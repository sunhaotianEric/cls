package com.team.lottery.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.signinconfig.SigninConfigMapper;
import com.team.lottery.vo.SigninConfig;


@Service("signinConfigService")
public class SigninConfigService {

	@Autowired
	private SigninConfigMapper signinConfigMapper;
	
	public int deleteByPrimaryKey(Integer id) {
		return signinConfigMapper.deleteByPrimaryKey(id);
	}
	
	public int insert(SigninConfig record){
		record.setCreateTime(new Date());
    	return signinConfigMapper.insert(record);
    }

    public SigninConfig selectByPrimaryKey(Integer id){
    	return signinConfigMapper.selectByPrimaryKey(id);
    }
    
    public int updateByPrimaryKeySelective(SigninConfig record){
    	return signinConfigMapper.updateByPrimaryKeySelective(record);
    }
    
    /**
     * 分页查询符合条件签到设置
     * @param query
     * @return
     */
    public Page getAllSignInConfigByQueryPage(SigninConfig query, Page page){
    	page.setTotalRecNum(signinConfigMapper.getAllSigninConfigByQueryPageCount(query));
		page.setPageContent(signinConfigMapper.getAllSigninConfigByQueryPage(query,page.getStartIndex(),page.getPageSize()));
		return page;
    }
    
    /**
     * 查询符合条件的签到设置
     * @param query
     * @return
     */
    public List<SigninConfig> selectByQuery(SigninConfig record){
    	return signinConfigMapper.selectByQuery(record);
    }
    
	
}
