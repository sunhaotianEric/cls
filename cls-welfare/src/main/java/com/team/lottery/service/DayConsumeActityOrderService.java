package com.team.lottery.service;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.enums.EDayConsumeActivityOrderStatus;
import com.team.lottery.enums.EMoneyDetailType;
import com.team.lottery.enums.ENoteStatus;
import com.team.lottery.enums.ENoteType;
import com.team.lottery.extvo.DayConsumeActityOrderQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.dayconsumeactityorder.DayConsumeActityOrderMapper;
import com.team.lottery.mapper.user.UserMapper;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.MakeOrderNum;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.DayConsumeActityOrder;
import com.team.lottery.vo.MoneyDetail;
import com.team.lottery.vo.Notes;
import com.team.lottery.vo.User;

@Service
public class DayConsumeActityOrderService {
	private static Logger logger = LoggerFactory.getLogger(DayConsumeActityOrderService.class);

	@Autowired
	private DayConsumeActityOrderMapper dayConsumeActityOrderMapper;
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private UserService userService;
	@Autowired
	private UserMoneyTotalService userMoneyTotalService;
	@Autowired
	private MoneyDetailService moneyDetailService;
	@Autowired
	private NotesService notesService;

	public int deleteByPrimaryKey(Long id) {
		return dayConsumeActityOrderMapper.deleteByPrimaryKey(id);
	}

	public int insert(DayConsumeActityOrder record) {
		return dayConsumeActityOrderMapper.insert(record);
	}

	public int insertSelective(DayConsumeActityOrder record) {
		return dayConsumeActityOrderMapper.insertSelective(record);
	}

	public DayConsumeActityOrder selectByPrimaryKey(Long id) {
		return dayConsumeActityOrderMapper.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(DayConsumeActityOrder record) {
		return dayConsumeActityOrderMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(DayConsumeActityOrder record) {
		return dayConsumeActityOrderMapper.updateByPrimaryKey(record);
	}

	public Page getDayConsumeActityOrderPage(DayConsumeActityOrderQuery query, Page page) {
		if (query.getBelongDate() != null) {
			// 初始化时设置 日期和时间模式
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(query.getBelongDate());
			calendar.set(Calendar.HOUR_OF_DAY, 23);
			calendar.set(Calendar.MINUTE, 59);
			calendar.set(Calendar.SECOND, 59);
			Date endTime = calendar.getTime();
			query.setBelongDateStart(query.getBelongDate());
			query.setBelongDateEnd(endTime);
		}
		page.setTotalRecNum(dayConsumeActityOrderMapper.getDayConsumeActivitysCount(query));
		page.setPageContent(dayConsumeActityOrderMapper.getDayConsumeActityOrderPage(query, page.getStartIndex(),
				page.getPageSize()));
		return page;
	}

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void insertBatch(List<DayConsumeActityOrder> list) throws Exception {
		try {
			if (CollectionUtils.isNotEmpty(list)) {
				dayConsumeActityOrderMapper.insertBatch(list);
			}
		} catch (Exception e) {
			logger.error("批量插入待遇申请数据出错", e);
			throw e;
		}
	}

	/**
	 * 查询用户昨天的流水返送订单
	 * 
	 * @param userId
	 */
	public DayConsumeActityOrder getYesterDayConsumeActivityOrder(Integer userId) {
		DayConsumeActityOrder dayConsumeActityOrderOrder = null;

		// 查询昨天的日期
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, -1);
		Date startTime = DateUtil.getNowStartTimeByStart(calendar.getTime());
		Date endTime = DateUtil.getNowStartTimeByEnd(calendar.getTime());
		DayConsumeActityOrderQuery query = new DayConsumeActityOrderQuery();
		query.setUserId(userId);
		query.setBelongDateStart(startTime);
		query.setBelongDateEnd(endTime);
		List<DayConsumeActityOrder> dayConsumeActityOrders = dayConsumeActityOrderMapper.getDayConsumeActityOrderByCondition(query);

		// 每日活动
		if (CollectionUtils.isNotEmpty(dayConsumeActityOrders)) {
			if (dayConsumeActityOrders.size() > 1) {
				logger.error("查询流水返送订单发现有2条记录，查询用户id[{}]", userId);
				// 若查询大于2条记录则返回空
				return dayConsumeActityOrderOrder;
			}
			// 昨天的流水返送订单应当只有一条记录
			dayConsumeActityOrderOrder = dayConsumeActityOrders.get(0);
		}
		return dayConsumeActityOrderOrder;
	}

	/**
	 * 批量处理申请生成订单数据
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int batchTreatmentApply(List<DayConsumeActityOrder> dayConsumeActityOrders) {
		// 插入订单的条数
		int count = 0;
		Date nowDate = new Date();
		if (CollectionUtils.isNotEmpty(dayConsumeActityOrders)) {
			for (DayConsumeActityOrder dayConsumeActityOrder : dayConsumeActityOrders) {
				// 只有未申请的状态才能进入订单
				if (EDayConsumeActivityOrderStatus.NOT_APPLY.getCode() == dayConsumeActityOrder.getReleaseStatus()) {
					DayConsumeActityOrder actityOrder = new DayConsumeActityOrder();
					String orderId = MakeOrderNum.makeOrderNum(MakeOrderNum.CJ);
					actityOrder.setSerialNumber(orderId);
					// 设置处理订单的管理员
					actityOrder.setId(dayConsumeActityOrder.getId());
					actityOrder.setBizSystem(dayConsumeActityOrder.getBizSystem());
					actityOrder.setUserId(dayConsumeActityOrder.getUserId());
					actityOrder.setUserName(dayConsumeActityOrder.getUserName());
					actityOrder.setMoney(dayConsumeActityOrder.getMoney());
					actityOrder.setLotteryMoney(dayConsumeActityOrder.getLotteryMoney());
					actityOrder.setBelongDate(dayConsumeActityOrder.getBelongDate());
					actityOrder.setReleaseStatus(EDayConsumeActivityOrderStatus.NOT_APPLY.getCode());
					actityOrder.setRemark(dayConsumeActityOrder.getRemark());
					actityOrder.setCreatedDate(nowDate);
					actityOrder.setFfcLotteryMoney(dayConsumeActityOrder.getFfcLotteryMoney());
					actityOrder.setFfcMoney(dayConsumeActityOrder.getFfcMoney());
					actityOrder.setFfcReleaseStatus(dayConsumeActityOrder.getFfcReleaseStatus());
					insertSelective(actityOrder);
					count++;
				}
			}
		}
		return count;
	}

	/**
	 * 根据归属时间查询
	 * 
	 * @param belongDate
	 * @return
	 */
	public List<DayConsumeActityOrder> getDayConsumeActityOrderBybelongDate(
			DayConsumeActityOrderQuery consumeActityOrder) {
		DayConsumeActityOrderQuery query = new DayConsumeActityOrderQuery();
		// 初始化时设置 日期和时间模式
		if (consumeActityOrder.getBelongDate() != null) {
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(consumeActityOrder.getBelongDate());
			calendar.set(Calendar.HOUR_OF_DAY, 23);
			calendar.set(Calendar.MINUTE, 59);
			calendar.set(Calendar.SECOND, 59);
			Date endTime = calendar.getTime();
			// 查询条件
			query.setReleaseStatus(EDayConsumeActivityOrderStatus.APPLIED.getCode());
			query.setBelongDateStart(consumeActityOrder.getBelongDate());
			query.setBelongDateEnd(endTime);
			query.setBizSystem(consumeActityOrder.getBizSystem());
		}
		List<DayConsumeActityOrder> actityOrderBybelongDate = dayConsumeActityOrderMapper
				.getDayConsumeActityOrderBybelongDate(query);
		return actityOrderBybelongDate;
	}

	/**
	 * 用户流水返送发放金额相关操作
	 * 
	 * @param operateUser
	 * @throws Exception
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void giveActityMoney(Long oderId, Admin currentAdmin) throws Exception, Exception {
		// 申请的每日活动金金额
		DayConsumeActityOrder dayConsumeActityOrder = dayConsumeActityOrderMapper.selectByPrimaryKey(oderId);

		if (dayConsumeActityOrder == null) {
			return;
		}

		Date nowDate = new Date();

		logger.info("用户开始领取活动彩金, userId[" + dayConsumeActityOrder.getUserId() + "], 领取金额["
				+ dayConsumeActityOrder.getMoney() + "]");
		User operationUser = userMapper.selectByPrimaryKey(dayConsumeActityOrder.getUserId());

		if (operationUser == null) {
			return;
		}
		// 游客 领取就是无效订单
		Integer enabled = operationUser.getIsTourist() == 1 ? 0 : 1;

		MoneyDetail moneyDetail = new MoneyDetail();
		moneyDetail.setEnabled(enabled);
		moneyDetail.setUserId(operationUser.getId()); // 用户id
		moneyDetail.setOrderId(null); // 订单id
		moneyDetail.setUserName(operationUser.getUserName()); // 用户名
		moneyDetail.setBizSystem(operationUser.getBizSystem());
		moneyDetail.setOperateUserName(currentAdmin.getUsername()); // 操作人员
		// 订单单号
		String newOrderId = MakeOrderNum.makeOrderNum(MakeOrderNum.CJ);
		moneyDetail.setLotteryId(newOrderId);
		moneyDetail.setWinLevel(null);
		moneyDetail.setWinCost(null);
		moneyDetail.setWinCostRule(null);

		if (dayConsumeActityOrder.getReleaseStatus().equals(EDayConsumeActivityOrderStatus.APPLIED.getCode())) {

			// 资金明细
			moneyDetail.setDetailType(EMoneyDetailType.ACTIVITIES_MONEY.name());
			moneyDetail.setIncome(dayConsumeActityOrder.getMoney());
			moneyDetail.setPay(new BigDecimal(0));
			moneyDetail.setBalanceBefore(operationUser.getMoney()); // 收入前金额
			// 活动彩金
			operationUser.addMoney(dayConsumeActityOrder.getMoney(), false);
			moneyDetail.setBalanceAfter(operationUser.getMoney()); // 收入后的金额
			moneyDetail.setDescription("活动彩金:" + dayConsumeActityOrder.getMoney());
			// 更新申请表状态为已发放
			DayConsumeActityOrder updateDayConsumeActityOrder = new DayConsumeActityOrder();
			updateDayConsumeActityOrder.setId(dayConsumeActityOrder.getId());
			updateDayConsumeActityOrder.setReleaseStatus(EDayConsumeActivityOrderStatus.RELEASED.getCode());
			updateDayConsumeActityOrder.setUpdateDate(nowDate);
			dayConsumeActityOrderMapper.updateByPrimaryKeySelective(updateDayConsumeActityOrder);

			userService.updateByPrimaryKeyWithVersionSelective(operationUser);
			moneyDetailService.insertSelective(moneyDetail); // 保存资金明细

			// 发送系统消息
			Notes notes = new Notes();
			notes.setParentId(0l);
			notes.setEnabled(1);
			notes.setToUserId(dayConsumeActityOrder.getUserId());
			notes.setToUserName(dayConsumeActityOrder.getUserName());
			notes.setBizSystem(dayConsumeActityOrder.getBizSystem());
			notes.setFromUserName(ConstantUtil.SYSTEM_OPERATE_NAME);
			notes.setSub("[流水返送]");
			notes.setBody("恭喜您,获得流水返送,赠送金额：" + dayConsumeActityOrder.getMoney());
			notes.setType(ENoteType.SYSTEM.name());
			notes.setStatus(ENoteStatus.NO_READ.name());
			notes.setCreatedDate(new Date());
			notesService.insertSelective(notes);
			// 最后执行 增加用户总金额 线程问题
			userMoneyTotalService.addUserTotalMoneyByType(operationUser, EMoneyDetailType.ACTIVITIES_MONEY,
					dayConsumeActityOrder.getMoney());
		}
	}

	/**
	 * 用户流水返送发放金额相关操作
	 * 
	 * @param operateUser
	 * @throws Exception
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void giveActityFfcMoney(Long oderId, Admin currentAdmin) throws Exception, Exception {
		// 申请的每日活动金金额
		DayConsumeActityOrder dayConsumeActityOrder = dayConsumeActityOrderMapper.selectByPrimaryKey(oderId);

		if (dayConsumeActityOrder == null) {
			return;
		}

		Date nowDate = new Date();

		logger.info("用户开始领取活动彩金, userId[" + dayConsumeActityOrder.getUserId() + "], 领取金额["
				+ dayConsumeActityOrder.getFfcMoney() + "]");
		User operationUser = userMapper.selectByPrimaryKey(dayConsumeActityOrder.getUserId());

		if (operationUser == null) {
			return;
		}
		// 游客 领取就是无效订单
		Integer enabled = operationUser.getIsTourist() == 1 ? 0 : 1;

		MoneyDetail moneyDetail = new MoneyDetail();
		moneyDetail.setEnabled(enabled);
		moneyDetail.setUserId(operationUser.getId()); // 用户id
		moneyDetail.setOrderId(null); // 订单id
		moneyDetail.setUserName(operationUser.getUserName()); // 用户名
		moneyDetail.setBizSystem(operationUser.getBizSystem());
		moneyDetail.setOperateUserName(currentAdmin.getUsername()); // 操作人员
		// 订单单号
		String newOrderId = MakeOrderNum.makeOrderNum(MakeOrderNum.CJ);
		moneyDetail.setLotteryId(newOrderId);
		moneyDetail.setWinLevel(null);
		moneyDetail.setWinCost(null);
		moneyDetail.setWinCostRule(null);

		if (dayConsumeActityOrder.getFfcReleaseStatus().equals(EDayConsumeActivityOrderStatus.APPLIED.getCode())) {

			// 资金明细
			moneyDetail.setDetailType(EMoneyDetailType.ACTIVITIES_MONEY.name());
			moneyDetail.setIncome(dayConsumeActityOrder.getFfcMoney());
			moneyDetail.setPay(new BigDecimal(0));
			moneyDetail.setBalanceBefore(operationUser.getMoney()); // 收入前金额
			// 活动彩金
			operationUser.addMoney(dayConsumeActityOrder.getFfcMoney(), false);
			moneyDetail.setBalanceAfter(operationUser.getMoney()); // 收入后的金额
			moneyDetail.setDescription("活动彩金:" + dayConsumeActityOrder.getFfcMoney());
			// 更新申请表状态为已发放
			DayConsumeActityOrder updateDayConsumeActityOrder = new DayConsumeActityOrder();
			updateDayConsumeActityOrder.setId(dayConsumeActityOrder.getId());
			updateDayConsumeActityOrder.setFfcReleaseStatus(EDayConsumeActivityOrderStatus.RELEASED.getCode());
			updateDayConsumeActityOrder.setUpdateDate(nowDate);
			dayConsumeActityOrderMapper.updateByPrimaryKeySelective(updateDayConsumeActityOrder);

			userService.updateByPrimaryKeyWithVersionSelective(operationUser);
			moneyDetailService.insertSelective(moneyDetail); // 保存资金明细

			// 发送系统消息
			Notes notes = new Notes();
			notes.setParentId(0l);
			notes.setEnabled(1);
			notes.setToUserId(dayConsumeActityOrder.getUserId());
			notes.setToUserName(dayConsumeActityOrder.getUserName());
			notes.setBizSystem(dayConsumeActityOrder.getBizSystem());
			notes.setFromUserName(ConstantUtil.SYSTEM_OPERATE_NAME);
			notes.setSub("[流水返送]");
			notes.setBody("恭喜您,获得流水返送,赠送金额：" + dayConsumeActityOrder.getFfcMoney());
			notes.setType(ENoteType.SYSTEM.name());
			notes.setStatus(ENoteStatus.NO_READ.name());
			notes.setCreatedDate(new Date());
			notesService.insertSelective(notes);
			// 最后执行 增加用户总金额 线程问题
			userMoneyTotalService.addUserTotalMoneyByType(operationUser, EMoneyDetailType.ACTIVITIES_MONEY,
					dayConsumeActityOrder.getFfcMoney());
		}
	}

	public List<DayConsumeActityOrder> getDayConsumeActityOrderByCondition(DayConsumeActityOrderQuery query) {
		return dayConsumeActityOrderMapper.getDayConsumeActityOrderByCondition(query);
	}
}
