package com.team.lottery.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.mapper.userbonusconfig.UserBonusConfigMapper;
import com.team.lottery.vo.UserBonusConfig;

@Service("userBonusConfigService")
public class UserBonusConfigService {

	@Autowired
	private UserBonusConfigMapper userBonusConfigMapper;
	
	public int deleteByPrimaryKey(Long id) {
		return userBonusConfigMapper.deleteByPrimaryKey(id);
	}
	
	public int insertSelective(UserBonusConfig record){
    	return userBonusConfigMapper.insertSelective(record);
    }

    public UserBonusConfig selectByPrimaryKey(Long id){
    	return userBonusConfigMapper.selectByPrimaryKey(id);
    }
    
    public int updateByPrimaryKeySelective(UserBonusConfig record){
    	return userBonusConfigMapper.updateByPrimaryKeySelective(record);
    }
	
	public List<UserBonusConfig> getAllUserBonusConfigs() {
		return userBonusConfigMapper.getAllUserBounsConfigs();
	}
	
	
}
