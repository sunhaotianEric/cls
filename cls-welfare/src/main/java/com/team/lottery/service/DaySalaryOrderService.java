package com.team.lottery.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.enums.EDaySalaryOrderStatus;
import com.team.lottery.enums.EMoneyDetailType;
import com.team.lottery.enums.ENoteStatus;
import com.team.lottery.enums.ENoteType;
import com.team.lottery.enums.EUserDailLiLevel;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.SalaryOrderQuery;
import com.team.lottery.extvo.UserTotalMoneyByTypeVo;
import com.team.lottery.mapper.daysalaryorder.DaySalaryOrderMapper;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.MakeOrderNum;
import com.team.lottery.vo.DaySalaryOrder;
import com.team.lottery.vo.MoneyDetail;
import com.team.lottery.vo.Notes;
import com.team.lottery.vo.User;


@Service("daySalaryOrderService")
public class DaySalaryOrderService {

	@Autowired
	private DaySalaryOrderMapper daySalaryOrderMapper;
	@Autowired
	private MoneyDetailService moneyDetailService;
	@Autowired
	private UserService userService;
	@Autowired
	private UserMoneyTotalService userMoneyTotalService;
	@Autowired
	private NotesService notesService;
	
	public int deleteByPrimaryKey(Long id) {
		return daySalaryOrderMapper.deleteByPrimaryKey(id);
	}
	
	public int insertSelective(DaySalaryOrder record){
    	return daySalaryOrderMapper.insertSelective(record);
    }

    public DaySalaryOrder selectByPrimaryKey(Long id){
    	return daySalaryOrderMapper.selectByPrimaryKey(id);
    }
    
    public int updateByPrimaryKeySelective(DaySalaryOrder record){
    	return daySalaryOrderMapper.updateByPrimaryKeySelective(record);
    }
    
    /**
     * 根据条件查询符合条件的记录
     * @param salaryOrderQuery
     * @return
     */
    public List<DaySalaryOrder> querySalaryOrdersByCondition(SalaryOrderQuery salaryOrderQuery) {
    	return daySalaryOrderMapper.querySalaryOrdersByCondition(salaryOrderQuery);
    }
    
    public Page selectSalaryOrderByUserList(SalaryOrderQuery salaryOrderQuery,Page page){
    	page.setTotalRecNum(daySalaryOrderMapper.getDaySalaryOrderByQueryPageCount(salaryOrderQuery));
		page.setPageContent(daySalaryOrderMapper.getDaySalaryOrderByQueryPage(salaryOrderQuery,page.getStartIndex(),page.getPageSize()));
    	return page;
    }
    
    /**
     * 发放日工资操作
     * @param id
     * @param isForceRelease
     * @throws Exception
     */
	public void releaseDaysalary(Long id, boolean isForceRelease) throws Exception {
		DaySalaryOrder daySalaryOrder = daySalaryOrderMapper.selectByPrimaryKey(id);
		User user = userService.selectByPrimaryKey(daySalaryOrder.getUserId());
		User fromUser = userService.selectByPrimaryKey(daySalaryOrder.getFromUserId());
		List<UserTotalMoneyByTypeVo> userTotalMoneyByTypeVos = new ArrayList<UserTotalMoneyByTypeVo>();
		if (daySalaryOrder != null) {
			// 是超级管理员的，进行发放处理，不扣减超级管理员余额
			if(EUserDailLiLevel.SUPERAGENCY.getCode().equals(fromUser.getDailiLevel())) {
				MoneyDetail moneyDetailAdd = new MoneyDetail();
				moneyDetailAdd.setEnabled((user.getIsTourist() == 1 ? 0 : 1));
				moneyDetailAdd.setUserId(user.getId());
				moneyDetailAdd.setOrderId(null);
				moneyDetailAdd.setOperateUserName(fromUser.getUserName()); // 操作人员
				moneyDetailAdd.setUserName(user.getUserName()); // 用户名
				moneyDetailAdd.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.GZ)); // 订单单号
				moneyDetailAdd.setExpect(""); // 哪个期号
				moneyDetailAdd.setLotteryType(""); // 彩种
				moneyDetailAdd.setLotteryTypeDes(""); // 彩种名称
				moneyDetailAdd.setDetailType(EMoneyDetailType.DAY_SALARY.name()); // 资金类型为日工资
				moneyDetailAdd.setIncome(daySalaryOrder.getMoney()); // 收入为0
				moneyDetailAdd.setPay(new BigDecimal(0)); // 支出金额
				moneyDetailAdd.setBalanceBefore(user.getMoney()); // 支出前金额
				moneyDetailAdd.setBalanceAfter(user.getMoney().add(daySalaryOrder.getMoney())); // 支出后的金额
				moneyDetailAdd.setWinLevel(null);
				moneyDetailAdd.setWinCost(null);
				moneyDetailAdd.setWinCostRule(null);
				moneyDetailAdd.setDescription("日工资收入:" + daySalaryOrder.getMoney());
				moneyDetailAdd.setBizSystem(user.getBizSystem());
				moneyDetailService.insertSelective(moneyDetailAdd);
				user.addMoney(daySalaryOrder.getMoney(), true);
				// 针对盈亏报表多线程不会事物回滚处理方式
				UserTotalMoneyByTypeVo userTotalMoneyByTypeVo = new UserTotalMoneyByTypeVo(user,
						EMoneyDetailType.DAY_SALARY, daySalaryOrder.getMoney());
				userTotalMoneyByTypeVos.add(userTotalMoneyByTypeVo);
				// userMoneyTotalService.addUserTotalMoneyByType(addUser,
				// EMoneyDetailType.DAY_SALARY, daySalaryOrder.getMoney());
				userService.updateByPrimaryKeyWithVersionSelective(user);
				if (isForceRelease) {
					daySalaryOrder.setReleaseStatus(EDaySalaryOrderStatus.FORCE_RELEASED.getCode());
				} else {
					daySalaryOrder.setReleaseStatus(EDaySalaryOrderStatus.RELEASED.getCode());
				}
				daySalaryOrder.setReleaseDate(new Date());
				daySalaryOrder.setUpdateDate(new Date());
				this.updateByPrimaryKeySelective(daySalaryOrder);
				
			} else {
				//非超级管理员的日工资发放，先从发放者扣除余额，不够发放则停止
				if (fromUser.getMoney().compareTo(daySalaryOrder.getMoney()) < 0) {
					throw new Exception("用户余额不足,不能发放工资!");
				}
				// 扣除工资(上级)
				MoneyDetail moneyDetailSub = new MoneyDetail();
				moneyDetailSub.setEnabled((fromUser.getIsTourist() == 1 ? 0 : 1));
				moneyDetailSub.setUserId(fromUser.getId());
				moneyDetailSub.setOrderId(null);
				moneyDetailSub.setOperateUserName(fromUser.getUserName()); // 操作人员
				moneyDetailSub.setUserName(fromUser.getUserName()); // 用户名
				moneyDetailSub.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.GZ)); // 订单单号
				moneyDetailSub.setExpect(""); // 哪个期号
				moneyDetailSub.setLotteryType(""); // 彩种
				moneyDetailSub.setLotteryTypeDes(""); // 彩种名称
				moneyDetailSub.setDetailType(EMoneyDetailType.DAY_SALARY.name()); // 资金类型为日工资
				moneyDetailSub.setIncome(new BigDecimal(0)); // 收入为0
				moneyDetailSub.setPay(daySalaryOrder.getMoney()); // 支出金额
				moneyDetailSub.setBalanceBefore(fromUser.getMoney()); // 支出前金额
				moneyDetailSub.setBalanceAfter(fromUser.getMoney().subtract(daySalaryOrder.getMoney())); // 支出后的金额
				moneyDetailSub.setWinLevel(null);
				moneyDetailSub.setWinCost(null);
				moneyDetailSub.setWinCostRule(null);
				moneyDetailSub.setDescription("日工资支出:" + daySalaryOrder.getMoney());
				moneyDetailSub.setBizSystem(fromUser.getBizSystem());
				moneyDetailService.insertSelective(moneyDetailSub);
				fromUser.reduceMoney(daySalaryOrder.getMoney(), true);
				// 针对盈亏报表多线程不会事物回滚处理方式
				UserTotalMoneyByTypeVo userTotalMoneyByTypeVo = new UserTotalMoneyByTypeVo(fromUser, EMoneyDetailType.DAY_SALARY, BigDecimal.ZERO.subtract(daySalaryOrder.getMoney()));
				userTotalMoneyByTypeVos.add(userTotalMoneyByTypeVo);
				// userMoneyTotalService.addUserTotalMoneyByType(subUser,
				// EMoneyDetailType.DAY_SALARY, daySalaryOrder.getMoney());
				userService.updateByPrimaryKeyWithVersionSelective(fromUser);
				
				// 获取工资(下级)
				MoneyDetail moneyDetailAdd = new MoneyDetail();
				moneyDetailAdd.setEnabled((user.getIsTourist() == 1 ? 0 : 1));
				moneyDetailAdd.setUserId(user.getId());
				moneyDetailAdd.setOrderId(null);
				moneyDetailAdd.setOperateUserName(fromUser.getUserName()); // 操作人员
				moneyDetailAdd.setUserName(user.getUserName()); // 用户名
				moneyDetailAdd.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.GZ)); // 订单单号
				moneyDetailAdd.setExpect(""); // 哪个期号
				moneyDetailAdd.setLotteryType(""); // 彩种
				moneyDetailAdd.setLotteryTypeDes(""); // 彩种名称
				moneyDetailAdd.setDetailType(EMoneyDetailType.DAY_SALARY.name()); // 资金类型为日工资
				moneyDetailAdd.setIncome(daySalaryOrder.getMoney()); // 收入为0
				moneyDetailAdd.setPay(new BigDecimal(0)); // 支出金额
				moneyDetailAdd.setBalanceBefore(user.getMoney()); // 支出前金额
				moneyDetailAdd.setBalanceAfter(user.getMoney().add(daySalaryOrder.getMoney())); // 支出后的金额
				moneyDetailAdd.setWinLevel(null);
				moneyDetailAdd.setWinCost(null);
				moneyDetailAdd.setWinCostRule(null);
				moneyDetailAdd.setDescription("日工资收入:" + daySalaryOrder.getMoney());
				moneyDetailAdd.setBizSystem(user.getBizSystem());
				moneyDetailService.insertSelective(moneyDetailAdd);
				user.addMoney(daySalaryOrder.getMoney(), true);
				// 针对盈亏报表多线程不会事物回滚处理方式
				UserTotalMoneyByTypeVo userTotalMoneyByTypeVo2 = new UserTotalMoneyByTypeVo(user,
						EMoneyDetailType.DAY_SALARY, daySalaryOrder.getMoney());
				userTotalMoneyByTypeVos.add(userTotalMoneyByTypeVo2);
				// userMoneyTotalService.addUserTotalMoneyByType(addUser,
				// EMoneyDetailType.DAY_SALARY, daySalaryOrder.getMoney());
				userService.updateByPrimaryKeyWithVersionSelective(user);
				if (isForceRelease) {
					daySalaryOrder.setReleaseStatus(EDaySalaryOrderStatus.FORCE_RELEASED.getCode());
				} else {
					daySalaryOrder.setReleaseStatus(EDaySalaryOrderStatus.RELEASED.getCode());
				}
				daySalaryOrder.setReleaseDate(new Date());
				daySalaryOrder.setUpdateDate(new Date());
				this.updateByPrimaryKeySelective(daySalaryOrder);
			}
		}
		// 循环遍历插入报表总额和盈亏报表
		for (UserTotalMoneyByTypeVo userTotalMoneyByTypeVo : userTotalMoneyByTypeVos) {
			userMoneyTotalService.addUserTotalMoneyByType(userTotalMoneyByTypeVo.getUser(),
					userTotalMoneyByTypeVo.getDetailType(), userTotalMoneyByTypeVo.getOperationValue());
		}
		
		 //发送系统消息
        Notes notes = new Notes();
        notes.setParentId(0l);
        notes.setEnabled(1);
        notes.setToUserId(user.getId());
        notes.setToUserName(user.getUserName());
        notes.setBizSystem(user.getBizSystem());
        notes.setFromUserName(ConstantUtil.SYSTEM_OPERATE_NAME);
        notes.setSub("[日工资发放通知]");
        notes.setBody("您的上级发放了归属日期["+ daySalaryOrder.getBelongDateStr() +"]的日工资，金额为："+ daySalaryOrder.getMoney()); 
        notes.setType(ENoteType.SYSTEM.name()); 
        notes.setStatus(ENoteStatus.NO_READ.name());
        notes.setCreatedDate(new Date());
        notesService.insertSelective(notes);
	}
	
	/**
     * 查询某段时间内平台发放的日工资总额
     * @param startTime
     * @param endTime
     * @return
     */
    public BigDecimal queryDaySalaryOrderTotalMoney(String bizSystem, String fromUserName, Date startTime, Date endTime) {
    	BigDecimal totalMoney = BigDecimal.ZERO;
    	BigDecimal queryTotalMoney = daySalaryOrderMapper.queryDaySalaryOrderTotalMoney(bizSystem, fromUserName, startTime, endTime);
    	if(queryTotalMoney != null) {
    		totalMoney = queryTotalMoney;
    	}
    	return totalMoney;
    }
}
