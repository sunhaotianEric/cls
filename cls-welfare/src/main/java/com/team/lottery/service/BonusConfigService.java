package com.team.lottery.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.enums.ENoteStatus;
import com.team.lottery.enums.ENoteType;
import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.bonusconfig.BonusConfigMapper;
import com.team.lottery.mapper.user.UserMapper;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.vo.BonusAuth;
import com.team.lottery.vo.BonusConfig;
import com.team.lottery.vo.Notes;
import com.team.lottery.vo.User;

@Service("bonusConfigService")
public class BonusConfigService {
	
	
	@Autowired
	private BonusConfigMapper bonusConfigMapper;
	@Autowired
	private NotesService notesService;
	@Autowired
	private BonusAuthService bonusAuthService;
	
	@Autowired
	private UserMapper userMapper;
	
	public int deleteByPrimaryKey(Long id) {
		return bonusConfigMapper.deleteByPrimaryKey(id);
	}
	
	public int insertSelective(BonusConfig record){
		record.setCreateTime(new Date());
    	return bonusConfigMapper.insertSelective(record);
    }

    public BonusConfig selectByPrimaryKey(Long id){
    	return bonusConfigMapper.selectByPrimaryKey(id);
    }
    
    public int updateByPrimaryKeySelective(BonusConfig record){
    	return bonusConfigMapper.updateByPrimaryKeySelective(record);
    }
	
    
    /**
     * 撤销契约，根据userId来删除
     * @param userId
     * @return
     */
    @Transactional(readOnly=false,rollbackFor=Exception.class)
    public void deleteByUid(Integer userId){
    	User user = userMapper.selectByPrimaryKey(userId);
    	User updateUser = new User();
    	updateUser.setId(user.getId());
    	
    	updateUser.setBonusState(User.BONUS_NOT_SIGN);
    	// 更新用户对象信息.
    	userMapper.updateByPrimaryKeySelective(updateUser);
    	// 根据用户ID删除契约信息.
    	bonusConfigMapper.deleteNewByUserId(userId);
    	
    	// 判断是否有分红权限授权，有的话同时进行删除
    	BonusAuth bonusAuth = bonusAuthService.queryByUserId(userId);
    	if(bonusAuth != null) {
    		bonusAuthService.deleteByPrimaryKey(bonusAuth.getId());
    	}
    	
    	// 发送通知短信.
    	Notes notes = new Notes();
        notes.setParentId(0l);
        notes.setEnabled(1);
        notes.setBizSystem(user.getBizSystem());
        notes.setFromUserName(ConstantUtil.SYSTEM_OPERATE_NAME);
        notes.setFromUserId(null);
        notes.setToUserName(user.getUserName());
        notes.setToUserId(user.getId());
        notes.setSub("[契约变更通知]");
        notes.setBody("您的上级解除了对您的契约."); 
        notes.setType(ENoteType.SYSTEM.name()); 
        notes.setStatus(ENoteStatus.NO_READ.name());
        notes.setCreatedDate(new Date());
        notesService.insertSelective(notes); 
    }
    
    /**
     * 分页条件查询
     * @return
     */
    public Page getBonusConfigByQueryPage(BonusConfig query,Page page){
    	page.setTotalRecNum(bonusConfigMapper.getBonusConfigByQueryPageCount(query));
    	page.setPageContent(bonusConfigMapper.getBonusConfigByQueryPage(query, page.getStartIndex(),page.getPageSize()));
       return page;
    }
    
    
    /**
     * 查询符合条件的契约记录
     * @param query
     * @return
     */
    public List<BonusConfig> getBonusConfigByQuery(BonusConfig query){
    	return bonusConfigMapper.getBonusConfigByQuery(query);
    }
	
    /**
     * 建立契约，重新签订契约
     * @param list
     * @return
     * @throws Exception 
     */
    @Transactional(readOnly=false,rollbackFor=Exception.class)
	public void createBonusConfigs(List<BonusConfig> list, User bonusUser ,User currentUser,BonusAuth userBonusAuth ,Integer bonusAuth) throws Exception {
		try {
			//重新签订契约要先删除之前未同意的契约
    		bonusConfigMapper.deleteNewByUserId(bonusUser.getId());
			for (BonusConfig bonusConfig : list) {
				// list有且仅有一个getScale=0，保底分红
				if (bonusConfig.getScale().compareTo(BigDecimal.ZERO) == 0) {
					User user = new User();
					user.setId(bonusUser.getId());
					user.setBonusState(User.BONUS_SIGN_AGREE);
					userMapper.updateByPrimaryKeySelective(user);
				}
				bonusConfig.setBizSystem(bonusUser.getBizSystem());
				bonusConfig.setCreateTime(new Date());
				bonusConfig.setCreateUserId(currentUser.getId());
				bonusConfig.setCreateUserName(currentUser.getUserName());
				bonusConfig.setDescription(null);
				bonusConfig.setType("CURRENT");
				bonusConfig.setUpdateTime(new Date());
				bonusConfig.setUserId(bonusUser.getId());
				bonusConfig.setUserName(bonusUser.getUserName());
				this.insertSelective(bonusConfig);
			}
			// 当前用户有授权对象时
			if (userBonusAuth != null) {
				// 撤销授权，进行删除
				if (bonusAuth == 0) {
					bonusAuthService.deleteByPrimaryKey(userBonusAuth.getId());
				} else {
					bonusAuthService.deleteByPrimaryKey(userBonusAuth.getId());
					userBonusAuth = new BonusAuth();
					userBonusAuth.setBizSystem(bonusUser.getBizSystem());
					userBonusAuth.setUserId(bonusUser.getId());
					userBonusAuth.setUserName(bonusUser.getUserName());
					userBonusAuth.setCreateTime(new Date());
					bonusAuthService.insertSelective(userBonusAuth);
				}
			} else {
				if (bonusAuth == 1) {
					userBonusAuth = new BonusAuth();
					userBonusAuth.setBizSystem(bonusUser.getBizSystem());
					userBonusAuth.setUserId(bonusUser.getId());
					userBonusAuth.setUserName(bonusUser.getUserName());
					userBonusAuth.setCreateTime(new Date());
					bonusAuthService.insertSelective(userBonusAuth);
				}
			}
			//发送站内信通知下级用户有新契约
			Notes notes = new Notes();
            notes.setParentId(0l);
            notes.setEnabled(1);
            notes.setBizSystem(bonusUser.getBizSystem());
            notes.setFromUserName(ConstantUtil.SYSTEM_OPERATE_NAME);
            notes.setFromUserId(null);
            notes.setToUserName(bonusUser.getUserName());
            notes.setToUserId(bonusUser.getId());
            notes.setSub("[契约签订通知]");
            notes.setBody("您的上级向您签订了契约，请到代理中心-我的契约查看"); 
            notes.setType(ENoteType.SYSTEM.name()); 
            notes.setStatus(ENoteStatus.NO_READ.name());
            notes.setCreatedDate(new Date());
            notesService.insertSelective(notes); 
			
		} catch (Exception e) {
			throw new Exception("签订失败！" + e.getMessage());
		}
	}
    
    
    /**
     * 用户同意建立契约
     * @param list
     * @return
     * @throws Exception 
     */
    @Transactional(readOnly=false,rollbackFor=Exception.class)
    public void agreeBonusConfigs(Integer userId) throws Exception{
    	User user=userMapper.selectByPrimaryKey(userId);
    	if(user.getBonusState()!=1&&user.getBonusState()!=3){
    		throw new Exception("契约已经被撤销！或者其他原因！请重新操作！");
    	}
    	BonusConfig query =new BonusConfig();
    	query.setUserId(userId);
    	query.setType(BonusConfig.BONUS_TYPE_CURRENT);
    	List<BonusConfig> list=this.getBonusConfigByQuery(query);
    	for(BonusConfig bonusConfig:list){
    		bonusConfigMapper.deleteByPrimaryKey(bonusConfig.getId());
    	}
    	
        
    	query.setType(BonusConfig.BONUS_TYPE_NEW);
    	list=this.getBonusConfigByQuery(query);
    	if(CollectionUtils.isEmpty(list)) {
    		throw new Exception("当前没有新契约，操作失败");
    	}
    	//契约发起者信息
    	Integer toUserId = list.get(0).getCreateUserId();
        String toUserName = list.get(0).getCreateUserName();
        
    	for(BonusConfig bonusConfig:list){
    		if(bonusConfig.getScale().compareTo(BigDecimal.ZERO)==0){
    			BigDecimal bonusScale=bonusConfig.getValue().divide(new BigDecimal("100"));
    			//分红比例应该在用户同意签订后改变
    			user.setBonusScale(bonusScale);
    		}
    		
    		bonusConfig.setType(BonusConfig.BONUS_TYPE_CURRENT);
    		bonusConfigMapper.updateByPrimaryKeySelective(bonusConfig);
    	}
        
    	User updateUser = new User();
    	updateUser.setId(user.getId());
    	updateUser.setBonusState(User.BONUS_SIGN_AGREE);
    	userMapper.updateByPrimaryKeySelective(updateUser);
    	
    	//向契约发起者发送站内信
        Notes notes = new Notes();
        notes.setParentId(0l);
        notes.setEnabled(1);
        notes.setBizSystem(user.getBizSystem());
        notes.setFromUserName(ConstantUtil.SYSTEM_OPERATE_NAME);
        notes.setFromUserId(null);
        notes.setToUserName(toUserName);
        notes.setToUserId(toUserId);
        notes.setSub("[契约变更通知]");
        notes.setBody("您的下级用户["+ user.getUserName() +"]通过了您发送的契约，请到代理中心-团队列表，点击对应用户的签契约按钮查看"); 
        notes.setType(ENoteType.SYSTEM.name()); 
        notes.setStatus(ENoteStatus.NO_READ.name());
        notes.setCreatedDate(new Date());
        notesService.insertSelective(notes); 
    }
    
    /**
     * 用户拒接
     * @param list
     * @return
     * @throws Exception 
     */
    @Transactional(readOnly=false,rollbackFor=Exception.class)
    public void refuseBonusConfigs(Integer userId) throws Exception{
    	User user=userMapper.selectByPrimaryKey(userId);
    	
    	if(user.getBonusState()!=1&&user.getBonusState()!=3){
    		throw new Exception("契约已经被撤销！或者其他原因！请重新操作！");
    	}
    	User updateUser = new User();
    	updateUser.setId(user.getId());
	    if(user.getBonusState()==1){
	    	updateUser.setBonusState(User.BONUS_NOT_SIGN);
	    }else{
	    	updateUser.setBonusState(User.BONUS_SIGN_REJECT);
	    }
    	
    	userMapper.updateByPrimaryKeySelective(updateUser);
    	
    	//向契约发起者发送站内信
    	BonusConfig query =new BonusConfig();
    	query.setUserId(userId);
    	query.setType(BonusConfig.BONUS_TYPE_NEW);
    	List<BonusConfig> list = this.getBonusConfigByQuery(query);
    	if(CollectionUtils.isNotEmpty(list)) {
    		//契约发起者信息
    		Integer toUserId = list.get(0).getCreateUserId();
    		String toUserName = list.get(0).getCreateUserName();
    		
    		Notes notes = new Notes();
            notes.setParentId(0l);
            notes.setEnabled(1);
            notes.setBizSystem(user.getBizSystem());
            notes.setFromUserName(ConstantUtil.SYSTEM_OPERATE_NAME);
            notes.setFromUserId(null);
            notes.setToUserName(toUserName);
            notes.setToUserId(toUserId);
            notes.setSub("[契约变更通知]");
            notes.setBody("您的下级用户["+ user.getUserName() +"]拒绝了您发送的契约，请到代理中心-团队列表，点击对应用户的签契约按钮查看并操作，您可以重新修改或者撤销新的契约内容"); 
            notes.setType(ENoteType.SYSTEM.name()); 
            notes.setStatus(ENoteStatus.NO_READ.name());
            notes.setCreatedDate(new Date());
            notesService.insertSelective(notes); 
    	}
    	
    } 
    
    /**
     * 查找最大的分红比例
     * @param query
     * @return
     */
    public int getMaxValueBonusConfigByQuery(BonusConfig query) {
    	return bonusConfigMapper.getMaxValueBonusConfigByQuery(query);
    }
    
    /**
     * 查找最小的分红比例
     * @param query
     * @return
     */
    public int getMinValueBonusConfigByQuery(BonusConfig query) {
    	BonusConfig result = bonusConfigMapper.getMinValueBonusConfigByQuery(query);
    	if(result == null){
    		return 0;	
    	}else{
    		return result.getValue().intValue();
    	}
    }

    /**
     * 根据用户名查询契约记录(根据userId去重)
     * @param userName
     * @return
     */
	public List<BonusConfig> getCurrentUserBonusesByUserName(Integer createUserId) {
		return bonusConfigMapper.getCurrentUserBonusesByUserName(createUserId);
	}
}
