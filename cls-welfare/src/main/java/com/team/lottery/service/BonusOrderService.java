package com.team.lottery.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.enums.EBonusOrderStatus;
import com.team.lottery.enums.EMoneyDetailType;
import com.team.lottery.enums.ENoteStatus;
import com.team.lottery.enums.ENoteType;
import com.team.lottery.enums.EUserDailLiLevel;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.UserTotalMoneyByTypeVo;
import com.team.lottery.mapper.bonusorder.BonusOrderMapper;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.MakeOrderNum;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.BonusOrder;
import com.team.lottery.vo.MoneyDetail;
import com.team.lottery.vo.Notes;
import com.team.lottery.vo.User;

@Service("bonusOrderService")
public class BonusOrderService {
	
	private static Logger log = LoggerFactory.getLogger(BonusOrderService.class);

	@Autowired
	private BonusOrderMapper bonusOrderMapper;
	@Autowired
	private UserMoneyTotalService userMoneyTotalService;
	@Autowired
	private MoneyDetailService moneyDetailService;
	@Autowired
	private UserService userService;
	@Autowired
	private NotesService notesService;

	
	public int deleteByPrimaryKey(Long id) {
		return bonusOrderMapper.deleteByPrimaryKey(id);
	}
	
	public int insertSelective(BonusOrder record){
		record.setCreatedDate(new Date());
    	return bonusOrderMapper.insertSelective(record);
    }

    public BonusOrder selectByPrimaryKey(Long id){
    	return bonusOrderMapper.selectByPrimaryKey(id);
    }
    
    public int updateByPrimaryKeySelective(BonusOrder record){
    	return bonusOrderMapper.updateByPrimaryKeySelective(record);
    } 
    
    /**
     * 查询符合条件记录
     * @param query
     * @return
     */
    public List<BonusOrder> getBonusOrderByQuery(BonusOrder query){
		return bonusOrderMapper.getBonusOrderByQuery(query);
    	
    }
    
    /**
     * 查询统计某个用户下级所要 发放的总金额-
     * @param query
     * @return
     */
    public BonusOrder getALlMoneyByQuery(BonusOrder query){
    	return bonusOrderMapper.getALlMoneyByQuery(query);
    }
    
    /**
     * 分页条件查询
     * @return
     */
    public Page getBonusOrderByQueryPage(BonusOrder query,Page page){
    	page.setTotalRecNum(bonusOrderMapper.getBonusOrderByQueryPageCount(query));
    	page.setPageContent(bonusOrderMapper.getBonusOrderByQueryPage(query, page.getStartIndex(),page.getPageSize()));
       return page;
    }
    
    /**
     * 分页条件查询
     * @return
     */
    public Page getMgrBonusOrderByQueryPage(BonusOrder query,Page page){
    	page.setTotalRecNum(bonusOrderMapper.getMgrBonusOrderByQueryPageCount(query));
    	page.setPageContent(bonusOrderMapper.getMgrBonusOrderByQueryPage(query, page.getStartIndex(),page.getPageSize()));
       return page;
    }
    
    /**
     * 统计
     * @return
     */
    public int getBonusOrderByQueryCount(BonusOrder query){
       return bonusOrderMapper.getBonusOrderByQueryPageCount(query);
    }
    
    /**
     * 前台 发放分红
     * @param query
     * @throws Exception 
     */
    @Transactional(readOnly=false,rollbackFor=Exception.class)
    public int releaseBounsMoney(BonusOrder query) throws Exception{
    	List<UserTotalMoneyByTypeVo> userTotalMoneyByTypeVos = new ArrayList<UserTotalMoneyByTypeVo>();
    	BonusOrder bonusOrder=bonusOrderMapper.selectByPrimaryKey(query.getId());
    	//属于已经发放完成了，或者不需分红的返回0
    	if(bonusOrder == null || "RELEASED".equals(bonusOrder.getReleaseStatus())||"NEEDNOT_RELEASE".equals(bonusOrder.getReleaseStatus())
    			||"OUTTIME_RELEASED".equals(bonusOrder.getReleaseStatus())||"FORCE_RELEASED".equals(bonusOrder.getReleaseStatus())){
    		return 0;
    	}
    	
    	bonusOrder.setReleaseStatus(EBonusOrderStatus.RELEASED.name());
    	bonusOrder.setReleaseDate(new Date());
    	
    	try {
	    	User parentUser=userService.selectByPrimaryKey(bonusOrder.getFromUserId());
	    	User user=userService.selectByPrimaryKey(bonusOrder.getUserId());
	    	
	    	//如果上级用户是超级代理
	    	if(parentUser.getDailiLevel().equals(EUserDailLiLevel.SUPERAGENCY.name())){
	    		    //针对盈亏报表多线程不会事物回滚处理方式
	    		    UserTotalMoneyByTypeVo userTotalMoneyByTypeVo =  new UserTotalMoneyByTypeVo(user, EMoneyDetailType.HALF_MONTH_BOUNS, bonusOrder.getMoney());
	    		    userTotalMoneyByTypeVos.add(userTotalMoneyByTypeVo);
	    		    //userMoneyTotalService.addUserTotalMoneyByType(user, EMoneyDetailType.HALF_MONTH_BOUNS, bonusOrder.getMoney());
					MoneyDetail dailiUserMoneyDetail = new MoneyDetail();
					dailiUserMoneyDetail.setUserId(user.getId());  //用户id 
					dailiUserMoneyDetail.setOrderId(null); //订单id
					dailiUserMoneyDetail.setBizSystem(user.getBizSystem());
					dailiUserMoneyDetail.setUserName(user.getUserName()); //用户名
					dailiUserMoneyDetail.setOperateUserName(ConstantUtil.SYSTEM_OPERATE_NAME); //操作人员
					dailiUserMoneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.FH)); //订单单号
					dailiUserMoneyDetail.setWinLevel(null);
					dailiUserMoneyDetail.setWinCost(null);
					dailiUserMoneyDetail.setWinCostRule(null);
					dailiUserMoneyDetail.setDetailType(EMoneyDetailType.HALF_MONTH_BOUNS.name()); 
					dailiUserMoneyDetail.setIncome(bonusOrder.getMoney()); 
					dailiUserMoneyDetail.setPay(new BigDecimal(0)); 
					dailiUserMoneyDetail.setBalanceBefore(user.getMoney()); 
					dailiUserMoneyDetail.setBalanceAfter(user.getMoney().add(bonusOrder.getMoney()));  //支出后的金额
					dailiUserMoneyDetail.setDescription("契约分红收入:" + bonusOrder.getMoney());
					moneyDetailService.insertSelective(dailiUserMoneyDetail); //保存资金明细
					user.addMoney(bonusOrder.getMoney(), true);
					userService.updateByPrimaryKeyWithVersionSelective(user);
	    		
	    	}else{
	    	
		    	if(parentUser.getMoney().compareTo(bonusOrder.getMoney())<0){
		    		throw new Exception("账号余额不足！请先充值！");
		    	}
		    	 //针对盈亏报表多线程不会事物回滚处理方式
		    	UserTotalMoneyByTypeVo userTotalMoneyByTypeVo =  new UserTotalMoneyByTypeVo(parentUser, EMoneyDetailType.HALF_MONTH_BOUNS, BigDecimal.ZERO.subtract(bonusOrder.getMoney()));
		    	userTotalMoneyByTypeVos.add(userTotalMoneyByTypeVo);
			    //当前用分红，父级余额扣减
				//userMoneyTotalService.addUserTotalMoneyByType(parentUser, EMoneyDetailType.HALF_MONTH_BOUNS, BigDecimal.ZERO.subtract(bonusOrder.getMoney()));
				MoneyDetail dailiUserMoneyDetail = new MoneyDetail();
				dailiUserMoneyDetail.setUserId(parentUser.getId());  //用户id 
				dailiUserMoneyDetail.setOrderId(null); //订单id
				dailiUserMoneyDetail.setBizSystem(parentUser.getBizSystem());
				dailiUserMoneyDetail.setUserName(parentUser.getUserName()); //用户名
				dailiUserMoneyDetail.setOperateUserName(parentUser.getUserName()); //操作人员
				dailiUserMoneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.FH)); //订单单号
				dailiUserMoneyDetail.setWinLevel(null);
				dailiUserMoneyDetail.setWinCost(null);
				dailiUserMoneyDetail.setWinCostRule(null);
				dailiUserMoneyDetail.setDetailType(EMoneyDetailType.HALF_MONTH_BOUNS.name()); 
				dailiUserMoneyDetail.setIncome(new BigDecimal(0)); 
				dailiUserMoneyDetail.setPay(bonusOrder.getMoney()); 
				dailiUserMoneyDetail.setBalanceBefore(parentUser.getMoney()); 
				dailiUserMoneyDetail.setBalanceAfter(parentUser.getMoney().subtract(bonusOrder.getMoney()));  //支出后的金额
				dailiUserMoneyDetail.setDescription("契约分红支出收入:" + bonusOrder.getMoney());
				moneyDetailService.insertSelective(dailiUserMoneyDetail); //保存资金明细
				parentUser.reduceMoney(bonusOrder.getMoney(), true);
				userService.updateByPrimaryKeyWithVersionSelective(parentUser);
			
				 //针对盈亏报表多线程不会事物回滚处理方式
				UserTotalMoneyByTypeVo userTotalMoneyByTypeVo1 =  new UserTotalMoneyByTypeVo(user, EMoneyDetailType.HALF_MONTH_BOUNS, bonusOrder.getMoney());
				userTotalMoneyByTypeVos.add(userTotalMoneyByTypeVo1);
				//当前用分红，余额增加
			    //userMoneyTotalService.addUserTotalMoneyByType(user, EMoneyDetailType.HALF_MONTH_BOUNS, bonusOrder.getMoney());
				dailiUserMoneyDetail = new MoneyDetail();
				dailiUserMoneyDetail.setUserId(user.getId());  //用户id 
				dailiUserMoneyDetail.setOrderId(null); //订单id
				dailiUserMoneyDetail.setBizSystem(user.getBizSystem());
				dailiUserMoneyDetail.setUserName(user.getUserName()); //用户名
				dailiUserMoneyDetail.setOperateUserName(parentUser.getUserName()); //操作人员
				dailiUserMoneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.FH)); //订单单号
				dailiUserMoneyDetail.setWinLevel(null);
				dailiUserMoneyDetail.setWinCost(null);
				dailiUserMoneyDetail.setWinCostRule(null);
				dailiUserMoneyDetail.setDetailType(EMoneyDetailType.HALF_MONTH_BOUNS.name()); 
				dailiUserMoneyDetail.setIncome(bonusOrder.getMoney()); 
				dailiUserMoneyDetail.setPay(new BigDecimal(0)); 
				dailiUserMoneyDetail.setBalanceBefore(user.getMoney()); 
				dailiUserMoneyDetail.setBalanceAfter(user.getMoney().add(bonusOrder.getMoney()));  //支出后的金额
				dailiUserMoneyDetail.setDescription("契约分红收入:" + bonusOrder.getMoney());
				moneyDetailService.insertSelective(dailiUserMoneyDetail); //保存资金明细
				user.addMoney(bonusOrder.getMoney(), true);
				userService.updateByPrimaryKeyWithVersionSelective(user);
	    	}
	    	bonusOrderMapper.updateByPrimaryKeySelective(bonusOrder);
	    	 //循环遍历插入报表总额和盈亏报表
	        for(UserTotalMoneyByTypeVo userTotalMoneyByTypeVo:userTotalMoneyByTypeVos){
	        	userMoneyTotalService.addUserTotalMoneyByType(userTotalMoneyByTypeVo.getUser(), userTotalMoneyByTypeVo.getDetailType(), userTotalMoneyByTypeVo.getOperationValue());
	        }
	        
	        //发送系统消息
	        Notes notes = new Notes();
	        notes.setParentId(0l);
	        notes.setEnabled(1);
	        notes.setToUserId(user.getId());
	        notes.setToUserName(user.getUserName());
	        notes.setBizSystem(user.getBizSystem());
	        notes.setFromUserName(ConstantUtil.SYSTEM_OPERATE_NAME);
	        notes.setSub("[契约分红发放通知]");
	        notes.setBody("您的上级发放了归属日期["+ bonusOrder.getStartDateStr() +"-"+ bonusOrder.getEndDateStr() +"]的契约分红，金额为："+ bonusOrder.getMoney()); 
	        notes.setType(ENoteType.SYSTEM.name()); 
	        notes.setStatus(ENoteStatus.NO_READ.name());
	        notes.setCreatedDate(new Date());
	        notesService.insertSelective(notes);
    	} catch (UnEqualVersionException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		}
    	return 1;
    	
    	
    }
    
    
    /**
     * 后台 发放分红
     * @param query
     * @throws Exception 
     */
    @Transactional(readOnly=false,rollbackFor=Exception.class)
    public int managerReleaseBounsMoney(BonusOrder query,Admin operateAdmin) throws Exception{
    	
    	BonusOrder bonusOrder=bonusOrderMapper.selectByPrimaryKey(query.getId());
    	//属于已经发放完成了，或者不需分红的返回0
    	if(bonusOrder==null||"RELEASED".equals(bonusOrder.getReleaseStatus())||"NEEDNOT_RELEASE".equals(bonusOrder.getReleaseStatus())
    			||"OUTTIME_RELEASED".equals(bonusOrder.getReleaseStatus())||"FORCE_RELEASED".equals(bonusOrder.getReleaseStatus())){
    		return 0;
    	}
    	log.info("管理员[{}]处理发放分红记录id[{}],业务系统[{}],发放用户[{}],用户名[{}],应发分红[{}]", operateAdmin.getUsername(), 
    			bonusOrder.getId(), bonusOrder.getBizSystem(), bonusOrder.getFromUserName(), bonusOrder.getUserName(), bonusOrder.getMoney());
    	bonusOrder.setReleaseDate(new Date());
    	List<UserTotalMoneyByTypeVo> userTotalMoneyByTypeVos = new ArrayList<UserTotalMoneyByTypeVo>();
    	try {
	    	User parentUser=userService.selectByPrimaryKey(bonusOrder.getFromUserId());
	    	User user=userService.selectByPrimaryKey(bonusOrder.getUserId());
	    	//如果上级用户是超级代理
	    	if(parentUser.getDailiLevel().equals(EUserDailLiLevel.SUPERAGENCY.name())){
		    		//直属的设置直接发放
		    		bonusOrder.setReleaseStatus(EBonusOrderStatus.RELEASED.name());
	    		  //针对盈亏报表多线程不会事物回滚处理方式
	    		    UserTotalMoneyByTypeVo userTotalMoneyByTypeVo =  new UserTotalMoneyByTypeVo(user, EMoneyDetailType.HALF_MONTH_BOUNS, bonusOrder.getMoney());
	    		    userTotalMoneyByTypeVos.add(userTotalMoneyByTypeVo);
	    		   //userMoneyTotalService.addUserTotalMoneyByType(user, EMoneyDetailType.HALF_MONTH_BOUNS, bonusOrder.getMoney());
					Integer enabled = user.getIsTourist() == 1?0:1;
	    		    MoneyDetail dailiUserMoneyDetail = new MoneyDetail();
	    		    dailiUserMoneyDetail.setEnabled(enabled);
					dailiUserMoneyDetail.setUserId(user.getId());  //用户id 
					dailiUserMoneyDetail.setOrderId(null); //订单id
					dailiUserMoneyDetail.setBizSystem(user.getBizSystem());
					dailiUserMoneyDetail.setUserName(user.getUserName()); //用户名
					dailiUserMoneyDetail.setOperateUserName(operateAdmin.getUsername()); //操作人员
					dailiUserMoneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.FH)); //订单单号
					dailiUserMoneyDetail.setWinLevel(null);
					dailiUserMoneyDetail.setWinCost(null);
					dailiUserMoneyDetail.setWinCostRule(null);
					dailiUserMoneyDetail.setDetailType(EMoneyDetailType.HALF_MONTH_BOUNS.name()); 
					dailiUserMoneyDetail.setIncome(bonusOrder.getMoney()); 
					dailiUserMoneyDetail.setPay(new BigDecimal(0)); 
					dailiUserMoneyDetail.setBalanceBefore(user.getMoney()); 
					dailiUserMoneyDetail.setBalanceAfter(user.getMoney().add(bonusOrder.getMoney()));  //支出后的金额
					dailiUserMoneyDetail.setDescription("契约分红收入:" + bonusOrder.getMoney());
					moneyDetailService.insertSelective(dailiUserMoneyDetail); //保存资金明细
					user.addMoney(bonusOrder.getMoney(), true);
					userService.updateByPrimaryKeyWithVersionSelective(user);
	    		
	    	}else{
	    	
		    	if(parentUser.getMoney().compareTo(bonusOrder.getMoney())<0){
		    		throw new Exception("账号余额不足！请先充值！");
		    	}
		    	
		    	//非直属的设置直接发放
	    		bonusOrder.setReleaseStatus(EBonusOrderStatus.FORCE_RELEASED.name());
	    		//针对盈亏报表多线程不会事物回滚处理方式
	    		UserTotalMoneyByTypeVo userTotalMoneyByTypeVo =  new UserTotalMoneyByTypeVo(parentUser, EMoneyDetailType.HALF_MONTH_BOUNS, BigDecimal.ZERO.subtract(bonusOrder.getMoney()));
	    		userTotalMoneyByTypeVos.add(userTotalMoneyByTypeVo);
			    //当前用分红，父级余额扣减
				//userMoneyTotalService.addUserTotalMoneyByType(parentUser, EMoneyDetailType.HALF_MONTH_BOUNS, BigDecimal.ZERO.subtract(bonusOrder.getMoney()));
				Integer enabled = parentUser.getIsTourist() == 1?0:1;
				MoneyDetail dailiUserMoneyDetail = new MoneyDetail();
				dailiUserMoneyDetail.setEnabled(enabled);
				dailiUserMoneyDetail.setUserId(parentUser.getId());  //用户id 
				dailiUserMoneyDetail.setOrderId(null); //订单id
				dailiUserMoneyDetail.setBizSystem(parentUser.getBizSystem());
				dailiUserMoneyDetail.setUserName(parentUser.getUserName()); //用户名
				dailiUserMoneyDetail.setOperateUserName(operateAdmin.getUsername()); //操作人员
				dailiUserMoneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.FH)); //订单单号
				dailiUserMoneyDetail.setWinLevel(null);
				dailiUserMoneyDetail.setWinCost(null);
				dailiUserMoneyDetail.setWinCostRule(null);
				dailiUserMoneyDetail.setDetailType(EMoneyDetailType.HALF_MONTH_BOUNS.name()); 
				dailiUserMoneyDetail.setIncome(new BigDecimal(0)); 
				dailiUserMoneyDetail.setPay(bonusOrder.getMoney()); 
				dailiUserMoneyDetail.setBalanceBefore(parentUser.getMoney()); 
				dailiUserMoneyDetail.setBalanceAfter(parentUser.getMoney().subtract(bonusOrder.getMoney()));  //支出后的金额
				dailiUserMoneyDetail.setDescription("契约分红支出:" + bonusOrder.getMoney());
				moneyDetailService.insertSelective(dailiUserMoneyDetail); //保存资金明细
				parentUser.reduceMoney(bonusOrder.getMoney(), true);
				userService.updateByPrimaryKeyWithVersionSelective(parentUser);
			
				//针对盈亏报表多线程不会事物回滚处理方式
				UserTotalMoneyByTypeVo userTotalMoneyByTypeVo1 =  new UserTotalMoneyByTypeVo(user, EMoneyDetailType.HALF_MONTH_BOUNS, bonusOrder.getMoney());
				userTotalMoneyByTypeVos.add(userTotalMoneyByTypeVo1);
				//当前用分红，余额增加
			    //userMoneyTotalService.addUserTotalMoneyByType(user, EMoneyDetailType.HALF_MONTH_BOUNS, bonusOrder.getMoney());
			    enabled = user.getIsTourist() == 1?0:1;
			    dailiUserMoneyDetail = new MoneyDetail();
			    dailiUserMoneyDetail.setEnabled(enabled);
				dailiUserMoneyDetail.setUserId(user.getId());  //用户id 
				dailiUserMoneyDetail.setOrderId(null); //订单id
				dailiUserMoneyDetail.setBizSystem(user.getBizSystem());
				dailiUserMoneyDetail.setUserName(user.getUserName()); //用户名
				dailiUserMoneyDetail.setOperateUserName(operateAdmin.getUsername()); //操作人员
				dailiUserMoneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.FH)); //订单单号
				dailiUserMoneyDetail.setWinLevel(null);
				dailiUserMoneyDetail.setWinCost(null);
				dailiUserMoneyDetail.setWinCostRule(null);
				dailiUserMoneyDetail.setDetailType(EMoneyDetailType.HALF_MONTH_BOUNS.name()); 
				dailiUserMoneyDetail.setIncome(bonusOrder.getMoney()); 
				dailiUserMoneyDetail.setPay(new BigDecimal(0)); 
				dailiUserMoneyDetail.setBalanceBefore(user.getMoney()); 
				dailiUserMoneyDetail.setBalanceAfter(user.getMoney().add(bonusOrder.getMoney()));  //支出后的金额
				dailiUserMoneyDetail.setDescription("契约分红收入:" + bonusOrder.getMoney());
				moneyDetailService.insertSelective(dailiUserMoneyDetail); //保存资金明细
				user.addMoney(bonusOrder.getMoney(), true);
				userService.updateByPrimaryKeyWithVersionSelective(user);
	    	}
	    	bonusOrderMapper.updateByPrimaryKeySelective(bonusOrder);

	        //循环遍历插入报表总额和盈亏报表
	        for(UserTotalMoneyByTypeVo userTotalMoneyByTypeVo:userTotalMoneyByTypeVos){
	        	userMoneyTotalService.addUserTotalMoneyByType(userTotalMoneyByTypeVo.getUser(), userTotalMoneyByTypeVo.getDetailType(), userTotalMoneyByTypeVo.getOperationValue());
	        }
	        
	        //发送系统消息
	        Notes notes = new Notes();
	        notes.setParentId(0l);
	        notes.setEnabled(1);
	        notes.setToUserId(user.getId());
	        notes.setToUserName(user.getUserName());
	        notes.setBizSystem(user.getBizSystem());
	        notes.setFromUserName(ConstantUtil.SYSTEM_OPERATE_NAME);
	        notes.setSub("[契约分红发放通知]");
	        notes.setBody("您的上级发放了归属日期["+ bonusOrder.getStartDateStr() +"-"+ bonusOrder.getEndDateStr() +"]的契约分红，金额为："+ bonusOrder.getMoney()); 
	        notes.setType(ENoteType.SYSTEM.name()); 
	        notes.setStatus(ENoteStatus.NO_READ.name());
	        notes.setCreatedDate(new Date());
	        notesService.insertSelective(notes);
    	} catch (UnEqualVersionException e) {
    		log.error("发放分红出现版本号不一致", e);
			throw e;
		} catch (Exception e) {
			log.error("发放分红出现异常", e);
			throw e;
		}
    	return 1;
    }
    
    /**
     * 查询某段时间内平台发放的分红总额
     * @param startTime
     * @param endTime
     * @return
     */
    public BigDecimal queryBonusOrderTotalMoney(String bizSystem, String fromUserName, Date startTime, Date endTime) {
    	BigDecimal totalMoney = BigDecimal.ZERO;
    	BigDecimal queryTotalMoney = bonusOrderMapper.queryBonusOrderTotalMoney(bizSystem, fromUserName, startTime, endTime);
    	if(queryTotalMoney != null) {
    		totalMoney = queryTotalMoney;
    	}
    	return totalMoney;
    }
}
