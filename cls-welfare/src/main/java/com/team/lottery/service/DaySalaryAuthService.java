package com.team.lottery.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.mapper.daysalaryauth.DaySalaryAuthMapper;
import com.team.lottery.vo.DaySalaryAuth;

@Service
public class DaySalaryAuthService {

	@Autowired
	private DaySalaryAuthMapper daySalaryAuthMapper;
	
	public int deleteByPrimaryKey(Integer id) {
		return daySalaryAuthMapper.deleteByPrimaryKey(id);
	}

    public int insertSelective(DaySalaryAuth record) {
    	return daySalaryAuthMapper.insertSelective(record);
    }

    public DaySalaryAuth selectByPrimaryKey(Integer id) {
    	return daySalaryAuthMapper.selectByPrimaryKey(id);
    }

    public int updateByPrimaryKeySelective(DaySalaryAuth record) {
    	return daySalaryAuthMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * 根据用户id查找记录
     * @param userId
     * @return
     */
    public DaySalaryAuth queryByUserId(Integer userId) {
    	return daySalaryAuthMapper.queryByUserId(userId);
    }
}
