package com.team.lottery.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.enums.ENoteStatus;
import com.team.lottery.enums.ENoteType;
import com.team.lottery.extvo.DaySalaryConfigVo;
import com.team.lottery.mapper.daysalaryconfig.DaySalaryConfigMapper;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.vo.DaySalaryAuth;
import com.team.lottery.vo.DaySalaryConfig;
import com.team.lottery.vo.Notes;
import com.team.lottery.vo.User;

@Service("daySalaryConfigService")
public class DaySalaryConfigService {

	@Autowired
	private DaySalaryConfigMapper daySalaryConfigMapper;
	@Autowired
	private UserService userService;
	@Autowired
	private NotesService notesService;
	@Autowired
	private DaySalaryAuthService daySalaryAuthService;
	
	public int deleteByPrimaryKey(Long id) {
		return daySalaryConfigMapper.deleteByPrimaryKey(id);
	}
	
	public int insertSelective(DaySalaryConfig record){
    	return daySalaryConfigMapper.insertSelective(record);
    }

    public DaySalaryConfig selectByPrimaryKey(Long id){
    	return daySalaryConfigMapper.selectByPrimaryKey(id);
    }
    
    public int updateByPrimaryKeySelective(DaySalaryConfig record){
    	return daySalaryConfigMapper.updateByPrimaryKeySelective(record);
    }
	
	public List<DaySalaryConfig> getAllDaySalaryConfig(DaySalaryConfigVo daySalaryConfigVo) {
		return daySalaryConfigMapper.getAllDaySalaryConfig(daySalaryConfigVo);
	}
	
	public DaySalaryConfig selectByPrimaryKeyForUpdate(Long id){
    	return daySalaryConfigMapper.selectByPrimaryKeyForUpdate(id);
    }
	
	/**
	 * 保存日工资设定处理
	 */
	@Transactional(readOnly=false,rollbackFor=Exception.class)
	public void saveOrUpdateDaySalaryConfig(List<DaySalaryConfig> daySalaryConfigList, String type, User user, User currentUser) {
		DaySalaryConfigVo query = new DaySalaryConfigVo();
		query.setUserId(user.getId());
		List<DaySalaryConfig> rechargeConfig = this.getAllDaySalaryConfig(query);
		//如果更换设定类型则删除掉原来的记录
		for (DaySalaryConfig config : rechargeConfig) {
			if (!type.equals(config.getType())) {
				this.deleteByPrimaryKey(config.getId());
			}
		}
		for (int i = 0; i < daySalaryConfigList.size(); i++) {
			DaySalaryConfig daySalaryConfig = daySalaryConfigList.get(i);
			Long id = daySalaryConfig.getId();

			daySalaryConfig.setBizSystem(user.getBizSystem());
			daySalaryConfig.setUserName(user.getUserName());
			daySalaryConfig.setUserId(user.getId());

			if (id == null) {
				daySalaryConfig.setCreateTime(new Date());
				daySalaryConfig.setCreateUserId(currentUser.getId());
				daySalaryConfig.setCreateUserName(currentUser.getUserName());
				daySalaryConfig.setState(1);
				this.insertSelective(daySalaryConfig);
			} else {
				daySalaryConfig.setUpdateTime(new Date());
				// daySalaryConfig.setUpdateAdmin(currentAdmin.getUsername());
				this.updateByPrimaryKeySelective(daySalaryConfig);
			}
		}
		//更新用户为启用日工资状态
		if (user != null) {
			User updateUser = new User();
			updateUser.setId(user.getId());
			updateUser.setSalaryState(1);
			userService.updateByPrimaryKeySelective(updateUser);
		}
		
		//发送站内信通知下级用户有工资设定
		Notes notes = new Notes();
        notes.setParentId(0l);
        notes.setEnabled(1);
        notes.setBizSystem(user.getBizSystem());
        notes.setFromUserName(ConstantUtil.SYSTEM_OPERATE_NAME);
        notes.setFromUserId(null);
        notes.setToUserName(user.getUserName());
        notes.setToUserId(user.getId());
        notes.setSub("[日工资设定通知]");
        notes.setBody("您的上级给您设定了日工资，请到代理中心-我的日工资查看"); 
        notes.setType(ENoteType.SYSTEM.name()); 
        notes.setStatus(ENoteStatus.NO_READ.name());
        notes.setCreatedDate(new Date());
        notesService.insertSelective(notes); 
	}
	
	/**
     * 启用或停用日工资设定
     * @param id
     */
	@Transactional(readOnly=false,rollbackFor=Exception.class)
    public void updateDaySalaryConfigEnabled(Integer userId,Integer enabled){
		DaySalaryConfigVo query = new DaySalaryConfigVo();
		query.setUserId(userId);
		List<DaySalaryConfig> daySalaryConfigs = this.getAllDaySalaryConfig(query);
		for(DaySalaryConfig daySalaryConfig : daySalaryConfigs) {
			daySalaryConfig.setState(enabled);
			this.updateByPrimaryKeySelective(daySalaryConfig);
		}
		
		User user = userService.selectByPrimaryKey(userId);
		//发送站内信通知下级用户日工资变动
		Notes notes = new Notes();
        notes.setParentId(0l);
        notes.setEnabled(1);
        notes.setBizSystem(user.getBizSystem());
        notes.setFromUserName(ConstantUtil.SYSTEM_OPERATE_NAME);
        notes.setFromUserId(null);
        notes.setToUserName(user.getUserName());
        notes.setToUserId(user.getId());
        notes.setSub("[日工资设定变更通知]");
        String operStr = "";
        if(enabled == 1) {
        	operStr = "启用";
        } else {
        	operStr = "停用";
        }
        notes.setBody("您的上级"+ operStr +"了您的日工资，请到代理中心-我的日工资查看"); 
        notes.setType(ENoteType.SYSTEM.name()); 
        notes.setStatus(ENoteStatus.NO_READ.name());
        notes.setCreatedDate(new Date());
        notesService.insertSelective(notes); 
    }
	
	/**
     * 撤销日工资设定
     * @param id
     */
	@Transactional(readOnly=false,rollbackFor=Exception.class)
    public void cancelDaySalaryConfig(User user){
		DaySalaryConfigVo query = new DaySalaryConfigVo();
		query.setUserId(user.getId());
		List<DaySalaryConfig> daySalaryConfigs = this.getAllDaySalaryConfig(query);
		for(DaySalaryConfig daySalaryConfig : daySalaryConfigs) {
			this.deleteByPrimaryKey(daySalaryConfig.getId());
		}
		
		//更新用户日工资状态
		User updateUser = new User();
		updateUser.setId(user.getId());
		updateUser.setSalaryState(0);
		userService.updateByPrimaryKeySelective(updateUser);
		
		//发送站内信通知下级用户日工资变动
		Notes notes = new Notes();
        notes.setParentId(0l);
        notes.setEnabled(1);
        notes.setBizSystem(user.getBizSystem());
        notes.setFromUserName(ConstantUtil.SYSTEM_OPERATE_NAME);
        notes.setFromUserId(null);
        notes.setToUserName(user.getUserName());
        notes.setToUserId(user.getId());
        notes.setSub("[日工资设定变更通知]");
        notes.setBody("您的上级撤销了您的日工资设定"); 
        notes.setType(ENoteType.SYSTEM.name()); 
        notes.setStatus(ENoteStatus.NO_READ.name());
        notes.setCreatedDate(new Date());
        notesService.insertSelective(notes); 
		
		//判断是否有授权下级设定日工资权限，有则删除该用户权限
        DaySalaryAuth daySalaryAuth = daySalaryAuthService.queryByUserId(user.getId());
        if(daySalaryAuth != null) {
        	daySalaryAuthService.deleteByPrimaryKey(daySalaryAuth.getId());
        }
    }
	
}
