package com.team.lottery.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.dayconsumeactityconfig.DayConsumeActityConfigMapper;
import com.team.lottery.vo.DayConsumeActityConfig;

@Service
public class DayConsumeActityConfigService {
	
	@Autowired
	private DayConsumeActityConfigMapper dayConsumeActityConfigMapper;
	
	public int deleteByPrimaryKey(Long id){
		return dayConsumeActityConfigMapper.deleteByPrimaryKey(id);
	}

	public int insert(DayConsumeActityConfig record){
		return dayConsumeActityConfigMapper.insert(record);
	}

	public int insertSelective(DayConsumeActityConfig record){
		return dayConsumeActityConfigMapper.insertSelective(record);
	}

	public DayConsumeActityConfig selectByPrimaryKey(Long id){
		return dayConsumeActityConfigMapper.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(DayConsumeActityConfig record){
		return dayConsumeActityConfigMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(DayConsumeActityConfig record){
		return dayConsumeActityConfigMapper.updateByPrimaryKey(record);
	}
	
	
	public Page getDayConsumeActityConfigsPage(DayConsumeActityConfig query,Page page){
		page.setTotalRecNum(dayConsumeActityConfigMapper.getDayConsumeActityConfigsCount(query));
		page.setPageContent(dayConsumeActityConfigMapper.getDayConsumeActityConfigsPage(query, page.getStartIndex(), page.getPageSize()));
		return page;
	}
	
	public List<DayConsumeActityConfig> getDayConsumeActityConfigs(String bizSystem){
		return dayConsumeActityConfigMapper.getDayConsumeActityConfigs(bizSystem);
	}
	public List<DayConsumeActityConfig> getDayConsumeActityConfigsQuery(DayConsumeActityConfig query){
		return dayConsumeActityConfigMapper.getDayConsumeActityConfigsQuery(query);
	}
}
