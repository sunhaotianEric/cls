package com.team.lottery.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.mapper.bonusauth.BonusAuthMapper;
import com.team.lottery.vo.BonusAuth;

@Service
public class BonusAuthService {

	@Autowired
	private BonusAuthMapper bonusAuthMapper;
	
	public int deleteByPrimaryKey(Integer id) {
		return bonusAuthMapper.deleteByPrimaryKey(id);
	}

    public int insertSelective(BonusAuth record) {
    	return bonusAuthMapper.insertSelective(record);
    }

    public BonusAuth selectByPrimaryKey(Integer id) {
    	return bonusAuthMapper.selectByPrimaryKey(id);
    }

    public int updateByPrimaryKeySelective(BonusAuth record) {
    	return bonusAuthMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * 根据用户id查找记录
     * @param userId
     * @return
     */
    public BonusAuth queryByUserId(Integer userId) {
    	return bonusAuthMapper.queryByUserId(userId);
    }
}
