package com.team.lottery.mapper.userbonusconfig;

import java.util.List;

import com.team.lottery.vo.UserBonusConfig;


public interface UserBonusConfigMapper {
    int deleteByPrimaryKey(Long id);

    int insert(UserBonusConfig record);

    int insertSelective(UserBonusConfig record);

    UserBonusConfig selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UserBonusConfig record);

    int updateByPrimaryKey(UserBonusConfig record);
    
    /**
     * 查询全部的分红配置
     * @return
     */
    public List<UserBonusConfig> getAllUserBounsConfigs();
}