package com.team.lottery.mapper.dayconsumeactityorder;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.DayConsumeActityOrderQuery;
import com.team.lottery.vo.DayConsumeActityOrder;

public interface DayConsumeActityOrderMapper {
    int deleteByPrimaryKey(Long id);

    int insert(DayConsumeActityOrder record);

    int insertSelective(DayConsumeActityOrder record);

    DayConsumeActityOrder selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(DayConsumeActityOrder record);

    int updateByPrimaryKey(DayConsumeActityOrder record);
    
    Integer getDayConsumeActivitysCount(@Param("query")DayConsumeActityOrderQuery query);
    
    List<DayConsumeActityOrder> getDayConsumeActityOrderPage(@Param("query")DayConsumeActityOrderQuery query, @Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum );

    List<DayConsumeActityOrder> getDayConsumeActityOrderByCondition(DayConsumeActityOrderQuery query);
    
    List<DayConsumeActityOrder> getDayConsumeActityOrderBybelongDate(@Param("query")DayConsumeActityOrderQuery query);
    
    /**
     * 批量插入
     * @param list
     */
    public void insertBatch(List<DayConsumeActityOrder> list);
}