package com.team.lottery.mapper.daysalaryauth;

import com.team.lottery.vo.DaySalaryAuth;

public interface DaySalaryAuthMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(DaySalaryAuth record);

    int insertSelective(DaySalaryAuth record);

    DaySalaryAuth selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(DaySalaryAuth record);

    int updateByPrimaryKey(DaySalaryAuth record);
    
    /**
     * 根据用户id查找记录
     * @param userId
     * @return
     */
    DaySalaryAuth queryByUserId(Integer userId);
}