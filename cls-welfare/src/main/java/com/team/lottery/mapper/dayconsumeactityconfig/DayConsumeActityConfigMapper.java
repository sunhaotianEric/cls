package com.team.lottery.mapper.dayconsumeactityconfig;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.DayConsumeActityConfig;

public interface DayConsumeActityConfigMapper {
    int deleteByPrimaryKey(Long id);

    int insert(DayConsumeActityConfig record);

    int insertSelective(DayConsumeActityConfig record);

    DayConsumeActityConfig selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(DayConsumeActityConfig record);

    int updateByPrimaryKey(DayConsumeActityConfig record);
    
    Integer getDayConsumeActityConfigsCount(@Param("query")DayConsumeActityConfig query);
    
    List<DayConsumeActityConfig> getDayConsumeActityConfigsPage(@Param("query")DayConsumeActityConfig query, @Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
    
    List<DayConsumeActityConfig> getDayConsumeActityConfigs(String bizSystem);
    
    List<DayConsumeActityConfig> getDayConsumeActityConfigsQuery(DayConsumeActityConfig query);
}