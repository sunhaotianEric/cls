package com.team.lottery.mapper.daysalaryconfig;

import java.util.List;

import com.team.lottery.extvo.DaySalaryConfigVo;
import com.team.lottery.vo.DaySalaryConfig;

public interface DaySalaryConfigMapper {
    int deleteByPrimaryKey(Long id);

    int insert(DaySalaryConfig record);

    int insertSelective(DaySalaryConfig record);

    DaySalaryConfig selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(DaySalaryConfig record);

    int updateByPrimaryKey(DaySalaryConfig record);
    
    DaySalaryConfig selectByPrimaryKeyForUpdate(Long id);
    
    List<DaySalaryConfig> getAllDaySalaryConfig(DaySalaryConfigVo daySalaryConfigVo);
}