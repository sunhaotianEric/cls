package com.team.lottery.mapper.signinconfig;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.BizSystemInfo;
import com.team.lottery.vo.SigninConfig;

public interface SigninConfigMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SigninConfig record);

    int insertSelective(SigninConfig record);

    SigninConfig selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SigninConfig record);

    int updateByPrimaryKey(SigninConfig record);
    
    /**
     * 查询符合条件的记录条数
     * @param query
     * @return
     */
    public Integer getAllSigninConfigByQueryPageCount(@Param("query")SigninConfig query);
        
    /**
     * 分页条件
     * @return
     */
    public  List<SigninConfig> getAllSigninConfigByQueryPage(@Param("query")SigninConfig query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
    
    /**
     * 查询符合条件的签到设置
     * @param query
     * @return
     */
    public  List<SigninConfig> selectByQuery(SigninConfig query);

}