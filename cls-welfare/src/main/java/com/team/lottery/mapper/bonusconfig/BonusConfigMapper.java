package com.team.lottery.mapper.bonusconfig;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.BonusConfig;

public interface BonusConfigMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BonusConfig record);

    int insertSelective(BonusConfig record);

    BonusConfig selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BonusConfig record);

    int updateByPrimaryKey(BonusConfig record);
    
    /**
     * 查询符合条件的记录条数
     * @param query
     * @return
     */
    public Integer getBonusConfigByQueryPageCount(@Param("query")BonusConfig query);
    
    /**
     * 分页条件查询
     * @return
     */
    public List<BonusConfig> getBonusConfigByQueryPage(@Param("query")BonusConfig query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
    
    
    /**
     * 查询符合条件的契约记录
     * @param query
     * @return
     */
    public List<BonusConfig> getBonusConfigByQuery(@Param("query")BonusConfig query);
    
    /**
     * 根据userId来删除,只能删除类型NEW的契约
     * @param userId
     * @return
     */
    public int deleteNewByUserId(@Param("userId")Integer userId);
    
    /**
     * 根据userId来删除
     * @param userId
     * @return
     */
    public int deleteCurrentByUserId(@Param("userId")Integer userId);
    
    /**
     * 查找最大的分红比例
     * @param query
     * @return
     */
    public int getMaxValueBonusConfigByQuery(@Param("query")BonusConfig query);
    /**
     * 查找最小的分红比例
     * @param query
     * @return
     */
    public BonusConfig getMinValueBonusConfigByQuery(@Param("query")BonusConfig query);

    /**
     * 根据用户名查询契约配置,去重
     * @param userName
     * @return
     */
	public List<BonusConfig> getCurrentUserBonusesByUserName(@Param("createUserId")Integer createUserId);
    
}