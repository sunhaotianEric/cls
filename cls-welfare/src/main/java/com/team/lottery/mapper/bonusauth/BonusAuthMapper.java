package com.team.lottery.mapper.bonusauth;

import com.team.lottery.vo.BonusAuth;

public interface BonusAuthMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(BonusAuth record);

    int insertSelective(BonusAuth record);

    BonusAuth selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(BonusAuth record);

    int updateByPrimaryKey(BonusAuth record);
    
    /**
     * 根据用户id查找记录
     * @param userId
     * @return
     */
    BonusAuth queryByUserId(Integer userId);
}