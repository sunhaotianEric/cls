package com.team.lottery.mapper.bonusorder;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.BonusOrder;

public interface BonusOrderMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BonusOrder record);

    int insertSelective(BonusOrder record);

    BonusOrder selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BonusOrder record);

    int updateByPrimaryKey(BonusOrder record);
    /**
     * 查询符合条件记录
     * @param query
     * @return
     */
    public List<BonusOrder> getBonusOrderByQuery(@Param("query")BonusOrder query);
    
    /**
     * 查询统计某个用户下级所要 发放的总金额-
     * @param query
     * @return
     */
    public BonusOrder getALlMoneyByQuery(@Param("query")BonusOrder query);
    
    
    /**
     * 查询符合条件的记录条数
     * @param query
     * @return
     */
    public Integer getBonusOrderByQueryPageCount(@Param("query")BonusOrder query);
    
    /**
     * 分页条件查询
     * @return
     */
    public List<BonusOrder> getBonusOrderByQueryPage(@Param("query")BonusOrder query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
   
    
    /**
     * 查询符合条件的记录条数
     * @param query
     * @return
     */
    public Integer getMgrBonusOrderByQueryPageCount(@Param("query")BonusOrder query);
    
    
    /**
     * 后台分页条件查询
     * @return
     */
    public List<BonusOrder> getMgrBonusOrderByQueryPage(@Param("query")BonusOrder query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
    
    /**
     * 查询某段时间内发放的分红总额
     * @param startTime
     * @param endTime
     * @return
     */
    public BigDecimal queryBonusOrderTotalMoney(@Param("bizSystem")String bizSystem, @Param("fromUserName")String fromUserName, @Param("startTime")Date startTime, @Param("endTime")Date endTime);
}