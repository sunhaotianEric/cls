package com.team.lottery.mapper.daysalaryorder;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.SalaryOrderQuery;
import com.team.lottery.vo.DaySalaryOrder;

public interface DaySalaryOrderMapper {
    int deleteByPrimaryKey(Long id);

    int insert(DaySalaryOrder record);

    int insertSelective(DaySalaryOrder record);

    DaySalaryOrder selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(DaySalaryOrder record);

    int updateByPrimaryKey(DaySalaryOrder record);
    
    /**
     * 根据条件查询符合条件的记录
     * @param salaryOrderQuery
     * @return
     */
    List<DaySalaryOrder> querySalaryOrdersByCondition(@Param("query")SalaryOrderQuery salaryOrderQuery);
    
	 /**
     * 
     * @param query
     * @return
     */
    public Integer getDaySalaryOrderByQueryPageCount(SalaryOrderQuery query);
    
    
    /**
     * 
     * @param query
     * @return
     */
    public List<DaySalaryOrder> getDaySalaryOrderByQueryPage(@Param("query")SalaryOrderQuery query, @Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
    
    /**
     * 查询某段时间内发放的日工资总额
     * @param startTime
     * @param endTime
     * @return
     */
    public BigDecimal queryDaySalaryOrderTotalMoney(@Param("bizSystem")String bizSystem, @Param("fromUserName")String fromUserName, @Param("startTime")Date startTime, @Param("endTime")Date endTime);
}