package com.team.lottery.mapper.lotterydisturbconfig;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.LotteryDisturbConfigQuery;
import com.team.lottery.vo.LotteryDisturbConfig;

public interface LotteryDisturbConfigMapper {
    int deleteByPrimaryKey(Long id);

    int insert(LotteryDisturbConfig record);

    int insertSelective(LotteryDisturbConfig record);

    LotteryDisturbConfig selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(LotteryDisturbConfig record);

    int updateByPrimaryKey(LotteryDisturbConfig record);
    
    /**
	 * 根据用户名查询有效的下单干扰配置项
	 * @return
	 */
	List<LotteryDisturbConfig> queryLotteryDisturbConfigsByCondition(@Param("query")LotteryDisturbConfigQuery query);
    
    /**
	 * 查询所有有效的下单干扰配置项
	 * @return
	 */
	List<LotteryDisturbConfig> queryAllLotteryDisturbConfigs();
}