package com.team.lottery.mapper.lotterywinlhc;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.LotteryWinLhc;

public interface LotteryWinLhcMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(LotteryWinLhc record);

    int insertSelective(LotteryWinLhc record);

    LotteryWinLhc selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(LotteryWinLhc record);

    int updateByPrimaryKey(LotteryWinLhc record);
    
    List<LotteryWinLhc> getLotteryWinLhcByLotteryTypeProtype(@Param("query")LotteryWinLhc query);
    
    /**
     * 获取所有业务系统六合彩彩种赔率
     * @author listgoo
     * @Description: 
     * @date: 2019年6月6日
     * @return
     */
    List<LotteryWinLhc> getLotteryWinLhc();
    
    int getLotteryWinLhcCount(@Param("query")LotteryWinLhc query);
    
    List<LotteryWinLhc> getLotteryWinLhcByPage(@Param("query")LotteryWinLhc query, @Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
   /**
    * 根据种类获取玩法赔率
    * @param query
    * @return
    */
    List<LotteryWinLhc> getLotteryWinLhcByKind(@Param("query")LotteryWinLhc query);
    
    /**
     * 根据种类获取玩法最低赔率
     * @param query
     * @return
     */
     List<LotteryWinLhc> getMinLotteryWinLhcByKind(@Param("query")LotteryWinLhc query,@Param("codeArr")String[] codeArr);
    
    /**
     * 复制数据，初始化化业务系统时，初始化六合彩赔率数据
     * @param query
     */
    void  initLotteryLhcCode(@Param("query")LotteryWinLhc query);
}