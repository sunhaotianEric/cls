package com.team.lottery.mapper.opencodeapi;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.OpenCodeApi;

public interface OpenCodeApiMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(OpenCodeApi record);

    int insertSelective(OpenCodeApi record);

    OpenCodeApi selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(OpenCodeApi record);

    int updateByPrimaryKey(OpenCodeApi record);
    public Integer getOpenCodeApiByQueryPageCount(@Param("query")OpenCodeApi query);
    /**
     * 分页条件
     * @return
     */
    public List<OpenCodeApi> getOpenCodeApiByQueryPage(@Param("query")OpenCodeApi query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
    

    public List<OpenCodeApi> getOpenCodeApiByLotteryKind(@Param("query")OpenCodeApi query);
    
}