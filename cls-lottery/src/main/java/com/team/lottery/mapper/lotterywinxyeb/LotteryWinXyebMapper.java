package com.team.lottery.mapper.lotterywinxyeb;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.LotteryWinXyeb;

public interface LotteryWinXyebMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(LotteryWinXyeb record);

    int insertSelective(LotteryWinXyeb record);

    LotteryWinXyeb selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(LotteryWinXyeb record);

    int updateByPrimaryKey(LotteryWinXyeb record);
    
    List<LotteryWinXyeb> getLotteryWinXyebByLotteryTypeProtype(@Param("query")LotteryWinXyeb query);
    
    int getLotteryWinXyebCount(@Param("query")LotteryWinXyeb query);
    
    List<LotteryWinXyeb> getLotteryWinXyebByPage(@Param("query")LotteryWinXyeb query, @Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
   /**
    * 根据种类获取玩法赔率
    * @param query
    * @return
    */
    List<LotteryWinXyeb> getLotteryWinXyebByKind(@Param("query")LotteryWinXyeb query);
    
    /**
     * 根据种类获取玩法最低赔率
     * @param query
     * @return
     */
     List<LotteryWinXyeb> getMinLotteryWinXyebByKind(@Param("query")LotteryWinXyeb query,@Param("codeArr")String[] codeArr);
    
    /**
     * 复制数据，初始化化业务系统时
     * @param query
     */
    void  initLotteryXyebCode(@Param("query")LotteryWinXyeb query);
}