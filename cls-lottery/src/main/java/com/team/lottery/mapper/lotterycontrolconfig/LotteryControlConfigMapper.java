package com.team.lottery.mapper.lotterycontrolconfig;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.LotteryControlConfig;

public interface LotteryControlConfigMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(LotteryControlConfig record);

    int insertSelective(LotteryControlConfig record);

    LotteryControlConfig selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(LotteryControlConfig record);

    int updateByPrimaryKey(LotteryControlConfig record);
    
    List<LotteryControlConfig> getAllLotteryControlConfig(@Param("query")LotteryControlConfig record);
    
    List<LotteryControlConfig> getAllConfigs(@Param("query")LotteryControlConfig record);
    
    LotteryControlConfig queryLotteryControlConfigBybizSystem(@Param("query")LotteryControlConfig record);
}