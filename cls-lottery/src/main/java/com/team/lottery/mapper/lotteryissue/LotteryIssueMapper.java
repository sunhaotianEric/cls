package com.team.lottery.mapper.lotteryissue;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.LotteryIssueAdjustVo;
import com.team.lottery.extvo.LotteryIssueQuery;
import com.team.lottery.vo.LotteryIssue;

public interface LotteryIssueMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(LotteryIssue record);

    int insertSelective(LotteryIssue record);

    LotteryIssue selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(LotteryIssue record);

    int updateByPrimaryKey(LotteryIssue record);
    
    /**
     * 查询当前某个彩种正在开奖的期号
     * @deprecated 改走缓存
     * @param lotteryKind
     * @param date
     * @return
     */
    public LotteryIssue getNewExpectByLotteryKind(@Param("lotteryKind")String lotteryKind,@Param("date")Date date);
    
    /**
     * 加载预售期号
     * @deprecated 改走缓存
     * @return
     */
    public LotteryIssue getPresellExpectByLotteryKind(@Param("lotteryKind")String lotteryKind);
    
    /**
     * 加载对应编号的期号
     * @deprecated 改走缓存
     * @param lotteryKind
     * @param lotteryNum
     * @return
     */
    public LotteryIssue getExpectByLotteryNum(@Param("lotteryKind")String lotteryKind,@Param("lotteryNum")String lotteryNum);    
   
    
    /**
     * 加载所有的彩种的投注期号
     * @param lotteryKind
     * @return
     */
    public List<LotteryIssue> getExpectsAllByKind(@Param("lotteryKind")String lotteryKind);
    
    /**
     * 查找某个时间段的彩种的所有期号
     * @deprecated 改走缓存
     * @param lotteryKind
     * @param date
     * @return
     */
    public List<LotteryIssue> getAllExpectsByCondition(@Param("lotteryKind")String lotteryKind,@Param("startDate")Date startDate,@Param("endDate")Date endDate);

    public Integer getAllLotteryIssuesByQueryPageCount(LotteryIssueQuery query);

    public List<LotteryIssue> getAllLotteryIssuesByQueryPage(@Param("query")LotteryIssueQuery query,
    		@Param("startNum")int startIndex, @Param("pageNum")Integer pageSize);
    /**
     * 查询彩种最高期数
     * @return
     */
    public List<LotteryIssue> getMaxLotteryNum();
    
    /**
     * 更新倒计时时间
     * @return
     */
    public int updateTimeBylotteryType(LotteryIssueAdjustVo query);
}