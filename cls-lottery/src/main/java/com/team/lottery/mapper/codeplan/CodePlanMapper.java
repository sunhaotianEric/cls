package com.team.lottery.mapper.codeplan;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.CodePlan;

public interface CodePlanMapper {
    int deleteByPrimaryKey(Long id);

    int insert(CodePlan record);

    int insertSelective(CodePlan record);

    CodePlan selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(CodePlan record);

    int updateByPrimaryKeyWithBLOBs(CodePlan record);

    int updateByPrimaryKey(CodePlan record);
    
    List<CodePlan> getAllByexpect(@Param("expect") String expect,@Param("lotteryType") String lotteryType);
    
    int updateByexpect(CodePlan record);
    
    int insertBatch(List<CodePlan> codePlans);
    
    int updateBatch(List<CodePlan> codePlans);
    
    List<CodePlan>  getByLotteryType(@Param("lotteryType") String lotteryType,@Param("playkind") String playkind,@Param("lateExpectNum") int lateExpectNum);
}