package com.team.lottery.mapper.lhcissueconfig;

import com.team.lottery.vo.LhcIssueConfig;

public interface LhcIssueConfigMapper {
    int deleteByPrimaryKey(Long id);

    int insert(LhcIssueConfig record);

    int insertSelective(LhcIssueConfig record);

    LhcIssueConfig selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(LhcIssueConfig record);

    int updateByPrimaryKey(LhcIssueConfig record);
    
    LhcIssueConfig getLhcIssueConfig(String lotteryName);
}