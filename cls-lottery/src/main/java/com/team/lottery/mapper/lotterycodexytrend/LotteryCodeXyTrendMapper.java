package com.team.lottery.mapper.lotterycodexytrend;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.ZstQueryVo;
import com.team.lottery.vo.LotteryCodeXyTrend;

public interface LotteryCodeXyTrendMapper {
    int deleteByPrimaryKey(Long id);

    int insert(LotteryCodeXyTrend record);

    int insertSelective(LotteryCodeXyTrend record);

    LotteryCodeXyTrend selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(LotteryCodeXyTrend record);

    int updateByPrimaryKey(LotteryCodeXyTrend record);
    
    /**
     * 根据彩种查询走势图对象
     * @param lotteryName
     * @return
     */
    LotteryCodeXyTrend selectByLotteryName(@Param("lotteryName") String lotteryName);
    
    /**
     * 根据走势图参数查询
     * @param zstQueryVo
     * @return
     */
    List<LotteryCodeXyTrend> getByZstQuery(ZstQueryVo zstQueryVo);
    
    /**
     * 每日任务删除走势图数据，删除7天前数据（除低频彩）
     * @return
     */
    int deleteTrendCodeBefore7Days(@Param("day") Long day);
}