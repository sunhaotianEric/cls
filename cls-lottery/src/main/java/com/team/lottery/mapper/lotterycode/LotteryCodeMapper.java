package com.team.lottery.mapper.lotterycode;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.LotteryCodeQuery;
import com.team.lottery.extvo.ZstQueryVo;
import com.team.lottery.vo.LotteryCode;

public interface LotteryCodeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(LotteryCode record);

    int insertSelective(LotteryCode record);

    LotteryCode selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(LotteryCode record);

    int updateByPrimaryKey(LotteryCode record);
    
    /**
     *  获取某个彩种最新的开奖记录
     * @return
     */
    public LotteryCode getLastLotteryCode(String lotteryKind);
    
    
    /**
     * 查找某个彩种的某个期号的开奖号码
     * @param lotteryKind
     * @param expect
     * @return
     */
    public LotteryCode getLotteryCodeByKindAndExpect(@Param("lotteryKind")String lotteryKind,@Param("expect")String expect);
    
    /**
     * 某个彩种某个时间段的所有期号
     * @param lotteryKind
     * @param createDateStart
     * @param createDateEnd
     * @return
     */
    public List<LotteryCode> getLotteryCodesByKindAndDate(@Param("lotteryKind")String lotteryKind,@Param("createDateStart")Date createDateStart,@Param("createDateEnd")Date createDateEnd);

 
    /**
     * 走势图查询展示的vo
     * @param zstQueryVo
     * @return
     */
    public List<LotteryCode> getZSTByCondition(ZstQueryVo zstQueryVo);


    /**
     * 查询规定几期号码
     * @param zstQueryVo
     * @return
     */
    public List<LotteryCode> getZSTQuery(ZstQueryVo zstQueryVo);
    
    public Integer getAllLotteryCodesByQueryPageCount(LotteryCodeQuery query);

    public List<LotteryCode> getAllLotteryCodesByQueryPage(@Param("query")LotteryCodeQuery query,
			@Param("startNum")int startIndex, @Param("pageNum")Integer pageSize);
    
    /**
     * 条件查询所有期号
     * @param query
     * @return
     */
    public List<LotteryCode> getAllLotteryCodesByQuery(@Param("query")LotteryCodeQuery query);
    
    /**
     * 查找某个彩种小于某个期号固定长度的所有期号
     * @param lotteryKind
     * @param expect
     * @return
     */
    public List<LotteryCode> getLotteryCodesLessThanId(@Param("lotteryKind")String lotteryKind,@Param("id")Integer id,@Param("querySize")Integer querySize);
    
    /**
     * 查询彩种的遗漏和冷热数据
     * @param lotteryKind  彩种
     * @param queryCodes   开奖号码拼接
     * @param expectNum    查询期数
     * @return
     */
    public List<LotteryCode> getLotteryCodesOmmitAndHotCold(@Param("lotteryKind")String lotteryKind, @Param("queryCodes")List<String> queryCodes, @Param("expectNum")Integer expectNum);
    
    /**
     * 查询彩种的遗漏和冷热数据(和值)
     * @param lotteryKind  彩种
     * @param queryCodes   开奖号码拼接
     * @param expectNum    查询期数
     * @return
     */
    public List<LotteryCode> getLotteryCodesSumOmmitAndHotCold(@Param("lotteryKind")String lotteryKind, @Param("queryCodes")List<String> queryCodes, @Param("expectNum")Integer expectNum);
    
    /**
     * 查找某个彩种已经记录的最近几条开奖号码记录
     * @param lotteryKind 彩种
     * @param recodeSize 记录条数
     * @return
     */
    public List<LotteryCode> getNearestLotteryCode(@Param("lotteryKind") String lotteryKind, @Param("recordSize") Integer recordSize);
   
    /**
     * 查找某个彩种最后开奖期号
     * @param query
     * @return
     */
    public List<LotteryCode> getYesterdayLastLotteryNum(@Param("query")LotteryCodeQuery query);
}