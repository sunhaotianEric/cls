package com.team.lottery.mapper.lotterycodexy;

import java.util.Date;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.team.lottery.extvo.LotteryCodeQuery;
import com.team.lottery.extvo.LotteryCodeXyQuery;
import com.team.lottery.extvo.ZstQueryVo;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryCodeXy;

public interface LotteryCodeXyMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(LotteryCodeXy record);

    int insertSelective(LotteryCodeXy record);

    LotteryCodeXy selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(LotteryCodeXy record);

    int updateByPrimaryKey(LotteryCodeXy record);
    
    int updateByPrimaryKeyByBizSystem(LotteryCodeXy record);
    /**
     *  获取某个彩种最新的开奖记录
     * @return
     */
    public LotteryCodeXy getLastLotteryCode(@Param("updateLastLotteryCode")String lotteryKind,@Param("bizSystem")String bizSystem);
    
    
    /**
     * 查找某个彩种的某个期号的开奖号码
     * @param lotteryKind
     * @param expect
     * @return
     */
    public LotteryCodeXy getLotteryCodeByKindAndExpect(@Param("lotteryKind")String lotteryKind,@Param("expect")String expect,@Param("bizSystem")String bizSystem);
    
    /**
     * 查找某个彩种的某个期号的开奖号码
     * @param lotteryKind
     * @param expect
     * @return
     */
    public List<LotteryCodeXy> getLotteryCodeByKindAndExpects(@Param("lotteryKind")String lotteryKind,@Param("expects")List<String> expects,@Param("bizSystem")String bizSystem);
    
    /**
     * 某个彩种某个时间段的所有期号
     * @param lotteryKind
     * @param createDateStart
     * @param createDateEnd
     * @return
     */
    public List<LotteryCode> getLotteryCodesByKindAndDate(@Param("lotteryKind")String lotteryKind,@Param("createDateStart")Date createDateStart,@Param("createDateEnd")Date createDateEnd,@Param("bizSystem")String bizSystem);

 
    /**
     * 走势图查询展示的vo
     * @param zstQueryVo
     * @return
     */
    public List<LotteryCodeXy> getZSTByCondition(@Param("query")ZstQueryVo zstQueryVo,@Param("bizSystem")String bizSystem);


    /**
     * 查询规定几期号码
     * @param zstQueryVo
     * @return
     */
    public List<LotteryCodeXy> getZSTQuery(@Param("query")ZstQueryVo zstQueryVo,@Param("bizSystem")String bizSystem);
    
    public Integer getAllLotteryCodesByQueryPageCount(@Param("query")LotteryCodeQuery query);

    public List<LotteryCodeXy> getAllLotteryCodesByQueryPage(@Param("query")LotteryCodeQuery query,
			@Param("startNum")int startIndex, @Param("pageNum")Integer pageSize);
    
    /**
     * 查找某个彩种小于某个期号固定长度的所有期号
     * @param lotteryKind
     * @param expect
     * @return
     */
    public List<LotteryCodeXy> getLotteryCodesLessThanNum(@Param("lotteryKind")String lotteryKind,@Param("lotteryNum")String lotteryNum,@Param("querySize")Integer querySize,@Param("bizSystem")String bizSystem);
    
    /**
     * 查询彩种的遗漏和冷热数据
     * @param lotteryKind  彩种
     * @param queryCodes   开奖号码拼接
     * @param expectNum    查询期数
     * @return
     */
    public List<LotteryCodeXy> getLotteryCodesOmmitAndHotCold(@Param("lotteryKind")String lotteryKind, @Param("queryCodes")List<String> queryCodes, @Param("expectNum")Integer expectNum,@Param("bizSystem")String bizSystem);
    
    /**
     * 查询彩种的遗漏和冷热数据(和值)
     * @param lotteryKind  彩种
     * @param queryCodes   开奖号码拼接
     * @param expectNum    查询期数
     * @return
     */
    public List<LotteryCodeXy> getLotteryCodesSumOmmitAndHotCold(@Param("lotteryKind")String lotteryKind, @Param("queryCodes")List<String> queryCodes, @Param("expectNum")Integer expectNum,@Param("bizSystem")String bizSystem);
    
    /**
     * 查找某个彩种已经记录的最近几条开奖号码记录
     * @param lotteryKind
     * @return
     */
	public List<LotteryCode> getNearestLotteryCode(@Param("lotteryKind")String lotteryKind,@Param("bizSystem")String bizSystem, @Param("recordSize") Integer recordSize);
    
	
	/**
	 * 查找某个彩种已经记录的最近几条开奖号码记录
	 * @param lotteryKind
	 * @return
	 */
	public List<LotteryCodeXy> getNearestLotteryCodeXy(@Param("lotteryKind")String lotteryKind,@Param("bizSystem")String bizSystem, @Param("recordSize") Integer recordSize);
	
    /**
     * 查找某个彩种最后开奖期号
     * @param query
     * @return
     */
    public List<LotteryCode> getYesterdayLastLotteryNum(@Param("query")LotteryCodeQuery query);
    
    public Integer getAllLotteryCodesXyByQueryPageCount(LotteryCodeXyQuery query);
    
    public List<LotteryCodeXy> getAllLotteryCodesXyByQueryPage(@Param("query")LotteryCodeXyQuery query,
			@Param("startNum")int startIndex, @Param("pageNum")Integer pageSize);
    
    /**
     * 条件查询所有期号
     * @param query
     * @return
     */
    public List<LotteryCodeXy> getAllLotteryCodesXyByQuery(@Param("query")LotteryCodeXyQuery query);
    

}