package com.team.lottery.mapper.lotterywin;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.LotteryWinQuery;
import com.team.lottery.vo.LotteryWin;

public interface LotteryWinMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(LotteryWin record);

    int insertSelective(LotteryWin record);

    LotteryWin selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(LotteryWin record);

    int updateByPrimaryKey(LotteryWin record);
    
    /**
     * 加载对应玩法的奖金
     * @param lotteryTypeProtype
     * @return
     */
    public List<LotteryWin> getLotteryWinByKindPlay(String lotteryTypeProtype);

    public Integer getAllLotteryWinsByQueryPageCount(LotteryWinQuery query);

    List<LotteryWin> getAllLotteryWinsByQueryPage(@Param("query")LotteryWinQuery query,
    		@Param("startNum")int startIndex, @Param("pageNum")Integer pageSize);
 
    /**
     * 模糊查找,加载彩种的奖金对照表
     * @param lotteryKindLike
     * @return
     */
    public List<LotteryWin> getLotteryWinByKindLike(String lotteryKindLike);
}