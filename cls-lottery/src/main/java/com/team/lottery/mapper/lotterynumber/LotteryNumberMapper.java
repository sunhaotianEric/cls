package com.team.lottery.mapper.lotterynumber;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.LotteryNumber;


public interface LotteryNumberMapper {
    int deleteByPrimaryKey(Long id);

    int insert(LotteryNumber record);

    int insertSelective(LotteryNumber record);

    LotteryNumber selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(LotteryNumber record);

    int updateByPrimaryKey(LotteryNumber record);
    
    /**
     * 查找某个可开奖的彩种的当前期号
     * @param lotteryKind
     * @param date
     * @return
     */
    public LotteryNumber getNewExpectByLotteryKind(@Param("lotteryKind")String lotteryKind,@Param("date")Date date);

    /**
     * 查找某个时间段的可开奖的期号
     * @param lotteryKind
     * @param date
     * @return
     */
    public List<LotteryNumber> getAllLotteryNumbersByCondition(@Param("lotteryKind")String lotteryKind,@Param("startDate")Date startDate,@Param("endDate")Date endDate);
    
}