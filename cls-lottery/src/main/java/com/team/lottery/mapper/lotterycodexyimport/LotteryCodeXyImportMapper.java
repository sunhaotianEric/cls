package com.team.lottery.mapper.lotterycodexyimport;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.LotteryCodeXyImportQuery;
import com.team.lottery.vo.LotteryCodeXyImport;

public interface LotteryCodeXyImportMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(LotteryCodeXyImport record);

    int insertSelective(LotteryCodeXyImport record);

    LotteryCodeXyImport selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(LotteryCodeXyImport record);

    int updateByPrimaryKey(LotteryCodeXyImport record);
    
    /**
     * 查询所有的幸运开奖导入对象的条数,用于做分页操作.
     * @param query
     * @return
     */
	Integer getAllLotteryCodesXyImportByQueryPageCount(@Param("query")LotteryCodeXyImportQuery query);

	/**
	 * 查询所有的幸运开奖号码导入对象,用于做数据展示.
	 * @param query
	 * @param startIndex
	 * @param pageSize
	 * @return
	 */
	List<LotteryCodeXyImport> getAllLotteryCodesXyImportByQueryPage(@Param("query")LotteryCodeXyImportQuery query, @Param("startNum")int startIndex, @Param("pageNum")Integer pageSize);
	
	/**
	 * 开奖处理时，需要查询的某区间期号的数据
	 * @param lotteryType
	 * @param minIssueNo
	 * @param maxIssueNo
	 * @return
	 */
	List<LotteryCodeXyImport> getByLotteryTypeAndNumInterval(@Param("lotteryType")String lotteryType,@Param("minIssueNo")String minIssueNo,@Param("maxIssueNo")String maxIssueNo);
	
	/**
	 * 批量更新数据的使用状态
	 */
	int updateBatch(List<LotteryCodeXyImport> lotteryCodeXyImports);
	
}