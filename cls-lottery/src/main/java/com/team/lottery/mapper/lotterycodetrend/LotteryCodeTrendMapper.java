package com.team.lottery.mapper.lotterycodetrend;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.ZstQueryVo;
import com.team.lottery.vo.LotteryCodeTrend;

public interface LotteryCodeTrendMapper {
    int deleteByPrimaryKey(Long id);

    int insert(LotteryCodeTrend record);

    int insertSelective(LotteryCodeTrend record);

    LotteryCodeTrend selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(LotteryCodeTrend record);

    int updateByPrimaryKey(LotteryCodeTrend record);
    
    /**
     * 根据彩种查询走势图对象
     * @param lotteryName
     * @return
     */
    LotteryCodeTrend selectByLotteryName(@Param("lotteryName") String lotteryName);
    
    LotteryCodeTrend selectByKindAndExpect(@Param("lotteryName") String lotteryName,@Param("lotteryNum") String lotteryNum);
    
    /**
     * 根据走势图参数查询
     * @param zstQueryVo
     * @return
     */
    List<LotteryCodeTrend> getByZstQuery(ZstQueryVo zstQueryVo);
    
    /**
     * 每日任务删除走势图数据，删除7天前数据（除低频彩）
     * @return
     */
    int deleteTrendCodeBefore7Days(@Param("day") Long day);
}