package com.team.lottery.mapper.lotterycache;

import java.util.List;

import com.team.lottery.vo.LotteryCache;

public interface LotteryCacheMapper {
    int deleteByPrimaryKey(Long id);

    int insert(LotteryCache record);

    int insertSelective(LotteryCache record);

    LotteryCache selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(LotteryCache record);

    int updateByPrimaryKey(LotteryCache record);
    
    /**
     * 根据订单,获取投注号码的缓存信息
     * @param orderId
     * @return
     */
    public List<LotteryCache> getLotteryCacheMsg(Long orderId);
}