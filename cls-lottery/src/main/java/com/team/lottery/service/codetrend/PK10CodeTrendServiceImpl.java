package com.team.lottery.service.codetrend;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.team.lottery.service.lotterykindplay.CodeTrendService;

/**
 * pk10走势图
 * @author Administrator
 *
 */
@Service("pK10CodeTrendServiceImpl")
public class PK10CodeTrendServiceImpl extends CodeTrendService{

	@Override
	public String getPlaykindContent(String openCode) {
		//冠军大小、冠军单双、冠亚和值大小、冠亚和值单双
		String[] ocs = openCode.split(",");
		String str = "";
		if (ocs[0].startsWith("0")) {
			ocs[0] = ocs[0].substring(1);
		}
		if (ocs[1].startsWith("0")) {
			ocs[1] = ocs[1].substring(1);
		}
		Integer first_num = Integer.parseInt(ocs[0]);
		Integer second_num = Integer.parseInt(ocs[1]);
		if (first_num> 5) {
			str += "大,";
		}else {
			str += "小,";
		}
		if (first_num%2==0) {
			str += "双,";
		}else {
			str += "单,";
		}
		if ((first_num+second_num)>10) {
			str += "大,";
		}else {
			str += "小,";
		}
		if ((first_num+second_num)%2==0) {
			str += "双";
		}else {
			str += "单";
		}
		return str;
	}

	@Override
	public String getNumPosition(String openCode, String lastNumPosition) {
		String[] ocs = openCode.split(",");
		for (int i = 0; i < ocs.length; i++) {
			if (ocs[i].startsWith("0")) {
				ocs[i] = ocs[i].substring(1);
			}
		}
		String str = "";
		if (StringUtils.isBlank(lastNumPosition)) { //第一期数据
			for (int i = 0; i < ocs.length; i++) {
				for (int j = 1; j < 11; j++) {
					if (Integer.valueOf(ocs[i]).intValue()==j) {
						str += "0,";
					}else {
						str += "1,";
					}
				}
			}
		}else {//非第一期数据需要以第一期数据为根据
			String[] lastNumPositions = lastNumPosition.split(",");
			for (int i = 0; i < lastNumPositions.length; i++) {
				// i/10:ocs数组的位数；
				// ocs[i/10]:ocs数组元素
				// Integer.valueOf(ocs[i/10]).intValue()：ocs数组元素整型值
				// i%10：lastNumPosition中每11位循环一次设置位置信息
				if ((i%10+1)==Integer.valueOf(ocs[i/10]).intValue()) {
					str += "0,";
				}else {
					Integer num_position = Integer.parseInt(lastNumPositions[i])+1;//相应的位置遗漏值+1
					str += num_position+",";
				}
			}
		}
		str = str.substring(0,str.length()-1);
		return str;
	}

}
