package com.team.lottery.service.codeplan.ks;

import java.util.Random;

import org.springframework.stereotype.Service;

import com.team.lottery.service.lotterykindplay.LotterykindPlayCodeplanService;
import com.team.lottery.vo.CodePlan;

/**
 * 和值单双
 * @author Administrator
 *
 */
@Service("ks_codeplan_hzdsServiceImpl")
public class HzdsServiceImpl extends LotterykindPlayCodeplanService{

	@Override
	public String generateCodes() {
		int num1 = new Random().nextInt(6)+1;
		int num2 = new Random().nextInt(6)+1;
		int num3 = new Random().nextInt(6)+1;
		if ((num1+num2+num3)%2==0) {
			return "双|0";
		}else {
			return "单|1";
		}
	}

	@Override
	public boolean getProstate(CodePlan codePlan, String openCode) {
		String codes = codePlan.getCodes();
		String code2 = codes.split("\\|")[1];
		String[] openCodes = openCode.split(",");
		if ((code2.equals("0")&&(Integer.parseInt(openCodes[0])+Integer.parseInt(openCodes[1])+Integer.parseInt(openCodes[2]))%2==0) ||
				(code2.equals("1")&&(Integer.parseInt(openCodes[0])+Integer.parseInt(openCodes[1])+Integer.parseInt(openCodes[2]))%2==1)) {
			return true;
		}
		return false;
	}

	@Override
	public int getCodeNum() {
		return 1;
	}

	@Override
	public String showCode(CodePlan codePlan) {
		String codes = codePlan.getCodes();
		String code2 = codes.split("\\|")[0];
		return code2;
	}

}
