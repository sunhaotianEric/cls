package com.team.lottery.service.lotterykindplay;

import com.team.lottery.vo.CodePlan;

public abstract class LotterykindPlayCodeplanService {
	
	/**
	 * 生成预期计划投注
	 * @return
	 */
	public abstract String generateCodes();
	
	/**
	 * 根据预测号codePlan和开奖号计算是否中奖
	 * @param codePlan
	 * @param openCode
	 * @return
	 */
	public abstract boolean getProstate(CodePlan codePlan, String openCode);
	
	/**
	 * 投注的号码个数
	 */
	public abstract int getCodeNum();
	
	public abstract String showCode(CodePlan codePlan);

}
