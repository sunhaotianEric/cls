package com.team.lottery.service.codeplan.ssc;

import java.util.Random;

import org.springframework.stereotype.Service;

import com.team.lottery.service.lotterykindplay.LotterykindPlayCodeplanService;
import com.team.lottery.vo.CodePlan;

/**
 * 五星定位胆
 * @author Administrator
 *
 */
@Service("ssc_codeplan_wxdwdServiceImpl")
public class WxdwdServiceImpl extends LotterykindPlayCodeplanService{

	@Override
	public String generateCodes() {
		String codes = "";
		for (int i = 0; i < 5; i++) {
			int num = new Random().nextInt(10);
			if (i==4) {
				codes += num;
			}else {
				codes += num+",";
			}
		}
		return codes;
	}

	@Override
	public boolean getProstate(CodePlan codePlan, String openCode) {
		String[] cps = codePlan.getCodes().split(",");
		String[] ops = openCode.split(",");
		if (cps[0].equals(ops[0]) || cps[1].equals(ops[1]) ||
				cps[2].equals(ops[2]) || cps[3].equals(ops[3]) ||
				cps[4].equals(ops[4])) {
			return true;
		}
		return false;
	}

	@Override
	public int getCodeNum() {
		return 5;
	}

	@Override
	public String showCode(CodePlan codePlan) {
		String codes = codePlan.getCodes();
		return codes;
	}

}
