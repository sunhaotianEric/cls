package com.team.lottery.service.codeplan.syxw;

import java.util.Random;

import org.springframework.stereotype.Service;

import com.team.lottery.service.lotterykindplay.LotterykindPlayCodeplanService;
import com.team.lottery.vo.CodePlan;

/**
 * 第一球定位
 * @author Administrator
 *
 */
@Service("syxw_codeplan_dyqdwServiceImpl")
public class DyqdwServiceImpl extends LotterykindPlayCodeplanService{

	@Override
	public String generateCodes() {
		int num1 = new Random().nextInt(11)+1;
		if (num1>9) {
			return num1 + "";
		}else {
			return "0" + num1;
		}
	}

	@Override
	public boolean getProstate(CodePlan codePlan, String openCode) {
		String codes = codePlan.getCodes();
		String openCode_0 = openCode.split(",")[0];
		if (codes.equals(openCode_0)) {
			return true;
		}
		return false;
	}

	@Override
	public int getCodeNum() {
		return 1;
	}

	@Override
	public String showCode(CodePlan codePlan) {
		String codes = codePlan.getCodes();
		return codes;
	}

}
