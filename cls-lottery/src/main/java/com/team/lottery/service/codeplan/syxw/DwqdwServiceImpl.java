package com.team.lottery.service.codeplan.syxw;

import java.util.Random;

import org.springframework.stereotype.Service;

import com.team.lottery.service.lotterykindplay.LotterykindPlayCodeplanService;
import com.team.lottery.vo.CodePlan;

/**
 * 第五球定位
 * @author Administrator
 *
 */
@Service("syxw_codeplan_dwqdwServiceImpl")
public class DwqdwServiceImpl extends LotterykindPlayCodeplanService{

	@Override
	public String generateCodes() {
		int num1 = new Random().nextInt(11)+1;
		if (num1>9) {
			return num1 + "";
		}else {
			return "0" + num1;
		}
	}

	@Override
	public boolean getProstate(CodePlan codePlan, String openCode) {
		String codes = codePlan.getCodes();
		if (codes.startsWith("0")) {
			codes = codes.substring(1);
		}
		String openCode_0 = openCode.split(",")[4];
		if (codes.equals(openCode_0)) {
			return true;
		}
		return false;
	}

	@Override
	public int getCodeNum() {
		return 1;
	}

	@Override
	public String showCode(CodePlan codePlan) {
		String codes = codePlan.getCodes();
		return codes;
	}

}
