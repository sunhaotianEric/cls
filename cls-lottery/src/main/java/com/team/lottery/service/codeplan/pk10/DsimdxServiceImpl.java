package com.team.lottery.service.codeplan.pk10;

import java.util.Random;

import org.springframework.stereotype.Service;

import com.team.lottery.service.lotterykindplay.LotterykindPlayCodeplanService;
import com.team.lottery.vo.CodePlan;

/**
 * 第四名大小
 * @author Administrator
 *
 */
@Service("pk10_dsimdxServiceImpl")
public class DsimdxServiceImpl extends LotterykindPlayCodeplanService{

	@Override
	public String generateCodes() {
		int num = new Random().nextInt(11);
		if (num>4) {
			return "大|4";
		}
		return "小|3";
	}

	@Override
	public boolean getProstate(CodePlan codePlan, String openCode) {
		String codes = codePlan.getCodes();
		String code2 = codes.split("\\|")[1];
		String open_code = openCode.split(",")[3];
		Integer open_codeInteger = Integer.parseInt(open_code);
		if ((code2.equals("4")&&(open_codeInteger>4)) || (code2.equals("3")&&(open_codeInteger<5))) {
			return true;
		}
		return false;
	}

	@Override
	public int getCodeNum() {
		return 1;
	}

	@Override
	public String showCode(CodePlan codePlan) {
		String codes = codePlan.getCodes();
		String code2 = codes.split("\\|")[0];
		return code2;
	}

}
