package com.team.lottery.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import org.apache.commons.collections.map.HashedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.extvo.CodeTrendVo;
import com.team.lottery.extvo.ZstQueryVo;
import com.team.lottery.mapper.lotterycodexytrend.LotteryCodeXyTrendMapper;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCodeXyTrend;

@Service("lotteryCodeXyTrendService")
public class LotteryCodeXyTrendService {
	private static Logger logger = LoggerFactory.getLogger(LotteryCodeXyService.class);
	
	@Autowired
	private LotteryCodeXyTrendMapper lotteryCodeXyTrendMapper;
	
	public LotteryCodeXyTrend selectByLotteryName( String lotteryName){
		return lotteryCodeXyTrendMapper.selectByLotteryName(lotteryName);
	}
	
	@Transactional(readOnly=false,rollbackFor=Exception.class)
	public int insertSelective(LotteryCodeXyTrend record){
		return lotteryCodeXyTrendMapper.insertSelective(record);
	}
	
	/**
	 * 时时彩、分分彩走势图数据
	 * @param zstQueryVo
	 * @return
	 */
	public Map<String, Object> getSscOrFfcCodeTrend(ZstQueryVo zstQueryVo){
		Map<String, Object> result = new HashedMap();
		result.put("lotteryName", zstQueryVo.getLotteryKind().getDescription());
		result.put("headerInfo", getSscOrFfcTrendHeader());
		result.put("openResultHeader", getSscOrFfcOpenResultHeader());
		result.put("zsHeader", get0To9TrendHeader());
		List<LotteryCodeXyTrend> list = lotteryCodeXyTrendMapper.getByZstQuery(zstQueryVo);
		List<CodeTrendVo> list2 = new ArrayList<CodeTrendVo>();//走势图数据
		if ((list != null) && (list.size()>0)) {
			result = calculateData(list, list2, result);
		}
		return result;
	}
	
	/**
	 * 快三走势图数据
	 * @param zstQueryVo
	 * @return
	 */
	public Map<String, Object> getK3CodeTrend(ZstQueryVo zstQueryVo){
		Map<String, Object> result = new HashedMap();
		result.put("lotteryName", zstQueryVo.getLotteryKind().getDescription());
		result.put("headerInfo", getK3OrDpcTrendHeader());
		result.put("openResultHeader", getK3OpenResult());
		result.put("zsHeader", get1To6TrendHeader());
		List<LotteryCodeXyTrend> list = lotteryCodeXyTrendMapper.getByZstQuery(zstQueryVo);
		List<CodeTrendVo> list2 = new ArrayList<CodeTrendVo>();//走势图数据
		if ((list != null) && (list.size()>0)) {
			result = calculateData(list, list2, result);
		}
		return result;
	}
	
	/**
	 * PK10走势图数据
	 * @param zstQueryVo
	 * @return
	 */
	public Map<String, Object> getPk10CodeTrend(ZstQueryVo zstQueryVo){
		Map<String, Object> result = new HashedMap();
		result.put("lotteryName", zstQueryVo.getLotteryKind().getDescription());
		result.put("headerInfo", getPk10TrendHeader());
		result.put("openResultHeader", getPk10OpenResultHeader());
		result.put("zsHeader", get1To10TrendHeader());
		List<LotteryCodeXyTrend> list = lotteryCodeXyTrendMapper.getByZstQuery(zstQueryVo);
		List<CodeTrendVo> list2 = new ArrayList<CodeTrendVo>();//走势图数据
		if ((list != null) && (list.size()>0)) {
			result = calculateData(list, list2, result);
		}
		return result;
	}
	
	/**
	 * 六合彩走势图数据
	 * @param zstQueryVo
	 * @return
	 */
	public Map<String, Object> getLhcCodeTrend(ZstQueryVo zstQueryVo){
		Map<String, Object> result = new HashedMap();
		result.put("lotteryName", zstQueryVo.getLotteryKind().getDescription());
		result.put("headerInfo", "");
		result.put("openResultHeader", getLhcOpenResult());
		result.put("zsHeader", "");
		List<LotteryCodeXyTrend> list = lotteryCodeXyTrendMapper.getByZstQuery(zstQueryVo);
		List<CodeTrendVo> list2 = new ArrayList<CodeTrendVo>();//走势图数据
		if ((list != null) && (list.size()>0)) {
			result = calculateData(list, list2, result);
		}
		return result;
	}
	
	/**
	 * 六合彩开奖结果头信息
	 * @return
	 */
	private List<String> getLhcOpenResult(){
		List<String> list = new ArrayList<String>();
		list.add("期号");
		list.add("正码");
		list.add("特码");
		list.add("生肖");
		list.add("大小/单双");
		list.add("家禽野兽");
		list.add("尾数");
		list.add("和大小/单双");
		list.add("总和");
		list.add("总单双");
		list.add("总大小");
		return list;
	}
	
	/**
	 * PK10走势图头信息
	 */
	private List<String> getPk10TrendHeader(){
		List<String> headerInfo = new ArrayList<String>();
		headerInfo.add("开奖结果");
		headerInfo.add("第一位");
		headerInfo.add("第二位");
		headerInfo.add("第三位");
		headerInfo.add("第四位");
		headerInfo.add("第五位");
		headerInfo.add("第六位");
		headerInfo.add("第七位");
		headerInfo.add("第八位");
		headerInfo.add("第九位");
		headerInfo.add("第十位");
		return headerInfo;
	}
	
	/**
	 * PK10开奖结果头信息
	 * @return
	 */
	private List<String> getPk10OpenResultHeader(){
		//冠军大小、冠军单双、冠亚和值大小、冠亚和值单双
		List<String> list = new ArrayList<String>();
		list.add("期号");
		list.add("开奖号码");
		list.add("冠军大小");
		list.add("冠军单双");
		list.add("冠亚和值大小");
		list.add("冠亚和值单双");
		return list;
	}
	
	/**
	 * 走势位范围是1~10头信息
	 * @return
	 */
	private List<String> get1To10TrendHeader(){
		List<String> list = new ArrayList<String>();
		for (int i = 1; i <= 10; i++) {
			list.add(i+"");
		}
		return list;
	}
	
	/**
	 * 走势位范围是1~6
	 * @return
	 */
	private List<String> get1To6TrendHeader(){
		List<String> list = new ArrayList<String>();
		for (int i = 1; i <= 6; i++) {
			list.add(i+"");
		}
		return list;
	}
	
	/**
	 * 快3开奖结果头信息
	 * @return
	 */
	private List<String> getK3OpenResult(){
		List<String> list = new ArrayList<String>();
		list.add("期号");
		list.add("开奖号码");
		list.add("和值");
		list.add("总和大小");
		list.add("总和单双");
		list.add("跨度");
		list.add("形态");
		return list;
	}
	
	/**
	 * 快3、低频彩走势图头信息
	 * @return
	 */
	private List<String> getK3OrDpcTrendHeader(){
		List<String> headerInfo = new ArrayList<String>();
		headerInfo.add("开奖结果");
		headerInfo.add("百位");
		headerInfo.add("十位");
		headerInfo.add("个位");
		return headerInfo;
	}
	
	/**
	 * 时时彩分分彩走势图头信息
	 * @return
	 */
	private List<String> getSscOrFfcTrendHeader(){
		List<String> headerInfo = new ArrayList<String>();
		headerInfo.add("开奖结果");
		headerInfo.add("万位");
		headerInfo.add("千位");
		headerInfo.add("百位");
		headerInfo.add("十位");
		headerInfo.add("个位");
		return headerInfo;
	}
	
	/**
	 * 时时彩分分彩走势图开奖结果头信息
	 * @return
	 */
	private List<String> getSscOrFfcOpenResultHeader(){
		List<String> openResultHeader = new ArrayList<String>();
		openResultHeader.add("期号");
		openResultHeader.add("开奖号码");
		openResultHeader.add("万位");
		openResultHeader.add("千位");
		openResultHeader.add("百位");
		openResultHeader.add("十位");
		openResultHeader.add("前三");
		openResultHeader.add("中三");
		openResultHeader.add("后三");
		openResultHeader.add("和值");
		openResultHeader.add("大小");
		openResultHeader.add("单双");
		openResultHeader.add("牛牛");
		openResultHeader.add("1VS2");
		openResultHeader.add("1VS3");
		openResultHeader.add("1VS4");
		openResultHeader.add("1VS5");
		openResultHeader.add("2VS3");
		openResultHeader.add("2VS4");
		openResultHeader.add("2VS5");
		openResultHeader.add("3VS4");
		openResultHeader.add("3VS5");
		openResultHeader.add("4VS5");
		return openResultHeader;
	}
	
	/**
	 * 走势位范围是0~9头信息
	 * @return
	 */
	private List<String> get0To9TrendHeader(){
		List<String> list = new ArrayList<String>();
		for (int i = 0; i <= 9; i++) {
			list.add(i+"");
		}
		return list;
	}
	
	/**
	 * 根据开奖数据计算出现总次数、最大连出值、最大遗漏值、平均遗漏值
	 * @param list
	 * @param list2
	 * @param result
	 * @return
	 */
	private Map<String, Object> calculateData(List<LotteryCodeXyTrend> list,List<CodeTrendVo> list2,Map<String, Object> result){
		String[][] arr = new String[list.size()][];
		for (int i = 0; i < list.size(); i++) {
			LotteryCodeXyTrend lotteryCodeTrend = list.get(i);
			CodeTrendVo codeTrendVo = new CodeTrendVo();
			codeTrendVo.setId(lotteryCodeTrend.getId());
			codeTrendVo.setLotteryNum(lotteryCodeTrend.getLotteryNum());
			codeTrendVo.setOpenCodeArr(lotteryCodeTrend.getOpenCode().split(","));
			if (StringUtils.isNotBlank(lotteryCodeTrend.getLotteryNum())) {
				codeTrendVo.setIssue(lotteryCodeTrend.getLotteryNum().substring(lotteryCodeTrend.getLotteryNum().length()-3) + "期");
			}
			if (StringUtils.isNotBlank(lotteryCodeTrend.getOpenCode())) {
				//六合彩开奖号码对应的是正码，特别处理
				if (lotteryCodeTrend.getLotteryName().endsWith("LHC")) {
					String[] ocs = lotteryCodeTrend.getOpenCode().split(",");
					String zhengma = ocs[0]+","+ocs[1]+","+ocs[2]+","+ocs[3]+","+ocs[4]+","+ocs[5];
					codeTrendVo.setOpenCode(zhengma);
				}else {
					codeTrendVo.setOpenCode(lotteryCodeTrend.getOpenCode());
				}
			}
			if (StringUtils.isNotBlank(lotteryCodeTrend.getPlaykindContent())) {
				codeTrendVo.setPlaykindResult(lotteryCodeTrend.getPlaykindContent().split(","));
			}
			if (StringUtils.isNotBlank(lotteryCodeTrend.getNumPosition())) {
				codeTrendVo.setTrendData(lotteryCodeTrend.getNumPosition().split(","));
				arr[i] = codeTrendVo.getTrendData();
			}
			list2.add(codeTrendVo);
		}
		//各个位置出现总次数、最大连出值、最大遗漏值、平均遗漏值
		if (arr[0]!=null) {
			String[] winTotalNum = new String[arr[0].length];
			String[] maxSeriesNum = new String[arr[0].length];
			String[] maxMissNum = new String[arr[0].length];
			String[] avgMissNum = new String[arr[0].length];
			for (int j = 0; j < arr[0].length; j++) {
				TreeSet<String> maxMissNumSet = new TreeSet<String>();
				TreeSet<Integer> maxSeriesNumSet = new TreeSet<Integer>();
				Integer totalNum = 0;
				Integer maxMiss = 0;
				for (int i = 0; i < arr.length; i++) {
					maxMissNumSet.add(arr[i][j]);
					if (arr[i][j].equals("0")) {
						totalNum++;
						maxMiss++;
						Integer ii = new Integer(i);
						while ((ii+1) < arr.length) {
							if (arr[ii][j].equals(arr[ii+1][j])) {
								maxMiss++;
								ii++;
							}else {
								break;
							}
						}
					}
					maxSeriesNumSet.add(maxMiss);
					maxMiss = 0;
				}
				if (totalNum.equals(0)) {
					avgMissNum[j] = arr.length+"";
				}else {
					avgMissNum[j] = String.format("%.0f", arr.length*1.0/totalNum);
				}
				winTotalNum[j] = totalNum+"";
				maxMissNum[j] = maxMissNumSet.last();
				maxSeriesNum[j] = maxSeriesNumSet.last()+"";
			}
			result.put("winTotalNum", winTotalNum);
			result.put("maxSeriesNum", maxSeriesNum);
			result.put("maxMissNum", maxMissNum);
			result.put("avgMissNum", avgMissNum);
		}
		result.put("trendList", list2);
		return result;
	}

}
