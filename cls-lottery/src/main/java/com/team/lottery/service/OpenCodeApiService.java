package com.team.lottery.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.opencodeapi.OpenCodeApiMapper;
import com.team.lottery.vo.OpenCodeApi;

@Service("openCodeApiService")
public class OpenCodeApiService {

	@Autowired
	private OpenCodeApiMapper openCodeApiMapper;
		
	
	
    public int deleteByPrimaryKey(Integer id){ 
    	return openCodeApiMapper.deleteByPrimaryKey(id);
    }
    @Transactional(readOnly=false,rollbackFor=Exception.class)
    public int updateByPrimaryKeySelective(OpenCodeApi openCodeApi){
    	int result = openCodeApiMapper.updateByPrimaryKeySelective(openCodeApi);
    
		return result;
    }
    


    @Transactional(readOnly=false,rollbackFor=Exception.class)
    public int insertSelective(OpenCodeApi openCodeApi){
    	openCodeApi.setCreateTime(new Date());
    	int result=openCodeApiMapper.insertSelective(openCodeApi);
    	return result;
    }

    public OpenCodeApi selectByPrimaryKey(Integer id){
    	return openCodeApiMapper.selectByPrimaryKey(id);
    }
    

	
    /**
     * 分页查找所有的业务系统
     * @return
     */
    public Page getOpenCodeApiByQueryPage(OpenCodeApi query,Page page){
		page.setTotalRecNum(openCodeApiMapper.getOpenCodeApiByQueryPageCount(query));
		page.setPageContent(openCodeApiMapper.getOpenCodeApiByQueryPage(query,page.getStartIndex(),page.getPageSize()));
    	return page;
    }
    
    public List<OpenCodeApi> getOpenCodeApiByLotteryKind(OpenCodeApi query){
		return openCodeApiMapper.getOpenCodeApiByLotteryKind(query);	
    }
}
