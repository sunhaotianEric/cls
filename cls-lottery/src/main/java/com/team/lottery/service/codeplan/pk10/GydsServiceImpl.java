package com.team.lottery.service.codeplan.pk10;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Service;

import com.team.lottery.service.lotterykindplay.LotterykindPlayCodeplanService;
import com.team.lottery.vo.CodePlan;

/**
 * 冠亚单双
 * @author Administrator
 */
@Service("pk10_gydsServiceImpl")
public class GydsServiceImpl extends LotterykindPlayCodeplanService{

	@Override
	public String generateCodes() {
		int num1 = new Random().nextInt(11);
		int num2 = new Random().nextInt(11);
		if ((num1+num2)%2==0) {
			return "双|0";
		}
		return "单|1";
	}

	@Override
	public boolean getProstate(CodePlan codePlan, String openCode) {
		String codes = codePlan.getCodes();
		String code2 = codes.split("\\|")[1];
		String open_code_gj = openCode.split(",")[0];
		String open_code_yj = openCode.split(",")[1];
		Integer open_code_gjInteger = Integer.parseInt(open_code_gj);
		Integer open_code_yjInteger = Integer.parseInt(open_code_yj);
		if ((code2.equals("0")&&((open_code_gjInteger+open_code_yjInteger)%2==0)) ||
				(code2.equals("1")&&((open_code_gjInteger+open_code_yjInteger)%2==1))) {
			return true;
		}
		return false;
	}

	@Override
	public int getCodeNum() {
		return 2;
	}

	@Override
	public String showCode(CodePlan codePlan) {
		String codes = codePlan.getCodes();
		String code2 = codes.split("\\|")[0];
		return code2;
	}

}
