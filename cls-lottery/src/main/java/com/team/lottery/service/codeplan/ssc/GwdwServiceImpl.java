package com.team.lottery.service.codeplan.ssc;

import java.util.Random;

import org.springframework.stereotype.Service;

import com.team.lottery.service.lotterykindplay.LotterykindPlayCodeplanService;
import com.team.lottery.vo.CodePlan;

/**
 * 个位定位
 * @author Administrator
 *
 */
@Service("ssc_codeplan_gwdwServiceImpl")
public class GwdwServiceImpl extends LotterykindPlayCodeplanService{

	@Override
	public String generateCodes() {
		int num = new Random().nextInt(10);
		return num+"";
	}

	@Override
	public boolean getProstate(CodePlan codePlan, String openCode) {
		String codes = codePlan.getCodes();
		String open_code = openCode.split(",")[4];
		if (open_code.equals(codes)) {
			return true;
		}
		return false;
	}

	@Override
	public int getCodeNum() {
		return 1;
	}

	@Override
	public String showCode(CodePlan codePlan) {
		String codes = codePlan.getCodes();
		return codes;
	}

}
