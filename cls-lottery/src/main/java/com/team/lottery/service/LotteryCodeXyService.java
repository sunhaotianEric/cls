package com.team.lottery.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.extvo.LhcOpenCode;
import com.team.lottery.extvo.LotteryCodeQuery;
import com.team.lottery.extvo.LotteryCodeXyQuery;
import com.team.lottery.extvo.OmmitAndHotColdQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.ZstQueryVo;
import com.team.lottery.extvo.ZstVo;
import com.team.lottery.mapper.lotterycodexy.LotteryCodeXyMapper;
import com.team.lottery.redis.JedisUtils;
import com.team.lottery.service.lotterykind.LotteryKindService;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.util.BeanUtilsExtends;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.OmmitUntil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryCodeXy;

import redis.clients.jedis.Jedis;

@Service("lotteryCodeXyService")
public class LotteryCodeXyService {
	
	private static Logger logger = LoggerFactory.getLogger(LotteryCodeXyService.class);

	@Autowired
	private LotteryCodeXyMapper lotteryCodeXyMapper;
	
    public int deleteByPrimaryKey(Integer id){
    	return lotteryCodeXyMapper.deleteByPrimaryKey(id);
    }

    public int insert(LotteryCodeXy record){
    	return lotteryCodeXyMapper.insert(record);
    }

    public int insertSelective(LotteryCodeXy record){
    	record.setAddtime(new Date());
    	int res = lotteryCodeXyMapper.insertSelective(record);
    	if(res > 0) {
    		//将数据插入到redis中
    		this.addRedisLotteryCode(record);
    	}
    	return res;
    }

    public LotteryCodeXy selectByPrimaryKey(Integer id){
    	return lotteryCodeXyMapper.selectByPrimaryKey(id);
    }

    public int updateByPrimaryKeySelective(LotteryCodeXy record){
    	return lotteryCodeXyMapper.updateByPrimaryKeySelective(record);
    }

    public int updateByPrimaryKey(LotteryCodeXy record){
    	return lotteryCodeXyMapper.updateByPrimaryKey(record);
    }
    
    public int updateByPrimaryKeyByBizSystem(LotteryCodeXy record){
    	return lotteryCodeXyMapper.updateByPrimaryKeyByBizSystem(record);
    }
	/**
	 * 查找某个彩种的某个期号的开奖号码是否存在
	 */
	public boolean getLotteryCodeXyByKindAndExpectExist(LotteryCodeXy lastLotteryCodeXy,String lotteryKindName){
	    LotteryCodeXy lastRecordLotteryCodeXy = this.getLotteryCodeByKindAndExpect(lotteryKindName,lastLotteryCodeXy.getLotteryNum(),lastLotteryCodeXy.getBizSystem());
	    if(lastRecordLotteryCodeXy != null){
	    	return true;
	    }else{
	    	return false;
	    }
	}
	
    /**
     * 某个彩种某个时间段的所有期号
     * @param lotteryKind
     * @param createDateStart
     * @param createDateEnd
     * @return
     */
    public List<LotteryCode> getLotteryCodesByKindAndDate(String lotteryKind,Date createDateStart,Date createDateEnd,String bizSystem){
    	return lotteryCodeXyMapper.getLotteryCodesByKindAndDate(lotteryKind, createDateStart, createDateEnd,bizSystem);
    }
	
    /**
     *  获取某个彩种最新的开奖记录
     * @return
     */
    public LotteryCodeXy getLastLotteryCode(ELotteryKind lotteryKind,String bizSystem){
	    LotteryCodeXy lastRecordLotteryCodeXy = lotteryCodeXyMapper.getLastLotteryCode(lotteryKind.name(),bizSystem);
	    LotteryCodeXy lastRecordLotteryCode = new LotteryCodeXy();
	    if(lastRecordLotteryCodeXy!=null){
	    	BeanUtilsExtends.copyProperties(lastRecordLotteryCode, lastRecordLotteryCodeXy);
	    }
	    return lastRecordLotteryCode;
    }
    
    /**
     *  在redis里获取某个彩种最新的开奖记录
     * @return
     */
	public LotteryCodeXy getLastLotteryCodeOnRedis(ELotteryKind lotteryKind,String bizSystem) {
		LotteryCodeXy lotteryCode = null;
		Jedis jedis = null;
		try {
			jedis = JedisUtils.getResource();
			// 根据key在jedis中得最新开奖信息
			String value = jedis.get(lotteryKind.name()+"."+bizSystem+ ".new");
			if (!StringUtils.isEmpty(value)) {
				// 竖线分割value值 如：20160927001|1,2,3,4,5 前面为期号 后面为开奖号
				String[] lotteryCodeInfo = value.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
				
				lotteryCode = new LotteryCodeXy();
				lotteryCode.setLotteryNum(lotteryCodeInfo[0]);
				lotteryCode.setCodesStr(lotteryCodeInfo[1]);
				lotteryCode.setBizSystem(bizSystem);
				String[] openNum = lotteryCodeInfo[1].split(ConstantUtil.OPENCODE_PLIT);
				for (int i = 0; i < openNum.length; i++) {
					String numInfo = openNum[i];
					if (i == 0) {
						lotteryCode.setNumInfo1(numInfo);
					} else if (i == 1) {
						lotteryCode.setNumInfo2(numInfo);
					} else if (i == 2) {
						lotteryCode.setNumInfo3(numInfo);
					} else if (i == 3) {
						lotteryCode.setNumInfo4(numInfo);
					} else if (i == 4) {
						lotteryCode.setNumInfo5(numInfo);
					} else if (i == 5) {
						lotteryCode.setNumInfo6(numInfo);
					} else if (i == 6) {
						lotteryCode.setNumInfo7(numInfo);
					} else if (i == 7) {
						lotteryCode.setNumInfo8(numInfo);
					} else if (i == 8) {
						lotteryCode.setNumInfo9(numInfo);
					} else if (i == 9) {
						lotteryCode.setNumInfo10(numInfo);
					} else if (i == 10) {
						lotteryCode.setNumInfo11(numInfo);
					} else if (i == 11) {
						lotteryCode.setNumInfo12(numInfo);
					} else if (i == 12) {
						lotteryCode.setNumInfo13(numInfo);
					} else if (i == 13) {
						lotteryCode.setNumInfo14(numInfo);
					}
				}
			}
		} catch (Exception e) {
			logger.error("jedis获取最新开奖号码失败！", e);
			// 归还资源
			JedisUtils.returnBrokenResource(jedis);
		} finally {
			// 归还资源
			JedisUtils.returnResource(jedis);
		}
		return lotteryCode;
	}
    
	/**
	 * 从redis缓存中获取最近十期的开奖号码
	 * @param lotteryKind
	 * @return
	 */
	public List<LotteryCodeXy> getNearestTenLotteryCodeOnRedis(ELotteryKind lotteryKind,String bizSystem) {
		List<LotteryCodeXy> lotteryList = new ArrayList<LotteryCodeXy>();
		Jedis jedis = null;
		try {
			jedis = JedisUtils.getResource();
			//根据彩种获得最近十期的开奖信息
			Set<String> lotteryCodes = jedis.zrevrange(lotteryKind.name()+ ".zset", 0, 9);
			if (CollectionUtils.isNotEmpty(lotteryCodes)) {
				String lotteryCode = "";
				Iterator<String> iterator = lotteryCodes.iterator();
				
				while(iterator.hasNext()){
					lotteryCode = iterator.next();

					// 竖线分割value值 如：20160927001|1,2,3,4,5 前面为期号 后面为开奖号
					String[] openCodeInfor = lotteryCode.split(ConstantUtil.SOURCECODE_ARRANGE_SPLIT);
					
					LotteryCodeXy nearestTenLotteryCode = new LotteryCodeXy();
					nearestTenLotteryCode.setLotteryNum(openCodeInfor[0]);
					nearestTenLotteryCode.setCodesStr(openCodeInfor[1]);
					nearestTenLotteryCode.setBizSystem(bizSystem);
					String[] openNum = openCodeInfor[1].split(ConstantUtil.OPENCODE_PLIT);
					for (int i = 0; i < openNum.length; i++) {
						String numInfo = openNum[i];
						if (i == 0) {
							nearestTenLotteryCode.setNumInfo1(numInfo);
						} else if (i == 1) {
							nearestTenLotteryCode.setNumInfo2(numInfo);
						} else if (i == 2) {
							nearestTenLotteryCode.setNumInfo3(numInfo);
						} else if (i == 3) {
							nearestTenLotteryCode.setNumInfo4(numInfo);
						} else if (i == 4) {
							nearestTenLotteryCode.setNumInfo5(numInfo);
						} else if (i == 5) {
							nearestTenLotteryCode.setNumInfo6(numInfo);
						} else if (i == 6) {
							nearestTenLotteryCode.setNumInfo7(numInfo);
						} else if (i == 7) {
							nearestTenLotteryCode.setNumInfo8(numInfo);
						} else if (i == 8) {
							nearestTenLotteryCode.setNumInfo9(numInfo);
						} else if (i == 9) {
							nearestTenLotteryCode.setNumInfo10(numInfo);
						} else if (i == 10) {
							nearestTenLotteryCode.setNumInfo11(numInfo);
						} else if (i == 11) {
							nearestTenLotteryCode.setNumInfo12(numInfo);
						} else if (i == 12) {
							nearestTenLotteryCode.setNumInfo13(numInfo);
						} else if (i == 13) {
							nearestTenLotteryCode.setNumInfo14(numInfo);
						}
					}
					lotteryList.add(nearestTenLotteryCode);
				}
			}	
		} catch (Exception e) {
			logger.error("jedis获取最近十期开奖号码失败！", e);
			// 归还资源
			JedisUtils.returnBrokenResource(jedis);
		} finally {
			// 归还资源
			JedisUtils.returnResource(jedis);
		}

		return lotteryList;
	}

    /**
     * 查找某个彩种的某个期号的开奖号码
     * @param lotteryKind
     * @param expect
     * @return
     */
    public LotteryCodeXy getLotteryCodeByKindAndExpect(String lotteryKind,String expect,String bizSystem){
    	return lotteryCodeXyMapper.getLotteryCodeByKindAndExpect(lotteryKind, expect,bizSystem);
    }
    
    /**
     * 查找某个彩种的某个期号的开奖号码
     * @param lotteryKind
     * @param expect
     * @return
     */
    public List<LotteryCodeXy> getLotteryCodeByKindAndExpects(String lotteryKind,List<String> expects,String bizSystem){
    	return lotteryCodeXyMapper.getLotteryCodeByKindAndExpects(lotteryKind, expects,bizSystem);
    }
    
    /**
     * 查找某个彩种小于某个期号固定长度的所有期号
     * @param lotteryKind
     * @param expect
     * @return
     */
    public List<LotteryCodeXy> getLotteryCodesLessThanNum(String lotteryKind, String lotteryNum, Integer querySize,String bizSystem){
    	return lotteryCodeXyMapper.getLotteryCodesLessThanNum(lotteryKind, lotteryNum, querySize,bizSystem);
    }
    
    /**
     * 向redis添加开奖数据
     * @param record
     */
    public void addRedisLotteryCode(LotteryCodeXy record) {
    	Jedis jedis = null; 
    	try {
	    	jedis = JedisUtils.getResource();
	    	//放置到列表中
	    	//jedis.lpush(record.getLotteryName() + "_list", record.getLotteryNum() + ConstantUtil.SYSTEM_SETTING_SPLIT + record.getOpencodeStr());
	    	
	    	//集合的key
	    	String key = record.getLotteryName()+"."+record.getBizSystem() + ".zset";
	    	//最新开奖号码的key
	    	String newKey = record.getLotteryName()+"."+record.getBizSystem() + ".new";
	    	//期数加开奖号码作为值
	    	String value = record.getLotteryNum()  + ConstantUtil.SYSTEM_SETTING_SPLIT + record.getOpencodeStr();
	    	//使用期数作为分数
	    	Double score = Double.parseDouble(record.getLotteryNum());
	    	
	    	jedis.zadd(key, score, value);
	    	logger.debug("往集合[{}]中添加值[{}]", key, value);
	    	Long setLength = jedis.zcard(key);
	    	if(setLength >ConstantUtil.REDIS_CODE_MAX_RECORD) {
	    		//删除前50条数据，保留550条
	    		logger.debug("删除集合[{}]的前50条数据", key);
	    		jedis.zremrangeByRank(key, 0, 50);
	    	}
	    	
	    	//倒序取出最后一条数据
	    	Set<String> newCodeSets = jedis.zrevrange(key, 0, 0);
	    	if(CollectionUtils.isNotEmpty(newCodeSets)) {
	    		String setLastValue = newCodeSets.iterator().next();
	    		String currentValue = jedis.get(newKey);
	    		//取出最新一条的值与当前最新的值一致，则无需更新,这种情况在插入开奖号码顺序逆序的时候会出现
	    		if(!StringUtils.isEmpty(currentValue) && setLastValue.equals(currentValue)) {
	    			logger.debug("集合中的最新一条记录值[{}]与当前最新记录值[{}]一致，无需更新", setLastValue, currentValue);
	    		} else {
	    			//这时更新号码，取zset中最新的记录
	    			logger.debug("向redis写入最新开奖号码key[{}],value[{}]", newKey, setLastValue);
		    		jedis.set(newKey, setLastValue);
	    		}
	    	}
    	} catch (Exception e) {
    		logger.error("向redis写入数据失败", e);
    		JedisUtils.returnBrokenResource(jedis);
    	} finally{
    		JedisUtils.returnResource(jedis);
    	}
    }
    
    /**
     * 开奖号码删除  联动redis删除
     * @param LotteryName
     * @return
     */
    public boolean delRedisLotteryCode(LotteryCodeXy lotteryCodeXy){
    	if(lotteryCodeXy == null){
    		return false;
    	}
    	
    	Jedis jedis = null;
    	boolean state = false;
		try {
			String lotteryKind = lotteryCodeXy.getLotteryName();
			ELotteryKind valueOf = ELotteryKind.valueOf(lotteryKind);
			LotteryCodeXy lastLotteryCodeXy = this.getLastLotteryCode(valueOf,lotteryCodeXy.getBizSystem());
			
			String oldCodesStr = lotteryCodeXy.getLotteryNum() + ConstantUtil.SYSTEM_SETTING_SPLIT + lotteryCodeXy.getOpencodeStr();
			//期数加开奖号码作为值
			String value = lastLotteryCodeXy.getLotteryNum()  + ConstantUtil.SYSTEM_SETTING_SPLIT + lastLotteryCodeXy.getOpencodeStr();
			
			jedis = JedisUtils.getResource();
			
			//删除集合开奖号码
			jedis.zrem(lotteryKind+"."+lotteryCodeXy.getBizSystem() + ".zset", oldCodesStr);
			
			//重置redis最新开奖记录
			String newKey = lastLotteryCodeXy.getLotteryName()+"."+lastLotteryCodeXy.getBizSystem()+".new";
			jedis.set(newKey, value);
	    	
			state = true;
		} catch (Exception e) {
			logger.error("jedis删除幸运彩开奖号码失败！", e);
			// 归还资源
			JedisUtils.returnBrokenResource(jedis);
		} finally {
			// 归还资源
			JedisUtils.returnResource(jedis);
		}
		return state;
    }
    
    /**
     * lotteryCodeXies同步重置redis
     * @param LotteryName
     * @return
     */
    public boolean resetRedisLotteryCodeXy(List<LotteryCodeXy> lotteryCodeXies){
    	if(lotteryCodeXies == null){
    		return false;
    	}
    	Jedis jedis = null;
    	boolean state = false;
		try {
//			Integer a = 100/0; //测试
			jedis = JedisUtils.getResource();
			//第一步，获取所有key
			Set<String> set = new TreeSet<String>();
			for (LotteryCodeXy record : lotteryCodeXies){
				set.add(record.getLotteryName()+"."+record.getBizSystem() + ".zset");
			}
			//清除redis集合key的value
			for (String string : set) {
				jedis.zremrangeByRank(string, 0, -1);
			}
			//第二步，将数据库中的最新数据同步到redis，最新开奖号码不用清除最终将会被覆盖
			for (LotteryCodeXy record : lotteryCodeXies) {
				//集合的key
		    	String key = record.getLotteryName()+"."+record.getBizSystem() + ".zset";
		    	//最新开奖号码的key
		    	String newKey = record.getLotteryName()+"."+record.getBizSystem() + ".new";
		    	//期数加开奖号码作为值
		    	String value = record.getLotteryNum()  + ConstantUtil.SYSTEM_SETTING_SPLIT + record.getOpencodeStr();
		    	//使用期数作为分数
		    	Double score = Double.parseDouble(record.getLotteryNum());
		    	
		    	jedis.zadd(key, score, value);
		    	logger.debug("往集合[{}]中添加值[{}]", key, value);
		    	Long setLength = jedis.zcard(key);
		    	if(setLength >= 600) {
		    		//删除前50条数据，保留550条
		    		logger.debug("删除集合[{}]的前50条数据", key);
		    		jedis.zremrangeByRank(key, 0, 50);
		    	}
		    	
		    	//倒序取出最后一条数据
		    	Set<String> newCodeSets = jedis.zrevrange(key, 0, 0);
		    	if(CollectionUtils.isNotEmpty(newCodeSets)) {
		    		String setLastValue = newCodeSets.iterator().next();
		    		String currentValue = jedis.get(newKey);
		    		//取出最新一条的值与当前最新的值一致，则无需更新,这种情况在插入开奖号码顺序逆序的时候会出现
		    		if(!StringUtils.isEmpty(currentValue) && setLastValue.equals(currentValue)) {
		    			logger.debug("集合中的最新一条记录值[{}]与当前最新记录值[{}]一致，无需更新", setLastValue, currentValue);
		    		} else {
		    			//这时更新号码，取zset中最新的记录
		    			logger.debug("向redis写入最新开奖号码key[{}],value[{}]", newKey, setLastValue);
			    		jedis.set(newKey, setLastValue);
		    		}
		    	}
			}
	    	
			state = true;
		} catch (Exception e) {
			logger.error("jedis删除幸运彩开奖号码失败！", e);
			// 归还资源
			JedisUtils.returnBrokenResource(jedis);
		} finally {
			// 归还资源
			JedisUtils.returnResource(jedis);
		}
		return state;
    }
    
    /**
     * lotteryCodeXies同步重置redis
     * @param LotteryName
     * @return
     */
    public boolean resetRedisLotteryCodeXy(List<LotteryCodeXy> lotteryCodeXies,String bizSystem,String lotteryName){
    	if(lotteryCodeXies == null){
    		return false;
    	}
    	Jedis jedis = null;
    	boolean state = false;
    	try {
    		/*if (bizSystem.equals("k8cp")) {
    			Integer a = 100/0; //测试
			}*/
    		//集合的key
    		String key = lotteryName +"."+bizSystem+ ".zset";
    		//最新开奖号码的key
    		String newKey = lotteryName +"."+bizSystem+ ".new";
    		jedis = JedisUtils.getResource();
    		//第一步，清除redis中key的值
    		jedis.zremrangeByRank(key, 0, -1);
    		//开奖号码为空的情况
    		if(CollectionUtils.isEmpty(lotteryCodeXies)) {
    			jedis.set(newKey, "");
    		} else {
    			//第二步，将数据库中的最新数据同步到redis，最新开奖号码不用清除最终将会被覆盖
    			for (LotteryCodeXy record : lotteryCodeXies) {
    				//期数加开奖号码作为值
    				String value = record.getLotteryNum()  + ConstantUtil.SYSTEM_SETTING_SPLIT + record.getOpencodeStr();
    				//使用期数作为分数
    				Double score = Double.parseDouble(record.getLotteryNum());
    				
    				jedis.zadd(key, score, value);
    				logger.debug("往集合[{}]中添加值[{}]", key, value);
    			}
    			Long setLength = jedis.zcard(key);
    			if(setLength >ConstantUtil.REDIS_CODE_MAX_RECORD) {
    				//删除前50条数据，保留550条
    				logger.debug("删除集合[{}]的前50条数据", key);
    				jedis.zremrangeByRank(key, 0, 50);
    			}
    			//倒序取出最后一条数据
    			Set<String> newCodeSets = jedis.zrevrange(key, 0, 0);
    			if(CollectionUtils.isNotEmpty(newCodeSets)) {
    				String setLastValue = newCodeSets.iterator().next();
    				String currentValue = jedis.get(newKey);
    				//取出最新一条的值与当前最新的值一致，则无需更新,这种情况在插入开奖号码顺序逆序的时候会出现
    				if(!StringUtils.isEmpty(currentValue) && setLastValue.equals(currentValue)) {
    					logger.debug("集合中的最新一条记录值[{}]与当前最新记录值[{}]一致，无需更新", setLastValue, currentValue);
    				} else {
    					//这时更新号码，取zset中最新的记录
    					logger.debug("向redis写入最新开奖号码key[{}],value[{}]", newKey, setLastValue);
    					jedis.set(newKey, setLastValue);
    				}
    			}
    		}
    		
    		state = true;
    	} catch (Exception e) {
    		logger.error("jedis删除幸运彩开奖号码失败！", e);
    		// 归还资源
    		JedisUtils.returnBrokenResource(jedis);
    	} finally {
    		// 归还资源
    		JedisUtils.returnResource(jedis);
    	}
    	return state;
    }
    
    
	/**
	 * 位数对应的遗漏值(当前遗漏和最大遗漏)
	 * @param nums
	 * @param maps
	 * @param numValue
	 * @param position
	 */
    public void numsOmmit(String [] nums,Map<String, Integer> maps,String numValue,Integer position){
		for(int j = 0; j < nums.length; j++){ 
			String num = nums[j];
			if(numValue.equals(num)){
				if(position == 0){
					maps.put(num, 0); 
				}
				nums[j] = null;
				continue;
			}else{
				if(num != null){
					maps.put(num, maps.get(num) + 1); 
				}
			}
		}
	}
    
    /**
	 * 位数对应的遗漏值(当前遗漏和最大遗漏)
	 * @param nums
	 * @param maps
	 * @param numValue
	 * @param position
	 */
    public void numsOmmit(String [] nums,Map<String, Integer> maps,List<String> numValues,Integer position){
		for(int i = 0; i < nums.length; i++){ 
			String num = nums[i];
			//值是否在数组里面
			boolean isNumIn = false;
			for(int j = 0; j < numValues.size(); j++) {
				if(numValues.get(j).equals(num)){
					isNumIn = true;
					if(position == 0){
						maps.put(num, 0); 
					}
					nums[i] = null;
					continue;
				}
			}
			if(!isNumIn) {
				if(num != null){
					maps.put(num, maps.get(num) + 1); 
				}
			}
		}
	}
    
    
    
    /**
     * 获取某个彩种的遗漏冷热值
     * @param lotteryKind
     * @return
     */
    public Object[] getOmmitAndHotCold(ELotteryKind lotteryKind,String bizSystem){
    	Object [] result = new Object[3];
    	List<Map<String, Integer>> hotColds = null;
    	List<Map<String, Integer>> ommits = null;
    	
        Date nowStartDate = DateUtil.getNowStartTime();
        Date now = new Date();
        List<LotteryCode> lotteryCodes = this.getLotteryCodesByKindAndDate(lotteryKind.name(), nowStartDate, now,bizSystem);
    	LotteryKindService lotteryKindService = (LotteryKindService)ApplicationContextUtil.getBean(lotteryKind.getServiceClassName());
    	ommits = lotteryKindService.getOmmit(lotteryCodes);
    	hotColds = lotteryKindService.getHotCold(lotteryCodes);
    	
    	result[0] = ConstantUtil.DWR_AJAX_RESULT_OK;
    	result[1] = ommits;
    	result[2] = hotColds;
        return result;
    }
    
    /**
     * 获取某个彩种的遗漏冷热值
     * @param lotteryKind
     * @return
     */
    public List<Map<String, Integer>> getOmmitAndHotCold(OmmitAndHotColdQuery query){
    	LotteryKindService lotteryKindService = (LotteryKindService)ApplicationContextUtil.getBean(query.getLotteryKind().getServiceClassName());
    	//根据彩种获取对应的实现（目前只实现快三的）
    	List<Map<String, Integer>> resArrarysMap = lotteryKindService.getOmmitAndHotColdData(query);
        return resArrarysMap;
    }
    
    /**
     * 走势图查询展示的vo
     * @param zstQueryVo
     * @return
     */
    public List<ZstVo> getZSTByCondition(ZstQueryVo zstQueryVo,String bizSystem){
    	List<ZstVo> zstVos = new ArrayList<ZstVo>(); //走势图结果集
    	ZstVo zstVo = null;
    	List<LotteryCodeXy> lotteryCodes = lotteryCodeXyMapper.getZSTByCondition(zstQueryVo,bizSystem);
    	int totalExpectNum = lotteryCodes.size();
    	int extendNums = zstQueryVo.getExtendNums();
    	int realExpectNum = 0;
    	if(totalExpectNum > extendNums){
        	realExpectNum = totalExpectNum - extendNums;
    	}else{
    		realExpectNum = totalExpectNum;
    	}
    	List<LotteryCodeXy> subLotteryCodeXys = null;
    	List<LotteryCode> subLotteryCodes = null;

    	LotteryKindService lotteryKindService = (LotteryKindService)ApplicationContextUtil.getBean(zstQueryVo.getLotteryKind().getServiceClassName());

    	//遍历真实的期数
    	for(int i = (realExpectNum - 1); i >= 0;i--){
    		zstVo = new ZstVo();
    		LotteryCodeXy currentLotteryCode = lotteryCodes.get(i);
    		zstVo.setExpect(currentLotteryCode.getLotteryNum());
    		zstVo.setOpenCodes(currentLotteryCode.getOpencodeStr());
    		subLotteryCodeXys = lotteryCodes.subList(i, totalExpectNum);
    		subLotteryCodes = new ArrayList<LotteryCode>();
    		for(LotteryCodeXy lotteryCodeXy:subLotteryCodeXys){
    			LotteryCode lotteryCode = new LotteryCode();
    			BeanUtilsExtends.copyProperties(lotteryCode, lotteryCodeXy);
    			subLotteryCodes.add(lotteryCode);
    		}
    		zstVo.setOmitMap(lotteryKindService.getOmmit(subLotteryCodes));
        	zstVos.add(zstVo);
    	}
		return zstVos;
    }
    
    /**
     * 查询规定几期号码
     * @param zstQueryVo
     * @return
     */
    public List getZSTQuery(ZstQueryVo zstQueryVo,String bizSystem){
    	List list = null;
    	//Integer nearExpectNums = zstQueryVo.getNearExpectNums() - zstQueryVo.getExtendNums();
    	Integer nearExpectNums = zstQueryVo.getNearExpectNums();
    	List<LotteryCodeXy> lotteryCodeXys = lotteryCodeXyMapper.getZSTQuery(zstQueryVo,bizSystem);
    	List<LotteryCode> lotteryCodes = new ArrayList<LotteryCode>();
    	for(LotteryCodeXy lotteryCodeXy:lotteryCodeXys){
			LotteryCode lotteryCode = new LotteryCode();
			BeanUtilsExtends.copyProperties(lotteryCode, lotteryCodeXy);
			lotteryCodes.add(lotteryCode);
		}
    	List<LhcOpenCode> zstVos = new ArrayList<LhcOpenCode>(); //开奖历史
    	if(zstQueryVo.getLotteryKind().getCode().equals("XYLHC")){
    		for(LotteryCode lotteryCode:lotteryCodes){
        		LhcOpenCode lhcOpenCode = new LhcOpenCode();
        		lhcOpenCode.setCode1(Integer.parseInt(lotteryCode.getNumInfo1()));
        		lhcOpenCode.setCode2(Integer.parseInt(lotteryCode.getNumInfo2()));
        		lhcOpenCode.setCode3(Integer.parseInt(lotteryCode.getNumInfo3()));
        		lhcOpenCode.setCode4(Integer.parseInt(lotteryCode.getNumInfo4()));
        		lhcOpenCode.setCode5(Integer.parseInt(lotteryCode.getNumInfo5()));
        		lhcOpenCode.setCode6(Integer.parseInt(lotteryCode.getNumInfo6()));
        		lhcOpenCode.setCode7(Integer.parseInt(lotteryCode.getNumInfo7()));
        		lhcOpenCode.setLotteryNum(lotteryCode.getLotteryNum());
        		lhcOpenCode.setKjtime(lotteryCode.getKjtime());
        		zstVos.add(lhcOpenCode);
        	}
        	return zstVos;	
    	}
    	
    	
    	if(zstQueryVo.getLotteryKind().getCode().contains("KS")){
    		list = OmmitUntil.getKsOmmit(lotteryCodes);
    	}else if(zstQueryVo.getLotteryKind().getCode().contains("SSC")){
    		list = OmmitUntil.getSscOmmit(lotteryCodes);
    	}else if(zstQueryVo.getLotteryKind().getCode().contains("SYXW")){
    		list = OmmitUntil.getSyxwOmmit(lotteryCodes);
    	}else if(zstQueryVo.getLotteryKind().getCode().contains("FC")){
    		list = OmmitUntil.getFfcOmmit(lotteryCodes);
    	}else if(zstQueryVo.getLotteryKind().getCode().contains("DPC")){
    		list = OmmitUntil.getDpcOmmit(lotteryCodes);
    	}else if(zstQueryVo.getLotteryKind().getCode().contains("PK10")){
    		list = OmmitUntil.getPK10Ommit(lotteryCodes);
    	}else{
    		list = new ArrayList();
    	}
    	
    	int size = list.size();
    	
    	if(nearExpectNums.compareTo(size) < 0){
    		int subInt = size-nearExpectNums ;
    		List subList = list.subList(subInt, size);
    		return subList;
    	}else{
    		return list;
    	}
		
    }

    

    
    /**
     * 分页查找奖金期号表
     * @param query
     * @param page
     * @return
     */
	public Page getAllLotteryCodeByQueryPage(LotteryCodeQuery query, Page page) {
		page.setTotalRecNum(lotteryCodeXyMapper.getAllLotteryCodesByQueryPageCount(query));
		List<LotteryCodeXy> lotteryCodes = lotteryCodeXyMapper.getAllLotteryCodesByQueryPage(query,page.getStartIndex(),page.getPageSize());
		for(LotteryCodeXy lotteryCode : lotteryCodes){
			lotteryCode.setCodesStr(lotteryCode.getOpencodeStr());
			lotteryCode.setLotteryTypeDes(ELotteryKind.valueOf(lotteryCode.getLotteryName()).getDescription());
		}
		page.setPageContent(lotteryCodes);
    	return page;
	}
	
	/**
     * 查找某个彩种已经记录的最新10条开奖记录
     * @param lotteryKind
     * @return
     */
	public List<LotteryCode> getNearestTenLotteryCode(String lotteryKind,String bizSystem){
    	return lotteryCodeXyMapper.getNearestLotteryCode(lotteryKind,bizSystem, 10);
    }
	
	/**
	 * 查找某个彩种已经记录的最新600条开奖记录
	 * @param lotteryKind
	 * @return
	 */
	public List<LotteryCodeXy> getNearestSixHundredLotteryCode(String lotteryKind,String bizSystem){
		return lotteryCodeXyMapper.getNearestLotteryCodeXy(lotteryKind,bizSystem, ConstantUtil.REDIS_CODE_MAX_RECORD);
	}
	
	
    /**
     * 查找某个彩种最后开奖期号
     * @param query
     * @return
     */
    public List<LotteryCode> getYesterdayLastLotteryNum(LotteryCodeQuery query){
    	Date nowDate = new Date();
	 	Calendar cal = Calendar.getInstance();
        cal.setTime(nowDate);
    	cal.set(Calendar.DATE, cal.get(Calendar.DATE) - 1);
    	//LotteryCodeQuery有特殊处理 00:00 23:59
    	query.setStartime(cal.getTime());
    	query.setEndtime(cal.getTime());
    	return lotteryCodeXyMapper.getYesterdayLastLotteryNum(query);
    }
    
    
    /**
     * 列出所有的数据，用于同步到redis
     * @param query
     * @return
     */
    public List<LotteryCodeXy> getAllLotteryCodesXyByQuery(LotteryCodeXyQuery query){
    	return lotteryCodeXyMapper.getAllLotteryCodesXyByQuery(query);
    }
    
    /**
     * 分页查找奖金期号表
     * @param query
     * @param page
     * @return
     */
	public Page getAllLotteryCodeXyByQueryPage(LotteryCodeXyQuery query, Page page) {
		page.setTotalRecNum(lotteryCodeXyMapper.getAllLotteryCodesXyByQueryPageCount(query));
		List<LotteryCodeXy> lotteryCodesXy = lotteryCodeXyMapper.getAllLotteryCodesXyByQueryPage(query,page.getStartIndex(),page.getPageSize());
		page.setPageContent(lotteryCodesXy);
    	return page;
	}
	
 
}
