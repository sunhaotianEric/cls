package com.team.lottery.service.codetrend;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.team.lottery.service.lotterykindplay.CodeTrendService;

/**
 * 28走势图
 * @author Administrator
 */
@Service("xYEBCodeTrendServiceImpl")
public class XYEBCodeTrendServiceImpl extends CodeTrendService{

	@Override
	public String getPlaykindContent(String openCode) {
		//和值、大小、单双、色波、大小单双、极值
		String[] ocs = openCode.split(",");
		Integer[] ocsIn = new Integer[ocs.length];
		Integer sum = 0;
		for (int i = 0; i < ocs.length; i++) {
			ocsIn[i] = Integer.parseInt(ocs[i]);
			sum += ocsIn[i];
		}
		String str = "";
		str += sum+",";
		String dx = "";//大小
		String ds = "";//单双
		if (sum>13) {
			dx = "大";
			str += "大,";
		}else {
			dx = "小";
			str += "小,";
		}
		if (sum%2==0) {
			ds = "双";
			str += "双,";
		}else {
			ds = "单";
			str += "单,";
		}
		str += getBoSe(sum+"")+","+dx+ds+",";
		if (sum<5) {
			str += "极小";
		}else if (sum>22) {
			str += "极大";
		}else {
			str += "--";
		}
		return str;
	}

	@Override
	public String getNumPosition(String openCode, String lastNumPosition) {
		String[] ocs = openCode.split(",");
		Integer sum = 0;
		for (int i = 0; i < ocs.length; i++) {
			sum += Integer.parseInt(ocs[i]);
		}
		String str = "";
		if (StringUtils.isBlank(lastNumPosition)) { //第一期数据
			for (int j = 0; j < 28; j++) {
				if (sum.intValue()==j) {
					str += "0,";
				}else {
					str += "1,";
				}
			}
		}else {//非第一期数据需要以第一期数据为根据
			String[] lastNumPositions = lastNumPosition.split(",");
			for (int i = 0; i < lastNumPositions.length; i++) {
				if (i==sum.intValue()) {
					str += "0,";
				}else {
					Integer num_position = Integer.parseInt(lastNumPositions[i])+1;//相应的位置遗漏值+1
					str += num_position+",";
				}
			}
		}
		str = str.substring(0,str.length()-1);
		return str;
	}
	
	/**
	 * 获取波色
	 * @param codeStr
	 * @return
	 */
	public String getBoSe(String codeStr){
		String hongboCodes = "3,6,9,12,15,18,21,24";
		String lvboCodes =   "1,4,7,10,16,19,22,25";
		String lanboCodes = "2,5,8,11,17,20,23,26";
		String[] hongboStr = hongboCodes.split(",");
		 String[] lanboStr = lanboCodes.split(",");
		 String[] lvboStr = lvboCodes.split(",");
		for(String c:hongboStr){
			 if(c.equals(codeStr)){
				 return "红波"; 
			 }
		 }
		 for(String c:lanboStr){
			 if(c.equals(codeStr)){
				 return "蓝波"; 
			 }
		 }
		 for(String c:lvboStr){
			 if(c.equals(codeStr)){
				 return "绿波"; 
			 }
		 }
		 return "无波";
	}

}
