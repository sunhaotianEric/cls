package com.team.lottery.service.codeplan.ssc;

import java.util.Random;

import org.springframework.stereotype.Service;

import com.team.lottery.service.lotterykindplay.LotterykindPlayCodeplanService;
import com.team.lottery.vo.CodePlan;

/**
 * 总和大小
 * @author Administrator
 *
 */
@Service("ssc_codeplan_zhdxServiceImpl")
public class ZhdxServiceImpl extends LotterykindPlayCodeplanService{

	@Override
	public String generateCodes() {
		String codes = "";
		int sum = 0;
		for (int i = 0; i < 5; i++) {
			int num = new Random().nextInt(10);
			sum += num;
		}
		if (sum>22) {
			codes = "大|4";
		}else {
			codes = "小|3";
		}
		return codes;
	}

	@Override
	public boolean getProstate(CodePlan codePlan, String openCode) {
		String codes = codePlan.getCodes();
		String codes2 = codes.split("\\|")[1];
		String[] ocs = openCode.split(",");
		Integer ocs_sum = 0;
		for (int i = 0; i < ocs.length; i++) {
			ocs_sum += Integer.parseInt(ocs[i]);
		}
		if ((codes2.equals("4")&&(ocs_sum>22))||(codes2.equals("3")&&(ocs_sum<23))) {
			return true;
		}
		return false;
	}

	@Override
	public int getCodeNum() {
		return 1;
	}

	@Override
	public String showCode(CodePlan codePlan) {
		String codes = codePlan.getCodes();
		String codes2 = codes.split("\\|")[0];
		return codes2;
	}

}
