package com.team.lottery.service.lotterykindplay;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;

import com.team.lottery.service.LotteryWinService;
import com.team.lottery.vo.LotteryCode;

public abstract class LotteryKindPlayService {

	// 正则表达式
	// 只能是一位数字的正则表达式,即0-9
	public static final String ONLY_ONE_NUMBER = "^\\d{1}$";
	// 两位数字的正则表达式,即01-11
	public static final String ONLY_ONE_NUMBER_SYXW = "^(0[1-9]|1\\d|[1])$";
	// 只能是一位数字的正则表达式,即01-10
	public static final String ONLY_ONE_NUMBER_PK10 = "^(0[1-9]|1[0])$";
	public static String ONLY_ONE_NUMBER_LHC = "^(0[1-9]|[1-4]\\d)$";
	@Autowired
	public LotteryWinService lotteryWinService;
	
	
	/**
	 * 中奖处理
	 * 
	 * @param lotteryCode
	 *            (开奖号码)
	 * @param order
	 *            (需要处理的订单) 返回中奖金额
	 */
	public abstract BigDecimal prostateDeal(LotteryCode lotteryCode, String code, BigDecimal awardModel, Integer codeMultiple, Integer orderMultiple, String lotteryModel);

	/**
	 * 投注的彩种玩法的号码校验
	 * 
	 * @param codes
	 */
	public abstract boolean lotteryPlayKindCodesCheck(String codes);

	/**
	 * 获取投注的数目
	 * 
	 * @param codes
	 * @return
	 */
	public abstract Integer getCathecticCount(String codes);

	/**
	 * 六合彩
	 * 
	 * @param lotteryCode
	 * @param codes
	 * @param bizSystem
	 * @param kindPlay
	 * @param lotteryMoney
	 * @return
	 * @throws Exception
	 */
	public BigDecimal prostateDealToLhc(LotteryCode lotteryCode, String codes, String bizSystem, String kindPlay, BigDecimal lotteryMoney) throws Exception {
		return null;
	}

	/**
	 * 幸运28专用
	 * 
	 * @param lotteryCode
	 * @param codes
	 * @param bizSystem
	 * @param kindPlay
	 * @param lotteryMoney
	 * @return
	 * @throws Exception
	 */
	public BigDecimal prostateDealToXyeb(LotteryCode lotteryCode, String codes, String bizSystem, String kindPlay, BigDecimal lotteryMoney) throws Exception {
		return null;
	}
}
