package com.team.lottery.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.mapper.lotterycontrolconfig.LotteryControlConfigMapper;
import com.team.lottery.vo.LotteryControlConfig;

@Service("lotteryControlConfiService")
public class LotteryControlConfiService {

	@Autowired
	private LotteryControlConfigMapper lotteryControlConfigMapper;
	
	public int insertSelective(LotteryControlConfig record){
		return lotteryControlConfigMapper.insertSelective(record);
	}
	/**
	 * 业务系统盈利模式查询
	 * @param record
	 * @return
	 */
	public List<LotteryControlConfig> getAllLotteryControlConfig(LotteryControlConfig record){
		return lotteryControlConfigMapper.getAllLotteryControlConfig(record);
	}
	
	/**
	 * 业务系统盈利模式查询
	 * @param record
	 * @return
	 */
	public List<LotteryControlConfig> getAllConfigs(LotteryControlConfig record){
		return lotteryControlConfigMapper.getAllConfigs(record);
	}
	
	
	/**
	 * 修改业务系统盈利模式
	 * @return
	 */
	public Integer saveLotteryControlConfig(LotteryControlConfig record){
		return lotteryControlConfigMapper.updateByPrimaryKeySelective(record);
	}
	
	/**
	 * 根据ID查询数据
	 * @param id
	 * @return
	 */
	public LotteryControlConfig queryLotteryControlConfigByid(Integer id){
		return lotteryControlConfigMapper.selectByPrimaryKey(id);
	}
	
	/**
	 * 根据业务系统 查询数据
	 * @param record
	 * @return
	 */
	public LotteryControlConfig queryLotteryControlConfigBybizSystem(LotteryControlConfig record){
		return lotteryControlConfigMapper.queryLotteryControlConfigBybizSystem(record);
	}
}
