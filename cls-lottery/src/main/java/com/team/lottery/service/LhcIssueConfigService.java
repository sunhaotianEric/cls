package com.team.lottery.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.mapper.lhcissueconfig.LhcIssueConfigMapper;
import com.team.lottery.vo.LhcIssueConfig;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryIssue;

@Service("lhcIssueConfigService")
public class LhcIssueConfigService {
	
	@Autowired
	private LhcIssueConfigMapper lhcIssueConfigMapper;
	
	
    public int deleteByPrimaryKey(Long id){
    	return lhcIssueConfigMapper.deleteByPrimaryKey(id);
    }

    public int insertSelective(LhcIssueConfig record){
    	return lhcIssueConfigMapper.insertSelective(record);
    }

    public LhcIssueConfig selectByPrimaryKey(Long id){
    	return lhcIssueConfigMapper.selectByPrimaryKey(id);
    }

    public int updateByPrimaryKeySelective(LhcIssueConfig record){
    	return lhcIssueConfigMapper.updateByPrimaryKeySelective(record);
    }

    
    public LhcIssueConfig getLhcIssueConfig(String lotteryName){
    	return lhcIssueConfigMapper.getLhcIssueConfig(lotteryName);
    }
    
    /**
     * 返回封装后的lotteryIssue
     * @return
     */
    public LotteryIssue getIssue(String lotteryName){
    	LhcIssueConfig lhcIssueConfig=lhcIssueConfigMapper.getLhcIssueConfig(lotteryName);
    	
    	LotteryIssue lotteryIssue = new LotteryIssue();
    	lotteryIssue.setLotteryType(lotteryName);
    	Date nowDate =new Date();
    	SimpleDateFormat sdf =new SimpleDateFormat("yyyy-MM-dd");
    	String nowDateStr = sdf.format(nowDate);
    	String lastEndTime = sdf.format(lhcIssueConfig.getLastEndTime());
    	//跟上期是同一个时间端，处当前刚刚开奖时候
    	if(nowDateStr.equals(lastEndTime)){
    		//处于封盘状态
    		//if(nowDate.compareTo(lhcIssueConfig.getLastEndTime())>0){
    			lotteryIssue.setIsClose(true);
    			lotteryIssue.setLotteryNum(lhcIssueConfig.getNextExpect());
        		lotteryIssue.setSpecialTime(1);
        		lotteryIssue.setTimeDifference(3600L);
        		lotteryIssue.setEndtime(lhcIssueConfig.getLastEndTime());
    		//}
    		
    	//当前时间大于当期时间
    	}else if(nowDate.compareTo(lhcIssueConfig.getNextEndTime())>=0){
    		lotteryIssue.setIsClose(true);
    		lotteryIssue.setSpecialTime(1);
    		lotteryIssue.setTimeDifference(3600L);
    		lotteryIssue.setLotteryNum(lhcIssueConfig.getNextExpect());
    		lotteryIssue.setEndtime(lhcIssueConfig.getNextEndTime());
    	//正常时候，时间倒计时
    	}else{
    		 //计算时间差值
            long timeDiff = lhcIssueConfig.getNextEndTime().getTime() - nowDate.getTime();
            //毫秒转成秒
            lotteryIssue.setTimeDifference(timeDiff/1000);
        	lotteryIssue.setLotteryNum(lhcIssueConfig.getNextExpect());
        	lotteryIssue.setSpecialTime(1);
        	if(lhcIssueConfig.getIsClose()==1){
        		lotteryIssue.setIsClose(true);
        	}else{
        		lotteryIssue.setIsClose(false);	
        	}
        	
        	lotteryIssue.setEndtime(lhcIssueConfig.getNextEndTime());
    	}
    	
    	//业务系统封盘，不管什么状态就是封盘了
//    	BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(bizSystem);
//    	if(bizSystemConfigVO!=null&&bizSystemConfigVO.getIsClose()!=null){
//    		if(bizSystemConfigVO.getIsClose()==1){
//        		lotteryIssue.setIsClose(true);
//        	}
//    	}
    	
    	return lotteryIssue;
    }
    
    /**
     * 更新期号时间，根据开奖时间来判断
     */
    public void updateDataByOpenTime(LotteryCode lotteryCode){
    	LhcIssueConfig lhcIssueConfig=lhcIssueConfigMapper.getLhcIssueConfig(lotteryCode.getLotteryName());
    	int nextExpect = Integer.parseInt(lhcIssueConfig.getNextExpect());
    	int openExpect = Integer.parseInt(lotteryCode.getLotteryNum());
    	//开奖期号大于等于当期期号,防止多线程期号执行顺序错乱
    	if(openExpect>=nextExpect){
			//香港六合彩处理方式
			if(ELotteryKind.LHC.name().equals(lotteryCode.getLotteryName())){
				lhcIssueConfig.setLastEndTime(lhcIssueConfig.getNextEndTime());
				lhcIssueConfig.setLastExpect(lotteryCode.getLotteryNum());
				Date nextTime = this.getNextTime(lhcIssueConfig.getLastEndTime(), lhcIssueConfig.getNextEndTime().getHours(), lhcIssueConfig.getNextEndTime().getMinutes());
				lhcIssueConfig.setNextEndTime(nextTime);
				lhcIssueConfig.setNextExpect(new SimpleDateFormat("yyyyMMdd").format(nextTime));
				lhcIssueConfig.setUpdateTime(new Date());
				this.updateByPrimaryKeySelective(lhcIssueConfig);
			}else if(ELotteryKind.AMLHC.name().equals(lotteryCode.getLotteryName())){
				//澳门六合彩直接在上次开奖的日期上加1
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(lhcIssueConfig.getNextEndTime());
				lhcIssueConfig.setLastEndTime(calendar.getTime());
				lhcIssueConfig.setLastExpect(String.valueOf(openExpect));
				calendar.add(Calendar.DAY_OF_WEEK,1);
				Date nextTime = calendar.getTime();
				lhcIssueConfig.setNextEndTime(nextTime);
				lhcIssueConfig.setNextExpect(String.valueOf(openExpect+1));
				lhcIssueConfig.setUpdateTime(new Date());
				this.updateByPrimaryKeySelective(lhcIssueConfig);
			}
    	}
    	
    }
    
    /**
     * 获取下一期封盘时间
     * @param openTime
     * @return
     */
    private Date getNextTime(Date openTime,int hour,int mi){
    	
    	  //判断开奖日期是星期几
    	  Calendar cal = Calendar.getInstance();
          cal.setTime(openTime);
          int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
          if (w < 0)
              w = 0;

          switch (w) {
			case 0:
				cal.add(Calendar.DAY_OF_WEEK, 2);
				break;
			case 1:
				cal.add(Calendar.DAY_OF_WEEK, 1);
				break;
			case 2:
				cal.add(Calendar.DAY_OF_WEEK, 2);
				break;
			case 3:
				cal.add(Calendar.DAY_OF_WEEK, 1);
				break;
			case 4:
				cal.add(Calendar.DAY_OF_WEEK, 2);
				break;
			case 5:
				cal.add(Calendar.DAY_OF_WEEK, 1);
				break;
			case 6:
				cal.add(Calendar.DAY_OF_WEEK, 3);
				break;

		}
        /*  cal.set(Calendar.HOUR_OF_DAY, hour);
          cal.set(Calendar.MINUTE, mi);
          cal.set(Calendar.SECOND, 0);*/
		return cal.getTime();
    }
    
    public static void main(String[] args) throws ParseException {
		/*SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date openDate = sdf.parse("2017-03-26 18:03:34");
		System.out.println(openDate.getMinutes());*/
		Date nextTime = new LhcIssueConfigService().getNextTime(new Date(), 0, 0);
		System.out.println(nextTime);
	}
}
