package com.team.lottery.service.codeplan.xyeb;

import java.util.Random;

import org.springframework.stereotype.Service;

import com.team.lottery.service.lotterykindplay.LotterykindPlayCodeplanService;
import com.team.lottery.vo.CodePlan;

/**
 * 特码
 * @author Administrator
 *
 */
@Service("xyeb_codeplan_tmServiceImpl")
public class TmServiceImpl extends LotterykindPlayCodeplanService{

	@Override
	public String generateCodes() {
		int num1 = new Random().nextInt(10);
		int num2 = new Random().nextInt(10);
		int num3 = new Random().nextInt(10);
		int sum = num1 + num2 + num3;
		if (sum<10) {
			return "0"+sum;
		}
		return sum+"";
	}

	@Override
	public boolean getProstate(CodePlan codePlan, String openCode) {
		String codes = codePlan.getCodes();
		String[] ocs = openCode.split(",");
		Integer codesum = 0;
		if (codes.startsWith("0")) {
			codesum = Integer.parseInt(codes.substring(1));
		}else {
			codesum = Integer.parseInt(codes);
		}
		if ((Integer.parseInt(ocs[0])+Integer.parseInt(ocs[1])+Integer.parseInt(ocs[2]))==codesum) {
			return true;
		}
		return false;
	}

	@Override
	public int getCodeNum() {
		return 1;
	}

	@Override
	public String showCode(CodePlan codePlan) {
		String codes = codePlan.getCodes();
		return codes;
	}

}
