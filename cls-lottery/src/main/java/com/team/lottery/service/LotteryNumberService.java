package com.team.lottery.service;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.mapper.lotterynumber.LotteryNumberMapper;
import com.team.lottery.vo.LotteryNumber;

@Service("lotteryNumberService")
public class LotteryNumberService {

	@Autowired
	private LotteryNumberMapper lotteryNumberMapper;
	
    /**
     * 查找某个可开奖的彩种的当前期号
     * @param lotteryKind
     * @param date
     * @return
     */
    public LotteryNumber getNewExpectByLotteryKind(@Param("lotteryKind")String lotteryKind,@Param("date")Date date){
    	return lotteryNumberMapper.getNewExpectByLotteryKind(lotteryKind, date);
    }
    
    /**
     * 查找某个时间段的可开奖的期号
     * @param lotteryKind
     * @param date
     * @return
     */
    public List<LotteryNumber> getAllLotteryNumbersByCondition(@Param("lotteryKind")String lotteryKind,@Param("startDate")Date startDate,@Param("endDate")Date endDate){
    	return lotteryNumberMapper.getAllLotteryNumbersByCondition(lotteryKind, startDate, endDate);
    }
}
