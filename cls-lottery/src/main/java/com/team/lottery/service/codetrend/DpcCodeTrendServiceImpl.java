package com.team.lottery.service.codetrend;

import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.team.lottery.service.lotterykindplay.CodeTrendService;

/**
 * 低频彩：福彩3D、排列3等
 * @author Administrator
 */
@Service("dpcCodeTrendServiceImpl")
public class DpcCodeTrendServiceImpl extends CodeTrendService{

	@Override
	public String getPlaykindContent(String openCode) {
		//和值、跨度、百位、十位、个位、形态
		String[] ocs = openCode.split(",");
		Integer[] ocsIn = new Integer[ocs.length];
		String str = "";
		Integer sum = 0;
		TreeSet<Integer> set = new TreeSet<Integer>();
		for (int i = 0; i < ocsIn.length; i++) {
			ocsIn[i] = Integer.parseInt(ocs[i]);
			set.add(ocsIn[i]);
			sum += ocsIn[i];
		}
		str += sum +","+(set.last()-set.first())+",";
		str += getDxds(ocsIn[0])+getDxds(ocsIn[1])+getDxds(ocsIn[2]);
		str += getXingTai(set);
		return str;
	}

	@Override
	public String getNumPosition(String openCode, String lastNumPosition) {
		String[] ocs = openCode.split(",");
		String str = "";
		if (StringUtils.isBlank(lastNumPosition)) { //第一期数据
			for (int i = 0; i < ocs.length; i++) {
				for (int j = 0; j < 10; j++) {
					if (Integer.valueOf(ocs[i]).intValue()==j) {
						str += "0,";
					}else {
						str += "1,";
					}
				}
			}
		}else {//非第一期数据需要以第一期数据为根据
			String[] lastNumPositions = lastNumPosition.split(",");
			for (int i = 0; i < lastNumPositions.length; i++) {
				// i/10:ocs数组的位数；
				// ocs[i/10]:ocs数组元素
				// Integer.valueOf(ocs[i/10]).intValue()：ocs数组元素整型值
				// i%10：lastNumPosition中每10位循环一次设置位置信息
				if ((i%10)==Integer.valueOf(ocs[i/10]).intValue()) {
					str += "0,";
				}else {
					Integer num_position = Integer.parseInt(lastNumPositions[i])+1;//相应的位置遗漏值+1
					str += num_position+",";
				}
			}
		}
		str = str.substring(0,str.length()-1);
		return str;
	}
	
	/**
	 * 获取形态：组三：有2个数值相同；组六：3个数值各自不同；豹子：3个数值全部相同
	 * @param set
	 * @return
	 */
	public String getXingTai(TreeSet<Integer> set){
		if (set.size()==1) {
			return "豹子";
		}else if (set.size()==2) {
			return "组三";
		}else if (set.size()==3) {
			return "组六";
		}
		return "";
	}
	
	/**
	 * 大小单双
	 * @param i
	 * @return
	 */
	public String getDxds(Integer i){
		String str = "";
		if (i>4) {
			str += "大";
		}else {
			str += "小";
		}
		if (i%2==0) {
			str += "双,";
		}else {
			str += "单,";
		}
		return str;
	}

}
