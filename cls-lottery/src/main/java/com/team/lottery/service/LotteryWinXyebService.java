package com.team.lottery.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.lotterywinxyeb.LotteryWinXyebMapper;
import com.team.lottery.vo.LotteryWinXyeb;

@Service("lotteryWinXyebService")
public class LotteryWinXyebService {
	private static Logger logger = LoggerFactory.getLogger(LotteryWinXyebService.class);
	
	@Autowired
	public LotteryWinXyebMapper lotteryWinXyebMapper;
	
	public int deleteByPrimaryKey(Integer id){
		return lotteryWinXyebMapper.deleteByPrimaryKey(id);
	}

	public int insert(LotteryWinXyeb record){
		return lotteryWinXyebMapper.insert(record);
	}

	public int insertSelective(LotteryWinXyeb record){
		return lotteryWinXyebMapper.insertSelective(record);
	}

	public LotteryWinXyeb selectByPrimaryKey(Integer id){
		return lotteryWinXyebMapper.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(LotteryWinXyeb record){
		return lotteryWinXyebMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(LotteryWinXyeb record){
		return lotteryWinXyebMapper.updateByPrimaryKey(record);
	}
	
	public List<LotteryWinXyeb> getLotteryWinXyebByLotteryTypeProtype(LotteryWinXyeb query){
		return lotteryWinXyebMapper.getLotteryWinXyebByLotteryTypeProtype(query);
	}
	@Transactional(readOnly=false,rollbackFor=Exception.class)
	public int bathUpdateByPrimaryKey(LotteryWinXyeb[] records){
		
		for(LotteryWinXyeb record:records ){
			lotteryWinXyebMapper.updateByPrimaryKeySelective(record);
		}
		return 1;
	}
	
	public Page getLotteryWinXyebByPage(LotteryWinXyeb query,Page page){
		
		try {
			page.setPageContent(lotteryWinXyebMapper.getLotteryWinXyebByPage(query, page.getStartIndex(), page.getPageSize()));
			page.setTotalRecNum(lotteryWinXyebMapper.getLotteryWinXyebCount(query));
		} catch (Exception e) {
			System.err.println(logger);
			logger.info("查询失败---"+e);
		}
		 return page;
	}
	
	  /**
	    * 根据种类获取玩法赔率
	    * @param query
	    * @return
	    */
	public List<LotteryWinXyeb> getLotteryWinXyebByKind(LotteryWinXyeb query){
		return lotteryWinXyebMapper.getLotteryWinXyebByKind(query);		
	}
	
	  /**
	    * 根据种类获取玩法最低赔率
	    * @param query
	    * @return
	    */
	public List<LotteryWinXyeb> getMinLotteryWinXyebByKind(LotteryWinXyeb query){
		return lotteryWinXyebMapper.getMinLotteryWinXyebByKind(query,query.getCodeArr());		
	}
	  /**
	    * 初始化
	    * @param query
	    * @return
	    */
	public void initLotteryXyebCode(LotteryWinXyeb query){
		lotteryWinXyebMapper.initLotteryXyebCode(query);
	}
	
	
}
