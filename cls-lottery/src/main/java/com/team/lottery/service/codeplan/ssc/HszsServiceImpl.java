package com.team.lottery.service.codeplan.ssc;

import java.util.Random;

import org.springframework.stereotype.Service;

import com.team.lottery.service.lotterykindplay.LotterykindPlayCodeplanService;
import com.team.lottery.vo.CodePlan;

/**
 * 后三组三
 * @author Administrator
 *
 */
@Service("ssc_codeplan_hszsServiceImpl")
public class HszsServiceImpl extends LotterykindPlayCodeplanService{

	@Override
	public String generateCodes() {
		String codes = "";
		for (int i = 0; i < 3; i++) {
			int num = new Random().nextInt(10);
			if (i==2) {
				codes += num;
			}else {
				codes += num+",";
			}
		}
		return codes;
	}

	@Override
	public boolean getProstate(CodePlan codePlan, String openCode) {
		String[] cps = codePlan.getCodes().split(",");
		String[] ocs = openCode.substring(4, 9).split(",");
		String oc = cps[cps.length-1]+cps[cps.length-2]+cps[cps.length-3];
		int count = 0;
		for (int i = 0; i < ocs.length; i++) {
			if (oc.contains(ocs[i])) {
				count++;
			}
			oc = oc.replaceFirst(ocs[i], "");
		}
		if (count==ocs.length) {
			return true;
		}
		return false;
	}

	@Override
	public int getCodeNum() {
		return 3;
	}

	@Override
	public String showCode(CodePlan codePlan) {
		String codes = codePlan.getCodes();
		return codes;
	}

}
