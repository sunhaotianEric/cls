package com.team.lottery.service.codeplan.xyeb;

import java.util.Random;

import org.springframework.stereotype.Service;

import com.team.lottery.service.lotterykindplay.LotterykindPlayCodeplanService;
import com.team.lottery.vo.CodePlan;

/**
 * 混合大小
 * @author Administrator
 *
 */
@Service("xyeb_codeplan_hhdxServiceImpl")
public class HhdxServiceImpl extends LotterykindPlayCodeplanService{

	@Override
	public String generateCodes() {
		int num1 = new Random().nextInt(10);
		int num2 = new Random().nextInt(10);
		int num3 = new Random().nextInt(10);
		int sum = num1 + num2 + num3;
		if (sum<14) {
			return "小|3";
		}
		return "大|4";
	}

	@Override
	public boolean getProstate(CodePlan codePlan, String openCode) {
		String codes = codePlan.getCodes();
		String codes2 = codes.split("\\|")[1];
		String[] ocs = openCode.split(",");
		if (((Integer.parseInt(ocs[0])+Integer.parseInt(ocs[1])+Integer.parseInt(ocs[2]))<14&&codes2.equals("3"))||
				((Integer.parseInt(ocs[0])+Integer.parseInt(ocs[1])+Integer.parseInt(ocs[2]))>13&&codes2.equals("4"))) {
			return true;
		}
		return false;
	}

	@Override
	public int getCodeNum() {
		return 1;
	}

	@Override
	public String showCode(CodePlan codePlan) {
		String codes = codePlan.getCodes();
		String codes2 = codes.split("\\|")[0];
		return codes2;
	}

}
