package com.team.lottery.service.codetrend;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.team.lottery.service.lotterykindplay.CodeTrendService;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.ShengXiaoNumberUtil;

/**
 * 六合彩走势图
 * @author Administrator
 */
@Service("lHCCodeTrendServiceImpl")
public class LHCCodeTrendServiceImpl extends CodeTrendService{
	public static final Logger log = LoggerFactory.getLogger(LHCCodeTrendServiceImpl.class);

	@Override
	public String getPlaykindContent(String openCode) {
		//正码、特码、生肖、大小/单双、家禽野兽、尾数、和大小/单双、总和、总单双、总大小
		String[] ocs = openCode.split(",");
		for (int i = 0; i < ocs.length; i++) {
			if (ocs[i].startsWith("0")) {
				ocs[i] = ocs[i].substring(1);
			}
		}
		String str = "";
		str += ocs[0]+" "+ocs[1]+" "+ocs[2]+" "+ocs[3]+" "+ocs[4]+" "+ocs[5]+",";
		str += ocs[6]+",";
		Date kjtime = new Date();
		try {
			str += getShengxiao(Integer.parseInt(ocs[6]), kjtime)+",";
		} catch (NumberFormatException e) {
			log.info("六合彩走势图生成玩法内容获取生肖异常,"+e);
		} catch (Exception e) {
			log.info("六合彩走势图生成玩法内容获取生肖异常,"+e);
		}
		if (ocs[6].equals("49")) {
			str += "和/和,";
		}else {
			if (Integer.parseInt(ocs[6])>24) {
				str += "大/";
			}else {
				str += "小/";
			}
			if (Integer.parseInt(ocs[6])%2==0) {
				str += "双,";
			}else {
				str += "单,";
			}
		}
		try {
			str += getJiaQingOrYeShou(Integer.parseInt(ocs[6]), kjtime)+",";
		} catch (NumberFormatException e) {
			log.info("六合彩走势图生成玩法内容获取家禽异常,"+e);
		} catch (Exception e) {
			log.info("六合彩走势图生成玩法内容获取家禽异常,"+e);
		}
		str += Integer.parseInt(ocs[6])%10 +",";
		str += getHeDaOrHeXiao(Integer.parseInt(ocs[6]))+"/"+getHeDanOrHeShuang(Integer.parseInt(ocs[6]))+",";
		str += (Integer.parseInt(ocs[0])+Integer.parseInt(ocs[1])+Integer.parseInt(ocs[2])+Integer.parseInt(ocs[3])+Integer.parseInt(ocs[4])+
				Integer.parseInt(ocs[5])+Integer.parseInt(ocs[6]))+",";
		try {
			str += (ShengXiaoNumberUtil.decideZongHeDan_Shuang(ocs) == true?"总单":"总双")+","+(ShengXiaoNumberUtil.decideZongHeDa_Xiao(ocs) == true?"总大":"总小");
		} catch (Exception e) {
			log.info("六合彩走势图生成玩法内容获取总单双、大小异常,"+e);
		}
		return str;
	}

	@Override
	public String getNumPosition(String openCode, String lastNumPosition) {
		return "";
	}
	
	/**
	 * 特码合单双 49为和
	 * @param code7
	 * @return
	 */
	public String getHeDanOrHeShuang(Integer code7) {	
		if(code7 != null){
			if(code7==49){
				return "和";	
			}
			return ShengXiaoNumberUtil.decideHeDanOrHeShuang(code7.toString())==true?"合单":"合双";	
		}else{
			return "";
		}
	}
	
	/**
	 * 特码合大小 49为和
	 * @param code7
	 * @return
	 */
	public String getHeDaOrHeXiao(Integer code7){
		if(code7 != null){
			if(code7==49){
				return "和";	
			}
			return ShengXiaoNumberUtil.decideHeDaOrHeXiao(code7.toString())==true?"合大":"合小";	
		}else{
			return "";
		}
	}
	
	/**
	 * 特码家禽 野兽 49为和
	 * @param code1
	 * @param kjtime
	 * @return
	 * @throws Exception 
	 */
	public String getJiaQingOrYeShou(Integer code7,Date kjtime) throws Exception{
		if(code7 != null && kjtime != null){
			if(code7==49){
				return "和";	
			}
			 Date festivalStartDate = new SimpleDateFormat("yyyy-MM-dd").parse(SystemConfigConstant.springFestivalStartDate);
			 Date chuyiDate = DateUtil.addDaysToDate(festivalStartDate, 1);
			if(kjtime.before(chuyiDate) && (new Date().compareTo(festivalStartDate) > 0)){
				return ShengXiaoNumberUtil.decideJiaQinOrYeShou(code7.toString(),1)==true?"家禽":"野兽";
			}else{
				return ShengXiaoNumberUtil.decideJiaQinOrYeShou(code7.toString(),0)==true?"家禽":"野兽";
			}
				
		}else{
			return "";
		}
	}
	
	/**
	 * 获取生肖
	 * @param code1
	 * @param kjtime
	 * @return
	 * @throws Exception
	 */
	public String getShengxiao(Integer code1,Date kjtime) throws Exception{
		String[] animalArr = new String[]{"鼠", "牛", "虎", "兔", "龙", "蛇", "马", "羊", "猴", "鸡", "狗", "猪"};
		Integer animal = 0;
		if(code1 != null && kjtime != null){
			 Date festivalStartDate = new SimpleDateFormat("yyyy-MM-dd").parse(SystemConfigConstant.springFestivalStartDate);
			 Date chuyiDate = DateUtil.addDaysToDate(festivalStartDate, 1);
			if(kjtime.before(chuyiDate) && (new Date().compareTo(festivalStartDate) > 0)){
				animal = ShengXiaoNumberUtil.retuenShengXiaoCode(code1.toString(),1);	
			}else{
				animal = ShengXiaoNumberUtil.retuenShengXiaoCode(code1.toString(),0);		
			}
		}
		return animalArr[animal-1];
	}

}
