package com.team.lottery.service.codeplan.pk10;

import java.util.Random;

import org.springframework.stereotype.Service;

import com.team.lottery.service.lotterykindplay.LotterykindPlayCodeplanService;
import com.team.lottery.vo.CodePlan;

/**
 * 冠军定位
 * @author Administrator
 */
@Service("pk10_gjdwServiceImpl")
public class GjdwServiceImpl extends LotterykindPlayCodeplanService{

	@Override
	public String generateCodes() {
		int num = new Random().nextInt(11);
		if (num<10) {
			return "0"+num;
		}
		return num+"";
	}

	@Override
	public boolean getProstate(CodePlan codePlan, String openCode) {
		String codes = codePlan.getCodes();
		if (codes.startsWith("0")) {
			codes = codes.substring(1);
		}
		String open_code = openCode.split(",")[0];
		if (codes.equals(open_code)) {
			return true;
		}
		return false;
	}

	@Override
	public int getCodeNum() {
		return 1;
	}

	@Override
	public String showCode(CodePlan codePlan) {
		return codePlan.getCodes();
	}

}
