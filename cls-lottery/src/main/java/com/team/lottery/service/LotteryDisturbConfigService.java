package com.team.lottery.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.LotteryDisturbConfigQuery;
import com.team.lottery.mapper.lotterydisturbconfig.LotteryDisturbConfigMapper;
import com.team.lottery.vo.LotteryDisturbConfig;

@Service("lotteryDisturbConfigService")
public class LotteryDisturbConfigService {
	private static Logger logger = LoggerFactory.getLogger(LotteryDisturbConfigService.class);
	@Autowired
	private LotteryDisturbConfigMapper lotteryDisturbConfigMapper;
	
	
	public int deleteByPrimaryKey(Long id) {
		return lotteryDisturbConfigMapper.deleteByPrimaryKey(id);
	}

	public int insertSelective(LotteryDisturbConfig record) {
    	return lotteryDisturbConfigMapper.insertSelective(record);
    }

	public LotteryDisturbConfig selectByPrimaryKey(Long id) {
    	return lotteryDisturbConfigMapper.selectByPrimaryKey(id);
    }

	public int updateByPrimaryKeySelective(LotteryDisturbConfig record) {
    	return lotteryDisturbConfigMapper.updateByPrimaryKeySelective(record);
    }
	
	/**
	 * 根据用户名查询有效的下单干扰配置项
	 * @return
	 */
	public List<LotteryDisturbConfig> queryLotteryDisturbConfigsByCondition(LotteryDisturbConfigQuery query) {
		return lotteryDisturbConfigMapper.queryLotteryDisturbConfigsByCondition(query);
	}
	
	/**
	 * 查询所有有效的干扰下单配置项
	 * @return
	 */
	public List<LotteryDisturbConfig> queryAllLotteryDisturbConfigs() {
		return lotteryDisturbConfigMapper.queryAllLotteryDisturbConfigs();
	}

}
