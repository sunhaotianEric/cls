package com.team.lottery.service.codeplan.ssc;

import java.util.Random;

import org.springframework.stereotype.Service;

import com.team.lottery.service.lotterykindplay.LotterykindPlayCodeplanService;
import com.team.lottery.vo.CodePlan;

/**
 * 万位大小
 * @author Administrator
 *
 */
@Service("ssc_codeplan_wwdxServiceImpl")
public class WwdxServiceImpl extends LotterykindPlayCodeplanService{

	@Override
	public String generateCodes() {
		int num = new Random().nextInt(10);
		if (num>4) {
			return "大|4";
		}
		return "小|3";
	}

	@Override
	public boolean getProstate(CodePlan codePlan, String openCode) {
		String codes = codePlan.getCodes();
		String code2 = codes.split("\\|")[1];
		String open_code = openCode.split(",")[0];
		Integer open_codeInteger = Integer.parseInt(open_code);
		if ((code2.equals("4")&&(open_codeInteger>4))||(code2.equals("3")&&(open_codeInteger<5))) {
			return true;
		}
		return false;
	}

	@Override
	public int getCodeNum() {
		return 1;
	}

	@Override
	public String showCode(CodePlan codePlan) {
		String codes = codePlan.getCodes();
		String code2 = codes.split("\\|")[0];
		return code2;
	}

}
