package com.team.lottery.service.codeplan.syxw;

import java.util.Random;

import org.springframework.stereotype.Service;

import com.team.lottery.service.lotterykindplay.LotterykindPlayCodeplanService;
import com.team.lottery.vo.CodePlan;

/**
 * 一中一
 * @author Administrator
 *
 */
@Service("syxw_codeplan_yzyServiceImpl")
public class YzyServiceImpl extends LotterykindPlayCodeplanService{

	@Override
	public String generateCodes() {
		int num1 = new Random().nextInt(11)+1;
		if (num1>9) {
			return num1 + "";
		}else {
			return "0" + num1;
		}
	}

	@Override
	public boolean getProstate(CodePlan codePlan, String openCode) {
		String codes = codePlan.getCodes();
		if (codes.startsWith("0")) {
			codes = codes.substring(1);
		}
		String[] openCodes = openCode.split(",");
		for (int i = 0; i < openCodes.length; i++) {
			if (codes.equals(openCodes[i])) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int getCodeNum() {
		return 1;
	}

	@Override
	public String showCode(CodePlan codePlan) {
		String codes = codePlan.getCodes();
		return codes;
	}

}
