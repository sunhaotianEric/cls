package com.team.lottery.service.codeplan.ks;

import java.util.Random;

import org.springframework.stereotype.Service;

import com.team.lottery.service.lotterykindplay.LotterykindPlayCodeplanService;
import com.team.lottery.vo.CodePlan;

/**
 * 第三球定位
 * @author Administrator
 *
 */
@Service("ks_codeplan_dsqdwServiceImpl")
public class DsqdwServiceImpl extends LotterykindPlayCodeplanService{

	@Override
	public String generateCodes() {
		int num1 = new Random().nextInt(6)+1;
		return num1+"";
	}

	@Override
	public boolean getProstate(CodePlan codePlan, String openCode) {
		String codes = codePlan.getCodes();
		String openCode_num1 = openCode.split(",")[2];
		if (codes.equals(openCode_num1)) {
			return true;
		}
		return false;
	}

	@Override
	public int getCodeNum() {
		return 1;
	}

	@Override
	public String showCode(CodePlan codePlan) {
		return codePlan.getCodes();
	}

}
