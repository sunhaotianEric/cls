package com.team.lottery.service.lotterykind;

import java.util.List;
import java.util.Map;

import com.team.lottery.extvo.OmmitAndHotColdQuery;
import com.team.lottery.vo.LotteryCode;

public interface LotteryKindService {

	/**
	 * 各类彩种的投注号码校验
	 * @param codes
	 * @return
	 */
    public abstract boolean lotteryCodesCheckByTopKind(String codes,String playKind);
    
    /**
     * 开奖号码校验
     * @param codes
     * @return
     */
    public abstract boolean lotteryCodesCheckByOpenCode(String codes);
    
    /**
     * 计算各个彩种的遗漏值(当前遗漏,目前只计算当前遗漏值)
     * @return
     */
    public abstract List<Map<String,Integer>> getOmmit(List<LotteryCode> lotteryCodes);

    /**
     * 当前前十期的号码冷热
     * @param lotteryCodes
     * @return
     */
    public abstract List<Map<String,Integer>> getHotCold(List<LotteryCode> lotteryCodes);
    
    
    /**
     * 获取彩种玩法的遗漏冷热数据
     * @return
     */
    public abstract List<Map<String,Integer>> getOmmitAndHotColdData(OmmitAndHotColdQuery query);
    

}
