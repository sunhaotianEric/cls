package com.team.lottery.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.vo.LhcIssueConfig;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotterySwitch;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.enums.EAfterNumberType;
import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.extvo.LotteryIssueAdjustVo;
import com.team.lottery.extvo.LotteryIssueQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.lotteryissue.LotteryIssueMapper;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.system.cache.LotteryIssueCache;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.DateUtils;
import com.team.lottery.vo.LotteryIssue;


@Service("lotteryIssueService")
public class LotteryIssueService {

	private static Logger log = LoggerFactory.getLogger(LotteryIssueService.class);
	//加载今天跟明天的期数返回的条数
	private static final int RETURN_ISSUE_COUNT = 20;

	@Autowired
	private LotteryCodeService lotteryCodeService;

	@Autowired
	private LotteryIssueMapper lotteryIssueMapper;
	@Autowired
	private LhcIssueConfigService lhcIssueConfigService;

	public int updateByPrimaryKeySelective(LotteryIssue record){
		return lotteryIssueMapper.updateByPrimaryKeySelective(record);
	}

	public int insertSelective(LotteryIssue record){
		return lotteryIssueMapper.insertSelective(record);
	}

	public int deleteByPrimaryKey(Integer id){
		return lotteryIssueMapper.deleteByPrimaryKey(id);
	}
	public LotteryIssue selectByPrimaryKey(Integer id){
		return lotteryIssueMapper.selectByPrimaryKey(id);
	}

	/**
	 * 查询当前某个彩种正在开奖的期号,缓存中查询
	 * @param lotteryKind
	 * @param date
	 * @return
	 */
	public LotteryIssue getNewExpectByLotteryKind(String lotteryKind, Date date) {
		LotteryIssue res = null;
		List<LotteryIssue> lotteryIssues = LotteryIssueCache.getInstance().getLotteryIssueByKind(lotteryKind);
		if(CollectionUtils.isNotEmpty(lotteryIssues)) {
			for(LotteryIssue lotteryIssue : lotteryIssues) {
				if(lotteryIssue.getBegintime().getTime() <= date.getTime() && lotteryIssue.getEndtime().getTime() > date.getTime()) {
					//返回新的对象以免导致缓存对象被修改
					res = new LotteryIssue();
					BeanUtils.copyProperties(lotteryIssue, res);
					return res;
				}
			}
		}
		return res;
	}


	/**
	 * 查询当前某个彩种未来3期开奖的期号,缓存中查询
	 * @param lotteryKind
	 * @param date
	 * @return
	 */
	public List<LotteryIssue> get3NewExpectByLotteryKind(String lotteryKind, Date date,int size) {
		List<LotteryIssue> res = new ArrayList<LotteryIssue>();
		List<LotteryIssue> lotteryIssues = LotteryIssueCache.getInstance().getLotteryIssueByKind(lotteryKind);
		if(CollectionUtils.isNotEmpty(lotteryIssues)) {
			for(LotteryIssue lotteryIssue : lotteryIssues) {
				if(lotteryIssue.getEndtime().getTime() > date.getTime()) {
					//返回新的对象以免导致缓存对象被修改
					LotteryIssue r = new LotteryIssue();
					BeanUtils.copyProperties(lotteryIssue, r);
					res.add(r);
					if (res.size()==size) {
						return res;
					}
				}
			}
		}
		return res;
	}

	/**
	 * 查询当前某个彩种最后开奖时间
	 * @param lotteryKind
	 * @param
	 * @return
	 */
	public LotteryIssue getLastExpectByLotteryKind(String lotteryKind) {
		LotteryIssue res = null;
		List<LotteryIssue> lotteryIssues = LotteryIssueCache.getInstance().getLotteryIssueByKind(lotteryKind);
		if(CollectionUtils.isNotEmpty(lotteryIssues)) {
			res = lotteryIssues.get(0);
			for(LotteryIssue lotteryIssue : lotteryIssues) {
				if(Integer.parseInt(lotteryIssue.getLotteryNum()) > Integer.parseInt(res.getLotteryNum())) {
					res = lotteryIssue;
				}
			}
		}
		return res;
	}

	/**
	 * 加载预售期号(查询当天第一期)，缓存中查询
	 * @return
	 */
	public LotteryIssue getPresellExpectByLotteryKind(String lotteryKind) {
		LotteryIssue res = null;
		List<LotteryIssue> lotteryIssues = LotteryIssueCache.getInstance().getLotteryIssueByKind(lotteryKind);
		if(CollectionUtils.isNotEmpty(lotteryIssues)) {
			LotteryIssue lotteryIssue = null;
			//韩国1.5分彩预售期号特殊处理
			if(lotteryKind.equals("HGFFC")){
				for(LotteryIssue issue : lotteryIssues){
					//韩国1.5分彩预售期号为201期
					if(issue.getLotteryNum().equals("201")){
						lotteryIssue = issue;
						break;
					}
				}
			}else if(lotteryKind.equals("XJPLFC")){
				for(LotteryIssue issue : lotteryIssues){
					//新加坡2分彩预售期号为181期
					if(issue.getLotteryNum().equals("181")){
						lotteryIssue = issue;
						break;
					}
				}
			}else{
				//第一条就是第一期
				lotteryIssue = lotteryIssues.get(0);
			}

			//返回新的对象以免导致缓存对象被修改
			res = new LotteryIssue();
			if(lotteryIssue !=null){
				BeanUtils.copyProperties(lotteryIssue, res);
			}
			return res;
		}
		return res;
	}

	/**
	 * 加载对应编号的期号 ,缓存中查询
	 * @param lotteryKind
	 * @param lotteryNum
	 * @return
	 */
	public LotteryIssue getExpectByLotteryNum(String lotteryKind, String lotteryNum){
		LotteryIssue res = null;
		List<LotteryIssue> lotteryIssues = LotteryIssueCache.getInstance().getLotteryIssueByKind(lotteryKind);
		if(CollectionUtils.isNotEmpty(lotteryIssues)) {
			for(LotteryIssue lotteryIssue : lotteryIssues) {
				if(lotteryIssue.getLotteryNum().equals(lotteryNum)) {
					//返回新的对象以免导致缓存对象被修改
					res = new LotteryIssue();
					BeanUtils.copyProperties(lotteryIssue, res);
					return res;
				}
			}
		}
		return res;
	}

	/**
	 * 查找某个时间段的彩种的所有期号,缓存中查询
	 * @param lotteryKind
	 * @param
	 * @return
	 */
	public List<LotteryIssue> getAllExpectsByCondition(String lotteryKind,Date startDate,Date endDate) {
		List<LotteryIssue> res = new ArrayList<LotteryIssue>();
		List<LotteryIssue> lotteryIssues = LotteryIssueCache.getInstance().getLotteryIssueByKind(lotteryKind);
		if(CollectionUtils.isNotEmpty(lotteryIssues)) {
			for(LotteryIssue lotteryIssue : lotteryIssues) {
				boolean isInCondition = false;
				if(startDate != null) {
					if(lotteryIssue.getBegintime().getTime() >= startDate.getTime()) {
						isInCondition = true;
					}
					if(endDate != null) {
						if(lotteryIssue.getEndtime().getTime() > endDate.getTime()) {
							isInCondition = false;
						}
					}
				} else {
					isInCondition = true;
					if(endDate != null) {
						if(lotteryIssue.getEndtime().getTime() > endDate.getTime()) {
							isInCondition = false;
						}
					}
				}
				if(isInCondition) {
					//返回新的对象以免导致缓存对象被修改
					LotteryIssue lotteryIssueCopy = new LotteryIssue();
					BeanUtils.copyProperties(lotteryIssue, lotteryIssueCopy);
					res.add(lotteryIssueCopy);
				}

			}
		}
		return res;
	}

	/**
	 * 加载所有的彩种的投注期号
	 * @param lotteryKind
	 * @return
	 */
	public List<LotteryIssue> getExpectsAllByKind(String lotteryKind) {
		return lotteryIssueMapper.getExpectsAllByKind(lotteryKind);
	}

	/**
	 * 分页查找开奖时间表
	 * @param query
	 * @param page
	 * @return
	 */
	public Page getAllLotteryIssueByQueryPage(LotteryIssueQuery query, Page page) {
//		log.info("Startime:" + DateUtil.getDate(query.getStartime(), "yyyy-MM-dd HH:mm:ss") + "---Endtime:" + DateUtil.getDate(query.getEndtime(), "yyyy-MM-dd HH:mm:ss"));
		page.setTotalRecNum(lotteryIssueMapper.getAllLotteryIssuesByQueryPageCount(query));
		List<LotteryIssue> lotteryIssues = lotteryIssueMapper.getAllLotteryIssuesByQueryPage(query,page.getStartIndex(),page.getPageSize());
//		for(LotteryCode lotteryCode : lotteryCodes){
//			lotteryCode.setCodesStr(this.getCodesStr(lotteryCode));
//		}
		page.setPageContent(lotteryIssues);
		return page;
	}

	/**
	 * 根据服务器时间,查找最新待开奖期号及距离结束的时间差
	 * @param lotteryKind
	 * @param
	 * @return
	 * @throws ParseException
	 */
	public LotteryIssue getNewExpectAndDiffTimeByLotteryKind(ELotteryKind lotteryKind){
		if(lotteryKind == null){
			return null;
		}
		//获取最后开奖时间
		LotteryIssue lastLotteryIssue =  getLastExpectByLotteryKind(lotteryKind.getCode());
		String currentSFM = DateUtils.getDateToStrByFormat(new Date(), "HH:mm:ss");  //当前服务器时间时分秒
		String configDateControl = lotteryKind.getDateControlStr();
		Date queryDate = DateUtils.getDateByStrFormat(configDateControl + " " + currentSFM, "yyyy-MM-dd HH:mm:ss");
		Date firstQueryDate = DateUtils.getDateByStrFormat(configDateControl + " " + currentSFM, "yyyy-MM-dd HH:mm:ss");
		//显示开奖期号的日期
		Date kaijiangDate = new Date();
		Long appendTimeDiff = 0l;
		//新疆时时彩时间特殊处理
		if(lotteryKind.equals(ELotteryKind.XJSSC)){
			Date kuaduDateStart = DateUtils.getDateByStrFormat(configDateControl + " " + "00:00:00", "yyyy-MM-dd HH:mm:ss");
			String currentHms = DateUtils.getDateToStrByFormat(lastLotteryIssue.getEndtime(), "HH:mm:ss");
			Date kuaduDateEnd = DateUtils.getDateByStrFormat(configDateControl + " " + currentHms, "yyyy-MM-dd HH:mm:ss");

			if(kuaduDateStart.getTime() <= queryDate.getTime() && queryDate.getTime() < kuaduDateEnd.getTime()){
				queryDate = DateUtil.addDaysToDate(queryDate, 1);
				//超过当天，但期号要显示前一天的
				kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, -1);
			}else{
				//kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
			}
		} else if(lotteryKind.equals(ELotteryKind.JLFFC) //新增三分时时彩、五分时时彩、三分快三、五分快三、十分时时彩
				|| lotteryKind.equals(ELotteryKind.SFSSC) || lotteryKind.equals(ELotteryKind.WFSSC)
				|| lotteryKind.equals(ELotteryKind.SFKS) || lotteryKind.equals(ELotteryKind.WFKS) || lotteryKind.equals(ELotteryKind.WFSYXW) || lotteryKind.equals(ELotteryKind.SHFSSC) || lotteryKind.equals(ELotteryKind.SFSYXW)){
			//分分彩最后一期期号要显示后一天的
			//Date kuaduDateStart = DateUtils.getDateByStrFormat(configDateControl + " " + "23:59:59", "yyyy-MM-dd HH:mm:ss");
			Date kuaduDateStart = lastLotteryIssue.getEndtime();
			if(kuaduDateStart.getTime() <= queryDate.getTime()){
				kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
			}
		}else if(lotteryKind.equals(ELotteryKind.TWWFC)){
			//台湾5分彩最后一期期号要显示后一天的
			//Date kuaduDateStart = DateUtils.getDateByStrFormat(configDateControl + " " + "23:55:00", "yyyy-MM-dd HH:mm:ss");
			Date kuaduDateStart = lastLotteryIssue.getEndtime();
			if(kuaduDateStart.getTime() <= queryDate.getTime()){
				kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
			}
		} else if(lotteryKind.equals(ELotteryKind.XJPLFC)) {
			//新加坡2分彩最后一期未配置时间
			//Date kuaduDateStart = DateUtils.getDateByStrFormat(configDateControl + " " + "23:59:35", "yyyy-MM-dd HH:mm:ss");
			Date kuaduDateStart = lastLotteryIssue.getEndtime();
			Date kuaduDateEnd = DateUtils.getDateByStrFormat(configDateControl + " " + "23:59:59", "yyyy-MM-dd HH:mm:ss");
			if(kuaduDateStart.getTime() <= queryDate.getTime()){
				//补上未配置时间的差值
				appendTimeDiff = kuaduDateEnd.getTime() - queryDate.getTime();
				//置为当天最开始时间
				queryDate = DateUtils.getDateByStrFormat(configDateControl + " " + "00:00:00", "yyyy-MM-dd HH:mm:ss");
				//时间设置为后面一天，以便计算基准日期加上一天
				kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
			}
		} else if(lotteryKind.equals(ELotteryKind.HGFFC)) {
			//韩国1.5分彩最后一期未配置时间
			//Date kuaduDateStart = DateUtils.getDateByStrFormat(configDateControl + " " + "23:59:59", "yyyy-MM-dd HH:mm:ss");
			Date kuaduDateStart = lastLotteryIssue.getEndtime();
			Date kuaduDateEnd = DateUtils.getDateByStrFormat(configDateControl + " " + "23:59:59", "yyyy-MM-dd HH:mm:ss");
			if(kuaduDateStart.getTime() <= queryDate.getTime()){
				//补上未配置时间的差值
				appendTimeDiff = kuaduDateEnd.getTime() - queryDate.getTime();
				//置为当天最开始时间
				queryDate = DateUtils.getDateByStrFormat(configDateControl + " " + "00:00:00", "yyyy-MM-dd HH:mm:ss");
				//时间设置为后面一天，以便计算基准日期加上一天
				kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
			}
		} /*else if(lotteryKind.equals(ELotteryKind.BJKS)) {
        	//北京快3最后一期未配置时间
        	//Date kuaduDateStart = DateUtils.getDateByStrFormat(configDateControl + " " + "23:48:05", "yyyy-MM-dd HH:mm:ss");
        	Date kuaduDateStart = lastLotteryIssue.getEndtime();
        	Date kuaduDateEnd = DateUtils.getDateByStrFormat(configDateControl + " " + "09:08:05", "yyyy-MM-dd HH:mm:ss");
        	if(kuaduDateStart.getTime() <= queryDate.getTime()){
        		//补上未配置时间的差值
        		appendTimeDiff = kuaduDateEnd.getTime() - queryDate.getTime();
        		//置为当天最开始时间
        		queryDate = DateUtils.getDateByStrFormat(configDateControl + " " + "00:00:00", "yyyy-MM-dd HH:mm:ss");
        		//时间设置为后面一天，以便计算基准日期加上一天
        		kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
        	}
        }*/else if(lotteryKind.equals(ELotteryKind.XYFT)){
			//分分彩最后一期期号要显示后一天的
			//Date kuaduDateStart = DateUtils.getDateByStrFormat(configDateControl + " " + "23:59:59", "yyyy-MM-dd HH:mm:ss");
			Date kuaduDateStart = lastLotteryIssue.getEndtime();
			Date seconddate = DateUtils.getDateByStrFormat(configDateControl + " " + "00:00:00", "yyyy-MM-dd HH:mm:ss");
			if (seconddate.getTime() <= queryDate.getTime()
					&& queryDate.getTime() <= DateUtils.getYesDateTime(kuaduDateStart).getTime()) {
				queryDate = DateUtil.addDaysToDate(queryDate, 1);
			}
		}
		LotteryIssue issue = this.getNewExpectByLotteryKind(lotteryKind.name(), queryDate);
		if(issue != null){
			//计算时间差值
			long timeDiff = issue.getEndtime().getTime() - queryDate.getTime() + appendTimeDiff;
			//毫秒转成秒
			issue.setTimeDifference(timeDiff/1000);

			//期号
			//福彩3D的期号进行特殊处理
			if(issue.getLotteryType().equals(ELotteryKind.FCSDDPC.name())){
				dealExpectNumForFcsdByDate(issue, false);
				//PK10期号特殊处理
			}else if(issue.getLotteryType().equals(ELotteryKind.BJPK10.name())
			){
				dealExpectNumForPk10ByDate(issue);
			}else if(issue.getLotteryType().equals(ELotteryKind.HGFFC.name())){
				dealExpectNumForHgffcByDate(issue, kaijiangDate);
			}/*else if(issue.getLotteryType().equals(ELotteryKind.BJKS.name())){
            	dealExpectNumForBjksByDate(issue, kaijiangDate);
            }*/else if(issue.getLotteryType().equals(ELotteryKind.XJPLFC.name())){
				dealExpectNumForXjplfcByDate(issue, kaijiangDate);
			}else if(issue.getLotteryType().equals(ELotteryKind.TWWFC.name())){
				dealExpectNumForTwwfcByDate(issue, kaijiangDate);
			}else if(lotteryKind.equals(ELotteryKind.XYEB)){
				dealExpectNumForXyebByDate(issue, kaijiangDate);
			}else if (lotteryKind.equals(ELotteryKind.XYFT)) {
				issue.setLotteryNumCount(issue.getLotteryNum());
				issue.setLotteryNumCount(issue.getLotteryNum());
				Date kuaduDateStart = lastLotteryIssue.getEndtime();
				Date seconddate = DateUtils.getDateByStrFormat(configDateControl + " " + "00:00:00", "yyyy-MM-dd HH:mm:ss");
				if (seconddate.getTime() <= firstQueryDate.getTime()
						&& firstQueryDate.getTime() <= DateUtils.getYesDateTime(kuaduDateStart).getTime()) {
					issue.setLotteryNum(DateUtils.getDateToStrByFormat(DateUtils.getYesDateTime(kaijiangDate), "yyyyMMdd") + issue.getLotteryNum());
				}else {
					issue.setLotteryNum(DateUtils.getDateToStrByFormat(kaijiangDate, "yyyyMMdd") + issue.getLotteryNum());

				}
			}
			else{
				issue.setLotteryNumCount(issue.getLotteryNum());
				issue.setLotteryNum(DateUtils.getDateToStrByFormat(kaijiangDate, "yyyyMMdd") + issue.getLotteryNum());
			}
			if(lotteryKind.equals(ELotteryKind.XYLHC)){
				issue.setSpecialTime(1);
			}

			//春节期间，属于福彩，体彩的彩种需要处理为预售中（自己彩种  幸运分分彩，幸运快三，极速PK10,台湾五分彩，新加坡两分彩，韩国1.5分彩不需要加此控制）
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date springFestivalStartDate = null;
			Date springFestivalEndDate = null;
			try {
				springFestivalStartDate = sdf.parse(SystemConfigConstant.springFestivalStartDate+" 00:00:00");
				springFestivalEndDate = sdf.parse(SystemConfigConstant.springFestivalEndDate+" 23:59:59");
			} catch (ParseException e) {
				log.error("转化春节时间失败！",e);
			}  //当前服务器时间时分秒
			Date nowDate = new Date();
			if(springFestivalStartDate.before(nowDate)&&springFestivalEndDate.after(nowDate)){
				if(ELotteryKind.JYKS.name().equals(issue.getLotteryType())||ELotteryKind.JLFFC.name().equals(issue.getLotteryType())
						||ELotteryKind.SFSSC.name().equals(issue.getLotteryType())||ELotteryKind.WFSSC.name().equals(issue.getLotteryType())
						||ELotteryKind.JSPK10.name().equals(issue.getLotteryType())||ELotteryKind.TWWFC.name().equals(issue.getLotteryType())
						||ELotteryKind.XJPLFC.name().equals(issue.getLotteryType())||ELotteryKind.HGFFC.name().equals(issue.getLotteryType())
						||ELotteryKind.SFKS.name().equals(issue.getLotteryType())||ELotteryKind.WFKS.name().equals(issue.getLotteryType())
						||ELotteryKind.WFSYXW.name().equals(issue.getLotteryType()) || ELotteryKind.SHFSSC.name().equals(issue.getLotteryType())
						||ELotteryKind.SFSYXW.name().equals(issue.getLotteryType()) ||ELotteryKind.SFPK10.name().equals(issue.getLotteryType())
						||ELotteryKind.WFPK10.name().equals(issue.getLotteryType()) ||ELotteryKind.SHFPK10.name().equals(issue.getLotteryType())
						|| ELotteryKind.XYFT.name().equals(issue.getLotteryType()) || ELotteryKind.LCQSSC.name().equals(issue.getLotteryType())
						|| ELotteryKind.JSSSC.name().equals(issue.getLotteryType()) || ELotteryKind.BJSSC.name().equals(issue.getLotteryType())
						|| ELotteryKind.GDSSC.name().equals(issue.getLotteryType()) || ELotteryKind.SCSSC.name().equals(issue.getLotteryType())
						|| ELotteryKind.SHSSC.name().equals(issue.getLotteryType()) || ELotteryKind.SDSSC.name().equals(issue.getLotteryType())

				){
					return issue;
				}else{
					issue.setSpecialExpect(2);
					return issue;
				}
			}

			//查询不到，进入预售期号处理
		}else{

			//判断香港六合彩，特殊处理
			if(ELotteryKind.LHC.name().equals(lotteryKind.name())){
				issue =lhcIssueConfigService.getIssue(lotteryKind.name());
				issue.setLotteryType(ELotteryKind.LHC.getCode());
				issue.setLotteryName(ELotteryKind.LHC.getDescription());
				Calendar cal = Calendar.getInstance();
				cal.set(Calendar.HOUR_OF_DAY, 21);
				cal.set(Calendar.MINUTE, 30);
				System.out.println(cal.getTime());
				issue.setEndtime(cal.getTime());
				return issue;
				//判断澳门六合彩，特殊处理
			}else if(ELotteryKind.AMLHC.name().equals(lotteryKind.name())){
					issue =lhcIssueConfigService.getIssue(lotteryKind.name());
					issue.setLotteryType(ELotteryKind.AMLHC.getCode());
					issue.setLotteryName(ELotteryKind.AMLHC.getDescription());
					Calendar cal = Calendar.getInstance();
					cal.set(Calendar.HOUR_OF_DAY, 21);
					cal.set(Calendar.MINUTE, 30);
					System.out.println(cal.getTime());
					issue.setEndtime(cal.getTime());
					return issue;
				}
			issue = this.getPresellExpectByLotteryKind(lotteryKind.name());
			//查询预售期号为空，返回空数据
			if(issue == null) {
				return new LotteryIssue();
			}
			long result = 0l;
			//时间差值,24小时都开奖的猜中处理
			if(lotteryKind.name().equals(ELotteryKind.JLFFC.name())
					|| lotteryKind.name().equals(ELotteryKind.SFSSC.name()) || lotteryKind.name().equals(ELotteryKind.WFSSC.name())
					|| lotteryKind.name().equals(ELotteryKind.SFKS.name()) || lotteryKind.name().equals(ELotteryKind.WFKS.name()) ||
					lotteryKind.name().equals(ELotteryKind.WFSYXW.name()) || lotteryKind.name().equals(ELotteryKind.SHFSSC.name())  ||
					lotteryKind.name().equals(ELotteryKind.WFSYXW.name()) || lotteryKind.name().equals(ELotteryKind.SFPK10.name())
					|| lotteryKind.name().equals(ELotteryKind.WFPK10.name()) || lotteryKind.name().equals(ELotteryKind.SHFPK10.name())
					|| lotteryKind.name().equals(ELotteryKind.LCQSSC.name())
			){
				result += issue.getEndtime().getTime() - DateUtil.getNowStartTimeByStart(queryDate).getTime();
				result += DateUtil.getNowStartTimeByEnd(queryDate).getTime() - queryDate.getTime();
			}else if(lotteryKind.name().equals(ELotteryKind.FCSDDPC.name())){
				result += DateUtil.getNowStartTimeByEnd(queryDate).getTime() - queryDate.getTime() ;
				result += issue.getEndtime().getTime() - issue.getBegintime().getTime();
			}else{
				if(issue.getEndtime().getTime() > queryDate.getTime()){
					result = issue.getEndtime().getTime() - queryDate.getTime();
				}else{
					result += queryDate.getTime() - DateUtil.getNowStartTimeByStart(queryDate).getTime();
					result += DateUtil.getNowStartTimeByEnd(queryDate).getTime() - issue.getEndtime().getTime();
				}
			}
			issue.setTimeDifference(result/1000); //毫秒转成秒
			issue.setLotteryNumCount(issue.getLotteryNum());

			Date endDate = lastLotteryIssue.getEndtime();
			if(lotteryKind.equals(ELotteryKind.CQSSC)){
				kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
			}else if(lotteryKind.equals(ELotteryKind.JXSSC)){
				//Date endDate = DateUtils.getDateByStrFormat("2015-01-01 23:10:40", "yyyy-MM-dd HH:mm:ss");
				if(endDate.getTime() <= queryDate.getTime() && queryDate.getTime() <= DateUtil.getNowStartTimeByEnd(endDate).getTime()){
					kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
				}
			}else if(lotteryKind.equals(ELotteryKind.TJSSC)){
				//Date endDate = DateUtils.getDateByStrFormat("2015-01-13 22:57:25", "yyyy-MM-dd HH:mm:ss");
				if(endDate.getTime() <= queryDate.getTime() && queryDate.getTime() <= DateUtil.getNowStartTimeByEnd(endDate).getTime()){
					kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
				}
			}else if(lotteryKind.equals(ELotteryKind.XJSSC)){

			}else if(lotteryKind.equals(ELotteryKind.GDSYXW)){
				//Date endDate = DateUtils.getDateByStrFormat("2015-01-05 22:58:50", "yyyy-MM-dd HH:mm:ss");
				if(endDate.getTime() <= queryDate.getTime() && queryDate.getTime() <= DateUtil.getNowStartTimeByEnd(endDate).getTime()){
					kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
				}
			}else if(lotteryKind.equals(ELotteryKind.SDSYXW)){
				//Date endDate = DateUtils.getDateByStrFormat("2015-01-06 21:53:20", "yyyy-MM-dd HH:mm:ss");
				if(endDate.getTime() <= queryDate.getTime() && queryDate.getTime() <= DateUtil.getNowStartTimeByEnd(endDate).getTime()){
					kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
				}
			}else if(lotteryKind.equals(ELotteryKind.JXSYXW)){
				//Date endDate = DateUtils.getDateByStrFormat("2015-01-07 22:58:10", "yyyy-MM-dd HH:mm:ss");
				if(endDate.getTime() <= queryDate.getTime() && queryDate.getTime() <= DateUtil.getNowStartTimeByEnd(endDate).getTime()){
					kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
				}
			}else if(lotteryKind.equals(ELotteryKind.FJSYXW)){
				//Date endDate = DateUtils.getDateByStrFormat("2015-01-09 23:05:10", "yyyy-MM-dd HH:mm:ss");
				if(endDate.getTime() <= queryDate.getTime() && queryDate.getTime() <= DateUtil.getNowStartTimeByEnd(endDate).getTime()){
					kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
				}
			}else if(lotteryKind.equals(ELotteryKind.JSKS)){
				//Date endDate = DateUtils.getDateByStrFormat("2015-01-14 22:08:00", "yyyy-MM-dd HH:mm:ss");
				if(endDate.getTime() <= queryDate.getTime() && queryDate.getTime() <= DateUtil.getNowStartTimeByEnd(endDate).getTime()){
					kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
				}
			}else if(lotteryKind.equals(ELotteryKind.AHKS)||lotteryKind.equals(ELotteryKind.BJKS)){
				//Date endDate = DateUtils.getDateByStrFormat("2015-01-18 21:58:10", "yyyy-MM-dd HH:mm:ss");
				if(endDate.getTime() <= queryDate.getTime() && queryDate.getTime() <= DateUtil.getNowStartTimeByEnd(endDate).getTime()){
					kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
				}
			}else if(lotteryKind.equals(ELotteryKind.JLKS)){
				//Date endDate = DateUtils.getDateByStrFormat("2015-01-20 21:59:05", "yyyy-MM-dd HH:mm:ss");
				if(endDate.getTime() <= queryDate.getTime() && queryDate.getTime() <= DateUtil.getNowStartTimeByEnd(endDate).getTime()){
					kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
				}
			}else if(lotteryKind.equals(ELotteryKind.HBKS)){
				//Date endDate = DateUtils.getDateByStrFormat("2015-01-20 21:59:05", "yyyy-MM-dd HH:mm:ss");
				if(endDate.getTime() <= queryDate.getTime() && queryDate.getTime() <= DateUtil.getNowStartTimeByEnd(endDate).getTime()){
					kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
				}
			}else if(lotteryKind.equals(ELotteryKind.JYKS)){
				//Date endDate = DateUtils.getDateByStrFormat("2015-01-24 22:08:40", "yyyy-MM-dd HH:mm:ss");
				if(endDate.getTime() <= queryDate.getTime() && queryDate.getTime() <= DateUtil.getNowStartTimeByEnd(endDate).getTime()){
					kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
				}
			}else if(lotteryKind.equals(ELotteryKind.GXKS)){
				//Date endDate = DateUtils.getDateByStrFormat("2015-01-26 22:08:40", "yyyy-MM-dd HH:mm:ss");
				if(endDate.getTime() <= queryDate.getTime() && queryDate.getTime() <= DateUtil.getNowStartTimeByEnd(endDate).getTime()){
					kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
				}
			}else if(lotteryKind.equals(ELotteryKind.GSKS)){
				//Date endDate = DateUtils.getDateByStrFormat("2015-01-28 22:08:40", "yyyy-MM-dd HH:mm:ss");
				if(endDate.getTime() <= queryDate.getTime() && queryDate.getTime() <= DateUtil.getNowStartTimeByEnd(endDate).getTime()){
					kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
				}
			}else if(lotteryKind.equals(ELotteryKind.SHKS)){
				//Date endDate = DateUtils.getDateByStrFormat("2015-01-20 22:20:59", "yyyy-MM-dd HH:mm:ss");
				if(endDate.getTime() <= queryDate.getTime() && queryDate.getTime() <= DateUtil.getNowStartTimeByEnd(endDate).getTime()){
					kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
				}
			}else if(lotteryKind.equals(ELotteryKind.TWWFC)){
				//Date endDate = DateUtils.getDateByStrFormat("2015-01-20 23:55:00", "yyyy-MM-dd HH:mm:ss");
				if(endDate.getTime() <= queryDate.getTime() && queryDate.getTime() <= DateUtil.getNowStartTimeByEnd(endDate).getTime()){
					kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
				}
			}else if(lotteryKind.equals(ELotteryKind.XYEB)){
				//Date endDate = DateUtils.getDateByStrFormat("2015-01-23 23:54:40", "yyyy-MM-dd HH:mm:ss");
				if(endDate.getTime() <= queryDate.getTime() && queryDate.getTime() <= DateUtil.getNowStartTimeByEnd(endDate).getTime()){
					kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
				}
			}

			//期号
			//福彩3D预售旗号处理
			if(lotteryKind.equals(ELotteryKind.FCSDDPC)){
				//在配置的时间查询不到，需要将期数+1
				dealExpectNumForFcsdByDate(issue, true);
				//PK10期号特殊处理
			} else if(lotteryKind.equals(ELotteryKind.BJPK10)){
				dealExpectNumForPk10ByDate(issue);
				//Date endDate = DateUtils.getDateByStrFormat("2015-01-16 23:56:00", "yyyy-MM-dd HH:mm:ss");
				//在最后一期时间之后，预售期号需要加上44期
				if(queryDate.after(endDate)) {
					Integer expectNum = Integer.parseInt(issue.getLotteryNum());
					expectNum += 44;
					issue.setLotteryNum(String.valueOf(expectNum));
				}
			} else if(lotteryKind.equals(ELotteryKind.HGFFC)){
				dealExpectNumForHgffcByDate(issue, new Date());
			} else if(lotteryKind.equals(ELotteryKind.XJPLFC)){
				dealExpectNumForXjplfcByDate(issue, new Date());
			} else if(lotteryKind.equals(ELotteryKind.TWWFC)){
				dealExpectNumForTwwfcByDate(issue, kaijiangDate);
			}else if(lotteryKind.equals(ELotteryKind.XYEB)){
				dealExpectNumForXyebByDate(issue, kaijiangDate);
			} else if (lotteryKind.equals(ELotteryKind.LCQSSC)) {
				if(endDate.getTime() <= queryDate.getTime() && queryDate.getTime() <= DateUtil.getNowStartTimeByEnd(endDate).getTime()){
					kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
					issue.setLotteryNum(DateUtils.getDateToStrByFormat(kaijiangDate, "yyyyMMdd") + issue.getLotteryNum());
				}else {
					issue.setLotteryNum(DateUtils.getDateToStrByFormat(kaijiangDate, "yyyyMMdd") + issue.getLotteryNum());
				}
			}
			else{
				issue.setLotteryNum(DateUtils.getDateToStrByFormat(kaijiangDate, "yyyyMMdd") + issue.getLotteryNum());
			}
		}
		return issue;

	}


	/**
	 * 根据服务器时间,查找最新3期待开奖期号及距离结束的时间差
	 * @param lotteryKind
	 * @param
	 * @return
	 * @throws ParseException
	 */
	public List<LotteryIssue> get3NewExpectAndDiffTimeByLotteryKind(ELotteryKind lotteryKind){
		if(lotteryKind == null){
			return null;
		}
		//获取最后开奖时间
		LotteryIssue lastLotteryIssue =  getLastExpectByLotteryKind(lotteryKind.getCode());
		String currentSFM = DateUtils.getDateToStrByFormat(new Date(), "HH:mm:ss");  //当前服务器时间时分秒
		String configDateControl = lotteryKind.getDateControlStr();
		Date queryDate = DateUtils.getDateByStrFormat(configDateControl + " " + currentSFM, "yyyy-MM-dd HH:mm:ss");
		//显示开奖期号的日期
		Date kaijiangDate = new Date();
		Long appendTimeDiff = 0l;
		//新疆时时彩时间特殊处理
		if(lotteryKind.equals(ELotteryKind.XJSSC)){
			Date kuaduDateStart = DateUtils.getDateByStrFormat(configDateControl + " " + "00:00:00", "yyyy-MM-dd HH:mm:ss");
			String currentHms = DateUtils.getDateToStrByFormat(lastLotteryIssue.getEndtime(), "HH:mm:ss");
			Date kuaduDateEnd = DateUtils.getDateByStrFormat(configDateControl + " " + currentHms, "yyyy-MM-dd HH:mm:ss");

			if(kuaduDateStart.getTime() <= queryDate.getTime() && queryDate.getTime() < kuaduDateEnd.getTime()){
				queryDate = DateUtil.addDaysToDate(queryDate, 1);
				//超过当天，但期号要显示前一天的
				kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, -1);
			}else{
				//kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
			}
		}else if(lotteryKind.equals(ELotteryKind.JLFFC) //新增三分时时彩、五分时时彩、三分快三、五分快三、十分时时彩
				|| lotteryKind.equals(ELotteryKind.SFSSC) || lotteryKind.equals(ELotteryKind.WFSSC)
				|| lotteryKind.equals(ELotteryKind.SFKS) || lotteryKind.equals(ELotteryKind.WFKS) || lotteryKind.equals(ELotteryKind.WFSYXW)
				|| lotteryKind.equals(ELotteryKind.SHFSSC) || lotteryKind.equals(ELotteryKind.SFSYXW)
				|| lotteryKind.equals(ELotteryKind.SFPK10) || lotteryKind.equals(ELotteryKind.WFPK10) || lotteryKind.equals(ELotteryKind.SHFPK10)
		){
			//分分彩最后一期期号要显示后一天的
			//Date kuaduDateStart = DateUtils.getDateByStrFormat(configDateControl + " " + "23:59:59", "yyyy-MM-dd HH:mm:ss");
			Date kuaduDateStart = lastLotteryIssue.getEndtime();
			if(kuaduDateStart.getTime() <= queryDate.getTime()){
				kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
			}
		}else if(lotteryKind.equals(ELotteryKind.TWWFC)){
			//台湾5分彩最后一期期号要显示后一天的
			//Date kuaduDateStart = DateUtils.getDateByStrFormat(configDateControl + " " + "23:55:00", "yyyy-MM-dd HH:mm:ss");
			Date kuaduDateStart = lastLotteryIssue.getEndtime();
			if(kuaduDateStart.getTime() <= queryDate.getTime()){
				kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
			}
		} else if(lotteryKind.equals(ELotteryKind.XJPLFC)) {
			//新加坡2分彩最后一期未配置时间
			//Date kuaduDateStart = DateUtils.getDateByStrFormat(configDateControl + " " + "23:59:35", "yyyy-MM-dd HH:mm:ss");
			Date kuaduDateStart = lastLotteryIssue.getEndtime();
			Date kuaduDateEnd = DateUtils.getDateByStrFormat(configDateControl + " " + "23:59:59", "yyyy-MM-dd HH:mm:ss");
			if(kuaduDateStart.getTime() <= queryDate.getTime()){
				//补上未配置时间的差值
				appendTimeDiff = kuaduDateEnd.getTime() - queryDate.getTime();
				//置为当天最开始时间
				queryDate = DateUtils.getDateByStrFormat(configDateControl + " " + "00:00:00", "yyyy-MM-dd HH:mm:ss");
				//时间设置为后面一天，以便计算基准日期加上一天
				kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
			}
		} else if(lotteryKind.equals(ELotteryKind.HGFFC)) {
			//韩国1.5分彩最后一期未配置时间
			//Date kuaduDateStart = DateUtils.getDateByStrFormat(configDateControl + " " + "23:59:59", "yyyy-MM-dd HH:mm:ss");
			Date kuaduDateStart = lastLotteryIssue.getEndtime();
			Date kuaduDateEnd = DateUtils.getDateByStrFormat(configDateControl + " " + "23:59:59", "yyyy-MM-dd HH:mm:ss");
			if(kuaduDateStart.getTime() <= queryDate.getTime()){
				//补上未配置时间的差值
				appendTimeDiff = kuaduDateEnd.getTime() - queryDate.getTime();
				//置为当天最开始时间
				queryDate = DateUtils.getDateByStrFormat(configDateControl + " " + "00:00:00", "yyyy-MM-dd HH:mm:ss");
				//时间设置为后面一天，以便计算基准日期加上一天
				kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
			}
		} //else if(lotteryKind.equals(ELotteryKind.BJKS)) {
//    		//北京快3最后一期未配置时间
//    		//Date kuaduDateStart = DateUtils.getDateByStrFormat(configDateControl + " " + "23:48:05", "yyyy-MM-dd HH:mm:ss");
//    		Date kuaduDateStart = lastLotteryIssue.getEndtime();
//    		Date kuaduDateEnd = DateUtils.getDateByStrFormat(configDateControl + " " + "09:08:05", "yyyy-MM-dd HH:mm:ss");
//    		if(kuaduDateStart.getTime() <= queryDate.getTime()){
//    			//补上未配置时间的差值
//    			appendTimeDiff = kuaduDateEnd.getTime() - queryDate.getTime();
//    			//置为当天最开始时间
//    			queryDate = DateUtils.getDateByStrFormat(configDateControl + " " + "00:00:00", "yyyy-MM-dd HH:mm:ss");
//    			//时间设置为后面一天，以便计算基准日期加上一天
//    			kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
//    		}
//    	}
		List<LotteryIssue> issues = new ArrayList<LotteryIssue>();
		//十一选五：二中二 玩法选择未来4期
		if (lotteryKind.getCode().contains("SYXW")) {
			issues = this.get3NewExpectByLotteryKind(lotteryKind.name(), queryDate, 4);
		}else {
			issues = this.get3NewExpectByLotteryKind(lotteryKind.name(), queryDate, 3);
		}
		for (LotteryIssue issue : issues) {
			if(issue != null){
				//计算时间差值
				long timeDiff = issue.getEndtime().getTime() - queryDate.getTime() + appendTimeDiff;
				//毫秒转成秒
				issue.setTimeDifference(timeDiff/1000);

				//期号
				//福彩3D的期号进行特殊处理
				if(issue.getLotteryType().equals(ELotteryKind.FCSDDPC.name())){
					dealExpectNumForFcsdByDate(issue, false);
					//PK10期号特殊处理
				}else if(issue.getLotteryType().equals(ELotteryKind.BJPK10.name())
				){
					dealExpectNumForPk10ByDate(issue);
				}else if(issue.getLotteryType().equals(ELotteryKind.HGFFC.name())){
					dealExpectNumForHgffcByDate(issue, kaijiangDate);
				}/*else if(issue.getLotteryType().equals(ELotteryKind.BJKS.name())){
        			dealExpectNumForBjksByDate(issue, kaijiangDate);
        		}*/else if(issue.getLotteryType().equals(ELotteryKind.XJPLFC.name())){
					dealExpectNumForXjplfcByDate(issue, kaijiangDate);
				}else if(issue.getLotteryType().equals(ELotteryKind.TWWFC.name())){
					dealExpectNumForTwwfcByDate(issue, kaijiangDate);
				}else if(lotteryKind.equals(ELotteryKind.XYEB)){
					dealExpectNumForXyebByDate(issue, kaijiangDate);
				} else{
					issue.setLotteryNumCount(issue.getLotteryNum());
					issue.setLotteryNum(DateUtils.getDateToStrByFormat(kaijiangDate, "yyyyMMdd") + issue.getLotteryNum());
				}
				if(lotteryKind.equals(ELotteryKind.XYLHC)){
					issue.setSpecialTime(1);
				}

				//春节期间，属于福彩，体彩的彩种需要处理为预售中（自己彩种  幸运分分彩，幸运快三，极速PK10,台湾五分彩，新加坡两分彩，韩国1.5分彩不需要加此控制）
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date springFestivalStartDate = null;
				Date springFestivalEndDate = null;
				try {
					springFestivalStartDate = sdf.parse(SystemConfigConstant.springFestivalStartDate+" 00:00:00");
					springFestivalEndDate = sdf.parse(SystemConfigConstant.springFestivalEndDate+" 23:59:59");
				} catch (ParseException e) {
					log.error("转化春节时间失败！",e);
				}  //当前服务器时间时分秒
				Date nowDate = new Date();
				if(springFestivalStartDate.before(nowDate)&&springFestivalEndDate.after(nowDate)){
					if(ELotteryKind.JYKS.name().equals(issue.getLotteryType())||ELotteryKind.JLFFC.name().equals(issue.getLotteryType())
							||ELotteryKind.SFSSC.name().equals(issue.getLotteryType())||ELotteryKind.WFSSC.name().equals(issue.getLotteryType())
							||ELotteryKind.JSPK10.name().equals(issue.getLotteryType())||ELotteryKind.TWWFC.name().equals(issue.getLotteryType())
							||ELotteryKind.XJPLFC.name().equals(issue.getLotteryType())||ELotteryKind.HGFFC.name().equals(issue.getLotteryType())
							||ELotteryKind.SFKS.name().equals(issue.getLotteryType())||ELotteryKind.WFKS.name().equals(issue.getLotteryType())
							||ELotteryKind.WFSYXW.name().equals(issue.getLotteryType()) ||ELotteryKind.SHFSSC.name().equals(issue.getLotteryType())
							||ELotteryKind.SFSYXW.name().equals(issue.getLotteryType()) ||ELotteryKind.SFPK10.name().equals(issue.getLotteryType())
							||ELotteryKind.WFPK10.name().equals(issue.getLotteryType()) ||ELotteryKind.SHFPK10.name().equals(issue.getLotteryType())
					){
						continue;
					}else{
						issue.setSpecialExpect(2);
						continue;
					}
				}

				//查询不到，进入预售期号处理
			}else{

				//判断香港六合彩，特殊处理
				if(ELotteryKind.LHC.name().equals(lotteryKind.name())){
					issue =lhcIssueConfigService.getIssue(lotteryKind.name());
					issue.setLotteryType(ELotteryKind.LHC.getCode());
					issue.setLotteryName(ELotteryKind.LHC.getDescription());
					continue;
					//判断香港六合彩，特殊处理
				}else if(ELotteryKind.AMLHC.name().equals(lotteryKind.name())){
						issue =lhcIssueConfigService.getIssue(lotteryKind.name());
						issue.setLotteryType(ELotteryKind.AMLHC.getCode());
						issue.setLotteryName(ELotteryKind.AMLHC.getDescription());
						continue;
					}
				issue = this.getPresellExpectByLotteryKind(lotteryKind.name());
				//查询预售期号为空，返回空数据
				if(issue == null) {
					continue;
				}
				long result = 0l;
				//时间差值
				if(lotteryKind.name().equals(ELotteryKind.JLFFC.name())
						|| lotteryKind.name().equals(ELotteryKind.SFSSC.name()) || lotteryKind.name().equals(ELotteryKind.WFSSC.name())
						|| lotteryKind.name().equals(ELotteryKind.SFKS.name()) || lotteryKind.name().equals(ELotteryKind.WFKS.name()) ||
						lotteryKind.name().equals(ELotteryKind.WFSYXW.name()) || lotteryKind.name().equals(ELotteryKind.SHFSSC.name()) || lotteryKind.name().equals(ELotteryKind.SFSYXW.name())
						|| lotteryKind.name().equals(ELotteryKind.SFPK10.name()) || lotteryKind.name().equals(ELotteryKind.WFPK10.name()) || lotteryKind.name().equals(ELotteryKind.SHFPK10.name())
				){
					result += issue.getEndtime().getTime() - DateUtil.getNowStartTimeByStart(queryDate).getTime();
					result += DateUtil.getNowStartTimeByEnd(queryDate).getTime() - queryDate.getTime();
				}else if(lotteryKind.name().equals(ELotteryKind.FCSDDPC.name())){
					result += DateUtil.getNowStartTimeByEnd(queryDate).getTime() - queryDate.getTime() ;
					result += issue.getEndtime().getTime() - issue.getBegintime().getTime();
				}else{
					if(issue.getEndtime().getTime() > queryDate.getTime()){
						result = issue.getEndtime().getTime() - queryDate.getTime();
					}else{
						result += queryDate.getTime() - DateUtil.getNowStartTimeByStart(queryDate).getTime();
						result += DateUtil.getNowStartTimeByEnd(queryDate).getTime() - issue.getEndtime().getTime();
					}
				}
				issue.setTimeDifference(result/1000); //毫秒转成秒
				issue.setLotteryNumCount(issue.getLotteryNum());

				Date endDate = lastLotteryIssue.getEndtime();
				if(lotteryKind.equals(ELotteryKind.CQSSC)){
					kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
				}else if(lotteryKind.equals(ELotteryKind.JXSSC)){
					//Date endDate = DateUtils.getDateByStrFormat("2015-01-01 23:10:40", "yyyy-MM-dd HH:mm:ss");
					if(endDate.getTime() <= queryDate.getTime() && queryDate.getTime() <= DateUtil.getNowStartTimeByEnd(endDate).getTime()){
						kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
					}
				}else if(lotteryKind.equals(ELotteryKind.TJSSC)){
					//Date endDate = DateUtils.getDateByStrFormat("2015-01-13 22:57:25", "yyyy-MM-dd HH:mm:ss");
					if(endDate.getTime() <= queryDate.getTime() && queryDate.getTime() <= DateUtil.getNowStartTimeByEnd(endDate).getTime()){
						kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
					}
				}else if(lotteryKind.equals(ELotteryKind.XJSSC)){

				}else if(lotteryKind.equals(ELotteryKind.GDSYXW)){
					//Date endDate = DateUtils.getDateByStrFormat("2015-01-05 22:58:50", "yyyy-MM-dd HH:mm:ss");
					if(endDate.getTime() <= queryDate.getTime() && queryDate.getTime() <= DateUtil.getNowStartTimeByEnd(endDate).getTime()){
						kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
					}
				}else if(lotteryKind.equals(ELotteryKind.SDSYXW)){
					//Date endDate = DateUtils.getDateByStrFormat("2015-01-06 21:53:20", "yyyy-MM-dd HH:mm:ss");
					if(endDate.getTime() <= queryDate.getTime() && queryDate.getTime() <= DateUtil.getNowStartTimeByEnd(endDate).getTime()){
						kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
					}
				}else if(lotteryKind.equals(ELotteryKind.JXSYXW)){
					//Date endDate = DateUtils.getDateByStrFormat("2015-01-07 22:58:10", "yyyy-MM-dd HH:mm:ss");
					if(endDate.getTime() <= queryDate.getTime() && queryDate.getTime() <= DateUtil.getNowStartTimeByEnd(endDate).getTime()){
						kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
					}
				}else if(lotteryKind.equals(ELotteryKind.FJSYXW)){
					//Date endDate = DateUtils.getDateByStrFormat("2015-01-09 23:05:10", "yyyy-MM-dd HH:mm:ss");
					if(endDate.getTime() <= queryDate.getTime() && queryDate.getTime() <= DateUtil.getNowStartTimeByEnd(endDate).getTime()){
						kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
					}
				}else if(lotteryKind.equals(ELotteryKind.JSKS)){
					//Date endDate = DateUtils.getDateByStrFormat("2015-01-14 22:08:00", "yyyy-MM-dd HH:mm:ss");
					if(endDate.getTime() <= queryDate.getTime() && queryDate.getTime() <= DateUtil.getNowStartTimeByEnd(endDate).getTime()){
						kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
					}
				}else if(lotteryKind.equals(ELotteryKind.AHKS)||lotteryKind.equals(ELotteryKind.BJKS)){
					//Date endDate = DateUtils.getDateByStrFormat("2015-01-18 21:58:10", "yyyy-MM-dd HH:mm:ss");
					if(endDate.getTime() <= queryDate.getTime() && queryDate.getTime() <= DateUtil.getNowStartTimeByEnd(endDate).getTime()){
						kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
					}
				}else if(lotteryKind.equals(ELotteryKind.JLKS)){
					//Date endDate = DateUtils.getDateByStrFormat("2015-01-20 21:59:05", "yyyy-MM-dd HH:mm:ss");
					if(endDate.getTime() <= queryDate.getTime() && queryDate.getTime() <= DateUtil.getNowStartTimeByEnd(endDate).getTime()){
						kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
					}
				}else if(lotteryKind.equals(ELotteryKind.HBKS)){
					//Date endDate = DateUtils.getDateByStrFormat("2015-01-20 21:59:05", "yyyy-MM-dd HH:mm:ss");
					if(endDate.getTime() <= queryDate.getTime() && queryDate.getTime() <= DateUtil.getNowStartTimeByEnd(endDate).getTime()){
						kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
					}
				}else if(lotteryKind.equals(ELotteryKind.JYKS)){
					//Date endDate = DateUtils.getDateByStrFormat("2015-01-24 22:08:40", "yyyy-MM-dd HH:mm:ss");
					if(endDate.getTime() <= queryDate.getTime() && queryDate.getTime() <= DateUtil.getNowStartTimeByEnd(endDate).getTime()){
						kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
					}
				}else if(lotteryKind.equals(ELotteryKind.GXKS)){
					//Date endDate = DateUtils.getDateByStrFormat("2015-01-26 22:08:40", "yyyy-MM-dd HH:mm:ss");
					if(endDate.getTime() <= queryDate.getTime() && queryDate.getTime() <= DateUtil.getNowStartTimeByEnd(endDate).getTime()){
						kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
					}
				}else if(lotteryKind.equals(ELotteryKind.GSKS)){
					//Date endDate = DateUtils.getDateByStrFormat("2015-01-28 22:08:40", "yyyy-MM-dd HH:mm:ss");
					if(endDate.getTime() <= queryDate.getTime() && queryDate.getTime() <= DateUtil.getNowStartTimeByEnd(endDate).getTime()){
						kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
					}
				}else if(lotteryKind.equals(ELotteryKind.SHKS)){
					//Date endDate = DateUtils.getDateByStrFormat("2015-01-20 22:20:59", "yyyy-MM-dd HH:mm:ss");
					if(endDate.getTime() <= queryDate.getTime() && queryDate.getTime() <= DateUtil.getNowStartTimeByEnd(endDate).getTime()){
						kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
					}
				}else if(lotteryKind.equals(ELotteryKind.TWWFC)){
					//Date endDate = DateUtils.getDateByStrFormat("2015-01-20 23:55:00", "yyyy-MM-dd HH:mm:ss");
					if(endDate.getTime() <= queryDate.getTime() && queryDate.getTime() <= DateUtil.getNowStartTimeByEnd(endDate).getTime()){
						kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
					}
				}else if(lotteryKind.equals(ELotteryKind.XYEB)){
					//Date endDate = DateUtils.getDateByStrFormat("2015-01-23 23:54:40", "yyyy-MM-dd HH:mm:ss");
					if(endDate.getTime() <= queryDate.getTime() && queryDate.getTime() <= DateUtil.getNowStartTimeByEnd(endDate).getTime()){
						kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, 1);
					}
				}

				//期号
				//福彩3D预售旗号处理
				if(lotteryKind.equals(ELotteryKind.FCSDDPC)){
					//在配置的时间查询不到，需要将期数+1
					dealExpectNumForFcsdByDate(issue, true);
					//PK10期号特殊处理
				} else if(lotteryKind.equals(ELotteryKind.BJPK10)){
					dealExpectNumForPk10ByDate(issue);
					//Date endDate = DateUtils.getDateByStrFormat("2015-01-16 23:56:00", "yyyy-MM-dd HH:mm:ss");
					//在最后一期时间之后，预售期号需要加上44期
					if(queryDate.after(endDate)) {
						Integer expectNum = Integer.parseInt(issue.getLotteryNum());
						expectNum += 44;
						issue.setLotteryNum(String.valueOf(expectNum));
					}
				} else if(lotteryKind.equals(ELotteryKind.HGFFC)){
					dealExpectNumForHgffcByDate(issue, new Date());
				} else if(lotteryKind.equals(ELotteryKind.XJPLFC)){
					dealExpectNumForXjplfcByDate(issue, new Date());
				} else if(lotteryKind.equals(ELotteryKind.TWWFC)){
					dealExpectNumForTwwfcByDate(issue, kaijiangDate);
				}else if(lotteryKind.equals(ELotteryKind.XYEB)){
					dealExpectNumForXyebByDate(issue, kaijiangDate);
				} /*else if(lotteryKind.equals(ELotteryKind.BJKS)){
        			dealExpectNumForBjksByDate(issue);
        			//Date endDate = DateUtils.getDateByStrFormat("2015-01-21 23:48:05", "yyyy-MM-dd HH:mm:ss");
        			//在最后一期时间之后，预售期号需要加上44期
        			if(queryDate.after(endDate)) {
        				Integer expectNum = Integer.parseInt(issue.getLotteryNum());
        				expectNum += 44;
        				issue.setLotteryNum(String.valueOf(expectNum));
        			}
        		}*/else{
					issue.setLotteryNum(DateUtils.getDateToStrByFormat(kaijiangDate, "yyyyMMdd") + issue.getLotteryNum());
				}
			}
		}

		return issues;

	}

	/**
	 * 根据时间获取福彩3D对应的旗号数目
	 * @param isAfterEndTime 是否在配置的截止时间之后，是需要将期数+1
	 * @return
	 */
	private void dealExpectNumForFcsdByDate(LotteryIssue issue, boolean isAfterEndTime){
		//春节预售期开始时间
		Date d3StopStartDate = DateUtil.getNowStartTimeByStart(DateUtils.getDateByStrFormat(SystemConfigConstant.springFestivalStartDate, "yyyy-MM-dd"));
		//春节预售期结束时间
		Date d3StopEndDate = DateUtil.getNowStartTimeByEnd(DateUtils.getDateByStrFormat(SystemConfigConstant.springFestivalEndDate, "yyyy-MM-dd"));
		Date currentDate = new Date();
		int yearNum = DateUtil.getYear(currentDate); //获取年份
		int springFestivalYearNum = DateUtil.getYear(d3StopStartDate);

		Integer expectNum = 0;
		// 当年和春节时间同年才处理
		if(yearNum == springFestivalYearNum) {

			// 春节期间
			if(currentDate.getTime() >= d3StopStartDate.getTime() && currentDate.getTime() <= d3StopEndDate.getTime()){
				Integer timediff = 60 * 60 * 24 * 3;  //超过一天，已经是预售
				issue.setTimeDifference(timediff.longValue());
				issue.setSpecialExpect(1); //标识为预售期号
				try {
					expectNum =  DateUtil.daysBetween(DateUtils.getDateByStrFormat(yearNum + "/01/01","yyyy/MM/dd"), DateUtil.addDaysToDate(d3StopEndDate, 1)) - DateUtil.daysBetween(d3StopStartDate, d3StopEndDate);
				} catch (ParseException e) {
					log.error("getNewExpectAndDiffTimeByLotteryKind方法日期转变异常.");
				}
				// 春节之前
			}else if(currentDate.getTime() < d3StopStartDate.getTime()){
				try {
					expectNum = DateUtil.daysBetween(DateUtils.getDateByStrFormat(yearNum + "/01/01","yyyy/MM/dd"), currentDate) + 1;
				} catch (ParseException e) {
					log.error("getNewExpectAndDiffTimeByLotteryKind方法日期转变异常.");
				}
				// 春节之后
			}else{
				try {
					//得到最后一期开奖的对象
//					LotteryCode lastLotteryCode =  lotteryCodeService.getLastLotteryCode(ELotteryKind.FCSDDPC);
//					String lotteryNumStr=lastLotteryCode.getLotteryNum();
//					Integer lotteryNum=Integer.parseInt(lotteryNumStr.substring(lotteryNumStr.length()-2));
//					expectNum=lotteryNum+1;
					expectNum = DateUtil.daysBetween(DateUtils.getDateByStrFormat(yearNum + "/01/01","yyyy/MM/dd"), currentDate) - DateUtil.daysBetween(d3StopStartDate, d3StopEndDate);
					expectNum-=42;
				} catch (Exception e) {
					log.error("getNewExpectAndDiffTimeByLotteryKind方法日期转变异常.");
				}
			}
		} else {
			try {
				expectNum = DateUtil.daysBetween(DateUtils.getDateByStrFormat(yearNum + "/01/01","yyyy/MM/dd"), currentDate) + 1;
			} catch (ParseException e) {
				log.error("getNewExpectAndDiffTimeByLotteryKind方法日期转变异常.");
			}
		}

		if(isAfterEndTime) {
			expectNum++;
		}
		//因为之前已经+1了，所以这里就不需要+1了
		//特殊判断处理，2019年国庆期间不销售，这里要扣掉对应的期号
		if("2019".equals(String.valueOf(DateUtil.getYear(currentDate)))) {
			expectNum = expectNum - 7;
		}

		String lotteryNum = expectNum.toString();
		if(expectNum < 10) {
			lotteryNum = "00" + expectNum.toString();
		} else if(expectNum < 100) {
			lotteryNum = "0" + expectNum.toString();
		}
		issue.setSpecialTime(1);
		issue.setLotteryNumCount(lotteryNum);
		issue.setLotteryNum(yearNum + lotteryNum);

	}

	/**
	 * 根据时间获取北京pk10对应的旗号数目
	 * @return
	 */
	private void dealExpectNumForPk10ByDate(LotteryIssue issue){

		Integer expectNum = Integer.parseInt(issue.getLotteryNum());
		//获取今日的基准期号
		int baseExpect = this.getPK10BaseExpect(new Date());
		//基准期号加上当前的第几期期号
		expectNum = expectNum + baseExpect;
		//============ericSun2020年5月7日20:24:22修改北京pk10=========
		//===========由于北京pk10的预测期号不准
		String lotteryNum = expectNum.toString();
		Integer intLotteryNum = Integer.valueOf(lotteryNum) ;
		intLotteryNum-=4312;
		lotteryNum=intLotteryNum.toString();
		//这段代码用于校准期号
		issue.setLotteryNumCount(lotteryNum);
		issue.setLotteryNum(lotteryNum);

	}

	/**
	 * 根据时间获取北京快3对应的旗号数目
	 * @return
	 */
	private void dealExpectNumForBjksByDate(LotteryIssue issue){
		Integer expectNum = Integer.parseInt(issue.getLotteryNum());
		//获取今日的基准期号
		int baseExpect = this.getBjksBaseExpect(new Date());
		//基准期号加上当前的第几期期号
		expectNum = expectNum + baseExpect;


		String lotteryNum = expectNum.toString();
		issue.setLotteryNumCount(lotteryNum);
		issue.setLotteryNum(lotteryNum);

	}
	/**
	 * 根据日期获取基准期号
	 * @param nowDate
	 * @return
	 */
	private int getPK10BaseExpect(Date nowDate) {
		//春节预售期开始时间
		Date d3StopStartDate = DateUtil.getNowStartTimeByStart(DateUtils.getDateByStrFormat(SystemConfigConstant.springFestivalStartDate, "yyyy-MM-dd"));
		//春节预售期结束时间
		Date d3StopEndDate = DateUtil.getNowStartTimeByEnd(DateUtils.getDateByStrFormat(SystemConfigConstant.springFestivalEndDate, "yyyy-MM-dd"));
    	/*Date baseDate = DateUtils.getDateByStrFormat("2016-02-14", "yyyy-MM-dd");
    	//基准期数
    	int basetExpect = 537523;*/

		Date baseDate = DateUtils.getDateByStrFormat(SystemConfigConstant.pk10BaseDate, "yyyy-MM-dd");
		//基准期数
		int basetExpect = SystemConfigConstant.pk10BasetExpect;
		//每天的期数
		int everyDayExpectCount = 44;
		if(nowDate.before(baseDate)) {
			log.error("当前时间在PK10基准时间之前，配置错误...");
			throw new RuntimeException("当前时间在PK10基准时间之前，配置错误...");
		}

		//距离基准日期的天数
		int betweenBaseDays = 0;
		int springDays = 7;
		try {
			betweenBaseDays = DateUtil.daysBetween(baseDate, nowDate);
			springDays = DateUtil.daysBetween(d3StopStartDate, d3StopEndDate) + 1;
		} catch (ParseException e) {
			log.error("getNewExpectAndDiffTimeByLotteryKind方法日期转变异常.");
		}
		//春节期间
		if(d3StopStartDate.getTime() <= nowDate.getTime() && nowDate.getTime() <= d3StopEndDate.getTime()){
			basetExpect = basetExpect + betweenBaseDays * everyDayExpectCount;
			//春节之后的时间
		}else if(nowDate.getTime() > d3StopEndDate.getTime()){
			// 基准日期在春节结束时间前
			if(baseDate.before(d3StopEndDate)) {
				basetExpect = basetExpect + (betweenBaseDays - springDays) * everyDayExpectCount;
			} else {
				basetExpect = basetExpect + betweenBaseDays * everyDayExpectCount;
			}
		}else{
			basetExpect = basetExpect + betweenBaseDays * everyDayExpectCount;
		}
		return basetExpect;
	}

	/**
	 * 判断彩种是否是封盘状态
	 * @param lotteryKind
	 * @param bizSystem
	 * @return
	 */
	public boolean nowIsClosingTime( ELotteryKind lotteryKind,String bizSystem){
		List<LotteryIssue> lotteryIssues = LotteryIssueCache.getInstance().getLotteryIssueByKind(lotteryKind.getCode());
		String currentSFM = DateUtils.getDateToStrByFormat(new Date(), "HH:mm:ss");
		String timeStr = lotteryKind.getDateControlStr() + "  " + currentSFM;
		Date dateByDateStr = DateUtils.getDateByStr2(timeStr);
		//六合彩特殊处理
		if(ELotteryKind.LHC.equals(lotteryKind)||ELotteryKind.AMLHC.equals(lotteryKind)){
			LhcIssueConfig lhcIssueConfig = lhcIssueConfigService.getLhcIssueConfig(lotteryKind.name());
			for (LotterySwitch lotterySwitch : ClsCacheManager.getLotterySwitchByBizSystem(bizSystem)) {
				if(lotterySwitch.getLotteryType().equals(lotteryKind.getCode())){
					Long closingTime =new Date().getTime()+(lotterySwitch.getClosingTime()*1000);
					if(closingTime>=lhcIssueConfig.getNextEndTime().getTime()) return true;
				}
			}
		}else {
			for(LotteryIssue lotteryIssue:lotteryIssues){
				if(dateByDateStr.getTime()>lotteryIssue.getBegintime().getTime()&&dateByDateStr.getTime()<=lotteryIssue.getEndtime().getTime()){
					for (LotterySwitch lotterySwitch : ClsCacheManager.getLotterySwitchByBizSystem(bizSystem)) {
						if(lotterySwitch.getLotteryType().equals(lotteryKind.getCode())){
							Long closingTime =dateByDateStr.getTime()+(lotterySwitch.getClosingTime()*1000);
							if(closingTime>=lotteryIssue.getEndtime().getTime()) return true;
						}
					}
				}
			}
		}

		return false;
	}

	/**
	 * 根据时间获取韩国1.5分彩对应的旗号数目
	 * @return
	 */
	private void dealExpectNumForHgffcByDate(LotteryIssue issue, Date calculateDate){

		Integer expectNum = Integer.parseInt(issue.getLotteryNum());
		//获取今日的基准期号
		int baseExpect = this.getHgffcBaseExpect(calculateDate);
		//基准期号加上当前的第几期期号
		expectNum = expectNum + baseExpect;
		String lotteryNum = expectNum.toString();
		issue.setLotteryNumCount(lotteryNum);
		issue.setLotteryNum(lotteryNum);

	}

	/**
	 * 韩国1.5分彩根据日期获取基准期号
	 * @param calculateDate
	 * @return
	 */
	private int getHgffcBaseExpect(Date calculateDate) {

		Date baseDate = DateUtils.getDateByStrFormat(SystemConfigConstant.hgffcBaseDate, "yyyy-MM-dd");
		//基准期数
		int basetExpect = SystemConfigConstant.hgffcBasetExpect;
		//每天的期数
		int everyDayExpectCount = 880;
		if(calculateDate.before(baseDate)) {
			log.error("当前时间在韩国1.5分彩基准时间之前，配置错误...");
			throw new RuntimeException("当前时间在韩国1.5分彩基准时间之前，配置错误...");
		}

		//距离基准日期的天数
		int betweenBaseDays = 0;
		try {
			betweenBaseDays = DateUtil.daysBetween(baseDate, calculateDate);
		} catch (ParseException e) {
			log.error("getNewExpectAndDiffTimeByLotteryKind方法日期转变异常.");
		}
		basetExpect = basetExpect + betweenBaseDays * everyDayExpectCount;
		return basetExpect;
	}

	/**
	 * 根据时间获取新加坡2分彩对应的旗号数目
	 * @return
	 */
	private void dealExpectNumForXjplfcByDate(LotteryIssue issue, Date calculateDate){

		Integer expectNum = Integer.parseInt(issue.getLotteryNum());
		//获取今日的基准期号
		int baseExpect = this.getXjplfcBaseExpect(calculateDate);
		//基准期号加上当前的第几期期号
		expectNum = expectNum + baseExpect;
		String lotteryNum = expectNum.toString();
		issue.setLotteryNumCount(lotteryNum);
		issue.setLotteryNum(lotteryNum);
	}

	/**
	 *	新加坡2分彩根据日期获取基准期号
	 * @param calculateDate
	 * @return
	 */
	private int getXjplfcBaseExpect(Date calculateDate) {

		Date baseDate = DateUtils.getDateByStrFormat(SystemConfigConstant.xjplfcBaseDate, "yyyy-MM-dd");
		//基准期数
		int basetExpect = SystemConfigConstant.xjplfcBasetExpect;
		//每天的期数
		int everyDayExpectCount = 660;
		if(calculateDate.before(baseDate)) {
			log.error("当前时间在新加坡2分彩基准时间之前，配置错误...");
			throw new RuntimeException("当前时间在新加坡2分彩基准时间之前，配置错误...");
		}

		//距离基准日期的天数
		int betweenBaseDays = 0;
		try {
			betweenBaseDays = DateUtil.daysBetween(baseDate, calculateDate);
		} catch (ParseException e) {
			log.error("getNewExpectAndDiffTimeByLotteryKind方法日期转变异常.");
		}
		basetExpect = basetExpect + betweenBaseDays * everyDayExpectCount;
		return basetExpect;
	}

	/**
	 * 根据时间获取台湾5分彩对应的旗号数目
	 * @return
	 */
	private void dealExpectNumForTwwfcByDate(LotteryIssue issue, Date calculateDate){

		Integer expectNum = Integer.parseInt(issue.getLotteryNum());
		//获取今日的基准期号
		int baseExpect = this.getTwwfcBaseExpect(calculateDate);
		//基准期号加上当前的第几期期号
		expectNum = expectNum + baseExpect;
		String lotteryNum = expectNum.toString();
		issue.setLotteryNumCount(lotteryNum);
		issue.setLotteryNum(lotteryNum);
	}

	/**
	 * 台湾5分彩 根据日期获取基准期号
	 * @param calculateDate
	 * @return
	 */
	private int getTwwfcBaseExpect(Date calculateDate) {

		Date baseDate = DateUtils.getDateByStrFormat(SystemConfigConstant.twwfcBaseDate, "yyyy-MM-dd");
		//基准期数
		int basetExpect = SystemConfigConstant.twwfcBasetExpect;
		//每天的期数
		int everyDayExpectCount = 203;
		if(calculateDate.before(baseDate)) {
			log.error("当前时间在台湾5分彩基准时间之前，配置错误...");
			throw new RuntimeException("当前时间在台湾5分彩基准时间之前，配置错误...");
		}

		//距离基准日期的天数
		int betweenBaseDays = 0;
		try {
			betweenBaseDays = DateUtil.daysBetween(baseDate, calculateDate);
		} catch (ParseException e) {
			log.error("getNewExpectAndDiffTimeByLotteryKind方法日期转变异常.");
		}
		basetExpect = basetExpect + betweenBaseDays * everyDayExpectCount;
		return basetExpect;
	}

	/**
	 * 根据时间获取幸运28对应的旗号数目
	 * @return
	 */
	private void dealExpectNumForXyebByDate(LotteryIssue issue, Date calculateDate){

		Integer expectNum = Integer.parseInt(issue.getLotteryNum());
		//获取今日的基准期号
		int baseExpect = this.getXyebBaseExpect(calculateDate);
		//基准期号加上当前的第几期期号
		expectNum = expectNum + baseExpect;
		//==========ericSun2020年5月7日20:30:46修改
		expectNum-=18795;
		//===========校准幸运28期号=============================
		String lotteryNum = expectNum.toString();
		issue.setLotteryNumCount(lotteryNum);
		issue.setLotteryNum(lotteryNum);
	}

	/**
	 * 幸运28 根据日期获取基准期号
	 * @param calculateDate
	 * @return
	 */
	private int getXyebBaseExpect(Date calculateDate) {

		Date baseDate = DateUtils.getDateByStrFormat(SystemConfigConstant.xyebBaseDate, "yyyy-MM-dd");
		//基准期数
		int basetExpect = SystemConfigConstant.xyebBasetExpect;
		//每天的期数
		int everyDayExpectCount = 179;
		if(calculateDate.before(baseDate)) {
			log.error("当前时间在幸运28基准时间之前，配置错误...");
			throw new RuntimeException("当前时间在幸运28基准时间之前，配置错误...");
		}

		//距离基准日期的天数
		int betweenBaseDays = 0;
		try {
			betweenBaseDays = DateUtil.daysBetween(baseDate, calculateDate);
		} catch (ParseException e) {
			log.error("getNewExpectAndDiffTimeByLotteryKind方法日期转变异常.");
		}
		basetExpect = basetExpect + betweenBaseDays * everyDayExpectCount;
		return basetExpect;
	}

	/**
	 * 根据时间获取北京快3对应的旗号数目
	 * @return
	 */
	private void dealExpectNumForBjksByDate(LotteryIssue issue, Date calculateDate){

		Integer expectNum = Integer.parseInt(issue.getLotteryNum());
		//获取今日的基准期号
		int baseExpect = this.getBjksBaseExpect(calculateDate);
		//基准期号加上当前的第几期期号
		expectNum = expectNum + baseExpect;
		//===========期号校准 ericSun 2020年5月8日20:09:25修改=========
		expectNum-=4312;

		String lotteryNum = expectNum.toString();
		issue.setLotteryNumCount(lotteryNum);
		issue.setLotteryNum(lotteryNum);

	}

	/**
	 * 北京快3根据日期获取基准期号
	 * @param calculateDate
	 * @return
	 */
	private int getBjksBaseExpect(Date calculateDate) {
		//春节预售期开始时间
		Date d3StopStartDate = DateUtil.getNowStartTimeByStart(DateUtils.getDateByStrFormat(SystemConfigConstant.springFestivalStartDate, "yyyy-MM-dd"));
		//春节预售期结束时间
		Date d3StopEndDate = DateUtil.getNowStartTimeByEnd(DateUtils.getDateByStrFormat(SystemConfigConstant.springFestivalEndDate, "yyyy-MM-dd"));
		Date baseDate = DateUtils.getDateByStrFormat(SystemConfigConstant.bjksBaseDate, "yyyy-MM-dd");
		//基准期数
		int basetExpect = SystemConfigConstant.bjksBasetExpect;
		//每天的期数
		int everyDayExpectCount = 44;
		if(calculateDate.before(baseDate)) {
			log.error("当前时间在北京快3基准时间之前，配置错误...");
			throw new RuntimeException("当前时间在北京快3基准时间之前，配置错误...");
		}

		//距离基准日期的天数
		int betweenBaseDays = 0;
		int springDays = 7;
		try {
			betweenBaseDays = DateUtil.daysBetween(baseDate, calculateDate);
			springDays = DateUtil.daysBetween(d3StopStartDate, d3StopEndDate) + 1;
		} catch (ParseException e) {
			log.error("getNewExpectAndDiffTimeByLotteryKind方法日期转变异常.");
		}
		//春节期间
		if(d3StopStartDate.getTime() <= calculateDate.getTime() && calculateDate.getTime() <= d3StopEndDate.getTime()){
			basetExpect = basetExpect + betweenBaseDays * everyDayExpectCount;
			//春节之后的时间
		}else if(calculateDate.getTime() > d3StopEndDate.getTime()){
			if(baseDate.before(d3StopEndDate)) {
				basetExpect = basetExpect + (betweenBaseDays - springDays) * everyDayExpectCount;
			} else {
				basetExpect = basetExpect + betweenBaseDays * everyDayExpectCount;
			}
		}else{
			basetExpect = basetExpect + betweenBaseDays * everyDayExpectCount;
		}

		return basetExpect;
	}

	/**
	 * 加载今天和明天的所有期号
	 * @param lotteryKind
	 * @param
	 * @return
	 */
	public Map<String,List<LotteryIssue>> getAllExpectsByTodayAndTomorrow(ELotteryKind lotteryKind) {
		Map<String,List<LotteryIssue>> result = new HashMap<String, List<LotteryIssue>>();
		List<LotteryIssue> currentIssues = null;
		List<LotteryIssue> todayListIssue = null;
		List<LotteryIssue> tommorrowListIssue = null;
		//获取最后开奖时间
		LotteryIssue lastLotteryIssue =  getLastExpectByLotteryKind(lotteryKind.getCode());
		if(lastLotteryIssue == null){
			throw new RuntimeException("该彩种未配置时间");
		}
		String configDateControl = lotteryKind.getDateControlStr();
		String curretnDateSFM = DateUtils.getDateToStrByFormat(new Date(),"HH:mm:ss");
		Date startDate = DateUtils.getDateByStrFormat(configDateControl + " " + curretnDateSFM, "yyyy-MM-dd HH:mm:ss");
		Date endDate = lastLotteryIssue.getEndtime();
//    	if(lotteryKind.equals(ELotteryKind.CQSSC)){
//	        endDate = DateUtils.getDateByStrFormat(configDateControl + " 23:59:10", "yyyy-MM-dd HH:mm:ss");
//    	}else if(lotteryKind.equals(ELotteryKind.JXSSC)){
//	        endDate = DateUtils.getDateByStrFormat(configDateControl + " 23:13:30", "yyyy-MM-dd HH:mm:ss");
//    	}else if(lotteryKind.equals(ELotteryKind.HLJSSC)){  //待转换
//	        endDate = DateUtils.getDateByStrFormat(configDateControl + " 23:59:00", "yyyy-MM-dd HH:mm:ss");
//    	}else if(lotteryKind.equals(ELotteryKind.TJSSC)){
//	        endDate = DateUtils.getDateByStrFormat(configDateControl + " 23:09:30", "yyyy-MM-dd HH:mm:ss");
//    	}else
		//新疆的特殊处理
		if(lotteryKind.equals(ELotteryKind.XJSSC)){
			if(startDate.getTime() <= DateUtil.getNowStartTimeByEnd(startDate).getTime()){ // DateUtil.getNowStartTimeByStart().getTime() <= kuaduDateStart2.getTime()
			}else{
				startDate = DateUtil.addDaysToDate(startDate, 1);
			}
			String currentHms = DateUtils.getDateToStrByFormat(lastLotteryIssue.getEndtime(), "HH:mm:ss");
			endDate = DateUtils.getDateByStrFormat(configDateControl + " "+currentHms, "yyyy-MM-dd HH:mm:ss");
			endDate = DateUtil.addDaysToDate(endDate, 1);
		}
		//福彩3D的追号期数处理
		if(lotteryKind.equals(ELotteryKind.FCSDDPC)){
			todayListIssue = new ArrayList<LotteryIssue>();
			tommorrowListIssue = new ArrayList<LotteryIssue>();
			LotteryIssue currentIssue = this.getNewExpectAndDiffTimeByLotteryKind(ELotteryKind.FCSDDPC);

			//投注旗号数目
			String lotteryNumStr = currentIssue.getLotteryNum();
			Integer lotteryNumInt = Integer.parseInt(lotteryNumStr);

			Integer yearNum = DateUtil.getYear(new Date()); //获取年份
			Integer maxLotteryNum = 0;
			if(DateUtil.isRunYear(yearNum)){
				maxLotteryNum = Integer.parseInt(yearNum+"366");
			}else{
				maxLotteryNum = Integer.parseInt(yearNum+"365");
			}

			LotteryIssue expectIssue = null;
			Date expectDate = new Date();
			//是否是过年预售期间
			Date d3StopStartDate = DateUtil.getNowStartTimeByStart(DateUtils.getDateByStrFormat(SystemConfigConstant.springFestivalStartDate, "yyyy-MM-dd"));
			Date d3StopEndDate = DateUtil.getNowStartTimeByEnd(DateUtils.getDateByStrFormat(SystemConfigConstant.springFestivalEndDate, "yyyy-MM-dd"));

			for(int i = 0; i < 60; i++){
				if(d3StopStartDate.getTime() <= expectDate.getTime() && expectDate.getTime() <= d3StopEndDate.getTime()){
					expectDate = DateUtil.addDaysToDate(expectDate, 1);
					continue;
				}

				expectIssue = new LotteryIssue();
				expectIssue.setLotteryName(ELotteryKind.FCSDDPC.getDescription());
				expectIssue.setLotteryType(ELotteryKind.FCSDDPC.name());
				expectIssue.setBegintime(expectDate);
				expectIssue.setEndtime(expectDate);
				expectIssue.setIssale(1);
				expectIssue.setState(1);
				expectIssue.setLotteryNum(lotteryNumInt.toString());
				expectDate = DateUtil.addDaysToDate(expectDate, 1);

				if(i < 30){
					todayListIssue.add(expectIssue);
				}else{
					tommorrowListIssue.add(expectIssue);
				}

				if(lotteryNumInt > maxLotteryNum){
					lotteryNumInt = Integer.parseInt((yearNum + 1) + "000"); //从0期开始
				}
				lotteryNumInt++;
			}
			result.put(EAfterNumberType.TODAY.name(), todayListIssue);
			result.put(EAfterNumberType.TOMORROW.name(), tommorrowListIssue);
		}else{
			Long startTime = System.currentTimeMillis();
			Date queryDate = DateUtils.getDateByStrFormat(configDateControl + " " + curretnDateSFM, "yyyy-MM-dd HH:mm:ss");
			LotteryIssue curretnIssue = null;
			if (lotteryKind.equals(ELotteryKind.XJSSC)) {
			}else {
				todayListIssue = this.getAllExpectsByCondition(lotteryKind.name(), startDate, endDate);
			}
			tommorrowListIssue = this.getAllExpectsByCondition(lotteryKind.name(),null,null);
			Long endTime = System.currentTimeMillis();
			log.debug("查询今天明天期数耗时["+(endTime - startTime)+"]ms");
			//确保包含当前期号
			currentIssues = new ArrayList<LotteryIssue>();

			if(lotteryKind.equals(ELotteryKind.XJSSC)){
				Date kuaduDateStart = DateUtils.getDateByStrFormat(configDateControl + " " + "00:00:00", "yyyy-MM-dd HH:mm:ss");
				if (queryDate.getTime() >=kuaduDateStart.getTime() && queryDate.getTime() <= DateUtil.addDaysToDate(lastLotteryIssue.getEndtime(), -1).getTime()) {
					queryDate = DateUtil.addDaysToDate(queryDate, 1);
				}
				curretnIssue = this.getNewExpectByLotteryKind(lotteryKind.name(), queryDate);
				todayListIssue = this.getAllExpectsByCondition(lotteryKind.name(), queryDate, endDate);
			} else if(lotteryKind.equals(ELotteryKind.BJPK10)) {
				//获取今日的基准期号
				Date today = new Date();
				/*Date tommorrow = DateUtil.addDaysToDate(today, 1);*/
				int todayBaseExpect = this.getPK10BaseExpect(today);
				for(LotteryIssue todayIssue : todayListIssue){
					Integer expectNum = Integer.parseInt(todayIssue.getLotteryNum());
					//基准期号加上当前的第几期期号
					expectNum = expectNum + todayBaseExpect;
					//===============ericSun2020年5月11日22:15:3校准追号期号========
					expectNum-=4313;
					//=====================================================
					String lotteryNum = expectNum.toString();
					todayIssue.setLotteryNum(lotteryNum);
				}

				//获取明日的基准期号
              	/*int tommorrowBaseExpect = this.getPK10BaseExpect(tommorrow);
            	for(LotteryIssue tommorrowIssue : tommorrowListIssue){
            		Integer expectNum = Integer.parseInt(tommorrowIssue.getLotteryNum());
                  	//基准期号加上当前的第几期期号
                  	expectNum = expectNum + tommorrowBaseExpect;
                	String lotteryNum = expectNum.toString();
                	tommorrowIssue.setLotteryNum(lotteryNum);
            	}*/
			}else if(lotteryKind.equals(ELotteryKind.HGFFC)){
				//获取今日的基准期号
				Date todate = new Date();
				/*Date tommorrow = DateUtil.addDaysToDate(todate, 1);*/
				int todayBaseExpect = this.getHgffcBaseExpect(todate);
				for(LotteryIssue todayIssue : todayListIssue){
					Integer expectNum = Integer.parseInt(todayIssue.getLotteryNum());
					//基准期号加上当前的第几期期号
					expectNum = expectNum + todayBaseExpect;
					String lotteryNum = expectNum.toString();
					todayIssue.setLotteryNum(lotteryNum);
				}
			}else if(lotteryKind.equals(ELotteryKind.BJKS)){
				//获取今日的基准期号
				Date todate = new Date();
				/*Date tommorrow = DateUtil.addDaysToDate(todate, 1);*/
				int todayBaseExpect = this.getBjksBaseExpect(todate);
				for(LotteryIssue todayIssue : todayListIssue){
					Integer expectNum = Integer.parseInt(todayIssue.getLotteryNum());
					//基准期号加上当前的第几期期号
					expectNum = expectNum + todayBaseExpect;
					expectNum-=4313;
					String lotteryNum = expectNum.toString();
					todayIssue.setLotteryNum(lotteryNum);
				}
			}else if(lotteryKind.equals(ELotteryKind.XJPLFC)){
				//获取今日的基准期号
				Date todate = new Date();
				/*Date tommorrow = DateUtil.addDaysToDate(todate, 1);*/
				int todayBaseExpect = this.getXjplfcBaseExpect(todate);
				for(LotteryIssue todayIssue : todayListIssue){
					Integer expectNum = Integer.parseInt(todayIssue.getLotteryNum());
					//基准期号加上当前的第几期期号
					expectNum = expectNum + todayBaseExpect;
					String lotteryNum = expectNum.toString();
					todayIssue.setLotteryNum(lotteryNum);
				}
			}else if(lotteryKind.equals(ELotteryKind.TWWFC)){
				//获取今日的基准期号
				Date todate = new Date();
				/*Date tommorrow = DateUtil.addDaysToDate(todate, 1);*/
				int todayBaseExpect = this.getTwwfcBaseExpect(todate);
				for(LotteryIssue todayIssue : todayListIssue){
					Integer expectNum = Integer.parseInt(todayIssue.getLotteryNum());
					//基准期号加上当前的第几期期号
					expectNum = expectNum + todayBaseExpect;
					String lotteryNum = expectNum.toString();
					todayIssue.setLotteryNum(lotteryNum);
				}
			}
			if (!lotteryKind.equals(ELotteryKind.XJSSC)) {
				curretnIssue = this.getNewExpectByLotteryKind(lotteryKind.name(), queryDate);
			}
			//特殊处理当前期号
			if(lotteryKind.equals(ELotteryKind.BJPK10)) {
				int todayBaseExpect = this.getPK10BaseExpect(new Date());
				Integer expectNum = Integer.parseInt(curretnIssue.getLotteryNum());
				//基准期号加上当前的第几期期号
				expectNum = expectNum + todayBaseExpect;
				//===============ericSun2020年5月11日22:15:3校准期号========
				expectNum-=4312;
				//===============================================
				String lotteryNum = expectNum.toString();
				curretnIssue.setLotteryNum(lotteryNum);
			}else if(lotteryKind.equals(ELotteryKind.HGFFC)){
				int todayBaseExpect = this.getHgffcBaseExpect(new Date());
				Integer expectNum = Integer.parseInt(curretnIssue.getLotteryNum());
				//基准期号加上当前的第几期期号
				expectNum = expectNum + todayBaseExpect;
				String lotteryNum = expectNum.toString();
				curretnIssue.setLotteryNum(lotteryNum);
			}else if(lotteryKind.equals(ELotteryKind.BJKS)){
				int todayBaseExpect = this.getBjksBaseExpect(new Date());
				Integer expectNum = Integer.parseInt(curretnIssue.getLotteryNum());
				//基准期号加上当前的第几期期号
				expectNum = expectNum + todayBaseExpect;
				expectNum-=4312;
				String lotteryNum = expectNum.toString();
				curretnIssue.setLotteryNum(lotteryNum);
			}else if(lotteryKind.equals(ELotteryKind.XJPLFC)){
				int todayBaseExpect = this.getXjplfcBaseExpect(new Date());
				Integer expectNum = Integer.parseInt(curretnIssue.getLotteryNum());
				//基准期号加上当前的第几期期号
				expectNum = expectNum + todayBaseExpect;
				String lotteryNum = expectNum.toString();
				curretnIssue.setLotteryNum(lotteryNum);
			}else if(lotteryKind.equals(ELotteryKind.TWWFC)){
				int todayBaseExpect = this.getTwwfcBaseExpect(new Date());
				Integer expectNum = Integer.parseInt(curretnIssue.getLotteryNum());
				//基准期号加上当前的第几期期号
				expectNum = expectNum + todayBaseExpect;
				String lotteryNum = expectNum.toString();
				curretnIssue.setLotteryNum(lotteryNum);
			}
			if(curretnIssue != null && todayListIssue != null && todayListIssue.size() > 0 && !todayListIssue.get(0).getLotteryNum().equals(curretnIssue.getLotteryNum())){
				currentIssues.add(curretnIssue);
				currentIssues.addAll(todayListIssue);
			}else{
				currentIssues = todayListIssue;
			}
			todayListIssue = currentIssues;

			if(CollectionUtils.isNotEmpty(todayListIssue)) {
				//控制返回的期数
				if(todayListIssue.size() > 20) {
					todayListIssue = todayListIssue.subList(0, RETURN_ISSUE_COUNT);
				}
				for(LotteryIssue todayIssue : todayListIssue){
					todayIssue.setCurrentDateStr(DateUtils.getDateToStrByFormat(new Date(), "yyyyMMdd"));
					//将追号的投注截止时间更改
					todayIssue.setEndtime(DateUtils.getDateByStrFormat(DateUtils.getDateToStrByFormat(new Date(), "yyyy/MM/dd") + " " +DateUtils.getDateToStrByFormat(todayIssue.getEndtime(), "HH:mm:ss"), "yyyy/MM/dd HH:mm:ss"));
				}
			}
			if(CollectionUtils.isNotEmpty(tommorrowListIssue)) {
				//控制返回的期数
				if(tommorrowListIssue.size() > 20) {
					tommorrowListIssue = tommorrowListIssue.subList(0, RETURN_ISSUE_COUNT);
				}
				for(LotteryIssue tommorrowIssue : tommorrowListIssue){
					tommorrowIssue.setCurrentDateStr(DateUtils.getDateToStrByFormat(DateUtil.addDaysToDate(new Date(), 1), "yyyyMMdd"));
					//将追号的投注截止时间更改
					tommorrowIssue.setEndtime(DateUtils.getDateByStrFormat(DateUtils.getDateToStrByFormat(DateUtil.addDaysToDate(new Date(), 1), "yyyy/MM/dd") + " " +DateUtils.getDateToStrByFormat(tommorrowIssue.getEndtime(), "HH:mm:ss"), "yyyy/MM/dd HH:mm:ss"));
				}
			}

			result.put(EAfterNumberType.TODAY.name(), todayListIssue);
			result.put(EAfterNumberType.TOMORROW.name(), tommorrowListIssue);
		}
		return result;
	}


	/**
	 * 根据期号和彩种类型获取投注关闭时间
	 * @param expect
	 * @param lotteryKind
	 * @return
	 * @throws Exception
	 */
	public Date getOrderCloseDate(String expect, ELotteryKind lotteryKind) throws Exception {
		String lotteryNum = expect.replace(DateUtils.getDateToStrByFormat(new Date(), "yyyyMMdd"), "").replace(DateUtils.getDateToStrByFormat(DateUtil.addDaysToDate(new Date(), 1), "yyyyMMdd"), "");
		if(lotteryKind.name().equals(ELotteryKind.FCSDDPC.name())) {
			lotteryNum = "000";
		} else if(lotteryKind.name().equals(ELotteryKind.BJPK10.name())) {
			Integer intLotteryNum = Integer.parseInt(expect);
			int realLotteryNum = (intLotteryNum - SystemConfigConstant.pk10BasetExpect) % 44;
			if(realLotteryNum < 10) {
				lotteryNum = "00" + realLotteryNum;
			} else if(realLotteryNum < 100) {
				lotteryNum = "0" + realLotteryNum;
			} else {
				lotteryNum = "" + realLotteryNum;
			}
			if (realLotteryNum == 0) {
				lotteryNum = "044";
			}
		} else if(lotteryKind.name().equals(ELotteryKind.HGFFC.name())) {
			Integer intLotteryNum = Integer.parseInt(expect);
			int realLotteryNum = (intLotteryNum - SystemConfigConstant.hgffcBasetExpect) % 880;
			if(realLotteryNum < 10) {
				lotteryNum = "00" + realLotteryNum;
			} else if(realLotteryNum < 100) {
				lotteryNum = "0" + realLotteryNum;
			} else {
				lotteryNum = "" + realLotteryNum;
			}
			if (realLotteryNum == 0) {
				lotteryNum = "880";
			}
		} else if(lotteryKind.name().equals(ELotteryKind.XJPLFC.name())) {
			Integer intLotteryNum = Integer.parseInt(expect);
			int realLotteryNum = (intLotteryNum - SystemConfigConstant.xjplfcBasetExpect) % 660;
			if(realLotteryNum < 10) {
				lotteryNum = "00" + realLotteryNum;
			} else if(realLotteryNum < 100) {
				lotteryNum = "0" + realLotteryNum;
			} else {
				lotteryNum = "" + realLotteryNum;
			}
			if (realLotteryNum == 0) {
				lotteryNum = "660";
			}
		} else if(lotteryKind.name().equals(ELotteryKind.TWWFC.name())) {
			Integer intLotteryNum = Integer.parseInt(expect);
			int realLotteryNum = (intLotteryNum - SystemConfigConstant.twwfcBasetExpect) % 203;
			if(realLotteryNum < 10) {
				lotteryNum = "00" + realLotteryNum;
			} else if(realLotteryNum < 100) {
				lotteryNum = "0" + realLotteryNum;
			} else {
				lotteryNum = "" + realLotteryNum;
			}
			if (realLotteryNum == 0) {
				lotteryNum = "203";
			}
		} else if(lotteryKind.name().equals(ELotteryKind.BJKS.name())) {
			Integer intLotteryNum = Integer.parseInt(expect);
			int realLotteryNum = (intLotteryNum - SystemConfigConstant.bjksBasetExpect) % 44;
			if(realLotteryNum < 10) {
				lotteryNum = "00" + realLotteryNum;
			} else if(realLotteryNum < 100) {
				lotteryNum = "0" + realLotteryNum;
			} else {
				lotteryNum = "" + realLotteryNum;
			}
			if (realLotteryNum == 0) {
				lotteryNum = "044";
			}
		}
		LotteryIssue issue = this.getExpectByLotteryNum(lotteryKind.name(), lotteryNum);

		if(issue == null){
			throw new Exception("未找到该期号.");
		}

		//获取期号的截止时间
		String endSFM = DateUtils.getDateToStrByFormat(issue.getEndtime(), "HH:mm:ss");
		String endDateStr = "";
		if(expect.contains(DateUtils.getDateToStrByFormat(new Date(), "yyyyMMdd"))){
			endDateStr = DateUtils.getDateToStrByFormat(new Date(), "yyyy-MM-dd");
		}else if(expect.contains(DateUtils.getDateToStrByFormat(DateUtil.addDaysToDate(new Date(), 1), "yyyyMMdd"))){
			endDateStr = DateUtils.getDateToStrByFormat(DateUtil.addDaysToDate(new Date(), 1), "yyyy-MM-dd");
		}else{
			if(lotteryKind.name().equals(ELotteryKind.FCSDDPC.name())) {
				//获取后面三位的期号
				int expectNum = Integer.parseInt(expect.substring(4));
				Integer yearNum = DateUtil.getYear(new Date()); //获取年份
				Date yearStartDate = DateUtils.getDateByStrFormat(yearNum + "/01/01","yyyy/MM/dd");
				//判断是否大于春节前一天期号，否则需要加上春节天数
				Date springBeforeDate = DateUtil.addDaysToDate(DateUtils.getDateByStrFormat(SystemConfigConstant.springFestivalStartDate, "yyyy-MM-dd"), -1);
				int baseExpectNum = DateUtil.daysBetween(yearStartDate, springBeforeDate);
				if(expectNum > baseExpectNum) {
					int springDays = 7;
					try {
						//春节预售期开始时间
						Date d3StopStartDate = DateUtil.getNowStartTimeByStart(DateUtils.getDateByStrFormat(SystemConfigConstant.springFestivalStartDate, "yyyy-MM-dd"));
						//春节预售期结束时间
						Date d3StopEndDate = DateUtil.getNowStartTimeByEnd(DateUtils.getDateByStrFormat(SystemConfigConstant.springFestivalEndDate, "yyyy-MM-dd"));
						springDays = DateUtil.daysBetween(d3StopStartDate, d3StopEndDate) + 1;
					} catch (ParseException e) {
						log.error("getNewExpectAndDiffTimeByLotteryKind方法日期转变异常.");
					}
					expectNum = expectNum + springDays;
				}
				//截止的日期是每一年加上期号的天数
				Date endDate = DateUtil.addDaysToDate(yearStartDate, expectNum);
				endDateStr = DateUtils.getDateToStrByFormat(endDate, "yyyy-MM-dd");
			} else if(lotteryKind.name().equals(ELotteryKind.BJPK10.name())) {
				Integer intLotteryNum = Integer.parseInt(expect);
				int betweenDays = (intLotteryNum - SystemConfigConstant.pk10BasetExpect) / 44;
				Date baseDate = DateUtils.getDateByStrFormat(SystemConfigConstant.pk10BaseDate, "yyyy-MM-dd");
				Date expectDate = DateUtil.addDaysToDate(baseDate, betweenDays);
				endDateStr = DateUtils.getDateToStrByFormat(expectDate, "yyyy-MM-dd");
			} else if(lotteryKind.name().equals(ELotteryKind.HGFFC.name())) {
				Integer intLotteryNum = Integer.parseInt(expect);
				int betweenDays = (intLotteryNum - SystemConfigConstant.hgffcBasetExpect) / 880;
				Date baseDate = DateUtils.getDateByStrFormat(SystemConfigConstant.hgffcBaseDate, "yyyy-MM-dd");
				Date expectDate = DateUtil.addDaysToDate(baseDate, betweenDays);
				endDateStr = DateUtils.getDateToStrByFormat(expectDate, "yyyy-MM-dd");
			} else if(lotteryKind.name().equals(ELotteryKind.XJPLFC.name())) {
				Integer intLotteryNum = Integer.parseInt(expect);
				int betweenDays = (intLotteryNum - SystemConfigConstant.xjplfcBasetExpect) / 660;
				Date baseDate = DateUtils.getDateByStrFormat(SystemConfigConstant.xjplfcBaseDate, "yyyy-MM-dd");
				Date expectDate = DateUtil.addDaysToDate(baseDate, betweenDays);
				endDateStr = DateUtils.getDateToStrByFormat(expectDate, "yyyy-MM-dd");
			} else if(lotteryKind.name().equals(ELotteryKind.TWWFC.name())) {
				Integer intLotteryNum = Integer.parseInt(expect);
				int betweenDays = (intLotteryNum - SystemConfigConstant.twwfcBasetExpect) / 203;
				Date baseDate = DateUtils.getDateByStrFormat(SystemConfigConstant.twwfcBaseDate, "yyyy-MM-dd");
				Date expectDate = DateUtil.addDaysToDate(baseDate, betweenDays);
				endDateStr = DateUtils.getDateToStrByFormat(expectDate, "yyyy-MM-dd");
			} else if(lotteryKind.name().equals(ELotteryKind.BJKS.name())) {
				Integer intLotteryNum = Integer.parseInt(expect);
				int betweenDays = (intLotteryNum - SystemConfigConstant.bjksBasetExpect) / 44;
				Date baseDate = DateUtils.getDateByStrFormat(SystemConfigConstant.bjksBaseDate, "yyyy-MM-dd");
				Date expectDate = DateUtil.addDaysToDate(baseDate, betweenDays);
				endDateStr = DateUtils.getDateToStrByFormat(expectDate, "yyyy-MM-dd");
			}  else {
				throw new Exception("期号不正确.");
			}
		}
		Date endCancel = DateUtils.getDateByStrFormat(endDateStr + " " + endSFM,"yyyy-MM-dd HH:mm:ss");
		return endCancel;
	}

	/**
	 * 查询彩种最高期数
	 * @return
	 */
	public List<LotteryIssue> getMaxLotteryNum(){
		return lotteryIssueMapper.getMaxLotteryNum();
	}

	/**
	 * 更新倒计时时间
	 * @return
	 */
	public int updateTimeBylotteryType(LotteryIssueAdjustVo record){

		return lotteryIssueMapper.updateTimeBylotteryType(record);
	}
}
