package com.team.lottery.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.extvo.LotteryWinQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.lotterywin.LotteryWinMapper;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.vo.LotteryWin;

@Service("lotteryWinService")
public class LotteryWinService {

	@Autowired
	private LotteryWinMapper lotteryWinMapper;
	
	public int deleteByPrimaryKey(Integer id){
    	return lotteryWinMapper.deleteByPrimaryKey(id);
    }

	public int insert(LotteryWin record){
    	return lotteryWinMapper.insert(record);
    }

    public int insertSelective(LotteryWin record){
    	return lotteryWinMapper.insertSelective(record);
    }

    public LotteryWin selectByPrimaryKey(Integer id){
       return lotteryWinMapper.selectByPrimaryKey(id);    	
    }

    public int updateByPrimaryKeySelective(LotteryWin record){
    	return lotteryWinMapper.updateByPrimaryKeySelective(record);
    }

    public int updateByPrimaryKey(LotteryWin record){
    	return lotteryWinMapper.updateByPrimaryKey(record);
    }
    
    /**
     * 加载对应玩法的奖金
     * @param lotteryTypeProtype
     * @return
     */
    public List<LotteryWin> getLotteryWinByKindPlay(String lotteryType,String lotteryProtype){
    	return lotteryWinMapper.getLotteryWinByKindPlay(lotteryType + ConstantUtil.LOTTERY_AND_PROTYPE_PLIT + lotteryProtype);
    }
    
    /**
     * 模糊查找,加载彩种的奖金对照表
     * @param lotteryKindLike
     * @return
     */
    public Map<String,LotteryWin> getLotteryWinByKindLike(String lotteryKindLike){
    	Map<String,LotteryWin> winMaps = new HashMap<String, LotteryWin>();
    	List<LotteryWin> wins = null;
    	if(lotteryKindLike.contains(ELotteryTopKind.SSC.name())){
    		wins = lotteryWinMapper.getLotteryWinByKindLike(ELotteryTopKind.SSC.name() + ConstantUtil.LOTTERY_AND_PROTYPE_PLIT);
    	}else if(lotteryKindLike.contains(ELotteryTopKind.FFC.name()) || lotteryKindLike.contains("LFC") || lotteryKindLike.contains("WFC")){
    		wins = lotteryWinMapper.getLotteryWinByKindLike(ELotteryTopKind.FFC.name() + ConstantUtil.LOTTERY_AND_PROTYPE_PLIT);
    	}else if(lotteryKindLike.contains(ELotteryTopKind.SYXW.name())){
    		wins = lotteryWinMapper.getLotteryWinByKindLike(ELotteryTopKind.SYXW.name() + ConstantUtil.LOTTERY_AND_PROTYPE_PLIT);
    	}else if(lotteryKindLike.contains(ELotteryTopKind.DPC.name())){
    		wins = lotteryWinMapper.getLotteryWinByKindLike(ELotteryTopKind.DPC.name() + ConstantUtil.LOTTERY_AND_PROTYPE_PLIT);
    	}else if(lotteryKindLike.contains(ELotteryTopKind.KS.name())){
    		wins = lotteryWinMapper.getLotteryWinByKindLike(ELotteryTopKind.KS.name() + ConstantUtil.LOTTERY_AND_PROTYPE_PLIT);
    	}else if(lotteryKindLike.contains(ELotteryTopKind.PK10.name()) || lotteryKindLike.equals(ELotteryKind.XYFT.getCode())){
    		wins = lotteryWinMapper.getLotteryWinByKindLike(ELotteryTopKind.PK10.name() + ConstantUtil.LOTTERY_AND_PROTYPE_PLIT);
    	}else{
    		throw new RuntimeException("未查找到该彩种的奖金配置");
    	}
    	//转成map映射 
    	for(LotteryWin win : wins){
    		if(win.getWinLevel() == 1){
    			winMaps.put(win.getLotteryTypeProtype(), win);
    		}else{
    			winMaps.put(win.getLotteryTypeProtype()+"_"+win.getWinLevel(), win);
    		}
    	}  
        return winMaps;
    }
    
    /**
     * 分页查找奖金对照表
     * @param query
     * @param page
     * @return
     */
    public Page getAllLotteryWinByQueryPage(LotteryWinQuery query, Page page) {
		page.setTotalRecNum(lotteryWinMapper.getAllLotteryWinsByQueryPageCount(query));
		page.setPageContent(lotteryWinMapper.getAllLotteryWinsByQueryPage(query,page.getStartIndex(),page.getPageSize()));
    	return page;
	}
    
}
