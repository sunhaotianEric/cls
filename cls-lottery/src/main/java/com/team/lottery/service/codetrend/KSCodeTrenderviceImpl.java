package com.team.lottery.service.codetrend;

import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.team.lottery.service.lotterykindplay.CodeTrendService;

/**
 * 快3走势图
 * @author Administrator
 *
 */
@Service("kSCodeTrenderviceImpl")
public class KSCodeTrenderviceImpl extends CodeTrendService{

	@Override
	public String getPlaykindContent(String openCode) {
		//和值、总和大小、总合单双、跨度、形态
		String[] ocs = openCode.split(",");
		String str = "";
		Integer sum = 0;
		for (int i = 0; i < ocs.length; i++) {
			sum += Integer.parseInt(ocs[i]);
		}
		str += sum+",";
		//总和大小:>10
		if (sum > 10) {
			str += "大,";
		}else {
			str += "小,";
		}
		if (sum%2==0) {
			str += "双,";
		}else {
			str += "单,";
		}
		TreeSet<Integer> set = new TreeSet<Integer>();
		for (int i = 0; i < ocs.length; i++) {
			set.add(Integer.parseInt(ocs[i]));
		}
		//跨度
		str += (set.last()-set.first())+",";
		if (set.size()==1) {
			str += "三同号";
		}else if (set.size()==2) {
			str += "二同号";
		}else if (set.size()==3) {
			Integer d_value = set.last()-set.first();
			if (d_value.equals(2)) {
				str += "三连号";
			}else {
				str += "三不同";
			}
		}
		return str;
	}

	@Override
	public String getNumPosition(String openCode, String lastNumPosition) {
		String[] ocs = openCode.split(",");
		String str = "";
		if (StringUtils.isBlank(lastNumPosition)) { //第一期数据
			for (int i = 0; i < ocs.length; i++) {
				for (int j = 1; j < 7; j++) {
					if (Integer.valueOf(ocs[i]).intValue()==j) {
						str += "0,";
					}else {
						str += "1,";
					}
				}
			}
		}else {//非第一期数据需要以第一期数据为根据
			String[] lastNumPositions = lastNumPosition.split(",");
			for (int i = 0; i < lastNumPositions.length; i++) {
				// i/6:ocs数组的位数；
				// ocs[i/6]:ocs数组元素
				// Integer.valueOf(ocs[i/6]).intValue()：ocs数组元素整型值
				// i%6：lastNumPosition中每6位循环一次设置位置信息
				if ((i%6+1)==Integer.valueOf(ocs[i/6]).intValue()) {
					str += "0,";
				}else {
					Integer num_position = Integer.parseInt(lastNumPositions[i])+1;//相应的位置遗漏值+1
					str += num_position+",";
				}
			}
		}
		str = str.substring(0,str.length()-1);
		return str;
	}
}
