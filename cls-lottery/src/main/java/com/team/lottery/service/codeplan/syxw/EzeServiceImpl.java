package com.team.lottery.service.codeplan.syxw;

import java.util.Random;

import org.springframework.stereotype.Service;

import com.team.lottery.service.lotterykindplay.LotterykindPlayCodeplanService;
import com.team.lottery.vo.CodePlan;

/**
 * 二中二
 * @author Administrator
 *
 */
@Service("syxw_codeplan_ezeServiceImpl")
public class EzeServiceImpl extends LotterykindPlayCodeplanService{

	@Override
	public String generateCodes() {
		int num1 = new Random().nextInt(11)+1;
		int num2 = new Random().nextInt(11)+1;
		String num1Str = "";
		String num2Str = "";
		if (num1>9) {
			num1Str = ""+num1;
		}else {
			num1Str = "0"+num1;
		}
		if (num2>9) {
			num2Str = ""+num2;
		}else {
			num2Str = "0"+num2;
		}
		return num1Str + "," + num2Str;
	}

	@Override
	public boolean getProstate(CodePlan codePlan, String openCode) {
		String codes = codePlan.getCodes();
		String[] codes2 = codes.split(",");
		for (int i = 0; i < codes2.length; i++) {
			if (codes2[i].startsWith("0")) {
				codes2[i] = codes2[i].substring(1);
			}
		}
		String[] openCodes = openCode.split(",");
		int right_num = 0;
		for (int i = 0; i < openCodes.length; i++) {
			if (codes2[0].equals(openCodes[i])) {
				right_num++;
			}
			if (codes2[1].equals(openCodes[i])) {
				right_num++;
			}
		}
		if (right_num==2) {
			return true;
		}
		return false;
	}

	@Override
	public int getCodeNum() {
		return 2;
	}

	@Override
	public String showCode(CodePlan codePlan) {
		String codes = codePlan.getCodes();
		return codes;
	}

}
