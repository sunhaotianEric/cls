package com.team.lottery.service;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.extvo.LotteryCodeXyImportQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.lotterycodexyimport.LotteryCodeXyImportMapper;
import com.team.lottery.vo.LotteryCodeXyImport;

/**
 * 幸运开奖号码导入功能Service层
 * 
 * @author Jamine
 *
 */
@Service("lotteryCodeXyImportService")
public class LotteryCodeXyImportService {
	@Autowired
	private LotteryCodeXyImportMapper lotteryCodeXyImportMapper;

	public Integer deleteByPrimaryKey(Integer id) {
		return lotteryCodeXyImportMapper.deleteByPrimaryKey(id);
	}

	public Integer insertSelective(LotteryCodeXyImport record) {
		return lotteryCodeXyImportMapper.insertSelective(record);
	}

	public LotteryCodeXyImport selectByPrimaryKey(Integer id) {
		return lotteryCodeXyImportMapper.selectByPrimaryKey(id);
	}

	public Integer updateByPrimaryKeySelective(LotteryCodeXyImport record) {
		return lotteryCodeXyImportMapper.updateByPrimaryKeySelective(record);
	}

	/**
	 * 分页查询所有的幸运开奖号码导入对象.
	 * 
	 * @param query
	 * @param page
	 * @return
	 */
	public Page getAllLotteryCodeXyImportByQueryPage(LotteryCodeXyImportQuery query, Page page) {
		page.setTotalRecNum(lotteryCodeXyImportMapper.getAllLotteryCodesXyImportByQueryPageCount(query));
		page.setPageContent(lotteryCodeXyImportMapper.getAllLotteryCodesXyImportByQueryPage(query, page.getStartIndex(), page.getPageSize()));
		return page;
	}

	/**
	 * 往数据库添加数据,要进行事务处理
	 * 
	 * @param lotteryCodeXyImportList
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public Integer insertListSelective(List<LotteryCodeXyImport> lotteryCodeXyImportList) {
		// 初始化插入条数为0
		Integer importSuccessCount = 0;
		try {
			Iterator<LotteryCodeXyImport> iterator = lotteryCodeXyImportList.iterator();
			while (iterator.hasNext()) {
				LotteryCodeXyImport lotteryCodeXyImport = iterator.next();
				// 往数据库添加数据.
				lotteryCodeXyImportMapper.insertSelective(lotteryCodeXyImport);
				importSuccessCount++;
			}
			return importSuccessCount;
		} catch (Exception e) {
			// 出现异常的时候成功条数设置为0;
			return importSuccessCount;
		}
	}
	
	/**
	 * 开奖处理从LotteryCodeXyImport表中取数据处理
	 * @param lotteryType
	 * @param minIssueNo
	 * @param maxIssueNo
	 * @return
	 */
	public List<LotteryCodeXyImport> getByLotteryTypeAndNumInterval(String lotteryType,String minIssueNo,String maxIssueNo){
		return lotteryCodeXyImportMapper.getByLotteryTypeAndNumInterval(lotteryType, minIssueNo, maxIssueNo);
	}

	/**
	 * 
	 * @param lotteryCodeXyImports
	 * @return
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int setBatchUsedStatus1(List<LotteryCodeXyImport> lotteryCodeXyImports){
		return lotteryCodeXyImportMapper.updateBatch(lotteryCodeXyImports);
	}
}
