package com.team.lottery.service.codetrend;

import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.team.lottery.service.lotterykindplay.CodeTrendService;

@Service("sYXWCodeTrendServiceImpl")
public class SYXWCodeTrendServiceImpl extends CodeTrendService{

	@Override
	public String getPlaykindContent(String openCode) {
		//跨度、重号个数
		String str = "";
		String[] ocs = openCode.split(",");
		TreeSet<Integer> set = new TreeSet<Integer>();
		for (int i = 0; i < ocs.length; i++) {
			if (ocs[i].startsWith("0")) {
				set.add(Integer.parseInt(ocs[i].substring(1)));
			}else {
				set.add(Integer.parseInt(ocs[i]));
			}
		}
		str += (set.last()-set.first())+","+(ocs.length-set.size());
		return str;
	}

	@Override
	public String getNumPosition(String openCode, String lastNumPosition) {
		String[] ocs = openCode.split(",");
		for (int i = 0; i < ocs.length; i++) {
			if (ocs[i].startsWith("0")) {
				ocs[i] = ocs[i].substring(1);
			}
		}
		String str = "";
		if (StringUtils.isBlank(lastNumPosition)) { //第一期数据
			for (int i = 0; i < ocs.length; i++) {
				for (int j = 1; j < 12; j++) {
					if (Integer.valueOf(ocs[i]).intValue()==j) {
						str += "0,";
					}else {
						str += "1,";
					}
				}
			}
		}else {//非第一期数据需要以第一期数据为根据
			String[] lastNumPositions = lastNumPosition.split(",");
			for (int i = 0; i < lastNumPositions.length; i++) {
				// i/11:ocs数组的位数；
				// ocs[i/11]:ocs数组元素
				// Integer.valueOf(ocs[i/11]).intValue()：ocs数组元素整型值
				// i%11：lastNumPosition中每11位循环一次设置位置信息
				if ((i%11+1)==Integer.valueOf(ocs[i/11]).intValue()) {
					str += "0,";
				}else {
					Integer num_position = Integer.parseInt(lastNumPositions[i])+1;//相应的位置遗漏值+1
					str += num_position+",";
				}
			}
		}
		str = str.substring(0,str.length()-1);
		return str;
	}
}
