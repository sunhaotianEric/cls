package com.team.lottery.service.codeplan.pk10;

import java.util.Random;

import org.springframework.stereotype.Service;

import com.team.lottery.service.lotterykindplay.LotterykindPlayCodeplanService;
import com.team.lottery.vo.CodePlan;

/**
 * 第三名单双
 * @author Administrator
 *
 */
@Service("pk10_dsmdsServiceImpl")
public class DsmdsServiceImpl extends LotterykindPlayCodeplanService{

	@Override
	public String generateCodes() {
		int num = new Random().nextInt(11);
		if (num%2==0) {
			return "双|0";
		}
		return "单|1";
	}

	@Override
	public boolean getProstate(CodePlan codePlan, String openCode) {
		String codes = codePlan.getCodes();
		String code2 = codes.split("\\|")[1];
		String open_code = openCode.split(",")[2];
		Integer open_codeInteger = Integer.parseInt(open_code);
		if ((code2.equals("0")&&(open_codeInteger%2==0)) || (code2.equals("1")&&(open_codeInteger%2==1))) {
			return true;
		}
		return false;
	}

	@Override
	public int getCodeNum() {
		return 1;
	}

	@Override
	public String showCode(CodePlan codePlan) {
		String codes = codePlan.getCodes();
		String code2 = codes.split("\\|")[0];
		return code2;
	}

}
