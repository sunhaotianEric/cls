package com.team.lottery.service.codeplan.ssc;

import java.util.Random;

import org.springframework.stereotype.Service;

import com.team.lottery.service.lotterykindplay.LotterykindPlayCodeplanService;
import com.team.lottery.vo.CodePlan;

/**
 * 总和单双
 * @author Administrator
 *
 */
@Service("ssc_codeplan_zhdsServiceImpl")
public class ZhdsServiceImpl extends LotterykindPlayCodeplanService{

	@Override
	public String generateCodes() {
		String codes = "";
		int sum = 0;
		for (int i = 0; i < 5; i++) {
			int num = new Random().nextInt(10);
			sum += num;
		}
		if (sum%2==0) {
			codes = "双|0";
		}else {
			codes = "单|1";
		}
		return codes;
	}

	@Override
	public boolean getProstate(CodePlan codePlan, String openCode) {
		String codes = codePlan.getCodes();
		String codes2 = codes.split("\\|")[1];
		String[] ocs = openCode.split(",");
		Integer ocs_sum = 0;
		for (int i = 0; i < ocs.length; i++) {
			ocs_sum += Integer.parseInt(ocs[i]);
		}
		if ((codes2.equals("0")&&(ocs_sum%2==0))||(codes2.equals("1")&&(ocs_sum%2==1))) {
			return true;
		}
		return false;
	}

	@Override
	public int getCodeNum() {
		return 1;
	}

	@Override
	public String showCode(CodePlan codePlan) {
		String codes = codePlan.getCodes();
		String codes2 = codes.split("\\|")[0];
		return codes2;
	}

}
