package com.team.lottery.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.lotterywinlhc.LotteryWinLhcMapper;
import com.team.lottery.vo.LotteryWinLhc;

@Service("lotteryWinLhcService")
public class LotteryWinLhcService {
	private static Logger logger = LoggerFactory.getLogger(LotteryWinLhcService.class);
	
	@Autowired
	public LotteryWinLhcMapper lotteryWinLhcMapper;
	
	public int deleteByPrimaryKey(Integer id){
		return lotteryWinLhcMapper.deleteByPrimaryKey(id);
	}

	public int insert(LotteryWinLhc record){
		return lotteryWinLhcMapper.insert(record);
	}

	public int insertSelective(LotteryWinLhc record){
		return lotteryWinLhcMapper.insertSelective(record);
	}

	public LotteryWinLhc selectByPrimaryKey(Integer id){
		return lotteryWinLhcMapper.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(LotteryWinLhc record){
		return lotteryWinLhcMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(LotteryWinLhc record){
		return lotteryWinLhcMapper.updateByPrimaryKey(record);
	}
	
	public List<LotteryWinLhc> getLotteryWinLhcByLotteryTypeProtype(LotteryWinLhc query){
		return lotteryWinLhcMapper.getLotteryWinLhcByLotteryTypeProtype(query);
	}
	
	/**
	 * 获取所有业务系统六合彩彩种赔率
	 * @author listgoo
	 * @Description: 
	 * @date: 2019年6月6日
	 * @return
	 */
	public List<LotteryWinLhc> getLotteryWinLhc(){
		return lotteryWinLhcMapper.getLotteryWinLhc();
	}
	
	@Transactional(readOnly=false,rollbackFor=Exception.class)
	public int bathUpdateByPrimaryKey(LotteryWinLhc[] records){
		for(LotteryWinLhc record:records ){
			lotteryWinLhcMapper.updateByPrimaryKeySelective(record);
		}
		return 1;
	}
	
	public Page getLotteryWinLhcByPage(LotteryWinLhc query,Page page){
		
		try {
			page.setPageContent(lotteryWinLhcMapper.getLotteryWinLhcByPage(query, page.getStartIndex(), page.getPageSize()));
			page.setTotalRecNum(lotteryWinLhcMapper.getLotteryWinLhcCount(query));
		} catch (Exception e) {
			logger.error("查询赔率失败", e);
		}
		 return page;
	}
	
	  /**
	    * 根据种类获取玩法赔率
	    * @param query
	    * @return
	    */
	public List<LotteryWinLhc> getLotteryWinLhcByKind(LotteryWinLhc query){
		return lotteryWinLhcMapper.getLotteryWinLhcByKind(query);		
	}
	
	  /**
	    * 根据种类获取玩法最低赔率
	    * @param query
	    * @return
	    */
	public List<LotteryWinLhc> getMinLotteryWinLhcByKind(LotteryWinLhc query){
		return lotteryWinLhcMapper.getMinLotteryWinLhcByKind(query,query.getCodeArr());		
	}
	  /**
	    * 初始化六合彩赔率数据
	    * @param query
	    * @return
	    */
	public void initLotteryLhcCode(LotteryWinLhc query){
		lotteryWinLhcMapper.initLotteryLhcCode(query);
	}
	
	
}
