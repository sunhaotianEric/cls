package com.team.lottery.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.enums.EMessageType;
import com.team.lottery.mapper.lotterycache.LotteryCacheMapper;
import com.team.lottery.system.message.MessageQueueCenter;
import com.team.lottery.system.message.messagetype.LotteryCacheMessage;
import com.team.lottery.vo.LotteryCache;

@Service("lotteryCacheService")
public class LotteryCacheService {
	
	@Autowired
	private LotteryCacheMapper lotteryCacheMapper;
	
    public int deleteByPrimaryKey(Long id){
    	return lotteryCacheMapper.deleteByPrimaryKey(id);
    }

    public int insert(LotteryCache record){
    	return lotteryCacheMapper.insert(record);
    }

    public int insertSelective(LotteryCache record){
    	record.setCreatedDate(new Date());
    	return lotteryCacheMapper.insertSelective(record);
    }

    public LotteryCache selectByPrimaryKey(Long id){
    	return lotteryCacheMapper.selectByPrimaryKey(id);
    }

    public int updateByPrimaryKeySelective(LotteryCache record){
    	return lotteryCacheMapper.updateByPrimaryKeySelective(record);
    }

    public int updateByPrimaryKey(LotteryCache record){
    	return lotteryCacheMapper.updateByPrimaryKey(record);
    }
    
    /**
     * 根据订单,获取投注号码的缓存信息
     * @param orderId
     * @return
     */
    public List<LotteryCache> getLotteryCacheMsg(Long orderId){
    	return lotteryCacheMapper.getLotteryCacheMsg(orderId);
    }
    
    /**
     * 发送添加订单缓存消息
     * @param record
     */
    public void sendLotteryCacheMsg(LotteryCache record) {
    	LotteryCacheMessage message = new LotteryCacheMessage();
    	message.setLotteryCache(record);
    	MessageQueueCenter.putMessage(message);
    }
}
