package com.team.lottery.service.lotterykindplay;

/**
 * 走势图处理抽象接口类
 * @author Administrator
 *
 */
public abstract class CodeTrendService {
	
	/**
	 * 
	 * @param openCode:本期开奖号码
	 * @param lotteryName：本期彩种
	 * @return
	 */
	public abstract String getPlaykindContent(String openCode);
	
	/**
	 * 
	 * @param openCode:本期开奖号码
	 * @param lotteryName：本期彩种
	 * @param lastNumPosition:上期走势位数据
	 * @return
	 */
	public abstract String getNumPosition(String openCode,String lastNumPosition);

}
