package com.team.lottery.service.codeplan.xyeb;

import java.util.Random;

import org.springframework.stereotype.Service;

import com.team.lottery.service.lotterykindplay.LotterykindPlayCodeplanService;
import com.team.lottery.vo.CodePlan;

/**
 * 混合单双
 * @author Administrator
 *
 */
@Service("xyeb_codeplan_hhdsServiceImpl")
public class HhdsServiceImpl extends LotterykindPlayCodeplanService{

	@Override
	public String generateCodes() {
		int num1 = new Random().nextInt(10);
		int num2 = new Random().nextInt(10);
		int num3 = new Random().nextInt(10);
		int sum = num1 + num2 + num3;
		if (sum%2==0) {
			return "双|0";
		}
		return "单|1";
	}

	@Override
	public boolean getProstate(CodePlan codePlan, String openCode) {
		String codes = codePlan.getCodes();
		String codes2 = codes.split("\\|")[1];
		String[] ocs = openCode.split(",");
		if (((Integer.parseInt(ocs[0])+Integer.parseInt(ocs[1])+Integer.parseInt(ocs[2]))%2==0&&codes2.equals("0")) ||
				((Integer.parseInt(ocs[0])+Integer.parseInt(ocs[1])+Integer.parseInt(ocs[2]))%2==1&&codes2.equals("1"))) {
			return true;
		}
		return false;
	}

	@Override
	public int getCodeNum() {
		return 1;
	}

	@Override
	public String showCode(CodePlan codePlan) {
		String codes = codePlan.getCodes();
		String codes2 = codes.split("\\|")[0];
		return codes2;
	}

}
