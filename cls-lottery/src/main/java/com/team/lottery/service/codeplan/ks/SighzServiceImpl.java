package com.team.lottery.service.codeplan.ks;

import java.util.Random;

import org.springframework.stereotype.Service;

import com.team.lottery.service.lotterykindplay.LotterykindPlayCodeplanService;
import com.team.lottery.vo.CodePlan;

/**
 * 四个和值
 * @author Administrator
 *
 */
@Service("ks_codeplan_sighzServiceImpl")
public class SighzServiceImpl extends LotterykindPlayCodeplanService{

	@Override
	public String generateCodes() {
		String sums = "";
		for (int i = 0; i < 4; i++) {
			int num1 = new Random().nextInt(6)+1;
			int num2 = new Random().nextInt(6)+1;
			int num3 = new Random().nextInt(6)+1;
			if (i==3) {
				sums += num1+num2+num3;
			}else {
				sums += num1+num2+num3 + " ";
			}
		}
		return sums;
	}

	@Override
	public boolean getProstate(CodePlan codePlan, String openCode) {
		String codes = codePlan.getCodes();
		String[] code = codes.split(" ");
		String[] ocs = openCode.split(",");
		Integer openCInteger = Integer.parseInt(ocs[0])+Integer.parseInt(ocs[1])+Integer.parseInt(ocs[2]);
		for (int i = 0; i < code.length; i++) {
			if (openCInteger.equals(Integer.parseInt(code[i]))) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int getCodeNum() {
		return 4;
	}

	@Override
	public String showCode(CodePlan codePlan) {
		return codePlan.getCodes();
	}

}
