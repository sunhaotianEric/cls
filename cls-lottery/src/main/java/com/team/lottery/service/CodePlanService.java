package com.team.lottery.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.mapper.codeplan.CodePlanMapper;
import com.team.lottery.vo.CodePlan;

/**
 * 计划列表处理类
 * @author Administrator
 *
 */
@Service("codePlanService")
public class CodePlanService {
	
	private static Logger logger = LoggerFactory.getLogger(CodePlanService.class);
			
	@Autowired
	private CodePlanMapper codePlanMapper;
	
	@Transactional(readOnly=false,rollbackFor=Exception.class)
	public int insertSelective(CodePlan record){
		return codePlanMapper.insertSelective(record);
	}
	
	public int deleteByPrimaryKey(Long id){
		return codePlanMapper.deleteByPrimaryKey(id);
	}
	
	public CodePlan selectByPrimaryKey(Long id){
		return codePlanMapper.selectByPrimaryKey(id);
	}
	
	@Transactional(readOnly=false,rollbackFor=Exception.class)
	public int updateByexpect(CodePlan record){
		return codePlanMapper.updateByexpect(record);
	}
	
	/**
	 * 根据期号查找订单
	 * @param expect
	 * @param lotteryType
	 * @return
	 */
	public List<CodePlan> getAllByexpect(String expect,String lotteryType){
		return codePlanMapper.getAllByexpect(expect,lotteryType);
	}
	
	/**
	 * 批量插入计划列表数据
	 * @param codePlans
	 * @return
	 */
	@Transactional(readOnly=false,rollbackFor=Exception.class)
	public int insertBatch(List<CodePlan> codePlans){
		return codePlanMapper.insertBatch(codePlans);
	}
	
	/**
	 * 批量更新数据
	 * @param codePlans
	 * @return
	 */
	@Transactional(readOnly=false,rollbackFor=Exception.class)
	public int updateBatch(List<CodePlan> codePlans){
		return codePlanMapper.updateBatch(codePlans);
	}
	
	/**
	 * 查询最近几期的某彩种玩法的计划列表
	 * @param lotteryType
	 * @param playkind
	 * @param lateExpectNum
	 * @return
	 */
	public List<CodePlan> getByLotteryType(String lotteryType,String playkind,int lateExpectNum){
		return codePlanMapper.getByLotteryType(lotteryType, playkind, lateExpectNum);
	}
}
