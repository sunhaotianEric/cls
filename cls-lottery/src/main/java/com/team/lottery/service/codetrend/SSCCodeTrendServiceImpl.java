package com.team.lottery.service.codetrend;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.team.lottery.service.lotterykindplay.CodeTrendService;

/**
 * 时时彩走势图数据计算
 * @author Administrator
 *
 */
@Service("sSCCodeTrendServiceImpl")
public class SSCCodeTrendServiceImpl extends CodeTrendService{
	
	@Override
	public String getPlaykindContent(String openCode){
		String[] ocs = openCode.split(",");
		//万位、千位、百位、十位、个位、前三、中三、后三、和值、大小、单双、牛牛、1VS2、1VS3、1VS4、1VS5、2VS3、2VS4、2VS5、3VS4、3VS5、4VS5
		String str = "";
		str = getStr(Integer.parseInt(ocs[0]))+","+getStr(Integer.parseInt(ocs[1]))+","+getStr(Integer.parseInt(ocs[2]))+
				","+getStr(Integer.parseInt(ocs[3]))+","+getStr(Integer.parseInt(ocs[4]))+",";
		str += get3Str(ocs[0], ocs[1], ocs[2])+","+get3Str(ocs[1], ocs[2], ocs[3])+","+get3Str(ocs[2], ocs[3], ocs[4])+",";
		str += getHeZhi(openCode)+",";
		str += getDXDS(openCode);
		str += getNiuniu(openCode)+",";
		str += getVSs(openCode);
		return str;
	}
	
	@Override
	public String getNumPosition(String openCode, String lastNumPosition) {
		String[] ocs = openCode.split(",");
		String str = "";
		if (StringUtils.isBlank(lastNumPosition)) { //第一期数据
			for (int i = 0; i < ocs.length; i++) {
				for (int j = 0; j < 10; j++) {
					if (Integer.valueOf(ocs[i]).intValue()==j) {
						str += "0,";
					}else {
						str += "1,";
					}
				}
			}
		}else {//非第一期数据需要以第一期数据为根据
			String[] lastNumPositions = lastNumPosition.split(",");
			for (int i = 0; i < lastNumPositions.length; i++) {
				// i/10:ocs数组的位数；
				// ocs[i/10]:ocs数组元素
				// Integer.valueOf(ocs[i/10]).intValue()：ocs数组元素整型值
				// i%10：lastNumPosition中每10位循环一次设置位置信息
				if ((i%10)==Integer.valueOf(ocs[i/10]).intValue()) {
					str += "0,";
				}else {
					Integer num_position = Integer.parseInt(lastNumPositions[i])+1;//相应的位置遗漏值+1
					str += num_position+",";
				}
			}
		}
		str = str.substring(0,str.length()-1);
		return str;
	}
	
	//1VS2、1VS3、1VS4、1VS5、2VS3、2VS4、2VS5、3VS4、3VS5、4VS5
	public static String getVSs(String openCode){
		String[] ocs = openCode.split(",");
		Integer num1 = Integer.parseInt(ocs[0]);
		Integer num2 = Integer.parseInt(ocs[1]);
		Integer num3 = Integer.parseInt(ocs[2]);
		Integer num4 = Integer.parseInt(ocs[3]);
		Integer num5 = Integer.parseInt(ocs[4]);
		String str = "";
		if (num1>num2) {
			str += "龙,";
		}else if (num1.equals(num2)) {
			str += "和,";
		}else {
			str += "虎,";
		}
		if (num1>num3) {
			str += "龙,";
		}else if (num1.equals(num3)) {
			str += "和,";
		}else {
			str += "虎,";
		}
		if (num1>num4) {
			str += "龙,";
		}else if (num1.equals(num4)) {
			str += "和,";
		}else {
			str += "虎,";
		}
		if (num1>num5) {
			str += "龙,";
		}else if (num1.equals(num5)) {
			str += "和,";
		}else {
			str += "虎,";
		}
		if (num2>num3) {
			str += "龙,";
		}else if (num2.equals(num3)) {
			str += "和,";
		}else {
			str += "虎,";
		}
		if (num2>num4) {
			str += "龙,";
		}else if (num2.equals(num4)) {
			str += "和,";
		}else {
			str += "虎,";
		}
		if (num2>num5) {
			str += "龙,";
		}else if (num2.equals(num5)) {
			str += "和,";
		}else {
			str += "虎,";
		}
		if (num3>num4) {
			str += "龙,";
		}else if (num3.equals(num4)) {
			str += "和,";
		}else {
			str += "虎,";
		}
		if (num3>num5) {
			str += "龙,";
		}else if (num3.equals(num5)) {
			str += "和,";
		}else {
			str += "虎,";
		}
		if (num4>num5) {
			str += "龙";
		}else if (num4.equals(num5)) {
			str += "和";
		}else {
			str += "虎";
		}
		return str;
	}
	
	//牛牛
	public static String getNiuniu(String openCode){
		String[] ocs = openCode.split(",");
		Integer[] ocIntegers = {Integer.parseInt(ocs[0]),Integer.parseInt(ocs[1]),Integer.parseInt(ocs[2]),Integer.parseInt(ocs[3]),Integer.parseInt(ocs[4])};
		String str = "";
		if ((ocIntegers[0]+ocIntegers[1]+ocIntegers[2])%10==0) {
			str += "牛";
			str += getUpperCase((ocIntegers[3]+ocIntegers[4])%10);
		}else if ((ocIntegers[0]+ocIntegers[1]+ocIntegers[3])%10==0) {
			str += "牛";
			str += getUpperCase((ocIntegers[2]+ocIntegers[4])%10);
		}else if ((ocIntegers[0]+ocIntegers[1]+ocIntegers[4])%10==0) {
			str += "牛";
			str += getUpperCase((ocIntegers[3]+ocIntegers[2])%10);
		}else if ((ocIntegers[0]+ocIntegers[2]+ocIntegers[3])%10==0) {
			str += "牛";
			str += getUpperCase((ocIntegers[1]+ocIntegers[4])%10);
		}else if ((ocIntegers[0]+ocIntegers[2]+ocIntegers[4])%10==0) {
			str += "牛";
			str += getUpperCase((ocIntegers[3]+ocIntegers[1])%10);
		}else if ((ocIntegers[0]+ocIntegers[3]+ocIntegers[4])%10==0) {
			str += "牛";
			str += getUpperCase((ocIntegers[1]+ocIntegers[2])%10);
		}else if ((ocIntegers[1]+ocIntegers[2]+ocIntegers[3])%10==0) {
			str += "牛";
			str += getUpperCase((ocIntegers[0]+ocIntegers[4])%10);
		}else if ((ocIntegers[1]+ocIntegers[2]+ocIntegers[4])%10==0) {
			str += "牛";
			str += getUpperCase((ocIntegers[0]+ocIntegers[3])%10);
		}else if ((ocIntegers[1]+ocIntegers[3]+ocIntegers[4])%10==0) {
			str += "牛";
			str += getUpperCase((ocIntegers[0]+ocIntegers[2])%10);
		}else if ((ocIntegers[2]+ocIntegers[3]+ocIntegers[4])%10==0) {
			str += "牛";
			str += getUpperCase((ocIntegers[0]+ocIntegers[1])%10);
		}else {
			str += "无牛";
		}
		return str;
	}
	
	//牛牛按照数字返回大写
	public static String getUpperCase(Integer i){
		if (i.equals(0)) {
			return "牛";
		}else if (i.equals(1)) {
			return "一";
		}else if (i.equals(2)) {
			return "二";
		}else if (i.equals(3)) {
			return "三";
		}else if (i.equals(4)) {
			return "四";
		}else if (i.equals(5)) {
			return "五";
		}else if (i.equals(6)) {
			return "六";
		}else if (i.equals(7)) {
			return "七";
		}else if (i.equals(8)) {
			return "八";
		}else if (i.equals(9)) {
			return "九";
		}
		return "";
	}
	
	//和值大小单双
	public static String getDXDS(String openCode){
		String str = "";
		String[] ocs = openCode.split(",");
		Integer sum = Integer.parseInt(ocs[0])+Integer.parseInt(ocs[1])+Integer.parseInt(ocs[2])+Integer.parseInt(ocs[3])+Integer.parseInt(ocs[4]);
		if (sum>22) {
			str += "大,";
		}else {
			str += "小,";
		}
		if (sum%2==0) {
			str += "双,";
		}else {
			str += "单,";
		}
		return str;
	}
	
	//和值
	public static String getHeZhi(String openCode){
		String[] ocs = openCode.split(",");
		Integer hezhi = Integer.parseInt(ocs[0])+Integer.parseInt(ocs[1])+Integer.parseInt(ocs[2])+Integer.parseInt(ocs[3])+Integer.parseInt(ocs[4]);
		return hezhi+"";
	}
	
	//前三、中三、后三
	public static String get3Str(String code1,String code2,String code3){
		Set<String> set = new HashSet<String>();
		set.add(code1);
		set.add(code2);
		set.add(code3);
		if (set.size()==1) {
			return "豹子";
		}else if (set.size()==2) {
			return "组三";
		}else if (set.size()==3) {
			return "组六";
		}
		return "";
	}
	
	//返回某位置上的大小单双
	public static String getStr(Integer i){
		String str = "";
		if (i>4) {
			str += "大";
		}else {
			str += "小";
		}
		if (i%2==0) {
			str += "双";
		}else {
			str += "单";
		}
		return str;
	}

}
