package com.team.lottery.system.message.handler;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.system.cache.LotteryIssueCache;
import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.BaseMessageHandler;
import com.team.lottery.system.message.messagetype.LotteryIssueCacheMessage;

public class LotteryIssueCacheHandler extends BaseMessageHandler implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger logger = LoggerFactory.getLogger(LotteryIssueCacheHandler.class);
	@Override
	public void dealMessage(BaseMessage message) {
		try {
			logger.info("接受redis订阅消息，开始更新彩种开奖时间缓存...");
			LotteryIssueCacheMessage lotteryIssueCacheMessage = (LotteryIssueCacheMessage) message;
		    LotteryIssueCache.getInstance().refreshLotteryIssueCache();
		} catch (Exception e) {
			logger.error("处理更新单个彩种开奖时间缓存发生错误...", e);
		}
	}

}
