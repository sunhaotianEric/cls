package com.team.lottery.system.cache;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.extvo.LotteryCodeQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.LotteryCodeService;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.vo.LotteryCode;

/**
 * 最新开奖号码缓存,此类缓存弃用，改用redis实现
 * @deprecated
 * @author gs
 *
 */
public class LotteryCodeCache {

	private static Logger logger = LoggerFactory.getLogger(LotteryCodeCache.class);
	private static LotteryCodeCache instance = new LotteryCodeCache();
	private LotteryCodeCache() {}
	
	public static LotteryCodeCache getInstance() {
		return instance;
	}
	
	//最新开奖号码
	private static Map<String, LotteryCode> latestLotteryCodeMap = new ConcurrentHashMap<String, LotteryCode>();
	//最近十期的开奖号码
	private static Map<String, List<LotteryCode>> nearestTenLotteryCodeMap = new ConcurrentHashMap<String, List<LotteryCode>>();
	//昨天最后开奖期号
	public static Map<String,Integer> yesterDayLatestLotteryCodeMap = new ConcurrentHashMap<String, Integer>();
	/**
	 * 初始化最新开奖号码缓存
	 */
	@SuppressWarnings("unchecked")
	public static void initCache() {
		logger.info("开始初始化开奖号码缓存...");
		ELotteryKind[] lotteryKinds = ELotteryKind.values();
		LotteryCodeService lotteryCodeService = ApplicationContextUtil.getBean("lotteryCodeService");
		String bizsystem = "";
		for(ELotteryKind lotteryKind : lotteryKinds) {
			//加载最新的开奖号码
			LotteryCode lotteryCode = lotteryCodeService.getLastLotteryCodeOnRedis(lotteryKind, bizsystem);
			if(lotteryCode != null) {
				instance.putLatestLotteryCode(lotteryKind, lotteryCode);	
			}
			//加载最近十期的开奖号码
			LotteryCodeQuery query = new LotteryCodeQuery();
			query.setLotteryName(lotteryKind.name());
			Page page = new Page(10);
			page = lotteryCodeService.getAllLotteryCodeByQueryPage(query, page);
			List<LotteryCode> nearestTenLotteryCode = (List<LotteryCode>)page.getPageContent();
			if(CollectionUtils.isNotEmpty(nearestTenLotteryCode)) {
				instance.putNearestTenLotteryCode(lotteryKind, nearestTenLotteryCode);
			}
		}
		
		logger.info("初始化开奖号码缓存结束...");
	}
	
		
	/**
	 * 从缓存中获取最新的开奖号码
	 * @param lotteryKind
	 * @return
	 */
	public LotteryCode getLatestLotteryCodeByKind(ELotteryKind lotteryKind) {
		LotteryCode lotteryCode = latestLotteryCodeMap.get(lotteryKind.getCode());
		return lotteryCode;
	}
	
	/**
	 * 根据彩种类型放置最新开奖号码到缓存Map中
	 * @param lotteryKind
	 * @param lotteryCode
	 */
	public void putLatestLotteryCode(ELotteryKind lotteryKind, LotteryCode lotteryCode) {
		latestLotteryCodeMap.put(lotteryKind.getCode(), lotteryCode);
	}
	
	/**
	 * 从缓存中获取最近十期的开奖号码
	 * @param lotteryKind
	 * @return
	 */
	public List<LotteryCode> getNearestTenLotteryCode(ELotteryKind lotteryKind) {
		List<LotteryCode> nearestTenLotteryCode = nearestTenLotteryCodeMap.get(lotteryKind.getCode());
		return nearestTenLotteryCode;
	}
	
	/**
	 * 根据彩种类型放置最近十期的开奖号码到缓存Map中
	 * @param lotteryKind
	 * @param nearestTenLotteryCode
	 */
	public void putNearestTenLotteryCode(ELotteryKind lotteryKind, List<LotteryCode> nearestTenLotteryCode) {
		nearestTenLotteryCodeMap.put(lotteryKind.getCode(), nearestTenLotteryCode);
	}
	

}
