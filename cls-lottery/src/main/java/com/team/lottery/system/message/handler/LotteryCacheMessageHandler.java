package com.team.lottery.system.message.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.team.lottery.service.LotteryCacheService;
import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.BaseMessageHandler;
import com.team.lottery.system.message.messagetype.LotteryCacheMessage;
import com.team.lottery.util.ApplicationContextUtil;

@Component
public class LotteryCacheMessageHandler extends BaseMessageHandler {
	
	private static final long serialVersionUID = -2960267991969569755L;
	
	private static Logger logger = LoggerFactory.getLogger(LotteryCacheMessageHandler.class);

	@Override
	public void dealMessage(BaseMessage message) {
		try {
			LotteryCacheMessage lotteryCacheMessage = (LotteryCacheMessage)message;
			LotteryCacheService lotteryCacheService = ApplicationContextUtil.getBean("lotteryCacheService");
			if(lotteryCacheService != null) {
				lotteryCacheService.insertSelective(lotteryCacheMessage.getLotteryCache());
			}
		} catch (Exception e) {
			logger.error("处理投注缓存消息发生错误...", e);
		}
	}

}
