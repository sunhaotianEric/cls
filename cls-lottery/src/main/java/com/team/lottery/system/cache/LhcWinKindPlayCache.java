package com.team.lottery.system.cache;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.service.LotteryWinLhcService;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.vo.LotteryWinLhc;

/**
 * 六合彩玩法赔率缓存
 * @author listgoo
 * @Description: 
 * @date: 2019年6月7日
 */
public class LhcWinKindPlayCache {
	
	private static Logger logger = LoggerFactory.getLogger(LhcWinKindPlayCache.class);
	
	// 存储六合彩赔率
	public static List<LotteryWinLhc> listLotteryWinLhc = new ArrayList<LotteryWinLhc>();

	private static LotteryWinLhcService lotteryWinLhcService;
	
	/**
	 * 重新获取六合彩玩法赔率
	 * @author listgoo
	 * @Description: 
	 * @date: 2019年6月6日
	 */
	public static void refreshLotteryWinLhc(){
		logger.info("开始刷新六合彩彩种赔率数据....");
		if (lotteryWinLhcService == null) {
			lotteryWinLhcService = ApplicationContextUtil.getBean(LotteryWinLhcService.class);
		}
		listLotteryWinLhc = lotteryWinLhcService.getLotteryWinLhc();
		logger.info("刷新六合彩彩种赔率数据结束....");
	}
	
	public static void initLhcWinKindPlay(){
		logger.info("开始初始化六合彩彩种赔率数据....");
		if (lotteryWinLhcService == null) {
			lotteryWinLhcService = ApplicationContextUtil.getBean(LotteryWinLhcService.class);
		}
		listLotteryWinLhc = lotteryWinLhcService.getLotteryWinLhc();
		logger.info("初始化六合彩彩种赔率数据结束....");
	}

}
