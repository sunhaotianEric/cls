package com.team.lottery.system.message.messagetype;

import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.handler.LotteryIssueCacheHandler;

public class LotteryIssueCacheMessage extends BaseMessage{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String  lotteryKindStr;
	public LotteryIssueCacheMessage() {
		this.handler= new LotteryIssueCacheHandler();
	}
	public String getLotteryKindStr() {
		return lotteryKindStr;
	}
	public void setLotteryKindStr(String lotteryKindStr) {
		this.lotteryKindStr = lotteryKindStr;
	}
	
	
}
