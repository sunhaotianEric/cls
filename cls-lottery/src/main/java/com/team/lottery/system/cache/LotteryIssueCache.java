package com.team.lottery.system.cache;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.service.LotteryIssueService;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.vo.LotteryIssue;

/**
 * 彩种开奖时间缓存
 * @author gs
 *
 */
public class LotteryIssueCache {

	private static Logger logger = LoggerFactory.getLogger(LotteryIssueCache.class);
	private static LotteryIssueCache instance = new LotteryIssueCache();
	private LotteryIssueCache() {}
	
	//彩种对应的开奖时间
	private static Map<String, List<LotteryIssue>> lotteryIssueMap = new ConcurrentHashMap<String, List<LotteryIssue>>();
	public static Map<String,Integer> lotteryIssueMaxNumMap = new ConcurrentHashMap<String, Integer>();
	public static LotteryIssueCache getInstance() {
		return instance;
	}
	
	/**
	 * 初始化彩种开奖时间,顺便加载彩种最高期数
	 */
	public static void initCache() {
		logger.info("开始初始化彩种开奖时间缓存...");
		ELotteryKind[] lotteryKinds = ELotteryKind.values();
		LotteryIssueService lotteryIssueService = ApplicationContextUtil.getBean("lotteryIssueService");
		for(ELotteryKind lotteryKind : lotteryKinds) {

			//加载彩种所有的开奖时间配置			
			List<LotteryIssue> lotteryIssues = lotteryIssueService.getExpectsAllByKind(lotteryKind.getCode());
			if(CollectionUtils.isNotEmpty(lotteryIssues)) {
				lotteryIssueMap.put(lotteryKind.getCode(), lotteryIssues);
			} else {
				logger.error("加载到彩种["+lotteryKind.getCode()+"]的开奖时间配置为空...");
			}
		}
		//加载彩种最高期数
		loadLotteryKindMaxNun();
		logger.info("初始化彩种开奖时间缓存结束...");
	}
	
 
	
	
	/**
	 * 加载彩种最高期数
	 */
	public static void loadLotteryKindMaxNun(){
		logger.info("正在清空彩种最高期数缓存...");
		lotteryIssueMaxNumMap.clear();
		logger.info("开始刷新彩种最高期数缓存...");
		LotteryIssueService lotteryIssueService = ApplicationContextUtil.getBean("lotteryIssueService");
		List<LotteryIssue> list = lotteryIssueService.getMaxLotteryNum();
		for(LotteryIssue lotteryIssue:list){
			lotteryIssueMaxNumMap.put(lotteryIssue.getLotteryType(),Integer.parseInt(lotteryIssue.getLotteryNum()));
		}
		
		logger.info("刷新刷新彩种最高期数缓存结束...");
	}
	
	
	/**
	 * 刷新彩种开奖时间缓存
	 */
	public static void refreshLotteryIssueCache(){
		logger.info("正在清空彩种开奖时间缓存...");
		lotteryIssueMap.clear();
		logger.info("开始刷新彩种开奖时间缓存...");
		ELotteryKind[] lotteryKinds = ELotteryKind.values();
		LotteryIssueService lotteryIssueService = ApplicationContextUtil.getBean("lotteryIssueService");
		for(ELotteryKind lotteryKind : lotteryKinds) {

			//加载彩种所有的开奖时间配置			
			List<LotteryIssue> lotteryIssues = lotteryIssueService.getExpectsAllByKind(lotteryKind.getCode());
			if(CollectionUtils.isNotEmpty(lotteryIssues)) {
				lotteryIssueMap.put(lotteryKind.getCode(), lotteryIssues);
			} else {
				logger.error("加载到彩种["+lotteryKind.getCode()+"]的开奖时间配置为空...");
			}
		}
		//加载彩种最高期数
		loadLotteryKindMaxNun();
		logger.info("刷新彩种开奖时间缓存结束...");
	}
	
	
	/**
	 * 从缓存中根据彩种查询开奖时间配置
	 * @param lotteryKind
	 * @return
	 */
	public List<LotteryIssue> getLotteryIssueByKind(String lotteryKind) {
		List<LotteryIssue> lotteryIssues = lotteryIssueMap.get(lotteryKind);
		return lotteryIssues;
	}
}
