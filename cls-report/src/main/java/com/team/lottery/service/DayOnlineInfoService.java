package com.team.lottery.service;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.DayOnlineInfoQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.dayonlineinfo.DayOnlineInfoMapper;
import com.team.lottery.vo.DayOnlineInfo;
import com.team.lottery.vo.Login;

@Service("dayOnlineInfoService")
public class DayOnlineInfoService {
	
	private static Logger logger = LoggerFactory.getLogger(DayOnlineInfoService.class);

	@Autowired
	private DayOnlineInfoMapper dayOnlineInfoMapper;
	
	public int deleteByPrimaryKey(Long id) {
		return dayOnlineInfoMapper.deleteByPrimaryKey(id);
	}
	
	public int insertSelective(DayOnlineInfo record) {
		record.setCreateDate(new Date());
		return dayOnlineInfoMapper.insertSelective(record);
	}
	
	public int updateByPrimaryKeySelective(DayOnlineInfo record) {
		return dayOnlineInfoMapper.updateByPrimaryKeySelective(record);
	}
	
	/**
     * 根据归属日期查询每日在线统计信息
     * @param belongDate
     * @return
     */
    public DayOnlineInfo selectByBelongDate(Date belongDate,String bizSystem) {
    	return dayOnlineInfoMapper.selectByBelongDate(belongDate,bizSystem);
    }
	
	/**
	 * 根据类型更新最高在线记录数
	 * @param maxOnlineCount
	 * @param type  
	 * @param onlineDate  归属日期
	 */
	public void updateMaxOnlineCountByType(Integer maxOnlineCount, String type, Date onlineDate,String bizSystem) {
		DayOnlineInfo dayOnlineInfo = dayOnlineInfoMapper.selectByBelongDate(onlineDate,bizSystem);
		//如果为空则新增
		if(dayOnlineInfo == null) {
			dayOnlineInfo = new DayOnlineInfo();
			dayOnlineInfo.setBelongDate(onlineDate);
			if(Login.LOGTYPE_MOBILE.equals(type)) {
				dayOnlineInfo.setMobileMaxOnlineCount(maxOnlineCount);
			} else if(Login.LOGTYPE_PC.equals(type)) {
				dayOnlineInfo.setPcMaxOnlineCount(maxOnlineCount);
			} else {
				dayOnlineInfo.setMaxOnlineCount(maxOnlineCount);
			}
			dayOnlineInfoMapper.insertSelective(dayOnlineInfo);
		} else {
			if(Login.LOGTYPE_MOBILE.equals(type)) {
				dayOnlineInfo.setMobileMaxOnlineCount(maxOnlineCount);
			} else if(Login.LOGTYPE_PC.equals(type)) {
				dayOnlineInfo.setPcMaxOnlineCount(maxOnlineCount);
			} else {
				dayOnlineInfo.setMaxOnlineCount(maxOnlineCount);
			}
			dayOnlineInfoMapper.updateByPrimaryKeySelective(dayOnlineInfo);
		}
	}
	
	public Page getUserDayOnlineInfoPage(DayOnlineInfoQuery query,Page page){
		try {
			page.setTotalRecNum(dayOnlineInfoMapper.getUserDayOnlineInfoPageCount(query));
			List<DayOnlineInfo> dayOnlineInfos = dayOnlineInfoMapper.getUserDayOnlineInfoPage(query,page.getStartIndex(),page.getPageSize());
			page.setPageContent(dayOnlineInfos);
		} catch (Exception e) {
			logger.error("查询异常", e);
		}
		return page;
	}
	
	public DayOnlineInfo getUserDayOnlineInfoTotal(DayOnlineInfoQuery query){
		DayOnlineInfo dayOnlineInfo=dayOnlineInfoMapper.getUserDayOnlineInfoTotal(query);
		return dayOnlineInfo;
	}
}
