package com.team.lottery.service;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.enums.EPlatformChargeType;
import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.platformchargeorder.PlatformChargeOrderMapper;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.BizSystem;
import com.team.lottery.vo.PlatformChargeOrder;
import com.team.lottery.vo.PlatformMonthCharge;
@Service("platformChargeOrderService")
public class PlatformChargeOrderService {
	 @Autowired
	 private PlatformChargeOrderMapper platformChargeOrderMapper;
	 
	 @Autowired
	 private BizSystemService bizSystemService;
	 
	 @Autowired
	 private PlatformMonthChargeService platformMonthChargeService;
	 
	public int deleteByPrimaryKey(Long id){
		return platformChargeOrderMapper.deleteByPrimaryKey(id);
    }

	public int insertSelective(PlatformChargeOrder record){
    	return platformChargeOrderMapper.insertSelective(record);
    }

	public PlatformChargeOrder selectByPrimaryKey(Long id){
    	return platformChargeOrderMapper.selectByPrimaryKey(id);
    }

	public int updateByPrimaryKeySelective(PlatformChargeOrder platformChargeOrder){
	    return platformChargeOrderMapper.updateByPrimaryKeySelective(platformChargeOrder);
    }

    /**
     * 分页条件
     * @return
     */
    public Page getPlatformChargeOrderByQueryPage(PlatformChargeOrder query,Page page){
		page.setTotalRecNum(platformChargeOrderMapper.getPlatformChargeOrderByQueryPageCount(query));
		page.setPageContent(platformChargeOrderMapper.getPlatformChargeOrderByQueryPage(query,page.getStartIndex(),page.getPageSize()));
    	return page;
    }
    /**
     * 分页条件
     * @return
     */
    public PlatformChargeOrder getPlatformChargeOrderByQuerySum(PlatformChargeOrder query){
    	return platformChargeOrderMapper.getPlatformChargeOrderByQuerySum(query);
    }
    
    
    /**
     * 查询符合条件的记录条数
     * @param query
     * @return
     */
    public Integer getPlatformChargeOrderByQueryPageCount(PlatformChargeOrder query){
    	return platformChargeOrderMapper.getPlatformChargeOrderByQueryPageCount(query);
    }
     
    /**
     * 收取费用
     * @return
     * @throws Exception 
     */
    @Transactional(readOnly=false,rollbackFor=Exception.class)
	public void getPlatformChangeOrderForMoney(PlatformChargeOrder platformChargeOrder, Admin currentAdmin) throws Exception {

		try {
			//查询当月的收费统计
			PlatformMonthCharge monthCharge = platformMonthChargeService.queryByDateStr(platformChargeOrder.getBelongDateStr());
			if(monthCharge == null) {
				monthCharge = new PlatformMonthCharge();
				monthCharge.setBelongDate(platformChargeOrder.getBelongDate());
				monthCharge.setBelongDateStr(platformChargeOrder.getBelongDateStr());
				monthCharge.setCreateTime(new Date());
				monthCharge.setBonusMoney(BigDecimal.ZERO);
				monthCharge.setMaintainMoney(BigDecimal.ZERO);
				monthCharge.setLaborMoney(BigDecimal.ZERO);
				monthCharge.setEquipmentMoney(BigDecimal.ZERO);
				if(EPlatformChargeType.BONUS.getCode().equals(platformChargeOrder.getChargeType())) {
					monthCharge.setBonusMoney(platformChargeOrder.getMoney());
				} else if(EPlatformChargeType.MAINTAIN.getCode().equals(platformChargeOrder.getChargeType())) {
					monthCharge.setMaintainMoney(platformChargeOrder.getMoney());
				}else if(EPlatformChargeType.LABOR.getCode().equals(platformChargeOrder.getChargeType())) {
					monthCharge.setLaborMoney(platformChargeOrder.getMoney());
				}else if(EPlatformChargeType.EQUIPMENT.getCode().equals(platformChargeOrder.getChargeType())) {
					monthCharge.setEquipmentMoney(platformChargeOrder.getMoney());
				}
				//计算盈利  分红费+维护费-工资费-服务器费用
				BigDecimal gain = monthCharge.getBonusMoney().add(monthCharge.getMaintainMoney()).subtract(monthCharge.getLaborMoney()).subtract(monthCharge.getEquipmentMoney());
				monthCharge.setGain(gain);
				platformMonthChargeService.insertSelective(monthCharge);
			} else {
				//累加金额
				if(EPlatformChargeType.BONUS.getCode().equals(platformChargeOrder.getChargeType())) {
					monthCharge.setBonusMoney(monthCharge.getBonusMoney().add(platformChargeOrder.getMoney()));
				} else if(EPlatformChargeType.MAINTAIN.getCode().equals(platformChargeOrder.getChargeType())) {
					monthCharge.setMaintainMoney(monthCharge.getMaintainMoney().add(platformChargeOrder.getMoney()));
				}else if(EPlatformChargeType.LABOR.getCode().equals(platformChargeOrder.getChargeType())) {
					monthCharge.setLaborMoney(monthCharge.getLaborMoney().add(platformChargeOrder.getMoney()));
				}else if(EPlatformChargeType.EQUIPMENT.getCode().equals(platformChargeOrder.getChargeType())) {
					monthCharge.setEquipmentMoney(monthCharge.getEquipmentMoney().add(platformChargeOrder.getMoney()));
				}
				//计算盈利  分红费+维护费-工资费-服务器费用
				BigDecimal gain = monthCharge.getBonusMoney().add(monthCharge.getMaintainMoney()).subtract(monthCharge.getLaborMoney()).subtract(monthCharge.getEquipmentMoney());
				monthCharge.setGain(gain);
				
				monthCharge.setUpdateTime(new Date());
				monthCharge.setUpdateAdmin(currentAdmin.getUsername());
				platformMonthChargeService.updateByPrimaryKeySelective(monthCharge);
			}
			
			//业务系统总分红费，总维护费统计
			if (EPlatformChargeType.BONUS.getCode().equals(platformChargeOrder.getChargeType())) {
				BizSystem bizSystem = new BizSystem();
				bizSystem.setBizSystem(platformChargeOrder.getBizSystem());
				bizSystem = bizSystemService.getBizSystemByCondition(bizSystem);
				bizSystem.setTotalBonusMoney(bizSystem.getTotalBonusMoney().add(platformChargeOrder.getMoney()));
				bizSystemService.updateByPrimaryKeySelective(bizSystem);
			} else if (EPlatformChargeType.MAINTAIN.getCode().equals(platformChargeOrder.getChargeType())) {
				BizSystem bizSystem = new BizSystem();
				bizSystem.setBizSystem(platformChargeOrder.getBizSystem());
				bizSystem = bizSystemService.getBizSystemByCondition(bizSystem);
				bizSystem.setTotalMaintainMoney(bizSystem.getTotalMaintainMoney().add(platformChargeOrder.getMoney()));
				//系统失效时间增加一个月
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(bizSystem.getExpireTime());
				calendar.set(Calendar.DAY_OF_MONTH, 1);
				bizSystem.setLastRenewTime(calendar.getTime());
				calendar.add(Calendar.MONTH, 1);
				bizSystem.setExpireTime(calendar.getTime());
				bizSystemService.updateByPrimaryKeySelective(bizSystem);
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
    
    /**
     * 不收取费用
     * @return
     * @throws Exception 
     */
    @Transactional(readOnly=false,rollbackFor=Exception.class)
	public void getPlatformChangeOrderPaymentClose(PlatformChargeOrder platformChargeOrder, Admin currentAdmin) throws Exception {

		try {
			

		   //不收取费用，系统失效时间也要增加一个月
		   if (EPlatformChargeType.MAINTAIN.getCode().equals(platformChargeOrder.getChargeType())) {
				BizSystem bizSystem = new BizSystem();
				bizSystem.setBizSystem(platformChargeOrder.getBizSystem());
				bizSystem = bizSystemService.getBizSystemByCondition(bizSystem);
				
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(bizSystem.getExpireTime());
				calendar.set(Calendar.DAY_OF_MONTH, 1);
				bizSystem.setLastRenewTime(calendar.getTime());
				calendar.add(Calendar.MONTH, 1);
				bizSystem.setExpireTime(calendar.getTime());
				bizSystemService.updateByPrimaryKeySelective(bizSystem);
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
}