package com.team.lottery.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.UserDayConsumeQuery;
import com.team.lottery.mapper.userselfdayconsume.UserSelfDayConsumeMapper;
import com.team.lottery.vo.UserSelfDayConsume;

@Service("userSelfDayConsumeService")
public class UserSelfDayConsumeService {

	private static Logger logger = LoggerFactory.getLogger(UserSelfDayConsumeService.class);

	@Autowired
	private UserSelfDayConsumeMapper userSelfDayConsumeMapper;

	public int insertSelective(UserSelfDayConsume record) {
		return userSelfDayConsumeMapper.insertSelective(record);
	}

	public UserSelfDayConsume selectByPrimaryKey(Long id) {
		return userSelfDayConsumeMapper.selectByPrimaryKey(id);
	}

	public UserSelfDayConsume getUserSelfDayConsumeByCondition(UserDayConsumeQuery query) {
		return userSelfDayConsumeMapper.getUserSelfDayConsumeByCondition(query);
	}

	/**
	 * 根据条件查询符合条件的列表数据
	 * 
	 * @param query
	 * @return
	 */
	public List<UserSelfDayConsume> getUserSelfDayConsumeListByCondition(UserDayConsumeQuery query) {
		return userSelfDayConsumeMapper.getUserSelfDayConsumeListByCondition(query);
	}

	/**
	 * 根据开始归属时间和结束归属时间获取用户团队的盈利数据
	 * 
	 * @param query
	 * @return
	 */
	public UserSelfDayConsume getUserSelfDayConsume(UserDayConsumeQuery query) {
		UserSelfDayConsume userSelfDayConsume = null;
		try {
			userSelfDayConsume = userSelfDayConsumeMapper.getUserSelfDayConsume(query);
		} catch (Exception e) {
			logger.info("userSelfDayConsume查询失败" + e);
		}
		return userSelfDayConsume;
	}

	/**
	 * 分页查找资金明细记录
	 * 
	 * @param query
	 * @param startNum
	 * @param pageNum
	 * @return
	 */
	public Page getUserSelfDayConsumePage(UserDayConsumeQuery query, Page page) {
		try {
			page.setTotalRecNum(userSelfDayConsumeMapper.getUserSelfDayConsumeConut(query));
			page.setPageContent(userSelfDayConsumeMapper.getUserSelfDayConsumePage(query, page.getStartIndex(), page.getPageSize()));
		} catch (Exception e) {
			logger.info("userSelfDayConsume查询失败" + e);
		}
		return page;
	}

	/**
	 * 加版本号更新
	 * 
	 * @param record
	 * @return
	 * @throws UnEqualVersionException
	 */
	public int updateByPrimaryKeyWithVersionSelective(UserSelfDayConsume record) throws UnEqualVersionException {
		int res = userSelfDayConsumeMapper.updateByPrimaryKeyWithVersionSelective(record);
		if (res == 0) {
			logger.error("更新kr_user_self_day_consume记录id[" + record.getId() + "]时发现版本号不一致");
			throw new UnEqualVersionException("更新kr_user_self_day_consume记录id[" + record.getId() + "]时发现版本号不一致");
		}
		return res;
	}

	/**
	 * 查询当前用户的今日盈亏!
	 * 
	 * @param query
	 * @return
	 */
	public UserSelfDayConsume getUserSelfDayConsumeByUserName(UserDayConsumeQuery query) {
		UserSelfDayConsume userSelfDayConsume = null;
		try {
			userSelfDayConsume = userSelfDayConsumeMapper.getUserSelfDayConsumeByUserName(query);
		} catch (Exception e) {
			logger.info("userSelfDayConsume查询失败" + e);
		}
		return userSelfDayConsume;
	}

	/**
	 * 获取系统中每日中奖金额前十的用户
	 * 
	 * @param bizSystem
	 * @return
	 */
	public List<UserSelfDayConsume> getUserSelfDayConsumeTopTen(UserDayConsumeQuery query) {
		List<UserSelfDayConsume> userSelfDayConsume = null;
		try {
			userSelfDayConsume = userSelfDayConsumeMapper.getUserSelfDayConsumeTopTen(query);
		} catch (Exception e) {
			logger.info("系统[" + query.getBizSystem() + "]查询每日前十中奖金额失败" + e);
		}
		return userSelfDayConsume;
	}
}
