package com.team.lottery.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.TeamUserQuery;
import com.team.lottery.extvo.UserDayConsumeQuery;
import com.team.lottery.mapper.userdayconsume.UserDayConsumeMapper;
import com.team.lottery.vo.UserDayConsume;

@Service("userDayConsumeService")
public class UserDayConsumeService {

	private static Logger logger = LoggerFactory.getLogger(UserDayConsumeService.class);
	
	@Autowired
	private UserDayConsumeMapper userDayConsumeMapper;
	
	public int insertSelective(UserDayConsume record) {
		return userDayConsumeMapper.insertSelective(record);
	}
	
	public UserDayConsume selectByPrimaryKey(Long id) {
		return userDayConsumeMapper.selectByPrimaryKey(id);
	}
	
	public Page getUserDayConsumeByQueryPage(UserDayConsumeQuery query,Page page) {
		try {
			page.setTotalRecNum(userDayConsumeMapper.getUserDayConsumeByQueryPageCount(query));
			page.setPageContent(userDayConsumeMapper.getUserDayConsumeByQueryPage(query,page.getStartIndex(),page.getPageSize()));
		} catch (Exception e) {
			logger.error("查询下级明细记录出错", e);
		}
		return page;
	}
	
	
	public List<UserDayConsume> getUserDayConsumeByCondition(UserDayConsumeQuery query) {
		return userDayConsumeMapper.getUserDayConsumeByCondition(query);
	}
	
	public List<UserDayConsume> getUserDayConsumeByCondition2(UserDayConsumeQuery query) {
		return userDayConsumeMapper.getUserDayConsumeByCondition2(query);
	}
	
	public List<UserDayConsume> getUserDayConsumeByCondition3(UserDayConsumeQuery query) {
		return userDayConsumeMapper.getUserDayConsumeByCondition3(query);
	}
	/**
	 * 查询团队的下级会员列表,含注册,投注等统计
	 * @param query
	 * @param startNum
	 * @param pageNum
	 * @return
	 */
	public List<UserDayConsume> getAllTeamUserList2(@Param("query")TeamUserQuery query) {
		return userDayConsumeMapper.getAllTeamUserList2(query);
	}
	
	public Page getAgentReportPage(UserDayConsumeQuery query,Page page){
		page.setPageContent(userDayConsumeMapper.getAgentReport(query,page.getStartIndex(), page.getPageSize()));
		page.setTotalRecNum(userDayConsumeMapper.getAgentReportCount(query));
		return page;
	}
	/**
     * 加版本号更新
     * @param record
     * @return
	 * @throws UnEqualVersionException 
     */
	public int updateByPrimaryKeyWithVersionSelective(UserDayConsume record) throws UnEqualVersionException{
		int res = userDayConsumeMapper.updateByPrimaryKeyWithVersionSelective(record);		
		if(res == 0) {
			logger.error("更新kr_user_day_consume记录id["+record.getId()+"]时发现版本号不一致");
			throw new UnEqualVersionException("更新kr_user_day_consume记录id["+record.getId()+"]时发现版本号不一致");
			
		}
		return res;
	}
}
