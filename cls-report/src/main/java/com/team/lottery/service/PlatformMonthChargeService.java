package com.team.lottery.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.platformmonthcharge.PlatformMonthChargeMapper;
import com.team.lottery.vo.PlatformChargeOrder;
import com.team.lottery.vo.PlatformMonthCharge;

@Service
public class PlatformMonthChargeService {

	@Autowired
	private PlatformMonthChargeMapper platformMonthChargeMapper;
	
	public int deleteByPrimaryKey(Integer id) {
		return platformMonthChargeMapper.deleteByPrimaryKey(id);
	}

	public int insertSelective(PlatformMonthCharge record) {
    	return platformMonthChargeMapper.insertSelective(record);
    }

	public PlatformMonthCharge selectByPrimaryKey(Integer id) {
    	return platformMonthChargeMapper.selectByPrimaryKey(id);
    }

	public int updateByPrimaryKeySelective(PlatformMonthCharge record) {
    	return platformMonthChargeMapper.updateByPrimaryKeySelective(record);
    }
	
	/**
     * 按月份查找
     * @param monthDate
     * @return
     */
    public PlatformMonthCharge queryByDateStr(String monthDateStr) {
    	return platformMonthChargeMapper.queryByDateStr(monthDateStr);
    }
    
    /**
     * 分页查询
     * @return
     */
    public Page queryPlatformMonthChargePage(PlatformChargeOrder query,Page page){
		page.setTotalRecNum(platformMonthChargeMapper.queryPlatformMonthChargePageCount(query));
		page.setPageContent(platformMonthChargeMapper.queryPlatformMonthChargePage(query,page.getStartIndex(),page.getPageSize()));
    	return page;
    }
    
    /**
     * 按条件总统计
     * @return
     */
    public PlatformMonthCharge queryPlatformMonthChargeSum(PlatformChargeOrder query){
    	return platformMonthChargeMapper.queryPlatformMonthChargeSum(query);
    }

}
