package com.team.lottery.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.extvo.LotteryDayGainQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.lotterydaygain.LotteryDayGainMapper;
import com.team.lottery.util.ClsDateHelper;
import com.team.lottery.vo.LotteryDayGain;

@Service("lotteryDayGainService")
public class LotteryDayGainService {

	@Autowired
	private LotteryDayGainMapper lotteryDayGainMapper;
	
	public int deleteByPrimaryKey(Long id) {
		return lotteryDayGainMapper.deleteByPrimaryKey(id);
	}

	public int insertSelective(LotteryDayGain record) {
		return lotteryDayGainMapper.insertSelective(record);
	}

	public LotteryDayGain selectByPrimaryKey(Long id) {
		return lotteryDayGainMapper.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(LotteryDayGain record) {
		return lotteryDayGainMapper.updateByPrimaryKeySelective(record);
	}
	
	/**
	 * 分页查询彩票盈利
	 * @param query
	 * @param page
	 * @return
	 */
	public Page getLotteryDayGainPage(LotteryDayGainQuery query,Page page){
		try {
			page.setTotalRecNum(lotteryDayGainMapper.getLotteryDayGainCount(query));
			List<LotteryDayGain> dayGain = lotteryDayGainMapper.getLotteryDayGainPage(query, page.getStartIndex(), page.getPageSize());
			Iterator<LotteryDayGain> iterator = dayGain.iterator();
			while(iterator.hasNext()){
				LotteryDayGain lotteryDayGain = iterator.next();
				String lotteryType = lotteryDayGain.getLotteryType();
				ELotteryKind eLotteryKind = ELotteryKind.valueOf(lotteryType);
				lotteryDayGain.setLotteryType(eLotteryKind.getDescription());
			}
			page.setPageContent(dayGain);
			
		} catch (Exception e) {
			System.err.println("查询失败"+e);
		}
		
		return page;
	}

	/**
	 * 某一时间段的具体日期
	 * @param beginDate
	 * @param endDate
	 * @return
	 *
	 */
	public static Set<String> getDatesBetweenTwoDate(Date beginDate, Date endDate) {    
	    Set<String> lDate = new HashSet<String>();
	    DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
		/*String strDate=df.format(nowDate);*/
	    lDate.add(df.format(beginDate));//把开始时间加入集合
	    Calendar cal = Calendar.getInstance();
	    //使用给定的 Date 设置此 Calendar 的时间
	    cal.setTime(beginDate);
	    boolean bContinue = true;
	    while (bContinue) {
	        //根据日历的规则，为给定的日历字段添加或减去指定的时间量
	        cal.add(Calendar.DAY_OF_MONTH, 1);
	        // 测试此日期是否在指定日期之后
	        if (endDate.after(cal.getTime())) {
	            lDate.add(df.format(cal.getTime()));
	        } else {
	            break;
	        }
	    }
	    lDate.add(df.format(endDate));//把结束时间加入集合
	    return lDate;
	}
	/**
	 * 所属时间信息是否缺失判断方法
	 * @param dateSet
	 * @param list
	 * @return
	 *
	 */
	public String isHasLoseDate(Set<String> dateSet,List<Date> list) {  
		String returnStr="";
		for(String str:dateSet)
		{
			boolean dateIsHas=true;
			for(Date date:list)
			{
				 DateFormat df2=new SimpleDateFormat("yyyy-MM-dd");
				if(str.equals(df2.format(date)))
				{
					dateIsHas=false;
					break;
				}
			}
			if(dateIsHas)
			{
				returnStr=str+"查询数据缺失!";
			}
		}
		
	
	    return returnStr;
	}
	/**
	 * 查询相关的所属时间信息
	 * @param belongDateStart
	 * @param belongDateEnd
	 * @return
	 *
	 */
	public List<Date> selectBelongDate(Date belongDateStart,Date belongDateEnd) {
		// TODO Auto-generated method stub
		return lotteryDayGainMapper.selectBelongDate(belongDateStart,belongDateEnd);
	}
	/**
	 * 返回相关日期数据丢失信息
	 * @param query
	 * 
	 * @return
	 * 
	 */
	public String getDateStr(LotteryDayGainQuery query){
		    String str="";
			Date reportLastTime=ClsDateHelper.getClsReportLastTime(new Date());
			Date nowDate=new Date();
			DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
			String strDate=df.format(nowDate);
			//Date nowTime=df.parse(strDate);
			Date startTime=query.getBelongDateStart();
			Date endTime=null;
			if(strDate.equals(df.format(query.getBelongDateEnd())))
			{
				if(df.format(query.getBelongDateStart()).equals(df.format(query.getBelongDateEnd())))
						{
					
					return str;
					
						}
				if(reportLastTime.compareTo(nowDate)>0)
				{
					Calendar currentDate = new GregorianCalendar(); 
					if(strDate.equals(df.format(endTime)))
					{
						currentDate.setTime(query.getBelongDateEnd());
				    	currentDate.add(Calendar.DATE, -2);
						endTime=currentDate.getTime();
					}
			    	
					Set<String> dateSet=this.getDatesBetweenTwoDate(startTime,endTime);
					List<Date> list=this.selectBelongDate(startTime,endTime);
					str=this.isHasLoseDate(dateSet, list);
				}
				else if(reportLastTime.compareTo(nowDate)<=0)
				{
					endTime=query.getBelongDateEnd();
					Calendar currentDate = new GregorianCalendar();   
					if(strDate.equals(df.format(endTime)))
					{
						currentDate.setTime(endTime);
						currentDate.add(Calendar.DATE, -1);
						endTime=currentDate.getTime();
					}
					
					Set<String> dateSet=this.getDatesBetweenTwoDate(startTime,endTime);
					List<Date> list=this.selectBelongDate(startTime,endTime);
					str=this.isHasLoseDate(dateSet, list);
					
				}
			}
			else
			{
				endTime=query.getBelongDateEnd();
				Set<String> dateSet=this.getDatesBetweenTwoDate(startTime,endTime);
				List<Date> list=this.selectBelongDate(startTime,endTime);
				str=this.isHasLoseDate(dateSet, list);
				
			}
				
			return str;
	}
	/**
	 * 查询彩票盈利
	 * @param query
	 *
	 * @return
	 */
	public List<LotteryDayGain> getLotteryDayGain(LotteryDayGainQuery query){
		    List<LotteryDayGain> lotteryList=new ArrayList<LotteryDayGain>();
		try {
			
			 lotteryList = lotteryDayGainMapper.getLotteryDayGain(query);
			
			
		} catch (Exception e) {
			System.err.println("查询失败"+e);
		}
		
		return lotteryList;
	}

}
