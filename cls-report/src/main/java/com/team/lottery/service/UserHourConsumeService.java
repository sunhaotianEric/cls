package com.team.lottery.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.UserHourConsumeQuery;
import com.team.lottery.mapper.userhourconsume.UserHourConsumeMapper;
import com.team.lottery.vo.UserHourConsume;

@Service("userHourConsumeService")
public class UserHourConsumeService {

	
	@Autowired
	private UserHourConsumeMapper userHourConsumeMapper;
	
	public int insertSelective(UserHourConsume record) {
		return userHourConsumeMapper.insertSelective(record);
	}
	
	public UserHourConsume selectByPrimaryKey(Long id) {
		return userHourConsumeMapper.selectByPrimaryKey(id);
	}
	
	 /**
     * 根据条件查询小时盈亏数据
     * @param query
     * @return
     */
	public List<UserHourConsume> getUserHourConsumeByCondition(UserHourConsumeQuery query) {
		return userHourConsumeMapper.getUserHourConsumeByCondition(query);
	}
	
	
}
