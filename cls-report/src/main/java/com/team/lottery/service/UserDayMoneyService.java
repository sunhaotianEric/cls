package com.team.lottery.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.UserDayMoneyQuery;
import com.team.lottery.mapper.userdaymoney.UserDayMoneyMapper;
import com.team.lottery.vo.UserDayMoney;

@Service("userDayMoneyService")
public class UserDayMoneyService {

	@Autowired
	private UserDayMoneyMapper userDayMoneyMapper;
	
	public int insertSelective(UserDayMoney record) {
		return userDayMoneyMapper.insert(record);
	}
	
	public UserDayMoney selectByPrimaryKey(Long id) {
		return userDayMoneyMapper.selectByPrimaryKey(id);
	}
	
	public List<UserDayMoney> getUserDayMoneyByCondition(UserDayMoneyQuery query) {
		return userDayMoneyMapper.getUserDayMoneyByCondition(query);
	}
}
