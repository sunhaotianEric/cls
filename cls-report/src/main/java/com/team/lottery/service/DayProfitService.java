package com.team.lottery.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.DayProfitQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.dayprofit.DayProfitMapper;
import com.team.lottery.util.ClsDateHelper;
import com.team.lottery.vo.DayProfit;


@Service("dayProfitService")
public class DayProfitService {
	private static Logger logger = LoggerFactory.getLogger(DayProfitService.class);
	@Autowired
	private DayProfitMapper dayProfitMapper;
	
	public int deleteByPrimaryKey(Long id){
		return dayProfitMapper.deleteByPrimaryKey(id);
	}
	
	public int insert(DayProfit record){
		return dayProfitMapper.insert(record);
	}
	
	public int insertSelective(DayProfit record){
		return dayProfitMapper.insertSelective(record);
	}
	
	public DayProfit selectByPrimaryKey(Long id){
		return dayProfitMapper.selectByPrimaryKey(id);
	}
	
	public int updateByPrimaryKeySelective(DayProfit record){
		return dayProfitMapper.updateByPrimaryKeySelective(record);
	}
	
	public int updateByPrimaryKey(DayProfit record){
		return dayProfitMapper.updateByPrimaryKey(record);
	}
	
	/**
	 * 根据bizSystem和归属时间查询平台盈利数据
	 * @param query
	 * @return
	 */
	public List<DayProfit> getAllDayProfitByBelongDate(DayProfitQuery query){
		
		List<DayProfit> dayProfitList = dayProfitMapper.getAllDayProfitByBelongDate(query);
		
		return dayProfitList;
	}
	
	/**
	 * 分页查询平台赢利总览
	 * @param query
	 * @param page
	 * @return
	 */
	public Page getDayProfiyPandect(DayProfitQuery query,Page page){
		try {
			page.setTotalRecNum(dayProfitMapper.getDayProfiyPandectCount(query));
			List<DayProfit> list = dayProfitMapper.getDayProfiyPandect(query, page.getStartIndex(), page.getPageSize());
			//数据处理
			
			page.setPageContent(list);
		} catch (Exception e) {
			logger.info("查询异常！-->"+e);
		}
		return page;
	}
	
	
	
	
	/**
	 * 根据归属时间查询平台投注盈利
	 * @param query
	 */
	public List<DayProfit> getLotteryGainProfitByBelongDate(DayProfitQuery dayProfitQuery){
		
	    List<DayProfit> dayProfitList = dayProfitMapper.getLotteryGainProfitByBelongDate(dayProfitQuery);
		return dayProfitList;
	}
    
	/**
	 * 验证数据是否完整
	 */
	public String checkDataComplete(DayProfitQuery query){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String str="";
		Calendar currentDate = new GregorianCalendar(); 
		currentDate.setTime(query.getBelongDateStart());
		String allDateStr = dayProfitMapper.getDayProfitByDate(query.getBizSystem(), ClsDateHelper.getClsMiddleTime(query.getBelongDateStart()), ClsDateHelper.getClsMiddleTime(query.getBelongDateEnd()));
		allDateStr = (allDateStr == null?"":allDateStr);
		while(currentDate.getTime().compareTo(query.getBelongDateEnd()) <= 0){
			String currentDateStr = sdf.format(currentDate.getTime());
			int result= allDateStr.indexOf(currentDateStr);
		    if(result == -1){
		    	str +=","+sdf.format(currentDate.getTime());
		    }
		    currentDate.add(Calendar.DAY_OF_MONTH, 1);
		}
		
		return str;	
	}
	
	 /**
     * 根据条件查询数据
     * @param dayProfitQuery
     * @return
     */
	public List<DayProfit> getDayProfiyByQuery(DayProfitQuery dayProfitQuery){
		
		return dayProfitMapper.getDayProfiyByQuery(dayProfitQuery);
    }
	
	
}
