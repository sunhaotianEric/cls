package com.team.lottery.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.extvo.DayProfitRankQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.UserDayConsumeQuery;
import com.team.lottery.mapper.dayprofitrank.DayProfitRankMapper;
import com.team.lottery.vo.DayProfitRank;

@Service("dayProfitRankService")
public class DayProfitRankService {
	
	private static Logger logger = LoggerFactory.getLogger(DayProfitRankService.class);

	@Autowired
	private DayProfitRankMapper dayProfitRankMapper;
	
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int deleteByPrimaryKey(Integer id){
		return dayProfitRankMapper.deleteByPrimaryKey(id);
	}
	
	@Transactional(readOnly = false, rollbackFor = Exception.class)
    public int insertSelective(DayProfitRank record){
		return dayProfitRankMapper.insertSelective(record);
    }
    
    public DayProfitRank selectByPrimaryKey(Integer id){
		return dayProfitRankMapper.selectByPrimaryKey(id);
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(DayProfitRank record){
		return dayProfitRankMapper.updateByPrimaryKeySelective(record);	
    }
	
	/**
	 * 根据业务系统和归属日期查询所有
	 * @return
	 */
	public List<DayProfitRank> getAllDayProfitRank(UserDayConsumeQuery query){
		List<DayProfitRank> dayProfitRanks = null;
		try {
			dayProfitRanks = dayProfitRankMapper.getAllDayProfitRank(query);
		} catch (Exception e) {
			logger.info("查询失败"+e);
		}
		return dayProfitRanks;
	}
	
    /**
     * 查找所有的数据
     * @return
     */
    public Page getAllDayProfitRankByQueryPage(DayProfitRankQuery query,Page page){
		page.setTotalRecNum(dayProfitRankMapper.getAllDayProfitRankQueryPageCount(query));
		List<DayProfitRank> dayProfitRankQuery = dayProfitRankMapper.getAllDayProfitRankQueryPage(query,page.getStartIndex(),page.getPageSize());
		page.setPageContent(dayProfitRankQuery);
    	return page;
    }
    
    /**
     * 根据系统,归属时间查询所有
     * @param bizSystem
     * @return
     */
    public List<DayProfitRank> getDayProfitRank(String bizSystem, Date belongDate){
		return dayProfitRankMapper.getDayProfitRank(bizSystem, belongDate);
    	
    }
    
    /**
     * 重新设置中奖排名
     * @param userName
     */
    public void resetBonusList(String bizSystem, Date belongDate){
    	List<DayProfitRank> dayProfitRanks = getDayProfitRank(bizSystem, belongDate);
    	// List根据Bonus排序.排序完毕过后进行排名设置,并且保存到数据库
		List<DayProfitRank> rankWithBonusList = getRankWithBonusList(dayProfitRanks);
		Integer ranking = 10;
		for (DayProfitRank dayProfitRank : rankWithBonusList) {
			dayProfitRank.setRanking(ranking);
			int i = updateByPrimaryKeySelective(dayProfitRank);
			if (i == 1) {
				ranking--;
				// 修改成功,打印日志.
				logger.info("执行每日前十奖金榜往数据库修改数据结果[成功]修改的用户名为:[" + dayProfitRank.getUserName() + "],系统为:[" + dayProfitRank.getBizSystem() + "]");
			} else {
				logger.info("执行每日前十奖金榜往数据库修改数据结果[失败]");
			}
		}
    }
    
	/**
	 * 根据dayProfitRanks中的Bonus字段排序.
	 * @param dayProfitRank
	 * @return
	 */
	private static List<DayProfitRank> getRankWithBonusList(List<DayProfitRank> dayProfitRank) {
//		重写Comparator中的compare方法
		Collections.sort(dayProfitRank, new Comparator<DayProfitRank> (){
			@Override
			public int compare(DayProfitRank o1, DayProfitRank o2) {
				return o1.getBonus().compareTo(o2.getBonus());
			}
		});
		return dayProfitRank;
	}
	
}
