package com.team.lottery.mapper.userhourconsume;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.UserHourConsumeQuery;
import com.team.lottery.vo.UserHourConsume;


public interface UserHourConsumeMapper {
    int deleteByPrimaryKey(Long id);

    int insert(UserHourConsume record);

    int insertSelective(UserHourConsume record);

    UserHourConsume selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UserHourConsume record);

    int updateByPrimaryKey(UserHourConsume record);

    /**
     * 根据条件查询小时盈亏数据
     * @param query
     * @return
     */
	List<UserHourConsume> getUserHourConsumeByCondition(@Param("query")UserHourConsumeQuery query);
}