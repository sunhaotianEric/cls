package com.team.lottery.mapper.userdaymoney;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.UserDayMoneyQuery;
import com.team.lottery.vo.UserDayMoney;


public interface UserDayMoneyMapper {
    int deleteByPrimaryKey(Long id);

    int insert(UserDayMoney record);

    int insertSelective(UserDayMoney record);

    UserDayMoney selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UserDayMoney record);

    int updateByPrimaryKey(UserDayMoney record);
    
    /**
     * 根据起始时间和结束时间获取用户团队的余额数据
     * @param query
     * @return
     */
    List<UserDayMoney> getUserDayMoneyByCondition(@Param("query")UserDayMoneyQuery query);
}