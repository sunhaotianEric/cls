package com.team.lottery.mapper.platformchargeorder;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.PlatformChargeOrder;

public interface PlatformChargeOrderMapper {
    int deleteByPrimaryKey(Long id);

    int insert(PlatformChargeOrder record);

    int insertSelective(PlatformChargeOrder record);

    PlatformChargeOrder selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(PlatformChargeOrder record);

    int updateByPrimaryKey(PlatformChargeOrder record);
    
    /**
     * 查询符合条件的记录条数
     * @param query
     * @return
     */
    public Integer getPlatformChargeOrderByQueryPageCount(@Param("query")PlatformChargeOrder query);
        
    /**
     * 分页条件
     * @return
     */
    public List<PlatformChargeOrder> getPlatformChargeOrderByQueryPage(@Param("query")PlatformChargeOrder query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
    
    
    /**
     * 统计费用
     * @param query
     * @return
     */
    public PlatformChargeOrder getMoneyByQuery(@Param("query")PlatformChargeOrder query);
    
    /**
     * 根据相关条件统计费用
     * @param query
     * @return
     */
    public PlatformChargeOrder getPlatformChargeOrderByQuerySum(@Param("query")PlatformChargeOrder query);
}