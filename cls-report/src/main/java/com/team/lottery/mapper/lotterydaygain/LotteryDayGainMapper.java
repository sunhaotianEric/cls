package com.team.lottery.mapper.lotterydaygain;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.LotteryDayGainQuery;
import com.team.lottery.vo.LotteryDayGain;

public interface LotteryDayGainMapper {
    int deleteByPrimaryKey(Long id);

    int insert(LotteryDayGain record);

    int insertSelective(LotteryDayGain record);

    LotteryDayGain selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(LotteryDayGain record);

    int updateByPrimaryKey(LotteryDayGain record);
    
    int getLotteryDayGainCount(@Param("query")LotteryDayGainQuery lotteryDayGainQuery);
    
   List<LotteryDayGain> getLotteryDayGainPage(@Param("query")LotteryDayGainQuery lotteryDayGainQuery, @Param("startNum")Integer startNum, @Param("pageNum")Integer pageNum);

   List<Date> selectBelongDate(@Param("belongDateStart")Date belongDateStart,@Param("belongDateEnd")Date belongDateEnd);
   
   List<LotteryDayGain> getLotteryDayGain(@Param("query")LotteryDayGainQuery lotteryDayGainQuery);
}