package com.team.lottery.mapper.dayonlineinfo;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.DayOnlineInfoQuery;
import com.team.lottery.vo.DayOnlineInfo;


public interface DayOnlineInfoMapper {
    int deleteByPrimaryKey(Long id);

    int insert(DayOnlineInfo record);

    int insertSelective(DayOnlineInfo record);

    DayOnlineInfo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(DayOnlineInfo record);

    int updateByPrimaryKey(DayOnlineInfo record);
    
    /**
     * 根据归属日期查询每日在线统计信息
     * @param belongDate
     * @return
     */
    DayOnlineInfo selectByBelongDate(@Param("belongDate")Date belongDate,@Param("bizSystem")String bizSystem);

    /**
     * 根据日期段查询每日在线统计总条数
     * @param query
     * @return
     */
	Integer getUserDayOnlineInfoPageCount(@Param("query")DayOnlineInfoQuery query);

	/**
	 * 根据日期段查询每日在线统计每页记录
	 * @param query
	 * @param startIndex
	 * @param pageSize
	 * @return
	 */
	List<DayOnlineInfo> getUserDayOnlineInfoPage(@Param("query")DayOnlineInfoQuery query, @Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
	DayOnlineInfo getUserDayOnlineInfoTotal(@Param("query")DayOnlineInfoQuery query);
}