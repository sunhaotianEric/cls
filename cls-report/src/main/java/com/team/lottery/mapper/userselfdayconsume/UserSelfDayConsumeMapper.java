package com.team.lottery.mapper.userselfdayconsume;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.UserDayConsumeQuery;
import com.team.lottery.vo.UserSelfDayConsume;

public interface UserSelfDayConsumeMapper {
	int deleteByPrimaryKey(Long id);

	int insert(UserSelfDayConsume record);

	int insertSelective(UserSelfDayConsume record);

	UserSelfDayConsume selectByPrimaryKey(Long id);

	int updateByPrimaryKeySelective(UserSelfDayConsume record);

	int updateByPrimaryKey(UserSelfDayConsume record);

	/**
	 * 根据起始时间和结束时间获取用户团队的盈利数据
	 * 
	 * @return
	 */
	UserSelfDayConsume getUserSelfDayConsumeByCondition(@Param("query") UserDayConsumeQuery query);

	/**
	 * 根据起始时间和结束时间获取用户团队的盈利数据
	 * 
	 * @return
	 */
	List<UserSelfDayConsume> getUserSelfDayConsumeListByCondition(@Param("query") UserDayConsumeQuery query);

	/**
	 * 根据开始归属时间和结束归属时间获取用户团队的盈利数据
	 * 
	 * @return
	 */
	UserSelfDayConsume getUserSelfDayConsume(@Param("query") UserDayConsumeQuery query);

	/**
	 * 查询条数
	 * 
	 * @param query
	 * @return
	 */
	int getUserSelfDayConsumeConut(@Param("query") UserDayConsumeQuery query);

	/**
	 * 分页查找资金明细记录
	 * 
	 * @param query
	 * @param startNum
	 * @param pageNum
	 * @return
	 */
	List<UserSelfDayConsume> getUserSelfDayConsumePage(@Param("query") UserDayConsumeQuery query, @Param("startNum") Integer startNum, @Param("pageNum") Integer pageNum);

	/**
	 * 加版本号更新
	 * 
	 * @param record
	 * @return
	 */
	int updateByPrimaryKeyWithVersionSelective(UserSelfDayConsume record);

	/**
	 * 根据用户名和系统查询今日盈亏条件.
	 * 
	 * @param query
	 * @return
	 */
	UserSelfDayConsume getUserSelfDayConsumeByUserName(@Param("query") UserDayConsumeQuery query);

	/**
	 * 获取系统中每日中奖金额前十的用户.
	 * 
	 * @param bizSystem
	 * @return
	 */
	List<UserSelfDayConsume> getUserSelfDayConsumeTopTen(@Param("query") UserDayConsumeQuery query);
}