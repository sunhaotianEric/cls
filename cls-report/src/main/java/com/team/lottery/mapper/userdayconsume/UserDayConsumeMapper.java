package com.team.lottery.mapper.userdayconsume;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.TeamUserQuery;
import com.team.lottery.extvo.UserDayConsumeQuery;
import com.team.lottery.vo.UserDayConsume;


public interface UserDayConsumeMapper {
   
	int deleteByPrimaryKey(Long id);

    int insert(UserDayConsume record);

    int insertSelective(UserDayConsume record);

    UserDayConsume selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UserDayConsume record);

    int updateByPrimaryKey(UserDayConsume record);
    
    /**
     * 根据起始时间和结束时间获取用户团队的盈利数据，分页查询
     * @return
     */
    List<UserDayConsume> getUserDayConsumeByQueryPage(@Param("query")UserDayConsumeQuery query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
    
    /**
     * 根据起始时间和结束时间获取用户团队的盈利数据，个数
     * @return
     */
    Integer getUserDayConsumeByQueryPageCount(@Param("query")UserDayConsumeQuery query);
    
    /**
     * 根据起始时间和结束时间获取用户团队的盈利数据
     * @return
     */
    List<UserDayConsume> getUserDayConsumeByCondition(@Param("query")UserDayConsumeQuery query);
    /**
     * 根据起始时间和结束时间获取用户团队的盈利数据
     * @return
     */
    List<UserDayConsume> getUserDayConsumeByCondition2(@Param("query")UserDayConsumeQuery query);
    
    /**
     * 根据起始时间和结束时间获取用户团队的盈利数据
     * @return
     */
    List<UserDayConsume> getUserDayConsumeByCondition3(@Param("query")UserDayConsumeQuery query);
    /**
   	 * 查询团队的下级会员列表
   	 * @param query
   	 * @param startNum
   	 * @param pageNum
   	 * @return
   	 */
    public List<UserDayConsume> getAllTeamUserList2(@Param("query")TeamUserQuery query);
    /**
     * 分页查询代理报表
     * @param query
     * @param startNum
     * @param pageNum
     * @return
     */
    List<UserDayConsume> getAgentReport(@Param("query")UserDayConsumeQuery query, @Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
    /**
     * 获取代理报表总数据量
     * @param query
     * @return
     */
    int getAgentReportCount(@Param("query")UserDayConsumeQuery query);
    
    /**
     * 加版本号更新
     * @param record
     * @return
     */
    int updateByPrimaryKeyWithVersionSelective(UserDayConsume record);
}