package com.team.lottery.mapper.dayprofitrank;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.DayProfitRankQuery;
import com.team.lottery.extvo.UserDayConsumeQuery;
import com.team.lottery.vo.DayProfitRank;

public interface DayProfitRankMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(DayProfitRank record);

    int insertSelective(DayProfitRank record);

    DayProfitRank selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(DayProfitRank record);

    int updateByPrimaryKey(DayProfitRank record);
    
    /**
     * 根据业务系统和归属日期查询所有
     * @return
     */
    public List<DayProfitRank> getAllDayProfitRank(@Param("query")UserDayConsumeQuery query);
    
    /**
     * 查询符合条件的记录条数
     * @param query
     * @return
     */
    public Integer getAllDayProfitRankQueryPageCount(DayProfitRankQuery query);
    
    /**
     * 分页条件查询
     * @return
     */
    public List<DayProfitRank> getAllDayProfitRankQueryPage(@Param("query")DayProfitRankQuery query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
    
    /**
     * 根据系统,归属时间查询所有
     * @param bizSystem
     * @param belongDate
     * @return
     */
    public List<DayProfitRank> getDayProfitRank(@Param("bizSystem")String bizSystem, @Param("belongDate")Date belongDate);
}