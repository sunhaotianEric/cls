package com.team.lottery.mapper.platformmonthcharge;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.PlatformChargeOrder;
import com.team.lottery.vo.PlatformMonthCharge;

public interface PlatformMonthChargeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(PlatformMonthCharge record);

    int insertSelective(PlatformMonthCharge record);

    PlatformMonthCharge selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(PlatformMonthCharge record);

    int updateByPrimaryKey(PlatformMonthCharge record);
    
    
    /**
     * 按月份查找
     * @param monthDate
     * @return
     */
    PlatformMonthCharge queryByDateStr(String monthDateStr);
    
    /**
     * 分页查询符合记录的记录数
     * @param query
     * @return
     */
    Integer queryPlatformMonthChargePageCount(@Param("query")PlatformChargeOrder query);
    
    /**
     * 分页查询符合记录
     * @return
     */
    List<PlatformMonthCharge> queryPlatformMonthChargePage(@Param("query")PlatformChargeOrder query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
    
    /**
     * 按条件总统计
     * @param query
     * @return
     */
    PlatformMonthCharge queryPlatformMonthChargeSum(@Param("query")PlatformChargeOrder query);
}