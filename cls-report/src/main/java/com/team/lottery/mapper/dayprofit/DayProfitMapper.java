package com.team.lottery.mapper.dayprofit;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.DayProfitQuery;
import com.team.lottery.vo.DayProfit;

public interface DayProfitMapper {
    int deleteByPrimaryKey(Long id);

    int insert(DayProfit record);

    int insertSelective(DayProfit record);

    DayProfit selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(DayProfit record);

    int updateByPrimaryKey(DayProfit record);
    
	
	/**
	 * 根据归属时间查询平台盈利数据
	 * @param query
	 */
    List<DayProfit> getAllDayProfitByBelongDate(@Param("query")DayProfitQuery dayProfitQuery);
    
    /**
	 * 根据归属时间查询平台投注盈利
	 * @param query
	 */
    List<DayProfit> getLotteryGainProfitByBelongDate(@Param("query")DayProfitQuery dayProfitQuery);
    
    /**
     * 查询条数
     * @param dayProfitQuery
     * @return
     */
    int getDayProfiyPandectCount(@Param("query")DayProfitQuery dayProfitQuery);
    
    /**
     * 查询某天业务系统的数据
     * @param bizSystem
     * @param belongDate
     * @return
     */
    int getDayProfitCount(@Param("bizSystem")String bizSystem ,@Param("belongDate")Date belongDate);
    /**
     * 查询某天业务系统的数据
     * @param bizSystem
     * @param belongDate
     * @return
     */
    String getDayProfitByDate(@Param("bizSystem")String bizSystem ,@Param("belongDateStart")Date belongDateStart,@Param("belongDateEnd")Date belongDateEnd);
   
    /**
     * 分页查询平台赢利
     */
    List<DayProfit> getDayProfiyPandect(@Param("query")DayProfitQuery dayProfitQuery,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum );
    
    /**
     * 根据条件查询数据
     * @param dayProfitQuery
     * @return
     */
    List<DayProfit> getDayProfiyByQuery(@Param("query")DayProfitQuery dayProfitQuery);
}