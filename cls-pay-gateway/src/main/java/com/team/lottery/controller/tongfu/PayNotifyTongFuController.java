package com.team.lottery.controller.tongfu;
import java.math.BigDecimal;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.ChargePayVo;
import com.team.lottery.pay.util.AesUtil;
import com.team.lottery.pay.util.Md5Util;
import com.team.lottery.service.ChargePayService;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.util.JSONUtils;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.ChargePay;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.ThirdPayConfig;
import net.sf.json.JSONObject;

/**
 * 通富支付入账处理
 * 
 * @author jamine
 *
 */
@Controller
public class PayNotifyTongFuController extends BaseController {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;
	@Autowired
	private ChargePayService chargePayService;
	
	
	/**
	 * 通富支付回调报文跳转地址
	 * 
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/notify/tongfu", method = { RequestMethod.POST, RequestMethod.GET })
	public @ResponseBody String qitongbao(HttpServletRequest request, HttpServletResponse response) throws Exception {
		log.info("接收到通富支付入账通知");
		
		//插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("TONGFU");
		PayNotifyLog payNotifyLog=construstNotifyLog();
		payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(),
				thirdPayConfig.getChargeDes(), "");
		
		//校验白名单
		boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
		if (!validateIp) {
			log.info("第三方支付访问IP异常!请联系客服!");
			return "SUC";
		}
         
		String platformno = request.getParameter("platformno");
		String parameter = request.getParameter("parameter");
		String sign = request.getParameter("sign");
		
		AesUtil se=new AesUtil();
		ChargePayVo query=new ChargePayVo();
		query.setChargeType("TONGFU");
		query.setMemberId(platformno);
		List<ChargePay> chargePayList=chargePayService.selectAllChargePays(query);
		
		String Md5key ="";
		if(chargePayList!=null && chargePayList.size()>0){
			Md5key=chargePayList.get(0).getSign();
		}
		 String paramStr=se.des(parameter,Md5key.substring(0, 16));
		 JSONObject jsonObject  = JSONUtils.toJSONObject(paramStr);
         String commercialOrderNo = jsonObject.getString("commercialOrderNo");
         String orderAmount = jsonObject.getString("orderAmount");
         String orderNo = jsonObject.getString("orderNo");
         String result = jsonObject.getString("result");
         
		
		//组装通知报文内容
		String messageContent = "通富支付,回调返回参数支付状态=" + result + "订单号=" + commercialOrderNo + "金额="
				+ orderAmount + "签名=" + sign;
		// 日志打印.
		log.info(messageContent);
		

		
		RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(commercialOrderNo);
		if (rechargeOrder == null) {
			log.error("根据订单号[" + commercialOrderNo + "]查找充值订单记录为空");
			return "success";
		}
		
		//更新回调通知信息表
		payNotifyLog.setMessageContent(messageContent);
		payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
		payNotifyLog.setOrderId(rechargeOrder.getSerialNumber());
		payNotifyLogService.updateByPrimaryKey(payNotifyLog);
        
		String WaitSign = Md5Util.getMD5ofStr(paramStr);
		// 从第三方返回的MD5加密签名串与后台Md5加密串进行校验
		if (WaitSign.compareTo(sign) == 0) {
			boolean dealResult = false;
			if (!StringUtils.isEmpty(result) && result.equals("success")) {
				// 将金额从分转换为元.
				BigDecimal factMoneyValue = new BigDecimal(orderAmount);
				try {
					dealResult = rechargeOrderService.rechargeOrderSuccessDeal(commercialOrderNo, factMoneyValue);
				} catch (UnEqualVersionException e) {
					log.error("通富支付支付充值时发现版本号不一致,订单号[" + commercialOrderNo + "]");
					// 启动新进程进行多次重试
					UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
							UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, commercialOrderNo, factMoneyValue, "");
					thread.start();
				} catch (Exception e) {
					// 异常不进行重试
					log.error("通富支付处理入账发生异常", e);
				}
			} else {
				if (!result.equals("success")) {
					log.info("通富支付处理错误支付结果result为：" + result);
				}
			}
			log.info("通富支付订单处理入账结果:{}", dealResult);
			// 通知第三方报文接收成功
			return "success";
		} else {
			/*log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容[" + WaitSign + "]");*/
			return "ERROR";
		}
	}

}
