package com.team.lottery.controller.tengfei;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.pay.util.Md5Util;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.util.JSONUtils;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.ThirdPayConfig;

import net.sf.json.JSONObject;

/**
 * 腾飞支付新版入账处理.
 * 
 * @author jamine
 *
 */
@Controller
public class PayNotifyTengFeiController extends BaseController {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;

	/**
	 * 腾飞支付回调报文跳转地址
	 * 
	 * @return
	 */
	@RequestMapping(value = "/notify/tengfei", method = { RequestMethod.POST, RequestMethod.GET })
	public @ResponseBody String tengfei(HttpServletRequest request, HttpServletResponse response) {
		log.info("接收到腾飞支付入账通知");

		// 插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("TENGFEI");
		PayNotifyLog payNotifyLog = construstNotifyLog();
		payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(), thirdPayConfig.getChargeDes(), "");

		// 校验白名单
		boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
		if (!validateIp) {
			log.info("第三方支付访问IP异常!请联系客服!");
			return "error";
		}

		JSONObject jsonObject = null;
		try {
			BufferedReader streamReader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
			StringBuilder responseStrBuilder = new StringBuilder();
			String inputStr;
			while ((inputStr = streamReader.readLine()) != null) {
				responseStrBuilder.append(inputStr);
			}
			log.info("接收腾飞支付回调通知报文: " + responseStrBuilder.toString());
			jsonObject = JSONUtils.toJSONObject(responseStrBuilder.toString());
		} catch (Exception e) {
			log.error("接收腾飞支付回调通知出现异常");
		}
		// 商户编号
		String uid = jsonObject.getString("uid");
		// 订单号.
		String orderid = jsonObject.getString("orderid");
		//平台订单流水号
		String transid = jsonObject.getString("transid");
		//交易完成时间.
		String transtime = jsonObject.getString("transtime");
		//金额
		String price = jsonObject.getString("price");
		//支付通道
		String paytype = jsonObject.getString("paytype");
		//
		String extra = jsonObject.getString("extra");
		// 支付时间戳.
		String status = jsonObject.getString("status");
		// 平台订单号.
		String key = jsonObject.getString("key");
		// 支付状态(1是成功,2是失败).
		String version = jsonObject.getString("version");
		// 组装通知报文内容
		String messageContent = "腾飞支付,回调返回参数商户编号=" + uid + "订单号=" + orderid + "支付状态 = " + status + "金额 = " + price + "分,支付通道 = "+paytype+",签名="
				+ key;
		// 日志打印.
		log.info(messageContent);

		RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(orderid);
		if (rechargeOrder == null) {
			log.error("根据订单号[" + orderid + "]查找充值订单记录为空");
			return "SUCCESS";
		}

		// 更新回调通知信息表
		payNotifyLog.setMessageContent(messageContent);
		payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
		payNotifyLog.setOrderId(rechargeOrder.getSerialNumber());
		payNotifyLogService.updateByPrimaryKey(payNotifyLog);

		// 获取秘钥.
		String Md5key = rechargeOrder.getSign();
		String MARK = "&";
		// 拼接加密字符串.
		String mD5Str = "extra=" + extra + MARK + "orderid=" + orderid + MARK + "paytype=" + paytype + MARK + "price=" + price+MARK+
		                "status=" + status + MARK + "transid=" + transid + MARK + "transtime=" + transtime + MARK + "uid=" + uid + MARK + "version=" +version+ Md5key;
		log.info("当前MD5源码:" + mD5Str);
		// Md5加密串并且大写
		String WaitSign = Md5Util.getMD5ofStr(mD5Str);
		log.info("加密后的MD5: " + WaitSign);

		// 从第三方返回的MD5加密签名串与后台Md5加密串进行校验
		if (WaitSign.compareTo(key) == 0) {
			boolean dealResult = false;
			if (!StringUtils.isEmpty(status) && status.equals("2")) {
				// 将金额从分转换为元.
				BigDecimal factMoneyValue = new BigDecimal(Integer.parseInt(price)).divide(new BigDecimal(100));
				try {
					dealResult = rechargeOrderService.rechargeOrderSuccessDeal(orderid, factMoneyValue);
				} catch (UnEqualVersionException e) {
					log.error("腾飞支付支付充值时发现版本号不一致,订单号[" + orderid + "]");
					// 启动新进程进行多次重试
					UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
							UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, orderid, factMoneyValue, "");
					thread.start();
				} catch (Exception e) {
					// 异常不进行重试
					log.error("腾飞支付处理入账发生异常", e);
				}
			} else {
				if (!status.equals("2")) {
					log.info("腾飞支付处理错误支付结果为：" + status);
				}
			}
			log.info("腾飞支付订单处理入账结果:{}", dealResult);
			// 通知第三方报文接收成功
			return "SUCCESS";
		} else {
			log.info("MD5校验失败,报文返回MD5签名内容[" + key + "],计算MD5签名内容[" + WaitSign + "]");
			return "SUCCESS";
		}
	}

}
