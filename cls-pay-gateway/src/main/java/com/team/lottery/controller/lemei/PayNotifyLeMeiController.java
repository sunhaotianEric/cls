package com.team.lottery.controller.lemei;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.model.lemei.LeMeiReturn;
import com.team.lottery.pay.util.Md5Util;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.ChargePay;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.ThirdPayConfig;

/**
 * 乐美支付回调处理
 * @Description: 
 * @Author: Jamine.
 * @CreateTime: 2019-05-15 02:36:32.
 */
@Controller
public class PayNotifyLeMeiController extends BaseController {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;

	/**
	 * 乐美支付回调处理地址.
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/notify/lemei", method = { RequestMethod.POST, RequestMethod.GET })
	public @ResponseBody String mumingpay(HttpServletRequest request, HttpServletResponse response) {
		log.info("接收到乐美支付入账通知");
		//插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("LEMEI");
		PayNotifyLog payNotifyLog=construstNotifyLog();
		payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(),
				thirdPayConfig.getChargeDes(), "");
		
		//校验白名单
		boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
		if (!validateIp) {
			log.info("第三方支付访问IP异常!请联系客服!");
			return "SUCCESS";
		}
		// 商户号
	  	String UserID = request.getParameter("UserID");
	 	// 商户订单号.
	  	String OrderID = request.getParameter("OrderID");
	 	// 平台订单号.
	  	String OrderIDP = request.getParameter("OrderIDP");
	 	// 支付金额(元).
	  	String FaceValue = request.getParameter("FaceValue");
	 	// 支付时间
	  	String PayTime = request.getParameter("PayTime");
	 	// 订单状态.
	  	String PayState = request.getParameter("PayState");
	 	// 签名.
	  	String sign = request.getParameter("sign");
	  	
	  	// 封装返回参数.
	  	LeMeiReturn leMeiReturn = new LeMeiReturn();
	  	leMeiReturn.setFaceValue(FaceValue);
	  	leMeiReturn.setOrderID(OrderID);
	  	leMeiReturn.setOrderIDP(OrderIDP);
	  	leMeiReturn.setPayState(PayState);
	  	leMeiReturn.setPayTime(PayTime);
	  	leMeiReturn.setSign(sign);
	  	leMeiReturn.setUserID(UserID);
		
		// 日志打印.
		log.info(leMeiReturn.toString());
		
		RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(OrderID);
		if (rechargeOrder == null) {
			log.error("根据订单号[" + OrderID + "]查找充值订单记录为空");
			return "SUCCESS";
		}
		
		//更新回调通知信息表
		payNotifyLog.setMessageContent(leMeiReturn.toString());
		payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
		payNotifyLog.setOrderId(rechargeOrder.getSerialNumber());
		payNotifyLogService.updateByPrimaryKey(payNotifyLog);

		Long chargePayId = rechargeOrder.getChargePayId();

		ChargePay chargePay = chargePayService.selectByPrimaryKey(chargePayId);
		String privateKey = "";
		if (chargePay != null) {
			privateKey = chargePay.getPrivatekey();

		}
		String MARK = "&";
		// 拼接加密字符串.
		String mD5Str = "FaceValue=" + FaceValue + MARK + 
						"OrderID=" +OrderID + MARK + 
						"OrderIDP=" +OrderIDP + MARK + 
						"PayState=" +PayState + MARK + 
						"PayTime=" +PayTime + MARK + 
						"UserID=" +UserID + MARK +
						"key=" +privateKey;
		
		log.info("当前MD5源码:" + mD5Str);
		//Md5加密小写
		String WaitSign = Md5Util.getMD5ofStr(mD5Str);
		log.info("加密后的MD5: " + WaitSign);

		// 从第三方返回的MD5加密签名串与后台Md5加密串进行校验
		if (WaitSign.compareTo(sign) == 0) {
			boolean dealResult = false;
			if (!StringUtils.isEmpty(PayState) && PayState.equals("1")) {
				// 支付金额元.
				BigDecimal factMoneyValue = new BigDecimal(FaceValue);
				try {
					dealResult = rechargeOrderService.rechargeOrderSuccessDeal(OrderID, factMoneyValue);
				} catch (UnEqualVersionException e) {
					log.error("乐美支付支付充值时发现版本号不一致,订单号[" + OrderID + "]");
					// 启动新进程进行多次重试
					UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
							UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, OrderID, factMoneyValue, "");
					thread.start();
				} catch (Exception e) {
					// 异常不进行重试
					log.error("乐美支付处理入账发生异常", e);
				}
			} else {
				if (!PayState.equals("1")) {
					log.info("乐美宝支付处理错误支付结果status为：" + PayState);
				}
			}
			log.info("乐美支付订单处理入账结果:{}", dealResult);
			// 通知第三方报文接收成功
			return "SUCCESS";
		} else {
			log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容[" + WaitSign + "]");
			return "error";
		}
	}
}
