package com.team.lottery.controller.chengxintong;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.RechargeWithDrawOrderQuery;
import com.team.lottery.service.ChargePayService;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.service.WithdrawAutoConfigService;
import com.team.lottery.service.WithdrawOrderService;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.ChargePay;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.ThirdPayConfig;
import com.team.lottery.vo.WithdrawAutoConfig;
import com.team.lottery.vo.WithdrawOrder;

/**
 * 诚信通 支付入账处理.
 * 
 * @author jamine
 *
 */

@Controller
public class PayNotifyChengXinTongController extends BaseController {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;
	@Autowired
	private WithdrawOrderService withdrawOrderService;
	@Autowired
	private ChargePayService chargePayService;
	@Autowired
	private WithdrawAutoConfigService withdrawAutoConfigService;

	/**
	 * 诚信通 支付回调报文跳转地址
	 * 
	 * @return PayNotifyLog
	 * @throws Exception
	 * @throws UnEqualVersionException
	 */
	@RequestMapping(value = "/notify/chengxintong", method = { RequestMethod.POST, RequestMethod.GET })
	public @ResponseBody String laku(HttpServletRequest request, HttpServletResponse response) throws UnEqualVersionException, Exception {
		log.info("接收到诚信通 支付入账通知");

		// 插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("CHENGXINTONG");
		PayNotifyLog payNotifyLog = construstNotifyLog();
		payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(), thirdPayConfig.getChargeDes(), "");

		// 校验白名单
		boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
		if (!validateIp) {
			log.info("第三方支付访问IP异常!请联系客服!");
			return "error";
		}

		//String path = request.getContextPath();
		String partner = request.getParameter("partner");
		String tradeCode = request.getParameter("tradeCode");
		String paymoney = request.getParameter("paymoney");
		String timestamp = request.getParameter("timestamp");
		String status = request.getParameter("status");
		String sysNumber = request.getParameter("sysNumber");
		String finishTime = request.getParameter("finishTime");
		String remark = request.getParameter("remark");
		String bankAccount = request.getParameter("bankAccount");
		String bankName = request.getParameter("bankName");
		String bankUserName = request.getParameter("bankUserName");
		String settlementAmount = request.getParameter("settlementAmount");
		String sign = request.getParameter("sign");
		
		String bizSystem = "";
		Long chargePayId = null;
		// 获取当前的状态
		// String PRIVATE_KEY
		// ="MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAMZ/pTwDuhR7qwZRCLo++LaV8RwpUZdZZzqJvHaewT1/UPxn2sT+M/dNOF/Z4ittR3uhlMy4N8+MUqxyQ89qQShilmu6fzURIgNp6uQzpJEwNzPNI2j1M0aR2Bykq6SjB05pdqWYP+fLKVHyhXx9qdU1qvmlGznvmFQM9/5GE8QbAgMBAAECgYBpFlrcEv0IqxA4H96G+5+gynEDf7x6bBKCozjrc9DKRgUSG8luUUYhvvgBhtB+PTSC92BXoY+qiZebREMyJAap8/5bsfq2iKu29K1TZAI7yKGyBcq+hKYkjPhGZ38QpRqWYWMcKcGrsxmRiByCPoCqWxjI1J8tNZZsCShiJvR0AQJBAOgLAdfb8jqIET+M60YXfNy/JBkTP6KbK7eq1iLS73Pd3sehEGJjfdtTDTBayDzalFsvU1HN+SNSmOG6107boEECQQDa/gvaU6FBqOHuIMLKVFsjJP2lGBknWh4igiGd9kd7OUnidTW6VWD9FEWSi+d3Y1h+2Zd4QKq3WZx6XIe3Po1bAkBrNeq8GVtpv8R4YFu0HtYKCCZdj48vShKA4eXeYSdRmYl9IuW3D9DurQjC9q7drwAswUj12vzpXRhV80XXoZIBAkBK+ubGBxJUb+WDafYn3oAh5V1vNHQQVDuzJwkpk5Rf7XqNrgIKXYdKv7EefwZuizZWoFvLUaDiDC2We64AIMu3AkEA5dYOdqWprz29bCCZkwLJldHVSZWBbBSmmPTii4n79JB6Kudeu1kuL68ytVd+D0LeGJmALh/Z6dx+7PxjblSn4w==";
		// String key = "932888676AF3405FB969E5DFA24CFCF9";
		String charset = "UTF-8";
		RechargeWithDrawOrderQuery query = new RechargeWithDrawOrderQuery();
		query.setSerialNumber(tradeCode);
		List<WithdrawOrder> rechargeWithDrawOrders = withdrawOrderService.getRechargeWithDrawOrders(query);
		if (rechargeWithDrawOrders != null && rechargeWithDrawOrders.size() > 0) {
			bizSystem = rechargeWithDrawOrders.get(0).getBizSystem();
		}
		WithdrawAutoConfig withdrawAutoConfig = withdrawAutoConfigService.selectWithdrawAutoConfig(bizSystem);
		if (withdrawAutoConfig != null) {
			chargePayId = withdrawAutoConfig.getChargePayId();
		}
		ChargePay chargePay = chargePayService.selectByPrimaryKey(chargePayId);
		//String privateKey = "";
		String md5Key = "";
		if (chargePay != null) {
			md5Key = chargePay.getSign();
		}
		String Md5key = chargePay.getSign();
		String md5Str ="partner="+partner+"&tradeCode="+tradeCode+"&paymoney="+paymoney+"&timestamp="+timestamp;
		//String md5 =new String();//MD5签名格式
		String WaitSign = DigestUtils.md5Hex(md5Str+Md5key);
		log.info("sign:" + sign);
		log.info("WaitSign:" + WaitSign);
		/*
		 * if (!sign.equals(jsonObj.getString("sign"))) { return; }
		 */
		// System.out.println("签名校验成功");
		//BigDecimal factMoneyValue = new BigDecimal(jsonObj.getString("amount")).divide(new BigDecimal(100));
		/* response.getOutputStream().write("0".getBytes()); */
		log.info("诚信通支付成功通知平台处理信息如下:订单号:" + tradeCode + "订单金额:" + paymoney + " 元,支付结果:" + status);

		// 组装通知报文内容
		String messageContent = "收到诚信通 支付成功通知报文如下: 商户ID: " + partner + " 商户订单号: " + tradeCode + " 订单结果: " + status + " 订单金额: " + paymoney
				+ " MD5签名: " + sign;

		WithdrawOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
		if (rechargeWithDrawOrder == null) {
			log.error("根据订单号[" + tradeCode + "]查找充值订单记录为空");
			// 通知诚信通支付报文接收成功

			return "ok";
		}
		
		if (rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1) {
			log.error("根据订单号[" + tradeCode + "]查找充值订单记录为空或者有两笔相同订单号订单");
			// 通知诚信通支付报文接收成功
			// response.getOutputStream().write("0".getBytes("UTF-8"));
			return "ok";

		}

		// 更新回调通知信息表
		payNotifyLog.setMessageContent(messageContent);
		payNotifyLog.setBizSystem(rechargeWithDrawOrder.getBizSystem());
		payNotifyLog.setOrderId(rechargeWithDrawOrder.getSerialNumber());
		payNotifyLogService.updateByPrimaryKey(payNotifyLog);
		// 从第三方返回的MD5加密签名串与后台Md5加密串进行校验
		if (WaitSign.compareTo(sign) == 0) {
			boolean dealResult = false;
			if (!StringUtils.isEmpty(status) && status.equals("FINISH")) {

				dealResult = withdrawOrderService.setWithDrawOrdersAutoPaySuccess(rechargeWithDrawOrder.getId(), tradeCode);
			} else {
				if (!status.equals("FINISH")) {
					log.info("诚信通支付处理错误支付结果：" + status);
					withdrawOrderService.setAutoWithDrawOrdersPayClose(rechargeWithDrawOrder, withdrawAutoConfig);
				}
			}
			log.info("诚信通支付订单处理入账结果:{}", dealResult);
			// 通知诚信通支付报文接收成功
			// response.getOutputStream().write("0".getBytes("UTF-8"));
			return "ok";
		} else {
			log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容[" + WaitSign + "]");
			// 让第三方继续补发
			/* response.getOutputStream().write("error".getBytes("UTF-8")); */
			return "error";
		}

	}
}
