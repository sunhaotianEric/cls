package com.team.lottery.controller.hema;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.pay.util.Md5Util;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.util.JSONUtils;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.ThirdPayConfig;

import net.sf.json.JSONObject;

/**
 * 河马支付入账处理
 * 
 * @author jamine
 *
 */
@Controller
public class PayNotifyHemaController extends BaseController {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;
	
	/**
	 * 河马支付回调报文跳转地址
	 * 
	 * @return
	 */
	@RequestMapping(value = "/notify/hema", method = { RequestMethod.POST, RequestMethod.GET })
	public @ResponseBody String mumingpay(HttpServletRequest request, HttpServletResponse response) {
		log.info("接收到河马支付入账通知");
		
		//插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("HEMA");
		PayNotifyLog payNotifyLog=construstNotifyLog();
		payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(),
				thirdPayConfig.getChargeDes(), "");
		
		//校验白名单
		boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
		if (!validateIp) {
			log.info("第三方支付访问IP异常!请联系客服!");
			return "SUCCESS";
		}

		JSONObject jsonObject = null;
		JSONObject jsonObjectHead = null;
		JSONObject jsonObjectBody = null;
		try {
			BufferedReader streamReader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
			StringBuilder responseStrBuilder = new StringBuilder();
			String inputStr;
			while ((inputStr = streamReader.readLine()) != null) {
				responseStrBuilder.append(inputStr);
			}
			log.info("接收河马支付回调通知报文: " + responseStrBuilder.toString());
			jsonObject = JSONUtils.toJSONObject(responseStrBuilder.toString());
			String jsonHead=jsonObject.getString("REP_HEAD");
			jsonObjectHead=JSONUtils.toJSONObject(jsonHead);
			String jsonBody=jsonObject.getString("REP_BODY");
			jsonObjectBody=JSONUtils.toJSONObject(jsonBody);
		} catch (Exception e) {
			log.error("接收河马支付回调通知出现异常");
		}
		
		
		String orderState = jsonObjectBody.getString("orderState");
		String tranSeqId = jsonObjectBody.getString("tranSeqId");
		String orderId = jsonObjectBody.getString("orderId");
		String payTime = jsonObjectBody.getString("payTime");
		String orderAmt = jsonObjectBody.getString("orderAmt");
		String sign = jsonObjectHead.getString("sign");
		
		//组装通知报文内容
		String messageContent = "河马支付,回调返回参数支付状态=" + orderState + "订单号=" + orderId + "时间 = " + payTime  + "金额="
				+ orderAmt + "签名=" + sign;
		// 日志打印.
		log.info(messageContent);
		

		
		RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(orderId);
		if (rechargeOrder == null) {
			log.error("根据订单号[" + orderId + "]查找充值订单记录为空");
			return "SUCCESS";
		}
		
		//更新回调通知信息表
		payNotifyLog.setMessageContent(messageContent);
		payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
		payNotifyLog.setOrderId(rechargeOrder.getSerialNumber());
		payNotifyLogService.updateByPrimaryKey(payNotifyLog);

		// 获取秘钥.
		String Md5key = rechargeOrder.getSign();
		String MARK = "&";
		// 拼接加密字符串.
		String mD5Str = "orderAmt=" + orderAmt + MARK + 
						"orderId=" + orderId + MARK + 
						"orderState=" + orderState + MARK + 
						"payTime=" + payTime + MARK + 
						"tranSeqId=" + tranSeqId + MARK + 
						"key=" + Md5key;

		log.info("当前MD5源码:" + mD5Str);
		// Md5加密串并且大写
		String WaitSign = Md5Util.getMD5ofStr(mD5Str).toUpperCase();
		log.info("加密后的MD5: " + WaitSign);

		// 从第三方返回的MD5加密签名串与后台Md5加密串进行校验
		if (WaitSign.compareTo(sign) == 0) {
			boolean dealResult = false;
			if (!StringUtils.isEmpty(orderState) && orderState.equals("01")) {
				// 将金额从分转换为元.
				BigDecimal factMoneyValue = new BigDecimal(orderAmt).divide(new BigDecimal(100));
				try {
					dealResult = rechargeOrderService.rechargeOrderSuccessDeal(orderId, factMoneyValue);
				} catch (UnEqualVersionException e) {
					log.error("河马支付支付充值时发现版本号不一致,订单号[" + orderId + "]");
					// 启动新进程进行多次重试
					UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
							UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, orderId, factMoneyValue, "");
					thread.start();
				} catch (Exception e) {
					// 异常不进行重试
					log.error("河马支付处理入账发生异常", e);
				}
			} else {
				if (!orderState.equals("01")) {
					log.info("河马支付处理错误支付结果State为：" + orderState);
				}
			}
			log.info("河马支付订单处理入账结果:{}", dealResult);
			// 通知第三方报文接收成功
			return "SUCCESS";
		} else {
			log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容[" + WaitSign + "]");
			return "SUCCESS";
		}
	}

}
