package com.team.lottery.controller.hipay3721;
import java.io.IOException;
import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.pay.util.Md5Util;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.ChargePay;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.ThirdPayConfig;

/**
 *hipay3721支付入账处理.
 * 
 * @author jamine
 *
 */
@Controller
public class PayNotifyHiPay3721Controller extends BaseController {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;

	/**
	 *hipay3721支付回调报文跳转地址
	 * 
	 * @return PayNotifyLog
	 */
	@RequestMapping(value = "/notify/hipay3721", method = { RequestMethod.POST, RequestMethod.GET })
	public @ResponseBody String hipay3721(HttpServletRequest request, HttpServletResponse response) throws IOException {
	log.info("接收到hipay3721支付入账通知");
		
		//插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("HIPAY3721");
		PayNotifyLog payNotifyLog=construstNotifyLog();
		payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(),
				thirdPayConfig.getChargeDes(), "");
		
		//校验白名单
		boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
		if (!validateIp) {
			log.info("第三方支付访问IP异常!请联系客服!");
			return "OK";
		}

		String platOrderNo = request.getParameter("platOrderNo");//商户订单号
		String merchantOrderNo = request.getParameter("merchantOrderNo");//
		String amount = request.getParameter("amount");//订单金额
		String status = request.getParameter("status");//订单结果
		String sign = request.getParameter("sign");//MD5签名
		
		//组装通知报文内容
		String messageContent = "hipay3721支付,回调返回参数商户订单号=" + merchantOrderNo + ",第三方订单号=" + platOrderNo + ",订单金额=" + amount + "元,交易状态=" + status +",签名=" + sign;
		// 日志打印.
		log.info(messageContent);
		// 保存日志.
		
		
		RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(merchantOrderNo);
		if (rechargeOrder == null) {
			log.error("根据订单号[" + merchantOrderNo + "]查找充值订单记录为空或者有两笔相同订单号订单");
			// 通知支付报文接收成功
			return "success";
		}
		
		//更新回调通知信息表
		payNotifyLog.setMessageContent(messageContent);
		payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
		payNotifyLog.setOrderId(rechargeOrder.getSerialNumber());
		payNotifyLogService.updateByPrimaryKey(payNotifyLog);
		//签名
		Long chargePayId = rechargeOrder.getChargePayId();

		ChargePay chargePay = chargePayService.selectByPrimaryKey(chargePayId);
		String privateKey = "";
		if (chargePay != null) {
			privateKey = chargePay.getPrivatekey();

		}
		String MARK = "&";
		String md5 = "amount=" + amount + MARK + "merchantOrderNo=" + merchantOrderNo + MARK + "platOrderNo=" + platOrderNo + MARK+"status=" + status + MARK+"key=" + privateKey ;
		log.info("当前MD5源码:" + md5);
		//Md5加密串
		String WaitSign = Md5Util.getMD5ofStr(md5);
		log.info("加密后的MD5: " + WaitSign);
		// 从第三方返回的MD5加密签名串与后台Md5加密串进行校验
		if (WaitSign.compareTo(sign) == 0) {
			boolean dealResult = false;
			if (!StringUtils.isEmpty(status) && status.equals("1")) {
				// 将金额从分转换为元.
				BigDecimal factMoneyValue = new BigDecimal(amount);
				try {
					dealResult = rechargeOrderService.rechargeOrderSuccessDeal(merchantOrderNo, factMoneyValue);
				} catch (UnEqualVersionException e) {
					log.error("hipay3721支付支付充值时发现版本号不一致,订单号[" + merchantOrderNo + "]");
					// 启动新进程进行多次重试
					UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
							UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, merchantOrderNo, factMoneyValue, "");
					thread.start();
				} catch (Exception e) {
					// 异常不进行重试
					log.error("hipay3721支付处理入账发生异常", e);
				}
			} else {
				if (!status.equals("1")) {
					log.info("hipay3721支付处理错误支付结果payState为：" + status);

				}
			}
			log.info("hipay3721支付订单处理入账结果:{}", dealResult);
			// 通知第三方报文接收成功
			return "success";
		} else {
			log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容[" + WaitSign + "]");
			return "error";
		}
	}

}
