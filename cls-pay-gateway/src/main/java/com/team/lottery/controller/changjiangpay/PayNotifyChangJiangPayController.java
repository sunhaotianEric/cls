package com.team.lottery.controller.changjiangpay;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.pay.util.Md5Util;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.util.JSONUtils;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.ThirdPayConfig;

import net.sf.json.JSONObject;

/**
 * 长江支付入账处理
 * 
 * @author jamine
 *
 */
@Controller
public class PayNotifyChangJiangPayController extends BaseController {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;
	
	/**
	 * 长江支付回调报文跳转地址
	 * 
	 * @return
	 */
	@RequestMapping(value = "/notify/changjiangpay", method = { RequestMethod.POST, RequestMethod.GET })
	public @ResponseBody String mumingpay(HttpServletRequest request, HttpServletResponse response) {
		log.info("接收到长江支付入账通知");
		
		//插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("CHANGJIANGPAY");
		PayNotifyLog payNotifyLog=construstNotifyLog();
		payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(),
				thirdPayConfig.getChargeDes(), "");
		
		//校验白名单
		boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
		if (!validateIp) {
			log.info("第三方支付访问IP异常!请联系客服!");
			return "SUCCESS";
		}

		JSONObject jsonObject = null;
		try {
			BufferedReader streamReader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
			StringBuilder responseStrBuilder = new StringBuilder();
			String inputStr;
			while ((inputStr = streamReader.readLine()) != null) {
				responseStrBuilder.append(inputStr);
			}
			log.info("接收长江支付回调通知报文: " + responseStrBuilder.toString());
			jsonObject = JSONUtils.toJSONObject(responseStrBuilder.toString());
		} catch (Exception e) {
			log.error("接收长江支付回调通知出现异常");
		}
		// 获取返回状态success的值,值为true的时候为成功
		String success = jsonObject.getString("success");
		if (!success.equals("true")) {
			log.error("接收长江支付状态success为:" + success);
			return "SUCCESS";
		}
		String jsonObjectDataStr = jsonObject.getString("data");
		log.info("接收长江支付回调通知报文data数据为: " + jsonObjectDataStr);
		JSONObject jsonObjectData = JSONUtils.toJSONObject(jsonObjectDataStr);
		
		String status = jsonObjectData.getString("status");
		String request_no = jsonObjectData.getString("request_no");
		String request_time = jsonObjectData.getString("request_time");
		String pay_time = jsonObjectData.getString("pay_time");
		String pay_channel = jsonObjectData.getString("pay_channel");
		String amount = jsonObjectData.getString("amount");
		String order_no = jsonObjectData.getString("order_no");
		String merchant_no = jsonObjectData.getString("merchant_no");
		String nonce_str = jsonObjectData.getString("nonce_str");
		String call_nums = jsonObjectData.getString("call_nums");
		String sign = jsonObjectData.getString("sign");
		
		//组装通知报文内容
		String messageContent = "长江支付,回调返回参数支付状态=" + status + "订单号=" + request_no + "时间戳 = " + request_time + "支付成功时间戳 = " + pay_time + "金额="
				+ amount + "商户号=" + merchant_no + "随机字符串=" + nonce_str + "回调次数=" + call_nums + "签名=" + sign;
		// 日志打印.
		log.info(messageContent);
		

		
		RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(request_no);
		if (rechargeOrder == null) {
			log.error("根据订单号[" + request_no + "]查找充值订单记录为空");
			return "SUCCESS";
		}
		
		//更新回调通知信息表
		payNotifyLog.setMessageContent(messageContent);
		payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
		payNotifyLog.setOrderId(rechargeOrder.getSerialNumber());
		payNotifyLogService.updateByPrimaryKey(payNotifyLog);

		// 获取秘钥.
		String Md5key = rechargeOrder.getSign();
		String MARK = "&";
		
		// 拼接加密字符串.
		String mD5Str = "amount=" + amount + MARK + 
						"call_nums=" + call_nums + MARK + 
						"merchant_no=" + merchant_no + MARK + 
						"nonce_str=" + nonce_str + MARK + 
						"order_no=" + order_no + MARK + 
						"pay_channel=" + pay_channel + MARK + 
						"pay_time=" + pay_time + MARK + 
						"request_no=" + request_no + MARK + 
						"request_time=" + request_time + MARK + 
						"status=" + status + MARK + 
						"key=" + Md5key;

		log.info("当前MD5源码:" + mD5Str);
		// Md5加密串并且大写
		String WaitSign = Md5Util.getMD5ofStr(mD5Str).toUpperCase();
		log.info("加密后的MD5: " + WaitSign);

		// 从第三方返回的MD5加密签名串与后台Md5加密串进行校验
		if (WaitSign.compareTo(sign) == 0) {
			boolean dealResult = false;
			if (!StringUtils.isEmpty(status) && status.equals("3")) {
				// 将金额从分转换为元.
				BigDecimal factMoneyValue = new BigDecimal(amount);
				try {
					dealResult = rechargeOrderService.rechargeOrderSuccessDeal(request_no, factMoneyValue);
				} catch (UnEqualVersionException e) {
					log.error("长江支付支付充值时发现版本号不一致,订单号[" + request_no + "]");
					// 启动新进程进行多次重试
					UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
							UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, request_no, factMoneyValue, "");
					thread.start();
				} catch (Exception e) {
					// 异常不进行重试
					log.error("长江支付处理入账发生异常", e);
				}
			} else {
				if (!status.equals("3")) {
					log.info("长江支付处理错误支付结果payState为：" + status);
				}
			}
			log.info("长江支付订单处理入账结果:{}", dealResult);
			// 通知第三方报文接收成功
			return "SUCCESS";
		} else {
			log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容[" + WaitSign + "]");
			return "SUCCESS";
		}
	}

}
