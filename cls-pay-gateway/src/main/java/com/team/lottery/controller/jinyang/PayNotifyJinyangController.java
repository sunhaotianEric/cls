package com.team.lottery.controller.jinyang;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.returnModel.JinYangReturnParam;
import com.team.lottery.pay.util.Md5Util;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.ThirdPayConfig;

/**
 * 金阳支付入账处理.
 * 
 * @author jamine
 *
 */
@Controller
public class PayNotifyJinyangController extends BaseController {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;

	/**
	 * 金阳支付回调报文跳转地址
	 * 
	 * @return
	 */
	@RequestMapping(value = "/notify/jinyang", method = { RequestMethod.POST, RequestMethod.GET })
	public @ResponseBody String jinyang(HttpServletRequest request, HttpServletResponse response) {
		log.info("接收到金阳支付入账通知");
		
		//插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("JINYANG");
		PayNotifyLog payNotifyLog=construstNotifyLog();
		payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(),
				thirdPayConfig.getChargeDes(), "");
		
		//校验白名单
		boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
		if (!validateIp) {
			log.info("第三方支付访问IP异常!请联系客服!");
			return "ERROR";
		}
		JinYangReturnParam param = new JinYangReturnParam();
		param.setPartner(request.getParameter("partner"));
		param.setOrdernumber(request.getParameter("ordernumber"));
		param.setOrderstatus(request.getParameter("orderstatus"));
		param.setPaymoney(request.getParameter("paymoney"));
		param.setSysnumber(request.getParameter("sysnumber"));
		param.setAttach(request.getParameter("attach"));
		param.setSign(request.getParameter("sign"));
		
		//组装通知报文内容
		String messageContent = "收到金阳支付通知报文如下: partner: " + request.getParameter("partner") + " ordernumber: "
				+ request.getParameter("ordernumber") + " orderstatus: " + request.getParameter("orderstatus") + " paymoney: "
				+ request.getParameter("paymoney") + " sysnumber: " + request.getParameter("sysnumber") + " attach: "
				+ request.getParameter("attach") + " sign: " + request.getParameter("sign");
		
		
		log.info("金阳支付成功通知平台处理信息如下: 商户ID: " + param.getPartner() + " 商户订单号: " + param.getOrdernumber() + " 订单结果: " + param.getOrderstatus()
				+ " 订单金额: " + param.getPaymoney() + " 银宝商务订单号: " + param.getSysnumber() + " 备注信息: " + param.getAttach() + "MD5签名: "
				+ param.getSign());

		RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(param.getOrdernumber());
		if (rechargeOrder == null) {
			log.error("根据订单号[" + param.getOrdernumber() + "]查找充值订单记录为空");
			return "OK";
		}
		
		//更新回调通知信息表
		payNotifyLog.setMessageContent(messageContent);
		payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
		payNotifyLog.setOrderId(rechargeOrder.getSerialNumber());
		payNotifyLogService.updateByPrimaryKey(payNotifyLog);

		// 签名
		String Md5key = rechargeOrder.getSign();
		String MARK = "&";
		String md5 = "partner=" + param.getPartner() + MARK + "ordernumber=" + param.getOrdernumber() + MARK + "orderstatus="
				+ param.getOrderstatus() + MARK + "paymoney=" + param.getPaymoney() + Md5key;
		// Md5加密串
		String WaitSign = Md5Util.getMD5ofStr(md5);
		// 从第三方返回的MD5加密签名串与后台Md5加密串进行校验
		if (WaitSign.compareTo(param.getSign()) == 0) {
			boolean dealResult = false;
			if (!StringUtils.isEmpty(param.getOrderstatus()) && param.getOrderstatus().equals("1")) {
				BigDecimal factMoneyValue = new BigDecimal(param.getPaymoney());
				try {
					dealResult = rechargeOrderService.rechargeOrderSuccessDeal(param.getOrdernumber(), factMoneyValue);
				} catch (UnEqualVersionException e) {
					log.error("金阳支付在线充值时发现版本号不一致,订单号[" + param.getOrdernumber() + "]");
					// 启动新进程进行多次重试
					UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
							UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, param.getOrdernumber(), factMoneyValue, "");
					thread.start();
				} catch (Exception e) {
					// 异常不进行重试
					log.error("金阳支付处理入账发生异常", e);
				}
			} else {
				if (!param.getOrderstatus().equals("1")) {
					log.info("金阳支付处理错误支付结果：" + param.getOrderstatus());
				}
			}
			log.info("金阳支付订单处理入账结果:{}", dealResult);
			// 通知金阳支付报文接收成功
			// out.println("ok");
			return "OK";
		} else {
			log.info("MD5校验失败,报文返回MD5签名内容[" + param.getSign() + "],计算MD5签名内容[" + WaitSign + "]");
			// 让第三方继续补发
			// out.println("error");
			return "ERROR";
		}
	}
}
