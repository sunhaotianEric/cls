package com.team.lottery.controller.quanyin;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.pay.model.QuanYinRetrunPay;
import com.team.lottery.pay.model.zhaoqianpay.ZhaoQianPayReturn;
import com.team.lottery.pay.util.Md5Util;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.ThirdPayConfig;

/**
 * 全银支付入账处理.
 * 
 * @author 
 *
 */
@Controller
public class PayNotifyQuanYinController extends BaseController {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;

	/**
	 * 全银支付回调报文跳转地址
	 * 
	 * @return
	 */
	@RequestMapping(value = "/notify/quanyin", method = { RequestMethod.POST, RequestMethod.GET })
	public @ResponseBody String mumingpay(HttpServletRequest request, HttpServletResponse response) {
		
		log.info("接收到全银支付入账通知");
		//插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("QUANYIN");
		PayNotifyLog payNotifyLog=construstNotifyLog();
		payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(),
				thirdPayConfig.getChargeDes(), "");
		
		//校验白名单
		boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
		if (!validateIp) {
			log.info("第三方支付访问IP异常!请联系客服!");
			return "success";
		}
		// 交易日期.
	   	String payKey = request.getParameter("payKey");
	  	// 交易时间.
	   	String productName = request.getParameter("productName");
	  	// 商户号.
	   	String orderNo = request.getParameter("orderNo");
	  	// 商户名称.
	   	String orderPrice = request.getParameter("orderPrice");
	  	// 客户号.
	   	String payWayCode = request.getParameter("payWayCode");
	  	// 交易金额.
	   	String payPayCode = request.getParameter("payPayCode");
	  	// 商户订单号.
	   	String orderDate = request.getParameter("orderDate");
	  	// 支付码表.
	   	String orderTime = request.getParameter("orderTime");
	  	// 平台订单号.
	   	String remark = request.getParameter("remark");
	  	// 渠道订单号.
	   	String trxNo = request.getParameter("trxNo");
	  	// 渠道流水号.
	   	String tradeStatus = request.getParameter("tradeStatus");
	  	// 数据签名.
	   	String sign = request.getParameter("sign");
	  	// 全银支付对象.
	   	QuanYinRetrunPay quanYinRetrunPay = new QuanYinRetrunPay();
	   	quanYinRetrunPay.setOrderDate(orderDate);
	   	quanYinRetrunPay.setOrderNo(orderNo);
	   	quanYinRetrunPay.setOrderPrice(orderPrice);
	   	quanYinRetrunPay.setOrderTime(orderTime);
	   	quanYinRetrunPay.setPayKey(payKey);
	   	quanYinRetrunPay.setPayPayCode(payPayCode);
	   	quanYinRetrunPay.setPayWayCode(payWayCode);
	   	quanYinRetrunPay.setProductName(productName);
	   	quanYinRetrunPay.setRemark(remark);
	   	quanYinRetrunPay.setSign(sign);
	   	quanYinRetrunPay.setTradeStatus(tradeStatus);
	   	quanYinRetrunPay.setTrxNo(trxNo);
		
		//组装通知报文内容
		String messageContent = quanYinRetrunPay.toString();
		// 日志打印.
		log.info(messageContent);
		
		RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(orderNo);
		if (rechargeOrder == null) {
			log.error("根据订单号[" + orderNo + "]查找充值订单记录为空");
			return "success";
		}
		
		//更新回调通知信息表
		payNotifyLog.setMessageContent(messageContent);
		payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
		payNotifyLog.setOrderId(rechargeOrder.getSerialNumber());
		payNotifyLogService.updateByPrimaryKey(payNotifyLog);

		//获取秘钥.
		String Md5key = rechargeOrder.getSign();
		String MARK = "&";
		String md5 = new String("orderDate=" + orderDate + MARK + "orderNo=" + orderNo + MARK + "orderPrice=" + orderPrice + MARK
				+ "orderTime=" + orderTime + MARK + "payKey=" + payKey + MARK + "payPayCode=" + payPayCode + MARK + "payWayCode="
				+ payWayCode + MARK + "productName=" + productName + MARK + "remark=" + remark + MARK + "tradeStatus=" + tradeStatus
				+ MARK + "trxNo=" + trxNo + MARK + "paySecret=" + Md5key);//MD5签名格式
		log.info("加密前的MD5: " + md5);
		//Md5加密串并且大写
		String WaitSign = Md5Util.getMD5ofStr(md5).toUpperCase();
		log.info("加密后的MD5: " + WaitSign);
		
		// 从第三方返回的MD5加密签名串与后台Md5加密串进行校验
		if (WaitSign.compareTo(sign) == 0) {
			boolean dealResult = false;
			if (!StringUtils.isEmpty(tradeStatus) && tradeStatus.equals("SUCCESS")) {
				// 支付金额元.
				BigDecimal factMoneyValue = new BigDecimal(orderPrice);
				try {
					dealResult = rechargeOrderService.rechargeOrderSuccessDeal(orderNo, factMoneyValue);
				} catch (UnEqualVersionException e) {
					log.error("全银支付支付充值时发现版本号不一致,订单号[" + orderNo + "]");
					// 启动新进程进行多次重试
					UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
							UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, orderNo, factMoneyValue, "");
					thread.start();
				} catch (Exception e) {
					// 异常不进行重试
					log.error("全银支付处理入账发生异常", e);
				}
			} else {
				if (!tradeStatus.equals("SUCCESS")) {
					log.info("全银支付处理错误支付结果status为：" + tradeStatus);
				}
			}
			log.info("全银支付订单处理入账结果:{}", dealResult);
			// 通知第三方报文接收成功
			return "success";
		} else {
			log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容[" + WaitSign + "]");
			return "success";
		}
	}

}
