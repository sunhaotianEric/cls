package com.team.lottery.controller.dudupay;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.returnModel.DuDuPayReturnParam;
import com.team.lottery.pay.util.Md5Util;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.util.JSONUtils;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.ThirdPayConfig;

import net.sf.json.JSONObject;

/**
 * 嘟嘟支付入账处理.
 * 
 * @author jamine
 *
 */
@Controller
public class PayNotifyDuduController extends BaseController {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;

	/**
	 * 嘟嘟支付回调报文跳转地址
	 * 
	 * @return PayNotifyLog
	 */
	@RequestMapping(value = "/notify/dudupay", method = { RequestMethod.POST, RequestMethod.GET })
	public @ResponseBody String dudupay(HttpServletRequest request, HttpServletResponse response) throws IOException {
		log.info("接收到嘟嘟支付入账通知");
		
		//插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("DUDUPAY");
		PayNotifyLog payNotifyLog = construstNotifyLog();
		payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(),
				thirdPayConfig.getChargeDes(), "");
		//校验白名单
		String ip = getRequestIp();
		boolean validateIp = validateRequestIp(thirdPayConfig, ip);
		if (!validateIp) {
			log.info("嘟嘟支付当前通知ip[{}]未再配置的白名单里!请前往后台进行添加!", ip);
			return "error";
		}

		//组装通知报文内容
		String msgContent = "";
		BufferedReader streamReader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
		StringBuilder responseStrBuilder = new StringBuilder();
		String inputStr;
		while ((inputStr = streamReader.readLine()) != null) {
			responseStrBuilder.append(inputStr);
		}
		msgContent = responseStrBuilder.toString();
		log.info("嘟嘟嘟支付付回调通知报文: " + msgContent);
		JSONObject jsonObject = JSONUtils.toJSONObject(msgContent);
		DuDuPayReturnParam parm = JSONUtils.toBean(jsonObject, DuDuPayReturnParam.class);

		log.info("嘟嘟支付成功通知平台处理信息如下: 商户ID:" + parm.getMerchant_id() + " 订单号:" + parm.getOrder_id() + "订单金额:" + parm.getAmount() + " 元,支付方式:"
				+ parm.getPay_method() + "签名:" + parm.getSign());

		//查询对应的充值订单
		RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(parm.getOrder_id());
		if (rechargeOrder == null) {
			log.error("根据订单号[" + parm.getOrder_id() + "]查找充值订单记录为空");
			return "success";
		}
		
		//更新回调通知信息表
		payNotifyLog.setMessageContent(msgContent);
		payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
		payNotifyLog.setOrderId(rechargeOrder.getSerialNumber());
		payNotifyLogService.updateByPrimaryKeySelective(payNotifyLog);
		
		
		// 计算签名
		String Md5key = rechargeOrder.getSign();
		String MARK = "&";
		// 需要加密的字符串
		String MD5Str = "merchant_id=" + parm.getMerchant_id() + MARK + "order_id=" + parm.getOrder_id() + MARK + "amount="
				+ parm.getAmount() + MARK + "sign=" + Md5key;
		log.info("当前MD5源码:" + MD5Str);
		// Md5加密串并且大写
		String WaitSign = Md5Util.getMD5ofStr(MD5Str);
		log.info("加密后的MD5: " + WaitSign);

		// 从第三方返回的MD5加密签名串与后台Md5加密串进行校验
		if (WaitSign.compareTo(parm.getSign()) == 0) {
			boolean dealResult = false;
			// 获取当前订单的订单价格,并且将后面的0去掉!!
			String money = rechargeOrder.getApplyValue().toString();
			if (money.indexOf(".") > 0) {
				money = money.replaceAll("0+?$", "");// 去掉多余的0
				money = money.replaceAll("[.]$", "");// 如最后一位是.则去掉
			}
			if (!StringUtils.isEmpty(parm.getAmount()) && parm.getAmount().equals(money)) {
				BigDecimal factMoneyValue = new BigDecimal(parm.getAmount());
				try {
					dealResult = rechargeOrderService.rechargeOrderSuccessDeal(parm.getOrder_id(), factMoneyValue);
				} catch (UnEqualVersionException e) {
					log.error("嘟嘟支付充值时发现版本号不一致,订单号[" + parm.getOrder_id() + "]");
					// 启动新进程进行多次重试
					UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
							UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, parm.getOrder_id(), factMoneyValue, "");
					thread.start();
				} catch (Exception e) {
					// 异常不进行重试
					log.error("嘟嘟支付处理入账发生异常", e);
				}
			} else {
				if (!parm.getAmount().equals(money)) {
					log.info("嘟嘟支付宝处理错误支付结果：" + parm.getAmount().equals(money));
				}
			}
			log.info("嘟嘟支付付订单处理入账结果:{}", dealResult);
			return "success";
		} else {
			log.info("MD5校验失败,报文返回MD5签名内容[" + parm.getSign() + "],计算MD5签名内容[" + WaitSign + "]");
			return "error";
		}
	}

}
