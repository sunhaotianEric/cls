
package com.team.lottery.controller.lppay;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.pay.util.Md5Util;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.util.JSONUtils;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.ThirdPayConfig;

import net.sf.json.JSONObject;

/** 
 * @Description: 光速支付回调通知.
 * @Author: Jamine.
 * @CreateDate：2019-10-07 04:42:46.
 */
@Controller
public class PayNotifyLpController extends BaseController {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;

	/**
	 * 安吉支付回调处理地址.
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/notify/lppay", method = { RequestMethod.POST, RequestMethod.GET })
	public @ResponseBody String mumingpay(HttpServletRequest request, HttpServletResponse response) {
		log.info("接收到LP支付入账通知");
		// 插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("LPPAY");
		PayNotifyLog payNotifyLog = construstNotifyLog();
		payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(), thirdPayConfig.getChargeDes(), "");
		// 校验白名单
		boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
		if (!validateIp) {
			log.info("第三方支付访问IP异常!请联系客服!");
			return "success";
		}
		// 状态值.
		String callbacks = request.getParameter("callbacks");
		// 用户秘钥.
		String appid = request.getParameter("appid");
		// 支付方式
		String pay_type = request.getParameter("pay_type");
		// 交易类型
		String out_trade_no =request.getParameter("out_trade_no");
		// 实付金额
		String amount_true = request.getParameter("amount_true");
		// 交易金额
		String amount = request.getParameter("amount");
		// 签名
		String sign = request.getParameter("sign");
		String msgContent  = "状态:" + callbacks+ "用户秘钥:" + appid + "支付方式:" + pay_type+ "订单号:" +  out_trade_no + "支付金额:" + amount_true + "发起金额:" +amount + "签名:" + sign; 
		// 日志打印.
		log.info(msgContent);
		RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(out_trade_no);
		if (rechargeOrder == null) {
			log.error("根据订单号[" + out_trade_no + "]查找充值订单记录为空");
			return "success";
		}
		
		// 更新回调通知信息表
		payNotifyLog.setMessageContent(msgContent);
		payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
		payNotifyLog.setOrderId(rechargeOrder.getSerialNumber());
		payNotifyLogService.updateByPrimaryKey(payNotifyLog);
		
		String mark = "&";
		// 获取秘钥.
		String md5key = rechargeOrder.getSign();
		// 拼接加密字符串.
		String md5SignStr = "amount=" + amount + mark + 
							"amount_true=" + amount_true + mark + 
							"appid=" + appid +mark + 
							"callbacks=" + callbacks +mark + 
							"out_trade_no=" + out_trade_no +mark + 
							"pay_type=" + pay_type + mark + 
							"key=" + md5key ;
		log.info("当前MD5源码:" + md5SignStr);
		// Md5加密大写
		String WaitSign = Md5Util.getMD5ofStr(md5SignStr).toUpperCase();
		log.info("加密后的MD5: " + WaitSign);

		// 从第三方返回的MD5加密签名串与后台Md5加密串进行校验
		if (WaitSign.compareTo(sign) == 0) {
			boolean dealResult = false;
			if (!StringUtils.isEmpty(callbacks) && "CODE_SUCCESS".equals(callbacks)) {
				// 支付金额元.
				BigDecimal factMoneyValue = new BigDecimal(amount);
				try {
					dealResult = rechargeOrderService.rechargeOrderSuccessDeal(out_trade_no, factMoneyValue);
				} catch (UnEqualVersionException e) {
					log.error("光速支付支付充值时发现版本号不一致,订单号[" + out_trade_no + "]");
					// 启动新进程进行多次重试
					UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
							UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, out_trade_no, factMoneyValue, "");
					thread.start();
				} catch (Exception e) {
					// 异常不进行重试
					log.error("光速支付处理入账发生异常", e);
				}
			} else {
				if (!"CODE_SUCCESS".equals(callbacks)) {
					log.info("LP支付处理错误支付结果status为：" + callbacks);
				}
			}
			log.info("LP支付订单处理入账结果:{}", dealResult);
			// 通知第三方报文接收成功
			return "success";
		} else {
			log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容[" + WaitSign + "]");
			return "success";
		}
	}
}
