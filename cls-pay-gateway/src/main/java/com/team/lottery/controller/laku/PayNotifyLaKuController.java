package com.team.lottery.controller.laku;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.pay.util.Md5Util;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.util.StringUtils;
import com.team.lottery.utils.returnModel.LaKuReturnParam;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.ThirdPayConfig;

/**
 * 拉酷支付入账处理.
 * 
 * @author jamine
 *
 */

@Controller
public class PayNotifyLaKuController extends BaseController {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;

	/**
	 * 拉酷支付回调报文跳转地址
	 * 
	 * @return PayNotifyLog
	 */
	@RequestMapping(value = "/notify/laku", method = { RequestMethod.POST, RequestMethod.GET })
	public @ResponseBody String laku(HttpServletRequest request, HttpServletResponse response) {
		log.info("接收到拉酷支付入账通知");
		
		//插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("LAKU");
		PayNotifyLog payNotifyLog=construstNotifyLog();
		payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(),
				thirdPayConfig.getChargeDes(), "");
		
		//校验白名单
		boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
		if (!validateIp) {
			log.info("第三方支付访问IP异常!请联系客服!");
			return "error";
		}
		
		MultipartResolver resolver = new CommonsMultipartResolver(request.getSession().getServletContext());
		MultipartHttpServletRequest multipartRequest = resolver.resolveMultipart(request);
		String status = multipartRequest.getParameter("status");
		String customerid = multipartRequest.getParameter("customerid");
		String sdpayno = multipartRequest.getParameter("sdpayno");
		String sdorderno = multipartRequest.getParameter("sdorderno");
		String total_fee = multipartRequest.getParameter("total_fee");
		String paytype = multipartRequest.getParameter("paytype");
		String remark = multipartRequest.getParameter("remark");
		String sign = multipartRequest.getParameter("sign");
		
		LaKuReturnParam param = new LaKuReturnParam();
		param.setCustomerid(customerid);
		param.setPaytype(paytype);
		param.setRemark(remark);
		param.setSdorderno(sdorderno);
		param.setSdpayno(sdpayno);
		param.setSign(sign);
		param.setStatus(status);
		param.setTotal_fee(total_fee);
		
		//组装通知报文内容
		String messageContent = "收到拉酷支付成功通知报文如下: 商户ID: " + param.getCustomerid() + " 商户订单号: " + param.getSdorderno() + " 订单结果: "
				+ param.getStatus() + " 订单金额: " + param.getTotal_fee() + " 拉酷商务订单号: " + param.getSdpayno() + " MD5签名: " + param.getSign();
		

		log.info("拉酷支付成功通知平台处理信息如下: 商户ID: " + param.getCustomerid() + " 商户订单号: " + param.getSdorderno() + " 订单结果: " + param.getStatus()
				+ " 订单金额: " + param.getTotal_fee() + " 拉酷商务订单号: " + param.getSdpayno() + " MD5签名: " + param.getSign());
		
		RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(param.getSdorderno());
		if (rechargeOrder == null) {
			log.error("根据订单号[" + param.getSdorderno() + "]查找充值订单记录为空");
			return "success";
		}
		
		//更新回调通知信息表
		payNotifyLog.setMessageContent(messageContent);
		payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
		payNotifyLog.setOrderId(rechargeOrder.getSerialNumber());
		payNotifyLogService.updateByPrimaryKey(payNotifyLog);

		String Md5key = rechargeOrder.getSign();
		String MARK = "&";
		String md5 = new String("customerid=" + param.getCustomerid() + MARK + "status=" + param.getStatus() + MARK + "sdpayno="
				+ param.getSdpayno() + MARK + "sdorderno=" + param.getSdorderno() + MARK + "total_fee=" + param.getTotal_fee() + MARK
				+ "paytype=" + param.getPaytype() + MARK + Md5key);// MD5签名格式
		// Md5加密串
		String WaitSign = Md5Util.getMD5ofStr(md5);
		log.info("加密后的MD5: " + WaitSign);

		// 从第三方返回的MD5加密签名串与后台Md5加密串进行校验
		if (WaitSign.compareTo(param.getSign()) == 0) {
			boolean dealResult = false;
			if (!StringUtils.isEmpty(param.getStatus()) && param.getStatus().equals("1")) {
				BigDecimal factMoneyValue = new BigDecimal(param.getTotal_fee());
				try {
					dealResult = rechargeOrderService.rechargeOrderSuccessDeal(param.getSdorderno(), factMoneyValue);
				} catch (UnEqualVersionException e) {
					log.error("拉酷支付在线充值时发现版本号不一致,订单号[" + param.getSdorderno() + "]");
					// 启动新进程进行多次重试
					UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
							UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, param.getSdorderno(), factMoneyValue, "");
					thread.start();
				} catch (Exception e) {
					// 异常不进行重试
					log.error("拉酷支付处理入账发生异常", e);
				}
			} else {
				if (!param.getStatus().equals("1")) {
					log.info("拉酷支付处理错误支付结果：" + param.getStatus());
				}
			}
			log.info("拉酷支付订单处理入账结果:{}", dealResult);
			return "success";
		} else {
			log.info("MD5校验失败,报文返回MD5签名内容[" + param.getSign() + "],计算MD5签名内容[" + WaitSign + "]");
			return "error";
		}
	}
	
}
