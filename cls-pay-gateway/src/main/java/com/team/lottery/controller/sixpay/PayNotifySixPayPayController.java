package com.team.lottery.controller.sixpay;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.pay.util.Md5Util;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.ThirdPayConfig;

/**
 * GT支付新版入账处理.
 * 
 * @author jamine
 *
 */
@Controller
public class PayNotifySixPayPayController extends BaseController {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;

	/**
	 * 慕名支付回调报文跳转地址
	 * 
	 * @return
	 */
	@RequestMapping(value = "/notify/sixpay", method = { RequestMethod.POST, RequestMethod.GET })
	public @ResponseBody String mumingpay(HttpServletRequest request, HttpServletResponse response) {
		log.info("接收到第六支付入账通知");
		//插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("SIXPAY");
		PayNotifyLog payNotifyLog=construstNotifyLog();
		payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(),
				thirdPayConfig.getChargeDes(), "");
		
		//校验白名单
		boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
		if (!validateIp) {
			log.info("第三方支付访问IP异常!请联系客服!");
			return "success";
		}
		// 商户号.
	  	String mchNo = request.getParameter("mchNo");
	 	// 平台订单号.
	  	String tradeno = request.getParameter("tradeno");
	 	// 支付金额.
	  	String money = request.getParameter("money");
	 	// 第六支付内部订单号.
	  	String orderid = request.getParameter("orderid");
	 	// 支付方式.
	  	String paytype = request.getParameter("paytype");
	 	// 支付状态.
	  	String status = request.getParameter("status");
	 	// 备注.
	  	String attach = request.getParameter("attach");
	 	// 签名.
	  	String sign = request.getParameter("sign");
		
		//组装通知报文内容
		String messageContent = "第六支付支付,回调返回参数商户号=" + mchNo + "平台订单号=" + tradeno + "支付金额 = " + money + "第六支付内部订单号 = " + orderid + "支付方式=" + paytype + "支付状态=" + status + "备注=" + attach + "签名=" + sign;
		// 日志打印.
		log.info(messageContent);
		
		RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(tradeno);
		if (rechargeOrder == null) {
			log.error("根据订单号[" + tradeno + "]查找充值订单记录为空");
			return "success";
		}
		
		//更新回调通知信息表
		payNotifyLog.setMessageContent(messageContent);
		payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
		payNotifyLog.setOrderId(rechargeOrder.getSerialNumber());
		payNotifyLogService.updateByPrimaryKey(payNotifyLog);

		//获取秘钥.
		String Md5key = rechargeOrder.getSign();
		String MARK = "&";
		//拼接加密字符串.
		String mD5Str = "attach=" + attach + MARK +
						"mchNo=" + mchNo + MARK +
						"money=" + money + MARK + 
						"orderid=" + orderid + MARK + 
						"paytype=" + paytype + MARK + 
						"status=" + status + MARK + 
						"tradeno=" + tradeno + MARK + 
						"key=" + Md5key;
		
		log.info("当前MD5源码:" + mD5Str);
		//Md5加密小写
		String WaitSign = Md5Util.getMD5ofStr(mD5Str);
		log.info("加密后的MD5: " + WaitSign);

		// 从第三方返回的MD5加密签名串与后台Md5加密串进行校验
		if (WaitSign.compareTo(sign) == 0) {
			boolean dealResult = false;
			if (!StringUtils.isEmpty(status) && status.equals("success")) {
				// 支付金额元.
				BigDecimal factMoneyValue = new BigDecimal(money);
				try {
					dealResult = rechargeOrderService.rechargeOrderSuccessDeal(tradeno, factMoneyValue);
				} catch (UnEqualVersionException e) {
					log.error("第六支付支付充值时发现版本号不一致,订单号[" + tradeno + "]");
					// 启动新进程进行多次重试
					UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
							UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, tradeno, factMoneyValue, "");
					thread.start();
				} catch (Exception e) {
					// 异常不进行重试
					log.error("第六支付处理入账发生异常", e);
				}
			} else {
				if (!status.equals("success")) {
					log.info("第六支付处理错误支付结果status为：" + status);
				}
			}
			log.info("第六支付订单处理入账结果:{}", dealResult);
			// 通知第三方报文接收成功
			return "success";
		} else {
			log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容[" + WaitSign + "]");
			return "success";
		}
	}

}
