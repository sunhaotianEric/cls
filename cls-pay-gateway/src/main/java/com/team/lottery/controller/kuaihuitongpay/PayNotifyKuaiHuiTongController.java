package com.team.lottery.controller.kuaihuitongpay;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.RechargeWithDrawOrderQuery;
import com.team.lottery.model.kuaihuitongpay.KuaiHuiTongReturn;
import com.team.lottery.pay.util.Md5Util;
import com.team.lottery.service.ChargePayService;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.service.WithdrawAutoConfigService;
import com.team.lottery.service.WithdrawOrderService;
import com.team.lottery.util.JSONUtils;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.ChargePay;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.ThirdPayConfig;
import com.team.lottery.vo.WithdrawAutoConfig;
import com.team.lottery.vo.WithdrawOrder;

import net.sf.json.JSONObject;

/**
 * 快汇通自动出款回调.
 * @Description: 
 * @Author: Jamine.
 * @CreateTime: 2019-05-20 10:44:17.
 */
@Controller
public class PayNotifyKuaiHuiTongController extends BaseController {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;
	@Autowired
	private WithdrawOrderService withdrawOrderService;
	@Autowired
	private ChargePayService chargePayService;
	@Autowired
	private WithdrawAutoConfigService withdrawAutoConfigService;

	/**
	 * 快汇通自动入款回调.
	 * @param request
	 * @param response
	 * @return
	 * @throws UnEqualVersionException
	 * @throws Exception
	 */
	@RequestMapping(value = "/notify/kuaihuitong", method = { RequestMethod.POST, RequestMethod.GET })
	public @ResponseBody String kuaiHuiTongPay(HttpServletRequest request, HttpServletResponse response) throws UnEqualVersionException, Exception {
		log.info("接收到快汇通自动出款入账通知");

		// 插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("KUAIHUITONGPAY");
		PayNotifyLog payNotifyLog = construstNotifyLog();
		payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(), thirdPayConfig.getChargeDes(), "");

		// 校验白名单
		boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
		if (!validateIp) {
			log.info("快汇通第三方支付访问IP异常!请联系客服!");
			return "success";
		}
		JSONObject jsonObject = null;
		try {
			BufferedReader streamReader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
			StringBuilder responseStrBuilder = new StringBuilder();
			String inputStr;
			while ((inputStr = streamReader.readLine()) != null) {
				responseStrBuilder.append(inputStr);
			}
			log.info("接收快汇通自动出款回调通知报文: " + responseStrBuilder.toString());
			jsonObject = JSONUtils.toJSONObject(responseStrBuilder.toString());
		} catch (Exception e) {
			log.error("接收快汇通自动出款回调通知出现异常");
		}
		
		
		//String path = request.getContextPath();
		String orderNum = jsonObject.getString("orderNo");
		String bizSystem = "";
		Long chargePayId = null;
		RechargeWithDrawOrderQuery query = new RechargeWithDrawOrderQuery();
		query.setSerialNumber(orderNum);
		List<WithdrawOrder> rechargeWithDrawOrders = withdrawOrderService.getRechargeWithDrawOrders(query);
		if (rechargeWithDrawOrders != null && rechargeWithDrawOrders.size() > 0) {
			bizSystem = rechargeWithDrawOrders.get(0).getBizSystem();
		} else {
			log.error("根据订单号[" + orderNum + "]查找提现订单记录为空");
			// 通知久通支付报文接收成功
			return "success";
		}
		WithdrawOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
		if (rechargeWithDrawOrder == null) {
			log.error("根据订单号[" + orderNum + "]查找提现订单记录为空");
			// 通知久通支付报文接收成功
			return "success";
		}
		WithdrawAutoConfig withdrawAutoConfig = withdrawAutoConfigService.selectWithdrawAutoConfig(bizSystem);
		if (withdrawAutoConfig != null) {
			chargePayId = withdrawAutoConfig.getChargePayId();
		} 
		ChargePay chargePay = chargePayService.selectByPrimaryKey(chargePayId);
		String md5Key = "";
		if (chargePay != null) {
			md5Key = chargePay.getSign();
		}
		// 获取第三方返回的参数.
		// 金额(分.)
		String transAmt = jsonObject.getString("transAmt");
		// 实际金额.
		String actualAmount = jsonObject.getString("actualAmount");
		// 交易日期.
		String transDate = jsonObject.getString("transDate");
		// 订单号.
		String orderNo = jsonObject.getString("orderNo");
		// 订单状态.
		String orderStatus = jsonObject.getString("orderStatus");
		// 交易流水.
		String tranSerno = jsonObject.getString("tranSerno");
		// 签名
		String sign = jsonObject.getString("sign");
		// 通讯答应码.
		String ret_code = jsonObject.getString("ret_code");
		// 通讯描述.
		String ret_msg = jsonObject.getString("ret_msg");
		KuaiHuiTongReturn kuaiHuiTongReturn = new KuaiHuiTongReturn();
		kuaiHuiTongReturn.setActualAmount(actualAmount);
		kuaiHuiTongReturn.setOrderNo(orderNo);
		kuaiHuiTongReturn.setOrderStatus(orderStatus);
		kuaiHuiTongReturn.setRet_code(ret_code);
		kuaiHuiTongReturn.setRet_msg(ret_msg);
		kuaiHuiTongReturn.setTransAmt(transAmt);
		kuaiHuiTongReturn.setTransDate(transDate);
		kuaiHuiTongReturn.setTranSerno(tranSerno);
		kuaiHuiTongReturn.setSign(sign);
		log.info(kuaiHuiTongReturn.toString());
		
		// md5加密校验.
		String mark = "&";
		String md5Str = "actualAmount=" + actualAmount + mark +
						"orderNo=" + orderNo + mark + 
						"orderStatus=" + orderStatus + mark + 
						"ret_code=" + ret_code + mark + 
						"ret_msg=" + ret_msg + mark + 
						"tranSerno=" + tranSerno + mark +
						"transAmt=" + transAmt + mark + 
						"transDate=" + transDate + mark + 
						"key=" + md5Key;
		String md5Sign = Md5Util.getMD5ofStr(md5Str);				
		logger.info("快汇通加密前字符串为:" + md5Str + "加密后的MD5字符串为:" + md5Sign);
		// 更新回调通知信息表
		payNotifyLog.setMessageContent(kuaiHuiTongReturn.toString());
		payNotifyLog.setBizSystem(rechargeWithDrawOrder.getBizSystem());
		payNotifyLog.setOrderId(rechargeWithDrawOrder.getSerialNumber());
		payNotifyLogService.updateByPrimaryKey(payNotifyLog);
		// 从第三方返回的MD5加密签名串与后台Md5加密串进行校验
		if (md5Sign.compareTo(sign) == 0) {
			boolean dealResult = false;
			if (!StringUtils.isEmpty(ret_code) && ret_code.equals("0000")) {
				dealResult = withdrawOrderService.setWithDrawOrdersAutoPaySuccess(rechargeWithDrawOrder.getId(), orderNum);
			} else {
				if (!ret_code.equals("0000")) {
					log.info("快汇通支付处理错误支付结果：" + ret_msg);
					withdrawOrderService.setAutoWithDrawOrdersPayClose(rechargeWithDrawOrder, withdrawAutoConfig);
				}
			}
			log.info("快汇通提现订单订单处理入账结果:{}", dealResult);
			// 通知久通支付报文接收成功
			return "success";
		} else {
			log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容[" + md5Sign + "]");
			return "success";
		}

	}
}
