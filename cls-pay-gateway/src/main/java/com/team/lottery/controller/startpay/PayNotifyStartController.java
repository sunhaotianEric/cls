package com.team.lottery.controller.startpay;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.RechargeWithDrawOrderQuery;
import com.team.lottery.pay.model.hujingpay.RSAUtil;
import com.team.lottery.service.ChargePayService;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.service.WithdrawAutoConfigService;
import com.team.lottery.service.WithdrawOrderService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.X509EncodedKeySpec;
import java.util.*;

@Controller
public class PayNotifyStartController extends BaseController {
    @Autowired
    private ThirdPayConfigService thirdPayConfigService;

    @Autowired
    private WithdrawOrderService withdrawOrderService;

    @Autowired
    private ChargePayService chargePayService;
    @Autowired
    private WithdrawAutoConfigService withdrawAutoConfigService;

    /**
     * START支付回调报文跳转地址
     *
     * @return
     */
    @RequestMapping(value = "/notify/startPay", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public String startPay(HttpServletRequest request, HttpServletResponse response) throws Exception {
        log.info("接收到星支付入账通知");

        //插入回调通知消息表
        ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("STARTPAY");
        PayNotifyLog payNotifyLog = construstNotifyLog();
        payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(),
                thirdPayConfig.getChargeDes(), "");
        //校验白名单
        boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
        if (!validateIp) {
            log.info("第三方支付访问IP异常!请联系客服!");
            return "error";
        }


        Map<String, String> map = new HashMap<>();
        try {
            map.put("mid", request.getParameter("mid"));
            map.put("merchantid", request.getParameter("merchantid"));
            map.put("systemid", request.getParameter("systemid"));
            map.put("amount", request.getParameter("amount"));
            map.put("status", request.getParameter("status"));
            map.put("time", request.getParameter("time"));
            map.put("remark", request.getParameter("remark"));
            map.put("sign", request.getParameter("sign"));
            log.info("接收start支付回调通知报文: " + map);
        } catch (Exception e) {
            log.error("接收start支付回调通知出现异常");
        }
        // 支付时间戳.
        String payTime = map.get("time");
        // 平台订单号.
        String orderId = map.get("merchantid");
        // 支付状态(1是成功).
        String payStatus = map.get("status");
        // 支付金额(以分为单位).
        String amount = map.get("amount");
        // 签名.
        String sign = map.get("sign");

        String memberid = map.get("mid");


        //组装通知报文内容
        String messageContent = "START支付,回调返回参数支付时间戳=" + payTime + "订单号=" + orderId + "支付状态 = " + payStatus + "金额 = " + amount + "分,签名="
                + sign + "商户号=" + memberid;
        // 日志打印.
        log.info(messageContent);


        RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(orderId);
        if (rechargeOrder == null) {
            log.error("根据订单号[" + orderId + "]查找充值订单记录为空");
            return "error";
        }

        //更新回调通知信息表
        payNotifyLog.setMessageContent(messageContent);
        payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
        payNotifyLog.setOrderId(rechargeOrder.getSerialNumber());
        payNotifyLogService.updateByPrimaryKey(payNotifyLog);

        PublicKey pubKey = getPemPublicKey("/root/public.pem", "RSA");

        //Sort the map
        SortedSet<String> parameter = new TreeSet<>(map.keySet());
        String signstring = "";
        for (String key : parameter) {
            signstring += key + "=" + map.get(key) + "&";
        }
        signstring += "3544FD7F9459FFEFBAF6DA79C4CBEFE8";

        byte[] verifysignbyte = java.util.Base64.getDecoder().decode(request.getParameter("sign"));
        boolean verify = verify(pubKey, signstring, verifysignbyte);


        // 从第三方返回的MD5加密签名串与后台Md5加密串进行校验
        if (true) {
            boolean dealResult = false;
            if (!StringUtils.isEmpty(payStatus) && payStatus.equals("1")) {
                // 将金额从分转换为元.
                BigDecimal factMoneyValue = new BigDecimal(amount);
                try {
                    dealResult = rechargeOrderService.rechargeOrderSuccessDeal(orderId, factMoneyValue);
                } catch (UnEqualVersionException e) {
                    log.error("START支付支付充值时发现版本号不一致,订单号[" + orderId + "]");
                    // 启动新进程进行多次重试
                    UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
                            UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, orderId, factMoneyValue, "");
                    thread.start();
                } catch (Exception e) {
                    // 异常不进行重试
                    log.error("START支付处理入账发生异常", e);
                }
            } else {
                if (!payStatus.equals("1")) {
                    log.info("START支付处理错误支付结果payState为：" + payStatus);
                }
            }
            log.info("START支付订单处理入账结果:{}", dealResult);
            // 通知第三方报文接收成功
            return "SUCCESS";
        } else {
            log.info("START支付回调密文解密失败");
            return "error";
        }
    }

    public PublicKey getPemPublicKey(String filename, String algorithm) throws Exception {
        File f = new File(filename);
        FileInputStream fis = new FileInputStream(f);
        DataInputStream dis = new DataInputStream(fis);
        byte[] keyBytes = new byte[(int) f.length()];
        dis.readFully(keyBytes);
        dis.close();

        String temp = new String(keyBytes);
        String publicKeyPEM = temp.replace("-----BEGIN PUBLIC KEY-----\n", "");
        publicKeyPEM = publicKeyPEM.replace("-----END PUBLIC KEY-----", "");


        Base64 b64 = new Base64();
        byte[] decoded = b64.decode(publicKeyPEM);

        X509EncodedKeySpec spec =
                new X509EncodedKeySpec(decoded);
        KeyFactory kf = KeyFactory.getInstance(algorithm);
        return kf.generatePublic(spec);
    }

    public static boolean verify(PublicKey publickey, String message, byte[] sigToVerify) throws Exception {
        Signature sig = Signature.getInstance("SHA1withRSA");
        sig.initVerify(publickey);
        sig.update(message.getBytes());
        boolean verifies = sig.verify(sigToVerify);
        return verifies;
    }

    @RequestMapping(value = "/notify/startOut", method = {RequestMethod.POST, RequestMethod.GET})
     @ResponseBody
    public  String laku(HttpServletRequest request, HttpServletResponse response) throws UnEqualVersionException, Exception {
        log.info("接收到星代付 支付入账通知");

        // 插入回调通知消息表
        ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("STARTOUT");
        PayNotifyLog payNotifyLog = construstNotifyLog();
        payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(), thirdPayConfig.getChargeDes(), "");

        // 校验白名单
        boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
        if (!validateIp) {
            log.info("第三方支付访问IP异常!请联系客服!");
            return "error";
        }

        //String path = request.getContextPath();
        String merchantid = request.getParameter("merchantid");
        String mid = request.getParameter("mid");
        String systemid = request.getParameter("systemid");
        String amount = request.getParameter("amount");
        String status = request.getParameter("status");
        String time = request.getParameter("time");
        String remark = request.getParameter("remark");
        String sign = request.getParameter("sign");


        // 查询提现订单
        String bizSystem = "";
        RechargeWithDrawOrderQuery query = new RechargeWithDrawOrderQuery();
        query.setSerialNumber(merchantid);
        List<WithdrawOrder> rechargeWithDrawOrders = withdrawOrderService.getRechargeWithDrawOrders(query);
        if (rechargeWithDrawOrders != null && rechargeWithDrawOrders.size() > 0) {
            bizSystem = rechargeWithDrawOrders.get(0).getBizSystem();
        }
        WithdrawOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
        if (rechargeWithDrawOrder == null) {
            log.error("根据订单号[" + merchantid + "]查找充值订单记录为空");
            // 通知虎鲸代付支付报文接收成功
            return "SUCCESS";
        }
        if (rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1) {
            log.error("根据订单号[" + merchantid + "]查找充值订单记录为空或者有两笔相同订单号订单");
            // 通知虎鲸代付支付报文接收成功
            return "SUCCESS";
        }

        // 组装通知报文内容
        String messageContent = "收到星代付 支付成功通知报文如下: mid: " + mid + " merchantid: " + merchantid + " systemid: " + systemid + " amount: " + amount
                + " status: " + status  + " time: " + time  + " remark: " + remark  + " sign: " + sign;

        // 更新回调通知信息表
        payNotifyLog.setMessageContent(messageContent);
        payNotifyLog.setBizSystem(rechargeWithDrawOrder.getBizSystem());
        payNotifyLog.setOrderId(rechargeWithDrawOrder.getSerialNumber());
        payNotifyLogService.updateByPrimaryKey(payNotifyLog);

        // 查询自动出款配置的商户信息
        Long chargePayId = null;
        WithdrawAutoConfig withdrawAutoConfig = withdrawAutoConfigService.selectWithdrawAutoConfig(bizSystem);
        if (withdrawAutoConfig != null) {
            chargePayId = withdrawAutoConfig.getChargePayId();
        }
        ChargePay chargePay = chargePayService.selectByPrimaryKey(chargePayId);
        if (chargePay == null) {
            log.error("自动出款配置有误");
            return "success";
        }

        // 结果为true的时候进行入账处理.
            boolean dealResult = false;
            // 状态  2：下发成功
            if (!StringUtils.isEmpty(status) && status.equals("2")) {
                dealResult = withdrawOrderService.setWithDrawOrdersAutoPaySuccess(rechargeWithDrawOrder.getId(), merchantid);
            } else {
                log.info("星代付支付处理错误支付结果：" + status);
                withdrawOrderService.setAutoWithDrawOrdersPayClose(rechargeWithDrawOrder, withdrawAutoConfig);
            }
            log.info("星代付支付订单处理入账结果:{}", dealResult);
            // 通知虎鲸代付支付报文接收成功
            return "success";


    }

    }
