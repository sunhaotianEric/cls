package com.team.lottery.controller.feilong;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.returnModel.FeiLongPayParam;
import com.team.lottery.pay.util.Md5Util;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.ThirdPayConfig;

/**
 * 飞龙支付入账处理.
 * 
 * @author jamine
 *
 */

@Controller
public class PayNotifyFeilongController extends BaseController {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;

	/**
	 * 菲龙支付回调报文跳转地址
	 * 
	 * @return PayNotifyLog
	 */
	@RequestMapping(value = "/notify/feilong", method = { RequestMethod.POST, RequestMethod.GET })
	public @ResponseBody String feilong(HttpServletRequest request, HttpServletResponse response) {
		log.info("接收到飞龙支付入账通知");
		
		//插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("FEILONGPAY");
		PayNotifyLog payNotifyLog=construstNotifyLog();
		payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(),
				thirdPayConfig.getChargeType(),"");
		
		//校验白名单
		boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
		if (!validateIp) {
			log.info("第三方支付访问IP异常!请联系客服!");
			return "error";
		}
		
		FeiLongPayParam param = new FeiLongPayParam();
		param.setCompanyId(request.getParameter("companyId"));
		param.setOrderId(request.getParameter("outOrderId"));
		param.setUserOrderId(request.getParameter("userOrderId"));
		param.setOutOrderId(request.getParameter("outOrderId"));
		param.setFee(request.getParameter("fee"));
		param.setPayResult(request.getParameter("payResult"));
		param.setTimestamp(request.getParameter("timestamp"));
		param.setSign(request.getParameter("sign"));
		log.info("菲龙支付成功通知平台处理信息如下: 商户ID: " + param.getCompanyId() + " 商户订单号: " + param.getUserOrderId() + " 订单结果: " + param.getPayResult()
				+ " 订单金额: " + param.getFee() + " 菲龙商务订单号: " + param.getOrderId() + " MD5签名: " + param.getSign());

		RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(param.getOrderId());
		if (rechargeOrder == null) {
			log.error("根据订单号[" + param.getOrderId() + "]查找充值订单记录为空");
			return "ok";
		}
		
		//组装通知报文内容
		String messageContent = "收到菲龙支付成功通知报文如下: 商户ID: " + param.getCompanyId() + " 商户订单号: " + param.getUserOrderId() + " 订单结果: "
				+ param.getPayResult() + " 订单金额: " + param.getFee() + " 菲龙商务订单号: " + param.getOrderId() + " MD5签名: " + param.getSign();
		//更新回调通知信息表
		payNotifyLog.setMessageContent(messageContent);
		payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
		payNotifyLog.setOrderId(rechargeOrder.getSerialNumber());
		payNotifyLogService.updateByPrimaryKey(payNotifyLog);
		// 签名
		String Md5key = rechargeOrder.getSign();
		String MARK = "_";
		String md5 = new String(param.getCompanyId() + MARK + param.getUserOrderId() + MARK + param.getFee() + MARK + Md5key);
		// Md5加密串
		String WaitSign = Md5Util.getMD5ofStr(md5);

		// 从第三方返回的MD5加密签名串与后台Md5加密串进行校验
		if (WaitSign.compareTo(param.getSign()) == 0) {
			boolean dealResult = false;
			if (!StringUtils.isEmpty(param.getPayResult()) && param.getPayResult().equals("0")) {
				BigDecimal factMoneyValue = new BigDecimal(param.getFee()).divide(new BigDecimal("100"));
				try {
					dealResult = rechargeOrderService.rechargeOrderSuccessDeal(param.getUserOrderId(), factMoneyValue);
				} catch (UnEqualVersionException e) {
					log.error("菲龙支付在线充值时发现版本号不一致,订单号[" + param.getUserOrderId() + "]");
					// 启动新进程进行多次重试
					UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
							UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, param.getUserOrderId(), factMoneyValue, "");
					thread.start();
				} catch (Exception e) {
					// 异常不进行重试
					log.error("嘟嘟支付处理入账发生异常", e);
				}
			} else {
				if (!param.getPayResult().equals("0")) {
					log.info("菲龙支付处理错误支付结果：" + param.getPayResult());
				}
			}
			log.info("菲龙支付订单处理入账结果:{}", dealResult);
			return "ok";
		} else {
			log.info("MD5校验失败,报文返回MD5签名内容[" + param.getSign() + "],计算MD5签名内容[" + WaitSign + "]");
			return "error";
		}
	}
}
