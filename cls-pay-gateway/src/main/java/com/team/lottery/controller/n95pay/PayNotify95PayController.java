
package com.team.lottery.controller.n95pay;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.pay.util.Md5Util;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.ThirdPayConfig;

/** 
 * @Description:
 * @Author: luocheng.
 * @CreateDate：2019-06-03 02:08:22.
 */
@Controller
public class PayNotify95PayController extends BaseController {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;

	/**
	 * 95支付回调报文跳转地址
	 * 
	 * @return
	 */
	@RequestMapping(value = "/notify/95pay", method = { RequestMethod.POST, RequestMethod.GET })
	public @ResponseBody String mumingpay(HttpServletRequest request, HttpServletResponse response) {
		log.info("接收到95支付入账通知");

		// 插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("95PAY");
		PayNotifyLog payNotifyLog = construstNotifyLog();
		payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(), thirdPayConfig.getChargeDes(), "");

		// 校验白名单
		boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
		if (!validateIp) {
			log.info("第三方支付访问IP异常!请联系客服!");
			return "success";
		}

		// 状态
		String status = request.getParameter("status");
		// 交易金额，单位分，以此金额作为实际到账
		String amount = request.getParameter("amount");
		// 平台订单号.
		String order_no = request.getParameter("order_no");
		// 交易状态
		String order_status = request.getParameter("order_status");
		// 交易时间
		String pay_time = request.getParameter("pay_time");
		// 签名.
		String sign = request.getParameter("sign");

		// 组装通知报文内容
		String messageContent = "95支付,status=" + status + ",amount=" + amount + ",order_no= " + order_no + ",order_status= " + order_status
				+ ",pay_time=" + pay_time + ",sign=" + sign;
		// 日志打印.
		log.info(messageContent);

		RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(order_no);
		if (rechargeOrder == null) {
			log.error("根据订单号[" + order_no + "]查找充值订单记录为空或者有两笔相同订单号订单");
			// 通知支付报文接收成功
			return "success";
		}

		// 更新回调通知信息表
		payNotifyLog.setMessageContent(messageContent);
		payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
		payNotifyLog.setOrderId(rechargeOrder.getSerialNumber());
		payNotifyLogService.updateByPrimaryKey(payNotifyLog);
		
		// 获取秘钥.
		String app_secret = rechargeOrder.getSign();
		String MARK = "&";
		// 拼接加密字符串.
		String mD5Str = "status=" + status + MARK + 
						"amount=" + amount + MARK + 
						"order_no=" + order_no + MARK + 
						"order_status=" + order_status + MARK + 
						"pay_time=" + pay_time + MARK + 
						"app_secret=" + app_secret;

		log.info("当前MD5源串:" + mD5Str);
		// Md5加密小写
		String WaitSign = Md5Util.getMD5ofStr(mD5Str).toLowerCase();
		log.info("加密后的MD5: " + WaitSign);

		// 从第三方返回的MD5加密签名串与后台Md5加密串进行校验
		if (WaitSign.compareTo(sign) == 0) {
			boolean dealResult = false;
			if (!StringUtils.isEmpty(order_status) && order_status.equals("success")) {
				// 将金额从分转换为元.
				BigDecimal factMoneyValue = new BigDecimal(amount).divide(new BigDecimal("100"));
				log.info("分转为元的金额为[{}]", factMoneyValue);
				try {
					dealResult = rechargeOrderService.rechargeOrderSuccessDeal(order_no, factMoneyValue);
				} catch (UnEqualVersionException e) {
					log.error("95支付支付充值时发现版本号不一致,订单号[" + order_no + "]");
					// 启动新进程进行多次重试
					UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
							UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, order_no, factMoneyValue, "");
					thread.start();
				} catch (Exception e) {
					// 异常不进行重试
					log.error("95支付处理入账发生异常", e);
				}
			} else {
				log.info("95支付处理错误支付结果order_status为：" + order_status);
			}
			log.info("95支付订单处理入账结果:{}", dealResult);
			// 通知第三方报文接收成功
			return "success";
		} else {
			log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容[" + WaitSign + "]");
			return "success";
		}
	}
}
