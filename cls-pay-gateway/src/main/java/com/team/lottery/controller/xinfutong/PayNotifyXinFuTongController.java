package com.team.lottery.controller.xinfutong;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.model.xinfutong.XinFuTongReturn;
import com.team.lottery.pay.util.Md5Util;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.ThirdPayConfig;

/**
 * 信付通支付回调处理
 * @Description: 
 * @Author: Jamine.
 * @CreateTime: 2019-05-15 02:36:32.
 */
@Controller
public class PayNotifyXinFuTongController extends BaseController {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;

	/**
	 * 信付通支付回调处理地址.
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/notify/xinfutong", method = { RequestMethod.POST, RequestMethod.GET })
	public @ResponseBody String mumingpay(HttpServletRequest request, HttpServletResponse response) {
		log.info("接收到信付通支付入账通知");
		//插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("XINFUTONG");
		PayNotifyLog payNotifyLog=construstNotifyLog();
		payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(),
				thirdPayConfig.getChargeDes(), "");
		
		//校验白名单
		boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
		if (!validateIp) {
			log.info("第三方支付访问IP异常!请联系客服!");
			return "ok";
		}
		
	  	String gmt_create = request.getParameter("gmt_create");
	  	String order_no = request.getParameter("order_no");
	  	String gmt_payment = request.getParameter("gmt_payment");
	  	String seller_email = request.getParameter("seller_email");
	  	String notify_time = request.getParameter("notify_time");
	  	String quantity = request.getParameter("quantity");
	  	String sign = request.getParameter("sign");
	  	String discount = request.getParameter("discount");
	  	String body = request.getParameter("body");
	  	String is_success = request.getParameter("is_success");
	  	String title = request.getParameter("title");
	  	String gmt_logistics_modify = request.getParameter("gmt_logistics_modify");
	  	String notify_id = request.getParameter("notify_id");
	  	String notify_type = request.getParameter("notify_type");
	  	String payment_type = request.getParameter("payment_type");
	  	String ext_param2 = request.getParameter("ext_param2");
	  	String price = request.getParameter("price");
	  	String total_fee = request.getParameter("total_fee");
	  	String trade_status = request.getParameter("trade_status");
	  	String trade_no = request.getParameter("trade_no");
	  	String signType = request.getParameter("signType");
	  	String seller_actions = request.getParameter("seller_actions");
	  	String seller_id = request.getParameter("seller_id");
	  	String is_total_fee_adjust = request.getParameter("is_total_fee_adjust");
	  	
	  	
	  	
	  	// 封装返回参数.
	  	XinFuTongReturn xinFuTongReturn = new XinFuTongReturn();
	  	xinFuTongReturn.setGmt_create(gmt_create);
	  	xinFuTongReturn.setOrder_no(order_no);
	  	xinFuTongReturn.setGmt_payment(gmt_payment);
	  	xinFuTongReturn.setSeller_email(seller_email);
	  	xinFuTongReturn.setNotify_time(notify_time);
	  	xinFuTongReturn.setQuantity(quantity);
	  	xinFuTongReturn.setSign(sign);
	  	xinFuTongReturn.setDiscount(discount);
	  	xinFuTongReturn.setBody(body);
	  	xinFuTongReturn.setIs_success(is_success);
	  	xinFuTongReturn.setTitle(title);
	  	xinFuTongReturn.setGmt_logistics_modify(gmt_logistics_modify);
	  	xinFuTongReturn.setNotify_id(notify_id);
	  	xinFuTongReturn.setNotify_type(notify_type);
	  	xinFuTongReturn.setPayment_type(payment_type);
	  	xinFuTongReturn.setExt_param2(ext_param2);
	  	xinFuTongReturn.setPrice(price);
	  	xinFuTongReturn.setTotal_fee(total_fee);
	  	xinFuTongReturn.setTrade_status(trade_status);
	  	xinFuTongReturn.setTrade_no(trade_no);
	  	xinFuTongReturn.setSignType(signType);
	  	xinFuTongReturn.setSeller_actions(seller_actions);
	  	xinFuTongReturn.setSeller_id(seller_id);
	  	xinFuTongReturn.setIs_total_fee_adjust(is_total_fee_adjust);

		// 日志打印.
		log.info(xinFuTongReturn.toString());
		
		RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(order_no);
		if (rechargeOrder == null) {
			log.error("根据订单号[" + order_no + "]查找充值订单记录为空");
			return "success";
		}
		
		//更新回调通知信息表
		payNotifyLog.setMessageContent(xinFuTongReturn.toString());
		payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
		payNotifyLog.setOrderId(rechargeOrder.getSerialNumber());
		payNotifyLogService.updateByPrimaryKey(payNotifyLog);

		//获取秘钥.
		String md5key = rechargeOrder.getSign();
		String MARK = "&";
		// 拼接加密字符串.
		String mD5Str = "body=" + xinFuTongReturn.getBody() + MARK + 
						"discount=" +xinFuTongReturn.getDiscount() + MARK + 
						"ext_param2=" +xinFuTongReturn.getExt_param2() + MARK + 
						"gmt_create=" +xinFuTongReturn.getGmt_create() + MARK + 
						"gmt_logistics_modify=" +xinFuTongReturn.getGmt_logistics_modify() + MARK + 
						"gmt_payment=" +xinFuTongReturn.getGmt_payment() + MARK + 
						"is_success=" +xinFuTongReturn.getIs_success() + MARK + 
						"is_total_fee_adjust=" +xinFuTongReturn.getIs_total_fee_adjust() + MARK + 
						"notify_id=" +xinFuTongReturn.getNotify_id() + MARK + 
						"notify_time=" +xinFuTongReturn.getNotify_time() + MARK + 
						"notify_type=" +xinFuTongReturn.getNotify_type() + MARK + 
						"order_no=" +xinFuTongReturn.getOrder_no() + MARK + 
						"payment_type=" +xinFuTongReturn.getPayment_type() + MARK + 
						"price=" +xinFuTongReturn.getPrice() + MARK + 
						"quantity=" +xinFuTongReturn.getQuantity() + MARK + 
						"seller_actions=" +xinFuTongReturn.getSeller_actions() + MARK + 
						"seller_email=" +xinFuTongReturn.getSeller_email() + MARK + 
						"seller_id=" +xinFuTongReturn.getSeller_id() + MARK + 
						"title=" +xinFuTongReturn.getTitle() + MARK + 
						"total_fee=" +xinFuTongReturn.getTotal_fee() + MARK + 
						"trade_no=" +xinFuTongReturn.getTrade_no() + MARK + 
						"trade_status=" +xinFuTongReturn.getTrade_status() + 
						md5key;
		
		log.info("当前MD5源码:" + mD5Str);
		//Md5加密小写
		String WaitSign = Md5Util.getMD5ofStr(mD5Str);
		log.info("加密后的MD5: " + WaitSign);

		// 从第三方返回的MD5加密签名串与后台Md5加密串进行校验
		if (WaitSign.compareTo(sign) == 0) {
			boolean dealResult = false;
			if (!StringUtils.isEmpty(trade_status) && trade_status.equals("TRADE_FINISHED")) {
				// 支付金额元.
				BigDecimal factMoneyValue = new BigDecimal(total_fee);
				try {
					dealResult = rechargeOrderService.rechargeOrderSuccessDeal(order_no, factMoneyValue);
				} catch (UnEqualVersionException e) {
					log.error("信付通支付支付充值时发现版本号不一致,订单号[" + order_no + "]");
					// 启动新进程进行多次重试
					UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
							UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, order_no, factMoneyValue, "");
					thread.start();
				} catch (Exception e) {
					// 异常不进行重试
					log.error("信付通支付处理入账发生异常", e);
				}
			} else {
				if (!trade_status.equals("TRADE_FINISHED")) {
					log.info("信付通宝支付处理错误支付结果status为：" + trade_status);
				}
			}
			log.info("信付通支付订单处理入账结果:{}", dealResult);
			// 通知第三方报文接收成功
			return "success";
		} else {
			log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容[" + WaitSign + "]");
			return "error";
		}
	}

}
