package com.team.lottery.controller.htpay;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.pay.model.hujingpay.RSAUtil;
import com.team.lottery.pay.util.Md5Util;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.vo.ChargePay;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.ThirdPayConfig;

/**
 * 恒通入账处理.
 * 
 * @author jamine
 *
 */
@Controller
public class PayNotifyHtPayController extends BaseController {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;

	/**
	 * 恒通支付回调报文跳转地址
	 * 
	 * @return
	 */
	@RequestMapping(value = "/notify/htpay", method = { RequestMethod.POST, RequestMethod.GET })
	public @ResponseBody String hujingpay(HttpServletRequest request, HttpServletResponse response) {
		log.info("接收到恒通支付入账通知");
		
		//插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("HTPAY");
		PayNotifyLog payNotifyLog=construstNotifyLog();
		payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(),
				thirdPayConfig.getChargeDes(),"");
		
		//校验白名单
		boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
		if (!validateIp) {
			log.info("第三方支付访问IP异常!请联系客服!");
			return "success";
		}
		
		// 回调内容处理
		String orderid = request.getParameter("orderid");
		// 订单结果 0 未支付  1 支付成功  2 失败
		String result = request.getParameter("result");
		// 订单金额
		String amount = request.getParameter("amount");
		// 恒通接口系统内的订单Id 
		String systemorderid = request.getParameter("systemorderid");
		// 订单时间
		String completetime = request.getParameter("completetime");
		// 通知时间
		String notifytime = request.getParameter("notifytime");
		// 备注信息
		String attach = request.getParameter("attach");
		// 提交金额
		String sourceamount = request.getParameter("sourceamount");
		String sign = request.getParameter("sign");
		
		//组装通知报文内容
		String messageContent = "收到恒通通知报文如下: orderid: " + orderid + " result: "
				+ result + " amount: " + amount + " systemorderid: "
				+ systemorderid + " completetime: " + completetime + " notifytime: "
				+ notifytime + " attach: " + attach + " sourceamount: "
				+ sourceamount + " sign: " + sign;
		log.info("恒通报文信息:" + messageContent);
		

		RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(orderid);
		if (rechargeOrder == null) {
			log.error("根据订单号[" + orderid + "]查找充值订单记录为空");
			return "success";
		}
		
		//更新回调通知信息表
		payNotifyLog.setMessageContent(messageContent);
		payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
		payNotifyLog.setOrderId(rechargeOrder.getSerialNumber());
		payNotifyLogService.updateByPrimaryKey(payNotifyLog);

		// 查询出充值配置对象.
		Long chargePayId = null;
		if (rechargeOrder != null) {
			chargePayId = rechargeOrder.getChargePayId();
		}
		ChargePay chargePay = chargePayService.selectByPrimaryKey(chargePayId);
		String key = chargePay.getSign();
		
		String MARK = "&";
		// 拼接加密字符串.
		String md5SignStr = "orderid=" + orderid + MARK + "result=" + result
				+ MARK + "amount=" + amount + MARK + "systemorderid=" + systemorderid + MARK
				+ "completetime=" + completetime + MARK + "key=" + key;
		String localSign = Md5Util.getMD5ofStr(md5SignStr).toLowerCase();
		log.info("md5原始串: " + md5SignStr + "加密后生成的交易签名:" + localSign);
		
		try {
			// 数据签名校验
			if (localSign.equals(sign)) {
				log.info("恒通支付验签通过，开始支付入账处理");
				
				if("1".equals(result)) {
					BigDecimal factMoneyValue = new BigDecimal(amount);
					boolean dealResult = false;
					try {
						dealResult = rechargeOrderService.rechargeOrderSuccessDeal(orderid, factMoneyValue);
					} catch (UnEqualVersionException e) {
						log.error("恒通支付充值时发现版本号不一致,订单号[" + orderid + "]");
						// 启动新进程进行多次重试
						UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
								UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, orderid,
								factMoneyValue, "");
						thread.start();
					} catch (Exception e) {
						// 异常不进行重试
						log.error("恒通支付处理入账发生异常", e);
					}
					log.info("恒通支付订单处理入账结果:{}", dealResult);
					return "success";
				} else {
					// 失败则不处理
					log.info("恒通支付返回订单处理结果[{}]", result);
					return "fail";
				}
				// 结果为true的时候进行入账处理.
				
			} else {
				log.info("MD5验签失败,报文返回签名内容[" + sign + "],本地计算出签名内容结果[" + localSign + "]");
				// out.println("success");
				return "error";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "success";
	}
}
