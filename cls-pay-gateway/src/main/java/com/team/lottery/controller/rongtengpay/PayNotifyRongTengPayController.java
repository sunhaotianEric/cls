package com.team.lottery.controller.rongtengpay;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.pay.util.Md5Util;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.ThirdPayConfig;

/**
 * 融腾新版入账处理.
 * 
 * @author jamine
 *
 */
@Controller
public class PayNotifyRongTengPayController extends BaseController {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;

	/**
	 * 融腾支付回调报文跳转地址
	 * 
	 * @return
	 */
	@RequestMapping(value = "/notify/rongtengpay", method = { RequestMethod.POST, RequestMethod.GET })
	public @ResponseBody String mumingpay(HttpServletRequest request, HttpServletResponse response) {
		log.info("接收到融腾支付入账通知");
		
		//插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("RONGTENGPAY");
		PayNotifyLog payNotifyLog=construstNotifyLog();
		// 保存日志.
		payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(),
				thirdPayConfig.getChargeDes(), "");
		
		//校验白名单
		boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
		if (!validateIp) {
			log.info("第三方支付访问IP异常!请联系客服!");
			return "error";
		}
		
		// 商户号.
		String memberid = request.getParameter("memberid");
		// 订单号.
		String orderid = request.getParameter("orderid");
		// 订单金额.
		String amount = request.getParameter("amount");
		// 交易流水号.
		String transaction_id = request.getParameter("transaction_id");
		// 交易时间.
		String datetime = request.getParameter("datetime");
		// 交易状态(00为成功).
		String returncode = request.getParameter("returncode");
		// 签名
		String sign = request.getParameter("sign");
        
		//组装通知报文内容
		String messageContent = "融腾支付,回调返回参数商户号=" + memberid + "商户订单号=" + orderid + "支付金额 = " + amount + "交易流水号 = " + transaction_id
				+ "交易时间=" + datetime + ",状态=" + returncode + ",签名=" + sign;
		// 日志打印.
		log.info(messageContent);
		

		RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(orderid);
		if (rechargeOrder == null) {
			log.error("根据订单号[" + orderid + "]查找充值订单记录为空或者有两笔相同订单号订单");
			// 通知支付报文接收成功
			return "success";
		}
		
		//更新回调通知信息表
		payNotifyLog.setMessageContent(messageContent);
		payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
		payNotifyLog.setOrderId(rechargeOrder.getSerialNumber());
		payNotifyLogService.updateByPrimaryKey(payNotifyLog);
		// 获取秘钥.
		String Md5key = rechargeOrder.getSign();
		String MARK = "&";
		String mD5Str = "amount=" + amount + MARK + "datetime=" + datetime + MARK + "memberid=" + memberid + MARK + "orderid=" + orderid
				+ MARK + "returncode=" + returncode + MARK + "transaction_id=" + transaction_id + MARK + "key=" + Md5key;

		log.info("当前MD5源码:" + mD5Str);
		// Md5加密小写
		String WaitSign = Md5Util.getMD5ofStr(mD5Str).toUpperCase();
		log.info("加密后的MD5: " + WaitSign);

		// 从第三方返回的MD5加密签名串与后台Md5加密串进行校验
		if (WaitSign.compareTo(sign) == 0) {
			boolean dealResult = false;
			if (!StringUtils.isEmpty(returncode) && returncode.equals("00")) {
				// 将金额从分转换为元.
				BigDecimal factMoneyValue = new BigDecimal(amount);
				try {
					dealResult = rechargeOrderService.rechargeOrderSuccessDeal(orderid, factMoneyValue);
				} catch (UnEqualVersionException e) {
					log.error("融腾支付支付充值时发现版本号不一致,订单号[" + orderid + "]");
					// 启动新进程进行多次重试
					UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
							UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, orderid, factMoneyValue, "");
					thread.start();
				} catch (Exception e) {
					// 异常不进行重试
					log.error("融腾支付处理入账发生异常", e);
				}
			} else {
				if (!returncode.equals("00")) {
					log.info("融腾支付处理错误支付结果payState为：" + returncode);

				}
			}
			log.info("融腾支付订单处理入账结果:{}", dealResult);
			// 通知第三方报文接收成功
			return "OK";
		} else {
			log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容[" + WaitSign + "]");
			return "OK";
		}
	}

}
