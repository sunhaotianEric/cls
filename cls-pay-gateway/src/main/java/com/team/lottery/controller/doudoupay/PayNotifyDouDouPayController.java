package com.team.lottery.controller.doudoupay;

import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.model.xinfa.XinFaPayReturn;
import com.team.lottery.pay.util.Md5Util;
import com.team.lottery.service.ChargePayService;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.util.JSONUtils;
import com.team.lottery.util.StringUtils;
import com.team.lottery.utils.MapUtil;
import com.team.lottery.utils.ToolKit;
import com.team.lottery.vo.ChargePay;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.ThirdPayConfig;

import net.sf.json.JSONObject;

@Controller
public class PayNotifyDouDouPayController extends BaseController {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;
	@Autowired
	private ChargePayService chargePayService;

	/**
	 * 豆豆支付入账处理.
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/notify/doudoupay", method = { RequestMethod.POST, RequestMethod.GET })
	public @ResponseBody String doudoupay(HttpServletRequest request, HttpServletResponse response) {
		log.info("接收到豆豆支付入账通知");

		// 插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("DOUDOUPAY");
		PayNotifyLog payNotifyLog = construstNotifyLog();
		payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(), thirdPayConfig.getChargeDes(), "");

		// 校验白名单
		boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
		if (!validateIp) {
			log.info("第三方支付访问IP异常!请联系客服!");
			return "success";
		}

		// 获取参数
		String pid = request.getParameter("pid");
		String tradeNo = request.getParameter("trade_no");
		String outTradeNo = request.getParameter("out_trade_no");
		String name = request.getParameter("name");
		String money = request.getParameter("money");
		String tradeStatus = request.getParameter("trade_status");
		String sign = request.getParameter("sign");
		String signType = request.getParameter("sign_type");

		// 组装通知报文内容
		String messageContent = "收到豆豆 支付成功通知报文如下: 商户号: "+ pid +"商户订单号: "
				+ outTradeNo + "平台订单号: "+ tradeNo +" 订单结果: " + tradeStatus + " 订单金额: " + money
				+ " MD5签名: " + sign + " 商品名称: " + name;
		log.info(messageContent);

		RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(outTradeNo);
		if (rechargeOrder == null) {
			log.error("根据订单号[" + outTradeNo + "]查找充值订单记录为空");
			return "success";
		}

		// 更新回调通知信息表
		payNotifyLog.setMessageContent(messageContent);
		payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
		payNotifyLog.setOrderId(rechargeOrder.getSerialNumber());
		payNotifyLogService.updateByPrimaryKey(payNotifyLog);

		// 从第三方返回的MD5加密签名串与后台Md5加密串进行校验
		String md5key = rechargeOrder.getSign();
		String MARK = "&";
		String mD5Str = "money=" + money + MARK +
				"name=" +name + MARK +
				"out_trade_no=" + outTradeNo + MARK +
				"pid=" +pid + MARK +
				"trade_no=" +tradeNo + MARK +
				"trade_status=" +tradeStatus +
				md5key;
		log.info("当前MD5源码:" + mD5Str);
		//Md5加密小写
		String WaitSign = Md5Util.getMD5ofStr(mD5Str).toLowerCase();
		log.info("加密后的MD5: " + WaitSign);
		if (WaitSign.compareTo(sign) == 0) {
			boolean dealResult = false;
			if ((!StringUtils.isEmpty(tradeStatus) && tradeStatus.equals("TRADE_SUCCESS"))) {
				// 将金额从分转换为元.
				BigDecimal factMoneyValue = new BigDecimal(money);
				try {
					dealResult = rechargeOrderService.rechargeOrderSuccessDeal(outTradeNo, factMoneyValue);
				} catch (UnEqualVersionException e) {
					log.error("豆豆支付支付充值时发现版本号不一致,订单号[" + outTradeNo + "]");
					// 启动新进程进行多次重试
					UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
							UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, outTradeNo, factMoneyValue, "");
					thread.start();
				} catch (Exception e) {
					// 异常不进行重试
					log.error("豆豆支付处理入账发生异常", e);
				}
			} else {
				if (!tradeStatus.equals("TRADE_SUCCESS")) {
					log.info("豆豆支付处理错误支付结果status为：" + tradeStatus);
					return "success";
				}
			}
			log.info("豆豆支付订单处理入账结果:{}", dealResult);
			// 通知第三方报文接收成功
			return "success";
		} else {
			log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容[" + WaitSign + "]");
			return "success";
		}

	}

}
