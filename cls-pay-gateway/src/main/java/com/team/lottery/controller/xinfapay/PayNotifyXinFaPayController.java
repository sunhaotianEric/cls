package com.team.lottery.controller.xinfapay;

import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.model.xinfa.XinFaPayReturn;
import com.team.lottery.pay.util.Md5Util;
import com.team.lottery.service.ChargePayService;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.util.JSONUtils;
import com.team.lottery.util.StringUtils;
import com.team.lottery.utils.MapUtil;
import com.team.lottery.utils.ToolKit;
import com.team.lottery.vo.ChargePay;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.ThirdPayConfig;

import net.sf.json.JSONObject;

/**
 * 鑫发入账处理
 * @Description: 
 * @Author: Jamine.
 * @CreateTime: 2019-05-25 06:23:51.
 */
@Controller
public class PayNotifyXinFaPayController extends BaseController {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;
	@Autowired
	private ChargePayService chargePayService;

	/**
	 * 鑫发入账处理.
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/notify/xinfapay", method = { RequestMethod.POST, RequestMethod.GET })
	public @ResponseBody String mumingpay(HttpServletRequest request, HttpServletResponse response) {
		log.info("接收到鑫发支付入账通知");

		// 插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("XINFAPAY");
		PayNotifyLog payNotifyLog = construstNotifyLog();
		payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(), thirdPayConfig.getChargeDes(), "");

		// 校验白名单
		boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
		if (!validateIp) {
			log.info("第三方支付访问IP异常!请联系客服!");
			return "SUCCESS";
		}

		// 获取参数
		String data = request.getParameter("data");
		String merchNo = request.getParameter("merchNo");
		String orderNo = request.getParameter("orderNo");

		// 参数封装.
		XinFaPayReturn xinFaPayReturn = new XinFaPayReturn();
		xinFaPayReturn.setData(data);
		xinFaPayReturn.setMerchNo(merchNo);
		xinFaPayReturn.setOrderNo(orderNo);

		// 日志打印.
		log.info(xinFaPayReturn.toString());

		RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(orderNo);
		if (rechargeOrder == null) {
			log.error("根据订单号[" + orderNo + "]查找充值订单记录为空");
			return "SUCCESS";
		}

		// 更新回调通知信息表
		payNotifyLog.setMessageContent(xinFaPayReturn.toString());
		payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
		payNotifyLog.setOrderId(rechargeOrder.getSerialNumber());
		payNotifyLogService.updateByPrimaryKey(payNotifyLog);
		// 解密操作.
		// Base 解码
		String jsonForData = "";
		try {
			String encodeData = URLEncoder.encode(data, "UTF-8");
			if (StringUtils.isEmpty(merchNo)) {
				log.error("返回回来的商户号为空,请联系第三方");
				return "SUCCESS";
			}
			// 根据商户号获取商户秘钥.
			List<ChargePay> chargePays = chargePayService.getChargePaysByMemberId(merchNo, rechargeOrder.getBizSystem());
			if (chargePays == null || chargePays.size() == 0) {
				log.error("根据商户号[{}]查询出的配置信息为空.", merchNo);
				return "SUCCESS";
			}
			ChargePay chargePay = chargePays.get(0);
			
			
			String  replaceBlank = StringUtils.replaceBlank(chargePay.getPrivatekey());
			log.info("私钥获取成功...");
			byte[] result = ToolKit.decryptByPrivateKey(Base64.decodeBase64(data), replaceBlank);
			jsonForData = new String(result, ToolKit.CHARSET);
		} catch (Exception e1) {
			log.error("鑫发解密出现异常");
			return "SUCCESS";
		}
		log.info("私钥解密后的数据为[{}]", jsonForData);
		Boolean isJson = JSONUtils.isJson(jsonForData);
		if (!isJson) {
			log.error("获取到的参数不是Json串.", merchNo);
			return "SUCCESS";
		}
		JSONObject jsonObject = JSONUtils.toJSONObject(jsonForData);
		String amount = jsonObject.getString("amount");
		String goodsName = jsonObject.getString("goodsName");
		String merchNo1 = jsonObject.getString("merchNo");
		String orderNo1 = jsonObject.getString("orderNo");
		String payDate = jsonObject.getString("payDate");
		String payStateCode = jsonObject.getString("payStateCode");
		String payType = jsonObject.getString("payType");
		String sign = jsonObject.getString("sign");

		// 获取秘钥.
		String Md5key = rechargeOrder.getSign();
		Map<String, String> treeMap = new TreeMap<String, String>();
		treeMap.put("amount", amount);
		treeMap.put("goodsName", goodsName);
		treeMap.put("merchNo", merchNo1);
		treeMap.put("orderNo", orderNo1);
		treeMap.put("payDate", payDate);
		treeMap.put("payStateCode", payStateCode);
		treeMap.put("payType", payType);
		String mapToJson = MapUtil.mapToJson(treeMap);
		String md5SignStr = mapToJson + Md5key;

		// 进行MD5大写加密
		String md5Sign = Md5Util.getMD5ofStr(md5SignStr).toUpperCase();
		// 打印日志
		log.info("md5原始串: " + md5SignStr + "加密后生成的交易签名:" + md5Sign);

		// 从第三方返回的MD5加密签名串与后台Md5加密串进行校验
		if (md5Sign.compareTo(sign) == 0) {
			if ("00".equals(payStateCode)) {
				boolean dealResult = false;
				// 将金额从分转换为元.
				BigDecimal factMoneyValue = new BigDecimal(Integer.parseInt(amount)).divide(new BigDecimal(100));
				try {
					dealResult = rechargeOrderService.rechargeOrderSuccessDeal(orderNo1, factMoneyValue);
				} catch (UnEqualVersionException e) {
					log.error("鑫支付支付充值时发现版本号不一致,订单号[" + orderNo1 + "]");
					// 启动新进程进行多次重试
					UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
							UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, orderNo1, factMoneyValue, "");
					thread.start();
				} catch (Exception e) {
					// 异常不进行重试
					log.error("鑫发支付处理入账发生异常", e);
				}

				log.info("鑫发支付订单处理入账结果:{}", dealResult);
				// 通知第三方报文接收成功
				return "SUCCESS";
			} else {
				log.info("第三方返回支付结果编码为[{}]", payStateCode);
				return "SUCCESS";
			}

		} else {
			log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容[" + md5Sign + "]");
			return "SUCCESS";
		}
	}

}
