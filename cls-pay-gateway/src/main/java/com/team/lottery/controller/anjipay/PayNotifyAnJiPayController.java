package com.team.lottery.controller.anjipay;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.model.anjipay.AnJiPayReturn;
import com.team.lottery.pay.util.Md5Util;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.ThirdPayConfig;

/**
 * 安吉支付回调处理
 * @Description: 
 * @Author: Jamine.
 * @CreateTime: 2019-05-15 02:36:32.
 */
@Controller
public class PayNotifyAnJiPayController extends BaseController {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;

	/**
	 * 安吉支付回调处理地址.
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/notify/anjipay", method = { RequestMethod.POST, RequestMethod.GET })
	public @ResponseBody String mumingpay(HttpServletRequest request, HttpServletResponse response) {
		log.info("接收到安吉支付入账通知");
		//插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("ANJIPAY");
		PayNotifyLog payNotifyLog=construstNotifyLog();
		payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(),
				thirdPayConfig.getChargeDes(), "");
		
		//校验白名单
		boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
		if (!validateIp) {
			log.info("第三方支付访问IP异常!请联系客服!");
			return "ok";
		}
		// 商户号
	  	String Merchant_ID = request.getParameter("Merchant_ID");
	 	// 类型.
	  	String Type = request.getParameter("Type");
	 	// 商户订单号.
	  	String Merchant_Order = request.getParameter("Merchant_Order");
	 	// 平台订单号.
	  	String Sys_Order = request.getParameter("Sys_Order");
	 	// 支付金额(元).
	  	String Amount = request.getParameter("Amount");
	 	// 订单状态.
	  	String Status = request.getParameter("Status");
	 	// 加密方式.
	  	String Sign_Type = request.getParameter("Sign_Type");
	 	// 签名.
	  	String Sign = request.getParameter("Sign");
	  	
	  	// 封装返回参数.
	  	AnJiPayReturn anJiPayReturn = new AnJiPayReturn();
	  	anJiPayReturn.setAmount(Amount);
	  	anJiPayReturn.setMerchant_ID(Merchant_ID);
	  	anJiPayReturn.setMerchant_Order(Merchant_Order);
	  	anJiPayReturn.setSign(Sign);
	  	anJiPayReturn.setSign_Type(Sign_Type);
	  	anJiPayReturn.setStatus(Status);
	  	anJiPayReturn.setSys_Order(Sys_Order);
	  	anJiPayReturn.setType(Type);
		
		// 日志打印.
		log.info(anJiPayReturn.toString());
		
		RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(Merchant_Order);
		if (rechargeOrder == null) {
			log.error("根据订单号[" + Merchant_Order + "]查找充值订单记录为空");
			return "success";
		}
		
		//更新回调通知信息表
		payNotifyLog.setMessageContent(anJiPayReturn.toString());
		payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
		payNotifyLog.setOrderId(rechargeOrder.getSerialNumber());
		payNotifyLogService.updateByPrimaryKey(payNotifyLog);

		//获取秘钥.
		String md5key = rechargeOrder.getSign();
		String MARK = "&";
		// 拼接加密字符串.
		String mD5Str = "Amount=" + Amount + MARK + 
						"Merchant_ID=" +Merchant_ID + MARK + 
						"Merchant_Order=" +Merchant_Order + MARK + 
						"Status=" +Status + MARK + 
						"Sys_Order=" +Sys_Order + MARK + 
						"Type=" +Type + 
						md5key;
		
		log.info("当前MD5源码:" + mD5Str);
		//Md5加密小写
		String WaitSign = Md5Util.getMD5ofStr(mD5Str).toUpperCase();
		log.info("加密后的MD5: " + WaitSign);

		// 从第三方返回的MD5加密签名串与后台Md5加密串进行校验
		if (WaitSign.compareTo(Sign) == 0) {
			boolean dealResult = false;
			if (!StringUtils.isEmpty(Status) && Status.equals("SUCCESS")) {
				// 支付金额元.
				BigDecimal factMoneyValue = new BigDecimal(Amount);
				try {
					dealResult = rechargeOrderService.rechargeOrderSuccessDeal(Merchant_Order, factMoneyValue);
				} catch (UnEqualVersionException e) {
					log.error("安吉支付支付充值时发现版本号不一致,订单号[" + Merchant_Order + "]");
					// 启动新进程进行多次重试
					UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
							UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, Merchant_Order, factMoneyValue, "");
					thread.start();
				} catch (Exception e) {
					// 异常不进行重试
					log.error("安吉支付处理入账发生异常", e);
				}
			} else {
				if (!Status.equals("SUCCESS")) {
					log.info("安吉宝支付处理错误支付结果status为：" + Status);
				}
			}
			log.info("安吉支付订单处理入账结果:{}", dealResult);
			// 通知第三方报文接收成功
			return "success";
		} else {
			log.info("MD5校验失败,报文返回MD5签名内容[" + Sign + "],计算MD5签名内容[" + WaitSign + "]");
			return "success";
		}
	}

}
