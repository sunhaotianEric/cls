package com.team.lottery.controller.crypt;

import java.io.PrintWriter;
import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.service.RechargeOrderService;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.ThirdPayConfig;

/**
 * 密付入账处理.
 * 
 * @author jamine
 *
 */
@Controller
public class PayNotifyCryptController extends BaseController {
	@Autowired
	private ThirdPayConfigService thirdPayConfigService;

	/**
	 * 
	 */
	@RequestMapping(value = "/deposit", method = { RequestMethod.POST, RequestMethod.GET })
	public void cryptpay(HttpServletRequest request, HttpServletResponse response) {
		
		
		try {
			response.setContentType("application/json;charset=utf-8");
			response.setCharacterEncoding("UTF-8");
			String outMsg = "{\"message\":\"received!\"}";
			PrintWriter out = null;
			out = response.getWriter();
			
			//插入回调通知消息表
			log.info("接收到密付支付入账通知");
			ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("CRYPTPAY");
			PayNotifyLog payNotifyLog=construstNotifyLog();
			payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(),
					thirdPayConfig.getChargeDes(),"");
			
			//校验白名单
			boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
			if (!validateIp) {
				log.info("第三方支付访问IP异常!请联系客服!");
				out.write(outMsg);
				out.flush();
				out.close();
			}

			String orderid = request.getParameter("orderid");
			String appId = request.getParameter("appId");
			String cryptoType = request.getParameter("cryptoType");
			String cryptoValue = request.getParameter("cryptoValue");
			String fee = request.getParameter("fee");
			String fiatCurrency = request.getParameter("fiatCurrency");
			String fiatRate = request.getParameter("fiatRate");
			String fromAddress = request.getParameter("fromAddress");
			String timestamp = request.getParameter("timestamp");
			String toAddress = request.getParameter("toAddress");
			String txId = request.getParameter("txId");
			String userId = request.getParameter("userId");
			String signature = request.getParameter("signature");
			
			log.info("接收到密付支付通知报文:orderid[{}],appId[{}], cryptoType[{}], cryptoValue[{}], fee[{}],"
					+ "fiatCurrency[{}], fiatRate[{}], fromAddress[{}], timestamp[{}], toAddress[{}],"
					+ "txId[{}], userId[{}], signature[{}]", orderid, appId, cryptoType, cryptoValue, fee, fiatCurrency, fiatRate,
					fromAddress, timestamp, toAddress, txId, userId, signature);
			
			if (StringUtils.isBlank(orderid) || StringUtils.isBlank(userId)) {
				log.error("参数错误...");
				// return;
			}

			RechargeOrderService rechargeWithDrawOrderService = ApplicationContextUtil.getBean(RechargeOrderService.class);

			RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrderService.getRechargeOrderByOderNumber(orderid);
			String userOrderId = rechargeWithDrawOrder.getSerialNumber();
			// 取订单的金额
			BigDecimal factMoneyValue = rechargeWithDrawOrder.getApplyValue();
			
			//组装通知报文内容
			String messageContent = "收到密付支付通知报文如下: orderid: " + request.getParameter("orderid") + " appId: "
					+ request.getParameter("appId") + " cryptoType: " + request.getParameter("cryptoType") + " cryptoValue: "
					+ request.getParameter("cryptoValue") + " fee: " + request.getParameter("fee") + " fiatCurrency: "
					+ request.getParameter("fiatCurrency") + " fiatRate: " + request.getParameter("fiatRate") + " fromAddress: "
					+ request.getParameter("fromAddress") + " timestamp: " + request.getParameter("timestamp") + " toAddress:"
					+ request.getParameter("toAddress") + " txId: " + request.getParameter("txId") + " userId:"
					+ request.getParameter("userId") + " signature: " + request.getParameter("signature");
			
			//更新回调通知信息表
			payNotifyLog.setMessageContent(messageContent);
			payNotifyLog.setBizSystem(rechargeWithDrawOrder.getBizSystem());
			payNotifyLog.setOrderId(userOrderId);
			payNotifyLogService.updateByPrimaryKey(payNotifyLog);
			boolean dealResult = false;
			try {
				dealResult = rechargeWithDrawOrderService.rechargeOrderSuccessDeal(userOrderId, factMoneyValue);
			} catch (UnEqualVersionException e) {
				log.error("密付支付在线充值时发现版本号不一致,订单号[" + userOrderId + "]");
				// 启动新进程进行多次重试
				UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
						UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, userOrderId, factMoneyValue, "");
				thread.start();
			} catch (Exception e) {
				// 异常不进行重试
				log.error("密付支付处理入账发生异常", e);
			}
			log.info("密付支付订单处理入账结果:{}", dealResult);
			out.write(outMsg);
			out.flush();
			out.close();
		} catch (Exception e) {
			log.error("密付支付入账处理失败，出现异常", e);
		}
	}
}
