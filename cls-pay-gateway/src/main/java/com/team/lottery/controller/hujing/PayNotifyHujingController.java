package com.team.lottery.controller.hujing;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.RechargeWithDrawOrderQuery;
import com.team.lottery.pay.model.hujingpay.HuJingPayReturn;
import com.team.lottery.pay.model.hujingpay.RSAUtil;
import com.team.lottery.service.ChargePayService;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.service.WithdrawAutoConfigService;
import com.team.lottery.service.WithdrawOrderService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.util.StringUtils;
import com.team.lottery.util.ToolKit;
import com.team.lottery.vo.ChargePay;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.ThirdPayConfig;
import com.team.lottery.vo.WithdrawAutoConfig;
import com.team.lottery.vo.WithdrawOrder;

/**
 * 虎鲸入账处理.
 * 
 * @author jamine
 *
 */
@Controller
public class PayNotifyHujingController extends BaseController {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;
	@Autowired
    private WithdrawOrderService withdrawOrderService;
	@Autowired
    private ChargePayService chargePayService;
    @Autowired
    private WithdrawAutoConfigService withdrawAutoConfigService;

	/**
	 * 虎鲸支付回调报文跳转地址
	 * 
	 * @return
	 */
	@RequestMapping(value = "/notify/hujingpay", method = { RequestMethod.POST, RequestMethod.GET })
	public @ResponseBody String hujingpay(HttpServletRequest request, HttpServletResponse response) {
		log.info("接收到虎鲸支付入账通知");
		
		//插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("HUJINGPAY");
		PayNotifyLog payNotifyLog=construstNotifyLog();
		payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(),
				thirdPayConfig.getChargeDes(),"");
		
		//校验白名单
		boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
		if (!validateIp) {
			log.info("第三方支付访问IP异常!请联系客服!");
			return "success";
		}
		
		// 新建虎鲸回调对象.
		HuJingPayReturn huJingPayReturn = new HuJingPayReturn();
		// 商户号.
		huJingPayReturn.setMerchantId(request.getParameter("merchantId"));
		// 商户订单号.
		huJingPayReturn.setMerchantOrderId(request.getParameter("merchantOrderId"));
		// 商户对应用户ID
		huJingPayReturn.setMerchantUid(request.getParameter("merchantUid"));
		// 支付金额
		huJingPayReturn.setMoney(request.getParameter("money"));
		// 订单号.
		huJingPayReturn.setOrderId(request.getParameter("orderId"));
		// 支付金额.
		huJingPayReturn.setPayAmount(request.getParameter("payAmount"));
		// 支付时间.
		huJingPayReturn.setPayTime(request.getParameter("payTime"));
		// 支付方式.
		huJingPayReturn.setPayType(request.getParameter("payType"));
		// 签名.
		huJingPayReturn.setSign(request.getParameter("sign"));
		//组装通知报文内容
		String messageContent = "收到虎鲸通知报文如下: merchantId: " + request.getParameter("merchantId") + " merchantOrderId: "
				+ request.getParameter("merchantOrderId") + " merchantUid: " + request.getParameter("merchantUid") + " money: "
				+ request.getParameter("money") + " orderId: " + request.getParameter("orderId") + " payAmount: "
				+ request.getParameter("payAmount") + " payTime: " + request.getParameter("payTime") + " payType: "
				+ request.getParameter("payType") + " sign: " + request.getParameter("sign");
		log.info("虎鲸报文信息:" + huJingPayReturn.toString());
		

		RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(huJingPayReturn.getMerchantOrderId());
		if (rechargeOrder == null) {
			log.error("根据订单号[" + huJingPayReturn.getMerchantOrderId() + "]查找充值订单记录为空");
			return "success";
		}
		
		//更新回调通知信息表
		payNotifyLog.setMessageContent(messageContent);
		payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
		payNotifyLog.setOrderId(rechargeOrder.getSerialNumber());
		payNotifyLogService.updateByPrimaryKey(payNotifyLog);

		// 查询出充值配置对象.
		Long chargePayId = null;
		if (rechargeOrder != null) {
			chargePayId = rechargeOrder.getChargePayId();
		}
		ChargePay chargePay = chargePayService.selectByPrimaryKey(chargePayId);
		String privateKey = "";
		if (chargePay != null) {
			privateKey = chargePay.getPrivatekey();
		}
		String MARK = "&";
		// 拼接加密字符串.
		String rsaStr = "merchantId=" + huJingPayReturn.getMerchantId() + MARK + "merchantOrderId=" + huJingPayReturn.getMerchantOrderId()
				+ MARK + "merchantUid=" + huJingPayReturn.getMerchantUid() + MARK + "money=" + huJingPayReturn.getMoney() + MARK
				+ "orderId=" + huJingPayReturn.getOrderId() + MARK + "payAmount=" + huJingPayReturn.getPayAmount() + MARK + "payTime="
				+ huJingPayReturn.getPayTime() + MARK + "payType=" + huJingPayReturn.getPayType();
		log.info("虎鲸需要RSA加密的字符串为[" + rsaStr + "]");
		boolean ret = false;
		try {
			// 使用公钥进行RSA解密
			ret = RSAUtil.rsaVerify(chargePay.getPublickey(), huJingPayReturn.getSign(), rsaStr);
			// 结果为true的时候进行入账处理.
			if (ret = true) {
				BigDecimal factMoneyValue = new BigDecimal(huJingPayReturn.getMoney());
				boolean dealResult = false;
				try {
					dealResult = rechargeOrderService.rechargeOrderSuccessDeal(huJingPayReturn.getMerchantOrderId(), factMoneyValue);
				} catch (UnEqualVersionException e) {
					log.error("虎鲸支付充值时发现版本号不一致,订单号[" + huJingPayReturn.getMerchantOrderId() + "]");
					// 启动新进程进行多次重试
					UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
							UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, huJingPayReturn.getMerchantOrderId(),
							factMoneyValue, "");
					thread.start();
				} catch (Exception e) {
					// 异常不进行重试
					log.error("GT支付处理入账发生异常", e);
				}
				log.info("虎鲸支付订单处理入账结果:{}", dealResult);
				// 通知凡客付报文接收成功
				// out.println("success");
				return "success";
			} else {
				log.info("RSA校验失败,报文返回签名内容[" + huJingPayReturn.getSign() + "],计算RSA签名内容结果[" + ret + "]");
				// out.println("success");
				return "error";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "success";
	}
	
	
	/**
     * 虎鲸 代付回调报文跳转地址
     * 
     * @return PayNotifyLog
     * @throws Exception
     * @throws UnEqualVersionException
     */
    @RequestMapping(value = "/notify/hujingd", method = { RequestMethod.POST, RequestMethod.GET })
    public @ResponseBody String laku(HttpServletRequest request, HttpServletResponse response) throws UnEqualVersionException, Exception {
        log.info("接收到虎鲸代付 支付入账通知");

        // 插入回调通知消息表
        ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("HUJINDPAY");
        PayNotifyLog payNotifyLog = construstNotifyLog();
        payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(), thirdPayConfig.getChargeDes(), "");

        // 校验白名单
        boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
        if (!validateIp) {
            log.info("第三方支付访问IP异常!请联系客服!");
            return "error";
        }

        //String path = request.getContextPath();
        String merchantId = request.getParameter("merchantId");
        String sign = request.getParameter("sign");
        String merchantUid = request.getParameter("merchantUid");
        String merchantOrderId = request.getParameter("merchantOrderId");
        String orderId = request.getParameter("orderId");
        String amount = request.getParameter("amount");
        String payType = request.getParameter("payType");
        String timestamp = request.getParameter("timestamp");
        String status = request.getParameter("status");
        
        
        // 查询提现订单
        String bizSystem = "";
        RechargeWithDrawOrderQuery query = new RechargeWithDrawOrderQuery();
        query.setSerialNumber(merchantOrderId);
        List<WithdrawOrder> rechargeWithDrawOrders = withdrawOrderService.getRechargeWithDrawOrders(query);
        if (rechargeWithDrawOrders != null && rechargeWithDrawOrders.size() > 0) {
            bizSystem = rechargeWithDrawOrders.get(0).getBizSystem();
        }
        WithdrawOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
        if (rechargeWithDrawOrder == null) {
            log.error("根据订单号[" + merchantOrderId + "]查找充值订单记录为空");
            // 通知虎鲸代付支付报文接收成功
            return "success";
        }
        if (rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1) {
            log.error("根据订单号[" + merchantOrderId + "]查找充值订单记录为空或者有两笔相同订单号订单");
            // 通知虎鲸代付支付报文接收成功
            return "success";
        }
        
        // 组装通知报文内容
        String messageContent = "收到虎鲸代付 支付成功通知报文如下: merchantId: " + merchantId + " sign: " + sign + " merchantUid: " + merchantUid + " merchantOrderId: " + merchantOrderId
                + " orderId: " + orderId  + " amount: " + amount  + " payType: " + payType  + " timestamp: " + timestamp + " status: " + status;
        
        // 更新回调通知信息表
        payNotifyLog.setMessageContent(messageContent);
        payNotifyLog.setBizSystem(rechargeWithDrawOrder.getBizSystem());
        payNotifyLog.setOrderId(rechargeWithDrawOrder.getSerialNumber());
        payNotifyLogService.updateByPrimaryKey(payNotifyLog);
        
        // 查询自动出款配置的商户信息
        Long chargePayId = null;
        WithdrawAutoConfig withdrawAutoConfig = withdrawAutoConfigService.selectWithdrawAutoConfig(bizSystem);
        if (withdrawAutoConfig != null) {
            chargePayId = withdrawAutoConfig.getChargePayId();
        }
        ChargePay chargePay = chargePayService.selectByPrimaryKey(chargePayId);
        if (chargePay == null) {
            log.error("自动出款配置有误");
            return "success";
        }

        // 拼接加密字符串.
        String MARK = "&";
        String rsaStr = "amount=" + amount + MARK + "merchantId=" + merchantId
                + MARK + "merchantOrderId=" + merchantOrderId + MARK + "orderId=" + orderId + MARK
                + "payType=" + payType + MARK + "status=" + status + MARK + "timestamp="
                + timestamp;
        log.info("虎鲸需要RSA加密的字符串为[" + rsaStr + "]");
        boolean ret = false;
        // 使用公钥进行RSA解密
        ret = RSAUtil.rsaVerify(chargePay.getPublickey(), sign, rsaStr);
        // 结果为true的时候进行入账处理.
        if (ret = true) {
            boolean dealResult = false;
            // 状态 -1：下发失败 1：下发成功 
            if (!StringUtils.isEmpty(status) && status.equals("1")) {
                dealResult = withdrawOrderService.setWithDrawOrdersAutoPaySuccess(rechargeWithDrawOrder.getId(), merchantOrderId);
            } else {
                log.info("虎鲸代付支付处理错误支付结果：" + status);
                withdrawOrderService.setAutoWithDrawOrdersPayClose(rechargeWithDrawOrder, withdrawAutoConfig);
            }
            log.info("虎鲸代付支付订单处理入账结果:{}", dealResult);
            // 通知虎鲸代付支付报文接收成功
            return "success";
        } else {
            log.info("RSA校验失败,报文返回签名内容[" + sign + "],计算RSA签名内容结果[" + ret + "]");
            return "success";
        }

    }
}
