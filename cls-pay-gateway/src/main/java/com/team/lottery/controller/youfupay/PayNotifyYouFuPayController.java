
package com.team.lottery.controller.youfupay;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.pay.util.Base64Utils;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.util.JSONUtils;
import com.team.lottery.util.RSAUtils;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.ChargePay;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.ThirdPayConfig;

import net.sf.json.JSONObject;

/** 
 * @Description:
 * @Author: Jamine.
 * @CreateDate：2019-10-14 05:25:49.
 */
@Controller
public class PayNotifyYouFuPayController extends BaseController {
	@Autowired
	private ThirdPayConfigService thirdPayConfigService;

	/**
	 * 优付支付回调通知处理.
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/notify/youfupay", method = { RequestMethod.POST, RequestMethod.GET })
	public @ResponseBody String mumingpay(HttpServletRequest request, HttpServletResponse response) {
		log.info("接收到优付支付入账通知");

		// 插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("YOUFUPAY");
		PayNotifyLog payNotifyLog = construstNotifyLog();
		payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(), thirdPayConfig.getChargeDes(), "");

		// 校验白名单
		boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
		if (!validateIp) {
			log.info("第三方支付访问IP异常!请联系客服!");
			return "OK";
		}

		JSONObject jsonObject = null;
		try {
			BufferedReader streamReader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
			StringBuilder responseStrBuilder = new StringBuilder();
			String inputStr;
			while ((inputStr = streamReader.readLine()) != null) {
				responseStrBuilder.append(inputStr);
			}
			log.info("接收优付支付回调通知报文: " + responseStrBuilder.toString());
			jsonObject = JSONUtils.toJSONObject(responseStrBuilder.toString());
		} catch (Exception e) {
			log.error("接收优付支付回调通知出现异常");
		}
		// 状态
		String code = jsonObject.getString("code");
		// 回调消息.
		String message = jsonObject.getString("message");
		// 我方订单号.
		String orderNo = jsonObject.getString("orderNo");
		// 三方订单号.
		String tradeNo = jsonObject.getString("tradeNo");
		// 上分金额.
		String price = jsonObject.getString("price");
		// 支付金额.
		String actualPrice = jsonObject.getString("actualPrice");
		// 交易发起时间.
		String orderTime = jsonObject.getString("orderTime");
		// 交易处理时间.
		String dealTime = jsonObject.getString("dealTime");
		// 订单状态.
		String orderStatus = jsonObject.getString("orderStatus");
		// 签名.
		String signature = jsonObject.getString("signature");

		// 时间处理
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:sss", Locale.CHINA);
		Date orderTimeDate = null;
		Date dealTimeDate = null;
		try {
			orderTimeDate = formatter1.parse(orderTime);
			dealTimeDate = formatter1.parse(dealTime);
		} catch (ParseException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss", Locale.CHINA);
		orderTime = formatter2.format(orderTimeDate);
		dealTime = formatter2.format(dealTimeDate);
		// 组装通知报文内容
		String messageContent = jsonObject.toString();
		// 日志打印.
		log.info(messageContent);

		RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(orderNo);
		if (rechargeOrder == null) {
			log.error("根据订单号[" + orderNo + "]查找充值订单记录为空");
			return "OK";
		}

		// 更新回调通知信息表
		payNotifyLog.setMessageContent(messageContent);
		payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
		payNotifyLog.setOrderId(rechargeOrder.getSerialNumber());
		payNotifyLogService.updateByPrimaryKey(payNotifyLog);
		// 查询出配置对象.
		Long chargePayId = null;
		if (rechargeOrder != null) {
			chargePayId = rechargeOrder.getChargePayId();
		}
		ChargePay chargePay = chargePayService.selectByPrimaryKey(chargePayId);
		String publicKey = "";
		if (chargePay != null) {
			publicKey = chargePay.getPublickey();
		}
		String MARK = "&";
		// 拼接加密字符串.
		String mD5Str = "actualPrice=" + actualPrice + MARK + "dealTime=" + dealTime + MARK + "orderNo=" + orderNo + MARK + "orderStatus="
				+ orderStatus + MARK + "orderTime=" + orderTime + MARK + "price=" + price + MARK + "tradeNo=" + tradeNo;
		log.info("当前MD5源码:" + mD5Str);
		boolean checkSignByPubkey = false;
		try {
			String decode2 = URLDecoder.decode(signature,"UTF-8");
			byte[] decode = Base64Utils.decode(decode2);
			checkSignByPubkey = RSAUtils.checkSignByPubkey(mD5Str, decode, publicKey);
		} catch (Exception e1) {
			log.info("优付支付解密失败." + e1.getMessage());
			e1.printStackTrace();
			return "OK";
		}
		// Md5加密串并且大写
		log.info("checkSignByPubkey的结果为: " + checkSignByPubkey);

		// 从第三方返回的MD5加密签名串与后台Md5加密串进行校验
		if (checkSignByPubkey) {
			boolean dealResult = false;
			if (!StringUtils.isEmpty(orderStatus) && orderStatus.equals("1")) {
				BigDecimal factMoneyValue = new BigDecimal(price);
				try {
					dealResult = rechargeOrderService.rechargeOrderSuccessDeal(orderNo, factMoneyValue);
				} catch (UnEqualVersionException e) {
					log.error("GT支付支付充值时发现版本号不一致,订单号[" + orderNo + "]");
					// 启动新进程进行多次重试
					UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
							UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, orderNo, factMoneyValue, "");
					thread.start();
				} catch (Exception e) {
					// 异常不进行重试
					log.error("优付支付处理入账发生异常", e);
				}
			} else {
				if (!orderStatus.equals("1")) {
					log.info("优付支付处理错误支付结果payState为：" + orderStatus);
				}
			}
			log.info("优付支付订单处理入账结果:{}", dealResult);
			// 通知第三方报文接收成功
			return "OK";
		} else {
			log.info("RSA解密失败");
			return "OK";
		}
	}

}
