
package com.team.lottery.controller.guangsupay;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.model.guangsupay.GuangSuPayReturn;
import com.team.lottery.pay.util.Md5Util;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.ThirdPayConfig;

import net.sf.json.JSONObject;

/** 
 * @Description: 光速支付回调通知.
 * @Author: Jamine.
 * @CreateDate：2019-10-07 04:42:46.
 */
@Controller
public class PayNotifyGuangSuController extends BaseController {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;

	/**
	 * 安吉支付回调处理地址.
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/notify/guangsupay", method = { RequestMethod.POST, RequestMethod.GET })
	public @ResponseBody String mumingpay(HttpServletRequest request, HttpServletResponse response) {
		log.info("接收到光速支付入账通知");
		// 插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("GUANGSUPAY");
		PayNotifyLog payNotifyLog = construstNotifyLog();
		payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(), thirdPayConfig.getChargeDes(), "");
		JSONObject returnJson = new JSONObject();
		returnJson.put("errcode", "0");
		returnJson.put("errmsg", "");
		// 校验白名单
		boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
		if (!validateIp) {
			log.info("第三方支付访问IP异常!请联系客服!");
			return returnJson.toString();
		}
		MultipartResolver resolver = new CommonsMultipartResolver(request.getSession().getServletContext());
		MultipartHttpServletRequest multipartRequest = resolver.resolveMultipart(request);
		// 用户名
		String username = multipartRequest.getParameter("username");
		// 商户系统订单号.
		String order_no = multipartRequest.getParameter("order_no");
		// 商户支付系统订单号.
		String pay_order_no = multipartRequest.getParameter("pay_order_no");
		// 交易类型
		String order_type = multipartRequest.getParameter("order_type");
		// 交易币代码
		String vcoin_code = multipartRequest.getParameter("vcoin_code");
		// 交易数量.
		String vcoin_count = multipartRequest.getParameter("vcoin_count");
		// 法币类型
		String currency_type = multipartRequest.getParameter("currency_type");
		// 金额
		String currency_money = multipartRequest.getParameter("currency_money");
		// 支付金额
		String pay_amount = multipartRequest.getParameter("pay_amount");
		// 数量
		String price = multipartRequest.getParameter("price");
		// 状态.
		String status = multipartRequest.getParameter("status");
		// 创建时间.
		String create_time = multipartRequest.getParameter("create_time");
		// 交易结束时间.
		String finish_time = multipartRequest.getParameter("finish_time");
		// 备注.
		String remark = multipartRequest.getParameter("remark");
		// 商户号.
		String mchno = multipartRequest.getParameter("mchno");
		// 时间戳.
		String timestamp = multipartRequest.getParameter("timestamp");
		// 签名.
		String signature = multipartRequest.getParameter("signature");

		// 封装返回参数.
		GuangSuPayReturn guangSuPayReturn = new GuangSuPayReturn();
		guangSuPayReturn.setCreate_time(create_time);
		guangSuPayReturn.setCurrency_money(currency_money);
		guangSuPayReturn.setCurrency_type(currency_type);
		guangSuPayReturn.setFinish_time(finish_time);
		guangSuPayReturn.setMchno(mchno);
		guangSuPayReturn.setOrder_no(order_no);
		guangSuPayReturn.setOrder_type(order_type);
		guangSuPayReturn.setPay_amount(pay_amount);
		guangSuPayReturn.setPay_order_no(pay_order_no);
		guangSuPayReturn.setPrice(price);
		guangSuPayReturn.setRemark(remark);
		guangSuPayReturn.setSignature(signature);
		guangSuPayReturn.setStatus(status);
		guangSuPayReturn.setTimestamp(timestamp);
		guangSuPayReturn.setUsername(username);
		guangSuPayReturn.setVcoin_code(vcoin_code);
		guangSuPayReturn.setVcoin_count(vcoin_count);

		// 日志打印.
		log.info(guangSuPayReturn.toString());
		RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(order_no);
		if (rechargeOrder == null) {
			log.error("根据订单号[" + order_no + "]查找充值订单记录为空");
			return returnJson.toString();
		}

		// 更新回调通知信息表
		payNotifyLog.setMessageContent(guangSuPayReturn.toString());
		payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
		payNotifyLog.setOrderId(rechargeOrder.getSerialNumber());
		payNotifyLogService.updateByPrimaryKey(payNotifyLog);

		// 获取秘钥.
		String md5key = rechargeOrder.getSign();
		// 拼接加密字符串.
		String mD5Str = md5key + "create_time" + create_time + "currency_money" + currency_money + "currency_type" + currency_type
				+ "finish_time" + finish_time + "mchno" + mchno + "order_no" + order_no + "order_type" + order_type + "pay_amount"
				+ pay_amount + "pay_order_no" + pay_order_no + "price" + price + "remark" + remark + "status" + status + "timestamp"
				+ timestamp + "username" + username + "vcoin_code" + vcoin_code + "vcoin_count" + vcoin_count + md5key;

		log.info("当前MD5源码:" + mD5Str);
		// Md5加密小写
		String WaitSign = Md5Util.getMD5ofStr(mD5Str);
		log.info("加密后的MD5: " + WaitSign);

		// 从第三方返回的MD5加密签名串与后台Md5加密串进行校验
		if (WaitSign.compareTo(signature) == 0) {
			boolean dealResult = false;
			if (!StringUtils.isEmpty(status) && status.equals("2")) {
				// 支付金额元.
				BigDecimal factMoneyValue = new BigDecimal(currency_money);
				try {
					dealResult = rechargeOrderService.rechargeOrderSuccessDeal(order_no, factMoneyValue);
				} catch (UnEqualVersionException e) {
					log.error("光速支付支付充值时发现版本号不一致,订单号[" + order_no + "]");
					// 启动新进程进行多次重试
					UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
							UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, order_no, factMoneyValue, "");
					thread.start();
				} catch (Exception e) {
					// 异常不进行重试
					log.error("光速支付处理入账发生异常", e);
				}
			} else {
				if (!status.equals("2")) {
					log.info("光速支付处理错误支付结果status为：" + status);
				}
			}
			log.info("光速支付订单处理入账结果:{}", dealResult);
			// 通知第三方报文接收成功
			return returnJson.toString();
		} else {
			log.info("MD5校验失败,报文返回MD5签名内容[" + signature + "],计算MD5签名内容[" + WaitSign + "]");
			return returnJson.toString();
		}
	}
}
