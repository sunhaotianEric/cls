package com.team.lottery.controller.yifutong;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.returnModel.YiFuTongReturnParam;
import com.team.lottery.pay.util.Md5Util;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.ThirdPayConfig;

/**
 * 易付通入账处理.
 * 
 * @author jamine
 *
 */
@Controller
public class PayNotifyYifutongController extends BaseController {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;

	/**
	 * 易付通支付回调报文跳转地址
	 * 
	 * @return
	 */
	@RequestMapping(value = "/notify/yifutong", method = { RequestMethod.POST, RequestMethod.GET })
	public @ResponseBody String yifutong(HttpServletRequest request, HttpServletResponse response) {
		log.info("接收到易付通支付入账通知");
		
		//插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("YIFUTONG");
		PayNotifyLog payNotifyLog=construstNotifyLog();
		payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(),
				thirdPayConfig.getChargeDes(), "");
		
		//校验白名单
		boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
		if (!validateIp) {
			log.info("第三方支付访问IP异常!请联系客服!");
			return "ERROR";
		}
		YiFuTongReturnParam param = new YiFuTongReturnParam();
		param.setAmount(request.getParameter("amount"));
		param.setAttach(request.getParameter("attach"));
		param.setDatetime(request.getParameter("datetime"));
		param.setMemberid(request.getParameter("memberid"));
		param.setOrderid(request.getParameter("orderid"));
		param.setReturncode(request.getParameter("returncode"));
		param.setSign(request.getParameter("sign"));
		param.setTransaction_id(request.getParameter("transaction_id"));
		
		//组装通知报文内容
		String messageContent = "收到易付通支付通知报文如下: amount: " + request.getParameter("amount") + " attach: " + request.getParameter("attach")
				+ " datetime: " + request.getParameter("datetime") + " memberid: " + request.getParameter("memberid") + " orderid: "
				+ request.getParameter("orderid") + "transaction_id: " + request.getParameter("transaction_id") + " sign: "
				+ request.getParameter("sign");
		
		log.info("易付通支付成功通知平台处理信息如下: 商户ID: " + param.getMemberid() + " 商户订单号: " + param.getOrderid() + " 订单金额: " + param.getAmount()
				+ " 交易时间 " + param.getDatetime() + " MD5签名: " + param.getSign());
		

		RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(param.getOrderid());
		if (rechargeOrder == null) {
			log.error("根据订单号[" + param.getOrderid() + "]查找充值订单记录为空");
			return "OK";
		}
		
		//更新回调通知信息表
		payNotifyLog.setMessageContent(messageContent);
		payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
		payNotifyLog.setOrderId(rechargeOrder.getSerialNumber());
		payNotifyLogService.updateByPrimaryKey(payNotifyLog);

		// 签名
		String Md5key = rechargeOrder.getSign();
		String SignTemp = "amount=" + param.getAmount() + "&datetime=" + param.getDatetime() + "&memberid=" + param.getMemberid()
				+ "&orderid=" + param.getOrderid() + "&returncode=" + param.getReturncode() + "&transaction_id="
				+ param.getTransaction_id() + "&key=" + Md5key + "";
		// Md5加密串
		String WaitSign = Md5Util.getMD5ofStr(SignTemp).toUpperCase();
		log.info("易付通入账处理需要加密的字符串:[" + SignTemp + "]加密后的字符串为:[" + WaitSign + "]");

		// 从第三方返回的MD5加密签名串与后台Md5加密串进行校验
		if (WaitSign.compareTo(param.getSign()) == 0) {
			boolean dealResult = false;
			if (!StringUtils.isEmpty(param.getReturncode()) && param.getReturncode().equals("00")) {
				BigDecimal factMoneyValue = new BigDecimal(param.getAmount());
				try {
					dealResult = rechargeOrderService.rechargeOrderSuccessDeal(param.getOrderid(), factMoneyValue);
				} catch (UnEqualVersionException e) {
					log.error("易付通支付在线充值时发现版本号不一致,订单号[" + param.getOrderid() + "]");
					// 启动新进程进行多次重试
					UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
							UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, param.getOrderid(), factMoneyValue, "");
					thread.start();
				} catch (Exception e) {
					// 异常不进行重试
					log.error("易付通支付处理入账发生异常", e);
				}
			} else {
				if (!param.getReturncode().equals("00")) {
					log.info("易付通支付处理错误支付结果：" + param.getReturncode());
				}
			}
			log.info("易付通支付订单处理入账结果:{}", dealResult);
			// 通知易付通支付报文接收成功
			// out.print("OK");
			return "OK";
		} else {
			log.info("MD5校验失败,报文返回MD5签名内容[" + param.getSign() + "],计算MD5签名内容[" + WaitSign + "]");
			// 让第三方继续补发
			// out.print("error");
			return "ERROR";
		}

	}

}
