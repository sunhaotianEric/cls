package com.team.lottery.controller.xinduobaopay;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.model.xinduobaopay.XinDuoBaoPayReturn;
import com.team.lottery.pay.util.Md5Util;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.ThirdPayConfig;

/**
 * 
 * @Description: 鑫多宝支付入账处理
 * @Author: Jamine.
 * @CreateTime: 2019-05-14 11:09:31.
 */
@Controller
public class PayNotifyXinDuoBaoPayPayController extends BaseController {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;

	/**
	 * 鑫多宝支付回调处理地址.
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/notify/xinduobaopay", method = { RequestMethod.POST, RequestMethod.GET })
	public @ResponseBody String mumingpay(HttpServletRequest request, HttpServletResponse response) {
		log.info("接收到鑫多宝支付入账通知");
		//插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("XINDUOBAOPAY");
		PayNotifyLog payNotifyLog=construstNotifyLog();
		payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(),
				thirdPayConfig.getChargeDes(), "");
		
		//校验白名单
		boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
		if (!validateIp) {
			log.info("第三方支付访问IP异常!请联系客服!");
			return "ok";
		}
		// 订单号.
	  	String orderno = request.getParameter("orderno");
	 	// 商户号.
	  	String usercode = request.getParameter("usercode");
	 	// 商户订单号.
	  	String customno = request.getParameter("customno");
	 	// 支付类型.
	  	String type = request.getParameter("type");
	 	// 银行编号.
	  	String bankcode = request.getParameter("bankcode");
	 	// 提交支付金额.
	  	String tjmoney = request.getParameter("tjmoney");
	 	// 结算金额.
	  	String money = request.getParameter("money");
	 	// 充值订单状态.
	  	String status = request.getParameter("status");
	  	// 退款状态.
	  	String refundstatus = request.getParameter("refundstatus");
	  	// 币种.
	  	String currency = request.getParameter("currency");
	  	// 签名.
	  	String sign = request.getParameter("sign");
	  	// 返回码.
	  	String resultcode = request.getParameter("resultcode");
	  	// 返回信息.
	  	String resultmsg = request.getParameter("resultmsg");
	  	
	  	XinDuoBaoPayReturn xinDuoBaoPayReturn = new XinDuoBaoPayReturn();
	  	xinDuoBaoPayReturn.setBankcode(bankcode);
	  	xinDuoBaoPayReturn.setCurrency(currency);
	  	xinDuoBaoPayReturn.setCustomno(customno);
	  	xinDuoBaoPayReturn.setMoney(money);
	  	xinDuoBaoPayReturn.setOrderno(orderno);
	  	xinDuoBaoPayReturn.setRefundstatus(refundstatus);
	  	xinDuoBaoPayReturn.setResultcode(resultcode);
	  	xinDuoBaoPayReturn.setResultmsg(resultmsg);
	  	xinDuoBaoPayReturn.setSign(sign);
	  	xinDuoBaoPayReturn.setStatus(status);
	  	xinDuoBaoPayReturn.setTjmoney(tjmoney);
	  	xinDuoBaoPayReturn.setType(type);
	  	xinDuoBaoPayReturn.setUsercode(usercode);
		
		// 日志打印.
		log.info(xinDuoBaoPayReturn.toString());
		
		RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(customno);
		if (rechargeOrder == null) {
			log.error("根据订单号[" + customno + "]查找充值订单记录为空");
			return "ok";
		}
		
		//更新回调通知信息表
		payNotifyLog.setMessageContent(xinDuoBaoPayReturn.toString());
		payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
		payNotifyLog.setOrderId(rechargeOrder.getSerialNumber());
		payNotifyLogService.updateByPrimaryKey(payNotifyLog);

		//获取秘钥.
		String md5key = rechargeOrder.getSign();
		String MARK = "|";
		// 拼接加密字符串.
		String mD5Str = usercode + MARK + 
						orderno + MARK + 
						customno + MARK + 
						type + MARK + 
						bankcode + MARK + 
						tjmoney + MARK + 
						money + MARK + 
						status + MARK + 
						currency + MARK + 
						md5key;
		
		log.info("当前MD5源码:" + mD5Str);
		//Md5加密小写
		String WaitSign = Md5Util.getMD5ofStr(mD5Str);
		log.info("加密后的MD5: " + WaitSign);

		// 从第三方返回的MD5加密签名串与后台Md5加密串进行校验
		if (WaitSign.compareTo(sign) == 0) {
			boolean dealResult = false;
			if (!StringUtils.isEmpty(status) && status.equals("1")) {
				// 支付金额元.
				BigDecimal factMoneyValue = new BigDecimal(tjmoney);
				try {
					dealResult = rechargeOrderService.rechargeOrderSuccessDeal(customno, factMoneyValue);
				} catch (UnEqualVersionException e) {
					log.error("鑫多宝支付支付充值时发现版本号不一致,订单号[" + customno + "]");
					// 启动新进程进行多次重试
					UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
							UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, customno, factMoneyValue, "");
					thread.start();
				} catch (Exception e) {
					// 异常不进行重试
					log.error("第六支付处理入账发生异常", e);
				}
			} else {
				if (!status.equals("1")) {
					log.info("鑫多宝支付处理错误支付结果status为：" + status);
				}
			}
			log.info("鑫多宝支付订单处理入账结果:{}", dealResult);
			// 通知第三方报文接收成功
			return "ok";
		} else {
			log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容[" + WaitSign + "]");
			return "ok";
		}
	}

}
