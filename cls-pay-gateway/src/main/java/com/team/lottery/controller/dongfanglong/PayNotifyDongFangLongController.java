package com.team.lottery.controller.dongfanglong;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.pay.util.Md5Util;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.util.JSONUtils;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.ThirdPayConfig;

import net.sf.json.JSONObject;

/**
 * 东方龙支付入账处理
 * 
 * @author jamine
 *
 */
@Controller
public class PayNotifyDongFangLongController extends BaseController {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;
	
	/**
	 * 东方龙支付回调报文跳转地址
	 * 
	 * @return
	 */
	@RequestMapping(value = "/notify/dongfanglong", method = { RequestMethod.POST, RequestMethod.GET })
	public @ResponseBody String dongfanglong(HttpServletRequest request, HttpServletResponse response,@RequestParam(value="CLOUDID",required=false) String CLOUDID,
			@RequestParam(value="CLOUDCODESIGN",required=false) String CLOUDCODESIGN,@RequestParam(value="ACCOUNTINFO",required=false) String ACCOUNTINFO,
			@RequestParam(value="SIGN",required=false) String SIGN,@RequestParam(value="ADDACCOUNTMONEY",required=false) String ADDACCOUNTMONEY) {
		log.info("接收到东方龙支付入账通知");
		
		//插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("DONGFANGLONG");
		PayNotifyLog payNotifyLog=construstNotifyLog();
		payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(),
				thirdPayConfig.getChargeDes(), "");
		
		//校验白名单
		boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
		if (!validateIp) {
			log.info("第三方支付访问IP异常!请联系客服!");
			return "SUCCESS";
		}

		/*JSONObject jsonObject = null;
		try {
			BufferedReader streamReader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
			StringBuilder responseStrBuilder = new StringBuilder();
			String inputStr;
			while ((inputStr = streamReader.readLine()) != null) {
				responseStrBuilder.append(inputStr);
			}
			log.info("接收东方龙支付回调通知报文: " + responseStrBuilder.toString());
			jsonObject = JSONUtils.toJSONObject(responseStrBuilder.toString());
		} catch (Exception e) {
			log.error("接收东方龙支付回调通知出现异常");
		}*/
		
		
		/*String CLOUDID = request.getParameter("CLOUDID");
		String CLOUDCODESIGN = request.getParameter("CLOUDCODESIGN");
		String ACCOUNTINFO = request.getParameter("ACCOUNTINFO");
		String SIGN = request.getParameter("SIGN");
		String ADDACCOUNTMONEY = request.getParameter("ADDACCOUNTMONEY");*/
		
		//组装通知报文内容
		String messageContent = "东方龙支付,订单号=" + ACCOUNTINFO + "金额="
				+ ADDACCOUNTMONEY + "签名=" + SIGN;
		// 日志打印.
		log.info(messageContent);
		

		
		RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(ACCOUNTINFO);
		if (rechargeOrder == null) {
			log.error("根据订单号[" + ACCOUNTINFO + "]查找充值订单记录为空");
			return "success";
		}
		
		//更新回调通知信息表
		payNotifyLog.setMessageContent(messageContent);
		payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
		payNotifyLog.setOrderId(rechargeOrder.getSerialNumber());
		payNotifyLogService.updateByPrimaryKey(payNotifyLog);

		// 获取秘钥.
		String Md5key = rechargeOrder.getSign();
		String MARK = "&";
		/*// 拼接加密字符串.
		String mD5Str = "orderAmt=" + orderAmt + MARK + 
						"orderId=" + orderId + MARK + 
						"orderState=" + orderState + MARK + 
						"payTime=" + payTime + MARK + 
						"tranSeqId=" + tranSeqId + MARK + 
						"key=" + Md5key;*/
		
		String sign_str = ACCOUNTINFO + ADDACCOUNTMONEY + "m.odpay.net" +CLOUDID;
		String sign_1 = Md5Util.getMD5ofStr(sign_str);
		String sign_2 = Md5Util.getMD5ofStr(sign_1 + Md5key);
		String WaitSign = sign_2.substring(8, 24);

		//log.info("当前MD5源码:" + mD5Str);
		// Md5加密串并且大写
		//String WaitSign = Md5Util.getMD5ofStr(mD5Str);
		log.info("加密后的MD5: " + WaitSign);

		// 从第三方返回的MD5加密签名串与后台Md5加密串进行校验
		if (WaitSign.compareTo(SIGN) == 0) {
			boolean dealResult = false;
			/*if (!StringUtils.isEmpty(orderState) && orderState.equals("01")) {*/
				// 将金额从分转换为元.
				BigDecimal factMoneyValue = new BigDecimal(ADDACCOUNTMONEY);
				try {
					dealResult = rechargeOrderService.rechargeOrderSuccessDeal(ACCOUNTINFO, factMoneyValue);
				} catch (UnEqualVersionException e) {
					log.error("东方龙支付支付充值时发现版本号不一致,订单号[" + ACCOUNTINFO + "]");
					// 启动新进程进行多次重试
					UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
							UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, ACCOUNTINFO, factMoneyValue, "");
					thread.start();
				} catch (Exception e) {
					// 异常不进行重试
					log.error("东方龙支付处理入账发生异常", e);
				}
			/*} else {
				if (!orderState.equals("01")) {
					log.info("东方龙支付处理错误支付结果State为：" + orderState);
				}
			}*/
			log.info("东方龙支付订单处理入账结果:{}", dealResult);
			// 通知第三方报文接收成功
			return "success";
		} else {
			log.info("MD5校验失败,报文返回MD5签名内容[" + SIGN + "],计算MD5签名内容[" + WaitSign + "]");
			return "success";
		}
	}

}
