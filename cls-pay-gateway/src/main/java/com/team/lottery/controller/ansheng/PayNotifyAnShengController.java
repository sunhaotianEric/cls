package com.team.lottery.controller.ansheng;

import java.math.BigDecimal;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.pay.util.Base64Utils;
import com.team.lottery.service.ChargePayService;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.service.WithdrawAutoConfigService;
import com.team.lottery.service.WithdrawOrderService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.ChargePay;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.ThirdPayConfig;


/**
 * 安胜 支付入账处理.
 * 
 * @author jamine
 *
 */

@Controller
public class PayNotifyAnShengController extends BaseController {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;
	@Autowired
	private WithdrawOrderService withdrawOrderService;
	@Autowired
	private ChargePayService chargePayService;
	@Autowired
	private WithdrawAutoConfigService withdrawAutoConfigService;

	/**
	 * 安胜支付回调报文跳转地址
	 * 
	 * @return PayNotifyLog
	 * @throws Exception
	 * @throws UnEqualVersionException
	 */
	@RequestMapping(value = "/notify/ansheng", method = { RequestMethod.POST,
			RequestMethod.GET })
	public @ResponseBody String laku(HttpServletRequest request,
			HttpServletResponse response) throws UnEqualVersionException,
			Exception {
		log.info("接收到安胜支付入账通知");

		// 插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService
				.thirdPayConfigByChargeType("ANSHENG");
		PayNotifyLog payNotifyLog = construstNotifyLog();
		payNotifyLogService.addPayNotifyLog(payNotifyLog,
				thirdPayConfig.getChargeType(), thirdPayConfig.getChargeDes(),
				"");

		// 校验白名单
		boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
		if (!validateIp) {
			log.info("第三方支付访问IP异常!请联系客服!");
			return "error";
		}

		String code = request.getParameter("code");
		String msg = request.getParameter("msg");
		String totalamount = request.getParameter("data[totalamount]");
		String amount = request.getParameter("data[amount]");
		String status = request.getParameter("data[status]");
		String agentorderno = request.getParameter("data[agentorderno]");
		String orderno = request.getParameter("data[orderno]");
		String ordertime = request.getParameter("data[ordertime]");
		String sign = request.getParameter("sign");
		/*String risk = request.getParameter("data[risk]");*/
        
		// code为1并且status为1再进行验签

		String sBuilder="";
		// 字典序排序
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("amount", amount);
		map.put("totalamount", totalamount);
		map.put("status", status);
		map.put("agentorderno", agentorderno);
		map.put("orderno", orderno);
		map.put("ordertime", ordertime);
		// 先给data排序并组成k=v&结构
		/*sBuilder = HashMapUtils.sort(map);*/
		//String sBuilder = "";
		Collection<String> keyset= map.keySet();
		List list=new ArrayList<String>(keyset);
		Collections.sort(list);
		for(int i=0;i<list.size();i++){
			if(i == 0) {
				sBuilder += list.get(i)+"="+map.get(list.get(i)).toString();
			}
			else {
				sBuilder += "&"+list.get(i)+"="+map.get(list.get(i)).toString();
			}
		}
		map.clear();
		map.put("data", sBuilder);
		map.put("code", code);
		map.put("msg", request.getParameter("msg"));
		// 除sign外排序并组成k=v&结构
		 
		sBuilder="";
		keyset= map.keySet();
		list=new ArrayList<String>(keyset);
		Collections.sort(list);
		for(int i=0;i<list.size();i++){
			if(i == 0) {
				sBuilder += list.get(i)+"="+map.get(list.get(i)).toString();
			}
			else {
				sBuilder += "&"+list.get(i)+"="+map.get(list.get(i)).toString();
			}
		}		
		Long chargePayId = null;

		RechargeOrder rechargeOrder = rechargeOrderService
				.getRechargeOrderByOderNumber(agentorderno);
		if (rechargeOrder == null) {
			log.error("根据订单号[" + agentorderno + "]查找充值订单记录为空");
			return "SUCCESS";
		}

		chargePayId = rechargeOrder.getChargePayId();

		ChargePay chargePay = chargePayService.selectByPrimaryKey(chargePayId);
		String sysPubKey = "";
		if (chargePay != null) {
			sysPubKey = chargePay.getPublickey();

		}
		
		log.info("安胜支付成功通知平台处理信息如下:订单号:" + agentorderno + "订单金额:" + totalamount
				+ " 元,支付结果:" + status+" 订单信息:" + msg+" 订单返回码:" + code);

		// 组装通知报文内容
		String messageContent = "收到安胜 支付成功通知报文如下: 商户订单号: "
				+ agentorderno + " 订单结果: " + status + " 订单金额: " + totalamount
				+ " MD5签名: " + sign;

		// 更新回调通知信息表
		payNotifyLog.setMessageContent(messageContent);
		payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
		payNotifyLog.setOrderId(rechargeOrder.getSerialNumber());
		payNotifyLogService.updateByPrimaryKey(payNotifyLog);
		// 从第三方返回的MD5加密签名串与后台Md5加密串进行校验
		
		byte[] keyBytes = Base64Utils.decode(sysPubKey);
		X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		PublicKey publicK = keyFactory.generatePublic(keySpec);
		Signature signature = Signature.getInstance("SHA1withRSA");
		signature.initVerify(publicK);
		signature.update(sBuilder.getBytes("UTF-8"));
		Boolean verify = signature.verify(Base64Utils.decode(sign));
		log.info("签名验证结果=" + verify);
		if (verify) {
			boolean dealResult = false;
			if ((!StringUtils.isEmpty(status) && status.equals("1")) && (!StringUtils.isEmpty(code) && code.equals("1"))) {
				// 将金额从分转换为元.
				BigDecimal factMoneyValue = new BigDecimal(totalamount);
				try {
					dealResult = rechargeOrderService.rechargeOrderSuccessDeal(agentorderno, factMoneyValue);
				} catch (UnEqualVersionException e) {
					log.error("安胜支付支付充值时发现版本号不一致,订单号[" + agentorderno + "]");
					// 启动新进程进行多次重试
					UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
							UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, agentorderno, factMoneyValue, "");
					thread.start();
				} catch (Exception e) {
					// 异常不进行重试
					log.error("安胜支付处理入账发生异常", e);
				}
			} else {
				if (!status.equals("1")) {
					log.info("安胜支付处理错误支付结果status为：" + status);

				}
			}
			log.info("安胜支付订单处理入账结果:{}", dealResult);
			// 通知第三方报文接收成功
			return "SUCCESS";
		} else {
			log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容[" + signature.toString() + "]");
			return "FAILED";
		}

	}
}
