package com.team.lottery.controller.jindouyun;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.pay.model.jindouyun.JinDouYunPayReturn;
import com.team.lottery.pay.util.Md5Util;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.ThirdPayConfig;

/**
 * 筋斗云支付入账处理.
 * 
 * @author jamine
 *
 */
@Controller
public class PayNotifyJindouyunController extends BaseController {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;

	/**
	 * 筋斗云支付回调报文跳转地址
	 * 
	 * @return
	 */
	@RequestMapping(value = "/notify/jindouyunpay", method = { RequestMethod.POST, RequestMethod.GET })
	private @ResponseBody String jindouyunpay(HttpServletRequest request, HttpServletResponse response) {
		log.info("接收到筋斗云支付入账通知");
		
		//插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("JINDOUYUNPAY");
		PayNotifyLog payNotifyLog=construstNotifyLog();
		payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(),
				thirdPayConfig.getChargeDes(),"");
		
		//校验白名单
		boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
		if (!validateIp) {
			log.info("第三方支付访问IP异常!请联系客服!");
			return "ERROR";
		}
		
		// 新建筋斗云支付回调对象
		JinDouYunPayReturn jinDouYunPayReturn = new JinDouYunPayReturn();
		// 商户号.
		jinDouYunPayReturn.setMerchantCode(request.getParameter("merchantCode"));
		// 商户订单号.
		jinDouYunPayReturn.setMerchantOrderNo(request.getParameter("merchantOrderNo"));
		// 支付金额.
		jinDouYunPayReturn.setOrderAmount(request.getParameter("orderAmount"));
		// 完成时间.
		jinDouYunPayReturn.setOrderCompleteTime(request.getParameter("orderCompleteTime"));
		// 支付状态.
		jinDouYunPayReturn.setPayState(request.getParameter("payState"));
		// 平台订单号.
		jinDouYunPayReturn.setPlatOrderNo(request.getParameter("platOrderNo"));
		// 产品信息.
		jinDouYunPayReturn.setProductCode(request.getParameter("productCode"));
		// 相印码.
		jinDouYunPayReturn.setRetCode(request.getParameter("retCode"));
		// 响应信息.
		jinDouYunPayReturn.setRetMsg(request.getParameter("retMsg"));
		// 签名
		jinDouYunPayReturn.setSign(request.getParameter("sign"));
		
		////组装通知报文内容
		String messageContent = "收到筋斗云支付通知报文如下: merchantCode: " + request.getParameter("merchantCode") + " merchantOrderNo: "
				+ request.getParameter("merchantOrderNo") + " orderAmount: " + request.getParameter("orderAmount") + " orderCompleteTime: "
				+ request.getParameter("orderCompleteTime") + " payState: " + request.getParameter("payState") + " platOrderNo: "
				+ request.getParameter("platOrderNo") + " productCode: " + request.getParameter("productCode") + " retCode: "
				+ request.getParameter("retCode") + " retMsg: " + request.getParameter("retMsg") + " sign: " + request.getParameter("sign");
		log.info(jinDouYunPayReturn.toString());
		

		RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(jinDouYunPayReturn.getMerchantOrderNo());
		if (rechargeOrder == null) {
			log.error("根据订单号[" + jinDouYunPayReturn.getMerchantOrderNo() + "]查找充值订单记录为空");
			return "SUCCESS";
		}
		
		//更新回调通知信息表
		payNotifyLog.setMessageContent(messageContent);
		payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
		payNotifyLog.setOrderId(rechargeOrder.getSerialNumber());
		payNotifyLogService.updateByPrimaryKey(payNotifyLog);
		// 获取秘钥.
		String Md5key = rechargeOrder.getSign();
		String MARK = "&";
		// 拼接加密字符串.
		String mD5Str = "merchantCode=" + jinDouYunPayReturn.getMerchantCode() + MARK + "merchantOrderNo="
				+ jinDouYunPayReturn.getMerchantOrderNo() + MARK + "orderAmount=" + jinDouYunPayReturn.getOrderAmount() + MARK
				+ "orderCompleteTime=" + jinDouYunPayReturn.getOrderCompleteTime() + MARK + "payState=" + jinDouYunPayReturn.getPayState()
				+ MARK + "platOrderNo=" + jinDouYunPayReturn.getPlatOrderNo() + MARK + "productCode=" + jinDouYunPayReturn.getProductCode()
				+ MARK + "retCode=" + jinDouYunPayReturn.getRetCode() + MARK + "retMsg=" + jinDouYunPayReturn.getRetMsg() + MARK + "key="
				+ Md5key;

		log.info("当前MD5源码:" + mD5Str);
		// Md5加密串并且大写
		String WaitSign = Md5Util.getMD5ofStr(mD5Str).toUpperCase();
		log.info("加密后的MD5: " + WaitSign);

		// 从第三方返回的MD5加密签名串与后台Md5加密串进行校验
		if (WaitSign.compareTo(jinDouYunPayReturn.getSign()) == 0) {
			boolean dealResult = false;
			if (!StringUtils.isEmpty(jinDouYunPayReturn.getPayState()) && jinDouYunPayReturn.getPayState().equals("TRADE_SUCCESS")) {
				BigDecimal factMoneyValue = new BigDecimal(jinDouYunPayReturn.getOrderAmount());
				try {
					dealResult = rechargeOrderService.rechargeOrderSuccessDeal(jinDouYunPayReturn.getMerchantOrderNo(), factMoneyValue);
				} catch (UnEqualVersionException e) {
					log.error("筋斗云支付充值时发现版本号不一致,订单号[" + jinDouYunPayReturn.getMerchantOrderNo() + "]");
					// 启动新进程进行多次重试
					UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
							UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, jinDouYunPayReturn.getMerchantOrderNo(),
							factMoneyValue, "");
					thread.start();
				} catch (Exception e) {
					// 异常不进行重试
					log.error("筋斗云支付处理入账发生异常", e);
				}
			} else {
				if (!jinDouYunPayReturn.getPayState().equals("TRADE_SUCCESS")) {
					log.info("筋斗云支付处理错误支付结果payState为：" + jinDouYunPayReturn.getPayState());
				}
			}
			log.info("筋斗云支付订单处理入账结果:{}", dealResult);
			// 通知凡客付报文接收成功
			// out.println("SUCCESS");
			return "SUCCESS";
		} else {
			log.info("MD5校验失败,报文返回MD5签名内容[" + jinDouYunPayReturn.getSign() + "],计算MD5签名内容[" + WaitSign + "]");
			// out.println("SUCCESS");
			return "ERROR";
		}

	}
}
