package com.team.lottery.controller.haitupay;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.model.haitupay.HaiTuPayReturn;
import com.team.lottery.pay.util.Md5Util;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.ThirdPayConfig;

/**
 * 海图支付入账处理
 * @Description: 
 * @Author: Jamine.
 * @CreateTime: 2019-05-25 06:23:51.
 */
@Controller
public class PayNotifyHaiTuPayController extends BaseController {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;

	/**
	 * 海图支付入账处理
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/notify/haitupay", method = { RequestMethod.POST, RequestMethod.GET })
	public @ResponseBody String mumingpay(HttpServletRequest request, HttpServletResponse response) {
		log.info("接收到海图支付入账通知");
		
		//插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("HAITUPAY");
		PayNotifyLog payNotifyLog=construstNotifyLog();
		payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(),
				thirdPayConfig.getChargeDes(), "");
		
		//校验白名单
		boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
		if (!validateIp) {
			log.info("第三方支付访问IP异常!请联系客服!");
			return "success";
		}

		// 获取参数
		String mch_id = request.getParameter("mch_id");
		String nonce_str = request.getParameter("nonce_str");
		String out_trade_no = request.getParameter("out_trade_no");
		String body = request.getParameter("body");
		String total_fee = request.getParameter("total_fee");
		String charset = request.getParameter("charset");
		String notify_time = request.getParameter("notify_time");
		String transaction_id = request.getParameter("transaction_id");
		String third_transaction_id = request.getParameter("third_transaction_id");
		String sign = request.getParameter("sign");
		
		// 参数封装.
		HaiTuPayReturn haiTuPayReturn = new HaiTuPayReturn();
		haiTuPayReturn.setBody(body);
		haiTuPayReturn.setCharset(charset);
		haiTuPayReturn.setMch_id(mch_id);
		haiTuPayReturn.setNonce_str(nonce_str);
		haiTuPayReturn.setNotify_time(notify_time);
		haiTuPayReturn.setOut_trade_no(out_trade_no);
		haiTuPayReturn.setSign(sign);
		haiTuPayReturn.setThird_transaction_id(third_transaction_id);
		haiTuPayReturn.setTotal_fee(total_fee);
		haiTuPayReturn.setTransaction_id(transaction_id);

		// 日志打印.
		log.info(haiTuPayReturn.toString());
		

		
		RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(out_trade_no);
		if (rechargeOrder == null) {
			log.error("根据订单号[" + out_trade_no + "]查找充值订单记录为空");
			return "success";
		}
		
		//更新回调通知信息表
		payNotifyLog.setMessageContent(haiTuPayReturn.toString());
		payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
		payNotifyLog.setOrderId(rechargeOrder.getSerialNumber());
		payNotifyLogService.updateByPrimaryKey(payNotifyLog);

		// 获取秘钥.
		String Md5key = rechargeOrder.getSign();
		String MARK = "&";
		// 拼接加密字符串.
		String mD5Str = "body=" + body + MARK + 
						"charset=" + charset + MARK +
						"mch_id=" + mch_id + MARK +
						"nonce_str=" + nonce_str + MARK +
						"notify_time=" + notify_time + MARK +
						"out_trade_no=" + out_trade_no + MARK +
						"third_transaction_id=" + third_transaction_id + MARK +
						"total_fee=" + total_fee + MARK +
						"transaction_id=" + transaction_id + MARK +
						"key=" + Md5key;

		
		// Md5加密串并且大写
		String WaitSign = Md5Util.getMD5ofStr(mD5Str).toUpperCase();
		log.info("当前MD5源码:" + mD5Str + "加密后的MD5: " + WaitSign);

		// 从第三方返回的MD5加密签名串与后台Md5加密串进行校验
		if (WaitSign.compareTo(sign) == 0) {
			boolean dealResult = false;
			// 将金额从分转换为元.
			BigDecimal factMoneyValue = new BigDecimal(Integer.parseInt(total_fee)).divide(new BigDecimal(100));
			try {
				dealResult = rechargeOrderService.rechargeOrderSuccessDeal(out_trade_no, factMoneyValue);
			} catch (UnEqualVersionException e) {
				log.error("海图支付支付充值时发现版本号不一致,订单号[" + out_trade_no + "]");
				// 启动新进程进行多次重试
				UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
						UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, out_trade_no, factMoneyValue, "");
				thread.start();
			} catch (Exception e) {
				// 异常不进行重试
				log.error("海图支付处理入账发生异常", e);
			}

			log.info("海图支付订单处理入账结果:{}", dealResult);
			// 通知第三方报文接收成功
			return "success";
		} else {
			log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容[" + WaitSign + "]");
			return "success";
		}
	}

}
