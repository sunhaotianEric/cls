package com.team.lottery.controller.muming;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.pay.model.muming.MuMingPayReturn;
import com.team.lottery.pay.util.Md5Util;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.ThirdPayConfig;

/**
 * 慕名支付入账处理.
 * 
 * @author jamine
 *
 */
@Controller
public class PayNotifyMumingController extends BaseController {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;

	/**
	 * 慕名支付回调报文跳转地址
	 * 
	 * @return
	 */
	@RequestMapping(value = "/notify/mumingpay", method = { RequestMethod.POST, RequestMethod.GET })
	public @ResponseBody String mumingpay(HttpServletRequest request, HttpServletResponse response) {
		log.info("接收到慕名支付入账通知");
		
		//插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("MUMINGPAY");
		PayNotifyLog payNotifyLog=construstNotifyLog();
		payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(),
				thirdPayConfig.getChargeDes(), "");
		
		//校验白名单
		boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
		if (!validateIp) {
			log.info("第三方支付访问IP异常!请联系客服!");
			return "error";
		}
		
		// 新建慕名回调对象.
		MuMingPayReturn muMingPayReturn = new MuMingPayReturn();
		muMingPayReturn.setAmount(request.getParameter("amount"));
		muMingPayReturn.setAttach(request.getParameter("attach"));
		muMingPayReturn.setDatetime(request.getParameter("datetime"));
		muMingPayReturn.setMemberid(request.getParameter("memberid"));
		muMingPayReturn.setOrderid(request.getParameter("orderid"));
		muMingPayReturn.setReturncode(request.getParameter("returncode"));
		muMingPayReturn.setSign(request.getParameter("sign"));
		muMingPayReturn.setTransaction_id(request.getParameter("transaction_id"));
		
		//组装通知报文内容
		String messageContent = "收到慕名支付通知报文如下: merchant_no: " + request.getParameter("merchant_no") + " merchant_order_no: "
				+ request.getParameter("merchant_order_no") + " payment_time: " + request.getParameter("payment_time") + " status: "
				+ request.getParameter("status") + " trade_amount: " + request.getParameter("trade_amount") + " sign: "
				+ request.getParameter("sign");
		
		

		RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(muMingPayReturn.getOrderid());
		if (rechargeOrder == null) {
			log.error("根据订单号[" + muMingPayReturn.getOrderid() + "]查找充值订单记录为空");
			return "success";
		}
		
		//更新回调通知信息表
		payNotifyLog.setMessageContent(messageContent);
		payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
		payNotifyLog.setOrderId(rechargeOrder.getSerialNumber());
		payNotifyLogService.updateByPrimaryKey(payNotifyLog);

		// 获取秘钥.
		String Md5key = rechargeOrder.getSign();
		String MARK = "&";
		// 拼接加密字符串.
		String mD5Str = "amount=" + muMingPayReturn.getAmount() + MARK +
				"datetime=" + muMingPayReturn.getDatetime() + MARK +
				"memberid=" + muMingPayReturn.getMemberid() + MARK + 
				"orderid=" + muMingPayReturn.getOrderid() + MARK + 
				"returncode=" + muMingPayReturn.getReturncode() + MARK + 
				"transaction_id=" + muMingPayReturn.getTransaction_id() + MARK + 
				"key=" + Md5key;

		log.info("当前MD5源码:" + mD5Str);
		// Md5加密串并且大写
		String WaitSign = Md5Util.getMD5ofStr(mD5Str);
		log.info("加密后的MD5: " + WaitSign);

		// 从第三方返回的MD5加密签名串与后台Md5加密串进行校验
		if (WaitSign.compareTo(muMingPayReturn.getSign()) == 0) {
			boolean dealResult = false;
			if (!StringUtils.isEmpty(muMingPayReturn.getReturncode()) && muMingPayReturn.getReturncode().equals("00")) {
				BigDecimal factMoneyValue = new BigDecimal(muMingPayReturn.getAmount());
				try {
					dealResult = rechargeOrderService.rechargeOrderSuccessDeal(muMingPayReturn.getOrderid(), factMoneyValue);
				} catch (UnEqualVersionException e) {
					log.error("慕名支付充值时发现版本号不一致,订单号[" + muMingPayReturn.getOrderid() + "]");
					// 启动新进程进行多次重试
					UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
							UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, muMingPayReturn.getOrderid(),
							factMoneyValue, "");
					thread.start();
				} catch (Exception e) {
					// 异常不进行重试
					log.error("慕名支付处理入账发生异常", e);
				}
			} else {
				if (!muMingPayReturn.getReturncode().equals("00")) {
					log.info("慕名支付处理错误支付结果payState为：" + muMingPayReturn.getReturncode());
				}
			}
			log.info("慕名支付订单处理入账结果:{}", dealResult);
			// 通知凡客付报文接收成功
			// out.println("success");
			return "success";
		} else {
			log.info("MD5校验失败,报文返回MD5签名内容[" + muMingPayReturn.getSign() + "],计算MD5签名内容[" + WaitSign + "]");
			// out.println("success");
			return "error";
		}
	}

}
