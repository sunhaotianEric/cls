package com.team.lottery.controller;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.team.lottery.service.ChargePayService;
import com.team.lottery.service.PayNotifyLogService;
import com.team.lottery.service.RechargeOrderService;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.ThirdPayConfig;

/**
 * pay-gateway项目中所有controller的父级项目.
 * 
 * @author jamine
 *
 */
public class BaseController extends ApplicationObjectSupport{
	protected static Logger log = LoggerFactory.getLogger(BaseController.class);
	@Autowired
	protected RechargeOrderService rechargeOrderService;
	@Autowired
	protected PayNotifyLogService payNotifyLogService;
	@Autowired
	protected ChargePayService chargePayService;

	/**
	 * 获取Request对象
	 * @return
	 */
	public HttpServletRequest getRequest() {
		return ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
	}
	
	/**
	 * 获取Response对象
	 * @return
	 */
	public HttpServletResponse getResponse() {
		return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
	}
	
	/**
	 * 获取Session对象
	 * @return
	 */
	public HttpSession getSession() {
		return this.getRequest().getSession();
	}

	/**
	 * 获取用户的客户端请求IP
	 * 
	 * @return
	 */
	protected String getRequestIp() {
		String ip = this.getRequest().getHeader("x-forwarded-for");
		if(ip != null && ip.length() > 0 && "unknown".equalsIgnoreCase(ip)) {
			//多次反向代理后会有多个ip值，
			//防止伪造ip处理  取最左边的ip地址 
			int index = ip.indexOf(",");
            if(index != -1){
            	log.info("x-forwarded-for发现多个ip地址，当前ip[{}]", ip);
            	String[] ipArrays = ip.split(",");
            	if(ipArrays.length > 0) {
            		ip = ipArrays[ipArrays.length - 1].trim();
            	}
            }
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = this.getRequest().getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = this.getRequest().getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = this.getRequest().getRemoteAddr();
		}
		//处理特殊字符
		if(ip != null && ip.length() > 0 && ip.indexOf("::ffff:") != -1) {
			log.info("当前ip含有特殊字符[{}]", ip);
	        ip = ip.substring(7);
		}
		return ip;
	}

	/**
	 * 验证第三方支付是否在ip白名单里面
	 * 
	 * @return boolean
	 */
	public boolean validateRequestIp(ThirdPayConfig thirdPayConfig, String requestIp) {
		boolean result = false;
		if (thirdPayConfig != null) {
			// 验证IP开启是否开启
			if (thirdPayConfig.getAuthorizedIpEnabled() == 1) {
				// 验证IP根据,号来分割成字符串数组
				String[] authorizedIp = thirdPayConfig.getAuthorizedIp().split(",");
				for (String str : authorizedIp) {
					if (requestIp.equals(str)) {
						result = true;
						break;
					}
				}
			} else {
				result = true;
			}
		}
		return result;
	}

	/**
	 * 把request相关信息保存到数据库 param payNotifyLog
	 * 
	 * @return
	 */
	public PayNotifyLog construstNotifyLog() {
		
		PayNotifyLog payNotifyLog = new PayNotifyLog();
		// 保存远程请求URL
		if (this.getRequest().getRequestURL() != null) {
			payNotifyLog.setRequestUrl(String.valueOf(this.getRequest().getRequestURL()));
		}
		// 保存远程请求方法
		if (this.getRequest().getMethod() != null) {
			payNotifyLog.setRequestMethod(this.getRequest().getMethod());
		}
		// 保存远程请求数据类型
		if (this.getRequest().getHeader("Content-Type") != null) {
			payNotifyLog.setContentType(this.getRequest().getHeader("Content-Type"));
		}
		// 保存远程请求数据的长度
		if (this.getRequest().getHeader("Content-Length") != null) {
			payNotifyLog.setContentLength(Integer.parseInt(this.getRequest().getHeader("Content-Length")));
		}

		// 保存http request相关头部请求参数
		Map<String, String> map = new HashMap<String, String>();
		Enumeration<?> enum1 = this.getRequest().getHeaderNames();
		while (enum1.hasMoreElements()) {
			String key = (String) enum1.nextElement();
			String value = this.getRequest().getHeader(key);
			map.put(key, value);
		}

		payNotifyLog.setHeaderNames(map.toString());
		map.clear();

		// 保存http request相关请求参数
		Enumeration paramNames = this.getRequest().getParameterNames();
		while (paramNames.hasMoreElements()) {
			String paramName = (String) paramNames.nextElement();

			String[] paramValues = this.getRequest().getParameterValues(paramName);
			if (paramValues.length > 0) {
				String paramValue = paramValues[0];
				if (paramValue.length() != 0) {
					map.put(paramName, paramValue);
				}
			}
		}
		payNotifyLog.setParameterNames(map.toString());
		payNotifyLog.setRequestIp(getRequestIp());
		return payNotifyLog;
	}
}
