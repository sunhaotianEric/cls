package com.team.lottery.controller.fenglipay;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.returnModel.FengLiPayReturnParam;
import com.team.lottery.pay.util.Md5Util;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.ThirdPayConfig;

/**
 * 凤梨支付入账处理.
 * 
 * @author jamine
 *
 */

@Controller
public class PayNotifyFengLiController extends BaseController {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;

	/**
	 * 凤梨支付回调报文跳转地址
	 * 
	 * @return PayNotifyLog
	 */
	@RequestMapping(value = "/notify/fenglipay", method = { RequestMethod.POST, RequestMethod.GET })
	public @ResponseBody String feilong(HttpServletRequest request, HttpServletResponse response) {
		log.info("接收到凤梨支付入账通知");
		
		//插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("FENGLIPAY");
		PayNotifyLog payNotifyLog=construstNotifyLog();
		payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(),
				thirdPayConfig.getChargeDes(),"");
		
		//校验白名单
		boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
		if (!validateIp) {
			log.info("第三方支付访问IP异常!请联系客服!");
			return "error";
		}
		
		FengLiPayReturnParam param = new FengLiPayReturnParam();
		param.setCharset(request.getParameter("charset"));
		param.setCustid(request.getParameter("custid"));
		param.setOut_trade_no(request.getParameter("out_trade_no"));
		param.setSign(request.getParameter("sign"));
		param.setSign_type(request.getParameter("sign_type"));
		param.setStatus(request.getParameter("status"));
		param.setTime_end(request.getParameter("time_end"));
		param.setTotal_fee(Float.valueOf(request.getParameter("total_fee")));
		param.setTransaction_id(request.getParameter("transaction_id"));
		log.info("凤梨支付成功通知平台处理信息如下: 商户ID: " + param.getCustid() + " 商户订单号: " + param.getOut_trade_no() + " 订单结果: " + param.getStatus()
				+ " 订单金额: " + param.getTotal_fee() + " 凤梨商务订单号: " + param.getTransaction_id() + " MD5签名: " + param.getSign());

		

		RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(param.getOut_trade_no());
		if (rechargeOrder == null) {
			log.error("根据订单号[" + param.getOut_trade_no() + "]查找充值订单记录为空");
			return "success";
		}
		
		//组装通知报文内容
		String messageContent = "收到凤梨支付成功通知报文如下: 商户ID: " + param.getCustid() + " 商户订单号: " + param.getOut_trade_no() + " 订单结果: "
				+ param.getStatus() + " 订单金额: " + param.getTotal_fee() + " 凤梨商务订单号: " + param.getTransaction_id() + " MD5签名: "
				+ param.getSign();
		//更新回调通知信息表
		payNotifyLog.setMessageContent(messageContent);
		payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
		payNotifyLog.setOrderId(rechargeOrder.getSerialNumber());
		payNotifyLogService.updateByPrimaryKey(payNotifyLog);

		// 签名
		String Md5key = rechargeOrder.getSign();
		String md5 = new String(param.getCustid() + param.getOut_trade_no() + param.getTime_end() + rechargeOrder.getSign());
		// Md5加密串
		String WaitSign = Md5Util.getMD5ofStr(md5);

		// 从第三方返回的MD5加密签名串与后台Md5加密串进行校验
		if (WaitSign.compareTo(param.getSign()) == 0) {
			boolean dealResult = false;
			if (!StringUtils.isEmpty(param.getStatus()) && param.getStatus().equals("0")) {
				BigDecimal factMoneyValue = new BigDecimal(param.getTotal_fee()).divide(new BigDecimal("100"));
				try {
					dealResult = rechargeOrderService.rechargeOrderSuccessDeal(param.getOut_trade_no(), factMoneyValue);
				} catch (UnEqualVersionException e) {
					log.error("凤梨支付在线充值时发现版本号不一致,订单号[" + param.getOut_trade_no() + "]");
					// 启动新进程进行多次重试
					UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
							UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, param.getOut_trade_no(), factMoneyValue, "");
					thread.start();
				} catch (Exception e) {
					// 异常不进行重试
					log.error("凤梨支付处理入账发生异常", e);
				}
			} else {
				if (!param.getStatus().equals("0")) {
					log.info("凤梨支付处理错误支付结果：" + param.getStatus());
				}
			}
			log.info("凤梨支付订单处理入账结果:{}", dealResult);
			return "success";
		} else {
			log.info("MD5校验失败,报文返回MD5签名内容[" + param.getSign() + "],计算MD5签名内容[" + WaitSign + "]");
			return "Fail";
		}
	}
}
