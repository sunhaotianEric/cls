package com.team.lottery.controller.shande;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.pay.util.shandeutil.CertUtil;
import com.team.lottery.pay.util.shandeutil.CryptoUtil;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.ThirdPayConfig;

import net.sf.json.JSONObject;

/**
 * 山德支付入账处理.
 * 
 * @author jamine
 *
 */
@Controller
public class PayNotifyShandeController extends BaseController {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;

	/**
	 * 杉德支付回调报文跳转地址
	 * 
	 * @return
	 */
	@RequestMapping(value = "/notify/shandepay", method = { RequestMethod.POST, RequestMethod.GET })
	public @ResponseBody String shandepay(HttpServletRequest request, HttpServletResponse response) {
		log.info("接收到衫德支付入账通知");
		
		//插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("SHANDEPAY");
		PayNotifyLog payNotifyLog=construstNotifyLog();
		payNotifyLogService.addPayNotifyLog(payNotifyLog, thirdPayConfig.getChargeType(),
				thirdPayConfig.getChargeDes(), "");
		
		//校验白名单
		boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
		if (!validateIp) {
			log.info("第三方支付访问IP异常!请联系客服!");
			return "respCode=999999";
		}
		String data = request.getParameter("data");// 数据.
		String sign = request.getParameter("sign");
		JSONObject jsonParam = new JSONObject();
		jsonParam.put("data", data);
		jsonParam.put("sign", sign);
		
		//组装通知报文内容
		String messageContent = "收到杉德支付通知报文如下: data: " + request.getParameter("data") + " sign: " + request.getParameter("sign");
		log.info("杉德返回的data为:[" + data + "]Sign为[" + sign + "]");
		

		if (StringUtils.isEmpty(data) && StringUtils.isEmpty(sign)) {
			/* out.print("respCode=000000"); */
			return "respCode=000000";
		}
		boolean valid;
		try {
			valid = CryptoUtil.verifyDigitalSign(data.getBytes("utf-8"), Base64.decodeBase64(sign), CertUtil.getPublicKey(), "SHA1WithRSA");
			if (!valid) {
				log.error("验证杉德签名验证失败...");
				log.error("签名字符串(data)为:" + data);
				log.error("签名值(sign)为:" + sign);
			} else {
				log.info("验证杉德签名验证成功...");
				JSONObject dataJson = JSONObject.fromObject(data);
				boolean dealResult = false;
				if (dataJson != null) {
					log.info("通知业务数据为:" + dataJson.toString());
					// 获取Head内容.获取Body内容.
					String jsonObjecHead = dataJson.getString("head");
					String jsonObjecBody = dataJson.getString("body");

					// 转换为JsonObject对象
					JSONObject headJson = JSONObject.fromObject(jsonObjecHead);
					JSONObject bodyJson = JSONObject.fromObject(jsonObjecBody);
					if (jsonObjecHead != null && jsonObjecBody != null) {
						log.info("杉德通知获取到的head为" + headJson.toString());
						log.info("杉德通知获取到的body为" + bodyJson.toString());
						// 判断返回的信息.
						String respCode = headJson.getString("respCode");
						if (respCode.equals("000000")) {
							// 订单号
							String orderCode = bodyJson.getString("orderCode");
							// 订单金额,以分为单位的12位长度的数字.
							String totalAmount = bodyJson.getString("totalAmount");
							log.info("杉德支付成功通知平台处理信息如下订单号: " + orderCode + " 支付结果: " + respCode + "支付金额" + totalAmount + "分");

							RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(orderCode);
							if (rechargeOrder == null) {
								log.error("根据订单号[" + orderCode + "]查找充值订单记录为空");
								return "respCode=000000";
							}
							
							//更新回调通知信息表
							payNotifyLog.setMessageContent(messageContent);
							payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
							payNotifyLog.setOrderId(rechargeOrder.getSerialNumber());
							payNotifyLogService.updateByPrimaryKey(payNotifyLog);
							// 以分为单位
							BigDecimal factMoneyValue = new BigDecimal(Integer.parseInt(totalAmount)).divide(new BigDecimal(100));
							try {
								dealResult = rechargeOrderService.rechargeOrderSuccessDeal(orderCode, factMoneyValue);
							} catch (UnEqualVersionException e) {
								log.error("杉德支付充值时发现版本号不一致,订单号[" + orderCode + "]");
								// 启动新进程进行多次重试
								UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
										UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, orderCode, factMoneyValue, "");
								thread.start();
							} catch (Exception e) {
								// 异常不进行重试
								log.error(e.getMessage());
							}

						}

					}
				} else {
					log.error("通知数据异常!!!");
				}
				log.info("杉德支付付订单处理入账结果:{}", dealResult);
			}

		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return "respCode=999999";

	}

	/**
	 * 杉德支付业务处理
	 * 
	 * @return boolean
	 */
	private boolean shandepayService(JSONObject jsonParam, PayNotifyLog payNotifyLog) {
		String data = jsonParam.getString("data");
		String sign = jsonParam.getString("sign");
		boolean result = false;
		log.info("杉德返回的data为:[" + data + "]Sign为[" + sign + "]");
		if (StringUtils.isEmpty(data) && StringUtils.isEmpty(sign)) {
			/* out.print("respCode=000000"); */
			result = true;
		}
		boolean valid;
		try {
			valid = CryptoUtil.verifyDigitalSign(data.getBytes("utf-8"), Base64.decodeBase64(sign), CertUtil.getPublicKey(), "SHA1WithRSA");
			if (!valid) {
				log.error("验证杉德签名验证失败...");
				log.error("签名字符串(data)为:" + data);
				log.error("签名值(sign)为:" + sign);
			} else {
				log.info("验证杉德签名验证成功...");
				JSONObject dataJson = JSONObject.fromObject(data);
				boolean dealResult = false;
				if (dataJson != null) {
					log.info("通知业务数据为:" + dataJson.toString());
					// 获取Head内容.获取Body内容.
					String jsonObjecHead = dataJson.getString("head");
					String jsonObjecBody = dataJson.getString("body");

					// 转换为JsonObject对象
					JSONObject headJson = JSONObject.fromObject(jsonObjecHead);
					JSONObject bodyJson = JSONObject.fromObject(jsonObjecBody);
					if (jsonObjecHead != null && jsonObjecBody != null) {
						log.info("杉德通知获取到的head为" + headJson.toString());
						log.info("杉德通知获取到的body为" + bodyJson.toString());
						// 判断返回的信息.
						String respCode = headJson.getString("respCode");
						if (respCode.equals("000000")) {
							// 订单号
							String orderCode = bodyJson.getString("orderCode");
							// 订单金额,以分为单位的12位长度的数字.
							String totalAmount = bodyJson.getString("totalAmount");
							log.info("杉德支付成功通知平台处理信息如下订单号: " + orderCode + " 支付结果: " + respCode + "支付金额" + totalAmount + "分");

							RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(orderCode);
							if (rechargeOrder == null) {
								log.error("根据订单号[" + orderCode + "]查找充值订单记录为空");
								return true;
							}
							payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
							payNotifyLogService.updateByPrimaryKey(payNotifyLog);
							// 以分为单位
							BigDecimal factMoneyValue = new BigDecimal(Integer.parseInt(totalAmount)).divide(new BigDecimal(100));
							try {
								dealResult = rechargeOrderService.rechargeOrderSuccessDeal(orderCode, factMoneyValue);
							} catch (UnEqualVersionException e) {
								log.error("杉德支付充值时发现版本号不一致,订单号[" + orderCode + "]");
								// 启动新进程进行多次重试
								UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
										UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, orderCode, factMoneyValue, "");
								thread.start();
							} catch (Exception e) {
								// 异常不进行重试
								log.error("衫德支付处理入账发生异常", e);
							}

						}

					}
				} else {
					log.error("通知数据异常!!!");
				}
				log.info("杉德支付付订单处理入账结果:{}", dealResult);
			}

		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return result;
	}
}
