package com.team.lottery.controller.xunyin;

import java.math.BigDecimal;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.team.lottery.controller.BaseController;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.pay.util.Base64Utils;
import com.team.lottery.pay.util.Md5Util;
import com.team.lottery.service.ChargePayService;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.service.WithdrawAutoConfigService;
import com.team.lottery.service.WithdrawOrderService;
import com.team.lottery.system.UserVersionExceptionPayDealThread;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.ChargePay;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.ThirdPayConfig;


/**
 * 讯银 支付入账处理.
 * 
 * @author jamine
 *
 */

@Controller
public class PayNotifyXunYinController extends BaseController {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;
	@Autowired
	private WithdrawOrderService withdrawOrderService;
	@Autowired
	private ChargePayService chargePayService;
	@Autowired
	private WithdrawAutoConfigService withdrawAutoConfigService;

	/**
	 * 讯银支付回调报文跳转地址
	 * 
	 * @return PayNotifyLog
	 * @throws Exception
	 * @throws UnEqualVersionException
	 */
	@RequestMapping(value = "/notify/xunyin", method = { RequestMethod.POST,
			RequestMethod.GET })
	public @ResponseBody String laku(HttpServletRequest request,
			HttpServletResponse response) throws UnEqualVersionException,
			Exception {
		log.info("接收到讯银支付入账通知");

		// 插入回调通知消息表
		ThirdPayConfig thirdPayConfig = thirdPayConfigService
				.thirdPayConfigByChargeType("XUNYIN");
		PayNotifyLog payNotifyLog = construstNotifyLog();
		payNotifyLogService.addPayNotifyLog(payNotifyLog,
				thirdPayConfig.getChargeType(), thirdPayConfig.getChargeDes(),
				"");

		// 校验白名单
		boolean validateIp = validateRequestIp(thirdPayConfig, getRequestIp());
		if (!validateIp) {
			log.info("第三方支付访问IP异常!请联系客服!");
			return "error";
		}

		String memberid = request.getParameter("memberid");
		String orderid = request.getParameter("orderid");
		String amount = request.getParameter("amount");
		String transaction_id = request.getParameter("transaction_id");
		String datetime = request.getParameter("datetime");
		String returncode = request.getParameter("returncode");
		String attach = request.getParameter("attach");
		String sign = request.getParameter("sign");
		/*String risk = request.getParameter("data[risk]");*/
        
		// code为1并且status为1再进行验签

		String sBuilder="";
		// 字典序排序
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("memberid", memberid);
		map.put("orderid", orderid);
		map.put("amount", amount);
		map.put("transaction_id", transaction_id);
		map.put("datetime", datetime);
		map.put("returncode", returncode);
		// 先给data排序并组成k=v&结构
		/*sBuilder = HashMapUtils.sort(map);*/
		//String sBuilder = "";
		Collection<String> keyset= map.keySet();
		List list=new ArrayList<String>(keyset);
		Collections.sort(list);
		for(int i=0;i<list.size();i++){
			if(i == 0) {
				sBuilder += list.get(i)+"="+map.get(list.get(i)).toString();
			}
			else {
				sBuilder += "&"+list.get(i)+"="+map.get(list.get(i)).toString();
			}
		}

		RechargeOrder rechargeOrder = rechargeOrderService
				.getRechargeOrderByOderNumber(orderid);
		if (rechargeOrder == null) {
			log.error("根据订单号[" + orderid + "]查找充值订单记录为空");
			return "OK";
		}
		
		log.info("讯银支付成功通知平台处理信息如下:订单号:" + orderid + "订单金额:" + amount
				+ " 元,支付结果:" + returncode+" 交易时间:" + datetime+" 交易流水号:" + transaction_id);

		// 组装通知报文内容
		String messageContent = "收到讯银 支付成功通知报文如下: 商户订单号: "
				+ orderid + " 订单结果: " + returncode + " 订单金额: " + amount
				+ " MD5签名: " + sign;

		// 更新回调通知信息表
		payNotifyLog.setMessageContent(messageContent);
		payNotifyLog.setBizSystem(rechargeOrder.getBizSystem());
		payNotifyLog.setOrderId(rechargeOrder.getSerialNumber());
		payNotifyLogService.updateByPrimaryKey(payNotifyLog);
		// 从第三方返回的MD5加密签名串与后台Md5加密串进行校验
		
		String md5key = rechargeOrder.getSign();
		log.info("当前MD5源码:" + sBuilder);
		//Md5加密小写
		String WaitSign = Md5Util.getMD5ofStr(sBuilder+"&key="+md5key).toUpperCase();
		log.info("加密后的MD5: " + WaitSign);
		if (WaitSign.compareTo(sign) == 0) {
			boolean dealResult = false;
			if ((!StringUtils.isEmpty(returncode) && returncode.equals("00"))) {
				// 将金额从分转换为元.
				BigDecimal factMoneyValue = new BigDecimal(amount);
				try {
					dealResult = rechargeOrderService.rechargeOrderSuccessDeal(orderid, factMoneyValue);
				} catch (UnEqualVersionException e) {
					log.error("讯银支付支付充值时发现版本号不一致,订单号[" + orderid + "]");
					// 启动新进程进行多次重试
					UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
							UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, orderid, factMoneyValue, "");
					thread.start();
				} catch (Exception e) {
					// 异常不进行重试
					log.error("讯银支付处理入账发生异常", e);
				}
			} else {
				if (!returncode.equals("00")) {
					log.info("讯银支付处理错误支付结果status为：" + returncode);

				}
			}
			log.info("讯银支付订单处理入账结果:{}", dealResult);
			// 通知第三方报文接收成功
			return "OK";
		} else {
			log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容[" + WaitSign + "]");
			return "FAILED";
		}

	}
}
