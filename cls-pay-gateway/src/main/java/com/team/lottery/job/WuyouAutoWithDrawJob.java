
package com.team.lottery.job;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.team.lottery.enums.EWithdrawAutoStatus;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.RechargeWithDrawOrderQuery;
import com.team.lottery.pay.util.Md5Util;
import com.team.lottery.pay.util.WuyouAESUtil;
import com.team.lottery.service.ChargePayService;
import com.team.lottery.service.WithdrawAutoConfigService;
import com.team.lottery.service.WithdrawAutoLogService;
import com.team.lottery.service.WithdrawOrderService;
import com.team.lottery.util.HttpClientUtil;
import com.team.lottery.util.JSONUtils;
import com.team.lottery.vo.ChargePay;
import com.team.lottery.vo.WithdrawAutoConfig;
import com.team.lottery.vo.WithdrawAutoLog;
import com.team.lottery.vo.WithdrawOrder;

import net.sf.json.JSONObject;

/** 
 * @Description: 无忧自动出款定时查询.
 * @Author: Jamine.
 * @CreateDate：2019-07-03 10:08:15.
 */
@Component("WuyouAutoWithDrawJob")
public class WuyouAutoWithDrawJob extends QuartzJob {

	private static Logger logger = LoggerFactory.getLogger(WuyouAutoWithDrawJob.class);

	@Autowired
	private WithdrawAutoLogService withdrawAutoLogService;

	@Autowired
	private WithdrawOrderService withdrawOrderService;

	@Autowired
	private ChargePayService chargePayService;

	@Autowired
	private WithdrawAutoConfigService withdrawAutoConfigService;

	@Override
	public void execute() {
		logger.info("开始处理无忧自动出款Job");
		// 查询出自动出款日一天中所有的DEALING记录
		List<WithdrawAutoLog> withdrawAutoLogs = withdrawAutoLogService.selectWithDrawAutoLogByDealStatus();
		for (WithdrawAutoLog withdrawAutoLog : withdrawAutoLogs) {
			if (withdrawAutoLog.getChargeType().equals("WUYOU")) {
				WithdrawAutoConfig withdrawAutoConfig = withdrawAutoConfigService.getWithdrawAutoConfigByBizSystem(withdrawAutoLog.getBizSystem());
				if (withdrawAutoConfig == null) {
					return;
				} 
				if (!"WUYOU".equals(withdrawAutoConfig.getThirdPayType())) {
					return;
				}
				ChargePay chargePay = chargePayService.selectByPrimaryKey(withdrawAutoConfig.getChargePayId());
				// 商户ID
				String firmNumber = chargePay.getMemberId();
				// 商户API密码
				String sign = chargePay.getSign();
				// 商户订单号
				String orderNumber = withdrawAutoLog.getSerialNumber();
				String aesStr = "{\"OrderNumber\":\""+ orderNumber +"\"}";
				// 进行AES签名
				String signature = WuyouAESUtil.encrypt(aesStr, sign);
				
				// 获取当前unix时间
				logger.info("无忧自动出款进行AES加密前字符串为:" + aesStr + "加密后为:" + signature);
				
				// 提交post表单.
				Map<String, String> dataMap = new HashMap<String, String>();
				dataMap.put("FirmNumber", firmNumber);
				dataMap.put("Signature", signature);
				
				// 代付查询的网关地址
				String url = chargePay.getReturnUrl();
				String doJsonForPost = HttpClientUtil.doPost(url, dataMap);
				logger.info("无忧自动出款返回信息:" + doJsonForPost);
				Boolean isJson = JSONUtils.isJson(doJsonForPost);
				
				if (isJson) {
					JSONObject jsonObject = JSONObject.fromObject(doJsonForPost);
					// 进行无忧自动出款处理处理.
					String msg = jsonObject.getString("msg");
					if ("OK，代付成功".equals(msg)) {
						RechargeWithDrawOrderQuery query = new RechargeWithDrawOrderQuery();
						query.setSerialNumber(withdrawAutoLog.getSerialNumber());
						List<WithdrawOrder> rechargeWithDrawOrders = withdrawOrderService.getRechargeWithDrawOrders(query);
						if (rechargeWithDrawOrders != null && rechargeWithDrawOrders.size() > 0) {
							bizSystem = rechargeWithDrawOrders.get(0).getBizSystem();
						} else {
							logger.error("根据订单号[" + withdrawAutoLog.getSerialNumber() + "]查找提现订单记录为空");
						}
						WithdrawOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
						if (rechargeWithDrawOrder == null) {
							logger.error("根据订单号[" + withdrawAutoLog.getSerialNumber() + "]查找提现订单记录为空");
						}
						logger.info("无忧自动出款开始处理提现订单号为[{}]的订单数据.",withdrawAutoLog.getSerialNumber());
						try {
							withdrawOrderService.setWithDrawOrdersAutoPaySuccess(rechargeWithDrawOrder.getId(),
									withdrawAutoLog.getSerialNumber());
						} catch (UnEqualVersionException e) {
							logger.error("改变提现订单状态时候发生异常:", e);
						} catch (Exception e) {
							logger.error("改变提现订单状态时候发生异常:", e);
						}

					}else if (msg.contains("No")) {
						// 更新失败日志
						WithdrawAutoLog query = new WithdrawAutoLog();
						query.setSerialNumber(withdrawAutoLog.getSerialNumber());
						List<WithdrawAutoLog> withdrawAutoLogFail = withdrawAutoLogService.getWithdrawAutoLogs(query);
						if (withdrawAutoLogFail != null && withdrawAutoLogFail.size() > 0) {
							WithdrawAutoLog autoLog = withdrawAutoLogFail.get(0);
							autoLog.setDealStatus(EWithdrawAutoStatus.FAIL.name());
							autoLog.setUpdatedDate(new Date());
							withdrawAutoLogService.updateByPrimaryKeySelective(autoLog);
						}
						logger.info("无忧通自动出款查询订单号[{}],错误描述为[{}]", withdrawAutoLog.getSerialNumber(), msg);
					} else {
						logger.info("无忧通自动出款查询订单号[{}],错误描述为[{}]", withdrawAutoLog.getSerialNumber(), msg);
					}
				}

			}
		}
		logger.info("结束处理无忧自动出款Job");
	}
}
