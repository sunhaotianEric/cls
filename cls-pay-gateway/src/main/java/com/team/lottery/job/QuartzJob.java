package com.team.lottery.job;

import java.util.Date;

/**
 * 
 * @author gs
 *
 */
public abstract class QuartzJob {

	protected Date nowTime;  //可以设置当前的时间
	
	protected String  bizSystem = null; //业务系统
	
	
	protected Boolean  isReleaseMoney = true; //是否同时发放分红，或者工资
	
	protected String  releasePolicy = null; //发放策略
	
	
	public abstract void execute();

	public Date getNowTime() {
		return nowTime;
	}

	public void setNowTime(Date nowTime) {
		this.nowTime = nowTime;
	}

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}


	public Boolean getIsReleaseMoney() {
		return isReleaseMoney;
	}


	public void setIsReleaseMoney(Boolean isReleaseMoney) {
		this.isReleaseMoney = isReleaseMoney;
	}


	public String getReleasePolicy() {
		return releasePolicy;
	}


	public void setReleasePolicy(String releasePolicy) {
		this.releasePolicy = releasePolicy;
	}
	

	
	
	
}
