package com.team.lottery.utils;

import java.io.IOException;
import java.util.Properties;

public class PropertyUtils {
	public static Properties p = null;

	static {
		p = new Properties();
		try {
			p.load(PropertyUtils.class.getResourceAsStream("/url.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void set(String key, String value) {
		p.setProperty(key, value);
	}

	public static String get(String key) {
		return p.getProperty(key);
	}
}
