package com.team.lottery.utils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.json.JSONObject;

/**
 * 发送POST请求工具类.
 * @author Jamine
 *
 */
public class HttpClientUtil {
	
	private static Logger log = LoggerFactory.getLogger(HttpClientUtil.class);

	/**
	 * 发送POST请求.
	 * @param urls 发送地址.
	 * @param jsonParam 发送参数.
	 * @return
	 */
	public static String doJsonForPost(String urls, JSONObject jsonParam) {
		HttpPost httpPost = null;
		CloseableHttpClient client = null;
		HttpResponse resp = null;
		HttpEntity he = null;
		String respContent = null;
		
		try {
			
			httpPost = new HttpPost(urls);
			client = HttpClients.createDefault();

			StringEntity entity = new StringEntity(jsonParam.toString(), "utf-8");// 解决中文乱码问题
			entity.setContentEncoding("UTF-8");
			entity.setContentType("application/json");
			httpPost.setEntity(entity);
			resp = client.execute(httpPost);
			if (resp.getStatusLine().getStatusCode() == 200) {
				he = resp.getEntity();
				respContent = EntityUtils.toString(he, "UTF-8");
			}
			return respContent;
		} catch (Exception e) {
			log.error("HttpClientUtil doJsonForPost发生异常", e);
		}
		return respContent;
	}
}
