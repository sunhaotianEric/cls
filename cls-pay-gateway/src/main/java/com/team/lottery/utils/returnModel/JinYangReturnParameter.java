package com.team.lottery.utils.returnModel;

public class JinYangReturnParameter {

	private String rspCode;
	
	private String rspMsg;
	
	private JinYangData data;

	public String getRspCode() {
		return rspCode;
	}

	public void setRspCode(String rspCode) {
		this.rspCode = rspCode;
	}

	public String getRspMsg() {
		return rspMsg;
	}

	public void setRspMsg(String rspMsg) {
		this.rspMsg = rspMsg;
	}

	public JinYangData getData() {
		return data;
	}

	public void setData(JinYangData data) {
		this.data = data;
	}
	
	
	
}	