package com.team.lottery.utils.returnModel;
/**
 * 拉酷回调返回对象
 * @author Jamine
 * 
 * 
 *
 */
public class LaKuReturnParam {
	
    private String status;
	
	private String customerid;
	
	private String sdpayno;
	
	private String sdorderno;
	
	private String total_fee;
	
	private String paytype;
	
	private String remark;
	
	private String sign;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCustomerid() {
		return customerid;
	}

	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}

	public String getSdpayno() {
		return sdpayno;
	}

	public void setSdpayno(String sdpayno) {
		this.sdpayno = sdpayno;
	}

	public String getSdorderno() {
		return sdorderno;
	}

	public void setSdorderno(String sdorderno) {
		this.sdorderno = sdorderno;
	}

	public String getTotal_fee() {
		return total_fee;
	}

	public void setTotal_fee(String total_fee) {
		this.total_fee = total_fee;
	}

	public String getPaytype() {
		return paytype;
	}

	public void setPaytype(String paytype) {
		this.paytype = paytype;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}
	
}
