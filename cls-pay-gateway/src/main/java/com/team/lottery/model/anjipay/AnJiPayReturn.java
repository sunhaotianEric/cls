
package com.team.lottery.model.anjipay;

/** 
 * @Description: 安吉支付返回对象 
 * @Author: Jamine.
 * @CreateDate：2019-05-15 02:40:08.
 */
public class AnJiPayReturn {
	// 商户号ID.
	private String Merchant_ID;
	// 类型.
	private String Type;
	// 商户订单号.
	private String Merchant_Order;
	// 平台订单号.
	private String Sys_Order;
	// 订单号金额(元).
	private String Amount;
	// 订单状态.
	private String Status;
	// 签名类型.
	private String Sign_Type;
	// 签名.
	private String Sign;
	public String getMerchant_ID() {
		return Merchant_ID;
	}
	public void setMerchant_ID(String merchant_ID) {
		Merchant_ID = merchant_ID;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	public String getMerchant_Order() {
		return Merchant_Order;
	}
	public void setMerchant_Order(String merchant_Order) {
		Merchant_Order = merchant_Order;
	}
	public String getSys_Order() {
		return Sys_Order;
	}
	public void setSys_Order(String sys_Order) {
		Sys_Order = sys_Order;
	}
	public String getAmount() {
		return Amount;
	}
	public void setAmount(String amount) {
		Amount = amount;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getSign_Type() {
		return Sign_Type;
	}
	public void setSign_Type(String sign_Type) {
		Sign_Type = sign_Type;
	}
	public String getSign() {
		return Sign;
	}
	public void setSign(String sign) {
		Sign = sign;
	}
	@Override
	public String toString() {
		return "安吉支付回调对象 [商户号=" + Merchant_ID + ", 类型=" + Type + ", 商户订单号=" + Merchant_Order + ", 平台订单号="
				+ Sys_Order + ", 金额=" + Amount + "元, 订单状态=" + Status + ", 签名类型=" + Sign_Type + ", 签名=" + Sign + "]";
	}
	
	
}
