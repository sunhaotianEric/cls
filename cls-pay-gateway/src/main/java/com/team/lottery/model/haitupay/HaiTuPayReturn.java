
package com.team.lottery.model.haitupay;

/** 
 * @Description: 海图回调参数封装
 * @Author: Jamine.
 * @CreateDate：2019-05-25 06:28:56.
 */
public class HaiTuPayReturn {
	// 商户ID.
	private String mch_id;
	// 随机字符串.
	private String nonce_str;
	// 平台订单号.
	private String out_trade_no;
	// 商品描述.
	private String body;
	// 支付金额.
	private String total_fee;
	// 字符串.
	private String charset;
	// 通知时间.
	private String notify_time;
	// 平台订单号.
	private String transaction_id;
	// 第三方平台订单号.
	private String third_transaction_id;
	// 签名.
	private String sign;
	public String getMch_id() {
		return mch_id;
	}
	public void setMch_id(String mch_id) {
		this.mch_id = mch_id;
	}
	public String getNonce_str() {
		return nonce_str;
	}
	public void setNonce_str(String nonce_str) {
		this.nonce_str = nonce_str;
	}
	public String getOut_trade_no() {
		return out_trade_no;
	}
	public void setOut_trade_no(String out_trade_no) {
		this.out_trade_no = out_trade_no;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getTotal_fee() {
		return total_fee;
	}
	public void setTotal_fee(String total_fee) {
		this.total_fee = total_fee;
	}
	public String getCharset() {
		return charset;
	}
	public void setCharset(String charset) {
		this.charset = charset;
	}
	public String getNotify_time() {
		return notify_time;
	}
	public void setNotify_time(String notify_time) {
		this.notify_time = notify_time;
	}
	public String getTransaction_id() {
		return transaction_id;
	}
	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}
	public String getThird_transaction_id() {
		return third_transaction_id;
	}
	public void setThird_transaction_id(String third_transaction_id) {
		this.third_transaction_id = third_transaction_id;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	@Override
	public String toString() {
		return "海图回调对象 [商户号=" + mch_id + ", 随机字符串=" + nonce_str + ", 商户订单号=" + out_trade_no + ", 商品描述=" + body
				+ ", 支付金额=" + total_fee + "分, 字符集=" + charset + ", 回调时间=" + notify_time + ", 平台订单号="
				+ transaction_id + ", 第三方平台号=" + third_transaction_id + ", 签名=" + sign + "]";
	}
	
	
}
