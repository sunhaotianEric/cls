
package com.team.lottery.model.xinduobaopay;

/** 
 * @Description: 鑫多宝支付通知对象.
 * @Author: Jamine.
 * @CreateDate：2019-05-14 11:00:41.
 */
public class XinDuoBaoPayReturn {
	// 订单号.
	private String orderno;
	// 商户号.
	private String usercode;
	// 商户订单号.
	private String customno;
	// 支付类型.
	private String type;
	// 银行编码.
	private String bankcode;
	// 提交金额.
	private String tjmoney;
	// 结算金额.
	private String money;
	// 订单状态: 0充值中 1充值成功 2充值失败
	private String status;
	// 0未退款，1申请退款，2退款中，  3退款成功，4退款失败 
	private String refundstatus;
	// 1人民币，2美元
	private String currency;
	// 签名.
	private String sign;
	// 返回码
	private String resultcode;
	// 返回信息.
	private String resultmsg;

	public String getOrderno() {
		return orderno;
	}

	public void setOrderno(String orderno) {
		this.orderno = orderno;
	}

	public String getUsercode() {
		return usercode;
	}

	public void setUsercode(String usercode) {
		this.usercode = usercode;
	}

	public String getCustomno() {
		return customno;
	}

	public void setCustomno(String customno) {
		this.customno = customno;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getBankcode() {
		return bankcode;
	}

	public void setBankcode(String bankcode) {
		this.bankcode = bankcode;
	}

	public String getTjmoney() {
		return tjmoney;
	}

	public void setTjmoney(String tjmoney) {
		this.tjmoney = tjmoney;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRefundstatus() {
		return refundstatus;
	}

	public void setRefundstatus(String refundstatus) {
		this.refundstatus = refundstatus;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getResultcode() {
		return resultcode;
	}

	public void setResultcode(String resultcode) {
		this.resultcode = resultcode;
	}

	public String getResultmsg() {
		return resultmsg;
	}

	public void setResultmsg(String resultmsg) {
		this.resultmsg = resultmsg;
	}

	@Override
	public String toString() {
		return "鑫多宝通知对象 [订单号=" + orderno + ", 商户号=" + usercode + ", 商户订单号=" + customno + ", 支付类型=" + type
				+ ", 银行编号=" + bankcode + ", 提交金额=" + tjmoney + ", 支付金额=" + money + ", 状态=" + status + ", 退款状态="
				+ refundstatus + ", 币种=" + currency + ", 签名=" + sign + ", 返回码=" + resultcode + ", 返回信息=" + resultmsg
				+ "]";
	}

	
}
