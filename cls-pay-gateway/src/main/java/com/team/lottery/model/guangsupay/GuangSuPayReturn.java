
package com.team.lottery.model.guangsupay;

/** 
 * @Description: 光速支付返回对象.
 * @Author: Jamine.
 * @CreateDate：2019-10-07 04:58:47.
 */
public class GuangSuPayReturn {
	// 商户号.
	private String mchno;
	// 10位秒数
	private String timestamp;
	// 签名
	private String signature;
	// 用户名
	private String username;
	// 平台订单号.
	private String order_no;
	// 支付系统订单号.
	private String pay_order_no;
	// 订单类型.
	private String order_type;
	private String vcoin_code;
	private String currency_type;
	private String currency_money;
	private String pay_amount;
	private String price;
	private String status;
	private String create_time;
	private String finish_time;
	private String remark;
	private String vcoin_count;

	public String getMchno() {
		return mchno;
	}

	public void setMchno(String mchno) {
		this.mchno = mchno;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getOrder_no() {
		return order_no;
	}

	public void setOrder_no(String order_no) {
		this.order_no = order_no;
	}

	public String getPay_order_no() {
		return pay_order_no;
	}

	public void setPay_order_no(String pay_order_no) {
		this.pay_order_no = pay_order_no;
	}

	public String getOrder_type() {
		return order_type;
	}

	public void setOrder_type(String order_type) {
		this.order_type = order_type;
	}

	public String getVcoin_code() {
		return vcoin_code;
	}

	public void setVcoin_code(String vcoin_code) {
		this.vcoin_code = vcoin_code;
	}

	public String getCurrency_type() {
		return currency_type;
	}

	public void setCurrency_type(String currency_type) {
		this.currency_type = currency_type;
	}

	public String getCurrency_money() {
		return currency_money;
	}

	public void setCurrency_money(String currency_money) {
		this.currency_money = currency_money;
	}

	public String getPay_amount() {
		return pay_amount;
	}

	public void setPay_amount(String pay_amount) {
		this.pay_amount = pay_amount;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreate_time() {
		return create_time;
	}

	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}

	public String getFinish_time() {
		return finish_time;
	}

	public void setFinish_time(String finish_time) {
		this.finish_time = finish_time;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getVcoin_count() {
		return vcoin_count;
	}

	public void setVcoin_count(String vcoin_count) {
		this.vcoin_count = vcoin_count;
	}

	@Override
	public String toString() {
		return "光速支付返回对象. [mchno=" + mchno + ", timestamp=" + timestamp + ", signature=" + signature + ", username=" + username
				+ ", order_no=" + order_no + ", pay_order_no=" + pay_order_no + ", order_type=" + order_type + ", vcoin_code=" + vcoin_code
				+ ", currency_type=" + currency_type + ", currency_money=" + currency_money + ", pay_amount=" + pay_amount + ", price="
				+ price + ", status=" + status + ", create_time=" + create_time + ", finish_time=" + finish_time + ", remark=" + remark
				+ "]";
	}
	
}
