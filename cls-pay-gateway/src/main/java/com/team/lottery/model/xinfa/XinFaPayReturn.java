
package com.team.lottery.model.xinfa;

/** 
 * @Description: 鑫发支付返回参数封装.
 * @Author: Jamine.
 * @CreateDate：2019-08-10 06:18:48.
 */
public class XinFaPayReturn {
	// 加密参数数据.
	private String data;
	// 商户号.
	private String merchNo;
	// 订单号.
	private String orderNo;

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getMerchNo() {
		return merchNo;
	}

	public void setMerchNo(String merchNo) {
		this.merchNo = merchNo;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	@Override
	public String toString() {
		return "鑫发支付返回对象 [data=" + data + ", merchNo=" + merchNo + ", orderNo=" + orderNo + "]";
	}

	
}
