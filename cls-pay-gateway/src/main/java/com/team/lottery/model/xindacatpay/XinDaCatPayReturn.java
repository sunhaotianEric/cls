
package com.team.lottery.model.xindacatpay;

/** 
 * @Description: 新达猫支付回调对象.
 * @Author: Jamine.
 * @CreateDate：2019-05-23 01:43:06.
 */
public class XinDaCatPayReturn {
	// 商户号.
	private String memberid;
	// 订单号.
	private String orderid;
	// 支付金额.
	private String amount;
	// 交易流水号.
	private String transaction_id;
	// 交易时间.
	private String datetime;
	// 交易状态码00为成功.
	private String returncode;
	// 签名.
	private String sign;
	public String getMemberid() {
		return memberid;
	}
	public void setMemberid(String memberid) {
		this.memberid = memberid;
	}
	public String getOrderid() {
		return orderid;
	}
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getTransaction_id() {
		return transaction_id;
	}
	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}
	public String getDatetime() {
		return datetime;
	}
	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}
	public String getReturncode() {
		return returncode;
	}
	public void setReturncode(String returncode) {
		this.returncode = returncode;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	
	@Override
	public String toString() {
		return "新达猫回调参数 [商户号=" + memberid + ", 订单号=" + orderid + ", 支付金额=" + amount + "元, 交易流水号="
				+ transaction_id + ", 交易日期=" + datetime + ", 返回状态码=" + returncode + ", 签名=" + sign + "]";
	}
	
}
