package com.team.lottery.model.xinfutong;

/**
 * @Description: 信付通支付返回对象
 * @Author: Jamine.
 * @CreateDate：2019-05-15 02:40:08.
 */
public class XinFuTongReturn {
	private String gmt_create;
	private String order_no;
	private String gmt_payment;
	private String seller_email;
	private String notify_time;
	private String quantity;
	private String sign;
	private String discount;
	private String body;
	private String is_success;
	private String title;
	private String gmt_logistics_modify;
	private String notify_id;
	private String notify_type;
	private String payment_type;
	private String ext_param2;
	private String price;
	private String total_fee;
	private String trade_status;
	private String trade_no;
	private String signType;
	private String seller_actions;
	private String seller_id;
	private String is_total_fee_adjust;

	public String getGmt_create() {
		return gmt_create;
	}

	public void setGmt_create(String gmt_create) {
		this.gmt_create = gmt_create;
	}

	public String getOrder_no() {
		return order_no;
	}

	public void setOrder_no(String order_no) {
		this.order_no = order_no;
	}

	public String getGmt_payment() {
		return gmt_payment;
	}

	public void setGmt_payment(String gmt_payment) {
		this.gmt_payment = gmt_payment;
	}

	public String getSeller_email() {
		return seller_email;
	}

	public void setSeller_email(String seller_email) {
		this.seller_email = seller_email;
	}

	public String getNotify_time() {
		return notify_time;
	}

	public void setNotify_time(String notify_time) {
		this.notify_time = notify_time;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getIs_success() {
		return is_success;
	}

	public void setIs_success(String is_success) {
		this.is_success = is_success;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getGmt_logistics_modify() {
		return gmt_logistics_modify;
	}

	public void setGmt_logistics_modify(String gmt_logistics_modify) {
		this.gmt_logistics_modify = gmt_logistics_modify;
	}

	public String getNotify_id() {
		return notify_id;
	}

	public void setNotify_id(String notify_id) {
		this.notify_id = notify_id;
	}

	public String getNotify_type() {
		return notify_type;
	}

	public void setNotify_type(String notify_type) {
		this.notify_type = notify_type;
	}

	public String getPayment_type() {
		return payment_type;
	}

	public void setPayment_type(String payment_type) {
		this.payment_type = payment_type;
	}

	public String getExt_param2() {
		return ext_param2;
	}

	public void setExt_param2(String ext_param2) {
		this.ext_param2 = ext_param2;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getTotal_fee() {
		return total_fee;
	}

	public void setTotal_fee(String total_fee) {
		this.total_fee = total_fee;
	}

	public String getTrade_status() {
		return trade_status;
	}

	public void setTrade_status(String trade_status) {
		this.trade_status = trade_status;
	}

	public String getTrade_no() {
		return trade_no;
	}

	public void setTrade_no(String trade_no) {
		this.trade_no = trade_no;
	}

	public String getSignType() {
		return signType;
	}

	public void setSignType(String signType) {
		this.signType = signType;
	}

	public String getSeller_actions() {
		return seller_actions;
	}

	public void setSeller_actions(String seller_actions) {
		this.seller_actions = seller_actions;
	}

	public String getSeller_id() {
		return seller_id;
	}

	public void setSeller_id(String seller_id) {
		this.seller_id = seller_id;
	}

	public String getIs_total_fee_adjust() {
		return is_total_fee_adjust;
	}

	public void setIs_total_fee_adjust(String is_total_fee_adjust) {
		this.is_total_fee_adjust = is_total_fee_adjust;
	}

	@Override
	public String toString() {
		return "安吉支付回调对象 [gmt_create=" + gmt_create + ", order_no=" + order_no + ", gmt_payment="
				+ gmt_payment + ", seller_email=" + seller_email + ", notify_time=" + notify_time
				+ ", quantity=" + quantity + ", discount="+ discount + ", body=" + body
				+ ", is_success=" + is_success +", title=" + title + ", gmt_logistics_modify=" + gmt_logistics_modify
				+ ", notify_id=" + notify_id + ", notify_type="+ notify_type + ", payment_type=" + payment_type
				+ ", ext_param2=" + ext_param2 +", price=" + price + ", total_fee=" + total_fee
				+ ", trade_status=" + trade_status + ", trade_no="+ trade_no + ", seller_actions=" + seller_actions
				+ ", seller_id=" + seller_id + ", is_total_fee_adjust="+ is_total_fee_adjust + ", signType=" + signType+ ", sign=" + sign+ "]";
	}
	
}
