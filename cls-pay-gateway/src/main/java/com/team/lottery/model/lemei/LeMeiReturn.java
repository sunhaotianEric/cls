package com.team.lottery.model.lemei;

/**
 * @Description: 乐美支付返回对象
 * @Author: Jamine.
 * @CreateDate：2019-05-15 02:40:08.
 */
public class LeMeiReturn {

	public String UserID;

	public String OrderID;

	public String OrderIDP;

	public String FaceValue;

	public String PayTime;

	public String PayState;

	public String sign;

	public String getUserID() {
		return UserID;
	}

	public void setUserID(String userID) {
		UserID = userID;
	}

	public String getOrderID() {
		return OrderID;
	}

	public void setOrderID(String orderID) {
		OrderID = orderID;
	}

	public String getOrderIDP() {
		return OrderIDP;
	}

	public void setOrderIDP(String orderIDP) {
		OrderIDP = orderIDP;
	}

	public String getFaceValue() {
		return FaceValue;
	}

	public void setFaceValue(String faceValue) {
		FaceValue = faceValue;
	}

	public String getPayTime() {
		return PayTime;
	}

	public void setPayTime(String payTime) {
		PayTime = payTime;
	}

	public String getPayState() {
		return PayState;
	}

	public void setPayState(String payState) {
		PayState = payState;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	

	@Override
	public String toString() {
		return "乐美支付回调对象 [商户号=" + UserID + ", 商户订单号="
				+ OrderID + ", 平台订单号=" + OrderIDP + ", 金额=" + FaceValue
				+ "元, 订单状态=" + PayState + ", 支付支付时间=" + PayTime + ", 签名=" + sign
				+ "]";
	}

}
