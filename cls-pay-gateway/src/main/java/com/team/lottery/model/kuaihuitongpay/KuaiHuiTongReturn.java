
package com.team.lottery.model.kuaihuitongpay;

/** 
 * @Description: 快汇通回调处理.
 * @Author: Jamine.
 * @CreateDate：2019-05-20 10:54:20.
 */
public class KuaiHuiTongReturn {
	// 交易金额.
	private String transAmt;
	// 结算金额.
	private String actualAmount;
	// 交易日期.
	private String transDate;
	// 订单号.
	private String orderNo;
	// 订单状态.
	private String orderStatus;
	// 交易流水.
	private String tranSerno;
	// 签名.
	private String sign;
	// 通讯应答码.
	private String ret_code;
	// 通讯描述.
	private String ret_msg;
	
	public String getTransAmt() {
		return transAmt;
	}
	public void setTransAmt(String transAmt) {
		this.transAmt = transAmt;
	}
	public String getActualAmount() {
		return actualAmount;
	}
	public void setActualAmount(String actualAmount) {
		this.actualAmount = actualAmount;
	}
	public String getTransDate() {
		return transDate;
	}
	public void setTransDate(String transDate) {
		this.transDate = transDate;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getTranSerno() {
		return tranSerno;
	}
	public void setTranSerno(String tranSerno) {
		this.tranSerno = tranSerno;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public String getRet_code() {
		return ret_code;
	}
	public void setRet_code(String ret_code) {
		this.ret_code = ret_code;
	}
	public String getRet_msg() {
		return ret_msg;
	}
	public void setRet_msg(String ret_msg) {
		this.ret_msg = ret_msg;
	}
	@Override
	public String toString() {
		return "快汇通自动出款成功通知对象 [支付金额=" + transAmt + "分, 结算金额=" + actualAmount + ", 交易日期=" + transDate + ", 订单号="
				+ orderNo + ", 订单状态=" + orderStatus + ", 交易流水号=" + tranSerno + ", 签名=" + sign + ", 响应码=" + ret_code
				+ ", 响应描述=" + ret_msg + "]";
	}
	
	
	
}
