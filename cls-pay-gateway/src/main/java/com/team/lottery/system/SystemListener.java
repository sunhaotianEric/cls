package com.team.lottery.system;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.system.message.MessageQueueThread;

public class SystemListener implements ServletContextListener {

	private static Logger logger = LoggerFactory.getLogger(SystemListener.class);

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {

	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {

		// 初始化业务系统参数配置
		BizSystemConfigManager.initAllBizSystemConfig();

		// 启动消息接收处理线程
		MessageQueueThread messageQueueThread = new MessageQueueThread();
		messageQueueThread.start();

		// 启动订阅线程

	}

}
