package com.team.lottery.mapper.usermoneytotal;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.UserQuery;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserMoneyTotal;

public interface UserMoneyTotalMapper {
    int deleteByPrimaryKey(Long id);

    int insert(UserMoneyTotal record);

    int insertSelective(UserMoneyTotal record);

    UserMoneyTotal selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UserMoneyTotal record);

    int updateByPrimaryKey(UserMoneyTotal record);
    
    /**
     * 更新用户 版本号+1
     * @param record
     * @return
     */
    public int updateByPrimaryKeyWithVersionSelective(UserMoneyTotal record);
    
    /**
     * 根据userid查询 
     * @param record
     * @return
     */
    public UserMoneyTotal selectByUserId(Integer userId);
    
    /**
     * 根据userid和bizsystem等 查询 
     * @param record
     * @return
     */
    public UserMoneyTotal selectByQuery(@Param("query")UserMoneyTotal record);
    
    
    /**
     * 查询符合条件的记录条数
     * @param query
     * @return
     */
    public Integer getAllUserMoneyTotalByQueryPageCount(@Param("query")UserMoneyTotal query);
    
    /**
     * 分页条件查询
     * @return
     */
	public List<UserMoneyTotal> getAllUserMoneyTotalByQueryPage(@Param("query")UserMoneyTotal query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
	
	/**
	 * 统计用户的所有余额、分红、盈利等数据
	 * @return
	 */
	public UserMoneyTotal getTotalUsersConsumeReport(@Param("query")UserMoneyTotal query);
	
	 /**
     * 查询符合条件的记录条数
     * @param query
     * @return
     */
    public Integer getUserStatisticalRankingsPageCount(@Param("query")UserQuery query);

    
	/**
	 * 统计用户的所有余额、分红、盈利等数据
	 * @return
	 */
    public List<UserMoneyTotal> getUserStatisticalRankingsPage(@Param("query")UserQuery query, @Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
    
    /**
     * 统计符合条件用户的所有id
     * @param query
     * @return
     */
    public List<UserMoneyTotal> queryUserStatisticalRankingsIds(@Param("query")UserQuery query);
    
    /**
     * 根据ID字符串一键修改用户星级数量
     * @param sl
     * @param ids
     * @return
     */
    public Integer updateUserStarLevelByUserId(@Param("sl")Integer sl,@Param("ids")String ids);
    
}