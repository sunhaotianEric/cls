package com.team.lottery.mapper.usermodelused;

import java.util.Date;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.UserModelUsed;


public interface UserModelUsedMapper {
    int deleteByPrimaryKey(Long id);

    int insert(UserModelUsed record);

    int insertSelective(UserModelUsed record);

    UserModelUsed selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UserModelUsed record);

    int updateByPrimaryKey(UserModelUsed record);
    
    UserModelUsed selectByUserId(@Param("userId")Integer userId, @Param("belongDate")Date belongDate);
    
    UserModelUsed selectByUserName(@Param("userName")String userName, @Param("belongDate")Date belongDate);
}