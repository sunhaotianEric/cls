package com.team.lottery.mapper.userbettingdemand;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.UserBettingDemandQuery;
import com.team.lottery.vo.UserBettingDemand;

public interface UserBettingDemandMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UserBettingDemand record);

    int insertSelective(UserBettingDemand record);

    UserBettingDemand selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserBettingDemand record);

    int updateByPrimaryKey(UserBettingDemand record);

    /**
     * 分页查询所有符合要求的记录条数
     * @param query
     * @return
     */
	Integer getAllUserBettingDemandPageCount(@Param("query")UserBettingDemandQuery query);

	/**
	 * 分页查询所有符合要求的记录
	 * @param query
	 * @param startIndex
	 * @param pageSize
	 * @return
	 */
	List<UserBettingDemand> getAllUserBettingDemandPage(@Param("query")UserBettingDemandQuery query, @Param("startNum")int startNum, @Param("pageNum")Integer pageNum);
	
	/**
	 * 查询用户进行中的打码要求
	 * @param userId
	 * @return
	 */
	UserBettingDemand getUserGoingBettingDemand(@Param("userId")Integer userId);
}