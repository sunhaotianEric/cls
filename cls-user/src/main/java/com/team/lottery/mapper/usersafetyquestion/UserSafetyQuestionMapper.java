package com.team.lottery.mapper.usersafetyquestion;

import java.util.List;

import com.team.lottery.vo.UserSafetyQuestion;

public interface UserSafetyQuestionMapper {
    int deleteByPrimaryKey(Long id);

    int insert(UserSafetyQuestion record);

    int insertSelective(UserSafetyQuestion record);

    UserSafetyQuestion selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UserSafetyQuestion record);

    int updateByPrimaryKey(UserSafetyQuestion record);

	int getQuestionCountByUserId(Integer id);

	List<UserSafetyQuestion> getQuestionsByUserId(Integer id);

	int getQuestionCountByUsername(UserSafetyQuestion query);

	List<UserSafetyQuestion> getQuestionsByUsername(UserSafetyQuestion query);

	void deleteSafeQuestionByUserId(Integer userId);
}