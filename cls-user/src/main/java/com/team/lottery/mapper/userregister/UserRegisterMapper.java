package com.team.lottery.mapper.userregister;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.UserRegister;

public interface UserRegisterMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UserRegister record);

    int insertSelective(UserRegister record);

    UserRegister selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserRegister record);

    int updateByPrimaryKey(UserRegister record);
    
    /**
     * 获取用户的开户链接地址
     * @param userId
     * @return
     */
    public List<UserRegister> getUserRegisterLinks(Integer userId);
    
    /**
     * 根据UUID,查询用户的注册链接地址
     * @param uuid
     * @return
     */
    public UserRegister getUserRegisterLinkByUUID(String uuid);
    
    /**
     * 加载所有的注册链接地址
     * @return
     */
    public List<UserRegister> getAllUserRegisterLinks();
    
    
    /**
     * 获取用户的开户链接地址 分页查询
     * @param userId
     * @return
     */
    public List<UserRegister> getUserRegisterLinksPage(@Param("query")UserRegister userRegister,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
    
    /**
     * 获取用户的开户链接地址 分页统计
     * @param userId
     * @return
     */
    public Integer getUserRegisterLinksPageCount(@Param("query")UserRegister userRegister);
    
    /**
             验证查询邀请码个数
     * @return
     */
    public Integer getInvitationCodeCount (@Param("invitationCode")String invitationCode ,@Param("bizSystem")String bizSystem);
   
		/**
		  根据邀请码查询
		* @return
		*/
    public UserRegister getUserRegisterLinkByInvitationCode (@Param("invitationCode")String invitationCode ,@Param("bizSystem")String bizSystem);
  
    /**
     * 查询推广数据
     * @param userRegister
     * @param startNum
     * @param pageNum
     * @return
     */
    public List<UserRegister> queryUserRegister(@Param("query")UserRegister userRegister,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
    
    /**
     * 推广数据总条数
     * @param userRegister
     * @return
     */
    public int queryUserRegisterCount(@Param("query")UserRegister userRegister);
    
    /**
     * 删除推广链接
     * @param id
     * @return
     */
    public int delRegister(@Param("id")Integer id);
    
    /**
     * 加载某业务系统的注册链接地址
     * @return
     */
    public List<UserRegister> getUserRegisterLinksWithBizSystem(@Param("bizSystem") String bizSystem);
    
    /**
     * 批量更新注册链接中当前奖金模式
     * @param userRegisters
     * @return
     */
    public int updateBatchLinkSets(List<UserRegister> userRegisters);
  
}