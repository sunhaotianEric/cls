package com.team.lottery.mapper.signinrecord;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.SigninRecordQuery;
import com.team.lottery.extvo.SigninRecrodQuery;
import com.team.lottery.vo.SigninRecord;

public interface SigninRecordMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SigninRecord record);

    int insertSelective(SigninRecord record);

    SigninRecord selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SigninRecord record);

    int updateByPrimaryKey(SigninRecord record);
    
    List<SigninRecord> querySingninRecord(@Param("query")SigninRecrodQuery query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
   
    int querySingninRecordCount(@Param("query")SigninRecrodQuery query);
   
    List<SigninRecord> getSigninRecordByUserSigninTime(@Param("query")SigninRecordQuery query);
    
    SigninRecord getSigninRecordByContinueSigninDay(@Param("query")SigninRecordQuery signinRecordQuery);

	SigninRecord getSigninRecordBySignDate(@Param("query")SigninRecordQuery signinRecordQuery);
}