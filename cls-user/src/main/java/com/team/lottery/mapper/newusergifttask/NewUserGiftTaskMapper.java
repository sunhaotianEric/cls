package com.team.lottery.mapper.newusergifttask;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.NewUserGiftTaskQuery;
import com.team.lottery.vo.NewUserGiftTask;

public interface NewUserGiftTaskMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(NewUserGiftTask record);

    int insertSelective(NewUserGiftTask record);

    NewUserGiftTask selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(NewUserGiftTask record);

    int updateByPrimaryKey(NewUserGiftTask record);
    
    /**
     *根据用户ID去查询用户新手注册礼包完成的情况.
     * @param id 前端传过来的用户ID.
     * @return
     */
	NewUserGiftTask getNewUserReceiveStatusById(Integer id);
	
	/**
	 * 根据条件查询新手礼包领取表中的记录条数.
	 * @param query 查询条件.
	 * @return
	 */
	Integer getNewUserGiftTaskCount(@Param("query")NewUserGiftTaskQuery query);
	
	/**
	 * 根据条件查询新手礼包中的数据详情.
	 * @param query 查询条件.
	 * @return 返回List集合.
	 */
	List<NewUserGiftTask> getNewUserGiftTask(@Param("query")NewUserGiftTaskQuery query,@Param("startNum")Integer startNum, @Param("pageNum")Integer pageNum);

}