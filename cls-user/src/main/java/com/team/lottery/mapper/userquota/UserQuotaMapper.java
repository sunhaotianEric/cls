package com.team.lottery.mapper.userquota;

import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.UserQuota;


public interface UserQuotaMapper {
    int deleteByPrimaryKey(Long id);

    int insert(UserQuota record);

    int insertSelective(UserQuota record);

    UserQuota selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UserQuota record);

    int updateByPrimaryKey(UserQuota record);
    
    /**
     * 根据用户名得到用户配额信息
     * @param userName
     * @return
     */
    List<UserQuota> getQuotaByUserName(@Param("userName")String userName,@Param("bizSystem")String bizSystem);
    
    /**
     * 根据用户Id得到用户配额信息
     * @param userId
     * @return
     */
    List<UserQuota> getQuotaByUserId(Integer userId);
    
  /*  *//**
     * 根据用户Id和模式得到用户配额信息
     * @param userId
     * @return
     *//*
    List<UserQuota> getQuotaByQuery(@Param("query")UserQuota query);*/
    
    /**
	 * 根据用户Id和时时彩模式查询配额记录
	 * @param userId
	 * @param sscRebate
	 * @return
	 */
    UserQuota getQuotaByUserIdAndSscRebate(@Param("userId")Integer userId, @Param("sscRebate")BigDecimal sscRebate);
}