package com.team.lottery.mapper.usercookielog;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.UserCookieLog;

public interface UserCookieLogMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UserCookieLog record);

    int insertSelective(UserCookieLog record);

    UserCookieLog selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserCookieLog record);

    int updateByPrimaryKey(UserCookieLog record);
    
    /**
     * 根据token来查找记录
     * @param token
     * @return
     */
    UserCookieLog queryByToken(String token);
    
    /**
     * 使之前用户的cookie失效
     * @param userId
     * @param domainUrl
     */
    int setBeforeCookieExpired(@Param("userId")Integer userId, @Param("domainUrl")String domainUrl);
}