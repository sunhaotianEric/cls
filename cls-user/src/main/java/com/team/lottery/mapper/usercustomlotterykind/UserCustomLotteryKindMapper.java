package com.team.lottery.mapper.usercustomlotterykind;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.UserCustomLotteryKind;


public interface UserCustomLotteryKindMapper {
    int deleteByPrimaryKey(Long id);

    int insert(UserCustomLotteryKind record);

    int insertSelective(UserCustomLotteryKind record);

    UserCustomLotteryKind selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UserCustomLotteryKind record);

    int updateByPrimaryKey(UserCustomLotteryKind record);
    
    /**
     * 通过用户Id查找对应的用户自定义彩种设置
     * @param userName
     * @return
     */
    public UserCustomLotteryKind getUserCustomLotteryKindById(@Param("userId")Integer userId,@Param("setType")String setType);
}