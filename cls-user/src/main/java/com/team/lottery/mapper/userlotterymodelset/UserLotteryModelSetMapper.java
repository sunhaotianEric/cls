package com.team.lottery.mapper.userlotterymodelset;

import java.util.List;

import com.team.lottery.vo.UserLotteryModelSet;

public interface UserLotteryModelSetMapper {
    int deleteByPrimaryKey(Long id);

    int insert(UserLotteryModelSet record);

    int insertSelective(UserLotteryModelSet record);

    UserLotteryModelSet selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UserLotteryModelSet record);

    int updateByPrimaryKey(UserLotteryModelSet record);
    
    /**
     * 根据用户ID获取用户投注模式设置
     * @param userName
     * @return
     */
    List<UserLotteryModelSet> getUserLotteryModelSetByUserId(Integer userId);
}