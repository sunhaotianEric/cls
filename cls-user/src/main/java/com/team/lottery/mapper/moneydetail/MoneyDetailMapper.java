package com.team.lottery.mapper.moneydetail;

import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.MoneyDetailQuery;
import com.team.lottery.vo.MoneyDetail;

public interface MoneyDetailMapper {
    int deleteByPrimaryKey(Long id);

    int insert(MoneyDetail record);

    int insertSelective(MoneyDetail record);

    MoneyDetail selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(MoneyDetail record);

    int updateByPrimaryKey(MoneyDetail record);
    
    /**
     * 查找最新的可分红的资金明细记录
     * @return
     */
    public MoneyDetail getLastestCanBonusRecord(String bizSystem);
    
    /**
     * 查找符合条件的资金明细记录
     * @param query
     * @return
     */
    public List<MoneyDetail> queryMoneyDetailsByCondition(@Param("query")MoneyDetailQuery query);
    
    
    /**
     * 分页查找符合条件的资金明细记录记录条数
     * @param query
     * @return
     */
    public Integer queryMoneyDetailsByQueryPageCount(@Param("query")MoneyDetailQuery query);
    
    
    /**
     * 资金明细记录 是否已经生成
     * @param query
     * @return
     */
    public Integer getCountByOrderDtype(@Param("query")MoneyDetailQuery query);
    
    /**
     * 分页查找符合条件的资金明细记录
     * @param query
     * @return
     */
    public List<MoneyDetail> queryMoneyDetailsByQueryPage(@Param("query")MoneyDetailQuery query, @Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
    
    
    /**
     * 查询符合条件的记录条数
     * @param query
     * @return
     */
    public Integer getAllMoneyDetailsByQueryPageCount(@Param("query")MoneyDetailQuery query);
    
    /**
     * 分页条件查询登陆日志
     * @return
     */
	List<MoneyDetail> getAllMoneyDetailsByQueryPage(@Param("query")MoneyDetailQuery query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
	
	/**
	 * 资金明细总额
	 * @param query
	 * @return
	 */
	public MoneyDetail getAllMoneyDetailsTotal(@Param("query")MoneyDetailQuery query);
	
	/**
     * 查询用户账户明细记录条数
     * @param query
     * @return
     */
    public Integer getUserMoneyDetailsByQueryPageCount(@Param("query")MoneyDetailQuery query);
    
    /**
     * 分页条件查询用户账户明细
     * @return
     */
	List<MoneyDetail> getUserMoneyDetailsByQueryPage(@Param("query")MoneyDetailQuery query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
	
	/**
     * 查找团队资金明细记录，包括团队领导人自己
     * @return
     */
	List<MoneyDetail> queryTeamMoneyDetailsByCondition(@Param("query")MoneyDetailQuery query);
//	/**
//	 * 条件查找资金明细以统计消费报表
//	 * @param query
//	 * @return
//	 */
//    public List<MoneyDetail> getConsumeReportForQueryPage(@Param("query")MoneyDetailQuery query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
//
//    /**
//     * 分页统计消费报表
//     * @param query
//     * @return
//     */
//    public Integer getConsumeReportForQueryPageCount(@Param("query")MoneyDetailQuery query);
	
	/**
	 * 查找某段时间内有资金明细的所有用户
	 * @param query
	 * @return
	 */
	 List<String> getUsersHasMoneyDetail(@Param("query")MoneyDetailQuery query);
	 
	 /**
	  * 根据资金明细统计各类型总额
	  * @return
	  */
	 List<MoneyDetail> getTotalMoneyByDetailType(@Param("query")MoneyDetailQuery query);
	 
	 /**
	  * 根据资金明细统计各类型用户人数
	  * @param query
	  * @return
	  */
	 List<MoneyDetail> getUserCountByDetailType(@Param("query")MoneyDetailQuery query);
	 
	 /**
	  * 根据资金明细统计相关类型数据
	  * @param query
	  * @return
	  */
	 public BigDecimal getMoneyDetailByType(@Param("query")MoneyDetailQuery query);
}