package com.team.lottery.mapper.domaininvitationcode;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.DomainInvitationCode;
import com.team.lottery.vo.QuickBankType;

public interface DomainInvitationCodeMapper {
    int deleteByPrimaryKey(Long id);

    int insert(DomainInvitationCode record);

    int insertSelective(DomainInvitationCode record);

    DomainInvitationCode selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(DomainInvitationCode record);

    int updateByPrimaryKey(DomainInvitationCode record);

    DomainInvitationCode selectByPrimaryKeyForUpdate(Long id);
    
    /**
     * 查询符合条件的记录条数
     * @param query
     * @return
     */
    public Integer getAllDomainInvitationCodePageCount(@Param("query")DomainInvitationCode query);
    /**
     * 分页条件查询银行代码记录
     * @return
     */
    List<DomainInvitationCode> getAllDomainInvitationCodePage(@Param("query")DomainInvitationCode query,@Param("startNum")int startIndex, @Param("pageNum")Integer pageSize);

	DomainInvitationCode selectDomainInvitationCode(DomainInvitationCode query);

	
}