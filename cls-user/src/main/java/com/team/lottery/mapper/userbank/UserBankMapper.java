package com.team.lottery.mapper.userbank;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.UserBankQueryVo;
import com.team.lottery.extvo.UserQuery;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserBank;

public interface UserBankMapper {
    int deleteByPrimaryKey(Long id);

    int insert(UserBank record);

    int insertSelective(UserBank record);

    UserBank selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UserBank record);

    int updateByPrimaryKey(UserBank record);

	List<UserBank> getAllUserBanksByUserId(Integer userId);

	UserBank getUserBankByBankNum(@Param("bizSystem")String bizSystem, @Param("bankCardNum")String bankCardNum);
	
	/**
	 * 查找用户对应的银行卡
	 * @param withDrawId
	 * @param withDrawValue
	 * @return
	 */
	public UserBank getUserBankByIdAndUserId(@Param("withDrawId")Long withDrawId,@Param("userId")Integer userId);
	
	/**
	 * 查询用户的银行卡信息
	 * @return
	 */
	public List<UserBank> getUserBanksByUserName(@Param("userBankQueryVo")UserBankQueryVo userBankQueryVo);
	
	  /**
     * 查询符合条件的记录条数
     * @param query
     * @return
     */
    public Integer getAllUserBanksByQueryPageCount(@Param("query")UserBankQueryVo query);
        
    /**
     * 分页条件
     * @return
     */
    public  List<UserBank> getAllUserBanksByQueryPage(@Param("query")UserBankQueryVo query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);

    
    /**查询其他姓名的银行卡记录
     * 分页条件
     * @return
     */
	List<UserBank> queryOtherNameBanks(UserBankQueryVo queryUser);
    
}