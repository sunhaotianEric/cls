package com.team.lottery.mapper.quotacontrol;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.Admin;
import com.team.lottery.vo.QuotaControl;

public interface QuotaControlMapper {
    int deleteByPrimaryKey(Long id);

    int insert(QuotaControl record);

    int insertSelective(QuotaControl record);

    QuotaControl selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(QuotaControl record);

    int updateByPrimaryKey(QuotaControl record);
    
    /**
     * 根据bizSystem查找相应的记录
     * @param bizSystem
     * @return
     */
    List<QuotaControl> getQuotaControlByBizSystem(@Param("bizSystem")String bizSystem);
	
     /**
     * 查询符合条件的记录条数
     * @param query
     * @return
     */
    public Integer getAllQuotaControlByQueryPageCount(@Param("query")QuotaControl query);
        
    /**
     * 分页条件
     * @return
     */
    public  List<Admin> getAllQuotaControlByQueryPage(@Param("query")QuotaControl query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
	
}