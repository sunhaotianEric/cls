package com.team.lottery.mapper.userdelete;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.DeleteUserQuery;
import com.team.lottery.vo.UserDel;

public interface UserDelMapper {
	int deleteByPrimaryKey(Integer id);

	int insert(UserDel record);

	int insertSelective(UserDel record);

	UserDel selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(UserDel record);

	int updateByPrimaryKey(UserDel record);
	
	UserDel deleteByInvalidMember(Integer id);
	/**
	 * 查找所有被删用户
	 * @return
	 */
	List<UserDel> getDelAllUsers();
	/**
	 * 查找满足条件的用户
	 * @param query
	 * @return
	 */
	List<UserDel> getdelUserByParameter(@Param("query") DeleteUserQuery query);
	/**
	 * 分页查找被删用户
	 * @param query
	 * @param startNum
	 * @param pageNum
	 * @return
	 */
	List<UserDel> selectDeleteUserAll(@Param("query")DeleteUserQuery query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
	/**
	 *  查找符合条件的记录总条数
	 * @param query
	 * @return
	 */
	public Integer getAllUsersByQueryPageCount(@Param("query")DeleteUserQuery query);
	/**
	 * 恢复被删会员
	 * @param userId
	 */
	public int deleteRegressionById(@Param("query")DeleteUserQuery query);


}