package com.team.lottery.mapper.userrebateforbid;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.UserRebateForbid;

public interface UserRebateForbidMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UserRebateForbid record);

    int insertSelective(UserRebateForbid record);

    UserRebateForbid selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserRebateForbid record);

    int updateByPrimaryKey(UserRebateForbid record);
    
    UserRebateForbid getUserRebateForbidByUserId(Integer userId);
    
    /**
     * 根据业务系统查找所有返点禁止名单
     * @param bizSystem
     * @return
     */
    List<Integer> getUserRebateForbidByBizSystem (@Param("bizSystem")String bizSystem);
    
    /**
     * 查找所有返点禁止名单
     * @return
     */
    List<Integer> getAllUserRebateForbid ();
    
    int getAllUserRebateForbIdPageCount (@Param("bizSystem")String bizSystem, @Param("userName")String userName);
    
    /**
     * 分页查询返点禁止名单
     * @param bizSystem
     * @param userName
     * @param startIndex
     * @param pageSize
     * @return
     */
    List<UserRebateForbid> getAllUserRebateForbidPage (@Param("bizSystem")String bizSystem, @Param("userName")String userName,@Param("startNum")int startIndex, @Param("pageNum")Integer pageSize);

}