package com.team.lottery.mapper.riskrecord;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.RiskRecord;

public interface RiskRecordMapper {
    int deleteByPrimaryKey(Long id);

    int insert(RiskRecord record);

    int insertSelective(RiskRecord record);

    RiskRecord selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(RiskRecord record);

    int updateByPrimaryKey(RiskRecord record);

	List<RiskRecord> getAllRiskRecordByQueryPage(@Param("query")RiskRecord query, @Param("startNum")int startIndex, @Param("pageNum")Integer pageNum);

	Integer getAllRiskRecordByQueryPageCount(@Param("query")RiskRecord query);

    
}