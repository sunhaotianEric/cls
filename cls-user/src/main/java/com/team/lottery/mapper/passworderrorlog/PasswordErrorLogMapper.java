package com.team.lottery.mapper.passworderrorlog;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.PasswordErrorLogQuery;
import com.team.lottery.vo.PasswordErrorLog;

/**
 * 密码错误日志类Mapper.
 * @author jamine
 *
 */
public interface PasswordErrorLogMapper {
    
	int deleteByPrimaryKey(Long id);

    int insert(PasswordErrorLog record);

    int insertSelective(PasswordErrorLog record);

    PasswordErrorLog selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(PasswordErrorLog record);

    int updateByPrimaryKey(PasswordErrorLog record);
    
    /**
     * 查询密码错误日志数量.
     * @param query
     * @return
     */
	int getAllPasswordErrorLogByQueryPageCount(@Param("query")PasswordErrorLogQuery query);
	
	/**
	 * 解锁用户时，删除安全密码日志错误记录数(软删除).
	 * 
	 * @return
	 */
	void updatePasswordErrorLogByUserIdForSafePwd(Integer userId);
	
	/**
	 * 根据条件查询错误日志数据.
	 * @param query
	 * @return
	 */
	List<PasswordErrorLog> getAllPasswordErrorLogByQuery(@Param("query")PasswordErrorLogQuery query);
	
	/**
	 * 解锁用户时，删除密码日志错误记录数(软删除).
	 * 
	 * @return
	 */
	void updatePasswordErrorLogByUserIdForPwd(Integer userId);

	/**
	 * 删除30天之前的数据(硬删除).
	 * @param query
	 */
	void deleteByThirtyDaysBeforeData(@Param("query")PasswordErrorLogQuery query);

	/**
	 * 根据ip软删除数据.
	 * @param bizSystem
	 * @param userName
	 * @param requestIp
	 */
	void updatePasswordErrorLogByUserName(@Param("bizSystem")String bizSystem, @Param("userName")String userName);

	/**
	 * 根据用户ID删除安全密码错误日志.软删除()
	 * 
	 * @param userId
	 */
	void delPasswordErrorLogByUserIdForCheckSavePassWord(Integer userId);
}