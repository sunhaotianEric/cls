package com.team.lottery.mapper.userquotadetail;

import com.team.lottery.vo.UserQuotaDetail;


public interface UserQuotaDetailMapper {
    int deleteByPrimaryKey(Long id);

    int insert(UserQuotaDetail record);

    int insertSelective(UserQuotaDetail record);

    UserQuotaDetail selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UserQuotaDetail record);

    int updateByPrimaryKey(UserQuotaDetail record);
    
}