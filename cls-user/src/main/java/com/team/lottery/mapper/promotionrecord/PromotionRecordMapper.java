package com.team.lottery.mapper.promotionrecord;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.PromotionRecordQuery;
import com.team.lottery.vo.PromotionRecord;

public interface PromotionRecordMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(PromotionRecord record);

    int insertSelective(PromotionRecord record);

    PromotionRecord selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(PromotionRecord record);

    int updateByPrimaryKey(PromotionRecord record);
    
    /**
     * 查询符合条件的记录条数
     * @param query
     * @return
     */
    public Integer getAllPromotionRecordPageCount(PromotionRecordQuery query);
    
    /**
     * 分页条件查询登陆日志
     * @return
     */
    public List<PromotionRecord> getAllPromotionRecordPage(@Param("query")PromotionRecordQuery query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
    
    public List<PromotionRecord> getAllPromotionRecord(@Param("query")PromotionRecordQuery promotionRecordQuery);
    
    public List<PromotionRecord> getByAllPromotionRecordKey(@Param("promotionRecordId")List<Integer> promotionRecordId, @Param("userName")String userName);
}