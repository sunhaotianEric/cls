package com.team.lottery.system;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.enums.EMoneyDetailType;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.UserDayConsumeQuery;
import com.team.lottery.service.UserDayConsumeService;
import com.team.lottery.service.UserSelfDayConsumeService;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.RandomUtil;
import com.team.lottery.vo.UserDayConsume;
import com.team.lottery.vo.UserSelfDayConsume;

/**
 * 在线实时统计报表异常线程处理
 * @author Administrator
 *
 */
public class UserVersionExceptionConsumeDealThread extends Thread{
	private static Logger logger = LoggerFactory.getLogger(UserVersionExceptionConsumeDealThread.class);
	public static final int dayConsumeflag = 1;//团队
	public static final int selfDayConsumeflag = 2;//自身
	//重试次数
    private Integer retryConut = 0; 
    //处理更新版本号不一致最大重试次数
  	public static final Integer PROSTATE_DEAL_EXCEPTION = 10;
  	private String bizSystem;
	private String userName; 
	private Date belongDateTime;
	private String detailType;
	private BigDecimal operationValue;
	private int flag ;
	private UserDayConsumeService userDayConsumeService;
	private UserSelfDayConsumeService userSelfDayConsumeService;
	
	
	public UserVersionExceptionConsumeDealThread(String bizSystem,String userName,Date belongDateTime,String detailType,BigDecimal operationValue,int flag) {
	     this.bizSystem = bizSystem;
		 this.userName = userName;
		 this.belongDateTime = belongDateTime;
	     this.detailType = detailType;
	     this.operationValue = operationValue;
	     this.flag = flag;
	     if(dayConsumeflag == flag){
	    	 this.userDayConsumeService = ApplicationContextUtil.getBean("userDayConsumeService"); 
	     }else if(selfDayConsumeflag == flag){
	    	 this.userSelfDayConsumeService = ApplicationContextUtil.getBean("userSelfDayConsumeService");
	     }
	    
	}
	
	
	@Override
	public void run() {
	   this.excuteConsume(bizSystem, userName, belongDateTime, detailType, operationValue, flag);
	}
	
	private void excuteConsume(String bizSystem,String userName,Date belongDateTime,String detailType,BigDecimal operationValue,int flag){
		//当前线程休眠
		try {
			int sec = RandomUtil.getRandom(1, 9);
			Thread.sleep((2+sec) * 1000);
		} catch (InterruptedException e1) {
			//业务异常
			logger.error(e1.getMessage(), e1);
		}
		logger.info("进入版本号不一致"+(flag == 1?"团队":"自身")+"盈亏报表第"+(retryConut+1)+"次处理线程，"+bizSystem+"+用户["+userName+"]时，类型="+detailType+",金额="+operationValue);
	
		try {
			if(dayConsumeflag == flag){
			   UserDayConsumeQuery query = new UserDayConsumeQuery();
	    	   query.setUserName(userName);
	    	   query.setBizSystem(bizSystem);
	    	   query.setBelongDate(belongDateTime);
	    	   UserDayConsume reportVo = new UserDayConsume();
	    	   List<UserDayConsume> list = userDayConsumeService.getUserDayConsumeByCondition(query);
	    	   if(list.size()>0){
	    		    UserDayConsume userDayConsume = list.get(0);
	    		    reportVo.setId(userDayConsume.getId());
	    		    reportVo.setVersion(userDayConsume.getVersion());
   		    		if(detailType.equals(EMoneyDetailType.RECHARGE.name())){  //存款(充值)
   		   		    	reportVo.setRecharge(userDayConsume.getRecharge().add(operationValue));
   		   		    	//不能参与
   		   		    }else if(detailType.equals(EMoneyDetailType.RECHARGE_PRESENT.name())){ //充值赠送 充值赠送(系统扣费)
   		   		    	reportVo.setRechargepresent(userDayConsume.getRechargepresent().add(operationValue));
   		   		   
   		   		        //盈利计算
   		   		    	reportVo.setGain(userDayConsume.getGain().add(operationValue));
   		   		    }else if(detailType.equals(EMoneyDetailType.ACTIVITIES_MONEY.name())){ //活动彩金//活动彩金(系统扣费)
   		   		    	reportVo.setActivitiesmoney(userDayConsume.getActivitiesmoney().add(operationValue));	   		    	
   		   		        //盈利计算
   		   		    	reportVo.setGain(userDayConsume.getGain().add(operationValue));
   		   		    }else if(detailType.equals(EMoneyDetailType.WITHDRAW.name())){  //取款(在线取款)
   		   	   		    reportVo.setWithdraw(userDayConsume.getWithdraw().add(operationValue));
   		   		    	//不能参与
   		   		    }else if(detailType.equals(EMoneyDetailType.WITHDRAW_FEE.name())){  //取款手续费
   		   		    	reportVo.setWithdrawfee(userDayConsume.getWithdrawfee().add(operationValue));
   		   		 
   			   		    reportVo.setWithdraw(userDayConsume.getWithdraw().subtract(operationValue));
   		
   			   		    reportVo.setGain(userDayConsume.getGain().subtract(operationValue));
   		   		    }else if(detailType.equals(EMoneyDetailType.LOTTERY.name())){ //投注  
   		   		    	reportVo.setLottery(userDayConsume.getLottery().add(operationValue));
   		   		    	//盈利计算
   		   		    	reportVo.setGain(userDayConsume.getGain().subtract(operationValue));  
   		   		    }else if(detailType.equals(EMoneyDetailType.LOTTERY_STOP_AFTER_NUMBER.name())){  //撤单
   		   		    	reportVo.setLottery(userDayConsume.getLottery().subtract(operationValue));
   		   		    	reportVo.setLotteryStopAfterNumber(userDayConsume.getLotteryStopAfterNumber().add(operationValue));
   		   		    	//盈利计算
   		   		    	reportVo.setGain(userDayConsume.getGain().add(operationValue));
   		   		    }else if(detailType.equals(EMoneyDetailType.LOTTERY_REGRESSION.name())){  //管理员投注退单
   		   		    	reportVo.setLottery(userDayConsume.getLottery().subtract(operationValue));
   		   		    	reportVo.setLotteryRegression(userDayConsume.getLotteryRegression().add(operationValue));
   		   		    	//盈利计算
   		   		    	reportVo.setGain(userDayConsume.getGain().add(operationValue));
   		   		    }else if(detailType.equals(EMoneyDetailType.WIN.name())){  //中奖 
   		   		    	reportVo.setWin(userDayConsume.getWin().add(operationValue));
   		   		    	//盈利计算
   		   		    	reportVo.setGain(userDayConsume.getGain().add(operationValue));
   		   		    }else if(detailType.equals(EMoneyDetailType.REBATE.name())){  //返点
   		   		    	reportVo.setRebate(userDayConsume.getRebate().add(operationValue));
   		   		    	//盈利计算
   		   		    	reportVo.setGain(userDayConsume.getGain().add(operationValue));
   		   		    }else if(detailType.equals(EMoneyDetailType.PERCENTAGE.name())){  //提成
   		   		    	reportVo.setPercentage(userDayConsume.getPercentage().add(operationValue));
   		   		    	//盈利计算
   		   		    	reportVo.setGain(userDayConsume.getGain().add(operationValue));
   		   		    }else if(detailType.equals(EMoneyDetailType.HALF_MONTH_BOUNS.name())){  //半月分红
   		   		    	reportVo.setHalfmonthbonus(userDayConsume.getHalfmonthbonus().add(operationValue));
   		   		    }else if(detailType.equals(EMoneyDetailType.DAY_BOUNS.name())){  //日分红
   		   		    	reportVo.setDaybonus(userDayConsume.getDaybonus().add(operationValue));
   		   		    }else if(detailType.equals(EMoneyDetailType.DAY_SALARY.name())){  //日工资
   		   		    	reportVo.setDaysalary(userDayConsume.getDaysalary().add(operationValue));
   		   		    }else if(detailType.equals(EMoneyDetailType.MONTH_SALARY.name())){  //月工资
   		   		    	reportVo.setMonthsalary(userDayConsume.getMonthsalary().add(operationValue));
   		   		    }else if(detailType.equals(EMoneyDetailType.DAY_COMMISSION.name())){  //日佣金
   		   		    	reportVo.setDaycommission(userDayConsume.getDaycommission().add(operationValue));
   		   		    }else if(detailType.equals(EMoneyDetailType.PAY_TRANFER.name())){  //支出转账
   		   	    		reportVo.setPaytranfer(userDayConsume.getPaytranfer().add(operationValue));
   		   	    		//不能参与
   			   		    //reportVo.setGain(reportVo.getGain().subtract(moneyDetail.getPay()));
   		   		    }else if(detailType.equals(EMoneyDetailType.INCOME_TRANFER.name())){  //收入转账
   		   	    		reportVo.setIncometranfer(userDayConsume.getIncometranfer().add(operationValue));
   		   	    		//不能参与
   			   		    //reportVo.setGain(reportVo.getGain().add(moneyDetail.getIncome()));
   		   		    }else if(detailType.equals(EMoneyDetailType.SYSTEM_ADD_MONEY.name())){  //系统续费
   		   		    	reportVo.setSystemaddmoney(userDayConsume.getSystemaddmoney().add(operationValue));
   		   		    	//盈利计算
   		   		    	reportVo.setGain(userDayConsume.getGain().add(operationValue));
   		   		    }else if(detailType.equals(EMoneyDetailType.SYSTEM_REDUCE_MONEY.name())){  //系统扣费
   		   		    	reportVo.setSystemreducemoney(userDayConsume.getSystemreducemoney().add(operationValue));
   		   		    	//盈利计算
   		   		    	reportVo.setGain(userDayConsume.getGain().subtract(operationValue));
   		   		    }else if(detailType.equals(ConstantUtil.LOTTERY_EFFECTIVE)){  //有效投注额，特别处理
   		   		    	reportVo.setLotteryEffective(userDayConsume.getLotteryEffective().add(operationValue));
   		   		    }else{
   		   		    	throw new RuntimeException("该资金明细类型不可知.");
   		   		    }
   		    	   
   		    	    userDayConsumeService.updateByPrimaryKeyWithVersionSelective(reportVo);
	    	   }
	    	   
			}else if(selfDayConsumeflag == flag){
		    	   UserDayConsumeQuery query = new UserDayConsumeQuery();
		    	   query.setUserName(userName);
		    	   query.setBizSystem(bizSystem);
		    	   query.setBelongDate(belongDateTime);
		    	   UserSelfDayConsume reportVo = new UserSelfDayConsume();
		    	   UserSelfDayConsume userSelfDayConsume = userSelfDayConsumeService.getUserSelfDayConsumeByCondition(query);
		    	   if(userSelfDayConsume != null){
		    		    reportVo.setId(userSelfDayConsume.getId());
		    		    reportVo.setVersion(userSelfDayConsume.getVersion());
	   		    		if(detailType.equals(EMoneyDetailType.RECHARGE.name())){  //存款(充值)
	   		   		    	reportVo.setRecharge(userSelfDayConsume.getRecharge().add(operationValue));
	   		   		    	//不能参与
	   		   		    }else if(detailType.equals(EMoneyDetailType.RECHARGE_PRESENT.name())){ //充值赠送 充值赠送(系统扣费)
	   		   		    	reportVo.setRechargepresent(userSelfDayConsume.getRechargepresent().add(operationValue));
	   		   		   
	   		   		        //盈利计算
	   		   		    	reportVo.setGain(userSelfDayConsume.getGain().add(operationValue));
	   		   		    }else if(detailType.equals(EMoneyDetailType.ACTIVITIES_MONEY.name())){ //活动彩金//活动彩金(系统扣费)
	   		   		    	reportVo.setActivitiesmoney(userSelfDayConsume.getActivitiesmoney().add(operationValue));	   		    	
	   		   		        //盈利计算
	   		   		    	reportVo.setGain(userSelfDayConsume.getGain().add(operationValue));
	   		   		    }else if(detailType.equals(EMoneyDetailType.WITHDRAW.name())){  //取款(在线取款)
	   		   	   		    reportVo.setWithdraw(userSelfDayConsume.getWithdraw().add(operationValue));
	   		   		    	//不能参与
	   		   		    }else if(detailType.equals(EMoneyDetailType.WITHDRAW_FEE.name())){  //取款手续费
	   		   		    	reportVo.setWithdrawfee(userSelfDayConsume.getWithdrawfee().add(operationValue));
	   		   		 
	   			   		    reportVo.setWithdraw(userSelfDayConsume.getWithdraw().subtract(operationValue));
	   		
	   			   		    reportVo.setGain(userSelfDayConsume.getGain().subtract(operationValue));
	   		   		    }else if(detailType.equals(EMoneyDetailType.LOTTERY.name())){ //投注  
	   		   		    	reportVo.setLottery(userSelfDayConsume.getLottery().add(operationValue));
	   		   		    	//盈利计算
	   		   		    	reportVo.setGain(userSelfDayConsume.getGain().subtract(operationValue));  
	   		   		    }else if(detailType.equals(EMoneyDetailType.LOTTERY_STOP_AFTER_NUMBER.name())){  //撤单
	   		   		    	reportVo.setLottery(userSelfDayConsume.getLottery().subtract(operationValue));
	   		   		    	reportVo.setLotteryStopAfterNumber(userSelfDayConsume.getLotteryStopAfterNumber().add(operationValue));
	   		   		    	//盈利计算
	   		   		    	reportVo.setGain(userSelfDayConsume.getGain().add(operationValue));
	   		   		    }else if(detailType.equals(EMoneyDetailType.LOTTERY_REGRESSION.name())){  //管理员投注退单
	   		   		    	reportVo.setLottery(userSelfDayConsume.getLottery().subtract(operationValue));
	   		   		    	reportVo.setLotteryRegression(userSelfDayConsume.getLotteryRegression().add(operationValue));
	   		   		    	//盈利计算
	   		   		    	reportVo.setGain(userSelfDayConsume.getGain().add(operationValue));
	   		   		    }else if(detailType.equals(EMoneyDetailType.WIN.name())){  //中奖 
	   		   		    	reportVo.setWin(userSelfDayConsume.getWin().add(operationValue));
	   		   		    	//盈利计算
	   		   		    	reportVo.setGain(userSelfDayConsume.getGain().add(operationValue));
	   		   		    }else if(detailType.equals(EMoneyDetailType.REBATE.name())){  //返点
	   		   		    	reportVo.setRebate(userSelfDayConsume.getRebate().add(operationValue));
	   		   		    	//盈利计算
	   		   		    	reportVo.setGain(userSelfDayConsume.getGain().add(operationValue));
	   		   		    }else if(detailType.equals(EMoneyDetailType.PERCENTAGE.name())){  //提成
	   		   		    	reportVo.setPercentage(userSelfDayConsume.getPercentage().add(operationValue));
	   		   		    	//盈利计算
	   		   		    	reportVo.setGain(userSelfDayConsume.getGain().add(operationValue));
	   		   		    }else if(detailType.equals(EMoneyDetailType.HALF_MONTH_BOUNS.name())){  //半月分红
	   		   		    	reportVo.setHalfmonthbonus(userSelfDayConsume.getHalfmonthbonus().add(operationValue));
	   		   		    }else if(detailType.equals(EMoneyDetailType.DAY_BOUNS.name())){  //日分红
	   		   		    	reportVo.setDaybonus(userSelfDayConsume.getDaybonus().add(operationValue));
	   		   		    }else if(detailType.equals(EMoneyDetailType.DAY_SALARY.name())){  //日工资
	   		   		    	reportVo.setDaysalary(userSelfDayConsume.getDaysalary().add(operationValue));
	   		   		    }else if(detailType.equals(EMoneyDetailType.MONTH_SALARY.name())){  //月工资
	   		   		    	reportVo.setMonthsalary(userSelfDayConsume.getMonthsalary().add(operationValue));
	   		   		    }else if(detailType.equals(EMoneyDetailType.DAY_COMMISSION.name())){  //日佣金
	   		   		    	reportVo.setDaycommission(userSelfDayConsume.getDaycommission().add(operationValue));
	   		   		    }else if(detailType.equals(EMoneyDetailType.PAY_TRANFER.name())){  //支出转账
	   		   	    		reportVo.setPaytranfer(userSelfDayConsume.getPaytranfer().add(operationValue));
	   		   	    		//不能参与
	   			   		    //reportVo.setGain(reportVo.getGain().subtract(moneyDetail.getPay()));
	   		   		    }else if(detailType.equals(EMoneyDetailType.INCOME_TRANFER.name())){  //收入转账
	   		   	    		reportVo.setIncometranfer(userSelfDayConsume.getIncometranfer().add(operationValue));
	   		   	    		//不能参与
	   			   		    //reportVo.setGain(reportVo.getGain().add(moneyDetail.getIncome()));
	   		   		    }else if(detailType.equals(EMoneyDetailType.SYSTEM_ADD_MONEY.name())){  //系统续费
	   		   		    	reportVo.setSystemaddmoney(userSelfDayConsume.getSystemaddmoney().add(operationValue));
	   		   		    	//盈利计算
	   		   		    	reportVo.setGain(userSelfDayConsume.getGain().add(operationValue));
	   		   		    }else if(detailType.equals(EMoneyDetailType.SYSTEM_REDUCE_MONEY.name())){  //系统扣费
	   		   		    	reportVo.setSystemreducemoney(userSelfDayConsume.getSystemreducemoney().add(operationValue));
	   		   		    	//盈利计算
	   		   		    	reportVo.setGain(userSelfDayConsume.getGain().subtract(operationValue));
	   		   		    }else if(detailType.equals(ConstantUtil.LOTTERY_EFFECTIVE)){  //有效投注额，特别处理
	   		   		    	reportVo.setLotteryEffective(userSelfDayConsume.getLotteryEffective().add(operationValue));
	   		   		    }else{
	   		   		    	throw new RuntimeException("该资金明细类型不可知.");
	   		   		    }
	   		    	
					   userSelfDayConsumeService.updateByPrimaryKeyWithVersionSelective(reportVo);
		    	   }
			}
		} catch (UnEqualVersionException e) {
			if(retryConut < PROSTATE_DEAL_EXCEPTION){
				retryConut ++;
				 this.excuteConsume(bizSystem, userName, belongDateTime, detailType, operationValue, flag);
			}else{
				logger.error("发现版本号不一致，"+(flag == 1?"团队":"自身")+"盈亏报表统计,重试次数超出"+PROSTATE_DEAL_EXCEPTION+"次，终止重试进程");
				return;
			}
			
		} catch (Exception e) {
			logger.error("UserVersionExceptionConsumeDealThread调用出现异常", e);
			return;
		}
	}

	
}
