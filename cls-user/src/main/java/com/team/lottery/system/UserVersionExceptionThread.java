package com.team.lottery.system;

import java.math.BigDecimal;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.service.MoneyDetailService;
import com.team.lottery.service.UserService;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.util.BeanUtilsExtends;
import com.team.lottery.vo.MoneyDetail;
import com.team.lottery.vo.User;

/**
 * 用户表版本号不一致重试线程
 * 注：使用此类处理资金余额时，必须先更新余额在插入资金明细，插入资金明细必须要在更新余额成功后执行
 * @author luocheng
 *
 */
public class UserVersionExceptionThread extends Thread{
	
	private static Logger logger = LoggerFactory.getLogger(UserVersionExceptionThread.class);

	//重试次数
    private Integer retryConut = 0; 
    
    //处理更新版本号不一致最大重试次数
  	public static final Integer PROSTATE_DEAL_EXCEPTION = 5;
  	
  	public static final String ADD_TYPE = "ADD";
  	public static final String REDUCE_TYPE = "REDUCE";
    
    private Integer userId;
    private BigDecimal money;
    private String type;
    private boolean isDealCanWithdraw;
    private MoneyDetail moneyDetail;
    
    private UserService userService;
    private MoneyDetailService moneyDetailService;

	public UserVersionExceptionThread(Integer userId, BigDecimal money, String type, boolean isDealCanWithdraw, MoneyDetail moneyDetail) {
		super();
		this.userId = userId;
		this.money = money;
		this.type = type;
		this.isDealCanWithdraw = isDealCanWithdraw;
		this.moneyDetail = moneyDetail;
		if(this.userService == null) {
			this.userService = ApplicationContextUtil.getBean(UserService.class);
		}
		if(this.moneyDetailService == null) {
			this.moneyDetailService = ApplicationContextUtil.getBean(MoneyDetailService.class);
		}
	}
	
	@Override
	public void run() {
		this.excuteDeal(userId, money, type, isDealCanWithdraw);
	}
	
	/**
	 * 再次异常则递归再调用处理
	 * @param userId
	 * @param money
	 * @param type
	 * @param isDealCanWithdraw
	 */
	private void excuteDeal(Integer userId, BigDecimal money, String type, boolean isDealCanWithdraw) {
		//当前线程休眠
		try {
			//相同用户同时进入线程前后顺序会乱
//			int sec = RandomUtil.getRandom(1, 6);
//			Thread.sleep((2+sec) * 500);
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			//业务异常
			logger.error(e1.getMessage(), e1);
		}
		User dealUser = userService.selectByPrimaryKey(userId);
		logger.info("进入用户表版本号不一致第[{}]次处理线程，业务系统[{}],用户名[{}]，用户id[{}]，类型[{}],金额[{}],可提现处理[{}]", 
				retryConut+1, dealUser.getBizSystem(), dealUser.getUserName(), userId, type, money, isDealCanWithdraw);
		if(ADD_TYPE.equals(type)) {
			dealUser.addMoney(money, isDealCanWithdraw);
		} else {
			dealUser.reduceMoney(money, isDealCanWithdraw);
		}
		//当前的资金余额
		BigDecimal currentMoney = dealUser.getMoney();
		int res = 0;
		try {
			res = userService.updateByPrimaryKeyWithVersionSelective(dealUser);
		} catch (UnEqualVersionException exception) {
			if(retryConut < PROSTATE_DEAL_EXCEPTION){
				retryConut ++;
				this.excuteDeal(userId, money, type, isDealCanWithdraw);
			}else{
				logger.error("发现用户表版本号不一致，业务系统[{}],用户名[{}]，用户id[{}]，类型[{}],金额[{}],可提现处理[{}],重试次数超出"+PROSTATE_DEAL_EXCEPTION+"次，终止重试进程",
						dealUser.getBizSystem(), dealUser.getUserName(), userId, type, money, isDealCanWithdraw);
				return;
			}
		} catch (Exception e) {
			logger.error("UserVersionExceptionThread调用出现异常", e);
			return;
		}
		//更新成功才插入资金明细
		if(res == 1) {
			MoneyDetail newMoneyDetail = new MoneyDetail();
			BeanUtilsExtends.copyProperties(newMoneyDetail, moneyDetail);
			//重新定义更新资金前后金额
			newMoneyDetail.setBalanceBefore(currentMoney);
			newMoneyDetail.setBalanceAfter(dealUser.getMoney());
			newMoneyDetail.setCreatedDate(new Date());
			moneyDetailService.insertSelective(newMoneyDetail);
		}
		
	}
    
    
}
