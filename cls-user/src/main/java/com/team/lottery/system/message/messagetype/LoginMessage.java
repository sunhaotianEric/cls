package com.team.lottery.system.message.messagetype;

import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.handler.LoginMessageHandler;

/**
 * 用户登录消息对象
 * @author luocheng
 *
 */
public class LoginMessage extends BaseMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8782533829248615014L;
	private String userName;
	private String bizSystem;
	private String ip;
	//登录记录的ID
	private Integer loginId;
	private String loginType;
	private String sessionId;
	private String unusualLogin;
	private Integer userId;
	
	
	
	public LoginMessage() {
		this.handler = new LoginMessageHandler();
	}
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}

	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public Integer getLoginId() {
		return loginId;
	}
	public void setLoginId(Integer loginId) {
		this.loginId = loginId;
	}
	public String getLoginType() {
		return loginType;
	}
	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getUnusualLogin() {
		return unusualLogin;
	}

	public void setUnusualLogin(String unusualLogin) {
		this.unusualLogin = unusualLogin;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
}
