package com.team.lottery.system.message.handler;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.team.lottery.enums.ERiskRecordType;
import com.team.lottery.extvo.LoginLogQuery;
import com.team.lottery.service.LoginLogService;
import com.team.lottery.service.RiksRecordService;
import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.BaseMessageHandler;
import com.team.lottery.system.message.messagetype.LoginMessage;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.util.IPUtil;
import com.team.lottery.vo.LoginLog;
import com.team.lottery.vo.RiskRecord;

public class LoginMessageHandler extends BaseMessageHandler {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5269966949190255216L;
	private static Logger logger = LoggerFactory.getLogger(LoginMessageHandler.class);

	@Override
	public void dealMessage(BaseMessage message) {
		try {
			LoginMessage loginMessage = (LoginMessage) message;
			LoginLogService loginService = ApplicationContextUtil.getBean("loginLogService");
			RiksRecordService riksRecordService = ApplicationContextUtil.getBean("riksRecordService");
			Integer loginId = loginMessage.getLoginId();
			String ip = loginMessage.getIp();
			String userName = loginMessage.getUserName();
			String bizSystem = loginMessage.getBizSystem();
			String loginType = loginMessage.getLoginType();
			
			
			// 当前线程休眠0.5秒，保证获取到login记录
			try {
				Thread.sleep(500);
			} catch (Exception e) {
				logger.error("登陆消息线程休眠发生错误...", e);
			}
			LoginLog loginLog = loginService.selectByPrimaryKey(loginId);

			String address ="未知";//IPUtil.getAddressesPcOnline(ip);
			logger.info("业务系统[" + bizSystem + "],用户[" + userName + "]进行登录，ip[" + ip + "],登陆方式[" + loginType + "],登录地点[" + address + "]");
			if (loginLog == null) {
				logger.info("未找到该用户登录记录");
				return;
			}
			loginLog.setAddress(address);
			loginService.updateByPrimaryKeySelective(loginLog);
			
			
			LoginLogQuery loginLogQuery = new LoginLogQuery();
			Integer userId = loginMessage.getUserId();
			loginLogQuery.setUserId(userId);
			loginLogQuery.setLogip(ip);
			loginLogQuery.setBizSystem(bizSystem);

			//查询登录日志表
			List<LoginLog> unusualLogList = loginService.getLoginLogsByUnusual(loginLogQuery);
			Map<Integer, String> map = new HashMap<Integer, String>();
			if (unusualLogList != null && unusualLogList.size() > 0) {
				for (LoginLog log : unusualLogList) {
					map.put(log.getUserId(), log.getUserName());
				}
			}
			if (map.size() > 0) {
				String userIdStr = "";
				String userNameStr = "";

				Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();
				while (it.hasNext()) {
					Map.Entry<Integer, String> entry = it.next();
					userIdStr += entry.getKey() + ",";
					userNameStr += entry.getValue() + ",";

				}

				//将相关信息写入队列
				userIdStr = userIdStr.substring(0, userIdStr.length() - 1);
				userNameStr = userNameStr.substring(0, userNameStr.length() - 1);
				loginMessage.setUnusualLogin("用户[" + userName + "]与用户[" + userNameStr + "]登录相同的IP地址");
				logger.info("用户[" + userName + "]与用户[" + userNameStr + "]登录相同的IP地址!"+"相关联的用户userId:"+userIdStr);
				//保存到风控记录表
				RiskRecord record = new RiskRecord();
				record.setCreateDate(new Date());
				record.setBizSystem(bizSystem);
				record.setUserId(userId);
				record.setUserName(userName);
				record.setRecordType(ERiskRecordType.USER_LOGIN_IP_UNUSUAL.name());
				record.setRecordDesc("用户[" + userName + "]与用户[" + userNameStr + "]登录相同的IP地址");
				record.setRelationUserId(userIdStr);
				record.setRelationUserName(userNameStr);
				riksRecordService.insert(record);
			}
			
		} catch (Exception e) {
			logger.error("处理登陆消息发生错误...", e);
		}
	}

}
