package com.team.lottery.system.message.handler;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.team.lottery.enums.ERiskRecordType;
import com.team.lottery.extvo.UserBankQueryVo;
import com.team.lottery.service.RiksRecordService;
import com.team.lottery.service.UserBankService;
import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.BaseMessageHandler;
import com.team.lottery.system.message.messagetype.BankCardMessage;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.vo.RiskRecord;
import com.team.lottery.vo.UserBank;

public class BankCardMessageHandler extends BaseMessageHandler {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5269966949190255216L;
	private static Logger logger = LoggerFactory.getLogger(BankCardMessageHandler.class);

	@Override
	public void dealMessage(BaseMessage message) {
		try {
			BankCardMessage bankCardMessage = (BankCardMessage) message;
			UserBankService userBankService = ApplicationContextUtil.getBean("userBankService");
			RiksRecordService riksRecordService = ApplicationContextUtil.getBean("riksRecordService");
			String userName = bankCardMessage.getUserName();
			String bizSystem = bankCardMessage.getBizSystem();
			Integer userId = bankCardMessage.getUserId();
			String bankAccountName = bankCardMessage.getBankAccountName();
			String bankCardNum = bankCardMessage.getBankCardNum();
            
			//查询用户银行卡信息表
			UserBankQueryVo queryUser = new UserBankQueryVo();
			queryUser.setBankAccountName(bankAccountName);
			queryUser.setUserId(userId);
			List<UserBank> list = userBankService.queryOtherNameBanks(queryUser);
			String userIdStr = "";
			String userNameStr = "";
			Map<Integer, String> map = new HashMap<Integer, String>();
			if (list != null && list.size() > 0) {
				for (UserBank bank : list) {
					map.put(bank.getUserId(), bank.getUserName());
				}
			}
			if (map.size() > 0) {
				Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();
				while (it.hasNext()) {
					Map.Entry<Integer, String> entry = it.next();
					userIdStr += entry.getKey() + ",";
					userNameStr += entry.getValue() + ",";
				}
				
				//将相关信息写入队列
				userIdStr = userIdStr.substring(0, userIdStr.length() - 1);
				userNameStr = userNameStr.substring(0, userNameStr.length() - 1);
				bankCardMessage.setRelationUserId(userIdStr);
				bankCardMessage.setRelationUserName(userNameStr);
				bankCardMessage.setUnusualBankCard("用户[" + userName + "]与用户[" + userNameStr + "]银行卡姓名同名");
				
				//保存到风控记录表
				RiskRecord record = new RiskRecord();
				record.setCreateDate(new Date());
				record.setBizSystem(bizSystem);
				record.setUserId(userId);
				record.setUserName(userName);
				record.setRecordType(ERiskRecordType.BANK_CARD_SAME_NAME.name());
				record.setRecordDesc("用户[" + userName + "]与用户[" + userNameStr + "]银行卡姓名同名");
				record.setRelationUserId(userIdStr);
				record.setRelationUserName(userNameStr);
				record.setCreateDate(new Date());
				riksRecordService.insertSelective(record);
			}
			// 当前线程休眠0.5秒，保证获取到login记录
			try {
				Thread.sleep(500);
			} catch (Exception e) {
				logger.error("银行卡用户消息线程休眠发生错误...", e);
			}

			logger.info("业务系统[" + bizSystem + "],用户[" + userName + "]与用户[" + userNameStr + "],使用相同的开户人姓名[" + bankAccountName
					+ "]的银行卡,银行卡号[" + bankCardNum + "]");
		} catch (Exception e) {
			logger.error("处理银行卡用户消息发生错误...", e);
		}
	}

}
