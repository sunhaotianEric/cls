package com.team.lottery.system.message.messagetype;

import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.handler.BankCardMessageHandler;

/**
 * 用户银行卡信息消息对象
 * 
 * @author luocheng
 *
 */
public class BankCardMessage extends BaseMessage {
	private static final long serialVersionUID = 8782533829248615014L;
	private String userName;
	private String bizSystem;
	private Integer userId;
	private String bankType;
	private String bankName;
	private String relationUserName;
	private String relationUserId;
	private String bankAccountName;
	private String bankCardNum;
	private String unusualBankCard;

	public BankCardMessage() {
		this.handler = new BankCardMessageHandler();
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getBankType() {
		return bankType;
	}

	public void setBankType(String bankType) {
		this.bankType = bankType;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getRelationUserName() {
		return relationUserName;
	}

	public void setRelationUserName(String relationUserName) {
		this.relationUserName = relationUserName;
	}

	public String getRelationUserId() {
		return relationUserId;
	}

	public void setRelationUserId(String relationUserId) {
		this.relationUserId = relationUserId;
	}

	public String getBankAccountName() {
		return bankAccountName;
	}

	public void setBankAccountName(String bankAccountName) {
		this.bankAccountName = bankAccountName;
	}

	public String getBankCardNum() {
		return bankCardNum;
	}

	public void setBankCardNum(String bankCardNum) {
		this.bankCardNum = bankCardNum;
	}

	public String getUnusualBankCard() {
		return unusualBankCard;
	}

	public void setUnusualBankCard(String unusualBankCard) {
		this.unusualBankCard = unusualBankCard;
	}

}
