package com.team.lottery.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.util.DateUtil;

public class PromotionRecord {
	private Integer id;

	private String bizSystem;

	private Integer userId;

	private String userName;

	private String level;

	private String levelAfter;

	private Integer status;

	private BigDecimal money;

	private Date createDate;

	private Date updateDate;
	
	private Date logtimeStart;
    
    private Date logtimeEnd;

	public Date getLogtimeStart() {
		return logtimeStart;
	}

	public void setLogtimeStart(Date logtimeStart) {
		this.logtimeStart = logtimeStart;
	}

	public Date getLogtimeEnd() {
		return logtimeEnd;
	}

	public void setLogtimeEnd(Date logtimeEnd) {
		this.logtimeEnd = logtimeEnd;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem == null ? null : bizSystem.trim();
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName == null ? null : userName.trim();
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level == null ? null : level.trim();
	}

	public String getLevelAfter() {
		return levelAfter;
	}

	public void setLevelAfter(String levelAfter) {
		this.levelAfter = levelAfter == null ? null : levelAfter.trim();
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getCreateDateStr() {
		String dateStr = "";
		if (this.createDate != null) {
			dateStr = DateUtil.parseDate(createDate, "yyyy-MM-dd HH:mm:ss");
		}
		return dateStr;
	}

	public String getUpdateDateStr() {
		String dateStr = "";
		if (this.updateDate != null) {
			dateStr = DateUtil.parseDate(updateDate, "yyyy-MM-dd HH:mm:ss");
		}
		return dateStr;
	}

	public String getBizSystemName() {
		return this.bizSystem == null ? "" : ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
	}
}