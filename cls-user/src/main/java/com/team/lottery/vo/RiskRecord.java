package com.team.lottery.vo;

import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.enums.EConsumeActivityType;
import com.team.lottery.enums.ERiskRecordType;

public class RiskRecord {
    private Long id;

    private String bizSystem;

    private Integer userId;

    private String userName;

    private String relationUserId;

    private String relationUserName;

    private String recordDesc;

    private String recordType;

    private Date createDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getRelationUserId() {
        return relationUserId;
    }

    public void setRelationUserId(String relationUserId) {
        this.relationUserId = relationUserId;
    }

    public String getRelationUserName() {
        return relationUserName;
    }

    public void setRelationUserName(String relationUserName) {
        this.relationUserName = relationUserName == null ? null : relationUserName.trim();
    }

    public String getRecordDesc() {
        return recordDesc;
    }

    public void setRecordDesc(String recordDesc) {
        this.recordDesc = recordDesc == null ? null : recordDesc.trim();
    }

    public String getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType == null ? null : recordType.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
    public String getBizSystemName() {
		 return this.bizSystem==null?"":ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
 }
    /**
     * 第三方充值类型
     * @return
     */
    public String getRecordTypeStr(){
    	if(this.getRecordType() != null){
    		String recordType = this.getRecordType();
    		ERiskRecordType[] eRiskRecordType = ERiskRecordType.values();
    		for(ERiskRecordType type : eRiskRecordType){
    			if(recordType.equals(type.name())){
    				return type.getDescription();
    			}
    		}
    		
    	}
		return "";
    }
}