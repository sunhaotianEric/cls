package com.team.lottery.vo;

import java.util.Date;

/**
 * 前台用户登陆错误密码日志(安全密码和登陆密码)
 * 
 * @author jamine
 *
 */
public class PasswordErrorLog {
	// 唯一ID.
	private Long id;
	// 系统.
	private String bizSystem;
	// 用户ID.
	private Integer userId;
	// 用户名.
	private String userName;
	// 错误类型.(安全密码错误,登陆密码错误).
	private String errorType;
	// 来源类型.
	private String sourceType;
	// 请求者IP.
	private String loginIp;
	// 错误类型描述.
	private String sourceDes;
	// 创建日期.
	private Date createdDate;
	// 更新日期.
	private Date updateDate;
	// 是否删除.(0未删除,1是删除).
	private Integer isDeleted;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem == null ? null : bizSystem.trim();
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName == null ? null : userName.trim();
	}

	public String getErrorType() {
		return errorType;
	}

	public void setErrorType(String errorType) {
		this.errorType = errorType == null ? null : errorType.trim();
	}

	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType == null ? null : sourceType.trim();
	}

	public String getLoginIp() {
		return loginIp;
	}

	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp == null ? null : loginIp.trim();
	}

	public String getSourceDes() {
		return sourceDes;
	}

	public void setSourceDes(String sourceDes) {
		this.sourceDes = sourceDes == null ? null : sourceDes.trim();
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Integer getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}
}