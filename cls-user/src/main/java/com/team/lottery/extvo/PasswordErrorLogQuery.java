package com.team.lottery.extvo;

import java.util.Date;

/**
 * 密码错误日志查询对象.
 * 
 * @author jamine
 *
 */
public class PasswordErrorLogQuery {

	// 系统.
	private String bizSystem;
	// 用户ID.
	private Integer userId;
	// 用户名.
	private String userName;
	// 来源类型.
	private String sourceType;
	// 起始时间
	private Date createdDateStart;
	// 结束时间
	private Date createdDateEnd;
	// 请求者IP.
	private String loginIp;
	// 错误类型.(安全密码错误,登陆密码错误).
	private String errorType;
	// 删除状态.(0是为删除,1是已经删除)
	private Integer isDeleted;

	public Integer getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getErrorType() {
		return errorType;
	}

	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	public Date getCreatedDateStart() {
		return createdDateStart;
	}

	public void setCreatedDateStart(Date createdDateStart) {
		this.createdDateStart = createdDateStart;
	}

	public Date getCreatedDateEnd() {
		return createdDateEnd;
	}

	public void setCreatedDateEnd(Date createdDateEnd) {
		this.createdDateEnd = createdDateEnd;
	}

	public String getLoginIp() {
		return loginIp;
	}

	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}

}
