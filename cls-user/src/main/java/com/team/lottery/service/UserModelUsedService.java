package com.team.lottery.service;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.mapper.usermodelused.UserModelUsedMapper;
import com.team.lottery.vo.UserModelUsed;

@Service("userModelUsedService")
public class UserModelUsedService {

	@Autowired
	private UserModelUsedMapper userModelUsedMapper;
	
	public int deleteByPrimaryKey(Long id) {
		return userModelUsedMapper.deleteByPrimaryKey(id);
	}
	
	public int insertSelective(UserModelUsed record) {
		return userModelUsedMapper.insertSelective(record);
	}
	
	public int updateByPrimaryKeySelective(UserModelUsed record) {
		return userModelUsedMapper.updateByPrimaryKeySelective(record);
	}
	
	public UserModelUsed selectByPrimaryKey(Long id) {
		return userModelUsedMapper.selectByPrimaryKey(id);
	}
	
	/**
	 * 根据用户的Id来查找对应的记录
	 * @param id
	 * @return
	 */
	public UserModelUsed selectByUserId(Integer id, Date belongDate) {
		return userModelUsedMapper.selectByUserId(id, belongDate);
	}
    
	/**
	 * 根据用户名来查找对应的记录
	 * @param userName
	 * @param belongDate
	 * @return
	 */
    public UserModelUsed selectByUserName(String userName, Date belongDate) {
    	return userModelUsedMapper.selectByUserName(userName, belongDate);
    }
}
