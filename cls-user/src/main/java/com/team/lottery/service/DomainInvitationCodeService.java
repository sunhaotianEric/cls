package com.team.lottery.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.domaininvitationcode.DomainInvitationCodeMapper;
import com.team.lottery.vo.DomainInvitationCode;


@Service("domainInvitationCodeService")
public class DomainInvitationCodeService {

	@Autowired
	private DomainInvitationCodeMapper domainInvitationCodeMapper;

	public int deleteByPrimaryKey(Long id){
		return domainInvitationCodeMapper.deleteByPrimaryKey(id);
	}

	public int insert(DomainInvitationCode record){
		return domainInvitationCodeMapper.insert(record);
	}

	public int insertSelective(DomainInvitationCode record){
		return domainInvitationCodeMapper.insertSelective(record);
	}

	public DomainInvitationCode selectByPrimaryKey(Long id){
		return domainInvitationCodeMapper.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(DomainInvitationCode record){
		return domainInvitationCodeMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(DomainInvitationCode record){
		return domainInvitationCodeMapper.updateByPrimaryKey(record);
	}
	
	public Page getAllDomainInvitationCode(DomainInvitationCode record,Page page){
		page.setPageContent(domainInvitationCodeMapper.getAllDomainInvitationCodePage(record,page.getStartIndex(),page.getPageSize()));
		page.setTotalRecNum(domainInvitationCodeMapper.getAllDomainInvitationCodePageCount(record));
		return page;
	}


	
    

    /**
     * 查找对应的设置,并且锁定
     * @param id
     * @return
     */
    public DomainInvitationCode selectByPrimaryKeyForUpdate(Long id){
    	return domainInvitationCodeMapper.selectByPrimaryKeyForUpdate(id);
    }

	public void updateDomainInvitationCodeEnabled(Long id, int i) {
		// TODO Auto-generated method stub
		DomainInvitationCode domainInvitationCode = this.selectByPrimaryKeyForUpdate(id);
		domainInvitationCode.setEnable(i);
    	this.updateByPrimaryKeySelective(domainInvitationCode);
	}
    
	 /**
     * 查找对应的设置,并且锁定
     * @param id
     * @return
     */
    public DomainInvitationCode selectDomainInvitationCode(DomainInvitationCode record){
    	return domainInvitationCodeMapper.selectDomainInvitationCode(record);
    }


}
