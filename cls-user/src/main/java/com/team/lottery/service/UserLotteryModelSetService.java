package com.team.lottery.service;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.mapper.userlotterymodelset.UserLotteryModelSetMapper;
import com.team.lottery.vo.UserLotteryModelSet;

@Service("userLotteryModelSetService")
public class UserLotteryModelSetService {

	@Autowired
	private UserLotteryModelSetMapper userLotteryModelSetMapper;
	
	public int deleteByPrimaryKey(Long id) {
		return userLotteryModelSetMapper.deleteByPrimaryKey(id);
	}

    public int insertSelective(UserLotteryModelSet record) {
    	return userLotteryModelSetMapper.insertSelective(record);
    }

    public UserLotteryModelSet selectByPrimaryKey(Long id) {
    	return userLotteryModelSetMapper.selectByPrimaryKey(id);
    }

    public int updateByPrimaryKeySelective(UserLotteryModelSet record) {
    	return userLotteryModelSetMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * 根据用户ID获取用户投注模式设置
     * @param userName
     * @return
     */
    public UserLotteryModelSet getUserLotteryModelSetByUserId(Integer userId) {
    	List<UserLotteryModelSet> sets = userLotteryModelSetMapper.getUserLotteryModelSetByUserId(userId);
    	UserLotteryModelSet set = null;
    	if(CollectionUtils.isNotEmpty(sets)) {
    		//默认取第一条记录
    		set = sets.get(0);
    	}
    	return set;
    }
}
