package com.team.lottery.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.enums.EUserDailLiLevel;
import com.team.lottery.mapper.userquota.UserQuotaMapper;
import com.team.lottery.util.UserNameUtil;
import com.team.lottery.vo.QuotaControl;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserQuota;
import com.team.lottery.vo.UserQuotaDetail;

@Service("userQuotaService")
public class UserQuotaService {
	
	private static Logger logger = LoggerFactory.getLogger(UserQuotaService.class);

	@Autowired
	private UserQuotaMapper userQuotaMapper;
	@Autowired
	private UserQuotaDetailService userQuotaDetailService;
	@Autowired
	private UserService userService;
	@Autowired
	private QuotaControlService quotaControlService;
	
	@Transactional(readOnly=false,rollbackFor=Exception.class)
	public int deleteByPrimaryKey(Long id) {
		return userQuotaMapper.deleteByPrimaryKey(id);
	}

	public int insert(UserQuota record) {
		return userQuotaMapper.insert(record);
	}

	public int insertSelective(UserQuota record) {
		return userQuotaMapper.insertSelective(record);
	}

	public UserQuota selectByPrimaryKey(Long id) {
		return userQuotaMapper.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(UserQuota record) {
		return userQuotaMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(UserQuota record) {
		return userQuotaMapper.updateByPrimaryKey(record);
	}
	
	/**
     * 根据用户名得到用户配额信息
     * @param userName
     * @return
     */
	public List<UserQuota> getQuotaByUserName(String userName,String bizSystem) {
		return userQuotaMapper.getQuotaByUserName(userName,bizSystem);
	}
	
	 /**
     * 根据用户Id得到用户配额信息
     * @param userId
     * @return
     */
	public List<UserQuota> getQuotaByUserId(Integer userId) {
		return userQuotaMapper.getQuotaByUserId(userId);
	}
	
	/**
	 * 根据用户Id和时时彩模式查询配额记录
	 * @param userId
	 * @param sscRebate
	 * @return
	 */
	public UserQuota getQuotaByUserIdAndSscRebate(Integer userId, BigDecimal sscRebate) {
		return userQuotaMapper.getQuotaByUserIdAndSscRebate(userId, sscRebate);
	}
	
	/**
	 * 根据用户时时彩模式判断是否新增配额记录
	 * 若用户对应的模式记录存在，不进行新增
	 * @param user
	 */
	public void saveQuotaByUser(User newUser) {
		
		List<QuotaControl> quotaControls = quotaControlService.getQuotaControlByBizSystem(newUser.getBizSystem());
		if(CollectionUtils.isNotEmpty(quotaControls)) {
			BigDecimal sscRebate = newUser.getSscRebate();
			for(QuotaControl quotaControl : quotaControls) {
				//新注册的用户模式大于等于全局配置的模式
				if(sscRebate.compareTo(quotaControl.getSscRebate()) >= 0) {
					//先查询用户当前有没有此模式的记录
					UserQuota userQuota = this.getQuotaByUserIdAndSscRebate(newUser.getId(), quotaControl.getSscRebate());
					if(userQuota == null) {
						userQuota = new UserQuota();
						userQuota.setBizSystem(newUser.getBizSystem());
						userQuota.setUserId(newUser.getId());
						userQuota.setUserName(newUser.getUserName());
						userQuota.setSscRebate(quotaControl.getSscRebate());
						userQuota.setRemainNum(quotaControl.getOpenLowerLevelNum());
						userQuota.setCreatedDate(new Date());
						userQuota.setUpdateDate(new Date());
						this.insertSelective(userQuota);
					} else {
						logger.debug("用户["+ newUser.getUserName() +"],已经有对应模式["+ newUser.getSscRebate() +"]的配额记录，不进行新增");
					}
				}
			}
		}
		
	}
	
	
	/**
	 * 扣减用户的配额
	 * @throws Exception 
	 */
	@Transactional(readOnly=false,rollbackFor=Exception.class)
	public void dealAddUserQuotaChange(User parentUser, User newUser) throws Exception {
		if(parentUser == null) {
			return;
		}
		BigDecimal sscRebate = newUser.getSscRebate();
		UserQuota userQuota = getQuotaByUserIdAndSscRebate(parentUser.getId(),sscRebate);
		//上级有配额记录，判断新注册的用户是否需要扣减配额
		if(userQuota != null) {
			int remain = userQuota.getRemainNum();
			//配额为零的时候，无法注册
			if(remain == 0) {
				logger.error("用户[{}]配额数不足,操作失败", parentUser.getUserName());
				throw new RuntimeException("对不起，您的["+sscRebate+"]模式用户配额数不足，操作失败");
			} else {
				//处理配额扣减
				userQuota.setRemainNum(remain - 1);
				userQuota.setUpdateDate(new Date());
				this.updateByPrimaryKeySelective(userQuota);
				
				//插入配额帐变明细
				UserQuotaDetail quotaDetail = new UserQuotaDetail();
				quotaDetail.setUserId(parentUser.getId());
				quotaDetail.setBizSystem(parentUser.getBizSystem());
				quotaDetail.setUserName(parentUser.getUserName());
				quotaDetail.setOperateUserName(newUser.getUserName());
				quotaDetail.setOperateUserId(newUser.getId());
				quotaDetail.setType(UserQuotaDetail.REDUCE_TYPE);
				quotaDetail.setChangeNum(1);
				quotaDetail.setOperateUseSscRebate(newUser.getSscRebate());
				quotaDetail.setBeforeRemain(remain);
				quotaDetail.setAfterRemain(remain - 1);
				quotaDetail.setRemark("用户："+parentUser.getUserName()+"注册了用户："+newUser.getUserName()+"");
				quotaDetail.setCreatedDate(new Date());
				userQuotaDetailService.insertSelective(quotaDetail);
			}
		} 
		
		//处理新增用户是否需要添加配额数据
		this.saveQuotaByUser(newUser);
	}
	
	/**
	 * 批量更新模式
	 * @param quotaList
	 */
	@Transactional(readOnly=false,rollbackFor=Exception.class)
	public void batchUpdateUserQuota(List<UserQuota> quotaList) {
		for(UserQuota userQuota : quotaList){
			updateUserQuota(userQuota);
		}
		
	}
	
	/**
	 * 更新用户配额信息，同时插入记录
	 * @param record
	 */
	private  void updateUserQuota(UserQuota quota) {
		UserQuota oldQuota = userQuotaMapper.getQuotaByUserIdAndSscRebate(quota.getUserId(), quota.getSscRebate());
		if(oldQuota == null) {
			throw new RuntimeException("更新用户配额出错");
		}
		
		//保存配额帐变明细
		userQuotaDetailService.saveQuotaDetailWhenChange(quota, oldQuota, false, null);
		
		
		
		//更新配额信息表
		quota.setUpdateDate(new Date());
		userQuotaMapper.updateByPrimaryKeySelective(quota);
		
		//判断是否改变
		/*StringBuffer itemChangeLogDesc = new StringBuffer();
		if(!quota.getRemainEqulaLevelNum().equals(oldQuota.getRemainEqulaLevelNum())) {
			itemChangeLogDesc.append(RecordLogUtil.constructMessage("quota_item_update_log", "同级配额数", 
					String.valueOf(oldQuota.getRemainEqulaLevelNum()), String.valueOf(quota.getRemainEqulaLevelNum())));
		}
		if(!quota.getRemainLowerLevelNum().equals(oldQuota.getRemainLowerLevelNum())) {
			itemChangeLogDesc.append(RecordLogUtil.constructMessage("quota_item_update_log", "下级配额数", 
					String.valueOf(oldQuota.getRemainLowerLevelNum()), String.valueOf(quota.getRemainLowerLevelNum())));
		}
		if(!quota.getRemainLowerTwoLevelNum().equals(oldQuota.getRemainLowerTwoLevelNum())) {
			itemChangeLogDesc.append(RecordLogUtil.constructMessage("quota_item_update_log", "下两级配额数", 
					String.valueOf(oldQuota.getRemainLowerTwoLevelNum()), String.valueOf(quota.getRemainLowerTwoLevelNum())));
		}
		//保存操作日志
    	try {
    		//操作日志保存
            String recordKey = "quota_update_log";
            Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
        	String operateDesc = RecordLogUtil.constructMessage(recordKey, currentAdmin.getUsername(), quota.getUserName(), itemChangeLogDesc.toString());
	        recordLogService.saveRecordLog(currentAdmin.getBizSystem(), ERecordLogType.BACK_SYSTEM, currentAdmin.getUsername(), quota.getUserName(), ERecordLogModel.QUOTA_MANAGE, operateDesc);
        } catch (Exception e) {
        	//保存日志出现异常 不进行处理
        	logger.error("保存添加操作日志出错", e);
        }*/
	}
	
	
	/**
	 * 前台模式改变，判断更新配额及配额详细
	 * @param updateUser
	 * @throws Exception 
	 */
	public void updateQuotaBySscrebateChangeFront(User updateUser) throws Exception {
		User olduser = userService.getUserByName(updateUser.getBizSystem(), updateUser.getUserName());
		// 模式,代理类型不变不进行操作
		if (olduser.getSscRebate().compareTo(updateUser.getSscRebate()) == 0 && olduser.getDailiLevel().equals(updateUser.getDailiLevel())) {
			throw new RuntimeException("用户数据未发生改动！");
		}
		// 代理变会员，不允许
		if (updateUser.getDailiLevel().equals(EUserDailLiLevel.REGULARMEMBERS.name())
				&& !olduser.getDailiLevel().equals(updateUser.getDailiLevel())) {
			throw new RuntimeException("更新出错!,不能把代理变成会员，只能升！");
		}

		// 模式不变，会员变代理，判断更新配额及配额详细
		if (olduser.getSscRebate().compareTo(updateUser.getSscRebate()) == 0
				&& olduser.getDailiLevel().equals(EUserDailLiLevel.REGULARMEMBERS.name())
				&& !olduser.getDailiLevel().equals(updateUser.getDailiLevel())) {
			// 添加配额表
			this.saveQuotaByUser(updateUser);
		// 模式变，判断更新配额及配额详细
		} else if (olduser.getSscRebate().compareTo(updateUser.getSscRebate()) != 0) {
			String parentUserName = UserNameUtil.getUpLineUserName(updateUser.getRegfrom());
			User parentUser = userService.getUserByName(updateUser.getBizSystem(), parentUserName);
			// 查询上级用户当前配额是否足够
			UserQuota parentUserQuota = this.getQuotaByUserIdAndSscRebate(parentUser.getId(), updateUser.getSscRebate());
			if (parentUserQuota != null) {
				if (parentUserQuota.getRemainNum() == 0) {
					throw new RuntimeException("对不起，您的[" + updateUser.getSscRebate() + "]模式用户配额数不足，操作失败");
				} else {
					int remain = parentUserQuota.getRemainNum();
					// 扣减上级用户的配额数
					parentUserQuota.setRemainNum(remain - 1);
					parentUserQuota.setUpdateDate(new Date());
					this.updateByPrimaryKeySelective(parentUserQuota);

					// 插入配额帐变明细
					UserQuotaDetail quotaDetail = new UserQuotaDetail();
					quotaDetail.setUserId(parentUser.getId());
					quotaDetail.setBizSystem(parentUser.getBizSystem());
					quotaDetail.setUserName(parentUser.getUserName());
					quotaDetail.setOperateUserName(updateUser.getUserName());
					quotaDetail.setOperateUserId(updateUser.getId());
					quotaDetail.setType(UserQuotaDetail.REDUCE_TYPE);
					quotaDetail.setChangeNum(1);
					quotaDetail.setOperateUseSscRebate(updateUser.getSscRebate());
					quotaDetail.setBeforeRemain(remain);
					quotaDetail.setAfterRemain(remain - 1);
					quotaDetail.setRemark("用户：" + parentUser.getUserName() + "对用户：" + updateUser.getUserName() + "进行了升点操作");
					quotaDetail.setCreatedDate(new Date());
					userQuotaDetailService.insertSelective(quotaDetail);

					// 处理保存改变用户的配额记录
					this.saveQuotaByUser(updateUser);

					// 原先的模式为代理类型的，原先为会员类型的不需要此逻辑
					if (!olduser.getDailiLevel().equals(EUserDailLiLevel.REGULARMEMBERS.name())) {
						// 判断升级模式前的配额是否需要归还
						UserQuota parentUserUsedQuota = this.getQuotaByUserIdAndSscRebate(parentUser.getId(), olduser.getSscRebate());
						if (parentUserUsedQuota != null) {
							int usedRemain = parentUserUsedQuota.getRemainNum();
							parentUserUsedQuota.setRemainNum(usedRemain + 1);
							parentUserUsedQuota.setUpdateDate(new Date());
							this.updateByPrimaryKeySelective(parentUserUsedQuota);

							// 插入配额帐变明细
							UserQuotaDetail usedQuotaDetail = new UserQuotaDetail();
							usedQuotaDetail.setUserId(parentUser.getId());
							usedQuotaDetail.setBizSystem(parentUser.getBizSystem());
							usedQuotaDetail.setUserName(parentUser.getUserName());
							usedQuotaDetail.setOperateUserName(updateUser.getUserName());
							usedQuotaDetail.setOperateUserId(updateUser.getId());
							usedQuotaDetail.setType(UserQuotaDetail.ADD_TYPE);
							usedQuotaDetail.setChangeNum(1);
							usedQuotaDetail.setOperateUseSscRebate(updateUser.getSscRebate());
							usedQuotaDetail.setBeforeRemain(remain);
							usedQuotaDetail.setAfterRemain(remain - 1);
							usedQuotaDetail.setRemark("用户：" + parentUser.getUserName() + "对用户：" + updateUser.getUserName() + "进行了升点操作");
							usedQuotaDetail.setCreatedDate(new Date());
							userQuotaDetailService.insertSelective(usedQuotaDetail);
						}
					}
				}
			}
		}
	}
	
	
	/**
	 * 后台当用户模式变动时更新用户配额信息,同步更新模式信息
	 * @param record
	 * 
	 */
	@Transactional(readOnly=false,rollbackFor=Exception.class)
	public void updateUserQuotaBySscrebateChange(User updateUser, User oldUser) {
		User olduser = userService.getUserByName(updateUser.getBizSystem(), updateUser.getUserName());
   try {
		// 模式不变，会员变代理，判断更新配额及配额详细
	   System.out.println(olduser.getSscRebate() + ":" + updateUser.getSscRebate() );
		if (olduser.getSscRebate().compareTo(updateUser.getSscRebate()) == 0
				&& olduser.getDailiLevel().equals(EUserDailLiLevel.REGULARMEMBERS.name())
				&& !olduser.getDailiLevel().equals(updateUser.getDailiLevel())) {
			// 添加配额表
			this.saveQuotaByUser(updateUser);
		// 模式变，判断更新配额及配额详细
		} else if (olduser.getSscRebate().compareTo(updateUser.getSscRebate()) != 0) {
			String parentUserName = UserNameUtil.getUpLineUserName(olduser.getRegfrom());
			User parentUser = userService.getUserByName(updateUser.getBizSystem(), parentUserName);
			// 查询上级用户当前配额是否足够
			UserQuota parentUserQuota = this.getQuotaByUserIdAndSscRebate(parentUser.getId(), updateUser.getSscRebate());
			if (parentUserQuota != null) {
				if (parentUserQuota.getRemainNum() == 0) {
					throw new RuntimeException("对不起，您的[" + updateUser.getSscRebate() + "]模式用户配额数不足，操作失败");
				} else {
					int remain = parentUserQuota.getRemainNum();
					// 扣减上级用户的配额数
					parentUserQuota.setRemainNum(remain - 1);
					parentUserQuota.setUpdateDate(new Date());
					this.updateByPrimaryKeySelective(parentUserQuota);

					// 插入配额帐变明细
					UserQuotaDetail quotaDetail = new UserQuotaDetail();
					quotaDetail.setUserId(parentUser.getId());
					quotaDetail.setBizSystem(parentUser.getBizSystem());
					quotaDetail.setUserName(parentUser.getUserName());
					quotaDetail.setOperateUserName(updateUser.getUserName());
					quotaDetail.setOperateUserId(updateUser.getId());
					quotaDetail.setType(UserQuotaDetail.REDUCE_TYPE);
					quotaDetail.setChangeNum(1);
					quotaDetail.setOperateUseSscRebate(updateUser.getSscRebate());
					quotaDetail.setBeforeRemain(remain);
					quotaDetail.setAfterRemain(remain - 1);
					quotaDetail.setRemark("用户：" + parentUser.getUserName() + "对用户：" + updateUser.getUserName() + "进行了升点操作");
					quotaDetail.setCreatedDate(new Date());
					userQuotaDetailService.insertSelective(quotaDetail);

					// 处理保存改变用户的配额记录
					this.saveQuotaByUser(updateUser);

					// 原先的模式为代理类型的，原先为会员类型的不需要此逻辑
					if (!olduser.getDailiLevel().equals(EUserDailLiLevel.REGULARMEMBERS.name())) {
						// 判断升级模式前的配额是否需要归还
						UserQuota parentUserUsedQuota = this.getQuotaByUserIdAndSscRebate(parentUser.getId(), olduser.getSscRebate());
						if (parentUserUsedQuota != null) {
							int usedRemain = parentUserUsedQuota.getRemainNum();
							parentUserUsedQuota.setRemainNum(usedRemain + 1);
							parentUserUsedQuota.setUpdateDate(new Date());
							this.updateByPrimaryKeySelective(parentUserUsedQuota);

							// 插入配额帐变明细
							UserQuotaDetail usedQuotaDetail = new UserQuotaDetail();
							usedQuotaDetail.setUserId(parentUser.getId());
							usedQuotaDetail.setBizSystem(parentUser.getBizSystem());
							usedQuotaDetail.setUserName(parentUser.getUserName());
							usedQuotaDetail.setOperateUserName(updateUser.getUserName());
							usedQuotaDetail.setOperateUserId(updateUser.getId());
							usedQuotaDetail.setType(UserQuotaDetail.ADD_TYPE);
							usedQuotaDetail.setChangeNum(1);
							usedQuotaDetail.setOperateUseSscRebate(updateUser.getSscRebate());
							usedQuotaDetail.setBeforeRemain(remain);
							usedQuotaDetail.setAfterRemain(remain - 1);
							usedQuotaDetail.setRemark("用户：" + parentUser.getUserName() + "对用户：" + updateUser.getUserName() + "进行了升点操作");
							usedQuotaDetail.setCreatedDate(new Date());
							userQuotaDetailService.insertSelective(usedQuotaDetail);
						}
					}
				}
			}
		}
     } catch (Exception e) {
		e.printStackTrace();
	 }
	}
	
	
	
	/**
	 * 前台当用户模式变动时更新用户配额信息
	 * @param record
	 */
//	@Transactional(readOnly=false,rollbackFor=Exception.class)
//	public void updateUserQuotaBySscrebateChangeFront(User user, User oldUser, UserQuotaInfo userQuotaInfo) {
//		user.setUserName(oldUser.getUserName()); //获取用户名
//		//如果模式未发生变化
//		if(user.getSscRebate().compareTo(oldUser.getSscRebate()) == 0) {
//			return;
//		}
//		
//		//先查找上级配额，进行更新
//		String upUserName = UserNameUtil.getUpLineUserName(oldUser.getRegfrom());
//		if(StringUtils.isNotBlank(upUserName)) {
//			UserQuota oldUpUserQuota = this.getQuotaByUserName(upUserName);
//			if(oldUpUserQuota != null) {
//				UserQuota upUserQuota = new UserQuota();
//				try {
//					BeanUtils.copyProperties(upUserQuota, oldUpUserQuota);
//				} catch (Exception e) {
//					throw new RuntimeException("会员信息编辑出现错误");
//				}
//				
//				//调整用户模式至1956时，对应配额减一
//				if(user.getSscRebate().compareTo(ConstantUtil.QUOTA_1956_REBATE) == 0) {
//					if(upUserQuota.getRemain1956() <= 0) {
//						throw new RuntimeException("当前用户注册模式为1956的配额数不足，调整失败");
//					}
//					upUserQuota.setRemain1956(upUserQuota.getRemain1956() - 1);
//				}
//				//调整用户模式至1954时，对应配额减一
//				if(user.getSscRebate().compareTo(ConstantUtil.QUOTA_1954_REBATE) == 0) {
//					if(upUserQuota.getRemain1954() <= 0) {
//						throw new RuntimeException("当前用户注册模式为1954的配额数不足，调整失败");
//					}
//					upUserQuota.setRemain1954(upUserQuota.getRemain1954() - 1);
//				}
//				//调整用户模式至1952时，对应配额减一
//				if(user.getSscRebate().compareTo(ConstantUtil.QUOTA_1952_REBATE) == 0) {
//					if(upUserQuota.getRemain1952() <= 0) {
//						throw new RuntimeException("当前用户注册模式为1952的配额数不足，调整失败");
//					}
//					upUserQuota.setRemain1952(upUserQuota.getRemain1952() - 1);
//				}
//				//调整前用户模式是1956时，对应配额加1
//				if(oldUser.getSscRebate().compareTo(ConstantUtil.QUOTA_1956_REBATE) == 0) {
//					upUserQuota.setRemain1956(upUserQuota.getRemain1956() + 1);
//				}
//				//调整前用户模式是1954时，对应配额加1
//				if(oldUser.getSscRebate().compareTo(ConstantUtil.QUOTA_1954_REBATE) == 0) {
//					upUserQuota.setRemain1954(upUserQuota.getRemain1954() + 1);
//				}
//				//调整前用户模式是1952时，对应配额加1
//				if(oldUser.getSscRebate().compareTo(ConstantUtil.QUOTA_1952_REBATE) == 0) {
//					upUserQuota.setRemain1952(upUserQuota.getRemain1952() + 1);
//				}
//				
//				//保存帐变明细
//				userQuotaDetailService.saveQuotaDetailWhenChange(upUserQuota, oldUpUserQuota, true, oldUser.getUserName());
//				//更新设置的值
//				upUserQuota.setUpdateDate(new Date());
//				this.updateByPrimaryKey(upUserQuota);
//			}
//		}
//		
//		//记录操作日志
//    	try {
//    		User currentUser = BaseDwrUtil.getCurrentUser();
//    		String operateDesc = RecordLogUtil.constructMessage("agency_model_add_log", currentUser.getUserName(), user.getUserName(), String.valueOf(oldUser.getSscRebate()), String.valueOf(user.getSscRebate()));
//    		recordLogService.saveRecordLog(currentUser.getUserName(), user.getUserName(), ERecordLogModel.AGENCY_MODEL_ADD, operateDesc);
//    	} catch (Exception e) {
//    		//保存日志出现异常 不进行处理
//    		logger.error("代理用户进行升点操作，保存添加操作日志出错", e);
//    	}
//		
//		try {
//			//判断是否升点
//			boolean isRebateUp = false;
//			if(user.getSscRebate().compareTo(oldUser.getSscRebate()) > 0) {
//				isRebateUp = true;
//			}
//			
//			UserQuota oldQuota = userQuotaMapper.getQuotaByUserId(user.getId());
//			if(oldQuota != null) {
//				UserQuota quota = new UserQuota();
//				BeanUtils.copyProperties(quota, oldQuota);
//				
//				//前台没有降点，这里不会执行
//				if(!isRebateUp) {
//					if(user.getSscRebate().compareTo(ConstantUtil.QUOTA_1956_REBATE) <= 0) {
//						quota.setRemain1956(0);
//					}
//					if(user.getSscRebate().compareTo(ConstantUtil.QUOTA_1954_REBATE) <= 0) {
//						quota.setRemain1954(0);
//					}
//					if(user.getSscRebate().compareTo(ConstantUtil.QUOTA_1952_REBATE) <= 0) {
//						quota.setRemain1952(0);
//					}
//				} else {
//					//更改后是1958
//					if(user.getSscRebate().compareTo(ConstantUtil.QUOTA_1958_REBATE) == 0) {
//						//如果系统设置的1958可以注册1956的人数大于当前已经注册的1956
//						if(SystemSet.quota1958to1956 - userQuotaInfo.getCount1956() > 0) {
//							quota.setRemain1956(SystemSet.quota1958to1956 - userQuotaInfo.getCount1956());
//						} else {
//							quota.setRemain1956(0);
//						}
//						//如果系统设置的1958可以注册1954的人数大于当前已经注册的1954
//						if(SystemSet.quota1958to1954 - userQuotaInfo.getCount1954() > 0) {
//							quota.setRemain1954(SystemSet.quota1958to1954 - userQuotaInfo.getCount1954());
//						} else {
//							quota.setRemain1954(0);
//						}
//						//如果系统设置的1958可以注册1952的人数大于当前已经注册的1952
//						if(SystemSet.quota1958to1952 - userQuotaInfo.getCount1952() > 0) {
//							quota.setRemain1952(SystemSet.quota1958to1952 - userQuotaInfo.getCount1952());
//						} else {
//							quota.setRemain1952(0);
//						}
//					}
//					//更改后是1956
//					if(user.getSscRebate().compareTo(ConstantUtil.QUOTA_1956_REBATE) == 0) {
//						quota.setRemain1956(0);
//						//如果系统设置的1956可以注册1954的人数大于当前已经注册的1954
//						if(SystemSet.quota1956to1954 - userQuotaInfo.getCount1954() > 0) {
//							quota.setRemain1954(SystemSet.quota1956to1954 - userQuotaInfo.getCount1954());
//						} else {
//							quota.setRemain1954(0);
//						}
//						//如果系统设置的1956可以注册1952的人数大于当前已经注册的1952
//						if(SystemSet.quota1956to1952 - userQuotaInfo.getCount1952() > 0) {
//							quota.setRemain1952(SystemSet.quota1956to1952 - userQuotaInfo.getCount1952());
//						} else {
//							quota.setRemain1952(0);
//						}
//					}
//					//更改后是1954
//					if(user.getSscRebate().compareTo(ConstantUtil.QUOTA_1954_REBATE) == 0) {
//						quota.setRemain1956(0);
//						quota.setRemain1954(0);
//						//如果系统设置的1954可以注册1952的人数大于当前已经注册的1952
//						if(SystemSet.quota1954to1952 - userQuotaInfo.getCount1952() > 0) {
//							quota.setRemain1952(SystemSet.quota1954to1952 - userQuotaInfo.getCount1952());
//						} else {
//							quota.setRemain1952(0);
//						}
//					}
//					//更改后是1952
//					if(user.getSscRebate().compareTo(ConstantUtil.QUOTA_1952_REBATE) == 0) {
//						quota.setRemain1956(0);
//						quota.setRemain1954(0);
//						quota.setRemain1952(0);
//					}
//					
//				}
//				
//				//保存帐变明细
//				userQuotaDetailService.saveQuotaDetailWhenChange(quota, oldQuota, true, null);
//				//更新设置的值
//				quota.setUpdateDate(new Date());
//				this.updateByPrimaryKey(quota);
//			} else {
//				//判断是否达到配额要求，新增配额记录
//				user.setUserName(oldUser.getUserName());
//				this.saveQuotaByUser(user);
//			}
//			
//		} catch (Exception e) {
//			throw new RuntimeException("会员信息编辑出现错误");
//		} 
//	}
	
	
}
