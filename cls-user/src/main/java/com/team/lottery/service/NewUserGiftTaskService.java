package com.team.lottery.service;

import java.math.BigDecimal;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.enums.EMoneyDetailType;
import com.team.lottery.extvo.NewUserGiftTaskQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.UserTotalMoneyByTypeVo;
import com.team.lottery.mapper.newusergifttask.NewUserGiftTaskMapper;
import com.team.lottery.mapper.user.UserMapper;
import com.team.lottery.system.BizSystemConfigManager;
import com.team.lottery.system.BizSystemConfigVO;
import com.team.lottery.util.MakeOrderNum;
import com.team.lottery.vo.BizSystem;
import com.team.lottery.vo.MoneyDetail;
import com.team.lottery.vo.NewUserGiftTask;
import com.team.lottery.vo.User;
/**
 * 新手礼包领取页面相关Service
 * @author yzm
 *
 */
@Service("newUserGiftTaskService")
public class NewUserGiftTaskService {
	private static Logger logger = LoggerFactory.getLogger(NewUserGiftTaskService.class);
	@Autowired
	private NewUserGiftTaskMapper newUserGiftTaskMapper;
	@Autowired
	private MoneyDetailService moneyDetailService;
	@Autowired
	private UserService userService;
	@Autowired
	private UserMoneyTotalService userMoneyTotalService;
	@Autowired
	private UserMapper userMapper;


	/**
	 * 根据用户Id去查询新手注册的奖品领取情况
	 * 
	 * @param id
	 */
	public NewUserGiftTask getNewUserReceiveStatusById(Integer userId) {
		NewUserGiftTask newUserReceiveStatusById = newUserGiftTaskMapper.getNewUserReceiveStatusById(userId);
		return newUserReceiveStatusById;
	}

	/**
	 * 更新或者创建新手注册礼包
	 * @param type 传入的领取奖金的类型
	 * @param currentUser 当前领奖的用户信息
	 * @param rechargeWithDrawOrdersPageCount 当前用户是否有效充值
	 * @param orderSize 当前用户是否有效投注
	 * @param loginSizeWithApp 当前用户是否下载App登录;
	 * @param downMemberCount传入的参数用于判断有效的下级客户
	 * @return
	 * @throws Exception
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public Boolean updateNewUserReceiveStatus(String type, User currentUser, Integer rechargeWithDrawOrdersPageCount, Integer orderSize, Integer loginSizeWithApp,
			Integer bandUserBankSize, Integer downMemberCount) throws Exception {
		// 从数据库中去查询用户的相关信息,不要从缓存中查询,可能会版本号不一致
		User operationUser = userMapper.getUserByName(currentUser.getBizSystem(), currentUser.getUserName());
		// 根据当前用户ID去查询当前用户的新手礼包信息
		NewUserGiftTask newUserGiftTaskById = newUserGiftTaskMapper.getNewUserReceiveStatusById(currentUser.getId());

		String bizSystemCode = currentUser.getBizSystem();// 获取当前用户系统的name
		BizSystem bizSystem = ClsCacheManager.getBizSystemByCode(bizSystemCode);// 获取系统对象
		BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(bizSystem.getBizSystem());
		BigDecimal appDownGiftMoney = bizSystemConfigVO.getAppDownGiftMoney();// 获取系统下载App赠送的金额
		BigDecimal completeInfoGiftMoney = bizSystemConfigVO.getCompleteInfoGiftMoney();// 获取系统完善个人信息赠送的金额;
		BigDecimal lotteryGiftMoney = bizSystemConfigVO.getLotteryGiftMoney();// 获取系统中完成投注一笔赠送的金额;有效投注;
		BigDecimal rechargeGiftMoney = bizSystemConfigVO.getRechargeGiftMoney();// 获取当前系统中完成首冲赠送的金额;
		BigDecimal extendUserGiftMoney = bizSystemConfigVO.getExtendUserGiftMoney();// 获取当前系统中发展一位客户赠送的金额;

		MoneyDetail transUserMoneyDetail = new MoneyDetail();// 创建资金明细对象

		String userReceiveName = "";// 根据前端type的字段去赋值
		Date updateTime = new Date();
		BigDecimal userReceiveBonus = BigDecimal.ZERO;
		// 如果查询为空,就是第一次领取,新建一个NewUserGiftTask对象
		if (newUserGiftTaskById == null) {

			NewUserGiftTask newUserGiftTask = new NewUserGiftTask();
			newUserGiftTask.setUserId(currentUser.getId());
			newUserGiftTask.setUserName(currentUser.getUserName());
			newUserGiftTask.setBizSystem(bizSystemCode);

			// 设置新增新手礼包领取时候的默认值;
			if (loginSizeWithApp > 0) {
				newUserGiftTask.setAppDownTaskStatus(1);
			}
			if (rechargeWithDrawOrdersPageCount > 0) {
				newUserGiftTask.setRechargeTaskStatus(1);
			}
			if (bandUserBankSize > 0) {
				newUserGiftTask.setCompleteInfoTaskStatus(1);
			}
			if (orderSize > 0) {
				newUserGiftTask.setLotteryTaskStatus(1);
			}
			if (downMemberCount > 0) {
				newUserGiftTask.setExtendUserTaskStatus(1);
			}

			if (type.equals("appDownGiftMoney") && loginSizeWithApp > 0) {
				newUserGiftTask.setAppDownTaskStatus(1);
				userReceiveBonus = appDownGiftMoney;
				newUserGiftTask.setAppDownTaskMoney(userReceiveBonus);
				userReceiveName = "下载App领取奖金:";
			} else if (type.equals("completeInfoGiftMoney") && bandUserBankSize > 0) {
				newUserGiftTask.setCompleteInfoTaskStatus(1);
				userReceiveBonus = completeInfoGiftMoney;
				newUserGiftTask.setCompleteInfoTaskMoney(userReceiveBonus);
				userReceiveName = "完善个人信息领取奖金:";
			} else if (type.equals("lotteryGiftMoney") && orderSize > 0) {
				newUserGiftTask.setLotteryTaskStatus(1);
				userReceiveBonus = lotteryGiftMoney;
				newUserGiftTask.setLotteryTaskMoney(userReceiveBonus);
				userReceiveName = "完成首笔投注领取奖金:";
			} else if (type.equals("rechargeGiftMoney") && rechargeWithDrawOrdersPageCount > 0) {
				newUserGiftTask.setRechargeTaskStatus(1);
				userReceiveBonus = rechargeGiftMoney;
				newUserGiftTask.setRechargeTaskMoney(userReceiveBonus);

				userReceiveName = "完成首笔充值领取奖金:";
			} else if (type.equals("extendUserGiftMoney") && downMemberCount > 0) {
				newUserGiftTask.setExtendUserTaskStatus(1);
				userReceiveBonus = extendUserGiftMoney;
				newUserGiftTask.setExtendUserTaskMoney(userReceiveBonus);
				userReceiveName = "完成新用户发展领取奖金:";
			}
			// 设置跟新时间和修改时间一样
			newUserGiftTask.setCreateTime(updateTime);
			// 设置更新时间
			newUserGiftTask.setUpdateTime(updateTime);
			// 数据库中新增一条数据
			newUserGiftTaskMapper.insertSelective(newUserGiftTask);
			logger.info("领取新手任务奖金通报: 用户:"+operationUser.getUserName()+"在["+operationUser.getBizSystem()+"]系统中,领取:"+userReceiveName+"领取了:"+userReceiveBonus+"块钱!");

		}
		// 如果礼包信息不为空进入下面的逻辑就使用update语句
		else {
			// 用户领取的奖金的类型并且领取时候判断数据是否领取合法!防止直接修改数据库
			if (type.equals("appDownGiftMoney") && loginSizeWithApp > 0) {
				userReceiveName = "下载App领取奖金:";
				// 判断查询出来的对象去获取下载App领取的奖金,如果不等于0,就说明用户已经领取,返回false;
				if (newUserGiftTaskById.getAppDownTaskMoney().compareTo(userReceiveBonus) > 0) {
					logger.error("领取新手任务奖金通报: 用户:"+operationUser.getUserName()+"在["+operationUser.getBizSystem()+"]系统中,想要多次领取:"+userReceiveName+"请注意!!!");
					return false;
				}
				userReceiveBonus = appDownGiftMoney;
				newUserGiftTaskById.setAppDownTaskStatus(1);
				newUserGiftTaskById.setAppDownTaskMoney(userReceiveBonus);
			} else if (type.equals("completeInfoGiftMoney") && bandUserBankSize > 0) {
				// 判断用户是否领取过完成个人信息的奖金;
				if (newUserGiftTaskById.getCompleteInfoTaskMoney().compareTo(userReceiveBonus) > 0) {
					logger.error("领取新手任务奖金通报: 用户:"+operationUser.getUserName()+"在["+operationUser.getBizSystem()+"]系统中,想要多次领取:"+userReceiveName+"请注意!!!");
					return false;
				}
				userReceiveBonus = completeInfoGiftMoney;
				newUserGiftTaskById.setCompleteInfoTaskStatus(1);
				newUserGiftTaskById.setCompleteInfoTaskMoney(userReceiveBonus);
				userReceiveName = "完善个人信息领取奖金:";
			} else if (type.equals("lotteryGiftMoney") && orderSize > 0) {
				// 判断用户是否领取首笔投注的奖金;
				if (newUserGiftTaskById.getLotteryTaskMoney().compareTo(userReceiveBonus) > 0) {
					logger.error("领取新手任务奖金通报: 用户:"+operationUser.getUserName()+"在["+operationUser.getBizSystem()+"]系统中,想要多次领取:"+userReceiveName+"请注意!!!");
					return false;
				}
				userReceiveBonus = lotteryGiftMoney;
				newUserGiftTaskById.setLotteryTaskStatus(1);
				newUserGiftTaskById.setLotteryTaskMoney(userReceiveBonus);
				userReceiveName = "完成首笔投注领取奖金:";
			} else if (type.equals("rechargeGiftMoney") && rechargeWithDrawOrdersPageCount > 0) {
				// 判断用户是否领取首笔充值的奖金;
				if (newUserGiftTaskById.getRechargeTaskMoney().compareTo(userReceiveBonus) > 0) {
					logger.error("领取新手任务奖金通报: 用户:"+operationUser.getUserName()+"在["+operationUser.getBizSystem()+"]系统中,想要多次领取:"+userReceiveName+"请注意!!!");
					return false;
				}
				userReceiveBonus = rechargeGiftMoney;
				newUserGiftTaskById.setRechargeTaskStatus(1);
				newUserGiftTaskById.setRechargeTaskMoney(userReceiveBonus);
				userReceiveName = "完成首笔充值领取奖金:";
			} else if (type.equals("extendUserGiftMoney") && downMemberCount > 0) {
				// 判断用户是否领取邀请新用户的奖金;
				if (newUserGiftTaskById.getExtendUserTaskMoney().compareTo(userReceiveBonus) > 0) {
					return false;
				}
				userReceiveBonus = extendUserGiftMoney;
				newUserGiftTaskById.setExtendUserTaskStatus(1);
				newUserGiftTaskById.setExtendUserTaskMoney(userReceiveBonus);
				userReceiveName = "完成新用户发展领取奖金:";
			}
			// 设置更新时间
			newUserGiftTaskById.setUpdateTime(updateTime);
			// 修改UserGiftTask表中对应的数据
			newUserGiftTaskMapper.updateByPrimaryKeySelective(newUserGiftTaskById);
			logger.info("领取新手任务奖金通报: 用户:"+operationUser.getUserName()+"在["+operationUser.getBizSystem()+"]系统中,领取:"+userReceiveName+"领取了:"+userReceiveBonus+"块钱!");
		}
		// 添加资金添加明细
		transUserMoneyDetail.setUserId(currentUser.getId()); // 用户id
		transUserMoneyDetail.setOrderId(null); // 订单id
		transUserMoneyDetail.setBizSystem(bizSystem.getBizSystem());
		transUserMoneyDetail.setUserName(operationUser.getUserName()); // 用户名
		transUserMoneyDetail.setOperateUserName(operationUser.getUserName()); // 操作人员
		transUserMoneyDetail.setWinLevel(null);
		transUserMoneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.TS));
		transUserMoneyDetail.setWinCost(null);
		transUserMoneyDetail.setWinCostRule(null);
		transUserMoneyDetail.setDetailType(EMoneyDetailType.ACTIVITIES_MONEY.name()); // 设置金额类型
		transUserMoneyDetail.setPay(new BigDecimal(0));
		transUserMoneyDetail.setBalanceBefore(operationUser.getMoney()); // 支出前金额
		transUserMoneyDetail.setIncome(userReceiveBonus);
		transUserMoneyDetail.setBalanceAfter(operationUser.getMoney().add(userReceiveBonus)); // 支出后的金额
		transUserMoneyDetail.setDescription(userReceiveName + userReceiveBonus);
		operationUser.addMoney(userReceiveBonus, false); // 余额增加
		int res = userService.updateByPrimaryKeyWithVersionSelective(operationUser);
		if (res == 1) {
			// 新增资金明细
			moneyDetailService.insertSelective(transUserMoneyDetail);
			// 增加回滚操作
			UserTotalMoneyByTypeVo userTotalMoneyByTypeVo = new UserTotalMoneyByTypeVo(operationUser, EMoneyDetailType.RECHARGE_PRESENT, userReceiveBonus);
			userMoneyTotalService.addUserTotalMoneyByType(userTotalMoneyByTypeVo.getUser(), userTotalMoneyByTypeVo.getDetailType(), userTotalMoneyByTypeVo.getOperationValue());
			return true;
		} else {
			return false;
		}

	}

	/**
	 * 根据查询条件查询出对应的newUserGiftTask对象
	 * @param query 查询参数
	 * @param page 查询分页
	 * @return 返回newUserGiftTask集合
	 */
	public Page getAllNewUserGiftTaskService(NewUserGiftTaskQuery query, Page page) {
		page.setTotalRecNum(newUserGiftTaskMapper.getNewUserGiftTaskCount(query));
		page.setPageContent(newUserGiftTaskMapper.getNewUserGiftTask(query, page.getStartIndex(), page.getPageSize()));
		return page;
	}

}
