package com.team.lottery.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.UserBankQueryVo;
import com.team.lottery.extvo.UserQuery;
import com.team.lottery.mapper.userbank.UserBankMapper;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserBank;


/**
 * 用户银行卡服务类
 * @author
 *
 */
@Service("userBankService")
public class UserBankService {

	@Autowired
	private UserBankMapper userBankMapper;

    public int deleteByPrimaryKey(Long id){
    	return userBankMapper.deleteByPrimaryKey(id);
    }

    public int insert(UserBank record){
    	record.setCreateDate(new Date());
    	return userBankMapper.insert(record);
    }

    public int insertSelective(UserBank record){
    	record.setCreateDate(new Date());
    	return userBankMapper.insertSelective(record);
    }

    public UserBank selectByPrimaryKey(Long id){
    	return userBankMapper.selectByPrimaryKey(id);
    }

    public int updateByPrimaryKeySelective(UserBank record){
    	record.setUpdateDate(new Date());
    	return userBankMapper.updateByPrimaryKeySelective(record);
    }

    public int updateByPrimaryKey(UserBank record){
    	record.setUpdateDate(new Date());
    	return userBankMapper.updateByPrimaryKey(record);
    }

	public List<UserBank> getAllUserBanksByUserId(Integer userId) {
		List<UserBank> userBanks = userBankMapper.getAllUserBanksByUserId(userId);
		return userBanks;
	}

	public UserBank getUserBankByBankNum(String bizSystem, String bankCardNum) {

		return userBankMapper.getUserBankByBankNum(bizSystem,bankCardNum);
	}

	/**
	 * 查找用户对应的银行卡
	 * @param withDrawId
	 * @param withDrawValue
	 * @return
	 */
	public UserBank getUserBankByIdAndUserId(Long withDrawId,Integer userId){
		return userBankMapper.getUserBankByIdAndUserId(withDrawId, userId);
	}
    
	/**
	 * 查询用户的银行卡信息 
	 * @return
	 */
	public List<UserBank> getUserBanksByUserName(UserBankQueryVo userBankQueryVo){
		return userBankMapper.getUserBanksByUserName(userBankQueryVo);
	}
    
	  /**
     * 查找所有的
     * @return
     */
    public Page getAllUserBanksByQueryPage(UserBankQueryVo query,Page page){
		page.setTotalRecNum(userBankMapper.getAllUserBanksByQueryPageCount(query));
		page.setPageContent(userBankMapper.getAllUserBanksByQueryPage(query,page.getStartIndex(),page.getPageSize()));
    	return page;
    }
    
    
    /**
     * 查询其他姓名的银行卡记录
     * @return
     */
	public List<UserBank> queryOtherNameBanks(UserBankQueryVo queryUser) {
		return userBankMapper.queryOtherNameBanks(queryUser);
	}
}
