package com.team.lottery.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.mapper.usercookielog.UserCookieLogMapper;
import com.team.lottery.vo.UserCookieLog;

@Service
public class UserCookieLogService {

	@Autowired
	private UserCookieLogMapper userCookieLogMapper;
	
	public int deleteByPrimaryKey(Integer id) {
		return userCookieLogMapper.deleteByPrimaryKey(id);
	}
	
    public int insertSelective(UserCookieLog record) {
    	return userCookieLogMapper.insertSelective(record);
    }

    public UserCookieLog selectByPrimaryKey(Integer id){
    	return userCookieLogMapper.selectByPrimaryKey(id);
    }

    public int updateByPrimaryKeySelective(UserCookieLog record){
    	return userCookieLogMapper.updateByPrimaryKeySelective(record);
    }
    
    /**
     * 根据token来查找记录
     * @param token
     * @return
     */
    public UserCookieLog queryByToken(String token) {
    	return userCookieLogMapper.queryByToken(token);
    }
    
    /**
     * 使之前用户的cookie失效
     * @param userId
     * @param domainUrl
     */
    public int setBeforeCookieExpired(Integer userId, String domainUrl) {
    	return userCookieLogMapper.setBeforeCookieExpired(userId, domainUrl);
    }

}
