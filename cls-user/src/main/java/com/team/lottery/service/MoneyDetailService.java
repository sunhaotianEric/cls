package com.team.lottery.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.enums.EMoneyDetailType;
import com.team.lottery.enums.EUserDailLiLevel;
import com.team.lottery.extvo.MoneyDetailQuery;
import com.team.lottery.extvo.OrderQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.TeamUserQuery;
import com.team.lottery.extvo.UserConsumeReportVo;
import com.team.lottery.extvo.UserDayConsumeQuery;
import com.team.lottery.extvo.UserHourConsumeQuery;
import com.team.lottery.extvo.UserQuery;
import com.team.lottery.mapper.moneydetail.MoneyDetailMapper;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.DateUtils;
import com.team.lottery.util.UserNameUtil;
import com.team.lottery.vo.MoneyDetail;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserDayConsume;
import com.team.lottery.vo.UserDayMoney;
import com.team.lottery.vo.UserHourConsume;
import com.team.lottery.vo.UserSelfDayConsume;

/**
 * 资金明细业务逻辑
 * @author chenhsh
 *
 */
@Service("moneyDetailService")
public class MoneyDetailService {
	
	private static Logger logger = LoggerFactory.getLogger(MoneyDetailService.class);
	
	@Autowired
	private MoneyDetailMapper  moneyDetailMapper;
	
	@Autowired
	private UserService userService;
	
	
	@Autowired
	private UserDayConsumeService userDayConsumeService;
	
	@Autowired
	private UserSelfDayConsumeService userSelfDayConsumeService;
	
/*	@Autowired
	private UserDayMoneyService userDayMoneyService;
	*/
	@Autowired
	private UserHourConsumeService userHourConsumeService;
	
    public int deleteByPrimaryKey(Long id){
    	return moneyDetailMapper.deleteByPrimaryKey(id);
    }

    public int insert(MoneyDetail record){
    	return moneyDetailMapper.insert(record);
    }

    public int insertSelective(MoneyDetail record){
    	record.setCreatedDate(new Date());
    	setMoneyScale(record);
    	return moneyDetailMapper.insertSelective(record);
    }

    public MoneyDetail selectByPrimaryKey(Long id){
    	return moneyDetailMapper.selectByPrimaryKey(id);
    }

    public int updateByPrimaryKeySelective(MoneyDetail record){
    	setMoneyScale(record);
    	return moneyDetailMapper.updateByPrimaryKeySelective(record);
    }

    public int updateByPrimaryKey(MoneyDetail record){
    	return moneyDetailMapper.updateByPrimaryKey(record);
    }
    
    /**
     * 资金明细记录 是否已经生成
     * @param query
     * @return
     */
    public Integer getCountByOrderDtype(@Param("query")MoneyDetailQuery query){
		return moneyDetailMapper.getCountByOrderDtype(query);
    }
    
    
    /**
     * 根据订单id和资金类型去查询资金明细
     */
    
    
    
	/**
	 * 控制所有金额的精度
	 * @param record
	 */
	public void setMoneyScale(MoneyDetail record){
    	if(record.getIncome() != null){
    		record.setIncome(record.getIncome().setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP));
    	}
    	if(record.getPay() != null){
    		record.setPay(record.getPay().setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP));
    	}
    	if(record.getBalanceAfter() != null){
    		record.setBalanceAfter(record.getBalanceAfter().setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP));
    	}
    	if(record.getBalanceBefore() != null){
    		record.setBalanceBefore(record.getBalanceBefore().setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP));
    	}    	
    	if(record.getWinCost() != null){
    		record.setWinCost(record.getWinCost().setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP));
    	}  
    	if(record.getWinCostRule() != null){
    		record.setWinCostRule(record.getWinCostRule().setScale(ConstantUtil.BIGDECIMAL_SCAL,BigDecimal.ROUND_HALF_UP));
    	}  
	}
	
    /**
     * 查找最新的可分红的资金明细记录
     * @return
     */
    public MoneyDetail getLastestCanBonusRecord(String bizSyStem){
    	return moneyDetailMapper.getLastestCanBonusRecord(bizSyStem);
    }
	
    /**
     * 分页查找资金明细记录  不关联用户表
     * @return
     */
    public Page getAllMoneyDetailsByQueryPage(MoneyDetailQuery query,Page page){
		page.setTotalRecNum(moneyDetailMapper.getAllMoneyDetailsByQueryPageCount(query));
		page.setPageContent(moneyDetailMapper.getAllMoneyDetailsByQueryPage(query,page.getStartIndex(),page.getPageSize()));
    	return page;
    }
    
    /**
     * 分页查找用户账户明细记录 关联用户表
     * @return
     */
    public Page getMoneyDetailsByQueryPage(MoneyDetailQuery query,Page page){
		page.setTotalRecNum(moneyDetailMapper.queryMoneyDetailsByQueryPageCount(query));
		page.setPageContent(moneyDetailMapper.queryMoneyDetailsByQueryPage(query,page.getStartIndex(),page.getPageSize()));
    	return page;
    }
    
    /**
     * 分页查找用户账户明细记录  关联用户表
     * @return
     */
    public Page getUserMoneyDetailsByQueryPage(MoneyDetailQuery query,Page page){
		page.setTotalRecNum(moneyDetailMapper.getUserMoneyDetailsByQueryPageCount(query));
		page.setPageContent(moneyDetailMapper.getUserMoneyDetailsByQueryPage(query,page.getStartIndex(),page.getPageSize()));
    	return page;
    }
    
	/**
	 * 资金明细总额
	 * @param query
	 * @return
	 */
	public MoneyDetail getAllMoneyDetailsTotal(MoneyDetailQuery query){
		return moneyDetailMapper.getAllMoneyDetailsTotal(query);
	}
	
	/**
	 * 根据资金明细统计各类型总额
	 * @param query
	 * @return
	 */
	public List<MoneyDetail> getTotalMoneyByDetailType(MoneyDetailQuery query){
		return moneyDetailMapper.getTotalMoneyByDetailType(query);
	}
	
	/**
	  * 根据资金明细统计各类型用户人数
	  * @param query
	  * @return
	  */
	public List<MoneyDetail> getUserCountByDetailType(MoneyDetailQuery query){
		return moneyDetailMapper.getUserCountByDetailType(query);
	}
    
    /**
     * 一个月之内的输赢总览
     * @return
     */
//    public List<UserConsumeReportVo> getBunkoStatistics(){
//    	List<UserConsumeReportVo> result = new ArrayList<UserConsumeReportVo>();
//    	User currentUser = BaseDwrUtil.getCurrentUser();
//		Date now = new Date();
//		Date oneMonthAgo = DateUtil.addDaysToDate(now, -30);
//		Date sevenDaysAgo = DateUtil.addDaysToDate(now, -7);
//		Date oneDaysAgo = DateUtil.addDaysToDate(now, -1);
//		
//        MoneyDetailQuery moneyDetailQuery = new MoneyDetailQuery();
//        moneyDetailQuery.setUserName(currentUser.getUserName());
//        moneyDetailQuery.setCreatedDateStart(oneMonthAgo);
//        moneyDetailQuery.setCreatedDateEnd(now);
//	    List<MoneyDetail> oneMonthAgoMoneyDetails = moneyDetailMapper.queryMoneyDetailsByCondition(moneyDetailQuery);
//	    List<MoneyDetail> sevenDaysAgoMoneyDetails = new ArrayList<MoneyDetail>();
//	    List<MoneyDetail> oneDayAgoMoneyDetails = new ArrayList<MoneyDetail>();
//	    for(MoneyDetail moneyDetail : oneMonthAgoMoneyDetails){
//			long moneyDetailTime = moneyDetail.getCreatedDate().getTime();
//        	if((oneDaysAgo.getTime() <  moneyDetailTime) && (moneyDetailTime < now.getTime())){
//        		oneDayAgoMoneyDetails.add(moneyDetail);
//        	}
//        	if((sevenDaysAgo.getTime() <  moneyDetailTime) && (moneyDetailTime < now.getTime())){
//        		sevenDaysAgoMoneyDetails.add(moneyDetail);
//        	}           	
//        }
//	    
//	    Map<String,UserConsumeReportVo> oneMonthConsumeReportMaps = this.getMoneyDetailForuserMap(oneMonthAgoMoneyDetails, null);
//	    Map<String,UserConsumeReportVo> sevenDaysConsumeReportMaps = this.getMoneyDetailForuserMap(sevenDaysAgoMoneyDetails, null);
//	    Map<String,UserConsumeReportVo> oneDayConsumeReportMaps = this.getMoneyDetailForuserMap(oneDayAgoMoneyDetails, null);
//
//	    //一天
//	    if(oneDayConsumeReportMaps.size() == 0){
//	    	UserConsumeReportVo reportVo = new UserConsumeReportVo();
//		    result.add(reportVo);
//	    }else{
//		    result.add(oneDayConsumeReportMaps.get(currentUser.getUserName()));
//	    }
//	    
//	    //7天
//	    if(sevenDaysConsumeReportMaps.size() == 0){
//	    	UserConsumeReportVo reportVo = new UserConsumeReportVo();
//		    result.add(reportVo);
//	    }else{
//		    result.add(sevenDaysConsumeReportMaps.get(currentUser.getUserName()));
//	    }
//	    
//	    //一个月
//	    if(oneMonthConsumeReportMaps.size() == 0){
//	    	UserConsumeReportVo reportVo = new UserConsumeReportVo();
//		    result.add(reportVo);
//	    }else{
//		    result.add(oneMonthConsumeReportMaps.get(currentUser.getUserName()));
//	    }
//		
//	    return result;
//    }

    
    /**
     * 后台用户消费报表
     * @param query
     * @return
     */
   	public Map<String,UserConsumeReportVo> getConsumeReport(MoneyDetailQuery query){ //,Page page
   		List<MoneyDetail> moneyDetails = moneyDetailMapper.queryMoneyDetailsByCondition(query);
   		Map<String,UserConsumeReportVo> consumeReportMaps = this.getMoneyDetailForuserMap(moneyDetails, null);
   	
   		User queryUser = userService.getUserByName(query.getBizSystem(),query.getUserName());
   		UserConsumeReportVo userConsumeReportVo = consumeReportMaps.get(query.getUserName());
   		if(userConsumeReportVo != null && queryUser != null) {
   			userConsumeReportVo.setMoney(queryUser.getMoney());
   		}
        return consumeReportMaps;
   		
//   	List<MoneyDetail> moneyDetails = moneyDetailMapper.getConsumeReportForQueryPage(query,page.getStartIndex(),page.getPageSize());
//   	Map<String,UserConsumeReportVo> consumeReportMaps = this.getMoneyDetailForuserMap(moneyDetails);
//   	page.setTotalRecNum(moneyDetailMapper.getConsumeReportForQueryPageCount(query));
//   	page.setPageContent(consumeReportMaps);
//		return page;
   	}
   	
   	/**
   	 * 获取团队的消费报表
   	 * @deprecated 以前的方法，新方法改用
   	 * @param query
   	 * @return
   	 */
   	public Map<String,UserConsumeReportVo> getConsumeReportForReamOld(MoneyDetailQuery query,String leaderUserName){ 
   		
   		List<MoneyDetail> moneyDetails = moneyDetailMapper.queryMoneyDetailsByCondition(query);
   		List<MoneyDetail> totalMoneyDetails = new ArrayList<MoneyDetail>();
   		String [] regFromUserNames = null;
   		Map<String,String> teamUsersMap = new LinkedHashMap<String,String>();
   		teamUsersMap.put(leaderUserName, leaderUserName);
   		String leaderNameFormat = ConstantUtil.REG_FORM_SPLIT + leaderUserName + ConstantUtil.REG_FORM_SPLIT;
   		
   		User leaderUser = userService.getUserByName(query.getBizSystem(),leaderUserName);
   		
   		//查询用户直接下级所有用户
   		TeamUserQuery teamUserQuery = new TeamUserQuery();
   		teamUserQuery.setTeamLeaderName(leaderNameFormat);
   		List<User> subUsers = userService.getAllTeamUserList(teamUserQuery);
   		
   		if(CollectionUtils.isNotEmpty(subUsers)) {
   			for(User subUser : subUsers) {
   				teamUsersMap.put(subUser.getUserName(), subUser.getUserName());
   			}
   		}
   		
   		//查询用户的所有下级用户,统计所有下级用户的余额
   		UserQuery userquery = new UserQuery();
   		userquery.setTeamLeaderName(ConstantUtil.REG_FORM_SPLIT + leaderUserName + ConstantUtil.REG_FORM_SPLIT);
   		userquery.setRealLeaderName(leaderUserName);
   		List<User> allSubUsers = userService.getUsersByCondition(userquery);
   		Map<String, BigDecimal> userBalanceMap = getUserBalanceMap(allSubUsers, subUsers);
   		
   		
   		for(MoneyDetail moneyDetail : moneyDetails){
   			regFromUserNames = moneyDetail.getRegfrom().substring(moneyDetail.getRegfrom().indexOf(leaderNameFormat) + 1, moneyDetail.getRegfrom().length()).split(ConstantUtil.REG_FORM_SPLIT);
   			for(String regFromUserName : regFromUserNames){
   				if(regFromUserName.equals(ConstantUtil.FRONT_ADMIN_USER_NAME)){
   				   continue;	
   				}
   				//只拷贝到直接下级用户下
   				if(!regFromUserName.equals(leaderUserName) && teamUsersMap.get(regFromUserName) != null){
   	   				totalMoneyDetails.add(moneyDetail.copy(regFromUserName));
   				}
   			} 
   			//只拷贝到直接下级用户和团队领导人下
   			if(teamUsersMap.get(moneyDetail.getUserName()) != null || moneyDetail.getUserName().equals(leaderUserName)) {
   				totalMoneyDetails.add(moneyDetail);
   			}
   		}
   		
   		Map<String,UserConsumeReportVo> consumeReportMaps = this.getMoneyDetailForuserMap(totalMoneyDetails, null);
   		Map<String,UserConsumeReportVo> consumeReportMapsResult = new LinkedHashMap<String, UserConsumeReportVo>();
   		//下级会员数目 
   		for(Map.Entry<String, String> entry:teamUsersMap.entrySet()){ 
   			//不为空才添加
   			if(consumeReportMaps.get(entry.getKey()) != null) {
   				UserConsumeReportVo reportVo = consumeReportMaps.get(entry.getKey());
   				BigDecimal userBalance = userBalanceMap.get(entry.getKey());
   				if(userBalance != null) {
   					reportVo.setMoney(userBalance);
   				}
   				consumeReportMapsResult.put(entry.getKey(), reportVo);
   			}
   		}   
   		
   		//如果当天自己团队领导人没有消费的话
   		if(consumeReportMapsResult.get(leaderUserName) == null){
   			UserConsumeReportVo reportVo = new UserConsumeReportVo(leaderUserName);
   			reportVo.setMoney(leaderUser.getMoney());
   			consumeReportMapsResult.put(leaderUserName, reportVo);
   		} else {
   			UserConsumeReportVo reportVo = consumeReportMapsResult.get(leaderUserName);
   			reportVo.setMoney(leaderUser.getMoney());
   		}
   		
   		//设置查询领导人的regFrom 用于展现导航栏
   		UserConsumeReportVo leaderConsumeReportVo = consumeReportMapsResult.get(leaderUserName);
   		leaderConsumeReportVo.setRegfrom(leaderUser.getRegfrom());
   		
        return consumeReportMapsResult;
   	}
   	
   	/**
   	 * 获取团队的消费报表
   	 * @param query
   	 * @return
   	 */
   	public Map<String,UserConsumeReportVo> getConsumeReportForReam(MoneyDetailQuery query,String leaderUserName){ 
   		
   		Long startTimeAll = System.currentTimeMillis();
   		Map<String,String> teamUsersMap = new LinkedHashMap<String,String>();
   		teamUsersMap.put(leaderUserName, leaderUserName);
   		String leaderNameFormat = ConstantUtil.REG_FORM_SPLIT + leaderUserName + ConstantUtil.REG_FORM_SPLIT;
   	   
   		User leaderUser = userService.getUserByName(query.getBizSystem(),leaderUserName);
   		if(EUserDailLiLevel.SUPERAGENCY.getCode().equals(leaderUser.getDailiLevel())) {
   			leaderNameFormat = leaderUser.getUserName() + ConstantUtil.REG_FORM_SPLIT;
   		}
   		
   		//查询用户直接下级所有用户
   		TeamUserQuery teamUserQuery = new TeamUserQuery();
   		teamUserQuery.setTeamLeaderName(leaderNameFormat);
   		teamUserQuery.setBizSystem(query.getBizSystem());
   		List<User> subUsers = userService.getAllTeamUserList(teamUserQuery);
   		
   		if(CollectionUtils.isNotEmpty(subUsers)) {
   			for(User subUser : subUsers) {
   				teamUsersMap.put(subUser.getUserName(), subUser.getUserName());
   			}
   		}
   		
   		Long startTime = System.currentTimeMillis();
   		//从用户团队日余额表中查询
   		startTime = System.currentTimeMillis();
   	//	Map<String, UserDayMoney> userDayMoneyMaps = getUserDayMoneyMaps(query);
   		Map<String, UserDayMoney> userDayMoneyMaps = new HashMap<String, UserDayMoney>();
   		logger.debug("查询用户团队日余额表耗时["+(System.currentTimeMillis() - startTime)+"]ms");
   		
   		Map<String,UserConsumeReportVo> consumeReportMaps = new HashMap<String, UserConsumeReportVo>();
   		//历史盈亏
   		if(MoneyDetailQuery.REPORT_TYPE_HISTORY == query.getReportType()) {
   			//从用户团队每日盈利表中查询
   			consumeReportMaps = getUserConsumeReportFromDayConsume(query);
   			logger.debug("查询用户团队日盈利表耗时["+(System.currentTimeMillis() - startTime)+"]ms");
   		//实时盈亏
   		} else if(MoneyDetailQuery.REPORT_TYPE_TODAY == query.getReportType()) {
   			//如果查询现在之后的时间，设置查询的结束时间为当前时间
   			MoneyDetailQuery todayQuery = new MoneyDetailQuery();
   			BeanUtils.copyProperties(query, todayQuery);
   			
   			Date nowDate = new Date();
   			if(query.getCreatedDateEnd().after(nowDate)) {
   				todayQuery.setCreatedDateEnd(nowDate);
   			}
   			consumeReportMaps = getTodayUserConsumeReport(todayQuery, leaderUser, subUsers, teamUsersMap);
   			
   			//查询用户的所有下级用户,统计所有下级用户的余额
   			//startTime = System.currentTimeMillis();
   			//UserQuery userquery = new UserQuery();
   			//userquery.setTeamLeaderName(leaderNameFormat);
   			//userquery.setRealLeaderName(leaderUserName);
   			//List<User> allSubUsers = userService.getUsersByCondition(userquery);
   			//新增查询领导人自身
   			subUsers.add(leaderUser);
   			//Map<String, BigDecimal> userBalanceMap = getUserBalanceMap(allSubUsers, subUsers);
   			//将今日的余额与用户团队余额进行合并
   			//combineUserDayMoneyMaps(userDayMoneyMaps, userBalanceMap, leaderUser);
   			//logger.debug("查询今日余额明细耗时["+(System.currentTimeMillis() - startTime)+"]ms");
   			
   		}
   		
   		//查询团队领导人自身盈亏数据,重新设置查询条件，只查询领导人自身的资金明细
   		startTime = System.currentTimeMillis();
   		query.setBizSystem(query.getBizSystem());
   		query.setUserName(leaderUserName);
   		query.setTeamLeaderName(null);
   		query.setRealLeaderName(null);
   		UserConsumeReportVo leaderUserConsumeReportVo = getSelfUserConsumeReport(query);
   		consumeReportMaps.put(leaderUserName, leaderUserConsumeReportVo);
   		logger.debug("查询用户领导人自身盈亏耗时["+(System.currentTimeMillis() - startTime)+"]ms");
   		
   		Map<String,UserConsumeReportVo> consumeReportMapsResult = new LinkedHashMap<String, UserConsumeReportVo>();
   		//下级会员数目 
   		for(Map.Entry<String, String> entry:teamUsersMap.entrySet()){ 
   			//不为空才添加
   			if(consumeReportMaps.get(entry.getKey()) != null) {
   				UserConsumeReportVo reportVo = consumeReportMaps.get(entry.getKey());
   				//设置金额
   				UserDayMoney userDayMoney = userDayMoneyMaps.get(entry.getKey());
   				if(userDayMoney != null) {
   					if(leaderUserName.equals(entry.getKey())) {
   						reportVo.setMoney(userDayMoney.getMoney());
   					} else {
   						reportVo.setMoney(userDayMoney.getTeamMoney());
   					}
   				}
   				consumeReportMapsResult.put(entry.getKey(), reportVo);
   			}
   		}   
   		
   		//如果当天自己团队领导人没有消费的话
   		if(consumeReportMapsResult.get(leaderUserName) == null){
   			UserConsumeReportVo reportVo = new UserConsumeReportVo(leaderUserName);
   			UserDayMoney userDayMoney = userDayMoneyMaps.get(leaderUserName);
			if(userDayMoney != null) {
				reportVo.setMoney(userDayMoney.getTeamMoney());
			}
   			consumeReportMapsResult.put(leaderUserName, reportVo);
   		}
   		
   		//设置查询领导人的regFrom 用于展现导航栏
   		UserConsumeReportVo leaderConsumeReportVo = consumeReportMapsResult.get(leaderUserName);
   		leaderConsumeReportVo.setRegfrom(leaderUser.getRegfrom());
   		logger.info("用户["+leaderUserName+"]查询盈亏报表,查询盈亏报表总耗时["+(System.currentTimeMillis() - startTimeAll)+"]ms");
   		
        return consumeReportMapsResult;
   	}
   	
   	/**
   	 * 获取今天的用户消费报表
   	 * @param query
   	 * @param leaderUserName
   	 * @param subUsers
   	 * @return
   	 */
   	private Map<String,UserConsumeReportVo> getTodayUserConsumeReport(MoneyDetailQuery query, User leaderUser, List<User> subUsers, Map<String,String> teamUsersMap) {
   		
   		//获取小时盈亏查询起始时间
   		Date nowDateStart = DateUtil.getNowStartTimeByStart(query.getCreatedDateStart());
   		Date hourQueryDateStart = nowDateStart;
   		Calendar currentDate = new GregorianCalendar();   
   		if(query.getCreatedDateStart().after(nowDateStart)) {
   			currentDate.setTime(query.getCreatedDateStart());
   			currentDate.set(Calendar.MINUTE, 0);  
   	    	currentDate.set(Calendar.SECOND, 0); 
   	    	hourQueryDateStart = currentDate.getTime();
   		}
   		
   		//获取小时盈亏查询结束时间
    	currentDate.setTime(query.getCreatedDateEnd());
    	currentDate.set(Calendar.MINUTE, 0);  
    	currentDate.set(Calendar.SECOND, 0);  
    	//查询结束的当前小时的起始时间
   		Date hourQueryDateEnd = currentDate.getTime();
   		//当前时间的小时起始时间
   		Date nowDateTime = new Date();
   		currentDate.setTime(nowDateTime);
    	currentDate.set(Calendar.MINUTE, 0);  
    	currentDate.set(Calendar.SECOND, 0);
   		Date nowDateHourStart = currentDate.getTime();
   		//计算结束的分钟数
   		int calculateMinute = 30;
   		//获取当前小时计算结束时间
   		Date calculateEndTime = DateUtil.addMinutesToDate(nowDateHourStart, calculateMinute);
   		//查询结束时间在当前小时开始结束计算时间之前,认为小时计算数据还未完成,查询小时盈亏的时间应该再推前一个小时
   		if(query.getCreatedDateEnd().before(calculateEndTime)) {
   			hourQueryDateEnd = DateUtil.addHoursToDate(hourQueryDateEnd, -1);
   		}
   		
   		//是否需要查询小时盈亏表
   		boolean isQueryHourConsume = true;
   		if(hourQueryDateEnd.before(hourQueryDateStart)) {
   			isQueryHourConsume = false;
   		}
   		//查询小时盈亏表
   		String leaderUserName = leaderUser.getUserName();
   		String leaderNameFormat = ConstantUtil.REG_FORM_SPLIT + leaderUserName + ConstantUtil.REG_FORM_SPLIT;
   		if(EUserDailLiLevel.SUPERAGENCY.getCode().equals(leaderUser.getDailiLevel())) {
   			leaderNameFormat = leaderUser.getUserName() + ConstantUtil.REG_FORM_SPLIT;
    	} 
   		Map<String,UserConsumeReportVo> todayHourUserConsumeReport = null;
   		if(isQueryHourConsume) {
   			try{
   				todayHourUserConsumeReport = getUserConsumeReportFromHourConsume(hourQueryDateStart, hourQueryDateEnd, leaderUserName, leaderNameFormat,leaderUser.getBizSystem());
   			} catch(Exception e) {
   				logger.error("查询小时盈亏数据出错...", e);
   			}
   			//资金明细的查询时间应该是小时盈亏数据结束时间
   			query.setCreatedDateStart(hourQueryDateEnd);
   		}
   		
   		//接下来查询未在小时盈亏计算时间之内的资金明细
   		Long startTime = System.currentTimeMillis();   		
   		List<MoneyDetail> moneyDetails = moneyDetailMapper.queryMoneyDetailsByCondition(query);
   		logger.debug("查询今日资金明细表耗时["+(System.currentTimeMillis() - startTime)+"]ms");   	
   		List<MoneyDetail> totalMoneyDetails = new ArrayList<MoneyDetail>();
   		String [] regFromUserNames = null;   		
   			
   		startTime = System.currentTimeMillis(); 
   		for(MoneyDetail moneyDetail : moneyDetails){
   			regFromUserNames = moneyDetail.getRegfrom().substring(moneyDetail.getRegfrom().indexOf(leaderNameFormat) + 1, moneyDetail.getRegfrom().length()).split(ConstantUtil.REG_FORM_SPLIT);
   			for(String regFromUserName : regFromUserNames){
   				if(regFromUserName.equals(ConstantUtil.FRONT_ADMIN_USER_NAME)){
   				   continue;	
   				}
   				//只拷贝到直接下级用户下
   				if(!regFromUserName.equals(leaderUserName) && teamUsersMap.get(regFromUserName) != null){
   	   				totalMoneyDetails.add(moneyDetail.copy(regFromUserName));
   				}
   			} 
   			//只拷贝到直接下级用户   (团队领导人资金明细不做拷贝)
   			if(teamUsersMap.get(moneyDetail.getUserName()) != null) {
   				totalMoneyDetails.add(moneyDetail);
   			}
   		}
   		
   		Map<String,UserConsumeReportVo> consumeReportMaps = this.getMoneyDetailForuserMap(totalMoneyDetails, null);
   		Iterator<Map.Entry<String, UserConsumeReportVo>> entries = consumeReportMaps.entrySet().iterator();  
   	  
   		while (entries.hasNext()) {  
   		  
   		    Map.Entry<String, UserConsumeReportVo> entry = entries.next();
   		   UserConsumeReportVo userConsumeReportVoEntry = entry.getValue();
   		    User user = userService.getUserByName(userConsumeReportVoEntry.getBizSystem(), userConsumeReportVoEntry.getUserName());
   		    //统计注册人数，首冲人数，投注人数
   		    if(user != null){
				TeamUserQuery teamUserQuery = new TeamUserQuery();
				teamUserQuery.setBizSystem(user.getBizSystem());
				teamUserQuery.setRegisterDateStart(query.getCreatedDateStart());
				teamUserQuery.setTeamLeaderName(user.getRegfrom()+user.getUserName()+"&");
				teamUserQuery.setUserName(user.getUserName());
				OrderQuery orderQuery =  new OrderQuery();
				orderQuery.setBizSystem(user.getBizSystem());
				orderQuery.setTeamLeaderName(user.getRegfrom()+user.getUserName()+"&");
				orderQuery.setCreatedDateStart(query.getCreatedDateStart());
				orderQuery.setUserName(user.getUserName());
				Integer regCount = userService.getTeamUserRegistersByQueryCount(teamUserQuery);
				Integer firstRechargeCount = userService.getTeamUserfirstrechargeTimeByQueryCount(teamUserQuery);
			    Integer lotteryCount = userService.getTeamUserLotteryByQueryCount(orderQuery);
			    userConsumeReportVoEntry.setRegCount(regCount);
			    userConsumeReportVoEntry.setFirstRechargeCount(firstRechargeCount);
			    userConsumeReportVoEntry.setLotteryCount(lotteryCount);
   		    }else{
   		        userConsumeReportVoEntry.setRegCount(0);
			    userConsumeReportVoEntry.setFirstRechargeCount(0);
			    userConsumeReportVoEntry.setLotteryCount(0);
   		    }
   		  
   		} 
   		logger.debug("计算资金明细记录耗时["+(System.currentTimeMillis() - startTime)+"]ms");
   		//合并记录
   		if(todayHourUserConsumeReport != null) {
   			combineUserConsumeReport(consumeReportMaps, todayHourUserConsumeReport);
   		}
   		//logger.info("今日资金明细记录拷贝耗时["+(System.currentTimeMillis() - startTime)+"]ms");
   		return consumeReportMaps;
   	}
   	
   	/**
   	 * 查询用户团队日盈利表获取数据
   	 * @param query
   	 * @return
   	 */
   	private Map<String,UserConsumeReportVo> getUserConsumeReportFromDayConsume(MoneyDetailQuery query) {
   		Map<String,UserConsumeReportVo> consumeReportMaps = new HashMap<String, UserConsumeReportVo>();
   		UserDayConsumeQuery userDayConsumeQuery = new UserDayConsumeQuery();
   		userDayConsumeQuery.setCreatedDateStart(query.getCreatedDateStart());
   		userDayConsumeQuery.setCreatedDateEnd(query.getCreatedDateEnd());
   		userDayConsumeQuery.setTeamLeaderName(query.getTeamLeaderName());
   		userDayConsumeQuery.setRealLeaderName(query.getRealLeaderName());
   		userDayConsumeQuery.setBizSystem(query.getBizSystem());
   		List<UserDayConsume> userDayConsumes = userDayConsumeService.getUserDayConsumeByCondition(userDayConsumeQuery);
   		
   		if(CollectionUtils.isNotEmpty(userDayConsumes)) {
   			for(UserDayConsume userDayConsume : userDayConsumes) {
   				UserConsumeReportVo userConsumeReportVo = new UserConsumeReportVo();
   				if(userDayConsume == null) {
   					continue;
   				}
   				BeanUtils.copyProperties(userDayConsume, userConsumeReportVo);

   				userConsumeReportVo.setRechargePresent(userDayConsume.getRechargepresent());
   				userConsumeReportVo.setActivitiesMoney(userDayConsume.getActivitiesmoney());
   				userConsumeReportVo.setBizSystem(userDayConsume.getBizSystem());
   				userConsumeReportVo.setWithDraw(userDayConsume.getWithdraw());
   				userConsumeReportVo.setWithDrawFee(userDayConsume.getWithdrawfee());
   				userConsumeReportVo.setSystemaddmoney(userDayConsume.getSystemaddmoney());
   				userConsumeReportVo.setSystemreducemoney(userDayConsume.getSystemreducemoney());
   				userConsumeReportVo.setPayTranfer(userDayConsume.getPaytranfer());
   				userConsumeReportVo.setIncomeTranfer(userDayConsume.getIncometranfer());
   				userConsumeReportVo.setHalfMonthBonus(userDayConsume.getHalfmonthbonus());
   				userConsumeReportVo.setDayBonus(userDayConsume.getDaybonus());
   				userConsumeReportVo.setMonthSalary(userDayConsume.getMonthsalary());
   				userConsumeReportVo.setDaySalary(userDayConsume.getDaysalary());
   				userConsumeReportVo.setDayCommission(userDayConsume.getDaycommission());
   				userConsumeReportVo.setUserId(userDayConsume.getUserId());
   				
   				TeamUserQuery teamUserQuery = new TeamUserQuery();
 				teamUserQuery.setBizSystem(query.getBizSystem());
				teamUserQuery.setRegisterDateStart(query.getCreatedDateStart());
				teamUserQuery.setRegisterDateEnd(query.getCreatedDateEnd());
				teamUserQuery.setUserName(userDayConsume.getUserName());
				teamUserQuery.setTeamLeaderName(userDayConsume.getRegfrom()+userDayConsume.getUserName()+"&");
				OrderQuery orderQuery =  new OrderQuery();
				orderQuery.setBizSystem(query.getBizSystem());
				orderQuery.setTeamLeaderName(userDayConsume.getRegfrom()+userDayConsume.getUserName()+"&");
				orderQuery.setCreatedDateStart(query.getCreatedDateStart());
				orderQuery.setCreatedDateEnd(query.getCreatedDateEnd());
				orderQuery.setUserName(userDayConsume.getUserName());
				Integer regCount = userService.getTeamUserRegistersByQueryCount(teamUserQuery);
				Integer firstRechargeCount = userService.getTeamUserfirstrechargeTimeByQueryCount(teamUserQuery);
			    Integer lotteryCount = userService.getTeamUserLotteryByQueryCount(orderQuery);
			    userConsumeReportVo.setRegCount(regCount);
			    userConsumeReportVo.setFirstRechargeCount(firstRechargeCount);
			    userConsumeReportVo.setLotteryCount(lotteryCount);
   				
   				/**
   				 * userConsumeReportVo.setFirstRechargeCount(userDayConsume.getFirstRechargeCount());
   				 * userConsumeReportVo.setRegCount(userDayConsume.getRegCount());
   				userConsumeReportVo.setLotteryCount(userDayConsume.getLotteryCount());
   				 */
   				
   				consumeReportMaps.put(userDayConsume.getUserName(), userConsumeReportVo);
   			}
   		}
   		return consumeReportMaps;
	}
   	
   	/**
   	 * 查询用户团队小时盈利表获取数据,直接下级的数据
   	 * @param query
   	 * @return
   	 */
   	private Map<String,UserConsumeReportVo> getUserConsumeReportFromHourConsume(Date hourQueryDateStart, Date hourQueryDateEnd, String leaderUserName, String leaderNameFormat,String bizSystem) {
   		Map<String,UserConsumeReportVo> consumeReportMaps = new HashMap<String, UserConsumeReportVo>();
   		UserHourConsumeQuery userHourConsumeQuery = new UserHourConsumeQuery();
   		userHourConsumeQuery.setCreatedDateStart(hourQueryDateStart);
   		userHourConsumeQuery.setCreatedDateEnd(hourQueryDateEnd);
   		userHourConsumeQuery.setTeamLeaderName(leaderNameFormat);
   		userHourConsumeQuery.setRealLeaderName(leaderUserName);
   		userHourConsumeQuery.setBizSystem(bizSystem);
   		List<UserHourConsume> userHourConsumes = userHourConsumeService.getUserHourConsumeByCondition(userHourConsumeQuery);
   		
   		if(CollectionUtils.isNotEmpty(userHourConsumes)) {
   			for(UserHourConsume userHourConsume : userHourConsumes) {
   				UserConsumeReportVo userConsumeReportVo = new UserConsumeReportVo();
   				if(userHourConsume == null) {
   					continue;
   				}
   				BeanUtils.copyProperties(userHourConsume, userConsumeReportVo);

   				userConsumeReportVo.setRechargePresent(userHourConsume.getRechargepresent());
   				userConsumeReportVo.setActivitiesMoney(userHourConsume.getActivitiesmoney());
   			//	userConsumeReportVo.setNetRecharge(userHourConsume.getNetrecharge());
   			//	userConsumeReportVo.setQuickRecharge(userHourConsume.getQuickrecharge());
   				userConsumeReportVo.setWithDraw(userHourConsume.getWithdraw());
   				userConsumeReportVo.setWithDrawFee(userHourConsume.getWithdrawfee());
   				userConsumeReportVo.setSystemaddmoney(userHourConsume.getSystemaddmoney());
   				userConsumeReportVo.setSystemreducemoney(userHourConsume.getSystemreducemoney());
   				userConsumeReportVo.setBizSystem(userHourConsume.getBizSystem());
   				//userConsumeReportVo.setTotalExtend(userHourConsume.getTotalextend());
   				//userConsumeReportVo.setShopPoint(userHourConsume.getShoppoint());
   				userConsumeReportVo.setPayTranfer(userHourConsume.getPaytranfer());
   				userConsumeReportVo.setIncomeTranfer(userHourConsume.getIncometranfer());
   				userConsumeReportVo.setHalfMonthBonus(userHourConsume.getHalfmonthbonus());
   				userConsumeReportVo.setDayBonus(userHourConsume.getDaybonus());
   				userConsumeReportVo.setMonthSalary(userHourConsume.getMonthsalary());
   				userConsumeReportVo.setDaySalary(userHourConsume.getDaysalary());
   				userConsumeReportVo.setDayCommission(userHourConsume.getDaycommission());
   				userConsumeReportVo.setUserId(userHourConsume.getUserId());
   				
//   				TeamUserQuery teamUserQuery = new TeamUserQuery();
// 				teamUserQuery.setBizSystem(bizSystem);
//				teamUserQuery.setRegisterDateStart(hourQueryDateStart);
//				teamUserQuery.setRegisterDateEnd(hourQueryDateEnd);
//				teamUserQuery.setTeamLeaderName(userHourConsume.getRegfrom()+userHourConsume.getUserName()+"&");
//				teamUserQuery.setUserName(userHourConsume.getUserName());
//				OrderQuery orderQuery =  new OrderQuery();
//				orderQuery.setBizSystem(bizSystem);
//				orderQuery.setTeamLeaderName(userHourConsume.getRegfrom()+userHourConsume.getUserName()+"&");
//				orderQuery.setCreatedDateStart(hourQueryDateStart);
//				orderQuery.setCreatedDateEnd(hourQueryDateEnd);
//				orderQuery.setUserName(userHourConsume.getUserName());
//				Integer regCount = userService.getTeamUserRegistersByQueryCount(teamUserQuery);
//				Integer firstRechargeCount = userService.getTeamUserfirstrechargeTimeByQueryCount(teamUserQuery);
//			    Integer lotteryCount = userService.getTeamUserLotteryByQueryCount(orderQuery);
			    userConsumeReportVo.setRegCount(0);
			    userConsumeReportVo.setFirstRechargeCount(0);
			    userConsumeReportVo.setLotteryCount(0);
   				/**
   				 * userConsumeReportVo.setRegCount(userHourConsume.getRegCount());
   				userConsumeReportVo.setLotteryCount(userHourConsume.getLotteryCount());
   				consumeReportMaps.put(userHourConsume.getUserName(), userConsumeReportVo);
   				 */
   				
   			}
   		}
   		return consumeReportMaps;
	}
   	
   	/**
   	 * 将用户团队盈利报表数据合并，结果合并到m1中
   	 * @param m1
   	 * @param m2
   	 */
   	public void combineUserConsumeReport(Map<String,UserConsumeReportVo> m1, Map<String,UserConsumeReportVo> m2) {
   		if(m2.isEmpty()) {
   			return;
   		}
   		Iterator<String> keysIterator = m2.keySet().iterator();
   		while(keysIterator.hasNext()) {
   			String key = keysIterator.next();
   			
   			UserConsumeReportVo userConsumeReportVo = m1.get(key);
   			//有存在相同的key则进行合并
   			if(userConsumeReportVo != null) {
   				UserConsumeReportVo userConsumeReportVo2 = m2.get(key);
   				userConsumeReportVo.setMoney(userConsumeReportVo.getMoney().add(userConsumeReportVo2.getMoney()));
   				userConsumeReportVo.setRecharge(userConsumeReportVo.getRecharge().add(userConsumeReportVo2.getRecharge()));
   				userConsumeReportVo.setRechargePresent(userConsumeReportVo.getRechargePresent().add(userConsumeReportVo2.getRechargePresent()));
   				userConsumeReportVo.setActivitiesMoney(userConsumeReportVo.getActivitiesMoney().add(userConsumeReportVo2.getActivitiesMoney()));
   				userConsumeReportVo.setSystemaddmoney(userConsumeReportVo.getSystemaddmoney().add(userConsumeReportVo2.getSystemaddmoney()));
   				userConsumeReportVo.setSystemreducemoney(userConsumeReportVo.getSystemreducemoney().add(userConsumeReportVo2.getSystemreducemoney()));
   				userConsumeReportVo.setWithDraw(userConsumeReportVo.getWithDraw().add(userConsumeReportVo2.getWithDraw()));
   				userConsumeReportVo.setWithDrawFee(userConsumeReportVo.getWithDrawFee().add(userConsumeReportVo2.getWithDrawFee()));
   				userConsumeReportVo.setExtend(userConsumeReportVo.getExtend().add(userConsumeReportVo2.getExtend()));
   				userConsumeReportVo.setRegister(userConsumeReportVo.getRegister().add(userConsumeReportVo2.getRegister()));
   				userConsumeReportVo.setLottery(userConsumeReportVo.getLottery().add(userConsumeReportVo2.getLottery()));
   				userConsumeReportVo.setWin(userConsumeReportVo.getWin().add(userConsumeReportVo2.getWin()));
   				userConsumeReportVo.setRebate(userConsumeReportVo.getRebate().add(userConsumeReportVo2.getRebate()));
   				userConsumeReportVo.setPercentage(userConsumeReportVo.getPercentage().add(userConsumeReportVo2.getPercentage()));
   				userConsumeReportVo.setPayTranfer(userConsumeReportVo.getPayTranfer().add(userConsumeReportVo2.getPayTranfer()));
   				userConsumeReportVo.setIncomeTranfer(userConsumeReportVo.getIncomeTranfer().add(userConsumeReportVo2.getIncomeTranfer()));
   				userConsumeReportVo.setHalfMonthBonus(userConsumeReportVo.getHalfMonthBonus().add(userConsumeReportVo2.getHalfMonthBonus()));
   				userConsumeReportVo.setDayBonus(userConsumeReportVo.getDayBonus().add(userConsumeReportVo2.getDayBonus()));
   				userConsumeReportVo.setMonthSalary(userConsumeReportVo.getMonthSalary().add(userConsumeReportVo2.getMonthSalary()));
   				userConsumeReportVo.setDaySalary(userConsumeReportVo.getDaySalary().add(userConsumeReportVo2.getDaySalary()));
   				userConsumeReportVo.setDayCommission(userConsumeReportVo.getDayCommission().add(userConsumeReportVo2.getDayCommission()));
   				userConsumeReportVo.setGain(userConsumeReportVo.getGain().add(userConsumeReportVo2.getGain()));
   				userConsumeReportVo.setRegCount((userConsumeReportVo.getRegCount()==null?0:userConsumeReportVo.getRegCount())+(userConsumeReportVo2.getRegCount()==null?0:userConsumeReportVo2.getRegCount()));
   				userConsumeReportVo.setFirstRechargeCount((userConsumeReportVo.getFirstRechargeCount()==null?0:userConsumeReportVo.getFirstRechargeCount())+(userConsumeReportVo2.getFirstRechargeCount()==null?0:userConsumeReportVo2.getFirstRechargeCount()));
   				userConsumeReportVo.setLotteryCount((userConsumeReportVo.getLotteryCount()==null?0:userConsumeReportVo.getLotteryCount())+(userConsumeReportVo2.getLotteryCount() == null?0:userConsumeReportVo2.getLotteryCount()));
   			//不存在m2对应的key则直接添加
   			} else {
   				m1.put(key, m2.get(key));
   			}
   		}
   	}
   	
	/**
   	 * 查询用户团队日余额表获取数据
   	 * @param query
   	 * @return
   	 */
//   	private Map<String,UserDayMoney> getUserDayMoneyMaps(MoneyDetailQuery query) {
//   		Map<String,UserDayMoney> userDayMoneyMaps = new HashMap<String, UserDayMoney>();
//   		UserDayMoneyQuery userDayMoneyQuery = new UserDayMoneyQuery();
//   		userDayMoneyQuery.setCreatedDateStart(query.getCreatedDateStart());
//   		userDayMoneyQuery.setCreatedDateEnd(query.getCreatedDateEnd());
//   		userDayMoneyQuery.setTeamLeaderName(query.getTeamLeaderName());
//   		userDayMoneyQuery.setRealLeaderName(query.getRealLeaderName());
//   		List<UserDayMoney> userDayMoneys = userDayMoneyService.getUserDayMoneyByCondition(userDayMoneyQuery);
//   		
//   		if(CollectionUtils.isNotEmpty(userDayMoneys)) {
//   			for(UserDayMoney userDayMoney : userDayMoneys) {
//   				userDayMoneyMaps.put(userDayMoney.getUserName(), userDayMoney);
//   			}
//   		}
//   		return userDayMoneyMaps;
//	}
   	
   	/**
   	 * 根据所有用户列表和要查询的用户列表的出团队的余额
   	 * @param users
   	 * @param queryUsers
   	 * @return
   	 */
   	private Map<String, BigDecimal> getUserBalanceMap(List<User> users, List<User> queryUsers) {
   		Map<String, BigDecimal> userBalanceMap = new HashMap<String, BigDecimal>();
   		if(CollectionUtils.isNotEmpty(users)) {
   			for(User user : users) {
   				for(User queryUser : queryUsers) {
   					//如果是查询用户的下级或者当前用户是查询用户，累加金额
   					if(UserNameUtil.isInRegfrom(user.getRegfrom(),queryUser.getUserName()) || user.getUserName().equals(queryUser.getUserName())) {
   						BigDecimal userBalance = userBalanceMap.get(queryUser.getUserName());
   						if(userBalance == null) {
   							userBalance = user.getMoney();
   							userBalanceMap.put(queryUser.getUserName(), userBalance);
   						} else {
   							userBalance = userBalance.add(user.getMoney());
   							userBalanceMap.put(queryUser.getUserName(), userBalance);
   						}
   					}
   				}
   			}
   		}
   		return userBalanceMap;
   	}
   	
   	/**
   	 * 合并用户团队日余额数据
   	 * @param userDayMoneyMaps
   	 * @param userBalanceMap
   	 */
   	private void combineUserDayMoneyMaps(Map<String, UserDayMoney> userDayMoneyMaps, Map<String, BigDecimal> userBalanceMap, User leaderUser) {
   		Iterator<String> keyIterator = userBalanceMap.keySet().iterator();
   		while(keyIterator.hasNext()) {
   			String key = keyIterator.next();
   			BigDecimal userBalance = userBalanceMap.get(key);
   			UserDayMoney userDayMoney = userDayMoneyMaps.get(key);
   			if(userDayMoney != null) {
   				userDayMoney.setTeamMoney(userDayMoney.getTeamMoney().add(userBalance));
   				//累计领导人
   				if(key.equals(leaderUser.getUserName())) {
   					userDayMoney.setMoney(userDayMoney.getMoney().add(leaderUser.getMoney()));
   				}
   			} else {
   				UserDayMoney userDayMoneyNew = new UserDayMoney();
   				userDayMoneyNew.setUserName(key);
   				userDayMoneyNew.setTeamMoney(userBalance);
   				userDayMoneyNew.setMoney(BigDecimal.ZERO);
   				//累计领导人
   				if(key.equals(leaderUser.getUserName())) {
   					userDayMoneyNew.setMoney(userDayMoneyNew.getMoney().add(leaderUser.getMoney()));
   				}
   				userDayMoneyMaps.put(key, userDayMoneyNew);
   			}
   		}
   	}
   	
   	/**
   	 * 查询用户自身盈亏
   	 * @param query
   	 * @return
   	 */
   	public UserConsumeReportVo getSelfUserConsumeReport(MoneyDetailQuery query) {
   		UserConsumeReportVo userConsumeReportVo = new UserConsumeReportVo();
   		userConsumeReportVo.setUserName(query.getUserName());
   		userConsumeReportVo.setMoney(new BigDecimal(0)); //用户余额
   		userConsumeReportVo.setRecharge(new BigDecimal(0));
   		userConsumeReportVo.setRechargePresent(new BigDecimal(0));
   		userConsumeReportVo.setActivitiesMoney(new BigDecimal(0));  //活动彩金
    	
    	userConsumeReportVo.setWithDraw(new BigDecimal(0));
    	userConsumeReportVo.setWithDrawFee(new BigDecimal(0));
    	userConsumeReportVo.setExtend(new BigDecimal(0));
    	userConsumeReportVo.setRegister(new BigDecimal(0));
    	userConsumeReportVo.setLottery(new BigDecimal(0));
    	userConsumeReportVo.setGain(new BigDecimal(0));
    	userConsumeReportVo.setWin(new BigDecimal(0));
    	userConsumeReportVo.setRebate(new BigDecimal(0));
    	
    	userConsumeReportVo.setPercentage(new BigDecimal(0));
    	userConsumeReportVo.setHalfMonthBonus(new BigDecimal(0));
    	userConsumeReportVo.setDayBonus(new BigDecimal(0));
    	userConsumeReportVo.setDaySalary(new BigDecimal(0));
    	userConsumeReportVo.setMonthSalary(new BigDecimal(0));
    	userConsumeReportVo.setDayCommission(new BigDecimal(0));
    	userConsumeReportVo.setPayTranfer(new BigDecimal(0));
    	userConsumeReportVo.setIncomeTranfer(new BigDecimal(0));
    	userConsumeReportVo.setSystemaddmoney(new BigDecimal(0));
    	userConsumeReportVo.setSystemreducemoney(new BigDecimal(0));
    	userConsumeReportVo.setBizSystem(query.getBizSystem());
    	userConsumeReportVo.setRegCount(0);
    	userConsumeReportVo.setLotteryCount(0);
    	userConsumeReportVo.setFirstRechargeCount(0);
    	Long startTime = System.currentTimeMillis();
    	
   		User queryUser = userService.getUserByName(query.getBizSystem(),query.getUserName());
   		UserConsumeReportVo userTodayConsumeReportVo = null;
   		
   		//历史盈亏
   		if(MoneyDetailQuery.REPORT_TYPE_HISTORY == query.getReportType()) {
   			UserDayConsumeQuery userDayConsumeQuery = new UserDayConsumeQuery();
   			userDayConsumeQuery.setCreatedDateStart(query.getCreatedDateStart());
   			userDayConsumeQuery.setCreatedDateEnd(query.getCreatedDateEnd());
   			userDayConsumeQuery.setUserName(query.getUserName());
   			userDayConsumeQuery.setBizSystem(query.getBizSystem());
   			UserSelfDayConsume userSelfDayConsumes = userSelfDayConsumeService.getUserSelfDayConsumeByCondition(userDayConsumeQuery);
   			if(userSelfDayConsumes != null) {
   				BeanUtils.copyProperties(userSelfDayConsumes, userConsumeReportVo);
   				userConsumeReportVo.setRechargePresent(userSelfDayConsumes.getRechargepresent());
   				userConsumeReportVo.setActivitiesMoney(userSelfDayConsumes.getActivitiesmoney());
   				
   				userConsumeReportVo.setWithDraw(userSelfDayConsumes.getWithdraw());
   				userConsumeReportVo.setWithDrawFee(userSelfDayConsumes.getWithdrawfee());
   				userConsumeReportVo.setSystemaddmoney(userSelfDayConsumes.getSystemaddmoney());
   				userConsumeReportVo.setSystemreducemoney(userSelfDayConsumes.getSystemreducemoney());
   				userConsumeReportVo.setPayTranfer(userSelfDayConsumes.getPaytranfer());
   				userConsumeReportVo.setIncomeTranfer(userSelfDayConsumes.getIncometranfer());
   				userConsumeReportVo.setHalfMonthBonus(userSelfDayConsumes.getHalfmonthbonus());
   				userConsumeReportVo.setDayBonus(userSelfDayConsumes.getDaybonus());
   				userConsumeReportVo.setMonthSalary(userSelfDayConsumes.getMonthsalary());
   				userConsumeReportVo.setDaySalary(userSelfDayConsumes.getDaysalary());
   				userConsumeReportVo.setDayCommission(userSelfDayConsumes.getDaycommission());
   				
   			}
   		//实时盈亏查询
		} else if(MoneyDetailQuery.REPORT_TYPE_TODAY == query.getReportType()) {
			MoneyDetailQuery todayQuery = new MoneyDetailQuery();
   			BeanUtils.copyProperties(query, todayQuery);
   			
   			//查询用户的所有资金明细  按每2小时进行查询 (加快查询速度)
   	   		List<MoneyDetail> allMoneyDetails = new ArrayList<MoneyDetail>();
   	   		startTime = System.currentTimeMillis();
   	   		Date moneyDetailQueryEnd = todayQuery.getCreatedDateEnd();
   	   		Date perMoneyDetailQueryEnd = DateUtil.addHoursToDate(todayQuery.getCreatedDateStart(), 2);
   	   		boolean isQueryContinue = true;
   	   		while(isQueryContinue) {
   	   			if(moneyDetailQueryEnd.before(perMoneyDetailQueryEnd)) {
   	   				isQueryContinue = false;
   	   				perMoneyDetailQueryEnd = moneyDetailQueryEnd;
   	   			}
   	   			todayQuery.setCreatedDateEnd(perMoneyDetailQueryEnd);   	   			
   	   			List<MoneyDetail> moneyDetails = moneyDetailMapper.queryMoneyDetailsByCondition(todayQuery);
   	   			if(CollectionUtils.isNotEmpty(moneyDetails)) {
   	   				allMoneyDetails.addAll(moneyDetails);
   	   			}
   	   			todayQuery.setCreatedDateStart(perMoneyDetailQueryEnd);
   	   			perMoneyDetailQueryEnd = DateUtil.addHoursToDate(todayQuery.getCreatedDateStart(), 2);
   	   		}
   			logger.debug("查询用户领导人自身盈亏耗时["+(System.currentTimeMillis() - startTime)+"]ms");
   			startTime = System.currentTimeMillis();
   			if(CollectionUtils.isNotEmpty(allMoneyDetails)) {
   				
   				Map<String,UserConsumeReportVo> consumeReportMaps = this.getMoneyDetailForuserMap(allMoneyDetails, null);
   				userTodayConsumeReportVo = consumeReportMaps.get(query.getUserName());
   				//合并数据
   				if(userTodayConsumeReportVo != null) {
   					userConsumeReportVo.setMoney(userConsumeReportVo.getMoney().add(userTodayConsumeReportVo.getMoney()));
   	   				userConsumeReportVo.setRecharge(userConsumeReportVo.getRecharge().add(userTodayConsumeReportVo.getRecharge()));
   	   				userConsumeReportVo.setRechargePresent(userConsumeReportVo.getRechargePresent().add(userTodayConsumeReportVo.getRechargePresent()));
   	   				userConsumeReportVo.setActivitiesMoney(userConsumeReportVo.getActivitiesMoney().add(userTodayConsumeReportVo.getActivitiesMoney()));
   	   			    userConsumeReportVo.setSystemaddmoney(userConsumeReportVo.getSystemaddmoney().add(userTodayConsumeReportVo.getSystemaddmoney()));
   	   			    userConsumeReportVo.setSystemreducemoney(userConsumeReportVo.getSystemreducemoney().add(userTodayConsumeReportVo.getSystemreducemoney()));
	   			    
   	   				userConsumeReportVo.setWithDraw(userConsumeReportVo.getWithDraw().add(userTodayConsumeReportVo.getWithDraw()));
   	   				userConsumeReportVo.setWithDrawFee(userConsumeReportVo.getWithDrawFee().add(userTodayConsumeReportVo.getWithDrawFee()));
   	   				userConsumeReportVo.setExtend(userConsumeReportVo.getExtend().add(userTodayConsumeReportVo.getExtend()));
   	   				userConsumeReportVo.setRegister(userConsumeReportVo.getRegister().add(userTodayConsumeReportVo.getRegister()));
   	   				userConsumeReportVo.setLottery(userConsumeReportVo.getLottery().add(userTodayConsumeReportVo.getLottery()));
   	   				userConsumeReportVo.setWin(userConsumeReportVo.getWin().add(userTodayConsumeReportVo.getWin()));
   	   				userConsumeReportVo.setRebate(userConsumeReportVo.getRebate().add(userTodayConsumeReportVo.getRebate()));
   	   				userConsumeReportVo.setPercentage(userConsumeReportVo.getPercentage().add(userTodayConsumeReportVo.getPercentage()));
   	   				userConsumeReportVo.setPayTranfer(userConsumeReportVo.getPayTranfer().add(userTodayConsumeReportVo.getPayTranfer()));
   	   				userConsumeReportVo.setIncomeTranfer(userConsumeReportVo.getIncomeTranfer().add(userTodayConsumeReportVo.getIncomeTranfer()));
   	   				userConsumeReportVo.setHalfMonthBonus(userConsumeReportVo.getHalfMonthBonus().add(userTodayConsumeReportVo.getHalfMonthBonus()));
   	   				userConsumeReportVo.setDayBonus(userConsumeReportVo.getDayBonus().add(userTodayConsumeReportVo.getDayBonus()));
   	   				userConsumeReportVo.setMonthSalary(userConsumeReportVo.getMonthSalary().add(userTodayConsumeReportVo.getMonthSalary()));
   	   				userConsumeReportVo.setDaySalary(userConsumeReportVo.getDaySalary().add(userTodayConsumeReportVo.getDaySalary()));
   	   				userConsumeReportVo.setDayCommission(userConsumeReportVo.getDayCommission().add(userTodayConsumeReportVo.getDayCommission()));
   	   				userConsumeReportVo.setGain(userConsumeReportVo.getGain().add(userTodayConsumeReportVo.getGain()));
   				}
   			}
		}
   		
   		//设置余额
		if(userConsumeReportVo != null) {
			userConsumeReportVo.setUserId(queryUser.getId());
   			userConsumeReportVo.setMoney(queryUser.getMoney());
   		 //统计注册人数，首冲人数，投注人数
//			TeamUserQuery teamUserQuery = new TeamUserQuery();
//			teamUserQuery.setBizSystem(queryUser.getBizSystem());
//			teamUserQuery.setRegisterDateStart(query.getCreatedDateStart());
//			teamUserQuery.setRegisterDateEnd(query.getCreatedDateEnd());
//			teamUserQuery.setTeamLeaderName(queryUser.getRegfrom()+queryUser.getUserName()+"&");
//			teamUserQuery.setUserName(queryUser.getUserName());
//			OrderQuery orderQuery =  new OrderQuery();
//			orderQuery.setBizSystem(queryUser.getBizSystem());
//			orderQuery.setTeamLeaderName(queryUser.getRegfrom()+queryUser.getUserName()+"&");
//			orderQuery.setCreatedDateStart(query.getCreatedDateStart());
//			orderQuery.setCreatedDateEnd(query.getCreatedDateEnd());
//			orderQuery.setUserName(queryUser.getUserName());
//			Integer regCount = userService.getTeamUserRegistersByQueryCount(teamUserQuery);
//			Integer firstRechargeCount = userService.getTeamUserfirstrechargeTimeByQueryCount(teamUserQuery);
//		    Integer lotteryCount = userService.getTeamUserLotteryByQueryCount(orderQuery);
		    userConsumeReportVo.setRegCount(0);
		    userConsumeReportVo.setFirstRechargeCount(0);
		    userConsumeReportVo.setLotteryCount(0);
   			
   		}
		logger.debug("查询用户领导人自身盈亏拷贝耗时["+(System.currentTimeMillis() - startTime)+"]ms");
   		return userConsumeReportVo;
   	}
   	
   	/**
   	 * 查询用户自身盈亏
   	 * @deprecated
   	 * @param query
   	 * @return
   	 */
   	public UserConsumeReportVo getSelfUserConsumeReportOld(MoneyDetailQuery query) {
   		UserConsumeReportVo userConsumeReportVo = null;
   	
   		User queryUser = userService.getUserByName(query.getBizSystem(),query.getUserName());
   		Long startTime = System.currentTimeMillis();
   		//查询用户的所有资金明细
   		List<MoneyDetail> moneyDetails = moneyDetailMapper.queryMoneyDetailsByCondition(query);
   		logger.info("查询用户领导人自身盈亏耗时["+(System.currentTimeMillis() - startTime)+"]ms");
   		
   		startTime = System.currentTimeMillis();
   		if(CollectionUtils.isEmpty(moneyDetails)) {
   			//加入空数据   使得map有返回
   			MoneyDetail moneyDetail = new MoneyDetail();
   			moneyDetail.setDetailType(EMoneyDetailType.RECHARGE.name());
   			moneyDetail.setUserName(query.getUserName());
   			moneyDetail.setMoney(new BigDecimal(0));
   			moneyDetail.setIncome(new BigDecimal(0)); //收入为0
   	        moneyDetail.setPay(new BigDecimal(0)); //支出金额
   	        moneyDetail.setBalanceBefore(new BigDecimal(0)); //支出前金额
   	        moneyDetail.setBalanceAfter(new BigDecimal(0));  //支出后的金额
   			moneyDetails.add(moneyDetail);
   		}
   		Map<String,UserConsumeReportVo> consumeReportMaps = this.getMoneyDetailForuserMap(moneyDetails, null);
		userConsumeReportVo = consumeReportMaps.get(query.getUserName());
   		//设置余额
		if(userConsumeReportVo != null) {
   			userConsumeReportVo.setMoney(queryUser.getMoney());
   		}
		logger.info("用户领导人自身盈亏拷贝耗时["+(System.currentTimeMillis() - startTime)+"]ms");
   		return userConsumeReportVo;
   	}
   	
	
   	/**
   	 * 资金明细列表转成映射
   	 * @param moneyDetails
   	 * @param userName 盈亏计算的归属用户
   	 * @return
   	 */
   	public Map<String,UserConsumeReportVo> getMoneyDetailForuserMap(List<MoneyDetail> moneyDetails, String userName){
   		Map<String,UserConsumeReportVo> consumeReportMaps = new HashMap<String, UserConsumeReportVo>();
   		//盈亏计算的归属用户
   		String userKey = null;
   		if(CollectionUtils.isNotEmpty(moneyDetails)) {
	   		for(MoneyDetail moneyDetail : moneyDetails){
	   			if(StringUtils.isNotBlank(userName)){
	   				userKey = userName;
	   			} else {
	   				userKey = moneyDetail.getUserName();
	   			}
	   			UserConsumeReportVo reportVo = consumeReportMaps.get(userKey);
	   		    if(reportVo == null){
//             		未使用的属性
//		    		reportVo.setUserId(moneyDetail.getUserId());
//   		    	reportVo.setRegfrom(moneyDetail.getRegfrom()); //推荐人
//   		    	reportVo.setDailiLevel(moneyDetail.getDailiLevel()); //会员类型
	   		    	
	   		    	reportVo = new UserConsumeReportVo();
	   		    	reportVo.setUserId(moneyDetail.getUserId());
	   		    	reportVo.setUserName(userKey);
	   		    	reportVo.setMoney(BigDecimal.ZERO); //用户余额
	   		    	reportVo.setRecharge(BigDecimal.ZERO);
	   		    	reportVo.setRechargePresent(BigDecimal.ZERO);
	   		    	reportVo.setActivitiesMoney(BigDecimal.ZERO);  //活动彩金
	   		    	reportVo.setBizSystem(moneyDetail.getBizSystem());
	   		    	reportVo.setWithDraw(BigDecimal.ZERO);
	   		    	reportVo.setWithDrawFee(BigDecimal.ZERO);
	   		    	reportVo.setExtend(BigDecimal.ZERO);
	   		    	reportVo.setRegister(BigDecimal.ZERO);
	   		    	reportVo.setLottery(BigDecimal.ZERO);
	   		    	reportVo.setGain(BigDecimal.ZERO);
	   		    	reportVo.setWin(BigDecimal.ZERO);
	   		    	reportVo.setRebate(BigDecimal.ZERO);
	   		  
	   		    	reportVo.setPercentage(BigDecimal.ZERO);
	   		    	reportVo.setHalfMonthBonus(BigDecimal.ZERO);
	   		    	reportVo.setDayBonus(BigDecimal.ZERO);
	   		    	reportVo.setDaySalary(BigDecimal.ZERO);
	   		    	reportVo.setMonthSalary(BigDecimal.ZERO);
	   		    	reportVo.setDayCommission(BigDecimal.ZERO);
	   		    	reportVo.setPayTranfer(BigDecimal.ZERO);
	   		    	reportVo.setIncomeTranfer(BigDecimal.ZERO);
	   		    	reportVo.setSystemaddmoney(BigDecimal.ZERO);
	   		    	reportVo.setSystemreducemoney(BigDecimal.ZERO);  		    	
	   		    	consumeReportMaps.put(userKey, reportVo);
	   		    }
	   		    if(moneyDetail == null) {
	   		    	logger.info("errr null......");
	   		    }
	   		    String currentMoneyDetailType = moneyDetail.getDetailType();
	   		    if(currentMoneyDetailType == null) {
	   		    	logger.info("errr type null......");
	   		    }
	   		    if(currentMoneyDetailType.equals(EMoneyDetailType.RECHARGE.name())){  //存款(充值)
	   		    	reportVo.setRecharge(reportVo.getRecharge().add(moneyDetail.getIncome()));
	   		    	//不能参与
	   		    }else if(currentMoneyDetailType.equals(EMoneyDetailType.RECHARGE_PRESENT.name())){ //充值赠送
	   		    	reportVo.setRechargePresent(reportVo.getRechargePresent().add(moneyDetail.getIncome()));
	   		    	//充值赠送(系统扣费)
	   		    	reportVo.setRechargePresent(reportVo.getRechargePresent().subtract(moneyDetail.getPay()));
	   		    	
	   		        //盈利计算
	   		    	reportVo.setGain(reportVo.getGain().add(moneyDetail.getIncome()));
	   		    	reportVo.setGain(reportVo.getGain().subtract(moneyDetail.getPay()));
	   		    }else if(currentMoneyDetailType.equals(EMoneyDetailType.ACTIVITIES_MONEY.name())){ //活动彩金
	   		    	reportVo.setActivitiesMoney(reportVo.getActivitiesMoney().add(moneyDetail.getIncome()));
	   		    	//活动彩金(系统扣费)
	   		    	reportVo.setActivitiesMoney(reportVo.getActivitiesMoney().subtract(moneyDetail.getPay()));
	   		    	
	   		        //盈利计算
	   		    	reportVo.setGain(reportVo.getGain().add(moneyDetail.getIncome()));
	   		    	reportVo.setGain(reportVo.getGain().subtract(moneyDetail.getPay()));
	   		    }else if(currentMoneyDetailType.equals(EMoneyDetailType.WITHDRAW.name())){  //取款(在线取款)
	   		    	if(moneyDetail.getIncome().compareTo(new BigDecimal(0)) > 0){
	   	   		    	reportVo.setWithDraw(reportVo.getWithDraw().subtract(moneyDetail.getIncome()));
	   		    	}else{
	   	   		    	reportVo.setWithDraw(reportVo.getWithDraw().add(moneyDetail.getPay()));
	   		    	}
	   		    	//不能参与
	   		    }else if(currentMoneyDetailType.equals(EMoneyDetailType.WITHDRAW_FEE.name())){  //取款手续费
	   		    	reportVo.setWithDrawFee(reportVo.getWithDrawFee().add(moneyDetail.getPay()));
	   		    	reportVo.setWithDrawFee(reportVo.getWithDrawFee().subtract(moneyDetail.getIncome()));
		   		    reportVo.setWithDraw(reportVo.getWithDraw().add(moneyDetail.getPay()));
		   		    reportVo.setWithDraw(reportVo.getWithDraw().subtract(moneyDetail.getIncome()));
		   		    reportVo.setGain(reportVo.getGain().subtract(moneyDetail.getPay()));
		   		    reportVo.setGain(reportVo.getGain().add(moneyDetail.getIncome()));
	   		    }else if(currentMoneyDetailType.equals(EMoneyDetailType.LOTTERY.name())){ //投注  
	   		    	reportVo.setLottery(reportVo.getLottery().add(moneyDetail.getPay()));
	   		    	//盈利计算
	   		    	reportVo.setGain(reportVo.getGain().subtract(moneyDetail.getPay()));  
	   		    }else if(currentMoneyDetailType.equals(EMoneyDetailType.LOTTERY_STOP_AFTER_NUMBER.name())){  //撤单
	   		    	reportVo.setLottery(reportVo.getLottery().subtract(moneyDetail.getIncome()));
	   		    	//盈利计算
	   		    	reportVo.setGain(reportVo.getGain().add(moneyDetail.getIncome()));
	   		    }else if(currentMoneyDetailType.equals(EMoneyDetailType.LOTTERY_REGRESSION.name())){  //管理员投注退单
	   		    	reportVo.setLottery(reportVo.getLottery().subtract(moneyDetail.getIncome()));
	   		    	//盈利计算
	   		    	reportVo.setGain(reportVo.getGain().add(moneyDetail.getIncome()));
	   		    }else if(currentMoneyDetailType.equals(EMoneyDetailType.WIN.name())){  //中奖 
	   		    	reportVo.setWin(reportVo.getWin().add(moneyDetail.getIncome()).subtract(moneyDetail.getPay()));
	   		    	//盈利计算
	   		    	reportVo.setGain(reportVo.getGain().add(moneyDetail.getIncome()).subtract(moneyDetail.getPay()));
	   		    }else if(currentMoneyDetailType.equals(EMoneyDetailType.REBATE.name())){  //返点
	   		    	reportVo.setRebate(reportVo.getRebate().add(moneyDetail.getIncome()).subtract(moneyDetail.getPay()));
	   		    	//盈利计算
	   		    	reportVo.setGain(reportVo.getGain().add(moneyDetail.getIncome()).subtract(moneyDetail.getPay()));
	   		    }else if(currentMoneyDetailType.equals(EMoneyDetailType.PERCENTAGE.name())){  //提成
	   		    	reportVo.setPercentage(reportVo.getPercentage().add(moneyDetail.getIncome()));
	   		    	//盈利计算
	   		    	reportVo.setGain(reportVo.getGain().add(moneyDetail.getIncome()));
	   		    }else if(currentMoneyDetailType.equals(EMoneyDetailType.HALF_MONTH_BOUNS.name())){  //半月分红
	   		    	reportVo.setHalfMonthBonus(reportVo.getHalfMonthBonus().add(moneyDetail.getIncome()));
	   		    }else if(currentMoneyDetailType.equals(EMoneyDetailType.DAY_BOUNS.name())){  //日分红
	   		    	reportVo.setDayBonus(reportVo.getDayBonus().add(moneyDetail.getIncome()));
	   		    }else if(currentMoneyDetailType.equals(EMoneyDetailType.DAY_SALARY.name())){  //日工资
	   		    	reportVo.setDaySalary(reportVo.getDaySalary().add(moneyDetail.getIncome()));
	   		    }else if(currentMoneyDetailType.equals(EMoneyDetailType.MONTH_SALARY.name())){  //月工资
	   		    	reportVo.setMonthSalary(reportVo.getMonthSalary().add(moneyDetail.getIncome()));
	   		    }else if(currentMoneyDetailType.equals(EMoneyDetailType.DAY_COMMISSION.name())){  //日佣金
	   		    	reportVo.setDayCommission(reportVo.getDayCommission().add(moneyDetail.getIncome()));
	   		    }else if(currentMoneyDetailType.equals(EMoneyDetailType.PAY_TRANFER.name())){  //支出转账
	   	    		reportVo.setPayTranfer(reportVo.getPayTranfer().add(moneyDetail.getPay()));
	   	    		//不能参与
		   		    //reportVo.setGain(reportVo.getGain().subtract(moneyDetail.getPay()));
	   		    }else if(currentMoneyDetailType.equals(EMoneyDetailType.INCOME_TRANFER.name())){  //收入转账
	   	    		reportVo.setIncomeTranfer(reportVo.getIncomeTranfer().add(moneyDetail.getIncome()));
	   	    		//不能参与
		   		    //reportVo.setGain(reportVo.getGain().add(moneyDetail.getIncome()));
	   		    }else if(currentMoneyDetailType.equals(EMoneyDetailType.SYSTEM_ADD_MONEY.name())){  //系统续费
	   		    	reportVo.setSystemaddmoney(reportVo.getSystemaddmoney().add(moneyDetail.getIncome()));
	   		    	//盈利计算
	   		    	reportVo.setGain(reportVo.getGain().add(moneyDetail.getIncome()));
	   		    }else if(currentMoneyDetailType.equals(EMoneyDetailType.SYSTEM_REDUCE_MONEY.name())){  //系统扣费
	   		    	reportVo.setSystemreducemoney(reportVo.getSystemreducemoney().add(moneyDetail.getPay()));
	   		    	//盈利计算
	   		    	reportVo.setGain(reportVo.getGain().subtract(moneyDetail.getPay()));
	   		    }
	   		    
	   		    
	   		    /*else if(currentMoneyDetailType.equals(EMoneyDetailType.OUT_WIN_CONTROL.name())){  //超额扣款
		  		    //扣除中奖的  
	   		    	reportVo.setWin(reportVo.getWin().subtract(moneyDetail.getPay()));
	   		    	//盈利计算
	   		    	reportVo.setGain(reportVo.getGain().subtract(moneyDetail.getPay()));
	   		    }*/else{
	   		    	throw new RuntimeException("该资金明细类型不可知.");
	   		    }
	   		}
   		}
   		return consumeReportMaps;
   	}
   	
    /**
     * 查找符合条件的资金明细记录
     * @param query
     * @return
     */
    public List<MoneyDetail> queryMoneyDetailsByCondition(MoneyDetailQuery query) {
    	return moneyDetailMapper.queryMoneyDetailsByCondition(query);
    }
    
    /**
     * 查找团队资金明细记录，包括团队领导人自己
     * @return
     */
	public List<MoneyDetail> queryTeamMoneyDetailsByCondition(@Param("query")MoneyDetailQuery query) {
		return moneyDetailMapper.queryTeamMoneyDetailsByCondition(query);
	}
   	
	/**
	 * 查找某段时间内有资金明细的所有用户
	 * @param query
	 * @return
	 */
	public List<String> getUsersHasMoneyDetail(MoneyDetailQuery query) {
		return moneyDetailMapper.getUsersHasMoneyDetail(query);
	}
	
/*	  *//**
     * 根据传过来的类型，金额统计
     * @param user (userid,bizSystem..)
     * @param operationType
     * @param operationValueStr 扣费为负数，增加为正数
     * @param orderId 订单号id
     * @param operationUserName 当前操作人的名字
     * @param isPay是否是支出的
     * @param headOrderNum 订单头部字母
     * @return
	 * @throws Exception 
     *//*
    public int updateByOperationType(User user,EMoneyDetailType operationType,BigDecimal operationValue,String operationUserName,String orderId,Boolean isPay,String headOrderNum) throws Exception
    {
    	
    	MoneyDetail moneyDetail = new MoneyDetail();
		moneyDetail.setUserId(user.getId());  //用户id 
		moneyDetail.setUserName(user.getUserName()); //用户名
		moneyDetail.setBizSystem(user.getBizSystem());
		moneyDetail.setOperateUserName(operationUserName); //操作人员
		moneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(headOrderNum)); //订单单号
		moneyDetail.setWinLevel(null);
		moneyDetail.setWinCost(null);
		moneyDetail.setWinCostRule(null);
		moneyDetail.setBizSystem(user.getBizSystem());
		moneyDetail.setBalanceBefore(user.getMoney().subtract(operationValue)); //支出前金额
		moneyDetail.setBalanceAfter(user.getMoney());  //支出后的金额
		moneyDetail.setDetailType(operationType.name());
		moneyDetail.setIncome(isPay==false?operationValue:BigDecimal.ZERO); 
		moneyDetail.setPay(isPay==false?BigDecimal.ZERO:BigDecimal.ZERO.subtract(operationValue));
		if(orderId!=null&&!"".equals(orderId))
		{
			moneyDetail.setOrderId(Long.valueOf(orderId)); //订单id
		}
		
		if(operationValue.compareTo(BigDecimal.ZERO)<0){//变为正数
			operationValue=BigDecimal.ZERO.subtract(operationValue);
		}
		
		if(EMoneyDetailType.RECHARGE_PRESENT.getCode().equals(operationType.getCode())){
			
		  moneyDetail.setDescription("系统充值赠送"+(isPay==false?"":"扣除")+":" + operationValue);
		
		}else if(EMoneyDetailType.ACTIVITIES_MONEY.getCode().equals(operationType.getCode())){  //活动彩金,扣费
			
		  moneyDetail.setDescription("活动彩金"+(isPay==false?"":"扣除")+":" + operationValue);
			
		}else{
			throw new Exception("不可知的操作类型");
		} 
		return this.insertSelective(moneyDetail);
    	
    }*/
	
	/**
	 * 查询昨天结束半个小时未计入分红的投注资金明细
	 * @param nowDate
	 * @param userName 如果为空则查询所有用户
	 * @return
	 */
	/*public List<MoneyDetail> getYesterdayNotYetBonus(Date nowDate, String userName) {
		Date yesterDate = DateUtil.addDaysToDate(nowDate, -1);
		Date queryDateEnd = DateUtil.getNowStartTimeByEnd(yesterDate);
		Date queryDateStart = DateUtil.addMinutesToDate(queryDateEnd, -30);
		MoneyDetailQuery query = new MoneyDetailQuery();
		query.setLotteryType(EMoneyDetailType.LOTTERY.getCode());
		query.setYetBonus(MoneyDetail.NOT_YETBONUS);
		query.setCreatedDateStart(queryDateStart);
		query.setCreatedDateEnd(queryDateEnd);
		if(StringUtils.isNotBlank(userName)) {
			query.setUserName(userName);
		}
		List<MoneyDetail> moneyDetails = this.queryMoneyDetailsByCondition(query);
		return moneyDetails;
	}*/
    
    /**
     * 获取昨日中奖
     * @return
     */
    public BigDecimal getYestodayWin(User currentUser){

		Calendar calendar = Calendar.getInstance();
		//获取前一天的日期
		calendar.add(Calendar.DAY_OF_MONTH, -1);
		Date startTime = DateUtil.getNowStartTimeByStart(calendar.getTime());
		Date endTime = DateUtil.getNowStartTimeByEnd(calendar.getTime());
		
		MoneyDetailQuery moneyDetailQuery = new MoneyDetailQuery();
		moneyDetailQuery.setDetailType(EMoneyDetailType.WIN.getCode());
        moneyDetailQuery.setUserName(currentUser.getUserName());
        moneyDetailQuery.setCreatedDateStart(startTime);
        moneyDetailQuery.setCreatedDateEnd(endTime);
	    List<MoneyDetail> yestodayAgoMoneyDetails = moneyDetailMapper.queryMoneyDetailsByCondition(moneyDetailQuery);
	    
	    BigDecimal yestodayWinMoney = BigDecimal.ZERO;
	    if(CollectionUtils.isNotEmpty(yestodayAgoMoneyDetails)){
	    	for(MoneyDetail moneyDetail : yestodayAgoMoneyDetails){
	    		yestodayWinMoney = yestodayWinMoney.add(moneyDetail.getWinCost());
	    	}
	    }
	    
    	return yestodayWinMoney;
    }
    
    public BigDecimal getMoneyDetailByType(MoneyDetailQuery moneyDetailQuery)
    {
    	return moneyDetailMapper.getMoneyDetailByType(moneyDetailQuery);
    }
   	public static void main(String[] args) {
		//1422459085171  
   		//1422459316151
   		//1422459338196
   		System.out.println(DateUtils.getDateToStr(new Date()));
   		
	}
}
