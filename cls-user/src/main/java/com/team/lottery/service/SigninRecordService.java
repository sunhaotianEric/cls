package com.team.lottery.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.enums.EMoneyDetailType;
import com.team.lottery.enums.ENoteStatus;
import com.team.lottery.enums.ENoteType;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.SigninRecordQuery;
import com.team.lottery.extvo.SigninRecrodQuery;
import com.team.lottery.mapper.signinrecord.SigninRecordMapper;
import com.team.lottery.mapper.user.UserMapper;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.MakeOrderNum;
import com.team.lottery.vo.MoneyDetail;
import com.team.lottery.vo.Notes;
import com.team.lottery.vo.SigninRecord;
import com.team.lottery.vo.User;

@Service
public class SigninRecordService {
	private static Logger logger = LoggerFactory.getLogger(SigninRecordService.class);
	@Autowired
	private SigninRecordMapper signinRecordMapper;

	@Autowired
	private UserMapper userMapper;
	@Autowired
	private UserService userService;
	@Autowired
	private UserMoneyTotalService userMoneyTotalService;
	@Autowired
	private MoneyDetailService moneyDetailService;
	@Autowired
	private NotesService notesService;

	/**
	 * 分页查询签到记录
	 * 
	 * @param query
	 * @param page
	 * @return
	 */
	public Page querySigninRecord(SigninRecrodQuery query, Page page) {
		page.setTotalRecNum(signinRecordMapper.querySingninRecordCount(query));
		page.setPageContent(signinRecordMapper.querySingninRecord(query, page.getStartIndex(), page.getPageSize()));
		return page;
	}

	/**
	 * 新增签到记录
	 * 
	 * @param record
	 * @return
	 */
	public int savaSingninRecord(SigninRecord record) {
		record.setSigninTime(new Date());
		return signinRecordMapper.insertSelective(record);
	}

	/**
	 * 根据ID查询单个签到记录信息
	 * 
	 * @param id
	 * @return
	 */
	public SigninRecord querySigninRecordByid(int id) {
		return signinRecordMapper.selectByPrimaryKey(id);
	}

	/**
	 * 根据ID修改签到记录
	 * 
	 * @param record
	 * @return
	 */
	public int updateSigninRecord(SigninRecord record) {
		return signinRecordMapper.updateByPrimaryKeySelective(record);
	}

	/**
	 * 根据ID删除签到记录
	 * 
	 * @param id
	 * @return
	 */
	public int delSigninRecordById(int id) {
		return signinRecordMapper.deleteByPrimaryKey(id);
	}

	/**
	 * 根据用户查找签到记录
	 * 
	 * @param signinRecord
	 * @return
	 */
	public List<SigninRecord> getSigninRecordByUserSigninTime(SigninRecordQuery query) {
		return signinRecordMapper.getSigninRecordByUserSigninTime(query);
	}

	/**
	 * 根据用户查找签到记录
	 * 
	 * @param signinRecord
	 * @return
	 */
	public SigninRecord getSigninRecordByContinueSigninDay(SigninRecordQuery query) {
		return signinRecordMapper.getSigninRecordByContinueSigninDay(query);
	}

	/**
	 * 用户领取签到金返送发放金额相关操作
	 * 
	 * @param operateUser
	 * @throws Exception
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void giveSigninMoney(Integer userId, Integer continueDay, BigDecimal signinReceiveMoney) throws Exception, Exception {
		if (userId == null && signinReceiveMoney == null && continueDay <= 0) {
			return;
		}
		Date nowDate = new Date();

		logger.info("用户开始领取签到彩金, userId[" + userId + "],签到第" + continueDay + "天，领取金额[" + signinReceiveMoney + "]");
		User operationUser = userMapper.selectByPrimaryKey(userId);

		if (operationUser == null) {
			return;
		}

		// 签到记录
		SigninRecord signinRecord = new SigninRecord();
		signinRecord.setUserId(userId);
		signinRecord.setUserName(operationUser.getUserName());
		signinRecord.setBizSystem(operationUser.getBizSystem());
		signinRecord.setContinueSigninDays(continueDay);
		signinRecord.setMoney(signinReceiveMoney);
		signinRecord.setSigninTime(nowDate);
		signinRecord.setSigninDate(nowDate);
		signinRecordMapper.insertSelective(signinRecord);
		// 游客 领取就是无效订单
		Integer enabled = operationUser.getIsTourist() == 1 ? 0 : 1;

		MoneyDetail moneyDetail = new MoneyDetail();
		moneyDetail.setEnabled(enabled);
		moneyDetail.setUserId(operationUser.getId()); // 用户id
		moneyDetail.setOrderId(null); // 订单id
		moneyDetail.setUserName(operationUser.getUserName()); // 用户名
		moneyDetail.setBizSystem(operationUser.getBizSystem());
		moneyDetail.setOperateUserName(operationUser.getUserName()); // 操作人员
		// 订单单号
		String newOrderId = MakeOrderNum.makeOrderNum(MakeOrderNum.CJ);
		moneyDetail.setLotteryId(newOrderId);
		moneyDetail.setWinLevel(null);
		moneyDetail.setWinCost(null);
		moneyDetail.setWinCostRule(null);

		// 资金明细
		moneyDetail.setDetailType(EMoneyDetailType.ACTIVITIES_MONEY.name());
		moneyDetail.setIncome(signinRecord.getMoney());
		moneyDetail.setPay(new BigDecimal(0));
		moneyDetail.setBalanceBefore(operationUser.getMoney()); // 收入前金额
		// 活动彩金
		operationUser.addMoney(signinRecord.getMoney(), false);
		moneyDetail.setBalanceAfter(operationUser.getMoney()); // 收入后的金额
		moneyDetail.setDescription("签到活动(第" + continueDay + "天)彩金:" + signinRecord.getMoney());

		userService.updateByPrimaryKeyWithVersionSelective(operationUser);
		moneyDetailService.insertSelective(moneyDetail); // 保存资金明细

		// 发送系统消息
		Notes notes = new Notes();
		notes.setParentId(0l);
		notes.setEnabled(1);
		notes.setToUserId(signinRecord.getUserId());
		notes.setToUserName(signinRecord.getUserName());
		notes.setBizSystem(signinRecord.getBizSystem());
		notes.setFromUserName(ConstantUtil.SYSTEM_OPERATE_NAME);
		notes.setSub("[签到赠送]");
		notes.setBody("恭喜您,获得签到赠送,赠送金额：" + signinRecord.getMoney());
		notes.setType(ENoteType.SYSTEM.name());
		notes.setStatus(ENoteStatus.NO_READ.name());
		notes.setCreatedDate(new Date());
		notesService.insertSelective(notes);
		// 最后执行 增加用户总金额 线程问题
		userMoneyTotalService.addUserTotalMoneyByType(operationUser, EMoneyDetailType.ACTIVITIES_MONEY, signinRecord.getMoney());
	}

	/**
	 * 根据业务系统,用户名,用户ID查询昨天的签到记录.
	 * 
	 * @param signinRecordQuery 插叙对象.
	 * @return
	 */
	public SigninRecord getSigninRecordBySignDate(SigninRecordQuery signinRecordQuery) {
		return signinRecordMapper.getSigninRecordBySignDate(signinRecordQuery);

	}
}
