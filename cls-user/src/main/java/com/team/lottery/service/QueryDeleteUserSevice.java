package com.team.lottery.service;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.extvo.DeleteUserQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.user.UserMapper;
import com.team.lottery.mapper.userdelete.UserDelMapper;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserDel;

@Service
public class QueryDeleteUserSevice {
	private static Logger logger = LoggerFactory.getLogger(QueryDeleteUserSevice.class);
	@Autowired
	private UserDelMapper userDelMapper;
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private UserService userService;



	/**
	 * 查找满足条件的用户
	 * @param query
	 * @return
	 */
	public List<UserDel> getdelUser(DeleteUserQuery query){
		List<UserDel> getdelUserByParameter = userDelMapper.getdelUserByParameter(query);
		return getdelUserByParameter;

	}

	/**
	 * 分页查询会员列表
	 * @param query
	 * @param startNum
	 * @param pageNum
	 * @return
	 */
	public Page getAllUsersByQueryPage(DeleteUserQuery query,Page page){
		try {
			page.setTotalRecNum(userDelMapper.getAllUsersByQueryPageCount(query));
			List<UserDel> users = userDelMapper.selectDeleteUserAll(query,page.getStartIndex(),page.getPageSize());
			page.setPageContent(users);
		} catch (Exception e) {
			logger.error("查询异常", e);
		}
		return page;
	}
	/**
	 * 恢复被删会员
	 * @param id
	 * @return
	 */
	@Transactional(readOnly=false,rollbackFor=Exception.class)
	public void resumeMember(Integer id){
		User user=new User();
		UserDel delUser= userDelMapper.selectByPrimaryKey(id);
		BeanUtils.copyProperties(delUser, user);
		//TODO
		User updateAdminUser = userService.getUserByName("",ConstantUtil.FRONT_ADMIN_USER_NAME);
	
		//超级用户恢复删该会员时的余额
		/*updateAdminUser.setIcemoney(updateAdminUser.getIcemoney().subtract(delUser.getIcemoney()));
		updateAdminUser.setMoney(updateAdminUser.getMoney().subtract(delUser.getMoney()));
		updateAdminUser.setUermoney(updateAdminUser.getUermoney().subtract(delUser.getUermoney()));
		updateAdminUser.setTotalUerMoney(updateAdminUser.getTotalUerMoney().subtract(delUser.getTotalUerMoney()));
		updateAdminUser.setTotalNetRecharge(updateAdminUser.getTotalNetRecharge().subtract(delUser.getTotalNetRecharge()));
		updateAdminUser.setTotalQuickRecharge(updateAdminUser.getTotalQuickRecharge().subtract(delUser.getTotalQuickRecharge()));
		updateAdminUser.setTotalRecharge(updateAdminUser.getTotalRecharge().subtract(delUser.getTotalRecharge()));
		updateAdminUser.setTotalRechargePresent(updateAdminUser.getTotalRechargePresent().subtract(delUser.getTotalRechargePresent()));
		updateAdminUser.setTotalActivitiesMoney(updateAdminUser.getTotalActivitiesMoney().subtract(delUser.getTotalActivitiesMoney()));
		updateAdminUser.setTotalWithdraw(updateAdminUser.getTotalWithdraw().subtract(delUser.getTotalWithdraw()));
		updateAdminUser.setTotalFeevalue(updateAdminUser.getTotalFeevalue().subtract(delUser.getTotalFeevalue()));
		updateAdminUser.setTotalExtend(updateAdminUser.getTotalExtend().subtract(delUser.getTotalExtend()));
		updateAdminUser.setTotalRegister(updateAdminUser.getTotalRegister().subtract(delUser.getTotalRegister()));
		updateAdminUser.setTotalLottery(updateAdminUser.getTotalLottery().subtract(delUser.getTotalLottery()));
		updateAdminUser.setTotalShopPoint(updateAdminUser.getTotalShopPoint().subtract(delUser.getTotalShopPoint()));
		updateAdminUser.setTotalWin(updateAdminUser.getTotalWin().subtract(delUser.getTotalWin()));
		updateAdminUser.setTotalRebate(updateAdminUser.getTotalRebate().subtract(delUser.getTotalRebate()));
		updateAdminUser.setTotalPercentage(updateAdminUser.getTotalPercentage().subtract(delUser.getTotalPercentage()));
		updateAdminUser.setTotalPayTranfer(updateAdminUser.getTotalPayTranfer().subtract(delUser.getTotalPayTranfer()));
		updateAdminUser.setTotalIncomeTranfer(updateAdminUser.getTotalIncomeTranfer().subtract(delUser.getTotalIncomeTranfer()));
		updateAdminUser.setTotalHalfMonthBonus(updateAdminUser.getTotalHalfMonthBonus().subtract(delUser.getTotalHalfMonthBonus()));
		updateAdminUser.setTotalDayBonus(updateAdminUser.getTotalDayBonus().subtract(delUser.getTotalDayBonus()));
		updateAdminUser.setTotalMonthSalary(updateAdminUser.getTotalMonthSalary().subtract(delUser.getTotalMonthSalary()));
		updateAdminUser.setTotalDaySalary(updateAdminUser.getTotalDaySalary().subtract(delUser.getTotalDaySalary()));
		updateAdminUser.setTotalDayCommission(updateAdminUser.getTotalDayCommission().subtract(delUser.getTotalDayCommission()));
		updateAdminUser.setRemainExtend(updateAdminUser.getRemainExtend().subtract(delUser.getRemainExtend()));
*/
		 //恢复该会员
		userMapper.insertSelective(user);
		//更新超级用户余额
		userMapper.updateByPrimaryKeySelective(updateAdminUser);
		//根据恢复会员的Id移除被删会员表的数据
		userDelMapper.deleteByPrimaryKey(id);

	}

}
