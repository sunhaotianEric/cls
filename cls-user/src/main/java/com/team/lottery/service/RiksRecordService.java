package com.team.lottery.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.riskrecord.RiskRecordMapper;
import com.team.lottery.vo.PayNotifyLog;
import com.team.lottery.vo.RiskRecord;

/**
 * 用户风控记录
 * 
 * @author
 *
 */
@Service("riksRecordService")
public class RiksRecordService {

	@Autowired
	private RiskRecordMapper riskRecordMapper;

	public int deleteByPrimaryKey(Long id) {
		return riskRecordMapper.deleteByPrimaryKey(id);
	}

	public int insert(RiskRecord record) {

		return riskRecordMapper.insert(record);
	}

	public int insertSelective(RiskRecord record) {

		return riskRecordMapper.insertSelective(record);
	}

	public RiskRecord selectByPrimaryKey(Long id) {
		return riskRecordMapper.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(RiskRecord record) {

		return riskRecordMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(RiskRecord record) {

		return riskRecordMapper.updateByPrimaryKey(record);
	}
	
	/**
	 * 查找所有的
	 * 
	 * @return
	 */
	public Page getAllRiskRecordByQueryPage(RiskRecord query, Page page) {
		page.setTotalRecNum(riskRecordMapper.getAllRiskRecordByQueryPageCount(query));
		page.setPageContent(riskRecordMapper.getAllRiskRecordByQueryPage(query, page.getStartIndex(), page.getPageSize()));
		return page;
	}
}
