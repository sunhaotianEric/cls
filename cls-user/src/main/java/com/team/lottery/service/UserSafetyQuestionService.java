package com.team.lottery.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.mapper.usersafetyquestion.UserSafetyQuestionMapper;
import com.team.lottery.vo.UserSafetyQuestion;

/**
 * 用户安全问题服务类
 * 
 * @author
 *
 */
@Service("userSafetyQuestionService")
public class UserSafetyQuestionService {

	@Autowired
	private UserSafetyQuestionMapper userSafetyQuestionMapper;

	public int deleteByPrimaryKey(Long id) {
		return userSafetyQuestionMapper.deleteByPrimaryKey(id);
	}

	public int insert(UserSafetyQuestion record) {
		record.setCreateDate(new Date());
		return userSafetyQuestionMapper.insert(record);
	}

	public int insertSelective(UserSafetyQuestion record) {
		record.setCreateDate(new Date());
		return userSafetyQuestionMapper.insertSelective(record);
	}

	public UserSafetyQuestion selectByPrimaryKey(Long id) {
		return userSafetyQuestionMapper.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(UserSafetyQuestion record) {
		record.setUpdateDate(new Date());
		return userSafetyQuestionMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(UserSafetyQuestion record) {
		record.setUpdateDate(new Date());
		return userSafetyQuestionMapper.updateByPrimaryKey(record);
	}

	/*
	 * 获取用户安全问题记录数
	 */
	public int getQuestionCountByUserId(Integer id) {
		return userSafetyQuestionMapper.getQuestionCountByUserId(id);
	}

	/*
	 * 获取用户安全问题记录
	 */
	public List<UserSafetyQuestion> getQuestionsUserId(Integer id) {
		return userSafetyQuestionMapper.getQuestionsByUserId(id);
	}

	public int getQuestionCountByUsername(UserSafetyQuestion query) {
		return userSafetyQuestionMapper.getQuestionCountByUsername(query);
	}

	public List<UserSafetyQuestion> getQuestionsByUsername(UserSafetyQuestion query) {
		return userSafetyQuestionMapper.getQuestionsByUsername(query);
	}

	/**
	 * 根据用户id删除对应用户的安全问题.并保存新的安全问题.
	 * 
	 * @param userSafetyQuestionList
	 */
	@Transactional(rollbackFor = Exception.class)
	public void insertNewSafeQuestionsDeleteOldQuestions(List<UserSafetyQuestion> userSafetyQuestionList, Integer userId) {
		// 删除对应用户id的安全问题.
		userSafetyQuestionMapper.deleteSafeQuestionByUserId(userId);
		// 新增对应用户的安全问题.
		for (UserSafetyQuestion userSafetyQuestion : userSafetyQuestionList) {
			userSafetyQuestionMapper.insertSelective(userSafetyQuestion);
		}

	}

}
