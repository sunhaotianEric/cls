package com.team.lottery.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.enums.EMoneyDetailType;
import com.team.lottery.enums.ENoteStatus;
import com.team.lottery.enums.ENoteType;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.PromotionRecordQuery;
import com.team.lottery.extvo.UserTotalMoneyByTypeVo;
import com.team.lottery.mapper.promotionrecord.PromotionRecordMapper;
import com.team.lottery.system.BizSystemConfigVO;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.MakeOrderNum;
import com.team.lottery.vo.LevelSystem;
import com.team.lottery.vo.MoneyDetail;
import com.team.lottery.vo.Notes;
import com.team.lottery.vo.PromotionRecord;
import com.team.lottery.vo.User;

@Service("promotionRecordService")
public class PromotionRecordService {
	private static Logger logger = LoggerFactory.getLogger(PromotionRecordService.class);

	@Autowired
	private PromotionRecordMapper promotionRecordMapper;
	// @Autowired
	// private LevelSytemService levelSytemService;
	@Autowired
	private UserService userService;
	@Autowired
	private UserMoneyTotalService userMoneyTotalService;
	@Autowired
	private MoneyDetailService moneyDetailService;
	@Autowired
	private NotesService notesService;

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int deleteByPrimaryKey(Integer id) {
		return promotionRecordMapper.deleteByPrimaryKey(id);
	}

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int insertSelective(PromotionRecord record) {
		return promotionRecordMapper.insertSelective(record);
	}

	public PromotionRecord selectByPrimaryKey(Integer id) {
		return promotionRecordMapper.selectByPrimaryKey(id);
	}

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int updateByPrimaryKeySelective(PromotionRecord record) {
		return promotionRecordMapper.updateByPrimaryKeySelective(record);
	}

	/**
	 * 查找所有的登陆后台日志数据
	 * 
	 * @return
	 */
	public Page getAllPromotionRecordByQueryPage(PromotionRecordQuery query, Page page) {
		page.setTotalRecNum(promotionRecordMapper.getAllPromotionRecordPageCount(query));
		page.setPageContent(
				promotionRecordMapper.getAllPromotionRecordPage(query, page.getStartIndex(), page.getPageSize()));
		return page;
	}

	/**
	 * 晋级记录
	 * 
	 * @param point
	 * @param levelSystem
	 * @param levelSystemList
	 * @param orderUser
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void openPromotionRecord(List<UserTotalMoneyByTypeVo> userTotalMoneyByTypeVos,
			List<LevelSystem> levelSystemList, BizSystemConfigVO systemConfigVO, User orderUser) {
		// 当积分达到可以晋级时添加晋级奖励记录
		if (levelSystemList.size() != 0) {
			for (LevelSystem levelSystem : levelSystemList) {
				// 充值晋级记录
				PromotionRecord promotionRecord = new PromotionRecord();
				promotionRecord.setBizSystem(orderUser.getBizSystem());
				promotionRecord.setCreateDate(new Date());
				Integer levels = levelSystem.getLevel();
				if (orderUser.getVipLevel().equals(levels.toString())) {
					continue;
				}
				promotionRecord.setLevel(orderUser.getVipLevel());
				promotionRecord.setLevelAfter(levelSystem.getLevel().toString());
				orderUser.setVipLevel(String.valueOf(levelSystem.getLevel()));
				orderUser.setLevelName(levelSystem.getLevelName());
				// 晋级奖励
				promotionRecord.setMoney(levelSystem.getPromotionAward());
				// 晋级奖励自动领取是否开启
				if (systemConfigVO.getAutomaticPromotionReceiveSwitch() == 0) {
					// 未开启,晋级奖励待领取
					promotionRecord.setStatus(0);
				} else {
					// 开启,自动领取晋级奖励
					promotionRecord.setStatus(2);
				}
				promotionRecord.setUserId(orderUser.getId());
				promotionRecord.setUserName(orderUser.getUserName());
				logger.info("业务系统[{}]，用户名[{}]晋级成功,插入晋级奖励记录", orderUser.getBizSystem(), orderUser.getUserName());
				this.insertSelective(promotionRecord);

				BigDecimal addPromotionAward = levelSystem.getPromotionAward();
				// 只有获得晋级奖励金额才相关信息写入资金明细！并向用户发站内信,晋级奖励自动领取是否开启
				if (addPromotionAward != null && addPromotionAward.compareTo(BigDecimal.ZERO) > 0
						&& systemConfigVO.getAutomaticPromotionReceiveSwitch() == 1) {
					Integer level = Integer.valueOf(orderUser.getVipLevel());
					BigDecimal userMoney = orderUser.getMoney();

					// 开启,自动领取晋级奖励
					orderUser.addMoney(addPromotionAward, false);
					
					promotionRecord.setUpdateDate(new Date());
					this.updateByPrimaryKeySelective(promotionRecord);
					
					MoneyDetail zsmoneyDetail = new MoneyDetail();
					zsmoneyDetail.setUserId(orderUser.getId()); // 用户id
					zsmoneyDetail.setOrderId(null); // 订单id
					zsmoneyDetail.setOperateUserName(orderUser.getUserName()); // 操作人员
					zsmoneyDetail.setUserName(orderUser.getUserName()); // 用户名
					zsmoneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.ZS)); // 订单单号
					zsmoneyDetail.setExpect(""); // 哪个期号
					zsmoneyDetail.setLotteryType(""); // 彩种
					zsmoneyDetail.setLotteryTypeDes(""); // 彩种名称
					zsmoneyDetail.setDetailType(EMoneyDetailType.ACTIVITIES_MONEY.name()); // 资金类型为投注
					zsmoneyDetail.setIncome(addPromotionAward); // 收入为0
					zsmoneyDetail.setPay(BigDecimal.ZERO); // 支出金额
					zsmoneyDetail.setBalanceBefore(userMoney); // 支出前金额
					zsmoneyDetail.setBalanceAfter(orderUser.getMoney()); // 支出后的金额
					zsmoneyDetail.setWinLevel(null);
					zsmoneyDetail.setWinCost(null);
					zsmoneyDetail.setWinCostRule(null);
					zsmoneyDetail.setDescription("晋升为VIP"+ orderUser.getVipLevel() +"获得晋级礼金:" + addPromotionAward);
					zsmoneyDetail.setBizSystem(orderUser.getBizSystem());
					moneyDetailService.insertSelective(zsmoneyDetail);
					// 针对盈亏报表多线程不会事物回滚处理方式
					UserTotalMoneyByTypeVo userTotalMoneyByTypeVo = new UserTotalMoneyByTypeVo(orderUser,
							EMoneyDetailType.ACTIVITIES_MONEY, addPromotionAward);
					userTotalMoneyByTypeVos.add(userTotalMoneyByTypeVo);
					// userMoneyTotalService.addUserTotalMoneyByType(orderUser,EMoneyDetailType.ACTIVITIES_MONEY,
					// addPromotionAward);
					logger.info("用户名[" + orderUser.getUserName() + "]" + ",获得晋级金额:" + addPromotionAward + ",等级从" + level
							+ "升级到" + levelSystem.getLevel() + "级");

				}
				// 成功获得活动彩金系统消息
				Notes notesActivities = new Notes();
				notesActivities.setParentId(0l);
				notesActivities.setEnabled(1);
				notesActivities.setFromUserName(ConstantUtil.SYSTEM_OPERATE_NAME);
				notesActivities.setToUserName(orderUser.getUserName());
				notesActivities.setSub("头衔晋升通知");
				if (systemConfigVO.getAutomaticPromotionReceiveSwitch() == 1) {
					notesActivities.setBody("您的等级成功升到" + levelSystem.getLevel() + "级,头衔晋升为" + levelSystem.getLevelName()
							+ ",您成功获得晋级金额" + addPromotionAward + "。");
				} else {
					notesActivities.setBody("您的等级成功升到" + levelSystem.getLevel() + "级,头衔晋升为" + levelSystem.getLevelName()
							+ ",您成功获得晋级金额" + addPromotionAward + "。");
				}
				notesActivities.setType(ENoteType.SYSTEM.name());
				notesActivities.setStatus(ENoteStatus.NO_READ.name());
				notesActivities.setCreatedDate(new Date());
				notesActivities.setToUserId(orderUser.getId());
				notesActivities.setBizSystem(orderUser.getBizSystem());
				notesService.insertSelective(notesActivities);
			}
		}

	}

	/**
	 * 根据条件查询所有的晋级奖励
	 * 
	 * @param username
	 * @param bizSystem
	 * @return
	 */
	public List<PromotionRecord> getAllPromotionRecord(PromotionRecordQuery promotionRecordQuery) {
		return promotionRecordMapper.getAllPromotionRecord(promotionRecordQuery);
	}

	/**
	 * 晋级奖金金额资金明细
	 * 
	 * @param promotionRecord
	 * @param user
	 * @param redmoney
	 * @throws Exception
	 * @throws Exception
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void givePromotionRecordMoney(List<PromotionRecord> promotionRecords) throws Exception, Exception {
		for (PromotionRecord promotionRecord : promotionRecords) {
			Integer userId = promotionRecord.getUserId();
			User operationUser = userService.selectByPrimaryKey(userId);

			if (operationUser == null) {
				return;
			}
			BigDecimal money = operationUser.getMoney();
			// 修改晋级领取的领取状态
			promotionRecord.setStatus(1);
			promotionRecord.setUpdateDate(new Date());
			this.updateByPrimaryKeySelective(promotionRecord);
			// 将用户本身所拥有的金额加上用户领取的晋级金额
			operationUser.addMoney(promotionRecord.getMoney(), false);
			// 游客 领取就是无效订单
			Integer enabled = operationUser.getIsTourist() == 1 ? 0 : 1;

			MoneyDetail moneyDetail = new MoneyDetail();
			moneyDetail.setEnabled(enabled);
			moneyDetail.setUserId(operationUser.getId()); // 用户id
			moneyDetail.setOrderId(null); // 订单id
			moneyDetail.setUserName(operationUser.getUserName()); // 用户名
			moneyDetail.setBizSystem(operationUser.getBizSystem());
			moneyDetail.setOperateUserName(operationUser.getUserName()); // 操作人员
			// 订单单号
			String newOrderId = MakeOrderNum.makeOrderNum(MakeOrderNum.CJ);
			moneyDetail.setLotteryId(newOrderId);
			moneyDetail.setWinLevel(null);
			moneyDetail.setWinCost(null);
			moneyDetail.setWinCostRule(null);

			// 资金明细
			moneyDetail.setDetailType(EMoneyDetailType.ACTIVITIES_MONEY.name());
			moneyDetail.setIncome(promotionRecord.getMoney());
			moneyDetail.setPay(new BigDecimal("0"));
			moneyDetail.setBalanceBefore(money); // 收入前金额
			// 活动彩金
			moneyDetail.setBalanceAfter(operationUser.getMoney()); // 收入后的金额
			moneyDetail.setDescription(promotionRecord.getUserName() + "领取了晋级奖金金额" + promotionRecord.getMoney()
					+ "晋级到vip" + promotionRecord.getLevelAfter());
			// int i = 1/0;
			userService.updateByPrimaryKeyWithVersionSelective(operationUser);
			moneyDetailService.insertSelective(moneyDetail); // 保存资金明细
			// 最后执行 增加用户总金额 线程问题
			// int i = 1/0;
			userMoneyTotalService.addUserTotalMoneyByType(operationUser, EMoneyDetailType.ACTIVITIES_MONEY,
					promotionRecord.getMoney());
		}

	}

	/**
	 * 批量查询
	 * 
	 * @param promotionRecordId
	 * @return
	 */
	public List<PromotionRecord> getByAllPromotionRecordKey(List<Integer> promotionRecordId, String userName) {
		return promotionRecordMapper.getByAllPromotionRecordKey(promotionRecordId, userName);

	}

}
