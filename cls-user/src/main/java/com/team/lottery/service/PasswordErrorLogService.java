package com.team.lottery.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.PasswordErrorLogQuery;
import com.team.lottery.mapper.passworderrorlog.PasswordErrorLogMapper;
import com.team.lottery.vo.PasswordErrorLog;

/**
 * 密码输入错误时相关错误日志.
 * 
 * @author jamine
 *
 */
@Service("passwordErrLogService")
public class PasswordErrorLogService {
	//private static Logger logger = LoggerFactory.getLogger(PasswordErrLogService.class);
	
	@Autowired
	private PasswordErrorLogMapper passwordErrorLogMapper;
	

	public int deleteByPrimaryKey(Long id){
		return passwordErrorLogMapper.deleteByPrimaryKey(id);
	}

	public int insert(PasswordErrorLog record){
		return passwordErrorLogMapper.insert(record);
	}

	public int insertSelective(PasswordErrorLog record){
		return passwordErrorLogMapper.insertSelective(record);
	}

	public PasswordErrorLog selectByPrimaryKey(Long id){
		return passwordErrorLogMapper.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(PasswordErrorLog record){
		return passwordErrorLogMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(PasswordErrorLog record){
		return passwordErrorLogMapper.updateByPrimaryKey(record);
	}

	/**
	 * 查询密码错误日志记录数.
	 * @param query
	 * @return
	 */
	public int getAllPasswordErrorLogByQueryPageCount(PasswordErrorLogQuery query) {
		return passwordErrorLogMapper.getAllPasswordErrorLogByQueryPageCount(query);
	}

	/**
	 * 解锁用户时，删除安全密码日志错误记录数
	 * 
	 * @return
	 */
	public void updatePasswordErrorLogByUserIdForSafePwd(Integer userId) {
		passwordErrorLogMapper.updatePasswordErrorLogByUserIdForSafePwd(userId);
		
	}

	/**
	 * 根据查询对象,查询出对应的错误日志记录.
	 * @param withDrawquery
	 * @return
	 */
	public List<PasswordErrorLog> getAllPasswordErrorLogByQuery(PasswordErrorLogQuery query) {
		return passwordErrorLogMapper.getAllPasswordErrorLogByQuery(query);
	}
	
	/**
	 * 解锁用户时，删除登陆密码日志错误记录数
	 * 
	 * @return
	 */
	public void updatePasswordErrorLogByUserIdForPwd(Integer userId) {
		passwordErrorLogMapper.updatePasswordErrorLogByUserIdForPwd(userId);
	}

	/**
	 * 删除30天之前的数据(硬删除).
	 * @param query
	 */
	public void deleteByThirtyDaysBeforeData(PasswordErrorLogQuery query) {
		passwordErrorLogMapper.deleteByThirtyDaysBeforeData(query);
	}

	/**
	 * 软删除登陆密码错误的数据.
	 * @param bizSystem
	 * @param userName
	 * @param requestIp
	 */
	public void updatePasswordErrorLogByUserName(String bizSystem, String userName) {
		passwordErrorLogMapper.updatePasswordErrorLogByUserName(bizSystem,userName);
		
	}
	
	/**
	 * 根据用户ID删除密码错误记录.(后台重置安全密码时)
	 * @param userId
	 */
	public void delPasswordErrorLogByUserIdForCheckSavePassWord(Integer userId) {
		passwordErrorLogMapper.delPasswordErrorLogByUserIdForCheckSavePassWord(userId);
	}
	
}
