package com.team.lottery.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.enums.EUserDailLiLevel;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.UserRegisterVo;
import com.team.lottery.mapper.userregister.UserRegisterMapper;
import com.team.lottery.system.message.MessageQueueCenter;
import com.team.lottery.system.message.messagetype.LoginMessage;
import com.team.lottery.util.AwardModelUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LoginLog;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserRegister;

@Service("userRegisterService")
public class UserRegisterService {
	
	private static Logger logger = LoggerFactory.getLogger(UserRegisterService.class);
	
	@Autowired
	private UserService userService;
	@Autowired
	private LoginLogService loginLogService;
	
	@Autowired
	private UserRegisterMapper userRegisterMapper;
	
    public int deleteByPrimaryKey(Integer id){
    	return userRegisterMapper.deleteByPrimaryKey(id);
    }

    public int insert(UserRegister record){
    	return userRegisterMapper.insert(record);
    }

    public int insertSelective(UserRegister record){
    	record.setCreatedDate(new Date());
    	return userRegisterMapper.insertSelective(record);
    }

    public UserRegister selectByPrimaryKey(Integer id){
    	return userRegisterMapper.selectByPrimaryKey(id);
    }

    public int updateByPrimaryKeySelective(UserRegister record){
    	return userRegisterMapper.updateByPrimaryKeySelective(record);
    }

    public int updateByPrimaryKey(UserRegister record){
    	return userRegisterMapper.updateByPrimaryKey(record);
    }
   
    
    /**
     * 用户注册链接验证
     * @param userRegisterVo
     * @return
     */
    public Object[] userRegisterLinkCheck(UserRegisterVo userRegisterVo,UserRegister userRegisterLink){
    	Object [] result = new Object[3];

		if(userRegisterLink == null){
			userRegisterLink = this.getUserRegisterLinkByInvitationCode(userRegisterVo.getInvitationCode(), userRegisterVo.getBizSystem());
		}
		if(userRegisterLink==null){
			result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
			result[1] = "链接已经失效.";
    		return result; 
		}
		
    	if(userRegisterLink.getSetCount()!=-1&&userRegisterLink.getSetCount()<=userRegisterLink.getUseCount()){
    		result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
			result[1] = "本链接注册名额已经满了，请使用其他链接.";
    		return result; 
    	}
//    	if(userRegisterLink.getLinkSets() == null){
//			result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
//			result[1] = "链接已经不存在.";
//    		return result; 
//    	}
    	if(userRegisterLink.getLoseDate() != null){
    		if(userRegisterLink.getLoseDate().getTime() < new Date().getTime()){
    			result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
    			result[1] = "该注册链接已经失效了,请联系上级获取新的注册链接.";
        		return result; 
    		}
    	}
    	if(userRegisterLink.getEnabled() != 1){
  			result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
			result[1] = "该注册链接已经停用了,请联系您的上级.";
    		return result; 
    	}
		User dailiUser = userService.selectByPrimaryKey(userRegisterLink.getUserId());
    	if(dailiUser == null){
			result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
			result[1] = "代理人用户不存在.";
    		return result; 
    	}
    	
//    	String [] registerParams = userRegisterLink.getLinkSets().split(ConstantUtil.REGISTER_SPLIT);
//    	if(registerParams.length != 12){
//			result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
//			result[1] = "注册链接长度不正确.";
//    		return result; 
//    	}
    	
		result[0] = ConstantUtil.DWR_AJAX_RESULT_OK;

    	return result;
    }
    
    /**
     * 用户注册
     * @param userRegister
     * @return
     * @throws Exception 
     */
	@Transactional(readOnly=false,rollbackFor=Exception.class)
    public Object[] userRegister(UserRegisterVo userRegisterVo,LoginLog loginLog) throws UnEqualVersionException,Exception{
    	logger.info("用户链接注册, 业务系统[{}],注册用户名[{}]",userRegisterVo.getBizSystem(), userRegisterVo.getUserName());
		Object [] result = new Object[3];
		//验证邀请码
	   	int count =userRegisterMapper.getInvitationCodeCount(userRegisterVo.getInvitationCode(), userRegisterVo.getBizSystem());
	   	if(count<=0){
	   		result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
			result[1] = "邀请码失效！请重新获取！";
			result[2] = "invitationCodeError";
    		return result; 
	   	}
		
		UserRegister userRegisterLink = this.getUserRegisterLinkByInvitationCode(userRegisterVo.getInvitationCode(), userRegisterVo.getBizSystem());
		userRegisterVo.setBizSystem(userRegisterLink.getBizSystem());
		//校验注册链接是否正确
		result = userRegisterLinkCheck(userRegisterVo, userRegisterLink);
	   	if(result[0] == ConstantUtil.DWR_AJAX_RESULT_ERROR){
    		return result;
    	}

	   
	   	//用户名是否重复的校验
	   	User tempuser = userService.getUserByName(userRegisterVo.getBizSystem(), userRegisterVo.getUserName());
	   	if(tempuser != null){
			result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
			result[1] = "该用户名已经被注册了.";
			result[2] = "userNameError";
    		return result; 
    	}
    	
    	User dailiUser = userService.selectByPrimaryKey(userRegisterLink.getUserId());
    	String [] registerParams = userRegisterLink.getLinkSets().split(ConstantUtil.REGISTER_SPLIT);
    	
    	//新创建的用户 
    	User newUser = new User(); 
    	newUser.setBizSystem(userRegisterVo.getBizSystem());
    	
    	//GENERALAGENCY&1958&1958.00&1939.00&1958.00&1958.00&1958.00&1958.00&1958.00&1958.00&-&-&
        //注册代理级别&时时彩模式&分分彩模式&十一选五模式&快三模式&PK10模式&低频彩模式&骰宝模式&六合彩模式&快乐十分模式&分红比例&注册赠送资金
    	
    	//设置代理类型
    	newUser.setDailiLevel(registerParams[0]);
    	
    	//设定用户的模式
    	BigDecimal sscRebate = new BigDecimal(registerParams[1]);
    	BigDecimal ffcRebate = new BigDecimal(registerParams[2]);
    	BigDecimal syxwRebate = new BigDecimal(registerParams[3]);
    	BigDecimal ksRebate = new BigDecimal(registerParams[4]);
    	BigDecimal pk10Rebate = new BigDecimal(registerParams[5]);
    	BigDecimal dpcRebate = new BigDecimal(registerParams[6]);
    	BigDecimal tbRebate = new BigDecimal(registerParams[7]);
    	BigDecimal lhcRebate = new BigDecimal(registerParams[8]);
    	BigDecimal klsfRebate = new BigDecimal(registerParams[9]);
    	newUser.setSscRebate(sscRebate);
    	newUser.setFfcRebate(ffcRebate);
    	newUser.setSyxwRebate(syxwRebate);
    	newUser.setKsRebate(ksRebate);
    	newUser.setPk10Rebate(pk10Rebate);
    	newUser.setDpcRebate(dpcRebate);
    	newUser.setTbRebate(tbRebate);
    	newUser.setLhcRebate(lhcRebate);
    	newUser.setKlsfRebate(klsfRebate);
    	//用户模式校验
    	result = AwardModelUtil.userAwardModelCheck(newUser, dailiUser);
    	if(result[0] == ConstantUtil.DWR_AJAX_RESULT_ERROR){
    		return result;
    	}
    	//分红比例
    	if(!StringUtils.isEmpty(registerParams[10]) && !registerParams[10].equals(ConstantUtil.REGISTER_NULL_REPLACE)){
    		newUser.setBonusScale(new BigDecimal(registerParams[10]));
    	}
    	newUser.setRegfrom(dailiUser.getRegfrom() + dailiUser.getUserName() + ConstantUtil.REG_FORM_SPLIT);
    	// 设置用户初始头像
    	newUser.setHeadImg(userRegisterVo.getHeadImg());
    	newUser.setUserName(userRegisterVo.getUserName());
		if (StringUtils.isEmpty(userRegisterVo.getUserPassword())||userRegisterVo.getUserPassword()==null) {
			newUser.setPassword(ConstantUtil.INITIALIZATION_PASSWORD);
		} else {
			newUser.setPassword(userRegisterVo.getUserPassword());
		}

		newUser.setPhone(userRegisterVo.getMobile());
    	newUser.setQq(userRegisterVo.getQq());
    	String loginIp = loginLog.getLoginIp();
    	
    	newUser.setLogip(loginIp);
    	newUser.setLastLoginTime(new Date());
    	newUser.setLoginTimes(1);
    	newUser.setTrueName(userRegisterVo.getTrueName());
    	if(userRegisterVo.getIsTourist()!=null){
    		newUser.setIsTourist(userRegisterVo.getIsTourist());
    		//游客才给他钱
    		if(userRegisterVo.getIsTourist() == 1){
    			if(userRegisterVo.getMoney()!=null){
    	    		newUser.setMoney(userRegisterVo.getMoney());
    	    	}
    		}
    	}
    	
    
    	userService.saveUser(newUser);
    	
    	userRegisterLink.setUseCount(userRegisterLink.getUseCount()+1);
    	this.updateByPrimaryKeySelective(userRegisterLink);
    	//更新团队人数
		User dealUser = null;
    	String [] regFroms = newUser.getRegfrom().split(ConstantUtil.REG_FORM_SPLIT);
		for(int i = 0; i < regFroms.length; i++){
			User regFromUser = userService.getUserByName(userRegisterVo.getBizSystem(),regFroms[i]);
			if(regFromUser != null){
				dealUser = new User();
				dealUser.setId(regFromUser.getId());
				dealUser.setTeamMemberCount(regFromUser.getTeamMemberCount());
				dealUser.setDownMemberCount(regFromUser.getDownMemberCount());
				if(i == (regFroms.length - 1)){
					dealUser.setTeamMemberCount(dealUser.getTeamMemberCount() + 1);
					dealUser.setDownMemberCount(dealUser.getDownMemberCount() + 1);
				}else{
					dealUser.setTeamMemberCount(dealUser.getTeamMemberCount() + 1);
				}
				userService.updateByPrimaryKeySelective(dealUser);
			}
		}
    	//注册时需要插入一条登录日志，已方便查询用户注册ip地址
		/*HttpServletRequest request = BaseDwrUtil.getRequest();
		
        LoginLog loginLog = new LoginLog();
        loginLog.setEnabled(dailiUser.getIsTourist()==1?0:1);
        loginLog.setUserId(newUser.getId());
        loginLog.setUserName(newUser.getUserName());
        loginLog.setBizSystem(newUser.getBizSystem());
        loginLog.setLoginIp(loginIp);*/
		loginLog.setEnabled(dailiUser.getIsTourist()==1?0:1);
        loginLog.setUserId(newUser.getId());
        loginLog.setUserName(newUser.getUserName());
		/*StringBuffer url = new StringBuffer(userRegisterVo.getUrl());
		String tempContextUrl = url.delete(url.length() - userRegisterVo.getUrI().length(), url.length()).append("/").toString();
		
		loginLog.setBrowser(tempContextUrl + "-----" + userRegisterVo.getHeaders());	
        
        //根据域名获取登录类型
	    String loginType = ClsCacheManager.getLoginType(userRegisterVo.getServerName());
        loginLog.setLogType(loginType);*/
    	loginLogService.insertSelective(loginLog); 
    	
    	//放置登录消息队列
		LoginMessage loginMessage = new LoginMessage();
		loginMessage.setIp(loginIp);
		loginMessage.setLoginId(loginLog.getId());
		loginMessage.setUserName(newUser.getUserName());
		loginMessage.setBizSystem(newUser.getBizSystem());
		loginMessage.setLoginType(loginLog.getLogType());
		loginMessage.setSessionId(userRegisterVo.getSessionId());	
		
		MessageQueueCenter.putMessage(loginMessage);
    	
		result[0] = ConstantUtil.DWR_AJAX_RESULT_OK;
    	return result;
    }
    
    /**
     * 获取用户的开户链接地址
     * @param userId
     * @return
     */
    public List<UserRegister> getUserRegisterLinks(Integer userId){
    	return userRegisterMapper.getUserRegisterLinks(userId);
    }
    
    /**
     * 获取用户的开户链接地址 分页
     * @param userId
     * @return
     */
    public Page getUserRegisterLinksPage(UserRegister query,Page page){
    	page.setTotalRecNum(userRegisterMapper.getUserRegisterLinksPageCount(query));
    	page.setPageContent(userRegisterMapper.getUserRegisterLinksPage(query,page.getStartIndex(),page.getPageSize()));
		return page;
    }
    
    
    
    /**
     * 根据UUID,查询用户的注册链接地址
     * @param uuid
     * @return
     */
    public UserRegister getUserRegisterLinkByUUID(String uuid){
    	return userRegisterMapper.getUserRegisterLinkByUUID(uuid);
    }
    
    /**
     * 根据邀请码,查询用户的注册链接地址
     * @param uuid
     * @return
     */
    public UserRegister getUserRegisterLinkByInvitationCode(String invitationCode,String bizSystem){
    	return userRegisterMapper.getUserRegisterLinkByInvitationCode(invitationCode, bizSystem);
    }
    /**
     * 加载所有的注册链接地址
     * @return
     */
    public List<UserRegister> getAllUserRegisterLinks(){
    	return userRegisterMapper.getAllUserRegisterLinks();
    }
    
    /**
     * 加载所有的注册链接地址
     * @return
     */
    public String  makeInvitationCode(String bizSystem){
    	String invitationCode=StringUtils.getRandomInt(8);
    	int count =userRegisterMapper.getInvitationCodeCount(invitationCode, bizSystem);
    	 
    	if(count>0){
    		invitationCode=this.makeInvitationCode(bizSystem);
    	}else{
    		return invitationCode;
    	}
    	return invitationCode;
    }
    
    /**
     * 推广数据分页查询
     * @param query
     * @param page
     * @return
     */
    public Page queryUserRegister(UserRegister query,Page page){
    	page.setTotalRecNum(userRegisterMapper.queryUserRegisterCount(query));
    	// 分页查询所有的推广数据.并且强制转换为UserRegisterList
		ArrayList<UserRegister> alist=(ArrayList<UserRegister>) userRegisterMapper.queryUserRegister(query,page.getStartIndex(),page.getPageSize());
    	// :TODO 注释代码.下一个版本删除代码!
		/*for(int i=0;i<alist.size();i++){
    		String[] sp=alist.get(i).getLinkSets().split("&");
    		UserRegister stre=alist.get(i);
    		String linkSets = "";
    		try {
				linkSets = EUserDailLiLevel.valueOf(sp[0]).getDescription();
			} catch (Exception e) {
				logger.info("id为["+stre.getId()+"] 用户id为["+stre.getUserId()+"] 的代理类型["+sp[0]+"] 报错,"+e);
			}
    		stre.setLinkSets(linkSets);
    		stre.setSscrebate(new BigDecimal(sp[1]));
    		stre.setKsRebate(new BigDecimal(sp[4]));
    		stre.setSyxwRebate(new BigDecimal(sp[3]));
    		stre.setPk10Rebate(new BigDecimal(sp[5]));
    		stre.setDpcRebate(new BigDecimal(sp[6]));
    	}*/
		// 通过forech循环设置代理对应的中文代号.
    	for (UserRegister userRegister : alist) {
    		userRegister.setDailiLevel(EUserDailLiLevel.valueOf(userRegister.getDailiLevel()).getDescription());
		}
    	page.setPageContent(alist);
    	return page;
    }
    
    /**
     * 删除推广链接
     * @param id
     * @return
     */
    public int delRegister(Integer id){
    	return userRegisterMapper.delRegister(id);
    }
}
