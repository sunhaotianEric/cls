package com.team.lottery.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.quotacontrol.QuotaControlMapper;
import com.team.lottery.vo.BizSystemDomain;
import com.team.lottery.vo.QuotaControl;

@Service("quotaControlService")
public class QuotaControlService {
	
	private static Logger logger = LoggerFactory.getLogger(QuotaControlService.class);

	@Autowired
	QuotaControlMapper controlMapper;
	
	public int deleteByPrimaryKey(Long id)
	{
		return controlMapper.deleteByPrimaryKey(id);
	}


	public int insertSelective(QuotaControl record){
    	return controlMapper.insertSelective(record);
    }

	public QuotaControl selectByPrimaryKey(Long id){
    	return controlMapper.selectByPrimaryKey(id);
    }

	public int updateByPrimaryKeySelective(QuotaControl record){
    	return controlMapper.updateByPrimaryKeySelective(record);
    }

	public int updateByPrimaryKey(QuotaControl record){
    	return controlMapper.updateByPrimaryKey(record);
    }
    
    /**
     * 根据bizSystem查找相应的记录
     * @param bizSystem
     * @return
     */
	public List<QuotaControl> getQuotaControlByBizSystem(String bizSystem){
    	return controlMapper.getQuotaControlByBizSystem(bizSystem);
    }
	
    /**
     * 查找所有的
     * @return
     */
    public Page getAllQuotaControlByQueryPage(QuotaControl query,Page page){
		page.setTotalRecNum(controlMapper.getAllQuotaControlByQueryPageCount(query));
		page.setPageContent(controlMapper.getAllQuotaControlByQueryPage(query,page.getStartIndex(),page.getPageSize()));
    	return page;
    }
}
