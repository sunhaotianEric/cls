package com.team.lottery.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.mapper.userquotadetail.UserQuotaDetailMapper;
import com.team.lottery.vo.UserQuota;
import com.team.lottery.vo.UserQuotaDetail;

@Service("userQuotaDetailService")
public class UserQuotaDetailService {
	
	@Autowired
	private UserQuotaDetailMapper userQuotaDetailMapper;
	
	public int deleteByPrimaryKey(Long id) {
		return userQuotaDetailMapper.deleteByPrimaryKey(id);
	}

	public int insert(UserQuotaDetail record) {
		return userQuotaDetailMapper.insert(record);
	}

	public int insertSelective(UserQuotaDetail record) {
		return userQuotaDetailMapper.insertSelective(record);
	}

	public UserQuotaDetail selectByPrimaryKey(Long id) {
		return userQuotaDetailMapper.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(UserQuotaDetail record) {
		return userQuotaDetailMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(UserQuotaDetail record) {
		return userQuotaDetailMapper.updateByPrimaryKey(record);
	}
	
	/**
	 * 当配额变动时保存配额帐变明细
	 * @param quota
	 * @param oldQuota
	 * @param isFront 是否是前台编辑改变
	 * @param opertateUserName 相关用户名
	 */
	@Transactional(readOnly=false,rollbackFor=Exception.class)
	public void saveQuotaDetailWhenChange(UserQuota quota, UserQuota oldQuota, Boolean isFront, String opertateUserName) {
		
		int remainChange = quota.getRemainNum() - (oldQuota.getRemainNum()==-1?0:oldQuota.getRemainNum());
		//!=0表示有配额数有变化
		if(remainChange != 0) {
			UserQuotaDetail quotaDetail = new UserQuotaDetail();
			quotaDetail.setUserId(oldQuota.getUserId());
			quotaDetail.setUserName(oldQuota.getUserName());
		
			if(remainChange > 0) {
				quotaDetail.setType(1);
				quotaDetail.setChangeNum(remainChange);
				quotaDetail.setRemark(quota.getUserName()+"的"+quota.getSscRebate()+"模式配额增加了"+remainChange+"个");
			} else {
				quotaDetail.setType(0);
				quotaDetail.setChangeNum(-remainChange);
				quotaDetail.setRemark(quota.getUserName()+"的"+quota.getSscRebate()+"模式配额减少了"+(-remainChange)+"个");
			}
			quotaDetail.setOperateUseSscRebate(oldQuota.getSscRebate());
			quotaDetail.setUpdateLevel(0);
			quotaDetail.setBizSystem(oldQuota.getBizSystem());
			quotaDetail.setBeforeRemain(oldQuota.getRemainNum());
			quotaDetail.setAfterRemain(quota.getRemainNum());
			quotaDetail.setCreatedDate(new Date());
			this.insertSelective(quotaDetail);
		}
		
	}
}
