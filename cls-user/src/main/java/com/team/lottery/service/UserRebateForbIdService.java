package com.team.lottery.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.userrebateforbid.UserRebateForbidMapper;
import com.team.lottery.vo.UserRebateForbid;

@Service("UserRebateForbIdService")
public class UserRebateForbIdService {
	@Autowired
	public UserRebateForbidMapper userRebateForbidMapper;
	

	public int deleteByPrimaryKey(Integer id){
		return userRebateForbidMapper.deleteByPrimaryKey(id);
	}

	public int insert(UserRebateForbid record){
		return userRebateForbidMapper.insert(record);
	}

	public int insertSelective(UserRebateForbid record){
		return userRebateForbidMapper.insertSelective(record);
	}

	public UserRebateForbid selectByPrimaryKey(Integer id){
		return userRebateForbidMapper.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(UserRebateForbid record){
		return userRebateForbidMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(UserRebateForbid record){
		return userRebateForbidMapper.updateByPrimaryKey(record);
	}
	

	public UserRebateForbid getUserRebateForbidByUserId(Integer userId){
		return userRebateForbidMapper.getUserRebateForbidByUserId(userId);
	}
	
	/**
	 * 根据业务系统查找所有返点禁止名单
	 * @param bizSystem
	 * @return
	 */
	public List<Integer> getUserRebateForbidByBizSystem(String bizSystem){
		return userRebateForbidMapper.getUserRebateForbidByBizSystem(bizSystem);
	}
	
	/**
	 * 查找所有返点禁止名单
	 * @param bizSystem
	 * @return
	 */
	public List<Integer> getAllUserRebateForbid(){
		return userRebateForbidMapper.getAllUserRebateForbid();
	}
	/**
	 * 分页查询返点禁止名单
	 */
	public Page getAllUserRebateForbid (String bizSystem,String userName,Page page){
		page.setPageContent(userRebateForbidMapper.getAllUserRebateForbidPage(bizSystem, userName,page.getStartIndex(),page.getPageSize()));
		page.setTotalRecNum(userRebateForbidMapper.getAllUserRebateForbIdPageCount(bizSystem, userName));
		return page;
	}
	
	
}
