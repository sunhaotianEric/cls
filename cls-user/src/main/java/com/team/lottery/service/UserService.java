package com.team.lottery.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.enums.EMoneyDetailType;
import com.team.lottery.enums.EUserDailLiLevel;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.DeleteAgoDataInfo;
import com.team.lottery.extvo.MoneyDetailQuery;
import com.team.lottery.extvo.OrderQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.SendNoteQueryVo;
import com.team.lottery.extvo.TeamUserQuery;
import com.team.lottery.extvo.UserDeleteQuery;
import com.team.lottery.extvo.UserQuery;
import com.team.lottery.extvo.UserTotalMoneyByTypeVo;
import com.team.lottery.mapper.user.UserMapper;
import com.team.lottery.system.BizSystemConfigManager;
import com.team.lottery.system.BizSystemConfigVO;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.MD5PasswordUtil;
import com.team.lottery.util.MakeOrderNum;
import com.team.lottery.util.UserNameUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.LoginLog;
import com.team.lottery.vo.MoneyDetail;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserBank;
import com.team.lottery.vo.UserDel;
import com.team.lottery.vo.UserMoneyTotal;
import com.team.lottery.vo.UserQuota;
import com.team.lottery.vo.UserRegister;
import com.team.lottery.vo.UserSafetyQuestion;

@Service("userService")
public class UserService {

	private static Logger logger = LoggerFactory.getLogger(UserService.class);

	@Autowired
	private UserMapper userMapper;

	@Autowired
	private LoginLogService loginLogService;
	@Autowired
	private MoneyDetailService moneyDetailService;
	@Autowired
	private UserBankService userBankService;
	@Autowired
	private UserSafetyQuestionService userSafetyQuestionService;
	@Autowired
	private UserQuotaService userQuotaService;
	@Autowired
	private UserRegisterService userRegisterService;// 用户注册的
	@Autowired
	private UserDeleteServier userDeleteServier;
	@Autowired
	private UserMoneyTotalService userMoneyTotalService;

	@Autowired
	private PasswordErrorLogService passwordErrorLogService;
	

	public User selectByPrimaryKey(Integer id) {
		return userMapper.selectByPrimaryKey(id);
	}

	public int deleteByPrimaryKey(Integer id) {
		return userMapper.deleteByPrimaryKey(id);
	}

	/**
	 * 删除游客账户
	 * 
	 * @param id
	 * @return
	 */
	public int delTouristUser(Integer id) {
		return userMapper.delTouristUser(id);
	}

	/**
	 * 删除游客账户
	 * 
	 * @param id
	 * @return
	 */
	public int delAllTouristUser(User user) {
		return userMapper.delAllTouristUser(user);
	}

	/**
	 * 查找用户并且加上锁
	 * 
	 * @param id
	 * @return
	 * @deprecated
	 */
	public User selectByPrimaryKeyForUpdate(Integer id) {
		logger.info("使用锁对用户进行锁定,用户id[" + id + "]");
		User user = userMapper.selectByPrimaryKeyForUpdate(id);
		logger.info("锁获取成功,用户名[" + user.getUserName() + "]");
		return user;
	}

	/**
	 * 根据用户名,查找用户并且加上锁
	 * 
	 * @param id
	 * @return
	 * @deprecated
	 */
	public User selectByUserNameForUpdate(String memberUserName) {
		logger.info("使用锁对用户进行锁定,用户名[" + memberUserName + "]");
		User user = userMapper.selectByUserNameForUpdate(memberUserName);
		logger.info("锁获取成功,用户名[" + user.getId() + "]");
		return user;
	}

	public synchronized int insertSelective(User record) {
		record.setCode(getUserMaxCode(record.getBizSystem())); // 获取最大的用户编码
		record.setAddtime(new Date());
		record.setLastLoginTime(new Date());
		record.setLastLogoutTime(new Date()); // 添加时，添加时间即是退出时间
		record.setPassword(MD5PasswordUtil.GetMD5Code(record.getPassword())); // 密码加密
		setMoneyScale(record); // 控制所有金额的精度
		int result = userMapper.insertSelective(record);

		if (result > 0)// 插入成功，新增一条用户money统计表
		{
			record = userMapper.getUserByName(record.getBizSystem(), record.getUserName());
			UserMoneyTotal userMoneyTotal = new UserMoneyTotal();
			userMoneyTotal.setUserId(record.getId());
			userMoneyTotal.setUserName(record.getUserName());
			userMoneyTotal.setBizSystem(record.getBizSystem());
			userMoneyTotalService.insertSelective(userMoneyTotal);
		}
		return result;
	}

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int updateByPrimaryKeySelective(User record) {
		setMoneyScale(record); // 控制所有金额的精度
		userMapper.updateByPrimaryKeySelective(record);
		return record.getId();
	}

	/**
	 * 更新用户 版本号+1
	 * 
	 * @param record
	 * @return
	 * @throws UnEqualVersionException
	 */
	public int updateByPrimaryKeyWithVersionSelective(User record) throws UnEqualVersionException {
		int res = userMapper.updateByPrimaryKeyWithVersionSelective(record);
		if (res == 0) {
			throw new UnEqualVersionException("更新用户id[" + record.getId() + "]时发现版本号不一致");
		}
		return res;
	}

	/**
	 * 控制所有金额的精度
	 * 
	 * @param record
	 */
	public void setMoneyScale(User record) {
		if (record.getMoney() != null) {
			record.setMoney(record.getMoney().setScale(ConstantUtil.BIGDECIMAL_SCAL, BigDecimal.ROUND_HALF_UP));
		}
		if (record.getCanWithdrawMoney() != null) {
			record.setCanWithdrawMoney(record.getCanWithdrawMoney().setScale(ConstantUtil.BIGDECIMAL_SCAL, BigDecimal.ROUND_HALF_UP));
		}
		if (record.getIceMoney() != null) {
			record.setIceMoney(record.getIceMoney().setScale(ConstantUtil.BIGDECIMAL_SCAL, BigDecimal.ROUND_HALF_UP));
		}
		if (record.getTotalCanWithdrawMoney() != null) {
			record.setTotalCanWithdrawMoney(record.getTotalCanWithdrawMoney().setScale(ConstantUtil.BIGDECIMAL_SCAL, BigDecimal.ROUND_HALF_UP));
		}
		if (record.getPoint() != null) {
			record.setPoint(record.getPoint().setScale(ConstantUtil.BIGDECIMAL_SCAL, BigDecimal.ROUND_HALF_UP));
		}
		if (record.getBonusScale() != null) {
			record.setBonusScale(record.getBonusScale().setScale(ConstantUtil.BIGDECIMAL_SCAL, BigDecimal.ROUND_HALF_UP));
		}
	}

	/**
	 * 查询用户最大编码
	 * 
	 * @return
	 */
	public Integer getUserMaxCode(String bizSystem) {
		Integer userCode = userMapper.getUserMaxCode(bizSystem);
		if (userCode == null || userCode == 0) {
			return 1;
		}
		userCode++;
		return userCode;
	}

	/**
	 * 添加用户，同时添加用户配额信息，新增一条用户money统计表
	 * 
	 * @param record
	 * @return
	 * @throws Exception
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public synchronized void saveUser(User newUser) throws Exception {
		newUser.setCode(getUserMaxCode(newUser.getBizSystem())); // 获取最大的用户编码
		newUser.setAddtime(new Date());
		newUser.setLastLoginTime(new Date());
		newUser.setLastLogoutTime(new Date()); // 添加时，添加时间即是退出时间
		newUser.setPassword(MD5PasswordUtil.GetMD5Code(newUser.getPassword())); // 密码加密
		newUser.setVipLevel("1");
		newUser.setLevelName("农民");
		newUser.setPoint(new BigDecimal(0));
		setMoneyScale(newUser); // 控制所有金额的精度
		// 根据模式设置代理类型
		if (newUser.getRegfrom() != null && !"".equals(newUser.getRegfrom()) && !newUser.getDailiLevel().equals(EUserDailLiLevel.REGULARMEMBERS.getCode())) {
			String parentUserName = UserNameUtil.getUpLineUserName(newUser.getRegfrom());
			User parentUser = getUserByName(newUser.getBizSystem(), parentUserName);
			// 父超级代理，下级为直属
			if (EUserDailLiLevel.SUPERAGENCY.getCode().equals(parentUser.getDailiLevel())) {
				newUser.setDailiLevel(EUserDailLiLevel.DIRECTAGENCY.getCode());
			}
			// 父直属，下级为总代
			if (EUserDailLiLevel.DIRECTAGENCY.getCode().equals(parentUser.getDailiLevel())) {
				newUser.setDailiLevel(EUserDailLiLevel.GENERALAGENCY.getCode());
			}
			// 模式 为总代， 下级为代理
			if (EUserDailLiLevel.GENERALAGENCY.getCode().equals(parentUser.getDailiLevel())) {
				newUser.setDailiLevel(EUserDailLiLevel.ORDINARYAGENCY.getCode());
			}
		}

		try {
			int result = userMapper.insertSelective(newUser);
			if (result > 0)// 插入成功，新增一条用户money统计表
			{
				newUser = userMapper.getUserByName(newUser.getBizSystem(), newUser.getUserName());
				UserMoneyTotal userMoneyTotal = new UserMoneyTotal();
				userMoneyTotal.setEnabled(newUser.getIsTourist() == 1 ? 0 : 1);
				userMoneyTotal.setUserId(newUser.getId());
				userMoneyTotal.setUserName(newUser.getUserName());
				userMoneyTotal.setBizSystem(newUser.getBizSystem());
				userMoneyTotalService.insertSelective(userMoneyTotal);
			}

			// 处理配额变动
			String parentUserName = UserNameUtil.getUpLineUserName(newUser.getRegfrom());
			User parentUser = getUserByName(newUser.getBizSystem(), parentUserName);
			userQuotaService.dealAddUserQuotaChange(parentUser, newUser);
		} catch (Exception e) {
			logger.error("保存用户名[{}],业务系统[{}]异常", newUser.getUserName(), newUser.getBizSystem(), e);
			throw e;
		}

	}

	/**
	 * 代理给用户充值金额
	 * 
	 * @param userId
	 * @param transferMoney
	 * @throws Exception
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void transferForUser(String userName, String bizSystem, BigDecimal transferMoney, User currentUser) throws UnEqualVersionException, Exception {
		logger.info("代理给用户充值金额,userName[" + userName + "]");
		// User currentUser = BaseDwrUtil.getCurrentUser();
		BizSystemConfigVO bizSystemConfig = BizSystemConfigManager.getBizSystemConfig(bizSystem);

		// User dailiUser =
		// this.selectByPrimaryKeyForUpdate(currentUser.getId());
		User dailiUser = this.selectByPrimaryKey(currentUser.getId());
		if (dailiUser.getMoney().compareTo(transferMoney) < 0) {
			throw new Exception("对不起,您余额不足.");
		}

		User transUser = this.getUserByName(bizSystem, userName);
		if (transUser == null) {
			throw new Exception("用户不存在.");
		}

		// 查询当日转出的金额
		Date nowDate = new Date();
		Date todayStart = DateUtil.getNowStartTimeByStart(nowDate);
		Date todayEnd = DateUtil.getNowStartTimeByEnd(nowDate);
		MoneyDetailQuery query = new MoneyDetailQuery();
		query.setUserName(dailiUser.getUserName());
		query.setDetailType(EMoneyDetailType.PAY_TRANFER.name());
		query.setCreatedDateStart(todayStart);
		query.setCreatedDateEnd(todayEnd);
		List<MoneyDetail> moneyDetails = moneyDetailService.queryMoneyDetailsByCondition(query);
		BigDecimal todayTransMoney = BigDecimal.ZERO;
		if (CollectionUtils.isNotEmpty(moneyDetails)) {
			for (MoneyDetail md : moneyDetails) {
				todayTransMoney = todayTransMoney.add(md.getPay());
			}
		}
		todayTransMoney = todayTransMoney.add(transferMoney);

		if (todayTransMoney.compareTo(bizSystemConfig.getExceedTransferDayLimitExpenses()) > 0) {
			throw new Exception("当天转账总金额不能超出系统设定最高金额" + bizSystemConfig.getExceedTransferDayLimitExpenses() + "元");
		}
		List<UserTotalMoneyByTypeVo> userTotalMoneyByTypeVos = new ArrayList<UserTotalMoneyByTypeVo>();
		MoneyDetail transUserMoneyDetail = new MoneyDetail();
		transUserMoneyDetail.setUserId(transUser.getId()); // 用户id
		transUserMoneyDetail.setOrderId(null); // 订单id
		transUserMoneyDetail.setBizSystem(bizSystem);
		transUserMoneyDetail.setUserName(transUser.getUserName()); // 用户名
		transUserMoneyDetail.setOperateUserName(currentUser.getUserName()); // 操作人员
		transUserMoneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.TS)); // 订单单号
		transUserMoneyDetail.setWinLevel(null);
		transUserMoneyDetail.setWinCost(null);
		transUserMoneyDetail.setWinCostRule(null);
		transUserMoneyDetail.setDetailType(EMoneyDetailType.INCOME_TRANFER.name());
		transUserMoneyDetail.setIncome(transferMoney);
		transUserMoneyDetail.setPay(new BigDecimal(0));
		transUserMoneyDetail.setBalanceBefore(transUser.getMoney()); // 支出前金额
		transUserMoneyDetail.setBalanceAfter(transUser.getMoney().add(transferMoney)); // 支出后的金额
		transUserMoneyDetail.setDescription("收入转账:" + transferMoney);
		moneyDetailService.insertSelective(transUserMoneyDetail); // 保存资金明细
		// 转账转入增加用户余额
		transUser.addMoney(transferMoney, false); // 余额增加
		// 针对盈亏报表多线程不会事物回滚处理方式
		UserTotalMoneyByTypeVo userTotalMoneyByTypeVo = new UserTotalMoneyByTypeVo(transUser, EMoneyDetailType.INCOME_TRANFER, transferMoney);
		userTotalMoneyByTypeVos.add(userTotalMoneyByTypeVo);
		// 转账收入
		// userMoneyTotalService.addUserTotalMoneyByType(transUser,
		// EMoneyDetailType.INCOME_TRANFER, transferMoney);

		MoneyDetail dailiUserMoneyDetail = new MoneyDetail();
		dailiUserMoneyDetail.setUserId(dailiUser.getId()); // 用户id
		dailiUserMoneyDetail.setOrderId(null); // 订单id
		dailiUserMoneyDetail.setBizSystem(bizSystem);
		dailiUserMoneyDetail.setUserName(dailiUser.getUserName()); // 用户名
		dailiUserMoneyDetail.setOperateUserName(currentUser.getUserName()); // 操作人员
		dailiUserMoneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.TS)); // 订单单号
		dailiUserMoneyDetail.setWinLevel(null);
		dailiUserMoneyDetail.setWinCost(null);
		dailiUserMoneyDetail.setWinCostRule(null);
		dailiUserMoneyDetail.setDetailType(EMoneyDetailType.PAY_TRANFER.name());
		dailiUserMoneyDetail.setIncome(new BigDecimal(0));
		dailiUserMoneyDetail.setPay(transferMoney);
		dailiUserMoneyDetail.setBalanceBefore(dailiUser.getMoney()); // 支出前金额
		dailiUserMoneyDetail.setBalanceAfter(dailiUser.getMoney().subtract(transferMoney)); // 支出后的金额
		dailiUserMoneyDetail.setDescription("支出转账:" + transferMoney);
		moneyDetailService.insertSelective(dailiUserMoneyDetail); // 保存资金明细

		// 代理转出扣减用户余额
		dailiUser.addMoney(transferMoney.multiply(new BigDecimal(-1)), true);
		// 针对盈亏报表多线程不会事物回滚处理方式
		UserTotalMoneyByTypeVo userTotalMoneyByTypeVo1 = new UserTotalMoneyByTypeVo(dailiUser, EMoneyDetailType.PAY_TRANFER, transferMoney);
		userTotalMoneyByTypeVos.add(userTotalMoneyByTypeVo1);
		// 转账收入
		// userMoneyTotalService.addUserTotalMoneyByType(dailiUser,
		// EMoneyDetailType.PAY_TRANFER, transferMoney);

		this.updateByPrimaryKeyWithVersionSelective(transUser);
		this.updateByPrimaryKeyWithVersionSelective(dailiUser);
		// 循环遍历插入报表总额和盈亏报表
		for (UserTotalMoneyByTypeVo uTotalMoneyByTypeVo : userTotalMoneyByTypeVos) {
			userMoneyTotalService.addUserTotalMoneyByType(uTotalMoneyByTypeVo.getUser(), uTotalMoneyByTypeVo.getDetailType(), uTotalMoneyByTypeVo.getOperationValue());
		}
	}

	/**
	 * 操作用户续费相关操作
	 * 
	 * @param operateUser
	 * @throws Exception
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void userRenew(Integer userId, String operationType, String operationValueStr, String description, Admin currentAdmin) throws UnEqualVersionException, Exception {
		logger.info("操作用户续费, userId[" + userId + "], operationType[" + operationType + "]");
		User operationUser = this.selectByPrimaryKey(userId);
		if (operationUser == null) {
			return;
		}
		EMoneyDetailType moneyDetailType = null;
		BigDecimal operationValue = new BigDecimal(operationValueStr);
		// 验证
		if (currentAdmin.getState() > Admin.FINANCE_STATE) {
			throw new Exception("您当前没有权限。");
		}
		if (operationValue.compareTo(BigDecimal.ZERO) < 0) {
			throw new Exception("请输入正数！");
		}
		// 给游客充值无效资金明细
		Integer enabled = operationUser.getIsTourist() == 1 ? 0 : 1;
		MoneyDetail moneyDetail = new MoneyDetail();
		moneyDetail.setEnabled(enabled);
		moneyDetail.setUserId(operationUser.getId()); // 用户id
		moneyDetail.setUserName(operationUser.getUserName()); // 用户名
		moneyDetail.setBizSystem(operationUser.getBizSystem());
		moneyDetail.setOperateUserName(currentAdmin.getUsername()); // 操作人员

		moneyDetail.setWinLevel(null);
		moneyDetail.setWinCost(null);
		moneyDetail.setWinCostRule(null);
		moneyDetail.setBizSystem(operationUser.getBizSystem());
		if (operationType.equals("RECHARGE_PRESENT")) {// 充值赠送
			moneyDetailType = EMoneyDetailType.RECHARGE_PRESENT;
			moneyDetail.setDetailType(moneyDetailType.name());
			moneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.ZS)); // 订单单号
			moneyDetail.setIncome(operationValue);
			moneyDetail.setPay(BigDecimal.ZERO);
			moneyDetail.setBalanceBefore(operationUser.getMoney()); // 支出前金额
			moneyDetail.setBalanceAfter(operationUser.getMoney().add(operationValue)); // 支出后的金额
			moneyDetail.setDescription("系统充值赠送:" + operationValue);

			operationUser.addMoney(operationValue, false);

		} else if (operationType.equals("ACTIVITIES_MONEY")) { // 活动彩金
			moneyDetailType = EMoneyDetailType.ACTIVITIES_MONEY;
			moneyDetail.setDetailType(moneyDetailType.name());
			moneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.CJ)); // 订单单号
			moneyDetail.setIncome(operationValue);
			moneyDetail.setPay(BigDecimal.ZERO);
			moneyDetail.setBalanceBefore(operationUser.getMoney()); // 支出前金额
			moneyDetail.setBalanceAfter(operationUser.getMoney().add(operationValue)); // 支出后的金额
			moneyDetail.setDescription("活动彩金:" + operationValue);

			operationUser.addMoney(operationValue, false);
		} else if (operationType.equals("SYSTEM_REDUCE_MONEY_FOR_ACTIVITIES")) { // 系统扣费(活动彩金)
			moneyDetailType = EMoneyDetailType.ACTIVITIES_MONEY;
			moneyDetail.setDetailType(moneyDetailType.name());
			moneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.CJ)); // 订单单号
			moneyDetail.setIncome(BigDecimal.ZERO);
			moneyDetail.setPay(operationValue);
			moneyDetail.setBalanceBefore(operationUser.getMoney()); // 支出前金额
			moneyDetail.setBalanceAfter(operationUser.getMoney().subtract(operationValue)); // 支出后的金额
			moneyDetail.setDescription("活动彩金扣除:" + operationValue);

			if (operationUser.getMoney().compareTo(operationValue) < 0) {
				throw new Exception("当前用户余额不足扣减金额，扣减失败！");
			}
			operationUser.reduceMoney(operationValue, false);

			// 查出UserMoneyTotal 来验证比较金钱
			UserMoneyTotal userMoneyTotal = userMoneyTotalService.selectByUserId(operationUser.getId());
			if (userMoneyTotal.getTotalActivitiesMoney().subtract(operationValue).compareTo(BigDecimal.ZERO) < 0) {
				throw new Exception("活动金额扣减不能超过活动金额【" + userMoneyTotal.getTotalActivitiesMoney() + "】！");
			}
			// 到UserMoneyTotal，对活动金额扣减，要变成负数的
			operationValue = BigDecimal.ZERO.subtract(operationValue);

		} else if (operationType.equals("SYSTEM_REDUCE_MONEY_FOR_RECHARGE_PRESENT")) { // 系统扣费(充值赠送)

			moneyDetailType = EMoneyDetailType.RECHARGE_PRESENT;
			moneyDetail.setDetailType(moneyDetailType.name());
			moneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.ZS)); // 订单单号
			moneyDetail.setIncome(BigDecimal.ZERO);
			moneyDetail.setPay(operationValue);
			moneyDetail.setBalanceBefore(operationUser.getMoney()); // 支出前金额
			moneyDetail.setBalanceAfter(operationUser.getMoney().subtract(operationValue)); // 支出后的金额
			moneyDetail.setDescription("系统充值赠送扣除:" + operationValue);

			if (operationUser.getMoney().compareTo(operationValue) < 0) {
				throw new Exception("当前用户余额不足扣减金额，扣减失败！");
			}
			operationUser.reduceMoney(operationValue, false);

			// 查出UserMoneyTotal 来验证比较金钱
			UserMoneyTotal userMoneyTotal = userMoneyTotalService.selectByUserId(operationUser.getId());
			if (userMoneyTotal.getTotalRechargePresent().subtract(operationValue).compareTo(BigDecimal.ZERO) < 0) {
				throw new Exception("赠送金额扣减不能超过赠送金额【" + userMoneyTotal.getTotalRechargePresent() + "】！");
			}
			// 到UserMoneyTotal，对赠送金额的扣除，要变成负数的
			operationValue = BigDecimal.ZERO.subtract(operationValue);

		} else if (operationType.equals("SYSTEM_ADD_MONEY")) { // 人工存入
			moneyDetailType = EMoneyDetailType.SYSTEM_ADD_MONEY;
			moneyDetail.setDetailType(moneyDetailType.name());
			moneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.XF)); // 订单单号
			moneyDetail.setIncome(operationValue);
			moneyDetail.setPay(BigDecimal.ZERO);
			moneyDetail.setBalanceBefore(operationUser.getMoney()); // 支出前金额
			moneyDetail.setBalanceAfter(operationUser.getMoney().add(operationValue)); // 支出后的金额
			moneyDetail.setDescription("人工存入:" + operationValue);
			operationUser.addMoney(operationValue, true);
			// if(operationType.equals("SYSTEM_ADD_MONEY_HAS_CanWithdraw")){
			// operationUser.addMoney(operationValue, true);
			// }else{
			// operationUser.addMoney(operationValue, false);
			// }

		} else if (operationType.equals("SYSTEM_REDUCE_MONEY")) { // 误存提出

			moneyDetailType = EMoneyDetailType.SYSTEM_REDUCE_MONEY;
			moneyDetail.setDetailType(moneyDetailType.name());
			moneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.KF)); // 订单单号
			moneyDetail.setIncome(BigDecimal.ZERO);
			moneyDetail.setPay(operationValue);
			moneyDetail.setBalanceBefore(operationUser.getMoney()); // 支出前金额
			moneyDetail.setBalanceAfter(operationUser.getMoney().subtract(operationValue)); // 支出后的金额
			moneyDetail.setDescription("误存提出:" + operationValue);

			if (operationUser.getMoney().compareTo(operationValue) < 0) {
				throw new Exception("当前用户余额不足扣减金额，扣减失败！");
			}
			operationUser.reduceMoney(operationValue, false);
			// if(operationType.equals("SYSTEM_REDUCE_MONEY_HAS_CanWithdraw")){
			// operationUser.reduceMoney(operationValue, true);
			// }else{
			// operationUser.reduceMoney(operationValue, false);
			// }

		} else if (operationType.equals("MANAGE_REDUCE_MONEY")) { // 行政提出

			moneyDetailType = EMoneyDetailType.MANAGE_REDUCE_MONEY;
			moneyDetail.setDetailType(moneyDetailType.name());
			moneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.XZ)); // 订单单号
			moneyDetail.setIncome(BigDecimal.ZERO);
			moneyDetail.setPay(operationValue);
			moneyDetail.setBalanceBefore(operationUser.getMoney()); // 支出前金额
			moneyDetail.setBalanceAfter(operationUser.getMoney().subtract(operationValue)); // 支出后的金额
			moneyDetail.setDescription("行政提出:" + operationValue);

			if (operationUser.getMoney().compareTo(operationValue) < 0) {
				throw new Exception("当前用户余额不足扣减金额，扣减失败！");
			}
			operationUser.reduceMoney(operationValue, true);
			// if(operationType.equals("SYSTEM_REDUCE_MONEY_HAS_CanWithdraw")){
			// operationUser.reduceMoney(operationValue, true);
			// }else{
			// operationUser.reduceMoney(operationValue, false);
			// }

		} else if (operationType.equals("RECHARGE_MANUAL")) { // 人工存款
			moneyDetailType = EMoneyDetailType.RECHARGE_MANUAL;
			moneyDetail.setDetailType(moneyDetailType.name());
			moneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.CK)); // 订单单号
			moneyDetail.setIncome(operationValue);
			moneyDetail.setPay(BigDecimal.ZERO);
			moneyDetail.setBalanceBefore(operationUser.getMoney()); // 支出前金额
			moneyDetail.setBalanceAfter(operationUser.getMoney().add(operationValue)); // 支出后的金额
			moneyDetail.setDescription("人工存款:" + operationValue);
			operationUser.addMoney(operationValue, false);
			// if(operationType.equals("SYSTEM_ADD_MONEY_HAS_CanWithdraw")){
			// operationUser.addMoney(operationValue, true);
			// }else{
			// operationUser.addMoney(operationValue, false);
			// }

		} else {
			throw new Exception("不可知的操作类型");
		}
		if (description != null && description != "") {
			moneyDetail.setDescription(moneyDetail.getDescription() + "(" + description + ")");
		}
		moneyDetailService.insertSelective(moneyDetail);
		this.updateByPrimaryKeyWithVersionSelective(operationUser);
		userMoneyTotalService.addUserTotalMoneyByType(operationUser, moneyDetailType, operationValue);
	}

	/**
	 * 操作用户积分相关操作
	 * 
	 * @param operateUser
	 * @throws Exception
	 */
	/*
	 * @Transactional(readOnly=false,rollbackFor=Exception.class) public void
	 * userPoint(Integer userId,String operationType,String operationValueStr)
	 * throws UnEqualVersionException,Exception{
	 * logger.info("操作用户积分,userId["+userId
	 * +"],operationType["+operationType+"]"); //User operationUser =
	 * this.selectByPrimaryKeyForUpdate(userId); User operationUser =
	 * this.selectByPrimaryKey(userId); if(operationUser == null){ return; }
	 * BigDecimal operationValue = new BigDecimal(operationValueStr);
	 * if(operationUser.getPoint() == null){ operationUser.setPoint(new
	 * BigDecimal("0")); }
	 * 
	 * 
	 * PointDetail pointDetail = new PointDetail();
	 * pointDetail.setLotteryId(chargeIdService.getNewOrderId().toString());
	 * pointDetail
	 * .setOperateUserName(BaseDwrUtil.getCurrentAdmin().getUsername()); //操作人员
	 * pointDetail.setUserName(operationUser.getUserName());
	 * pointDetail.setUserId(operationUser.getId());
	 * 
	 * 
	 * if(operationType.equals("add")){
	 * operationUser.setPoint(operationUser.getPoint().add(operationValue));
	 * 
	 * pointDetail.setDetailType(EPointDetailType.SYSTEM_ADD_POINT.name());
	 * //管理员续积分 pointDetail.setIncome(operationValue); pointDetail.setPay(new
	 * BigDecimal(0));
	 * pointDetail.setPointsBefore(operationUser.getPoint().subtract
	 * (operationValue)); //支出前金额
	 * pointDetail.setPointsAfter(operationUser.getPoint()); //支出后的金额
	 * pointDetail.setDescription("管理员为您续积分:" + operationValue); }else
	 * if(operationType.equals("reduce")){
	 * if(operationValue.compareTo(operationUser.getPoint()) <= 0){
	 * operationUser
	 * .setPoint(operationUser.getPoint().subtract(operationValue)); }else{
	 * throw new Exception("去除的积分数目比余积分数大,暂不支持此操作."); }
	 * 
	 * pointDetail.setDetailType(EPointDetailType.SYSTEM_REDUCE_POINT.name());
	 * //管理员扣积分 pointDetail.setIncome(new BigDecimal(0));
	 * pointDetail.setPay(operationValue);
	 * pointDetail.setPointsBefore(operationUser
	 * .getPoint().add(operationValue)); //支出前金额
	 * pointDetail.setPointsAfter(operationUser.getPoint()); //支出后的金额
	 * pointDetail.setDescription("管理员扣除积分:" + operationValue); }else
	 * if(operationType.equals("addExtend")){
	 * if(operationUser.getDailiLevel().equals
	 * (EUserDailLiLevel.DIRECTAGENCY.name())){
	 * operationUser.setPoint(operationUser.getPoint().add(operationValue));
	 * }else{ throw new Exception("当前操作的用户非直属用户,暂不支持此操作."); }
	 * 
	 * pointDetail.setDetailType(EPointDetailType.ADD_EXTEND.name()); //积分类型为投注
	 * pointDetail.setIncome(operationValue); pointDetail.setPay(new
	 * BigDecimal(0));
	 * pointDetail.setPointsBefore(operationUser.getPoint().subtract
	 * (operationValue)); //支出前金额
	 * pointDetail.setPointsAfter(operationUser.getPoint()); //支出后的金额
	 * pointDetail.setDescription("管理员增加推广积分:" + operationValue); }else
	 * if(operationType.equals("reduceExtend")){ //扣除推广积分
	 * if(!operationUser.getDailiLevel
	 * ().equals(EUserDailLiLevel.DIRECTAGENCY.name())){ throw new
	 * Exception("当前操作的用户非直属用户,暂不支持此操作."); } //是否积分充足
	 * if(operationValue.compareTo(operationUser.getPoint()) <= 0){
	 * operationUser
	 * .setPoint(operationUser.getPoint().subtract(operationValue)); }else{
	 * throw new Exception("扣费的积分比剩余的积分大,暂不支持此操作."); }
	 * 
	 * pointDetail.setDetailType(EPointDetailType.REDUCE_EXTEND.name());
	 * //资金类型为投注 pointDetail.setIncome(new BigDecimal(0));
	 * pointDetail.setPay(operationValue);
	 * pointDetail.setPointsBefore(operationUser
	 * .getPoint().add(operationValue)); //支出前金额
	 * pointDetail.setPointsAfter(operationUser.getPoint()); //支出后的金额
	 * pointDetail.setDescription("管理员减少推广积分:" + operationValue); }else{ throw
	 * new RuntimeException("不可知的操作类型"); }
	 * 
	 * //this.updateByPrimaryKeySelective(operationUser);
	 * this.updateByPrimaryKeyWithVersionSelective(operationUser);
	 * pointDetailService.insertSelective(pointDetail); //保存积分明细 }
	 */

	/**
	 * 根据条件查找用户
	 * 
	 * @param user
	 * @return
	 */
	public User getUser(User user) {
		return userMapper.getUser(user);
	}

	/**
	 * 查找用户的下级会员
	 * 
	 * @param username
	 * @return
	 */
	public List<User> getRegsByUser(String currentUserName, String memberUserName, Boolean isQuerySub, String currentUsersBizSystem) {
		return userMapper.getRegFromByUser(currentUserName, memberUserName, isQuerySub, currentUsersBizSystem);
	}

	/**
	 * 根据用户名查找用户
	 * 
	 * @param username
	 * @param bizSystem
	 * @return
	 */
	public User getUserByName(String bizSystem, String username) {
		return userMapper.getUserByName(bizSystem, username);
	}

	/**
	 * 用户名查找对应的用户
	 * 
	 * @param username
	 * @return
	 */
	public User getUserForUnique(User user) {
		return userMapper.getUserForUnique(user);
	}

	/**
	 * 登录逻辑事务处理
	 * 
	 * @param loginUser
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void loginDeal(User loginUser, LoginLog loginLog) {

		User editorUser = new User();
		editorUser.setId(loginUser.getId());
		editorUser.setLoginTimes(loginUser.getLoginTimes() + 1);
		editorUser.setLogip(loginLog.getLoginIp());
		editorUser.setLastLoginTime(new Date());
		this.updateByPrimaryKeySelective(editorUser);

		// 插入登录日志
		loginLogService.insertSelective(loginLog);
		// 登陆成功删除错误日志记录
		passwordErrorLogService.updatePasswordErrorLogByUserName(loginUser.getBizSystem(), loginUser.getUserName());
		logger.info("用户[" + loginUser.getUserName() + "]登录成功");
	}

	/**
	 * 查找满足条件的用户
	 * 
	 * @param query
	 * @return
	 */
	public List<User> getUsersByCondition(UserQuery query) {
		return userMapper.getUsersByCondition(query);
	}

	/**
	 * 查找所有的用户数据
	 * 
	 * @return
	 */
	public Page getAllUsersByQueryPage(UserQuery query, Page page) {
		page.setTotalRecNum(userMapper.getAllUsersByQueryPageCount(query));
		page.setPageContent(userMapper.getAllUsersByQueryPage(query, page.getStartIndex(), page.getPageSize()));
		return page;
	}

	/**
	 * 查找所有的用户首充数据
	 * 
	 * @return
	 */
	public Page getAllUsersByQueryfirstRechargeTimePage(UserQuery query, Page page) {
		page.setTotalRecNum(userMapper.getAllUsersByQueryFristRechargeTimePageCount(query));
		page.setPageContent(userMapper.getAllUsersByQueryFristRechargeTimePage(query, page.getStartIndex(), page.getPageSize()));
		return page;
	}

	/**
	 * 分页查询团队的下级会员列表
	 * 
	 * @param query
	 * @param startNum
	 * @param pageNum
	 * @return
	 */
	public Page getTeamUserListByQueryPage(@Param("query") TeamUserQuery query, Page page) {
		try {
			page.setTotalRecNum(userMapper.getTeamUserListByQueryPageCount(query));
			List<User> users = userMapper.getTeamUserListByQueryPage(query, page.getStartIndex(), page.getPageSize());
			page.setPageContent(users);
		} catch (Exception e) {
			logger.error("查询异常", e);
		}
		return page;
	}

	/**
	 * 分页子线会员列表
	 * 
	 * @param query
	 * @param startNum
	 * @param pageNum
	 * @return
	 */
	public Page getOnlineUserListByQueryPage(List<TeamUserQuery> querys, Page page) {
		try {

			int count = userMapper.getOnlineUserListByQueryPageCount(querys);
			List<User> users = userMapper.getOnlineUserListByQueryPage(querys, page.getStartIndex(), page.getPageSize(), querys.get(0).getOrderSort());
			page.setTotalRecNum(count);
			page.setPageContent(users);
		} catch (Exception e) {
			logger.error("查询异常", e);
		}
		return page;
	}

	/**
	 * 查询团队的下级会员列表
	 * 
	 * @param query
	 * @param startNum
	 * @param pageNum
	 * @return
	 */
	public List<User> getAllTeamUserList(@Param("query") TeamUserQuery query) {
		return userMapper.getAllTeamUserList(query);
	}

	/**
	 * 删除多少天以上没有消费及余额为0的会员
	 * 
	 * @param dayCount
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public Integer deleteNoMoneyForNumberOfDays(Integer dayCount) {
		List<User> users = userMapper.queryNoMoneyForNumberOfDays(dayCount);
		User operateUser = null;
		for (User user : users) { // 将用户标识为无效记录,到时候进行数据库记录的删除
			// userMapper.deleteByPrimaryKey(user.getId());

			operateUser = new User();
			operateUser.setId(user.getId());
			operateUser.setState(0);
			userMapper.updateByPrimaryKeySelective(operateUser);
			// 删除的用户，必须重新登陆
			// FrontUserOnlineInterface.deleteAlreadyEnter(user.getUserName());
			/*
			 * if(SingleLogin.hUserNameForFront.containsValue(user.getUsername())
			 * ){ SingleLogin.deleteAlreadyEnter(user.getUsername()); }
			 */
		}
		return users.size();
	}

	/**
	 * 删除多少天没有登陆的会员
	 * 
	 * @param dayCount
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public Integer deleteForNumberOfDays(Integer dayCount) {
		List<User> users = userMapper.queryForNumberOfDays(dayCount);
		User operateUser = null;
		for (User user : users) { // 将用户标识为无效记录,到时候进行数据库记录的删除
			// userMapper.deleteByPrimaryKey(user.getId());

			operateUser = new User();
			operateUser.setId(user.getId());
			operateUser.setState(0);
			userMapper.updateByPrimaryKeySelective(operateUser);

		}
		return users.size();
	}

	/**
	 * 查找符合条件删除的会员
	 * 
	 * @return
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public Integer everyDeleteUsers(DeleteAgoDataInfo deleteDataInfo, Date date) {

		// 查询出所有的要删除的用户数据
		UserDeleteQuery query = new UserDeleteQuery();
		List<EUserDailLiLevel> level = new ArrayList<EUserDailLiLevel>();
		level.add(EUserDailLiLevel.ORDINARYAGENCY);// 普通代理
		level.add(EUserDailLiLevel.REGULARMEMBERS);// 普通会员

		// Date delDate = date;
		// 30天未登录的会员
		Date delDate = DateUtil.addDaysToDate(date, -30);
		query.setDailiLevels(level);
		query.setLastLogout(delDate);
		query.setMoney(BigDecimal.ZERO);
		query.setSscreBate(deleteDataInfo.getSscRebate()); // 模式
		query.setTeamMember(0); // 团队人数小于等于0
		query.setLowerLevel(0); // 下级人数小于等于0

		List<User> users = getUsersByDeleteCondition(query);

		logger.info("查询出来符合条件的记录有[" + users.size() + "]条");
		// 通过指定用户名找到需要更新的目标
		// TODO 每个业务系统的超级管理员
		User updateUser = getUserByName("SUPER_SYSTEM", "lotterychenhsh");// 每个业务系统的超级管理员
		BigDecimal addMoney = BigDecimal.ZERO;
		int delCount = 0;
		if (CollectionUtils.isNotEmpty(users)) {
			for (User user : users) {
				UserDel userDel = new UserDel();
				BeanUtils.copyProperties(user, userDel);

				try {
					// 删除用户关联银行卡信息
					if (deleteDataInfo.getIsDeleteUserBank() != null && deleteDataInfo.getIsDeleteUserBank() == 1) {
						List<UserBank> userBanks = userBankService.getAllUserBanksByUserId(user.getId());
						if (CollectionUtils.isNotEmpty(userBanks)) {
							logger.info("发现用户[" + user.getUserName() + "]绑定银行卡信息,条数[" + userBanks.size() + "],删除用户的绑定银行卡...");
							for (UserBank userBank : userBanks) {
								userBankService.deleteByPrimaryKey(userBank.getId());
							}
						}
					}
					if (deleteDataInfo.getIsDeleteSafetyQuestions() != null && deleteDataInfo.getIsDeleteSafetyQuestions() == 1) {
						// 删除用户关联的安全问题
						List<UserSafetyQuestion> userSafetyQuestions = userSafetyQuestionService.getQuestionsUserId(user.getId());
						if (CollectionUtils.isNotEmpty(userSafetyQuestions)) {
							logger.info("发现用户[" + user.getUserName() + "]绑定安全问题信息,条数[" + userSafetyQuestions.size() + "],删除用户的安全问题...");
							for (UserSafetyQuestion userSafetyQuestion : userSafetyQuestions) {
								userSafetyQuestionService.deleteByPrimaryKey(userSafetyQuestion.getId());
							}
						}
					}
					// 删除用户关联的注册链接
					List<UserRegister> userRegisters = userRegisterService.getUserRegisterLinks(user.getId());
					if (CollectionUtils.isNotEmpty(userRegisters)) {
						logger.info("发现用户[" + user.getUserName() + "]绑定注册链接,条数[" + userRegisters.size() + "],删除用户的推广链接...");
						for (UserRegister userRegister : userRegisters) {
							userRegisterService.deleteByPrimaryKey(userRegister.getId());
						}
					}

					// 删除用户配额表
					List<UserQuota> userQuotas = userQuotaService.getQuotaByUserName(user.getUserName(), user.getBizSystem());
					if (userQuotas != null) {
						logger.info("发现用户[" + user.getUserName() + "]配额表记录,进行删除");
						for (UserQuota userQuota : userQuotas) {
							userQuotaService.deleteByPrimaryKey(userQuota.getId());
						}
					}

					// 更新被删的用户上级的团队人数和直接下级人数
					if (StringUtils.isNotBlank(user.getRegfrom())) {
						String[] regFromUserNames = user.getRegfrom().split(ConstantUtil.REG_FORM_SPLIT);
						if (regFromUserNames.length > 0) {
							logger.info("更新被删的用户上级的团队人数和直接下级人数：[" + regFromUserNames.length + "]-----");
							for (int i = 0; i < regFromUserNames.length; i++) {
								String upLineUserName = regFromUserNames[i];
								// TODO 待验证
								User updateTeamMember = getUserByName(user.getBizSystem(), upLineUserName);
								if (updateTeamMember != null) {
									User updateTeamUser = new User();
									updateTeamUser.setId(updateTeamMember.getId());
									// 更新上级团队人数
									int teamMemberNum = updateTeamMember.getTeamMemberCount() - 1;
									if (teamMemberNum < 0) {
										teamMemberNum = 0;
									}
									updateTeamUser.setTeamMemberCount(teamMemberNum);
									updateByPrimaryKeySelective(updateTeamUser);
								}

								// 更新上级的直接下级人数。 如果只有上级就减一位下级，如果很多个级别
								// 就减当前等级（regFromUserNames.length - 1）下级人数
								String upLineUserName2 = regFromUserNames[regFromUserNames.length - 1];
								// TODO 待验证
								User directlyUnder = getUserByName(user.getBizSystem(), upLineUserName2);

								if (directlyUnder != null) {
									User updateTeamUser = new User();
									updateTeamUser.setId(directlyUnder.getId());
									// 更新上级团队人数
									int teamMemberNum = directlyUnder.getTeamMemberCount() - 1;
									if (teamMemberNum < 0) {
										teamMemberNum = 0;
									}
									updateTeamUser.setTeamMemberCount(teamMemberNum);

									// 更新上级的直接下级人数
									int lowerLevel = 0;
									lowerLevel = directlyUnder.getDownMemberCount() - 1;
									if (lowerLevel < 0) {
										lowerLevel = 0;
									}
									updateTeamUser.setDownMemberCount(lowerLevel);
									updateByPrimaryKeySelective(updateTeamUser);
								}
							}
						}
					}

					// 只累加有效的会员到 被删的余额加到指定用户名下
					if (user.getState() == 1) {

						updateUser.setIceMoney(updateUser.getIceMoney().add(user.getIceMoney()));
						updateUser.setMoney(updateUser.getMoney().add(user.getMoney()));
						updateUser.setCanWithdrawMoney(updateUser.getCanWithdrawMoney().add(user.getCanWithdrawMoney()));
						updateUser.setTotalCanWithdrawMoney(updateUser.getTotalCanWithdrawMoney().add(user.getTotalCanWithdrawMoney()));
						// :TODO
						/*
						 * updateUser.setTotalNetRecharge(updateUser.
						 * getTotalNetRecharge
						 * ().add(user.getTotalNetRecharge()));
						 * updateUser.setTotalQuickRecharge
						 * (updateUser.getTotalQuickRecharge
						 * ().add(user.getTotalQuickRecharge()));
						 * updateUser.setTotalRecharge
						 * (updateUser.getTotalRecharge
						 * ().add(user.getTotalRecharge()));
						 * updateUser.setTotalRechargePresent
						 * (updateUser.getTotalRechargePresent
						 * ().add(user.getTotalRechargePresent()));
						 * updateUser.setTotalActivitiesMoney
						 * (updateUser.getTotalActivitiesMoney
						 * ().add(user.getTotalActivitiesMoney()));
						 * updateUser.setTotalWithdraw
						 * (updateUser.getTotalWithdraw
						 * ().add(user.getTotalWithdraw()));
						 * updateUser.setTotalFeevalue
						 * (updateUser.getTotalFeevalue
						 * ().add(user.getTotalFeevalue()));
						 * updateUser.setTotalExtend
						 * (updateUser.getTotalExtend().
						 * add(user.getTotalExtend()));
						 * updateUser.setTotalRegister
						 * (updateUser.getTotalRegister
						 * ().add(user.getTotalRegister()));
						 * if(user.getTotalRegister() != BigDecimal.ZERO){
						 * addMoney.add(user.getTotalLottery()); }
						 * logger.info("TotalRegister:[" +
						 * user.getTotalRegister() +"]，第 "+ delCount +"的记录");
						 * 
						 * 
						 * updateUser.setTotalLottery(updateUser.getTotalLottery(
						 * ).add(user.getTotalLottery()));
						 * updateUser.setTotalShopPoint
						 * (updateUser.getTotalShopPoint
						 * ().add(user.getTotalShopPoint()));
						 * if(user.getTotalWin() != BigDecimal.ZERO){
						 * addMoney.add(user.getTotalWin()); }
						 * logger.info("TotalWin:[" + user.getTotalWin()
						 * +"]，第 "+ delCount +"的记录");
						 * 
						 * updateUser.setTotalWin(updateUser.getTotalWin().add(user
						 * .getTotalWin()));
						 * updateUser.setTotalRebate(updateUser
						 * .getTotalRebate().add(user.getTotalRebate()));
						 * updateUser
						 * .setTotalPercentage(updateUser.getTotalPercentage
						 * ().add(user.getTotalPercentage()));
						 * updateUser.setTotalPayTranfer
						 * (updateUser.getTotalPayTranfer
						 * ().add(user.getTotalPayTranfer()));
						 * updateUser.setTotalIncomeTranfer
						 * (updateUser.getTotalIncomeTranfer
						 * ().add(user.getTotalIncomeTranfer()));
						 * updateUser.setTotalHalfMonthBonus
						 * (updateUser.getTotalHalfMonthBonus
						 * ().add(user.getTotalHalfMonthBonus()));
						 * updateUser.setTotalDayBonus
						 * (updateUser.getTotalDayBonus
						 * ().add(user.getTotalDayBonus()));
						 * updateUser.setTotalMonthSalary
						 * (updateUser.getTotalMonthSalary
						 * ().add(user.getTotalMonthSalary()));
						 * updateUser.setTotalDaySalary
						 * (updateUser.getTotalDaySalary
						 * ().add(user.getTotalDaySalary()));
						 * updateUser.setTotalDayCommission
						 * (updateUser.getTotalDayCommission
						 * ().add(user.getTotalDayCommission()));
						 * updateUser.setRemainExtend
						 * (updateUser.getRemainExtend(
						 * ).add(user.getRemainExtend()));
						 */
					}

					userDel.setDeleteDate(delDate);
					userDel.setDeleteCause("满足删除要求");
					userDeleteServier.insert(userDel);// 插入被删除的数据
					deleteByPrimaryKey(user.getId());

					delCount++;
					logger.info("删除用户[" + user.getUserName() + "]，第 " + delCount + "条的记录");

				} catch (Exception e) {
					logger.error("删除用户[" + user.getUserName() + "]时出现错误", e);
				}

			}
		}
		return delCount;
	}

	/**
	 * 统计用户的所有余额、分红、盈利等数据
	 * 
	 * @return
	 */
	public User getTotalUsersConsumeReport() {
		return userMapper.getTotalUsersConsumeReport();
	}

	/**
	 * 更新用户的推荐人关系链
	 * 
	 * @param fromUserName
	 * @param toUserName
	 */
	/*
	 * public void updateUsersRegFrom(User fromUser,User toUser){ //查找到该会员的推荐关系链
	 * List<User> toUpdateUsers =
	 * userMapper.getRegFromByUser(fromUser.getUsername(),null); for(User user :
	 * toUpdateUsers){ user.setRegfrom(toUser.getRegfrom() +
	 * toUser.getUsername() + ConstantUtil.REG_FORM_SPLIT);
	 * this.updateByPrimaryKeySelective(user); } }
	 */

	/**
	 * 判断用户安全密码是否为空
	 * 
	 * @param userId
	 * @return
	 */
	public User getUserByPassword1IsNotNull(Integer userId) {
		return userMapper.getUserByPassword1IsNotNull(userId);
	}

	/**
	 * 用户退出
	 * 
	 * @param
	 * @return
	 */
	public void userLogout(User loginUser) {
		User dealUser = new User();
		dealUser.setId(loginUser.getId());
		dealUser.setLastLogoutTime(new Date());
		this.updateByPrimaryKeySelective(dealUser);
	}


	/**
	 * 查找用户被锁数据
	 * 
	 * @param user
	 * @return
	 */
	public User getUserLock(User user) {
		return userMapper.getUserLock(user);
	}

	/**
	 * 查找所有的用户
	 * 
	 * @return
	 */
	public List<User> getAllUsers() {
		return userMapper.getAllUsers();
	}

	/*	*//**
	 * 获取用户下级的配额信息
	 * 
	 * @param userName
	 * 
	 * @return
	 */
	/*
	 * public UserQuotaInfo getUserQuotaInfoByUser(User user) { String
	 * userNameFormat = ConstantUtil.REG_FORM_SPLIT + user.getUserName() +
	 * ConstantUtil.REG_FORM_SPLIT; UserQuotaInfo userQuotaInfo = new
	 * UserQuotaInfo(); try { user.setUserName(userNameFormat); Integer
	 * equlaLevelNum = userMapper.getUserCountBySscRebate(user,true); //下级模式 -2
	 * user.setSscRebate(user.getSscRebate().subtract(new BigDecimal("2")));
	 * Integer lowerLevelNum = userMapper.getUserCountBySscRebate(user,true);
	 * if(equlaLevelNum == null) { equlaLevelNum = 0; } if(lowerLevelNum ==
	 * null) { lowerLevelNum = 0; }
	 * 
	 * userQuotaInfo.setEqulaLevelNum(equlaLevelNum);
	 * userQuotaInfo.setLowerLevelNum(lowerLevelNum); } catch (Exception e) {
	 * logger.error("查询用户配额出错", e); throw new RuntimeException("查询用户配额出错"); }
	 * return userQuotaInfo; }
	 */

	/**
	 * 根据时时彩模式条件查找用户列表
	 * 
	 * @param query
	 * @return
	 */
	public List<User> getUsersByModelCondition(UserQuery query) {
		return userMapper.getUsersByModelCondition(query);
	}

	/**
	 * 根据要发送站内信条件查找用户列表
	 * 
	 * @param query
	 * @return
	 */
	public List<User> getUsersBySendNoteQuery(SendNoteQueryVo query) {
		return userMapper.getUsersBySendNoteQuery(query);
	}

	/**
	 * 查询需要删除的会员
	 * 
	 * @param query
	 * @return
	 */
	public List<User> getUsersByDeleteCondition(UserDeleteQuery query) {
		return userMapper.getUsersByDeleteCondition(query);
	}

	/**
	 * 根据代理类型统计用户余额总额
	 * 
	 * @param query
	 * @return
	 */
	public List<User> getTotalMoneyByDailiLevel(String bizSystem) {
		return userMapper.getTotalMoneyByDailiLevel(bizSystem);
	}

	/**
	 * 查询新加入的会员人数
	 * 
	 * @param query
	 * @return
	 */
	public Integer getNewUsersCount(TeamUserQuery query) {
		return userMapper.getNewUsersCount(query);
	}

	/**
	 * 获取最大模式值
	 */
	public Integer getMaxRebate(String regfromName, String bizSystem, Integer rebateType) {
		return userMapper.getMaxRebate(regfromName, bizSystem, rebateType);

	}

	/**
	 * 查询出有配额的已使用配额数
	 * 
	 * @return
	 */
	public List<User> userquotaCount(User query) {
		return userMapper.userquotaCount(query);
	}

	/**
	 * 查询首充人数
	 * 
	 * @param bizSystem
	 * @param firstRechargeTime
	 * @return
	 */
	public Integer getUserCountByFirstRecharger(String bizSystem, Date startTime, Date endTime, String userName) {
		return userMapper.getUserCountByFirstRecharger(bizSystem, startTime, endTime, userName);
	}

	/**
	 * 查询当天注册的用户
	 * 
	 * @param addtime
	 * @return
	 */
	public List<User> getUserByAddtime(String bizSystem, String userName, Date startTime, Date endTime, String dailiLevel) {
		return userMapper.getUserByAddtime(bizSystem, userName, startTime, endTime, dailiLevel);
	}

	/**
	 * 根据业务系统和用户名查询该用户是否存在
	 * 
	 * @param user
	 * @return
	 */
	public User getUserByByzSystem(User user) {
		return userMapper.getUserByByzSystem(user);
	}

	/**
	 * 修改用户所有下级的奖金模式或者星级数
	 * 
	 * @param user
	 * @return
	 */
	public int updateUserAllLowerLevelsByName(User user) {
		return userMapper.updateUserAllLowerLevelsByName(user);
	};

	/**
	 * 查找符合条件的团队注册人数
	 * 
	 * @param query
	 * @return
	 */
	public Integer getTeamUserRegistersByQueryCount(TeamUserQuery query) {
		return userMapper.getTeamUserRegistersByQueryCount(query);
	}

	/**
	 * 查找符合条件的团队首冲人数
	 * 
	 * @param query
	 * @return
	 */
	public Integer getTeamUserfirstrechargeTimeByQueryCount(TeamUserQuery query) {
		return userMapper.getTeamUserfirstrechargeTimeByQueryCount(query);
	}

	/**
	 * 统计用户的有效
	 * 
	 * @param query
	 * @return
	 */
	public Integer getTeamUserLotteryByQueryCount(OrderQuery query) {
		return userMapper.getTeamUserLotteryByQueryCount(query);
	}

	/**
	 * 查询手机号是否唯一
	 * 
	 * @param query
	 * @return
	 */
	public Integer getAllUserPhoneByPhone(User query) {
		return userMapper.getAllUserPhoneByPhone(query);
	}

	/**
	 * 根据ID修改用户资料
	 * 
	 * @param user
	 * @return
	 */
	public Integer udateUserPhoneByid(User user) {
		return userMapper.updateByPrimaryKeySelective(user);
	}

	/**
	 * 根据条件查找用户列表
	 * 
	 * @param query
	 * @return
	 */
	public List<User> getUsersByQuery(UserQuery query) {
		return userMapper.getUsersByQuery(query);
	}
	
	/**
	 * 批量查询
	 * @param currentSystem
	 * @param username
	 * @return
	 */
	public List<User> selectAllByUserNames(String currentSystem, List<String> username) {
		return userMapper.selectAllByUserNames(currentSystem, username);
	}
}
