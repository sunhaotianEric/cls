package com.team.lottery.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.enums.EMoneyDetailType;
import com.team.lottery.enums.EUserDailLiLevel;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.UserDayConsumeQuery;
import com.team.lottery.extvo.UserQuery;
import com.team.lottery.mapper.usermoneytotal.UserMoneyTotalMapper;
import com.team.lottery.system.UserVersionExceptionConsumeDealThread;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserDayConsume;
import com.team.lottery.vo.UserMoneyTotal;
import com.team.lottery.vo.UserSelfDayConsume;
@Service("userMoneyTotalService")
public class UserMoneyTotalService {
	private static Logger logger = LoggerFactory.getLogger(UserMoneyTotalService.class);
	@Autowired
	private UserMoneyTotalMapper userMoneyTotalMapper;
	@Autowired
	private UserDayConsumeService userDayConsumeService;
	@Autowired
	private UserSelfDayConsumeService userSelfDayConsumeService;
	@Autowired
	private UserService userService;
	public int deleteByPrimaryKey(Long id) {
		return userMoneyTotalMapper.deleteByPrimaryKey(id);
	}
	
	public int insertSelective(UserMoneyTotal record) {
		return userMoneyTotalMapper.insertSelective(record);
	}
	
	public UserMoneyTotal selectByPrimaryKey(Long id) {
		return userMoneyTotalMapper.selectByPrimaryKey(id);
	}
	
	
	/**
	 * 查找所有的用户数据
	 * @return
	 */
	public Page getAllUserMoneyTotalByQueryPage(UserMoneyTotal query,Page page){
		page.setTotalRecNum(userMoneyTotalMapper.getAllUserMoneyTotalByQueryPageCount(query));
		page.setPageContent(userMoneyTotalMapper.getAllUserMoneyTotalByQueryPage(query,page.getStartIndex(),page.getPageSize()));
		return page;
	}
	
	/**
	 * 根据查询条件 统计用户的所有余额、分红、盈利等数据
	 * @return
	 */
	public UserMoneyTotal getTotalUsersConsumeReport(UserMoneyTotal query){
		return userMoneyTotalMapper.getTotalUsersConsumeReport(query);
	}
	
	 /**
     * 根据userid 查询 
     * @param userId
     * @return
     */
    public UserMoneyTotal selectByUserId(Integer userId){
    	return userMoneyTotalMapper.selectByUserId(userId);
    }
	
    /**
     * 根据userid和bizsystem等 查询 
     * @param record
     * @return
     */
    public UserMoneyTotal selectByQuery(UserMoneyTotal record){
    	return userMoneyTotalMapper.selectByQuery(record);
    }
    
	
	 /**
     * 更新用户 版本号+1
     * @param record
     * @return
	 * @throws UnEqualVersionException 
     */
    public int updateByPrimaryKeyWithVersionSelective(UserMoneyTotal record) throws UnEqualVersionException
    {
    	
		int res= userMoneyTotalMapper.updateByPrimaryKeyWithVersionSelective(record);
		if(res == 0) {
			throw new UnEqualVersionException("更新kr_user_money_total记录id["+record.getId()+"]时发现版本号不一致");
		}
		return res;
    }
    
   
    /**
     * 按类型增加用户总金额
     * @param user
     * @param detailType   资金明细类型
     * @param operationValue  操作金额
     * @return
     * @throws Exception
     */
	public int addUserTotalMoneyByType(User user, EMoneyDetailType detailType, BigDecimal operationValue) throws Exception {
	UserMoneyTotal userMoneyTotal = new UserMoneyTotal();
		//判断是否游客
		Integer enabled =  user.getIsTourist() == 1?0:1;
		if(enabled.intValue() == 0){
			return 0;
		}
		userMoneyTotal.setEnabled(enabled);
    	userMoneyTotal.setUserId(user.getId());// 用户id
		userMoneyTotal.setBizSystem(user.getBizSystem());
		userMoneyTotal.setUserName(user.getUserName());
		userMoneyTotal = userMoneyTotalMapper.selectByQuery(userMoneyTotal);
		if (userMoneyTotal == null) {
			userMoneyTotal = new UserMoneyTotal();
			userMoneyTotal.setUserId(user.getId());
			userMoneyTotal.setBizSystem(user.getBizSystem());
			userMoneyTotal.setUserName(user.getUserName());
			userMoneyTotalMapper.insertSelective(userMoneyTotal);
			userMoneyTotal = userMoneyTotalMapper.selectByQuery(userMoneyTotal);
		}
		if (EMoneyDetailType.LOTTERY.getCode().equals(detailType.getCode())) {
			userMoneyTotal.setTotalLottery(userMoneyTotal.getTotalLottery().add(operationValue));
		} else if (EMoneyDetailType.WIN.getCode().equals(detailType.getCode())) {
			userMoneyTotal.setTotalWin(userMoneyTotal.getTotalWin().add(operationValue));
		} else if (EMoneyDetailType.LOTTERY_STOP_AFTER_NUMBER.getCode().equals(detailType.getCode())) {
			userMoneyTotal.setTotalLotteryStopAfterNumber(userMoneyTotal.getTotalLotteryStopAfterNumber().add(operationValue));
		} else if (EMoneyDetailType.LOTTERY_REGRESSION.getCode().equals(detailType.getCode())) {
			userMoneyTotal.setTotalLotteryRegression(userMoneyTotal.getTotalLotteryRegression().add(operationValue));
		} else if (EMoneyDetailType.PERCENTAGE.getCode().equals(detailType.getCode())) {
			userMoneyTotal.setTotalPercentage(userMoneyTotal.getTotalPercentage().add(operationValue));
		} else if (EMoneyDetailType.REBATE.getCode().equals(detailType.getCode())) {
			userMoneyTotal.setTotalRebate(userMoneyTotal.getTotalRebate().add(operationValue));
		} else if (EMoneyDetailType.RECHARGE.getCode().equals(detailType.getCode())) {
			userMoneyTotal.setTotalRecharge(userMoneyTotal.getTotalRecharge().add(operationValue));
		} else if (EMoneyDetailType.RECHARGE_PRESENT.getCode().equals(detailType.getCode())) {
			userMoneyTotal.setTotalRechargePresent(userMoneyTotal.getTotalRechargePresent().add(operationValue));
		} else if (EMoneyDetailType.ACTIVITIES_MONEY.getCode().equals(detailType.getCode())) {
			userMoneyTotal.setTotalActivitiesMoney(userMoneyTotal.getTotalActivitiesMoney().add(operationValue));
		} else if (EMoneyDetailType.SYSTEM_ADD_MONEY.getCode().equals(detailType.getCode())) {
			userMoneyTotal.setTotalSystemAddMoney(userMoneyTotal.getTotalSystemAddMoney().add(operationValue));
		} else if (EMoneyDetailType.SYSTEM_REDUCE_MONEY.getCode().equals(detailType.getCode())) {
			userMoneyTotal.setTotalSystemReduceMoney(userMoneyTotal.getTotalSystemReduceMoney().add(operationValue));
		} else if (EMoneyDetailType.WITHDRAW.getCode().equals(detailType.getCode())) {
			userMoneyTotal.setTotalWithdraw(userMoneyTotal.getTotalWithdraw().add(operationValue));
		} else if (EMoneyDetailType.WITHDRAW_FEE.getCode().equals(detailType.getCode())) {
			userMoneyTotal.setTotalWithdarwFee(userMoneyTotal.getTotalWithdarwFee().add(operationValue));
		} else if (EMoneyDetailType.PAY_TRANFER.getCode().equals(detailType.getCode())) {
			userMoneyTotal.setTotalPayTranfer(userMoneyTotal.getTotalPayTranfer().add(operationValue));
		} else if (EMoneyDetailType.INCOME_TRANFER.getCode().equals(detailType.getCode())) {
			userMoneyTotal.setTotalIncomeTranfer(userMoneyTotal.getTotalIncomeTranfer().add(operationValue));
		} else if (EMoneyDetailType.HALF_MONTH_BOUNS.getCode().equals(detailType.getCode())) {
			userMoneyTotal.setTotalHalfMonthBonus(userMoneyTotal.getTotalHalfMonthBonus().add(operationValue));
		} else if (EMoneyDetailType.DAY_SALARY.getCode().equals(detailType.getCode())) {
			userMoneyTotal.setTotalDaySalary(userMoneyTotal.getTotalDaySalary().add(operationValue));
		} else if (EMoneyDetailType.DAY_BOUNS.getCode().equals(detailType.getCode())) {
			userMoneyTotal.setTotalDayBonus(userMoneyTotal.getTotalDayBonus().add(operationValue));
		} else if (EMoneyDetailType.MONTH_SALARY.getCode().equals(detailType.getCode())) {
			userMoneyTotal.setTotalMonthSalary(userMoneyTotal.getTotalMonthSalary().add(operationValue));
		} else if (EMoneyDetailType.DAY_COMMISSION.getCode().equals(detailType.getCode())) {
			userMoneyTotal.setTotalDayCommission(userMoneyTotal.getTotalDayCommission().add(operationValue));
		}else if (EMoneyDetailType.MANAGE_REDUCE_MONEY.getCode().equals(detailType.getCode())) {
			userMoneyTotal.setTotalManageReduceMoney(userMoneyTotal.getTotalManageReduceMoney().add(operationValue));
		}else if (EMoneyDetailType.RECHARGE_MANUAL.getCode().equals(detailType.getCode())) {
			userMoneyTotal.setTotalManualRecharge(userMoneyTotal.getTotalManualRecharge().add(operationValue));
		} else {
			throw new Exception("不可知的操作类型");
		}
		Long startTime = System.currentTimeMillis();
		int result = userMoneyTotalMapper.updateByPrimaryKeyWithVersionSelective(userMoneyTotal);
		if(enabled.intValue() == 1){
			//统计日实时团队盈亏报表
	        this.excuteUserDayConsume(user, detailType.name(), operationValue);	
			//统计自身实时盈亏报表
	        this.excuteUserSelfDayConsume(user, detailType.name(), operationValue);
		}
        Long endTime = System.currentTimeMillis();
        logger.debug("处理团队和自身实时盈亏报表 业务系统{"+user.getBizSystem()+"}["+user.getUserName()+"],耗时["+(endTime - startTime)+"]ms");
		return result;

	}

	
	
   	/**
   	 * 统计日实时团队盈亏报表
   	 * @return
   	 * @throws UnEqualVersionException 
   	 */
	public void excuteUserDayConsume(User user, String detailType, BigDecimal operationValue) {
		// 判断是否游客
		Integer enabled = user.getIsTourist() == 1 ? 0 : 1;
		if (enabled.intValue() == 0) {
			return;
		}

		List<UserDayConsume> list = new ArrayList<UserDayConsume>();
		String regfrom = "";
		regfrom = user.getRegfrom();
		// 过滤掉超级代理
		if (regfrom != null && !"".equals(regfrom)) {
			regfrom = regfrom.substring(regfrom.indexOf("&") + 1);
		}

		String reform = regfrom + user.getUserName() + "&";
		String[] refromArr = reform.split("&");
		Date nowDate = new Date();
		Date nowDateStart = new Date();// 三点
		nowDateStart = DateUtil.getNowStartTimeByStart(nowDateStart);
		nowDateStart = DateUtil.addHoursToDate(nowDateStart, ConstantUtil.DAY_COMSUME_END_HOUR);
		nowDateStart = DateUtil.addMinutesToDate(nowDateStart, ConstantUtil.DAY_COMSUME_END_MINUTE);

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		// 0点到3点属于昨天
		if (nowDate.before(nowDateStart)) {
			calendar.add(Calendar.DAY_OF_MONTH, -1);
		}
		// 归属时间置为十二点
		calendar.set(Calendar.HOUR_OF_DAY, 12);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		Date belongDateTime = calendar.getTime();
		// 所有上级和他自己累加
		for (String userName : refromArr) {
			UserDayConsumeQuery query = new UserDayConsumeQuery();
			query.setUserName(userName);
			query.setBizSystem(user.getBizSystem());
			query.setBelongDate(belongDateTime);
			UserDayConsume reportVo = new UserDayConsume();
			list = userDayConsumeService.getUserDayConsumeByCondition(query);
			if (list.size() > 0) {
				UserDayConsume userDayConsume = list.get(0);
				reportVo.setId(userDayConsume.getId());
				reportVo.setVersion(userDayConsume.getVersion());
				if (detailType.equals(EMoneyDetailType.RECHARGE.name())) { // 存款(充值)
					reportVo.setRecharge(userDayConsume.getRecharge().add(operationValue));
					// 不能参与
				} else if (detailType.equals(EMoneyDetailType.RECHARGE_MANUAL.name())) { // 人工存款 属于充值
					reportVo.setRecharge(userDayConsume.getRecharge().add(operationValue));

					// 不能参与
				} else if (detailType.equals(EMoneyDetailType.RECHARGE_PRESENT.name())) { // 充值赠送 充值赠送(系统扣费)
					reportVo.setRechargepresent(userDayConsume.getRechargepresent().add(operationValue));

					// 盈利计算
					reportVo.setGain(userDayConsume.getGain().add(operationValue));
				} else if (detailType.equals(EMoneyDetailType.ACTIVITIES_MONEY.name())) { // 活动彩金//活动彩金(系统扣费)
					reportVo.setActivitiesmoney(userDayConsume.getActivitiesmoney().add(operationValue));
					// 盈利计算
					reportVo.setGain(userDayConsume.getGain().add(operationValue));
				} else if (detailType.equals(EMoneyDetailType.WITHDRAW.name())) { // 取款(在线取款)
					reportVo.setWithdraw(userDayConsume.getWithdraw().add(operationValue));
					// 不能参与
				} else if (detailType.equals(EMoneyDetailType.WITHDRAW_FEE.name())) { // 取款手续费
					reportVo.setWithdrawfee(userDayConsume.getWithdrawfee().add(operationValue));

					reportVo.setWithdraw(userDayConsume.getWithdraw().subtract(operationValue));

					reportVo.setGain(userDayConsume.getGain().subtract(operationValue));
				} else if (detailType.equals(EMoneyDetailType.LOTTERY.name())) { // 投注
					reportVo.setLottery(userDayConsume.getLottery().add(operationValue));
					//这里有效投注额暂时先做总投注额统计
					reportVo.setLotteryEffective(userDayConsume.getLotteryEffective().add(operationValue));
					// 盈利计算
					reportVo.setGain(userDayConsume.getGain().subtract(operationValue));
				} else if (detailType.equals(EMoneyDetailType.LOTTERY_STOP_AFTER_NUMBER.name())) { // 撤单
					reportVo.setLottery(userDayConsume.getLottery().subtract(operationValue));
					reportVo.setLotteryStopAfterNumber(userDayConsume.getLotteryStopAfterNumber().add(operationValue));
					// 盈利计算
					reportVo.setGain(userDayConsume.getGain().add(operationValue));
				} else if (detailType.equals(EMoneyDetailType.LOTTERY_REGRESSION.name())) { // 管理员投注退单
					reportVo.setLottery(userDayConsume.getLottery().subtract(operationValue));
					reportVo.setLotteryRegression(userDayConsume.getLotteryRegression().add(operationValue));
					// 盈利计算
					reportVo.setGain(userDayConsume.getGain().add(operationValue));
				} else if (detailType.equals(EMoneyDetailType.WIN.name())) { // 中奖
					reportVo.setWin(userDayConsume.getWin().add(operationValue));
					// 盈利计算
					reportVo.setGain(userDayConsume.getGain().add(operationValue));
				} else if (detailType.equals(EMoneyDetailType.REBATE.name())) { // 返点
					reportVo.setRebate(userDayConsume.getRebate().add(operationValue));
					// 盈利计算
					reportVo.setGain(userDayConsume.getGain().add(operationValue));
				} else if (detailType.equals(EMoneyDetailType.PERCENTAGE.name())) { // 提成
					reportVo.setPercentage(userDayConsume.getPercentage().add(operationValue));
					// 盈利计算
					reportVo.setGain(userDayConsume.getGain().add(operationValue));
				} else if (detailType.equals(EMoneyDetailType.HALF_MONTH_BOUNS.name())) { // 半月分红
					reportVo.setHalfmonthbonus(userDayConsume.getHalfmonthbonus().add(operationValue));
				} else if (detailType.equals(EMoneyDetailType.DAY_BOUNS.name())) { // 日分红
					reportVo.setDaybonus(userDayConsume.getDaybonus().add(operationValue));
				} else if (detailType.equals(EMoneyDetailType.DAY_SALARY.name())) { // 日工资
					reportVo.setDaysalary(userDayConsume.getDaysalary().add(operationValue));
				} else if (detailType.equals(EMoneyDetailType.MONTH_SALARY.name())) { // 月工资
					reportVo.setMonthsalary(userDayConsume.getMonthsalary().add(operationValue));
				} else if (detailType.equals(EMoneyDetailType.DAY_COMMISSION.name())) { // 日佣金
					reportVo.setDaycommission(userDayConsume.getDaycommission().add(operationValue));
				} else if (detailType.equals(EMoneyDetailType.PAY_TRANFER.name())) { // 支出转账
					reportVo.setPaytranfer(userDayConsume.getPaytranfer().add(operationValue));
					// 不能参与
					// reportVo.setGain(reportVo.getGain().subtract(moneyDetail.getPay()));
				} else if (detailType.equals(EMoneyDetailType.INCOME_TRANFER.name())) { // 收入转账
					reportVo.setIncometranfer(userDayConsume.getIncometranfer().add(operationValue));
					// 不能参与
					// reportVo.setGain(reportVo.getGain().add(moneyDetail.getIncome()));
				} else if (detailType.equals(EMoneyDetailType.SYSTEM_ADD_MONEY.name())) { // 人工存入
					reportVo.setSystemaddmoney(userDayConsume.getSystemaddmoney().add(operationValue));
					// 盈利计算
					reportVo.setGain(userDayConsume.getGain().add(operationValue));
				} else if (detailType.equals(EMoneyDetailType.SYSTEM_REDUCE_MONEY.name())) { // 误存提出 不计入报表盈利
					reportVo.setSystemreducemoney(userDayConsume.getSystemreducemoney().add(operationValue));
					// 盈利计算
					// reportVo.setGain(userDayConsume.getGain().subtract(operationValue));
				} else if (detailType.equals(EMoneyDetailType.MANAGE_REDUCE_MONEY.name())) { // 行政提出
					reportVo.setManagereducemoney(userDayConsume.getManagereducemoney().add(operationValue));
					// 盈利计算
					reportVo.setGain(userDayConsume.getGain().subtract(operationValue));
				} else if (detailType.equals(ConstantUtil.LOTTERY_EFFECTIVE)) { // 有效投注额，特别处理
					reportVo.setLotteryEffective(userDayConsume.getLotteryEffective().add(operationValue));
				} else {
					throw new RuntimeException("该资金明细类型不可知.");
				}
				try {
					userDayConsumeService.updateByPrimaryKeyWithVersionSelective(reportVo);
				} catch (Exception e) {
					logger.error("更新日实时团队盈亏报表,业务系统[" + user.getBizSystem() + "],用户[" + userName + "]时，类型=" + detailType
							+ ",金额=" + operationValue + "发现版本号不一致,进入新线程处理！");
					UserVersionExceptionConsumeDealThread thread = new UserVersionExceptionConsumeDealThread(
							user.getBizSystem(), userName, belongDateTime, detailType, operationValue,
							UserVersionExceptionConsumeDealThread.dayConsumeflag);
					thread.start();

				}

				// if(result == 0){
				// logger.error("更新日实时团队盈亏报表
				// id["+reportVo.getId()+"]时，类型="+detailType+",金额="+operationValue+"发现版本号不一致,更新失败！");
				// }

			} else {
				User u = userService.getUserByName(user.getBizSystem(), userName);
				if (StringUtils.isEmpty(u.getRegfrom())
						&& EUserDailLiLevel.SUPERAGENCY.name().equals(u.getDailiLevel())) {
					continue;
				}
				reportVo.setUserId(u.getId());
				reportVo.setUserName(userName);
				reportVo.setBizSystem(user.getBizSystem());
				reportVo.setBelongDate(belongDateTime);
				reportVo.setRegfrom(u.getRegfrom());
				reportVo.setVersion(1l);
				reportVo.setCreateDate(new Date());
				if (detailType.equals(EMoneyDetailType.RECHARGE.name())) { // 存款(充值)
					reportVo.setRecharge(operationValue);
					// 不能参与
				} else if (detailType.equals(EMoneyDetailType.RECHARGE_MANUAL.name())) { // 人工存款 属于充值
					reportVo.setRecharge(operationValue);

					// 不能参与
				} else if (detailType.equals(EMoneyDetailType.RECHARGE_PRESENT.name())) { // 充值赠送 充值赠送(系统扣费)
					reportVo.setRechargepresent(operationValue);

					// 盈利计算
					reportVo.setGain(operationValue);
				} else if (detailType.equals(EMoneyDetailType.ACTIVITIES_MONEY.name())) { // 活动彩金//活动彩金(系统扣费)
					reportVo.setActivitiesmoney(operationValue);
					// 盈利计算
					reportVo.setGain(operationValue);
				} else if (detailType.equals(EMoneyDetailType.WITHDRAW.name())) { // 取款(在线取款)
					reportVo.setWithdraw(operationValue);
					// 不能参与
				} else if (detailType.equals(EMoneyDetailType.WITHDRAW_FEE.name())) { // 取款手续费
					reportVo.setWithdrawfee(operationValue);

					reportVo.setWithdraw(BigDecimal.ZERO.subtract(operationValue));

					reportVo.setGain(BigDecimal.ZERO.subtract(operationValue));
				} else if (detailType.equals(EMoneyDetailType.LOTTERY.name())) { // 投注
					reportVo.setLottery(operationValue);
					//这里有效投注额暂时先做总投注额统计
					reportVo.setLotteryEffective(operationValue);
					// 盈利计算
					reportVo.setGain(BigDecimal.ZERO.subtract(operationValue));
				} else if (detailType.equals(EMoneyDetailType.LOTTERY_STOP_AFTER_NUMBER.name())) { // 撤单
					reportVo.setLottery(BigDecimal.ZERO.subtract(operationValue));
					reportVo.setLotteryStopAfterNumber(operationValue);
					// 盈利计算
					reportVo.setGain(operationValue);
				} else if (detailType.equals(EMoneyDetailType.LOTTERY_REGRESSION.name())) { // 管理员投注退单
					reportVo.setLottery(BigDecimal.ZERO.subtract(operationValue));
					reportVo.setLotteryRegression(operationValue);
					// 盈利计算
					reportVo.setGain(operationValue);
				} else if (detailType.equals(EMoneyDetailType.WIN.name())) { // 中奖
					reportVo.setWin(operationValue);
					// 盈利计算
					reportVo.setGain(operationValue);
				} else if (detailType.equals(EMoneyDetailType.REBATE.name())) { // 返点
					reportVo.setRebate(operationValue);
					// 盈利计算
					reportVo.setGain(operationValue);
				} else if (detailType.equals(EMoneyDetailType.PERCENTAGE.name())) { // 提成
					reportVo.setPercentage(operationValue);
					// 盈利计算
					reportVo.setGain(operationValue);
				} else if (detailType.equals(EMoneyDetailType.HALF_MONTH_BOUNS.name())) { // 半月分红
					reportVo.setHalfmonthbonus(operationValue);
				} else if (detailType.equals(EMoneyDetailType.DAY_BOUNS.name())) { // 日分红
					reportVo.setDaybonus(operationValue);
				} else if (detailType.equals(EMoneyDetailType.DAY_SALARY.name())) { // 日工资
					reportVo.setDaysalary(operationValue);
				} else if (detailType.equals(EMoneyDetailType.MONTH_SALARY.name())) { // 月工资
					reportVo.setMonthsalary(operationValue);
				} else if (detailType.equals(EMoneyDetailType.DAY_COMMISSION.name())) { // 日佣金
					reportVo.setDaycommission(operationValue);
				} else if (detailType.equals(EMoneyDetailType.PAY_TRANFER.name())) { // 支出转账
					reportVo.setPaytranfer(operationValue);
					// 不能参与
					// reportVo.setGain(reportVo.getGain().subtract(moneyDetail.getPay()));
				} else if (detailType.equals(EMoneyDetailType.INCOME_TRANFER.name())) { // 收入转账
					reportVo.setIncometranfer(operationValue);
					// 不能参与
					// reportVo.setGain(reportVo.getGain().add(moneyDetail.getIncome()));
				} else if (detailType.equals(EMoneyDetailType.SYSTEM_ADD_MONEY.name())) { // 人工存入
					reportVo.setSystemaddmoney(operationValue);
					// 盈利计算
					reportVo.setGain(operationValue);
				} else if (detailType.equals(EMoneyDetailType.SYSTEM_REDUCE_MONEY.name())) { // 误存提出
					reportVo.setSystemreducemoney(operationValue);
					// 盈利计算
					// reportVo.setGain(BigDecimal.ZERO.subtract(operationValue));
				} else if (detailType.equals(EMoneyDetailType.MANAGE_REDUCE_MONEY.name())) { // 行政提出
					reportVo.setManagereducemoney(operationValue);
					// 盈利计算
					reportVo.setGain(BigDecimal.ZERO.subtract(operationValue));
				} else if (detailType.equals(ConstantUtil.LOTTERY_EFFECTIVE)) { // 有效投注额，特别处理
					reportVo.setLotteryEffective(operationValue);
				} else {
					throw new RuntimeException("该资金明细类型不可知.");
				}
				userDayConsumeService.insertSelective(reportVo);
			}
		}
	}
	
   	/**
   	 * 统计日实时自身盈亏报表
   	 * @return
   	 * @throws UnEqualVersionException 
   	 */
	public void excuteUserSelfDayConsume(User user, String detailType, BigDecimal operationValue) {
		// 判断是否游客
		Integer enabled = user.getIsTourist() == 1 ? 0 : 1;
		if (enabled.intValue() == 0) {
			return;
		}

		if (StringUtils.isEmpty(user.getRegfrom())
				&& EUserDailLiLevel.SUPERAGENCY.name().equals(user.getDailiLevel())) {
			return;
		}
		Date nowDate = new Date();
		Date nowDateStart = new Date();// 三点
		nowDateStart = DateUtil.getNowStartTimeByStart(nowDateStart);
		nowDateStart = DateUtil.addHoursToDate(nowDateStart, ConstantUtil.DAY_COMSUME_END_HOUR);
		nowDateStart = DateUtil.addMinutesToDate(nowDateStart, ConstantUtil.DAY_COMSUME_END_MINUTE);

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		// 0点到3点属于昨天
		if (nowDate.before(nowDateStart)) {
			calendar.add(Calendar.DAY_OF_MONTH, -1);
		}
		// 归属时间置为十二点
		calendar.set(Calendar.HOUR_OF_DAY, 12);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		Date belongDateTime = calendar.getTime();

		UserDayConsumeQuery query = new UserDayConsumeQuery();
		query.setUserName(user.getUserName());
		query.setBizSystem(user.getBizSystem());
		query.setBelongDate(belongDateTime);
		UserSelfDayConsume reportVo = new UserSelfDayConsume();
		UserSelfDayConsume userSelfDayConsume = userSelfDayConsumeService.getUserSelfDayConsumeByCondition(query);
		if (userSelfDayConsume != null) {
			reportVo.setId(userSelfDayConsume.getId());
			reportVo.setVersion(userSelfDayConsume.getVersion());
			if (detailType.equals(EMoneyDetailType.RECHARGE.name())) { // 存款(充值)
				reportVo.setRecharge(userSelfDayConsume.getRecharge().add(operationValue));
				// 不能参与
			} else if (detailType.equals(EMoneyDetailType.RECHARGE_MANUAL.name())) { // 人工存款(充值)
				reportVo.setRecharge(userSelfDayConsume.getRecharge().add(operationValue));
				// 不能参与
			} else if (detailType.equals(EMoneyDetailType.RECHARGE_PRESENT.name())) { // 充值赠送 充值赠送(系统扣费)
				reportVo.setRechargepresent(userSelfDayConsume.getRechargepresent().add(operationValue));

				// 盈利计算
				reportVo.setGain(userSelfDayConsume.getGain().add(operationValue));
			} else if (detailType.equals(EMoneyDetailType.ACTIVITIES_MONEY.name())) { // 活动彩金//活动彩金(系统扣费)
				reportVo.setActivitiesmoney(userSelfDayConsume.getActivitiesmoney().add(operationValue));
				// 盈利计算
				reportVo.setGain(userSelfDayConsume.getGain().add(operationValue));
			} else if (detailType.equals(EMoneyDetailType.WITHDRAW.name())) { // 取款(在线取款)
				reportVo.setWithdraw(userSelfDayConsume.getWithdraw().add(operationValue));
				// 不能参与
			} else if (detailType.equals(EMoneyDetailType.WITHDRAW_FEE.name())) { // 取款手续费
				reportVo.setWithdrawfee(userSelfDayConsume.getWithdrawfee().add(operationValue));

				reportVo.setWithdraw(userSelfDayConsume.getWithdraw().subtract(operationValue));

				reportVo.setGain(userSelfDayConsume.getGain().subtract(operationValue));
			} else if (detailType.equals(EMoneyDetailType.LOTTERY.name())) { // 投注
				reportVo.setLottery(userSelfDayConsume.getLottery().add(operationValue));
				//这里有效投注额暂时先做总投注额统计
				reportVo.setLotteryEffective(userSelfDayConsume.getLotteryEffective().add(operationValue));
				// 盈利计算
				reportVo.setGain(userSelfDayConsume.getGain().subtract(operationValue));
			} else if (detailType.equals(EMoneyDetailType.LOTTERY_STOP_AFTER_NUMBER.name())) { // 撤单
				reportVo.setLottery(userSelfDayConsume.getLottery().subtract(operationValue));
				reportVo.setLotteryStopAfterNumber(userSelfDayConsume.getLotteryStopAfterNumber().add(operationValue));
				// 盈利计算
				reportVo.setGain(userSelfDayConsume.getGain().add(operationValue));
			} else if (detailType.equals(EMoneyDetailType.LOTTERY_REGRESSION.name())) { // 管理员投注退单
				reportVo.setLottery(userSelfDayConsume.getLottery().subtract(operationValue));
				reportVo.setLotteryRegression(userSelfDayConsume.getLotteryRegression().add(operationValue));
				// 盈利计算
				reportVo.setGain(userSelfDayConsume.getGain().add(operationValue));
			} else if (detailType.equals(EMoneyDetailType.WIN.name())) { // 中奖
				reportVo.setWin(userSelfDayConsume.getWin().add(operationValue));
				// 盈利计算
				reportVo.setGain(userSelfDayConsume.getGain().add(operationValue));
			} else if (detailType.equals(EMoneyDetailType.REBATE.name())) { // 返点
				reportVo.setRebate(userSelfDayConsume.getRebate().add(operationValue));
				// 盈利计算
				reportVo.setGain(userSelfDayConsume.getGain().add(operationValue));
			} else if (detailType.equals(EMoneyDetailType.PERCENTAGE.name())) { // 提成
				reportVo.setPercentage(userSelfDayConsume.getPercentage().add(operationValue));
				// 盈利计算
				reportVo.setGain(userSelfDayConsume.getGain().add(operationValue));
			} else if (detailType.equals(EMoneyDetailType.HALF_MONTH_BOUNS.name())) { // 半月分红
				reportVo.setHalfmonthbonus(userSelfDayConsume.getHalfmonthbonus().add(operationValue));
			} else if (detailType.equals(EMoneyDetailType.DAY_BOUNS.name())) { // 日分红
				reportVo.setDaybonus(userSelfDayConsume.getDaybonus().add(operationValue));
			} else if (detailType.equals(EMoneyDetailType.DAY_SALARY.name())) { // 日工资
				reportVo.setDaysalary(userSelfDayConsume.getDaysalary().add(operationValue));
			} else if (detailType.equals(EMoneyDetailType.MONTH_SALARY.name())) { // 月工资
				reportVo.setMonthsalary(userSelfDayConsume.getMonthsalary().add(operationValue));
			} else if (detailType.equals(EMoneyDetailType.DAY_COMMISSION.name())) { // 日佣金
				reportVo.setDaycommission(userSelfDayConsume.getDaycommission().add(operationValue));
			} else if (detailType.equals(EMoneyDetailType.PAY_TRANFER.name())) { // 支出转账
				reportVo.setPaytranfer(userSelfDayConsume.getPaytranfer().add(operationValue));
				// 不能参与
				// reportVo.setGain(reportVo.getGain().subtract(moneyDetail.getPay()));
			} else if (detailType.equals(EMoneyDetailType.INCOME_TRANFER.name())) { // 收入转账
				reportVo.setIncometranfer(userSelfDayConsume.getIncometranfer().add(operationValue));
				// 不能参与
				// reportVo.setGain(reportVo.getGain().add(moneyDetail.getIncome()));
			} else if (detailType.equals(EMoneyDetailType.SYSTEM_ADD_MONEY.name())) { // 人工存入
				reportVo.setSystemaddmoney(userSelfDayConsume.getSystemaddmoney().add(operationValue));
				// 盈利计算
				reportVo.setGain(userSelfDayConsume.getGain().add(operationValue));
			} else if (detailType.equals(EMoneyDetailType.SYSTEM_REDUCE_MONEY.name())) { // 误存提出
				reportVo.setSystemreducemoney(userSelfDayConsume.getSystemreducemoney().add(operationValue));
				// 盈利计算
				// reportVo.setGain(userSelfDayConsume.getGain().subtract(operationValue));
			} else if (detailType.equals(EMoneyDetailType.MANAGE_REDUCE_MONEY.name())) { // 行政提出
				reportVo.setManagereducemoney(userSelfDayConsume.getManagereducemoney().add(operationValue));
				// 盈利计算
				reportVo.setGain(userSelfDayConsume.getGain().subtract(operationValue));
			} else if (detailType.equals(ConstantUtil.LOTTERY_EFFECTIVE)) { // 有效投注额，特别处理
				reportVo.setLotteryEffective(userSelfDayConsume.getLotteryEffective().add(operationValue));
			} else {
				throw new RuntimeException("该资金明细类型不可知.");
			}

			try {
				userSelfDayConsumeService.updateByPrimaryKeyWithVersionSelective(reportVo);
			} catch (UnEqualVersionException e) {
				logger.error("更新日实时自身盈亏报表,业务系统[" + user.getBizSystem() + "],用户[" + user.getUserName() + "]时，类型="
						+ detailType + ",金额=" + operationValue + "发现版本号不一致,进入新线程处理！");
				UserVersionExceptionConsumeDealThread thread = new UserVersionExceptionConsumeDealThread(
						user.getBizSystem(), userSelfDayConsume.getUserName(), belongDateTime, detailType,
						operationValue, UserVersionExceptionConsumeDealThread.selfDayConsumeflag);
				thread.start();
			}

		} else {

			reportVo.setUserId(user.getId());
			reportVo.setUserName(user.getUserName());
			reportVo.setBizSystem(user.getBizSystem());
			reportVo.setBelongDate(belongDateTime);
			reportVo.setRegfrom(user.getRegfrom());
			reportVo.setVersion(1l);
			reportVo.setCreateDate(new Date());
			if (detailType.equals(EMoneyDetailType.RECHARGE.name())) { // 存款(充值)
				reportVo.setRecharge(operationValue);
				// 不能参与
			} else if (detailType.equals(EMoneyDetailType.RECHARGE_MANUAL.name())) { // 人工存款(充值)
				reportVo.setRecharge(operationValue);
				// 不能参与
			} else if (detailType.equals(EMoneyDetailType.RECHARGE_PRESENT.name())) { // 充值赠送 充值赠送(系统扣费)
				reportVo.setRechargepresent(operationValue);

				// 盈利计算
				reportVo.setGain(operationValue);
			} else if (detailType.equals(EMoneyDetailType.ACTIVITIES_MONEY.name())) { // 活动彩金//活动彩金(系统扣费)
				reportVo.setActivitiesmoney(operationValue);
				// 盈利计算
				reportVo.setGain(operationValue);
			} else if (detailType.equals(EMoneyDetailType.WITHDRAW.name())) { // 取款(在线取款)
				reportVo.setWithdraw(operationValue);
				// 不能参与
			} else if (detailType.equals(EMoneyDetailType.WITHDRAW_FEE.name())) { // 取款手续费
				reportVo.setWithdrawfee(operationValue);

				reportVo.setWithdraw(BigDecimal.ZERO.subtract(operationValue));

				reportVo.setGain(BigDecimal.ZERO.subtract(operationValue));
			} else if (detailType.equals(EMoneyDetailType.LOTTERY.name())) { // 投注
				reportVo.setLottery(operationValue);
				//这里有效投注额暂时先做总投注额统计
				reportVo.setLotteryEffective(operationValue);
				// 盈利计算
				reportVo.setGain(BigDecimal.ZERO.subtract(operationValue));
			} else if (detailType.equals(EMoneyDetailType.LOTTERY_STOP_AFTER_NUMBER.name())) { // 撤单
				reportVo.setLottery(BigDecimal.ZERO.subtract(operationValue));
				reportVo.setLotteryStopAfterNumber(BigDecimal.ZERO.add(operationValue));
				// 盈利计算
				reportVo.setGain(operationValue);
			} else if (detailType.equals(EMoneyDetailType.LOTTERY_REGRESSION.name())) { // 管理员投注退单
				reportVo.setLottery(BigDecimal.ZERO.subtract(operationValue));
				reportVo.setLotteryRegression(BigDecimal.ZERO.add(operationValue));
				// 盈利计算
				reportVo.setGain(operationValue);
			} else if (detailType.equals(EMoneyDetailType.WIN.name())) { // 中奖
				reportVo.setWin(operationValue);
				// 盈利计算
				reportVo.setGain(operationValue);
			} else if (detailType.equals(EMoneyDetailType.REBATE.name())) { // 返点
				reportVo.setRebate(operationValue);
				// 盈利计算
				reportVo.setGain(operationValue);
			} else if (detailType.equals(EMoneyDetailType.PERCENTAGE.name())) { // 提成
				reportVo.setPercentage(operationValue);
				// 盈利计算
				reportVo.setGain(operationValue);
			} else if (detailType.equals(EMoneyDetailType.HALF_MONTH_BOUNS.name())) { // 半月分红
				reportVo.setHalfmonthbonus(operationValue);
			} else if (detailType.equals(EMoneyDetailType.DAY_BOUNS.name())) { // 日分红
				reportVo.setDaybonus(operationValue);
			} else if (detailType.equals(EMoneyDetailType.DAY_SALARY.name())) { // 日工资
				reportVo.setDaysalary(operationValue);
			} else if (detailType.equals(EMoneyDetailType.MONTH_SALARY.name())) { // 月工资
				reportVo.setMonthsalary(operationValue);
			} else if (detailType.equals(EMoneyDetailType.DAY_COMMISSION.name())) { // 日佣金
				reportVo.setDaycommission(operationValue);
			} else if (detailType.equals(EMoneyDetailType.PAY_TRANFER.name())) { // 支出转账
				reportVo.setPaytranfer(operationValue);
				// 不能参与
				// reportVo.setGain(reportVo.getGain().subtract(moneyDetail.getPay()));
			} else if (detailType.equals(EMoneyDetailType.INCOME_TRANFER.name())) { // 收入转账
				reportVo.setIncometranfer(operationValue);
				// 不能参与
				// reportVo.setGain(reportVo.getGain().add(moneyDetail.getIncome()));
			} else if (detailType.equals(EMoneyDetailType.SYSTEM_ADD_MONEY.name())) { // 人工存入
				reportVo.setSystemaddmoney(operationValue);
				// 盈利计算
				reportVo.setGain(operationValue);
			} else if (detailType.equals(EMoneyDetailType.SYSTEM_REDUCE_MONEY.name())) { // 误存提出
				reportVo.setSystemreducemoney(operationValue);
				// 盈利计算
				// reportVo.setGain(BigDecimal.ZERO.subtract(operationValue));
			} else if (detailType.equals(EMoneyDetailType.MANAGE_REDUCE_MONEY.name())) { // 行政提出
				reportVo.setManagereducemoney(operationValue);
				// 盈利计算
				reportVo.setGain(BigDecimal.ZERO.subtract(operationValue));
			} else if (detailType.equals(ConstantUtil.LOTTERY_EFFECTIVE)) { // 有效投注额，特别处理
				reportVo.setLotteryEffective(operationValue);
			} else {
				throw new RuntimeException("该资金明细类型不可知.");
			}
			userSelfDayConsumeService.insertSelective(reportVo);
		}

	}
	
	
	
	public Page getUserStatisticalRankingsPage(UserQuery query, Page page) {
		page.setTotalRecNum(userMoneyTotalMapper.getUserStatisticalRankingsPageCount(query));
		page.setPageContent(userMoneyTotalMapper.getUserStatisticalRankingsPage(query,page.getStartIndex(),page.getPageSize()));
		return page;
	}
	
	public String queryUserStatisticalRankingsIds(UserQuery query){
		String ids="";
		List<UserMoneyTotal> list=userMoneyTotalMapper.queryUserStatisticalRankingsIds(query);
		for(int i=0;i<list.size();i++){
			UserMoneyTotal user=list.get(i);
			ids+=user.getId()+",";
		}
		ids=ids.substring(0,ids.length()-1);
		return ids;
	}
	
	public Integer updateUserStarLevelByUserId(Integer sl,String ids){
		Integer i=userMoneyTotalMapper.updateUserStarLevelByUserId(sl, ids);
		return i;
	}
	
}
