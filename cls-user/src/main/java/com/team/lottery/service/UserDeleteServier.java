package com.team.lottery.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.mapper.user.UserMapper;
import com.team.lottery.mapper.userdelete.UserDelMapper;
import com.team.lottery.vo.UserDel;

@Service("userDeleteServier")
public class UserDeleteServier {
	
	@Autowired
	private UserDelMapper userDelMapper;
	@Autowired
	private UserMapper userMapper;
	
	public int deleteByPrimaryKey(Integer id) {
		return userDelMapper.deleteByPrimaryKey(id);
	}

	public int insert(UserDel record) {
		return userDelMapper.insert(record);
	}

	public int insertSelective(UserDel record) {
		return userDelMapper.insertSelective(record);
	}

	public UserDel selectByPrimaryKey(Integer id) {
		return userDelMapper.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(UserDel record) {
		return userDelMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(UserDel record) {
		return userDelMapper.updateByPrimaryKey(record);
	}

	public UserDel deleteByInvalidMember(Integer id) {
		return userDelMapper.deleteByInvalidMember(id);
	}
	
}
