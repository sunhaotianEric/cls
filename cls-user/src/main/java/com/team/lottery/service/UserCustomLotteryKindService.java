package com.team.lottery.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.mapper.usercustomlotterykind.UserCustomLotteryKindMapper;
import com.team.lottery.vo.UserCustomLotteryKind;

/**
 * 用户自定义彩种服务类
 * @author gs
 *
 */
@Service
public class UserCustomLotteryKindService {

	@Autowired
	private UserCustomLotteryKindMapper userCustomLotteryKindMapper;
	
	public int insert(UserCustomLotteryKind record) {
		return userCustomLotteryKindMapper.insert(record);
	}

    public int insertSelective(UserCustomLotteryKind record) {
    	return userCustomLotteryKindMapper.insertSelective(record);
    }

    public UserCustomLotteryKind selectByPrimaryKey(Long id) {
    	return userCustomLotteryKindMapper.selectByPrimaryKey(id);
    }

    public int updateByPrimaryKeySelective(UserCustomLotteryKind record) {
    	return userCustomLotteryKindMapper.updateByPrimaryKeySelective(record);
    }

    public int updateByPrimaryKey(UserCustomLotteryKind record) {
    	return userCustomLotteryKindMapper.updateByPrimaryKey(record);
    }
    
    /**
     * 通过用户Id查找对应的用户自定义彩种设置
     * @param userName
     * @return
     */
    public UserCustomLotteryKind getUserCustomLotteryKindById(Integer userId,String setType) {
    	return userCustomLotteryKindMapper.getUserCustomLotteryKindById(userId,setType);
    }

}
