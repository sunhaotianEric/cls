package com.team.lottery.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.UserBettingDemandQuery;
import com.team.lottery.mapper.userbettingdemand.UserBettingDemandMapper;
import com.team.lottery.vo.UserBettingDemand;

@Service
public class UserBettingDemandService {

	@Autowired
	private UserBettingDemandMapper userBettingDemandMapper;
	
	public int deleteByPrimaryKey(Integer id) {
		return userBettingDemandMapper.deleteByPrimaryKey(id);
	}

	public int insertSelective(UserBettingDemand record) {
		return userBettingDemandMapper.insertSelective(record);
	}

	public UserBettingDemand selectByPrimaryKey(Integer id) {
		return userBettingDemandMapper.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(UserBettingDemand record) {
		return userBettingDemandMapper.updateByPrimaryKeySelective(record);
	}

	/**
	 * 分页查询所有打码记录
	 * @param query
	 * @param page
	 * @return
	 */
	public Page getAllUserBettingDemandPage(UserBettingDemandQuery query, Page page) {
		page.setTotalRecNum(userBettingDemandMapper.getAllUserBettingDemandPageCount(query));
		page.setPageContent(userBettingDemandMapper.getAllUserBettingDemandPage(query, page.getStartIndex(), page.getPageSize()));
    	return page;
	}
	
	/**
	 * 查询用户进行中的打码要求
	 * @param userId
	 * @return
	 */
	public UserBettingDemand getUserGoingBettingDemand(Integer userId) {
		return userBettingDemandMapper.getUserGoingBettingDemand(userId);
	}

}
