<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>  
    <head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>配置编辑</title>
    <link rel="shortcut icon" href="favicon.ico">
    <jsp:include page="/admin/include/include.jsp"></jsp:include>

    <%
	 String id = request.getParameter("id");  
	%>
</head>
  <body class="gray-bg">
        <div class="wrapper wrapper-content animated fadeIn">
         <form action="javascript:void(0)">
            <div class="row">
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">彩种类型</span>
                        <cls:enmuSel name="lotteryKind" className="com.team.lottery.enums.ELotteryKind" options="class:ipt form-control" id="lotteryKind" />
                        </div>
                </div>
                 <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">线程休眠秒数</span>
                        <input id="sleepSecond" name="sleepSecond" type="text" value="" class="form-control"></div>
                 </div>  
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">正式接口url</span>
                        <input id="id" name="id" type="hidden" value="" class="form-control">
                        <input id="apiUrl" name="apiUrl" type="text" value="" class="form-control"></div>
                </div>
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">正式接口类型</span>
                        <cls:enmuSel name="apiType" className="com.team.lottery.enums.EOpenInterfaceType" options="class:ipt form-control" id="apiType" /></div>
                </div>
                   <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">备用接口url</span>
                        <input id="backApiUrl" name="backApiUrl" type="text" value="" class="form-control"></div>
                </div>
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">备用接口类型</span>
                        <cls:enmuSel name="backApiType" className="com.team.lottery.enums.EOpenInterfaceType" options="class:ipt form-control" id="backApiType" /></div>
                </div>


                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button type="button" class="btn btn-w-m btn-white" onclick="editOpenCodeApiPage.saveData()">提 交</button></div>
                        <div class="col-sm-6 text-center">
                            <!-- <button type="button" class="btn btn-w-m btn-white">重 置</button> -->
                            <input type="reset" class="btn btn-w-m btn-white" value="重 置"/>
                            </div>
                    </div>
                </div> 
            </div>
            </form>
        </div>

      <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/opencodeapi/js/editopencodeapi.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerOpenCodeApiAction.js'></script>
     <script type="text/javascript">
     editOpenCodeApiPage.param.id = <%=id%>;
	</script>
</body>
</html>

