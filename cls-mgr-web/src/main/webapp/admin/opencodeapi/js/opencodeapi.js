function OpenCodeApiPage(){}
var openCodeApiPage = new OpenCodeApiPage();

openCodeApiPage.param = {
   	
};

/**
 * 查询参数
 */
openCodeApiPage.queryParam = {
		lottery_kind : null,
};

//分页参数
openCodeApiPage.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize
};


$(document).ready(function() {
	openCodeApiPage.initTableData(); //初始化查询数据
});



/**
 * 初始化列表数据
 */
OpenCodeApiPage.prototype.initTableData = function() {
	//openCodeApiPage.queryParam = getFormObj($("#openCodeApiForm"));
	openCodeApiPage.table = $('#opencodeapiTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"scrollX": true,
		"iDisplayLength" : defaultPageSize,
		"ajax":function (data, callback, settings) {
			openCodeApiPage.pageParam.pageSize = data.length;//页面显示记录条数
			openCodeApiPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerOpenCodeApiAction.getOpenCodeApiPage(openCodeApiPage.queryParam,openCodeApiPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": "lotteryKindName"},
		            {"data": "apiUrl",
		            	render: function(data, type, row, meta) {
		            		return '<marquee title ="'+data+'"   behavior="scroll" contenteditable="true" onstart="this.firstChild.innerHTML+=this.firstChild.innerHTML;" scrollamount="1" width="100" onmouseover="this.stop();" onmouseout="this.start();">'+ 
      			                    data+'</marquee>';
	            		}},
		            {"data": "apiTypeName"},
		            {"data": "backApiUrl",
		            	render: function(data, type, row, meta) {
		            		return '<marquee title ="'+data+'"   behavior="scroll" contenteditable="true" onstart="this.firstChild.innerHTML+=this.firstChild.innerHTML;" scrollamount="1" width="100" onmouseover="this.stop();" onmouseout="this.start();">'+ 
		            			          data+'</marquee>';
	            		}
		            },
		            {"data": "backApiTypeName"},
		            {"data": "sleepSecond"},
		            {"data": "currentApiUrl",
		            	render: function(data, type, row, meta) {
		            		return '<marquee title ="'+data+'"   behavior="scroll" contenteditable="true" onstart="this.firstChild.innerHTML+=this.firstChild.innerHTML;" scrollamount="1" width="100" onmouseover="this.stop();" onmouseout="this.start();">'+ 
			                    data+'</marquee>';
	            		}},
		            {"data": "currentApiTypeName"},
		            {   
		            	"data": "updateTime",
		            	render: function(data, type, row, meta) {
		            		return dateFormat(data,'yyyy-MM-dd hh:mm:ss');
	            		}
		            },
		            {
	                	 "data": "id",
	                	 render: function(data, type, row, meta) {
	                		 var str="<a title='切换接口 ' class='btn btn-success btn-sm btn-bj' onclick='openCodeApiPage.changeUrlAndType("+data+")'><i class='fa fa-retweet'></i></a>&nbsp;&nbsp;"
	                			 str= str+"<a title='编辑' class='btn btn-info btn-sm btn-bj' onclick='openCodeApiPage.editOpenCodeApi("+data+")'><i class='fa fa-pencil'></i></a>";
	                		     //str= str+"<a title='删除' class='btn btn-danger btn-sm btn-del' onclick='openCodeApiPage.delOpenCodeApi("+data+")'><i class='fa fa-trash' ></i></a>";	   
	                	         return str;
	                	 }
	                }
	            ]
	}).api(); 
}



/**
 * 查询数据
 */
OpenCodeApiPage.prototype.findOpenCodeApi = function(){
	openCodeApiPage.queryParam.lotteryKind = $("#lotteryKind").val();
	openCodeApiPage.table.ajax.reload();
};

/**
 * 一键切换
 */
OpenCodeApiPage.prototype.changeUrlAndTypeAll=function(){
	swal({
        title: "您确定要一键切换开奖接口吗？",
        text: "请谨慎操作！",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        cancelButtonText: "取消",
        confirmButtonText: "确定",
        closeOnConfirm: false
    },
    function() {
 	   managerOpenCodeApiAction.changeOpenCodeApiUrlAndTypeAll(function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "操作成功",type: "success"});
				openCodeApiPage.findOpenCodeApi();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("操作失败.");
			}
	    });
    });
}

/**
 * 查询数据
 */
OpenCodeApiPage.prototype.changeUrlAndType = function(id){
	  swal({
          title: "您确定要切换开奖接口？",
          text: "请谨慎操作！",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          cancelButtonText: "取消",
          confirmButtonText: "确定",
          closeOnConfirm: false
      },
      function() {
   	   managerOpenCodeApiAction.changeOpenCodeApiUrlAndType(id,function(r){
  			if (r[0] != null && r[0] == "ok") {
  				swal({ title: "提示", text: "操作成功",type: "success"});
  				openCodeApiPage.findOpenCodeApi();
  			}else if(r[0] != null && r[0] == "error"){
  				swal(r[1]);
  			}else{
  				swal("操作失败.");
  			}
  	    });
      });
};

/**
 * 查询数据
 */
OpenCodeApiPage.prototype.refreshOpenCodeApi = function(){
	  swal({
          title: "您确定要刷新所有开奖接口？",
          text: "请谨慎操作！",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          cancelButtonText: "取消",
          confirmButtonText: "确定",
          closeOnConfirm: false
      },
      function() {
   	   managerOpenCodeApiAction.refreshOpenCodeApiUrlAndType(function(r){
  			if (r[0] != null && r[0] == "ok") {
  				swal({ title: "提示", text: "操作成功",type: "success"});
  			}else if(r[0] != null && r[0] == "error"){
  				swal(r[1]);
  			}else{
  				swal("操作失败.");
  			}
  	    });
      });
};
/**
 * 删除
 */
OpenCodeApiPage.prototype.delOpenCodeApi = function(id){
	
	   swal({
           title: "您确定要删除这条信息吗",
           text: "请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "删除",
           closeOnConfirm: false
       },
       function() {
    	   managerOpenCodeApiAction.delOpenCodeApiById(id,function(r){
   			if (r[0] != null && r[0] == "ok") {
   				swal({ title: "提示", text: "删除成功",type: "success"});
   				openCodeApiPage.findOpenCodeApi();
   			}else if(r[0] != null && r[0] == "error"){
   				swal(r[1]);
   			}else{
   				swal("删除信息失败.");
   			}
   	    });
       });
};



OpenCodeApiPage.prototype.addOpenCodeApi=function()
{
	
	 layer.open({
         type: 2,
         title: '配置新增',
         maxmin: false,
         shadeClose: true,
         //点击遮罩关闭层
         area: ['800px', '360px'],
        // area: ['60%', '45%'],
         content: contextPath + "/managerzaizst/opencodeapi/editopencodeapi.html",
     /*    btn: ['确 定', '关闭'],
         yes: function(index, layero){
        	 alert(index);
        	 var iframeWin = window['layui-layer-iframe' + index].window;
        	 iframeWin.editBizSystemPage.saveData();
        	 iframeWin.saveData();
        	  },*/
         cancel: function(index){ 
        	 openCodeApiPage.findOpenCodeApi();
      	   }
     });
}

OpenCodeApiPage.prototype.editOpenCodeApi=function(id)
{
	
	  layer.open({
           type: 2,
           title: '接口配置编辑',
           maxmin: false,
           shadeClose: true,
           //点击遮罩关闭层
           area: ['800px', '360px'],
          // area: ['60%', '45%'],
           content: contextPath + "/managerzaizst/opencodeapi/"+id+"/editopencodeapi.html",
           cancel: function(index){ 
        	   openCodeApiPage.findOpenCodeApi();
        	   }
       });
}

OpenCodeApiPage.prototype.closeLayer=function(index){
	layer.close(index);
	openCodeApiPage.findOpenCodeApi();
}


