function EditOpenCodeApiPage(){}
var editOpenCodeApiPage = new EditOpenCodeApiPage();

editOpenCodeApiPage.param = {
   	
};

$(document).ready(function() {
	var id = editOpenCodeApiPage.param.id;  //获取查询ID
	if(id != null){
		editOpenCodeApiPage.editOpenCodeApi(id);
	}
});


/**
 * 保存更新信息
 */
EditOpenCodeApiPage.prototype.saveData = function(){
	var id = $("#id").val();
	var lotteryKind = $("#lotteryKind").val();
	var apiUrl = $("#apiUrl").val();
	var apiType = $("#apiType").val();
	var backApiUrl = $("#backApiUrl").val();
	var backApiType = $("#backApiType").val();
	var sleepSecond = $("#sleepSecond").val();

	var va=/^[0-9]*$/;//验证正小数
	
	if(lotteryKind==null || lotteryKind.trim()==""){
		swal("请输入彩种类型!");
		$("#lotteryKind").focus();
		return;
	}
	
	if(apiUrl==null || apiUrl.trim()==""){
		swal("请输入正式接口url!");
		$("#apiUrl").focus();
		return;
	}
	
	if(apiType==null || apiType.trim()==""){
		swal("请输入正式接口类型!");
		$("#apiType").focus();
		return;
	}
	
	if(backApiUrl==null || backApiUrl.trim()==""){
		swal("请输入备用接口url!");
		$("#backApiUrl").focus();
		return;
	}
	
	if(backApiType==null || backApiType.trim()==""){
		swal("请输入备用接口类型!");
		$("#backApiType").focus();
		return;
	}
	if(sleepSecond==null || sleepSecond.trim()==""){
		swal("请输入线程休眠秒数!");
		$("#sleepSecond").focus();
		return;
	}
	if(!va.test(sleepSecond))
	{
		swal("请输入数字!");
	    $("#sleepSecond").focus();
	    return;
	}

	
	var openCodeApi = {};
	openCodeApi.id = editOpenCodeApiPage.param.id;
	openCodeApi.lotteryKind = lotteryKind;
	openCodeApi.apiUrl = apiUrl;
	openCodeApi.apiType = apiType;
	openCodeApi.backApiUrl = backApiUrl;
	openCodeApi.backApiType = backApiType;
	openCodeApi.sleepSecond = sleepSecond;
	managerOpenCodeApiAction.saveOrUpdateOpenCodeApi(openCodeApi,function(r){
		if (r[0] != null && r[0] == "ok") {
			   swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.openCodeApiPage.closeLayer(index);
		   	    });     
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("保存信息失败.");
		}
    });
};


/**
 * 赋值信息
 */
EditOpenCodeApiPage.prototype.editOpenCodeApi = function(id){
	managerOpenCodeApiAction.getOpenCodeApiById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			$("#id").val(r[1].id);
			$("#lotteryKind").val(r[1].lotteryKind);
			$("#apiUrl").val(r[1].apiUrl);
			$("#apiType").val(r[1].apiType);
			$("#backApiUrl").val(r[1].backApiUrl);
			$("#backApiType").val(r[1].backApiType);
			$("#sleepSecond").val(r[1].sleepSecond);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取信息失败.");
		}
    });
};