<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>开奖接口配置管理</title>
<link rel="shortcut icon" href="favicon.ico">
<jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
<body>
	<div class="animated fadeIn">
		<div class="ibox-content">
			<div class="row m-b-sm m-t-sm">
				<form class="form-horizontal" id="openCodeApiForm">
					<div class="row">
						<div class="col-sm-3">
							<div class="input-group m-b">
								<span class="input-group-addon">彩种类型</span>
								<cls:enmuSel name="lotteryKind"
									className="com.team.lottery.enums.ELotteryKind"
									options="class:ipt form-control" id="lotteryKind" />
							</div>
						</div>
						<div class="col-sm-4"></div>
						<div class="col-sm-5">
							<div class="form-group nomargin text-right">
								<button type="button" class="btn btn-outline btn-default btn-js"
									onclick="openCodeApiPage.pageParam.pageNo = 1;openCodeApiPage.findOpenCodeApi()">
									<i class="fa fa-search"></i>&nbsp;查 询
								</button>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<button type="button"
									class="btn btn-outline btn-default btn-add"
									onclick="openCodeApiPage.addOpenCodeApi()">
									<i class="fa fa-plus"></i>&nbsp;新 增
								</button>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<button type="button" class="btn btn-outline btn-default btn-js"
									onclick="openCodeApiPage.pageParam.pageNo = 1;openCodeApiPage.changeUrlAndTypeAll()">
									<i class="fa fa-refresh"></i>&nbsp;一键切换
								</button>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<button type="button"
									class="btn btn-outline btn-default btn-add"
									onclick="openCodeApiPage.refreshOpenCodeApi()">
									<i class="fa fa-refresh"></i>&nbsp;刷新
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="jqGrid_wrapper">
				<div class="ui-jqgrid ">
					<div class="ui-jqgrid-view">
						<div class="ui-jqgrid-hdiv">
							<div class="ui-jqgrid-hbox" style="padding-right: 0">
								<table id="opencodeapiTable"
									class="ui-jqgrid-htable ui-common-table table table-bordered">
									<thead>
										<tr class="ui-jqgrid-labels">
											<th class="ui-th-column ui-th-ltr">彩种类型</th>
											<th class="ui-th-column ui-th-ltr">正式接口url</th>
											<th class="ui-th-column ui-th-ltr">正式接口类型</th>
											<th class="ui-th-column ui-th-ltr">备用接口url</th>
											<th class="ui-th-column ui-th-ltr">备用接口类型</th>
											<th class="ui-th-column ui-th-ltr">线程休眠秒数</th>
											<th class="ui-th-column ui-th-ltr">当前使用接口url</th>
											<th class="ui-th-column ui-th-ltr">当前使用api类型</th>
											<th class="ui-th-column ui-th-ltr">更新时间</th>
											<th class="ui-th-column ui-th-ltr">操 作</th>
										</tr>
									</thead>
									<tbody id="opencodeapiList">
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- js -->
	<script type="text/javascript"
		src="<%=path%>/admin/opencodeapi/js/opencodeapi.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
	<!-- dwr -->
	<script type='text/javascript'
		src='<%=path%>/dwr/interface/managerOpenCodeApiAction.js'></script>
</body>
</html>

