<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>  
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>聊天数据库配置</title>
        <jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
    <body >
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
                    <div class="ibox ">
                        <div class="ibox-content">
                            <div class="row m-b-sm m-t-sm">
                                <form class="form-horizontal" id="queryForm">
                                    <div class="row">
			                            <div class="col-sm-12">
				                              <div class="form-group">
				                                  <div class="col-sm-5">
				                                      <button type="button" class="btn btn-outline btn-default btn-kj" onclick="chatRoomConfigPage.addChatRoomConfig()"><i class="fa fa-plus"></i>&nbsp;新增</button>
				                                  </div>
				                              </div>
			                            </div>
                                    </div>
                                </form>
                            </div>
                            <div class="jqGrid_wrapper">
                                <div class="ui-jqgrid ">
                                    <div class="ui-jqgrid-view">
                                        <div class="ui-jqgrid-hdiv">
                                            <div class="ui-jqgrid-hbox" style="padding-right:0">
                                                <table class="ui-jqgrid-htable ui-common-table table table-bordered" id="chatRoomConfigTable">
                                                    <thead>
                                                        <tr class="ui-jqgrid-labels">
                                                        	  <th class="ui-th-column ui-th-ltr">序号</th>
                                                              <th class="ui-th-column ui-th-ltr">商户</th>
                                                              <th class="ui-th-column ui-th-ltr">数据库地址</th>
													          <th class="ui-th-column ui-th-ltr">数据库账号</th>
													          <th class="ui-th-column ui-th-ltr">数据库密码</th>
													          <th class="ui-th-column ui-th-ltr">聊天代理用户名</th>
													          <th class="ui-th-column ui-th-ltr">token</th>
													          <th class="ui-th-column ui-th-ltr">操  作</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/chatroomconfig/js/chatroomconfig.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/manageChatRoomConfigAction.js'></script>    
</body>
</html>

