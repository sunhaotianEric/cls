function ChatRoomConfigEditPage(){}
var chatRoomConfigEditPage = new ChatRoomConfigEditPage();

chatRoomConfigEditPage.param = {
   	
};

$(document).ready(function() {
	if(chatRoomConfigEditPage.param.id!=null){
		chatRoomConfigEditPage.editChatRoomConfig(chatRoomConfigEditPage.param.id);
	}
});


/**
 * 保存更新信息
 */
ChatRoomConfigEditPage.prototype.saveData = function(){
	var jdbcUrl = $("#jdbcUrl").val();
	var bizSystem = $("#bizSystem").val();
	var jdbcUsername = $("#jdbcUsername").val();
	var jdbcPassword = $("#jdbcPassword").val();
	var token=$("#token").val();
	var roomUsername = $("#roomUsername").val();
	
	if(jdbcUrl==null || jdbcUrl.trim()==""){
		swal("请输入数据库地址!");
		$("#jdbcUrl").focus();
		return;
	}
	if(jdbcUsername==null || jdbcUsername.trim()==""){
		swal("请输入数据库账号!");
		$("#jdbcUsername").focus();
		return;
	}
	if(jdbcPassword==null || jdbcPassword.trim()==""){
		swal("请输入数据库密码!");
		$("#jdbcPassword").focus();
		return;
	}
	if(bizSystem==null || bizSystem.trim()==""){
		swal("请选择业务系统!");
		$("#bizSystem").focus();
		return;
	}
	if(token==null || token.trim()==""){
		swal("请输入加密token!");
		$("#token").focus();
		return;
	}
	/*if(roomUsername==null || roomUsername.trim()==""){
		swal("请输入模式!");
		$("#roomUsername").focus();
		return;
	}*/
	
	
	var chatroomconfig = {};
	chatroomconfig.id = chatRoomConfigEditPage.param.id==null?0:chatRoomConfigEditPage.param.id;
	chatroomconfig.bizSystem = bizSystem;
	chatroomconfig.jdbcUrl =jdbcUrl;
	chatroomconfig.jdbcUsername =jdbcUsername;
	chatroomconfig.jdbcPassword =jdbcPassword;
	chatroomconfig.token =token;
	chatroomconfig.roomUsername =roomUsername;
	manageChatRoomConfigAction.saveOrUpdateChatRoomConfig(chatroomconfig,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.chatRoomConfigPage.closeLayer(index);
		   	   });
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("保存信息失败.");
		}
    });
};


/**
 * 赋值配置信息
 */
ChatRoomConfigEditPage.prototype.editChatRoomConfig= function(id){
	manageChatRoomConfigAction.getChatRoomConfigById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			$("#jdbcUrl").val(r[1].jdbcUrl);
			$("#bizSystem").val(r[1].bizSystem);
			$("#jdbcUsername").val(r[1].jdbcUsername);
			$("#jdbcPassword").val(r[1].jdbcPassword);
			$("#token").val(r[1].token);
			$("#roomUsername").val(r[1].roomUsername);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取信息失败.");
		}
    });
};
