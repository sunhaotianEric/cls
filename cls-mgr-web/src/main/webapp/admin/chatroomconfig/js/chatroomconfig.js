function ChatRoomConfigPage(){}
var chatRoomConfigPage = new ChatRoomConfigPage();

chatRoomConfigPage.param = {
   	
};

/**
 * 查询参数
 */
chatRoomConfigPage.queryParam = {
		bizSystem:null,
		roomUsername:null
};

//分页参数
chatRoomConfigPage.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize

};


$(document).ready(function() {

	chatRoomConfigPage.initTableData(); //初始化查询数据
});

/**
 * 初始化列表数据
 */
ChatRoomConfigPage.prototype.initTableData = function() {
	chatRoomConfigPage.table = $('#chatRoomConfigTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"scrollX": true,
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			chatRoomConfigPage.pageParam.pageSize = data.length;//页面显示记录条数
			chatRoomConfigPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	manageChatRoomConfigAction.getAllChatRoomConfig(chatRoomConfigPage.queryParam,chatRoomConfigPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data":null},
                    {"data": "bizSystemName"},
		            {
                    	"data": "jdbcUrl",
                    	render:function(data,type,row,meta){
                    		return "<laber title="+data+">"+data.substring(0,15)+"</laber>";
                    	}
                    },
		            {"data": "jdbcUsername"},
		            {"data": "jdbcPassword"},
		            {"data": "roomUsername"},
		            {"data": "token"},
		            {
	                	 "data": "id",
	                	 render: function(data, type, row, meta) {
	                		 return "<a class='btn btn-info btn-sm btn-bj' onclick='chatRoomConfigPage.editChatRoomConfig("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;" +
	                		 		"<a class='btn btn-danger btn-sm btn-del' onclick='chatRoomConfigPage.delChatRoomConfig("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";
	                	 }
	                }
	            ]
	}).api(); 
}





/**
 * 查询数据
 */
ChatRoomConfigPage.prototype.findChatRoomConfig= function(){
	chatRoomConfigPage.table.ajax.reload();
};


/**
 * 删除
 */
ChatRoomConfigPage.prototype.delChatRoomConfig= function(id){
	   swal({
           title: "您确定要删除这条信息吗",
           text: "请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "删除",
           closeOnConfirm: false
       },
       function() {
    	   manageChatRoomConfigAction.delChatRoomConfigById(id,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "删除成功",type: "success"});
				chatRoomConfigPage.findChatRoomConfig();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("删除信息失败.");
			}
	    });
	});
};

ChatRoomConfigPage.prototype.addChatRoomConfig=function()
{
	  layer.open({
          type: 2,
          title: '系统配额新增',
          maxmin: false,
          shadeClose: true,
          //点击遮罩关闭层
          area: ['600px', '500px'],
         // area: ['70%', '80%'],
          content: contextPath + "/managerzaizst/chatroomconfig/chatroomconfigedit.html",
          cancel: function(index){ 
        	  chatRoomConfigPage.findChatRoomConfig();
       	   }
      });
}

ChatRoomConfigPage.prototype.editChatRoomConfig=function(id)
{
	  layer.open({
          type: 2,
          title: '系统配额编辑',
          maxmin: false,
          shadeClose: true,
          //点击遮罩关闭层
          area: ['600px', '500px'],
         // area: ['70%', '80%'],
          content: contextPath + "/managerzaizst/chatroomconfig/chatroomconfigedit.html?id="+id,
          cancel: function(index){ 
        	  chatRoomConfigPage.findChatRoomConfig();
       	   }
      });
}

ChatRoomConfigPage.prototype.closeLayer=function(index){
	layer.close(index);
	chatRoomConfigPage.findChatRoomConfig();
}