<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant,com.team.lottery.util.MD5PasswordUtil,java.util.Date" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>  
    <head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>配置编辑</title>
    <link rel="shortcut icon" href="favicon.ico">
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    <%
	 String id = request.getParameter("id");  //获取查询的商品ID
	%>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content  animated fadeIn">
		<div class="row">
			<form class="form-horizontal" id="quotaControlForm">
				<div class="col-sm-6">
					<div class="input-group m-b">
                        <span class="input-group-addon">商户</span>
                        <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control" />
                    </div>
                    <div class="input-group m-b">
                        <span class="input-group-addon">数据库地址</span>
                        <input id="jdbcUrl" name="jdbcUrl" type="text" value="" class="form-control">
                    </div>
                    <div class="input-group m-b">
                        <span class="input-group-addon">数据库账号</span>
                        <input id="jdbcUsername" name="jdbcUsername" type="text" value="" class="form-control">
                    </div>
                    <div class="input-group m-b">
                        <span class="input-group-addon">数据库密码</span>
                        <input id="jdbcPassword" name="jdbcPassword" type="text" value="" class="form-control">
                    </div>
                    <div class="input-group m-b">
                        <span class="input-group-addon">加密token</span>
                        <input id="token" name="token" type="text" value="" class="form-control" disabled="disabled">
                        <a type="button" class="input-group-addon" onclick="md5Date()"><i class="fa fa-plus"></i>&nbsp;生成token</a>
                    </div>
                    <div class="input-group m-b">
                        <span class="input-group-addon">聊天代理用户名</span>
                        <input id="roomUsername" name="roomUsername" type="text" value="" class="form-control">
                    </div>
				</div>
				<div class="col-sm-12">
					<div class="row">
						<div class="col-sm-12 text-center">
							<button type="button" class="btn btn-w-m btn-white" onclick="chatRoomConfigEditPage.saveData()">提 交</button>							
							&nbsp;&nbsp; 
							<input type="reset" class="btn btn-w-m btn-white" value="重 置" />
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/chatroomconfig/js/chatroomconfigedit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/manageChatRoomConfigAction.js'></script>
    <script type="text/javascript">
    chatRoomConfigEditPage.param.id = <%=id%>;
    /**
     * 生成token
     */
    function md5Date(){
    	$("#token").val('<%=MD5PasswordUtil.GetMD5Code(new Date().getTime()+"")%>');
    }
	</script>    
</body>
</html>

