<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
    String id = request.getParameter("id")=="0"?"0":request.getParameter("id"); 
%>
<!DOCTYPE html>
<html>  
    <head>
     <meta charset="utf-8">
    <title>聊天房间编辑</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    <link rel="shortcut icon" href="favicon.ico">
</head>
   <body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeIn">
		<div class="row back-change">
			<div class="col-sm-12">
				<div class="ibox ">
					<div class="ibox-content">
						<form class="form-horizontal" id="bonusOrderForm">
							<c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
								<div class="form-group">
									<label class="col-sm-2 control-label">商户：</label>
									<div class="col-sm-10">
										<cls:bizSel name="bizSystem" id="bizSystem"	options="class:ipt form-control" />
									</div>
								</div>
							</c:if>
							<div class="form-group">
								<label class="col-sm-2 control-label">房间ID：</label>
								<div class="col-sm-10">
									<input id="roomId" name="roomId" type="text" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">房间名：</label>
								<div class="col-sm-10">
									<input id="roomName" name="roomName" type="text" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">归属代理：</label>
								<div class="col-sm-10">
									<input id="agent" name="agent" type="text" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">关键词：</label>
								<div class="col-sm-10">
									<textarea name="msgban" id="msgban" style="width:100%;height:200px;"></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label"></label>
								<div class="col-sm-10">
									<font color="red">注意：格式请按照此“关键词|关键词” 格式。</font>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<div class="col-sm-6 text-center">
										<button type="button" class="btn btn-w-m btn-white"	onclick="chatRoomEdit.saveData()">提 交</button>
									</div>
									<div class="col-sm-6 text-center">
										<input type="reset" class="btn btn-w-m btn-white" value="重 置" />
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="<%=path%>/admin/chatroom/js/chatroomedit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerChatRoomAction.js'></script>
    <script type="text/javascript">
    	chatRoomEdit.param.id = <%=id%>;
	</script>
</body>
</html>

