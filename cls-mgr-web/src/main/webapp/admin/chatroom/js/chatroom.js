function ChatRoomPage(){}
var chatRoomPage = new ChatRoomPage();


/**
 * 查询参数
 */
chatRoomPage.queryParam = {
		bizSystem:null,
		roomId:null,
		agent:null
};

//分页参数
chatRoomPage.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize
};

$(document).ready(function() {
	chatRoomPage.initTableData();
});

ChatRoomPage.prototype.initTableData=function(){
	chatRoomPage.table = $('#chatRoomTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"scrollX": true,
		"iDisplayLength" : defaultPageSize,
		"ajax":function (data, callback, settings) {
		chatRoomPage.pageParam.pageSize = data.length;//页面显示记录条数
		chatRoomPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerChatRoomAction.getChatRoom(chatRoomPage.queryParam,chatRoomPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": "bizSystemName","bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "roomId"},
		            {"data": "roomName" },
		            {"data": "agent" },
		            {"data": "createDateStr"},		        
		            {
	                	 "data": "id",
	                	 render: function(data, type, row, meta) {
	                		 var str="";
	                		 str +="<a class='btn btn-info btn-sm btn-bj' onclick='chatRoomPage.editChatRoom("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;";
	                		 str +="<a class='btn btn-danger btn-sm btn-del' onclick='chatRoomPage.delChatRoom("+data+")'><i class='fa fa-trash'></i>&nbsp;删除 </a>&nbsp;";
	                		 return str;
	                	 }
	                }
	            ]
	}).api(); 
}

ChatRoomPage.prototype.findChatRoomByBizSystem=function(){
	var bizSystem=$("#bizSystem").val();
	var roomId=$("#roomId").val();
	var agent=$("#agent").val();
	if(bizSystem==null||bizSystem==''){
		chatRoomPage.queryParam.bizSystem=null;
	}else{
		chatRoomPage.queryParam.bizSystem=bizSystem;
	}
	if(roomId==null||roomId==''){
		chatRoomPage.queryParam.roomId=null;
	}else{
		chatRoomPage.queryParam.roomId=roomId;
	}
	if(agent==null||agent==''){
		chatRoomPage.queryParam.agent=null;
	}else{
		chatRoomPage.queryParam.agent=agent;
	}
	chatRoomPage.table.ajax.reload();
}

ChatRoomPage.prototype.editChatRoom=function(id){
	layer.open({
        type: 2,
        title: '聊天室操作',
        maxmin: false,
        shadeClose: true,
        //点击遮罩关闭层
        area: ['65%', '80%'],
        content: contextPath + "/managerzaizst/chatroom/chatroomedit.html?id="+id,
    });
}

ChatRoomPage.prototype.delChatRoom=function(id){
	swal({
        title: "您确定要删除该聊天室吗",
        text: "请谨慎操作！",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        cancelButtonText: "取消",
        confirmButtonText: "确定",
        closeOnConfirm: false
    },
    function() {
    	managerChatRoomAction.delChatRoom(id,function(r){
	    		if (r[0] != null && r[0] == "ok") {
	    			swal("删除成功！");
	    			chatRoomPage.findChatRoomByBizSystem();
	    		}else if(r[0] != null && r[0] == "error"){
	    			swal(r[1]);
	    		}else{
	    			swal("查询数据失败.");
	    		}
	        });  
     });
}

ChatRoomPage.prototype.closeLayer=function(index){
	layer.close(index);
	chatRoomPage.findChatRoomByBizSystem();
}