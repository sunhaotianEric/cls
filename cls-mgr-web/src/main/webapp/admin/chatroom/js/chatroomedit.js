function ChatRoomEdit(){}
var chatRoomEdit = new ChatRoomEdit();

chatRoomEdit.param={
		id:null,
		bizSystem:null,
		roomId:null,
		roomName:null,
		agent:null,
		msgban:null
};

$(document).ready(function() {
	if(chatRoomEdit.param.id!=0){
		chatRoomEdit.getChatRoomById();
	}
});

/**
 * 保存
 */
ChatRoomEdit.prototype.saveData=function(){
	var roomId=$("#roomId").val();
	var roomName=$("#roomName").val();
	var agent=$("#agent").val();
	var msgban=$("#msgban").val();
	if(isNaN(roomId)){
		swal("房间号只能是数字");
		return;
	}
	if(roomId==null||roomId==''){
		swal("房间号不能为空");
		return;
	}
	if(roomName==null||roomName==''){
		swal("房间名不能为空");
		return;
	}
	if(agent==null||agent==''){
		swal("归属代理不能为空");
		return;
	}
	var bizSystem=$("#bizSystem").val();
	chatRoomEdit.param.bizSystem=bizSystem;
	chatRoomEdit.param.roomId=roomId;
	chatRoomEdit.param.roomName=roomName;
	chatRoomEdit.param.agent=agent;
	chatRoomEdit.param.msgban=msgban;
	if(chatRoomEdit.param.id!=0){
		managerChatRoomAction.updateChatRoomByid(chatRoomEdit.param,function(r){
			if(r[0]=="ok"){
				swal({
			           title: "编辑成功",
			           text: "您已经编辑了这条信息。",
			           type: "success"
			       },
			       function() {
			    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
		               parent.chatRoomPage.closeLayer(index);
			   	    });
			}else{
				swal(r[1]);
			}
		});
	}else{
		managerChatRoomAction.saveChatRoom(chatRoomEdit.param,function(r){
			if(r[0]=="ok"){
				swal({
			           title: "保存成功",
			           text: "您已经保存了这条信息。",
			           type: "success"
			       },
			       function() {
			    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
		               parent.chatRoomPage.closeLayer(index);
			   	    });
			}else{
				swal(r[1]);
			}
		});
	}
}

/**
 * 根据ID查询单个聊天室数据
 */
ChatRoomEdit.prototype.getChatRoomById=function(){
	managerChatRoomAction.getChatRoomById(chatRoomEdit.param,function(r){
		if(r[0]=="ok"){
			$("#bizSystem").val(r[1].bizSystem);
			$("#roomId").val(r[1].roomId);
			$("#roomName").val(r[1].roomName);
			$("#agent").val(r[1].agent);
			$("#bizSystem").attr("disabled","disabled");
			if(r[1]==1){
				$("#agent").attr("disabled","disabled");
			}
			$("#msgban").html(r[1].msgban);
		}else{
			swal("查询数据失败");
		}
	});
}