<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
	String chatlogId=request.getParameter("chatlogId");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>红包详情</title>
	<link rel="shortcut icon" href="favicon.ico">
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    <link href="<%=path%>/css/plugins/iCheck/custom.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" rel="stylesheet">
    <script type="text/javascript">
   		var chatlogId='<%=chatlogId%>';
    </script>
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeIn">
	<div class="jqGrid_wrapper">
		<div class="ui-jqgrid ">
			<div class="ui-jqgrid-view">
				<div class="ui-jqgrid-hdiv">
					<div class="ui-jqgrid-hbox" style="padding-right: 0">
						<table class="ui-jqgrid-htable ui-common-table table table-bordered" id="redbagEditTable">
							<thead>
								<tr class="ui-jqgrid-labels">
									<th class="ui-th-column ui-th-ltr">序号</th>
									<th class="ui-th-column ui-th-ltr">用户名</th>
									<th class="ui-th-column ui-th-ltr">金额</th>
								</tr>
							</thead>
							<tbody>

							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	<!-- js -->
	<script type="text/javascript"	src="<%=path%>/admin/chatredbag/js/chatredbag_edit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
	<!-- dwr -->
		<script type='text/javascript'	src='<%=path%>/dwr/interface/managerChatRedbagAction.js'></script>
</body>
</html>