function RedbagInfoPage(){};
var redbagInfoPage=new RedbagInfoPage();

redbagInfoPage.param={
};

//分页参数
redbagInfoPage.pageParam = {
        pageNo : 1,
        pageSize : 15
};

$(function(){
	redbagInfoPage.interDate2();
});

RedbagInfoPage.prototype.queryredbaginfo=function(){
	var bizSystem=$("#bizSysteminfo").val();
	if(bizSystem!=""){
		redbagInfoPage.param.bizSystem=bizSystem;
	}else{
		redbagInfoPage.param.bizSystem=null;
	}
	redbagInfoPage.table.ajax.reload();
}


RedbagInfoPage.prototype.interDate2=function(){
	redbagInfoPage.param = getFormObj($("#redbaginfoForm"));
	redbagInfoPage.table = $('#redbaginfoTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			redbagInfoPage.pageParam.pageSize = data.length;//页面显示记录条数
			redbagInfoPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerChatRedbagAction.getChatRedbaginfoPage(redbagInfoPage.param,redbagInfoPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
                    {"data": null},
                    {"data": "bizSystemName", 
		                 "bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "roomId"},
		            {"data": "userName"},
		            {"data": "money"},
		            {"data": "createDateStr"}
	            ]
	}).api(); 
}

