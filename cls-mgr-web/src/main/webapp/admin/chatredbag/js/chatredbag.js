function RedbagPage(){};
var redbagPage=new RedbagPage();

redbagPage.param={
};

/*redbagPage.param2={
		bizSystem:null,
		username:null,
		createDate:null
};*/

redbagPage.queryParam = {
};

//分页参数
redbagPage.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize
};

$(function(){
	redbagPage.interDate();
//	$("#redbaginfo").one("click",function(){
//		redbagPage.interDate2();
//	});
});

RedbagPage.prototype.queryredbag=function(){
	var bizSystem=$("#bizSystem").val();
	if(bizSystem==null||bizSystem==""){
		redbagPage.queryParam.bizSystem=null;
	}else{
		redbagPage.queryParam.bizSystem=bizSystem;
	}
	redbagPage.table.ajax.reload();
}



RedbagPage.prototype.interDate=function(){
	redbagPage.queryParam = getFormObj($("#redbagForm"));
	redbagPage.table = $('#redbagTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			redbagPage.pageParam.pageSize = data.length;//页面显示记录条数
			redbagPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerChatRedbagAction.getChatRedbagPage(redbagPage.queryParam,redbagPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
                    {"data": null},
                    {"data": "bizSystemName", 
		                 "bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "roomId"},
		            {"data": "userName"},
		            {"data": "type",
		            	render:function(data,type,row,meta){
	            			var str = "";
	            			if(data == "RANDOM"){
	            				str = "拼手气红包";
	            			}
	            			if(data == "AVG"){
	            				str = "普通红包";
	            			}
	            			return str;
	            		}	
		            },
		            {"data": "money"},
		            {"data": "count"/*,
		            	render:function(data,type,row,meta){
	            			var str = data+"/"+row.receivedCount;
	            			return str;
	            		}	*/
		            },
		            {"data": "createTimeStr"}
		            /*{"data":null,
		            		render:function(data,type,row,meta){
		            			return '<button type="button" class="btn btn-outline btn-default" onclick="redbagPage.url('+row.chatlogId+')"> <i class="fa fa-search"></i>&nbsp;详 情</button>';
		            		}
		            }*/
	            ]
	}).api(); 
}

RedbagPage.prototype.layerClose=function(index){
	layer.close(index);
	redbagPage.queryredbag();
}

/*RedbagPage.prototype.url=function(id){
	layer.open({
		  type: 2,
		  title: '红包详情',
		  shadeClose: true,
		  shade: 0.8,
		  area: ['17%', '50%'],
		  content: contextPath + '/managerzaizst/chatredbag/chatredbag_edit.html?chatlogId='+id //iframe的url
		});
}*/