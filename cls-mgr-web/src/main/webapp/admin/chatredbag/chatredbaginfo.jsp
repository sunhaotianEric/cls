<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>抢红包记录</title>
	<link rel="shortcut icon" href="favicon.ico">
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    <link href="<%=path%>/css/plugins/iCheck/custom.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" rel="stylesheet">
</head>
<body>
	<div class="animated fadeIn">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox ">
                <div class="ibox-content">
                			<form class="form-horizontal" id="redbaginfoForm">
									<div class="row">
										<c:if test="${admin.bizSystem == 'SUPER_SYSTEM'}">
										<div class="col-sm-2">
											<div class="input-group m-b">
												<span class="input-group-addon">商户</span>
												<cls:bizSel name="bizSysteminfo" id="bizSysteminfo"
													options="class:ipt form-control" />
											</div>
										</div>
										</c:if>
<!-- 										 <div class="col-sm-2">
											<div class="input-group m-b">
												<span class="input-group-addon">聊天室</span>
                                            <input id="roomId" type="text" value="" class="form-control">
											</div>
										</div>
										<div class="col-sm-2">
											<div class="input-group m-b">
												<span class="input-group-addon">用户名</span> <input
													id="userNameinfo" type="text" value="" class="form-control">
											</div>
										</div>
										<div class="col-sm-4">
									        <div class="input-group">
									            <span class="input-group-addon">时间始终</span>
									            <div class="input-daterange input-group" id="datepicker">
									                <input class="form-control layer-date" placeholder="开始日期" onClick="laydate({istime: true, format: 'YYYY-MM-DD'})" id="startime">
									                <span class="input-group-addon">到</span>
									                <input class="form-control layer-date" placeholder="结束日期" onClick="laydate({istime: true, format: 'YYYY-MM-DD'})" id="endtime"></div>
									        </div>
									    </div> -->
										<div class="col-sm-2">
											<div class="form-group nomargin text-right">
												<button type="button" class="btn btn-outline btn-default"
													onclick="redbagInfoPage.queryredbaginfo()">
													<i class="fa fa-search"></i>&nbsp;查 询
												</button>
											</div>
										</div>
									</div>
								</form>
    							<div class="jqGrid_wrapper">
									<div class="ui-jqgrid ">
										<div class="ui-jqgrid-view">
											<div class="ui-jqgrid-hdiv">
												<div class="ui-jqgrid-hbox" style="padding-right: 0">
													<table class="ui-jqgrid-htable ui-common-table table table-bordered" id="redbaginfoTable">
														<thead>
															<tr class="ui-jqgrid-labels">
																<th class="ui-th-column ui-th-ltr">序号</th>
																<th class="ui-th-column ui-th-ltr">商户</th>
																<th class="ui-th-column ui-th-ltr">聊天室</th>
																<th class="ui-th-column ui-th-ltr">用户名</th>
																<th class="ui-th-column ui-th-ltr">金额</th>
																<th class="ui-th-column ui-th-ltr">时间</th>
															</tr>
														</thead>
														<tbody>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
                </div>
            </div>
        </div>
    </div>
</div>
	<script src="<%=path%>/js/plugins/iCheck/icheck.min.js"></script>
	<!-- js -->
	<script type="text/javascript"	src="<%=path%>/admin/chatredbag/js/chatredbaginfo.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
	<!-- dwr -->
	<script type='text/javascript'	src='<%=path%>/dwr/interface/managerChatRedbagAction.js'></script>
</body>
</html>