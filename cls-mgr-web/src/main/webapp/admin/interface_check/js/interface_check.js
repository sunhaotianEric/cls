function InterfaceCheckPage(){}
var interfaceCheckPage = new InterfaceCheckPage();

/**
 * 查询参数
 */
interfaceCheckPage.queryParam = {
	lotteryType : null,
	interfaceType : null
};

$(document).ready(function(){
	
	$(".i-checks").iCheck({
		checkboxClass:"icheckbox_square-green",
		radioClass:"iradio_square-green",
	})
	
    $("#lotteryName").find("option[value='JYKS']").remove();
    $("#lotteryName").find("option[value='JLFFC']").remove();
});

/**
 * 查询接口数据
 */
InterfaceCheckPage.prototype.queryInterfaceData = function(){
	//每次查询前先清空结果
	$("#ccomment").val("");
	var lotteryType = $("#lotteryName").val();
	if(lotteryType == "") {
		alert("请选择彩种!");
		return;
	}
	var interfaceType = "";
	$(".b").each(function(i){
		var hasClass =$(this).parent().hasClass("checked");
		if(hasClass){
			interfaceType = $(this).val();
		}
	})
	
	if(interfaceType == ""){
		interfaceType = "0"
	}
	
	managerOpenCodeApiAction.refreshOpenCodeApiUrlAndTypeByLotteryKind(lotteryType,interfaceType,function(r){
		if (r[0] != null && r[0] == "ok") {
			$("#ccomment").val(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			layer.alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询接口数据失败.");
		}
    });
};

