<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import = "com.team.lottery.enums.ELotteryKind" %>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<html>
<head>
    <meta charset="utf-8" />
    <title>开奖接口数据查看</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
      <link rel="shortcut icon" href="favicon.ico">
       <link href="<%=path%>/css/animate.min.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" rel="stylesheet">
        <link href="<%=path%>/css/style.min862f.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>?v=4.1.0" rel="stylesheet">
      <link href="<%=path%>/css/plugins/iCheck/custom.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" rel="stylesheet">
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeIn">
          <div class="row">
              <div class="col-sm-12">
                  <div class="ibox">
                      <div class="ibox-content">
                          <form class="form-horizontal">
                              <div class="row">
                                  <div class="col-sm-4">
                                      <div class="form-group">
                                          <label class="col-sm-3 control-label nopadding" style="padding-top:7px!important;">彩种：</label>
                                        	<div class="input-group m-b">
				                           	<span class="input-group-addon">选择彩种</span>
					                          <cls:enmuSel name="lotteryName" id="lotteryName" options="class:ipt form-control"  className="com.team.lottery.enums.ELotteryKind"/> 
					                           <%--     <select class="ipt form-control" name="lotteryName" id="lotteryName">
					                                    <option value="" selected="selected">==请选择==</option>
					                                    <option value="CQSSC">重庆时时彩</option>
					                                    <option value="TJSSC">天津时时彩</option>
					                                    <option value="XJSSC">新疆时时彩</option>
					                                    
					                                    <option value="TWWFC">台湾五分彩</option>
					                                    <option value="HGFFC">韩国1.5分彩</option>
					                                    <option value="XJPLFC">新加坡2分彩</option>
					                                    <option value="BJPK10">北京PK10</option>
					                                    
					                                    <option value="GDSYXW">广东11选5</option>
					                                    <option value="SDSYXW">山东11选5</option>
					                                    <option value="JXSYXW">江西11选5</option>
					                                    <option value="FJSYXW">福建11选5</option>
					                                    
					                                    <option value="JSKS">江苏快3</option>
					                                    <option value="AHKS">安徽快3</option>
					                                    <option value="HBKS">湖北快3</option>
					                                    <option value="JLKS">吉林快3</option>
					                                    <option value="BJKS">北京快3</option>
					                                    <option value="GXKS">广西快3</option>
					                                    <option value="GSKS">甘肃快3</option>
					                                    <option value="SHKS">上海快3</option>
					                                    
					                                    <option value="FJSYXW">福彩3D</option>
					                                    <option value="LHC">六合彩</option>
					                                    <option value="XYEB">幸运28</option>
				                                   </select>--%>
					                           </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label nopadding" style="padding-top:7px!important;">类型：</label>
                                    <div class="col-sm-8 radio i-checks">
                                        <label>
                                            <input type="radio" value="0" name="a" class="b" checked="checked"> <i></i>正式
                                        </label>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <label>
                                            <input type="radio" value="1" name="a" class="b"> <i></i>备用
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group text-right">
                                    <button type="button" class="btn btn-outline btn-default"  onclick="interfaceCheckPage.queryInterfaceData()"><i class="fa fa-search"></i>&nbsp;查 询</button></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12" style="margin: 0 12px;">
                                <div class="form-group">
                                    <textarea id="ccomment" rows="20" name="comment" class="form-control" required="" aria-required="true"></textarea>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
 <!-- js -->
 <script type="text/javascript" src="<%=path%>/admin/interface_check/js/interface_check.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
 <!-- dwr -->
 <script type='text/javascript' src='<%=path%>/dwr/interface/managerOpenCodeApiAction.js'></script>
</body>
</html>

