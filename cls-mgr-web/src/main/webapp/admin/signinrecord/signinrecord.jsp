<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>签到记录</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
<body >
<div class="animated fadeIn">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox ">
                <div class="ibox-content">
                    <form class="form-horizontal" id="activityQuery">
                    <c:if test="${admin.bizSystem == 'SUPER_SYSTEM'}">
                        <div class="row">
                            <div class="col-sm-3 biz" >
	                                <div class="input-group m-b">
	                                    <span class="input-group-addon">商户：</span>
	                                     <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
	                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">用户名</span>
                                    <input type="text" id='userName' value="" class="form-control"></div>
                                </div>
                            <div class="col-sm-5">
								<div class="input-group m-b">
									<span class="input-group-addon">签到时间：</span> 
									<input	class="form-control layer-date" placeholder="开始时间"	onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"	name="createdDateStart" id="createdDateStart"> 
									<span class="input-group-addon">到</span> 
									<input class="form-control layer-date" placeholder="结束日期"	onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"	name="createdDateEnd" id="createdDateEnd">
								</div>
							</div>
                            <div class="col-sm-2">
                                <div class="form-group nomargin text-right">
                                    <button type="button" class="btn btn-outline btn-default btn-kj biz" onclick="signinRecordPage.findBizSystem()"><i class="fa fa-search"></i>&nbsp;查 询</button>
                                    <!-- <button type="button" class="btn btn-outline btn-default btn-kj" onclick="signinRecordPage.editActivity(0)"><i class="fa fa-plus"></i>&nbsp;新增</button> -->
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <c:if test="${admin.bizSystem != 'SUPER_SYSTEM'}">
                    	<div class="row">
                            <div class="col-sm-3">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">用户名</span>
                                    <input type="text" id='userName' value="" class="form-control"></div>
                                </div>
                            <div class="col-sm-5">
								<div class="input-group m-b">
									<span class="input-group-addon">签到时间：</span> 
									<input	class="form-control layer-date" placeholder="开始时间"	onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"	name="createdDateStart" id="createdDateStart"> 
									<span class="input-group-addon">到</span> 
									<input class="form-control layer-date" placeholder="结束日期"	onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"	name="createdDateEnd" id="createdDateEnd">
								</div>
							</div>
                            <div class="col-sm-4">
                                <div class="form-group nomargin text-right">
                                    <button type="button" class="btn btn-outline btn-default btn-kj biz" onclick="signinRecordPage.findBizSystem()"><i class="fa fa-search"></i>&nbsp;查 询</button>
                                    <!-- <button type="button" class="btn btn-outline btn-default btn-kj" onclick="signinRecordPage.editActivity(0)"><i class="fa fa-plus"></i>&nbsp;新增</button> -->
                                </div>
                            </div>
                        </div>
                    </c:if>
                    </form>
                    <div class="jqGrid_wrapper">
                        <div class="ui-jqgrid ">
                            <div class="ui-jqgrid-view">
                                <div class="ui-jqgrid-hdiv">
                                    <div class="ui-jqgrid-hbox" style="padding-right:0">
                                        <table class="ui-jqgrid-htable ui-common-table table table-bordered" id="signinRecordTable">
                                            <thead>
                                                <tr class="ui-jqgrid-labels">
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">序号</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">商户</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">用户名</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">签到天数</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">签到获得金额</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">签到时间</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">操作</div></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                           </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
	<!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/signinrecord/js/signinrecord.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerSigninRecordAction.js'></script>
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerUserAction.js'></script>
</body>
</html>