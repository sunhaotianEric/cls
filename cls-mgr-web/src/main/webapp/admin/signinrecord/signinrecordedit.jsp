<%@ page language="java" contentType="text/html; charset=UTF-8" 
	import = "com.team.lottery.enums.EHelpType,com.team.lottery.system.SystemConfigConstant" 
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
	String id = request.getParameter("id");  //获取查询的ID
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>签到记录操作</title>
<jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeIn">
           <div class="row back-change">
               <div class="col-sm-12">
                   <div class="ibox ">
                       <div class="ibox-content">
                           <form class="form-horizontal">
                           	   <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
	                           	   <div class="form-group biz">
	                                   <label class="col-sm-2 control-label">商户：</label>
	                                   <div class="col-sm-10">
	                                       <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/></div>
	                               </div>
                               </c:if>
							   <div class="form-group">
                                   <label class="col-sm-2 control-label">用户名：</label>
                                   <div class="col-sm-10">
                                       <input type="text" value="" class="form-control" id="userName" size="50px"></div>
                               </div>    
                               <div class="form-group">
                                   <label class="col-sm-2 control-label">连续签到天数：</label>
                                   <div class="col-sm-10">
                                       <input type="text" value="" class="form-control" id="continueSigninDays" size="50px"></div>
                               </div> 
                               <div class="form-group">
                                   <label class="col-sm-2 control-label">签到获得金额：</label>
                                   <div class="col-sm-10">
                                       <input type="text" value="" class="form-control" id="money" size="50px"></div>
                               </div>        
		                        <div class="form-group">
		                            <div class="col-sm-12 ibox">
		                                <div class="row">
		                                    <div class="col-sm-6 text-center">
		                                        <button type="button" class="btn btn-w-m btn-white btn－tj" onclick="signinRecordEditPage.signinRecord()" >提 交</button>
		                                        <button type="button" class="btn btn-w-m btn-white" onclick="signinRecordEditPage.closelayer()">取消</button>
		                                   </div>
		                                </div>
		                            </div>
		                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/signinrecord/js/signinrecordedit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerSigninRecordAction.js'></script>
	<script type="text/javascript">
	signinRecordEditPage.param.id = '<%=id %>';
	</script>
</body>
</html>