function SigninRecordEditPage(){}
var signinRecordEditPage = new SigninRecordEditPage();

signinRecordEditPage.param = {
		bizSystem :null,
		id:null,
		userName:null,
		continueSigninDays:null,
		money:null
};

//分页参数
signinRecordEditPage.pageParam = {
    pageNo : 1,
    pageSize : defaultPageSize
};


$(document).ready(function() {
	if(signinRecordEditPage.param.id!=null&&signinRecordEditPage.param.id>0){
		signinRecordEditPage.getSigninRecordByid();
	}
});

SigninRecordEditPage.prototype.getSigninRecordByid=function(){
	managerSigninRecordAction.querySigninRecordByid(signinRecordEditPage.param.id,function(r){
		if(r[0]=="ok"){
			$("#bizSystem").val(r[1].bizSystem);
			$("#userName").val(r[1].userName);
			$("#continueSigninDays").val(r[1].continueSigninDays);
			$("#money").val(r[1].money);
			$("#bizSystem").attr("disabled","disabled");
			$("#userName").attr("disabled","disabled");
		}else{
			swal("查询失败");
		}
	});
}
SigninRecordEditPage.prototype.signinRecord=function(){
		signinRecordEditPage.param.continueSigninDays= $("#continueSigninDays").val();
		signinRecordEditPage.param.money=$("#money").val();
		if(signinRecordEditPage.param.continueSigninDays==null||signinRecordEditPage.param.continueSigninDays==""){
			swal("连续签到天数不能为空");
			return;
		}
		if(isNaN(signinRecordEditPage.param.continueSigninDays)){
			swal("连续签到天数必须为数字");
			return;
		}
		if(signinRecordEditPage.param.money==null||signinRecordEditPage.param.money==""){
			swal("签到获取金额不能为空");
			return;
		}
		if(isNaN(signinRecordEditPage.param.money)){
			swal("签到获得金额必须为数字");
			return;
		}
		if(signinRecordEditPage.param.id!=null&&signinRecordEditPage.param.id>0){
			managerSigninRecordAction.updateSigninRecordById(signinRecordEditPage.param,function(r){
				if(r[0]=="ok"){
					swal({
				           title: "修改成功",
				           text: "您已经修改了这条签到记录。",
				           type: "success"
				       },
				       function() {
				    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
			               parent.signinRecordPage.closeLayer(index);
				   	    });
				}else{
					swal("修改失败");
				}
			});
		}else{
			signinRecordEditPage.param.bizSystem=$("#bizSystem").val();
			signinRecordEditPage.param.userName=$("#userName").val();
			managerSigninRecordAction.savaSigninRecord(signinRecordEditPage.param,function(r){
				if(r[0]=="ok"){
					swal({
				           title: "新增成功",
				           text: "您已经保存了这条签到记录。",
				           type: "success"
				       },
				       function() {
				    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
			               parent.signinRecordPage.closeLayer(index);
				   	  });
				}else{
					swal(r[1]);
				}
			});
		}
}

SigninRecordEditPage.prototype.closelayer=function(){
	var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
    parent.signinRecordPage.closeLayer(index);
}