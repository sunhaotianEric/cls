function SigninRecordPage(){}
var signinRecordPage = new SigninRecordPage();

signinRecordPage.param = {
		bizSystem : null,
		userName : null,
		createdDateStart : null,
		createdDateEnd : null
};

//分页参数
signinRecordPage.pageParam = {
    pageNo : 1,
    pageSize : defaultPageSize
};


$(document).ready(function() {
	var today = new Date();
	var start = today.setHours(0, 0, 0, 0);
	var end = today.setHours(23, 59, 59, 999);
	$("#createdDateStart").val(new Date(start).format("yyyy-MM-dd hh:mm:ss"));
	$("#createdDateEnd").val(new Date(end).format("yyyy-MM-dd hh:mm:ss") );
	signinRecordPage.param.createdDateStart = start;
	signinRecordPage.param.createdDateEnd = end;
	signinRecordPage.initTableData();
});

SigninRecordPage.prototype.initTableData = function(){
	signinRecordPage.table =  $('#signinRecordTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"scrollX": true,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			signinRecordPage.pageParam.pageSize = data.length;//页面显示记录条数
			signinRecordPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
			managerSigninRecordAction.querySigninRecord(signinRecordPage.param,signinRecordPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				}; 
				
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询签到记录失败.");
				}
				callback(returnData);
		    });
		},
		"columns": [
		            {"data":null},
		            {"data":"bizSystemName","bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {
		            	"data": "userName",
		            	render: function(data, type, row, meta) {
		            		return "<a href='javascript:void(0)' style='color:blue;text-decoration:underline;'  onclick='commonPage.showUserMsgPage(\""+data+"\","+row.userId+")'>"+ row.userName +"</a>";
	            		}
		            },
		            {"data":"continueSigninDays"},
		            {"data":"money"},
		            {"data":"signinTimeStr"},
		            {"data":"id",
		            	 render: function(data, type, row, meta) {
		            		 var str = "";
	                		 str += "<a class='btn btn-info btn-sm btn-bj' onclick='signinRecordPage.editActivity("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>"
	                		 /*str +=	"<a class='btn btn-danger btn-sm btn-del' onclick='signinRecordPage.delActivity("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";*/
	                		 return str;
	                	 },"bVisible" : false
		            },
		      ]
	}).api();
}

SigninRecordPage.prototype.findBizSystem=function(){
	var bizSystem=$("#bizSystem").val();
	var userName = $("#userName").val();
	var createdDateStart=$("#createdDateStart").val();
	var createdDateEnd=$("#createdDateEnd").val();
	if(bizSystem==null||bizSystem==""){
		signinRecordPage.param.bizSystem=null;
	}else{
		signinRecordPage.param.bizSystem=bizSystem;
	}
	if(userName==null||userName==""){
		signinRecordPage.param.userName=null;
	}else{
		signinRecordPage.param.userName=userName;
	}
	if(createdDateStart==null||createdDateStart==""){
		signinRecordPage.param.createdDateStart=null;
	}else{
		signinRecordPage.param.createdDateStart=new Date(createdDateStart);
	}
	if(createdDateEnd==null||createdDateEnd==""){
		signinRecordPage.param.createdDateEnd=null;
	}else{
		signinRecordPage.param.createdDateEnd=new Date(createdDateEnd);
	}
	signinRecordPage.table.ajax.reload();
}

/*SigninRecordPage.prototype.editActivity=function(id){
		 layer.open({
		         type: 2,
		         title: '删除签到记录',
		         maxmin: false,
		         shadeClose: true,
		         //点击遮罩关闭层
		         area: ['600px', '420px'],
		         content: contextPath + "/managerzaizst/signinrecord/signinrecordedit.html?id="+id,   
		         cancel: function(index){
		        	 signinRecordPage.findBizSystem();
		       }
		  });
}*/

SigninRecordPage.prototype.delActivity=function(id){
	   swal({
		     title: "您确定要该条签到记录吗？",
		     text: "请谨慎操作！",
		     type: "warning",
		     showCancelButton: true,
		     confirmButtonColor: "#DD6B55",
		     cancelButtonText: "取消",
		     confirmButtonText: "确定",
		     closeOnConfirm: false
		 },
		 function() {
			 managerSigninRecordAction.delSigninRecordByid(id,function(r){
					if (r[0] == "ok") {
						swal({ title: "提示", text: "操作成功！",type: "success"});
						signinRecordPage.findBizSystem();
					}else if(r[0] == "error"){
						swal(r[1]);
					}else{
						swal("操作失败.");
					}
			    });
		 });
}
SigninRecordPage.prototype.closeLayer=function(index){
	layer.close(index);
	signinRecordPage.findBizSystem(); //查询奖金配置数据
}