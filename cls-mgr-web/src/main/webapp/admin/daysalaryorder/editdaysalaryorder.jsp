<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
    String id = request.getParameter("id")==null?"0":request.getParameter("id"); 
%>
<!DOCTYPE html>
<html>  
    <head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>日工资编辑</title>
    <link rel="shortcut icon" href="favicon.ico">
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
   <body class="gray-bg">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row back-change">
                 <!-- <div class="ibox-content"> -->
                            <form class="form-horizontal" id="bonusOrderForm">
                            <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">商户：</label>
                                    <div class="col-sm-9">
                                    <input id="bizSystemName" name="bizSystemName" type="text"  class="form-control" readonly="readonly" />
                                    <input id="bizSystem" name="bizSystem" type="hidden"  />
                                    </div>
                                </div>
                                </c:if>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">用户名：</label>
                                    <div class="col-sm-9">
                                        <input id="id" name="id" type="hidden"  />
                                        <input id="userName" name="userName"  type="text" class="form-control" readonly="readonly">
                                     </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">发放者用户名：</label>
                                    <div class="col-sm-9">
                                       
                                        <input id="fromUserName" name="fromUserName"  type="text" class="form-control" readonly="readonly">
                                     </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">归属日期：</label>
                                    <div class="col-sm-9">
                                     <input id="belongDate" name="belongDate"  type="text" class="form-control" readonly="readonly">
                               
                                     </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">申请金额：</label>
                                    <div class="col-sm-9">
                                        
                                        <input id="money" name="money"  type="text" class="form-control" >
                                     </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">投注金额：</label>
                                    <div class="col-sm-9">
                                       
                                        <input id="lotteryMoney" name="lotteryMoney"  type="text" class="form-control">
                                     </div>
                                </div>
                                 
                                  
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">发放处理状态：</label>
                                    <div class="col-sm-9" >
                                    <%-- <cls:enmuSel name="releaseStatus"
											options="class:ipt form-control"
											className="com.team.lottery.enums.EDaySalaryOrderStatus"
											id="releaseStatus" /> --%>
											<input id="releaseStatusStr" name="releaseStatusStr"  type="text" class="form-control" readonly="readonly">
											<select class="form-control" id="releaseStatusSelect">
								                            <option value="NOT_RELEASE">尚未发放</option>
								                            <option value="NEEDNOT_RELEASE">不需发放</option>
							                </select>
											<input id="releaseStatus" name="releaseStatus"  type="hidden">
                                    </div>
                                    
                                </div>
                               
                                  <div class="col-sm-12">
					                   
					                        <div class="col-sm-6 text-center">
					                            <button type="button" class="btn btn-w-m btn-white" onclick="editDaySaralyOrderPage.saveData()">提 交</button></div>
					                            <div class="col-sm-6 text-center">
					                            <!-- <button type="button" class="btn btn-w-m btn-white">重 置</button> -->
					                            <input type="reset" class="btn btn-w-m btn-white" value="重 置"/>
					                            </div>
					                   
					               </div>
                            </form>
                   <!--  </div> -->
              
            </div>
        </div>
    <script type="text/javascript" src="<%=path%>/admin/daysalaryorder/js/editdaysalaryorder.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerDaySaralyOrderAction.js'></script>
    <script type="text/javascript">
    editDaySaralyOrderPage.param.id = <%=id%>;
	</script>
</body>
</html>

