function DaySaralyOrderPage(){}
var daySaralyOrderPage = new DaySaralyOrderPage();

daySaralyOrderPage.param = {
   	
};

daySaralyOrderPage.admin = {
	   	
};
/**
 * 查询参数
 */
daySaralyOrderPage.queryParam = {
		bizSystem:null,
		userName:null,
		createdDateStart:null,
		createdDateEnd:null,
		releaseStatus:null

};

//分页参数
daySaralyOrderPage.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize
};


$(document).ready(function() {

	//daySaralyOrderPage.initdate();
	daySaralyOrderPage.initTableData(); //初始化查询数据
	
});





/**
 * 初始化列表数据
 */
DaySaralyOrderPage.prototype.initTableData = function() {
	//daySaralyOrderPage.queryParam = getFormObj($("#queryForm"));
	daySaralyOrderPage.table = $('#daySalaryOrderTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"scrollX": true,
		"iDisplayLength" : defaultPageSize,
		"ajax":function (data, callback, settings) {
			daySaralyOrderPage.pageParam.pageSize = data.length;//页面显示记录条数
			daySaralyOrderPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerDaySaralyOrderAction.selectSalaryOrderByUserList(daySaralyOrderPage.queryParam,daySaralyOrderPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": "bizSystemName","bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "userName",render: function(data, type, row, meta) {	
	            		return "<a href='javascript:void(0)' style='color:blue;text-decoration:underline;'  onclick='commonPage.showUserMsgPage(this,"+row.userId+")'>"+ row.userName +"</a>";
            		}},
		            {"data": "fromUserName",render: function(data, type, row, meta) {	
	            		return "<a href='javascript:void(0)' style='color:blue;text-decoration:underline;'  onclick='commonPage.showUserMsgPage(this,"+row.fromUserId+")'>"+ row.fromUserName +"</a>";
            		}},
		            {"data": "money"},
		            {"data": "lotteryMoney"},
		            {"data": "remark"},
		            {"data": "belongDateStr"},
		            {"data": "releaseDateStr"},
		            {
		            	"data": "releaseStatus",
		            	render: function(data, type, row, meta) {
		            		if(row.releaseStatus == "NOT_RELEASE"){
		            			return '<font class="cp-red">'+ row.releaseStatusStr +"</font>";
		            		}else if(row.releaseStatus == "RELEASED" || row.releaseStatus == "FORCE_RELEASED"){
		            			return '<font class="cp-green">'+ row.releaseStatusStr +"</font>";
		            		}else if(row.releaseStatus == "NEEDNOT_RELEASE"){
		            			return '<font class="cp-yellow">'+ row.releaseStatusStr +"</font>";
		            		}else{
		            			return row.releaseStatusStr ;
		            		}
	            		}
		            	
		            },
		            {
	                	 "data": "id",
	                	 render: function(data, type, row, meta) {
	                		 var str="";
	                		 //发放状态为尚未发放或者不需发放才有编辑按钮
	                		 if(row.releaseStatus=='NOT_RELEASE' || row.releaseStatus=='NEEDNOT_RELEASE'){
	                			 str +="<a class='btn btn-info btn-sm btn-bj' onclick='daySaralyOrderPage.editDaySaralyOrder("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;";
	                		 }
	                		//发放状态为尚未发放才能发日工资
	                		 if(row.releaseStatus=='NOT_RELEASE'){
	                			 str += "<a class='btn btn-info btn-sm btn-bj' onclick='daySaralyOrderPage.releaseDaySaralyOrder(\""+row.id+"\",\""+row.userName+"\")'><i class='fa fa-pencil'></i>&nbsp;发放工资 </a>&nbsp;";
		                	  }
	                		 return str;
	                	 }
	                }
	            ]
	}).api(); 
}




/**
 * 查询数据
 */
DaySaralyOrderPage.prototype.findDaySaralyOrder = function(){
	var releaseStatus = $("#releaseStatus").val();
	var userName = $("#userName").val();
	var createdDateStart = $("#createdDateStart").val();
	var createdDateEnd = $("#createdDateEnd").val();
	var bizSystem = $("#bizSystem").val();
	if(userName == ""){
		daySaralyOrderPage.queryParam.userName = null;
	}else{
		daySaralyOrderPage.queryParam.userName = userName;
	}
	if(releaseStatus == ""){
		daySaralyOrderPage.queryParam.releaseStatus = null;
	}else{
		daySaralyOrderPage.queryParam.releaseStatus = releaseStatus;
	}
	if(createdDateStart == ""){
		daySaralyOrderPage.queryParam.createdDateStart = null;
	}else{
		daySaralyOrderPage.queryParam.createdDateStart = new Date(createdDateStart);
	}
	if(createdDateEnd == ""){
		daySaralyOrderPage.queryParam.createdDateEnd = null;
	}else{
		daySaralyOrderPage.queryParam.createdDateEnd = new Date(createdDateEnd);
	}
	if(bizSystem == ""){
		daySaralyOrderPage.queryParam.bizSystem = null;
	}else{
		daySaralyOrderPage.queryParam.bizSystem = bizSystem;
	}
	daySaralyOrderPage.table.ajax.reload();
};

/**
 * 发放
 */
DaySaralyOrderPage.prototype.releaseDaySaralyOrder = function(id,userName){
	
	 swal({
         title: "您确定要给"+userName+"发放工资吗",
         text: "请谨慎操作！",
         type: "warning",
         showCancelButton: true,
         confirmButtonColor: "#DD6B55",
         cancelButtonText: "取消",
         confirmButtonText: "确定",
         closeOnConfirm: false
     },
     function() {
	   managerDaySaralyOrderAction.releaseDaysalary(id,function(r){
	    		if (r[0] != null && r[0] == "ok") {
	    			swal("发放工资成功！");
	    			daySaralyOrderPage.findDaySaralyOrder();
	    		}else if(r[0] != null && r[0] == "error"){
	    			swal(r[1]);
	    		}else{
	    			swal("查询数据失败.");
	    		}
	        });  
      }); 
	
};

DaySaralyOrderPage.prototype.editDaySaralyOrder=function(id)
{
	  layer.open({
          type: 2,
          title: '日工资编辑',
          maxmin: false,
          shadeClose: true,
          //点击遮罩关闭层
          area: ['70%', '80%'],
          content: contextPath + "/managerzaizst/daysalaryorder/"+id+"/editdaysalaryorder.html",
          cancel: function(index){ 
        	  daySaralyOrderPage.findDaySaralyOrder();
       	   }
      });
}

DaySaralyOrderPage.prototype.closeLayer=function(index){
	layer.close(index);
	daySaralyOrderPage.findDaySaralyOrder();
}