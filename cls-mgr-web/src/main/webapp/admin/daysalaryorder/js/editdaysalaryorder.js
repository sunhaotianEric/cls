function EditDaySaralyOrderPage(){}
var editDaySaralyOrderPage = new EditDaySaralyOrderPage();

editDaySaralyOrderPage.param = {
   	
};

$(document).ready(function() {
	
	var daySaralyOrderId = editDaySaralyOrderPage.param.id;  //获取查询ID
	if(daySaralyOrderId != null){
		editDaySaralyOrderPage.editDaySaralyOrder(daySaralyOrderId);
	}
	$("#releaseStatusSelect").change(function(){
		$("#releaseStatus").val($(this).val());
	})
});


/**
 * 保存更新信息
 */
EditDaySaralyOrderPage.prototype.saveData = function(){
	var updateParm={};
	/*var */
	var bizSystem=$("#bizSystem").val();
	var id=$("#id").val();
	var userName=$("#userName").val();
	var fromUserName=$("#fromUserName").val();
	/*var belongDate=$("#belongDate").val();*/
	var money=$("#money").val();
	var lotteryMoney=$("#lotteryMoney").val();
	var releaseStatus=$("#releaseStatus").val();
	if (money==null || money.trim()=="") {
		swal("请输入申请金额!");
		$("#lowestValue").focus();
		return;
	}
	if( isNaN(money)){
		swal("申请金额必须为数字!");
		$("#money").focus();
		return;
	}
	if (lotteryMoney==null || lotteryMoney.trim()=="") {
		swal("请输入投注金额!");
		$("#lotteryMoney").focus();
		return;
	}
	if( isNaN(lotteryMoney)){
		swal("投注金额必须为数字!");
		$("#lotteryMoney").focus();
		return;
	}
	updateParm.id=id;
	updateParm.bizSystem=bizSystem;
	updateParm.userName=userName;
	updateParm.fromUserName=fromUserName;
	/*updateParm.belongDate=new Date(belongDate);*/
	updateParm.releaseStatus=releaseStatus;
	updateParm.money=money;
	updateParm.lotteryMoney=lotteryMoney;
	 managerDaySaralyOrderAction.saveOrUpdateDaySaralyOrder(updateParm,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.daySaralyOrderPage.closeLayer(index);
		   	    }); 
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("保存信息失败.");
		}
     });
};


/**
 * 赋值信息
 */
EditDaySaralyOrderPage.prototype.editDaySaralyOrder = function(id){
	managerDaySaralyOrderAction.getDaySaralyOrderById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			
			$("#id").val(r[1].id);
			$("#bizSystemName").val(r[1].bizSystemName);
			$("#bizSystem").val(r[1].bizSystem);
			$("#userName").val(r[1].userName);
			$("#fromUserName").val(r[1].fromUserName);
			$("#belongDate").val(r[1].belongDateStr);
			$("#lotteryMoney").val(r[1].lotteryMoney);
			$("#money").val(r[1].money);
			if(r[1].releaseStatus=="NOT_RELEASE" || r[1].releaseStatus=="NEEDNOT_RELEASE")
				{
				$("#releaseStatusSelect").show();
				$("#releaseStatusSelect").val(r[1].releaseStatus);
				$("#releaseStatus").val(r[1].releaseStatus);
				$("#releaseStatusStr").val(r[1].releaseStatusStr);
				$("#releaseStatusStr").hide();
				}
			else
				{
				$("#releaseStatusStr").show();
				$("#releaseStatusSelect").hide();
				$("#releaseStatus").val(r[1].releaseStatus);
				$("#releaseStatusStr").val(r[1].releaseStatusStr);
				}
			
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取信息失败.");
		}
    });
};

