function RechargeorderPage(){}
var rechargeorderPage = new RechargeorderPage();

rechargeorderPage.param = {
	orderList : null,
	operatePayId : null,
	refreshKey : null
};

/**
 * 查询参数
 */
rechargeorderPage.queryParam = {
	userName : null,
	statuss : null,
	payType : null,
	operateType : 'WITHDRAW',
	createdDateStart : null,
	createdDateEnd : null,
	serialNumber : null
};

//分页参数
rechargeorderPage.pageParam = {
		    pageNo : 1,
		    pageSize : defaultPageSize
};

$(document).ready(function() {
	
	$("#createdDateStart").val(getClsStartTimeZero().format("yyyy-MM-dd hh:mm:ss"));
	$("#createdDateEnd").val(getClsEndTimeZero().format("yyyy-MM-dd hh:mm:ss"));
	/*rechargeorderPage.getallbizSystem('bizSystem');*/
	//rechargeorderPage.getAllRechargeOrdersForQuery(); //根据条件,查找资金明细
	rechargeorderPage.initTableData();
	//10秒钟刷新一次记录
	//rechargeorderPage.param.refreshKey = window.setInterval("rechargeorderPage.getAllRechargeOrdersForQuery()",10000);
});
	
/**
 * 加载所有的登陆日志数据
 */
RechargeorderPage.prototype.getAllRechargeOrdersForQuery = function(dateRange){
	if(dateRange != undefined && dateRange != null && dateRange != ''){
		commonPage.setDateRangeTimeZero(dateRange, "createdDateStart", "createdDateEnd",2);	
	}
	var rechargeStatus = $("#rechargeStatus").val();
	var userName = $("#userName").val();
	var serialNumber = $("#serialNumber").val();
	var createdDateStart = $("#createdDateStart").val();
	var createdDateEnd = $("#createdDateEnd").val();
	var bizSystem = $("#bizSystem").val();
	if(userName == ""){
		rechargeorderPage.queryParam.userName = null;
	}else{
		rechargeorderPage.queryParam.userName = userName;
	}
	if(rechargeStatus == ""){
		rechargeorderPage.queryParam.statuss = null;
	}else{
		rechargeorderPage.queryParam.statuss = rechargeStatus;
	}
	if(serialNumber == ""){
		rechargeorderPage.queryParam.serialNumber = null;
	}else{
		rechargeorderPage.queryParam.serialNumber = serialNumber;
	}
	if(createdDateStart == ""){
		rechargeorderPage.queryParam.createdDateStart = null;
	}else{
		rechargeorderPage.queryParam.createdDateStart = new Date(createdDateStart);
	}
	if(createdDateEnd == ""){
		rechargeorderPage.queryParam.createdDateEnd = null;
	}else{
		rechargeorderPage.queryParam.createdDateEnd = new Date(createdDateEnd);
	}
	if(bizSystem == ""){
		rechargeorderPage.queryParam.bizSystem = null;
	}else{
		rechargeorderPage.queryParam.bizSystem = bizSystem;
	}
	rechargeorderPage.queryParam.operateType ='WITHDRAW';
	rechargeorderPage.table.ajax.reload();
};
RechargeorderPage.prototype.initTableData = function(){

	rechargeorderPage.queryParam = getFormObj($("#withdrawreportForm"));
	rechargeorderPage.queryParam.operateType ='WITHDRAW';
	rechargeorderPage.table = $('#withdrawreportTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"fnDrawCallback": function (oSettings) {  
			rechargeorderPage.refreshRechargeOrdersPages(rechargeorderPage.totalRechargeOrder);
	        },
		"ajax":function (data, callback, settings) {
			rechargeorderPage.pageParam.pageSize = data.length;//页面显示记录条数
			rechargeorderPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerWithDrawOrderAction.getAllWithDrawOrdersByQueryForReport(rechargeorderPage.queryParam,rechargeorderPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
					rechargeorderPage.totalRechargeOrder=r[2];
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询提现数据失败.");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": "bizSystemName","bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "userName"},
		            {"data": "serialNumber"},
		            {"data": "applyValue"},
		            {"data": "accountValue"},
		            {"data": "feeValue"},
		            {"data": "createdDateStr"},
		            {"data": "statusStr",render: function(data, type, row, meta) {
 		            	var value=row.dealStatus;
 		            	if(value=="DEALING")
 		            		{
 		            		return "<font color='blue'>"+data+"</font>" ;
 		            		}
 		            	else if(value=="WITHDRAW_SUCCESS")
 		            		{
 		            		return "<font color='green'>"+data+"</font>" ;
 		            		}
 		            	else if(value=="WITHDRAW_FAIL")
 		            		{
 		            		return "<font color='red'>"+data+"</font>" ;
 		            		}
 		            	else if(value=="LOCKED")
 		            		{
 		            		return "<font color='purple'>"+data+"</font>" ;
 		            		}
                		 
                  }}
	            ]
	}).api(); 
};
/**
 * 查找资金明细
 */
/*RechargeorderPage.prototype.queryConditionRechargeOrders = function(queryParam,pageNo){
	managerRechargeWithDrawOrderAction.getAllRechargeOrdersByQueryForReport(queryParam,pageNo,function(r){
		if (r[0] != null && r[0] == "ok") {
			rechargeorderPage.refreshRechargeOrdersPages(r[1],r[2]);
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询所有的资金类型失败.");
		}
    });
};*/

/**
 * 刷新列表数据
 */
RechargeorderPage.prototype.refreshRechargeOrdersPages = function(totalRechargeOrder){
	var rechargeOrderListObj = $("#withdrawreportTable tbody");
	//rechargeOrderListObj.html("");
	var str ="";
	/*var str = "";
    var rechargeOrders = page;
    
    //记录数据显示
	for(var i = 0 ; i < rechargeOrders.length; i++){
		var rechargeOrder = rechargeOrders[i];
		str += "<tr>";
		str += "  <td><a href='javascript:void(0)' style='color:blue;text-decoration:underline;'  onclick='commonPage.showUserMsgPage(this,"+rechargeOrder.userId+")'>"+ rechargeOrder.userName +"</a></td>";
		if(currentUser.bizSystem=='SUPER_SYSTEM'){
			str += "  <td>"+ rechargeOrder.bizSystemName +"</td>";
		}
		str += "  <td>"+ rechargeOrder.serialNumber +"</td>";
		str += "  <td>"+ rechargeOrder.applyValue.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ rechargeOrder.accountValue.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ rechargeOrder.feeValue.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ rechargeOrder.createdDateStr +"</td>";
		str += "  <td>"+ rechargeOrder.statusStr +"</td>";
		str += "</tr>";
		rechargeOrderListObj.append(str);
		str = "";
	}
	*/
	if(isNotEmpty(totalRechargeOrder)) {
		str += "<tr>";
		str += "  <td>总计</td>";
		str += "  <td></td>";
		if(currentUser.bizSystem=='SUPER_SYSTEM'){
			str += "  <td></td>";
		}
		str += "  <td style='color:red;'>"+ totalRechargeOrder.applyValue.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td style='color:red;'>"+ totalRechargeOrder.accountValue.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td style='color:red;'>"+ totalRechargeOrder.feeValue.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td></td>";
		str += "  <td></td>";
		str += "</tr>";
		rechargeOrderListObj.append(str);
	}
	
	/*//记录统计
	var pageMsg = $("#pageMsg");
	var pageStr = "";
	pageStr += "<th colspan='100'>";
	pageStr += "  当前第" + page.pageNo + "页/共" + page.totalPageNum +"页&nbsp;&nbsp;共" + page.totalRecNum +"条记录&nbsp;&nbsp;<a href='javascript:void(0)'  onclick='rechargeorderPage.pageQuery(1)'>首页</a>&nbsp;";
	pageStr += "<a href='javascript:void(0)' onclick='rechargeorderPage.pageParam.pageNo--;rechargeorderPage.pageQuery(rechargeorderPage.pageParam.pageNo)'>上一页</a>&nbsp;";
	pageStr += "<a href='javascript:void(0)' onclick='rechargeorderPage.pageParam.pageNo++;rechargeorderPage.pageQuery(rechargeorderPage.pageParam.pageNo)'>下一页</a>&nbsp;";
	pageStr += "<a href='javascript:void(0)' onclick='rechargeorderPage.pageQuery("+ page.totalPageNum +")'>尾页</a>";
	
	var skipHtmlStr = "";
	if($("#pageSkip").html() != null){
		$("#pageSkip").remove();
	}
    skipHtmlStr += "<span id='pageSkip'>";
    skipHtmlStr += "&nbsp;&nbsp;跳转至";
	skipHtmlStr += "<select id='currentPageChoose' class='ipt' onchange='rechargeorderPage.pageParam.pageNo = this.value;rechargeorderPage.pageQuery(rechargeorderPage.pageParam.pageNo)'  style='width:auto;'>";
	for(var i = 1; i <= page.totalPageNum; i++){
		skipHtmlStr += "<option value='"+ i +"'>"+ i +"</option>";
	}
	skipHtmlStr += "</select>";
	skipHtmlStr += "</span>";	
	pageStr += skipHtmlStr;
	pageStr += "</th>";
	pageMsg.html(pageStr);
	
	$("#currentPageChoose").find("option[value='"+ page.pageNo +"']").attr("selected",true);  //显示当前页码
	rechargeorderPage.pageParam.pageMaxNo = page.totalPageNum;  //标识最大的页数
*/	
};

/**
 * 根据条件查找对应页
 */
/*RechargeorderPage.prototype.pageQuery = function(pageNo){
	if(pageNo < 1){
		rechargeorderPage.pageParam.pageNo = 1;
	}else if(pageNo > rechargeorderPage.pageParam.pageMaxNo){
		rechargeorderPage.pageParam.pageNo = rechargeorderPage.pageParam.pageMaxNo;
	}else{
		rechargeorderPage.pageParam.pageNo = pageNo;
	}
	//如果页码小于等于0，则不进行分页查找
	if(rechargeorderPage.pageParam.pageNo <= 0){
		return;
	}
	
	if(rechargeorderPage.pageParam.queryMethodParam == 'getAllRechargeOrdersForQuery'){
		rechargeorderPage.getAllRechargeOrdersForQuery();
	}else{
		alert("类型对应的查找方法未找到.");
	}
};*/
/*RechargeorderPage.prototype.getallbizSystem=function (id)
{
	dwr.engine.setAsync(false); 
	var selDom = $("#"+id);
	managerRechargeWithDrawOrderAction.getAllEnableBizSystem(function(r){
		if (r[0] != null && r[0] == "ok") {
			bizsystems=r[1];
			for(var i=0;i<bizsystems.length;i++)
			{
				var bs=bizsystems[i];
				
				selDom.append("<option value='"+bs.bizSystem+"'>"+bs.bizSystemName+"</option>");
			}
		}
	});
};*/