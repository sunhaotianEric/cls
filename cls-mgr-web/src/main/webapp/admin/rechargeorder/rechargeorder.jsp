<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant"%>
<%
	String path = request.getContextPath();
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>充值记录</title>
	<jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
<body>
	<div class="animated fadeIn">
		<div class="ibox-content ibox">
			<div class="row m-b-sm m-t-sm">
				<form class="form-horizontal" id="rechargeorderForm">
					<div class="row">
						<c:choose>
							<c:when test="${admin.bizSystem=='SUPER_SYSTEM'}">
								<div class="col-sm-3">
									<div class="input-group m-b">
										<span class="input-group-addon">商户</span>
										<cls:bizSel name="bizSystem" id="bizSystem"
											options="class:ipt form-control" />
									</div>
								</div>
								<div class="col-sm-3">
									<div class="input-group m-b">
										<span class="input-group-addon">充值状态</span> <select
											id="rechargeStatus" id="rechargeStatus"
											class="ipt form-control">
											<option value="">==请选择==</option>
											<option value="PAYMENT">支付中</option>
											<option value="PAYMENT_SUCCESS">支付成功</option>
											<option value="PAYMENT_CLOSE">订单关闭</option>
										</select>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="input-group m-b">
										<span class="input-group-addon">用户名</span> <input type="text"
											value="" name="userName" id="userName" class="form-control">
									</div>
								</div>
								<div class="col-sm-3">
									<div class="input-group m-b">
										<span class="input-group-addon">订单号</span> <input type="text"
											value="" name="serialNumber" id="serialNumber"
											class="form-control">
									</div>
								</div>
							</c:when>
							<c:otherwise>
								<div class="col-sm-4">
									<div class="input-group m-b">
										<span class="input-group-addon">充值状态</span> <select
											id="rechargeStatus" id="rechargeStatus"
											class="ipt form-control">
											<option value="">==请选择==</option>
											<option value="PAYMENT">支付中</option>
											<option value="PAYMENT_SUCCESS">支付成功</option>
											<option value="PAYMENT_CLOSE">订单关闭</option>
										</select>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="input-group m-b">
										<span class="input-group-addon">用户名</span> <input type="text"
											value="" name="userName" id="userName" class="form-control">
									</div>
								</div>
								<div class="col-sm-4">
									<div class="input-group m-b">
										<span class="input-group-addon">订单号</span> <input type="text"
											value="" name="serialNumber" id="serialNumber"
											class="form-control">
									</div>
								</div>
								<div class="col-sm-2"></div>
							</c:otherwise>
						</c:choose>


					</div>
					<div class="row">
						<div class="col-sm-3">
							<div class="input-group m-b">
								<span class="input-group-addon">刷新时间</span> <select
									id="refreshTime" name="refreshTime" class="ipt form-control">
									<option value="5">5秒</option>
									<option value="10">10秒</option>
									<option value="15">15秒</option>
									<option value="20">20秒</option>
								</select>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="input-group m-b">
								<span class="input-group-addon">订单类型</span>
								<cls:enmuSel name="refType"
									className="com.team.lottery.enums.EFundRefType"
									options="class:ipt form-control" id="refType" />
							</div>
						</div>
						<div class="col-sm-3">
							<div class="input-group m-b">
								<span class="input-group-addon">充值金额</span> <input type="text"
									value="" name="applyValue" id="applyValue" class="form-control">
							</div>
						</div>
						<div class="col-sm-3">
							<div class="input-group m-b">
								<span class="input-group-addon">声音开关</span> <select
									id="RechargeSoundControl" id="RechargeSoundControl"
									class="ipt form-control">
									<option value="1">开启</option>
									<option value="0">关闭</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4">
							<div class="input-group m-b">
								<span class="input-group-addon">充值时间</span> <input
									class="form-control layer-date" placeholder="开始时间"
									onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"
									name="createdDateStart" id="createdDateStart"> <span
									class="input-group-addon">到</span> <input
									class="form-control layer-date" placeholder="结束日期"
									onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"
									name="createdDateEnd" id="createdDateEnd">
							</div>
						</div>
						<div class="col-sm-3">
									<div class="input-group m-b">
										<span class="input-group-addon">订单来源</span> <select
											id="fromType" id="fromType"
											class="ipt form-control">
											<option value="">==请选择==</option>
											<option value="PC">PC</option>
											<option value="MOBILE">手机WEB</option>
											<option value="APP">APP</option>
										</select>
									</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group nomargin text-left">
								<button type="button" class="btn btn-xs btn-info"
									onclick="rechargeorderPage.findRechargeorder(1)">今 天</button>
								<button type="button" class="btn btn-xs btn-danger"
									onclick="rechargeorderPage.findRechargeorder(-1)">昨 天</button>
								<button type="button" class="btn btn-xs btn-warning"
									onclick="rechargeorderPage.findRechargeorder(-7)">最近7天</button>
								<br />
								<button type="button" class="btn btn-xs btn-success"
									onclick="rechargeorderPage.findRechargeorder(30)">本 月</button>
								<button type="button" class="btn btn-xs btn-primary"
									onclick="rechargeorderPage.findRechargeorder(-30)">上 月</button>
							</div>
						</div>
						<div class="col-sm-1">
							<div class="form-group nomargin text-right">
								<button type="button" class="btn btn-outline btn-default"
									onclick="rechargeorderPage.findRechargeorder()">
									<i class="fa fa-search"></i>&nbsp;查 询
								</button>
							</div>
						</div>
						<div class="col-sm-1">
							<div class="form-group nomargin text-right">
								<button type="button" class="btn btn-outline btn-default"
									onclick="rechargeorderPage.rechargeorderInfoExport()">
									<i class="fa fa-search"></i>&nbsp;导出数据
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="jqGrid_wrapper">
				<div class="ui-jqgrid ">
					<div class="ui-jqgrid-view ">
						<div class="ui-jqgrid-hdiv">
							<div class="ui-jqgrid-hbox" style="padding-right: 0">
								<table id="rechargeorderTable"
									class="ui-jqgrid-htable ui-common-table table table-bordered">
									<thead>
										<tr class="ui-jqgrid-labels">
											<th class="ui-th-column ui-th-ltr">商户</th>
											<th class="ui-th-column ui-th-ltr">订单号</th>
											<th class="ui-th-column ui-th-ltr">用户名</th>
											<th class="ui-th-column ui-th-ltr">充值金额</th>
											<th class="ui-th-column ui-th-ltr">到账金额</th>
											<th class="ui-th-column ui-th-ltr">充值时间</th>
											<th class="ui-th-column ui-th-ltr">状态</th>
											<th class="ui-th-column ui-th-ltr">操作</th>
											<th class="ui-th-column ui-th-ltr">充值方式</th>
											<th class="ui-th-column ui-th-ltr">订单类型</th>
											<th class="ui-th-column ui-th-ltr">订单来源</th>
											<th class="ui-th-column ui-th-ltr">附言</th>
											<th class="ui-th-column ui-th-ltr">收款人</th>
											<th class="ui-th-column ui-th-ltr">操作人</th>
											<th class="ui-th-column ui-th-ltr">操作时间</th>
											<!-- <th class="ui-th-column ui-th-ltr">充值赠送</th> -->
										</tr>
									</thead>

								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<audio controls=false id='voiceAutoMsg' style='display: none'>
		<!-- autoplay=true  loop=loop-->
		<source src="<%=path%>/resources/music/cz.mp3">
		<source src="<%=path%>/resources/music/cz.ogg">

	</audio>
	<!-- js -->
	<script type="text/javascript"
		src="<%=path%>/admin/rechargeorder/js/rechargeorder.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
	<!-- dwr -->
	<script type='text/javascript' src='<%=path%>/dwr/interface/managerRechargeOrderAction.js'></script>
	<script type='text/javascript' src='<%=path%>/dwr/interface/managerUserAction.js'></script>
</body>

</html>

