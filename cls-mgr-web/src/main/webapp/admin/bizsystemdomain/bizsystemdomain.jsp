<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>  
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>域名管理</title>
		<link rel="shortcut icon" href="favicon.ico">
        <jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
    <body>
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
                    <div class="ibox ">
                        <div class="ibox-content">
                            <div class="row m-b-sm m-t-sm">
                                <form class="form-horizontal" id="queryForm">
                                    <div class="row">
                                     <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                                        <div class="col-sm-2">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">商户</span>
                                                <cls:bizSel name="bizSystem" id="bizSystem" options="class:'ipt form-control'"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">域名地址</span>
                                                <input id="domainUrl" name="domainUrl" type="text" value="" class="form-control"></div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">域名类型</span>
                                                <select id="domainType" name="domainType" class="ipt form-control">
													<option value="" selected="selected">全部</option>
													<option value="PC">PC端</option>
													<option value="MOBIlE">手机web端</option>
													<option value="APP">APP</option>
												</select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">申请状态</span>
                                                <select id="applyStatus" name="applyStatus" class="ipt form-control">
													<option value="" selected="selected">全部</option>
													<option value="0">申请中</option>
													<option value="1">通过</option>
													<option value="2">不通过</option>
											     </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">启用状态</span>
							                     <select id="enable" name="enable" class="ipt form-control">
												    <option value="" selected="selected">全部</option>
												    <option value="1">启用</option>
												    <option value="0">禁用</option>
											     </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group  text-right">
                                                <button type="button" class="btn btn-outline btn-default btn-kj" onclick="bizSystemDomainPage.findBizSystemDomain()">
                                                    <i class="fa fa-search"></i>&nbsp;查 询</button>&nbsp;&nbsp;
                                                <button type="button" class="btn btn-outline btn-default btn-add" onclick="bizSystemDomainPage.addBizSystemDomain()">
                                                    <i class="fa fa-plus"></i>&nbsp;域名申请</button></div>
                                        </div>
                                        </c:if>
                                     <c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
                              
                                        <div class="col-sm-3">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">域名地址</span>
                                                <input id="domainUrl" name="domainUrl" type="text" value="" class="form-control"></div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">域名类型</span>
                                                <select id="domainType" name="domainType" class="ipt form-control">
													<option value="" selected="selected">全部</option>
													<option value="PC">PC端</option>
													<option value="MOBIlE">手机web端</option>
													<option value="APP">APP</option>
												</select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">申请状态</span>
                                                <select id="applyStatus" name="applyStatus" class="ipt form-control">
													<option value="" selected="selected">全部</option>
													<option value="0">申请中</option>
													<option value="1">通过</option>
													<option value="2">不通过</option>
											     </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">启用状态</span>
							                     <select id="enable" name="enable" class="ipt form-control">
												    <option value="" selected="selected">全部</option>
												    <option value="1">启用</option>
												    <option value="0">禁用</option>
											     </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group nomargin text-right">
                                                <button type="button" class="btn btn-outline btn-default btn-kj" onclick="bizSystemDomainPage.findBizSystemDomain()">
                                                    <i class="fa fa-search"></i>&nbsp;查 询</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <button type="button" class="btn btn-outline btn-default btn-add" onclick="bizSystemDomainPage.addBizSystemDomain()">
                                                    <i class="fa fa-plus"></i>&nbsp;域名申请</button></div>
                                        </div>
                                        </c:if>
                                        
                                    </div>
                                    <div class="row">
                                    	<div class="col-sm-12">
                                            <div class="form-group nomargin text-right">
                                                <button type="button" class="btn btn-w-m btn-white" onclick="bizSystemDomainPage.refreshCache()">刷新缓存</button>
                                            </div>    
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="jqGrid_wrapper">
                                <div class="ui-jqgrid ">
                                    <div class="ui-jqgrid-view">
                                        <div class="ui-jqgrid-hdiv">
                                            <div class="ui-jqgrid-hbox" style="padding-right:0">
                                                <table class="ui-jqgrid-htable ui-common-table table table-bordered" id="bizsystemdomainTable">
                                                    <thead>
                                                        <tr class="ui-jqgrid-labels">
                                                            <th class="ui-th-column ui-th-ltr">商户</th>
                                                            <th class="ui-th-column ui-th-ltr">域名地址</th>
                                                            <th class="ui-th-column ui-th-ltr">域名类型</th>
                                                            <th class="ui-th-column ui-th-ltr">申请状态</th>
                                                            <th class="ui-th-column ui-th-ltr">审核备注</th>
                                                            <th class="ui-th-column ui-th-ltr">首推域名</th>
                                                            <th class="ui-th-column ui-th-ltr">启用状态</th>
                                                            <th class="ui-th-column ui-th-ltr">创建时间</th>
	                                                        <th class="ui-th-column ui-th-ltr">更新时间</th>
                                                            <th class="ui-th-column ui-th-ltr">操作人</th>
                                                            <th class="ui-th-column ui-th-ltr">操作</th></tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!-- js -->
<script type="text/javascript"
	src="<%=path%>/admin/bizsystemdomain/js/bizsystemdomain.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript'
	src='<%=path%>/dwr/interface/managerBizSystemDomainAction.js'></script>
</body>
</html>

