function BizSystemDomainPage(){}
var bizSystemDomainPage = new BizSystemDomainPage();

bizSystemDomainPage.param = {
   	
};

bizSystemDomainPage.admin = {
	   	
};
/**
 * 查询参数
 */
bizSystemDomainPage.queryParam = {

};

//分页参数
bizSystemDomainPage.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize
 
};


$(document).ready(function() {
	bizSystemDomainPage.initTableData(); //初始化查询数据
	
});

/**
 * 加载所有
 */
BizSystemDomainPage.prototype.getAllbizSystemDomains = function(){
	
	bizSystemDomainPage.queryParam = {};
	bizSystemDomainPage.refreshbizSystemDomainPages(bizSystemDomainPage.queryParam,bizSystemDomainPage.pageParam.pageNo);
};

/**
 * 初始化列表数据
 */
BizSystemDomainPage.prototype.initTableData = function() {
	bizSystemDomainPage.queryParam = getFormObj($("#queryForm"));
	bizSystemDomainPage.table = $('#bizsystemdomainTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"scrollX": true,
		"iDisplayLength" : defaultPageSize,
//		"createdRow": function ( row, data, index ) {   //定义行样式
//			 if(data.firstFlag==1){
//			  $('td', row).css('font-weight',"").css("color","red");
//			 }         
//           },
		"ajax":function (data, callback, settings) {
			bizSystemDomainPage.pageParam.pageSize = data.length;//页面显示记录条数
			bizSystemDomainPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerBizSystemDomainAction.getAllBizSystemDomain(bizSystemDomainPage.queryParam,bizSystemDomainPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": "bizSystemName","bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "domainUrl"},
		            {
		            	"data": "domainType",
		            	render: function(data, type, row, meta) {
		            		if (data == 'PC') {
		            			return 'PC端';
		            		}else if(data == 'MOBILE'){
		            			return '手机web端';
		            		}else{
		            			return 'APP';
		            		}
		            		return data;
	            		}
		            },
		            {
		            	"data": "applyStatus",
		            	render: function(data, type, row, meta) {
		            		if (data == 1) {
		            			return '<span style="color: green;">通过</span>';
		            		}else if(data == 0){
		            			return '<span style="color: #F7A54A;">申请中</span>';
		            		}else{
		            			return '<span style="color: red;">不通过</span>';
		            		}
		            		return data;
	            		}
		            },
		            {"data": "auditDesc"},
		            {
		            	"data": "firstFlag",
		            	render: function(data, type, row, meta) {
		            		if (data == 1) {
		            			return '<span style="color: red;">是</span>';
		            		}else {
		            			return '否';
		            		}
		            		return data;
	            		}
		            },
		            {
		            	"data": "enable","bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false,
		            	render: function(data, type, row, meta) {
		            		if (data == "1") {
		            			return '启用';
		            		} else {
		            			return '禁用';
		            		}
		            		return data;
	            		}
		            },
		            {   
		            	"data": "createTime","bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false,
		            	render: function(data, type, row, meta) {
		            		return dateFormat(data,'yyyy-MM-dd hh:mm:ss');
	            		}
		            },
		     
		            {   
		            	"data": "updateTime","bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false,
		            	render: function(data, type, row, meta) {
		            		return dateFormat(data,'yyyy-MM-dd hh:mm:ss');
	            		}
		            },
		            {"data": "updateAdmin","bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {
	                	 "data": "id",
	                	 render: function(data, type, row, meta) {
	                		 if(currentUser.bizSystem=='SUPER_SYSTEM'){
		                		 return "<a class='btn btn-info btn-sm btn-bj' onclick='bizSystemDomainPage.editBizSystemDomain("+data+")'><i class='fa fa-pencil'></i>&nbsp;审核</a>&nbsp;&nbsp;" +
	                		 		"<a class='btn btn-danger btn-sm btn-del' onclick='bizSystemDomainPage.delBizSystemDomain("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>"; 
	                		 }else{
	                			 return "<a class='btn btn-danger btn-sm btn-del' onclick='bizSystemDomainPage.delBizSystemDomain("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";  
	                		 }

	                	 }
	                }
	            ]
	}).api(); 
}


/**
 * 查询数据
 */
BizSystemDomainPage.prototype.findBizSystemDomain = function(){
	bizSystemDomainPage.queryParam=getFormObj("#queryForm");
	bizSystemDomainPage.table.ajax.reload();
};

BizSystemDomainPage.prototype.refreshCache=function(){
	managerBizSystemDomainAction.updatecache(function(r){
		if(r[0]=="ok"){
			swal({ title: "提示", text: "缓存已刷新成功",type: "success"});
		}else{
			swal("刷新缓存失败");
		}
	});
}

/**
 * 删除
 */
BizSystemDomainPage.prototype.delBizSystemDomain = function(id){
	 swal({
         title: "您确定要删除这条信息吗",
         text: "请谨慎操作！",
         type: "warning",
         showCancelButton: true,
         confirmButtonColor: "#DD6B55",
         cancelButtonText: "取消",
         confirmButtonText: "删除",
         closeOnConfirm: false
     },
     function() {
		managerBizSystemDomainAction.delBizSystemDomainById(id,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "删除成功",type: "success"});
				bizSystemDomainPage.findBizSystemDomain();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
   			}else{
   				swal("删除信息失败.");
   			}
	    });
	});
};

BizSystemDomainPage.prototype.addBizSystemDomain=function()
{
	  layer.open({
          type: 2,
          title: '域名申请',
          maxmin: false,
          shadeClose: true,
          //点击遮罩关闭层
          area: ['600px', '500px'],
        //  area: ['45%', '63%'],
          content: contextPath + "/managerzaizst/bizsystemdomain/editbizsystemdomain.html",
          cancel: function(index){ 
        	  bizSystemDomainPage.findBizSystemDomain();
       	   }
      });
	
}

BizSystemDomainPage.prototype.editBizSystemDomain=function(id)
{
	  layer.open({
          type: 2,
          title: '域名申请审核',
          maxmin: false,
          shadeClose: true,
          //点击遮罩关闭层
          area: ['600px', '500px'],
         // area: ['45%', '63%'],
          content: contextPath + "/managerzaizst/bizsystemdomain/"+id+"/editbizsystemdomain.html",
          cancel: function(index){ 
        	  bizSystemDomainPage.findBizSystemDomain();
       	   }
      });

}

BizSystemDomainPage.prototype.closeLayer=function(index){
	layer.close(index);
	bizSystemDomainPage.findBizSystemDomain();
}