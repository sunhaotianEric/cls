function EditBizSystemDomainPage(){}
var editBizSystemDomainPage = new EditBizSystemDomainPage();

editBizSystemDomainPage.param = {
   	
};

$(document).ready(function() {
	var bizSystemDomainId = editBizSystemDomainPage.param.id;  //获取查询ID
	if(bizSystemDomainId != null){
		editBizSystemDomainPage.editBizSystemDomain(bizSystemDomainId);
	}
	
	if($("#domainType").val()=="PC"){
		$("#jumpMobileDomainUrlDiv").show();
		editBizSystemDomainPage.selectSystemDomain("MOBILE",$("#bizSystem").val(),"null");
	}
	
	$("#domainType").change(function(){
		var val=$(this).val();
		if(val=="PC"){
			$("#jumpMobileDomainUrlDiv").show();
			editBizSystemDomainPage.selectSystemDomain("MOBILE",$("#bizSystem").val(),"null");
		}else{
			$("#jumpMobileDomainUrlDiv").hide();
		}
		
	})
	$("#bizSystem").change(function(){
		$("#domainType").change();
		
	})
});


/**
 * 保存更新信息
 */
EditBizSystemDomainPage.prototype.saveData = function(){
	
	var bizsystemdomain=getFormObj("#bizsystemdomainForm");
	bizsystemdomain.id=editBizSystemDomainPage.param.id;

	if(bizsystemdomain.domainUrl==null || bizsystemdomain.domainUrl.trim()==""){
		swal("请输入域名地址!");
		$("#domainUrl").focus();
		return;
	}
	managerBizSystemDomainAction.saveOrUpdateBizSystenDomain(bizsystemdomain,function(r){
		if (r[0] != null && r[0] == "ok") {
			 swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.bizSystemDomainPage.closeLayer(index);
		   	    }); 
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("保存信息失败.");
		}
     });
};


/**
 * 赋值信息
 */
EditBizSystemDomainPage.prototype.editBizSystemDomain = function(id){
	managerBizSystemDomainAction.getBizSystemDomainById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			var admin=r[2];//拿到登陆账号对象，去判断权限
			$("#id").val(r[1].id);
			$("#domainUrl").val(r[1].domainUrl);
			$("#domainType").val(r[1].domainType);
			$("#systemStatus").val(r[1].systemStatus);
			$("#firstFlag").val(r[1].firstFlag);
			$("#isHttps").val(r[1].isHttps);
			
			
			if(admin.bizSystem=='SUPER_SYSTEM')
			{
				$("#bizSystem").val(r[1].bizSystem);
				$("#applyStatus").val(r[1].applyStatus);
				$("#auditDesc").val(r[1].auditDesc);
				$("#enable").val(r[1].enable);
				
			}
			if(r[1].domainType=="PC"){
				$("#jumpMobileDomainUrlDiv").show();
				editBizSystemDomainPage.selectSystemDomain("MOBILE",r[1].bizSystem,r[1].jumpMobileDomainUrl);
				
			}else{
				$("#jumpMobileDomainUrlDiv").hide();
			}
			
			
			
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取信息失败.");
		}
    });
};

EditBizSystemDomainPage.prototype.selectSystemDomain = function(domainType,bizSystem,jumpMobileDomainUrl){
	var domain = {};
	domain.bizSystem=bizSystem;
	domain.domainType=domainType;
	managerBizSystemDomainAction.selectAllBizSystemDomain(domain,function(r){
		if (r[0] != null && r[0] == "ok") {
			var domains=r[1];
			var domainListObj = $("#jumpMobileDomainUrl");
			var str = "";
			domainListObj.html("<option value=''>无</option>");
			for(var i = 0 ; i < domains.length; i++){
				var domain = domains[i];
				str="<option value='" +domain.domainUrl+"'>"+domain.domainUrl+"</option>"; 
				domainListObj.append(str);
				str="";
			}
			if(!$("#jumpMobileDomainUrlDiv").is(":hidden") && jumpMobileDomainUrl!="null"){
				$("#jumpMobileDomainUrl").val(jumpMobileDomainUrl);
			}
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取信息失败.");
		}
    });
}


