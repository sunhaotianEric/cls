<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>  
    <head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>域名申请</title>
    <link rel="shortcut icon" href="favicon.ico">
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    <%
	 String id = request.getParameter("id");
	%>
	
</head>
    <body class="gray-bg">
        <div class="wrapper wrapper-content animated fadeIn">
           <form action="javascript:void(0)" id="bizsystemdomainForm">
            <div class="row">
            <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                <div class="col-sm-12">
                    <div class="input-group m-b">
                        <span class="input-group-addon">商户</span>
                        <cls:bizSel name="bizSystem" id="bizSystem" emptyOption="false" options="class:ipt form-control"/> 
                    </div>
                </div>
                </c:if>
                <div class="col-sm-12">
                    <div class="input-group m-b">
                        <span class="input-group-addon">域名地址</span>
                         <c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
	   			          <input type="hidden" id="bizSystem" name="bizSystem" value="${admin.bizSystem}"/>
	   			         </c:if>
	   			         <input type="hidden" id="id" name="id" />
                        <input id="domainUrl" name="domainUrl" type="text" value="" class="form-control"></div>
                </div>
                <div class="col-sm-12">
                    <div class="input-group m-b">
                        <span class="input-group-addon">域名类型</span>
                        <select id="domainType" name="domainType" class="ipt form-control">
		   			    <option value="PC" selected="selected">PC端</option>
		   			    <option value="MOBILE" >手机web端</option>
		   			    <option value="APP" >APP</option>
		   			    </select>
                    </div>
                </div>
                  <div class="col-sm-12">
                    <div class="input-group m-b">
                        <span class="input-group-addon">首推域名</span>
                        <select id="firstFlag" name="firstFlag" class="ipt form-control">
		   			    <option value="1" >是</option>
		   			    <option value="0" selected="selected">否</option>
		   			    
		   			    </select>
                    </div>
                </div>
                <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                <div class="col-sm-12">
                    <div class="input-group m-b">
                        <span class="input-group-addon">是否跳转https</span>
                        <select id="isHttps" name="isHttps" class="ipt form-control">
		   			    <option value="1">是</option>
		   			    <option value="0" selected="selected">否</option>
		   			    
		   			    </select>
                    </div>
                </div>
                </c:if>
                <c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
                <input type="hidden" id="isHttps" name="isHttps" value="0" />
                </c:if>
                <div class="col-sm-12" id="jumpMobileDomainUrlDiv">
                    <div class="input-group m-b">
                        <span class="input-group-addon">跳转手机域名</span>
                        <select id="jumpMobileDomainUrl" name="jumpMobileDomainUrl" class="ipt form-control">
		   			    </select>
                    </div>
                </div>
               <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                <div class="col-sm-12">
                    <div class="input-group m-b">
                        <span class="input-group-addon">启用状态</span>
                       <select id="enable" name="enable" class="ipt form-control">
		   			    <option value="1" selected="selected">启用</option>
		   			    <option value="0" >禁用</option>
		   			   </select>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="input-group m-b">
                        <span class="input-group-addon">申请状态</span>
			   			  <select id="applyStatus" name="applyStatus" class="ipt form-control">
			   			    <option value="1" selected="selected">通过</option>
			   			    <% if(id!=null&&!"".equals(id)){ %>
			   		        <option value="0" >申请中</option>
			   		        <%} %>	    
			   			    <option value="2" >不通过</option>
			   			  </select>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="input-group m-b">
                        <span class="input-group-addon">审核备注</span>
                        <textarea rows="3" cols="50" id="auditDesc" name="auditDesc" class="form-control"></textarea>
                       </div>
                </div>
                </c:if>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <button type="button" class="btn btn-w-m btn-white" onclick="editBizSystemDomainPage.saveData()">提 交</button>
                            &nbsp;&nbsp;<input type="reset" class="btn btn-w-m btn-white" value="重 置"/></div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/bizsystemdomain/js/editbizsystemdomain.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerBizSystemDomainAction.js'></script>
    <script type="text/javascript">
	  editBizSystemDomainPage.param.id = <%=id%>;
	</script>
</body>
</html>

