function UserquotaPage(){}
var userquotaPage = new UserquotaPage();

userquotaPage.quotaParam = {

};
userquotaPage.size = 0;
$(document).ready(function() {
	//业务系统选择框初始化
//	if(currentUser.bizSystem=='SUPER_SYSTEM'){
//		userquotaPage.getallbizSystem('bizSystem');
//	}

	var _this = $('.list').find('thead');
    //折叠
    _this.click(function () {
        var i = $(this).find('i');
        if (i.attr('class') == 'tip-down') { i.attr('class', 'tip-up'); } else { i.attr('class', 'tip-down'); }
        $(this).parent().find('tbody').toggle();
    });
    
  //配额增加
	$(".add-btn").click(function(){
		var id = $(this).attr("data-val");
		var quotaVal = $("#" + id).val();
		if(quotaVal != null && quotaVal != "") {
			quotaVal++;
			$("#" + id).val(quotaVal);
		}
	});
	//配额减少
	$(".reduction-btn").click(function(){
		var id = $(this).attr("data-val");
		var quotaVal = $("#" + id).val();
		if(quotaVal != null && quotaVal != "") {
			if(quotaVal == 0) {
				swal("配额最低只能为0");
				return;
			}
			quotaVal--;
			$("#" + id).val(quotaVal);
		}
	});
    
});

/**
 * 根据查询用户配额已使用的结果
 */
UserquotaPage.prototype.queryUserQuotaInfoByUserName = function(){
	var usedUserName = getSearchVal("usedUserName");
	var usedbizSystem = getSearchVal("usedbizSystem");
	 $('.userQuotaInfo .form-control').val(""); 
	if(usedUserName == null || usedUserName == ""){
		swal("查询用户名不能为空");
		return;
	}
	managerUserQuotaAction.getUserQuotaInfoByUserName(usedUserName,usedbizSystem,function(r){
		if (r[0] != null && r[0] == "ok") {
			userquotaPage.showUserQuotaInfo(r[1]);
		}else if (r[0] != null && r[0] == "error") {
			swal(r[1]);
			$("#userQuotaInfo").html("");
		}else{
			swal("查询用户已使用配额信息失败");
			$("#userQuotaInfo").html("");
		}
    });
};

UserquotaPage.prototype.showUserQuotaInfo = function (page){
	var userQuotaList = page.pageContent;
	userquotaPage.size = userQuotaList.length;
	$("#userQuotaInfo").html("");
	var str="";
	for(var i=0;i<userQuotaList.length;i++){
		var userQuota = userQuotaList[i];
		
		str +='<div class="row">';
		str +='<div class="col-sm-4">';
		str +='<div class="input-group m-b">';
		str +='<span class="input-group-addon">'+userQuota.sscRebate+'模式已使用配额数</span>';
		str +='<input type="text" class="form-control"  readonly="readonly" value="'+userQuota.rebateCount+'" style="background-color: white;">';
		str +='</div>';
		str +='</div>';
		str +='</div>';
	}
	$("#userQuotaInfo").append(str);
}



/**
 * 根据用户名查询用户配额剩余结果
 */
UserquotaPage.prototype.queryUserQuotaByUserName = function(){
	//重置查询结果
	 $('.rowDiv .form-control').val(""); 
	var remainUserName = getSearchVal("remainUserName");
	var remainbizSystem = getSearchVal("remainbizSystem");
	if(remainUserName == null || remainUserName == ""){
		swal("查询用户名不能为空");
		return;
	}
	//更新配额根据查询的用户名
	userquotaPage.quotaParam.userName = remainUserName;
	managerUserQuotaAction.getUserQuotaByUserName(remainUserName,remainbizSystem,function(r){
		if (r[0] != null && r[0] == "ok") {
			userquotaPage.showUserQuota(r[1]);
		}else if (r[0] != null && r[0] == "error") {
			swal(r[1]);
			$("#rowDiv").html("");
		}else{
			swal("加载配额信息失败");
			$("#rowDiv").html("");
		}
    });
		
};

UserquotaPage.prototype.showUserQuota = function (page){
	var userQuotaList = page.pageContent;
	userquotaPage.size = userQuotaList.length;
	$("#rowDiv").html("");
	var str="";
	for(var i=0;i<userQuotaList.length;i++){
		var userQuota = userQuotaList[i];
		var userQuotaId = userQuota.id;
		str +='<div class="row" id="quotaRow'+i+'">';
		str +='<div class="col-sm-4">';
		str +='<div class="input-group m-b">';
		str +='<span class="input-group-addon">'+userQuota.sscRebate+'模式未使用配额数</span>';
		str +='<input type="text" class="form-control" id="remainNum'+i+'" readonly="readonly" value="'+userQuota.remainNum+'" style="background-color: white;">';
		str +='<input type="hidden" class="form-control" id="userId'+i+'" name="userId'+i+'" value="'+userQuota.userId+'">';
		str +='<input type="hidden" class="form-control" id="id'+i+'" name="id'+i+'" value="'+userQuota.id+'">';
		str +='<input type="hidden" class="form-control" id="userName'+i+'" name="userName'+i+'" value="'+userQuota.userName+'">';
		str +='<input type="hidden" class="form-control" id="sscRebate'+i+'" name="sscRebate'+i+'" value="'+userQuota.sscRebate+'">';
		str +='</div>';
		str +='</div>';
		str +='<div class="col-sm-8">';
		str +='<div class="form-group">&nbsp;&nbsp;&nbsp;';
		str +='<a href="javascript:void(0);" class="btn btn-white btn-sm add-btn" id="remain1958Add" data-val="remainNum'+i+'"><i class="glyphicon glyphicon-plus"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;';
		str +='<a href="javascript:void(0);" class="btn btn-white btn-sm reduction-btn" id="remain1958Reduction" data-val="remainNum'+i+'"><i class="glyphicon glyphicon-minus"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;';
		str +='<a  title = "点击删除该条模式配额" href="javascript:void(0);" class="btn btn-danger btn-sm btn-del" onclick="userquotaPage.delQuota('+userQuotaId+','+i+')"><i class="fa fa-trash" ></i></a>';
		str +='</div>';
		str +='</div>';
		str +='</div>';
	}
	str +='<div class="row" id="addrow">';
	str +='<div class="col-sm-1">';
	str +='<a title="点击添加新模式配额" href="javascript:void(0);" class="btn btn-info btn-sm btn-mx" onclick="userquotaPage.showAddUserQuota()"><i class="glyphicon glyphicon-plus"></i></a>';
	str +='</div></div>';
	$("#rowDiv").append(str);
	
	
	  //配额增加
	$(".add-btn").click(function(){
		var id = $(this).attr("data-val");
		var quotaVal = $("#" + id).val();
		if(quotaVal != null && quotaVal != "") {
			quotaVal++;
			$("#" + id).val(quotaVal);
		}
	});
	//配额减少
	$(".reduction-btn").click(function(){
		var id = $(this).attr("data-val");
		var quotaVal = $("#" + id).val();
		if(quotaVal != null && quotaVal != "") {
			if(quotaVal == 0) {
				swal("配额最低只能为0");
				return;
			}
			quotaVal--;
			$("#" + id).val(quotaVal);
		}
	});
}

/**
 * 删除
 */
UserquotaPage.prototype.delQuota = function(id,delrowid) {
	   swal({
	        title: "您确定要删除该条配额模式？",
	        text: "请谨慎操作！",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonColor: "#DD6B55",
	        cancelButtonText: "取消",
	        confirmButtonText: "确定",
	        closeOnConfirm: false
	    },
	    function() {
	    	managerUserQuotaAction.deleteUserQuota(id,function(r){
				if (r[0] != null && r[0] == "ok") {
					swal(
					   { title: "提示", text: "操作成功！",type: "success"},
				       function() {
						   $("#quotaRow"+delrowid).remove();
				   	    });
					
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("操作失败.");
				}
		    });
	    });
}
/**
 * 新增配额信息
 */
UserquotaPage.prototype.showAddUserQuota = function() {
	var remainUserName = getSearchVal("remainUserName");
	//len>0,模式列表的用户名和查询条件的用户名对比
	if(userquotaPage.size>0){
		var userName = $("#userName0").val();
		
		if(userName!=remainUserName){
			swal("当前模式列表不属于"+remainUserName+"的,请先重新查询，再来添加！");
			return ;
		}
	}
	var remainbizSystem = getSearchVal("remainbizSystem");
	
	
    layer.open({
        type: 2,
        title: '模式配额新增',
        maxmin: false,
        shadeClose: true,
        //点击遮罩关闭层
        area: ['500px', '300px'],
        content: contextPath + "/managerzaizst/userquota/adduserquota.html?remainbizSystem="+remainbizSystem+"&remainUserName="+remainUserName,
      });
	
}

/**
 * 新增配额信息
 */
UserquotaPage.prototype.addUserQuota = function() {
	userquotaPage.userQuota = {};
	var remainUserName = getSearchVal("remainUserName");
	var remainbizSystem = getSearchVal("remainbizSystem");
	var dlms = getSearchVal("dlms");
	var pes = getSearchVal("pes");
	userquotaPage.userQuota.bizSystem = remainbizSystem ;
	userquotaPage.userQuota.userName = remainUserName ;
	userquotaPage.userQuota.sscRebate = dlms ;
	userquotaPage.userQuota.remainNum = pes ;
	
	managerUserQuotaAction.addUserQuota(userquotaPage.userQuota,function(r){
		if (r[0] != null && r[0] == "ok") {
			   swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.userquotaPage.closeLayer(index);
		   	    });  
		}else if (r[0] != null && r[0] == "error") {
			swal(r[1]);
		}else{
			swal("请求失败.");
		}
    });		
}

UserquotaPage.prototype.closeLayer=function(index){
	layer.close(index);
	userquotaPage.queryUserQuotaByUserName();
}

/**
 * 加载配额信息
 */
UserquotaPage.prototype.getAwardModel = function() {
	
	var selDom = $("#dlms");
	//初始化加载最高模式
	managerUserQuotaAction.getAwardModel(remainbizSystem_add,function(r){
		if (r[0] != null && r[0] == "ok") {
			var highestAwardModel=r[1].sscHighestAwardModel;
			var lowestAwardModel=r[1].sscLowestAwardModel;
			for(var i=0;i<(highestAwardModel-lowestAwardModel);i=i+2){
				selDom.append("<option value='"+(highestAwardModel-i)+"'>"+(highestAwardModel-i)+"</option>");	
			}	
		}else if (r[0] != null && r[0] == "error") {
			swal(r[1]);
		}else{
			swal("请求失败.");
		}
    });	
}


/**
 * 更新配额信息
 */
UserquotaPage.prototype.updateUserQuota = function() {
	userquotaPage.userQuotaVo = {};
	if(userquotaPage.size==0){
		swal("无操作数据！");
		return ;
	}	
	userquotaPage.userQuotaVo.userQuotaList=new Array();
	for(var i=0;i<userquotaPage.size;i++){
		var userQuota={};
		userQuota.id=$("#id"+i).val();
		userQuota.userName=$("#userName"+i).val();
		userQuota.remainNum=$("#remainNum"+i).val();
		userQuota.userId=$("#userId"+i).val();
		userQuota.sscRebate=$("#sscRebate"+i).val();
		userquotaPage.userQuotaVo.userQuotaList.push(userQuota);
	}
	
	
	managerUserQuotaAction.updateUserQuota(userquotaPage.userQuotaVo,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal("配额更新成功");
		}else if (r[0] != null && r[0] == "error") {
			swal(r[1]);
		}else{
			swal("更新配额信息失败");
		}
    });
}

