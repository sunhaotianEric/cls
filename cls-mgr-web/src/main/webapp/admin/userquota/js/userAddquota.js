function UserAddquotaPage(){}
var userAddquotaPage = new UserAddquotaPage();

userAddquotaPage.quotaParam = {

};

$(document).ready(function() {

    
});

/**
 * 新增配额信息
 */
UserAddquotaPage.prototype.addUserQuota = function() {
	userAddquotaPage.userQuota = {};
	var dlms = getSearchVal("dlms");
	var pes = getSearchVal("pes");
	userAddquotaPage.userQuota.bizSystem = remainbizSystem_add ;
	userAddquotaPage.userQuota.userName = remainUserName_add ;
	userAddquotaPage.userQuota.sscRebate = dlms ;
	userAddquotaPage.userQuota.remainNum = pes ;
	
	managerUserQuotaAction.addUserQuota(userAddquotaPage.userQuota,function(r){
		if (r[0] != null && r[0] == "ok") {
			   swal({
		           title: "保存成功",
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.userquotaPage.closeLayer(index);
		   	    });  
		}else if (r[0] != null && r[0] == "error") {
			swal(r[1]);
		}else{
			swal("请求失败.");
		}
    });		
}



/**
 * 加载配额信息
 */
UserAddquotaPage.prototype.getAwardModel = function() {
	
	var selDom = $("#dlms");
	//初始化加载最高模式
	managerUserQuotaAction.getAwardModel(remainbizSystem_add,function(r){
		if (r[0] != null && r[0] == "ok") {
			var highestAwardModel=r[1].sscHighestAwardModel;
			var lowestAwardModel=r[1].sscLowestAwardModel;
			for(var i=0;i<(highestAwardModel-lowestAwardModel);i=i+2){
				selDom.append("<option value='"+(highestAwardModel-i)+"'>"+(highestAwardModel-i)+"</option>");	
			}	
		}else if (r[0] != null && r[0] == "error") {
			swal(r[1]);
		}else{
			swal("请求失败.");
		}
    });	
}
