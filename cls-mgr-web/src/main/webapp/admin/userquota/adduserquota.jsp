<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
    String remainbizSystem = request.getParameter("remainbizSystem");
    String remainUserName = request.getParameter("remainUserName");
%>
<!DOCTYPE html>
<html>  
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>会员配额添加</title>
        <link rel="shortcut icon" href="favicon.ico">
		<jsp:include page="/admin/include/include.jsp"></jsp:include>	
</head>
  <body class="gray-bg">
    <div id="layerDiv">
      <div class="wrapper wrapper-content animated fadeIn">
            <div class="row">
               
                <div class="col-sm-12">
                    <div class="input-group m-b">
                        <span class="input-group-addon">模式</span>
                        <select class="ipt form-control" id="dlms">
                         </select>
                    </div>
                </div>
             
            </div> 
            <div class="row">
              
                <div class="col-sm-12">
                    <div class="input-group m-b">
                        <span class="input-group-addon">配额数</span>
                        <input type="text" value="0" class="form-control" id="pes" onkeyup="checkNum(this)"></div>
                </div>
  
             </div>
        
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <button type="button" class="btn btn-w-m btn-white btn-add" onclick="userAddquotaPage.addUserQuota()">保 存</button></div>
                    </div>
                </div>      
        </div>

   </div>
<script type="text/javascript">
var remainbizSystem_add = '<%=remainbizSystem%>';
var remainUserName_add = '<%=remainUserName%>';
</script>
    <!-- js -->
	<script type="text/javascript" src="<%=path%>/admin/userquota/js/userAddquota.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
	<!-- dwr -->
	<script type='text/javascript' src='<%=path%>/dwr/interface/managerUserQuotaAction.js'></script>  
<script type="text/javascript">
userAddquotaPage.getAwardModel();
</script>
</body>
</html>

