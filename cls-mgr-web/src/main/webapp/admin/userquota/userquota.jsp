<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>  
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>会员配额管理</title>
        <link rel="shortcut icon" href="favicon.ico">
		<jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
  <body class="gray-bg">
    <div class="wrapper wrapper-content animated fadeIn">
      <div class="row">
        <div class="col-sm-12">
          <div class="tabs-container">
            <ul class="nav nav-tabs">
              <li class="active">
                <a data-toggle="tab" href="#tab-1" aria-expanded="true">配额已使用查询</a></li>
              <li class="">
                <a data-toggle="tab" href="#tab-2" aria-expanded="false">配额剩余查询修改</a></li>
            </ul>
            <div class="tab-content">
              <div id="tab-1" class="tab-pane active">
                <div class="panel-body">
                  <form class="form-horizontal">
                   <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                     <div class="col-sm-2">
                      <div class="input-group m-b">
                        <span class="input-group-addon">商户</span>
                           <cls:bizSel name="usedbizSystem" id="usedbizSystem" options="class:ipt form-control" />
                        </div>
                     </div>
                    <div class="col-sm-2">
                      <div class="input-group m-b">
                        <span class="input-group-addon">用户名</span>
                          <input type="text" id='usedUserName' class="form-control"/>               
                        </div>
                     </div>
                     </c:if>
                     <c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
                     <div class="col-sm-4">
                      <div class="input-group m-b">
                        <span class="input-group-addon">用户名</span>
                          <input type="text" id='usedUserName' class="form-control"/>
	                     
	                     <input type="hidden" id="usedbizSystem" name="usedbizSystem" value="${admin.bizSystem}">
	                    
                        </div>
                     </div>
                     </c:if>
                    <div class="col-sm-8">
                      <div class="form-group">&nbsp;&nbsp;&nbsp;&nbsp;
                        <button type="button" class="btn btn-outline btn-default btn-js" onclick="userquotaPage.queryUserQuotaInfoByUserName()"><i class="fa fa-search"></i>&nbsp;查 询</button>
                    </div>
                    </div>
                   <div class="userQuotaInfo" id="userQuotaInfo">
                    </div> 
                  </form>
                </div>
              </div>
              
              <div id="tab-2" class="tab-pane">
                <div class="panel-body">
                  <div class="form-horizontal">
                    <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                     <div class="col-sm-2">
                      <div class="input-group m-b">
                        <span class="input-group-addon">商户</span>
                           <cls:bizSel name="remainbizSystem" id="remainbizSystem" options="class:ipt form-control" />
                        </div>
                     </div>
                    <div class="col-sm-2">
                      <div class="input-group m-b">
                        <span class="input-group-addon">用户名</span>
                          <input type="text" id='remainUserName' class="form-control"/>               
                        </div>
                     </div>
                     </c:if>
                     <c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
                     <div class="col-sm-4">
                      <div class="input-group m-b">
                        <span class="input-group-addon">用户名</span>
                          <input type="text" id='remainUserName' class="form-control"/>    
	                     <input type="hidden" id="remainbizSystem" name="remainbizSystem" value="${admin.bizSystem}">
                        </div>
                     </div>
                     </c:if>
                    <div class="col-sm-8">
                      <div class="form-group">&nbsp;&nbsp;&nbsp;&nbsp;
                        <button type="button" class="btn btn-outline btn-default btn-js" onclick="userquotaPage.queryUserQuotaByUserName()"><i class="fa fa-search"></i>&nbsp;查 询</button>&nbsp;&nbsp;&nbsp;
                        <button type="button" class="btn btn-outline btn-default btn-js" onclick="userquotaPage.updateUserQuota()"><i class="fa fa-check"></i>&nbsp;提 交</button></div>
                    </div>
                   <div class="rowDiv" id="rowDiv">
                    
                    </div>        
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> 
    
<!--     <div id="layerDiv" style="display: none;z-index: -999">
    
      <div class="wrapper wrapper-content animated fadeIn">
            <div class="row">
               
                <div class="col-sm-12">
                    <div class="input-group m-b">
                        <span class="input-group-addon">模式</span>
                        <select class="ipt form-control" id="dlms">
                         </select>
                    </div>
                </div>
             
            </div> 
            <div class="row">
              
                <div class="col-sm-12">
                    <div class="input-group m-b">
                        <span class="input-group-addon">配额数</span>
                        <input type="text" value="0" class="form-control" id="pes"></div>
                </div>
  
             </div>
        
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <button type="button" class="btn btn-w-m btn-white btn-add" onclick="userquotaPage.addUserQuota()">保 存</button></div>
                    </div>
                </div>      
        </div>

   </div> -->

 <!-- js -->
<script type="text/javascript" src="<%=path%>/admin/userquota/js/userquota.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerUserQuotaAction.js'></script>   
</body>
</html>

