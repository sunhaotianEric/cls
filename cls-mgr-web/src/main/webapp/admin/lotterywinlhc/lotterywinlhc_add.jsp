<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
  	<%
	 	String id = request.getParameter("id");  //获取查询的商品ID
	%>
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeIn">
 <form class="form-horizontal">
    <div class="row">
	    <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
	    <div class="col-sm-12">
            <div class="input-group m-b" draggable="false">
                <span class="input-group-addon">商户：</span>
                  <cls:bizSel name="bizSystem" id="bizSystemId" options="class:ipt form-control"/>
            </div>
		</div>    
    </c:if>
        <div class="col-sm-6">
            <div class="input-group m-b">
                <span class="input-group-addon">玩法类别</span>
                <select class="ipt form-control" name="lotteryTypeProtype" id="lotteryTypeProtypeId" onChange="lotteryWinLhcSelectEventPage.selectEvent()">
                     <option value="" selected="selected">==请选择==</option>
                     <option value="TM">特码</option>
                     <option value="ZM">正码</option>
                     <option value="ZMT">正码特</option>
                     <option value="ZM16">正码1-6</option>
                     <option value="LM">连码</option>
                      <option value="BB">半波</option>
                     <option value="YXWS">一肖/尾数</option>
                     <option value="TMSX">特码生肖</option>
                     <option value="HX">合肖</option>
                     <option value="LX">连肖</option>
                     <option value="WSL">尾数连</option>
                     <option value="QBZ">全不中</option>
                </select>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="input-group m-b">
                <span class="input-group-addon">玩法</span>
                <select class="ipt form-control" name="lotteryName" id="lotteryNameId">
                  	<option value="" selected="selected">==请选择==</option>
                   	<option value="TMA">特A</option>
                   	<option value="TMB">特B</option>
                   	<option value="ZMA">正码A</option>
                   	<option value="ZMB">正码B</option>
                   	<option value="ZMT1">正一特</option>
                   	<option value="ZMT2">正二特</option>
                   	<option value="ZMT3">正三特</option>
                   	<option value="ZMT4">正四特</option>
                   	<option value="ZMT5">正五特</option>
                   	<option value="ZMT6">正六特</option>
                   	<option value="ZM16">正码1-6</option>
                   	<option value="LMSQZ">三全中</option>
                   	<option value="LMSZ22">三中二之中二</option>
                   	<option value="LMSZ23">三中二之中三</option>
                   	<option value="LMEQZ">二全中</option>
                   	<option value="LMEZT">二中特之中特</option>
                   	<option value="LMEZ2">二中特之中二</option>
                   	<option value="LMTC">特串</option>
                   	<option value="LMSZ1">四中一</option>
                   	<option value="BB">半波</option>
                   	<option value="YXWS">一肖/尾数</option>
                   	<option value="TMSX">特码生肖</option>
                   	<option value="HX2">二肖</option>
                   	<option value="HX3">三肖</option>
                   	<option value="HX4">四肖</option>
                   	<option value="HX5">五肖</option>
                   	<option value="HX6">六肖</option>
                   	<option value="HX7">七肖</option>
                   	<option value="HX8">八肖</option>
                   	<option value="HX9">九肖</option>
                   	<option value="HX10">十肖</option>
                   	<option value="HX11">十一肖</option>
                   	<option value="LXELZ">二肖连中</option>
                   	<option value="LXSLZ">三肖连中</option>
                   	<option value="LXSILZ">四肖连中</option>
                   	<option value="LXWLZ">五肖连中</option>
                   	<option value="LXELBZ">二肖连不中</option>
                   	<option value="LXSLBZ">三肖连不中</option>
                   	<option value="LXSILBZ">四肖连不中</option>
                   	<option value="WSLELZ">二尾连中</option>
                   	<option value="WSLSLZ">三尾连中</option>
                   	<option value="WSLSILZ">四尾连中</option>
                   	<option value="WSLELBZ">二尾连不中</option>
                   	<option value="WSLSLBZ">三尾连不中</option>
                   	<option value="WSLSILBZ">四尾连不中</option>
                   	<option value="QBZ5">五不中</option>
                   	<option value="QBZ6">六不中</option>
                   	<option value="QBZ7">七不中</option>
                   	<option value="QBZ8">八不中</option>
                   	<option value="QBZ9">九不中</option>
                   	<option value="QBZ10">十不中</option>
                   	<option value="QBZ11">十一不中</option>
                   	<option value="QBZ12">十二不中</option>
                </select>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="input-group m-b">
                <span class="input-group-addon">号码</span>
                <input type="text" value="" class="form-control" id="code"></div>
        </div>
        <div class="col-sm-6">
            <div class="input-group m-b">
                <span class="input-group-addon">号码描述</span>
                <input type="text" value="" class="form-control"  id="codeDes"></div>
        </div>
        <div class="col-sm-6">
            <div class="input-group m-b">
                <span class="input-group-addon">赔率</span>
                <input type="text" value="" class="form-control" id="winMoney"></div>
        </div>
        <div class="col-sm-6">
            <div class="input-group m-b">
                <span class="input-group-addon">返水</span>
                <input type="text" value="" class="form-control"  id="returnPercent"><span class="input-group-addon">%</span></div>
        </div>
      <!--  <div class="col-sm-6">
            <div class="input-group m-b">
                <span class="input-group-addon">开奖号码数</span>
                <input type="text" value="" class="form-control" id="codeNum"></div>
        </div>
        <div class="col-sm-6">
            <div class="input-group m-b">
                <span class="input-group-addon">是否按顺序</span>
                <select class="ipt form-control"  id="isorder">
                    <option value="0">是</option>
                    <option value ="1">否</option>
               </select>
           </div>
        </div> -->
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-6 text-center">
                    <button type="button" class="btn btn-w-m btn-white" onclick="editLotteryWinLhcPage.editLotteryWinLhc()">提 交</button></div>
                <div class="col-sm-6 text-center">
                    <button type="reset" class="btn btn-w-m btn-white">重 置</button></div>
            </div>
        </div>
    </div>
  </form>
</div>
<!-- js -->
<script type="text/javascript" src="<%=path%>/admin/lotterywinlhc/js/lotterywinlhc_edit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<script type="text/javascript" src="<%=path%>/admin/lotterywinlhc/js/lotterywinlhc.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
 <script type="text/javascript" src="<%=path%>/admin/lotterywinlhc/js/selectEvent.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerLotteryWinLhcAction.js'></script>
<script type="text/javascript">
editLotteryWinLhcPage.param.id = <%=id%>;
</script>
</body>

</html>