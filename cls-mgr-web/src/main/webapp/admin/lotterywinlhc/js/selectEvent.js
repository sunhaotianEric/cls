function LotteryWinLhcSelectEventPage(){}
var lotteryWinLhcSelectEventPage = new LotteryWinLhcSelectEventPage();


var map = {};
var mapType = {};
var AllLotteryName=[
                  ["特A","特B"],
      	          ["正码A","正码B"],
      	          ["正一特","正二特","正三特","正四特","正五特","正六特"],
      	          ["正码1-6"],
      	          ["三全中","三中二之中二","三中二之中三","二全中","二中特之中特","二中特之中二","特串","四中一"],
      	          ["半波"],
      	          ["一肖/尾数"],
      	          ["特码生肖"],
      	          ["二肖","三肖","四肖","五肖","六肖","七肖","八肖","九肖","十肖","十一肖"],
      	          ["二肖连中","三肖连中","四肖连中","五肖连中","二肖连不中","三肖连不中","四肖连不中"],
      	          ["二尾连中","三尾连中","四尾连中","二尾连不中","三尾连不中","四尾连不中"],
      	          ["五不中","六不中","七不中","八不中","九不中","十不中","十一不中","十二不中"],
          ];

var lotteryTypeProtypes=[
                         ["特码"],["特码"],
                         ["正码"],["正码"],
                         ["正码特"],["正码特"],["正码特"],["正码特"],["正码特"],["正码特"],
                         ["正码1-6"],
                         ["连码"], ["连码"], ["连码"], ["连码"], ["连码"], ["连码"], ["连码"], ["连码"],
                         ["半波"],["一肖/尾数"], ["特码生肖"],
                         ["合肖"], ["合肖"],["合肖"],["合肖"],["合肖"],["合肖"],["合肖"],["合肖"],["合肖"],["合肖"],
                         ["连肖"], ["连肖"], ["连肖"], ["连肖"], ["连肖"], ["连肖"], ["连肖"],
                         ["尾数连"],["尾数连"],["尾数连"],["尾数连"],["尾数连"],["尾数连"],
                         ["全不中"],["全不中"],["全不中"],["全不中"],["全不中"],["全不中"],["全不中"],["全不中"],
                       ];

$(document).ready(function() {
	$("#lotteryNameId option").each(function(){
		var val=$(this).val();
		var text = $(this).text();
		map[text]=val;
	})
	$("#lotteryTypeProtypeId option").each(function(){
		var val=$(this).val();
		var text = $(this).text();
		mapType[text]=val;
	})
	
	$("#lotteryTypeProtypeId").find("option[value='TM']").attr("selected","selected");
	lotteryWinLhcSelectEventPage.selectEvent();
})

LotteryWinLhcSelectEventPage.prototype.selectEvent = function() {
	var lotteryTypeProtype=document.getElementById("lotteryTypeProtypeId");
	//没有玩法类别
	if(lotteryTypeProtype == null){
		return;
	}
	var sltlotteryName=document.getElementById("lotteryNameId");
	var lotteryNames=AllLotteryName[(lotteryTypeProtype.selectedIndex==null?0:lotteryTypeProtype.selectedIndex)];
	sltlotteryName.length=0;
	for(var i=0;i<lotteryNames.length;i++){
		var string = lotteryNames[i]
	    sltlotteryName[i]=new Option(lotteryNames[i],map[string]);
	}
	
	lotteryWinLhcPage.queryConditionLotteryCodes();
}

LotteryWinLhcSelectEventPage.prototype.selectReverseEvent = function() {
	var sltlotteryNameR=document.getElementById("lotteryNameId");
	var lotteryTypeProtypeR=document.getElementById("lotteryTypeProtypeId");
	var lotteryTypeProtypess=lotteryTypeProtypes[sltlotteryNameR.selectedIndex==null?0:sltlotteryNameR.selectedIndex];
	lotteryTypeProtypeR.length=1;
	for(var i=0;i<lotteryTypeProtypess.length;i++){
		var string = lotteryTypeProtypess[i]
		lotteryTypeProtypeR[i+1]=new Option(lotteryTypeProtypess[i],mapType[string]);
	}
}
