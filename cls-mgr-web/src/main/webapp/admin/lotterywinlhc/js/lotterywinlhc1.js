function LotteryWinLhcPage(){}
var lotteryWinLhcPage = new LotteryWinLhcPage();

/**
 * 查询参数
 */
lotteryWinLhcPage.queryParam = {
	bizSystem : null,
	lotteryTypeProtype : null,
	lotteryName : null
	
};
lotteryWinLhcPage.param = {
	codeNum:0  //号码个数	
}
//分页参数
lotteryWinLhcPage.pageParam = {
    pageNo : 1,
    pageSize : defaultPageSize
};

$(document).ready(function() {
	lotteryWinLhcPage.queryConditionLotteryCodes();
	
	// 赔率修改
	$('.btn-peilv').on('click',function(){
	    var oldValue = $(this).closest('span').find('.oldValue').val();
	    var newValue = $(this).closest('span').find('.newValue').val();
	    if(currentUser.bizSystem == "SUPER_SYSTEM"){
		    $('.table-block .table tr td:nth-child(3)').each(function(i, item) {
		        if ($(item).find('input').val() == oldValue) {
		            $(item).find('input').val(newValue);
		        }
		    });
	    }else {
	    	$('.table-block .table tr td:nth-child(2)').each(function(i, item) {
		        if ($(item).find('input').val() == oldValue) {
		            $(item).find('input').val(newValue);
		        }
		    });
	    }
	})
	// 返水修改
	$('.btn-fanshui').on('click',function(){
	    var oldValue = $(this).closest('span').find('.oldValue').val();
	    var newValue = $(this).closest('span').find('.newValue').val();
	    if(currentUser.bizSystem == "SUPER_SYSTEM"){
	    	$('.table-block .table tr td:nth-child(4)').each(function(i, item) {
		        if ($(item).find('input').val() == oldValue) {
		            $(item).find('input').val(newValue);
		        }
		    });
	    }else{
	    	$('.table-block .table tr td:nth-child(3)').each(function(i, item) {
		        if ($(item).find('input').val() == oldValue) {
		            $(item).find('input').val(newValue);
		        }
		    });
	    }
	})
	
	// 点击底色变亮
	$('.table-block .table tbody').on('click','tr td',function(){
		$('.table-block table tr').removeClass('on')
		$(this).parent('tr').addClass('on');
	})
});



/**
 * 初始化列表数据
 */
LotteryWinLhcPage.prototype.initTableData = function() {
	lotteryWinLhcPage.queryParam = getFormObj($("#lotteryCodeForm"));
	//查询条件取玩法的值字段
	lotteryWinLhcPage.queryParam.lotteryTypeProtype = $("#lotteryNameId").val();
	//名称不作为查询条件
	lotteryWinLhcPage.queryParam.lotteryName = null;
	lotteryWinLhcPage.table = $('#lotteryWinLhcTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			lotteryWinLhcPage.pageParam.pageSize = data.length;//页面显示记录条数
			lotteryWinLhcPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerLotteryWinLhcAction.getLotteryWinLhcByPage(lotteryWinLhcPage.queryParam,lotteryWinLhcPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败.");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": "code",},
		            {"data": "codeDes"},
		            {"data": "winMoney"},
		            {"data": "returnPercent",
		            	 render: function(data, type, row, meta) {
		            		 	return data +"%";
		            		 },
		            	 },
	            	 {"data": "code"},
			            {"data": "codeDes"},
			            {"data": "winMoney"},
			            {"data": "returnPercent",
			            	 render: function(data, type, row, meta) {
			            		 	return data +"%";
			            		 },
			            	 },
	            	 {"data": "code"},
			            {"data": "codeDes"},
			            {"data": "winMoney"},
			            {"data": "returnPercent",
			            	 render: function(data, type, row, meta) {
			            		 	return data +"%";
			            		 },
			            	 },
		            /*{"data":"id",
		            	 render: function(data, type, row, meta) {
		            		 var str="";
		            		 str += "<a class='btn btn-info btn-sm btn-bj' onclick='lotteryWinLhcPage.editLotterywinlhc("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;"
		            		 if(currentUser.bizSystem=='SUPER_SYSTEM'){
		            			 str += "<a class='btn btn-danger btn-sm btn-del' onclick='lotteryWinLhcPage.deleteLotteryWinLhcById("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";
		            		 }
		            		 
		            		 return str;
	                	 },
		            },*/
	            ]
	}).api(); 
}

LotteryWinLhcPage.prototype.initTableHTML = function(obj) {
	var readOnly = "";
	if(currentUser.bizSystem != "SUPER_SYSTEM"){
		readOnly = "readonly='readonly'";
	}
	var lhTable1 = $("#lhTable1");
	var lhTable2 = $("#lhTable2");
	var lhTable3 = $("#lhTable3");
	lhTable1.html("");
	lhTable2.html("");
	lhTable3.html("");
	lotteryWinLhcPage.param.codeNum = obj.length;
	for(var i =0;i<obj.length;i=i+3){
		var tr1 = "<tr>";
		var tr2 = "<tr>";
		var tr3 = "<tr>";
		k=i;
		if(currentUser.bizSystem == "SUPER_SYSTEM"){
			tr1+= "<td><input id='cid"+k+"' type='hidden' value='"+obj[k].id+"' />" +
					"<input id='code"+k+"' type='text' value='"+obj[k].code+"' "+readOnly+" style='background-color: #b5ecc4;'/></td>";
			tr1+= "<td><input id='codeDes"+k+"' type='text' value='"+obj[k].codeDes+"' "+readOnly+" /></td>";
		}else{
			tr1+= "<td style='background-color: #b5ecc4;'><input id='cid"+k+"' type='hidden' value='"+obj[k].id+"' />"+obj[k].codeDes+"</td>";
		}
		
		tr1+= "<td><input id='winMoney"+k+"' type='text' value='"+obj[k].winMoney+"' /></td>";
		tr1+= "<td><input id='returnPercent"+k+"' type='text' value='"+obj[k].returnPercent+"' /> %</td></tr>";
		k=i+1;
		if(k<obj.length){
			if(currentUser.bizSystem == "SUPER_SYSTEM"){
				tr2+= "<td><input id='cid"+k+"' type='hidden' value='"+obj[k].id+"' />" +
				      "<input id='code"+k+"' type='text' value='"+obj[k].code+"' "+readOnly+" style='background-color: #b5ecc4;'/></td>";	
				tr2+= "<td><input id='codeDes"+k+"' type='text' value='"+obj[k].codeDes+"' "+readOnly+" /></td>";
			}else{
				tr2+= "<td style='background-color: #b5ecc4;'><input id='cid"+k+"' type='hidden' value='"+obj[k].id+"' />"+obj[k].codeDes+"</td>";
			}
			
			tr2+= "<td><input id='winMoney"+k+"' type='text' value='"+obj[k].winMoney+"' /></td>";
			tr2+= "<td><input id='returnPercent"+k+"' type='text' value='"+obj[k].returnPercent+"' /> %</td></tr>";	
		}
		k=i+2;
        if(k<obj.length){
        	if(currentUser.bizSystem == "SUPER_SYSTEM"){
    			tr3+= "<td><input id='cid"+k+"' type='hidden' value='"+obj[k].id+"' />" +
				      "<input id='code"+k+"' type='text' value='"+obj[k].code+"' "+readOnly+" style='background-color: #b5ecc4;'/></td>";	
    			tr3+= "<td><input id='codeDes"+k+"' type='text' value='"+obj[k].codeDes+"' "+readOnly+" /></td>";
			}else{
				tr3+= "<td style='background-color: #b5ecc4;'><input id='cid"+k+"' type='hidden' value='"+obj[k].id+"' />"+obj[k].codeDes+"</td>";
			}
    		tr3+= "<td><input id='winMoney"+k+"' type='text' value='"+obj[k].winMoney+"' /></td>";
    		tr3+= "<td><input id='returnPercent"+k+"' type='text' value='"+obj[k].returnPercent+"' /> %</td></tr>";
		} 
        lhTable1.append(tr1);
        lhTable2.append(tr2);
        lhTable3.append(tr3);
	}
}



/**
 * 条件查询赔率
 */
LotteryWinLhcPage.prototype.queryConditionLotteryCodes = function(){
	lotteryWinLhcPage.queryParam = getFormObj($("#lotteryWinLhcForm"));
	//查询条件取玩法的值字段
	lotteryWinLhcPage.queryParam.lotteryTypeProtype = $("#lotteryNameId").val();
	//名称不作为查询条件
	lotteryWinLhcPage.queryParam.lotteryName = null;
	managerLotteryWinLhcAction.getLotteryWinLhcByQueryCondition(lotteryWinLhcPage.queryParam,function(r){
		if (r[0] != null && r[0] == "ok") {
			lotteryWinLhcPage.initTableHTML(r[1]);
			
			$('.table-block table tr td input').focus(function(){
				$('.table-block table tr').removeClass('on')
				$(this).parents('tr').addClass('on');
			});
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("查询数据失败.");
		}
	});
};

//查询配置数据
LotteryWinLhcPage.prototype.closeLayer=function(index){
	layer.close(index);
	lotteryWinLhcPage.queryConditionLotteryCodes(); 
}

LotteryWinLhcPage.prototype.getLotteryWinLhcData = function(){
	var lhcArr = new Array();
	var num = lotteryWinLhcPage.param.codeNum;
	for(var i=0 ;i<num;i++){
		var lotteryWinLhc = {}
		var cid = $("#cid"+i).val();
		if(currentUser.bizSystem == "SUPER_SYSTEM"){
			var code = $("#code"+i).val();
			if(code == null || code == ""){
				swal("号码不为空");
				$("#code"+i).focus();
				return ;
			}
			lotteryWinLhc.code= code;
			var codeDes = $("#codeDes"+i).val();
			if(codeDes == null || codeDes == ""){
				swal("描述不为空");
				$("#codeDes"+i).focus();
				return ;
			}
			lotteryWinLhc.codeDes= codeDes;
		}
		
		var winMoney = $("#winMoney"+i).val();
		var returnPercent = $("#returnPercent"+i).val();
		
		if(winMoney == null || winMoney == ""){
			swal("赔率不为空");
			$("#winMoney"+i).focus();
			return ;
		}
		if(returnPercent == null || returnPercent == ""){
			swal("返水不为空");
			$("#returnPercent"+i).focus();
			return ;
		}
		lotteryWinLhc.id= cid;
		lotteryWinLhc.winMoney= winMoney;
		lotteryWinLhc.returnPercent= returnPercent;
		lhcArr[i]=lotteryWinLhc;
	}
	
	return lhcArr;
}


/**
 * 编辑修改赔率配置数据
 */
LotteryWinLhcPage.prototype.editLotteryWinLhc = function(){
	
	var lhcArr = lotteryWinLhcPage.getLotteryWinLhcData();
	
	
	
	var lotteryWinLhcParam = {};
	lotteryWinLhcParam.lotteryWinLhcArr = lhcArr;
	

	
	managerLotteryWinLhcAction.updateLotteryWinLhcByCodes(lotteryWinLhcParam,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	                lotteryWinLhcPage.closeLayer(index);
		   	    });
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("修改奖金配置数据失败.");
		}
    });
};




/**
 * 删除对应的赔率数据
 */
LotteryWinLhcPage.prototype.deleteLotteryWinLhcById = function(lotteryWinLhcId){
	managerLotteryWinLhcAction.deleteLotteryWinLhcById(lotteryWinLhcId,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({ title: "提示", text: "删除成功",type: "success"});
			lotteryWinLhcPage.table.ajax.reload();
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("查询奖金期号数据失败.");
		}
    });
};

/**
 * 编辑赔率信息
 */
LotteryWinLhcPage.prototype.editLotterywinlhc = function(id){
	layer.open({
		type: 2,
		title: '编辑六合彩赔率',
		maxmin: false,
		shadeClose: true,
		//点击遮罩关闭层
		area: ['70%', '80%'],
		content:contextPath + '/managerzaizst/lotterywinlhc/'+ id +'/lotterywinlhc_edit.html',
	})
}

/**
 * 添加赔率信息
 */
LotteryWinLhcPage.prototype.addLotterywinlhc = function(){
	layer.open({
		type: 2,
		title: '新增六合彩赔率',
		maxmin: false,
		shadeClose: true,
		//点击遮罩关闭层
		area: ['70%', '80%'],
		content:contextPath + '/managerzaizst/lotterywinlhc/lotterywinlhc_add.html',
	})
}
