function LotteryWinLhcPage(){}
var lotteryWinLhcPage = new LotteryWinLhcPage();

/**
 * 查询参数
 */
lotteryWinLhcPage.queryParam = {
	bizSystem : null,
	lotteryTypeProtype : null,
	lotteryName : null
};

//分页参数
lotteryWinLhcPage.pageParam = {
    pageNo : 1,
    pageSize : defaultPageSize
};

$(document).ready(function() {
	lotteryWinLhcPage.initTableData();
});

/**
 * 初始化列表数据
 */
LotteryWinLhcPage.prototype.initTableData = function() {
	lotteryWinLhcPage.queryParam = getFormObj($("#lotteryCodeForm"));
	//查询条件取玩法的值字段
	lotteryWinLhcPage.queryParam.lotteryTypeProtype = $("#lotteryNameId").val();
	//名称不作为查询条件
	lotteryWinLhcPage.queryParam.lotteryName = null;
	lotteryWinLhcPage.table = $('#lotteryWinLhcTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			lotteryWinLhcPage.pageParam.pageSize = data.length;//页面显示记录条数
			lotteryWinLhcPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerLotteryWinLhcAction.getLotteryWinLhcByPage(lotteryWinLhcPage.queryParam,lotteryWinLhcPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询奖金期号数据失败.");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data":null},
		            {"data":"bizSystemName",
		            	"bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "lotteryName"},
		            {"data": "code"},
		            {"data": "codeDes"},
		            {"data": "winMoney"},
		            {"data": "returnPercent",
		            	 render: function(data, type, row, meta) {
		            		 	return data +"%";
		            		 },
		            	 },
		            {"data":"id",
		            	 render: function(data, type, row, meta) {
		            		 var str="";
		            		 str += "<a class='btn btn-info btn-sm btn-bj' onclick='lotteryWinLhcPage.editLotterywinlhc("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;"
		            		 if(currentUser.bizSystem=='SUPER_SYSTEM'){
		            			 str += "<a class='btn btn-danger btn-sm btn-del' onclick='lotteryWinLhcPage.deleteLotteryWinLhcById("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";
		            		 }
		            		 
		            		 return str;
	                	 },
		            },
	            ]
	}).api(); 
}

/**
 * 条件查询赔率
 */
LotteryWinLhcPage.prototype.queryConditionLotteryCodes = function(){
	lotteryWinLhcPage.queryParam = getFormObj($("#lotteryWinLhcForm"));
	//查询条件取玩法的值字段
	lotteryWinLhcPage.queryParam.lotteryTypeProtype = $("#lotteryNameId").val();
	//名称不作为查询条件
	lotteryWinLhcPage.queryParam.lotteryName = null;
	lotteryWinLhcPage.table.ajax.reload();
};

//查询配置数据
LotteryWinLhcPage.prototype.closeLayer=function(index){
	layer.close(index);
	lotteryWinLhcPage.queryConditionLotteryCodes(); 
}

/**
 * 删除对应的赔率数据
 */
LotteryWinLhcPage.prototype.deleteLotteryWinLhcById = function(lotteryWinLhcId){
	managerLotteryWinLhcAction.deleteLotteryWinLhcById(lotteryWinLhcId,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({ title: "提示", text: "删除成功",type: "success"});
			lotteryWinLhcPage.table.ajax.reload();
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("查询奖金期号数据失败.");
		}
    });
};

/**
 * 编辑赔率信息
 */
LotteryWinLhcPage.prototype.editLotterywinlhc = function(id){
	layer.open({
		type: 2,
		title: '编辑六合彩赔率',
		maxmin: false,
		shadeClose: true,
		//点击遮罩关闭层
		area: ['70%', '80%'],
		content:contextPath + '/managerzaizst/lotterywinlhc/'+ id +'/lotterywinlhc_edit.html',
	})
}

/**
 * 添加赔率信息
 */
LotteryWinLhcPage.prototype.addLotterywinlhc = function(){
	layer.open({
		type: 2,
		title: '新增六合彩赔率',
		maxmin: false,
		shadeClose: true,
		//点击遮罩关闭层
		area: ['70%', '80%'],
		content:contextPath + '/managerzaizst/lotterywinlhc/lotterywinlhc_add.html',
	})
}
