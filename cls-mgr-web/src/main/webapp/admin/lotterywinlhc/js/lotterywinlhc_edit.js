function EditLotteryWinLhcPage() {
}
var editLotteryWinLhcPage = new EditLotteryWinLhcPage();

editLotteryWinLhcPage.param = {

};

/**
 * 查询参数
 */
editLotteryWinLhcPage.queryParam = {
	lotteryType : null
};


$(document).ready(function() {
	var id = editLotteryWinLhcPage.param.id; 
	if(typeof(id) != "undefined" && id != null){
		editLotteryWinLhcPage.getLotteryWinLhcById(id); // 查询所有的奖金配置数据
	}
});

/**
 * 赋值赔率配置
 * @param id
 */
EditLotteryWinLhcPage.prototype.getLotteryWinLhcById = function(id){
	managerLotteryWinLhcAction.getLotteryWinLhcById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			if(currentUser.bizSystem!="SUPER_SYSTEM"){
				$("#lotteryNameId").attr("readOnly","readonly");
				$("#code").attr("readOnly","readonly");
				$("#codeDes").attr("readOnly","readonly");
			}
			$("#bizSystemId").val(r[1].bizSystem);
			$("#bizSystemId").attr("readOnly","readonly");
			$("#lotteryTypeProtypeId").find("option[value='"+r[1].lotteryTypeProtype+"']").attr("selected","selected");
			$("#lotteryNameId").find("option[value='"+r[1].lotteryTypeProtype+"']").attr("selected","selected");
			/*lotteryWinLhcSelectEventPage.selectReverseEvent();*/
			
	/*		$("#lotteryTypeProtypeId option").each(function(){
				var text=$(this).text();
				if(text == r[1].lotteryName){
					$(this).attr("selected","selected");
				}
			})
			lotteryWinLhcSelectEventPage.selectEvent();
			$("#lotteryNameId").find("option[value='"+r[1].lotteryTypeProtype+"']").attr("selected","selected");*/
			$("#code").val(r[1].code);
			$("#codeDes").val(r[1].codeDes);
			$("#winMoney").val(r[1].winMoney);
			$("#returnPercent").val(r[1].returnPercent);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取奖金配置数据失败.");
		}
    });
}

/**
 * 编辑修改赔率配置数据
 */
EditLotteryWinLhcPage.prototype.editLotteryWinLhc = function(){
	var lotteryWinLhc = {};
	lotteryWinLhc.id = editLotteryWinLhcPage.param.id; 
	lotteryWinLhc.bizSystem = $("#bizSystemId").val();
	lotteryWinLhc.lotteryTypeProtype = $("#lotteryNameId").val();
	lotteryWinLhc.lotteryName = $("#lotteryNameId").find("option:selected").text();
	lotteryWinLhc.code = $("#code").val();
	lotteryWinLhc.codeDes = $("#codeDes").val();
	lotteryWinLhc.winMoney = $("#winMoney").val();
	lotteryWinLhc.returnPercent = $("#returnPercent").val();
	
	
	if(currentUser.bizSystem=="SUPER_SYSTEM"){
		if (lotteryWinLhc.bizSystem==null || lotteryWinLhc.bizSystem.trim()=="") {
			swal("请选择子系统!");
			$('#bizSystem').focus();
			return;
		}
	}
	if (lotteryWinLhc.lotteryTypeProtype==null || lotteryWinLhc.lotteryTypeProtype.trim()=="") {
		swal("请选择玩法!");
		$('#lotteryTypeProtypeId').focus();
		return;
	}
	if (lotteryWinLhc.lotteryName==null || lotteryWinLhc.lotteryName.trim()=="") {
		swal("请选择玩法名称!");
		$('#lotteryNameId').focus();
		return;
	}
	if (lotteryWinLhc.code==null || lotteryWinLhc.code.trim()=="") {
		swal("请输入开奖号码数!");
		$('#code').focus();
		return;
	}
	if (lotteryWinLhc.codeDes==null || lotteryWinLhc.codeDes.trim()=="") {
		swal("请输入号码描述!");
		$('#codeDes').focus();
		return;
	}
	if (lotteryWinLhc.winMoney==null || lotteryWinLhc.winMoney.trim()=="") {
		swal("请输入奖金金额!");
		$('#winMoney').focus();
		return;
	}
	if (lotteryWinLhc.returnPercent==null || lotteryWinLhc.returnPercent.trim()=="") {
		swal("请输入返水!");
		$('#returnPercent').focus();
		return;
	}
	
	managerLotteryWinLhcAction.updateLotteryWinLhc(lotteryWinLhc,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.lotteryWinLhcPage.closeLayer(index);
		   	    });
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("修改奖金配置数据失败.");
		}
    });
};
