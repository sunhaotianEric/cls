<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
    
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>六合彩管理</title>
        <jsp:include page="/admin/include/include.jsp"></jsp:include>
        <style type="text/css">
        	.table-block table tr:nth-child(2n){background-color: #e4e3e3;}
        </style>
    </head>
    
    <body>
        <div class="animated fadeIn">
            <div class="ibox-content">
                <div class="row m-b-sm m-t-sm">
                    <form class="form-horizontal" id="lotteryWinLhcForm">
                        <div class="row">
                            <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                        	<div class="col-sm-4 biz" >
                                <div class="input-group m-b">
                                    <span class="input-group-addon">商户：</span>
                                     <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control,onChange:lotteryWinLhcPage.queryConditionLotteryCodes()" emptyOption="false"/>
                                </div>
                            </div>
                           	</c:if>
                        	<div class="col-sm-2">
                              <div class="input-group m-b">
                                  <span class="input-group-addon">玩法类别</span>
                                 <%--  <cls:enmuSel name="lotteryName" id="lotteryTypeProtype" options="class:ipt form-control"  className="com.team.lottery.enums.ELotteryKind"/> --%>
	                                 <select class="ipt form-control" name="lotteryTypeProtype" id="lotteryTypeProtypeId" onChange="lotteryWinLhcSelectEventPage.selectEvent()">
	                                      <option value="TM">特码</option>
	                                      <option value="ZM">正码</option>
	                                      <option value="ZMT">正码特</option>
	                                      <option value="ZM16">正码1-6</option>
	                                      <option value="LM">连码</option>
	                                      <option value="BB">半波</option>
	                                      <option value="YXWS">一肖/尾数</option>
	                                      <option value="TMSX">特码生肖</option>
	                                      <option value="HX">合肖</option>
	                                      <option value="LX">连肖</option>
	                                      <option value="WSL">尾数连</option>
	                                      <option value="QBZ">全不中</option>
	                                 </select>
                               </div>
                              </div>
                            <div class="col-sm-2">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">玩法</span>
                                    <select class="ipt form-control" name="lotteryName" id="lotteryNameId" onchange="lotteryWinLhcPage.queryConditionLotteryCodes()">
                                    	<option value="TMA" selected="selected">特A</option>
                                    	<option value="TMB">特B</option>
                                    	<option value="ZMA">正码A</option>
                                    	<option value="ZMB">正码B</option>
                                    	<option value="ZMT1">正一特</option>
                                    	<option value="ZMT2">正二特</option>
                                    	<option value="ZMT3">正三特</option>
                                    	<option value="ZMT4">正四特</option>
                                    	<option value="ZMT5">正五特</option>
                                    	<option value="ZMT6">正六特</option>
                                    	<option value="ZM16">正码1-6</option>
                                    	<option value="LMSQZ">三全中</option>
                                    	<option value="LMSZ22">三中二之中二</option>
                                    	<option value="LMSZ23">三中二之中三</option>
                                    	<option value="LMEQZ">二全中</option>
                                    	<option value="LMEZT">二中特之中特</option>
                                    	<option value="LMEZ2">二中特之中二</option>
                                    	<option value="LMTC">特串</option>
                                    	<option value="LMSZ1">四中一</option>
                                    	<option value="BB">半波</option>
                                    	<option value="YXWS">一肖/尾数</option>
                                    	<option value="TMSX">特码生肖</option>
                                    	<option value="HX2">二肖</option>
                                    	<option value="HX3">三肖</option>
                                    	<option value="HX4">四肖</option>
                                    	<option value="HX5">五肖</option>
                                    	<option value="HX6">六肖</option>
                                    	<option value="HX7">七肖</option>
                                    	<option value="HX8">八肖</option>
                                    	<option value="HX9">九肖</option>
                                    	<option value="HX10">十肖</option>
                                    	<option value="HX11">十一肖</option>
                                    	<option value="LXELZ">二肖连中</option>
                                    	<option value="LXSLZ">三肖连中</option>
                                    	<option value="LXSILZ">四肖连中</option>
                                    	<option value="LXWLZ">五肖连中</option>
                                    	<option value="LXELBZ">二肖连不中</option>
                                    	<option value="LXSLBZ">三肖连不中</option>
                                    	<option value="LXSILBZ">四肖连不中</option>
                                    	<option value="WSLELZ">二尾连中</option>
                                    	<option value="WSLSLZ">三尾连中</option>
                                    	<option value="WSLSILZ">四尾连中</option>
                                    	<option value="WSLELBZ">二尾连不中</option>
                                    	<option value="WSLSLBZ">三尾连不中</option>
                                    	<option value="WSLSILBZ">四尾连不中</option>
                                    	<option value="QBZ5">五不中</option>
                                    	<option value="QBZ6">六不中</option>
                                    	<option value="QBZ7">七不中</option>
                                    	<option value="QBZ8">八不中</option>
                                    	<option value="QBZ9">九不中</option>
                                    	<option value="QBZ10">十不中</option>
                                    	<option value="QBZ11">十一不中</option>
                                    	<option value="QBZ12">十二不中</option>
                                    </select>
                            </div>
                         </div>
                        <div class="col-sm-2"></div>
                        <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
	                         <div class="col-sm-2">
	                         <div class="form-group text-right">
	                                 <button type="button" class="btn btn-outline btn-default btn-js" onclick="lotteryWinLhcPage.queryConditionLotteryCodes()"><i class="fa fa-search"></i>&nbsp;查 询</button>
	                             	 <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
	                             	 <button type="button" class="btn btn-outline btn-default btn-kj" onclick="lotteryWinLhcPage.addLotterywinlhc()"><i class="fa fa-plus"></i>&nbsp;新增</button>
	                             	 </c:if>
	                             </div>
	                         </div>
                        </c:if>
                        <c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
	                         <div class="col-sm-8">
	                             <div class="form-group text-right">
	                                 <button type="button" class="btn btn-outline btn-default btn-js" onclick="lotteryWinLhcPage.queryConditionLotteryCodes()"><i class="fa fa-search"></i>&nbsp;查 询</button>
	                             </div>
	                         </div>
                        </c:if>
                         </div>
                    </form>
                    </div>
                    <div class="table-block clear">
	                    <table class="table">
	                        <thead>
	                        <tr>
	                            <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
	                             <th>号码</th>
	                            </c:if>
	                            <th>描述</th>
	                            <th>赔率</th>
	                            <th>返水</th>
	                        </tr>
	                        </thead>
	                        <tbody id="lhTable1">
	                        	
	                        </tbody>
	                    </table>
	                    <table class="table">
	                        <thead>
	                        <tr>
	                            <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
	                             <th>号码</th>
	                            </c:if>
	                            <th>描述</th>
	                            <th>赔率</th>
	                            <th>返水</th>
	                        </tr>
	                        </thead>
	                        <tbody id="lhTable2">
		                       
	                        </tbody>
	                    </table>
	                    <table class="table">
	                        <thead>
	                        <tr>
	                            <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
	                             <th>号码</th>
	                            </c:if>
	                            <th>描述</th>
	                            <th>赔率</th>
	                            <th>返水</th>
	                        </tr>
	                        </thead>
	                        <tbody id="lhTable3">
		                        
	                        </tbody>
	                    </table>
                    </div>
                </div>
            </div>
            <div class="fixed-footer">
               	<div class="row-edit"><b style="font-size:15px;">快捷修改：</b>
               		<span>赔率：把 <input type="text"  class="oldValue"/> 改成 <input type="text" class="newValue" /><button class="btn btn-sm btn-primary btn-peilv">修改</button></span>
               		<span>返水：把 <input type="text"  class="oldValue"/> 改成 <input type="text" class="newValue" /> <button class="btn btn-sm btn-primary btn-fanshui">修改</button></span>
               	</div>
               	<div class="btn-block"><button class="btn btn-sm btn-primary m-t-n-xs" type="submit" onclick="lotteryWinLhcPage.editLotteryWinLhc()"><strong>确认提交</strong></button> （友情提示：修改完需确认提交）</span></div>
            </div>
            
		    <!-- dwr -->
		    <script type='text/javascript' src='<%=path%>/dwr/interface/managerLotteryWinLhcAction.js'></script>
            <!-- js -->
	 	    <script type="text/javascript" src="<%=path%>/admin/lotterywinlhc/js/selectEvent.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script> 
		    <script type="text/javascript" src="<%=path%>/admin/lotterywinlhc/js/lotterywinlhc1.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    </body>

</html>
