function UserOperateLogPage(){}
var userOperateLogPage = new UserOperateLogPage();

/**
 * 查询参数
 */
userOperateLogPage.param = {
	bizSystem : null,
	userName : null,
	operatorIp : null,
	modelName : null,
	operateDesc : null,
	logtimeStart : null,
	logtimeEnd : null  
};

//分页参数
userOperateLogPage.pageParam = {
	pageNo : 1,
	pageSize : defaultPageSize
};


$(document).ready(function() {
	//查询所有的操作日志数据
	userOperateLogPage.getAllRecordLogs();
	
})

UserOperateLogPage.prototype.findRecordLogs = function(){
	userOperateLogPage.param = getFormObj($("#recordLogQuery"));
	userOperateLogPage.table.ajax.reload();
}

/**
 * 加载所有的登陆日志数据
 */
UserOperateLogPage.prototype.getAllRecordLogs = function(){
	userOperateLogPage.param = getFormObj($("#activityQuery"));
	userOperateLogPage.table =  $('#activityTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"ajax":function (data, callback, settings) {
			userOperateLogPage.pageParam.pageSize = data.length;//页面显示记录条数
			userOperateLogPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
			managerUserOperateLogAction.getAllUserOperateLogs(userOperateLogPage.param,userOperateLogPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				}; 
				
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询奖金配置数据失败.");
				}
				callback(returnData);
		    });
		},
	"columns": [
				{"data":"modelName"},
	            {"data":"userName"},
	            {"data":"operateDesc",
	            	 render: function(data, type, row, meta) {
	            		 if(data.length > 45){
	            			 var title =  "<span title='"+data+"'>"+data.substring(0,45)+"．．．</span>";
	            			 return title;
	            		 }else{
	            			 return data;
	            		 }
	            	 },
	            },
	            {"data":"operatorIp"},
	            {"data":"createDateStr"},  
	     ]
	}).api();
}

/**
 * 按日期查询数据
 */
UserOperateLogPage.prototype.findRecordlogByQuery = function(dateRange){
	if(dateRange != undefined && dateRange != null && dateRange != ''){
		commonPage.setDateRangeTimeZero(dateRange, "startime", "endtime",2);	
	}
	userOperateLogPage.param = getFormObj($("#recordLogQuery"));
	userOperateLogPage.table.ajax.reload();
};