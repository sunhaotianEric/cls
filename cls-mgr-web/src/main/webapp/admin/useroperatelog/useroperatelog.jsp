<%@page import="com.team.lottery.enums.EUserOperateLogModel"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>管理员后台操作记录</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    <link rel="shortcut icon" href="favicon.ico">
</head>

<body >
<div class="animated fadeIn">
    <div class="ibox-content">
        <div class="row m-b-sm m-t-sm">
            <form class="form-horizontal" id="recordLogQuery">
                <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
	                <div class="row">
		              <div class="col-sm-3">
		                  <div class="input-group m-b">
		                      <span class="input-group-addon">操作模块</span>
		                      	 <cls:enmuSel name="modelName" className="com.team.lottery.enums.EUserOperateLogModel" id="modelName" options="class:'ipt form-control'"/>
		                  </div>
		              </div>
		              <div class="col-sm-3">
		                  <div class="input-group m-b">
		                      <span class="input-group-addon">用户名</span>
		                      <input type="text" value="" class="form-control" name="userName" id="userName"></div>
		              </div>
		           </div>
		           <div class="row">
			           <div class="col-sm-5">
		                     <div class="input-group m-b">
		                         <span class="input-group-addon">时间</span>
		                         <div class="input-daterange input-group" id="datepicker" style="width:100%">
		                           <input class="form-control layer-date" placeholder="开始日期" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" name="logtimeStart" id="startime">
				                    <span class="input-group-addon">到</span>
				                   <input class="form-control layer-date" placeholder="结束日期" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" name="logtimeEnd" id="endtime">
		                       </div>
		                  </div>
		              </div>
		               <div class="col-sm-3">
                       	 <div class="form-group nomargin">
					            <button type="button" class="btn btn-xs btn-info" onclick="userOperateLogPage.findRecordlogByQuery(1)">今 天</button>
					            <button type="button" class="btn btn-xs btn-danger" onclick="userOperateLogPage.findRecordlogByQuery(-1)">昨 天</button>
					        	<button type="button" class="btn btn-xs btn-warning" onclick="userOperateLogPage.findRecordlogByQuery(-7)">最近7天</button><br/>
					        	<button type="button" class="btn btn-xs btn-success" onclick="userOperateLogPage.findRecordlogByQuery(30)">本 月</button>
					        	<button type="button" class="btn btn-xs btn-primary" onclick="userOperateLogPage.findRecordlogByQuery(-30)">上 月</button>
                           </div>
                       </div>
                       <div class="col-sm-2">
		                  <div class="form-group text-right">
		                      <button type="button" class="btn btn-outline btn-default btn-js" onclick="userOperateLogPage.findRecordLogs();" id="bonusButton"><i class="fa fa-search"></i>&nbsp;查 询</button>
		                  </div>
		              </div>
	          	</div>
          	</c:if>
          	<c:if test="${admin.bizSystem != 'SUPER_SYSTEM'}">
          		<div class="row">
		              	<div class="col-sm-3">
		                  <div class="input-group m-b">
		                      <span class="input-group-addon">操作模块</span>
		                      	 <cls:enmuSel name="modelName" className="com.team.lottery.enums.EUserOperateLogModel" id="modelName" options="class:'ipt form-control'"/>
		                  </div>
		              	</div>
			           <div class="col-sm-3">
		                  <div class="input-group m-b">
		                      <span class="input-group-addon">用户名</span>
		                      <input type="text" value="" class="form-control" name="userName" id="userName"></div>
		              </div>
		           </div>
		           <div class="row">
		              <div class="col-sm-5">
		                     <div class="input-group m-b">
		                         <span class="input-group-addon">时间</span>
		                         <div class="input-daterange input-group" id="datepicker" style="width:100%">
		                           <input class="form-control layer-date" placeholder="开始日期" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" name="logtimeStart" id="startime">
				                    <span class="input-group-addon">到</span>
				                   <input class="form-control layer-date" placeholder="结束日期" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" name="logtimeEnd" id="endtime">
		                       </div>
		                  </div>
		              	</div>
		              	<div class="col-sm-5">
                       	 <div class="form-group nomargin">
					            <button type="button" class="btn btn-xs btn-info" onclick="userOperateLogPage.findRecordlogByQuery(1)">今 天</button>
					            <button type="button" class="btn btn-xs btn-danger" onclick="userOperateLogPage.findRecordlogByQuery(-1)">昨 天</button>
					        	<button type="button" class="btn btn-xs btn-warning" onclick="userOperateLogPage.findRecordlogByQuery(-7)">最近7天</button><br/>
					        	<button type="button" class="btn btn-xs btn-success" onclick="userOperateLogPage.findRecordlogByQuery(30)">本 月</button>
					        	<button type="button" class="btn btn-xs btn-primary" onclick="userOperateLogPage.findRecordlogByQuery(-30)">上 月</button>
                           </div>
                       </div>
		              <div class="col-sm-2">
		                  <div class="form-group text-right mb0">
		                      <button type="button" class="btn btn-outline btn-default btn-js" onclick="userOperateLogPage.findRecordLogs();" id="bonusButton"><i class="fa fa-search"></i>&nbsp;查 询</button>
		                  </div>
		              </div>
	          	</div>
          	</c:if>
      </form>
      </div>
      <div class="jqGrid_wrapper">
          <div class="ui-jqgrid ">
              <div class="ui-jqgrid-view">
                  <div class="ui-jqgrid-hdiv">
                      <div class="ui-jqgrid-hbox" style="padding-right:0">
                            <table class="ui-jqgrid-htable ui-common-table table table-bordered" id="activityTable">
                                <thead>
                                    <tr class="ui-jqgrid-labels">
                                        <th class="ui-th-column ui-th-ltr">商户</th>
                                        <th class="ui-th-column ui-th-ltr">	
                                            <div class="ui-jqgrid-sortable">操作模块</div></th>
                                        <th class="ui-th-column ui-th-ltr">用户名</th>
                                        <th class="ui-th-column ui-th-ltr">
                                            <div class="ui-jqgrid-sortable">操作内容</div></th>
                                        <th class="ui-th-column ui-th-ltr">操作IP</th>
                                        <th class="ui-th-column ui-th-ltr">
                                            <div class="ui-jqgrid-sortable">操作时间</div></th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

 <!-- js -->
 <script type="text/javascript" src="<%=path%>/admin/useroperatelog/js/useroperatelog.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
 <!-- dwr -->
 <script type='text/javascript' src='<%=path%>/dwr/interface/managerUserOperateLogAction.js'></script>
</body>
</html>

