<%@page import="com.team.lottery.vo.BizSystem"%>
<%@page import="com.team.lottery.service.BizSystemService"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="java.applet.AppletContext"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>  
    <head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>删除商户</title>
    <link rel="shortcut icon" href="favicon.ico">
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    <%
	 String id = request.getParameter("id");  
     String bizSystemName=request.getParameter("bizSystem");
     BizSystemService bizSystemService=ApplicationContextUtil.getBean(BizSystemService.class);
     BizSystem bizSystem=bizSystemService.selectByPrimaryKey(Long.parseLong(id));
	%>
</head>
<body>
	<div class="wrapper wrapper-content animated fadeIn">
		<form action="javascript:void(0)">
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group nomargin text-left">
						&nbsp;&nbsp;&nbsp;&nbsp;<font color="red" id="bizSystem">确认删除[<%=bizSystem.getBizSystemName() %>]商户的全部数据吗？操作后将不可恢复，请谨慎操作</font>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group nomargin text-left">
						&nbsp;
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group nomargin text-left">
					&nbsp;
					</div>
				</div>
				<div class="col-sm-6">
					<div class="input-group m-b">
						<span class="input-group-addon">执行密码</span> 
						<input id="password" name="password" type="text" value="" class="form-control">
					</div>
				</div>
				<div class="col-sm-12">
					<div class="row">
						<div class="col-sm-6 text-center">
							<button type="button" class="btn btn-w-m btn-white"	onclick="delBizSystemPage.delbizSystem()">确认</button>
							<input type="reset" class="btn btn-w-m btn-white" onclick="delBizSystemPage.cancel()" value="取消" />
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>

	<script type="text/javascript" src="<%=path%>/admin/bizsystem/js/delbizsystem.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerBizSystemAction.js'></script>
     <script type="text/javascript">
     delBizSystemPage.param.id = <%=id%>;
     delBizSystemPage.param.bizSystemName='<%=bizSystemName%>';
	</script>
</body>
</html>