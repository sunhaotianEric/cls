<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>  
    <head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>配置编辑</title>
    <link rel="shortcut icon" href="favicon.ico">
    <jsp:include page="/admin/include/include.jsp"></jsp:include>

    <%
	 String id = request.getParameter("id");  
	%>
</head>
  <body class="gray-bg">
        <div class="wrapper wrapper-content animated fadeIn">
         <form action="javascript:void(0)">
            <div class="row">
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">商户英文简称</span>
                        <input type="hidden" id="bizsystemId"/>
                        <% if(id!=null){ %>
                        <input id="bizSystem" name="bizSystem" type="text" value="" class="form-control" disabled="disabled" >
	   		            <%}else{ %>
	   		              <input id="bizSystem" name="bizSystem" type="text" value="" class="form-control">
	   		             <%} %>
                        </div>
                </div>
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">商户名称</span>
                        <input id="bizSystemName" name="bizSystemName" type="text" value="" class="form-control"></div>
                </div>
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">描述</span>
                        <input id="bizSystemDesc" name="bizSystemDesc" type="text" value="" class="form-control"></div>
                </div>
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">商户状态</span>
                        <select class="ipt form-control" id="systemStatus" name="systemStatus">
                            <option value="1" selected="selected">正常</option>
                            <option value="0">欠费</option>
                            <option value="2">维护</option></select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">启用状态</span>
                        <select class="ipt form-control" id="enable" name="enable">
                            <option value="1" selected="selected">启用</option>
                            <option value="0">停用</option>
                         </select>
                    </div>
                </div>
        <!--         <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">续费时间</span>
                        <input id="lastRenewTime" name="lastRenewTime" class="form-control layer-date form-control" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"></div>
                </div>
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">到期时间</span>
                        <input id="expireTime" name="expireTime" class="form-control layer-date form-control" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"></div>
                </div> -->
                  <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">维护费用</span>
                        <input id="maintainMoney" name="maintainMoney" type="text" value="" class="form-control"></div>
                 </div>
         <!--        <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">总维护费用</span>
                        <input id="totalMaintainMoney" name="totalMaintainMoney" type="text" value="" class="form-control"></div>
                </div>
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">总分红费用</span>
                        <input id="totalBonusMoney" name="totalBonusMoney" type="text" value="" class="form-control"></div>
                </div> -->
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">最高奖金模式</span>
                        <select class="ipt form-control" id="highestAwardModel" name="highestAwardModel" >
                          <c:forEach var="val" begin="0" end="298" step="2">
	   			            <option value="${2000-val}">${2000-val}</option>
	   			           </c:forEach>
                         </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">最低奖金模式</span>
                        <select class="ipt form-control" id="lowestAwardModel" name="lowestAwardModel">
	   			           <c:forEach var="val" begin="0" end="298" step="2">
	   			            <option value="${1700+val}">${1700+val}</option>
	   			           </c:forEach>
                          </select>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="input-group m-b" >
                        <!-- <span class="input-group-addon">分红比例</span>
                        <input id="bonusScale" name="bonusScale" type="text" value="" class="form-control"> -->
		               	<div class="fenhong" id="trLine">
		               		<div class="fh-title">分红比例：</div>
		               		<div class="fh-edit">
		               			<span><font color="red"><=</font>盈利 <input type="text" class="startValue" id="gain" name ="gain">元</span>
		               			<span><font color="red">收</font>比例  <input type="text" class="scaleValue" id="bonusScale" name ="bonusScale">%</span>
		               			<button class="btn btn-sm btn-primary btn-add" onclick="editBizSystemPage.addItem()">+</button>
		               		</div>
		               	</div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button type="button" class="btn btn-w-m btn-white" onclick="editBizSystemPage.saveData()">提 交</button></div>
                        <div class="col-sm-6 text-center">
                            <!-- <button type="button" class="btn btn-w-m btn-white">重 置</button> -->
                            <input type="reset" class="btn btn-w-m btn-white" value="重 置"/>
                            </div>
                    </div>
                </div> 
            </div>
            </form>
        </div>

      <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/bizsystem/js/editbizsystem.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerBizSystemAction.js'></script>
     <script type="text/javascript">
	editBizSystemPage.param.id = <%=id%>;
	</script>
</body>
</html>

