function EditBizSystemPage(){}
var editBizSystemPage = new EditBizSystemPage();

editBizSystemPage.param = {
   	
};

$(document).ready(function() {
	var bizsystemId = editBizSystemPage.param.id;  //获取查询ID
	if(bizsystemId != null){
		editBizSystemPage.editBizSystem(bizsystemId);
	}
});


/**
 * 保存更新信息
 */
EditBizSystemPage.prototype.saveData = function(){
	var id = $("#bizsystemId").val();
	var bizSystem = $("#bizSystem").val();
	var bizSystemName = $("#bizSystemName").val();
	var bizSystemDesc = $("#bizSystemDesc").val();
	var systemStatus = $("#systemStatus").val();
	//var bonusScale = $("#bonusScale").val();
	var maintainMoney = $("#maintainMoney").val();
	//var lastRenewTime = $("#lastRenewTime").val();
	//var expireTime = $("#expireTime").val();
	//var totalMaintainMoney = $("#totalMaintainMoney").val();
	var lowestAwardModel = $("#lowestAwardModel").val();
	var highestAwardModel = $("#highestAwardModel").val();
	var enable = $("#enable").val();
	var va=/^-?[0-9]*.?[0-9]*$/;//验证正小数
	
	if(bizSystem==null || bizSystem.trim()==""){
		swal("请输入系统英文简称!");
		$("#bizSystem").focus();
		return;
	}
	if(bizSystemName==null || bizSystemName.trim()==""){
		swal("请输入系统名称!");
		$("#bizSystemName").focus();
		return;
	}
//	if(bonusScale==null || bonusScale.trim()==""){
//		swal("请输入分红比例!");
//		$("#bonusScale").focus();
//		return;
//	}
//	if(!va.test(bonusScale))
//	{
//		swal("分红比例不是数字，请输入数字!");
//	  $("#bonusScale").focus();
//	  return;
//	}
//	if(lastRenewTime==null || lastRenewTime.trim()==""){
//		swal("请输入续费日期!");
//		$("#lastRenewTime").focus();
//		return;
//	}
//	if(expireTime==null || expireTime.trim()==""){
//		swal("请输入到期日期!");
//		$("#expireTime").focus();
//		return;
//	}
//	if(expireTime<lastRenewTime)
//	{
//		swal("到期日期不能小于续费日期！请重新输入！");
//		$("#expireTime").focus();
//		return;
//	}
	if(maintainMoney==null || maintainMoney.trim()==""){
		swal("请输入维护费用!");
		$("#maintainMoney").focus();
		return;
	}
	if(!va.test(maintainMoney))
	{
		swal("维护费用不是数字，请输入数字!");
		  $("#maintainMoney").focus();
	      return;
	}
	if(highestAwardModel<=lowestAwardModel){
		swal("最低奖金模式必须得小于最高奖金模式");
		$("#totalMaintainMoney").focus();
		return;
	}
	
	var bizSystems = {};
	bizSystems.id = editBizSystemPage.param.id;
	bizSystems.bizSystem = bizSystem;
	bizSystems.bizSystemName = bizSystemName;
	bizSystems.bizSystemDesc = bizSystemDesc;
	bizSystems.systemStatus = systemStatus;
	bizSystems.enable = enable;
	//bizSystems.bonusScale = bonusScale;
	bizSystems.maintainMoney = maintainMoney;
	bizSystems.highestAwardModel = highestAwardModel;
	bizSystems.lowestAwardModel = lowestAwardModel;
	var bizSystemBonusConfigList = new Array();
	var bizSystemBonusConfig = {};
	var k = 0;
	$(".fh-edit").each(function(i){
		var gain = $(this).find("input[name='gain']").val();
		var bonusScale = $(this).find("input[name='bonusScale']").val();
		bizSystemBonusConfig = {};
		bizSystemBonusConfig.gain = gain;
		bizSystemBonusConfig.bonusScale = bonusScale;
		bizSystemBonusConfigList[k++] = bizSystemBonusConfig;
	});
	 for(var j =0 ;j<bizSystemBonusConfigList.length;j++){
		 var gain = bizSystemBonusConfigList[j].gain;
		 var bonusScale = bizSystemBonusConfigList[j].bonusScale;
		 if(gain==null || gain.trim()==""){
				swal("请输入盈利!");
				return;
			}
			if(!va.test(gain))
			{
				swal("盈利不是数字，请输入数字!");
			      return;
			}
			if(bonusScale==null || bonusScale.trim()==""){
				swal("请输入比例!");
				return;
			}
			if(!va.test(bonusScale))
			{
				swal("比例不是数字，请输入数字!");
			      return;
			}
			
			gain = Number(gain);
			bonusScale = Number(bonusScale);
			if(bonusScale<0 || bonusScale>100){
				swal("比例范围是0-100");
			      return;
			}
			if(j>0 && gain <= bizSystemBonusConfigList[j-1].gain){
				swal("分红盈利应该是逐级递增，不能比前面的小！");
				return;
			}
			if(j>0 && bonusScale >= bizSystemBonusConfigList[j-1].bonusScale){
				swal("分红比例应该是逐级递减，不能比前面的大！");
				return;
			}
	 }
	
	
	bizSystems.bizSystemBonusConfigList = bizSystemBonusConfigList;
	managerBizSystemAction.saveOrUpdateBizSysten(bizSystems,function(r){
		if (r[0] != null && r[0] == "ok") {
			   swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.bizSystemPage.closeLayer(index);
		   	    });     
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("保存信息失败.");
		}
    });
};


/**
 * 赋值信息
 */
EditBizSystemPage.prototype.editBizSystem = function(id){
	managerBizSystemAction.getBizSystemById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			var lastRenewTime=dateFormat(r[1].lastRenewTime,'yyyy-MM-dd hh:mm:ss');
			var expireTime=dateFormat(r[1].expireTime,'yyyy-MM-dd hh:mm:ss');
			$("#bizsystemId").val(r[1].id);
			$("#bizSystem").val(r[1].bizSystem);
			$("#bizSystemName").val(r[1].bizSystemName);
			$("#bizSystemDesc").val(r[1].bizSystemDesc);
			$("#systemStatus").val(r[1].enable);
			//$("#bonusScale").val(r[1].bonusScale);
			$("#highestAwardModel").val(r[1].highestAwardModel);
			$("#lowestAwardModel").val(r[1].lowestAwardModel);
			$("#highestAwardModel").attr('disabled','disabled');
			$("#lowestAwardModel").attr('disabled','disabled');
//			$("#lastRenewTime").val(lastRenewTime);
//			$("#expireTime").val(expireTime);
			$("#maintainMoney").val(r[1].maintainMoney);
//			$("#totalMaintainMoney").val(r[1].totalMaintainMoney);
//			$("#totalBonusMoney").val(r[1].totalBonusMoney);
			$("#enable").val(r[1].enable);
			
			if(r[2] !=  null && r[2].length>0){
				
				var itemTpl = "";
				$('.fenhong').html(itemTpl);
				var bizSystemBonusConfigList = r[2];
				for(var i=0;i<bizSystemBonusConfigList.length;i++){
					if(r[1].length ==1){
						 itemTpl += '<div class="fh-edit">'
					        + '    <span><font color="red"><=</font>盈利  <input type="text" class="startValue" id="gain" name ="gain" value ="'+bizSystemBonusConfigList[i].gain+'" >元</span>'
					        + '    <span><font color="red">收</font>比例  <input type="text" class="scaleValue" id="bonusScale" name ="bonusScale" value ="'+bizSystemBonusConfigList[i].bonusScale+'">%</span>'
					        + '    </div>';
					       // +'     <button class="btn btn-sm btn-primary btn-add" onclick="editBizSystemPage.addItem()">+</button>';
						// $('.fenhong').append(itemTpl);
					}else {
						 itemTpl += '<div class="fh-edit">'
						        + '    <span><font color="red"><=</font>盈利  <input type="text" class="startValue" id="gain" name ="gain" value ="'+bizSystemBonusConfigList[i].gain+'">元</span>'
						        + '    <span><font color="red">收</font>比例  <input type="text" class="scaleValue" id="bonusScale" name ="bonusScale" value ="'+bizSystemBonusConfigList[i].bonusScale+'">%</span>'
						        + '    </div>';
						       // +'     <button class="btn btn-sm btn-primary btn-delete" onclick="editBizSystemPage.deleteItem(this)">-</button>';
//						 if(i == (bizSystemBonusConfigList.length -1)){
//							 itemTpl +='<button class="btn btn-sm btn-primary btn-add" onclick="editBizSystemPage.addItem()">+</button>'; 
//						 }
						 
					}
				}
				$('.fenhong').append('<div class="fh-title">分红比例：</div>'+itemTpl);
				editBizSystemPage.resetList();
			}
			
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取信息失败.");
		}
    });
};

/**
 * 分红比例删除行
 */
EditBizSystemPage.prototype.deleteItem = function(self){
	$(self).parent('.fh-edit').remove();
    this.resetList();
}

/**
 * 分红比例增加行
 */
EditBizSystemPage.prototype.addItem = function(){
	var itemTpl = '<div class="fh-edit">'
        + '    <span><font color="red"><=</font>盈利  <input type="text" class="startValue" id="gain" name ="gain">元</span>'
        + '    <span><font color="red">收</font>比例  <input type="text" class="scaleValue" id="bonusScale" name ="bonusScale">%</span>'
        + '</div>';
    $('.fenhong').append(itemTpl);
    this.resetList();
}

/**
 * 分红比例列表刷新
 */
EditBizSystemPage.prototype.resetList = function(){
	var $fhEdits = $('.fenhong .fh-edit');
    var length = $fhEdits.length;
    $fhEdits.find('.btn-add').remove();
    $fhEdits.find('.btn-delete').remove();
    $fhEdits.each(function(i, item) {
        if (i == 0) {
            if (length <= 1) {
                $(item).append(' <button class="btn btn-sm btn-primary btn-add" onclick="editBizSystemPage.addItem()">+</button>')
            } else {
                $(item).append(' <button class="btn btn-sm btn-primary btn-delete" onclick="editBizSystemPage.deleteItem(this)">-</button>');
            }
        } else {
             $(item).append(' <button class="btn btn-sm btn-primary btn-delete" onclick="editBizSystemPage.deleteItem(this)">-</button>');
             if (i == length - 1) {
                $(item).append(' <button class="btn btn-sm btn-primary btn-add" onclick="editBizSystemPage.addItem()">+</button>')
             }
        }
    });
}