function BizSystemPage(){}
var bizSystemPage = new BizSystemPage();

bizSystemPage.param = {

};

/**
 * 查询参数
 */
bizSystemPage.queryParam = {
		bizSystemName : null,
		enable : null
};

//分页参数
bizSystemPage.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize
};


$(document).ready(function() {
	bizSystemPage.initTableData(); //初始化查询数据
});



/**
 * 初始化列表数据
 */
BizSystemPage.prototype.initTableData = function() {
	bizSystemPage.queryParam = getFormObj($("#bizsystemForm"));
	bizSystemPage.table = $('#bizsystemTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"scrollX": true,
		"iDisplayLength" : defaultPageSize,
		"ajax":function (data, callback, settings) {
			bizSystemPage.pageParam.pageSize = data.length;//页面显示记录条数
			bizSystemPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerBizSystemAction.getAllBizSystem(bizSystemPage.queryParam,bizSystemPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": "bizSystem"},
					{"data": "secretCode"},
		            {"data": "bizSystemName"},
		            {"data": "bizSystemDesc","bVisible":false},
		            {
		            	"data": "systemStatus",
		            	render: function(data, type, row, meta) {
		            		if (data == "1") {
		            			return '正常';
		            		} else if(data == "1"){
		            			return '欠费';
		            		} else{
		            			return '维护';
		            		}
		            		return data;
	            		}
		            },
		            {"data": "bonusScale"},
		            {
		            	"data": "lastRenewTime",
		            	render: function(data, type, row, meta) {
		            		if(data!=null&&data!=''){
		            		 return dateFormat(data,'yyyy-MM-dd');
		            		}else{
		            		 return "-";
		            		}
	            		}
		            },
		            {
		            	"data": "expireTime",
		            	render: function(data, type, row, meta) {
		            		return dateFormat(data,'yyyy-MM-dd');
	            		}
		            },
		            {"data": "totalMaintainMoney","bVisible":false},
		            {"data": "totalBonusMoney","bVisible":false},
		            {
		            	"data": "enable",
		            	render: function(data, type, row, meta) {
		            		if (data == "1") {
		            			return '启用';
		            		} else {
		            			return "<span style='color: red;'>禁用</span>";
		            		}
		            		return data;
	            		}
		            },
		            {
		            	"data": "createTime",
		            	render: function(data, type, row, meta) {
		            		return dateFormat(data,'yyyy-MM-dd hh:mm:ss');
	            		}
		            },
		            {"data": "updateAdmin","bVisible":false},
		            {
		            	"data": "updateTime","bVisible":false,
		            	render: function(data, type, row, meta) {
		            		return dateFormat(data,'yyyy-MM-dd hh:mm:ss');
	            		}
		            },
		            {
	                	 "data": "id",
	                	 render: function(data, type, row, meta) {
	                		 var str=""
	                			 str="<a class='btn btn-info btn-sm btn-bj' onclick='bizSystemPage.editBizSystem("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;";
	                	         if(row.enable==1){
	                	        	 str += "<a class='btn btn-danger btn-sm btn-del' onclick='bizSystemPage.enableBizSystem("+data+",0)'><i class='fa fa-star' ></i>&nbsp;停用 </a>&nbsp;&nbsp;";
	                	         }else{
	                	        	 str += "<a class='btn btn-info btn-sm btn-bj' onclick='bizSystemPage.enableBizSystem("+data+",1)'><i class='fa fa-star' ></i>&nbsp;启用 </a>&nbsp;&nbsp;";
	                	         }
							 str += "<a class='btn btn-info btn-sm btn-bj' onclick='bizSystemPage.resetSecretCode("+data+")'><i class='fa fa-star' ></i>&nbsp;重置秘钥 </a>&nbsp;&nbsp;";
								 //特殊管理员才显示删除
	                	         if(currentUser.bizSystem=="SUPER_SYSTEM"&&currentUser.username=="ericSun"){
									 str+=  "<a class='btn btn-danger btn-sm btn-del' onclick=bizSystemPage.deleteBizSystemTc("+data+")><i class='fa fa-trash' ></i>&nbsp;删除 </a>";
	                	         }
	                	         return str;
	                	 }
	                }
	            ]
	}).api();
}



/**
 * 查询数据
 */
BizSystemPage.prototype.findBizSystem = function(){
	bizSystemPage.queryParam.bizSystemName = $("#bizSystemName").val();
	bizSystemPage.queryParam.enable = $("#enable").val();
	bizSystemPage.table.ajax.reload();
};


/**
 * 停用，启用
 */
BizSystemPage.prototype.resetSecretCode = function(id){
	swal({
			title: "您确定要重置该系统秘钥？",
			text: "请谨慎操作！",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			cancelButtonText: "取消",
			confirmButtonText: "确定",
			closeOnConfirm: false
		},
		function() {
			managerBizSystemAction.resetSecretCode(id,function(r){
				if (r[0] != null && r[0] == "ok") {
					swal({ title: "提示", text: "操作成功！",type: "success"});
					bizSystemPage.findBizSystem();
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("操作失败.");
				}
			});
		});
};


/**
 * 停用，启用
 */
BizSystemPage.prototype.enableBizSystem = function(id,flag){
	var str="";
	   if(flag==0){
		   str="停用";  
	   }else{
		   str="启用";  
	   }
	   
	   swal({
        title: "您确定要"+str+"该条业务系统？",
        text: "请谨慎操作！",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        cancelButtonText: "取消",
        confirmButtonText: "确定",
        closeOnConfirm: false
    },
    function() {
 	   managerBizSystemAction.enableBizSystem(id,flag,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "操作成功！",type: "success"});
				bizSystemPage.findBizSystem();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("操作失败.");
			}
	    });
    });
};

BizSystemPage.prototype.addBizSystem=function()
{
	
	 layer.open({
         type: 2,
         title: '业务系统新增',
         maxmin: false,
         shadeClose: true,
         //点击遮罩关闭层
         area: ['800px', '420px'],
        // area: ['60%', '45%'],
         content: contextPath + "/managerzaizst/bizsystem/editbizsystem.html",
     /*    btn: ['确 定', '关闭'],
         yes: function(index, layero){
        	 alert(index);
        	 var iframeWin = window['layui-layer-iframe' + index].window;
        	 iframeWin.editBizSystemPage.saveData();
        	 iframeWin.saveData();
        	  },*/
         cancel: function(index){
        	 bizSystemPage.findBizSystem();
      	   }
     });
}

BizSystemPage.prototype.editBizSystem=function(id)
{
	
	  layer.open({
           type: 2,
           title: '业务系统编辑',
           maxmin: false,
           shadeClose: true,
           //点击遮罩关闭层
           area: ['800px', '420px'],
          // area: ['60%', '45%'],
           content: contextPath + "/managerzaizst/bizsystem/"+id+"/editbizsystem.html",
           cancel: function(index){ 
        	   bizSystemPage.findBizSystem();
        	   }
       });
}

BizSystemPage.prototype.closeLayer=function(index){
	layer.close(index);
	bizSystemPage.findBizSystem();
}

BizSystemPage.prototype.deleteBizSystemTc=function(id){
    layer.open({
           type: 2,
           title: '删除业务系统',
           maxmin: false,
           shadeClose: true,
           //点击遮罩关闭层
           area: ['600px', '420px'],
           content: contextPath + "/managerzaizst/bizsystem/delbizsystem.html?id="+id,
           cancel: function(index){
        	   bizSystemPage.findBizSystem();
         }
    });
}
