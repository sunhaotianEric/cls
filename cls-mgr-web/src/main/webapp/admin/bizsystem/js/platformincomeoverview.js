function PlatformIncomeOverview(){}
var platformIncomeOverview = new PlatformIncomeOverview();

platformIncomeOverview.param = {
   	
};

/**
 * 查询参数
 */
platformIncomeOverview.queryParam = {
		bizSystemName : null,
		enable : null
};

//分页参数
platformIncomeOverview.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize
};
platformIncomeOverview.obj ={}

$(document).ready(function() {
	platformIncomeOverview.initTableData(); //初始化查询数据
});



/**
 * 初始化列表数据
 */
PlatformIncomeOverview.prototype.initTableData = function() {
	platformIncomeOverview.queryParam = getFormObj($("#bizsystemForm"));
	platformIncomeOverview.table = $('#bizsystemTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"scrollX": true,
		"iDisplayLength" : defaultPageSize,
		"fnDrawCallback": function (oSettings) {  
			platformIncomeOverview.refreshBizSystemPage(platformIncomeOverview.obj);
	        },
		"ajax":function (data, callback, settings) {
			platformIncomeOverview.pageParam.pageSize = data.length;//页面显示记录条数
			platformIncomeOverview.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerBizSystemAction.getAllBizSystem(platformIncomeOverview.queryParam,platformIncomeOverview.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
					platformIncomeOverview.obj = r[2];
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": "bizSystemName"},
		            {
		            	"data": "totalBonusMoney",
		            	render: function(data, type, row, meta) {
		            		
		            		return row.totalBonusMoney.toFixed(commonPage.param.fixVaue);
	            		}
		            },
		            {
		            	"data": "totalMaintainMoney",
		            	render: function(data, type, row, meta) {
		            		
		            		return row.totalMaintainMoney.toFixed(commonPage.param.fixVaue);
	            		}
		            },
		            {
		            	"data": null,
		            	render: function(data, type, row, meta) {
		            		
		            		return (row.totalMaintainMoney+row.totalBonusMoney).toFixed(commonPage.param.fixVaue);
	            		}
		            }
	            ]
	}).api(); 
}



/**
 * 查询数据
 */
PlatformIncomeOverview.prototype.findBizSystem = function(){
	platformIncomeOverview.queryParam.bizSystem = $("#bizSystem").val();
	platformIncomeOverview.queryParam.enable = $("#enable").val();
	platformIncomeOverview.table.ajax.reload();
};

/**
 * 刷新列表数据
 */
PlatformIncomeOverview.prototype.refreshBizSystemPage = function(obj){
	var reportListObj = $("#bizsystemTable tbody");
	var str = "";
	str += "<tr style='color:red'>";
    str += "  <td>所有用户总计</td>";
    str += "  <td>"+ obj.totalBonusMoney.toFixed(commonPage.param.fixVaue)+"</td>";
    str += "  <td>"+ obj.totalMaintainMoney.toFixed(commonPage.param.fixVaue)+"</td>";
    str += "  <td>"+ (obj.totalMaintainMoney+obj.totalBonusMoney).toFixed(commonPage.param.fixVaue)+"</td>";
    str += "</tr>"
    reportListObj.append(str);
}

