function DelBizSystemPage(){}
var delBizSystemPage = new DelBizSystemPage();

delBizSystemPage.param = {
	   	id:null,
	   	bizSystemName:null
};

$(document).ready(function() {
	if(delBizSystemPage.param.id==null||delBizSystemPage.param.id==""){
		swal("参数错误");
		return;
	}
});

DelBizSystemPage.prototype.delbizSystem=function(){
	var password=$("#password").val();
	if(password==""||password==null){
		swal("执行密码不能够为空");
		return;
	}
	managerBizSystemAction.delbizSystemByAllId(delBizSystemPage.param.id,password,function(r){
		if(r[0]="ok"){
			swal({title:"删除完成！",
		        text:"已成功删除数据",
		        type:"success"},
		      function(){
		        	var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
		            parent.bizSystemPage.closeLayer(index);
			  }
		     )	
		}else{
			swal(r[1]);
		}
	});
}

DelBizSystemPage.prototype.cancel=function(){
	var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	parent.layer.close(index);
}