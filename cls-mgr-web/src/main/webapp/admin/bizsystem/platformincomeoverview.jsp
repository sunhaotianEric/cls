<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>  
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>平台收入总览</title>
        <link rel="shortcut icon" href="favicon.ico">
        <jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>

   <body>
        <div class="animated fadeIn">
            <div class="ibox-content">
                 <div class="row m-b-sm m-t-sm">
                    <form class="form-horizontal" id="bizsystemForm">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">商户</span>
                                    <cls:bizSel name="bizSystem" id="bizSystem" options="class:'ipt form-control'"/>
                                    </div>
                            </div>
                            <div class="col-sm-1">
                                <div class="form-group nomargin text-right">
                                    <button type="button" class="btn btn-outline btn-default btn-js" onclick="platformIncomeOverview.findBizSystem()"><i class="fa fa-search"></i>&nbsp;查 询</button>
                                 </div>
                            </div>
                            <div class="col-sm-8"></div>
                         </div>
                    </form>
                    </div>
                     <div class="jqGrid_wrapper">
                        <div class="ui-jqgrid ">
                            <div class="ui-jqgrid-view">
                                <div class="ui-jqgrid-hdiv">
                                    <div class="ui-jqgrid-hbox" style="padding-right:0">
                                        <table id="bizsystemTable" class="ui-jqgrid-htable ui-common-table table table-bordered">
                                            <thead>
                                                <tr class="ui-jqgrid-labels">
                                                    <th class="ui-th-column ui-th-ltr">商户</th>
                                                    <th class="ui-th-column ui-th-ltr">总分红费用</th>
                                                    <th class="ui-th-column ui-th-ltr">总维护费用</th>
													<th class="ui-th-column ui-th-ltr">总收入</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>     
		         </div>
		      </div>
  <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/bizsystem/js/platformincomeoverview.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerBizSystemAction.js'></script>  
</body>
</html>

