<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>签到设置</title>
<jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
<body>
	<div class="animated fadeIn">
		<div class="ibox-content">
			<div class="row">
				<form class="form-horizontal" id="signinconfigForm">
					<div class="row">
						<c:choose>
							<c:when test="${admin.bizSystem=='SUPER_SYSTEM'}">
								<div class="col-sm-6">
									<div class="input-group m-b">
										<span class="input-group-addon">商户</span>
										<cls:bizSel name="bizSystem" id="bizSystem"
											options="class:ipt form-control" />
									</div>
								</div>
								
							</c:when>
							<c:otherwise>
								<div class="col-sm-6"></div>
							</c:otherwise>
						</c:choose>

						<div class="col-sm-6">
							<div class="form-group  text-right">

								<button type="button" class="btn btn-outline btn-default btn-kj"
									onclick="signinconfigPage.findSigninconfig()">
									<i class="fa fa-search"></i>&nbsp;查 询
								</button>
								&nbsp;&nbsp;&nbsp;&nbsp;

								<button type="button"
									class="btn btn-outline btn-default btn-add"
									onclick="signinconfigPage.addSigninconfig()">
									<i class="fa fa-plus"></i>&nbsp;新增
								</button>
							</div>

						</div>

					</div>
				</form>
			</div>
			<div class="jqGrid_wrapper">
				<div class="ui-jqgrid ">
					<div class="ui-jqgrid-view ">
						<div class="ui-jqgrid-hdiv">
							<div class="ui-jqgrid-hbox" style="padding-right: 0">
								<table id="signinconfigTable"
									class="ui-jqgrid-htable ui-common-table table table-bordered">
									<thead>
										<tr class="ui-jqgrid-labels">
											<th class="ui-th-column ui-th-ltr">序号</th>

											<th class="ui-th-column ui-th-ltr">商户</th>

											<th class="ui-th-column ui-th-ltr">签到天数</th>
											<th class="ui-th-column ui-th-ltr">签到赠送金额</th>
											<th class="ui-th-column ui-th-ltr">创建时间</th>

											<th class="ui-th-column ui-th-ltr">操作</th>
										</tr>
									</thead>

								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- js -->
	<script type="text/javascript"
		src="<%=path%>/admin/signinconfig/js/signinconfig.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
	<!-- dwr -->
	<script type='text/javascript'
		src='<%=path%>/dwr/interface/managerSignInConfigAction.js'></script>
</body>
</html>

