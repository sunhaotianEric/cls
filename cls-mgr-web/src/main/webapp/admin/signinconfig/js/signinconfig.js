function SigninconfigPage(){}
var signinconfigPage = new SigninconfigPage();

signinconfigPage.param = {
		
};
//分页参数
signinconfigPage.pageParam = {
		    pageNo : 1,
		    pageSize : defaultPageSize
};
/**
 * 查询参数
 */
signinconfigPage.queryParam = {
		bizSystem : null
};
$(document).ready(function() {
	signinconfigPage.initTableData();//查询所有的快捷支付数据
});


SigninconfigPage.prototype.initTableData = function(){

	signinconfigPage.queryParam = getFormObj($("#signinconfigForm"));
	signinconfigPage.table = $('#signinconfigTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"bPaginate" : false, 
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			/*signinconfigPage.pageParam.pageSize = data.length;//页面显示记录条数
			signinconfigPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码*/
	    	managerSignInConfigAction.getAllSignInConfig(signinconfigPage.queryParam,signinconfigPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					/*returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
*/					returnData.data = r[1].pageContent;;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询所有的快捷支付数据失败.");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data":null},
		            {"data": "bizSystemName", 
		            	"bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "signinDay"},
		            {"data": "money"},
		            {"data": "createTimeStr"},
		            {
	                	 "data": "id",
	                	 render: function(data, type, row, meta) {
	                		 var str="<a class='btn btn-info btn-sm btn-bj' onclick='signinconfigPage.editSigninconfig("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;" +
	             		 		"<a class='btn btn-danger btn-sm btn-del' onclick='signinconfigPage.delSigninconfig("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";
	                		
	                			 
	                		 return str ;
	                	 }
	                }
	            ]
	}).api();
};

/**
 * 查询数据
 */
SigninconfigPage.prototype.findSigninconfig = function(){
	var bizSystem = $("#bizSystem").val();
	if(bizSystem == ""){
		signinconfigPage.queryParam.bizSystem = null;
	}else{
		signinconfigPage.queryParam.bizSystem = bizSystem;
	}
	signinconfigPage.table.ajax.reload();
};




/**
 * 删除
 */
SigninconfigPage.prototype.delSigninconfig = function(id){
	
	   swal({
           title: "您确定要删除这条信息吗",
           text: "请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "删除",
           closeOnConfirm: false
       },
       function() {
    	   managerSignInConfigAction.delSignInConfig(id,function(r){
   			if (r[0] != null && r[0] == "ok") {
   				swal({ title: "提示", text: "删除成功",type: "success"});
   				signinconfigPage.table.ajax.reload();
   			}else if(r[0] != null && r[0] == "error"){
   				swal(r[1]);
   			}else{
   				swal("删除信息失败.");
   			}
   	    });
       });
};

/**
 * 新增
 */
SigninconfigPage.prototype.addSigninconfig=function()
{
	
	 layer.open({
         type: 2,
         title: '签到设置新增',
         maxmin: false,
         shadeClose: true,
         //点击遮罩关闭层
         area: ['70%', '80%'],
         content: contextPath + "/managerzaizst/signinconfig/signinconfig_edit.html",
         cancel: function(index){ 
      	   signinconfigPage.table.ajax.reload();
      	   }
     });
}
/**
 * 编辑
 */
SigninconfigPage.prototype.editSigninconfig=function(id)
{
	
	  layer.open({
           type: 2,
           title: '签到设置编辑',
           maxmin: false,
           shadeClose: true,
           //点击遮罩关闭层
           area: ['70%', '80%'],
           content: contextPath + "/managerzaizst/signinconfig/"+id+"/signinconfig_edit.html",
           cancel: function(index){ 
        	   signinconfigPage.table.ajax.reload();
        	   }
       });
}

SigninconfigPage.prototype.closeLayer=function(index){
	layer.close(index);
	signinconfigPage.table.ajax.reload();
}

