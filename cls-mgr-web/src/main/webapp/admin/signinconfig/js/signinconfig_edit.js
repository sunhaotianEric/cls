function SigninconfigEditPage(){}
var signinconfigEditPage = new SigninconfigEditPage();

signinconfigEditPage.param = {
   	
};
var initFinsh = false;

$(document).ready(function() {
	var id = signinconfigEditPage.param.id;  //获取查询的ID
	if(id != null){
		signinconfigEditPage.editSigninconfig(id);
	}

	
});

/**
 * 签到设置保存
 */
SigninconfigEditPage.prototype.saveData = function(){
	var signinconfig = {};
	signinconfig.id = $("#id").val();
	signinconfig.signinDay = $("#signinDay").val();
	signinconfig.bizSystem = $("#bizSystem").val();
	signinconfig.money = $("#money").val();
	
	if(currentUser.bizSystem=='SUPER_SYSTEM'){
		if (signinconfig.bizSystem==null || signinconfig.bizSystem.trim()=="") {
			swal("请选择业务系统!");
			$("#bizSystem").focus();
			return;
		} 
	}
	
	if (signinconfig.signinDay==null || signinconfig.signinDay.trim()=="") {
		swal("请输入签到天数!");
		$("#signinDay").focus();
		return;
	}
	if (signinconfig.money==null || signinconfig.money.trim()=="") {
		swal("签到赠送金额!");
		$("#money").focus();
		return;
	}
	managerSignInConfigAction.saveOrUpdateSignInConfig(signinconfig,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.signinconfigPage.closeLayer(index);
		   	    }); 
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("保存签到设置失败!");
		}
    });
};


/**
 * 签到设置编辑
 */
SigninconfigEditPage.prototype.editSigninconfig = function(id){
	managerSignInConfigAction.getSignInConfigById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			var config = r[1];
			$("#id").val(config.id);
			$("#bizSystem").val(config.bizSystem);
			$("#signinDay").val(config.signinDay);
			$("#money").val(config.money);
		}else if(r[0] != null && r[0] == "error"){
			showErrorDlg(jQuery(document.body), r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "获取签到设置信息失败.");
		}
    });
};