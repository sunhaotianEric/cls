<%@page import="com.team.lottery.system.SystemConfigConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" 
	import = "com.team.lottery.enums.EBankInfo" 
	pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>签到设置</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
	<%
	 String id = request.getParameter("id");  //获取查询的商品ID
	%>
	
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeIn">
  <form action="javascript:void(0)">
    <div class="row">
     <input type="hidden" id="id"/>
    <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
    <div class="col-sm-12">
    <div class="input-group m-b">
    <span class="input-group-addon">商户：</span>
    <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
    </div>
    </div>
    </c:if>
   <div class="col-sm-12">
    <div class="input-group m-b">
    <span class="input-group-addon">签到天数：</span>
    <!-- <input type="text" value="" class="form-control" name="signinDay" id="signinDay"> -->
         <select class="form-control" id="signinDay" name="signinDay">
	     <option value="1">1天</option>
	     <option value="2">2天</option>
	     <option value="3">3天</option>
	     <option value="4">4天</option>
	     <option value="5">5天</option>
	     <option value="6">6天</option>
	     <option value="7">7天</option>
	     </select>
    </div>
    </div>
    <div class="col-sm-12">
    <div class="input-group m-b">
    <span class="input-group-addon">签到赠送金额：</span>
    <input type="text" value="" class="form-control" name="money" id="money">
    </div>
    </div>
    <div class="col-sm-12">
     <div class="row">
     <div class="col-sm-6 text-center">
     <button type="button" class="btn btn-w-m btn-white" onclick="signinconfigEditPage.saveData()">提 交</button></div>
      <div class="col-sm-6 text-center">
      <input type="reset" class="btn btn-w-m btn-white" value="重 置"/>
        </div>
       </div>
      </div>
    </div>
  </form>
  </div>
  
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/signinconfig/js/signinconfig_edit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerSignInConfigAction.js'></script>
    <script type="text/javascript">
    signinconfigEditPage.param.id = <%=id %>;
	</script>
</body>
</html>

