<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
	String imgServerUrl = SystemConfigConstant.imgServerUrl;
%>
<!DOCTYPE html>
<html>  
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>商户信息管理</title>
        <jsp:include page="/admin/include/include.jsp"></jsp:include>
        <script type="text/javascript">
	    	var imgServerUrl = '<%= imgServerUrl %>';
	    </script>
</head>
    <body>
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
                    <div class="ibox ">
                        <div class="ibox-content">
                            <div class="row m-b-sm m-t-sm">
                                <form class="form-horizontal" id="queryForm">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">商户</span>
                                                 <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
                                   
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                        </div>
                                        
                                        <div class="col-sm-4">
                                            <div class="form-group nomargin text-right">
                                                <button type="button" class="btn btn-outline btn-default btn-kj" onclick="bizSystemInfoPage.findBizSystemInfo()"><i class="fa fa-search"></i>&nbsp;查 询</button></div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="jqGrid_wrapper">
                                <div class="ui-jqgrid ">
                                    <div class="ui-jqgrid-view">
                                        <div class="ui-jqgrid-hdiv">
                                            <div class="ui-jqgrid-hbox" style="padding-right:0">
                                                <table class="ui-jqgrid-htable ui-common-table table table-bordered" id="bizsysteminfoTable">
                                                    <thead>
                                                        <tr class="ui-jqgrid-labels">
                                                            <th class="ui-th-column ui-th-ltr">商户</th>
                                                            <th class="ui-th-column ui-th-ltr">商户名称</th>
                                                            <th class="ui-th-column ui-th-ltr">模板</th>
                                                            <th class="ui-th-column ui-th-ltr">首推彩种</th>
                                                            <th class="ui-th-column ui-th-ltr">logo图片</th>
                                                            
                                                         <!--    <th class="ui-th-column ui-th-ltr">首页默认彩种</th>
                                                            <th class="ui-th-column ui-th-ltr">客服地址</th>
                                                            <th class="ui-th-column ui-th-ltr">电话号码</th> -->
                                                       
                                                            <th class="ui-th-column ui-th-ltr">创建时间</th>
                                                            <th class="ui-th-column ui-th-ltr">操作人</th>
                                                            <th class="ui-th-column ui-th-ltr">更新时间</th>
                                                            <th class="ui-th-column ui-th-ltr">操作</th></tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!-- js -->
<script type="text/javascript"
	src="<%=path%>/admin/bizsysteminfo/js/bizsysteminfo.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript'
	src='<%=path%>/dwr/interface/managerBizSystemInfoAction.js'></script>
</body>
</html>

