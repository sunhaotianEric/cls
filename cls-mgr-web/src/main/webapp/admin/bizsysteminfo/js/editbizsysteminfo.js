function EditBizSystemInfoPage(){}
var editBizSystemInfoPage = new EditBizSystemInfoPage();

editBizSystemInfoPage.param = {
   	
};
editBizSystemInfoPage.lotteryTypesMap = new JS_OBJECT_MAP();
editBizSystemInfoPage.hotlotteryTypesMap = new JS_OBJECT_MAP();
editBizSystemInfoPage.mobilelotteryTypesMap = new JS_OBJECT_MAP();

editBizSystemInfoPage.pcQuickLotteryKindMap = new JS_OBJECT_MAP();
editBizSystemInfoPage.pcPopularLotteryKindMap = new JS_OBJECT_MAP();
editBizSystemInfoPage.pcIndexPushLotteryKindMap = new JS_OBJECT_MAP();
$(document).ready(function() {
	
	
	editBizSystemInfoPage.initLotteryTypes();
	var bizSystemInfoId = editBizSystemInfoPage.param.id;  //获取查询ID
	if(bizSystemInfoId != null){
		editBizSystemInfoPage.editBizSystemInfo(bizSystemInfoId);
	}
});


/**
 * 保存更新信息
 */
EditBizSystemInfoPage.prototype.saveData = function(){
	
	var bizsystemInfo=getFormObj("#bizsysteminfoForm");
	bizsystemInfo.id=editBizSystemInfoPage.param.id;
	bizsystemInfo.contactUs=editor1.html();
	bizsystemInfo.aboutUs=editor2.html();
	bizsystemInfo.franchiseAgent=editor3.html();
	bizsystemInfo.legalStatement=editor4.html();
	bizsystemInfo.privacyStatement=editor5.html();
	
	if(bizsystemInfo.bizSystemName==null || bizsystemInfo.bizSystemName.trim()==""){
		swal("请输入业务系统名称!");
		$("#bizSystemName").focus();
		return;
	 }
	if(bizsystemInfo.qq==null){
		bizsystemInfo.qq = "";
	 }
	if(bizsystemInfo.pcQuickLotteryKind==null || bizsystemInfo.pcQuickLotteryKind.trim()==""){
		swal("请输入pc快捷彩种!");
		return;
	 }
	if(bizsystemInfo.pcIndexPushLotteryKind==null || bizsystemInfo.pcIndexPushLotteryKind.trim()==""){
		swal("请输入pc首页主推彩种!");
		return;
	 }
	if(bizsystemInfo.pcPopularLotteryKind==null || bizsystemInfo.pcPopularLotteryKind.trim()==""){
		swal("请输入pc热门彩种!");
		return;
	 }
	
	if(bizsystemInfo.hotLotteryKinds==null || bizsystemInfo.hotLotteryKinds.trim()==""){
		swal("请输入热门彩种!");
		return;
	 }
	if(bizsystemInfo.indexDefaultLotteryKinds==null || bizsystemInfo.indexDefaultLotteryKinds.trim()==""){
		swal("请输入首页默认彩种!");
		return;
	 }
	
	if(bizsystemInfo.indexDefaultLotteryKinds.split(",").length>9){
		swal("首页默认彩种不能超过9个！");
		return;
	}
	
	if(bizsystemInfo.indexMobileDefaultLotteryKinds==null || bizsystemInfo.indexMobileDefaultLotteryKinds.trim()==""){
		swal("请输入手机端首页默认彩种!");
		return;
	 }
	
	if(bizsystemInfo.indexMobileDefaultLotteryKinds.split(",").length>12){
		swal("手机端首页默认彩种不能超过11个！");
		return;
	}
	if(bizsystemInfo.customUrl==null || bizsystemInfo.customUrl.trim()==""){
		swal("请输入客服地址!");
		$("#customUrl").focus();
		return;
	}
	
	if(bizsystemInfo.telphoneNum==null || bizsystemInfo.telphoneNum.trim()==""){
		swal("请输入电话号码!");
		$("#telphoneNum").focus();
		return;
	}
	if(bizsystemInfo.hasApp==null || bizsystemInfo.hasApp.trim()==""){
		swal("请输入是否有APP!");
		$("#hasApp").focus();
		return;
	}
	if(bizsystemInfo.siteScript == null){
		bizsystemInfo.siteScript = "";
	}
	
//	if(bizsystemInfo.hasApp==1&&(bizsystemInfo.appBarcodeUrl==null||bizsystemInfo.appBarcodeUrl.trim()=="")){
//		swal("上传appapp端二维码图片!");
//		$("#appBarcodeUrl").focus();
//		return;
//	}
	//否的时候，清空app上传图片内容
	 if(bizsystemInfo.hasApp==0){
		 bizsystemInfo.appAndroidDownloadUrl=null;
		 bizsystemInfo.appStoreIosUrl=null;
		 bizsystemInfo.appBarcodeUrl=null;
		 bizsystemInfo.appIosBarcodeUrl=null;
		 bizsystemInfo.andriodIosDownloadUrl=null;
		 bizsystemInfo.appShowName=null;
		 bizsystemInfo.appDownLogoUrl=null;
	  }else{
		if(bizsystemInfo.appAndroidDownloadUrl == null){
			bizsystemInfo.appAndroidDownloadUrl = "";
		}
		if(bizsystemInfo.appStoreIosUrl == null){
			bizsystemInfo.appStoreIosUrl = "";
		}
		if(bizsystemInfo.andriodIosDownloadUrl ==null){
			bizsystemInfo.andriodIosDownloadUrl ="";
		}
	  }
	bizsystemInfo.appDownLogo=bizsystemInfo.appDownLogoUrl;
	managerBizSystemInfoAction.saveOrUpdateBizSystenInfo(bizsystemInfo,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   if(currentUser.bizSystem=='SUPER_SYSTEM'){
		    		   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
		    		   parent.bizSystemInfoPage.closeLayer(index);
		    	   }
		   	    }); 
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("保存信息失败.");
		}
     });
};


/**
 * 赋值信息
 */
EditBizSystemInfoPage.prototype.editBizSystemInfo = function(id){
	managerBizSystemInfoAction.getBizSystemInfoById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			editBizSystemInfoPage.param.id=r[1].id;
			$("#id").val(r[1].id);
			$("#bizSystemName").val(r[1].bizSystemName);
			$("#templateId").val(r[1].templateId);
			$("#indexLotteryKind").val(r[1].indexLotteryKind);
			$("#logoUrl").val(r[1].logoUrl);
			$("#hasApp").val(r[1].hasApp);
			$("#withdrawSecond").val(r[1].withdrawSecond);
			$("#rechargeSecond").val(r[1].rechargeSecond);
			$("#siteScript").val(r[1].siteScript);
			$("#mobileSiteScript").val(r[1].mobileSiteScript);
			$("#qq").val(r[1].qq);
			
			$("#contactUs").val(r[1].contactUs);
			$("#aboutUs").val(r[1].aboutUs);
			$("#franchiseAgent").val(r[1].franchiseAgent);
			$("#legalStatement").val(r[1].legalStatement);
			$("#privacyStatement").val(r[1].privacyStatement);
			editBizSystemInfoPage.checkTheBox(r[1].indexDefaultLotteryKinds,"indexDefaultLotteryKinds",editBizSystemInfoPage.lotteryTypesMap);
			editBizSystemInfoPage.checkTheBox(r[1].hotLotteryKinds,"hotLotteryKinds",editBizSystemInfoPage.hotlotteryTypesMap);
			editBizSystemInfoPage.checkTheBox(r[1].indexMobileDefaultLotteryKinds,"indexMobileDefaultLotteryKinds",editBizSystemInfoPage.mobilelotteryTypesMap);
			
		
				editBizSystemInfoPage.checkTheBox(r[1].pcQuickLotteryKind,"pcQuickLotteryKind",editBizSystemInfoPage.pcQuickLotteryKindMap);
			
				editBizSystemInfoPage.checkTheBox(r[1].pcIndexPushLotteryKind,"pcIndexPushLotteryKind",editBizSystemInfoPage.pcIndexPushLotteryKindMap);
			
				editBizSystemInfoPage.checkTheBox(r[1].pcPopularLotteryKind,"pcPopularLotteryKind",editBizSystemInfoPage.pcPopularLotteryKindMap);
	
			
			if(r[1].logoUrl){
				$("#logo").attr("src", r[2] + r[1].logoUrl);
				$("#logo").show();
			}
			$("#headLogoUrl").val(r[1].headLogoUrl);
			if(r[1].headLogoUrl){
				$("#headLogo").attr("src",r[2] + r[1].headLogoUrl);
				$("#headLogo").show();	
				
			}
			$("#mobileHeadLogoUrl").val(r[1].mobileHeadLogoUrl);
			if(r[1].mobileHeadLogoUrl){
				$("#mobileHeadLogo").attr("src",r[2] + r[1].mobileHeadLogoUrl);
				$("#mobileHeadLogo").show();	
				
			}
			$("#mobileLogoUrl").val(r[1].mobileLogoUrl);
			if(r[1].mobileLogoUrl){
				$("#mobileLogo").attr("src",r[2] + r[1].mobileLogoUrl);
				$("#mobileLogo").show();	
				
			}
			$("#barcodeUrl").val(r[1].barcodeUrl);
			if(r[1].barcodeUrl){
				$("#barcode").attr("src",r[2] + r[1].barcodeUrl);
				$("#barcode").show();	
			}
			
			$("#faviconUrl").val(r[1].faviconUrl);
			if(r[1].faviconUrl){
				$("#favicon").attr("src", r[2] + r[1].faviconUrl);
				$("#favicon").show();
			}
			
			$("#mobileBarcodeUrl").val(r[1].mobileBarcodeUrl);
			if(r[1].mobileBarcodeUrl){
				$("#mobileBarcode").attr("src", r[2] + r[1].mobileBarcodeUrl);
				$("#mobileBarcode").show();	
				
			}
			if(r[1].hasApp==1){
				
				
				 $("#appAndroidDownloadUrl").val(r[1].appAndroidDownloadUrl);
				
				$("#appAndroidDownloadDiv").show();
			    $("#appBarcodeUrl").val(r[1].appBarcodeUrl);
				if(r[1].appBarcodeUrl){
					$("#appBarcode").attr("src",r[2] + r[1].appBarcodeUrl);
					$("#appBarcode").show();	
					
				}
				 $("#appStoreIosUrl").val(r[1].appStoreIosUrl);
				$("#appStoreIosDiv").show();
				$("#appIosBarcodeUrl").val(r[1].appIosBarcodeUrl);
				if(r[1].appIosBarcodeUrl){
					$("#appIosBarcode").attr("src",r[2] + r[1].appIosBarcodeUrl);
					$("#appIosBarcode").show();	
	
			    }
				$('*[id=file-pretty]').eq(6).show();
				$('#appBarcode_groupDiv').show();
				$('*[id=file-pretty]').eq(7).show();
				$('#appIosBarcode_groupDiv').show();
				
				$('*[id=file-pretty]').eq(5).show();
				$("#appAndriodIosDownloadUrl").show();
				$("#appShowNames").show();
				$("#appDown_Logo").show();
				$("#andriodIosDownloadUrl").val(r[1].andriodIosDownloadUrl);
				$("#appShowname").val(r[1].appShowname);
				$("#appDownLogoUrl").val(r[1].appDownLogo);
				if(r[1].appDownLogo){
					$("#appDownLogo").attr("src",r[2] + r[1].appDownLogo);
					$("#appDownLogo").show();	
				}
			}
			
			$("#customUrl").val(r[1].customUrl);
			$("#telphoneNum").val(r[1].telphoneNum);
			$("#advantageDesc").val(r[1].advantageDesc);
			if(currentUser.bizSystem=='SUPER_SYSTEM')
			{
				$("#bizSystem").val(r[1].bizSystem);
			}
			
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取信息失败.");
		}
    });
};

/**
 * 复选框赋值选中
 * @param typeStr
 */
EditBizSystemInfoPage.prototype.checkTheBox=function (typeStr,eName,tempMap)
{  
    var cbArray = new Array;  
    if(typeStr==null){
    	typeStr="";
    }
     cbArray = typeStr.split(",");  
     var checksDom = $("#"+eName+"_div");
     //初始化
     for(i=0;i<cbArray.length;i++){  
    	 kindStr = cbArray[i];
    	
    	 //手机特殊过滤
    	 if(eName=='indexMobileDefaultLotteryKinds' && kindStr.indexOf("SYXW") < 0){
 	       checksDom.append("<div class=\"sortable-list connectList checkbox i-checks\" style=\"float:left\"><label>" +
 			"<input type='checkbox' id='"+eName+"' name='"+eName+"' value='"+kindStr+"' /> <i></i>"+tempMap.get(kindStr)+"</label></div>");
    	 }else{
    		 if(kindStr){
    			 checksDom.append("<div class=\"sortable-list connectList checkbox i-checks\" style=\"float:left\"><label>" +
     		 			"<input type='checkbox' id='"+eName+"' name='"+eName+"' value='"+kindStr+"' /> <i></i>"+tempMap.get(kindStr)+"</label></div>"); 
    		 }
    	 }
 	      tempMap.remove(kindStr);
     } 
     
      var str=document.getElementsByName(eName);  
        var objarray=str.length;  
         for(i=0;i<cbArray.length;i++){  
            for (j=0;j<objarray;j++){  

                  if(str[j].value == cbArray[i]){  
                     str[j].checked = true;  
                  }  

            }  

       }  
         var keys = tempMap.keys();
        //剩余彩种出来
	     for(k=0;k<keys.length;k++){ 
	    	 var key = keys[k];
	    	 //手机特殊过滤
	    	 if(eName=='indexMobileDefaultLotteryKinds' && key.indexOf("SYXW") < 0){
	 	       checksDom.append("<div class=\"sortable-list connectList checkbox i-checks\" style=\"float:left\"><label>" +
	 			"<input type='checkbox' id='"+eName+"' name='"+eName+"' value='"+key+"' /> <i></i>"+tempMap.get(key)+"</label></div>");
	    	 }else{
	    		 checksDom.append("<div class=\"sortable-list connectList checkbox i-checks\" style=\"float:left\"><label>" +
	    		 			"<input type='checkbox' id='"+eName+"' name='"+eName+"' value='"+key+"' /> <i></i>"+tempMap.get(key)+"</label></div>"); 
	    	 }
	     }  
	     
}


/**
 * 下拉框选则
 * @param typeStr
 */
EditBizSystemInfoPage.prototype.showAppCodeDiv=function ()
{  
	var hasApp=$("#hasApp").val();
	if(hasApp==1){
		$('*[id=file-pretty]').eq(5).show();
		$('*[id=file-pretty]').eq(6).show();
		$('#appBarcode_groupDiv').show();
		$('*[id=file-pretty]').eq(7).show();
		$('#appIosBarcode_groupDiv').show();
		$("#appAndroidDownloadDiv").show();
		$("#appStoreIosDiv").show();
		$("#appAndriodIosDownloadUrl").show();
		$("#appShowNames").show();
	}else{
		$('*[id=file-pretty]').eq(5).hide();
		$('*[id=file-pretty]').eq(6).hide();
		$('#appBarcode_groupDiv').hide();
		$('*[id=file-pretty]').eq(7).hide();
		$('#appIosBarcode_groupDiv').hide();
		$("#appAndroidDownloadDiv").hide();
		$("#appStoreIosDiv").hide();
		$("#appAndriodIosDownloadUrl").hide();
		$("#appShowNames").hide();
	}
}
/**
 * 根据id赋值selelct标签
 * @param id
 */

EditBizSystemInfoPage.prototype.initLotteryTypes=function ()
{
	dwr.engine.setAsync(false); 
	var selDom = $("#indexLotteryKind");
//	var checksDom=$("#indexDefaultLotteryKinds_div");
//	var checksDomhot=$("#hotLotteryKinds_div");
//	var checksDomMobile=$("#indexMobileDefaultLotteryKinds_div");
//	var checksDomMobile=$("#pcQuickLotteryKind_div");
//	var checksDomMobile=$("#pcIndexPushLotteryKind_div");
//	var checksDomMobile=$("#pcPopularLotteryKind_div");
	managerBizSystemInfoAction.getLotteryTypes(function(r){
		if (r[0] != null && r[0] == "ok") {
			LotteryTypesMap=r[1];
			//editBizSystemInfoPage.lotteryTypesMap= r[1];
			var count=1;
			for(var key in LotteryTypesMap){
				selDom.append("<option value='"+key+"'>"+LotteryTypesMap[key]+"</option>");
				
				editBizSystemInfoPage.lotteryTypesMap.put(key,LotteryTypesMap[key]);
				editBizSystemInfoPage.hotlotteryTypesMap.put(key,LotteryTypesMap[key]);
				editBizSystemInfoPage.mobilelotteryTypesMap.put(key,LotteryTypesMap[key]);
				
				editBizSystemInfoPage.pcQuickLotteryKindMap.put(key,LotteryTypesMap[key]);
				editBizSystemInfoPage.pcPopularLotteryKindMap.put(key,LotteryTypesMap[key]);
				editBizSystemInfoPage.pcIndexPushLotteryKindMap.put(key,LotteryTypesMap[key]);
				//				checksDom.append("<div class=\"sortable-list connectList checkbox i-checks\" style=\"float:left\"><label>" +
//						"<input type='checkbox' id='indexDefaultLotteryKinds' name='indexDefaultLotteryKinds' value='"+key+"' /> <i></i>"+LotteryTypesMap[key]+"</label></div>");
//				checksDomhot.append("<div class=\"sortable-list connectList checkbox i-checks\" style=\"float:left\"><label>" +
//						"<input type='checkbox' id='hotLotteryKinds' name='hotLotteryKinds' value='"+key+"' /> <i></i>"+LotteryTypesMap[key]+"</label></div>");
//				if(key.indexOf("SYXW") < 0){
//					checksDomMobile.append("<div class=\"sortable-list connectList checkbox i-checks\" style=\"float:left\"><label>" +
//							"<input type='checkbox' id='indexMobileDefaultLotteryKinds' name='indexMobileDefaultLotteryKinds' value='"+key+"' /> <i></i>"+LotteryTypesMap[key]+"</label></div>");
//				}

//			    if(count%12==0)
//			    {
//			    	checksDom.append("</br>");
//			    }
				
				count++;
			}
		}
	});
}





/**
 * 上传图片，回显图片
 * @param dir 上传目录
 */
EditBizSystemInfoPage.prototype.uploadImgFile = function(id, dir){
	var fileId = id + "File";
	if($("#" + fileId).val() == "") {
		alert("请选择图片文件");
		return; 
	}
	$("#" + id +"Info").text("上传中...");
	$("#" + id +"Info").css("color","blue");
	var uploadFile = dwr.util.getValue(fileId);
	var bizSystem = "";
	if(currentUser.bizSystem=='SUPER_SYSTEM') {
		bizSystem = $("#bizSystem").val();
	}
	//使用id作为文件名称
	managerBizSystemInfoAction.uploadImgFile(uploadFile, dir, id, bizSystem,function(r){
		if (r[0] != null && r[0] == "ok") {
			$("#" + id + "Url").val(r[1]);
			$("#" + id).attr("src", r[2] + r[1]);
			$("#" + id).show();
			$("#" + id +"Info").text("上传成功!");
			$("#" + id +"Info").css("color","red");
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("上传文件失败");
		}
	});
};