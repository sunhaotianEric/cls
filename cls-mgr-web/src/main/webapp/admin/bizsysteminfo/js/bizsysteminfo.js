function BizSystemInfoPage(){}
var bizSystemInfoPage = new BizSystemInfoPage();

bizSystemInfoPage.param = {
   	
};

bizSystemInfoPage.admin = {
	   	
};
/**
 * 查询参数
 */
bizSystemInfoPage.queryParam = {

};

//分页参数
bizSystemInfoPage.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize
};


$(document).ready(function() {

	bizSystemInfoPage.initTableData(); //初始化查询数据
	
});

/**
 * 初始化列表数据
 */
BizSystemInfoPage.prototype.initTableData = function() {
	bizSystemInfoPage.queryParam = getFormObj($("#queryForm"));
	bizSystemInfoPage.table = $('#bizsysteminfoTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"scrollX": true,
		"iDisplayLength" : defaultPageSize,
		"ajax":function (data, callback, settings) {
			bizSystemInfoPage.pageParam.pageSize = data.length;//页面显示记录条数
			bizSystemInfoPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerBizSystemInfoAction.getAllBizSystemInfo(bizSystemInfoPage.queryParam,bizSystemInfoPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": "bizSystem"},
		            {"data": "bizSystemName"},
		            {"data": "templateName"},
		            {"data": "indexLotteryKindName"},
		            {"data": "headLogoUrl",
            			render: function(data, type, row, meta) {
            				var str ="";
            				if(row.headLogoUrl != "" && row.headLogoUrl != null){
            					str = "<a target='_blank' href='"+ imgServerUrl + data +"'><img  alt='logo图片' src='"+ imgServerUrl+ data+"' style=' width: 100px;height: 50px;' ></a>";
            				}else{
            					str = "<a target='_blank' href='"+ imgServerUrl+ data +"'><img  alt='logo图片' src='"+ imgServerUrl+ data+"' style=' width: 100px;height: 50px;' ></a>";
            				}
    	            		return str;
                		}
		            },
		          /*  {"data": "indexDefaultLotteryKindsName"},
		            {"data": "customUrl"},
		            {"data": "telphoneNum"},*/
		            {   
		            	"data": "createTime",
		            	render: function(data, type, row, meta) {
		            		return dateFormat(data,'yyyy-MM-dd hh:mm:ss');
	            		}
		            },
		            {"data": "updateAdmin","bVisible":false},
		            {   
		            	"data": "updateTime","bVisible":false,
		            	render: function(data, type, row, meta) {
		            		return dateFormat(data,'yyyy-MM-dd hh:mm:ss');
	            		}
		            },
		 
		            {
	                	 "data": "id",
	                	 render: function(data, type, row, meta) {
	                		 return "<a class='btn btn-info btn-sm btn-bj' onclick='bizSystemInfoPage.editBizSystemInfo("+data+")'><i class='fa fa-pencil'></i>&nbsp;设置 </a>";
	                	 }
	                }
	            ]
	}).api(); 
}




/**
 * 查询数据
 */
BizSystemInfoPage.prototype.findBizSystemInfo = function(){
	bizSystemInfoPage.queryParam=getFormObj("#queryForm");
	bizSystemInfoPage.table.ajax.reload();
};

/**
 * 删除
 */
BizSystemInfoPage.prototype.delBizSystemInfo = function(id){
	   swal({
           title: "您确定要删除这条信息吗",
           text: "请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "删除",
           closeOnConfirm: false
       },
       function() {
		managerBizSystemInfoAction.delBizSystemInfoById(id,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "删除成功",type: "success"});
				bizSystemInfoPage.findBizSystemInfo();
			}else if(r[0] != null && r[0] == "error"){
   				swal(r[1]);
   			}else{
   				swal("删除信息失败.");
			}
	    });
	});
};


BizSystemInfoPage.prototype.addBizSystemInfo=function()
{
	  layer.open({
          type: 2,
          title: '业务系统编辑',
          maxmin: false,
          shadeClose: true,
          //点击遮罩关闭层
           area: ['800px', '600px'],
         // area: ['60%', '70%'],
          content: contextPath + "/managerzaizst/bizsysteminfo/editbizsysteminfo.html",
          cancel: function(index){ 
        	  bizSystemInfoPage.findBizSystemInfo();
       	   }
      });
	
}

BizSystemInfoPage.prototype.editBizSystemInfo=function(id)
{
	  layer.open({
          type: 2,
          title: '业务系统编辑',
          maxmin: false,
          shadeClose: true,
          //点击遮罩关闭层
           area: ['800px', '600px'],
         // area: ['70%', '80%'],
          content: contextPath + "/managerzaizst/bizsysteminfo/"+id+"/editbizsysteminfo.html",
          cancel: function(index){ 
        	  bizSystemInfoPage.findBizSystemInfo();
       	   }
      });
}

BizSystemInfoPage.prototype.closeLayer=function(index){
	layer.close(index);
	bizSystemInfoPage.findBizSystemInfo();
}