<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>

<%
	String path = request.getContextPath();
    String id = request.getParameter("id")==null?"0":request.getParameter("id"); 
%>
<!DOCTYPE html>
<html>  
    <head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>商户信息管理</title>
    <link rel="shortcut icon" href="favicon.ico">
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
     <link href="<%=path%>/css/plugins/iCheck/custom.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" rel="stylesheet">
     <link href="<%=path%>/css/plugins/summernote/summernote.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" rel="stylesheet">
     <link href="<%=path%>/css/plugins/summernote/summernote-bs3.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/plugins/sweetalert/sweetalert.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" rel="stylesheet">
	<link rel="stylesheet" href="<%=path%>/resources/kindeditor-4.1.10/themes/default/default.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>"/>
	<script charset="utf-8" src="<%=path%>/resources/kindeditor-4.1.10/kindeditor-min.js"></script>
	<script charset="utf-8" src="<%=path%>/resources/kindeditor-4.1.10/lang/zh_CN.js"></script>
		<script>
		var staticImageUrl = 'http://www.zzh.com:8080/cls-mgr-web';
		var url = staticImageUrl.split('//');
		var imgurl =  "http://42.51.191.174";
		KindEditor.ready(function(K) {
			editor1 = K.create('textarea[name="ruleDesc1"]', {
				uploadJson : location.protocol+'//'+url[1]+'/UploadFileForJsonServlet',
				fileManagerJson :imgurl+'/file_manager_json.jsp',
				allowFileManager : true
			});
		});
	</script>
	<script>
		var staticImageUrl = 'http://www.zzh.com:8080/cls-mgr-web';
		var imgurl =  "http://42.51.191.174";
		KindEditor.ready(function(K) {
			editor2 = K.create('textarea[name="ruleDesc2"]', {
				uploadJson : staticImageUrl+'/UploadFileForJsonServlet',
				fileManagerJson :imgurl+'/file_manager_json.jsp',
				allowFileManager : true
			});
		});
	</script>
	<script>
		var staticImageUrl = 'http://www.zzh.com:8080/cls-mgr-web';
		var imgurl =  "http://42.51.191.174";
		KindEditor.ready(function(K) {
			editor3 = K.create('textarea[name="ruleDesc3"]', {
				uploadJson : staticImageUrl+'/UploadFileForJsonServlet',
				fileManagerJson :imgurl+'/file_manager_json.jsp',
				allowFileManager : true
			});
		});
	</script>
	<script>
		var staticImageUrl = 'http://www.zzh.com:8080/cls-mgr-web';
		var imgurl =  "http://42.51.191.174";
		KindEditor.ready(function(K) {
			editor4 = K.create('textarea[name="ruleDesc4"]', {
				uploadJson : staticImageUrl+'/UploadFileForJsonServlet',
				fileManagerJson :imgurl+'/file_manager_json.jsp',
				allowFileManager : true
			});
		});
	</script>
	<script>
		var staticImageUrl = 'http://www.zzh.com:8080/cls-mgr-web';
		var imgurl =  "http://42.51.191.174";
		KindEditor.ready(function(K) {
			editor5 = K.create('textarea[name="ruleDesc5"]', {
				uploadJson : staticImageUrl+'/UploadFileForJsonServlet',
				fileManagerJson :imgurl+'/file_manager_json.jsp',
				allowFileManager : true
			});
		});
	</script>
</head>
   <body class="gray-bg">
        <div class="wrapper wrapper-content animated fadeInRight"  >
            <div class="row back-change">
                 <div class="ibox-content">
                            <form class="form-horizontal" id="bizsysteminfoForm">
                            <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">商户：</label>
                                    <div class="col-sm-3">
                                    <input id="bizSystem" name="bizSystem" type="text"  class="form-control" disabled="disabled" />
                                    </div>
                                    <div class="col-sm-6"></div>
                                </div>
                                </c:if>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">商户名称：</label>
                                    <div class="col-sm-3">
                                        <input id="id" name="id" type="hidden"  />
                                         <c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
                                         <input id="bizSystem" name="bizSystem" type="hidden"  value="${admin.bizSystem}" />
                                         </c:if>
                                        <input id="bizSystemName" name="bizSystemName"  type="text"  class="form-control" disabled="disabled"></div>
                                     <div class="col-sm-6"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">模板：</label>
                                    <div class="col-sm-3">
                                    <cls:enmuSel name="templateType" id="templateType" className="com.team.lottery.enums.ETemplateType" emptyOption="false" options="class:'ipt form-control'"/>
                                <!--       <select id="templateId" name="templateId" class="ipt form-control">
						   			    <option value="1" selected="selected">模板1</option>
						   			    <option value="2" >模板2</option>
						   			    <option value="3" >模板3</option>
						   			 </select> -->
                               
                                     </div>
                                     <div class="col-sm-6"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">pc快捷彩种：</label>
                                    <div class="col-sm-6" id="pcQuickLotteryKind_div">
                                     </div>
                                     <div class="col-sm-3"></div>
                                 </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">pc首页主推彩种：</label>
                                    <div class="col-sm-6" id="pcIndexPushLotteryKind_div">
                                     </div>
                                     <div class="col-sm-3"></div>
                                 </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">pc热门彩种：</label>
                                    <div class="col-sm-6" id="pcPopularLotteryKind_div">
                                     </div>
                                     <div class="col-sm-3"></div>
                                 </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">首推彩种：</label>
                                    <div class="col-sm-3">
                                        <select id="indexLotteryKind" name="indexLotteryKind" class="ipt form-control" id="frequentlyQuestion">
                                        </select>
                                     </div>
                                     <div class="col-sm-6"></div>
                                </div>
                                 <div class="form-group">
                                    <label class="col-sm-3 control-label">热门彩种：</label>
                                    <div class="col-sm-6" id="hotLotteryKinds_div">
                                     </div>
                                     <div class="col-sm-3"></div>
                                 </div>
                                  <div class="form-group">
                                    <label class="col-sm-3 control-label">首页默认彩种：</label>
                                    <div class="col-sm-6" id="indexDefaultLotteryKinds_div">
                                     </div>
                                     <div class="col-sm-3"></div>
                                 </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">手机端首页默认彩种：</label>
                                    <div class="col-sm-6" id="indexMobileDefaultLotteryKinds_div">
                                     </div>
                                     <div class="col-sm-3"></div>
                                 </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">客服地址：</label>
                                    <div class="col-sm-3">
                                        <input id="customUrl" name="customUrl" type="text" value="" class="form-control"></div>
                                    <div class="col-sm-6"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">电话号码：</label>
                                    <div class="col-sm-3">
                                       <input id="telphoneNum" name="telphoneNum" type="text" value="" class="form-control"></div>
                                        <div class="col-sm-6"></div>
                                </div>
                                 <div class="form-group">
                                    <label class="col-sm-3 control-label">QQ：</label>
                                    <div class="col-sm-3">
                                       <input id="qq" name="qq" type="text" value="" class="form-control"></div>
                                        <div class="col-sm-6"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">站点统计脚本：</label>
                                    <div class="col-sm-3">
                                       <input id="siteScript" name="siteScript" type="text" value="" class="form-control"></div>
                                        <div class="col-sm-6"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">手机站点统计：</label>
                                    <div class="col-sm-3">
                                       <input id="mobileSiteScript" name="mobileSiteScript" type="text" value="" class="form-control"></div>
                                        <div class="col-sm-6"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">平均充值时间：</label>
                                    <div class="col-sm-3">
                                    <input id="rechargeSecond" name="rechargeSecond"  type="text" value="" onkeyup="checkNum(this)" class="form-control"></div>
                                      <div class="col-sm-6"></div>  
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">平均提现时间：</label>
                                    <div class="col-sm-3">
                                        <input id="withdrawSecond" name="withdrawSecond" type="text" value="" onkeyup="checkNum(this)" class="form-control"></div>
                                   <div class="col-sm-6"></div>
                                </div>
                                    <div class="form-group" id="file-pretty">
                                    <label class="col-sm-3 control-label">PC端头部logo图片(352*58)：</label>
                                    <div class="col-sm-3">
                                        <input type="hidden" id="headLogoUrl" name="headLogoUrl"/>
                                        <input type="file" id="headLogoFile"  class="form-control" readonly="readonly" accept=".jpg,.png">
                                     </div>
                                    <div class="col-sm-1 text-center">
                                        <button class="btn btn-success " type="button" onclick="editBizSystemInfoPage.uploadImgFile('headLogo', 'logo')">
                                            <i class="fa fa-upload"></i>&nbsp;&nbsp;
                                            <span class="bold">上传</span></button>
                                             <span id="headLogoInfo"></span> 
                                    </div>
                                    <div class="col-sm-5"></div>
                                </div>	
                                 <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-9">
                                        <img id="headLogo" alt="页面顶部logo图片" src="" style="display: none;margin-top: 5px; width:150px;height: 120px;"/>
                                    </div>
                                </div>
                                 <!-- 没有PC登陆页Logo了暂时不展示 start -->
                                <div class="form-group" id="file-pretty" style="display: none;">
                                    <label class="col-sm-3 control-label">PC端登陆页logo图片：</label>
                                    <div class="col-sm-3">
                                        <input type="hidden" id="logoUrl" name="logoUrl"/>
                                        <input type="file" id="logoFile"  class="form-control" readonly="readonly" accept=".jpg,.png">
                                     </div>
                                    <div class="col-sm-1 text-center">
                                        <button class="btn btn-success " type="button" onclick="editBizSystemInfoPage.uploadImgFile('logo', 'logo')">
                                            <i class="fa fa-upload"></i>&nbsp;&nbsp;
                                            <span class="bold">上传</span></button>
                                             <span id="logoInfo"></span> 
                                    </div>
                                    <div class="col-sm-5"></div>
                                </div>
                                <div class="form-group" style="display: none;">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-9">
                                        <img id="logo" alt="logo图片" src="" style="display: none;margin-top: 5px; width:150px;height: 120px;"/>
                                    </div>
                                </div>
                                <!-- 没有PC登陆页Logo了暂时不展示 end -->
                                <div class="form-group" id="file-pretty">
                                    <label class="col-sm-3 control-label">手机端顶部logo图片(185*77)：</label>
                                    <div class="col-sm-3">
                                        <input type="hidden" id="mobileHeadLogoUrl" name="mobileHeadLogoUrl"/>
                                        <input type="file" id="mobileHeadLogoFile"  class="form-control" readonly="readonly" accept=".jpg,.png">
                                     </div>
                                    <div class="col-sm-1 text-center">
                                        <button class="btn btn-success " type="button" onclick="editBizSystemInfoPage.uploadImgFile('mobileHeadLogo', 'logo')">
                                            <i class="fa fa-upload"></i>&nbsp;&nbsp;
                                            <span class="bold">上传</span></button>
                                             <span id="mobileHeadLogoInfo"></span> 
                                    </div>
                                    <div class="col-sm-5"></div>
                                </div> 
                                 <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-9">
                                        <img id="mobileHeadLogo" alt="手机端顶部logo图片" src="" style="display: none;margin-top: 5px; width:150px;height: 120px;"/>
                                    </div>
                                </div>
                                
                               <div class="form-group" id="file-pretty">
                                    <label class="col-sm-3 control-label">手机端登录页logo图片(201*161)：</label>
                                    <div class="col-sm-3">
                                        <input type="hidden" id="mobileLogoUrl" name="mobileLogoUrl"/>
                                        <input type="file" id="mobileLogoFile"  class="form-control" readonly="readonly" accept=".jpg,.png">
                                     </div>
                                    <div class="col-sm-1 text-center">
                                        <button class="btn btn-success " type="button" onclick="editBizSystemInfoPage.uploadImgFile('mobileLogo', 'logo')">
                                            <i class="fa fa-upload"></i>&nbsp;&nbsp;
                                            <span class="bold">上传</span></button>
                                             <span id="mobileLogoInfo"></span> 
                                    </div>
                                    <div class="col-sm-5"></div>
                                </div>  
                                 <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-9">
                                        <img id="mobileLogo" alt="手机端登录页logo图片" src="" style="display: none;margin-top: 5px; width:150px;height: 120px;"/>
                                    </div>
                                </div>
                              
                                
                                <div class="form-group" id="file-pretty">
                                    <label class="col-sm-3 control-label">PC右侧二维码图片(120*120)：</label>
                                    <div class="col-sm-3">
                                       <input type="hidden" id="barcodeUrl" name="barcodeUrl" size="50px"/>
	   				                   <input type="file" id="barcodeFile"  class="form-control" readonly="readonly" accept=".jpg,.png"/>
                                        </div>
                                    <div class="col-sm-1 text-center">
                                        <button class="btn btn-success " type="button" onclick="editBizSystemInfoPage.uploadImgFile('barcode', 'info')">
                                            <i class="fa fa-upload"></i>&nbsp;&nbsp;
                                            <span class="bold">上传</span></button>
                                            <span id="barcodeInfo"></span>  
                                    </div>
                                    <div class="col-sm-5"></div>
                                </div>
                                 <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-9">
                                        <img id="barcode" alt="PC右侧二维码图片" src="" style="display: none;margin-top: 5px; width:150px;height: 120px;" />
                                    </div>
                                </div>
                                
                                <div class="form-group" id="file-pretty">
                                    <label class="col-sm-3 control-label">PC手机购彩二维码图片(200*200)：</label>
                                    <div class="col-sm-3">
                                       <input type="hidden" id="mobileBarcodeUrl" name="mobileBarcodeUrl" size="50px"/>
	   				                   <input type="file" id="mobileBarcodeFile"  class="form-control" readonly="readonly" accept=".jpg,.png"/>
                                        </div>
                                    <div class="col-sm-1 text-center">
                                        <button class="btn btn-success " type="button" onclick="editBizSystemInfoPage.uploadImgFile('mobileBarcode', 'info')">
                                            <i class="fa fa-upload"></i>&nbsp;&nbsp;
                                            <span class="bold">上传</span></button>
                                            <span id="mobileBarcodeInfo"></span>  
                                    </div>
                                    <div class="col-sm-5"></div>
                                </div>
                                 <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-9">
                                        <img id="mobileBarcode" alt="PC手机购彩二维码图片" src="" style="display: none;margin-top: 5px; width:150px;height: 120px;" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">是否有APP：</label>
                                    <div class="col-sm-3">
                                      <select id="hasApp" name="hasApp" class="ipt form-control" onchange="editBizSystemInfoPage.showAppCodeDiv()">
						   			    <option value="0" selected="selected">否</option>
 										<option value="1" >是</option>
						   			 </select>
                                     </div>
                                     <div class="col-sm-6"></div>
                                </div>
                                <div class="form-group" style="display: none;" id="appAndriodIosDownloadUrl">
                                    <label class="col-sm-3 control-label">手机Web首页APP下载页面地址：</label>
                                    <div class="col-sm-3">
                                       <input id="andriodIosDownloadUrl" name="andriodIosDownloadUrl" type="text" value="" class="form-control"></div>
                                        <div class="col-sm-6"><span style="color:red;">包含苹果和安卓APP下载页面地址,没有可不填，会默认跳转到appdownload.html页面</span></div>
                                </div>
                                <div class="form-group" style="display: none;" id="appShowNames">
                                    <label class="col-sm-3 control-label">APP名称：</label>
                                    <div class="col-sm-3">
                                       <input id="appShowname" name="appShowname" type="text" value="" class="form-control"></div>
                                        <div class="col-sm-6"></div>
                                </div>
                                <div class="form-group" id="file-pretty" style="display: none;">
                                    <label class="col-sm-3 control-label">APP logo(250*250)：</label>
                                    <div class="col-sm-3">
                                       <input type="hidden" id="appDownLogoUrl" name="appDownLogoUrl" size="50px"/>
	   				                   <input type="file" id="appDownLogoFile"  class="form-control" readonly="readonly" accept=".jpg,.png"/>
                                        </div>
                                    <div class="col-sm-1 text-center">
                                        <button class="btn btn-success " type="button" onclick="editBizSystemInfoPage.uploadImgFile('appDownLogo', 'app')">
                                            <i class="fa fa-upload"></i>&nbsp;&nbsp;
                                            <span class="bold">上传</span></button>
                                            <span id="appDownLogoInfo"></span>  
                                    </div>
                                    <div class="col-sm-5"></div>
                                </div>
                                <div class="form-group" id="appDown_Logo">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-9">
                                        <img id="appDownLogo" alt="APP logo" src="" style="display: none;margin-top: 5px; width:150px;height: 120px;" />
                                    </div>
                                </div> 
                                <div class="form-group" style="display: none;" id="appAndroidDownloadDiv">
                                    <label class="col-sm-3 control-label">安卓app下载地址：</label>
                                    <div class="col-sm-3">
                                       <input id="appAndroidDownloadUrl" name="appAndroidDownloadUrl" type="text" value="" class="form-control"></div>
                                        <div class="col-sm-6"></div>
                                </div>
                                <div class="form-group" id="file-pretty" style="display: none;">
                                    <label class="col-sm-3 control-label">安卓app文件二维码(200*200)：</label>
                                    <div class="col-sm-3">
                                       <input type="hidden" id="appBarcodeUrl" name="appBarcodeUrl" size="50px"/>
	   				                   <input type="file" id="appBarcodeFile"  class="form-control" readonly="readonly" accept=".jpg,.png"/>
                                        </div>
                                    <div class="col-sm-1 text-center">
                                        <button class="btn btn-success " type="button" onclick="editBizSystemInfoPage.uploadImgFile('appBarcode', 'app')">
                                            <i class="fa fa-upload"></i>&nbsp;&nbsp;
                                            <span class="bold">上传</span></button>
                                            <span id="appBarcodeInfo"></span>  
                                    </div>
                                    <div class="col-sm-5"></div>
                                </div>
                                 <div class="form-group" id="appBarcode_groupDiv">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-9">
                                        <img id="appBarcode" alt="安卓app文件二维码" src="" style="display: none;margin-top: 5px; width:150px;height: 120px;" />
                                    </div>
                                </div>                                
                                
                                <div class="form-group" style="display: none;" id="appStoreIosDiv">
                                    <label class="col-sm-3 control-label">苹果app下载地址：</label>
                                    <div class="col-sm-3">
                                       <input id="appStoreIosUrl" name="appStoreIosUrl" type="text" value="" class="form-control"></div>
                                        <div class="col-sm-6"></div>
                                </div>
                                <div class="form-group" id="file-pretty">
                                    <label class="col-sm-3 control-label">苹果app文件二维码(200*200)：</label>
                                    <div class="col-sm-3">
                                       <input type="hidden" id="appIosBarcodeUrl" name="appIosBarcodeUrl" size="50px"/>
	   				                   <input type="file" id="appIosBarcodeFile"  class="form-control" readonly="readonly" accept=".jpg,.png"/>
                                        </div>
                                    <div class="col-sm-1 text-center">
                                        <button class="btn btn-success " type="button" onclick="editBizSystemInfoPage.uploadImgFile('appIosBarcode', 'app')">
                                            <i class="fa fa-upload"></i>&nbsp;&nbsp;
                                            <span class="bold">上传</span></button>
                                            <span id="appIosBarcodeInfo"></span>  
                                    </div>
                                    <div class="col-sm-5"></div>
                                </div>
                                 <div class="form-group" id="appIosBarcode_groupDiv">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-9">
                                        <img id="appIosBarcode" alt="苹果app二维码" src="" style="display: none;margin-top: 5px; width:150px;height: 120px;" />
                                    </div>
                                </div>  
                              <!--   <div class="form-group">
                                    <label class="col-sm-3 control-label">优势编辑：</label>
                                    <div class="col-sm-9">
                                        <textarea name="advantageDesc" id="advantageDesc" class="form-control"></textarea>
                                      </div>
                                </div> -->
                                <div class="form-group" id="file-pretty">
                                    <label class="col-sm-3 control-label">网站icon图标(64*64)：</label>
                                    <div class="col-sm-3">
                                        <input type="hidden" id="faviconUrl" name="faviconUrl"/>
                                        <input type="file" id="faviconFile"  class="form-control" readonly="readonly" accept=".jpg,.png,.ico">
                                     </div>
                                    <div class="col-sm-1 text-center">
                                        <button class="btn btn-success " type="button" onclick="editBizSystemInfoPage.uploadImgFile('favicon', 'info')">
                                            <i class="fa fa-upload"></i>&nbsp;&nbsp;
                                            <span class="bold">上传</span></button>
                                             <span id="faviconInfo"></span> 
                                    </div>
                                    <div class="col-sm-5"></div>
                                </div>  
                                 <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-9">
                                        <img id="favicon" alt="网站图标" src="" style="display: none;margin-top: 5px; width:150px;height: 120px;"/>
                                    </div>
                                </div>

								<div class="col-sm-12">
									<div class="input-group m-b">
										<span class="input-group-addon">联系我们：</span>
										<textarea name="ruleDesc1" id="contactUs" style="width: 800px; height: 400px; visibility: hidden;"></textarea>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="input-group m-b">
										<span class="input-group-addon">关于我们：</span>
										<textarea name="ruleDesc2" id="aboutUs" style="width: 800px; height: 400px; visibility: hidden;"></textarea>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="input-group m-b">
										<span class="input-group-addon">加盟代理：</span>
										<textarea name="ruleDesc3" id="franchiseAgent" style="width: 800px; height: 400px; visibility: hidden;"></textarea>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="input-group m-b">
										<span class="input-group-addon">法律声明：</span>
										<textarea name="ruleDesc4" id="legalStatement"style="width: 800px; height: 400px; visibility: hidden;"></textarea>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="input-group m-b">
										<span class="input-group-addon">隐私声明：</span>
										<textarea name="ruleDesc5" id="privacyStatement" style="width: 800px; height: 400px; visibility: hidden;"></textarea>
									</div>
								</div>
		
		
							<div class="form-group">
								<div class="row">
									<div class="col-sm-3"></div>
									<div class="col-sm-1">
										<button type="button" class="btn btn-w-m btn-white"
											onclick="editBizSystemInfoPage.saveData()">提 交</button>
									</div>
									<div class="col-sm-2"></div>
									<div class="col-sm-1">
										<!-- <button type="button" class="btn btn-w-m btn-white">重 置</button> -->
										<input type="reset" class="btn btn-w-m btn-white" value="重 置" />
									</div>
									<div class="col-sm-5"></div>
								</div>
							</div>

				</form>
                    </div>
              
            </div>
        </div>
    <!-- js -->
    <script src="<%=path%>/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="<%=path%>/js/plugins/prettyfile/bootstrap-prettyfile.js"></script>
    <script src="<%=path%>/js/plugins/switchery/switchery.js"></script>
    <script src="<%=path%>/js/plugins/iCheck/icheck.min.js"></script>
    <script src="<%=path%>/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
     <script src="<%=path%>/js/plugins/clockpicker/clockpicker.js"></script>
     <script src="<%=path%>/js/plugins/cropper/cropper.min.js"></script>
     <script src="<%=path%>/js/demo/form-advanced-demo.min.js"></script>
     <script src="<%=path%>/js/jquery-ui-1.10.4.min.js"></script>
   
    <script type="text/javascript" src="<%=path%>/admin/bizsysteminfo/js/editbizsysteminfo.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerBizSystemInfoAction.js'></script>
    <script type="text/javascript">
	    $(document).ready(function(){$(".sortable-list").sortable({connectWith:".connectList"}).disableSelection()});
		editBizSystemInfoPage.param.id = <%=id%>;
	</script>
	 <script>
       $(document).ready(function(){$(".i-checks").iCheck({checkboxClass:"icheckbox_square-green",radioClass:"iradio_square-green",})});
     </script>
</body>
</html>

