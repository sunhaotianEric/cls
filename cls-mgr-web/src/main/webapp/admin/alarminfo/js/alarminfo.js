function AlarminfoPage(){};
var alarminfo=new AlarminfoPage();

//分页参数
alarminfo.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize
};

/**
 * 查询参数
 */
alarminfo.queryParam = {
		bizSystem : null,
		alarmType : null
};

$(document).ready(function() {
	alarminfo.initTableData();
});

AlarminfoPage.prototype.initTableData=function(){
	var alarmType=$("alarmType").val();
	var bizSystem = $("#bizSystem").val();
	if(bizSystem != ""){
		alarminfo.queryParam.bizSystem = bizSystem;
	}
	if(alarmType!=""){
		alarminfo.queryParam.alarmType = alarmType;
	}
	alarminfo.table = $('#alarminfo').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"scrollX": true,
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			alarminfo.pageParam.pageSize = data.length;//页面显示记录条数
			alarminfo.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerAlarmInfoAction.getAlarminfoPage(alarminfo.queryParam,alarminfo.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data":null},
                    {"data": "bizSystemName","bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {
                    	"data": "alarmType",
                    	render: function(data, type, row, meta){
                    		var str="";
                    		if(data=="TZ"){
                    			str="投注";
                    		}else if(data=="ZJ"){
                    			str="中奖";
                    		}else if(data=="CZ"){
                    			str="充值";
                    		}else if(data=="TX"){
                    			str="提现";
                    		}
                    		return str;
                    	}
                    		
                    },
		            {
		            	"data": "alarmContent",
		            	render: function(data, type, row, meta){
		            		if(data.length>30){
		            			return "<span title='"+data+"'>"+data.substring(0,30)+"....</span>";
		            		}
		            		return data;
		            	}
		            },
		            {
		            	"data": "smsSendOpen",
		            	render: function(data, type, row, meta){
		            		return data==0?"否":"是";
		            	}
		            },
		            {
		            	"data": "emailSendOpen",
		            	render: function(data, type, row, meta){
		            		return data==0?"否":"是";
		            	}
		            },
		            {"data": "createDateStr"},
		            {
		            	"data": "id",
		            	render: function(data, type, row, meta) {
		            		var str='<button type="button" class="btn btn-danger btn-sm btn-del" onclick="alarminfo.delAlarminfoByid('+data+')">删除</button>'
		            		return str;
	            		}

		            }
	            ]
	}).api(); 
}

AlarminfoPage.prototype.delAlarminfoByid=function(id){
	
	swal({
        title: "您确定要删除这条信息吗",
        text: "请谨慎操作！",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        cancelButtonText: "取消",
        confirmButtonText: "删除",
        closeOnConfirm: false
    },
    function() {
    	managerAlarmInfoAction.delAlarminfoById(id,function(r){
    		if(r[0]=="ok"){
    			swal("删除成功");
    			alarminfo.reloadtable();
    		}else{
    			swal("删除失败");
    		}
    	});
    });
}
AlarminfoPage.prototype.reloadtable=function(){
	alarminfo.queryParam.bizSystem = $("#bizSystem").val()==undefined?null:$("#bizSystem").val();
	alarminfo.queryParam.alarmType =$("#alarmType").val()==undefined?null:$("#alarmType").val();
	alarminfo.table.ajax.reload();
}
