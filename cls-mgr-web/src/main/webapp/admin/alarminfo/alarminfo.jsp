<%@ page language="java" contentType="text/html; charset=UTF-8" 
	pageEncoding="UTF-8"%>	
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
    
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>告警信息查询</title>
        <jsp:include page="/admin/include/include.jsp"></jsp:include>
   
</head>
    <body>
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
                    <div class="ibox ">
                        <div class="ibox-content">
                            <div class="panel-body">
                                <form class="form-horizontal">
                                    <div class="row">
                                    <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                                      <div class="col-sm-3">
                                       <div class="input-group m-b">
                                         <span class="input-group-addon">商户</span>
                                          <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control" />
                                         </div>  
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">告警类型</span>
                                          		<select class="ipt form-control" id="alarmType">
	                                          		<option value="">= = 请选择 = =</option>
	                                          		<option value="TZ">投注</option>
	                                          		<option value="ZJ">中奖</option>
	                                          		<option value="CZ">充值</option>
	                                          		<option value="TX">提现</option>
                                          		</select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group nomargin text-left">
                                                <button type="button" class="btn btn-outline btn-default btn-kj"  onclick="alarminfo.reloadtable()">查询</button></div>
                                        </div>
                                         </c:if>
                                     <c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
                             
                                        <div class="col-sm-3">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">告警类型</span>
                                          		<select class="ipt form-control" id="alarmType">
	                                          		<option value="">= = 请选择 = =</option>
	                                          		<option value="TZ">投注</option>
		                                          	<option value="ZJ">中奖</option>
		                                          	<option value="CZ">充值</option>
		                                          	<option value="TX">提现</option>
                                          		</select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group nomargin text-left">
                                                <button type="button" class="btn btn-outline btn-default btn-kj"  onclick="alarminfo.reloadtable()">查询</button></div>
                                        </div>
                                         </c:if>
                                    </div>
                                </form>
                                <div class="jqGrid_wrapper">
                                    <div class="ui-jqgrid ">
                                        <div class="ui-jqgrid-view">
                                            <div class="ui-jqgrid-hdiv">
                                                <div class="ui-jqgrid-hbox" style="padding-right:0">
                                                    <table class="ui-jqgrid-htable ui-common-table table table-bordered" id="alarminfo">
                                                        <thead>
                                                            <tr class="ui-jqgrid-labels">
                                                                <th class="ui-th-column ui-th-ltr">序号</th>
                                                                <th class="ui-th-column ui-th-ltr">商户</th>
                                                                <th class="ui-th-column ui-th-ltr">告警类型</th>
                                                                <th class="ui-th-column ui-th-ltr">告警内容</th>
                                                                <th class="ui-th-column ui-th-ltr">是否发送短信</th>
                                                            
                                                                <th class="ui-th-column ui-th-ltr">是否发送邮件</th>
                                                                <th class="ui-th-column ui-th-ltr">创建时间</th>
                                                                <th class="ui-th-column ui-th-ltr">操作</th>
                                                             </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/admin/alarminfo/js/alarminfo.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerAlarmInfoAction.js'></script>    
</body>
</html>
	