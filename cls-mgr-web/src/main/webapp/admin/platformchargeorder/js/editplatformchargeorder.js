function EditPlatformChangeOrderPage(){}
var editPlatformChangeOrderPage = new EditPlatformChangeOrderPage();

editPlatformChangeOrderPage.param = {
   	
};

$(document).ready(function() {
	//绑定收费类型下拉框事件
	$("#chargeType").change(function(){
		//分红费用才显示分红比例盈利金额
		if($(this).val() == "BONUS") {
			$("#bonusScaleDiv").show();
			$("#gainDiv").show();
		} else {
			$("#bonusScaleDiv").hide();
			$("#gainDiv").hide();
		}
	});
	
	var bizsystemId = editPlatformChangeOrderPage.param.id;  //获取查询ID
	if(bizsystemId != null){
		editPlatformChangeOrderPage.edit(bizsystemId);
	}
});


/**
 * 保存更新信息
 */
EditPlatformChangeOrderPage.prototype.saveData = function(){
	var id = $("#id").val();
	var bizSystem = $("#bizSystem").val();
	var chargeType = $("#chargeType").val();
	var money = $("#money").val();
	var bonusScale = $("#bonusScale").val();
	var gain = $("#gain").val();
	var belongDate = $("#belongDate").val();
	
	var va=/^-?[0-9]*.?[0-9]*$/;//验证正小数
	
	if(bizSystem==null || bizSystem.trim()==""){
		swal("请输入业务系统!");
		$("#bizSystem").focus();
		return;
	}
	
	if(chargeType==null || chargeType.trim()==""){
		swal("请输入收费类型!");
		$("#chargeType").focus();
		return;
	}
	
	if(chargeType == "BONUS") {
		if(bonusScale==null || bonusScale.trim()==""){
			swal("请输入分红比例!");
			$("#bonusScale").focus();
			return;
		}
		if(!va.test(bonusScale))
		{
			swal("分红比例不是数字，请输入数字!");
			$("#bonusScale").focus();
			return;
		}
		if(bonusScale>1){
			swal("分红比例 请输入小于1数字!");
			$("#bonusScale").focus();
			return;	
		}
	}
	
	if(belongDate==null || belongDate.trim()==""){
		swal("请输入归属月份!");
		$("#belongDate").focus();
		return;
	}


	
	var platformChangeOrder = {};
	platformChangeOrder.id = editPlatformChangeOrderPage.param.id;
	platformChangeOrder.bizSystem = bizSystem;
	platformChangeOrder.chargeType = chargeType;
	platformChangeOrder.money = money;
	platformChangeOrder.bonusScale = bonusScale;
	platformChangeOrder.gain = gain;
	platformChangeOrder.belongDate = belongDate.stringToDate();
	managerPlatformChangeOrderAction.saveOrUpdatePlatformChangeOrder(platformChangeOrder,function(r){
		if (r[0] != null && r[0] == "ok") {
			   swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.platformChangeOrderPage.closeLayer(index);
		   	    });     
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("保存信息失败.");
		}
    });
};


/**
 * 赋值信息
 */
EditPlatformChangeOrderPage.prototype.edit = function(id){
	managerPlatformChangeOrderAction.getPlatformChangeOrderById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			var belongDate=dateFormat(r[1].belongDate,'yyyy-MM');
			$("#id").val(r[1].id);
			$("#bizSystem").val(r[1].bizSystem);
			$("#chargeType").val(r[1].chargeType);
			$("#money").val(r[1].money);
			//分红费用才显示分红比例盈利金额
			if(r[1].chargeType == "BONUS") {
				$("#bonusScaleDiv").show();
				$("#gainDiv").show();
			} else {
				$("#bonusScaleDiv").hide();
				$("#gainDiv").hide();
			}
			$("#bonusScale").val(r[1].bonusScale);
			$("#gain").val(r[1].gain);
			$("#belongDate").val(belongDate);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取信息失败.");
		}
    });
};