function PlatformChangeOrderPage(){}
var platformChangeOrderPage = new PlatformChangeOrderPage();

platformChangeOrderPage.param = {
   	
};
platformChangeOrderPage.obj ={
		money:null,
		gain:null
}
/**
 * 查询参数
 */
platformChangeOrderPage.queryParam = {
		bizSystemName : null,
		enable : null
};

//分页参数
platformChangeOrderPage.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize
};


$(document).ready(function() {
	
	var lastMonthDate = new Date(); //上月日期
	lastMonthDate.setDate(1);
	lastMonthDate.setMonth(lastMonthDate.getMonth()-1);
	$("#belongDateStart").val(dateFormat(lastMonthDate,'yyyy-MM'));
	$("#belongDateEnd").val(dateFormat(lastMonthDate,'yyyy-MM'));
	platformChangeOrderPage.initTableData(); //初始化查询数据
});



/**
 * 初始化列表数据
 */
PlatformChangeOrderPage.prototype.initTableData = function() {
	platformChangeOrderPage.queryParam = getFormObj($("#queryForm"));
	platformChangeOrderPage.queryParam.belongDateStart = ($("#belongDateStart").val()+"-01").stringToDate();
	platformChangeOrderPage.queryParam.belongDateEnd = ($("#belongDateEnd").val()+"-25").stringToDate();
	platformChangeOrderPage.table = $('#platformChargeOrderTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"scrollX": true,
		"iDisplayLength" : defaultPageSize,
		"fnDrawCallback": function (oSettings) {  
			platformChangeOrderPage.refreshBizSystemPage(platformChangeOrderPage.obj);
	     },
		"ajax":function (data, callback, settings) {
			platformChangeOrderPage.pageParam.pageSize = data.length;//页面显示记录条数
			platformChangeOrderPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerPlatformChangeOrderAction.getPlatformChangeOrderByQueryPage(platformChangeOrderPage.queryParam,platformChangeOrderPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
					platformChangeOrderPage.obj.money = r[2];
					platformChangeOrderPage.obj.gain = r[3];
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": "bizSystemName","bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "chargeTypeStr"},
		            {"data": "belongDateStr"}, 
		            {"data": "money"},
		            {"data": "bonusScale",
		              render: function(data, type, row, meta) {
                		if(row.chargeType =='BONUS'){
                			return data;	
                		}else{
                			return '-';
                		}
                		
                	 }},
		            {"data": "gain",
		              render: function(data, type, row, meta) {
	                		if(row.chargeType =='BONUS'){
	                			return data;	
	                		}else{
	                			return '-';
	                		}
	                	 }
                	 },  
		            {"data": "payStatusName",
	                	 render: function(data, type, row, meta) {
		                		if(row.payStatus=='PAYMENT'){
		                			
		                			return "<span style='color: red;'>"+data+"</span>";
		                		}else if(row.payStatus=='PAYMENT_SUCCESS'){
		                			return "<span style='color: green;'>"+data+"</span>";
		                		}else{
		                			return "<span style='color: blue;'>"+data+"</span>";
		                		}
		                	
		                		return '';
		                	 }
		            },
		            {
	                	 "data": "id","bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false,
	                	 render: function(data, type, row, meta) {
	                		 if(row.payStatus=='PAYMENT'){
	                			 var operStr = "";
	                			 if(row.chargeType == "LABOR" || row.chargeType == "EQUIPMENT") {
	                				 operStr = "<a class='btn btn-danger btn-sm btn-del' onclick='platformChangeOrderPage.getPlatformChangeOrderForMoney("+data+")'>&nbsp;确认 </a>&nbsp;&nbsp;" +
	                			 		"<a class='btn btn-info btn-sm btn-bj' onclick='platformChangeOrderPage.edit("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>"; 
	                			 } else {
	                				 operStr = "<a class='btn btn-warning btn-sm btn-xf' onclick='platformChangeOrderPage.getPlatformChangeOrderPaymentClose("+data+")'>&nbsp;不收取 </a>&nbsp;&nbsp;" +
	                			        "<a class='btn btn-danger btn-sm btn-del' onclick='platformChangeOrderPage.getPlatformChangeOrderForMoney("+data+")'>&nbsp;收取 </a>&nbsp;&nbsp;" +
	                			 		"<a class='btn btn-info btn-sm btn-bj' onclick='platformChangeOrderPage.edit("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>";
	                			 }
	                			 return operStr; 
	                		 }else {
	                			 return "";
	                		 }
	                		
	                	 }
	                }
	            ]
	}).api(); 
}



/**
 * 查询数据
 */
PlatformChangeOrderPage.prototype.find = function(){
	platformChangeOrderPage.queryParam.bizSystem = $("#bizSystem").val();
	platformChangeOrderPage.queryParam.chargeType = $("#chargeType").val();
	platformChangeOrderPage.queryParam.payStatus = $("#payStatus").val();
	platformChangeOrderPage.queryParam.belongDateStart = ($("#belongDateStart").val()+"-01").stringToDate();
	platformChangeOrderPage.queryParam.belongDateEnd = ($("#belongDateEnd").val()+"-25").stringToDate();
	if(platformChangeOrderPage.queryParam.belongDateStart>platformChangeOrderPage.queryParam.belongDateEnd){
		swal("开始月份要小于结束月份！");
		return ;
	}
	platformChangeOrderPage.table.ajax.reload();
};

/**
 * 查询数据
 */
PlatformChangeOrderPage.prototype.getPlatformChangeOrderForMoney = function(id){
	   managerPlatformChangeOrderAction.getPlatformChangeOrderForMoney(id,function(r){
  			if (r[0] != null && r[0] == "ok") {
  				swal({ title: "提示", text: "操作成功",type: "success"});
  				platformChangeOrderPage.find();
  			}else if(r[0] != null && r[0] == "error"){
  				swal(r[1]);
  			}else{
  				swal("收取失败.");
  			}
  	    });
	
};



/**
 * 不收取费用
 */
PlatformChangeOrderPage.prototype.getPlatformChangeOrderPaymentClose = function(id){
	   managerPlatformChangeOrderAction.getPlatformChangeOrderPaymentClose(id,function(r){
  			if (r[0] != null && r[0] == "ok") {
  				swal({ title: "提示", text: "操作成功",type: "success"});
  				platformChangeOrderPage.find();
  			}else if(r[0] != null && r[0] == "error"){
  				swal(r[1]);
  			}else{
  				swal("操作失败.");
  			}
  	    });
	
};
/**
 * 删除
 */
PlatformChangeOrderPage.prototype.del = function(id){
	
	   swal({
           title: "您确定要删除这条信息吗",
           text: "请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "删除",
           closeOnConfirm: false
       },
       function() {
    	   managerPlatformChangeOrderAction.delPlatformChangeOrder(id,function(r){
   			if (r[0] != null && r[0] == "ok") {
   				swal({ title: "提示", text: "删除成功",type: "success"});
   				platformChangeOrderPage.find();
   			}else if(r[0] != null && r[0] == "error"){
   				swal(r[1]);
   			}else{
   				swal("删除信息失败.");
   			}
   	    });
       });
};

PlatformChangeOrderPage.prototype.add=function()
{
	
	 layer.open({
         type: 2,
         title: '系统收费管理新增',
         maxmin: false,
         shadeClose: true,
         //点击遮罩关闭层
         area: ['800px', '420px'],
         //area: ['60%', '45%'],
         content: contextPath + "/managerzaizst/platformchargeorder/editplatformchargeorder.html",
     /*    btn: ['确 定', '关闭'],
         yes: function(index, layero){
        	 alert(index);
        	 var iframeWin = window['layui-layer-iframe' + index].window;
        	 iframeWin.editplatformChangeOrderPage.saveData();
        	 iframeWin.saveData();
        	  },*/
         cancel: function(index){ 
        	 platformChangeOrderPage.find();
      	   }
     });
}

PlatformChangeOrderPage.prototype.edit=function(id)
{
	
	  layer.open({
           type: 2,
           title: '系统收费管理编辑',
           maxmin: false,
           shadeClose: true,
           //点击遮罩关闭层
           area: ['800px', '420px'],
           //area: ['60%', '45%'],
           content: contextPath + "/managerzaizst/platformchargeorder/"+id+"/editplatformchargeorder.html",
           cancel: function(index){ 
        	   platformChangeOrderPage.find();
        	   }
       });
}

PlatformChangeOrderPage.prototype.closeLayer=function(index){
	layer.close(index);
	platformChangeOrderPage.find();
}
/**
 * 刷新列表数据
 */
PlatformChangeOrderPage.prototype.refreshBizSystemPage = function(obj){
	if(obj.money!=null && obj.gain!=null){
		var reportListObj = $("#platformChargeOrderTable tbody");
		var str = "";
		str += "<tr style='color:red'>";
	    str += "  <td>所有系统总计</td>";
	    str += "  <td>-</td>";
	    str += "  <td>-</td>";
	    str += "  <td>"+ obj.money.toFixed(commonPage.param.fixVaue)+"</td>";
	    str += "  <td>-</td>";
	    str += "  <td>"+ obj.gain.toFixed(commonPage.param.fixVaue)+"</td>";
	    str += "  <td>-</td>";
	    str += "  <td>-</td>";
	    str += "</tr>"
	    reportListObj.append(str);
	}
	
}


