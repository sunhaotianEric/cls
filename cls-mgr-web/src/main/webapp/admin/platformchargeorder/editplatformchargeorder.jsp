<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>  
    <head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>配置编辑</title>
    <link rel="shortcut icon" href="favicon.ico">
    <jsp:include page="/admin/include/include.jsp"></jsp:include>

    <%
	 String id = request.getParameter("id");  
	%>
</head>
  <body class="gray-bg">
        <div class="wrapper wrapper-content animated fadeIn">
         <form action="javascript:void(0)">
            <div class="row">
                <div class="col-sm-12">
                    <div class="input-group m-b">
                        <span class="input-group-addon">商户</span>
                        <input type="hidden" readonly="readonly" id="id"/>
                        <% if(id==null){  %>
                       <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control" hasSuperSystemOption="true"/>
                       <%}else{ %>
                       <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control,disabled:'disabled'" hasSuperSystemOption="true"/>
                       <%} %>
                        </div>
                </div>
                <div class="col-sm-12">
                    <div class="input-group m-b">
                        <span class="input-group-addon">收费类型</span>
                        <select class="ipt form-control" id="chargeType" name="chargeType"  <%=id==null?"":"disabled='disabled'" %> >
                            <option value="MAINTAIN" >维护费</option>
	   			            <option value="BONUS" >分红费</option>
	   			            <option value="LABOR" >工资支出</option>
	   			            <option value="EQUIPMENT" >服务器支出</option>
                         </select>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="input-group m-b">
                        <span class="input-group-addon">费用</span>
                        <input id="money" name="money" type="text" value="" class="form-control"></div>
                </div>
          
                <div class="col-sm-12" id="bonusScaleDiv" style="display: none">
                    <div class="input-group m-b">
                        <span class="input-group-addon">分红比例</span>
                        <input id="bonusScale" name="bonusScale" value="" class="form-control" <%=id==null?"":"readonly='readonly'" %>>
                        </div>
                </div>
                <div class="col-sm-12" id="gainDiv" style="display: none">
                    <div class="input-group m-b">
                        <span class="input-group-addon">盈利金额</span>
                         <input id="gain" name="gain" value="" class="form-control" <%=id==null?"":"readonly='readonly'" %>>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="input-group m-b">
                        <span class="input-group-addon">归属月份</span>
                        <input id="belongDate" name="belongDate" class="form-control layer-date form-control"  <%=id==null?"onclick=\"laydate({ format: 'YYYY-MM'})\"":"readonly='readonly'" %>></div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button type="button" class="btn btn-w-m btn-white" onclick="editPlatformChangeOrderPage.saveData()">提 交</button></div>
                        <div class="col-sm-6 text-center">
                            <!-- <button type="button" class="btn btn-w-m btn-white">重 置</button> -->
                            <input type="reset" class="btn btn-w-m btn-white" value="重 置"/>
                            </div>
                    </div>
                </div> 
            </div>
            </form>
        </div>

      <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/platformchargeorder/js/editplatformchargeorder.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerPlatformChangeOrderAction.js'></script> 
     <script type="text/javascript">
     editPlatformChangeOrderPage.param.id = <%=id%>;
	</script>
</body>
</html>

