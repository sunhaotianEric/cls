<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>  
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>商户收费管理</title>
        <link rel="shortcut icon" href="favicon.ico">
        <jsp:include page="/admin/include/include.jsp"></jsp:include>
        <script  type="text/javascript" src="<%=path%>/resources/My97DatePicker/WdatePicker.js"></script>
</head>

   <body>
        <div class="animated fadeIn">
            <div class="ibox-content">
                 <div class="row m-b-sm m-t-sm">
                    <form class="form-horizontal" id="queryForm">
                    <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                        <div class="row">
                             <div class="col-sm-2">
                                  <div class="input-group m-b">
                                      <span class="input-group-addon">商户</span>
                                       <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control" hasSuperSystemOption="true"/>
                                  </div>
                             </div>
                             <div class="col-sm-2">
                                  <div class="input-group m-b">
                                      <span class="input-group-addon">支付状态</span>
                                       <cls:enmuSel className="com.team.lottery.enums.EPlatformChargePayStatus" id="payStatus" name="payStatus" options="class:ipt form-control"/>
                                  </div>
                             </div>
                            <div class="col-sm-2">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">收费类型</span>
                                    <cls:enmuSel id="chargeType" name="chargeType" className="com.team.lottery.enums.EPlatformChargeType" options="class:ipt form-control"/>
                                </div>
                            </div>
                           <div class="col-sm-4">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">归属月份</span>
                                    <div class="input-daterange input-group" >
                                        <input class="form-control layer-date" placeholder="开始月份" id="belongDateStart" style="background-color: white;" readonly="readonly"  onclick="WdatePicker({dateFmt:'yyyy-MM'})">                                   
                                     <span class="input-group-addon">到</span>                                   
                                        <input class="form-control layer-date" placeholder="结束月份" id="belongDateEnd" style="background-color: white;" readonly="readonly" onclick="WdatePicker({dateFmt:'yyyy-MM'})">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-2">
                                <div class="form-group nomargin text-right">
                                    <button type="button" class="btn btn-outline btn-default btn-js" onclick="platformChangeOrderPage.find()"><i class="fa fa-search"></i>&nbsp;查 询</button>
                                    <button type="button" class="btn btn-outline btn-default btn-add" onclick="platformChangeOrderPage.add()"><i class="fa fa-plus"></i>&nbsp;新 增</button> 
                                 </div>
                            </div>
                         </div>
                         </c:if>
                            <c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">收费类型</span>
                                    <select class="ipt form-control" id="chargeType" name="chargeType">
                                        <option value="" selected="selected">==请选择==</option>
                                        <option value="MAINTAIN" >维护费</option>
	   			                        <option value="BONUS" >分红费</option></select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                  <div class="input-group m-b">
                                      <span class="input-group-addon">支付状态</span>
                                       <cls:enmuSel className="com.team.lottery.enums.EPlatformChargePayStatus" id="payStatus" name="payStatus" options="class:ipt form-control"/>
                                  </div>
                             </div>
                              <div class="col-sm-5">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">归属月份</span>
                                    <div class="input-daterange input-group" >
                                        <input class="form-control layer-date" placeholder="开始月份" id="belongDateStart" style="background-color: white;" readonly="readonly"  onclick="WdatePicker({dateFmt:'yyyy-MM'})">
                                   
                                     <span class="input-group-addon">到</span>
                                        <input class="form-control layer-date" placeholder="结束月份" id="belongDateEnd" style="background-color: white;" readonly="readonly" onclick="WdatePicker({dateFmt:'yyyy-MM'})">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <div class="form-group nomargin text-right">
                                    <button type="button" class="btn btn-outline btn-default btn-js" onclick="platformChangeOrderPage.find()"><i class="fa fa-search"></i>&nbsp;查 询</button>
                                   
                        <!--             <button type="button" class="btn btn-outline btn-default btn-add" onclick="platformChangeOrderPage.add()"><i class="fa fa-plus"></i>&nbsp;新 增</button> -->
                                 </div>
                            </div>
                         </div>
                         </c:if>
                    </form>
                    </div>
                     <div class="jqGrid_wrapper">
                        <div class="ui-jqgrid ">
                            <div class="ui-jqgrid-view">
                                <div class="ui-jqgrid-hdiv">
                                    <div class="ui-jqgrid-hbox" style="padding-right:0">
                                        <table id="platformChargeOrderTable" class="ui-jqgrid-htable ui-common-table table table-bordered">
                                            <thead>
                                                <tr class="ui-jqgrid-labels">
                                                    <th class="ui-th-column ui-th-ltr">商户</th>
                                                    <th class="ui-th-column ui-th-ltr">收费类型</th>
                                                    <th class="ui-th-column ui-th-ltr">归属月份</th>
													<th class="ui-th-column ui-th-ltr">费用</th>
													<th class="ui-th-column ui-th-ltr">分红比例</th>
													<th class="ui-th-column ui-th-ltr">盈利金额  </th>
													<th class="ui-th-column ui-th-ltr">支付状态</th>
													<th class="ui-th-column ui-th-ltr">操 作</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>     
		         </div>
		      </div>
  <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/platformchargeorder/js/platformchargeorder.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerPlatformChangeOrderAction.js'></script>  
<script type="text/javascript">
function dateChoose(date){
	alert(date);
	
}
</script>

</body>
</html>

