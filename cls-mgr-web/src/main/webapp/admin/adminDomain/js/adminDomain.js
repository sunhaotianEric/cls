function AdminDomainPage(){}
var adminDomainPage = new AdminDomainPage();

adminDomainPage.param = {
   	
};

/**
 * 查询参数
 */
adminDomainPage.queryParam = {
		bizSystem : null,
		domains : null,
		allowIps : null
};

//分页参数
adminDomainPage.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize
};


$(document).ready(function() {
	adminDomainPage.initTableData(); //初始化查询数据
});



/**
 * 初始化列表数据
 */
AdminDomainPage.prototype.initTableData = function() {
	adminDomainPage.queryParam = getFormObj($("#queryForm"));
	adminDomainPage.table = $('#adminDomainTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"scrollX": true,
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			adminDomainPage.pageParam.pageSize = data.length;//页面显示记录条数
			adminDomainPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	manageAdminDomainAction.getAllAdminDomainPage(adminDomainPage.queryParam,adminDomainPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data":null},
		            {"data": "bizSystemName","bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            //设置IP地址显示样式超过长度40过后就用...表示
		            {"data": "domains",
		            	 render: function(data, type, row, meta) {
		            		 if(data.length > 40){
		            			 var title =  "<span title='"+data+"'>"+data.substring(0,40)+"．．．</span>";
		            			 return title;
		            		 }else{
		            			 return data;
		            		 }
		            	 },		
		            },
		            //设置IP地址显示样式超过长度40过后就用...表示
		            {"data": "allowIps",
		            	 render: function(data, type, row, meta) {
		            		 if(data.length > 40){
		            			 var title =  "<span title='"+data+"'>"+data.substring(0,40)+"．．．</span>";
		            			 return title;
		            		 }else{
		            			 return data;
		            		 }
		            	 },	
		            },
		            {"data": "createDateStr"},
		            {"data": "updateDateStr"},
		            {"data": "createAdmin"}, 
		            {"data": "id",/*"bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false,*/
	                	 render: function(data, type, row, meta) {
	                		 var str = "";
	                		 str += "	<a class='btn btn-info btn-sm btn-bj' onclick='adminDomainPage.edit("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;";
         			 		if(row.enable == 0){
         			 			str +="	<a class='btn btn-success btn-sm btn-bj' onclick='adminDomainPage.enableAdminDomain("+data+")'><i class='fa fa-pencil'></i>&nbsp;启用 </a>";
         			 		}else{
         			 			str +="	<a class='btn btn-warning btn-sm btn-bj' onclick='adminDomainPage.disAbleAdminDomain("+data+")'><i class='fa fa-pencil'></i>&nbsp;停用 </a>";
         			 		}
         			 		str += "	<a class='btn btn-danger btn-sm btn-xf' onclick='adminDomainPage.del("+data+")'>&nbsp;删除 </a>&nbsp;&nbsp;";
	                		 
	                		 return str;
	                		
	                	 }
	                }
	            ]
	}).api(); 
}

/**
 * 查询数据
 */
AdminDomainPage.prototype.find = function(){
	 adminDomainPage.queryParam.bizSystem = $("#bizSystem").val();
	 adminDomainPage.queryParam.domains = $("#domains").val();
	
	if(typeof(adminDomainPage.queryParam.bizSystem) === "undefined" || adminDomainPage.queryParam.bizSystem.trim()==""){
		adminDomainPage.queryParam.bizSystem =  bizSystem = null;
	}
	
	if(typeof(adminDomainPage.queryParam.domains) === "undefined" || adminDomainPage.queryParam.domains.trim()==""){
		adminDomainPage.queryParam.domains = domains = null;
	}
	
/*	if(typeof(adminDomainPage.queryParam.allowIps) === "undefined" || adminDomainPage.queryParam.allowIps.trim()==""){
		adminDomainPage.queryParam.allowIps = allowIps = null;
	}*/
	
	adminDomainPage.table.ajax.reload();
};

/**
 * 删除
 */
AdminDomainPage.prototype.del = function(id){
	
	   swal({
           title: "您确定要删除这条信息吗",
           text: "请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "删除",
           closeOnConfirm: false
       },
       function() {
    	   manageAdminDomainAction.delAdminAllowById(id,function(r){
   			if (r[0] != null && r[0] == "ok") {
   				swal({ title: "提示", text: "删除成功",type: "success"});
   				adminDomainPage.find();
   			}else if(r[0] != null && r[0] == "error"){
   				swal(r[1]);
   			}else{
   				swal("删除信息失败.");
   			}
   	    });
       });
};

/**
 * 启用
 */
AdminDomainPage.prototype.enableAdminDomain = function(id){
	   swal({
           title: "您确定要启用这条信息吗",
           text: "请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "启用",
           closeOnConfirm: false
       },
       function() {
    	   manageAdminDomainAction.enableAdminDomain(id,function(r){
   			if (r[0] != null && r[0] == "ok") {
   				swal({ title: "提示", text: "启用成功",type: "success"});
   				adminDomainPage.find();
   			}else if(r[0] != null && r[0] == "error"){
   				swal(r[1]);
   			}else{
   				swal("启用失败.");
   			}
   	    });
       });
};

/**
 * 停用
 */
AdminDomainPage.prototype.disAbleAdminDomain = function(id){
	   swal({
           title: "您确定要停用这条信息吗",
           text: "请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "停用",
           closeOnConfirm: false
       },
       function() {
    	   manageAdminDomainAction.disAbleAdminDomain(id,function(r){
   			if (r[0] != null && r[0] == "ok") {
   				swal({ title: "提示", text: "成功停用",type: "success"});
   				adminDomainPage.find();
   			}else if(r[0] != null && r[0] == "error"){
   				swal(r[1]);
   			}else{
   				swal("停用失败.");
   			}
   	    });
       });
};

/**
 * 添加
 */
AdminDomainPage.prototype.add=function(){
	
	 layer.open({
         type: 2,
         title: '后台域名IP授权管理新增',
         maxmin: false,
         shadeClose: true,
         //点击遮罩关闭层
         area: ['800px', '420px'],
         //area: ['60%', '45%'],
         content: contextPath + "/managerzaizst/adminDomain/editAdminDomain.html",
     /*    btn: ['确 定', '关闭'],
         yes: function(index, layero){
        	 alert(index);
        	 var iframeWin = window['layui-layer-iframe' + index].window;
        	 iframeWin.editadminDomainPage.saveData();
        	 iframeWin.saveData();
        	  },*/
         cancel: function(index){ 
        	 adminDomainPage.find();
      	   }
     });
}

/**
 * 编辑
 */
AdminDomainPage.prototype.edit=function(id){
	
	  layer.open({
           type: 2,
           title: '后台域名IP授权管理编辑',
           maxmin: false,
           shadeClose: true,
           //点击遮罩关闭层
           area: ['800px', '420px'],
           //area: ['60%', '45%'],
           content: contextPath + "/managerzaizst/adminDomain/"+id+"/editAdminDomain.html",
           cancel: function(index){ 
        	   adminDomainPage.find();
        	   }
       });
}

AdminDomainPage.prototype.closeLayer=function(index){
	layer.close(index);
	adminDomainPage.find();
}


