function EditAdminDomainPage(){}
var editAdminDomainPage = new EditAdminDomainPage();

editAdminDomainPage.param = {
   	
};

$(document).ready(function() {
	var id = editAdminDomainPage.param.id;  //获取查询ID
	if(id != null){
		editAdminDomainPage.getData(id);
	}
});

EditAdminDomainPage.prototype.testReg = function(data){
	var money=/[,]/g;
	if(money.test(data)){
		swal("不能能有逗号!");
		return false;
	}
	return true;
}

/**
 * 保存更新信息
 */
EditAdminDomainPage.prototype.saveData = function(){
	var id = $("#id").val();
	var bizSystem = $("#bizSystem").val();
	var domains = $("#domains").val();
	var allowIps = $("#allowIpsContent").val();
	var flagdomains = editAdminDomainPage.testReg(domains);
	if(!flagdomains){
		return;
	}
	if(currentUser.bizSystem=='SUPER_SYSTEM' && (bizSystem==null || bizSystem.trim()=="")){
		swal("请输入业务系统!");
		$("#bizSystem").focus();
		return;
	}
	
	if(domains==null && domains.trim()==""){
		swal("请输入域名!");
		$("#domains").focus();
		return;
	}else{
//		var reg = /^(http:\/\/+)?[A-Za-z0-9]+\.[A-Za-z0-9]+[\/=\?%\-&_~`@[\]\':+!]*([^<>\"\"])*$/
		var reg = /^(?=^.{3,255}$)[a-zA-Z0-9][-a-zA-Z0-9]{0,62}(\.[a-zA-Z0-9][-a-zA-Z0-9]{0,62})+$/
//		var reg=/(([A-Za-z0-9-~]+)\.)+([A-Za-z0-9-~\/])+$/;
		var failDomain="";
		var allDomains=domains.split(",");
		for(var i=0;i<allDomains.length;i++){
			var domain= allDomains[i];
			var test=reg.test(domain);
			if(!test){
				failDomain +=domain + ",";
			}
		}
		if(failDomain !=""){
			swal("域名:["+failDomain.substring(0,failDomain.length-1)+"]不合法！");
			return;
		}
	}
	
	var failIp="";
	if(allowIps!=null && allowIps.trim()!=""){
		var reg = /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/;
//		var reg = /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/;
		var allIp=allowIps.split(",");
		for(var i=0;i<allIp.length;i++){
			var ip= allIp[i];
			var test=reg.test(ip);
			if(!test){
				failIp += ip +",";
			}
		}
		if(failIp != ""){
			swal("IP:["+failIp.substring(0,failIp.length-1)+"]不合法！");
			return;
		}
	}
    	
	var AdminDomain = {};
	AdminDomain.id = editAdminDomainPage.param.id;
	AdminDomain.bizSystem = bizSystem;
	AdminDomain.domains = domains;
	AdminDomain.allowIps = allowIps;
	
	manageAdminDomainAction.saveAdminDomain(AdminDomain,function(r){
		if (r[0] != null && r[0] == "ok") {
			 swal({
		           title: r[1],
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.adminDomainPage.closeLayer(index);
		   	    }); 
		}else if(r[0] != null && r[0] == "error"){
			swal("保存信息失败.");
		}else{
			swal("保存信息失败.");
		}
    });
};

/**
 * 获取数据
 */
EditAdminDomainPage.prototype.getData = function(id){
	manageAdminDomainAction.getAdminAllowById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
	      $("#bizSystem").val(r[1].bizSystem);
	      $("#domains").val(r[1].domains);
	      $("#allowIpsContent").val(r[1].allowIps);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("保存信息失败.");
		}
    });
};