<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>  
    <head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>后台域名IP授权配置编辑</title>
    <link rel="shortcut icon" href="favicon.ico">
    <jsp:include page="/admin/include/include.jsp"></jsp:include>

    <%
	 String id = request.getParameter("id");  
	%>
</head>
  <body class="gray-bg">
        <div class="wrapper wrapper-content animated fadeIn">
         <form action="javascript:void(0)">
	        <div class="row border-bottom" style="padding-left: 0px;">
				<div class="col-sm-12">
					<p style="color: red">温馨提醒：多个IP填写时用逗号隔开",",IP记录可为空（这时相当于只通过域名来限制，不限制IP）,相同域名不允许重复添加</p>
					<p style="color: red">匹配规则是先匹配域名，然后再匹配IP,若都不匹配，则无法正常访问</p>
				</div>
			</div>
            <div class="row">
             <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                <div class="col-sm-4">
                    <div class="input-group m-b">
                        <span class="input-group-addon">商户</span>
                        <input type="hidden" readonly="readonly" id="id"/>
                        <% if(id==null){  %>
                       <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control" hasSuperSystemOption="true"/>
                       <%}else{ %>
                       <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control,disabled:'disabled'" />
                       <%} %>
                        </div>
                </div>
                </c:if>
                
              	<div class="col-sm-10">
                          <div class="input-group m-b">
                          <span class="input-group-addon">域名</span>
                          <input id='domains' name="domains" type="text" value="" class="form-control" ></div>
                </div>
               <!-- 设置IP地址的时候使用文本框显示 -->
               <div class="col-sm-8">
                              <div class="form-group">
                              	  <input id="allowIps" name="" type="hidden"  />
                                  <label class="col-sm-1 control-label">IP:</label>
                                  <div class="col-sm-8">
                                      <textarea name="comment" class="form-control" required="" style="height: 100px;width:570px" aria-required="true" id="allowIpsContent"></textarea>
                                  </div>
                              </div>
                          </div>
                <div class="col-sm-8">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button type="button" class="btn btn-w-m btn-white" onclick="editAdminDomainPage.saveData()">提 交</button></div>
                        <div class="col-sm-6 text-center">
                            <!-- <button type="button" class="btn btn-w-m btn-white">重 置</button> -->
                            <input type="reset" class="btn btn-w-m btn-white" value="重 置"/>
                            </div>
                    </div>
                </div>
            </div>
            </form>
        </div>

      <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/adminDomain/js/editAdminDomain.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/manageAdminDomainAction.js'></script> 
     <script type="text/javascript">
     editAdminDomainPage.param.id = <%=id%>;
	</script>
</body>
</html>

