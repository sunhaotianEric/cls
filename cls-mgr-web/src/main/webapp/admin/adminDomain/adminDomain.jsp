<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>  
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>后台域名IP授权管理</title>
        <link rel="shortcut icon" href="favicon.ico">
        <jsp:include page="/admin/include/include.jsp"></jsp:include>
        <script  type="text/javascript" src="<%=path%>/resources/My97DatePicker/WdatePicker.js"></script>
</head>

   <body>
        <div class="animated fadeIn">
            <div class="ibox-content">
                 <div class="row m-b-sm m-t-sm">
                    <form class="form-horizontal" id="queryForm">
                    <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                        <div class="row">
                             <div class="col-sm-3">
                                  <div class="input-group m-b">
                                      <span class="input-group-addon">商户</span>
                                       <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
                                  </div>
                             </div>
                            <div class="col-sm-3">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">域名</span>
                                    <input id='domains' name="domains" type="text" value="" class="form-control" ></div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group nomargin text-right">
                                    <button type="button" class="btn btn-outline btn-default btn-js" onclick="adminDomainPage.find()"><i class="fa fa-search"></i>&nbsp;查 询</button>
                               
                        		   <button type="button" class="btn btn-outline btn-default btn-add" onclick="adminDomainPage.add()"><i class="fa fa-plus"></i>&nbsp;新 增</button>
                                 </div>
                            </div>
                         </div>
                         </c:if>
<%--                         <c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">域名</span>
                                    <input id='userName' name="userName" type="text" value="" class="form-control" ></div>
                            </div>
                            <div class="col-sm-8">
                                <div class="form-group nomargin text-right">
                                    <button type="button" class="btn btn-outline btn-default btn-js" onclick="adminDomainPage.find()"><i class="fa fa-search"></i>&nbsp;查 询</button>
                                   
                                   <button type="button" class="btn btn-outline btn-default btn-add" onclick="adminDomainPage.add()"><i class="fa fa-plus"></i>&nbsp;新 增</button>
                                 </div>
                            </div>
                         </div>
                         </c:if> --%>
                    </form>
                    </div>
                     <div class="jqGrid_wrapper">
                        <div class="ui-jqgrid ">
                            <div class="ui-jqgrid-view">
                                <div class="ui-jqgrid-hdiv">
                                    <div class="ui-jqgrid-hbox" style="padding-right:0">
                                        <table id="adminDomainTable" class="ui-jqgrid-htable ui-common-table table table-bordered">
                                            <thead>
                                                <tr class="ui-jqgrid-labels">
                                                 	<th class="ui-th-column ui-th-ltr">序号</th>
                                                    <th class="ui-th-column ui-th-ltr">商户</th>
                                                    <th class="ui-th-column ui-th-ltr">域名</th>
                                                    <th class="ui-th-column ui-th-ltr">IP</th>
													<th class="ui-th-column ui-th-ltr">创建时间</th>
													<th class="ui-th-column ui-th-ltr">更新时间</th>
													<th class="ui-th-column ui-th-ltr">操作人</th>
													<th class="ui-th-column ui-th-ltr">操 作</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>     
		         </div>
		      </div>
  <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/adminDomain/js/adminDomain.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/manageAdminDomainAction.js'></script>  
<script type="text/javascript">
function dateChoose(date){
	alert(date);
	
}
</script>

</body>
</html>

