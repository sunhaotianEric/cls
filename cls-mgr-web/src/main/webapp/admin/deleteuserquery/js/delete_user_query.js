function DeleteUserQueryPage(){}
var deleteUserQuery = new DeleteUserQueryPage();

deleteUserQuery.param = {
};

/**ManagerSystemAction.java
 * 查询参数
 */
deleteUserQuery.queryParam = {

};

//分页参数
deleteUserQuery.pageParam = {
		queryMethodParam : null,
		pageNo : 1,
		pageMaxNo : 1,  //最大的页数
		skipHtmlStr : ""
};

/**
 * 查询参数
 */
deleteUserQuery.queryParam = {
		id:null,
		username : null,
		truename : null,
		dailiLevel : null,
		sscrebate : null
};

$(document).ready(function() {
	deleteUserQuery.getdelUserList();//默认加载所有被删会员
});

/**
 * 查找满足条件的用户
 */
DeleteUserQueryPage.prototype.getdelUserByParameter = function(){
	deleteUserQuery.pageParam.queryMethodParam = 'getdelUserByParameter';
	deleteUserQuery.queryParam = {};
	var username=$("#userName").val();
	var truename=$("#trueName").val();
	var dailiLevel=$("#dailiLevel").val();
	var sscrebate=$("#SscRebate").val();
	if(username==""){
		deleteUserQuery.queryParam.username=null;
	}else{
		deleteUserQuery.queryParam.username=username;
	}
	if(truename==""){
		deleteUserQuery.queryParam.truename=null;
	}else{
		deleteUserQuery.queryParam.truename=truename;
	}
	if(dailiLevel==""){
		deleteUserQuery.queryParam.dailiLevel=null;
	}else{
		deleteUserQuery.queryParam.dailiLevel=dailiLevel;
	}
	if(sscrebate == ""){
		deleteUserQuery.queryParam.sscerebate=null;
	}else{
		deleteUserQuery.queryParam.sscrebate=sscrebate
	}
	deleteUserQuery.getUserByParameter(deleteUserQuery.queryParam);
};

DeleteUserQueryPage.prototype.getUserByParameter = function(queryParam){
	managerDelUserQueryAction.getdelUserByParameter(queryParam,function(r){
		if (r[0] != null && r[0] == "ok") {
			deleteUserQuery.refreshdeleteUserPages(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询用户数据失败.");
		}
	});
};

/**
 * 条件查找
 */
DeleteUserQueryPage.prototype.selectByParame = function(){
	deleteUserQuery.pageParam.queryMethodParam = 'selectByParame';
	/*deleteUserQuery.queryParam = {};
	var username=$("#userName").val();
	var truename=$("#trueName").val();
	var dailiLevel=$("#dailiLevel").val();
	var sscrebate=$("#SscRebate").val();
	if(username==""){
		deleteUserQuery.queryParam.username=null;
	}else{
		deleteUserQuery.queryParam.username=username;
	}
	if(truename==""){
		deleteUserQuery.queryParam.truename=null;
	}else{
		deleteUserQuery.queryParam.truename=truename;
	}
	if(dailiLevel==""){
		deleteUserQuery.queryParam.dailiLevel=null;
	}else{
		deleteUserQuery.queryParam.dailiLevel=dailiLevel;
	}
	if(sscrebate == ""){
		deleteUserQuery.queryParam.sscerebate=null;
	}else{
		deleteUserQuery.queryParam.sscrebate=sscrebate
	}*/

	deleteUserQuery.queryDeleteUserOrders(deleteUserQuery.queryParam,deleteUserQuery.pageParam.pageNo);
};

/**
 * 分页查询用户列表
 */
DeleteUserQueryPage.prototype.getdelUserList = function(){
	deleteUserQuery.pageParam.queryMethodParam = 'delAllUsers';
	deleteUserQuery.queryDeleteUserOrders(deleteUserQuery.queryParam,deleteUserQuery.pageParam.pageNo);
};
/**
 * 
 */
DeleteUserQueryPage.prototype.queryDeleteUserOrders = function(queryParam,pageNo){
	managerDelUserQueryAction.delUserList(queryParam,pageNo,function(r){
		if (r[0] != null && r[0] == "ok") {
			deleteUserQuery.refreshdeleteUserPages(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询用户数据失败.");
		}
	});
};

/**
 * 恢复被删会员
 */
DeleteUserQueryPage.prototype.deleteRegression = function(obj,pageParam){
	if(confirm("确认恢复吗？")){
		managerDelUserQueryAction.deleteRegressionById(pageParam,function(r){
			if (r[0] != null && r[0] == "ok") {
				alert("恢复成功！")
				window.location.href= 'delete_user_query.html';
			}else if(r[0] != null && r[0] == "error"){
				alert(r[1]);
			}else{
				showErrorDlg(jQuery(document.body), "用户数据恢复失败.");
			}
		});
	}
}

/**
 * 根据条件查找对应页
 */
DeleteUserQueryPage.prototype.pageQuery = function(pageNo){
	if(pageNo < 1){
		deleteUserQuery.pageParam.pageNo = 1;
	}else if(pageNo > deleteUserQuery.pageParam.pageMaxNo){
		deleteUserQuery.pageParam.pageNo = deleteUserQuery.pageParam.pageMaxNo;
	}else{
		deleteUserQuery.pageParam.pageNo = pageNo;
	}
	//如果页码小于等于0，则不进行分页查找
	if(deleteUserQuery.pageParam.pageNo <= 0){
		return;
	}

 if(deleteUserQuery.pageParam.queryMethodParam='selectByParame'){
		deleteUserQuery.selectByParame();
	}else{
		alert("类型对应的查找方法未找到.");
	}
};
/**
 * 刷新列表
 * @param page
 */
DeleteUserQueryPage.prototype.refreshdeleteUserPages = function(page){
	var deleteListObj = $("#deleteOrderList");
	deleteListObj.html("");
	var str = "";
	var users="";
	if(page.pageContent != null){
		users = page.pageContent;
	}else{
		users=page;
	}
	/**
	 * 记录数据显示
	 */
	for(var i = 0 ; i < users.length; i++){
		var querydelete = users[i];

		str += "<tr>";
		str += "  <td>"+ querydelete.id+"</td>";
		str += "  <td>"+ querydelete.username +"</td>";
		var truename = "";
		if(querydelete.truename != null && querydelete.truename != "") {
			truename = querydelete.truename;
		}
		str += "  <td>"+ truename +"</td>";
		var sex="";
		if(querydelete.sex !=null && querydelete.sex != ""){
			sex=querydelete.sex;
		}
		str += "  <td>"+ sex +"</td>";
		str += "  <td>"+ querydelete.money +"</td>";
		str += "  <td>"+ querydelete.uermoney +"</td>";
		str += "  <td>"+ querydelete.sscrebate +"</td>";
		str += "  <td>"+ querydelete.dailiLevelStr +"</td>";
		var phone="";
		if(querydelete.phone != null && querydelete.phone !="" ){
			phone=querydelete.phone;
		}
		str += "  <td>"+ phone +"</td>";
		var email="";
		if(querydelete.email != null && querydelete.email !=""){
			email=querydelete.email;
		}
		str += "  <td>"+ email +"</td>";	
		str += "  <td>"+querydelete.deleteDateStr+"</td>";
		var deleteCause="";
		if(querydelete.deleteCause !=null && querydelete.deleteCause !=""){
			deleteCause=querydelete.deleteCause;
		}
		str += "  <td>"+ deleteCause +"</td>";
		str+=" <td><a style='color: blue;text-decoration: underline;' href='javascript:void(0)' onclick='deleteUserQuery.deleteRegression(this,"+querydelete.id+")'>恢复</a></td>";
		str += "</tr>";
		deleteListObj.append(str);
		str = "";
	}
	/**
	 *  记录统计
	 */
	var pageMsg = $("#pageMsg");
	var pageStr = "";
	pageStr += "<th colspan='100'>";
	pageStr += "  当前第" + page.pageNo + "页/共" + page.totalPageNum +"页&nbsp;&nbsp;共" + page.totalRecNum +"条记录&nbsp;&nbsp;<a href='javascript:void(0)'  onclick='deleteUserQuery.pageQuery(1)'>首页</a>&nbsp;";
	pageStr += "<a href='javascript:void(0)' onclick='deleteUserQuery.pageParam.pageNo--;deleteUserQuery.pageQuery(deleteUserQuery.pageParam.pageNo)'>上一页</a>&nbsp;";
	pageStr += "<a href='javascript:void(0)' onclick='deleteUserQuery.pageParam.pageNo++;deleteUserQuery.pageQuery(deleteUserQuery.pageParam.pageNo)'>下一页</a>&nbsp;";
	pageStr += "<a href='javascript:void(0)' onclick='deleteUserQuery.pageQuery("+ page.totalPageNum +")'>尾页</a>";

	var skipHtmlStr = "";
	if($("#pageSkip").html() != null){
		$("#pageSkip").remove();
	}
	skipHtmlStr += "<span id='pageSkip'>";
	skipHtmlStr += "&nbsp;&nbsp;跳转至";
	skipHtmlStr += "<select id='currentPageChoose' class='ipt' onchange='deleteUserQuery.pageParam.pageNo = this.value;deleteUserQuery.pageQuery(deleteUserQuery.pageParam.pageNo)'  style='width:auto;'>";
	for(var i = 1; i <= page.totalPageNum; i++){
		skipHtmlStr += "<option value='"+ i +"'>"+ i +"</option>";
	}
	skipHtmlStr += "</select>";
	skipHtmlStr += "</span>";	
	pageStr += skipHtmlStr;
	pageStr += "</th>";
	pageMsg.html(pageStr);

	$("#currentPageChoose").find("option[value='"+ page.pageNo +"']").attr("selected",true);  //显示当前页码
	deleteUserQuery.pageParam.pageMaxNo = page.totalPageNum;  //标识最大的页数
}

