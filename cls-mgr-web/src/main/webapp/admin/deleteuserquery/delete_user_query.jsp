<%@ page language="java" contentType="text/html; charset=UTF-8"
 import = "com.team.lottery.enums.ELotteryModel"
 pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<html>
<head>
<meta charset="utf-8" />
<title>删除会员管理</title>
<jsp:include page="/admin/include/include.jsp"></jsp:include>
<!-- js -->
<script type="text/javascript" src="<%=path%>/admin/deleteuserquery/js/delete_user_query.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerOrderAction.js'></script>
<script type='text/javascript' src='<%=path%>/dwr/interface/managerUserAction.js'></script>
<script type='text/javascript' src='<%=path%>/dwr/interface/managerDelUserQueryAction.js'></script>
<style type="text/css">
caption, th {
    text-align: center;
}

.tb td{
    text-align: center;
}

.ui-dialog{
    width : auto;
}
</style>
</head>
<body>
   <div class="alert alert-info">当前位置<b class="tip"></b><a href="<%=path %>/managerzaizst/user/user_list.html">会员信息管理</a><b class="tip"></b>删除会员列表</div>

    <table class="tbform">
        <tbody>
            <tr>
                <td class="tdl">会员类型</td>
                <td class="detail">
                   <select class="ipt" id="dailiLevel">
                   		<option value=''>==请选择==</option>
                        <option value='ORDINARYAGENCY'>普通代理</option>
                        <option value='REGULARMEMBERS'>普通会员</option>
                        <option value='DIRECTAGENCY' >直属代理</option>
                    </select>
                </td>
                
                <td class="tdl">真实姓名</td>
                <td class="td_detail">
                    <input type="text" id="trueName" class="ipt"/></td>
                    
                    
                <td class="tdl">用户名 </td>
                <td>
                    <input type="text" id="userName" class="ipt"/>
                </td>
                
                
                 <td class="tdl">时时彩返点 </td>
                <td>
                    <input type="text" id="SscRebate" class="ipt"/>
                </td>
                
                
                <td colspan="10" align="right">
                    <input class="btn" id="find" type="button" onclick="deleteUserQuery.pageParam.pageNo = 1;deleteUserQuery.getdelUserByParameter()" value="查询" />
                </td>
            </tr>
        </tbody>
    </table>
    <table class="tb">
        <tr>
          <th>用户ID</th>
          <th>用户名</th>
          <th>真实姓名</th>
          <th>性别</th>
          <th>全部金额</th>
          <th>可用余额</th>
          <th>时时彩返点</th>
          <th>代理级别 </th>
          <th>电话号码</th>
          <th>电子邮件</th>
          <th>删除时间</th>
          <th>删除原因</th>
          <th>是否恢复</th>
        </tr>
      <tbody  id="deleteOrderList">
        <!--  
        <tr>
          <td>HS30</td>
          <td>现金</td>
          <td>票据 </td>
          <td>负责人 </td>
          <td>3009.40</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td align="center">
             <input class="btn btn-mini btnedit"  type="button" value="编辑"/>
             <input class="btn btn-mini btnedit"  type="button" value="续费"/>
             <input class="btn btn-mini btnedit"  type="button" value="积分"/>
             <input class="btn btn-mini btnedit"  type="button" value="删除"/>                       
          </td>
        </tr>
        -->
      </tbody>
      <tr class="pager" id='pageMsg'>
        <!-- 
	        <th colspan="100">当前第5页/共55页&nbsp;&nbsp;共650条记录&nbsp;&nbsp;<a>首页</a>&nbsp;<a>下一页</a>&nbsp;
	            <a class="current">1</a>&nbsp;<a>2</a>&nbsp;<a>3</a>&nbsp;<a>4</a>&nbsp;
	            <a class="badge badge-warning">5</a>&nbsp;<a class="navpage">...</a>&nbsp;<a>55</a>&nbsp;<a>上一页</a>&nbsp;<a>尾页</a>
	        </th>
         -->
      </tr>
    </table>
</body>
</html>

