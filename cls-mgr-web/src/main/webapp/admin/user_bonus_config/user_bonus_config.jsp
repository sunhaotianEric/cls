<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<html>
<head>
    <meta charset="utf-8" />
    <title>代理福利参数设置</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/user_bonus_config/js/user_bonus_config.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerUserBonusConfigAction.js'></script>
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerPayBankAction.js'></script>
    
     <%
    	String id = request.getParameter("id");  //获取查询ID
    %>
	<script type="text/javascript">
		var tableId = <%=id%>;
	</script>
</head>
<body>
   <div class="alert alert-info">当前位置<b class="tip"></b>网站信息管理<b class="tip"></b>代理福利参数设置</div>
   
   <div align="left">
       <input class="btn" id="find" type="button" value="提交" onclick="userBounsConfigPage.saveTreatmentConfig()" />
   </div>
    <br/>
   	
   	<table class="tbform list" id="halfMonthBonusTable">
        <thead>
            <tr class="tr">
                <th colspan="6">半月分红设置<i class="tip-down"></i></th>
            </tr>
        </thead>
        <tbody style="display: none;">
            <tr>
                <td colspan="3">申请半月分红开关</td>
                <td colspan="3">
                	订单处理管理员
                	<select class="select adminSel" id="dealHalfMonthBonusSel"></select>   
                	<input class="btn" type="button" value="添加" onclick="userBounsConfigPage.saveDealAdmin('dealHalfMonthBonus')" />
                	<input class="btn" type="button" value="删除" onclick="userBounsConfigPage.delDealAdmin('dealHalfMonthBonus')" />
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <select class="ipt" id='applyHalfMonthBonusSwitch'>
                        <option value='1' selected="selected">开启</option>
                        <option value='0'>关闭</option>
                    </select>
                </td>
                <td colspan="3">
                    <input type="hidden" id="dealHalfMonthBonusAdmins"/>
                    <input type="text" id='dealHalfMonthBonusAdminNames' class="ipt" size="50" readonly="readonly"/>                      
                </td>
            </tr>           
           <!--  <tr>
                <td colspan="3">半月分红直属比例</td>
                <td colspan="3">半月分红总代比例</td>
            </tr>
            <tr>
                <td colspan="3">
                    <input type="text" id='halfMonthBonus1956' class="ipt" />               
                </td>
                <td colspan="3">
                    <input type="text" id='halfMonthBonus1954' class="ipt" />               
                </td>
            </tr>   -->
            <tr>
                <td colspan="3">上半月分红每月申请日期</td>
                <td colspan="3">下半月分红每月申请日期</td>
            </tr>
            <tr>
                <td colspan="3">
                    <input type="text" id='applyUpHalfMonthBonusTime' class="ipt" /> 
                </td>
                <td colspan="3">
                    <input type="text" id='applyDownHalfMonthBonusTime' class="ipt" />              
                </td>
            </tr>  
            <tr>
                <td colspan="3">日量判断开关</td>
                <td colspan="3">降级处理开关</td>
            </tr>
            <tr>
                <td colspan="3">
                    <select class="ipt" id='orderAmountJudgeSwitch'>
                        <option value='1' selected="selected">开启</option>
                        <option value='0'>关闭</option>
                    </select>
                </td>
                <td colspan="3">
                    <select class="ipt" id='downDealSwitch'>
                        <option value='1' selected="selected">开启</option>
                        <option value='0'>关闭</option>
                    </select>              
                </td>
            </tr> 
            <tr>
                <td colspan="3">1958分红平均日量值</td>
                <td colspan="3">1956分红平均日量值 与至少三个玩家要达到的平均日量值</td>
            </tr>
            <tr>
                <td colspan="3">
                    <input type="text" id='reach1958DayNum' class="ipt" /> 
                </td>
                <td colspan="3">
                    <input type="text" id='reach1956DayNum' class="ipt" /> 
                    <input type="text" id='userReach1956DayNum' class="ipt" />              
                </td>
            </tr>   
            <tr>
                <td colspan="3">半月分红日量允许差值</td>
                <td colspan="3">半月分红日量要求百分比</td>
            </tr>
            <tr>
                <td colspan="3">
                    <input type="text" id='halfMonthAllowDiff' class="ipt" /> 
                </td>
                <td colspan="3">
                    <input type="text" id='halfMonthDayNumPercent' class="ipt" />              
                </td>
            </tr>   
            <tr>
	         	<td colspan="6"> 半月分红亏损量与分红比例表  &nbsp;&nbsp; <a href="<%=path %>/managerzaizst/user_bonus_config/user_bonus_config_edit.html">新增分红设置</a></td>
	        </tr> 
        	<tr>
			   <th>序号</th>
			   <th>分红类型</th>
			   <th>代理类型</th>
			   <th>半月团队亏损量</th>
			   <th>分红比例</th>
			   <th>操作</th>
			</tr>        
        </tbody>
        
        <tbody id="userBounsConfigHalfMonthBonusList" style="display: none;">
      	</tbody>
    </table>
    
    <table class="tbform list" >
        <thead>
            <tr class="tr">
                <th colspan="6">日分红设置<i class="tip-down"></i></th>
            </tr>
        </thead>
        <tbody style="display: none;">
            <tr>
                <td>申请日分红开关</td>
                <td>
                	订单处理管理员
                	<select class="select adminSel" id="dealDayBonusSel"></select>   
                	<input class="btn" type="button" value="添加" onclick="userBounsConfigPage.saveDealAdmin('dealDayBonus')" />
                	<input class="btn" type="button" value="删除" onclick="userBounsConfigPage.delDealAdmin('dealDayBonus')" />
                </td>
            </tr>
            <tr>
                <td>
                    <select class="ipt" id='applyDayBonusSwitch'>
                        <option value='1' selected="selected">开启</option>
                        <option value='0'>关闭</option>
                    </select>
                </td>
                <td>
               	    <input type="hidden" id="dealDayBonusAdmins"/>
                    <input type="text" id='dealDayBonusAdminNames' class="ipt" size="50" readonly="readonly"/>
                </td>
            </tr>           
            <tr>
                <td>日分红比例</td>
                <td>每月日分红发放日期</td>
            </tr>
            <tr>
                <td>
                    <input type="text" id='dayBonus' class="ipt" />               
                </td>
                <td>
                    <input type="text" id='dayBonusOpenTime' class="ipt" />               
                </td>
            </tr> 
            <tr>
                <td>申请日分红每日几点之前可申请</td>
            	<td></td>
            </tr>
            <tr>
                <td>
                    <input type="text" id='applyDayBonusTime' class="ipt" />               
                </td>
                <td>
                </td>
            </tr>  
        </tbody>
    </table>
    
    <table class="tbform list" id="dayCommissionTable">
        <thead>
            <tr class="tr">
                <th colspan="6">日佣金设置<i class="tip-down"></i></th>
            </tr>
        </thead>
        <tbody style="display: none;">
            <tr>
                <td colspan="3">申请日佣金开关</td>
                <td colspan="3">
                	订单处理管理员
                	<select class="select adminSel" id="dealDayCommissionSel"></select>   
                	<input class="btn" type="button" value="添加" onclick="userBounsConfigPage.saveDealAdmin('dealDayCommission')" />
                	<input class="btn" type="button" value="删除" onclick="userBounsConfigPage.delDealAdmin('dealDayCommission')" />
                </td>
                
            </tr>
            <tr>
                <td colspan="3">
                    <select class="ipt" id='applyDayCommissionSwitch'>
                        <option value='1' selected="selected">开启</option>
                        <option value='0'>关闭</option>
                    </select>
                </td>
                <td colspan="3">
                    <input type="hidden" id="dealDayCommissionAdmins"/>
                    <input type="text" id='dealDayCommissionAdminNames' class="ipt" size="50" readonly="readonly"/>           
                </td>
            </tr>   
            <tr>
            	<td colspan="3">申请日佣金每日几点之前可申请</td>
            	<td colspan="3"></td>
            </tr> 
            <tr>
            	<td colspan="3">
                    <input type="text" id='applyDayCommissionTime' class="ipt" />              
                </td>
            	<td colspan="3"></td>
            </tr>       
             
            <tr>
	         	<td colspan="6"> 日佣金奖励金额比例表  &nbsp;&nbsp; <a href="<%=path %>/managerzaizst/user_bonus_config/user_bonus_config_edit.html">新增分红设置</a></td>
	        </tr> 
	        <tr>
			   <th>序号</th>
			   <th>分红类型</th>
			   <th>消费金额</th>
			   <th>上级奖励</th>
			   <th>上上级奖励</th>
			   <th>操作</th>
			</tr>          
        </tbody>
        <tbody id="userBounsConfigDayCommissionList" style="display: none;">
      	</tbody> 
    </table>  
    
    <table class="tbform list" id="monthSalaryTable">
        <thead>
            <tr class="tr">
                <th colspan="6">月工资设置<i class="tip-down"></i></th>
            </tr>
        </thead>
        <tbody style="display: none;">
            <tr>
                <td colspan="3">申请月工资开关</td>
                <td colspan="3">
					订单处理管理员
                	<select class="select adminSel" id="dealMonthSalarySel"></select>   
                	<input class="btn" type="button" value="添加" onclick="userBounsConfigPage.saveDealAdmin('dealMonthSalary')" />
                	<input class="btn" type="button" value="删除" onclick="userBounsConfigPage.delDealAdmin('dealMonthSalary')" />
				</td>
            </tr>
            <tr>
                <td colspan="3">
                    <select class="ipt" id='applyMonthSalarySwitch'>
                        <option value='1' selected="selected">开启</option>
                        <option value='0'>关闭</option>
                    </select>
                </td>
                <td colspan="3">
                    <input type="hidden" id="dealMonthSalaryAdmins"/>
                    <input type="text" id='dealMonthSalaryAdminNames' class="ipt" size="50" readonly="readonly"/>         
                </td>
            </tr> 
            <tr>
            	<td colspan="3">申请月工资每月日期</td>
            	<td colspan="3"></td>
            </tr>
            <tr>
            	<td colspan="3">
            		<input type="text" id='applyMonthSalaryTime' class="ipt" />  
            	</td>
            	<td colspan="3"></td>
            </tr> 
            <tr>
	         	<td colspan="6"> 月工资日量与奖励金额比例表  &nbsp;&nbsp; <a href="<%=path %>/managerzaizst/user_bonus_config/user_bonus_config_edit.html">新增分红设置</a></td>
	        </tr> 
	        <tr>
			   <th>序号</th>
			   <th>分红类型</th>
			   <th>代理类型</th>
			   <th>团队月销量</th>
			   <th>月工资</th>
			   <th>操作</th>
			</tr>         
        </tbody>
        <tbody id="userBounsConfigMonthSalaryList" style="display: none;">
      	</tbody> 
    </table>  
    
    <table class="tbform list" id="daySalaryTable">
        <thead>
            <tr class="tr">
                <th colspan="6">日工资设置<i class="tip-down"></i></th>
            </tr>
        </thead>
        <tbody style="display: none;">
            <tr>
                <td colspan="3">申请日工资开关</td>
                <td colspan="3">
					订单处理管理员
                	<select class="select adminSel" id="dealDaySalarySel"></select>   
                	<input class="btn" type="button" value="添加" onclick="userBounsConfigPage.saveDealAdmin('dealDaySalary')" />
                	<input class="btn" type="button" value="删除" onclick="userBounsConfigPage.delDealAdmin('dealDaySalary')" />
				</td>
            </tr>
            <tr>
                <td colspan="3">
                    <select class="ipt" id='applyDaySalarySwitch'>
                        <option value='1' selected="selected">开启</option>
                        <option value='0'>关闭</option>
                    </select>
                </td>
                <td colspan="3">
                    <input type="hidden" id="dealDaySalaryAdmins"/>
                    <input type="text" id='dealDaySalaryAdminNames' class="ipt" size="50" readonly="readonly"/>               
                </td>
            </tr>
            <tr>
            	<td colspan="3">申请日工资每日几点之前可申请</td>
            	<td colspan="3"></td>
            </tr> 
            <tr>
            	<td colspan="3">
					<input type="text" id='applyDaySalaryTime' class="ipt" />  
				</td>
            	<td colspan="3"></td>
            </tr> 
            <tr>
	         	<td colspan="6"> 日工资日量与奖励金额比例表  &nbsp;&nbsp; <a href="<%=path %>/managerzaizst/user_bonus_config/user_bonus_config_edit.html">新增分红设置</a></td>
	        </tr> 
	        <tr>
			   <th>序号</th>
			   <th>分红类型</th>
			   <th>代理类型</th>
			   <th>团队日量</th>
			   <th>日工资</th>
			   <th>操作</th>
			</tr>
             
        </tbody>
        <tbody id="userBounsConfigDaySalaryList" style="display: none;">
      	</tbody>  
    </table>  
    
    
        <table class="tbform list" id="dayActivityTable">
        <thead>
            <tr class="tr">
                <th colspan="6">日活动设置<i class="tip-down"></i></th>
            </tr>
        </thead>
        <tbody style="display: none;">
         	 <tr>
                <td colspan="3">申请日活动开关</td>
                <td colspan="3">
             	   	订单处理管理员
	                <select class="select adminSel" id="dealDayActivitySel"></select>   
	                <input class="btn" type="button" value="添加" onclick="userBounsConfigPage.saveDealAdmin('dealDayActivity')" />
	                <input class="btn" type="button" value="删除" onclick="userBounsConfigPage.delDealAdmin('dealDayActivity')" />
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <select class="ipt" id='applyDayActivitySwitch'>
                        <option value='1' selected="selected">开启</option>
                        <option value='0'>关闭</option>
                    </select>
                </td>
                 <td colspan="3">
                    <input type="hidden" id="dealDayActivityAdmins"/>
                    <input type="text" id='dealDayActivityAdminNames' class="ipt" size="50" readonly="readonly"/>               
                </td>
            </tr>
            <tr>
            	<td colspan="3">申请日活动每日几点之前可申请</td>
            	<td colspan="3"></td>
            </tr> 
            <tr>
            	<td colspan="3">
					<input type="text" id='applyDayActivityTime' class="ipt" />  
				</td>
            	<td colspan="3"></td>
            </tr> 
            <tr>
	         	<td colspan="6"> 日活动日量与奖励金额比例表  &nbsp;&nbsp; <a href="<%=path %>/managerzaizst/user_bonus_config/user_bonus_config_edit.html">新增活动奖励设置</a></td>
	        </tr> 
	        <tr>
			   <th>序号</th>
			   <th>日活动</th>
			   <th>投注量</th>
			   <th>赠送金额</th>
			   <th>操作</th>
			</tr>
             
        </tbody>
        <tbody id="userBounsConfigDayActivityList" style="display: none;">
      	</tbody>  
    </table>  
   	
</body>
</html>

