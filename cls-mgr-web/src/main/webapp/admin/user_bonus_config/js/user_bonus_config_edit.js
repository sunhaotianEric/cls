function UserBonusConfigEditPage(){}
var userBonusConfigEditPage = new UserBonusConfigEditPage();

userBonusConfigEditPage.param = {
   	
};

$(document).ready(function() {
	$("#measureType").change(function(){
		if($(this).val() == 'DAY_COMMISSION_MEASURE') {
			$("#scale2Tr").show();
			$("#measurementDesc").html("消费金额：");
			$("#scaleDesc").html("上级奖励：");
		} else if($(this).val() == 'MONTH_SALARY_MEASURE') {
			$("#scale2Tr").hide();
			$("#measurementDesc").html("团队月销量：");
			$("#scaleDesc").html("月工资：");
		} else if($(this).val() == 'DAY_SALARY_MEASURE') {
			$("#scale2Tr").hide();
			$("#measurementDesc").html("团队日量：");
			$("#scaleDesc").html("日工资：");
		} else if($(this).val() == 'HALF_MONTH_BONUS_MEASURE') {
			$("#scale2Tr").hide();
			$("#measurementDesc").html("半月团队亏损量：");
			$("#scaleDesc").html("分红比例：");
		} else if($(this).val() == 'DAY_ACTIVITY_MEASURE') {
			$("#scale2Tr").hide();
			$("#measurementDesc").html("日投注量：");
			$("#scaleDesc").html("奖励金额");
		}
	});
	var bonusConfigId = userBonusConfigEditPage.param.id;  //获取查询的公告ID
	if(bonusConfigId != null){
		$("#addOrEditSpan").html("编辑");
		userBonusConfigEditPage.editBonusConfig(bonusConfigId);
		$("#measureType").attr("disabled","disabled");
	}
});


/**
 * 保存分红配置
 */
UserBonusConfigEditPage.prototype.saveData = function(){
	var id = $("#bonusConfigId").val();
	var measureType = $("#measureType").val();
	var dailiLevel = $("#dailiLevel").val();
	var measurement = $("#measurement").val();
	var scale = $("#scale").val();
	var scale2 = getSearchVal("scale2");
	var description = getSearchVal("description");
	
	if(measurement==null || measurement.trim()==""){
		alert("请输入量值!");
		$("#measurement").focus();
		return;
	}
	if(scale==null || scale.trim()==""){
		alert("请输入对应值!");
		$("#scale").focus();
		return;
	}
	
	var bonusCofig = {};
	bonusCofig.id = id;
	bonusCofig.measureType = measureType;
	bonusCofig.dailiLevel = dailiLevel;
	bonusCofig.measurement = measurement;
	bonusCofig.scale = scale;
	bonusCofig.scale2 = scale2;
	bonusCofig.description = description;
	managerUserBonusConfigAction.saveOrUpdateUserBonusConfig(bonusCofig,function(r){
		if (r[0] != null && r[0] == "ok") {
			alert("保存成功");
			//跳转到平台待遇设置页面，并初始化
			var tableId = "1";
			if(bonusCofig.measureType == 'DAY_COMMISSION_MEASURE') {
				tableId = "1";
			} else if(bonusCofig.measureType == 'MONTH_SALARY_MEASURE') {
				tableId = "2";
			} else if(bonusCofig.measureType == 'DAY_SALARY_MEASURE') {
				tableId = "3";
			} else if(bonusCofig.measureType == 'HALF_MONTH_BONUS_MEASURE') {
				tableId = "4";
			} else if(bonusCofig.measureType == 'DAY_ACTIVITY_MEASURE') {
				tableId = "5";
			}
			self.location = contextPath + "/managerzaizst/user_bonus_config/"+ tableId +"/user_bonus_config.html";
		}else if(r[0] != null && r[0] == "error"){
			showErrorDlg(jQuery(document.body), r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "保存分红配置失败.");
		}
    });
};


/**
 * 赋值分红配置信息
 */
UserBonusConfigEditPage.prototype.editBonusConfig = function(id){
	managerUserBonusConfigAction.getUserBonusConfigById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			if(r[1].measureType == 'DAY_COMMISSION_MEASURE') {
				$("#scale2Tr").show();
				$("#measurementDesc").html("消费金额：");
				$("#scaleDesc").html("上级奖励：");
			} else if(r[1].measureType == 'MONTH_SALARY_MEASURE') {
				$("#scale2Tr").hide();
				$("#measurementDesc").html("团队月销量：");
				$("#scaleDesc").html("月工资：");
			} else if(r[1].measureType == 'DAY_SALARY_MEASURE') {
				$("#scale2Tr").hide();
				$("#measurementDesc").html("团队日量：");
				$("#scaleDesc").html("日工资：");
			} else if(r[1].measureType == 'HALF_MONTH_BONUS_MEASURE') {
				$("#scale2Tr").hide();
				$("#measurementDesc").html("半月团队亏损量：");
				$("#scaleDesc").html("分红比例：");
			} else if(r[1].measureType == 'DAY_ACTIVITY_MEASURE') {
				$("#scale2Tr").hide();
				$("#measurementDesc").html("日投注量：");
				$("#scaleDesc").html("奖励金额：");
			}
			$("#bonusConfigId").val(r[1].id);
			$("#measureType").val(r[1].measureType);
			$("#dailiLevel").val(r[1].dailiLevel);
			$("#measurement").val(r[1].measurement);
			$("#scale").val(r[1].scale);
			$("#scale2").val(r[1].scale2);
			$("#description").val(r[1].description);
		}else if(r[0] != null && r[0] == "error"){
			showErrorDlg(jQuery(document.body), r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "获取分红配置信息失败.");
		}
    });
};