function UserBounsConfigPage(){}
var userBounsConfigPage = new UserBounsConfigPage();

userBounsConfigPage.param = {
   	
};

userBounsConfigPage.treatmentConfigParam = {
	   	
};

$(document).ready(function() {
	var _this = $('.list').find('thead');
    //折叠
    _this.click(function () {
        var i = $(this).find('i');
        if (i.attr('class') == 'tip-down') { i.attr('class', 'tip-up'); } else { i.attr('class', 'tip-down'); }
        $(this).parent().find('tbody').toggle();
    });
    
    userBounsConfigPage.getTreatmentConfig();
	userBounsConfigPage.getAllUserBounsConfigs();
	
	//初始化管理员下拉数据
	managerPayBankAction.initForPayBankPage(function(r){
		if (r[0] != null && r[0] == "ok") {
			var adminnameSelectObj = $(".adminSel");
			adminnameSelectObj.empty();
			adminnameSelectObj.append("<option value=''></option>");
			
			var initAdmins = r[1];
			for(var i = 0 ; i < initAdmins.length; i++){
				var admin = initAdmins[i];
				//只加载财务管理员
				if(admin.state==1){
					adminnameSelectObj.append("<option value='"+admin.id+"'>"+admin.username+"</option>");
				}
			}
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询管理员数据失败.");
		}
    });
	
	//页面初始化展开项
	if(tableId == "1") {
		$("#dayCommissionTable").find('tbody').toggle();
	} else if(tableId == "2") {
		$("#monthSalaryTable").find('tbody').toggle();
	} else if(tableId == "3") {
		$("#daySalaryTable").find('tbody').toggle();
	} else if(tableId == "4") {
		$("#halfMonthBonusTable").find('tbody').toggle();
	}else if(tableId == "5") {
		$("#dayActivityTable").find('tbody').toggle();
	}
	
});


/**
 * 保存待遇配置信息
 */
UserBounsConfigPage.prototype.saveTreatmentConfig = function(){
	userBounsConfigPage.treatmentConfigParam.applyHalfMonthBonusSwitch = $("#applyHalfMonthBonusSwitch").val();
	//userBounsConfigPage.treatmentConfigParam.halfMonthBonus1956 = $("#halfMonthBonus1956").val();
	//userBounsConfigPage.treatmentConfigParam.halfMonthBonus1954 = $("#halfMonthBonus1954").val();
	userBounsConfigPage.treatmentConfigParam.applyUpHalfMonthBonusTime = $("#applyUpHalfMonthBonusTime").val();
	userBounsConfigPage.treatmentConfigParam.applyDownHalfMonthBonusTime = $("#applyDownHalfMonthBonusTime").val();
	
	userBounsConfigPage.treatmentConfigParam.orderAmountJudgeSwitch = $("#orderAmountJudgeSwitch").val();
	userBounsConfigPage.treatmentConfigParam.downDealSwitch = $("#downDealSwitch").val();
	userBounsConfigPage.treatmentConfigParam.reach1958DayNum = $("#reach1958DayNum").val();
	userBounsConfigPage.treatmentConfigParam.reach1956DayNum = $("#reach1956DayNum").val();
	userBounsConfigPage.treatmentConfigParam.userReach1956DayNum = $("#userReach1956DayNum").val();
	userBounsConfigPage.treatmentConfigParam.halfMonthAllowDiff = $("#halfMonthAllowDiff").val();
	userBounsConfigPage.treatmentConfigParam.halfMonthDayNumPercent = $("#halfMonthDayNumPercent").val();
	
	userBounsConfigPage.treatmentConfigParam.applyDayBonusSwitch = $("#applyDayBonusSwitch").val();
	userBounsConfigPage.treatmentConfigParam.dayBonus = $("#dayBonus").val();
	userBounsConfigPage.treatmentConfigParam.applyDayBonusTime = $("#applyDayBonusTime").val();
	userBounsConfigPage.treatmentConfigParam.dayBonusOpenTime = $("#dayBonusOpenTime").val();
	
	userBounsConfigPage.treatmentConfigParam.applyDayCommissionSwitch = $("#applyDayCommissionSwitch").val();
	userBounsConfigPage.treatmentConfigParam.applyDayCommissionTime = $("#applyDayCommissionTime").val();
	
	userBounsConfigPage.treatmentConfigParam.applyMonthSalarySwitch = $("#applyMonthSalarySwitch").val();
	userBounsConfigPage.treatmentConfigParam.applyMonthSalaryTime = $("#applyMonthSalaryTime").val();
	
	userBounsConfigPage.treatmentConfigParam.applyDaySalarySwitch = $("#applyDaySalarySwitch").val();
	userBounsConfigPage.treatmentConfigParam.applyDaySalaryTime = $("#applyDaySalaryTime").val();
	
	userBounsConfigPage.treatmentConfigParam.applyDayActivitySwitch = $("#applyDayActivitySwitch").val();
	userBounsConfigPage.treatmentConfigParam.applyDayActivityTime = $("#applyDayActivityTime").val();
	
	userBounsConfigPage.treatmentConfigParam.dealHalfMonthBonusAdmins = $("#dealHalfMonthBonusAdmins").val();
	userBounsConfigPage.treatmentConfigParam.dealDayBonusAdmins = $("#dealDayBonusAdmins").val();
	userBounsConfigPage.treatmentConfigParam.dealDayCommissionAdmins = $("#dealDayCommissionAdmins").val();
	userBounsConfigPage.treatmentConfigParam.dealMonthSalaryAdmins = $("#dealMonthSalaryAdmins").val();
	userBounsConfigPage.treatmentConfigParam.dealDaySalaryAdmins = $("#dealDaySalaryAdmins").val();
	userBounsConfigPage.treatmentConfigParam.dealDaySalaryAdmins = $("#dealDaySalaryAdmins").val();
	userBounsConfigPage.treatmentConfigParam.dealDayActivityAdmins = $("#dealDayActivityAdmins").val();
	userBounsConfigPage.treatmentConfigParam.dealDayActivityAdminNames = $("#dealDayActivityAdminNames").val();
	
	managerUserBonusConfigAction.updateTreatmentConfigs(userBounsConfigPage.treatmentConfigParam,function(r){
		if (r[0] != null && r[0] == "ok") {
			alert("更新分红配置数据成功");
		}else if (r[0] != null && r[0] == "error") {
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "更新分红配置数据数据失败.");
		}
    });
}

/**
 * 加载待遇配置信息
 */
UserBounsConfigPage.prototype.getTreatmentConfig = function(){
	managerUserBonusConfigAction.getTreatmentConfig(function(r){
		if (r[0] != null && r[0] == "ok") {
			$("#applyHalfMonthBonusSwitch").val(r[1].applyHalfMonthBonusSwitch);
			$("#halfMonthBonus1956").val(r[1].halfMonthBonus1956);
			$("#halfMonthBonus1954").val(r[1].halfMonthBonus1954);
			$("#applyUpHalfMonthBonusTime").val(r[1].applyUpHalfMonthBonusTime);
			$("#applyDownHalfMonthBonusTime").val(r[1].applyDownHalfMonthBonusTime);
			
			$("#orderAmountJudgeSwitch").val(r[1].orderAmountJudgeSwitch);
			$("#downDealSwitch").val(r[1].downDealSwitch);
			$("#reach1958DayNum").val(r[1].reach1958DayNum);
			$("#reach1956DayNum").val(r[1].reach1956DayNum);
			$("#userReach1956DayNum").val(r[1].userReach1956DayNum);
			$("#halfMonthAllowDiff").val(r[1].halfMonthAllowDiff);
			$("#halfMonthDayNumPercent").val(r[1].halfMonthDayNumPercent);
			
			
			$("#applyDayBonusSwitch").val(r[1].applyDayBonusSwitch);
			$("#dayBonus").val(r[1].dayBonus);
			$("#applyDayBonusTime").val(r[1].applyDayBonusTime);
			$("#dayBonusOpenTime").val(r[1].dayBonusOpenTime);
			
			$("#applyDayCommissionSwitch").val(r[1].applyDayCommissionSwitch);
			$("#applyDayCommissionTime").val(r[1].applyDayCommissionTime);
			
			$("#applyMonthSalarySwitch").val(r[1].applyMonthSalarySwitch);
			$("#applyMonthSalaryTime").val(r[1].applyMonthSalaryTime);
			
			$("#applyDaySalarySwitch").val(r[1].applyDaySalarySwitch);
			$("#applyDaySalaryTime").val(r[1].applyDaySalaryTime);
			
			$("#applyDayActivitySwitch").val(r[1].applyDayActivitySwitch);
			$("#applyDayActivityTime").val(r[1].applyDayActivityTime);
			
			$("#dealHalfMonthBonusAdmins").val(r[1].dealHalfMonthBonusAdmins);
			$("#dealDayBonusAdmins").val(r[1].dealDayBonusAdmins);
			$("#dealDayCommissionAdmins").val(r[1].dealDayCommissionAdmins);
			$("#dealMonthSalaryAdmins").val(r[1].dealMonthSalaryAdmins);
			$("#dealDaySalaryAdmins").val(r[1].dealDaySalaryAdmins);
			
			$("#dealHalfMonthBonusAdminNames").val(r[1].dealHalfMonthBonusAdminNames);
			$("#dealDayBonusAdminNames").val(r[1].dealDayBonusAdminNames);
			$("#dealDayCommissionAdminNames").val(r[1].dealDayCommissionAdminNames);
			$("#dealMonthSalaryAdminNames").val(r[1].dealMonthSalaryAdminNames);
			$("#dealDaySalaryAdminNames").val(r[1].dealDaySalaryAdminNames);
			
		}else if (r[0] != null && r[0] == "error") {
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询待遇配置数据数据失败.");
		}
    });
}

/**
 * 加载所有的分红配置
 */
UserBounsConfigPage.prototype.getAllUserBounsConfigs = function(){
	managerUserBonusConfigAction.getAllUserBonusConfigs(function(r){
		if (r[0] != null && r[0] == "ok") {
			userBounsConfigPage.refreshUserBounsConfigPages(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询分红配置数据失败.");
		}
    });
};

/**
 * 刷新列表数据
 */
UserBounsConfigPage.prototype.refreshUserBounsConfigPages = function(bonusConfigs){
	var bounsConfigHalfMonthBonusListObj = $("#userBounsConfigHalfMonthBonusList");
	var bounsConfigDayCommissionListObj = $("#userBounsConfigDayCommissionList");
	var bounsConfigMonthSalaryListObj = $("#userBounsConfigMonthSalaryList");
	var bounsConfigDaySalaryListObj = $("#userBounsConfigDaySalaryList");
	var bounsConfigDayActivityListObj = $("#userBounsConfigDayActivityList");
	bounsConfigDayCommissionListObj.html("");
	bounsConfigMonthSalaryListObj.html("");
	bounsConfigDaySalaryListObj.html("");
	bounsConfigDayActivityListObj.html("");
	
	var bounsConfigHalfMonthBonusDatas = new Array();
	var bounsConfigDayCommissionDatas = new Array();
	var bounsConfigMonthSalaryDatas = new Array();
	var bounsConfigDaySalaryDatas = new Array();
	var bounsConfigDayActivityDatas = new Array();

	for(var i = 0 ; i < bonusConfigs.length; i++){
		var bonusConfig = bonusConfigs[i];
		if(bonusConfig.measureType == 'HALF_MONTH_BONUS_MEASURE') {
			bounsConfigHalfMonthBonusDatas.push(bonusConfig);
		} else if(bonusConfig.measureType == 'DAY_SALARY_MEASURE') {
			bounsConfigDaySalaryDatas.push(bonusConfig);
		} else if(bonusConfig.measureType == 'DAY_ACTIVITY_MEASURE') {
			bounsConfigDayActivityDatas.push(bonusConfig);
		} else if (bonusConfig.measureType == 'MONTH_SALARY_MEASURE') {
			bounsConfigMonthSalaryDatas.push(bonusConfig);
		} else if (bonusConfig.measureType == 'DAY_COMMISSION_MEASURE') {
			bounsConfigDayCommissionDatas.push(bonusConfig);
		}
	}
	
	var str = "";
	for(var i = 0 ; i < bounsConfigHalfMonthBonusDatas.length; i++){
		var bonusConfig = bounsConfigHalfMonthBonusDatas[i];
		str += "<tr>";
		str += "  <td>"+(i+1)+"</td>";
		str += "  <td>"+ bonusConfig.measureTypeStr +"</td>";
		str += "  <td>"+ bonusConfig.dailiLevelStr +"</td>";
		str += "  <td>"+ bonusConfig.measurement +"</td>";
		str += "  <td>"+ bonusConfig.scale +"</td>";
		str += "  <td><a href='javascript:void(0)' onclick='userBounsConfigPage.editUserBounsConfig("+ bonusConfig.id +")'>编辑</a>&nbsp;|&nbsp;<a href='javascript:void(0)' onclick='userBounsConfigPage.delUserBounsConfig("+ bonusConfig.id +")'>删除</a></td>";
		str += "</tr>";
		
		bounsConfigHalfMonthBonusListObj.append(str);
		str = "";
	}
	for(var i = 0 ; i < bounsConfigDayCommissionDatas.length; i++){
		var bonusConfig = bounsConfigDayCommissionDatas[i];
		str += "<tr>";
		str += "  <td>"+(i+1)+"</td>";
		str += "  <td>"+ bonusConfig.measureTypeStr +"</td>";
		str += "  <td>"+ bonusConfig.measurement +"</td>";
		str += "  <td>"+ bonusConfig.scale +"</td>";
		str += "  <td>"+ bonusConfig.scale2 +"</td>";
		str += "  <td><a href='javascript:void(0)' onclick='userBounsConfigPage.editUserBounsConfig("+ bonusConfig.id +")'>编辑</a>&nbsp;|&nbsp;<a href='javascript:void(0)' onclick='userBounsConfigPage.delUserBounsConfig("+ bonusConfig.id +")'>删除</a></td>";
		str += "</tr>";
		
		bounsConfigDayCommissionListObj.append(str);
		str = "";
	}
	for(var i = 0 ; i < bounsConfigMonthSalaryDatas.length; i++){
		var bonusConfig = bounsConfigMonthSalaryDatas[i];
		str += "<tr>";
		str += "  <td>"+(i+1)+"</td>";
		str += "  <td>"+ bonusConfig.measureTypeStr +"</td>";
		str += "  <td>"+ bonusConfig.dailiLevelStr +"</td>";
		str += "  <td>"+ bonusConfig.measurement +"</td>";
		str += "  <td>"+ bonusConfig.scale +"</td>";
		str += "  <td><a href='javascript:void(0)' onclick='userBounsConfigPage.editUserBounsConfig("+ bonusConfig.id +")'>编辑</a>&nbsp;|&nbsp;<a href='javascript:void(0)' onclick='userBounsConfigPage.delUserBounsConfig("+ bonusConfig.id +")'>删除</a></td>";
		str += "</tr>";
		
		bounsConfigMonthSalaryListObj.append(str);
		str = "";
	}
	for(var i = 0 ; i < bounsConfigDaySalaryDatas.length; i++){
		var bonusConfig = bounsConfigDaySalaryDatas[i];
		str += "<tr>";
		str += "  <td>"+(i+1)+"</td>";
		str += "  <td>"+ bonusConfig.measureTypeStr +"</td>";
		str += "  <td>"+ bonusConfig.dailiLevelStr +"</td>";
		str += "  <td>"+ bonusConfig.measurement +"</td>";
		str += "  <td>"+ bonusConfig.scale +"</td>";
		str += "  <td><a href='javascript:void(0)' onclick='userBounsConfigPage.editUserBounsConfig("+ bonusConfig.id +")'>编辑</a>&nbsp;|&nbsp;<a href='javascript:void(0)' onclick='userBounsConfigPage.delUserBounsConfig("+ bonusConfig.id +")'>删除</a></td>";
		str += "</tr>";
		
		bounsConfigDaySalaryListObj.append(str);
		str = "";
	}
	for(var i = 0 ; i < bounsConfigDayActivityDatas.length; i++){
		var bonusConfig = bounsConfigDayActivityDatas[i];
		str += "<tr>";
		str += "  <td>"+(i+1)+"</td>";
		str += "  <td>"+ bonusConfig.measureTypeStr +"</td>";
		str += "  <td>"+ bonusConfig.measurement +"</td>";
		str += "  <td>"+ bonusConfig.scale +"</td>";
		str += "  <td><a href='javascript:void(0)' onclick='userBounsConfigPage.editUserBounsConfig("+ bonusConfig.id +")'>编辑</a>&nbsp;|&nbsp;<a href='javascript:void(0)' onclick='userBounsConfigPage.delUserBounsConfig("+ bonusConfig.id +")'>删除</a></td>";
		str += "</tr>";
		
		bounsConfigDayActivityListObj.append(str);
		str = "";
	}
};

/**
 * 删除分红配置
 */
UserBounsConfigPage.prototype.delUserBounsConfig = function(id){
	if (confirm("确认要删除？")){
		managerUserBonusConfigAction.delUserBonusConfig(id,function(r){
			if (r[0] != null && r[0] == "ok") {
				userBounsConfigPage.getAllUserBounsConfigs();
			}else if(r[0] != null && r[0] == "error"){
				showErrorDlg(jQuery(document.body), r[1]);
			}else{
				showErrorDlg(jQuery(document.body), "删除分红配置失败.");
			}
	    });
	}
};

/**
 * 编辑分红配置
 */
UserBounsConfigPage.prototype.editUserBounsConfig = function(id){
	self.location = contextPath + "/managerzaizst/user_bonus_config/" + id+"/user_bonus_config_edit.html";
};

/**
 * 保存处理管理员
 */
UserBounsConfigPage.prototype.saveDealAdmin = function(inputId){
	var addId = $("#" + inputId + "Sel").val();
	var addText = $("#" + inputId + "Sel").find("option:selected").text();
	var adminIds = $("#" + inputId + "Admins").val();
	var adminNames = $("#" + inputId + "AdminNames").val();
	if(isNotEmpty(adminIds)) {
		var adminIdArray = adminIds.split(",");
		for(var i = 0; i < adminIdArray.length; i++) {
			if(addId == adminIdArray[i]) {
				alert("不能重复添加");
				return;
			}
		}
		adminIds += "," + addId;
		adminNames += "," + addText;
	} else {
		adminIds = addId;
		adminNames = addText;
	}
	$("#" + inputId + "Admins").val(adminIds);
	$("#" + inputId + "AdminNames").val(adminNames);
}

/**
 * 删除处理管理员
 */
UserBounsConfigPage.prototype.delDealAdmin = function(inputId){
	var adminIds = $("#" + inputId + "Admins").val();
	var adminNames = $("#" + inputId + "AdminNames").val();
	if(isNotEmpty(adminIds)) {
		if(adminIds.lastIndexOf(",") != -1) {
			adminIds = adminIds.substring(0, adminIds.lastIndexOf(","));
			adminNames = adminNames.substring(0, adminNames.lastIndexOf(","));
		} else {
			adminIds = "";
			adminNames = "";
		}

		$("#" + inputId + "Admins").val(adminIds);
		$("#" + inputId + "AdminNames").val(adminNames);
	} 
}


