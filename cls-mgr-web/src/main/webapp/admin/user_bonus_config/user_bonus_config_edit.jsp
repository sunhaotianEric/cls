<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.enums.EMeasureType,com.team.lottery.enums.EUserDailLiLevel" %>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<html>
<head>
    <meta charset="utf-8" />
    <title>新增分红设置</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/user_bonus_config/js/user_bonus_config_edit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerUserBonusConfigAction.js'></script>
    <%
    	String id = request.getParameter("id");  //获取查询ID
    %>
	<script type="text/javascript">
		userBonusConfigEditPage.param.id = <%=id%>;
	</script>
</head>
<body>
   <div class="alert alert-info">当前位置<b class="tip"></b>平台待遇设置<b class="tip"></b><span id="addOrEditSpan">新增</span>分红设置</div>
   	
   	<table class="tb">
         <tr>
         	<td> <a href="<%=path%>/managerzaizst/user_bonus_config.html">返回列表</a>
         </tr>
    </table>
    <form action="javascript:void(0)">
	    <table class="tb">
	   		<tr>
	   			<td width="120px">分红类型：<input type="hidden" id="bonusConfigId"/></td>
	   			<td>
	   				<select class="select" name="measureType" id="measureType">
                           <%
                           	EMeasureType[] types = EMeasureType.values();
                             for(EMeasureType type : types){
                           %>
	                          <option value='<%=type.getCode() %>'><%=type.getDescription() %></option>
	                       <%
	                         }
	                       %>
                      </select>
	   			</td>
	   		</tr>
	   		<tr>
	   			<td width="120px">代理类型：</td>
	   			<td>
	   				<select class="select" name="dailiLevel" id="dailiLevel">
	   					<option value="">全部</option>
	                     <option value='SUPERDIRECTAGENCY'>特级直属</option>
                           <%
                           EUserDailLiLevel[] levels = EUserDailLiLevel.values();
	                         for(EUserDailLiLevel level : levels){
	                       %>
	                          <option value='<%=level.getCode() %>'><%=level.getDescription() %></option>
	                       <%
	                         }
	                       %>
	   				</select>
	   			</td>
	   		</tr>
	   		<tr>
	   			<td id="measurementDesc">半月团队亏损量：</td>
	   			<td>
	   			   <input type="text" id="measurement"/>
	   			</td>
	   		</tr>
	   		<tr>
	   			<td id="scaleDesc">分红比例：</td>
	   			<td>
	   			   <input type="text" id="scale"/>
	   			</td>
	   		</tr>
	   		<tr style="display: none" id="scale2Tr">
	   			<td>上上级奖励：</td>
	   			<td>
	   			   <input type="text" id="scale2"/>
	   			</td>
	   		</tr>
	   		<tr>
	   			<td>描述：</td>
	   			<td>
	   			   <input type="text" id="description" size="50"/>
	   			</td>
	   		</tr>
	   		<tr>
	   			<td></td>
	   			<td><input type="button" onclick="userBonusConfigEditPage.saveData()" value="提交"/>&nbsp;&nbsp;&nbsp;&nbsp;<input type="reset" value="重置"/></td>
	   		</tr>
	    </table>
    </form>
</body>
</html>

