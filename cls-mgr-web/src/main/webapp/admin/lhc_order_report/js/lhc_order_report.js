function LhcOrderReportPage(){}
var lhcOrderReportPage = new LhcOrderReportPage();

/**
 * 查询参数
 */
lhcOrderReportPage.queryParam = {
	bizSystem : null,
	expect : null,
	lotteryTypeProtype : null
};

//玩法map数据
var lotteryTypeProtypeMap = {};
//玩法组别数组
var lotteryNameIndex=[
      ["特A","特B"],
      ["正码A","正码B"],
      ["正一特","正二特","正三特","正四特","正五特","正六特"],
      ["正码1-6"],
      ["三全中","三中二之中二","三中二之中三","二全中","二中特之中特","二中特之中二","特串","四中一"],
      ["半波"],
      ["一肖/尾数"],
      ["特码生肖"],
      ["二肖","三肖","四肖","五肖","六肖","七肖","八肖","九肖","十肖","十一肖"],
      ["二肖连中","三肖连中","四肖连中","五肖连中","二肖连不中","三肖连不中","四肖连不中"],
      ["二尾连中","三尾连中","四尾连中","二尾连不中","三尾连不中","四尾连不中"],
      ["五不中","六不中","七不中","八不中","九不中","十不中","十一不中","十二不中"],
];

$(document).ready(function() {
	//初始化map数据
//	$("#lotteryNameId option").each(function(){
//		var val=$(this).val();
//		var text = $(this).text();
//		lotteryTypeProtypeMap[text]=val;
//	});
//	
//	//添加玩法类别事件
//	$("#lotteryNameId").change(function(){
//		var selectIndex = $(this).prop('selectedIndex');
//		var lotteryTypeArrays = lotteryNameIndex[selectIndex];
//		//清空原来下拉框内容
//		$("#lotteryTypeProtypeId").empty();
//		//添加下拉选项
//		for(var i=0; i < lotteryTypeArrays.length; i++){
//			var optionText = lotteryTypeArrays[i];
//			var optionValue = lotteryTypeProtypeMap[optionText];
//			$("#lotteryTypeProtypeId").append("<option value='"+ optionValue +"'>"+ optionText +"</option>"); 
//		}
//	});
	
	//选中特码
//	$("#lotteryNameId").find("option[value='TM']").attr("selected","selected");
	
	// 点击底色变亮
	$('.table-block .table tbody').on('click','tr td',function(){
		$('.table-block table tr').removeClass('on');
		$(this).parent('tr').addClass('on');
	})
});


LhcOrderReportPage.prototype.initTableHTML = function(codeReport, allReport) {
	var readOnly = "readonly='readonly'";
	var lhTable1 = $("#lhTable1");
	var lhTable2 = $("#lhTable2");
	var lhTable3 = $("#lhTable3");
	lhTable1.html("");
	lhTable2.html("");
	lhTable3.html("");
	for(var i =0; i < codeReport.length; i = i+3){
		var tr1 = "<tr>";
		var tr2 = "<tr>";
		var tr3 = "<tr>";
		k=i;
		tr1+= "<td><input id='code"+k+"' type='text' value='"+codeReport[k].codeDes+"' "+readOnly+" style='background-color: #b5ecc4;'/></td>";
		tr1+= "<td><input id='codeDes"+k+"' type='text' value='"+codeReport[k].lotteryMoney+"' "+readOnly+" /></td>";
		if(codeReport[k].winMoney > 0) {
			tr1+= "<td><input id='winMoney"+k+"' type='text' value='"+codeReport[k].winMoney+"' "+readOnly+" style='color:red'/></td>";
		} else {
			tr1+= "<td><input id='winMoney"+k+"' type='text' value='"+codeReport[k].winMoney+"' "+readOnly+"/></td>";			
		}
		tr1+= "<td><input id='returnPercent"+k+"' type='text' value='"+codeReport[k].num+"' "+readOnly+"/></td></tr>";
		
		k=i+1;
		if(k<codeReport.length){
			tr2+= "<td><input id='code"+k+"' type='text' value='"+codeReport[k].codeDes+"' "+readOnly+" style='background-color: #b5ecc4;'/></td>";
			tr2+= "<td><input id='codeDes"+k+"' type='text' value='"+codeReport[k].lotteryMoney+"' "+readOnly+" /></td>";
			if(codeReport[k].winMoney > 0) {
				tr2+= "<td><input id='winMoney"+k+"' type='text' value='"+codeReport[k].winMoney+"' "+readOnly+" style='color:red'/></td>";
			} else {
				tr2+= "<td><input id='winMoney"+k+"' type='text' value='"+codeReport[k].winMoney+"' "+readOnly+"/></td>";			
			}
			tr2+= "<td><input id='returnPercent"+k+"' type='text' value='"+codeReport[k].num+"' "+readOnly+"/></td></tr>";
		}
		k=i+2;
        if(k<codeReport.length){
        	tr3+= "<td><input id='code"+k+"' type='text' value='"+codeReport[k].codeDes+"' "+readOnly+" style='background-color: #b5ecc4;'/></td>";
    		tr3+= "<td><input id='codeDes"+k+"' type='text' value='"+codeReport[k].lotteryMoney+"' "+readOnly+" /></td>";
    		if(codeReport[k].winMoney > 0) {
    			tr3+= "<td><input id='winMoney"+k+"' type='text' value='"+codeReport[k].winMoney+"' "+readOnly+" style='color:red'/></td>";
    		} else {
    			tr3+= "<td><input id='winMoney"+k+"' type='text' value='"+codeReport[k].winMoney+"' "+readOnly+"/></td>";			
    		}
    		tr3+= "<td><input id='returnPercent"+k+"' type='text' value='"+codeReport[k].num+"' "+readOnly+"/></td></tr>";
		} 
        lhTable1.append(tr1);
        lhTable2.append(tr2);
        lhTable3.append(tr3);
	}
	
	$("#allReportInfo").html("下注玩法总览：当前无数据");
	if(allReport != null && allReport.length > 0) {
		var allReportInfo = "下注玩法总览：";
		for(var i =0; i < allReport.length; i++){
			var report = allReport[i];
			var info = "玩法[<span style='color:#3333ca;cursor:pointer' data-val='"+ report.kindPlay +"' onClick='lhcOrderReportPage.changeKindPlay(\""+ report.kindPlay +"\")'>"+ report.kindPlayDes +"</span>]-下注人数["+ report.num +"] &nbsp;&nbsp;";
			allReportInfo += info;
		}
		$("#allReportInfo").html(allReportInfo);
	}

}



/**
 * 条件查询报表
 */
LhcOrderReportPage.prototype.queryReport = function(){
	
	lhcOrderReportPage.queryParam = getFormObj($("#lhcOrderReportForm"));
	//查询条件取玩法的值字段
	//lhcOrderReportPage.queryParam.lotteryTypeProtype = $("#lotteryNameId").val();
	//名称不作为查询条件
	//lhcOrderReportPage.queryParam.lotteryName = null;
	managerOrderAction.queryLhcOrderReport(lhcOrderReportPage.queryParam,function(r){
		if (r[0] != null && r[0] == "ok") {
			lhcOrderReportPage.initTableHTML(r[1], r[2]);
			
			$('.table-block table tr td input').focus(function(){
				$('.table-block table tr').removeClass('on');
				$(this).parents('tr').addClass('on');
			});
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("查询数据失败.");
		}
	});
};

/**
 * 改变玩法查询
 */
LhcOrderReportPage.prototype.changeKindPlay = function(kindPlay){
	//$("#lotteryTypeProtypeId option").removeAttr("selected","selected");
	//$("#lotteryTypeProtypeId").find("option[value='"+ kindPlay+"']").attr("selected","selected");
	$("#lotteryTypeProtypeId").val(kindPlay);
	lhcOrderReportPage.queryReport();
}
