<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="com.team.lottery.system.SystemConfigConstant"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	/* response.setHeader("Access-Control-Allow-Origin", "*");  
	response.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE,SUBMIT");  
	response.setHeader("Access-Control-Max-Age", "3600");  
	response.setHeader("Access-Control-Allow-Headers", "x-requested-with, Content-Type");  
	response.setHeader("Access-Control-Allow-Credentials", "true");  */
	String path = request.getContextPath();
	/* StringBuffer url = request.getRequestURL();
	String domainUrl = url.toString().replaceAll(request.getRequestURI(), "") + path;
	
	String staticImageServerUrl = "http://" + SystemConfigConstant.picHost; */
%>
<html>
<head>
<meta charset="utf-8" />
<title>聊天室权限管理编辑</title>
<jsp:include page="/admin/include/include.jsp"></jsp:include>
<link rel="stylesheet"
	href="<%=path%>/resources/kindeditor-4.1.10/themes/default/default.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" />
<script charset="utf-8"
	src="<%=path%>/resources/kindeditor-4.1.10/kindeditor-min.js"></script>
<script charset="utf-8"
	src="<%=path%>/resources/kindeditor-4.1.10/lang/zh_CN.js"></script>
<%
	String id = request.getParameter("id"); //获取查询的商品ID
%>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeIn">
		<form action="javascript:void(0)">
			<div class="row">
				<c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
					<div class="col-sm-12">
						<div class="input-group m-b" draggable="false">
							<span class="input-group-addon">商户：</span>
							<cls:bizSel name="bizSystem" id="bizSystem"
								options="class:ipt form-control" />

						</div>
					</div>
				</c:if>
				<div class="col-sm-12">
				<input type="hidden" id="chatRoleUserId">
					<div class="input-group m-b">
						<span class="input-group-addon">用户名：</span> <input type="text"
							value="" class="form-control" name="userName" id="userName"><input
							type="hidden" id="userName" readonly="readonly">
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">角色：</span> <select
							class="form-control" id="role">
							<option value="GROUPOWNER">群主</option>
							<option value="MANAGER">管理员</option>
							<option value="ORDINARYUSERS">普通用户</option>
							<option value="CUSTOMER">客服号</option>
							<option value="TUTOR">导师</option>
						</select>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">房间ID：</span> <input type="text"
							value="" class="form-control" name="roomId" id="roomId"><input
							type="hidden" id="roomId" readonly="readonly">
					</div>
				</div>
				<div class="col-sm-12">
					<div class="row">
						<div class="col-sm-6 text-center">
							<button type="button" class="btn btn-w-m btn-white"
								onclick="editChatRoleUserPage.saveData()">提 交</button>
						</div>
						<div class="col-sm-6 text-center">
							<input type="reset" class="btn btn-w-m btn-white" value="重 置" />
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<!-- js -->
	<script type="text/javascript"
		src="<%=path%>/admin/chatroleuser/js/editchatroleuser.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
	<!-- dwr -->
	<script type='text/javascript'
		src='<%=path%>/dwr/interface/manageChatRoleUserAction.js'></script>
	<script type="text/javascript">
	editChatRoleUserPage.param.id =
	<%=id%>
		;
	</script>
</body>
</html>

