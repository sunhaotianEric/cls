function EditChatRoleUserPage(){}
var editChatRoleUserPage = new EditChatRoleUserPage();

editChatRoleUserPage.param = {
   	
};

$(document).ready(function() {
	var chatRoleUserId = editChatRoleUserPage.param.id;  //获取查询的帮助ID
	if(chatRoleUserId != null){
		editChatRoleUserPage.editChatRoleUser(chatRoleUserId);
	}
});


/**
 * 保存帮助信息
 */
EditChatRoleUserPage.prototype.saveData = function(){
	var id = $("#chatRoleUserId").val();
	var bizSystem = $("#bizSystem").val();
	var userName = $("#userName").val();
	var roomId = $("#roomId").val();
	var role = $("#role").val();
	
	if(currentUser.bizSystem == "SUPER_SYSTEM"){
		if(bizSystem==null || bizSystem.trim()==""){
			swal("请选择业务系统!");
			$("#bizSystem").focus();
			return;
		}
	}else{
		bizSystem = currentUser.bizSystem;
	}
	
	if(userName==null || userName.trim()==""){
		swal("请输入用户名!");
		$("#userName").focus();
		return;
	}
	if(roomId==null || roomId.trim()==""){
		swal("请输入房间ID!");
		$("#roomId").focus();
		return;
	}
	if(role==null || role.trim()==""){
		swal("请选择角色!");
		$("#role").focus();
		return;
	}
	
	var chatRoleUser = {};
	chatRoleUser.id = id;
	chatRoleUser.bizSystem = bizSystem;
	chatRoleUser.userName = userName;
	chatRoleUser.roomId = roomId;
	chatRoleUser.role = role;
	manageChatRoleUserAction.saveChatRoleUser(chatRoleUser,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({title:"保存成功！",
		        text:"已成功保存数据",
		        type:"success"},
		        function(){
		        	var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
					parent.chatroleuserPage.layerClose(index);
			  }
			)
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("保存帮助信息失败.");
		}
    });
};


/**
 * 赋值帮助信息
 */
EditChatRoleUserPage.prototype.editChatRoleUser = function(id){
	manageChatRoleUserAction.selectChatRoleUser(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			$("#chatRoleUserId").val(r[1].id);
			$("#bizSystem").val(r[1].bizSystem);
			$("#bizSystem").attr("disabled","disabled");
			$("#userName").val(r[1].userName);
			$("#userName").attr("disabled","disabled");
			$("#roomId").val(r[1].roomId);
			$("#roomId").attr("disabled","disabled");
			$("#role").val(r[1].role);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取帮助信息失败.");
		}
    });
};