function ChatRoleUserPage(){}
var chatroleuserPage = new ChatRoleUserPage();

chatroleuserPage.param = {
   	
};
// 分页参数
chatroleuserPage.pageParam = {
		    pageNo : 1,
		    pageSize : defaultPageSize
};
/**
 * 查询参数
 */
chatroleuserPage.queryParam = {
		bizSystem:null
};
$(document).ready(function() {
	chatroleuserPage.initTableData();
});

ChatRoleUserPage.prototype.initTableData = function(){
	chatroleuserPage.queryParam = getFormObj($("#chatroleuserForm"));
	chatroleuserPage.table = $('#chatroleuserTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;// 获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			chatroleuserPage.pageParam.pageSize = data.length;// 页面显示记录条数
			chatroleuserPage.pageParam.pageNo = (data.start / data.length)+1;// 当前页码
	    	manageChatRoleUserAction.getAllChatRoleUserPage(chatroleuserPage.queryParam,chatroleuserPage.pageParam,function(r){
				// 封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
				returnData.recordsTotal = r[1].totalRecNum;// 返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;// 后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;// 返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询所有的数据失败.");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": "id"},
		            {"data": "bizSystemName",
		            	"bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "userName"},
		            {"data": "role"},
		            {"data": "roomId"},
		            {"data": "createDateStr"},
		            {"data": "updateDateStr"},
		            {
	                	 "data": "id",
	                	 render: function(data, type, row, meta) {
	                		 var str=""
	                			 str="<a class='btn btn-info btn-sm btn-bj' onclick='chatroleuserPage.editChatRoleUser("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;"+
	                		 		"<a class='btn btn-danger btn-sm btn-del' onclick='chatroleuserPage.delChatRoleUser("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";
	                	         return str;
	                	 }
	                }
	            ]
	}).api();
};

/**
 * 查询数据
 */
ChatRoleUserPage.prototype.findChatRoleUser = function(){
	var bizSystem = $("#bizSystem").val();
	if(bizSystem == ""){
		chatroleuserPage.queryParam.bizSystem = null;
	}else{
		chatroleuserPage.queryParam.bizSystem = bizSystem;
	}
	chatroleuserPage.table.ajax.reload();
};


/**
 * 删除
 */
ChatRoleUserPage.prototype.delChatRoleUser =function(id){
	  swal({
          title: "您确定要删除这条信息吗",
          text: "请谨慎操作！",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          cancelButtonText: "取消",
          confirmButtonText: "删除",
          closeOnConfirm: false
      },
      function() {
    	  manageChatRoleUserAction.delChatRoleUser(id,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "删除成功",type: "success"});
				chatroleuserPage.findChatRoleUser();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("删除失败.");
			}
	    });
	});
};

/**
 * 编辑
 */
ChatRoleUserPage.prototype.editChatRoleUser=function(id){
	layer.open({
        type: 2,
        title: '聊天室权限管理编辑',
        maxmin: false,
        shadeClose: true,
        //点击遮罩关闭层
       /* area: ['600px', '450px'],*/
        area: ['70%', '80%'],
        content: contextPath + "/managerzaizst/chatroleuser/editchatroleuser.html?id="+id,
        cancel: function(index){ 
        	chatroleuserPage.table.ajax.reload();
     	}
    });
}

/**
 * 新增
 */
ChatRoleUserPage.prototype.addChatRoleUser=function(id){
    layer.open({
           type: 2,
           title: '聊天室权限管理新增',
           maxmin: false,
           shadeClose: true,
           //点击遮罩关闭层
//           area: ['600px', '420px'],
           area: ['70%', '80%'],
           content: contextPath + "/managerzaizst/chatroleuser/chatroleuser_add.html?id="+id,   
           cancel: function(index){
        	   chatroleuserPage.findChatRoleUser();
         }
    });
}

/**
 * 停用，启用
 */
/*ChatRoleUserPage.prototype.enableHelps = function(id,flag){
	var str="";
	   if(flag==0){
		   str="停用";  
	   }else{
		   str="启用";  
	   }
	   swal({
        title: "您确定要"+str+"该条帮助信息？",
        text: "请谨慎操作！",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        cancelButtonText: "取消",
        confirmButtonText: "确定",
        closeOnConfirm: false
    },
    function() {
    	managerHelpsAction.HelpsIsShow(id,flag,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "操作成功！",type: "success"});
				chatroleuserPage.table.ajax.reload();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("操作失败.");
			}
	    });
    });
};*/

ChatRoleUserPage.prototype.layerClose=function(index){
	layer.close(index);
	chatroleuserPage.findChatRoleUser();
}