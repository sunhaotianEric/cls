<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
import = "com.team.lottery.enums.EHelpType,com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();

	StringBuffer  url = request.getRequestURL();
	String domainUrl=url.toString().replaceAll(request.getRequestURI(), "")+path;
	String staticImageServerUrl="http://"+SystemConfigConstant.picHost;
%>
<html>
<head>
    <meta charset="utf-8" />
    <title>网站公告</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/announce/js/editannounce.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerAnnounceAction.js'></script>
    
    <link rel="stylesheet" href="<%=path%>/resources/kindeditor-4.1.10/themes/default/default.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" />
	<script charset="utf-8" src="<%=path%>/resources/kindeditor-4.1.10/kindeditor-min.js"></script>
	<script charset="utf-8" src="<%=path%>/resources/kindeditor-4.1.10/lang/zh_CN.js"></script>
    <script>
    var  helpPage=parent.helpPage;
    var layerindex = parent.layer.getFrameIndex(window.name);
    var olddomain = document.domain;
       
      // alert("domain is:"+document.domain);  
		var editor;  //editor.html()
		//图片服务器地址
		//var staticImageUrl = 'http://www.zzh.com:8080/cls-mgr-web';
		var staticImageUrl='<%=domainUrl%>';
		var url = staticImageUrl.split('//');
		//var staticImageUrl = 'http://www.img.zzh.com';
		//var imgurl =  "http://42.51.191.174";
		var imgurl='<%=staticImageServerUrl%>';
		KindEditor.ready(function(K) {
			//document.domain = 'zzh.com';
			editor = K.create('textarea[name="content"]', {
				uploadJson : location.protocol+'//'+url[1]+'/UploadFileForJsonServlet',
				fileManagerJson :imgurl+'/file_manager_json.jsp',
				allowFileManager : true
			});
		});
	</script>
</head>
<body>
   <div class="alert alert-info">当前位置<b class="tip"></b>网站公告管理<b class="tip"></b>编辑网站公告</div>
   	
   	<table class="tb">
         <tr>
         	<td> <a href="<%=path %>/managerzaizst/announce.html">信息管理</a> &nbsp;&nbsp; <a href="<%=path %>/managerzaizst/announce/announce_add.html">添加信息</a></td>
         </tr>
    </table>
    <form action="javascript:void(0)">
	    <table class="tb">
	    <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
	    <tr>
	   			<td width="15px">商户：</td>
	   			<td><select id="bizSystem" name="bizSystem">
	   			  <option value="">全部</option>
	   			  </select></td>
	   		</tr>
	   		</c:if>
	   		<tr>
	   			<td width="15px">公告标题：<input type="hidden" id="announceId"/></td>
	   			<td><input type="text" id="title" size="50px"/></td>
	   		</tr>
	   		<tr>
	   			<td>公告内容：</td>
	   			<td>
	   			   <textarea name="content" id="content" style="width:800px;height:400px;visibility:hidden;"></textarea>
	   			</td>
	   		</tr>
	   		<tr>
	   			<td></td>
	   			<td><input type="button" onclick="editAnnouncePage.saveData()" value="提交"/>&nbsp;&nbsp;&nbsp;&nbsp;<input type="reset" value="重置"/></td>
	   		</tr>
	    </table>
    </form>
</body>
</html>

