<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>网站公告管理</title>
        <link rel="shortcut icon" href="favicon.ico">
        
        <jsp:include page="/admin/include/include.jsp"></jsp:include>
    </head>
<body>
<div class="animated fadeIn">
       <div class="ibox-content">
           <div class="row">
               <form class="form-horizontal" id="announceForm">
                   <div class="row">
                   
                       <div class="col-sm-4">
                        <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                    <div class="input-group m-b">
                        <span class="input-group-addon">商户：</span>
                         <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
                        
                    </div>
                    </c:if>
                </div>
                <div class="col-sm-8">
                    <div class="form-group  text-right">
                    <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                        <button type="button" class="btn btn-outline btn-default btn-kj" onclick="announcePage.findAnnounces()"><i class="fa fa-search"></i>&nbsp;查 询</button>&nbsp;&nbsp;&nbsp;&nbsp;
                        </c:if>
                        <button type="button" class="btn btn-outline btn-default btn-add" onclick="announcePage.addAnnounces()"><i class="fa fa-plus"></i>&nbsp;新 增</button>
                        </div>
                         
                </div>
                
             </div>
        </form>
        </div>
        <div class="jqGrid_wrapper">
            <div class="ui-jqgrid ">
                <div class="ui-jqgrid-view ">
                    <div class="ui-jqgrid-hdiv">
                        <div class="ui-jqgrid-hbox" style="padding-right:0">
                            <table id="announceTable" class="ui-jqgrid-htable ui-common-table table table-bordered">
                                <thead>
                                    <tr class="ui-jqgrid-labels">
                                        <th class="ui-th-column ui-th-ltr">序号</th>
                                        <th class="ui-th-column ui-th-ltr" >商户</th>
                                        <th class="ui-th-column ui-th-ltr">标题</th>
                                        <th class="ui-th-column ui-th-ltr">发布日期</th>
                                        <th class="ui-th-column ui-th-ltr">截止日期</th>
                                        <th class="ui-th-column ui-th-ltr">是否置顶</th>
                                        <th class="ui-th-column ui-th-ltr">首页展示</th>
                                        <th class="ui-th-column ui-th-ltr">是否过期</th>
                                        <th class="ui-th-column ui-th-ltr">操作</th></tr>
                                </thead>
                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

		    <!-- dwr -->
		    <script type='text/javascript' src='<%=path%>/dwr/interface/managerAnnounceAction.js'></script>
            <!-- js -->
		    <script type="text/javascript" src="<%=path%>/admin/announce/js/announce.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
</body>
</html>

