function AnnouncePage(){}
var announcePage = new AnnouncePage();

announcePage.param = {
   	
};
announcePage.queryParam = {
		bizSystem : null
	
	};
// 分页参数
announcePage.pageParam = {
		    pageNo : 1,
		    pageSize : defaultPageSize
};
$(document).ready(function() {
	announcePage.initTableData(); 
});

/*
 * $(".btn-del").click(function() {
 * 
 * });
 */
/**
 * 加载所有的网站公告
 *//*
	 * AnnouncePage.prototype.getAllAnnounces = function(){
	 * managerAnnounceAction.getAllAnnounces(function(r){ if (r[0] != null &&
	 * r[0] == "ok") { announcePage.refreshAnnouncePages(r[1]); }else if(r[0] !=
	 * null && r[0] == "error"){ alert(r[1]); }else{
	 * showErrorDlg(jQuery(document.body), "查询网页公告数据失败."); } }); };
	 */

AnnouncePage.prototype.initTableData = function(){

	announcePage.queryParam = getFormObj($("#announceForm"));
	announcePage.table = $('#announceTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;// 获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			announcePage.pageParam.pageSize = data.length;// 页面显示记录条数
			announcePage.pageParam.pageNo = (data.start / data.length)+1;// 当前页码
	    	managerAnnounceAction.getAllAnnounce(announcePage.queryParam,announcePage.pageParam,function(r){
				// 封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;// 返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;// 后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;// 返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询奖金期号数据失败.");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": null},
                    {"data": "bizSystemName", 
	                 "bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data":"title",
		            	 render: function(data, type, row, meta) {
		            		 if(data.length > 45){
		            			 var title =  "<span title='"+data+"'>"+data.substring(0,45)+"．．．</span>";
		            			 return title;
		            		 }else{
		            			 return data;
		            		 }
		            	 },
		            },
		            {"data": "logtimeStr"},
		            {"data": "logdateStr"},
		            {
		            	"data": "topping",
		            	render: function(data, type, row, meta) {
		            		if (data == "1") {
		            			return '是';
		            		} else {
		            			return '否';
		            		}
		            		return data;
	            		}
		            },
		            {
		            	"data": "yetshow",
		            	render: function(data, type, row, meta) {
		            		if (data == "1") {
		            			return '是';
		            		} else {
		            			return '否';
		            		}
		            		return data;
	            		}
		            },
		            {
		            	"data": "logdateStr",
		            	render: function(data, type, row, meta) {
		            		let date = new Date()
		            		if (new Date(data).getTime() > date.getTime()) {
		            			return '未过期';
		            		} else {
		            			return '已过期';
		            		}
		            		return data;
	            		}
		            },
		            {
	                	 "data": "id",
	                	 render: function(data, type, row, meta) {
	                		 var str="<a class='btn btn-info btn-sm btn-bj' onclick='announcePage.editAnnounces("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;" +
	             		 		"<a class='btn btn-danger btn-sm btn-del' onclick='announcePage.delAnnounces("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";
	                		 if(row.yetshow=="1")
	                			 {
	                			 str="<a class='btn btn-warning btn-sm ' onclick='announcePage.unshowAnnounce("+data+")'><i class='glyphicon glyphicon-remove' ></i>&nbsp;隐藏显示 </a>&nbsp;&nbsp;"+str;
	                			 }
	                		 else
	                			 {
	                			 str="<a class='btn btn-sm btn-success' onclick='announcePage.showAnnounce("+data+")'><i class='glyphicon glyphicon-ok'></i>&nbsp;启用显示 </a>&nbsp;&nbsp;"+str;
	                			 }
	                		 if(row.topping=="1")
                			 	{
	                			 str="<a class='btn btn-warning btn-sm ' onclick='announcePage.unshowAnnounceTopping("+data+")'><i class='glyphicon glyphicon-remove' ></i>&nbsp;取消置顶 </a>&nbsp;&nbsp;"+str;
                			 	}
	                		 else
                			 	{
	                			 str="<a class='btn btn-sm btn-success' onclick='announcePage.showAnnounceTopping("+data+")'><i class='glyphicon glyphicon-ok'></i>&nbsp;置顶 </a>&nbsp;&nbsp;"+str;
                			 	}	 
	                		 return str ;
	                	 }
	                }
	            ]
	}).api(); 
	$("#tiye").hide();

	/*
	 * announcePage.queryParam = {};
	 * announcePage.refreshAnnouncePages(announcePage.queryParam,announcePage.pageParam.pageNo);
	 */
};

/**
 * 查询数据
 */
AnnouncePage.prototype.findAnnounces = function(){
	var bizSystem = $("#bizSystem").val();
	if(bizSystem == ""){
		announcePage.queryParam.bizSystem = null;
	}else{
		announcePage.queryParam.bizSystem = bizSystem;
	}
	announcePage.table.ajax.reload();
};




/**
 * 删除
 */
AnnouncePage.prototype.delAnnounces = function(id){
	
	   swal({
           title: "您确定要删除这条信息吗",
           text: "请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "删除",
           closeOnConfirm: false
       },
       function() {
    	   managerAnnounceAction.delAnnounce(id,function(r){
   			if (r[0] != null && r[0] == "ok") {
   				swal({ title: "提示", text: "删除成功",type: "success"});
   				announcePage.table.ajax.reload();
   			}else if(r[0] != null && r[0] == "error"){
   				swal(r[1]);
   			}else{
   				swal("删除信息失败.");
   			}
   	    });
       });
};

/**
 * 新增
 */
AnnouncePage.prototype.addAnnounces=function()
{
	
	 layer.open({
         type: 2,
         title: '网站公告新增',
         maxmin: false,
         shadeClose: true,
         // 点击遮罩关闭层
         area: ['70%', '80%'],
         content: contextPath + "/managerzaizst/announce/editannounce.html",
         cancel: function(index){ 
      	   announcePage.table.ajax.reload();
      	   }
     });
}
/**
 * 编辑
 */
AnnouncePage.prototype.editAnnounces=function(id)
{
	
	  layer.open({
           type: 2,
           title: '网站公告编辑',
           maxmin: false,
           shadeClose: true,
           // 点击遮罩关闭层
           area: ['70%', '80%'],
           content: contextPath + "/managerzaizst/announce/"+id+"/editannounce.html",
           cancel: function(index){ 
        	   announcePage.table.ajax.reload();
        	   }
       });
}

AnnouncePage.prototype.closeLayer=function(index){
	layer.close(index);
	announcePage.table.ajax.reload();
}

/**
 * 展示网站公告信息
 */
AnnouncePage.prototype.showAnnounce = function(id){
	managerAnnounceAction.showAnnounce(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({ title: "提示", text: "操作成功",type: "success"});
				announcePage.table.ajax.reload();
		}else if(r[0] != null && r[0] == "error"){
			showErrorDlg(jQuery(document.body), r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "操作失败.");
		}
    });
};

/**
 * 关闭展示网站公告信息
 */
AnnouncePage.prototype.unshowAnnounce = function(id){
	managerAnnounceAction.unshowAnnounce(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({ title: "提示", text: "操作成功",type: "success"});
			announcePage.table.ajax.reload();
		}else if(r[0] != null && r[0] == "error"){
			showErrorDlg(jQuery(document.body), r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "操作失败.");
		}
    });
};

/**
 * 展示网站公告信息
 */
AnnouncePage.prototype.showAnnounceTopping = function(id){
	managerAnnounceAction.showAnnounceTopping(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({ title: "提示", text: "操作成功",type: "success"});
				announcePage.table.ajax.reload();
		}else if(r[0] != null && r[0] == "error"){
			showErrorDlg(jQuery(document.body), r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "操作失败.");
		}
    });
};

/**
 * 关闭展示网站公告信息
 */
AnnouncePage.prototype.unshowAnnounceTopping = function(id){
	managerAnnounceAction.unshowAnnounceTopping(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({ title: "提示", text: "操作成功",type: "success"});
			announcePage.table.ajax.reload();
		}else if(r[0] != null && r[0] == "error"){
			showErrorDlg(jQuery(document.body), r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "操作失败.");
		}
    });
};

