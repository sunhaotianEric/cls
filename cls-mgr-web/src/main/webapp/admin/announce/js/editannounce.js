function EditAnnouncePage(){}
var editAnnouncePage = new EditAnnouncePage();

editAnnouncePage.param = {
   	
};

$(document).ready(function() {
	var announceId = editAnnouncePage.param.id;  //获取查询的公告ID
	if(announceId != null){
		editAnnouncePage.editAnnounce(announceId);
	}
});

$("#showValue").on('click', function() {
	let id = $(this).attr("data-id")
	if (id == 1) {
		$("#aaa").show()
		$("#sss").hide()
		$(".addsss").removeClass("showDays")
		$(".addaaa").addClass("showDays")
		$(this).attr("data-id", '2')
	} else {
		$("#aaa").hide()
		$("#sss").show()
		$(".addsss").addClass("showDays")
		$(".addaaa").removeClass("showDays")
		$(this).attr("data-id", '1')
	}
})

/**
 * 保存网站公告信息
 */
EditAnnouncePage.prototype.saveData = function(){
	var bizSystem = $("#bizSystem").val();
	var showDays = $(".showDays").val();
	var topping = $("#topping").val();
	var id = $("#announceId").val();
	var title = $("#title").val();
	var content = editor.html();
	
	if(topping==null || topping.trim()==""){
		swal("请输入公告是否置顶!");
		$("#showDays").focus();
		return;
	}
	if(showDays==null || showDays.trim()==""){
		swal("请输入公告显示天数!");
		$("#showDays").focus();
		return;
	}
	if(title==null || title.trim()==""){
		swal("请输入公告主题!");
		$("#title").focus();
		return;
	}
	if(content==null || content.trim()==""){
		swal("请输入公告内容!");
		editor.focus();
		return;
	}
	
	var announce = {};
	announce.bizSystem = bizSystem;
	announce.showDays = showDays;
	announce.topping = topping;
	announce.id = id;
	announce.title = title;
	announce.content = content;
	managerAnnounceAction.saveOrUpdateAnnounces(announce,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.announcePage.closeLayer(index);
		   	    }); 
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("保存公告信息失败.");
		}
    });
};


/**
 * 赋值网站公告信息
 */
EditAnnouncePage.prototype.editAnnounce = function(id){
	managerAnnounceAction.getAnnounceById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			$(".showDays").val(r[1].showDays);
			$("#topping").val(r[1].topping);
			$("#announceId").val(r[1].id);
			$("#title").val(r[1].title);
			$("#bizSystem").val(r[1].bizSystem);
			editor.html(r[1].content);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取公告信息失败.");
		}
		if (r[1].showDays > 5) {
			$("#aaa").show()
			$("#sss").hide()
			$(".addsss").removeClass("showDays")
			$(".addaaa").addClass("showDays")
			$(this).attr("data-id", '2')
		} else {
			$("#aaa").hide()
			$("#sss").show()
			$(".addsss").addClass("showDays")
			$(".addaaa").removeClass("showDays")
			$(this).attr("data-id", '1')
		}
    });
};
