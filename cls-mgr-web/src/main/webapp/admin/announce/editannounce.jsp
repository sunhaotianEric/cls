<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
import = "com.team.lottery.enums.EHelpType,com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();

	StringBuffer  url = request.getRequestURL();
	String domainUrl=url.toString().replaceAll(request.getRequestURI(), "")+path;
	String staticImageServerUrl="http://"+SystemConfigConstant.picHost;
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>网站公告</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    
    
    <link rel="stylesheet" href="<%=path%>/resources/kindeditor-4.1.10/themes/default/default.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>"/>
	<script charset="utf-8" src="<%=path%>/resources/kindeditor-4.1.10/kindeditor-min.js"></script>
	<script charset="utf-8" src="<%=path%>/resources/kindeditor-4.1.10/lang/zh_CN.js"></script>
    <script>
    var  helpPage=parent.helpPage;
    var layerindex = parent.layer.getFrameIndex(window.name);
    var olddomain = document.domain;
       
      // alert("domain is:"+document.domain);  
		var editor;  //editor.html()
		//图片服务器地址
		//var staticImageUrl = 'http://www.zzh.com:8080/cls-mgr-web';
		var staticImageUrl='<%=domainUrl%>';
		var url = staticImageUrl.split('//');
		//var staticImageUrl = 'http://www.img.zzh.com';
		//var imgurl =  "http://42.51.191.174";
		var imgurl='<%=staticImageServerUrl%>';
		KindEditor.ready(function(K) {
			//document.domain = 'zzh.com';
			editor = K.create('textarea[name="content"]', {
				uploadJson : location.protocol+'//'+url[1]+'/UploadFileForJsonServlet',
				fileManagerJson :imgurl+'/file_manager_json.jsp',
				allowFileManager : true
			});
		});
	</script>
	<%
	 String id = request.getParameter("id");  //获取查询的商品ID
	%>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeIn">
		<form action="javascript:void(0)">
			<div class="row">
				<c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
					<div class="col-sm-12">
						<div class="input-group m-b">
							<span class="input-group-addon">商户：</span>
							<cls:bizSel name="bizSystem" id="bizSystem"
								options="class:ipt form-control" />
						</div>
					</div>
				</c:if>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">公告标题：</span> <input type="text"
							value="" class="form-control" name="title" id="title"><input
							type="hidden" id="announceId" name="id">
					</div>
				</div>
				<div class="col-sm-8">
					<div class="input-group m-b">
						<span class="input-group-addon">展示天数：</span> <select
							class="form-control addsss showDays" id="sss">
							<option value="1">一天</option>
							<option value="3">三天</option>
							<option value="4">四天</option>
							<option value="5">五天</option>
							<option value="-1">永久</option>
						</select>
						<input type="text"  style="display:none" value="" class="form-control addaaa showDays" name="showDays" id="aaa">
					</div>
				</div>
				<div class="col-sm-4 text-center">
					<input type="button" class="btn btn-w-m btn-white" value="自定义" id="showValue" data-id='1' />
				</div>	
				
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">是否置顶：</span> <select
							class="form-control" id="topping">
							<option value="0">取消置顶</option>
							<option value="1">置顶</option>
						</select>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="input-group m-b">
						<span class="input-group-addon">公告内容：</span>
						<textarea name="content" id="content"
							style="width: 800px; height: 400px; visibility: hidden;"></textarea>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="row">
						<div class="col-sm-6 text-center">
							<button type="button" class="btn btn-w-m btn-white"
								onclick="editAnnouncePage.saveData()">提 交</button>
						</div>
						<div class="col-sm-6 text-center">
							<input type="reset" class="btn btn-w-m btn-white" value="重 置" />
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/announce/js/editannounce.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerAnnounceAction.js'></script>
    <script type="text/javascript">
    editAnnouncePage.param.id = <%=id%>;
	</script>
</body>
</html>

