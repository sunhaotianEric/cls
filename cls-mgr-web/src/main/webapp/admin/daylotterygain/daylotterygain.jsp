<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>彩种盈亏报表</title>
<jsp:include page="/admin/include/include.jsp"></jsp:include>

</head>
<body>
<div class="animated fadeIn">
     <div class="row"> 
        <div class="col-sm-12">
            <div class="ibox ">
                <div class="ibox-content">
                    <div class="row m-b-sm m-t-sm">
                        <form class="form-horizontal" id="lotteryDayGainQuery">
                      
                           <div class="row">
                           <c:choose>
                           <c:when test="${admin.bizSystem=='SUPER_SYSTEM'}">
                           		<div class="col-sm-3">
                               
	                                <div class="input-group m-b">
	                                    <span class="input-group-addon">商户：</span>
	                                     <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
	                                </div>
                                
                               	</div>
							    <div class="col-sm-2">
							        <div class="input-group m-b">
							            <span class="input-group-addon">排序</span>
							            <div class="input-daterange input-group" id="datepicker">
							                <select class="ipt form-control" name="gainModel">
							                	<option value="1">盈利</option>
							                	<option value="2">赢率</option>
							                </select>
							            </div>
							        </div>
							    </div>
	                           <div class="col-sm-4">
							        <div class="input-group m-b">
							            <span class="input-group-addon">时间始终</span>
							            <div class="input-daterange input-group" id="datepicker">
							                <input class="form-control layer-date" placeholder="开始日期" onclick="" name="belongDateStart" id="startime">
							                <span class="input-group-addon">到</span>
							                <input class="form-control layer-date" placeholder="结束日期" onclick="" name="belongDateEnd" id="endtime"></div>
							        </div>
							    </div>
							    <div class="col-sm-2">
                                       	<div class="form-group nomargin">
								            <button type="button" class="btn btn-xs btn-info" onclick="dayLotteryGainPage.findLotteryDayGain(1)">今 天</button>
								            <button type="button" class="btn btn-xs btn-danger" onclick="dayLotteryGainPage.findLotteryDayGain(-1)">昨 天</button>
								        	<button type="button" class="btn btn-xs btn-warning" onclick="dayLotteryGainPage.findLotteryDayGain(-7)">最近7天</button><br/>
								        	<button type="button" class="btn btn-xs btn-success" onclick="dayLotteryGainPage.findLotteryDayGain(30)">本 月</button>
								        	<button type="button" class="btn btn-xs btn-primary" onclick="dayLotteryGainPage.findLotteryDayGain(-30)">上 月</button>
								        </div>
                                </div>
                           </c:when>
                           <c:otherwise>
							    <div class="col-sm-2">
							        <div class="input-group m-b">
							            <span class="input-group-addon">排序</span>
							            <div class="input-daterange input-group" id="datepicker">
							                <select class="ipt form-control" name="gainModel">
							                	<option value="1">盈利</option>
							                	<option value="2">赢率</option>
							                </select>
							            </div>
							        </div>
							    </div>
                           		<div class="col-sm-4">
							        <div class="input-group m-b">
							            <span class="input-group-addon">时间始终</span>
							            <div class="input-daterange input-group" id="datepicker">
							                <input class="form-control layer-date" placeholder="开始日期" onclick="" name="belongDateStart" id="startime">
							                <span class="input-group-addon">到</span>
							                <input class="form-control layer-date" placeholder="结束日期" onclick="" name="belongDateEnd" id="endtime"></div>
							        </div>
							    </div>
							    <div class="col-sm-5">
                                       	<div class="form-group nomargin text-left">
								            <button type="button" class="btn btn-xs btn-info" onclick="dayLotteryGainPage.findLotteryDayGain(1)">今 天</button>
								            <button type="button" class="btn btn-xs btn-danger" onclick="dayLotteryGainPage.findLotteryDayGain(-1)">昨 天</button>
								        	<button type="button" class="btn btn-xs btn-warning" onclick="dayLotteryGainPage.findLotteryDayGain(-7)">最近7天</button><br/>
								        	<button type="button" class="btn btn-xs btn-success" onclick="dayLotteryGainPage.findLotteryDayGain(30)">本 月</button>
								        	<button type="button" class="btn btn-xs btn-primary" onclick="dayLotteryGainPage.findLotteryDayGain(-30)">上 月</button>
								        </div>
                                </div>
                           </c:otherwise>
                           
                           </c:choose>
                              
							    <div class="col-sm-1">
							        <div class="form-group nomargin text-right">
							            <button type="button" class="btn btn-outline btn-default" onclick="dayLotteryGainPage.findLotteryDayGain()">
							                <i class="fa fa-search"></i>&nbsp;查 询</button>
							        </div>
							    </div>
                            </div>
                            <div class="row white-bg">
								<div class="col-sm-12">
									<p style="color: red">温馨提醒：彩票赢率报表计算周期是每天的03:00-03:00</p>
								</div>
							</div>
 
                        </form>
                    </div>
                    <div class="jqGrid_wrapper">
                        <div class="ui-jqgrid ">
                            <div class="ui-jqgrid-view">
                                <div class="ui-jqgrid-hdiv">
                                    <div class="ui-jqgrid-hbox" style="padding-right:0">
                                        <table class="ui-jqgrid-htable ui-common-table table table-bordered" id="lotteryDayGainTable">
                                            <thead>
                                                <tr class="ui-jqgrid-labels">
                                                  <th class="ui-th-column ui-th-ltr">序号</th>
										          <th class="ui-th-column ui-th-ltr">商户</th>
										          <th class="ui-th-column ui-th-ltr">彩种类型</th>
										          <th class="ui-th-column ui-th-ltr">投注人数</th>
										          <th class="ui-th-column ui-th-ltr">投注额</th>
										          <th class="ui-th-column ui-th-ltr">中奖金额</th>
										          <th class="ui-th-column ui-th-ltr">盈利</th>
										          <th class="ui-th-column ui-th-ltr">赢率</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                 
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>          
                        </div>
                    </div>
                </div>
            </div>
        </div>
     </div> 
</div>
 <!-- js -->
 <script type="text/javascript" src="<%=path%>/admin/daylotterygain/js/daylotterygain.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
 <!-- dwr -->
 <script type='text/javascript' src='<%=path%>/dwr/interface/managerDayLotteryGainAction.js'></script>
</body>
</html>

