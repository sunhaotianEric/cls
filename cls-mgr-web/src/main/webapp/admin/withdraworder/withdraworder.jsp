<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant"%>
<%
	String path = request.getContextPath();
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
	<title>取现记录</title>
	<jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
<body>
	<div class="animated fadeIn">
		<div class="ibox-content ibox">
			<div class="row m-b-sm m-t-sm">
				<form class="form-horizontal" id="withDrawForm">
					<div class="row">
						<c:choose>
							<c:when test="${admin.bizSystem=='SUPER_SYSTEM'}">
								<div class="col-sm-3">
									<div class="input-group m-b">
										<span class="input-group-addon">商户</span>
										<cls:bizSel name="bizSystem" id="bizSystem"
											options="class:ipt form-control" />
									</div>
								</div>
								<div class="col-sm-3">
									<div class="input-group m-b">
										<span class="input-group-addon">取现状态</span>
										<cls:enmuSel name="withDrawStatus"
											options="class:ipt form-control"
											className="com.team.lottery.enums.EFundWithDrawStatus"
											id="withDrawStatus" />
									</div>
								</div>
								<div class="col-sm-3">
									<div class="input-group m-b">
										<span class="input-group-addon">用户名</span> <input type="text"
											value="" name="userName" id="userName" class="form-control">
									</div>
								</div>
							</c:when>
							<c:otherwise>
								<div class="col-sm-5">
									<div class="input-group m-b">
										<span class="input-group-addon">取现状态</span>
										<cls:enmuSel name="withDrawStatus"
											options="class:ipt form-control"
											className="com.team.lottery.enums.EFundWithDrawStatus"
											id="withDrawStatus" />
									</div>
								</div>
								<div class="col-sm-4">
									<div class="input-group m-b">
										<span class="input-group-addon">用户名</span> <input type="text"
											value="" name="userName" id="userName" class="form-control">
									</div>
								</div>
								<div class="col-sm-3"></div>
							</c:otherwise>
						</c:choose>
						<div class="col-sm-3">
							<div class="input-group m-b">
								<span class="input-group-addon">刷新时间</span> <select
									id="refreshTime" name="refreshTime" class="ipt form-control">
									<option value="5">5秒</option>
									<option value="10">10秒</option>
									<option value="15">15秒</option>
									<option value="20">20秒</option>
								</select>
							</div>
						</div>						
					</div>
					<div class="row">					
						<div class="col-sm-2">
							<div class="input-group m-b">
								<span class="input-group-addon">声音开关</span> <select
									id="WithdrawSoundControl" id="WithdrawSoundControl"
									class="ipt form-control">
									<option value="1">开启</option>
									<option value="0">关闭</option>
								</select>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="input-group m-b">
								<span class="input-group-addon">申请时间</span> <input
									class="form-control layer-date" placeholder="开始时间"
									onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"
									name="createdDateStart" id="createdDateStart"> <span
									class="input-group-addon">到</span> <input
									class="form-control layer-date" placeholder="结束日期"
									onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"
									name="createdDateEnd" id="createdDateEnd">
							</div>
						</div>
						<div class="col-sm-2">
									<div class="input-group m-b">
										<span class="input-group-addon">订单来源</span> <select
											id="fromType" id="fromType"
											class="ipt form-control">
											<option value="">==请选择==</option>
											<option value="PC">PC</option>
											<option value="MOBILE">手机WEB</option>
											<option value="APP">APP</option>
										</select>
									</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group nomargin text-left">
								<button type="button" class="btn btn-xs btn-info"
									onclick="withDrawPage.findWithDraw(1)">今 天</button>
								<button type="button" class="btn btn-xs btn-danger"
									onclick="withDrawPage.findWithDraw(-1)">昨 天</button>
								<button type="button" class="btn btn-xs btn-warning"
									onclick="withDrawPage.findWithDraw(-7)">最近7天</button>
								<br />
								<button type="button" class="btn btn-xs btn-success"
									onclick="withDrawPage.findWithDraw(30)">本 月</button>
								<button type="button" class="btn btn-xs btn-primary"
									onclick="withDrawPage.findWithDraw(-30)">上 月</button>
							</div>
						</div>
						<div class="col-sm-1">
							<div class="form-group nomargin text-right">
								<button type="button" class="btn btn-outline btn-default"
									onclick="withDrawPage.findWithDraw()">
									<i class="fa fa-search"></i>&nbsp;查 询
								</button>
							</div>
						</div>
						<div class="col-sm-1">
							<div class="form-group nomargin text-right">
								<button type="button" class="btn btn-outline btn-default"
									onclick="withDrawPage.withDraworderInfoExport()">
									<i class="fa fa-search"></i>&nbsp;导出数据
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="jqGrid_wrapper">
				<div class="ui-jqgrid ">
					<div class="ui-jqgrid-view ">
						<div class="ui-jqgrid-hdiv">
							<div class="ui-jqgrid-hbox" style="padding-right: 0">
								<table id="withDrawTable"
									class="ui-jqgrid-htable ui-common-table table table-bordered">
									<thead>
										<tr class="ui-jqgrid-labels">
											<th class="ui-th-column ui-th-ltr">商户</th>
											<th class="ui-th-column ui-th-ltr">订单号</th>
											<th class="ui-th-column ui-th-ltr">用户名</th>
											<th class="ui-th-column ui-th-ltr">取现金额</th>
											<th class="ui-th-column ui-th-ltr">手续费</th>
											<th class="ui-th-column ui-th-ltr">到账金额</th>
											<th class="ui-th-column ui-th-ltr">申请时间</th>
											<th class="ui-th-column ui-th-ltr">状态</th>
											<th class="ui-th-column ui-th-ltr">自动出款状态</th>
											<th class="ui-th-column ui-th-ltr">操作</th>
											<th class="ui-th-column ui-th-ltr">订单来源</th>
											<!-- <th class="ui-th-column ui-th-ltr">操作描述</th> -->
											<!-- <th class="ui-th-column ui-th-ltr">附言</th> -->
											<th class="ui-th-column ui-th-ltr">收款姓名</th>
											<th class="ui-th-column ui-th-ltr">银行名称</th>
											<th class="ui-th-column ui-th-ltr">支行名称</th>
											<th class="ui-th-column ui-th-ltr">收款卡号</th>
											<th class="ui-th-column ui-th-ltr">操作人</th>
											<th class="ui-th-column ui-th-ltr">操作时间</th>
											<!-- <th class="ui-th-column ui-th-ltr">关闭原因</th> -->
										</tr>
									</thead>

								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- js -->
	<script type="text/javascript"
		src="<%=path%>/admin/withdraworder/js/withdraworder.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
	<!-- dwr -->
	<script type='text/javascript'
		src='<%=path%>/dwr/interface/managerWithDrawOrderAction.js'></script>
	<script type='text/javascript'
		src='<%=path%>/dwr/interface/managerUserAction.js'></script>
</body>
</html>

