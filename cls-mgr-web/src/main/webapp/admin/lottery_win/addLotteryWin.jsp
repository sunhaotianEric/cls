<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeIn">
 <form class="form-horizontal">
    <div class="row">
        <div class="col-sm-6">
            <div class="input-group m-b">
                <span class="input-group-addon">奖金等级</span>
                <input type="text" value="" class="form-control" id="winLevel"></div>
        </div>
        <div class="col-sm-6">
            <div class="input-group m-b">
                <span class="input-group-addon">奖金名称</span>
                <input type="text" value="" class="form-control" id="winName"></div>
        </div>
        <div class="col-sm-6">
            <div class="input-group m-b">
                <span class="input-group-addon">奖金金额</span>
                <input type="text" value="" class="form-control" id="winMoney"></div>
        </div>
        <div class="col-sm-6">
            <div class="input-group m-b">
                <span class="input-group-addon">中奖位数</span>
                <input type="text" value="" class="form-control"  id="winNum"></div>
        </div>
        <div class="col-sm-6">
            <div class="input-group m-b">
                <span class="input-group-addon">玩法名称</span>
                <input type="text" value="" class="form-control" id="lotteryName"></div>
        </div>
        <div class="col-sm-6">
            <div class="input-group m-b">
                <span class="input-group-addon">递增比例</span>
                <input type="text" value="" class="form-control"  id="specCode"></div>
        </div>
        <div class="col-sm-6">
            <div class="input-group m-b">
                <span class="input-group-addon">开奖号码数</span>
                <input type="text" value="" class="form-control" id="codeNum"></div>
        </div>
        <div class="col-sm-6">
            <div class="input-group m-b">
                <span class="input-group-addon">是否按顺序</span>
                <select class="ipt form-control"  id="isorder">
                    <option value="0">是</option>
                    <option value ="1">否</option>
               </select>
           </div>
        </div>
        <div class="col-sm-6">
            <div class="input-group m-b">
                <span class="input-group-addon">识别码</span>
                <input type="text" value="" class="form-control" id="lotteryTypeProtype"></div>
        </div>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-6 text-center">
                    <button type="button" class="btn btn-w-m btn-white" onclick="editLotteryWinPage.addLotteryWin()">提 交</button>
                </div>
                <div class="col-sm-6 text-center">
                   <input type="reset" class="btn btn-w-m btn-white" value="重 置"/>
            </div>
        </div>
    </div>
</div>
</form>
</div>
<!-- js -->
<script type="text/javascript" src="<%=path%>/admin/lottery_win/js/lottery_winlottery_edit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<script type="text/javascript" src="<%=path%>/admin/lottery_win/js/lottery_win.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerLotteryWinAction.js'></script>
</body>

</html>