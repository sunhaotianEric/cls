<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import = "com.team.lottery.enums.ELotteryTopKind" %>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>彩票奖金管理</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
     <link rel="shortcut icon" href="favicon.ico">
</head>
<body>
<div class="animated fadeIn">
    <div class="ibox-content">
        <div class="row m-b-sm m-t-sm">
            <form class="form-horizontal" id="lotteryWinQuery">
            	<div class="row">
            		<div class="col-sm-3">
                         	<div class="input-group m-b">
                                <span class="input-group-addon">选择彩种</span>
                                <cls:enmuSel name="lotteryName" id="lotteryName" options="class:ipt form-control"  className="com.team.lottery.enums.ELotteryTopKind"/>
                                <!-- <select class="ipt form-control" name="lotteryName">
                                     <option value="" selected="selected">==请选择==</option>
                                     <option value="1">安微快3</option>
                                     <option value="1">新加坡2分彩</option>
                                     <option value="1">福建11选5</option>
                                     <option value="1">广东快乐十分</option>
                                    </select> -->
                            </div>
                    </div>
                    <div class="col-sm-7">
                    </div>
	            	<div class="col-sm-2">
	                   <div class="form-group text-right">
	                       <button type="button" class="btn btn-outline btn-default btn-js" onclick="lotteryWinPage.findLotteryWinConfig()"><i class="fa fa-search"></i>&nbsp;查 询</button>
	                       <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                           		<button type="button" class="btn btn-outline btn-default btn-js" onclick="lotteryWinPage.addLotteryWinHtml()"><i class="fa fa-plus"></i>&nbsp;添 加</button>
                           </c:if>
                        </div>
	                </div>
	            </div>
        </form>
        </div>
        <div class="jqGrid_wrapper">
            <div class="ui-jqgrid ">
                <div class="ui-jqgrid-view">
                    <div class="ui-jqgrid-hdiv">
                        <div class="ui-jqgrid-hbox" style="padding-right:0">
                            <table class="ui-jqgrid-htable ui-common-table table table-bordered" id="lotteryWinConfigTable">
                                <thead>
                                    <tr class="ui-jqgrid-labels">
                                        <th class="ui-th-column ui-th-ltr" >奖金等级</th>
                                        <th class="ui-th-column ui-th-ltr" >奖金名称</th>
                                        <th class="ui-th-column ui-th-ltr" >玩法名称</th>
                                        <th class="ui-th-column ui-th-ltr" >识别码</th>
                                        <th class="ui-th-column ui-th-ltr" >奖金金额</th>
                                        <th class="ui-th-column ui-th-ltr" >递增比例</th>
                                        <th class="ui-th-column ui-th-ltr" >中奖位数</th>
                                        <th class="ui-th-column ui-th-ltr" >开奖号码数</th>
                                        <th class="ui-th-column ui-th-ltr" >是否按顺序</th>
                                        <th class="ui-th-column ui-th-ltr" >操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 <!-- js -->
 <script type="text/javascript" src="<%=path%>/admin/lottery_win/js/lottery_win.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
 <!-- dwr -->
 <script type='text/javascript' src='<%=path%>/dwr/interface/managerLotteryWinAction.js'></script>
</body>
</html>

