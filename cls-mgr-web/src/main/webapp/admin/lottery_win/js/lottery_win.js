function LotteryWinPage(){}
var lotteryWinPage = new LotteryWinPage();

lotteryWinPage.param = {
};

/**
 * 查询参数
 */
lotteryWinPage.queryParam = {
	lotteryType : null,
};

//分页参数
lotteryWinPage.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize
};

$(document).ready(function() {
	lotteryWinPage.initTableData(); //查询所有的奖金配置数据
});

/**
 * 加载所有的奖金配置数据
 */
LotteryWinPage.prototype.initTableData = function(){
	lotteryWinPage.queryParam = getFormObj($("#lotteryWinQuery"));
	lotteryWinPage.table = $('#lotteryWinConfigTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"scrollX": true,
		"iDisplayLength" : defaultPageSize,
		"ajax":function (data, callback, settings) {
			lotteryWinPage.pageParam.pageSize = data.length;//页面显示记录条数
			lotteryWinPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
			managerLotteryWinAction.getAllLotteryWin(lotteryWinPage.queryParam,lotteryWinPage.pageParam, function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询奖金配置数据失败.");
				}
				callback(returnData);
		    });
		},
		"columns": [
		            {"data": "winLevel"},
		            {"data": "winName"},
		            {"data": "lotteryName"},
		            {"data":"lotteryTypeProtype"},
		            {"data": "winMoney"},
		            {"data": "specCode"},
		            {"data": "winNum"},
		            {"data": "codeNum"},
		            {"data": "isorder",
		            render: function(data, type, row, meta) {
		            		if(data==1){
		            			return '是';	
		            		}else{
		            			return '否';
		            		}
	            		}	
		            },
		          
		            {
	                	 "data": "id",
	                	 render: function(data, type, row, meta) {
	                		var str="";
                			 str += "<a class='btn btn-info btn-sm btn-bj' onclick='lotteryWinPage.editWinlottery("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;";
                			 str += "<a class='btn btn-danger btn-sm btn-del' onclick='lotteryWinPage.delLotteryWin("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";
	                		return  str;
	                	 },"bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false
	                }
		            
	            ]
		
	}).api(); 
};

/**
 * 查询数据
 */
LotteryWinPage.prototype.findLotteryWinConfig = function(){
	lotteryWinPage.queryParam.lotteryType = $("#lotteryName").val();
	lotteryWinPage.table.ajax.reload();
};

/**
 * 添加奖金配置数据
 */
LotteryWinPage.prototype.addLotteryWinHtml = function(){
	layer.open({
		type: 2,
		title: '新增彩票开奖处理',
		maxmin: false,
		shadeClose: true,
		//点击遮罩关闭层
		area: ['800px', '400px'],
		content:contextPath + '/managerzaizst/lottery_win/addLotteryWin.html',
	})
}

/**
 * 修改奖金配置数据
 */
LotteryWinPage.prototype.editWinlottery = function(id){
	layer.open({
		type: 2,
		title: '彩票开奖处理编辑',
		maxmin: false,
		shadeClose: true,
		//点击遮罩关闭层
		area: ['800px', '400px'],
		content:contextPath + '/managerzaizst/lottery_win/'+id+'/lottery_winlottery_edit.html',
	})
}

/**
 * 删除奖金配置数据
 */
LotteryWinPage.prototype.delLotteryWin = function(id){
	   swal({
           title: "您确定要删除这条信息吗",
           text: "请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "删除",
           closeOnConfirm: false
       },
       function(){
    	   managerLotteryWinAction.delLotteryWin(id,function(r){
   			if (r[0] != null && r[0] == "ok") {
   				swal({ title: "提示", text: "删除成功",type: "success"});
   				lotteryWinPage.findLotteryWinConfig(); //查询奖金配置数据
   			}else if(r[0] != null && r[0] == "error"){
   				swal(r[1]);
   			}else{
   				swal("删除奖金配置数据失败.");
   			}
   	    });
       })
	
};

LotteryWinPage.prototype.closeLayer=function(index){
	layer.close(index);
	lotteryWinPage.findLotteryWinConfig(); //查询奖金配置数据
}