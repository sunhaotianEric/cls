function EditLotteryWinPage() {
}
var editLotteryWinPage = new EditLotteryWinPage();

editLotteryWinPage.param = {

};

/**
 * 查询参数
 */
editLotteryWinPage.queryParam = {
	lotteryType : null
};


$(document).ready(function() {
	var id = editLotteryWinPage.param.id; 
	if(typeof(id) != "undefined"){
		editLotteryWinPage.getLotteryWin(id); // 查询所有的奖金配置数据
	}
});

/**
 * 赋值配置
 * @param id
 */
EditLotteryWinPage.prototype.getLotteryWin = function(id){
	managerLotteryWinAction.getLotteryWinById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			$("#winLevel").val(r[1].winLevel);
			$("#winName").val(r[1].winName);
			$("#winMoney").val(r[1].winMoney);
			$("#winNum").val(r[1].winNum);
			$("#lotteryName").val(r[1].lotteryName);
			$("#specCode").val(r[1].specCode);
			$("#codeNum").val(r[1].codeNum);
			$("#isorder").val(r[1].isorder);
			$("#lotteryTypeProtype").val(r[1].lotteryTypeProtype);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取奖金配置数据失败.");
		}
    });
}

/**
 * 添加奖金配置数据
 */
EditLotteryWinPage.prototype.addLotteryWin = function(){
	var lotteryWin = {};
	lotteryWin.winLevel = $("#winLevel").val();
	lotteryWin.winName = $("#winName").val();
	lotteryWin.winMoney = $("#winMoney").val();
	lotteryWin.winNum = $("#winNum").val();
	lotteryWin.lotteryName = $("#lotteryName").val();
	lotteryWin.specCode = $("#specCode").val();
	lotteryWin.codeNum = $("#codeNum").val();
	lotteryWin.isorder = $("#isorder").val();
	lotteryWin.lotteryTypeProtype = $("#lotteryTypeProtype").val();
	
	if (lotteryWin.winLevel==null || lotteryWin.winLevel.trim()=="") {
		swal("请输入奖金等级!");
		$("#winLevel").focus();
		return;
	}
	if (lotteryWin.winName==null || lotteryWin.winName.trim()=="") {
		swal("请输入奖金名称!");
		$("#winName").focus();
		return;
	}
	if (lotteryWin.winMoney==null || lotteryWin.winMoney.trim()=="") {
		swal("请输入奖金金额!");
		$("#winMoney").focus();
		return;
	}
	if (lotteryWin.winNum==null || lotteryWin.winNum.trim()=="") {
		swal("请输入中奖位数!");
		$("#winNum").focus();
		return;
	}
	if (lotteryWin.lotteryName==null || lotteryWin.lotteryName.trim()=="") {
		swal("请输入玩法名称!");
		$("#lotteryName").focus();
		return;
	}
	if (lotteryWin.specCode==null || lotteryWin.specCode.trim()=="") {
		swal("请输入递增比例!");
		$("#specCode").focus();
		return;
	}
	if (lotteryWin.codeNum==null || lotteryWin.codeNum.trim()=="") {
		swal("请输入开奖号码数!");
		$("#codeNum").focus();
		return;
	}
	if (lotteryWin.isorder==null || lotteryWin.isorder.trim()=="") {
		swal("请输入是否按顺序!");
		$("#isorder").focus();
		return;
	}
	if (lotteryWin.lotteryTypeProtype==null || lotteryWin.lotteryTypeProtype.trim()=="") {
		swal("请输入识别码!");
		$("#lotteryTypeProtype").focus();
		return;
	}
	
	
	managerLotteryWinAction.addLotteryWin(lotteryWin,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.lotteryWinPage.closeLayer(index);
		   	    });
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("添加奖金配置数据失败.");
		}
    });
};

/**
 * 修改奖金配置数据
 */
EditLotteryWinPage.prototype.editLotteryWin = function(){
	var lotteryWin = {};
	lotteryWin.id = editLotteryWinPage.param.id; 
	lotteryWin.winLevel = $("#winLevel").val();
	lotteryWin.winName = $("#winName").val();
	lotteryWin.winMoney = $("#winMoney").val();
	lotteryWin.winNum = $("#winNum").val();
	lotteryWin.lotteryName = $("#lotteryName").val();
	lotteryWin.specCode = $("#specCode").val();
	lotteryWin.codeNum = $("#codeNum").val();
	lotteryWin.isorder = $("#isorder").val();
	lotteryWin.lotteryTypeProtype = $("#lotteryTypeProtype").val();
	
	if (lotteryWin.id == null){ 
		swal("该记录id为空!");
		return;
	}
	if (lotteryWin.winLevel==null || lotteryWin.winLevel.trim()=="") {
		swal("请输入奖金等级!");
		$("input[name='winLevel']",recordTr).focus();
		return;
	}
	if (lotteryWin.winName==null || lotteryWin.winName.trim()=="") {
		swal("请输入奖金名称!");
		$("input[name='winName']",recordTr).focus();
		return;
	}
	if (lotteryWin.winMoney==null || lotteryWin.winMoney.trim()=="") {
		swal("请输入奖金金额!");
		$("input[name='winMoney']",recordTr).focus();
		return;
	}
	if (lotteryWin.winNum==null || lotteryWin.winNum.trim()=="") {
		swal("请输入中奖位数!");
		$("input[name='winNum']",recordTr).focus();
		return;
	}
	if (lotteryWin.lotteryName==null || lotteryWin.lotteryName.trim()=="") {
		swal("请输入玩法名称!");
		$("input[name='lotteryName']",recordTr).focus();
		return;
	}
	if (lotteryWin.specCode==null || lotteryWin.specCode.trim()=="") {
		swal("请输入递增比例!");
		$("input[name='specCode']",recordTr).focus();
		return;
	}
	if (lotteryWin.codeNum==null || lotteryWin.codeNum.trim()=="") {
		swal("请输入开奖号码数!");
		$("input[name='codeNum']",recordTr).focus();
		return;
	}
	if (lotteryWin.isorder==null || lotteryWin.isorder.trim()=="") {
		swal("请输入是否按顺序!");
		$("input[name='isorder']",recordTr).focus();
		return;
	}
	if (lotteryWin.lotteryTypeProtype==null || lotteryWin.lotteryTypeProtype.trim()=="") {
		swal("请输入识别码!");
		$("input[name='lotteryTypeProtype']",recordTr).focus();
		return;
	}
	managerLotteryWinAction.updateLotteryWin(lotteryWin,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.lotteryWinPage.closeLayer(index);
		   	    });
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("修改奖金配置数据失败.");
		}
    });
};
