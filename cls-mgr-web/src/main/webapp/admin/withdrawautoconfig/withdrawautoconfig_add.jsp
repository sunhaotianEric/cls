 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
 %>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<html>
<head>
    <meta charset="utf-8" />
    <title>自动入款信息编辑</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    
	<%
	 	String id = request.getParameter("id");  // 获取编辑的Id
	%>
</head>
<body class="gray-bg">
  <div class="wrapper wrapper-content animated fadeIn">
  <form action="javascript:void(0)">
    <div class="row">
	<c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
	    <div class="col-sm-12">
           <div class="input-group m-b">
				<span class="input-group-addon">商户：</span>
				<cls:bizSel  name="bizSystem" id="bizSystem" options="class:ipt form-control" />
			</div>
		</div>    
    </c:if>
    <div class="col-sm-12">
		<div class="input-group m-b">
			<span class="input-group-addon">关联方式：</span>
			<select type="radio" class="form-control" name="chargePayId" id="chargePayId"></select>
		</div>
	</div>
	<div class="col-sm-12">
		<div class="input-group m-b">
			<span class="input-group-addon">管理员：</span>
			<select class="form-control" name="dealAdminId" id="dealAdminId"></select><input type="hidden" value="" name="dealAdminUsername" id="dealAdminUsername">
		</div>
	</div>
     <div class="col-sm-12">
    	<div class="input-group m-b">
    		<span class="input-group-addon">最高出款金额：</span>
    		<input type="text" value="" class="form-control" onkeyup="checkNum(this)" name="highestMoney" id="highestMoney">
    	</div>
    </div>
    <div class="col-sm-12">
    	<div class="input-group m-b">
    		<span class="input-group-addon">全自动出款开关：</span>
    		<select class="ipt form-control" id="autoWithdrawSwitch" name="autoWithdrawSwitch">
                            <option value="1">开</option>
                            <option value="0" selected="selected">关</option>
            </select>
    	</div>
    </div>
    <div class="col-sm-12">
    	<div class="input-group m-b">
    		<span class="input-group-addon">启用状态：</span>
    		<select class="ipt form-control" id="enabled" name="enabled">
                            <option value="1" selected="selected">启用</option>
                            <option value="0">停用</option>
            </select>
    	</div>
    </div>
    <div class="col-sm-12">
     <div class="row">
	     <div class="col-sm-6 text-center">
	     	<button type="button" class="btn btn-w-m btn-white" onclick="withdrawAutoConfigEditPage.saveWithdrawAutoConfig()">提 交</button>
	     </div>
     	<div class="col-sm-6 text-center">
      		<input type="reset" class="btn btn-w-m btn-white" value="重 置"/>
        </div>
      </div>
    </div>
    </div>
   
      </form>
  </div>
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/withdrawautoconfig/js/withdrawautoconfig_edit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerWithDrawAutoConfigAction.js'></script>
    <script type="text/javascript">
    		withdrawAutoConfigEditPage.param.id = <%=id%>;
	</script>
</body>
</html>

 