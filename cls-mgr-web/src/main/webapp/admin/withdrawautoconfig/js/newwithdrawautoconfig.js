function NewwithdrawautoconfigPage(){}
var newwithdrawautoconfigPage = new NewwithdrawautoconfigPage();

newwithdrawautoconfigPage.param = {
   	
};


$(document).ready(function() {
	newwithdrawautoconfigPage.editnewwithdrawautoconfigInfo();
	$("#dealAdminId").change(function() {
		$("#dealAdminUsername").val("");
		var text=$("#dealAdminId").find("option:selected").text();
		$("#dealAdminUsername").val(text.substring(0,text.length));
		
	});
	
});


/**
 * 保存更新信息
 */
NewwithdrawautoconfigPage.prototype.saveData = function(){
	
	var newwithdrawautoconfig=getFormObj("#newwithdrawautoconfigForm");
	newwithdrawautoconfig.bizSystem=newwithdrawautoconfigPage.param.bizSystem;
	
	if(newwithdrawautoconfig.bizSystemName==null || newwithdrawautoconfig.bizSystemName.trim()==""){
		swal("请输入业务系统名称!");
		$("#bizSystemName").focus();
		return;
	 }
	
	if(newwithdrawautoconfig.chargePayId==null || newwithdrawautoconfig.chargePayId.trim()==""){
		swal("请输入关联方式!");
		$("#chargePayId").focus();
		return;
	 }
	
	if(newwithdrawautoconfig.dealAdminId==null || newwithdrawautoconfig.dealAdminId.trim()==""){
		swal("请输入管理员!");
		$("#dealAdminId").focus();
		return;
	 }
	
	if(newwithdrawautoconfig.highestMoney==null || newwithdrawautoconfig.highestMoney.trim()==""){
		swal("请输入最高出款金额!");
		$("#highestMoney").focus();
		return;
	 }
	
	if(newwithdrawautoconfig.autoWithdrawSwitch==null || newwithdrawautoconfig.autoWithdrawSwitch.trim()==""){
		swal("请输入开关!");
		$("#autoWithdrawSwitch").focus();
		return;
	 }
	
	if(newwithdrawautoconfig.enabled==null || newwithdrawautoconfig.enabled.trim()==""){
		swal("请输入是否启用!");
		$("#enabled").focus();
		return;
	}

	managerWithDrawAutoConfigAction.saveOrUpdateWithdrawAutoConfig(newwithdrawautoconfig,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   newwithdrawautoconfigPage.editnewwithdrawautoconfigInfo();
		   	    }); 
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("保存信息失败.");
		}
     });
};


/**
 * 赋值信息
 */
NewwithdrawautoconfigPage.prototype.editnewwithdrawautoconfigInfo = function(){
	managerWithDrawAutoConfigAction.getWithdrawAutoConfig(function(r){
		if (r[0] != null && r[0] == "ok") {
			if (r[1].id == null) {
				newwithdrawautoconfigPage.param.bizSystem=r[1];
				$("#bizSystemName").val(r[1]);
				$("#chargePayId").val(r[1].chargePayId);
				newwithdrawautoconfigPage.getThirdPayType(r[1],r[1].chargePayId);
				$("#dealAdminId").val(r[1].dealAdminId);
				newwithdrawautoconfigPage.getDealadminType(r[1],r[1].dealAdminId);
				if(currentUser.bizSystem=='SUPER_SYSTEM')
				{
					$("#bizSystem").val(r[1].bizSystem);
				}
			}else {
				newwithdrawautoconfigPage.param.bizSystem=r[1].bizSystem;
				$("#id").val(r[1].id);
				$("#bizSystemName").val(r[1].bizSystemName);
				$("#chargePayId").val(r[1].chargePayId);
				newwithdrawautoconfigPage.getThirdPayType(r[1].bizSystem,r[1].chargePayId);
				$("#dealAdminId").val(r[1].dealAdminId);
				$("#dealAdminUsername").val(r[1].dealAdminUsername);
				newwithdrawautoconfigPage.getDealadminType(r[1].bizSystem,r[1].dealAdminId);
				$("#highestMoney").val(r[1].highestMoney);
				$("#autoWithdrawSwitch").val(r[1].autoWithdrawSwitch);
				$("#enabled").val(r[1].enabled);
				
				if(currentUser.bizSystem=='SUPER_SYSTEM')
				{
					$("#bizSystem").val(r[1].bizSystem);
				}
			}
			
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取信息失败.");
		}
    });
};


/**
 * 获取第三方所有支付配置.
 */
NewwithdrawautoconfigPage.prototype.getThirdPayType = function(bizSystem,chargePayId) {
	var id = newwithdrawautoconfigPage.param.id
	managerWithDrawAutoConfigAction.getAllRechargePayByBizSystem(bizSystem, function(r) {
		if (r[0] != null && r[0] == "ok") {
			var domains = r[1];
			var domainListObj = $("#chargePayId");
			domainListObj.html("<option value=''>全部</option>");
			var str = "";
			for (var i = 0; i < domains.length; i++) {
				var domain = domains[i];
				if (chargePayId != null && domain.id == chargePayId) {
					str += "<option selected='selected' value='" + domain.id + "'>" + domain.chargeDes + "[" + domain.memberId + "]</option>";
				} else {
					str += "<option value='" + domain.id + "'>" + domain.chargeDes + "[" + domain.memberId + "]</option>";
				}
				domainListObj.append(str);
				str = "";
			}
		} else if (r[0] != null && r[0] == "error") {
			swal(r[1]);
		} else {
			swal("添加数据失败.");
		}
	});
};

/**
 * 获取管理员配置.
 */
NewwithdrawautoconfigPage.prototype.getDealadminType = function(bizSystem,dealAdminId) {
	var paraId = newwithdrawautoconfigPage.param.id
	managerWithDrawAutoConfigAction.getAllDealadminByBizSystem(bizSystem, function(r) {
		if (r[0] != null && r[0] == "ok") {
			var domains = r[1];
			var domainListObj = $("#dealAdminId");
			domainListObj.html("<option value=''>全部</option>");
			var str = "";
			for (var i = 0; i < domains.length; i++) {
				var domain = domains[i];
				if (dealAdminId != null && domain.id == dealAdminId) {
					str += "<option selected='selected' value='" + domain.id + "'>" + domain.username + "</option>";
				} else {
					str += "<option value='" + domain.id + "'>" + domain.username + "</option>";
				}
				domainListObj.append(str);
				str = "";
			}
		} else if (r[0] != null && r[0] == "error") {
			swal(r[1]);
		} else {
			swal("添加数据失败.");
		}
	});
};

