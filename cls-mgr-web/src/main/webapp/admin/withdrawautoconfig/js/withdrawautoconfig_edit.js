function WithdrawAutoConfigEditPage() {
}
var withdrawAutoConfigEditPage = new WithdrawAutoConfigEditPage();

withdrawAutoConfigEditPage.param = {

};

$(document).ready(function() {
	var withdrawAutoConfigId = withdrawAutoConfigEditPage.param.id; // 获取查询的ID
	if (withdrawAutoConfigId != null) {
		$("#bizSystem").attr("disabled", true)
		$("#bizSystem").attr("thirdPayType", true)
		withdrawAutoConfigEditPage.editWithdrawAutoConfig(withdrawAutoConfigId);
	} else {
		withdrawAutoConfigEditPage.addWithdrawAutoConfig();
		$("#bizSystem").change(function(e) {
			var bizSystem = $(this).val();
			withdrawAutoConfigEditPage.getThirdPayType(bizSystem);
			withdrawAutoConfigEditPage.getDealadminType(bizSystem);
		});
	}
	
	$("#dealAdminId").change(function() {
		$("#dealAdminUsername").val("");
		var text=$("#dealAdminId").find("option:selected").text();
		$("#dealAdminUsername").val(text.substring(0,text.length));
		
	});

});

WithdrawAutoConfigEditPage.prototype.addWithdrawAutoConfig = function() {
	withdrawAutoConfigEditPage.getThirdPayType(null);
	withdrawAutoConfigEditPage.getDealadminType(null);
};

/**
 * 编辑时候赋值自动出款相关配置
 */
WithdrawAutoConfigEditPage.prototype.editWithdrawAutoConfig = function(id) {
	managerWithDrawAutoConfigAction.getWithdrawAutoConfigById(id, function(r) {
		if (r[0] != null && r[0] == "ok") {
			var withdrawAutoConfig = r[1];
			$("#id").val(withdrawAutoConfig.id);
			$("#bizSystem").val(withdrawAutoConfig.bizSystem);
			withdrawAutoConfigEditPage.getThirdPayType(withdrawAutoConfig.bizSystem,withdrawAutoConfig.chargePayId);
			withdrawAutoConfigEditPage.getDealadminType(withdrawAutoConfig.bizSystem,withdrawAutoConfig.dealAdminId);
			$("#dealAdminUsername").val(withdrawAutoConfig.dealAdminUsername);
			$("#thirdPayType").val(withdrawAutoConfig.thirdPayType);
			$("#highestMoney").val(withdrawAutoConfig.highestMoney);
			$("#autoWithdrawSwitch").val(withdrawAutoConfig.autoWithdrawSwitch);
			$("#enabled").val(withdrawAutoConfig.enabled);
			$(":radio").attr("disabled", true);
		} else if (r[0] != null && r[0] == "error") {
			swal(r[1]);
		} else {
			swal("获取充值配置失败.");
		}
	});
}

/**
 * 获取第三方所有支付配置.
 */
WithdrawAutoConfigEditPage.prototype.getThirdPayType = function(bizSystem,chargePayId) {
	var paraId = withdrawAutoConfigEditPage.param.id
	managerWithDrawAutoConfigAction.getAllRechargePayByBizSystem(bizSystem, function(r) {
		if (r[0] != null && r[0] == "ok") {
			var domains = r[1];
			var domainListObj = $("#chargePayId");
			domainListObj.html("<option value=''>全部</option>");
			var str = "";
			for (var i = 0; i < domains.length; i++) {
				var domain = domains[i];
				if (chargePayId != null && domain.id == chargePayId) {
					str += "<option selected='selected' value='" + domain.id + "'>" + domain.chargeDes + "[" + domain.memberId + "]</option>";
				} else {
					str += "<option value='" + domain.id + "'>" + domain.chargeDes + "[" + domain.memberId + "]</option>";
				}
				domainListObj.append(str);
				str = "";
			}
		} else if (r[0] != null && r[0] == "error") {
			swal(r[1]);
		} else {
			swal("添加收款银行数据失败.");
		}
	});
};

/**
 * 获取管理员配置.
 */
WithdrawAutoConfigEditPage.prototype.getDealadminType = function(bizSystem,dealAdminId) {
	var paraId = withdrawAutoConfigEditPage.param.id
	managerWithDrawAutoConfigAction.getAllDealadminByBizSystem(bizSystem, function(r) {
		if (r[0] != null && r[0] == "ok") {
			var domains = r[1];
			var domainListObj = $("#dealAdminId");
			domainListObj.html("<option value=''>全部</option>");
			var str = "";
			for (var i = 0; i < domains.length; i++) {
				var domain = domains[i];
				if (dealAdminId != null && domain.id == dealAdminId) {
					str += "<option selected='selected' value='" + domain.id + "'>" + domain.username + "</option>";
				} else {
					str += "<option value='" + domain.id + "'>" + domain.username + "</option>";
				}
				domainListObj.append(str);
				str = "";
			}
		} else if (r[0] != null && r[0] == "error") {
			swal(r[1]);
		} else {
			swal("添加数据失败.");
		}
	});
};

/**
 * 保存第三方自动出款配置.
 */
WithdrawAutoConfigEditPage.prototype.saveWithdrawAutoConfig = function() {
	// 新建保存对象.
	var withDrawAutoConfig = {

	};

	withDrawAutoConfig.id = withdrawAutoConfigEditPage.param.id;
	// 系统.
	withDrawAutoConfig.bizSystem = $("#bizSystem").val();
	// 关联的第三方.
	withDrawAutoConfig.chargePayId = $("#chargePayId").val();
	// 关联的管理员.
	withDrawAutoConfig.dealAdminId = $("#dealAdminId").val();
	// 最高出款金额.
	withDrawAutoConfig.highestMoney = $("#highestMoney").val();
	// 开关.
	withDrawAutoConfig.autoWithdrawSwitch = $("#autoWithdrawSwitch").val();
	// 是否启用.
	withDrawAutoConfig.enabled = $("#enabled").val();
	
	withDrawAutoConfig.dealAdminUsername = $("#dealAdminUsername").val();

	// 参数判断.
	if (currentUser.bizSystem == "SUPER_SYSTEM") {
		if (withDrawAutoConfig.bizSystem == null
				|| withDrawAutoConfig.bizSystem.trim() == "") {
			swal("请选择业务系统!");
			$("#bizSystem").focus();
			return;
		}
	}
	if (withDrawAutoConfig.chargePayId == null || withDrawAutoConfig.chargePayId.trim() == "") {
		swal("请选择关联方式!");
		$("#chargePayId").focus();
		return;
	}
	if (withDrawAutoConfig.dealAdminId == null || withDrawAutoConfig.dealAdminId.trim() == "") {
		swal("请选择管理员!");
		$("#dealAdminId").focus();
		return;
	}
	if (withDrawAutoConfig.highestMoney == null || withDrawAutoConfig.highestMoney.trim() == "") {
		swal("请设置出款最高金额!");
		$("#highestMoney").focus();
		return;
	}
	if (withDrawAutoConfig.autoWithdrawSwitch == null || withDrawAutoConfig.autoWithdrawSwitch.trim() == "") {
		swal("请选择开关!");
		$("#autoWithdrawSwitch").focus();
		return;
	}
	if (withDrawAutoConfig.enabled == null || withDrawAutoConfig.enabled.trim() == "") {
		swal("请选择是否启用!");
		$("#enabled").focus();
		return;
	}
	// 保存配置信息.
	managerWithDrawAutoConfigAction.saveOrUpdateWithdrawAutoConfig(withDrawAutoConfig, function(r) {

		if (r[0] != null && r[0] == "ok") {
			swal({
				title : "保存成功",
				text : "您已经保存了这条信息。",
				type : "success"
			}, function() {
				var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
				parent.withDrawautoConfigPage.closeLayer(index);
			});
		} else if (r[0] != null && r[0] == "error") {
			swal(r[1]);
		} else {
			swal("设置自动出款配置数据失败.");
		}
	});
};