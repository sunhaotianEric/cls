function WithdrawAutoConfigPage() {

}

var withDrawautoConfigPage = new WithdrawAutoConfigPage();

withDrawautoConfigPage.param = {

};

// 分页参数
withDrawautoConfigPage.pageParam = {
	pageNo : 1,
	pageSize : defaultPageSize
};

/**
 * 查询参数
 */
withDrawautoConfigPage.queryParam = {
	// 系统.
	bizSystem : null,
	// 开启状态.
	enabled : null,
};

/**
 * 初始化页面.
 */
$(document).ready(function() {
	// 查询所有的自动出款页面配置
	withDrawautoConfigPage.initTableData();
});

WithdrawAutoConfigPage.prototype.initTableData = function() {

	withDrawautoConfigPage.queryParam = getFormObj($("#withDrawAutoConfigForm"));
	withDrawautoConfigPage.table = $('#withDrawAutoConfigTable').dataTable(
			{
				"oLanguage" : dataTableLanguage,
				"bProcessing" : false,
				"bServerSide" : true,
				"iDisplayLength" : defaultPageSize,
				"ajax" : function(data, callback, settings) {
					withDrawautoConfigPage.pageParam.pageSize = data.length;// 页面显示记录条数
					withDrawautoConfigPage.pageParam.pageNo = (data.start / data.length) + 1;// 当前页码
					managerWithDrawAutoConfigAction.getAllWithdrawAutoConfigByQuery(withDrawautoConfigPage.queryParam, withDrawautoConfigPage.pageParam, function(r) {
						// 封装返回数据
						var returnData = {
							recordsTotal : 0,
							recordsFiltered : 0,
							data : null
						};
						if (r[0] != null && r[0] == "ok") {
							returnData.recordsTotal = r[1].totalRecNum;// 返回数据全部记录
							returnData.recordsFiltered = r[1].totalRecNum;// 后台不实现过滤功能，每次查询均视作全部结果
							returnData.data = r[1].pageContent;// 返回的数据列表
						} else if (r[0] != null && r[0] == "error") {
							swal(r[1]);
						} else {
							swal("查询数据失败.");
						}
						callback(returnData);
					});
				},
				"columns" : [
						// 设置查询返回数据的值
						{
							"data" : "bizSystemName",
							"bVisible" : currentUser.bizSystem == 'SUPER_SYSTEM' ? true : false
						},
						{
							"data" : "thirdPayTypeDesc"
						},
						{
							"data" : "memberId"
						},
						// 设置app下载任务完成状态
						{
							"data" : "highestMoney"
						},
						{
							"data" : "dealAdminUsername"
						},
						// 设置是否领取奖金
						{
							"data" : "autoWithdrawSwitch",
							render : function(data, type, row, meta) {
								if (data == "1") {
									return '开';
								} else {
									return "<span style='color: red;'>关</span>";
								}
								return data;
							}
						},
						{
							"data" : "enabled",
							render : function(data, type, row, meta) {
								if (data == "1") {
									return '启用';
								} else {
									return "<span style='color: red;'>禁用</span>";
								}
								return data;
							}
						},
						/*{
							"data" : "id",
							render : function(data, type, row, meta) {
								var str = ""

								
								return str;
							}
						},*/
						{
							"data" : "id",
							render : function(data, type, row, meta) {
								var str = ""
								str = "<a class='btn btn-info btn-sm btn-bj' onclick='withDrawautoConfigPage.editWithdrawAutoConfig(" + data
										+ ")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;";
								if (row.enabled == 1) {
									str += "<a class='btn btn-danger btn-sm btn-del' onclick='withDrawautoConfigPage.enabledWithdrawAutoConfig(" + data
											+ ",0)'><i class='fa fa-star' ></i>&nbsp;停用 </a>&nbsp;&nbsp;";
								} else {
									str += "<a class='btn btn-info btn-sm btn-bj' onclick='withDrawautoConfigPage.enabledWithdrawAutoConfig(" + data
											+ ",1)'><i class='fa fa-star' ></i>&nbsp;启用 </a>&nbsp;&nbsp;";
								}
								if (row.autoWithdrawSwitch == 1) {
									str += "<a class='btn btn-danger btn-sm btn-del' onclick='withDrawautoConfigPage.enableWithdrawAutoConfig(" + data
											+ ",0)'><i class='fa fa-star' ></i>&nbsp;关闭全自动出款 </a>&nbsp;&nbsp;";
								} else {
									str += "<a class='btn btn-info btn-sm btn-bj' onclick='withDrawautoConfigPage.enableWithdrawAutoConfig(" + data
											+ ",1)'><i class='fa fa-star' ></i>&nbsp;开启全自动出款 </a>&nbsp;&nbsp;";
								}
								return str;
							}
						}, ]
			}).api();
};

/**
 * 查询数据
 */
// 设置查询按钮点击事件
WithdrawAutoConfigPage.prototype.findWithdrawAutoConfig = function() {
	// 获取页面ID为bizSystem的值
	var bizSystem = $("#bizSystem").val();
	// 获取页面ID为enable的值
	var enabled = $("#enabled").val();

	if (bizSystem == "") {
		withDrawautoConfigPage.queryParam.bizSystem = null;
	} else {
		withDrawautoConfigPage.queryParam.bizSystem = bizSystem;
	}
	if (enabled == "") {
		withDrawautoConfigPage.queryParam.enabled = null;
	} else {
		withDrawautoConfigPage.queryParam.enabled = enabled;
	}
	withDrawautoConfigPage.table.ajax.reload();
};

WithdrawAutoConfigPage.prototype.closeLayer = function(index) {
	layer.close(index);
	withDrawautoConfigPage.table.ajax.reload();
}

/**
 * 开关
 */
WithdrawAutoConfigPage.prototype.enableWithdrawAutoConfig = function(id, flag) {
	var str = "";
	if (flag == 0) {
		str = "关闭";
	} else {
		str = "开启";
	}

	swal({
		title : "您确定要" + str + "该条充值配置？",
		text : "请谨慎操作！",
		type : "warning",
		showCancelButton : true,
		confirmButtonColor : "#DD6B55",
		cancelButtonText : "取消",
		confirmButtonText : "确定",
		closeOnConfirm : false
	}, function() {
		managerWithDrawAutoConfigAction.enableWithdrawAutoConfigSwitch(id, flag, function(r) {
			if (r[0] != null && r[0] == "ok") {
				swal({
					title : "提示",
					text : "操作成功！",
					type : "success"
				});
				withDrawautoConfigPage.table.ajax.reload();
			} else if (r[0] != null && r[0] == "error") {
				swal(r[1]);
			} else {
				swal("操作失败.");
			}
		});
	});
};


/**
 * 启用停用.
 */
WithdrawAutoConfigPage.prototype.enabledWithdrawAutoConfig = function(id, flag) {
	var str = "";
	if (flag == 0) {
		str = "停用";
	} else {
		str = "启用";
	}

	swal({
		title : "您确定要" + str + "该条充值配置？",
		text : "请谨慎操作！",
		type : "warning",
		showCancelButton : true,
		confirmButtonColor : "#DD6B55",
		cancelButtonText : "取消",
		confirmButtonText : "确定",
		closeOnConfirm : false
	}, function() {
		managerWithDrawAutoConfigAction.enableWithdrawAutoConfig(id, flag, function(r) {
			if (r[0] != null && r[0] == "ok") {
				swal({
					title : "提示",
					text : "操作成功！",
					type : "success"
				});
				withDrawautoConfigPage.table.ajax.reload();
			} else if (r[0] != null && r[0] == "error") {
				swal(r[1]);
			} else {
				swal("操作失败.");
			}
		});
	});
};


/**
 * 编辑自动入账对象.
 */
WithdrawAutoConfigPage.prototype.editWithdrawAutoConfig = function(id) {
	layer.open({
		type : 2,
		title : '编辑自动入账配置',
		maxmin : false,
		shadeClose : true,
		// 点击遮罩关闭层
		area : [ '70%', '80%' ],
		content : contextPath + "/managerzaizst/withdrawautoconfig/" + id + "/withdrawautoconfig_edit.html",
		cancel : function(index) {
			withDrawautoConfigPage.table.ajax.reload();
		}
	});
};

/**
 * 新增
 */
WithdrawAutoConfigPage.prototype.addWithdrawAutoConfig=function()
{
	
	 layer.open({
         type: 2,
         title: '新增自动入款配置',
         maxmin: false,
         shadeClose: true,
         //点击遮罩关闭层
         area: ['70%', '80%'],
         content: contextPath + "/managerzaizst/withdrawautoconfig/withdrawautoconfig_add.html",
         cancel: function(index){ 
        	 withDrawautoConfigPage.table.ajax.reload();
      	 }
     });
}

/**
 * 编辑页面关闭窗口.
 */
WithdrawAutoConfigPage.prototype.layerClose = function(index) {
	layer.close(index);
	withDrawautoConfigPage.findRecordControl();
}
