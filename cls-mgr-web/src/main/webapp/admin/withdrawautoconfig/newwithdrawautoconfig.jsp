<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
    String id = request.getParameter("id")==null?"0":request.getParameter("id"); 
%>
<!DOCTYPE html>
<html>  
    <head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>商户信息管理</title>
    <link rel="shortcut icon" href="favicon.ico">
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
     <link href="<%=path%>/css/plugins/iCheck/custom.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" rel="stylesheet">
     <link href="<%=path%>/css/plugins/summernote/summernote.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" rel="stylesheet">
     <link href="<%=path%>/css/plugins/summernote/summernote-bs3.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/plugins/sweetalert/sweetalert.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" rel="stylesheet">

</head>
   <body class="gray-bg">
        <div class="wrapper wrapper-content animated fadeInRight"  >
            <div class="row back-change">
                 <div class="ibox-content">
                            <form class="form-horizontal" id="newwithdrawautoconfigForm">
                            <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">商户：</label>
                                    <div class="col-sm-3">
                                    <input id="bizSystem" name="bizSystem" type="text"  class="form-control" disabled="disabled" />
                                    </div>
                                    <div class="col-sm-6"></div>
                                </div>
                                </c:if>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">商户名称：</label>
                                    <div class="col-sm-3">
                                        <input id="id" name="id" type="hidden"  />
                                         <c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
                                         <input id="bizSystem" name="bizSystem" type="hidden"  value="${admin.bizSystem}" />
                                         </c:if>
                                        <input id="bizSystemName" name="bizSystemName"  type="text"  class="form-control" disabled="disabled"></div>
                                     <div class="col-sm-6"></div>
                                </div>
                               
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">关联方式：</label>
                                    <div class="col-sm-3">
                                        <select id="chargePayId" name="chargePayId" class="ipt form-control" >
                                        </select>
                                     </div>
                                     <div class="col-sm-6"></div>
                                </div>   
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">管理员：</label>
                                    <div class="col-sm-3">
                                        <select id="dealAdminId" name="dealAdminId" class="ipt form-control" >
                                        </select><input type="hidden" value="" name="dealAdminUsername" id="dealAdminUsername">
                                     </div>
                                     <div class="col-sm-6"></div>
                                </div> 
                                                          
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">最高出款金额：</label>
                                    <div class="col-sm-3">
                                        <input id="highestMoney" name="highestMoney" type="text" value="" class="form-control"></div>
                                    <div class="col-sm-6"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">开关：</label>
                                    <div class="col-sm-3">
                                   <select class="ipt form-control" id="autoWithdrawSwitch" name="autoWithdrawSwitch">
					                            <option value="1" selected="selected">开</option>
					                            <option value="0">关</option>
            						</select>
                                     </div>
                                     <div class="col-sm-6"></div>
                                </div> 
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">是否启用：</label>
                                    <div class="col-sm-3">
                                    <select class="ipt form-control" id="enabled" name="enabled">
					                            <option value="1" selected="selected">启用</option>
					                            <option value="0">停用</option>
            						</select>
                                     </div>
                                     <div class="col-sm-6"></div>
                                </div> 
                                  <div class="form-group">
                                    <div class="row">
                                      <div class="col-sm-3"></div>
					                        <div class="col-sm-1">
					                            <button type="button" class="btn btn-w-m btn-white" onclick="newwithdrawautoconfigPage.saveData()">提 交</button></div>
					                        <div class="col-sm-2"></div>
					                        <div class="col-sm-1">
					                            <!-- <button type="button" class="btn btn-w-m btn-white">重 置</button> -->
					                            <input type="reset" class="btn btn-w-m btn-white" value="重 置"/>
					                            </div>
					                            <div class="col-sm-5"></div>
					                    </div>
                                </div>    
                         
                            </form>
                    </div>
              
            </div>
        </div>
    <!-- js -->
    <script src="<%=path%>/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="<%=path%>/js/plugins/prettyfile/bootstrap-prettyfile.js"></script>
    <script src="<%=path%>/js/plugins/switchery/switchery.js"></script>
    <script src="<%=path%>/js/plugins/iCheck/icheck.min.js"></script>
    <script src="<%=path%>/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
     <script src="<%=path%>/js/plugins/clockpicker/clockpicker.js"></script>
     <script src="<%=path%>/js/plugins/cropper/cropper.min.js"></script>
     <script src="<%=path%>/js/demo/form-advanced-demo.min.js"></script>
     <script src="<%=path%>/js/jquery-ui-1.10.4.min.js"></script>
   
    <script type="text/javascript" src="<%=path%>/admin/withdrawautoconfig/js/newwithdrawautoconfig.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerWithDrawAutoConfigAction.js'></script>
    <script type="text/javascript">
	    $(document).ready(function(){$(".sortable-list").sortable({connectWith:".connectList"}).disableSelection()});
	    newwithdrawautoconfigPage.param.id = <%=id%>;
	</script>
	 <script>
       $(document).ready(function(){$(".i-checks").iCheck({checkboxClass:"icheckbox_square-green",radioClass:"iradio_square-green",})});
     </script>
</body>
</html>

