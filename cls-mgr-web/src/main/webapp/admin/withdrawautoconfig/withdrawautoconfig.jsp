<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>商户管理</title>
<link rel="shortcut icon" href="favicon.ico">
<jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>

<body>
	<div class="animated fadeIn">
		<div class="ibox-content">
			<div class="row m-b-sm m-t-sm">
				<form class="form-horizontal" id="withDrawAutoConfigForm">
					<div class="row">
					 <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
						<div class="col-sm-3">
							<div class="input-group m-b">
								<span class="input-group-addon">商户</span>
								<cls:bizSel name="bizSystem" id="bizSystem"
									options="class:ipt form-control" />
							</div>
						</div>
						 </c:if>
						 <c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
						<div class="col-sm-3">
						
						</div>
						 </c:if>
						<div class="col-sm-4">
							<div class="input-group m-b" style="display:none">
								<span class="input-group-addon">启用状态</span> <select
									class="ipt form-control" id="enabled" name="enabled">
									<option value="" selected="selected">==请选择==</option>
									<option value="1">启用</option>
									<option value="0">禁用</option>
								</select>
							</div>
						</div>

						<div class="col-sm-4">
							<div class="form-group nomargin text-right">
								<button type="button" class="btn btn-outline btn-default btn-js"
									onclick="withDrawautoConfigPage.pageParam.pageNo = 1;withDrawautoConfigPage.findWithdrawAutoConfig()">
									<i class="fa fa-search"></i>&nbsp;查 询
								</button>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<button type="button"
									class="btn btn-outline btn-default btn-add"
									onclick="withDrawautoConfigPage.addWithdrawAutoConfig()">
									<i class="fa fa-plus"></i>&nbsp;新 增
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="jqGrid_wrapper">
				<div class="ui-jqgrid ">
					<div class="ui-jqgrid-view">
						<div class="ui-jqgrid-hdiv">
							<div class="ui-jqgrid-hbox" style="padding-right: 0">
								<table id="withDrawAutoConfigTable"
									class="ui-jqgrid-htable ui-common-table table table-bordered">
									<thead>
										<tr class="ui-jqgrid-labels">
											<th class="ui-th-column ui-th-ltr">商户名称</th>
											<th class="ui-th-column ui-th-ltr">第三方支付描述</th>
											<!--<th>商户类型</th>预留-->
											<th class="ui-th-column ui-th-ltr">关联商户号</th>
											<th class="ui-th-column ui-th-ltr">最高出款金额</th>
											<th class="ui-th-column ui-th-ltr">关联管理员</th>
											<!-- <th class="ui-th-column ui-th-ltr">启用状态</th> -->
											<th class="ui-th-column ui-th-ltr">全自动出款开关</th>
											<th class="ui-th-column ui-th-ltr">启用状态</th>
											<th class="ui-th-column ui-th-ltr">操   作</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- js -->
	<script type="text/javascript" src="<%=path%>/admin/common/common.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
	<script type="text/javascript" src="<%=path%>/admin/withdrawautoconfig/js/withdrawautoconfig.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
	<!-- dwr -->
	<script type='text/javascript' src='<%=path%>/dwr/interface/managerWithDrawAutoConfigAction.js'></script>
</body>
</html>

