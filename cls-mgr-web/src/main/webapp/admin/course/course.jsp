<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
	String imgServerUrl = SystemConfigConstant.imgServerUrl;
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>教程管理</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    <!-- js -->
    <script type="text/javascript">
    	var imgServerUrl = '<%= imgServerUrl %>';
    </script>
    <script type="text/javascript" src="<%=path%>/admin/course/js/course.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerCourseAction.js'></script>
    
</head>
<body >
<div class="animated fadeIn">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox ">
                <div class="ibox-content">
                    <form class="form-horizontal" id="courseQuery">
                            <div class="col-sm-12 biz" >
                            <div class="col-sm-12">
                                <div class="form-group nomargin text-right">
                                    <button type="button" style="display: inline-block; vertical-align: top;" class="btn btn-outline btn-default btn-kj biz" onclick="coursePage.findCourseControl()"><i class="fa fa-search"></i>&nbsp;查 询</button>
                                    <button type="button" style="display: inline-block; vertical-align: top;" class="btn btn-outline btn-default btn-kj" onclick="coursePage.addCourse()"><i class="fa fa-plus"></i>&nbsp;新增</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="jqGrid_wrapper">
                        <div class="ui-jqgrid ">
                            <div class="ui-jqgrid-view">
                                <div class="ui-jqgrid-hdiv">
                                    <div class="ui-jqgrid-hbox" style="padding-right:0">
                                        <table class="ui-jqgrid-htable ui-common-table table table-bordered" id="courseTable">
                                            <thead>
                                                <tr class="ui-jqgrid-labels">
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">序号</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">标题</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">内容</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">发布日期</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">更新日期</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">创建管理员</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">修改管理员</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">是否启用</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">操作</div></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    
</body>
</html>

