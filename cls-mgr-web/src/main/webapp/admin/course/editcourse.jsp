<%@ page language="java" contentType="text/html; charset=UTF-8" 
	import = "com.team.lottery.enums.EHelpType,com.team.lottery.system.SystemConfigConstant" 
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
	String id = request.getParameter("id");  //获取查询的ID
	
	StringBuffer  url = request.getRequestURL();
	String domainUrl=url.toString().replaceAll(request.getRequestURI(), "")+path;
 	String staticImageServerUrl="http://"+SystemConfigConstant.picHost;
%>
<html>
<head>
    <meta charset="utf-8" />
    <title>教程管理</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
 	<link rel="stylesheet" href="<%=path%>/resources/kindeditor-4.1.10/themes/default/default.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>"/>
	<script charset="utf-8" src="<%=path%>/resources/kindeditor-4.1.10/kindeditor-min.js"></script>
	<script charset="utf-8" src="<%=path%>/resources/kindeditor-4.1.10/lang/zh_CN.js"></script>
    <link rel="shortcut icon" href="favicon.ico">
    <script>
    var  helpPage=parent.helpPage;
    var layerindex = parent.layer.getFrameIndex(window.name);
    var olddomain = document.domain;
       
      // alert("domain is:"+document.domain);  
		var editor;  //editor.html()
		//图片服务器地址
		//var staticImageUrl = 'http://www.zzh.com:8080/cls-mgr-web';
		var staticImageUrl='<%=domainUrl%>';
		//var staticImageUrl = 'http://www.img.zzh.com';
		//var imgurl =  "http://42.51.191.174";
		var imgurl='<%=staticImageServerUrl%>';
		var url = staticImageUrl.split('//');
		KindEditor.ready(function(K) {
			//document.domain = 'zzh.com';
			editor = K.create('textarea[name="content"]', {
				uploadJson : location.protocol+'//'+url[1]+'/UploadFileForJsonServlet?dirfile=activityRichText',
				fileManagerJson :imgurl+'/file_manager_json.jsp',
				allowFileManager : true
			});
		});
	</script>
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeIn">
           <div class="row back-change">
               <div class="col-sm-12">
                   <div class="ibox ">
                       <div class="ibox-content">
                           <form class="form-horizontal">
							   <div class="form-group">
                                   <label class="col-sm-2 control-label">标题：<input type="hidden" id="courseId"/></label>
                                   <div class="col-sm-10">
                                       <input type="email" value="" class="form-control" id="title" size="50px"></div>
                               </div>
                               <div class="form-group">
                                   <label class="col-sm-2 control-label">内容：</label>
                                   <div class="col-sm-10">
                                       <!-- <textarea name="comment" class="form-control" style="height: 154px;visibility:hidden" required="" aria-required="true" id="des">***|***</textarea> -->
                                       <textarea name="content" id="content" style="width:805px;height:400px;visibility:hidden;"></textarea>
                                   </div>
                               </div>
	                        <div class="form-group">
	                            <label class="col-sm-2 control-label">是否启用：</label>
	                            <div class="col-sm-10">
	                                <select class="ipt" id="enable">
	                                    <option value="0">否</option>
	                                    <option value="1">是</option></select>
	                            </div>
	                        </div>
	                        <div class="form-group">
	                            <div class="col-sm-12 ibox">
	                                <div class="row">
	                                    <div class="col-sm-6 text-center">
	                                        <button type="button" class="btn btn-w-m btn-white btn－tj" onclick="editCoursePage.saveData()" >提 交</button></div>
	                                    <div class="col-sm-6 text-center">
	                                        <button type="button" class="btn btn-w-m btn-white">重 置</button></div>
	                                </div>
	                            </div>
	                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/course/js/editcourse.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    
    <script src="<%=path%>/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="<%=path%>/js/plugins/prettyfile/bootstrap-prettyfile.js"></script>
    <script src="<%=path%>/js/plugins/switchery/switchery.js"></script>
    <script src="<%=path%>/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <script src="<%=path%>/js/plugins/clockpicker/clockpicker.js"></script>
    <script src="<%=path%>/js/plugins/cropper/cropper.min.js"></script>
    <script src="<%=path%>/js/demo/form-advanced-demo.min.js"></script>
        
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerCourseAction.js'></script>
	<script type="text/javascript">
	editCoursePage.param.id = <%=id %>;
	</script>
</body>
</html>

