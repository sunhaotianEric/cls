function CoursePage(){}
var coursePage = new CoursePage();

coursePage.param = {
};

//分页参数
coursePage.pageParam = {
    pageNo : 1,
    pageSize : defaultPageSize
};


$(document).ready(function() {
	coursePage.initTableData();
	

});

CoursePage.prototype.initTableData = function(){
	coursePage.param = getFormObj($("#courseQuery"));
	coursePage.table =  $('#courseTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"scrollX": true,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			coursePage.pageParam.pageSize = data.length;//页面显示记录条数
			coursePage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
			managerCourseAction.getAllCourse(coursePage.param,coursePage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				}; 
				
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询奖金配置数据失败.");
				}
				callback(returnData);
		    });
		},
		"columns": [
		            {"data":null},
		            {"data":"title"},
		            {"data":"content",
		            	render: function(data, type, row, meta) {
		            		if (data.length > 20) {
		            			var title = data.substring(0,20) + "...";
		            			return "<span title = '"+data+"'>"+title+"</span>";
		            		}
		            		return data;
	            		}
		            },		      
		            {"data":"createDateStr"},
		            {"data":"updateDateStr"},
		            {"data":"createAdmin"},
		            {"data":"updateAdmin"},
		            {"data":"enable",
		            	render: function(data, type, row, meta) {
		            		if (data == "1") {
		            			return '启用';
		            		} else {
		            			return '停用';
		            		}
		            		return data;
	            		}
		            },
		            {"data":"id",
		            	 render: function(data, type, row, meta) {
		            		 var str = "";
		            		 if(row.enable == "1"){
		            			 str += "<a class='btn btn-warning btn-sm btn-bj' onclick='coursePage.enableCourse("+data+",0)'><i class='fa fa-pencil'></i>&nbsp;停用 </a>&nbsp;&nbsp;"
		            		 }else{
		            			 str += "<a class='btn btn-sm btn-bj btn-success' onclick='coursePage.enableCourse("+data+",1)'><i class='fa fa-pencil'></i>&nbsp;启用 </a>&nbsp;&nbsp;"
		            		 }
	                		 str += "<a class='btn btn-info btn-sm btn-bj' onclick='coursePage.editCourse("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;"
	                		 str +=	"<a class='btn btn-danger btn-sm btn-del' onclick='coursePage.delCourse("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";
	                		 return str;
	                	 }
		            },
		      ]
	}).api();
	
}

CoursePage.prototype.findCourseControl = function(){
	coursePage.table.ajax.reload();
}

/**
 * 删除活动中心信息
 */
CoursePage.prototype.delCourse = function(id){
	   swal({
           title: "您确定要删除这条信息吗",
           text: "删除后将无法恢复，请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "删除",
           closeOnConfirm: false
       },
       function() {
    	   managerCourseAction.delCourseById(id,function(r){
    		   if (r[0] != null && r[0] == "ok") {
    			   swal("删除成功！", "您已经永久删除了这条信息。", "success");
    			   coursePage.findCourseControl();
    		   }else if(r[0] != null && r[0] == "error"){
    			   swal(r[1]);
    		   }else{
    			   swal("删除活动中心信息失败.");
    		   }
    	   });
       })
};

/**
 * 停用，启用
 */
CoursePage.prototype.enableCourse = function(id,flag){
	var str="";
	   if(flag==0){
		   str="停用";  
	   }else{
		   str="启用";  
	   }
	   swal({
        title: "您确定要"+str+"该条帮助信息？",
        text: "请谨慎操作！",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        cancelButtonText: "取消",
        confirmButtonText: "确定",
        closeOnConfirm: false
    },
    function() {
    	managerCourseAction.enableCourse(id,flag,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "操作成功！",type: "success"});
				coursePage.table.ajax.reload();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("操作失败.");
			}
	    });
    });
};

CoursePage.prototype.closeLayer=function(index){
	layer.close(index);
	coursePage.findCourseControl(); //查询奖金配置数据
}

/**
 * 编辑活动中心信息
 */
CoursePage.prototype.editCourse = function(id){
	layer.open({
		type: 2,
		title: '活动信息编辑',
		maxmin: false,
		shadeClose: true,
		//点击遮罩关闭层
		area: ['70%', '80%'],
		content:contextPath + '/managerzaizst/course/'+ id +'/editcourse.html',
	})
}

CoursePage.prototype.layerClose=function(index){
	layer.close(index);
	coursePage.findCourseControl();
}

/**
 * 编辑活动中心信息
 */
CoursePage.prototype.addCourse = function(){
	layer.open({
		type: 2,
		title: '添加活动信息编辑',
		maxmin: false,
		shadeClose: true,
		//点击遮罩关闭层
		area: ['70%', '80%'],
		content:contextPath + '/managerzaizst/course/course_add.html',
	})
}
