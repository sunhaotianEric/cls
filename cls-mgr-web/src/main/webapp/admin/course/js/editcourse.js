function EditCoursePage(){}
var editCoursePage = new EditCoursePage();

editCoursePage.param = {
   	
};

$(document).ready(function() {
	var courseId = editCoursePage.param.id;  //获取查询的活动ID
	if(courseId != null){
		editCoursePage.editCourse(courseId);
	}
});

/**
 * 保存帮助信息
 */
EditCoursePage.prototype.saveData = function(){
	var id = $("#courseId").val();
	var title = $("#title").val();
	var content = editor.html();
	var enable = $("#enable").val();
	
	if(title==null || title.trim()==""){
		swal("请输入公告标题!");
		$("#title").focus();
		return;
	}
	if(content==null || content.trim()==""){
		swal("请输入公告内容!");
		editor.focus();
		return;
	}
	if(enable==null || enable.trim()==""){
		swal("请选择是否启用!");
		$("#enable").focus();
		return;
	}
	
	var course = {};
	course.id = id;
	course.title = title;
	course.content = content;
	course.enable = enable;
	managerCourseAction.saveCourse(course,function(r){
		if (r[0] != null && r[0] == "ok") {
			var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
			parent.coursePage.layerClose(index);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("保存帮助信息失败.");
		}
    });
};


/**
 * 赋值帮助信息
 */
EditCoursePage.prototype.editCourse = function(id){
	managerCourseAction.selectCourse(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			$("#courseId").val(r[1].id);
			$("#title").val(r[1].title);
			editor.html(r[1].content);
			$("#enable").val(r[1].enable);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取帮助信息失败.");
		}
    });
};