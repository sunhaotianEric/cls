function NotesPage(){}
var notesPage = new NotesPage();

notesPage.param = {
		
};

//分页参数
notesPage.pageParam = {
        pageNo : 1,
        pageMaxNo : 1,  //最大的页数
        skipHtmlStr : ""
};

/**
 * 查询参数
 */
notesPage.queryParam = {
		username : null,
		retype : null,
};

$(document).ready(function() {
	notesPage.getAllNotes(); //查询所有的客服数据
});

/**
 * 加载所有的客服数据
 */
NotesPage.prototype.getAllNotes = function(){
	notesPage.queryParam = {};
	notesPage.queryConditionNotes(notesPage.queryParam,notesPage.pageParam.pageNo);
};

/**
 * 条件查询客服数据
 */
NotesPage.prototype.queryConditionNotes = function(queryParam,pageNo){
	managerNotesAction.getAllNotes(queryParam,pageNo,function(r){
		if (r[0] != null && r[0] == "ok") {
			notesPage.refreshNotesPages(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询客服数据失败.");
		}
    });
};

/**
 * 刷新列表数据
 */
NotesPage.prototype.refreshNotesPages = function(page){
	var notesListObj = $("#notesList");
	notesListObj.html("");
	var str = "";
	var notes = page.pageContent;
	
	for(var i = 0 ; i < notes.length; i++){
		var note = notes[i];
		str += "<tr>";
		str += "  <td><input type='hidden' name='noteid' value='"+ note.noteid +"'/>"+ note.noteid +"</td>";
		str += "  <td>"+ note.notesub +"</td>";
		str += "  <td>"+ note.notetype +"</td>";
		str += "  <td>"+ note.username +"</td>";
		str += "  <td>"+ note.notetimeStr +"</td>";
		str += "  <td><a href='javascript:void(0)' onclick=''>"+ note.retypeStr +"</a></td>";
		str += "  <td><a href='javascript:void(0)' onclick=''>删除</a></td>";
		
		str += "</tr>";
		
		notesListObj.append(str);
		str = "";
	}
	
	
	//记录统计
	var pageMsg = $("#pageMsg");
	var pageStr = "";
	pageStr += "<th colspan='100'>";
	pageStr += "  当前第" + page.pageNo + "页/共" + page.totalPageNum +"页&nbsp;&nbsp;共" + page.totalRecNum +"条记录&nbsp;&nbsp;<a href='javascript:void(0)'  onclick='notesPage.pageQuery(1)'>首页</a>&nbsp;";
	pageStr += "<a href='javascript:void(0)' onclick='notesPage.pageParam.pageNo--;notesPage.pageQuery(notesPage.pageParam.pageNo)'>上一页</a>&nbsp;";
	pageStr += "<a href='javascript:void(0)' onclick='notesPage.pageParam.pageNo++;notesPage.pageQuery(notesPage.pageParam.pageNo)'>下一页</a>&nbsp;";
	pageStr += "<a href='javascript:void(0)' onclick='notesPage.pageQuery("+ page.totalPageNum +")'>尾页</a>";
	
	var skipHtmlStr = "";
	if($("#pageSkip").html() != null){
		$("#pageSkip").remove();
	}
    skipHtmlStr += "<span id='pageSkip'>";
    skipHtmlStr += "&nbsp;&nbsp;跳转至";
	skipHtmlStr += "<select id='currentPageChoose' class='ipt' onchange='notesPage.pageParam.pageNo = this.value;notesPage.pageQuery(notesPage.pageParam.pageNo)'  style='width:auto;'>";
	for(var i = 1; i <= page.totalPageNum; i++){
		skipHtmlStr += "<option value='"+ i +"'>"+ i +"</option>";
	}
	skipHtmlStr += "</select>";
	skipHtmlStr += "</span>";	
	pageStr += skipHtmlStr;
	pageStr += "</th>";
	pageMsg.html(pageStr);
	
	$("#currentPageChoose").find("option[value='"+ page.pageNo +"']").attr("selected",true);  //显示当前页码
	notesPage.pageParam.pageMaxNo = page.totalPageNum;  //标识最大的页数
};


/**
 * 根据条件查找对应页
 */
NotesPage.prototype.pageQuery = function(pageNo){
	if(pageNo < 1){
		notesPage.pageParam.pageNo = 1;
	}else if(pageNo > notesPage.pageParam.pageMaxNo){
		notesPage.pageParam.pageNo = notesPage.pageParam.pageMaxNo;
	}else{
		notesPage.pageParam.pageNo = pageNo;
	}
	//如果页码小于等于0，则不进行分页查找
	if(notesPage.pageParam.pageNo <= 0){
		return;
	}
	
	notesPage.findNotes();
};

/**
 * 按条件查询数据
 */
NotesPage.prototype.findNotes = function(){
	notesPage.queryParam.username = $("#username").val();
	notesPage.queryParam.retype = $("#retype").val();
	
	if(notesPage.queryParam.username!=null && notesPage.queryParam.username.trim()==""){
		notesPage.queryParam.username = null;
	}
	if(notesPage.queryParam.retype!=null && notesPage.queryParam.retype.trim()==""){
		notesPage.queryParam.retype = null;
	}
	
	notesPage.queryConditionNotes(notesPage.queryParam,notesPage.pageParam.pageNo);
};