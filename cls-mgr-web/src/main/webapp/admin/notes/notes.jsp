<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<html>
<head>
    <meta charset="utf-8" />
    <title>客服管理</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/notes/js/notes.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerNotesAction.js'></script>
</head>
<body>
   <div class="alert alert-info">当前位置<b class="tip"></b>客服管理<b class="tip"></b>所有问题列表</div>
   		   	<table class="tb">
	         <tr>
	          <td>会员：</td>
	             <td>
	             	<input id="username" type="text"/>
	             </td>
	             <td>状态：</td>
	             <td>
	             	<select id="retype">
	             		<option value="">全部</option>
	             		<option value="1">尚未处理</option>
	             		<option value="2">正在处理</option>
	             		<option value="3">处理成功</option>
	             	</select>
	             </td>
	             <td><input class="btn" id="btn" type="button" onclick="notesPage.pageParam.pageNo = 1;notesPage.findNotes()" value="搜索" /></td>
	          </tr>
	     </table>
     
		<table class="tb">
			<tr>
			    <th>序号</th>
			    <th>问题标题</th>
			    <th>问题类别</th>
			    <th>提问会员</th>
			    <th>提问时间</th>
			    <th>处理状态</th>
			    <th>操作</th>
			 </tr>
			<tbody id="notesList">
			</tbody>
			<tr class="pager" id='pageMsg'>
		</table>
    
</body>
</html>

