function ImportLotteryXyCodeData() {
};

var importLotteryXyCodeData = new ImportLotteryXyCodeData();

/**
 * 参数
 */
importLotteryXyCodeData.editParam = {
	bizSystem : null
};

$(document).ready(function() {

});

/**
 * 上传excel
 */
ImportLotteryXyCodeData.prototype.uploadExcelFile = function(id) {
	var fileId = id + "File";
	var bizSystem = $("#bizSystem").val();
	var lotteryType = $("#lotteryType").val();

	if ($("#" + fileId).val() == "") {
		swal("请选择文件");
		return;
	}
	if (currentUser.bizSystem == 'SUPER_SYSTEM' && (bizSystem == null || bizSystem.trim() == "")) {
		swal("请选择子系统!");
		$("#bizSystemId").focus();
		return;
	}
	if (lotteryType == null && lotteryType == "") {
		swal("请选择彩种!");
		$("#bizSystemId").focus();
		return;
	}
	if (currentUser.bizSystem != 'SUPER_SYSTEM') {
		bizSystem = currentUser.bizSystem;
	}

	$("#" + id + "Info").text("导入中...");
	var uploadFile = dwr.util.getValue(fileId);
	managerLotteryCodeXyImportAction.uploadExcelFile(uploadFile, bizSystem, lotteryType, function(r) {
		if (r[0] != null && r[0] == "ok") {
			swal(r[1]);
			$("#" + id + "Info").text("导入成功!");
			$("#" + id + "Info").parent().addClass("btn-danger");
		} else if (r[0] != null && r[0] == "error") {
			swal(r[1]);
		} else {
			swal("上传文件失败");
			$("#" + id + "Info").text("重新导入");
		}
	});
};