function LotteryCodeXyImportListPage() {

};

var lotteryCodeXyImportListPage = new LotteryCodeXyImportListPage();

/**
 * 参数.
 */
lotteryCodeXyImportListPage.param = {

};
function HandUpdatePage(){}
var handUpdatePage = new HandUpdatePage();
handUpdatePage.param = {
};
/**
 * 加载彩种下拉
 */
HandUpdatePage.prototype.getAllTypes = function(){
	managerOrderAction.getPrivateLotteryTypes(function(r){
		if (r[0] != null && r[0] == "ok") {
			handUpdatePage.refreshTypes(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("查询所有的资金类型失败.");
		}
	});
};
/**
 * 展示彩种下拉框
 */
HandUpdatePage.prototype.refreshTypes = function(detailTypeMaps){
	var obj3=document.getElementById('lotteryTypeRegression');
	obj3.options.add(new Option("=请选择=",""));
	for(var key in detailTypeMaps){
		obj3.options.add(new Option(detailTypeMaps[key],key));
	}

};

/**
 * 查询参数.
 */
lotteryCodeXyImportListPage.queryParam = {

	// 所属系统.
	bizSystem : null,
	// 幸运彩种类型.
	lotteryType : null,
	// 期号.
	issueNo : null,
	// 使用状态.(0是未使用,1是已使用,2是已失效).
	usedStatus : null,
};

/**
 * 分页参数.
 */
lotteryCodeXyImportListPage.pageParam = {

	pageNo : 1,
	pageSize : defaultPageSize
};

/**
 * 页面初始化.
 */
$(document).ready(function() {
	lotteryCodeXyImportListPage.initTableData(); // 默认查询所有的用户
	handUpdatePage.getAllTypes(); //加载投注类型
	// 回车提交事件
	$("body").keydown(function() {
		if (event.keyCode == "13") {// keyCode=13是回车键
			lotteryCodeXyImportListPage.getAllUsersByCondition()
		}
	});
});

/**
 * 初始化列表数据
 */
LotteryCodeXyImportListPage.prototype.initTableData = function() {
	// 获取查询参数.
	lotteryCodeXyImportListPage.queryParam = getFormObj($("#queryForm"));
	lotteryCodeXyImportListPage.table = $('#lotteryCodeXyImportListTable').dataTable(
			{
				"oLanguage" : dataTableLanguage,
				"bProcessing" : false,
				"bServerSide" : true,
				"iDisplayLength" : defaultPageSize,
				"scrollX" : true,
				"drawCallback" : function(settings) {
					var api = this.api();
					var startIndex = api.context[0]._iDisplayStart;// 获取到本页开始的条数
					api.column(0).nodes().each(function(cell, i) {
						cell.innerHTML = startIndex + i + 1;
					});
				},
				"ajax" : function(data, callback, settings) {
					lotteryCodeXyImportListPage.pageParam.pageSize = data.length;// 页面显示记录条数
					lotteryCodeXyImportListPage.pageParam.pageNo = (data.start / data.length) + 1;// 当前页码
					managerLotteryCodeXyImportAction.getAllLotteryCodeXyImport(lotteryCodeXyImportListPage.queryParam, lotteryCodeXyImportListPage.pageParam, function(r) {
						// 封装返回数据
						var returnData = {
							recordsTotal : 0,
							recordsFiltered : 0,
							data : null
						};
						if (r[0] != null && r[0] == "ok") {
							returnData.recordsTotal = r[1].totalRecNum;// 返回数据全部记录
							returnData.recordsFiltered = r[1].totalRecNum;// 后台不实现过滤功能，每次查询均视作全部结果
							returnData.data = r[1].pageContent;// 返回的数据列表
						} else if (r[0] != null && r[0] == "error") {
							swal(r[1]);
						} else {
							swal("查询数据失败!");
						}
						callback(returnData);
					});
				},
				"columns" : [
						{
							"data" : null
						},
						{
							"data" : "bizSystemName",
							"bVisible" : currentUser.bizSystem == 'SUPER_SYSTEM' ? true : false
						},
						{
							"data" : "lotteryTypeDes"
						},
						{
							"data" : "issueNo"
						},
						{
							"data" : "openCode"
						},
						{
							"data" : "createTimeStr"
						},
						{
							"data" : "createAdmin"
						},
						{
							"data" : "usedStatus",
							render : function(data, type, row, meta) {
								if (data == "0") {
									return "<span style='color: green;'>未使用</span>";
								} else if (data == "1") {
									return "<span style='color: red;'>已使用</span>";
								} else {
									return "<span style='color: black;'>已失效</span>";
									;
								}
								return data;
							}
						},
						{
							"data" : "id",
							render : function(data, type, row, meta) {
								var str = ""
								if (row.usedStatus == 0) {
									str += "<a class='btn btn-danger btn-sm btn-del' onclick='lotteryCodeXyImportListPage.changeUsedStatus(" + data
											+ ",0)'><i class='fa fa-star' ></i>&nbsp;标记为失效 </a>&nbsp;&nbsp;";
								} else {
									str += "<span style='color: grey;'>该数据暂不能操作</span>";
								}
								return str;
							}
						}, ]
			}).api();

	// new $.fn.dataTable.FixedColumns(userList.table,{"leftColumns":2 });
}

/**
 * 查询条件获取.
 */
LotteryCodeXyImportListPage.prototype.queryConditionLotteryCodes = function() {
	lotteryCodeXyImportListPage.queryParam = getFormObj($("#queryForm"));
	lotteryCodeXyImportListPage.table.ajax.reload();
};

/**
 * 幸运号码导入类别.
 */
LotteryCodeXyImportListPage.prototype.importingLotteryCodeXyImportData = function() {
	layer.open({
		type : 2,
		title : '幸运开奖号码导入',
		maxmin : false,
		shadeClose : true,
		// 点击遮罩关闭层
		area : [ '800px', '300px' ],
		// area: ['60%', '58%'],
		content : contextPath + "/managerzaizst/lottery_code_xy_import/import_lotteryXyCodeData.html",
		cancel : function(index) {
			lotteryCodeXyImportListPage.getAllLotteryCodeXyImport();
		}
	});
}

/**
 * 导入成功,重新加载页面.
 */
LotteryCodeXyImportListPage.prototype.getAllLotteryCodeXyImport = function() {
	lotteryCodeXyImportListPage.table.ajax.reload();
	;
};

/**
 * 标记使用状态为失效.
 */
LotteryCodeXyImportListPage.prototype.changeUsedStatus = function(id) {
	swal({
		title : "您确定要将该条开奖号码标记为失效",
		text : "请谨慎操作！",
		type : "warning",
		showCancelButton : true,
		confirmButtonColor : "#DD6B55",
		cancelButtonText : "取消",
		confirmButtonText : "确定",
		closeOnConfirm : false
	}, function() {
		managerLotteryCodeXyImportAction.changeUsedStatus(id, function(r) {
			if (r[0] != null && r[0] == "ok") {
				swal({
					title : "提示",
					text : "操作成功！",
					type : "success"
				});
				lotteryCodeXyImportListPage.table.ajax.reload();
			} else if (r[0] != null && r[0] == "error") {
				swal(r[1]);
			} else {
				swal("操作失败.");
			}
		});
	});
}