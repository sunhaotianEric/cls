<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page
	import="com.team.lottery.util.ConstantUtil,com.team.lottery.vo.Admin"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
	Admin admin = (Admin) request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_ADMIN);
	String isTourist = request.getParameter("isTourist");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>幸运开奖号码导入</title>
<jsp:include page="/admin/include/include.jsp"></jsp:include>
<link rel="shortcut icon" href="favicon.ico">
</head>
<body>
	<div class="animated fadeIn">
		<div class="ibox-content">
			<div class="row m-b-sm m-t-sm">
				<form class="form-horizontal" id="queryForm">
					<div class="row">
						<c:if test="${admin.bizSystem == 'SUPER_SYSTEM'}">
							<div class="col-sm-3 biz">
								<div class="input-group m-b">
									<span class="input-group-addon">商户:</span>
									<cls:bizSel name="bizSystem" id="bizSystem"
										options="class:ipt form-control" />
								</div>
							</div>
						</c:if>
						<div class="col-sm-3">
							<div class="input-group m-b">
								<span class="input-group-addon">选择彩种</span>
								<select id='lotteryTypeRegression' class="ipt form-control" name="lotteryType">
								</select>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="input-group m-b">
								<span class="input-group-addon">期号</span> <input type="text"
									value="" name="issueNo" id="issueNo" class="form-control">
							</div>
						</div>
						<div class="col-sm-2">
							<div class="input-group m-b">
								<span class="input-group-addon">使用状态</span> <select
									class="ipt form-control" name="usedStatus">
									<option value="" selected="selected">请选择=</option>
									<!-- <option value="XYEB">幸运28</option> -->
									<option value="0">未使用</option>
									<option value="1">已使用</option>
									<option value="2">已失效</option>
								</select>
							</div>
						</div>
						<div class="col-sm-1">
							<div class="form-group  text-right">
								<button type="button" class="btn btn-outline btn-default btn-kj"
									onclick="lotteryCodeXyImportListPage.queryConditionLotteryCodes()">
									<i class="fa fa-search"></i>&nbsp;查 询
								</button>
							</div>
						</div>
					</div>
					<div class="ibox-title p4-8" style="padding: 14px 20px">
						<a class="btn-model btn btn-primary"
							onclick="lotteryCodeXyImportListPage.importingLotteryCodeXyImportData()"><i
							class='fa fa-upload'></i>&nbsp;导入幸运开奖号码数据</a>
					</div>
				</form>
			</div>
			<div class="jqGrid_wrapper">
				<div class="ui-jqgrid ">
					<div class="ui-jqgrid-view ">
						<div class="ui-jqgrid-hdiv">
							<div class="ui-jqgrid-hbox" style="padding-right: 0">
								<table id="lotteryCodeXyImportListTable"
									class="ui-jqgrid-htable ui-common-table table table-bordered">
									<thead>
										<tr class="ui-jqgrid-labels">
											<th class="ui-th-column ui-th-ltr">序号</th>
											<th class="ui-th-column ui-th-ltr">商户</th>
											<th class="ui-th-column ui-th-ltr">彩种类型</th>
											<th class="ui-th-column ui-th-ltr">期号</th>
											<th class="ui-th-column ui-th-ltr">开奖号码</th>
											<th class="ui-th-column ui-th-ltr">导入时间</th>
											<th class="ui-th-column ui-th-ltr">导入人</th>
											<th class="ui-th-column ui-th-ltr">使用状态</th>
											<th class="ui-th-column ui-th-ltr">操 作</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- js -->
	<script type="text/javascript"
		src="<%=path%>/admin/lottery_code_xy_import/js/lottery_code_xy_import_list.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
	<!-- dwr -->
	<script type='text/javascript'
		src='<%=path%>/dwr/interface/managerLotteryCodeXyImportAction.js'></script>
	<script type='text/javascript' src='<%=path%>/dwr/interface/managerOrderAction.js'></script>
</body>
</html>
