<%@ page language="java" contentType="text/html; charset=UTF-8"
	import="com.team.lottery.util.ConstantUtil,com.team.lottery.vo.Admin,java.math.BigDecimal"
	import="com.team.lottery.service.LotteryCoreService"
	import="com.team.lottery.enums.ELotteryTopKind"
	import="com.team.lottery.util.ApplicationContextUtil,com.team.lottery.system.SystemConfigConstant"
	import="com.team.lottery.util.StringUtils" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>幸运开奖号码导入</title>
<jsp:include page="/admin/include/include.jsp"></jsp:include>
<link
	href="<%=path%>/css/plugins/iCheck/custom.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>"
	rel="stylesheet">
<style type="text/css">
.mb0 {
	margin-bottom: 0;
}

.mb30 {
	margin-bottom: 30px;
}

.mt50 {
	margin-top: 50px;
}

.clear:after {
	content: '';
	display: table;
	clear: both;
}

.input-group-btn {
	float: left;
	width: auto;
}

#sscrebateValue, #ksrebateValue, #syxwrebateValue, #pk10rebateValue,
	#dpcrebateValue {
	float: left;
	width: 70%;
}

.col-sm-2 span {
	display: inline-block;
	line-height: 34px;
}
</style>
<style type="text/css">
.jsstar {
	list-style: none;
	margin: 0px;
	padding: 0px;
	width: 100px;
	height: 20px;
	position: relative;
	display: inline-block;
}

.jsstar li {
	padding: 0px;
	margin: 0px;
	float: left;
	width: 20px;
	height: 20px;
	background: url(../../admin/resource/img/star_rating.gif) 0 0 no-repeat;
}

.jsstar_bright {
	background-position: left bottom;
}
</style>
<style type="text/css">
.row p {
	position: absolute;
	top: 8px;
}

.alert-msg-bg {
	display: none;
	position: fixed;
	top: 0px;
	bottom: 0px;
	left: 0px;
	right: 0px;
	z-index: 8;
	background: #000;
	opacity: 0.4;
}

.alert-msg {
	display: none;
	position: fixed;
	top: 50%;
	right: 0px;
	left: 0px;
	margin: -200px auto 0 auto;
	z-index: 9;
	width: 500px;
	min-height: initial;
	background: #fff;
	box-shadow: 0px 4px 6px 0px rgba(0, 0, 0, 0.4);
	-webkit-box-shadow: 0px 4px 6px 0px rgba(0, 0, 0, 0.4);
}

.alert-msg .msg-head {
	position: relative;
	height: 60px;
	line-height: 60px;
	background: #3088ff;
}

.alert-msg .msg-head p {
	color: #fff;
	font-size: 18px;
	padding-left: 28px;
}

.alert-msg .msg-head .close {
	position: absolute;
	top: 0px;
	bottom: 0px;
	right: 20px;
	margin: auto 0;
	cursor: pointer;
}

.alert-msg .msg-content {
	padding: 26px 28px;
	box-sizing: border-box;
}

.alert-msg .msg-content .msg-title {
	line-height: 26px;
	color: #676767;
	font-size: 14px;
	padding-bottom: 16px;
}

.alert-msg .msg-content .info {
	line-height: 24px;
	padding: 14px 0;
	color: #676767;
	font-size: 13px;
}

.alert-msg .msg-content .info .blue {
	color: #006CFF;
}

.alert-msg .msg-content .btn {
	text-align: center;
}

.alert-msg .msg-content .btn .inputBtn {
	transition-duration: .3s;
	-webkit-transition-duration: .3s;
	cursor: pointer;
	font-size: 18px;
	color: #fff;
	font-family: "Microsoft YaHei", Helvetica Neue, Tahoma, Arial, "寰蒋闆呴粦",
		"瀹嬩綋", "榛戜綋";
	background: #388cff;
	border: 0px;
	width: 170px;
	height: 49px;
	border-radius: 3px;
	-webkit-border-radius: 3px;
}

.alert-msg .msg-content .btn .inputBtn:hover {
	background: #1577FD;
}

.alert-msg .msg-content .btn .inputBtn.gray {
	background: #B1B1B1;
}

.alert-msg .msg-content .btn .inputBtn.gray:hover {
	background: #B1B1B1;
}

.clear:after {
	content: '';
	display: table;
	clear: both;
}

.input-group-btn {
	float: left;
	width: auto;
}

.input-group-btn .btn {
	height: 34px;
}

#sscrebateValue, #ksrebateValue, #syxwrebateValue, #pk10rebateValue,
	#dpcrebateValue {
	float: left;
	width: 80%;
}
/* .col-sm-2 span{display: inline-block;line-height: 34px;} */
.btn-sm {
	margin-top: 20px;
}

.input-append {
	display: block;
	overflow: hidden;
}

.input-append:after {
	content: '';
	display: table;
	clear: both;
}

.input-append .form-control {
	float: left;
	width: 80%;
}
</style>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeIn">
		<div class="row back-change">
			<div class="col-sm-12">
				<div class="ibox ">
					<div class="ibox-content">
						<!-- <form class="form-horizontal" id="queryForm">
                             
                         </form> -->
						<form class="form-horizontal">
							<div class="row">
								<c:if test="${admin.bizSystem =='SUPER_SYSTEM'}">
									<div class="col-sm-6">
										<div class="input-group m-b">
											<span class="input-group-addon">商户</span>
											<cls:bizSel name="bizSystem" id="bizSystem"
												options="class:'ipt form-control'" />
										</div>
									</div>
								</c:if>
								<div class="col-sm-4">
									<div class="input-group m-b">
										<span class="input-group-addon">选择彩种</span>
										<%--   <cls:enmuSel name="lotteryName" id="lotteryName" options="class:ipt form-control"  className="com.team.lottery.enums.ELotteryKind"/> --%>
										<select class="ipt form-control" name="lotteryType" id="lotteryType">
											<option value="" selected="selected">==请选择==</option>
											<!-- <option value="XYEB">幸运28</option> -->
											<option value="JYKS">幸运快3</option>
											<option value="JLFFC">幸运分分彩</option>
											<option value="JSPK10">极速PK10</option>
											<option value="XYLHC">幸运六合彩</option>
											<option value="SFSSC">三分时时彩</option>
											<option value="WFSSC">五分时时彩</option>
											<option value="LCQSSC">老重庆时时彩</option>
											<option value="JSSSC">江苏时时彩</option>
											<option value="BJSSC">北京时时彩</option>
											<option value="GDSSC">广东时时彩</option>
											<option value="SCSSC">四川时时彩</option>
											<option value="SHSSC">上海时时彩</option>
											<option value="SDSSC">山东时时彩</option>
											<option value="SFKS">三分快三</option>
											<option value="WFKS">五分快三</option>
											<option value="SFSYXW">三分11选5</option>
											<option value="WFSYXW">五分11选5</option>
											<option value="SHFSSC">十分时时彩</option>
											<option value="SFPK10">三分PK10</option>
											<option value="WFPK10">五分pk10</option>
											<option value="SHFPK10">十分pk10</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div id="file-pretty">
									<div class="col-sm-8 ">
										<div class="input-append input-group ">
											<input type="file" class="form-control" id="imageFile">
										</div>
									</div>
									<div class="col-sm-4 text-center">
										<button class="btn btn-success " type="button"
											onclick="importLotteryXyCodeData.uploadExcelFile('image')">
											<i class="fa fa-upload"></i>&nbsp;&nbsp; <span class="bold"
												id="imageInfo" style="line-height: inherit;">导入</span>
										</button>
									</div>
								</div>
							</div>
						</form>
						<div class="row">
							<div class="col-sm-12">
								<p style="color: red">温馨提醒excel排列顺序:期号,开奖号码,注意开奖号码是数字并且以,号隔开,如果导入失败请检查表格格式是否正确!!</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<!-- 业务js -->
<script src="<%=path%>/js/plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript"
	src="<%=path%>/admin/lottery_code_xy_import/js/import_lotteryXyCodeData.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<script src="<%=path%>/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script src="<%=path%>/js/plugins/prettyfile/bootstrap-prettyfile.js"></script>
<script src="<%=path%>/js/plugins/switchery/switchery.js"></script>
<script
	src="<%=path%>/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<script src="<%=path%>/js/plugins/clockpicker/clockpicker.js"></script>
<script src="<%=path%>/js/plugins/cropper/cropper.min.js"></script>
<script src="<%=path%>/js/demo/form-advanced-demo.min.js"></script>
<!-- dwr -->
<script type='text/javascript'
	src='<%=path%>/dwr/interface/managerLotteryCodeXyImportAction.js'></script>
</html>
