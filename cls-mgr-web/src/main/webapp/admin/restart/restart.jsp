<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<html>
<head>
    <meta charset="utf-8" />
    <title>后台重启</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/restart/js/restart.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerSystemAction.js'></script>
</head>
<body>
    <div class="alert alert-info">当前位置<b class="tip"></b>网站信息管理<b class="tip"></b>后台重启</div>
   	<table class="tbform list">
		<tbody>
			<tr>
				<td colspan="3" align="left">
					重启密码: <input size="20" type="password" id="restartPass" />
				</td>
				<td>
					<input class="btn" type="button" value="重启" id="restartBtn"/>
					 &nbsp;&nbsp;注：重启会导致后台暂时无法访问
				</td>
			</tr>
		</tbody>
	</table>
</body>
</html>

