function RestartPage(){}
var restartPage = new RestartPage();

restartPage.param = {};

$(document).ready(function() {
	
    
    //绑定重启服务器按钮事件
    $("#restartBtn").click(function(){
    	restartPage.restartServer();
    });
    
});


/**
 * 重启服务器
 */
RestartPage.prototype.restartServer = function(){
	var restartPass = $("#restartPass").val();
	if(restartPass == null || restartPass == "") {
		alert("请输入重启密码!");
		return;
	}
	$("#restartBtn").attr('disabled','disabled');
	managerSystemAction.restartServer(restartPass, function(r){
		if (r[0] != null && r[0] == "ok") {
			alert("重启成功,请稍后访问后台系统");
			$("#restartBtn").removeAttr("disabled");
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
			$("#restartBtn").removeAttr("disabled");
		}else{
			showErrorDlg(jQuery(document.body), "数据删除请求失败.");
			$("#restartBtn").removeAttr("disabled");
		}
    });
};
