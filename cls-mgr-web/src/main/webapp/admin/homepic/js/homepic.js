function HomePicPage(){}
var homePicPage = new HomePicPage();

homePicPage.param = {

};

//分页参数
homePicPage.pageParam = {
    pageNo : 1,
    pageSize : defaultPageSize
};

/**
 * 查询参数
 */
homePicPage.queryParam = {
		bizSystem :null,
		isShow :null,
		picType :null
};

$(document).ready(function() {
	homePicPage.initTableData();
});

HomePicPage.prototype.initTableData = function(){
//	if(currentUser.bizSystem != "SUPER_SYSTEM"){
//		$(".biz").hide();
//		$(".text-right").parent().removeClass();
//	}
	homePicPage.queryParam = getFormObj($("#activityQuery"));
	homePicPage.table =  $('#homePicTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;// 获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			homePicPage.pageParam.pageSize = data.length;//页面显示记录条数
			homePicPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
			managerHomePicAction.getHomePicByQueryPage(homePicPage.queryParam,homePicPage.pageParam,function(r){
				// 封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
				returnData.recordsTotal = r[1].totalRecNum;// 返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;// 后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;// 返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询所有的数据失败.");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data":null},
		            {"data":"bizSystemName",
		            	"bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
	                {"data":"homePicUrl",
            			render: function(data, type, row, meta) {
            				var str ="";
            				/*if(row.homePicLinkUrl != "" && row.homePicLinkUrl != null){
            					str = "<a target='_blank' href='"+ row.homePicLinkUrl +"'><img  alt='活动图片' src='"+data+"' style='margin-left: 21px; width: 100px;height: 50px;' ></a>";
            				}else{
            					str = "<a target='_blank' href='"+ data +"'><img  alt='活动图片' src='"+data+"' style='margin-left: 21px; width: 100px;height: 50px;' ></a>";
            				}*/
            				str = "<a target='_blank' href='"+ imgServerUrl + data +"'><img  alt='活动图片' src='"+ imgServerUrl + data+"' style='margin-left: 21px; width: 100px;height: 50px;' ></a>";
    	            		return str;
                		}},
		            {"data":"name"},
		          
		            {"data":"picType",
		            	render: function(data, type, row, meta) {
	            		if (data == "MOBILE") {
	            			return '手机';
	            		} else {
	            			return '电脑';
	            		}
	            		return data;
            		}},
		           
		       
		      /*    {"data":"homePicLinkUrl"},
		            {"data":"createTimeStr"},
		            {"data":"createAdmin"},
		            {"data":"updateTimeStr"},
		            {"data":"updateAdmin"},*/
            		{"data":"orders"},
            		{"data":"isShow",
		            	render: function(data, type, row, meta) {
		            		if (data == "1") {
		            			return '启用';
		            		} else {
		            			return '停用';
		            		}
		            		return data;
	            		}
		            },
		            {"data":"id",
		            	 render: function(data, type, row, meta) {
		            		 var str="";
		            		 if(row.isShow == 1){
		            			 str += "<a class='btn btn-warning btn-sm btn-bj' onclick='homePicPage.disnableHomePicConfig("+data+")'><i class='fa fa-pencil'></i>&nbsp;停用</a>&nbsp;&nbsp;"
		            		 }else{
		            			 str += "<a class='btn btn-sm btn-bj btn-success' onclick='homePicPage.enableHomePicConfig("+data+")'><i class='fa fa-pencil'></i>&nbsp;启用</a>&nbsp;&nbsp;"
		            		 }
		            		 
		            		 str += "<a class='btn btn-info btn-sm btn-bj' onclick='homePicPage.editHomePic("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;"
		            		 str += "<a class='btn btn-danger btn-sm btn-del' onclick='homePicPage.delHomePicConfig("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";
		            		 
		            		 return str;
	                	 }
		            },
		      ]
	}).api();
	
}

/**
 * 条件查找活动
 */
HomePicPage.prototype.findHomePicByBizSystem = function(){
	var bizSystem = $("#bizSystem").val();
	var isShow = $("#isShow").val();
	var picType = $("#picType").val();
	if(bizSystem==null || bizSystem==""){
		homePicPage.queryParam.bizSystem=null;
	}else{
		homePicPage.queryParam.bizSystem=bizSystem;
	}
	if(isShow==null || isShow==""){
		homePicPage.queryParam.isShow=null;
	}else{
		homePicPage.queryParam.isShow=isShow;
	}
	if(picType==null || picType==""){
		homePicPage.queryParam.picType=null;
	}else{
		homePicPage.queryParam.picType=picType;
	}
	homePicPage.table.ajax.reload();
};

/**
 * 删除活动中心信息
 */
HomePicPage.prototype.delHomePicConfig = function(id){
	   swal({
           title: "您确定要删除这条信息吗",
           text: "删除后将无法恢复，请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "删除",
           closeOnConfirm: false
       },
       function() {
    	   managerHomePicAction.delHomePicConfig(id,function(r){
    		   if (r[0] != null && r[0] == "ok") {
    			   swal("删除成功！", "您已经永久删除了这条信息。", "success");
    			   homePicPage.findHomePicByBizSystem(); //查询奖金配置数据
    		   }else if(r[0] != null && r[0] == "error"){
    			   swal(r[1]);
    		   }else{
    			   swal("删除活动中心信息失败.");
    		   }
    	   });
       })
};

/**
 * 关闭显示
 * @param id
 * @return
 */
HomePicPage.prototype.disnableHomePicConfig=function(id){
	managerHomePicAction.disnableHomePicConfig(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal("停用成功！")
		    homePicPage.findHomePicByBizSystem();
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("启用失败.");
		}
    });
}

/**
 * 启用显示
 * @param id
 * @return
 */
HomePicPage.prototype.enableHomePicConfig=function(id){
	managerHomePicAction.enableHomePicConfig(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal("启用成功！")
		    homePicPage.findHomePicByBizSystem(); 
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("启用失败.");
		}
    });
}

/**
 * 刷新首页图片缓存
 * @return
 */
HomePicPage.prototype.refreshHomePicCache=function(){
	managerHomePicAction.refreshHomePicCache(function(r){
		if (r[0] != null && r[0] == "ok") {
			swal("刷新成功！")
		    homePicPage.findHomePicByBizSystem(); //刷新配置数据
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("刷新失败.");
		}
    });
}

//查询奖金配置数据
HomePicPage.prototype.closeLayer=function(index){
	layer.close(index);
	homePicPage.findHomePicByBizSystem(); 
}

/**
 * 编辑活动中心信息
 */
HomePicPage.prototype.editHomePic = function(id){
	layer.open({
		type: 2,
		title: '首页图片信息编辑',
		maxmin: false,
		shadeClose: true,
		//点击遮罩关闭层
		area: ['70%', '80%'],
		content:contextPath + '/managerzaizst/homepic/'+ id +'/edithomepic.html',
	})
}

/**
 * 添加活动中心信息
 */
HomePicPage.prototype.addHomePic = function(){
	layer.open({
		type: 2,
		title: '添加首页图片信息编辑',
		maxmin: false,
		shadeClose: true,
		//点击遮罩关闭层
		area: ['70%', '80%'],
		content:contextPath + '/managerzaizst/homepic/homepic_add.html',
	})
}
