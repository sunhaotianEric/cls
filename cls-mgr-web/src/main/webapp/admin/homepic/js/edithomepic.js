function EditHomePicPage(){}
var editHomePicPage = new EditHomePicPage();

editHomePicPage.param = {
   	
};

$(document).ready(function() {
	var homePicId = editHomePicPage.param.id;  //获取查询的首页ID
	var bizSytem = editHomePicPage.param.bizSytem;  //获取查询的首页ID
	if(homePicId != null){
		editHomePicPage.editHomePic(homePicId);
	}
	
//	editHomePicPage.getcurrentBizSystem();
});


/**
 * 保存首页信息
 */
EditHomePicPage.prototype.saveData = function(){
	var id = $("#Id").val();
	var bizSystem = $("#bizSystemId").val();
	var name = $("#nameId").val();
	var homePicUrl = $("#imagePath").val();
	var homePicLinkUrl = $("#homePicLinkId").val();
	var isShow = $("#isShow").val();
	var order = $("#order").val();
	var picType = $("#picTypeId").val();
	
	if(currentUser.bizSystem=='SUPER_SYSTEM' && (bizSystem == null || bizSystem.trim() == "")){
		swal("请选择子系统!");
		$("#bizSystemId").focus();
		return;
	}
	if(name==null || name.trim()==""){
		swal("请输入首页主题!");
		$("#nameId").focus();
		return;
	}
	if(homePicUrl==null || homePicUrl.trim()==""){
		swal("请输入首页图片!");
		$("#image").focus();
		return;
	}
	//var reg = /^(?:http(?:s|):\/\/)(?:(?:\w*?)\.|)(?:\w*?)\.(?:\w{2,4})(?:\?.*|\/.*|)$/ig;
//    var reg = /^(http|https):\/\//;
//	if(homePicLinkUrl != null && homePicLinkUrl != ""){
//		if(!reg.test(homePicLinkUrl)){
//			swal("必须以(http或https)://开头的域名地址！请输入正确的域名地址!");
//			$("#homePicLinkId").focus();
//			return;
//		}
//	}
/*	if(homePicLinkUrl==null || homePicLinkUrl.trim()==""){
		swal("请输入首页链接!");
		$("#homePicLinkId").focus();
		return;
	}*/
	
/*	if(!skipPath.startWith("/gameBet")) {
		swal("首页链接格式错误，请检查!");
		$("#skipPath").focus();
		return;
	}*/
	if(isShow==null || isShow.trim()==""){
		swal("请选择首页展示!");
		return;
	}
	if(order==null || order.trim()==""){
		swal("请输入图片展示顺序!");
		$("#order").focus();
		return;
	}
	
	if(isShow == 1){
		if(homePicUrl==null || homePicUrl.trim()==""){
			swal("请输入首页图片!");
			$("#image").focus();
			return;
		}
		if(picType==null || picType.trim()==""){
			swal("请输入图片展示方式!");
			$("#picTypeId").focus();
			return;
		}
	}
	
	
	var homePic = {};
	homePic.id = id;
	homePic.bizSystem = bizSystem;
	homePic.name = name;
	homePic.homePicUrl = homePicUrl;
	homePic.homePicLinkUrl = homePicLinkUrl;
	homePic.isShow = isShow;
	homePic.orders = order;
	homePic.picType = picType;
	managerHomePicAction.saveOrUpdateHomePic(homePic,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({
		           title: "保存成功",
		           text: "您已经保存了这条图片。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.homePicPage.closeLayer(index);
		   	    });
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("保存首页信息失败.");
		}
    });
};

/**
 * 获取当前业务系统
 */
EditHomePicPage.prototype.getcurrentBizSystem=function(){
	managerHomePicAction.getcurrentBizSystem(function(r){
		if (r[0] != null && r[0] == "ok") {
			var bizSystem = r[1];
			if(bizSystem == "SUPER_SYSTEM"){
				$("#bizSystemId").val(bizSystem);
			}else{
				$("#bizSystemId").val(bizSystem);
				$('#bizSystemId').attr("disabled",true);
				$('.biz').hide();
			}
		}else{
			swal("获取当前业务系统失败！");
		}
	});
}

/**
 * 赋值首页信息
 */
EditHomePicPage.prototype.editHomePic = function(id){
	managerHomePicAction.getHgomePicById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			$("#Id").val(r[1].id);
			$("#bizSystemId").val(r[1].bizSystem);
			$("#nameId").val(r[1].name);
			$("#imagePath").val(r[1].homePicUrl);
			$("#homePicLinkId").val(r[1].homePicLinkUrl);
			$("#isShow").val(r[1].isShow);
			$("#order").val(r[1].orders);
			$("#picTypeId").val(r[1].picType);
			//展示图片内容
			$("#image").attr("src",r[2] + r[1].homePicUrl);
			$("#image").show();
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取首页信息失败.");
		}
    });
};

/**
 * 赋值首页信息
 */
EditHomePicPage.prototype.uploadImgFile = function(id){
	var fileId = id + "File";
	if($("#" + fileId).val() == "") {
		swal("请选择图片文件");
		return; 
	}
	//获取业务系统和图片
	var bizSystem = $("#bizSystemId").val();
	var picType = $("#picTypeId").val();
	if(currentUser.bizSystem=='SUPER_SYSTEM' && (bizSystem == null || bizSystem.trim() == "")){
		swal("请选择子系统!");
		$("#bizSystemId").focus();
		return;
	}
	if(currentUser.bizSystem !='SUPER_SYSTEM') {
		bizSystem = currentUser.bizSystem;
	}
	if(picType==null || picType.trim()==""){
		swal("请输入图片展示方式!");
		$("#picTypeId").focus();
		return;
	}
	
	$("#" + id +"Info").text("上传中...");
	var uploadFile = dwr.util.getValue(fileId);
	managerHomePicAction.uploadImgFile(uploadFile, bizSystem, picType, function(r){
		if (r[0] != null && r[0] == "ok") {
			$("#" + id + "Path").val(r[1]);
			$("#" + id).attr("src", r[2] + r[1]);
			$("#" + id).show();
			$("#" + id +"Info").text("上传成功!");
			$("#" + id + "Info").parent().addClass("btn-danger");
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("上传文件失败");
		}
	});
};