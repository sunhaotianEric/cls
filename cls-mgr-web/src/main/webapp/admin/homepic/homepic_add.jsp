<%@ page language="java" contentType="text/html; charset=UTF-8" 
	import = "com.team.lottery.enums.EHelpType" 
	pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<html>
<head>
    <meta charset="utf-8" />
    <title>活动中心</title>
     <jsp:include page="/admin/include/include.jsp"></jsp:include>
    <link rel="shortcut icon" href="favicon.ico">
</head>
<body>
<div class="wrapper wrapper-content animated fadeIn">
           <div class="row back-change">
               <div class="col-sm-12">
                   <div class="ibox ">
                       <div class="ibox-content">
                           <form class="form-horizontal">
	                           <div class="form-group biz" >
	                              <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
	                                <label class="col-sm-2 control-label">商户：</label>
	                                <div class="col-sm-10">
	                                     <cls:bizSel name="bizSystem" id="bizSystemId" options="class:ipt form-control"/>
	                                </div>
	                                </c:if>
	                            </div>
                               <div class="form-group">
                                   <label class="col-sm-2 control-label">图片标题：<input type="hidden" id="Id"/></label>
                                   <div class="col-sm-10">
                                       <input type="email" value="" class="form-control" id="nameId" size="50px"></div>
                               </div>
                               <!--  -->
                               <div class="form-group">
                                   <label class="col-sm-2 control-label">活动链接：</label>
                                   <div class="col-sm-10">
                                       <input type="text" value="" class="form-control" id="homePicLinkId"></div>
                               </div>
                               <div class="form-group" id="file-pretty">
                                   <label class="col-sm-2 control-label">首页图片：</label>
                                   <div class="col-sm-8">
                                   	   <input type="hidden" id="imagePath" size="50px"/>
                                       <input type="file" class="form-control"  id="imageFile"></div>
                                   <div class="col-sm-2 text-center">
                                       <button class="btn btn-success " type="button" onclick="editHomePicPage.uploadImgFile('image')">
                                           <i class="fa fa-upload"></i>&nbsp;&nbsp;
                                           <span class="bold" id="imageInfo">上传</span></button>
                                   </div>
                               </div>
                               <div class="form-group" style="margin-bottom:5px;">
                                   <label class="col-sm-2 control-label"></label>
                                   <div class="col-sm-10">说明：电脑版的请上传像素为800 × 383图片格式的文件，手机版的请上传像素为640 × 251图片格式的文件</div>
                        	  </div>
                               <div class="form-group">
                                   <label class="col-sm-2 control-label"></label>
                                   <div class="col-sm-10">
                                    <img id="image"  alt="活动图片" src="" style="display: none;margin-top: 5px;width:300px"></div>
                        	  </div>
                        
                        <div class="form-group">
                            <label class="col-sm-2 control-label">首页展示顺序：</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" style="width:40px" onkeyup='checkNum(this)' id="order">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">手机/电脑展示：</label>
                            <div class="col-sm-10">
                                <select class="ipt" id="picTypeId">
                                	<option value="">==请选择==</option>
                                    <option value="PC">电脑</option>
                                    <option value="MOBILE">手机</option></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">是否启用：</label>
                            <div class="col-sm-10">
                                <select class="ipt" id="isShow">
                                    <option value="1">是</option>
                                    <option value="0">否</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12 ibox">
                                <div class="row">
                                    <div class="col-sm-6 text-center">
                                        <button type="button" class="btn btn-w-m btn-white btn－tj" onclick="editHomePicPage.saveData()" >提 交</button></div>
                                    <div class="col-sm-6 text-center">
                                        <button type="reset" class="btn btn-w-m btn-white">重 置</button></div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

    <!-- js -->
    <script src="<%=path%>/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="<%=path%>/js/plugins/prettyfile/bootstrap-prettyfile.js"></script>
    <script src="<%=path%>/js/plugins/switchery/switchery.js"></script>
    <script src="<%=path%>/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <script src="<%=path%>/js/plugins/clockpicker/clockpicker.js"></script>
    <script src="<%=path%>/js/plugins/cropper/cropper.min.js"></script>
    <script src="<%=path%>/js/demo/form-advanced-demo.min.js"></script>
        
     <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/homepic/js/edithomepic.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerHomePicAction.js'></script>
</body>
</html>

