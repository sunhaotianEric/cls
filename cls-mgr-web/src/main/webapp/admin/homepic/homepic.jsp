<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
	String imgServerUrl = SystemConfigConstant.imgServerUrl;
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>首页图片展示</title>
<jsp:include page="/admin/include/include.jsp"></jsp:include>
<script type="text/javascript">
    	var imgServerUrl = '<%=imgServerUrl%>';
</script>
</head>
<body>
	<div class="animated fadeIn">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox ">
					<div class="ibox-content">
						<form class="form-horizontal" id="activityQuery">
							<div class="row">
								<div class="col-sm-4 biz">
									<c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
										<div class="input-group m-b">
											<span class="input-group-addon">商户：</span>
											<cls:bizSel name="bizSystem" id="bizSystem"
												options="class:ipt form-control" />

										</div>
									</c:if>
								</div>
								<div class="col-sm-2">
									<div class="input-group m-b">
										<span class="input-group-addon">启用状态</span> <select
											class="ipt form-control" id="isShow" name="isShow">
											<option value="" selected="selected">==请选择==</option>
											<option value="1">启用</option>
											<option value="0">停用</option>
										</select>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="input-group m-b">
										<span class="input-group-addon">图片类型</span> <select
											class="ipt form-control" id="picType" name="picType">
											<option value="" selected="selected">==请选择==</option>
											<option value="MOBILE">手机</option>
											<option value="PC">电脑</option>
										</select>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group nomargin text-right">
										<button type="button"
											class="btn btn-outline btn-default btn-kj biz"
											onclick="homePicPage.findHomePicByBizSystem()">
											<i class="fa fa-search"></i>&nbsp;查 询
										</button>
										<button type="button"
											class="btn btn-outline btn-default btn-kj"
											onclick="homePicPage.addHomePic()">
											<i class="fa fa-plus"></i>&nbsp;新增
										</button>
										<button type="button"
											class="btn btn-outline btn-default btn-kj"
											onclick="homePicPage.refreshHomePicCache()">
											<i class=""></i>&nbsp;刷新缓存
										</button>
									</div>
								</div>
							</div>
						</form>
						<div class="jqGrid_wrapper">
							<div class="ui-jqgrid ">
								<div class="ui-jqgrid-view">
									<div class="ui-jqgrid-hdiv">
										<div class="ui-jqgrid-hbox" style="padding-right: 0">
											<table
												class="ui-jqgrid-htable ui-common-table table table-bordered"
												id="homePicTable">
												<thead>
													<tr class="ui-jqgrid-labels">
														<th class="ui-th-column ui-th-ltr">
															<div class="ui-jqgrid-sortable">序号</div>
														</th>
														<th class="ui-th-column ui-th-ltr">
															<div class="ui-jqgrid-sortable">商户</div>
														</th>
														<th class="ui-th-column ui-th-ltr">
															<div class="ui-jqgrid-sortable">图片链接</div>
														</th>
														<th class="ui-th-column ui-th-ltr">
															<div class="ui-jqgrid-sortable">标题</div>
														</th>
														<th class="ui-th-column ui-th-ltr">
															<div class="ui-jqgrid-sortable">首页图片类型</div>
														</th>
														<th class="ui-th-column ui-th-ltr">
															<div class="ui-jqgrid-sortable">展示顺序</div>
														</th>
														<th class="ui-th-column ui-th-ltr">
															<div class="ui-jqgrid-sortable">展示状态</div>
														</th>
														<!-- <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">活动链接</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                    	<div class="ui-jqgrid-sortable">创建日期</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">创建管理员</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">更新日期</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">更新管理员</div></th> -->
														<th class="ui-th-column ui-th-ltr">
															<div class="ui-jqgrid-sortable">操作</div>
														</th>
													</tr>
												</thead>
												<!-- <tbody>
												</tbody> -->
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- js -->
	<script type="text/javascript"
		src="<%=path%>/admin/homepic/js/homepic.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
	<!-- dwr -->
	<script type='text/javascript'
		src='<%=path%>/dwr/interface/managerHomePicAction.js'></script>
</body>
</html>

