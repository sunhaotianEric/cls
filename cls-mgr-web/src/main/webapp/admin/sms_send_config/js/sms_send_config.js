function SmsSendConfigPage(){}
var smsSendConfigPage = new SmsSendConfigPage();

/**
 * 查询参数
 */
smsSendConfigPage.queryParam = {
		id:null,
		apiType : null,
		apiUrl:null,
		userName:null,
		password:null,
		type:null,
		orders:null,
		enable:null,
		createTime:null,
		updateTime:null
};

/**
 * 分页参数
 */
smsSendConfigPage.pageParam = {
    pageNo : 1,
    pageSize : defaultPageSize
};

$(document).ready(function() {
	smsSendConfigPage.initTableData();
});

SmsSendConfigPage.prototype.initTableData = function() {
	smsSendConfigPage.queryParam = getFormObj($("#smsSendConfigPageForm"));
	smsSendConfigPage.table = $('#smsSendConfigPageTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"bPaginate" : false, 
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			smsSendConfigPage.pageParam.pageSize = data.length;//页面显示记录条数
			smsSendConfigPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerSmsSendConfigAction.getAllSmsSendConfig(smsSendConfigPage.queryParam,smsSendConfigPage.pageParam,function(r){
		//封装返回数据
		var returnData = {
			recordsTotal : 0,
			recordsFiltered : 0,
			data : null
		};
		if (r[0] != null && r[0] == "ok") {
			returnData.data = r[1].pageContent;;//返回的数据列表
			returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
			returnData.data = r[1].pageContent;;//返回的数据列表
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("查询信息失败.");
		}
		callback(returnData);
			});
		},
		"columns": [
            {"data": "id"},
            {"data": "apiType"},
            {"data": "apiUrl"},
            {"data": "userName"},
            {"data": "password"},
            {"data": "type"},
            {
            	"data": "enable",
            	render: function(data, type, row, meta) {
            		if (data == "1") {
            			return '启用';
            		} else {
            			return "<span style='color: red;'>停用</span>";
            		}
            		return data;
            	}
        	},
            {   
            	"data": "createTime",
            	render: function(data, type, row, meta) {
            		return dateFormat(data,'yyyy-MM-dd hh:mm:ss');
        		}
            },
            {   
            	"data": "updateTime",
            	render: function(data, type, row, meta) {
            		return dateFormat(data,'yyyy-MM-dd hh:mm:ss');
        		}
            },
            {
            	 "data": "id",
            	 render: function(data, type, row, meta) {
            		 var str=""
            			 str="<a class='btn btn-info btn-sm btn-bj' onclick='smsSendConfigPage.editConfigControl("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;"+
            		 		"<a class='btn btn-danger btn-sm btn-del' onclick='smsSendConfigPage.delConfigControl("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";
            	         if(row.enable=="1"){
            	        	 str = "<a class='btn btn-danger btn-sm btn-del' onclick='smsSendConfigPage.enableSms("+data+",0)'><i class='fa fa-star' ></i>&nbsp;停用 </a>&nbsp;&nbsp;"+str; 
            	         }else{
            	        	 str = "<a class='btn btn-info btn-sm btn-bj' onclick='smsSendConfigPage.enableSms("+data+",1)'><i class='fa fa-star' ></i>&nbsp;启用 </a>&nbsp;&nbsp;"+str;  
            	         }
            	         return str;
            	 }
            }
        ]
}).api(); 
}

/**
 * 查询数据
 */
SmsSendConfigPage.prototype.findConfigControl=function(){
	var type=$("#type").val();
	if(type==null||type==""){
		smsSendConfigPage.queryParam.type=null;
	}else{
		smsSendConfigPage.queryParam.type="DEFAULT";
	}
	smsSendConfigPage.table.ajax.reload();
}

SmsSendConfigPage.prototype.delConfigControl =function(id){
	  swal({
          title: "您确定要删除这条信息吗",
          text: "请谨慎操作！",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          cancelButtonText: "取消",
          confirmButtonText: "删除",
          closeOnConfirm: false
      },
      function() {
    	  managerSmsSendConfigAction.delSmsSendConfigById(id,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "删除成功",type: "success"});
				smsSendConfigPage.findConfigControl();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("删除失败.");
			}
	    });
	});
};

/**
 * 编辑
 */
SmsSendConfigPage.prototype.editConfigControl=function(id){
	layer.open({
        type: 2,
        title: '短信发送配置编辑',
        maxmin: false,
        shadeClose: true,
        //点击遮罩关闭层
        area: ['600px', '450px'],
        content: contextPath + "/managerzaizst/sms_send_config/sms_send_config_edit.html?id="+id,
        cancel: function(index){ 
        	smsSendConfigPage.table.ajax.reload();
     	}
    });
}

/**
 * 新增
 */
SmsSendConfigPage.prototype.addConfigControl=function(id){
    layer.open({
           type: 2,
           title: '短信发送配置新增',
           maxmin: false,
           shadeClose: true,
           //点击遮罩关闭层
           area: ['600px', '420px'],
           content: contextPath + "/managerzaizst/sms_send_config/sms_send_config_add.html?id="+id,   
           cancel: function(index){
        	   smsSendConfigPage.findConfigControl();
         }
    });
}

/**
 * 停用，启用
 */
SmsSendConfigPage.prototype.enableSms = function(id,flag){
	var str="";
	   if(flag==0){
		   str="停用";  
	   }else{
		   str="启用";  
	   }
	   
	   swal({
        title: "您确定要"+str+"该条邮件配置？",
        text: "请谨慎操作！",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        cancelButtonText: "取消",
        confirmButtonText: "确定",
        closeOnConfirm: false
    },
    function() {
    	managerSmsSendConfigAction.enableSms(id,flag,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "操作成功！",type: "success"});
				smsSendConfigPage.findConfigControl();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("操作失败.");
			}
	    });
    });
};

SmsSendConfigPage.prototype.layerClose=function(index){
	layer.close(index);
	smsSendConfigPage.findConfigControl();
}