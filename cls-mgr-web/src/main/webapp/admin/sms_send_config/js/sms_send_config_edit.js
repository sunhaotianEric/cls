function SmsSendConfigPage(){}
var smsSendConfigPage = new SmsSendConfigPage();

/**
 * 查询参数
 */
smsSendConfigPage.queryParam = {
		id:null,
		apiType : null,
		apiUrl:null,
		userName:null,
		password:null,
		type:null,
		orders:null,
		enable:null,
		createTime:null,
		updateTime:null
};
smsSendConfigPage.param = {
// skipAwardZh:0
};

$(document).ready(function() {
	var id = smsSendConfigPage.param.id; // 获取查询的ID
	if (id != null) {
		smsSendConfigPage.editConfigControl(id);
	}

});

SmsSendConfigPage.prototype.selectByPrimary = function() {
	var id = $("#id").val();
	var apiType = $("#apiType").val();
	var apiUrl = $("#apiUrl").val();
	var userName = $("#userName").val();
	var password = $("#password").val();
	var type = $("#type").val();
	var orders = $("#orders").val();
	var enable = $("#enable").val();
	var va = /^[0-9]*$/;// 验证正小数

	smsSendConfigPage.queryParam.id = id;
	smsSendConfigPage.queryParam.apiType = apiType;
	smsSendConfigPage.queryParam.apiUrl = apiUrl;
	smsSendConfigPage.queryParam.userName = userName;
	smsSendConfigPage.queryParam.password = password;
	smsSendConfigPage.queryParam.type = type;
	smsSendConfigPage.queryParam.orders = orders;
	smsSendConfigPage.queryParam.enable = enable;
	if (!va.test(orders)) {
		swal("请输入排序数字!");
		$("#orders").focus();
		return;
	}
	if (smsSendConfigPage.queryParam.id != null
			|| smsSendConfigPage.queryParam.id != "") {
		managerSmsSendConfigAction.updateSmsSendConfig(
				smsSendConfigPage.queryParam, function(r) {
					if (r[0] == "ok") {
						swal({
							title : "修改成功！",
							text : "已成功修改数据",
							type : "success"
						},
								function() {
									var index = parent.layer
											.getFrameIndex(window.name); // 获取窗口索引
									parent.smsSendConfigPage
											.layerClose(index);
								})
					} else {
						swal(r[1]);
					}
				});
	} else {
		managerSmsSendConfigAction.saveSmsSendConfig(
				smsSendConfigPage.queryParam, function(r) {
					if (r[0] == "ok") {
						swal({
							title : "保存成功！",
							text : "已成功保存数据",
							type : "success"
						},
								function() {
									var index = parent.layer
											.getFrameIndex(window.name); // 获取窗口索引
									parent.smsSendConfigPage
											.layerClose(index);
								})
					} else {
						swal(r[1]);
					}
				});
	}
	smsSendConfigPage.queryParam = {};
}

/**
 * 保存信息
 */
SmsSendConfigPage.prototype.saveData = function() {
	var id = $("#id").val();
	var apiType = $("#apiType").val();
	var apiUrl = $("#apiUrl").val();
	var userName = $("#userName").val();
	var password = $("#password").val();
	var type = $("#type").val();
	var enable = $("#enable").val();
	var orders = $("#orders").val();

	var va = /^[0-9]*$/;// 验证正小数
	if (apiType == null || apiType.trim() == "") {
		swal("请输入接口类型!");
		$("#apiType").focus();
		return;
	}
	if (apiUrl == null || apiUrl.trim() == "") {
		swal("请输入接口url地址!");
		$("#mailPort").focus();
		return;
	}
	if (userName == null || userName.trim() == "") {
		swal("请输入用户名!");
		$("#userName").focus();
		return;
	}
	if (password == null || password.trim() == "") {
		swal("请输入密码!");
		$("#password").focus();
		return;
	}
	if (type == null || type.trim() == "") {
		swal("请输入发送类型!");
		$("#type").focus();
		return;
	}
	if (enable == null || enable.trim() == "") {
		swal("请输入状态!");
		$("#enable").focus();
		return;
	}
	if (!va.test(orders)) {
		swal("请输入排序数字!");
		$("#orders").focus();
		return;
	}

	var smsSendConfig = {};
	smsSendConfig.id = id;
	smsSendConfig.apiType = apiType;
	smsSendConfig.apiUrl = apiUrl;
	smsSendConfig.userName = userName;
	smsSendConfig.password = password;
	smsSendConfig.type = type;
	smsSendConfig.enable = enable;
	smsSendConfig.orders = orders;
	managerSmsSendConfigAction.saveSmsSendConfig(smsSendConfig, function(
			r) {
		if (r[0] == "ok") {
			swal({
				title : "保存成功！",
				text : "已成功新增数据",
				type : "success"
			}, function() {
				var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
				parent.smsSendConfigPage.layerClose(index);
			})
		} else {
			swal(r[1]);
		}
	});
};

/**
 * 编辑时赋值
 */
SmsSendConfigPage.prototype.editConfigControl = function(id) {
	managerSmsSendConfigAction.selectSmsSendConfig(id, function(r) {
		if (r[0] != null && r[0] == "ok") {
			$("#id").val(r[1].id);
			$("#apiType").val(r[1].apiType);
			$("#apiUrl").val(r[1].apiUrl);
			$("#userName").val(r[1].userName);
			$("#password").val(r[1].password);
			$("#type").val(r[1].type);
			$("#enable").val(r[1].enable);
			$("#orders").val(r[1].orders);
		} else if (r[0] != null && r[0] == "error") {
			showErrorDlg(jQuery(document.body), r[1]);
		} else {
			showErrorDlg(jQuery(document.body), "获取数据失败.");
		}
	});
};
