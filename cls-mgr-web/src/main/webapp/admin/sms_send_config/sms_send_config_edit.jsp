<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
	String id=request.getParameter("id");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>短信发送配置编辑</title>
        <link rel="shortcut icon" href="favicon.ico">
        <jsp:include page="/admin/include/include.jsp"></jsp:include>
        <!-- dwr -->
		<script type='text/javascript' src='<%=path%>/dwr/interface/managerSmsSendConfigAction.js'></script>
		<script type="text/javascript" src="<%=path%>/admin/sms_send_config/js/sms_send_config_edit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
        <script type="text/javascript">
        	smsSendConfigPage.param.id=<%=id%>;
        </script>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeIn">
		<form action="javascript:void(0)">
		<input type="hidden" name="id" id="id" value="<%=id%>">
			<div class="row">
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">接口类型：</span>
						<input type="text" name="apiType" id="apiType" class="form-control" />
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">接口url地址：</span>
						<input type="text" name="apiUrl" id="apiUrl" class="form-control" />
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">用户名：</span>
						<input type="text" id="userName" name="userName" class="form-control" />
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">密码：</span>
						<input type="text" id="password" name="password" class="form-control" />
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">发送类型：</span>
						<input type="text" id="type" class="form-control" name="type" />
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">排序：</span>
						<input type="text" id="orders" class="form-control" name="orders" />
					</div>
				</div>
				<div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">状态：</span>
                        <select class="ipt form-control" id="enable" name="enable">
                            <option value="0" selected="selected">停用</option>
                            <option value="1">启用</option>
                        </select>
                    </div>
                </div>
				<div class="col-sm-12">
						<div class="col-sm-6 text-center">
							<button type="button" class="btn btn-w-m btn-white"
								onclick="smsSendConfigPage.selectByPrimary()">提 交</button>
							<input type="reset" class="btn btn-w-m btn-white" value="重 置" />
						</div>
				</div>
			</div>
		</form>
	</div>
	<script type="text/javascript" src="<%=path%>/admin/common/common.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>

</body>
</html>