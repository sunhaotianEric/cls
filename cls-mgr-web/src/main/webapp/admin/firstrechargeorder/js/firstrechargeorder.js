function FirstRechargeorderPage(){}
var firstRechargeorderPage = new FirstRechargeorderPage();


/**
 * 查询参数
 */
firstRechargeorderPage.queryParam = {
		bizSystem : null,
		userName : null,
		isQueryFirstTime : true,
		createdDateStart : null,
		createdDateEnd : null
};

//分页参数
firstRechargeorderPage.pageParam = {
		    pageNo : 1,
		    pageSize : defaultPageSize
};

$(document).ready(function() {
	$("#createdDateStart").val(getClsStartTime().format("yyyy-MM-dd hh:mm:ss"));
	$("#createdDateEnd").val(getClsEndTime().format("yyyy-MM-dd hh:mm:ss"));
	firstRechargeorderPage.initTableData(); 
	
});

FirstRechargeorderPage.prototype.initTableData = function(){
	firstRechargeorderPage.queryParam.createdDateStart=new Date($("#createdDateStart").val());
	firstRechargeorderPage.queryParam.createdDateEnd=new Date($("#createdDateEnd").val());
	firstRechargeorderPage.table = $('#rechargeorderTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"ajax":function (data, callback, settings) {
			firstRechargeorderPage.pageParam.pageSize = data.length;//页面显示记录条数
			firstRechargeorderPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerUserAction.userFirstRechargeList(firstRechargeorderPage.queryParam,firstRechargeorderPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败.");
				}
				callback(returnData);
			});
		},
		"columns": [
                    {
                    	"data": "bizSystemName", 
                    	"bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false
            		},
	                {
		            	"data": "userName",
		            	render: function(data, type, row, meta) {
		            		return "<a href='javascript:void(0)' style='color:blue;text-decoration:underline;'  onclick='commonPage.showUserMsgPage(\""+data+"\","+row.id+")'>"+ row.userName +"</a>";
	            		}
		            },
		            {"data": "firstRechargeTimeStr"},
		            {"data": "accountValue"}
	            ]
	}).api(); 
};

/**
 * 查询数据
 */
FirstRechargeorderPage.prototype.findRechargeorder = function(dateRange){
	if(dateRange != undefined && dateRange != null && dateRange != ''){
		commonPage.setDateRangeTime(dateRange, "createdDateStart", "createdDateEnd",2);	
	}
	
	
	var userName = $("#userName").val();
	var bizSystem = $("#bizSystem").val();
	var createdDateStart = $("#createdDateStart").val();
	var createdDateEnd = $("#createdDateEnd").val();
	if(userName == ""){
		firstRechargeorderPage.queryParam.userName = null;
	}else{
		firstRechargeorderPage.queryParam.userName = userName;
	}
	
	
	if(bizSystem == ""){
		firstRechargeorderPage.queryParam.bizSystem = null;
	}else{
		firstRechargeorderPage.queryParam.bizSystem = bizSystem;
	}
	if(createdDateStart == ""){
		firstRechargeorderPage.queryParam.createdDateStart = null;
	}else{
		firstRechargeorderPage.queryParam.createdDateStart =new Date(createdDateStart);
	}
	if(createdDateEnd == ""){
		firstRechargeorderPage.queryParam.createdDateEnd = null;
	}else{
		firstRechargeorderPage.queryParam.createdDateEnd =new Date(createdDateEnd);
	}
	firstRechargeorderPage.table.ajax.reload();
};











