<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>首冲记录</title>
<jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
<body>
	<div class="animated fadeIn">
		<div class="ibox-content ibox">
			<div class="row m-b-sm m-t-sm">
				<form class="form-horizontal" id="rechargeorderForm">
					<div class="row">
						<c:choose>
                           <c:when test="${admin.bizSystem=='SUPER_SYSTEM'}">
                           <div class="col-sm-3">
                               
	                                <div class="input-group m-b">
	                                    <span class="input-group-addon">商户：</span>
	                                     <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
	                                </div>
                                
                               </div>
	                           <div class="col-sm-5">
							        <div class="input-group m-b">
							            <span class="input-group-addon">时间始终</span>
							            <div class="input-daterange input-group" >
							                <input class="form-control layer-date" placeholder="开始日期" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" name="createdDateStart" id="createdDateStart">
							                <span class="input-group-addon">到</span>
							                <input class="form-control layer-date" placeholder="结束日期" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" name="createdDateEnd" id="createdDateEnd"></div>
							        </div>
							    </div>
							    <div class="col-sm-2 pl0">
							        <div class="form-group nomargin text-left">
							            <button type="button" class="btn btn-xs btn-info" onclick="firstRechargeorderPage.findRechargeorder(1)">今 天</button>
							            <button type="button" class="btn btn-xs btn-danger" onclick="firstRechargeorderPage.findRechargeorder(-1)">昨 天</button>
							        	<button type="button" class="btn btn-xs btn-warning" onclick="firstRechargeorderPage.findRechargeorder(-7)">最近7天</button><br/>
							        	<button type="button" class="btn btn-xs btn-success" onclick="firstRechargeorderPage.findRechargeorder(30)">本 月</button>
							        	<button type="button" class="btn btn-xs btn-primary" onclick="firstRechargeorderPage.findRechargeorder(-30)">上 月</button>
							        </div>
							    </div>
                           </c:when>
                           <c:otherwise>
                           <div class="col-sm-5">
							        <div class="input-group m-b">
							            <span class="input-group-addon">时间始终</span>
							            <div class="input-daterange input-group" >
							                <input class="form-control layer-date" placeholder="开始日期" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" name="createdDateStart" id="createdDateStart">
							                <span class="input-group-addon">到</span>
							                <input class="form-control layer-date" placeholder="结束日期" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" name="createdDateEnd" id="createdDateEnd"></div>
							        </div>
							    </div>
							       <div class="col-sm-5" style="padding-left:0;">
								       <div class="form-group nomargin text-left">
								            <button type="button" class="btn btn-xs btn-info" onclick="firstRechargeorderPage.findRechargeorder(1)">今 天</button>
								            <button type="button" class="btn btn-xs btn-danger" onclick="firstRechargeorderPage.findRechargeorder(-1)">昨 天</button>
								        	<button type="button" class="btn btn-xs btn-warning" onclick="firstRechargeorderPage.findRechargeorder(-7)">最近7天</button><br/>
								        	<button type="button" class="btn btn-xs btn-success" onclick="firstRechargeorderPage.findRechargeorder(30)">本 月</button>
								        	<button type="button" class="btn btn-xs btn-primary" onclick="firstRechargeorderPage.findRechargeorder(-30)">上 月</button>
								      </div>
								   </div>
                           </c:otherwise>
                           
                           </c:choose>

						<div class="col-sm-2">
							<div class="form-group nomargin text-right">
								<button type="button" class="btn btn-outline btn-default"
									onclick="firstRechargeorderPage.findRechargeorder()">
									<i class="fa fa-search"></i>&nbsp;查 询
								</button>
							</div>
						</div>
					</div>
					
				</form>
			</div>
			<div class="jqGrid_wrapper">
				<div class="ui-jqgrid ">
					<div class="ui-jqgrid-view ">
						<div class="ui-jqgrid-hdiv">
							<div class="ui-jqgrid-hbox" style="padding-right: 0">
								<table id="rechargeorderTable"
									class="ui-jqgrid-htable ui-common-table table table-bordered">
									<thead>
										<tr class="ui-jqgrid-labels">
										    <th class="ui-th-column ui-th-ltr">商户</th>
											<th class="ui-th-column ui-th-ltr">用户名</th>
											<th class="ui-th-column ui-th-ltr">首充时间</th>
											<th class="ui-th-column ui-th-ltr">到帐金额</th>
											<!-- <th class="ui-th-column ui-th-ltr">充值赠送</th> -->
										</tr>
									</thead>

								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- js -->
	<script type="text/javascript"
		src="<%=path%>/admin/firstrechargeorder/js/firstrechargeorder.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
	<!-- dwr -->
		<script type='text/javascript' src='<%=path%>/dwr/interface/managerUserAction.js'></script>
</body>

</html>

