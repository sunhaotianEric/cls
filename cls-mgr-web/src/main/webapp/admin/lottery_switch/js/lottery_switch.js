function LotterySwitchPage(){}
var lotterySwitchPage = new LotterySwitchPage();

lotterySwitchPage.lotterySwitchtingParam = {
};

$(document).ready(function() {
	lotterySwitchPage.getAllLotterySwitchMsg();
});

/**
 * 保存彩种配置信息
 */
LotterySwitchPage.prototype.saveLotterySwitchMsg = function(){
	// 获取所有的select的name值和对应的option值.
	let updateArray =[];
	// 获取所有select中的name值删除name为bizSystem的值
	$("select").each(function(){
		lotterySwitch = {};
		lotterySwitch.lotteryType = $(this).attr("name");
		lotterySwitch.lotteryStatus = $(this).find("option:selected").val();
		updateArray.push(lotterySwitch)
	})
	// 根据业务系统更新彩种开关数据.
	managerLotterySwitchAction.updateLotterySwitchsByBizSystem(updateArray, currentUser.bizSystem, function(r) {
		if (r[0] != null && r[0] == "ok") {
			swal("更新彩种信息数据成功");
		} else if (r[0] != null && r[0] == "error") {
			swal(r[1]);
		} else {
			swal("查询数据失败.");
		}
	});
	
};
	
/**
 * 获取彩种配置信息
 */
LotterySwitchPage.prototype.getAllLotterySwitchMsg = function(){
	
	// 根据系统获取出彩种开关信息.
	managerLotterySwitchAction.getAllLotterySwitchsByBizSystem(currentUser.bizSystem,function(r){
		if (r[0] != null && r[0] == "ok") {
			lotterySwitchPage.toShowLotterySwitchMsg(r[1]);
		}else if (r[0] != null && r[0] == "error") {
			swal(r[1]);
		}else{
			swal("获取系统信息数据失败.");
		}
	});
};


/**
 * 展示彩种配置信息
 */
LotterySwitchPage.prototype.toShowLotterySwitchMsg = function(lotterySwitchMsg){
	let array =[];
	let list ={};
	// 获取所有select中的name值
	$("select").each(function(){
		list = {}
		list.name = $(this).attr("name")
		list.id = $(this).attr("id")
		array.push(list)
	})
	// for循环出.查询出来的结果.并且赋值到select中为选中的状态.
	for (var i = 0; i < lotterySwitchMsg.length; i++) {
		var lotterySwitch = lotterySwitchMsg[i];
		for (var key = 0; key < array.length; key++) {
			var name1 = array[key];
			// 判断如果彩种类型和select中name相同,就设置选中的状态.
			if (lotterySwitch.lotteryType == name1.name) {
				$("#" + name1.id + "").val(lotterySwitch.lotteryStatus)
			}
		}
	}
};