function LotterySwitchPage(){}
var lotterySwitchPage = new LotterySwitchPage();

lotterySwitchPage.lotterySettingParam = {

};

$(document).ready(function() {
	
});

/**
 * 保存彩种配置信息
 */
LotterySwitchPage.prototype.saveLotterySwitchMsg = function(){
	// 获取所有的select的name值和对应的option值.
	let updateArray =[];
	// 获取所有input中的id值
	$(".lottery_closing_time input").each(function(){
		lotterySwitch = {};
		lotterySwitch.lotteryType = $(this).attr("id").toUpperCase();
		lotterySwitch.closingTime = $(this).val();
		lotterySwitch.lotteryStatus = $(this).attr('data-switch')
		updateArray.push(lotterySwitch);
	})
	var bizSystem  = $("#bizSystem").val();
	if(bizSystem == null || bizSystem == ""){
    	swal("请选择业务系统！");
    	return ;
    }
	
	// 根据业务系统更新彩种开关数据.
	managerLotterySwitchAction.updateLotterySwitchsByBizSystem(updateArray,bizSystem,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal("更新彩种信息数据成功");
		}else if (r[0] != null && r[0] == "error") {
			swal(r[1]);
		}else{
			swal( "查询数据失败.");
		}
	});
	
};
	
/**
 * 获取彩种配置信息
 */
LotterySwitchPage.prototype.getAllLotterySwitchMsg = function(){
        var bizSystem  = $("#bizSystem").val();
        if(bizSystem == null || bizSystem == ""){
        	swal("请选择业务系统！");
        	return ;
        }
        // 根据系统获取出彩种开关信息.
    	managerLotterySwitchAction.getAllLotterySwitchsByBizSystem(bizSystem,function(r){
    		if (r[0] != null && r[0] == "ok") {
    			lotterySwitchPage.toShowLotterySwitchMsg(r[1]);
    		}else if (r[0] != null && r[0] == "error") {
    			swal(r[1]);
    		}else{
    			swal("获取系统信息数据失败.");
    		}
    	});

};


/**
 * 展示彩种配置信息
 */
LotterySwitchPage.prototype.toShowLotterySwitchMsg = function(lotterySwitchMsg){
	// 如果查询出来没有数据,那么就全部状态标记为请选择
	if(lotterySwitchMsg.length == 0){
		let array =[];
		let list ={};
		$("input").each(function(){
			list = {}
			list.name = $(this).attr("id").toUpperCase()
			list.id = $(this).attr("id").toUpperCase();
			array.push(list)
			
		})
		for (var key = 0; key < array.length; key++) {
			var name1 = array[key];
			$("#" + name1.id + "").val("")
		}
		swal("该系统暂无彩种开关.");
		
	} else {
		let array =[];
		let list ={};
		// 获取所有select中的name值
		$("input").each(function(){
			list = {}
			list.name = $(this).attr("id").toUpperCase()
			list.id = $(this).attr("id")
			array.push(list)
		})
		// for循环出.查询出来的结果.并且赋值到input中为选中的状态.
		for (var i = 0; i < lotterySwitchMsg.length; i++) {
			var lotterySwitch = lotterySwitchMsg[i];
			for (var key = 0; key < array.length; key++) {
				var name1 = array[key];
				// 判断如果彩种类型和select中name相同,就设置选中的状态.
				if (lotterySwitch.lotteryType == name1.name) {
					$("#" + name1.id + "").val(lotterySwitch.closingTime)
					$("#" + name1.id + "").attr('data-switch', lotterySwitch.lotteryStatus)
				}
			}
		}
	}
};