<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>商户彩种开关设置</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-sm-3">
            <div class="input-group m-b">
                <span class="input-group-addon">商户</span>
                <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group nomargin text-left input-group-btn">
                <button type="button" class="btn btn-outline btn-default btn-kj btn-white" onclick="lotterySwitchPage.getAllLotterySwitchMsg()"><i class="fa fa-search"></i>&nbsp;搜索</button>
            </div>
        </div>
        <div class="col-sm-7">
        </div>
    </div>
    <div class="tabs-container ibox">
        <ul class="nav nav-tabs">
            <li class="active">
                <a data-toggle="tab" href="#tab-1" aria-expanded="true">时时彩设置</a></li>
            <li class="">
                <a data-toggle="tab" href="#tab-3" aria-expanded="false">分分彩设置</a></li>
            <li class="">
                <a data-toggle="tab" href="#tab-2" aria-expanded="false">11选5</a></li>
            <li class="">
                <a data-toggle="tab" href="#tab-4" aria-expanded="false">快3设置</a></li>
            <li class="">
                <a data-toggle="tab" href="#tab-6" aria-expanded="false">低频彩</a></li>
            <li class="">
                <a data-toggle="tab" href="#tab-8" aria-expanded="false">PK10</a></li>
            <li class="">
                <a data-toggle="tab" href="#tab-9" aria-expanded="false">幸运28</a></li>
            <li class="">
                <a data-toggle="tab" href="#tab-10" aria-expanded="false">六合彩</a></li>
        </ul>
        <div class="tab-content lottery_closing_time">
            <div id="tab-1" class="tab-pane active">
                <div class="panel-body">
                    <div class="col-sm-4">
                        <div class="input-group m-b">
                            <span class="input-group-addon">重庆时时彩</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="cqssc">
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">江西时时彩</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="jxssc">
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">黑龙江时时彩</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="hljssc">
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">新疆时时彩</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="xjssc" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">天津时时彩</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="tjssc" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">三分时时彩</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="sfssc" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">五分时时彩</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="wfssc" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">十分时时彩</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="shfssc" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">老重庆时时彩</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="lcqssc" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">江苏时时彩</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="jsssc" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">北京时时彩</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="bjssc" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">广东时时彩</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="gdssc" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">四川时时彩</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="scssc" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">上海时时彩</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="shssc" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">山东时时彩</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="sdssc" >
                        </div>
                    </div>
                </div>
            </div>
            <div id="tab-2" class="tab-pane">
                <div class="panel-body">
                    <div class="col-sm-4">
                        <div class="input-group m-b">
                            <span class="input-group-addon">广东11选5</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="gdsyxw" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">山东11选5</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="sdsyxw" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">江西11选5</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="jxsyxw" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">重庆11选5</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="cqsyxw" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">福建11选5</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="fjsyxw" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">三分11选5</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="sfsyxw" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">五分11选5</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="wfsyxw" >
                        </div>

                    </div>
                </div>
            </div>
            <div id="tab-3" class="tab-pane">
                <div class="panel-body">
                    <div class="col-sm-4">
                        <div class="input-group m-b">
                            <span class="input-group-addon">幸运分分彩</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="jlffc" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">韩国1.5分彩</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="hgffc" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">新加坡2分彩</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="xjplfc" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">台湾5分彩</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="twwfc" >
                        </div>
                    </div>
                </div>
            </div>
            <div id="tab-4" class="tab-pane">
                <div class="panel-body">
                    <div class="col-sm-4">
                        <div class="input-group m-b">
                            <span class="input-group-addon">江苏快3</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="jsks" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">安徽快3</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="ahks" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">吉林快3</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="jlks" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">湖北快3</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="hbks" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">北京快3</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="bjks" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">幸运快3</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="jyks" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">广西快3</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="gxks" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">甘肃快3</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="gsks" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">上海快3</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="shks" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">三分快3</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="sfks" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">五分快3</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="wfks" >
                        </div>
                    </div>
                </div>
            </div>
            <div id="tab-6" class="tab-pane">
                <div class="panel-body">
                    <div class="col-sm-4">
                        <div class="input-group m-b">
                            <span class="input-group-addon">上海时时乐</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="shssldpc" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">福彩3D</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="fcsddpc" >
                        </div>
                    </div>
                </div>
            </div>
            <div id="tab-8" class="tab-pane">
                <div class="panel-body">
                    <div class="col-sm-4">
                        <div class="input-group m-b">
                            <span class="input-group-addon">北京PK10</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="bjpk10" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">极速PK10</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="jspk10" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">幸运飞艇</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="xyft" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">三分PK10</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="sfpk10" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">五分PK10</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="wfpk10" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">十分PK10</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="shfpk10" >
                        </div>
                    </div>
                </div>
            </div>
            <div id="tab-9" class="tab-pane">
                <div class="panel-body">
                    <div class="col-sm-4">
                        <div class="input-group m-b">
                            <span class="input-group-addon">幸运28</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="xyeb" >
                        </div>
                    </div>
                </div>
            </div>
            <div id="tab-10" class="tab-pane">
                <div class="panel-body">
                    <div class="col-sm-4">
                        <div class="input-group m-b">
                            <span class="input-group-addon">六合彩</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="lhc" >
                        </div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">幸运六合彩</span>
                            <input type="number" placeholder="请输入封盘时间（秒）" class="ipt form-control" id="xylhc" >
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-12 ibox">
    <div class="row">
        <div class="col-sm-6 text-center">
            <button type="button" class="btn btn-w-m btn-white" onclick="lotterySwitchPage.saveLotterySwitchMsg()">保 存</button>
        </div>
        <!-- <div class="col-sm-6 text-center">
            <button type="button" class="btn btn-w-m btn-white" onclick="lotterySwitchPage.refreshCache()">一键更新缓存</button>
        </div> -->
    </div>
</div>
</div>
<!-- js -->
<script type="text/javascript" src="<%=path%>/admin/lottery_switch/js/lottery_closing_time_bizsystem.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerLotterySwitchAction.js'></script>
<script type='text/javascript' src='<%=path%>/dwr/interface/managerBizSystemConfigAction.js'></script>
</body>
</html>

