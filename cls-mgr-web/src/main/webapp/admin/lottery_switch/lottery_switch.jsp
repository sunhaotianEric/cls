<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>  
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <title>全局彩种开关设置</title>
        <jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
 <body class="gray-bg">
         <div class="wrapper wrapper-content animated fadeIn">
            <div class="tabs-container ibox">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a data-toggle="tab" href="#tab-1" aria-expanded="true">时时彩设置</a></li>
                    <li class="">
                        <a data-toggle="tab" href="#tab-3" aria-expanded="false">分分彩设置</a></li>
                    <li class="">
                        <a data-toggle="tab" href="#tab-2" aria-expanded="false">11选5</a></li>
                    <li class="">
                        <a data-toggle="tab" href="#tab-4" aria-expanded="false">快3设置</a></li>
                    <li class="">
                        <a data-toggle="tab" href="#tab-6" aria-expanded="false">低频彩</a></li>
                    <li class="">
                        <a data-toggle="tab" href="#tab-8" aria-expanded="false">PK10</a></li>
                    <li class="">
                        <a data-toggle="tab" href="#tab-9" aria-expanded="false">幸运28</a></li>
                    <li class="">
                        <a data-toggle="tab" href="#tab-10" aria-expanded="false">六合彩</a></li>
                </ul>
                <div class="tab-content">
                    <div id="tab-1" class="tab-pane active">
                        <div class="panel-body">
                            <div class="col-sm-4">
                                <div class="input-group m-b">
                                            <span class="input-group-addon">重庆时时彩</span>
                                            <select class="ipt form-control" id='cqssc' name="CQSSC">
                                           		<option value="">请选择</option>
                                                <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                                <option value="3">暂停销售</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">江西时时彩</span>
                                            <select class="ipt form-control" id='jxssc' name="JXSSC">
                                            	<option value="">请选择</option>
                                                <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                                <option value="3">暂停销售</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">黑龙江时时彩</span>
                                            <select class="ipt form-control" id='hljssc' name="HLJSSC">
                                            	<option value="">请选择</option>
                                                <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                                <option value="3">暂停销售</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">新疆时时彩</span>
                                            <select class="ipt form-control" id='xjssc' name="XJSSC">
                                                <option value="1" selected="selected">开启</option>
                                                <option value="">请选择</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                                <option value="3">暂停销售</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">天津时时彩</span>
                                            <select class="ipt form-control" id='tjssc' name="TJSSC">
                                            	<option value="">请选择</option>
                                                <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                                <option value="3">暂停销售</option></select>
                                                
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">三分时时彩</span>
                                            <select class="ipt form-control" id='sfssc' name="SFSSC">
                                            	<option value="">请选择</option>
                                                <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                                <option value="3">暂停销售</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">五分时时彩</span>
                                            <select class="ipt form-control" id='wfssc' name="WFSSC">
                                            	<option value="">请选择</option>
                                                <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                                <option value="3">暂停销售</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">十分时时彩</span>
                                            <select class="ipt form-control" id='shfssc' name="SHFSSC">
                                            	<option value="">请选择</option>
                                                <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                                <option value="3">暂停销售</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">老重庆时时彩</span>
                                            <select class="ipt form-control" id='lcqssc' name="LCQSSC">
                                            	<option value="">请选择</option>
                                                <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                                <option value="3">暂停销售</option></select>
                                        </div>

                                <div class="input-group m-b">
                                    <span class="input-group-addon">江苏时时彩</span>
                                    <select class="ipt form-control" id='jsssc' name="JSSSC">
                                        <option value="">请选择</option>
                                        <option value="1">开启</option>
                                        <option value="0">关闭</option>
                                        <option value="2">维护</option>
                                        <option value="3">暂停销售</option></select>
                                </div>


                                <div class="input-group m-b">
                                    <span class="input-group-addon">北京时时彩</span>
                                    <select class="ipt form-control" id='bjssc' name="BJSSC">
                                        <option value="">请选择</option>
                                        <option value="1">开启</option>
                                        <option value="0">关闭</option>
                                        <option value="2">维护</option>
                                        <option value="3">暂停销售</option></select>
                                </div>

                                <div class="input-group m-b">
                                    <span class="input-group-addon">广东时时彩</span>
                                    <select class="ipt form-control" id='gdssc' name="GDSSC">
                                        <option value="">请选择</option>
                                        <option value="1">开启</option>
                                        <option value="0">关闭</option>
                                        <option value="2">维护</option>
                                        <option value="3">暂停销售</option></select>
                                </div>


                                <div class="input-group m-b">
                                    <span class="input-group-addon">四川时时彩</span>
                                    <select class="ipt form-control" id='scssc' name="SCSSC">
                                        <option value="">请选择</option>
                                        <option value="1">开启</option>
                                        <option value="0">关闭</option>
                                        <option value="2">维护</option>
                                        <option value="3">暂停销售</option></select>
                                </div>


                                <div class="input-group m-b">
                                    <span class="input-group-addon">上海时时彩</span>
                                    <select class="ipt form-control" id='shssc' name="SHSSC">
                                        <option value="">请选择</option>
                                        <option value="1">开启</option>
                                        <option value="0">关闭</option>
                                        <option value="2">维护</option>
                                        <option value="3">暂停销售</option></select>
                                </div>


                                <div class="input-group m-b">
                                    <span class="input-group-addon">山东时时彩</span>
                                    <select class="ipt form-control" id='sdssc' name="SDSSC">
                                        <option value="">请选择</option>
                                        <option value="1">开启</option>
                                        <option value="0">关闭</option>
                                        <option value="2">维护</option>
                                        <option value="3">暂停销售</option></select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="tab-2" class="tab-pane">
                        <div class="panel-body">
                            <div class="col-sm-4">
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">广东11选5</span>
                                            <select class="ipt form-control" id='gdsyxw' name="GDSYXW">
                                            	<option value="">请选择</option>
                                                <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                                <option value="3">暂停销售</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">山东11选5</span>
                                            <select class="ipt form-control" id='sdsyxw' name="SDSYXW">
                                            	<option value="">请选择</option>
                                                <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                                <option value="3">暂停销售</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">江西11选5</span>
                                            <select class="ipt form-control" id='jxsyxw' name="JXSYXW">
                                            	<option value="">请选择</option>
                                                <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                                <option value="3">暂停销售</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">重庆11选5</span>
                                            <select class="ipt form-control" id='cqsyxw' name="CQSYXW">
                                            	<option value="">请选择</option>
                                                <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                                <option value="3">暂停销售</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">福建11选5</span>
                                            <select class="ipt form-control" id='fjsyxw' name="FJSYXW">
                                            	<option value="">请选择</option>
                                                <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                                <option value="3">暂停销售</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">三分11选5</span>
                                            <select class="ipt form-control" id='sfsyxw' name="SFSYXW">
                                           		<option value="">请选择</option>
                                                <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                                <option value="3">暂停销售</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">五分11选5</span>
                                            <select class="ipt form-control" id='wfsyxw' name="WFSYXW">
                                            	<option value="">请选择</option>
                                                <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                                <option value="3">暂停销售</option></select>
                                        </div>
                                        
                                    </div>
                        </div>
                    </div>
                    <div id="tab-3" class="tab-pane">
                        <div class="panel-body">
                            <div class="col-sm-4">
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">幸运分分彩</span>
                                            <select class="ipt form-control" id='jlffc' name="JLFFC">
                                            	<option value="">请选择</option>
                                                <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                                <option value="3">暂停销售</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">韩国1.5分彩</span>
                                            <select class="ipt form-control" id='hgffc' name="HGFFC">
                                            	<option value="">请选择</option>
                                                <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                                <option value="3">暂停销售</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">新加坡2分彩</span>
                                            <select class="ipt form-control" id='xjplfc' name="XJPLFC">
                                            	<option value="">请选择</option>
                                                <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                                <option value="3">暂停销售</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">台湾5分彩</span>
                                            <select class="ipt form-control" id='twwfc' name="TWWFC">
                                            	<option value="">请选择</option>
                                                <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                                <option value="3">暂停销售</option></select>
                                        </div>
                                    </div>
                        </div>
                    </div>
                    <div id="tab-4" class="tab-pane">
                        <div class="panel-body">
                            <div class="col-sm-4">
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">江苏快3</span>
                                            <select class="ipt form-control" id='jsks' name="JSKS">
                                            	<option value="">请选择</option>
                                                <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                                <option value="3">暂停销售</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">安徽快3</span>
                                            <select class="ipt form-control" id='ahks' name="AHKS">
                                            	<option value="">请选择</option>
                                                <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                                <option value="3">暂停销售</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">吉林快3</span>
                                            <select class="ipt form-control" id='jlks' name="JLKS">
                                            	<option value="">请选择</option>
                                                <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                                <option value="3">暂停销售</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">湖北快3</span>
                                            <select class="ipt form-control" id='hbks' name="HBKS">
                                            	<option value="">请选择</option>
                                                <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                                <option value="3">暂停销售</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">北京快3</span>
                                            <select class="ipt form-control" id='bjks' name="BJKS">
                                            	<option value="">请选择</option>
                                                <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                                <option value="3">暂停销售</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">幸运快3</span>
                                            <select class="ipt form-control" id='jyks' name="JYKS">
                                            	<option value="">请选择</option>
                                                <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                                <option value="3">暂停销售</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">广西快3</span>
                                            <select class="ipt form-control" id='gxks' name="GXKS">
                                            	<option value="">请选择</option>
                                                <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                                <option value="3">暂停销售</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">甘肃快3</span>
                                            <select class="ipt form-control" id='gsks' name="GSKS">
                                            	<option value="">请选择</option>
                                                <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                                <option value="3">暂停销售</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">上海快3</span>
                                            <select class="ipt form-control" id='shks' name="SHKS">
                                            	<option value="">请选择</option>
                                                <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                                <option value="3">暂停销售</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">三分快3</span>
                                            <select class="ipt form-control" id='sfks' name="SFKS">
                                            	<option value="">请选择</option>
                                                <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                                <option value="3">暂停销售</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">五分快3</span>
                                            <select class="ipt form-control" id='wfks' name="WFKS">
                                            	<option value="">请选择</option>
                                                <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                                <option value="3">暂停销售</option></select>
                                        </div>
                                    </div>
                        </div>
                    </div>
                    <div id="tab-6" class="tab-pane">
                        <div class="panel-body">
                            <div class="col-sm-4">
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">上海时时乐</span>
                                            <select class="ipt form-control" id='shssldpc' name="SHSSLDPC">
                                            	<option value="">请选择</option>
                                                <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                                <option value="3">暂停销售</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                             <span class="input-group-addon">福彩3D</span>
                                            <select class="ipt form-control" id='fc3ddpc' name="FCSDDPC">
                                            	<option value="">请选择</option>
                                                <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                                <option value="3">暂停销售</option></select>
                                        </div>
                                    </div>
                        </div>
                    </div>
                    <div id="tab-8" class="tab-pane">
                        <div class="panel-body">
                           <div class="col-sm-4">
                               <div class="input-group m-b">
                                   <span class="input-group-addon">北京PK10</span>
                                   <select class="ipt form-control" id='bjpk10' name="BJPK10">
                                       <option value="">请选择</option>
                                       <option value="1">开启</option>
                                       <option value="0">关闭</option>
                                       <option value="2">维护</option>
                                       <option value="3">暂停销售</option></select>
                               </div>
                               <div class="input-group m-b">
                                   <span class="input-group-addon">极速PK10</span>
                                   <select class="ipt form-control" id='jspk10' name="JSPK10">
                                       <option value="">请选择</option>
                                       <option value="1">开启</option>
                                       <option value="0">关闭</option>
                                       <option value="2">维护</option>
                                       <option value="3">暂停销售</option></select>
                               </div>
                               <div class="input-group m-b">
                                   <span class="input-group-addon">幸运飞艇</span>
                                   <select class="ipt form-control" id='xyft' name="XYFT">
                                       <option value="">请选择</option>
                                       <option value="1">开启</option>
                                       <option value="0">关闭</option>
                                       <option value="2">维护</option>
                                       <option value="3">暂停销售</option></select>
                               </div>
                               <div class="input-group m-b">
                                   <span class="input-group-addon">三分PK10</span>
                                   <select class="ipt form-control" id='sfpk10' name="SFPK10">
                                       <option value="">请选择</option>
                                       <option value="1">开启</option>
                                       <option value="0">关闭</option>
                                       <option value="2">维护</option>
                                       <option value="3">暂停销售</option></select>
                               </div>
                               <div class="input-group m-b">
                                   <span class="input-group-addon">五分PK10</span>
                                   <select class="ipt form-control" id='wfpk10' name="WFPK10">
                                       <option value="">请选择</option>
                                       <option value="1">开启</option>
                                       <option value="0">关闭</option>
                                       <option value="2">维护</option>
                                       <option value="3">暂停销售</option></select>
                               </div>
                               <div class="input-group m-b">
                                   <span class="input-group-addon">十分PK10</span>
                                   <select class="ipt form-control" id='shfpk10' name="SHFPK10">
                                       <option value="">请选择</option>
                                       <option value="1">开启</option>
                                       <option value="0">关闭</option>
                                       <option value="2">维护</option>
                                       <option value="3">暂停销售</option></select>
                               </div>
                           </div>
                        </div>
                   </div>
                   <div id="tab-9" class="tab-pane">
                        <div class="panel-body">
                                    <div class="col-sm-4">
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">幸运28</span>
                                            <select class="ipt form-control" id='xyeb' name="XYEB">
		                                       	<option value="">请选择</option>
		                                        <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                      			<option value="3">暂停销售</option></select>
                                        </div>
                                        
                                    </div>
                        </div>
                   </div>
                   
                   <div id="tab-10" class="tab-pane">
                        <div class="panel-body">
                                    <div class="col-sm-4">
                                    	<div class="input-group m-b">
                                            <span class="input-group-addon">六合彩</span>
                                            <select class="ipt form-control" id='lhc' name="LHC">
                                            	<option value="">请选择</option>
                                                <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                                <option value="3">暂停销售</option></select>
                                        </div>
                                    	<div class="input-group m-b">
                                            <span class="input-group-addon">幸运六合彩</span>
                                            <select class="ipt form-control" id='xylhc' name="XYLHC">
		                                       	<option value="">请选择</option>
		                                        <option value="1">开启</option>
                                                <option value="0">关闭</option>
                                                <option value="2">维护</option>
                                       			<option value="3">暂停销售</option></select>
                                        </div>
                                    </div>
                        </div>
                   </div>
                   
                   
                </div>
            </div>
            <div class="col-sm-12 ibox">
                <div class="row">
                    <div class="col-sm-6 text-center">
                        <button type="button" class="btn btn-w-m btn-white" onclick="lotterySwitchPage.saveLotterySwitchMsg()">保 存</button>
                     </div>
                    <!-- <div class="col-sm-6 text-center">
						<button type="button" class="btn btn-w-m btn-white" onclick="lotterySwitchPage.refreshCache()">一键更新缓存</button>
					</div> -->
				</div>
                </div>
            </div>
        </div>
<!-- js -->
<script type="text/javascript" src="<%=path%>/admin/lottery_switch/js/lottery_switch.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerLotterySwitchAction.js'></script>
<script type='text/javascript' src='<%=path%>/dwr/interface/managerBizSystemConfigAction.js'></script>
</body>
</html>

