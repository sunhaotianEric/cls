<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>第三方支付设置</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
   <style type="text/css">
   		.dataTables_scrollBody{width:100% !important;} 
   </style>
</head>
<body>
 <!--   添加第三方支付 -->
    
   	<div class="animated fadeIn">
            <div class="ibox-content">
                <div class="row">
                    <form class="form-horizontal" id="thirdpayconfigForm">
                        <div class="row">
                        
                            <div class="col-sm-4">
                             
                            </div>
                            <div class="col-sm-8">
                                <div class="form-group  text-right">
                                
                                    <button type="button" class="btn btn-outline btn-default btn-add" onclick="thirdPayConfigPage.addThirdPayConfig()"><i class="fa fa-plus"></i>&nbsp;新增</button>
                                    </div>
                                     
                            </div>
                         </div>
                    </form>
                    </div>
                    <div class="jqGrid_wrapper">
                        <div class="ui-jqgrid ">
                            <div class="ui-jqgrid-view ">
                                <div class="ui-jqgrid-hdiv">
                                    <div class="ui-jqgrid-hbox" style="padding-right:0">
                                        <table id="thirdpayconfigTable" class="ui-jqgrid-htable ui-common-table table table-bordered">
                                            <thead>
                                                <tr class="ui-jqgrid-labels">
                                                    <th class="ui-th-column ui-th-ltr">序号</th>
                                                    <th class="ui-th-column ui-th-ltr" >第三方支付编码</th>
			                                        <th class="ui-th-column ui-th-ltr" >第三方支付名称</th>
                                                    <th class="ui-th-column ui-th-ltr" >接口版本号</th>
                                                    <th class="ui-th-column ui-th-ltr">加密类型</th>
                                                    <th class="ui-th-column ui-th-ltr">实际支付接口地址</th>
                                                    <th class="ui-th-column ui-th-ltr">转发支付网关的URL</th>
                                                    <th class="ui-th-column ui-th-ltr" >同步通知(无交易处理)</th>
                                                    <th class="ui-th-column ui-th-ltr" >异步通知(交易处理)</th>
                                                    <th class="ui-th-column ui-th-ltr" >post页面路径</th>
                                                    <th class="ui-th-column ui-th-ltr" >是否支持https</th>
                                                    <th class="ui-th-column ui-th-ltr" >启用状态</th>
                                                    <th class="ui-th-column ui-th-ltr">操作</th></tr>
                                            </thead>
                                            
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
     
    <!-- dwr -->
     <script type='text/javascript' src='<%=path%>/dwr/interface/managerThirdPayConfigAction.js'></script> 
     <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/thirdpayconfig/js/thirdpayconfig.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
</body>
</html>

