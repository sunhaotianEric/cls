<%@page import="com.team.lottery.system.SystemConfigConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	import="com.team.lottery.enums.EBankInfo" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>第三方支付设置</title>
<jsp:include page="/admin/include/include.jsp"></jsp:include>


<link rel="stylesheet"
	href="<%=path%>/resources/kindeditor-4.1.10/themes/default/default.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" />
<script charset="utf-8"
	src="<%=path%>/resources/kindeditor-4.1.10/kindeditor-min.js"></script>
<script charset="utf-8"
	src="<%=path%>/resources/kindeditor-4.1.10/lang/zh_CN.js"></script>
<%
	String id = request.getParameter("id"); //获取查询的商品ID
%>

</head>
<body>

	<div class="wrapper wrapper-content animated fadeIn">

		<form action="javascript:void(0)">
			<div class="row">
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">第三方支付编码：</span>
						 <%-- <cls:thirdPaySel name="chargeType" id="chargeType" options="class:ipt form-control" /> --%>
						 <input id="chargeType" name="chargeType" type="text" class="form-control"/>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">第三方支付名称：</span> <input type="hidden"
							id="id" /> <input type="text" value="" class="form-control"
							name="chargeDes" id="chargeDes">
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">接口版本号：</span> <input type="text"
							value="" class="form-control" name="interfaceVersion"
							id="interfaceVersion">
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">加密类型：</span> <input type="text"
							value="" class="form-control" name="signType" id="signType">
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">实际支付接口地址：</span> <input
							type="text" value="" class="form-control" name="address"
							id="address">
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">转发支付网关的URL：</span> <input
							type="text" value="" class="form-control" name="payUrl"
							id="payUrl">
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">同步通知(无交易处理)：</span> <input
							type="text" value="" class="form-control" name="notifyUrl"
							id="notifyUrl">
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">异步通知(交易处理)：</span> <input
							type="text" value="" class="form-control col-sm-8"
							name="returnUrl" id="returnUrl">
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">post页面路径：</span> <input
							type="text" value="" class="form-control col-sm-8"
							name="postUrl" id="postUrl">
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">是否支持https：</span> <select
							class="form-control" id="supportHttps">
							<option value="1">是</option>
							<option value="0">否</option>
						</select>
					</div>
					<div class="input-group m-b">
						<span class="input-group-addon">是否有效：</span> <select
							class="form-control" id="enabled">
							<option value="1">是</option>
							<option value="0">否</option>
						</select>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">IP验证是否开启：</span> <select
							class="form-control" id="authorizedIpEnabled">
							<option value="1">是</option>
							<option value="0" selected = "selected">否</option>
						</select>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">授权IP：</span> <input
							type="text" value="" class="form-control col-sm-8"
							name="authorizedIp" id="authorizedIp" placeholder="请在每个授权IP后面加上,">
					</div>
				</div>
				<div class="col-sm-12">
					<div class="row">
						<div class="col-sm-6 text-center">
							<button type="button" class="btn btn-w-m btn-white"
								onclick="thirdPayConfigEditPage.saveThirdPayConfig()">提
								交</button>
						</div>
						<div class="col-sm-6 text-center">
							<input type="reset" class="btn btn-w-m btn-white" value="重 置" />
						</div>
					</div>
				</div>
			</div>



		</form>
	</div>
	<!-- js -->
	<script type="text/javascript"
		src="<%=path%>/admin/thirdpayconfig/js/thirdpayconfig_edit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
	<!-- dwr -->
	<script type='text/javascript'
		src='<%=path%>/dwr/interface/managerThirdPayConfigAction.js'></script>
	<script type="text/javascript">
	thirdPayConfigEditPage.param.id = <%=id%>;
		<%-- var imgServerUrl = '<%= SystemConfigConstant.imgServerUrl %>'; --%>
	</script>
</body>
</html>

