function ThirdPayConfigEditPage(){}
var thirdPayConfigEditPage = new ThirdPayConfigEditPage();

thirdPayConfigEditPage.param = {
   	
};

$(document).ready(function() {
	/*thirdPayConfigEditPage.initPage();*/
	var configId = thirdPayConfigEditPage.param.id;  //获取查询的ID
	if(configId != null){
		thirdPayConfigEditPage.editThirdPayConfig(configId);
	}
	
	/*//选择银行时控制二维码显示
	$("#bankname").change(function(){
		if($(this).val() == "ALIPAY" || $(this).val() == "WEIXIN") {
			$("#barcodeTr").show();
		} else {
			$("#barcodeTr").hide();
		}
	});*/
	
});

/**
 * 保存第三方支付设置
 */
ThirdPayConfigEditPage.prototype.saveThirdPayConfig = function(){
	var pay = {};
	pay.id = $("#id").val();
	/*payBank.adminname = $("#adminname").val();*/
	pay.chargeType = $("#chargeType").val();
	pay.chargeDes = $("#chargeDes").val();
	pay.interfaceVersion = $("#interfaceVersion").val();
	pay.signType = $("#signType").val();
	pay.address = $("#address").val();
	pay.payUrl = $("#payUrl").val();
	pay.notifyUrl = $("#notifyUrl").val();
	pay.returnUrl = $("#returnUrl").val();
	pay.supportHttps = $("#supportHttps").val();
	pay.enabled = $("#enabled").val();
	pay.postUrl = $("#postUrl").val();
	pay.authorizedIpEnabled = $("#authorizedIpEnabled").val();
	pay.authorizedIp = $("#authorizedIp").val();
	
	if (pay.chargeType==null || pay.chargeType.trim()=="") {
		swal("请选择支付类型!");
		$("#chargeType").focus();
		return;
	}
	if (pay.chargeDes==null || pay.chargeDes.trim()=="") {
		swal("请输入支付描述!");
		$("#chargeDes").focus();
		return;
	}
	if (pay.interfaceVersion==null || pay.interfaceVersion.trim()=="") {
		swal("请输入接口版本号!");
		$("#interfaceVersion").focus();
		return;
	}
	if (pay.signType==null || pay.signType.trim()=="") {
		swal("请输入加密类型!");
		$("#signType").focus();
		return;
	}
	if (pay.address==null || pay.address.trim()=="") {
		swal("请输入实际支付接口地址!");
		$("#address").focus();
		return;
	}
	
	if (pay.payUrl==null || pay.payUrl.trim()=="") {
		swal("请输入转发支付网关的URL!");
		$("#payUrl").focus();
		return;
	}
	if (pay.notifyUrl==null || pay.notifyUrl.trim()=="") {
		swal("请输入同步通知url(交易处理)!");
		$("#notifyUrl").focus();
		return;
	}
	if (pay.returnUrl==null || pay.returnUrl.trim()=="") {
		swal("请输入异步通知url(交易处理)!");
		$("#returnUrl").focus();
		return;
	}
	if (pay.postUrl==null || pay.postUrl.trim()=="") {
		swal("请输入post页面路径!");
		$("#postUrl").focus();
		return;
	}
	
	if (pay.authorizedIpEnabled.trim()=="1" && (pay.authorizedIp==null || pay.authorizedIp.trim()=="")) {
		swal("IP验证如果开启,请输入相关的授权IP");
		$("#authorizedIpEnabled").focus();
		return;
	}
	/*if (pay.enabled==null || pay.enabled.trim()=="") {
		alert("请输入银行网址!");
		$("#enabled").focus();
		return;
	}*/
	managerThirdPayConfigAction.saveOrUpdateThirdPayConfig(pay,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.thirdPayConfigPage.closeLayer(index);
		   	    }); 
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("添加第三方支付设置数据失败.");
		}
    });
};

/**
 * 初始化页面
 */
ThirdPayConfigEditPage.prototype.initPage = function(id){
	$("#isdisabled").val("0");
};

/**
 * 初始化页面
 */
ThirdPayConfigEditPage.prototype.editThirdPayConfig = function(id){
	managerThirdPayConfigAction.getThirdPayConfigById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			var pay = r[1];
			$("#id").val(pay.id);
			$("#chargeDes").val(pay.chargeDes);
			$("#interfaceVersion").val(pay.interfaceVersion);
			$("#signType").val(pay.signType);
			$("#address").val(pay.address);
			$("#chargeType").val(pay.chargeType);
			$("#payUrl").val(pay.payUrl);
			$("#notifyUrl").val(pay.notifyUrl);
			$("#returnUrl").val(pay.returnUrl);
			$("#postUrl").val(pay.postUrl);
			$("#supportHttps").val(pay.supportHttps);
			$("#enabled").val(pay.enabled);
			$("#authorizedIpEnabled").val(pay.authorizedIpEnabled);
			$("#authorizedIp").val(pay.authorizedIp);
			/*$("#isDisabled").val(pay.isDisabled);*/
			/*if("ALIPAY" == pay.bankType || "WEIXIN" == pay.bankType) {
				$("#barcodeTr").show();
				if(pay.barcodeUrl != null && pay.barcodeUrl != "") {
					//展示图片内容
					$("#image").attr("src",imgServerUrl + pay.barcodeUrl);
					$("#image").show();
				}
			} else {
				$("#barcodeTr").hide();
			}*/
			
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取第三方支付设置信息失败.");
		}
    });
};
