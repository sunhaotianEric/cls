function ThirdPayConfigPage(){}
var thirdPayConfigPage = new ThirdPayConfigPage();

thirdPayConfigPage.param = {
		
};

thirdPayConfigPage.queryParam = {
		
};
thirdPayConfigPage.pageParam = {
		    pageNo : 1,
		    pageSize : defaultPageSize
};
$(document).ready(function() {
	thirdPayConfigPage.initTableData(); //查询所有的系统域名数据
});

ThirdPayConfigPage.prototype.initTableData = function(){

	thirdPayConfigPage.queryParam = getFormObj($("#thirdpayconfigForm"));
	thirdPayConfigPage.table = $('#thirdpayconfigTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"bPaginate" : false, 
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			thirdPayConfigPage.pageParam.pageSize = data.length;//页面显示记录条数
			thirdPayConfigPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerThirdPayConfigAction.getAllThirdPayConfig(function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					/*returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
*/					returnData.data = r[1]//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询奖金期号数据失败.");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": null},
		            {"data": "chargeType"},
		            {"data": "chargeDes"},
		            {"data": "interfaceVersion"},
		            {"data": "signType"},
		            {"data": "address"},
		            {"data": "payUrl"},
		            {"data": "notifyUrl"},
		            {"data": "returnUrl"},
		            {"data": "postUrl"},
		            {
		            	"data": "supportHttps",
		            	render: function(data, type, row, meta) {
		            		if (data == "1") {
		            			return '是';
		            		} else {
		            			return '否';
		            		}
		            		return data;
	            		}
		            },
		            {
		            	"data": "enabled",
		            	render: function(data, type, row, meta) {
		            		if (data == "1") {
		            			return '是';
		            		} else {
		            			return '否';
		            		}
		            		return data;
	            		}
		            },
		            {
	                	 "data": "id",
	                	 render: function(data, type, row, meta) {
	                		 var str="<a class='btn btn-info btn-sm btn-bj' onclick='thirdPayConfigPage.editThirdPayConfig("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;" +
	             		 		"<a class='btn btn-danger btn-sm btn-del' onclick='thirdPayConfigPage.delThirdPayConfig("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";
	                		 if(row.enabled=="1")
	                			 {
	                			 str="<a class='btn btn-warning btn-sm ' onclick='thirdPayConfigPage.closeThirdpayconfig("+data+")'><i class='glyphicon glyphicon-remove' ></i>&nbsp;停用 </a>&nbsp;&nbsp;"+str;
	                			 }
	                		 else
	                			 {
	                			 str="<a class='btn btn-sm btn-success' onclick='thirdPayConfigPage.openThirdpayconfig("+data+")'><i class='glyphicon glyphicon-ok'></i>&nbsp;启用 </a>&nbsp;&nbsp;"+str;
	                			 }
	                			 
	                		 return str ;
	                	 }
	                }
	            ]
	}).api(); 
	$("#thirdpayconfigTable_info").hide();

	/*thirdPayConfigPage.queryParam = {};
	thirdPayConfigPage.refreshThirdPayConfigPages(thirdPayConfigPage.queryParam,thirdPayConfigPage.pageParam.pageNo);*/
};

/**
 * 查询数据
 */
ThirdPayConfigPage.prototype.findThirdPayConfig = function(){
	/*thirdPayConfigPage.queryParam.bizSystem = $("#bizSystem").val();*/
	thirdPayConfigPage.table.ajax.reload();
};




/**
 * 删除
 */
ThirdPayConfigPage.prototype.delThirdPayConfig = function(id){
	
	   swal({
           title: "您确定要删除这条信息吗",
           text: "请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "删除",
           closeOnConfirm: false
       },
       function() {
    	   managerThirdPayConfigAction.delThirdPayConfig(id,function(r){
   			if (r[0] != null && r[0] == "ok") {
   				swal({ title: "提示", text: "删除成功",type: "success"});
   				thirdPayConfigPage.table.ajax.reload();
   			}else if(r[0] != null && r[0] == "error"){
   				swal(r[1]);
   			}else{
   				swal("删除信息失败.");
   			}
   	    });
       });
};

/**
 * 新增
 */
ThirdPayConfigPage.prototype.addThirdPayConfig=function()
{
	
	 layer.open({
         type: 2,
         title: '新增第三方支付设置',
         maxmin: false,
         shadeClose: true,
         //点击遮罩关闭层
         area: ['70%', '80%'],
         content: contextPath + "/managerzaizst/thirdpayconfig/thirdpayconfig_edit.html",
         cancel: function(index){ 
      	   thirdPayConfigPage.table.ajax.reload();
      	   }
     });
}
/**
 * 编辑
 */
ThirdPayConfigPage.prototype.editThirdPayConfig=function(id)
{
	
	  layer.open({
           type: 2,
           title: '编辑第三方支付设置',
           maxmin: false,
           shadeClose: true,
           //点击遮罩关闭层
           area: ['70%', '80%'],
           content: contextPath + "/managerzaizst/thirdpayconfig/"+id+"/thirdpayconfig_edit.html",
           cancel: function(index){ 
        	   thirdPayConfigPage.table.ajax.reload();
        	   }
       });
}

ThirdPayConfigPage.prototype.closeLayer=function(index){
	layer.close(index);
	thirdPayConfigPage.table.ajax.reload();
}

/**
 * 开启状态
 */
ThirdPayConfigPage.prototype.openThirdpayconfig = function(id){
	
		managerThirdPayConfigAction.openThirdPayConfig(id,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "操作成功",type: "success"});
				thirdPayConfigPage.table.ajax.reload();
			}else if(r[0] != null && r[0] == "error"){
				showErrorDlg(jQuery(document.body), r[1]);
			}else{
				showErrorDlg(jQuery(document.body), "启用状态失败.");
			}
	    });
	
};

/**
 * 停用状态
 */
ThirdPayConfigPage.prototype.closeThirdpayconfig = function(id){
	
		managerThirdPayConfigAction.closeThirdPayConfig(id,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "操作成功",type: "success"});
				thirdPayConfigPage.table.ajax.reload();
			}else if(r[0] != null && r[0] == "error"){
				showErrorDlg(jQuery(document.body), r[1]);
			}else{
				showErrorDlg(jQuery(document.body), "停用状态失败.");
			}
	    });

};