function LoadFilePage(){}
var loadFilePage = new LoadFilePage();

loadFilePage.param = {
   	
};


$(document).ready(function() {
	loadFilePage.getDownLoadFileName(); //读取文件名
	
});
/**
 * 读取文件名
 */
LoadFilePage.prototype.getDownLoadFileName = function(){

		managerSystemAction.getDownLoadFileName(function(r){
			var fileTr= $("#fileTr")
			var str="";
		if (r[0] != null && r[0] == "ok") {
			var fileName=r[1];
			
			for(var i=0;i<fileName.length;i++){
				if(i%5== 0){
					str+= "<tr> "; 
				}
                str+="<td><a href='javascript:void(0)' onclick='loadFilePage.downLoadFile(\""+fileName[i]+"\")'> "+fileName[i]+" </a></td>";
				
				if((i + 1) % 5 == 0){
					str+= "</tr> ";
				}
			}
			fileTr.html(str);
			
			
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询操作日志数据失败.");
		}
    });
};
//文件下载
LoadFilePage.prototype.downLoadFile=function(fileName){
	if(fileName==null){
		alert("下载文件不存在");
		return;
	}
	managerSystemAction.downLoadFile(fileName,function (data){
		if(data!=null){
		     dwr.engine.openInDownload(data);  
		     async : false
		}else{
			alert("对不起！该文件不能下载");
		}
	}); 

}
