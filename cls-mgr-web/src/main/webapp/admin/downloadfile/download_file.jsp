<%@page import="com.team.lottery.enums.ERecordLogModel"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<html>
<head>
    <meta charset="utf-8" />
    <title>日志文件下载</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/downloadfile/js/download_file.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerRecordLogAction.js'></script>
     <script type='text/javascript' src='<%=path%>/dwr/interface/managerSystemAction.js'></script>
    
<style type="text/css">
caption, th {
    text-align: center;
}

.tb td{
    text-align: center;
}

.ui-dialog{
    width : auto;
}
</style>
</head>


<body>
   <div class="alert alert-info">当前位置<b class="tip"></b>网站信息管理<b class="tip"></b>日志文件下载</div>
    
   	<table class="tbform" id=fileTr>
         <tr>
             <td class="tdl" colspan="1">文件列表：</td>
          </tr>
     
     </table>
    
</body>
</html>
