function DayConsumeActivityConfigPage(){}
var dayConsumeActivityConfigPage = new DayConsumeActivityConfigPage();

dayConsumeActivityConfigPage.param = {
   	
};
//分页参数
dayConsumeActivityConfigPage.pageParam = {
		    pageNo : 1,
		    pageSize : defaultPageSize
};
/**
 * 查询参数
 */
dayConsumeActivityConfigPage.queryParam = {
		bizSystem:null
};


$(document).ready(function() {
	dayConsumeActivityConfigPage.initTableData();
});

DayConsumeActivityConfigPage.prototype.initTableData = function(){

	dayConsumeActivityConfigPage.queryParam = getFormObj($("#dayConsumeActivityConfigForm"));
	dayConsumeActivityConfigPage.table = $('#dayConsumeActivityConfigTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			dayConsumeActivityConfigPage.pageParam.pageSize = data.length;//页面显示记录条数
			dayConsumeActivityConfigPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	manageDayConsumeActivityConfigAction.getDayConsumeActityConfigsPage(dayConsumeActivityConfigPage.queryParam,dayConsumeActivityConfigPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
				returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询所有的快捷支付数据失败.");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": null},
		            {"data": "bizSystemName",
		            	"bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "activityTypeStr"},	
		            {"data": "lotteryNum"},
		            {"data": "money"},
		            {"data": "createAdmin"},
		            {"data": "updateAdmin"},
		            {"data": "createDateStr"},
		            {"data": "updateDateStr"},
		            {
	                	 "data": "id",
	                	 render: function(data, type, row, meta) {
	                		 var str="<a class='btn btn-info btn-sm btn-bj' onclick='dayConsumeActivityConfigPage.editDayConsumeActivityConfig("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;" +
	             		 		"<a class='btn btn-danger btn-sm btn-del' onclick='dayConsumeActivityConfigPage.delDayConsumeActivityConfig("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";
	                		 
	                			 
	                		 return str ;
	                	 }
	                }
	            ]
	}).api();
};

/**
 * 查询数据
 */
DayConsumeActivityConfigPage.prototype.findDayConsumeActivityConfig = function(){
	var bizSystem = $("#bizSystem").val();
	if(bizSystem == ""){
		dayConsumeActivityConfigPage.queryParam.bizSystem = null;
	}else{
		dayConsumeActivityConfigPage.queryParam.bizSystem = bizSystem;
	}
	
	dayConsumeActivityConfigPage.table.ajax.reload();
};

/**
 * 删除
 */
DayConsumeActivityConfigPage.prototype.delDayConsumeActivityConfig = function(id){
	
	   swal({
           title: "您确定要删除这条信息吗",
           text: "请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "删除",
           closeOnConfirm: false
       },
       function() {
    	   manageDayConsumeActivityConfigAction.delDayConsumeActityConfig(id,function(r){
   			if (r[0] != null && r[0] == "ok") {
   				swal({ title: "提示", text: "删除成功",type: "success"});
   				dayConsumeActivityConfigPage.table.ajax.reload();
   			}else if(r[0] != null && r[0] == "error"){
   				swal(r[1]);
   			}else{
   				swal("删除信息失败.");
   			}
   	    });
       });
};

/**
 * 新增
 */
DayConsumeActivityConfigPage.prototype.addDayConsumeActivityConfig=function()
{
	
	 layer.open({
         type: 2,
         title: '新增流水返送配置',
         maxmin: false,
         shadeClose: true,
         //点击遮罩关闭层
         area: ['70%', '80%'],
         content: contextPath + "/managerzaizst/day_consume_activity_config/day_consume_activity_config_add.html",
         cancel: function(index){ 
      	   dayConsumeActivityConfigPage.table.ajax.reload();
      	   }
     });
}
/**
 * 编辑
 */
DayConsumeActivityConfigPage.prototype.editDayConsumeActivityConfig=function(id)
{
	
	  layer.open({
           type: 2,
           title: '编辑新增流水返送配置',
           maxmin: false,
           shadeClose: true,
           //点击遮罩关闭层
           area: ['70%', '80%'],
           content: contextPath + "/managerzaizst/day_consume_activity_config/"+id+"/day_consume_activity_config_edit.html",
           cancel: function(index){ 
        	   dayConsumeActivityConfigPage.table.ajax.reload();
        	   }
       });
}

DayConsumeActivityConfigPage.prototype.closeLayer=function(index){
	layer.close(index);
	swal({
        title: "保存成功",
        text: "您已经保存了这条流水返送配置。",
        type: "success"});
	dayConsumeActivityConfigPage.table.ajax.reload();
}