function EditDayConsumeActivityConfigPage(){}
var editDayConsumeActivityConfigPage = new EditDayConsumeActivityConfigPage();

editDayConsumeActivityConfigPage.param = {
   	
};

$(document).ready(function() {
	var id = editDayConsumeActivityConfigPage.param.id;  //获取查询的帮助ID
	if(id != null){
		editDayConsumeActivityConfigPage.editDayConsumeActityConfig(id);
	}
});


/**
 * 保存帮助信息
 */
EditDayConsumeActivityConfigPage.prototype.saveData = function(){
	
	var id = $("#id").val();
	var bizSystem = $("#bizSystem").val();
    var activityType = $("#activityType").val();
	var lotteryNum = $("#lotteryNumId").val();
	var money = $("#moneyId").val();
	
	if(currentUser.bizSystem == "SUPER_SYSTEM"){
		if(bizSystem==null || bizSystem.trim()==""){
			swal("请选择业务系统!");
			$("#bizSystem").focus();
			return;
		}
	}else{
		bizSystem = currentUser.bizSystem;
	}
/*	if(type==null || type.trim()==""){
		swal("请选择类型!");
		$("#type").focus();
		return;
	}*/
	if(lotteryNum==null || lotteryNum.trim()==""){
		swal("请输入投注额!");
		$("#lotteryNumId").focus();
		return;
	}
	if(money==null || money.trim()==""){
		swal("请输入活动赠送金额!");
		editor.focus();
		return;
	}
	if(activityType==null || activityType.trim()==""){
		swal("请选择活动类型!");
		editor.focus();
		return;
	}
	var dayConsumeActityConfig = {};
	dayConsumeActityConfig.id = id;
	dayConsumeActityConfig.bizSystem = bizSystem;
    dayConsumeActityConfig.activityType = activityType;
	dayConsumeActityConfig.lotteryNum = lotteryNum;
	dayConsumeActityConfig.money = money;
	manageDayConsumeActivityConfigAction.saveDayConsumeActityConfig(dayConsumeActityConfig,function(r){
		if (r[0] != null && r[0] == "ok") {
			var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
			parent.dayConsumeActivityConfigPage.closeLayer(index);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("保存帮助信息失败.");
		}
    });
};


/**
 * 赋值帮助信息
 */
EditDayConsumeActivityConfigPage.prototype.editDayConsumeActityConfig = function(id){
	manageDayConsumeActivityConfigAction.getDayConsumeActityConfigById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			$("#id").val(r[1].id);
			$("#bizSystem").val(r[1].bizSystem);
			$("#bizSystem").attr("disabled","disabled");
			$("#activityType").val(r[1].activityType);
			$("#lotteryNumId").val(r[1].lotteryNum);
			$("#moneyId").val(r[1].money);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取帮助信息失败.");
		}
    });
};