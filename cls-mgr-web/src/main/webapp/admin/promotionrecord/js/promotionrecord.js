function PromotionRecordPage(){}
var promotionRecordPage = new PromotionRecordPage();

/**
 * 查询参数
 */
promotionRecordPage.param = {
	bizSystem : null,
	level : null,
	levelAfter : null,
	userName : null,
	status : null,
	money : null,
	createDateStart : null,
	createDateEnd : null,
	startime : null,
	endtime : null,
};

//分页参数
promotionRecordPage.pageParam = {
	pageNo : 1,
	pageSize : defaultPageSize
};
var startime,endtime,num=0

$(document).ready(function() {
	//归属日期默认昨天
	startime= new Date().AddDays(-1).format("yyyy-MM-dd 00:00:00");
	endtime= new Date().AddDays(-1).format("yyyy-MM-dd 23:59:59");
	$("#startime").val(startime);
	$("#endtime").val(endtime);
	//查询所有的操作日志数据
	promotionRecordPage.getAllRecordLogs();
	
})

PromotionRecordPage.prototype.findRecordLogs = function(){
	promotionRecordPage.param = getFormObj($("#recordLogQuery"));
	startime=$("#startime").val();
	if(startime!=null&&startime!=''){
		promotionRecordPage.param.startime=startime.stringToDate();
	}
	endtime=$("#endtime").val();
	if(endtime!=null&&endtime!=''){
		promotionRecordPage.param.endtime=endtime.stringToDate();
	}
	promotionRecordPage.table.ajax.reload();
}

/**
 * 加载所有的登陆日志数据
 */
PromotionRecordPage.prototype.getAllRecordLogs = function(){
	promotionRecordPage.param = getFormObj($("#activityQuery"));
	promotionRecordPage.table =  $('#activityTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"ajax":function (data, callback, settings) {
			promotionRecordPage.pageParam.pageSize = data.length;//页面显示记录条数
			promotionRecordPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
			managerPromotionRecordAction.getAllPromotionRecords(promotionRecordPage.param,promotionRecordPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				}; 
				
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
					if(num===0){
						promotionRecordPage.findRecordLogs();
					}
					num++
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询奖金配置数据失败.");
				}
				callback(returnData);
		    });
		},
	"columns": [
	            {"data":"bizSystemName"},
	            {"data":"userName"},
	            {"data":"level"},
	            {"data":"levelAfter"},
	            {"data":"money"},
	            {"data":"status",
	            	render: function(data, type, row, meta) {
	            		if (data == "1") {
	            			return '已手动领取';
	            		} else if (data == "0") {
	            			return "<span style='color: red;'>未领取</span>";
	            		} else {
	            			return '已自动领取';
	            		}
	            		return data;
	            	}
	            },
	            {"data":"createDateStr"},
	            {"data":"updateDateStr"},
	            
	          /*  {"data":"id",
	            	 render: function(data, type, row, meta) {
	            		 return  "<a class='btn btn-danger btn-sm btn-del' onclick='recordLogPage.delRecordLogs("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";
	            	 },"bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false
	            },*/
	            
	     ]
	}).api();
}

/**
 * 按日期查询数据
 */
PromotionRecordPage.prototype.findRecordlogByQuery = function(dateRange){
	if(dateRange != undefined && dateRange != null && dateRange != ''){
		commonPage.setDateRangeTimeZero(dateRange, "startime", "endtime",2);	
	}
	promotionRecordPage.param = getFormObj($("#recordLogQuery"));
	promotionRecordPage.table.ajax.reload();
};

/*RecordLogPage.prototype.delRecordLogs = function(id){
	   swal({
           title: "您确定要删除这条日志吗",
           text: "请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "删除",
           closeOnConfirm: false
       },
       function(){
    	   managerRecordLogAction.delRecordLogs(id, function(r){
      			if (r[0] != null && r[0] == "ok") {
      				swal({ title: "提示", text: "删除成功",type: "success"});
      				recordLogPage.findRecordLogs();
      			}else if(r[0] != null && r[0] == "error"){
      				swal(r[1]);
      			}else{
      				swal("删除日志数据失败.");
      			}
      	    
    	   });
       })
}
*/