function EditUserRebateForbidPage(){}
var editUserRebateForbidPage = new EditUserRebateForbidPage();

editUserRebateForbidPage.param = {
   	
};

$(document).ready(function() {
	var id = editUserRebateForbidPage.param.id;  //获取查询ID
/*	if(id != null){
		editUserRebateForbidPage.edit(id);
	}*/
});


/**
 * 保存更新信息
 */
EditUserRebateForbidPage.prototype.saveData = function(){
	var id = $("#id").val();
	var bizSystem = $("#bizSystem").val();
	var userName = $("#userName").val();
	
	if(currentUser.bizSystem=='SUPER_SYSTEM' && (bizSystem==null || bizSystem.trim()=="")){
		swal("请输入业务系统!");
		$("#bizSystem").focus();
		return;
	}
	
	if(userName==null || userName.trim()==""){
		swal("请输入用户名!");
		$("#userName").focus();
		return;
	}
	
	var userRebateForbid = {};
	userRebateForbid.id = editUserRebateForbidPage.param.id;
	userRebateForbid.bizSystem = bizSystem;
	userRebateForbid.userName = userName;
	
	manageUserRebateForbidAction.saveUserRebateForbid(userRebateForbid,function(r){
		if (r[0] != null && r[0] == "ok") {
			 swal({
		           title: r[1],
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.userRebateForbidPage.closeLayer(index);
		   	    }); 
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("保存信息失败.");
		}
    });
};

