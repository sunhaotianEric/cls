function UserRebateForbidPage(){}
var userRebateForbidPage = new UserRebateForbidPage();

userRebateForbidPage.param = {
   	
};

/**
 * 查询参数
 */
userRebateForbidPage.queryParam = {
		bizSystem : null,
		enable : null
};

//分页参数
userRebateForbidPage.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize
};


$(document).ready(function() {
	userRebateForbidPage.initTableData(); //初始化查询数据
});



/**
 * 初始化列表数据
 */
UserRebateForbidPage.prototype.initTableData = function() {
	userRebateForbidPage.queryParam = getFormObj($("#queryForm"));
	userRebateForbidPage.table = $('#userRebateForbIdTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"scrollX": true,
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			userRebateForbidPage.pageParam.pageSize = data.length;//页面显示记录条数
			userRebateForbidPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	manageUserRebateForbidAction.getAllUserRebateForbid(userRebateForbidPage.queryParam.bizSystem ,userRebateForbidPage.queryParam.userName,userRebateForbidPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data":null},
		            {"data": "bizSystemName","bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "userName"},
		            {"data": "createDateStr"},
		            {"data": "createAdmin"}, 
		            {"data": "id",/*"bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false,*/
	                	 render: function(data, type, row, meta) {
	                 return "<a class='btn btn-danger btn-sm btn-xf' onclick='userRebateForbidPage.del("+data+")'>&nbsp;删除 </a>&nbsp;&nbsp;"
         			 	/*	"<a class='btn btn-info btn-sm btn-bj' onclick='userRebateForbidPage.edit("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>"; */
	                		
	                	 }
	                }
	            ]
	}).api(); 
}

/**
 * 查询数据
 */
UserRebateForbidPage.prototype.find = function(){
	 userRebateForbidPage.queryParam.bizSystem = $("#bizSystem").val();
	 userRebateForbidPage.queryParam.userName = $("#userName").val();
	
	if(typeof(userRebateForbidPage.queryParam.bizSystem) === "undefined" || userRebateForbidPage.queryParam.bizSystem.trim()==""){
		userRebateForbidPage.queryParam.bizSystem =  bizSystem = null;
	}
	
	if(typeof(userRebateForbidPage.queryParam.userName) === "undefined" || userRebateForbidPage.queryParam.userName.trim()==""){
		userRebateForbidPage.queryParam.userName = userName = null;
	}
	
	userRebateForbidPage.table.ajax.reload();
};

/**
 * 删除
 */
UserRebateForbidPage.prototype.del = function(id){
	
	   swal({
           title: "您确定要删除这条信息吗",
           text: "请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "删除",
           closeOnConfirm: false
       },
       function() {
    	   manageUserRebateForbidAction.delUserRebateForbid(id,function(r){
   			if (r[0] != null && r[0] == "ok") {
   				swal({ title: "提示", text: "删除成功",type: "success"});
   				userRebateForbidPage.find();
   			}else if(r[0] != null && r[0] == "error"){
   				swal(r[1]);
   			}else{
   				swal("删除信息失败.");
   			}
   	    });
       });
};

UserRebateForbidPage.prototype.add=function(){
	
	 layer.open({
         type: 2,
         title: '返点禁止管理新增',
         maxmin: false,
         shadeClose: true,
         //点击遮罩关闭层
         area: ['800px', '420px'],
         //area: ['60%', '45%'],
         content: contextPath + "/managerzaizst/userRebateForbid/editUserRebateForbid.html",
     /*    btn: ['确 定', '关闭'],
         yes: function(index, layero){
        	 alert(index);
        	 var iframeWin = window['layui-layer-iframe' + index].window;
        	 iframeWin.edituserRebateForbidPage.saveData();
        	 iframeWin.saveData();
        	  },*/
         cancel: function(index){ 
        	 userRebateForbidPage.find();
      	   }
     });
}

UserRebateForbidPage.prototype.edit=function(id){
	
	  layer.open({
           type: 2,
           title: '返点禁止管理编辑',
           maxmin: false,
           shadeClose: true,
           //点击遮罩关闭层
           area: ['800px', '420px'],
           //area: ['60%', '45%'],
           content: contextPath + "/managerzaizst/userRebateForbid/"+id+"/editUserRebateForbid.html",
           cancel: function(index){ 
        	   userRebateForbidPage.find();
        	   }
       });
}

UserRebateForbidPage.prototype.closeLayer=function(index){
	layer.close(index);
	userRebateForbidPage.find();
}


