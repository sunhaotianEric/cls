<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>  
    <head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>返点禁止配置编辑</title>
    <link rel="shortcut icon" href="favicon.ico">
    <jsp:include page="/admin/include/include.jsp"></jsp:include>

    <%
	 String id = request.getParameter("id");  
	%>
</head>
  <body class="gray-bg">
        <div class="wrapper wrapper-content animated fadeIn">
         <form action="javascript:void(0)">
         	<div class="row border-bottom" style="padding-left: 0px;">
				<div class="col-sm-12">
					<p style="color: red">温馨提醒：多个用户名时用逗号隔开","</p>
				</div>
			</div>
            <div class="row">
             <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">商户</span>
                        <input type="hidden" readonly="readonly" id="id"/>
                        <% if(id==null){  %>
                       <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control" />
                       <%}else{ %>
                       <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control,disabled:'disabled'" />
                       <%} %>
                        </div>
                </div>
                </c:if>
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">用户名</span>
                        <input id='userName' name="userName" type="text" value="" class="form-control" ></div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button type="button" class="btn btn-w-m btn-white" onclick="editUserRebateForbidPage.saveData()">提 交</button></div>
                        <div class="col-sm-6 text-center">
                            <!-- <button type="button" class="btn btn-w-m btn-white">重 置</button> -->
                            <input type="reset" class="btn btn-w-m btn-white" value="重 置"/>
                            </div>
                    </div>
                </div> 
            </div>
            </form>
        </div>

      <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/userRebateForbid/js/editUserRebateForbid.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/manageUserRebateForbidAction.js'></script> 
     <script type="text/javascript">
     editUserRebateForbidPage.param.id = <%=id%>;
	</script>
</body>
</html>

