function EmailSendRecordPage(){}
var emailSendRecordPage = new EmailSendRecordPage();

$("#bizSystem").attr("disabled", true);//系统下拉框禁止编辑

/**
 * 查询参数
 */
emailSendRecordPage.queryParam = {
	id:null,
	bizSystem : null,
	ip:null,
	userName:null,
	email:null,
	vcode:null,
	remark:null,
	createDate:null
};
emailSendRecordPage.param = {
//	skipAwardZh:0  
}

$(document).ready(function() {
	emailSendRecordPage.queryParam.id = id; // 获取查询的ID
	if (id != null) {
		emailSendRecordPage.editRecordControl(id);
	}
});

EmailSendRecordPage.prototype.selectByPrimary=function(){
	var id=emailSendRecordPage.queryParam.id;
	var bizSystem=$("#bizSystem").val();
	var ip=$("#ip").val();
	var userName=$("#userName").val();
	var email=$("#email").val();
	var vcode=$("#vcode").val();
	var remark=$("#remark").val();
	var createDate=$("#createDate").val();
	emailSendRecordPage.queryParam.id=id;
	emailSendRecordPage.queryParam.bizSystem=bizSystem;
	emailSendRecordPage.queryParam.ip=ip;
	emailSendRecordPage.queryParam.userName=userName;
	emailSendRecordPage.queryParam.email=email;
	emailSendRecordPage.queryParam.vcode=vcode;
	emailSendRecordPage.queryParam.remark=remark;
	emailSendRecordPage.queryParam.createDate=createDate;
		if(emailSendRecordPage.queryParam.id!=null||emailSendRecordPage.queryParam.id!=""){
			managerEmailSendRecordAction.updateEmailSendRecord(emailSendRecordPage.queryParam,function(r){
				if(r[0]=="ok"){
					swal({title:"修改成功！",
				        text:"已成功修改数据",
				        type:"success"},
				      function(){
				        	var index = parent.layer.getFrameIndex(window.name); //获取窗口索引    
				        	parent.emailSendRecordPage.layerClose(index);
					  }
				     )				
				}else{
					swal(r[1]);
				}
			});
		}else{
			managerEmailSendRecordAction.insertEmailSendRecord(emailSendRecordPage.queryParam,function(r){
				if(r[0]=="ok"){
					swal({title:"保存成功！",
				        text:"已成功保存数据",
				        type:"success"},
				      function(){
				        	var index = parent.layer.getFrameIndex(window.name); //获取窗口索引    
				        	parent.emailSendRecordPage.layerClose(index);
					  }
				     )	
				}else{
					swal(r[1]);
				}
			});
		}
		emailSendRecordPage.queryParam={};
}

/**
 * 编辑时赋值
 */
EmailSendRecordPage.prototype.editRecordControl = function(id) {
	managerEmailSendRecordAction.selectEmailSendRecord(id, function(r) {
		if (r[0] != null && r[0] == "ok") {
			$("#id").val(r[1].id);
			$("#bizSystem").val(r[1].bizSystem);
			$("#ip").val(r[1].ip);
			$("#userName").val(r[1].userName);
			$("#email").val(r[1].email);
			$("#vcode").val(r[1].vcode);
			$("#remark").val(r[1].remark);
		} else if (r[0] != null && r[0] == "error") {
			showErrorDlg(jQuery(document.body), r[1]);
		} else {
			showErrorDlg(jQuery(document.body), "获取数据失败.");
		}
	});
};