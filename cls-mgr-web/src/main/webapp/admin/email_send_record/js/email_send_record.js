function EmailSendRecordPage(){}
var emailSendRecordPage = new EmailSendRecordPage();

/**
 * 查询参数
 */
emailSendRecordPage.queryParam = {
		id:null,
		bizSystem : null,
		ip:null,
		type:null,
		userName:null,
		email:null,
		vcode:null,
		remark:null,
		createDate:null,
		startDate:null,
		endDate:null
};
//分页参数
emailSendRecordPage.pageParam = {
    pageNo : 1,
    pageSize : defaultPageSize
};

$(document).ready(function() {
	emailSendRecordPage.initTableData();
});

EmailSendRecordPage.prototype.initTableData = function() {
	emailSendRecordPage.queryParam = getFormObj($("#emailSendRecordForm"));
	emailSendRecordPage.table = $('#emailSendRecordPageTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"bPaginate" : false, 
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			emailSendRecordPage.pageParam.pageSize = data.length;//页面显示记录条数
			emailSendRecordPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerEmailSendRecordAction.getAllEmailSendRecord(emailSendRecordPage.queryParam,emailSendRecordPage.pageParam,function(r){
		//封装返回数据
		var returnData = {
			recordsTotal : 0,
			recordsFiltered : 0,
			data : null
		};
		if (r[0] != null && r[0] == "ok") {
			returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
			returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
			returnData.data = r[1].pageContent;;//返回的数据列表
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("查询信息失败.");
		}
		callback(returnData);
			});
		},
		"columns": [
            {"data": "id"},
            {
            	"data": "bizSystemName",
            	"bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false,
            },
            {"data": "ip"},
            {"data": "typeName"},
            /*{"data": "type",
            	render: function(data, type, row, meta) {
            		if (data == "DEFAULT_TYPE") {
            			return "默认类型";
            		} else if(data == "FORGETPWD_TYPE"){
            			return "忘记密码类型";
            		} else if(data == "BIND_EMAIL_TYPE"){
            			return "绑定手机类型";
            		} else if(data == "CHANGE_EMAIL_TYPE"){
            			return "修改绑定手机类型";
            		} else {
            			return data;
            		}
            	}
            },*/
            {"data": "userName"},
            {"data": "email"},
            {"data": "vcode"},
            {"data": "remark",	
            	render: function(data, type, row, meta) {
            		if (data == null || data=="") {
            			return "暂未备注";
            		} else {
            			return data;
            		}
            	}
            },
            {   
            	"data": "createDate",
            	render: function(data, type, row, meta) {
            		return dateFormat(data,'yyyy-MM-dd hh:mm:ss');
        		}
            },
            {
            	 "data": "id",
            	 render: function(data, type, row, meta) {
            		 return "<a class='btn btn-info btn-sm btn-bj' onclick='emailSendRecordPage.editRecordControl("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;" +
            		 		"<a class='btn btn-danger btn-sm btn-del' onclick='emailSendRecordPage.delRecordControl("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";
            	 }
            }
        ]
}).api(); 
}

/**
 * 查询数据
 */
EmailSendRecordPage.prototype.findRecordControl=function(){
	var bizSystem=$("#bizSystem").val();
	var type=$("#type").val();
	var userName = $("#userName").val();
	var email = $("#email").val();
	var startDate=$("#startDate").val();
	var endDate=$("#endDate").val();
	if(bizSystem==null||bizSystem==""){
		emailSendRecordPage.queryParam.bizSystem=null;
	}else{
		emailSendRecordPage.queryParam.bizSystem=bizSystem;
	}
	if(type==null||type==""){
		emailSendRecordPage.queryParam.type=null;
	}else{
		emailSendRecordPage.queryParam.type=type;
	}
	if(userName==null||userName==""){
		emailSendRecordPage.queryParam.userName=null;
	}else{
		emailSendRecordPage.queryParam.userName=userName;
	}
	if(email==null||email==""){
		emailSendRecordPage.queryParam.email=null;
	}else{
		emailSendRecordPage.queryParam.email=email;
	}
	if(startDate==null||startDate==""){
		emailSendRecordPage.queryParam.startDate=null;
	}else{
		emailSendRecordPage.queryParam.startDate=new Date(startDate);
	}
	if(endDate==null||endDate==""){
		emailSendRecordPage.queryParam.endDate=null;
	}else{
		emailSendRecordPage.queryParam.endDate=new Date(endDate);
	}
	emailSendRecordPage.table.ajax.reload();
}

EmailSendRecordPage.prototype.delRecordControl =function(id){
	  swal({
          title: "您确定要删除这条信息吗",
          text: "请谨慎操作！",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          cancelButtonText: "取消",
          confirmButtonText: "删除",
          closeOnConfirm: false
      },
      function() {
    	  managerEmailSendRecordAction.deleteEmailSendRecord(id,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "删除成功",type: "success"});
				emailSendRecordPage.findRecordControl();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("删除失败.");
			}
	    });
	});
};

/**
 * 编辑
 */
EmailSendRecordPage.prototype.editRecordControl=function(id){
	layer.open({
        type: 2,
        title: '邮件发送记录编辑',
        maxmin: false,
        shadeClose: true,
        //点击遮罩关闭层
        area: ['600px', '450px'],
        content: contextPath + "/managerzaizst/email_send_record/email_send_record_edit.html?id="+id,
        cancel: function(index){ 
        	
     	}
    });
}


EmailSendRecordPage.prototype.layerClose=function(index){
	layer.close(index);
	emailSendRecordPage.findRecordControl();
}