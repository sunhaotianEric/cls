function TreatmentApplyManualPage(){}
var treatmentApplyManualPage = new TreatmentApplyManualPage();

treatmentApplyManualPage.param = {
};

$(document).ready(function() {
	if(currentUser.bizSystem!='SUPER_SYSTEM'){
		$("#syncBizSystem").val(currentUser.bizSystem);
		$("#syncBizSystem").attr("disabled","disabled");
	}
});

/**
 * 手动代理福利结算
 */
TreatmentApplyManualPage.prototype.treatmentApplyManual = function(){
	var treatmentType = $("#treatmentType").val();
	var dealTime = $("#dealTime").val();
	var treatmentBizSystem = $("#treatmentBizSystem").val();
	var releasePolicy = $("#releasePolicy").val();
	var isReleaseMoneySwitch = false;
	if($("[name='isReleaseMoneySwitch']").prop('checked')) {
		isReleaseMoneySwitch = true;
	}

	if(treatmentType == null || treatmentType == "" ||
			dealTime == null || dealTime== ""){
		 swal("参数有误.");
		return;
	}
	
	$("#bonusButton").attr('disabled','disabled');

	managerTreatmentOrderAction.treatmentApplyManual(treatmentType, isReleaseMoneySwitch, new Date(dealTime),releasePolicy,treatmentBizSystem,function(r){
		if (r[0] != null && r[0] == "ok") {
			 swal(r[1]);
      		$("#bonusButton").removeAttr("disabled");
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
			$("#bonusButton").removeAttr("disabled");
		}else{
			showErrorDlg(jQuery(document.body), "请求失败.");
			$("#bonusButton").removeAttr("disabled");
		}
    });
};

/**
 * 手动用户团队日盈亏计算
 */
TreatmentApplyManualPage.prototype.userDayConsumeManual = function(){
	var dayConsumeDays = $("#dayConsumeDays").val();
	var dayConsumeTime = $("#dayConsumeTime").val();
	var dayConsumeBizSystem = $("#dayConsumeBizSystem").val();
	if(dayConsumeDays == null || dayConsumeDays == "" ||
			dayConsumeTime == null || dayConsumeTime== ""){
		 swal("参数有误.1");
		return;
	}
	
	$("#dayConsumeButton").attr('disabled','disabled');
	
	managerTreatmentOrderAction.userDayConsumeManual(new Date(dayConsumeTime), dayConsumeDays,dayConsumeBizSystem, function(r){
		if (r[0] != null && r[0] == "ok") {
			 swal(r[1]);
      		$("#dayConsumeButton").removeAttr("disabled");
		}else if(r[0] != null && r[0] == "error"){
			 swal(r[1]);
			$("#dayConsumeButton").removeAttr("disabled");
		}else{
			 swal("开奖请求失败.");
			$("#dayConsumeButton").removeAttr("disabled");
		}
    });
}


/**
 * 手动用户自身日盈亏计算
 */
TreatmentApplyManualPage.prototype.userSelfDayConsumeManual = function(){
	var dayConsumeDays = $("#daySelfConsumeDays").val();
	var dayConsumeTime = $("#daySelfConsumeTime").val();
	var daySelfConsumeBizSystem = $("#daySelfConsumeBizSystem").val();
	if(dayConsumeDays == null || dayConsumeDays == "" ||
			dayConsumeTime == null || dayConsumeTime== ""){
		swal("参数有误.");
		return;
	}

	$("#daySelfConsumeButton").attr('disabled','disabled');
	
	managerTreatmentOrderAction.userSelfDayConsumeManual(new Date(dayConsumeTime), dayConsumeDays,daySelfConsumeBizSystem, function(r){
		if (r[0] != null && r[0] == "ok") {
			swal(r[1]);
      		$("#daySelfConsumeButton").removeAttr("disabled");
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
			$("#daySelfConsumeButton").removeAttr("disabled");
		}else{
			swal("开奖请求失败.");
			$("#daySelfConsumeButton").removeAttr("disabled");
		}
    });
}

/**
 * 手动用户团队日余额计算
 */
TreatmentApplyManualPage.prototype.userDayMoneyManual = function(){
	var dayMoneyDays = $("#dayMoneyDays").val();
	var dayMoneyTime = $("#dayMoneyTime").val();
	
	if(dayMoneyDays == null || dayMoneyDays == "" ||
			dayMoneyTime == null || dayMoneyTime== ""){
		swal("参数有误.");
		return;
	}
	
	$("#dayMoneyButton").attr('disabled','disabled');
	
	managerTreatmentOrderAction.userDayMoneyManual(new Date(dayMoneyTime), dayMoneyDays, function(r){
		if (r[0] != null && r[0] == "ok") {
			swal(r[1]);
      		$("#dayMoneyButton").removeAttr("disabled");
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
			$("#dayMoneyButton").removeAttr("disabled");
		}else{
			showErrorDlg(jQuery(document.body), "请求失败.");
			$("#dayMoneyButton").removeAttr("disabled");
		}
    });
}

/**
 * 手动用户团队小时盈亏计算
 */
TreatmentApplyManualPage.prototype.userHourConsumeManual = function(){
	var hourConsumeCount = $("#hourConsumeCount").val();
	var hourConsumeTime = $("#hourConsumeTime").val();
	var hourConsumeBizSystem = $("#hourConsumeBizSystem").val();
	if(hourConsumeCount == null || hourConsumeCount == "" ||
			hourConsumeTime == null || hourConsumeTime== ""){
		swal("参数有误.");
		return;
	}

	$("#hourConsumeButton").attr('disabled','disabled');
	
	managerTreatmentOrderAction.userHourConsumeManual(new Date(hourConsumeTime), hourConsumeCount,hourConsumeBizSystem, function(r){
		if (r[0] != null && r[0] == "ok") {
			swal(r[1]);
      		$("#hourConsumeButton").removeAttr("disabled");
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
			$("#hourConsumeButton").removeAttr("disabled");
		}else{
			swal("计算请求失败.");
			$("#hourConsumeButton").removeAttr("disabled");
		}
    });
}

/**
 * 手动结算平台盈利
 */
TreatmentApplyManualPage.prototype.dayProfitManual = function(){
	var dayProfitTime = $("#dayProfitTime").val();
	var dayProfitCount = $("#dayProfitCount").val();
	var dayProfitBizSystem = $("#dayProfitBizSystem").val();
	if(dayProfitTime == null || dayProfitTime == "" || dayProfitCount == null || dayProfitCount== ""){
		swal("参数有误.");
		return;
	}
	$("#dayProfitButton").attr('disabled','disabled');
	managerTreatmentOrderAction.dayProfitManual(new Date(dayProfitTime), dayProfitCount,dayProfitBizSystem,function(r){
		if(r[0] != null && r[0] == "ok"){
			swal(r[1]);
			$("#dayProfitButton").removeAttr("disabled");
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
			$("#dayProfitButton").removeAttr("disabled");
		}else{
			swal("计算请求失败.");
			$("#dayProfitButton").removeAttr("disabled");
		}
	});
}


/**
 * 手动结算每日彩票赢率
 */
TreatmentApplyManualPage.prototype.lotteryDayGainManual = function(){
	var lotteryDayGainTime = $("#lotteryDayGainTime").val();
	var lotteryDayGainCount = $("#lotteryDayGainCount").val();
	var lotteryDayGainBizSystem = $("#lotteryDayGainBizSystem").val();
	if(lotteryDayGainTime == null || lotteryDayGainTime == "" || lotteryDayGainCount == null || lotteryDayGainCount== ""){
		swal("参数有误.");
		return;
	}
	$("#lotteryDayGainButton").attr('disabled','disabled');
	managerTreatmentOrderAction.lotteryDayGainManual(new Date(lotteryDayGainTime), lotteryDayGainCount,lotteryDayGainBizSystem,function(r){
		if(r[0] != null && r[0] == "ok"){
			swal(r[1]);
			$("#lotteryDayGainButton").removeAttr("disabled");
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
			$("#lotteryDayGainButton").removeAttr("disabled");
		}else{
			swal("计算请求失败.");
			$("#lotteryDayGainButton").removeAttr("disabled");
		}
	});
}


/**
 * 手动用户团队日盈亏计算
 */
TreatmentApplyManualPage.prototype.platformManual = function(){
	var platformTime = $("#platformTime").val();
	var platformBizSystem = $("#platformBizSystem").val();
	if(platformTime == null || platformTime == ""){
		 swal("参数有误！");
		return;
	}
	$("#platformButton").attr('disabled','disabled');
	platformTime=platformTime+"-01";
	managerTreatmentOrderAction.platformManual(new Date(platformTime), platformBizSystem, function(r){
		if (r[0] != null && r[0] == "ok") {
			 swal(r[1]);
      		$("#platformButton").removeAttr("disabled");
		}else if(r[0] != null && r[0] == "error"){
			 swal(r[1]);
			$("#platformButton").removeAttr("disabled");
		}else{
			 swal("开奖请求失败.");
			$("#platformButton").removeAttr("disabled");
		}
    });
}

/**
 * 手动结算活动彩金
 */
TreatmentApplyManualPage.prototype.activityMoneyManual = function(){
	var activityTime = $("#activityTime").val();
	var activityDateCount = $("#activityDateCount").val();
	var activityBizSystem = $("#activityBizSystem").val();
	if(activityTime == null || activityTime == "" || activityDateCount == null || activityDateCount== ""){
		swal("参数有误.");
		return;
	}
	$("#activityButton").attr('disabled','disabled');
	managerTreatmentOrderAction.activityMoneyManual(new Date(activityTime), activityDateCount,activityBizSystem,function(r){
		if(r[0] != null && r[0] == "ok"){
			swal(r[1]);
			$("#activityButton").removeAttr("disabled");
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
			$("#activityButton").removeAttr("disabled");
		}else{
			swal("计算请求失败.");
			$("#activityButton").removeAttr("disabled");
		}
	});
}

TreatmentApplyManualPage.prototype.syncChatRoomUserJobAll=function(){
	var bizSystem=$("#syncBizSystem").val();
	var synchronousMode=$("#synchronousMode").val();
	var userName=$("#userName").val()==""?null:$("#userName").val();
	if(bizSystem==""||bizSystem==null){
		swal("请选择业务系统");
		return;
	}
	managerTreatmentOrderAction.executAll(bizSystem,userName,synchronousMode,function(r){
		if(r[0]=="ok"){
			swal("全量同步完成");
		}else{
			swal(r[1]);
		}
	});
}

TreatmentApplyManualPage.prototype.syncChatRoomUserJob=function(){
	var bizSystem=$("#syncBizSystem").val();
	var synchronousMode=$("#synchronousMode").val();
	var userName=$("#userName").val()==""?null:$("#userName").val();
	if(bizSystem==""||bizSystem==null){
		swal("请选择业务系统");
		return;
	}
	managerTreatmentOrderAction.executAdd(bizSystem,userName,synchronousMode,function(r){
		if(r[0]=="ok"){
			swal("增量同步完成");
		}else{
			swal(r[1]);
		}
	});
}

TreatmentApplyManualPage.prototype.syncChatRoomDayOnlineinfoJob=function(){
	var bizSystem=$("#dayOnlineBizSystem").val();
	var time=$("#dayOnlineTime").val();
	if(time==""||time==null){
		swal("请选择每日在线统计时间");
		return;
	}
	managerTreatmentOrderAction.syncChatRoomDayOnlineinfoJob(bizSystem,new Date(time),function(r){
		if(r[0]=="ok"){
			swal(r[1]);
		}else{
			swal(r[1]);
		}
	});
}

TreatmentApplyManualPage.prototype.dataDelete=function(){
	managerTreatmentOrderAction.dataDelete(function(r){
		if(r[0]=="ok"){
			swal(r[1]);
		}else{
			swal(r[1]);
		}
	});
}

/**
 * 手动处理注册链接设置信息
 */
TreatmentApplyManualPage.prototype.dataProcess = function(){
	var treatmentBizSystem = $("#treatmentBizSystemTab13").val();
	managerTreatmentOrderAction.dataProcess(treatmentBizSystem,function(r){
		if(r[0]=="ok"){
			swal(r[1]);
		}else{
			swal(r[1]);
		}
	});
}

/**
 * 六合彩走势图设置信息
 */
TreatmentApplyManualPage.prototype.dataCode = function(){
//	var treatmentBizSystem = $("#treatmentBizSystemTab14").val();
	managerTreatmentOrderAction.dataCode(function(r){
		if(r[0]=="ok"){
			swal(r[1]);
		}else{
			swal(r[1]);
		}
	});
}