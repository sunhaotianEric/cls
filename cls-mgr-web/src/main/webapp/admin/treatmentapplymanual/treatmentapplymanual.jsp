<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
    
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>代理福利手动结算</title>
		<jsp:include page="/admin/include/include.jsp"></jsp:include>
		<link href="<%=path%>/css/plugins/iCheck/custom.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" rel="stylesheet">
</head>
    <body class="gray-bg">
        <div class="wrapper wrapper-content animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a data-toggle="tab" href="#tab-1" aria-expanded="true">代理福利手动结算</a></li>
                            <li class="">
                                <a data-toggle="tab" href="#tab-2" aria-expanded="false">用户团队日盈亏手动结算</a></li>
                            <li class="">
                                <a data-toggle="tab" href="#tab-3" aria-expanded="false">用户自身日盈亏手动结算</a></li>
  <!--                           <li class="">
                                <a data-toggle="tab" href="#tab-4" aria-expanded="false">用户团队日余额手动结算</a></li> -->
                            <li class="">
                                <a data-toggle="tab" href="#tab-5" aria-expanded="false">用户团队小时盈亏手动结算</a></li>
                            <li class="">
                                <a data-toggle="tab" href="#tab-6" aria-expanded="false">平台每日盈亏手动结算</a></li>
                           <li class="">
                                <a data-toggle="tab" href="#tab-7" aria-expanded="false">平台收费手动结算</a></li>
                           <li class="">
                                <a data-toggle="tab" href="#tab-8" aria-expanded="false">彩票每日赢率手动结算</a></li>
                           <li class="">
                                <a data-toggle="tab" href="#tab-9" aria-expanded="false">流水返送手动结算</a></li>
                           <li class="">
                                <a data-toggle="tab" href="#tab-10" aria-expanded="false">聊天室数据同步</a></li>
                           <li class="">
                                <a data-toggle="tab" href="#tab-11" aria-expanded="false">每日在线统计</a></li>
                           <li class="">
                                <a data-toggle="tab" href="#tab-12" aria-expanded="false">删除数据</a></li>
                           <li class="">
                                <a data-toggle="tab" href="#tab-13" aria-expanded="false">数据修复</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane active">
                                <div class="panel-body">
                                    <form class="form-horizontal">
                                        <div class="row">
                                           <div class="col-sm-2">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">商户</span>
                                                    <cls:bizSel name="treatmentBizSystem" id="treatmentBizSystem" options="class:ipt form-control"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">福利类型</span>
                                                    <select class="ipt form-control" id="treatmentType">
                                                        <option value="HALF_MONTH_BOUNS">契约分红</option>
                                                        <option value="DAY_SALARY">日工资</option>
                                                        </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">时间</span>
                                                    <input  class="laydate-icon form-control layer-date" id="dealTime" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})"></div>
                                            </div>
                                             
                                            <div class="col-sm-2">
                                                <div class="form-group text-center">
                                                    <div class="checkbox i-checks">
                                                        <label>
                                                            <input type="checkbox" name="isReleaseMoneySwitch">
                                                            <i>
                                                            </i>是否发放</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">发放策略</span>
                                                    <cls:enmuSel className="com.team.lottery.enums.EReleasePolicy" name="releasePolicy" id="releasePolicy" emptyOption="false" options="class:ipt form-control"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-group text-center">
                                                    <button id="bonusButton" type="button" class="btn btn-outline btn-default btn-js" onclick="treatmentApplyManualPage.treatmentApplyManual()">手动结算</button></div>
                                            </div>
                                        </div>
                                    </form>
                                    <h4 class="text-danger">温馨提醒：</h4>
                                    <ol>
                                        <li>福利类型选择半月分红时，时间只能选择1号或16号，如选择2015-03-01，结算时会计算2015-02-16至2015-02-28的数据，因为是下半月分红同时会累计 2015-02-01至2015-02-15的数据，所以如需要累计2015年2月份全部数据，需先选择2015-02-16来先发放2015-02-01至2015-02-15的上半月分红，然后再选择2015-03-01 发放下半月数据</li>
                                        <li>福利类型选择月工资时，发放的月工资是选择日期当月的数据，如选择2015-02-08，发放的月工资是2015-02-01至2015-02-28的数据</li></ol>
                                </div>
                            </div>
                            <div id="tab-2" class="tab-pane">
                                <div class="panel-body">
                                    <form class="form-horizontal">
                                        <div class="row">
                                          <div class="col-sm-3">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">商户</span>
                                                    <cls:bizSel name="dayConsumeBizSystem" id="dayConsumeBizSystem" options="class:ipt form-control"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">起始时间</span>
                                                    <input  class="laydate-icon form-control layer-date" id="dayConsumeTime" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})"></div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">计算天数</span>
                                                    <input type="text" value="" class="form-control" id="dayConsumeDays" onkeyup="checkNum(this)" ></div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group text-center">
                                                    <button id="dayConsumeButton" type="button" class="btn btn-outline btn-default btn-js" onclick="treatmentApplyManualPage.userDayConsumeManual()">手动结算</button></div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div id="tab-3" class="tab-pane">
                                <div class="panel-body">
                                    <form class="form-horizontal">
                                        <div class="row">
                                            <div class="col-sm-3">
                                              <div class="input-group m-b">
                                                    <span class="input-group-addon">商户</span>
                                                    <cls:bizSel name="daySelfConsumeBizSystem" id="daySelfConsumeBizSystem" options="class:ipt form-control"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">起始时间</span>
                                                    <input  class="laydate-icon form-control layer-date" id="daySelfConsumeTime" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})"></div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">计算天数</span>
                                                    <input type="text" value="" class="form-control" id="daySelfConsumeDays" onkeyup="checkNum(this)"></div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group text-center">
                                                    <button id="daySelfConsumeButton" type="button" class="btn btn-outline btn-default btn-js" onclick="treatmentApplyManualPage.userSelfDayConsumeManual()">手动结算</button></div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                         <!--      <div id="tab-4" class="tab-pane">
                                <div class="panel-body">
                                    <form class="form-horizontal">
                                        <div class="row">
                                          <div class="col-sm-2">
                                              <div class="input-group m-b">
                                                    <span class="input-group-addon">商户</span>
                                                    <cls:bizSel name="daySelfConsumeBizSystem" id="daySelfConsumeBizSystem" options="class:ipt form-control"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">起始时间</span>
                                                    <input  class="laydate-icon form-control layer-date" id="dayMoneyTime" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})"></div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">计算天数</span>
                                                    <input type="text" value="" class="form-control" id="daySelfConsumeDays" onkeyup="checkNum(this)"></div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group text-center">
                                                    <button type="button" class="btn btn-outline btn-default btn-js" onclick="treatmentApplyManualPage.userDayMoneyManual()">手动结算</button></div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>-->
                          <div id="tab-5" class="tab-pane">
                                <div class="panel-body">
                                    <form class="form-horizontal">
                                        <div class="row">
                                          <div class="col-sm-3">
                                              <div class="input-group m-b">
                                                    <span class="input-group-addon">商户</span>
                                                    <cls:bizSel name="hourConsumeBizSystem" id="hourConsumeBizSystem" options="class:ipt form-control"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">起始时间</span>
                                                    <input  class="laydate-icon form-control layer-date" id="hourConsumeTime" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"></div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">计算小时</span>
                                                    <input type="text" value="" class="form-control" id="hourConsumeCount" onkeyup="checkNum(this)"></div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group text-center">
                                                    <button id="hourConsumeButton" type="button" class="btn btn-outline btn-default btn-js" onclick="treatmentApplyManualPage.userHourConsumeManual()">手动结算</button></div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div> 
                            <div id="tab-6" class="tab-pane">
                                <div class="panel-body">
                                    <form class="form-horizontal">
                                        <div class="row">
                                        <div class="col-sm-3">
                                              <div class="input-group m-b">
                                                    <span class="input-group-addon">商户</span>
                                                    <cls:bizSel name="dayProfitBizSystem" id="dayProfitBizSystem" options="class:ipt form-control"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">起始时间</span>
                                                    <input  class="laydate-icon form-control layer-date" id="dayProfitTime" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})"></div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">计算天数</span>
                                                    <input type="text" value="" class="form-control" id="dayProfitCount" onkeyup="checkNum(this)"></div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group text-center">
                                                    <button id="dayProfitButton" type="button" class="btn btn-outline btn-default btn-js" onclick="treatmentApplyManualPage.dayProfitManual()">手动结算</button></div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div id="tab-7" class="tab-pane">
                                <div class="panel-body">
                                    <form class="form-horizontal">
                                        <div class="row">
                                          <div class="col-sm-4">
                                              <div class="input-group m-b">
                                                    <span class="input-group-addon">商户</span>
                                                    <cls:bizSel name="platformBizSystem" id="platformBizSystem" options="class:ipt form-control"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">归属时间</span>
                                                    <input  class="laydate-icon form-control layer-date" id="platformTime" onclick="laydate({istime: true, format: 'YYYY-MM'})"></div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group text-center">
                                                    <button id="platformButton" type="button" class="btn btn-outline btn-default btn-js" onclick="treatmentApplyManualPage.platformManual()">手动结算</button></div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div id="tab-8" class="tab-pane">
                                <div class="panel-body">
                                    <form class="form-horizontal">
                                        <div class="row">
                                        <div class="col-sm-3">
                                              <div class="input-group m-b">
                                                    <span class="input-group-addon">商户</span>
                                                    <cls:bizSel name="lotteryDayGainBizSystem" id="lotteryDayGainBizSystem" options="class:ipt form-control"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">起始时间</span>
                                                    <input  class="laydate-icon form-control layer-date" id="lotteryDayGainTime" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})"></div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">计算天数</span>
                                                    <input type="text" value="" class="form-control" id="lotteryDayGainCount" onkeyup="checkNum(this)"></div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group text-center">
                                                    <button id="lotteryDayGainButton" type="button" class="btn btn-outline btn-default btn-js" onclick="treatmentApplyManualPage.lotteryDayGainManual()">手动结算</button></div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        	<div id="tab-9" class="tab-pane">
                                <div class="panel-body">
                                    <form class="form-horizontal">
                                        <div class="row">
                                        <div class="col-sm-3">
                                              <div class="input-group m-b">
                                                    <span class="input-group-addon">商户</span>
                                                    <cls:bizSel name="activityBizSystem" id="activityBizSystem" options="class:ipt form-control"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">起始时间</span>
                                                    <input  class="laydate-icon form-control layer-date" id="activityTime" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})"></div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">计算天数</span>
                                                    <input type="text" value="" class="form-control" id="activityDateCount" onkeyup="checkNum(this)"></div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group text-center">
                                                    <button id="activityButton" type="button" class="btn btn-outline btn-default btn-js" onclick="treatmentApplyManualPage.activityMoneyManual()">手动结算</button></div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>   
                            <div id="tab-10" class="tab-pane">
                                <div class="panel-body">
                                    <form class="form-horizontal">
                                        <div class="row">
                                        	<div class="col-sm-3">
                                              <div class="input-group m-b">
                                                    <span class="input-group-addon">商户</span>
                                                    <cls:bizSel name="syncBizSystem" id="syncBizSystem" options="class:ipt form-control"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">代理线用户名</span>
                                                    <input type="text" value="" class="form-control" id="userName"></div>
                                            </div>
                                            <div class="col-sm-3">
                                              <div class="input-group m-b">
                                                    <span class="input-group-addon">同步方式</span>
                                                    <select class="ipt form-control" id="synchronousMode">
                                                        <option value="0">删除已有数据同步</option>
                                                        <option value="1">保留数据同步</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group text-center">
                                                    <button id="activityButton" type="button" class="btn btn-outline btn-default btn-js" onclick="treatmentApplyManualPage.syncChatRoomUserJobAll()">全量同步</button>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                                    <button id="activityButton" type="button" class="btn btn-outline btn-default btn-js" onclick="treatmentApplyManualPage.syncChatRoomUserJob()">增量同步</button></div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>   
                            <div id="tab-11" class="tab-pane">
                                <div class="panel-body">
                                    <form class="form-horizontal">
                                        <div class="row">
                                          <div class="col-sm-4">
                                              <div class="input-group m-b">
                                                    <span class="input-group-addon">商户</span>
                                                    <cls:bizSel name="dayOnlineBizSystem" id="dayOnlineBizSystem" options="class:ipt form-control"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">归属时间</span>
                                                    <input  class="laydate-icon form-control layer-date" id="dayOnlineTime" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})"></div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group text-center">
                                                    <button id="platformButton" type="button" class="btn btn-outline btn-default btn-js" onclick="treatmentApplyManualPage.syncChatRoomDayOnlineinfoJob()">手动统计</button></div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div id="tab-12" class="tab-pane">
                                <div class="panel-body">
                                    <form class="form-horizontal">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group text-center">
                                                    <button id="platformButton" type="button" class="btn btn-outline btn-default btn-js" onclick="treatmentApplyManualPage.dataDelete()">手动删除</button></div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>                            
                            <div id="tab-13" class="tab-pane">
                                <div class="panel-body">
                                    <form class="form-horizontal">
                                        <div class="row">
                                        <div class="col-sm-2">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">商户</span>
                                                    <cls:bizSel name="treatmentBizSystemTab13" id="treatmentBizSystemTab13" options="class:ipt form-control"/>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group text-center">
                                                    <button id="dataProcessButton" type="button" class="btn btn-outline btn-default btn-js" onclick="treatmentApplyManualPage.dataProcess()">执行</button></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                                <div class="form-group text-center">
                                                    <button id="dataProcessButton" type="button" class="btn btn-outline btn-default btn-js" onclick="treatmentApplyManualPage.dataCode()">处理六合彩</button></div>
                                        </div>
                                    </form>
                                </div>
                            </div>                                
                        </div>
                    </div>
                </div>
            </div>
        </div>
   <script src="<%=path%>/js/plugins/iCheck/icheck.min.js"></script>
<!-- js -->
<script type="text/javascript" src="<%=path%>/admin/treatmentapplymanual/js/treatmentapplymanual.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerTreatmentOrderAction.js'></script>
	<script>$(document).ready(function() {
	        $(".i-checks").iCheck({
	            checkboxClass: "icheckbox_square-green",
	            radioClass: "iradio_square-green",
	        })
	    });</script>
</body>
</html>

