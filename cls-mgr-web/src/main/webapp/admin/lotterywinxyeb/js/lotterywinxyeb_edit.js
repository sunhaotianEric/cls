function EditLotteryWinXyebPage() {
}
var editLotteryWinXyebPage = new EditLotteryWinXyebPage();

editLotteryWinXyebPage.param = {

};

/**
 * 查询参数
 */
editLotteryWinXyebPage.queryParam = {
	lotteryType : null
};


$(document).ready(function() {
	var id = editLotteryWinXyebPage.param.id; 
	if(typeof(id) != "undefined" && id != null){
		editLotteryWinXyebPage.getLotteryWinXyebById(id); // 查询所有的奖金配置数据
	}
});

/**
 * 赋值赔率配置
 * @param id
 */
EditLotteryWinXyebPage.prototype.getLotteryWinXyebById = function(id){
	managerLotteryWinXyebAction.getLotteryWinXyebById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			if(currentUser.bizSystem!="SUPER_SYSTEM"){
				$("#lotteryNameId").attr("readOnly","readonly");
				$("#code").attr("readOnly","readonly");
				$("#codeDes").attr("readOnly","readonly");
			}
			$("#bizSystemId").val(r[1].bizSystem);
			$("#bizSystemId").attr("readOnly","readonly");
			$("#lotteryNameId").find("option[value='"+r[1].lotteryTypeProtype+"']").attr("selected","selected");
			$("#code").val(r[1].code);
			$("#codeDes").val(r[1].codeDes);
			$("#winMoney").val(r[1].winMoney);
			$("#returnPercent").val(r[1].returnPercent);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取数据失败.");
		}
    });
}

/**
 * 编辑修改赔率配置数据
 */
EditLotteryWinXyebPage.prototype.editLotteryWinXyeb = function(){
	var lotteryWinXyeb = {};
	lotteryWinXyeb.id = editLotteryWinXyebPage.param.id; 
	lotteryWinXyeb.bizSystem = $("#bizSystemId").val();
	lotteryWinXyeb.lotteryTypeProtype = $("#lotteryNameId").val();
	lotteryWinXyeb.lotteryName = $("#lotteryNameId").find("option:selected").text();
	lotteryWinXyeb.code = $("#code").val();
	lotteryWinXyeb.codeDes = $("#codeDes").val();
	lotteryWinXyeb.winMoney = $("#winMoney").val();
	lotteryWinXyeb.returnPercent = $("#returnPercent").val();
	
	
	if(currentUser.bizSystem=="SUPER_SYSTEM"){
		if (lotteryWinXyeb.bizSystem==null || lotteryWinXyeb.bizSystem.trim()=="") {
			swal("请选择子系统!");
			$('#bizSystem').focus();
			return;
		}
	}

	if (lotteryWinXyeb.lotteryName==null || lotteryWinXyeb.lotteryName.trim()=="") {
		swal("请选择玩法!");
		$('#lotteryNameId').focus();
		return;
	}
	if (lotteryWinXyeb.code==null || lotteryWinXyeb.code.trim()=="") {
		swal("请输入开奖号码数!");
		$('#code').focus();
		return;
	}
	if (lotteryWinXyeb.codeDes==null || lotteryWinXyeb.codeDes.trim()=="") {
		swal("请输入号码描述!");
		$('#codeDes').focus();
		return;
	}
	if (lotteryWinXyeb.winMoney==null || lotteryWinXyeb.winMoney.trim()=="") {
		swal("请输入奖金金额!");
		$('#winMoney').focus();
		return;
	}
	if (lotteryWinXyeb.returnPercent==null || lotteryWinXyeb.returnPercent.trim()=="") {
		swal("请输入返水!");
		$('#returnPercent').focus();
		return;
	}
	
	managerLotteryWinXyebAction.updateLotteryWinXyeb(lotteryWinXyeb,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.lotteryWinXyebPage.closeLayer(index);
		   	    });
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("修改数据失败.");
		}
    });
};
