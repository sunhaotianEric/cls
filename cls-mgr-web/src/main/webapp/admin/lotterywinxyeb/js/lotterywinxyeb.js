function LotteryWinXyebPage(){}
var lotteryWinXyebPage = new LotteryWinXyebPage();

/**
 * 查询参数
 */
lotteryWinXyebPage.queryParam = {
	bizSystem : null,
	lotteryTypeProtype : null,
	lotteryName : null
	
};
lotteryWinXyebPage.param = {
	codeNum:43  //号码个数	
}
//分页参数
lotteryWinXyebPage.pageParam = {
    pageNo : 1,
    pageSize : defaultPageSize
};

$(document).ready(function() {
	lotteryWinXyebPage.queryConditionLotteryCodes();
	
	// 赔率修改
	$('.btn-peilv').on('click',function(){
	    var oldValue = $(this).closest('span').find('.oldValue').val();
	    var newValue = $(this).closest('span').find('.newValue').val();
	    if(currentUser.bizSystem == "SUPER_SYSTEM"){
		    $('.table-block .table tr td:nth-child(3)').each(function(i, item) {
		        if ($(item).find('input').val() == oldValue) {
		            $(item).find('input').val(newValue);
		        }
		    });
	    }else{
	    	$('.table-block .table tr td:nth-child(2)').each(function(i, item) {
		        if ($(item).find('input').val() == oldValue) {
		            $(item).find('input').val(newValue);
		        }
		    });
	    }
	})
	// 返水修改
	$('.btn-fanshui').on('click',function(){
	    var oldValue = $(this).closest('span').find('.oldValue').val();
	    var newValue = $(this).closest('span').find('.newValue').val();
	    if(currentUser.bizSystem == "SUPER_SYSTEM"){
		    $('.table-block .table tr td:nth-child(4)').each(function(i, item) {
		        if ($(item).find('input').val() == oldValue) {
		            $(item).find('input').val(newValue);
		        }
		    });
	    }else {
	    	$('.table-block .table tr td:nth-child(3)').each(function(i, item) {
		        if ($(item).find('input').val() == oldValue) {
		            $(item).find('input').val(newValue);
		        }
		    });
	    }
	})
	
	// 点击底色变亮
	$('.table-block .table tbody').on('click','tr td',function(){
		$('.table-block table tr').removeClass('on')
		$(this).parent('tr').addClass('on');
	})
});





LotteryWinXyebPage.prototype.initTableHTML = function(obj,id) {
	var readOnly = "";
	if(currentUser.bizSystem != "SUPER_SYSTEM"){
		readOnly = "readonly='readonly'";
	}
	var table1 = $("#"+id+"Table1");
	var table2 = $("#"+id+"Table2");
	var table3 = $("#"+id+"Table3");
	table1.html("");
	table2.html("");
	table3.html("");
	var addk= 0;
	if(id == 'tmsb'){
		addk = 28;
	}else if(id == 'hh'){
		addk = 29;
	}else if(id == 'bs'){
		addk = 39;
	}else if(id == 'bz'){
		addk = 42;
	}
	
	var len = lotteryWinXyebPage.param.codeNum % 3 ;
	for(var i =0;i<obj.length;i=i+3){
		var tr1 = "<tr>";
		var tr2 = "<tr>";
		var tr3 = "<tr>";
		k=i;
		if(currentUser.bizSystem == "SUPER_SYSTEM"){
			tr1+= "<td><input id='cid"+(k+addk)+"' type='hidden' value='"+obj[k].id+"' />" +
					"<input id='code"+(k+addk)+"' type='text' value='"+obj[k].code+"' "+readOnly+" style='background-color: #b5ecc4;'/></td>";
			tr1+= "<td><input id='codeDes"+(k+addk)+"' type='text' value='"+obj[k].codeDes+"' "+readOnly+" /></td>";
		}else{
			tr1+= "<td style='background-color: #b5ecc4;'><input id='cid"+(k+addk)+"' type='hidden' value='"+obj[k].id+"' />"+obj[k].codeDes+"</td>";
		}
		
		tr1+= "<td><input id='winMoney"+(k+addk)+"' type='text' value='"+obj[k].winMoney+"' /></td>";
		tr1+= "<td><input id='returnPercent"+(k+addk)+"' type='text' value='"+obj[k].returnPercent+"' /> %</td></tr>";
		k=i+1;
		if(k<obj.length){
			if(currentUser.bizSystem == "SUPER_SYSTEM"){
				tr2+= "<td><input id='cid"+(k+addk)+"' type='hidden' value='"+obj[k].id+"' />" +
				      "<input id='code"+(k+addk)+"' type='text' value='"+obj[k].code+"' "+readOnly+" style='background-color: #b5ecc4;'/></td>";	
				tr2+= "<td><input id='codeDes"+(k+addk)+"' type='text' value='"+obj[k].codeDes+"' "+readOnly+" /></td>";
			}else{
				tr2+= "<td style='background-color: #b5ecc4;'><input id='cid"+(k+addk)+"' type='hidden' value='"+obj[k].id+"' />"+obj[k].codeDes+"</td>";
			}
			
			tr2+= "<td><input id='winMoney"+(k+addk)+"' type='text' value='"+obj[k].winMoney+"' /></td>";
			tr2+= "<td><input id='returnPercent"+(k+addk)+"' type='text' value='"+obj[k].returnPercent+"' /> %</td></tr>";	
		}else{
			if(currentUser.bizSystem == "SUPER_SYSTEM"){
				tr2+= "<td><input readonly='readonly'/></td>";	
				tr2+= "<td><input readonly='readonly' /></td>";
			}else{
				tr2+= "<td><input readonly='readonly' /></td>";
			}
			
			tr2+= "<td><input readonly='readonly' /></td>";
			tr2+= "<td><input readonly='readonly' /></tr>";	
		}
		
		
		
		k=i+2;
        if(k<obj.length){
        	if(currentUser.bizSystem == "SUPER_SYSTEM"){
    			tr3+= "<td><input id='cid"+(k+addk)+"' type='hidden' value='"+obj[k].id+"' />" +
				      "<input id='code"+(k+addk)+"' type='text' value='"+obj[k].code+"' "+readOnly+" style='background-color: #b5ecc4;'/></td>";	
    			tr3+= "<td><input id='codeDes"+(k+addk)+"' type='text' value='"+obj[k].codeDes+"' "+readOnly+" /></td>";
			}else{
				tr3+= "<td style='background-color: #b5ecc4;'><input id='cid"+(k+addk)+"' type='hidden' value='"+obj[k].id+"' />"+obj[k].codeDes+"</td>";
			}
    		tr3+= "<td><input id='winMoney"+(k+addk)+"' type='text' value='"+obj[k].winMoney+"' /></td>";
    		tr3+= "<td><input id='returnPercent"+(k+addk)+"' type='text' value='"+obj[k].returnPercent+"' /> %</td></tr>";
		}else{
			if(currentUser.bizSystem == "SUPER_SYSTEM"){
				tr3+= "<td><input readonly='readonly' /></td>";	
				tr3+= "<td><input readonly='readonly' /></td>";
			}else{
				tr3+= "<td><input readonly='readonly' /></td>";
			}
			
			tr3+= "<td><input readonly='readonly' /></td>";
			tr3+= "<td><input readonly='readonly' /></tr>";	
		} 
        table1.append(tr1);
        table2.append(tr2);
        table3.append(tr3);
	}
}



/**
 * 条件查询赔率
 */
LotteryWinXyebPage.prototype.queryConditionLotteryCodes = function(){
	lotteryWinXyebPage.queryParam = getFormObj($("#lotteryWinXyebForm"));
	//查询条件取玩法的值字段
	lotteryWinXyebPage.queryParam.lotteryTypeProtype = $("#lotteryNameId").val();
	//名称不作为查询条件
	lotteryWinXyebPage.queryParam.lotteryName = null;
	managerLotteryWinXyebAction.getLotteryWinXyebByQueryCondition(lotteryWinXyebPage.queryParam,function(r){
		if (r[0] != null && r[0] == "ok") {
			lotteryWinXyebPage.initTableHTML(r[1],'tm');
			lotteryWinXyebPage.initTableHTML(r[2],'tmsb');
			lotteryWinXyebPage.initTableHTML(r[3],'hh');
			lotteryWinXyebPage.initTableHTML(r[4],'bs');
			lotteryWinXyebPage.initTableHTML(r[5],'bz');
			$('.table-block table tr td input').focus(function(){
				$('.table-block table tr').removeClass('on')
				$(this).parents('tr').addClass('on');
			});
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("查询数据失败.");
		}
	});
};

//查询配置数据
LotteryWinXyebPage.prototype.closeLayer=function(index){
	layer.close(index);
	lotteryWinXyebPage.queryConditionLotteryCodes(); 
}

LotteryWinXyebPage.prototype.getLotteryWinXyebData = function(){
	var XyebArr = new Array();
	var num = lotteryWinXyebPage.param.codeNum;
	for(var i=0 ;i<num;i++){
		var lotteryWinXyeb = {}
		var cid = $("#cid"+i).val();
		if(currentUser.bizSystem == "SUPER_SYSTEM"){
			var code = $("#code"+i).val();
			if(code == null || code == ""){
				swal("号码不为空");
				$("#code"+i).focus();
				return ;
			}
			lotteryWinXyeb.code= code;
			var codeDes = $("#codeDes"+i).val();
			if(codeDes == null || codeDes == ""){
				swal("描述不为空");
				$("#codeDes"+i).focus();
				return ;
			}
			lotteryWinXyeb.codeDes= codeDes;
		}
		
		var winMoney = $("#winMoney"+i).val();
		var returnPercent = $("#returnPercent"+i).val();
		
		if(winMoney == null || winMoney == ""){
			swal("赔率不为空");
			$("#winMoney"+i).focus();
			return ;
		}
		if(returnPercent == null || returnPercent == ""){
			swal("返水不为空");
			$("#returnPercent"+i).focus();
			return ;
		}
		lotteryWinXyeb.id= cid;
		lotteryWinXyeb.winMoney= winMoney;
		lotteryWinXyeb.returnPercent= returnPercent;
		XyebArr[i]=lotteryWinXyeb;
	}
	
	return XyebArr;
}


/**
 * 编辑修改赔率配置数据
 */
LotteryWinXyebPage.prototype.editLotteryWinXyeb = function(){
	
	var XyebArr = lotteryWinXyebPage.getLotteryWinXyebData();
	
	
	
	var lotteryWinXyebParam = {};
	lotteryWinXyebParam.lotteryWinXyebArr = XyebArr;
	

	
	managerLotteryWinXyebAction.updateLotteryWinXyebByCodes(lotteryWinXyebParam,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	                lotteryWinXyebPage.closeLayer(index);
		   	    });
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("修改奖金配置数据失败.");
		}
    });
};




/**
 * 删除对应的赔率数据
 */
LotteryWinXyebPage.prototype.deleteLotteryWinXyebById = function(lotteryWinXyebId){
	managerLotteryWinXyebAction.deleteLotteryWinXyebById(lotteryWinXyebId,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({ title: "提示", text: "删除成功",type: "success"});
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("查询数据失败.");
		}
    });
};

/**
 * 编辑赔率信息
 */
LotteryWinXyebPage.prototype.editLotterywinXyeb = function(id){
	layer.open({
		type: 2,
		title: '编辑幸运28赔率',
		maxmin: false,
		shadeClose: true,
		//点击遮罩关闭层
		area: ['70%', '80%'],
		content:contextPath + '/managerzaizst/lotterywinxyeb/'+ id +'/lotterywinxyeb_edit.html',
	})
}

/**
 * 添加赔率信息
 */
LotteryWinXyebPage.prototype.addLotterywinXyeb = function(){
	layer.open({
		type: 2,
		title: '新增幸运28赔率',
		maxmin: false,
		shadeClose: true,
		//点击遮罩关闭层
		area: ['70%', '80%'],
		content:contextPath + '/managerzaizst/lotterywinxyeb/lotterywinxyeb_add.html',
	})
}
