<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
  	<%
	 	String id = request.getParameter("id");  //获取查询的商品ID
	%>
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeIn">
 <form class="form-horizontal">
    <div class="row">
	    <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
	    <div class="col-sm-12">
            <div class="input-group m-b" draggable="false">
                <span class="input-group-addon">商户：</span>
                  <cls:bizSel name="bizSystem" id="bizSystemId" options="class:ipt form-control"/>
            </div>
		</div>    
    </c:if>
        <div class="col-sm-6">
            <div class="input-group m-b">
                <span class="input-group-addon">玩法</span>
                <select class="ipt form-control" name="lotteryName" id="lotteryNameId">
                  	<option value="" selected="selected">==请选择==</option>
                   	<option value="TM">特码</option>
                   	<option value="TMSB">特码三包</option>
                   	<option value="HH">混合</option>
                   	<option value="BS">波色</option>
                   	<option value="BZ">豹子</option>
                </select>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="input-group m-b">
                <span class="input-group-addon">号码</span>
                <input type="text" value="" class="form-control" id="code"></div>
        </div>
        <div class="col-sm-6">
            <div class="input-group m-b">
                <span class="input-group-addon">号码描述</span>
                <input type="text" value="" class="form-control"  id="codeDes"></div>
        </div>
        <div class="col-sm-6">
            <div class="input-group m-b">
                <span class="input-group-addon">赔率</span>
                <input type="text" value="" class="form-control" id="winMoney"></div>
        </div>
        <div class="col-sm-6">
            <div class="input-group m-b">
                <span class="input-group-addon">返水</span>
                <input type="text" value=""  class="form-control"  id="returnPercent"><span class="input-group-addon">%</span></div>
        </div>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-6 text-center">
                    <button type="button" class="btn btn-w-m btn-white" onclick="editLotteryWinXyebPage.editLotteryWinXyeb()">提 交</button></div>
                <div class="col-sm-6 text-center">
                    <button type="reset" class="btn btn-w-m btn-white">重 置</button></div>
            </div>
        </div>
    </div>
  </form>
</div>
<!-- js -->
<script type="text/javascript" src="<%=path%>/admin/lotterywinxyeb/js/lotterywinxyeb_edit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerLotteryWinXyebAction.js'></script>
<script type="text/javascript">
editLotteryWinXyebPage.param.id = <%=id%>;
</script>
</body>

</html>