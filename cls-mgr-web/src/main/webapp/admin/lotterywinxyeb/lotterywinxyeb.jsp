<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>幸运28赔率管理</title>
	<link rel="shortcut icon" href="favicon.ico">
	<jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
<body>
	<div class="animated fadeIn">
		<div class="ibox-content">
			<div class="row m-b-sm m-t-sm">
				<form class="form-horizontal" id="lotteryWinXyebForm">
					<div class="row">
						<c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
							<div class="col-sm-4 biz">
								<div class="input-group m-b">
									<span class="input-group-addon">商户：</span>
									<cls:bizSel name="bizSystem" id="bizSystem"
										options="class:ipt form-control,onChange:lotteryWinXyebPage.queryConditionLotteryCodes()"
										emptyOption="false" />
								</div>
							</div>
							<div class="col-sm-2">
								<div class="form-group text-left ml0">
									<button type="button"
										class="btn btn-outline btn-default btn-js"
										onclick="lotteryWinXyebPage.queryConditionLotteryCodes()">
										<i class="fa fa-search"></i>&nbsp;查 询
									</button>
									<c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
										<button type="button"
											class="btn btn-outline btn-default btn-kj"
											onclick="lotteryWinXyebPage.addLotterywinXyeb()">
											<i class="fa fa-plus"></i>&nbsp;新增
										</button>
									</c:if>
								</div>
							</div>	
						</c:if>
						<%--         <c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
	                         <div class="col-sm-8">
	                             <div class="form-group text-right">
	                                 <button type="button" class="btn btn-outline btn-default btn-js" onclick="lotteryWinXyebPage.queryConditionLotteryCodes()"><i class="fa fa-search"></i>&nbsp;查 询</button>
	                             </div>
	                         </div>
                        </c:if> --%>
					</div>
				</form>
			</div>
			<div class="table-block clear">
				<table class="table">
					<thead>
						<tr>
							<c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
								<th>号码</th>
							</c:if>
							<th>描述</th>
							<th>赔率</th>
							<th>返水</th>
						</tr>
					</thead>
					<tbody class="play-way">
						<tr>
							<td colspan='4'>特码</td>
						</tr>
					</tbody>
					<tbody id="tmTable1">

					</tbody>
					<tbody class="play-way">
						<tr>
							<td colspan='4'>特码包三</td>
						</tr>
					</tbody>
					<tbody id="tmsbTable1">

					</tbody>
					<tbody class="play-way">
						<tr>
							<td colspan='4'>混合</td>
						</tr>
					</tbody>
					<tbody id="hhTable1">

					</tbody>
					<tbody class="play-way">
						<tr>
							<td colspan='4'>波色</td>
						</tr>
					</tbody>
					<tbody id="bsTable1">

					</tbody>
					<tbody class="play-way">
						<tr>
							<td colspan='4'>豹子</td>
						</tr>
					</tbody>
					<tbody id="bzTable1">

					</tbody>
				</table>
				<table class="table">
					<thead>
						<tr>
							<c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
								<th>号码</th>
							</c:if>
							<th>描述</th>
							<th>赔率</th>
							<th>返水</th>
						</tr>
					</thead>
					<tbody class="play-way">
						<tr>
							<td colspan='4'>特码</td>
						</tr>
					</tbody>
					<tbody id="tmTable2">

					</tbody>
					<tbody class="play-way">
						<tr>
							<td colspan='4'>特码包三</td>
						</tr>
					</tbody>
					<tbody id="tmsbTable2">

					</tbody>
					<tbody class="play-way">
						<tr>
							<td colspan='4'>混合</td>
						</tr>
					</tbody>
					<tbody id="hhTable2">

					</tbody>
					<tbody class="play-way">
						<tr>
							<td colspan='4'>波色</td>
						</tr>
					</tbody>
					<tbody id="bsTable2">

					</tbody>
					<tbody class="play-way">
						<tr>
							<td colspan='4'>豹子</td>
						</tr>
					</tbody>
					<tbody id="bzTable2">

					</tbody>
				</table>
				<table class="table">
					<thead>
						<tr>
							<c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
								<th>号码</th>
							</c:if>
							<th>描述</th>
							<th>赔率</th>
							<th>返水</th>
						</tr>
					</thead>
					<tbody class="play-way">
						<tr>
							<td colspan='4'>特码</td>
						</tr>
					</tbody>
					<tbody id="tmTable3">

					</tbody>
					<tbody class="play-way">
						<tr>
							<td colspan='4'>特码包三</td>
						</tr>
					</tbody>
					<tbody id="tmsbTable3">

					</tbody>
					<tbody class="play-way">
						<tr>
							<td colspan='4'>混合</td>
						</tr>
					</tbody>
					<tbody id="hhTable3">

					</tbody>
					<tbody class="play-way">
						<tr>
							<td colspan='4'>波色</td>
						</tr>
					</tbody>
					<tbody id="bsTable3">

					</tbody>
					<tbody class="play-way">
						<tr>
							<td colspan='4'>豹子</td>
						</tr>
					</tbody>
					<tbody id="bzTable3">

					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="fixed-footer">
		<div class="row-edit">
			<b style="font-size: 15px;">快捷修改：</b> <span>赔率：把 <input
				type="text" class="oldValue" /> 改成 <input type="text"
				class="newValue" />
			<button class="btn btn-sm btn-primary btn-peilv">修改</button></span> <span>返水：把
				<input type="text" class="oldValue" /> 改成 <input type="text"
				class="newValue" />
				<button class="btn btn-sm btn-primary btn-fanshui">修改</button>
			</span>
		</div>
		<div class="btn-block">
			<button class="btn btn-sm btn-primary m-t-n-xs" type="submit"
				onclick="lotteryWinXyebPage.editLotteryWinXyeb()">
				<strong>确认提交</strong>
			</button>
			（友情提示：修改完需确认提交）</span>
		</div>
	</div>

	<!-- dwr -->
	<script type='text/javascript'
		src='<%=path%>/dwr/interface/managerLotteryWinXyebAction.js'></script>
	<script type="text/javascript"
		src="<%=path%>/admin/lotterywinxyeb/js/lotterywinxyeb.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
</body>

</html>
