function CommonPage(){}
var commonPage = new CommonPage();

commonPage.param = {
	 fixVaue : 3
};

/**
 * dataTable全局分页配置
 */
dataTableLanguage = {
		"sLengthMenu" : "每页显示 _MENU_ 条记录",
		"sZeroRecords" : "抱歉， 没有找到",
		"sInfo" : "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
		"sInfoEmpty" : "没有数据",
		"sInfoFiltered" : "(从 _MAX_ 条数据中检索)",
		"oPaginate" : {
			"sFirst" : "首页",
			"sPrevious" : "前一页",
			"sNext" : "后一页",
			"sLast" : "尾页"
		},
		"sZeroRecords" : "没有检索到数据",
		//"sProcessing" : "<img src='../img/loading.gif' />"
	};

dataTableSettings = {
		"bPaginate" : true,// 分页按钮
		"bFilter" : false,// 搜索栏
		"bSort": false,   //排序功能  
		"bLengthChange" : true,// 每行显示记录数
		"iDisplayLength" : 30,// 每页显示行数
		"bInfo" : true,// Showing 1 to 10 of 23 entries 总记录数没也显示多少等信息
		"sPaginationType" : "full_numbers", // 分页，一共两种样式 另一种为two_button // 是datatables默认
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true
};

// 新增属性
$.extend($.fn.dataTable.defaults, {"pagingType": "input"})


/**
 * 显示用户详情
 */
CommonPage.prototype.showUserMsgPage = function(obj, userId, userName){
	managerUserAction.getUser(userId, userName, function(r){
		if (r[0] != null && r[0] == "ok") {
			var userMsg = r[1];
			var userBannks = r[3];
			var userMoneyTotal=r[4];
			//个人盈利计算： 总充值赠送+总活动彩金+总系统续费+总中奖+总返点+总提成+总转账转入+总分红+总日工资+总回退+总追号撤单-总投注-总转账转出-总系统扣费  -提现手续费
			var totalGain = (userMoneyTotal.totalRechargePresent + userMoneyTotal.totalActivitiesMoney + userMoneyTotal.totalSystemAddMoney //+ report.remainExtend 
								+ userMoneyTotal.totalRebate + userMoneyTotal.totalPercentage + userMoneyTotal.totalWin
								+ userMoneyTotal.totalIncomeTranfer + userMoneyTotal.totalHalfMonthBonus  + userMoneyTotal.totalDaySalary + userMoneyTotal.totalLotteryRegression +userMoneyTotal.totalLotteryStopAfterNumber
								- userMoneyTotal.totalLottery - userMoneyTotal.totalPayTranfer - userMoneyTotal.totalSystemReduceMoney - userMoneyTotal.totalWithdarwFee).toFixed(commonPage.param.fixVaue);
           var otherMoney = (userMoneyTotal.totalActivitiesMoney+userMoneyTotal.totalHalfMonthBonus  + userMoneyTotal.totalDaySalary).toFixed(commonPage.param.fixVaue);			
           var obj = $("#modal-form");
		   obj.remove();
			var str =" <div id='modal-form' class='modal fade' aria-hidden='true'>";
			str +="	<div class='modal-dialog' style='width:90%'>";
			str +="		<div class='modal-content' style='overflow:auto;'>";
			str +="			<div class='modal-header'>";
			str +="				<button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>";
			str +="			</div>";
			str +="			<div class='modal-body'>";
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>会员名称：</span>";
			str +="						<span class='form-control' >"+userMsg.userName+"</span></div>";
			str +="				</div>";
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>真实姓名：</span>";
			str +="						<span class='form-control' >"+(userMsg.trueName == null?"无":userMsg.trueName)+"</span></div>";
			str +="				</div>";
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>代理级别：</span>";
			str +="						<span class='form-control' >"+userMsg.dailiLevelStr+"</span></div>";
			str +="				</div>";
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>余额：</span>";
			str +="						<span class='form-control' >"+userMsg.money.toFixed(3)+"</span></div>";
			str +="				</div>";
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>可提现金额：</span>";
			str +="						<span class='form-control' >"+ userMsg.canWithdrawMoney.toFixed(commonPage.param.fixVaue) +"</span></div>";
			str +="				</div>";                                     
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>推荐人：</span>";
			var regfrom = userMsg.regfrom;
			if(userMsg.regfrom != null && typeof(regfrom) != "undefined"){
				regfrom = userMsg.regfrom.substring(0,35);
			}
			str +="						<span class='form-control' title='"+userMsg.regfrom+"'>"+(userMsg.regfrom == null?"无":regfrom)+"</span></div>";
			str +="				</div>";
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>VIP等级/积分：</span>";
			str +="						<span class='form-control' >VIP"+userMsg.vipLevel+"/"+Math.floor(userMsg.point)+"</span></div>";
			str +="				</div>";
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>状态：</span>";
			if(userMsg.loginLock != 0 && userMsg.withdrawLock !=0){
    			str +="						<span class='form-control sd' >登录提现锁定</span></div>";
    		}else if(userMsg.loginLock != 0){
    			str +="						<span class='form-control sd' >登录锁定</span></div>";
    		}else if(userMsg.withdrawLock != 0){
    			str +="						<span class='form-control sd' >提现锁定</span></div>";
    		}else{
    			str +="						<span class='form-control tx' >正常</span></div>";
    		}
			str +="				</div>";
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>团队人数/下级人数：</span>";
			str +="						<span class='form-control' >"+(userMsg.teamMemberCount == null?"无":userMsg.teamMemberCount)+"/"+(userMsg.downMemberCount == null?"无":userMsg.downMemberCount)+"</span></div>";
			str +="				</div>";
			
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>时时彩返点：</span>";
			str +="						<span class='form-control' >"+userMsg.sscRebate+"</span></div>";
			str +="				</div>";
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>分分彩返点：</span>";
			str +="						<span class='form-control' >"+userMsg.ffcRebate+"</span></div>";
			str +="				</div>";
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>11选5返点：</span>";
			str +="						<span class='form-control' >"+userMsg.syxwRebate+"</span></div>";
			str +="				</div>";
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>快3返点：</span>";
			str +="						<span class='form-control' >"+userMsg.ksRebate+"</span></div>";
			str +="				</div>";
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>北京PK10返点：</span>";
			str +="						<span class='form-control' >"+userMsg.pk10Rebate+"</span></div>";
			str +="				</div>";
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>低频彩：</span>";
			str +="						<span class='form-control' >"+userMsg.dpcRebate+"</span></div>";
			str +="				</div>";
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>注册时间：</span>";
			str +="						<span class='form-control' >"+userMsg.addTimeStr+"</span></div>";
			str +="				</div>";
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>最后登陆时间：</span>";
			str +="						<span class='form-control' >"+userMsg.logTimeStr+"</span></div>";
			str +="				</div>";
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>最后登陆IP：</span>";
			var logip=(userMsg.logip == null?"":userMsg.logip);
			var bizSystem = userMsg.bizSystem;
			if(logip==''){
				str +="	<span class='form-control' >"+(userMsg.logip == null?"":userMsg.logip)+"</span></div>";	
			}else{
				str +="	<a  href='javascript:void(0)' style='color:blue;text-decoration:underline;'  onclick='commonPage.openTabLink(\"menuItemUtil\",\""+contextPath+"/managerzaizst/loginlog/loginlog.html?logip="+logip+"&bizSystem="+bizSystem+"&userName="+userMsg.userName+"\",\"登陆日志查询\")' ><span class='form-control' >"+logip+"</span></a></div>";	
			}
			str +="				</div>";
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>登陆次数：</span>";
			str +="						<span class='form-control' >"+userMsg.loginTimes+"</span></div>";
			str +="				</div>";
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>联系方式：</span>";
		
			//判断管理员是否能查看数据权限，若不能则显示为***
		if(currentUser.dataViewSwitch == 1) {
			str +="						<span class='form-control' >"+(userMsg.phone == null?"无":userMsg.phone)+"</span></div>";
		} else {
			str +="						<span class='form-control' >*******</span></div>";								
		}
			
			str +="				</div>";
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>QQ号码：</span>";
			
		//判断管理员是否能查看数据权限，若不能则显示为***
		if(currentUser.dataViewSwitch == 1) {
			str +="						<span class='form-control' >"+(userMsg.qq == null?"无":userMsg.qq)+"</span></div>";
		} else {
			str +="						<span class='form-control' >*******</span></div>";								
		}
			
			str +="				</div>";
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>地址：</span>";
				var addressStr = (userMsg.province == null?"":userMsg.province) + (userMsg.city == null?"":userMsg.city) + (userMsg.address == null?"":userMsg.address)
			str += "					<span class='form-control' >"+(addressStr == ""?'无':addressStr)+"</span></div>"
//			str +="						<span class='form-control' >"+(userMsg.province == null?"":userMsg.province) + (userMsg.city == null?"":userMsg.city) + (userMsg.address == null?"":userMsg.address) +"</span></div>";
			str +="				</div>";
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>电子邮箱：</span>";
			str +="						<span class='form-control' >"+(userMsg.email == null?"无":userMsg.email)+"</span></div>";
			str +="				</div>";
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>是否绑定安全码：</span>";
			str +="						<span class='form-control' >"+(userMsg.haveSafePassword == 0?"否":"是")+"</span></div>";
			str +="				</div>";
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>总存款值：</span>";
			str +="						<span class='form-control' >"+(userMoneyTotal.totalRecharge == null?"无":userMoneyTotal.totalRecharge.toFixed(3))+"</span></div>";
			str +="				</div>";
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>总取款值：</span>";
			str +="						<span class='form-control' >"+(userMoneyTotal.totalWithdraw == null?"无":userMoneyTotal.totalWithdraw.toFixed(3))+"</span></div>";
			str +="				</div>";
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>总返点：</span>";
			str +="						<span class='form-control' >"+(userMoneyTotal.totalRebate+userMoneyTotal.totalPercentage).toFixed(3)+"</span></div>";
			str +="				</div>";
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>总投注额：</span>";
			str +="						<span class='form-control' >"+(userMoneyTotal.totalLottery == null?"无":(userMoneyTotal.totalLottery-userMoneyTotal.totalLotteryRegression-userMoneyTotal.totalLotteryStopAfterNumber).toFixed(3))+"</span></div>";
			str +="				</div>";
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>总中奖额：</span>";
			str +="						<span class='form-control' >"+(userMoneyTotal.totalWin == null?"无":userMoneyTotal.totalWin.toFixed(3))+"</span></div>";
			str +="				</div>";
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>总盈利额：</span>";
			str +="						<span class='form-control' >"+ totalGain +"</span></div>";
			str +="				</div>";
			//增加一行
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>总系统续费：</span>";
			str +="						<span class='form-control' >"+(userMoneyTotal.totalSystemAddMoney == null?"无":userMoneyTotal.totalSystemAddMoney.toFixed(3))+"</span></div>";
			str +="				</div>";
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>总系统扣费：</span>";
			str +="						<span class='form-control' >"+(userMoneyTotal.totalSystemReduceMoney == null?"无":userMoneyTotal.totalSystemReduceMoney.toFixed(3))+"</span></div>";
			str +="				</div>";
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>其他：</span>";
			str +="						<span class='form-control' >"+ otherMoney +"</span></div>";
			str +="				</div>";
			
			
			
/*			
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>不可提现金额：</span>";
			str +="						<span class='form-control' >"+ (userMsg.money - userMsg.canWithdrawMoney).toFixed(commonPage.param.fixVaue) +"</span></div>";
			str +="				</div>";
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>可用积分：</span>";
			str +="						<span class='form-control' >"+userMsg.point+"</span></div>";
			str +="				</div>";
	
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>分红比例(直属代理由总代决定)：</span>";
			str +="						<span class='form-control' >"+(userMsg.bonusScale == null?"无":userMsg.bonusScale)+"</span></div>";
			str +="				</div>";
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>总提成额：</span>";
			str +="						<span class='form-control' >"+(userMoneyTotal.totalPercentage == null?"无":userMoneyTotal.totalPercentage)+"</span></div>";
			str +="				</div>";
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>在线状态：</span>";
			str +="						<span class='form-control' >"+(userMsg.isOnline == 1?"在线":"离线")+"</span></div>";
			str +="				</div>";
			
			str +="				<div class='col-sm-4'>";
			str +="					<div class='input-group m-b'>";
			str +="						<span class='input-group-addon'>下级人数：</span>";
			str +="						<span class='form-control' >"+(userMsg.downMemberCount == null?"无":userMsg.downMemberCount)+"</span></div>";
			str +="				</div>";*/
			str +="			</div>";
			str +="		</div>";
			str +="	</div>";
			str +="</div>";
			
			$(document.body).append(str);
			/*$('#modal-form').find('input,textarea,select').attr('disabled','disabled');*/
			$('#modal-form').modal('show');
			if(userMsg.loginLock == 1){
				$(".sd").css("color","red");
			}else{
				$('.sd').removeAttr("style")
			}
			if(userMsg.withdrawLock == 1){
				$(".tx").css("color","red")
			}else{
				$(".tx").removeAttr("style")
			}
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询用户数据请求失败.");
		}
    });
};

/**
 * 打开另外选项卡，
 * aid = index.jsp 上a标签的id,就是对应打开选项卡的链接
 * link  a标签的链接,
 * tabName 选项卡名称
 */
CommonPage.prototype.openTabLink =function (aid,link,tabName){
	 var el=parent.document.getElementById(aid); 
	 el.href=link;
	 el.text=tabName;
	 el.click();
}


/**
 * 获取查询起始时间，如果小于三点，置为前一天三点
 */
function getClsStartTime() {
	var nowDate = new Date();
	var curHour = nowDate.getHours(); 
	if(curHour<3){
		nowDate = nowDate.AddDays(-1);
	}
	nowDate.setHours(3,0,0,0);
	return nowDate;
}

/**
 * 获取查询截止时间，如果大于三点，置为后一天三点
 */
function getClsEndTime() {
	var nowDate = new Date();
	var curHour = nowDate.getHours(); 
	if(curHour>3){
		nowDate = nowDate.AddDays(1);
	}
	nowDate.setHours(2,59,59,999);
	return nowDate;
}

/**
 * 根据要查询日期天数设定开始结束时间
 * @param dateRange  天数类型(今天，昨天，最近7天，本月，上月)
 * @param startTimeId  开始时间元素ID
 * @param endTimeId   结束时间ID
 * @param dateFormatType  1-年月日   2-年月日时分秒
 */
CommonPage.prototype.setDateRangeTime = function(dateRange, startTimeId, endTimeId, dateFormatType){
	
	var formateStr = "yyyy-MM-dd";
	
	var todate = new Date();
	
	var compareDate = new Date();
	compareDate.setHours(3, 0, 0, 0);
	var start;
	var end;
	//点击之后这个时间改下，主要是要判断03:00的逻辑 
	switch(dateRange){
		//今天
	    case 1:
	    	if(todate > compareDate){
	    		//大于三点到现在 属于今天
	    		 end = new Date();
	    		 start = todate;
	    	}else{
	    		//昨天早上3点到现在 属于今天
	    		 start = new Date().AddDays(-1);
	    		 end = todate.AddDays(-1);
	    	}

	        break;
	    //昨天    
	    case -1:
	    	if(todate > compareDate){
	    		//昨天三点到今天3点 属于昨天
	    		start = new Date().AddDays(-1);
	    		end = todate.AddDays(-1);
	    	}else{
	    		//小于3点属于今天，就前2天早上3点到前1天3点 属于昨天
	    		start = new Date().AddDays(-2);
	    		end = todate.AddDays(-2);
	    	}
	        break;
	    //最近7天
	    case -7:
	    	if(todate > compareDate){
	    		 end = new Date();
	    		 start = todate.AddDays(-6);
	    		 
	    	}else{
	    		 end = new Date();
	    		 start = todate.AddDays(-7);
	    		 
	    	}
            break;
        //本月
	    case 30:
    		end = new Date();
    		todate.setDate(1);
    		start=todate;
	        break;
	    //上月
	    case -30:
	    	var enddate = new Date();
	        enddate.setDate(1);
	        enddate.AddDays(-1);
	        end = enddate;
	        todate.setDate(1);
    		todate.AddDays(-1);
    		todate.setDate(1);
    		start=todate;
	        break;
	     //默认今天
	    default:
	    	if(todate > compareDate){
	    		//大于三点到现在 属于今天
	    		 end = new Date();
	    		 start = todate;
	    	}else{
	    		//昨天早上3点到现在 属于今天
	    		 start = new Date().AddDays(-1);
	    		 end = todate.AddDays(-1);
	    	}
	        break;
	}
	
	if(dateFormatType == 2) {
		formateStr = "yyyy-MM-dd hh:mm:ss";
		start.setHours(3, 0, 0, 0);
		end.AddDays(1);
		end.setHours(2, 59, 59, 999);
	}
	$("#" + startTimeId).val(new Date(start).format(formateStr));
	$("#" + endTimeId).val(new Date(end).format(formateStr));
	
}

/**
 * 获取查询起始时间，如果小于零点，置为前一天零点
 */
function getClsStartTimeZero() {
	var nowDate = new Date();
	var curHour = nowDate.getHours(); 
	if(curHour<3){
		nowDate = nowDate.AddDays(-1);
	}
	nowDate.setHours(0,0,0,0);
	return nowDate;
}

/**
 * 获取查询截止时间，如果大于零点，置为后一天零点
 */
function getClsEndTimeZero() {
	var nowDate = new Date();
	var curHour = nowDate.getHours(); 
	if(curHour>3){
		nowDate = nowDate.AddDays(1);
	}
	nowDate.setHours(0,0,0,0);
	return nowDate;
}

/**
 * 根据要查询日期天数设定开始结束时间
 * @param dateRange  天数类型(今天，昨天，最近7天，本月，上月)
 * @param startTimeId  开始时间元素ID
 * @param endTimeId   结束时间ID
 * @param dateFormatType  1-年月日   2-年月日时分秒
 */
CommonPage.prototype.setDateRangeTimeZero = function(dateRange, startTimeId, endTimeId, dateFormatType){
	
	var formateStr = "yyyy-MM-dd";
	
	var todate = new Date();
	
	var compareDate = new Date();
	compareDate.setHours(0, 0, 0, 0);
	var start;
	var end;
	//点击之后这个时间改下，主要是要判断03:00的逻辑 
	switch(dateRange){
		//今天
	    case 1:
	    	if(todate > compareDate){
	    		//大于三点到现在 属于今天
	    		 end = new Date();
	    		 start = todate;
	    	}else{
	    		//昨天早上3点到现在 属于今天
	    		 start = new Date().AddDays(-1);
	    		 end = todate.AddDays(-1);
	    	}

	        break;
	    //昨天    
	    case -1:
	    	if(todate > compareDate){
	    		//昨天三点到今天3点 属于昨天
	    		start = new Date().AddDays(-1);
	    		end = todate.AddDays(-1);
	    	}else{
	    		//小于3点属于今天，就前2天早上3点到前1天3点 属于昨天
	    		start = new Date().AddDays(-2);
	    		end = todate.AddDays(-2);
	    	}
	        break;
	    //最近7天
	    case -7:
	    	if(todate > compareDate){
	    		 end = new Date();
	    		 start = todate.AddDays(-6);
	    		 
	    	}else{
	    		 end = new Date();
	    		 start = todate.AddDays(-7);
	    		 
	    	}
            break;
        //本月
	    case 30:
    		end = new Date();
    		todate.setDate(1);
    		start=todate;
	        break;
	    //上月
	    case -30:
	    	var enddate = new Date();
	        enddate.setDate(1);
	        enddate.AddDays(-1);
	        end = enddate;
	        todate.setDate(1);
    		todate.AddDays(-1);
    		todate.setDate(1);
    		start=todate;
	        break;
	     //默认今天
	    default:
	    	if(todate > compareDate){
	    		//大于三点到现在 属于今天
	    		 end = new Date();
	    		 start = todate;
	    	}else{
	    		//昨天早上3点到现在 属于今天
	    		 start = new Date().AddDays(-1);
	    		 end = todate.AddDays(-1);
	    	}
	        break;
	}
	
	if(dateFormatType == 2) {
		formateStr = "yyyy-MM-dd hh:mm:ss";
		start.setHours(0, 0, 0, 0);
		end.AddDays(1);
		end.setHours(0,0,0,0);
	}
	$("#" + startTimeId).val(new Date(start).format(formateStr));
	$("#" + endTimeId).val(new Date(end).format(formateStr));
	
}