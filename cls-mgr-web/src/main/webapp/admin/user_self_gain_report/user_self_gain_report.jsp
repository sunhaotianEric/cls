<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
    
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>用户盈利报表</title>
<style type="text/css">
.wrapper {padding:0 10px}
.wrapper-content {padding:10px}
</style>
<jsp:include page="/admin/include/include.jsp"></jsp:include>

</head>
    <body>
        <div class="animated fadeIn" style="padding: 0 5px;">
            <div class="row">
                <div class="col-sm-12">
                    <!-- <div class="ibox "> -->
                        <div class="ibox-content">
                            <div class="row m-b-sm m-t-sm">
                                <form class="form-horizontal" id="userSelfGainReportQuery">
                                <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                                    <div class="row">
                                      <div class="col-sm-3">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">商户</span>
                                                <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
                                                </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">用户名</span>
                                                <input name="userName" id="userName" type="text" value="" class="form-control"></div>
                                        </div>
                                        <div class="col-sm-4">
						                     <div class="input-group m-b">
						                        <span class="input-group-addon">开始时间</span>
						                        <div class="input-daterange input-group">
						                        <input class="form-control layer-date" placeholder="开始日期" onclick="laydate({istime: false, format: 'YYYY-MM-DD',max: laydate.now(-1)})" name="belongDateStart" id="belongDateStart">
						                      	<span class="input-group-addon">到</span>
						                      	<input class="form-control layer-date" placeholder="结束日期" onclick="laydate({istime: false, format: 'YYYY-MM-DD',max: laydate.now(-1)})" name="belongDateEnd" id="belongDateEnd" ></div>
						                    </div>
						                </div>
						                <div class="col-sm-2">
				                        	<div class="form-group nomargin">
									            <button type="button" class="btn btn-xs btn-info" onclick="userSelfGainReportPage.getTotalUsersConsumeReportsForQuery(1)">今 天</button>
									            <button type="button" class="btn btn-xs btn-danger" onclick="userSelfGainReportPage.getTotalUsersConsumeReportsForQuery(-1)">昨 天</button>
									        	<button type="button" class="btn btn-xs btn-warning" onclick="userSelfGainReportPage.getTotalUsersConsumeReportsForQuery(-7)">最近7天</button><br/>
									        	<button type="button" class="btn btn-xs btn-success" onclick="userSelfGainReportPage.getTotalUsersConsumeReportsForQuery(30)">本 月</button>
									        	<button type="button" class="btn btn-xs btn-primary" onclick="userSelfGainReportPage.getTotalUsersConsumeReportsForQuery(-30)">上 月</button>
				                            </div>
				                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group nomargin text-right">
                                                <button type="button" class="btn btn-outline btn-default btn-kj" onclick="userSelfGainReportPage.getTotalUsersConsumeReportsForQuery()"><i class="fa fa-search"></i>&nbsp;查 询</button></div>
                                        </div>
                                    </div>
                                    </c:if>
                                    <c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">用户名</span>
                                                <input  name="userName" id="userName" type="text" value="" class="form-control" width="40%"></div>
                                        </div>
                                         <div class="col-sm-4">
						                     <div class="input-group m-b">
						                        <span class="input-group-addon">开始时间</span>
						                        <div class="input-daterange input-group">
						                        <input class="form-control layer-date" placeholder="开始日期" onclick="laydate({istime: false, format: 'YYYY-MM-DD'})" name="belongDateStart" id="belongDateStart">
						                      	<span class="input-group-addon">到</span>
						                      	<input class="form-control layer-date" placeholder="结束日期" onclick="laydate({istime: false, format: 'YYYY-MM-DD'})" name="belongDateEnd" id="belongDateEnd" ></div>
						                    </div>
						                </div>
						                <div class="col-sm-3">
				                        	<div class="form-group nomargin">
									            <button type="button" class="btn btn-xs btn-info" onclick="userSelfGainReportPage.getTotalUsersConsumeReportsForQuery(1)">今 天</button>
									            <button type="button" class="btn btn-xs btn-danger" onclick="userSelfGainReportPage.getTotalUsersConsumeReportsForQuery(-1)">昨 天</button>
									        	<button type="button" class="btn btn-xs btn-warning" onclick="userSelfGainReportPage.getTotalUsersConsumeReportsForQuery(-7)">最近7天</button><br/>
									        	<button type="button" class="btn btn-xs btn-success" onclick="userSelfGainReportPage.getTotalUsersConsumeReportsForQuery(30)">本 月</button>
									        	<button type="button" class="btn btn-xs btn-primary" onclick="userSelfGainReportPage.getTotalUsersConsumeReportsForQuery(-30)">上 月</button>
				                            </div>
				                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group nomargin text-right">
                                                <button type="button" class="btn btn-outline btn-default btn-kj" onclick="userSelfGainReportPage.getTotalUsersConsumeReportsForQuery()"><i class="fa fa-search"></i>&nbsp;查 询</button></div>
                                        </div>
                                    </div>
                                    </c:if>
                                </form>
                            </div>
                            <div class="jqGrid_wrapper">
                                <div class="ui-jqgrid ">
                                    <div class="ui-jqgrid-view">
                                        <div class="ui-jqgrid-hdiv">
                                            <div class="ui-jqgrid-hbox" style="padding-right:0">
                                                <table class="ui-jqgrid-htable ui-common-table table table-bordered" id="userMoneyTotalTable">
                                                    <thead>
                                                        <tr class="ui-jqgrid-labels">
												          <th class="ui-th-column ui-th-ltr">商户</th>
                                                          <th class="ui-th-column ui-th-ltr">用户名</th>
												          <th class="ui-th-column ui-th-ltr">余额</th>
												          <th class="ui-th-column ui-th-ltr">充值</th>
												          <th class="ui-th-column ui-th-ltr">充值赠送总额</th>
												          <th class="ui-th-column ui-th-ltr">活动彩金总额</th>
												          <th class="ui-th-column ui-th-ltr">商户续费</th>
												          <th class="ui-th-column ui-th-ltr">商户扣费</th>
												          <th class="ui-th-column ui-th-ltr">取款</th>
												          <th class="ui-th-column ui-th-ltr">取款手续费</th>
												          <th class="ui-th-column ui-th-ltr">支出转账</th> 
												          <th class="ui-th-column ui-th-ltr">收入转账</th>
												          <th class="ui-th-column ui-th-ltr">总共投注额(-撤单)</th>
												          <th class="ui-th-column ui-th-ltr">中奖</th>
												          <th class="ui-th-column ui-th-ltr">投注返点</th>
												          <!-- <th class="ui-th-column ui-th-ltr">提成返点</th> -->
												          <th class="ui-th-column ui-th-ltr">提成金额</th> 
												     	  <th class="ui-th-column ui-th-ltr">月分红</th>
												     	  <th class="ui-th-column ui-th-ltr">日分红</th>
												          <th class="ui-th-column ui-th-ltr">月工资</th>
												          <th class="ui-th-column ui-th-ltr">日工资</th>
												          <th class="ui-th-column ui-th-ltr">日佣金</th>
												          <th class="ui-th-column ui-th-ltr">盈利</th>
												         <!--  <th class="ui-th-column ui-th-ltr">其他</th> -->
<!-- 												          <th class="ui-th-column ui-th-ltr">中奖金额</th> -->
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                         
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>          
                                </div>
                            </div>
                        </div>
                   <!--  </div> -->
                </div>
            </div>
        </div>
<!-- js -->
<script type="text/javascript" src="<%=path%>/admin/user_self_gain_report/js/user_self_gain_report.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/mangerUserSelfDayConsumeAction.js'></script>
<script type='text/javascript' src='<%=path%>/dwr/interface/managerUserAction.js'></script>
</body>
</html>

