<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
	String id=request.getParameter("id");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>玩法规则管理菜单编辑</title>
        <link rel="shortcut icon" href="favicon.ico">
        <jsp:include page="/admin/include/include.jsp"></jsp:include>
        <!-- dwr -->
		<script type='text/javascript' src='<%=path%>/dwr/interface/manageGameRuleAction.js'></script>
		<script type="text/javascript" src="<%=path%>/admin/game_rule/js/game_rule_edit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
        <script type="text/javascript">
        gameRulePage.param.id=<%=id%>;
        </script>
        <link rel="stylesheet" href="<%=path%>/resources/kindeditor-4.1.10/themes/default/default.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>"/>
		<script charset="utf-8" src="<%=path%>/resources/kindeditor-4.1.10/kindeditor-min.js"></script>
		<script charset="utf-8" src="<%=path%>/resources/kindeditor-4.1.10/lang/zh_CN.js"></script>
		<script>
		var staticImageUrl = 'http://www.zzh.com:8080/cls-mgr-web';
		var url = staticImageUrl.split('//');
		var imgurl =  "http://42.51.191.174";
		KindEditor.ready(function(K) {
			editor = K.create('textarea[name="ruleDesc"]', {
				uploadJson : location.protocol+'//'+url[1]+'/UploadFileForJsonServlet',
				fileManagerJson :imgurl+'/file_manager_json.jsp',
				allowFileManager : true
			});
		});
	</script>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeIn">
		<form action="javascript:void(0)">
			<div class="row">
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">彩种类别：</span>
						<cls:enmuSel name="lotteryType" className="com.team.lottery.enums.ELotteryKind" options="class:ipt form-control" id="lotteryType" />
					</div>
				</div>
				<div class="col-sm-6">
				    <div class="input-group m-b">
					    <span class="input-group-addon">规则说明：</span>
					    <textarea name="ruleDesc" id="ruleDesc" style="width:800px;height:400px;visibility:hidden;"></textarea>
				    </div>
			    </div>
				<div class="col-sm-12">
						<div class="col-sm-6 text-center">
							<button type="button" class="btn btn-w-m btn-white"
								onclick="gameRulePage.selectByPrimary()">提 交</button>
							<input type="reset" class="btn btn-w-m btn-white" value="重 置" />
						</div>
				</div>
			</div>
		</form>
	</div>
	<script type="text/javascript" src="<%=path%>/admin/common/common.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>

</body>
</html>