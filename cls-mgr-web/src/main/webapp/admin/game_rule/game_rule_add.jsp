<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<html>
<head>
    <meta charset="utf-8" />
    <title>玩法规则管理菜单添加</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    <link rel="stylesheet" href="<%=path%>/resources/kindeditor-4.1.10/themes/default/default.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>"/>
		<script charset="utf-8" src="<%=path%>/resources/kindeditor-4.1.10/kindeditor-min.js"></script>
		<script charset="utf-8" src="<%=path%>/resources/kindeditor-4.1.10/lang/zh_CN.js"></script>
		<script>
		var staticImageUrl = 'http://www.zzh.com:8080/cls-mgr-web';
		var url = staticImageUrl.split('//');
		var imgurl =  "http://42.51.191.174";
		KindEditor.ready(function(K) {
			editor = K.create('textarea[name="ruleDesc"]', {
				uploadJson : location.protocol+'//'+url[1]+'/UploadFileForJsonServlet',
				fileManagerJson :imgurl+'/file_manager_json.jsp',
				allowFileManager : true
			});
		});
	</script>
</head>
<body>
    <div class="wrapper wrapper-content animated fadeIn">
		<form action="javascript:void(0)">
		<%-- <input type="hidden" name="id" id="id" value="<%=id%>"> --%>
			<div class="row">
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">彩种类别：</span>
						<cls:enmuSel name="lotteryType" className="com.team.lottery.enums.ELotteryKind" options="class:ipt form-control" id="lotteryType" />
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">规则说明：</span>
						<textarea name="ruleDesc" id="ruleDesc" style="width:800px;height:400px;visibility:hidden;"></textarea>
					</div>
				</div>
				<div class="col-sm-12">
						<div class="col-sm-6 text-center">
							<button type="button" class="btn btn-w-m btn-white"
								onclick="gameRulePage.saveData()">提 交</button>
							<input type="reset" class="btn btn-w-m btn-white" value="重 置" />
						</div>
				</div>
			</div>
		</form>
	</div>
	 <script type="text/javascript" src="<%=path%>/admin/common/common.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/game_rule/js/game_rule_edit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/manageGameRuleAction.js'></script>
</body>
</html>

