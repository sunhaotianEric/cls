<%@ page language="java" contentType="text/html; charset=UTF-8" 
	pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>  
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>玩法规则管理</title>
        <link rel="shortcut icon" href="favicon.ico">
        <jsp:include page="/admin/include/include.jsp"></jsp:include>
    </head>
    <body>
        <div class="animated fadeIn">
            <div class="ibox-content">
                 <div class="row m-b-sm m-t-sm">
                    <form class="form-horizontal" id="gameRuleForm">
                        <div class="row">
		                        <div class="col-sm-3">
									<div class="input-group m-b">
										<span class="input-group-addon">彩种类别</span>
										<cls:enmuSel name="lotteryType"
											className="com.team.lottery.enums.ELotteryKind"
											options="class:ipt form-control" id="lotteryType" />
									</div>
								</div>
	                            <div class="col-sm-4">
	                                <div class="form-group nomargin text-right">
	                                    <button type="button" class="btn btn-outline btn-default btn-js" onclick="gameRulePage.findGameRule()"><i class="fa fa-search"></i>&nbsp;查 询</button>
	                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	                                    <button type="button" class="btn btn-outline btn-default btn-add" onclick="gameRulePage.addGameRule()"><i class="fa fa-plus"></i>&nbsp;新 增</button>
	                                 </div>
	                            </div>
                         </div>
                    </form>
                    </div>
                     <div class="jqGrid_wrapper">
                        <div class="ui-jqgrid ">
                            <div class="ui-jqgrid-view">
                                <div class="ui-jqgrid-hdiv">
                                    <div class="ui-jqgrid-hbox" style="padding-right:0">
                                        <table id="gameRulePageTable" class="ui-jqgrid-htable ui-common-table table table-bordered">
                                            <thead>
                                                <tr class="ui-jqgrid-labels">
                                                	<th class="ui-th-column ui-th-ltr">编号</th>
                                                	<th class="ui-th-column ui-th-ltr">彩种类别</th>
                                                	<th class="ui-th-column ui-th-ltr">规则说明</th>
													<th class="ui-th-column ui-th-ltr">创建时间</th>
											<!-- 		<th class="ui-th-column ui-th-ltr">修改时间</th> -->
													<th class="ui-th-column ui-th-ltr">操 作</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>     
		         </div>
		      </div>
     <%--        <script type="text/javascript" src="<%=path%>/admin/common/common.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script> --%>
		    <!-- dwr -->
		    <script type='text/javascript' src='<%=path%>/dwr/interface/manageGameRuleAction.js'></script>
		    <script type="text/javascript" src="<%=path%>/admin/game_rule/js/game_rule.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    </body>

</html>
