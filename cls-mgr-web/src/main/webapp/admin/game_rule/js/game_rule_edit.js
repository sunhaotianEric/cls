function GameRulePage(){}
var gameRulePage = new GameRulePage();

/**
 * 查询参数
 */
gameRulePage.queryParam = {
		id:null,
		lotteryType : null,
		createDate:null,
		createAdmin:null,
		updateAdmin:null,
		updateDate:null,
		ruleDesc:null
};
gameRulePage.param = {
//	skipAwardZh:0  
};

$(document).ready(function() {
	/*gameRulePage.queryParam.id=id;
	if(gameRulePage.queryParam.id!=null&&gameRulePage.queryParam.id!=""){
		gameRulePage.selectByPrimaryKey();
	}*/
	var id = gameRulePage.param.id;  //获取查询的ID
	if(id != null){
		gameRulePage.editGameRule(id);
	}
	
});

GameRulePage.prototype.selectGameRule=function(){
	manageGameRuleAction.selectGameRule(gameRulePage.queryParam.id,function(r){
		if(r[0]=="ok"){
			$("#lotteryType").val(r[1].lotteryType);
			$("#createDate").val(r[1].createDate);
			$("#createAdmin").val(r[1].createAdmin);
			$("#updateAdmin").val(r[1].updateAdmin);
			$("#updateDate").val(r[1].updateDate);
			$("#ruleDesc").val(r[1].ruleDesc);
		}
	});
}

/**
 * 编辑
 */
GameRulePage.prototype.selectByPrimary=function(){
	var id = gameRulePage.param.id;  //获取查询的ID
	var lotteryType=$("#lotteryType").val();
	var createDate=$("#createDate").val();
	var createAdmin=$("#createAdmin").val();
	var updateAdmin=$("#updateAdmin").val();
	var updateDate=$("#updateDate").val();
	
	gameRulePage.queryParam.id=id;
	gameRulePage.queryParam.lotteryType=lotteryType;
	gameRulePage.queryParam.createDate=createDate;
	gameRulePage.queryParam.createAdmin=createAdmin;
	gameRulePage.queryParam.updateAdmin=updateAdmin;
	gameRulePage.queryParam.updateDate=updateDate;
	gameRulePage.queryParam.ruleDesc=editor.html();;
		if(gameRulePage.queryParam.id!=null || gameRulePage.queryParam.id!=""){
			manageGameRuleAction.updateGameRule(gameRulePage.queryParam,function(r){
				if(r[0]=="ok"){
					swal({title:"修改成功！",
				        text:"已成功修改数据",
				        type:"success"},
				      function(){
				        	var index = parent.layer.getFrameIndex(window.name); //获取窗口索引    
				        	parent.gameRulePage.layerClose(index);
					  }
				     )				
				}else{
					swal(r[1]);
				}
			});
		}else{
			manageGameRuleAction.saveGameRule(gameRulePage.queryParam,function(r){
				if(r[0]=="ok"){
					swal({title:"保存成功！",
				        text:"已成功保存数据",
				        type:"success"},
				      function(){
				        	var index = parent.layer.getFrameIndex(window.name); //获取窗口索引    
				        	parent.gameRulePage.layerClose(index);
					  }
				     )	
				}else{
					swal(r[1]);
				}
			});
		}
		gameRulePage.queryParam={};
}

/**
 * 保存信息
 */
GameRulePage.prototype.saveData = function(){
	var id = $("#id").val();
	var lotteryType = $("#lotteryType").val();
	var ruleDesc = editor.html();
	
	if(lotteryType==null || lotteryType.trim()==""){
		swal("请选择彩种类别!");
		$("#lotteryType").focus();
		return;
	}
	if(ruleDesc==null || ruleDesc.trim()==""){
		swal("请输入规则说明!");
		editor.focus();
		return;
	}
	
	var gameRule = {};
	gameRule.id = gameRulePage.param.id;
	gameRule.lotteryType = lotteryType;
	gameRule.ruleDesc = ruleDesc;
	manageGameRuleAction.saveGameRule(gameRule,function(r){
		if(r[0]=="ok"){
			swal({title:"保存成功！",
		        text:"已成功新增数据",
		        type:"success"},
		      function(){
		        	var index = parent.layer.getFrameIndex(window.name); //获取窗口索引    
		        	parent.gameRulePage.layerClose(index);
			  }
		     )				
		}else{
			swal(r[1]);
		}
    });
};

/**
 * 编辑时赋值
 */
GameRulePage.prototype.editGameRule = function(id){
	manageGameRuleAction.selectGameRule(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			$("#id").val(r[1].id);
			$("#lotteryType").val(r[1].lotteryType);
			editor.html(r[1].ruleDesc);
		}else if(r[0] != null && r[0] == "error"){
//			showErrorDlg(jQuery(document.body), r[1]);
			swal(r[1]);
		}else{
//			showErrorDlg(jQuery(document.body), "获取数据失败.");
			swal("获取信息失败.");
		}
    });
};
