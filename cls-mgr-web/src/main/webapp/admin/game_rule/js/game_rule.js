function GameRulePage(){}
var gameRulePage = new GameRulePage();

/**
 * 查询参数
 */
gameRulePage.queryParam = {
//		id:null,
//		lotteryType : null,
//		createDate:null,
//		createAdmin:null,
//		updateAdmin:null,
//		updateDate:null,
//		ruleDesc:null
		lottery_type : null,
};

/**
 * 分页参数
 */
gameRulePage.pageParam = {
    pageNo : 1,
    pageSize : defaultPageSize
};

$(document).ready(function() {
	gameRulePage.initTableData();//初始化查询数据
});

GameRulePage.prototype.initTableData = function() {
	gameRulePage.queryParam = getFormObj($("#gameRuleForm"));
	gameRulePage.table = $('#gameRulePageTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"bPaginate" : false, 
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			gameRulePage.pageParam.pageSize = data.length;//页面显示记录条数
			gameRulePage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	manageGameRuleAction.getAllGameRulePage(gameRulePage.queryParam,gameRulePage.pageParam,function(r){
		//封装返回数据
		var returnData = {
			recordsTotal : 0,
			recordsFiltered : 0,
			data : null
		};
		if (r[0] != null && r[0] == "ok") {
			returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
			returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
			returnData.data = r[1].pageContent;//返回的数据列表
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("查询信息失败.");
		}
		callback(returnData);
			});
		},
		"columns": [
            {"data": "id"},
            {"data": "lotteryTypeName"},
            {"data": "ruleDesc",
            	render: function(data, type, row, meta) {
            		if (data.length > 20) {
            			var title = data.substring(0,20) + "...";
            			return "<span title = '"+data+"'>"+title+"</span>";
            		}
            		return data;
        		}
            },
            {   
            	"data": "createDate",
            	render: function(data, type, row, meta) {
            		return dateFormat(data,'yyyy-MM-dd hh:mm:ss');
        		}
            },
          /*  {   
            	"data": "updateDate",
            	render: function(data, type, row, meta) {
            		return dateFormat(data,'yyyy-MM-dd hh:mm:ss');
        		}
            },*/
            {
            	 "data": "id",
            	 render: function(data, type, row, meta) {
            		 return "<a class='btn btn-info btn-sm btn-bj' onclick='gameRulePage.editGameRule("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;" +
     		 				"<a class='btn btn-danger btn-sm btn-del' onclick='gameRulePage.delGameRule("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";
            	 }
            }
        ]
}).api(); 
}

/**
 * 查询数据
 */
GameRulePage.prototype.findGameRule=function(){
	/*var type=$("#lotteryKind").val();
	if(lotteryKind==null||lotteryKind==""){
		gameRulePage.queryParam.type=null;
	}else{
		gameRulePage.queryParam.type=lotteryKind;
	}
	gameRulePage.table.ajax.reload();*/
	
	gameRulePage.queryParam.lotteryType = $("#lotteryType").val();
	gameRulePage.table.ajax.reload();
}

/**
 * 删除
 */
GameRulePage.prototype.delGameRule =function(id){
	  swal({
          title: "您确定要删除这条信息吗",
          text: "请谨慎操作！",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          cancelButtonText: "取消",
          confirmButtonText: "删除",
          closeOnConfirm: false
      },
      function() {
    	  manageGameRuleAction.delGameRule(id,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "删除成功",type: "success"});
				gameRulePage.findGameRule();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("删除失败.");
			}
	    });
	});
};

/**
 * 编辑
 */
GameRulePage.prototype.editGameRule=function(id){
	layer.open({
        type: 2,
        title: '玩法规则管理菜单编辑',
        maxmin: false,
        shadeClose: true,
        //点击遮罩关闭层
       /* area: ['600px', '450px'],*/
        area: ['70%', '80%'],
        content: contextPath + "/managerzaizst/game_rule/game_rule_edit.html?id="+id,
        cancel: function(index){ 
        	gameRulePage.table.ajax.reload();
     	}
    });
}

/**
 * 新增
 */
GameRulePage.prototype.addGameRule=function(id){
    layer.open({
           type: 2,
           title: '邮件发送配置新增',
           maxmin: false,
           shadeClose: true,
           //点击遮罩关闭层
//           area: ['600px', '420px'],
           area: ['70%', '80%'],
           content: contextPath + "/managerzaizst/game_rule/game_rule_add.html?id="+id,   
           cancel: function(index){
        	   gameRulePage.findGameRule();
         }
    });
}

GameRulePage.prototype.layerClose=function(index){
	layer.close(index);
	gameRulePage.findGameRule();
}