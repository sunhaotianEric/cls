<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>  
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>用户打码管理</title>
        <link rel="shortcut icon" href="favicon.ico">
        <jsp:include page="/admin/include/include.jsp"></jsp:include>
        <script  type="text/javascript" src="<%=path%>/resources/My97DatePicker/WdatePicker.js"></script>
        <style type="text/css">
	      .cp-yellow{color:#f8a525}
	      .cp-red{color:red}
	      .cp-green{color:green}
	      </style>
</head>

   <body>
        <div class="animated fadeIn">
            <div class="ibox-content">
                 <div class="row m-b-sm m-t-sm">
                    <form class="form-horizontal" id="queryForm">
                    <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                        <div class="row">
                             <div class="col-sm-3">
                                  <div class="input-group m-b">
                                      <span class="input-group-addon">商户</span>
                                       <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
                                  </div>
                             </div>
                            <div class="col-sm-3">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">用户名</span>
                                    <input id='userName' name="userName" type="text" value="" class="form-control" ></div>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">完成状态</span>
                                    <cls:enmuSel id="doneStatus" name="doneStatus" options="class:ipt form-control" className="com.team.lottery.enums.EUserBettingDemandStatus"/>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group nomargin text-right">
                                    <button type="button" class="btn btn-outline btn-default btn-js" onclick="userBettingDemandPage.find()"><i class="fa fa-search"></i>&nbsp;查 询</button>
                        		   <button type="button" class="btn btn-outline btn-default btn-add" onclick="userBettingDemandPage.add()"><i class="fa fa-plus"></i>&nbsp;新 增</button>
                                 </div>
                            </div>
                            
                         </div>
                         <div class="row">
                         	
                            <div class="col-sm-5">
	                            <div class="input-group m-b">
	                                 <span class="input-group-addon">日期</span>
	                                 <div class="input-daterange input-group" id="datepicker">
	                                     <input class="form-control layer-date" placeholder="开始日期" name="createDateStart" id="createDateStart" readonly="readonly" style="background-color: white;" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})">
	                                     <span class="input-group-addon">到</span>
	                                     <input class="form-control layer-date" placeholder="结束日期" name="createDateEnd" id="createDateEnd" readonly="readonly" style="background-color: white;" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"></div>
	                             </div>
	                        </div>
	                        <div class="col-sm-2">
	                        	<div class="form-group nomargin" style="margin-left:5px;">
						            <button type="button" class="btn btn-xs btn-info" onclick="userBettingDemandPage.find(1)">今 天</button>
						            <button type="button" class="btn btn-xs btn-danger" onclick="userBettingDemandPage.find(-1)">昨 天</button>
						        	<button type="button" class="btn btn-xs btn-warning" onclick="userBettingDemandPage.find(-7)">最近7天</button><br/>
						        	<button type="button" class="btn btn-xs btn-success" onclick="userBettingDemandPage.find(30)">本 月</button>
						        	<button type="button" class="btn btn-xs btn-primary" onclick="userBettingDemandPage.find(-30)">上 月</button>
	                            </div>
	                        </div>
                         	<div class="col-sm-5">
                         	</div>
                         </div>
                        </c:if>
                        <c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">用户名</span>
                                    <input id='userName' name="userName" type="text" value="" class="form-control" ></div>
                            </div>
                            <div class="col-sm-2">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">完成状态</span>
                                    <cls:enmuSel id="doneStatus" name="doneStatus" options="class:ipt form-control" className="com.team.lottery.enums.EUserBettingDemandStatus"/>
                                </div>
                            </div>
                            <div class="col-sm-4">
	                            <div class="input-group m-b">
	                                 <span class="input-group-addon">日期</span>
	                                 <div class="input-daterange input-group" id="datepicker">
	                                     <input class="form-control layer-date" placeholder="开始日期" name="createDateStart" id="createDateStart" readonly="readonly" style="background-color: white;" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})">
	                                     <span class="input-group-addon">到</span>
	                                     <input class="form-control layer-date" placeholder="结束日期" name="createDateEnd" id="createDateEnd" readonly="readonly" style="background-color: white;" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"></div>
	                             </div>
	                        </div>
	                        <div class="col-sm-2">
	                        	<div class="form-group nomargin" style="margin-left:15px;">
						            <button type="button" class="btn btn-xs btn-info" onclick="userBettingDemandPage.find(1)">今 天</button>
						            <button type="button" class="btn btn-xs btn-danger" onclick="userBettingDemandPage.find(-1)">昨 天</button>
						        	<button type="button" class="btn btn-xs btn-warning" onclick="userBettingDemandPage.find(-7)">最近7天</button><br/>
						        	<button type="button" class="btn btn-xs btn-success" onclick="userBettingDemandPage.find(30)">本 月</button>
						        	<button type="button" class="btn btn-xs btn-primary" onclick="userBettingDemandPage.find(-30)">上 月</button>
	                            </div>
	                        </div>
                            <div class="col-sm-2">
                                <div class="form-group nomargin text-right">
                                    <button type="button" class="btn btn-outline btn-default btn-js" onclick="userBettingDemandPage.find()"><i class="fa fa-search"></i>&nbsp;查 询</button>
                                   	<button type="button" class="btn btn-outline btn-default btn-add" onclick="userBettingDemandPage.add()"><i class="fa fa-plus"></i>&nbsp;新 增</button>
                                </div>
                            </div>
                         </div>
                         </c:if>
                    </form>
                    </div>
                     <div class="jqGrid_wrapper">
                        <div class="ui-jqgrid ">
                            <div class="ui-jqgrid-view">
                                <div class="ui-jqgrid-hdiv">
                                    <div class="ui-jqgrid-hbox" style="padding-right:0">
                                        <table id="userBettingDemandTable" class="ui-jqgrid-htable ui-common-table table table-bordered">
                                            <thead>
                                                <tr class="ui-jqgrid-labels">
                                                 	<th class="ui-th-column ui-th-ltr">序号</th>
                                                    <th class="ui-th-column ui-th-ltr">商户</th>
                                                    <th class="ui-th-column ui-th-ltr">用户名</th>
                                                    <th class="ui-th-column ui-th-ltr">要求打码量</th>
                                                    <th class="ui-th-column ui-th-ltr">完成打码量</th>
                                                    <th class="ui-th-column ui-th-ltr">完成状态</th>
													<th class="ui-th-column ui-th-ltr">创建时间</th>
													<th class="ui-th-column ui-th-ltr">完成时间</th>
													<th class="ui-th-column ui-th-ltr">操 作</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>     
		         </div>
		      </div>
  	<!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/user_betting_demand/js/user_betting_demand.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/manageUserBettingDemandAction.js'></script>  
	<script type='text/javascript' src='<%=path%>/dwr/interface/managerUserAction.js'></script>
</body>
</html>

