function UserBettingDemandPage(){}
var userBettingDemandPage = new UserBettingDemandPage();

userBettingDemandPage.param = {
   	
};

/**
 * 查询参数
 */
userBettingDemandPage.queryParam = {
		bizSystem : null,
		enable : null
};

//分页参数
userBettingDemandPage.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize
};


$(document).ready(function() {
	//默认查询今天
	commonPage.setDateRangeTimeZero(1, "createDateStart", "createDateEnd",2);
	
	userBettingDemandPage.initTableData(); //初始化查询数据
});



/**
 * 初始化列表数据
 */
UserBettingDemandPage.prototype.initTableData = function() {
	userBettingDemandPage.queryParam = getFormObj($("#queryForm"));
	userBettingDemandPage.table = $('#userBettingDemandTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"scrollX": true,
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			userBettingDemandPage.pageParam.pageSize = data.length;//页面显示记录条数
			userBettingDemandPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	manageUserBettingDemandAction.getAllUserBettingDemandPage(userBettingDemandPage.queryParam, userBettingDemandPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data":null},
		            {"data": "bizSystemName","bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "userName",
		            	render: function(data, type, row, meta) {	
		            		
		            		if(row.logType == "ADMIN"){
		            			return data;
		            		}else{
		            		return "<a href='javascript:void(0)' style='color:blue;text-decoration:underline;'  onclick='commonPage.showUserMsgPage(this,"+row.userId+")'>"+ row.userName +"</a>";
		            		}
	            		}
		            },
		            {"data": "requiredBettingMoney"},
		            {"data": "doneBettingMoney"},
		            {"data": "doneStatus",
		            	render: function(data, type, row, meta) {
		            		if(row.doneStatus == "GOING"){
		            			return '<font class="cp-yellow">未完成</font>';
		            		}else if(row.doneStatus == "END"){
		            			return '<font class="cp-green">已完成</font>';
		            		}else if(row.doneStatus == "FORCE_END"){
		            			return '<font class="cp-red">强制完成</font>';
		            		}
		            		return data;
		            	}
		            },
		            {"data": "createDateStr"},
		            {"data": "endDateStr"}, 
		            {"data": "id",/*"bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false,*/
	                	 render: function(data, type, row, meta) {
	                		 var operStr = "<a class='btn btn-info btn-sm btn-bj' onclick='userBettingDemandPage.edit("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;" +
	                		 "<a class='btn btn-warning btn-sm btn-xf' onclick='userBettingDemandPage.forceEnd("+data+")'>&nbsp;强制完成 </a>&nbsp;&nbsp" +
	                		 "<a class='btn btn-danger btn-sm btn-xf' onclick='userBettingDemandPage.del("+data+")'>&nbsp;删除 </a>";
	                		 return operStr;
	                	 }
	                }
	            ]
	}).api(); 
}

/**
 * 查询数据
 */
UserBettingDemandPage.prototype.find = function(dateRange){
	if(dateRange != undefined && dateRange != null && dateRange != ''){
		commonPage.setDateRangeTimeZero(dateRange, "createDateStart", "createDateEnd",2);	
	}
	userBettingDemandPage.queryParam = getFormObj($("#queryForm"));
	userBettingDemandPage.table.ajax.reload();
};

/**
 * 删除
 */
UserBettingDemandPage.prototype.del = function(id){
	
	   swal({
           title: "您确定要删除这条信息吗",
           text: "请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "删除",
           closeOnConfirm: false
       },
       function() {
    	   manageUserBettingDemandAction.delUserBettingDemand(id,function(r){
   			if (r[0] != null && r[0] == "ok") {
   				swal({ title: "提示", text: "删除成功",type: "success"});
   				userBettingDemandPage.find();
   			}else if(r[0] != null && r[0] == "error"){
   				swal(r[1]);
   			}else{
   				swal("删除信息失败.");
   			}
   	    });
       });
};

/**
 * 管理员强制完成
 */
UserBettingDemandPage.prototype.forceEnd = function(id){
	
	   swal({
           title: "您确定要强制完成打码要求么",
           text: "请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "删除",
           closeOnConfirm: false
       },
       function() {
    	   manageUserBettingDemandAction.forceEnd(id,function(r){
   			if (r[0] != null && r[0] == "ok") {
   				swal({ title: "提示", text: "强制完成成功",type: "success"});
   				userBettingDemandPage.find();
   			}else if(r[0] != null && r[0] == "error"){
   				swal(r[1]);
   			}else{
   				swal("删除信息失败.");
   			}
   	    });
       });
};

UserBettingDemandPage.prototype.add=function(){
	
	 layer.open({
         type: 2,
         title: '用户打码要求新增',
         maxmin: false,
         shadeClose: true,
         //点击遮罩关闭层
         area: ['800px', '420px'],
         //area: ['60%', '45%'],
         content: contextPath + "/managerzaizst/user_betting_demand/user_betting_demand_edit.html",
     /*    btn: ['确 定', '关闭'],
         yes: function(index, layero){
        	 alert(index);
        	 var iframeWin = window['layui-layer-iframe' + index].window;
        	 iframeWin.edituserBettingDemandPage.saveData();
        	 iframeWin.saveData();
        	  },*/
         cancel: function(index){ 
        	 userBettingDemandPage.find();
      	 }
     });
}

UserBettingDemandPage.prototype.edit=function(id){
	
	  layer.open({
           type: 2,
           title: '用户打码要求编辑',
           maxmin: false,
           shadeClose: true,
           //点击遮罩关闭层
           area: ['800px', '420px'],
           //area: ['60%', '45%'],
           content: contextPath + "/managerzaizst/user_betting_demand/"+id+"/user_betting_demand_edit.html",
           cancel: function(index){ 
        	   userBettingDemandPage.find();
           }
       });
}

UserBettingDemandPage.prototype.closeLayer=function(index){
	layer.close(index);
	userBettingDemandPage.find();
}


