function UserBettingDemandEditPage(){}
var userBettingDemandEditPage = new UserBettingDemandEditPage();

userBettingDemandEditPage.param = {
   	id : null
};

$(document).ready(function() {
	var id = userBettingDemandEditPage.param.id;  //获取查询ID
	if(id != null){
		$("#userName").attr("disabled", "disabled");
		$("#requiredBettingMoney").attr("disabled", "disabled");
		$("#doneBettingMoneyCol").show();
		$("#addRequiredBettingMoneyCol").show();
		$("#addDoneBettingMoneyCol").show();
		userBettingDemandEditPage.getById(id);
	}
});


/**
 * 保存更新信息
 */
UserBettingDemandEditPage.prototype.saveData = function(){
	var id = $("#id").val();
	var bizSystem = $("#bizSystem").val();
	var userName = $("#userName").val();
	var requiredBettingMoney = $("#requiredBettingMoney").val();
	var doneBettingMoney = $("#doneBettingMoney").val();
	var addRequiredBettingMoney = $("#addRequiredBettingMoney").val();
	var addDoneBettingMoney = $("#addDoneBettingMoney").val();
	
	if(currentUser.bizSystem=='SUPER_SYSTEM' && (bizSystem==null || bizSystem.trim()=="")){
		swal("请输入业务系统!");
		$("#bizSystem").focus();
		return;
	}
	
	if(userName==null || userName.trim()==""){
		swal("请输入用户名!");
		$("#userName").focus();
		return;
	}
	
	var userBettingDemand = {};
	userBettingDemand.id = userBettingDemandEditPage.param.id;
	userBettingDemand.bizSystem = bizSystem;
	userBettingDemand.userName = userName;
	userBettingDemand.requiredBettingMoney = requiredBettingMoney;
	userBettingDemand.doneBettingMoney = doneBettingMoney;
	userBettingDemand.addRequiredBettingMoney = addRequiredBettingMoney;
	userBettingDemand.addDoneBettingMoney = addDoneBettingMoney;
	
	manageUserBettingDemandAction.saveUserBettingDemand(userBettingDemand,function(r){
		if (r[0] != null && r[0] == "ok") {
			 swal({
		           title: r[1],
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.userBettingDemandPage.closeLayer(index);
		   	    }); 
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("保存信息失败.");
		}
    });
};

/**
 * 获取编辑信息
 */
UserBettingDemandEditPage.prototype.getById = function(id){
	manageUserBettingDemandAction.getUserBettingDemand(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			$("#bizSystem").val(r[1].bizSystem);
			$("#userName").val(r[1].userName);
			$("#requiredBettingMoney").val(r[1].requiredBettingMoney);
			$("#doneBettingMoney").val(r[1].doneBettingMoney);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("查询信息失败.");
		}
    });
	
}