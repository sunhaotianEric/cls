<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>商户盈利模式设置</title>
  	<link rel="shortcut icon" href="favicon.ico">
  	<jsp:include page="/admin/include/include.jsp"></jsp:include>
  	<style type="text/css">
  		select[disabled=disabled] { background: #eee; }
  		.yinglv{display:inline-block;margin-left: 15px;vertical-align: middle;}
  		.yinglv label{margin-bottom:0;font-weight:normal;}
  		.yinglv label input{width:40px !important;height:16px !important;background-color: #fff !important;font-size:12px;}
  	</style>
</head>
<body>
<div class="animated fadeIn">
      <div class="tabs-container ibox">
<!--           <ul class="nav nav-tabs">
              <li class="active">
                  <a data-toggle="tab" href="#tab-5" aria-expanded="false">自身彩种盈利模式配置</a></li>
          </ul> -->
          <div class="tab-content">
            <div id="tab-5" class="tab-pane active">
                <div class="panel-body">
                    <form class="form-horizontal">
                    	<div class="row">
                            <div  class="col-sm-12" >
                                <div class="table-block clear" style='overflow:auto'>
                                	<table class="table" style="width:100%;">
				                        <thead>
					                        <tr>
					                            <th id="bizSystemTh">商户</th>
					                            <th>幸运快3</th>
					                            <th>幸运分分彩</th>
					                            <th>极速PK10</th>
					                            <th>幸运六合彩</th>
					                            <th>三分时时彩</th>
					                            <th>五分时时彩</th>
					                            <th>老重庆时时彩</th>
					                            <th>江苏时时彩</th>
					                            <th>北京时时彩</th>
					                            <th>广东时时彩</th>
					                            <th>四川时时彩</th>
					                            <th>上海时时彩</th>
					                            <th>山东时时彩</th>
					                            <th>三分快三</th>
					                            <th>五分快三</th>
					                            <th>五分11选5</th>
					                            <th>三分11选5</th>
					                            <th>十分时时彩</th>
					                            <th>三分pk10</th>
					                            <th>五分pk10</th>
					                            <th>十分pk10</th>
					                            <th id="operateTh">操作</th>
					                        </tr>
				                        </thead>
				                        <tbody id="lottery1">
<!-- 				                     <tr>
				                        		<td>
					                        		<label>666666</label>
					                        	</td>
					                        	<td>
					                        		<label>超级666</label>
					                        	</td>
					                        	<td>
					                        		<select class="ipt" id="jyksProfitModelId" disabled="disabled">
			                                            <option value="0" selected="selected">开启随机模式</option>
			                                            <option value="1">开启随机盈利模式</option>
			                                            <option value="2">开启绝对盈利模式</option>
			                                        </select><span>最高赢率:<input type='text' style='width:30px;height: 16px;' value=''>%<br>最低赢率:<input type='text' style='width:30px;height: 16px' value=''>%</span>
					                        	</td>
					                        	<td>
					                        		<select class="ipt" id="jlffcProfitModelId">
			                                            <option value="0" selected="selected">开启随机模式</option>
			                                            <option value="1">开启随机盈利模式</option>
			                                            <option value="2">开启绝对盈利模式</option>
			                                        </select>
					                        	</td>
					                        	<td>
					                        		<select class="ipt" id="jlffcProfitModelId">
			                                            <option value="0" selected="selected">开启随机模式</option>
			                                            <option value="1">开启随机盈利模式</option>
			                                            <option value="2">开启绝对盈利模式</option>
			                                        </select>
					                        	</td>
					                        	<td>
					                        		<a class="btn btn-w-m btn-white btn-sm" onclick="lotteryControlConfiPage.setSystemConfig()">保 存</a></div>
					                        	</td>
				                        	</tr>  -->
				                        </tbody>
				                    </table>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>     
        </div>
    </div>
    <div class="col-sm-12 ibox">
        <div class="row">
            <div class="col-sm-12 text-center">
                <button type="button" class="btn btn-w-m btn-white" onclick="lotteryControlConfiPage.setLotteryControlConfigStatus()">保 存</button></div>
             <!-- <div class="col-sm-6 text-center">
                <button type="button" class="btn btn-w-m btn-white" onclick="editSystemConfigPage.refreshCache()">一键更新缓存</button></div> 
        </div> -->
    </div>
</div>
</div>
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/lottery_control_config/js/lottery_control_config.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerLotteryControlConfigAction.js'></script>    
</body>
</html>