function LotteryControlConfiPage() {
};
var lotteryControlConfiPage = new LotteryControlConfiPage();

lotteryControlConfiPage.param = {
    bizSystem: null,
    jyksProfitModel: null,
    jlffcProfitModel: null
};

//全局开奖模式设定权限
lotteryControlConfiPage.root = {
    autHority: 1
}

$(function () {
    //子系统管理员 隐藏字段
    if (currentUser.bizSystem != "SUPER_SYSTEM") {
        $("#bizSystemTh").hide();
        $("#operateTh").hide();
    }

    lotteryControlConfiPage.initTableData();

});

/**
 * 数据获取，不用分页
 */
LotteryControlConfiPage.prototype.initTableData = function () {
    managerLotteryControlConfigAction.getLotteryControlConfig(lotteryControlConfiPage.param, function (r) {
        if (r[0] == "ok") {
            lotteryControlConfiPage.root.autHority = r[2].configValue;
            $("#lottery1").empty();
            if (r[1] != null) {

                $(r[1]).each(function (i, idx) {
                    var jyks = {jyks0: "", jyks1: "", jyks2: "", jyks3: ""};
                    var jlffc = {jlffc0: "", jlffc1: "", jlffc2: "", jlffc3: ""};
                    var jspks = {jspks0: "", jspks1: "", jspks2: "", jspks3: ""};
                    var xylhc = {xylhc0: "", xylhc1: "", xylhc2: "", xylhc3: ""};
                    var sfssc = {sfssc0: "", sfssc1: "", sfssc2: "", sfssc3: ""};
                    var wfssc = {wfssc0: "", wfssc1: "", wfssc2: "", wfssc3: ""};
                    var lcqssc = {lcqssc0: "", lcqssc1: "", lcqssc2: "", lcqssc3: ""};
                    var jsssc = {jsssc0: "", jsssc1: "", jsssc2: "", jsssc3: ""};
                    var bjssc = {bjssc0: "", bjssc1: "", bjssc2: "", bjssc3: ""};
                    var gdssc = {gdssc0: "", gdssc1: "", gdssc2: "", gdssc3: ""};
                    var scssc = {scssc0: "", scssc1: "", scssc2: "", scssc3: ""};
                    var shssc = {shssc0: "", shssc1: "", shssc2: "", shssc3: ""};
                    var sdssc = {sdssc0: "", sdssc1: "", sdssc2: "", sdssc3: ""};
                    var sfks = {sfks0: "", sfks1: "", sfks2: "", sfks3: ""};
                    var wfks = {wfks0: "", wfks1: "", wfks2: "", wfks3: ""};
                    var wfsyxw = {wfsyxw0: "", wfsyxw1: "", wfsyxw2: "", wfsyxw3: ""};
                    var sfsyxw = {sfsyxw0: "", sfsyxw1: "", sfsyxw2: "", sfsyxw3: ""};
                    var shfssc = {shfssc0: "", shfssc1: "", shfssc2: "", shfssc3: ""};
                    var sfpk10 = {sfpk100: "", sfpk101: "", sfpk102: "", sfpk103: ""};
                    var wfpk10 = {wfpk100: "", wfpk101: "", wfpk102: "", wfpk103: ""};
                    var shfpk10 = {shfpk100: "", shfpk101: "", shfpk102: "", shfpk103: ""};

                    if (idx.sfksProfitModel == 0) {
                        sfks.sfks0 = "selected";
                    } else if (idx.sfksProfitModel == 1) {
                        sfks.sfks1 = "selected";
                    } else if (idx.sfksProfitModel == 2) {
                        sfks.sfks2 = "selected";
                    } else if (idx.sfksProfitModel == 3) {
                        sfks.sfks3 = "selected";
                    }

                    if (idx.wfksProfitModel == 0) {
                        wfks.wfks0 = "selected";
                    } else if (idx.wfksProfitModel == 1) {
                        wfks.wfks1 = "selected";
                    } else if (idx.wfksProfitModel == 2) {
                        wfks.wfks2 = "selected";
                    } else if (idx.wfksProfitModel == 3) {
                        wfks.wfks3 = "selected";
                    }

                    if (idx.sfsscProfitModel == 0) {
                        sfssc.sfssc0 = "selected";
                    } else if (idx.sfsscProfitModel == 1) {
                        sfssc.sfssc1 = "selected";
                    } else if (idx.sfsscProfitModel == 2) {
                        sfssc.sfssc2 = "selected";
                    } else if (idx.sfsscProfitModel == 3) {
                        sfssc.sfssc3 = "selected";
                    }

                    if (idx.shfsscProfitModel == 0) {
                        shfssc.shfssc0 = "selected";
                    } else if (idx.shfsscProfitModel == 1) {
                        shfssc.shfssc1 = "selected";
                    } else if (idx.shfsscProfitModel == 2) {
                        shfssc.shfssc2 = "selected";
                    } else if (idx.shfsscProfitModel == 3) {
                        shfssc.shfssc3 = "selected";
                    }

                    if (idx.wfsscProfitModel == 0) {
                        wfssc.wfssc0 = "selected";
                    } else if (idx.wfsscProfitModel == 1) {
                        wfssc.wfssc1 = "selected";
                    } else if (idx.wfsscProfitModel == 2) {
                        wfssc.wfssc2 = "selected";
                    } else if (idx.wfsscProfitModel == 3) {
                        wfssc.wfssc3 = "selected";
                    }

                    if (idx.lcqsscProfitModel == 0) {
                        lcqssc.lcqssc0 = "selected";
                    } else if (idx.lcqsscProfitModel == 1) {
                        lcqssc.lcqssc1 = "selected";
                    } else if (idx.lcqsscProfitModel == 2) {
                        lcqssc.lcqssc2 = "selected";
                    } else if (idx.lcqsscProfitModel == 3) {
                        lcqssc.lcqssc3 = "selected";
                    }

                    if (idx.jssscProfitModel == 0) {
                        jsssc.jsssc0 = "selected";
                    } else if (idx.jssscProfitModel == 1) {
                        jsssc.jsssc1 = "selected";
                    } else if (idx.jssscProfitModel == 2) {
                        jsssc.jsssc2 = "selected";
                    } else if (idx.jssscProfitModel == 3) {
                        jsssc.jsssc3 = "selected";
                    }

                    if (idx.bjsscProfitModel == 0) {
                        bjssc.bjssc0 = "selected";
                    } else if (idx.bjsscProfitModel == 1) {
                        bjssc.bjssc1 = "selected";
                    } else if (idx.bjsscProfitModel == 2) {
                        bjssc.bjssc2 = "selected";
                    } else if (idx.bjsscProfitModel == 3) {
                        lcqssc.bjssc3 = "selected";
                    }

                    if (idx.gdsscProfitModel == 0) {
                        gdssc.gdssc0 = "selected";
                    } else if (idx.gdsscProfitModel == 1) {
                        gdssc.gdssc1 = "selected";
                    } else if (idx.gdsscProfitModel == 2) {
                        gdssc.gdssc2 = "selected";
                    } else if (idx.gdsscProfitModel == 3) {
                        gdssc.gdssc3 = "selected";
                    }

                    if (idx.scsscProfitModel == 0) {
                        scssc.scssc0 = "selected";
                    } else if (idx.scsscProfitModel == 1) {
                        scssc.scssc1 = "selected";
                    } else if (idx.scsscProfitModel == 2) {
                        scssc.scssc2 = "selected";
                    } else if (idx.scsscProfitModel == 3) {
                        scssc.scssc3 = "selected";
                    }

                    if (idx.shsscProfitModel == 0) {
                        shssc.shssc0 = "selected";
                    } else if (idx.shsscProfitModel == 1) {
                        shssc.shssc1 = "selected";
                    } else if (idx.shsscProfitModel == 2) {
                        shssc.shssc2 = "selected";
                    } else if (idx.shsscProfitModel == 3) {
                        shssc.shssc3 = "selected";
                    }

                    if (idx.sdsscProfitModel == 0) {
                        sdssc.sdssc0 = "selected";
                    } else if (idx.sdsscProfitModel == 1) {
                        sdssc.sdssc1 = "selected";
                    } else if (idx.sdsscProfitModel == 2) {
                        sdssc.sdssc2 = "selected";
                    } else if (idx.sdsscProfitModel == 3) {
                        sdssc.sdssc3 = "selected";
                    }

                    if (idx.wfsyxwProfitModel == 0) {
                        wfsyxw.wfsyxw0 = "selected";
                    } else if (idx.wfsyxwProfitModel == 1) {
                        wfsyxw.wfsyxw1 = "selected";
                    } else if (idx.wfsyxwProfitModel == 2) {
                        wfsyxw.wfsyxw2 = "selected";
                    } else if (idx.wfsyxwProfitModel == 3) {
                        wfsyxw.wfsyxw3 = "selected";
                    }

                    if (idx.sfsyxwProfitModel == 0) {
                        sfsyxw.sfsyxw0 = "selected";
                    } else if (idx.sfsyxwProfitModel == 1) {
                        sfsyxw.sfsyxw1 = "selected";
                    } else if (idx.sfsyxwProfitModel == 2) {
                        sfsyxw.sfsyxw2 = "selected";
                    } else if (idx.wfsyxwProfitModel == 3) {
                        sfsyxw.sfsyxw3 = "selected";
                    }

                    if (idx.jyksProfitModel == 0) {
                        jyks.jyks0 = "selected";
                    } else if (idx.jyksProfitModel == 1) {
                        jyks.jyks1 = "selected";
                    } else if (idx.jyksProfitModel == 2) {
                        jyks.jyks2 = "selected";
                    } else if (idx.jyksProfitModel == 3) {
                        jyks.jyks3 = "selected";
                    }

                    if (idx.jlffcProfitModel == 0) {
                        jlffc.jlffc0 = "selected";
                    } else if (idx.jlffcProfitModel == 1) {
                        jlffc.jlffc1 = "selected";
                    } else if (idx.jlffcProfitModel == 2) {
                        jlffc.jlffc2 = "selected";
                    } else if (idx.jlffcProfitModel == 3) {
                        jlffc.jlffc3 = "selected";
                    }

                    if (idx.jspk10ProfitModel == 0) {
                        jspks.jspks0 = "selected";
                    } else if (idx.jspk10ProfitModel == 1) {
                        jspks.jspks1 = "selected";
                    } else if (idx.jspk10ProfitModel == 2) {
                        jspks.jspks2 = "selected";
                    } else if (idx.jspk10ProfitModel == 3) {
                        jspks.jspks3 = "selected";
                    }

                    if (idx.sfpk10ProfitModel == 0) {
                        sfpk10.sfpk100 = "selected";
                    } else if (idx.sfpk10ProfitModel == 1) {
                        sfpk10.sfpk101 = "selected";
                    } else if (idx.sfpk10ProfitModel == 2) {
                        sfpk10.sfpk102 = "selected";
                    } else if (idx.sfpk10ProfitModel == 3) {
                        sfpk10.sfpk103 = "selected";
                    }

                    if (idx.wfpk10ProfitModel == 0) {
                        wfpk10.wfpk100 = "selected";
                    } else if (idx.wfpk10ProfitModel == 1) {
                        wfpk10.wfpk101 = "selected";
                    } else if (idx.wfpk10ProfitModel == 2) {
                        wfpk10.wfpk102 = "selected";
                    } else if (idx.wfpk10ProfitModel == 3) {
                        wfpk10.wfpk103 = "selected";
                    }

                    if (idx.shfpk10ProfitModel == 0) {
                        shfpk10.shfpk100 = "selected";
                    } else if (idx.shfpk10ProfitModel == 1) {
                        shfpk10.shfpk101 = "selected";
                    } else if (idx.shfpk10ProfitModel == 2) {
                        shfpk10.shfpk102 = "selected";
                    } else if (idx.shfpk10ProfitModel == 3) {
                        shfpk10.shfpk103 = "selected";
                    }

                    if (idx.xylhcProfitModel == 0) {
                        xylhc.xylhc0 = "selected";
                    } else if (idx.xylhcProfitModel == 1) {
                        xylhc.xylhc1 = "selected";
                    } else if (idx.xylhcProfitModel == 2) {
                        xylhc.xylhc2 = "selected";
                    } else if (idx.xylhcProfitModel == 3) {
                        xylhc.xylhc3 = "selected";
                    }

                    var sfpk10InputBoxStr = "<span class='yinglv " + idx.bizSystem + "sfpk10' style='display:none'><label>最高赢率:<input type='text' name='" + idx.bizSystem + "Sfpk10MaximumWinRate' value=''>%</label>" +
                        "<label>最低赢率:<input type='text' name='" + idx.bizSystem + "Sfpk10MinimumWinRate' value=''>%</label></span>";

                    var wfpk10InputBoxStr = "<span class='yinglv " + idx.bizSystem + "wfpk10' style='display:none'><label>最高赢率:<input type='text' name='" + idx.bizSystem + "Wfpk10MaximumWinRate' value=''>%</label>" +
                        "<label>最低赢率:<input type='text' name='" + idx.bizSystem + "Wfpk10MinimumWinRate' value=''>%</label></span>";

                    var shfpk10InputBoxStr = "<span class='yinglv " + idx.bizSystem + "shfpk10' style='display:none'><label>最高赢率:<input type='text' name='" + idx.bizSystem + "Shfpk10MaximumWinRate' value=''>%</label>" +
                        "<label>最低赢率:<input type='text' name='" + idx.bizSystem + "Shfpk10MinimumWinRate' value=''>%</label></span>";

                    var sfksInputBoxStr = "<span class='yinglv " + idx.bizSystem + "sfks' style='display:none'><label>最高赢率:<input type='text' name='" + idx.bizSystem + "SfksMaximumWinRate' value=''>%</label>" +
                        "<label>最低赢率:<input type='text' name='" + idx.bizSystem + "SfksMinimumWinRate' value=''>%</label></span>";

                    var wfksInputBoxStr = "<span class='yinglv " + idx.bizSystem + "wfks' style='display:none'><label>最高赢率:<input type='text' name='" + idx.bizSystem + "WfksMaximumWinRate' value=''>%</label>" +
                        "<label>最低赢率:<input type='text' name='" + idx.bizSystem + "WfksMinimumWinRate' value=''>%</label></span>";

                    var sfsscInputBoxStr = "<span class='yinglv " + idx.bizSystem + "sfssc' style='display:none'><label>最高赢率:<input type='text' name='" + idx.bizSystem + "SfsscMaximumWinRate' value=''>%</label>" +
                        "<label>最低赢率:<input type='text' name='" + idx.bizSystem + "SfsscMinimumWinRate' value=''>%</label></span>";

                    var shfsscInputBoxStr = "<span class='yinglv " + idx.bizSystem + "shfssc' style='display:none'><label>最高赢率:<input type='text' name='" + idx.bizSystem + "ShfsscMaximumWinRate' value=''>%</label>" +
                        "<label>最低赢率:<input type='text' name='" + idx.bizSystem + "ShfsscMinimumWinRate' value=''>%</label></span>";

                    var wfsscInputBoxStr = "<span class='yinglv " + idx.bizSystem + "wfssc' style='display:none'><label>最高赢率:<input type='text' name='" + idx.bizSystem + "WfsscMaximumWinRate' value=''>%</label>" +
                        "<label>最低赢率:<input type='text' name='" + idx.bizSystem + "WfsscMinimumWinRate' value=''>%</label></span>";

                    var lcqsscInputBoxStr = "<span class='yinglv " + idx.bizSystem + "lcqssc' style='display:none'><label>最高赢率:<input type='text' name='" + idx.bizSystem + "LcqsscMaximumWinRate' value=''>%</label>" +
                        "<label>最低赢率:<input type='text' name='" + idx.bizSystem + "LcqsscMinimumWinRate' value=''>%</label></span>";

                    var jssscInputBoxStr = "<span class='yinglv " + idx.bizSystem + "jsssc' style='display:none'><label>最高赢率:<input type='text' name='" + idx.bizSystem + "JssscMaximumWinRate' value=''>%</label>" +
                        "<label>最低赢率:<input type='text' name='" + idx.bizSystem + "JssscMinimumWinRate' value=''>%</label></span>";

                    var bjsscInputBoxStr = "<span class='yinglv " + idx.bizSystem + "bjssc' style='display:none'><label>最高赢率:<input type='text' name='" + idx.bizSystem + "BjsscMaximumWinRate' value=''>%</label>" +
                        "<label>最低赢率:<input type='text' name='" + idx.bizSystem + "BjsscMinimumWinRate' value=''>%</label></span>";

                    var gdsscInputBoxStr = "<span class='yinglv " + idx.bizSystem + "gdssc' style='display:none'><label>最高赢率:<input type='text' name='" + idx.bizSystem + "GdsscMaximumWinRate' value=''>%</label>" +
                        "<label>最低赢率:<input type='text' name='" + idx.bizSystem + "GdsscMinimumWinRate' value=''>%</label></span>";

                    var scsscInputBoxStr = "<span class='yinglv " + idx.bizSystem + "scssc' style='display:none'><label>最高赢率:<input type='text' name='" + idx.bizSystem + "ScsscMaximumWinRate' value=''>%</label>" +
                        "<label>最低赢率:<input type='text' name='" + idx.bizSystem + "ScsscMinimumWinRate' value=''>%</label></span>";

                    var shsscInputBoxStr = "<span class='yinglv " + idx.bizSystem + "shssc' style='display:none'><label>最高赢率:<input type='text' name='" + idx.bizSystem + "ShsscMaximumWinRate' value=''>%</label>" +
                        "<label>最低赢率:<input type='text' name='" + idx.bizSystem + "ShsscMinimumWinRate' value=''>%</label></span>";

                    var sdsscInputBoxStr = "<span class='yinglv " + idx.bizSystem + "sdssc' style='display:none'><label>最高赢率:<input type='text' name='" + idx.bizSystem + "SdsscMaximumWinRate' value=''>%</label>" +
                        "<label>最低赢率:<input type='text' name='" + idx.bizSystem + "SdsscMinimumWinRate' value=''>%</label></span>";

                    var sfsyxwInputBoxStr = "<span class='yinglv " + idx.bizSystem + "sfsyxw' style='display:none'><label>最高赢率:<input type='text' name='" + idx.bizSystem + "SfsyxwMaximumWinRate' value=''>%</label>" +
                        "<label>最低赢率:<input type='text' name='" + idx.bizSystem + "SfsyxwMinimumWinRate' value=''>%</label></span>";

                    var wfsyxwInputBoxStr = "<span class='yinglv " + idx.bizSystem + "wfsyxw' style='display:none'><label>最高赢率:<input type='text' name='" + idx.bizSystem + "WfsyxwMaximumWinRate' value=''>%</label>" +
                        "<label>最低赢率:<input type='text' name='" + idx.bizSystem + "WfsyxwMinimumWinRate' value=''>%</label></span>";

                    var jyksInputBoxStr = "<span class='yinglv " + idx.bizSystem + "jyks' style='display:none'><label>最高赢率:<input type='text' name='" + idx.bizSystem + "JyksMaximumWinRate' value=''>%</label>" +
                        "<label>最低赢率:<input type='text' name='" + idx.bizSystem + "JyksMinimumWinRate' value=''>%</label></span>";

                    var jlffcInputBoxStr = "<span class='yinglv " + idx.bizSystem + "jlffc' style='display:none'><label>最高赢率:<input type='text' name='" + idx.bizSystem + "JlffcMaximumWinRate' value=''>%</label>" +
                        "<label>最低赢率:<input type='text' name='" + idx.bizSystem + "JlffcMinimumWinRate' value=''>%</label></span>";

                    var jspksInputBoxStr = "<span class='yinglv " + idx.bizSystem + "jspks' style='display:none'><label>最高赢率:<input type='text' name='" + idx.bizSystem + "JspksMaximumWinRate' value=''>%</label>" +
                        "<label>最低赢率:<input type='text' name='" + idx.bizSystem + "JspksMinimumWinRate' value=''>%</label></span>";

                    var xylhcInputBoxStr = "<span class='yinglv " + idx.bizSystem + "xylhc' style='display:none'><label>最高赢率:<input type='text' name='" + idx.bizSystem + "XylhcMaximumWinRate' value=''>%</label>" +
                        "<label>最低赢率:<input type='text' name='" + idx.bizSystem + "XylhcMinimumWinRate' value=''>%</label></span>";

                    var root = "";
                    if (idx.enabled == 0 || r[2].configValue == 0) {
                        root = "disabled='disabled'";
                    }
                    if (idx.bizSystem != "SUPER_SYSTEM") {
                        var str = "<tr id=" + idx.id + " data-value=" + idx.bizSystem + ">";
                        if (currentUser.bizSystem == "SUPER_SYSTEM") {
                            str += "<td>" +
                                "<label>" + idx.bizSystemName + "</label>" +
                                "</td>";
                        }
                        str += "<td>" +
                            "<select class='ipt " + idx.bizSystem + "JyksSelect' id='jyksProfitModel' " + root + ">" +
                            "<option value='0' " + jyks.jyks0 + ">开启随机模式</option>" +
                            "<option value='1' " + jyks.jyks1 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + jyks.jyks2 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + jyks.jyks3 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +
                            "<td>" +
                            "<select class='ipt " + idx.bizSystem + "JlffcSelect' id='jlffcProfitModel' " + root + ">" +
                            "<option value='0' " + jlffc.jlffc0 + ">开启随机模式</option>" +
                            "<option value='1' " + jlffc.jlffc1 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + jlffc.jlffc2 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + jlffc.jlffc3 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +
                            "<td>" +
                            "<select class='ipt " + idx.bizSystem + "JspksSelect' id='jspk10ProfitModel' " + root + ">" +
                            "<option value='0' " + jspks.jspks0 + ">开启随机模式</option>" +
                            "<option value='1' " + jspks.jspks1 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + jspks.jspks2 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + jspks.jspks3 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +
                            "<td>" +
                            "<select class='ipt " + idx.bizSystem + "XylhcSelect' id='xylhcProfitModel' " + root + ">" +
                            "<option value='0' " + xylhc.xylhc0 + ">开启随机模式</option>" +
                            "<option value='1' " + xylhc.xylhc1 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + xylhc.xylhc2 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + xylhc.xylhc3 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +
                            "<td>" +
                            "<select class='ipt " + idx.bizSystem + "SfsscSelect' id='sfsscProfitModel' " + root + ">" +
                            "<option value='0' " + sfssc.sfssc0 + ">开启随机模式</option>" +
                            "<option value='1' " + sfssc.sfssc1 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + sfssc.sfssc2 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + sfssc.sfssc3 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +
                            "<td>" +
                            "<select class='ipt " + idx.bizSystem + "WfsscSelect' id='wfsscProfitModel' " + root + ">" +
                            "<option value='0' " + wfssc.wfssc0 + ">开启随机模式</option>" +
                            "<option value='1' " + wfssc.wfssc1 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + wfssc.wfssc2 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + wfssc.wfssc3 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +
                            "<td>" +
                            "<select class='ipt " + idx.bizSystem + "LcqsscSelect' id='lcqsscProfitModel' " + root + ">" +
                            "<option value='0' " + lcqssc.lcqssc0 + ">开启随机模式</option>" +
                            "<option value='1' " + lcqssc.lcqssc1 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + lcqssc.lcqssc2 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + lcqssc.lcqssc3 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +

                            "<td>" +
                            "<select class='ipt " + idx.bizSystem + "jssscSelect' id='jssscProfitModel' " + root + ">" +
                            "<option value='0' " + jsssc.jsssc0 + ">开启随机模式</option>" +
                            "<option value='1' " + jsssc.jsssc1 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + jsssc.jsssc2 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + jsssc.jsssc3 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +

                            "<td>" +
                            "<select class='ipt " + idx.bizSystem + "bjsscSelect' id='bjsscProfitModel' " + root + ">" +
                            "<option value='0' " + bjssc.bjssc0 + ">开启随机模式</option>" +
                            "<option value='1' " + bjssc.bjssc1 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + bjssc.bjssc2 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + bjssc.bjssc3 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +

                            "<td>" +
                            "<select class='ipt " + idx.bizSystem + "gdsscSelect' id='gdsscProfitModel' " + root + ">" +
                            "<option value='0' " + gdssc.gdssc0 + ">开启随机模式</option>" +
                            "<option value='1' " + gdssc.gdssc1 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + gdssc.gdssc2 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + gdssc.gdssc3 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +

                            "<td>" +
                            "<select class='ipt " + idx.bizSystem + "scsscSelect' id='scsscProfitModel' " + root + ">" +
                            "<option value='0' " + scssc.scssc0 + ">开启随机模式</option>" +
                            "<option value='1' " + scssc.scssc1 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + scssc.scssc2 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + scssc.scssc3 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +

                            "<td>" +
                            "<select class='ipt " + idx.bizSystem + "shsscSelect' id='shsscProfitModel' " + root + ">" +
                            "<option value='0' " + shssc.shssc0 + ">开启随机模式</option>" +
                            "<option value='1' " + shssc.shssc1 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + shssc.shssc2 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + shssc.shssc3 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +

                            "<td>" +
                            "<select class='ipt " + idx.bizSystem + "sdsscSelect' id='sdsscProfitModel' " + root + ">" +
                            "<option value='0' " + sdssc.sdssc0 + ">开启随机模式</option>" +
                            "<option value='1' " + sdssc.sdssc1 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + sdssc.sdssc2 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + sdssc.sdssc3 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +


                            "<td>" +
                            "<select class='ipt " + idx.bizSystem + "SfksSelect' id='sfksProfitModel' " + root + ">" +
                            "<option value='0' " + sfks.sfks0 + ">开启随机模式</option>" +
                            "<option value='1' " + sfks.sfks1 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + sfks.sfks2 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + sfks.sfks3 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +
                            "<td>" +
                            "<select class='ipt " + idx.bizSystem + "WfksSelect' id='wfksProfitModel' " + root + ">" +
                            "<option value='0' " + wfks.wfks0 + ">开启随机模式</option>" +
                            "<option value='1' " + wfks.wfks1 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + wfks.wfks2 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + wfks.wfks3 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +
                            "<td>" +
                            "<select class='ipt " + idx.bizSystem + "WfsyxwSelect' id='wfsyxwProfitModel' " + root + ">" +
                            "<option value='0' " + wfsyxw.wfsyxw0 + ">开启随机模式</option>" +
                            "<option value='1' " + wfsyxw.wfsyxw1 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + wfsyxw.wfsyxw2 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + wfsyxw.wfsyxw3 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +
                            "<td>" +
                            "<select class='ipt " + idx.bizSystem + "SfsyxwSelect' id='sfsyxwProfitModel' " + root + ">" +
                            "<option value='0' " + sfsyxw.sfsyxw0 + ">开启随机模式</option>" +
                            "<option value='1' " + sfsyxw.sfsyxw1 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + sfsyxw.sfsyxw2 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + sfsyxw.sfsyxw3 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +
                            "<td>" +
                            "<select class='ipt " + idx.bizSystem + "ShfsscSelect' id='shfsscProfitModel' " + root + ">" +
                            "<option value='0' " + shfssc.shfssc0 + ">开启随机模式</option>" +
                            "<option value='1' " + shfssc.shfssc1 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + shfssc.shfssc2 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + shfssc.shfssc3 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +
                            "<td>" +
                            "<select class='ipt " + idx.bizSystem + "Sfpk10Select' id='sfpk10ProfitModel' " + root + ">" +
                            "<option value='0' " + sfpk10.sfpk100 + ">开启随机模式</option>" +
                            "<option value='1' " + sfpk10.sfpk101 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + sfpk10.sfpk102 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + sfpk10.sfpk103 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +
                            "<td>" +
                            "<select class='ipt " + idx.bizSystem + "Wfpk10Select' id='wfpk10ProfitModel' " + root + ">" +
                            "<option value='0' " + wfpk10.wfpk100 + ">开启随机模式</option>" +
                            "<option value='1' " + wfpk10.wfpk101 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + wfpk10.wfpk102 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + wfpk10.wfpk103 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +
                            "<td>" +
                            "<select class='ipt " + idx.bizSystem + "Shfpk10Select' id='shfpk10ProfitModel' " + root + ">" +
                            "<option value='0' " + shfpk10.shfpk100 + ">开启随机模式</option>" +
                            "<option value='1' " + shfpk10.shfpk101 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + shfpk10.shfpk102 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + shfpk10.shfpk103 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>";

                        if (currentUser.bizSystem == "SUPER_SYSTEM") {
                            str += "<td>";
                            if (idx.enabled == 0) {
                                str += "<a class='btn btn-sm btn-bj btn-success' onclick=lotteryControlConfiPage.setEnabled(" + idx.id + ",'" + idx.bizSystem + "',1)><i class='fa fa-pencil'></i>&nbsp;启用</a></div>";
                            } else {
                                str += "<a class='btn btn-danger btn-sm btn-del' onclick=lotteryControlConfiPage.setEnabled(" + idx.id + ",'" + idx.bizSystem + "',0)><i class='fa fa-pencil'></i>&nbsp;关闭</a></div>";
                            }
                            str += "</td>";
                        }
                        str += "</tr>";
                        $("#lottery1").append(str);
                    } else {
                        var sfpk10DefaultInputBoxStr = "<span class='yinglv sfpk10DefaultBoxStr' style='display:none'><label>最高赢率:<input type='text' name='sfpk10DefaultMaximumWinRate' value=''>%</label>" +
                            "<label>最低赢率:<input type='text' name='sfpk10DefaultMinimumWinRate' value=''>%</label></span>";

                        var wfpk10DefaultInputBoxStr = "<span class='yinglv wfpk10DefaultBoxStr' style='display:none'><label>最高赢率:<input type='text' name='wfpk10DefaultMaximumWinRate' value=''>%</label>" +
                            "<label>最低赢率:<input type='text' name='wfpk10DefaultMinimumWinRate' value=''>%</label></span>";

                        var shfpk10DefaultInputBoxStr = "<span class='yinglv shfpk10DefaultBoxStr' style='display:none'><label>最高赢率:<input type='text' name='shfpk10DefaultMaximumWinRate' value=''>%</label>" +
                            "<label>最低赢率:<input type='text' name='shfpk10DefaultMinimumWinRate' value=''>%</label></span>";

                        var sfksDefaultInputBoxStr = "<span class='yinglv sfksDefaultBoxStr' style='display:none'><label>最高赢率:<input type='text' name='sfksDefaultMaximumWinRate' value=''>%</label>" +
                            "<label>最低赢率:<input type='text' name='sfksDefaultMinimumWinRate' value=''>%</label></span>";

                        var wfksDefaultInputBoxStr = "<span class='yinglv wfksDefaultBoxStr' style='display:none'><label>最高赢率:<input type='text' name='wfksDefaultMaximumWinRate' value=''>%</label>" +
                            "<label>最低赢率:<input type='text' name='wfksDefaultMinimumWinRate' value=''>%</label></span>";

                        var sfsscDefaultInputBoxStr = "<span class='yinglv sfsscDefaultBoxStr' style='display:none'><label>最高赢率:<input type='text' name='sfsscDefaultMaximumWinRate' value=''>%</label>" +
                            "<label>最低赢率:<input type='text' name='sfsscDefaultMinimumWinRate' value=''>%</label></span>";

                        var shfsscDefaultInputBoxStr = "<span class='yinglv shfsscDefaultBoxStr' style='display:none'><label>最高赢率:<input type='text' name='shfsscDefaultMaximumWinRate' value=''>%</label>" +
                            "<label>最低赢率:<input type='text' name='shfsscDefaultMinimumWinRate' value=''>%</label></span>";

                        var wfsscDefaultInputBoxStr = "<span class='yinglv wfsscDefaultBoxStr' style='display:none'><label>最高赢率:<input type='text' name='wfsscDefaultMaximumWinRate' value=''>%</label>" +
                            "<label>最低赢率:<input type='text' name='wfsscDefaultMinimumWinRate' value=''>%</label></span>";

                        var lcqsscDefaultInputBoxStr = "<span class='yinglv lcqsscDefaultBoxStr' style='display:none'><label>最高赢率:<input type='text' name='lcqsscDefaultMaximumWinRate' value=''>%</label>" +
                            "<label>最低赢率:<input type='text' name='lcqsscDefaultMinimumWinRate' value=''>%</label></span>";

                        var jssscDefaultInputBoxStr = "<span class='yinglv jssscDefaultBoxStr' style='display:none'><label>最高赢率:<input type='text' name='jssscDefaultMaximumWinRate' value=''>%</label>" +
                            "<label>最低赢率:<input type='text' name='jssscDefaultMinimumWinRate' value=''>%</label></span>";

                        var bjsscDefaultInputBoxStr = "<span class='yinglv bjsscDefaultBoxStr' style='display:none'><label>最高赢率:<input type='text' name='bjsscDefaultMaximumWinRate' value=''>%</label>" +
                            "<label>最低赢率:<input type='text' name='bjsscDefaultMinimumWinRate' value=''>%</label></span>";

                        var gdsscDefaultInputBoxStr = "<span class='yinglv gdsscDefaultBoxStr' style='display:none'><label>最高赢率:<input type='text' name='gdsscDefaultMaximumWinRate' value=''>%</label>" +
                            "<label>最低赢率:<input type='text' name='gdsscDefaultMinimumWinRate' value=''>%</label></span>";

                        var scsscDefaultInputBoxStr = "<span class='yinglv scsscDefaultBoxStr' style='display:none'><label>最高赢率:<input type='text' name='scsscDefaultMaximumWinRate' value=''>%</label>" +
                            "<label>最低赢率:<input type='text' name='scsscDefaultMinimumWinRate' value=''>%</label></span>";

                        var shsscDefaultInputBoxStr = "<span class='yinglv shsscDefaultBoxStr' style='display:none'><label>最高赢率:<input type='text' name='shsscDefaultMaximumWinRate' value=''>%</label>" +
                            "<label>最低赢率:<input type='text' name='shsscDefaultMinimumWinRate' value=''>%</label></span>";

                        var sdsscDefaultInputBoxStr = "<span class='yinglv sdsscDefaultBoxStr' style='display:none'><label>最高赢率:<input type='text' name='sdsscDefaultMaximumWinRate' value=''>%</label>" +
                            "<label>最低赢率:<input type='text' name='sdsscDefaultMinimumWinRate' value=''>%</label></span>";

                        var wfsyxwDefaultInputBoxStr = "<span class='yinglv wfsyxwDefaultBoxStr' style='display:none'><label>最高赢率:<input type='text' name='wfsyxwDefaultMaximumWinRate' value=''>%</label>" +
                            "<label>最低赢率:<input type='text' name='wfsyxwDefaultMinimumWinRate' value=''>%</label></span>";

                        var sfsyxwDefaultInputBoxStr = "<span class='yinglv sfsyxwDefaultBoxStr' style='display:none'><label>最高赢率:<input type='text' name='sfsyxwDefaultMaximumWinRate' value=''>%</label>" +
                            "<label>最低赢率:<input type='text' name='sfsyxwDefaultMinimumWinRate' value=''>%</label></span>";

                        var jyksDefaultInputBoxStr = "<span class='yinglv jyksDefaultBoxStr' style='display:none'><label>最高赢率:<input type='text' name='jyksDefaultMaximumWinRate' value=''>%</label>" +
                            "<label>最低赢率:<input type='text' name='jyksDefaultMinimumWinRate' value=''>%</label></span>";

                        var jlffcDefaultInputBoxStr = "<span class='yinglv jlffcDefaultBoxStr' style='display:none'><label>最高赢率:<input type='text' name='jlffcDefaultMaximumWinRate' value=''>%</label>" +
                            "<label>最低赢率:<input type='text' name='jlffcDefaultMinimumWinRate' value=''>%</label></span>";


                        var jspksDefaultInputBoxStr = "<span class='yinglv jspksDefaultBoxStr' style='display:none'><label>最高赢率:<input type='text' name='jspksDefaultMaximumWinRate' value=''>%</label>" +
                            "<label>最低赢率:<input type='text' name='jspksDefaultMinimumWinRate' value=''>%</label></span>";

                        var xylhcDefaultInputBoxStr = "<span class='yinglv xylhcDefaultBoxStr' style='display:none'><label>最高赢率:<input type='text' name='xylhcDefaultMaximumWinRate' value=''>%</label>" +
                            "<label>最低赢率:<input type='text' name='xylhcDefaultMinimumWinRate' value=''>%</label></span>";

                        var str1 = "<tr id=" + idx.id + " data-value='SUPER_SYSTEM'>" +
                            "<td>" +
                            "<label>默认</label>" +
                            "</td>" +
                            "<td>" +
                            "<select class='ipt jyksDefault' id='jyksProfitModel' >" +
                            "<option value='0' " + jyks.jyks0 + " selected>开启随机模式</option>" +
                            "<option value='1' " + jyks.jyks1 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + jyks.jyks2 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + jyks.jyks3 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +
                            "<td>" +
                            "<select class='ipt jlffcDefault' id='jlffcProfitModel'>" +
                            "<option value='0' " + jlffc.jlffc0 + ">开启随机模式</option>" +
                            "<option value='1' " + jlffc.jlffc1 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + jlffc.jlffc2 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + jlffc.jlffc3 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +
                            "<td>" +
                            "<select class='ipt jspksDefault' id='jspk10ProfitModel'>" +
                            "<option value='0' " + jspks.jspks0 + ">开启随机模式</option>" +
                            "<option value='1' " + jspks.jspks1 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + jspks.jspks2 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + jspks.jspks3 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +
                            "<td>" +
                            "<select class='ipt xylhcDefault' id='xylhcProfitModel'>" +
                            "<option value='0' " + xylhc.xylhc0 + ">开启随机模式</option>" +
                            "<option value='1' " + xylhc.xylhc1 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + xylhc.xylhc2 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + xylhc.xylhc3 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +
                            "<td>" +
                            "<select class='ipt sfsscDefault' id='sfsscProfitModel'>" +
                            "<option value='0' " + sfssc.sfssc0 + ">开启随机模式</option>" +
                            "<option value='1' " + sfssc.sfssc1 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + sfssc.sfssc2 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + sfssc.sfssc3 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +
                            "<td>" +
                            "<select class='ipt wfsscDefault' id='wfsscProfitModel'>" +
                            "<option value='0' " + wfssc.wfssc0 + ">开启随机模式</option>" +
                            "<option value='1' " + wfssc.wfssc1 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + wfssc.wfssc2 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + wfssc.wfssc3 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +
                            "<td>" +
                            "<select class='ipt lcqsscDefault' id='lcqsscProfitModel'>" +
                            "<option value='0' " + lcqssc.lcqssc0 + ">开启随机模式</option>" +
                            "<option value='1' " + lcqssc.lcqssc1 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + lcqssc.lcqssc2 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + lcqssc.lcqssc3 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +

                            "<td>" +
                            "<select class='ipt jssscDefault' id='jssscProfitModel'>" +
                            "<option value='0' " + jsssc.jsssc0 + ">开启随机模式</option>" +
                            "<option value='1' " + jsssc.jsssc1 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + jsssc.jsssc2 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + jsssc.jsssc3 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +

                            "<td>"+
                            "<select class='ipt bjsscDefault' id='bjsscProfitModel'>"+
                            "<option value='0' "+bjssc.bjssc0+">开启随机模式</option>"+
                            "<option value='1' "+bjssc.bjssc1+">开启随机盈利模式</option>"+
                            "<option value='2' "+bjssc.bjssc2+">开启绝对盈利模式</option>"+
                            "<option value='3' "+bjssc.bjssc3+">开启嬴率控制模式</option>"+
                            "</select>"+
                            "</td>"+

                            "<td>"+
                            "<select class='ipt gdsscDefault' id='gdsscProfitModel'>"+
                            "<option value='0' "+gdssc.gdssc0+">开启随机模式</option>"+
                            "<option value='1' "+gdssc.gdssc1+">开启随机盈利模式</option>"+
                            "<option value='2' "+gdssc.gdssc2+">开启绝对盈利模式</option>"+
                            "<option value='3' "+gdssc.gdssc3+">开启嬴率控制模式</option>"+
                            "</select>"+
                            "</td>"+

                            "<td>"+
                            "<select class='ipt scsscDefault' id='scsscProfitModel'>"+
                            "<option value='0' "+scssc.scssc0+">开启随机模式</option>"+
                            "<option value='1' "+scssc.scssc1+">开启随机盈利模式</option>"+
                            "<option value='2' "+scssc.scssc2+">开启绝对盈利模式</option>"+
                            "<option value='3' "+scssc.scssc3+">开启嬴率控制模式</option>"+
                            "</select>"+
                            "</td>"+

                            "<td>"+
                            "<select class='ipt shsscDefault' id='shsscProfitModel'>"+
                            "<option value='0' "+shssc.shssc0+">开启随机模式</option>"+
                            "<option value='1' "+shssc.shssc1+">开启随机盈利模式</option>"+
                            "<option value='2' "+shssc.shssc2+">开启绝对盈利模式</option>"+
                            "<option value='3' "+shssc.shssc3+">开启嬴率控制模式</option>"+
                            "</select>"+
                            "</td>"+

                            "<td>"+
                            "<select class='ipt sdsscDefault' id='sdsscProfitModel'>"+
                            "<option value='0' "+sdssc.sdssc0+">开启随机模式</option>"+
                            "<option value='1' "+sdssc.sdssc1+">开启随机盈利模式</option>"+
                            "<option value='2' "+sdssc.sdssc2+">开启绝对盈利模式</option>"+
                            "<option value='3' "+sdssc.sdssc3+">开启嬴率控制模式</option>"+
                            "</select>"+
                            "</td>"+

                            "<td>" +
                            "<select class='ipt sfksDefault' id='sfksProfitModel'>" +
                            "<option value='0' " + sfks.sfks0 + ">开启随机模式</option>" +
                            "<option value='1' " + sfks.sfks1 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + sfks.sfks2 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + sfks.sfks3 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +
                            "<td>" +
                            "<select class='ipt wfksDefault' id='wfksProfitModel'>" +
                            "<option value='0' " + wfks.wfks0 + ">开启随机模式</option>" +
                            "<option value='1' " + wfks.wfks1 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + wfks.wfks2 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + wfks.wfks3 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +
                            "<td>" +
                            "<select class='ipt wfsyxwDefault' id='wfsyxwProfitModel'>" +
                            "<option value='0' " + wfsyxw.wfsyxw0 + ">开启随机模式</option>" +
                            "<option value='1' " + wfsyxw.wfsyxw1 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + wfsyxw.wfsyxw2 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + wfsyxw.wfsyxw3 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +
                            "<td>" +
                            "<select class='ipt sfsyxwDefault' id='sfsyxwProfitModel'>" +
                            "<option value='0' " + sfsyxw.sfsyxw0 + ">开启随机模式</option>" +
                            "<option value='1' " + sfsyxw.sfsyxw1 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + sfsyxw.sfsyxw2 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + sfsyxw.sfsyxw3 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +
                            "</td>" +
                            "<td>" +
                            "<select class='ipt shfsscDefault' id='shfsscProfitModel'>" +
                            "<option value='0' " + shfssc.shfssc0 + ">开启随机模式</option>" +
                            "<option value='1' " + shfssc.shfssc1 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + shfssc.shfssc2 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + shfssc.shfssc3 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +
                            "</td>" +
                            "<td>" +
                            "<select class='ipt sfpk10Default' id='sfpk10ProfitModel'>" +
                            "<option value='0' " + sfpk10.sfpk100 + ">开启随机模式</option>" +
                            "<option value='1' " + sfpk10.sfpk101 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + sfpk10.sfpk102 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + sfpk10.sfpk103 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +
                            "</td>" +
                            "<td>" +
                            "<select class='ipt wfpk10Default' id='wfpk10ProfitModel'>" +
                            "<option value='0' " + wfpk10.wfpk100 + ">开启随机模式</option>" +
                            "<option value='1' " + wfpk10.wfpk101 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + wfpk10.wfpk102 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + wfpk10.wfpk103 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +
                            "</td>" +
                            "<td>" +
                            "<select class='ipt shfpk10Default' id='shfpk10ProfitModel'>" +
                            "<option value='0' " + shfpk10.shfpk100 + ">开启随机模式</option>" +
                            "<option value='1' " + shfpk10.shfpk101 + ">开启随机盈利模式</option>" +
                            "<option value='2' " + shfpk10.shfpk102 + ">开启绝对盈利模式</option>" +
                            "<option value='3' " + shfpk10.shfpk103 + ">开启嬴率控制模式</option>" +
                            "</select>" +
                            "</td>" +
                            "</td>" +
                            "</tr>";
                        $("#lottery1").append(str1);

                        $(".jyksDefault").after(jyksDefaultInputBoxStr);
                        $(".jlffcDefault").after(jlffcDefaultInputBoxStr);
                        $(".jspksDefault").after(jspksDefaultInputBoxStr);
                        $(".xylhcDefault").after(xylhcDefaultInputBoxStr);
                        $(".sfsscDefault").after(sfsscDefaultInputBoxStr);
                        $(".shfsscDefault").after(shfsscDefaultInputBoxStr);
                        $(".wfsscDefault").after(wfsscDefaultInputBoxStr);
                        $(".lcqsscDefault").after(lcqsscDefaultInputBoxStr);
                        $(".jssscDefault").after(jssscDefaultInputBoxStr);
                        $(".bjsscDefault").after(bjsscDefaultInputBoxStr);
                        $(".gdsscDefault").after(gdsscDefaultInputBoxStr);
                        $(".scsscDefault").after(scsscDefaultInputBoxStr);
                        $(".shsscDefault").after(shsscDefaultInputBoxStr);
                        $(".sdsscDefault").after(sdsscDefaultInputBoxStr);
                        $(".sfksDefault").after(sfksDefaultInputBoxStr);
                        $(".wfksDefault").after(wfksDefaultInputBoxStr);
                        $(".wfsyxwDefault").after(wfsyxwDefaultInputBoxStr);
                        $(".sfsyxwDefault").after(sfsyxwDefaultInputBoxStr);
                        $(".sfpk10Default").after(sfpk10DefaultInputBoxStr);
                        $(".wfpk10Default").after(wfpk10DefaultInputBoxStr);
                        $(".shfpk10Default").after(shfpk10DefaultInputBoxStr);

                        $("input[name='jyksDefaultMaximumWinRate']").val(idx.jyksLargestWinGain);
                        $("input[name='jyksDefaultMinimumWinRate']").val(idx.jyksLowestWinGain);
                        $("input[name='jlffcDefaultMaximumWinRate']").val(idx.jlffcLargestWinGain);
                        $("input[name='jlffcDefaultMinimumWinRate']").val(idx.jlffcLowestWinGain);
                        $("input[name='jspksDefaultMaximumWinRate']").val(idx.jspk10LargestWinGain);
                        $("input[name='jspksDefaultMinimumWinRate']").val(idx.jspk10LowestWinGain);
                        $("input[name='xylhcDefaultMaximumWinRate']").val(idx.xylhcLargestWinGain);
                        $("input[name='xylhcDefaultMinimumWinRate']").val(idx.xylhcLowestWinGain);
                        $("input[name='sfsscDefaultMaximumWinRate']").val(idx.sfsscLargestWinGain);
                        $("input[name='sfsscDefaultMinimumWinRate']").val(idx.sfsscLowestWinGain);
                        $("input[name='shfsscDefaultMaximumWinRate']").val(idx.shfsscLargestWinGain);
                        $("input[name='shfsscDefaultMinimumWinRate']").val(idx.shfsscLowestWinGain);
                        $("input[name='wfsscDefaultMaximumWinRate']").val(idx.wfsscLargestWinGain);
                        $("input[name='wfsscDefaultMinimumWinRate']").val(idx.wfsscLowestWinGain);
                        $("input[name='lcqsscDefaultMaximumWinRate']").val(idx.lcqsscLargestWinGain);
                        $("input[name='lcqsscDefaultMinimumWinRate']").val(idx.lcqsscLowestWinGain);
                        $("input[name='jssscDefaultMaximumWinRate']").val(idx.jssscLargestWinGain);
                        $("input[name='jssscDefaultMinimumWinRate']").val(idx.jssscLowestWinGain);
                        $("input[name='bjsscDefaultMaximumWinRate']").val(idx.bjsscLargestWinGain);
                        $("input[name='bjsscDefaultMinimumWinRate']").val(idx.bjsscLowestWinGain);
                        $("input[name='gdsscDefaultMaximumWinRate']").val(idx.gdsscLargestWinGain);
                        $("input[name='gdsscDefaultMinimumWinRate']").val(idx.gdsscLowestWinGain);
                        $("input[name='scsscDefaultMaximumWinRate']").val(idx.scsscLargestWinGain);
                        $("input[name='scsscDefaultMinimumWinRate']").val(idx.scsscLowestWinGain);
                        $("input[name='shsscDefaultMaximumWinRate']").val(idx.shsscLargestWinGain);
                        $("input[name='shsscDefaultMinimumWinRate']").val(idx.shsscLowestWinGain);
                        $("input[name='sdsscDefaultMaximumWinRate']").val(idx.sdsscLargestWinGain);
                        $("input[name='sdsscDefaultMinimumWinRate']").val(idx.sdsscLowestWinGain);
                        $("input[name='sfksDefaultMaximumWinRate']").val(idx.sfksLargestWinGain);
                        $("input[name='sfksDefaultMinimumWinRate']").val(idx.sfksLowestWinGain);
                        $("input[name='wfksDefaultMaximumWinRate']").val(idx.wfksLargestWinGain);
                        $("input[name='wfksDefaultMinimumWinRate']").val(idx.wfksLowestWinGain);
                        $("input[name='wfsyxwDefaultMaximumWinRate']").val(idx.wfsyxwLargestWinGain);
                        $("input[name='wfsyxwDefaultMinimumWinRate']").val(idx.wfsyxwLowestWinGain);
                        $("input[name='sfsyxwDefaultMaximumWinRate']").val(idx.sfsyxwLargestWinGain);
                        $("input[name='sfsyxwDefaultMinimumWinRate']").val(idx.sfsyxwLowestWinGain);
                        $("input[name='sfpk10DefaultMaximumWinRate']").val(idx.sfpk10LargestWinGain);
                        $("input[name='sfpk10DefaultMinimumWinRate']").val(idx.sfpk10LowestWinGain);
                        $("input[name='wfpk10DefaultMaximumWinRate']").val(idx.wfpk10LargestWinGain);
                        $("input[name='wfpk10DefaultMinimumWinRate']").val(idx.wfpk10LowestWinGain);
                        $("input[name='shfpk10DefaultMaximumWinRate']").val(idx.shfpk10LargestWinGain);
                        $("input[name='shfpk10DefaultMinimumWinRate']").val(idx.shfpk10LowestWinGain);

                        //最高赢率暂时未用
                        $("input[name='jyksDefaultMaximumWinRate']").parent().hide();
                        $("input[name='jlffcDefaultMaximumWinRate']").parent().hide();
                        $("input[name='jspksDefaultMaximumWinRate']").parent().hide();
                        $("input[name='xylhcDefaultMaximumWinRate']").parent().hide();
                        $("input[name='sfsscDefaultMaximumWinRate']").parent().hide();
                        $("input[name='shfsscDefaultMaximumWinRate']").parent().hide();
                        $("input[name='wfsscDefaultMaximumWinRate']").parent().hide();
                        $("input[name='lcqsscDefaultMaximumWinRate']").parent().hide();
                        $("input[name='jssscDefaultMaximumWinRate']").parent().hide();
                        $("input[name='bjsscDefaultMaximumWinRate']").parent().hide();
                        $("input[name='gdsscDefaultMaximumWinRate']").parent().hide();
                        $("input[name='scsscDefaultMaximumWinRate']").parent().hide();
                        $("input[name='shsscDefaultMaximumWinRate']").parent().hide();
                        $("input[name='sdsscDefaultMaximumWinRate']").parent().hide();
                        $("input[name='sfksDefaultMaximumWinRate']").parent().hide();
                        $("input[name='wfksDefaultMaximumWinRate']").parent().hide();
                        $("input[name='wfsyxwDefaultMaximumWinRate']").parent().hide();
                        $("input[name='sfsyxwDefaultMaximumWinRate']").parent().hide();
                        $("input[name='sfpk10DefaultMaximumWinRate']").parent().hide();
                        $("input[name='wfpk10DefaultMaximumWinRate']").parent().hide();
                        $("input[name='shfpk10DefaultMaximumWinRate']").parent().hide();
                    }


                    if (idx.sfpk10ProfitModel == 3) {
                        $(".sfpk10Default").show();
                    }
                    if (idx.wfpk10ProfitModel == 3) {
                        $(".wfpk10Default").show();
                    }
                    if (idx.shfpk10ProfitModel == 3) {
                        $(".shfpk10Default").show();
                    }
                    if (idx.sfksProfitModel == 3) {
                        $(".sfksDefault").show();
                    }
                    if (idx.wfksProfitModel == 3) {
                        $(".wfksDefault").show();
                    }
                    if (idx.sfsscProfitModel == 3) {
                        $(".sfsscDefault").show();
                    }
                    if (idx.shfsscProfitModel == 3) {
                        $(".shfsscDefault").show();
                    }
                    if (idx.wfsscProfitModel == 3) {
                        $(".wfsscDefault").show();
                    }
                    if (idx.lcqsscProfitModel == 3) {
                        $(".lcqsscDefault").show();
                    }
                    if (idx.jssscProfitModel == 3) {
                        $(".jssscDefault").show();
                    }
                    if (idx.bjsscProfitModel == 3) {
                        $(".bjsscDefault").show();
                    }
                    if (idx.gdsscProfitModel == 3) {
                        $(".gdsscDefault").show();
                    }
                    if (idx.scsscProfitModel == 3) {
                        $(".scsscDefault").show();
                    }
                    if (idx.shsscProfitModel == 3) {
                        $(".shsscDefault").show();
                    }
                    if (idx.sdsscProfitModel == 3) {
                        $(".sdsscDefault").show();
                    }
                    if (idx.wfsyxwProfitModel == 3) {
                        $(".wfsyxwDefault").show();
                    }
                    if (idx.sfsyxwProfitModel == 3) {
                        $(".sfsyxwDefault").show();
                    }
                    if (idx.jyksProfitModel == 3) {
                        $(".jyksDefault").show();
                    }
                    if (idx.jlffcProfitModel == 3) {
                        $(".jlffcDefault").show();
                    }
                    if (idx.jspk10ProfitModel == 3) {
                        $(".jspksDefault").show();
                    }

                    if (idx.xylhcProfitModel == 3) {
                        $(".xylhcDefault").show();
                    }
                    $(".sfpk10Default").change(function () {
                        var val = $(".sfpk10Default").find("option:selected").val();
                        if (val != 3) {
                            $(".sfpk10DefaultBoxStr").hide();
                        } else {
                            $(".sfpk10DefaultBoxStr").show();
                        }
                    })

                    $(".wfpk10Default").change(function () {
                        var val = $(".wfpk10Default").find("option:selected").val();
                        if (val != 3) {
                            $(".wfpk10DefaultBoxStr").hide();
                        } else {
                            $(".wfpk10DefaultBoxStr").show();
                        }
                    })

                    $(".shfpk10Default").change(function () {
                        var val = $(".shfpk10Default").find("option:selected").val();
                        if (val != 3) {
                            $(".shfpk10DefaultBoxStr").hide();
                        } else {
                            $(".shfpk10DefaultBoxStr").show();
                        }
                    })

                    $(".sfksDefault").change(function () {
                        var val = $(".sfksDefault").find("option:selected").val();
                        if (val != 3) {
                            $(".sfksDefaultBoxStr").hide();
                        } else {
                            $(".sfksDefaultBoxStr").show();
                        }
                    })

                    $(".wfksDefault").change(function () {
                        var val = $(".wfksDefault").find("option:selected").val();
                        if (val != 3) {
                            $(".wfksDefaultBoxStr").hide();
                        } else {
                            $(".wfksDefaultBoxStr").show();
                        }
                    })

                    $(".sfsscDefault").change(function () {
                        var val = $(".sfsscDefault").find("option:selected").val();
                        if (val != 3) {
                            $(".sfsscDefaultBoxStr").hide();
                        } else {
                            $(".sfsscDefaultBoxStr").show();
                        }
                    })

                    $(".shfsscDefault").change(function () {
                        var val = $(".shfsscDefault").find("option:selected").val();
                        if (val != 3) {
                            $(".shfsscDefaultBoxStr").hide();
                        } else {
                            $(".shfsscDefaultBoxStr").show();
                        }
                    })

                    $(".wfsscDefault").change(function () {
                        var val = $(".wfsscDefault").find("option:selected").val();
                        if (val != 3) {
                            $(".wfsscDefaultBoxStr").hide();
                        } else {
                            $(".wfsscDefaultBoxStr").show();
                        }
                    })

                    $(".lcqsscDefault").change(function () {
                        var val = $(".lcqsscDefault").find("option:selected").val();
                        if (val != 3) {
                            $(".lcqsscDefaultBoxStr").hide();
                        } else {
                            $(".lcqsscDefaultBoxStr").show();
                        }
                    })
                    $(".jssscDefault").change(function () {
                        var val = $(".jssscDefault").find("option:selected").val();
                        if (val != 3) {
                            $(".jssscDefaultBoxStr").hide();
                        } else {
                            $(".jssscDefaultBoxStr").show();
                        }
                    })
                    $(".bjsscDefault").change(function () {
                        var val = $(".bjsscDefault").find("option:selected").val();
                        if (val != 3) {
                            $(".bjsscDefaultBoxStr").hide();
                        } else {
                            $(".bjsscDefaultBoxStr").show();
                        }
                    })
                    $(".gdsscDefault").change(function () {
                        var val = $(".gdsscDefault").find("option:selected").val();
                        if (val != 3) {
                            $(".gdsscDefaultBoxStr").hide();
                        } else {
                            $(".gdsscDefaultBoxStr").show();
                        }
                    })
                    $(".scsscDefault").change(function () {
                        var val = $(".scsscDefault").find("option:selected").val();
                        if (val != 3) {
                            $(".scsscDefaultBoxStr").hide();
                        } else {
                            $(".scsscDefaultBoxStr").show();
                        }
                    })
                    $(".shsscDefault").change(function () {
                        var val = $(".shsscDefault").find("option:selected").val();
                        if (val != 3) {
                            $(".shsscDefaultBoxStr").hide();
                        } else {
                            $(".shsscDefaultBoxStr").show();
                        }
                    })
                    $(".sdsscDefault").change(function () {
                        var val = $(".sdsscDefault").find("option:selected").val();
                        if (val != 3) {
                            $(".sdsscDefaultBoxStr").hide();
                        } else {
                            $(".sdsscDefaultBoxStr").show();
                        }
                    })

                    $(".wfsyxwDefault").change(function () {
                        var val = $(".wfsyxwDefault").find("option:selected").val();
                        if (val != 3) {
                            $(".wfsyxwDefaultBoxStr").hide();
                        } else {
                            $(".wfsyxwDefaultBoxStr").show();
                        }
                    })

                    $(".sfsyxwDefault").change(function () {
                        var val = $(".sfsyxwDefault").find("option:selected").val();
                        if (val != 3) {
                            $(".sfsyxwDefaultBoxStr").hide();
                        } else {
                            $(".sfsyxwDefaultBoxStr").show();
                        }
                    })

                    $(".jyksDefault").change(function () {
                        var val = $(".jyksDefault").find("option:selected").val();
                        if (val != 3) {
                            $(".jyksDefaultBoxStr").hide();
                        } else {
                            $(".jyksDefaultBoxStr").show();
                        }
                    })

                    $(".jlffcDefault").change(function () {
                        var val = $(".jlffcDefault").find("option:selected").val();
                        if (val != 3) {
                            $(".jlffcDefaultBoxStr").hide();
                        } else {
                            $(".jlffcDefaultBoxStr").show();
                        }
                    })

                    $(".jspksDefault").change(function () {
                        var val = $(".jspksDefault").find("option:selected").val();
                        if (val != 3) {
                            $(".jspksDefaultBoxStr").hide();
                        } else {
                            $(".jspksDefaultBoxStr").show();
                        }
                    })
                    $(".xylhcDefault").change(function () {
                        var val = $(".xylhcDefault").find("option:selected").val();
                        if (val != 3) {
                            $(".xylhcDefaultBoxStr").hide();
                        } else {
                            $(".xylhcDefaultBoxStr").show();
                        }
                    })

                    $("." + idx.bizSystem + "Sfpk10Select").after(sfpk10InputBoxStr);
                    $("." + idx.bizSystem + "Wfpk10Select").after(wfpk10InputBoxStr);
                    $("." + idx.bizSystem + "Shfpk10Select").after(shfpk10InputBoxStr);
                    $("." + idx.bizSystem + "SfksSelect").after(sfksInputBoxStr);
                    $("." + idx.bizSystem + "WfksSelect").after(wfksInputBoxStr);
                    $("." + idx.bizSystem + "SfsscSelect").after(sfsscInputBoxStr);
                    $("." + idx.bizSystem + "ShfsscSelect").after(shfsscInputBoxStr);
                    $("." + idx.bizSystem + "WfsscSelect").after(wfsscInputBoxStr);
                    $("." + idx.bizSystem + "LcqsscSelect").after(lcqsscInputBoxStr);
                    $("." + idx.bizSystem + "JssscSelect").after(jssscInputBoxStr);
                    $("." + idx.bizSystem + "BjsscSelect").after(bjsscInputBoxStr);
                    $("." + idx.bizSystem + "GdsscSelect").after(gdsscInputBoxStr);
                    $("." + idx.bizSystem + "ScsscSelect").after(scsscInputBoxStr);
                    $("." + idx.bizSystem + "ShsscSelect").after(shsscInputBoxStr);
                    $("." + idx.bizSystem + "SdsscSelect").after(sdsscInputBoxStr);
                    $("." + idx.bizSystem + "WfsyxwSelect").after(wfsyxwInputBoxStr);
                    $("." + idx.bizSystem + "SfsyxwSelect").after(sfsyxwInputBoxStr);
                    $("." + idx.bizSystem + "JyksSelect").after(jyksInputBoxStr);
                    $("." + idx.bizSystem + "JlffcSelect").after(jlffcInputBoxStr);
                    $("." + idx.bizSystem + "JspksSelect").after(jspksInputBoxStr);
                    $("." + idx.bizSystem + "XylhcSelect").after(xylhcInputBoxStr);
                    $("input[name='" + idx.bizSystem + "JyksMaximumWinRate']").val(idx.jyksLargestWinGain);
                    $("input[name='" + idx.bizSystem + "JyksMinimumWinRate']").val(idx.jyksLowestWinGain);
                    $("input[name='" + idx.bizSystem + "JlffcMaximumWinRate']").val(idx.jlffcLargestWinGain);
                    $("input[name='" + idx.bizSystem + "JlffcMinimumWinRate']").val(idx.jlffcLowestWinGain);
                    $("input[name='" + idx.bizSystem + "JspksMaximumWinRate']").val(idx.jspk10LargestWinGain);
                    $("input[name='" + idx.bizSystem + "JspksMinimumWinRate']").val(idx.jspk10LowestWinGain);
                    $("input[name='" + idx.bizSystem + "XylhcMaximumWinRate']").val(idx.xylhcLargestWinGain);
                    $("input[name='" + idx.bizSystem + "XylhcMinimumWinRate']").val(idx.xylhcLowestWinGain);
                    $("input[name='" + idx.bizSystem + "SfsscMaximumWinRate']").val(idx.sfsscLargestWinGain);
                    $("input[name='" + idx.bizSystem + "SfsscMinimumWinRate']").val(idx.sfsscLowestWinGain);
                    $("input[name='" + idx.bizSystem + "ShfsscMaximumWinRate']").val(idx.shfsscLargestWinGain);
                    $("input[name='" + idx.bizSystem + "ShfsscMinimumWinRate']").val(idx.shfsscLowestWinGain);
                    $("input[name='" + idx.bizSystem + "WfsscMaximumWinRate']").val(idx.wfsscLargestWinGain);
                    $("input[name='" + idx.bizSystem + "WfsscMinimumWinRate']").val(idx.wfsscLowestWinGain);
                    $("input[name='" + idx.bizSystem + "LcqsscMaximumWinRate']").val(idx.lcqsscLargestWinGain);
                    $("input[name='" + idx.bizSystem + "LcqsscMinimumWinRate']").val(idx.lcqsscLowestWinGain);

                    $("input[name='" + idx.bizSystem + "JssscMaximumWinRate']").val(idx.jssscLargestWinGain);
                    $("input[name='" + idx.bizSystem + "JssscMinimumWinRate']").val(idx.jssscLowestWinGain);
                    $("input[name='" + idx.bizSystem + "BjsscMaximumWinRate']").val(idx.bjsscLargestWinGain);
                    $("input[name='" + idx.bizSystem + "BjsscMinimumWinRate']").val(idx.bjsscLowestWinGain);
                    $("input[name='" + idx.bizSystem + "GdsscMaximumWinRate']").val(idx.gdsscLargestWinGain);
                    $("input[name='" + idx.bizSystem + "GdsscMinimumWinRate']").val(idx.gdsscLowestWinGain);
                    $("input[name='" + idx.bizSystem + "ScsscMaximumWinRate']").val(idx.scsscLargestWinGain);
                    $("input[name='" + idx.bizSystem + "ScsscMinimumWinRate']").val(idx.scsscLowestWinGain);
                    $("input[name='" + idx.bizSystem + "ShsscMaximumWinRate']").val(idx.shsscLargestWinGain);
                    $("input[name='" + idx.bizSystem + "ShsscMinimumWinRate']").val(idx.shsscLowestWinGain);
                    $("input[name='" + idx.bizSystem + "SdsscMaximumWinRate']").val(idx.sdsscLargestWinGain);
                    $("input[name='" + idx.bizSystem + "SdsscMinimumWinRate']").val(idx.sdsscLowestWinGain);

                    $("input[name='" + idx.bizSystem + "SfksMaximumWinRate']").val(idx.sfksLargestWinGain);
                    $("input[name='" + idx.bizSystem + "SfksMinimumWinRate']").val(idx.sfksLowestWinGain);
                    $("input[name='" + idx.bizSystem + "WfksMaximumWinRate']").val(idx.wfksLargestWinGain);
                    $("input[name='" + idx.bizSystem + "WfksMinimumWinRate']").val(idx.wfksLowestWinGain);
                    $("input[name='" + idx.bizSystem + "WfsyxwMaximumWinRate']").val(idx.wfsyxwLargestWinGain);
                    $("input[name='" + idx.bizSystem + "WfsyxwMinimumWinRate']").val(idx.wfsyxwLowestWinGain);
                    $("input[name='" + idx.bizSystem + "SfsyxwMaximumWinRate']").val(idx.sfsyxwLargestWinGain);
                    $("input[name='" + idx.bizSystem + "SfsyxwMinimumWinRate']").val(idx.sfsyxwLowestWinGain);
                    $("input[name='" + idx.bizSystem + "Sfpk10MaximumWinRate']").val(idx.sfpk10LargestWinGain);
                    $("input[name='" + idx.bizSystem + "Sfpk10MinimumWinRate']").val(idx.sfpk10LowestWinGain);
                    $("input[name='" + idx.bizSystem + "Wfpk10MaximumWinRate']").val(idx.wfpk10LargestWinGain);
                    $("input[name='" + idx.bizSystem + "Wfpk10MinimumWinRate']").val(idx.wfpk10LowestWinGain);
                    $("input[name='" + idx.bizSystem + "Shfpk10MaximumWinRate']").val(idx.shfpk10LargestWinGain);
                    $("input[name='" + idx.bizSystem + "Shfpk10MinimumWinRate']").val(idx.shfpk10LowestWinGain);

                    //最高赢率暂时未用
                    $("input[name='" + idx.bizSystem + "JyksMaximumWinRate']").parent().hide();
                    $("input[name='" + idx.bizSystem + "JlffcMaximumWinRate']").parent().hide();
                    $("input[name='" + idx.bizSystem + "JspksMaximumWinRate']").parent().hide();
                    $("input[name='" + idx.bizSystem + "XylhcMaximumWinRate']").parent().hide();
                    $("input[name='" + idx.bizSystem + "SfsscMaximumWinRate']").parent().hide();
                    $("input[name='" + idx.bizSystem + "ShfsscMaximumWinRate']").parent().hide();
                    $("input[name='" + idx.bizSystem + "WfsscMaximumWinRate']").parent().hide();
                    $("input[name='" + idx.bizSystem + "LcqsscMaximumWinRate']").parent().hide();
                    $("input[name='" + idx.bizSystem + "JssscMaximumWinRate']").parent().hide();
                    $("input[name='" + idx.bizSystem + "BjsscMaximumWinRate']").parent().hide();
                    $("input[name='" + idx.bizSystem + "GdsscMaximumWinRate']").parent().hide();
                    $("input[name='" + idx.bizSystem + "ScsscMaximumWinRate']").parent().hide();
                    $("input[name='" + idx.bizSystem + "ShsscMaximumWinRate']").parent().hide();
                    $("input[name='" + idx.bizSystem + "SdsscMaximumWinRate']").parent().hide();
                    $("input[name='" + idx.bizSystem + "SfksMaximumWinRate']").parent().hide();
                    $("input[name='" + idx.bizSystem + "WfksMaximumWinRate']").parent().hide();
                    $("input[name='" + idx.bizSystem + "WfsyxwMaximumWinRate']").parent().hide();
                    $("input[name='" + idx.bizSystem + "SfsyxwMaximumWinRate']").parent().hide();
                    $("input[name='" + idx.bizSystem + "Sfpk10wMaximumWinRate']").parent().hide();
                    $("input[name='" + idx.bizSystem + "Wfpk10MaximumWinRate']").parent().hide();
                    $("input[name='" + idx.bizSystem + "Shfpk10MaximumWinRate']").parent().hide();


                    if (idx.sfpk10ProfitModel == 3) {
                        $("." + idx.bizSystem + "sfpk10").show();
                    }
                    if (idx.wfpk10ProfitModel == 3) {
                        $("." + idx.bizSystem + "wfpk10").show();
                    }
                    if (idx.shfpk10ProfitModel == 3) {
                        $("." + idx.bizSystem + "shfpk10").show();
                    }
                    if (idx.sfksProfitModel == 3) {
                        $("." + idx.bizSystem + "sfks").show();
                    }
                    if (idx.wfksProfitModel == 3) {
                        $("." + idx.bizSystem + "wfks").show();
                    }
                    if (idx.sfsscProfitModel == 3) {
                        $("." + idx.bizSystem + "sfssc").show();
                    }
                    if (idx.shfsscProfitModel == 3) {
                        $("." + idx.bizSystem + "shfssc").show();
                    }
                    if (idx.wfsscProfitModel == 3) {
                        $("." + idx.bizSystem + "wfssc").show();
                    }
                    if (idx.lcqsscProfitModel == 3) {
                        $("." + idx.bizSystem + "lcqssc").show();
                    }
                    if (idx.jssscProfitModel == 3) {
                        $("." + idx.bizSystem + "jsssc").show();
                    }
                    if (idx.bjsscProfitModel == 3) {
                        $("." + idx.bizSystem + "bjssc").show();
                    }
                    if (idx.gdsscProfitModel == 3) {
                        $("." + idx.bizSystem + "gdssc").show();
                    }
                    if (idx.scsscProfitModel == 3) {
                        $("." + idx.bizSystem + "scssc").show();
                    }
                    if (idx.shsscProfitModel == 3) {
                        $("." + idx.bizSystem + "shssc").show();
                    }
                    if (idx.sdsscProfitModel == 3) {
                        $("." + idx.bizSystem + "sdssc").show();
                    }
                    if (idx.wfsyxwProfitModel == 3) {
                        $("." + idx.bizSystem + "wfsyxw").show();
                    }
                    if (idx.sfsyxwProfitModel == 3) {
                        $("." + idx.bizSystem + "sfsyxw").show();
                    }
                    if (idx.jyksProfitModel == 3) {
                        $("." + idx.bizSystem + "jyks").show();
                    }
                    if (idx.jlffcProfitModel == 3) {
                        $("." + idx.bizSystem + "jlffc").show();
                    }
                    if (idx.jspk10ProfitModel == 3) {
                        $("." + idx.bizSystem + "jspks").show();
                    }
                    if (idx.xylhcProfitModel == 3) {
                        $("." + idx.bizSystem + "xylhc").show();
                    }

                    $("." + idx.bizSystem + "SfksSelect").change(function () {
                        var val = $("." + idx.bizSystem + "SfksSelect").find("option:selected").val();
                        if (val != 3) {
                            $("." + idx.bizSystem + "sfks").hide();
                        } else {
                            $("." + idx.bizSystem + "sfks").show();
                        }
                    })
                    $("." + idx.bizSystem + "WfksSelect").change(function () {
                        var val = $("." + idx.bizSystem + "WfksSelect").find("option:selected").val();
                        if (val != 3) {
                            $("." + idx.bizSystem + "wfks").hide();
                        } else {
                            $("." + idx.bizSystem + "wfks").show();
                        }
                    })
                    $("." + idx.bizSystem + "ShfsscSelect").change(function () {
                        var val = $("." + idx.bizSystem + "ShfsscSelect").find("option:selected").val();
                        if (val != 3) {
                            $("." + idx.bizSystem + "shfssc").hide();
                        } else {
                            $("." + idx.bizSystem + "shfssc").show();
                        }
                    })
                    $("." + idx.bizSystem + "WfsscSelect").change(function () {
                        var val = $("." + idx.bizSystem + "WfsscSelect").find("option:selected").val();
                        if (val != 3) {
                            $("." + idx.bizSystem + "wfssc").hide();
                        } else {
                            $("." + idx.bizSystem + "wfssc").show();
                        }
                    })
                    $("." + idx.bizSystem + "LcqsscSelect").change(function () {
                        var val = $("." + idx.bizSystem + "LcqsscSelect").find("option:selected").val();
                        if (val != 3) {
                            $("." + idx.bizSystem + "lcqssc").hide();
                        } else {
                            $("." + idx.bizSystem + "lcqssc").show();
                        }
                    })
                    $("." + idx.bizSystem + "JssscSelect").change(function () {
                        var val = $("." + idx.bizSystem + "JssscSelect").find("option:selected").val();
                        if (val != 3) {
                            $("." + idx.bizSystem + "jsssc").hide();
                        } else {
                            $("." + idx.bizSystem + "jsssc").show();
                        }
                    })
                    $("." + idx.bizSystem + "BjsscSelect").change(function () {
                        var val = $("." + idx.bizSystem + "BjsscSelect").find("option:selected").val();
                        if (val != 3) {
                            $("." + idx.bizSystem + "bjssc").hide();
                        } else {
                            $("." + idx.bizSystem + "bjssc").show();
                        }
                    })
                    $("." + idx.bizSystem + "GdsscSelect").change(function () {
                        var val = $("." + idx.bizSystem + "GdsscSelect").find("option:selected").val();
                        if (val != 3) {
                            $("." + idx.bizSystem + "gdssc").hide();
                        } else {
                            $("." + idx.bizSystem + "gdssc").show();
                        }
                    })
                    $("." + idx.bizSystem + "ScsscSelect").change(function () {
                        var val = $("." + idx.bizSystem + "ScsscSelect").find("option:selected").val();
                        if (val != 3) {
                            $("." + idx.bizSystem + "scssc").hide();
                        } else {
                            $("." + idx.bizSystem + "scssc").show();
                        }
                    })
                    $("." + idx.bizSystem + "ShsscSelect").change(function () {
                        var val = $("." + idx.bizSystem + "ShsscSelect").find("option:selected").val();
                        if (val != 3) {
                            $("." + idx.bizSystem + "shssc").hide();
                        } else {
                            $("." + idx.bizSystem + "shssc").show();
                        }
                    })
                    $("." + idx.bizSystem + "SdsscSelect").change(function () {
                        var val = $("." + idx.bizSystem + "SdsscSelect").find("option:selected").val();
                        if (val != 3) {
                            $("." + idx.bizSystem + "sdssc").hide();
                        } else {
                            $("." + idx.bizSystem + "sdssc").show();
                        }
                    })
                    $("." + idx.bizSystem + "WfsyxwSelect").change(function () {
                        var val = $("." + idx.bizSystem + "WfsyxwSelect").find("option:selected").val();
                        if (val != 3) {
                            $("." + idx.bizSystem + "wfsyxw").hide();
                        } else {
                            $("." + idx.bizSystem + "wfsyxw").show();
                        }
                    })
                    $("." + idx.bizSystem + "SfsyxwSelect").change(function () {
                        var val = $("." + idx.bizSystem + "SfsyxwSelect").find("option:selected").val();
                        if (val != 3) {
                            $("." + idx.bizSystem + "sfsyxw").hide();
                        } else {
                            $("." + idx.bizSystem + "sfsyxw").show();
                        }
                    })
                    $("." + idx.bizSystem + "JyksSelect").change(function () {
                        var val = $("." + idx.bizSystem + "JyksSelect").find("option:selected").val();
                        if (val != 3) {
                            $("." + idx.bizSystem + "jyks").hide();
                        } else {
                            $("." + idx.bizSystem + "jyks").show();
                        }
                    })
                    $("." + idx.bizSystem + "JlffcSelect").change(function () {
                        var val = $("." + idx.bizSystem + "JlffcSelect").find("option:selected").val();
                        if (val != 3) {
                            $("." + idx.bizSystem + "jlffc").hide();
                        } else {
                            $("." + idx.bizSystem + "jlffc").show();
                        }
                    })
                    $("." + idx.bizSystem + "JspksSelect").change(function () {
                        var val = $("." + idx.bizSystem + "JspksSelect").find("option:selected").val();
                        if (val != 3) {
                            $("." + idx.bizSystem + "jspks").hide();
                        } else {
                            $("." + idx.bizSystem + "jspks").show();
                        }
                    })
                    $("." + idx.bizSystem + "XylhcSelect").change(function () {
                        var val = $("." + idx.bizSystem + "XylhcSelect").find("option:selected").val();
                        if (val != 3) {
                            $("." + idx.bizSystem + "xylhc").hide();
                        } else {
                            $("." + idx.bizSystem + "xylhc").show();
                        }
                    })
                    $("." + idx.bizSystem + "SfsscSelect").change(function () {
                        var val = $("." + idx.bizSystem + "SfsscSelect").find("option:selected").val();
                        if (val != 3) {
                            $("." + idx.bizSystem + "sfssc").hide();
                        } else {
                            $("." + idx.bizSystem + "sfssc").show();
                        }
                    })

                    $("." + idx.bizSystem + "Sfpk10Select").change(function () {
                        var val = $("." + idx.bizSystem + "Sfpk10Select").find("option:selected").val();
                        if (val != 3) {
                            $("." + idx.bizSystem + "sfpk10").hide();
                        } else {
                            $("." + idx.bizSystem + "sfpk10").show();
                        }
                    })
                    $("." + idx.bizSystem + "Wfpk10Select").change(function () {
                        var val = $("." + idx.bizSystem + "Wfpk10Select").find("option:selected").val();
                        if (val != 3) {
                            $("." + idx.bizSystem + "wfpk10").hide();
                        } else {
                            $("." + idx.bizSystem + "wfpk10").show();
                        }
                    })
                    $("." + idx.bizSystem + "Shfpk10Select").change(function () {
                        var val = $("." + idx.bizSystem + "Shfpk10Select").find("option:selected").val();
                        if (val != 3) {
                            $("." + idx.bizSystem + "shfpk10").hide();
                        } else {
                            $("." + idx.bizSystem + "shfpk10").show();
                        }
                    })
                });
            }
        }
    });
}

/**
 * 保存设置
 */
LotteryControlConfiPage.prototype.setLotteryControlConfigStatus = function () {
    var list = [];
    $($("#lottery1").find("tr")).each(function (i, idx) {
        var json = new Object;
        json.id = $(idx).attr("id");
        json.bizSystem = $(idx).attr("data-value");
        json.jyksProfitModel = $(idx).find("select option:selected").eq(0).val();
        json.jlffcProfitModel = $(idx).find("select option:selected").eq(1).val();
        json.jspk10ProfitModel = $(idx).find("select option:selected").eq(2).val();
        json.xylhcProfitModel = $(idx).find("select option:selected").eq(3).val();
        json.sfsscProfitModel = $(idx).find("select option:selected").eq(4).val();
        json.wfsscProfitModel = $(idx).find("select option:selected").eq(5).val();
        json.lcqsscProfitModel = $(idx).find("select option:selected").eq(6).val();
        json.jssscProfitModel = $(idx).find("select option:selected").eq(7).val();
        json.bjsscProfitModel = $(idx).find("select option:selected").eq(8).val();
        json.gdsscProfitModel = $(idx).find("select option:selected").eq(9).val();
        json.scsscProfitModel = $(idx).find("select option:selected").eq(10).val();
        json.shsscProfitModel = $(idx).find("select option:selected").eq(11).val();
        json.sdsscProfitModel = $(idx).find("select option:selected").eq(12).val();
        json.sfksProfitModel = $(idx).find("select option:selected").eq(13).val();
        json.wfksProfitModel = $(idx).find("select option:selected").eq(14).val();
        json.wfsyxwProfitModel = $(idx).find("select option:selected").eq(15).val();
        json.sfsyxwProfitModel = $(idx).find("select option:selected").eq(16).val();
        json.shfsscProfitModel = $(idx).find("select option:selected").eq(17).val();
        json.sfpk10ProfitModel = $(idx).find("select option:selected").eq(18).val();
        json.wfpk10ProfitModel = $(idx).find("select option:selected").eq(19).val();
        json.shfpk10ProfitModel = $(idx).find("select option:selected").eq(20).val();

        json.jyksLargestWinGain = $("input[name='" + json.bizSystem + "JyksMaximumWinRate']").val();
        json.jyksLowestWinGain = $("input[name='" + json.bizSystem + "JyksMinimumWinRate']").val();
        json.jlffcLargestWinGain = $("input[name='" + json.bizSystem + "JlffcMaximumWinRate']").val();
        json.jlffcLowestWinGain = $("input[name='" + json.bizSystem + "JlffcMinimumWinRate']").val();
        json.jspk10LargestWinGain = $("input[name='" + json.bizSystem + "JspksMaximumWinRate']").val();
        json.jspk10LowestWinGain = $("input[name='" + json.bizSystem + "JspksMinimumWinRate']").val();
        json.xylhcLargestWinGain = $("input[name='" + json.bizSystem + "XylhcMaximumWinRate']").val();
        json.xylhcLowestWinGain = $("input[name='" + json.bizSystem + "XylhcMinimumWinRate']").val();
        json.sfsscLargestWinGain = $("input[name='" + json.bizSystem + "SfsscMaximumWinRate']").val();
        json.sfsscLowestWinGain = $("input[name='" + json.bizSystem + "SfsscMinimumWinRate']").val();
        json.shfsscLargestWinGain = $("input[name='" + json.bizSystem + "ShfsscMaximumWinRate']").val();
        json.shfsscLowestWinGain = $("input[name='" + json.bizSystem + "ShfsscMinimumWinRate']").val();
        json.wfsscLargestWinGain = $("input[name='" + json.bizSystem + "WfsscMaximumWinRate']").val();
        json.wfsscLowestWinGain = $("input[name='" + json.bizSystem + "WfsscMinimumWinRate']").val();
        json.lcqsscLargestWinGain = $("input[name='" + json.bizSystem + "LcqsscMaximumWinRate']").val();
        json.lcqsscLowestWinGain = $("input[name='" + json.bizSystem + "LcqsscMinimumWinRate']").val();

        json.jssscLargestWinGain = $("input[name='" + json.bizSystem + "JssscMaximumWinRate']").val();
        json.jssscLowestWinGain = $("input[name='" + json.bizSystem + "JssscMinimumWinRate']").val();
        json.bjsscLargestWinGain = $("input[name='" + json.bizSystem + "BjsscMaximumWinRate']").val();
        json.bjsscLowestWinGain = $("input[name='" + json.bizSystem + "BjsscMinimumWinRate']").val();
        json.gdsscLargestWinGain = $("input[name='" + json.bizSystem + "GdsscMaximumWinRate']").val();
        json.gdsscLowestWinGain = $("input[name='" + json.bizSystem + "GdsscMinimumWinRate']").val();
        json.scsscLargestWinGain = $("input[name='" + json.bizSystem + "ScsscMaximumWinRate']").val();
        json.scsscLowestWinGain = $("input[name='" + json.bizSystem + "ScsscMinimumWinRate']").val();
        json.shsscLargestWinGain = $("input[name='" + json.bizSystem + "ShsscMaximumWinRate']").val();
        json.shsscLowestWinGain = $("input[name='" + json.bizSystem + "ShsscMinimumWinRate']").val();
        json.sdsscLargestWinGain = $("input[name='" + json.bizSystem + "SdsscMaximumWinRate']").val();
        json.sdsscLowestWinGain = $("input[name='" + json.bizSystem + "SdsscMinimumWinRate']").val();


        json.sfksLargestWinGain = $("input[name='" + json.bizSystem + "SfksMaximumWinRate']").val();
        json.sfksLowestWinGain = $("input[name='" + json.bizSystem + "SfksMinimumWinRate']").val();
        json.wfksLargestWinGain = $("input[name='" + json.bizSystem + "WfksMaximumWinRate']").val();
        json.wfksLowestWinGain = $("input[name='" + json.bizSystem + "WfksMinimumWinRate']").val();
        json.wfsyxwLargestWinGain = $("input[name='" + json.bizSystem + "WfsyxwMaximumWinRate']").val();
        json.wfsyxwLowestWinGain = $("input[name='" + json.bizSystem + "WfsyxwMinimumWinRate']").val();
        json.sfsyxwLargestWinGain = $("input[name='" + json.bizSystem + "SfsyxwMaximumWinRate']").val();
        json.sfsyxwLowestWinGain = $("input[name='" + json.bizSystem + "SfsyxwMinimumWinRate']").val();
        json.sfpk10LargestWinGain = $("input[name='" + json.bizSystem + "Sfpk10MaximumWinRate']").val();
        json.sfpk10LowestWinGain = $("input[name='" + json.bizSystem + "Sfpk10MinimumWinRate']").val();
        json.wfpk10LargestWinGain = $("input[name='" + json.bizSystem + "Wfpk10MaximumWinRate']").val();
        json.wfpk10LowestWinGain = $("input[name='" + json.bizSystem + "Wfpk10MinimumWinRate']").val();
        json.shfpk10LargestWinGain = $("input[name='" + json.bizSystem + "Shfpk10MaximumWinRate']").val();
        json.shfpk10LowestWinGain = $("input[name='" + json.bizSystem + "Shfpk10MinimumWinRate']").val();

        if (json.bizSystem == 'SUPER_SYSTEM') {
            json.jyksLargestWinGain = $("input[name='jyksDefaultMaximumWinRate']").val();
            json.jyksLowestWinGain = $("input[name='jyksDefaultMinimumWinRate']").val();
            json.jlffcLargestWinGain = $("input[name='jlffcDefaultMaximumWinRate']").val();
            json.jlffcLowestWinGain = $("input[name='jlffcDefaultMinimumWinRate']").val();
            json.jspk10LargestWinGain = $("input[name='jspksDefaultMaximumWinRate']").val();
            json.jspk10LowestWinGain = $("input[name='jspksDefaultMinimumWinRate']").val();
            json.xylhcLargestWinGain = $("input[name='xylhcDefaultMaximumWinRate']").val();
            json.xylhcLowestWinGain = $("input[name='xylhcDefaultMinimumWinRate']").val();
            json.sfsscLargestWinGain = $("input[name='sfsscDefaultMaximumWinRate']").val();
            json.sfsscLowestWinGain = $("input[name='sfsscDefaultMinimumWinRate']").val();
            json.shfsscLargestWinGain = $("input[name='shfsscDefaultMaximumWinRate']").val();
            json.shfsscwestWinGain = $("input[name='shfsscDefaultMinimumWinRate']").val();
            json.wfsscLargestWinGain = $("input[name='wfsscDefaultMaximumWinRate']").val();
            json.wfsscLowestWinGain = $("input[name='wfsscDefaultMinimumWinRate']").val();
            json.lcqsscLargestWinGain = $("input[name='lcqsscDefaultMaximumWinRate']").val();
            json.lcqsscLowestWinGain = $("input[name='lcqsscDefaultMinimumWinRate']").val();
            json.jssscLargestWinGain = $("input[name='jssscDefaultMaximumWinRate']").val();
            json.jssscLowestWinGain = $("input[name='jssscDefaultMinimumWinRate']").val();
            json.bjsscLargestWinGain = $("input[name='bjsscDefaultMaximumWinRate']").val();
            json.bjsscLowestWinGain = $("input[name='bjsscDefaultMinimumWinRate']").val();
            json.gdsscLargestWinGain = $("input[name='gdsscDefaultMaximumWinRate']").val();
            json.gdsscLowestWinGain = $("input[name='gdsscDefaultMinimumWinRate']").val();
            json.scsscLargestWinGain = $("input[name='scsscDefaultMaximumWinRate']").val();
            json.scsscLowestWinGain = $("input[name='scsscDefaultMinimumWinRate']").val();
            json.shsscLargestWinGain = $("input[name='shsscDefaultMaximumWinRate']").val();
            json.shsscLowestWinGain = $("input[name='shsscDefaultMinimumWinRate']").val();
            json.sdsscLargestWinGain = $("input[name='sdsscDefaultMaximumWinRate']").val();
            json.sdsscLowestWinGain = $("input[name='sdsscDefaultMinimumWinRate']").val();

            json.sfksLargestWinGain = $("input[name='sfksDefaultMaximumWinRate']").val();
            json.sfksLowestWinGain = $("input[name='sfksDefaultMinimumWinRate']").val();
            json.wfksLargestWinGain = $("input[name='wfksDefaultMaximumWinRate']").val();
            json.wfksLowestWinGain = $("input[name='wfksDefaultMinimumWinRate']").val();
            json.wfsyxwLargestWinGain = $("input[name='wfsyxwDefaultMaximumWinRate']").val();
            json.wfsyxwLowestWinGain = $("input[name='wfsyxwDefaultMinimumWinRate']").val();
            json.sfsyxwLargestWinGain = $("input[name='sfsyxwDefaultMaximumWinRate']").val();
            json.sfsyxwLowestWinGain = $("input[name='sfsyxwDefaultMinimumWinRate']").val();
            json.sfpk10LargestWinGain = $("input[name='sfpk10DefaultMaximumWinRate']").val();
            json.sfpk10LowestWinGain = $("input[name='sfpk10DefaultMinimumWinRate']").val();
            json.wfpk10LargestWinGain = $("input[name='wfpk10DefaultMaximumWinRate']").val();
            json.wfpk10LowestWinGain = $("input[name='wfpk10DefaultMinimumWinRate']").val();
            json.shfpk10LargestWinGain = $("input[name='shfpk10DefaultMaximumWinRate']").val();
            json.shfpk10LowestWinGain = $("input[name='shfpk10DefaultMinimumWinRate']").val();
        }

        if (lotteryControlConfiPage.root.autHority == 1 || $(idx).attr("data-value") == "SUPER_SYSTEM") {
            if ($(idx).find("a").text() == " 关闭" || $(idx).find("a").text() == "") {
                list[list.length] = json;
            }
        }
    });
    if (list.length != 0) {
        managerLotteryControlConfigAction.saveLotteryControlConfig(list, function (r) {
            if (r[0] == "ok") {
                swal({title: "提示", text: "保存成功", type: "success"});
            } else if (r[0] == "error") {
                swal(r[1]);
            } else {
                swal("保存失败");
            }
        });
    }
}

/**
 * 开/关
 */
LotteryControlConfiPage.prototype.setEnabled = function (lotterid, bizSystem, enabledid) {
    if (lotteryControlConfiPage.root.autHority == 1) {
        var list = [];
        var json = new Object;
        json.id = lotterid;
        json.enabled = enabledid;
        json.bizSystem = bizSystem;
        list[list.length] = json;
        managerLotteryControlConfigAction.saveLotteryControlConfig(list, function (r) {
            if (r[0] == "ok") {
                if (enabledid == 0) {
                    swal("关闭成功");
                } else {
                    swal("启用成功");
                }
                lotteryControlConfiPage.initTableData();
            } else {
                swal("操作无效");
            }
        });

    }
}