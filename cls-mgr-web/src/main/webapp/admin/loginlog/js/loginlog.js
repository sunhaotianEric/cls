function LoginLogPage(){}
var loginLogPage = new LoginLogPage();

loginLogPage.param = {
   	
};

/**
 * 查询参数
 */
loginLogPage.queryParam = {
		bizSystem:null,
	    username : null,
	    logip : null,
	    loginDomain : null,
	    loginPort : null,
	    enabled : 1,	//默认用户非游客,有效数据
	    logtimeStart : null,
	    logtimeEnd : null,
        queryByLoginIp : null,
        queryByLoginTime : null,
        queryByUserName : null
};

//分页参数
loginLogPage.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize
};


$(document).ready(function() {

	
	//默认查询今天
	commonPage.setDateRangeTimeZero(1, "startime", "endtime",2);
	
	if(loginLogPage.queryParam.bizSystem != null){
		$("#bizSystem").val(loginLogPage.queryParam.bizSystem);
	}
	if(loginLogPage.queryParam.username != null){
		$("#username").val(loginLogPage.queryParam.username);
	}
	if(loginLogPage.queryParam.logip != null){
		$("#logip").val(loginLogPage.queryParam.logip);
	}
	loginLogPage.initTableData(); //按时间查询所有的登录日志数据
});


/**
 * 初始化列表数据
 */
LoginLogPage.prototype.initTableData = function() {
	loginLogPage.queryParam = getFormObj($("#queryForm"));
	if(isTouristParameter == 1){
		loginLogPage.queryParam.enabled = 0;
	}else{
		loginLogPage.queryParam.enabled = 1;
	}
	loginLogPage.table = $('#loginLogTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"scrollX": true,
		"iDisplayLength" : defaultPageSize,
		"ajax":function (data, callback, settings) {
			loginLogPage.pageParam.pageSize = data.length;//页面显示记录条数
			loginLogPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerLoginLogAction.getAllLoginLog(loginLogPage.queryParam,loginLogPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": "bizSystemName","bVisible":currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "userName",
		            	render: function(data, type, row, meta) {	
		            		
		            		if(row.logType == "ADMIN"){
		            			return data;
		            		}else{
		            		return "<a href='javascript:void(0)' style='color:blue;text-decoration:underline;'  onclick='commonPage.showUserMsgPage(this,"+row.userId+")'>"+ row.userName +"</a>";
		            		}
	            		}
		            },
		            {"data": "loginIp"},
		            {   
		            	"data": "address",
		            	render: function(data, type, row, meta) {
		            		var str="";
		            		if(row.address == null || row.address == ""){
		            			str += "未知地址";
		            		}else{
		            			str += ""+ row.address;
		            		}
		            		return str;
	            		}
		            },
		            {"data": "logtimeStr"},
		            {   
		            	"data": "logType",
		            	render: function(data, type, row, meta) {
		            		var loginType = "";
		            		if("PC" == row.logType) {
		            			loginType = "电脑";
		            		}else if("MOBILE" == row.logType) {
		            			loginType = "手机web";
		            		}else if("ADMIN" == row.logType){
		            			loginType = "管理员"
		            		}else if("APP" == row.logType){
		            			loginType = "APP"
		            		}
		            		return loginType;
	            		}
		            },	
		            {   
		            	"data": "loginDomain",
		            	render: function(data, type, row, meta) {
		            		var scheme = "http://";
		            		if(443 == row.loginPort) {
		            			scheme = "https://";
		            		}
		            		var domainStr = scheme + data + ":" + row.loginPort;
		            		return domainStr;
	            		}
		            },	
		            {
	                	 "data": "browser",
	                	 render: function(data, type, row, meta) {
	                		 return "<input type='hidden'id='browser' value='"+ row.browser +"'/><a href='javascript:void(0)' onclick='loginLogPage.showInfo(\""+ row.browser +"\")' class='btn btn-white btn-look'><i class='fa fa-search'></i>&nbsp;查看用户系统信息</a>";
	                	 }
	                }
	            ]
	}).api(); 
}







/**
 * 按日期查询数据
 */
LoginLogPage.prototype.findLoginLogByQuery = function(dateRange){
	if(dateRange != undefined && dateRange != null && dateRange != ''){
		commonPage.setDateRangeTimeZero(dateRange, "startime", "endtime",2);	
	}
	loginLogPage.queryParam = getFormObj($("#queryForm"));
	if(isTouristParameter == 1){
		loginLogPage.queryParam.enabled = 0;
	}else{
		loginLogPage.queryParam.enabled = 1;
	}
	loginLogPage.queryParam.queryByLoginTime = 1;
	loginLogPage.queryParam.queryByUserName = 0;
	loginLogPage.queryParam.queryByLoginIp = 0;
	loginLogPage.table.ajax.reload();
};




/**
 * 查看用户系统信息
 */
LoginLogPage.prototype.showInfo = function(browser){
	swal(browser);
};
/**
 * 根据id赋值selelct标签
 * @param id
 */

