<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
	String imgServerUrl = SystemConfigConstant.imgServerUrl;
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>活动中心</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    <!-- js -->
    <script type="text/javascript">
    	var imgServerUrl = '<%= imgServerUrl %>';
    </script>
    <script type="text/javascript" src="<%=path%>/admin/activity/js/activity.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerActivityAction.js'></script>
    
</head>
<body >
<div class="animated fadeIn">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox ">
                <div class="ibox-content">
                    <form class="form-horizontal" id="activityQuery">
                        <div class="row">
                            <div class="col-sm-4 biz" >
                              <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">商户：</span>
                                     <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
                                    
                                </div>
                                </c:if>
                            </div>
                            
                            <div class="col-sm-8">
                                <div class="form-group nomargin text-right">
                                    <button type="button" class="btn btn-outline btn-default btn-kj biz" onclick="activityPage.findActivityByBizSystem()"><i class="fa fa-search"></i>&nbsp;查 询</button>
                                    <button type="button" class="btn btn-outline btn-default btn-kj" onclick="activityPage.addActivity()"><i class="fa fa-plus"></i>&nbsp;新增</button>
                                </div>
                            </div>
                          <!--   <div class="col-sm-8">
                                <div class="form-group nomargin text-right">
                                </div>
                            </div> -->
                        </div>
                    </form>
                    <div class="jqGrid_wrapper">
                        <div class="ui-jqgrid ">
                            <div class="ui-jqgrid-view">
                                <div class="ui-jqgrid-hdiv">
                                    <div class="ui-jqgrid-hbox" style="padding-right:0">
                                        <table class="ui-jqgrid-htable ui-common-table table table-bordered" id="activityTable">
                                            <thead>
                                                <tr class="ui-jqgrid-labels">
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">序号</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">商户</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">活动类型</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">活动图片</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">标题</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">活动说明</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">展示顺序</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">发布日期</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">更新日期</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">是否启用</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">操作</div></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                   <!--              <tr role="row" tabindex="-1" class="jqgrow ui-row-ltr">
                                                    <td>13</td>
                                                    <td>天天消费 天天有奖励</td>
                                                    <td>是</td>
                                                    <td>是</td>
                                                    <td>是</td>
                                                    <td>2</td>
                                                    <td>2016/08/05 12:15:05</td>
                                                    <td>
                                                        <a class="btn btn-info btn-sm btn-bj">
                                                            <i class="fa fa-pencil"></i>&nbsp;编辑</a>
                                                        <a class="btn btn-danger btn-sm btn-del">
                                                            <i class="fa fa-trash"></i>&nbsp;删除</a>
                                                    </td>
                                                </tr>
                                                <tr role="row" tabindex="-1" class="jqgrow ui-row-ltr">
                                                    <td>14</td>
                                                    <td>今春豪礼 百元现金赠送</td>
                                                    <td>是</td>
                                                    <td>是</td>
                                                    <td>是</td>
                                                    <td>2</td>
                                                    <td>2016/08/05 12:15:05</td>
                                                    <td>
                                                        <a class="btn btn-info btn-sm btn-bj">
                                                            <i class="fa fa-pencil"></i>&nbsp;编辑</a>
                                                        <a class="btn btn-danger btn-sm btn-del">
                                                            <i class="fa fa-trash"></i>&nbsp;删除</a>
                                                    </td>
                                                </tr> -->
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    
</body>
</html>

