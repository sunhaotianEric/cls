function EditActivityPage(){}
var editActivityPage = new EditActivityPage();

editActivityPage.param = {
   	
};

$(document).ready(function() {
	var activityId = editActivityPage.param.id;  //获取查询的活动ID
	var bizSytem = editActivityPage.param.bizSytem;  //获取查询的活动ID
	if(activityId != null){
		editActivityPage.editActivity(activityId);
	}
	
	editActivityPage.getcurrentBizSystem();
});


/**
 * 保存活动信息
 */
EditActivityPage.prototype.saveData = function(){
	var id = $("#activityId").val();
	var bizSystem = $("#bizSystemId").val();
	var type = $("#typeId").val();
	var title = $("#title").val();
	var des = editor.html();
	var startDate = new Date($("#startime").val());
	var endDate = new Date($("#endtime").val());
	var status = $("#status").val();
	var imagePath = $("#imagePath").val();
	var mobileImagePath = $("#mobileImagePath").val();
	var showOrder = $("#showOrder").val();
	
	if(title==null || title.trim()==""){
		swal("请输入活动主题!");
		$("#title").focus();
		return;
	}
	if(currentUser.bizSystem=='SUPER_SYSTEM' && (bizSystem == null || bizSystem.trim() == "")){
		swal("请选择子系统!");
		$("#bizSystemId").focus();
		return;
	}
	
	if(type==null || type.trim()==""){
		swal("请选择活动类型!");
		$("#typeId").focus();
		return;
	}
	
	if(des==null || des.trim()==""){
		swal("请输入活动说明!");
		$("#des").focus();
		return;
	}
	if(startDate==null || $("#startime").val().trim()==""){
		swal("请输入开始时间!");
		return;
	}
	if(endDate==null || $("#endtime").val().trim()==""){
		swal("请输入结束时间!");
		return;
	}
	if(imagePath==null || imagePath.trim()==""){
		swal("请上传活动图片!");
		$("#imagePath").focus();
		return;
	}

	if(showOrder==null || showOrder.trim()==""){
		swal("请输入展示顺序!");
		$("#showOrder").focus();
		return;
	}
	if(status==null || status.trim()==""){
		swal("请选择活动状态!");
		return;
	}
	if(status == 1){
		if(imagePath==null || imagePath.trim()==""){
			swal("请上传活动图片!");
			$("#imagePath").focus();
			return;
		}
	}
	
	
	var activityCore = {};
	activityCore.id = id;
	activityCore.bizSystem = bizSystem;
	activityCore.type = type;
	activityCore.title = title;
	activityCore.des = des;
	activityCore.startDate = startDate;
	activityCore.endDate = endDate;
	activityCore.status = status;
	activityCore.imagePath = imagePath;
	activityCore.mobileImagePath = mobileImagePath;
	activityCore.showOrder = showOrder;
	managerActivityAction.saveOrUpdateActivity(activityCore,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({
		           title: "保存成功",
		           text: "您已经保存了这条活动。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.activityPage.closeLayer(index);
		   	    });
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("保存活动信息失败.");
		}
    });
};

EditActivityPage.prototype.onlyNum=function() {
    if(!(event.keyCode==46)&&!(event.keyCode==8)&&!(event.keyCode==37)&&!(event.keyCode==39))
    if(!((event.keyCode>=48&&event.keyCode<=57)||(event.keyCode>=96&&event.keyCode<=105)))
    event.returnValue=false;
}
/**
 * 获取当前业务系统
 */
EditActivityPage.prototype.getcurrentBizSystem=function(){
	managerActivityAction.getcurrentBizSystem(function(r){
		if (r[0] != null && r[0] == "ok") {
			var bizSystem = r[1];
			if(bizSystem == "SUPER_SYSTEM"){
				$("#bizSystem").val(bizSystem);
			}else{
				$("#bizSystem").val(bizSystem);
				$('#bizSystem').attr("disabled",true);
				$('.biz').hide();
			}
		}else{
			swal("获取当前业务系统失败！");
		}
	});
}

/**
 * 赋值活动信息
 */
EditActivityPage.prototype.editActivity = function(id){
	managerActivityAction.getActivityById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			$("#activityId").val(r[1].id);
			$("#bizSystemId").val(r[1].bizSystem);
			$("#typeId").val(r[1].type);
			$("#title").val(r[1].title);
			editor.html(r[1].des);
			$("#startime").val(r[1].startDateStr);
			$("#endtime").val(r[1].endDateStr);
			
			$("#imagePath").val(r[1].imagePath);
			var mobileImagePath = r[1].mobileImagePath;
			$("#mobileImagePath").val(mobileImagePath);
			
			$("#status").val(r[1].status);
			$("#showOrder").val(r[1].showOrder);
			
			//展示图片内容
			$("#image").attr("src",r[2] + r[1].imagePath);
			$("#image").show();
			if(typeof(mobileImagePath) != "undefined" && mobileImagePath != null){
				$("#mobileImage").attr("src",r[2] + r[1].mobileImagePath);
				$("#mobileImage").show();
			}
			
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取活动信息失败.");
		}
    });
};

/**
 * 赋值活动信息
 */
EditActivityPage.prototype.uploadImgFile = function(id, picType){
	var fileId = id + "File";
	if($("#" + fileId).val() == "") {
		swal("请选择图片文件");
		return; 
	}
	
	var bizSystem = $("#bizSystemId").val();
	if(currentUser.bizSystem=='SUPER_SYSTEM' && (bizSystem == null || bizSystem.trim() == "")){
		swal("请选择子系统!");
		$("#bizSystemId").focus();
		return;
	}
	if(currentUser.bizSystem !='SUPER_SYSTEM') {
		bizSystem = currentUser.bizSystem;
	}
	
	$("#" + id +"Info").text("上传中...");
	var uploadFile = dwr.util.getValue(fileId);
	managerActivityAction.uploadImgFile(uploadFile, bizSystem, picType, function(r){
		if (r[0] != null && r[0] == "ok") {
			$("#" + id + "Path").val(r[1]);
			$("#" + id).attr("src", r[2] + r[1]);
			$("#" + id).show();
			$("#" + id +"Info").text("上传成功!");
			$("#" + id + "Info").parent().addClass("btn-danger");
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("上传文件失败");
		}
	});
};