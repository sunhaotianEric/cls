function ActivityPage(){}
var activityPage = new ActivityPage();

activityPage.param = {
		bizSystem :null
};

//分页参数
activityPage.pageParam = {
    pageNo : 1,
    pageSize : defaultPageSize
};


$(document).ready(function() {
	activityPage.initTableData();
	

});

ActivityPage.prototype.initTableData = function(){
	if(currentUser.bizSystem != "SUPER_SYSTEM"){
		$(".biz").hide();
		$(".text-right").parent().removeClass();
	}
	activityPage.param = getFormObj($("#activityQuery"));
	activityPage.table =  $('#activityTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"scrollX": true,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			activityPage.pageParam.pageSize = data.length;//页面显示记录条数
			activityPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
			managerActivityAction.getActivitysByQueryPage(activityPage.param,activityPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				}; 
				
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询奖金配置数据失败.");
				}
				callback(returnData);
		    });
		},
		"columns": [
		            {"data":null},
		            {"data":"bizSystemName",
		            	"bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data":"type",
		            	render: function(data, type, row, meta) {
		            		if(data == "DAY_CONSUME_ACTIVITY"){
		            			return '流水返送活动';
		            		}else if(data == "SIGNIN_ACTIVITY"){
		            			return '签到活动';
		            		}else if(data == "NORMAL"){
		            			return '普通活动';
		            		}else if(data == "PROMOTON_ACTIVITY"){
		            			return '晋级活动';
		            		}
		            		return data;
	            		}
		            },
		            {"data":"imagePath",
		            		render:function(data,type,row,meta){
		            			return "<a target='_blank' href='" + imgServerUrl + data +"'><img  alt='活动图片' src='"+ imgServerUrl + data+"' style='margin-left: 21px; width: 100px;height: 50px;' ></a>"
		            		}
		            },
		            {"data":"title"},
		            {"data":"des",
		            	render: function(data, type, row, meta) {
		            		if (data.length > 15) {
		            			var title = data.substring(0,15) + "...";
		            			return "<span title = '"+data+"'>"+title+"</span>";
		            		}
		            		return data;
	            		}
		            },		      
		            {"data":"showOrder"},
		            {"data":"createDateStr"},
		            {"data":"updateDateStr"},
		            {"data":"status",
		            	render: function(data, type, row, meta) {
		            		if (data == "1") {
		            			return '是';
		            		} else {
		            			return '否';
		            		}
		            		return data;
	            		}
		            },
		            {"data":"id",
		            	 render: function(data, type, row, meta) {
		            		 var str = "";
		            		 if(row.status == 1){
		            			 str += "<a class='btn btn-warning btn-sm btn-bj' onclick='activityPage.disEnableActivity("+data+")'><i class='fa fa-pencil'></i>&nbsp;停用 </a>&nbsp;&nbsp;"
		            		 }else{
		            			 str += "<a class='btn btn-sm btn-bj btn-success' onclick='activityPage.enableActivity("+data+")'><i class='fa fa-pencil'></i>&nbsp;启用 </a>&nbsp;&nbsp;"
		            		 }
	                		 str += "<a class='btn btn-info btn-sm btn-bj' onclick='activityPage.editActivity("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;"
	                		 str +=	"<a class='btn btn-danger btn-sm btn-del' onclick='activityPage.delActivity("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";
	                		 return str;
	                	 }
		            },
		      ]
	}).api();
	
}

/**
 * 条件查找活动
 */
ActivityPage.prototype.findActivityByBizSystem = function(){
	activityPage.param.bizSystem = $("#bizSystem").val();
	activityPage.table.ajax.reload();
}

/**
 * 删除活动中心信息
 */
ActivityPage.prototype.delActivity = function(id){
	   swal({
           title: "您确定要删除这条信息吗",
           text: "删除后将无法恢复，请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "删除",
           closeOnConfirm: false
       },
       function() {
    	   managerActivityAction.delActivity(id,function(r){
    		   if (r[0] != null && r[0] == "ok") {
    			   swal("删除成功！", "您已经永久删除了这条信息。", "success");
    			   activityPage.findActivityByBizSystem(); //查询奖金配置数据
    		   }else if(r[0] != null && r[0] == "error"){
    			   swal(r[1]);
    		   }else{
    			   swal("删除活动中心信息失败.");
    		   }
    	   });
       })
};

/**
 * 启用活动
 * @param id
 */
ActivityPage.prototype.enableActivity = function(id){
	managerActivityAction.enableActivity(id,function(r){
	   if (r[0] != null && r[0] == "ok") {
		   swal("启用成功！");
		   activityPage.findActivityByBizSystem(); //查询奖金配置数据
	   }else if(r[0] != null && r[0] == "error"){
		   swal(r[1]);
	   }else{
		   swal("删除活动中心信息失败.");
	   }
   })
}

/**
 * 关闭活动
 * @param id
 */
ActivityPage.prototype.disEnableActivity = function(id){
	managerActivityAction.disEnableActivity(id,function(r){
	   if (r[0] != null && r[0] == "ok") {
		   swal("关闭成功！");
		   activityPage.findActivityByBizSystem(); //查询奖金配置数据
	   }else if(r[0] != null && r[0] == "error"){
		   swal(r[1]);
	   }else{
		   swal("删除活动中心信息失败.");
	   }
   })
}

ActivityPage.prototype.closeLayer=function(index){
	layer.close(index);
	activityPage.findActivityByBizSystem(); //查询奖金配置数据
}

/**
 * 编辑活动中心信息
 */
ActivityPage.prototype.editActivity = function(id){
	layer.open({
		type: 2,
		title: '活动信息编辑',
		maxmin: false,
		shadeClose: true,
		//点击遮罩关闭层
		area: ['70%', '80%'],
		content:contextPath + '/managerzaizst/activity/'+ id +'/editactivity.html',
	})
}

/**
 * 编辑活动中心信息
 */
ActivityPage.prototype.addActivity = function(){
	layer.open({
		type: 2,
		title: '添加活动信息编辑',
		maxmin: false,
		shadeClose: true,
		//点击遮罩关闭层
		area: ['70%', '80%'],
		content:contextPath + '/managerzaizst/activity/activity_add.html',
	})
}
