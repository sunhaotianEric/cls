function WithDrawPage(){}
var withDrawPage = new WithDrawPage();

withDrawPage.param = {
	orderList : null,
	operatePayId : null,
	refreshKey : null,
	nowSerialNumber:null,
	refreshTime:5
};


/**
 * 查询参数
 */
withDrawPage.queryParam = {
	userName : null,
	statuss : null,
	bizSystem:null,
	createdDateStart : null,
	createdDateEnd : null,
	fromType:null
	
};

//分页参数
withDrawPage.pageParam = {
		pageNo : 1,
	    pageSize : defaultPageSize
};

$(document).ready(function() {
	$("#createdDateStart").val(getClsStartTimeZero().format("yyyy-MM-dd hh:mm:ss"));
	$("#createdDateEnd").val(getClsEndTimeZero().format("yyyy-MM-dd hh:mm:ss"));
	$("#WithdrawSoundControl").val(parent.WithdrawSoundControl);
	withDrawPage.initTableData(); //根据条件,查找资金明细
	parent.withdrawOrderTable=withDrawPage.table;
	
	$("#refreshTime").val(parent.refreshWithdrawKeyTime);
	
	$("#refreshTime").change(function(){
		var value=parseInt($(this).val());
		parent.refreshWithdrawKeyTime=value;
		parent.resetWithdrawOrderInterval();
	})
	$("#WithdrawSoundControl").on("change",function(){
		parent.WithdrawSoundControl=$("#WithdrawSoundControl").val();
	});
});

WithDrawPage.prototype.intervalLoad = function(){
	interVal = setInterval("parent.refreshWithdrawOrderKey()",parseInt(withDrawPage.param.refreshTime)*1000);  
}

/*WithDrawPage.prototype.refreshKey=function(){
	var param={}
	param.operateType="WITHDRAW";
	managerUptodateSerialNumberAction.getUptodateSerialNumber(param,function(r){
		if (r[0] != null && r[0] == "ok") {
			if(r[1]!=null && r[1]!="")
				{
				
				if(withDrawPage.param.nowSerialNumber==null)
				{
					
				withDrawPage.param.nowSerialNumber=r[1];
				}
				
			else
				{
				
				if(withDrawPage.param.nowSerialNumber!=r[1])
					{
					$("#voiceAutoMsg")[0].play();
					withDrawPage.param.nowSerialNumber=r[1];
					withDrawPage.table.ajax.reload();
					}
				}
				
				}
			
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("查询数据失败.");
		}
	})
}	*/
/**
 * 加载所有的登陆日志数据
 */


WithDrawPage.prototype.initTableData = function(){

	withDrawPage.queryParam = getFormObj($("#withDrawForm"));
	withDrawPage.table = $('#withDrawTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"ajax":function (data, callback, settings) {
			withDrawPage.pageParam.pageSize = data.length;//页面显示记录条数
			withDrawPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerRechargeWithDrawOrderAction.getAllWithDrawOrdersByQuery(withDrawPage.queryParam,withDrawPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败.");
				}
				/*console.log(returnData);*/
				callback(returnData);
			});
		},
		"columns": [
                    {"data": "bizSystemName", 
	                 "bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "serialNumber"},
		            {"data": "userName",render: function(data, type, row, meta) {	
	            		return "<a href='javascript:void(0)' style='color:blue;text-decoration:underline;'  onclick='commonPage.showUserMsgPage(this,"+row.userId+")'>"+ row.userName +"</a>";
            		}},
		            {"data": "applyValue",render: function(data, type, row, meta) {
		            	var str=data.toFixed(2);
	                		 return str ;
	                	 }},
	               {"data": "feeValue",render: function(data, type, row, meta) {
	 		            	var str=data.toFixed(2);
	 	                		 return str ;
	 	                	 }},
		            {"data": "accountValue",render: function(data, type, row, meta) {
		            	var str=data.toFixed(2);
	                		 return str ;
	                	 }},
	                	 {"data": "createdDateStr"},
		           /* {"data": "operateDes"},
		            {"data": "postscript"},*/
		            {"data": "statusStr",render: function(data, type, row, meta) {
 		            	var value=row.dealStatus;
 		            	if(value=="DEALING")
 		            		{
 		            		return "<font color='blue'>"+data+"</font>" ;
 		            		}
 		            	else if(value=="WITHDRAW_SUCCESS")
 		            		{
 		            		return "<font color='green'>"+data+"</font>" ;
 		            		}
 		            	else if(value=="WITHDRAW_FAIL")
 		            		{
 		            		return "<font color='red'>"+data+"</font>" ;
 		            		}
 		            	else if(value=="LOCKED")
 		            		{
 		            		return "<font color='purple'>"+data+"</font>" ;
 		            		}
                		 
                  }}, 
		            /*{"data": "id",
		            	render: function(data, type, row, meta) {
	                		var str="<input class='ipt' name='analyseValue' id='analyseValue"+row.id+"' type='text'>"
	                		 return str ;
	                	 }
		            },*/
		            {
	                	 "data": "id",
	                	 render: function(data, type, row, meta) {
	                		 var str="";
	                		 if(row.dealStatus == 'DEALING'){
	                				str += "     <button type='button' name='setWithDrawOrdersLockedButton' onclick='withDrawPage.setWithDrawOrdersLocked("+row.id+")'  class='btn btn-primary btn-large'>锁定</button>";
	                			} else if(row.dealStatus == 'LOCKED') {
	                				if(currentAdminName == row.lockAdmin) {
	                					str += "     <button type='button' name='setWithDrawOrdersPaySuccessButton' onclick='withDrawPage.setWithDrawOrdersPaySuccess("+row.id+")'  class='btn btn-primary btn-large'>确认取现</button>";
	                					str += "     <button type='button' name='setWithDrawOrdersPaySuccessButton' onclick='withDrawPage.setWithDrawAutoOrdersPaySuccess("+row.id+")'  class='btn btn-primary btn-large'>自动出款</button>";
	                					str += "     <button type='button'  name='setWithDrawOrdersPayCloseButton' onclick='withDrawPage.setWithDrawOrdersPayClose("+row.id+")'   class='btn btn-danger btn-large'>关闭订单</button>";
	                				} else {
	                					str += "     <span style='color:green'>已被管理员["+ row.lockAdmin +"]锁定</span>";
	                				}
	                			}else{
	                				str += "     <span style='color:green'>已处理</span>";
	                			}
	                			 
	                		 return str ;
	                	 }
	                },
	                {"data": "fromType",
		            	  render: function(data, type, row, meta) {
		            			var fromType = "";
		            			if("PC" == row.fromType) {
		            				fromType = "电脑";
		            			} else if("MOBILE" == row.fromType) {
		            				fromType = "手机web";
		            			} else if("APP" == row.fromType) {
		            				fromType = "APP";
		            			}
		            			return fromType;
		            	  }
		            },
	                {"data": "bankUserName"},
		            {"data": "bankName"},
		            {"data": "subbranchName"},
		            {"data": "bankId"},
	                {"data": "updateAdmin"},
		            {"data": "updateDateStr"}
	            ]
	}).api(); 
	//withDrawPage.reBindClickEvent();
};

/**
 * 查询数据
 */
WithDrawPage.prototype.findWithDraw = function(dateRange){
	if(dateRange != undefined && dateRange != null && dateRange != ''){
		commonPage.setDateRangeTimeZero(dateRange, "createdDateStart", "createdDateEnd",2);	
	}
	var withDrawStatus = $("#withDrawStatus").val();
	var userName = $("#userName").val();
	var bizSystem = $("#bizSystem").val();
	var createdDateStart = $("#createdDateStart").val();
	var createdDateEnd = $("#createdDateEnd").val();
	var fromType = $("#fromType").val();
	if(userName == ""){
		withDrawPage.queryParam.userName = null;
	}else{
		withDrawPage.queryParam.userName = userName;
	}
	if(withDrawStatus == ""){
		withDrawPage.queryParam.statuss = null;
	}else{
		withDrawPage.queryParam.statuss = withDrawStatus;
	}
	if(bizSystem == ""){
		withDrawPage.queryParam.bizSystem = null;
	}else{
		withDrawPage.queryParam.bizSystem = bizSystem;
	}
	if(createdDateStart == ""){
		withDrawPage.queryParam.createdDateStart = null;
	}else{
		withDrawPage.queryParam.createdDateStart =new Date(createdDateStart);
	}
	if(createdDateEnd == ""){
		withDrawPage.queryParam.createdDateEnd = null;
	}else{
		withDrawPage.queryParam.createdDateEnd =new Date(createdDateEnd);
	}
	if(fromType == ""){
		withDrawPage.queryParam.fromType = null;
	}else{
		withDrawPage.queryParam.fromType = fromType;
	}
	withDrawPage.table.ajax.reload();
};




/**
 * 订单锁定
 */
WithDrawPage.prototype.setWithDrawOrdersLocked = function(rechargeOrderId){
	if(rechargeOrderId != null){
		managerRechargeWithDrawOrderAction.setWithDrawOrdersLocked(rechargeOrderId,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "订单锁定成功",type: "success"});
				withDrawPage.table.ajax.reload();
			}else if(r[0] != null && r[0] == "error"){
				swal({ title: "提示", text: r[1],type: "error"});
				withDrawPage.table.ajax.reload();
			}else{
				swal({ title: "提示", text: "订单锁定失败",type: "success"});
				//showErrorDlg(jQuery(document.body), "确认到账该订单请求失败.");
			}
	    });
    }else{
    	  swal("该订单参数未找到!");
    }
	
};

/**
 * 订单确认到账
 */
WithDrawPage.prototype.setWithDrawOrdersPaySuccess = function(rechargeOrderId){

    if(rechargeOrderId!= null){
	    swal({ 
	        title: "确认取现", 
	        text: "请核对到账金额;您确认要取现?", 
	        type: "warning", 
	        showCancelButton: true, 
	        closeOnConfirm: false,
	        cancelButtonText: "取消", 
	        confirmButtonText: "是的，我确定", 
	        confirmButtonColor: "#ec6c62" 
	    }, function() { 
	    	managerRechargeWithDrawOrderAction.setWithDrawOrdersPaySuccess(rechargeOrderId,function(r){
	    		if (r[0] != null && r[0] == "ok") {
	    			swal({ title: "提示", text: "订单确认到账成功",type: "success"});
	    			withDrawPage.table.ajax.reload();
	    		}else if(r[0] != null && r[0] == "error"){
	    			swal({ title: "提示", text: r[1],type: "error"});
	    			withDrawPage.table.ajax.reload();
	    		}else{
	    			swal({ title: "提示", text: "确认到账该订单请求失败",type: "error"});
	    		}
	        });
	    });
    }else{
    	swal("该订单参数未找到!");
    }
	
};

/**
 * 订单取消 rechargeOrderId,analyseValue
 */
WithDrawPage.prototype.setWithDrawOrdersPayClose = function(rechargeOrderId){
	/*var id="#analyseValue"+rechargeOrderId;*/
	/*var analyseValue=$(id).val();
	if(analyseValue == null || analyseValue.length == 0){
		swal("请填写关闭订单原因!");
    	return;
    }
    if(analyseValue.length > 300){
    	swal("原因的内容输入过多,请压缩.!");
    	return;
    }*/
   
    if(rechargeOrderId != null){
	   
	    swal({ 
	        title: "取消订单", 
	        text: "请核对相关订单信息;是否确认取消订单?", 
	        type: "input", 
	        showCancelButton: true, 
	        closeOnConfirm: false,
	        cancelButtonText: "取消", 
	        confirmButtonText: "是的，我要取消", 
	        confirmButtonColor: "#ec6c62" 
	    }, function(inputValue) {
	    	if(inputValue == null || inputValue.length == 0){
	    		swal("请填写关闭订单原因!");
	        	return;
	        }
	        if(inputValue.length > 300){
	        	swal("原因的内容输入过多,请压缩.!");
	        	return;
	        }
	        if(inputValue==false){
	        	return;
	        }
	    	managerRechargeWithDrawOrderAction.setWithDrawOrdersPayClose(rechargeOrderId,inputValue,function(r){
	    		if (r[0] != null && r[0] == "ok") {
	    			swal({ title: "提示", text: "订单取消成功",type: "success"});
	    			withDrawPage.table.ajax.reload();
	    		}else if(r[0] != null && r[0] == "error"){
	    			swal({ title: "提示", text: r[1],type: "error"});
	    			withDrawPage.table.ajax.reload();
	    		}else{
	    			swal({ title: "提示", text: "取消该取现订单请求失败",type: "error"});
	    		}
	        });
	    });
	    
    }else{
    	swal("该订单参数未找到!");
    }
	/**/
};
WithDrawPage.prototype.withDraworderInfoExport = function(){
	
	withDrawPage.queryParam = getFormObj($("#withDrawForm"));
	withDrawPage.queryParam.operateType="WITHDRAW";
	managerRechargeWithDrawOrderAction.rechargeWithDrawInfoExport(withDrawPage.queryParam,function(r){
		dwr.engine.openInDownload(r);
		
    });
}

WithDrawPage.prototype.setWithDrawAutoOrdersPaySuccess = function(rechargeOrderId){

	managerRechargeWithDrawOrderAction.setWithDrawAutoOrdersPaySuccess(rechargeOrderId,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({ title: "提示", text:r[1],type: "success"});
			withDrawPage.table.ajax.reload();
		}else if(r[0] != null && r[0] == "error"){
			swal({ title: "提示", text: r[1],type: "error"});
			withDrawPage.table.ajax.reload();
		}else{
			swal({ title: "提示", text: "确认到账该订单请求失败",type: "error"});
		}
    });
	
};
/**
 * 重新加载单个订单数据
 *//*
WithDrawPage.prototype.reloadWithDrawOrderInfo = function(rechargeOrderId){
	managerRechargeWithDrawOrderAction.getRechargeWithDrawOrder(rechargeOrderId,function(r){
		if (r[0] != null && r[0] == "ok") {
			withDrawPage.refreshWithDrawOrdersInfo(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "重新加载单个订单请求失败.");
		}
    });
};*/




/**
 * 重新绑定按钮点击事件
 */
/*WithDrawPage.prototype.reBindClickEvent = function() {
	//订单锁定
	$("button[name='setWithDrawOrdersLockedButton']").click(function(){
	    var orderId = $(this).attr("data-id");
	    withDrawPage.param.operatePayId = orderId;
	    if(withDrawPage.param.operatePayId != null){
	    	withDrawPage.setWithDrawOrdersLocked(withDrawPage.param.operatePayId);
	    }else{
           alert("该订单参数未找到");
	    }
	});
	//确认到账
	$("button[name='setWithDrawOrdersPaySuccessButton']").click(function(){
		var orderId = $(this).attr("data-id");
	    withDrawPage.param.operatePayId = orderId;
	    if(withDrawPage.param.operatePayId != null){
		    if(confirm("是否确认取现?")){
		    	withDrawPage.setWithDrawOrdersPaySuccess(withDrawPage.param.operatePayId);
			}
	    }else{
           alert("该订单参数未找到");
	    }
	});
	//订单取消
	$("button[name='setWithDrawOrdersPayCloseButton']").click(function(){
		var orderId = $(this).attr("data-id");
	    var analyseValue = $(this).parents("tr").find("input[name='analyseValue']").val();
	    if(analyseValue == null || analyseValue.length == 0){
	    	alert("请填写关闭订单原因.");
	    	return;
	    }
	    if(analyseValue.length > 300){
	    	alert("原因的内容输入过多,请压缩.");
	    	return;
	    }
	    withDrawPage.param.operatePayId = orderId;
	    if(withDrawPage.param.operatePayId != null){
		    if(confirm("是否确认取消订单?")){
		    	withDrawPage.setWithDrawOrdersPayClose(withDrawPage.param.operatePayId,analyseValue);
			}
	    }else{
           alert("该订单参数未找到");
	    }
	});
}
*/