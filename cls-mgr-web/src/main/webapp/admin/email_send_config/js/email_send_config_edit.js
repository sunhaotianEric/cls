function EmailSendConfigPage() {
}
var emailSendConfigPage = new EmailSendConfigPage();

/**
 * 查询参数
 */
emailSendConfigPage.queryParam = {
	id : null,
	mailHost : null,
	mailPort : null,
	userName : null,
	password : null,
	validate : null,
	sendType : null,
	type : null,
	orders : null,
	enable : null,
	createTime : null,
	updateTime : null,
	orders : null
};
emailSendConfigPage.param = {
// skipAwardZh:0
};

$(document).ready(function() {
	/*
	 * emailSendConfigPage.queryParam.id=id;
	 * if(emailSendConfigPage.queryParam.id!=null&&emailSendConfigPage.queryParam.id!=""){
	 * emailSendConfigPage.selectByPrimaryKey(); }
	 */
	var id = emailSendConfigPage.param.id; // 获取查询的ID
	if (id != null) {
		emailSendConfigPage.editConfigControl(id);
	}

});

EmailSendConfigPage.prototype.selectByPrimary = function() {
	var id = $("#id").val();
	var mailHost = $("#mailHost").val();
	var mailPort = $("#mailPort").val();
	var userName = $("#userName").val();
	var password = $("#password").val();
	var validate = $("#validate").val();
	var sendType = $("#sendType").val();
	var type = $("#type").val();
	var orders = $("#orders").val();
	var enable = $("#enable").val();
	var va = /^[0-9]*$/;// 验证正小数

	emailSendConfigPage.queryParam.id = id;
	emailSendConfigPage.queryParam.mailHost = mailHost;
	emailSendConfigPage.queryParam.mailPort = mailPort;
	emailSendConfigPage.queryParam.userName = userName;
	emailSendConfigPage.queryParam.password = password;
	emailSendConfigPage.queryParam.validate = validate;
	emailSendConfigPage.queryParam.sendType = sendType;
	emailSendConfigPage.queryParam.type = type;
	emailSendConfigPage.queryParam.orders = orders;
	emailSendConfigPage.queryParam.enable = enable;
	if (!va.test(orders)) {
		swal("请输入排序数字!");
		$("#orders").focus();
		return;
	}
	if (emailSendConfigPage.queryParam.id != null
			|| emailSendConfigPage.queryParam.id != "") {
		managerEmailSendConfigAction.updateEmailSendConfig(
				emailSendConfigPage.queryParam, function(r) {
					if (r[0] == "ok") {
						swal({
							title : "修改成功！",
							text : "已成功修改数据",
							type : "success"
						},
								function() {
									var index = parent.layer
											.getFrameIndex(window.name); // 获取窗口索引
									parent.emailSendConfigPage
											.layerClose(index);
								})
					} else {
						swal(r[1]);
					}
				});
	} else {
		managerEmailSendConfigAction.saveEmailSendConfig(
				emailSendConfigPage.queryParam, function(r) {
					if (r[0] == "ok") {
						swal({
							title : "保存成功！",
							text : "已成功保存数据",
							type : "success"
						},
								function() {
									var index = parent.layer
											.getFrameIndex(window.name); // 获取窗口索引
									parent.emailSendConfigPage
											.layerClose(index);
								})
					} else {
						swal(r[1]);
					}
				});
	}
	emailSendConfigPage.queryParam = {};
}

/**
 * 保存信息
 */
EmailSendConfigPage.prototype.saveData = function() {
	var id = $("#id").val();
	var mailHost = $("#mailHost").val();
	var mailPort = $("#mailPort").val();
	var userName = $("#userName").val();
	var password = $("#password").val();
	var validate = $("#validate").val();
	var sendType = $("#sendType").val();
	var type = $("#type").val();
	var enable = $("#enable").val();
	var orders = $("#orders").val();

	var va = /^[0-9]*$/;// 验证正小数
	if (mailHost == null || mailHost.trim() == "") {
		swal("请输入主机!");
		$("#mailHost").focus();
		return;
	}
	if (mailPort == null || mailPort.trim() == "") {
		swal("请输入接口!");
		$("#mailPort").focus();
		return;
	}
	if (userName == null || userName.trim() == "") {
		swal("请输入名称!");
		$("#userName").focus();
		return;
	}
	if (password == null || password.trim() == "") {
		swal("请输入密码!");
		$("#password").focus();
		return;
	}
	if (validate == null || validate.trim() == "") {
		swal("请输入身份验证!");
		$("#validate").focus();
		return;
	}
	if (sendType == null || sendType.trim() == "") {
		swal("请输入发送协议!");
		$("#sendType").focus();
		return;
	}
	if (type == null || type.trim() == "") {
		swal("请输入发送类型!");
		$("#type").focus();
		return;
	}
	if (enable == null || enable.trim() == "") {
		swal("请输入状态!");
		$("#enable").focus();
		return;
	}
	if (!va.test(orders)) {
		swal("请输入排序数字!");
		$("#orders").focus();
		return;
	}

	var emailSendConfig = {};
	emailSendConfig.id = id;
	emailSendConfig.mailHost = mailHost;
	emailSendConfig.mailPort = mailPort;
	emailSendConfig.userName = userName;
	emailSendConfig.password = password;
	emailSendConfig.validate = validate;
	emailSendConfig.sendType = sendType;
	emailSendConfig.type = type;
	emailSendConfig.enable = enable;
	emailSendConfig.orders = orders;
	managerEmailSendConfigAction.saveEmailSendConfig(emailSendConfig, function(
			r) {
		if (r[0] == "ok") {
			swal({
				title : "保存成功！",
				text : "已成功新增数据",
				type : "success"
			}, function() {
				var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
				parent.emailSendConfigPage.layerClose(index);
			})
		} else {
			swal(r[1]);
		}
	});
};

/**
 * 编辑时赋值
 */
EmailSendConfigPage.prototype.editConfigControl = function(id) {
	managerEmailSendConfigAction.selectEmailSendConfig(id, function(r) {
		if (r[0] != null && r[0] == "ok") {
			$("#id").val(r[1].id);
			$("#mailHost").val(r[1].mailHost);
			$("#mailPort").val(r[1].mailPort);
			$("#userName").val(r[1].userName);
			$("#password").val(r[1].password);
			$("#validate").val(r[1].validate);
			$("#sendType").val(r[1].sendType);
			$("#type").val(r[1].type);
			$("#enable").val(r[1].enable);
			$("#orders").val(r[1].orders);
		} else if (r[0] != null && r[0] == "error") {
			showErrorDlg(jQuery(document.body), r[1]);
		} else {
			showErrorDlg(jQuery(document.body), "获取微邮件配置数据失败.");
		}
	});
};
