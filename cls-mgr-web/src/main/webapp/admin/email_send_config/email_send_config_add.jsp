<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<html>
<head>
    <meta charset="utf-8" />
    <title>邮件发送配置添加</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
<body>
    <div class="wrapper wrapper-content animated fadeIn">
		<form action="javascript:void(0)">
		<%-- <input type="hidden" name="id" id="id" value="<%=id%>"> --%>
			<div class="row">
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">主机：</span>
						<input type="text" name="mailHost" id="mailHost" class="form-control" />
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">接口：</span>
						<input type="text" name="mailPort" id="mailPort" class="form-control" />
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">名称：</span>
						<input type="text" id="userName" name="userName" class="form-control" />
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">密码：</span>
						<input type="text" id="password" name="password" class="form-control" />
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">身份验证：</span>
						<input type="text" id="validate" class="form-control" name="validate" />
					</div>
				</div>
				
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">发送协议：</span>
						<input type="text" id="sendType" class="form-control" name="sendType" />
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">发送类型：</span>
						<input type="text" id="type" class="form-control" name="type" />
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">排序：</span>
						<input type="text" id="orders" class="form-control" name="orders" />
					</div>
				</div>
				<div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">状态：</span>
                        <select class="ipt form-control" id="enable" name="enable">
                            <option value="0" selected="selected">停用</option>
                            <option value="1">启用</option>
                        </select>
                    </div>
                </div>
				<div class="col-sm-12">
						<div class="col-sm-6 text-center">
							<button type="button" class="btn btn-w-m btn-white"
								onclick="emailSendConfigPage.saveData()">提 交</button>
							<input type="reset" class="btn btn-w-m btn-white" value="重 置" />
						</div>
				</div>
			</div>
		</form>
	</div>
	 <script type="text/javascript" src="<%=path%>/admin/common/common.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/email_send_config/js/email_send_config_edit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerEmailSendConfigAction.js'></script>
</body>
</html>

