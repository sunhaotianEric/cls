<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import = "com.team.lottery.enums.ELotteryKind" %>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
<body class="">
<div class="wrapper wrapper-content animated fadeIn">
 <form class="form-horizontal">
	    <div class="row">
	     	<div class="col-sm-5">
	              	<div class="input-group m-b">
	              		<input type="hidden" id="id">
	                     <span class="input-group-addon">选择彩种</span>
	                     <cls:enmuSel name="lotteryName" id="lotteryType" options="class:ipt form-control"  className="com.team.lottery.enums.ELotteryKind"/>
	                     <!-- <select class="ipt form-control" name="lotteryName">
	                          <option value="" selected="selected">==请选择==</option>
	                          <option value="1">安微快3</option>
	                          <option value="1">新加坡2分彩</option>
	                          <option value="1">福建11选5</option>
	                          <option value="1">广东快乐十分</option>
	                         </select> -->
	                 </div>
	         </div>
	    </div> 
        <div class="row">
	        <div class="col-sm-5">
	            <div class="input-group m-b">
	                <span class="input-group-addon">期号</span>
	                <input type="text" value="" class="form-control" id="lotteryNum"></div>
	        </div>
	   </div>
         <div class="row">
	          <div class="col-sm-6">
	             <div class="input-group m-b">
	                 <span class="input-group-addon">开始时间</span>
	                 <div class="input-daterange input-group" id="datepicker">
	                   <input class="form-control layer-date" placeholder="开始日期" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" id="begintime">
	               </div>
	       		</div>
		   	  </div>
         </div>
          <div class="row">
			   	<div class="col-sm-6">
			        <div class="input-group m-b">
			            <span class="input-group-addon">截止时间</span>
			            <div class="input-daterange input-group" id="datepicker">
			                <input class="form-control layer-date" placeholder="结束时间" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" id="endtime">
			            </div>
			        </div>
			   	</div>
          </div>
		<br>
		<br>
		<br>
        <div class="col-sm-10">
            <div class="row">
                <div class="col-sm-6 text-center">
                    <button type="button" class="btn btn-w-m btn-white" onclick="editlotteryIssue.saveLotteryIssue()">提 交</button>
                    <button type="reset" class="btn btn-w-m btn-white">重 置</button>
                </div>
            </div>
        </div>
  </form>
</div>
 <!-- js -->
 <script type="text/javascript" src="<%=path%>/admin/lottery_issue/js/editlottery_issue.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
 <!-- dwr -->
 <script type='text/javascript' src='<%=path%>/dwr/interface/managerLotteryIssueAction.js'></script>
</body>

</html>