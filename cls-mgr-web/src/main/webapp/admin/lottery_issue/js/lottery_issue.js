function LotteryIssuePage(){}
var lotteryIssuePage = new LotteryIssuePage();

lotteryIssuePage.param = {
		
};

/**
 * 查询参数
 */
lotteryIssuePage.queryParam = {
		lotteryType : null,
		lotteryNum : null,
		startime : null,
		endtime : null,
		startime2 : null,
		endtime2 : null
};

//分页参数
lotteryIssuePage.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize
};

$(document).ready(function() {
	lotteryIssuePage.initTableData(); //查询所有的开奖时间数据
});


/**
 * 查询数据
 */

LotteryIssuePage.prototype.getlotteryIssues = function(){
	lotteryIssuePage.queryParam = getFormObj($("#lotteryIssueQuery"));
	lotteryIssuePage.table.ajax.reload();
}

LotteryIssuePage.prototype.initTableData = function(){
	lotteryIssuePage.queryParam = getFormObj($("#lotteryIssueQuery"));
	lotteryIssuePage.table = $('#lotteryIssueTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"ajax":function (data, callback, settings) {
			lotteryIssuePage.pageParam.pageSize = data.length;//页面显示记录条数
			lotteryIssuePage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
			managerLotteryIssueAction.getAllLotteryIssue(lotteryIssuePage.queryParam,lotteryIssuePage.pageParam,function(r){			//封装返回数据
				var returnData = {
						recordsTotal : 0,
						recordsFiltered : 0,
						data : null
					};
					if (r[0] != null && r[0] == "ok") {
						returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
						returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
						returnData.data = r[1].pageContent;//返回的数据列表
					}else if(r[0] != null && r[0] == "error"){
						swal(r[1]);
					}else{
						swal("查询奖金期号数据失败.");
					}
					callback(returnData);
				});
		},
		"columns": [
		            {"data": "lotteryName"},
		            {"data": "lotteryNum"},
		            {"data": "begintimeStr"},
		            {"data":"endtimeStr"},
		            {	 "data": "id",
	                	 render: function(data, type, row, meta) {
	                		var str="";
               			 str += "<a class='btn btn-info btn-sm btn-bj' onclick='lotteryIssuePage.editlotteryIssue("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;";
               			 str += "<a class='btn btn-danger btn-sm btn-del' onclick='lotteryIssuePage.dellotteryIssue("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";
	                		return  str;
	                	 },"bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false
	                }
		            ]
	}).api(); 
}


/**
 * 添加奖金配置数据
 */
LotteryIssuePage.prototype.addLotteryIssueHtml = function(){
	layer.open({
		type: 2,
		title: '新增开奖时间',
		maxmin: false,
		shadeClose: true,
		//点击遮罩关闭层
		area: ['600px', '500px'],
		content:contextPath + '/managerzaizst/lottery_issue/addlottery_issue.html',
	})
}

/**
 * 修改奖金配置数据
 */
LotteryIssuePage.prototype.editlotteryIssue = function(id){
	layer.open({
		type: 2,
		title: '编辑开奖时间',
		maxmin: false,
		shadeClose: true,
		//点击遮罩关闭层
		area: ['600px', '500px'],
		content:contextPath + '/managerzaizst/lottery_issue/'+id+'/editlottery_issue.html',
	})
}

/**
 * 修改倒计时配置数据
 */
LotteryIssuePage.prototype.editlotteryIssueAdjustHtml = function(){
	layer.open({
		type: 2,
		title: '调整倒计时间',
		maxmin: false,
		shadeClose: true,
		//点击遮罩关闭层
		area: ['600px', '500px'],
		content:contextPath + '/managerzaizst/lottery_issue/editlottery_issue_adjust.html',
	})
}

LotteryIssuePage.prototype.dellotteryIssue = function(id){
	   swal({
           title: "您确定要删除这条数据吗",
           text: "请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "删除",
           closeOnConfirm: false
       },
       function(){
    	   managerLotteryIssueAction.delLotteryIssue(id, function(r){
      			if (r[0] != null && r[0] == "ok") {
      				swal({ title: "提示", text: "删除成功",type: "success"});
      				lotteryIssuePage.getlotteryIssues(); //查询奖金配置数据
      			}else if(r[0] != null && r[0] == "error"){
      				swal(r[1]);
      			}else{
      				swal("删除奖金配置数据失败.");
      			}
      	    
    	   });
       })
}

LotteryIssuePage.prototype.closeLayer=function(index){
	layer.close(index);
	lotteryIssuePage.getlotteryIssues(); //查询奖金配置数据
}

