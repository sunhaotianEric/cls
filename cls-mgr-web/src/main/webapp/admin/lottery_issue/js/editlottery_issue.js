function EditlotteryIssue(){};

var editlotteryIssue = new EditlotteryIssue();

editlotteryIssue.param={
		
}

$(document).ready(function(){
	var id = editlotteryIssue.param.id;
	if(typeof(id) != "undefined" || id != null){
		editlotteryIssue.getLotteryIssue(id);
	}
});

EditlotteryIssue.prototype.saveLotteryIssue = function(){
	var lotteryIssue ={}
	lotteryIssue.id = editlotteryIssue.param.id;
	lotteryIssue.lotteryType = $("#lotteryType").val();
	lotteryIssue.lotteryNum = $("#lotteryNum").val();
	lotteryIssue.begintime = $("#begintime").val();
	lotteryIssue.endtime = $("#endtime").val();
	if(lotteryIssue.lotteryType == null || lotteryIssue.lotteryType.trim() == ""){
		swal("请选择彩种");
		$("#lotteryName").focus();
		return;
	}
	if(lotteryIssue.lotteryNum == null || lotteryIssue.lotteryNum.trim() == ""){
		swal("请输入期号");
		$("#lotteryNum").focus();
		return;
	}
	if(lotteryIssue.begintime == null || lotteryIssue.begintime.trim() == ""){
		swal("请选择开始时间");
		$("#startime").focus();
		return;
	}
	if(lotteryIssue.endtime == null || lotteryIssue.endtime.trim() == ""){
		swal("请选择结束时间");
		$("#endTime").focus();
		return;
	}
	lotteryIssue.begintime = new Date(lotteryIssue.begintime);
	lotteryIssue.endtime = new Date(lotteryIssue.endtime);
	managerLotteryIssueAction.saveOrUpdateLotteryIssue(lotteryIssue,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.lotteryIssuePage.closeLayer(index);
		   	    });
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("添加奖金配置数据失败.");
		}
		
	});
}

EditlotteryIssue.prototype.getLotteryIssue = function(id){
	managerLotteryIssueAction.getLotteryIssueByid(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			$("#id").val(r[1].id);
			$("#lotteryType").val(r[1].lotteryType);
			$("#lotteryNum").val(r[1].lotteryNum);
			$("#begintime").val(r[1].begintimeStr);
			$("#endtime").val(r[1].endtimeStr);
			$("#lotteryType").attr("disabled","disabled");  
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取配置信息失败.");
		}
    
	});
}

/**
 * 调整倒计时
 */
EditlotteryIssue.prototype.saveLotteryIssueAdjust = function(){
	var lotteryIssueAdjustVo ={}
	lotteryIssueAdjustVo.lotteryType = $("#lotteryType").val();
	lotteryIssueAdjustVo.adjustmenTime = $("#adjustmenTime").val();
	lotteryIssueAdjustVo.adjustMode = $("#adjustMode").val();
	
	if(lotteryIssueAdjustVo.adjustMode == null || lotteryIssueAdjustVo.adjustMode.trim() == ""){
		swal("调整模式");
		$("#adjustMode").focus();
		return;
	}
	if(lotteryIssueAdjustVo.lotteryType == null || lotteryIssueAdjustVo.lotteryType.trim() == ""){
		swal("请选择彩种");
		$("#lotteryName").focus();
		return;
	}
	if(lotteryIssueAdjustVo.adjustmenTime == null || lotteryIssueAdjustVo.adjustmenTime.trim() == ""){
		swal("请填写调整时间");
		$("#begintime").focus();
		return;
	}
	managerLotteryIssueAction.updateTimeBylotteryType(lotteryIssueAdjustVo,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.lotteryIssuePage.closeLayer(index);
		   	    });
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("调整时间数据失败.");
		}
		
	});
}
