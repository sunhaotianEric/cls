<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import = "com.team.lottery.enums.ELotteryKind" %>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>开奖时间管理</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
   
</head>
<body>
<div class="animated fadeIn">
	       <div class="ibox-content">
	           <div class="row m-b-sm m-t-sm">
	             <form class="form-horizontal" id="lotteryIssueQuery">
	               <div class="row">
	              	 <div class="col-sm-3">
	                   	<div class="input-group m-b">
	                           <span class="input-group-addon">选择彩种</span>
	                           <cls:enmuSel name="lotteryType" id="lotteryType" options="class:ipt form-control"  className="com.team.lottery.enums.ELotteryKind"/>
	                           <!-- <select class="ipt form-control" name="lotteryName">
	                                <option value="" selected="selected">==请选择==</option>
	                                <option value="1">安微快3</option>
	                                <option value="1">新加坡2分彩</option>
	                                <option value="1">福建11选5</option>
	                                <option value="1">广东快乐十分</option>
	                               </select> -->
	                       </div>
	                  </div>
	                  	                 <div class="col-sm-4">
	                     <div class="input-group m-b">
	                        <span class="input-group-addon">开始时间</span>
	                        <div class="input-daterange input-group" id="datepicker">
	                        <input class="form-control layer-date" placeholder="开始日期" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" name="startime" id="startime">
	                      	<span class="input-group-addon">到</span>
	                      	<input class="form-control layer-date" placeholder="结束日期" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" name="endtime" id="endtime" ></div>
	                    </div>
	                </div>
	                <div class="col-sm-4">
	                    <div class="input-group m-b">
	                        <span class="input-group-addon">截止时间</span>
	                        <div class="input-daterange input-group" id="datepicker">
	                            <input class="form-control layer-date" placeholder="开始日期" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" name="startime2" id="startime2">
	                            <span class="input-group-addon">到</span>
	                            <input class="form-control layer-date" placeholder="结束日期" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" name="endtime2" id="endtime2" ></div>
	                    </div>
	                </div>
	                  
	                  <div class="col-sm-3">
	                      <div class="input-group m-b">
	                          <span class="input-group-addon">期号</span>
	                          <input type="text" value="" class="form-control" name ="lotteryNum" id="lotteryNum"></div>
	                  </div>
	                  <div class="col-sm-4">
	                    <div class="form-group text-left ml0">
	                        <button type="button" class="btn btn-outline btn-default btn-select" onclick="lotteryIssuePage.getlotteryIssues()"><i class="fa fa-search"></i>&nbsp;查 询</button>
	                         <c:if test="${admin.bizSystem == 'SUPER_SYSTEM'}">
	                        	<button type="button" class="btn btn-outline btn-default btn-select bz" onclick="lotteryIssuePage.addLotteryIssueHtml()"><i class="fa fa-plus"></i>&nbsp;添加</button>
	                   			<button type="button" class="btn btn-outline btn-default btn-select bz" onclick="lotteryIssuePage.editlotteryIssueAdjustHtml()"><i class="fa fa-adjust"></i>&nbsp;调整倒计时</button>
	                   		</c:if>
	                    </div>
	             	</div>
	            </div>
	        </form>
	    </div>
	    <div class="jqGrid_wrapper">
	        <div class="ui-jqgrid ">
	            <div class="ui-jqgrid-view">
	                <div class="ui-jqgrid-hdiv">
	                    <div class="ui-jqgrid-hbox" style="padding-right:0">
	                        <table class="ui-jqgrid-htable ui-common-table table table-bordered" id="lotteryIssueTable">
	                            <thead>
	                                <tr class="ui-jqgrid-labels">
	                                    <th class="ui-th-column ui-th-ltr clo-sm-3">彩种</th>
	                                    <th class="ui-th-column ui-th-ltr clo-sm-3">期号</th>
	                                    <th class="ui-th-column ui-th-ltr clo-sm-3">开始下注时间</th>
	                                    <th class="ui-th-column ui-th-ltr clo-sm-3">截止下注时间</th>
	                                    <th class="ui-th-column ui-th-ltr clo-sm-3">操作</th>
	                                 </tr>
	                            </thead>
	                            <tbody>
	                              <!--   <tr role="row" tabindex="-1" class="jqgrow ui-row-ltr">
	                                    <td>韩国1.5分彩</td>
	                                    <td>000</td>
	                                    <td>2015/01/17 00:00:00</td>
	                                    <td>2015/01/17 00:00:15</td></tr>
	                                <tr role="row" tabindex="-1" class="jqgrow ui-row-ltr">
	                                    <td>韩国1.5分彩</td>
	                                    <td>000</td>
	                                    <td>2015/01/17 00:00:00</td>
	                                    <td>2015/01/17 00:00:15</td></tr>
	                                <tr role="row" tabindex="-1" class="jqgrow ui-row-ltr">
	                                    <td>韩国1.5分彩</td>
	                                    <td>000</td>
	                                    <td>2015/01/17 00:00:00</td>
	                                    <td>2015/01/17 00:00:15</td></tr>
	                                <tr role="row" tabindex="-1" class="jqgrow ui-row-ltr">
	                                    <td>韩国1.5分彩</td>
	                                    <td>000</td>
	                                    <td>2015/01/17 00:00:00</td>
	                                    <td>2015/01/17 00:00:15</td></tr> -->
	                            </tbody>
	                        </table>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>
 <!-- js -->
 <script type="text/javascript" src="<%=path%>/admin/lottery_issue/js/lottery_issue.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
 <!-- dwr -->
 <script type='text/javascript' src='<%=path%>/dwr/interface/managerLotteryIssueAction.js'></script>
</body>
</html>

