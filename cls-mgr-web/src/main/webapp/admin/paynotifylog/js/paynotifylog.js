function PayNotifyLogPage(){}
var payNotifyLogPage = new PayNotifyLogPage();

payNotifyLogPage.param = {
   	
};

payNotifyLogPage.admin = {
	   	
};
/**
 * 查询参数
 */
payNotifyLogPage.queryParam = {

};

//分页参数
payNotifyLogPage.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize
};


$(document).ready(function() {

	payNotifyLogPage.initTableData(); //初始化查询数据
	
});

/**
 * 初始化列表数据
 */
PayNotifyLogPage.prototype.initTableData = function() {
	payNotifyLogPage.queryParam = getFormObj($("#queryForm"));
	payNotifyLogPage.table = $('#paynotifylogTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"scrollX": true,
		"iDisplayLength" : defaultPageSize,
		"ajax":function (data, callback, settings) {
			payNotifyLogPage.pageParam.pageSize = data.length;//页面显示记录条数
			payNotifyLogPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerPayNotifyLogAction.getAllPayNotifyLog(payNotifyLogPage.queryParam,payNotifyLogPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": "bizSystemName"},
		            {"data": "thirdTypeDesc"},
		            {"data": "requestMethod"},
		            {"data": "contentType"},
		            {"data": "contentLength"},
		            {"data": "messageContent",render: function(data, type, row, meta) {
	            		if (data!=null && data.length > 15) {
	            			var title = data.substring(0,15) + "...";
	            			return "<span title = '"+data+"'>"+title+"</span>";
	            		}
	            		return data;
            		}},
            		{"data": "headerNames",render: function(data, type, row, meta) {
	            		if (data!=null && data.length > 15) {
	            			var title = data.substring(0,15) + "...";
	            			return "<span title = '"+data+"'>"+title+"</span>";
	            		}
	            		return data;
            		}},
            		{"data": "parameterNames",render: function(data, type, row, meta) {
	            		if (data!=null && data.length > 15) {
	            			var title = data.substring(0,15) + "...";
	            			return "<span title = '"+data+"'>"+title+"</span>";
	            		}
	            		return data;
            		}},
		            {"data": "requestIp"},
		            {   
		            	"data": "createdDate",
		            	render: function(data, type, row, meta) {
		            		return dateFormat(data,'yyyy-MM-dd hh:mm:ss');
	            		}
		            }
		         
		 
		           
	            ]
	}).api(); 
}




/**
 * 查询数据
 */
PayNotifyLogPage.prototype.findPayNotifyLog = function(){
	payNotifyLogPage.queryParam=getFormObj("#queryForm");
	payNotifyLogPage.table.ajax.reload();
};


