<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>  
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>商户信息管理</title>
        <jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
    <body>
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
                    <div class="ibox ">
                        <div class="ibox-content">
                            <div class="row m-b-sm m-t-sm">
                                <form class="form-horizontal" id="queryForm">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">商户</span>
                                                 <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
                                   
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                           <div class="input-group m-b">
                                                <span class="input-group-addon">第三方类型</span>
                                                 <cls:thirdPaySel name="thirdType" id="thirdType" options="class:ipt form-control" />
                                           </div>
                                        </div>
									<div class="col-sm-4">
										<div class="input-group m-b">
											<span class="input-group-addon">充值时间</span> <input
												class="form-control layer-date" placeholder="开始时间"
												onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"
												name="createdDateStart" id="createdDateStart"> <span
												class="input-group-addon">到</span> <input
												class="form-control layer-date" placeholder="结束日期"
												onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"
												name="createdDateEnd" id="createdDateEnd">
										</div>
									</div>

									<div class="col-sm-2">
                                            <div class="form-group nomargin text-right">
                                                <button type="button" class="btn btn-outline btn-default btn-kj" onclick="payNotifyLogPage.findPayNotifyLog()"><i class="fa fa-search"></i>&nbsp;查 询</button></div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="jqGrid_wrapper">
                                <div class="ui-jqgrid ">
                                    <div class="ui-jqgrid-view">
                                        <div class="ui-jqgrid-hdiv">
                                            <div class="ui-jqgrid-hbox" style="padding-right:0">
                                                <table class="ui-jqgrid-htable ui-common-table table table-bordered" id="paynotifylogTable">
                                                    <thead>
                                                        <tr class="ui-jqgrid-labels">
                                                            <th class="ui-th-column ui-th-ltr">商户</th>
                                                            <th class="ui-th-column ui-th-ltr">第三方类型</th>
                                                            <th class="ui-th-column ui-th-ltr">请求方法</th>
                                                            <th class="ui-th-column ui-th-ltr">请求content_type</th>
                                                            <th class="ui-th-column ui-th-ltr">请求content_length</th>
                                                            <th class="ui-th-column ui-th-ltr">报文内容</th>
                                                            <th class="ui-th-column ui-th-ltr">请求头部集合</th>
                                                            <th class="ui-th-column ui-th-ltr">请求参数集合</th>
                                                            <th class="ui-th-column ui-th-ltr">请求IP</th>
                                                            <th class="ui-th-column ui-th-ltr">创建时间</th>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!-- js -->
<script type="text/javascript"
	src="<%=path%>/admin/paynotifylog/js/paynotifylog.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript'
	src='<%=path%>/dwr/interface/managerPayNotifyLogAction.js'></script>
</body>
</html>

