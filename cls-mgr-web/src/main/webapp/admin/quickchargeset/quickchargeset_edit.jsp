<%@page import="com.team.lottery.system.SystemConfigConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" 
	import = "com.team.lottery.enums.EBankInfo" 
	pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>快捷支付设置</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
	<%
	 String id = request.getParameter("id");  //获取查询的商品ID
	%>
	
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeIn">
  <form action="javascript:void(0)">
    <div class="row">
    <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
    <div class="col-sm-12">
    <div class="input-group m-b">
    <span class="input-group-addon">商户：</span>
    <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
    </div>
    </div>
    </c:if>
    <div class="col-sm-12">
    <div class="input-group m-b">
    <span class="input-group-addon">第三方支付名称：</span>
    <input type="hidden" id="id"/>
     <cls:thirdPaySel name="chargeType" id="chargeType" options="class:ipt form-control" />
    </div>
    </div>
   <div class="col-sm-12">
    <div class="input-group m-b">
    <span class="input-group-addon">商户号：</span>
    <input type="text" value="" class="form-control" name="memberId" id="memberId">
    </div>
    </div>
    <div class="col-sm-12" id="signDiv">
    <div class="input-group m-b">
    <span class="input-group-addon">签名：</span>
    <input type="text" value="" class="form-control" name="sign" id="sign">
    </div>
    </div>
    <div class="col-sm-12">
    <div class="input-group m-b">
    <span class="input-group-addon">终端号：</span>
    <input type="text" value="" class="form-control" name=terminalId id="terminalId">
    </div>
    </div>
    <div class="col-sm-12" id="privatekeyDiv" style="display:none">
    <div class="input-group m-b">
    <span class="input-group-addon">RSA私钥：</span>
    <input type="text" value="" class="form-control" name="privatekey" id="privatekey">
    </div>
    </div>
    <div class="col-sm-12" id="publickeyDiv" style="display:none">
    <div class="input-group m-b">
    <span class="input-group-addon">RSA公钥：</span>
    <input type="text" value="" class="form-control" name="publickey" id="publickey">
    </div>
    </div>
    <div class="col-sm-12">
    <div class="input-group m-b">
    <span class="input-group-addon">是否启用：</span>
    <select class="form-control" id="enabled">
	   				<option value="1">是</option>
	   				<option value="0">否</option>
	   			</select>
    </div>
    </div>
    <div class="col-sm-12">
     <div class="row">
     <div class="col-sm-6 text-center">
     <button type="button" class="btn btn-w-m btn-white" onclick="domainEditPage.saveData()">提 交</button></div>
      <div class="col-sm-6 text-center">
      <input type="reset" class="btn btn-w-m btn-white" value="重 置"/>
        </div>
       </div>
      </div>
    </div>
    
    
   
      </form>
  </div>
  
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/quickchargeset/js/quickchargeset_edit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerChargePayAction.js'></script>
    <script type="text/javascript">
	domainEditPage.param.id = <%=id %>;
		var imgServerUrl = '<%= SystemConfigConstant.imgServerUrl %>';
	</script>
</body>
</html>

