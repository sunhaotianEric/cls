<%@ page language="java" contentType="text/html; charset=UTF-8" 
	import = "com.team.lottery.enums.EBankInfo,com.team.lottery.system.SystemConfigConstant" 
	pageEncoding="UTF-8"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<html>
<head>
    <meta charset="utf-8" />
    <title>快捷支付设置</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/quickchargeset/js/quickchargeset_edit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerChargePayAction.js'></script>
    
    <link rel="stylesheet" href="<%=path%>/resources/kindeditor-4.1.10/themes/default/default.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" />
	<script charset="utf-8" src="<%=path%>/resources/kindeditor-4.1.10/kindeditor-min.js"></script>
	<script charset="utf-8" src="<%=path%>/resources/kindeditor-4.1.10/lang/zh_CN.js"></script>
	
	<script type="text/javascript">
		var imgServerUrl = '<%= SystemConfigConstant.imgServerUrl %>';
	</script>
</head>
<body>
   <div class="alert alert-info">当前位置<b class="tip"></b>网站信息管理<b class="tip"></b>快捷支付设置</div>
   	
   	<%-- <table class="tb">
         <tr>
         	<td> <a href="<%=path %>/managerzaizst/paybank.html">银行卡管理</a> &nbsp;&nbsp; <a href="<%=path %>/managerzaizst/paybank/paybank_add.html">添加银行卡</a></td>
         </tr>
     </table> --%>
    <form action="javascript:void(0)">
	    <table class="tb">
	    
	   		<!-- <tr>
	   			<td width="10%">管理员：</td>
	   			<td><input type="hidden" id="paybankId"/><select class="select" id="adminname"></select></td>
	   		</tr> -->
	   		<c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
	   		<tr>
	   			<td>商户：</td>
	   			<td><cls:bizSel name="bizSystem" id="bizSystem"/></td>
	   		</tr>
	   		</c:if>
	   		<tr>
	   			<td>第三方支付名称：</td>
	   			<td>
	   			 <cls:thirdPaySel name="chargeType" id="chargeType" options="class:ipt form-control" />
	   			</td>
	   		</tr>
	   		<tr>
	   			<td>商户号：</td>
	   			<td><input id="memberId" type="text" size="35"/></td>
	   		</tr>
	   		<tr>
	   			<td>签名：</td>
	   			<td><input id="sign" type="text" size="35"/></td>
	   		</tr>
	   		<tr>
	   			<td>充值最低金额：</td>
	   			<td><input id="lowestValue" type="text" size="35"/></td>
	   		</tr>
	   		<tr>
	   			<td>充值最高金额：</td>
	   			<td><input id="highestValue" type="text" size="35"/></td>
	   		</tr>
	         <tr>
	   			<td>是否启用：</td>
	   			<td>
	   			<select class="select" id="enabled">
	   				<option value="1">启用</option>
	   				<option value="0">停用</option>
	   			</select>
	   			</td>
	   		</tr>
	   		<tr>
	   			<td></td>
	   			<td><input type="button" onclick="domainEditPage.saveChargePay()" value="提交"/>&nbsp;&nbsp;&nbsp;&nbsp;<input type="reset" value="重置"/></td>
	   		</tr>
	    </table>
    </form>
</body>
</html>

