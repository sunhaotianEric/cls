function DomainEditPage(){}
var domainEditPage = new DomainEditPage();

domainEditPage.param = {
   	
};
var initFinsh = false;

$(document).ready(function() {
	domainEditPage.initPage();
	var domainId = domainEditPage.param.id;  //获取查询的ID
	if(domainId != null){
		domainEditPage.editChargePay(domainId);
	}
$("#chargeType").change(function(){
	if($(this).val()=="RONGCAN" || $(this).val()=="KANGFUTONG"||$(this).val()=="MSPAY" 
		|| $(this).val()=="BEIFUBAOPAY" || $(this).val()=="HUJINGPAY" || $(this).val()=="HUJINDPAY"
			|| $(this).val()=="ANSHENG" || $(this).val()=="HIPAY3721" || $(this).val()=="LEMEI" || $(this).val()=="YOUFUPAY"){
		$("#sign").val("1");
		$("#signDiv").hide();
		$("#publickeyDiv").show();
		$("#privatekeyDiv").show();
	}else if($(this).val()=="JIUTONG"){
		$("#publickeyDiv").show();
		$("#privatekeyDiv").show();
	}else if($(this).val()== "XINFAPAY") {
		$("#sign").val("1");
		$("#signDiv").show();
		$("#publickeyDiv").show();
		$("#privatekeyDiv").show();
	}else{
		$("#sign").val("");
		$("#signDiv").show();
		$("#publickeyDiv").hide();
		$("#privatekeyDiv").hide();
	}
})
	
});

/**
 * 保存快捷支付
 */
DomainEditPage.prototype.saveData = function(){
	var chargePay = {};
	chargePay.id = $("#id").val();
	chargePay.memberId = $("#memberId").val();
	chargePay.bizSystem = $("#bizSystem").val();
	chargePay.chargeType = $("#chargeType").val();
	chargePay.sign = $("#sign").val();
	chargePay.enabled = $("#enabled").val();
	chargePay.privatekey = $("#privatekey").val();
	chargePay.publickey = $("#publickey").val();
	chargePay.terminalId = $("#terminalId").val();
	/*payBank.createTime = $("#createTime").val();*/
	if(currentUser.bizSystem=='SUPER_SYSTEM'){
		if (chargePay.bizSystem==null || chargePay.bizSystem.trim()=="") {
			swal("请选择业务系统!");
			$("#bizSystem").focus();
			return;
		} 
	}
	
	if (chargePay.chargeType==null || chargePay.chargeType.trim()=="") {
		swal("请选择充值类型!");
		$("#chargeType").focus();
		return;
	}
	if (chargePay.sign==null || chargePay.sign.trim()=="") {
		swal("请输入签名!");
		$("#sign").focus();
		return;
	}
	if (chargePay.enabled==null || chargePay.enabled.trim()=="") {
		swal("请选择状态!");
		$("#enabled").focus();
		return;
	}
	if (chargePay.memberId==null || chargePay.memberId.trim()=="") {
		swal("请输入商户号!");
		$("#memberId").focus();
		return;
	}
	/*if (chargePay.lowestValue==null || chargePay.lowestValue.trim()=="") {
		swal("请输入最低金额!");
		$("#lowestValue").focus();
		return;
	}
	if( isNaN(chargePay.lowestValue)){
		swal("最低金额必须为数字!");
		$("#lowestValue").focus();
		return;
	}
	if (chargePay.highestValue==null || chargePay.highestValue.trim()=="") {
		swal("请输入最高金额!");
		$("#highestValue").focus();
		return;
	}
	if( isNaN(chargePay.highestValue)){
		swal("最高金额必须为数字!");
		$("#highestValue").focus();
		return;
	}*/
	managerChargePayAction.saveOrUpdateChargePay(chargePay,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.domainPage.closeLayer(index);
		   	    }); 
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("保存快捷支付设置失败!");
		}
    });
};

/**
 * 初始化页面
 */
DomainEditPage.prototype.initPage = function(id){
	$("#enabled").val("1");
};

/**
 * 快捷支付编辑
 */
DomainEditPage.prototype.editChargePay = function(id){
	managerChargePayAction.getChargePayById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			var pay = r[1];
			$("#id").val(pay.id);
			$("#bizSystem").val(pay.bizSystem);
			$("#chargeType").val(pay.chargeType);
			$("#memberId").val(pay.memberId);
			$("#sign").val(pay.sign);
			$("#enabled").val(pay.enabled);
			if(pay.chargeType=="RONGCAN" || pay.chargeType=="KANGFUTONG" ||pay.chargeType=="MSPAY" 
				|| pay.chargeType=="BEIFUBAOPAY" || pay.chargeType=="HUJINGPAY" || pay.chargeType=="HUJINDPAY"
				|| pay.chargeType=="ANSHENG" || pay.chargeType=="HIPAY3721" || pay.chargeType=="LEMEI" || pay.chargeType=="YOUFUPAY"){
				$("#privatekey").val(pay.privatekey);
				$("#publickey").val(pay.publickey);
				$("#publickeyDiv").show();
				$("#privatekeyDiv").show();
				$("#signDiv").hide();
			} else if(pay.chargeType=="XINFAPAY") {
				$("#privatekey").val(pay.privatekey);
				$("#publickey").val(pay.publickey);
				$("#sign").val(pay.sign);
				$("#publickeyDiv").show();
				$("#privatekeyDiv").show();
				$("#signDiv").show()
			} else if(pay.chargeType=="JIUTONG"){
				$("#privatekey").val(pay.privatekey);
				$("#publickey").val(pay.publickey);
				$("#publickeyDiv").show();
				$("#privatekeyDiv").show();
				$("#signDiv").show();
			}
			
			$("#terminalId").val(pay.terminalId);
		}else if(r[0] != null && r[0] == "error"){
			showErrorDlg(jQuery(document.body), r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "获取快捷支付信息失败.");
		}
    });
};