function DomainPage(){}
var domainPage = new DomainPage();

domainPage.param = {
		
};
//分页参数
domainPage.pageParam = {
		    pageNo : 1,
		    pageSize : defaultPageSize
};
/**
 * 查询参数
 */
domainPage.queryParam = {
		bizSystem : null,
		chargeType : null
};
$(document).ready(function() {
	domainPage.initTableData();//查询所有的快捷支付数据
});


DomainPage.prototype.initTableData = function(){

	domainPage.queryParam = getFormObj($("#quickchargesetForm"));
	domainPage.table = $('#quickchargesetTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"bPaginate" : false, 
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			/*domainPage.pageParam.pageSize = data.length;//页面显示记录条数
			domainPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码*/
	    	managerChargePayAction.getAllChargepays(domainPage.queryParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					/*returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
*/					returnData.data = r[1];//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询所有的快捷支付数据失败.");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data":null},
		            {"data": "bizSystemName", 
		            	"bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "chargeTypeStr"},
		            {"data": "memberId"},
		            {
		            	"data": "enabled",
		            	render: function(data, type, row, meta) {
		            		if (data == "1") {
		            			return '是';
		            		} else {
		            			return "<span style='color: red;'>否</span>";
		            		}
		            		return data;
	            		}
		            },
		            {
		            	"data": "supportHttps",
		            	render: function(data, type, row, meta) {
		            		if (data == "1") {
		            			return '是';
		            		} else {
		            			return "<span style='color: red;'>否</span>";
		            		}
		            		return data;
	            		}
		            },
		            {
	                	 "data": "id",
	                	 render: function(data, type, row, meta) {
	                		 var str="<a class='btn btn-info btn-sm btn-bj' onclick='domainPage.editChargepay("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;" +
	             		 		"<a class='btn btn-danger btn-sm btn-del' onclick='domainPage.delChargepay("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";
	                		 if(row.enabled=="1")
	                			 {
	                			 str="<a class='btn btn-warning btn-sm ' onclick='domainPage.closeChargepay("+data+")'><i class='glyphicon glyphicon-remove' ></i>&nbsp;停用</a>&nbsp;&nbsp;"+str;
	                			 }
	                		 else
	                			 {
	                			 str="<a class='btn btn-sm btn-success' onclick='domainPage.openChargepay("+data+")'><i class='glyphicon glyphicon-ok'></i>&nbsp;启用</a>&nbsp;&nbsp;"+str;
	                			 }
	                			 
	                		 return str ;
	                	 }
	                }
	            ]
	}).api();
	$("#quickchargesetTable_info").hide();
};

/**
 * 查询数据
 */
DomainPage.prototype.findChargepay = function(){
	var bizSystem = $("#bizSystem").val();
	var chargeType = $("#chargeType").val();
	if(bizSystem == ""){
		domainPage.queryParam.bizSystem = null;
	}else{
		domainPage.queryParam.bizSystem = bizSystem;
	}
	if(chargeType == ""){
		domainPage.queryParam.chargeType = null;
	}else{
		domainPage.queryParam.chargeType = chargeType;
	}
	domainPage.table.ajax.reload();
};




/**
 * 删除
 */
DomainPage.prototype.delChargepay = function(id){
	
	   swal({
           title: "您确定要删除这条信息吗",
           text: "请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "删除",
           closeOnConfirm: false
       },
       function() {
    	   managerChargePayAction.delChargePay(id,function(r){
   			if (r[0] != null && r[0] == "ok") {
   				swal({ title: "提示", text: "删除成功",type: "success"});
   				domainPage.table.ajax.reload();
   			}else if(r[0] != null && r[0] == "error"){
   				swal(r[1]);
   			}else{
   				swal("删除信息失败.");
   			}
   	    });
       });
};

/**
 * 新增
 */
DomainPage.prototype.addChargepay=function()
{
	
	 layer.open({
         type: 2,
         title: '快捷支付新增',
         maxmin: false,
         shadeClose: true,
         //点击遮罩关闭层
         area: ['70%', '80%'],
         content: contextPath + "/managerzaizst/quickchargeset/quickchargeset_edit.html",
         cancel: function(index){ 
      	   domainPage.table.ajax.reload();
      	   }
     });
}
/**
 * 编辑
 */
DomainPage.prototype.editChargepay=function(id)
{
	
	  layer.open({
           type: 2,
           title: '快捷支付编辑',
           maxmin: false,
           shadeClose: true,
           //点击遮罩关闭层
           area: ['70%', '80%'],
           content: contextPath + "/managerzaizst/quickchargeset/"+id+"/quickchargeset_edit.html",
           cancel: function(index){ 
        	   domainPage.table.ajax.reload();
        	   }
       });
}

DomainPage.prototype.closeLayer=function(index){
	layer.close(index);
	domainPage.table.ajax.reload();
}

/**
 * 开启商户号的支付
 */
DomainPage.prototype.openChargepay = function(id){
	
		managerChargePayAction.openChargepay(id,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "操作成功",type: "success"});
				domainPage.table.ajax.reload();
			}else if(r[0] != null && r[0] == "error"){
				showErrorDlg(jQuery(document.body), r[1]);
			}else{
				showErrorDlg(jQuery(document.body), "启用商户号的支付失败.");
			}
	    });
	
};

/**
 * 关闭商户号的支付
 */
DomainPage.prototype.closeChargepay = function(id){
	
		managerChargePayAction.closeChargepay(id,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "操作成功",type: "success"});
				domainPage.table.ajax.reload();
			}else if(r[0] != null && r[0] == "error"){
				showErrorDlg(jQuery(document.body), r[1]);
			}else{
				showErrorDlg(jQuery(document.body), "关闭商户号的支付失败.");
			}
	    });
	
};

