<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>流水返送订单</title>
<jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
<body>
<div class="animated fadeIn">
	<div class="ibox-content">
		<div class="row m-b-sm m-t-sm">
			<form class="form-horizontal" id="dayConsumeActivityOrderForm">
                <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
					<div class="row">
							<div class="col-sm-2 biz" >
		                       <div class="input-group m-b">
		                           <span class="input-group-addon">商户</span>
		                            <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
		                       </div>
		                   </div>
						<div class="col-sm-2">
							<div class="input-group m-b">
								<span class="input-group-addon">订单号</span>
								<input type="text"
									value="" name="serialNumber" id="serialNumberId" class="form-control">
							</div>
						</div>
						<div class="col-sm-2">
							<div class="input-group m-b">
								<span class="input-group-addon">用户名</span>
								<input type="text"
									value="" name="userName" id="userNameId" class="form-control">
							</div>
						</div>
						<div class="col-sm-2">
							<div class="input-group m-b">
								<span class="input-group-addon">发放状态</span>
								<cls:enmuSel name="releaseStatus" className="com.team.lottery.enums.EDayConsumeActivityOrderStatus" options="class:ipt form-control" id="releaseStatusId" />
							</div>
						</div>
						<div class="col-sm-2">
							<div class="input-group m-b">
								<span class="input-group-addon">归属日期</span>
								 <input id="belongDate" name="belongDate" class="form-control layer-date" placeholder="归属日期" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})">
							</div>
						</div>
						<div class="col-sm-1">
							<div class="form-group  text-right">
								<button type="button" class="btn btn-outline btn-default btn-kj"
									onclick="dayConsumeActivityOrderPage.findDayConsumeActivityConfig()">
									<i class="fa fa-search"></i>&nbsp;查 询
								</button>
							</div>
						</div>
						<div class="col-sm-1">
							<div class="form-group  text-right">
								<button type="button" class="btn btn-outline btn-default btn-kj"
									onclick="dayConsumeActivityOrderPage.onekeyGiveActityMoney()">
									<i class="fa fa-star"></i>&nbsp;一键发放
								</button>
							</div>
						</div>
					</div>
				</c:if>
                <c:if test="${admin.bizSystem !='SUPER_SYSTEM'}">
					<div class="row">
						<div class="col-sm-3">
							<div class="input-group m-b">
								<span class="input-group-addon">订单号</span>
								<input type="text"
									value="" name="serialNumber" id="serialNumberId" class="form-control">
							</div>
						</div>
						<div class="col-sm-2">
							<div class="input-group m-b">
								<span class="input-group-addon">用户名</span>
								<input type="text"
									value="" name="userName" id="userNameId" class="form-control">
							</div>
						</div>
						<div class="col-sm-2">
							<div class="input-group m-b">
								<span class="input-group-addon">发放状态</span>
								<cls:enmuSel name="releaseStatus" className="com.team.lottery.enums.EDayConsumeActivityOrderStatus" options="class:ipt form-control" id="releaseStatusId" />
							</div>
						</div>
						<div class="col-sm-3">
							<div class="input-group m-b">
								<span class="input-group-addon">归属日期</span>
								 <input id="belongDate" name="belongDate" class="form-control layer-date" placeholder="归属日期" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})">
							</div>
						</div>
						<div class="col-sm-1">
							<div class="form-group  text-right">
								<button type="button" class="btn btn-outline btn-default btn-kj"
									onclick="dayConsumeActivityOrderPage.findDayConsumeActivityConfig()">
									<i class="fa fa-search"></i>&nbsp;查 询
								</button>
							</div>
						</div>
						<div class="col-sm-1">
							<div class="form-group  text-right">
								<button type="button" class="btn btn-outline btn-default btn-kj"
									onclick="dayConsumeActivityOrderPage.onekeyGiveActityMoney()">
									<i class="fa fa-star"></i>&nbsp;一键发放
								</button>
							</div>
						</div>
					</div>
               </c:if>
               <div class="row">
               <div class="col-sm-1">
							<div class="form-group  text-right">
								<button type="button" class="btn btn-outline btn-default btn-kj"
									onclick="dayConsumeActivityOrderPage.dayConsumeActityOrderInfoExport()">
									<i class="fa fa-search"></i>&nbsp;导出数据
								</button>
							</div>
						</div>
               </div>
			</form>
		</div>
		<div class="jqGrid_wrapper">
			<div class="ui-jqgrid ">
				<div class="ui-jqgrid-view ">
					<div class="ui-jqgrid-hdiv">
						<div class="ui-jqgrid-hbox" style="padding-right: 0">
							<table id="dayConsumeActivityOrderTable"
								class="ui-jqgrid-htable ui-common-table table table-bordered">
								<thead>
									<tr class="ui-jqgrid-labels">
										<th class="ui-th-column ui-th-ltr">序号</th>
										<th class="ui-th-column ui-th-ltr">商户</th>
										<th class="ui-th-column ui-th-ltr">订单号</th>
										<th class="ui-th-column ui-th-ltr">用户名</th>
										<th class="ui-th-column ui-th-ltr">领取金额</th>
										<th class="ui-th-column ui-th-ltr">投注金额</th>
										<th class="ui-th-column ui-th-ltr">分分彩投注额</th>
										<th class="ui-th-column ui-th-ltr">分分彩领取金额</th>
										<th class="ui-th-column ui-th-ltr">归属日期</th>
										<th class="ui-th-column ui-th-ltr">发放处理状态</th>
										<th class="ui-th-column ui-th-ltr">分分彩发放处理状态</th>
										<th class="ui-th-column ui-th-ltr">申请时间</th>
										<th class="ui-th-column ui-th-ltr">操作</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	<!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/day_consume_activity_order/js/day_consume_activity_order.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerDayConsumeActityOrderAction.js'></script>
	<script type='text/javascript' src='<%=path%>/dwr/interface/managerUserAction.js'></script>
</body>
</html>

