function EditDayConsumeActivityOrderPage(){}
var editDayConsumeActivityOrderPage = new EditDayConsumeActivityOrderPage();

editDayConsumeActivityOrderPage.param = {
   	
};

$(document).ready(function() {
	var id = editDayConsumeActivityOrderPage.param.id;  //获取查询的帮助ID
	if(id != null){
		editDayConsumeActivityOrderPage.editDayConsumeActivityOrder(id);
	}
});


/**
 * 保存帮助信息
 */
EditDayConsumeActivityOrderPage.prototype.saveData = function(){
	
	var id = $("#id").val();
	var bizSystem = $("#bizSystem").val();
	var lotteryMoney = $("#lotteryMoneyId").val();
	var money = $("#moneyId").val();
	
	if(currentUser.bizSystem == "SUPER_SYSTEM"){
		if(bizSystem==null || bizSystem.trim()==""){
			swal("请选择业务系统!");
			$("#bizSystem").focus();
			return;
		}
	}else{
		bizSystem = currentUser.bizSystem;
	}
	if(lotteryMoney==null || lotteryMoney.trim()==""){
		swal("请输入投注额!");
		$("#lotteryNumId").focus();
		return;
	}
	if(money==null || money.trim()==""){
		swal("请输入活动赠送金额!");
		editor.focus();
		return;
	}
	
	var dayConsumeActityOrder = {};
	dayConsumeActityOrder.id = id;
	dayConsumeActityOrder.bizSystem = bizSystem;
	dayConsumeActityOrder.lotteryMoney = lotteryMoney;
	dayConsumeActityOrder.money = money;
	managerDayConsumeActityOrderAction.updateDayConsumeActityOrder(dayConsumeActityOrder,function(r){
		if (r[0] != null && r[0] == "ok") {
			var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
			parent.dayConsumeActivityOrderPage.closeLayer(index);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("编辑流水返送信息失败.");
		}
    });
};


/**
 * 赋值帮助信息
 */
EditDayConsumeActivityOrderPage.prototype.editDayConsumeActivityOrder = function(id){
	managerDayConsumeActityOrderAction.getDayConsumeActityOrderById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			$("#id").val(r[1].id);
			$("#bizSystem").val(r[1].bizSystem);
			$("#bizSystem").attr("disabled","disabled");
			$("#lotteryMoneyId").val(r[1].lotteryMoney);
			$("#moneyId").val(r[1].money);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("流水返送信息失败.");
		}
    });
};