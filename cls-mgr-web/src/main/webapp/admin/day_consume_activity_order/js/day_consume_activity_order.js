function DayConsumeActivityOrderPage(){}
var dayConsumeActivityOrderPage = new DayConsumeActivityOrderPage();

dayConsumeActivityOrderPage.param = {
		ffcDisplay:true
};
//分页参数
dayConsumeActivityOrderPage.pageParam = {
		    pageNo : 1,
		    pageSize : defaultPageSize
};
/**
 * 查询参数
 */
dayConsumeActivityOrderPage.queryParam = {
		bizSystem:null,
		belongDate:null,
		serialNumber:null,
		releaseStatus:null,
		userName:null
		
};


$(document).ready(function() {
	//归属日期默认昨天
	var belongDate= new Date().AddDays(-1).format("yyyy-MM-dd");
	$("#belongDate").val(belongDate);
	if( currentUser.bizSystem!='SUPER_SYSTEM'){
		dayConsumeActivityOrderPage.getBizSystemByDayConsumeActivityConfig();
	}else{
		dayConsumeActivityOrderPage.initTableData();
	}
	
	
});

DayConsumeActivityOrderPage.prototype.getBizSystemByDayConsumeActivityConfig = function(){
	var query={}
	query.bizSystem=currentUser.bizSystem;
	query.activityType="FFC";
	managerDayConsumeActityOrderAction.getBizSystemByDayConsumeActivityConfig(query,function(r){
			if (r[0] != null && r[0] == "ok") {
				if(r[1]==null){
					dayConsumeActivityOrderPage.param.ffcDisplay=false;
				}else{
					dayConsumeActivityOrderPage.param.ffcDisplay=true;
				}
				dayConsumeActivityOrderPage.initTableData();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("获取数据失败");
			}
	    });
}

DayConsumeActivityOrderPage.prototype.initTableData = function(){

	dayConsumeActivityOrderPage.queryParam = getFormObj($("#dayConsumeActivityOrderForm"));
	dayConsumeActivityOrderPage.table = $('#dayConsumeActivityOrderTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			dayConsumeActivityOrderPage.pageParam.pageSize = data.length;//页面显示记录条数
			dayConsumeActivityOrderPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerDayConsumeActityOrderAction.getDayConsumeActityOrderPage(dayConsumeActivityOrderPage.queryParam,dayConsumeActivityOrderPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
				returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询所有的快捷支付数据失败.");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": null},
		            {"data": "bizSystemName",
		            	"bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "serialNumber"},
		            {"data":"userName",
		            	render: function(data, type, row, meta) {	
	            		return "<a href='javascript:void(0)' style='color:blue;text-decoration:underline;'  onclick='commonPage.showUserMsgPage(this,"+row.userId+")'>"+ row.userName +"</a>";
		            	}
		            },
		            {"data":"money"},
		            {"data":"lotteryMoney"},
		            {"data":"ffcLotteryMoney","bVisible" : dayConsumeActivityOrderPage.param.ffcDisplay==true?true:false},
		            {"data":"ffcMoney","bVisible" : dayConsumeActivityOrderPage.param.ffcDisplay==true?true:false},
		            {"data":"belongDateStr"},
		            {"data": "releaseStatus",
		            	render: function(data, type, row, meta) {
	                		 var str ="";
	                		 if(row.releaseStatus == "NOT_APPLY"){
	                			 return str = "未申请";
	                		 }else if(row.releaseStatus == "APPLIED"){
	                			 return str = "已申请";
	                		 }else if(row.releaseStatus == "RELEASED"){
	                			 return str = "已发放";
	                		 }else if(row.releaseStatus == "NOTALLOWED_APPLY"){
	                			 return str = "不可申请";
	                		 }
	                		 return data ;
	                	 }
		            },
		            {"data": "ffcReleaseStatus",
		            	render: function(data, type, row, meta) {
	                		 var str ="";
	                		 if(row.ffcReleaseStatus == "NOT_APPLY"){
	                			 return str = "未申请";
	                		 }else if(row.ffcReleaseStatus == "APPLIED"){
	                			 return str = "已申请";
	                		 }else if(row.ffcReleaseStatus == "RELEASED"){
	                			 return str = "已发放";
	                		 }else if(row.ffcReleaseStatus == "NOTALLOWED_APPLY"){
	                			 return str = "不可申请";
	                		 }
	                		 return data ;
	                	 },"bVisible" : dayConsumeActivityOrderPage.param.ffcDisplay==true?true:false
		            },
		            {"data":"appliyDateStr"},
		            {"data": "id",
	                	 render: function(data, type, row, meta) {
	                		 var str ="";
                			 if(row.releaseStatus == "APPLIED"){
	                			 str += "<a class='btn btn-info btn-sm btn-bj' onclick='dayConsumeActivityOrderPage.giveActityMoney("+data+")'><i class='fa fa-trash' ></i>&nbsp;发放 </a>&nbsp;&nbsp;"
	                		 }
                			 if(row.ffcReleaseStatus == "APPLIED"){
	                			 str += "<a class='btn btn-info btn-sm btn-bj' onclick='dayConsumeActivityOrderPage.giveActityFfcMoney("+data+")'><i class='fa fa-trash' ></i>&nbsp;分分彩发放 </a>&nbsp;&nbsp;"
	                		 }
	                		 str += "<a class='btn btn-info btn-sm btn-bj' onclick='dayConsumeActivityOrderPage.editDayConsumeActityOrder("+data+")'><i class='fa fa-trash' ></i>&nbsp;编辑 </a>&nbsp;&nbsp;";
	                		 str += "<a class='btn btn-danger btn-sm btn-del' onclick='dayConsumeActivityOrderPage.delDayConsumeActityOrder("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";
	                		 return str ;
	                	 }
	                }
	            ]
	}).api();
};

/**
 * 友情提示
 */
function hint(){
	swal("客服管理员不能操作此功能！");
}

/**
 * 查询数据
 */
DayConsumeActivityOrderPage.prototype.findDayConsumeActivityConfig = function(){
	dayConsumeActivityOrderPage.queryParam = getFormObj($("#dayConsumeActivityOrderForm"));
	var belongDate=$("#belongDate").val();
	if(belongDate!=null&&belongDate!=''){
		dayConsumeActivityOrderPage.queryParam.belongDate=belongDate.stringToDate();
	}
	dayConsumeActivityOrderPage.table.ajax.reload();
};

/**
 * 一键发放流水返送金额
 */
DayConsumeActivityOrderPage.prototype.onekeyGiveActityMoney = function(){
	//客服管理员用户不能操作
	if(currentUser.state == 3){
		swal("客服管理员不能操作此功能！");
		 return;
	}
	var belongDate=$("#belongDate").val();
	
	var bizSystem = $("#bizSystem").val();
	if(belongDate == null || belongDate == ""){
		swal("请选归属日期！");
		return;
	}
	   swal({
        title: "您确定要一键发放"+belongDate+"的流水返送吗？",
        text: "请谨慎操作！",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        cancelButtonText: "取消",
        confirmButtonText: "发放",
        closeOnConfirm: false
    },
    function() {
    	//字符串日期转date
    	var date = new Date(Date.parse(belongDate.replace(/-/g, "/")));
    	
    	var dayConsumeActityOrder = {};
    	dayConsumeActityOrder.bizSystem = bizSystem;
    	dayConsumeActityOrder.belongDate = date;
    	
    	managerDayConsumeActityOrderAction.onekeyGiveActityMoney(dayConsumeActityOrder,function(r){
    		if (r[0] != null && r[0] == "ok") {
    			swal({ title: "提示", text: "成功发放【"+r[1]+"】条流水返送订单",type: "success"});
    			dayConsumeActivityOrderPage.table.ajax.reload();
    		}else if(r[0] != null && r[0] == "error"){
    			swal(r[1]);
    		}else{
    			swal("一键发放失败.");
    		}
    	});
    });

};

/**
 * 发放活动金
 */
DayConsumeActivityOrderPage.prototype.giveActityMoney = function(id){
	
	   swal({
           title: "您确定要发放流水返送吗",
           text: "请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "发放",
           closeOnConfirm: false
       },
       function() {
    	   managerDayConsumeActityOrderAction.giveActityMoney(id,function(r){
   			if (r[0] != null && r[0] == "ok") {
   				swal({ title: "提示", text: "发放成功",type: "success"});
   				dayConsumeActivityOrderPage.table.ajax.reload();
   			}else if(r[0] != null && r[0] == "error"){
   				swal(r[1]);
   			}else{
   				swal("流水返送发放失败.");
   			}
   	    });
       });
};

/**
 * 发放活动金
 */
DayConsumeActivityOrderPage.prototype.giveActityFfcMoney = function(id){
	
	   swal({
           title: "您确定要发放分分彩流水返送吗",
           text: "请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "发放",
           closeOnConfirm: false
       },
       function() {
    	   managerDayConsumeActityOrderAction.giveActityFfcMoney(id,function(r){
   			if (r[0] != null && r[0] == "ok") {
   				swal({ title: "提示", text: "发放成功",type: "success"});
   				dayConsumeActivityOrderPage.table.ajax.reload();
   			}else if(r[0] != null && r[0] == "error"){
   				swal(r[1]);
   			}else{
   				swal("流水返送发放失败.");
   			}
   	    });
       });
};


/**
 * 修改流水返送订单
 */
DayConsumeActivityOrderPage.prototype.editDayConsumeActityOrder=function(id){
	
	  layer.open({
           type: 2,
           title: '编辑流水返送订单',
           maxmin: false,
           shadeClose: true,
           //点击遮罩关闭层
           area: ['70%', '80%'],
           content: contextPath + "/managerzaizst/day_consume_activity_order/"+id+"/day_consume_activity_order_edit.html",
           cancel: function(index){ 
        	   dayConsumeActivityOrderPage.table.ajax.reload();
        	   }
       });
}

/**
 * 删除
 */
DayConsumeActivityOrderPage.prototype.delDayConsumeActityOrder = function(id){
	   swal({
           title: "您确定要删除这条流水返送订单吗",
           text: "请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "删除",
           closeOnConfirm: false
       },
       function() {
    	   managerDayConsumeActityOrderAction.delDayConsumeActityOrder(id,function(r){
   			if (r[0] != null && r[0] == "ok") {
   				swal({ title: "提示", text: "删除成功",type: "success"});
   				dayConsumeActivityOrderPage.table.ajax.reload();
   			}else if(r[0] != null && r[0] == "error"){
   				swal(r[1]);
   			}else{
   				swal("删除信息失败.");
   			}
   	    });
       });
};

DayConsumeActivityOrderPage.prototype.closeLayer=function(index){
	layer.close(index);
	swal({
        title: "保存成功",
        text: "您已经保存了这条订单。",
        type: "success"});
	dayConsumeActivityOrderPage.table.ajax.reload();
}

DayConsumeActivityOrderPage.prototype.dayConsumeActityOrderInfoExport = function(){
	dayConsumeActivityOrderPage.queryParam = getFormObj($("#dayConsumeActivityOrderForm"));
	managerDayConsumeActityOrderAction.dayConsumeActityOrderInfoExport(dayConsumeActivityOrderPage.queryParam,function(r){
		dwr.engine.openInDownload(r);
		
    });
}
