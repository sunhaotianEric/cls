<%@ page language="java" contentType="text/html; charset=UTF-8"
	import="com.team.lottery.enums.EHelpType,com.team.lottery.system.SystemConfigConstant"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
	StringBuffer url = request.getRequestURL();
	String domainUrl = url.toString().replaceAll(request.getRequestURI(), "") + path;
	String staticImageServerUrl = "http://" + SystemConfigConstant.picHost;
%>
<html>
<head>
<meta charset="utf-8" />
<title>帮助中心</title>
<jsp:include page="/admin/include/include.jsp"></jsp:include>
<!-- js -->
<script type="text/javascript"
	src="<%=path%>/admin/helps/js/edithelps.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript'
	src='<%=path%>/dwr/interface/managerHelpsAction.js'></script>

<link rel="stylesheet"
	href="<%=path%>/resources/kindeditor-4.1.10/themes/default/default.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" />
<script charset="utf-8"
	src="<%=path%>/resources/kindeditor-4.1.10/kindeditor-min.js"></script>
<script charset="utf-8"
	src="<%=path%>/resources/kindeditor-4.1.10/lang/zh_CN.js"></script>
<script>
       var  helpPage=parent.helpPage;
       var layerindex = parent.layer.getFrameIndex(window.name);
       var olddomain = document.domain;
         
		var editor;  //editor.html()
		//图片服务器地址
		var staticImageUrl='<%=domainUrl%>';
		var url = staticImageUrl.split('//');
<%-- 		var imgurl='<%=staticImageServerUrl%>
	'; --%>
	KindEditor.ready(function(K) {
		editor = K.create('textarea[name="content"]', {
			uploadJson : location.protocol+'//'+url[1]+'/UploadFileForJsonServlet?dirfile=help',
			/* fileManagerJson : imgurl + '/file_manager_json.jsp', */
			allowFileManager : true
		});
	});
</script>
</head>
<body>
	<div class="alert alert-info">
		当前位置<b class="tip"></b>帮助中心管理<b class="tip"></b>编辑帮助信息
	</div>

	<table class="tb">
		<tr>
			<td><a href="<%=path%>/managerzaizst/help.html">信息管理</a>
				&nbsp;&nbsp; <a href="<%=path%>/managerzaizst/help/help_add.html">添加信息</a></td>
		</tr>
	</table>
	<form action="javascript:void(0)">
		<c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
			<div class="col-sm-12">
				<div class="input-group m-b">
					<span class="input-group-addon">商户：</span>
					<cls:bizSel name="bizSystem" id="bizSystem"
						options="class:ipt form-control" />
				</div>
			</div>
		</c:if>
		<div class="col-sm-12">
			<div class="input-group m-b">
				<span class="input-group-addon">标题：</span> <input type="text"
					value="" class="form-control" name="title" id="title"><input
					type="hidden" id="helpId">
			</div>
		</div>
		<div class="col-sm-6">
			<div class="input-group m-b">
				<span class="input-group-addon">帮助内容：</span>
				<textarea name="content" id="content"
					style="width: 805px; height: 400px; visibility: hidden;"></textarea>
			</div>
		</div>
		<div class="col-sm-12">
			<div class="input-group m-b">
				<span class="input-group-addon">是否启用：</span> <select
					class="form-control" id="isShow">
					<option value="1">启用</option>
					<option value="0">停用</option>
				</select>
			</div>
		</div>
		<div class="col-sm-12">
			<div class="input-group m-b">
				<span class="input-group-addon">排序：</span> <input type="text"
					value="" class="form-control" name="sortNum" id="sortNum">
			</div>
		</div>
		<div class="col-sm-12">
			<div class="row">
				<div class="col-sm-6 text-center">
					<button type="button" class="btn btn-w-m btn-white"
						onclick="editHelpsPage.saveData()">提 交</button>
				</div>
				<div class="col-sm-6 text-center">
					<input type="reset" class="btn btn-w-m btn-white" value="重 置" />
				</div>
			</div>
		</div>
	</form>
</body>
</html>

