function EditHelpsPage(){}
var editHelpsPage = new EditHelpsPage();

editHelpsPage.param = {
   	
};

$(document).ready(function() {
	var helpId = editHelpsPage.param.id;  //获取查询的帮助ID
	if(helpId != null){
		editHelpsPage.editHelps(helpId);
	}
});


/**
 * 保存帮助信息
 */
EditHelpsPage.prototype.saveData = function(){
	
	var id = $("#helpId").val();
	var bizSystem = $("#bizSystem").val();
	var title = $("#title").val();
	var content = editor.html();
	var isShow = $("#isShow").val();
	var sortNum = $("#sortNum").val();
	
	if(currentUser.bizSystem == "SUPER_SYSTEM"){
		if(bizSystem==null || bizSystem.trim()==""){
			swal("请选择业务系统!");
			$("#bizSystem").focus();
			return;
		}
	}else{
		bizSystem = currentUser.bizSystem;
	}
	
	if(title==null || title.trim()==""){
		swal("请输入公告标题!");
		$("#title").focus();
		return;
	}
	if(content==null || content.trim()==""){
		swal("请输入公告内容!");
		editor.focus();
		return;
	}
	if(isShow==null || isShow.trim()==""){
		swal("请选择是否启用!");
		$("#isShow").focus();
		return;
	}
	if(sortNum==null || sortNum.trim()==""){
		swal("请选择排序!");
		$("#sortNum").focus();
		return;
	}
	
	var helps = {};
	helps.id = id;
	helps.bizSystem = bizSystem;
	helps.title = title;
	helps.content = content;
	helps.isShow = isShow;
	helps.sortNum = sortNum;
	managerHelpsAction.saveOrUpdateHelps(helps,function(r){
		if (r[0] != null && r[0] == "ok") {
			var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
			parent.helpsPage.closeLayer(index);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("保存帮助信息失败.");
		}
    });
};


/**
 * 赋值帮助信息
 */
EditHelpsPage.prototype.editHelps = function(id){
	managerHelpsAction.getHelpsById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			$("#helpId").val(r[1].id);
			$("#bizSystem").val(r[1].bizSystem);
			$("#bizSystem").attr("disabled","disabled");
			$("#title").val(r[1].title);
			editor.html(r[1].content);
			$("#isShow").val(r[1].isShow);
			$("#sortNum").val(r[1].sortNum);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取帮助信息失败.");
		}
    });
};