function HelpsPage(){}
var helpsPage = new HelpsPage();

helpsPage.param = {
   	
};
// 分页参数
helpsPage.pageParam = {
		    pageNo : 1,
		    pageSize : defaultPageSize
};
/**
 * 查询参数
 */
helpsPage.queryParam = {
		bizSystem:null,
		type : null,
		title : null
};
$(document).ready(function() {
	helpsPage.initTableData();
});

/**
 * 加载所有的帮助中心
 */
HelpsPage.prototype.getAllHelps = function(){
	managerHelpsAction.getAllHelps(function(r){
		if (r[0] != null && r[0] == "ok") {
			helpsPage.refreshHelpPages(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询帮助中心数据失败.");
		}
    });
};
//
// /**
// * 刷新列表数据
// */
// HelpsPage.prototype.refreshHelpPages = function(helps){
// var helpListObj = $("#helpList");
// helpListObj.html("");
// var str = "";
//
// for(var i = 0 ; i < helps.length; i++){
// var help = helps[i];
// str += "<tr>";
// str += " <td><input type='hidden' name='helpId' value='"+ help.id +"'/>"+
// help.id +"</td>";
// str += " <td>"+ help.typeDes +"</td>";
// str += " <td>"+ help.title +"</td>";
// if(help.frequentlyQuestion == 1){
// str += " <td>是</td>";
// }else{
// str += " <td>否</td>";
// }
// str += " <td>"+ help.logtimeStr +"</td>";
// str += " <td><a href='javascript:void(0)' onclick='helpPage.editHelp("+
// help.id +")'>编辑</a>&nbsp;|&nbsp;<a href='javascript:void(0)'
// onclick='helpPage.delHelp("+ help.id +")'>删除</a></td>";
//
// str += "</tr>";
//		
// helpListObj.append(str);
// str = "";
// }
// };

/**
 * 删除帮助中心信息
 */
HelpsPage.prototype.delHelp = function(id){
	if (confirm("确认要删除？")){
		managerHelpsAction.delHelp(id,function(r){
			if (r[0] != null && r[0] == "ok") {
				helpsPage.getAllHelps();
			}else if(r[0] != null && r[0] == "error"){
				showErrorDlg(jQuery(document.body), r[1]);
			}else{
				showErrorDlg(jQuery(document.body), "删除帮助中心信息失败.");
			}
	    });
	}
};

/**
 * 编辑帮助中心信息
 */
HelpsPage.prototype.editHelp = function(id){
	self.location = contextPath + "/managerzaizst/help/" + id+"/edithelp.html";
};
HelpsPage.prototype.initTableData = function(){

	helpsPage.queryParam = getFormObj($("#helpsForm"));
	helpsPage.table = $('#helpsTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;// 获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			helpsPage.pageParam.pageSize = data.length;// 页面显示记录条数
			helpsPage.pageParam.pageNo = (data.start / data.length)+1;// 当前页码
	    	managerHelpsAction.getAllHelps(helpsPage.queryParam,helpsPage.pageParam,function(r){
				// 封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
				returnData.recordsTotal = r[1].totalRecNum;// 返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;// 后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;// 返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询所有的数据失败.");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": "id"},
		            {"data": "bizSystemName",
		            	"bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "title"},
		            {"data": "content",
		            	render: function(data, type, row, meta) {
		            		if (data.length > 30) {
		            			var title = data.substring(0,30) + "...";
		            			return "<span title = '"+data+"'>"+title+"</span>";
		            		}
		            		return data;
		        		}
		            },
		            {"data": "createTimeStr"},
		            {"data": "updateTimeStr"},
		            {"data": "isShow",
		            	render: function(data, type, row, meta) {
		            		if (data == "1") {
		            			return '启用';
		            		} else {
		            			return "<span style='color: red;'>停用</span>";
		            		}
		            		return data;
	            		}
		            },
		            {
	                	 "data": "id",
	                	 render: function(data, type, row, meta) {
	                		 var str=""
	                			 str="<a class='btn btn-info btn-sm btn-bj' onclick='helpsPage.editHelps("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;"+
	                		 		"<a class='btn btn-danger btn-sm btn-del' onclick='helpsPage.delHelps("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";
	                	         if(row.isShow=="1"){
	                	        	 str = "<a class='btn btn-danger btn-sm btn-del' onclick='helpsPage.enableHelps("+data+",0)'><i class='fa fa-star' ></i>&nbsp;停用 </a>&nbsp;&nbsp;"+str; 
	                	         }else{
	                	        	 str = "<a class='btn btn-info btn-sm btn-bj' onclick='helpsPage.enableHelps("+data+",1)'><i class='fa fa-star' ></i>&nbsp;启用 </a>&nbsp;&nbsp;"+str;  
	                	         }
	                	         return str;
	                	 }
	                }
	            ]
	}).api();
};

/**
 * 查询数据
 */
HelpsPage.prototype.findHelps = function(){
	var bizSystem = $("#bizSystem").val();
	var title = $("#title").val();
	if(bizSystem == ""){
		helpsPage.queryParam.bizSystem = null;
	}else{
		helpsPage.queryParam.bizSystem = bizSystem;
	}
	if(title == ""){
		helpsPage.queryParam.title = null;
	}else{
		helpsPage.queryParam.title = title;
	}
	helpsPage.table.ajax.reload();
};




/**
 * 删除
 */
HelpsPage.prototype.delHelps = function(id){
	   swal({
           title: "您确定要删除这条信息吗",
           text: "请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "删除",
           closeOnConfirm: false
       },
       function() {
    	   managerHelpsAction.delHelps(id,function(r){
   			if (r[0] != null && r[0] == "ok") {
   				swal({ title: "提示", text: "删除成功",type: "success"});
   				helpsPage.table.ajax.reload();
   			}else if(r[0] != null && r[0] == "error"){
   				swal(r[1]);
   			}else{
   				swal("删除信息失败.");
   			}
   	    });
       });
};

/**
 * 新增
 */
HelpsPage.prototype.addHelps=function()
{
	
	 layer.open({
         type: 2,
         title: '帮助信息新增',
         maxmin: false,
         shadeClose: true,
         // 点击遮罩关闭层
         area: ['70%', '80%'],
         content: contextPath + "/managerzaizst/helps/edithelps.html",
         cancel: function(index){ 
      	   helpsPage.table.ajax.reload();
      	   }
     });
}
/**
 * 编辑
 */
HelpsPage.prototype.editHelps=function(id)
{
	
	  layer.open({
           type: 2,
           title: '帮助信息编辑',
           maxmin: false,
           shadeClose: true,
           // 点击遮罩关闭层
           area: ['70%', '80%'],
           content: contextPath + "/managerzaizst/helps/"+id+"/edithelps.html",
           cancel: function(index){ 
        	   helpsPage.table.ajax.reload();
        	   }
       });
}

/**
 * 停用，启用
 */
HelpsPage.prototype.enableHelps = function(id,flag){
	var str="";
	   if(flag==0){
		   str="停用";  
	   }else{
		   str="启用";  
	   }
	   
	   swal({
        title: "您确定要"+str+"该条帮助信息？",
        text: "请谨慎操作！",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        cancelButtonText: "取消",
        confirmButtonText: "确定",
        closeOnConfirm: false
    },
    function() {
    	managerHelpsAction.HelpsIsShow(id,flag,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "操作成功！",type: "success"});
				helpsPage.table.ajax.reload();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("操作失败.");
			}
	    });
    });
};

HelpsPage.prototype.closeLayer=function(index){
	layer.close(index);
	swal({
        title: "保存成功",
        text: "您已经保存了这条图片。",
        type: "success"});
	helpsPage.table.ajax.reload();
}

HelpsPage.prototype.layerClose=function(index){
	layer.close(index);
	helpsPage.addHelps();
}