function RechargeorderPage(){}
var rechargeorderPage = new RechargeorderPage();

rechargeorderPage.param = {
	orderList : null,
	operatePayId : null,
	refreshKey : null
};

/**
 * 查询参数
 */
rechargeorderPage.queryParam = {
	userName : null,
	statuss : null,
	payType : null,
	operateType : 'RECHARGE',
	createdDateStart : null,
	createdDateEnd : null,
	serialNumber : null,
	bankType : null
};

//分页参数
rechargeorderPage.pageParam = {
		    pageNo : 1,
		    pageSize : defaultPageSize
};

$(document).ready(function() {
	$("#createdDateStart").val(getClsStartTimeZero().format("yyyy-MM-dd hh:mm:ss"));
	$("#createdDateEnd").val(getClsEndTimeZero().format("yyyy-MM-dd hh:mm:ss"));
	//选择充值类别改变事件
	$("#payType").change(function(){
		//网银汇款显示充值方式
		if($(this).val() == "BANK_TRANSFER"||$(this).val() == "QUICK_PAY") {
			$("#bankType option:gt(0)").show();
		} else {
			$("#bankType option:gt(0)").hide();
		}
	});
	
	rechargeorderPage.initTableData();
	//10秒钟刷新一次记录
	//rechargeorderPage.param.refreshKey = window.setInterval("rechargeorderPage.getAllRechargeOrdersForQuery()",10000);
});
	
/**
 * 加载所有的登陆日志数据
 */
RechargeorderPage.prototype.getAllRechargeOrdersForQuery = function(dateRange){
	/*rechargeorderPage.pageParam.queryMethodParam = 'getAllRechargeOrdersForQuery';*/
	if(dateRange != undefined && dateRange != null && dateRange != ''){
		commonPage.setDateRangeTimeZero(dateRange, "createdDateStart", "createdDateEnd",2);	
	}
	var rechargeStatus = $("#rechargeStatus").val();
	var userName = $("#userName").val();
	var payType = $("#payType").val();
	var bankType = $("#bankType").val();
	var serialNumber = $("#serialNumber").val();
	var createdDateStart = $("#createdDateStart").val();
	var createdDateEnd = $("#createdDateEnd").val();
	var bizSystem = $("#bizSystem").val();
	var thirdPayType = $("#thirdPayType").val();
	var refType = $("#refType").val();
	if(rechargeStatus == ""){
		rechargeorderPage.queryParam.statuss = null;
	}else{
		rechargeorderPage.queryParam.statuss = rechargeStatus;
	}
	if(refType == ""){
		rechargeorderPage.queryParam.refType = null;
	}else{
		rechargeorderPage.queryParam.refType = refType;
	}
	if(userName == ""){
		rechargeorderPage.queryParam.userName = null;
	}else{
		rechargeorderPage.queryParam.userName = userName;
	}
	if(payType == ""){
		rechargeorderPage.queryParam.payType = null;
	}else{
		rechargeorderPage.queryParam.payType = payType;
	}
	if(bankType == ""){
		rechargeorderPage.queryParam.bankType = null;
	}else{
		rechargeorderPage.queryParam.bankType = bankType;
	}
	if(serialNumber == ""){
		rechargeorderPage.queryParam.serialNumber = null;
	}else{
		rechargeorderPage.queryParam.serialNumber = serialNumber;
	}
	if(createdDateStart == ""){
		rechargeorderPage.queryParam.createdDateStart = null;
	}else{
		rechargeorderPage.queryParam.createdDateStart = new Date(createdDateStart);
	}
	if(createdDateEnd == ""){
		rechargeorderPage.queryParam.createdDateEnd = null;
	}else{
		rechargeorderPage.queryParam.createdDateEnd = new Date(createdDateEnd);
	}
	if(bizSystem == ""){
		rechargeorderPage.queryParam.bizSystem = null;
	}else{
		rechargeorderPage.queryParam.bizSystem = bizSystem;
	}
	if(thirdPayType == ""){
		rechargeorderPage.queryParam.thirdPayType = null;
	}else{
		rechargeorderPage.queryParam.thirdPayType = thirdPayType;
	}
	rechargeorderPage.queryParam.operateType ='RECHARGE';
	rechargeorderPage.table.ajax.reload();
	
};

RechargeorderPage.prototype.initTableData = function(){
	
	rechargeorderPage.queryParam = getFormObj($("#rechargereportForm"));
	rechargeorderPage.queryParam.operateType ='RECHARGE';
	rechargeorderPage.table = $('#rechargereportTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"fnDrawCallback": function (oSettings) {  
			rechargeorderPage.refreshRechargeOrdersPages(rechargeorderPage.totalRechargeOrder);
	        },
		"ajax":function (data, callback, settings) {
			rechargeorderPage.pageParam.pageSize = data.length;//页面显示记录条数
			rechargeorderPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerRechargeWithDrawOrderAction.getAllRechargeOrdersByQueryForReport(rechargeorderPage.queryParam,rechargeorderPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
					rechargeorderPage.totalRechargeOrder=r[2];
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询充值数据失败.");
				}
				callback(returnData);
			});
		},
		"columns": [
                    {"data": "bizSystemName","bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "userName"},
		            {"data": "serialNumber"},
		            {"data": "applyValue"},
		            {"data": "accountValue"},
		            {"data": "payTypeStr"},
		            {"data": "thirdPayTypeStr"},
		            {"data": "bankName"},
		            {"data": "createdDateStr"},
		            {"data": "statusStr",render: function(data, type, row, meta) {
 		            	var value=row.dealStatus;
 		            	if(value=="PAYMENT")
 		            		{
 		            		return "<font color='blue'>"+data+"</font>" ;
 		            		}
 		            	else if(value=="PAYMENT_SUCCESS")
 		            		{
 		            		return "<font color='green'>"+data+"</font>" ;
 		            		}
 		            	else if(value=="PAYMENT_CLOSE")
 		            		{
 		            		return "<font color='red'>"+data+"</font>" ;
 		            		}
                		 
                  }}
	            ]
	}).api(); 
};
/**
 * 刷新列表数据
 */
RechargeorderPage.prototype.refreshRechargeOrdersPages = function(totalRechargeOrder){
	var rechargeOrderListObj = $("#rechargereportTable tbody");
	/*rechargeOrderListObj.html("");*/

	var str = "";
    /*var rechargeOrders = page;
    
    //记录数据显示
	for(var i = 0 ; i < rechargeOrders.length; i++){
		var rechargeOrder = rechargeOrders[i];
		str += "<tr>";
		str += "  <td><a href='javascript:void(0)' style='color:blue;text-decoration:underline;'  onclick='commonPage.showUserMsgPage(this,"+rechargeOrder.userId+")'>"+ rechargeOrder.userName +"</a></td>";
		if(currentUser.bizSystem=='SUPER_SYSTEM'){
			str += "  <td>"+ rechargeOrder.bizSystemName +"</td>";
		}
		str += "  <td>"+ rechargeOrder.serialNumber +"</td>";
		str += "  <td>"+ rechargeOrder.applyValue.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ rechargeOrder.accountValue.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ rechargeOrder.payTypeStr +"</td>";
		str += "  <td>"+ rechargeOrder.thirdPayTypeStr +"</td>";
		str += "  <td>"+ rechargeOrder.createdDateStr +"</td>";
		str += "  <td>"+ rechargeOrder.statusStr +"</td>";
		str += "</tr>";
		rechargeOrderListObj.append(str);
		str = "";
	}*/
	if(totalRechargeOrder != null){
		str += "<tr>";
		str += "  <td>总计</td>";
		str += "  <td></td>";
		if(currentUser.bizSystem=='SUPER_SYSTEM'){
			str += "  <td></td>";
		}
		str += "  <td style='color:red;'>"+ totalRechargeOrder.applyValue.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td style='color:red;'>"+ totalRechargeOrder.accountValue.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td></td>";
		str += "  <td></td>";
		str += "  <td></td>";
		str += "  <td></td>";
		str += "</tr>";
		rechargeOrderListObj.append(str);		
	}
};