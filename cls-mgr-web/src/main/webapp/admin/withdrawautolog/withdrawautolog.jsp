<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="com.team.lottery.enums.EWithdrawAutoStatus"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>自动出款日志</title>
<link rel="shortcut icon" href="favicon.ico">
<jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
<body>
	<div class="animated fadeIn">
		<div class="ibox-content">
			<div class="row m-b-sm m-t-sm">
				<form class="form-horizontal" id="withdrawautoForm">
					<div class="row">
						<div class="col-sm-4 biz">
	                                <div class="input-group m-b">
	                                    <span class="input-group-addon">商户</span>
	                                    <cls:bizSel name="bizSystem" id="bizSystem"	options="class:ipt form-control" />
	                                </div>
	                            </div>
						<div class="col-sm-3">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">第三方支付名称</span>
                                   <cls:thirdPaySel name="chargeType" id="chargeType" options="class:ipt form-control" />
                            </div>
                        </div>
						<div class="col-sm-3">
							<div class="input-group m-b">
								<span class="input-group-addon">处理状态</span>
								<cls:enmuSel name="dealStatus"
									className="com.team.lottery.enums.EWithdrawAutoStatus"
									options="class:ipt form-control" id="dealStatus" />
							</div>
						</div>
						<div class="col-sm-4">
							<div class="input-group m-b">
								<span class="input-group-addon">时间：</span> <input
									class="form-control layer-date" placeholder="开始时间"
									onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"
									name="startDate" id="startDate"> <span
									class="input-group-addon">到</span> <input
									class="form-control layer-date" placeholder="结束时间"
									onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"
									name="endDate" id="endDate">
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group nomargin text-right">
								<button type="button" class="btn btn-outline btn-default btn-js"
									onclick="withdrawautoPage.findwithdrawauto()">
									<i class="fa fa-search"></i>&nbsp;查 询
								</button>
								<!--    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	                                    <button type="button" class="btn btn-outline btn-default btn-add" onclick="withdrawautoPage.addwithdrawauto()"><i class="fa fa-plus"></i>&nbsp;新 增</button> -->
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="jqGrid_wrapper">
				<div class="ui-jqgrid ">
					<div class="ui-jqgrid-view">
						<div class="ui-jqgrid-hdiv">
							<div class="ui-jqgrid-hbox" style="padding-right: 0">
								<table id="withdrawautoPageTable"
									class="ui-jqgrid-htable ui-common-table table table-bordered">
									<thead>
										<tr class="ui-jqgrid-labels">
											<th class="ui-th-column ui-th-ltr">编号</th>
											<th class="ui-th-column ui-th-ltr">商户</th>
											<th class="ui-th-column ui-th-ltr">第三方支付名称</th>
											<th class="ui-th-column ui-th-ltr">商户号</th>
											<th class="ui-th-column ui-th-ltr">金额</th>
											<th class="ui-th-column ui-th-ltr">银行名称</th>
											<th class="ui-th-column ui-th-ltr">银行卡号</th>
											<th class="ui-th-column ui-th-ltr">处理状态</th>
											<th class="ui-th-column ui-th-ltr">类型</th>
											<th class="ui-th-column ui-th-ltr">操作管理员</th>
											<th class="ui-th-column ui-th-ltr">创建时间</th>
											<th class="ui-th-column ui-th-ltr">修改时间</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript"
		src="<%=path%>/admin/common/common.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
	<!-- dwr -->
	<script type='text/javascript'
		src='<%=path%>/dwr/interface/mangerWithdrawAutoLogAction.js'></script>
	<script type="text/javascript"
		src="<%=path%>/admin/withdrawautolog/js/withdrawautolog.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
</body>

</html>
