function WithdrawautoPage(){}
var withdrawautoPage = new WithdrawautoPage();

/**
 * 查询参数
 */
withdrawautoPage.queryParam = {
		id:null,
		bizSystem: null,
		chargeType : null,
		memberId:null,
		money:null,
		bankName:null,
		bankCardNum:null,
		dealStatus:null,
		type:null,
		adminName:null,
		createdDate:null,	
		updatedDate:null
};

/**
 * 分页参数
 */
withdrawautoPage.pageParam = {
    pageNo : 1,
    pageSize : defaultPageSize,
};

$(document).ready(function() {
	withdrawautoPage.initTableData();// 初始化查询数据
});

WithdrawautoPage.prototype.initTableData = function() {
	withdrawautoPage.queryParam = getFormObj($("#withdrawautoForm"));
	withdrawautoPage.table = $('#withdrawautoPageTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"bPaginate" : true, 
		"iDisplayLength" : defaultPageSize,
//		"drawCallback": function(settings) {
//			var api = this.api();
//            var startIndex= api.context[0]._iDisplayStart;// 获取到本页开始的条数
//            api.column(0).nodes().each(function(cell, i) {
//            cell.innerHTML = startIndex + i + 1;
//            }); 
//		 },
		"ajax":function (data, callback, settings) {
			withdrawautoPage.pageParam.pageSize = data.length;// 页面显示记录条数
			withdrawautoPage.pageParam.pageNo = (data.start / data.length)+1;// 当前页码    	
			mangerWithdrawAutoLogAction.getAllWithdrawAutoLog(withdrawautoPage.queryParam,withdrawautoPage.pageParam,function(r){
		// 封装返回数据
		var returnData = {
			recordsTotal : 0,
			recordsFiltered : 0,
			data : null
		};
		if (r[0] != null && r[0] == "ok") {
			returnData.recordsTotal = r[1].totalRecNum;// 返回数据全部记录
			returnData.recordsFiltered = r[1].totalRecNum;// 后台不实现过滤功能，每次查询均视作全部结果
			returnData.data = r[1].pageContent;// 返回的数据列表
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("查询信息失败.");
		}
		callback(returnData);
			});
		},
		"columns": [
            {"data": "id"},
            {
            	"data": "bizSystemName",
            	"bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false,
            },
            {"data": "chargeTypeStr"},
            {"data": "memberId"},
            {"data": "money"},
            {"data": "bankName"},
            {"data": "bankCardNum"},
            {"data": "dealStatusName"},
            {"data": "typeName"},
            {"data": "adminName"},
            {"data": "createdDateStr"},
            {
            	"data": "updatedDate","bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false,
	            render: function(data, type, row, meta) {
	            	return dateFormat(data,'yyyy-MM-dd hh:mm:ss');
            	}
            }
        ]
}).api(); 
};

/**
 * 查询数据
 */
WithdrawautoPage.prototype.findwithdrawauto=function(){
	var bizSystem=$("#bizSystem").val();
	var chargeType=$("#chargeType").val();
	var dealStatus=$("#dealStatus").val();
	var startDate=$("#startDate").val();
	var endDate=$("#endDate").val();
	if(bizSystem==null||bizSystem==""){
		withdrawautoPage.queryParam.bizSystem=null;
	}else{
		withdrawautoPage.queryParam.bizSystem=bizSystem;
	}
	if(chargeType==null||chargeType==""){
		withdrawautoPage.queryParam.chargeType=null;
	}else{
		withdrawautoPage.queryParam.chargeType=chargeType;
	}
	if(dealStatus==null||dealStatus==""){
		withdrawautoPage.queryParam.dealStatus=null;
	}else{
		withdrawautoPage.queryParam.dealStatus=dealStatus;
	}
	if(startDate==null||startDate==""){
		withdrawautoPage.queryParam.startDate=null;
	}else{
		withdrawautoPage.queryParam.startDate=new Date(startDate);
	}
	if(endDate==null||endDate==""){
		withdrawautoPage.queryParam.endDate=null;
	}else{
		withdrawautoPage.queryParam.endDate=new Date(endDate);
	}
	withdrawautoPage.table.ajax.reload();
}

WithdrawautoPage.prototype.layerClose=function(index){
	layer.close(index);
	withdrawautoPage.findwithdrawauto();
}