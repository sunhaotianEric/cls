function NewWithdrawautoPage(){}
var newwithdrawautoPage = new NewWithdrawautoPage();

/**
 * 查询参数
 */
NewWithdrawautoPage.queryParam = {
		id:null,
		bizSystem:null,
		chargeType : null,
		memberId:null,
		money:null,
		bankName:null,
		bankCardNum:null,
		dealStatus:null,
		type:null,
		adminName:null,
		createdDate:null,	
		updatedDate:null
};

/**
 * 分页参数
 */
newwithdrawautoPage.pageParam = {
    pageNo : 1,
    pageSize : defaultPageSize,
};

$(document).ready(function() {
	newwithdrawautoPage.initTableData();// 初始化查询数据
});

NewWithdrawautoPage.prototype.initTableData = function() {
	newwithdrawautoPage.queryParam = getFormObj($("#withdrawautoForm"));
	newwithdrawautoPage.table = $('#withdrawautoPageTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"bPaginate" : true, 
		"iDisplayLength" : defaultPageSize,
//		"drawCallback": function(settings) {
//			var api = this.api();
//            var startIndex= api.context[0]._iDisplayStart;// 获取到本页开始的条数
//            api.column(0).nodes().each(function(cell, i) {
//            cell.innerHTML = startIndex + i + 1;
//            }); 
//		 },
		"ajax":function (data, callback, settings) {
			newwithdrawautoPage.pageParam.pageSize = data.length;// 页面显示记录条数
			newwithdrawautoPage.pageParam.pageNo = (data.start / data.length)+1;// 当前页码    	
			mangerWithdrawAutoLogAction.getAllWithdrawAutoLog(newwithdrawautoPage.queryParam,newwithdrawautoPage.pageParam,function(r){
		// 封装返回数据
		var returnData = {
			recordsTotal : 0,
			recordsFiltered : 0,
			data : null
		};
		if (r[0] != null && r[0] == "ok") {
			returnData.recordsTotal = r[1].totalRecNum;// 返回数据全部记录
			returnData.recordsFiltered = r[1].totalRecNum;// 后台不实现过滤功能，每次查询均视作全部结果
			returnData.data = r[1].pageContent;// 返回的数据列表
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("查询信息失败.");
		}
		callback(returnData);
			});
		},
		"columns": [
            {"data": "id"},
            {"data": "chargeTypeStr"},
            {"data": "memberId"},
            {"data": "money"},
            {"data": "bankName"},
            {"data": "bankCardNum"},
            {"data": "dealStatusName"},
            {"data": "typeName"},
            {"data": "adminName"},
            {"data": "createdDateStr"},
            {"data": "updatedDateStr"}
        ]
}).api(); 
};

/**
 * 查询数据
 */
NewWithdrawautoPage.prototype.findwithdrawauto=function(){
	var chargeType=$("#chargeType").val();
	var dealStatus=$("#dealStatus").val();
	var startDate=$("#startDate").val();
	var endDate=$("#endDate").val();
	if(chargeType==null||chargeType==""){
		newwithdrawautoPage.queryParam.chargeType=null;
	}else{
		newwithdrawautoPage.queryParam.chargeType=chargeType;
	}
	if(dealStatus==null||dealStatus==""){
		newwithdrawautoPage.queryParam.dealStatus=null;
	}else{
		newwithdrawautoPage.queryParam.dealStatus=dealStatus;
	}
	if(startDate==null||startDate==""){
		newwithdrawautoPage.queryParam.startDate=null;
	}else{
		newwithdrawautoPage.queryParam.startDate=new Date(startDate);
	}
	if(endDate==null||endDate==""){
		newwithdrawautoPage.queryParam.endDate=null;
	}else{
		newwithdrawautoPage.queryParam.endDate=new Date(endDate);
	}
	newwithdrawautoPage.table.ajax.reload();
}

NewWithdrawautoPage.prototype.layerClose=function(index){
	layer.close(index);
	newwithdrawautoPage.findwithdrawauto();
}