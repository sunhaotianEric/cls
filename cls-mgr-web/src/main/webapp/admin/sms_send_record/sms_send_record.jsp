<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>  
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>短信发送记录</title>
        <link rel="shortcut icon" href="favicon.ico">
        <jsp:include page="/admin/include/include.jsp"></jsp:include>
    </head>
    <body>
        <div class="animated fadeIn">
            <div class="ibox-content">
                 <div class="row m-b-sm m-t-sm">
                    <form class="form-horizontal" id="smsSendRecordForm">
                        <div class="row">
                        	<c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
	                            <div class="col-sm-4 biz">
	                                <div class="input-group m-b">
	                                    <span class="input-group-addon">商户</span>
	                                    <cls:bizSel name="bizSystem" id="bizSystem"	options="class:ipt form-control" />
	                                </div>
	                            </div>
	                            <div class="col-sm-4">
					              <div class="input-group m-b">
					                <span class="input-group-addon">发送类型</span>
					                    <select id="type" name="type" class="ipt form-control">
											<option value="" selected="selected">-----请选择------</option>
											<option value="DEFAULT_TYPE">默认类型</option>
											<option value="FORGETPWD_TYPE">忘记密码类型</option>
											<option value="BIND_PHONE_TYPE">绑定手机类型</option>
											<option value="CHANGE_PHONE_TYPE">修改绑定手机类型</option>
											<option value="REG_TYPE">注册类型</option>
										</select>
					              </div>
					           </div>
	                            <div class="col-sm-4">
	                                <div class="input-group m-b">
	                                    <span class="input-group-addon">用户名</span>
	                                    <input type="text" id="userName"  name="userName" class="form-control"></div>
	                            </div>
	                            <div class="col-sm-4">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">手机号码</span>
                                    <input type="text" id="mobile"  name="mobile" class="form-control"></div>
                                </div>
	                            <div class="col-sm-4">
								<div class="input-group m-b">
									<span class="input-group-addon">时间：</span> 
									<input	class="form-control layer-date" placeholder="开始时间"	onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"	name="startDate" id="startDate"> 
									<span class="input-group-addon">到</span> 
									<input class="form-control layer-date" placeholder="结束时间"	onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"	name="endDate" id="endDate">
								</div>
							</div>
	                            <div class="col-sm-4">
	                                <div class="form-group nomargin text-right">
	                                    <button type="button" class="btn btn-outline btn-default btn-kj biz" onclick="smsSendRecordPage.findRecordControl()"><i class="fa fa-search"></i>&nbsp;查 询</button>
	                                   <!--  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	                                    <button type="button" class="btn btn-outline btn-default btn-add" onclick="smsSendRecordPage.addRecordControl()"><i class="fa fa-plus"></i>&nbsp;新 增</button> -->
	                                 </div>
	                            </div>
	                        </c:if>
	                        <c:if test="${admin.bizSystem != 'SUPER_SYSTEM'}">
	                            <div class="col-sm-4">
	                                <div class="input-group m-b">
	                                    <span class="input-group-addon">商户</span>
	                                    <cls:bizSel name="bizSystem" id="bizSystem"	options="class:ipt form-control" />
	                                </div>
	                            </div>
	                            <div class="col-sm-4">
					              <div class="input-group m-b">
					                <span class="input-group-addon">发送类型</span>
					                    <select id="type" name="type" class="ipt form-control">
											<option value="" selected="selected">-----请选择------</option>
											<option value="DEFAULT_TYPE">默认类型</option>
											<option value="FORGETPWD_TYPE">忘记密码类型</option>
											<option value="BIND_EMAIL_TYPE">绑定手机类型</option>
											<option value="CHANGE_EMAIL_TYPE">修改绑定手机类型</option>
										</select>
					              </div>
					           </div>
	                            <div class="col-sm-4">
	                                <div class="input-group m-b">
	                                    <span class="input-group-addon">用户名</span>
	                                    <input type="text" id="userName"  name="userName" class="form-control"></div>
	                            </div>
	                            <div class="col-sm-4">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">手机号码</span>
                                    <input type="text" id="mobile"  name="mobile" class="form-control"></div>
                                </div>
	                            <div class="col-sm-4">
								<div class="input-group m-b">
									<span class="input-group-addon">时间：</span> 
									<input	class="form-control layer-date" placeholder="开始时间"	onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"	name="startDate" id="startDate"> 
									<span class="input-group-addon">到</span> 
									<input class="form-control layer-date" placeholder="结束时间"	onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"	name="endDate" id="endDate">
								</div>
							</div>
	                            <div class="col-sm-4">
	                                <div class="form-group nomargin text-right">
	                                    <button type="button" class="btn btn-outline btn-default btn-js" onclick="smsSendRecordPage.findRecordControl()"><i class="fa fa-search"></i>&nbsp;查 询</button>
	                                   <!--  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	                                    <button type="button" class="btn btn-outline btn-default btn-add" onclick="smsSendRecordPage.addRecordControl()"><i class="fa fa-plus"></i>&nbsp;新 增</button> -->
	                                 </div>
	                            </div>
	                        </c:if>
                         </div>
                    </form>
                    </div>
                     <div class="jqGrid_wrapper">
                        <div class="ui-jqgrid ">
                            <div class="ui-jqgrid-view">
                                <div class="ui-jqgrid-hdiv">
                                    <div class="ui-jqgrid-hbox" style="padding-right:0">
                                        <table id="smsSendRecordPageTable" class="ui-jqgrid-htable ui-common-table table table-bordered">
                                            <thead>
                                                <tr class="ui-jqgrid-labels">
                                                	<th class="ui-th-column ui-th-ltr">编号</th>
                                                    <th class="ui-th-column ui-th-ltr">商户</th>
                                                    <th class="ui-th-column ui-th-ltr">用户名</th>
                                                    <th class="ui-th-column ui-th-ltr">IP地址</th>
                                                    <th class="ui-th-column ui-th-ltr">短信类型</th>
                                                   <!--  <th class="ui-th-column ui-th-ltr">session_id</th> -->
                                                    <th class="ui-th-column ui-th-ltr">手机号码</th>
													<th class="ui-th-column ui-th-ltr">验证码</th>
													<th class="ui-th-column ui-th-ltr">备注</th>
													<th class="ui-th-column ui-th-ltr">创建时间</th>
													<th class="ui-th-column ui-th-ltr">操 作</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>     
		         </div>
		      </div>
            <script type="text/javascript" src="<%=path%>/admin/common/common.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
		    <!-- dwr -->
		    <script type='text/javascript' src='<%=path%>/dwr/interface/managerSmsSendRecordAction.js'></script>
		    <script type="text/javascript" src="<%=path%>/admin/sms_send_record/js/sms_send_record.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    </body>
</html>
