function SmsSendRecordPage(){}
var smsSendRecordPage = new SmsSendRecordPage();

$("#bizSystem").attr("disabled", true);//系统下拉框禁止编辑

/**
 * 查询参数
 */
smsSendRecordPage.queryParam = {
		id:null,
		bizSystem : null,
		mobile : null,
		ip: null,
		sessionId:null,
		userName:null,
		type:null,
		vcode:null,
		remark:null,
		createDate:null,
		startDate:null,
		endDate:null
};
smsSendRecordPage.param = {
//	skipAwardZh:0  
}

$(document).ready(function() {
	smsSendRecordPage.queryParam.id = id; // 获取查询的ID
	if (id != null) {
		smsSendRecordPage.editRecordControl(id);
	}
});

SmsSendRecordPage.prototype.selectByPrimary=function(){
	var id=smsSendRecordPage.queryParam.id;
	var bizSystem=$("#bizSystem").val();
	var userName=$("#userName").val();
	var ip=$("#ip").val();
	var type=$("#type").val();
	var mobile=$("#mobile").val();
	var vcode=$("#vcode").val();
	var remark=$("#remark").val();
	var createDate=$("#createDate").val();
	smsSendRecordPage.queryParam.id=id;
	smsSendRecordPage.queryParam.bizSystem=bizSystem;
	smsSendRecordPage.queryParam.userName=userName;
	smsSendRecordPage.queryParam.ip=ip;
	smsSendRecordPage.queryParam.type=type;
	smsSendRecordPage.queryParam.mobile=mobile;
	smsSendRecordPage.queryParam.vcode=vcode;
	smsSendRecordPage.queryParam.remark=remark;
	smsSendRecordPage.queryParam.createDate=createDate;
		if(smsSendRecordPage.queryParam.id!=null||smsSendRecordPage.queryParam.id!=""){
			managerSmsSendRecordAction.updateSmsSendRecord(smsSendRecordPage.queryParam,function(r){
				if(r[0]=="ok"){
					swal({title:"修改成功！",
				        text:"已成功修改数据",
				        type:"success"},
				      function(){
				        	var index = parent.layer.getFrameIndex(window.name); //获取窗口索引    
				        	parent.smsSendRecordPage.layerClose(index);
					  }
				     )				
				}else{
					swal(r[1]);
				}
			});
		}else{
			managerSmsSendRecordAction.insertSmsSendRecord(smsSendRecordPage.queryParam,function(r){
				if(r[0]=="ok"){
					swal({title:"保存成功！",
				        text:"已成功保存数据",
				        type:"success"},
				      function(){
				        	var index = parent.layer.getFrameIndex(window.name); //获取窗口索引    
				        	parent.smsSendRecordPage.layerClose(index);
					  }
				     )	
				}else{
					swal(r[1]);
				}
			});
		}
		smsSendRecordPage.queryParam={};
}

/**
 * 编辑时赋值
 */
SmsSendRecordPage.prototype.editRecordControl = function(id) {
	managerSmsSendRecordAction.selectSmsSendRecord(id, function(r) {
		if (r[0] != null && r[0] == "ok") {
			$("#id").val(r[1].id);
			$("#bizSystem").val(r[1].bizSystem);
			$("#userName").val(r[1].userName);
			$("#ip").val(r[1].ip);
			$("#type").val(r[1].type);
			$("#mobile").val(r[1].mobile);
			$("#vcode").val(r[1].vcode);
			$("#remark").val(r[1].remark);
		} else if (r[0] != null && r[0] == "error") {
			showErrorDlg(jQuery(document.body), r[1]);
		} else {
			showErrorDlg(jQuery(document.body), "获取数据失败.");
		}
	});
};