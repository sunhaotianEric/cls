function SmsSendRecordPage(){}
var smsSendRecordPage = new SmsSendRecordPage();

/**
 * 查询参数
 */
smsSendRecordPage.queryParam = {
		id:null,
		bizSystem : null,
		mobile : null,
		ip: null,
		sessionId:null,
		userName:null,
		type:null,
		vcode:null,
		remark:null,
		createDate:null,
		startDate:null,
		endDate:null
};
//分页参数
smsSendRecordPage.pageParam = {
    pageNo : 1,
    pageSize : defaultPageSize
};

$(document).ready(function() {
	smsSendRecordPage.initTableData();
});

SmsSendRecordPage.prototype.initTableData = function() {
	smsSendRecordPage.queryParam = getFormObj($("#smsSendRecordForm"));
	smsSendRecordPage.table = $('#smsSendRecordPageTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"bPaginate" : false, 
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			smsSendRecordPage.pageParam.pageSize = data.length;//页面显示记录条数
			smsSendRecordPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerSmsSendRecordAction.getAllSmsSendRecord(smsSendRecordPage.queryParam,smsSendRecordPage.pageParam,function(r){
		//封装返回数据
		var returnData = {
			recordsTotal : 0,
			recordsFiltered : 0,
			data : null
		};
		if (r[0] != null && r[0] == "ok") {
			returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
			returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
			returnData.data = r[1].pageContent;;//返回的数据列表
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("查询信息失败.");
		}
		callback(returnData);
			});
		},
		"columns": [
            {"data": "id"},
            {
            	"data": "bizSystemName",
            	"bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false,
            },
            {"data": "userName"},
            {"data": "ip"},
            {"data": "typeName"},
            /*{"data": "type",
            	render: function(data, type, row, meta) {
            		if (data == "DEFAULT_TYPE") {
            			return "默认类型";
            		} else if(data == "FORGETPWD_TYPE"){
            			return "忘记密码类型";
            		} else if(data == "BIND_PHONE_TYPE"){
            			return "绑定手机类型";
            		} else if(data == "CHANGE_PHONE_TYPE"){
            			return "修改绑定手机类型";
            		} else if(data == "REG_TYPE"){
            			return "注册类型";
            		}
            		else {
            			return data;
            		}
            	}
            },*/
            {"data": "mobile"},
            {"data": "vcode"},
            {"data": "remark",	
            	render: function(data, type, row, meta) {
            		if (data == null || data=="") {
            			return "暂未备注";
            		} else {
            			return data;
            		}
            	}
            },
            {   
            	"data": "createDate",
            	render: function(data, type, row, meta) {
            		return dateFormat(data,'yyyy-MM-dd hh:mm:ss');
        		}
            },
            {
            	 "data": "id",
            	 render: function(data, type, row, meta) {
            		 return "<a class='btn btn-info btn-sm btn-bj' onclick='smsSendRecordPage.editRecordControl("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;" +
            		 		"<a class='btn btn-danger btn-sm btn-del' onclick='smsSendRecordPage.delRecordControl("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";
            	 }
            }
        ]
}).api(); 
}

/**
 * 查询数据
 */
SmsSendRecordPage.prototype.findRecordControl=function(){
	var bizSystem=$("#bizSystem").val();
	var type = $("#type").val();
	var userName = $("#userName").val();
	var mobile = $("#mobile").val();
	var startDate=$("#startDate").val();
	var endDate=$("#endDate").val();
	if(bizSystem==null||bizSystem==""){
		smsSendRecordPage.queryParam.bizSystem=null;
	}else{
		smsSendRecordPage.queryParam.bizSystem=bizSystem;
	}
	if(type==null||type==""){
		smsSendRecordPage.queryParam.type=null;
	}else{
		smsSendRecordPage.queryParam.type=type;
	}
	if(userName==null||userName==""){
		smsSendRecordPage.queryParam.userName=null;
	}else{
		smsSendRecordPage.queryParam.userName=userName;
	}
	if(mobile==null||mobile==""){
		smsSendRecordPage.queryParam.mobile=null;
	}else{
		smsSendRecordPage.queryParam.mobile=mobile;
	}
	if(startDate==null||startDate==""){
		smsSendRecordPage.queryParam.startDate=null;
	}else{
		smsSendRecordPage.queryParam.startDate=new Date(startDate);
	}
	if(endDate==null||endDate==""){
		smsSendRecordPage.queryParam.endDate=null;
	}else{
		smsSendRecordPage.queryParam.endDate=new Date(endDate);
	}
	smsSendRecordPage.table.ajax.reload();
}

SmsSendRecordPage.prototype.delRecordControl =function(id){
	  swal({
          title: "您确定要删除这条信息吗",
          text: "请谨慎操作！",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          cancelButtonText: "取消",
          confirmButtonText: "删除",
          closeOnConfirm: false
      },
      function() {
    	  managerSmsSendRecordAction.deleteSmsSendRecord(id,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "删除成功",type: "success"});
				smsSendRecordPage.findRecordControl();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("删除失败.");
			}
	    });
	});
};

/**
 * 编辑
 */
SmsSendRecordPage.prototype.editRecordControl=function(id){
	layer.open({
        type: 2,
        title: '短信发送记录编辑',
        maxmin: false,
        shadeClose: true,
        //点击遮罩关闭层
       /* area: ['600px', '450px'],*/
        area: ['70%', '80%'],
        content: contextPath + "/managerzaizst/sms_send_record/sms_send_record_edit.html?id="+id,
        cancel: function(index){ 
        	smsSendRecordPage.table.ajax.reload();
     	}
    });
}


SmsSendRecordPage.prototype.layerClose=function(index){
	layer.close(index);
	smsSendRecordPage.findRecordControl();
}