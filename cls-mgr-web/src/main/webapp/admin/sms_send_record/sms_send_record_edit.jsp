<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
	String id=request.getParameter("id");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>短信发送记录编辑</title>
        <link rel="shortcut icon" href="favicon.ico">
        <jsp:include page="/admin/include/include.jsp"></jsp:include>
        <script type="text/javascript">
        	var id=<%=id%>;
        </script>
<body>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeIn">
		<form action="javascript:void(0)">
			<div class="row">
				<c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
					<div class="col-sm-12">
						<div class="input-group m-b">
							<span class="input-group-addon">商户：</span>
							<cls:bizSel name="bizSystem" id="bizSystem"	options="class:ipt form-control" />
						</div>
					</div>
				</c:if>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">用户名：</span>
						<input type="text" id="userName" name="userName" readonly="readonly" class="form-control" />
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">IP地址：</span>
						<input type="text" name="ip" id="ip" readonly="readonly" class="form-control" />
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">手机号码：</span>
						<input type="text" id="mobile" name="mobile" readonly="readonly" class="form-control" />
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">验证码：</span>
						<input type="text" id="vcode" name="vcode" class="form-control" />
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">备注：</span>
						<input type="text" id="remark" class="form-control" name="remark" />
					</div>
				</div>		
				<div class="col-sm-12">
						<div class="col-sm-6 text-center">
							<button type="button" class="btn btn-w-m btn-white"
								onclick="smsSendRecordPage.selectByPrimary()">提 交</button>
							<input type="reset" class="btn btn-w-m btn-white" value="重 置" />
						</div>
				</div>
				
			</div>
		</form>
	</div>
	<script type="text/javascript" src="<%=path%>/admin/common/common.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
	<!-- dwr -->
	<script type='text/javascript' src='<%=path%>/dwr/interface/managerSmsSendRecordAction.js'></script>
	<script type="text/javascript" src="<%=path%>/admin/sms_send_record/js/sms_send_record_edit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
</body>
</html>