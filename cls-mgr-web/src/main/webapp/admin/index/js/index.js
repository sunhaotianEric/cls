function IndexPage() {
}
var indexPage = new IndexPage();

indexPage.param = {

};

/**
 * 刷新充值提现订单号相关公共变量与方法
 */
var refreshRechargeKeyTime = 5;
var refreshWithdrawKeyTime = 5;
var rechargeOrderSerialNumber = null;
var rechargeOrderSerialNumberNew = null;
var withdrawOrderSerialNumber = null;
var rechargeOrderTable;
var withdrawOrderTable;
var refreshKey;
var RechargeSoundControl = 1;
var WithdrawSoundControl = 1;
var refreshrechargeOrderKey = function() {
	/*var param = {}
	param.operateType = "RECHARGE";
	managerUptodateSerialNumberAction.getUptodateSerialNumber(param, function(r) {
		if (r[0] != null && r[0] == "ok") {
			if (r[1] != null && r[1] != "") {
				if (rechargeOrderSerialNumber == null) {
					rechargeOrderSerialNumber = r[1];
				} else {
					if (rechargeOrderSerialNumber != r[1]) {
						if (currentUser.bizSystem != "SUPER_SYSTEM") {
							if (RechargeSoundControl == 1) {
								$("#voiceAutoMsgRecharge")[0].play();
							}
						}
						rechargeOrderSerialNumber = r[1];
						$("div.page-tabs-content").find("a").each(function() {
							var url = $(this).attr("data-id");
							if (url == "rechargeorder.html") {
								rechargeOrderTable.ajax.reload();
							}
						})
					}
				}
			}

		} else if (r[0] != null && r[0] == "error") {
			swal(r[1]);
		} else {
			swal("查询数据失败.");
		}
	})*/
	
	var paramNew = {

	};
	managerRechargeOrderAction.getUptodateSerialNumber(paramNew, function(r) {
		if (r[0] != null && r[0] == "ok") {
			if (r[1] != null && r[1] != "") {
				if (rechargeOrderSerialNumberNew == null) {
					rechargeOrderSerialNumberNew = r[1];
				} else {
					if (rechargeOrderSerialNumberNew != r[1]) {
						if (currentUser.bizSystem != "SUPER_SYSTEM") {
							if (RechargeSoundControl == 1) {
								$("#voiceAutoMsgRecharge")[0].play();
							}
						}
						rechargeOrderSerialNumberNew = r[1];
						$("div.page-tabs-content").find("a").each(function() {
							var url = $(this).attr("data-id");
							if (url == "rechargeorder.html") {
								rechargeOrderTable.findRechargeorder();
							}
						})
					}
				}
			}

		} else if (r[0] != null && r[0] == "error") {
			swal(r[1]);
		} else {
			swal("查询数据失败.");
		}
	});
}
var refreshWithdrawOrderKey = function() {
	var param = {}
	/*param.operateType = "WITHDRAW";*/
	managerWithDrawOrderAction.getUptodateSerialNumber(param, function(r) {
		if (r[0] != null && r[0] == "ok") {
			if (r[1] != null && r[1] != "") {
				if (withdrawOrderSerialNumber == null) {
					withdrawOrderSerialNumber = r[1];
				} else {
					if (withdrawOrderSerialNumber != r[1]) {
						if (currentUser.bizSystem != "SUPER_SYSTEM") {
							if (WithdrawSoundControl == 1) {
								$("#voiceAutoMsgWithdraw")[0].play();
							}
						}
						withdrawOrderSerialNumber = r[1];
						$("div.page-tabs-content").find("a").each(function() {
							var url = $(this).attr("data-id");

							if (url == "withdraworder.html") {
								withdrawOrderTable.findWithDraw();
							}
						})

					}
				}

			}

		} else if (r[0] != null && r[0] == "error") {
			swal(r[1]);
		} else {
			swal("查询数据失败.");
		}
	})
}

// 重新设置定时器
function resetRechargeOrderInterval() {
	window.clearInterval(refreshKey);
	refreshKey = window.setInterval("refreshrechargeOrderKey()", refreshRechargeKeyTime * 1000);
}

// 重新设置定时器
function resetWithdrawOrderInterval() {
	window.clearInterval(refreshKey);
	refreshKey = window.setInterval("refreshWithdrawOrderKey()", refreshRechargeKeyTime * 1000);
}

$(document).ready(function() {

	$('#page-wrapper').children().not(".content-tabs, .footer").wrapAll("<div class='scroll-wrapper'></div>")

	$("#logoutBtn").click(function() {
		swal({
			title : "您确定要退出登陆么",
			type : "warning",
			showCancelButton : true,
			confirmButtonColor : "#DD6B55",
			cancelButtonText : "取消",
			confirmButtonText : "确定",
			closeOnConfirm : false
		}, function() {
			// 退出处理
			managerAdminAction.logout(function(r) {
				if (r[0] != null && r[0] == "ok") {
					indexPage.toLogin();
				} else if (r[0] != null && r[0] == "error") {
					swal(r[1]);
				} else {
					swal("操作失败.");
				}
			});
		})
	});
	indexPage.getRecvNote(2);
	indexPage.getAlarmInfo();
	refreshKey = window.setInterval("refreshrechargeOrderKey()", refreshRechargeKeyTime * 1000);
	refreshKey = window.setInterval("refreshWithdrawOrderKey()", refreshRechargeKeyTime * 1000);
	window.setInterval("indexPage.getRecvNote(2)", 60 * 1000);
	window.setInterval("indexPage.getAlarmInfo()", 10 * 1000);
});

IndexPage.prototype.closeUpdatePwd = function(index) {
	layer.close(index);
	swal({
		title : "保存成功",
		text : "您已成功修改密码。",
		type : "success"
	});
	setTimeout("indexPage.toLogin()", 3000);
}

IndexPage.prototype.toLogin = function() {
	window.location.href = contextPath + "/managerzaizst/login.html";
}

IndexPage.prototype.toEmail = function(root) {
	if (root == 1) {
		$(".count-info").click();
		$(".tip-close" + root).click();
	} else if (root == 2) {
		$("#alarminfo").click();
		$(".tip-close" + root).click();
	}
}

var ids = 0;
IndexPage.prototype.getRecvNote = function(root) {
	if (currentUser.bizSystem != 'SUPER_SYSTEM') {
		managerAdminRecvNoteAction.getAllAdminRecvNoteBytomainid("未读", function(r) {
			if (r[0] == "ok") {
				if (root == 2) {
					$(r[1]).each(function(i, idx) {
						if (i == 0) {
							if (ids == 0) {
								ids = idx.id;
							} else if (ids == idx.id) {
								return false;
							}
							showTip("" + idx.sub + "", "" + idx.content + "", idx.id, true, false, 1);
							return false;
						}
					});
				}
				if (r[1].length != 0) {
					$("#email").html(r[1].length);
				} else {
					$("#email").html("");
				}
			}
		});
	}
}

IndexPage.prototype.getAlarmInfo = function() {
	if (currentUser.bizSystem != 'SUPER_SYSTEM') {
		managerAlarmInfoAction.queryAlarmInfoBytime(function(r) {
			if (r[0] == "ok") {
				$(r[1]).each(function(i, idx) {
					var str = "";
					if (idx.alarmType == "TZ") {
						str = "投注";
					} else if (idx.alarmType == "ZJ") {
						str = "中奖";
					} else if (idx.alarmType == "CZ") {
						str = "充值";
					} else if (idx.alarmType == "TX") {
						str = "提现";
					}
					showTip("" + str + "提醒", "" + idx.alarmContent + "", idx.id, true, false, 2);
				});
			}
		});
	}
}

IndexPage.prototype.gotohref = function(link, tabName) {
	var el = parent.document.getElementById('menuItemUtil');

	el.href = link;
	el.text = tabName;
	el.click();
	if ($(el).text() == "邮件") {
		$(el).html('<i class="fa fa-envelope"></i><span class="label label-warning" id="email" style="right:2px;top:2px;"></span>');
		indexPage.getRecvNote(1);
	}
}