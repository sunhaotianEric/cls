  <%@ page language="java" contentType="text/html; charset=UTF-8"
           pageEncoding="UTF-8" %>
    <%@ page import="com.team.lottery.system.SystemConfigConstant" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
      <%
	String path = request.getContextPath();
%>
    <!DOCTYPE html>
    <html>

    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <title>后台管理商户</title>
    <link rel="shortcut icon" href="favicon.ico">
    <style type="text/css">
    .ac a, .ac a:hover {color: #999c9e;}
    .p0 * {padding: 0 !important;}
    .db {display: block;}
    .link-block a{display: inline-block;width: 100%;}
    .count-info .label {padding: 1px 5px !important;}
    </style>

    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    <style type="text/css">
    .tip{position:fixed;bottom:20px;right:20px;background:#fff;border-radius:6px;-webkit-border-radius:6px;padding:16px
    32px;z-index:999;box-shadow:1px 0 11px rgba(0,0,0,0.15);-webkit-box-shadow:1px 0 11px rgba(0,0,0,0.15);}
    .tip:after{content: "020"; display: block; height: 0; clear: both;visibility:hidden;}
    .tip .tip-close1{position:absolute;top:6px;right:8px;width:20px;cursor:pointer;}
    .tip .tip-close2{position:absolute;top:6px;right:8px;width:20px;cursor:pointer;}
    .tip .tip-logo{position:relative;width:48px;text-align:center;float:left;}
    .tip .tip-logo img{max-width:84%;vertical-align:middle;}
    .tip .tip-content{padding:0px 2px 4px 12px;width:100%;float:left;}
    .tip .tip-content .title{line-height:24px;font-size:16px;color:#000;}
    .tip .tip-content p{font-size:12px;color:#000;line-height:18px;}
    .tip{-webkit-animation-name: fadeInUp;animation-name: fadeInUp;-webkit-animation-duration: 1s;animation-duration:
    1s;-webkit-animation-fill-mode: both;animation-fill-mode: both;}
    .tip.out{-webkit-animation-name: fadeOutUp;animation-name: fadeOutUp;-webkit-animation-duration:
    1s;animation-duration: 1s;-webkit-animation-fill-mode: both;animation-fill-mode: both;}
    @-webkit-keyframes fadeInUp {
    from {opacity: 0;-webkit-transform: translate3d(0, 100%, 0)transform: translate3d(0, 100%, 0);}to {opacity:
    1;-webkit-transform: none;transform: none;}
    }
    @keyframes fadeInUp {
    from {opacity: 0;-webkit-transform: translate3d(0, 100%, 0);transform: translate3d(0, 100%, 0);}to {opacity:
    1;-webkit-transform: none;transform: none;}
    }
    /*fadeOutUp*/
    @-webkit-keyframes fadeOutUp {
    to {opacity: 0;-webkit-transform: translate3d(0, 100%, 0)transform: translate3d(0, 100%, 0);}
    from {opacity: 1;-webkit-transform: none;transform: none;}
    }
    @keyframes fadeOutUp {
    to {opacity: 0;-webkit-transform: translate3d(0, 100%, 0);transform: translate3d(0, 100%, 0);}
    from {opacity: 1;-webkit-transform: none;transform: none;}
    }
    </style>
    </head>

    <body onload="show()" class="fixed-sidebar full-height-layout gray-bg" style="overflow:hidden">
    <div id="wrapper">


    <!--左侧导航开始-->
    <nav class="navbar-default navbar-static-side" role="navigation">
    <div class="nav-close">
    <i class="fa fa-times-circle"></i>
    </div>
    <div class="sidebar-collapse">
    <ul class="nav" id="side-menu">
    <li class="nav-header">
    <div class="dropdown profile-element">
    <span>
    <img alt="image" class="img-circle" src="<%=path%>/img/logo.png" /></span>
    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
    <span class="clear" style="margin-left:20px">
    <span class="block m-t-xs">
    <strong class="font-bold"><c:out value="${admin.username}"></c:out><b class="caret"></b></strong></span>
    <small class="text-xs block" id="time"></small>
    </span>
    </a>
    <ul class="dropdown-menu animated fadeInRight m-t-xs">
    <li><a class="J_menuItem update_pwd" target="Conframe">修改密码</a>
    </li>
    </ul>
    </div>
    <div class="logo-element"></div></li>


    <%-- ===================超级后台菜单==================================================================== --%>
    <c:if test="${admin.bizSystem == 'SUPER_SYSTEM'}">
      <li>
      <a href="#">
      <i class="fa fa-opencart"></i>
      <span class="nav-label">商户管理</span>
      <span class="fa arrow"></span>
      </a>
      <ul class="nav nav-second-level">
      <li><a class="J_menuItem" href="bizsystem.html" target="Conframe">商户管理</a></li>
      <%--<c:if test="${admin.state < 3}">
        <li><a class="J_menuItem" href="platformchargeorder.html" target="Conframe">商户收费管理</a></li>
      </c:if>--%>

      <li><a class="J_menuItem" href="bizsystemdomain.html" target="Conframe">域名管理</a></li>
      <li><a class="J_menuItem" href="adminDomain.html" target="Conframe">后台域名IP授权管理</a></li>
     <%-- <li><a class="J_menuItem" href="domaininvitationcode.html" target="Conframe">域名邀请码绑定</a></li>--%>
      <%--<li><a class="J_menuItem" href="bizsysteminfo.html" target="Conframe">网站内容管理</a></li>--%>
      </ul>
      </li>


      <li>
      <a href="#">
      <i class="fa fa-pie-chart"></i>
      <span class="nav-label">平台报表</span>
      <span class="fa arrow"></span>
      </a>
      <ul class="nav nav-second-level">
      <li><a class="J_menuItem" href="dayprofit/dayprofit_bizsystem.html" target="Conframe">商户盈亏总览</a></li>
      <li><a class="J_menuItem" href="dayprofit.html" target="Conframe">商户盈亏报表</a></li>
      <!-- <li><a class="J_menuItem" href="user/user_account_report.html" target="Conframe">用户总额报表</a></li> -->
      <li><a class="J_menuItem" href="user/user_statistical_rankings.html" target="Conframe">统计排行报表</a></li>
      <li><a class="J_menuItem" href="user_self_gain_report.html" target="Conframe">用户盈利报表</a></li>
      <%--<li><a class="J_menuItem" href="agent_report/agent_report.html" target="Conframe">代理报表</a></li>--%>
      <li><a class="J_menuItem" href="daylotterygain/daylotterygain.html" target="Conframe">彩种报表</a></li>
<%--      <li><a class="J_menuItem" href="lhc_order_report.html" target="Conframe">六合彩注单报表</a></li>--%>
      <%--<li><a class="J_menuItem" href="firstrechargeorder.html" target="Conframe">首充报表</a></li>--%>
      <%--<li><a class="J_menuItem" href="rechargereport.html" target="Conframe">充值报表</a></li>--%>
      <!-- <li><a class="J_menuItem" href="rechargereportold/rechargereport" target="Conframe">旧充值报表</a></li> -->
      <%--<li><a class="J_menuItem" href="withdrawreport.html" target="Conframe">取现报表</a></li>--%>
      <!-- <li><a class="J_menuItem" href="withdrawreportold/withdrawreport.html" target="Conframe">旧取现报表</a></li> -->
      <li><a class="J_menuItem" href="user/user_day_online_info.html" target="Conframe">在线统计报表</a></li>
      <c:if test="${admin.state < 3}">
        <!-- <li><a class="J_menuItem" href="bizsystem/platformincomeoverview.html" target="Conframe">平台收入总览</a></li>
        -->
        <li><a class="J_menuItem" href="platform_month_charge.html" target="Conframe">平台收入总览</a></li>
      </c:if>
      </ul>
      </li>

        <c:if test="${admin.state <= 1|| admin.state==3}">
      <li>
      <a href="#">
      <i class="fa fa-database"></i>
      <span class="nav-label">彩票管理</span>
      <span class="fa arrow"></span>
      </a>
      <ul class="nav nav-second-level">
      <li><a class="J_menuItem" href="hand_update.html" target="Conframe">彩票开奖处理</a></li>
      <li><a class="J_menuItem" href="hand_update/hand_update_selfkind.html" target="Conframe">私彩开奖处理</a></li>
      <li><a class="J_menuItem" href="lottery_code_xy_import/lottery_code_xy_import_list.html" target="Conframe">私彩开奖号码导入</a></li>
      <li><a class="J_menuItem" href="interface_check.html" target="Conframe">开奖接口查看</a></li>
      <li><a class="J_menuItem" href="opencodeapi.html" target="Conframe">开奖接口管理</a></li>
      <li><a class="J_menuItem" href="lottery_code.html" target="Conframe">开奖号码管理</a></li>
      <li><a class="J_menuItem" href="lottery_code_xy.html" target="Conframe">私彩开奖号码查询</a></li>
      <li><a class="J_menuItem" href="lottery_win.html" target="Conframe">彩票奖金管理</a></li>
      <li><a class="J_menuItem" href="lottery_issue.html" target="Conframe">开奖时间管理</a></li>
      <li><a class="J_menuItem" href="lhc_issue_config.html" target="Conframe">六合彩期号设定</a></li>
      <!-- <li><a class="J_menuItem" href="lotterywinlhc.html" target="Conframe">六合彩赔率设定</a></li> -->
      <li><a class="J_menuItem" href="lotterywinlhc/lotterywinlhcnew.html" target="Conframe">六合彩赔率设定</a></li>
      <li><a class="J_menuItem" href="lotterywinxyeb.html" target="Conframe">幸运28赔率设定</a></li>
      <li><a class="J_menuItem" href="lottery_control_config/lottery_control_config.html"
      target="Conframe">私彩开奖模式设定</a></li>
      <!-- <li><a class="J_menuItem" href="lottery_set.html" target="Conframe">全局彩种开关设置</a></li> -->
      <li><a class="J_menuItem" href="lottery_switch.html" target="Conframe">全局彩种开关设置</a></li>
      <!-- <li><a class="J_menuItem" href="lottery_set/lottery_set_bizsystem.html" target="Conframe">商户彩种开关设置</a></li> -->
  	  <li><a class="J_menuItem" href="lottery_switch/lottery_switch_bizsystem.html" target="Conframe">商户彩种开关设置</a></li>
          <li><a class="J_menuItem" href="lottery_switch/lottery_closing_time_bizsystem.html" target="Conframe">彩种封盘时间设置</a></li>
      <li><a class="J_menuItem" href="game_rule/game_rule.html" target="Conframe">玩法规则管理</a></li>
      </ul>
      </li>
        </c:if>
      
<%--      <li>
      <a href="#">
      <i class="fa fa-magic"></i>
      <span class="nav-label">充值取现管理</span>
      <span class="fa arrow"></span>
      </a>
      <ul class="nav nav-second-level">
      <li><a class="J_menuItem" href="rechargeorder.html" target="Conframe">充值订单</a></li>
      <!-- <li><a class="J_menuItem" href="rechargeorderold/rechargeorder.html" target="Conframe">旧充值订单</a></li> -->
      <li><a class="J_menuItem" href="withdraworder.html" target="Conframe">提现订单</a></li>
      <!-- <li><a class="J_menuItem" href=withdraworderold/withdraworder.html" target="Conframe">旧提现订单</a></li> -->
      <li><a class="J_menuItem" href="thirdpayconfig.html" target="Conframe">第三方支付设置</a></li>
      <li><a class="J_menuItem" href="quickbanktype.html" target="Conframe">第三方支付码表设置</a></li>
      <li><a class="J_menuItem" href="quickchargeset.html" target="Conframe">第三方支付商户管理</a></li>
      <li><a class="J_menuItem" href="paybank.html" target="Conframe">收款银行设置</a></li>
      <li><a class="J_menuItem" href="rechargeconfig.html" target="Conframe">充值设置</a></li>
      <li><a class="J_menuItem" href="withdrawautoconfig.html" target="Conframe">自动出款设置</a></li>
      <li><a class="J_menuItem" href="withdrawautolog/withdrawautolog.html" target="Conframe">自动出款日志</a></li>
      <li><a class="J_menuItem" href="paynotifylog.html" target="Conframe">第三方支付通知日志</a></li>
      </ul>
      </li>--%>
      
      
      <li>
      <a href="#">
      <i class="fa fa-home"></i>
      <span class="nav-label">平台设置管理</span>
      <span class="fa arrow"></span>
      </a>
      <ul class="nav nav-second-level">
      <!-- <li><a class="J_menuItem" href="systemconfig.html" target="Conframe">平台参数设置</a></li> -->
      <li><a class="J_menuItem" href="systemconfig/systemconfig_set.html" target="Conframe">平台参数设置</a></li>
      <li><a class="J_menuItem" href="bizsystemconfig/bizsystemconfig_set.html" target="Conframe">商户参数配置</a></li>
      <%--<li><a class="J_menuItem" href="level_sytem/level_sytem.html" target="Conframe">等级机制管理</a></li>--%>
      <!-- <li><a class="J_menuItem" href="systemconfig/systemconfig.html" target="Conframe">平台参数设置22</a></li>
      <li><a class="J_menuItem" href="bizsystemconfig.html" target="Conframe">商户参数配置22</a></li> -->
      <li><a class="J_menuItem" href="quotacontrol.html" target="Conframe">配额配置</a></li>
      <!-- <li><a class="J_menuItem" href="user_bonus_config.html" target="Conframe">代理福利参数设置</a></li> -->
      <li><a class="J_menuItem" href="treatmentapplymanual.html" target="Conframe">定时任务手工结算</a></li>
      <!-- <li><a class="J_menuItem" href="user_vip_level_config.html" target="Conframe">会员特权参数设置</a></li> -->
      <!-- <li><a class="J_menuItem" href="domain.html" target="Conframe">商户域名设置</a></li> -->
      <!-- <li><a class="J_menuItem" href="recordlog.html" target="Conframe">操作记录查询</a></li> -->


     <!--  <li><a class="J_menuItem" href="chatroomconfig.html" target="Conframe">聊天室数据库配置</a></li> -->
      <!-- <li><a class="J_menuItem" href="download_file.html" target="Conframe">日志文件下载</a></li> -->

      <%--<li><a class="J_menuItem" href="email_send_record/email_send_record.html" target="Conframe">邮件发送记录</a></li>
      <li><a class="J_menuItem" href="email_send_config/email_send_config.html" target="Conframe">邮件发送配置</a></li>
      <li><a class="J_menuItem" href="sms_send_record/sms_send_record.html" target="Conframe">短信发送记录</a></li>
      <li><a class="J_menuItem" href="sms_send_config/sms_send_config.html" target="Conframe">短信发送配置</a></li>--%>
      </ul>
      </li>
      
      
     <%-- <li>
      <a href="#">
      <i class="fa fa-bullhorn"></i>
      <span class="nav-label">商户信息管理</span>
      <span class="fa arrow"></span>
      </a>
      <ul class="nav nav-second-level">
      <li><a class="J_menuItem" href="homepic.html" target="Conframe">首页图片管理</a></li>
      <li><a class="J_menuItem" href="announce.html" target="Conframe">网站公告</a></li>
      <li><a class="J_menuItem" href="help.html" target="Conframe">帮助管理</a></li>
      <li><a class="J_menuItem" href="helps/helps.html" target="Conframe">帮助中心</a></li>
      <li><a class="J_menuItem" href="activity.html" target="Conframe">活动管理</a></li>
      <li><a class="J_menuItem" href="day_consume_activity_config.html" target="Conframe">流水返送设置</a></li>
      <li><a class="J_menuItem" href="day_consume_activity_order.html" target="Conframe">流水返送订单</a></li>
      <li><a class="J_menuItem" href="signinconfig.html" target="Conframe">签到设置</a></li>
      <li><a class="J_menuItem" href="signinrecord.html" target="Conframe">签到记录</a></li>
      <li><a class="J_menuItem" href="promotionrecord.html" target="Conframe">晋级奖励金额</a></li>
      <li><a class="J_menuItem" href="new_user_gift_task.html" target="Conframe">新手活动任务查询</a></li>
      <li><a class="J_menuItem" href="alarminfo.html" target="Conframe">告警信息查询</a></li>
      <li><a class="J_menuItem" href="sitesearch.html" target="Conframe">站内信管理</a></li>
      <li><a class="J_menuItem" href="feedback.html" target="Conframe">反馈信息管理</a></li>
      <li><a class="J_menuItem" href="course.html" target="Conframe">教程管理</a></li>
      <li><a class="J_menuItem" href="dayprofitrank.html" target="Conframe">中奖榜单管理</a></li>
      </ul>
      </li>--%>
        <c:if test="${admin.bizSystem == 'SUPER_SYSTEM'}">
      <li>
      <a href="#">
      <i class="fa fa-user-secret "></i>
      <span class="nav-label">管理员信息管理</span>
      <span class="fa arrow"></span>
      </a>
      <ul class="nav nav-second-level">
      <li><a class="J_menuItem" href="admin.html" target="Conframe">管理员管理</a></li>
      <li><a class="J_menuItem" href="adminoperatelog.html" target="Conframe">管理员操作记录</a></li>
      <li><a class="J_menuItem" href="adminlog.html" target="Conframe">管理员登陆日志查询</a></li>
      </ul>
      </li>
        </c:if>
      <!-- <li>
      <a href="#">
      <i class="fa fa-comments-o"></i>
      <span class="nav-label">聊天室管理</span>
      <span class="fa arrow"></span>
      </a>
      <ul class="nav nav-second-level">
      <li><a class="J_menuItem" href="chatroom.html" target="Conframe">聊天房间管理</a></li>
      <li><a class="J_menuItem" href="chatlog.html" target="Conframe">聊天记录管理</a></li>
      <li><a class="J_menuItem" href="chatlevelauth.html" target="Conframe">聊天室权限管理</a></li>
      <li><a class="J_menuItem" href="chatban.html" target="Conframe">聊天室禁言管理</a></li>
      <li><a class="J_menuItem" href="chatredbag/chatredbag.html" target="Conframe">发红包记录</a></li>
      <li><a class="J_menuItem" href="chatredbag/chatredbaginfo.html" target="Conframe">抢红包记录</a></li>
      <li><a class="J_menuItem" href="shareorder.html" target="Conframe">分享投注记录</a></li>
      <li><a class="J_menuItem" href="chatusersendredbagauth/chatusersendredbagauth.html"
      target="Conframe">商户发红包人员管理</a></li>
      <li><a class="J_menuItem" href="chatroleuser/chatroleuser.html" target="Conframe">聊天室权限管理</a></li>
      </ul>
      </li> -->
      
      
      <li>
      <a href="#">
      <i class="fa fa-user"></i>
      <span class="nav-label">会员信息管理</span>
      <span class="fa arrow"></span>
      </a>
      <ul class="nav nav-second-level">
      <li><a class="J_menuItem" href="user/user_list.html" target="Conframe">会员管理</a>
      <a class="J_menuItem" href="" target="Conframe" style="display: none;" id="menuItemUtil"></a>
      </li>

      <li><a class="J_menuItem" href="user/user_lottery_order.html" target="Conframe">投注订单</a></li>
      <li><a class="J_menuItem" href="user/user_money_detail.html" target="Conframe">资金明细</a></li>
      <li><a class="J_menuItem" href="user_betting_demand.html" target="Conframe">用户打码管理</a></li>
      <%--<li><a class="J_menuItem" href="user/user_bank_list.html" target="Conframe">银行卡管理</a></li>--%>
      <%--<li><a class="J_menuItem" href="user_register/user_register.html" target="Conframe">推广管理</a></li>--%>
      <li><a id="loginlog" class="J_menuItem" href="loginlog.html" target="Conframe">登陆日志查询</a></li>

      <%--<li><a class="J_menuItem" href="useroperatelog.html" target="Conframe">用户操作记录</a></li>--%>
      <!-- <li><a class="J_menuItem" href="userquota.html" target="Conframe">配额管理</a></li> -->
      <li><a class="J_menuItem" href="user/user_consume_report.html" target="Conframe">盈亏报表</a></li>
      <%--<li><a class="J_menuItem" href="bonusorder.html" target="Conframe">契约分红管理</a></li>--%>
      <%--<li><a class="J_menuItem" href="daysalaryorder.html" target="Conframe">日工资管理</a></li>--%>
      <%--<li><a class="J_menuItem" href="userRebateForbid.html" target="Conframe">返点禁止管理</a></li>--%>
      <li><a class="J_menuItem" href="riskrecord.html" target="Conframe">风控记录管理</a></li>
      <!-- <li><a class="J_menuItem" href="redbag/redbag.html" target="Conframe">红包管理</a></li> -->
      </ul>
      </li>
      
      
     <%-- <li>
      <a href="#">
      <i class="fa fa-users"></i>
      <span class="nav-label">试玩账号管理</span>
      <span class="fa arrow"></span>
      </a>
      <ul class="nav nav-second-level">
      <li><a class="J_menuItem" href="user/user_list.html?isTourist=1" target="Conframe">试玩用户管理</a>
      <a class="J_menuItem" href="" target="Conframe" style="display: none;" id="menuItemUtil"></a>
      </li>

      <li><a class="J_menuItem" href="user/user_lottery_order.html?isTourist=1" target="Conframe">试玩投注订单</a></li>
      <li><a class="J_menuItem" href="user/user_money_detail.html?isTourist=1" target="Conframe">试玩资金明细</a></li>
      <li><a id="loginlog" class="J_menuItem" href="loginlog.html?isTourist=1" target="Conframe">试玩登陆日志查询</a></li>
      </ul>
      </li>--%>
    </c:if>


    <%-- ===================商户菜单==================================================================== --%>
    <c:if test="${admin.bizSystem != 'SUPER_SYSTEM'}">
      <li>
      <a href="#">
      <i class="fa fa-user"></i>
      <span class="nav-label">会员信息管理</span>
      <span class="fa arrow"></span>
      </a>
      <ul class="nav nav-second-level">

      <li><a class="J_menuItem" href="user/user_list.html" target="Conframe">会员管理</a>
      <a class="J_menuItem" href="" target="Conframe" style="display: none;" id="menuItemUtil"></a>
      </li>
      <!-- <li><a class="J_menuItem" href="user/user_add.html" target="Conframe" >直属代理添加</a></li> -->
      <li><a class="J_menuItem" href="user/user_lottery_order.html" target="Conframe">投注订单</a></li>
      <li><a class="J_menuItem" href="user/user_money_detail.html" target="Conframe">资金明细</a></li>
      <li><a class="J_menuItem" href="user_betting_demand.html" target="Conframe">用户打码管理</a></li>
      <%--<li><a class="J_menuItem" href="user/user_bank_list.html" target="Conframe">银行卡管理</a></li>
      <li><a class="J_menuItem" href="user_register/user_register.html" target="Conframe">推广管理</a></li>--%>
      <li><a id="loginlog" class="J_menuItem" href="loginlog.html" target="Conframe">登陆日志查询</a></li>

      <li><a class="J_menuItem" href="useroperatelog/systemuseroperatelog.html" target="Conframe">会员操作记录</a></li>
      <!-- <li><a class="J_menuItem" href="userquota.html" target="Conframe">配额管理</a></li> -->
      <li><a class="J_menuItem" href="user/user_consume_report.html" target="Conframe">盈亏查询</a></li>
    <%--  <li><a class="J_menuItem" href="bonusorder.html" target="Conframe">契约分红管理</a></li>
      <li><a class="J_menuItem" href="daysalaryorder.html" target="Conframe">日工资管理</a></li>
      <li><a class="J_menuItem" href="userRebateForbid.html" target="Conframe">返点禁止管理</a></li>--%>
      <li><a class="J_menuItem" href="riskrecord.html" target="Conframe">风控记录管理</a></li>
      <!-- <li><a class="J_menuItem" href="redbag/redbag.html" target="Conframe">红包管理</a></li> -->
      </ul>
      </li>
      
      
      <%--<li>
      <a href="#">
      <i class="fa fa-users"></i>
      <span class="nav-label">试玩账号管理</span>
      <span class="fa arrow"></span>
      </a>
      <ul class="nav nav-second-level">
      <li><a class="J_menuItem" href="user/user_list.html?isTourist=1" target="Conframe">试玩用户管理</a>
      <a class="J_menuItem" href="" target="Conframe" style="display: none;" id="menuItemUtil"></a>
      </li>

      <li><a class="J_menuItem" href="user/user_lottery_order.html?isTourist=1" target="Conframe">试玩投注订单</a></li>
      <li><a class="J_menuItem" href="user/user_money_detail.html?isTourist=1" target="Conframe">试玩资金明细</a></li>
      <li><a id="loginlog" class="J_menuItem" href="loginlog.html?isTourist=1" target="Conframe">试玩登陆日志查询</a></li>
      </ul>
      </li>--%>
      
      
      <%--<li>
      <a href="#">
      <i class="fa fa-magic"></i>
      <span class="nav-label">充值取现管理</span>
      <span class="fa arrow"></span>
      </a>
      <ul class="nav nav-second-level">
      <!-- <li><a class="J_menuItem" href="user/user_renew.html" target="Conframe">会员充值</a></li> -->
      <li><a class="J_menuItem" href="rechargeorder.html" target="Conframe">充值订单处理</a></li>
      <!-- <li><a class="J_menuItem" href="rechargeorderold/rechargeorder.html" target="Conframe">旧充值订单处理</a></li> -->
      <li><a class="J_menuItem" href="withdraworder.html" target="Conframe">提现订单处理</a></li>
      <!-- <li><a class="J_menuItem" href="withdraworderold/withdraworder.html" target="Conframe">旧提现订单处理</a></li> -->

      <c:if test="${admin.state < 3}">
        <li><a class="J_menuItem" href="quickchargeset.html" target="Conframe">第三方支付商户管理</a></li>
        <li><a class="J_menuItem" href="paybank.html" target="Conframe">收款银行设置</a></li>
        <li><a class="J_menuItem" href="rechargeconfig.html" target="Conframe">充值设置</a></li>
      </c:if>
      <li><a class="J_menuItem" href="withdrawautoconfig/withdrawautoconfig.html" target="Conframe">自动出款设置</a></li>
      <li><a class="J_menuItem" href="withdrawautolog/newwithdrawautolog.html" target="Conframe">自动出款日志</a></li>
      </ul>
      </li> --%>

      <c:if test="${admin.state < 3}">
        <li>
        <a href="#">
        <i class="fa fa-pie-chart"></i>
        <span class="nav-label">商户报表</span>
        <span class="fa arrow"></span>
        </a>
        <ul class="nav nav-second-level">
        <li><a class="J_menuItem" href="dayprofit.html" target="Conframe">盈亏报表</a></li>
        <!-- <li><a class="J_menuItem" href="user/user_account_report.html" target="Conframe">用户总额报表</a></li> -->
        <li><a class="J_menuItem" href="user/user_statistical_rankings.html" target="Conframe">统计排行报表</a></li>
        <li><a class="J_menuItem" href="daylotterygain/daylotterygain.html" target="Conframe">彩种报表</a></li>
       <%-- <li><a class="J_menuItem" href="lhc_order_report.html" target="Conframe">六合彩注单报表</a></li>--%>
        <li><a class="J_menuItem" href="user_self_gain_report.html" target="Conframe">用户盈利报表</a></li>
       <%-- <li><a class="J_menuItem" href="agent_report/agent_report.html" target="Conframe">代理报表</a></li>--%>
     <%--   <li><a class="J_menuItem" href="firstrechargeorder.html" target="Conframe">首充报表</a></li>--%>
      <%--  <li><a class="J_menuItem" href="rechargereport.html" target="Conframe">充值报表</a></li>--%>
        <!-- <li><a class="J_menuItem" href="rechargereportold/rechargereport.html" target="Conframe">旧充值报表</a></li> -->
    <%--    <li><a class="J_menuItem" href="withdrawreport.html" target="Conframe">取现报表</a></li>--%>
        <!-- <li><a class="J_menuItem" href="withdrawreportold/withdrawreport.html.html"
        target="Conframe">旧取现报表</a></li> -->
        <li><a class="J_menuItem" href="user/user_day_online_info.html" target="Conframe">在线统计报表</a></li>
        <%--<li><a class="J_menuItem" href="platformchargeorder.html" target="Conframe">平台收费明细</a></li>--%>
        </ul>
        </li>
      </c:if>
        <c:if test="${admin.state <= 1 || admin.state==3}">
      <li>
      <a href="#">
      <i class="fa fa-database"></i>
      <span class="nav-label">彩票管理</span>
      <span class="fa arrow"></span>
      </a>
      <ul class="nav nav-second-level">
      <!-- <li><a class="J_menuItem" href="hand_update.html" target="Conframe">彩票开奖处理</a></li> -->
      <li><a class="J_menuItem" href="hand_update/hand_update_selfkind.html" target="Conframe">私彩开奖处理</a></li>
      <li><a class="J_menuItem" href="lottery_code_xy_import/lottery_code_xy_import_list.html" target="Conframe">私彩开奖号码导入</a></li>
      <!-- <li><a class="J_menuItem" href="interface_check.html" target="Conframe">开奖接口查看</a></li> -->
      <li><a class="J_menuItem" href="lottery_code.html" target="Conframe">开奖号码查看</a></li>
      <li><a class="J_menuItem" href="lottery_code_xy.html" target="Conframe">私彩开奖号码查询</a></li>
      <c:if test="${enabled==1}">
        <li><a class="J_menuItem" href="lottery_control_config/lottery_control_config.html"
        target="Conframe">私彩开奖模式设定</a></li>
      </c:if>
      <%--<li><a class="J_menuItem" href="lottery_win.html" target="Conframe">彩票奖金查看</a></li>
      <li><a class="J_menuItem" href="lottery_issue.html" target="Conframe">开奖时间查看</a></li>
      <li><a class="J_menuItem" href="lhc_issue_config.html" target="Conframe">六合彩期号设定</a></li>--%>
      <!-- <li><a class="J_menuItem" href="lotterywinlhc.html" target="Conframe">六合彩赔率设定</a></li> -->
      <%--<li><a class="J_menuItem" href="lotterywinlhc/lotterywinlhcnew.html" target="Conframe">六合彩赔率设定</a></li>
      <li><a class="J_menuItem" href="lotterywinxyeb.html" target="Conframe">幸运28赔率设定</a></li>--%>
      </ul>
      </li>
        </c:if>
	  <!-- 子商户只有超级管理员才能看到这个菜单 -->
        <c:if test="${admin.state == 0}">
      <li>
      <a href="#">
      <i class="fa fa-user-secret "></i>
      <span class="nav-label">管理员信息管理</span>
      <span class="fa arrow"></span>
      </a>
      <ul class="nav nav-second-level">
      <c:if test="${admin.state == 0}">
      <li><a class="J_menuItem" href="admin.html" target="Conframe">管理员管理</a></li>
      </c:if>
      <li><a class="J_menuItem" href="adminoperatelog/systemadminoperatelog.html" target="Conframe">管理员操作记录</a></li>
      <c:if test="${admin.state == 0}">
      <li><a class="J_menuItem" href="adminlog.html" target="Conframe">管理员登陆日志查询</a></li>
      </c:if>
      </ul>
      </li>
        </c:if>
      

     <%-- <c:if test="${admin.state < 2}">
        <li>
        <a href="#">
        <i class="fa fa-home"></i>
        <span class="nav-label">网站设置</span>
        <span class="fa arrow"></span>
        </a>
        <ul class="nav nav-second-level">
        <li><a class="J_menuItem" href="bizsysteminfo/editbizsysteminfo.html" target="Conframe">网站内容设置</a></li>
        <li><a class="J_menuItem" href="bizsystemconfig/bizsystemconfig_set.html" target="Conframe">商户参数配置</a></li>
        <li><a class="J_menuItem" href="bizsystemdomain.html" target="Conframe">域名管理</a></li>
        &lt;%&ndash;<li><a class="J_menuItem" href="domaininvitationcode.html" target="Conframe">域名邀请码绑定</a></li>&ndash;%&gt;
        <!-- <li><a class="J_menuItem" href="bizsystemconfig.html" target="Conframe">商户参数配置</a></li> -->
        &lt;%&ndash;<li><a class="J_menuItem" href="level_sytem/level_sytem.html" target="Conframe">等级机制管理</a></li>&ndash;%&gt;
        <li><a class="J_menuItem" href="quotacontrol.html" target="Conframe">配额配置</a></li>
        <!-- <li><a class="J_menuItem" href="activity.html" target="Conframe">活动管理</a></li> -->
        <li><a class="J_menuItem" href="help.html" target="Conframe">帮助管理</a></li>
        <li><a class="J_menuItem" href="helps/helps.html" target="Conframe">帮助中心</a></li>
        <li><a class="J_menuItem" href="homepic.html" target="Conframe">首页图片管理</a></li>
        <!-- <li><a class="J_menuItem" href="lottery_set.html" target="Conframe">彩种开关设置</a></li> -->
        <li><a class="J_menuItem" href="lottery_switch.html" target="Conframe">彩种开关设置</a></li>
		<li><a class="J_menuItem" href="feedback.html" target="Conframe">反馈信息管理</a></li>
        <!-- <li><a class="J_menuItem" href="recordlog.html" target="Conframe">操作记录查询</a></li> -->


        <li><a class="J_menuItem" href="sitesearch.html" target="Conframe">站内信管理</a></li>
        <li><a class="J_menuItem" href="email_send_record/email_send_record.html" target="Conframe">邮件发送记录</a></li>
        <li><a class="J_menuItem" href="dayprofitrank.html" target="Conframe">中奖榜单查询</a></li>
        <c:if test="${admin.bizSystem == 'lc'}">
        </c:if>
        <!-- <li><a class="J_menuItem" href="chatlog.html" target="Conframe">聊天记录管理</a></li>
        <li><a class="J_menuItem" href="chatban.html" target="Conframe">用户ip禁言</a></li> -->
        </ul>
        </li>
      </c:if> --%>

    <%--  <li>
      <a href="#">
      <i class="fa fa-bullhorn"></i>
      <span class="nav-label">公告活动</span>
      <span class="fa arrow"></span>
      </a>
      <ul class="nav nav-second-level">
      <li><a class="J_menuItem" href="announce.html" target="Conframe">网站公告</a></li>
      <!-- <li><a class="J_menuItem" href="announce/editannounce.html" target="Conframe">发布公告</a></li> -->
      <li><a class="J_menuItem" href="activity.html" target="Conframe">活动管理</a></li>
      <li><a class="J_menuItem" href="day_consume_activity_config.html" target="Conframe">流水返送设置</a></li>
      <li><a class="J_menuItem" href="day_consume_activity_order.html" target="Conframe">流水返送订单</a></li>
      <li><a class="J_menuItem" href="signinconfig.html" target="Conframe">签到设置</a></li>
      <li><a class="J_menuItem" href="signinrecord.html" target="Conframe">签到记录</a></li>
      <li><a class="J_menuItem" href="promotionrecord.html" target="Conframe">晋级奖励金额</a></li>
      <li><a class="J_menuItem" href="new_user_gift_task.html" target="Conframe">新手活动任务查询</a></li>
      <li><a class="J_menuItem" id="alarminfo" href="alarminfo.html" target="Conframe">监控告警信息</a></li>
      </ul>
      </li>
      --%>
      
      <%-- <c:if test="${admin.bizSystem == 'lc'}">
        <li>
        <a href="#">
        <i class="fa fa-comments-o"></i>
        <span class="nav-label">聊天室管理</span>
        <span class="fa arrow"></span>
        </a>
        <ul class="nav nav-second-level">
        <li><a class="J_menuItem" href="chatroom.html" target="Conframe">聊天房间管理</a></li>
        <li><a class="J_menuItem" href="chatlog.html" target="Conframe">聊天记录管理</a></li>
        <li><a class="J_menuItem" href="chatlevelauth.html" target="Conframe">聊天室权限管理</a></li>
        <li><a class="J_menuItem" href="chatban.html" target="Conframe">聊天室禁言管理</a></li>
        <li><a class="J_menuItem" href="chatredbag/chatredbag.html" target="Conframe">发红包记录</a></li>
        <li><a class="J_menuItem" href="chatredbag/chatredbaginfo.html" target="Conframe">抢红包记录</a></li>
        <li><a class="J_menuItem" href="chatusersendredbagauth/chatusersendredbagauth.html"
        target="Conframe">商户发红包人员管理</a></li>
        </ul>
        </li>
      </c:if> --%>
      
      
    </c:if>
    </ul>
    </div>
    </nav>
    <!--左侧导航结束-->
    <!--右侧部分开始-->
    <div id="page-wrapper" class="dashbard-1">

    <div class="row content-tabs">
    <button class="roll-nav roll-left J_tabLeft">
    <i class="fa fa-backward"></i>
    </button>
    <nav class="page-tabs J_menuTabs">
    <div class="page-tabs-content">
    <a href="javascript:;" class="active J_menuTab" data-id="index.html">首页</a></div>
    </nav>
    <button class="roll-nav roll-right J_tabRight">
    <i class="fa fa-forward"></i>
    </button>
    <div class="btn-group roll-nav roll-right" style="right:120px">
    <button class="dropdown J_tabClose" data-toggle="dropdown">关闭操作
    <span class="caret"></span></button>
    <ul role="menu" class="dropdown-menu dropdown-menu-right">
    <li class="J_tabShowActive">
    <a>定位当前选项卡</a>
    </li>
    <li class="divider"></li>
    <li class="J_tabCloseAll">
    <a>关闭全部选项卡</a>
    </li>
    <li class="J_tabCloseOther">
    <a>关闭其他选项卡</a>
    </li>
    </ul>
    </div>
    <a class="dropdown-toggle count-info roll-nav roll-right J_tabExit J_menuItem" style="right:60px" id="userlisthref"
    onclick="indexPage.gotohref('email.html','邮件')" target="Conframe">
    <i class="fa fa-envelope"></i><span class="label label-warning" id="email" style="right:2px;top:2px;"></span>
    </a>
    <a href="javascript:void(0)" id="logoutBtn" class="roll-nav roll-right J_tabExit">
    <i class="fa fa fa-sign-out"></i>退出</a>
    </div>
    <div class="row J_mainContent" id="content-main">
    <c:if test="${admin.bizSystem == 'SUPER_SYSTEM'}">
      <iframe class="J_iframe" name="Conframe" width="100%" height="100%" src="bizsystem.html" frameborder="0"
      seamless></iframe>
    </c:if>
    <c:if test="${admin.bizSystem != 'SUPER_SYSTEM'}">
      <iframe class="J_iframe" name="Conframe" width="100%" height="100%" src="user/user_list.html" frameborder="0"
      seamless></iframe>
    </c:if>
    </div>
    <div class="footer">
    <!-- <div class="text-center">Copyright &copy; 2014-2018响牛网络科技</div> --></div>
    </div>
    <!--右侧部分结束-->
    <div id="small-chat">
    <a class="open-small-chat navbar-minimalize">
    <i class="fa fa-bars" style="font-size: 21px;"></i>
    </a>
    </div>

    <!-- 弹出警告框-->
    <div class="tip-container"></div>

    </div>
    <audio controls=false id='voiceAutoMsgRecharge' style='display:none'> <!-- autoplay=true loop=loop-->
    <source src="<%=path%>/resources/music/cz.mp3" >
    <source src="<%=path%>/resources/music/cz.ogg" >

    </audio>
    <audio controls=false id='voiceAutoMsgWithdraw' style='display:none'> <!-- autoplay=true loop=loop-->
    <source src="<%=path%>/resources/music/qx.mp3" >
    <source src="<%=path%>/resources/music/qx.ogg" >

    </audio>
    <script src="<%=path%>/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<%=path%>/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<%=path%>/js/hplus.min.js?v=4.1.0"></script>
    <script type="text/javascript" src="<%=path%>/js/contabs.min.js"></script>
    <script src="<%=path%>/js/plugins/pace/pace.min.js"></script>

    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerRechargeOrderAction.js'></script>
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerAdminAction.js'></script>
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerSystemAction.js'></script>
    <script type='text/javascript' src="<%=path%>/dwr/interface/managerAdminRecvNoteAction.js"></script>
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerAlarmInfoAction.js'></script>
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerWithDrawOrderAction.js'></script>
    <!-- js -->
   <script type="text/javascript" src="<%=path%>/admin/index/js/index.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
   <script> 
        $(".update_pwd").click(function() {
                    layer.open({
                        type: 2,
                        title: '修改密码',
                        maxmin: false,
                        shadeClose: true,
                        //点击遮罩关闭层
                        area: ['400px', '300px'],
                        content: 'updatepwd/update_pwd.html'
                    })
                });

        function show(){ 
        var date = new Date();
        var now = ""; 
        now = date.getFullYear()+"年";
        now = now + (date.getMonth()+1)+"月";
        now = now + date.getDate()+"日&nbsp;&nbsp;"; 
        now = now + date.getHours()+":"; 
        now = now + date.getMinutes()+":"; 
        now = now + date.getSeconds(); 
        document.getElementById("time").innerHTML = now;
        setTimeout("show()",1000); 
        } 
        
        /*弹出警告框*/
        function alarm(title, content, delay=-1){
            // 唯一id，方便多个共存，自动删除的时候确认删除目标
            var id = 'ID_' + (new Date() - 0);
            var str = '<div class="tip" id="' + id + '">'
                    + '    <div class="title"><span>{title}</span><span class="close"></span></div>'
                    + '    <div class="content">{content}</div>'
                    + '</div>';
            var tipHtml = str.replace('{title}', title)
                             .replace('{content}', content);
            $('.tip-container').append(tipHtml);
            delay = parseInt(delay);
            if (delay != -1 && delay > 0) {
                setTimeout(function() {
                    $('#' + id).remove();
                }, delay);
            }
        }
        $(document).on('click', '.tip .close', function() {
            var tip = $(this).parents('.tip');
            tip.animate({
            	'top': 0,
            	'opacity': 0
            },500)
            setTimeout(function() {
            	tip.remove();
            }, 500);
        });
        /**
         * [showTip 弹窗]
         * @param  {[string]} title [标题]
         * @param  {[string]} str   [内容]
         * @param  {[number|boolean]} delay [number为延时时间,boolean为是否开启延时关闭]
         * @param  {[boolean]} close [是否显示关闭按钮]
         * @return {[void]}       [description]
         */
        function showTip(title,str,id,close,delay,root){
        	 /* if(root==1){
	        	$(".tip-close").click();
        	 } */
        	var tipclass="tip"+new Date().getMilliseconds();
        	var _delay=delay||3000;
        	var _close=typeof(close)=="boolean"?close:true;
        	var html='<div class="tip '+tipclass+'" id="tip" style="cursor:pointer;width:315px">';
                html+='<div class="tip-content" onclick=indexPage.toEmail('+root+')>';
                if(str.length>40){
                	if(root==2){
                		html+='<h2 class="title">'+title+'</h2><p style="word-wrap: break-word;">'+str.substring(0,46)+'....</p></div></div>';
                	}else{
	                	html+='<h2 class="title">'+title+'</h2><p style="word-wrap: break-word;">'+str.substring(0,40)+'....</p></div></div>';
                	}
                }else{
	        		html+='<h2 class="title">'+title+'</h2><p style="word-wrap: break-word;">'+str+'</p></div></div>';
                }
        	$("body").append(html);
        	if(_close) 
        		$(".tip."+tipclass).append('<img class="tip-close'+root+'" onclick="hideTip(\''+tipclass+'\')" src="'+contextPath+'/img/icon-close.png" alt="">')
        	if(delay==false) return;
        	setTimeout(function(tipclass){
        		hideTip(tipclass);
        	},_delay,tipclass);
        	
        }
        function hideTip(tipclass){
        	$(".tip."+tipclass).addClass('out');
        	setTimeout(function(tipclass){
        		$(".tip."+tipclass).remove();
        	},1000,tipclass);
        }
    </script>
    </body>

    </html>