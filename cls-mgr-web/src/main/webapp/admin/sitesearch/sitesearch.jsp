<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.util.ConstantUtil,com.team.lottery.vo.Admin" %>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
	Admin admin = (Admin)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_ADMIN);
	String isTourist = request.getParameter("isTourist");
%>
<!DOCTYPE html>
<html>
<head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>会员信息管理</title>
     <jsp:include page="/admin/include/include.jsp"></jsp:include>
          <link rel="shortcut icon" href="favicon.ico">
     <style type="text/css">
			/* th, td { white-space: nowrap; }
			div.dataTables_wrapper { width: 1000px; margin: 0 auto; } */
       		.jsstar { list-style: none; margin: 0px; padding: 0px; width: 100px; height: 20px; position: relative; display: inline-block; }
			.jsstar li { padding: 0px; margin: 0px; float: left; width: 20px; height: 20px; background: url(../../admin/resource/img/star_rating.gif) 0 0 no-repeat; }
			.jsstar_bright { background-position: left bottom; }
			.mt5{margin-top:5px;margin-bottom: 10px;}
     </style>
     <script type="text/javascript">
			var isTouristParameter = '<%=isTourist %>';
     </script>       
</head>
    <body>
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
                    <div class="ibox ">
                        <div class="ibox-content bd0 pt1">
                            <div class="row">
                                <form class="form-horizontal" id="queryForm">
                                    <div class="row">
                                      <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                                        <div class="col-sm-4">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">商户</span>
                                                <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">发送人</span>
                                                <input id='fromUserName' name="fromUserName" type="text" value="" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">接收人</span>
                                                <input id='toUserName' name="toUserName" type="text" value="" class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">消息类型</span>
                                                <select id="type" name="type" class="ipt form-control">
                                                	<option value="">= = 请选择 = =</option>
                                                	<option value="SYSTEM">商户类型</option>
                                                	<option value="UP_DOWN">上级消息</option>
                                                	<option value="DOWN_UP">下级消息</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">状态</span>
                                                <select id="status" name="status" class="ipt form-control">
                                                	<option value="">= = 请选择 = =</option>
                                                	<option value="NO_READ">未读</option>
                                                	<option value="YET_READ">已读</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group mt5 text-right">
                                                <button type="button" class="btn btn-outline btn-default btn-kj" onclick="notesList.getAllNotesByCondition()"><i class="fa fa-search"></i>&nbsp;查 询</button></div>
                                        </div>
                                        </c:if>
                                 		<c:if test="${admin.bizSystem !='SUPER_SYSTEM'}">
	                                        <div class="col-sm-4">
	                                            <div class="input-group m-b">
	                                                <span class="input-group-addon">发送人</span>
	                                                <input id='fromUserName' name="fromUserName" type="text" value="" class="form-control" >
	                                            </div>
	                                        </div>
	                                        <div class="col-sm-4">
	                                            <div class="input-group m-b">
	                                                <span class="input-group-addon">接收人</span>
	                                                <input id='toUserName' name="toUserName" type="text" value="" class="form-control" >
	                                            </div>
	                                        </div>
	                                        <div class="col-sm-4">
	                                            <div class="input-group m-b">
	                                                <span class="input-group-addon">消息类型</span>
	                                                <select id="type" name="type" class="ipt form-control">
	                                                	<option value="">= = 请选择 = =</option>
	                                                	<option value="SYSTEM">商户类型</option>
	                                                	<option value="UP_DOWN">上级消息</option>
	                                                	<option value="DOWN_UP">下级消息</option>
	                                                </select>
	                                            </div>
	                                        </div>
	                                        <div class="col-sm-4">
	                                            <div class="input-group m-b">
	                                                <span class="input-group-addon">状态</span>
	                                                <select id="status" name="status" class="ipt form-control">
	                                                	<option value="">= = 请选择 = =</option>
	                                                	<option value="NO_READ">未读</option>
	                                                	<option value="YET_READ">已读</option>
	                                                </select>
	                                            </div>
	                                        </div>
	                                        <div class="col-sm-1">
	                                            <div class="form-group nomargin text-right">
	                                                <button type="button" class="btn btn-outline btn-default btn-kj" onclick="notesList.getAllNotesByCondition()"><i class="fa fa-search"></i>&nbsp;查 询</button></div>
	                                        </div>
                                        </c:if>
                                    </div>
                                </form>
                            </div>
                            <div class="jqGrid_wrapper">
                                <div class="ui-jqgrid ">
                                    <div class="ui-jqgrid-view">
                                        <div class="ui-jqgrid-hdiv">
                                            <div class="ui-jqgrid-hbox" style="padding-right:0">
                                                <table class="ui-jqgrid-htable ui-common-table table table-bordered" id="noteslistTable">
                                                <thead>
                                                        <tr class="ui-jqgrid-labels">                                                         
                                                            <th class="ui-th-column ui-th-ltr">商户</th>
                                                            <th class="ui-th-column ui-th-ltr">发送人</th> 
                                                            <th class="ui-th-column ui-th-ltr">接收人</th>
                                                            <th class="ui-th-column ui-th-ltr">入账方式</th>
                                                            <th class="ui-th-column ui-th-ltr">内容</th>
                                                            <th class="ui-th-column ui-th-ltr">状态</th>
                                                            <th class="ui-th-column ui-th-ltr">是否启用</th>
                                                            <th class="ui-th-column ui-th-ltr">消息类型</th>
                                                            <th class="ui-th-column ui-th-ltr">发送时间</th>
                                                            <th class="ui-th-column ui-th-ltr">操作</th> 
                                                          </tr>
                                                    </thead>                            
                                                    <tbody>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  
  
<!-- js -->
<script type="text/javascript" src="<%=path%>/admin/sitesearch/js/siteSearch.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerNotesAction.js'></script>
</body>
</html>

