function NotesListPage(){}
var notesList= new NotesListPage();

notesList.param = {
		
};

/**
 * 查询参数
 */
notesList.queryParam = {
		bizSystem : null,
		fromUserName : null,
		toUserName : null,
		type : null,
		status : null
};

//分页参数
notesList.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize
};

$(document).ready(function() {
	notesList.initTableData(); //默认查询所有的用户 
	//回车提交事件
    $("body").keydown(function() {
        if (event.keyCode == "13") {//keyCode=13是回车键
        	notesList.getAllNotesByCondition();
        }
    });
});

/**
 * 模糊查找站内信
 */
NotesListPage.prototype.getAllNotesByCondition = function(){
	notesList.queryParam = {};
    var bizSystem = $("#bizSystem").val();
    var fromUserName = $("#fromUserName").val();
    var toUserName = $("#toUserName").val();
    var type = $("#type").val();
    var status = $("#status").val();
    if(bizSystem == ""){
    	bizSystem = null;
    }
    if(fromUserName == ""){
    	fromUserName =  null;
    }
    if(toUserName == ""){
    	toUserName = null;
    }
    if(type == ""){
    	type = null;
    }
    if(status == ""){
    	status = null;
    }
	notesList.queryParam.bizSystem = bizSystem;
	notesList.queryParam.fromUserName = fromUserName;
	notesList.queryParam.toUserName = toUserName;
	notesList.queryParam.type = type;
	notesList.queryParam.status = status;
	notesList.table.ajax.reload();
};

/**
 * 查询符合条件的
 */
NotesListPage.prototype.queryConditionNotes = function(queryParam,pageNo){
	managerNotesAction.notesList(queryParam,pageNo,function(r){
		if (r[0] != null && r[0] == "ok") {
			notesList.refreshNotesPages(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("查询站内信数据请求失败.");
		}
    });
};

/**
 * 初始化列表数据
 */
NotesListPage.prototype.initTableData = function() {
	notesList.queryParam = getFormObj($("#queryForm"));
	notesList.table = $('#noteslistTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"scrollX": true,
		"ajax":function (data, callback, settings) {
			notesList.pageParam.pageSize = data.length;//页面显示记录条数
			notesList.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerNotesAction.notesList(notesList.queryParam,notesList.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": "bizSystemName","bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "fromUserName"},
		            {"data": "toUserName"},
		            {"data": "sub",
		            	render: function(data, type, row, meta) {
		            		if (row.sub.length > 6) {
								return data.substr(0,4) + '&nbsp;&nbsp;<a href="javascript:void(0);" title='+data+' ><b>...</b></a>';
							}else{
								return data;
							}
		            	}
		            },
		            {"data": "body",
		            	render: function(data, type, row, meta) {
		            		if (row.body.length > 10) {
								return getPartialRemarksHtml(data);
							}else{
								return data;
							}
		            	}
		            },
		            {"data": "status",
		            	render: function(data, type, row, meta) {
		            		if(row.status == "NO_READ"){
		            			return '<font color="red">未读</font>';
		            		}else{
		            			return '已读';
		            		}
		            		return data;
	            		}
		            },
		            {"data": "enabled",
		            	render: function(data, type, row, meta) {
		            		if(row.enabled == 0){
		            			return '<font color="red">禁用</font>';
		            		}else{
		            			return '启用';
		            		}
		            		return data;
	            		}
		            },
		            {"data": "type",
		            	render: function(data, type, row, meta) {
		            		if(row.type == 'SYSTEM'){
		            			return "系统消息";
		            		}
		            		if(row.type == 'DOWN_UP'){
		            			return "上下级联系";
		            		}
		            		return data;
	            		}
		            },
		            {"data": "createdDate",
		            	render: function(data, type, row, meta) {
		            		return dateFormat(data,'yyyy-MM-dd hh:mm:ss');
	            		}
		            },
		            {
	                	 "data": "id",
	                	 render: function(data, type, row, meta) {
	                		 return "<a class='btn btn-danger btn-sm btn-del' onclick='notesList.deleteNotes("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";
	                	 }
	                }
	            ]
	}).api(); 
	
}

/**
 * 删除站内信息
 */
NotesListPage.prototype.deleteNotes = function(id){
	 swal({
         title: "您确定要删除这条信息吗",
         text: "请谨慎操作！",
         type: "warning",
         showCancelButton: true,
         confirmButtonColor: "#DD6B55",
         cancelButtonText: "取消",
         confirmButtonText: "删除",
         closeOnConfirm: false
     },
     function() {
    	 managerNotesAction.deleteNotes(id,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "删除成功",type: "success"});
				notesList.getAllNotesByCondition();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
   			}else{
   				swal("删除信息失败.");
   			}
	    });
	});
};





/**
 * 内容过多显示部分，鼠标停留可显示全部
 */
function getPartialRemarksHtml(data){
      return data.substr(0,10) + '&nbsp;&nbsp;<a href="javascript:void(0);" title='+data+' ><b>...</b></a>';
}
