function AdminOperateLogPage(){}
var adminOperateLogPage = new AdminOperateLogPage();

/**
 * 查询参数
 */
adminOperateLogPage.param = {
	bizSystem : null,
	adminName : null,
	operationIp : null,
	userName : null,
	modelName : null,
	operateDesc : null,
	logtimeStart : null,
	logtimeEnd : null  
};

//分页参数
adminOperateLogPage.pageParam = {
	pageNo : 1,
	pageSize : defaultPageSize
};


$(document).ready(function() {
	//查询所有的操作日志数据
	adminOperateLogPage.getAllRecordLogs();
	
})

AdminOperateLogPage.prototype.findRecordLogs = function(){
	adminOperateLogPage.param = getFormObj($("#recordLogQuery"));
	adminOperateLogPage.table.ajax.reload();
}

/**
 * 加载所有的登陆日志数据
 */
AdminOperateLogPage.prototype.getAllRecordLogs = function(){
	adminOperateLogPage.param = getFormObj($("#activityQuery"));
	adminOperateLogPage.table =  $('#activityTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"ajax":function (data, callback, settings) {
			adminOperateLogPage.pageParam.pageSize = data.length;//页面显示记录条数
			adminOperateLogPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
			managerAdminOperateLogAction.getAllAdminOperateLogs(adminOperateLogPage.param,adminOperateLogPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				}; 
				
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询奖金配置数据失败.");
				}
				callback(returnData);
		    });
		},
	"columns": [
				{"data":"modelName"},
	            {"data":"adminName"},
	            {"data":"userName"},
	            {"data":"operateDesc",
	            	 render: function(data, type, row, meta) {
	            		 if(data.length > 45){
	            			 var title =  "<span title='"+data+"'>"+data.substring(0,45)+"．．．</span>";
	            			 return title;
	            		 }else{
	            			 return data;
	            		 }
	            	 },
	            },
	            {"data":"operationIp"},
	            {"data":"createDateStr"},
	            
	          /*  {"data":"id",
	            	 render: function(data, type, row, meta) {
	            		 return  "<a class='btn btn-danger btn-sm btn-del' onclick='recordLogPage.delRecordLogs("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";
	            	 },"bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false
	            },*/
	            
	     ]
	}).api();
}

/**
 * 按日期查询数据
 */
AdminOperateLogPage.prototype.findRecordlogByQuery = function(dateRange){
	if(dateRange != undefined && dateRange != null && dateRange != ''){
		commonPage.setDateRangeTimeZero(dateRange, "startime", "endtime",2);	
	}
	adminOperateLogPage.param = getFormObj($("#recordLogQuery"));
	adminOperateLogPage.table.ajax.reload();
};

/*RecordLogPage.prototype.delRecordLogs = function(id){
	   swal({
           title: "您确定要删除这条日志吗",
           text: "请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "删除",
           closeOnConfirm: false
       },
       function(){
    	   managerRecordLogAction.delRecordLogs(id, function(r){
      			if (r[0] != null && r[0] == "ok") {
      				swal({ title: "提示", text: "删除成功",type: "success"});
      				recordLogPage.findRecordLogs();
      			}else if(r[0] != null && r[0] == "error"){
      				swal(r[1]);
      			}else{
      				swal("删除日志数据失败.");
      			}
      	    
    	   });
       })
}
*/