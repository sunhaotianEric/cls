function LotteryMsgPage() {
}
var lotteryMsgPage = new LotteryMsgPage();

/**
 * 投注信息参数
 */
lotteryMsgPage.param = {
	lotteryId : null,
	lotteryCodes : new Array()
};

lotteryMsgPage.kindKeyValue={
		summationDescriptionMap:new JS_OBJECT_MAP()
};

$(document).ready(function() {
	

	
	
	if(lotteryMsgPage.param.lotteryId != null){
		lotteryMsgPage.getLotteryMsg();
	}else{
		swal("投注参数传递错误.");
	}
	

	
});


/**
 * 添加用户
 */
LotteryMsgPage.prototype.getLotteryMsg = function() {
	managerOrderAction.getOrderById(lotteryMsgPage.param.lotteryId,function(r) {
		if (r[0] != null && r[0] == "ok") {
			lotteryMsgPage.showLotteryMsg(r[1]);
		} else if (r[0] != null && r[0] == "error") {
			swal(r[1]);
		} else {
			swal("查找投注记录请求失败.");
		}
	});
};

/**
 * 展示投注信息
 */
LotteryMsgPage.prototype.showLotteryMsg = function(order) {
	
	if(order != null){
		var expect = order.expect;
		$("#lotteryTypeDes").text(order.lotteryTypeDes);
		
		//福彩3D期号特殊处理
		if(order.lotteryType == 'FCSDDPC' || order.lotteryType == 'BJKS' || order.lotteryType == 'BJPK10' || 
				order.lotteryType == 'XJPLFC' || order.lotteryType == 'TWWFC'|| order.lotteryType == 'LHC'|| order.lotteryType == 'XYEB'){
			$("#lotteryExpect").text(expect);
		}else{
			$("#lotteryExpect").text(expect.substring(0,8) + "-" + expect.substring(8,expect.length));
		}
		$("#userName").text(order.userName);
		$("#lotteryId").text(order.lotteryId);
		$("#lotteryTypeDes").text(order.lotteryTypeDes);
		
		$("#afterNumber").text(order.afterNumber==1?"是":"否");
		$("#lotteryCreateDate").text(order.createdDateStr);
		$("#lotteryPayMoney").text(parseFloat(order.payMoney).toFixed(commonPage.param.fixVaue));
		$("#lotteryWinCost").text(parseFloat(order.winCost).toFixed(commonPage.param.fixVaue));
		
		$("#lotteryawardModel").text(order.awardModel);
		if(order.openCode != null){
			var openCodeShowArray = order.openCode.split(",");
			var str = "";
			var sumCode = 0;
            for(var i = 0; i < openCodeShowArray.length; i++){
            	if(i==openCodeShowArray.length-1){
            		if(order.lotteryType == 'LHC'){
            			 str += "<i class='mun' style='color: red;'>"+openCodeShowArray[i]+"</i>";	
            		}else if(order.lotteryType == 'XYEB'){
            		 sumCode = sumCode + Number(openCodeShowArray[i]);
              		  str += "<i class='mun'>"+openCodeShowArray[i]+"="+sumCode+"</i>";
              		}else{
            			 str += "<i class='mun'>"+openCodeShowArray[i]+"</i>";
            		}
            	}else{
            		if(order.lotteryType == 'XYEB'){
            			sumCode = sumCode + Number(openCodeShowArray[i]);
            		  str += "<i class='mun'>"+openCodeShowArray[i]+"+</i>";
            		}else{
            		 str += "<i class='mun'>"+openCodeShowArray[i]+",</i>";	
            		}
            	 	
            	}
                
            }
            $("#opencodes").html(str);
		}else{
			$("#opencodes").html("<span style='color:red'>未开奖</span>");
		}	

		$("#codeList").html("");
		var codeListObj = $("#codeList");
		var orderCodesList = order.codesList;
		var str = "";
		var wincostStr = order.winCostStr;
		var wincostArrays = null;
		if(wincostStr != null){
			wincostArrays = wincostStr.split("&");
		}
		
		var codeCount = 0;
		var cathecticCountstr=0;
		for(var i = 0; i < orderCodesList.length; i++){
			var orderCodesMap = orderCodesList[i];
			for(var key in orderCodesMap){
				var codeDes = orderCodesMap[key];
				var codeStartPositon = codeDes.indexOf("[");
				var codeEndPositon = codeDes.indexOf("]");
				var beishuStartPosition = codeDes.lastIndexOf("[");
				var beishuEndPosition = codeDes.lastIndexOf("]");
				var payMoneyStartPosition = codeDes.lastIndexOf("{");
				var payMoneyEndPosition = codeDes.lastIndexOf("}");
				var cathecticCountStartPosition = codeDes.lastIndexOf("(");
				var cathecticCountEndPosition = codeDes.lastIndexOf(")");
				
				var code = codeDes.substring(0,codeStartPositon);
				var beishu =  codeDes.substring(beishuStartPosition + 1,beishuEndPosition);
				var payMoney =  codeDes.substring(payMoneyStartPosition + 1,payMoneyEndPosition);
				var cathecticCount =  codeDes.substring(cathecticCountStartPosition + 1,cathecticCountEndPosition);
				cathecticCountstr+=Number(cathecticCount);
				//追号采用期号的倍数
				if(order.afterNumber == 1){
					beishu = order.multiple;
					payMoney = payMoney * beishu;
				}

				str += "<tr role='row' tabindex='-1' class='jqgrow ui-row-ltr'>";
				str += " <td>"+key+"</td>";
				var codeArray = code.split("_");
				code = codeArray[0];
				if(codeArray.length == 2){
					var positionDes = lotteryMsgPage.getLotteryPosition(order.lotteryType,codeArray[1].split(","));
					code = code + positionDes;
				}
				if(order.openCode != null){
					if(code.length > 50){
						str += "<td>"+code.substring(0,18).replace(/\&/g," ")+"<a href='javascript:void(0)'  onclick='lotteryMsgPage.showDsCode("+codeCount+")'>...详情</a></td>";
						lotteryMsgPage.param.lotteryCodes.push(code);
						codeCount++;
					}else if(key=="五球/龙虎/梭哈"||key=="斗牛"||key=="后三总和"||key=="中三总和"||key=="前三总和"){
						var codeArray = code.split(",");
						var codeStr = "";
						for(var j = 0; j < codeArray.length; j++){
							codeStr +=  lotteryMsgPage.kindKeyValue.summationDescriptionMap.get(key).get(codeArray[j]) +",";
						}
						codeStr = codeStr.substring(0, codeStr.length -1);
						str += "<td>" + codeStr +"</td>";
						//str += "  <td>"+lotteryMsgPage.kindKeyValue.summationDescriptionMap.get(key).get(code)+"</td>";
					}else{
						var openCodeArray = order.openCode.split(",");
						var codeArray = code.split(",");
						var codeStr = "";
						for(var j = 0; j < codeArray.length; j++){
							if(lotteryMsgPage.judgeIsContainOpenCode(order.lotteryType,openCodeArray,codeArray[j],key,j)){
								codeStr += "<span>" + codeArray[j] +"</span>,";
							}else{
								if(codeArray[j].indexOf("|") != -1){
									var codeArrs = codeArray[j].split("|");
									for(var z = 0; z < codeArrs.length; z++){
										  var codeArr = codeArrs[z];
										  var isCodeContain = false;
										  for(var n = 0; n < openCodeArray.length; n++){
												if(openCodeArray[n] == codeArr){
													codeStr += "<span'>" + openCodeArray[n] +"</span>|";
													isCodeContain = true;
													break;
												}
										  }
										  if(!isCodeContain){
												codeStr += codeArr + "|";
										  }
									}
									codeStr = codeStr.substring(0, codeStr.length -1) + ",";
								}else{
									codeStr += codeArray[j] + ",";
								}
							}
						}
						codeStr = codeStr.substring(0, codeStr.length -1);
						str += "<td>" + codeStr +"</td>";
					}
				}else{
					if(code.length > 50){
						str += " <td>"+code.substring(0,18).replace(/\&/g," ")+"<a href='javascript:void(0)'  onclick='lotteryMsgPage.showDsCode("+codeCount+")'>...详情</a></td>";
						lotteryMsgPage.param.lotteryCodes.push(code);
						codeCount++;
					}else if(key=="五球/龙虎/梭哈"||key=="斗牛"||key=="后三总和"||key=="中三总和"||key=="前三总和"){
						var codeArray = code.split(",");
						var codeStr = "";
						for(var j = 0; j < codeArray.length; j++){
							codeStr +=  lotteryMsgPage.kindKeyValue.summationDescriptionMap.get(key).get(codeArray[j]) +",";
						}
						codeStr = codeStr.substring(0, codeStr.length -1);
						str += "<td>" + codeStr +"</td>";
						//str += "  <td>"+lotteryMsgPage.kindKeyValue.summationDescriptionMap.get(key).get(code)+"</td>";
					}else{
						str += "<td>"+code.replace(/\&/g," ")+"</td>";
					}
				}
				
				str += "<td>"+cathecticCount+"</td>";
				str += " <td>"+beishu+"</td>";
				str += " <td><span class='yellow'>¥"+parseFloat(payMoney).toFixed(commonPage.param.fixVaue)+"元</span></td>";
				str += " <td>"+order.lotteryModelStr+"</td>";
				if(wincostArrays != null){
					if(parseFloat(wincostArrays[i]) > 0){
						str += " <td>"+wincostArrays[i]+"</td>";
					}else{
						str += " <td>未中奖</td>";
					}
				}else{
					str += " <td>"+order.prostateStatusStr+"</td>";

				}
				
				str += "</tr>";
				codeListObj.append(str);
				str = "";
			}
		}
		$("#lotteryCount").text(cathecticCountstr+"注");
	}else{
		swal("投注订单为空.");
	}
};

/**
 * 展示投注号码
 */
LotteryMsgPage.prototype.showDsCode = function(codeCount){
	var code = lotteryMsgPage.param.lotteryCodes[codeCount];
	$("#kindPlayCodeDes").html(code.replace(/\&/g,"\n"));
	
	$(".alert-msg").each(function(i,n){
		var h=parseInt($(n).height())/2*-1;
		$(n).css("margin-top",h+"px");
	});
	$(".alert-msg").stop(false,true).delay(200).fadeIn(500);
	$(".alert-msg-bg").stop(false,true).fadeIn(500);
};


/**
 * 单式投注位数
 */
LotteryMsgPage.prototype.getLotteryPosition = function(lotteryType,positionArray){
	var positionDes = "(";
	if(lotteryType == 'FCSDDPC'){
		if(positionArray[0] == "1"){
			positionDes += "百位  ";
		}
		if(positionArray[1] == "1"){
			positionDes += "十位  ";
		}
		if(positionArray[2] == "1"){
			positionDes += "个位  ";
		}
	}else{
		if(positionArray[0] == "1"){
			positionDes += "万位  ";
		}
		if(positionArray[1] == "1"){
			positionDes += "千位  ";
		}
		if(positionArray[2] == "1"){
			positionDes += "百位  ";
		}
		if(positionArray[3] == "1"){
			positionDes += "十位  ";
		}
		if(positionArray[4] == "1"){
			positionDes += "个位  ";
		}
	}
	positionDes += ")";
	
	return positionDes;
};

/**
 * 判断是否包含开奖号码
 */
LotteryMsgPage.prototype.judgeIsContainOpenCode = function(lotteryType,openCodeArrays,code,key,index) {
	if(key == "前二大小单双复式"){
		var oneWeiBig = false;
		if(openCodeArrays[0] >= 5){
			oneWeiBig = true;
		}else{
			oneWeiBig = false;
		}
		var oneWeiDan = false;
		if(openCodeArrays[0] % 2 != 0){
			oneWeiDan = true;
		}else{
			oneWeiDan = false;
		}
		
		var twoWeiBig = false;
		if(openCodeArrays[1] >= 5){
			twoWeiBig = true;
		}else{
			twoWeiBig = false;
		}
		var twoWeiDan = false;
		if(openCodeArrays[1] % 2 != 0){
			twoWeiDan = true;
		}else{
			twoWeiDan = false;
		}
		
		if(index == 0){
			if(code == "大"){
                if(oneWeiBig){
                	return true;
                }else{
                	return false;
                }
			}
			if(code == "小"){
                if(oneWeiBig){
                	return false;
                }else{
                	return true;
                }
			}
			if(code == "单"){
                if(oneWeiDan){
                	return true;
                }else{
                	return false;
                }
			}
			if(code == "双"){
                if(oneWeiDan){
                	return false;
                }else{
                	return true;
                }
			}
		}else if(index == 1){
			if(code == "大"){
                if(twoWeiBig){
                	return true;
                }else{
                	return false;
                }
			}
			if(code == "小"){
                if(twoWeiBig){
                	return false;
                }else{
                	return true;
                }
			}
			if(code == "单"){
                if(twoWeiDan){
                	return true;
                }else{
                	return false;
                }
			}
			if(code == "双"){
                if(twoWeiDan){
                	return false;
                }else{
                	return true;
                }
			}
		}else{
			alert("位数不正确");
		}
	}else if(key == "后二大小单双复式"){
		var siWeiBig = false;
		var weiValueOne = 0; 
		var weiValueTwo = 0;
		
		if(lotteryType == 'FCSDDPC'){
			weiValueOne = openCodeArrays[1];
			weiValueTwo = openCodeArrays[2];
		}else{
			weiValueOne = openCodeArrays[3];
			weiValueTwo = openCodeArrays[4];
		}
		
		if(weiValueOne >= 5){
			siWeiBig = true;
		}else{
			siWeiBig = false;
		}
		var siWeiDan = false;
		if(weiValueOne % 2 != 0){
			siWeiDan = true;
		}else{
			siWeiDan = false;
		}
		
		var wuWeiBig = false;
		if(weiValueTwo >= 5){
			wuWeiBig = true;
		}else{
			wuWeiBig = false;
		}
		var wuWeiDan = false;
		if(weiValueTwo % 2 != 0){
			wuWeiDan = true;
		}else{
			wuWeiDan = false;
		}
		
		if(index == 0){
			if(code == "大"){
                if(siWeiBig){
                	return true;
                }else{
                	return false;
                }
			}
			if(code == "小"){
                if(siWeiBig){
                	return false;
                }else{
                	return true;
                }
			}
			if(code == "单"){
                if(siWeiDan){
                	return true;
                }else{
                	return false;
                }
			}
			if(code == "双"){
                if(siWeiDan){
                	return false;
                }else{
                	return true;
                }
			}
		}else if(index == 1){
			if(code == "大"){
                if(wuWeiBig){
                	return true;
                }else{
                	return false;
                }
			}
			if(code == "小"){
                if(wuWeiBig){
                	return false;
                }else{
                	return true;
                }
			}
			if(code == "单"){
                if(wuWeiDan){
                	return true;
                }else{
                	return false;
                }
			}
			if(code == "双"){
                if(wuWeiDan){
                	return false;
                }else{
                	return true;
                }
			}
		}else{
			alert("位数不正确");
		}
	}else if(key == "前三直选和值" || key == "前三组选和值"){
		var qsHz = parseInt(openCodeArrays[0]) + parseInt(openCodeArrays[1]) + parseInt(openCodeArrays[2]);
		if(qsHz == parseInt(code)){
			return true;
		}else{
			return false;
		}
	}else if(key == "后三直选和值" || key == "后三组选和值"){
		var hsHz = parseInt(openCodeArrays[2]) + parseInt(openCodeArrays[3]) + parseInt(openCodeArrays[4]);
		if(hsHz == parseInt(code)){
			return true;
		}else{
			return false;
		}
	}else if(key == "前二直选和值" || key == "前二组选和值"){
		var qeHz = parseInt(openCodeArrays[0]) + parseInt(openCodeArrays[1]);
		if(qeHz == parseInt(code)){
			return true;
		}else{
			return false;
		}
	}else if(key == "后二直选和值" || key == "后二组选和值"){
		var heHz = parseInt(openCodeArrays[3]) + parseInt(openCodeArrays[4]);
		if(heHz == parseInt(code)){
			return true;
		}else{
			return false;
		}
	}else if(key == "趣味定单双"){
		var openCode1 = openCodeArrays[0];
		var openCode2 = openCodeArrays[1];
		var openCode3 = openCodeArrays[2];
		var openCode4 = openCodeArrays[3];
		var openCode5 = openCodeArrays[4];
		
		var unevenCount = 0; //奇数个数
		var evenCount = 0;  //偶数个数
		//计算开奖号码单双个数
        if(parseInt(openCode1) % 2 == 0){
        	evenCount++;
        }else{
        	unevenCount++;
        }
        if(parseInt(openCode2) % 2 == 0){
        	evenCount++;
        }else{
        	unevenCount++;
        }
        if(parseInt(openCode3) % 2 == 0){
        	evenCount++;
        }else{
        	unevenCount++;
        }
        if(parseInt(openCode4) % 2 == 0){
        	evenCount++;
        }else{
        	unevenCount++;
        }		
        if(parseInt(openCode5) % 2 == 0){
        	evenCount++;
        }else{
        	unevenCount++;
        }			
        var isAward = false;
        var everyPositionCode = code;
		if(everyPositionCode == "5单0双"){
			if(unevenCount == 5 && evenCount == 0){  //5个单0个双
				isAward = true;
			}
		}else if(everyPositionCode == "4单1双"){
			if(unevenCount == 4 && evenCount == 1){  //4个单1个双
				isAward = true;
			}
		}else if(everyPositionCode == "3单2双"){
			if(unevenCount == 3 && evenCount == 2){  //3个单2个双
				isAward = true;
			}
		}else if(everyPositionCode == "2单3双"){
			if(unevenCount == 2 && evenCount == 3){  //2个单3个双
				isAward = true;
			}
		}else if(everyPositionCode == "1单4双"){
			if(unevenCount == 1 && evenCount == 4){  //1个单4个双
				isAward = true;
			}
		}else if(everyPositionCode == "0单5双"){
			if(unevenCount == 0 && evenCount == 5){  //0个单5个双
				isAward = true;
			}
		}else{
			alert("未配置该号码.");
		}
		return isAward;
	}else{
		for(var i = 0; i < openCodeArrays.length; i++){
			if(openCodeArrays[i] == code){
				return true;
			}
		}
	}
};

/**
 * 撤单事件
 */
LotteryMsgPage.prototype.stopOrder = function() {
	frontOrderAction.stopOrderById(lotteryMsgPage.param.lotteryId,function(r) {
		if (r[0] != null && r[0] == "ok") {
			swal("恭喜您,撤单成功.");
			window.location.reload();
		} else if (r[0] != null && r[0] == "error") {
			swal(r[1]);
			window.location.reload();
		} else {
			swal("撤销投注记录的请求失败.");
		}
	});
};

var zHWQLHSHMap=new JS_OBJECT_MAP();
zHWQLHSHMap.put("1","大");
zHWQLHSHMap.put("2","小");
zHWQLHSHMap.put("3","单");
zHWQLHSHMap.put("4","双");
zHWQLHSHMap.put("5","五条");
zHWQLHSHMap.put("6","四条");
zHWQLHSHMap.put("7","葫芦");
zHWQLHSHMap.put("8","顺子");
zHWQLHSHMap.put("9","三条");
zHWQLHSHMap.put("10","两对");
zHWQLHSHMap.put("11","一对");
zHWQLHSHMap.put("12","散号");
zHWQLHSHMap.put("13","龙");
zHWQLHSHMap.put("14","虎");
zHWQLHSHMap.put("15","和");
var zHQSMap=new JS_OBJECT_MAP();
zHQSMap.put("0","0");
zHQSMap.put("1","1");
zHQSMap.put("2","2");
zHQSMap.put("3","3");
zHQSMap.put("4","4");
zHQSMap.put("5","5");
zHQSMap.put("6","6");
zHQSMap.put("7","7");
zHQSMap.put("8","8");
zHQSMap.put("9","9");
zHQSMap.put("10","10");
zHQSMap.put("11","11");
zHQSMap.put("12","12");
zHQSMap.put("13","13");
zHQSMap.put("14","14");
zHQSMap.put("15","15");
zHQSMap.put("16","16");
zHQSMap.put("17","17");
zHQSMap.put("18","18");
zHQSMap.put("19","19");
zHQSMap.put("20","20");
zHQSMap.put("21","21");
zHQSMap.put("22","22");
zHQSMap.put("23","23");
zHQSMap.put("24","24");
zHQSMap.put("25","25");
zHQSMap.put("26","26");
zHQSMap.put("27","27");
zHQSMap.put("28","小");
zHQSMap.put("29","大");
zHQSMap.put("30","单");
zHQSMap.put("31","双");
zHQSMap.put("32","0-3");
zHQSMap.put("33","4-7");
zHQSMap.put("34","8-11");
zHQSMap.put("35","12-15");
zHQSMap.put("36","16-19");
zHQSMap.put("37","20-23");
zHQSMap.put("38","24-27");
zHQSMap.put("39","豹子");
zHQSMap.put("40","顺子");
zHQSMap.put("41","对子");
zHQSMap.put("42","半顺");
zHQSMap.put("43","杂六");
var zHDNMap=new JS_OBJECT_MAP();
zHDNMap.put("1","牛1");
zHDNMap.put("2","牛2");
zHDNMap.put("3","牛3");
zHDNMap.put("4","牛4");
zHDNMap.put("5","牛5");
zHDNMap.put("6","牛6");
zHDNMap.put("7","牛7");
zHDNMap.put("8","牛8");
zHDNMap.put("9","牛9");
zHDNMap.put("10","牛牛");
zHDNMap.put("11","牛大");
zHDNMap.put("12","牛小");
zHDNMap.put("13","牛单");
zHDNMap.put("14","牛双");
zHDNMap.put("15","无牛");
lotteryMsgPage.kindKeyValue.summationDescriptionMap.put("五球/龙虎/梭哈",zHWQLHSHMap);
lotteryMsgPage.kindKeyValue.summationDescriptionMap.put("前三总和",zHQSMap);
lotteryMsgPage.kindKeyValue.summationDescriptionMap.put("中三总和",zHQSMap);
lotteryMsgPage.kindKeyValue.summationDescriptionMap.put("后三总和",zHQSMap);
lotteryMsgPage.kindKeyValue.summationDescriptionMap.put("斗牛",zHDNMap);