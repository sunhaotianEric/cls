<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>投注明细订单详情</title>
    <link rel="shortcut icon" href="favicon.ico">
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
	<%
	  String lotteryId = request.getParameter("id");	  
	%>
	
	<style type="text/css">
	    .input-group-addon,.form-control{border:0;}
	    .input-group-addon{font-weight: 600;}
	    .alert-msg-bg{display:none;position:fixed;top:0px;bottom:0px;left:0px;right:0px;z-index:9;background:#000; opacity:0.4;}
		.alert-msg{display:none;position:fixed;top:50%;right:0px;left:0px;margin:-200px auto 0 auto;z-index:10;width:500px;min-height:initial;background:#fff;box-shadow: 0px 4px 6px 0px rgba(0,0,0,0.4);-webkit-box-shadow: 0px 4px 6px 0px rgba(0,0,0,0.4);}
		.alert-msg .msg-head{position:relative;height:40px;line-height:40px;background:#2f4050;}
		.alert-msg .msg-head p{color:#fff;font-size:16px;padding-left:28px;}
		.alert-msg .msg-head .close{position:absolute;top:0px;bottom:0px;right:20px;margin:auto 0;cursor:pointer;opacity: 1; width: 12px;}
		.alert-msg .msg-content{padding:26px 28px; box-sizing:border-box;}
    </style>
</head>
<body class="gray-bg">
  	<div class="alert-msg-bg"></div>
	<div class="alert-msg alert-msg1" >
	    <div class="msg-head">
	        <p>投注详情</p><img class="close" src="<%=path%>/img/close.png" onClick='(function msg_show(){$(".alert-msg").stop(false,true).delay(200).fadeOut(500);$(".alert-msg-bg").stop(false,true).fadeOut(500);})()' />
	  </div>
	  <div class="msg-content">
	
	      <div class="line line-textarea">
	          <div class="line-content ">
	              <div class="line-text"><textarea style="width:100%;height:120px;" class="textArea" id="kindPlayCodeDes"></textarea></div>
	            </div>
	        </div>
	    </div>
	</div>
  
	<div class="animated fadeIn">
    	<div class="ibox-content ibox">
        	<div class="row m-b-sm m-t-sm">
                  <form class="form-horizontal">
                      <div class="row">
                          <div class="col-sm-4">
                              <div class="input-group m-b">
                                  <span class="input-group-addon">用户名:</span>
                                  <span class="form-control" id="userName">-</span></div>
                          </div>
                          <div class="col-sm-4">
                              <div class="input-group m-b">
                                  <span class="input-group-addon">注单号:</span>
                                  <span class="form-control" id="lotteryId" >-</span></div>
                          </div>
                          <div class="col-sm-4">
                              <div class="input-group m-b">
                                  <span class="input-group-addon">彩种:</span>
                                  <span class="form-control" id="lotteryTypeDes">-</span>
                              </div>
                          </div>
                          <div class="col-sm-4">
                              <div class="input-group m-b">
                                  <span class="input-group-addon">期号:</span>
                                  <span class="form-control" id="lotteryExpect">-</span></div>
                          </div>
                          <div class="col-sm-4">
                              <div class="input-group m-b">
                                  <span class="input-group-addon">投注金额:</span>
                                  <span class="form-control" id="lotteryPayMoney">-</span></div>
                          </div>
                          <div class="col-sm-4">
                              <div class="input-group m-b">
                                  <span class="input-group-addon">开奖号码:</span>
                                  <span class="form-control" id="opencodes"></span></div>
                          </div>
                          <div class="col-sm-4">
                              <div class="input-group m-b">
                                  <span class="input-group-addon">中奖金额:</span>
                                  <span class="form-control" id="lotteryWinCost">-</span></div>
                          </div>
                          <div class="col-sm-4">
                              <div class="input-group m-b">
                                  <span class="input-group-addon">是否追号:</span>
                                  <span class="form-control" id="afterNumber">是</span>
                              </div>
                          </div>
                          <div class="col-sm-4">
                              <div class="input-group m-b">
                                  <span class="input-group-addon">投注时间：</span>
                                  <span class="form-control" id='lotteryCreateDate'>-</span></div>
                          </div>
                          <div class="col-sm-4">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">总注数：</span>
                                    <span class="form-control" id='lotteryCount'>0注</span></div>
                            </div>
                            <div class="col-sm-4">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">投注模式：</span>
                                    <span class="form-control" id='lotteryawardModel'>-</span></div>
                            </div>
                      </div>
                  </form>
                  <div class="jqGrid_wrapper">
                      <div class="ui-jqgrid ">
                          <div class="ui-jqgrid-view table-responsive">
                              <div class="ui-jqgrid-hdiv">
                                  <div class="ui-jqgrid-hbox" style="padding-right:0">
                                    <table class="ui-jqgrid-htable ui-common-table table table-bordered text-center">
                                        <thead>
                                            <tr class="ui-jqgrid-labels">
                                                <th class="ui-th-column ui-th-ltr">玩法</th>
                                                <th class="ui-th-column ui-th-ltr">投注内容</th>
                                                <th class="ui-th-column ui-th-ltr">注数</th>
                                                <th class="ui-th-column ui-th-ltr">倍数</th>
                                                <th class="ui-th-column ui-th-ltr">投注金额</th>
                                                 <th class="ui-th-column ui-th-ltr">模式</th>
                                                <th class="ui-th-column ui-th-ltr">状态/奖金</th></tr>
                                        </thead>
                                        <tbody id="codeList">
                                    
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--详情弹出层-->
    <div id="lottery-info" class="modal fade" aria-hidden="true">
        <div class="modal-dialog" style="width:300px;">
            <div class="modal-content" style="max-height:500px;overflow:auto;">
                <div class="modal-body">
                    <textarea style="width: 100%;height: 200px;">1,3,5,7,9
                        1,3,5,7,91,3,5,7,91,3,5,7,9
                    </textarea> 
                </div>
            </div>
        </div>
    </div>

<!-- js -->
<script type="text/javascript" src="<%=path%>/admin/lotterymsg/js/lotterymsg.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerOrderAction.js'></script>
<script type="text/javascript">
  lotteryMsgPage.param.lotteryId = <%=lotteryId %>;
</script>

</body>
</html>