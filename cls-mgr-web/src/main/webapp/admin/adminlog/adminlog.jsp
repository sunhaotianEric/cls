<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
    String logip = request.getParameter("logip");
    String bizSystem = request.getParameter("bizSystem")==null?"":request.getParameter("bizSystem");
    String adminName = request.getParameter("adminName")==null?"":request.getParameter("adminName");
    String isTourist = request.getParameter("isTourist");
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>用户登陆日志</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>

<%--     <script type='text/javascript' src='<%=path%>/dwr/interface/managerBizSystemAction.js'></script> --%>
</head>
<body>
	<div class="animated fadeIn">
    	<div class="ibox-content">
        	<form class="form-horizontal" id="queryForm">
                <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                    <div class="row">
                       <div class="col-sm-3">
                            <div class="input-group m-b">
                                <span class="input-group-addon">商户</span>
                                <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control" hasSuperSystemOption="true"/></div>
                        </div>
                        <div class="col-sm-3">
                            <div class="input-group m-b">
                                <span class="input-group-addon">用户名</span>
                                <input type="text" value="" id="adminName" name="adminName" class="form-control"></div>
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group m-b">
                                <span class="input-group-addon">IP地址</span>
                                <input type="text" value="" id="logip" name= "logip" class="form-control"></div>
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group m-b">
                                <span class="input-group-addon">用户地区</span>
                                <input type="text" value="" id="address" name="address"  class="form-control"></div>
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group m-b">
                                <span class="input-group-addon">登录域名</span>
                                <input type="text" value="" id="loginDomain" name="loginDomain" class="form-control"></div>
                        </div>
             		</div>
             		<div class="row">
                        <div class="col-sm-2">
	                         <div class="input-group m-b">
	                             <span class="input-group-addon">登录端口</span>
	                             <select class="ipt form-control" id="loginPort" name="loginPort">
		                             <option value="">=请选择=</option>
		                             <option value="80">http(80)</option>
		                             <option value="443">https(443)</option>
		                             <option value="-1">其他</option>
	                             </select>
                             </div>
	                     </div>	
                        <div class="col-sm-4">
                             <div class="input-group m-b">
                                 <span class="input-group-addon">日期</span>
                                 <div class="input-daterange input-group" id="datepicker">
                                     <input class="form-control layer-date" placeholder="开始日期" name="logtimeStart" id="startime" readonly="readonly" style="background-color: white;" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})">
                                     <span class="input-group-addon">到</span>
                                     <input class="form-control layer-date" placeholder="结束日期" name="logtimeEnd" id="endtime" readonly="readonly" style="background-color: white;" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"></div>
                             </div>
                        </div>
                        <div class="col-sm-2">
                        	<div class="form-group nomargin" style="margin-left:15px;">
					            <button type="button" class="btn btn-xs btn-info" onclick="loginLogPage.findLoginLogByQuery(1)">今 天</button>
					            <button type="button" class="btn btn-xs btn-danger" onclick="loginLogPage.findLoginLogByQuery(-1)">昨 天</button>
					        	<button type="button" class="btn btn-xs btn-warning" onclick="loginLogPage.findLoginLogByQuery(-7)">最近7天</button><br/>
					        	<button type="button" class="btn btn-xs btn-success" onclick="loginLogPage.findLoginLogByQuery(30)">本 月</button>
					        	<button type="button" class="btn btn-xs btn-primary" onclick="loginLogPage.findLoginLogByQuery(-30)">上 月</button>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group text-right">
                                <button type="button" class="btn btn-outline btn-default btn-js" onclick="loginLogPage.findLoginLogByQuery()"><i class="fa fa-search"></i>&nbsp;查询</button>
                                </div>
                        </div>
             		</div>
            	</c:if>
            	<c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="input-group m-b">
                                <span class="input-group-addon">用户名</span>
                                <input type="text" value="" id="adminName" name="adminName" class="form-control"></div>
                        </div>
                        <div class="col-sm-3">
                            <div class="input-group m-b">
                                <span class="input-group-addon">IP地址</span>
                                <input type="text" value="" id="logip" name="logip" class="form-control"></div>
                        </div>
                       <div class="col-sm-3">
                            <div class="input-group m-b">
                                <span class="input-group-addon">用户地区</span>
                                <input type="text" value="" id="address" name="address" class="form-control"></div>
                        </div>
                    	<div class="col-sm-3">
                            <div class="input-group m-b">
                                <span class="input-group-addon">登录域名</span>
                                <input type="text" value="" id="loginDomain" name="loginDomain" class="form-control"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">
	                         <div class="input-group m-b">
	                             <span class="input-group-addon">登录端口</span>
	                            
	                             <select class="ipt form-control" id="loginPort" name="loginPort">
		                             <option value="">=请选择=</option>
		                             <option value="80">http(80)</option>
		                             <option value="443">https(443)</option>
		                             <option value="-1">其他</option>
	                             </select>
                             </div>
	                     </div>	
                    	<div class="col-sm-4">
                             <div class="input-group m-b">
                                 <span class="input-group-addon">日期</span>
                                 <div class="input-daterange input-group" id="datepicker">
                                     <input class="form-control layer-date" placeholder="开始日期" id="startime" name="logtimeStart" readonly="readonly" style="background-color: white;" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})">
                                     <span class="input-group-addon">到</span>
                                     <input class="form-control layer-date" placeholder="结束日期" id="endtime" name="logtimeEnd" readonly="readonly" style="background-color: white;" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"></div>
                             </div>
                        </div>
                        <div class="col-sm-2">
                        	<div class="form-group nomargin">
					            <button type="button" class="btn btn-xs btn-info" onclick="loginLogPage.findLoginLogByQuery(1)">今 天</button>
					            <button type="button" class="btn btn-xs btn-danger" onclick="loginLogPage.findLoginLogByQuery(-1)">昨 天</button>
					        	<button type="button" class="btn btn-xs btn-warning" onclick="loginLogPage.findLoginLogByQuery(-7)">最近7天</button><br/>
					        	<button type="button" class="btn btn-xs btn-success" onclick="loginLogPage.findLoginLogByQuery(30)">本 月</button>
					        	<button type="button" class="btn btn-xs btn-primary" onclick="loginLogPage.findLoginLogByQuery(-30)">上 月</button>
                            </div>
                        </div>
	                    <div class="col-sm-2">
	                         <div class="form-group nomargin text-right">
	                             <button type="button" class="btn btn-outline btn-default btn-js" onclick="loginLogPage.findLoginLogByQuery()"><i class="fa fa-search"></i>&nbsp;查 询</button>
	                             </div>
	                    </div>
                    </div>
            	</c:if>
         	</form>
            <div class="jqGrid_wrapper">
                <div class="ui-jqgrid ">
                    <div class="ui-jqgrid-view">
                        <div class="ui-jqgrid-hdiv">
                            <div class="ui-jqgrid-hbox" style="padding-right:0">
                                <table class="ui-jqgrid-htable ui-common-table table table-bordered" id="loginLogTable">
                                    <thead>
                                        <tr class="ui-jqgrid-labels">
                                            <th class="ui-th-column ui-th-ltr">商户</th>
                                            <th class="ui-th-column ui-th-ltr">用户名</th>
                                            <th class="ui-th-column ui-th-ltr">IP地址</th>
                                            <th class="ui-th-column ui-th-ltr">用户地区</th>
                                            <th class="ui-th-column ui-th-ltr">登陆时间</th>
                                            <th class="ui-th-column ui-th-ltr">登陆域名</th>
                                            <th class="ui-th-column ui-th-ltr">登陆信息</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/adminlog/js/adminlog.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerAdminLogAction.js'></script>
    <script type="text/javascript">
loginLogPage.queryParam.logip = '<%=logip %>';
loginLogPage.queryParam.bizSystem = '<%=bizSystem %>';
loginLogPage.queryParam.adminName = '<%=adminName %>';
var isTouristParameter = '<%=isTourist %>';
if(loginLogPage.queryParam.logip == 'null'){
	loginLogPage.queryParam.logip = null;
}
if(loginLogPage.queryParam.bizSystem == ''){
	loginLogPage.queryParam.bizSystem = null;
}

if(loginLogPage.queryParam.adminName == ''){
	loginLogPage.queryParam.adminName = null;
}
</script>    
</body>
</html>

