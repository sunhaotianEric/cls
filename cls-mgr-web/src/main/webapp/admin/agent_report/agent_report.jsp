<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
    
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>代理报表</title>
<style type="text/css">
.wrapper {padding:0 10px}
.wrapper-content {padding:10px}
</style>
<jsp:include page="/admin/include/include.jsp"></jsp:include>

</head>
    <body>
        <div class="animated fadeIn" style="padding: 0 5px;">
            <div class="row">
                <div class="col-sm-12">
                    <!-- <div class="ibox "> -->
                        <div class="ibox-content">
                            <div class="row m-b-sm m-t-sm">
                                <form class="form-horizontal" id="userDayConsumeQuery">
                                <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                                    <div class="row">
                                      <div class="col-sm-3">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">商户</span>
                                                <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
                                                </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">用户名</span>
                                                <input name="userName" id="userName" type="text" value="" class="form-control"></div>
                                        </div>
                                        <div class="col-sm-4">
						                     <div class="input-group m-b">
						                        <span class="input-group-addon">开始时间</span>
						                        <div class="input-daterange input-group">
						                        <input class="form-control layer-date" placeholder="开始日期" onclick="laydate({istime: false, format: 'YYYY-MM-DD',max: laydate.now(0)})" name="belongDateStart" id="belongDateStart">
						                      	<span class="input-group-addon">到</span>
						                      	<input class="form-control layer-date" placeholder="结束日期" onclick="laydate({istime: false, format: 'YYYY-MM-DD',max: laydate.now(0)})" name="belongDateEnd" id="belongDateEnd" ></div>
						                    </div>
						                </div>
						                <div class="col-sm-2">
				                        	<div class="form-group nomargin">
									            <button type="button" class="btn btn-xs btn-info" onclick="agentReportPage.getTotalUsersConsumeReportsForQuery(1)">今 天</button>
									            <button type="button" class="btn btn-xs btn-danger" onclick="agentReportPage.getTotalUsersConsumeReportsForQuery(-1)">昨 天</button>
									        	<button type="button" class="btn btn-xs btn-warning" onclick="agentReportPage.getTotalUsersConsumeReportsForQuery(-7)">最近7天</button><br/>
									        	<button type="button" class="btn btn-xs btn-success" onclick="agentReportPage.getTotalUsersConsumeReportsForQuery(30)">本 月</button>
									        	<button type="button" class="btn btn-xs btn-primary" onclick="agentReportPage.getTotalUsersConsumeReportsForQuery(-30)">上 月</button>
				                            </div>
				                        </div>
				                        <div class="col-sm-1">
                                            <div class="form-group nomargin text-right">
                                                <button type="button" class="btn btn-outline btn-default btn-kj" onclick="agentReportPage.getTotalUsersConsumeReportsForQuery()"><i class="fa fa-search"></i>&nbsp;查 询</button></div>
                                        </div>
                                    </div>
                                    <div class="row">
				                        <div class="col-sm-3">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">排序</span>
	                                                <select id='sortMunber' name="sortMunber" class="ipt">
								                   		<option value="0">按盈利从高到低</option>
								                   		<option value='1'>按盈利从低到高</option>
								                   		<option value="2">按投注到高到低</option>
								                   		<option value='3'>按投注从低到高</option>
								                   		<option value="4">按中奖从高到低</option>
								                   		<option value='5'>按中奖从低到高</option>
								                    </select>
                                            </div>
                                        </div>
                                    </div>
                                    </c:if>
                                    <c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">用户名</span>
                                                <input  name="userName" id="userName" type="text" value="" class="form-control" width="40%"></div>
                                        </div>
                                         <div class="col-sm-4">
						                     <div class="input-group m-b">
						                        <span class="input-group-addon">开始时间</span>
						                        <div class="input-daterange input-group">
						                        <input class="form-control layer-date" placeholder="开始日期" onclick="laydate({istime: false, format: 'YYYY-MM-DD',max: laydate.now(0)})" name="belongDateStart" id="belongDateStart">
						                      	<span class="input-group-addon">到</span>
						                      	<input class="form-control layer-date" placeholder="结束日期" onclick="laydate({istime: false, format: 'YYYY-MM-DD',max: laydate.now(0)})" name="belongDateEnd" id="belongDateEnd" ></div>
						                    </div>
						                </div>
						                <div class="col-sm-2">
				                        	<div class="form-group nomargin">
									            <button type="button" class="btn btn-xs btn-info" onclick="agentReportPage.getTotalUsersConsumeReportsForQuery(1)">今 天</button>
									            <button type="button" class="btn btn-xs btn-danger" onclick="agentReportPage.getTotalUsersConsumeReportsForQuery(-1)">昨 天</button>
									        	<button type="button" class="btn btn-xs btn-warning" onclick="agentReportPage.getTotalUsersConsumeReportsForQuery(-7)">最近7天</button>
									        	<button type="button" class="btn btn-xs btn-success" onclick="agentReportPage.getTotalUsersConsumeReportsForQuery(30)">本 月</button>
									        	<button type="button" class="btn btn-xs btn-primary" onclick="agentReportPage.getTotalUsersConsumeReportsForQuery(-30)">上 月</button>
				                            </div>
				                        </div>
				                        <div class="col-sm-2">
                                            <div class="form-group nomargin text-right">
                                                <button type="button" class="btn btn-outline btn-default btn-kj" onclick="agentReportPage.getTotalUsersConsumeReportsForQuery()"><i class="fa fa-search"></i>&nbsp;查 询</button></div>
                                        </div>
                                    </div>
                                    <div class="row">
				                        <div class="col-sm-3">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">排序</span>
	                                                <select id='sortMunber' name="sortMunber" class="ipt">
								                   		<option value="0">按盈利从高到低</option>
								                   		<option value='1'>按盈利从低到高</option>
								                   		<option value="2">按投注到高到低</option>
								                   		<option value='3'>按投注从低到高</option>
								                   		<option value="4">按中奖从高到低</option>
								                   		<option value='5'>按中奖从低到高</option>
								                    </select>
                                            </div>
                                        </div>
                                    </div>
                                    </c:if>
                                </form>
                            </div>
                            <div class="jqGrid_wrapper">
                                <div class="ui-jqgrid ">
                                    <div class="ui-jqgrid-view">
                                        <div class="ui-jqgrid-hdiv">
                                            <div class="ui-jqgrid-hbox" style="padding-right:0">
                                                <table class="ui-jqgrid-htable ui-common-table table table-bordered" id="userMoneyTotalTable">
                                                    <thead>
                                                        <tr class="ui-jqgrid-labels">
												          <th class="ui-th-column ui-th-ltr">商户</th>
                                                          <th class="ui-th-column ui-th-ltr">用户名</th>
												          <th class="ui-th-column ui-th-ltr">充值余额</th>
												          <th class="ui-th-column ui-th-ltr">提现余额</th>
												          <th class="ui-th-column ui-th-ltr">投注金额</th>
												          <th class="ui-th-column ui-th-ltr">中奖金额</th>
												          <th class="ui-th-column ui-th-ltr">团队返点</th>
												          <th class="ui-th-column ui-th-ltr">活动礼金</th>
												          <th class="ui-th-column ui-th-ltr">行政提出</th>
												          <th class="ui-th-column ui-th-ltr">团队盈利</th>
												          <th class="ui-th-column ui-th-ltr">投注人数</th>
												          <th class="ui-th-column ui-th-ltr">首充人数</th>
												          <th class="ui-th-column ui-th-ltr">注册人数</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                         
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>          
                                </div>
                            </div>
                        </div>
                   <!--  </div> -->
                </div>
            </div>
        </div>
<!-- js -->
<script type="text/javascript" src="<%=path%>/admin/agent_report/js/agent_report.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/manageAgentReportAction.js'></script>
<script type='text/javascript' src='<%=path%>/dwr/interface/managerUserAction.js'></script>
</body>
</html>

