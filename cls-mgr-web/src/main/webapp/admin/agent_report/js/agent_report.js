function AgentReportPage(){}
var agentReportPage = new AgentReportPage();

agentReportPage.param = {
};

//分页参数
agentReportPage.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize
};
agentReportPage.reports=null;
agentReportPage.totalUser=null;
/**
 * 查询参数
 */
agentReportPage.queryParam = {
		userName : null,
		bizSystem:null,
		belongDateStart : null,
		belongDateEnd : null,
		gain:0
};

$(document).ready(function() {
	var newtimems = new Date().getTime()-(24*60*60*1000);
	var yesd = new Date(newtimems).format("yyyy-MM-dd");
	$("#belongDateStart").val(yesd);
	$("#belongDateEnd").val(yesd)
	
	agentReportPage.initTableData(); //根据条件,查找资金明细
});
	
/**
 * 加载所有的登陆日志数据
 */
AgentReportPage.prototype.getTotalUsersConsumeReportsForQuery = function(dateRange){
	if(dateRange != undefined && dateRange != null && dateRange != ''){
		commonPage.setDateRangeTime(dateRange, "belongDateStart", "belongDateEnd",1);	
	}
	agentReportPage.queryParam = {};
	agentReportPage.queryParam = getFormObj($("#userDayConsumeQuery"));
	var userName = $("#userName").val();
	
	if(currentUser.bizSystem == 'SUPER_SYSTEM'){
		agentReportPage.queryParam.bizSystem = getSearchVal("bizSystem");	
	}else{
		agentReportPage.queryParam.bizSystem = currentUser.bizSystem;
	}
	
	if(userName == ""){
		agentReportPage.queryParam.userName = null;
	}else{
		agentReportPage.queryParam.userName = userName;
	}
	
	agentReportPage.table.ajax.reload();
};



/**
 * 初始化列表数据
 */
AgentReportPage.prototype.initTableData = function() {
	agentReportPage.queryParam = getFormObj($("#userDayConsumeQuery"));
	if(currentUser.bizSystem == 'SUPER_SYSTEM'){
		agentReportPage.queryParam.bizSystem = getSearchVal("bizSystem");	
	}else{
		agentReportPage.queryParam.bizSystem = currentUser.bizSystem;
	}
	
	agentReportPage.table = $('#userMoneyTotalTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"scrollX": true,
		"iDisplayLength" : defaultPageSize,
		"ajax":function (data, callback, settings) {
			agentReportPage.pageParam.pageSize = data.length;//页面显示记录条数
			agentReportPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	manageAgentReportAction.getAgentReport(agentReportPage.queryParam,agentReportPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": "bizSystemName","bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "userName",
		            	  render: function(data, type, row, meta) {
		            		   return "<a href='javascript:void(0)' style='color:blue;text-decoration:underline;'  onclick='commonPage.showUserMsgPage(this,"+row.userId+")'>"+ row.userName +"</a>";
		            		}
		            },
		            {"data": "recharge",
		            	render: function(data, type, row, meta) {
		            		return row.recharge.toFixed(commonPage.param.fixVaue);
		            	}},
		            {"data": "withdraw",
		            	render: function(data, type, row, meta) {
		            		return row.withdraw.toFixed(commonPage.param.fixVaue);
		            	}},
		  
		            {"data":"lottery",
		            	render: function(data, type, row, meta) {
		            		return row.lottery.toFixed(commonPage.param.fixVaue);
		            	}},
		            {"data": "win",
		            	render: function(data, type, row, meta) {
		            		return row.win.toFixed(commonPage.param.fixVaue);
		            	}},
		             {"data": null,
			            	render: function(data, type, row, meta) {
			            		return (row.rebate + row.percentage).toFixed(commonPage.param.fixVaue);
			            	}},
	            	{"data": null,
		            	render: function(data, type, row, meta) {
		            		return (row.rechargepresent + row.activitiesmoney + row.systemaddmoney).toFixed(commonPage.param.fixVaue);
		            	}},
		         
		            {"data":"managereducemoney",
		            	render: function(data, type, row, meta) {
		            		return row.managereducemoney.toFixed(commonPage.param.fixVaue);
		            	}},
		            {"data": "gain",
		            	render: function(data, type, row, meta) {
		            		return row.gain.toFixed(commonPage.param.fixVaue);
		            	}},
		            {"data": "lotteryCount",
		            	render: function(data, type, row, meta) {
		            		return row.lotteryCount;
		            	}},
		            {"data": "firstRechargeCount",
		            	render: function(data, type, row, meta) {
		            		return row.firstRechargeCount;
		            	}},
		            {"data": "regCount",
		            	render: function(data, type, row, meta) {
		            		return row.regCount;
		              }}
	            ]
	}).api(); 

}

/**
 * 刷新列表数据
 */
AgentReportPage.prototype.refreshConsumeReportsPages = function(reports,totalUser){
	if(reports.length != 0){
		var reportListObj = $("#userMoneyTotalTable tbody");
		var str = "";
		var totalMoney = 0;
		var totalRecharge = 0;
		var totalRechargePresent = 0;
		var totalActivitiesMoney = 0;
		var totalSystemAddMoney = 0;
		var totalSystemReduceMoney = 0;
		var totalWithDraw = 0;
		var totalWithDrawFee= 0;
		var totalPayTranfer = 0;
		var totalIncomeTranfer = 0;
		var totalLottery = 0;
		var totalShopPoint = 0;
		var totalWin = 0;
		var totalRebate = 0;
		var totalPercentage = 0;
		var totalHalfMonthBonus = 0;
		var totalDayBonus = 0;
		var totalMonthSalary = 0;
		var totalDaySalary = 0;
		var totalDayCommission = 0;
		var gain = 0;
		var totalGain = 0;
		
		var totalOther = 0;
		
		//记录数据显示
		for(var i = 0; i < reports.length; i++){
			var report = reports[i];
			totalMoney      += report.money;
			totalRecharge   += report.recharge;
			totalRechargePresent += report.rechargepresent;
			totalActivitiesMoney += report.activitiesmoney;
			totalSystemAddMoney += report.systemaddmoney;
			totalSystemReduceMoney += report.systemreducemoney;
			totalWithDraw   += report.withdraw;
			totalWithDrawFee += report.withdrawfee;
			totalPayTranfer += report.paytranfer;
			totalIncomeTranfer += report.incometranfer;
			totalLottery    += report.lottery;
			totalWin        += report.win;
			totalRebate     += report.rebate;
			totalPercentage += report.percentage;
			totalHalfMonthBonus += report.halfmonthbonus;
			totalDayBonus += report.daybonus;
			totalMonthSalary += report.monthsalary;
			totalDaySalary += report.daysalary;
			totalDayCommission += report.daycommission;
			gain = report.rechargepresent + report.activitiesmoney + report.systemaddmoney + report.win
			+ report.rebate + report.percentage + report.halfmonthbonus + report.daysalary + report.incometranfer
			- report.lottery- report.paytranfer - report.systemreducemoney - report.withdrawfee;
			totalGain       += gain;
			totalOther       += report.other;
		}  
		
		str += "<tr style='color:red'>";
		str += "  <td>当前页总计</td>";
		if(currentUser.bizSystem=='SUPER_SYSTEM'){
			str += "  <td></td>";
		}
		str += "  <td>"+ totalMoney.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ totalRecharge.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ totalRechargePresent.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ totalActivitiesMoney.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ totalSystemAddMoney.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ totalSystemReduceMoney.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ totalWithDraw.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ totalWithDrawFee.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ totalPayTranfer.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ totalIncomeTranfer.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ totalLottery.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ totalWin.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ totalRebate.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ totalPercentage.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ totalHalfMonthBonus.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ totalDayBonus.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ totalMonthSalary.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ totalDaySalary.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ totalDayCommission.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ totalGain.toFixed(commonPage.param.fixVaue) +"</td>";
		/*str += "  <td>"+ totalOther.toFixed(commonPage.param.fixVaue) +"</td>";*/
		reportListObj.append(str);
		str = "";
		str += "<tr style='color:red'>";
		str += "  <td>所有用户总计</td>";
		if(currentUser.bizSystem=='SUPER_SYSTEM'){
			str += "  <td></td>";
		}
		if(totalUser==null)
		{
			str += "<td>0.000</td><td>0.000</td><td>0.000</td><td>0.000</td><td>0.000</td><td>0.000</td><td>0.000</td>";
			str += "<td>0.000</td><td>0.000</td><td>0.000</td><td>0.000</td><td>0.000</td><td>0.000</td><td>0.000</td>";
			str += "<td>0.000</td><td>0.000</td><td>0.000</td><td>0.000</td><td>0.000</td><td>0.000</td><td>0.000</td>";
		}else{
			str += "  <td>"+ totalUser.money.toFixed(commonPage.param.fixVaue) +"</td>";
			str += "  <td>"+ totalUser.recharge.toFixed(commonPage.param.fixVaue) +"</td>";
			str += "  <td>"+ totalUser.rechargepresent.toFixed(commonPage.param.fixVaue) +"</td>";
			str += "  <td>"+ totalUser.systemaddmoney.toFixed(commonPage.param.fixVaue) +"</td>";
			str += "  <td>"+ totalUser.systemreducemoney.toFixed(commonPage.param.fixVaue) +"</td>";
			str += "  <td>"+ totalUser.activitiesmoney.toFixed(commonPage.param.fixVaue) +"</td>";
			str += "  <td>"+ totalUser.withdraw.toFixed(commonPage.param.fixVaue) +"</td>";
			str += "  <td>"+ totalUser.withdrawfee.toFixed(commonPage.param.fixVaue) +"</td>";
			str += "  <td>"+ totalUser.paytranfer.toFixed(commonPage.param.fixVaue) +"</td>";
			str += "  <td>"+ totalUser.incometranfer.toFixed(commonPage.param.fixVaue) +"</td>";
			str += "  <td>"+ totalUser.lottery.toFixed(commonPage.param.fixVaue) +"</td>";
			str += "  <td>"+ totalUser.win.toFixed(commonPage.param.fixVaue) +"</td>";
			str += "  <td>"+ totalUser.rebate.toFixed(commonPage.param.fixVaue) +"</td>";
			str += "  <td>"+ totalUser.percentage.toFixed(commonPage.param.fixVaue) +"</td>";
			str += "  <td>"+ totalUser.halfmonthbonus.toFixed(commonPage.param.fixVaue) +"</td>";
			str += "  <td>"+ totalUser.daybonus.toFixed(commonPage.param.fixVaue) +"</td>";
			str += "  <td>"+ totalUser.monthsalary.toFixed(commonPage.param.fixVaue) +"</td>";
			str += "  <td>"+ totalUser.daysalary.toFixed(commonPage.param.fixVaue) +"</td>";
			str += "  <td>"+ totalUser.daycommission.toFixed(commonPage.param.fixVaue) +"</td>";
			var totalGain = totalUser.rechargepresent + totalUser.activitiesmoney + totalUser.systemaddmoney + totalUser.win
			+ totalUser.rebate + totalUser.percentage + totalUser.halfmonthbonus + totalUser.daysalary + totalUser.incometranfer
			- totalUser.lottery- totalUser.paytranfer - totalUser.systemreducemoney - totalUser.withdrawfee;
			str += "  <td>"+ totalGain.toFixed(commonPage.param.fixVaue) +"</td>";
		/*	str += "  <td>"+ totalUser.other.toFixed(commonPage.param.fixVaue) +"</td>";*/
		}
		str += "</tr>"; 
		reportListObj.append(str);
		agentReportPage.table.columns.adjust();
	}

};
