function ShareOrderPage(){}
var shareOrderPage = new ShareOrderPage();

shareOrderPage.queryParam={
		bizSystem:null,
		roomId:null,
		userName:null,
		createDate:null,
		updateDate:null
};

//分页参数
shareOrderPage.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize
};

$(document).ready(function() {
	shareOrderPage.initTableData();
});

ShareOrderPage.prototype.initTableData=function(){
	shareOrderPage.table = $('#shareOrderTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"scrollX": true,
		"iDisplayLength" : defaultPageSize,
		"ajax":function (data, callback, settings) {
		shareOrderPage.pageParam.pageSize = data.length;//页面显示记录条数
		shareOrderPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerShareOrderAction.getShareOrderPage(shareOrderPage.queryParam,shareOrderPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": "bizSystemName","bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "roomId"},
		            {"data": "userName" },
		            {"data": "lotteryId",
		            	render:function(data, type, row, meta){
		            		return "<a style='color: blue;text-decoration: underline;' href='javascript:void(0)' onclick='shareOrderPage.getOrderCodesDes(this,"+row.orderid+")'>"+data+"</a>";
		            	}
		            },
		            {"data": "createDateStr" }
	            ]
	}).api(); 
}

ShareOrderPage.prototype.findShareOrder=function(){
	var bizSystem=$("#bizSystem").val();
	var roomId=$("#roomId").val();
	var userName=$("#userName").val();
	var createDate=$("#createDate").val();
	var updateDate=$("#updateDate").val();
	
	if(bizSystem!=null&&bizSystem!=''){
		shareOrderPage.queryParam.bizSystem=bizSystem;
	}else{
		shareOrderPage.queryParam.bizSystem=null;
	}
	if(roomId!=null&&roomId!=''){
		shareOrderPage.queryParam.roomId=roomId;
	}else{
		shareOrderPage.queryParam.roomId=null;
	}
	if(userName!=null&&userName!=''){
		shareOrderPage.queryParam.userName=userName;
	}else{
		shareOrderPage.queryParam.userName=null;
	}
	if(createDate!=null&&createDate!=''){
		shareOrderPage.queryParam.createDate=new Date(createDate);
	}else{
		shareOrderPage.queryParam.createDate=null;
	}
	if(updateDate!=null&&updateDate!=''){
		shareOrderPage.queryParam.updateDate=new Date(updateDate);
	}else{
		shareOrderPage.queryParam.updateDate=null;
	}		
	shareOrderPage.table.ajax.reload();
}

/**
 * 获取订单详情
 */
ShareOrderPage.prototype.getOrderCodesDes = function(afterNumber,orderId){
	var url="";
	//是否追号
	if(afterNumber==1){
		url = contextPath + "/managerzaizst/lotterymsg/"+orderId+"/lotterymsg.html";
	}else{
		url = contextPath + "/managerzaizst/lotterymsg/"+orderId+"/lotterymsg.html";
	}
	
	layer.open({
          type: 2,
          title: '订单详情',
          maxmin: false,
          shadeClose: true,
          //点击遮罩关闭层
          area: ['900px', '450px'],
          content: url,
          cancel: function(index){ 
        	  layer.close();
       	   }
      });
	
	
};