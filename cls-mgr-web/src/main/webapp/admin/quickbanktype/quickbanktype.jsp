<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>第三方支付码表设置</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    
</head>
<body>
<div class="animated fadeIn">
            <div class="ibox-content">
                 <div class="row">
                    <form class="form-horizontal" id="quickBankTypeForm">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">第三方支付名称</span>
                                    <cls:thirdPaySel name="chargeType" id="chargeType" options="class:ipt form-control" />
                            </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">支付类型</span>
                           <cls:enmuSel name="bankType" className="com.team.lottery.enums.EBankInfo" options="class:ipt form-control" id="bankType"/>
                                    
                                </div>
                            </div>
                     
                            <div class="col-sm-6">
                                <div class="form-group nomargin text-right">
                                    <button type="button" class="btn btn-outline btn-default btn-js" onclick="quickBankTypePage.pageParam.pageNo = 1;quickBankTypePage.findQuickBankType()"><i class="fa fa-search"></i>&nbsp;查 询</button>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <button type="button" class="btn btn-outline btn-default btn-add" onclick="quickBankTypePage.addQuickBankType()"><i class="fa fa-plus"></i>&nbsp;新增</button>
                                 </div>
                            </div>
                         </div>
                    </form>
                    </div>
                     <div class="jqGrid_wrapper">
                        <div class="ui-jqgrid ">
                            <div class="ui-jqgrid-view">
                                <div class="ui-jqgrid-hdiv">
                                    <div class="ui-jqgrid-hbox" style="padding-right:0">
                                        <table id="quickBankTypeTable" class="ui-jqgrid-htable ui-common-table table table-bordered">
                                            <thead>
                                                <tr class="ui-jqgrid-labels">
                                                    <th class="ui-th-column ui-th-ltr">序号</th>
                                                    <th class="ui-th-column ui-th-ltr">第三方支付名称</th>
                                                    <th class="ui-th-column ui-th-ltr">支付类型</th>
													<!--<th>商户类型</th>预留-->
													<th class="ui-th-column ui-th-ltr">银行类型</th>
													<th class="ui-th-column ui-th-ltr">码表</th>
													<th class="ui-th-column ui-th-ltr">手机端跳转方式</th>
													<th class="ui-th-column ui-th-ltr">启用状态</th>
													<th class="ui-th-column ui-th-ltr">操 作</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>     
		         </div>
		      </div>

    
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/quickbanktype/js/quickbanktype.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerQuickBankTypeAction.js'></script>
</body>
</html>

