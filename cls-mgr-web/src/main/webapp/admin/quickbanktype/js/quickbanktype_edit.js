function QuickBankTypeEditPage(){}
var quickBankTypeEditPage = new QuickBankTypeEditPage();

quickBankTypeEditPage.param = {
   	
};
/*var initFinsh = false;*/

$(document).ready(function() {
	quickBankTypeEditPage.initPage();
	var domainId = quickBankTypeEditPage.param.id;  //获取查询的ID
	if(domainId != null){
		quickBankTypeEditPage.editQuickBankType(domainId);
	}
	
	
	 
	 $("#bankType").change(function(){
		 if($(this).val()!="")
			 {
			 var text=$(this).find("option:selected").text();
			 $("#bankName").val(text);
			 }
	 })
});



/**
 * 保存第三方支付银行代码
 */
QuickBankTypeEditPage.prototype.saveData = function(){
	var recharge = {};
	recharge.id = $("#id").val();
	recharge.chargeType = $("#chargeType").val();
	recharge.bankType = $("#bankType").val();
	recharge.bankName = $("#bankName").val();
	recharge.payId = $("#payId").val();
	recharge.enabled = $("#enabled").val();
	recharge.mobileSkipType = $("#mobileSkipType").val();
	$(".selector").find("option:selected").text();
	recharge.chargeTypeDesc = $("#chargeType").find("option:selected").text();
	if (recharge.chargeType==null || recharge.chargeType.trim()=="") {
		swal("请选择第三方充值类型!");
		$("#chargeType").focus();
		return;
	}
	if (recharge.bankType==null || recharge.bankType.trim()=="") {
		swal("请选择银行类型!");
		$("#bankType").focus();
		return;
	}
	if (recharge.bankName==null || recharge.bankName.trim()=="") {
		swal("请输入支付类型!");
		$("#bankName").focus();
		return;
	}
	if (recharge.payId==null || recharge.payId.trim()=="") {
		swal("请输入码表!");
		$("#payId").focus();
		return;
	}
	
	if (recharge.enabled==null || recharge.enabled.trim()=="") {
		swal("请选择启用状态!");
		$("#enabled").focus();
		return;
	}
	if (recharge.mobileSkipType==null || recharge.mobileSkipType.trim()=="") {
		swal("请选择手机端跳转方式!");
		$("#mobileSkipType").focus();
		return;
	}
	managerQuickBankTypeAction.saveOrUpdateQuickBankType(recharge,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.quickBankTypePage.closeLayer(index);
		   	    }); 
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("保存银行码表数据失败");
			
		}
    });
};

/**
 * 初始化页面
 */
QuickBankTypeEditPage.prototype.initPage = function(id){
	$("#enabled").val("1");
};

/**
 * 修改第三方支付银行代码
 */
QuickBankTypeEditPage.prototype.editQuickBankType = function(id){
	managerQuickBankTypeAction.getQuickBankTypeById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			var quickBankType = r[1];
			$("#id").val(quickBankType.id);
			$("#chargeType").val(quickBankType.chargeType);
			$("#bankType").val(quickBankType.bankType);
			/*$("#payType").attr("disabled",true);*/
			$("#bankName").val(quickBankType.bankName);
			$("#payId").val(quickBankType.payId);
			$("#mobileSkipType").val(quickBankType.mobileSkipType);
			$("#enabled").val(quickBankType.enabled);
		}else if(r[0] != null && r[0] == "error"){
			showErrorDlg(jQuery(document.body), r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "获取充值配置失败.");
		}
    });
};