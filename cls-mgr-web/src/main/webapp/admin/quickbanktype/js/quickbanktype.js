function QuickBankTypePage(){}
var quickBankTypePage = new QuickBankTypePage();

quickBankTypePage.param = {
		
};
//分页参数
quickBankTypePage.pageParam = {
		    pageNo : 1,
		    pageSize : defaultPageSize
};
/**
 * 查询参数
 */
quickBankTypePage.queryParam = {
		bankType : null,
		chargeType : null
		
};
$(document).ready(function() {
	quickBankTypePage.initTableData();
});



QuickBankTypePage.prototype.initTableData = function(){

	quickBankTypePage.queryParam = getFormObj($("#quickBankTypeForm"));
	quickBankTypePage.table = $('#quickBankTypeTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			quickBankTypePage.pageParam.pageSize = data.length;//页面显示记录条数
			quickBankTypePage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerQuickBankTypeAction.getAllQuickBankType(quickBankTypePage.queryParam,quickBankTypePage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询奖金期号数据失败.");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": null},
		            {"data": "chargeTypeStr"},
		            {"data": "bankName"},
		            {"data": "bankType"},
		            {"data": "payId"},
		            {"data": "mobileSkipTypeStr"},
		            {
		            	"data": "enabled",
		            	render: function(data, type, row, meta) {
		            		if (data == "1") {
		            			return '是';
		            		} else {
		            			return '否';
		            		}
		            		return data;
	            		}
		            },
		            {
	                	 "data": "id",
	                	 render: function(data, type, row, meta) {
	                		 var str="<a class='btn btn-info btn-sm btn-bj' onclick='quickBankTypePage.editQuickBankType("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;" +
	             		 		"<a class='btn btn-danger btn-sm btn-del' onclick='quickBankTypePage.delQuickBankType("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";
	                		 if(row.enabled=="1")
	                			 {
	                			 str="<a class='btn btn-warning btn-sm ' onclick='quickBankTypePage.closeQuickBankType("+data+")'><i class='glyphicon glyphicon-remove' ></i>&nbsp;停用 </a>&nbsp;&nbsp;"+str;
	                			 }
	                		 else
	                			 {
	                			 str="<a class='btn btn-sm btn-success' onclick='quickBankTypePage.openQuickBankType("+data+")'><i class='glyphicon glyphicon-ok'></i>&nbsp;启用 </a>&nbsp;&nbsp;"+str;
	                			 }
	                			 
	                		 return str ;
	                	 }
	                }
	            ]
	}).api(); 
};
/**
 * 开启第三方支付银行代码
 */
QuickBankTypePage.prototype.openQuickBankType = function(id){
	
		managerQuickBankTypeAction.openQuickBankType(id,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "操作成功",type: "success"});
				quickBankTypePage.table.ajax.reload();
			}else if(r[0] != null && r[0] == "error"){
				showErrorDlg(jQuery(document.body), r[1]);
			}else{
				showErrorDlg(jQuery(document.body), "启用第三方支付银行代码失败.");
			}
	    });
	
};

/**
 * 关闭第三方支付银行代码置
 */
QuickBankTypePage.prototype.closeQuickBankType = function(id){

		managerQuickBankTypeAction.closeQuickBankType(id,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "操作成功",type: "success"});
				quickBankTypePage.table.ajax.reload();
			}else if(r[0] != null && r[0] == "error"){
				showErrorDlg(jQuery(document.body), r[1]);
			}else{
				showErrorDlg(jQuery(document.body), "关闭第三方支付银行代码失败.");
			}
	    });
	
};

/**
 * 查询数据
 */
QuickBankTypePage.prototype.findQuickBankType = function(){
	var chargeType = $("#chargeType").val();
	var bankType = $("#bankType").val();
	if(bankType == ""){
		quickBankTypePage.queryParam.bankType = null;
	}else{
		quickBankTypePage.queryParam.bankType = bankType;
	}
	if(chargeType == ""){
		quickBankTypePage.queryParam.chargeType = null;
	}else{
		quickBankTypePage.queryParam.chargeType = chargeType;
	}
	quickBankTypePage.table.ajax.reload();
};




/**
 * 删除
 */
QuickBankTypePage.prototype.delQuickBankType = function(id){
	
	   swal({
           title: "您确定要删除这条信息吗",
           text: "请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "删除",
           closeOnConfirm: false
       },
       function() {
    	   managerQuickBankTypeAction.delQuickBankType(id,function(r){
   			if (r[0] != null && r[0] == "ok") {
   				swal({ title: "提示", text: "删除成功",type: "success"});
   				quickBankTypePage.table.ajax.reload();
   			}else if(r[0] != null && r[0] == "error"){
   				swal(r[1]);
   			}else{
   				swal("删除信息失败.");
   			}
   	    });
       });
};

/**
 * 新增
 */
QuickBankTypePage.prototype.addQuickBankType=function()
{
	
	 layer.open({
         type: 2,
         title: '码表新增',
         maxmin: false,
         shadeClose: true,
         //点击遮罩关闭层
         area: ['70%', '50%'],
         content: contextPath + "/managerzaizst/quickbanktype/quickbanktype_edit.html",
         cancel: function(index){ 
      	   quickBankTypePage.table.ajax.reload();
      	   }
     });
}
/**
 * 编辑
 */
QuickBankTypePage.prototype.editQuickBankType=function(id)
{
	
	  layer.open({
           type: 2,
           title: '码表编辑',
           maxmin: false,
           shadeClose: true,
           //点击遮罩关闭层
           area: ['70%', '50%'],
           content: contextPath + "/managerzaizst/quickbanktype/"+id+"/quickbanktype_edit.html",
           cancel: function(index){ 
        	   quickBankTypePage.table.ajax.reload();
        	   }
       });
}

QuickBankTypePage.prototype.closeLayer=function(index){
	layer.close(index);
	quickBankTypePage.table.ajax.reload();
}
