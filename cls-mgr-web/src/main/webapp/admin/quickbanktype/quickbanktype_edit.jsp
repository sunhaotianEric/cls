<%@page import="com.team.lottery.system.SystemConfigConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" 
	import = "com.team.lottery.enums.EBankInfo" 
	pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>第三方支付银行码表设置</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    
    
    <link rel="stylesheet" href="<%=path%>/resources/kindeditor-4.1.10/themes/default/default.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" />
	<script charset="utf-8" src="<%=path%>/resources/kindeditor-4.1.10/kindeditor-min.js"></script>
	<script charset="utf-8" src="<%=path%>/resources/kindeditor-4.1.10/lang/zh_CN.js"></script>
	<%
	 String id = request.getParameter("id");  //获取查询的商品ID
	%>
	
</head>
<body class="gray-bg">
    <div class="wrapper wrapper-content animated fadeIn">
  <form action="javascript:void(0)">
    <div class="row">
    
    <div class="col-sm-12">
    <div class="input-group m-b">
    <span class="input-group-addon">第三方支付名称：</span>
         <input type="hidden" id="id"/>
	   	 <cls:thirdPaySel name="chargeType" id="chargeType" options="class:ipt form-control" />
    </div>
    </div>
    <div class="col-sm-12">
    <div class="input-group m-b">
    <span class="input-group-addon">支付类型：</span>
          <cls:enmuSel name="bankType" className="com.team.lottery.enums.EBankInfo" options="class:ipt form-control" id="bankType"/>
	   	  <input type="hidden" id="bankName" name="bankName"/>
        </div>
    </div>
    <div class="col-sm-12">
    <div class="input-group m-b">
    <span class="input-group-addon">码表：</span>
        <input type="text" value="" class="form-control" name="payId" id="payId">
        </div>
    </div>
    <div class="col-sm-12">
    <div class="input-group m-b">
    <span class="input-group-addon">手机端跳转方式：</span>
        <cls:enmuSel name="mobileSkipType" className="com.team.lottery.enums.EMobileSkipType" options="class:ipt form-control" id="mobileSkipType"/>
        </div>
    </div>
    <div class="col-sm-12">
    <div class="input-group m-b">
    <span class="input-group-addon">是否启用：</span>
        <select class="form-control" id="enabled">
	   				<option value="1">是</option>
	   				<option value="0">否</option>
	   			</select>
        </div>
    </div>
    <div class="col-sm-12">
     <div class="row">
     <div class="col-sm-6 text-center">
     <button type="button" class="btn btn-w-m btn-white" onclick="quickBankTypeEditPage.saveData()">提 交</button></div>
      <div class="col-sm-6 text-center">
      <input type="reset" class="btn btn-w-m btn-white" value="重 置"/>
        </div>
       </div>
      </div>
    </div>
    
    
   
      </form>
  </div>
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/quickbanktype/js/quickbanktype_edit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerQuickBankTypeAction.js'></script>
    <script type="text/javascript">
	quickBankTypeEditPage.param.id = <%=id %>;
		var imgServerUrl = '<%= SystemConfigConstant.imgServerUrl %>';
	</script>
</body>
</html>

