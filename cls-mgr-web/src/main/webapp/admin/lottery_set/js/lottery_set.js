function LotterySetPage(){}
var lotterySetPage = new LotterySetPage();

lotterySetPage.lotterySettingParam = {
};

$(document).ready(function() {
	lotterySetPage.getLotterySettingMsg();
});

/**
 * 保存彩种配置信息
 */
LotterySetPage.prototype.saveLotterySettingMsg = function(){
	
	lotterySetPage.lotterySettingParam.isSFKSOpen =   $('#sfks').val();
	lotterySetPage.lotterySettingParam.isWFKSOpen =   $('#wfks').val();
	lotterySetPage.lotterySettingParam.isSHFSSCOpen =   $('#shfssc').val();
	lotterySetPage.lotterySettingParam.isSFSSCOpen =   $('#sfssc').val();
	lotterySetPage.lotterySettingParam.isLCQSSCOpen =   $('#lcqssc').val();
	lotterySetPage.lotterySettingParam.isWFSSCOpen =   $('#wfssc').val();
	lotterySetPage.lotterySettingParam.isCQSSCOpen =    $('#cqssc').val();
	lotterySetPage.lotterySettingParam.isJXSSCOpen =   $('#jxssc').val();
	lotterySetPage.lotterySettingParam.isHLJSSCOpen =   $('#hljssc').val();
	lotterySetPage.lotterySettingParam.isXJSSCOpen =   $('#xjssc').val();
	lotterySetPage.lotterySettingParam.isJLFFCOpen =   $('#jlffc').val();
	lotterySetPage.lotterySettingParam.isHGFFCOpen =   $('#hgffc').val();
	lotterySetPage.lotterySettingParam.isTJSSCOpen =   $('#tjssc').val();
	
	// 三分11选5获取值
	lotterySetPage.lotterySettingParam.isSFSYXWOpen = $('#sfsyxw').val();
	// 五分11选5获取值
	lotterySetPage.lotterySettingParam.isWFSYXWOpen = $('#wfsyxw').val();
	lotterySetPage.lotterySettingParam.isGDSYXWOpen = $('#gdsyxw').val();
	lotterySetPage.lotterySettingParam.isSDSYXWOpen = $('#sdsyxw').val();
	lotterySetPage.lotterySettingParam.isJXSYXWOpen = $('#jxsyxw').val();
	lotterySetPage.lotterySettingParam.isCQSYXWOpen = $('#cqsyxw').val();
	lotterySetPage.lotterySettingParam.isFJSYXWOpen = $('#fjsyxw').val();

	lotterySetPage.lotterySettingParam.isJSKSOpen =   $('#jsks').val();
	lotterySetPage.lotterySettingParam.isAHKSOpen =   $('#ahks').val();
	lotterySetPage.lotterySettingParam.isJLKSOpen =   $('#jlks').val();
	lotterySetPage.lotterySettingParam.isHBKSOpen =   $('#hbks').val();
	lotterySetPage.lotterySettingParam.isBJKSOpen =   $('#bjks').val();
	lotterySetPage.lotterySettingParam.isJYKSOpen =   $('#jyks').val();
	lotterySetPage.lotterySettingParam.isGXKSOpen =   $('#gxks').val();
	lotterySetPage.lotterySettingParam.isGSKSOpen =   $('#gsks').val();
	lotterySetPage.lotterySettingParam.isSHKSOpen =   $('#shks').val();
	
	lotterySetPage.lotterySettingParam.isGDKLSFOpen =   $('#gdklsf').val();
	lotterySetPage.lotterySettingParam.isSHSSLDPCOpen = $('#shssldpc').val();
	lotterySetPage.lotterySettingParam.isFC3DDPCOpen = $('#fc3ddpc').val();
	lotterySetPage.lotterySettingParam.isXYFTOpen = $('#xyft').val();
	lotterySetPage.lotterySettingParam.isBJPK10Open = $('#bjpk10').val();
	lotterySetPage.lotterySettingParam.isJSPK10Open = $('#jspk10').val();
	lotterySetPage.lotterySettingParam.isSFPK10Open = $('#sfpk10').val();
	lotterySetPage.lotterySettingParam.isWFPK10Open = $('#wfpk10').val();
	lotterySetPage.lotterySettingParam.isSHFPK10Open = $('#shfpk10').val();
	
	lotterySetPage.lotterySettingParam.isJSTBOpen =   $('#jstb').val();
	lotterySetPage.lotterySettingParam.isAHTBOpen =   $('#ahtb').val();
	lotterySetPage.lotterySettingParam.isJLTBOpen =   $('#jltb').val();
	lotterySetPage.lotterySettingParam.isHBTBOpen =   $('#hbtb').val();
	lotterySetPage.lotterySettingParam.isBJTBOpen =   $('#bjtb').val();
	
	lotterySetPage.lotterySettingParam.isXJPLFCOpen =   $('#xjplfc').val();
	lotterySetPage.lotterySettingParam.isTWWFCOpen =   $('#twwfc').val();
	lotterySetPage.lotterySettingParam.isLHCOpen =   $('#lhc').val();
	lotterySetPage.lotterySettingParam.isXYEBOpen =   $('#xyeb').val();
	lotterySetPage.lotterySettingParam.isXYLHCOpen =   $('#xylhc').val();
	if(currentUser.bizSystem=='SUPER_SYSTEM'){
		managerSystemAction.updateLotterySetting(lotterySetPage.lotterySettingParam,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal("更新彩种信息数据成功");
			}else if (r[0] != null && r[0] == "error") {
				swal(r[1]);
			}else{
				swal( "查询数据失败.");
			}
	    });
	}else{
		managerBizSystemConfigAction.updateLotterySetting(lotterySetPage.lotterySettingParam,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal("更新彩种信息数据成功");
			}else if (r[0] != null && r[0] == "error") {
				swal(r[1]);
			}else{
				swal("查询数据失败.");
			}
	    });
	}
	
};
	
/**
 * 获取彩种配置信息
 */
LotterySetPage.prototype.getLotterySettingMsg = function(){
	if(currentUser.bizSystem=='SUPER_SYSTEM'){
		managerSystemAction.getLotterySetting(function(r){
			if (r[0] != null && r[0] == "ok") {
				lotterySetPage.toShowLotterySetMsg(r[1]);
			}else if (r[0] != null && r[0] == "error") {
				swal(r[1]);
			}else{
				swal("获取系统信息数据失败.");
			}
	    });
	}else{
		managerBizSystemConfigAction.getLotterySetting(function(r){
			if (r[0] != null && r[0] == "ok") {
				lotterySetPage.toShowLotterySetMsg(r[1]);
			}else if (r[0] != null && r[0] == "error") {
				swal(r[1]);
			}else{
				swal("获取系统信息数据失败.");
			}
	    });
	}
};


/**
 * 展示彩种配置信息
 */
LotterySetPage.prototype.toShowLotterySetMsg = function(lotterySetMsg){
	
	$("#sfks").find("option[value='"+lotterySetMsg.isSFKSOpen+"']").attr("selected",true);
	$("#wfks").find("option[value='"+lotterySetMsg.isWFKSOpen+"']").attr("selected",true);
	$("#shfssc").find("option[value='"+lotterySetMsg.isSHFSSCOpen+"']").attr("selected",true);
	$("#sfssc").find("option[value='"+lotterySetMsg.isSFSSCOpen+"']").attr("selected",true);
	$("#lcqssc").find("option[value='"+lotterySetMsg.isLCQSSCOpen+"']").attr("selected",true);
	$("#wfssc").find("option[value='"+lotterySetMsg.isWFSSCOpen+"']").attr("selected",true);
	$("#cqssc").find("option[value='"+lotterySetMsg.isCQSSCOpen+"']").attr("selected",true);
	$("#jxssc").find("option[value='"+lotterySetMsg.isJXSSCOpen+"']").attr("selected",true);
	$("#hljssc").find("option[value='"+lotterySetMsg.isHLJSSCOpen+"']").attr("selected",true);
	$("#tjssc").find("option[value='"+lotterySetMsg.isTJSSCOpen+"']").attr("selected",true);
	$("#xjssc").find("option[value='"+lotterySetMsg.isXJSSCOpen+"']").attr("selected",true);
	$("#jlffc").find("option[value='"+lotterySetMsg.isJLFFCOpen+"']").attr("selected",true);
	$("#hgffc").find("option[value='"+lotterySetMsg.isHGFFCOpen+"']").attr("selected",true);
	$("#jsks").find("option[value='"+lotterySetMsg.isJSKSOpen+"']").attr("selected",true);
	$("#ahks").find("option[value='"+lotterySetMsg.isAHKSOpen+"']").attr("selected",true);
	$("#jlks").find("option[value='"+lotterySetMsg.isJLKSOpen+"']").attr("selected",true);
	$("#hbks").find("option[value='"+lotterySetMsg.isHBKSOpen+"']").attr("selected",true);
	$("#bjks").find("option[value='"+lotterySetMsg.isBJKSOpen+"']").attr("selected",true);
	$("#jyks").find("option[value='"+lotterySetMsg.isJYKSOpen+"']").attr("selected",true);
	$("#gxks").find("option[value='"+lotterySetMsg.isGXKSOpen+"']").attr("selected",true);
	$("#gsks").find("option[value='"+lotterySetMsg.isGSKSOpen+"']").attr("selected",true);
	$("#shks").find("option[value='"+lotterySetMsg.isSHKSOpen+"']").attr("selected",true);
	$("#gdklsf").find("option[value='"+lotterySetMsg.isGDKLSFOpen+"']").attr("selected",true);
	// 三分11选5赋值
	$("#sfsyxw").find("option[value='"+lotterySetMsg.isSFSYXWOpen+"']").attr("selected",true);
	// 五分11选五彩种信息赋值
	$("#wfsyxw").find("option[value='"+lotterySetMsg.isWFSYXWOpen+"']").attr("selected",true);
	$("#gdsyxw").find("option[value='"+lotterySetMsg.isGDSYXWOpen+"']").attr("selected",true);
	$("#sdsyxw").find("option[value='"+lotterySetMsg.isSDSYXWOpen+"']").attr("selected",true);
	$("#jxsyxw").find("option[value='"+lotterySetMsg.isJXSYXWOpen+"']").attr("selected",true);
	$("#cqsyxw").find("option[value='"+lotterySetMsg.isCQSYXWOpen+"']").attr("selected",true);
	$("#fjsyxw").find("option[value='"+lotterySetMsg.isFJSYXWOpen+"']").attr("selected",true);
	$("#fc3ddpc").find("option[value='"+lotterySetMsg.isFC3DDPCOpen+"']").attr("selected",true);
	$("#shssldpc").find("option[value='"+lotterySetMsg.isSHSSLDPCOpen+"']").attr("selected",true);
	$("#xyft").find("option[value='"+lotterySetMsg.isXYFTOpen+"']").attr("selected",true);
	$("#bjpk10").find("option[value='"+lotterySetMsg.isBJPK10Open+"']").attr("selected",true);
	$("#jspk10").find("option[value='"+lotterySetMsg.isJSPK10Open+"']").attr("selected",true);
	$("#sfpk10").find("option[value='"+lotterySetMsg.isSFPK10Open+"']").attr("selected",true);
	$("#wfpk10").find("option[value='"+lotterySetMsg.isWFPK10Open+"']").attr("selected",true);
	$("#shfpk10").find("option[value='"+lotterySetMsg.isSHFPK10Open+"']").attr("selected",true);
	$("#jstb").find("option[value='"+lotterySetMsg.isJSTBOpen+"']").attr("selected",true);
	$("#ahtb").find("option[value='"+lotterySetMsg.isAHTBOpen+"']").attr("selected",true);
	$("#jltb").find("option[value='"+lotterySetMsg.isJLTBOpen+"']").attr("selected",true);
	$("#hbtb").find("option[value='"+lotterySetMsg.isHBTBOpen+"']").attr("selected",true);
	$("#bjtb").find("option[value='"+lotterySetMsg.isBJTBOpen+"']").attr("selected",true);
	$("#xjplfc").find("option[value='"+lotterySetMsg.isXJPLFCOpen+"']").attr("selected",true);
	$("#twwfc").find("option[value='"+lotterySetMsg.isTWWFCOpen+"']").attr("selected",true);
	$("#lhc").find("option[value='"+lotterySetMsg.isLHCOpen+"']").attr("selected",true);
	$('#xyeb').find("option[value='"+lotterySetMsg.isXYEBOpen+"']").attr("selected",true);
	$('#xylhc').find("option[value='"+lotterySetMsg.isXYLHCOpen+"']").attr("selected",true);
	
};