function LotterySetPage(){}
var lotterySetPage = new LotterySetPage();

lotterySetPage.lotterySettingParam = {
};

$(document).ready(function() {
	//lotterySetPage.getLotterySettingMsg();
});

/**
 * 保存彩种配置信息
 */
LotterySetPage.prototype.saveLotterySettingMsg = function(){
	
	lotterySetPage.lotterySettingParam.isSFKSOpen =   $('#sfks').val();
	lotterySetPage.lotterySettingParam.isWFKSOpen =   $('#wfks').val();
	lotterySetPage.lotterySettingParam.isSHFSSCOpen =   $('#shfssc').val();
	lotterySetPage.lotterySettingParam.isSFSSCOpen =   $('#sfssc').val();
	lotterySetPage.lotterySettingParam.isLCQSSCOpen =   $('#lcqssc').val();
	lotterySetPage.lotterySettingParam.isWFSSCOpen =   $('#wfssc').val();
	lotterySetPage.lotterySettingParam.isCQSSCOpen =   $('#cqssc').val();
	lotterySetPage.lotterySettingParam.isJXSSCOpen =   $('#jxssc').val();
	lotterySetPage.lotterySettingParam.isHLJSSCOpen =  $('#hljssc').val();
	lotterySetPage.lotterySettingParam.isXJSSCOpen =   $('#xjssc').val();
	lotterySetPage.lotterySettingParam.isJLFFCOpen =   $('#jlffc').val();
	lotterySetPage.lotterySettingParam.isHGFFCOpen =   $('#hgffc').val();
	lotterySetPage.lotterySettingParam.isTJSSCOpen =   $('#tjssc').val();
	
	lotterySetPage.lotterySettingParam.isGDSYXWOpen = $('#gdsyxw').val();
	lotterySetPage.lotterySettingParam.isSDSYXWOpen = $('#sdsyxw').val();
	lotterySetPage.lotterySettingParam.isJXSYXWOpen = $('#jxsyxw').val();
	lotterySetPage.lotterySettingParam.isCQSYXWOpen = $('#cqsyxw').val();
	lotterySetPage.lotterySettingParam.isFJSYXWOpen = $('#fjsyxw').val();
	// 业务彩种管理-三分11选5
	lotterySetPage.lotterySettingParam.isSFSYXWOpen = $('#sfsyxw').val();
	// 业务彩种管理-五分11选5
	lotterySetPage.lotterySettingParam.isWFSYXWOpen = $('#wfsyxw').val();

	lotterySetPage.lotterySettingParam.isJSKSOpen =   $('#jsks').val();
	lotterySetPage.lotterySettingParam.isAHKSOpen =   $('#ahks').val();
	lotterySetPage.lotterySettingParam.isJLKSOpen =   $('#jlks').val();
	lotterySetPage.lotterySettingParam.isHBKSOpen =   $('#hbks').val();
	lotterySetPage.lotterySettingParam.isBJKSOpen =   $('#bjks').val();
	lotterySetPage.lotterySettingParam.isJYKSOpen =   $('#jyks').val();
	lotterySetPage.lotterySettingParam.isGXKSOpen =   $('#gxks').val();
	lotterySetPage.lotterySettingParam.isGSKSOpen =   $('#gsks').val();
	lotterySetPage.lotterySettingParam.isSHKSOpen =   $('#shks').val();
	
	lotterySetPage.lotterySettingParam.isGDKLSFOpen =   $('#gdklsf').val();
	lotterySetPage.lotterySettingParam.isSHSSLDPCOpen = $('#shssldpc').val();
	lotterySetPage.lotterySettingParam.isFC3DDPCOpen = $('#fc3ddpc').val();
	lotterySetPage.lotterySettingParam.isXYFTOpen = $('#xyft').val();
	lotterySetPage.lotterySettingParam.isBJPK10Open = $('#bjpk10').val();
	lotterySetPage.lotterySettingParam.isJSPK10Open = $('#jspk10').val();
	lotterySetPage.lotterySettingParam.isSFPK10Open = $('#sfpk10').val();
	lotterySetPage.lotterySettingParam.isWFPK10Open = $('#wfpk10').val();
	lotterySetPage.lotterySettingParam.isSHFPK10Open = $('#shfpk10').val();
	
	lotterySetPage.lotterySettingParam.isJSTBOpen =   $('#jstb').val();
	lotterySetPage.lotterySettingParam.isAHTBOpen =   $('#ahtb').val();
	lotterySetPage.lotterySettingParam.isJLTBOpen =   $('#jltb').val();
	lotterySetPage.lotterySettingParam.isHBTBOpen =   $('#hbtb').val();
	lotterySetPage.lotterySettingParam.isBJTBOpen =   $('#bjtb').val();
	
	lotterySetPage.lotterySettingParam.isXJPLFCOpen =  $('#xjplfc').val();
	lotterySetPage.lotterySettingParam.isTWWFCOpen =  $('#twwfc').val();
	lotterySetPage.lotterySettingParam.isLHCOpen =   $('#lhc').val();
	lotterySetPage.lotterySettingParam.isXYEBOpen =  $('#xyeb').val();
	lotterySetPage.lotterySettingParam.isXYLHCOpen =  $('#xylhc').val();
	var bizSystem  = $("#bizSystem").val();
	if(bizSystem == null || bizSystem == ""){
    	swal("请选择业务系统！");
    	return ;
    }
	 managerBizSystemConfigAction.updateLotterySettingByBizSystem(lotterySetPage.lotterySettingParam,bizSystem,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal("更新彩种信息数据成功");
			}else if (r[0] != null && r[0] == "error") {
				swal(r[1]);
			}else{
				swal("查询数据失败.");
			}
	    });

	
};
	
/**
 * 获取彩种配置信息
 */
LotterySetPage.prototype.getLotterySettingMsg = function(){
        var bizSystem  = $("#bizSystem").val();
        if(bizSystem == null || bizSystem == ""){
        	swal("请选择业务系统！");
        	return ;
        }
		managerBizSystemConfigAction.getLotterySettingByBizSystem(bizSystem,function(r){
			if (r[0] != null && r[0] == "ok") {
				lotterySetPage.toShowLotterySetMsg(r[1]);
			}else if (r[0] != null && r[0] == "error") {
				swal(r[1]);
			}else{
				swal("获取系统信息数据失败.");
			}
	    });

};


/**
 * 展示彩种配置信息
 */
LotterySetPage.prototype.toShowLotterySetMsg = function(lotterySetMsg){
	
	$("#sfks").val(lotterySetMsg.isSFKSOpen);
	$("#wfks").val(lotterySetMsg.isWFKSOpen);
	$("#shfssc").val(lotterySetMsg.isSHFSSCOpen);
	$("#sfssc").val(lotterySetMsg.isSFSSCOpen);
	$("#lcqssc").val(lotterySetMsg.isLCQSSCOpen);
	$("#wfssc").val(lotterySetMsg.isWFSSCOpen);
	$("#cqssc").val(lotterySetMsg.isCQSSCOpen);
	$("#jxssc").val(lotterySetMsg.isJXSSCOpen);
	$("#hljssc").val(lotterySetMsg.isHLJSSCOpen);
	$("#tjssc").val(lotterySetMsg.isTJSSCOpen);
	$("#xjssc").val(lotterySetMsg.isXJSSCOpen);
	$("#jlffc").val(lotterySetMsg.isJLFFCOpen);
	$("#hgffc").val(lotterySetMsg.isHGFFCOpen);
	$("#jsks").val(lotterySetMsg.isJSKSOpen);
	$("#ahks").val(lotterySetMsg.isAHKSOpen);
	$("#jlks").val(lotterySetMsg.isJLKSOpen);
	$("#hbks").val(lotterySetMsg.isHBKSOpen);
	$("#bjks").val(lotterySetMsg.isBJKSOpen);
	$("#jyks").val(lotterySetMsg.isJYKSOpen);
	$("#gxks").val(lotterySetMsg.isGXKSOpen);
	$("#gsks").val(lotterySetMsg.isGSKSOpen);
	$("#shks").val(lotterySetMsg.isSHKSOpen);
	$("#gdklsf").val(lotterySetMsg.isGDKLSFOpen);
	$("#gdsyxw").val(lotterySetMsg.isGDSYXWOpen);
	$("#sdsyxw").val(lotterySetMsg.isSDSYXWOpen);
	$("#jxsyxw").val(lotterySetMsg.isJXSYXWOpen);
	$("#cqsyxw").val(lotterySetMsg.isCQSYXWOpen);
	$("#fjsyxw").val(lotterySetMsg.isFJSYXWOpen);
	$("#wfsyxw").val(lotterySetMsg.isWFSYXWOpen);
	$("#sfsyxw").val(lotterySetMsg.isSFSYXWOpen);
	$("#fc3ddpc").val(lotterySetMsg.isFC3DDPCOpen);
	$("#shssldpc").val(lotterySetMsg.isSHSSLDPCOpen);
	$("#xyft").val(lotterySetMsg.isXYFTOpen);
	$("#bjpk10").val(lotterySetMsg.isBJPK10Open);
	$("#jspk10").val(lotterySetMsg.isJSPK10Open);
	$("#sfpk10").val(lotterySetMsg.isSFPK10Open);
	$("#wfpk10").val(lotterySetMsg.isWFPK10Open);
	$("#shfpk10").val(lotterySetMsg.isSHFPK10Open);
	$("#jstb").val(lotterySetMsg.isJSTBOpen);
	$("#ahtb").val(lotterySetMsg.isAHTBOpen);
	$("#jltb").val(lotterySetMsg.isJLTBOpen);
	$("#hbtb").val(lotterySetMsg.isHBTBOpen);
	$("#bjtb").val(lotterySetMsg.isBJTBOpen);
	$("#xjplfc").val(lotterySetMsg.isXJPLFCOpen);
	$("#twwfc").val(lotterySetMsg.isTWWFCOpen);
	$("#lhc").val(lotterySetMsg.isLHCOpen);
	$('#xyeb').val(lotterySetMsg.isXYEBOpen);
	$('#xylhc').val(lotterySetMsg.isXYLHCOpen);
	
	
};