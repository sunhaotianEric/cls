<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>  
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <title>彩种开关设置</title>
        <jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
 <body class="gray-bg">
         <div class="wrapper wrapper-content animated fadeIn">
 		<div class="row">
			 <div class="col-sm-3">
	              <div class="input-group m-b">
	                  <span class="input-group-addon">商户</span>
	                   <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control,onChange:lotterySetPage.getLotterySettingMsg()"/>
	              </div>
			 </div>
             <div class="col-sm-2">
                <div class="form-group nomargin text-left input-group-btn">
                    <button type="button" class="btn btn-outline btn-default btn-kj btn-white" onclick="lotterySetPage.getLotterySettingMsg()"><i class="fa fa-search"></i>&nbsp;搜索</button>
                </div>
             </div>
              <div class="col-sm-7">
              </div>
		</div>
            <div class="tabs-container ibox">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a data-toggle="tab" href="#tab-1" aria-expanded="true">时时彩设置</a></li>
                    <li class="">
                        <a data-toggle="tab" href="#tab-3" aria-expanded="false">分分彩设置</a></li>
                    <li class="">
                        <a data-toggle="tab" href="#tab-2" aria-expanded="false">11选5</a></li>
                    <li class="">
                        <a data-toggle="tab" href="#tab-4" aria-expanded="false">快3设置</a></li>
                    <li class="">
                        <a data-toggle="tab" href="#tab-5" aria-expanded="false">骰宝</a></li>
                    <li class="">
                        <a data-toggle="tab" href="#tab-6" aria-expanded="false">低频彩</a></li>
                    <li class="">
                        <a data-toggle="tab" href="#tab-7" aria-expanded="false">快乐十分设置</a></li>
                    <li class="">
                        <a data-toggle="tab" href="#tab-8" aria-expanded="false">PK10</a></li>
                    <li class="">
                        <a data-toggle="tab" href="#tab-9" aria-expanded="false">幸运28</a></li>
                </ul>
                <div class="tab-content">
                    <div id="tab-1" class="tab-pane active">
                        <div class="panel-body">
                            <div class="col-sm-4">
                                <div class="input-group m-b">
                                            <span class="input-group-addon">重庆时时彩</span>
                                            <select class="ipt form-control" id='cqssc'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">江西时时彩</span>
                                            <select class="ipt form-control" id='jxssc'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">黑龙江时时彩</span>
                                            <select class="ipt form-control" id='hljssc'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">新疆时时彩</span>
                                            <select class="ipt form-control" id='xjssc'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">天津时时彩</span>
                                            <select class="ipt form-control" id='tjssc'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">三分时时彩</span>
                                            <select class="ipt form-control" id='sfssc'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">五分时时彩</span>
                                            <select class="ipt form-control" id='wfssc'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">十分时时彩</span>
                                            <select class="ipt form-control" id='shfssc'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                         <div class="input-group m-b">
                                            <span class="input-group-addon">老重庆时时彩</span>
                                            <select class="ipt form-control" id='lcqssc'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">江苏时时彩</span>
                                    <select class="ipt form-control" id='jsssc'>
                                        <option value="1" selected="selected">开启</option>
                                        <option value="0">关闭</option></select>
                                </div>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">北京时时彩</span>
                                    <select class="ipt form-control" id='bjssc'>
                                        <option value="1" selected="selected">开启</option>
                                        <option value="0">关闭</option></select>
                                </div>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">广东时时彩</span>
                                    <select class="ipt form-control" id='gdssc'>
                                        <option value="1" selected="selected">开启</option>
                                        <option value="0">关闭</option></select>
                                </div>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">四川时时彩</span>
                                    <select class="ipt form-control" id='scssc'>
                                        <option value="1" selected="selected">开启</option>
                                        <option value="0">关闭</option></select>
                                </div>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">上海时时彩</span>
                                    <select class="ipt form-control" id='shssc'>
                                        <option value="1" selected="selected">开启</option>
                                        <option value="0">关闭</option></select>
                                </div>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">山东时时彩</span>
                                    <select class="ipt form-control" id='sdssc'>
                                        <option value="1" selected="selected">开启</option>
                                        <option value="0">关闭</option></select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="tab-2" class="tab-pane">
                        <div class="panel-body">
                            <div class="col-sm-4">
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">广东11选5</span>
                                            <select class="ipt form-control" id='gdsyxw'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">山东11选5</span>
                                            <select class="ipt form-control" id='sdsyxw'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">江西11选5</span>
                                            <select class="ipt form-control" id='jxsyxw'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">重庆11选5</span>
                                            <select class="ipt form-control" id='cqsyxw'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">福建11选5</span>
                                            <select class="ipt form-control" id='fjsyxw'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">三分11选5</span>
                                            <select class="ipt form-control" id='sfsyxw'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">五分11选5</span>
                                            <select class="ipt form-control" id='wfsyxw'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                    </div>
                        </div>
                    </div>
                    <div id="tab-3" class="tab-pane">
                        <div class="panel-body">
                            <div class="col-sm-4">
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">幸运分分彩</span>
                                            <select class="ipt form-control" id='jlffc'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">韩国1.5分彩</span>
                                            <select class="ipt form-control" id='hgffc'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">新加坡2分彩</span>
                                            <select class="ipt form-control" id='xjplfc'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">台湾5分彩</span>
                                            <select class="ipt form-control" id='twwfc'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                    </div>
                        </div>
                    </div>
                    <div id="tab-4" class="tab-pane">
                        <div class="panel-body">
                            <div class="col-sm-4">
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">江苏快3</span>
                                            <select class="ipt form-control" id='jsks'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">安徽快3</span>
                                            <select class="ipt form-control" id='ahks'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">吉林快3</span>
                                            <select class="ipt form-control" id='jlks'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">湖北快3</span>
                                            <select class="ipt form-control" id='hbks'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">北京快3</span>
                                            <select class="ipt form-control" id='bjks'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">幸运快3</span>
                                            <select class="ipt form-control" id='jyks'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                         <div class="input-group m-b">
                                            <span class="input-group-addon">广西快3</span>
                                            <select class="ipt form-control" id='gxks'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">甘肃快3</span>
                                            <select class="ipt form-control" id='gsks'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">上海快3</span>
                                            <select class="ipt form-control" id='shks'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">三分快3</span>
                                            <select class="ipt form-control" id='sfks'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">五分快3</span>
                                            <select class="ipt form-control" id='wfks'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                    </div>
                        </div>
                    </div>
                    <div id="tab-5" class="tab-pane">
                        <div class="panel-body">
                            <div class="col-sm-4">
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">江苏骰宝</span>
                                            <select class="ipt form-control" id='jstb'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">安徽骰宝</span>
                                            <select class="ipt form-control" id='ahtb'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">吉林骰宝</span>
                                            <select class="ipt form-control" id='jltb'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">湖北骰宝</span>
                                            <select class="ipt form-control" id='hbtb'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">北京骰宝</span>
                                            <select class="ipt form-control" id='bjtb'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                    </div>
                        </div>
                    </div>
                    <div id="tab-6" class="tab-pane">
                        <div class="panel-body">
                            <div class="col-sm-4">
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">上海时时乐</span>
                                            <select class="ipt form-control" id='shssldpc'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">福彩3D</span>
                                            <select class="ipt form-control" id='fc3ddpc'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                          <div class="input-group m-b">
                                            <span class="input-group-addon">六合彩</span>
                                            <select class="ipt form-control" id='lhc'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                    </div>
                        </div>
                    </div>
                    <div id="tab-7" class="tab-pane">
                        <div class="panel-body">
                            <div class="col-sm-4">
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">广东快乐十分</span>
                                            <select class="ipt form-control" id='gdklsf'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                    </div>
                        </div>
                    </div>
                    <div id="tab-8" class="tab-pane">
                        <div class="panel-body">
                           <div class="col-sm-4">
                               <div class="input-group m-b">
                                   <span class="input-group-addon">北京PK10</span>
                                   <select class="ipt form-control" id='bjpk10'>
                                       <option value="1" selected="selected">开启</option>
                                       <option value="0">关闭</option></select>
                               </div>
                               <div class="input-group m-b">
                                   <span class="input-group-addon">极速PK10</span>
                                   <select class="ipt form-control" id='jspk10'>
                                       <option value="1" selected="selected">开启</option>
                                       <option value="0">关闭</option></select>
                               </div>
                               <div class="input-group m-b">
                                   <span class="input-group-addon">幸运飞艇</span>
                                   <select class="ipt form-control" id='xyft'>
                                       <option value="1" selected="selected">开启</option>
                                       <option value="0">关闭</option></select>
                               </div>
                               <div class="input-group m-b">
                                   <span class="input-group-addon">三分PK10</span>
                                   <select class="ipt form-control" id='sfpk10'>
                                       <option value="1" selected="selected">开启</option>
                                       <option value="0">关闭</option></select>
                               </div>
                               <div class="input-group m-b">
                                   <span class="input-group-addon">五分PK10</span>
                                   <select class="ipt form-control" id='wfpk10'>
                                       <option value="1" selected="selected">开启</option>
                                       <option value="0">关闭</option></select>
                               </div>
                               <div class="input-group m-b">
                                   <span class="input-group-addon">十分PK10</span>
                                   <select class="ipt form-control" id='shfpk10'>
                                       <option value="1" selected="selected">开启</option>
                                       <option value="0">关闭</option></select>
                               </div>
                           </div>
                        </div>
                   </div>
                   <div id="tab-9" class="tab-pane">
                        <div class="panel-body">
                                    <div class="col-sm-4">
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">幸运28</span>
                                            <select class="ipt form-control" id='xyeb'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">幸运六合彩</span>
                                            <select class="ipt form-control" id='xylhc'>
                                                <option value="1" selected="selected">开启</option>
                                                <option value="0">关闭</option></select>
                                        </div>
                                    </div>
                        </div>
                   </div>
                </div>
            </div>
            <div class="col-sm-12 ibox">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <button type="button" class="btn btn-w-m btn-white" onclick="lotterySetPage.saveLotterySettingMsg()">保 存</button></div>
                </div>
            </div>
        </div>
<!-- js -->
<script type="text/javascript" src="<%=path%>/admin/lottery_set/js/lottery_set_bizsystem.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerSystemAction.js'></script>
<script type='text/javascript' src='<%=path%>/dwr/interface/managerBizSystemConfigAction.js'></script>
</body>
</html>

