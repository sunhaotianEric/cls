function ChatLogPage(){}
var chatLogPage = new ChatLogPage();

chatLogPage.queryParam={
		bizSystem:null,
		roomId:null,
		userName:null,
		sendType:null,
		nick:null
};

//分页参数
chatLogPage.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize
};

$(document).ready(function() {
	chatLogPage.initTableData();
});

ChatLogPage.prototype.initTableData=function(){
	chatLogPage.table = $('#chatLogTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"scrollX": true,
		"iDisplayLength" : defaultPageSize,
		"ajax":function (data, callback, settings) {
		chatLogPage.pageParam.pageSize = data.length;//页面显示记录条数
		chatLogPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerChatLogAction.getChatLogPage(chatLogPage.queryParam,chatLogPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": "bizSystemName","bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "roomId"},
		            {"data": "nick"},
		            {"data": "userName" },
		            {
		            	"data": "sendType",
		            	render:function(data,type,row,meta){
		            		var str="";
		            		if(data=="ALL"){
		            			str="群聊";
		            		}else if(data=="PERSONAL"){
		            			str="私聊";
		            		}
		            		return str;
		            	}
		            },
		            {
		            	"data": "msg",
		            	render:function(data,type,row,meta){
		            		var str="";
		            		if(data.length>30){
		            			str="<span title='"+data+"'>"+data.substring(1,30)+"...</span>";
		            		}else{
		            			str=data;
		            		}
		            		return str;
		            	}
		            },
		            {"data": "ipAddress" },
		            {"data": "sendTimeStr" },
		            {
	                	 "data": "id",
	                	 render: function(data, type, row, meta) {
	                		 var str="";
	                		 str +="<a class='btn btn-danger btn-sm btn-del' onclick='chatLogPage.delChatLog("+data+")'><i class='fa fa-trash'></i>&nbsp;删除 </a>&nbsp;";
	                		 return str;
	                	 }
	                }
	            ]
	}).api(); 
}

ChatLogPage.prototype.findChatLog=function(){
	var bizSystem=$("#bizSystem").val();
	var roomId=$("#roomId").val();
	var nick=$("#nick").val();
	var userName=$("#userName").val();
	var sendType=$("#sendType").val();
	if(bizSystem!=null||bizSystem!=''){
		chatLogPage.queryParam.bizSystem=bizSystem;
	}else{
		chatLogPage.queryParam.bizSystem=null;
	}
	if(roomId!=null||roomId!=''){
		chatLogPage.queryParam.roomId=roomId;
	}else{
		chatLogPage.queryParam.roomId=null;
	}
	if(userName!=null||userName!=''){
		chatLogPage.queryParam.userName=userName;
	}else{
		chatLogPage.queryParam.userName=null;
	}
	if(sendType!=null||sendType!=''){
		chatLogPage.queryParam.sendType=sendType;
	}else{
		chatLogPage.queryParam.sendType=null;
	}
	if(nick!=null||nick!=''){
		chatLogPage.queryParam.nick=nick;
	}else{
		chatLogPage.queryParam.nick=null;
	}
	chatLogPage.table.ajax.reload();
}

ChatLogPage.prototype.delChatLog=function(id){
	swal({
        title: "您确定要删除该聊天记录吗",
        text: "请谨慎操作！",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        cancelButtonText: "取消",
        confirmButtonText: "确定",
        closeOnConfirm: false
    },
    function() {
    	managerChatLogAction.delChatLogById(id,function(r){
	    		if (r[0] != null && r[0] == "ok") {
	    			swal("删除成功！");
	    			chatLogPage.findChatLog();
	    		}else if(r[0] != null && r[0] == "error"){
	    			swal(r[1]);
	    		}else{
	    			swal("查询数据失败.");
	    		}
	        });  
     });
}