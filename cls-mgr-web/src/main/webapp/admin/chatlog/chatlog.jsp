<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>聊天房间管理</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
<body>
<div class="animated fadeIn">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox ">
                <div class="ibox-content">
                    <form class="form-horizontal" id="activityQuery">
                        <div class="row">
                            <div class="col-sm-4 biz" >
                              	<c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">商户：</span>
                                     <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
                                </div>
                                </c:if>
                            </div>
                            <div class="col-sm-4 biz" >
                                <div class="input-group m-b">
                                    <span class="input-group-addon">聊天室：</span>
                                     <input type="text" name="roomId" id="roomId" class="form-control" >
                                </div>
                            </div>
                            <div class="col-sm-4 biz" >
                                <div class="input-group m-b">
                                    <span class="input-group-addon">用户名：</span>
                                     <input type="text" name="userName" id="userName" class="form-control" >
                                </div>
                            </div>
                            <div class="col-sm-4 biz" >
                                <div class="input-group m-b">
                                    <span class="input-group-addon">昵称：</span>
                                     <input type="text" name="nick" id="nick" class="form-control" >
                                </div>
                            </div>
                            <div class="col-sm-4 biz" >
                                <div class="input-group m-b">
                                    <span class="input-group-addon">发送类型：</span>
                                     <select class="ipt form-control" id="sendType" name="sendType">
                                        <option value="" selected="selected">==请选择==</option>
                                        <option value="ALL">群聊</option>
	   			                        <option value="PERSONAL">私聊</option>
	   			                     </select>
                                </div>
                            </div>
                            
                            <div class="col-sm-4">
                                <div class="form-group nomargin text-right">
                                    <button type="button" class="btn btn-outline btn-default btn-kj biz" onclick="chatLogPage.findChatLog()"><i class="fa fa-search"></i>&nbsp;查 询</button>
                                </div>
                            </div>
                          <!--   <div class="col-sm-8">
                                <div class="form-group nomargin text-right">
                                </div>
                            </div> -->
                        </div>
                    </form>
                    <div class="jqGrid_wrapper">
                        <div class="ui-jqgrid ">
                            <div class="ui-jqgrid-view">
                                <div class="ui-jqgrid-hdiv">
                                    <div class="ui-jqgrid-hbox" style="padding-right:0">
                                        <table class="ui-jqgrid-htable ui-common-table table table-bordered" id="chatLogTable">
                                            <thead>
                                                <tr class="ui-jqgrid-labels">
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">商户</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                    	<div class="ui-jqgrid-sortable">聊天室ID</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">昵称</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">用户名</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">发送类型</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">消息内容</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">发送地址</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">发送时间</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">操作</div></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 <!-- js -->
 <script type="text/javascript" src="<%=path%>/admin/chatlog/js/chatlog.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
 <!-- dwr -->
 <script type='text/javascript' src='<%=path%>/dwr/interface/managerChatLogAction.js'></script>
</body>
</html>

