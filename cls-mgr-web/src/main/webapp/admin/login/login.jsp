<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
 import="com.team.lottery.util.RSAUtil,java.security.KeyPair,java.security.interfaces.RSAPublicKey"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
/*     KeyPair keyPair = RSAUtil.generateKeyPair();
    RSAPublicKey rsap = (RSAPublicKey)keyPair.getPublic();
    session.setAttribute("rspr", keyPair.getPrivate());
    String m = rsap.getModulus().toString(16);
    String e = rsap.getPublicExponent().toString(16); */
%>
<html>
    
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>登录</title>
        <link rel="shortcut icon" href="favicon.ico">
        <script type="text/javascript" src="<%=path %>/resources/rsa/Barrett.js" ></script>
        <script type="text/javascript" src="<%=path %>/resources/rsa/BigInt.js" ></script>
        <script type="text/javascript" src="<%=path %>/resources/rsa/RSA.js" ></script>
        <link href="<%=path%>/css/plugins/sweetalert/sweetalert.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" rel="stylesheet">
        <link href="<%=path%>/css/bootstrap.min14ed.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>?v=3.3.6" rel="stylesheet">
        <link href="<%=path%>/css/font-awesome.min93e3.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>?v=4.4.0" rel="stylesheet">
        <link href="<%=path%>/css/animate.min.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" rel="stylesheet">
        <link href="<%=path%>/css/style.min862f.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>?v=4.1.0" rel="stylesheet">
        <script>
   <%--     var m = '<%=m %>';
       var e = '<%=e %>'; --%>
        var contextPath='<%=path%>';
           function checkCodeRefresh() {
	    	$('#codeImg').attr('src', contextPath + "/imageServlet?time="+Math.random());
	    	//document.getElementById("codeImg").src = "imageServlet?time="+Math.random();
	       }
        	//当前窗口是否被其他顶层窗口嵌套
        	if (window.top !== window.self) {
               	window.top.location = window.location;
           	}
        </script>
        <style type="text/css">.login-web{background-color: rgba(255, 255, 255, 0.38);width: 400px;height:400px;padding: 30px;top:50%;margin-top:-200px;left:50%;margin-left:-200px;position: absolute;-webkit-animation-name: shineRed; -webkit-animation-duration: 3s; -webkit-animation-iteration-count: infinite; } @-webkit-keyframes shineRed { from { -webkit-box-shadow: 0 0 10px #333; } 50% { -webkit-box-shadow: 0 0 20px #92957A; } to { -webkit-box-shadow: 0 0 10px #333; } }</style>
    </head>
    
    <body class="gray-bg" style="background-image: url(<%=path%>/img/bg.jpg);background-size:100% 100%">
        <div class="text-center loginscreen  animated fadeInDown login-web">
            <div>
                <h3 class="logo-name" style="margin-top: 0%;">后台管理商户</h3>
            </div>
            <form style="margin-top: 15%;" role="form">
                <div class="form-group">
                    <input type="text" id="userName"  class="form-control" placeholder="账号" >
                </div>
                <div class="form-group">
                    <input type="password" id="userPassword" class="form-control" placeholder="密码" >
                </div>
                <div class="form-group" style="overflow:hidden;">
                    <!-- <div class="col-xs-12" style="padding-left:0"> -->
                        <input type="password" id="secretCode" class="form-control" placeholder="秘密验证码" >
                   <!--  </div> -->
               <!--      <div class="col-xs-6" style="padding-left:0">
                        <input type="button" id="secretSendButton" value="点击发送 " class="btn btn-sm btn-white" style="margin-top:3px">
                    </div> -->
                </div>
                <div class="form-group" style="overflow:hidden;">
                    <div class="col-xs-6" style="padding-left:0">
                        <input type="text" id="code" class="form-control" placeholder="图片验证码" >
                    </div>
                    <div class="col-xs-3" style="padding-left:0;line-height: 34px;">
                        <img id="codeImg" src="<%=path %>/imageServlet" onclick="checkCodeRefresh()" />
                    </div>
                    <div class="col-xs-3" style="padding-left:0;line-height: 34px;">
                        <a onclick="checkCodeRefresh()">换一张</a>
                    </div>
                </div>
          
                <button type="button" id="loginButton" class="btn btn-primary block full-width m-b">登 录</button>
            </form>
        </div>
        
        <script src="<%=path%>/js/jquery.min.js?v=2.1.4"></script>
        <script src="<%=path%>/js/bootstrap.min.js?v=3.3.6"></script>
        <script src="<%=path%>/js/plugins/sweetalert/sweetalert.min.js"></script>
        <!-- DWR -->
		<script type='text/javascript' src='<%=path%>/dwr/engine.js'></script>
		<script type='text/javascript' src='<%=path%>/dwr/util.js'></script>
		
		<!-- js -->
		<script type="text/javascript" src="<%=path%>/admin/login/js/login.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
		<!-- dwr -->
		<script type='text/javascript' src='<%=path%>/dwr/interface/managerAdminAction.js'></script>
    </body>

</html>