function EditPasswordPage(){}
var editPasswordPage = new EditPasswordPage();

editPasswordPage.param = {
		
};



$(document).ready(function() {
	editPasswordPage.initPage();
	
	$("#editPassBtn").click(function(){
		editPasswordPage.updatePassword();
	});
});

/**
 * 初始化
 */
EditPasswordPage.prototype.initPage = function(){
	managerAdminAction.getCurrentAdmin(function(r){
		if (r[0] != null && r[0] == "ok") {
			$("#username").val(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("操作失败.");
		}
    });
};

/**
 * 修改密码
 */
EditPasswordPage.prototype.updatePassword = function(){
	var admin = {};
	admin.username = $("#username").val();
	admin.password = $("#passwordold").val();
	admin.newpassword = $("#passwordnew").val();
	admin.surepassword = $("#confirmPassword").val();
	
	if (admin.password==null || admin.password.trim()=="") {
		swal("请输入旧密码!");
		$("#passwordold").focus();
		return;
	}
	if (admin.newpassword==null || admin.newpassword.trim()=="") {
		swal("请输入新密码!");
		$("#passwordnew").focus();
		return;
	}
	if (admin.surepassword==null || admin.surepassword.trim()=="") {
		swal("请输入确认新密码!");
		$("#confirmPassword").focus();
		return;
	}
	if (admin.surepassword != admin.surepassword) {
		swal("新密码和确认新密码不一致!");
		$("#passwordnew").focus();
		return;
	}
	
	managerAdminAction.updatePassword(admin,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal(r[1]);
			$("#passwordold").val("");
			$("#passwordnew").val("");
			$("#confirmPassword").val("");
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("操作失败.");
		}
    });
};