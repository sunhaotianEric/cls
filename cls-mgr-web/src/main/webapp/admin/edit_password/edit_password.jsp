
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<html>
<head>
    <meta charset="utf-8" />
    <title>后台管理商户</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/edit_password/js/edit_password.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerAdminAction.js'></script>
</head>
<body>
   <div class="alert alert-info">当前位置<b class="tip"></b>后台管理<b class="tip"></b>修改密码</div>
     
     <table class="tb">
      <tr>
         <td width="15px">管理员账号：</td>
         <td>
	         <input type="text" id="username" readonly="readonly"/>
         </td>
      </tr>
      <tr>
         <td>旧密码：</td>
         <td><input id="passwordold" type="password"/></td>
      </tr>
      <tr>
         <td>新密码：</td>
         <td><input id="passwordnew" type="password"/></td>
      </tr>
      <tr>
         <td>确认新密码：</td>
         <td><input id="confirmPassword" type="password"/></td>
      </tr>
      <tr>
         <td></td>
         <td><input type="button" value="提交" id="editPassBtn"/></td>
      </tr>
     </table>
</body>
</html>

