<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>新手活动任务管理</title>
<jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
<body>
	<div class="animated fadeIn">
		<div class="ibox-content">
			<div class="row">
				<form class="form-horizontal" id="newUserGiftTaskForm">
					<div class="row">
						<c:choose>
							<c:when test="${admin.bizSystem=='SUPER_SYSTEM'}">
								<div class="col-sm-3">
									<div class="input-group m-b">
										<span class="input-group-addon">商户</span>
										<cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control" />
									</div>
								</div>
								<div class="col-sm-2">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">用户名</span>
                                    <input type="text" id='userName' value="" class="form-control"></div>
                                </div>
                                <div class="col-sm-4">
							        <div class="input-group m-b">
							            <span class="input-group-addon">时间筛选</span>
							            <div class="input-daterange input-group" id="datepicker">
							                <input class="form-control layer-date" placeholder="开始日期" onClick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" id="startime">
							                <span class="input-group-addon">到</span>
							                <input class="form-control layer-date" placeholder="结束日期"onClick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" id="endtime">
							            </div>
							        </div>
								</div>
								<div class="col-sm-3">
									<div class="form-group  text-right">
										<button type="button" class="btn btn-outline btn-default btn-kj"
											onclick="newUserGiftTaskPage.findNewUserGiftTask()">
											<i class="fa fa-search"></i>&nbsp;查 询
										</button>
									</div>
								</div>
							</div>
							</c:when>
							<c:otherwise>
								<div class="col-sm-2">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">用户名</span>
                                    <input type="text" id='userName' value="" class="form-control"></div>
                                </div>
                                 <div class="col-sm-4">
							        <div class="input-group m-b">
							           	<span class="input-group-addon">时间筛选</span>
							            <div class="input-daterange input-group" id="datepicker">
							                <input class="form-control layer-date" placeholder="开始日期" onClick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" id="startime">
							                <span class="input-group-addon">到</span>
							                <input class="form-control layer-date" placeholder="结束日期"onClick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" id="endtime">
							            </div>
							        </div>
								</div>
								<div class="col-sm-6">
								<div class="form-group  text-right">
									<button type="button" class="btn btn-outline btn-default btn-kj"
										onclick="newUserGiftTaskPage.findNewUserGiftTask()">
										<i class="fa fa-search"></i>&nbsp;查 询
									</button>
								</div>
								</div>
							</c:otherwise>
						</c:choose>
					</div>
				</form>
			
			<div class="jqGrid_wrapper">
				<div class="ui-jqgrid ">
					<div class="ui-jqgrid-view ">
						<div class="ui-jqgrid-hdiv">
							<div class="ui-jqgrid-hbox" style="padding-right: 0">
								<table id="newUserGiftTaskTable"
									class="ui-jqgrid-htable ui-common-table table table-bordered">
									<thead>
										<tr class="ui-jqgrid-labels">
											<th class="ui-th-column ui-th-ltr">序号</th>
											<th class="ui-th-column ui-th-ltr">商户</th>
											<th class="ui-th-column ui-th-ltr">用户名</th>
											<th class="ui-th-column ui-th-ltr">是否完成APP下载任务</th>
											<th class="ui-th-column ui-th-ltr">领取金额</th>
											<th class="ui-th-column ui-th-ltr">是否完成完成个人信息任务</th>
											<th class="ui-th-column ui-th-ltr">领取金额</th>
											<th class="ui-th-column ui-th-ltr">是否完成有效投注任务</th>
											<th class="ui-th-column ui-th-ltr">领取金额</th>
											<th class="ui-th-column ui-th-ltr">是否完成有效充值任务</th>
											<th class="ui-th-column ui-th-ltr">领取金额</th>
											<th class="ui-th-column ui-th-ltr">是否完成下级有效用户任务</th>
											<th class="ui-th-column ui-th-ltr">领取金额</th>
											<th class="ui-th-column ui-th-ltr">第一次领取时间</th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- js -->
	<script type="text/javascript"
		src="<%=path%>/admin/common/common.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
	<script type="text/javascript"
		src="<%=path%>/admin/new_user_gift_task/js/new_user_gift_task.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
	<!-- dwr -->
	<script type='text/javascript' src='<%=path%>/dwr/interface/manageNewUserGiftTaskAction.js'></script>
	<script type='text/javascript' src='<%=path%>/dwr/interface/managerUserAction.js'></script>
</body>
</html>

