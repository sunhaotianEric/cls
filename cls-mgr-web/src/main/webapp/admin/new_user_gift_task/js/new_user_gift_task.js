function NewUserGiftTaskPage(){}
var newUserGiftTaskPage = new NewUserGiftTaskPage();

newUserGiftTaskPage.param = {
		
};
//分页参数
newUserGiftTaskPage.pageParam = {
		    pageNo : 1,
		    pageSize : defaultPageSize
};
/**
 * 查询参数
 */
newUserGiftTaskPage.queryParam = {
		//设置系统默认值
		bizSystem : null,
		//设置默认用户名
		userName : null,
		//设置默认查询开始时间
		startime : null,
		//设置默认查询结束时间
		endtime : null
};
$(document).ready(function() {
	newUserGiftTaskPage.initTableData();//查询所有新手注册信息
});


NewUserGiftTaskPage.prototype.initTableData = function(){
	//从页面获取查询参数
	newUserGiftTaskPage.queryParam = getFormObj($("#newUserGiftTaskForm"));
	newUserGiftTaskPage.table = $('#newUserGiftTaskTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"bPaginate" : false, 
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			
	    	manageNewUserGiftTaskAction.getAllNewUserGiftTask(newUserGiftTaskPage.queryParam,newUserGiftTaskPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.data = r[1].pageContent;;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询所有新手礼包信息失败.");
				}
				callback(returnData);
			});
		},
		"columns": [
		            //设置查询返回数据的值
		            {"data":null},
		            {
		            	"data": "bizSystemName", 
		            	"bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false
		            },
		            {"data":"userName",
		            	render: function(data, type, row, meta) {	
	            		return "<a href='javascript:void(0)' style='color:blue;text-decoration:underline;'  onclick='commonPage.showUserMsgPage(this,"+row.userId+")'>"+ row.userName +"</a>";
		            	}
		            },
		            //设置app下载任务完成状态
		            {
		            		"data" : "appDownTaskStatus",
		            		render : function(data, type, row, meta) {
		            			if (data == "0") {
		            				return "未完成";
		            			} else {
		            				return "已完成";
		            			}
		            		}
		            },
		            // 设置是否领取奖金
		            {
		            	"data":"appDownTaskMoney",
		            },
	            	//设置是否完成个人信息状态
	            	{
		            	"data":"completeInfoTaskStatus",
		            	render: function(data, type, row, meta) {
			            		if (data == "0") {
			            			return "未完成";
			            		} else{
			            			return  "已完成";
			            		}
	            		}
		            },
	            	//设置个人信息礼包领取状态
		            {
		            	"data":"completeInfoTaskMoney",
		            },
	            	//设置首笔投注任务完成状态
	            	{
		            	"data":"lotteryTaskStatus",
		            	render: function(data, type, row, meta) {
			            		if (data == "0") {
			            			return "未完成";
			            		} else{
			            			return  "已完成";
			            		}
	            		}
		            },
	            	//设置首笔投注奖金领取状态
		            {
		            	"data":"lotteryTaskMoney",
		            },
	            	//设置首笔充值任务完成状态
	            	{
		            	"data":"rechargeTaskStatus",
		            	render: function(data, type, row, meta) {
			            		if (data == "0") {
			            			return "未完成";
			            		} else{
			            			return  "已完成";
			            		}
	            		}
		            },
	            	//设置首笔充值奖金领取状态
		            {
		            	"data":"rechargeTaskMoney",
		            },
	            	//设置邀请下级用户状态
	            	{
		            	"data":"extendUserTaskStatus",
		            	render: function(data, type, row, meta) {
			            		if (data == "0") {
			            			return "未完成";
			            		} else{
			            			return  "已完成";
			            		}
	            		}
		            },
	            	//设置邀请下级用户奖金领取状态
		            {
		            	"data":"extendUserTaskMoney",
		            },
		            //设置第一次领取时间
		            {
		            	"data":"createDateStr",
		            }
	            ]
	}).api();
};

/**
 * 查询数据
 */
//设置查询按钮点击事件
NewUserGiftTaskPage.prototype.findNewUserGiftTask = function(){
	//获取页面ID为bizSystem的值
	var bizSystem = $("#bizSystem").val();
	//获取页面ID为userName的值
	var userName=$("#userName").val();
	//获取页面ID为startime的值
	var startime=$("#startime").val();
	//获取页面ID为endtime的值
	var endtime=$("#endtime").val();
	//根据获取到的值的情况设置queryParam参数的值
	if(bizSystem == ""){
		newUserGiftTaskPage.queryParam.bizSystem = null;
	}else{
		newUserGiftTaskPage.queryParam.bizSystem = bizSystem;
	}
	if (userName=="") {
		newUserGiftTaskPage.queryParam.userName = null;
	}else{
		newUserGiftTaskPage.queryParam.userName = userName;
	}
	if(startime!=""){
		newUserGiftTaskPage.queryParam.starTime=new Date(startime);
	}else{
		newUserGiftTaskPage.queryParam.starTime=null;
	}
	if(endtime!=""){
		newUserGiftTaskPage.queryParam.endTime=new Date(endtime);
	}else{
		newUserGiftTaskPage.queryParam.endTime=null;
	}
	newUserGiftTaskPage.table.ajax.reload();
};

NewUserGiftTaskPage.prototype.closeLayer=function(index){
	layer.close(index);
	newUserGiftTaskPage.table.ajax.reload();
}

