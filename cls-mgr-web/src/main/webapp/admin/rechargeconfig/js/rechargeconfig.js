function RechargeConfigPage(){}
var rechargeConfigPage = new RechargeConfigPage();

rechargeConfigPage.param = {
		
};
//分页参数
rechargeConfigPage.pageParam = {
		pageNo : 1,
	    pageSize : defaultPageSize
};
/**
 * 查询参数
 */
rechargeConfigPage.queryParam = {
		bizSystem : null,
		payType : null,
		refType: null,
		showType:null,
		enabled:null
		
};
$(document).ready(function() {
	rechargeConfigPage.initTableData(); //查询所有的充值配置
});


/**
 * 查询数据
 */
RechargeConfigPage.prototype.findRechargeConfig = function(){
	var refType = $("#refType").val();
	var payType = $("#payType").val();
	var bizSystem = $("#bizSystem").val();
	var showType = $("#showType").val();
	var enabled = $("#enabled").val();
	if(payType == ""){
		rechargeConfigPage.queryParam.payType = null;
	}else{
		rechargeConfigPage.queryParam.payType = payType;
	}
	if(refType == ""){
		rechargeConfigPage.queryParam.refType = null;
	}else{
		rechargeConfigPage.queryParam.refType = refType;
	}
	if(bizSystem == ""){
		rechargeConfigPage.queryParam.bizSystem = null;
	}else{
		rechargeConfigPage.queryParam.bizSystem = bizSystem;
	}
	if(showType == ""){
		rechargeConfigPage.queryParam.showType = null;
	}else{
		rechargeConfigPage.queryParam.showType = showType;
	}
	if(enabled == ""){
		rechargeConfigPage.queryParam.enabled = null;
	}else{
		rechargeConfigPage.queryParam.enabled = enabled;
	}
	rechargeConfigPage.table.ajax.reload();
};




/**
 * 删除
 */
RechargeConfigPage.prototype.delRechargeConfig = function(id){
	
	   swal({
           title: "您确定要删除这条信息吗",
           text: "请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "删除",
           closeOnConfirm: false
       },
       function() {
    	   managerRechargeConfigAction.delRechargeConfig(id,function(r){
   			if (r[0] != null && r[0] == "ok") {
   				swal({ title: "提示", text: "删除成功",type: "success"});
   				rechargeConfigPage.table.ajax.reload();
   			}else if(r[0] != null && r[0] == "error"){
   				swal(r[1]);
   			}else{
   				swal("删除信息失败.");
   			}
   	    });
       });
};

/**
 * 新增
 */
RechargeConfigPage.prototype.addRechargeConfig=function()
{
	
	 layer.open({
         type: 2,
         title: '充值配置新增',
         maxmin: false,
         shadeClose: true,
         //点击遮罩关闭层
         area: ['70%', '80%'],
         content: contextPath + "/managerzaizst/rechargeconfig/rechargeconfig_edit.html",
         cancel: function(index){ 
      	   rechargeConfigPage.table.ajax.reload();
      	   }
     });
}
/**
 * 编辑
 */
RechargeConfigPage.prototype.editRechargeConfig=function(id)
{
	
	  layer.open({
           type: 2,
           title: '充值配置编辑',
           maxmin: false,
           shadeClose: true,
           //点击遮罩关闭层
           area: ['70%', '80%'],
           content: contextPath + "/managerzaizst/rechargeconfig/"+id+"/rechargeconfig_edit.html",
           cancel: function(index){ 
        	   rechargeConfigPage.table.ajax.reload();
        	   }
       });
}

RechargeConfigPage.prototype.closeLayer=function(index){
	layer.close(index);
	rechargeConfigPage.table.ajax.reload();
}


RechargeConfigPage.prototype.initTableData = function(){
	
	rechargeConfigPage.queryParam = getFormObj($("#rechargeConfigForm"));
	rechargeConfigPage.table = $('#rechargeConfigTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			rechargeConfigPage.pageParam.pageSize = data.length;//页面显示记录条数
			rechargeConfigPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
			    	managerRechargeConfigAction.getAllRechargeConfig(rechargeConfigPage.queryParam,rechargeConfigPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
						recordsTotal : 0,
						recordsFiltered : 0,
						data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询充值配置数据失败.");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": null},
		            {"data": "bizSystemName", 
		                 "bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "showName"},
		            {"data": "showType",render: function(data, type, row, meta) {
	            		if (data == "PC") {
	            			return "电脑";
	            		} else if (data == "MOBILE"){
	            			return  "手机";
	            		}
	            		return data;
	            	}},
		            {"data": "payTypeStr"},
		            {"data": "refTypeStr"},
		            {"data": "refType",render: function(data, type, row, meta) {
	            		if (data == "TRANSFER") {
	            			return row.bankName+"["+row.bankUserName+"]";
	            		} else if (data == "THIRDPAY"){
	            			return  row.chargeTypeStr+"[商户号:"+row.memberId+"]";
	            		}
	            		return data;
	            	}},
	        		
	        		/*{"data": "showUserStarLevel"},
	        		{"data": "showUserSscRebate"},*/
	        		{"data": "showUserStarLevel",render: function(data, type, row, meta) {
	            		return data+"/"+row.showUserSscRebate;
	            	}},
	        		{"data": "orderNum"},
	        		{"data": "lowestValue",render: function(data, type, row, meta) {
	            		return data+"/"+row.highestValue;
	            	}},
		            {
		            	"data": "enabled",
		            	render: function(data, type, row, meta) {
		            		if (data == "1") {
		            			return '是';
		            		} else {
		            			return '否';
		            		}
		            		return data;
		            	}
		            },
		            {
		            	"data": "id",
		            	render: function(data, type, row, meta) {
		            		var str="<a class='btn btn-info btn-sm btn-bj' onclick='rechargeConfigPage.editRechargeConfig("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;" +
		            		"<a class='btn btn-danger btn-sm btn-del' onclick='rechargeConfigPage.delRechargeConfig("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";
		            		if(row.enabled=="1")
		            		{
		            			str="<a class='btn btn-warning btn-sm ' onclick='rechargeConfigPage.closeRechargeConfig("+data+")'><i class='glyphicon glyphicon-remove' ></i>&nbsp;停用 </a>&nbsp;&nbsp;"+str;
		            		}
		            		else
		            		{
		            			str="<a class='btn btn-sm btn-success' onclick='rechargeConfigPage.openRechargeConfig("+data+")'><i class='glyphicon glyphicon-ok'></i>&nbsp;启用 </a>&nbsp;&nbsp;"+str;
		            		}
		            		
		            		return str ;
		            	}
		            }
		            ]
	}).api(); 
};
/**
 * 开启充值配置
 */
RechargeConfigPage.prototype.openRechargeConfig = function(id){
	
	managerRechargeConfigAction.openRechargeConfig(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({ title: "提示", text: "操作成功",type: "success"});
			rechargeConfigPage.table.ajax.reload();
		}else if(r[0] != null && r[0] == "error"){
			//showErrorDlg(jQuery(document.body), r[1]);
			swal(r[1]);
		}else{
			//showErrorDlg(jQuery(document.body), "启用第三方支付银行代码失败.");
			swal("启用充值配置失败");
		}
	});
	
};

/**
 * 关闭充值配置
 */
RechargeConfigPage.prototype.closeRechargeConfig = function(id){
	
	managerRechargeConfigAction.closeRechargeConfig(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({ title: "提示", text: "操作成功",type: "success"});
			rechargeConfigPage.table.ajax.reload();
		}else if(r[0] != null && r[0] == "error"){
			//showErrorDlg(jQuery(document.body), r[1]);
			swal(r[1]);
		}else{
			//showErrorDlg(jQuery(document.body), "关闭第三方支付银行代码失败.");
			swal("关闭充值配置失败");
		}
	});
	
};
