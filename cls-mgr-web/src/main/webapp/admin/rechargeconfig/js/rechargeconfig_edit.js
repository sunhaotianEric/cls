function RechargeConfigEditPage() {
}
var rechargeConfigEditPage = new RechargeConfigEditPage();

rechargeConfigEditPage.param = {

};
/* var initFinsh = false; */

$(document)
		.ready(
				function() {
					rechargeConfigEditPage.initPage();
					var domainId = rechargeConfigEditPage.param.id; // 获取查询的ID
					if (domainId != null) {
						rechargeConfigEditPage.editRechargeConfig(domainId);
					}

					$(":radio").click(function() {
						// var val=$(this).val();
						var chargeType = $("#payType").val();
						var bizSystem = $("#bizSystem").val();
						if (currentUser.bizSystem == 'SUPER_SYSTEM') {
							if (bizSystem == null || $("#bizSystem").val().trim() == "") {
								swal("请选择业务系统!");
								$("#bizSystem").focus();
								$(":radio").attr("checked", false);
								return;
							}
						} else {
							bizSystem = currentUser.bizSystem;
						}
						if (chargeType == "" || chargeType == null) {
							swal("请选择充值类型!");
							$(":radio").attr("checked", false);
							return;
						}
						rechargeConfigEditPage.getRefIdByRefType(chargeType, $(this).val(), bizSystem);
					});

					$("#payType").change(function() {
						var chargeTypeVal = $(this).val();
						var bizSystem = $("#bizSystem").val();
						if (currentUser.bizSystem == 'SUPER_SYSTEM') {
							if (bizSystem == null || $("#bizSystem").val().trim() == "") {
								swal("请选择业务系统!");
								$("#bizSystem").focus();
								return;
							}
						} else {
							bizSystem = currentUser.bizSystem;
						}
						$("#refId").html("");
						if (chargeTypeVal == "BANK_TRANSFER") {
							$(":radio").attr("checked", false);
							$(":radio").each(function(i) {
								if ($(this).val() == "THIRDPAY") {

									$(this).attr("disabled", true);
								} else {
									$(this).attr("disabled", false);
									$(this).attr("checked", true);
									$(this).trigger("click");
									rechargeConfigEditPage.getRefIdByRefType(chargeTypeVal, "TRANSFER", bizSystem);
								}
							});
						} else if (chargeTypeVal == "QUICK_PAY") {
							$(":radio").attr("checked", false);
							$(":radio").each(function(i) {
								if ($(this).val() == "TRANSFER") {

									$(this).attr("disabled", true);
								} else {
									$(this).attr("disabled", false);
									$(this).attr("checked", true);
									$(this).trigger("click");
									rechargeConfigEditPage.getRefIdByRefType(chargeTypeVal, "THIRDPAY", bizSystem);
								}
							});
						} else if (chargeTypeVal == "ALIPAY" || chargeTypeVal == "ALIPAYWAP") {
							$(":radio").attr("checked", false);
							$(":radio").attr("disabled", false);
							$(":radio:checked").each(function(i) {
								if ($(this).val() == "TRANSFER") {
									rechargeConfigEditPage.getRefIdByRefType(chargeTypeVal, "TRANSFER", bizSystem);
								} else {
									rechargeConfigEditPage.getRefIdByRefType(chargeTypeVal, "THIRDPAY", bizSystem);
								}
							});
						} else if (chargeTypeVal == "WEIXIN" || chargeTypeVal == "WEIXINWAP") {
							$(":radio").attr("checked", false);
							$(":radio").attr("disabled", false);
							$(":radio:checked").each(function(i) {
								if ($(this).val() == "TRANSFER") {
									rechargeConfigEditPage.getRefIdByRefType(chargeTypeVal, "TRANSFER", bizSystem);
								} else {
									rechargeConfigEditPage.getRefIdByRefType(chargeTypeVal, "THIRDPAY", bizSystem);
								}
							});
						} else if (chargeTypeVal == "QQPAY" || chargeTypeVal == "QQPAYWAP") {
							$(":radio").attr("checked", false);
							$(":radio").attr("disabled", false);
							$(":radio:checked").each(function(i) {
								if ($(this).val() == "TRANSFER") {
									rechargeConfigEditPage.getRefIdByRefType(chargeTypeVal, "TRANSFER", bizSystem);
								} else {
									rechargeConfigEditPage.getRefIdByRefType(chargeTypeVal, "THIRDPAY", bizSystem);
								}
							});
						} else if (chargeTypeVal == "UNIONPAY") {
							$(":radio").attr("checked", false);
							$(":radio").attr("disabled", false);
							$(":radio:checked").each(function(i) {
								if ($(this).val() == "TRANSFER") {
									rechargeConfigEditPage.getRefIdByRefType(chargeTypeVal, "TRANSFER", bizSystem);
								} else {
									rechargeConfigEditPage.getRefIdByRefType(chargeTypeVal, "THIRDPAY", bizSystem);
								}
							});
						} else if (chargeTypeVal == "JDPAY") {
							$(":radio").attr("checked", false);
							$(":radio").attr("disabled", false);
							$(":radio:checked").each(function(i) {
								if ($(this).val() == "TRANSFER") {
									rechargeConfigEditPage.getRefIdByRefType(chargeTypeVal, "TRANSFER", bizSystem);
								} else {
									rechargeConfigEditPage.getRefIdByRefType(chargeTypeVal, "THIRDPAY", bizSystem);
								}
							});
						} else if (chargeTypeVal == "WEIXINBARCODE") {
							$(":radio").attr("checked", false);
							$(":radio").each(function(i) {
								if ($(this).val() == "TRANSFER") {

									$(this).attr("disabled", true);
								} else {
									$(this).attr("disabled", false);
									$(this).attr("checked", true);
									$(this).trigger("click");
									rechargeConfigEditPage.getRefIdByRefType(chargeTypeVal, "THIRDPAY", bizSystem);
								}
							});
						} else if (chargeTypeVal == "") {
							$(":radio").attr("checked", false);
							$(":radio").attr("disabled", false);
						}
						/*
						 * else { $(":radio").attr("checked",false);
						 * $(":radio").each(function(i){
						 * 
						 * 
						 * $(this).attr("disabled",false);
						 * 
						 * }); }
						 */

					})

					$("#bizSystem").change(function() {
						$('#payType option:eq(0)').attr('selected', 'selected');
						$("#refId").html("");
						$(":radio").attr("checked", false);
					})

					$(":input[name='add-btn']")
							.click(
									function() {
										var showUserNameLine = $("input[name='showUserNameLine']");
										if (showUserNameLine.length < 5) {
											var line = $("#trLine");
											var str = '<span class="input-group-btn"><button class="btn" type="button"><i>,</i></button><button class="btn btn-primary" value=""onclick="rechargeConfigEditPage.add()" name="add-btn" type="button"><i class="fa fa-plus"></i></button>'
													+ '</span><input type="text" class="form-control" name="showUserNameLine"><span class="input-group-btn"><button class="btn btn-primary" onclick="rechargeConfigEditPage.reduction(this)" name="reduction-btn" type="button">'
													+ '<i class="fa fa-minus"></i></button></span>';
											line.append(str);
										}

									})

					$(":input[name='reduction-btn']").click(function() {
						var showUserNameLine = $("input[name='showUserNameLine']");
						if (showUserNameLine.length > 1) {
							var parent = $(this).parent("span");
							parent.next("span").remove();
							parent.nextAll("input").first().remove();
							parent.nextAll("span").first().remove();
						}

					})
				});

RechargeConfigEditPage.prototype.add = function() {
	var showUserNameLine = $("input[name='showUserNameLine']");
	if (showUserNameLine.length < 5) {
		var line = $("#trLine");
		var str = '<span class="input-group-btn"><button class="btn" type="button"><i>,</i></button><button class="btn btn-primary" onclick="rechargeConfigEditPage.add()" name="add-btn" type="button"><i class="fa fa-plus"></i></button>'
				+ '</span><input type="text" class="form-control" name="showUserNameLine"><span class="input-group-btn"><button class="btn btn-primary" onclick="rechargeConfigEditPage.reduction(this)" name="reduction-btn" type="button">'
				+ '<i class="fa fa-minus"></i></button></span>';
		line.append(str);
	}
}

RechargeConfigEditPage.prototype.reduction = function(obj) {

	var showUserNameLine = $("input[name='showUserNameLine']");
	if (showUserNameLine.length > 1) {
		var parent = $(obj).parent("span");
		parent.prevAll("span").first().remove();
		parent.prev("input").remove();
		parent.remove();
	}

}

RechargeConfigEditPage.prototype.getRefIdByRefType = function(payType, refType, bizSystem) {
	var val = payType;
	var type = refType;
	var paraId = rechargeConfigEditPage.param.id
	var refId = rechargeConfigEditPage.param.refId;
	managerRechargeConfigAction.getRefIdByRefType(payType, refType, bizSystem, function(r) {
		if (r[0] != null && r[0] == "ok") {
			/*
			 * alert("保存成功"); self.location = contextPath +
			 * "/managerzaizst/quickchargeset.html";
			 */
			var domains = r[1];
			var domainListObj = $("#refId");
			domainListObj.html("<option value=''>全部</option>");
			var str = "";
			if (val == "BANK_TRANSFER") {
				for (var i = 0; i < domains.length; i++) {
					var domain = domains[i];
					if (paraId != null && domain.id == refId) {
						str += "<option selected='selected' value='" + domain.id + "'>" + domain.bankName + "[" + domain.bankUserName + "]</option>";
					} else {
						str += "<option value='" + domain.id + "'>" + domain.bankName + "[" + domain.bankUserName + "]</option>";
					}
					domainListObj.append(str);
					str = "";
				}
			} else if (val == "QUICK_PAY") {
				for (var i = 0; i < domains.length; i++) {
					var domain = domains[i];
					if (paraId != null && domain.id == refId) {
						str += "<option selected='selected' value='" + domain.id + "'>" + domain.chargeTypeStr + "[商户号:" + domain.memberId + "]</option>";
					} else {
						str += "<option value='" + domain.id + "'>" + domain.chargeTypeStr + "[商户号:" + domain.memberId + "]</option>";
					}
					domainListObj.append(str);
					str = "";
				}
			} else if (val == "ALIPAY" || val == "ALIPAYWAP") {
				if (type == "TRANSFER") {
					for (var i = 0; i < domains.length; i++) {
						var domain = domains[i];
						if (paraId != null && domain.id == refId) {
							str += "<option selected='selected' value='" + domain.id + "'>" + domain.bankName + "[" + domain.bankUserName + "]</option>";
						} else {
							str += "<option value='" + domain.id + "'>" + domain.bankName + "[" + domain.bankUserName + "]</option>";
						}
						domainListObj.append(str);
						str = "";
					}
				} else if (type == "THIRDPAY") {
					for (var i = 0; i < domains.length; i++) {
						var domain = domains[i];
						if (paraId != null && domain.id == refId) {
							str += "<option selected='selected' value='" + domain.id + "'>" + domain.chargeTypeStr + "[商户号:" + domain.memberId + "]</option>";
						} else {
							str += "<option value='" + domain.id + "'>" + domain.chargeTypeStr + "[商户号:" + domain.memberId + "]</option>";
						}
						domainListObj.append(str);
						str = "";
					}
				}
			} else if (val == "WEIXIN" || val == "WEIXINWAP") {
				if (type == "TRANSFER") {
					for (var i = 0; i < domains.length; i++) {
						var domain = domains[i];
						if (paraId != null && domain.id == refId) {
							str += "<option selected='selected' value='" + domain.id + "'>" + domain.bankName + "[" + domain.bankUserName + "]</option>";
						} else {
							str += "<option value='" + domain.id + "'>" + domain.bankName + "[" + domain.bankUserName + "]</option>";
						}
						domainListObj.append(str);
						str = "";
					}
				} else if (type == "THIRDPAY") {
					for (var i = 0; i < domains.length; i++) {
						var domain = domains[i];
						if (paraId != null && domain.id == refId) {
							str += "<option selected='selected' value='" + domain.id + "'>" + domain.chargeTypeStr + "[商户号:" + domain.memberId + "]</option>";
						} else {
							str += "<option value='" + domain.id + "'>" + domain.chargeTypeStr + "[商户号:" + domain.memberId + "]</option>";
						}
						domainListObj.append(str);
						str = "";
					}
				}
			} else if (val == "QQPAY" || val == "QQPAYWAP") {
				if (type == "TRANSFER") {
					for (var i = 0; i < domains.length; i++) {
						var domain = domains[i];
						if (paraId != null && domain.id == refId) {
							str += "<option selected='selected' value='" + domain.id + "'>" + domain.bankName + "[" + domain.bankUserName + "]</option>";
						} else {
							str += "<option value='" + domain.id + "'>" + domain.bankName + "[" + domain.bankUserName + "]</option>";
						}
						domainListObj.append(str);
						str = "";
					}
				} else if (type == "THIRDPAY") {
					for (var i = 0; i < domains.length; i++) {
						var domain = domains[i];
						if (paraId != null && domain.id == refId) {
							str += "<option selected='selected' value='" + domain.id + "'>" + domain.chargeTypeStr + "[商户号:" + domain.memberId + "]</option>";
						} else {
							str += "<option value='" + domain.id + "'>" + domain.chargeTypeStr + "[商户号:" + domain.memberId + "]</option>";
						}
						domainListObj.append(str);
						str = "";
					}
				}
			} else if (val == "UNIONPAY") {
				if (type == "TRANSFER") {
					for (var i = 0; i < domains.length; i++) {
						var domain = domains[i];
						if (paraId != null && domain.id == refId) {
							str += "<option selected='selected' value='" + domain.id + "'>" + domain.bankName + "[" + domain.bankUserName + "]</option>";
						} else {
							str += "<option value='" + domain.id + "'>" + domain.bankName + "[" + domain.bankUserName + "]</option>";
						}
						domainListObj.append(str);
						str = "";
					}
				} else if (type == "THIRDPAY") {
					for (var i = 0; i < domains.length; i++) {
						var domain = domains[i];
						if (paraId != null && domain.id == refId) {
							str += "<option selected='selected' value='" + domain.id + "'>" + domain.chargeTypeStr + "[商户号:" + domain.memberId + "]</option>";
						} else {
							str += "<option value='" + domain.id + "'>" + domain.chargeTypeStr + "[商户号:" + domain.memberId + "]</option>";
						}
						domainListObj.append(str);
						str = "";
					}
				}
			} else if (val == "JDPAY") {
				if (type == "TRANSFER") {
					for (var i = 0; i < domains.length; i++) {
						var domain = domains[i];
						if (paraId != null && domain.id == refId) {
							str += "<option selected='selected' value='" + domain.id + "'>" + domain.bankName + "[" + domain.bankUserName + "]</option>";
						} else {
							str += "<option value='" + domain.id + "'>" + domain.bankName + "[" + domain.bankUserName + "]</option>";
						}
						domainListObj.append(str);
						str = "";
					}
				} else if (type == "THIRDPAY") {
					for (var i = 0; i < domains.length; i++) {
						var domain = domains[i];
						if (paraId != null && domain.id == refId) {
							str += "<option selected='selected' value='" + domain.id + "'>" + domain.chargeTypeStr + "[商户号:" + domain.memberId + "]</option>";
						} else {
							str += "<option value='" + domain.id + "'>" + domain.chargeTypeStr + "[商户号:" + domain.memberId + "]</option>";
						}
						domainListObj.append(str);
						str = "";
					}
				}
			} else if (val == "WEIXINBARCODE") {
				if (type == "TRANSFER") {
					for (var i = 0; i < domains.length; i++) {
						var domain = domains[i];
						if (paraId != null && domain.id == refId) {
							str += "<option selected='selected' value='" + domain.id + "'>" + domain.bankName + "[" + domain.bankUserName + "]</option>";
						} else {
							str += "<option value='" + domain.id + "'>" + domain.bankName + "[" + domain.bankUserName + "]</option>";
						}
						domainListObj.append(str);
						str = "";
					}
				} else if (type == "THIRDPAY") {
					for (var i = 0; i < domains.length; i++) {
						var domain = domains[i];
						if (paraId != null && domain.id == refId) {
							str += "<option selected='selected' value='" + domain.id + "'>" + domain.chargeTypeStr + "[商户号:" + domain.memberId + "]</option>";
						} else {
							str += "<option value='" + domain.id + "'>" + domain.chargeTypeStr + "[商户号:" + domain.memberId + "]</option>";
						}
						domainListObj.append(str);
						str = "";
					}
				}
			}
		} else if (r[0] != null && r[0] == "error") {
			swal(r[1]);
			// showErrorDlg(jQuery(document.body), r[1]);
		} else {
			swal("添加收款银行数据失败.");
			// showErrorDlg(jQuery(document.body),
			// "添加收款银行数据失败.");
		}
	});
};

/**
 * 保存充值配置
 */
RechargeConfigEditPage.prototype.saveRechargeConfig = function() {
	var recharge = {};
	//通过正则表达式判断输入的数字是否为正整数;
	var type = "^[0-9]*[1-9][0-9]*$";
	var r = new RegExp(type);

	recharge.id = $("#id").val();
	recharge.payType = $("#payType").val();
	recharge.bizSystem = $("#bizSystem").val();
	recharge.refType = $(":radio:checked").val();
	recharge.refId = $("#refId").val();
	recharge.showName = $("#showName").val();
	recharge.showUserStarLevel = $("#showUserStarLevel").val();

	recharge.showUserSscRebate = $("#showUserSscRebate").val();
	recharge.orderNum = $("#orderNum").val();
	recharge.enabled = $("#enabled").val();
	recharge.showType = $("#showType").val();
	/* payBank.createTime = $("#createTime").val(); */
	recharge.lowestValue = $("#lowestValue").val();
	recharge.highestValue = $("#highestValue").val();
	recharge.rechargeGiftEnabled = $("#rechargeGiftEnabled").val();
	recharge.rechargeGiftScale = $("#rechargeGiftScale").val();
	recharge.rechargeGiftLowestValue = $("#rechargeGiftLowestValue").val();
	var str = "";
	var t = $("input[name='showUserNameLine']").length

	$("input[name='showUserNameLine']").each(function(i) {
		var v = $(this).val();
		if (v.trim() != "") {
			str += (v + ",");
		}
	})
	if (str.trim() != "") {
		recharge.showUserNameLine = str;
	} else {
		recharge.showUserNameLine = null;
	}
	recharge.showUserVipLevel = $("#showUserVipLevel").val();
	// 可显示的VIP等级组合
	recharge.showUserVipLevels = $("#showUserVipLevels").val();
	// 充值说明
	if ($("description").val() == "" || $("description").val() == null) {
		recharge.description = "暂无充值说明;";
	}
	if (currentUser.bizSystem == 'SUPER_SYSTEM') {
		if (recharge.bizSystem == null || recharge.bizSystem.trim() == "") {
			swal("请选择业务系统!");
			$("#bizSystem").focus();
			return;
		}
	}

	if (recharge.payType == null || recharge.payType.trim() == "") {
		swal("请选择充值类型!");
		$("#payType").focus();
		return;
	}
	if (recharge.refType == null || recharge.refType.trim() == "") {
		swal("请选择关联方式类型!");
		$("#refType").focus();
		return;
	}
	if (recharge.refId == null || recharge.refId.trim() == "") {
		swal("请选择关联方式!");
		$("#refId").focus();
		return;
	}
	if (recharge.showName == null || recharge.showName.trim() == "") {
		swal("请输入显示名称!");
		$("#showName").focus();
		return;
	}
	if (recharge.showUserStarLevel == null || recharge.showUserStarLevel.trim() == "") {
		/*
		 * swal("请输入能展示的用户最低星级!"); $("#showUserStarLevel").focus(); return;
		 */
		recharge.showUserStarLevel = 0;
	}

	if (isNaN(recharge.showUserStarLevel)) {
		swal("能展示的用户最低星级必须为数字!");
		$("#showUserStarLevel").focus();
		return;
	}
	if (recharge.showUserSscRebate == null || recharge.showUserSscRebate.trim() == "") {
		/*
		 * swal("请输入能显示的用户最低模式!"); $("#showUserSscRebate").focus(); return;
		 */
		recharge.showUserSscRebate = 0;
	}
	if (isNaN(recharge.showUserSscRebate)) {
		swal("能显示的用户最低模式必须为数字!");
		$("#showUserSscRebate").focus();
		return;
	}
	if (recharge.orderNum == null || recharge.orderNum.trim() == "") {
		swal("请输入排序数!");
		$("#orderNum").focus();
		return;
	}
	if (isNaN(recharge.orderNum)) {
		swal("排序数必须为数字!");
		$("#orderNum").focus();
		return;
	}
	if (recharge.enabled == null || recharge.enabled.trim() == "") {
		swal("请选择启用状态!");
		$("#enabled").focus();
		return;
	}
	if (recharge.lowestValue == null || recharge.lowestValue.trim() == "") {
		swal("请输入最低金额!");
		$("#lowestValue").focus();
		return;
	}
	if (isNaN(recharge.lowestValue)) {
		swal("最低金额必须为数字!");
		$("#lowestValue").focus();
		return;
	}
	if (recharge.highestValue == null || recharge.highestValue.trim() == "") {
		swal("请输入最高金额!");
		$("#highestValue").focus();
		return;
	}
	if (isNaN(recharge.highestValue)) {
		swal("最高金额必须为数字!");
		$("#highestValue").focus();
		return;
	}
	if (recharge.showUserVipLevel == null || recharge.showUserVipLevel.trim() == "") {
		recharge.showUserVipLevel = 1;
	}
	if (String(recharge.showUserVipLevel).indexOf(".") > -1) {
		swal("能展示VIP等级必须为正整数!");
		$("#showUserVipLevel").focus();
		return;
	}
	if (Math.sign(recharge.showUserVipLevel)==-1) {
		swal("能展示VIP等级必须为正整数!");
		$("#showUserVipLevel").focus();
		return;
	}
	if (isNaN(recharge.showUserVipLevel)) {
		swal("VIP等级必须为数字!");
		$("#showUserVipLevel").focus();
		return;
	}
	if (recharge.rechargeGiftEnabled.trim()=="1") {
		if((recharge.rechargeGiftScale==null || recharge.rechargeGiftScale.trim()=="") || (recharge.rechargeGiftLowestValue==null || recharge.rechargeGiftLowestValue.trim()=="")){
			swal("充值赠送开关如果开启,充值赠送比例与充值赠送最低金额都不能为空");
			$("#rechargeGiftEnabled").focus();
			return;
		}
		
	}
	// 后台检查输入的VIP集合是否合格
	managerRechargeConfigAction.saveOrUpdateRechargeConfig(recharge, function(r) {
		/*
		 * if (r[0] != null && r[0] == "ok") { alert("保存成功"); self.location =
		 * contextPath + "/managerzaizst/rechargeconfig.html"; }else if(r[0] !=
		 * null && r[0] == "error"){ showErrorDlg(jQuery(document.body), r[1]);
		 * }else{ showErrorDlg(jQuery(document.body), "添加充值配置数据失败."); }
		 */
		if (r[0] != null && r[0] == "ok") {
			swal({
				title : "保存成功",
				text : "您已经保存了这条信息。",
				type : "success"
			}, function() {
				var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
				parent.rechargeConfigPage.closeLayer(index);
			});
		} else if (r[0] != null && r[0] == "error") {
			swal(r[1]);
			// showErrorDlg(jQuery(document.body), r[1]);
		} else {
			swal("添加充值配置数据失败.");
			// showErrorDlg(jQuery(document.body), "添加充值配置数据失败.");
		}
	});
};

/**
 * 初始化页面
 */
RechargeConfigEditPage.prototype.initPage = function(id) {
	$("#enabled").val("1");
};

/**
 * 快捷充值配置
 */
RechargeConfigEditPage.prototype.editRechargeConfig = function(id) {
	managerRechargeConfigAction
			.getRechargeConfigById(
					id,
					function(r) {
						if (r[0] != null && r[0] == "ok") {
							var recharge = r[1];
							$("#id").val(recharge.id);
							$("#bizSystem").val(recharge.bizSystem);
							$("#payType").val(recharge.payType);
							$("#payType").attr("disabled", true);
							$(":radio[value='" + recharge.refType + "']").attr("checked", true);
							$(":radio").attr("disabled", true);
							rechargeConfigEditPage.param.refId = recharge.refId;
							// $("#refType").val(recharge.refType);
							rechargeConfigEditPage.getRefIdByRefType(recharge.payType, recharge.refType, recharge.bizSystem);
							/*
							 * $("#refId option").each(function(i){
							 * if($(this).val()==recharge.refId) {
							 * $(this).attr("selected",true); } })
							 */
							$("#showName").val(recharge.showName);
							$("#showUserStarLevel").val(recharge.showUserStarLevel);
							$("#showUserSscRebate").val(recharge.showUserSscRebate);
							$("#orderNum").val(recharge.orderNum);
							$("#enabled").val(recharge.enabled);
							$("#showType").val(recharge.showType);
							$("#lowestValue").val(recharge.lowestValue);
							$("#highestValue").val(recharge.highestValue);
							if (recharge.showUserNameLine != null && recharge.showUserNameLine != "") {
								var str = recharge.showUserNameLine.split(",");
								if (str.length == 1) {
									$("input[name='showUserNameLine']").val(str[0]);
								} else {
									for (var s = 0; s < str.length; s++) {
										if (s == 0) {
											$("input[name='showUserNameLine']").val(str[s]);
										} else {
											if (str[s] != "") {
												var line = $("#trLine");
												line
														.append('<span class="input-group-btn"><button class="btn" type="button"><i>,</i></button><button class="btn btn-primary" onclick="rechargeConfigEditPage.add()" name="add-btn" type="button"><i class="fa fa-plus"></i></button>'
																+ '</span><input type="text" class="form-control" name="showUserNameLine" value="'
																+ str[s]
																+ '"><span class="input-group-btn"><button class="btn btn-primary" onclick="rechargeConfigEditPage.reduction(this)" name="reduction-btn" type="button">'
																+ '<i class="fa fa-minus"></i></button></span>');

											}

										}

									}
								}

							}
							$("#showUserNameLine").val(recharge.showUserNameLine);
							$("#showUserVipLevel").val(recharge.showUserVipLevel);
							$("#showUserVipLevels").val(recharge.showUserVipLevels);
							// 编辑框数据回显
							$("#descriptionContent").val(recharge.description);
							$("#rechargeGiftEnabled").val(recharge.rechargeGiftEnabled);
							$("#rechargeGiftScale").val(recharge.rechargeGiftScale);
							$("#rechargeGiftLowestValue").val(recharge.rechargeGiftLowestValue);
						} else if (r[0] != null && r[0] == "error") {
							swal(r[1]);
							// showErrorDlg(jQuery(document.body), r[1]);
						} else {
							swal("获取充值配置失败.");
							// showErrorDlg(jQuery(document.body), "获取充值配置失败.");
						}
					});
};