<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
	<title>充值配置设置</title>
	<jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
<body>
	<div class="animated fadeIn">
		<div class="ibox-content">
			<div class="row m-b-sm m-t-sm">
				<form class="form-horizontal" id="rechargeConfigForm">
					<div class="row">
						<c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
							<div class="col-sm-3">
								<div class="input-group m-b">
									<span class="input-group-addon">商户</span>
									<cls:bizSel name="bizSystem" id="bizSystem"
										options="class:ipt form-control" />
								</div>
							</div>
						</c:if>
						<div class="col-sm-3">
							<div class="input-group m-b">
								<span class="input-group-addon">展示位置</span> <select
									class="form-control" id="showType" name="showType">
									<option value="">＝＝请选择＝＝</option>
									<option value="PC">电脑</option>
									<option value="MOBILE">手机</option>
								</select>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="input-group m-b">
								<span class="input-group-addon">充值类型</span>
								<cls:enmuSel name="payType"
									className="com.team.lottery.enums.EFundPayType"
									options="class:ipt form-control" id="payType" />
							</div>
						</div>
						<div class="col-sm-3">
							<div class="input-group m-b">
								<span class="input-group-addon">关联方式类型</span>
								<cls:enmuSel name="refType"
									className="com.team.lottery.enums.EFundRefType"
									options="class:ipt form-control" id="refType" />

							</div>
						</div>
						<div class="col-sm-3">
							<div class="input-group m-b">
								<span class="input-group-addon">启用状态</span> <select
									class="form-control" id="enabled" name="enabled">
									<option value="">＝＝请选择＝＝</option>
									<option value="1">启用</option>
									<option value="0">停用</option>
								</select>
							</div>
						</div>
						<c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
							<!-- <div class="col-sm-2"></div> -->
						</c:if>
						<div class="col-sm-2">
							<div class="form-group nomargin">
								<button type="button" class="btn btn-outline btn-default btn-js"
									onclick="rechargeConfigPage.pageParam.pageNo = 1;rechargeConfigPage.findRechargeConfig()">
									<i class="fa fa-search"></i>&nbsp;查 询
								</button>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<button type="button"
									class="btn btn-outline btn-default btn-add"
									onclick="rechargeConfigPage.addRechargeConfig()">
									<i class="fa fa-plus"></i>&nbsp;新增
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="jqGrid_wrapper">
				<div class="ui-jqgrid ">
					<div class="ui-jqgrid-view">
						<div class="ui-jqgrid-hdiv">
							<div class="ui-jqgrid-hbox" style="padding-right: 0">
								<table id="rechargeConfigTable"
									class="ui-jqgrid-htable ui-common-table table table-bordered">
									<thead>
										<tr class="ui-jqgrid-labels">
											<th class="ui-th-column ui-th-ltr">序号</th>
											<th class="ui-th-column ui-th-ltr">商户</th>
											<th class="ui-th-column ui-th-ltr">显示名称</th>
											<th class="ui-th-column ui-th-ltr">展示位置</th>
											<th class="ui-th-column ui-th-ltr">充值类型</th>
											<!--<th>商户类型</th>预留-->
											<th class="ui-th-column ui-th-ltr">关联方式类型</th>
											<th class="ui-th-column ui-th-ltr">关联方式</th>

											<!-- <th class="ui-th-column ui-th-ltr">能展示的用户最低星级</th>
													<th class="ui-th-column ui-th-ltr">能显示的用户最低模式</th> -->
											<th class="ui-th-column ui-th-ltr">展示最低星级/模式</th>
											<th class="ui-th-column ui-th-ltr">排序数</th>
											<th class="ui-th-column ui-th-ltr">充值金额最低/最高</th>
											<th class="ui-th-column ui-th-ltr">启用状态</th>
											<th class="ui-th-column ui-th-ltr">操作</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<script type="text/javascript"
		src="<%=path%>/admin/rechargeconfig/js/rechargeconfig.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
	<!-- dwr -->
	<script type='text/javascript'
		src='<%=path%>/dwr/interface/managerRechargeConfigAction.js'></script>
</body>
</html>

