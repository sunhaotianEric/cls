<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>网站公告</title>
<jsp:include page="/admin/include/include.jsp"></jsp:include>


<link rel="stylesheet"
	href="<%=path%>/resources/kindeditor-4.1.10/themes/default/default.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" />
<script charset="utf-8"
	src="<%=path%>/resources/kindeditor-4.1.10/kindeditor-min.js"></script>
<script charset="utf-8"
	src="<%=path%>/resources/kindeditor-4.1.10/lang/zh_CN.js"></script>
<script>
	var editor; //editor.html()
	KindEditor.ready(function(K) {
		editor = K.create('textarea[name="content"]', {
			allowFileManager : false
		});
	});
</script>
<%
	String id = request.getParameter("id"); //获取查询的商品ID
%>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeIn">
		<form action="javascript:void(0)">
			<div class="row">
				<c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
					<div class="col-sm-12">
						<div class="input-group m-b">
							<span class="input-group-addon">商户：</span>
							<cls:bizSel name="bizSystem" id="bizSystem"
								options="class:ipt form-control" />
						</div>
					</div>
				</c:if>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">显示名称：</span> <input type="text"
							value="" class="form-control" name=showName id="showName">
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">展示位置：</span> <select
							class="form-control" id="showType">
							<option value="PC">电脑</option>
							<option value="MOBILE">手机</option>
						</select>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">充值类型：</span>
						<!-- <input type="text" value="" class="form-control" name="title" id="title"> -->
						<cls:enmuSel name="payType"
							className="com.team.lottery.enums.EFundPayType"
							options="class:ipt form-control" id="payType" />
						<input type="hidden" id="id" name="id">
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">

						<span class="input-group-addon">关联方式类型：</span>
						<div class="form-control">
							<input type="radio" name="refType" value="TRANSFER" />转账方式 <input
								type="radio" name="refType" value="THIRDPAY" />第三方支付方式
						</div>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">关联方式：</span>
						<!-- <input type="text" value="" class="form-control" name="refId" id="refId"> -->
						<select class="form-control" name="refId" id="refId"></select>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">充值最低金额：</span> <input type="text"
							value="" class="form-control" name="lowestValue" id="lowestValue">
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">充值最高金额：</span> <input type="text"
							value="" class="form-control" name="highestValue"
							id="highestValue">
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">充值赠送开关是否启用：</span> <select
							class="form-control" id="rechargeGiftEnabled">
							<option value="1">启用</option>
							<option value="0" selected = "selected">停用</option>
						</select>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">充值赠送比例：</span> 
						<select class="form-control" name='rechargeGiftScale' id='rechargeGiftScale'> 
		 							<option value='0.005'>0.5%</option>
									<option value='0.01'>1%</option>
									<option value='0.02'>2%</option>
									<option value='0.03'>3%</option>
									<option value='0.04'>4%</option>
									<option value='0.05'>5%</option>
									<option value='0.06'>6%</option>
									<option value='0.07'>7%</option>
									<option value='0.08'>8%</option>
									<option value='0.09'>9%</option>
									<option value='0.1'>10%</option>
								</select>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">充值赠送最低金额：</span> <input type="text"
							value="" class="form-control" name="rechargeGiftLowestValue"
							id="rechargeGiftLowestValue">
					</div>
				</div>

				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">VIP等级：</span> <input type="text"
							value="" class="form-control" name="showUserVipLevel"
							id="showUserVipLevel">
					</div>
				</div>

				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">VIP等级组：</span> <input type="text"
							value="" class="form-control" name="showUserVipLevels"
							id="showUserVipLevels">
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">能展示的用户最低星级：</span> <input
							type="text" value="0" class="form-control"
							name="showUserStarLevel" id="showUserStarLevel">
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">能显示的用户最低模式：</span> <input
							type="text" value="0" class="form-control"
							name="showUserSscRebate" id="showUserSscRebate">
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">能显示的代理线用户名：</span>
						<div class="from-group input-group" id="trLine">
							<span class="input-group-btn">
								<button class="btn btn-primary" name="add-btn" type="button">
									<i class="fa fa-plus"></i>
								</button>
							</span><input type="text" class="form-control" name="showUserNameLine">
							<span class="input-group-btn">
								<button class="btn btn-primary" name="reduction-btn"
									type="button">
									<i class="fa fa-minus"></i>
								</button>
							</span>
						</div>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">排序数：</span> <input type="text"
							value="" class="form-control" name="orderNum" id="orderNum">
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">是否启用：</span> <select
							class="form-control" id="enabled">
							<option value="1">启用</option>
							<option value="0">停用</option>
						</select>
					</div>
				</div>

				<div class="col-sm-12">
					<input id="description" name="" type="hidden" /> <label
						class="col-sm-2 control-label">充值说明:</label>
					<div class="col-sm-8">
						<textarea name="comment" class="form-control" required=""
							style="height: 80px; width: 800px" aria-required="true"
							id="descriptionContent"></textarea>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="row">
						<div class="col-sm-6 text-center">
							<button type="button" class="btn btn-w-m btn-white"
								onclick="rechargeConfigEditPage.saveRechargeConfig()">提
								交</button>
						</div>
						<div class="col-sm-6 text-center">
							<input type="reset" class="btn btn-w-m btn-white" value="重 置" />
						</div>
					</div>
				</div>
		</form>
	</div>
	<!-- js -->
	<!-- js -->
	<script type="text/javascript"
		src="<%=path%>/admin/rechargeconfig/js/rechargeconfig_edit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
	<!-- dwr -->
	<script type='text/javascript'
		src='<%=path%>/dwr/interface/managerRechargeConfigAction.js'></script>
	<script type="text/javascript">
		rechargeConfigEditPage.param.id =
	<%=id%>
		;
	</script>
</body>
</html>

