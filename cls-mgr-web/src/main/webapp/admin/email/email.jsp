<%-- <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%> --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
import = "com.team.lottery.enums.EHelpType,com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();

	StringBuffer  url = request.getRequestURL();
	String domainUrl=url.toString().replaceAll(request.getRequestURI(), "")+path;
	String staticImageServerUrl="http://"+SystemConfigConstant.picHost;
%>
<!DOCTYPE html>
<html>  
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>邮件管理</title>
        <link rel="shortcut icon" href="favicon.ico">
        <jsp:include page="/admin/include/include.jsp"></jsp:include>
        
        <link rel="stylesheet" href="<%=path%>/resources/kindeditor-4.1.10/themes/default/default.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>?v=<%=SystemConfigConstant.mgrWebRsVersion%>?v=<%=SystemConfigConstant.mgrWebRsVersion%>" />
		<script charset="utf-8" src="<%=path%>/resources/kindeditor-4.1.10/kindeditor-min.js"></script>
		<script charset="utf-8" src="<%=path%>/resources/kindeditor-4.1.10/lang/zh_CN.js"></script>
		<style>
        	.por {position: relative;}
	        .tags {position: absolute;top: 0;width: 100%;height: 34px;line-height: 34px;right: 8px;padding-left: 25px;overflow: auto;padding-right: 10px;}
	        .tags a{color: #555;margin-right: 5px;display: inline-block;}
	        .tags .fa-close {font-size: 14px;}
	        .ke-container{margin-left:2%;}
	        .bt0{border-top:0;}
	        .folder-list {max-height: 518px;overflow: auto;}
        </style>
	    <script>
	    var  helpPage=parent.helpPage;
	    var layerindex = parent.layer.getFrameIndex(window.name);
	    var olddomain = document.domain;
	       
	      // alert("domain is:"+document.domain);  
			var editor;  //editor.html()
			//图片服务器地址
			//var staticImageUrl = 'http://www.zzh.com:8080/cls-mgr-web';
			var staticImageUrl='<%=domainUrl%>';
			var url = staticImageUrl.split('//');
			//var staticImageUrl = 'http://www.img.zzh.com';
			//var imgurl =  "http://42.51.191.174";
			var imgurl='<%=staticImageServerUrl%>';
			KindEditor.ready(function(K) {
				//document.domain = 'zzh.com';
				editor = K.create('textarea[name="content"]', {
					uploadJson : location.protocol+'//'+url[1]+'/UploadFileForJsonServlet',
					fileManagerJson :imgurl+'/file_manager_json.jsp',
					allowFileManager : true
				});
			});
		</script>
		<%
		 String id = request.getParameter("id");  //获取查询的商品ID
		%>
	</head>
<body class="gray-bg">
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-sm-12 animated fadeInRight">
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                      <c:if test="${admin.bizSystem != 'SUPER_SYSTEM'}">
                      <li class="active">
                        <a data-toggle="tab" href="#tab-1" aria-expanded="true" id="recv">收件箱</a>
                      </li>
                      </c:if>
                      <c:if test="${admin.bizSystem == 'SUPER_SYSTEM'}">
                      <li class="">
                        <a data-toggle="tab" href="#tab-2" aria-expanded="false" id="send">已发送</a>
                      </li>
                      </c:if>
                      <c:if test="${admin.bizSystem == 'SUPER_SYSTEM'}">
                      <li class="">
                        <a data-toggle="tab" href="#tab-3" aria-expanded="false" id="xx">写信</a>
                      </li>
                      </c:if>
                    </ul>
                    <div class="tab-content">
                    	<c:if test="${admin.bizSystem != 'SUPER_SYSTEM'}">
                        <div id="tab-1" class="tab-pane active">
                            <div class="panel-body">
                                <div class="mail-box-header">
                                	<form class="pull-right mail-search">
                                        <div class="input-group">
                                            <input type="text" class="form-control input-sm" id="sub" name="sub" placeholder="搜索邮件标题">
                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-sm btn-primary" onclick="emailPage.Refresh(1)">
                                                    搜索
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                    <h2 id="recvNote">
                                    收件箱 (16)
                                    </h2>
                                    <div class="mail-tools tooltip-demo m-t-md">
                                        <!-- <div class="btn-group pull-right">
                                            <button class="btn btn-white btn-sm"><i class="fa fa-arrow-left"></i>
                                            </button>
                                            <button class="btn btn-white btn-sm"><i class="fa fa-arrow-right"></i>
                                            </button>
                                        </div>
                                        <button class="btn btn-white btn-sm pull-right" data-toggle="tooltip" data-placement="left" title="刷新邮件列表" style="margin-right: 5px;"><i class="fa fa-refresh"></i> 刷新</button> -->
                                        <!-- <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="标为已读"><i class="fa fa-eye"></i>
                                        </button>
                                        <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="标为重要"><i class="fa fa-exclamation"></i>
                                        </button>
                                        <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="标为垃圾邮件"><i class="fa fa-trash-o"></i>
                                        </button> -->
                                    </div>
                                </div>
                                <div class="mail-box">
                                    <table class="table table-hover table-mail" id="recvNoteTable">
                                        <thead>
                                           <tr>
                                            <th class="ui-th-column ui-th-ltr">序号</th>
                                            <th class="ui-th-column ui-th-ltr">状态</th>
                                            <th class="ui-th-column ui-th-ltr">发件人</th>
                                            <th class="ui-th-column ui-th-ltr">标题</th>
                                            <!-- <th class="ui-th-column ui-th-ltr">内容</th> -->
                                            <th class="ui-th-column ui-th-ltr">时间</th>
                                            <th class="ui-th-column ui-th-ltr">操作</th>
                                           </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        </c:if>
                        <c:if test="${admin.bizSystem == 'SUPER_SYSTEM'}">
                        <div id="tab-2" class="tab-pane">
                            <div class="panel-body">
                                <div class="mail-box-header">
                                    <form class="pull-right mail-search">
                                        <div class="input-group">
                                            <input type="text" class="form-control input-sm" id="sendsub" name="sendsub" placeholder="搜索邮件标题">
                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-sm btn-primary" onclick="emailPage.Refresh(2)">
                                                    搜索
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                    <h2 id="sendNote">
                                    已发送 (16)
                                    </h2>
                                    <div class="mail-tools tooltip-demo m-t-md">
                                        <!-- <div class="btn-group pull-right">
                                            <button class="btn btn-white btn-sm"><i class="fa fa-arrow-left"></i>
                                            </button>
                                            <button class="btn btn-white btn-sm"><i class="fa fa-arrow-right"></i>
                                            </button>

                                        </div>
                                        <button class="btn btn-white btn-sm pull-right" data-toggle="tooltip" data-placement="left" title="刷新邮件列表" style="margin-right: 5px;"><i class="fa fa-refresh"></i> 刷新</button> -->
                                        <!-- <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="标为已读"><i class="fa fa-eye"></i>
                                        </button>
                                        <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="标为重要"><i class="fa fa-exclamation"></i>
                                        </button>
                                        <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="标为垃圾邮件"><i class="fa fa-trash-o"></i>
                                        </button> -->
                                    </div>
                                </div>
                                <div class="mail-box">
                                    <table class="table table-hover table-mail" id="sendNoteTable">
                                        <thead>
                                           <tr>
                                            <th class="ui-th-column ui-th-ltr">序号</th>
                                            <th class="ui-th-column ui-th-ltr">收件人</th>
                                            <th class="ui-th-column ui-th-ltr">标题</th>
                                            <!-- <th class="ui-th-column ui-th-ltr">内容</th> -->
                                            <th class="ui-th-column ui-th-ltr">时间</th>
                                            <th class="ui-th-column ui-th-ltr">操作</th>
                                           </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        </c:if>
                        <c:if test="${admin.bizSystem == 'SUPER_SYSTEM'}">
                        <div id="tab-3" class="tab-pane">
                            <div class="panel-body">
                                <div class="col-lg-10 animated fadeInRight">
                                    <div class="mail-box-header">
                                        <div class="pull-right tooltip-demo">
                                            <!-- <a href="mailbox2.html" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="存为草稿"><i class="fa fa-pencil"></i> 存为草稿</a> -->
                                        </div>
                                        <h2>
                                        写信
                                        </h2>
                                    </div>
                                    <div class="mail-box">
                                        <div class="mail-body">
                                            <form class="form-horizontal" method="get">
                                                <div class="form-group">
                                                    <label class="col-sm-1 control-label">发送到：</label>
                                                    <div class="col-sm-11 por">
                                                    	<div class="tags">
                                                            <!-- <a href="#">chenchen <i class="fa fa-close"></i></a> -->
                                                        </div>
                                                        <input type="text" id="toAdminNames" name="toAdminNames" class="form-control add-name" value="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-1 control-label">主题：</label>

                                                    <div class="col-sm-11">
                                                        <input type="text" id="sub" name="sub" class="form-control" value="">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="mail-text h-500 bt0">	
                                            <textarea name="content" id="content" style="width:96%;height:400px;visibility:hidden;"></textarea>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="mail-body text-right tooltip-demo bt0">
                                            <a href="#" class="btn btn-sm btn-primary" onclick="emailPage.insertSendNote()" data-toggle="tooltip" data-placement="top" title="Send"><i class="fa fa-reply"></i> 发送</a>
                                            <a href="email.html" class="btn btn-danger btn-sm btn-abandon" data-toggle="tooltip" data-placement="top" title="放弃"><i class="fa fa-times"></i> 放弃</a>
                                            <!-- <a href="#" class="btn btn-white btn-sm" onclick="emailPage.insertSendNote()" data-toggle="tooltip" data-placement="top" title="Discard email"><i class="fa fa-times"></i> 放弃</a> -->
                                            <!-- <a href="mailbox2.html" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Move to draft folder"><i class="fa fa-pencil"></i> 存为草稿</a> -->
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-content mailbox-content">
                                            <div class="file-manager">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">商户</span>
                                                    <select class="ipt form-control" id="bizSystem" onchange="emailPage.changes()">
                                                        
                                                    </select>
                                                </div>
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">角色</span>
                                                    <select class="ipt form-control" id="state" onchange="emailPage.changes()">
                                                    	<option value="" selected="selected">所有管理员</option>
                                                        <option value="0">超级管理员</option>
                                                        <option value="1">管理员</option>
                                                        <option value="2">财务管理员</option>
                                                        <option value="3">客服管理员</option>
                                                    </select>
                                                </div>
                                                <div class="space-25"></div>
                                                <div class="mail-title">
                                                	<h5 style="float:left;">添加收件人</h5>
                                                	<a href="#" class="btn btn-sm btn-primary" onclick="emailPage.AllRecvNote()" style="float:right;margin-top: -8px;"><i class="fa fa-plus"></i> 添加所有</a>
                                                </div>
                                                <div class="clearfix"></div>
                                                <ul class="folder-list m-b-md" style="padding: 0" id="adminlist">
                                                    <li>
                                                        <a href="javascript:;"><i class="fa fa-user-plus"></i>&nbsp;&nbsp;陈陈陈(示例)</a>
                                                    </li>
                                                </ul>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </c:if>
                    </div>
            </div>
        </div>
    </div>
    </div>
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/email/js/email.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src="<%=path%>/dwr/interface/managerAdminRecvNoteAction.js"></script>
    <script type='text/javascript' src="<%=path%>/dwr/interface/managerAdminSendNoteAction.js"></script> 
    <script>
        $(document).ready(function(){$(".i-checks").iCheck({checkboxClass:"icheckbox_square-green",radioClass:"iradio_square-green",})});
    </script>
    <script>
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $('.summernote').summernote({
                lang: 'zh-CN'
            });

        });
        var save = function () {
            var aHTML = $('.click2edit').code(); //save HTML If you need(aHTML: array).
            $('.click2edit').destroy();
        };
    </script>

    <!-- 添加收件人 -->
    <script type="text/javascript">
        $(function(){
            /* $('.folder-list li').on('click',function(){
                $('.add-name').val($('.add-name').val()+$(this).find('a').text()+'；')
            }) */
        })
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $(".btn-del").click(function() {
                swal({
                    title: "您确定要删除这封信吗",
                    text: "请谨慎操作！",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    cancelButtonText: "取消",
                    confirmButtonText: "删除",
                    closeOnConfirm: false
                },
                function() {
                    swal("删除成功！", "您已经删除了这条信息。", "success")
                })
            });
        });
    </script>
</body>
</html>