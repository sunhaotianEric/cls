function MainPage(){};
var mainPage=new MainPage();

$(document).ready(function() {
	mainPage.initTableDate();
});

MainPage.prototype.initTableDate=function(){
	if(note=="recv"){
		managerAdminRecvNoteAction.selectByPrimaryKey(id,function(r){
			if(r[0]=="ok"){
				$("#bt h3").empty();
				$("#bt h5").empty();
				$("#count").empty();
				var name="";
				if(r[1].fromBizSystem=="SUPER_SYSTEM"){
					name="BOSS系统";
				}else{
					name=r[1].fromAdminName;
				}
				$("#bt h3").append("<span class='font-noraml'>主题： </span>"+r[1].sub);
				$("#bt h5").append("<span class='pull-right font-noraml'>"+r[1].createDateStr+"</span><span class='font-noraml'>发件人： </span>"+name);
				$("#count").append(r[1].content);
			}else{
				swal(r[1]);
			}
		});
	}else if(note=="send"){
		managerAdminSendNoteAction.selectByPrimaryKey(id,function(r){
			if(r[0]=="ok"){
				$("#bt h3").empty();
				$("#bt h5").empty();
				$("#count").empty();
				$("#bt h3").append("<span class='font-noraml'>主题： </span>"+r[1].sub);
				if(r[1].toAdminNames.length>60){
					$("#bt h5").append("<span class='pull-right font-noraml'>"+r[1].createDateStr+"</span><span class='font-noraml'>收件人： </span><span title="+r[1].toAdminNames.substring(0,r[1].toAdminNames.length-1)+">"+r[1].toAdminNames.substring(0,60)+"...</span>");
				}else{
					$("#bt h5").append("<span class='pull-right font-noraml'>"+r[1].createDateStr+"</span><span class='font-noraml'>收件人： </span>"+r[1].toAdminNames.substring(0,r[1].toAdminNames.length-1));
				}
				$("#count").append(r[1].content);
				if(r[1].readedAdminIds!=null&&r[1].readedAdminIds!=""){
					$("#Readmail").html("(已读)"+r[1].readedAdminIds.substring(0,r[1].readedAdminIds.length-1));
				}
			}else{
				swal(r[1]);
			}
		});
	}
}

/**
 * 删除邮件
 */
MainPage.prototype.delAdminRecvNote=function(){
	swal({
        title: "您确定要删除这条消息吗?",
        text: "请谨慎操作！",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        cancelButtonText: "取消",
        confirmButtonText: "删除",
        closeOnConfirm: false
    },
    function() {
    	if(note=="recv"){
    		managerAdminRecvNoteAction.deleteByPrimaryKey(id,function(r){
    			if(r[0]=="ok"){
    				swal(r[1]);
    				parent.emailPage.Refresh(1);
    			}else{
    				swal(r[1]);
    			}
    		});
    	}else if(note=="send"){
    		managerAdminSendNoteAction.deleteByPrimaryKey(id,function(r){
    			if(r[0]=="ok"){
    				swal(r[1]);
    				parent.emailPage.Refresh(2);
    			}else{
    				swal(r[1]);
    			}
    		});
    	}
    });
}