function EmailPage(){};
var emailPage=new EmailPage();

emailPage.queryParam = {
		sub:null
};
//分页参数
emailPage.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize
};

$(document).ready(function() {
	$("#recv").on("click",function(){
		emailPage.initTableDate1();
	});
	
	$("#send").on("click",function(){
		emailPage.initTableDate2();
	});
	
	$("#xx").on("click",function(){
		dwr.engine.setAsync(false);
		//加载所有业务系统
		emailPage.getAllBizSystem();
		emailPage.changes();
		dwr.engine.setAsync(true);
	});
	
	$('#sendNoteTable tbody').on('click', 'tr', function () {
        var data = emailPage.tableSendNote.row( this ).data();
        if(del){
        	emailPage.editNote(data.id,"send");
        }
    } );
	
	$('#recvNoteTable tbody').on('click', 'tr', function () {
        var data = emailPage.tableRecvNote.row( this ).data();
        if(del){
        	emailPage.editNote(data.id,"recv");
        }
    } );
	
	if(currentUser.bizSystem=='SUPER_SYSTEM'){
		$("#send").click();
	}else{
		$("#recv").click();
	}
});

EmailPage.prototype.changes=function(){
	//加载管理员
	emailPage.getAllAdminByQuery();
}

/**
 * 加载收件箱数据
 */
EmailPage.prototype.initTableDate1=function(){
	emailPage.queryParam.sub = $("#sub").val();
	emailPage.tableRecvNote = $('#recvNoteTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"scrollX": true,
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			emailPage.pageParam.pageSize = data.length;//页面显示记录条数
			emailPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerAdminRecvNoteAction.getAdminRecvNoteBytomainid(emailPage.queryParam,emailPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
					$("#recvNote").html("收件箱("+returnData.recordsFiltered+")");
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": null},
		            {
		            	"data": "readStatus",
		            	render:function(data, type, row, meta){
		            		var str="";
		            		if(data=="已读"){
		            			str="<i class='fa fa fa-envelope-o btn-white'></i>已读";
		            		}else{
		            			str="<i class='fa fa-envelope'></i>未读";
		            		}
		            		return str;
		            	}
		            },
		            {
		            	"data": "fromAdminName",
		            	render:function(data, type, row, meta){
		            		if(row.fromBizSystem=='SUPER_SYSTEM'){
		            			return "BOSS系统";
		            		}
		            		return data;
		            	}
		            },
		            {
		            	"data": "sub",
		            	render:function(data, type, row, meta){
		            		if(data.length>40){
		            			return data.substring(0,40)+"...";
		            		}
		            		return data;
		            	}
		            },
		            /*{
		            	"data": "content",
		            	render:function(data, type, row, meta){
		            		if(data.length>35){
		            			return data.substring(0,35)+"...";
		            		}
		            		return data;
		            	}
		            },*/
		            {"data":"createDateStr"},
		            {
		            	"data":"id",
		            	render:function(data, type, row, meta){
		            	   var str="<a class='btn btn-danger btn-sm btn-del' onclick=emailPage.delEmail("+data+",'recv')><i class='fa fa-trash'></i>&nbsp;删除</a>";
		            	   return str;
		            	}
		            }
	            ]
	}).api(); 
}

/**
 * 加载发件箱数据
 */
EmailPage.prototype.initTableDate2=function(){
	emailPage.queryParam.sub = $("#sendsub").val();
	emailPage.tableSendNote = $('#sendNoteTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"bRetrieve" : true,
		"scrollX": true,
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			emailPage.pageParam.pageSize = data.length;//页面显示记录条数
			emailPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerAdminSendNoteAction.getAdminSendNotePage(emailPage.queryParam,emailPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
					$("#sendNote").html("已发送("+returnData.recordsFiltered+")");
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data":null},
		            {
		            	"data": "toAdminNames",
		            	render:function(data, type, row, meta){
		            		if(data.length>40){
		            			return "<span title="+data+">"+data.substring(0,40)+"...</span>";
		            		}
		            		return data.substring(0,data.length-1);
		            	}
		            },
		            {
		            	"data": "sub",
		            	render:function(data, type, row, meta){
		            		if(data.length>30){
		            			return "<span title="+data+">"+data.substring(0,30)+"...</span>";
		            		}
		            		return data;
		            	}
		            },
		            /*{
		            	"data": "content",
		            	render:function(data, type, row, meta){
		            		if(data.length>30){
		            			return data.substring(0,30)+"...";
		            		}
		            		return data;
		            	}
		            },*/
		            {"data":"createDateStr"},
		            {
		            	"data":"id",
		            	render:function(data, type, row, meta){
		            	   var str="<a class='btn btn-danger btn-sm btn-del' onclick=emailPage.delEmail("+data+",'send')><i class='fa fa-trash'></i>&nbsp;删除</a>";
		            	   return str;
		            	}
		            }
	            ]
	}).api(); 
}

/**
 * 写信
 */
EmailPage.prototype.insertSendNote=function(){
	var json={};
	json.toAdminIds=userids;
	json.toAdminNames=names;
	json.sub=$("#sub").val();
	json.content=editor.html();
	json.recvCount=names.split(",").length-1;
	if(json.sub==""){
		swal("标题不能为空");
		return;
	}
	if(json.content==""){
		swal("内容不能为空");
		return;
	}
	if(json.toAdminNames==""){
		swal("请选择收件人");
		return;
	}
	managerAdminSendNoteAction.insertSelective(json,bizSystems,function(r){
		if(r[0]=="ok"){
			swal(r[1]);
			emailPage.Refresh(2);
			$(".tags").empty();
			$("#sub").val("");
			editor.html("");
			bizSystems=null;
			userids=0;
			names=null;
		}else{
			swal(r[1]);
		}
	});
}

var bizSystems=null;
var userids=0;
var names=null;
/**
 * 整理业务系统和角色id(并区分是否重复添加)
 * root=1 代表添加   root=2 代表移除
 */
EmailPage.prototype.UsernameAndBizSystem=function(name,bizSystem,userid,root){
	if(root==1){
		//添加
		var sear=new RegExp(name);
		if(sear.test(names)){
		    layer.alert("请勿重复添加收件人");
			return;
		}
		
		if(bizSystems==null){
			bizSystems=bizSystem+",";
		}else{
			bizSystems+=bizSystem+",";
		}
		
		if(userids==0){
			userids=userid+",";
		}else{
			userids+=userid+",";
		}
		
		if(names==null){
			names=name+",";
		}else{
			names+=name+",";
		}
		
        var html='<a id='+name+' href=javascript:emailPage.UsernameAndBizSystem("'+name+'","'+bizSystem+'",'+userid+',2);>'+name+'<i class="fa fa-close"></i></a>'
        $('.tags').append(html);
	}else if(root==2){
	    $("#"+name).remove();
		//移除
	    var a=name+",";
	    var b=bizSystem+",";
	    var c=userid+",";
		names=names.replace(a,"");
		bizSystems=bizSystems.replace(b,"");
		userids=userids.replace(c,"");
	}
}

/**
 * 加载所有业务系统
 */
EmailPage.prototype.getAllBizSystem=function(){
	managerAdminSendNoteAction.getAllBizSystem(function(r){
		if(r[0]=="ok"){
			$("#bizSystem").empty();
			var option="<option value>所有系统</option>";
			$(r[1]).each(function(i,idx){
				option+="<option value="+idx.bizSystem+">"+idx.bizSystemName+"</option>";
			});
			$("#bizSystem").append(option);
		}else{
			swal(r[1]);
		}
	});
}

/**
 * 加载管理员
 */
EmailPage.prototype.getAllAdminByQuery=function(){
	var json={};
	json.bizSystem=$("#bizSystem").val();
	json.state=$("#state").val();
	managerAdminSendNoteAction.getAllAdminByQuery(json,function(r){
		if(r[0]=="ok"){
			$("#adminlist").empty();
			$(r[1]).each(function(i,idx){
				var li="<li><a href=javascript:emailPage.UsernameAndBizSystem('"+idx.username+"','"+idx.bizSystem+"',"+idx.id+",1);><i class='fa fa-user-plus'></i>"+idx.username+"</a></li>";
				$("#adminlist").append(li);
			});
		}else{
			swal(r[1]);
		}
	});
}

/**
 * 查询信件详情
 */
EmailPage.prototype.editNote=function(Noteid,type)
{
	  layer.open({
           type: 2,
           title: '信件详情',
           maxmin: false,
           shadeClose: true,
           //点击遮罩关闭层
           area: ['800px', '80%'],
          // area: ['60%', '45%'],
           content: contextPath + "/managerzaizst/email/mail_detail.html?id="+Noteid+"&note="+type,
           cancel: function(index){ 
        	   emailPage.Refresh(type=="send"?2:1);
        	   parent.indexPage.getRecvNote(1);
           }
       });
}

var del=true;
/**
 * 删除邮件
 */
EmailPage.prototype.delEmail=function(data,type){
	del=false;
	swal({
        title: "您确定要删除这条信息吗?",
        text: "请谨慎操作！",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        cancelButtonText: "取消",
        confirmButtonText: "删除",
        closeOnConfirm: false
    },
    function(isconfirm) {
    	if(isconfirm){
    		if(type=="recv"){
    			managerAdminRecvNoteAction.deleteByPrimaryKey(data,function(r){
    				if(r[0]=="ok"){
    					swal(r[1]);
    					emailPage.Refresh(1);
    				}else{
    					swal(r[1]);
    				}
    			});
    		}else if(type=="send"){
    			managerAdminSendNoteAction.deleteByPrimaryKey(data,function(r){
    				if(r[0]=="ok"){
    					swal(r[1]);
    					emailPage.Refresh(2);
    				}else{
    					swal(r[1]);
    				}
    			});
    		}
    	}
    	del=true;
    });
}

/**
 * 选取所有用户
 */
EmailPage.prototype.AllRecvNote=function(){
	$("#adminlist a i").click();
}

/**
 * 刷新表格
 */
EmailPage.prototype.Refresh=function(i){
	if(i==1){
		emailPage.queryParam.sub=$("#sub").val();
		emailPage.tableRecvNote.ajax.reload();
	}else{
		emailPage.queryParam.sub=$("#sendsub").val();
		emailPage.tableSendNote.ajax.reload();
	}
}