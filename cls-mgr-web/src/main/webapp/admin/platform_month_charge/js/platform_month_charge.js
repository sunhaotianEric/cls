function PlatformMonthChargePage(){}
var platformMonthChargePage = new PlatformMonthChargePage();

platformMonthChargePage.param = {
   	
};
platformMonthChargePage.sumData ={}
/**
 * 查询参数
 */
platformMonthChargePage.queryParam = {
		bizSystemName : null,
		enable : null
};

//分页参数
platformMonthChargePage.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize
};


$(document).ready(function() {
	
	var lastMonthDate = new Date(); //上月日期
	lastMonthDate.setDate(1);
	lastMonthDate.setMonth(lastMonthDate.getMonth()-1);
	$("#belongDateEnd").val(dateFormat(lastMonthDate,'yyyy-MM'));
	lastMonthDate.setMonth(0);
	$("#belongDateStart").val(dateFormat(lastMonthDate,'yyyy-MM'));
	platformMonthChargePage.initTableData(); //初始化查询数据
});



/**
 * 初始化列表数据
 */
PlatformMonthChargePage.prototype.initTableData = function() {
	platformMonthChargePage.queryParam = getFormObj($("#queryForm"));
	platformMonthChargePage.queryParam.belongDateStart = ($("#belongDateStart").val()+"-01").stringToDate();
	platformMonthChargePage.queryParam.belongDateEnd = ($("#belongDateEnd").val()+"-25").stringToDate();
	platformMonthChargePage.table = $('#platformMonthChargeTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"scrollX": true,
		"iDisplayLength" : defaultPageSize,
		"fnDrawCallback": function (oSettings) {  
			platformMonthChargePage.refreshBizSystemPage(platformMonthChargePage.sumData);
	     },
		"ajax":function (data, callback, settings) {
			platformMonthChargePage.pageParam.pageSize = data.length;//页面显示记录条数
			platformMonthChargePage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerPlatformMonthChargeAction.queryPlatformMonthChargePage(platformMonthChargePage.queryParam,platformMonthChargePage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
					platformMonthChargePage.sumData = r[2];
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
					{"data": "belongDateStr"}, 
		            {"data": "maintainMoney"},
		            {"data": "bonusMoney"},
		            {"data": "laborMoney"},
		            {"data": "equipmentMoney"},
		            {"data": "gain"}
	            ]
	}).api(); 
}



/**
 * 查询数据
 */
PlatformMonthChargePage.prototype.find = function(){
	platformMonthChargePage.queryParam.belongDateStart = ($("#belongDateStart").val()+"-01").stringToDate();
	platformMonthChargePage.queryParam.belongDateEnd = ($("#belongDateEnd").val()+"-25").stringToDate();
	if(platformMonthChargePage.queryParam.belongDateStart>platformMonthChargePage.queryParam.belongDateEnd){
		swal("开始月份要小于结束月份！");
		return ;
	}
	platformMonthChargePage.table.ajax.reload();
};

/**
 * 刷新列表数据
 */
PlatformMonthChargePage.prototype.refreshBizSystemPage = function(obj){
	if(obj != null){
		var reportListObj = $("#platformMonthChargeTable tbody");
		var str = "";
		str += "<tr style='color:red'>";
	    str += "  <td>总计</td>";
	    str += "  <td>"+ obj.maintainMoney.toFixed(2)+"</td>";
	    str += "  <td>"+ obj.bonusMoney.toFixed(2)+"</td>";
	    str += "  <td>"+ obj.laborMoney.toFixed(2)+"</td>";
	    str += "  <td>"+ obj.equipmentMoney.toFixed(2)+"</td>";
	    str += "  <td>"+ obj.gain.toFixed(2)+"</td>";
	    str += "</tr>"
	    reportListObj.append(str);
	}
	
}


