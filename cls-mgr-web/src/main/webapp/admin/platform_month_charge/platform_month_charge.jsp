<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>  
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>平台收入总览</title>
        <link rel="shortcut icon" href="favicon.ico">
        <jsp:include page="/admin/include/include.jsp"></jsp:include>
        <script  type="text/javascript" src="<%=path%>/resources/My97DatePicker/WdatePicker.js"></script>
</head>

   <body>
        <div class="animated fadeIn">
            <div class="ibox-content">
                 <div class="row m-b-sm m-t-sm">
                    <form class="form-horizontal" id="queryForm">
                    <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                        <div class="row">
                        	
                           <div class="col-sm-4">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">归属月份</span>
                                    <div class="input-daterange input-group" >
                                        <input class="form-control layer-date" placeholder="开始月份" id="belongDateStart" style="background-color: white;" readonly="readonly"  onclick="WdatePicker({dateFmt:'yyyy-MM'})">                                   
                                     <span class="input-group-addon">到</span>                                   
                                        <input class="form-control layer-date" placeholder="结束月份" id="belongDateEnd" style="background-color: white;" readonly="readonly" onclick="WdatePicker({dateFmt:'yyyy-MM'})">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group nomargin text-left">
                                    <button type="button" class="btn btn-outline btn-default btn-js" onclick="platformMonthChargePage.find()"><i class="fa fa-search"></i>&nbsp;查 询</button>
                                </div>
                            </div>
                         </div>
                    </c:if>
                    </form>
                    </div>
                     <div class="jqGrid_wrapper">
                        <div class="ui-jqgrid ">
                            <div class="ui-jqgrid-view">
                                <div class="ui-jqgrid-hdiv">
                                    <div class="ui-jqgrid-hbox" style="padding-right:0">
                                        <table id="platformMonthChargeTable" class="ui-jqgrid-htable ui-common-table table table-bordered">
                                            <thead>
                                                <tr class="ui-jqgrid-labels">
                                                    <th class="ui-th-column ui-th-ltr">归属月份</th>
                                                    <th class="ui-th-column ui-th-ltr">总维护费</th>
													<th class="ui-th-column ui-th-ltr">总分红费</th>
													<th class="ui-th-column ui-th-ltr">总工资支出</th>
													<th class="ui-th-column ui-th-ltr">总服务器支出</th>
													<th class="ui-th-column ui-th-ltr">盈利</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>     
		         </div>
		      </div>
  <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/platform_month_charge/js/platform_month_charge.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerPlatformMonthChargeAction.js'></script>  
<script type="text/javascript">
function dateChoose(date){
	alert(date);
	
}
</script>

</body>
</html>

