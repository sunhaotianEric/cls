function LhcIssueConfigEditPage(){}
var lhcIssueConfigEditPage = new LhcIssueConfigEditPage();

lhcIssueConfigEditPage.param = {
   	
};

$(document).ready(function() {
	lhcIssueConfigEditPage.getLhcIssueConfig();
});

/**
 * 赋值配置信息
 */
LhcIssueConfigEditPage.prototype.getLhcIssueConfig = function(){
	managerLhcIssueConfigAction.getLhcIssueConfig(function(r){
		if (r[0] != null && r[0] == "ok") {
			var lastEndTimes = r[1].lastEndTime.getTime();
			var nextEndTimes = r[1].nextEndTime.getTime();
			var lastEndTime = new Date(lastEndTimes).format("yyyy-MM-dd hh:mm:ss");
			var nextEndTime = new Date(nextEndTimes).format("yyyy-MM-dd hh:mm:ss");
			$("#configId").val(r[1].id);
			$("#lastEndTimeId").val(lastEndTime);
			$("#nextEndTimeId").val(nextEndTime);
			$("#lastExpectId").val(r[1].lastExpect);
			$("#nextExpectId").val(r[1].nextExpect);
			$("#isCloseId").val(r[1].isClose);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取信息失败.");
		}
    });
};

/**
 * 修改默认值
 * @param map
 * @return
 */
LhcIssueConfigEditPage.prototype.setLhcIssueConfig = function(){
	var id = $("#configId").val();
	var lastEndTime = new Date($("#lastEndTimeId").val());
	var nextEndTime = new Date($("#nextEndTimeId").val());
	var isClose = $("#isCloseId").val();
	var lastExpect = $("#lastExpectId").val();
	var nextExpect = $("#nextExpectId").val();
	
	var lhcIssueConfig = {};
	lhcIssueConfig.id = id;
	lhcIssueConfig.lastEndTime = lastEndTime;
	lhcIssueConfig.nextEndTime = nextEndTime;
	lhcIssueConfig.isClose = isClose;
	lhcIssueConfig.lastExpect = lastExpect;
	lhcIssueConfig.nextExpect = nextExpect;
	managerLhcIssueConfigAction.updateLhcIssueConfig(lhcIssueConfig,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({title: "保存成功",text: "您已经保存了这条信息。",type: "success"});
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("保存信息失败.");
		}
    });
}

