<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
  <link rel="shortcut icon" href="favicon.ico">
  <jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeIn">
      <div class="tabs-container ibox">
          <ul class="nav nav-tabs">
             <li class="active">
                  <a data-toggle="tab" href="#tab-8" aria-expanded="false">六合期号设置</a></li> 
          </ul>
          <div class="tab-content">
            <div id="tab-8" class="tab-pane active">
                <div class="panel-body">
                    <form class="form-horizontal">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                	<input id="configId" name="" type="hidden"  />
                                    <label class="col-sm-3 control-label">上期截止投注时间:</label>
                                    <div class="col-sm-8">
                                        <input class="laydate-icon form-control layer-date" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" id="lastEndTimeId"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">下期截止投注时间:</label>
                                    <div class="col-sm-8">
                                        <input class="laydate-icon form-control layer-date" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" id="nextEndTimeId"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">是否封盘:</label>
                                    <div class="col-sm-5">
                                        <select class="ipt" id="isCloseId">
	                                         <option value="1" selected="selected">是</option>
	                                         <option value="0" selected="selected">否</option>
                                         </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">上期期号:</label>
                                    <div class="col-sm-8">
                                        <input type="text" value="" class="form-control" id="lastExpectId"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">下期期号:</label>
                                    <div class="col-sm-8">
                                        <input type="text" value="" class="form-control" id="nextExpectId"></div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
              </div>
        </div>
    </div>
    <div class="col-sm-12 ibox">
        <div class="row">
            <div class="col-sm-6 text-center">
                <button type="button" class="btn btn-w-m btn-white" onclick="lhcIssueConfigEditPage.setLhcIssueConfig()">保 存</button></div>
          <!--    <div class="col-sm-6 text-center">
                <button type="button" class="btn btn-w-m btn-white" onclick="lhcIssueConfigEditPage.refreshCache()">一键更新缓存</button></div>  -->
        </div>
    </div>
</div>

    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/lhc_issue_config/js/lhcissueconfig_edit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerLhcIssueConfigAction.js'></script>    
</body>
</html>