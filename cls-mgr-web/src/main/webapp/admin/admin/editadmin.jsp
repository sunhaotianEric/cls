<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>  
    <head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>管理员修改</title>
    <link rel="shortcut icon" href="favicon.ico">
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
 
    <%
	 String id = request.getParameter("id");
	%>
</head>
<body class="gray-bg">
        <div class="wrapper wrapper-content animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
            <!--          <div class="ibox ">
                        <div class="ibox-content"> -->
                            <form class="form-horizontal" id="adminForm">
                                <c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
                                    <input type="hidden" id="bizSystem" name="bizSystem" value="${admin.bizSystem}"/>
                                </c:if>
                                <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                                   <div class="input-group m-b">
                                    <span class="input-group-addon">商户</span>
                                    <cls:bizSel name="bizSystem" id="bizSystem" emptyOption="false" options="class:'ipt form-control'"/>
                                  </div>
                                </c:if>
                                <% if(id != null && !"".equals(id)){%>
                                <div class="input-group m-b">
                                    <input type="hidden" id="id" name="id"/>
                                    <span class="input-group-addon" >管理员账号${handleType}</span>
                                    <input id="username" name="username" type="text" value="" class="form-control" disabled="disabled">
                                </div>
                                <%} %>
                               <% if(id == null || "".equals(id)){%>
                                <div class="input-group m-b">
                                    <input type="hidden" id="id" name="id"/>
                                    <span class="input-group-addon" >管理员账号${handleType}</span>
                                    <input id="username" name="username" type="text" value="" class="form-control">
                                </div>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">管理员登陆密码</span>
                                    <input id="password" name="password" type="password" value="" class="form-control">
                                 </div>
 
                                <div class="input-group m-b">
                                    <span class="input-group-addon">管理员操作密码</span>
                                    <input id="operatePassword" name="operatePassword" type="password" value="" class="form-control">
                                 </div>
                                 <%} %>
 
                                <div class="input-group m-b">
                                    <span class="input-group-addon">管理员权限</span>
                                    <select id="state" name="state" class="ipt form-control">
					   			        <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
										<option value="0">超级管理员</option>
										</c:if>
										<c:if test="${admin.state==0}">
										<option value="1">管理员</option>
										</c:if>
										<option value="2">财务管理员</option>
										<option value="3">客服管理员</option>
                                     </select>
                                </div>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">用户数据权限</span>
                                    <select id="dataViewSwitch" name="dataViewSwitch" class="ipt form-control">
										<option value="1">可查看</option>
										<option value="0">不可查看</option>
                                     </select>
                                 </div>
                        
                                <div class="input-group m-b">
                                    <span class="input-group-addon">当前登陆账号操作密码</span>
                                    <input id="currentOperatePasswordAdd" name="currentOperatePasswordAdd" type="password" value="" class="form-control"></div>
                     
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-12 text-center">
                                            <button type="button" class="btn btn-w-m btn-white" onclick="editAdminPage.saveData()">提 交</button>
                                            &nbsp;&nbsp; <input type="reset" class="btn btn-w-m btn-white" value="重 置"/>
                                            </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                 <!--    </div>
                </div>  -->
            </div>
        </div>
        <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/admin/js/editadmin.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerAdminAction.js'></script>
    <script type="text/javascript">
	editAdminPage.param.id = <%=id%>;
	</script>
</body>
</html>

