<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%@ page import="com.team.lottery.util.ConstantUtil,com.team.lottery.vo.Admin" %>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
    Admin admin = (Admin)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_ADMIN);
%>
<!DOCTYPE html>
<html>  
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <title>管理员设置</title>
          <link rel="shortcut icon" href="favicon.ico">
         <jsp:include page="/admin/include/include.jsp"></jsp:include>
   
</head>
    <body>
        <div class="animated fadeIn">
            <div class="ibox-content">
                <div class="row m-b-sm m-t-sm">
                <form action="javascript:void(0)" class="form-horizontal" id="queryForm">
                        <div class="row">
                           <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                            <div class="col-sm-3">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">商户</span>
                                    <cls:bizSel name="bizSystem" id="bizSystem" options="class:'ipt form-control'"/>                 
                                </div>
                            </div>
                           <div class="col-sm-2">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">管理员账号</span>
                                    <input id="username" name="username" type="text" value="" class="form-control">
                                 </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">管理员权限</span>
                                    <select class="ipt form-control" id="state" name="state">
                                       <option value="" selected="selected">==请选择==</option>
										<option value="0">超级管理员</option>
										<option value="1">管理员</option>
										<option value="2">财务管理员</option>
										<option value="3">客服管理员</option>
                                     </select>
                                </div>
                            </div>
                       
                            <div class="col-sm-2">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">在线状态</span>
                                    <select class="ipt form-control" id="isOnline" name="isOnline">
			                           <option value="" selected="selected">==请选择==</option>	
									   <option value="1">当前在线</option>
									   <option value="0">未在线</option>
                                     </select>
                                </div>
                            </div>
                            <div class="col-sm-2">
                           
                                <div class="form-group nomargin text-right">
                                    <button type="button" class="btn btn-outline btn-default btn-kj" onclick="adminPage.getAllAdmins()">
                                        <i class="fa fa-search"></i>&nbsp;查 询</button>&nbsp;&nbsp;
                                    <button type="button" class="btn btn-outline btn-default btn-add" onclick="adminPage.addAdmin()">
                                        <i class="fa fa-plus" ></i>&nbsp;新增</button></div>
                            </div>
                            </c:if>
                            <c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
                          
	                           <div class="col-sm-3">
	                                <div class="input-group m-b">
	                                    <span class="input-group-addon">管理员账号</span>
	                                    <input id="username" name="username" type="text" value="" class="form-control">
	                                 </div>
	                            </div>
	                            <div class="col-sm-3">
	                                <div class="input-group m-b">
	                                    <span class="input-group-addon">管理员权限</span>
	                                    <select class="ipt form-control" id="state" name="state">
	                                       <option value="" selected="selected">==请选择==</option>
											<option value="0">超级管理员</option>
											<option value="1">管理员</option>
											<option value="2">财务管理员</option>
											<option value="3">客服管理员</option>
	                                     </select>
	                                 </div>
	                              </div>
		                           <div class="col-sm-3">
		                                <div class="input-group m-b">
		                                    <span class="input-group-addon">在线状态</span>
		                                    <select class="ipt form-control" id="isOnline" name="isOnline">
					                           <option value="" selected="selected">==请选择==</option>	
											   <option value="1">当前在线</option>
											   <option value="0">未在线</option>
		                                     </select>
		                                </div>
		                            </div>
	                             <div class="col-sm-3">
	                           
	                                <div class="form-group text-right">
	                                    <button type="button" class="btn btn-outline btn-default btn-kj" onclick="adminPage.getAllAdmins()">
	                                        <i class="fa fa-search"></i>&nbsp;查 询</button>&nbsp;&nbsp;
	                                    <button type="button" class="btn btn-outline btn-default btn-add" onclick="adminPage.addAdmin('add')">
	                                        <i class="fa fa-plus"></i>&nbsp;新增</button>
	                                </div>
	                            </div>
                            </c:if>
                        </div>
                    </form>
                </div>
              
                    <div class="jqGrid_wrapper">
                        <div class="ui-jqgrid ">
                            <div class="ui-jqgrid-view">
                                <div class="ui-jqgrid-hdiv">
                                    <div class="ui-jqgrid-hbox" style="padding-right:0">
                                        <table id="adminTable" class="ui-jqgrid-htable ui-common-table table table-bordered">
                                            <thead>
                                                <tr class="ui-jqgrid-labels">
                                                    <th class="ui-th-column ui-th-ltr">序号</th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">管理员账号</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">管理员权限</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">商户</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">创建时间</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">在线状态</div></th>
                                                    <th class="ui-th-column ui-th-ltr">
                                                        <div class="ui-jqgrid-sortable">操作</div></th>
                                                  </tr>
                                            </thead>
                                            <tbody id="adminsList">    
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
     
     <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/admin/js/admin.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerAdminAction.js'></script> 
</body>
</html>

