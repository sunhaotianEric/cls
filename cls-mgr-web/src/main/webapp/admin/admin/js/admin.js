function AdminPage(){}
var adminPage = new AdminPage();


adminPage.queryParam = {
		
};

//分页参数
adminPage.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize
};

$(document).ready(function() {
	//adminPage.getAllAdmins(); //查询所有的管理员数据
	adminPage.initTableData();
	/**
	 * 添加超级系统
	 */
	$("#bizSystem").append("<option value='SUPER_SYSTEM'>超级系统</option>"); 
});



/**
 * 初始化列表数据
 */
AdminPage.prototype.initTableData = function() {
	adminPage.queryParam = getFormObj($("#queryForm"));
	adminPage.table = $('#adminTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"scrollX": true,
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			adminPage.pageParam.pageSize = data.length;//页面显示记录条数
			adminPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerAdminAction.getAllAdmin(adminPage.queryParam,adminPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": null
		            },
		            {"data": "username"},
		            {"data": "stateStr"},
		            {"data": "bizSystemName","bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false,
			            	  render: function(data, type, row, meta) {
			            		  if(row.bizSystem == "SUPER_SYSTEM"){
			            			  return "超级系统";
			            			}else{
			            			   return data;
			            			}
			            		  return data;
			            		}
		            },
		            {"data": "createDateStr"},
		            {"data": "isOnline",
		            	  render: function(data, type, row, meta) {
		            		  if(data != null && data == 1){
		            			  return "当前在线";
		            			}else{
		            			   return "未在线";
		            			}
		            		  return data;
		            		}
		            },
		            {
	                	 "data": "id",
	                	 render: function(data, type, row, meta) {
	                		 var str='';
	                		//超级系统 超级管理员
	                		 if(currentUser.bizSystem == "SUPER_SYSTEM" && currentUser.state==0){
	              				str+="<a title='登陆密码设置' class='btn btn-info btn-sm btn-pwd' onclick='adminPage.editAdminLoginPass("+ row.id +")'><i class='fa fa-key'></i></a>&nbsp;";
                				str+="<a title='操作密码设置' class='btn btn-danger btn-sm btn-aqpwd' onclick='adminPage.editAdminHandlePass("+ row.id +")'><i class='fa fa-key'></i></a>&nbsp;";
                				if(currentUser.username!=row.username){
	                			    str += "<a title='编辑' class='btn btn-info btn-sm btn-bj' onclick='adminPage.editAdmin("+ row.id +")'><i class='fa fa-pencil'></i></a>&nbsp;";
	                			    str += "<a title='强制退出' class='btn btn-warning btn-sm btn-qz' onclick='adminPage.logoutAdmin(\""+row.bizSystem+"\",\""+ row.username +"\")'><i class='fa fa-reply-all'></i></a>&nbsp;";
	                			    str += "<a  title='删除'   class='btn btn-danger btn-sm btn-del' onclick='adminPage.delAdmin("+ row.id +")'><i class='fa fa-trash'></i></a>&nbsp;";
                				    if(row.loginLock == 0){
		                				 str += "<a title='锁定' class='btn btn-success btn-sm btn-sd' onclick='adminPage.loginAuthorityOff("+ row.id +")'><i class='fa fa-lock'></i></a>&nbsp;";
		              
		                			   }else{
		                				 str += "<a title='解锁' class='btn btn-success btn-sm btn-sd' onclick='adminPage.loginAuthorityOn("+ row.id +")'><i class='fa fa-unlock'></i></a>";
		                			  }
                				}
                				//超级系统 普通管理员
	                		 }else if(currentUser.bizSystem == "SUPER_SYSTEM" && currentUser.state!=0){
									if(row.bizSystem != "SUPER_SYSTEM"){
										str+="<a title='登陆密码设置' class='btn btn-info btn-sm btn-pwd' onclick='adminPage.editAdminLoginPass("+ row.id +")'><i class='fa fa-key'></i></a>&nbsp;";
										str+="<a title='操作密码设置' class='btn btn-danger btn-sm btn-aqpwd' onclick='adminPage.editAdminHandlePass("+ row.id +")'><i class='fa fa-key'></i></a>&nbsp;";
										if(currentUser.username!=row.username){
											str += "<a title='编辑' class='btn btn-info btn-sm btn-bj' onclick='adminPage.editAdmin("+ row.id +")'><i class='fa fa-pencil'></i></a>&nbsp;";
											str += "<a title='强制退出' class='btn btn-warning btn-sm btn-qz' onclick='adminPage.logoutAdmin(\""+row.bizSystem+"\",\""+ row.username +"\")'><i class='fa fa-reply-all'></i></a>&nbsp;";
											str += "<a  title='删除'   class='btn btn-danger btn-sm btn-del' onclick='adminPage.delAdmin("+ row.id +")'><i class='fa fa-trash'></i></a>&nbsp;";
											if(row.loginLock == 0){
												str += "<a title='锁定' class='btn btn-success btn-sm btn-sd' onclick='adminPage.loginAuthorityOff("+ row.id +")'><i class='fa fa-lock'></i></a>&nbsp;";

											}else{
												str += "<a title='解锁' class='btn btn-success btn-sm btn-sd' onclick='adminPage.loginAuthorityOn("+ row.id +")'><i class='fa fa-unlock'></i></a>";
											}
										}
									}else if(currentUser.username == row.username){
										str+="<a title='登陆密码设置' class='btn btn-info btn-sm btn-pwd' onclick='adminPage.editAdminLoginPass("+ row.id +")'><i class='fa fa-key'></i></a>&nbsp;";
										str+="<a title='操作密码设置' class='btn btn-danger btn-sm btn-aqpwd' onclick='adminPage.editAdminHandlePass("+ row.id +")'><i class='fa fa-key'></i></a>&nbsp;";
									}
									//子系统 超级管理员
							 }else if(currentUser.bizSystem != "SUPER_SYSTEM" && currentUser.state==0){
									 str+="<a title='登陆密码设置' class='btn btn-info btn-sm btn-pwd' onclick='adminPage.editAdminLoginPass("+ row.id +")'><i class='fa fa-key'></i></a>&nbsp;";
									 str+="<a title='操作密码设置' class='btn btn-danger btn-sm btn-aqpwd' onclick='adminPage.editAdminHandlePass("+ row.id +")'><i class='fa fa-key'></i></a>&nbsp;";
									 if(currentUser.username!=row.username){
										 str += "<a title='编辑' class='btn btn-info btn-sm btn-bj' onclick='adminPage.editAdmin("+ row.id +")'><i class='fa fa-pencil'></i></a>&nbsp;";
										 str += "<a title='强制退出' class='btn btn-warning btn-sm btn-qz' onclick='adminPage.logoutAdmin(\""+row.bizSystem+"\",\""+ row.username +"\")'><i class='fa fa-reply-all'></i></a>&nbsp;";
										 str += "<a  title='删除'   class='btn btn-danger btn-sm btn-del' onclick='adminPage.delAdmin("+ row.id +")'><i class='fa fa-trash'></i></a>&nbsp;";
										 if(row.loginLock == 0){
											 str += "<a title='锁定' class='btn btn-success btn-sm btn-sd' onclick='adminPage.loginAuthorityOff("+ row.id +")'><i class='fa fa-lock'></i></a>&nbsp;";

										 }else{
											 str += "<a title='解锁' class='btn btn-success btn-sm btn-sd' onclick='adminPage.loginAuthorityOn("+ row.id +")'><i class='fa fa-unlock'></i></a>";
										 }
									 }
							 }else{
								 if(currentUser.username==row.username){
									 str+="<a title='登陆密码设置' class='btn btn-info btn-sm btn-pwd' onclick='adminPage.editAdminLoginPass("+ row.id +")'><i class='fa fa-key'></i></a>&nbsp;";
									 str+="<a title='操作密码设置' class='btn btn-danger btn-sm btn-aqpwd' onclick='adminPage.editAdminHandlePass("+ row.id +")'><i class='fa fa-key'></i></a>&nbsp;";
								 }
							 }
	                		 return str;
	                	 }
	                }
	            ]
	}).api(); 
}




/**
 * 加载所有的管理员数据
 */
AdminPage.prototype.getAllAdmins = function(){
	adminPage.queryParam = getFormObj($("#queryForm"));
	adminPage.table.ajax.reload();
};


/**
 * 添加管理员
 */
AdminPage.prototype.addAdmin = function(){
	  layer.open({
          type: 2,
          title: '管理员新增',
          maxmin: false,
          shadeClose: true,
          //点击遮罩关闭层
          area: ['600px', '410px'],
         // area: ['45%', '52%'],
          content: contextPath + "/managerzaizst/admin/editadmin.html",
          cancel: function(index){ 
        	  adminPage.getAllAdmins();
       	   }
      });
};


/**
 * 修改管理员
 */
AdminPage.prototype.editAdmin = function(id){
	  layer.open({
          type: 2,
          title: '管理员编辑',
          maxmin: false,
          shadeClose: true,
          //点击遮罩关闭层
          area: ['600px', '410px'],
         // area: ['45%', '52%'],
          content: contextPath + "/managerzaizst/admin/"+id+"/editadmin.html",
          cancel: function(index){ 
        	  adminPage.getAllAdmins();
       	   }
      });
};

/**
 * 修改管理员
 */
AdminPage.prototype.editAdminLoginPass = function(id){
	  layer.open({
          type: 2,
          title: '管理员登陆密码编辑',
          maxmin: false,
          shadeClose: true,
          //点击遮罩关闭层
          area: ['600px', '410px'],
         // area: ['45%', '52%'],
          content: contextPath + "/managerzaizst/admin/"+id+"/editadminloginpass.html",
          cancel: function(index){ 
        	  adminPage.getAllAdmins();
       	   }
      });
};

/**
 * 修改管理员
 */
AdminPage.prototype.editAdminHandlePass = function(id){
	  layer.open({
          type: 2,
          title: '管理员操作密码编辑',
          maxmin: false,
          shadeClose: true,
          //点击遮罩关闭层
          area: ['600px', '410px'],
         // area: ['45%', '52%'],
          content: contextPath + "/managerzaizst/admin/"+id+"/editadminhandlepass.html",
          cancel: function(index){ 
        	  adminPage.getAllAdmins();
       	   }
      });
};



/**
 * 删除管理员
 */
AdminPage.prototype.delAdmin = function(id){
	 swal({
         title: "您确定要删除这条信息吗",
         text: "请谨慎操作！",
         type: "warning",
         showCancelButton: true,
         confirmButtonColor: "#DD6B55",
         cancelButtonText: "取消",
         confirmButtonText: "删除",
         closeOnConfirm: false
     },
     function() {
		managerAdminAction.delAdmin(id,function(r){
			if (r[0] != null && r[0] == "ok") {
   				swal({ title: "提示", text: "删除成功",type: "success"});
   				adminPage.getAllAdmins();
   			}else if(r[0] != null && r[0] == "error"){
   				swal(r[1]);
   			}else{
   				swal("删除信息失败.");
   			}
	    });
	});
};

/**
 * 强制退出管理员
 */
AdminPage.prototype.logoutAdmin = function(bizSystem,userName){
	 swal({
         title: "确认要退出该管理员？",
         text: "请谨慎操作！",
         type: "warning",
         showCancelButton: true,
         confirmButtonColor: "#DD6B55",
         cancelButtonText: "取消",
         confirmButtonText: "确定",
         closeOnConfirm: false
       },
     function() {
		managerAdminAction.logoutAdmin(bizSystem,userName,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "操作成功",type: "success"});
				adminPage.getAllAdmins();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("退出该管理员失败.");
			}
	    });
	});
};

/**
 * 控制登录权限锁定
 */
AdminPage.prototype.loginAuthorityOff = function(id){
	 swal({
         title: "确认要锁定该管理员？",
         text: "请谨慎操作！",
         type: "warning",
         showCancelButton: true,
         confirmButtonColor: "#DD6B55",
         cancelButtonText: "取消",
         confirmButtonText: "确定",
         closeOnConfirm: false
       },
     function() {
		managerAdminAction.loginAuthorityOff(id,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "操作成功",type: "success"});
				adminPage.getAllAdmins();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("该管理员锁定失败.");
			}
	    });
	});
};


/**
 * 控制登录权限解锁
 */
AdminPage.prototype.loginAuthorityOn = function(id){
	 swal({
         title: "确认要为该管理员解锁？",
         text: "请谨慎操作！",
         type: "warning",
         showCancelButton: true,
         confirmButtonColor: "#DD6B55",
         cancelButtonText: "取消",
         confirmButtonText: "确定",
         closeOnConfirm: false
       },
     function() {
		managerAdminAction.loginAuthorityOn(id, function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "操作成功",type: "success"});
				adminPage.getAllAdmins();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("该管理员锁定失败.");
			}
	    });
	});
};
AdminPage.prototype.closeLayer=function(index){
	layer.close(index);
	adminPage.getAllAdmins();
}