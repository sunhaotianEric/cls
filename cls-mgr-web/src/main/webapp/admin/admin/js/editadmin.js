function EditAdminPage(){}
var editAdminPage = new EditAdminPage();

editAdminPage.param = {
   	
};

$(document).ready(function() {
	var AdminId = editAdminPage.param.id;  //获取查询ID
	if(AdminId != null){
		editAdminPage.editAdmin(AdminId);
	}
	if(currentUser.bizSystem == "SUPER_SYSTEM" && currentUser.state == 0){
		$("#bizSystem").append("<option value='SUPER_SYSTEM'>超级系统</option>");
	}
	
	
});


/**
 * 保存更新信息
 */
EditAdminPage.prototype.saveData = function(){
	
	var admin=getFormObj("#adminForm");
	admin.id = editAdminPage.param.id;
	var currentOperatePassword = $("#currentOperatePasswordAdd").val();
	if(admin.username==null || admin.username.trim()==""){
		swal("请输入管理员账号!");
		$("#username").focus();
		return;
	}
//	if(admin.password==null || admin.password.trim()==""){
//		swal("请输入管理员密码!");
//		$("#password").focus();
//		return;
//	}
//	if(admin.operatePassword==null || admin.operatePassword.trim()==""){
//		swal("请输入管理员操作密码!");
//		$("#operatePassword").focus();
//		return;
//	}
	if(admin.currentOperatePasswordAdd==null || admin.currentOperatePasswordAdd.trim()==""){
		swal("请输入当前登陆账号操作密码!");
		$("#operatePassword").focus();
		return;
	}
	
	managerAdminAction.saveOrUpdateAdmin(admin, currentOperatePassword, function(r){
		if (r[0] != null && r[0] == "ok") {
			   swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.adminPage.closeLayer(index);
		   	    }); 
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("添加管理员失败.");
		}
    });

};

/**
 * 保存更新信息
 */
EditAdminPage.prototype.editLoginPass = function(){
	
	var admin=getFormObj("#adminForm");
	admin.id = editAdminPage.param.id;
	var currentOperatePassword = $("#currentOperatePasswordAdd").val();
	if(admin.username==null || admin.username.trim()==""){
		swal("请输入管理员账号!");
		$("#username").focus();
		return;
	}
	if(admin.password==null || admin.password.trim()==""){
		swal("请输入管理员密码!");
		$("#password").focus();
		return;
	}
//	if(admin.operatePassword==null || admin.operatePassword.trim()==""){
//		swal("请输入管理员操作密码!");
//		$("#operatePassword").focus();
//		return;
//	}
	if(admin.currentOperatePasswordAdd==null || admin.currentOperatePasswordAdd.trim()==""){
		swal("请输入当前登陆账号操作密码!");
		$("#operatePassword").focus();
		return;
	}
	
	managerAdminAction.editAdminToLoginPass(admin, currentOperatePassword, function(r){
		if (r[0] != null && r[0] == "ok") {
			   swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.adminPage.closeLayer(index);
		   	    }); 
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("添加管理员失败.");
		}
    });

};


/**
 * 保存更新信息
 */
EditAdminPage.prototype.editHandlePass = function(){
	
	var admin=getFormObj("#adminForm");
	admin.id = editAdminPage.param.id;
	var currentOperatePassword = $("#currentOperatePasswordAdd").val();
	if(admin.username==null || admin.username.trim()==""){
		swal("请输入管理员账号!");
		$("#username").focus();
		return;
	}
//	if(admin.password==null || admin.password.trim()==""){
//		swal("请输入管理员密码!");
//		$("#password").focus();
//		return;
//	}
	if(admin.operatePassword==null || admin.operatePassword.trim()==""){
		swal("请输入管理员操作密码!");
		$("#operatePassword").focus();
		return;
	}
	if(admin.currentOperatePasswordAdd==null || admin.currentOperatePasswordAdd.trim()==""){
		swal("请输入当前登陆账号操作密码!");
		$("#operatePassword").focus();
		return;
	}
	
	managerAdminAction.editAdminToHandlePass(admin, currentOperatePassword, function(r){
		if (r[0] != null && r[0] == "ok") {
			   swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.adminPage.closeLayer(index);
		   	    }); 
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("添加管理员失败.");
		}
    });

};
/**
 * 赋值信息
 */
EditAdminPage.prototype.editAdmin = function(id){
	managerAdminAction.getAdminById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			$("#id").val(r[1].id);
			$("#username").val(r[1].username);
			$("#state").val(r[1].state);
			$("#dataViewSwitch").val(r[1].dataViewSwitch);
			if(currentUser.bizSystem=='SUPER_SYSTEM')
			{
				$("#bizSystem").val(r[1].bizSystem);
			}
			
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取信息失败.");
		}
    });
};

 
