<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<html>
<head>
    <meta charset="utf-8" />
    <title>微信配置管理</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/weixinconfig/js/weixinconfig_list.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerWeixinConfigAction.js'></script>
</head>
<body>
   <div class="alert alert-info">当前位置<b class="tip"></b>微信管理<b class="tip"></b>微信配置管理</div>
   	
   	<table class="tb">
         <tr>
         	<td> <a href="<%=path %>/managerzaizst/weixinconfig/weixinconfig.html">微信管理</a> &nbsp;&nbsp; <a href="<%=path %>/managerzaizst/weixinconfig/weixinconfig_add.html">添加微信号</a></td>
         </tr>
     </table>
     <!-- <table class="tb">
         <tr>
         	<td>搜索：<input type="text"/>&nbsp;&nbsp;<input type="button" value="开始搜索"/></td>
         </tr>
     </table> -->
   	<table class="tb">
      <tr>
      	  <th>名称</th>
          <th>登录账号</th>
          <th>AppId</th>
          <th>访问域名</th>
          <th>推广用户名</th>
          <th>推广模式</th>
          <th>操作</th>
       </tr>
      <tbody id="weixinConfigList">
      </tbody>
     </table>
    
</body>
</html>

