function EditWeixinConfigPage(){}
var editWeixinConfigPage = new EditWeixinConfigPage();

editWeixinConfigPage.param = {
   	
};

$(document).ready(function() {
	var weixinConfigId = editWeixinConfigPage.param.id;  //获取查询的微信配置ID
	if(weixinConfigId != null){
		editWeixinConfigPage.editWeixinConfig(weixinConfigId);
	}
});


/**
 * 保存网站公告信息
 */
EditWeixinConfigPage.prototype.saveData = function(){
	var id = $("#weixinConfigId").val();
	var name = $("#name").val();
	var loginUserName = $("#loginUserName").val();
	var appId = $("#appId").val();
	var appSecret = $("#appSecret").val();
	var domainUrl = $("#domainUrl").val();
	var extendUserName = $("#extendUserName").val();
	var extendSscRebate = $("#extendSscRebate").val();
	
	if(name==null || name.trim()==""){
		alert("请输入公众号名称!");
		$("#loginUserName").focus();
		return;
	}
	if(loginUserName==null || loginUserName.trim()==""){
		alert("请输入登陆账号!");
		$("#loginUserName").focus();
		return;
	}
	if(appId==null || appId.trim()==""){
		alert("请输入AppId!");
		$("#appId").focus();
		return;
	}
	if(appSecret==null || appSecret.trim()==""){
		alert("请输入appSecret!");
		$("#appSecret").focus();
		return;
	}
	if(domainUrl==null || domainUrl.trim()==""){
		alert("请输入访问域名!");
		$("#domainUrl").focus();
		return;
	}
	if(extendUserName==null || extendUserName.trim()==""){
		alert("请输入推广用户名!");
		$("#extendUserName").focus();
		return;
	}
	if(extendSscRebate==null || extendSscRebate.trim()==""){
		alert("请输入推广模式!");
		$("#extendSscRebate").focus();
		return;
	}
	
	var weixinConfig = {};
	weixinConfig.id = id;
	weixinConfig.name = name;
	weixinConfig.loginUserName = loginUserName;
	weixinConfig.appId = appId;
	weixinConfig.appSecret = appSecret;
	weixinConfig.domainUrl = domainUrl;
	weixinConfig.extendUserName = extendUserName;
	weixinConfig.extendSscRebate = extendSscRebate;
	
	managerWeixinConfigAction.saveOrUpdateWeixinConfig(weixinConfig,function(r){
		if (r[0] != null && r[0] == "ok") {
			alert("保存成功");
			self.location = contextPath + "/managerzaizst/weixinconfig.html";
		}else if(r[0] != null && r[0] == "error"){
			showErrorDlg(jQuery(document.body), r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "保存微信配置数据失败.");
		}
    });
};


/**
 * 赋值网站公告信息
 */
EditWeixinConfigPage.prototype.editWeixinConfig = function(id){
	managerWeixinConfigAction.getWeixinConfigById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			$("#weixinConfigId").val(r[1].id);
			$("#name").val(r[1].name);
			$("#loginUserName").val(r[1].loginUserName);
			$("#appId").val(r[1].appId);
			$("#appSecret").val(r[1].appSecret);
			$("#domainUrl").val(r[1].domainUrl);
			$("#extendUserName").val(r[1].extendUserName);
			$("#extendSscRebate").val(r[1].extendSscRebate);
		}else if(r[0] != null && r[0] == "error"){
			showErrorDlg(jQuery(document.body), r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "获取微信配置数据失败.");
		}
    });
};