function WeixinConfigListPage(){}
var weixinConfigListPage = new WeixinConfigListPage();

weixinConfigListPage.param = {
   	
};

$(document).ready(function() {
	weixinConfigListPage.getAllWeixinConfigs();
});

/**
 * 加载所有的微信配置数据
 */
WeixinConfigListPage.prototype.getAllWeixinConfigs = function(){
	managerWeixinConfigAction.getAllWeixinConfigs(function(r){
		if (r[0] != null && r[0] == "ok") {
			weixinConfigListPage.refreshWeixinConfigListPages(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询网页公告数据失败.");
		}
    });
};

/**
 * 刷新列表数据
 */
WeixinConfigListPage.prototype.refreshWeixinConfigListPages = function(weixinConfigs){
	var weixinConfigListObj = $("#weixinConfigList");
	weixinConfigListObj.html("");
	var str = "";

	for(var i = 0 ; i < weixinConfigs.length; i++){
		var weixinConfig = weixinConfigs[i];
		str += "<tr>";
		str += "  <td>"+ weixinConfig.name +"</td>";
		str += "  <td><input type='hidden' name='announceId' value='"+ weixinConfig.id +"'/>"+ weixinConfig.loginUserName +"</td>";
		str += "  <td>"+ weixinConfig.appId +"</td>";
		str += "  <td>"+ weixinConfig.domainUrl +"</td>";
		str += "  <td>"+ weixinConfig.extendUserName +"</td>";
		str += "  <td>"+ weixinConfig.extendSscRebate +"</td>";
		
		if(weixinConfig.status == 0){
			str += "  <td><a href='javascript:void(0)' onclick='weixinConfigListPage.enableWeixinConfig("+ weixinConfig.id +")'>启用</a>";
		}else{
			str += "  <td><a href='javascript:void(0)' onclick='weixinConfigListPage.unableWeixinConfig("+ weixinConfig.id +")'>停用</a>";
		}
		str += "  &nbsp;|&nbsp;<a href='javascript:void(0)' onclick='weixinConfigListPage.editWeixinConfig("+ weixinConfig.id +")'>编辑</a>&nbsp;|&nbsp;<a href='javascript:void(0)' onclick='weixinConfigListPage.delWeixinConfig("+ weixinConfig.id +")'>删除</a>";
		str += "  &nbsp;|&nbsp;<a href='javascript:void(0)' onclick='weixinConfigListPage.generateNewMenu("+ weixinConfig.id +")'>生成新菜单</a></td>&nbsp;";
		/*str += "  &nbsp;|&nbsp;<a href='javascript:void(0)' onclick='weixinConfigListPage.generateNewMenu(\""+ weixinConfig.name +"\")'>生成新菜单</a></td>&nbsp;";*/
		str += "</tr>";
		
		weixinConfigListObj.append(str);
		str = "";
	}
};

/**
 * 删除微信配置数据
 */
WeixinConfigListPage.prototype.delWeixinConfig = function(id){
	if (confirm("确认要删除该微信号？")){
		managerWeixinConfigAction.delWeixinConfig(id,function(r){
			if (r[0] != null && r[0] == "ok") {
				alert("删除成功");
				weixinConfigListPage.getAllWeixinConfigs();
			}else if(r[0] != null && r[0] == "error"){
				showErrorDlg(jQuery(document.body), r[1]);
			}else{
				showErrorDlg(jQuery(document.body), "删除网站公告信息失败.");
			}
	    });
	}
};

/**
 * 编辑微信配置数据
 */
WeixinConfigListPage.prototype.editWeixinConfig = function(id){
	self.location = contextPath + "/managerzaizst/weixinconfig/" + id+"/weixinconfig_edit.html";
};

/**
 * 启用微信配置数据GenerateNewMenu
 */
WeixinConfigListPage.prototype.enableWeixinConfig = function(id){
	if (confirm("确认是否启用该微信号？")){
		managerWeixinConfigAction.enableWeixinConfig(id,function(r){
			if (r[0] != null && r[0] == "ok") {
				alert("启用成功");
				weixinConfigListPage.getAllWeixinConfigs();
			}else if(r[0] != null && r[0] == "error"){
				showErrorDlg(jQuery(document.body), r[1]);
			}else{
				showErrorDlg(jQuery(document.body), "操作失败.");
			}
	    });
	}
};

/**
 * 停用微信配置数据
 */
WeixinConfigListPage.prototype.unableWeixinConfig = function(id){
	if (confirm("确认是否停用该微信号？")){
		managerWeixinConfigAction.unableWeixinConfig(id,function(r){
			if (r[0] != null && r[0] == "ok") {
				alert("停用成功");
				weixinConfigListPage.getAllWeixinConfigs();
			}else if(r[0] != null && r[0] == "error"){
				showErrorDlg(jQuery(document.body), r[1]);
			}else{
				showErrorDlg(jQuery(document.body), "操作失败.");
			}
	    });
	}
};

/**
 * 生成新菜单NewMenu
 */
WeixinConfigListPage.prototype.generateNewMenu = function(weixinId){

	if (confirm("确认生成该微信号的新菜单？")){
		managerWeixinConfigAction.newWeixinMenu(weixinId,function(r){
			if (r[0] != null && r[0] == "ok") {
				alert("已通知前端生成菜单！");
				weixinConfigListPage.getAllWeixinConfigs();
			}else if(r[0] != null && r[0] == "error"){
				showErrorDlg(jQuery(document.body), r[1]);
			}else{
				showErrorDlg(jQuery(document.body), "生成操作失败.");
			}
	    });
	}
};

