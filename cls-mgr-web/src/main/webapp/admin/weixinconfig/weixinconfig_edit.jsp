<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<html>
<head>
    <meta charset="utf-8" />
    <title>微信公众号编辑</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/weixinconfig/js/weixinconfig_edit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerWeixinConfigAction.js'></script>
    
    <%
	 String id = request.getParameter("id");  //获取查询的商品ID
	%>
	<script type="text/javascript">
		editWeixinConfigPage.param.id = <%=id %>;
	</script>
</head>
<body>
   <div class="alert alert-info">当前位置<b class="tip"></b>微信公众号管理<b class="tip"></b>微信公众号编辑</div>
   	
   	<table class="tb">
         <tr>
         	<td> <a href="<%=path %>/managerzaizst/weixinconfig.html">返回微信公众号列表</a> </td>
         </tr>
    </table>
    <form action="javascript:void(0)">
	    <table class="tb">
	   	 	<tr>
	   			<td style="text-align: right;">公众号名称：</td>
	   			<td><input type="text" id="name"/></td>
	   		</tr>
	   		<tr>
	   			<td style="text-align: right;">登录账号：<input type="hidden" id="weixinConfigId"/></td>
	   			<td><input type="text" id="loginUserName"/></td>
	   		</tr>
	   		<tr>
	   			<td style="text-align: right;">AppId：</td>
	   			<td>
	   			   <input type="text" id="appId"/>
	   			</td>
	   		</tr>
	   		<tr>
	   			<td style="text-align: right;">AppSecret：</td>
	   			<td>
	   			   <input type="text" id="appSecret"/>
	   			</td>
	   		</tr>
	   		<tr>
	   			<td style="text-align: right;">访问域名：</td>
	   			<td>
	   			   <input type="text" id="domainUrl"/>
	   			</td>
	   		</tr>
	   		<tr>
	   			<td style="text-align: right;">推广用户名：</td>
	   			<td>
	   			   <input type="text" id="extendUserName"/>
	   			</td>
	   		</tr>
	   		<tr>
	   			<td style="text-align: right;">推广模式：</td>
	   			<td>
	   			   <input type="text" id="extendSscRebate"/>
	   			</td>
	   		</tr>
	   		<tr>
	   			<td></td>
	   			<td><input type="button" onclick="editWeixinConfigPage.saveData()" value="提交"/>&nbsp;&nbsp;&nbsp;&nbsp;<input type="reset" value="重置"/></td>
	   		</tr>
	    </table>
    </form>
</body>
</html>

