function HandUpdatePage(){}
var handUpdatePage = new HandUpdatePage();
handUpdatePage.param = {
};

$(document).ready(function() {
	handUpdatePage.getAllTypes(); //加载投注类型
	
//	var _this = $('.list').find('thead');
//    //折叠
//    _this.click(function () {
//        var i = $(this).find('i');
//        if (i.attr('class') == 'tip-down') { i.attr('class', 'tip-up'); } else { i.attr('class', 'tip-down'); }
//        $(this).parent().find('tbody').toggle();
//    });
});

/**
 * 手动开奖处理
 */
HandUpdatePage.prototype.hendUpdateDeal = function(){
	var lotteryType = $("#lotteryType").val();
	var openCode = $("#openCode").val();
	var expect = $("#expect").val();
	
	if(lotteryType == null || lotteryType == "" ||
			openCode == null || openCode== "" ||
			expect == null || expect == ""
			){
		swal("参数有误.");
		return;
	}
	
	$("#bonusButton").attr('disabled','disabled');

	managerSystemAction.hendUpdateDeal(lotteryType,openCode,expect,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal("处理结束,手动开奖成功.");
      		$("#bonusButton").removeAttr("disabled");
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
			$("#bonusButton").removeAttr("disabled");
		}else{
			swal("处理开奖请求失败.");
			$("#bonusButton").removeAttr("disabled");
		}
    });
};

/**
 * 部分未开奖处理
 */
HandUpdatePage.prototype.hendPartUpdateDeal = function(){
	var lotteryType = $("#lotteryTypePart").val();
	var expect = $("#expectPart").val();
	
	if(lotteryType == null || lotteryType == "" ||
			openCode == null || openCode== "" ||
			expect == null || expect == ""
			){
		swal("参数有误.");
		return;
	}
	
	$("#bonusPartButton").attr('disabled','disabled');

	managerSystemAction.hendPartUpdateDeal(lotteryType,expect,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal("部分未开奖处理成功,开奖处理了["+ r[1] +"]条投注订单");
      		$("#bonusPartButton").removeAttr("disabled");
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
			$("#bonusPartButton").removeAttr("disabled");
		}else{
			swal("部分未开奖处理请求失败.");
			$("#bonusPartButton").removeAttr("disabled");
		}
    });
};

/**
 * 错误开奖处理
 */
HandUpdatePage.prototype.hendErrUpdateDeal = function(){
	var lotteryType = $("#lotteryTypeModify").val();
	var openCode = $("#openCodeModify").val();
	var expect = $("#expectModify").val();
	
	if(lotteryType == null || lotteryType == "" ||
			openCode == null || openCode== "" ||
			expect == null || expect == ""
			){
		swal("参数有误.");
		return;
	}
	
	$("#bonusModifyButton").attr('disabled','disabled');

	managerSystemAction.hendErrUpdateDeal(lotteryType,openCode,expect,function(r){
		if (r[0] != null && r[0] == "ok") {
			var withdrawLockUsers = r[1];
			if(withdrawLockUsers != null && withdrawLockUsers.length > 0) {
				var lockUserNames = "";
				for(var i = 0; i < withdrawLockUsers.length; i++) {
					lockUserNames += withdrawLockUsers[i].userName + ",";
				}
				if(lockUserNames.length > 0) {
					lockUserNames = lockUserNames.substring(0, lockUserNames.length -1);
				}
				swal("处理错误开奖成功.发现以下用户["+ lockUserNames +"]扣除中奖金额余额不足，将其提现锁定。");
			} else {
				swal("处理错误开奖成功.");
			}
      		$("#bonusModifyButton").removeAttr("disabled");
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
			$("#bonusModifyButton").removeAttr("disabled");
		}else{
			swal("处理错误开奖请求失败.");
			$("#bonusModifyButton").removeAttr("disabled");
		}
    });
};

/**
 * 一键未开奖回退处理
 */
HandUpdatePage.prototype.hendlotteryRegressionDeal = function(){
	var lotteryType = $("#lotteryTypeRegression").val();
	var expect = $("#expectRegression").val();
	
	if(lotteryType == null || lotteryType == "" ||
			expect == null || expect == ""
			){
		swal("参数有误.");
		return;
	}
	
	$("#bonusRegressionButton").attr('disabled','disabled');

	managerOrderAction.hendLotteryRegressionDeal(lotteryType,expect,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal("处理未开奖一键回退成功,共处理["+ r[1] +"]个投注订单");
      		$("#bonusRegressionButton").removeAttr("disabled");
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
			$("#bonusRegressionButton").removeAttr("disabled");
		}else{
			swal("处理未开奖回退请求失败.");
			$("#bonusRegressionButton").removeAttr("disabled");
		}
    });
};

/**
 * 加载彩种下拉
 */
HandUpdatePage.prototype.getAllTypes = function(){
	managerOrderAction.getOfficialLotteryKinds(function(r){
		if (r[0] != null && r[0] == "ok") {
			handUpdatePage.refreshTypes(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("查询所有的资金类型失败.");
		}
    });
};

/**
 * 展示彩种下拉框
 */
HandUpdatePage.prototype.refreshTypes = function(detailTypeMaps){
	var obj=document.getElementById('lotteryType');
	var obj2=document.getElementById('lotteryTypeModify');
	var obj3=document.getElementById('lotteryTypeRegression');
	var obj4=document.getElementById('lotteryTypePart');
	obj.options.add(new Option("=请选择=",""));
	obj2.options.add(new Option("=请选择=",""));
	obj3.options.add(new Option("=请选择=",""));
	obj4.options.add(new Option("=请选择=",""));
	for(var key in detailTypeMaps){
		obj.options.add(new Option(detailTypeMaps[key],key)); 
		obj2.options.add(new Option(detailTypeMaps[key],key));
		obj3.options.add(new Option(detailTypeMaps[key],key));
		obj4.options.add(new Option(detailTypeMaps[key],key));
	}  
};