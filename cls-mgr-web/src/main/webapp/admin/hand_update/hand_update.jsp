<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>  
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <title>彩票开奖处理</title>
         <link rel="shortcut icon" href="favicon.ico">
        <jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
    <body class="gray-bg">
        <div class="wrapper wrapper-content animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a data-toggle="tab" href="#tab-1" aria-expanded="true">手动开奖处理</a></li>
                            <li class="">
                                <a data-toggle="tab" href="#tab-2" aria-expanded="false">部分未开奖处理</a></li>
                            <li class="">
                                <a data-toggle="tab" href="#tab-3" aria-expanded="false">错误开奖处理</a></li>
                            <li class="">
                                <a data-toggle="tab" href="#tab-4" aria-expanded="false">未开奖一键回退</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane active">
                                <div class="panel-body">
                                    <form class="form-horizontal">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">彩种</span>                                                
                                                     <select id='lotteryType' class="ipt form-control">
                                                      </select>
                                                    </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">期号</span>
                                                    <input type="text" value="" id='expect' class="form-control"></div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">开奖号码(,分隔)</span>
                                                    <input type="text" id='openCode' value="" class="form-control"></div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-group text-center">
                                                    <button type="button" class="btn btn-outline btn-default btn-kj" onclick="handUpdatePage.hendUpdateDeal()">开奖处理</button></div>
                                            </div>
                                        </div>
                                        <div class="row"> 
                                        	<div class="col-sm-12">
												<p style="color: red">温馨提醒：输入的期号必须全为数字，不能带有“-”字符或其他字符，开奖号码用英文,进行分隔，特殊彩种如十一选五、PK10、六合彩，开奖号码是两位的，要在前面补0</p>
											</div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div id="tab-2" class="tab-pane">
                                <div class="panel-body">
                                    <form class="form-horizontal">
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">彩种</span>
                                                      <select id='lotteryTypePart' class="ipt form-control">
                  										</select>
                                                </div>
                                            </div>
                                            <div class="col-sm-5">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">期号</span>
                                                    <input type="text" value="" id='expectPart' class="form-control"></div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-group text-center">
                                                    <button type="button" class="btn btn-outline btn-default btn-kj" onclick="handUpdatePage.hendPartUpdateDeal()">部分未开奖处理</button></div>
                                            </div>
                                        </div>
                                        <div class="row"> 
                                        	<div class="col-sm-12">
												<p style="color: red">温馨提醒：部分未开奖处理需要当前彩种对应期号的开奖号码已经存在才能处理订单，可以先到彩票管理-开奖号码查询查看</p>
											</div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div id="tab-3" class="tab-pane">
                                <div class="panel-body">
                                    <form class="form-horizontal">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">彩种</span>
									                  <select id='lotteryTypeModify' class="ipt form-control">
									                  </select>
                                          
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">期号</span>
                                                    <input type="text" value="" id='expectModify' class="form-control"></div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">开奖号码(,分隔)</span>
                                                    <input type="text" value="" id='openCodeModify' class="form-control"></div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-group text-center">
                                                    <button type="button" class="btn btn-outline btn-default btn-kj" onclick="handUpdatePage.hendErrUpdateDeal()">错误开奖处理</button></div>
                                            </div>
                                        </div>
                                        <div class="row"> 
                                        	<div class="col-sm-12">
												<p style="color: red">温馨提醒：错误开奖处理会对已经开奖的注单重新计算，若对新开奖号码还是保持中奖或者未中奖的，则只更新其开奖号码；
												若原来中奖，新开奖号码未中奖的，则扣除用户中奖金额（余额不够扣除的，设定用户为提现锁定状态）；
												若原来未中奖，新开奖号码中奖的，则进行奖金派送处理</p>
											</div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div id="tab-4" class="tab-pane">
                                <div class="panel-body">
                                    <form class="form-horizontal">
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">彩种</span>
                                                     <select id='lotteryTypeRegression' class="ipt form-control">
                  									 </select>
                                                   
                                                </div>
                                            </div>
                                            <div class="col-sm-5">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">期号</span>
                                                    <input type="text" value="" id='expectRegression' class="form-control"></div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-group text-center">
                                                    <button type="button" class="btn btn-outline btn-default btn-kj" onclick="handUpdatePage.hendlotteryRegressionDeal()">未开奖回退处理</button></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                        	<div class="col-sm-12">
												<p style="color: red">温馨提醒：订单回退处理后，订单的投注金额会归还给用户</p>
											</div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
<!-- js -->
<script type="text/javascript" src="<%=path%>/admin/hand_update/js/hand_update.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerSystemAction.js'></script>
<script type='text/javascript' src='<%=path%>/dwr/interface/managerOrderAction.js'></script>
    
</body>
</html>

