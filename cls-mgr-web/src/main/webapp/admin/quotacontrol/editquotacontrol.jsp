<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>  
    <head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>配置编辑</title>
    <link rel="shortcut icon" href="favicon.ico">
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    <%
	 String id = request.getParameter("id");  //获取查询的商品ID
	%>
</head>
   <body class="gray-bg">
        <div class="wrapper wrapper-content  animated fadeIn">       
                            <div class="row">
                            <form class="form-horizontal" id="quotaControlForm">
                               <div class="col-sm-12">
                               <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                                  <div class="input-group m-b">
                                   <span class="input-group-addon">商户</span>
                                       <cls:bizSel name="bizSystem" id="bizSystem" emptyOption="false" options="class:ipt form-control"/>
                                  </div>
                                 </c:if>
                                 <div class="input-group m-b">
                                   <span class="input-group-addon">模式</span>
                                    <input id="id" name="id" type="hidden"  />
                                     <c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
						   			<input type="hidden" id="bizSystem" name="bizSystem"/>
						   			</c:if>
						   			<select class="ipt form-control" id="sscRebate" onkeyup="checkNum(this)"></select>   
                                  </div>
                                <div class="input-group m-b">
                                   <span class="input-group-addon">配额数</span>
                                    <input id="id" name="id" type="hidden"  />
                                     <input  id="openLowerLevelNum" name="openLowerLevelNum" onkeyup="checkNum(this)"  type="text"  class="form-control">
                                  </div>
                                </div>
			                   <div class="col-sm-12">
			                    <div class="row">
			                        <div class="col-sm-12 text-center">
			                            <button type="button" class="btn btn-w-m btn-white" onclick="editQuotaControlPage.saveData()">提 交</button>
			                            &nbsp;&nbsp;
			                            <input type="reset" class="btn btn-w-m btn-white" value="重 置"/>
			                            </div>
			                    </div> 
			             </div>
			             </form>
                   </div>
                </div>
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/quotacontrol/js/editquotacontrol.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerQuotaControlAction.js'></script>
    <script type="text/javascript">
    editQuotaControlPage.param.id = <%=id%>;
	</script>    
</body>
</html>

