function QuotaControlPage(){}
var quotaControlPage = new QuotaControlPage();

quotaControlPage.param = {
   	
};

/**
 * 查询参数
 */
quotaControlPage.queryParam = {
};

//分页参数
quotaControlPage.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize

};


$(document).ready(function() {

	quotaControlPage.initTableData(); //初始化查询数据
});

/**
 * 初始化列表数据
 */
QuotaControlPage.prototype.initTableData = function() {
	quotaControlPage.queryParam = getFormObj($("#queryForm"));
	quotaControlPage.table = $('#systemConfigTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"scrollX": true,
		"iDisplayLength" : defaultPageSize,
		"ajax":function (data, callback, settings) {
			quotaControlPage.pageParam.pageSize = data.length;//页面显示记录条数
			quotaControlPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerQuotaControlAction.getAllQuotaControl(quotaControlPage.queryParam,quotaControlPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
                    {"data": "bizSystemName","bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "sscRebate"},
		            {"data": "openLowerLevelNum"},
		            {"data": "updateAdmin"},
		            {   
		            	"data": "updateTime",
		            	render: function(data, type, row, meta) {
		            		return dateFormat(data,'yyyy-MM-dd hh:mm:ss');
	            		}
		            },
		            {
	                	 "data": "id",
	                	 render: function(data, type, row, meta) {
	                		 return "<a class='btn btn-info btn-sm btn-bj' onclick='quotaControlPage.editQuotaControl("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;" +
	                		 		"<a class='btn btn-danger btn-sm btn-del' onclick='quotaControlPage.delQuotaControl("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";
	                	 }
	                }
	            ]
	}).api(); 
}





/**
 * 查询数据
 */
QuotaControlPage.prototype.findQuotaControl = function(){
	quotaControlPage.queryParam.configKey = $("#configKey").val();
	quotaControlPage.queryParam.configValue = $("#configValue").val();
	quotaControlPage.queryParam.enable = $("#enable").val();
	quotaControlPage.queryParam.bizSystem = $("#bizSystem").val();
	quotaControlPage.table.ajax.reload();
};


/**
 * 删除
 */
QuotaControlPage.prototype.delQuotaControl = function(id){
	   swal({
           title: "您确定要删除这条信息吗",
           text: "请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "删除",
           closeOnConfirm: false
       },
       function() {
    	   managerQuotaControlAction.delQuotaControlById(id,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "删除成功",type: "success"});
				quotaControlPage.findQuotaControl();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("删除信息失败.");
			}
	    });
	});
};

QuotaControlPage.prototype.addQuotaControl=function()
{
	  layer.open({
          type: 2,
          title: '系统配额新增',
          maxmin: false,
          shadeClose: true,
          //点击遮罩关闭层
          area: ['600px', '320px'],
         // area: ['70%', '80%'],
          content: contextPath + "/managerzaizst/quotacontrol/editquotacontrol.html",
          cancel: function(index){ 
        	  quotaControlPage.findQuotaControl();
       	   }
      });
}

QuotaControlPage.prototype.editQuotaControl=function(id)
{
	  layer.open({
          type: 2,
          title: '系统配额编辑',
          maxmin: false,
          shadeClose: true,
          //点击遮罩关闭层
          area: ['600px', '320px'],
         // area: ['70%', '80%'],
          content: contextPath + "/managerzaizst/quotacontrol/"+id+"/editquotacontrol.html",
          cancel: function(index){ 
        	  quotaControlPage.findQuotaControl();
       	   }
      });
}

QuotaControlPage.prototype.closeLayer=function(index){
	layer.close(index);
	quotaControlPage.findQuotaControl();
}