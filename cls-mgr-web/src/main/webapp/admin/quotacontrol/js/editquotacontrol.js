function EditQuotaControlPage(){}
var editQuotaControlPage = new EditQuotaControlPage();

editQuotaControlPage.param = {
   	
};

$(document).ready(function() {
	var bizSystem
	if(currentUser.bizSystem=='SUPER_SYSTEM'){
		bizSystem = $("#bizSystem").val();
	}else{
		bizSystem = currentUser.bizSystem;
	}
	editQuotaControlPage.getAwardModel(bizSystem);
	$("#bizSystem").change(function(){
		var bizSystem = $("#bizSystem").val();
		editQuotaControlPage.getAwardModel(bizSystem);
	});
	
	var id = editQuotaControlPage.param.id;  //获取查询ID
	if(id != null){
		editQuotaControlPage.editQuotaControl(id);
		$("#bizSystem").attr("disabled","disabled");
		$("#sscRebate").attr("disabled","disabled");
	}
});


/**
 * 保存更新信息
 */
EditQuotaControlPage.prototype.saveData = function(){
	var id = $("#id").val();
	var bizSystem = $("#bizSystem").val();
	var openLowerLevelNum = $("#openLowerLevelNum").val();
	var sscRebate = $("#sscRebate").val();

	
	if(sscRebate==null || sscRebate.trim()==""){
		swal("请输入模式!");
		$("#sscRebate").focus();
		return;
	}
	if(openLowerLevelNum==null || openLowerLevelNum.trim()==""){
		swal("请输入配额数!");
		$("#openLowerLevelNum").focus();
		return;
	}
	
	
	var quotaControl = {};
	quotaControl.id = editQuotaControlPage.param.id;
	quotaControl.bizSystem = bizSystem;
	quotaControl.sscRebate = parseInt(sscRebate);
	quotaControl.openLowerLevelNum = parseInt(openLowerLevelNum);
	managerQuotaControlAction.saveOrUpdateQuotaControl(quotaControl,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.quotaControlPage.closeLayer(index);
		   	   });
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("保存信息失败.");
		}
    });
};


/**
 * 赋值配置信息
 */
EditQuotaControlPage.prototype.editQuotaControl = function(id){
	managerQuotaControlAction.getQuotaControlById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			$("#id").val(r[1].id);
			$("#bizSystem").val(r[1].bizSystem);
			$("#sscRebate").val(r[1].sscRebate);
			$("#openLowerLevelNum").val(r[1].openLowerLevelNum);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取信息失败.");
		}
    });
};

/**
 * 初始化奖金模式
 */
EditQuotaControlPage.prototype.getAwardModel = function(bizSystem){
	var selDom = $("#sscRebate");
	    selDom.html("");
	managerQuotaControlAction.getAwardModel(bizSystem,function(r){
		if (r[0] != null && r[0] == "ok") {
			highestAwardModel=r[1].sscHighestAwardModel;
			lowestAwardModel=r[1].sscLowestAwardModel;
			for(var i=0;i<=(highestAwardModel-lowestAwardModel);i=i+2){
				selDom.append("<option value='"+(highestAwardModel-i)+"'>"+(highestAwardModel-i)+"</option>");	
			}	
		}else if (r[0] != null && r[0] == "error") {
			swal(r[1]);
		}else{
			swal("请求失败.");
		}}
	);
	
}