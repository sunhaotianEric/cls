<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>  
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>商户配额配置</title>
        <jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
    <body >
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
                    <div class="ibox ">
                        <div class="ibox-content">
                            <div class="row">
                                <form class="form-horizontal" id="queryForm">
                                    <div class="row">
                                      <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                                        <div class="col-sm-3">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">商户</span>
                                                 <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group nomargin text-left">
                                                <button type="button" class="btn btn-outline btn-default btn-kj" onclick="quotaControlPage.findQuotaControl()"><i class="fa fa-search"></i>&nbsp;查 询</button>
                                                &nbsp;&nbsp;
                                                <button type="button" class="btn btn-outline btn-default btn-kj" onclick="quotaControlPage.addQuotaControl()"><i class="fa fa-search"></i>&nbsp;配额新增</button>
                                                </div>
                                        </div>
                                        <div class="col-sm-5">
                                        </div>
                                        </c:if>
                                      <c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
           
                                        <div class="col-sm-4">
                                            <div class="form-group nomargin text-left">
                                                <button type="button" class="btn btn-outline btn-default btn-kj" onclick="quotaControlPage.findQuotaControl()"><i class="fa fa-search"></i>&nbsp;查 询</button>
                                                &nbsp;&nbsp;
                                                <button type="button" class="btn btn-outline btn-default btn-kj" onclick="quotaControlPage.addQuotaControl()"><i class="fa fa-search"></i>&nbsp;配置新增</button>
                                                </div>
                                        </div>
                                        </c:if>
                                    </div>
                                </form>
                            </div>
                            <div class="jqGrid_wrapper">
                                <div class="ui-jqgrid ">
                                    <div class="ui-jqgrid-view">
                                        <div class="ui-jqgrid-hdiv">
                                            <div class="ui-jqgrid-hbox" style="padding-right:0">
                                                <table class="ui-jqgrid-htable ui-common-table table table-bordered" id="systemConfigTable">
                                                    <thead>
                                                        <tr class="ui-jqgrid-labels">
                                                              <th class="ui-th-column ui-th-ltr">商户</th>
                                                              <th class="ui-th-column ui-th-ltr">模式</th>
													          <th class="ui-th-column ui-th-ltr">配额数</th>
													          <th class="ui-th-column ui-th-ltr">操作人</th>
													          <th class="ui-th-column ui-th-ltr">更新时间</th>
													          <th class="ui-th-column ui-th-ltr">操  作</th>
                                                            </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/quotacontrol/js/quotacontrol.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerQuotaControlAction.js'></script>    
</body>
</html>

