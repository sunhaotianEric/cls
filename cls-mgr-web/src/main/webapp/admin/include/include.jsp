<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="com.team.lottery.vo.LotteryControlConfig,com.team.lottery.service.LotteryControlConfiService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.ConstantUtil,net.sf.json.JSONObject,com.team.lottery.vo.Admin,com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>

<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />

<!-- css -->
<link href="<%=path%>/css/bootstrap.min14ed.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>?v=3.3.6" rel="stylesheet">
<link href="<%=path%>/css/font-awesome.min93e3.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>?v=4.4.0" rel="stylesheet">
<link href="<%=path%>/css/animate.min.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" rel="stylesheet">
<link href="<%=path%>/css/style.min862f.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" rel="stylesheet">
<link href="<%=path%>/css/plugins/sweetalert/sweetalert.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" rel="stylesheet">
<link href="<%=path%>/css/plugins/datapicker/datepicker3.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" rel="stylesheet">
<link href="<%=path%>/css/plugins/dataTables/dataTables.bootstrap.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" rel="stylesheet">
<link href="<%=path%>/css/cls.common.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" rel="stylesheet">
<%-- <link href="<%=path%>/css/plugins/dataTables/fixedColumns.dataTables.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" rel="stylesheet">
 --%>
<!-- js -->
<script src="<%=path%>/js/jquery.min.js?v=2.1.4"></script>
<script src="<%=path%>/js/bootstrap.min.js?v=3.3.6"></script>
<script src="<%=path%>/js/content.min.js?v=1.0.0"></script>
<script src="<%=path%>/js/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<%=path%>/js/plugins/layer/layer.min.js"></script>
<script src="<%=path%>/js/plugins/layer/laydate/laydate.js"></script>
<script src="<%=path%>/js/plugins/dataTables/jquery.dataTables.js"></script>
<%-- <script src="<%=path%>/js/plugins/dataTables/dataTables.fixedColumns.min.js"></script> --%>
<script src="<%=path%>/js/plugins/dataTables/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<%=path%>/js/plugins/dataTables/input.js"></script>
<script src="<%=path%>/js/plugins/iCheck/icheck.min.js"></script>
<script src="<%=path%>/js/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<%=path%>/js/plugins/summernote/summernote.min.js"></script>


<!-- util js -->
<script type="text/javascript" src="<%=path%>/resources/utils/array_util.js"></script>
<script type="text/javascript" src="<%=path%>/resources/utils/dialog_util.js"></script>
<script type="text/javascript" src="<%=path%>/resources/utils/eventproxy.js"></script>
<script type="text/javascript" src="<%=path%>/resources/utils/string_util.js"></script>
<script type="text/javascript" src="<%=path%>/resources/utils/date-util.js"></script>
<script type="text/javascript" src="<%=path%>/resources/utils/loading.js"></script>
<script type="text/javascript" src="<%=path%>/resources/utils/map_util.js"></script>
<script type="text/javascript" src="<%=path%>/resources/utils/common.js"></script>
<!--获取表单元素的js  -->
<script type="text/javascript" src="<%=path%>/resources/utils/formobj.js"></script>
<!-- DWR -->
<script type='text/javascript' src='<%=path%>/dwr/engine.js'></script>
<script type='text/javascript' src='<%=path%>/dwr/util.js'></script>


<!-- common js -->
<script type="text/javascript" src="<%=path%>/admin/common/common.js"></script>
<%
  Admin admin = (Admin)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_ADMIN);
  String userJson = "{}";
  if (admin != null) {
     JSONObject obj = JSONObject.fromObject(admin);
     userJson = obj.toString();
  }
  LotteryControlConfig record=new LotteryControlConfig();
  record.setBizSystem(admin.getBizSystem());
  LotteryControlConfiService lotteryControlConfiService=ApplicationContextUtil.getBean(LotteryControlConfiService.class);
  record=lotteryControlConfiService.queryLotteryControlConfigBybizSystem(record);
  if(record!=null){
	  request.setAttribute("enabled",record.getEnabled());
  }else{
	  request.setAttribute("enabled",0);
  }
  request.setAttribute("admin", admin);
%>
	
<script type="text/javascript">
  var contextPath = '<%=path %>';
  var currentUser = <%=userJson %>;
  var adminName = '<%=ConstantUtil.FRONT_ADMIN_USER_NAME %>';
  var currentAdminName = currentUser.username;
  var defaultPageSize = <%=SystemConfigConstant.defaultMgrPageSize %>; 
</script>

<script type="text/javascript">
function handler(errorString, ex){
	console.log("商户异常:请联系商户管理员人员,并将错误信息发送至管理人员:" + errorString + ",ex:" + ex);
	// alert("商户异常:请联系商户管理员人员,并将错误信息发送至管理人员-" + errorString);
}
dwr.engine.setErrorHandler(handler);
</script>

