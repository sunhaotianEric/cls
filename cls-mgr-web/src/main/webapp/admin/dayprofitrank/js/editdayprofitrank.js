function EditDayProfitRankPage(){}
var editDayProfitRankPage = new EditDayProfitRankPage();

editDayProfitRankPage.param = {
		
};

editDayProfitRankPage.queryParam = {
		bizSystem : null,
		userId : null,
		userName : null,
		bonus : null,
		ranking : null,
		nickName : null,
		userName : null,
};

$(document).ready(function() {
	var helpId = editDayProfitRankPage.param.id;  //获取查询的帮助ID
	if(helpId != null){
		editDayProfitRankPage.editHelps(helpId);
	}
});


/**
 * 保存帮助信息
 */
EditDayProfitRankPage.prototype.saveData = function(){
	
	var id = $("#helpId").val();
	var bizSystem = $("#bizSystem").val();
	var bonus = $("#bonus").val();
	var ranking = $("#ranking").val();
	var userName = $("#userName").val();
	var nickName = $("#nickName").val();
	
	editDayProfitRankPage.queryParam.id=id;
	editDayProfitRankPage.queryParam.bizSystem=bizSystem;
	editDayProfitRankPage.queryParam.bonus=bonus;
	editDayProfitRankPage.queryParam.ranking=ranking;
	editDayProfitRankPage.queryParam.userName=userName;
	editDayProfitRankPage.queryParam.nickName=nickName;
	if(editDayProfitRankPage.param.id!=null||editDayProfitRankPage.queryParam.id!=""){
		managerDayProfitRankAction.updateByPrimaryKeySelective(editDayProfitRankPage.queryParam,function(r){
			if(r[0]=="ok"){
				swal({title:"修改成功！",
			        text:"已成功修改数据",
			        type:"success"},
			      function(){
			        	var index = parent.layer.getFrameIndex(window.name); //获取窗口索引    
			        	parent.dayProfitRankPage.layerClose(index);
				  }
			     )				
			}else{
				swal(r[1]);
			}
		});
	}
	editDayProfitRankPage.queryParam={};
};


/**
 * 赋值信息
 */
EditDayProfitRankPage.prototype.editHelps = function(id){
	managerDayProfitRankAction.getDayProfitRankById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			$("#helpId").val(r[1].id);
			$("#bizSystem").val(r[1].bizSystem);
			$("#bizSystem").attr("disabled","disabled");
			$("#bonus").val(r[1].bonus);
			$("#ranking").val(r[1].ranking);
			$("#ranking").attr("disabled","disabled");
			$("#userName").val(r[1].userName);
			$("#nickName").val(r[1].nickName);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取信息失败.");
		}
    });
};