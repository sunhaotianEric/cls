function DayProfitRankPage(){}
var dayProfitRankPage = new DayProfitRankPage();

/**
 * 查询参数
 */
dayProfitRankPage.param = {
	id:null,
	bizSystem : null,
	userId : null,
	userName : null,
	nickName : null,
	bonus : null,
	ranking : null,
	profittimeStart : null,
	profittimeEnd : null,
	startime : null,
	endtime : null,
	
};

//分页参数
dayProfitRankPage.pageParam = {
	pageNo : 1,
	pageSize : defaultPageSize
};
var startime,endtime,num=0

$(document).ready(function() {
	//归属日期默认昨天
	startime= new Date().AddDays(-1).format("yyyy-MM-dd 00:00:00");
	endtime= new Date().AddDays(-1).format("yyyy-MM-dd 23:59:59");
	$("#startime").val(startime);
	$("#endtime").val(endtime);
	//查询所有的操作日志数据
	dayProfitRankPage.getAllRecordLogs();
	
})

DayProfitRankPage.prototype.findRecordLogs = function(){
	dayProfitRankPage.param = getFormObj($("#recordLogQuery"));
	startime=$("#startime").val();
	if(startime!=null&&startime!=''){
		dayProfitRankPage.param.startime=startime.stringToDate();
	}
	endtime=$("#endtime").val();
	if(endtime!=null&&endtime!=''){
		dayProfitRankPage.param.endtime=endtime.stringToDate();
	}
	dayProfitRankPage.table.ajax.reload();
}


/**
 * 加载所有的登陆日志数据
 */
DayProfitRankPage.prototype.getAllRecordLogs = function(){
	dayProfitRankPage.param = getFormObj($("#activityQuery"));
	dayProfitRankPage.table =  $('#activityTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"ajax":function (data, callback, settings) {
			dayProfitRankPage.pageParam.pageSize = data.length;//页面显示记录条数
			dayProfitRankPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
			managerDayProfitRankAction.getAllDayProfitRanks(dayProfitRankPage.param,dayProfitRankPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				}; 
				
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
					if(num===0){
						dayProfitRankPage.findRecordLogs();
					}
					num++
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询奖金配置数据失败.");
				}
				callback(returnData);
		    });
		},
	"columns": [
	            {"data":"bizSystemName"},
	            {"data":"userId",
	            	render: function(data, type, row, meta) {
	            		if (data == "-1") {
	            			return "<span style='color: red;'>假数据</span>";
	            		} else {
	            			return "<span style='color: blue;'>真数据</span>";
	            		}
	            		return data;
	            	}
	            },
	            {"data":"userName"},
	            {"data":"nickName"},
	            {"data":"ranking"},
	            {"data":"bonus"},
	            {"data":"belongDateStr"},
	            {"data":"createDateStr"},
	            {
	            	 "data": "id",
	            	 render: function(data, type, row, meta) {
	            		 return "<a class='btn btn-info btn-sm btn-bj' onclick='dayProfitRankPage.editConfigControl("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;";
	            	 }
	            }
	          /*  {"data":"id",
	            	 render: function(data, type, row, meta) {
	            		 return  "<a class='btn btn-danger btn-sm btn-del' onclick='recordLogPage.delRecordLogs("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";
	            	 },"bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false
	            },*/
	            
	     ]
	}).api();
}

/**
 * 按日期查询数据
 */
DayProfitRankPage.prototype.findRecordlogByQuery = function(dateRange){
	if(dateRange != undefined && dateRange != null && dateRange != ''){
		commonPage.setDateRangeTimeZero(dateRange, "startime", "endtime",2);	
	}
	dayProfitRankPage.param = getFormObj($("#recordLogQuery"));
	dayProfitRankPage.table.ajax.reload();
};

/**
 * 编辑
 */
DayProfitRankPage.prototype.editConfigControl=function(id){
	layer.open({
        type: 2,
        title: '中奖榜单编辑',
        maxmin: false,
        shadeClose: true,
        //点击遮罩关闭层
        area: ['600px', '450px'],
        content: contextPath + "/managerzaizst/dayprofitrank/editdayprofitrank.html?id="+id,
        cancel: function(index){ 
        	dayProfitRankPage.table.ajax.reload();
     	}
    });
}

DayProfitRankPage.prototype.layerClose=function(index){
	layer.close(index);
	dayProfitRankPage.findRecordLogs();
}