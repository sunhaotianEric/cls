function 	ChatLevelAuthEditPage(){}
var chatLevelAuthEditPage = new ChatLevelAuthEditPage();

chatLevelAuthEditPage.param = {
   	
};
/*var initFinsh = false;*/

$(document).ready(function() {
	var domainId = chatLevelAuthEditPage.param.id;  //获取查询的ID
	if(domainId != null){
		chatLevelAuthEditPage.editChatLevelAuth(domainId);
	}
	 
});
/**
 * 保存充值配置
 */
ChatLevelAuthEditPage.prototype.saveChatLevelAuth = function(){
	var chatLevelAuth = {};
	chatLevelAuth.id = $("#id").val();
	chatLevelAuth.bizSystem = $("#bizSystem").val();
	chatLevelAuth.level = $("#level").val();
	chatLevelAuth.levelName = $("#levelName").val();
	var check=$("input[name='rules']:checked");
	if(check.length>0){
		var rules="";
		check.each(function(i){
			   rules =rules+","+$(this).val();
		 });
		if(rules.length > 0){
			chatLevelAuth.rules = rules.substring(1);
		}else{
			chatLevelAuth.rules = rules;	
		}
		
	}

	
	if(currentUser.bizSystem=='SUPER_SYSTEM'){
		if (chatLevelAuth.bizSystem==null || chatLevelAuth.bizSystem.trim()=="") {
			swal("请选择业务系统!");
			$("#bizSystem").focus();
			return;
		} 
	}
	
	if (chatLevelAuth.level==null || chatLevelAuth.level.trim()=="") {
		swal("请输入等级!");
		$("#level").focus();
		return;
	}
	if (chatLevelAuth.levelName==null || chatLevelAuth.levelName.trim()=="") {
		swal("请输入等级名称!");
		$("#levelName").focus();
		return;
	}
	if (chatLevelAuth.rules==null || chatLevelAuth.rules.trim()=="") {
		chatLevelAuth.rules = '';
	}
	chatLevelAuth.level = chatLevelAuth.level.replace("VIP","");
 
	managerChatLevelAuthAction.saveOrUpdateChatLevelAuth(chatLevelAuth,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.chatLevelAuthPage.closeLayer(index);
		   	    }); 
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
			//showErrorDlg(jQuery(document.body), r[1]);
		}else{
			swal("添加充值配置数据失败.");
			//showErrorDlg(jQuery(document.body), "添加充值配置数据失败.");
		}
    });
};



/**
 * 快捷充值配置
 */
ChatLevelAuthEditPage.prototype.editChatLevelAuth = function(id){
	managerChatLevelAuthAction.getChatLevelAuthById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			var chatLevelAuth = r[1];
			$("#id").val(chatLevelAuth.id);
			$("#bizSystem").val(chatLevelAuth.bizSystem);
			$("#level").val("VIP"+chatLevelAuth.level);
			$("#bizSystem").attr("disabled",true);
			$("#levelName").val(chatLevelAuth.levelName);
			$("#level").attr("disabled",true);
			$("#levelName").attr("disabled",true);
			var checkBox = chatLevelAuth.rules;  
	        var checkBoxArray = checkBox.split(",");  
	        for(var i=0;i<checkBoxArray.length;i++){  
	            $("input[name='rules']").each(function(){  
	                if($(this).val()==checkBoxArray[i]){  
	                    $(this).attr("checked","checked");  
	                }  
	            })  
	        }  
			/*$("#rules").val(chatLevelAuth.rules);*/
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
			//showErrorDlg(jQuery(document.body), r[1]);
		}else{
			swal("获取充值配置失败.");
			//showErrorDlg(jQuery(document.body), "获取充值配置失败.");
		}
    });
};