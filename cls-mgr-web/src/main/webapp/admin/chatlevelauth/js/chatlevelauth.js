function ChatLevelAuthPage(){}
var chatLevelAuthPage = new ChatLevelAuthPage();

chatLevelAuthPage.param = {
		
};
//分页参数
chatLevelAuthPage.pageParam = {
		pageNo : 1,
	    pageSize : defaultPageSize
};
/**
 * 查询参数
 */
chatLevelAuthPage.queryParam = {
		bizSystem : null
};
$(document).ready(function() {
	chatLevelAuthPage.initTableData(); //查询所有的充值配置
});


/**
 * 查询数据
 */
ChatLevelAuthPage.prototype.findChatLevelAuth = function(){
	
	
	var bizSystem = $("#bizSystem").val();
	
	
	if(bizSystem == ""){
		chatLevelAuthPage.queryParam.bizSystem = null;
	}else{
		chatLevelAuthPage.queryParam.bizSystem = bizSystem;
	}
	
	chatLevelAuthPage.table.ajax.reload();
};






/**
 * 编辑
 */
ChatLevelAuthPage.prototype.editChatLevelAuth=function(id)
{
	
	  layer.open({
           type: 2,
           title: '充值配置编辑',
           maxmin: false,
           shadeClose: true,
           //点击遮罩关闭层
           area: ['70%', '80%'],
           content: contextPath + "/managerzaizst/chatlevelauth/"+id+"/chatlevelauth_edit.html",
           cancel: function(index){ 
        	   chatLevelAuthPage.table.ajax.reload();
        	   }
       });
}

ChatLevelAuthPage.prototype.closeLayer=function(index){
	layer.close(index);
	chatLevelAuthPage.table.ajax.reload();
}


ChatLevelAuthPage.prototype.initTableData = function(){
	
	chatLevelAuthPage.queryParam = getFormObj($("#chatlevelauthForm"));
	chatLevelAuthPage.table = $('#chatlevelauthTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			chatLevelAuthPage.pageParam.pageSize = data.length;//页面显示记录条数
			chatLevelAuthPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
			    	managerChatLevelAuthAction.getAllChatLevelAuth(chatLevelAuthPage.queryParam,chatLevelAuthPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
						recordsTotal : 0,
						recordsFiltered : 0,
						data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询充值配置数据失败.");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": null},
		            {"data": "bizSystemName", 
		                 "bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "level",
		            	render: function(data, type, row, meta) {
		     
		            		return "VIP"+data ;
		            	}
		             },
		            {"data": "levelName"},
		            {"data": "rules",
		            	render: function(data, type, row, meta) {
		            		var rulesArr = "";
		                    if(row.rules.length > 0){
		                    	var r = row.rules.split(",");
		                    	for(var i=0;i<r.length;i++){
		                    		if(r[i] == 'msg_send'){
		                    			rulesArr += "，信息发送";
		                    		}else if(r[i] == 'sendRedBag'){
		                    			rulesArr +="，发送红包";
		                    		}else if(r[i] == 'sendImg'){
		                    			rulesArr +="，发送图片";
		                    		}
		                    	}
		                    	rulesArr=rulesArr.substring(1);
		                    }
		            		return rulesArr ;
		            	}},
	        		{"data": "createDateStr"},
		            {
		            	"data": "id",
		            	render: function(data, type, row, meta) {
		            		var str="<a class='btn btn-info btn-sm btn-bj' onclick='chatLevelAuthPage.editChatLevelAuth("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;";         		
		            		return str ;
		            	}
		            }
		            ]
	}).api(); 
};

