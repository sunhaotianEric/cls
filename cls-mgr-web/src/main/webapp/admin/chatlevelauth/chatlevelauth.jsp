<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
	<title>充值配置设置</title>
	<jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
<body>
	<div class="animated fadeIn">
		<div class="ibox-content">
			<div class="row m-b-sm m-t-sm">
				<form class="form-horizontal" id="chatlevelauthForm">
					<div class="row">
						<c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
							<div class="col-sm-3">
								<div class="input-group m-b">
									<span class="input-group-addon">商户</span>
									<cls:bizSel name="bizSystem" id="bizSystem"
										options="class:ipt form-control" />
								</div>
							</div>
							
						</c:if>
						
						
						
		
						<%-- <c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
							<div class="col-sm-9"></div> 
						</c:if> --%>
						<div class="col-sm-3">
							<div class="form-group nomargin">
								<button type="button" class="btn btn-outline btn-default btn-js"
									onclick="chatLevelAuthPage.pageParam.pageNo = 1;chatLevelAuthPage.findChatLevelAuth()">
									<i class="fa fa-search"></i>查 询
								</button>
								<!-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<button type="button"
									class="btn btn-outline btn-default btn-add"
									onclick="rechargeConfigPage.addRechargeConfig()">
									<i class="fa fa-plus"></i>&nbsp;新增
								</button> -->
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="jqGrid_wrapper">
				<div class="ui-jqgrid ">
					<div class="ui-jqgrid-view">
						<div class="ui-jqgrid-hdiv">
							<div class="ui-jqgrid-hbox" style="padding-right: 0">
								<table id="chatlevelauthTable"
									class="ui-jqgrid-htable ui-common-table table table-bordered">
									<thead>
										<tr class="ui-jqgrid-labels">
											<th class="ui-th-column ui-th-ltr">序号</th>
											<th class="ui-th-column ui-th-ltr">商户</th>
											<th class="ui-th-column ui-th-ltr">等级</th>
											<th class="ui-th-column ui-th-ltr">等级名称</th>
											<th class="ui-th-column ui-th-ltr">规则权限</th>
											<!--<th>商户类型</th>预留-->
											<th class="ui-th-column ui-th-ltr">创建时间</th>
											<th class="ui-th-column ui-th-ltr">操作</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<script type="text/javascript"
		src="<%=path%>/admin/chatlevelauth/js/chatlevelauth.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
	<!-- dwr -->
	<script type='text/javascript'
		src='<%=path%>/dwr/interface/managerChatLevelAuthAction.js'></script>
</body>
</html>

