<%@ page language="java" contentType="text/html; charset=UTF-8" 
	import = "com.team.lottery.enums.EBankInfo,com.team.lottery.system.SystemConfigConstant" 
	pageEncoding="UTF-8"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<html>
<head>
    <meta charset="utf-8" />
    <title>充值配置</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/rechargeconfig/js/rechargeconfig_edit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerRechargeConfigAction.js'></script>
    
    <link rel="stylesheet" href="<%=path%>/resources/kindeditor-4.1.10/themes/default/default.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" />
	<script charset="utf-8" src="<%=path%>/resources/kindeditor-4.1.10/kindeditor-min.js"></script>
	<script charset="utf-8" src="<%=path%>/resources/kindeditor-4.1.10/lang/zh_CN.js"></script>
	
	<script type="text/javascript">
		var imgServerUrl = '<%= SystemConfigConstant.imgServerUrl %>';
	</script>
</head>
<body>
   <div class="alert alert-info">当前位置<b class="tip"></b>网站信息管理<b class="tip"></b>充值配置</div>
   
    <form action="javascript:void(0)">
	    <table class="tb">
	    
	   		<c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
	   		<tr>
	   			<td>商户：</td>
	   			<td><cls:bizSel name="bizSystem" id="bizSystem"/></td>
	   		</tr>
	   		</c:if>
	   		<tr>
	   			<td>充值类型：</td>
	   			<td>
	   			<cls:enmuSel name="payType" className="com.team.lottery.enums.EFundPayType" id="payType"/>
	   			</td>
	   		</tr>
	   		<tr>
	   			<td>关联方式类型：</td>
	   			<td>
	   			<input type="radio"  name="refType" value="TRANSFER" />转账方式
                <input type="radio" name="refType" value="THIRDPAY" />第三方支付方式
	   			</td>
	   		</tr>
	   		<tr>
	   			<td>关联方式：</td>
	   			<td><select id="refId" >
	   			</select></td>
	   		</tr>
	   		<tr>
	   			<td>显示名称：</td>
	   			<td><input id="showName" type="text" size="35"/></td>
	   		</tr>
	   		<tr>
	   			<td>能展示的用户最低星级：</td>
	   			<td><input id="showUserStarLevel" type="text" size="35"/></td>
	   		</tr>
	   		<tr>
	   			<td>能显示的用户最低模式：</td>
	   			<td><input id="showUserSscRebate" type="text" size="35"/></td>
	   		</tr>
	   		<tr>
	   			<td>排序数：</td>
	   			<td><input id="orderNum" type="text" size="35"/></td>
	   		</tr>
	         <tr>
	   			<td>是否启用：</td>
	   			<td>
	   			<select class="select" id="enabled">
	   				<option value="1">启用</option>
	   				<option value="0">停用</option>
	   			</select>
	   			</td>
	   		</tr>
	   		<tr>
	   			<td></td>
	   			<td><input type="button" onclick="rechargeConfigEditPage.saveRechargeConfig()" value="提交"/>&nbsp;&nbsp;&nbsp;&nbsp;<input type="reset" value="重置"/></td>
	   		</tr>
	    </table>
    </form>
</body>
</html>

