<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>网站公告</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    
    
    <link rel="stylesheet" href="<%=path%>/resources/kindeditor-4.1.10/themes/default/default.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" />
	<script charset="utf-8" src="<%=path%>/resources/kindeditor-4.1.10/kindeditor-min.js"></script>
	<script charset="utf-8" src="<%=path%>/resources/kindeditor-4.1.10/lang/zh_CN.js"></script>
    <script>
		var editor;  //editor.html()
		KindEditor.ready(function(K) {
			editor = K.create('textarea[name="content"]', {
				allowFileManager : false
			});
		});
	</script>
	<%
	 String id = request.getParameter("id");  //获取查询的商品ID
	%>
</head>
<body class="gray-bg">
  <div class="wrapper wrapper-content animated fadeIn">
  <form action="javascript:void(0)">
    <div class="row">
    <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
    <div class="col-sm-12">
    <div class="input-group m-b">
    <span class="input-group-addon">商户：</span>
    <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
    </div>
    </div>
    </c:if>
    <div class="col-sm-12">
    <div class="input-group m-b">
    <span class="input-group-addon">等级：</span>
    <input type="text" value="" class="form-control" name="level" id="level">
    <input type="hidden" id="id" name="id">
    </div>
    </div>
    <div class="col-sm-12">
    <div class="input-group m-b">
    <span class="input-group-addon">等级名称：</span>
    <input type="text" value="" class="form-control" name="levelName" id="levelName">
    </div>
    </div>
    <div class="col-sm-12">
    <div class="input-group m-b">
    <span class="input-group-addon">规则权限：</span>
                               <!-- <select class="form-control" id="rules" name="rules">
								<option value="msg_send">信息发送</option>
								<option value="send_redbag">发送红包</option>
							</select> -->
							<input type="checkbox" class=""  value="msg_send" name="rules">信息发送  
                            <input type="checkbox" class=""  value="sendRedBag" name="rules">发送红包
                            <input type="checkbox" class=""  value="sendImg" name="rules">发送图片  
                           
							</div>
    </div>
    <div class="col-sm-12">
     <div class="row">
     <div class="col-sm-6 text-center">
     <button type="button" class="btn btn-w-m btn-white" onclick="chatLevelAuthEditPage.saveChatLevelAuth()">提 交</button></div>
      <div class="col-sm-6 text-center">
      <input type="reset" class="btn btn-w-m btn-white" value="重 置"/>
        </div>
       </div>
      </div>
    </div>
    
    
   
      </form>
  </div>
    <!-- js -->
     <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/chatlevelauth/js/chatlevelauth_edit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerChatLevelAuthAction.js'></script>
    <script type="text/javascript">
    chatLevelAuthEditPage.param.id = <%=id%>;
	</script>
</body>
</html>

 