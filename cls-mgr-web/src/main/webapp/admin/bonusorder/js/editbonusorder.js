function EditBonusOrderPage(){}
var editBonusOrderPage = new EditBonusOrderPage();

editBonusOrderPage.param = {
   	
};

$(document).ready(function() {
	
	var bonusOrderId = editBonusOrderPage.param.id;  //获取查询ID
	if(bonusOrderId != null){
		editBonusOrderPage.editBonusOrder(bonusOrderId);
	}
	
});


/**
 * 保存更新信息
 */
EditBonusOrderPage.prototype.saveData = function(){
	var updateParm={};
	var bonusOrder=getFormObj("#bonusOrderForm");
	
	
	delete bonusOrder.bizSystemName;//删除属性
	if(bonusOrder.lotteryMoney==null || bonusOrder.lotteryMoney.trim()==""){
		swal("请输入周期投注额!");
		$("#lotteryMoney").focus();
		return;
	 }
	if(isNaN(bonusOrder.lotteryMoney)){
		swal("周期投注额必须是数字？");
		$("#lotteryMoney").focus();
		return;
	}
	
	
	if(bonusOrder.gain==null || bonusOrder.gain.trim()==""){
		swal("请输入盈亏!");
		$("#gain").focus();
		return;
	 }
	
	if(isNaN(bonusOrder.gain)){
		swal("盈亏必须是数字？");
		$("#gain").focus();
		return;
	}
	if(bonusOrder.bonusScale==null || bonusOrder.bonusScale.trim()==""){
		swal("请输入分红比例!");
		$("#bonusScale").focus();
		return;
	}
	if(isNaN(bonusOrder.gain)){
		swal("分红比例必须是数字？");
		$("#bonusScale").focus();
		return;
	}
	if(bonusOrder.money==null || bonusOrder.money.trim()==""){
		swal("请输入应发分红!");
		$("#money").focus();
		return;
	}
	if(isNaN(bonusOrder.money)){
		swal("应发分红必须是数字？");
		$("#money").focus();
		return;
	}
	
	updateParm.id=editBonusOrderPage.param.id;
	updateParm.lotteryMoney=bonusOrder.lotteryMoney;
	updateParm.bonusScale=bonusOrder.bonusScale;
	updateParm.gain=bonusOrder.gain;
	updateParm.money=bonusOrder.money;
	updateParm.releaseStatus=bonusOrder.releaseStatus;
	 managerBonusOrderAction.saveOrUpdateBonusOrder(updateParm,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.bonusOrderPage.closeLayer(index);
		   	    }); 
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("保存信息失败.");
		}
     });
};


/**
 * 赋值信息
 */
EditBonusOrderPage.prototype.editBonusOrder = function(id){
	managerBonusOrderAction.getBonusOrderById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			
			$("#id").val(r[1].id);
			$("#bizSystemName").val(r[1].bizSystemName);
			$("#userName").val(r[1].userName);
			$("#belongDate").val(dateFormat(r[1].belongDate,'yyyy-MM-dd'));
			$("#lotteryMoney").val(r[1].lotteryMoney);
			$("#gain").val(r[1].gain);
			$("#bonusScale").val(r[1].bonusScale);
			$("#money").val(r[1].money);
			$("#releaseStatus").val(r[1].releaseStatus);
			
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取信息失败.");
		}
    });
};

