function BonusOrderPage(){}
var bonusOrderPage = new BonusOrderPage();

bonusOrderPage.param = {
   	
};

bonusOrderPage.admin = {
	   	
};
/**
 * 查询参数
 */
bonusOrderPage.queryParam = {

};

//分页参数
bonusOrderPage.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize
};


$(document).ready(function() {

	bonusOrderPage.initdate();
	bonusOrderPage.initTableData(); //初始化查询数据
	
});





/**
 * 初始化列表数据
 */
BonusOrderPage.prototype.initTableData = function() {
	//bonusOrderPage.queryParam = getFormObj($("#queryForm"));
	bonusOrderPage.table = $('#bonusOrderTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"scrollX": true,
		"iDisplayLength" : defaultPageSize,
		"ajax":function (data, callback, settings) {
			bonusOrderPage.pageParam.pageSize = data.length;//页面显示记录条数
			bonusOrderPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerBonusOrderAction.getBonusOrders(bonusOrderPage.queryParam,bonusOrderPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": "bizSystemName","bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "userName",
		            	  render: function(data, type, row, meta) {
		            		   return "<a href='javascript:void(0)' style='color:blue;text-decoration:underline;'  onclick='commonPage.showUserMsgPage(\""+data+"\","+row.userId+")'>"+ row.userName +"</a>";
		            		}
		            },
            		{"data": "fromUserName",
            			render: function(data, type, row, meta) {
            				return "<a href='javascript:void(0)' style='color:blue;text-decoration:underline;'  onclick='commonPage.showUserMsgPage(\""+data+"\","+row.fromUserId+")'>"+ row.fromUserName +"</a>";
            			}
		            },
		            {"data": "startDateStr"},
		            {   
		            	"data": "lotteryMoney",
		            	render: function(data, type, row, meta) {
		            		return row.lotteryMoney.toFixed(3);
	            		}
		            },
		            {   
		            	"data": "gain",
		            	render: function(data, type, row, meta) {
		            		return row.gain.toFixed(3);
	            		}
		            },
		            {   
		            	"data": "bonusScale",
		            	render: function(data, type, row, meta) {
		            		return data+"%";
	            		}
		            },
		            {   
		            	"data": "money",
		            	render: function(data, type, row, meta) {
		            		return row.money.toFixed(3);
	            		}
		            },
		            {"data": "releaseDateStr"},
		            {
		            	"data": "releaseStatus",
		            	render: function(data, type, row, meta) {
		            		if(row.releaseStatus == "NOT_RELEASE" || row.releaseStatus == "OUTTIME_NOT_RELEASE"){
		            			return '<font class="cp-red">'+ row.releaseStatusName +"</font>";
		            		}else if(row.releaseStatus == "RELEASED" || row.releaseStatus == "OUTTIME_RELEASED" || row.releaseStatus == "FORCE_RELEASED"){
		            			return '<font class="cp-green">'+ row.releaseStatusName +"</font>";
		            		}else if(row.releaseStatus == "NEEDNOT_RELEASE"){
		            			return '<font class="cp-yellow">'+ row.releaseStatusName +"</font>";
		            		}else{
		            			return row.releaseStatusName ;
		            		}
	            		}
		            	
		            },
		            {
	                	 "data": "id",
	                	 render: function(data, type, row, meta) {
	                		 var str="";
	                		 //超级系统才有编辑按钮
	                		 if(currentUser.bizSystem=='SUPER_SYSTEM'&&(row.releaseStatus!='RELEASED'&&row.releaseStatus!='OUTTIME_RELEASED'&&row.releaseStatus!='FORCE_RELEASED')){
	                			 str +="<a class='btn btn-info btn-sm btn-bj' onclick='bonusOrderPage.editBonusOrder("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;";
	                		 }
	                		 if(row.releaseStatus=='NOT_RELEASE'||row.releaseStatus=='OUTTIME_NOT_RELEASE'){
	                			 str += "<a class='btn btn-info btn-sm btn-bj' onclick='bonusOrderPage.releaseBounsMoney(\""+row.id+"\",\""+row.releaseState+"\",\""+row.userName+"\")'><i class='fa fa-pencil'></i>&nbsp;发放 </a>&nbsp;";
		                	  }
	                		 return str;
	                	 }
	                }
	            ]
	}).api(); 
}




/**
 * 查询数据
 */
BonusOrderPage.prototype.findBonusOrder = function(){
	bonusOrderPage.queryParam=getFormObj("#queryForm");
	var belongDate=$("#belongDate").val();
	if(belongDate!=null&&belongDate!=''){
		bonusOrderPage.queryParam.belongDate=belongDate.stringToDate();
	}
	bonusOrderPage.table.ajax.reload();
};

/**
 * 发放
 */
BonusOrderPage.prototype.releaseBounsMoney = function(id,releaseState,userName){
	
	 swal({
         title: "您确定要给"+userName+"发放分红吗",
         text: "请谨慎操作！",
         type: "warning",
         showCancelButton: true,
         confirmButtonColor: "#DD6B55",
         cancelButtonText: "取消",
         confirmButtonText: "确定",
         closeOnConfirm: false
     },
     function() {
	   managerBonusOrderAction.releaseBounsMoney(id,releaseState,function(r){
	    		if (r[0] != null && r[0] == "ok") {
	    			swal("发放成功！");
	    			bonusOrderPage.findBonusOrder();
	    		}else if(r[0] != null && r[0] == "error"){
	    			swal(r[1]);
	    		}else{
	    			swal("查询数据失败.");
	    		}
	        });  
      }); 
	
};


BonusOrderPage.prototype.initdate =function (){
	
	var m=20;//m为后台自动获取数据
	
	
	
	var myDate = new Date();
	$("#belongDate").html("");
	var str="<option value=''>归属月份</option>";
	for(var i=2;i<8;i++){
		    var mytime=myDate.format('yyyy-MM');//获取当前日期
		    var year = myDate.getFullYear();//获取当前年
		    var month = myDate.getMonth()+1;//获取当前月
		    var day = myDate.getDate();//获取当前日
		    var lowData='';//当前年月日往前推m个月后获取到的年月日
		    ylow=parseInt(parseInt(i)/12);//往前推的总月份换成对应的年数取整
		    mlow=parseInt(i)%12;//往前推的月数
		    if(ylow>0){
		        year-=ylow;//年要再减一
		    }else{
		        year=year;//年取当前年份
		    }
		    if((mlow>month) || (mlow==month)){//往前推的月份大于或等于当前月份，12减去往前推的月份加上现在的月份
		        year=year-1;
		        month=12-mlow+month;
		        lowData=year+'年'+month+'月'
		    }else{//往前推的月份小于当前月份
		        month-=mlow;
		        lowData=year+'年'+month+'月'
		    }
		    
		     var endDate = new Date();
		     endDate.setDate(1);
		     endDate.setYear(year);
		     endDate.setMonth(month);
		     var endDateStr = endDate.format('yyyy-MM');
		     str +="<option value='"+endDateStr+"'>" + endDateStr +"</option>";
		
	}
	$("#belongDate").append(str);

}



BonusOrderPage.prototype.editBonusOrder=function(id)
{
	  layer.open({
          type: 2,
          title: '契约分红编辑',
          maxmin: false,
          shadeClose: true,
          //点击遮罩关闭层
          area: ['800px', '500px'],
          content: contextPath + "/managerzaizst/bonusorder/"+id+"/editbonusorder.html",
          cancel: function(index){ 
        	  bonusOrderPage.findBonusOrder();
       	   }
      });
}

BonusOrderPage.prototype.closeLayer=function(index){
	layer.close(index);
	bonusOrderPage.findBonusOrder();
}