<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>  
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>契约分红管理</title>
        <link rel="shortcut icon" href="favicon.ico">
        <style type="text/css">
	      .cp-yellow{color:#f8a525}
	      .cp-red{color:red}
	      .cp-green{color:green}
	      </style>
        <jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
    <body>
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
                    <div class="ibox ">
                        <div class="ibox-content">
                            <div class="row m-b-sm m-t-sm">
                                <form class="form-horizontal" id="queryForm">
                                 <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                                    <div class="row">
                                       <div class="col-sm-3">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">商户</span>
                                                 <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">用户名</span>
                                                 <input id="userName" name="userName"  type="text" class="form-control">
                                            </div>
                                        </div>
                                         <div class="col-sm-2">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">发放用户</span>
                                                 <input id="fromUserName" name="fromUserName"  type="text" class="form-control">
                                            </div>
                                        </div>
                                         <div class="col-sm-2">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">归属月份</span>
                                                 <select id="belongDate" name="belongDate" class="ipt form-control"></select>
                                            </div>
                                        </div>
                                         <div class="col-sm-2">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">类型</span>
                                                <select id="releaseStatus" name="releaseStatus" class="ipt form-control">
                                                 <option value="">全部</option>
                                                 <option value="NOT_RELEASE">尚未发放</option>
                                                 <option value="RELEASED">发放完毕</option>
                                                 <option value="NEEDNOT_RELEASE">不需分红</option>
                                                 <option value="FORCE_RELEASED">强制发放完毕</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group nomargin text-right">
                                                <button type="button" class="btn btn-outline btn-default btn-kj" onclick="bonusOrderPage.findBonusOrder()"><i class="fa fa-search"></i>&nbsp;查 询</button></div>
                                        </div>
                                    </div>
                                     </c:if>
                                     <c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
                                        <div class="row">
                                
                                        <div class="col-sm-3">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">账号</span>
                                                 <input id="userName" name="userName"  type="text" class="form-control">
                                            </div>
                                        </div>
                                         <div class="col-sm-3">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">日期</span>
                                                  <select id="belongDate" name="belongDate" class="ipt form-control"></select>
                                            </div>
                                        </div>
                                         <div class="col-sm-3">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">类型</span>
                                                <select id="releaseStatus" name="releaseStatus" class="ipt form-control">
                                                 <option value="">全部</option>
                                                 <option value="NOT_RELEASE">尚未发放</option>
                                                 <option value="RELEASED">发放完毕</option>
                                                 <option value="NEEDNOT_RELEASE">不需分红</option>
                                                 <option value="FORCE_RELEASED">强制发放完毕</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group nomargin text-right">
                                                <button type="button" class="btn btn-outline btn-default btn-kj" onclick="bonusOrderPage.findBonusOrder()"><i class="fa fa-search"></i>&nbsp;查 询</button></div>
                                        </div>
                                    </div>
                                    </c:if>
                                </form>
                            </div>
                            <div class="jqGrid_wrapper">
                                <div class="ui-jqgrid ">
                                    <div class="ui-jqgrid-view">
                                        <div class="ui-jqgrid-hdiv">
                                            <div class="ui-jqgrid-hbox" style="padding-right:0">
                                                <table class="ui-jqgrid-htable ui-common-table table table-bordered" id="bonusOrderTable">
                                                    <thead>
                                                        <tr class="ui-jqgrid-labels">
                                                            <th class="ui-th-column ui-th-ltr">商户</th>
                                                            <th class="ui-th-column ui-th-ltr">用户名</th>
                                                            <th class="ui-th-column ui-th-ltr">发放用户</th>
                                                            <th class="ui-th-column ui-th-ltr">归属月份</th>
                                                            <th class="ui-th-column ui-th-ltr">周期投注额</th>
                                                            <th class="ui-th-column ui-th-ltr">盈亏</th>
                                                            <th class="ui-th-column ui-th-ltr">分红比例</th>
                                                            <th class="ui-th-column ui-th-ltr">应发分红</th>
                                                            <th class="ui-th-column ui-th-ltr">发放时间</th>
                                                            <th class="ui-th-column ui-th-ltr">状态</th>
                                                            <th class="ui-th-column ui-th-ltr">操作</th>
                                                            </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!-- js -->
<script type="text/javascript"
	src="<%=path%>/admin/bonusorder/js/bonusorder.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerBonusOrderAction.js'></script>
	<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerUserAction.js'></script>
</body>
</html>

