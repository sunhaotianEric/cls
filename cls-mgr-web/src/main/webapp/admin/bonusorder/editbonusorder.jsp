<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
    String id = request.getParameter("id")==null?"0":request.getParameter("id"); 
%>
<!DOCTYPE html>
<html>  
    <head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>分红契约编辑</title>
    <link rel="shortcut icon" href="favicon.ico">
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
   <body class="gray-bg">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row back-change">
                 <!-- <div class="ibox-content"> -->
                            <form class="form-horizontal" id="bonusOrderForm">
                            <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">商户：</label>
                                    <div class="col-sm-9">
                                    <input id="bizSystemName" name="bizSystemName" type="text"  class="form-control" disabled="disabled" />
                                    </div>
                                </div>
                                </c:if>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">用户名：</label>
                                    <div class="col-sm-9">
                                        <input id="id" name="id" type="hidden"  />
                                        <input id="userName" name="userName"  type="text" class="form-control" readonly="readonly">
                                     </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">归属日期：</label>
                                    <div class="col-sm-9">
                                     <input id="belongDate" name="belongDate"  type="text" class="form-control" readonly="readonly">
                               
                                     </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">周期投注额：</label>
                                    <div class="col-sm-9">
                                         <input id="lotteryMoney" name="lotteryMoney"  type="text" class="form-control"  >
                                     </div>
                                </div>
                                 <div class="form-group">
                                    <label class="col-sm-3 control-label">盈亏：</label>
                                    <div class="col-sm-9">
                                     <input id="gain" name="gain"  type="text" class="form-control"  >
                                     </div>
                                 </div>
                                  <div class="form-group">
                                    <label class="col-sm-3 control-label">分红比例(%)：</label>
                                    <div class="col-sm-9" >
                                     <input id="bonusScale" name="bonusScale"  type="text" class="form-control"  >
                                     </div>
                                 </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">应发分红：</label>
                                    <div class="col-sm-9" >
                                    <input id="money" name="money"  type="text" class="form-control"  >
                                     </div>
                                 </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">状态：</label>
                                    <div class="col-sm-9">
                                          <select id="releaseStatus" name="releaseStatus" class="ipt form-control">
                                                 <option value="NOT_RELEASE">尚未发放</option>
                                                 <option value="RELEASED">发放完毕</option>
                                                 <option value="NEEDNOT_RELEASE">不需分红</option>
                                                 <option value="OUTTIME_NOT_RELEASE">逾期未发放</option>
                                                 <option value="OUTTIME_RELEASED">逾期补发完毕</option>
                                                 <option value="FORCE_RELEASED">强制发放完毕</option>
                                           </select>
                                     </div>
                                </div>
                               
                                  <div class="col-sm-12">
					                   
					                        <div class="col-sm-6 text-center">
					                            <button type="button" class="btn btn-w-m btn-white" onclick="editBonusOrderPage.saveData()">提 交</button></div>
					                            <div class="col-sm-6 text-center">
					                            <!-- <button type="button" class="btn btn-w-m btn-white">重 置</button> -->
					                            <input type="reset" class="btn btn-w-m btn-white" value="重 置"/>
					                            </div>
					                   
					               </div>
                            </form>
                   <!--  </div> -->
              
            </div>
        </div>
    <script type="text/javascript" src="<%=path%>/admin/bonusorder/js/editbonusorder.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerBonusOrderAction.js'></script>
    <script type="text/javascript">
     editBonusOrderPage.param.id = <%=id%>;
	</script>
</body>
</html>

