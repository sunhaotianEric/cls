function EditSystemConfigPage(){}
var editSystemConfigPage = new EditSystemConfigPage();

editSystemConfigPage.param = {
   	
};

$(document).ready(function() {
	var systemConfigId = editSystemConfigPage.param.id;  //获取查询ID
	if(systemConfigId != null){
		editSystemConfigPage.editSystemConfig(systemConfigId);
	}
	editSystemConfigPage.getSystemConfig();
});


/**
 * 保存更新信息
 */
EditSystemConfigPage.prototype.saveData = function(){
	var id = $("#systemconfigId").val();
	var configKey = $("#configKey").val();
	var configValue = $("#configValue").val();
	var configDes = $("#configDes").val();
	var enable = $("#enable").val();
	
	
	if(configKey==null || configKey.trim()==""){
		swal("请输入参数名!");
		$("#configKey").focus();
		return;
	}
	if(configValue==null || configValue.trim()==""){
		swal("请输入参数值!");
		$("#configValue").focus();
		return;
	}
	
	
	var systemConfig = {};
	systemConfig.id = id;
	systemConfig.configKey = configKey;
	systemConfig.configValue = configValue;
	systemConfig.configDes = configDes;
	systemConfig.enable = enable;
	managerSystemConfigAction.saveOrUpdateSystenConfig(systemConfig,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.systemConfigPage.closeLayer(index);
		   	    }); 
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("保存信息失败.");
		}
    });
};


/**
 * 赋值配置信息
 */
EditSystemConfigPage.prototype.editSystemConfig = function(id){
	managerSystemConfigAction.getSystemConfigById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			$("#systemconfigId").val(r[1].id);
			$("#configKey").val(r[1].configKey);
			$("#configValue").val(r[1].configValue);
			$("#configDes").val(r[1].configDes);
			$("#enable").val(r[1].enable);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取信息失败.");
		}
    });
};

/**
 * 修改系统默认值
 * @param map
 * @return
 */
EditSystemConfigPage.prototype.setSystemConfig = function(){
	var managerLoginSwich=$("#managerLoginSwichId").val();
	var defaultMgrPageSize=$("#defaultMgrPageSizeId").val();
	var secretCode=$("#secretCodeId").val();
	var defaultAdminPassword=$("#defaultAdminPasswordId").val();
	var isStationLogin=$("#isStationLoginId").val();
	var frontDefaultPageSize=$("#frontDefaultPageSizeId").val();
	var maxLoginCount=$("#maxLoginCountId").val();
	//var jlffcProfitModel=$("#jlffcProfitModelId").val();
	//var jyksProfitModel=$("#jyksProfitModelId").val();
	var picHost=$("#picHostId").val();
	var picPassword=$("#picPasswordId").val();
	var picPort=$("#picPortId").val();
	var picUsername=$("#picUsernameId").val();
	var realImgFiledir=$("#realImgFiledirId").val();
	var imgServerUrl=$("#imgServerUrlId").val();
	var bjpk10BaseDate=$("#bjpk10BaseDateId").val();
	var hgffcBaseDate=$("#hgffcBaseDateId").val();
	var bjksBaseDate=$("#bjksBaseDateId").val();
	var xjplfcBaseDate=$("#xjplfcBaseDateId").val();
	var twwfcBaseDate=$("#twwfcBaseDateId").val();
	var xyebBaseDate=$("#xyebBaseDateId").val();
	var pk10BasetExpect=$("#pk10BasetExpectId").val();
	var hgffcBasetExpect=$("#hgffcBasetExpectId").val();
	var bjksBasetExpect=$("#bjksBasetExpectId").val();
	var xjplfcBasetExpect=$("#xjplfcBasetExpectId").val();
	var twwfcBasetExpect=$("#twwfcBasetExpectId").val();
	var xyebBasetExpect=$("#xyebBasetExpectId").val();
	var springFestivalStartDate=$("#springFestivalStartDateId").val();
	var springFestivalEndDate=$("#springFestivalEndDateId").val();
	var maintainSwich=$("#maintainSwichId").val();
	var maintainContent=$("#maintainContentId").val();
	var allowUrls=$("#allowUrlsId").val();
	var lotteryControlConfig=$("#lotteryControlConfigId").val();
	var pcWebRsVersion=$("#pcWebRsVersionId").val();
	var mobileWebRsVersion=$("#mobileWebRsVersionId").val();
	var mgrWebRsVersion=$("#mgrWebRsVersionId").val();
	var chatRoomPort=$("#chatRoomPortId").val();
	var chatRoomIp=$("#chatRoomIpId").val();
	var imgServerUploadUrl=$("#imgServerUploadUrlId").val();
//	var currentZodiac=$("#currentZodiacId").val();
	
	var managerLoginSwichId="managerLoginSwich";
	var defaultMgrPageSizeId="defaultMgrPageSize";
	var secretCodeId="secretCode";
	var defaultAdminPasswordId="defaultAdminPassword";
	var isStationLoginId="isStationLogin";
	var frontDefaultPageSizeId="frontDefaultPageSize";
	var maxLoginCountId="maxLoginCount";
	//var jlffcProfitModelId="jlffcProfitModel";
	//var jyksProfitModelId="jyksProfitModel";
	var picHostId="picHost";
	var picPasswordId="picPassword";
	var picPortId="picPort";
	var picUsernameId="picUsername";
	var realImgFiledirId="realImgFiledir";
	var imgServerUrlId="imgServerUrl";
	var bjpk10BaseDateId="pk10BaseDate";
	var hgffcBaseDateId="hgffcBaseDate";
	var bjksBaseDateId="bjksBaseDate";
	var xjplfcBaseDateId="xjplfcBaseDate";
	var twwfcBaseDateId="twwfcBaseDate";
	var xyebBaseDateId="xyebBaseDate";
	var pk10BasetExpectId="pk10BasetExpect";
	var hgffcBasetExpectId="hgffcBasetExpect";
	var bjksBasetExpectId="bjksBasetExpect";
	var xjplfcBasetExpectId="xjplfcBasetExpect";
	var twwfcBasetExpectId="twwfcBasetExpect";
	var xyebBasetExpectId="xyebBasetExpect";
	var springFestivalStartDateId="springFestivalStartDate";
	var springFestivalEndDateId="springFestivalEndDate";
	var maintainSwichId="maintainSwich";
	var maintainContentId="maintainContent";
	var allowUrlsId="allowUrls";
	var lotteryControlConfigId="lotteryControlConfig";
	var pcWebRsVersionId="pcWebRsVersion";
	var mobileWebRsVersionId="mobileWebRsVersion";
	var mgrWebRsVersionId="mgrWebRsVersion";
	var chatRoomIpId="chatRoomIp";
	var chatRoomPortId="chatRoomPort";
	var imgServerUploadUrlId="imgServerUploadUrl";
//	var currentZodiacId="currentZodiac"
	
	var map = {};
	map[secretCodeId] = secretCode;
	map[defaultAdminPasswordId] = defaultAdminPassword;
	map[isStationLoginId] = isStationLogin;
	map[frontDefaultPageSizeId] = frontDefaultPageSize;
	map[maxLoginCountId] = maxLoginCount;
	//map[jlffcProfitModelId] = jlffcProfitModel;
	//map[jyksProfitModelId] = jyksProfitModel;
	map[picHostId] = picHost;
	map[picPasswordId] = picPassword;
	map[picPortId] = picPort;
	map[picUsernameId] = picUsername;
	map[realImgFiledirId] = realImgFiledir;
	map[imgServerUrlId] = imgServerUrl;
	map[bjpk10BaseDateId] = bjpk10BaseDate;
	map[hgffcBaseDateId] = hgffcBaseDate;
	map[bjksBaseDateId] = bjksBaseDate;
	map[xjplfcBaseDateId] = xjplfcBaseDate;
	map[twwfcBaseDateId] = twwfcBaseDate;
	map[pk10BasetExpectId] = pk10BasetExpect;
	map[hgffcBasetExpectId] = hgffcBasetExpect;
	map[bjksBasetExpectId] = bjksBasetExpect;
	map[xjplfcBasetExpectId] = xjplfcBasetExpect;
	map[twwfcBasetExpectId] = twwfcBasetExpect;
	map[xyebBasetExpectId] = xyebBasetExpect;
	map[springFestivalStartDateId] = springFestivalStartDate;
	map[springFestivalEndDateId] = springFestivalEndDate;
	map[managerLoginSwichId] = managerLoginSwich;
	map[defaultMgrPageSizeId] = defaultMgrPageSize;
	map[maintainSwichId] = maintainSwich;
	map[maintainContentId] = maintainContent;
	map[allowUrlsId] = allowUrls;
	map[lotteryControlConfigId]=lotteryControlConfig;
	map[pcWebRsVersionId]=pcWebRsVersion;
	map[mobileWebRsVersionId]=mobileWebRsVersion;
	map[mgrWebRsVersionId]=mgrWebRsVersion;
	map[xyebBaseDateId] = xyebBaseDate;
	map[chatRoomIpId]=chatRoomIp;
	map[chatRoomPortId]=chatRoomPort;
	map[imgServerUploadUrlId]=imgServerUploadUrl;
//	map[currentZodiacId] = currentZodiac;

	managerSystemConfigAction.updateDefaultSystemConfig(map,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({title: "保存成功",text: "您已经保存了这条信息。",type: "success"});
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("保存信息失败.");
		}
    });
}

/**
 * 获取系统默认值
 */
EditSystemConfigPage.prototype.getSystemConfig = function(){
	managerSystemConfigAction.getAllSystemConfig(function(r){
		if (r[0] != null && r[0] == "ok") {
			editSystemConfigPage.voluationSystemConfig(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取信息失败.");
		}
	});
}

/**
 * 赋值配置信息
 */
EditSystemConfigPage.prototype.voluationSystemConfig = function(systemConfigMap){
	var map = systemConfigMap;
	for (var key in map){
		if(key == "managerLoginSwich"){
			$("#managerLoginSwichId").val(map[key].configValue);
			$("#managerLoginSwich").val(map[key].id);
		}
		if(key == "defaultMgrPageSize"){
			$("#defaultMgrPageSizeId").val(map[key].configValue);
			$("#defaultMgrPageSize").val(map[key].id);
		}
		if(key == "secretCode"){
			$("#secretCodeId").val(map[key].configValue);
			$("#secretCode").val(map[key].id);
		}
		if(key == "defaultAdminPassword"){
			$("#defaultAdminPasswordId").val(map[key].configValue);
			$("#defaultAdminPassword").val(map[key].id);
		}
		if(key == "isStationLogin"){
			$("#isStationLoginId").val(map[key].configValue);
			$("#isStationLogin").val(map[key].id);
		}
		if(key == "frontDefaultPageSize"){
			$("#frontDefaultPageSizeId").val(map[key].configValue);
			$("#frontDefaultPageSize").val(map[key].id);
		}
		if(key == "maxLoginCount"){
			$("#maxLoginCountId").val(map[key].configValue);
			$("#maxLoginCount").val(map[key].id);
		}
		if(key == "jlffcProfitModel"){
			$("#jlffcProfitModelId").val(map[key].configValue);
			$("#jlffcProfitModel").val(map[key].id);
		}
		if(key == "jyksProfitModel"){
			$("#jyksProfitModelId").val(map[key].configValue);
			$("#jyksProfitModel").val(map[key].id);
		}
		if(key == "picHost"){
			$("#picHostId").val(map[key].configValue);
			$("#picHost").val(map[key].id);
		}
		if(key == "picPassword"){
			$("#picPasswordId").val(map[key].configValue);
			$("#picPassword").val(map[key].id);
		}
		if(key == "picPort"){
			$("#picPortId").val(map[key].configValue);
			$("#picPort").val(map[key].id);
		}
		if(key == "picUsername"){
			$("#picUsernameId").val(map[key].configValue);
			$("#picUsername").val(map[key].id);
		}
		
		if(key == "realImgFiledir"){
			$("#realImgFiledirId").val(map[key].configValue);
			$("#realImgFiledir").val(map[key].id);
		}
		if(key == "imgServerUrl"){
			$("#imgServerUrlId").val(map[key].configValue);
			$("#imgServerUrl").val(map[key].id);
		}
		if(key == "pk10BaseDate"){
			$("#bjpk10BaseDateId").val(map[key].configValue);
			$("#bjpk10BaseDate").val(map[key].id);
		}
		if(key == "hgffcBaseDate"){
			$("#hgffcBaseDateId").val(map[key].configValue);
			$("#hgffcBaseDate").val(map[key].id);
		}
		if(key == "bjksBaseDate"){
			$("#bjksBaseDateId").val(map[key].configValue);
			$("#bjksBaseDate").val(map[key].id);
		}
		if(key == "xjplfcBaseDate"){
			$("#xjplfcBaseDateId").val(map[key].configValue);
			$("#xjplfcBaseDate").val(map[key].id);
		}
		if(key == "twwfcBaseDate"){
			$("#twwfcBaseDateId").val(map[key].configValue);
			$("#twwfcBaseDate").val(map[key].id);
		}
		if(key == "xyebBaseDate"){
			$("#xyebBaseDateId").val(map[key].configValue);
			$("#xyebBaseDate").val(map[key].id);
		}
		if(key == "pk10BasetExpect"){
			$("#pk10BasetExpectId").val(map[key].configValue);
			$("#pk10BasetExpect").val(map[key].id);
		}
		if(key == "hgffcBasetExpect"){
			$("#hgffcBasetExpectId").val(map[key].configValue);
			$("#hgffcBasetExpect").val(map[key].id);
		}
		if(key == "bjksBasetExpect"){
			$("#bjksBasetExpectId").val(map[key].configValue);
			$("#bjksBasetExpect").val(map[key].id);
		}
		if(key == "xjplfcBasetExpect"){
			$("#xjplfcBasetExpectId").val(map[key].configValue);
			$("#xjplfcBasetExpect").val(map[key].id);
		}
		if(key == "twwfcBasetExpect"){
			$("#twwfcBasetExpectId").val(map[key].configValue);
			$("#twwfcBasetExpect").val(map[key].id);
		}
		if(key == "xyebBasetExpect"){
			$("#xyebBasetExpectId").val(map[key].configValue);
			$("#xyebBasetExpect").val(map[key].id);
		}
		if(key == "springFestivalStartDate"){
			$("#springFestivalStartDateId").val(map[key].configValue);
			$("#springFestivalStartDate").val(map[key].id);
		}
		if(key == "springFestivalEndDate"){
			$("#springFestivalEndDateId").val(map[key].configValue);
			$("#springFestivalEndDate").val(map[key].id);
		}
		if(key == "maintainSwich"){
			$("#maintainSwichId").val(map[key].configValue);
			$("#maintainSwich").val(map[key].id);
		}
		if(key == "maintainContent"){
			$("#maintainContentId").val(map[key].configValue);
			$("#maintainContent").val(map[key].id);
		}
		if(key == "allowUrls"){
			$("#allowUrlsId").val(map[key].configValue);
			$("#allowUrls").val(map[key].id);
		}
		if(key == "lotteryControlConfig"){
			$("#lotteryControlConfigId").val(map[key].configValue);
			$("#lotteryControlConfig").val(map[key].id);
		}
		if(key == "pcWebRsVersion"){
			$("#pcWebRsVersionId").val(map[key].configValue);
			$("#pcWebRsVersion").val(map[key].id);
		}
		if(key == "mobileWebRsVersion"){
			$("#mobileWebRsVersionId").val(map[key].configValue);
			$("#mobileWebRsVersion").val(map[key].id);
		}
		if(key == "mgrWebRsVersion"){
			$("#mgrWebRsVersionId").val(map[key].configValue);
			$("#mgrWebRsVersion").val(map[key].id);
		}
		if(key == "chatRoomIp"){
			$("#chatRoomIpId").val(map[key].configValue);
			$("#chatRoomIp").val(map[key].id);
		}
		if(key == "chatRoomPort"){
			$("#chatRoomPortId").val(map[key].configValue);
			$("#chatRoomPort").val(map[key].id);
		}
		if (key == "imgServerUploadUrl") {
			$("#imgServerUploadUrlId").val(map[key].configValue);
			$("#imgServerUploadUrl").val(map[key].id)
		}
/*		if(key == "currentZodiac"){
			$("#currentZodiacId").val(map[key].configValue);
			$("#currentZodiac").val(map[key].id);
		}*/
		
	}
}


/**
 * 一键刷新缓存
 */
EditSystemConfigPage.prototype.refreshCache = function(){
	managerSystemConfigAction.refreshCache(function(r){
		if (r[0] != null && r[0] == "ok") {
			swal("刷新成功！");
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取信息失败.");
		}
    });
};
