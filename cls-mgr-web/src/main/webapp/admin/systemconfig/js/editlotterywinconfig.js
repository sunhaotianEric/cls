function EditSystemConfigPage(){}
var editSystemConfigPage = new EditSystemConfigPage();

editSystemConfigPage.param = {
   	
};

$(document).ready(function() {
	editSystemConfigPage.getSystemConfig();
});


/**
 * 保存更新信息
 */
EditSystemConfigPage.prototype.saveData = function(){
	var id = $("#systemconfigId").val();
	var configKey = $("#configKey").val();
	var configValue = $("#configValue").val();
	var configDes = $("#configDes").val();
	var enable = $("#enable").val();
	
	
	if(configKey==null || configKey.trim()==""){
		swal("请输入参数名!");
		$("#configKey").focus();
		return;
	}
	if(configValue==null || configValue.trim()==""){
		swal("请输入参数值!");
		$("#configValue").focus();
		return;
	}
	
	
	var systemConfig = {};
	systemConfig.id = id;
	systemConfig.configKey = configKey;
	systemConfig.configValue = configValue;
	systemConfig.configDes = configDes;
	systemConfig.enable = enable;
	managerSystemConfigAction.saveOrUpdateSystenConfig(systemConfig,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.systemConfigPage.closeLayer(index);
		   	    }); 
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("保存信息失败.");
		}
    });
};


/**
 * 赋值配置信息
 */
EditSystemConfigPage.prototype.editSystemConfig = function(id){
	managerSystemConfigAction.getSystemConfigById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			$("#systemconfigId").val(r[1].id);
			$("#configKey").val(r[1].configKey);
			$("#configValue").val(r[1].configValue);
			$("#configDes").val(r[1].configDes);
			$("#enable").val(r[1].enable);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取信息失败.");
		}
    });
};

/**
 * 修改系统默认值
 * @param map
 * @return
 */
EditSystemConfigPage.prototype.setSystemConfig = function(){
	var jlffcProfitModel=$("#jlffcProfitModelId").val();
	var jyksProfitModel=$("#jyksProfitModelId").val();
	var jspk10ProfitModel=$("#jspk10ProfitModelId").val();
	var sfsscProfitModel=$("#sfsscProfitModelId").val();
	var wfsscProfitModel=$("#wfsscProfitModelId").val();
	var shfsscProfitModel=$("#shfsscProfitModelId").val();
	var sfksProfitModel=$("#sfksProfitModelId").val();
	var wfksProfitModel=$("#wfksProfitModelId").val();
	var sfpk10ProfitModel=$("#sfpk10ProfitModelId").val();
	var wfpk10ProfitModel=$("#wfpk10ProfitModelId").val();
	var shfpk10ProfitModel=$("#shfpk10ProfitModelId").val();
	var sfsyxwProfitModel=$("#sfsyxwProfitModelId").val();
	var wfsyxwProfitModel=$("#wfsyxwProfitModelId").val();
	
	var list=[];
	var json=new Object;
	json.id=$("#lotterid").val();
	json.jlffcProfitModel=jlffcProfitModel;
	json.jyksProfitModel=jyksProfitModel;
	json.jspk10ProfitModel=jspk10ProfitModel;
	json.sfsscProfitModel=sfsscProfitModel;
	json.wfsscProfitModel=wfsscProfitModel;
	json.shfsscProfitModel=shfsscProfitModel;
	json.sfksProfitModel=sfksProfitModel;
	json.wfksProfitModel=wfksProfitModel;
	json.sfpk10ProfitModel=sfpk10ProfitModel;
	json.wfpk10ProfitModel=wfpk10ProfitModel;
	json.shfpk10ProfitModel=shfpk10ProfitModel;
	json.sfsyxwProfitModel=sfsyxwProfitModel;
	json.wfsyxwProfitModel=wfsyxwProfitModel;
	list[list.length]=json;
	managerLotteryControlConfigAction.saveLotteryControlConfig(list,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({title: "保存成功",text: "您已经保存了这条信息。",type: "success"});
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("保存信息失败.");
		}
    });
}

/**
 * 获取系统默认值
 */
EditSystemConfigPage.prototype.getSystemConfig = function(){
	managerLotteryControlConfigAction.getLotteryControlConfigBybizSystem(function(r){
		if (r[0] != null && r[0] == "ok") {
			editSystemConfigPage.voluationSystemConfig(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取信息失败.");
		}
	});
}

/**
 * 赋值配置信息
 */
EditSystemConfigPage.prototype.voluationSystemConfig = function(record){
	if(record!=null){
		$("#jlffcProfitModelId").val(record.jlffcProfitModel);
		$("#jyksProfitModelId").val(record.jyksProfitModel);
		$("#jspk10ProfitModelId").val(record.jspk10ProfitModel);
		$("#lotterid").val(record.id);
	}else{
		$("#lotterid").val(0);
	}
}


/**
 * 一键刷新缓存
 *//*
EditSystemConfigPage.prototype.refreshCache = function(){
	managerSystemConfigAction.refreshCache(function(r){
		if (r[0] != null && r[0] == "ok") {
			swal("刷新成功！");
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取信息失败.");
		}
    });
};*/
