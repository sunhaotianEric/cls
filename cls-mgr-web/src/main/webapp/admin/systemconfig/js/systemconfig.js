function SystemConfigPage(){}
var systemConfigPage = new SystemConfigPage();

systemConfigPage.param = {
   	
};

/**
 * 查询参数
 */
systemConfigPage.queryParam = {
		configKey : null,
		configValue : null
};

//分页参数
systemConfigPage.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize
};


$(document).ready(function() {

	systemConfigPage.initTableData(); //初始化查询数据
});



/**
 * 初始化列表数据
 */
SystemConfigPage.prototype.initTableData = function() {
	systemConfigPage.queryParam = getFormObj($("#queryForm"));
	systemConfigPage.table = $('#systemConfigTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"scrollX": true,
		"iDisplayLength":defaultPageSize,
		"ajax":function (data, callback, settings) {
			systemConfigPage.pageParam.pageSize = data.length;//页面显示记录条数
			systemConfigPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerSystemConfigAction.getAllsystemConfig(systemConfigPage.queryParam,systemConfigPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": "configKey"},
		            {"data": "configValue"},
		            {"data": "configDes"},
		            {  
		            	"data": "enable",
		            	render: function(data, type, row, meta) {
		            		if(data==1){
		            			return '启用';	
		            		}else{
		            			return '禁用';
		            		}
		            		
	            		}
		            },
		            {   
		            	"data": "createDate",
		            	render: function(data, type, row, meta) {
		            		return dateFormat(data,'yyyy-MM-dd hh:mm:ss');
	            		}
		            },
		            {"data": "updateAdmin","bVisible":false},
		            {   
		            	"data": "updateDate","bVisible":false,
		            	render: function(data, type, row, meta) {
		            		return dateFormat(data,'yyyy-MM-dd hh:mm:ss');
	            		}
		            },
		 
		            {
	                	 "data": "id",
	                	 render: function(data, type, row, meta) {
	                		 return "<a class='btn btn-info btn-sm btn-bj' onclick='systemConfigPage.editSystemConfig("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;" +
	                		 		"<a class='btn btn-danger btn-sm btn-del' onclick='systemConfigPage.delSystemConfig("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";
	                	 }
	                }
	            ]
	}).api(); 
}


/**
 * 查询数据
 */
SystemConfigPage.prototype.findSystemConfig = function(){
	systemConfigPage.queryParam.configKey = $("#configKey").val();
	systemConfigPage.queryParam.configValue = $("#configValue").val();
	systemConfigPage.queryParam.enable = $("#enable").val();
	systemConfigPage.table.ajax.reload();
};



/**
 * 删除
 */
SystemConfigPage.prototype.delSystemConfig = function(id){
	   swal({
           title: "您确定要删除这条信息吗",
           text: "请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "删除",
           closeOnConfirm: false
       },
       function() {
		managerSystemConfigAction.delSystemConfigById(id,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "删除成功",type: "success"});
				systemConfigPage.findSystemConfig();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("删除信息失败.");
			}
	    });
	});
};

SystemConfigPage.prototype.addSystemConfig=function()
{
	  layer.open({
          type: 2,
          title: '系统参数新增',
          maxmin: false,
          shadeClose: true,
          //点击遮罩关闭层
          area: ['800px', '380px'],
          content: contextPath + "/managerzaizst/systemconfig/editsystemconfig.html",
          cancel: function(index){ 
        	  systemConfigPage.findSystemConfig();
       	   }
      });
}

SystemConfigPage.prototype.editSystemConfig=function(id)
{
	  layer.open({
          type: 2,
          title: '系统参数编辑',
          maxmin: false,
          shadeClose: true,
          //点击遮罩关闭层
          area: ['800px', '380px'],
          content: contextPath + "/managerzaizst/systemconfig/"+id+"/editsystemconfig.html",
          cancel: function(index){ 
        	  systemConfigPage.findSystemConfig();
       	   }
      });
}

SystemConfigPage.prototype.closeLayer=function(index){
	layer.close(index);
	systemConfigPage.findSystemConfig();
}