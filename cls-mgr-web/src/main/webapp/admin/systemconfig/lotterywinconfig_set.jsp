<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
  <link rel="shortcut icon" href="favicon.ico">
  <jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeIn">
      <div class="tabs-container ibox">
<!--           <ul class="nav nav-tabs">
              <li class="active">
                  <a data-toggle="tab" href="#tab-5" aria-expanded="false">自身彩种盈利模式配置</a></li>
          </ul> -->
          <div class="tab-content">
            <div id="tab-5" class="tab-pane active">
                <div class="panel-body">
                    <form class="form-horizontal">
                    	<div class="row">
                            <div  class="col-sm-12" >
                                <div class="form-group col-sm-4">
                                    <input id="lotterid" name="" type="hidden"  />
                                    <label class="col-sm-5 control-label">幸运快3彩盈利模式:</label>
                                    <div class="col-sm-7">
                                        <select class="ipt" id="jyksProfitModelId">
                                            <option value="0" selected="selected">开启随机模式</option>
                                            <option value="1">开启随机盈利模式</option>
                                            <option value="2">开启绝对盈利模式</option>
                                            </select>
                                    </div>
                                </div>
                                <div class="form-group col-sm-4">
                                    <label class="col-sm-5 control-label">幸运分分彩盈利模式:</label>
                                    <div class="col-sm-7">
                                        <select class="ipt" id="jlffcProfitModelId">
                                            <option value="0" selected="selected">开启随机模式</option>
                                            <option value="1">开启随机盈利模式</option>
                                            <option value="2">开启绝对盈利模式</option>
                                        </select>
                                    </div>
                                 </div>
                                 <div class="form-group col-sm-4">
                                    <label class="col-sm-5 control-label">极速pk盈利模式:</label>
                                    <div class="col-sm-7">
                                        <select class="ipt" id="jspk10ProfitModelId">
                                            <option value="0" selected="selected">开启随机模式</option>
                                            <option value="1">开启随机盈利模式</option>
                                            <option value="2">开启绝对盈利模式</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- <div class="form-group small-col-sm-3">
                                	
                                </div> -->
                            </div>
                        </div>
                    </form>
                </div>
            </div>     
        </div>
    </div>
    <div class="col-sm-12 ibox">
        <div class="row">
            <div class="col-sm-12 text-center">
                <button type="button" class="btn btn-w-m btn-white" onclick="editSystemConfigPage.setSystemConfig()">保 存</button></div>
             <!-- <div class="col-sm-6 text-center">
                <button type="button" class="btn btn-w-m btn-white" onclick="editSystemConfigPage.refreshCache()">一键更新缓存</button></div>  -->
        </div>
    </div>
</div>

    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/systemconfig/js/editlotterywinconfig.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerLotteryControlConfigAction.js'></script>     
</body>
</html>