<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
  <link rel="shortcut icon" href="favicon.ico">
  <jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeIn">
      <div class="tabs-container ibox">
          <ul class="nav nav-tabs">
              <li class="active">
                  <a data-toggle="tab" href="#tab-1" aria-expanded="true">商户默认值</a></li>
              <li class="">
                  <a data-toggle="tab" href="#tab-2" aria-expanded="false">图片服务配置</a></li>
             <!--  <li class="">
                  <a data-toggle="tab" href="#tab-3" aria-expanded="false">金鹰金额控制</a></li> -->
              <li class="">
                  <a data-toggle="tab" href="#tab-4" aria-expanded="false">期号配置</a></li>
      <!--         <li class="">
                  <a data-toggle="tab" href="#tab-5" aria-expanded="false">自身彩种盈利模式配置</a></li> -->
              <li class="">
                  <a data-toggle="tab" href="#tab-6" aria-expanded="false">春节时间设置</a></li>
              <!-- <li class="">
                  <a data-toggle="tab" href="#tab-7" aria-expanded="false">六合生肖设置</a></li> -->
              <li class="">
                  <a data-toggle="tab" href="#tab-8" aria-expanded="false">维护设置</a></li>
               <li class="">
                  <a data-toggle="tab" href="#tab-9" aria-expanded="false">其他设置</a></li> 
               <c:if test="${admin.bizSystem=='SUPER_SYSTEM' }">
               <li class="">
                  <a data-toggle="tab" href="#tab-10" aria-expanded="false">商户盈利模式设置</a>
               </li>
               <li class="">
                  <a data-toggle="tab" href="#tab-11" aria-expanded="false">聊天服务配置</a>
               </li>
               </c:if>
          </ul>
          <div class="tab-content">
              <div id="tab-1" class="tab-pane active">
                  <div class="panel-body">
                      <form class="form-horizontal">
                          <div class="row">
                          	  <div class="col-sm-6">
                                 <div class="form-group">
                                 	<input id="secretCode" name="" type="hidden"  />
                                    <label class="col-sm-3 control-label">秘密验证码:</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" style="width:180px" value="" class="ipt" id="secretCodeId">
                                    </div>
                                </div>
                          	  	<div class="form-group">
                                 	<input id="defaultMgrPageSize" name="" type="hidden"  />
                                    <label class="col-sm-3 control-label">后台商户分页条数:</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" style="width:180px" value="" class="ipt" id="defaultMgrPageSizeId">
                                    </div>
                                </div>
                                <div class="form-group">
                                	<input id="isStationLogin" name="" type="hidden"  />
                                    <label class="col-sm-3 control-label">单点登陆开关:</label>
                                    <div class="col-sm-5">
                                        <select class="ipt" id="isStationLoginId">
                                        	 <option value="">请选择</option>
	                                         <option value="1">开启</option>
	                                         <option value="0">关闭</option>
                                         </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                	<input id="managerLoginSwich" name="" type="hidden"  />
                                    <label class="col-sm-3 control-label">后台登陆开关:</label>
                                    <div class="col-sm-5">
                                        <select class="ipt" id="managerLoginSwichId">
                                        	 <option value="">请选择</option>
	                                         <option value="1">开启</option>
	                                         <option value="0">关闭</option>
                                         </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                 	<input id="pcWebRsVersion" name="" type="hidden"  />
                                    <label class="col-sm-3 control-label">PC站点资源版本号:</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" style="width:180px" value="" class="ipt" id="pcWebRsVersionId">
                                    </div>
                                </div>
                                <div class="form-group">
                                 	<input id="mobileWebRsVersion" name="" type="hidden"  />
                                    <label class="col-sm-3 control-label">手机站点资源版本号:</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" style="width:180px" value="" class="ipt" id="mobileWebRsVersionId">
                                    </div>
                                </div>
                                <div class="form-group">
                                 	<input id="mgrWebRsVersion" name="" type="hidden"  />
                                    <label class="col-sm-3 control-label">后台站点资源版本号:</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" style="width:180px" value="" class="ipt" id="mgrWebRsVersionId">
                                    </div>
                                </div>
                              </div>
                                    
                              <div class="col-sm-6">
                                <div class="form-group">
                                	<input id="defaultAdminPassword" name="" type="hidden"  />
                                    <label class="col-sm-3 control-label">默认管理员密码:</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" style="width:180px" value="" class="ipt" id="defaultAdminPasswordId">
                                    </div>
                                </div>
                                <div class="form-group">
                                	<input id="frontDefaultPageSize" name="" type="hidden"  />
                                    <label class="col-sm-3 control-label">前台分页条数:</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" style="width:180px" value="" class="ipt" id="frontDefaultPageSizeId">
                                    </div>
                                </div>
                                <div class="form-group">
                                	<input id="maxLoginCount" name="" type="hidden"  />
                                    <label class="col-sm-3 control-label">最大登陆验证次数:</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" style="width:180px" value="" class="ipt" id="maxLoginCountId">
                                    </div>
                                </div>
                              </div>
                        </div>
                    </form>
                </div>
            </div>
            <div id="tab-2" class="tab-pane">
                <div class="panel-body">
                    <form class="form-horizontal">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                	<input id="picHost" name="" type="hidden"  />
                                    <label class="col-sm-3 control-label">FTP访问IP:</label>
                                    <div class="col-sm-8">
                                        <input type="text" value="10000" class="form-control"  id="picHostId"></div>
                                </div>
                                <div class="form-group">
                                	<input id="picPassword" name="" type="hidden"  />
                                    <label class="col-sm-3 control-label">FTP访问密码:</label>
                                    <div class="col-sm-8">
                                        <input type="text" value="50000" class="form-control" id="picPasswordId"></div>
                                </div>
                                <div class="form-group">
                                	<input id="picPort" name="" type="hidden"  />
                                    <label class="col-sm-3 control-label">FTP端口:</label>
                                    <div class="col-sm-8">
                                        <input type="text" value="300000" class="form-control" id="picPortId"></div>
                                </div>
                                <div class="form-group">
                                	<input id="picUsername" name="" type="hidden"  />
                                    <label class="col-sm-3 control-label">FTP用户名:</label>
                                    <div class="col-sm-8">
                                        <input type="text" value="300000" class="form-control" id="picUsernameId"></div>
                                </div>
                                <div class="form-group">
                                	<input id="realImgFiledir" name="" type="hidden"  />
                                    <label class="col-sm-3 control-label">上传图片路径:</label>
                                    <div class="col-sm-8">
                                        <input type="text" value="300000" class="form-control" id="realImgFiledirId"></div>
                                </div>
                                 <div class="form-group">
                                 	<input id="imgServerUrl" name="" type="hidden"  />
                                    <label class="col-sm-3 control-label">图片访问URL:</label>
                                    <div class="col-sm-8">
                                        <input type="text" value="300000" class="form-control" id="imgServerUrlId">
                                    </div>
                                </div>
                                 <div class="form-group">
                                 	<input id="imgServerUploadUrl" name="" type="hidden"  />
                                    <label class="col-sm-3 control-label">图片上传URL:</label>
                                    <div class="col-sm-8">
                                        <input type="text" value="300000" class="form-control" id="imgServerUploadUrlId">
                                     </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
<!--             <div id="tab-2" class="tab-pane">
                <div class="panel-body">
                    <form class="form-horizontal">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">每天取现次数:</label>
                                    <div class="col-sm-8">
                                        <input type="text" value="6" class="form-control"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">取现单笔最低金额:</label>
                                    <div class="col-sm-8">
                                        <input type="text" value="100" class="form-control"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">取现单笔最高金额:</label>
                                    <div class="col-sm-8">
                                        <input type="text" value="330000" class="form-control"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">每天取现总金额:</label>
                                    <div class="col-sm-8">
                                        <input type="text" value="2000000" class="form-control"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">宝付快捷充值最少金额:</label>
                                    <div class="col-sm-8">
                                        <input type="text" value="50" class="form-control"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">宝付快捷充值最多金额:</label>
                                    <div class="col-sm-8">
                                        <input type="text" value="50000" class="form-control"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">超过可提现的手续费:</label>
                                    <div class="col-sm-8">
                                        <input type="text" value="3" class="form-control"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">上下级转账开关:</label>
                                    <div class="col-sm-8">
                                        <select class="ipt" id="allowZhuihao">
                                            <option value="1" selected="selected">开启</option>
                                            <option value="0" selected="selected">关闭</option></select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">每日转账限额:</label>
                                    <div class="col-sm-8">
                                        <input type="text" value="5000000" class="form-control"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">闪付快捷充值最少金额:</label>
                                    <div class="col-sm-8">
                                        <input type="text" value="10" class="form-control"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">闪付快捷充值最多金额:</label>
                                    <div class="col-sm-8">
                                        <input type="text" value="3000" class="form-control"></div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div> -->
            <div id="tab-4" class="tab-pane">
                <div class="panel-body">
                    <form class="form-horizontal">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                	<input id="bjpk10BaseDate" name="" type="hidden"  />
                                    <label class="col-sm-3 control-label">PK10基准日期:</label>
                                    <div class="col-sm-8">
                                        <input class="laydate-icon form-control layer-date" onclick="laydate({istime: false, format: 'YYYY-MM-DD'})" id="bjpk10BaseDateId"></div>
                                </div>
                                <div class="form-group">
                                	<input id="hgffcBaseDate" name="" type="hidden"  />
                                    <label class="col-sm-3 control-label">韩国1.5分彩基准日期:</label>
                                    <div class="col-sm-8">
                                        <input class="laydate-icon form-control layer-date" onclick="laydate({istime: false, format: 'YYYY-MM-DD'})" id="hgffcBaseDateId"></div>
                                </div>
                                <div class="form-group">
                                	<input id="bjksBaseDate" name="" type="hidden"  />
                                    <label class="col-sm-3 control-label">北京快3基准日期:</label>
                                    <div class="col-sm-8">
                                        <input class="laydate-icon form-control layer-date" onclick="laydate({istime: false, format: 'YYYY-MM-DD'})" id="bjksBaseDateId"></div>
                                </div>
                                <div class="form-group">
                                	<input id="xjplfcBaseDate" name="" type="hidden"  />
                                    <label class="col-sm-3 control-label">新加坡2分彩基准日期:</label>
                                    <div class="col-sm-8">
                                        <input class="laydate-icon form-control layer-date" onclick="laydate({istime: false, format: 'YYYY-MM-DD'})" id="xjplfcBaseDateId"></div>
                                </div>
                                <div class="form-group">
                                	<input id="twwfcBaseDate" name="" type="hidden"  />
                                    <label class="col-sm-3 control-label">台湾5分彩基准日期:</label>
                                    <div class="col-sm-8">
                                        <input class="laydate-icon form-control layer-date" onclick="laydate({istime: false, format: 'YYYY-MM-DD'})" id="twwfcBaseDateId"></div>
                                </div>
                                <div class="form-group">
                                	<input id="xyebBaseDate" name="" type="hidden"  />
                                    <label class="col-sm-3 control-label">幸运28基准日期:</label>
                                    <div class="col-sm-8">
                                        <input class="laydate-icon form-control layer-date" onclick="laydate({istime: false, format: 'YYYY-MM-DD'})" id="xyebBaseDateId"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                	<input id="pk10BasetExpect" name="" type="hidden"  />
                                    <label class="col-sm-3 control-label">PK10基准日期数:</label>
                                    <div class="col-sm-8">
                                        <input type="text" value="537523" class="form-control" id="pk10BasetExpectId"></div>
                                </div>
                                <div class="form-group">
                                	<input id="hgffcBasetExpect" name="" type="hidden"  />
                                    <label class="col-sm-3 control-label">韩国1.5分彩基准日期数:</label>
                                    <div class="col-sm-8">
                                        <input type="text" value="1655485" class="form-control" id="hgffcBasetExpectId"></div>
                                </div>
                                <div class="form-group">
                                	<input id="bjksBasetExpect" name="" type="hidden"  />
                                    <label class="col-sm-3 control-label">北京快3基准日期数:</label>
                                    <div class="col-sm-8">
                                        <input type="text" value="55800" class="form-control"  id="bjksBasetExpectId"></div>
                                </div>
                                <div class="form-group">
                                	<input id="xjplfcBasetExpect" name="" type="hidden"  />
                                    <label class="col-sm-3 control-label">新加坡2分彩基准日期数:</label>
                                    <div class="col-sm-8">
                                        <input type="text" value="2578187" class="form-control" id="xjplfcBasetExpectId"></div>
                                </div>
                                <div class="form-group">
                                	<input id="twwfcBasetExpect" name="" type="hidden"  />
                                    <label class="col-sm-3 control-label">台湾5分彩基准日期数:</label>
                                    <div class="col-sm-8">
                                        <input type="text" value="105057246" class="form-control" id="twwfcBasetExpectId"></div>
                                </div>
                               <div class="form-group">
                                	<input id="xyebBasetExpect" name="" type="hidden"  />
                                    <label class="col-sm-3 control-label">幸运28基准日期数:</label>
                                    <div class="col-sm-8">
                                        <input type="text" value="" class="form-control" id="xyebBasetExpectId"></div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
<!--             <div id="tab-5" class="tab-pane">
                <div class="panel-body">
                    <form class="form-horizontal">
                    	<div class="row">
                            <div class="col-sm-6">
                                <div class="form-group small-col-sm-3">
                                	<input id="jlffcProfitModel" name="" type="hidden"  />
                                    <label class="col-sm-3 control-label">幸运分分彩盈利模式:</label>
                                    <div class="col-sm-8">
                                        <select class="ipt" id="jlffcProfitModelId">
                                            <option value="0" selected="selected">开启随机模式</option>
                                            <option value="1">开启随机盈利模式</option>
                                            <option value="2">开启绝对盈利模式</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group small-col-sm-3">
                                	<input id="jyksProfitModel" name="" type="hidden"  />
                                    <label class="col-sm-3 control-label">幸运快3彩盈利模式:</label>
                                    <div class="col-sm-8">
                                        <select class="ipt" id="jyksProfitModelId">
                                            <option value="0" selected="selected">开启随机模式</option>
                                            <option value="1">开启随机盈利模式</option>
                                            <option value="2">开启绝对盈利模式</option>
                                            </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div> -->
            <div id="tab-6" class="tab-pane">
                <div class="panel-body">
                    <form class="form-horizontal">
                        <div class="row">
	                        <div class="col-sm-6">
	                        	<input id="springFestivalStartDate" name="" type="hidden"  />
	                            <label class="col-sm-3 control-label">春节开始时间:</label>
	                            <div class="col-sm-4">
	                            	<input class="laydate-icon form-control layer-date" onclick="laydate({istime: false, format: 'YYYY-MM-DD'})" id="springFestivalStartDateId"></div>
	                        </div>
							<div class="col-sm-6"> 
                              	<input id="springFestivalEndDate" name="" type="hidden"  />
                            		<label class="col-sm-3 control-label">春节结束时间:</label>
                            	<div class="col-sm-4">
                                	<input class="laydate-icon form-control layer-date" onclick="laydate({istime: false, format: 'YYYY-MM-DD'})" id="springFestivalEndDateId"></div>
                        	</div>
                        </div>
                    </form>
                </div>
            </div>
            <div id="tab-7" class="tab-pane">
               <div class="panel-body">
                    <form class="form-horizontal">
                    	<div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                	<input id="currentZodiac" name="" type="hidden"  />
                                    <label class="col-sm-3 control-label">当前生肖:</label>
                                    <div class="col-sm-8">
                                        <select class="ipt" style="width: 50px;" id="currentZodiacId">
                                            <option value="SHU" selected="selected">鼠</option>
                                            <option value="NIU">牛</option>
                                            <option value="HU">虎</option>
                                            <option value="TU">兔</option>
                                            <option value="LONG">龙</option>
                                            <option value="SHE">蛇</option>
                                            <option value="MA">马</option>
                                            <option value="YANG">羊</option>
                                            <option value="HOU">猴</option>
                                            <option value="JI">鸡</option>
                                            <option value="GOU">狗</option>
                                            <option value="ZHU">猪</option>
                                        </select>
                                    </div>
                                </div>
                         <!--        <div class="form-group">
                                	<input id="jyksProfitModel" name="" type="hidden"  />
                                    <label class="col-sm-3 control-label">幸运快3彩盈利模式:</label>
                                    <div class="col-sm-8">
                                        <select class="ipt" id="jyksProfitModelId">
                                            <option value="0" selected="selected">开启绝对盈利模式</option>
                                            <option value="1">开启随机盈利模式</option>
                                            <option value="2">开启随机模式</option></select>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
           <div id="tab-8" class="tab-pane">
              <div class="panel-body">
                  <form class="form-horizontal">
                      <div class="row">
                          <div class="col-sm-3">
                              <div class="form-group">
                              	  <input id="maintainSwich" name="" type="hidden"  />
                                  <label class="col-sm-3 control-label">平台维护开关:</label>
                                  <div class="col-sm-8">
                                      <select class="ipt" id="maintainSwichId">
                                      	   <option value="">请选择</option>
		                                   <option value="1">开启</option>
		                                   <option value="0">关闭</option>
	                                  </select>
                                  </div>
                              </div>
                          </div>
                          <div class="col-sm-8">
                              <div class="form-group">
                              	  <input id="maintainContent" name="" type="hidden"  />
                                  <label class="col-sm-3 control-label">平台维护说明:</label>
                                  <div class="col-sm-8">
                                      <textarea name="comment" class="form-control" required="" style="height: 150px;width:660px" aria-required="true" id="maintainContentId"></textarea>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
          <div id="tab-9" class="tab-pane">
              <div class="panel-body">
                  <form class="form-horizontal">
                      <div class="row">
                          <div class="col-sm-8">
                              <div class="form-group">
                              	  <input id="allowUrls" name="" type="hidden"  />
                                  <label class="col-sm-3 control-label">允许访问的域名:</label>
                                  <div class="col-sm-8">
                                      <textarea name="comment" class="form-control" required="" style="height: 150px;width:660px" aria-required="true" id="allowUrlsId"></textarea>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
          <c:if test="${admin.bizSystem=='SUPER_SYSTEM' }">
          <div id="tab-10" class="tab-pane">
              <div class="panel-body">
                  <form class="form-horizontal">
                      <div class="row">
                          <div class="col-sm-8">
                              <div class="form-group">
                              	  <input id="lotteryControlConfig" name="" type="hidden"  />
                                  <label class="col-sm-3 control-label">模式控制:</label>
                                  <div class="col-sm-8">
                                      <select class="ipt" style="width: 200px;" id="lotteryControlConfigId">
                                      		<option value="">请选择</option>
                                            <option value="0">全局控制</option>
                                            <option value="1">子商户控制</option>
                                        </select>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
          </c:if>
          <div id="tab-11" class="tab-pane">
              <div class="panel-body">
                  <form class="form-horizontal">
                      <div class="row">
                          <div class="form-group">
                                   <input id="chatRoomIp" name="" type="hidden"  />
                                   <label class="col-sm-3 control-label">聊天服务器IP:</label>
                                   <div class="col-sm-8">
                                   <input type="text" value="" class="form-control" id="chatRoomIpId"></div>
                          </div>
                          <div class="form-group">
                                  <input id="chatRoomPort" name="" type="hidden"  />
                                  <label class="col-sm-3 control-label">聊天服务器端口:</label>
                                  <div class="col-sm-8">
                                  <input type="text" value="" class="form-control" id="chatRoomPortId"></div>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
        </div>
    </div>
    <div class="col-sm-12 ibox">
        <div class="row">
            <div class="col-sm-6 text-center">
                <button type="button" class="btn btn-w-m btn-white" onclick="editSystemConfigPage.setSystemConfig()">保 存</button></div>
             <div class="col-sm-6 text-center">
                <button type="button" class="btn btn-w-m btn-white" onclick="editSystemConfigPage.refreshCache()">一键更新缓存</button></div> 
        </div>
    </div>
</div>

    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/systemconfig/js/editsystemconfig.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerSystemConfigAction.js'></script>    
</body>
</html>