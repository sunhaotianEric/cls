<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<html>
<head>
    <title>后台管理商户</title>
    <link rel="stylesheet" type="text/css" href="<%=path %>/admin/resource/Styles/base.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" />
    <link rel="stylesheet" type="text/css" href="<%=path %>/admin/resource/Styles/admin-all.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" />
    <link rel="stylesheet" type="text/css" href="<%=path %>/admin/resource/Styles/bootstrap.min.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" />
    <script type="text/javascript" src="<%=path %>/admin/resource/Scripts/jquery-1.7.2.js"></script>
    <script type="text/javascript" src="<%=path %>/admin/resource/Scripts/jquery.spritely-0.6.js"></script>
    <link rel="stylesheet" type="text/css" href="<%=path %>/admin/resource/Styles/login.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" />
	
	<!-- DWR -->
	<script type='text/javascript' src='<%=path%>/dwr/engine.js'></script>
	<script type='text/javascript' src='<%=path%>/dwr/util.js'></script>

	<!-- js -->
	<script type="text/javascript" src="<%=path%>/admin/loginzsxszx163/js/loginzsxszx163.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
	<!-- dwr -->
	<script type='text/javascript' src='<%=path%>/dwr/interface/managerAdminAction.js'></script>
    <script type="text/javascript">
	    function checkCodeRefresh() {
	    	$('#codeImg').attr('src', contextPath + "/imageServlet?time="+Math.random());
	    	//document.getElementById("codeImg").src = "imageServlet?time="+Math.random();
	    }
    
        $(function () {
            $('.login').click(function () {
                if ($('#uid').val() == "" || $('#pwd').val() == "" || $('#code').val() == "") { 
                	$('.tip').html('用户名或密码不可为空！');
                }else if($('#code').val() == ""){
                	$('.tip').html('验证码不可为空！');
                }
            });
        });
    </script>
</head>
<script type="text/javascript">
  var contextPath = '<%=path %>';
</script>
<body>
    <div class="row-fluid">
        <h1>后台管理商户</h1>
        <p>
            <label>帐&nbsp;&nbsp;&nbsp;号：<input type="text" id="userName" value="lotterychenhsh" style="height: 28px;"/></label>
        </p>
        <p>
            <label>密&nbsp;&nbsp;&nbsp;码：<input type="password" id="userPassword" value="lotterychenhsh" style="height: 28px;"/></label>
        </p>
        <p class="pcode">
            <label>图片验证码：<input type="text" id="code" maxlength="5" class="code" style="height: 28px;" /><img id='codeImg' src="<%=path %>/imageServlet" class="imgcode" /><a href="javascript:void(0)"  onclick="checkCodeRefresh()">换一张</a></label>
        </p>
        <p class="pcode" style="margin-left:-90px;">
            <label>秘密验证码：<input type="text" id="secretCode" class="code" style="height: 28px;" value="xiangniu"/>&nbsp;&nbsp;<input type="button" id='secretSendButton' value="点击发送 " class="btn btn-small" /></label>
        </p>
        <p class="tip">&nbsp;</p>
        <input type="button" value=" 登 录 " id='loginButton' class="btn btn-primary btn-large login" />
        &nbsp;&nbsp;&nbsp;
        <!--  
        <input type="button" value=" 取 消 " class="btn btn-large" />
        -->
    </div>
</body>
</html>

