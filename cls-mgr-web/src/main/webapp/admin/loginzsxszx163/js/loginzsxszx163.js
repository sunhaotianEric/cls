function LoginPage(){}
var loginPage = new LoginPage();


$(document).ready(function() {
	//回车提交事件
    $("body").keydown(function() {
        if (event.keyCode == "13") {//keyCode=13是回车键
        	loginPage.loginto();
        }
    });	
    
    //登陆按钮事件
    $("#loginButton").unbind("click").click(function(){
    	loginPage.loginto();
	});
    
    //秘密验证码按钮事件
    $("#secretSendButton").unbind("click").click(function(){
    	loginPage.sendSecretCode();
	});
    
});

/**
 * 管理员登录验证
 */
LoginPage.prototype.loginto = function(){
	var userName = $("#userName").val();
	var userPassword = $("#userPassword").val();
	var code = $("#code").val();
	var secretCode = $("#secretCode").val();

	
	if(userName == null || userName == ""){
		alert("用户名不能为空.");
		return;
	}
	if(userPassword == null || userPassword == ""){
		alert("密码不能为空.");
		return;
	}
	/*if(code == null || code == ""){
		alert("验证码不能为空.");
		return;
	}
	if(secretCode == null || secretCode == ""){
		alert("秘密验证码不能为空.");
		return;
	}*/
	
	var loginParam = {};
	loginParam.username = userName;
	loginParam.password = userPassword;
	loginParam.checkCode = code;
	loginParam.secretCode = secretCode;
	
	managerAdminAction.login(loginParam,function(r){
		if (r[0] != null && r[0] == "ok") {
			window.location.href= "mainwbdzst.html";
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询管理员数据失败.");
		}
		checkCodeRefresh();
    });
};

var time = 5;
var interval = 0;

/**
 * 发送秘密编码
 */
LoginPage.prototype.sendSecretCode = function(){
	$("#secretSendButton").attr('disabled','disabled');
	$("#secretSendButton").val("已发送");
	managerAdminAction.sendSecretCode(function(r){
		if (r[0] != null && r[0] == "ok") {
			interval = setInterval(function(){
				if(time == 0){
					$("#secretSendButton").removeAttr("disabled");
					$("#secretSendButton").val("点击发送");
					time = 5;
					clearInterval(interval);
				}else{
					$("#secretSendButton").val("已发送("+time+")");
				}
				time--;
			},1000);
		}else if(r[0] != null && r[0] == "error"){
			alert("发送失败.");
		}else{
			interval = setInterval(function(){
				if(time == 0){
					$("#secretSendButton").removeAttr("disabled");
					$("#secretSendButton").val("点击发送");
					time = 5;
					clearInterval(interval);
				}else{
					$("#secretSendButton").val("已发送("+time+")");
				}
				time--;
			},1000);
		}
    });
};

