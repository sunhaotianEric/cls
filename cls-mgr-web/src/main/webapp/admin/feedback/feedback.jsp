<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>  
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>反馈信息管理</title>
        <link rel="shortcut icon" href="favicon.ico">
         <link rel="stylesheet" href="../css/feedback/layer.css">
        <jsp:include page="/admin/include/include.jsp"></jsp:include>
    </head>
    <body>
        <div class="animated fadeIn">
            <div class="ibox-content">
                 <div class="row m-b-sm m-t-sm">
                    <form class="form-horizontal" id="feedbackForm">
                    
                        <div class="row">
	                       <div class="col-sm-3 biz" >
		                       <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">商户：</span>
                                     <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
                                </div>
                                <div class="col-sm-3">
	                                <div class="form-group nomargin text-right">
	                                    <button type="button" class="btn btn-outline btn-default btn-js" onclick="feedbackPage.findFeedBackControl()"><i class="fa fa-search"></i>&nbsp;查 询</button>
	                                 </div>
	                            </div>
                                </c:if>
		                   </div>     
                         </div>
                         
                    </form>
                    </div>
                     <div class="jqGrid_wrapper">
                        <div class="ui-jqgrid ">
                            <div class="ui-jqgrid-view">
                                <div class="ui-jqgrid-hdiv">
                                    <div class="ui-jqgrid-hbox" style="padding-right:0">
                                        <table id="feedbackPageTable" class="ui-jqgrid-htable ui-common-table table table-bordered">
                                            <thead>
                                                <tr class="ui-jqgrid-labels">
                                                	<th class="ui-th-column ui-th-ltr">编号</th>
                                                	<th class="ui-th-column ui-th-ltr">商户</th>
                                                	<th class="ui-th-column ui-th-ltr">用户名称</th>
                                                    <th class="ui-th-column ui-th-ltr">反馈内容</th>
													<th class="ui-th-column ui-th-ltr">反馈时间</th>
												<!-- 	<th class="ui-th-column ui-th-ltr">修改时间</th> -->
													<th class="ui-th-column ui-th-ltr">管理员</th>
													<th class="ui-th-column ui-th-ltr">处理状态</th>
													<th class="ui-th-column ui-th-ltr">操 作</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>     
		         </div>
		      </div>
            <script type="text/javascript" src="<%=path%>/admin/common/common.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
		    <!-- dwr -->
		    <script type='text/javascript' src='<%=path%>/dwr/interface/managerFeedBackAction.js'></script>
		    <script type="text/javascript" src="<%=path%>/admin/feedback/js/feedback.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    </body>

</html>