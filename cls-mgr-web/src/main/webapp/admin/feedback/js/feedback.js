function FeedBackPage(){}
var feedbackPage = new FeedBackPage();

/**
 * 查询参数
 */
feedbackPage.queryParam = {
		bizSystem:null
};

/**
 * 分页参数
 */
feedbackPage.pageParam = {
    pageNo : 1,
    pageSize : defaultPageSize
};

$(document).ready(function() {
	feedbackPage.initTableData();
});

FeedBackPage.prototype.initTableData = function() {
	feedbackPage.queryParam = getFormObj($("#feedbackForm"));
	feedbackPage.table = $('#feedbackPageTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
//		"bPaginate" : false, 
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			feedbackPage.pageParam.pageSize = data.length;//页面显示记录条数
			feedbackPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerFeedBackAction.getAllFeedBack(feedbackPage.queryParam,feedbackPage.pageParam,function(r){
		//封装返回数据
		var returnData = {
			recordsTotal : 0,
			recordsFiltered : 0,
			data : null
		};
		if (r[0] != null && r[0] == "ok") {
			returnData.data = r[1].pageContent;;//返回的数据列表
			returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
			returnData.data = r[1].pageContent;;//返回的数据列表
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("查询信息失败.");
		}
		callback(returnData);
			});
		},
		"columns": [
            {"data": "id"},
            {"data": "bizSystemName",
            	"bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
            {"data": "userName"},
            {"data": "content",
            	render: function(data, type, row, meta) {
            		if (data.length > 20) {
            			var title = data.substring(0,20) + "...";
            			return "<span title = '"+data+"'>"+title+"</span>";
            		}
            		return data;
        		}
            	
            },
            {   
            	"data": "createTime",
            	render: function(data, type, row, meta) {
            		return dateFormat(data,'yyyy-MM-dd hh:mm:ss');
        		}
            },
/*            {   
            	"data": "updateTime",
            	render: function(data, type, row, meta) {
            		return dateFormat(data,'yyyy-MM-dd hh:mm:ss');
        		}
            },*/
        	{"data": "updateAdmin"},
        	{
            	"data": "dealstate",
            	render: function(data, type, row, meta) {
            		if (data == "1") {
            			return '已处理';
            		} else {
            			return "<span style='color: red;'>未处理</span>";
            		}
            		return data;
            	}
        	},
            {
            	 "data": "id",
            	 render: function(data, type, row, meta) {
            		var str=""
            		 	str="<a class='btn btn-danger btn-sm btn-del' onclick='feedbackPage.delFeedBackControl("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";
            	         if(row.dealstate=="0"){
            	        	 str = "<a class='btn btn-info btn-sm btn-del' onclick='feedbackPage.enableFeedBack("+data+",0)'><i class='fa fa-star' ></i>&nbsp;处理 </a>&nbsp;&nbsp;"+str; 
            	         }else{
            	        	 str = "<span style='color: grey;'>已处理</span>";+str;  
            	         }
            	         return str;
            	 }
            }
        ]
}).api(); 
}

/**
 * 查询数据
 */
FeedBackPage.prototype.findFeedBackControl=function(){
	var bizSystem=$("#bizSystem").val();
	if(bizSystem==null||bizSystem==""){
		feedbackPage.queryParam.bizSystem=null;
	}else{
		feedbackPage.queryParam.bizSystem=bizSystem;
	}
	feedbackPage.table.ajax.reload();
}

/**
 * 删除
 */
FeedBackPage.prototype.delFeedBackControl =function(id){
	  swal({
          title: "您确定要删除这条信息吗",
          text: "请谨慎操作！",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          cancelButtonText: "取消",
          confirmButtonText: "删除",
          closeOnConfirm: false
      },
      function() {
    	  managerFeedBackAction.delFeedBackById(id,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "删除成功",type: "success"});
				feedbackPage.findFeedBackControl();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("删除失败.");
			}
	    });
	});
};

/**
 * 停用，启用
 */
FeedBackPage.prototype.enableFeedBack = function(id){
	 $("body").append('<div class="ly-layer ly-tips"><div class="ly-layer-content">'
			 +'<div class="ly-title">请选择回复内容：</div>'
			 +'<div class="ly-cont">'
			 +'<ul class="ly-list">'
			 +'<li><input type="radio" name="lyid" id="lyid1"/><label for="lyid1">多谢您的反馈建议，我们会不断改进优化！</label></li>'
			 +'<li><input type="radio" name="lyid" id="lyid2"/><label for="lyid2">我们已经收到您的反馈建议，感谢您对我们提出的宝贵意见！</label></li>'
			 +'</ul></div>'
			 +'<div class="ly-text"><textarea id="textArea" placeholder="请输入回复内容"></textarea></div>'
			 +'<div class="ly-footer">'
			 +'<p>请谨慎操作！</p>'
			 +'<div class="ly-btn"><span class="ly-cancel">取消</span><span class="ly-submit">确定</span></div>'
			 +'</div>'
			 +'</div></div>');
	 $("body").on('click','.ly-cancel',function(){
		$(".ly-layer").remove();
	})
	$("body").on('click','.ly-submit',function(){
		let content = '';
		// 获取选择的选项内容
		$(".ly-list li").each(function(){
			if($(this).children('input').is(":checked")){
				content = $(this).children('label').text()
			}
		})
		// 获取输入内容
		let textarea = $("#textArea").val();
		managerFeedBackAction.enableFeedBack(id,content,textarea,function(r){
			$(".ly-layer").remove();
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "操作成功！",type: "success"});
				feedbackPage.findFeedBackControl();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("操作失败.");
			}
	    });
	})

};


FeedBackPage.prototype.layerClose=function(index){
	layer.close(index);
	feedbackPage.findFeedBackControl();
}