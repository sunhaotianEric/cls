<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>  
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>商户参数配置</title>
        <jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
    <body class="gray-bg">
        <div class="wrapper wrapper-content animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
                    <div class="ibox ">
                        <div class="ibox-content">
                            <div class="row m-b-sm m-t-sm">
                                <form class="form-horizontal" id="queryForm">
                                    <div class="row">
                                      <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                                        <div class="col-sm-4">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">商户</span>
                                                 <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
                                            </div>
                                        </div>
                                         <div class="col-sm-4">
                                           <div class="input-group m-b">
                                                <span class="input-group-addon">启用状态</span>
                                                 <select id="enable" name="enable" class="ipt form-control">
													<option value="" selected="selected">全部</option>
													<option value="1">启用</option>
													<option value="0">禁用</option>
											     </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group nomargin text-right">
                                                <button type="button" class="btn btn-outline btn-default btn-kj" onclick="bizsystemConfigPage.findBizSystemConfig()"><i class="fa fa-search"></i>&nbsp;查 询</button>
                                                &nbsp;&nbsp;
                                                <button type="button" class="btn btn-outline btn-default btn-kj" onclick="bizsystemConfigPage.addBizSystemConfig()"><i class="fa fa-search"></i>&nbsp;配置新增</button>
                                                </div>
                                        </div>
                                        </c:if>
                                      <c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
           
                                         <div class="col-sm-6">
                                           <div class="input-group m-b">
                                                <span class="input-group-addon">启用状态</span>
                                                 <select id="enable" name="enable" class="ipt form-control">
													<option value="" selected="selected">全部</option>
													<option value="1">启用</option>
													<option value="0">禁用</option>
											     </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group nomargin text-right">
                                                <button type="button" class="btn btn-outline btn-default btn-kj" onclick="bizsystemConfigPage.findBizSystemConfig()"><i class="fa fa-search"></i>&nbsp;查 询</button>
                                                &nbsp;&nbsp;
                                                <button type="button" class="btn btn-outline btn-default btn-kj" onclick="bizsystemConfigPage.addBizSystemConfig()"><i class="fa fa-search"></i>&nbsp;配置新增</button>
                                                </div>
                                        </div>
                                        </c:if>
                                    </div>
                                </form>
                            </div>
                            <div class="jqGrid_wrapper">
                                <div class="ui-jqgrid ">
                                    <div class="ui-jqgrid-view">
                                        <div class="ui-jqgrid-hdiv">
                                            <div class="ui-jqgrid-hbox" style="padding-right:0">
                                                <table class="ui-jqgrid-htable ui-common-table table table-bordered" id="systemConfigTable">
                                                    <thead>
                                                        <tr class="ui-jqgrid-labels">
                                                              <th class="ui-th-column ui-th-ltr">商户</th>
                                                              <th class="ui-th-column ui-th-ltr">参数名</th>
													          <th class="ui-th-column ui-th-ltr">参数值</th>
													          <th class="ui-th-column ui-th-ltr">描述</th>
													          <th class="ui-th-column ui-th-ltr">启用状态</th>
													          <th class="ui-th-column ui-th-ltr">创建时间</th>
													          <th class="ui-th-column ui-th-ltr">操作人</th>
													          <th class="ui-th-column ui-th-ltr">更新时间</th>
													          <th class="ui-th-column ui-th-ltr">操  作</th>
                                                            </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/bizsystemconfig/js/bizsystemconfig.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerBizSystemConfigAction.js'></script>    
</body>
</html>

