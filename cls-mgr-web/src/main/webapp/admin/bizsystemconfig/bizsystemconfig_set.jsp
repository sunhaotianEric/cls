<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="com.team.lottery.enums.ELotteryKind"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="shortcut icon" href="favicon.ico">
<jsp:include page="/admin/include/include.jsp"></jsp:include>
<script type="text/javascript"
	src="<%=path%>/resources/My97DatePicker/WdatePicker.js"></script>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeIn">
		<c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
			<div class="row">
				<div class="col-sm-3">
					<div class="input-group m-b">
						<span class="input-group-addon">商户</span>
						<cls:bizSel name="bizSystem" id="bizSystem"
							options="class:ipt form-control" />
					</div>
				</div>
				<div class="col-sm-2">
					<div class="form-group nomargin text-left input-group-btn">
						<button type="button"
							class="btn btn-outline btn-default btn-white btn-kj"
							onclick="editBizSystemConfigPage.getBizSystemConfig()">
							<i class="fa fa-search"></i>&nbsp;搜索
						</button>
					</div>
				</div>
				<div class="col-sm-7"></div>
			</div>
		</c:if>
		<div class="tabs-container ibox">
			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#tab-1"
					aria-expanded="true">参数配置</a></li>
				<li class=""><a data-toggle="tab" href="#tab-3"
					aria-expanded="false">取现转账设置</a></li>
				<li class=""><a data-toggle="tab" href="#tab-4"
					aria-expanded="false">工资分红发放设置</a></li>
				<li class=""><a data-toggle="tab" href="#tab-5"
					aria-expanded="false">维护设置</a></li>
				<li class=""><a data-toggle="tab" href="#tab-6"
					aria-expanded="false">配额设置</a></li>
				<li class=""><a data-toggle="tab" href="#tab-8"
					aria-expanded="false">六合投注配置</a></li>
				<li class=""><a data-toggle="tab" href="#tab-9"
					aria-expanded="false">监控设置</a></li>
				<li class=""><a data-toggle="tab" href="#tab-10"
					aria-expanded="false">注册设置</a></li>
				<li class=""><a data-toggle="tab" href="#tab-11"
					aria-expanded="false">试玩功能设置</a></li>
				<li class=""><a data-toggle="tab" href="#tab-12"
					aria-expanded="false">签到设置</a></li>
				<li class=""><a data-toggle="tab" href="#tab-13"
					aria-expanded="false">新手礼包设置</a></li>
				<li class=""><a data-toggle="tab" href="#tab-14"
					aria-expanded="false">晋级奖励活动</a></li>
					<li class=""><a data-toggle="tab" href="#tab-15"
					aria-expanded="false">流水返送活动</a></li>
			</ul>
			<div class="tab-content">
				<div id="tab-1" class="tab-pane active">
					<div class="panel-body">
						<form class="form-horizontal">
							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										<input id="canlotteryhightModelId" name="" type="hidden" /> <label
											class="col-sm-5 control-label">允许投注最高模式:</label>
										<div class="col-sm-5">
											<input type="text" class="form-control" style="width: 180px"
												value="" class="ipt" onkeyup='checkNum(this)'
												id="canlotteryhightModel">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-5 control-label">手机登陆开关:</label>
										<div class="col-sm-5">
											<select class="ipt" id="isOpenPhone">
												<option value="">请选择</option>
												<option value="0">关闭</option>
												<option value="1">开启</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-5 control-label">彩票计划开关开关:</label>
										<div class="col-sm-5">
											<select class="ipt" id="codePlanSwitch">
												<option value="">请选择</option>
												<option value="0">关闭</option>
												<option value="1">开启</option>
											</select>
										</div>
									</div>
								</div>

								<div class="col-sm-6">
									<div class="form-group">
										<input id="secretCodeId" name="" type="hidden" /> <label
											class="col-sm-3 control-label">秘密验证码:</label>
										<div class="col-sm-5">
											<input type="text" class="form-control" style="width: 180px"
												value="" class="ipt" id="secretCode">
										</div>
									</div>
									
									<div class="form-group">

										<label class="col-sm-3 control-label">邮箱登陆开关:</label>
										<div class="col-sm-5">
											<select class="ipt" id="isOpenEmail">
												<option value="">请选择</option>
												<option value="0">关闭</option>
												<option value="1">开启</option>
											</select>
										</div>
									</div>
									
									<div class="form-group">

										<label class="col-sm-3 control-label">银行卡同名允许绑卡:</label>
										<div class="col-sm-5">
											<select class="ipt" id="isNoSameNameBindBankCard">
												<option value="">请选择</option>
												<option value="0">关闭</option>
												<option value="1">开启</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div id="tab-3" class="tab-pane">
					<div class="panel-body">
						<form class="form-horizontal">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<input type="hidden" id="exceedTransferDayLimitExpensesId">
										<label class="col-sm-4 control-label">每日转账限额:</label>
										<div class="col-sm-7">
											<input type="text" value="" class="form-control"
												onkeyup='checkNum(this)' id="exceedTransferDayLimitExpenses">
										</div>
									</div>
									<div class="form-group">
										<input type="hidden" id="withDrawOneStrokeLowestMoneyId">
										<label class="col-sm-4 control-label">单笔取现最低金额:</label>
										<div class="col-sm-7">
											<input type="text" value="" class="form-control"
												onkeyup='checkNum(this)' id="withDrawOneStrokeLowestMoney">
										</div>
									</div>
									<div class="form-group">
										<input type="hidden" id="withDrawEveryDayTotalMoneyId">
										<label class="col-sm-4 control-label">每天取现总金额:</label>
										<div class="col-sm-7">
											<input type="text" value="" class="form-control"
												onkeyup='checkNum(this)' id="withDrawEveryDayTotalMoney">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label">提现的手续费率:</label> <input
											type="hidden" id="exceedWithdrawExpensesId">
										<div class="col-sm-7">
											<input type="text" value="" class="form-control"
												onkeyup='checkNum(this)' id="exceedWithdrawExpenses">
										</div>
									</div>
									<div class="form-group">
										<input type="hidden" id="withDrawLotteryMultipleId"> <label
											class="col-sm-4 control-label">进入可提现余额投注额倍数:</label>
										<div class="col-sm-7">
											<input type="text" value="" class="form-control"
												onkeyup='checkNum(this)' id="withDrawLotteryMultiple">
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<input type="hidden" id="isTransferSwichId"> <label
											class="col-sm-4 control-label">上下级转账开关:</label>
										<div class="col-sm-7">
											<select class="ipt" id="isTransferSwich">
												<option value="">请选择</option>
												<option value="1">开启</option>
												<option value="0">关闭</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<input type="hidden" id="withDrawOneStrokeHighestMoneyId">
										<label class="col-sm-4 control-label">单笔取现最高金额:</label>
										<div class="col-sm-7">
											<input type="text" value="" class="form-control"
												onkeyup='checkNum(this)' id="withDrawOneStrokeHighestMoney">
										</div>
									</div>
									<div class="form-group">
										<input type="hidden" id="withDrawEveryDayNumberId"> <label
											class="col-sm-4 control-label">每天取现次数:</label>
										<div class="col-sm-7">
											<input type="text" value="" class="form-control"
												onkeyup='checkNum(this)' id="withDrawEveryDayNumber">
										</div>
									</div>
									<div class="form-group">
										<input type="hidden" id="withDrawIsAllowNotEnoughId">
										<label class="col-sm-4 control-label">可提现不足不允许提现:</label>
										<div class="col-sm-7">
											<select class="ipt" id="withDrawIsAllowNotEnough">
												<option value="">请选择</option>
												<option value="1">开启</option>
												<option value="0">关闭</option>
											</select>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-sm-4 control-label">第三方充值要求打码倍数:</label> <input
											type="hidden" id="thirdPayOrderBettingMultipleId">
										<div class="col-sm-7">
											<input type="text" value="" class="form-control"
												onkeyup='checkNum(this)' id="thirdPayOrderBettingMultiple">
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div id="tab-4" class="tab-pane">
					<div class="panel-body">
						<form class="form-horizontal">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<input id="releasePolicyBonusId" name="" type="hidden" /> <label
											class="col-sm-3 control-label">契约分红发放策略:</label>
										<div class="col-sm-8">
											<!--  <input type="text" value="" class="form-control" id="releasePolicyBonus"> -->
											<cls:enmuSel
												className="com.team.lottery.enums.EReleasePolicy"
												name="releasePolicy" id="releasePolicyBonus"
												emptyOption="true" options="class:ipt form-control" />
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<input id="releasePolicyDaySaralyId" name="" type="hidden" />
										<label class="col-sm-3 control-label">日工资发放策略:</label>
										<div class="col-sm-8">
											<cls:enmuSel
												className="com.team.lottery.enums.EReleasePolicy"
												name="releasePolicy" id="releasePolicyDaySaraly"
												emptyOption="true" options="class:ipt form-control" />
											<!-- <input type="text" value="" class="form-control" id="releasePolicyDaySaraly"> -->
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div id="tab-5" class="tab-pane">
					<div class="panel-body">
						<form class="form-horizontal">
							<div class="row">
								<div class="col-sm-3">
									<div class="form-group">
										<input id="maintainSwichId" name="" type="hidden" /> <label
											class="col-sm-5 control-label">平台维护开关:</label>
										<div class="col-sm-6">
											<select class="ipt" id="maintainSwich">
												<option value="">请选择</option>
												<option value="1">开启</option>
												<option value="0">关闭</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-sm-8">
									<div class="form-group">
										<input id="maintainContentId" name="" type="hidden" /> <label
											class="col-sm-2 control-label">平台维护说明:</label>
										<div class="col-sm-8">
											<textarea name="comment" class="form-control" required=""
												style="height: 150px; width: 660px" aria-required="true"
												id="maintainContent"></textarea>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div id="tab-6" class="tab-pane">
					<div class="panel-body">
						<form class="form-horizontal">
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<div class="col-sm-4">
											<div class="input-group m-b">
												<input type="hidden" id="directagencyNumId"> <label
													class="col-sm-5 control-label">直属代理人数配置:</label>
												<div class="col-sm-7">
													<input type="text" value="" class="form-control"
														onkeyup='checkNum(this)' id="directagencyNum">
												</div>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="input-group m-b">
												<input type="hidden" id="genralagencyNumId"> <label
													class="col-sm-5 control-label">总代理人数配置:</label>
												<div class="col-sm-7">
													<input type="text" value="" class="form-control"
														onkeyup='checkNum(this)' id="genralagencyNum">
												</div>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="input-group m-b">
												<input type="hidden" id="ordinayagencyNumId"> <label
													class="col-sm-5 control-label">普通代理人数配置:</label>
												<div class="col-sm-7">
													<input type="text" value="" class="form-control"
														onkeyup='checkNum(this)' id="ordinayagencyNum">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div id="tab-8" class="tab-pane">
					<div class="panel-body">
						<form class="form-horizontal">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<input type="hidden" id="lhcLowerLotteryMoneyId"> <label
											class="col-sm-4 control-label">六合彩单注最低限额:</label>
										<div class="col-sm-7">
											<input type="text" value="" class="form-control"
												onkeyup='checkNum(this)' id="lhcLowerLotteryMoney">
										</div>
									</div>
									<div class="form-group">
										<input type="hidden" id="lhcHighestExpectLotteryMoneyId">
										<label class="col-sm-4 control-label">六合彩单场最高投注额:</label>
										<div class="col-sm-7">
											<input type="text" value="" class="form-control"
												onkeyup='checkNum(this)' id="lhcHighestExpectLotteryMoney">
										</div>
									</div>
									<div class="form-group">
										<input type="hidden" id="isCloseId"> <label
											class="col-sm-4 control-label">六合是否封盘:</label>
										<div class="col-sm-7">
											<select class="ipt" id="isClose">
												<option value="">请选择</option>
												<option value="1">开启</option>
												<option value="0">关闭</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<input type="hidden" id="lhcHighestLotteryMoneyId"> <label
											class="col-sm-4 control-label">六合彩单注最高限额:</label>
										<div class="col-sm-7">
											<input type="text" value="" class="form-control"
												onkeyup='checkNum(this)' id="lhcHighestLotteryMoney">
										</div>
									</div>
									<div class="form-group">
										<input type="hidden" id="lhcMaxWinMoneyId"> <label
											class="col-sm-4 control-label">六合彩最高返奖限额:</label>
										<div class="col-sm-7">
											<input type="text" value="" class="form-control"
												onkeyup='checkNum(this)' id="lhcMaxWinMoney">
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!-- 监控设置 -->
				<div id="tab-9" class="tab-pane">
					<div class="panel-body">
						<form class="form-horizontal">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label class="col-sm-3 control-label">投注监控开关:</label>
										<div class="col-sm-3">
											<select id="lotteryAlarmOpen" name="lotteryAlarmOpen"
												class="form-control">
												<option value="">请选择</option>
												<option value="0">关闭</option>
												<option value="1">开启</option>
											</select>
										</div>
									</div>

									<div class="form-group">
										<label class="col-sm-3 control-label">投注监控金额:</label>
										<div class="col-sm-3">
											<input id="lotteryAlarmValue" name="lotteryAlarmValue"
												class="form-control" onkeyup='checkNum(this)'>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">充值监控开关:</label>
										<div class="col-sm-3">
											<select id="rechargeAlarmOpen" name="rechargeAlarmOpen"
												class="form-control">
												<option value="">请选择</option>
												<option value="0">关闭</option>
												<option value="1">开启</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">充值监控金额:</label>
										<div class="col-sm-3">
											<input id="rechargeAlarmValue" name="rechargeAlarmValue"
												class="form-control" onkeyup='checkNum(this)'>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">是否邮件通知:</label>
										<div class="col-sm-3">
											<select id="alarmEmailOpen" name="alarmEmailOpen"
												class="form-control">
												<option value="">请选择</option>
												<option value="0">否</option>
												<option value="1">是</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">邮箱地址:</label>
										<div class="col-sm-3">
											<input id="alarmEmail" name="alarmEmail" class="form-control">
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label class="col-sm-3 control-label">中奖监控开关:</label>
										<div class="col-sm-3">
											<select id="winAlarmOpen" name="winAlarmOpen"
												class="form-control">
												<option value="">请选择</option>
												<option value="0">关闭</option>
												<option value="1">开启</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">中奖监控金额:</label>
										<div class="col-sm-3">
											<input id="winAlarmValue" name="winAlarmValue"
												class="form-control" onkeyup='checkNum(this)'>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">取现监控开关:</label>
										<div class="col-sm-3">
											<select id="withdrawAlarmOpen" name="withdrawAlarmOpen"
												class="form-control">
												<option value="">请选择</option>
												<option value="0">关闭</option>
												<option value="1">开启</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">取现监控金额:</label>
										<div class="col-sm-3">
											<input id="withdrawAlarmValue" name="withdrawAlarmValue"
												class="form-control" onkeyup='checkNum(this)'>
										</div>
									</div>

									<div class="form-group">
										<label class="col-sm-3 control-label">是否短信通知:</label>
										<div class="col-sm-3">
											<select id="alarmPhoneOpen" name="alarmPhoneOpen"
												class="form-control">
												<option value="">请选择</option>
												<option value="0">否</option>
												<option value="1">是</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">手机号码:</label>
										<div class="col-sm-3">
											<input id="alarmPhone" name="alarmPhone" class="form-control">
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!-- 注册设置 -->
				<div id="tab-10" class="tab-pane">
					<div class="panel-body">
						<form class="form-horizontal">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label class="col-sm-4 control-label">注册是否显示手机号码:</label>
										<div class="col-sm-3">
											<select id="regShowPhone" name="regShowPhone"
												class="form-control">
												<option value="">请选择</option>
												<option value="0">否</option>
												<option value="1">是</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label class="col-sm-4 control-label">注册手机号码是否必填:</label>
										<div class="col-sm-3">
											<select id="regRequiredPhone" name="regRequiredPhone"
												class="form-control">
												<option value="">请选择</option>
												<option value="0">否</option>
												<option value="1">是</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label class="col-sm-4 control-label">注册是否显示邮件:</label>
										<div class="col-sm-3">
											<select id="regShowEmail" name="regShowEmail"
												class="form-control">
												<option value="">请选择</option>
												<option value="0">否</option>
												<option value="1">是</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label class="col-sm-4 control-label">注册邮件是否必填:</label>
										<div class="col-sm-3">
											<select id="regRequiredEmail" name="regRequiredEmail"
												class="form-control">
												<option value="">请选择</option>
												<option value="0">否</option>
												<option value="1">是</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label class="col-sm-4 control-label">注册是否显示QQ:</label>
										<div class="col-sm-3">
											<select id="regShowQq" name="regShowQq" class="form-control">
												<option value="">请选择</option>
												<option value="0">否</option>
												<option value="1">是</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label class="col-sm-4 control-label">注册QQ是否必填:</label>
										<div class="col-sm-3">
											<select id="regRequiredQq" name="regRequiredQq"
												class="form-control">
												<option value="">请选择</option>
												<option value="0">否</option>
												<option value="1">是</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label class="col-sm-4 control-label">是否开启手机短信验证:</label>
										<div class="col-sm-3">
											<select id="regShowSms" name="regShowSms"
												class="form-control">
												<option value="">请选择</option>
												<option value="0">否</option>
												<option value="1">是</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label class="col-sm-4 control-label">注册完直接登录:</label>
										<div class="col-sm-3">
											<select id="regDirectLogin" name="regDirectLogin"
												class="form-control">
												<option value="">请选择</option>
												<option value="0">否</option>
												<option value="1">是</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label class="col-sm-4 control-label">是否开放注册:</label>
										<div class="col-sm-3">
											<select id="regOpen" name="regOpen" class="form-control">
												<option value="">请选择</option>
												<option value="0">否</option>
												<option value="1">是</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<input id="defaultInvitationCodeId" name="" type="hidden" />
										<label class="col-sm-4 control-label">默认邀请码:</label>
										<div class="col-sm-3">
											<input type="text" class="form-control" style="width: 180px"
												value="" class="ipt" id="defaultInvitationCode">
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label class="col-sm-4 control-label">是否展示真实姓名:</label>
										<div class="col-sm-3">
											<select id="regShowTrueName" name="regShowTrueName" class="form-control">
												<option value="">请选择</option>
												<option value="0">否</option>
												<option value="1">是</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label class="col-sm-4 control-label">是否必填真实姓名:</label>
										<div class="col-sm-3">
											<select id="regRequiredTrueName" name="regRequiredTrueName" class="form-control">
												<option value="">请选择</option>
												<option value="0">否</option>
												<option value="1">是</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div id="tab-11" class="tab-pane">
					<div class="panel-body">
						<form class="form-horizontal">
							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										<input id="guestModelId" name="" type="hidden" /> <label
											class="col-sm-6 control-label">是否开启试玩功能:</label>
										<div class="col-sm-5">
											<select class="ipt" id="guestModel">
												<option value="">请选择</option>
												<option value="0">否</option>
												<option value="1">是</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<input id="allowGuestLoginId" name="" type="hidden" /> <label
											class="col-sm-6 control-label">是否允许试玩账号登录:</label>
										<div class="col-sm-5">
											<select class="ipt" id="allowGuestLogin">
												<option value="">请选择</option>
												<option value="0">否</option>
												<option value="1">是</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<input id="guestMoneyId" name="" type="hidden" /> <label
											class="col-sm-3 control-label">试玩账号默认金钱:</label>
										<div class="col-sm-5">
											<input type="text" class="form-control" style="width: 180px"
												value="" class="ipt" id="guestMoney">
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div id="tab-12" class="tab-pane">
					<div class="panel-body">
						<form class="form-horizontal">
							<div class="row m-b-sm m-t-sm">
								<div class="col-sm-4">
									<div class="form-group">
										<input id="isOpenSignInActivityId" name="" type="hidden" /> <label
											class="col-sm-4 control-label">签到活动开启关闭:</label>
										<div class="col-sm-5">
											<select class="ipt" id="isOpenSignInActivity">
												<option value="">请选择</option>
												<option value="1">开启</option>
												<option value="0">关闭</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<input id="signinRechargeMoneyId" name="" type="hidden" /> <label
											class="col-sm-5 control-label">签到要求充值金额:</label>
										<div class="col-sm-5">
											<input type="text" class="form-control" style="width: 180px"
												value="" class="ipt" id="signinRechargeMoney">
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="input-group m-b">
										<span class="input-group-addon">每天签到最后时间:</span>
										<div class="input-daterange input-group" id="datepicker">
											<input class="form-control layer-date" placeholder="每天签到最后时间"
												onclick="WdatePicker({dateFmt:'HH:mm:ss'})"
												name="signinLastTime" id="signinLastTime">
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div id="tab-13" class="tab-pane">
					<div class="panel-body">
						<form class="form-horizontal">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<input type="hidden" id="isNewUserGiftOpenId"> <label
											class="col-sm-4 control-label">新手礼包功能:</label>
										<div class="col-sm-7">
											<select class="ipt" id="isNewUserGiftOpen">
												<option value="">请选择</option>
												<option value="1">开启</option>
												<option value="0">关闭</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<input type="hidden" id="appDownGiftMoneyId"> <label
											class="col-sm-4 control-label">下载APP赠送金额:</label>
										<div class="col-sm-5">
											<input type="text" value="" class="form-control"
												style="width: 180px" onkeyup='checkNum(this)'
												id="appDownGiftMoney">
										</div>
									</div>
									<div class="form-group">
										<input type="hidden" id="completeInfoGiftMoneyId"> <label
											class="col-sm-4 control-label">完善个人资料赠送金额:</label>
										<div class="col-sm-5">
											<input type="text" value="" class="form-control"
												style="width: 180px" onkeyup='checkNum(this)'
												id="completeInfoGiftMoney">
										</div>
									</div>
									<div class="form-group">
										<input type="hidden" id="complateSafeGiftMoneyId"> <label
											class="col-sm-4 control-label">完成安全资料赠送金额:</label>
										<div class="col-sm-5">
											<input type="text" value="" class="form-control"
												style="width: 180px" onkeyup='checkNum(this)'
												id="complateSafeGiftMoney">
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<input type="hidden" id="lotteryGiftMoneyId"> <label
											class="col-sm-4 control-label">首笔投注赠送金额:</label>
										<div class="col-sm-5">
											<input type="text" value="" class="form-control"
												style="width: 180px" onkeyup='checkNum(this)'
												id="lotteryGiftMoney">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label">首笔充值赠送金额:</label> <input
											type="hidden" id="rechargeGiftMoneyId">
										<div class="col-sm-5">
											<input type="text" value="" class="form-control"
												style="width: 180px" onkeyup='checkNum(this)'
												id="rechargeGiftMoney">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label">发展新玩家赠送金额:</label> <input
											type="hidden" id="extendUserGiftMoneyId">
										<div class="col-sm-5">
											<input type="text" value="" class="form-control"
												style="width: 180px" onkeyup='checkNum(this)'
												id="extendUserGiftMoney">
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div id="tab-14" class="tab-pane">
					<div class="panel-body">
						<form class="form-horizontal">
							<div class="row">
								<div class="form-group">
									<input id="isOpenLevelAwardId" name="" type="hidden" /> <label
										class="col-sm-2 control-label">用户晋级开关:</label>
									<div class="col-sm-4">
										<select class="ipt" id="isOpenLevelAward">
											<option value="">请选择</option>
											<option value="1">开启</option>
											<option value="0">关闭</option>
										</select>
									</div>
									<input id="automaticPromotionReceiveSwitchId" name="" type="hidden" /> <label
										class="col-sm-2 control-label">晋级奖励自动领取:</label>
									<div class="col-sm-2">
										<select class="ipt" id="automaticPromotionReceiveSwitch">
											<option value="">请选择</option>
											<option value="1">开启</option>
											<option value="0">关闭</option>
										</select>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div id="tab-15" class="tab-pane">
					<div class="panel-body">
						<form class="form-horizontal">
							<div class="row">
<!-- 								<div class="col-sm-5">
		                     		<div class="input-group m-b">
		                         		<span class="input-group-addon">流水返送领取开始时间 </span>
		                         	<div class="input-daterange input-group" id="datepicker" style="width:100%">
		                           		<input class="form-control layer-date" placeholder="开始日期" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" name="logtimeStart" id="startime">
				                    <span class="input-group-addon">流水返送领取结束时间</span>
				                   		<input class="form-control layer-date" placeholder="结束日期" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" name="logtimeEnd" id="endtime">
		                       		</div>
		                  		</div>
		              		</div> -->
							<div class="col-sm-4">
								<div class="input-group m-b">
									<span class="input-group-addon">流水返送领取开始时间:</span>
									<div class="input-daterange input-group" id="datepicker">
										<input class="form-control layer-date" placeholder="每天签到最后时间"
											onclick="WdatePicker({dateFmt:'HH:mm:ss'})"
											name="dayConsumeOrderTimeStart" id="dayConsumeOrderTimeStart">
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="input-group m-b">
									<span class="input-group-addon">流水返送领取结束时间:</span>
									<div class="input-daterange input-group" id="datepicker">
										<input class="form-control layer-date" placeholder="每天签到最后时间"
											onclick="WdatePicker({dateFmt:'HH:mm:ss'})"
											name="dayConsumeOrderTimeEnd" id="dayConsumeOrderTimeEnd">
									</div>
								</div>
							</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-12 ibox">
			<div class="row">
				<div class="col-sm-6 text-center">
					<button type="button" class="btn btn-w-m btn-white"
						onclick="editBizSystemConfigPage.updateDefaultSystemConfig()">保
						存</button>
				</div>
				<div class="col-sm-6 text-center">
					<button type="button" class="btn btn-w-m btn-white"
						onclick="editBizSystemConfigPage.refreshCache()">一键更新缓存</button>
				</div>
			</div>
		</div>
	</div>
	<!-- js -->
	<script type="text/javascript"
		src="<%=path%>/admin/bizsystemconfig/js/editbizsystemconfig.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
	<!-- dwr -->
	<script type='text/javascript'
		src='<%=path%>/dwr/interface/managerBizSystemConfigAction.js'></script>
</body>
</html>