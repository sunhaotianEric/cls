function EditBizSystemConfigPage(){}
var editBizSystemConfigPage = new EditBizSystemConfigPage();

editBizSystemConfigPage.param = {
   	
};

$(document).ready(function() {
	var bizsystemConfigId = editBizSystemConfigPage.param.id;  //获取查询ID
	if(bizsystemConfigId != null){
		editBizSystemConfigPage.editBizSystemConfig(bizsystemConfigId);
	}
	
	var currentSystem = currentUser.bizSystem;
	
	if(currentSystem != "SUPER_SYSTEM" ){
		editBizSystemConfigPage.getBizSystemConfig(currentSystem);
	}
});


/**
 * 保存更新信息
 */
EditBizSystemConfigPage.prototype.saveData = function(){
	var id = $("#bizsystemconfigId").val();
	var configKey = $("#configKey").val();
	var configValue = $("#configValue").val();
	var configDes = $("#configDes").val();
	var enable = $("#enable").val();
	var bizSystem = $("#bizSystem").val();
	
	if(configKey==null || configKey.trim()==""){
		swal("请输入参数名!");
		$("#configKey").focus();
		return;
	}
	if(configValue==null || configValue.trim()==""){
		swal("请输入参数值!");
		$("#configValue").focus();
		return;
	}
	
	
	var bizsystemConfig = {};
	bizsystemConfig.id = id;
	bizsystemConfig.configKey = configKey;
	bizsystemConfig.configValue = configValue;
	bizsystemConfig.configDes = configDes;
	bizsystemConfig.enable = enable;
	bizsystemConfig.bizSystem = bizSystem;
	managerBizSystemConfigAction.saveOrUpdateBizSystenConfig(bizsystemConfig,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.bizsystemConfigPage.closeLayer(index);
		   	   });
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("保存信息失败.");
		}
    });
};


/**
 * 赋值配置信息
 */
EditBizSystemConfigPage.prototype.editBizSystemConfig = function(id){
	managerBizSystemConfigAction.getBizSystemConfigById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			$("#bizsystemconfigId").val(r[1].id);
			$("#configKey").val(r[1].configKey);
			$("#configValue").val(r[1].configValue);
			$("#configDes").val(r[1].configDes);
			$("#enable").val(r[1].enable);
			$("#bizSystem").val(r[1].bizSystem);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取信息失败.");
		}
    });
};


/**
 * 查找配置信息
 */
EditBizSystemConfigPage.prototype.getBizSystemConfig = function(currentSystem){
	var bizSystem = "";
	if(typeof(currentSystem) != "undefined"){
		bizSystem = currentSystem;
	}else{
		bizSystem = $("#bizSystem").val();
	}
	managerBizSystemConfigAction.getBizSystemConfigByQuery(bizSystem,function(r){
		if (r[0] != null && r[0] == "ok") {
			editBizSystemConfigPage.refreshBizSystemConfig(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取信息失败.");
		}
    });
};

/**
 * 赋值配置信息
 */
EditBizSystemConfigPage.prototype.refreshBizSystemConfig = function(configList){
	$("#canlotteryhightModel").val("");
	$("#defaultInvitationCode").val("");
	$("#secretCode").val("");
	$("#guestModel").val("");
	$("#allowGuestLogin").val("");
	$("#guestMoney").val("");
	$("#withDrawOneStrokeLowestMoney").val("");
	$("#withDrawEveryDayTotalMoney").val("");
	$("#exceedWithdrawExpenses").val("");
	$("#exceedTransferDayLimitExpenses").val("");
	$("#withDrawLotteryMultiple").val("0");
	$("#withDrawOneStrokeHighestMoney").val("");
	$("#withDrawEveryDayNumber").val("");
	$("#thirdPayOrderBettingMultiple").val("");
//	$("#isTransferSwich").val("");
//	$("#releasePolicyBonus").val("");
//	$("#releasePolicyDaySaraly").val("");
//	$("#maintainSwich").val("");
	$("#maintainContent").val("");
	$("#genralagencyNum").val("");
	$("#directagencyNum").val("");
	$("#ordinayagencyNum").val("");
	$("#lhcLowerLotteryMoney").val("");
	$("#lhcHighestExpectLotteryMoney").val("");
	$("#lhcHighestLotteryMoney").val("");
	$("#lhcMaxWinMoney").val("");
	$("#codePlanSwitch").val("0");
	//监控设置
	$("#lotteryAlarmOpen").val("0");
	$("#lotteryAlarmValue").val("");
	$("#rechargeAlarmOpen").val("0");
	$("#rechargeAlarmValue").val("");
	$("#alarmEmailOpen").val("0");
	$("#alarmEmail").val("");
	$("#winAlarmOpen").val("0");
	$("#winAlarmValue").val("");
	$("#withdrawAlarmOpen").val("0");
	$("#withdrawAlarmValue").val("");
	$("#alarmPhoneOpen").val("0");
	$("#alarmPhone").val("");
	$("#isOpenSignInActivity").val("");
	$("#signinRechargeMoney").val("");
	$("#signinLastTime").val("");
	$("#dayConsumeOrderTimeStart").val("");
	$("#dayConsumeOrderTimeEnd").val("");
	//新手礼包设置
	//是否开启新手礼包
	$("#isNewUserGiftOpen").val("0");
	
	//下载APP赠送金额
	$("#appDownGiftMoney").val("");

	//完善个人资料赠送金额
	$("#completeInfoGiftMoney").val("");
	//完善安全资料赠送金额
	$("#complateSafeGiftMoney").val("");
	//首笔投注赠送金额
	$("#lotteryGiftMoney").val("");
	//首笔充值赠送金额
	$("#rechargeGiftMoney").val("");
	//发展新玩家赠送金额
	$("#extendUserGiftMoney").val("");
	// 是否展示真实姓名.
	$("#regShowTrueName").val("");
	// 真实姓名是否必填.
	$("#regRequiredTrueName").val("");
	
	if(configList.length == 0){
		
	}else{
		for(var i = 0;i < configList.length; i++){
			var config = configList[i];
			if(config.configKey == "canlotteryhightModel"){
				$("#canlotteryhightModel").val(config.configValue);
			}
			if(config.configKey == "defaultInvitationCode"){
				$("#defaultInvitationCode").val(config.configValue);
			}
			if(config.configKey == "secretCode"){
				$("#secretCode").val(config.configValue);
			}
			if(config.configKey == "isOpenLevelAward"){
				$("#isOpenLevelAward").val(config.configValue);
			}
			if(config.configKey == "automaticPromotionReceiveSwitch"){
				$("#automaticPromotionReceiveSwitch").val(config.configValue);
			}
			if(config.configKey == "guestModel"){
				$("#guestModel").val(config.configValue);
			}
			if(config.configKey == "guestMoney"){
				$("#guestMoney").val(config.configValue);
			}
			if(config.configKey == "allowGuestLogin"){
			$("#allowGuestLogin").val(config.configValue);
			}
			if(config.configKey == "withDrawOneStrokeLowestMoney"){
				$("#withDrawOneStrokeLowestMoney").val(config.configValue);
			}
			if(config.configKey == "withDrawEveryDayTotalMoney"){
				$("#withDrawEveryDayTotalMoney").val(config.configValue);
			}
			if(config.configKey == "exceedWithdrawExpenses"){
				$("#exceedWithdrawExpenses").val(config.configValue);
			}
			if(config.configKey == "thirdPayOrderBettingMultiple"){
				$("#thirdPayOrderBettingMultiple").val(config.configValue);
			}
			if(config.configKey == "exceedTransferDayLimitExpenses"){
				$("#exceedTransferDayLimitExpenses").val(config.configValue);
			}
			if(config.configKey == "withDrawLotteryMultiple"){
				$("#withDrawLotteryMultiple").val(config.configValue);
			}
			if(config.configKey == "withDrawOneStrokeHighestMoney"){
				$("#withDrawOneStrokeHighestMoney").val(config.configValue);
			}
			if(config.configKey == "withDrawEveryDayNumber"){
				$("#withDrawEveryDayNumber").val(config.configValue);
			}
			if(config.configKey == "isTransferSwich"){
				$("#isTransferSwich").val(config.configValue);
			}
			if(config.configKey == "releasePolicyBonus"){
				$("#releasePolicyBonus").val(config.configValue);
			}
			if(config.configKey == "releasePolicyDaySaraly"){
				$("#releasePolicyDaySaraly").val(config.configValue);
			}
			if(config.configKey == "maintainSwich"){
				$("#maintainSwich").val(config.configValue);
			}
			if(config.configKey == "maintainContent"){
				$("#maintainContent").val(config.configValue);
			}
			if(config.configKey == "genralagencyNum"){
				$("#genralagencyNum").val(config.configValue);
			}
			if(config.configKey == "directagencyNum"){
				$("#directagencyNum").val(config.configValue);
			}
			if(config.configKey == "ordinayagencyNum"){
				$("#ordinayagencyNum").val(config.configValue);
			}
			if(config.configKey == "isClose"){
				$("#isClose").val(config.configValue);
			}
			if(config.configKey == "lhcLowerLotteryMoney"){
				$("#lhcLowerLotteryMoney").val(config.configValue);
			}
			if(config.configKey == "lhcHighestLotteryMoney"){
				$("#lhcHighestLotteryMoney").val(config.configValue);
			}
			if(config.configKey == "lhcHighestExpectLotteryMoney"){
				$("#lhcHighestExpectLotteryMoney").val(config.configValue);
			}
			if(config.configKey == "lhcMaxWinMoney"){
				$("#lhcMaxWinMoney").val(config.configValue);
			}
			if(config.configKey == "withDrawIsAllowNotEnough"){
				$("#withDrawIsAllowNotEnough").val(config.configValue);
			}
			
			if(config.configKey == "isOpenPhone"){
				$("#isOpenPhone").val(config.configValue);
			}
			if(config.configKey == "isOpenEmail"){
				$("#isOpenEmail").val(config.configValue);
			}
			if(config.configKey == "isNoSameNameBindBankCard"){
				$("#isNoSameNameBindBankCard").val(config.configValue);
			}
			if(config.configKey == "codePlanSwitch"){
				$("#codePlanSwitch").val(config.configValue);
			}
			//监控设置
			if(config.configKey == "lotteryAlarmOpen"){
				$("#lotteryAlarmOpen").val(config.configValue);
			}
			if(config.configKey == "lotteryAlarmValue"){
				$("#lotteryAlarmValue").val(config.configValue);
			}
			if(config.configKey == "rechargeAlarmOpen"){
				$("#rechargeAlarmOpen").val(config.configValue);
			}
			if(config.configKey == "rechargeAlarmValue"){
				$("#rechargeAlarmValue").val(config.configValue);
			}
			if(config.configKey == "alarmEmailOpen"){
				$("#alarmEmailOpen").val(config.configValue);
			}
			if(config.configKey == "alarmEmail"){
				$("#alarmEmail").val(config.configValue);
			}
			if(config.configKey == "winAlarmOpen"){
				$("#winAlarmOpen").val(config.configValue);
			}
			if(config.configKey == "winAlarmValue"){
				$("#winAlarmValue").val(config.configValue);
			}
			if(config.configKey == "withdrawAlarmOpen"){
				$("#withdrawAlarmOpen").val(config.configValue);
			}
			if(config.configKey == "withdrawAlarmValue"){
				$("#withdrawAlarmValue").val(config.configValue);
			}
			if(config.configKey == "alarmPhoneOpen"){
				$("#alarmPhoneOpen").val(config.configValue);
			}
			if(config.configKey == "alarmPhone"){
				$("#alarmPhone").val(config.configValue);
			}
			if(config.configKey == "regShowPhone"){
				$("#regShowPhone").val(config.configValue);
			}
			
			if(config.configKey == "regShowSms"){
				$("#regShowSms").val(config.configValue);
			}
			if(config.configKey == "regDirectLogin"){
				$("#regDirectLogin").val(config.configValue);
			}
			if(config.configKey == "regRequiredPhone"){
				$("#regRequiredPhone").val(config.configValue);
			}
			if(config.configKey == "regShowEmail"){
				$("#regShowEmail").val(config.configValue);
			}
			if(config.configKey == "regRequiredEmail"){
				$("#regRequiredEmail").val(config.configValue);
			}
			
			if(config.configKey == "regShowQq"){
				$("#regShowQq").val(config.configValue);
			}
			if(config.configKey == "regRequiredQq"){
				$("#regRequiredQq").val(config.configValue);
			}
			if(config.configKey == "regOpen"){
				$("#regOpen").val(config.configValue);
			}
			if(config.configKey == "isOpenSignInActivity"){
				$("#isOpenSignInActivity").val(config.configValue);
			}
			if(config.configKey == "signinRechargeMoney"){
				$("#signinRechargeMoney").val(config.configValue);
			}
			if(config.configKey == "signinLastTime"){
				$("#signinLastTime").val(config.configValue);
			}
			if(config.configKey == "dayConsumeOrderTimeStart"){
				$("#dayConsumeOrderTimeStart").val(config.configValue);
			}
			if(config.configKey == "dayConsumeOrderTimeEnd"){
				$("#dayConsumeOrderTimeEnd").val(config.configValue);
			}
			//新手礼包开启
			if(config.configKey == "isNewUserGiftOpen"){
				$("#isNewUserGiftOpen").val(config.configValue);
			}
			if(config.configKey == "appDownGiftMoney"){
				$("#appDownGiftMoney").val(config.configValue);
			}
			if(config.configKey == "completeInfoGiftMoney"){
				$("#completeInfoGiftMoney").val(config.configValue);
			}
			if(config.configKey == "complateSafeGiftMoney"){
				$("#complateSafeGiftMoney").val(config.configValue);
			}
			if(config.configKey == "lotteryGiftMoney"){
				$("#lotteryGiftMoney").val(config.configValue);
			}
			if(config.configKey == "rechargeGiftMoney"){
				$("#rechargeGiftMoney").val(config.configValue);
			}
			if(config.configKey == "extendUserGiftMoney"){
				$("#extendUserGiftMoney").val(config.configValue);
			}
			if(config.configKey == "regShowTrueName"){
				$("#regShowTrueName").val(config.configValue);
			}
			if(config.configKey == "regRequiredTrueName"){
				$("#regRequiredTrueName").val(config.configValue);
			}
		}
	}
}

/**
 * 更新配置信息
 */
EditBizSystemConfigPage.prototype.updateDefaultSystemConfig = function(){
		var canlotteryhightModelValue=$("#canlotteryhightModel").val();
		var defaultInvitationCodeValue=$("#defaultInvitationCode").val();
		var secretCodeValue=$("#secretCode").val();
		var isOpenLevelAwardValue=$("#isOpenLevelAward").val();
		var automaticPromotionReceiveSwitchValue=$("#automaticPromotionReceiveSwitch").val();
		var guestModelValue=$("#guestModel").val();
		var guestMoneyValue=$("#guestMoney").val();
		var allowGuestLoginValue=$("#allowGuestLogin").val();
		var withDrawOneStrokeLowestMoneyValue=$("#withDrawOneStrokeLowestMoney").val();
		var withDrawEveryDayTotalMoneyValue=$("#withDrawEveryDayTotalMoney").val();
		var exceedWithdrawExpensesValue=$("#exceedWithdrawExpenses").val();
		var exceedTransferDayLimitExpensesValue=$("#exceedTransferDayLimitExpenses").val();
		var withDrawLotteryMultipleValue=$("#withDrawLotteryMultiple").val();
		var withDrawOneStrokeHighestMoneyValue=$("#withDrawOneStrokeHighestMoney").val();
		var withDrawEveryDayNumberValue=$("#withDrawEveryDayNumber").val();
		var thirdPayOrderBettingMultipleValue=$("#thirdPayOrderBettingMultiple").val();
		var isTransferSwichValue=$("#isTransferSwich").val();
		var releasePolicyBonusValue=$("#releasePolicyBonus").val();
		var releasePolicyDaySaralyValue=$("#releasePolicyDaySaraly").val();
		var maintainSwichValue=$("#maintainSwich").val();
		var maintainContentValue=$("#maintainContent").val();
		var genralagencyNumValue=$("#genralagencyNum").val();
		var directagencyNumValue=$("#directagencyNum").val();
		var ordinayagencyNumValue=$("#ordinayagencyNum").val();
		var isCloseValue=$("#isClose").val();
		var lhcLowerLotteryMoneyValue=$("#lhcLowerLotteryMoney").val();
		var lhcHighestLotteryMoneyValue=$("#lhcHighestLotteryMoney").val();
		var lhcHighestExpectLotteryMoneyValue=$("#lhcHighestExpectLotteryMoney").val();
		var lhcMaxWinMoneyValue=$("#lhcMaxWinMoney").val();
		var withDrawIsAllowNotEnoughValue=$("#withDrawIsAllowNotEnough").val();
		var codePlanSwitchValue=$("#codePlanSwitch").val();
        
		//监控设置		
		var lotteryAlarmOpen=$("#lotteryAlarmOpen").val();
		var lotteryAlarmValue=$("#lotteryAlarmValue").val();
		var rechargeAlarmOpen=$("#rechargeAlarmOpen").val();
		var rechargeAlarmValue=$("#rechargeAlarmValue").val();
		var alarmEmailOpen=$("#alarmEmailOpen").val();
		var alarmEmail=$("#alarmEmail").val();
		var winAlarmOpen=$("#winAlarmOpen").val();
		var winAlarmValue=$("#winAlarmValue").val();
		var withdrawAlarmOpen=$("#withdrawAlarmOpen").val();
		var withdrawAlarmValue=$("#withdrawAlarmValue").val();
		var alarmPhoneOpen=$("#alarmPhoneOpen").val();
		var alarmPhone=$("#alarmPhone").val();
		var regShowPhone=$("#regShowPhone").val();
		var regShowSms = $("#regShowSms").val();
		var regRequiredPhone=$("#regRequiredPhone").val();
		var regShowEmail=$("#regShowEmail").val();
		var regRequiredEmail=$("#regRequiredEmail").val();
		var regShowQq=$("#regShowQq").val();
		var regRequiredQq=$("#regRequiredQq").val();
		var regDirectLogin = $("#regDirectLogin").val();
		// 开放注册
		var regOpen = $("#regOpen").val();
		
		var isOpenSignInActivityValue=$("#isOpenSignInActivity").val();
		var signinRechargeMoneyValue=$("#signinRechargeMoney").val();
		var signinLastTimeValue=$("#signinLastTime").val();
		var dayConsumeOrderTimeStartValue=$("#dayConsumeOrderTimeStart").val();
		var dayConsumeOrderTimeEndValue=$("#dayConsumeOrderTimeEnd").val();
		var isOpenPhone=$("#isOpenPhone").val();
		var isOpenEmail=$("#isOpenEmail").val();
		var isNoSameNameBindBankCard=$("#isNoSameNameBindBankCard").val();
		
		//新手礼包设置
		//是否开启新手礼包
		var isNewUserGiftOpen=$("#isNewUserGiftOpen").val();
		
		//下载APP赠送金额
		var appDownGiftMoney=$("#appDownGiftMoney").val();
		
		//完善个人资料赠送金额
		var completeInfoGiftMoney=$("#completeInfoGiftMoney").val();
		//完善安全资料赠送金额
		var complateSafeGiftMoney=$("#complateSafeGiftMoney").val();
		//首笔投注赠送金额
		var lotteryGiftMoney=$("#lotteryGiftMoney").val();
		//首笔充值赠送金额
		var rechargeGiftMoney=$("#rechargeGiftMoney").val();
		//发展新玩家赠送金额
		var extendUserGiftMoney=$("#extendUserGiftMoney").val();
		// 是否展示真实姓名.
		var regShowTrueName=$("#regShowTrueName").val();
		// 是否必填真实姓名
		var regRequiredTrueName=$("#regRequiredTrueName").val();
		
		
		
		
		if(canlotteryhightModelValue.trim() == null || canlotteryhightModelValue == ""){
			var des=$("#canlotteryhightModelId").next().val();
			des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}
		
	/*	if(defaultInvitationCodeValue.trim() == null || defaultInvitationCodeValue == ""){
			var des=$("#defaultInvitationCodeId").next().val();
			des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}*/
		
		if(secretCodeValue.trim() == null || secretCodeValue == ""){
			var des=$("#secretCodeId").next().text();
				des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}
		if(guestModelValue.trim() == null || guestModelValue == ""){
			var des=$("#guestModelId").next().text();
			des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}
		if(guestMoneyValue.trim() == null || guestMoneyValue == ""){
			var des=$("#guestMoneyId").next().text();
			des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}
		if(allowGuestLoginValue.trim() == null || allowGuestLoginValue == ""){
		var des=$("#allowGuestLoginId").next().text();
		des = des.substring(0,des.length-1);
		swal(des + "参数值不能为空");
		return;
		}
		if(withDrawOneStrokeLowestMoneyValue.trim() == null || withDrawOneStrokeLowestMoneyValue == ""){
			var des=$("#withDrawOneStrokeLowestMoneyId").next().text();
				des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}
		
		if(withDrawEveryDayTotalMoneyValue.trim() == null || withDrawEveryDayTotalMoneyValue == ""){
			var des=$("#withDrawEveryDayTotalMoneyId").next().text();
				des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}
		if(exceedWithdrawExpensesValue.trim() == null || exceedWithdrawExpensesValue == ""){
			var des=$("#exceedWithdrawExpensesId").next().text();
				des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}
		if(exceedTransferDayLimitExpensesValue.trim() == null || exceedTransferDayLimitExpensesValue == ""){
			var des=$("#exceedTransferDayLimitExpensesId").next().text();
				des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}
		
		if(withDrawOneStrokeHighestMoneyValue.trim() == null || withDrawOneStrokeHighestMoneyValue == ""){
			var des=$("#withDrawOneStrokeHighestMoneyId").next().text();
				des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}
		if(withDrawEveryDayNumberValue.trim() == null || withDrawEveryDayNumberValue == ""){
			var des=$("#withDrawEveryDayNumberId").next().text();
				des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}
		if(thirdPayOrderBettingMultipleValue.trim() == null || thirdPayOrderBettingMultipleValue == ""){
			var des=$("#thirdPayOrderBettingMultipleId").next().text();
			des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}
		if(isTransferSwichValue.trim() == null || isTransferSwichValue == ""){
			var des=$("#isTransferSwichId").next().text();
				des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}
		if(releasePolicyBonusValue.trim() == null || releasePolicyBonusValue == ""){
			var des=$("#releasePolicyBonusId").next().text();
				des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}
		if(maintainSwichValue.trim() == null || maintainSwichValue == ""){
			var des=$("#maintainSwichId").next().text();
				des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}
		if(codePlanSwitchValue.trim() == null || codePlanSwitchValue == ""){
			var des=$("#codePlanSwitch").next().text();
				des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}
	/*	if(maintainContentValue.trim() == null || maintainContentValue == ""){
			var des=$("#maintainContentId").next().text();
				des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}*/
		if(releasePolicyDaySaralyValue.trim() == null || releasePolicyDaySaralyValue == ""){
			var des=$("#releasePolicyDaySaralyId").next().text();
				des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}
		if(genralagencyNumValue.trim() == null || genralagencyNumValue == ""){
			var des=$("#genralagencyNumId").next().text();
			des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}
		if(genralagencyNumValue.trim() == null || genralagencyNumValue == ""){
			var des=$("#genralagencyNumId").next().text();
			des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}
		if(directagencyNumValue.trim() == null || directagencyNumValue == ""){
			var des=$("#directagencyNumId").next().text();
			des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}
		if(ordinayagencyNumValue.trim() == null || ordinayagencyNumValue == ""){
			var des=$("#ordinayagencyNumId").next().text();
			des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}
		
		if(lhcLowerLotteryMoneyValue.trim() == null || lhcLowerLotteryMoneyValue == ""){
			var des=$("#lhcLowerLotteryMoneyId").next().text();
				des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}
		if(lhcHighestLotteryMoneyValue.trim() == null || lhcHighestLotteryMoneyValue == ""){
			var des=$("#lhcHighestLotteryMoneyId").next().text();
			des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}
		if(lhcHighestExpectLotteryMoneyValue.trim() == null || lhcHighestExpectLotteryMoneyValue == ""){
			var des=$("#lhcHighestExpectLotteryMoneyId").next().text();
			des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}
		if(lhcMaxWinMoneyValue.trim() == null || lhcMaxWinMoneyValue == ""){
			var des=$("#lhcMaxWinMoneyId").next().text();
			des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}
		if(isCloseValue.trim() == null || isCloseValue == ""){
			var des=$("#isCloseId").next().text();
			des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}
		if(withDrawIsAllowNotEnoughValue.trim() == null || withDrawIsAllowNotEnoughValue == ""){
			var des=$("#withDrawIsAllowNotEnoughId").next().text();
				des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}
		
		if(isOpenSignInActivityValue == 1){
			if(signinRechargeMoneyValue == null || signinRechargeMoneyValue == ""){
				var des=$("#signinRechargeMoneyId").next().text();
				des = des.substring(0,des.length-1);
				swal(des + "参数值不能为空");
				return;
			}
		}
		
		//新手礼包设置
		if(appDownGiftMoney.trim() == null || appDownGiftMoney == ""){
			var des=$("#appDownGiftMoneyId").next().text();
			des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}
		if(completeInfoGiftMoney.trim() == null || completeInfoGiftMoney == ""){
			var des=$("#completeInfoGiftMoneyId").next().text();
			des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			
			return;
		}if(complateSafeGiftMoney.trim() == null || complateSafeGiftMoney == ""){
			var des=$("#complateSafeGiftMoneyId").next().text();
			des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}if(lotteryGiftMoney.trim() == null || lotteryGiftMoney == ""){
			var des=$("#lotteryGiftMoneyId").next().text();
			des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}if(rechargeGiftMoney.trim() == null || rechargeGiftMoney == ""){
			var des=$("#rechargeGiftMoneyId").next().text();
			des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}if(extendUserGiftMoney.trim() == null || extendUserGiftMoney == ""){
			var des=$("#extendUserGiftMoney").next().text();
			des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}if(regShowTrueName.trim() == null || regShowTrueName == ""){
			var des=$("#regShowTrueName").next().text();
			des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}if(regRequiredTrueName.trim() == null || regRequiredTrueName == ""){
			var des=$("#regRequiredTrueName").next().text();
			des = des.substring(0,des.length-1);
			swal(des + "参数值不能为空");
			return;
		}if(regRequiredTrueName.trim() == 1 && regShowTrueName.trim() == 0){
			swal("真实姓名必填开启时,展示框必须开启!!!");
			return;
		}
		
		/**
		 * 监控设置
		 */
		if(lotteryAlarmValue.trim() == null || lotteryAlarmValue == ""){
			swal("投注监控金额不能为空");
			return;
		}
		if(rechargeAlarmValue.trim() == null || rechargeAlarmValue == ""){
			swal("充值监控金额不能为空");
			return;
		}
		
		if(winAlarmValue.trim() == null || winAlarmValue == ""){
			swal("中奖监控金额不能为空");
			return;
		}
		
		if(withdrawAlarmValue.trim() == null || withdrawAlarmValue == ""){
			swal("取现监控金额不能为空");
			return;
		}
		
		if(alarmEmailOpen ==1 ){
			if(alarmEmail == null || alarmEmail == ""){
				swal("邮箱地址不能为空");
				return;	
			}else{
				var myreg = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
				  if(!myreg.test(alarmEmail)){
					  swal("请输入有效的E_mail！");
					  return;
				  }
			}

		}
		
		
		
		if(alarmPhoneOpen ==1){
			if(alarmPhone == null || alarmPhone == ""){
				swal("手机号码不能为空");
				return;	
			}else{
				var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/; 
				  if(!myreg.test(alarmPhone)){
					  swal("请输入有效的手机号码！");
					  return;
				  }
			}
		}
		
		
		
		var map = {};
		
		var canlotteryhightModelKey = "canlotteryhightModel";
		var defaultInvitationCodeKey = "defaultInvitationCode";
		var secretCodeKey = "secretCode";
		var isOpenLevelAwardKey="isOpenLevelAward";
		var automaticPromotionReceiveSwitchKey="automaticPromotionReceiveSwitch";
		var guestModelKey="guestModel";
		var guestMoneyKey="guestMoney";
		var allowGuestLoginKey="allowGuestLogin";
		var withDrawOneStrokeLowestMoneyKey = "withDrawOneStrokeLowestMoney";
		var withDrawEveryDayTotalMoneyKey = "withDrawEveryDayTotalMoney";
		var exceedWithdrawExpensesKey = "exceedWithdrawExpenses";
		var exceedTransferDayLimitExpensesKey = "exceedTransferDayLimitExpenses";
		var withDrawLotteryMultipleKey = "withDrawLotteryMultiple";
		var withDrawOneStrokeHighestMoneyKey = "withDrawOneStrokeHighestMoney";
		var withDrawEveryDayNumberKey = "withDrawEveryDayNumber";
		var isTransferSwichKey = "isTransferSwich";
		var releasePolicyBonusKey = "releasePolicyBonus";
		var releasePolicyDaySaralyKey = "releasePolicyDaySaraly";
		var maintainSwichKey = "maintainSwich";
		var maintainContentKey = "maintainContent";
		var genralagencyNumKey = "genralagencyNum";
		var directagencyNumKey = "directagencyNum";
		var ordinayagencyNumKey = "ordinayagencyNum";
		var codePlanSwitchKey = "codePlanSwitch";
		
		var isCloseKey = "isClose";
		var lhcLowerLotteryMoneyKey = "lhcLowerLotteryMoney";
		var lhcHighestLotteryMoneyKey = "lhcHighestLotteryMoney";
		var lhcHighestExpectLotteryMoneyKey = "lhcHighestExpectLotteryMoney";
		var lhcMaxWinMoneyKey = "lhcMaxWinMoney";
		var withDrawIsAllowNotEnoughKey="withDrawIsAllowNotEnough";
		var thirdPayOrderBettingMultipleKey="thirdPayOrderBettingMultiple";
		
		/**
		 * 监控设置key值
		 */
		var lotteryAlarmOpenKey = "lotteryAlarmOpen";
		var lotteryAlarmValueKey = "lotteryAlarmValue";
		var rechargeAlarmOpenKey = "rechargeAlarmOpen";
		var rechargeAlarmValueKey = "rechargeAlarmValue";
		var alarmEmailOpenKey = "alarmEmailOpen";
		var alarmEmailKey = "alarmEmail";
		var winAlarmOpenKey = "winAlarmOpen";
		var winAlarmValueKey = "winAlarmValue";
		var withdrawAlarmOpenKey = "withdrawAlarmOpen";
		var withdrawAlarmValueKey = "withdrawAlarmValue";
		var alarmPhoneOpenKey = "alarmPhoneOpen";
		var alarmPhoneKey = "alarmPhone";
		var regShowSmskey = "regShowSms";
		var regShowPhoneKey="regShowPhone";
		var regRequiredPhoneKey="regRequiredPhone";
		var regShowEmailKey="regShowEmail";
		var regRequiredEmailKey="regRequiredEmail";
		var regShowQqKey="regShowQq";
		var regRequiredQqKey="regRequiredQq";
		var regDirectLoginKey = "regDirectLogin";
		var regOpenKey = "regOpen";
		
		var isOpenSignInActivityKey="isOpenSignInActivity";
		var signinRechargeMoneyKey="signinRechargeMoney";
		var signinLastTimeKey="signinLastTime";
		var dayConsumeOrderTimeStartKey="dayConsumeOrderTimeStart";
		var dayConsumeOrderTimeEndKey="dayConsumeOrderTimeEnd";
		
		var isOpenPhoneKey="isOpenPhone";
		var isOpenEmailKey="isOpenEmail";
		var isNoSameNameBindBankCardKey="isNoSameNameBindBankCard";
		
		
		//新手礼包
		
		var isNewUserGiftOpenKey="isNewUserGiftOpen";
		var appDownGiftMoneyKey="appDownGiftMoney"
		var completeInfoGiftMoneyKey="completeInfoGiftMoney";
		var complateSafeGiftMoneyKey="complateSafeGiftMoney";
		var lotteryGiftMoneyKey="lotteryGiftMoney";
		var rechargeGiftMoneyKey="rechargeGiftMoney";
		var extendUserGiftMoneyKey="extendUserGiftMoney";
		var regShowTrueNameKey="regShowTrueName";
		var regRequiredTrueNameKey="regRequiredTrueName";
		
		map[canlotteryhightModelKey] = canlotteryhightModelValue;
		map[defaultInvitationCodeKey] = defaultInvitationCodeValue;
		map[secretCodeKey] = secretCodeValue;
		map[isOpenLevelAwardKey]=isOpenLevelAwardValue;
		map[automaticPromotionReceiveSwitchKey]=automaticPromotionReceiveSwitchValue;
		map[guestModelKey] = guestModelValue;
		map[guestMoneyKey] = guestMoneyValue;
		map[allowGuestLoginKey] = allowGuestLoginValue;
		map[withDrawOneStrokeLowestMoneyKey] = withDrawOneStrokeLowestMoneyValue;
		map[withDrawEveryDayTotalMoneyKey] = withDrawEveryDayTotalMoneyValue;
		map[exceedWithdrawExpensesKey] = exceedWithdrawExpensesValue;
		map[exceedTransferDayLimitExpensesKey] = exceedTransferDayLimitExpensesValue;
		map[withDrawLotteryMultipleKey] = withDrawLotteryMultipleValue;
		map[withDrawOneStrokeHighestMoneyKey] = withDrawOneStrokeHighestMoneyValue;
		map[withDrawEveryDayNumberKey] = withDrawEveryDayNumberValue;
		map[thirdPayOrderBettingMultipleKey] = thirdPayOrderBettingMultipleValue;
		
		
		map[isTransferSwichKey] = isTransferSwichValue;
		map[releasePolicyBonusKey] = releasePolicyBonusValue;
		map[releasePolicyDaySaralyKey] = releasePolicyDaySaralyValue;
		map[maintainSwichKey] = maintainSwichValue;
		map[maintainContentKey] = maintainContentValue;
		map[genralagencyNumKey] = genralagencyNumValue;
		map[directagencyNumKey] = directagencyNumValue;
		map[ordinayagencyNumKey] = ordinayagencyNumValue;
		map[isCloseKey] = isCloseValue;
		map[lhcLowerLotteryMoneyKey] = lhcLowerLotteryMoneyValue;
		map[lhcHighestLotteryMoneyKey] = lhcHighestLotteryMoneyValue;
		map[lhcHighestExpectLotteryMoneyKey] = lhcHighestExpectLotteryMoneyValue;
		map[lhcMaxWinMoneyKey] = lhcMaxWinMoneyValue;
		map[withDrawIsAllowNotEnoughKey] = withDrawIsAllowNotEnoughValue;
		map[codePlanSwitchKey] = codePlanSwitchValue;
		
		//监控设置
		map[lotteryAlarmOpenKey] = lotteryAlarmOpen;
		map[lotteryAlarmValueKey] = lotteryAlarmValue;
		map[rechargeAlarmOpenKey] = rechargeAlarmOpen;
		map[rechargeAlarmValueKey] = rechargeAlarmValue;
		map[alarmEmailOpenKey] = alarmEmailOpen;
		map[alarmEmailKey] = alarmEmail;
		map[winAlarmOpenKey] = winAlarmOpen;
		map[winAlarmValueKey] = winAlarmValue;
		map[withdrawAlarmOpenKey] = withdrawAlarmOpen;
		map[withdrawAlarmValueKey] = withdrawAlarmValue;
		map[alarmPhoneOpenKey] = alarmPhoneOpen;
		map[alarmPhoneKey] = alarmPhone;
		map[regShowSmskey] = regShowSms;
		map[regShowPhoneKey]=regShowPhone;
		map[regRequiredPhoneKey]=regRequiredPhone;
		map[regShowEmailKey]=regShowEmail;
		map[regRequiredEmailKey]=regRequiredEmail;
		map[regShowQqKey]=regShowQq;
		map[regRequiredQqKey]=regRequiredQq;
		map[isOpenSignInActivityKey]=isOpenSignInActivityValue;
		map[signinRechargeMoneyKey]=signinRechargeMoneyValue
		map[signinLastTimeKey]=signinLastTimeValue;
		map[dayConsumeOrderTimeStartKey]=dayConsumeOrderTimeStartValue;
		map[dayConsumeOrderTimeEndKey]=dayConsumeOrderTimeEndValue;
		map[isOpenPhoneKey]=isOpenPhone;
		map[isOpenEmailKey]=isOpenEmail;
		map[isNoSameNameBindBankCardKey]=isNoSameNameBindBankCard;
		map[regDirectLoginKey] = regDirectLogin;
		map[regOpenKey] = regOpen;
		
		
		map[isNewUserGiftOpenKey]=isNewUserGiftOpen;
		map[appDownGiftMoneyKey]=appDownGiftMoney;
		map[completeInfoGiftMoneyKey]=completeInfoGiftMoney;
		map[complateSafeGiftMoneyKey]=complateSafeGiftMoney;
		map[lotteryGiftMoneyKey]=lotteryGiftMoney;
		map[rechargeGiftMoneyKey]=rechargeGiftMoney;
		map[extendUserGiftMoneyKey]=extendUserGiftMoney;
		map[regShowTrueNameKey]=regShowTrueName;
		map[regRequiredTrueNameKey]=regRequiredTrueName;
		//设置值到map中进行保存
		
		var currentSystem = currentUser.bizSystem;
		var bizSystem = "";
		if(currentSystem != "SUPER_SYSTEM"){
			bizSystem = currentSystem;
		}else{
			bizSystem = $("#bizSystem").val();
		}
	managerBizSystemConfigAction.updateDefaultSystemConfig(map,bizSystem,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({title: "保存成功",text: "您已经保存了这条信息。",type: "success"},
			function(){
				var currentSystem = currentUser.bizSystem;
				var bizSystem = "";
				if(currentSystem != "SUPER_SYSTEM"){
					bizSystem = currentSystem;
				}else{
					bizSystem = $("#bizSystem").val();
				}
				editBizSystemConfigPage.getBizSystemConfig(bizSystem);
			});
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取信息失败.");
		}
    });
};

/**
 * 一键刷新缓存
 */
EditBizSystemConfigPage.prototype.refreshCache = function(){
	managerBizSystemConfigAction.refreshCache(function(r){
		if (r[0] != null && r[0] == "ok") {
			swal("刷新成功！");
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取信息失败.");
		}
    });
};
