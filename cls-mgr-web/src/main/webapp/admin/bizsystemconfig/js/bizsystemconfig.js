function BizSystemConfigPage() {
}
var bizsystemConfigPage = new BizSystemConfigPage();

bizsystemConfigPage.param = {

};

/**
 * 查询参数
 */
bizsystemConfigPage.queryParam = {
	configKey : null,
	configValue : null
};

// 分页参数
bizsystemConfigPage.pageParam = {
	pageNo : 1,
	pageSize : defaultPageSize

};

$(document).ready(function() {

	bizsystemConfigPage.initTableData(); // 初始化查询数据
});

/**
 * 初始化列表数据
 */
BizSystemConfigPage.prototype.initTableData = function() {
	bizsystemConfigPage.queryParam = getFormObj($("#queryForm"));
	bizsystemConfigPage.table = $('#systemConfigTable').dataTable(
			{
				"oLanguage" : dataTableLanguage,
				"bProcessing" : false,
				"bServerSide" : true,
				"scrollX" : true,
				"iDisplayLength" : defaultPageSize,
				"ajax" : function(data, callback, settings) {
					bizsystemConfigPage.pageParam.pageSize = data.length;// 页面显示记录条数
					bizsystemConfigPage.pageParam.pageNo = (data.start / data.length) + 1;// 当前页码
					managerBizSystemConfigAction.getAllBizSystemConfig(bizsystemConfigPage.queryParam, bizsystemConfigPage.pageParam, function(r) {
						// 封装返回数据
						var returnData = {
							recordsTotal : 0,
							recordsFiltered : 0,
							data : null
						};
						if (r[0] != null && r[0] == "ok") {
							returnData.recordsTotal = r[1].totalRecNum;// 返回数据全部记录
							returnData.recordsFiltered = r[1].totalRecNum;// 后台不实现过滤功能，每次查询均视作全部结果
							returnData.data = r[1].pageContent;// 返回的数据列表
						} else if (r[0] != null && r[0] == "error") {
							swal(r[1]);
						} else {
							swal("查询数据失败!");
						}
						callback(returnData);
					});
				},
				"columns" : [
						{
							"data" : "bizSystemName",
							"bVisible" : currentUser.bizSystem == 'SUPER_SYSTEM' ? true : false
						},
						{
							"data" : "configKey"
						},
						{
							"data" : "configValue"
						},
						{
							"data" : "configDes"
						},
						{
							"data" : "enable",
							render : function(data, type, row, meta) {
								if (data == 1) {
									return '启用';
								} else {
									return '禁用';
								}

							}
						},
						{
							"data" : "createDate",
							render : function(data, type, row, meta) {
								return dateFormat(data, 'yyyy-MM-dd hh:mm:ss');
							}
						},
						{
							"data" : "updateAdmin",
							"bVisible" : false
						},
						{
							"data" : "updateDate",
							"bVisible" : false,
							render : function(data, type, row, meta) {
								return dateFormat(data, 'yyyy-MM-dd hh:mm:ss');
							}
						},

						{
							"data" : "id",
							render : function(data, type, row, meta) {
								return "<a class='btn btn-info btn-sm btn-bj' onclick='bizsystemConfigPage.editBizSystemConfig(" + data
										+ ")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;"
										+ "<a class='btn btn-danger btn-sm btn-del' onclick='bizsystemConfigPage.delBizSystemConfig(" + data
										+ ")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";
							}
						} ]
			}).api();
}

/**
 * 查询数据
 */
BizSystemConfigPage.prototype.findBizSystemConfig = function() {
	bizsystemConfigPage.queryParam.configKey = $("#configKey").val();
	bizsystemConfigPage.queryParam.configValue = $("#configValue").val();
	bizsystemConfigPage.queryParam.enable = $("#enable").val();
	bizsystemConfigPage.queryParam.bizSystem = $("#bizSystem").val();
	bizsystemConfigPage.table.ajax.reload();
};

/**
 * 删除
 */
BizSystemConfigPage.prototype.delBizSystemConfig = function(id) {
	swal({
		title : "您确定要删除这条信息吗",
		text : "请谨慎操作！",
		type : "warning",
		showCancelButton : true,
		confirmButtonColor : "#DD6B55",
		cancelButtonText : "取消",
		confirmButtonText : "删除",
		closeOnConfirm : false
	}, function() {
		managerBizSystemConfigAction.delBizSystemConfigById(id, function(r) {
			if (r[0] != null && r[0] == "ok") {
				swal({
					title : "提示",
					text : "删除成功",
					type : "success"
				});
				bizsystemConfigPage.findBizSystemConfig();
			} else if (r[0] != null && r[0] == "error") {
				swal(r[1]);
			} else {
				swal("删除信息失败.");
			}
		});
	});
};

BizSystemConfigPage.prototype.addBizSystemConfig = function() {
	layer.open({
		type : 2,
		title : '业务系统参数新增',
		maxmin : false,
		shadeClose : true,
		// 点击遮罩关闭层
		area : [ '800px', '420px' ],
		// area: ['70%', '80%'],
		content : contextPath + "/managerzaizst/bizsystemconfig/editbizsystemconfig.html",
		cancel : function(index) {
			bizsystemConfigPage.findBizSystemConfig();
		}
	});
}

BizSystemConfigPage.prototype.editBizSystemConfig = function(id) {
	layer.open({
		type : 2,
		title : '业务系统参数编辑',
		maxmin : false,
		shadeClose : true,
		// 点击遮罩关闭层
		area : [ '800px', '420px' ],
		// area: ['70%', '80%'],
		content : contextPath + "/managerzaizst/bizsystemconfig/" + id + "/editbizsystemconfig.html",
		cancel : function(index) {
			bizsystemConfigPage.findBizSystemConfig();
		}
	});
}

BizSystemConfigPage.prototype.closeLayer = function(index) {
	layer.close(index);
	bizsystemConfigPage.findBizSystemConfig();
}