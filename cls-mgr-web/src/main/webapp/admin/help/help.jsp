<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>快捷支付平台设置</title>
<jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
<body>
<div class="animated fadeIn">
	<div class="ibox-content">
		<div class="row">
			<form class="form-horizontal" id="helpForm">
                <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
					<div class="row">
							<div class="col-sm-3 biz" >
		                       <div class="input-group m-b">
		                           <span class="input-group-addon">商户：</span>
		                            <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
		                       </div>
		                   </div>
						<div class="col-sm-2">
							<div class="input-group m-b">
								<span class="input-group-addon">类型：</span>
								<cls:enmuSel name="type"
									className="com.team.lottery.enums.EHelpType"
									options="class:ipt form-control" id="type" />
							</div>
						</div>
						<div class="col-sm-3">
							<div class="input-group m-b">
								<span class="input-group-addon">标题：</span>
								<input type="text"
									value="" name="title" id="title" class="form-control">
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group  text-right">
								<button type="button" class="btn btn-outline btn-default btn-kj"
									onclick="helpPage.findHelp()">
									<i class="fa fa-search"></i>&nbsp;查 询
								</button>
								&nbsp;&nbsp;&nbsp;&nbsp;
	
								<button type="button"
									class="btn btn-outline btn-default btn-add"
									onclick="helpPage.addHelp()">
									<i class="fa fa-plus"></i>&nbsp;新增
								</button>
							</div>
						</div>
					</div>
				</c:if>
                <c:if test="${admin.bizSystem !='SUPER_SYSTEM'}">
					<div class="row">
						<div class="col-sm-2">
							<div class="input-group m-b">
								<span class="input-group-addon">类型：</span>
								<cls:enmuSel name="type"
									className="com.team.lottery.enums.EHelpType"
									options="class:ipt form-control" id="type" />
							</div>
						</div>
						<div class="col-sm-3">
							<div class="input-group m-b">
								<span class="input-group-addon">标题：</span>
								<input type="text"
									value="" name="title" id="title" class="form-control">
							</div>
						</div>
						<div class="col-sm-3">
						</div>
						<div class="col-sm-4">
							<div class="form-group  text-right">
								<button type="button" class="btn btn-outline btn-default btn-kj"
									onclick="helpPage.findHelp()">
									<i class="fa fa-search"></i>&nbsp;查 询
								</button>
								&nbsp;&nbsp;&nbsp;&nbsp;
	
								<button type="button"
									class="btn btn-outline btn-default btn-add"
									onclick="helpPage.addHelp()">
									<i class="fa fa-plus"></i>&nbsp;添加帮助信息
								</button>
							</div>
						</div>
					</div>
               </c:if>
			</form>
		</div>
		<div class="jqGrid_wrapper">
			<div class="ui-jqgrid ">
				<div class="ui-jqgrid-view ">
					<div class="ui-jqgrid-hdiv">
						<div class="ui-jqgrid-hbox" style="padding-right: 0">
							<table id="helpTable"
								class="ui-jqgrid-htable ui-common-table table table-bordered">
								<thead>
									<tr class="ui-jqgrid-labels">
										<th class="ui-th-column ui-th-ltr">序号</th>
										<th class="ui-th-column ui-th-ltr">商户</th>
										<th class="ui-th-column ui-th-ltr">类型</th>
										<th class="ui-th-column ui-th-ltr">标题</th>
										<th class="ui-th-column ui-th-ltr">是否常见</th>
										<th class="ui-th-column ui-th-ltr">发布日期</th>
										<th class="ui-th-column ui-th-ltr">操作</th>
									</tr>
								</thead>

							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	<!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/help/js/help.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerHelpAction.js'></script>
</body>
</html>

