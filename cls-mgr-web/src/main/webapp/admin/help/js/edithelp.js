function EditHelpPage(){}
var editHelpPage = new EditHelpPage();

editHelpPage.param = {
   	
};

$(document).ready(function() {
	var helpId = editHelpPage.param.id;  //获取查询的帮助ID
	if(helpId != null){
		editHelpPage.editHelp(helpId);
	}
});


/**
 * 保存帮助信息
 */
EditHelpPage.prototype.saveData = function(){
	
	//document.domain = olddomain;
	var id = $("#helpId").val();
	var bizSystem = $("#bizSystem").val();
	var type = $("#type").val();
	var title = $("#title").val();
	var essentialsDes = $("#essentialsDes").val();
	var content = editor.html();
	var frequentlyQuestion = $("#frequentlyQuestion").val();
	
	if(currentUser.bizSystem == "SUPER_SYSTEM"){
		if(bizSystem==null || bizSystem.trim()==""){
			swal("请选择业务系统!");
			$("#bizSystem").focus();
			return;
		}
	}else{
		bizSystem = currentUser.bizSystem;
	}
	
	if(title==null || title.trim()==""){
		swal("请输入公告标题!");
		$("#title").focus();
		return;
	}
	if(essentialsDes==null || essentialsDes.trim()==""){
		swal("请输入概要说明!");
		$("#essentialsDes").focus();
		return;
	}
	if(content==null || content.trim()==""){
		swal("请输入公告内容!");
		editor.focus();
		return;
	}
	if(type==null || type.trim()==""){
		swal("请选择类型!");
		$("#type").focus();
		return;
	}
	if(frequentlyQuestion==null || frequentlyQuestion.trim()==""){
		swal("请选择是否常见!");
		$("#frequentlyQuestion").focus();
		return;
	}
	var help = {};
	help.id = id;
	help.bizSystem = bizSystem;
	help.type = type;
	help.title = title;
	help.essentialsDes = essentialsDes;
	help.content = content;
	help.frequentlyQuestion = frequentlyQuestion;
	managerHelpAction.saveOrUpdateHelp(help,function(r){
		if (r[0] != null && r[0] == "ok") {
			var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
			parent.helpPage.closeLayer(index);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("保存帮助信息失败.");
		}
    });
};


/**
 * 赋值帮助信息
 */
EditHelpPage.prototype.editHelp = function(id){
	managerHelpAction.getHelpById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			$("#helpId").val(r[1].id);
			$("#bizSystem").val(r[1].bizSystem);
			$("#bizSystem").attr("disabled","disabled");
			$("#type").val(r[1].type);
			$("#essentialsDes").val(r[1].essentialsDes);
			$("#title").val(r[1].title);
			$("#frequentlyQuestion").val(r[1].frequentlyQuestion);
			editor.html(r[1].content);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取帮助信息失败.");
		}
    });
};