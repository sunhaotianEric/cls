function HelpPage(){}
var helpPage = new HelpPage();

helpPage.param = {
   	
};
//分页参数
helpPage.pageParam = {
		    pageNo : 1,
		    pageSize : defaultPageSize
};
/**
 * 查询参数
 */
helpPage.queryParam = {
		bizSystem:null,
		type : null,
		title : null
};
$(document).ready(function() {
	helpPage.initTableData();
});

/**
 * 加载所有的帮助中心
 */
HelpPage.prototype.getAllHelps = function(){
	managerHelpAction.getAllHelps(function(r){
		if (r[0] != null && r[0] == "ok") {
			helpPage.refreshHelpPages(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询帮助中心数据失败.");
		}
    });
};

/**
 * 刷新列表数据
 */
HelpPage.prototype.refreshHelpPages = function(helps){
	var helpListObj = $("#helpList");
	helpListObj.html("");
	var str = "";

	for(var i = 0 ; i < helps.length; i++){
		var help = helps[i];
		str += "<tr>";
		str += "  <td><input type='hidden' name='helpId' value='"+ help.id +"'/>"+ help.id +"</td>";
		str += "  <td>"+ help.typeDes +"</td>";
		str += "  <td>"+ help.title +"</td>";
		if(help.frequentlyQuestion == 1){
			str += "  <td>是</td>";
		}else{
			str += "  <td>否</td>";
		}
		str += "  <td>"+ help.logtimeStr +"</td>";
		str += "  <td><a href='javascript:void(0)' onclick='helpPage.editHelp("+ help.id +")'>编辑</a>&nbsp;|&nbsp;<a href='javascript:void(0)' onclick='helpPage.delHelp("+ help.id +")'>删除</a></td>";

		str += "</tr>";
		
		helpListObj.append(str);
		str = "";
	}
};

/**
 * 删除帮助中心信息
 */
HelpPage.prototype.delHelp = function(id){
	if (confirm("确认要删除？")){
		managerHelpAction.delHelp(id,function(r){
			if (r[0] != null && r[0] == "ok") {
				helpPage.getAllHelps();
			}else if(r[0] != null && r[0] == "error"){
				showErrorDlg(jQuery(document.body), r[1]);
			}else{
				showErrorDlg(jQuery(document.body), "删除帮助中心信息失败.");
			}
	    });
	}
};

/**
 * 编辑帮助中心信息
 */
HelpPage.prototype.editHelp = function(id){
	self.location = contextPath + "/managerzaizst/help/" + id+"/edithelp.html";
};
HelpPage.prototype.initTableData = function(){

	helpPage.queryParam = getFormObj($("#helpForm"));
	helpPage.table = $('#helpTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			helpPage.pageParam.pageSize = data.length;//页面显示记录条数
			helpPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerHelpAction.getAllHelps(helpPage.queryParam,helpPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
				returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询所有的快捷支付数据失败.");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": null},
		            {"data": "bizSystemName",
		            	"bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "typeDes"},
		            {"data": "title"},
		            {"data": "frequentlyQuestion",
		            	render: function(data, type, row, meta) {
		            		if (data == "1") {
		            			return '是';
		            		} else {
		            			return '否';
		            		}
		            		return data;
	            		}
		            },
		            {"data": "logtimeStr"},
		            {
	                	 "data": "id",
	                	 render: function(data, type, row, meta) {
	                		 var str="<a class='btn btn-info btn-sm btn-bj' onclick='helpPage.editHelp("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;" +
	             		 		"<a class='btn btn-danger btn-sm btn-del' onclick='helpPage.delHelp("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";
	                		 
	                			 
	                		 return str ;
	                	 }
	                }
	            ]
	}).api();
};

/**
 * 查询数据
 */
HelpPage.prototype.findHelp = function(){
	var bizSystem = $("#bizSystem").val();
	var type = $("#type").val();
	var title = $("#title").val();
	if(bizSystem == ""){
		helpPage.queryParam.bizSystem = null;
	}else{
		helpPage.queryParam.bizSystem = bizSystem;
	}
	if(type == ""){
		helpPage.queryParam.type = null;
	}else{
		helpPage.queryParam.type = type;
	}
	if(title == ""){
		helpPage.queryParam.title = null;
	}else{
		helpPage.queryParam.title = title;
	}
	helpPage.table.ajax.reload();
};




/**
 * 删除
 */
HelpPage.prototype.delHelp = function(id){
	
	   swal({
           title: "您确定要删除这条信息吗",
           text: "请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "删除",
           closeOnConfirm: false
       },
       function() {
    	   managerHelpAction.delHelp(id,function(r){
   			if (r[0] != null && r[0] == "ok") {
   				swal({ title: "提示", text: "删除成功",type: "success"});
   				helpPage.table.ajax.reload();
   			}else if(r[0] != null && r[0] == "error"){
   				swal(r[1]);
   			}else{
   				swal("删除信息失败.");
   			}
   	    });
       });
};

/**
 * 新增
 */
HelpPage.prototype.addHelp=function()
{
	
	 layer.open({
         type: 2,
         title: '帮助信息新增',
         maxmin: false,
         shadeClose: true,
         //点击遮罩关闭层
         area: ['70%', '80%'],
         content: contextPath + "/managerzaizst/help/edithelp.html",
         cancel: function(index){ 
      	   helpPage.table.ajax.reload();
      	   }
     });
}
/**
 * 编辑
 */
HelpPage.prototype.editHelp=function(id)
{
	
	  layer.open({
           type: 2,
           title: '帮助信息编辑',
           maxmin: false,
           shadeClose: true,
           //点击遮罩关闭层
           area: ['70%', '80%'],
           content: contextPath + "/managerzaizst/help/"+id+"/edithelp.html",
           cancel: function(index){ 
        	   helpPage.table.ajax.reload();
        	   }
       });
}

HelpPage.prototype.closeLayer=function(index){
	layer.close(index);
	swal({
        title: "保存成功",
        text: "您已经保存了这条图片。",
        type: "success"});
	helpPage.table.ajax.reload();
}