<%@ page language="java" contentType="text/html; charset=UTF-8" 
	import = "com.team.lottery.enums.EHelpType,com.team.lottery.system.SystemConfigConstant" 
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
	StringBuffer url = request.getRequestURL();
	String domainUrl = url.toString().replaceAll(request.getRequestURI(), "")+path;
	String staticImageServerUrl = "http://"+SystemConfigConstant.picHost;
%>
<html>
<head>
    <meta charset="utf-8" />
    <title>帮助中心</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/help/js/edithelp.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerHelpAction.js'></script>
    
    <link rel="stylesheet" href="<%=path%>/resources/kindeditor-4.1.10/themes/default/default.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" />
	<script charset="utf-8" src="<%=path%>/resources/kindeditor-4.1.10/kindeditor-min.js"></script>
	<script charset="utf-8" src="<%=path%>/resources/kindeditor-4.1.10/lang/zh_CN.js"></script>
     <script>
       var  helpPage=parent.helpPage;
       var layerindex = parent.layer.getFrameIndex(window.name);
       var olddomain = document.domain;
         
		var editor;  //editor.html()
		//图片服务器地址
		var staticImageUrl='<%=domainUrl%>';
		var url = staticImageUrl.split('//');
		var imgurl='<%=staticImageServerUrl%>';
		KindEditor.ready(function(K) {
			editor = K.create('textarea[name="content"]', {
				uploadJson : location.protocol+'//'+url[1]+'/UploadFileForJsonServlet?dirfile=help',
				fileManagerJson :imgurl+'/file_manager_json.jsp',
				allowFileManager : true
			});
		});
	</script>
</head>
<body>
   <div class="alert alert-info">当前位置<b class="tip"></b>帮助中心管理<b class="tip"></b>编辑帮助信息</div>
   	
   	<table class="tb">
         <tr>
         	<td> <a href="<%=path %>/managerzaizst/help.html">信息管理</a> &nbsp;&nbsp; <a href="<%=path %>/managerzaizst/help/help_add.html">添加信息</a></td>
         </tr>
    </table>
    <form action="javascript:void(0)">
    	<c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
	    	<div class="col-sm-12">
            	<div class="input-group m-b">
                	<span class="input-group-addon">商户：</span>
                 	<cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
            	</div>
			</div>    
    	</c:if>
	    <table class="tb">
	   		<tr>
	   			<td width="15px">类型：</td>
	   			<td>
	   				<select class="select" id="type">
							<%
							EHelpType [] helpTypes = EHelpType.values();
							for(EHelpType helpType : helpTypes){
							%>
						<option value='<%=helpType.getCode() %>'><%=helpType.getDescription() %></option>
							<%
							}
							%>
					</select>
	   			</td>
	   		</tr>
	   		<tr>
	   			<td width="15px">帮助标题：<input type="hidden" id="helpId"/></td>
	   			<td><input type="text" id="title" size="50px"/></td>
	   		</tr>
	   		<tr>
	   			<td width="15px">概要说明：</td>
	   			<td><input type="text" id="essentialsDes" size="100px"/></td>
	   		</tr>
	   		<tr>
	   			<td>帮助内容：</td>
	   			<td>
	   			   <textarea name="content" id="content" style="width:800px;height:400px;visibility:hidden;"></textarea>
	   			</td>
	   		</tr>
	   		<tr>
	   			<td width="15px">是否常见：</td>
	   			<td>
	   				<select class="select" id="frequentlyQuestion">
	   					<option value="0">否</option>
	   					<option value="1">是</option>
					</select>
	   			</td>
	   		</tr>
	   		<tr>
	   			<td></td>
	   			<td><input type="button" onclick="editHelpPage.saveData()" value="提交"/>&nbsp;&nbsp;&nbsp;&nbsp;<input type="reset" value="重置"/></td>
	   		</tr>
	    </table>
    </form>
</body>
</html>

