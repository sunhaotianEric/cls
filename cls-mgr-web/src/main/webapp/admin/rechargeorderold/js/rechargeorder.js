function RechargeorderPage(){}
var rechargeorderPage = new RechargeorderPage();
rechargeorderPage.param = {
		nowSerialNumber:null,
		refreshTime: 5,
		refreshKey:null

};

/**
 * 查询参数
 */
rechargeorderPage.queryParam = {
		bizSystem : null,
		rechargeStatus : null,
		userName : null,
		serialNumber : null,
		createdDateStart : null,
		applyValue : null,
		refType:null,
		createdDateStart : null,
		createdDateEnd : null,
		fromType:null
};

//分页参数
rechargeorderPage.pageParam = {
		    pageNo : 1,
		    pageSize : defaultPageSize
};

$(document).ready(function() {
	$("#createdDateStart").val(getClsStartTimeZero().format("yyyy-MM-dd hh:mm:ss"));
	$("#createdDateEnd").val(getClsEndTimeZero().format("yyyy-MM-dd hh:mm:ss"));
	$("#RechargeSoundControl").val(parent.RechargeSoundControl);
	rechargeorderPage.initTableData(); //根据条件,查找资金明细
	parent.rechargeOrderTable=rechargeorderPage.table;
	
	$("#refreshTime").val(parent.refreshRechargeKeyTime);
	
	$("#refreshTime").change(function(){
		var value=parseInt($(this).val());
		parent.refreshRechargeKeyTime=value;
		parent.resetRechargeOrderInterval();
	})
	
	$("#RechargeSoundControl").on("change",function(){
		parent.RechargeSoundControl=$("#RechargeSoundControl").val();
	});
});

RechargeorderPage.prototype.intervalLoad = function(){
	interVal = setInterval("parent.refreshrechargeOrderKey()",parseInt(rechargeorderPage.param.refreshTime)*1000);  
}

RechargeorderPage.prototype.initTableData = function(){

	rechargeorderPage.queryParam = getFormObj($("#rechargeorderForm"));
	rechargeorderPage.table = $('#rechargeorderTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"ajax":function (data, callback, settings) {
			rechargeorderPage.pageParam.pageSize = data.length;//页面显示记录条数
			rechargeorderPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerRechargeWithDrawOrderAction.getAllRechargeOrdersByQuery(rechargeorderPage.queryParam,rechargeorderPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败.");
				}
				callback(returnData);
			});
		},
		"columns": [
                    {"data": "bizSystemName", 
	                 "bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "serialNumber"},
		            {"data": "userName",render: function(data, type, row, meta) {	
	            		return "<a href='javascript:void(0)' style='color:blue;text-decoration:underline;'  onclick='commonPage.showUserMsgPage(this,"+row.userId+")'>"+ row.userName +"</a>";
            		}},
		            {"data": "applyValue",render: function(data, type, row, meta) {
		            	var str=data.toFixed(2);
	                		 return str ;
	                	 }},
	                {"data": "accountValue",render: function(data, type, row, meta) {
	 		            	var str=data.toFixed(2);
	 	                		 return str ;
	 	                  }},
		            {"data": "createdDateStr"},
		            {"data": "statusStr",render: function(data, type, row, meta) {
 		            	var value=row.dealStatus;
 		            	if(value=="PAYMENT")
 		            		{
 		            		return "<font color='blue'>"+data+"</font>" ;
 		            		}
 		            	else if(value=="PAYMENT_SUCCESS")
 		            		{
 		            		return "<font color='green'>"+data+"</font>" ;
 		            		}
 		            	else if(value=="PAYMENT_CLOSE")
 		            		{
 		            		return "<font color='red'>"+data+"</font>" ;
 		            		}
                		 
                  }}, 
		            /*{"data": "id",
		            	render: function(data, type, row, meta) {
	                		var str="<input name='rechargePresentInput' class='ipt' onkeyup='checkNum(this)' id='rechargePresentInput"+row.id+"' type='text' value='0'>";
	                		 return str ;
	                	 }
		            },*/
		            
		            {
	                	 "data": "id",
	                	 render: function(data, type, row, meta) {
	                		 var str="";
	                		 /*if((row.payType == 'BANK_TRANSFER' || (row.payType == 'WEIXIN' && row.thirdPayType==null) || (row.payType == 'ALIPAY' && row.thirdPayType==null)) && row.dealStatus != 'PAYMENT_SUCCESS' && row.dealStatus != 'PAYMENT_CLOSE'){
	                			str += "<button type='button' name='setRechargerOrdersPaySuccessButton' onclick='rechargeorderPage.setRechargerOrdersPaySuccess("+row.id+")' class='btn btn-primary'>确认到账</button>&nbsp;&nbsp;";
                			 }else{
                				str += "<span style=''></span>";
                			 }
	                			
                			 if((row.payType == 'BANK_TRANSFER' || (row.payType == 'WEIXIN' && row.thirdPayType==null) || (row.payType == 'ALIPAY' && row.thirdPayType==null)) && row.dealStatus == 'PAYMENT'){
                				 str += "<button type='button'  name='setRechargerOrdersPayCloseButton' onclick='rechargeorderPage.setRechargerOrdersPayClose("+row.id+")' class='btn btn-danger'>关闭订单</button>";
                			 }else{
                				 str += "<span style=''></span>";
                			 }*/
                			 if(row.dealStatus == 'PAYMENT'){
	                			    if(row.refType=='TRANSFER'){
	                			    	str += "<button type='button' name='setRechargerOrdersPaySuccessButton' onclick='rechargeorderPage.setRechargerOrdersPaySuccess("+row.id+", "+ row.applyValue +")' class='btn btn-sm btn-primary'>确认到账</button>&nbsp;&nbsp;";
	                			    }else{
	                			    	str += "<button type='button' name='setRechargerOrdersPaySuccessButton' onclick='rechargeorderPage.setRechargerOrdersPaySuccess("+row.id+", "+ row.applyValue +")' class='btn btn-sm btn-primary'>强制入账</button>&nbsp;&nbsp;";
	                			    }
		                			
		                			str += "<button type='button'  name='setRechargerOrdersPayCloseButton' onclick='rechargeorderPage.setRechargerOrdersPayClose("+row.id+")' class='btn btn-sm btn-danger'>关闭订单</button>";
	                		  }else if(row.dealStatus == 'PAYMENT_CLOSE'){
	                				str += "<button type='button' name='setRechargerOrdersPaySuccessButton' onclick='rechargeorderPage.setRechargerOrdersPaySuccess("+row.id+", "+ row.applyValue +")' class='btn btn-sm btn-primary'>确认到账</button>&nbsp;&nbsp;";
	                		  }else{
	                			  	str += "<button type='button' onclick='rechargeorderPage.setUserBettingDemand("+row.id+", "+ row.accountValue +")' class='btn btn-sm badge-warning'>打码设定</button>&nbsp;&nbsp;"
	                		  }
                			 /* if(row.dealStatus == 'PAYMENT'){
                				 str += "<button type='button'  name='setRechargerOrdersPayCloseButton' onclick='rechargeorderPage.setRechargerOrdersPayClose("+row.id+")' class='btn btn-danger'>关闭订单</button>";
                			 }else{
                				 str += "<span style=''></span>";
                			 }	 */
	                		 return str ;
	                	 }
	                },
	                {"data": "operateDes"},
		            {"data": "refTypeStr"},
		            {"data": "fromType",
		            	  render: function(data, type, row, meta) {
		            			var fromType = "";
		            			if("PC" == row.fromType) {
		            				fromType = "电脑";
		            			} else if("MOBILE" == row.fromType) {
		            				fromType = "手机web";
		            			} else if("APP" == row.fromType) {
		            				fromType = "APP";
		            			}
		            			return fromType;
		            	  }
		            },
		            {"data": "postscript"},
		            {"data": "bankUserName"},
		            {"data": "updateAdmin"},
		            {"data": "updateDateStr"}
	            ]
	}).api(); 
};


/**
 * 查询数据
 */
RechargeorderPage.prototype.findRechargeorder = function(dateRange){
	if(dateRange != undefined && dateRange != null && dateRange != ''){
		commonPage.setDateRangeTimeZero(dateRange, "createdDateStart", "createdDateEnd",2);	
	}
	var rechargeStatus = $("#rechargeStatus").val();
	var serialNumber = $("#serialNumber").val();
	var userName = $("#userName").val();
	var bizSystem = $("#bizSystem").val();
	var createdDateStart = $("#createdDateStart").val();
	var createdDateEnd = $("#createdDateEnd").val();
	var applyValue = $("#applyValue").val();
	var refType = $("#refType").val();
	var fromType = $("#fromType").val();
	if(userName == ""){
		rechargeorderPage.queryParam.userName = null;
	}else{
		rechargeorderPage.queryParam.userName = userName;
	}
	if(rechargeStatus == ""){
		rechargeorderPage.queryParam.statuss = null;
	}else{
		rechargeorderPage.queryParam.statuss = rechargeStatus;
	}
	if(serialNumber == ""){
		rechargeorderPage.queryParam.serialNumber = null;
	}else{
		rechargeorderPage.queryParam.serialNumber = serialNumber;
	}
	if(bizSystem == ""){
		rechargeorderPage.queryParam.bizSystem = null;
	}else{
		rechargeorderPage.queryParam.bizSystem = bizSystem;
	}
	if(createdDateStart == ""){
		rechargeorderPage.queryParam.createdDateStart = null;
	}else{
		rechargeorderPage.queryParam.createdDateStart =new Date(createdDateStart);
	}
	if(createdDateEnd == ""){
		rechargeorderPage.queryParam.createdDateEnd = null;
	}else{
		rechargeorderPage.queryParam.createdDateEnd =new Date(createdDateEnd);
	}
	if(applyValue == ""){
		rechargeorderPage.queryParam.applyValue = null;
	}else{
		rechargeorderPage.queryParam.applyValue = applyValue;
	}
	if(refType == ""){
		rechargeorderPage.queryParam.refType = null;
	}else{
		rechargeorderPage.queryParam.refType = refType;
	}
	if(fromType == ""){
		rechargeorderPage.queryParam.fromType = null;
	}else{
		rechargeorderPage.queryParam.fromType = fromType;
	}
	rechargeorderPage.table.ajax.reload();
};










/**
 * 订单确认到账 rechargeOrderId,value
 */
RechargeorderPage.prototype.setRechargerOrdersPaySuccess = function(rechargeOrderId, applyValue){
	 // 客服管理员用户不能操作
	 if(currentUser.state == 3){
		 swal("客服管理员不能操作此功能！");
		 return;
	 }
	 var operStr =  "<div><span>充值赠送</span><input type='checkbox' onclick='showRechargePresentInput(this)' style='display: inline-block;width: 30px;vertical-align:middle;'>"
		 			+ "<span>打码要求</span><input type='checkbox' onclick='showBettingDemandInput(this)' style='display: inline-block;width: 30px;vertical-align:middle;'></div>"
		 			+ "<div id='rechargePresentDiv' style='display:none'><span>充值赠送金额</span>&nbsp;" +
		 					"<select name='presentScale' id='presentScale' data-val='"+ applyValue +"' onchange='changeRechargePresentSel(this)'>" +
		 							"<option value=''>手动填写</option>" +
		 							"<option value='0.005'>0.5%</option>" +
									"<option value='0.01'>1%</option>" +
									"<option value='0.02'>2%</option>" +
									"<option value='0.03'>3%</option>" +
									"<option value='0.04'>4%</option>" +
									"<option value='0.05'>5%</option>" +
									"<option value='0.06'>6%</option>" +
									"<option value='0.07'>7%</option>" +
									"<option value='0.08'>8%</option>" +
									"<option value='0.09'>9%</option>" +
									"<option value='0.1'>10%</option>" +
								"</select>"+
		 					"<input type='text' id='rechargePresent' style='display:inline-block;width: 200px;margin-left:10px;'></div>"
	  					+"<div id='bettingDemandDiv' style='display:none'><span>打码要求金额</span>&nbsp;" +
		  					"<select name='bettingDemandScale' id='bettingDemandScale' data-val='"+ applyValue +"' onchange='changeBettingDemandSel(this)'>" +
								"<option value=''>手动填写</option>" +
								"<option value='1'>1倍</option>" +
								"<option value='2'>2倍</option>" +
								"<option value='3'>3倍</option>" +
								"<option value='4'>4倍</option>" +
							"</select>"+
	  						"<input type='text' id='bettingDemand' style='display:inline-block;width: 200px;margin-left:10px;'></div>";
    if(rechargeOrderId != null){
    	swal({ 
	        title: "是否确认到帐", 
	        text: operStr, 
	        type: "warning", 
	        html : true,
	        showCancelButton: true, 
	        closeOnConfirm: false, 
	        cancelButtonText: "取消", 
	        confirmButtonText: "确定", 
	        confirmButtonColor: "#ec6c62",
	        inputPlaceholder: "充值赠送"
	    }, 
	    function(isConfirm){   
	    	if (isConfirm) { 
	    		var rechargePresent = 0;
	    		var bettingDemand = 0;
	    		if($("#rechargePresent").val() != "") {
	    			rechargePresent = $("#rechargePresent").val();
	    		}
	    		if($("#bettingDemand").val() != "") {
	    			bettingDemand = $("#bettingDemand").val();
	    		}
	    	    managerRechargeWithDrawOrderAction.setRechargerOrdersPaySuccess(rechargeOrderId, rechargePresent, bettingDemand,function(r){
    	    		if (r[0] != null && r[0] == "ok") {
    	    			// 刷新点击的内容
    	    			swal({ title: "提示", text: "订单确认到账成功",type: "success"});
    	    			rechargeorderPage.table.ajax.reload();
    	    		}else if(r[0] != null && r[0] == "error"){
    	    			swal({ title: "提示", text: r[1],type: "error"});
    	    			rechargeorderPage.table.ajax.reload();
    	    		}else{
    	    			swal({ title: "提示", text: "确认到账该订单请求失败.",type: "error"});
    	    		}
    	        });
	    	  } 
	    });
	    
    }else{
    	swal("该订单参数未找到!");
    }
	
};

/**
 * 充值赠送是否显示
 * @param ele
 * @returns
 */
function showRechargePresentInput(ele){
	if($(ele).is(":checked")) {
		$("#rechargePresentDiv").show();
	} else {
		$("#rechargePresentDiv").hide();
	}
}

/**
 * 改变充值赠送比例
 * @param ele
 * @returns
 */
function changeRechargePresentSel(ele){
	if($(ele).val() != "") {
		var money = $(ele).attr("data-val");
		var presentMoney = (money * $(ele).val()).toFixed(2);
		$("#rechargePresent").val(presentMoney);
	} else {
		$("#rechargePresent").val("");
	}
}

/**
 * 打码要求是否显示
 * @param ele
 * @returns
 */
function showBettingDemandInput(ele){
	if($(ele).is(":checked")) {
		$("#bettingDemandDiv").show();
	} else {
		$("#bettingDemandDiv").hide();
	}
}

/**
 * 改变打码要求倍数
 * @param ele
 * @returns
 */
function changeBettingDemandSel(ele){
	if($(ele).val() != "") {
		var money = $(ele).attr("data-val");
		var bettingDemandMoney = (money * $(ele).val()).toFixed(2);
		$("#bettingDemand").val(bettingDemandMoney);
	} else {
		$("#bettingDemand").val("");
	}
}


/**
 * 订单取消
 */
RechargeorderPage.prototype.setRechargerOrdersPayClose = function(rechargeOrderId){
	 //客服管理员用户不能操作
	 if(currentUser.state == 3){
		 swal("客服管理员不能操作此功能！");
		 return;
	 }
	if(rechargeOrderId != null){
		swal({ 
			title: "是否确认到帐", 
	        title: "订单取消", 
	        text: "是否关闭订单?", 
	        type: "warning", 
	        showCancelButton: true, 
	        closeOnConfirm: false,
	        cancelButtonText: "取消", 
	        confirmButtonText: "确认", 
	        confirmButtonColor: "#ec6c62" 
	    }, function() { 
	    	managerRechargeWithDrawOrderAction.setRechargerOrdersPayClose(rechargeOrderId,function(r){
	    		if (r[0] != null && r[0] == "ok") {
	    			//刷新点击的内容
	    			swal({ title: "提示", text: "订单取消成功",type: "success"});
	    			rechargeorderPage.table.ajax.reload();
	    		}else if(r[0] != null && r[0] == "error"){
	    			swal({ title: "提示", text: r[1],type: "error"});
	    			rechargeorderPage.table.ajax.reload();
	    		}else{
	    			swal({ title: "提示", text: "取消该充值订单请求失败",type: "error"});
	    		}
	        });
	    });
	   
    }else{
    	swal("该订单参数未找到!");
    }
	
};

/**
 * 设定用户打码要求
 */
RechargeorderPage.prototype.setUserBettingDemand = function(rechargeOrderId, money){
	 // 客服管理员用户不能操作
	 if(currentUser.state == 3){
		 swal("客服管理员不能操作此功能！");
		 return;
	 }
	 var operStr =  "<div><span>打码要求金额</span><input type='text' id='bettingDemand2' value='"+ money +"' style='display:inline-block;width: 200px;margin-left:10px;'></div>";
	if(rechargeOrderId != null){
		swal({ 
		 title: "添加打码要求", 
		 text: operStr, 
		 html : true,
		 showCancelButton: true, 
		 closeOnConfirm: false, 
		 cancelButtonText: "取消", 
		 confirmButtonText: "确定", 
		 confirmButtonColor: "#ec6c62"
		}, 
		function(isConfirm){   
			if (isConfirm) { 
				var bettingDemand = 0;
				if($("#bettingDemand2").val() != "") {
					bettingDemand = $("#bettingDemand2").val();
				}
			    managerRechargeWithDrawOrderAction.setUserBettingDemand(rechargeOrderId, bettingDemand,function(r){
		 		if (r[0] != null && r[0] == "ok") {
		 			// 刷新点击的内容
		 			swal({ title: "提示", text: "添加打码要求成功",type: "success"});
		 		}else if(r[0] != null && r[0] == "error"){
		 			swal({ title: "提示", text: r[1],type: "error"});
		 		}else{
		 			swal({ title: "提示", text: "添加打码要求失败.",type: "error"});
		 		}
		     });
			  } 
		});
		
		}else{
		swal("该订单参数未找到!");
		}
}



/**
 * 重新绑定按钮点击事件
 */
RechargeorderPage.prototype.reBindClickEvent = function() {
	//确认到账
	$("button[name='setRechargerOrdersPaySuccessButton']").unbind("click").click(function(){
	    var orderId = $(this).parents("dl").find("input[name='rechargeOrderId']").val();
	    var presentValue = $(this).parents("dl").find("input[name='rechargePresentInput']").val();
	    rechargeorderPage.param.operatePayId = orderId;
	    if(presentValue == null || presentValue == ""){
	    	presentValue = 0;
	    }
	    
	    if(rechargeorderPage.param.operatePayId != null){
		    if(confirm("是否确认到账?")){
		    	rechargeorderPage.setRechargerOrdersPaySuccess(rechargeorderPage.param.operatePayId,presentValue);
			}
	    }else{
           alert("该订单参数未找到");
	    }
	});
	//订单取消
	$("button[name='setRechargerOrdersPayCloseButton']").unbind("click").click(function(){
	    var orderId = $(this).parents("dl").find("input[name='rechargeOrderId']").val();
	    rechargeorderPage.param.operatePayId = orderId;
	    if(rechargeorderPage.param.operatePayId != null){
		    if(confirm("是否确认取消订单?")){
		    	rechargeorderPage.setRechargerOrdersPayClose(rechargeorderPage.param.operatePayId);
			}
	    }else{
           alert("该订单参数未找到");
	    }
	});
}

/**
 * 导出功能
 */
RechargeorderPage.prototype.rechargeorderInfoExport = function(){
	rechargeorderPage.queryParam = getFormObj($("#rechargeorderForm"));
	rechargeorderPage.queryParam.operateType="RECHARGE";
	managerRechargeWithDrawOrderAction.rechargeWithDrawInfoExport(rechargeorderPage.queryParam,function(r){
		dwr.engine.openInDownload(r);
    });
}
