function DayProfitBizSystemPage(){};

var dayProfitBizSystemPage = new DayProfitBizSystemPage();

dayProfitBizSystemPage.param ={
	bizSystem : null,
	gainModel : null,
	belongDateStart : null,
	belongDateEnd : null
}

//分页参数
dayProfitBizSystemPage.pageParam = {
    pageNo : 1,
    pageSize : defaultPageSize
};

$(document).ready(function(){
//	var newtimems = new Date().getTime()-(24*60*60*1000);
//	var yesd = new Date(newtimems).format("yyyy-MM-dd");
//	$("#startime").val(yesd);
//	$("#endtime").val(yesd);
	
	var curHour = new Date().getHours(); 
	if(curHour<3){
		var newtimems = new Date().getTime()-(24*60*60*1000);
		var yesd = new Date(newtimems).format("yyyy-MM-dd");
		$("#startime").val(yesd);
		$("#endtime").val(yesd);
		laydate({
			elem: '#startime',
			istime: false,
			format: 'YYYY-MM-DD',
			max: laydate.now(-1)});
		laydate({
			elem: '#endtime',
			istime: false,
			format: 'YYYY-MM-DD',
			max: laydate.now(-1)});
	}else{
		var newtimems = new Date().getTime();
		var yesd = new Date(newtimems).format("yyyy-MM-dd");
		$("#startime").val(yesd);
		$("#endtime").val(yesd);
		laydate({
			elem: '#startime',
			istime: false,
			format: 'YYYY-MM-DD',
			max: laydate.now()});
		laydate({
			elem: '#endtime',
			istime: false,
			format: 'YYYY-MM-DD',
			max: laydate.now()});
	}
	
	
	dayProfitBizSystemPage.initdayProfit();
});

DayProfitBizSystemPage.prototype.initdayProfit = function(){
	
	dayProfitBizSystemPage.param= getFormObj($("#dayProfitQuery"));
	dayProfitBizSystemPage.table =  $('#dayProfitTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"ajax":function (data, callback, settings) {
			dayProfitBizSystemPage.pageParam.pageSize = data.length;//页面显示记录条数
			dayProfitBizSystemPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
			managerDayProfitAction.getDayProfiyPandect(dayProfitBizSystemPage.param,dayProfitBizSystemPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				}; 
				
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
					returnData.recordsTotal = 0;
					returnData.recordsFiltered = 0;
					returnData.data = null;
				}else{
					swal("查询数据失败.");
					returnData.recordsTotal = 0;
					returnData.recordsFiltered = 0;
					returnData.data = null;
				}
				callback(returnData);
			});
		},
	"columns": [
	            {"data":"bizSystemName"},
	            {"data":"gain",
	            	render:function(data, type, row, meta){
	            		return data.toFixed(commonPage.param.fixVaue);
	            	}
	            },
	            {"data":"winGain",
	            	render: function(data, type, row, meta) {
	            		if(data != null){
	            			data = data *100;
	            			return data.toFixed(commonPage.param.fixVaue)  +"%";
	            		}
	            		return "";
	            	}
	            },
	            {"data":"lotteryGain",
	            	render:function(data, type, row, meta){
	            		return data.toFixed(commonPage.param.fixVaue);
	            	}
	            },
	            {"data":"moneyGain",
	            	render:function(data, type, row, meta){
	            		return data.toFixed(commonPage.param.fixVaue);
	            	}
	            },
	            {"data":"lottery",
	            	render: function(data, type, row, meta) {
	            		return (data - row.lotteryRegression - row.lotteryStopAfterNumber).toFixed(commonPage.param.fixVaue)
	            	}
	            },
	            {"data":"win",
	            	render:function(data, type, row, meta){
	            		return data.toFixed(commonPage.param.fixVaue);
	            	}
	            },
	            {"data":"rebate",
	            	render:function(data, type, row, meta){
	            		return (data + row.percentage).toFixed(commonPage.param.fixVaue);
	            	}
	            },
	            {"data":"activitiesMoney",
	            	render: function(data, type, row, meta){
	            		return (data + row.rechargePresent + row.systemAddMoney - row.systemReduceMoney).toFixed(commonPage.param.fixVaue);
	            	}
	            },
	            {"data":"halfMonthBonus",
	            	render:function(data, type, row, meta){
	            		return data.toFixed(commonPage.param.fixVaue);
	            	}
	            },
	            {"data":"daySalary",
	            	render:function(data, type, row, meta){
	            		return data.toFixed(commonPage.param.fixVaue);
	            	}
	            },
	            {"data":"recharge",
	            	render:function(data, type, row, meta){
	            		return data.toFixed(commonPage.param.fixVaue);
	            	}
	            },
	            {"data":"withdraw",
	            	render:function(data, type, row, meta){
	            		return data.toFixed(commonPage.param.fixVaue);
	            	}
	            },
	            
	            ]
	}).api();
}

/**
 * 条件查找活动
 */
DayProfitBizSystemPage.prototype.findDayProfit = function(){
//	dayProfitBizSystemPage.param.bizSystem = $("#bizSystem").val();
	dayProfitBizSystemPage.param = getFormObj($("#dayProfitQuery"));
	dayProfitBizSystemPage.table.ajax.reload();
}

/**
 * 按日期查询数据
 */
DayProfitBizSystemPage.prototype.getDayProfit = function(dateRange){
	if(dateRange != undefined && dateRange != null && dateRange != ''){
		commonPage.setDateRangeTime(dateRange, "startime", "endtime",1);	
	}
	var bizSystem = $("#bizSystem").val();
	var startDate = $("#startime").val()
	var endDate = $("#endtime").val()
	
//	if(currentUser.bizSystem == "SUPER_SYSTEM"){
//		if(bizSystem == "" || bizSystem == null){
//			swal("请选择子系统!");
//			return;
//		}
//	}
	
	if(currentUser.bizSystem != "SUPER_SYSTEM"){
		bizSystem = currentUser.bizSystem;
	}
//	if(typeof(bizSystem) == "undefined"){
//		bizSystem = currentUser.bizSystem;
//	}
	
	if(startDate == "" || startDate == null){
		swal("请选择开始时间!");
		return;
	}
	
	if(endDate == "" || endDate == null){
		swal("请选择结束时间!");
		return;
	}
	if(bizSystem == null || bizSystem == ""){
		dayProfitBizSystemPage.param.bizSystem = null;
	}else{
		dayProfitBizSystemPage.param.bizSystem = bizSystem;
	}
	
	dayProfitBizSystemPage.param.belongDateStart = new Date(startDate);
	dayProfitBizSystemPage.param.belongDateEnd = new Date(endDate);
	dayProfitBizSystemPage.table.ajax.reload();
};
