function DayProfitPage(){}
var dayProfitPage = new DayProfitPage();

dayProfitPage.queryParam = {
		bizSystem : null,
		belongDateStart : null,
		belongDateEnd : null
};


$(document).ready(function() {
	
	var curHour = new Date().getHours(); 
	if(curHour<3){
		var newtimems = new Date().getTime()-(24*60*60*1000);
		var yesd = new Date(newtimems).format("yyyy-MM-dd");
		$("#startime").val(yesd);
		$("#endtime").val(yesd);
		laydate({
			elem: '#startime',
			istime: false,
			format: 'YYYY-MM-DD',
			max: laydate.now(-1)});
		laydate({
			elem: '#endtime',
			istime: false,
			format: 'YYYY-MM-DD',
			max: laydate.now(-1)});
	}else{
		var newtimems = new Date().getTime();
		var yesd = new Date(newtimems).format("yyyy-MM-dd");
		$("#startime").val(yesd);
		$("#endtime").val(yesd);
		laydate({
			elem: '#startime',
			istime: false,
			format: 'YYYY-MM-DD',
			max: laydate.now()});
		laydate({
			elem: '#endtime',
			istime: false,
			format: 'YYYY-MM-DD',
			max: laydate.now()});
	}
	
//	dayProfitPage.getDayProfit(); //查询所有的
	
});

/**
 * 按日期查询数据
 */
DayProfitPage.prototype.getDayProfit = function(dateRange){
	if(dateRange != undefined && dateRange != null && dateRange != ''){
		commonPage.setDateRangeTime(dateRange, "startime", "endtime",1);	
	}
	var bizSystem = $("#bizSystem").val();
	var startDate = $("#startime").val()
	var endDate = $("#endtime").val()
	
//	if(currentUser.bizSystem == "SUPER_SYSTEM"){
//		if(bizSystem == "" || bizSystem == null){
//			swal("请选择子系统!");
//			return;
//		}
//	}
	
	if(currentUser.bizSystem != "SUPER_SYSTEM"){
		bizSystem = currentUser.bizSystem;
	}
//	if(typeof(bizSystem) == "undefined"){
//		bizSystem = currentUser.bizSystem;
//	}
	
	if(startDate == "" || startDate == null){
		swal("请选择开始时间!");
		return;
	}
	
	if(endDate == "" || endDate == null){
		swal("请选择结束时间!");
		return;
	}
	if(bizSystem == null || bizSystem == ""){
		dayProfitPage.queryParam.bizSystem = null;
	}else{
		dayProfitPage.queryParam.bizSystem = bizSystem;
	}
	
	dayProfitPage.queryParam.belongDateStart = new Date(startDate);
	dayProfitPage.queryParam.belongDateEnd = new Date(endDate);
	
	dayProfitPage.queryAllDayProFits(dayProfitPage.queryParam);
};



/**
 * 查询平台盈利
 */
DayProfitPage.prototype.queryAllDayProFits = function(queryParam){
	managerDayProfitAction.getAllDayProfit(queryParam,function(r){
		if (r[0] != null && r[0] == "ok") {
			dayProfitPage.refreshDayProfitPages(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("查询平台盈利数据失败.");
		}
    });
};

/**
 * 刷新列表数据
 */
DayProfitPage.prototype.refreshDayProfitPages = function(dayProfit){
	//----------------转账充值--------------
/*	var totalRechage = dayProfit.rechargeBankTransfer+dayProfit.rechargeQuickPay+dayProfit.rechargeAlipay+dayProfit.rechargeWeixin;
	var totalRechargeUserCount = dayProfit.rechargeBankTransferUserCount + dayProfit.rechargeQuickPayUserCount + dayProfit.rechargeAlipayUserCount + dayProfit.rechargeWeixinUserCount;
	var totalRechargeCount = dayProfit.rechargeBankTransferUserCount + dayProfit.rechargeQuickPayUserCount + dayProfit.rechargeAlipayUserCount + dayProfit.rechargeWeixinUserCount;
	$("#totalRechageId").html(totalRechage);
	$("#totalRechargeCountId").html(totalRechargeCount);
	$("#totalRechargeUserCountId").html(totalRechargeUserCount);*/
	
	$("#rechargeId").html(dayProfit.recharge.toFixed(commonPage.param.fixVaue));
	$("#rechargeCountId").html(dayProfit.rechargeCount);
	$("#rechargeUserCountId").html(dayProfit.rechargeUserCount);
	
	$("#rechargeBankTransferId").html(dayProfit.rechargeBankTransfer.toFixed(commonPage.param.fixVaue));
	$("#rechargeBankTransferUserCountId").html(dayProfit.rechargeBankTransferUserCount);
	$("#rechargeQuickPayId").html(dayProfit.rechargeQuickPay.toFixed(commonPage.param.fixVaue));
	$("#rechargeQuickPayUserCountId").html(dayProfit.rechargeQuickPayUserCount);
	$("#alipayRechargeId").html(dayProfit.rechargeAlipay.toFixed(commonPage.param.fixVaue));
	$("#rechargeAlipayUserCountId").html(dayProfit.rechargeAlipayUserCount);
	$("#weixinRechargeId").html(dayProfit.rechargeWeixin.toFixed(commonPage.param.fixVaue));
	$("#rechargeWeixinUserCountId").html(dayProfit.rechargeWeixinUserCount);
	
	$("#rechargeQqpayId").html(dayProfit.rechargeQqpay.toFixed(commonPage.param.fixVaue));
	$("#rechargeQqpayUserCountId").html(dayProfit.rechargeQqpayUserCount);
	
	$("#otherRechargeId").html((dayProfit.rechargeJdpay + dayProfit.rechargeUnionpay).toFixed(commonPage.param.fixVaue));
	$("#otherRechargeCountId").html(dayProfit.rechargeJdpayUserCount + dayProfit.rechargeUnionpayUserCount);
	 
	$("#rechargeManualId").html(dayProfit.rechargeManual.toFixed(commonPage.param.fixVaue));
	$("#rechargeManualUserCountId").html(dayProfit.rechargeManualUserCount);
	
	
	//--------取款------------
	var actualWithdraw = dayProfit.withdraw;
	$("#actualWithdrawId").html(actualWithdraw.toFixed(commonPage.param.fixVaue));
	$("#withdrawId").html(dayProfit.withdraw.toFixed(commonPage.param.fixVaue));
	$("#withdrawCountId").html(dayProfit.withdrawCount);
	$("#withdrawUserCountId").html(dayProfit.withdrawUserCount);
	
	$("#withdrawFeeId").html(dayProfit.withdrawFee.toFixed(commonPage.param.fixVaue));
	$("#withdrawFeeUserCountId").html(dayProfit.withdrawFeeUserCount);
	
	//----------投注回退--------	
/*	
	var totalLotteryNumCount = dayProfit.lotteryNumCount-dayProfit.lotteryRegressionUserCount-dayProfit.lotteryStopAfterNumberUserCount;
	var totalLotteryUserCount = dayProfit.lotteryUserCount - dayProfit.lotteryRegressionUserCount - dayProfit.lotteryStopAfterNumberUserCount;
	$("#totalLotteryNumCountId").html(totalLotteryNumCount);
	$("#totalLotteryUserCountId").html(totalLotteryUserCount);*/
	var totalLottery = dayProfit.lottery - dayProfit.lotteryRegression - dayProfit.lotteryStopAfterNumber;
	$("#totalLotteryId").html(totalLottery.toFixed(commonPage.param.fixVaue));
	
	$("#lotteryId").html(dayProfit.lottery.toFixed(commonPage.param.fixVaue));
	$("#lotteryNumCountId").html(dayProfit.lotteryNumCount);
	$("#lotteryUserCountId").html(dayProfit.lotteryUserCount);
	
	$("#lotteryRegressionId").html(dayProfit.lotteryRegression.toFixed(commonPage.param.fixVaue));
	$("#lotteryRegressionUserCountId").html(dayProfit.lotteryRegressionUserCount);
	
	$("#lotteryStopAfterNumberId").html(dayProfit.lotteryStopAfterNumber.toFixed(commonPage.param.fixVaue));
	$("#lotteryStopAfterNumberUserCountId").html(dayProfit.lotteryStopAfterNumberUserCount);
	
	//---------中奖、返点------------	 
	var totalPercentage = dayProfit.percentage + dayProfit.rebate;
	var totalRebateUserCount = dayProfit.percentageUserCount + dayProfit.rebateUserCount;
	$("#totalPercentage").html(totalPercentage.toFixed(commonPage.param.fixVaue));
	$("#totalRebateUserCount").html(totalRebateUserCount);
	
	$("#winId").html(dayProfit.win.toFixed(commonPage.param.fixVaue));
	$("#winUserCountId").html(dayProfit.winUserCount);
	$("#percentageId").html(dayProfit.percentage.toFixed(commonPage.param.fixVaue));
	$("#percentageUserCountId").html(dayProfit.percentageUserCount);
	$("#rebateId").html(dayProfit.rebate.toFixed(commonPage.param.fixVaue));
	$("#rebateUserCountId").html(dayProfit.rebateUserCount);
	
	//---------福利待遇-------------------
//	$("#dayBonusId").html(dayProfit.dayBonus);
	$("#halfMonthBonusId").html(dayProfit.halfMonthBonus.toFixed(commonPage.param.fixVaue));
	$("#halfMonthBonusUserCountId").html(dayProfit.halfMonthBonusUserCount);
	
//	$("#monthSalaryId").html(dayProfit.monthSalary);
//	$("#dayCommission").html(dayProfit.dayCommission);
	$("#daySalaryId").html(dayProfit.daySalary.toFixed(commonPage.param.fixVaue));
	$("#daySalaryUserCountId").html(dayProfit.daySalaryUserCount);
	
	//--------------其它----------------------
//	$("#dayActivityId").html(dayProfit.dayActivity);
	var otherMoney = dayProfit.activitiesMoney + dayProfit.rechargePresent + dayProfit.systemAddMoney;
	$("#otherMoneyId").html(otherMoney.toFixed(commonPage.param.fixVaue));
	$("#activitiesMoneyId").html(dayProfit.activitiesMoney.toFixed(commonPage.param.fixVaue));
	$("#activitiesMoneyUserCountId").html(dayProfit.activitiesMoneyUserCount);
	$("#rechargePresentId").html(dayProfit.rechargePresent.toFixed(commonPage.param.fixVaue))
	$("#rechargePresentUserCountId").html(dayProfit.rechargePresentUserCount);
	$("#systemAddMoneyId").html(dayProfit.systemAddMoney.toFixed(commonPage.param.fixVaue));
	$("#systemAddMoneyUserCountId").html(dayProfit.systemAddMoneyUserCount);
	
	$("#systemReduceMoneyId").html(dayProfit.systemReduceMoney.toFixed(commonPage.param.fixVaue))
	$("#systemReduceMoneyUserCountId").html(dayProfit.systemReduceMoneyUserCount);
	$("#manageReduceMoneyId").html(dayProfit.manageReduceMoney.toFixed(commonPage.param.fixVaue))
	$("#manageReduceMoneyUserCountId").html(dayProfit.manageReduceMoneyUserCount);
	//----------------上下级转账---------
	$("#transferId").html(dayProfit.transfer.toFixed(commonPage.param.fixVaue));
	$("#regularmembersMoneyId").html(dayProfit.regularmembersMoney.toFixed(commonPage.param.fixVaue));
	$("#agencyMoneyId").html(dayProfit.agencyMoney.toFixed(commonPage.param.fixVaue));
	
	//-----------新注册会员------------
	$("#newaddUserCountId").html(dayProfit.newaddUserCount);
	$("#firstRechargeUserCountId").html(dayProfit.firstRechargeUserCount)
	
	//-------盈利----------
	/*	var profitPercent = (totalLottery - dayProfit.win-totalPercentage)/totalLottery*100;
	$("#profitPercentId").html(profitPercent.toFixed(2));*/
	
	
	$("#lotteryGainId").html(dayProfit.lotteryGain.toFixed(commonPage.param.fixVaue));
	$("#moneyGainId").html(dayProfit.moneyGain.toFixed(commonPage.param.fixVaue));
	$("#winGainId").html((dayProfit.winGain*100).toFixed(commonPage.param.fixVaue));
	$("#gainId").html(dayProfit.gain.toFixed(commonPage.param.fixVaue));
	
	//-----------归属时间--------------
	$("#belongDateId").html(dayProfit.belongDate);
/*	
	var profit = totalLottery-dayProfit.win-totalPercentage-otherMoney-dayProfit.daySalary-dayProfit.halfMonthBonus;
	var lotteryProfit = totalLottery-dayProfit.win-totalPercentage;
	var capitalProfit = totalRechage - actualWithdraw;
	$("#profitId").html(profit);
	$("#lotteryProfitId").html(lotteryProfit);
	$("#capitalProfitId").html(capitalProfit);
	
	*/
	
	             
}
