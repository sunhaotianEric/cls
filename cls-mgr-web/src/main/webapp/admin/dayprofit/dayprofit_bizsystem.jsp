<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>盈亏报表总览</title>
<jsp:include page="/admin/include/include.jsp"></jsp:include>

</head>
<body>
<div class="animated fadeIn">
     <div class="row"> 
        <div class="col-sm-12">
            <div class="ibox ">
                <div class="ibox-content">
                    <div class="row m-b-sm m-t-sm">
                        <form class="form-horizontal" id="dayProfitQuery">
                      
                           <div class="row">
                              <div class="col-sm-3">
                                    <div class="input-group m-b">
                                        <span class="input-group-addon">商户</span>
                                        <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
                                     </div>
                               </div>
							   <div class="col-sm-2">
							        <div class="input-group m-b">
							            <span class="input-group-addon">排序</span>
							            <div class="input-daterange input-group" id="datepicker">
							                <select class="ipt form-control" name="gainModel">
							                	<option value="1">盈利</option>
							                	<option value="2">赢率</option>
							                	<option value="3">投注盈利</option>
							                	<option value="4">资金盈利</option>
							                </select>
							            </div>
							        </div>
							   </div>
	                           <div class="col-sm-4">
							        <div class="input-group m-b">
							            <span class="input-group-addon">时间始终</span>
							            <div class="input-daterange input-group" id="datepicker">
							                <input class="form-control layer-date" placeholder="开始日期" onclick="" name="belongDateStart" id="startime">
							                <span class="input-group-addon">到</span>
							                <input class="form-control layer-date" placeholder="结束日期" onclick="" name="belongDateEnd" id="endtime"></div>
							        </div>
							    </div>
							  	<div class="col-sm-2">
							        <div class="form-group nomargin text-left">
							            <button type="button" class="btn btn-xs btn-info" onclick="dayProfitBizSystemPage.getDayProfit(1)">今 天</button>
							            <button type="button" class="btn btn-xs btn-danger" onclick="dayProfitBizSystemPage.getDayProfit(-1)">昨 天</button>
							        	<button type="button" class="btn btn-xs btn-warning" onclick="dayProfitBizSystemPage.getDayProfit(-7)">最近7天</button><br/>
							        	<button type="button" class="btn btn-xs btn-success" onclick="dayProfitBizSystemPage.getDayProfit(30)">本 月</button>
							        	<button type="button" class="btn btn-xs btn-primary" onclick="dayProfitBizSystemPage.getDayProfit(-30)">上 月</button>
							        </div>
							    </div>
							    <div class="col-sm-1">
							        <div class="form-group nomargin text-right">
							            <button type="button" class="btn btn-outline btn-default" onclick="dayProfitBizSystemPage.findDayProfit()">
							                <i class="fa fa-search"></i>&nbsp;查 询</button>
							        </div>
							    </div>
                            </div>
 
                        </form>
                    </div>
                    <div class="jqGrid_wrapper">
                        <div class="ui-jqgrid ">
                            <div class="ui-jqgrid-view">
                                <div class="ui-jqgrid-hdiv">
                                    <div class="ui-jqgrid-hbox" style="padding-right:0">
                                        <table class="ui-jqgrid-htable ui-common-table table table-bordered" id="dayProfitTable">
                                            <thead>
                                                <tr class="ui-jqgrid-labels">
										          <th class="ui-th-column ui-th-ltr">商户</th>
										          <th class="ui-th-column ui-th-ltr">盈利</th>
										          <th class="ui-th-column ui-th-ltr">赢率</th>
										          <th class="ui-th-column ui-th-ltr">投注盈利</th>
										          <th class="ui-th-column ui-th-ltr">资金盈利</th>
										          <th class="ui-th-column ui-th-ltr">投注(-撤单)</th>
										          <th class="ui-th-column ui-th-ltr">中奖</th>
										          <th class="ui-th-column ui-th-ltr">返点</th>
										          <th class="ui-th-column ui-th-ltr">其他</th> 
										          <th class="ui-th-column ui-th-ltr">契约分红</th>
										          <th class="ui-th-column ui-th-ltr">日工资</th>
										          <th class="ui-th-column ui-th-ltr">充值</th>
										          <th class="ui-th-column ui-th-ltr">取款</th>
										          
										      <!--    <th class="ui-th-column ui-th-ltr">活动彩金</th>
										          <th class="ui-th-column ui-th-ltr">商户续费</th>
										          <th class="ui-th-column ui-th-ltr">商户扣费</th>
										          <th class="ui-th-column ui-th-ltr">取款手续费</th>
										          <th class="ui-th-column ui-th-ltr">支出转账</th>
										          <th class="ui-th-column ui-th-ltr">收入转账</th>
										          <th class="ui-th-column ui-th-ltr">月工资</th>-->
										          <!-- <th class="ui-th-column ui-th-ltr">日佣金</th>
										          <th class="ui-th-column ui-th-ltr">广告费(佣金+工资+分红)</th> -->
                                                </tr>
                                            </thead>
                                            <tbody>
                                 
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>          
                        </div>
                    </div>
                </div>
            </div>
        </div>
     </div> 
</div>
 <!-- js -->
 <script type="text/javascript" src="<%=path%>/admin/dayprofit/js/dayprofit_bizsystem.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
 <!-- dwr -->
 <script type='text/javascript' src='<%=path%>/dwr/interface/managerDayProfitAction.js'></script>
</body>
</html>

