<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
    String ip = request.getParameter("param1");
%>
<html>
<head>
    <meta charset="utf-8" />
    <title>平台盈亏利统计</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
     <link href="css/animate.min.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" rel="stylesheet">
	<style type="text/css">
	 .form-group{ margin: 6px 0; } 
	 .ibox-content{ overflow: hidden; padding: 10px 20px;}
	 .col-sm-3{padding: 0 8px;}
	 .col-sm-3 .ibox-content{height: 143px}
	 .gs{margin-left:15px}
	 .h240 .ibox-content{height:240px;}
	 .mb15{margin-bottom:15px;}
	 .dashboard-header{padding-bottom:10px;}
	 .form-group .dot{display:inline-block;width:8px;height:8px;background:#1AB394;border-radius:50%;margin-right:5px;margin-left:2px;margin-bottom:1px;}
	 .form-group .dot.blue{background:#54A6F7}
	 .form-group .dot.yellow{background:#F5D36C}
	 .form-group .dot.red{background:#ED5565}
	 .form-group .dot.gray{background:#8C98A0}
	</style>
</head>
<body class="gray-bg">
<c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
	<div class="row white-bg dashboard-header">
	      <div class="col-sm-3 biz" >
	          <div class="input-group">
	              <span class="input-group-addon">商户：</span>
	               <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
	          </div>
	      </div>
	    <div class="col-sm-4">
	        <div class="input-group">
	            <span class="input-group-addon">时间始终</span>
	            <div class="input-daterange input-group" id="datepicker">
	                <input class="form-control layer-date" placeholder="开始日期" onclick="" id="startime">
	                <span class="input-group-addon">到</span>
	                <input class="form-control layer-date" placeholder="结束日期" onclick="" id="endtime"></div>
	        </div>
	    </div>
	    <div class="col-sm-4">
	        <div class="form-group nomargin text-left">
	            <button type="button" class="btn btn-xs btn-info" onclick="dayProfitPage.getDayProfit(1)">今 天</button>
	            <button type="button" class="btn btn-xs btn-danger" onclick="dayProfitPage.getDayProfit(-1)">昨 天</button>
	        	<button type="button" class="btn btn-xs btn-warning" onclick="dayProfitPage.getDayProfit(-7)">最近7天</button><br/>
	        	<button type="button" class="btn btn-xs btn-success" onclick="dayProfitPage.getDayProfit(30)">本 月</button>
	        	<button type="button" class="btn btn-xs btn-primary" onclick="dayProfitPage.getDayProfit(-30)">上 月</button>
	        </div>
	    </div>
	    <div class="col-sm-1">
	        <div class="form-group nomargin text-right">
	            <button type="button" class="btn btn-outline btn-default" onclick="dayProfitPage.getDayProfit()">
	                <i class="fa fa-search"></i>&nbsp;查 询</button>
	        </div>
	    </div>
	</div>
	
</c:if>
<c:if test="${admin.bizSystem != 'SUPER_SYSTEM'}">
	<div class="row white-bg dashboard-header">
	    <div class="col-sm-4">
	        <div class="input-group">
	            <span class="input-group-addon">时间始终</span>
	            <div class="input-daterange input-group" id="datepicker">
	                <input class="form-control layer-date" placeholder="开始日期" onclick="laydate({istime: false, format: 'YYYY-MM-DD',max: laydate.now(-1)})" id="startime">
	                <span class="input-group-addon">到</span>
	                <input class="form-control layer-date" placeholder="结束日期" onclick="laydate({istime: false, format: 'YYYY-MM-DD',max: laydate.now(-1)})" id="endtime"></div>
	        </div>
	    </div>
	   <div class="col-sm-6">
	        <div class="form-group nomargin text-left">
	            <button type="button" class="btn btn-xs btn-info" onclick="dayProfitPage.getDayProfit(1)">今 天</button>
	            <button type="button" class="btn btn-xs btn-danger" onclick="dayProfitPage.getDayProfit(-1)">昨 天</button>
	        	<button type="button" class="btn btn-xs btn-warning" onclick="dayProfitPage.getDayProfit(-7)">最近7天</button><br/>
	        	<button type="button" class="btn btn-xs btn-success" onclick="dayProfitPage.getDayProfit(30)">本 月</button>
	        	<button type="button" class="btn btn-xs btn-primary" onclick="dayProfitPage.getDayProfit(-30)">上 月</button>
	        </div>
	    </div>
	    <div class="col-sm-2">
	        <div class="form-group nomargin text-right">
	            <button type="button" class="btn btn-outline btn-default" onclick="dayProfitPage.getDayProfit()">
	                <i class="fa fa-search"></i>&nbsp;查 询</button>
	        </div>
	    </div>
	</div>
</c:if>
<div class="row white-bg border-bottom" style="padding-left: 20px;">
	<div class="col-sm-12">
		<p style="color: red">温馨提醒：盈亏报表计算周期是每天的03:00-03:00</p>
	</div>
</div>
	
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row h240">
        <div class="col-sm-3">
            <div class="ibox mb15 float-e-margins">
                <div class="ibox-content">
                    <div class="form-group">
                        <strong>充值总额：</strong><span class="text-warning"><span id="rechargeId">0.00</span>元 / <span id="rechargeUserCountId">0</span>人</span></div>
                    <div class="form-group"><i class="dot blue"></i>网银转账：<span class="text-warning"><span id ="rechargeBankTransferId">0.00</span>元 / <span id ="rechargeBankTransferUserCountId">0</span>人</span></div>
                    <div class="form-group"><i class="dot"></i>快捷支付：<span class="text-warning"><span id="rechargeQuickPayId">0.00</span>元 / <span id="rechargeQuickPayUserCountId">0</span>人</span></div>
                    <div class="form-group"><i class="dot yellow"></i>支付宝：<span class="text-warning"><span id="alipayRechargeId">0.00</span>元 / <span id="rechargeAlipayUserCountId">0</span>人</span></div>
                    <div class="form-group"><i class="dot red"></i>QQ钱包：<span class="text-warning"><span id="rechargeQqpayId">0.00</span>元 / <span id="rechargeQqpayUserCountId">0</span>人</span></div>
                    <div class="form-group"><i class="dot gray"></i>微信：<span class="text-warning"><span id="weixinRechargeId">0.00</span>元 / <span id="rechargeWeixinUserCountId">0</span>人</span></div>
                    <div class="form-group"><i class="dot"></i>京东,银联：<span class="text-warning"><span id="otherRechargeId">0.00</span>元 / <span id="otherRechargeCountId">0</span>人</span></div>
                    <div class="form-group"><i class="dot blue"></i>人工存款：<span class="text-warning"><span id="rechargeManualId">0.00</span>元 / <span id="rechargeManualUserCountId">0</span>人</span></span></div>
                    <div class="form-group">充值笔数：<span class="text-warning" id="rechargeCountId">0笔</span></div></div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="ibox mb15 float-e-margins">
                <div class="ibox-content">
                    <div class="form-group"><strong>实际取款：</strong><span class="text-warning"><span id="actualWithdrawId">0.00</span>元 / <span id="withdrawUserCountId">0</span>人</span></div>
                    <div class="form-group"><strong>取款手续费：</strong><span class="text-warning"><span id="withdrawFeeId">0.00</span>元 / <span id="withdrawFeeUserCountId">0</span>人</span></div>
                    <div class="form-group">取款笔数：<span class="text-warning" id="withdrawCountId">0笔</span></div></div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="ibox mb15 float-e-margins">
                <div class="ibox-content">
                    <div class="form-group">
                    <strong>其它：</strong><span class="text-warning"><span id="otherMoneyId">0.00</span>元</span></div>
                    <div class="form-group"><i class="dot yellow"></i>充值赠送：<span class="text-warning"><span id="rechargePresentId">0.00</span>元 / <span id="rechargePresentUserCountId">0</span>人</span></div>
                    <div class="form-group"><i class="dot gray"></i>活动彩金：<span class="text-warning"><span id="activitiesMoneyId">0.00</span>元 / <span id="activitiesMoneyUserCountId">0</span>人</span></div>
                    <div class="form-group"><i class="dot gray"></i>人工存入：<span class="text-warning"><span id="systemAddMoneyId">0.00</span>元 / <span id="systemAddMoneyUserCountId">0</span>人</span></div>
                    <div class="form-group"><strong>行政提出：</strong><span class="text-warning"><span id="manageReduceMoneyId">0.00</span>元/<span id="manageReduceMoneyUserCountId">0</span>人</span></div>
                    <div class="form-group"><strong>误存提出：</strong><span class="text-warning"><span id="systemReduceMoneyId">0.00</span>元 /<span id="systemReduceMoneyUserCountId">0</span>人</span></div>
               </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="ibox mb15 float-e-margins">
                <div class="ibox-content">
                    <div class="form-group">
                        <strong>直属日工资：</strong><span class="text-warning"><span id="daySalaryId">0.00</span>元 / <span id="daySalaryUserCountId">0</span>人</span></div>
                    <div class="form-group">
                        <strong>契约分红：</strong><span class="text-warning" ><span id="halfMonthBonusId">0.00</span>元 / <span id="halfMonthBonusUserCountId">0</span>人</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <div class="ibox mb15 float-e-margins">
                <div class="ibox-content">
                    <div class="form-group">
                        <strong>投注总额：</strong><span class="text-warning"><span id="totalLotteryId">0.00</span>元  <span></span></span></div>
                    <div class="form-group"><i class="dot blue"></i>用户投注：<span class="text-warning"><span id="lotteryId">0.00</span>元  / <span id="lotteryUserCountId">0</span>人</span></div>
                    <div class="form-group"><i class="dot  yellow"></i>用户撤单：<span class="text-warning"><span id="lotteryRegressionId">0.00</span>元  / <span id="lotteryRegressionUserCountId">0</span>人</span></div>
                    <div class="form-group"><i class="dot"></i>追号回退：<span class="text-warning"><span id="lotteryStopAfterNumberId">0.00</span>元  / <span id="lotteryStopAfterNumberUserCountId">0</span>人</span></div>
                    <div class="form-group">投注单量：<span class="text-warning" id="lotteryNumCountId">0</span></div></div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="ibox mb15 float-e-margins">
                <div class="ibox-content">
                    <div class="form-group">
                        <strong>中奖总额：</strong><span class="text-warning"><span id="winId">0.00</span>元 / <span id="winUserCountId">0</span>人</span></div>
                    <div class="form-group">
                        <strong>返点总额：</strong><span class="text-warning"><span id="totalPercentage">0.00</span> 元 / <span id="totalRebateUserCount">0</span>人</span></div>
                    <div class="form-group"><i class="dot"></i>投注返点：<span class="text-warning"><span id="rebateId">0.00</span> 元 / <span id="rebateUserCountId">0</span>人</span></div>
                    <div class="form-group"><i class="dot  yellow"></i>推广返点：<span class="text-warning"><span id="percentageId">0.00</span> 元 / <span id="percentageUserCountId">0</span>人</span></div></div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="ibox mb15 float-e-margins">
                <div class="ibox-content">
                    <div class="form-group"><strong>上下级转账：</strong><span class="text-warning"><span id="transferId">0.00</span>元</span></div>
                    <div class="form-group"><strong>普通会员余额：</strong><span class="text-warning"><span id="regularmembersMoneyId">0.00</span>元 </span></div>
                    <div class="form-group"><strong>代理余额：</strong><span class="text-warning"><span id="agencyMoneyId">0.00</span>元 </span></div></div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="ibox mb15 float-e-margins">
                <div class="ibox-content">
                    <div class="form-group"><strong>新增会员：</strong><span class="text-warning"><span id="newaddUserCountId">0</span>人</span></div>
                    <div class="form-group"><strong>首充会员：</strong><span class="text-warning"><span id="firstRechargeUserCountId">0</span>人</span></div>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox mb15 float-e-margins">
                <div class="ibox-content">
                    <div class="form-group"><strong>盈利：</strong><span style="color:red;"id="gainId">0.00</span><span style="color:red;">元</span><span class="gs">（计算公式：投注+取款手续费+行政提出－中奖－返点－其它－日工资－分红）</span></div>
                    <div class="form-group"><strong>赢率：</strong><span style="color:red;" id="winGainId">0.00</span><span style="color:red;">%</span><span class="gs">（计算公式：（投注－中奖）／投注）</span></div>
                	<div class="form-group"><strong>投注盈利：</strong><span style="color:red;" id="lotteryGainId">0.00</span><span style="color:red;">元</span ><span class="gs">（计算公式：投注－中奖）</span></div>
                	<div class="form-group"><strong>资金盈利：</strong><span style="color:red;" id="moneyGainId">0.00</span><span style="color:red;">元</span><span class="gs">（计算公式：充值－取款）</span></div>
                </div>
            </div>
        </div>
    </div>
</div>
 <!-- js -->
 <script type="text/javascript" src="<%=path%>/admin/dayprofit/js/dayprofit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
 <!-- dwr -->
 <script type='text/javascript' src='<%=path%>/dwr/interface/managerDayProfitAction.js'></script>
</body>
</html>

