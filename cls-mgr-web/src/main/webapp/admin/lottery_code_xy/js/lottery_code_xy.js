function LotteryCodeXyPage(){}
var lotteryCodeXyPage = new LotteryCodeXyPage();

/**
 * 查询参数
 */
lotteryCodeXyPage.queryParam = {
	lotteryName : null,
	lotteryNum : null,
	bizSystem : null,
	startime : null,
	endtime : null
};

function HandUpdatePage(){}
var handUpdatePage = new HandUpdatePage();
handUpdatePage.param = {
};
/**
 * 加载彩种下拉
 */
HandUpdatePage.prototype.getAllTypes = function(){
	managerOrderAction.getPrivateLotteryTypes(function(r){
		if (r[0] != null && r[0] == "ok") {
			handUpdatePage.refreshTypes(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("查询彩种类型失败.");
		}
	});
};
/**
 * 展示彩种下拉框
 */
HandUpdatePage.prototype.refreshTypes = function(detailTypeMaps){
	var obj3=document.getElementById('lotteryTypeRegression');
	obj3.options.add(new Option("=请选择=",""));
	for(var key in detailTypeMaps){
		obj3.options.add(new Option(detailTypeMaps[key],key));
	}

};


// 分页参数
lotteryCodeXyPage.pageParam = {
    pageNo : 1,
    pageSize : defaultPageSize
};

$(document).ready(function() {
	lotteryCodeXyPage.initTableData();
	handUpdatePage.getAllTypes();
});

/**
 * 初始化列表数据
 */
LotteryCodeXyPage.prototype.initTableData = function() {
	lotteryCodeXyPage.queryParam = getFormObj($("#lotteryCodeForm"));
	lotteryCodeXyPage.table = $('#lotteryCodeTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"ajax":function (data, callback, settings) {
			lotteryCodeXyPage.pageParam.pageSize = data.length;// 页面显示记录条数
			lotteryCodeXyPage.pageParam.pageNo = (data.start / data.length)+1;// 当前页码
			managerLotteryCodeXyAction.getAllLotteryCodeXy(lotteryCodeXyPage.queryParam,lotteryCodeXyPage.pageParam,function(r){
				// 封装返回数据
				var returnData = {
						recordsTotal : 0,
						recordsFiltered : 0,
						data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;// 返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;// 后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;// 返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询奖金期号数据失败.");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": "bizSystemName","bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "lotteryTypeDes"},
		            {"data": "lotteryNum"},
		            {"data": "opencodeStr"},
		            {"data": "kjtimeStr"},
		            {"data": "addtimeStr"},
		            {
		            	"data": "prostateDeal",
		            	render: function(data, type, row, meta) {
		            		if (data == "1") {
		            			return '是';
		            		} else {
		            			return '否';
		            		}
		            		return data;
	            		}
		            },
		            {
	                	 "data": "id",
	                	 render: function(data, type, row, meta) {
	                		 return '<a class="btn btn-danger btn-sm btn-del" onclick="lotteryCodeXyPage.deleteLotteryCodeXyById('+data+')"><i class="fa fa-trash"></i>&nbsp;删除</a>';
	                	 }
	                }
	            ]
	}).api(); 
}

/**
 * 条件查询奖金期号
 */
LotteryCodeXyPage.prototype.queryConditionLotteryCodes = function(){
	lotteryCodeXyPage.queryParam = getFormObj($("#lotteryCodeForm"));
	lotteryCodeXyPage.table.ajax.reload();
};

/**
 * 同步条件查询奖金期号
 */
LotteryCodeXyPage.prototype.synQueryConditionLotteryCodes = function(){
	lotteryCodeXyPage.queryParam = getFormObj($("#lotteryCodeForm"));
	if(lotteryCodeXyPage.queryParam.lotteryName==null){
		swal({
	           title: "请选择彩种",
	           text: "否则无法继续操作！",
	           type: "warning",
	           showCancelButton: true,
	           confirmButtonColor: "#DD6B55",
	           cancelButtonText: "取消",
	           confirmButtonText: "OK",
	           closeOnConfirm: true
	           
	       },
	       function() {
	       });
	}
	if(lotteryCodeXyPage.queryParam.lotteryName!=null){
		managerLotteryCodeXyAction.resetRedisLotteryCodeXy(lotteryCodeXyPage.queryParam,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal("同步奖金期号数据到redis成功.");
			}else if(r[0] != null && r[0] == "error"){
				var list = r[1];
				var info = " ";
				for(var i=0;i<list.length;i++){
					info = info + list[i] + " ";
				}
				swal(info+"同步奖金期号数据到redis失败.");
			}else{
				swal("同步奖金期号数据到redis失败.");
			}
		});
	}
};


/**
 * 删除对应的开奖数据
 */
LotteryCodeXyPage.prototype.deleteLotteryCodeXyById = function(lotteryCodeId){
	   swal({
           title: "您确定要删除这条信息吗",
           text: "删除后将无法恢复，请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "删除",
           closeOnConfirm: false
       },
       function() {
		managerLotteryCodeXyAction.deleteLotteryCodeXyById(lotteryCodeId,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "删除成功",type: "success"});
				lotteryCodeXyPage.queryConditionLotteryCodes();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("查询奖金期号数据失败.");
			}
	    });
       });
};


