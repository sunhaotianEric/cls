<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
    
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>彩票期号管理</title>
        <link rel="shortcut icon" href="favicon.ico">
        
        <jsp:include page="/admin/include/include.jsp"></jsp:include>
    </head>
    
    <body>
        <div class="animated fadeIn">
            <div class="ibox-content">
                <div class="row m-b-sm m-t-sm">
                    <form class="form-horizontal" id="lotteryCodeForm">


                        <div class="row">
                            <c:if test="${admin.bizSystem == 'SUPER_SYSTEM'}">
	                        <div class="col-sm-2 biz" >
	                              <div class="input-group m-b">
	                                <span class="input-group-addon">商户：</span>
	                                  <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
	                             </div>
	                        </div>
                            </c:if>
                        	<div class="col-sm-2">
                              <div class="input-group m-b">
                                  <span class="input-group-addon">选择彩种</span>
                                <%--   <cls:enmuSel name="lotteryName" id="lotteryName" options="class:ipt form-control"  className="com.team.lottery.enums.ELotteryKind"/> --%>
                                  <select id='lotteryTypeRegression' class="ipt form-control" name="lotteryName">
                                  </select>
                                  <%-- <select class="ipt form-control" name="lotteryName">
                                      <option value="" selected="selected">==请选择==</option>
                                      <!-- <option value="XYEB">幸运28</option> -->
                                      <option value="JYKS">幸运快3</option>
                                      <option value="JLFFC">幸运分分彩</option>
                                      <option value="JSPK10">极速PK10</option>
                                      <option value="XYLHC">幸运六合彩</option>
                                      <option value="SFSSC">三分时时彩</option>
                                      <option value="WFSSC">五分时时彩</option>
                                      <option value="LCQSSC">老重庆时时彩</option>
                                      <option value="JSSSC">江苏时时彩</option>
                                      <option value="BJSSC">北京时时彩</option>
                                      <option value="GDSSC">广东时时彩</option>
                                      <option value="SCSSC">四川时时彩</option>
                                      <option value="SHSSC">上海时时彩</option>
                                      <option value="SDSSC">山东时时彩</option>
                                      <option value="SFKS">三分快三</option>
                                      <option value="WFKS">五分快三</option>
                                      <option value="SFSYXW">三分11选5</option>
                                      <option value="WFSYXW">五分11选5</option>
                                      <option value="SHFSSC">十分时时彩</option>
                                      <option value="SFPK10">三分pk10</option>
                                      <option value="WFPK10">五分pk10</option>
                                      <option value="SHFPK10">十分pk10</option>
                                   </select>--%>
                               </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">期号</span>
                                    <input type="text" value="" class="form-control" name="lotteryNum"></div>
                            </div>
                            <div class="col-sm-4">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">日期</span>
                                    <div class="input-daterange input-group" id="datepicker">
                                        <input class="form-control layer-date" placeholder="开始日期" name="startime" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})">
                                        <span class="input-group-addon">到</span>
                                        <input class="form-control layer-date" placeholder="结束日期" name="endtime" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"></div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group text-right">
                                    <button type="button" class="btn btn-outline btn-default btn-js" onclick="lotteryCodeXyPage.queryConditionLotteryCodes()"><i class="fa fa-search"></i>&nbsp;查 询</button>
                                	<button type="button" class="btn btn-outline btn-default btn-js" onclick="lotteryCodeXyPage.synQueryConditionLotteryCodes()"><i class="fa fa-arrows"></i>&nbsp;同步</button>
                                </div>
                            </div>
                         </div>
                    </form>
                    </div>
                    <div class="jqGrid_wrapper">
                        <div class="ui-jqgrid ">
                            <div class="ui-jqgrid-view">
                                <div class="ui-jqgrid-hdiv">
                                    <div class="ui-jqgrid-hbox" style="padding-right:0">
                                        <table id="lotteryCodeTable" class="ui-jqgrid-htable ui-common-table table table-bordered">
                                            <thead>
                                                <tr class="ui-jqgrid-labels">
                                                  	<th class="ui-th-column ui-th-ltr">商户</th>
                                                    <th class="ui-th-column ui-th-ltr">彩种</th>
                                                    <th class="ui-th-column ui-th-ltr">期号</th>
                                                    <th class="ui-th-column ui-th-ltr">开奖结果</th>
                                                    <th class="ui-th-column ui-th-ltr">开奖时间</th>
                                                    <th class="ui-th-column ui-th-ltr">录入时间</th>
                                                    <th class="ui-th-column ui-th-ltr">是否开奖处理</th>
                                                    <th class="ui-th-column ui-th-ltr">操作</th></tr>
                                            </thead>
                                            <tbody id="lotteryCodeList">
                                                <!-- <tr role="row" tabindex="-1" class="jqgrow ui-row-ltr">
                                                    <td>北京快3</td>
                                                    <td>62838</td>
                                                    <td>3,6,6</td>
                                                    <td>2016-11-23 20:46:56</td>
                                                    <td>2016-11-23 20:46:56</td>
                                                    <td>是</td>
                                                    <td>
                                                        <a class="btn btn-danger btn-sm btn-del">
                                                            <i class="fa fa-trash"></i>删除</a>
                                                    </td>
                                                </tr>
                                                <tr role="row" tabindex="-1" class="jqgrow ui-row-ltr">
                                                    <td>北京快3</td>
                                                    <td>62838</td>
                                                    <td>3,6,6</td>
                                                    <td>2016-11-23 20:46:56</td>
                                                    <td>2016-11-23 20:46:56</td>
                                                    <td>是</td>
                                                    <td>
                                                        <a class="btn btn-danger btn-sm btn-del">
                                                            <i class="fa fa-trash"></i>删除</a>
                                                    </td>
                                                </tr>
                                                <tr role="row" tabindex="-1" class="jqgrow ui-row-ltr">
                                                    <td>北京快3</td>
                                                    <td>62838</td>
                                                    <td>3,6,6</td>
                                                    <td>2016-11-23 20:46:56</td>
                                                    <td>2016-11-23 20:46:56</td>
                                                    <td>是</td>
                                                    <td>
                                                        <a class="btn btn-danger btn-sm btn-del">
                                                            <i class="fa fa-trash"></i>删除</a>
                                                    </td>
                                                </tr> -->
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="row">
                                <div class="col-sm-6">
                                    <div class="dataTables_info" id="editable_info" role="alert" aria-live="polite" aria-relevant="all">当前第1页/共5页，总共100条记录</div></div>
                                <div class="col-sm-6">
                                    <div class="dataTables_paginate paging_simple_numbers" id="editable_paginate">
                                        
                                        <ul class="pagination">
                                        	<li class="paginate_button previous disabled" aria-controls="editable" tabindex="0" id="editable_previous">
                                                <a href="#">首页</a></li>
                                            <li class="paginate_button previous disabled" aria-controls="editable" tabindex="0" id="editable_previous">
                                                <a href="#">上一页</a></li>
                                            <li class="paginate_button active" aria-controls="editable" tabindex="0">
                                                <a href="#">1</a></li>
                                            <li class="paginate_button " aria-controls="editable" tabindex="0">
                                                <a href="#">2</a></li>
                                            <li class="paginate_button " aria-controls="editable" tabindex="0">
                                                <a href="#">3</a></li>
                                            <li class="paginate_button " aria-controls="editable" tabindex="0">
                                                <a href="#">4</a></li>
                                            <li class="paginate_button " aria-controls="editable" tabindex="0">
                                                <a href="#">5</a></li>
                                            <li class="paginate_button " aria-controls="editable" tabindex="0">
                                                <a href="#">6</a></li>
                                            <li class="paginate_button next" aria-controls="editable" tabindex="0" id="editable_next">
                                                <a href="#">下一页</a></li>
                                            <li class="paginate_button next" aria-controls="editable" tabindex="0" id="editable_next">
                                                <a href="#">尾页</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
            
		    <!-- dwr -->
		    <script type='text/javascript' src='<%=path%>/dwr/interface/managerLotteryCodeXyAction.js'></script>
            <!-- js -->
		    <script type="text/javascript" src="<%=path%>/admin/lottery_code_xy/js/lottery_code_xy.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
            <script type='text/javascript' src='<%=path%>/dwr/interface/managerOrderAction.js'></script>
    </body>

</html>
