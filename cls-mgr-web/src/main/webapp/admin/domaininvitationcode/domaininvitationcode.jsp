<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>域名邀请码设置</title>
<jsp:include page="/admin/include/include.jsp"></jsp:include>

</head>
<body>
	<div class="animated fadeIn">
		<div class="ibox-content">
			<div class="row m-b-sm m-t-sm">
				<form class="form-horizontal" id="domaininvitationcodePageForm">
					<div class="row">
						<c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
							<div class="col-sm-3">
								<div class="input-group m-b">
									<span class="input-group-addon">商户</span>
									<cls:bizSel name="bizSystem" id="bizSystem"
										options="class:ipt form-control" />
								</div>

							</div>
						</c:if>


						<c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
							<div class="col-sm-3"></div>
						</c:if>
						<div class="col-sm-9">
							<div class="col-sm-3">
								<div class="input-group m-b">
									<span class="input-group-addon">域名</span> 
									<input id='domainUrl' name="domainUrl" type="text" value="" class="form-control">
								</div>
							</div>
							<div class="col-sm-3">
								<div class="input-group m-b">
									<span class="input-group-addon">邀请码</span>
									 <input id='invitationCode' name="invitationCode" type="text" value=""class="form-control">
								</div>
							</div>
							<div class="col-sm-3">
								<div class="input-group m-b">
									<span class="input-group-addon">是否启用</span> <select
										class="ipt form-control" id="enable">
										<option value="">= = 请选择 = =</option>
										<option value="1">启用</option>
										<option value="0">停用</option>
									</select>
								</div>
							</div>
							<div class="form-group nomargin text-right">
								<button type="button" class="btn btn-outline btn-default btn-js"
									onclick="domaininvitationcodePage.pageParam.pageNo = 1;domaininvitationcodePage.findDomaininvitationcode()">
									<i class="fa fa-search"></i>&nbsp;查 询
								</button>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<button type="button"
									class="btn btn-outline btn-default btn-add"
									onclick="domaininvitationcodePage.addDomaininvitationcode()">
									<i class="fa fa-plus"></i>&nbsp;新增
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="jqGrid_wrapper">
				<div class="ui-jqgrid ">
					<div class="ui-jqgrid-view">
						<div class="ui-jqgrid-hdiv">
							<div class="ui-jqgrid-hbox" style="padding-right: 0">
								<table id="domaininvitationcodePageTable"
									class="ui-jqgrid-htable ui-common-table table table-bordered">
									<thead>
										<tr class="ui-jqgrid-labels">
											<th class="ui-th-column ui-th-ltr">序号</th>
											<th class="ui-th-column ui-th-ltr">商户</th>
											<th class="ui-th-column ui-th-ltr">域名地址</th>
											<!--<th>商户类型</th>预留-->
											<th class="ui-th-column ui-th-ltr">邀请码</th>
											<th class="ui-th-column ui-th-ltr">创建时间</th>
											<th class="ui-th-column ui-th-ltr">启用状态</th>
											<th class="ui-th-column ui-th-ltr">操 作</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<!-- js -->
	<script type="text/javascript"
		src="<%=path%>/admin/domaininvitationcode/js/domaininvitationcode.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
	<!-- dwr -->
	<script type='text/javascript'
		src='<%=path%>/dwr/interface/managerDomainInvitationCodeAction.js'></script>
</body>
</html>

