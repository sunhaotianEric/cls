<%@page import="com.team.lottery.system.SystemConfigConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" 
	import = "com.team.lottery.enums.EBankInfo" 
	pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>域名邀请码设置</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    
    
    <link rel="stylesheet" href="<%=path%>/resources/kindeditor-4.1.10/themes/default/default.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" />
	<script charset="utf-8" src="<%=path%>/resources/kindeditor-4.1.10/kindeditor-min.js"></script>
	<script charset="utf-8" src="<%=path%>/resources/kindeditor-4.1.10/lang/zh_CN.js"></script>
	<%
	 String id = request.getParameter("id");  //获取查询的商品ID
	%>
	
</head>
<body class="gray-bg">
    <div class="wrapper wrapper-content animated fadeIn">
  <form action="javascript:void(0)">
    <div class="row">
    <c:choose>
    <c:when test="${admin.bizSystem=='SUPER_SYSTEM'}">
    <div class="col-sm-12">
    <div class="input-group m-b">
    <span class="input-group-addon">商户：</span>
    <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
    </div>
    </div>
    </c:when>
    <c:otherwise>
    <input type="hidden" id="bizSystem" name="bizSystem" value='${admin.bizSystem}'/>
    </c:otherwise>
    </c:choose>
    <div class="col-sm-12">
    <div class="input-group m-b">
    <span class="input-group-addon">域名地址：</span>
          <!-- <input type="text" value="" class="form-control" name="domainUrl" id="domainUrl"> -->
          <select class="form-control" name="domainUrl" id="domainUrl" ></select>
	   	  <input type="hidden" id="id" name="id"/>
        </div>
    </div>
    <div class="col-sm-12">
    <div class="input-group m-b">
    <span class="input-group-addon">邀请码：</span>
        <input type="text" value="" class="form-control" name="invitationCode" id="invitationCode">
        </div>
    </div>
    <div class="col-sm-12">
    <div class="input-group m-b">
    <span class="input-group-addon">是否启用：</span>
        <select class="form-control" id="enable">
	   				<option value="1">是</option>
	   				<option value="0">否</option>
	   			</select>
        </div>
    </div>
    <div class="col-sm-12">
     <div class="row">
     <div class="col-sm-6 text-center">
     <button type="button" class="btn btn-w-m btn-white" onclick="domaininvitationcodeEditPage.saveData()">提 交</button></div>
      <div class="col-sm-6 text-center">
      <input type="reset" class="btn btn-w-m btn-white" value="重 置"/>
        </div>
       </div>
      </div>
    </div>
    
    
   
      </form>
  </div>
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/domaininvitationcode/js/domaininvitationcode_edit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerDomainInvitationCodeAction.js'></script>
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerBizSystemDomainAction.js'></script>
    <script type="text/javascript">
    domaininvitationcodeEditPage.param.id = <%=id %>;
	</script>
</body>
</html>

