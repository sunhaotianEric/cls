function DomaininvitationcodePage(){}
var domaininvitationcodePage = new DomaininvitationcodePage();

domaininvitationcodePage.param = {
		
};
//分页参数
domaininvitationcodePage.pageParam = {
		    pageNo : 1,
		    pageSize : defaultPageSize
};
/**
 * 查询参数
 */
domaininvitationcodePage.queryParam = {
		
};
$(document).ready(function() {
	domaininvitationcodePage.initTableData();
});



DomaininvitationcodePage.prototype.initTableData = function(){

	domaininvitationcodePage.queryParam = getFormObj($("#domaininvitationcodePageForm"));
	domaininvitationcodePage.table = $('#domaininvitationcodePageTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			domaininvitationcodePage.pageParam.pageSize = data.length;//页面显示记录条数
			domaininvitationcodePage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerDomainInvitationCodeAction.getAllDomainInvitationCode(domaininvitationcodePage.queryParam,domaininvitationcodePage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败.");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": null},
		            {"data": "bizSystemName", 
		                 "bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "domainUrl"},
		            {"data": "invitationCode"},
		            {"data": "createTime",
		            	render: function(data, type, row, meta) {
		            		return dateFormat(data,'yyyy-MM-dd hh:mm:ss');
	            		}
		            },
		            {
		            	"data": "enable",
		            	render: function(data, type, row, meta) {
		            		if (data == "1") {
		            			return '是';
		            		} else {
		            			return "<span style='color: red;'>否</span>";
		            		}
		            		return data;
	            		}
		            },
		            {
	                	 "data": "id",
	                	 render: function(data, type, row, meta) {
	                		 var str="<a class='btn btn-info btn-sm btn-bj' onclick='domaininvitationcodePage.editDomaininvitationcode("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;" +
	             		 		"<a class='btn btn-danger btn-sm btn-del' onclick='domaininvitationcodePage.delDomaininvitationcode("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";
	                		 if(row.enable=="1")
	                			 {
	                			 str="<a class='btn btn-warning btn-sm ' onclick='domaininvitationcodePage.closeDomaininvitationcode("+data+")'><i class='glyphicon glyphicon-remove' ></i>&nbsp;停用 </a>&nbsp;&nbsp;"+str;
	                			 }
	                		 else
	                			 {
	                			 str="<a class='btn btn-sm btn-success' onclick='domaininvitationcodePage.openDomaininvitationcode("+data+")'><i class='glyphicon glyphicon-ok'></i>&nbsp;启用 </a>&nbsp;&nbsp;"+str;
	                			 }
	                			 
	                		 return str ;
	                	 }
	                }
	            ]
	}).api(); 
};
/**
 * 开启设置
 */
DomaininvitationcodePage.prototype.openDomaininvitationcode = function(id){
	
		managerDomainInvitationCodeAction.openDomainInvitationCode(id,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "操作成功",type: "success"});
				domaininvitationcodePage.table.ajax.reload();
			}else if(r[0] != null && r[0] == "error"){
				showErrorDlg(jQuery(document.body), r[1]);
			}else{
				showErrorDlg(jQuery(document.body), "启用失败.");
			}
	    });
	
};

/**
 * 关闭设置
 */
DomaininvitationcodePage.prototype.closeDomaininvitationcode = function(id){

		managerDomainInvitationCodeAction.closeDomainInvitationCode(id,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "操作成功",type: "success"});
				domaininvitationcodePage.table.ajax.reload();
			}else if(r[0] != null && r[0] == "error"){
				showErrorDlg(jQuery(document.body), r[1]);
			}else{
				showErrorDlg(jQuery(document.body), "关闭失败.");
			}
	    });
	
};

/**
 * 查询数据
 */
DomaininvitationcodePage.prototype.findDomaininvitationcode = function(){
	var bizSystem = $("#bizSystem").val();
	var domainUrl = $("#domainUrl").val();
	var invitationCode = $("#invitationCode").val();
	var enable = $("#enable").val();
	
	if(bizSystem==null || bizSystem.trim()==""){
		domaininvitationcodePage.queryParam.bizSystem = null;
	}
	else{
		domaininvitationcodePage.queryParam.bizSystem = bizSystem;
	}
	
	if (domainUrl==null || domainUrl.trim()=="") {
		domaininvitationcodePage.queryParam.domainUrl = null;
	}
	else{
		domaininvitationcodePage.queryParam.domainUrl = domainUrl;
	}
	
	if (invitationCode==null || invitationCode.trim()=="") {
		domaininvitationcodePage.queryParam.invitationCode = null;
	}
	else{
		domaininvitationcodePage.queryParam.invitationCode = invitationCode;
	}
	if (enable==null || enable.trim()=="") {
		domaininvitationcodePage.queryParam.enable = null;
	}
	else{
		domaininvitationcodePage.queryParam.enable = enable;
	}
	
	domaininvitationcodePage.table.ajax.reload();
};




/**
 * 删除
 */
DomaininvitationcodePage.prototype.delDomaininvitationcode = function(id){
	
	   swal({
           title: "您确定要删除这条信息吗",
           text: "请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "删除",
           closeOnConfirm: false
       },
       function() {
    	   managerDomainInvitationCodeAction.delDomainInvitationCode(id,function(r){
   			if (r[0] != null && r[0] == "ok") {
   				swal({ title: "提示", text: "删除成功",type: "success"});
   				domaininvitationcodePage.table.ajax.reload();
   			}else if(r[0] != null && r[0] == "error"){
   				swal(r[1]);
   			}else{
   				swal("删除信息失败.");
   			}
   	    });
       });
};

/**
 * 新增
 */
DomaininvitationcodePage.prototype.addDomaininvitationcode=function(){
	
	 layer.open({
         type: 2,
         title: '域名邀请码设置新增',
         maxmin: false,
         shadeClose: true,
         //点击遮罩关闭层
         area: ['70%', '50%'],
         content: contextPath + "/managerzaizst/domaininvitationcode/domaininvitationcode_edit.html",
         cancel: function(index){ 
      	   domaininvitationcodePage.table.ajax.reload();
      	   }
     });
}
/**
 * 编辑
 */
DomaininvitationcodePage.prototype.editDomaininvitationcode=function(id){
	
	  layer.open({
           type: 2,
           title: '域名邀请码设置编辑',
           maxmin: false,
           shadeClose: true,
           //点击遮罩关闭层
           area: ['70%', '50%'],
           content: contextPath + "/managerzaizst/domaininvitationcode/"+id+"/domaininvitationcode_edit.html",
           cancel: function(index){ 
        	   domaininvitationcodePage.table.ajax.reload();
        	   }
       });
}

DomaininvitationcodePage.prototype.closeLayer=function(index){
	layer.close(index);
	domaininvitationcodePage.table.ajax.reload();
}
