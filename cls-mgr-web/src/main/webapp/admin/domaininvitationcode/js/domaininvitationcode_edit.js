function DomaininvitationcodeEditPage(){}
var domaininvitationcodeEditPage = new DomaininvitationcodeEditPage();

domaininvitationcodeEditPage.param = {
   	submit:true
};

$(document).ready(function() {
	domaininvitationcodeEditPage.initPage();
	var domainId = domaininvitationcodeEditPage.param.id;  //获取查询的ID
	if(domainId != null){
		domaininvitationcodeEditPage.editDomaininvitationcode(domainId);
	}
	
	
	
	$("#bizSystem").change(function(){
		
		var query={}
		query.bizSystem=$(this).val();
		domaininvitationcodeEditPage.selectAllBizSystemDomain(query,"");
	})
	
	$("#invitationCode").change(function(){
		var bizSystem=$("#bizSystem").val();
		var invitationCode=$(this).val();
		managerDomainInvitationCodeAction.getUserRegisterLinkByInvitationCode(invitationCode,bizSystem,function(r){
			if (r[0] != null && r[0] == "ok") {
				if(r[1]==null ||r[1]==""){
					swal("此邀请码不存在,请重新输入!");
					domaininvitationcodeEditPage.param.submit=false;
				}
				else{
					domaininvitationcodeEditPage.param.submit=true;
				}
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("查询数据失败");
				
			}
		})
	})
	
});



/**
 * 保存域名邀请码设置
 */
DomaininvitationcodeEditPage.prototype.saveData = function(){
	var domaininvitationcode = {};
	domaininvitationcode.id = $("#id").val();
	domaininvitationcode.bizSystem = $("#bizSystem").val();
	domaininvitationcode.domainUrl = $("#domainUrl").val();
	domaininvitationcode.invitationCode = $("#invitationCode").val();
	domaininvitationcode.enable = $("#enable").val();
	if(currentUser.bizSystem=='SUPER_SYSTEM'){
		if (domaininvitationcode.bizSystem==null || domaininvitationcode.bizSystem.trim()=="") {
			swal("请选择业务系统!");
			$("#bizSystem").focus();
			return;
		} 
	}
	if (domaininvitationcode.domainUrl==null || domaininvitationcode.domainUrl.trim()=="") {
		swal("请输入域名地址!");
		$("#domainUrl").focus();
		return;
	}
	if (domaininvitationcode.invitationCode==null || domaininvitationcode.invitationCode.trim()=="") {
		swal("请输入邀请码!");
		$("#invitationCode").focus();
		return;
	}
	
	if(domaininvitationcodeEditPage.param.submit==true){
		managerDomainInvitationCodeAction.saveOrUpdateDomainInvitationCode(domaininvitationcode,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({
			           title: "保存成功",
			           text: "您已经保存了这条信息。",
			           type: "success"
			       },
			       function() {
			    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
		               parent.domaininvitationcodePage.closeLayer(index);
			   	    }); 
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("保存数据失败");
				
			}
	    });
	}
	
	
	
};

/**
 * 初始化页面
 */
DomaininvitationcodeEditPage.prototype.initPage = function(id){
	
	$("#enable").val("1");
	if($("#bizSystem").val().trim()!="" && currentUser.bizSystem!='SUPER_SYSTEM' && domaininvitationcodeEditPage.param.id==null)
	{
		var query={}
		query.bizSystem=$("#bizSystem").val();
		domaininvitationcodeEditPage.selectAllBizSystemDomain(query,"");
	}
	
};

DomaininvitationcodeEditPage.prototype.selectAllBizSystemDomain=function(query,domainUrlVal){
	var domainUrl=$("#domainUrl");
	managerBizSystemDomainAction.selectAllBizSystemDomain(query,function(r){
		if (r[0] != null && r[0] == "ok") {
			var domains=r[1];
			var str="";
			for(var i = 0 ; i < domains.length; i++){
				var domain = domains[i];
				str +="<option  value='" +domain.domainUrl+"'>"+domain.domainUrl+"</option>"; 
				domainUrl.append(str);
				str = "";
			}
			if(domainUrl!=null && domainUrl!=""){
				$("#domainUrl").val(domainUrlVal);
			}
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("查询数据失败");
			
		}
    });
}

/**
 * 修改域名邀请码设置
 */
DomaininvitationcodeEditPage.prototype.editDomaininvitationcode = function(id){
	managerDomainInvitationCodeAction.getDomainInvitationCodeById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			var code = r[1];
			$("#id").val(code.id);
			$("#bizSystem").val(code.bizSystem);
			var query={}
			query.bizSystem=code.bizSystem;
			domaininvitationcodeEditPage.selectAllBizSystemDomain(query,code.domainUrl);
			/*$("#domainUrl").val(code.domainUrl);*/
			$("#enable").val(code.enable);
			$("#invitationCode").val(code.invitationCode);
		}else if(r[0] != null && r[0] == "error"){
			showErrorDlg(jQuery(document.body), r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "获取充值配置失败.");
		}
    });
};