<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%@ page
	import="com.team.lottery.util.ConstantUtil,com.team.lottery.vo.Admin"%>
<%
	String path = request.getContextPath();
    Admin admin = (Admin)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_ADMIN);
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>银行卡设置</title>
<jsp:include page="/admin/include/include.jsp"></jsp:include>


</head>
<body class="gray-bg">
	<div class="animated fadeIn">

		<blockquote style="overflow: hidden;">
			<form class="form-horizontal" id="rechargeorderForm">
				<div class="row">
					<c:choose>
						<c:when test="${admin.bizSystem=='SUPER_SYSTEM'}">
							<div class="col-sm-3">
								<div class="input-group m-b">
									<span class="input-group-addon">商户</span>
									<cls:bizSel name="bizSystem" id="bizSystem"
										options="class:ipt form-control" />
								</div>
							</div>
							<div class="col-sm-3">
								<div class="input-group m-b">
									<span class="input-group-addon">银行名称</span>
									<cls:enmuSel name="bankName"
										className="com.team.lottery.enums.EBankInfo"
										options="class:ipt form-control" id="bankName" />
								</div>
							</div>
							<div class="col-sm-3">
								<div class="input-group m-b">
									<span class="input-group-addon">开户姓名</span> <input type="text"
										value="" name="bankUserName" id="bankUserName"
										class="form-control">
								</div>
							</div>
						</c:when>
						<c:otherwise>

							<div class="col-sm-3">
								<div class="input-group m-b">
									<span class="input-group-addon">银行名称</span>
									<cls:enmuSel name="bankName"
										className="com.team.lottery.enums.EBankInfo"
										options="class:ipt form-control" id="bankName" />
								</div>
							</div>
							<div class="col-sm-3">
								<div class="input-group m-b">
									<span class="input-group-addon">开户名称</span> <input type="text"
										value="" name="bankUserName" id="bankUserName"
										class="form-control">
								</div>
							</div>
							<div class="col-sm-3"></div>
						</c:otherwise>
					</c:choose>
					<div class="col-sm-3">
						<div class="form-group  text-right">
							<button type="button" class="btn btn-outline btn-default"
								onclick="payBankPage.findPayBank()">
								<i class="fa fa-search"></i>&nbsp;查 询
							</button>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<button type="button" class="btn btn-outline btn-default"
								onclick="payBankPage.addPaybank()">
								<i class="fa fa-plus"></i>&nbsp;新增
							</button>
						</div>
					</div>
				</div>
			</form>
		</blockquote>
		<div class="row" id="payBanksList"></div>

	</div>

	<!-- js -->
	<script type="text/javascript"
		src="<%=path%>/admin/paybank/js/paybank.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
	<!-- dwr -->
	<script type='text/javascript'
		src='<%=path%>/dwr/interface/managerPayBankAction.js'></script>
</body>
</html>

