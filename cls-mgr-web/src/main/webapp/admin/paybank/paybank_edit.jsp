<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="com.team.lottery.enums.ELotteryKind"%>
	<%@page import="com.team.lottery.system.SystemConfigConstant"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>商户信息管理</title>
<link rel="shortcut icon" href="favicon.ico">
<jsp:include page="/admin/include/include.jsp"></jsp:include>
<link href="<%=path%>/css/plugins/summernote/summernote.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>"
	rel="stylesheet">
<link href="<%=path%>/css/plugins/summernote/summernote-bs3.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>"
	rel="stylesheet">
<link href="<%=path%>/css/plugins/sweetalert/sweetalert.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>"
	rel="stylesheet">
<%
	String id = request.getParameter("id"); //获取查询的商品ID
	
%>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content">
		<div class="row back-change">
			<div class="ibox-content">
				<form class="form-horizontal" id="bizsysteminfoForm">
					<c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
						<div class="form-group">
							<label class="col-sm-3 control-label">商户：</label>
							<div class="col-sm-9">
								<cls:bizSel name="bizSystem" id="bizSystem"
									options="class:ipt form-control" />
							</div>
						</div>
					</c:if>
					<div class="form-group">
						<label class="col-sm-3 control-label">银行名称：</label>
						<div class="col-sm-9">
							<input type="hidden" id="paybankId" /> <input type="hidden"
								id="adminId" /> <input type="hidden" id="adminName" /> <input
								type="hidden" id="yetUse" /> <input type="hidden"
								id="createTime" />
							<cls:enmuSel name="bankName"
								className="com.team.lottery.enums.EBankInfo"
								options="class:ipt form-control" id="bankName" />
						</div>
					</div>



					<div class="form-group">
						<label class="col-sm-3 control-label">开户支行名称：</label>
						<div class="col-sm-9">
							<input type="text" value="" class="form-control"
								name="subBranchName" id="subBranchName">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">银行卡号：</label>
						<div class="col-sm-9">
							<input id="bankId" name="bankId" type="text" value=""
								class="form-control">
						</div>
					</div>
					<!-- <div class="form-group">
						<label class="col-sm-3 control-label">卡号别名：</label>
						<div class="col-sm-9">
							<input id="aliasName" name="aliasName" type="text" value=""
								class="form-control">
						</div>
					</div> -->
					<div class="form-group">
						<label class="col-sm-3 control-label">姓名：</label>
						<div class="col-sm-9">
							<input id="bankUserName" name="bankUserName" type="text" value=""
								class="form-control">
						</div>
					</div>
					<!-- <div class="form-group">
						<label class="col-sm-3 control-label">充值最低金额：</label>
						<div class="col-sm-9">
							<input id="lowestValue" name="lowestValue" type="text" value=""
								class="form-control">
						</div>
					</div> -->
					<!-- <div class="form-group">
						<label class="col-sm-3 control-label">充值最高金额：</label>
						<div class="col-sm-9">
							<input id="highestValue" name="highestValue" type="text" value=""
								class="form-control">
						</div>
					</div> -->
					<div class="form-group" id="bankUrlDiv">
						<label class="col-sm-3 control-label">银行网址：</label>
						<div class="col-sm-9">
							<input id="bankUrl" name="bankUrl" type="text" value=""
								class="form-control">
						</div>
					</div>
					<!-- <div class="form-group">
						<label class="col-sm-3 control-label">银行LOGO：</label>
						<div class="col-sm-9">
							<input id="bankLogo" name="bankLogo" type="text" value=""
								class="form-control">
						</div>
					</div> -->
					<div class="form-group twoCode" id="file-pretty" style="display: none;">
						<label class="col-sm-3 control-label">二维码图片：</label>
						<div class="col-sm-7">
						<input type="hidden" id="imagePath" size="50px"/>
							 <input type="file" id="imageFile"
								class="form-control" readonly="readonly" accept=".jpg,.png" />
						</div>
						<div class="col-sm-2 text-center">
							<button class="btn btn-success " type="button"
								onclick="payBankEditPage.uploadImgFile('image')">
								<i class="fa fa-upload"></i>&nbsp;&nbsp; <span class="bold">上传</span>
							</button>
							<span id="imageInfo"></span>
						</div>
					</div>
                    <div class="form-group twoCode" style="display: none;">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-9">
                                        <img id="image" alt="二维码图片" src="" style="width:100%;display: none;margin-top: 5px" />
                                    </div>
                                </div>
					<!-- <div class="form-group">
						<label class="col-sm-3 control-label">是否启用：</label>
						<div class="col-sm-9">
							<select class="form-control" id="isDisabled">
								<option value="1">启用</option>
								<option value="0">停用</option>
							</select>
						</div>
					</div> -->
					<div class="form-group">
						<div class="row">
							<div class="col-sm-6 text-center">
								<button type="button" class="btn btn-w-m btn-white"
									onclick="payBankEditPage.savePayBank()">提 交</button>
							</div>
							<div class="col-sm-6 text-center">
								<!-- <button type="button" class="btn btn-w-m btn-white">重 置</button> -->
								<input type="reset" class="btn btn-w-m btn-white" value="重 置" />
							</div>
						</div>
					</div>
				</form>
			</div>

		</div>
	</div>
	<!-- js -->
	<script src="<%=path%>/js/plugins/datapicker/bootstrap-datepicker.js"></script>
	<script src="<%=path%>/js/plugins/prettyfile/bootstrap-prettyfile.js"></script>
	<script src="<%=path%>/js/plugins/switchery/switchery.js"></script>
	<script src="<%=path%>/js/plugins/iCheck/icheck.min.js"></script>
	<script
		src="<%=path%>/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
	<script src="<%=path%>/js/plugins/clockpicker/clockpicker.js"></script>
	<script src="<%=path%>/js/plugins/cropper/cropper.min.js"></script>
	<script src="<%=path%>/js/demo/form-advanced-demo.min.js"></script>
		<script type="text/javascript"
		src="<%=path%>/admin/paybank/js/paybank_edit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
	<!-- dwr -->
	<script type='text/javascript'
		src='<%=path%>/dwr/interface/managerPayBankAction.js'></script>
	<script type="text/javascript">
	payBankEditPage.param.id = <%=id%>;
	var imgServerUrl = '<%= SystemConfigConstant.imgServerUrl %>';
	</script>
</body>
</html>

