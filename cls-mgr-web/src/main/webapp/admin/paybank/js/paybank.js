function PayBankPage(){}
var payBankPage = new PayBankPage();

payBankPage.param = {
		
};

/**
 * 查询参数
 */
payBankPage.queryParam = {
		bizSystem : null,
		bankName : null,
		bankUserName : null
};

$(document).ready(function() {
	payBankPage.findPayBank(); //查询所有的收款银行数据
});


/**
 * 加载所有的收款银行数据
 */
PayBankPage.prototype.getAllPayBanks = function(queryParam){
	managerPayBankAction.getAllPayBank(queryParam,function(r){
		if (r[0] != null && r[0] == "ok") {
			payBankPage.refreshPayBankPages(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
			swal(r[1]);
		}else{
			swal("查询收款银行数据失败.");
		}
    });
};

/**
 * 刷新列表数据 payBank.id
 */
PayBankPage.prototype.refreshPayBankPages = function(payBanks){
	var payBanksListObj = $("#payBanksList");
	payBanksListObj.html("");
	var str = "";
	for(var i = 0 ; i < payBanks.length; i++){
		var payBank = payBanks[i];
		
		str +='<div class="col-sm-4">'
                    +'<div class="ibox float-e-margins" style="border-color: #e7eaec;border-image: none;border-style: solid solid none;border-width: 4px 0 0;">'
                    +'<div class="ibox-content" style="padding-bottom:20px;"><table class="table table-bordered"><tbody>';
		/*str += "  <dd><span class='title'>序号：</span><span class='info'>"+ payBank.id +"</span></dd>";
		str += "  <dd><span class='title'>管理员：</span><span class='info'>"+ payBank.adminname +"</span></dd>";*/
		if(currentUser.bizSystem=='SUPER_SYSTEM'){
			str += '  <tr><td width="82">业务系统：</td><td>'+payBank.bizSystemName+'</td><tr>';
		}
		/*str += '  <tr><td width="82">序号：</td><td>'+payBank.id+'</td><tr>';*/
		if(payBank.bankType=="ALIPAY")
			{
			str += '  <tr><td width="82">银行名称：</td><td>'+payBank.bankName+'</td><tr>';
			str += '  <tr><td width="82">支行名称：</td><td>'+payBank.subbranchName+'</td><tr>';
			str += '  <tr><td width="82">支付宝帐号：</td><td>'+payBank.bankId+'</td><tr>';
			/*str += '  <tr><td width="82">卡号别名：</td><td>'+(payBank.aliasName == null?"":payBank.aliasName)+'</td><tr>';*/
			str += '  <tr><td width="82">支付宝姓名：</td><td>'+payBank.bankUserName+'</td><tr>';
			/*str += '  <tr><td width="82">银行URL：</td><td>'+payBank.bankUrl+'</td><tr>';*/
			}
		else if(payBank.bankType=="WEIXIN")
			{
			str += '  <tr><td width="82">银行名称：</td><td>'+payBank.bankName+'</td><tr>';
			str += '  <tr><td width="82">支行名称：</td><td>'+payBank.subbranchName+'</td><tr>';
			str += '  <tr><td width="82">微信账号：</td><td>'+payBank.bankId+'</td><tr>';
			/*str += '  <tr><td width="82">卡号别名：</td><td>'+(payBank.aliasName == null?"":payBank.aliasName)+'</td><tr>';*/
			str += '  <tr><td width="82">微信呢称：</td><td>'+payBank.bankUserName+'</td><tr>';
			/*str += '  <tr><td width="82">银行URL：</td><td>'+payBank.bankUrl+'</td><tr>';*/
			}
		else
			{
			str += '  <tr><td width="82">银行名称：</td><td>'+payBank.bankName+'</td><tr>';
			str += '  <tr><td width="82">支行名称：</td><td>'+payBank.subbranchName+'</td><tr>';
			str += '  <tr><td width="82">银行卡号：</td><td>'+payBank.bankId+'</td><tr>';
			/*str += '  <tr><td width="82">卡号别名：</td><td>'+(payBank.aliasName == null?"":payBank.aliasName)+'</td><tr>';*/
			str += '  <tr><td width="82">姓名：</td><td>'+payBank.bankUserName+'</td><tr>';
			/*str += '  <tr><td width="82">银行URL：</td><td>'+payBank.bankUrl+'</td><tr>';*/
			}
		
		/*str += '  <tr><td width="82">最低金额：</td><td>'+payBank.lowestValue.toFixed(3)+'</td><tr>';
		str += '  <tr><td width="82">最高金额：</td><td>'+payBank.highestValue.toFixed(3)+'</td><tr>';*/
		
		str += '   </tbody></table>';    
		if(payBank.isDisabled == 1){
			
			str += '     <div class="text-center" style="padding-top:20px;"><button class="btn  btn-warning" onclick="payBankPage.closePayBank('+ payBank.id +')" type="button"><i class="fa fa-close"></i>&nbsp;关闭</button>&nbsp';
		}else{
			str += '     <div class="text-center" style="padding-top:20px;"><button class="btn  btn-success" onclick="payBankPage.openPayBank('+ payBank.id +')" type="button"><i class="glyphicon glyphicon-ok"></i>&nbsp;启用</button>&nbsp';
			
		}
		str += '<button class="btn btn-info btn-paste" onclick="payBankPage.editPaybank('+ payBank.id +')" type="button">'+
               '<i class="fa fa-paste"></i>&nbsp;编辑</button>&nbsp;<button class="btn btn-danger btn-trash" onclick="payBankPage.delPayBank('+ payBank.id +')" type="button"><i class="fa fa-trash"></i>&nbsp;删除</button></div></div></div></div>';
                                        
		payBanksListObj.append(str);
		str = "";
	}
};

/**
 * 删除收款银行数据
 */
PayBankPage.prototype.delPayBank = function(id){
	swal({
        title: "您确定要删除这条信息吗",
        text: "请谨慎操作！",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        cancelButtonText: "取消",
        confirmButtonText: "删除",
        closeOnConfirm: false
    },
    function() {
    	managerPayBankAction.delPayBank(id,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "删除成功",type: "success"});
				payBankPage.findPayBank();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("删除收款银行数据失败.");
			}
	    });
 	   
    });

		
	
};

/**
 * 开启收款银行
 */
PayBankPage.prototype.openPayBank = function(id){
	swal({
        title: "启用收款银行",
        text: "你确认要启用收款银行?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        cancelButtonText: "取消",
        confirmButtonText: "是的，我确定",
        closeOnConfirm: false
    },
    function() {
    	managerPayBankAction.openPayBank(id,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "操作成功",type: "success"});
				payBankPage.findPayBank();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("启用收款银行失败.");
			}
	    });
 	   
    });
};

/**
 * 关闭收款银行
 */
PayBankPage.prototype.closePayBank = function(id){
	swal({
        title: "关闭收款银行",
        text: "你确认要关闭收款银行?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        cancelButtonText: "取消",
        confirmButtonText: "是的，我要关闭",
        closeOnConfirm: false
    },
    function() {
    	managerPayBankAction.closePayBank(id,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "操作成功",type: "success"});
				payBankPage.findPayBank();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("关闭收款银行失败.");
			}
	    });
 	   
    });
};

/**
 * 编辑银行卡信息
 */
PayBankPage.prototype.editPaybank = function(id){
	layer.open({
        type: 2,
        title: '银行卡信息编辑',
        maxmin: false,
        shadeClose: true,
        //点击遮罩关闭层
        area: ['70%', '80%'],
        content: contextPath + "/managerzaizst/paybank/"+id+"/paybank_edit.html",
        cancel: function(index){ 
        	payBankPage.findPayBank();
     	   }
    });
};

/**
 * 添加银行卡信息
 */
PayBankPage.prototype.addPaybank = function(id){
	layer.open({
        type: 2,
        title: '银行卡信息新增',
        maxmin: false,
        shadeClose: true,
        //点击遮罩关闭层
        area: ['70%', '80%'],
        content: contextPath + "/managerzaizst/paybank/paybank_edit.html",
        cancel: function(index){ 
        	payBankPage.findPayBank();
     	   }
    });
};

/**
 * 查询数据
 */
PayBankPage.prototype.findPayBank = function(){
	var bizSystem = $("#bizSystem").val();
	var bankName = $("#bankName").val();
	var bankUserName = $("#bankUserName").val();
	if(bizSystem == ""){
		payBankPage.queryParam.bizSystem = null;
	}else{
		payBankPage.queryParam.bizSystem = bizSystem;
	}
	if(bankName == ""){
		payBankPage.queryParam.bankName = null;
	}else{
		payBankPage.queryParam.bankName = bankName;
	}
	if(bankUserName == ""){
		payBankPage.queryParam.bankUserName = null;
	}else{
		payBankPage.queryParam.bankUserName = bankUserName;
	}
	
	payBankPage.getAllPayBanks(payBankPage.queryParam);
};

PayBankPage.prototype.closeLayer=function(index){
	layer.close(index);
	payBankPage.findPayBank();
}
