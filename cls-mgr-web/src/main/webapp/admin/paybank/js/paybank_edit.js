function PayBankEditPage(){}
var payBankEditPage = new PayBankEditPage();

payBankEditPage.param = {
   	
};
payBankEditPage.bankUrlList = {
		"BOC" :"http://www.boc.cn/" ,
		"ICBC" :"http://www.icbc.com.cn/icbc/" ,
		"ABC" :"http://www.abchina.com/cn/" ,
		"CCB" : "http://www.ccb.com/cn/home/indexv3.html",
		"COMM" :"http://www.bankcomm.com/BankCommSite/default.shtml" ,
		"CMB" :"http://www.cmbchina.com/" ,
		"PSBC" :"http://www.psbc.com/cn/index.html" ,
		"CEB" :"http://www.cebbank.com/" ,
		"SPABANK" :"http://bank.pingan.com/",
		"CITIC" :"http://www.citicbank.com/",
		"CMBC" :"http://www.cmbc.com.cn/",
		"CIB" :"http://www.cib.com.cn/cn/index.html",
		"GDB" :"http://www.cgbchina.com.cn/",
		"HXBANK" :"http://www.hxb.com.cn/home/cn/"
};
var initFinsh = false;

$(document).ready(function() {
	payBankEditPage.initPage();
	var paybankId = payBankEditPage.param.id;  //获取查询的ID
	if(paybankId != null){
		payBankEditPage.editPaybank(paybankId);
	}
	
	//选择银行时控制二维码显示
	$("#bankName").change(function(){
		if($(this).val() == "ALIPAY" || $(this).val() == "WEIXIN") {
			$(".twoCode").show();
			$("#subBranchName").parents("div.form-group").hide();
			$("#aliasName").parents("div.form-group").hide();
			if($(this).val() == "ALIPAY")
				{
				$("#bankId").parent("div").siblings("label").html("支付宝账号:");
				$("#bankUserName").parent("div").siblings("label").html("支付宝姓名:");
				}
			else if($(this).val() == "WEIXIN")
				{
				$("#bankId").parent("div").siblings("label").html("微信账号:");
				$("#bankUserName").parent("div").siblings("label").html("微信呢称:");
				}
			$("#bankUrlDiv").hide();
		} else {
			$(".twoCode").hide();
			$("#subBranchName").parents("div.form-group").show();
			$("#aliasName").parents("div.form-group").show();
			$("#bankId").parent("div").siblings("label").html("银行卡号:");
			$("#bankUserName").parent("div").siblings("label").html("姓名:");
			$("#bankUrlDiv").show();
			$("#bankUrl").val(payBankEditPage.bankUrlList[$(this).val()]);
		}
	});
	
});

/**
 * 保存收款银行数据
 */
PayBankEditPage.prototype.savePayBank = function(){
	var payBank = {};
	payBank.id = $("#paybankId").val();
	/*payBank.adminname = $("#adminname").val();*/
	payBank.bankType = $("#bankName").val();
	payBank.subbranchName = $("#subBranchName").val();
	payBank.bankId = $("#bankId").val();
	payBank.aliasName = $("#aliasName").val();
	payBank.bankUserName = $("#bankUserName").val();
	/*payBank.lowestValue = $("#lowestValue").val();
	payBank.highestValue = $("#highestValue").val();*/
	payBank.bankUrl = $("#bankUrl").val();
	payBank.bankLogo ="";
	payBank.isDisabled = "1";
	payBank.barcodeUrl = $("#imagePath").val();
	payBank.bizSystem = $("#bizSystem").val();
	payBank.adminId = $("#adminId").val();
	payBank.adminName = $("#adminName").val();
	payBank.yetUse = $("#yetUse").val();
	/*payBank.createTime = $("#createTime").val();*/
	if(currentUser.bizSystem=='SUPER_SYSTEM'){
		if (payBank.bizSystem==null || payBank.bizSystem.trim()=="") {
			swal("请选择业务系统!");
			$("#bizSystem").focus();
			return;
		}
	}
	
	if (payBank.bankType==null || payBank.bankType.trim()=="") {
		swal("请选择银行名称!");
		$("#bankName").focus();
		return;
	} else {
		//不为支付宝或者微信的时候 不需要二维码图片
		if("ALIPAY" != payBank.bankType && "WEIXIN" != payBank.bankType) {
			payBank.barcodeUrl = null;
		} 
	}
	if("ALIPAY" != payBank.bankType && "WEIXIN" != payBank.bankType)
		{
		if (payBank.subbranchName==null || payBank.subbranchName.trim()=="") {
			swal("请输入支行名称!");
			$("#subBranchName").focus();
			return;
		}
		}
	
	if (payBank.bankId==null || payBank.bankId.trim()=="") {
		swal("请输入银行卡号!");
		$("#bankId").focus();
		return;
	}
	if (payBank.bankUserName==null || payBank.bankUserName.trim()=="") {
		swal("请输入银行开户姓名!");
		$("#bankUserName").focus();
		return;
	}
	/*if (payBank.lowestValue==null || payBank.lowestValue.trim()=="") {
		swal("请输入最低金额!");
		$("#lowestValue").focus();
		return;
	}
	if( isNaN(payBank.lowestValue)){
		swal("最低金额必须为数字!");
		$("#lowestValue").focus();
		return;
	}
	if (payBank.highestValue==null || payBank.highestValue.trim()=="") {
		swal("请输入最高金额!");
		$("#highestValue").focus();
		return;
	}
	if( isNaN(payBank.highestValue)){
		swal("最高金额必须为数字!");
		$("#highestValue").focus();
		return;
	}*/
	
	if("ALIPAY" != payBank.bankType && "WEIXIN" != payBank.bankType)
	{
		if (payBank.bankUrl==null || payBank.bankUrl.trim()=="") {
			swal("请输入银行网址!");
			$("#bankUrl").focus();
			return;
		}
	}
	
	
	managerPayBankAction.saveOrUpdatePayBank(payBank,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.payBankPage.closeLayer(index);
		   	    }); 
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("添加收款银行数据失败.");
		}
    });
};

/**
 * 初始化页面
 */
PayBankEditPage.prototype.initPage = function(id){
	/*$("#isdisabled").val("0");*/
	/*managerPayBankAction.initForPayBankPage(function(r){
		if (r[0] != null && r[0] == "ok") {
			var adminnameSelectObj = $("#adminname");
			adminnameSelectObj.empty();
			adminnameSelectObj.append("<option value=''></option>");
			
			var initAdmins = r[1];
			for(var i = 0 ; i < initAdmins.length; i++){
				var admin = initAdmins[i];
				if(admin.state==0 || admin.state==1){
					adminnameSelectObj.append("<option value='"+admin.username+"'>"+admin.username+"</option>");
				}
			}
			initFinsh = true;
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询管理员数据失败.");
		}
    });*/
};

/**
 * 初始化页面
 */
PayBankEditPage.prototype.editPaybank = function(id){
	managerPayBankAction.getPayBankById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			var payBank = r[1];
			$("#paybankId").val(payBank.id);
			//加载管理员结束才赋值
			if(initFinsh) {
				$("#adminname").val(payBank.adminname);
			} else {
				//否则延迟执行设置
				setTimeout("payBankEditPage.setAdminname('"+payBank.adminname+"')" , 1000);
			}
			$("#bankName").val(payBank.bankType);
			$("#subBranchName").val(payBank.subbranchName);
			$("#bankId").val(payBank.bankId);
			$("#aliasName").val(payBank.aliasName);
			$("#bankUserName").val(payBank.bankUserName);
			/*$("#lowestValue").val(payBank.lowestValue);
			$("#highestValue").val(payBank.highestValue);*/
			$("#bankUrl").val(payBank.bankUrl);
			/*$("#bankLogo").val(payBank.bankLogo);*/
			/*$("#isDisabled").val(payBank.isDisabled);*/
			$("#bizSystem").val(payBank.bizSystem);
			$("#adminId").val(payBank.adminId);
			$("#adminName").val(payBank.adminName);
			$("#yetUse").val(payBank.yetUse);
			$("#createTime").val(payBank.createTime);
			if("ALIPAY" == payBank.bankType || "WEIXIN" == payBank.bankType) {
				$(".twoCode").show();
				if(payBank.barcodeUrl != null && payBank.barcodeUrl != "") {
					//展示图片内容
					$("#image").attr("src", r[2] + payBank.barcodeUrl);
					$("#imagePath").val(payBank.barcodeUrl);
					$("#image").show();
				}
			} else {
				$(".twoCode").hide();
			}
			
		}else if(r[0] != null && r[0] == "error"){
			showErrorDlg(jQuery(document.body), r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "获取银行卡信息失败.");
		}
    });
};

PayBankEditPage.prototype.setAdminname = function(adminname) {
	$("#adminname").val(adminname);
} 


/**
 * 赋值活动信息
 */
PayBankEditPage.prototype.uploadImgFile = function(id){
	var fileId = id + "File";
	if($("#" + fileId).val() == "") {
		swal("请选择图片文件.");
		return; 
	}
	var bizSystem = $("#bizSystem").val();
	if(currentUser.bizSystem=='SUPER_SYSTEM'){
		if (bizSystem == null || bizSystem.trim()=="") {
			swal("请选择业务系统!");
			$("#bizSystem").focus();
			return;
		}
	}
	$("#" + id +"Info").text("上传中...");
	$("#" + id +"Info").css("color","blue");
	var uploadFile = dwr.util.getValue(fileId);
	managerPayBankAction.uploadImgFile(uploadFile, bizSystem, function(r){
		if (r[0] != null && r[0] == "ok") {
			$("#" + id + "Path").val(r[1]);
			$("#" + id).attr("src",  r[2] + r[1]);
			$("#" + id).show();
			$("#" + id +"Info").text("上传成功!");
			$("#" + id +"Info").css("color","red");
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("上传文件失败");
		}
	});
};