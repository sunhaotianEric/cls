<%@ page language="java" contentType="text/html; charset=UTF-8" 
	import = "com.team.lottery.enums.EBankInfo,com.team.lottery.system.SystemConfigConstant" 
	pageEncoding="UTF-8"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<html>
<head>
    <meta charset="utf-8" />
    <title>银行卡设置</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/paybank/js/paybank_edit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerPayBankAction.js'></script>
    
    <link rel="stylesheet" href="<%=path%>/resources/kindeditor-4.1.10/themes/default/default.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" />
	<script charset="utf-8" src="<%=path%>/resources/kindeditor-4.1.10/kindeditor-min.js"></script>
	<script charset="utf-8" src="<%=path%>/resources/kindeditor-4.1.10/lang/zh_CN.js"></script>
	
	<script type="text/javascript">
		var imgServerUrl = '<%= SystemConfigConstant.imgServerUrl %>';
	</script>
</head>
<body>
   <div class="alert alert-info">当前位置<b class="tip"></b>网站信息管理<b class="tip"></b>编辑银行卡</div>
   	
   	<table class="tb">
         <tr>
         	<td> <a href="<%=path %>/managerzaizst/paybank.html">银行卡管理</a> &nbsp;&nbsp; <a href="<%=path %>/managerzaizst/paybank/paybank_add.html">添加银行卡</a></td>
         </tr>
     </table>
    <form action="javascript:void(0)">
	    <table class="tb">
	    
	   		<!-- <tr>
	   			<td width="10%">管理员：</td>
	   			<td><input type="hidden" id="paybankId"/><select class="select" id="adminname"></select></td>
	   		</tr> -->
	   		<c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
	   		<tr>
	   			<td>商户：</td>
	   			<td><cls:bizSel name="bizSystem" id="bizSystem"/></td>
	   		</tr>
	   		</c:if>
	   		<tr>
	   			<td>银行名称：</td>
	   			<td>
	   			<input type="hidden" id="paybankId"/>
	   			   <select class="select" id="bankName">
						<option value="">请选择</option>
							<%
							EBankInfo [] bankInfos = EBankInfo.values();
							for(EBankInfo bankInfo : bankInfos){
							%>
						<option value='<%=bankInfo.getCode() %>'><%=bankInfo.getDescription() %></option>
							<%
							}
							%>
					</select>
	   			</td>
	   		</tr>
	   		<tr>
	   			<td>支行名称：</td>
	   			<td><input id="subBranchName" type="text" size="35"/></td>
	   		</tr>
	   		<tr>
	   			<td>银行卡号：</td>
	   			<td><input id="bankId" type="text" size="35"/></td>
	   		</tr>
	   		<tr>
	   			<td>卡号别名：</td>
	   			<td><input id="aliasName" type="text" size="35"/></td>
	   		</tr>
	   		<tr>
	   			<td>开户名称：</td>
	   			<td><input id="bankUserName" type="text" size="35"/></td>
	   		</tr>
	   		<tr>
	   			<td>最低金额：</td>
	   			<td><input id="lowestValue" type="text" size="35"/></td>
	   		</tr>
	   		<tr>
	   			<td>最高金额：</td>
	   			<td><input id="highestValue" type="text" size="35"/></td>
	   		</tr>
	   		<tr>
	   			<td>银行网址：</td>
	   			<td><input id="bankUrl" type="text" size="35"/></td>
	   		</tr>
	   		<tr>
	   			<td>银行LOGO：</td>
	   			<td><input id="bankLogo" type="text" size="35"/></td>
	   		</tr>
	   		<tr id ="barcodeTr" style="display: none;">
	   			<td>二维码图片：</td>
	   			<td>
	   				<input type="hidden" id="imagePath" size="50px"/>
	   				<input type="file" id="imageFile" accept=".jpg,.png"/>
   				    <input type="button" onclick="payBankEditPage.uploadImgFile('image')" value="上传"/>  
   				    <span id="imageInfo"></span>
   				    <br/>
   				    <img id="image" alt="活动图片" src="" style="display: none;margin-top: 5px">
	   			</td>
	   		</tr>
	   		<tr>
	   			<td>是否启用：</td>
	   			<td>
	   			<select class="select" id="isDisabled">
	   				<option value="1">启用</option>
	   				<option value="0">停用</option>
	   			</select>
	   			</td>
	   		</tr>
	   		<tr>
	   			<td></td>
	   			<td><input type="button" onclick="payBankEditPage.savePayBank()" value="提交"/>&nbsp;&nbsp;&nbsp;&nbsp;<input type="reset" value="重置"/></td>
	   		</tr>
	    </table>
    </form>
</body>
</html>

