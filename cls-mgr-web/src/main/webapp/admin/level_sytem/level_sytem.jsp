<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
    
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>会员等级机制管理</title>
        <link rel="shortcut icon" href="favicon.ico">
        
        <jsp:include page="/admin/include/include.jsp"></jsp:include>
    </head>
    
    <body>
        <div class="animated fadeIn">
            <div class="ibox-content">
                 <div class="row m-b-sm m-t-sm">
                    <form class="form-horizontal" id="bizsystemForm">
                        <div class="row">
                        	<c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
	                            <div class="col-sm-4">
	                                <div class="input-group m-b">
	                                    <span class="input-group-addon">商户名称</span>
	                                    <cls:bizSel name="bizSystem" id="bizSystem"	options="class:ipt form-control" />
	                                </div>
	                            </div>
	                            <div class="col-sm-4">
	                                <div class="form-group nomargin text-right">
	                                    <button type="button" class="btn btn-outline btn-default btn-js" onclick="levelSystemPage.getLevelSytem()"><i class="fa fa-search"></i>&nbsp;查 询</button>
	                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	                                    <button type="button" class="btn btn-outline btn-default btn-add" onclick="levelSystemPage.editLevelSystem()"><i class="fa fa-plus"></i>&nbsp;新 增</button>
	                                 </div>
	                            </div>
	                        </c:if>
                         </div>
                    </form>
                    </div>
                     <div class="jqGrid_wrapper">
                        <div class="ui-jqgrid ">
                            <div class="ui-jqgrid-view">
                                <div class="ui-jqgrid-hdiv">
                                    <div class="ui-jqgrid-hbox" style="padding-right:0">
                                        <table id="levelSystemPageTable" class="ui-jqgrid-htable ui-common-table table table-bordered">
                                            <thead>
                                                <tr class="ui-jqgrid-labels">
                                                	<th class="ui-th-column ui-th-ltr">商户</th>
                                                    <th class="ui-th-column ui-th-ltr">等级</th>
                                                    <th class="ui-th-column ui-th-ltr">头衔</th>
                                                    <th class="ui-th-column ui-th-ltr">成长积分</th>
													<!--<th>商户类型</th>预留-->
													<th class="ui-th-column ui-th-ltr">晋级奖励(元)</th>
													<th class="ui-th-column ui-th-ltr">跳级奖励(元)</th>
													<th class="ui-th-column ui-th-ltr">操 作</th>
                                                </tr>
                                            </thead>
                                            <tbody id="bizsystemList">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>     
		         </div>
		      </div>
            
		    <!-- dwr -->
		    <script type='text/javascript' src='<%=path%>/dwr/interface/manageLevelSytemAction.js'></script>
		    <script type="text/javascript" src="<%=path%>/admin/level_sytem/js/level_sytem.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    </body>

</html>
