<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
	String id=request.getParameter("id");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>等级机制编辑</title>
        <link rel="shortcut icon" href="favicon.ico">
        <jsp:include page="/admin/include/include.jsp"></jsp:include>
        <script type="text/javascript">
        	var id=<%=id%>;
        </script>
<body>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeIn">
		<form action="javascript:void(0)">
			<div class="row">
				<c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
					<div class="col-sm-12">
						<div class="input-group m-b">
							<span class="input-group-addon">商户：</span>
							<cls:bizSel name="bizSystem" id="bizSystem"	options="class:ipt form-control" />
						</div>
					</div>
				</c:if>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">等级：</span>
						<input type="text" name="level" id="level" class="form-control" />
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">头衔：</span>
						<input type="text" id="levelName" name="levelName" class="form-control" />
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">成长积分：</span>
						<input type="text" id="point" name="point" class="form-control" />
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">晋级奖励：</span>
						<input type="text" id="promotionAward" class="form-control" name="promotionAward" />
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group m-b">
						<span class="input-group-addon">跳级奖励：</span> 
						<input type="text" value="" class="form-control" name="skipAward" id="skipAward" />
					</div>
				</div>
				<div class="col-sm-12">
						<div class="col-sm-6 text-center">
							<button type="button" class="btn btn-w-m btn-white"
								onclick="levelSystemEditPage.saveLevelSystem()">提 交</button>
							<input type="reset" class="btn btn-w-m btn-white" value="重 置" />
						</div>
				</div>
			</div>
		</form>
	</div>
	
	<!-- dwr -->
	<script type='text/javascript' src='<%=path%>/dwr/interface/manageLevelSytemAction.js'></script>
	<script type="text/javascript" src="<%=path%>/admin/level_sytem/js/level_systemEdit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
</body>
</html>