function LevelSystemEditPage(){}
var levelSystemEditPage = new LevelSystemEditPage();

/**
 * 查询参数
 */
levelSystemEditPage.queryParam = {
	id:null,
	bizSystem : null,
	level:null,
	levelName:null,
	point:null,
	promotionAward:null,
	skipAward:null
};
levelSystemEditPage.param = {
	skipAwardZh:0  //跳级奖励统计	
}

$(document).ready(function() {
	levelSystemEditPage.queryParam.id=id;
	if(levelSystemEditPage.queryParam.id!=null&&levelSystemEditPage.queryParam.id!=""){
		levelSystemEditPage.getLevelSystemByid();
	}
	
});

//编辑时查询单个会员等级机制信息
LevelSystemEditPage.prototype.getLevelSystemByid=function(){
	manageLevelSytemAction.getLevelSystemByid(levelSystemEditPage.queryParam.id,function(r){
		if(r[0]=="ok"){
			$("#bizSystem").val(r[1].bizSystem);
			$("#level").val(r[1].level);
			$("#levelName").val(r[1].levelName);
			$("#point").val(r[1].point);
			$("#promotionAward").val(r[1].promotionAward);
			$("#skipAward").val(r[1].skipAward);
			$("#bizSystem").attr("disabled","disabled");
			if(currentUser.bizSystem!='SUPER_SYSTEM'){
				$("#level").attr("disabled","disabled");
				$("#levelName").attr("disabled","disabled");
			}
		}else{
			swal(r[1]);
		}
	});
}

//新增等级机制
LevelSystemEditPage.prototype.saveLevelSystem=function(){
	var bizSystem=$("#bizSystem").val();
	var level=$("#level").val();
	var levelName=$("#levelName").val();
	var point=$("#point").val();
	var promotionAward=$("#promotionAward").val();
	var skipAward=$("#skipAward").val();
	if(level==null||level==""){
		swal("等级不能为空");
		return;
	}
	if(isNaN(level)){
		swal("等级请用数字录入！");
		return false;
	}
	if(levelName==null||levelName==""){
		swal("头衔不能为空");
		return;
	}
	if(point==null||point==""){
		swal("成长积分不能为空");
		return;	
	}
	if(isNaN(point)){
		swal("成长积分请用数字录入！");
		return false;
	}
	if(promotionAward==null||promotionAward==""){
		swal("晋级奖励不能为空");
		return;
	}
	if(isNaN(promotionAward)){
		swal("晋级奖励请用数字录入！");
		return false;
	}
	if(skipAward==null||skipAward==""){
		swal("跳级奖励不能为空");
		return;
	}
	if(isNaN(skipAward)){
		swal("跳级奖励请用数字录入！");
		return false;
	}
	
	levelSystemEditPage.queryParam.bizSystem=bizSystem;
	levelSystemEditPage.queryParam.level=level;
	levelSystemEditPage.queryParam.levelName=levelName;
	levelSystemEditPage.queryParam.point=point;
	levelSystemEditPage.queryParam.promotionAward=promotionAward;
	levelSystemEditPage.queryParam.skipAward=skipAward;
	if(levelSystemEditPage.queryParam.id!=null&&levelSystemEditPage.queryParam.id!=""){
		manageLevelSytemAction.updateLevelSystem(levelSystemEditPage.queryParam,function(r){
			if(r[0]=="ok"){
				swal({title:"修改成功！",
			        text:"已成功修改数据",
			        type:"success"},
			      function(){
			        	var index = parent.layer.getFrameIndex(window.name); //获取窗口索引    
			        	parent.levelSystemPage.layerClose(index);
				  }
			     )				
			}else{
				swal(r[1]);
			}
		});
	}else{
		manageLevelSytemAction.addLevelSystem(levelSystemEditPage.queryParam,function(r){
			if(r[0]=="ok"){
				swal({title:"保存成功！",
			        text:"已成功保存数据",
			        type:"success"},
			      function(){
			        	var index = parent.layer.getFrameIndex(window.name); //获取窗口索引    
			        	parent.levelSystemPage.layerClose(index);
				  }
			     )	
			}else{
				swal(r[1]);
			}
		});
	}
	levelSystemEditPage.queryParam={};
}