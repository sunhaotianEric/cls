function LevelSystemPage(){}
var levelSystemPage = new LevelSystemPage();

/**
 * 查询参数
 */
levelSystemPage.queryParam = {
		id:null,
		bizSystem : null,
		level:null,
		levelName:null,
		point:null,
		promotionAward:null,
		skipAward:null
};
//分页参数
levelSystemPage.pageParam = {
    pageNo : 1,
    pageSize : defaultPageSize
};

$(document).ready(function() {
	levelSystemPage.initTableHTML();
});

LevelSystemPage.prototype.initTableHTML = function() {
	levelSystemPage.table = $('#levelSystemPageTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"scrollX": true,
		"iDisplayLength" : defaultPageSize,
		"ajax":function (data, callback, settings) {
			levelSystemPage.pageParam.pageSize = data.length;//页面显示记录条数
			levelSystemPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	manageLevelSytemAction.getLevelSytem(levelSystemPage.queryParam,levelSystemPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": "bizSystemName","bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {
		            	"data": "level",
		            	render:function(data, type, row, meta){
		            		return "VIP"+data;
		            	}
		            },
		            {"data": "levelName"},
		            {"data": "point"},
		            {"data": "promotionAward"},
		            {"data": "skipAward"},		            
		            {
	                	 "data": null,
	                	 render: function(data, type, row, meta) {
	                		 var str=""
	                			 if(currentUser.bizSystem!='SUPER_SYSTEM'){
	                				 str+="<a class='btn btn-info btn-sm btn-bj' onclick='levelSystemPage.editLevelSystem("+row.id+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;";
	                			 }else{
	                				 str+="<a class='btn btn-info btn-sm btn-bj' onclick='levelSystemPage.editLevelSystem("+row.id+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;";
		                		 	 str+="<a class='btn btn-danger btn-sm btn-del' onclick='levelSystemPage.delLevelSystem("+row.id+")'><i class='fa fa-trash'></i>&nbsp;删除 </a>";
	                			 }
	                	         return str;
	                	 }
	                }
	            ]
	}).api(); 
}

LevelSystemPage.prototype.getLevelSytem=function(){
	var bizSystem=$("#bizSystem").val();
	if(bizSystem==null||bizSystem==""){
		levelSystemPage.queryParam.bizSystem=null;
	}else{
		levelSystemPage.queryParam.bizSystem=bizSystem;
	}
	levelSystemPage.table.ajax.reload();
}

LevelSystemPage.prototype.delLevelSystem=function(id,root){
	if(id==0){
		$("#"+root).parent().remove();
	}else{
		swal({
	        title: "您确定要删除这条信息吗",
	        text: "请谨慎操作！",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonColor: "#DD6B55",
	        cancelButtonText: "取消",
	        confirmButtonText: "删除",
	        closeOnConfirm: false
	    },
	    function() {
	    		manageLevelSytemAction.delLevelSystem(id,function(r){
	    			if(r[0]=="ok"){
	    				swal("删除完成");
	    				levelSystemPage.getLevelSytem();
	    			}else{
	    				swal(r[1]);
	    			}
	    		});
	    });
    }
}

//编辑当前选中的会员等级机制
LevelSystemPage.prototype.editLevelSystem=function(id){
	layer.open({
        type: 2,
        title: '会员等级机制编辑',
        maxmin: false,
        shadeClose: true,
        //点击遮罩关闭层
        area: ['600px', '450px'],
        content: contextPath + "/managerzaizst/level_sytem/level_systemEdit.html?id="+id,
        cancel: function(index){ 
        	
     	}
    });
}

LevelSystemPage.prototype.layerClose=function(index){
	layer.close(index);
	levelSystemPage.getLevelSytem();
}