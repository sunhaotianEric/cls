function LotteryDayGainPage(){};

var lotteryDayGainPage = new LotteryDayGainPage();

lotteryDayGainPage.param ={
	bizSystem : null,
	gainModel : null,
	belongDateStart : null,
	belongDateEnd : null
}

//分页参数
lotteryDayGainPage.pageParam = {
    pageNo : 1,
    pageSize : defaultPageSize
};

$(document).ready(function(){
//	var newtimems = new Date().getTime()-(24*60*60*1000);
//	var yesd = new Date(newtimems).format("yyyy-MM-dd");
//	$("#startime").val(yesd);
//	$("#endtime").val(yesd);
	var curHour = new Date().getHours(); 
	if(curHour<3){
		var newtimems = new Date().getTime()-(24*60*60*1000);
		var yesd = new Date(newtimems).format("yyyy-MM-dd");
		$("#startime").val(yesd);
		$("#endtime").val(yesd);
		laydate({
			elem: '#startime',
			istime: false,
			format: 'YYYY-MM-DD',
			max: laydate.now(-1)});
		laydate({
			elem: '#endtime',
			istime: false,
			format: 'YYYY-MM-DD',
			max: laydate.now(-1)});
	}else{
		var newtimems = new Date().getTime();
		var yesd = new Date(newtimems).format("yyyy-MM-dd");
		$("#startime").val(yesd);
		$("#endtime").val(yesd);
		laydate({
			elem: '#startime',
			istime: false,
			format: 'YYYY-MM-DD',
			max: laydate.now()});
		laydate({
			elem: '#endtime',
			istime: false,
			format: 'YYYY-MM-DD',
			max: laydate.now()});
	}
	
	lotteryDayGainPage.initLotteryDayGain();
});

LotteryDayGainPage.prototype.initLotteryDayGain = function(){
	
	lotteryDayGainPage.param= getFormObj($("#lotteryDayGainQuery"));
	lotteryDayGainPage.table =  $('#lotteryDayGainTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			lotteryDayGainPage.pageParam.pageSize = data.length;//页面显示记录条数
			lotteryDayGainPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
			managerLotteryDayGainAction.getLotteryDayGainPage(lotteryDayGainPage.param,lotteryDayGainPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				}; 
				
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询彩种盈利数据失败.");
				}
				callback(returnData);
			});
		},
	"columns": [
	            {"data":null},
	            {"data":"bizSystemName",
		            	"bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
	            {"data":"lotteryType"},
	            {"data":"payMoney",
	            	render: function(data, type, row, meta) {
	            		return data.toFixed(commonPage.param.fixVaue);
	            	}
	            },
	            {"data":"winPayMoney",
	            	render: function(data, type, row, meta) {
	            		return data.toFixed(commonPage.param.fixVaue);
	            	}
	            },
	            {"data":"notWinPayMoney",
	            	render: function(data, type, row, meta) {
	            		return data.toFixed(commonPage.param.fixVaue);
	            	}
	            },
	            {"data":"winMoney",
	            	render: function(data, type, row, meta) {
	            		return data.toFixed(commonPage.param.fixVaue);
	            	}
	            },
	            {"data":"gain",
	            	render: function(data, type, row, meta) {
	            		return data.toFixed(commonPage.param.fixVaue);
	            	}
	            },
	            {"data":"winGain",
	            	render: function(data, type, row, meta) {
	            		if(data != null){
	            			data = (data*100).toFixed(commonPage.param.fixVaue) +"%";
	            		}
	            		return data;
	            	}
	            },
	            ]
	}).api();
}

/**
 * 条件查找活动
 */
LotteryDayGainPage.prototype.findLotteryDayGain = function(){
//	lotteryDayGainPage.param.bizSystem = $("#bizSystem").val();
	lotteryDayGainPage.param = getFormObj($("#lotteryDayGainQuery"));
	lotteryDayGainPage.table.ajax.reload();
}
