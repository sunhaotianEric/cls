function UserRegister(){}
var userRegister=new UserRegister();

userRegister.param={
		userRegisterId:null
};

//分页参数
userRegister.pageParam={
		pageNo:1,
		pageSize:defaultPageSize
};
/**
 * 查询参数
 */
userRegister.queryParam = {
	bizSystem : null,
	userName : null,
	invitationCode:null
};

$(document).ready(function() {
	userRegister.initTableDate();
});

/**
 * 加载推广数据
 */
UserRegister.prototype.initTableDate=function(){
	//封装参数
	userRegister.queryParam = getFormObj($("#userRegisterForm"));
	userRegister.table = $("#userRegisterTable").dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"ajax":function (data, callback, settings) {
			userRegister.pageParam.pageSize = data.length;//页面显示记录条数
			userRegister.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerUserRegisterAction.queryUserRegister(userRegister.queryParam,userRegister.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询推广数据失败.");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": "bizSystemName","bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {
		            	"data": "userName",
		            	render: function(data, type, row, meta) {	
		            		return "<a href='javascript:void(0)' style='color:blue;text-decoration:underline;'  onclick='commonPage.showUserMsgPage(\""+data+"\","+row.userId+")'>"+ row.userName +"</a>";
	            		}
		            },
		            {"data": "invitationCode"},
		            {"data": "dailiLevel"},
		            {"data": "sscRebate"},
		            {"data": "ksRebate"},
		            {"data": "syxwRebate"},
		            {"data": "pk10Rebate"},
		            {"data": "dpcRebate"},
		            {"data": "useCount"},
		            {"data": "createdDateStr"},
		            {
	                	 "data": "id",
	                	 render: function(data, type, row, meta) {
	                		 var str="<a class='btn btn-info btn-sm btn-mx' onclick='userRegister.updateUserRegister("+data+")'><i class='fa fa-edit' ></i>&nbsp;编辑 </a>&nbsp;";
	                		 	 str+="<a class='btn btn-danger btn-sm btn-del' onclick='userRegister.delUserRegister("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";	
	                		 return str ;
	                	 }
	                }
	            ]
	}).api();
};

/**
 * 按条件查询数据并刷新表格
 */
UserRegister.prototype.getUserRegisterQuery=function(){
	var bizSystem=$("#bizSystem").val();
	var userName=$("#userName").val();
	var invitationCode=$("#invitationCode").val();
	if(bizSystem==""){
		userRegister.queryParam.bizSystem = null;
	}else{
		userRegister.queryParam.bizSystem = bizSystem;
	}
	if(userName == ""){
		userRegister.queryParam.userName = null;
	}else{
		userRegister.queryParam.userName = userName;
	}
	if(invitationCode == ""){
		userRegister.queryParam.invitationCode = null;
	}else{
		userRegister.queryParam.invitationCode = invitationCode;
	}
	userRegister.table.ajax.reload();
};

/**
 * 编辑推广链接
 */
UserRegister.prototype.updateUserRegister=function(id){
	layer.open({
        type: 2,
        title: '推广编辑',
        maxmin: false,
        shadeClose: true,
        //点击遮罩关闭层
        area: ['800px', '460px'],
        content:"user_register/user_register_edit.html?id="+id
    });
}

/**
 * 删除推广数据
 */
UserRegister.prototype.delUserRegister=function(id){
	swal({
        title: "您确定要删除本条推广链接吗?",
        text: "请谨慎操作！",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        cancelButtonText: "取消",
        confirmButtonText: "删除",
        closeOnConfirm: false
    },
    function() {
    	managerUserRegisterAction.delRegtister(id,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "删除成功",type: "success"});
				userRegister.table.ajax.reload();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("删除推广链接数据失败.");
			}
	    });
 	   
    });
}