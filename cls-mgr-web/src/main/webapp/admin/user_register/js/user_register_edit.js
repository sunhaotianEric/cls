function UserRegister(){}
var userRegister=new UserRegister();

userRegister.param = {
		linkId : null
};

/**
 * 添加参数
 */
userRegister.editParam = {
	id : null,
	invitationCode:null,
	sscrebate : null,
	sscRebate : null,
	ffcRebate : null,
	syxwRebate : null,
	ksRebate : null,
	klsfRebate : null,
	dpcRebate : null,
	pk10Rebate:null,
	ksRebate:null
};

$(document).ready(function() {
    //加载系统最低配额
	userRegister.loadUserRebate();
	if(userRegister.param.linkId !=null){
		userRegister.getRegisterLink();
	}else{
		swal({ title: "提示", text: "参数传递错误",type: "warning"});
	}
	$("#addExtendLinkButton").on("click",function(){
		userRegister.updateRegisterLink();
	});
	
	$("#sscModeAdd").unbind("click").click(function() {
		var sscrebateValue = Number($("#sscrebateValue").val());
		if(sscrebateValue%2!=0){
			userRegister.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		userRegister.editParam.sscrebate = sscrebateValue;
		if(userRegister.editParam.sscrebate > sscOpenUserHighest){
			userRegister.showKindlyReminder("大于时时彩模式["+ sscOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(userRegister.editParam.sscrebate < sscOpenUserLowest){
			userRegister.showKindlyReminder("小于时时彩模式["+ sscOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		if(userRegister.editParam.sscrebate == sscOpenUserHighest){
			userRegister.showKindlyReminder("当前时时彩模式已经是最高值了.");
			return;
		}
		
		userRegister.editParam.sscrebate += sscInterval; 
		if(userRegister.editParam.sscrebate > sscOpenUserHighest){
			userRegister.editParam.sscrebate = sscOpenUserHighest;
		}
		$("#sscrebateValue").val(userRegister.editParam.sscrebate);
	
		$("#sscTip").html('');
		$("#sscTip").append("返点"+userRegister.calculateRebateByAwardModelInterval(sscOpenUserHighest, userRegister.editParam.sscrebate, sscHighestAwardModel)+"%");
	});
	
	$("#sscModeReduction").unbind("click").click(function() {
		var sscrebateValue = Number($("#sscrebateValue").val());
		if(sscrebateValue%2!=0){
			userRegister.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		userRegister.editParam.sscrebate = sscrebateValue;
		if(userRegister.editParam.sscrebate > sscOpenUserHighest){
			userRegister.showKindlyReminder("大于时时彩模式最高值了.请重新输入！");
			return;
		}
		if(userRegister.editParam.sscrebate < sscOpenUserLowest){
			userRegister.showKindlyReminder("小于时时彩模式最低值了.请重新输入！");
			return;
		}
		if(userRegister.editParam.sscrebate == sscOpenUserLowest){
			userRegister.showKindlyReminder("当前时时彩模式已经是最低值了.");
			return;
		}
		userRegister
		userRegister.editParam.sscrebate -= sscInterval;
		if(userRegister.editParam.sscrebate < sscOpenUserLowest){
			userRegister.editParam.sscrebate = sscOpenUserLowest;
		}
		$("#sscrebateValue").val(userRegister.editParam.sscrebate);
		$("#sscTip").html('');
		$("#sscTip").append("返点"+userRegister.calculateRebateByAwardModelInterval(sscOpenUserHighest, userRegister.editParam.sscrebate, sscHighestAwardModel)+"%");
	});
	
	$("#sscrebateValue").change(function(){
		var val=$(this).val();
		if(val%2!=0){
			userRegister.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		userRegister.editParam.sscrebate = val;
		if(userRegister.editParam.sscrebate > sscOpenUserHighest){
			userRegister.showKindlyReminder("大于时时彩模式最高值了.请重新输入！");
			return;
		}
		if(userRegister.editParam.sscrebate < sscOpenUserLowest){
			userRegister.showKindlyReminder("小于时时彩模式最低值了.请重新输入！");
			return;
		}
		if(userRegister.editParam.sscrebate == sscOpenUserLowest){
			userRegister.showKindlyReminder("当前时时彩模式已经是最低值了.");
			return;
		}
		$("#sscTip").html('');
		$("#sscTip").append("返点"+userRegister.calculateRebateByAwardModelInterval(sscOpenUserHighest, userRegister.editParam.sscrebate, sscHighestAwardModel)+"%");
	});
	
	//快三模式事件
	$("#ksModeAdd").unbind("click").click(function() {
		var ksrebateValue = Number($("#ksrebateValue").val());
		if(ksrebateValue%2!=0){
			userRegister.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		userRegister.editParam.ksRebate = ksrebateValue;
		if(userRegister.editParam.ksRebate > ksOpenUserHighest){
			userRegister.showKindlyReminder("大于快三模式["+ ksOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(userRegister.editParam.ksRebate < ksOpenUserLowest){
			userRegister.showKindlyReminder("小于快三模式["+ ksOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		if(userRegister.editParam.ksRebate == ksOpenUserHighest){
			userRegister.showKindlyReminder("当前快三模式已经是最高值了.");
			return;
		}
		
		userRegister.editParam.ksRebate += ksInterval;
		if(userRegister.editParam.ksRebate > ksOpenUserHighest){
			userRegister.editParam.ksRebate = ksOpenUserHighest;
		}
		$("#ksrebateValue").val(userRegister.editParam.ksRebate);
		$("#ksTip").html('');
		$("#ksTip").append("返点"+userRegister.calculateRebateByAwardModelInterval(ksOpenUserHighest, userRegister.editParam.ksRebate, ksHighestAwardModel)+"%");
	});
	
	$("#ksModeReduction").unbind("click").click(function() {
		var ksrebateValue = Number($("#ksrebateValue").val());
		if(ksrebateValue%2!=0){
			userRegister.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		userRegister.editParam.ksRebate = ksrebateValue;
		if(userRegister.editParam.ksRebate > ksOpenUserHighest){
			userRegister.showKindlyReminder("大于快三模式最高值了.请重新输入！");
			return;
		}
		if(userRegister.editParam.ksRebate < ksOpenUserLowest){
			userRegister.showKindlyReminder("小于快三模式最低值了.请重新输入！");
			return;
		}
		
		
		userRegister.editParam.ksRebate -= ksInterval;
		if(userRegister.editParam.ksRebate < ksOpenUserLowest){
			userRegister.editParam.ksRebate = ksOpenUserLowest;
		}
		$("#ksrebateValue").val(userRegister.editParam.ksRebate);
		$("#ksTip").html('');
		$("#ksTip").append("返点"+userRegister.calculateRebateByAwardModelInterval(ksOpenUserHighest, userRegister.editParam.ksRebate, ksHighestAwardModel)+"%");
	});
	
	$("#ksrebateValue").change(function(){
		var val=$(this).val();
		if(val%2!=0){
			userRegister.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		userRegister.editParam.ksRebate = val;
		if(userRegister.editParam.ksRebate > ksOpenUserHighest){
			userRegister.showKindlyReminder("大于快三模式["+ ksOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(userRegister.editParam.ksRebate < ksOpenUserLowest){
			userRegister.showKindlyReminder("小于快三模式["+ ksOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		$("#ksTip").html('');
		$("#ksTip").append("返点"+userRegister.calculateRebateByAwardModelInterval(ksOpenUserHighest, userRegister.editParam.ksRebate, ksHighestAwardModel)+"%");
	});
	
	//十一选五模式事件
	$("#syxwModeAdd").unbind("click").click(function() {
		var syxwrebateValue = Number($("#syxwrebateValue").val());
		/*if(syxwrebateValue%2!=0){
			userRegister.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}*/
		userRegister.editParam.syxwRebate = syxwrebateValue;
		if(userRegister.editParam.syxwRebate > syxwOpenUserHighest){
			userRegister.showKindlyReminder("大于十一选五模式["+ syxwOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(userRegister.editParam.syxwRebate < syxwOpenUserLowest){
			userRegister.showKindlyReminder("小于十一选五模式["+ syxwOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		if(userRegister.editParam.syxwRebate == syxwOpenUserHighest){
			userRegister.showKindlyReminder("当前十一选五模式已经是最高值了.");
			return;
		}
		
		userRegister.editParam.syxwRebate += syxwInterval; 
		if(userRegister.editParam.syxwRebate > syxwOpenUserHighest){
			userRegister.editParam.syxwRebate = syxwOpenUserHighest;
		}
		$("#syxwrebateValue").val(userRegister.editParam.syxwRebate);
		$("#syxwTip").html('');
		$("#syxwTip").append("返点"+userRegister.calculateRebateByAwardModelInterval(syxwOpenUserHighest, userRegister.editParam.syxwRebate, syxwHighestAwardModel)+"%");
	});
	
	$("#syxwModeReduction").unbind("click").click(function() {
		var syxwrebateValue = Number($("#syxwrebateValue").val());
		/*if(syxwrebateValue%2!=0){
			userRegister.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}*/
		userRegister.editParam.syxwRebate = syxwrebateValue;
		if(userRegister.editParam.syxwRebate > syxwOpenUserHighest){
			userRegister.showKindlyReminder("大于十一选五模式最高值了.请重新输入！");
			return;
		}
		if(userRegister.editParam.syxwRebate < syxwOpenUserLowest){
			userRegister.showKindlyReminder("小于十一选五模式最低值了.请重新输入！");
			return;
		}
		if(userRegister.editParam.syxwRebate == syxwOpenUserLowest){
			userRegister.showKindlyReminder("当前十一选五模式已经是最低值了.");
			return;
		}
		
		userRegister.editParam.syxwRebate -= syxwInterval;
		if(userRegister.editParam.syxwRebate < syxwOpenUserLowest){
			userRegister.editParam.syxwRebate = syxwOpenUserLowest;
		}
		$("#syxwrebateValue").val(userRegister.editParam.syxwRebate);
		$("#syxwTip").html('');
		$("#syxwTip").append("返点"+userRegister.calculateRebateByAwardModelInterval(syxwOpenUserHighest, userRegister.editParam.syxwRebate, syxwHighestAwardModel)+"%");
	});
	
	$("#syxwrebateValue").change(function(){
		var val=$(this).val();
		/*if(val%2!=0){
			userRegister.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}*/
		userRegister.editParam.syxwRebate = val;
		if(userRegister.editParam.syxwRebate > syxwOpenUserHighest){
			userRegister.showKindlyReminder("大于十一选五模式["+ syxwOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(userRegister.editParam.syxwRebate < syxwOpenUserLowest){
			userRegister.showKindlyReminder("小于十一选五模式["+ syxwOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		$("#syxwTip").html('');
		$("#syxwTip").append("返点"+userRegister.calculateRebateByAwardModelInterval(syxwOpenUserHighest, userRegister.editParam.syxwRebate, syxwHighestAwardModel)+"%");
	});
	
	//pk10模式事件
	$("#pk10ModeAdd").unbind("click").click(function() {
		var pk10rebateValue = Number($("#pk10rebateValue").val());
		if(pk10rebateValue%2!=0){
			userRegister.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		userRegister.editParam.pk10Rebate = pk10rebateValue;
		if(userRegister.editParam.pk10Rebate > pk10OpenUserHighest){
			userRegister.showKindlyReminder("大于PK10模式["+ pk10OpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(userRegister.editParam.pk10Rebate < pk10OpenUserLowest){
			userRegister.showKindlyReminder("小于PK10模式["+ pk10OpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		if(userRegister.editParam.pk10Rebate == pk10OpenUserHighest){
			userRegister.showKindlyReminder("当前PK10模式已经是最高值了.");
			return;
		}
		
		userRegister.editParam.pk10Rebate += pk10Interval; 
		if(userRegister.editParam.pk10Rebate > pk10OpenUserHighest){
			userRegister.editParam.pk10Rebate = pk10OpenUserHighest;
		}
		$("#pk10rebateValue").val(userRegister.editParam.pk10Rebate);
		$("#pk10Tip").html('');
		$("#pk10Tip").append("返点"+userRegister.calculateRebateByAwardModelInterval(pk10OpenUserHighest, userRegister.editParam.pk10Rebate, pk10HighestAwardModel)+"%");
	});
	
	$("#pk10ModeReduction").unbind("click").click(function() {
		var pk10rebateValue = Number($("#pk10rebateValue").val());
		if(pk10rebateValue%2!=0){
			userRegister.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		userRegister.editParam.pk10Rebate = pk10rebateValue;
		if(userRegister.editParam.pk10Rebate > pk10OpenUserHighest){
			userRegister.showKindlyReminder("大于PK10模式最高值了.请重新输入！");
			return;
		}
		if(userRegister.editParam.pk10Rebate < pk10OpenUserLowest){
			userRegister.showKindlyReminder("小于PK10模式最低值了.请重新输入！");
			return;
		}
		if(userRegister.editParam.pk10Rebate == pk10OpenUserLowest){
			userRegister.showKindlyReminder("当前PK10模式已经是最低值了.");
			return;
		}
		
		userRegister.editParam.pk10Rebate -= pk10Interval;
		if(userRegister.editParam.pk10Rebate < pk10OpenUserLowest){
			userRegister.editParam.pk10Rebate = pk10OpenUserLowest;
		}
		$("#pk10rebateValue").val(userRegister.editParam.pk10Rebate);
		$("#pk10Tip").html('');
		$("#pk10Tip").append("返点"+userRegister.calculateRebateByAwardModelInterval(pk10OpenUserHighest, userRegister.editParam.pk10Rebate, pk10HighestAwardModel)+"%");
	});
	
	$("#pk10rebateValue").change(function(){
		var val=$(this).val();
		if(val%2!=0){
			userRegister.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		userRegister.editParam.pk10Rebate = val;
		if(userRegister.editParam.pk10Rebate > pk10OpenUserHighest){
			userRegister.showKindlyReminder("大于PK10模式["+ pk10OpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(userRegister.editParam.pk10Rebate < pk10OpenUserLowest){
			userRegister.showKindlyReminder("小于PK10模式["+ pk10OpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		$("#pk10Tip").html('');
		$("#pk10Tip").append("返点"+userRegister.calculateRebateByAwardModelInterval(pk10OpenUserHighest, userRegister.editParam.pk10Rebate, pk10HighestAwardModel)+"%");
	});
	
	//低频彩模式事件
	$("#dpcModeAdd").unbind("click").click(function() {
		var dpcrebateValue = Number($("#dpcrebateValue").val());
		if(dpcrebateValue%2!=0){
			userRegister.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		userRegister.editParam.dpcRebate = dpcrebateValue;
		if(userRegister.editParam.dpcRebate > dpcOpenUserHighest){
			userRegister.showKindlyReminder("大于低频彩模式["+ dpcOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(userRegister.editParam.dpcRebate < dpcOpenUserLowest){
			userRegister.showKindlyReminder("小于低频彩模式["+ dpcOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		if(userRegister.editParam.dpcRebate == dpcOpenUserHighest){
			userRegister.showKindlyReminder("当前低频彩模式已经是最高值了.");
			return;
		}
		
		userRegister.editParam.dpcRebate += dpcInterval; 
		if(userRegister.editParam.dpcRebate > dpcOpenUserHighest){
			userRegister.editParam.dpcRebate = dpcOpenUserHighest ;
		}
		$("#dpcrebateValue").val(userRegister.editParam.dpcRebate);
		$("#dpcTip").html('');
		$("#dpcTip").append("返点"+userRegister.calculateRebateByAwardModelInterval(dpcOpenUserHighest,userRegister.editParam.dpcRebate, dpcHighestAwardModel)+"%");
	});
	
	//
	$("#dpcModeReduction").unbind("click").click(function() {
		var dpcrebateValue = Number($("#dpcrebateValue").val());
		if(dpcrebateValue%2!=0){
			userRegister.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		userRegister.editParam.dpcRebate = dpcrebateValue;
		if(userRegister.editParam.dpcRebate > dpcOpenUserHighest){
			userRegister.showKindlyReminder("大于低频彩模式最高值了.请重新输入！");
			return;
		}
		if(userRegister.editParam.dpcRebate < dpcOpenUserLowest){
			userRegister.showKindlyReminder("小于低频彩模式最低值了.请重新输入！");
			return;
		}
		if(userRegister.editParam.dpcRebate == dpcOpenUserLowest){
			userRegister.showKindlyReminder("当前低频彩模式已经是最低值了.");
			return;
		}
		
		userRegister.editParam.dpcRebate -= dpcInterval;
		$("#dpcrebateValue").val(userRegister.editParam.dpcRebate);
		$("#dpcTip").html('');
		$("#dpcTip").append("返点"+userRegister.calculateRebateByAwardModelInterval(dpcOpenUserHighest,userRegister.editParam.dpcRebate, dpcHighestAwardModel)+"%");
	});
	
	$("#dpcrebateValue").change(function(){
		var val=$(this).val();
		if(val%2!=0){
			userRegister.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		userRegister.editParam.dpcRebate = val;
		if(userRegister.editParam.dpcRebate > dpcOpenUserHighest){
			userRegister.showKindlyReminder("大于低频彩模式["+ dpcOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(userRegister.editParam.dpcRebate < dpcOpenUserLowest){
			userRegister.showKindlyReminder("小于低频彩模式["+ dpcOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		$("#dpcTip").html('');
		$("#dpcTip").append("返点"+userRegister.calculateRebateByAwardModelInterval(dpcOpenUserHighest,userRegister.editParam.dpcRebate, dpcHighestAwardModel)+"%");
	});
	
});



/**
 * 获取本系统的最高最低模式
 */
UserRegister.prototype.loadUserRebate = function(){
	managerUserRegisterAction.loadUserRebate(function(r){
		if (r[0] != null && r[0] == "ok") {
			sscOpenUserLowest=r[1].sscLowestAwardModel;
			syxwOpenUserLowest=r[1].syxwLowestAwardModel;
			dpcOpenUserLowest=r[1].dpcLowestAwardModel;
			pk10OpenUserLowest=r[1].pk10LowestAwardModel;
			ksOpenUserLowest=r[1].ksLowestAwardModel;
		}else if(r[0] != null && r[0] == "error"){
			//加载为空不处理
		}else{
			showErrorDlg(jQuery(document.body), "加载用户配额信息请求失败.");
		}
	});
}


/**
 * 获取推广链接数据 
 */
UserRegister.prototype.getRegisterLink = function(){
	managerUserRegisterAction.getRegisterLink(userRegister.editParam.id,function(r) {
		if (r[0] != null && r[0] == "ok") {
			userRegister.showRegisterDetail(r[1] , r[2] , r[3]);
		} else if (r[0] != null && r[0] == "error") {
			userRegister.showKindlyReminder(r[1]);
		} else {
			showErrorDlg(jQuery(document.body), "获取系统可用域名请求失败.");
		}
	});
};



/**
 * 修改推广链接数据 
 */
UserRegister.prototype.updateRegisterLink = function(){
	var p=$("#invitationCode").val();
	if(!p.match( /\d{8}$/)){
		userRegister.showKindlyReminder("邀请码只能是8位数的数字");
		return;
	}
	if(p==""){
		userRegister.showKindlyReminder("邀请码不能为空");
		return;
	}
	userRegister.editParam.invitationCode=$("#invitationCode").val();
	userRegister.editParam.sscRebate = $("#sscrebateValue").val();
	userRegister.editParam.syxwRebate = $("#syxwrebateValue").val();
	userRegister.editParam.ksRebate = $("#ksrebateValue").val();
	userRegister.editParam.pk10Rebate = $("#pk10rebateValue").val();
	userRegister.editParam.dpcRebate = $("#dpcrebateValue").val();
	managerUserRegisterAction.updateUserRegisterLink(userRegister.editParam,function(r) {
		if (r[0] != null && r[0] == "ok") {
			swal({ title: "提示", text: "保存成功",type: "success"},function(){
				var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
				parent.userRegister.getUserRegisterQuery();
				parent.layer.close(index);
			});
		} else if (r[0] != null && r[0] == "error") {
			userRegister.showKindlyReminder(r[1]);
		} else {
			showErrorDlg(jQuery(document.body), "获取系统可用域名请求失败.");
		}
	});
};


/**
 * 展示注册详情的信息
 */
UserRegister.prototype.showRegisterDetail = function(register,pcDomains,mobileDomains) {
		var register = register;
		var user=pcDomains;
		sscOpenUserHighest =user.sscRebate;
		syxwOpenUserHighest =user.syxwRebate; 
		dpcOpenUserHighest =user.dpcRebate;
		pk10OpenUserHighest =user.pk10Rebate;
		ksOpenUserHighest =user.ksRebate;
		
		$("#invitationCode").val(register.invitationCode);
		if(user.isTourist==1){
			$("#invitationCode").attr("disabled","disabled");
		}
		// 时时彩
		if (register.sscRebate != "-") {
			userRegister.editParam.sscRebate = register.sscRebate;
			$("#sscrebateValue").val(register.sscRebate);
			$("#sscTip").html('');
			$("#sscTip").append("返点"+userRegister.calculateRebateByAwardModelInterval(sscOpenUserHighest, register.sscRebate, sscHighestAwardModel)+"%");
		} else {
			$("#sscrebateValue").text(register.sscRebate);
		}

		// 十一选五
		if (userRegister.syxwRebate != "-") {
			userRegister.editParam.syxwRebate = register.syxwRebate;
			$("#syxwTip").html('');
			$("#syxwTip").append("返点"+userRegister.calculateRebateByAwardModelInterval(syxwOpenUserHighest, register.syxwRebate, syxwHighestAwardModel)+"%");
			$("#syxwrebateValue").val(register.syxwRebate);
		} else {
			$("#syxwrebateValue").val(register.syxwRebate);
		}
		
		// 快三模式
		if (register.ksRebate != "-") {
			userRegister.editParam.ksRebate = register.ksRebate;
			$("#ksTip").html('');
			$("#ksTip").append("返点"+userRegister.calculateRebateByAwardModelInterval(ksOpenUserHighest, register.ksRebate, ksHighestAwardModel)+"%");
			$("#ksrebateValue").val(register.ksRebate);
		} else {
			$("#ksrebateValue").val(register.ksRebate);
		}
		
		// PK10模式
		if (register.pk10Rebate != "-") {
			userRegister.editParam.pk10Rebate = register.pk10Rebate;
			$("#pk10Tip").html('');
			$("#pk10Tip").append("返点"+userRegister.calculateRebateByAwardModelInterval(pk10OpenUserHighest, register.pk10Rebate, pk10HighestAwardModel)+"%");
			$("#pk10rebateValue").val(register.pk10Rebate);
		} else {
			$("#pk10rebateValue").val(register.pk10Rebate);
		}
		
		// 低频彩模式
		if (register.dpcRebate != "-") {
			userRegister.editParam.dpcRebate = register.dpcRebate;
			$("#dpcTip").html('');
			$("#dpcTip").append("返点"+userRegister.calculateRebateByAwardModelInterval(dpcOpenUserHighest, register.dpcRebate, dpcHighestAwardModel)+"%");
			$("#dpcrebateValue").val(register.dpcRebate);
		} else {
			$("#dpcrebateValue").val(register.dpcRebate);
		}

};

/**
 * 打开温馨提示框
 * @param msg
 */
UserRegister.prototype.showKindlyReminder = function(msg){
	//不存在的时候
	if($(".alert-msg-bg").length <= 0) {
		$("body").append("<div class='alert-msg-bg'></div>");
	}
	var msgStr = "<div class='alert-msg' id='lotteryMsgPopup'>";
	msgStr += "		<div class='msg-head'>";
	msgStr += "			<p>温馨提示</p><img class='close' src='"+contextPath+"/img/close.png' />";
	msgStr += "		</div>";
	msgStr += "		<div class='msg-content'>";
	msgStr += " 		<div class='line no-border'>";
	msgStr += msg;
	msgStr += "			</div>";
	msgStr += "		</div>";
	msgStr += "	  </div>";
	if($("#lotteryMsgPopup").length > 0) {
		$("#lotteryMsgPopup").remove();
	}
	$("body").append(msgStr);
	//绑定关闭事件
	$(".close").click(function(){
		$('#lotteryMsgPopup').stop(false,true).delay(200).fadeOut(500);
		$('.alert-msg-bg').stop(false,true).fadeOut(500);
	});
	$("#lotteryMsgPopup").stop(false,true).fadeIn(500);
	$(".alert-msg-bg").stop(false,true).delay(200).fadeIn(500);
};

/**
 * 关闭温馨提示框
 * @param msg
 */
UserRegister.prototype.closeKindlyReminder = function(msg){
	$('#lotteryMsgPopup').stop(false,true).delay(200).fadeOut(500);
	$('.alert-msg-bg').stop(false,true).fadeOut(500);
};

/**
 * 计算用户自身保留返点
 */
UserRegister.prototype.calculateRebateByAwardModelInterval = function(highRebate, lowRebate, highestAwardModel) {
	var rebate = (highRebate - lowRebate)/highestAwardModel;
	rebate = (rebate * 100).toFixed(2);
	return rebate;
}
