<%@ page language="java" contentType="text/html; charset=UTF-8"
	import="com.team.lottery.util.ConstantUtil,com.team.lottery.vo.Admin,java.math.BigDecimal"
	import="com.team.lottery.service.LotteryCoreService"
	import="com.team.lottery.enums.ELotteryTopKind"
	import="com.team.lottery.util.ApplicationContextUtil,com.team.lottery.system.SystemConfigConstant"
	import="com.team.lottery.util.StringUtils" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	Admin admin = (Admin)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_ADMIN);
    //计算时时彩的可调节的返奖率区间
  	BigDecimal sscInterval = new BigDecimal("2");
  	BigDecimal syxwInterval = new BigDecimal("2");
  	BigDecimal dpcInterval = new BigDecimal("2");
  	BigDecimal pk10Interval = new BigDecimal("2");
  	BigDecimal ksInterval = new BigDecimal("2");
  	
  	//商户最高模式值
  	BigDecimal sscHighestAwardModel = SystemConfigConstant.SSCHighestAwardModel;
  	BigDecimal syxwHighestAwardModel = SystemConfigConstant.SYXWHighestAwardModel;
  	BigDecimal dpcHighestAwardModel = SystemConfigConstant.DPCHighestAwardModel;
  	BigDecimal pk10HighestAwardModel = SystemConfigConstant.PK10HighestAwardModel;
  	BigDecimal ksHighestAwardModel = SystemConfigConstant.KSHighestAwardModel;
%>

<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>推广编辑</title>
<jsp:include page="/admin/include/include.jsp"></jsp:include>
<!-- 业务js -->
<script type="text/javascript"	src="<%=path%>/admin/user_register/js/user_register_edit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<script type='text/javascript' src='<%=path%>/dwr/interface/managerUserRegisterAction.js'></script>
<style type="text/css">
	.row p{
		position: absolute;
    	top: 32px;
	}
	.alert-msg-bg{display:none;position:fixed;top:0px;bottom:0px;left:0px;right:0px;z-index:8;background:#000; opacity:0.4;}
	.alert-msg{display:none;position:fixed;top:50%;right:0px;left:0px;margin:-200px auto 0 auto;z-index:9;width:500px;min-height:initial;background:#fff;box-shadow: 0px 4px 6px 0px rgba(0,0,0,0.4);-webkit-box-shadow: 0px 4px 6px 0px rgba(0,0,0,0.4);}
	.alert-msg .msg-head{position:relative;height:60px;line-height:60px;background:#3088ff;}
	.alert-msg .msg-head p{color:#fff;font-size:18px;padding-left:28px;}
	.alert-msg .msg-head .close{position:absolute;top:0px;bottom:0px;right:20px;margin:auto 0;cursor:pointer;}
	.alert-msg .msg-content{padding:26px 28px; box-sizing:border-box;}
	.alert-msg .msg-content .msg-title{line-height:26px;color:#676767;font-size:14px;padding-bottom:16px;}
	.alert-msg .msg-content .info{line-height:24px;padding:14px 0;color:#676767;font-size:13px;}
	.alert-msg .msg-content .info .blue{color:#006CFF;}
	.alert-msg .msg-content .btn{text-align:center;}
	.alert-msg .msg-content .btn .inputBtn{transition-duration: .3s;-webkit-transition-duration:.3s;cursor:pointer;font-size:18px;color:#fff;font-family:"Microsoft YaHei" ,Helvetica Neue,Tahoma,Arial,"寰蒋闆呴粦","瀹嬩綋","榛戜綋";background:#388cff;border:0px;width:170px;height:49px;border-radius:3px;-webkit-border-radius:3px;}
	.alert-msg .msg-content .btn .inputBtn:hover{background:#1577FD;}
	.alert-msg .msg-content .btn .inputBtn.gray{background:#B1B1B1;}
	.alert-msg .msg-content .btn .inputBtn.gray:hover{background:#B1B1B1;}
	
	.clear:after{content:'';display:table;clear:both;}
	.input-group-btn{float:left;width:auto;}
	#sscrebateValue, #ksrebateValue, #syxwrebateValue, #pk10rebateValue, #dpcrebateValue{float:left;width:80%;}
	.col-sm-2 span{display: inline-block;line-height: 34px;}
	.btn-sm{margin-top:20px;}
</style>
</head>
<%
String linkId= request.getParameter("id");  //获取推广链接ID
%>
<script type="text/javascript">
	//时时彩
	userRegister.param.linkId = <%=linkId %>;
	userRegister.editParam.id = <%=linkId %>;
	var sscOpenUserLowest = null;
	var syxwOpenUserLowest = null;
	var dpcOpenUserLowest = null;
	var pk10OpenUserLowest = null;
	var ksOpenUserLowest = null;
	var sscOpenUserHighest =null;
	var syxwOpenUserHighest =null; 
	var dpcOpenUserHighest =null;  
	var pk10OpenUserHighest =null;  
	var ksOpenUserHighest =null;  
	var sscHighestAwardModel = <%=sscHighestAwardModel%>;
	var syxwHighestAwardModel = <%=syxwHighestAwardModel %>;
	var dpcHighestAwardModel = <%=dpcHighestAwardModel %>;
	var pk10HighestAwardModel = <%=pk10HighestAwardModel%>;
	var ksHighestAwardModel = <%=ksHighestAwardModel%>;
	
	var sscInterval = <%=sscInterval %>;
	var syxwInterval = <%=syxwInterval %>;
	var ksInterval = <%=ksInterval %>;
	var pk10Interval = <%=pk10Interval %>;
	var dpcInterval = <%=dpcInterval %>;
  
</script>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeIn">
		<!-- main -->
			<div class="row  back-change">
            	<div class="col-sm-12">
            		<div class="ibox ">
                       <div class="ibox-content">
                           <form class="form-horizontal">
                           	   <div class="form-group">
                                   <label class="col-sm-2 control-label">邀请码：</label>
                                   <div class="col-sm-10">
                                      <span class="input-group-btn"></span>
			                          <input type="text" class="form-control" id="invitationCode">
			                       </div>
                               	</div>
                           		<div class="form-group">
                                   <label class="col-sm-2 control-label">时时彩模式：</label>
                                   <div class="col-sm-8 clear">
                                   	   <span class="input-group-btn">
				                           <button class="btn btn-primary" id="sscModeAdd" type="button">
				                              <i class="fa fa-plus"></i>
				                           </button>
				                       </span>
				                       <input type="text" class="form-control" id="sscrebateValue">
					                   <span class="input-group-btn">
					                       <button class="btn btn-primary" id="sscModeReduction" type="button">
					                              <i class="fa fa-minus"></i>
					                       </button>
					                   </span>
                                   </div>
                                   <div class="col-sm-2 text-center">
                                       <span id="sscTip"></span>
                                   </div>
                               </div>
                               <div class="form-group">
                                   <label class="col-sm-2 control-label">快三模式：</label>
                                   <div class="col-sm-8 clear">
                                   	  <span class="input-group-btn">
			                             <button class="btn btn-primary" id="ksModeAdd" type="button">
			                                  <i class="fa fa-plus"></i>
			                             </button>
			                          </span>
			                          <input type="text" class="form-control" id="ksrebateValue">
			                          <span class="input-group-btn">
			                             <button class="btn btn-primary" id="ksModeReduction" type="button">
			                                  <i class="fa fa-minus"></i>
			                             </button>
			                          </span>
                                   </div>
                                   <div class="col-sm-2 text-center">
                                       <span id="ksTip"></span>
                                   </div>
                               </div>
                               <div class="form-group">
                                   <label class="col-sm-2 control-label">十一选五模式：</label>
                                   <div class="col-sm-8 clear">
                                   	  <span class="input-group-btn">
			                             <button class="btn btn-primary" id="syxwModeAdd" type="button">
			                                  <i class="fa fa-plus"></i>
			                             </button>
			                          </span>
			                          <input type="text" class="form-control" id="syxwrebateValue">
			                          <span class="input-group-btn">
			                             <button class="btn btn-primary" id="syxwModeReduction" type="button">
			                                  <i class="fa fa-minus"></i>
			                             </button>
			                          </span>
                                   </div>
                                   <div class="col-sm-2 text-center">
                                       <span id="syxwTip"></span>
                                   </div>
                               </div>
                               <div class="form-group">
                                   <label class="col-sm-2 control-label">PK10模式：</label>
                                   <div class="col-sm-8">
                                   	  <span class="input-group-btn">
			                             <button class="btn btn-primary" id="pk10ModeAdd" type="button">
			                                  <i class="fa fa-plus"></i>
			                             </button>
			                          </span>
			                          <input type="text" class="form-control" id="pk10rebateValue">
			                          <span class="input-group-btn">
			                             <button class="btn btn-primary" id="pk10ModeReduction" type="button">
			                                  <i class="fa fa-minus"></i>
			                             </button>
			                          </span>
                                   </div>
                                   <div class="col-sm-2 text-center">
                                       <span id="pk10Tip"></span>
                                   </div>
                               </div>
                               <div class="form-group">
                                   <label class="col-sm-2 control-label">低频彩模式：</label>
                                   <div class="col-sm-8">
                                   	  <span class="input-group-btn">
			                             <button class="btn btn-primary" id="dpcModeAdd" type="button">
			                                  <i class="fa fa-plus"></i>
			                             </button>
			                          </span>
			                          <input type="text" class="form-control" id="dpcrebateValue">
			                          <span class="input-group-btn">
			                             <button class="btn btn-primary" id="dpcModeReduction" type="button">
			                                  <i class="fa fa-minus"></i>
			                             </button>
			                          </span>
                                   </div>
                                   <div class="col-sm-2 text-center">
                                       <span id="dpcTip"></span>
                                   </div>
                               </div>
                               <div class="form-group">
		                            <div class="col-sm-12 ibox">
		                                <div class="row">
		                                    <div class="col-sm-12 text-center">
		                                        <button type="button" class="btn btn-primary btn-sm" id="addExtendLinkButton">
									            &nbsp;&nbsp;修改&nbsp;&nbsp;
									            </button>
		                                    </div>
		                                </div>
		                            </div>
		                        </div>
                			</form>
            			</div>
            
            		</div>
            	</div>
			</div>
		</div>
</body>
</html>
