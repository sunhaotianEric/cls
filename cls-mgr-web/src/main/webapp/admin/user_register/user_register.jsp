<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>推广管理</title>
<jsp:include page="/admin/include/include.jsp"></jsp:include>

</head>
<body>
	<div class="animated fadeIn">
		<div class="ibox-content ibox">
			<div class="row m-b-sm m-t-sm">
				<form class="form-horizontal" id="userRegisterForm">
					<div class="row">
						<c:choose>
							<c:when test="${admin.bizSystem=='SUPER_SYSTEM'}">
								<div class="col-sm-3">
									<div class="input-group m-b">
										<span class="input-group-addon">商户</span>
										<cls:bizSel name="bizSystem" id="bizSystem"
											options="class:ipt form-control" />
									</div>
								</div>
								<div class="col-sm-2">
									<div class="input-group m-b">
										<span class="input-group-addon">用户名</span> <input type="text"
											value="" name="userName" id="userName" class="form-control">
									</div>
								</div>
								<div class="col-sm-2">
									<div class="input-group m-b">
										<span class="input-group-addon">邀请码</span> <input type="text"
											value="" name="invitationCode" id="invitationCode" class="form-control">
									</div>
								</div>
							</c:when>
							<c:otherwise>
								<div class="col-sm-4">
									<div class="input-group m-b">
										<span class="input-group-addon">用户名</span> <input type="text"
											value="" name="userName" id="userName" class="form-control">
									</div>
								</div>
								<div class="col-sm-2">
									<div class="input-group m-b">
										<span class="input-group-addon">邀请码</span> <input type="text"
											value="" name="invitationCode" id="invitationCode" class="form-control">
									</div>
								</div>
								<!-- <div class="col-sm-2"></div> -->
							</c:otherwise>
						</c:choose>

						<div class="col-sm-2">
							<div class="form-group nomargin text-left">
								<button type="button" class="btn btn-outline btn-default"
									onclick="userRegister.getUserRegisterQuery()">
									<i class="fa fa-search"></i>&nbsp;查 询
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="jqGrid_wrapper">
				<div class="ui-jqgrid ">
					<div class="ui-jqgrid-view ">
						<div class="ui-jqgrid-hdiv">
							<div class="ui-jqgrid-hbox" style="padding-right: 0">
								<table id="userRegisterTable"
									class="ui-jqgrid-htable ui-common-table table table-bordered">
									<thead>
										<tr class="ui-jqgrid-labels">
											<th class="ui-th-column ui-th-ltr">商户</th>
											<th class="ui-th-column ui-th-ltr">用户名</th>
											<th class="ui-th-column ui-th-ltr">邀请码</th>
											<th class="ui-th-column ui-th-ltr">开户类型</th>
											<th class="ui-th-column ui-th-ltr">时时彩</th>
											<th class="ui-th-column ui-th-ltr">快三</th>
											<th class="ui-th-column ui-th-ltr">十一选五</th>
											<th class="ui-th-column ui-th-ltr">PK10</th>
											<th class="ui-th-column ui-th-ltr">低频彩</th>
											<th class="ui-th-column ui-th-ltr">次数</th>
											<th class="ui-th-column ui-th-ltr">生成时间</th>
											<th class="ui-th-column ui-th-ltr">操作</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- js -->
<script type="text/javascript" src="<%=path%>/admin/user_register/js/user_register.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerUserRegisterAction.js'></script>
<script type='text/javascript' src='<%=path%>/dwr/interface/managerUserAction.js'></script>
</body>
</html>

