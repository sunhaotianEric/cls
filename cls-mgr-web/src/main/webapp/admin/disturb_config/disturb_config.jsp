<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<html>
<head>
    <meta charset="utf-8" />
    <title>干扰参数设置</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/disturb_config/js/disturb_config.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerLotteryDisturbConfigAction.js'></script>
    
     <%
    	String id = request.getParameter("id");  //获取查询ID
    %>
	<script type="text/javascript">
		var tableId = <%=id%>;
	</script>
</head>
<body>
   <div class="alert alert-info">当前位置<b class="tip"></b>网站信息管理<b class="tip"></b>干扰参数设置</div>
   
<!--    <div align="left">
       <input class="btn" id="find" type="button" value="提交" onclick="userBounsConfigPage.saveTreatmentConfig()" />
   </div> -->
    <br/>
   	
        <table class="tbform list" id="dayActivityTable">
        <tbody>
            <tr>
	         	<td colspan="18"> 干扰参数设置表  &nbsp;&nbsp; <a href="<%=path %>/managerzaizst/disturb_config/disturb_config_edit.html">新增干扰配置设置</a></td>
	        </tr> 
	        <tr>
			   <th>序号</th>
			   <th>干扰模式</th>
			   <th>用户名</th>
			   <th>投注金额</th>
			   <th>投注彩种</th>
			   <th>干扰期号集合</th>
			   <th>干扰彩种</th>
			   <th>干扰提示</th>
			   <th>是否生效</th>
			   <th>操作</th>
			</tr>
             
        </tbody>
        <tbody id="lotteryDisturbCconfigsList">
      	</tbody>  
    </table>  
   	
</body>
</html>

