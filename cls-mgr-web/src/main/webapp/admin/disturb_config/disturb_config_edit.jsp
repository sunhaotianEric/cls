<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.enums.ELotteryKind" %>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<html>
<head>
    <meta charset="utf-8" />
    <title>新增干扰参数设置</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/disturb_config/js/disturb_config_edit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
   <script type='text/javascript' src='<%=path%>/dwr/interface/managerLotteryDisturbConfigAction.js'></script>
    <%
    	String id = request.getParameter("id");  //获取查询ID
    %>
	<script type="text/javascript">
		lotteryDisturbConfigEditPage.param.id = <%=id%>;
	</script>
</head>
<body>
   <div class="alert alert-info">当前位置<b class="tip"></b>平台待遇设置<b class="tip"></b><span id="addOrEditSpan">新增</span>干扰参数设置</div>
   	
   	<table class="tb">
         <tr>
         	<td> <a href="<%=path%>/managerzaizst/disturb_config.html">返回列表</a>
         </tr>
    </table>
    <form action="javascript:void(0)">
	    <table class="tb">
	   		<tr>
	   			<td width="120px">干扰模式：<input type="hidden" id="disturbShowId"/></td>
	   			<td>
	   				<select class="select" name="disturbModel" id="disturbModel">
                         <option value='1'>下注显示干扰</option>
                         <option value='2'>下注彩种改变</option>
                         <option value='3'>下注提示干扰</option>
                    </select>
	   			</td>
	   		</tr>
	   		<tr>
	   			<td>投注彩种：</td>
	   			<td>
	   			   <select class="select" id="lotteryType">
	   			   		<%	ELotteryKind[] lotteryKinds = ELotteryKind.values();
	   			   			 for(ELotteryKind lotteryKind : lotteryKinds){
   			   			%>
   			   					<option value='<%=lotteryKind.getCode() %>'> <%=lotteryKind.getDescription() %> </option>
   			   			<%
   			   				}
   			   			%>
	   			   </select>
	   			</td>
	   		</tr>
	   		<tr>
	   			<td>干扰彩种：</td>
	   			<td>
	   			   <select class="select" id="disturbLotteryType">
	   			   		<%	ELotteryKind[] lotteryKinds2 = ELotteryKind.values();
	   			   			 for(ELotteryKind lotteryKind : lotteryKinds2){
   			   			%>
   			   					<option value='<%=lotteryKind.getCode() %>'> <%=lotteryKind.getDescription() %> </option>
   			   			<%
   			   				}
   			   			%>
	   			   </select>
	   			</td>
	   		</tr>
	   		<tr>
	   			<td>用户名：</td>
	   			<td>
	   			   <input type="text" id="userName"/>
	   			</td>
	   		</tr>
	   		<tr>
	   			<td>投注金额：</td>
	   			<td>
	   			   <input type="text" id="money" onkeyup="checkNum(this)"/>
	   			</td>
	   		</tr>
	   		<tr>
	   			<td>干扰期号集合：</td>
	   			<td>
		   			<input type="text" id="expectsets"/>
	   			</td>
	   		</tr>
	   		<tr>
	   			<td>干扰提示：</td>
	   			<td>
	   			  <input type="text" id="disturbTip"/>
	   			</td>
	   		</tr>
	   		<tr>
	   			<td>是否生效：</td>
	   			<td>
	   			   <select class="ipt" id="enableSwitch">
	                    <option value="1" selected="selected">开启</option>
	                    <option value="0">关闭</option>
	                </select>
	   			</td>
	   		</tr>
	   		<tr>
	   			<td></td>
	   			<td><input type="button" onclick="lotteryDisturbConfigEditPage.saveData()" value="提交"/>&nbsp;&nbsp;&nbsp;&nbsp;<input type="reset" value="重置"/></td>
	   		</tr>
	    </table>
    </form>
</body>
</html>

