function LotteryDisturbCconfigPage(){}
var lotteryDisturbCconfigPage = new LotteryDisturbCconfigPage();

lotteryDisturbCconfigPage.param = {
	   	
};

$(document).ready(function(){
	//查询特权配置数据
	lotteryDisturbCconfigPage.getAllUserVipLevelConfigs();
});

/**
 * 加载所有的特权配置
 */
LotteryDisturbCconfigPage.prototype.getAllUserVipLevelConfigs = function(){
	managerLotteryDisturbConfigAction.getLotteryDisturbConfig(function(r){
		if (r[0] != null && r[0] == "ok") {
			lotteryDisturbCconfigPage.refreshLotteryDisturbCconfigPages(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询特权配置数据失败.");
		}
    });
};

LotteryDisturbCconfigPage.prototype.refreshLotteryDisturbCconfigPages = function(lotteryDisturbCconfigs){
	var lotteryDisturbCconfigsListObj = $("#lotteryDisturbCconfigsList");
	lotteryDisturbCconfigsListObj.html("");
	var lotteryDisturbCconfigsDatas = new Array();
	var str = "";
	for(var i = 0 ; i < lotteryDisturbCconfigs.length; i++){
		var lotteryDisturbCconfig = lotteryDisturbCconfigs[i];
		str += "<tr>";
		str += "  <td>"+(i+1)+"</td>";
		if(lotteryDisturbCconfig.disturbModel == 1){
			str += "  <td>下注显示干扰</td>";
		}else if(lotteryDisturbCconfig.disturbModel == 2){
			str += "  <td>下注彩种改变</td>";
		}else if(lotteryDisturbCconfig.disturbModel == 3){
			str += "  <td>下注提示干扰</td>";
		}
		str += "  <td>"+ lotteryDisturbCconfig.userName +"</td>";
		str += "  <td>"+ lotteryDisturbCconfig.money +"</td>";
		str += "  <td>"+ lotteryDisturbCconfig.lotteryTypeStr +"</td>";
		str += "  <td>"+ lotteryDisturbCconfig.expectsets +"</td>";
		str += "  <td>"+ lotteryDisturbCconfig.disturbLotteryTypeStr +"</td>";
		str += "  <td>"+ lotteryDisturbCconfig.disturbTip +"</td>";
		
		if(lotteryDisturbCconfig.enable == 1){
			str += "  <td>开启</td>";
		}else{
			str += "  <td>关闭</td>";
		}
		str += "  <td><a href='javascript:void(0)' onclick='lotteryDisturbCconfigPage.editLotteryDisturbCconfig("+ lotteryDisturbCconfig.id +")'>编辑</a>&nbsp;|&nbsp;<a href='javascript:void(0)' onclick='lotteryDisturbCconfigPage.delLotteryDisturbCconfig("+ lotteryDisturbCconfig.id +")'>删除</a></td>";
		str += "</tr>";
		
		lotteryDisturbCconfigsListObj.append(str);
		str = "";
	}

/**
 * 编辑特权配置
 */
LotteryDisturbCconfigPage.prototype.editLotteryDisturbCconfig = function(id){
	self.location = contextPath + "/managerzaizst/disturb_config/" + id +"/disturb_config_edit.html";
};

/**
 * 删除干扰配置
 */
LotteryDisturbCconfigPage.prototype.delLotteryDisturbCconfig = function(id){
	managerLotteryDisturbConfigAction.delLotteryDisturbCconfigById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			alert("删除成功！");
			//查询特权配置数据
			lotteryDisturbCconfigPage.getAllUserVipLevelConfigs();
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询特权配置数据失败.");
		}
    });
};
	
}