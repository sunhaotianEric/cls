function LotteryDisturbConfigEditPage(){}
var lotteryDisturbConfigEditPage = new LotteryDisturbConfigEditPage();

lotteryDisturbConfigEditPage.param = {
   	
};

$(document).ready(function() {
	var lotteryDisturbConfigId = lotteryDisturbConfigEditPage.param.id;  //获取配置ID
	if(lotteryDisturbConfigId != null){
		$("#addOrEditSpan").html("编辑");
		lotteryDisturbConfigEditPage.editLotteryDisturbConfig(lotteryDisturbConfigId);
	}
});


/**
 * 保存干扰配置
 */
LotteryDisturbConfigEditPage.prototype.saveData = function(){
	var	id = $("#disturbShowId").val();
	var	disturbModel = $("#disturbModel").val();
	var	userName = $("#userName").val();
	var money = $("#money").val();
	var lotteryType =	$("#lotteryType").val();
	var expectsets = $("#expectsets").val();
	var disturbLotteryType =	$("#disturbLotteryType").val();
	var disturbTip =	$("#disturbTip").val();
	var enable =	$("#enableSwitch").val();
	
	var lotteryDisturbConfig = {};
	lotteryDisturbConfig.id	= id;
	lotteryDisturbConfig.disturbModel = disturbModel;
	lotteryDisturbConfig.userName = userName;
	lotteryDisturbConfig.money = money;
	lotteryDisturbConfig.lotteryType = lotteryType;
	lotteryDisturbConfig.expectsets = expectsets;
	lotteryDisturbConfig.disturbLotteryType = disturbLotteryType;
	lotteryDisturbConfig.disturbTip = disturbTip;
	lotteryDisturbConfig.enable = enable;
	
	managerLotteryDisturbConfigAction.saveLotteryDisturbConfig(lotteryDisturbConfig,function(r){
		if (r[0] != null && r[0] == "ok") {
			alert("保存成功");
			self.location = contextPath + "/managerzaizst/disturb_config.html";
		}else if(r[0] != null && r[0] == "error"){
			showErrorDlg(jQuery(document.body), r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "保存分红配置失败.");
		}
    });
};


/**
 * 赋值干扰配置信息
 */
LotteryDisturbConfigEditPage.prototype.editLotteryDisturbConfig = function(id){
	managerLotteryDisturbConfigAction.getLotteryDisturbConfigById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			$("#disturbShowId").val(r[1].id);
			$("#disturbModel").val(r[1].disturbModel);
			$("#userName").val(r[1].userName);
			$("#money").val(r[1].money);
			$("#lotteryType").val(r[1].lotteryType);
			$("#expectsets").val(r[1].expectsets);
			$("#disturbLotteryType").val(r[1].disturbLotteryType);
			$("#disturbTip").val(r[1].disturbTip);
			$("#enableSwitch").val(r[1].enableSwitch);
		}else if(r[0] != null && r[0] == "error"){
			showErrorDlg(jQuery(document.body), r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "获取分红配置信息失败.");
		}
    });
};