<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>  
    <head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>配置编辑</title>
    <link rel="shortcut icon" href="favicon.ico">
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    <%
	 String id = request.getParameter("id");  
	%>
</head>
  <body class="gray-bg">
        <div class="wrapper wrapper-content animated fadeIn">
         <form action="javascript:void(0)">
            <div class="row">  
               <c:if test="${admin.bizSystem == 'SUPER_SYSTEM'}">
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">商户</span>
                        <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
                        </div>
                </div>
                </c:if>
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">聊天室</span>
                        <select class="ipt form-control" id="roomid" name="roomid"></select>
                        </div>
                </div>
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">用户名</span>
                        <input id="userName" name="userName" type="text" value="" class="form-control"></div>
                </div>
                 <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">发红包总额(天)</span>
                        <input id="allowSendMoney" name="allowSendMoney" type="text" onkeyup="checkNum(this)" value="" class="form-control"></div>
                </div>
                 <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">显示呢称</span>
                        <input id="showName" name="showName" type="text" value="" class="form-control"></div>
                </div>
           
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-3 text-center">
                            <button type="button" class="btn btn-w-m btn-white" onclick="sendAuthEdit.saveData()">提 交</button>
                        </div>
                    </div>
                </div> 
            </div>
            </form>
        </div>

    <!-- js -->
	<script type="text/javascript"	src="<%=path%>/admin/chatusersendredbagauth/js/chatusersendredbagauthedit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
	<!-- dwr -->
	<script type='text/javascript'	src='<%=path%>/dwr/interface/managerChatUserSendredbagAuthAction.js'></script>
	<script type='text/javascript' src='<%=path%>/dwr/interface/managerChatBanAction.js'></script>
    <script type="text/javascript">
      sendAuthEdit.param.id = <%=id%>;
	</script>
</body>
</html>

