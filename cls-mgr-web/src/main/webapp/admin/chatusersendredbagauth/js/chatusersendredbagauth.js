function SendAuthPage(){};
var sendAuthPage=new SendAuthPage();

sendAuthPage.param={
		bizSystem:null,
		username:null,
		createTime:null
};

sendAuthPage.param2={
		bizSystem:null,
		username:null,
		createDate:null
};

//分页参数
sendAuthPage.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize
};

$(function(){
	sendAuthPage.initTableData();
});

SendAuthPage.prototype.query=function(){
	var bizSystem=$("#bizSystem").val();
	var userName=$("#userName").val();
	var roomid=$("#roomid").val();
//	var startime=$("#startime").val();
//	var endtime=$("#endtime").val();
	if(bizSystem!=""){
		sendAuthPage.param.bizSystem=bizSystem;
	}else{
		sendAuthPage.param.bizSystem=null;
	}
	if(userName!=""){
		sendAuthPage.param.userName=userName;
	}else{
		sendAuthPage.param.userName=null;
	}
	if(roomid!=""){
		sendAuthPage.param.roomid=roomid;
	}else{
		sendAuthPage.param.roomid=null;
	}
//	if(startime!=""){
//		sendAuthPage.param.startime=new Date(startime + ' 00:00:00');
//	}else{
//		sendAuthPage.param.startime=null;
//	}
//	if(endtime!=""){
//		sendAuthPage.param.endtime=new Date(endtime + " 23:59:59");
//	}else{
//		sendAuthPage.param.endtime=null;
//	}
	sendAuthPage.table.ajax.reload();
}



SendAuthPage.prototype.initTableData=function(){
	sendAuthPage.table = $('#table').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"scrollX": true,
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			sendAuthPage.pageParam.pageSize = data.length;//页面显示记录条数
			sendAuthPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerChatUserSendredbagAuthAction.getChatUserSendredbagAuthPage(sendAuthPage.param,sendAuthPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
                    {"data": null},
		            {"data": "bizSystemName"},
		            {"data": "roomid"},
		            {"data": "userName"},
		            {"data": "allowSendMoney"},
		            {"data": "showName"},
		            {"data": "createDateStr"},
		            {"data":null,
		            		render:function(data,type,row,meta){
		            			return '<button type="button" class="btn btn-info btn-sm btn-bj" onclick="sendAuthPage.edit('+row.id+')"> <i class="fa fa-pencil"></i>&nbsp;编辑</button>&nbsp;&nbsp;'
		            			      +'<button type="button" class="btn btn-danger btn-sm btn-del" onclick="sendAuthPage.delById('+row.id+')"> <i class="fa fa-trash"></i>&nbsp;删除</button>';
		            		}
		            }
	            ]
	}).api(); 
}


SendAuthPage.prototype.add=function()
{
	  layer.open({
          type: 2,
          title: '新增',
          maxmin: false,
          shadeClose: true,
          //点击遮罩关闭层
          area: ['600px', '500px'],
        //  area: ['45%', '63%'],
          content: contextPath + "/managerzaizst/chatusersendredbagauth/chatusersendredbagauth_edit.html",
          cancel: function(index){ 
        	  sendAuthPage.query();
       	   }
      });
	
}

SendAuthPage.prototype.edit=function(id)
{
	  layer.open({
          type: 2,
          title: '编辑',
          maxmin: false,
          shadeClose: true,
          //点击遮罩关闭层
          area: ['600px', '500px'],
         // area: ['45%', '63%'],
          content: contextPath + "/managerzaizst/chatusersendredbagauth/chatusersendredbagauth_edit.html?id="+id,
          cancel: function(index){ 
        	  sendAuthPage.query();
       	   }
      });
}
SendAuthPage.prototype.delById=function(id)
{
	 swal({
         title: "您确定要删除这条信息吗",
         text: "请谨慎操作！",
         type: "warning",
         showCancelButton: true,
         confirmButtonColor: "#DD6B55",
         cancelButtonText: "取消",
         confirmButtonText: "删除",
         closeOnConfirm: false
     },
     function() {
    	 managerChatUserSendredbagAuthAction.deleteById(id,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "删除成功",type: "success"});
				sendAuthPage.query();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
   			}else{
   				swal("删除信息失败.");
   			}
	    });
	});
}
SendAuthPage.prototype.closeLayer=function(index){
	layer.close(index);
	sendAuthPage.query();
}