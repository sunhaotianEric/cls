function SendAuthEdit(){};
var sendAuthEdit=new SendAuthEdit();

sendAuthEdit.param={
	id:null
}

$(function(){
	if(sendAuthEdit.param.id!=0){
		sendAuthEdit.getSendAuthEditById();
	}
	$("#bizSystem").change(function(){
		sendAuthEdit.initRoom($(this).val());
	});
});
SendAuthEdit.prototype.initRoom = function(bizSystem){
	dwr.engine.setAsync(false);
	$("#roomid").html('');
	managerChatBanAction.getChatRoomBizSystem(bizSystem,function(r){
		if (r[0] != null && r[0] == "ok") {
			var room = r[1];
			if(room!=null && room.length>0){
				for(var i=0;i<room.length;i++){
					$("#roomid").append("<option value='"+room[i].roomId+"'>"+room[i].roomId+"</option>")
				}
			}
			
		}else if(r[0] != null && r[0] == "error"){
			$("#roomid").html('');
			swal(r[1]);
		}else{
			$("#roomid").html('');
			swal("获取记录失败.");
			//showErrorDlg(jQuery(document.body), "获取充值配置失败.");
		}
    });
	dwr.engine.setAsync(true);
}
SendAuthEdit.prototype.getSendAuthEditById=function(){
	
	managerChatUserSendredbagAuthAction.selectById(sendAuthEdit.param.id,function(r){
		if (r[0] != null && r[0] == "ok") {
			var obj = r[1];
			sendAuthEdit.initRoom(obj.bizSystem);
			$("#bizSystem").val(obj.bizSystem);
			$("#roomid").val(obj.roomid);
			$("#userName").val(obj.userName);
			$("#allowSendMoney").val(obj.allowSendMoney);
			$("#showName").val(obj.showName);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("操作失败.");
		}
    });
}

SendAuthEdit.prototype.saveData=function(){
	var chatUserSendredbagAuth = {};
	var bizSystem = $("#bizSystem").val();
	var roomid = $("#roomid").val();
	var userName = $("#userName").val();
	var allowSendMoney = $("#allowSendMoney").val();
	var showName = $("#showName").val();
	if(currentUser.bizSystem=='SUPER_SYSTEM' && (bizSystem==null || bizSystem.trim()=="")){
		swal("请选择业务系统!");
		$("#bizSystem").focus();
		return;
	}
	if(roomid==null || roomid.trim()==""){
		swal("请选择聊天室!");
		$("#roomid").focus();
		return;
	}
	if(userName==null || userName.trim()==""){
		swal("请正确的用户名!");
		$("#userName").focus();
		return;
	}
	if(allowSendMoney==null || allowSendMoney.trim()==""){
		swal("请正确的发红包总额!");
		$("#allowSendMoney").focus();
		return;
	}
	chatUserSendredbagAuth.id = sendAuthEdit.param.id;
	chatUserSendredbagAuth.bizSystem =  bizSystem;
	chatUserSendredbagAuth.roomid =  roomid;
	chatUserSendredbagAuth.userName =  userName;
	chatUserSendredbagAuth.allowSendMoney =  allowSendMoney;
	chatUserSendredbagAuth.showName = showName;
	managerChatUserSendredbagAuthAction.saveOrUpdateChatUserSendredbagAuth(chatUserSendredbagAuth,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.sendAuthPage.closeLayer(index);
		   	   });
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("操作失败.");

		}
    });
}
