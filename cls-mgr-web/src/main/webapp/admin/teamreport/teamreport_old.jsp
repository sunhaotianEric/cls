<%@ page language="java" contentType="text/html; charset=UTF-8"
    import="com.team.lottery.util.ConstantUtil,com.team.lottery.vo.User,java.math.BigDecimal,com.team.lottery.enums.ELotteryKind,com.team.lottery.enums.EMoneyDetailType"
	pageEncoding="UTF-8"%>	
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
    String teamUserName="";
    String bizSystem=""; 
    String parm1=request.getParameter("param1");
    //System.out.println(parm1);
    if(parm1!=null){
    	//System.out.println(parm1.split("->").length);
    	teamUserName=parm1.split("->")[0];
    	bizSystem=parm1.split("->")[1];
    }
%>
<!DOCTYPE html>
<html>
    
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>团队报表</title>
     <jsp:include page="/admin/include/include.jsp"></jsp:include>
     <link href="<%=path%>/css/plugins/iCheck/custom.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" rel="stylesheet">
   <%--  <link href="<%=path%>/css/plugins/sweetalert/sweetalert.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" rel="stylesheet"> --%>
</head>
    <body class="gray-bg">
        <div class="wrapper wrapper-content animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a data-toggle="tab" href="#tab-1" id="tabContent1" aria-expanded="true">团队报表</a></li>
                            <li class="">
                                <a data-toggle="tab" href="#tab-2" id="tabContent2" aria-expanded="false">盈亏报表</a></li>
              <!--               <li class="">
                                <a data-toggle="tab" href="#tab-3" id="tabContent3" aria-expanded="false">总额报表</a></li> -->
                        </ul>
                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane active">
                                <div class="panel-body">
                                    <form class="form-horizontal" style="padding:0 20px" id="userTeamListUsersForm">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">用户名</span>
                                                    <input type="text" value="" id='teamUserListUserName' class="form-control"></div>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">注册时间始末</span>
                                                    <div class="input-daterange input-group" id="datepicker">
                                                        <input class="form-control layer-date" placeholder="开始日期" id="teamUserListRegisterDateStart" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})">
                                                        <span class="input-group-addon">到</span>
                                                        <input class="form-control layer-date" placeholder="结束日期" id="teamUserListRegisterDateEnd" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">账户余额始</span>
                                                    <input type="text" value="" onkeyup="checkNum(this)" id="teamUserListBalanceStart" class="form-control"></div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">账户余额终</span>
                                                    <input type="text" value="" onkeyup="checkNum(this)" id="teamUserListBalanceEnd" class="form-control"></div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">用户级别</span>
                                                    <select class="ipt form-control" id="teamUserListDailiLevel">
	                                                   	<option value="">所有</option>
								               			<!-- <option value="GENERALAGENCY">总代理</option> -->
								               			<option value="ORDINARYAGENCY">代理</option>
								               			<option value="REGULARMEMBERS">普通会员</option>
                                                      </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">排序</span>
                                                    <select name="teamUserListOrderSort" id="teamUserListOrderSort" class="ipt form-control" style="width:50%">
								               			<option value="1">默认排序</option>
								               			<option value="3">账户余额</option>
								               			<option value="5">注册时间</option>
								               		</select>
                                                    <div class="checkbox i-checks" style="display: inline-block;">
                                                        <label>
                                                            <input type="checkbox" id="teamUserListOrder" value="">
                                                            <i>
                                                            </i>从大到小</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="checkbox i-checks">
                                                    <label>
                                                        <input type="checkbox" id="teamUserListisOnline" value="">
                                                        <i>
                                                        </i>是否在线</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group text-center">
                                                    <button type="button" class="btn btn-outline btn-default btn-kj" onclick="userTeamListUsers.findUserTeamListUsersByQueryParam()">查询</button></div>
                                            </div>
                                        </div>
                                    </form>
                                     <div class="row">
                                               <div class="col-sm-12">
                                               <a href="javascript:void(0)" onclick="$('#teamUserListUserName').val(''),userTeamListUsers.findUserTeamListUsersByQueryParam();">我的用户</a>
                                               <span id="subTeamLink"></span>
                                               </div>
                                      </div>
                                     <div class="jqGrid_wrapper">
                                        <div class="ui-jqgrid ">
                                            <div class="ui-jqgrid-view">
                                                <div class="ui-jqgrid-hdiv">
                                                    <div class="ui-jqgrid-hbox" style="padding-right:0">
                                                        <table class="ui-jqgrid-htable ui-common-table table table-bordered" id="userTeamListUsersTable">
                                                            <thead>
                                                                <tr class="ui-jqgrid-labels">
                                                                    <th class="ui-th-column ui-th-ltr">序号</th>
                                                                    <th class="ui-th-column ui-th-ltr">用户名</th>
                                                                    <th class="ui-th-column ui-th-ltr">余额</th>
                                                                    <th class="ui-th-column ui-th-ltr">注册时间</th>
                                                                    <th class="ui-th-column ui-th-ltr">在线</th>
                                                                    <th class="ui-th-column ui-th-ltr">最后登录时间</th>
                                                                 </tr>
                                                            </thead>
                                                            <tbody>
                                                             
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                    
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-2" class="tab-pane">
                                <div class="panel-body">
                                    <form class="form-horizontal">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">用户名</span>
                                                    <input type="text" value="" id='teamUserConsumeReportUserName' class="form-control"></div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">消费时间</span>
                                                    <div class="input-daterange input-group" id="datepicker">
                                                        <input class="form-control layer-date" placeholder="开始日期"  id="teamUserConsumeHistoryReportDateStart"  onclick="laydate({istime: true, format: 'YYYY-MM-DD'})">
                                                      <!--   <input class="form-control layer-date" placeholder="开始日期"  id="teamUserConsumeReportDateStart"  onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" style="display: none;"> -->
       
                                                        <span class="input-group-addon">到</span>
                                                        <input class="form-control layer-date" placeholder="结束日期" id="teamUserConsumeHistoryReportDateEnd" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})">
                                                        <!-- <input class="form-control layer-date" placeholder="结束日期" id="teamUserConsumeReportDateEnd" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})">
                                                         -->
                                                        </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">报表类型</span>
                                                    	<select  name="reportType" id="reportType" class="ipt form-control" onchange="chageDateFt()">
																<option value="0">历史盈亏</option>
																<option value="1">实时盈亏</option>
														</select>
                                                    </div>
                                            </div>
                                            <div class="col-sm-1">
                                                <div class="form-group text-right">
                                                    <button type="button" class="btn btn-outline btn-default btn-sel" onclick="userConsumeReportPage.findUserConsumeReportByQueryParam()">查询</button></div>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="jqGrid_wrapper">
                                        <div class="ui-jqgrid ">
                                            <div class="ui-jqgrid-view">
                                                <div class="ui-jqgrid-hdiv">
                                                    <div class="ui-jqgrid-hbox" style="padding-right:0">
                                                        <table class="ui-jqgrid-htable ui-common-table table table-bordered" id="userConsumeReportTable">
                                                            <thead>
                                                                <tr class="ui-jqgrid-labels">
                                                                
                                                                    <th class="ui-th-column ui-th-ltr">用户名</th>
                                                                    <th class="ui-th-column ui-th-ltr">存款</th>
				                                               
				                                                    <th class="ui-th-column ui-th-ltr">取款</th>
				                                             
				                                                    <th class="ui-th-column ui-th-ltr">投注(-撤单)</th>
				                                                     <th class="ui-th-column ui-th-ltr">中奖</th>
				                                                    
				                                                    <th class="ui-th-column ui-th-ltr">返点</th>
				                                                    <th class="ui-th-column ui-th-ltr">活动赠送</th>
				                                                 
				                                                    <th class="ui-th-column ui-th-ltr">其他</th>
				                                                    <th class="ui-th-column ui-th-ltr">盈利</th>
				                                                    <th class="ui-th-column ui-th-ltr">注册/首充/投注(人)</th>
                                                                    
                                                                    </tr>
                                                            </thead>
                                                            <tbody>
                                            
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        
                                        </div>
                                    </div>
                                </div>
                            </div>
              <!--               <div id="tab-3" class="tab-pane">
                                <div class="panel-body">
                                    <form class="form-horizontal">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">用户名</span>
                                                    <input type="text" value="" id='teamUserAccountReportUserName' class="form-control"></div>
                                            </div>
                                            <div class="col-sm-1">
                                                <div class="form-group text-right">
                                                    <button type="button" class="btn btn-outline btn-default btn-sel" onclick="userAccountReportPage.findUserAccountReportByQueryParam()">查询</button></div>
                                            </div>
                                            <div class="col-sm-8"></div>
                                        </div>
                                    </form>
                                    <div class="jqGrid_wrapper">
                                        <div class="ui-jqgrid ">
                                            <div class="ui-jqgrid-view">
                                                <div class="ui-jqgrid-hdiv">
                                                    <div class="ui-jqgrid-hbox" style="padding-right:0">
                                                        <table style="width: 100%;" class="ui-jqgrid-htable ui-common-table table table-bordered" id="userMoneyTotalTable">
                                                            <thead >
                                                                <tr class="ui-jqgrid-labels">
                                                                      <th class="ui-th-column ui-th-ltr">用户名</th>
															          <th class="ui-th-column ui-th-ltr">余额</th>
															          <th class="ui-th-column ui-th-ltr">充值</th>
															           <th class="ui-th-column ui-th-ltr">充值赠送</th>
															          <th class="ui-th-column ui-th-ltr">活动彩金</th>
															          <th class="ui-th-column ui-th-ltr">商户续费</th>
												                      <th class="ui-th-column ui-th-ltr">商户扣费</th>
															          <th class="ui-th-column ui-th-ltr">取款</th>
															          <th class="ui-th-column ui-th-ltr">取款手续费</th>        
															          <th class="ui-th-column ui-th-ltr">支出转账</th>
															          <th class="ui-th-column ui-th-ltr">收入转账</th>   
															          <th class="ui-th-column ui-th-ltr">投注(-撤单)</th>
															          <th class="ui-th-column ui-th-ltr">中奖</th>  
															          <th class="ui-th-column ui-th-ltr">投注返点</th>
																      <th class="ui-th-column ui-th-ltr">推广返点</th>
																      <th class="ui-th-column ui-th-ltr">契约分红</th>
															          <th class="ui-th-column ui-th-ltr">日工资</th>  
															          <th class="ui-th-column ui-th-ltr">盈利</th>
                                                                    </tr>
                                                            </thead>
                                                            <tbody>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                          
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <%--  <script src="<%=path%>/js/plugins/switchery/switchery.js"></script> --%>
    <script src="<%=path%>/js/plugins/iCheck/icheck.min.js"></script>
 <!-- 业务js -->
<script type="text/javascript" src="<%=path%>/admin/teamreport/js/team_report.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<script type="text/javascript" src="<%=path%>/admin/teamreport/js/team_report_list_users.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<script type="text/javascript" src="<%=path%>/admin/teamreport/js/team_report_list_consume_report.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<script type="text/javascript" src="<%=path%>/admin/teamreport/js/team_report_list_account_report.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerTeamAction.js'></script>
	<script>$(document).ready(function() {
	        $(".i-checks").iCheck({
	            checkboxClass: "icheckbox_square-green",
	            radioClass: "iradio_square-green",
	        })
	    });</script>
<script type="text/javascript">
teamReportPage.param.teamUserName = '<%=teamUserName %>';
teamReportPage.param.bizSystem = '<%=bizSystem %>';

/* function chageDateFt(){
	var reportType=$("#reportType").val();
	if(reportType=='1'){
		$("#teamUserConsumeHistoryReportDateStart").attr("onclick","laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})");
		$("#teamUserConsumeHistoryReportDateEnd").attr("onclick","laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})");

	}else{
		$("#teamUserConsumeHistoryReportDateStart").attr("onclick","laydate({istime: true, format: 'YYYY-MM-DD'})");
		$("#teamUserConsumeHistoryReportDateEnd").attr("onclick","laydate({istime: true, format: 'YYYY-MM-DD'})");

	}
} */
function chageDateFt(){
	var reportType=$("#reportType").val();
	var nowDate = new Date();
	//在00:00:00 - 03:00:00之间的时间取前一天
	if(nowDate.getHours() < 3) {
		nowDate = nowDate.AddDays(-1);
	}
	if(reportType=='1'){
		$("#teamUserConsumeHistoryReportDateStart").attr("onclick","laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})");
		$("#teamUserConsumeHistoryReportDateEnd").attr("onclick","laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})");

		nowDate.setHours(3, 0, 0, 0);
		$("#teamUserConsumeHistoryReportDateStart").val(nowDate.format("yyyy-MM-dd hh:mm:ss"));
		nowDate.AddDays(1).setHours(2, 59, 59, 999);
		$("#teamUserConsumeHistoryReportDateEnd").val(nowDate.format("yyyy-MM-dd hh:mm:ss"));
	}else{
		$("#teamUserConsumeHistoryReportDateStart").attr("onclick","laydate({istime: true, format: 'YYYY-MM-DD'})");
		$("#teamUserConsumeHistoryReportDateEnd").attr("onclick","laydate({istime: true, format: 'YYYY-MM-DD'})");
		
		//默认查询前一天的
		nowDate = nowDate.AddDays(-1);
		nowDate.setHours(3, 0, 0, 0);
		$("#teamUserConsumeHistoryReportDateStart").val(nowDate.format("yyyy-MM-dd hh:mm:ss"));
		nowDate.AddDays(1).setHours(2, 59, 59, 999);
		$("#teamUserConsumeHistoryReportDateEnd").val(nowDate.format("yyyy-MM-dd hh:mm:ss"));
	}
}
</script>  
</body>
</html>
	