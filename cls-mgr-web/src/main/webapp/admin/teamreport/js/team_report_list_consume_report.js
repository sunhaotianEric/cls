function UserConsumeReportPage(){}
var userConsumeReportPage = new UserConsumeReportPage();

userConsumeReportPage.consumeReports = new Array();
userConsumeReportPage.isquery=true;
userConsumeReportPage.isneedDestroy=true;
userConsumeReportPage.table;
/**
 * 查询参数
 */
userConsumeReportPage.queryParam = {
		teamLeaderName : null,
		userName : null,
		createdDateStart : null,
		createdDateEnd : null,
		reportType : 0
};

//分页参数
userConsumeReportPage.pageParam = {
		   pageNo : 1,
	       pageSize : defaultPageSize
};

$(document).ready(function() {

	
	var nowDate = new Date();
	nowDate = nowDate.AddDays(-1);
	if(nowDate.getHours() < 3) {
		$("#teamUserConsumeHistoryReportDateStart").attr("onclick","laydate({istime: true, format: 'YYYY-MM-DD',max: laydate.now(-1)})");
		$("#teamUserConsumeHistoryReportDateEnd").attr("onclick","laydate({istime: true, format: 'YYYY-MM-DD',max: laydate.now(-1)})");
	}else{
		$("#teamUserConsumeHistoryReportDateStart").attr("onclick","laydate({istime: true, format: 'YYYY-MM-DD',max: laydate.now(0)})");
		$("#teamUserConsumeHistoryReportDateEnd").attr("onclick","laydate({istime: true, format: 'YYYY-MM-DD',max: laydate.now(0)})");
	}
	
	$("#teamUserConsumeHistoryReportDateStart").val(nowDate.format("yyyy-MM-dd"));
	$("#teamUserConsumeHistoryReportDateEnd").val(nowDate.format("yyyy-MM-dd"));
	
	userConsumeReportPage.queryParam.bizSystem = teamReportPage.param.bizSystem;
	$('#tabContent2').bind("click",function(){
		$("#teamUserConsumeReportUserName").val(teamReportPage.param.teamUserName.substring(0,teamReportPage.param.teamUserName.length-2));    
		userConsumeReportPage.findUserConsumeReportByQueryParam();
	});
	
});

/**
 * 按页面条件查询数据
 */
UserConsumeReportPage.prototype.findUserConsumeReportByQueryParam = function(page){
	var userName = $("#teamUserConsumeReportUserName").val();
	var startimeStr = "";
	var endtimeStr = "";

	startimeStr = $("#teamUserConsumeHistoryReportDateStart").val();
	endtimeStr = $("#teamUserConsumeHistoryReportDateEnd").val();

	if(startimeStr != ""){
		userConsumeReportPage.queryParam.createdDateStart = startimeStr.stringToDate();
	}
	if(endtimeStr != ""){
		userConsumeReportPage.queryParam.createdDateEnd = endtimeStr.stringToDate();
	}
	userConsumeReportPage.queryParam.userName = userName;
	if(userConsumeReportPage.isneedDestroy){
		userConsumeReportPage.isquery=false;
		userConsumeReportPage.initTableData();
		userConsumeReportPage.isneedDestroy=false;
	}else{
		userConsumeReportPage.table.ajax.reload();
	}
};

/**
 * 初始化列表数据
 */
UserConsumeReportPage.prototype.initTableData = function() {
//	userTeamListUsers.queryParam = getFormObj($("#userTeamListUsersForm"));
	userConsumeReportPage.table = $('#userConsumeReportTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"scrollX": true,
		"iDisplayLength" : defaultPageSize,
		"fnDrawCallback": function (oSettings) {  
			userConsumeReportPage.getTotalRes(userConsumeReportPage.pageParam.currentPageRes,"单页统计");

			userConsumeReportPage.getTotalRes(userConsumeReportPage.consumeReports,"总统计");   
        },
		"ajax":function (data, callback, settings) {
			userConsumeReportPage.pageParam.pageSize = data.length;//页面显示记录条数

			userConsumeReportPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
			
			//封装返回数据
			var returnData = {
				recordsTotal : 0,
				recordsFiltered : 0,
				data : null
			};
				managerTeamAction.getTeamConsumeReportForQuery(userConsumeReportPage.queryParam,userConsumeReportPage.pageParam,function(r){
					
					if (r[0] != null && r[0] == "ok") {
						userConsumeReportPage.initPageData(r[1]);
						
						returnData.recordsTotal = userConsumeReportPage.pageParam.totalRecNum;//返回数据全部记录
						returnData.recordsFiltered = userConsumeReportPage.pageParam.totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
						returnData.data = userConsumeReportPage.pageParam.currentPageRes;//返回的数据列表

					}else if(r[0] != null && r[0] == "error"){
						swal(r[1]);
						userConsumeReportPage.pageParam.totalRecNum=0;
						userConsumeReportPage.consumeReports = new Array();
						userConsumeReportPage.pageParam.currentPageRes=new Array();
						returnData.recordsTotal = userConsumeReportPage.pageParam.totalRecNum;//返回数据全部记录
						returnData.recordsFiltered = userConsumeReportPage.pageParam.totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
						returnData.data = userConsumeReportPage.pageParam.currentPageRes;//返回的数据列表

					}else{
						swal("查询数据失败!");
					}
					callback(returnData);
				});	
		
		
	    	
		},
		"columns": [
		            {"data": "userName"},
		            {"data": "recharge",
		            	  render: function(data, type, row, meta) {
		            		  return row.recharge.toFixed(commonPage.param.fixVaue);
		            		}
		            },
		            {"data": "withdraw",
		            	  render: function(data, type, row, meta) {
		            		  return row.withdraw.toFixed(commonPage.param.fixVaue);
		            		}
		            },
		     
		            {"data": "lottery",
		            	render: function(data, type, row, meta) {
		            		return row.lottery.toFixed(commonPage.param.fixVaue);
		            	}
		            },
		            {"data": "win",
		            	render: function(data, type, row, meta) {
		            		return row.win.toFixed(commonPage.param.fixVaue);
		            	}
		            },
		  
		            {"data": "rebate",
		            	  render: function(data, type, row, meta) {
		            		  return (row.rebate + row.percentage).toFixed(commonPage.param.fixVaue);
		            		}
		            },
		            {"data": "activitiesmoney",
		            	  render: function(data, type, row, meta) {
		            		  return (row.rechargepresent + row.activitiesmoney+row.systemaddmoney).toFixed(commonPage.param.fixVaue);
		            		}
		            },
		     /*       {"data": null,
		            	  render: function(data, type, row, meta) {
		            		  return (row.systemaddmoney - row.systemreducemoney).toFixed(commonPage.param.fixVaue);
		            		}
		            },*/
		            {"data":"managereducemoney",
		            	render: function(data, type, row, meta) {
		            		  return data==null?"0":data;
		            		}
		            },
		            {"data": "gain",
		            	  render: function(data, type, row, meta) {
		            		  return row.gain.toFixed(commonPage.param.fixVaue);
		            		}
		            },
		            {"data": null,
		            	  render: function(data, type, row, meta) {		            		 
		            		  return (row.regCount==null?"-":row.regCount)+"/"+(row.firstRechargeCount==null?"-":row.firstRechargeCount)+"/"+(row.lotteryCount==null?"-":row.lotteryCount);		            		  
		            		}
		            }		          
	            ]
	}).api(); 
}






/**
 * 进行数据统计的计算，返回tr字符串拼接
 * @param datas
 * @param totalDesc  单页统计或总统计
 */
UserConsumeReportPage.prototype.getTotalRes = function(datas, totalDesc) {
	var totalMoney = 0;
    var totalRecharge = 0;
    var totalWithDraw = 0;
    var totalRegister = 0;
    var totalExtend = 0;
    var totalLottery = 0;
    var totalWin = 0;
    var totalRebate = 0;
    var totalPercentage = 0;
    var totalPresent = 0;
    var totalOther = 0;
    var totalGain = 0;
    var totalRegCount=0;
    var totalFirstRechargeCoun=0;
    var totalLotteryCount=0;
    var totalManagereduceMoney=0;
	if(datas != null && datas.length > 0) {
		for(var i = 0; i < datas.length; i++) {
			var report = datas[i];
			totalMoney += report.money;
    	    totalRecharge += report.recharge;
    	    totalWithDraw += report.withdraw;
    		totalRegister += report.register;
    		totalExtend += report.extend;
    	    totalLottery += report.lottery;
    	    totalWin += report.win;
    	    totalRebate += report.rebate + report.percentage;
    	    totalPercentage += report.percentage;
    	    totalPresent += report.rechargepresent + report.activitiesmoney + report.systemaddmoney;
    	    //totalOther += (report.systemaddmoney - report.systemreducemoney);
    	    totalGain += report.gain;
    	    totalManagereduceMoney+=report.managereducemoney;
    	    if(totalDesc == '总统计' && report.userName == userConsumeReportPage.queryParam.userName){
    	    	totalRegCount = report.regCount;
        	    totalFirstRechargeCoun = report.firstRechargeCount;
        	    totalLotteryCount = report.lotteryCount;	
    	    }
		}
	}
	if(totalDesc == '单页统计'){
    	totalRegCount =null;
    	totalFirstRechargeCoun = null; 
    	totalLotteryCount = null;
   }
	var str = "";
	str += "<tr style='color:red'>";
    str += "  <td>"+totalDesc+"</td>";
	str += "  <td>"+ totalRecharge.toFixed(commonPage.param.fixVaue) +"</td>";
	str += "  <td>"+ totalWithDraw.toFixed(commonPage.param.fixVaue) +"</td>";
	str += "  <td>"+ totalLottery.toFixed(commonPage.param.fixVaue) +"</td>";
	str += "  <td>"+ totalWin.toFixed(commonPage.param.fixVaue) +"</td>";
	str += "  <td>"+ totalRebate.toFixed(commonPage.param.fixVaue) +"</td>";
	str += "  <td>"+ totalPresent.toFixed(commonPage.param.fixVaue) +"</td>";
	//str += "  <td>"+ totalOther.toFixed(commonPage.param.fixVaue) +"</td>";
	str += "  <td>"+ totalManagereduceMoney.toFixed(commonPage.param.fixVaue) +"</td>";
	str += "  <td>"+ totalGain.toFixed(commonPage.param.fixVaue) +"</td>";
	str += "  <td>"+ (totalRegCount==null?"-":totalRegCount)+"/"+(totalFirstRechargeCoun==null?"-":totalFirstRechargeCoun)+"/"+(totalLotteryCount==null?"-":totalLotteryCount)+"</td>";
    str += "</tr>";
    $("#userConsumeReportTable tbody").append(str);
    //return str;
}

/**
 * 初始化分页数据
 */
UserConsumeReportPage.prototype.initPageData = function(datas) {
	userConsumeReportPage.consumeReports = new Array();
	var page = userConsumeReportPage.pageParam;
	page.pageNo = userConsumeReportPage.pageParam.pageNo;
	page.totalPageNum = userConsumeReportPage.pageParam.totalPageNum;;
	if(datas != 0) {
		for(var key in datas){
			var report = datas[key];
			userConsumeReportPage.consumeReports.push(report);
		}
		page.totalRecNum = userConsumeReportPage.consumeReports.length;
		//计算总页数
		page.totalPageNum = parseInt(page.totalRecNum/page.pageSize);
		if(parseFloat("0."+page.totalRecNum%page.pageSize)>0){
			page.totalPageNum=page.totalPageNum+1;
		}
		userConsumeReportPage.getCurrentPageRes(page);
	}
}

/**
 * 得到当前页的数据
 */
UserConsumeReportPage.prototype.getCurrentPageRes = function(page) {
	if(userConsumeReportPage.consumeReports.length > 0) {
		page.currentPageRes = new Array();
		//计算查询当前页的开始结束位置
		var startIndex = page.pageSize*(page.pageNo-1);
		var endIndex = (page.pageNo * page.pageSize) - 1;
		if(page.pageNo * page.pageSize > page.totalRecNum) {
			endIndex = page.totalRecNum - 1;
		}
		for(var i = startIndex; i <= endIndex; i++) {
			page.currentPageRes.push(userConsumeReportPage.consumeReports[i]);
		}
	
	}
}