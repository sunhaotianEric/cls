function UserTeamListUsers(){}
var userTeamListUsers = new UserTeamListUsers();

userTeamListUsers.param = {
   teamUserList : new Array()		
};

/**
 * 查询参数
 */
userTeamListUsers.queryParam = {
		teamLeaderName : null,
		userName : null,
		registerDateStart : null,
		registerDateEnd : null,
		balanceStart : null,
		balanceEnd : null,
		dailiLevel:null,
		orderSort:null,
		isOnline:null,
		isQuerySub:false   //是否同时查询下级用户
};

//分页参数
userTeamListUsers.pageParam = {
		   pageNo : 1,
	       pageSize : defaultPageSize
};

$(document).ready(function() {
	userTeamListUsers.queryParam.teamLeaderName = teamReportPage.param.teamUserName;
	userTeamListUsers.queryParam.bizSystem = teamReportPage.param.bizSystem;
	
	

	userTeamListUsers.initTableData(); 
});


/**
 * 按页面条件查询数据
 */
UserTeamListUsers.prototype.findUserTeamListUsersByQueryParam = function(){

	userTeamListUsers.queryParam.userName = $("#teamUserListUserName").val();
	userTeamListUsers.queryParam.balanceStart = $("#teamUserListBalanceStart").val();
	userTeamListUsers.queryParam.balanceEnd = $("#teamUserListBalanceEnd").val();
	userTeamListUsers.queryParam.dailiLevel = getSearchVal("teamUserListDailiLevel");
	userTeamListUsers.queryParam.orderSort = getSearchVal("teamUserListOrderSort");
	userTeamListUsers.queryParam.isOnline = $('#teamUserListisOnline').is(':checked');
	var order =  $('#teamUserListOrder').is(':checked');
	if(userTeamListUsers.queryParam.orderSort != null && order) {
		userTeamListUsers.queryParam.orderSort = parseInt(userTeamListUsers.queryParam.orderSort) + 1;
	} 
	
	var registerDateStart = $("#teamUserListRegisterDateStart").val();
	var registerDateEnd = $("#teamUserListRegisterDateEnd").val();
	if(registerDateStart != ""){
		userTeamListUsers.queryParam.registerDateStart = registerDateStart.stringToDate();
	}
	if(registerDateEnd != ""){
		userTeamListUsers.queryParam.registerDateEnd = registerDateEnd.stringToDate();
	}
	if(userTeamListUsers.queryParam.userName != null && $.trim(userTeamListUsers.queryParam.userName)==""){
		userTeamListUsers.queryParam.userName = null;
		userTeamListUsers.queryParam.teamLeaderName = teamReportPage.param.teamUserName;
		userTeamListUsers.queryParam.isQuerySub = false;
	} else {
		userTeamListUsers.queryParam.isQuerySub = true;
		
	}
	if(userTeamListUsers.queryParam.balanceStart != null && $.trim(userTeamListUsers.queryParam.balanceStart)==""){
		userTeamListUsers.queryParam.balanceStart = null;
	}
	if(userTeamListUsers.queryParam.balanceEnd != null && $.trim(userTeamListUsers.queryParam.balanceEnd)==""){
		userTeamListUsers.queryParam.balanceEnd = null;
	}
	if(userTeamListUsers.queryParam.registerDateStart!=null && $.trim(registerDateStart)==""){
		userTeamListUsers.queryParam.registerDateStart = null;
	}
	if(userTeamListUsers.queryParam.registerDateEnd!=null && $.trim(registerDateEnd)==""){
		userTeamListUsers.queryParam.registerDateEnd = null;
	}
	$("#subTeamLink").html("");
	userTeamListUsers.table.ajax.reload();
};


/**
 * 初始化列表数据
 */
UserTeamListUsers.prototype.initTableData = function() {
//	userTeamListUsers.queryParam = getFormObj($("#userTeamListUsersForm"));
	userTeamListUsers.table = $('#userTeamListUsersTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"scrollX": true,
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			userTeamListUsers.pageParam.pageSize = data.length;//页面显示记录条数
			userTeamListUsers.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerTeamAction.getTeamUserListPage(userTeamListUsers.queryParam,userTeamListUsers.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				
				callback(returnData);
			});
		},
		"columns": [
		            {"data": null
		            },
		            {"data": "userName",
		            	  render: function(data, type, row, meta) {
		            		var str="";
		            		if(row.downMemberCount > 0 && (row.dailiLevel == 'GENERALAGENCY' || row.dailiLevel == 'ORDINARYAGENCY')) {
		            			str += "<a href='javascript:void(0)' onclick='userTeamListUsers.loadSubTeamListUser(\""+row.userName+
		            				"\", \""+row.regfrom+row.userName+"&\")' style='color:blue;text-decoration:underline;'>"+ row.userName +"</a>";
		            		} else {
		            			str += ""+ row.userName;
		            		}
		            		
		            		return str;
		            	}
		            },
		            {"data": "money",
		            	  render: function(data, type, row, meta) {
		            	
		            		  return row.money.toFixed(commonPage.param.fixVaue);
		            		}
		            },
		            {"data": "addTimeStr"},
		            {"data": "isOnline",
		            	  render: function(data, type, row, meta) {
		            		  if(data != null && data == 1){
		            			  return "当前在线";
		            			}else{
		            			   return "未在线";
		            			}
		            		  return data;
		            		}
		            },
		            
		            {"data": "lastlogoutTimeStr"}
		         
	            ]
	}).api(); 
}



/**
 * 加载子节点的方法
 */
UserTeamListUsers.prototype.loadSubTeamListUser = function(teamLeaderName, regfrom){
	$("#subTeamLink").html("");
	userTeamListUsers.queryParam.teamLeaderName = teamLeaderName +"00";
	userTeamListUsers.queryParam.userName = null;
	userTeamListUsers.queryParam.balanceStart=null;
	userTeamListUsers.queryParam.balanceEnd = null;
	userTeamListUsers.queryParam.registerDateStart = null;
	userTeamListUsers.queryParam.registerDateEnd = null;
	userTeamListUsers.queryParam.isOnline = false;
	userTeamListUsers.queryParam.isQuerySub = false;
	if(regfrom != undefined && regfrom != null) {
		var parentUserNames = regfrom.split("&");
		//用于构造每级的regFrom
		var subRegfrom = "";
		var subLegth = 0;
		if(parentUserNames.length > 0) {
			//最后一个不处理
			subLegth = parentUserNames.length - 1;
		}
		for(var i = 1; i < subLegth; i++) {
			if(i == 1) {
				subRegfrom = parentUserNames[0] + "&";
			}
			subRegfrom += parentUserNames[i] + "&";
			//第三级下面才开始加链接
			if(i > 1) {
				var linkBlock = "<span>&nbsp;>&nbsp;<a href='javascript:void(0)' onclick='userTeamListUsers.loadSubTeamListUser(\""+parentUserNames[i]+"\", \""+subRegfrom+"\")'>"+parentUserNames[i]+"</a></span>";
				$("#subTeamLink").append(linkBlock);
			}
		}
	}
	userTeamListUsers.table.ajax.reload();
};




/**
 * 条件查询投注记录
 */
UserTeamListUsers.prototype.queryConditionTeamUserList = function(queryParam,pageNo){
	managerTeamAction.getTeamUserListPage(queryParam,pageNo,function(r){
		if (r[0] != null && r[0] == "ok") {
			userTeamListUsers.refreshUserTeamListUsersPages(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			commonPage.showKindlyReminder(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询用户的团队会员数据失败.");
		}
    });
};

