<%@ page language="java" contentType="text/html; charset=UTF-8" 
	pageEncoding="UTF-8"%>	
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
    
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>每日在线统计报表</title>
        <jsp:include page="/admin/include/include.jsp"></jsp:include>
    
</head>
    <body>
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
                    <div class="ibox ">
                        <div class="ibox-content">
                            <div class="panel-body">
                                <form class="form-horizontal">
                                    <div class="row">
                                    <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                                      <div class="col-sm-3">
                                       <div class="input-group m-b">
                                         <span class="input-group-addon">商户</span>
                                          <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control" />
                                         </div>  
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">日期始末</span>
                                                <div class="input-daterange input-group">
                                                    <input class="laydate-icon form-control layer-date" placeholder="开始日期" readonly="readonly" style="background: white;" onclick="laydate({istime: false, format: 'YYYY-MM-DD'})" id="dayOnlineInfoDateStart">
                                                    <span class="input-group-addon">到</span>
                                                    <input class="laydate-icon form-control layer-date" placeholder="结束日期" readonly="readonly" style="background: white;" onclick="laydate({istime: false, format: 'YYYY-MM-DD'})" id="dayOnlineInfoDateEnd"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group nomargin text-right">
                                                <button type="button" class="btn btn-outline btn-default btn-kj"  onclick="userDayOnlineInfoPage.findUserDayOnlineInfoPageByQueryParam()">查询</button></div>
                                        </div>
                                         </c:if>
                                     <c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
                             
                                        <div class="col-sm-6">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">日期始末</span>
                                                <div class="input-daterange input-group">
                                                    <input class="laydate-icon form-control layer-date" placeholder="开始日期" readonly="readonly" style="background: white;" onclick="laydate({istime: false, format: 'YYYY-MM-DD'})" id="dayOnlineInfoDateStart">
                                                    <span class="input-group-addon">到</span>
                                                    <input class="laydate-icon form-control layer-date" placeholder="结束日期" readonly="readonly" style="background: white;" onclick="laydate({istime: false, format: 'YYYY-MM-DD'})" id="dayOnlineInfoDateEnd"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group nomargin text-right">
                                                <button type="button" class="btn btn-outline btn-default btn-kj"  onclick="userDayOnlineInfoPage.findUserDayOnlineInfoPageByQueryParam()">查询</button></div>
                                        </div>
                                         </c:if>
                                    </div>
                                </form>
                                <div class="jqGrid_wrapper">
                                    <div class="ui-jqgrid ">
                                        <div class="ui-jqgrid-view">
                                            <div class="ui-jqgrid-hdiv">
                                                <div class="ui-jqgrid-hbox" style="padding-right:0">
                                                    <table class="ui-jqgrid-htable ui-common-table table table-bordered" id="userDayOnlineInfoTable">
                                                        <thead>
                                                            <tr class="ui-jqgrid-labels">
                                                                <th class="ui-th-column ui-th-ltr">商户</th>
                                                                <th class="ui-th-column ui-th-ltr">日期</th>
                                                                <th class="ui-th-column ui-th-ltr">登录总人数</th>
                                                                <th class="ui-th-column ui-th-ltr">电脑登录人数</th>
                                                                <th class="ui-th-column ui-th-ltr">手机web登录人数</th>
                                                                <th class="ui-th-column ui-th-ltr">APP登录人数</th>
                                                            
                                                                <th class="ui-th-column ui-th-ltr" >投注总人数</th>
                                                                <th class="ui-th-column ui-th-ltr">电脑投注人数</th>
                                                                <th class="ui-th-column ui-th-ltr">手机web投注人数</th>
                                                                <th class="ui-th-column ui-th-ltr">APP投注人数</th></tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/admin/user/js/user_day_online_info.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerUserOnlineAction.js'></script>    
</body>
</html>
	