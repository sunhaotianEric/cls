<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>s
	<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>银行卡编辑</title>
<link rel="shortcut icon" href="favicon.ico">
<jsp:include page="/admin/include/include.jsp"></jsp:include>

<%
	String id = request.getParameter("id");
%>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeIn">
		<form action="javascript:void(0)">
			<div class="row">
				<div class="col-sm-6">
					<c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
						<div class="col-sm-12">
							<div class="input-group m-b">
								<span class="input-group-addon">商户：</span>
								<cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
							</div>
						</div>
					</c:if>
				</div>
				<div class="col-sm-6">
					<div class="line">
						<div class="line-title">开户所在城市：</div>

						<div class="line-content select-content province sort " onClick="event.cancelBubble = true;">
							<input type="hidden" class="select-save" id="province" />
							<p class="select-title">省份</p>
							<img class="select-pointer"
								src="<%=path%>/img/p2.png" />
							<ul class="select-list" id="s_province" name="s_province">
								<!--  <li><p>广东</p></li>
                    <li><p>广东</p></li>
                    <li><p>广东</p></li>
                    <li><p>广东</p></li>
                    <li><p>广东</p></li> -->
							</ul>
						</div>
						<div class="line-content select-content city sort"
							onClick="event.cancelBubble = true;">
							<input type="hidden" class="select-save" id="city" />
							<p class="select-title">地级市</p>
							<img class="select-pointer"
								src="<%=path%>/img/p2.png" />
							<ul class="select-list" id="s_city" name="s_city">
								<!--  <li><p>广东</p></li>
                    <li><p>广东</p></li>
                    <li><p>广东</p></li>
                    <li><p>广东</p></li>
                    <li><p>广东</p></li> -->
							</ul>
						</div>
						<div class="line-content select-content town sort"
							onClick="event.cancelBubble = true;" style="display: none;">
							<input type="hidden" class="select-save" id="county"
								value="市、县级市" />
							<p class="select-title">县级市</p>
							<img class="select-pointer"
								src="<%=path%>/img/p2.png" />
							<ul class="select-list" id="s_county" name="s_county">
								<!--  <li><p>广东</p></li>
                    <li><p>广东</p></li>
                    <li><p>广东</p></li>
                    <li><p>广东</p></li>
                    <li><p>广东</p></li> -->
							</ul>
						</div>
						<div class="line-msg" id="cityTip" style="display: none;">
							<p>* 请选择开户所在城市</p>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="input-group m-b">
						<span class="input-group-addon">用户名</span> <input
							id="userName" name="userName" type="text" value=""
							class="form-control">
					</div>
				</div>
				<div class="col-sm-6">
					<div class="input-group m-b">
						<span class="input-group-addon">用户id</span> <input
							id="userId" name="userId" type="text" value=""
							class="form-control">
					</div>
				</div>
				<div class="col-sm-6">
					<div class="input-group m-b">
						<span class="input-group-addon">银行类型</span>
						<cls:enmuSel name="bankType" className="com.team.lottery.enums.EBankInfo" options="class:ipt form-control" id="bankType"/>
	   	                <input type="hidden" id="bankName" name="bankName"/>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="input-group m-b">
						<span class="input-group-addon">开户人姓名</span> <input
							id="bankAccountName" name="bankAccountName" type="text" value=""
							class="form-control">
					</div>
				</div>
				<div class="col-sm-6">
					<div class="input-group m-b">
						<span class="input-group-addon">卡号码</span> <input
							id="bankCardNum" name="bankCardNum" type="text" value=""
							class="form-control">
					</div>
				</div>
				
				
				
				
				
				
				
				<div class="col-sm-6">
					<div class="row">
						<div class="col-sm-3 text-center">
							<button type="button" class="btn btn-w-m btn-white" id="subBankBtn"
								onclick="userBankListEditPage.saveData()">提 交&nbsp;&nbsp;&nbsp;&nbsp;</button>
						</div>
						<div class="col-sm-3 text-center">
							<!-- <button type="button" class="btn btn-w-m btn-white">重 置</button> -->
							<input type="reset" class="btn btn-w-m btn-white" value="重 置" />
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>

	<!-- js -->
		
		<link rel="stylesheet" type="text/css" href="<%=path%>/css/user_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
		<script type="text/javascript" src="<%=path%>/js/area.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
		<script type="text/javascript" src="<%=path%>/js/user_base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
		<script type="text/javascript"
		src="<%=path%>/admin/user/js/user_bank_list_edit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
	<!-- dwr -->
	<script type='text/javascript'
		src='<%=path%>/dwr/interface/managerUserAction.js'></script>
	<script type="text/javascript">
		editBizSystemPage.param.id =
	<%=id%>
		;
	</script>
</body>
</html>

