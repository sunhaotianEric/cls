<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>	
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
    
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>在线会员列表</title>
        <jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
    <body class="gray-bg">
        <div class="wrapper wrapper-content animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
                    <div class="ibox ">
                        <div class="ibox-content">
                            <div class="panel-body">
                                <form class="form-horizontal" id="queryForm">
                                
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">用户名</span>
                                                <input type="text" value="" id='teamUserListUserName' class="form-control"></div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">注册时间始终</span>
                                                <div class="input-daterange input-group">
                                                    <input class="laydate-icon form-control layer-date" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" placeholder="开始日期" id="teamUserListRegisterDateStart">
                                                    <span class="input-group-addon">到</span>
                                                    <input class="laydate-icon form-control layer-date" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})" placeholder="结束日期" id="teamUserListRegisterDateEnd"></div>
                                            </div>
                                            
                                        </div>
                                    
                                    </div>
                                    <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">账户余额始</span>
                                                <input type="text" value="" onkeyup="checkNum(this)" id="teamUserListBalanceStart" class="form-control"></div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">账户余额终</span>
                                                <input type="text" value="" onkeyup="checkNum(this)" id="teamUserListBalanceEnd" class="form-control"></div>
                                        </div>
	                                    <div class="col-sm-4">
	                                            <div class="input-group m-b">
	                                                <span class="input-group-addon">商户</span>
	                                                <cls:bizSel name="bizSystem" id="bizSystem" options="class:'ipt form-control'"/> 
	                                                </div>
	                                     </div>
                                    </div>
                                    </c:if>
                                    <c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">账户余额始</span>
                                                <input type="text" value="" onkeyup="checkNum(this)" id="teamUserListBalanceStart" class="form-control"></div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">账户余额终</span>
                                                <input type="text" value="" onkeyup="checkNum(this)" id="teamUserListBalanceEnd" class="form-control"></div>
                                        </div>
	                                    <div class="col-sm-4">
	                                        
	                                     </div>
                                    </div>
                                    </c:if>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">在线类型</span>
                                                <select name="onlineType" id="onlineType" class="ipt form-control">
                                                     <option value="">所有在线</option>
                                                    <option value="PC">电脑</option>
                                                    <option value="MOBILE">手机</option></select>
                                            </div>
                                        </div>
                                       <div class="col-sm-3">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">用户级别</span>
                                                <select name="dailiLevel" id="teamUserListDailiLevel"  class="ipt form-control">
                                                    <option value="">所有</option>
                                                    <option value="GENERALAGENCY">总代理</option>
                                                    <option value="ORDINARYAGENCY">普通代理</option>
                                                    <option value="REGULARMEMBERS">普通会员</option></select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">排序</span>
                                                <select name="teamUserListOrderSort" id="teamUserListOrderSort" class="ipt form-control">
                                                    <option value="1">默认排序</option>
                                                    <option value="3">账户余额</option>
                                                    <option value="5">注册时间</option></select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group nomargin text-right">
                                                <button type="button" class="btn btn-outline btn-default btn-kj" onclick="userOnlinePage.findUserOnlinePageByQueryParam()">查询</button></div>
                                        </div>
                                    </div>
                                </form>
                                <div class="jqGrid_wrapper">
                                    <div class="ui-jqgrid ">
                                        <div class="ui-jqgrid-view">
                                            <div class="ui-jqgrid-hdiv">
                                                <div class="ui-jqgrid-hbox" style="padding-right:0">
                                                    <table class="ui-jqgrid-htable ui-common-table table table-bordered" id="userOnlineTable">
                                                        <thead>
                                                            <tr class="ui-jqgrid-labels">
                                                                <th class="ui-th-column ui-th-ltr">序号</th>
                                                                <th class="ui-th-column ui-th-ltr">商户</th>
                                                                <th class="ui-th-column ui-th-ltr">用户名</th>
                                                                <th class="ui-th-column ui-th-ltr">余额</th>
                                                                <th class="ui-th-column ui-th-ltr">注册时间</th>
                                                                <th class="ui-th-column ui-th-ltr">在线</th>
                                                                <th class="ui-th-column ui-th-ltr">最后登录时间</th>
                                                               <!--  <th class="ui-th-column ui-th-ltr">队员</th> -->
                                                                </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
 <!-- 业务js -->
<script type="text/javascript" src="<%=path%>/admin/user/js/user_online.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerUserOnlineAction.js'></script>   
</body>
</html>
	