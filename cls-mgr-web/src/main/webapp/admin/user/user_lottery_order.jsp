<%@ page language="java" contentType="text/html; charset=UTF-8"
 import = "com.team.lottery.enums.ELotteryModel"
 pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
 <%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String isTourist = request.getParameter("isTourist");
	request.setAttribute("isTourist", isTourist);
%>
<!DOCTYPE html>
<html>  
    <head>
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	      <title>商户投注明细</title>
	      <link rel="shortcut icon" href="favicon.ico">
	      <jsp:include page="/admin/include/include.jsp"></jsp:include>
	      <style type="text/css">
	      .cp-yellow{color:#f8a525}
	      .cp-red{color:red}
	      .cp-green{color:green}
	      </style>
	      <script type="text/javascript">
	      	var isTouristParameter = '<%=isTourist %>';
	      </script>
</head>
  <body>
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
                    <div class="ibox ">
                        <div class="ibox-content">
                            <div class="row m-b-sm m-t-sm">
                                <form class="form-horizontal" id="queryForm">
                                <c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">彩种</span>                              
                                                 <cls:enmuSel name="lotteryType" className="com.team.lottery.enums.ELotteryKind" id="lotteryType" options="class:'ipt form-control'"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">期号</span>
                                                <input type="text" value="" id='expect' class="form-control"></div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">用户名</span>
                                                <input type="text" value="" id='userName' class="form-control"></div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">注单号</span>
                                                <input type="text" value=""  id='lotteryId' class="form-control"></div>
                                        </div>
                                        
                                    </div>
                                  
                                    <div class="row">
                                         <div class="col-sm-2">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">开奖状态</span>
                                                <cls:enmuSel name="prostateStatus" className="com.team.lottery.enums.EProstateStatus" id="prostateStatus" options="class:'ipt form-control'"/>
                                            </div>
                                        </div>
                                       
                                        <div class="col-sm-2">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">来源</span>
                                                <select id="fromType" class="ipt form-control">
                                                    <option value="">=请选择=</option>
                                                    <option value="PC">电脑</option>
                                                    <option value="MOBILE">手机web</option>
                                					<option value="APP">APP</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">排序</span>
                                                 <select id='orderSort' class="ipt form-control">
								                       <option value="">=请选择=</option>
								                       <option value="1">奖金高至低</option>
								                       <option value="2">奖金低至高</option>
								                       <option value="3">金额高至低</option>
								                       <option value="4">金额低至高</option>
								                       <option value="5">时间降序</option>
								                       <option value="6">时间升序</option>
								                   </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">时间始末</span>
                                                <div class="input-daterange input-group" id="datepicker">
                                                    <input name ="createdDateStart" id="createdDateStart" class="form-control layer-date" placeholder="开始日期" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})">
                                                    <span class="input-group-addon">到</span>
                                                    <input name ="createdDateEnd" id="createdDateEnd" class="form-control layer-date" placeholder="结束日期" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
				                        	<div class="form-group nomargin">
									            <button type="button" class="btn btn-xs btn-info" onclick="userLotteryOrder.getAllLotteryOrdersForQuery(1)">今 天</button>
									            <button type="button" class="btn btn-xs btn-danger" onclick="userLotteryOrder.getAllLotteryOrdersForQuery(-1)">昨 天</button>
									        	<button type="button" class="btn btn-xs btn-warning" onclick="userLotteryOrder.getAllLotteryOrdersForQuery(-7)">最近7天</button><br/>
									        	<button type="button" class="btn btn-xs btn-success" onclick="userLotteryOrder.getAllLotteryOrdersForQuery(30)">本 月</button>
									        	<button type="button" class="btn btn-xs btn-primary" onclick="userLotteryOrder.getAllLotteryOrdersForQuery(-30)">上 月</button>
				                            </div>
				                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group nomargin text-right">
                                                <button type="button" class="btn btn-outline btn-default btn-kj" onclick="userLotteryOrder.getAllLotteryOrdersForQuery()"><i class="fa fa-search"></i>&nbsp;查 询</button></div>
                                        </div>
                                    </div>
                                  
                                    </c:if>
                                    <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                                       <div class="row">
                                         <div class="col-sm-2">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">商户</span>
                                                 <cls:bizSel name="bizSystem" id="bizSystem" options="class:'ipt form-control'"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">彩种</span>                              
                                                 <cls:enmuSel name="lotteryType" className="com.team.lottery.enums.ELotteryKind" id="lotteryType" options="class:'ipt form-control'"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">期号</span>
                                                <input type="text" value="" id='expect' class="form-control"></div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">用户名</span>
                                                <input type="text" value="" id='userName' class="form-control"></div>
                                        </div>
                                         <div class="col-sm-2">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">注单号</span>
                                                <input type="text" value=""  id='lotteryId' class="form-control"></div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">开奖状态</span>
                                                <cls:enmuSel name="prostateStatus" className="com.team.lottery.enums.EProstateStatus" id="prostateStatus" options="class:'ipt form-control'"/>
                                            </div>
                                        </div>
                                     
                                    </div>
                                   
                                    
                                    <div class="row">
                                        
                                        <div class="col-sm-2">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">订单来源</span>
                                                <select id="fromType" class="ipt form-control">
                                                    <option value="">=请选择=</option>
                                                    <option value="PC">电脑</option>
                                                    <option value="MOBILE">手机web</option>
                                					<option value="APP">APP</option>
                                                </select>
                                            </div>
                                        </div>
                                         <div class="col-sm-2">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">排序</span>
                                                 <select id='orderSort' class="ipt form-control">
								                       <option value="">=请选择=</option>
								                       <option value="1">奖金高至低</option>
								                       <option value="2">奖金低至高</option>
								                       <option value="3">金额高至低</option>
								                       <option value="4">金额低至高</option>
								                       <option value="5">时间降序</option>
								                       <option value="6">时间升序</option>
								                   </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-5">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">时间始末</span>
                                                <div class="input-daterange input-group" id="datepicker">
                                                    <input name ="createdDateStart" id="createdDateStart" class="form-control layer-date" placeholder="开始日期" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})">
                                                    <span class="input-group-addon">到</span>
                                                    <input name ="createdDateEnd" id="createdDateEnd" class="form-control layer-date" placeholder="结束日期" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"></div>
                                            </div>
                                        </div>
                                      	<div class="col-sm-2" style="padding-left:5px;">
				                        	<div class="form-group nomargin">
									            <button type="button" class="btn btn-xs btn-info" onclick="userLotteryOrder.getAllLotteryOrdersForQuery(1)">今 天</button>
									            <button type="button" class="btn btn-xs btn-danger" onclick="userLotteryOrder.getAllLotteryOrdersForQuery(-1)">昨 天</button>
									        	<button type="button" class="btn btn-xs btn-warning" onclick="userLotteryOrder.getAllLotteryOrdersForQuery(-7)">最近7天</button><br/>
									        	<button type="button" class="btn btn-xs btn-success" onclick="userLotteryOrder.getAllLotteryOrdersForQuery(30)">本 月</button>
									        	<button type="button" class="btn btn-xs btn-primary" onclick="userLotteryOrder.getAllLotteryOrdersForQuery(-30)">上 月</button>
				                            </div>
				                        </div>
                                      	
                                        <div class="col-sm-1">
                                            <div class="form-group nomargin text-right">
                                                <button type="button" class="btn btn-outline btn-default btn-kj" onclick="userLotteryOrder.getAllLotteryOrdersForQuery()"><i class="fa fa-search"></i>&nbsp;查 询</button></div>
                                        </div>
                                    </div>
                                   <%-- <div class="row">
                                         <div class="col-sm-3">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">中奖状态</span>
                                                <cls:enmuSel name="prostateStatus" className="com.team.lottery.enums.EProstateStatus" id="prostateStatus" options="class:'ipt form-control'"/>
                                            </div>
                                        </div> 

                                    </div>--%>
                                     </c:if>
                                     <c:if test="${isTourist!=1}">
                                     <div class="row">
                                     <div class="col-sm-1">
                                            <div class="form-group nomargin text-right">
                                                <button type="button" class="btn btn-outline btn-default btn-kj" onclick="userLotteryOrder.orderInfoExport()"><i class="fa fa-search"></i>&nbsp;导出数据</button></div>
                                        </div>
                                     </div>
                                    </c:if>
                                </form>
                            </div>
                            <div class="jqGrid_wrapper">
                                <div class="ui-jqgrid ">
                                    <div class="ui-jqgrid-view">
                                        <div class="ui-jqgrid-hdiv">
                                            <div class="ui-jqgrid-hbox" style="padding-right:0">
                                                <table class="ui-jqgrid-htable ui-common-table table table-bordered" id="lotteryOrderTable" width="100%">
                                                    <thead>
                                                        <tr class="ui-jqgrid-labels">
                                                           <th class="ui-th-column ui-th-ltr">商户</th>
                                                            <th class="ui-th-column ui-th-ltr">注单号</th>
                                                            <th class="ui-th-column ui-th-ltr">彩种</th>
                                                            <th class="ui-th-column ui-th-ltr">期号</th>
                                                            <th class="ui-th-column ui-th-ltr">用户名</th>
                                                            <th class="ui-th-column ui-th-ltr">金额</th>
                                                            <th class="ui-th-column ui-th-ltr">倍数</th>
                                                            <!-- <th class="ui-th-column ui-th-ltr">比对</th> -->
                                                            <th class="ui-th-column ui-th-ltr">追号</th>
                                                            <th class="ui-th-column ui-th-ltr">订单状态</th>
                                                            <th class="ui-th-column ui-th-ltr">开奖状态</th>
															<th class="ui-th-column ui-th-ltr">奖金</th>
                                                            <th class="ui-th-column ui-th-ltr">号码</th>
                                                            <th class="ui-th-column ui-th-ltr">下注时间</th>
                                                           <!--  <th class="ui-th-column ui-th-ltr">备注</th> -->
                                                            <th class="ui-th-column ui-th-ltr">来源</th>
                                                           <!--  <th class="ui-th-column ui-th-ltr">未开奖回退</th> -->
                                                            <th class="ui-th-column ui-th-ltr">操  作</th></tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       <!--订单详情弹出层-->
        <div id="order-form" class="modal fade" aria-hidden="true">
            <div class="modal-dialog" style="width:800px;">
                <div class="modal-content" style="max-height:500px;overflow:auto;">
                   <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                       <h4 class="modal-title" id="myModalLabel">订单详情</h4>
                       </div>
                    <div class="modal-body">
                        <div class="input-group m-b">
                            <span class="input-group-addon">开奖号码</span>
                            <input id="form_openCode" type="text"  value="" class="form-control" readonly="readonly" ></div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">奖金</span>
                            <input id="form_winCost" type="text" value="" class="form-control" readonly="readonly"></div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">奖金模式</span>
                            <input id="form_awardModel" type="text" value="" class="form-control" readonly="readonly"></div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">总注数</span>
                            <input id="form_cathecticCount" type="text" value="" class="form-control" readonly="readonly"></div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">模式</span>
                            <input id="form_lotteryModel" type="text" value="" class="form-control" readonly="readonly"></div>
                        <div class="input-group m-b">
                            <span class="input-group-addon">号码详情</span>
                            <textarea id='form_codeDetail' readonly="readonly"  class="form-control"></textarea>
                            <textarea id='form_cacheCodeDetail' readonly='readonly' style="display: none;" class="form-control"></textarea>
                            </div>
                    </div>
                </div>
            </div>
        </div>        
<!-- js -->
<script type="text/javascript" src="<%=path%>/admin/user/js/user_lottery_order.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerOrderAction.js'></script>
<script type='text/javascript' src='<%=path%>/dwr/interface/managerUserAction.js'></script>

</body>
</html>

