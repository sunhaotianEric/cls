<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<html>
<head>
<meta charset="utf-8" />
<title>商户资金明细</title>
<jsp:include page="/admin/include/include.jsp"></jsp:include>
<!-- js -->
<script type="text/javascript" src="<%=path%>/admin/user/js/user_regFrom_update.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerUserAction.js'></script>

<style type="text/css">
caption, th {
    text-align: center;
}

.tb td{
    text-align: center;
}

.ui-dialog{
    width : auto;
}
</style>
<script type="text/javascript">
    $(function () {

    });
</script>
<%
 String fromUserName = request.getParameter("param1");  //获取查询的商品ID
%>
</head>
<body>
   <div class="alert alert-info">当前位置<b class="tip"></b><a href="/managerzaizst/user/user_list.html">会员信息管理</a><b class="tip"></b>推荐人变更</div>
    <table class="tbform">
        <tbody>
            <tr>
                <td class="tdl">当前要变更的用户 </td>
                <td>
                   <label id='fromUserName'></label>
                </td>
                <td class="tdl">转移至用户</td>
                <td>
                    <input type="text" id='toUserName' >
                </td>  
                <td colspan="12" align="right">
                    <input class="btn" id="updateButton" type="button" onclick="userRegFromUpdate.updateRegFrom()" value="变更" />
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>
<script type="text/javascript">
var fromUserName = '<%=fromUserName %>';
$('#fromUserName').text(fromUserName);
</script>

