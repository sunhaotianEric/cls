<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.util.ConstantUtil,com.team.lottery.vo.Admin" %>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
	Admin admin = (Admin)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_ADMIN);
	String isTourist = request.getParameter("isTourist");
%>
<!DOCTYPE html>
<html>
<head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>会员信息管理</title>
     <jsp:include page="/admin/include/include.jsp"></jsp:include>
          <link rel="shortcut icon" href="favicon.ico">
     <style type="text/css">
			/* th, td { white-space: nowrap; }
			div.dataTables_wrapper { width: 1000px; margin: 0 auto; } */
       		.jsstar { list-style: none; margin: 0px; padding: 0px; width: 100px; height: 20px; position: relative; display: inline-block; }
			.jsstar li { padding: 0px; margin: 0px; float: left; width: 20px; height: 20px; background: url(../../admin/resource/img/star_rating.gif) 0 0 no-repeat; }
			.jsstar_bright { background-position: left bottom; }
			.mt5{margin-top:5px;margin-bottom: 10px;}
     </style>
     <script type="text/javascript">
			var isTouristParameter = '<%=isTourist %>';
     </script>       
</head>
    <body>
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
                    <div class="ibox ">
                        <div class="ibox-title p4-8" style="padding:14px 20px">
                            <%  
                            if(!"SUPER_SYSTEM".equals(admin.getBizSystem())&&(admin.getState().equals(0) || admin.getState().equals(1))&&("0".equals(isTourist)||isTourist==null)){ 
                            %>
                            <a  class="btn-dc btn btn-primary" onclick="userList.showUserAdd()"  target="Conframe" ><i class='fa fa-plus'></i>&nbsp;添加直属会员</a>
		                    <%
		                    	}
		                    %>
		                    <%if("0".equals(isTourist)||isTourist==null){ %>
		                    <a class="btn-xf btn btn-primary" onclick="userList.gotohref('<%=path %>/managerzaizst/user/user_recharge.html','续费处理')"><i class='fa fa-rmb'></i>&nbsp;续费处理</a>
                           <a class="btn-dc btn btn-primary"  onclick="userList.gotohref('<%=path %>/managerzaizst/user/user_note_send.html','站内信发送')" target="Conframe"><i class='fa fa-envelope'></i>&nbsp;站内信发送</a> 
                          <%--   <%  
                        	 if(admin.getState() == Admin.SUPER_STATE){ 
		                    %>
		                    <a class="btn-dc btn btn-primary" onclick="userList.gotohref('<%=path %>/managerzaizst/user/user_info_export.html','会员信息导出')"><i class='fa fa-download'></i>&nbsp;会员信息导出</a>
		                    <%
		                    	}
		                    %> --%>
                            <a class="btn-zx btn btn-primary" onclick="userList.showOnlineUsers()"><i class='fa fa-user'></i>&nbsp;在线会员</a>
                            <a class="btn-tj btn btn-primary" onclick="userList.gotohref('<%=path %>/managerzaizst/user/user_day_online_info.html','每日在线统计报表')"><i class='fa fa-bar-chart'></i>&nbsp;每日在线统计报表</a>
                            <a class="btn-model btn btn-primary" onclick="userList.adjustUserModel()"><i class='fa fa-adjust'></i>&nbsp;会员模式调整</a>
                            <a class="btn-model btn btn-primary" onclick="userList.starLevelAdjust()"><i class='fa fa-star'></i>&nbsp;会员星级调整</a>
                            <a class="btn-model btn btn-primary" onclick="userList.importingMembersData()"><i class='fa fa-upload'></i>&nbsp;导入会员数据</a>
                            <%}else{ %>
                            <a class="btn-model btn btn-primary" onclick="userList.batchDeleting()"><i class='fa fa-trash'></i>&nbsp;批量删除试玩账号</a>
                            <%} %>
                            </div>
                        <div class="ibox-content bd0 pt0">
                            <div class="row">
                                <form class="form-horizontal" id="queryForm">
                                    <div class="row">
                                      <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                                        <div class="col-sm-4">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">商户</span>
                                                <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">用户名</span>
                                                <input id='userName' name="userName" type="text" value="" class="form-control" ></div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">排序</span>
                                                  <select id="fieldsorting" name="fieldsorting" class="ipt form-control">
							                        <option value="queryByLogtime">按最近登录排列</option>
							                        <option value="queryByUserName">按名字排列</option>
							                        <option value="queryByMoney">按金额排列</option>
							                        <option value="queryBySscRebate">按会员返点</option>
							                        <option value="queryByAgent">按代理排列</option>
							                        <option value="queryByRegisterDate">按注册时间排列</option>
							                        <option value="queryByLogins">按登陆次数排列</option>
							                        <option value="queryByStarLevel">按用户星级</option>
							                        <option value="queryByLock">按登陆锁定</option>
							                        <option value="queryByWithdrawLock">按提现锁定</option>
							                        <option value="queryByVipLevel">按VIP等级</option>
									   			  </select>
                                            </div>
                                        </div>
                                        <%if("0".equals(isTourist)||isTourist==null){ %>
                                        <div class="col-sm-4">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">姓名</span>
                                                <input id='trueName' name="trueName" type="text" value="" class="form-control" ></div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">手机号码</span>
                                                <input id='phone' name="phone" type="text" value="" class="form-control" ></div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">QQ</span>
                                                <input id='qq' name="qq" type="text" value="" class="form-control" ></div>
                                        </div>
                                        <%} %>
                                        <%if("0".equals(isTourist)||isTourist==null){ %>
                                        <div class="col-sm-11">
 											<h4>
				                            	<span >电脑在线人数:</span> <span style='color:red' id="allUserCount">加载中...</span> 
				                            	<span style='margin-left: 20px;'> 手机web在线人数:</span> <span style='color:red' id="mobileUserCount">加载中...</span>
				                            	<span style='margin-left: 20px;'> App在线人数:</span> <span style='color:red' id="appUserCount">加载中...</span>
				                           	 	<button class="btn btn-primary" type="button" style="margin-left: 20px;" onclick="userList.getUsersCount();">刷新人数</button> 
				                            </h4>      	                                 
                                        </div>
                                        <%} %>
                                        <div class="col-sm-1">
                                            <div class="form-group mt5 text-right">
                                                <button type="button" class="btn btn-outline btn-default btn-kj" onclick="userList.getAllUsersByCondition()"><i class="fa fa-search"></i>&nbsp;查 询</button></div>
                                        </div>
                                        </c:if>
                                 <c:if test="${admin.bizSystem !='SUPER_SYSTEM'}">
                                      
                                        <div class="col-sm-4">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">用户名</span>
                                                <input id='userName' name="userName" type="text" value="" class="form-control" ></div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">排序</span>
                                                  <select id="fieldsorting" name="fieldsorting" class="ipt form-control">
							                        <option value="queryByLogtime">按最近登录排列</option>
							                        <option value="queryByUserName">按名字排列</option>
							                        <option value="queryByMoney">按金额排列</option>
							                        <option value="queryBySscRebate">按会员返点</option>
							                        <option value="queryByAgent">按代理排列</option>
							                        <option value="queryByRegisterDate">按注册时间排列</option>
							                        <option value="queryByLogins">按登陆次数排列</option>
							                        <option value="queryByLock">按锁定会员</option>
							                        <option value="queryByStarLevel">按用户星级</option>
							                        <option value="queryByVipLevel">按VIP等级</option>
									   			  </select>
                                            </div>
                                        </div>
                                        <%if("0".equals(isTourist)||isTourist==null){ %>
                                        <div class="col-sm-4">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">姓名</span>
                                                <input id='trueName' name="trueName" type="text" value="" class="form-control" ></div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">手机号码</span>
                                                <input id='phone' name="phone" type="text" value="" class="form-control" ></div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">QQ</span>
                                                <input id='qq' name="qq" type="text" value="" class="form-control" ></div>
                                        </div>
                                        <%} %>
                                        <div class="col-sm-1">
                                            <div class="form-group nomargin text-right">
                                                <button type="button" class="btn btn-outline btn-default btn-kj" onclick="userList.getAllUsersByCondition()"><i class="fa fa-search"></i>&nbsp;查 询</button></div>
                                        </div>
                                        </c:if>
                                    </div>
                                </form>
                            </div>
                            <c:if test="${admin.bizSystem !='SUPER_SYSTEM'}">
                            <%if("0".equals(isTourist)||isTourist==null){ %>
                            <div class="row">
                            	<div class="col-sm-12">
                             		<h4>
		                            	<span >电脑在线人数:</span> <span style='color:red' id="allUserCount">加载中...</span> 
		                            	<span style='margin-left: 20px;'> 手机web在线人数:</span> <span style='color:red' id="mobileUserCount">加载中...</span>
		                            	<span style='margin-left: 20px;'> App在线人数:</span> <span style='color:red' id="appUserCount">加载中...</span>
		                           	 	<button class="btn btn-primary" type="button" style="margin-left: 20px;" onclick="userList.getUsersCount();">刷新人数</button> 
		                            </h4>
                            	</div>
                            </div>
                            <%} %>
                            </c:if>
                            <div class="jqGrid_wrapper">
                                <div class="ui-jqgrid ">
                                    <div class="ui-jqgrid-view">
                                        <div class="ui-jqgrid-hdiv">
                                            <div class="ui-jqgrid-hbox" style="padding-right:0">
                                                <table class="ui-jqgrid-htable ui-common-table table table-bordered" id="userlistTable">
                                                <thead>
                                                        <tr class="ui-jqgrid-labels">
                                                            <th class="ui-th-column ui-th-ltr">ID</th>
                                                            <th class="ui-th-column ui-th-ltr">商户</th>
                                                            <th class="ui-th-column ui-th-ltr">用户名</th>
                                                            
                                                            <th class="ui-th-column ui-th-ltr">余额/可提现</th>
                                                            <th class="ui-th-column ui-th-ltr">模式</th>
                                                            <th class="ui-th-column ui-th-ltr">代理级别</th>
                                                            <th class="ui-th-column ui-th-ltr">VIP等级</th>
                                                            <th class="ui-th-column ui-th-ltr">用户星级</th>
                                                            <th class="ui-th-column ui-th-ltr">登陆次数</th>
                                                            <th class="ui-th-column ui-th-ltr">状态</th>
                                                            <th class="ui-th-column ui-th-ltr">在线</th>
                                                            <th class="ui-th-column ui-th-ltr">操作</th></tr>
                                                    </thead>                            
                                                    <tbody>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  
  
<!-- js -->
<script type="text/javascript" src="<%=path%>/admin/user/js/user_list.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerUserAction.js'></script>
</body>
</html>

