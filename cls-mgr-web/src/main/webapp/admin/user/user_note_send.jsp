<%@ page language="java" contentType="text/html; charset=UTF-8" 
	import = "com.team.lottery.enums.EHelpType" 
	pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
    
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>站内信发送</title>
        <link href="<%=path%>/css/plugins/summernote/summernote.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" rel="stylesheet">
        <link href="<%=path%>/css/plugins/summernote/summernote-bs3.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" rel="stylesheet">
         <link href="<%=path%>/css/plugins/iCheck/custom.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" rel="stylesheet">
    <jsp:include page="/admin/include/include.jsp"></jsp:include>

</head>
    <body class="gray-bg">
        <div class="wrapper wrapper-content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="ibox ">
                        <div class="ibox-content">
                            <form class="form-horizontal" id="sendform">
                            <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                               <div class="form-group">
                                    <label class="col-sm-2 control-label">商户：</label>
                                    <div class="col-sm-5">
                                      <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt"/>
                                    </div>
                                </div>
                               </c:if>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">发送对象：</label>
                                    <div class="col-sm-1 radio i-checks">
                                        <label class="nopadding">
                                            <input type="radio" name="sendType" value="4">
                                            <i>
                                            </i>按用户名</label>
                                    </div>
                                    <div class="col-sm-3" style="text-align: left;">
                                        <input id="userName" type="text" value="" class="form-control"></div>
                                     <div class="col-sm-1"></div>
                                    <div class="col-sm-1 radio i-checks">
                                        <label class="nopadding">
                                            <input type="radio" name="sendType" id="sendTypeStarLevel" value="1">
                                            <i>
                                            </i>按用户星级</label>
                                    </div>
                                   
                                    <div class="col-sm-3">
                                    		<select class="ipt" id="starLevel">
                                    		    <option value='0' selected="selected">无</option>
												<option value='1' >一星</option>
												<option value='2'>二星</option>
												<option value='3'>三星</option>
												<option value='4'>四星</option>
												<option value='5'>五星</option>
											</select>
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-1 radio i-checks">
                                        <label class="nopadding">
                                            <input type="radio" name="sendType" value="2">
                                            <i>
                                            </i>按代理级别</label>
                                    </div>
                                    <div class="col-sm-3">
						   				<select class="ipt" id='dailiLevel'>
					                        <option value='DIRECTAGENCY' selected="selected">直属代理</option>
					                        <option value='GENERALAGENCY'>总代理</option>
					                        <option value='ORDINARYAGENCY'>普通代理</option>
					                        <option value='REGULARMEMBERS'>普通会员</option>
					                   </select>  
                                    </div>
                                     <div class="col-sm-1"></div>
                                    <div class="col-sm-1 radio i-checks">
                                        <label class="nopadding">
                                            <input type="radio" name="sendType" value="3">
                                            <i>
                                            </i>按时时彩模式</label>
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" id="sscrebate" onkeyup="checkNum(this)"class="form-control"></div>
                                        <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">对象过滤条件：</label>
                                    <div class="col-sm-5">
                                        <select class="ipt" id="filterCondition">
                                            <option value="1" selected="selected">等于</option>
                                            <option value="2">小于等于</option>
                                            <option value="3">大于等于</option></select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">站内信主题：</label>
                                    <div class="col-sm-5">
                                        <input id="sub" type="text" value="" class="form-control"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">内容：</label>
                                    <div class="col-sm-10">
                                    <textarea name="body" id="body" style="width:600px;height:200px;"></textarea>
                                     <!--    <div class="row">
                                            <div class="col-sm-12">
                                                <div class="float-e-margins">
                                                    <div class="ibox-content no-padding">
                                                        <div class="summernote"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12 ibox">
                                        <div class="row">
                                            <div class="col-sm-6 text-center">
                                                <button type="button" class="btn btn-w-m btn-white btn－tj" onclick="userNoteSendPage.userNoteSend()">提 交</button></div>
                                            <div class="col-sm-3 text-center">
                                                <input class="btn btn-w-m btn-white" type="reset" id="resetBtn" value="重置"/>
                                                </div>
                                                <div class="col-sm-3 text-center">
            
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/user/js/user_note_send.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerNotesAction.js'></script>
    <script src="<%=path%>/js/plugins/summernote/summernote.min.js"></script>
    <script src="<%=path%>/js/plugins/summernote/summernote-zh-CN.js"></script>  

      <script>
         $(document).ready(function() {
                $(".i-checks").iCheck({
                    checkboxClass: "icheckbox_square-green",
                    radioClass: "iradio_square-green",
                })
            });
      </script>
<!--     <script>
		var editor;  //editor.html()
		KindEditor.ready(function(K) {
			editor = K.create('textarea[name="content"]', {
				allowFileManager : false
			});
		});
	</script> -->
</body>
</html>

