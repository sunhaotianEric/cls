<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
    
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <title>会员总额报表</title>
<style type="text/css">
.wrapper {padding:0 10px}
.wrapper-content {padding:10px}
</style>
<jsp:include page="/admin/include/include.jsp"></jsp:include>

</head>
    <body class="gray-bg">
        <div class="wrapper wrapper-content animated fadeIn" style="padding: 0 5px;">
            <div class="row">
                <div class="col-sm-12">
                    <!-- <div class="ibox "> -->
                        <div class="ibox-content">
                            <div class="row m-b-sm m-t-sm">
                                <form class="form-horizontal">
                                <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                                    <div class="row">
                                      <div class="col-sm-3">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">商户</span>
                                                <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
                                                </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">用户名</span>
                                                <input id="userName" type="text" value="" class="form-control"></div>
                                        </div>
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-3">
                                            <div class="form-group nomargin text-right">
                                                <button type="button" class="btn btn-outline btn-default btn-kj" onclick="userConsumeReport.getTotalUsersConsumeReportsForQuery()"><i class="fa fa-search"></i>&nbsp;查 询</button></div>
                                        </div>
                                    </div>
                                    </c:if>
                                      <c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
                                    <div class="row">
                                        <div class="col-sm-9">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">用户名</span>
                                                <input id="userName" type="text" value="" class="form-control" width="40%"></div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group nomargin text-right">
                                                <button type="button" class="btn btn-outline btn-default btn-kj" onclick="userConsumeReport.getTotalUsersConsumeReportsForQuery()"><i class="fa fa-search"></i>&nbsp;查 询</button></div>
                                        </div>
                                    </div>
                                    </c:if>
                                </form>
                            </div>
                            <div class="jqGrid_wrapper">
                                <div class="ui-jqgrid ">
                                    <div class="ui-jqgrid-view">
                                        <div class="ui-jqgrid-hdiv">
                                            <div class="ui-jqgrid-hbox" style="padding-right:0">
                                                <table class="ui-jqgrid-htable ui-common-table table table-bordered" id="userMoneyTotalTable">
                                                    <thead>
                                                        <tr class="ui-jqgrid-labels">
                                                        <th class="ui-th-column ui-th-ltr" >商户</th>
                                                          <th class="ui-th-column ui-th-ltr">用户名</th>
												          <th class="ui-th-column ui-th-ltr">余额</th>
												          <th class="ui-th-column ui-th-ltr">充值</th>
												          <th class="ui-th-column ui-th-ltr">充值赠送</th>
												          <th class="ui-th-column ui-th-ltr">活动彩金</th>
												          <th class="ui-th-column ui-th-ltr">商户续费</th>
												          <th class="ui-th-column ui-th-ltr">商户扣费</th>
												          <th class="ui-th-column ui-th-ltr">取款</th>
												          <th class="ui-th-column ui-th-ltr">取款手续费</th>
												          <!-- <th class="ui-th-column ui-th-ltr">注册</th> -->
												          <th class="ui-th-column ui-th-ltr">支出转账</th>
												          <th class="ui-th-column ui-th-ltr">收入转账</th>
												          <!-- <th class="ui-th-column ui-th-ltr">推广(总)</th> -->
												          <th class="ui-th-column ui-th-ltr">投注(-撤单)</th>
												          <th class="ui-th-column ui-th-ltr">中奖</th>
												          <th class="ui-th-column ui-th-ltr">投注返点</th>
												          <th class="ui-th-column ui-th-ltr">推广返点</th>
												          <th class="ui-th-column ui-th-ltr">分红</th>
												          <!-- <th class="ui-th-column ui-th-ltr">日分红</th> 
												          <th class="ui-th-column ui-th-ltr">月工资</th>-->
												          <th class="ui-th-column ui-th-ltr">工资</th>
												          <!-- <th class="ui-th-column ui-th-ltr">日佣金</th>
												          <th class="ui-th-column ui-th-ltr">广告费(佣金+工资+分红)</th> -->
												          <th class="ui-th-column ui-th-ltr">盈利</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                         
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>          
                                </div>
                            </div>
                        </div>
                   <!--  </div> -->
                </div>
            </div>
        </div>
<!-- js -->
<script type="text/javascript" src="<%=path%>/admin/user/js/user_account_report.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerUserAction.js'></script>
</body>
</html>

