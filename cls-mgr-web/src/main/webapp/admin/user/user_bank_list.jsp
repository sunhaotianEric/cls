<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>用户银行卡</title>
<jsp:include page="/admin/include/include.jsp"></jsp:include>

</head>
<body>
	<div class="animated fadeIn">
		<div class="ibox-content ibox">
			<div class="row m-b-sm m-t-sm">
				<form class="form-horizontal" id="userBankForm">
					<div class="row">
						<c:choose>
							<c:when test="${admin.bizSystem=='SUPER_SYSTEM'}">
								<div class="col-sm-2">
									<div class="input-group m-b">
										<span class="input-group-addon">商户</span>
										<cls:bizSel name="bizSystem" id="bizSystem"
											options="class:ipt form-control" />
									</div>
								</div>
								<div class="col-sm-2">
									<div class="input-group m-b">
										<span class="input-group-addon">用户名</span> <input type="text"
											value="" name="userName" id="userName" class="form-control">
									</div>
								</div>
								<div class="col-sm-2">
									<div class="input-group m-b">
										<span class="input-group-addon">开户人</span> <input type="text"
											value="" name="bankAccountName" id="bankAccount" class="form-control">
									</div>
								</div>
								<div class="col-sm-3">
									<div class="input-group m-b">
										<span class="input-group-addon">卡号</span> <input type="text"
											value="" name="bankCardNum" id="bankNum" class="form-control">
									</div>
								</div>
								<div class="col-sm-2">
						                     <div class="input-group m-b">
						                        <span class="input-group-addon">创建时间</span>
						                        <div class="input-daterange input-group">
						                        <input class="form-control layer-date" placeholder="创建时间" onclick="laydate({istime: false, format: 'YYYY-MM-DD',max: laydate.now(0)})" name="belongDateStart" id="belongDateStart">
						                      	</div>
						                    </div>
						                </div>
							</c:when>
							<c:otherwise>
								<div class="col-sm-3">
									<div class="input-group m-b">
										<span class="input-group-addon">用户名</span> <input type="text"
											value="" name="userName" id="userName" class="form-control">
									</div>
								</div>
								<div class="col-sm-3">
									<div class="input-group m-b">
										<span class="input-group-addon">开户人</span> <input type="text"
											value="" name="bankAccount" id="bankAccount" class="form-control">
									</div>
								</div>
								<div class="col-sm-3">
									<div class="input-group m-b">
										<span class="input-group-addon">卡号</span> <input type="text"
											value="" name="bankNum" id="bankNum" class="form-control">
									</div>
								</div>
								<div class="col-sm-2">
								<div class="input-group m-b">
						                        <span class="input-group-addon">创建时间</span>
						                        <div class="input-daterange input-group">
						                        <input class="form-control layer-date" placeholder="创建时间" onclick="laydate({istime: false, format: 'YYYY-MM-DD',max: laydate.now(0)})" name="belongDateStart" id="belongDateStart">
						                      	</div>
						                    </div>
								</div>
							</c:otherwise>
						</c:choose>

						<div class="col-sm-1">
							<div class="form-group nomargin text-right">
								<button type="button" class="btn btn-outline btn-default"
									onclick="userBankListPage.getTotalUserBanksForQuery()">
									<i class="fa fa-search"></i>&nbsp;查 询
								</button>
							</div>
						</div>
						<div class="col-sm-1">
							<div class="form-group nomargin text-right">
								<button type="button" class="btn btn-outline btn-default"
									onclick="userBankListPage.addUserBank()">
									<i class="fa fa-search"></i>&nbsp;新 增
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="jqGrid_wrapper">
				<div class="ui-jqgrid ">
					<div class="ui-jqgrid-view ">
						<div class="ui-jqgrid-hdiv">
							<div class="ui-jqgrid-hbox" style="padding-right: 0">
								<table id="userBankTable"
									class="ui-jqgrid-htable ui-common-table table table-bordered">
									<thead>
										<tr class="ui-jqgrid-labels">
											<th class="ui-th-column ui-th-ltr">商户</th>
											<th class="ui-th-column ui-th-ltr">用户名</th>
											<th class="ui-th-column ui-th-ltr">开户银行</th>
											<th class="ui-th-column ui-th-ltr">开户人</th>
											<th class="ui-th-column ui-th-ltr">支行名称</th>
											<th class="ui-th-column ui-th-ltr">银行卡</th>
											<th class="ui-th-column ui-th-ltr">开户省份</th>
											<th class="ui-th-column ui-th-ltr">开户城市</th>
											<th class="ui-th-column ui-th-ltr">开户地区</th>
											<th class="ui-th-column ui-th-ltr">创建时间</th>
											<th class="ui-th-column ui-th-ltr">锁定状态</th>
											<th class="ui-th-column ui-th-ltr">管理</th>
										</tr>
									</thead>

								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- js -->
<script type="text/javascript" src="<%=path%>/admin/user/js/user_bank_list.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerUserAction.js'></script>
</body>
</html>

