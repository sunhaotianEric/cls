<%@ page language="java" contentType="text/html; charset=UTF-8"
	import="com.team.lottery.util.ConstantUtil,com.team.lottery.vo.Admin,java.math.BigDecimal"
	import="com.team.lottery.service.LotteryCoreService"
	import="com.team.lottery.enums.ELotteryTopKind"
	import="com.team.lottery.util.ApplicationContextUtil,com.team.lottery.system.SystemConfigConstant"
	import="com.team.lottery.util.StringUtils" pageEncoding="UTF-8"%>	
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<%
	Admin admin = (Admin)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_ADMIN);
    //计算时时彩的可调节的返奖率区间
  	BigDecimal sscInterval = new BigDecimal("2");
  	BigDecimal syxwInterval = new BigDecimal("2");
  	BigDecimal dpcInterval = new BigDecimal("2");
  	BigDecimal pk10Interval = new BigDecimal("2");
  	BigDecimal ksInterval = new BigDecimal("2");
  	
  	//商户最高模式值
  	BigDecimal sscHighestAwardModel = SystemConfigConstant.SSCHighestAwardModel;
  	BigDecimal syxwHighestAwardModel = SystemConfigConstant.SYXWHighestAwardModel;
  	BigDecimal dpcHighestAwardModel = SystemConfigConstant.DPCHighestAwardModel;
  	BigDecimal pk10HighestAwardModel = SystemConfigConstant.PK10HighestAwardModel;
  	BigDecimal ksHighestAwardModel = SystemConfigConstant.KSHighestAwardModel;
%>
<!DOCTYPE html>
<html>
    
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>在线会员列表</title>
        <jsp:include page="/admin/include/include.jsp"></jsp:include>
        <link href="<%=path%>/css/plugins/iCheck/custom.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" rel="stylesheet">
        <style type="text/css">
        	.mb0{margin-bottom:0;}
        	.mb30{margin-bottom:30px;}
        	.mt50{margin-top:50px;}
        	.clear:after{content:'';display:table;clear:both;}
			.input-group-btn{float:left;width:auto;}
			#sscrebateValue, #ksrebateValue, #syxwrebateValue, #pk10rebateValue, #dpcrebateValue{float:left;width:70%;}
			.col-sm-2 span{display: inline-block;line-height: 34px;}
        </style>
        <style type="text/css">
			.row p{
				position: absolute;
		    	top: 32px;
			}
			.alert-msg-bg{display:none;position:fixed;top:0px;bottom:0px;left:0px;right:0px;z-index:8;background:#000; opacity:0.4;}
			.alert-msg{display:none;position:fixed;top:50%;right:0px;left:0px;margin:-200px auto 0 auto;z-index:9;width:500px;min-height:initial;background:#fff;box-shadow: 0px 4px 6px 0px rgba(0,0,0,0.4);-webkit-box-shadow: 0px 4px 6px 0px rgba(0,0,0,0.4);}
			.alert-msg .msg-head{position:relative;height:60px;line-height:60px;background:#3088ff;}
			.alert-msg .msg-head p{color:#fff;font-size:18px;padding-left:28px;}
			.alert-msg .msg-head .close{position:absolute;top:0px;bottom:0px;right:20px;margin:auto 0;cursor:pointer;}
			.alert-msg .msg-content{padding:26px 28px; box-sizing:border-box;}
			.alert-msg .msg-content .msg-title{line-height:26px;color:#676767;font-size:14px;padding-bottom:16px;}
			.alert-msg .msg-content .info{line-height:24px;padding:14px 0;color:#676767;font-size:13px;}
			.alert-msg .msg-content .info .blue{color:#006CFF;}
			.alert-msg .msg-content .btn{text-align:center;}
			.alert-msg .msg-content .btn .inputBtn{transition-duration: .3s;-webkit-transition-duration:.3s;cursor:pointer;font-size:18px;color:#fff;font-family:"Microsoft YaHei" ,Helvetica Neue,Tahoma,Arial,"寰蒋闆呴粦","瀹嬩綋","榛戜綋";background:#388cff;border:0px;width:170px;height:49px;border-radius:3px;-webkit-border-radius:3px;}
			.alert-msg .msg-content .btn .inputBtn:hover{background:#1577FD;}
			.alert-msg .msg-content .btn .inputBtn.gray{background:#B1B1B1;}
			.alert-msg .msg-content .btn .inputBtn.gray:hover{background:#B1B1B1;}
			
			.clear:after{content:'';display:table;clear:both;}
			.input-group-btn{float:left;width:auto;}
			#sscrebateValue, #ksrebateValue, #syxwrebateValue, #pk10rebateValue, #dpcrebateValue{float:left;width:80%;}
			.col-sm-2 span{display: inline-block;line-height: 34px;}
			.btn-sm{margin-top:20px;}
		</style>
<script type="text/javascript">
	//时时彩
	var sscOpenUserLowest = null;
	var syxwOpenUserLowest = null;
	var dpcOpenUserLowest = null;
	var pk10OpenUserLowest = null;
	var ksOpenUserLowest = null;
	var sscOpenUserHighest =null;
	var syxwOpenUserHighest =null; 
	var dpcOpenUserHighest =null;  
	var pk10OpenUserHighest =null;  
	var ksOpenUserHighest =null;  
	var sscHighestAwardModel = <%=sscHighestAwardModel%>;
	var syxwHighestAwardModel = <%=syxwHighestAwardModel %>;
	var dpcHighestAwardModel = <%=dpcHighestAwardModel %>;
	var pk10HighestAwardModel = <%=pk10HighestAwardModel%>;
	var ksHighestAwardModel = <%=ksHighestAwardModel%>;
	
	var sscInterval = <%=sscInterval %>;
	var syxwInterval = <%=syxwInterval %>;
	var ksInterval = <%=ksInterval %>;
	var pk10Interval = <%=pk10Interval %>;
	var dpcInterval = <%=dpcInterval %>;
</script>
</head>
    <body class="gray-bg">
        <div class="wrapper wrapper-content animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
                    <div class="ibox ">
                        <div class="ibox-content">
                            <div class="panel-body">
                                <form class="form-horizontal" id="queryForm">
                                    <div class="row">
                                    	<c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                                    	<div class="col-sm-4">
	                                            <div class="input-group m-b">
	                                                <span class="input-group-addon">商户</span>
	                                                <cls:bizSel name="bizSystem" id="bizSystem" options="class:'ipt form-control'"/> 
	                                                </div>
	                                    </div>
	                                    </c:if>
                                        <div class="col-sm-4">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">用户名</span>
                                                <input type="text" value="" id='userName' class="form-control"></div>
                                        </div>
                                    	<div class="col-sm-4">
                                            <div class="form-group nomargin text-right">
                                                <button type="button" class="btn btn-outline btn-default btn-kj" onclick="userRebateAdjust.queryUser()">查询</button></div>
                                        </div>
                                    </div>
                                </form>
                                <div class="row mb30">
                                    <div class="col-sm-4">
                                            <div class="form-group nomargin text-left" id="sl">共查询到所有下级会员0个</div>
                                    </div>
                                </div>
                                <form class="form-horizontal">
                                	<div class="form-group">
                                	   <div class="col-sm-1">
                                	   		<label class="checkbox-inline i-checks">
	                                    		<input type="checkbox" name="box" value="ssc" style="position: absolute; opacity: 0;">
	                                    		<ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
		                                	</label>
                                	   </div>
	                                   <label class="col-sm-2 control-label">时时彩模式：</label>
	                                   <div class="col-sm-7 clear">
	                                   	   <span class="input-group-btn">
					                           <button class="btn btn-primary" id="sscModeAdd" type="button" disabled="disabled">
					                              <i class="fa fa-plus"></i>
					                           </button>
					                       </span>
					                       <input type="text" class="form-control" id="sscrebateValue" disabled="disabled">
						                   <span class="input-group-btn">
						                       <button class="btn btn-primary" id="sscModeReduction" type="button" disabled="disabled">
						                              <i class="fa fa-minus"></i>
						                       </button>
						                   </span>
	                                   </div>
	                                   <div class="col-sm-2 text-center">
	                                       <span id="sscTip">返点0.00%</span>
	                                   </div>
	                               </div>
	                               <div class="form-group">
	                               	   <div class="col-sm-1">
	                               	   		<label class="checkbox-inline i-checks">
	                                    		<input type="checkbox" name="box" value="ks" style="position: absolute; opacity: 0;">
	                                    		<ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
		                                	</label>
	                               	   </div>
	                                   <label class="col-sm-2 control-label">快三模式：</label>
	                                   <div class="col-sm-7 clear">
	                                   	  <span class="input-group-btn">
				                             <button class="btn btn-primary" id="ksModeAdd" type="button" disabled="disabled">
				                                  <i class="fa fa-plus"></i>
				                             </button>
				                          </span>
				                          <input type="text" class="form-control" id="ksrebateValue" disabled="disabled">
				                          <span class="input-group-btn">
				                             <button class="btn btn-primary" id="ksModeReduction" type="button" disabled="disabled">
				                                  <i class="fa fa-minus"></i>
				                             </button>
				                          </span>
	                                   </div>
	                                   <div class="col-sm-2 text-center">
	                                       <span id="ksTip">返点0.00%</span>
	                                   </div>
	                               </div>
	                               <div class="form-group">
	                               	   <div class="col-sm-1">
	                               	   		<label class="checkbox-inline i-checks">
	                                    		<input type="checkbox" name="box" value="syxw" style="position: absolute; opacity: 0;">
	                                    		<ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
		                                	</label>
		                               </div>
	                                   <label class="col-sm-2 control-label">十一选五模式：</label>
	                                   <div class="col-sm-7 clear">
	                                   	  <span class="input-group-btn">
				                             <button class="btn btn-primary" id="syxwModeAdd" type="button" disabled="disabled">
				                                  <i class="fa fa-plus"></i>
				                             </button>
				                          </span>
				                          <input type="text" class="form-control" id="syxwrebateValue" disabled="disabled">
				                          <span class="input-group-btn">
				                             <button class="btn btn-primary" id="syxwModeReduction" type="button" disabled="disabled">
				                                  <i class="fa fa-minus"></i>
				                             </button>
				                          </span>
	                                   </div>
	                                   <div class="col-sm-2 text-center">
	                                       <span id="syxwTip">返点0.00%</span>
	                                   </div>
	                               </div>
	                               <div class="form-group">
	                               	   <div class="col-sm-1">
	                               	   		<label class="checkbox-inline i-checks">
	                                    		<input type="checkbox" name="box" value="pk10" style="position: absolute; opacity: 0;">
	                                    		<ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
		                                	</label>
		                               </div>
	                                   <label class="col-sm-2 control-label">PK10模式：</label>
	                                   <div class="col-sm-7  clear">
	                                   	  <span class="input-group-btn">
				                             <button class="btn btn-primary" id="pk10ModeAdd" type="button" disabled="disabled">
				                                  <i class="fa fa-plus"></i>
				                             </button>
				                          </span>
				                          <input type="text" class="form-control" id="pk10rebateValue" disabled="disabled">
				                          <span class="input-group-btn">
				                             <button class="btn btn-primary" id="pk10ModeReduction" type="button" disabled="disabled">
				                                  <i class="fa fa-minus"></i>
				                             </button>
				                          </span>
	                                   </div>
	                                   <div class="col-sm-2 text-center">
	                                       <span id="pk10Tip">返点0.00%</span>
	                                   </div>
	                               </div>
	                               <div class="form-group">
	                               	   <div class="col-sm-1">
	                               	   		<label class="checkbox-inline i-checks">
	                                    		<input type="checkbox" name="box" value="dpc" style="position: absolute; opacity: 0;">
	                                    		<ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
		                                	</label>
		                               </div>
	                                   <label class="col-sm-2 control-label">低频彩模式：</label>
	                                   <div class="col-sm-7  clear">
	                                   	  <span class="input-group-btn">
				                             <button class="btn btn-primary" id="dpcModeAdd" type="button" disabled="disabled">
				                                  <i class="fa fa-plus"></i>
				                             </button>
				                          </span>
				                          <input type="text" class="form-control" id="dpcrebateValue" disabled="disabled">
				                          <span class="input-group-btn">
				                             <button class="btn btn-primary" id="dpcModeReduction" type="button" disabled="disabled">
				                                  <i class="fa fa-minus"></i>
				                             </button>
				                          </span>
	                                   </div>
	                                   <div class="col-sm-2 text-center">
	                                       <span id="dpcTip">返点0.00%</span>
	                                   </div>
	                               </div>
	                               <div class="form-group mb0 mt50">
			                            <div class="col-sm-12 ibox">
			                                <div class="row">
			                                    <div class="col-sm-12 text-left">
			                                        	温馨提醒：此功能调整后所有下级的返点都会变为一样，用户所有下级的上下级的返点将会没有，请谨慎使用，不可恢复
			                                    </div>
			                                </div>
			                            </div>
			                        </div>
	                               <div class="form-group">
			                            <div class="col-sm-12 ibox">
			                                <div class="row">
			                                    <div class="col-sm-12 text-center">
			                                        <button type="button" disabled="disabled" class="btn btn-primary btn-sm" onclick="userRebateAdjust.updateUserAllLowerLevelsByName()" id="addExtendLinkButton">
										            &nbsp;&nbsp;提交&nbsp;&nbsp;
										            </button>
			                                    </div>
			                                </div>
			                            </div>
			                        </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
 <!-- 业务js -->
 <script src="<%=path%>/js/plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript" src="<%=path%>/admin/user/js/user_rebate_adjust.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerUserAction.js'></script>   
<script type='text/javascript' src='<%=path%>/dwr/interface/managerUserRegisterAction.js'></script>
 <script>
   $(document).ready(function(){$(".i-checks").iCheck({checkboxClass:"icheckbox_square-green",radioClass:"iradio_square-green",})});
 </script>
</body>
</html>
	