<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import = "com.team.lottery.enums.ELotteryKind" %>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<html>
<head>
    <meta charset="utf-8" />
    <title>续费处理</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
      <link rel="shortcut icon" href="favicon.ico">
        <link href="<%=path%>/css/style.min862f.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>?v=4.1.0" rel="stylesheet">
      <link href="<%=path%>/css/plugins/iCheck/custom.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" rel="stylesheet">
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeIn">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-content">
						<form class="form-horizontal border-gray">
							<div class="row">
								<c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
									<div class="col-sm-12">
										<div class="form-group">
											<label class="col-sm-1 control-label">商户</label>
											<div class="col-sm-6">
												<cls:bizSel name="bizSystem" id="bizSystem"	options="class:ipt form-control" />
											</div>
										</div>
									</div>
								</c:if>
								<div class="col-sm-12">
									<div class="form-group">
										<label class="col-sm-1 control-label">会员账号</label>
										<div class="col-sm-6">
											<input type="text" value="" class="form-control" id="userName">
										</div>
										<label class="col-sm-5 control-label textleft">*
											批量操作可输入会员账号，以英文逗号 (,) 分隔开</label>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<label class="col-sm-1 control-label">金额</label>
										<div class="col-sm-3">
											<input type="text" value="" class="form-control" id="moneyCount">
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<label class="col-sm-1 control-label">类型</label>
										<div class="col-sm-8 radio i-checks">
											<label><input type="radio" value="SYSTEM_ADD_MONEY" name="a" class="b" checked="checked"> <i></i>人工存入</label>&nbsp;&nbsp; 
											<label><input type="radio" value="RECHARGE_MANUAL" name="a" class="b"> <i></i>人工存款	</label>&nbsp;&nbsp; 
											<label><input type="radio" value="MANAGE_REDUCE_MONEY" name="a" class="b"> <i></i>行政提出&nbsp;&nbsp;&nbsp;</label>&nbsp;&nbsp;&nbsp;&nbsp; 
											<label><input type="radio" value="SYSTEM_REDUCE_MONEY" name="a" class="b"> <i></i>误存提出</label>&nbsp;&nbsp;<br>
											<div style="margin-top:12px;"> 
												<label><input type="radio" value="RECHARGE_PRESENT" name="a" class="b"> <i></i>充值赠送</label>&nbsp;&nbsp; 
												<label><input type="radio" value="ACTIVITIES_MONEY" name="a" class="b"> <i></i>活动彩金</label>&nbsp;&nbsp; 
												<label><input type="radio" value="SYSTEM_REDUCE_MONEY_FOR_RECHARGE_PRESENT" name="a" class="b"><i></i>充值赠送扣费</label>&nbsp;&nbsp; 
												<label><input type="radio" value="SYSTEM_REDUCE_MONEY_FOR_ACTIVITIES" name="a" class="b"> <i></i>活动彩金扣费</label>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<label class="col-sm-1 control-label">备注</label>
										<div class="col-sm-3">
											<input type="text" value="" class="form-control" id="description">
										</div>
										<label class="col-sm-8 control-label textleft">* 备注信息</label>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<label class="col-sm-1 control-label">操作密码</label>
										<div class="col-sm-6">
											<input type="password" value="" class="form-control" id="operatePassword">
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<label class="col-sm-1 control-label"></label>
										<div class="col-sm-3">
											<button type="button" class="btn btn-success" onclick="userRechargePage.renewTreatment()">提交保存</button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<div class="row notice">
							<h2>* 注意事项：</h2>
	               	  		<p>为确保报表盈利数据准确，请认真选择存提类型；</p>
	               	  		<p>人工存款：客户入款需要手动添加时使用，计入报表充值额，但不生成充值订单；</p>
	               	  		<p>人工存入：可直接提现，用于补派奖缺少，测试投注使用，计入盈亏报表活动赠送正值；</p>
	               	  		<p>充值赠送：客户充值赠送金额使用，计入盈亏报表活动赠送正值；</p>
	               	  		<p>活动彩金：给客户赠送活动彩金使用，计入盈亏报表活动赠送正值；</p>
	               	  		<p>误存提出：误存时使用，扣减用户余额使用，不计入盈亏报表；</p>
	               	  		<p>行政提出：派彩错误、强行提出时使用，计入报表活动赠送负值；</p>
	               	  		<p>充值赠送扣费：扣减客户充值赠送金额使用，需已有充值赠送过，计入盈亏报表活动赠送负值；</p>
	               	  		<p>活动彩金扣费：扣减客户活动彩金金额使用，需已有获得活动彩金过，计入盈亏报表活动赠送负值；</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- js -->
 <script type="text/javascript" src="<%=path%>/admin/user/js/user_recharge.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
 <!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerUserAction.js'></script> 
</body>
</html>

