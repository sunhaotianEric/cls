<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<html>
<head>
<meta charset="utf-8" />
<title>商户资金明细</title>
<jsp:include page="/admin/include/include.jsp"></jsp:include>
<!-- js -->
<script type="text/javascript" src="<%=path%>/admin/user/js/user_point_log.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerPointDetailAction.js'></script>
<script type='text/javascript' src='<%=path%>/dwr/interface/managerUserAction.js'></script>

<style type="text/css">
caption, th {
    text-align: center;
}

.tb td{
    text-align: center;
}

.ui-dialog{
    width : auto;
}
</style>
<script type="text/javascript">
    $(function () {

    });
</script>
</head>
<body>
   <div class="alert alert-info">当前位置<b class="tip"></b><a href="/managerzaizst/user/user_list.html">会员信息管理</a><b class="tip"></b>积分明细</div>

    <table class="tbform">
        <tbody>
            <tr>
                <td class="tdl">类型</td>
                <td class="detail">
                   <select id='pointDetailTypes' class="ipt">
                    </select>
                </td>
                <td class="tdl">用户名 </td>
                <td>
                    <input type="text" id='userName' class="ipt"/>
                </td>
            </tr>
            <tr>
                <td class="tdl">开始时间 </td>
                <td>
                    <input class="Wdate" type="text" onClick="WdatePicker()" readonly="readonly" id="createdDateStart">
                </td>
                <td class="tdl">结束时间</td>
                <td>
                    <input class="Wdate" type="text" onClick="WdatePicker()" readonly="readonly" id="createdDateEnd">
                </td>  
                <td colspan="12" align="right">
                    <input class="btn" id="find" type="button" onclick="userPointLog.pageParam.pageNo = 1;userPointLog.getAllPointLogsForQuery()" value="查询" />
                </td>
            </tr>
        </tbody>
    </table>
    
    <table class="tb">
        <tr>
          <th>用户名</th>
          <th>操作积分</th>
          <th>剩余积分</th>
          <th>处理日期 </th>
          <th>操作说明</th>
        </tr>
      <tbody  id="pointLogList">
        <!--  
        <tr>
          <td>HS30</td>
          <td>现金</td>
          <td>票据 </td>
          <td>负责人 </td>
          <td>3009.40</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td align="center">
             <input class="btn btn-mini btnedit"  type="button" value="编辑"/>
             <input class="btn btn-mini btnedit"  type="button" value="续费"/>
             <input class="btn btn-mini btnedit"  type="button" value="积分"/>
             <input class="btn btn-mini btnedit"  type="button" value="删除"/>                       
          </td>
        </tr>
        -->
      </tbody>
      <tr class="pager" id='pageMsg'>
        <!-- 
	        <th colspan="100">当前第5页/共55页&nbsp;&nbsp;共650条记录&nbsp;&nbsp;<a>首页</a>&nbsp;<a>下一页</a>&nbsp;
	            <a class="current">1</a>&nbsp;<a>2</a>&nbsp;<a>3</a>&nbsp;<a>4</a>&nbsp;
	            <a class="badge badge-warning">5</a>&nbsp;<a class="navpage">...</a>&nbsp;<a>55</a>&nbsp;<a>上一页</a>&nbsp;<a>尾页</a>
	        </th>
         -->
      </tr>
    </table>
</body>
</html>

