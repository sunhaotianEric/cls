<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<html>
<head>
<meta charset="utf-8" />
<title>用户日佣金明细</title>
<jsp:include page="/admin/include/include.jsp"></jsp:include>
<!-- js -->
<script type="text/javascript" src="<%=path%>/admin/user/js/user_day_commission_report.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerOrderAction.js'></script>

<style type="text/css">
caption, th {
    text-align: center;
}

.tb td{
    text-align: center;
}

.ui-dialog{
    width : auto;
}
</style>
<script type="text/javascript">
    $(function () {

    });
</script>
</head>
<body>
   <div class="alert alert-info">当前位置<b class="tip"></b><a href="/managerzaizst/user/user_list.html">会员信息管理</a><b class="tip"></b>用户日佣金明细</div>

    <table class="tbform list">
        <tbody>
            <tr>
                <td class="tdl">用户名 </td>
                <td>
                    <input type="text" id='userName' class="ipt"/>
                </td>
                <td class="tdl">开始时间 </td>
                <td>
                    <input class="Wdate date-input" type="text" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="readonly" id="createdDateStart">
                </td>
                <td class="tdl">结束时间</td>
                <td>
                    <input class="Wdate date-input" type="text" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="readonly" id="createdDateEnd">
                </td>  
                <td colspan="12" align="right">
                    <input class="btn" id="find" type="button" onclick="userDayCommissionReportPage.findUserDayOrderNumByQueryParam()" value="查询" />
                </td>
            </tr>
        </tbody>
    </table>
    <table class="tbform list">
    	<thead>
            <tr class="tr">
                <th colspan="3" style="text-align: left;">来自下级佣金明细</th>
            </tr>
        </thead>
        <tr>
          <th style="width:15%">用户名</th>
          <th style="width:30%">投注金额</th>
          <th style="width:25%">奖励金额</th>
        </tr>
      <tbody  id="reportList">
      </tbody>
    </table>
    
    <table class="tbform list">
    	<thead>
            <tr class="tr">
                <th colspan="3" style="text-align: left;">来自下下级佣金明细</th>
            </tr>
        </thead>
        <tr>
         <th style="width:15%">用户名</th>
          <th style="width:30%">投注金额</th>
          <th style="width:25%">奖励金额</th>
        </tr>
      <tbody  id="reportList2">
      </tbody>
    </table>
</body>
</html>

