<%@page import="com.team.lottery.enums.ERecordLogModel"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<html>
<head>
    <meta charset="utf-8" />
    <title>会员信息导出</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/user/js/user_info_export.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerUserAction.js'></script>
<%--     <script type='text/javascript' src='<%=path%>/dwr/interface/managerBizSystemAction.js'></script> --%>
<style type="text/css">
caption, th {
    text-align: center;
}

.tb td{
    text-align: center;
}

.ui-dialog{
    width : auto;
}
</style>
</head>


<body>
   <div class="alert alert-info">当前位置<b class="tip"></b><a href="<%=path %>/managerzaizst/user/user_list.html">会员信息管理</a><b class="tip"></b>会员信息导出</div>
    
   	<table class="tbform">
          <tr>
          	<td class="tdl">导出用户名：</td>
             <td>
             	<input id="userName" type="text"  style="width: 80px"/>
             	 <c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
             	 <input id="bizSystem" name="bizSystem" type="hidden"  value="${admin.bizSystem}"/>
             	 </c:if>
             </td>
             <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
             <td class="tdl">商户：</td>
             <td>
             	   <cls:bizSel name="bizSystem" id="bizSystem"/>
             </td>
             </c:if>
             <td class="tdl">导出字段：</td>
             <td>
             	<input type="checkbox" name="hasTruename" id="hasTruename">真实姓名
             	<input type="checkbox" name="hasAddress" id="hasAddress">地址
             	<input type="checkbox" name="hasIdentityid" id="hasIdentityid">身份证号码
             	<input type="checkbox" name="hasPhone" id="hasPhone">手机
             	<input type="checkbox" name="hasQq" id="hasQq">QQ
             	<input type="checkbox" name="hasEmail" id="hasEmail">电子邮箱
             	<input type="checkbox" name="hasAddtime" id="hasAddtime">注册时间
             </td>
             <td colspan="2" align="left">
                    <input class="btn" id="bonusButton" type="button" onclick="userInfoExportPage.userInfoExport();" value="导出" />
             </td>
          </tr>
     </table>
    
</body>
</html>

