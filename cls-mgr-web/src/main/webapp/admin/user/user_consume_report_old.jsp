<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
    
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>会员盈亏报表</title>
        <jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
    <body>
        <div class="animated fadeIn">
            <div class="ibox-content ibox">
                <div class="row m-b-sm m-t-sm">
                    <form class="form-horizontal" id="queryFrom">
                    <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                        <div class="row">
                           <div class="col-sm-2">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">商户</span>
                                    <cls:bizSel name="bizSystem" id="bizSystem" options="class:'ipt form-control'" emptyOption="false"/>                 
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">用户名</span>
                                    <input id='userName' type="text" value="" class="form-control"></div>
                            </div>
                            <div class="col-sm-5">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">时间始终</span>
                                    <div class="input-daterange input-group" id="datepicker">
                                        <input class="form-control layer-date" placeholder="开始日期" id="teamUserConsumeHistoryReportDateStart"  onclick="laydate({istime: true, format: 'YYYY-MM-DD'})">
                                        <span class="input-group-addon">到</span>
                                        <input class="form-control layer-date" placeholder="结束日期" id="teamUserConsumeHistoryReportDateEnd" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})"></div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">报表类型</span>
                                    <select class="ipt form-control" name="reportType" id="reportType" onchange="chageDateFt()">
                                        <option value="0">历史盈亏</option>
                                        <option value="1">实时盈亏</option>
                                     </select>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <div class="form-group nomargin text-right">
                                    <button type="button" class="btn btn-outline btn-default" onclick="userConsumeReport.getAllConsumeReportsForQuery()"><i class="fa fa-search"></i>&nbsp;查 询</button></div>
                            </div>
                        </div>
                        </c:if>
                       <c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">用户名</span>
                                    <input id='userName' type="text" value="" class="form-control"></div>
                            </div>
                            <div class="col-sm-5">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">时间始终</span>
                                    <div class="input-daterange input-group" id="datepicker">
                                        <input class="form-control layer-date" placeholder="开始日期" id="teamUserConsumeHistoryReportDateStart"  onclick="laydate({istime: true, format: 'YYYY-MM-DD'})">
                                        <span class="input-group-addon">到</span>
                                        <input class="form-control layer-date" placeholder="结束日期" id="teamUserConsumeHistoryReportDateEnd" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})"></div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">报表类型</span>
                                    <select class="ipt form-control" name="reportType" id="reportType" onchange="chageDateFt()" >
                                        <option value="0">历史盈亏</option>
                                        <option value="1">实时盈亏</option></select>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <div class="form-group nomargin text-right">
                                    <button type="button" class="btn btn-outline btn-default" onclick="userConsumeReport.getAllConsumeReportsForQuery()"><i class="fa fa-search"></i>&nbsp;查 询</button></div>
                            </div>
                        </div>
                        </c:if>
                    </form>
                    <div class="jqGrid_wrapper">
                        <div class="ui-jqgrid ">
                            <div class="ui-jqgrid-view">
                                <div class="ui-jqgrid-hdiv">
                                    <div class="ui-jqgrid-hbox" style="padding-right:0">
                                        <table class="ui-jqgrid-htable ui-common-table table table-bordered" id="userConsumeReportTable">
                                            <thead>
                                                <tr class="ui-jqgrid-labels">
                                                 <th class="ui-th-column ui-th-ltr" id="bizSystemTr" style="display: none;">商户</th>
                                                    <th class="ui-th-column ui-th-ltr">用户名</th>
                                                   
                                                    <th class="ui-th-column ui-th-ltr">存款</th>
				                                               
                                                    <th class="ui-th-column ui-th-ltr">取款</th>
                                             
                                                    <th class="ui-th-column ui-th-ltr">投注(-撤单)</th>
                                                     <th class="ui-th-column ui-th-ltr">中奖</th>
                                                    
                                                    <th class="ui-th-column ui-th-ltr">返点</th>
                                                    <th class="ui-th-column ui-th-ltr">活动赠送</th>
                                                 
                                                    <th class="ui-th-column ui-th-ltr">其他</th>
                                                    <th class="ui-th-column ui-th-ltr">盈利</th>
                                                    <th class="ui-th-column ui-th-ltr">注册/首充/投注(人)</th>
                                                    </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!-- js -->
<script type="text/javascript" src="<%=path%>/admin/user/js/user_consume_report.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerTeamAction.js'></script>
<script type='text/javascript' src='<%=path%>/dwr/interface/managerUserAction.js'></script>
<script type="text/javascript">
function chageDateFt(){
	var reportType=$("#reportType").val();
	var nowDate = new Date();
	//在00:00:00 - 03:00:00之间的时间取前一天
	if(nowDate.getHours() < 3) {
		nowDate = nowDate.AddDays(-1);
	}
	if(reportType=='1'){
		$("#teamUserConsumeHistoryReportDateStart").attr("onclick","laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})");
		$("#teamUserConsumeHistoryReportDateEnd").attr("onclick","laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})");

		nowDate.setHours(3, 0, 0, 0);
		$("#teamUserConsumeHistoryReportDateStart").val(nowDate.format("yyyy-MM-dd hh:mm:ss"));
		nowDate.AddDays(1).setHours(2, 59, 59, 999);
		$("#teamUserConsumeHistoryReportDateEnd").val(nowDate.format("yyyy-MM-dd hh:mm:ss"));
	}else{
		$("#teamUserConsumeHistoryReportDateStart").attr("onclick","laydate({istime: true, format: 'YYYY-MM-DD'})");
		$("#teamUserConsumeHistoryReportDateEnd").attr("onclick","laydate({istime: true, format: 'YYYY-MM-DD'})");
		
		//默认查询前一天的
		nowDate = nowDate.AddDays(-1);
		//nowDate.setHours(3, 0, 0, 0);
		$("#teamUserConsumeHistoryReportDateStart").val(nowDate.format("yyyy-MM-dd"));
		//nowDate.AddDays(1).setHours(2, 59, 59, 999);
		nowDate.AddDays(1);
		$("#teamUserConsumeHistoryReportDateEnd").val(nowDate.format("yyyy-MM-dd"));
	}
}



</script>
</body>
</html>

