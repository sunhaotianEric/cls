<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
	String userId = request.getParameter("id");  //获取查询的用户ID
	String isTourist = request.getParameter("isTourist");
%>
<!DOCTYPE html>
<html>  
    <head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>编辑</title>
     <jsp:include page="/admin/include/include.jsp"></jsp:include>
		<style type="text/css">
		caption, th {
		    text-align: center;
		} 
		</style>
		        <style type="text/css">
		        .jsstar
				{   list-style: none;
				    margin: 0px;
				    padding: 0px; width: 100px;height: 20px;
				    position: relative;
				    
				}
				 .jsstar li  
				{
				   padding:0px;
				   margin: 0px; 
				   float: left; 
				   width:20px;
				   height:20px;
				   background:url(../../../admin/resource/img/star_rating.gif) 0 0 no-repeat;
				} 
				.jsstar_bright {
					background-position : left bottom;
				}
        </style>
</head>
   <body class="gray-bg">
        <div class="wrapper wrapper-content animated fadeIn">
            <div class="row">
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">用户名</span>
                        <input type="text" id='userName' name="userName" disabled="disabled" class="form-control"></div>
                </div>
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">账号锁定</span>
                         <select class="ipt" id='loginLock' name="loginLock" disabled="disabled">
                        <option value='0' selected="selected">未锁定</option>
                        <option value='1'>锁定</option>
                   </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">手机号码</span>
                        <input type="text" id='phone' class="form-control"></div>
                </div>
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">身份证号</span>
                        <input type="text" id='identityid' class="form-control"></div>
                </div>
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">生 日</span>
                        <input type="text" id='birthday' class="form-control"></div>
                </div>
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">QQ号码</span>
                        <input type="text" id='qq' class="form-control"></div>
                </div>
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">真实姓名</span>
                        <input type="text" id='trueName' class="form-control"></div>
                </div>
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">性别</span>
                          <select class="ipt" id='sex'>
		                        <option value='1' selected="selected">男</option>
		                        <option value='0' >女</option>
		                   </select>
                        
                        </div>
                </div>
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">电子邮箱</span>
                        <input type="text" id='email' class="form-control"></div>
                </div>
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">注册时间</span>
                        <input type="text" id='addTime' class="form-control"></div>
                </div>
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">提现锁定</span>
                       <select class="ipt" id='withdrawLock' >
                        <option value='0' selected="selected">未锁定</option>
                        <option value='1'>锁定</option>
                      </select>   
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">代理级别</span>
                       <select class="ipt" id='dailiLevel' disabled="disabled">
                        <option value='DIRECTAGENCY' selected="selected">直属代理</option>
                        <option value='SUPERAGENCY' >超级代理</option>
                        <option value='GENERALAGENCY'>总代理</option>
                        <option value='ORDINARYAGENCY'>普通代理</option>
                        <option value='REGULARMEMBERS'>普通会员</option>
                       </select> 
                    </div>
                </div>
               
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">用户星级</span>
                       	<ul class="jsstar">
					    <li title="一星" data-val="1"></li>
					    <li title="二星" data-val="2"></li>
					    <li title="三星" data-val="3"></li>
					    <li title="四星" data-val="4"></li>
					    <li title="五星" data-val="5"></li>
					   </ul>
                    </div>
                </div>
                 <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">时时彩模式</span>
                        <select class="ipt" id="sscRebate" disabled="disabled">
                            </select>
                    </div>
                </div>
<!--                  <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">分分彩模式</span>
                          <input type="text" id='ffcRebate' class="form-control" disabled="disabled" onkeyup="checkNum(this)">
                    </div>
                </div> -->
                 <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">11选5模式</span>
         
                         <input type="text" id='syxwRebate' class="form-control" disabled="disabled" onkeyup="checkNum(this)">
                    </div>
                </div>
                 <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">快三模式</span>
                          <input type="text" id='ksRebate' class="form-control" disabled="disabled" onkeyup="checkNum(this)">
                    </div>
                </div>
                 <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">PK10模式</span>
                         <input type="text" id='pk10Rebate' class="form-control" disabled="disabled" onkeyup="checkNum(this)">
                    </div>
                </div>
                 <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">低频彩模式</span>
                          <input type="text" id='dpcRebate' class="form-control" disabled="disabled" onkeyup="checkNum(this)">
                    </div>
                </div>
               <div class="col-sm-6 vipLevel" style="display: none">
                    <div class="input-group m-b">
                        <span class="input-group-addon">VIP等级</span>
                       <select class="ipt" id='vipLevel'>
                        <option value='1' selected="selected">VIP1</option>
                        <option value='2' >VIP2</option>
                        <option value='3'>VIP3</option>
                        <option value='4'>VIP4</option>
                        <option value='5'>VIP5</option>
                        <option value='6'>VIP6</option>
                        <option value='7'>VIP7</option>
                        <option value='8'>VIP8</option>
                        <option value='9'>VIP9</option>
                       </select> 
                    </div>
                </div>
                <div class="col-sm-6 point" style="display: none">
                    <div class="input-group m-b">
                        <span class="input-group-addon">积分</span>
                          <input type="text" id='point' class="form-control"onkeyup="checkNum(this)">
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <button type="button" class="btn btn-w-m btn-white" onclick="userEdit.editUser()">提 交</button></div>
                    </div>
                </div>
            </div>
        </div>
<!-- js -->
<script type="text/javascript" src="<%=path%>/admin/user/js/user_edit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerUserAction.js'></script>
<script type='text/javascript' src='<%=path%>/dwr/interface/managerBizSystemAction.js'></script>
<script type="text/javascript">
userEdit.editParam.userId = <%=userId %>;
userEdit.editParam.isTourist = <%=isTourist %>;
</script>    
</body>
</html>