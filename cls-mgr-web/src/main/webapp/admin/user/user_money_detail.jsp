<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
	String isTourist = request.getParameter("isTourist");
%>
<!DOCTYPE html>
<html>  
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>商户资金明细</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    <script type="text/javascript">
		var isTouristParameter = '<%=isTourist %>';
    </script>
</head>
    <body>
        <div class="animated fadeIn">
         <div class="row">
                <div class="col-sm-12">
                    <div class="ibox ">
                        <div class="ibox-content">
                            <!-- <div class="panel-body"> -->
                                <div class="row">
                                    <form class="form-horizontal" id="queryForm">
                                        <div class="row">
                                        <c:if test="${admin.bizSystem == 'SUPER_SYSTEM'}">
                                         	<div class="col-sm-3">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">商户</span>
                                                    <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
                                                    </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">用户名</span>
                                                    <input type="text" id='userName' value="" class="form-control"></div>
                                            </div>
                                             <div class="col-sm-3">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">流水号</span>
                                                    <input type="text" id='lotteryId' value="" class="form-control"></div>
                                            </div>
                                              <div class="col-sm-3">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">类型</span>
                                                     <select id='moneyDetailTypes' class="ipt form-control">
                                                     </select>
                                                </div>
                                            </div>
                                     	</c:if>
                                        <c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">  
                                            <div class="col-sm-4">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">用户名</span>
                                                    <input type="text" id='userName' value="" class="form-control"></div>
                                            </div>
                                             <div class="col-sm-4">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">流水号</span>
                                                    <input type="text" id='lotteryId' value="" class="form-control"></div>
                                            </div>
                                              <div class="col-sm-4">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">类型</span>
                                                     <select id='moneyDetailTypes' class="ipt form-control">
                                                     </select>
                                                </div>
                                            </div>
                                          </c:if>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <div class="input-group m-b">
                                                    <span class="input-group-addon">时间始末</span>
                                                    <div class="input-daterange input-group" id="datepicker">
                                                        <input readonly="readonly" name="createdDateStart" id="createdDateStart" style="background-color: white;" class="form-control layer-date" placeholder="开始日期" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})">
                                                        <span class="input-group-addon">到</span>
                                                        <input readonly="readonly" name="createdDateEnd" id="createdDateEnd" style="background-color: white;" class="form-control layer-date" placeholder="结束日期" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4" style="padding-left:2px;">
                                            	<div class="form-group nomargin">
										            <button type="button" class="btn btn-xs btn-info" onclick="userMoneyDetail.getAllMoneyDetailsForQuery(1)">今 天</button>
										            <button type="button" class="btn btn-xs btn-danger" onclick="userMoneyDetail.getAllMoneyDetailsForQuery(-1)">昨 天</button>
										        	<button type="button" class="btn btn-xs btn-warning" onclick="userMoneyDetail.getAllMoneyDetailsForQuery(-7)">最近7天</button><br/>
										        	<button type="button" class="btn btn-xs btn-success" onclick="userMoneyDetail.getAllMoneyDetailsForQuery(30)">本 月</button>
										        	<button type="button" class="btn btn-xs btn-primary" onclick="userMoneyDetail.getAllMoneyDetailsForQuery(-30)">上 月</button>
										        </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group nomargin text-right">
                                                    <button type="button" class="btn btn-outline btn-default btn-kj" onclick="userMoneyDetail.getAllMoneyDetailsForQuery()" ><i class="fa fa-search"></i>&nbsp;查 询</button></div>
                                            </div>
                           
                                            
                                        </div>
                                    </form>
                                    <div class="jqGrid_wrapper">
                                        <div class="ui-jqgrid ">
                                            <div class="ui-jqgrid-view">
                                                <div class="ui-jqgrid-hdiv">
                                                    <div class="ui-jqgrid-hbox" style="padding-right:0">
                                                        <table class="ui-jqgrid-htable ui-common-table table table-bordered" id="moneyDetailTable">
                                                            <thead>
                                                                <tr class="ui-jqgrid-labels">
                                                                 <th class="ui-th-column ui-th-ltr">商户</th>
                                                                    <th class="ui-th-column ui-th-ltr">流水号</th>
                                                                    <th class="ui-th-column ui-th-ltr">用户名</th>
                                                                    <th class="ui-th-column ui-th-ltr">操作类型</th>
                                                                   
                                                                    <th class="ui-th-column ui-th-ltr">操作人</th>
                                                                    
                                                                    <th class="ui-th-column ui-th-ltr">操作金额</th>
                                                                    <th class="ui-th-column ui-th-ltr">剩余余额</th>
                                                                    <th class="ui-th-column ui-th-ltr">创建日期</th>
                                                                    <th class="ui-th-column ui-th-ltr">备注</th></tr>
                                                            </thead>
                                                            <tbody>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>           
                                        </div>
                                    </div>
                                </div>
                            <!-- </div> -->
                        </div>
                    </div>
                </div>
            </div> 
        </div>
<!-- js -->
<script type="text/javascript" src="<%=path%>/admin/user/js/user_money_detail.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerMoneyDetailAction.js'></script>
<script type='text/javascript' src='<%=path%>/dwr/interface/managerOrderAction.js'></script>
<script type='text/javascript' src='<%=path%>/dwr/interface/managerUserAction.js'></script>
</body>
</html>

