<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="java.math.BigDecimal,java.lang.Double,com.team.lottery.vo.User"%>
<%@ page
	import="com.team.lottery.util.ConstantUtil,com.team.lottery.vo.Admin"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant"%>
<%
	String path = request.getContextPath();
	Admin admin = (Admin)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_ADMIN);
	String userId = request.getParameter("id");
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>续费</title>
	<jsp:include page="/admin/include/include.jsp"></jsp:include>
	<style>
		.control-label{text-align:right;}
		.col-sm-3{width:20%;}
		.col-sm-9{width:80%;}
		.col-sm-3, .col-sm-9, .col-sm-6{float:left;}
	</style>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeIn">
		<div class="row">
			<div class="col-sm-12">
				<form class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-3 control-label">会员名称：</label>
						<div class="col-sm-9">
							<label id='username'></label>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">总余额：</label>
						<div class="col-sm-9">
							<label id='money'></label>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">可提现余额：</label>
						<div class="col-sm-9">
							<label id='userMoney'></label>
						</div>
					</div>
					<!--           <div class="form-group">
                            <label class="col-sm-3 control-label">冻结资金：</label>
                            <div class="col-sm-9"><label id='iceMoney'></label></div></div> -->

					<div class="form-group">
						<label class="col-sm-3 control-label">资金操作：</label>
						<div class="col-sm-3">
							<select class="ipt" id="operationType">
								<option value=''>==请选择==</option>
								<%  
				                        if(admin.getState().equals(0) || admin.getState().equals(1)|| admin.getState().equals(2)){ 
				                    %>
								<option value='SYSTEM_ADD_MONEY'>人工存入</option>
								<option value='RECHARGE_MANUAL'>人工存款</option>
								<option value='MANAGE_REDUCE_MONEY'>行政提出</option>
								<option value='SYSTEM_REDUCE_MONEY'>误存提出</option>

								<option value='RECHARGE_PRESENT'>充值赠送</option>
								<option value='ACTIVITIES_MONEY'>活动彩金</option>
								<option value='SYSTEM_REDUCE_MONEY_FOR_RECHARGE_PRESENT'>充值赠送扣费</option>
								<option value='SYSTEM_REDUCE_MONEY_FOR_ACTIVITIES'>活动彩金扣费</option>
								<%
				                    	}
				                    %>
							</select>
						</div>
						<div class="col-sm-6">
							<input type="text" value="" id='moneyCount'
								onkeyup="checkNum(this)" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">备注：</label>
						<div class="col-sm-9">
							<input type="text" id='description' class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">操作密码：</label>
						<div class="col-sm-9">
							<input type="password" id='operatePassword' class="form-control">
						</div>
					</div>
					<div class="col-sm-12 ibox">
						<div class="row">
							<div class="col-sm-12 text-center">
								<button type="button" class="btn btn-w-m btn-white"
									onclick="userRenew.userRenew()">提 交</button>
							</div>
						</div>
					</div>
					<div class="col-sm-12 ibox" style="left: 12%;">
						<div class="row notice" id="explain">
							<h2>* 注意事项：</h2>
							<p id="all">为确保报表盈利数据准确，请认真选择存提类型；</p>
							<p id="SYSTEM_ADD_MONEY">人工存入：可直接提现，用于补派奖缺少，测试投注使用，计入盈亏报表活动赠送正值；</p>
							<p id="RECHARGE_MANUAL">人工存款：客户入款需要手动添加时使用，计入报表充值额，但不生成充值订单；</p>
							<p id="MANAGE_REDUCE_MONEY">行政提出：派彩错误、强行提出时使用，计入报表活动赠送负值；</p>
							<p id="SYSTEM_REDUCE_MONEY">误存提出：误存时使用，扣减用户余额使用，不计入盈亏报表；</p>
							<p id="RECHARGE_PRESENT">充值赠送：客户充值赠送金额使用，计入盈亏报表活动赠送正值；</p>
							<p id="ACTIVITIES_MONEY">活动彩金：给客户赠送活动彩金使用，计入盈亏报表活动赠送正值；</p>
							<p id="SYSTEM_REDUCE_MONEY_FOR_RECHARGE_PRESENT">充值赠送扣费：扣减客户充值赠送金额使用，需已有充值赠送过，计入盈亏报表活动赠送负值；</p>
							<p id="SYSTEM_REDUCE_MONEY_FOR_ACTIVITIES">活动彩金扣费：扣减客户活动彩金金额使用，需已有获得活动彩金过，计入盈亏报表活动赠送负值；</p>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- js -->
	<script type="text/javascript"
		src="<%=path%>/admin/user/js/user_renew.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
	<!-- dwr -->
	<script type='text/javascript'
		src='<%=path%>/dwr/interface/managerUserAction.js'></script>
	<script type="text/javascript">
userRenew.param.userId = <%=userId %>;
</script>
</body>
</html>