<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>会员总额报表</title>
	<style type="text/css">
	.wrapper {padding:0 10px}
	.wrapper-content {padding:10px}
	.jsstar{list-style: none;margin: 0px;padding: 0px; width: 100px;height: 20px;position: relative;margin-left: 172px;}
	.jsstar li{padding:0px;margin: 0px;float: left; width:20px;height:20px;background:url(../../admin/resource/img/star_rating.gif) 0 0 no-repeat;} 
	.jsstar_bright {background-position : left bottom;}
	</style>
	<jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
<body>
    <div class="animated fadeIn" style="padding: 0 5px;">
        <div class="row">
            <div class="col-sm-12">
                <!-- <div class="ibox "> -->
                    <div class="ibox-content">
                        <div class="row m-b-sm m-t-sm">
                            <form class="form-horizontal">
                            <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                                <div class="row">
                                  <div class="col-sm-3">
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">商户</span>
                                            <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
                                            </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">用户名</span>
                                            <input id="userName" type="text" value="" class="form-control"></div>
                                    </div>
                                    <div class="col-sm-3">
	                                    <div class="input-group m-b">
	                                    	<span class="input-group-addon">排序字段</span>
	                                        <select class="form-control" id="orderField" name="orderField">
	                                            <option value="">＝＝请选择＝＝</option>
					                            <option value="money">余额</option>
					                            <option value="totalRecharge">充值</option>
					                            <option value="totalWithdraw">取款</option>
					                            <option value="totalLottery">投注(-撤单)</option>
					                            <option value="totalWin">中奖</option>
					                            <option value="totalRebate">投注返点</option>
					                            <option value="totalPercentage">推广返点</option>
					                            <option value="totalHalfMonthBonus">分红</option>
					                            <option value="totalDaySalary">工资</option>
					                            <option value="gain">盈利</option>
				        					</select>
				        				</div>
                                    </div>
                                    <div class="col-sm-2">
	                                    <div class="input-group m-b">
	                                    	<span class="input-group-addon">排序方式</span>
	                                        <select class="form-control" id="orderType" name="orderType">
					                            <option value="bigBySmall"  selected="selected">从大到小</option>
					                            <option value="smallByBig">从小到大</option>
				                   			</select>
				                   		</div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">金额从</span>
                                            <input id="startMoney" type="text" name="startMoney" value="" class="form-control">
                                            <span class="input-group-addon">到</span>
                                            <input id="endMoney" type="text" name="endMoney" value="" class="form-control">
                                            </div>
                                    </div>
                                    <div class="col-sm-5"></div>
                                    <div class="col-sm-7">
                                        <div class="form-group nomargin text-right">
                                        	<button type="button" class="btn btn-outline btn-default btn-addstar" onclick="userConsumeReport.addStar()"><i class="fa fa-plus"></i>&nbsp;一键加星处理</button>
                                            <button type="button" class="btn btn-outline btn-default btn-kj" onclick="userConsumeReport.getTotalUsersConsumeReportsForQuery(1)"><i class="fa fa-search"></i>&nbsp;查 询</button>
                                        </div>
                                    </div>
                                </div>
                                </c:if>
                                  <c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">用户名</span>
                                            <input id="userName" type="text" value="" class="form-control" width="40%"></div>
                                    </div>
                                    <div class="col-sm-3">
                                    <div class="input-group m-b">
                                            <span class="input-group-addon">排序字段</span>
                                           <select class="form-control" id="orderField" name="orderField">
                                                        <option value="">＝＝请选择＝＝</option>
				                            <option value="money">余额</option>
				                            <option value="totalRecharge">充值</option>
				                            <option value="totalWithdraw">取款</option>
				                            <option value="totalLottery">投注(-撤单)</option>
				                            <option value="totalWin">中奖</option>
				                            <option value="totalRebate">投注返点</option>
				                            <option value="totalPercentage">推广返点</option>
				                            <option value="totalHalfMonthBonus">分红</option>
				                            <option value="totalDaySalary">工资</option>
				                            <option value="gain">盈利</option>
				                            
				                            
			        </select></div>
                                    </div>
                                    <div class="col-sm-3">
                                    <div class="input-group m-b">
                                            <span class="input-group-addon">排序方式</span>
                                           <select class="form-control" id="orderType" name="orderType">
				                            <option value="bigBySmall"  selected="selected">从大到小</option>
				                            <option value="smallByBig">从小到大</option>
			                   </select></div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="input-group m-b">
                                            <span class="input-group-addon">金额从</span>
                                            <input id="startMoney" type="text" name="startMoney" value="" class="form-control">
                                            <span class="input-group-addon">到</span>
                                            <input id="endMoney" type="text" name="endMoney" value="" class="form-control">
                                            </div>
                                    </div>
                                    <div class="col-sm-1">
                                        <div class="form-group nomargin text-right">
                                            <button type="button" class="btn btn-outline btn-default btn-kj" onclick="userConsumeReport.getTotalUsersConsumeReportsForQuery(1)"><i class="fa fa-search"></i>&nbsp;查 询</button></div>
                                    </div>
                                </div>
                                </c:if>
                                <!-- <select class="form-control" id="showType" name="showType">
                                                        <option value="">＝＝请选择＝＝</option>
				                            <option value="PC">电脑</option>
				                            <option value="MOBILE">手机</option>
			        </select> -->
                            </form>
                        </div>
                        <div class="jqGrid_wrapper">
                            <div class="ui-jqgrid ">
                                <div class="ui-jqgrid-view">
                                    <div class="ui-jqgrid-hdiv">
                                        <div class="ui-jqgrid-hbox" style="padding-right:0">
                                            <table class="ui-jqgrid-htable ui-common-table table table-bordered" id="userMoneyTotalTable">
                                                <thead>
                                                    <tr class="ui-jqgrid-labels">
                                                    <th class="ui-th-column ui-th-ltr" >商户</th>
                                                      <th class="ui-th-column ui-th-ltr">用户名</th>
								          <th class="ui-th-column ui-th-ltr">余额</th>
								          <th class="ui-th-column ui-th-ltr">充值</th>
								          <th class="ui-th-column ui-th-ltr">取款</th>
								          <th class="ui-th-column ui-th-ltr">投注(-撤单)</th>
								          <th class="ui-th-column ui-th-ltr">中奖</th>
								          <th class="ui-th-column ui-th-ltr">投注返点</th>
								          <th class="ui-th-column ui-th-ltr">推广返点</th>
								          <th class="ui-th-column ui-th-ltr">分红</th>
								          <th class="ui-th-column ui-th-ltr">工资</th>
								          <th class="ui-th-column ui-th-ltr">盈利</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                     
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>          
                            </div>
                        </div>
                    </div>
               <!--  </div> -->
            </div>
        </div>
    </div>
<!-- js -->
<script type="text/javascript" src="<%=path%>/admin/user/js/user_statistical_rankings.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerUserAction.js'></script>
</body>
</html>

