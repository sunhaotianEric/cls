<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import="java.math.BigDecimal,java.lang.Double"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<html>
<head>
<meta charset="utf-8" />
<title>支付平台管理</title>
<jsp:include page="/admin/include/include.jsp"></jsp:include>
<!-- js -->
<script type="text/javascript" src="<%=path%>/admin/user/js/user_point.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerUserAction.js'></script>
</head>
<%
 String userId = request.getParameter("id");  //获取查询的商品ID
%>
<script type="text/javascript">
userPoint.param.userId = <%=userId %>;
</script>
<body>
   <div class="alert alert-info">当前位置<b class="tip"></b><a href="/managerzaizst/user/user_list.html">会员信息管理</a><b class="tip"></b>添加会员</div>
   <br/>
   <table class="tbform list">
        <tbody>
            <tr>
                <td width="13px">会员名称</td>
                <td><label id='username'></label></td>
            </tr> 
            <tr>
                <td width="13px">可用积分</td>
                <td><label id='userPoint'></label></td>
            </tr>          
            <tr>
                <td width="13px">积分操作</td>
                <td>
                  <select id='operationType'>
                    <option value=''>==请选择==</option>
                    <option value='add'>增加</option>
                    <option value='reduce'>减少</option>
                  </select>
                  <input type="text" id='pointCount' class="ipt" value='' onkeyup="checkNum(this)"/> 
                </td>
            </tr>   
            <tr>
                <td colspan="6" align="left">
                    <input class="btn" onclick="userPoint.userPoint()" type="button" value="提交" />
            </tr>        
        </tbody>
    </table> 
</body>
</html>