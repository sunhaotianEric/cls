<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
    
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>会员盈亏报表</title>
        <jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
    <body>
        <div class="animated fadeIn">
            <div class="ibox-content ibox">
                <div class="row m-b-sm m-t-sm">
                    <form class="form-horizontal" id="queryFrom">
                    <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                        <div class="row">
                           <div class="col-sm-2">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">商户</span>
                                    <cls:bizSel name="bizSystem" id="bizSystem" options="class:'ipt form-control'" emptyOption="false"/>                 
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">用户名</span>
                                    <input id='userName' type="text" value="" class="form-control"></div>
                            </div>
                            <div class="col-sm-5">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">时间始终</span>
                                    <div class="input-daterange input-group" id="datepicker">
                                        <input class="form-control layer-date" placeholder="开始日期" id="teamUserConsumeHistoryReportDateStart"  onclick="laydate({istime: true, format: 'YYYY-MM-DD'})">
                                        <span class="input-group-addon">到</span>
                                        <input class="form-control layer-date" placeholder="结束日期" id="teamUserConsumeHistoryReportDateEnd" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})"></div>
                                </div>
                            </div>
              
                            <div class="col-sm-3">
                                <div class="form-group nomargin text-right">
                                    <button type="button" class="btn btn-outline btn-default" onclick="userConsumeReport.getAllConsumeReportsForQuery()"><i class="fa fa-search"></i>&nbsp;查 询</button></div>
                            </div>
                        </div>
                        </c:if>
                       <c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">用户名</span>
                                    <input id='userName' type="text" value="" class="form-control"></div>
                            </div>
                            <div class="col-sm-5">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">时间始终</span>
                                    <div class="input-daterange input-group" id="datepicker">
                                        <input class="form-control layer-date" placeholder="开始日期" id="teamUserConsumeHistoryReportDateStart"  onclick="laydate({istime: true, format: 'YYYY-MM-DD'})">
                                        <span class="input-group-addon">到</span>
                                        <input class="form-control layer-date" placeholder="结束日期" id="teamUserConsumeHistoryReportDateEnd" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})"></div>
                                </div>
                            </div>
       
                            <div class="col-sm-4">
                                <div class="form-group nomargin text-right">
                                    <button type="button" class="btn btn-outline btn-default" onclick="userConsumeReport.getAllConsumeReportsForQuery()"><i class="fa fa-search"></i>&nbsp;查 询</button></div>
                            </div>
                        </div>
                        </c:if>
                    </form>
                    <div class="jqGrid_wrapper">
                        <div class="ui-jqgrid ">
                            <div class="ui-jqgrid-view">
                                <div class="ui-jqgrid-hdiv">
                                    <div class="ui-jqgrid-hbox" style="padding-right:0">
                                        <table class="ui-jqgrid-htable ui-common-table table table-bordered" id="userConsumeReportTable">
                                            <thead>
                                                <tr class="ui-jqgrid-labels">
                                                 <th class="ui-th-column ui-th-ltr" id="bizSystemTr" style="display: none;">商户</th>
                                                    <th class="ui-th-column ui-th-ltr">用户名</th>
                                                   
                                                    <th class="ui-th-column ui-th-ltr">充值</th>
				                                               
                                                    <th class="ui-th-column ui-th-ltr">提现</th>
                                             
                                                    <th class="ui-th-column ui-th-ltr">投注(-撤单)</th>
                                                     <th class="ui-th-column ui-th-ltr">中奖</th>
                                                    
                                                    <th class="ui-th-column ui-th-ltr">返点</th>
                                                    <th class="ui-th-column ui-th-ltr">活动赠送</th>
                                                 
                                                    <!-- <th class="ui-th-column ui-th-ltr">其他</th> -->
                                                    <th class="ui-th-column ui-th-ltr">行政提出</th>
                                                    <th class="ui-th-column ui-th-ltr">盈利</th>
                                                    <th class="ui-th-column ui-th-ltr">注册/首充/投注(人)</th>
                                                    </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!-- js -->
<script type="text/javascript" src="<%=path%>/admin/user/js/user_consume_report.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerTeamAction.js'></script>
<script type='text/javascript' src='<%=path%>/dwr/interface/managerUserAction.js'></script>
<script type="text/javascript">
function chageDateFt(){
	var nowDate = new Date();
	nowDate = nowDate.AddDays(-1);
	if(nowDate.getHours() < 3) {
		$("#teamUserConsumeHistoryReportDateStart").attr("onclick","laydate({istime: true, format: 'YYYY-MM-DD',max: laydate.now(-1)})");
		$("#teamUserConsumeHistoryReportDateEnd").attr("onclick","laydate({istime: true, format: 'YYYY-MM-DD',max: laydate.now(-1)})");
	}else{
		$("#teamUserConsumeHistoryReportDateStart").attr("onclick","laydate({istime: true, format: 'YYYY-MM-DD',max: laydate.now(0)})");
		$("#teamUserConsumeHistoryReportDateEnd").attr("onclick","laydate({istime: true, format: 'YYYY-MM-DD',max: laydate.now(0)})");
	}
	
	$("#teamUserConsumeHistoryReportDateStart").val(nowDate.format("yyyy-MM-dd"));
	$("#teamUserConsumeHistoryReportDateEnd").val(nowDate.format("yyyy-MM-dd"));

}
chageDateFt();


</script>
</body>
</html>

