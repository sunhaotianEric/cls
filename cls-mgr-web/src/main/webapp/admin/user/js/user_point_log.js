function UserPointLogPage(){}
var userPointLog = new UserPointLogPage();

userPointLog.param = {
};

//分页参数
userPointLog.pageParam = {
        queryMethodParam : null,
        pageNo : 1,
        pageMaxNo : 1,  //最大的页数
        skipHtmlStr : ""
};

/**
 * 查询参数
 */
userPointLog.queryParam = {
		detailType : null,
		userName : null,
		createdDateStart : null,
		createdDateEnd : null
};

$(document).ready(function() {
	$("#createdDateStart").val(new Date().AddDays(-1).format("yyyy-MM-dd"));
	$("#createdDateEnd").val(new Date().AddDays(-1).format("yyyy-MM-dd"));
	
	userPointLog.getAllPointLogsDetailTypes(); //加载资金明细类型
	userPointLog.getAllPointLogsForQuery(); //根据条件,查找资金明细
});
	
/**
 * 加载所有的登陆日志数据
 */
UserPointLogPage.prototype.getAllPointLogsForQuery = function(){
	userPointLog.pageParam.queryMethodParam = 'getAllPointLogsForQuery';
	userPointLog.queryParam = {};
	
	var pointDetailTypes = $("#pointDetailTypes").val();
	var userName = $("#userName").val();
	var createdDateStart = $("#createdDateStart").val();
	var createdDateEnd = $("#createdDateEnd").val();
	
	if(pointDetailTypes == ""){
		userPointLog.queryParam.detailType = null;
	}else{
		userPointLog.queryParam.detailType = pointDetailTypes;
	}
	
	if(userName == ""){
		userPointLog.queryParam.userName = null;
	}else{
		userPointLog.queryParam.userName = userName;
	}
	
	if(createdDateStart == ""){
		userPointLog.queryParam.createdDateStart = null;
	}else{
		userPointLog.queryParam.createdDateStart = new Date(createdDateStart);
	}
	if(createdDateEnd == ""){
		userPointLog.queryParam.createdDateEnd = null;
	}else{
		userPointLog.queryParam.createdDateEnd = new Date(createdDateEnd);
	}
	
	userPointLog.queryConditionPointLogs(userPointLog.queryParam,userPointLog.pageParam.pageNo);
};

/**
 * 查找资金明细
 */
UserPointLogPage.prototype.queryConditionPointLogs = function(queryParam,pageNo){
	managerPointDetailAction.getAllPointLogsByQuery(queryParam,pageNo,function(r){
		if (r[0] != null && r[0] == "ok") {
			userPointLog.refreshPointLogPages(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询所有的资金类型失败.");
		}
    });
};

/**
 * 刷新列表数据
 */
UserPointLogPage.prototype.refreshPointLogPages = function(page){
	var pointLogListObj = $("#pointLogList");
	
	pointLogListObj.html("");
	var str = "";
    var pointLogs = page.pageContent;
    
    //记录数据显示
	for(var i = 0 ; i < pointLogs.length; i++){
		var pointLog = pointLogs[i];
		str += "<tr>";
		str += "  <td><a href='javascript:void(0)' style='color:blue;text-decoration:underline;'  onclick='commonPage.showUserMsgPage(this,"+pointLog.userId+")'>"+ pointLog.userName +"</a></td>";
		if(pointLog.income == 0){ //收入为0的情况
			str += "  <td>-"+ pointLog.pay +"</td>";
		}else{
			str += "  <td>"+ pointLog.income +"</td>";
		}
		str += "  <td>"+ pointLog.pointsAfter +"</td>";
		str += "  <td>"+ pointLog.createdDateStr +"</td>";
		str += "  <td><a style='color: blue;text-decoration: underline;' href='javascript:void(0)' onclick='userPointLog.showPointLogDes(this,\""+pointLog.description+"\")'>"+ pointLog.detailTypeDes +"</a></td>";
		str += "</tr>";
		pointLogListObj.append(str);
		str = "";
	}
	
	//记录统计
	var pageMsg = $("#pageMsg");
	var pageStr = "";
	pageStr += "<th colspan='100'>";
	pageStr += "  当前第" + page.pageNo + "页/共" + page.totalPageNum +"页&nbsp;&nbsp;共" + page.totalRecNum +"条记录&nbsp;&nbsp;<a href='javascript:void(0)'  onclick='userPointLog.pageQuery(1)'>首页</a>&nbsp;";
	pageStr += "<a href='javascript:void(0)' onclick='userPointLog.pageParam.pageNo--;userPointLog.pageQuery(userPointLog.pageParam.pageNo)'>上一页</a>&nbsp;";
	pageStr += "<a href='javascript:void(0)' onclick='userPointLog.pageParam.pageNo++;userPointLog.pageQuery(userPointLog.pageParam.pageNo)'>下一页</a>&nbsp;";
	pageStr += "<a href='javascript:void(0)' onclick='userPointLog.pageQuery("+ page.totalPageNum +")'>尾页</a>";
	
	var skipHtmlStr = "";
	if($("#pageSkip").html() != null){
		$("#pageSkip").remove();
	}
    skipHtmlStr += "<span id='pageSkip'>";
    skipHtmlStr += "&nbsp;&nbsp;跳转至";
	skipHtmlStr += "<select id='currentPageChoose' class='ipt' onchange='userPointLog.pageParam.pageNo = this.value;userPointLog.pageQuery(userPointLog.pageParam.pageNo)'  style='width:auto;'>";
	for(var i = 1; i <= page.totalPageNum; i++){
		skipHtmlStr += "<option value='"+ i +"'>"+ i +"</option>";
	}
	skipHtmlStr += "</select>";
	skipHtmlStr += "</span>";	
	pageStr += skipHtmlStr;
	pageStr += "</th>";
	pageMsg.html(pageStr);
	
	$("#currentPageChoose").find("option[value='"+ page.pageNo +"']").attr("selected",true);  //显示当前页码
	userPointLog.pageParam.pageMaxNo = page.totalPageNum;  //标识最大的页数
};

/**
 * 根据条件查找对应页
 */
UserPointLogPage.prototype.pageQuery = function(pageNo){
	if(pageNo < 1){
		userPointLog.pageParam.pageNo = 1;
	}else if(pageNo > userPointLog.pageParam.pageMaxNo){
		userPointLog.pageParam.pageNo = userPointLog.pageParam.pageMaxNo;
	}else{
		userPointLog.pageParam.pageNo = pageNo;
	}
	//如果页码小于等于0，则不进行分页查找
	if(userPointLog.pageParam.pageNo <= 0){
		return;
	}
	
	if(userPointLog.pageParam.queryMethodParam == 'getAllPointLogsForQuery'){
		userPointLog.getAllPointLogsForQuery();
	}else{
		alert("类型对应的查找方法未找到.");
	}
};

/**
 * 加载资金明细类型
 */
UserPointLogPage.prototype.getAllPointLogsDetailTypes = function(){
	managerPointDetailAction.getAllPointLogsDetailTypes(function(r){
		if (r[0] != null && r[0] == "ok") {
			userPointLog.refreshPointLogsTypes(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询所有的资金类型失败.");
		}
    });
};

/**
 * 展示资金类型数据
 */
UserPointLogPage.prototype.refreshPointLogsTypes = function(detailTypeMaps){
	var obj=document.getElementById('pointDetailTypes');
	obj.options.add(new Option("=请选择=",""));
	
	for(var key in detailTypeMaps){
		obj.options.add(new Option(detailTypeMaps[key],key)); 
	}  
};

/**
 * 显示资金明细的描述
 */
UserPointLogPage.prototype.showPointLogDes = function(obj,des){
	var d = dialog({
	    content: des,
	    padding: 0,
	    quickClose: true// 点击空白处快速关闭
	});
	d.show(obj);
};
