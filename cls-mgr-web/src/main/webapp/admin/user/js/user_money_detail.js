function UserMoneyDetailPage(){}
var userMoneyDetail = new UserMoneyDetailPage();

userMoneyDetail.param = {
};

//分页参数
userMoneyDetail.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize
};

/**
 * 查询参数
 */
userMoneyDetail.queryParam = {
		detailType : null,
		lotteryId : null,
		expect : null,
		enabled : 1,	//默认用户非游客,有效数据
		userName : null,
		createdDateStart : null,
		createdDateEnd : null
};

$(document).ready(function() {
	userMoneyDetail.getAllMoneyDetailsDetailTypes();
	commonPage.setDateRangeTime(1, "createdDateStart", "createdDateEnd",2);
	userMoneyDetail.initTableData(); //根据条件,查找资金明细
});
	
/**
 * 加载所有的登陆日志数据
 */
UserMoneyDetailPage.prototype.getAllMoneyDetailsForQuery = function(dateRange){
	userMoneyDetail.pageParam.queryMethodParam = 'getAllMoneyDetailsForQuery';
	if(dateRange != undefined && dateRange != null && dateRange != ''){
		commonPage.setDateRangeTime(dateRange, "createdDateStart", "createdDateEnd",2);	
	}
	var moneyDetailTypes = $("#moneyDetailTypes").val();
	var bizSystem = $("#bizSystem").val();
	var expect = $("#expect").val();
	var userName = $("#userName").val();
	var lotteryId = $("#lotteryId").val();
	var createdDateStart = $("#createdDateStart").val();
	var createdDateEnd = $("#createdDateEnd").val();
	userMoneyDetail.queryParam.bizSystem = getSearchVal("bizSystem");
	if(moneyDetailTypes == ""){
		userMoneyDetail.queryParam.detailType = null;
	}else{
		userMoneyDetail.queryParam.detailType = moneyDetailTypes;
	}
	if(bizSystem == ""){
		userMoneyDetail.queryParam.bizSystem = null;
	}else{
		userMoneyDetail.queryParam.bizSystem = bizSystem;
	}
	if(expect == ""){
		userMoneyDetail.queryParam.expect = null;
	}else{
		userMoneyDetail.queryParam.expect = expect;
	}
	
	if(lotteryId == ""){
		userMoneyDetail.queryParam.lotteryId = null;
	}else{
		userMoneyDetail.queryParam.lotteryId = lotteryId;
	}
	
	if(userName == ""){
		userMoneyDetail.queryParam.userName = null;
	}else{
		userMoneyDetail.queryParam.userName = userName;
	}
	
	if(createdDateStart == ""){
		userMoneyDetail.queryParam.createdDateStart = null;
	}else{
		userMoneyDetail.queryParam.createdDateStart = new Date(createdDateStart);
	}
	if(createdDateEnd == ""){
		userMoneyDetail.queryParam.createdDateEnd = null;
	}else{
		userMoneyDetail.queryParam.createdDateEnd = new Date(createdDateEnd);
	}
	
	userMoneyDetail.table.ajax.reload();
};




/**
 * 初始化列表数据
 */
UserMoneyDetailPage.prototype.initTableData = function() {
	userMoneyDetail.queryParam = getFormObj($("#queryForm"));
	if(isTouristParameter == 1){
		userMoneyDetail.queryParam.enabled = 0;
	}else{
		userMoneyDetail.queryParam.enabled = 1;
	}
	var moneyDetailTotal;
	userMoneyDetail.table = $('#moneyDetailTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"scrollX": true,
		"iDisplayLength" : defaultPageSize,
		"fnDrawCallback": function( oSettings ) {
//			var str="";
//			if(moneyDetailTotal != null){
//				str += "  <td style='color:red;'>"+ (moneyDetailTotal.income - moneyDetailTotal.pay).toFixed(commonPage.param.fixVaue) +"</td>";
//			}else{
//				str += "  <td>0</td>";
//			}
//			if(currentUser.bizSystem=='SUPER_SYSTEM'){
//		      $("#moneyDetailTable tbody").append("<tr class='old'><td>当前查询条件总计:</td><td></td><td></td><td></td><td></td>"+str+"<td></td><td></td><td></td></tr>");
//			}else{
//				 $("#moneyDetailTable tbody").append("<tr class='old'><td>当前查询条件总计:</td><td></td><td></td><td></td><td></td>"+str+"<td></td><td></td></tr>");	
//			}
			},
		"ajax":function (data, callback, settings) {
			//alert(1);
			userMoneyDetail.pageParam.pageSize = data.length;//页面显示记录条数
			userMoneyDetail.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerMoneyDetailAction.getAllMoneyDetailsByQuery(userMoneyDetail.queryParam,userMoneyDetail.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
					moneyDetailTotal=r[2];
					
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": "bizSystemName","bVisible":currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "lotteryId"},
		            {
		            	"data": "userName",
		            	render: function(data, type, row, meta) {
		            		return "<a href='javascript:void(0)' style='color:blue;text-decoration:underline;'  onclick='commonPage.showUserMsgPage(\""+data+"\","+row.userId+")'>"+ row.userName +"</a>";
	            		}
		            },
		            {"data": "detailTypeDes"},

		            {"data": "operateUserName"},
	
		            {   
		            	"data": "income",
		            	render: function(data, type, row, meta) {
		            		var str="";
		            		if(row.income == 0){ //收入为0的情况
		            			str += "-"+ row.pay.toFixed(commonPage.param.fixVaue);
		            		}else{
		            			str += ""+ row.income.toFixed(commonPage.param.fixVaue);
		            		}
		            		return str;
	            		}
		            },
		            {"data": "balanceAfter"},
		            {"data": "createDateStr"},
		            {   
		            	"data": "description"
		            } 
	            ]
	}).api(); 
}


/**
 * 加载资金明细类型
 */
UserMoneyDetailPage.prototype.getAllMoneyDetailsDetailTypes = function(){
	managerMoneyDetailAction.getAllMoneyDetailsDetailTypes(function(r){
		if (r[0] != null && r[0] == "ok") {
			userMoneyDetail.refreshMoneyDetailTypes(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询所有的资金类型失败.");
		}
    });
};

/**
 * 展示资金类型数据
 */
UserMoneyDetailPage.prototype.refreshMoneyDetailTypes = function(detailTypeMaps){
	var obj=document.getElementById('moneyDetailTypes');
	obj.options.add(new Option("=请选择=",""));
	
	for(var key in detailTypeMaps){
		obj.options.add(new Option(detailTypeMaps[key],key)); 
	}  
};

/**
 * 获取订单详情
 */
UserMoneyDetailPage.prototype.getOrderCodesDes = function(obj,orderId){
	managerOrderAction.getOrderCodesDes(orderId,function(r){
		if (r[0] != null && r[0] == "ok") {
			userMoneyDetail.showOrderCodesPage(obj,r[1]);
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询订单号码详情失败请求失败.");
		}
    });
};

/**
 * 显示订单详情
 */
UserMoneyDetailPage.prototype.showOrderCodesPage = function(obj,order){
	var orderCodesStr = "";
	if(order != null){
		if(order.openCode != null){
			orderCodesStr += "<div>开奖号码:" + order.openCode +"</div>";
			orderCodesStr += "<div>奖金:" + order.winCost +"</div>";
		}else{
			orderCodesStr += "<div>开奖号码:<span style='color:red;'>未开奖</span></div>";
			orderCodesStr += "<div>奖金:<span style='color:red;'>未知</span></div>";
		}
		
		orderCodesStr += "<div>号码详情:<br/>";
		var orderCodesList = order.codesList;
		for(var i = 0; i < orderCodesList.length; i++){
			var orderCodesMap = orderCodesList[i];
			for(var key in orderCodesMap){
				orderCodesStr += "<span style='color:red;'>["+key+"]</span><br/>";
				orderCodesStr += "<span>"+orderCodesMap[key]+"</span><br/>";
//				var codeStr = orderCodesMap[key].split(",");
//				for(var i = 0; i < codeStr.length; i++){
//					orderCodesStr += "<span>"+codeStr[i]+"</span><br/>";
//				}
			}
		}

		orderCodesStr += "</div>";
	}else{
		orderCodesStr += "<span style='color:red;'>订单信息不准确</span>";
	}
	
	var d = dialog({
	    content: orderCodesStr,
	    padding: 0,
	    quickClose: true// 点击空白处快速关闭
	});
	d.show(obj);
};

/**
 * 显示资金明细的描述
 */
UserMoneyDetailPage.prototype.showMoneyDetailDes = function(obj,des){
	var d = dialog({
	    content: des,
	    padding: 0,
	    quickClose: true// 点击空白处快速关闭
	});
	d.show(obj);
};

/**
 * 根据id赋值selelct标签
 * @param id
 */

//UserMoneyDetailPage.prototype.getallbizSystem=function (id)
//{
//	dwr.engine.setAsync(false); 
//	var selDom = $("#"+id);
//	managerBizSystemAction.getAllEnableBizSystem(function(r){
//		if (r[0] != null && r[0] == "ok") {
//			bizsystems=r[1];
//			for(var i=0;i<bizsystems.length;i++)
//			{
//				var bs=bizsystems[i];
//				
//				selDom.append("<option value='"+bs.bizSystem+"'>"+bs.bizSystemName+"</option>");
//			}
//		}
//	});
//}
