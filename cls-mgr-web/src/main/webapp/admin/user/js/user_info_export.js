function UserInfoExportPage(){}
var userInfoExportPage = new UserInfoExportPage();

/**
 * 查询参数
 */
userInfoExportPage.queryParam = {
		userName : null 
};


$(document).ready(function() {
	//业务系统选择框初始化
//	if(currentUser.bizSystem=='SUPER_SYSTEM'){
//		userInfoExportPage.getallbizSystem('bizSystem');	
//	}
		
	
	$('#hasTruename').attr("checked", true);
	$('#hasPhone').attr("checked", true);
	$('#hasQq').attr("checked", true);
});

/**
 * 加载所有的登陆日志数据
 */
UserInfoExportPage.prototype.userInfoExport = function(){
	userInfoExportPage.queryParam = {};
	
	var userName = $("#userName").val();
	userInfoExportPage.queryParam.hasTruename = $('#hasTruename').is(':checked');
	userInfoExportPage.queryParam.hasAddress = $('#hasAddress').is(':checked');
	userInfoExportPage.queryParam.hasPhone = $('#hasPhone').is(':checked');
	userInfoExportPage.queryParam.hasQq = $('#hasQq').is(':checked');
	userInfoExportPage.queryParam.hasEmail = $('#hasEmail').is(':checked');
	userInfoExportPage.queryParam.hasIdentityid = $('#hasIdentityid').is(':checked');
	userInfoExportPage.queryParam.hasAddtime = $('#hasAddtime').is(':checked');
	//增加用户所在系统
	userInfoExportPage.queryParam.bizSystem= $('#bizSystem').val();
	if(userName == ""){
		alert("导出用户名不能为空");
		return;
	}else{
		userInfoExportPage.queryParam.userName = userName;
	}
	
	managerUserAction.judgeUserExist(userInfoExportPage.queryParam,function(r){
		if (r[0] != null && r[0] == "ok") {
			//用户名存在才可以下载
			managerUserAction.userInfoExport(userInfoExportPage.queryParam,{
		        callback:function(data){
		            dwr.engine.openInDownload(data);
		        },
		        async : false
		    });
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "请求查找用户是否存在失败");
		}
    });	
	
	
};


/**
 * 根据id赋值selelct标签
 * @param id
 */

//UserInfoExportPage.prototype.getallbizSystem=function (id)
//{
//	
//	dwr.engine.setAsync(false); 
//	var selDom = $("#"+id);
//	managerBizSystemAction.getAllEnableBizSystem(function(r){
//		if (r[0] != null && r[0] == "ok") {
//			bizsystems=r[1];
//			for(var i=0;i<bizsystems.length;i++)
//			{
//				var bs=bizsystems[i];
//				
//				selDom.append("<option value='"+bs.bizSystem+"'>"+bs.bizSystemName+"</option>");
//			}
//		}
//	});
//}





