function UserStarLevelAdjust(){};
var userStarLevelAdjust=new UserStarLevelAdjust();

/**
 * 添加参数
 */
userStarLevelAdjust.editParam = {
	bizSystem:null,
	userName:null,
	starLevel:null,
	sl:null
};

$(document).ready(function() {
	//绑定星级编辑事件
	//绑定星级编辑事件
	$(".jsstar >li").hover(
        function(){
			var inLevel = $(this).attr("data-val");
			userStarLevelAdjust.setStarLevel(inLevel);
		},
        function(){
			userStarLevelAdjust.setStarLevel(userStarLevelAdjust.editParam.starLevel);
		})
    .click(function(){
    	userStarLevelAdjust.editParam.starLevel = $(this).attr("data-val");
    	userStarLevelAdjust.setStarLevel(userStarLevelAdjust.editParam.starLevel);
	});  
});

UserStarLevelAdjust.prototype.setStarLevel = function(starLevel) {
	$(".jsstar li").each(function(i){
		if(i < starLevel) {
			$(this).css({"background-position":"left bottom"});
		} else {
			$(this).css({"background-position":"left top"});
		}
	});
}
	
/**
 * 加载数据
 */
UserStarLevelAdjust.prototype.queryUser=function(){
	var bizSystem=$("#bizSystem").val();
	var userName=$("#userName").val();
	
	if(bizSystem!=null&&bizSystem!=""){
		userStarLevelAdjust.editParam.bizSystem=bizSystem;
	}else{
		userStarLevelAdjust.editParam.bizSystem=null;
	}
	if(userName!=null&&userName!=""){
		userStarLevelAdjust.editParam.userName=userName;
	}else{
		userStarLevelAdjust.editParam.userName=null;
		swal("用户名不能为空");
		return;
	}
	managerUserAction.queryUserLowerNumberByName(userStarLevelAdjust.editParam,function(r){
		if(r[0]=="ok"){
			$("#sl").text("共查询到所有下级会员"+r[2]+"个");
			userStarLevelAdjust.editParam.sl=r[2];
			userStarLevelAdjust.setStarLevel(r[1].starLevel);
			userStarLevelAdjust.editParam.starLevel=r[1].starLevel;
			userStarLevelAdjust.editParam.bizSystem=r[1].bizSystem;
			$("#addExtendLinkButton").removeAttr("disabled");
		}else{
			swal({ title: "提示", text: ""+r[1],type: "warning"});
			userRebateAdjust.editParam.userName=null;
			userRebateAdjust.editParam.bizSystem=null;
			userStarLevelAdjust.setStarLevel(0);
			$("#addExtendLinkButton").attr("disabled","disabled");
		}
	});
}

/**
 * 修改用户星级
 */
UserStarLevelAdjust.prototype.updateUserAllLowerLevelsByName=function(){
	if(userStarLevelAdjust.editParam.bizSystem==null||userStarLevelAdjust.editParam.userName==null){
		swal("请先查询上级用户");
		return;
	}
	if(userStarLevelAdjust.editParam.starLevel==null){
		swal("星级数不能为空");
		return;
	}
	if($("input[name='box']").is(":checked")){
		userStarLevelAdjust.editParam.state=1;
	}else{
		userStarLevelAdjust.editParam.state=null;
	}
	userStarLevelAdjust.editParam.code=2;
	managerUserAction.updateUserAllLowerLevelsByName(userStarLevelAdjust.editParam,function(r){
		if(r[0]=="ok"){
			swal({ title: "提示", text: "修改成功,共修改"+r[1]+"位下级用户星级为"+userStarLevelAdjust.editParam.starLevel+"星",type: "success"});
		}else{
			swal(r[1]);
		}
	});
}
