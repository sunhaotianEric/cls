function UserConsumeReportPage(){}
var userConsumeReport = new UserConsumeReportPage();

userConsumeReport.consumeReports = new Array();
userConsumeReport.isquery=true;
userConsumeReport.isneedDestroy=true;//
//分页参数
userConsumeReport.pageParam = {
		   pageNo : 1,
	       pageSize : defaultPageSize
};

/**
 * 查询参数
 */
userConsumeReport.queryParam = {
		userName : null,
		teamLeaderName : null,
		createdDateStart : null,
		createdDateEnd : null,
		reportType : 0
};
userConsumeReport.table;
$(document).ready(function() {
	


	userConsumeReport.table =$('#userConsumeReportTable').dataTable({  
	    //是否允许销毁table，值为true或者false，默认为false.  
	    "destroy": true  
	}).api();
	
	if(currentUser.bizSystem=='SUPER_SYSTEM'){
		$("#bizSystemTr").show();
	}
	
});
	
/**
 * 加载所有的登陆日志数据
 */
UserConsumeReportPage.prototype.getAllConsumeReportsForQuery = function(){

	var userName = $("#userName").val();
	var bizSystem = $("#bizSystem").val();
	var startimeStr = "";
	var endtimeStr = "";

	startimeStr = $("#teamUserConsumeHistoryReportDateStart").val();
	endtimeStr = $("#teamUserConsumeHistoryReportDateEnd").val();
	if(startimeStr != ""){
		userConsumeReport.queryParam.createdDateStart = startimeStr.stringToDate();
	}
	if(endtimeStr != ""){
		userConsumeReport.queryParam.createdDateEnd = endtimeStr.stringToDate();
	}
	userConsumeReport.queryParam.bizSystem=bizSystem;
	userConsumeReport.queryParam.userName = userName;

	if(userConsumeReport.isneedDestroy){
		userConsumeReport.table.destroy();
		userConsumeReport.initTableData();
		userConsumeReport.isneedDestroy=false;
	}else{
		userConsumeReport.table.ajax.reload();
	}
};


/**
 * 初始化列表数据
 */
UserConsumeReportPage.prototype.initTableData = function() {
	userConsumeReport.table = $('#userConsumeReportTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"scrollX": true,
		"fnDrawCallback": function (oSettings) {  
           userConsumeReport.getTotalRes(userConsumeReport.pageParam.currentPageRes,"单页统计");

          userConsumeReport.getTotalRes(userConsumeReport.consumeReports,"总统计");   
        },
		"ajax":function (data, callback, settings) {
			userConsumeReport.pageParam.pageSize = data.length;//页面显示记录条数

			userConsumeReport.pageParam.pageNo = (data.start / data.length)+1;//当前页码
			
			//封装返回数据
			var returnData = {
				recordsTotal : 0,
				recordsFiltered : 0,
				data : null
			};
//			if(userConsumeReport.isquery){
//				userConsumeReport.getCurrentPageRes(userConsumeReport.pageParam);
//				returnData.recordsTotal = userConsumeReport.pageParam.totalRecNum;//返回数据全部记录
//				returnData.recordsFiltered = userConsumeReport.pageParam.totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
//				returnData.data = userConsumeReport.pageParam.currentPageRes;//返回的数据列表
//				callback(returnData);
//			}else{
				managerTeamAction.getTeamConsumeReportForQuery(userConsumeReport.queryParam,userConsumeReport.pageParam,function(r){
					
					if (r[0] != null && r[0] == "ok") {
						userConsumeReport.initPageData(r[1]);
						
						returnData.recordsTotal = userConsumeReport.pageParam.totalRecNum;//返回数据全部记录
						returnData.recordsFiltered = userConsumeReport.pageParam.totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
						returnData.data = userConsumeReport.pageParam.currentPageRes;//返回的数据列表

					}else if(r[0] != null && r[0] == "error"){
						swal(r[1]);
						userConsumeReport.pageParam.totalRecNum=0;
						userConsumeReport.consumeReports = new Array();
						userConsumeReport.pageParam.currentPageRes=new Array();
						returnData.recordsTotal = userConsumeReport.pageParam.totalRecNum;//返回数据全部记录
						returnData.recordsFiltered = userConsumeReport.pageParam.totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
						returnData.data = userConsumeReport.pageParam.currentPageRes;//返回的数据列表

					}else{
						swal("查询数据失败!");
					}
					callback(returnData);
				});	
//			}
//			userConsumeReport.isquery=true;
	    	
		},
		"columns": [
                    {"data": "bizSystemName","bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "userName",
		            	  render: function(data, type, row, meta) {
		            		  return "<a href='javascript:void(0)' style='color:blue;text-decoration:underline;'  onclick='commonPage.showUserMsgPage(this,"+row.userId+")'>"+ row.userName +"</a>";
		            		}
		            },
		           
		            {"data": "recharge",
		            	  render: function(data, type, row, meta) {
		            		  return row.recharge.toFixed(commonPage.param.fixVaue);
		            		}
		            },
		            {"data": "withdraw",
		            	  render: function(data, type, row, meta) {
		            		  return row.withdraw.toFixed(commonPage.param.fixVaue);
		            		}
		            },
		            {"data": "lottery",
		            	  render: function(data, type, row, meta) {
		            		  return row.lottery.toFixed(commonPage.param.fixVaue);
		            		}
		            },
		            {"data": "win",
		            	render: function(data, type, row, meta) {
		            		return row.win.toFixed(commonPage.param.fixVaue);
		            	}
		            },
		            
		  
		            {"data": "rebate",
		            	  render: function(data, type, row, meta) {
		            		  return (row.rebate + row.percentage).toFixed(commonPage.param.fixVaue);
		            		}
		            },
		            {"data": "activitiesmoney",
		            	  render: function(data, type, row, meta) {
		            		  return (row.rechargepresent + row.activitiesmoney+row.systemaddmoney).toFixed(commonPage.param.fixVaue);
		            		}
		            },
		       /*     {"data": null,
		            	  render: function(data, type, row, meta) {
		            		  return (row.systemaddmoney - row.systemreducemoney).toFixed(commonPage.param.fixVaue);
		            		}
		            },*/
		            {"data":"managereducemoney",
		            	render: function(data, type, row, meta) {
		            		  return row.managereducemoney.toFixed(commonPage.param.fixVaue);
		            		}},
		            {"data": "gain",
		            	  render: function(data, type, row, meta) {
		            		  return row.gain.toFixed(commonPage.param.fixVaue);
		            		}
		            },
		            {"data": null,
		            	  render: function(data, type, row, meta) {
		            		  return (row.regCount==null?"-":row.regCount)+"/"+(row.firstRechargeCount==null?"-":row.firstRechargeCount)+"/"+(row.lotteryCount==null?"-":row.lotteryCount);
		            		}
		            }		            
	            ]
	}).api(); 
}



/**
 * 显示资金明细的描述
 */
UserConsumeReportPage.prototype.showMoneyDetailDes = function(obj,des){
	var d = dialog({
	    content: des,
	    padding: 0,
	    quickClose: true// 点击空白处快速关闭
	});
	d.show(obj);
};

/**
 * 进行数据统计的计算，返回tr字符串拼接
 * @param datas
 * @param totalDesc  单页统计或总统计
 */
UserConsumeReportPage.prototype.getTotalRes = function(datas, totalDesc) {
	var totalMoney = 0;
    var totalRecharge = 0;
    var totalWithDraw = 0;
    var totalRegister = 0;
    var totalExtend = 0;
    var totalLottery = 0;
    var totalWin = 0;
    var totalRebate = 0;
    var totalPercentage = 0;
    var totalPresent = 0;
    var totalOther = 0;
    var totalGain = 0;
    var totalRegCount=0;
    var totalManagereduceMoney=0;
    var totalFirstRechargeCoun=0;
    var totalLotteryCount=0;
	if(datas != null && datas.length > 0) {
		for(var i = 0; i < datas.length; i++) {
			var report = datas[i];
			totalMoney += report.money;
    	    totalRecharge += report.recharge;
    	    totalWithDraw += report.withdraw;
    		totalRegister += report.register;
    		totalExtend += report.extend;
    	    totalLottery += report.lottery;
    	    totalWin += report.win;
    	    totalRebate += report.rebate + report.percentage;
    	    totalPercentage += report.percentage;
    	    totalPresent += report.rechargepresent + report.activitiesmoney+report.systemaddmoney;
    	    //totalOther += (report.systemaddmoney - report.systemreducemoney);
    	    totalGain += report.gain;
    	    totalManagereduceMoney+=report.managereducemoney;
    	    if(totalDesc == '总统计' && report.userName == userConsumeReport.queryParam.userName){
    	    	totalRegCount = report.regCount;
        	    totalFirstRechargeCoun = report.firstRechargeCount;
        	    totalLotteryCount = report.lotteryCount;	
    	    }
    	  
    	    
		}
	}
	  if(totalDesc == '单页统计'){
	    	totalRegCount =null;
	    	totalFirstRechargeCoun = null; 
	    	totalLotteryCount = null;
	   }
	var str = "";
	str += "<tr style='color:red'>";
    str += "  <td>"+totalDesc+"</td>";
    if(currentUser.bizSystem=='SUPER_SYSTEM'){
     str +="<td></td>";
    }
	str += "  <td>"+ totalRecharge.toFixed(commonPage.param.fixVaue) +"</td>";
	str += "  <td>"+ totalWithDraw.toFixed(commonPage.param.fixVaue) +"</td>";
	str += "  <td>"+ totalLottery.toFixed(commonPage.param.fixVaue) +"</td>";
	str += "  <td>"+ totalWin.toFixed(commonPage.param.fixVaue) +"</td>";
	str += "  <td>"+ totalRebate.toFixed(commonPage.param.fixVaue) +"</td>";
	str += "  <td>"+ totalPresent.toFixed(commonPage.param.fixVaue) +"</td>";
	//str += "  <td>"+ totalOther.toFixed(commonPage.param.fixVaue) +"</td>";
	str += "  <td>"+ totalManagereduceMoney.toFixed(commonPage.param.fixVaue) +"</td>";
	str += "  <td>"+ totalGain.toFixed(commonPage.param.fixVaue) +"</td>";
	str += "  <td>"+ (totalRegCount==null?"-":totalRegCount)+"/"+(totalFirstRechargeCoun==null?"-":totalFirstRechargeCoun)+"/"+(totalLotteryCount==null?"-":totalLotteryCount)+"</td>";
    str += "</tr>";
    $("#userConsumeReportTable tbody").append(str);
    //return str;
}

/**
 * 初始化分页数据
 */
UserConsumeReportPage.prototype.initPageData = function(datas) {
	userConsumeReport.consumeReports = new Array();
	var page = userConsumeReport.pageParam;
	page.pageNo = userConsumeReport.pageParam.pageNo;
	page.totalPageNum = userConsumeReport.pageParam.totalPageNum;;
	if(datas != 0) {
		for(var key in datas){
			var report = datas[key];
			userConsumeReport.consumeReports.push(report);
		}
		page.totalRecNum = userConsumeReport.consumeReports.length;
		//计算总页数
		page.totalPageNum = parseInt(page.totalRecNum/page.pageSize);
		if(parseFloat("0."+page.totalRecNum%page.pageSize)>0){
			page.totalPageNum=page.totalPageNum+1;
		}
		userConsumeReport.getCurrentPageRes(page);
	}
}

/**
 * 得到当前页的数据
 */
UserConsumeReportPage.prototype.getCurrentPageRes = function(page) {
	if(userConsumeReport.consumeReports.length > 0) {
		page.currentPageRes = new Array();
		//计算查询当前页的开始结束位置
		var startIndex = page.pageSize*(page.pageNo-1);
		var endIndex = (page.pageNo * page.pageSize) - 1;
		if(page.pageNo * page.pageSize > page.totalRecNum) {
			endIndex = page.totalRecNum - 1;
		}
		for(var i = startIndex; i <= endIndex; i++) {
			page.currentPageRes.push(userConsumeReport.consumeReports[i]);
		}
	
	}
}



