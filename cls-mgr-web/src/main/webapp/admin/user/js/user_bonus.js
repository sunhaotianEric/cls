function UserMoneyDetailPage(){}
var userMoneyDetail = new UserMoneyDetailPage();

userMoneyDetail.param = {
};

$(document).ready(function() {
	userMoneyDetail.getSystemLastestCanBonusStartDate(); //加载科分红的最新资金明细记录
});
	
/**
 * 查找资金明细
 */
UserMoneyDetailPage.prototype.bonusDeal = function(){
	var createdDateStart = $("#createdDateStart").val();
	var createdDateEnd = $("#createdDateEnd").val();
	
	if(createdDateEnd == null || createdDateEnd == "" ||
			createdDateStart == null || createdDateStart == ""){
		alert("时间段不能为空.");
		return;
	}

	var startDate = new Date(createdDateStart);
	var endDate = new Date(createdDateEnd);
	
	if(startDate.getTime() >= endDate.getTime()){
        alert("结束时间不能小于开始时间");
		return;
	}
	
	managerMoneyDetailAction.systemBonus(endDate,function(r){
		if (r[0] != null && r[0] == "ok") {
			userMoneyDetail.refreshMoneyDetailPages(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询所有的资金类型失败.");
		}
    });
};

/**
 * 刷新列表数据
 */
UserMoneyDetailPage.prototype.refreshMoneyDetailPages = function(moneyDetails){
	var moneyDetailsListObj = $("#moneyDetailList");
	moneyDetailsListObj.html("");
	var str = "";
    
    //记录数据显示
	for(var i = 0 ; i < moneyDetails.length; i++){
		var moneyDetail = moneyDetails[i];
		str += "<tr>";
		str += "  <td>"+ moneyDetail.lotteryId +"</td>";
		str += "  <td><a href='javascript:void(0)' style='color:blue;text-decoration:underline;'  onclick='commonPage.showUserMsgPage(this,"+moneyDetail.userId+")'>"+ moneyDetail.userName +"</a></td>";
		if(moneyDetail.income == 0){ //收入为0的情况
			str += "  <td>-"+ moneyDetail.pay +"</td>";
		}else{
			str += "  <td>"+ moneyDetail.income +"</td>";
		}
		str += "  <td>"+ moneyDetail.balanceAfter +"</td>";
		str += "  <td>"+ moneyDetail.createDateStr +"</td>";
		str += "  <td>"+ moneyDetail.detailTypeDes +"</td>";
		str += "</tr>";
		moneyDetailsListObj.append(str);
		str = "";
	}
	
	//无记录情况的处理
	if(moneyDetails.length == 0){
		str += "<tr><td colspan='12' >当前无记录</td></tr>";
		moneyDetailsListObj.append(str);
	}
	
};

/**
 * 加载资金明细类型
 */
UserMoneyDetailPage.prototype.getSystemLastestCanBonusStartDate = function(){
	managerMoneyDetailAction.getSystemLastestCanBonusStartDate(function(r){
		if (r[0] != null && r[0] == "ok") {
			userMoneyDetail.showSystemLastestCanBonusStartDate(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询所有的资金类型失败.");
		}
    });
};

/**
 * 展示资金类型数据
 */
UserMoneyDetailPage.prototype.showSystemLastestCanBonusStartDate = function(moneyDetail){
	if(moneyDetail == null || moneyDetail.createDateStr == null || moneyDetail.createDateStr == ""){
		$("#bonusButton").val("暂无可分红的记录");
		$("#bonusButton").attr("disabled","disabled");
		$("#createdDateEnd").attr("disabled","disabled");
		return;
	}
	$("#createdDateStart").val(moneyDetail.createDateStr);
};

/**
 * 显示资金明细的描述
 */
UserMoneyDetailPage.prototype.showMoneyDetailDes = function(obj,des){
	var d = dialog({
	    content: des,
	    padding: 0,
	    quickClose: true// 点击空白处快速关闭
	});
	d.show(obj);
};


