function UserBankListEditPage(){}
var userBankListEditPage = new UserBankListEditPage();

userBankListEditPage.param = {
   	
};

$(document).ready(function() {
	var bizsystemId = userBankListEditPage.param.id;  //获取查询ID
	
	//自定义单击事件
	addSelectEvent();
	_init_area();
	
	$("#province").focus(function(){
		$("#cityTip").removeClass("red");
		$("#cityTip").hide();
	});

	$("#province").blur(function(){
		var province = $("#s_province").val();
		if(province==null || $.trim(province)==""){
			$("#cityTip").css({display:"inline"});
			$("#cityTip").addClass("red");
			$("#cityTip").html("<i></i>请选择省份");
		}
		if($.trim(province)=="省份"){
			$("#cityTip").css({display:"inline"});
			$("#cityTip").addClass("red");
			$("#cityTip").html("<i></i>请选择省份");
		}
	});
	
	$("#city").focus(function(){
		$("#cityTip").removeClass("red");
		$("#cityTip").hide();
	});
	
	$("#city").blur(function(){
		var city = $("#city").val();
		if(city==null || $.trim(city)==""){
			$("#cityTip").css({display:"inline"});
			$("#cityTip").addClass("red");
			$("#cityTip").html("<i></i>请选择地级市");
		}
		if($.trim(city)=="地级市"){
			$("#cityTip").css({display:"inline"});
			$("#cityTip").addClass("red");
			$("#cityTip").html("<i></i>请选择地级市");
		}
	});
});

function selectShow(val){
	$("#"+val+"").show()
}

/**
 * 保存更新信息
 */
UserBankListEditPage.prototype.saveData = function(){
	
	$("#subBankBtn").unbind("click");
	$("#subBankBtn").val("提交中.....");
	
	var bankAccountName = $("#bankAccountName").val();
	var bankType = $("#bankType").val();
	var bankName = $("#bankType").find("option:selected").text();
	/*var subbranchName = $("#subbranchName").val();*/
	var bankCardNum = $("#bankCardNum").val();
	var province = $("#province").val();
	var city = $("#city").val();
	var userId = $("#userId").val();
	var userName = $("#userName").val();
	var bizSystem = $("#bizSystem").val();
	/*var area = $("#county").val();*/
	
	
	if(bankAccountName==null || $.trim(bankAccountName)==""){
		$("#bankBankTip").css({display:"inline"});
		$("#bankBankTip").addClass("red");
		bankCardListPage.bindClick();
		$("#subBankBtn").val("提交");
		return;
	}
	
	
	
	
	
	if(bankCardNum==null || $.trim(bankCardNum)==""){
		swal("请填写银行卡号!");
		return;
	}
	var reg = new RegExp("^[0-9]*$");
	if(bankCardNum.length < 15 || bankCardNum.length > 20 || !reg.test(bankCardNum)){
		swal("银行卡号由15-19位数字组成!");
		return;
	}
	
	
	if(province==null || $.trim(province)=="" || city==null || $.trim(city)==""){
		swal("请选择开户所在城市!");
		return;
	}
	if($.trim(province)=="省份" || $.trim(city)=="地级市"){
		swal("请选择开户所在城市!")
		return;
	}
	
	
	var userBank = {};
	userBank.bankAccountName =bankAccountName;
	userBank.bankName = bankName;
	userBank.bankType = bankType;
	userBank.bankCardNum = bankCardNum;
	userBank.province = province;
	userBank.city = city;
	userBank.userId = userId;
	userBank.userName = userName;
	userBank.bizSystem=bizSystem;
	managerUserAction.addUserBanks(userBank,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.userBankListPage.closeLayer(index);
		   	    }); 
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("保存数据失败");
			
		}
    });
};

UserBankListEditPage.prototype.closeLayer=function(index){
	layer.close(index);
	domainPage.table.ajax.reload();
};

