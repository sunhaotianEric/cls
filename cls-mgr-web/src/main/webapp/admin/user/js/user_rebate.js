function UserRebatePage(){}
var userRebatePage= new UserRebatePage();

userRebatePage.param = {
		
};

/**
 * 查询参数
 */
userRebatePage.queryParam = {
		queryByUserName : null
};

//分页参数
userRebatePage.pageParam = {
        queryMethodParam : null,
        pageNo : 1,
        pageMaxNo : 1,  //最大的页数
        skipHtmlStr : ""
};

$(document).ready(function() {
	userRebatePage.getAllUsers(); //查询所有的用户数据
});

/**
 * 加载所有的用户数据
 */
UserRebatePage.prototype.getAllUsers = function(){
	userRebatePage.pageParam.queryMethodParam = 'getAllUsers';
	userRebatePage.queryParam = {};
	userRebatePage.queryConditionUsers(userRebatePage.queryParam,userRebatePage.pageParam.pageNo);
};

/**
 * 模糊用户名查找
 */
UserRebatePage.prototype.getAllUsersByLikeName = function(){
	userRebatePage.pageParam.queryMethodParam = 'getAllUsersByLikeName';
    var userName = $("#userName").val();
    if(userName == ""){
    	userName = null;
    }
	userRebatePage.queryParam = {};
	userRebatePage.queryParam.username = userName; 
	userRebatePage.queryConditionUsers(userRebatePage.queryParam,userRebatePage.pageParam.pageNo);
};

/**
 * 修改用户的返点信息
 */
UserRebatePage.prototype.updateUserRebate = function(obj,userId){
	var sscRebate = $("select[name='sscRebate']",$(obj).parent().parent()).val();
	var syxwRebate = $("select[name='syxwRebate']",$(obj).parent().parent()).val();
	var ksRebate = $("select[name='ksRebate']",$(obj).parent().parent()).val();
	var klsfRebate = $("select[name='klsfRebate']",$(obj).parent().parent()).val();
	var dpcRebate = $("select[name='dpcRebate']",$(obj).parent().parent()).val();
	
	var updateParam = {};
	updateParam.id = userId;
	updateParam.sscrebate = sscRebate;
	updateParam.syxwrebate = syxwRebate;
	updateParam.ksrebate = ksRebate;
	updateParam.klsfrebate = klsfRebate;
	updateParam.dpcrebate = dpcRebate;
	
	managerUserAction.userRebateSet(updateParam,function(r){
		if (r[0] != null && r[0] == "ok") {
			userRebatePage.pageQuery(1);  //更新数据
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询用户数据请求失败.");
		}
    });
};

/**
 * 查询符合条件的用户
 */
UserRebatePage.prototype.queryConditionUsers = function(queryParam,pageNo){
	managerUserAction.userList(queryParam,pageNo,function(r){
		if (r[0] != null && r[0] == "ok") {
			userRebatePage.refreshUserRebatePage(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询用户数据请求失败.");
		}
    });
};

/**
 * 根据当前点击的方法,查找对应页
 */
UserRebatePage.prototype.pageQuery = function(pageNo){
	if(pageNo < 1){
		userRebatePage.pageParam.pageNo = 1;
	}else if(pageNo > userRebatePage.pageParam.pageMaxNo){
		userRebatePage.pageParam.pageNo = userRebatePage.pageParam.pageMaxNo;
	}else{
		userRebatePage.pageParam.pageNo = pageNo;
	}
	//如果页码小于等于0，则不进行分页查找
	if(userRebatePage.pageParam.pageNo <= 0){
		return;
	}
	
	if(userRebatePage.pageParam.queryMethodParam == 'getAllUsersByLikeName'){
		userRebatePage.getAllUsersByLikeName();
	}else if(userRebatePage.pageParam.queryMethodParam == 'getAllUsers'){
		userRebatePage.getAllUsers();
	}else{
		alert("类型对应的查找方法未找到.");
	}
	
};

/**
 * 展示用户列表数据
 */
UserRebatePage.prototype.refreshUserRebatePage = function(page){
	var userRebateListObj = $("#userRebateList");
	
	userRebateListObj.html("");
	var str = "";
    var users = page.pageContent;
    
    //记录数据显示
	for(var i = 0 ; i < users.length; i++){
		var user = users[i];
		str += "<tr>";
		str += "  <td>"+ user.username +"</td>";
		if(user.daili == 0){
			str += "  <td>普通会员</td>";
		}else{
			str += "  <td>代理会员</td>";
		}
		str += "<td>" + sscRebateSelectStrForJs + " </td>";
		str += "<td>" + syxwRebateSelectStrForJs + " </td>";
		str += "<td>" + ksRebateSelectStrForJs + " </td>";
		str += "<td>" + klsfRebateSelectStrForJs + " </td>";
		str += "<td>" + dpcRebateSelectStrForJs + " </td>";
		str += "  <td>";
	    str += "    <input class='btn btn-mini btnedit'  type='button' value='修改'  onclick='userRebatePage.updateUserRebate(this,"+user.id+")'/>";
	    str += "  </td>";
		str += "</tr>";
		userRebateListObj.append(str);
		
		$("select[name='sscRebate']:last",userRebateListObj).find("option[value='"+user.sscrebate+"']").attr("selected",true);
		$("select[name='syxwRebate']:last",userRebateListObj).find("option[value='"+user.syxwrebate+"']").attr("selected",true);
		$("select[name='ksRebate']:last",userRebateListObj).find("option[value='"+user.ksrebate+"']").attr("selected",true);
		$("select[name='klsfRebate']:last",userRebateListObj).find("option[value='"+user.klsfrebate+"']").attr("selected",true);
		$("select[name='dpcRebate']:last",userRebateListObj).find("option[value='"+user.dpcrebate+"']").attr("selected",true);

		str = "";
	}
	
	//记录统计
	var pageMsg = $("#pageMsg");
	var pageStr = "";
	pageStr += "<th colspan='100'>";
	pageStr += "  当前第" + page.pageNo + "页/共" + page.totalPageNum +"页&nbsp;&nbsp;共" + page.totalRecNum +"条记录&nbsp;&nbsp;<a href='javascript:void(0)'  onclick='userRebatePage.pageQuery(1)'>首页</a>&nbsp;";
	pageStr += "<a href='javascript:void(0)' onclick='userRebatePage.pageParam.pageNo--;userRebatePage.pageQuery(userRebatePage.pageParam.pageNo)'>上一页</a>&nbsp;";
	pageStr += "<a href='javascript:void(0)' onclick='userRebatePage.pageParam.pageNo++;userRebatePage.pageQuery(userRebatePage.pageParam.pageNo)'>下一页</a>&nbsp;";
	pageStr += "<a href='javascript:void(0)' onclick='userRebatePage.pageQuery("+ page.totalPageNum +")'>尾页</a>";
	
	var skipHtmlStr = "";
	if($("#pageSkip").html() != null){
		$("#pageSkip").remove();
	}
    skipHtmlStr += "<span id='pageSkip'>";
    skipHtmlStr += "&nbsp;&nbsp;跳转至";
	skipHtmlStr += "<select id='currentPageChoose' class='ipt' onchange='userRebatePage.pageParam.pageNo = this.value;userRebatePage.pageQuery(userRebatePage.pageParam.pageNo)'  style='width:auto;'>";
	for(var i = 1; i <= page.totalPageNum; i++){
		skipHtmlStr += "<option value='"+ i +"'>"+ i +"</option>";
	}
	skipHtmlStr += "</select>";
	skipHtmlStr += "</span>";	
	pageStr += skipHtmlStr;
	pageStr += "</th>";
	pageMsg.html(pageStr);
	
	$("#currentPageChoose").find("option[value='"+ page.pageNo +"']").attr("selected",true);  //显示当前页码
	userRebatePage.pageParam.pageMaxNo = page.totalPageNum;  //标识最大的页数
};