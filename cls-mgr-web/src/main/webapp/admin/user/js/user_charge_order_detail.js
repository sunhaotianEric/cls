function UserChargeDrawOrderDetailPage(){}
var userChargeDrawOrderDetailPage = new UserChargeDrawOrderDetailPage();

userMoneyDetail.param = {
};

//分页参数
userMoneyDetail.pageParam = {
        queryMethodParam : null,
        pageNo : 1,
        pageMaxNo : 1,  //最大的页数
        skipHtmlStr : ""
};

/**
 * 查询参数
 */
userMoneyDetail.queryParam = {
		detailType : null,
		lotteryId : null,
		expect : null,
		userName : null,
		createdDateStart : null,
		createdDateEnd : null
};

$(document).ready(function() {
	
	$("#createdDateStart").val(new Date().AddDays(-1).format("yyyy-MM-dd"));
	$("#createdDateEnd").val(new Date().AddDays(-1).format("yyyy-MM-dd"));
	
	userMoneyDetail.getAllMoneyDetailsForQuery(); //根据条件,查找资金明细
});
	
/**
 * 加载所有的登陆日志数据
 */
UserChargeDrawOrderDetailPage.prototype.getAllMoneyDetailsForQuery = function(){
	userMoneyDetail.pageParam.queryMethodParam = 'getAllMoneyDetailsForQuery';
	userMoneyDetail.queryParam = {};
	
	var moneyDetailTypes = $("#moneyDetailTypes").val();
	var expect = $("#expect").val();
	var userName = $("#userName").val();
	var lotteryId = $("#lotteryId").val();
	var createdDateStart = $("#createdDateStart").val();
	var createdDateEnd = $("#createdDateEnd").val();
	
	if(moneyDetailTypes == ""){
		userMoneyDetail.queryParam.detailType = null;
	}else{
		userMoneyDetail.queryParam.detailType = moneyDetailTypes;
	}
	
	if(expect == ""){
		userMoneyDetail.queryParam.expect = null;
	}else{
		userMoneyDetail.queryParam.expect = expect;
	}
	
	if(lotteryId == ""){
		userMoneyDetail.queryParam.lotteryId = null;
	}else{
		userMoneyDetail.queryParam.lotteryId = lotteryId;
	}
	
	if(userName == ""){
		userMoneyDetail.queryParam.userName = null;
	}else{
		userMoneyDetail.queryParam.userName = userName;
	}
	
	if(createdDateStart == ""){
		userMoneyDetail.queryParam.createdDateStart = null;
	}else{
		userMoneyDetail.queryParam.createdDateStart = new Date(createdDateStart);
	}
	if(createdDateEnd == ""){
		userMoneyDetail.queryParam.createdDateEnd = null;
	}else{
		userMoneyDetail.queryParam.createdDateEnd = new Date(createdDateEnd);
	}
	
	userMoneyDetail.queryConditionMoneyDetails(userMoneyDetail.queryParam,userMoneyDetail.pageParam.pageNo);
};

/**
 * 查找资金明细
 */
UserChargeDrawOrderDetailPage.prototype.queryConditionMoneyDetails = function(queryParam,pageNo){
	managerMoneyDetailAction.getAllMoneyDetailsByQuery(queryParam,pageNo,function(r){
		if (r[0] != null && r[0] == "ok") {
			userMoneyDetail.refreshMoneyDetailPages(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询所有的资金类型失败.");
		}
    });
};

/**
 * 刷新列表数据
 */
UserChargeDrawOrderDetailPage.prototype.refreshMoneyDetailPages = function(page){
	var moneyDetailsListObj = $("#moneyDetailList");
	
	moneyDetailsListObj.html("");
	var str = "";
    var moneyDetails = page.pageContent;
    
    //记录数据显示
	for(var i = 0 ; i < moneyDetails.length; i++){
		var moneyDetail = moneyDetails[i];
		str += "<tr>";
		str += "  <td>"+ moneyDetail.lotteryId +"</td>";
		str += "  <td>"+ moneyDetail.detailTypeDes +"</td>";
		str += "  <td><a href='javascript:void(0)' style='color:blue;text-decoration:underline;'  onclick='commonPage.showUserMsgPage(this,"+moneyDetail.userId+")'>"+ moneyDetail.userName +"</a></td>";
		if(moneyDetail.income == 0){ //收入为0的情况
			str += "  <td>-"+ moneyDetail.pay.toFixed(commonPage.param.fixVaue) +"</td>";
		}else{
			str += "  <td>"+ moneyDetail.income.toFixed(commonPage.param.fixVaue) +"</td>";
		}
		str += "  <td>"+ moneyDetail.balanceAfter +"</td>";
		str += "  <td>"+ moneyDetail.createDateStr +"</td>";
		str += "  <td><a style='color: blue;text-decoration: underline;' href='javascript:void(0)' onclick='userMoneyDetail.showMoneyDetailDes(this,\""+moneyDetail.description+"\")'>"+ moneyDetail.detailTypeDes +"</a></td>";
		str += "</tr>";
		moneyDetailsListObj.append(str);
		str = "";
	}
	
	//记录统计
	var pageMsg = $("#pageMsg");
	var pageStr = "";
	pageStr += "<th colspan='100'>";
	pageStr += "  当前第" + page.pageNo + "页/共" + page.totalPageNum +"页&nbsp;&nbsp;共" + page.totalRecNum +"条记录&nbsp;&nbsp;<a href='javascript:void(0)'  onclick='userMoneyDetail.pageQuery(1)'>首页</a>&nbsp;";
	pageStr += "<a href='javascript:void(0)' onclick='userMoneyDetail.pageParam.pageNo--;userMoneyDetail.pageQuery(userMoneyDetail.pageParam.pageNo)'>上一页</a>&nbsp;";
	pageStr += "<a href='javascript:void(0)' onclick='userMoneyDetail.pageParam.pageNo++;userMoneyDetail.pageQuery(userMoneyDetail.pageParam.pageNo)'>下一页</a>&nbsp;";
	pageStr += "<a href='javascript:void(0)' onclick='userMoneyDetail.pageQuery("+ page.totalPageNum +")'>尾页</a>";
	
	var skipHtmlStr = "";
	if($("#pageSkip").html() != null){
		$("#pageSkip").remove();
	}
    skipHtmlStr += "<span id='pageSkip'>";
    skipHtmlStr += "&nbsp;&nbsp;跳转至";
	skipHtmlStr += "<select id='currentPageChoose' class='ipt' onchange='userMoneyDetail.pageParam.pageNo = this.value;userMoneyDetail.pageQuery(userMoneyDetail.pageParam.pageNo)'  style='width:auto;'>";
	for(var i = 1; i <= page.totalPageNum; i++){
		skipHtmlStr += "<option value='"+ i +"'>"+ i +"</option>";
	}
	skipHtmlStr += "</select>";
	skipHtmlStr += "</span>";	
	pageStr += skipHtmlStr;
	pageStr += "</th>";
	pageMsg.html(pageStr);
	
	$("#currentPageChoose").find("option[value='"+ page.pageNo +"']").attr("selected",true);  //显示当前页码
	userMoneyDetail.pageParam.pageMaxNo = page.totalPageNum;  //标识最大的页数
};

/**
 * 根据条件查找对应页
 */
UserChargeDrawOrderDetailPage.prototype.pageQuery = function(pageNo){
	if(pageNo < 1){
		userMoneyDetail.pageParam.pageNo = 1;
	}else if(pageNo > userMoneyDetail.pageParam.pageMaxNo){
		userMoneyDetail.pageParam.pageNo = userMoneyDetail.pageParam.pageMaxNo;
	}else{
		userMoneyDetail.pageParam.pageNo = pageNo;
	}
	//如果页码小于等于0，则不进行分页查找
	if(userMoneyDetail.pageParam.pageNo <= 0){
		return;
	}
	
	if(userMoneyDetail.pageParam.queryMethodParam == 'getAllMoneyDetailsForQuery'){
		userMoneyDetail.getAllMoneyDetailsForQuery();
	}else{
		alert("类型对应的查找方法未找到.");
	}
};