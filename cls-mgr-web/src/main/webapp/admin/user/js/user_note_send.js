function UserNoteSendPage(){}
var userNoteSendPage = new UserNoteSendPage();

userNoteSendPage.param = {
	sendType : null,
	starLevel : null,
	dailiLevel : null,
	sscrebate : null,
	userName : null,
	filterCondition : null,
	sub : null,
	body : null
};

$(document).ready(function() {
	$("#resetBtn").click(function(){
		$("#sscrebate").val("");
		$("#sub").val("");
		$("#body").val("");
	});
});


/**
 * 保存帮助信息
 */
UserNoteSendPage.prototype.userNoteSend = function(){
	userNoteSendPage.param.sendType = $("input[name='sendType']:checked").val();
	userNoteSendPage.param.starLevel = getSearchVal("starLevel");
	userNoteSendPage.param.dailiLevel = getSearchVal("dailiLevel");
	userNoteSendPage.param.sscrebate = getSearchVal("sscrebate");
	userNoteSendPage.param.userName = getSearchVal("userName");
	userNoteSendPage.param.filterCondition = getSearchVal("filterCondition");
	userNoteSendPage.param.sub = getSearchVal("sub");
	userNoteSendPage.param.body = getSearchVal("body");
	userNoteSendPage.param.bizSystem = getSearchVal("bizSystem");
	if(userNoteSendPage.param.sendType ==null ){
		swal("请选择发送对象!");
		return;
	} else {
		if(userNoteSendPage.param.sendType == "1" && userNoteSendPage.param.starLevel == null) {
			swal("请选择用户星级!");
			return;
		}
		if(userNoteSendPage.param.sendType == "2" && userNoteSendPage.param.dailiLevel == null) {
			swal("请选择代理级别!");
			return;
		}
		if(userNoteSendPage.param.sendType == "3" && userNoteSendPage.param.sscrebate == null) {
			swal("请输入时时彩模式!");
			return;
		}
		if(userNoteSendPage.param.sendType == "4" && userNoteSendPage.param.userName == null) {
			swal("请输入用户名!");
			return;
		}
		if(currentUser.bizSystem=='SUPER_SYSTEM'){
			if(userNoteSendPage.param.sendType == "4" && userNoteSendPage.param.bizSystem == null) {
				swal("选择用户名发送需要选择业务系统！");
				return;
			}
		}
	}
	if(userNoteSendPage.param.sub == null || userNoteSendPage.param.sub.trim()==""){
		swal("请输入站内信主题!");
		$("#sub").focus();
		return;
	}
	if(userNoteSendPage.param.body ==null || userNoteSendPage.param.body.trim()==""){
		swal("请输入内容!");
		$("#body").focus();
		return;
	}
	
	managerNotesAction.userNoteSend(userNoteSendPage.param,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({
		           title: "",
		           text: "成功向"+r[1]+"个用户发送了站内信",
		           type: "success"
		       },
		       function() {
		    	   document.getElementById("sendform").reset();
		    	   $(".i-checks").iCheck({
	                    checkboxClass: "icheckbox_square-green",
	                    radioClass: "iradio_square-green",
	                });
		    	  
		       });
			
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("发送站内信信息失败.");
		}
    });
};

