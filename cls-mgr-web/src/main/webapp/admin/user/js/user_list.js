function UserListPage(){}
var userList= new UserListPage();

userList.param = {
		
};

/**
 * 查询参数
 */
userList.queryParam = {
		bizSystem : null,
		username : null,
		queryByUserName : null,
		queryByMoney : null,
		queryBySscRebate : null,
		queryByAgent : null,
		queryByRegisterDate : null,  //按注册时间排列
		queryByLogtime : null,
		queryByLogins : null, //按登录次数
		queryByLock : null, //按锁定会员
		queryByLock : null,  //按用户星级
		isTourist : 0,	//默认用户非游客
		queryByWithdrawLock:null,	//按用户提现锁定
		queryByVipLevel:null
		
};

//分页参数
userList.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize
};

$(document).ready(function() {

	userList.initTableData(); //默认查询所有的用户 
	
	userList.getUsersCount("");  //查询用户在线人数数据
	
	 //回车提交事件
     $("body").keydown(function() {
         if (event.keyCode == "13") {//keyCode=13是回车键
        	 userList.getAllUsersByCondition()
         }
     });
});

/**
 * 查询用户在线人数数据
 */
UserListPage.prototype.getUsersCount = function(bizSystem){
	$("#allUserCount").text("加载中");
	$("#mobileUserCount").text("加载中");
	if(bizSystem == undefined){
		bizSystem = userList.queryParam.bizSystem;
	}
	managerUserAction.getUsersCount(bizSystem,function(r){
		if (r[0] != null && r[0] == "ok") {
			$("#allUserCount").text(r[1]);
			$("#mobileUserCount").text(r[2]);
			$("#appUserCount").text(r[3]);
		}else if(r[0] != null && r[0] == "error"){
			$("#allUserCount").text("加载失败");
			$("#mobileUserCount").text("加载失败");
			$("#appUserCount").text("加载失败");
			swal(r[1]);
		}else{
			$("#allUserCount").text("加载失败");
			$("#mobileUserCount").text("加载失败");
			$("#appUserCount").text("加载失败");
			swal("查询用户在线人数数据请求失败.");
		}
    });
};

/**
 * 模糊用户名查找
 */
UserListPage.prototype.getAllUsersByCondition = function(){
	userList.queryParam = {};
    var userName = $("#userName").val();
    var fieldsorting = $("#fieldsorting").val();
    var bizSystem = $("#bizSystem").val();
    var trueName=$("#trueName").val();
    var phone=$("#phone").val();
    var qq=$("#qq").val();
	    if(userName == ""){
	    	userName = null;
	    }
	    if(bizSystem == ""){
	    	bizSystem = null;
	    }
	    
	if(isTouristParameter == 1){
		userList.queryParam.isTourist = isTouristParameter;
		//$(".ibox-title").hide();
	}else{
		userList.queryParam.isTourist = 0;
	}
		
	  if(fieldsorting=='queryByLogtime'){
		  userList.queryParam.queryByLogtime = 1; 
	  }else if(fieldsorting=='queryByUserName'){
		  userList.queryParam.queryByUserName = 1;  
	  }else if(fieldsorting=='queryByMoney'){
		  userList.queryParam.queryByMoney = 1; 
	  }else if(fieldsorting=='queryBySscRebate'){
		  userList.queryParam.queryBySscRebate = 1; 
	  }else if(fieldsorting=='queryByAgent'){
		  userList.queryParam.queryByAgent = 1; 
	  }else if(fieldsorting=='queryByRegisterDate'){
		  userList.queryParam.queryByRegisterDate = 1; 
	  }else if(fieldsorting=='queryByLogins'){
		  userList.queryParam.queryByLogins = 1; 
	  }else if(fieldsorting=='queryByLock'){
		  userList.queryParam.queryByLock = 1; 
	  }else if(fieldsorting=='queryByStarLevel'){
		  userList.queryParam.queryByStarLevel = 1; 
	  }else if(fieldsorting=='queryByWithdrawLock'){
		  userList.queryParam.queryByWithdrawLock = 1; 
	  }else if(fieldsorting=='queryByVipLevel'){
		  userList.queryParam.queryByVipLevel = 1; 
	  }
	userList.queryParam.username = userName;
	userList.queryParam.bizSystem = bizSystem; 
	userList.queryParam.state = 1;
	userList.getUsersCount(bizSystem);
	userList.queryParam.trueName=trueName;
	userList.queryParam.phone=phone;
	userList.queryParam.qq=qq;
	userList.table.ajax.reload();
};

/**
 * 查询符合条件的用户
 */
UserListPage.prototype.queryConditionUsers = function(queryParam,pageNo){
	managerUserAction.userList(queryParam,pageNo,function(r){
		if (r[0] != null && r[0] == "ok") {
			userList.refreshUsersPages(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("查询用户数据请求失败.");
		}
    });
};


/**
 * 初始化列表数据
 */
UserListPage.prototype.initTableData = function() {
	userList.queryParam = getFormObj($("#queryForm"));
	if(isTouristParameter == 1){
		userList.queryParam.isTourist = isTouristParameter;
		//$(".ibox-title").hide();
	}else{
		userList.queryParam.isTourist = 0;
	}
	userList.table = $('#userlistTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"scrollX": true,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
//		"scrollCollapse": true,
//		"fixedColumns":{
//			"leftColumns":2,"rightColumns": 1
//		},
		"ajax":function (data, callback, settings) {
			userList.pageParam.pageSize = data.length;//页面显示记录条数
			userList.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerUserAction.userList(userList.queryParam,userList.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": null},
		            {"data": "bizSystemName","bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {
		            	"data": "userName",
		            	render: function(data, type, row, meta) {	
		            		return "<a href='javascript:void(0)' style='color:blue;text-decoration:underline;'  onclick='commonPage.showUserMsgPage(\""+data+"\","+row.id+")'>"+ row.userName +"</a>";
	            		}
		            },
		            
		           
		            {
		            	"data": "money",
		            	render: function(data, type, row, meta) {		           
		            		return "  <font style='color:#ff0000'>"+ row.money.toFixed(3) + "</font><font style='color:green'>/" + row.canWithdrawMoney.toFixed(3) + "</font>";
	            		}
		            },
		            {"data": "sscRebate"},
		            {"data": "dailiLevelStr",
		            	render: function(data, type, row, meta) {		           
		            		return "<a>"+data+"</a>";
	            		}
		            },
		            {"data": "vipLevel",
		            	render: function(data, type, row, meta) {		           
		            		return "VIP"+data;
	            		}
		            },
		            {   
		            	"data": "starLevel",
		            	render: function(data, type, row, meta) {
		            		var starLevel = data;
		            		var oneStarBright = false;
		            		var twoStarBright = false;
		            		var threeStarBright = false;
		            		var fourStarBright = false;
		            		var fiveStarBright = false;
		    
		            		if(starLevel != null) {
		            			if(starLevel == 1) {
		            				oneStarBright = true;
		            			} else if(starLevel == 2) {
		            				oneStarBright = true;
		            				twoStarBright = true;
		            			} else if(starLevel == 3) {
		            				oneStarBright = true;
		            				twoStarBright = true;
		            				threeStarBright = true;
		            			} else if(starLevel == 4) {
		            				oneStarBright = true;
		            				twoStarBright = true;
		            				threeStarBright = true;
		            				fourStarBright = true;
		            			} else if(starLevel == 5) {
		            				oneStarBright = true;
		            				twoStarBright = true;
		            				threeStarBright = true;
		            				fourStarBright = true;
		            				fiveStarBright = true;
		            			}
		            		}
		            		var str= "<ul class='jsstar'>";
		            		if(oneStarBright) {
		            			str += "		<li title='一星' data-val='1' style='background-position:left bottom'></li>"; 
		            		} else {
		            			str += "		<li title='一星' data-val='1'></li>"; 
		            		}
		            		if(twoStarBright) {
		            			str += "		<li title='二星' data-val='2' style='background-position:left bottom'></li>";
		            		} else {
		            			str += "		<li title='二星' data-val='2'></li>";
		            		}
		            		if(threeStarBright) {
		            			str += "		<li title='三星' data-val='3' style='background-position:left bottom'></li>";
		            		} else {
		            			str += "		<li title='三星' data-val='3'></li>";
		            		}
		            		if(fourStarBright) {
		            			str += "		<li title='四星' data-val='4' style='background-position:left bottom'></li>";
		            		} else {
		            			str += "		<li title='四星' data-val='4'></li>";
		            		}
		            		if(fiveStarBright) {
		            			str += "		<li title='五星' data-val='5' style='background-position:left bottom'></li>";
		            		} else {
		            			str += "		<li title='五星' data-val='5'></li>";
		            		}
		            		str += "</ul>";
		            		return str;
	            		}
		            },
		            {"data": "loginTimes"},
		            {
		            	"data": "loginLock",
		            	render: function(data, type, row, meta) {
		            		if(row.loginLock != 0 && row.withdrawLock !=0){
		            			return '<font color="red">登陆提现锁定</font>';
		            		}else if(row.loginLock != 0){
		            			return '<font color="red">登陆锁定</font>';
		            		}else if(row.withdrawLock != 0){
		            			return '<font color="red">提现锁定</font>';
		            		}else{
		            			return '正常';
		            		}
		            		return data;
	            		}
		            },
		            {
		            	"data": "isOnline",
		            	render: function(data, type, row, meta) {
		            		if (data != null && data == 1) {
		            			return '是';
		            		} else {
		            			return '否';
		            		}
		            		return data;
	            		}
		            },
		            {
	                	 "data": "id",
	                	 render: function(data, type, row, meta) {
	                		 var str="";
	                			if(currentUser.state == 0||currentUser.state == 1||currentUser.state == 2){
	                		           str+="<a title='团队' class='btn btn-danger btn-sm btn-td'  onclick=\"userList.showTeams('"+ row.userName +"00->"+row.bizSystem+"')\"><i class='fa fa-group'></i></a>&nbsp;";
	                		           str+="<a title='续费' class='btn btn-warning btn-sm btn-xf' onclick=\"userList.showUserRenew('"+row.id+"')\"><i class='fa fa-money'></i></a>&nbsp;"; 
	                                   str+="<a title='编辑' class='btn btn-info btn-sm btn-mx' onclick='userList.showUserEdit(\""+row.id+"\",\""+ row.isTourist +"\")'><i class='fa fa-pencil'></i></a>&nbsp;";
	                				if(row.loginLock == 0){
	                					str+="<a title='登陆锁定' class='btn btn-success btn-sm btn-sd' onclick='userList.lockUser("+ row.id +")'><i class='fa fa-lock'></i></a>&nbsp;";
	          
	                				}else{
	                					str+="<a title='登陆解锁' class='btn btn-success btn-sm btn-sd' onclick='userList.unlockUser("+ row.id +")'><i class='fa fa-unlock'></i></a>&nbsp;";
	                				}
	                				if(row.withdrawLock == 0){
	                					str+="<a title='提现锁定' class='btn btn-warning btn-sm btn-xf' onclick='userList.lockWithdraw("+ row.id +")'><i class='fa fa-lock'></i></a>&nbsp;";
	          
	                				}else{
	                					str+="<a title='提现解锁' class='btn btn-warning btn-sm btn-xf' onclick='userList.unlockWithdraw("+ row.id +")'><i class='fa fa-unlock'></i></a>&nbsp;";
	                				}
	                				str+="<a title='初始化密码' class='btn btn-info btn-sm btn-pwd' onclick='userList.resetUserPass("+ row.id +")'><i class='fa fa-key'></i></a>&nbsp;";
	                				str+="<a title='初始化安全密码' class='btn btn-danger btn-sm btn-aqpwd' onclick='userList.resetUserPass1("+ row.id +")'><i class='fa fa-key'></i></a>&nbsp;";
	                			}else if(currentUser.state == 3){
	                		           str+="<a title='团队' class='btn btn-danger btn-sm btn-td'  onclick=\"userList.showTeams('"+ row.userName +"00->"+row.bizSystem+"')\"><i class='fa fa-group'></i></a>&nbsp;";
	
	                                   str+="<a title='编辑' class='btn btn-info btn-sm btn-mx' onclick='userList.showUserEdit(\""+row.id+"\",\""+ row.isTourist+"\")'><i class='fa fa-pencil'></i></a>&nbsp;";
	                				if(row.loginLock == 0){
	                					str+="<a title='登陆锁定' class='btn btn-success btn-sm btn-sd' onclick='userList.lockUser("+ row.id +")'><i class='fa fa-lock'></i></a>&nbsp;";
	          
	                				}else{
	                					str+="<a title='登陆解锁' class='btn btn-success btn-sm btn-sd' onclick='userList.unlockUser("+ row.id +")'><i class='fa fa-unlock'></i></a>&nbsp;";
	                				}
	                				if(row.withdrawLock == 0){
	                					str+="<a title='提现锁定' class='btn btn-warning btn-sm btn-xf' onclick='userList.lockWithdraw("+ row.id +")'><i class='fa fa-lock'></i></a>&nbsp;";
	          
	                				}else{
	                					str+="<a title='提现解锁' class='btn btn-warning btn-sm btn-xf' onclick='userList.unlockWithdraw("+ row.id +")'><i class='fa fa-unlock'></i></a>&nbsp;";
	                				}
	                				str+="<a title='初始化密码' class='btn btn-info btn-sm btn-pwd' onclick='userList.resetUserPass("+ row.id +")'><i class='fa fa-key'></i></a>&nbsp;";
	                				str+="<a title='初始化安全密码' class='btn btn-danger btn-sm btn-aqpwd' onclick='userList.resetUserPass1("+ row.id +")'><i class='fa fa-key'></i></a>&nbsp;";
	                				
	                			}
	                			str += "<a title='强制退出' class='btn btn-warning btn-sm btn-qz' onclick='userList.logoutUser(\""+row.bizSystem+"\",\""+ row.userName +"\")'><i class='fa fa-reply-all'></i></a>&nbsp;";
	                			if(isTouristParameter == 1){
	                				str+="<a title='删除' class='btn btn-danger btn-sm btn-del' onclick='userList.delTouristUser("+ row.id +")'><i class='fa fa-trash'></i></a>&nbsp;";
	                			}
	                			return str;
	                	 }
	                }
	            ]
	}).api(); 
	
 //new $.fn.dataTable.FixedColumns(userList.table,{"leftColumns":2 });
}

/**
 * 强制退出会员
 */
UserListPage.prototype.logoutUser = function(bizSystem,userName){
	 swal({
         title: "确认要退出"+userName+"会员？",
         text: "请谨慎操作！",
         type: "warning",
         showCancelButton: true,
         confirmButtonColor: "#DD6B55",
         cancelButtonText: "取消",
         confirmButtonText: "确定",
         closeOnConfirm: false
       },
     function() {
    	   managerUserAction.logoutUser(bizSystem,userName,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "操作成功",type: "success"});
				adminPage.getAllAdmins();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("退出该管理员失败.");
			}
	    });
	});
};

/**
 * 显示团队
 */
UserListPage.prototype.showTeams = function(userName){
	  layer.open({
          type: 2,
          title: '团队列表',
          maxmin: false,
          shadeClose: true,
          //点击遮罩关闭层
          area: ['800px', '520px'],
         // area: ['70%', '50%'],
          content: contextPath + "/managerzaizst/teamreport/"+userName+"/teamreport.html",
          cancel: function(index){ 
        	  userList.getAllUsersByCondition();
       	   }
      });
}


/**
 * 续费
 */
UserListPage.prototype.showUserRenew= function(id)
{
		layer.open({
	          type: 2,
	          title: '续费',
	          maxmin: false,
	          shadeClose: true,
	          //点击遮罩关闭层
	          area: ['600px', '510px'],
	        //  area: ['55%', '45%'],
	          content: contextPath+"/managerzaizst/user/"+id+"/user_renew.html",
	          cancel: function(index){ 
	        	  userList.getAllUsersByCondition();
	       	   }
	      });
}

/**
 * 用户添加
 */
UserListPage.prototype.showUserAdd= function(id)
{
	  layer.open({
          type: 2,
          title: '直属会员添加',
          maxmin: false,
          shadeClose: true,
          //点击遮罩关闭层
          area: ['800px', '320px'],
         // area: ['60%', '33%'],
          content: contextPath+"/managerzaizst/user/"+id+"/user_add.html",
          cancel: function(index){ 
        	  userList.getAllUsersByCondition();
       	   }
      });
}
/**
 * 用户编辑
 */
UserListPage.prototype.showUserEdit= function(id,isTourist)
{
	  layer.open({
          type: 2,
          title: '用户编辑',
          maxmin: false,
          shadeClose: true,
          //点击遮罩关闭层
          area: ['800px', '460px'],
          //area: ['60%', '60%'],
          content: contextPath+"/managerzaizst/user/"+id+"/user_edit.html?isTourist="+isTourist+"",
          cancel: function(index){ 
        	  userList.getAllUsersByCondition();
       	   }
      });
}



/**
 * 删除用户(state = 0)
 */
UserListPage.prototype.deleteUser = function(userId){
	if(userId == null || userId == ""){
		swal("参数传递错误.");
		return;
	}
	
	   swal({
           title: "您确定要删除这条信息吗",
           text: "请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "删除",
           closeOnConfirm: false
       },
       function() {
		managerUserAction.deleteUser(userId,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "删除成功",type: "success"});
				userList.getAllUsersByCondition();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("删除信息失败.");
			}
	    });		
	});
};

/**
 * 删除游客用户(isTourist = 1)
 */
UserListPage.prototype.delTouristUser = function(userId){
	if(userId == null || userId == ""){
		swal("参数传递错误.");
		return;
	}
	
	   swal({
           title: "您确定要删除这条信息吗",
           text: "请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "删除",
           closeOnConfirm: false
       },
       function() {
		managerUserAction.delTouristUser(userId,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "删除成功",type: "success"});
				userList.getAllUsersByCondition();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("删除信息失败.");
			}
	    });		
	});
};

/**
 * 锁定用户
 */
UserListPage.prototype.lockUser = function(userId){
	if(userId == null || userId == ""){
		alert("参数传递错误.");
		return;
	}
	
	 swal({
         title: "确认要锁定该用户？",
         text: "请谨慎操作！",
         type: "warning",
         showCancelButton: true,
         confirmButtonColor: "#DD6B55",
         cancelButtonText: "取消",
         confirmButtonText: "确定",
         closeOnConfirm: false
       },
     function() {
		managerUserAction.lockUser(userId,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "操作成功",type: "success"});
				userList.getAllUsersByCondition();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("锁定用户请求失败.");
			}
	    });		
	});
};

/**
 * 解锁用户
 */
UserListPage.prototype.unlockUser = function(userId){
	if(userId == null || userId == ""){
		alert("参数传递错误.");
		return;
	}
	
	 swal({
         title: "确认要为该用户解锁？",
         text: "请谨慎操作！",
         type: "warning",
         showCancelButton: true,
         confirmButtonColor: "#DD6B55",
         cancelButtonText: "取消",
         confirmButtonText: "确定",
         closeOnConfirm: false
       },
     function() {
		managerUserAction.unlockUser(userId,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "操作成功",type: "success"});
				userList.getAllUsersByCondition();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("锁定用户请求失败.");
			}
	    });		
	});
};


/**
 * 锁定用户
 */
UserListPage.prototype.lockWithdraw = function(userId){
	if(userId == null || userId == ""){
		alert("参数传递错误.");
		return;
	}
	
	 swal({
         title: "确认要对该用户进行提现锁定？",
         text: "请谨慎操作！",
         type: "warning",
         showCancelButton: true,
         confirmButtonColor: "#DD6B55",
         cancelButtonText: "取消",
         confirmButtonText: "确定",
         closeOnConfirm: false
       },
     function() {
		managerUserAction.lockWithdraw(userId,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "操作成功",type: "success"});
				userList.getAllUsersByCondition();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("锁定请求失败.");
			}
	    });		
	});
};

/**
 * 解锁用户
 */
UserListPage.prototype.unlockWithdraw = function(userId){
	if(userId == null || userId == ""){
		alert("参数传递错误.");
		return;
	}
	
	 swal({
         title: "确认要为该用户提现解锁？",
         text: "请谨慎操作！",
         type: "warning",
         showCancelButton: true,
         confirmButtonColor: "#DD6B55",
         cancelButtonText: "取消",
         confirmButtonText: "确定",
         closeOnConfirm: false
       },
     function() {
		managerUserAction.unlockWithdraw(userId,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "操作成功",type: "success"});
				userList.getAllUsersByCondition();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("锁定用户请求失败.");
			}
	    });		
	});
};



/**
 * 初始化用户密码
 */
UserListPage.prototype.resetUserPass = function(userId){
	if(userId == null || userId == ""){
		alert("参数传递错误.");
		return;
	}
	 swal({
         title: "确定初始化该用户的登录密码吗?",
         text: "请谨慎操作！",
         type: "warning",
         showCancelButton: true,
         confirmButtonColor: "#DD6B55",
         cancelButtonText: "取消",
         confirmButtonText: "确定",
         closeOnConfirm: false
       },
     function() {
		managerUserAction.resetUserPass(userId,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "操作成功,初始化密码："+r[1],type: "success"});
				userList.getAllUsersByCondition();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("请求失败.");
			}
		});		
	});
};

/**
 * 初始化用户密码
 */
UserListPage.prototype.resetUserPass1 = function(userId){
	if(userId == null || userId == ""){
		alert("参数传递错误.");
		return;
	}
	 swal({
         title: "确定初始化该用户的安全密码吗?",
         text: "请谨慎操作！",
         type: "warning",
         showCancelButton: true,
         confirmButtonColor: "#DD6B55",
         cancelButtonText: "取消",
         confirmButtonText: "确定",
         closeOnConfirm: false
       },
     function() {
		managerUserAction.resetUserPass1(userId,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "操作成功,初始化安全密码："+r[1],type: "success"});
				userList.getAllUsersByCondition();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("请求失败.");
			}
		});		
	});
};


//查看在线会员
UserListPage.prototype.showOnlineUsers = function() {
	  layer.open({
          type: 2,
          title: '在线会员列表',
          maxmin: false,
          shadeClose: true,
          //点击遮罩关闭层
          area: ['800px', '420px'],
         // area: ['60%', '58%'],
          content: contextPath + "/managerzaizst/user/user_online.html",
          cancel: function(index){ 
        	  userList.getAllUsersByCondition();
       	   }
      });
}

//会员模式调整
UserListPage.prototype.adjustUserModel = function() {
	  layer.open({
          type: 2,
          title: '会员模式调整',
          maxmin: false,
          shadeClose: true,
          //点击遮罩关闭层
          area: ['800px', '500px'],
         // area: ['60%', '58%'],
          content: contextPath + "/managerzaizst/user/user_rebate_adjust.html",
          cancel: function(index){ 
        	  userList.getAllUsersByCondition();
       	   }
      });
}

//会员模式调整
UserListPage.prototype.starLevelAdjust = function() {
	  layer.open({
          type: 2,
          title: '会员星级调整',
          maxmin: false,
          shadeClose: true,
          //点击遮罩关闭层
          area: ['800px', '450px'],
         // area: ['60%', '58%'],
          content: contextPath + "/managerzaizst/user/user_star_level_adjust.html",
          cancel: function(index){ 
        	  userList.getAllUsersByCondition();
       	   }
      });
}

//导入会员数据
UserListPage.prototype.importingMembersData = function() {
	  layer.open({
          type: 2,
          title: '导入会员数据',
          maxmin: false,
          shadeClose: true,
          //点击遮罩关闭层
          area: ['800px', '300px'],
         // area: ['60%', '58%'],
          content: contextPath + "/managerzaizst/user/import_membersData.html",
          cancel: function(index){ 
        	  userList.getAllUsersByCondition();
       	   }
      });
}

//批量删除试玩账号
UserListPage.prototype.batchDeleting = function() {
	  layer.open({
          type: 2,
          title: '批量删除试玩账号',
          maxmin: false,
          shadeClose: true,
          //点击遮罩关闭层
          area: ['800px', '400px'],
         // area: ['60%', '58%'],
          content: contextPath + "/managerzaizst/user/user_istourist_adjust.html",
          cancel: function(index){ 
        	  userList.getAllUsersByCondition();
       	   }
      });
}

/**
 * 跳出
 */
UserListPage.prototype.gotohref =function (link,tabName)
{

	 var el=parent.document.getElementById('menuItemUtil');
	 
	 el.href=link;
	 el.text=tabName;
	 el.click();
	 
}

UserListPage.prototype.closeLayer=function(index){
	layer.close(index);
	userList.getAllUsersByCondition();
}