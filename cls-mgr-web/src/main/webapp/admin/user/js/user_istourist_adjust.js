function UserIstouristAdjust(){}
var userIstouristAdjust=new UserIstouristAdjust();

userIstouristAdjust.param = {
		
};
userIstouristAdjust.queryParam = {
		bizSystem:null,
		addtime:null	
};

$(document).ready(function() {
	var strDate = new Date();
	userIstouristAdjust.queryParam.addtime=strDate.AddDays(-2).setHours(0,0,0,0);
	$("#addtime").change(function(){
		var todate = new Date();
		var xz=$("#addtime").val();
		if(xz==1){
			userIstouristAdjust.queryParam.addtime=todate.AddDays(-2).setHours(0,0,0,0);
		}else if(xz==2){
			userIstouristAdjust.queryParam.addtime=todate.AddDays(-3).setHours(0,0,0,0);
		}else if(xz==3){
			userIstouristAdjust.queryParam.addtime=todate.AddDays(-4).setHours(0,0,0,0);
		}
	})
});

UserIstouristAdjust.prototype.batchDeletingIsTourIst=function(){
	var addtime=$("#addtime").val();
	swal({
        title: "您确定要删除"+addtime+"天前的账号相关信息吗",
        text: "请谨慎操作！",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        cancelButtonText: "取消",
        confirmButtonText: "删除",
        closeOnConfirm: false
    },
    function() {
    	var bizSystem=$("#bizSystem").val();
    	if(bizSystem==null||bizSystem==""){
    		userIstouristAdjust.queryParam.bizSystem=null;
    	}else{
    		userIstouristAdjust.queryParam.bizSystem=bizSystem;
    	}
		managerUserAction.delAllTouristUser(userIstouristAdjust.queryParam,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "删除成功",type: "success"});
				parent.userList.closeLayer();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("删除信息失败.");
			}
	    });	
    });
}