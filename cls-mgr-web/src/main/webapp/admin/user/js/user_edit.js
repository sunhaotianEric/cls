function UserEditPage(){}
var userEdit= new UserEditPage();

userEdit.editParam = {
	userId : null,
	starLevel : 0
};

$(document).ready(function() {
	userEdit.getAwardModel("sscRebate");
	
	if(userEdit.editParam.isTourist == 1){
	 $(".vipLevel").show();
	 $(".point").show();
	}
	//绑定星级编辑事件
	$(".jsstar >li").hover(
        function(){
			var inLevel = $(this).attr("data-val");
			userEdit.setStarLevel(inLevel);
		},
        function(){
			userEdit.setStarLevel(userEdit.editParam.starLevel);
		})
    .click(function(){
    	userEdit.editParam.starLevel = $(this).attr("data-val");
		userEdit.setStarLevel(userEdit.editParam.starLevel);
	});   
});


/**
 * 获取用户
 */
UserEditPage.prototype.getUser = function(userId){
	if(userId == null || userId == ""){
		swal("参数传递错误.");
		return;
	}
	managerUserAction.getUser(userId,function(r){
		if (r[0] != null && r[0] == "ok") {
			userEdit.showUserMsg(r[1],r[2]);
		}else if (r[0] != null && r[0] == "error") {
			swal(r[1]);
		}else{
			swal("获取用户信息请求失败.");
		}
    });
};
	
/**
 * 显示用户信息
 */
UserEditPage.prototype.showUserMsg = function(userMsg,awardModelConfig){
	$("#userName").val(userMsg.userName);
	$("#loginLock").find("option[value='"+userMsg.loginLock+"']").attr("selected",true);
	$("#withdrawLock").find("option[value='"+userMsg.withdrawLock+"']").attr("selected",true);
	$("#dailiLevel").find("option[value='"+userMsg.dailiLevel+"']").attr("selected",true);
	$("#phone").val(userMsg.phone);
	$("#identityid").val(userMsg.identityid);
	$("#birthday").val(userMsg.birthday);
	$("#trueName").val(userMsg.trueName);	
	$("#bankName").val(userMsg.bankName);	
	$("#bankArea").val(userMsg.bankArea);	
	$("#bankNum").val(userMsg.bankNum);	
	$("#qq").val(userMsg.qq);	
	$("#email").val(userMsg.email);	
	$("#addTime").val(userMsg.addTimeStr);	
	$("#sex").find("option[value='"+userMsg.sex+"']").attr("selected",true);
	//自己不能编辑自己 
//    if(currentUser.username==userMsg.userName){
//    	$("#withdrawLock").attr("disabled","disabled");
//    	$("#dailiLevel").attr("disabled","disabled");
//    	$("#sscRebate").attr("disabled","disabled");	
//     }
	if(userMsg.dailiLevel=='REGULARMEMBERS')
	{
		$("#dailiLevel").removeAttr("disabled");
		$("#dailiLevel").empty();        
		$("#dailiLevel").append("<option value='ORDINARYAGENCY'>普通代理</option><option value='REGULARMEMBERS'>普通会员</option>");
		
	}
	$("#dailiLevel").val(userMsg.dailiLevel);
	
	if(userMsg.dailiLevel !='SUPERAGENCY')
	{
		$("#sscRebate").removeAttr("disabled");
		$("#syxwRebate").removeAttr("disabled");
		$("#ksRebate").removeAttr("disabled");
		$("#pk10Rebate").removeAttr("disabled");
		$("#dpcRebate").removeAttr("disabled");
		//$("#ffcRebate").removeAttr("disabled");
	}
	//设置星级
	if(userMsg.starLevel != null) {
		userEdit.editParam.starLevel = userMsg.starLevel;
	} else {
		userEdit.editParam.starLevel = 0;
	}
	userEdit.setStarLevel(userEdit.editParam.starLevel);

	
//	if(higherLevelUser!=null)
//	{
//	    var higherLevelUserSscRebate = higherLevelUser.sscrebate;
//		var sscRebateArray = new Array();
//		$("#sscRebate option").each(function (){  
//	        var value = $(this).val();  
//	        if(value >= higherLevelUserSscRebate){
//	        	sscRebateArray.push(this);
//	        }
//	     });  
//	   for(var i = 0; i < sscRebateArray.length; i++){
//		   $(sscRebateArray[i]).remove();
//	   }	
//	}
	$("#syxwRebate").val(userMsg.syxwRebate);
	$("#ksRebate").val(userMsg.ksRebate);
	$("#pk10Rebate").val(userMsg.pk10Rebate);
	$("#dpcRebate").val(userMsg.dpcRebate);
	//$("#ffcRebate").val(userMsg.ffcRebate);
	$("#sscRebate").find("option[value='"+userMsg.sscRebate+"']").attr("selected",true);
	
	if(userEdit.editParam.isTourist == 1){
		$("#point").val(userMsg.point);
		$("#vipLevel").find("option[value='"+userMsg.vipLevel+"']").attr("selected",true);
	}
	
};

/**
 * 添加新用户
 */
UserEditPage.prototype.editUser = function(){
	var userName = $("#userName").val();
	var phone = $("#phone").val();
	var identityid = $("#identityid").val();
	var birthday = $("#birthday").val();
	var trueName = $("#trueName").val();	
	var loginLock = $("#loginLock").val();
	var withdrawLock = $("#withdrawLock").val();
	var dailiLevel = $("#dailiLevel").val();
	var sscRebate = $("#sscRebate").val();
//	var ffcRebate = $("#ffcRebate").val();
	var syxwRebate = $("#syxwRebate").val();
	var ksRebate = $("#ksRebate").val();
	var dpcRebate = $("#dpcRebate").val();
	var pk10Rebate = $("#pk10Rebate").val();
//	var klsfrebate = $("#klsfrebate").val();

	var qq = $("#qq").val();	
	var email = $("#email").val();	
	var sex = $("#sex").val();	
//	var province = $("#province").val();	
//	var city = $("#city").val();	
//	var address = $("#address").val();
	
	var vipLevel = $("#vipLevel").val();
	var point = $("#point").val();
	
	
	userEdit.editParam.id = userEdit.editParam.userId;
	userEdit.editParam.userName = userName;
	userEdit.editParam.phone = phone;
	userEdit.editParam.identityid = identityid;
	userEdit.editParam.birthday = birthday;
	userEdit.editParam.trueName = trueName;
	userEdit.editParam.loginLock = loginLock;
	userEdit.editParam.withdrawLock = withdrawLock;
	userEdit.editParam.dailiLevel = dailiLevel;
	userEdit.editParam.qq = qq;
	userEdit.editParam.email = email;	
	userEdit.editParam.sex = sex;
	
	userEdit.editParam.province = null;	
	userEdit.editParam.city = null;	
	userEdit.editParam.address = null;
	
	userEdit.editParam.vipLevel = vipLevel;
	userEdit.editParam.point = point;
	
//	if(province == "--请选择--"){
//		userEdit.editParam.province = null;
//	}else{
//		userEdit.editParam.province = province;	
//	}
//	userEdit.editParam.city = city;	
//	userEdit.editParam.address = address;	
	
	if (sscRebate != null && sscRebate > 0) {
		userEdit.editParam.sscRebate = sscRebate;
	}else{
		swal("请输入时时彩模式！");
	}
//	if (ffcRebate != null && ffcRebate > 0) {
//		userEdit.editParam.ffcRebate = ffcRebate;
//	}else{
//		swal("请输入分分彩模式！");
//		$("#ffcRebate").focus();
//	}
	if (syxwRebate != null && syxwRebate > 0) {
		userEdit.editParam.syxwRebate = syxwRebate;
	}else{
		swal("请输入十一选五模式！");
		$("#syxwRebate").focus();
	}
	if (ksRebate != null && ksRebate > 0) {
		userEdit.editParam.ksRebate = ksRebate;
	}else{
		swal("请输入快三模式！");
		$("#ksRebate").focus();
	}
	if (dpcRebate != null && dpcRebate > 0) {
		userEdit.editParam.dpcRebate = dpcRebate;
	}else{
		swal("请输入低频彩模式！");
		$("#dpcRebate").focus();
	}
	
	if (pk10Rebate != null && pk10Rebate > 0) {
		userEdit.editParam.pk10Rebate = pk10Rebate;
	}else{
		swal("请输入PK10模式！");
		$("#pk10Rebate").focus();
	}
	
	if(userEdit.editParam.isTourist == 1){
		if(point < 0){
			swal("积分不能小于0！");
			return;
		}
	}else{
		userEdit.editParam.vipLevel = "";
		userEdit.editParam.point = null;
	}
	
	//编辑用户
	managerUserAction.editUser(userEdit.editParam,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
	               parent.userList.closeLayer(index);
		   	   });
		}else if (r[0] != null && r[0] == "error") {
			swal(r[1]);
		}else{
			swal("编辑用户请求失败.");
		}
    });
};

UserEditPage.prototype.setStarLevel = function(starLevel) {
	$(".jsstar li").each(function(i){
		if(i < starLevel) {
			$(this).css({"background-position":"left bottom"});
		} else {
			$(this).css({"background-position":"left top"});
		}
	});
}


/**
 * 初始化奖金模式
 */
UserEditPage.prototype.getAwardModel = function(id){
	var selDom = $("#"+id);
	managerBizSystemAction.getAwardModel(userEdit.editParam.userId,function(r){
		if (r[0] != null && r[0] == "ok") {
			highestAwardModel=r[1].sscHighestAwardModel;
			lowestAwardModel=r[1].sscLowestAwardModel;
			for(var i=0;i<=(highestAwardModel-lowestAwardModel);i=i+2){
				selDom.append("<option value='"+(highestAwardModel-i)+"'>"+(highestAwardModel-i)+"</option>");	
			}
			//模式执行玩彩查询用户
			userEdit.getUser(userEdit.editParam.userId);
		}else if (r[0] != null && r[0] == "error") {
			swal(r[1]);
		}else{
			swal("请求失败.");
		}}
	);
	
}