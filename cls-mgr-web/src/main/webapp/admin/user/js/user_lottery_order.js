function UserLotteryOrderPage(){}
var userLotteryOrder = new UserLotteryOrderPage();

userLotteryOrder.param = {
};

/**
 * 查询参数
 */
userLotteryOrder.queryParam = {
		
};

// 分页参数
userLotteryOrder.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize
};

/**
 * 查询参数
 */
userLotteryOrder.queryParam = {
		lotteryType : null,
		lotteryId : null,
		expect : null,
		userName : null,
		fromType : null,
		enabled : 1,	// 默认用户非游客,有效数据
		createdDateStart : null,
		createdDateEnd : null
};

$(document).ready(function() {
	commonPage.setDateRangeTime(1, "createdDateStart", "createdDateEnd",2);
	userLotteryOrder.initTableData(); // 根据条件,查找资金明细
});
	
/**
 * 加载所有的登陆日志数据
 */
UserLotteryOrderPage.prototype.getAllLotteryOrdersForQuery = function(dateRange){
	userLotteryOrder.pageParam.queryMethodParam = 'getAllLotteryOrdersForQuery';
	if(dateRange != undefined && dateRange != null && dateRange != ''){
		commonPage.setDateRangeTime(dateRange, "createdDateStart", "createdDateEnd",2);	
	}
	var lotteryType = $("#lotteryType").val();
	var expect = $("#expect").val();
	var userName = $("#userName").val();
	var lotteryId = $("#lotteryId").val();
	var createdDateStart = $("#createdDateStart").val();
	var createdDateEnd = $("#createdDateEnd").val();
	var orderStatus = $("#orderStatus").val();
	var prostateStatus = $("#prostateStatus").val();
	var orderSort = $("#orderSort").val();
	var lotteryModel = $("#lotteryModel").val();
	var fromType = $("#fromType").val();
	userLotteryOrder.queryParam.bizSystem=getSearchVal("bizSystem");
	if(lotteryType == ""){
		userLotteryOrder.queryParam.lotteryType = null;
	}else{
		userLotteryOrder.queryParam.lotteryType = lotteryType;
	}
	
	if(expect == ""){
		userLotteryOrder.queryParam.expect = null;
	}else{
		userLotteryOrder.queryParam.expect = expect;
	}
	
	if(lotteryId == ""){
		userLotteryOrder.queryParam.lotteryId = null;
	}else{
		userLotteryOrder.queryParam.lotteryId = lotteryId;
	}
	
	if(userName == ""){
		userLotteryOrder.queryParam.userName = null;
	}else{
		userLotteryOrder.queryParam.userName = userName;
	}
	
	if(createdDateStart == ""){
		userLotteryOrder.queryParam.createdDateStart = null;
	}else{
		userLotteryOrder.queryParam.createdDateStart = new Date(createdDateStart);
	}
	if(createdDateEnd == ""){
		userLotteryOrder.queryParam.createdDateEnd = null;
	}else{
		userLotteryOrder.queryParam.createdDateEnd = new Date(createdDateEnd);
	}
	
	if(orderStatus == ""){
		userLotteryOrder.queryParam.orderStatus = null;
	}else{
		userLotteryOrder.queryParam.orderStatus = orderStatus;
	}
	
	if(prostateStatus == ""){
		userLotteryOrder.queryParam.prostateStatus = null;
	}else{
		userLotteryOrder.queryParam.prostateStatus = prostateStatus;
	}
	
	if(orderSort == ""){
		userLotteryOrder.queryParam.orderSort = null;
	}else{
		userLotteryOrder.queryParam.orderSort = orderSort;
	}
	
	if(lotteryModel == ""){
		userLotteryOrder.queryParam.lotteryModel = null;
	}else{
		userLotteryOrder.queryParam.lotteryModel = lotteryModel;
	}
	
	if(fromType == ""){
		userLotteryOrder.queryParam.fromType = null;
	}else{
		userLotteryOrder.queryParam.fromType = fromType;
	}
	
	userLotteryOrder.table.ajax.reload();
};




/**
 * 初始化列表数据
 */
UserLotteryOrderPage.prototype.initTableData = function() {
	userLotteryOrder.queryParam = getFormObj($("#queryForm"));
	if(isTouristParameter == 1){
		userLotteryOrder.queryParam.enabled = 0;
	}else{
		userLotteryOrder.queryParam.enabled = 1;
	}
	userLotteryOrder.table = $('#lotteryOrderTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"scrollX": true,
		"fnDrawCallback": function (oSettings) {  
			userLotteryOrder.refreshUserLotteryOrderPages(userLotteryOrder.totalOrder);
	        },
		"ajax":function (data, callback, settings) {
			userLotteryOrder.pageParam.pageSize = data.length;// 页面显示记录条数
			userLotteryOrder.pageParam.pageNo = (data.start / data.length)+1;// 当前页码
	    	managerOrderAction.getAllOrdersByQuery(userLotteryOrder.queryParam,userLotteryOrder.pageParam,function(r){
				// 封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;// 返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;// 后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;// 返回的数据列表
					userLotteryOrder.totalOrder=r[2];
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
                    {"data": "bizSystemName","bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {
		            	"data": "lotteryId"
		            },
		            {"data": "lotteryTypeDes"},
		            {"data": "expect"},
		            {
		            	"data": "userName",
		            	  render: function(data, type, row, meta) {
		            		  
		            		  return "<a href='javascript:void(0)' style='color:blue;text-decoration:underline;'  onclick='commonPage.showUserMsgPage(this,"+row.userId+")'>"+ row.userName +"</a>";
		            	}
		            },
		            {"data": "payMoney",
		            	  render: function(data, type, row, meta) {
		            		  return row.payMoney.toFixed(commonPage.param.fixVaue);
		            		}
		            },
		            {"data": "multiple"},
		     /*
				 * {"data": null, render: function(data, type, row, meta) {
				 * return "<a style='color: blue;text-decoration: underline;'
				 * href='javascript:void(0)'
				 * onclick='userLotteryOrder.comparisonOrderCodes(this,"+row.id+")'>点击比对</a>"; } },
				 */
		            {"data": "afterNumber",
		            	  render: function(data, type, row, meta) {
		            		  if(data == 1){
		            				return "是";
		            			}else{
		            				return "否";
		            			}
		            			
		            		}
		            },
		            {"data": "statusStr","bVisible" :false},
		            {"data": "prostateStatusStr",
		            	render: function(data, type, row, meta) {
		            		if(row.prostate == "DEALING"){
		            			return '<font class="cp-yellow">'+data+'</font>';
		            		}if(row.prostate == "NOT_WINNING"){
		            			return '<font class="cp-green">'+data+'</font>';
		            		}else{
		            			return '<font class="cp-red">'+data+'</font>';
		            		}
		            		return data;
		            }
		             },
		            {"data": "winCost"},
		            {"data": null,
		            	  render: function(data, type, row, meta) {
		            		  return "<a style='color: blue;text-decoration: underline;' href='javascript:void(0)' onclick='userLotteryOrder.getOrderCodesDes(this,"+row.id+")'>订单详情</a>";
		            		}
		            },
		            {"data": "createdDateStr"},
// {"data": "remark",
// render: function(data, type, row, meta) {
// var reamrk = "";
// if(row.remark != null && row.remark != "") {
// reamrk = data;
// }
// return reamrk;
// }
// },
		            {"data": "fromType",
		            	  render: function(data, type, row, meta) {
		            			var fromType = "";
		            			if("PC" == row.fromType) {
		            				fromType = "电脑";
		            			} else if("MOBILE" == row.fromType) {
		            				fromType = "手机web";
		            			} else if("APP" == row.fromType) {
		            				fromType = "APP";
		            			}
		            			return fromType;
		            	  }
		            },
	
		            {"data": null,
		            	  render: function(data, type, row, meta) {
		            		var str="<a class='btn btn-success btn-sm btn-xf' href='javascript:void(0)' onclick='userLotteryOrder.comparisonOrderCodes(this,"+row.id+")'>点击比对</a>&nbsp;";
		            		  if(row.prostate == "DEALING"){  // 处于处理中的订单使用均可以使用未开奖回退
		            				str += "<a class='btn btn-info btn-sm btn-xf' href='javascript:void(0)' onclick='userLotteryOrder.lotteryRegressionForNotDraw(this,"+row.id+")'>未开奖回退</a>&nbsp;";
		            			}else{
		            				str += "";
		            			}
		            		if(currentUser.bizSystem=='SUPER_SYSTEM'){
			            		var curdate = new Date();
			            		var endTime = (dateFormat(curdate,'yyyy-MM-dd')+" 02:59:59").stringToDate();
			            		
			            		if(curdate < endTime){
			            			var t=endTime.getTime()-1000*60*60*24+1000;
			            			var startTime=new Date(t);
			            	
			            			if(row.createdDate>startTime){
			            				if(row.prostate == "DEALING" || row.prostate == "NOT_WINNING" || row.prostate == "WINNING"){
					            			str += "<a class='btn btn-danger btn-sm btn-xf' href='javascript:void(0)' onclick='userLotteryOrder.lotteryRegression(this,"+row.id+")'>强制回退</a>";
					            		}else{
					            			str += "";
					            		}
			            			}
			            		}else{
			            			if(row.createdDate>endTime){
			            				if(row.prostate == "DEALING" || row.prostate == "NOT_WINNING" || row.prostate == "WINNING"){
					            			str += "<a class='btn btn-info btn-sm btn-xf' href='javascript:void(0)' onclick='userLotteryOrder.lotteryRegression(this,"+row.id+")'>强制回退</a>";
					            		}else{
					            			str += "";
					            		}
			            			}
			            		}
		            		}
		            	
		            		  return str;
		            		}
		            }
	            ]
	}).api(); 
}


// /**
// * 加载资金明细类型
// */
// UserLotteryOrderPage.prototype.getAllTypes = function(){
// managerOrderAction.getAllTypes(function(r){
// if (r[0] != null && r[0] == "ok") {
// userLotteryOrder.refreshTypes(r[1],r[2],r[3]);
// }else if(r[0] != null && r[0] == "error"){
// alert(r[1]);
// }else{
// showErrorDlg(jQuery(document.body), "查询所有的资金类型失败.");
// }
// });
// };

/**
 * 展示资金类型数据
 */
UserLotteryOrderPage.prototype.refreshTypes = function(detailTypeMaps,orderStatusMaps,prostateStatusMaps){
	var obj=document.getElementById('lotteryType');
	obj.options.add(new Option("=请选择=",""));
	for(var key in detailTypeMaps){
		obj.options.add(new Option(detailTypeMaps[key],key)); 
	}  
	
	var obj=document.getElementById('orderStatus');
	obj.options.add(new Option("=请选择=",""));
	for(var key in orderStatusMaps){
		obj.options.add(new Option(orderStatusMaps[key],key)); 
	}  
	
	var obj=document.getElementById('prostateStatus');
	obj.options.add(new Option("=请选择=",""));
	for(var key in prostateStatusMaps){
		obj.options.add(new Option(prostateStatusMaps[key],key)); 
	}  
	
};

/**
 * 获取订单详情
 */
UserLotteryOrderPage.prototype.getOrderCodesDes = function(afterNumber,orderId){
	var url="";
	// 是否追号
	if(afterNumber==1){
		url = contextPath + "/managerzaizst/lotterymsg/"+orderId+"/lotterymsg.html";
	}else{
		url = contextPath + "/managerzaizst/lotterymsg/"+orderId+"/lotterymsg.html";
	}
	
	layer.open({
          type: 2,
          title: '订单详情',
          maxmin: false,
          shadeClose: true,
          // 点击遮罩关闭层
          area: ['900px', '450px'],
          content: url,
          cancel: function(index){ 
        	  layer.close();
       	   }
      });
	
	
};

/**
 * 显示订单详情
 */
UserLotteryOrderPage.prototype.showOrderCodesPage = function(obj,order){
	if(order != null){
		if(order.openCode != null){  
			$('#form_openCode').val(order.openCode);
			$('#form_winCost').val(order.winCost.toFixed(commonPage.param.fixVaue));
		}else{
			$('#form_openCode').val('未开奖');
			$('#form_openCode').css("color",'red');
			$('#form_winCost').val('未知');
			$('#form_winCost').css("color",'red');
		}
		$('#form_awardModel').val(order.awardModel);
		$('#form_cathecticCount').val(order.cathecticCount);
		if(order.lotteryModel == "YUAN"){
			$('#form_lotteryModel').val("元");
		}else if(order.lotteryModel == "JIAO"){
			$('#form_lotteryModel').val("角");

		}else if(order.lotteryModel == "FEN"){
			$('#form_lotteryModel').val("分");
		}else if(order.lotteryModel == "LI"){
			$('#form_lotteryModel').val("厘");
		}else if(order.lotteryModel == "HAO"){
			$('#form_lotteryModel').val("毫");
		}else{
			swal("未知的投注类型.");
		}
		
		var str = "";
		var orderCodesList = order.codesList;
		for(var i = 0; i < orderCodesList.length; i++){
			var orderCodesMap = orderCodesList[i];
			for(var key in orderCodesMap){
				str += "[" + key +"] " + orderCodesMap[key].split("_")[0].replace(/\&/g," ") + "\n";

			}
		}
		$('#form_codeDetail').val(str);
	}else{
		swal("订单信息不准确");
	}
	$('#order-form').modal('show');
};


/**
 * 未开奖投注回退
 * 
 * @param obj
 * @param orderId
 */
UserLotteryOrderPage.prototype.lotteryRegressionForNotDraw = function(obj,orderId){
	 swal({
         title: "确认该订单是未开奖订单吗?回退将归还用户投注金额",
         text: "请谨慎操作！",
         type: "warning",
         showCancelButton: true,
         confirmButtonColor: "#DD6B55",
         cancelButtonText: "取消",
         confirmButtonText: "确定",
         closeOnConfirm: false
     },
     function() {
		managerOrderAction.lotteryRegressionForNotDraw(orderId,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "未开奖回退成功.",type: "success"});
				userLotteryOrder.getAllLotteryOrdersForQuery();		
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("订单回退请求失败.");
			}
	    });	
	});
};

/**
 * 强制回退
 * 
 * @param obj
 * @param orderId
 */
UserLotteryOrderPage.prototype.lotteryRegression = function(obj,orderId){
	 swal({
         title: "确认强制回退该订单吗?强制回退以后会扣除该用户中奖金额，归还用户投注金额",
         text: "请谨慎操作！",
         type: "warning",
         showCancelButton: true,
         confirmButtonColor: "#DD6B55",
         cancelButtonText: "取消",
         confirmButtonText: "确定",
         closeOnConfirm: false
     },
     function() {
		managerOrderAction.lotteryRegression(orderId,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "强制回退成功..",type: "success"});
				userLotteryOrder.getAllLotteryOrdersForQuery();		
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("订单回退请求失败.");
			}
	    });	
	});
};

/**
 * 订单比对
 * 
 * @param obj
 * @param orderId
 */
UserLotteryOrderPage.prototype.comparisonOrderCodes = function(obj,orderId){
	managerOrderAction.comparisonOrderCodes(orderId,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal("无异常.");
          // $(obj).text("无异常,点击再次比对");
		}else if(r[0] != null && r[0] == "error"){
			if(r[2] != null){
				userLotteryOrder.showComparisonOrderCodesPage(obj,r[1],r[2]);
			}else{
				swal(r[1]);
			}
			// $(obj).text("有异常");
		}else{
			swal("订单号码比对请求失败.");
		}
    });
};

/**
 * 显示订单详情
 */
UserLotteryOrderPage.prototype.showComparisonOrderCodesPage = function(obj,order,lotteryCache){
	if(order != null){
		
		if(order.openCode != null){  
			$('#form_openCode').val(order.openCode);
			$('#form_winCost').val(order.winCost.toFixed(commonPage.param.fixVaue));
		}else{
			$('#form_openCode').val('未开奖');
			$('#form_openCode').css("color",'red');
			$('#form_winCost').val('未知');
			$('#form_winCost').css("color",'red');
		}	
		// 正常投注号码
		var str = "";
		var orderCodesList = order.codesList;
		for(var i = 0; i < orderCodesList.length; i++){
			var orderCodesMap = orderCodesList[i];
			for(var key in orderCodesMap){
				str += "[" + key +"] " + orderCodesMap[key] + "\n";
			}
		}
		
		// 缓存投注号码
		var cacheCodesList = lotteryCache.codesList;;
		var cacheStr = "";
		for(var i = 0; i < cacheCodesList.length; i++){
			var cacheCodesMap = cacheCodesList[i];
			for(var key in cacheCodesMap){
				cacheStr += "[" + key +"] " + cacheCodesMap[key] + "\n";
			}
		}
		$('#form_codeDetail').val(str);	
		$("#form_cacheCodeDetail").val(cacheStr );
	}else{
		swal("订单信息不准确");
	}
};
/**
 * 加载所有的登陆日志数据
 */
UserLotteryOrderPage.prototype.orderInfoExport = function(dateRange){
	/*
	 * userLotteryOrder.pageParam.queryMethodParam =
	 * 'getAllLotteryOrdersForQuery';
	 */
	if(dateRange != undefined && dateRange != null && dateRange != ''){
		commonPage.setDateRangeTime(dateRange, "createdDateStart", "createdDateEnd",2);	
	}
	var lotteryType = $("#lotteryType").val();
	var expect = $("#expect").val();
	var userName = $("#userName").val();
	var lotteryId = $("#lotteryId").val();
	var createdDateStart = $("#createdDateStart").val();
	var createdDateEnd = $("#createdDateEnd").val();
	var orderStatus = $("#orderStatus").val();
	var prostateStatus = $("#prostateStatus").val();
	var orderSort = $("#orderSort").val();
	var lotteryModel = $("#lotteryModel").val();
	var fromType = $("#fromType").val();
	userLotteryOrder.queryParam.bizSystem=getSearchVal("bizSystem");
	if(lotteryType == ""){
		userLotteryOrder.queryParam.lotteryType = null;
	}else{
		userLotteryOrder.queryParam.lotteryType = lotteryType;
	}
	
	if(expect == ""){
		userLotteryOrder.queryParam.expect = null;
	}else{
		userLotteryOrder.queryParam.expect = expect;
	}
	
	if(lotteryId == ""){
		userLotteryOrder.queryParam.lotteryId = null;
	}else{
		userLotteryOrder.queryParam.lotteryId = lotteryId;
	}
	
	if(userName == ""){
		userLotteryOrder.queryParam.userName = null;
	}else{
		userLotteryOrder.queryParam.userName = userName;
	}
	
	if(createdDateStart == ""){
		userLotteryOrder.queryParam.createdDateStart = null;
	}else{
		userLotteryOrder.queryParam.createdDateStart = new Date(createdDateStart);
	}
	if(createdDateEnd == ""){
		userLotteryOrder.queryParam.createdDateEnd = null;
	}else{
		userLotteryOrder.queryParam.createdDateEnd = new Date(createdDateEnd);
	}
	
	if(orderStatus == ""){
		userLotteryOrder.queryParam.orderStatus = null;
	}else{
		userLotteryOrder.queryParam.orderStatus = orderStatus;
	}
	
	if(prostateStatus == ""){
		userLotteryOrder.queryParam.prostateStatus = null;
	}else{
		userLotteryOrder.queryParam.prostateStatus = prostateStatus;
	}
	
	if(orderSort == ""){
		userLotteryOrder.queryParam.orderSort = null;
	}else{
		userLotteryOrder.queryParam.orderSort = orderSort;
	}
	
	if(lotteryModel == ""){
		userLotteryOrder.queryParam.lotteryModel = null;
	}else{
		userLotteryOrder.queryParam.lotteryModel = lotteryModel;
	}
	
	if(fromType == ""){
		userLotteryOrder.queryParam.fromType = null;
	}else{
		userLotteryOrder.queryParam.fromType = fromType;
	}
	
	
	managerOrderAction.orderInfoExport(userLotteryOrder.queryParam,function(r){
		dwr.engine.openInDownload(r);
		
    });
};

/**
 * 用户投注订单列表底部,总计.
 */
UserLotteryOrderPage.prototype.refreshUserLotteryOrderPages = function(totalOrder){
	var lotteryOrderListObj = $("#lotteryOrderTable tbody");
	/* rechargeOrderListObj.html(""); */

	var str = "";
	if(totalOrder != null){
		str += "<tr>";
		str += "  <td>总计</td>";
		str += "  <td></td>";
		str += "  <td></td>";
		str += "  <td></td>";
		if(currentUser.bizSystem=='SUPER_SYSTEM'){
			str += "  <td></td>";
		}
		str += "  <td style='color:red;'>"+ totalOrder.payMoney.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td></td>";
		str += "  <td></td>";
		str += "  <td></td>";
		str += "  <td style='color:red;'>"+ totalOrder.winCost.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td></td>";
		str += "  <td></td>";
		str += "  <td></td>";
		str += "</tr>";
		lotteryOrderListObj.append(str);		
	}
};
