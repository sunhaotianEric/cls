function UserRebateAdjust(){};
var userRebateAdjust=new UserRebateAdjust();

/**
 * 添加参数
 */
userRebateAdjust.editParam = {
	bizSystem:null,
	userName:null,
	sscrebate : null,
	ffcRebate : null,
	syxwRebate : null,
	ksRebate : null,
	klsfRebate : null,
	dpcRebate : null,
	pk10Rebate:null,
	ksRebate:null,
	sl:null
};

$(document).ready(function() {
	userRebateAdjust.loadUserRebate();
	$("#sscModeAdd").unbind("click").click(function() {
		var sscrebateValue = Number($("#sscrebateValue").val());
		if(sscrebateValue%2!=0){
			swal({ title: "提示", text: "sorry!模式值需要是偶数,请输入偶数,谢谢！",type: "warning"});
			return;
		}
		userRebateAdjust.editParam.sscrebate = sscrebateValue;
		if(userRebateAdjust.editParam.sscrebate > sscOpenUserHighest){
			swal({ title: "提示", text: "大于时时彩模式["+ sscOpenUserHighest +"]最高值了.请重新输入！",type: "warning"});
			return;
		}
		if(userRebateAdjust.editParam.sscrebate < sscOpenUserLowest){
			swal({ title: "提示", text: "小于时时彩模式["+ sscOpenUserLowest +"]最低值了.请重新输入！",type: "warning"});
			return;
		}
		if(userRebateAdjust.editParam.sscrebate == sscOpenUserHighest){
			swal({ title: "提示", text: "当前时时彩模式已经是最高值了.",type: "warning"});
			return;
		}
		
		userRebateAdjust.editParam.sscrebate += sscInterval; 
		if(userRebateAdjust.editParam.sscrebate > sscOpenUserHighest){
			userRebateAdjust.editParam.sscrebate = sscOpenUserHighest;
		}
		$("#sscrebateValue").val(userRebateAdjust.editParam.sscrebate);
	
		$("#sscTip").html('');
		$("#sscTip").append("返点"+userRebateAdjust.calculateRebateByAwardModelInterval(sscOpenUserHighest, userRebateAdjust.editParam.sscrebate, sscHighestAwardModel)+"%");
	});
	
	$("#sscModeReduction").unbind("click").click(function() {
		var sscrebateValue = Number($("#sscrebateValue").val());
		if(sscrebateValue%2!=0){
			swal({ title: "提示", text: "sorry!模式值需要是偶数,请输入偶数,谢谢！",type: "warning"});
			return;
		}
		userRebateAdjust.editParam.sscrebate = sscrebateValue;
		if(userRebateAdjust.editParam.sscrebate > sscOpenUserHighest){
			swal({ title: "提示", text: "大于时时彩模式最高值了.请重新输入！",type: "warning"});
			return;
		}
		if(userRebateAdjust.editParam.sscrebate < sscOpenUserLowest){
			swal({ title: "提示", text: "小于时时彩模式最低值了.请重新输入！",type: "warning"});
			return;
		}
		if(userRebateAdjust.editParam.sscrebate == sscOpenUserLowest){
			swal({ title: "提示", text: "当前时时彩模式已经是最低值了.",type: "warning"});
			return;
		}
		userRebateAdjust
		userRebateAdjust.editParam.sscrebate -= sscInterval;
		if(userRebateAdjust.editParam.sscrebate < sscOpenUserLowest){
			userRebateAdjust.editParam.sscrebate = sscOpenUserLowest;
		}
		$("#sscrebateValue").val(userRebateAdjust.editParam.sscrebate);
		$("#sscTip").html('');
		$("#sscTip").append("返点"+userRebateAdjust.calculateRebateByAwardModelInterval(sscOpenUserHighest, userRebateAdjust.editParam.sscrebate, sscHighestAwardModel)+"%");
	});
	
	$("#sscrebateValue").change(function(){
		var val=$(this).val();
		if(val%2!=0){
			swal({ title: "提示", text: "sorry!模式值需要是偶数,请输入偶数,谢谢！",type: "warning"});
			return;
		}
		userRebateAdjust.editParam.sscrebate = val;
		if(userRebateAdjust.editParam.sscrebate > sscOpenUserHighest){
			swal({ title: "提示", text: "大于时时彩模式最高值了.请重新输入！",type: "warning"});
			return;
		}
		if(userRebateAdjust.editParam.sscrebate < sscOpenUserLowest){
			swal({ title: "提示", text: "小于时时彩模式最低值了.请重新输入！",type: "warning"});
			return;
		}
		if(userRebateAdjust.editParam.sscrebate == sscOpenUserLowest){
			swal({ title: "提示", text: "当前时时彩模式已经是最低值了.",type: "warning"});
			return;
		}
		$("#sscTip").html('');
		$("#sscTip").append("返点"+userRebateAdjust.calculateRebateByAwardModelInterval(sscOpenUserHighest, userRebateAdjust.editParam.sscrebate, sscHighestAwardModel)+"%");
	});
	
	//快三模式事件
	$("#ksModeAdd").unbind("click").click(function() {
		var ksrebateValue = Number($("#ksrebateValue").val());
		if(ksrebateValue%2!=0){
			swal({ title: "提示", text: "sorry!模式值需要是偶数,请输入偶数,谢谢！",type: "warning"});
			return;
		}
		userRebateAdjust.editParam.ksRebate = ksrebateValue;
		if(userRebateAdjust.editParam.ksRebate > ksOpenUserHighest){
			swal({ title: "提示", text: "大于快三模式["+ ksOpenUserHighest +"]最高值了.请重新输入！",type: "warning"});
			return;
		}
		if(userRebateAdjust.editParam.ksRebate < ksOpenUserLowest){
			swal({ title: "提示", text: "小于快三模式["+ ksOpenUserLowest +"]最低值了.请重新输入！",type: "warning"});
			return;
		}
		if(userRebateAdjust.editParam.ksRebate == ksOpenUserHighest){
			swal({ title: "提示", text: "当前快三模式已经是最高值了.",type: "warning"});
			return;
		}
		
		userRebateAdjust.editParam.ksRebate += ksInterval;
		if(userRebateAdjust.editParam.ksRebate > ksOpenUserHighest){
			userRebateAdjust.editParam.ksRebate = ksOpenUserHighest;
		}
		$("#ksrebateValue").val(userRebateAdjust.editParam.ksRebate);
		$("#ksTip").html('');
		$("#ksTip").append("返点"+userRebateAdjust.calculateRebateByAwardModelInterval(ksOpenUserHighest, userRebateAdjust.editParam.ksRebate, ksHighestAwardModel)+"%");
	});
	
	$("#ksModeReduction").unbind("click").click(function() {
		var ksrebateValue = Number($("#ksrebateValue").val());
		if(ksrebateValue%2!=0){
			swal({ title: "提示", text: "sorry!模式值需要是偶数,请输入偶数,谢谢！",type: "warning"});
			return;
		}
		userRebateAdjust.editParam.ksRebate = ksrebateValue;
		if(userRebateAdjust.editParam.ksRebate > ksOpenUserHighest){
			swal({ title: "提示", text: "大于快三模式最高值了.请重新输入！",type: "warning"});
			return;
		}
		if(userRebateAdjust.editParam.ksRebate < ksOpenUserLowest){
			swal({ title: "提示", text: "小于快三模式最低值了.请重新输入！",type: "warning"});
			return;
		}
		
		
		userRebateAdjust.editParam.ksRebate -= ksInterval;
		if(userRebateAdjust.editParam.ksRebate < ksOpenUserLowest){
			userRebateAdjust.editParam.ksRebate = ksOpenUserLowest;
		}
		$("#ksrebateValue").val(userRebateAdjust.editParam.ksRebate);
		$("#ksTip").html('');
		$("#ksTip").append("返点"+userRebateAdjust.calculateRebateByAwardModelInterval(ksOpenUserHighest, userRebateAdjust.editParam.ksRebate, ksHighestAwardModel)+"%");
	});
	
	$("#ksrebateValue").change(function(){
		var val=$(this).val();
		if(val%2!=0){
			swal({ title: "提示", text: "sorry!模式值需要是偶数,请输入偶数,谢谢！",type: "warning"});
			return;
		}
		userRebateAdjust.editParam.ksRebate = val;
		if(userRebateAdjust.editParam.ksRebate > ksOpenUserHighest){
			swal({ title: "提示", text: "大于快三模式["+ ksOpenUserHighest +"]最高值了.请重新输入！",type: "warning"});
			return;
		}
		if(userRebateAdjust.editParam.ksRebate < ksOpenUserLowest){
			swal({ title: "提示", text: "小于快三模式["+ ksOpenUserLowest +"]最低值了.请重新输入！",type: "warning"});
			return;
		}
		$("#ksTip").html('');
		$("#ksTip").append("返点"+userRebateAdjust.calculateRebateByAwardModelInterval(ksOpenUserHighest, userRebateAdjust.editParam.ksRebate, ksHighestAwardModel)+"%");
	});
	
	//十一选五模式事件
	$("#syxwModeAdd").unbind("click").click(function() {
		var syxwrebateValue = Number($("#syxwrebateValue").val());
		/*if(syxwrebateValue%2!=0){
			userRebateAdjust.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}*/
		userRebateAdjust.editParam.syxwRebate = syxwrebateValue;
		if(userRebateAdjust.editParam.syxwRebate > syxwOpenUserHighest){
			swal({ title: "提示", text: "大于十一选五模式["+ syxwOpenUserHighest +"]最高值了.请重新输入！",type: "warning"});
			return;
		}
		if(userRebateAdjust.editParam.syxwRebate < syxwOpenUserLowest){
			swal({ title: "提示", text: "小于十一选五模式["+ syxwOpenUserLowest +"]最低值了.请重新输入！",type: "warning"});
			return;
		}
		if(userRebateAdjust.editParam.syxwRebate == syxwOpenUserHighest){
			swal({ title: "提示", text: "当前十一选五模式已经是最高值了.",type: "warning"});
			return;
		}
		
		userRebateAdjust.editParam.syxwRebate += syxwInterval; 
		if(userRebateAdjust.editParam.syxwRebate > syxwOpenUserHighest){
			userRebateAdjust.editParam.syxwRebate = syxwOpenUserHighest;
		}
		$("#syxwrebateValue").val(userRebateAdjust.editParam.syxwRebate);
		$("#syxwTip").html('');
		$("#syxwTip").append("返点"+userRebateAdjust.calculateRebateByAwardModelInterval(syxwOpenUserHighest, userRebateAdjust.editParam.syxwRebate, syxwHighestAwardModel)+"%");
	});
	
	$("#syxwModeReduction").unbind("click").click(function() {
		var syxwrebateValue = Number($("#syxwrebateValue").val());
		/*if(syxwrebateValue%2!=0){
			userRebateAdjust.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}*/
		userRebateAdjust.editParam.syxwRebate = syxwrebateValue;
		if(userRebateAdjust.editParam.syxwRebate > syxwOpenUserHighest){
			swal({ title: "提示", text: "大于十一选五模式最高值了.请重新输入！",type: "warning"});
			return;
		}
		if(userRebateAdjust.editParam.syxwRebate < syxwOpenUserLowest){
			swal({ title: "提示", text: "小于十一选五模式最低值了.请重新输入！",type: "warning"});
			return;
		}
		if(userRebateAdjust.editParam.syxwRebate == syxwOpenUserLowest){
			swal({ title: "提示", text: "当前十一选五模式已经是最低值了.",type: "warning"});
			return;
		}
		
		userRebateAdjust.editParam.syxwRebate -= syxwInterval;
		if(userRebateAdjust.editParam.syxwRebate < syxwOpenUserLowest){
			userRebateAdjust.editParam.syxwRebate = syxwOpenUserLowest;
		}
		$("#syxwrebateValue").val(userRebateAdjust.editParam.syxwRebate);
		$("#syxwTip").html('');
		$("#syxwTip").append("返点"+userRebateAdjust.calculateRebateByAwardModelInterval(syxwOpenUserHighest, userRebateAdjust.editParam.syxwRebate, syxwHighestAwardModel)+"%");
	});
	
	$("#syxwrebateValue").change(function(){
		var val=$(this).val();
		/*if(val%2!=0){
			userRebateAdjust.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}*/
		userRebateAdjust.editParam.syxwRebate = val;
		if(userRebateAdjust.editParam.syxwRebate > syxwOpenUserHighest){
			swal({ title: "提示", text: "大于十一选五模式["+ syxwOpenUserHighest +"]最高值了.请重新输入！",type: "warning"});
			return;
		}
		if(userRebateAdjust.editParam.syxwRebate < syxwOpenUserLowest){
			swal({ title: "提示", text: "小于十一选五模式["+ syxwOpenUserLowest +"]最低值了.请重新输入！",type: "warning"});
			return;
		}
		$("#syxwTip").html('');
		$("#syxwTip").append("返点"+userRebateAdjust.calculateRebateByAwardModelInterval(syxwOpenUserHighest, userRebateAdjust.editParam.syxwRebate, syxwHighestAwardModel)+"%");
	});
	
	//pk10模式事件
	$("#pk10ModeAdd").unbind("click").click(function() {
		var pk10rebateValue = Number($("#pk10rebateValue").val());
		if(pk10rebateValue%2!=0){
			swal({ title: "提示", text: "sorry!模式值需要是偶数,请输入偶数,谢谢！",type: "warning"});
			return;
		}
		userRebateAdjust.editParam.pk10Rebate = pk10rebateValue;
		if(userRebateAdjust.editParam.pk10Rebate > pk10OpenUserHighest){
			swal({ title: "提示", text: "大于PK10模式["+ pk10OpenUserHighest +"]最高值了.请重新输入！",type: "warning"});
			return;
		}
		if(userRebateAdjust.editParam.pk10Rebate < pk10OpenUserLowest){
			swal({ title: "提示", text: "小于PK10模式["+ pk10OpenUserLowest +"]最低值了.请重新输入！",type: "warning"});
			return;
		}
		if(userRebateAdjust.editParam.pk10Rebate == pk10OpenUserHighest){
			swal({ title: "提示", text: "当前PK10模式已经是最高值了.",type: "warning"});
			return;
		}
		
		userRebateAdjust.editParam.pk10Rebate += pk10Interval; 
		if(userRebateAdjust.editParam.pk10Rebate > pk10OpenUserHighest){
			userRebateAdjust.editParam.pk10Rebate = pk10OpenUserHighest;
		}
		$("#pk10rebateValue").val(userRebateAdjust.editParam.pk10Rebate);
		$("#pk10Tip").html('');
		$("#pk10Tip").append("返点"+userRebateAdjust.calculateRebateByAwardModelInterval(pk10OpenUserHighest, userRebateAdjust.editParam.pk10Rebate, pk10HighestAwardModel)+"%");
	});
	
	$("#pk10ModeReduction").unbind("click").click(function() {
		var pk10rebateValue = Number($("#pk10rebateValue").val());
		if(pk10rebateValue%2!=0){
			swal({ title: "提示", text: "sorry!模式值需要是偶数,请输入偶数,谢谢！",type: "warning"});
			return;
		}
		userRebateAdjust.editParam.pk10Rebate = pk10rebateValue;
		if(userRebateAdjust.editParam.pk10Rebate > pk10OpenUserHighest){
			swal({ title: "提示", text: "大于PK10模式最高值了.请重新输入！",type: "warning"});
			return;
		}
		if(userRebateAdjust.editParam.pk10Rebate < pk10OpenUserLowest){
			swal({ title: "提示", text: "小于PK10模式最低值了.请重新输入！",type: "warning"});
			return;
		}
		if(userRebateAdjust.editParam.pk10Rebate == pk10OpenUserLowest){
			swal({ title: "提示", text: "当前PK10模式已经是最低值了.",type: "warning"});
			return;
		}
		
		userRebateAdjust.editParam.pk10Rebate -= pk10Interval;
		if(userRebateAdjust.editParam.pk10Rebate < pk10OpenUserLowest){
			userRebateAdjust.editParam.pk10Rebate = pk10OpenUserLowest;
		}
		$("#pk10rebateValue").val(userRebateAdjust.editParam.pk10Rebate);
		$("#pk10Tip").html('');
		$("#pk10Tip").append("返点"+userRebateAdjust.calculateRebateByAwardModelInterval(pk10OpenUserHighest, userRebateAdjust.editParam.pk10Rebate, pk10HighestAwardModel)+"%");
	});
	
	$("#pk10rebateValue").change(function(){
		var val=$(this).val();
		if(val%2!=0){
			swal({ title: "提示", text: "sorry!模式值需要是偶数,请输入偶数,谢谢！",type: "warning"});
			return;
		}
		userRebateAdjust.editParam.pk10Rebate = val;
		if(userRebateAdjust.editParam.pk10Rebate > pk10OpenUserHighest){
			swal({ title: "提示", text: "大于PK10模式["+ pk10OpenUserHighest +"]最高值了.请重新输入！",type: "warning"});
			return;
		}
		if(userRebateAdjust.editParam.pk10Rebate < pk10OpenUserLowest){
			swal({ title: "提示", text: "小于PK10模式["+ pk10OpenUserLowest +"]最低值了.请重新输入！",type: "warning"});
			return;
		}
		$("#pk10Tip").html('');
		$("#pk10Tip").append("返点"+userRebateAdjust.calculateRebateByAwardModelInterval(pk10OpenUserHighest, userRebateAdjust.editParam.pk10Rebate, pk10HighestAwardModel)+"%");
	});
	
	//低频彩模式事件
	$("#dpcModeAdd").unbind("click").click(function() {
		var dpcrebateValue = Number($("#dpcrebateValue").val());
		if(dpcrebateValue%2!=0){
			swal({ title: "提示", text: "sorry!模式值需要是偶数,请输入偶数,谢谢！",type: "warning"});
			return;
		}
		userRebateAdjust.editParam.dpcRebate = dpcrebateValue;
		if(userRebateAdjust.editParam.dpcRebate > dpcOpenUserHighest){
			swal({ title: "提示", text: "大于低频彩模式["+ dpcOpenUserHighest +"]最高值了.请重新输入！",type: "warning"});
			return;
		}
		if(userRebateAdjust.editParam.dpcRebate < dpcOpenUserLowest){
			swal({ title: "提示", text: "小于低频彩模式["+ dpcOpenUserLowest +"]最低值了.请重新输入！",type: "warning"});
			return;
		}
		if(userRebateAdjust.editParam.dpcRebate == dpcOpenUserHighest){
			swal({ title: "提示", text: "当前低频彩模式已经是最高值了.",type: "warning"});
			return;
		}
		
		userRebateAdjust.editParam.dpcRebate += dpcInterval; 
		if(userRebateAdjust.editParam.dpcRebate > dpcOpenUserHighest){
			userRebateAdjust.editParam.dpcRebate = dpcOpenUserHighest ;
		}
		$("#dpcrebateValue").val(userRebateAdjust.editParam.dpcRebate);
		$("#dpcTip").html('');
		$("#dpcTip").append("返点"+userRebateAdjust.calculateRebateByAwardModelInterval(dpcOpenUserHighest,userRebateAdjust.editParam.dpcRebate, dpcHighestAwardModel)+"%");
	});
	
	//
	$("#dpcModeReduction").unbind("click").click(function() {
		var dpcrebateValue = Number($("#dpcrebateValue").val());
		if(dpcrebateValue%2!=0){
			swal({ title: "提示", text: "sorry!模式值需要是偶数,请输入偶数,谢谢！",type: "warning"});
			return;
		}
		userRebateAdjust.editParam.dpcRebate = dpcrebateValue;
		if(userRebateAdjust.editParam.dpcRebate > dpcOpenUserHighest){
			swal({ title: "提示", text: "大于低频彩模式最高值了.请重新输入！",type: "warning"});
			return;
		}
		if(userRebateAdjust.editParam.dpcRebate < dpcOpenUserLowest){
			swal({ title: "提示", text: "小于低频彩模式最低值了.请重新输入！",type: "warning"});
			return;
		}
		if(userRebateAdjust.editParam.dpcRebate == dpcOpenUserLowest){
			swal({ title: "提示", text: "当前低频彩模式已经是最低值了.",type: "warning"});
			return;
		}
		
		userRebateAdjust.editParam.dpcRebate -= dpcInterval;
		$("#dpcrebateValue").val(userRebateAdjust.editParam.dpcRebate);
		$("#dpcTip").html('');
		$("#dpcTip").append("返点"+userRebateAdjust.calculateRebateByAwardModelInterval(dpcOpenUserHighest,userRebateAdjust.editParam.dpcRebate, dpcHighestAwardModel)+"%");
	});
	
	$("#dpcrebateValue").change(function(){
		var val=$(this).val();
		if(val%2!=0){
			swal({ title: "提示", text: "sorry!模式值需要是偶数,请输入偶数,谢谢！",type: "warning"});
			return;
		}
		userRebateAdjust.editParam.dpcRebate = val;
		if(userRebateAdjust.editParam.dpcRebate > dpcOpenUserHighest){
			swal({ title: "提示", text: "大于低频彩模式["+ dpcOpenUserHighest +"]最高值了.请重新输入！",type: "warning"});
			return;
		}
		if(userRebateAdjust.editParam.dpcRebate < dpcOpenUserLowest){
			swal({ title: "提示", text: "小于低频彩模式["+ dpcOpenUserLowest +"]最低值了.请重新输入！",type: "warning"});
			return;
		}
		$("#dpcTip").html('');
		$("#dpcTip").append("返点"+userRebateAdjust.calculateRebateByAwardModelInterval(dpcOpenUserHighest,userRebateAdjust.editParam.dpcRebate, dpcHighestAwardModel)+"%");
	});
	$("input[name='box']").on("ifChanged",function(){
		//未选中的复选框改为开启状态
		$("input[name='box']:checked").each(function(i,idx){
			var id=$(idx).val();
			$("input[id^='"+id+"']").removeAttr("disabled");
			$("button[id^='"+id+"']").removeAttr("disabled");
		})
		//未选中的复选框改为禁止状态
		$($("input[name='box']").not("input:checked")).each(function(i,idx){
			var id=$(idx).val();
			$("input[id^='"+id+"']").attr("disabled",true);
			$("button[id^='"+id+"']").attr("disabled",true);
		});
	});
});

/**
 * 获取本系统的最高最低模式
 */
UserRebateAdjust.prototype.loadUserRebate = function(){
	managerUserRegisterAction.loadUserRebate(function(r){
		if (r[0] != null && r[0] == "ok") {
			sscOpenUserLowest=r[1].sscLowestAwardModel;
			syxwOpenUserLowest=r[1].syxwLowestAwardModel;
			dpcOpenUserLowest=r[1].dpcLowestAwardModel;
			pk10OpenUserLowest=r[1].pk10LowestAwardModel;
			ksOpenUserLowest=r[1].ksLowestAwardModel;
		}else if(r[0] != null && r[0] == "error"){
			//加载为空不处理
		}else{
			swal("加载用户配额信息请求失败.");
		}
	});
}

/**
 * 加载数据
 */
UserRebateAdjust.prototype.queryUser=function(){
	var bizSystem=$("#bizSystem").val();
	var userName=$("#userName").val();
	
	if(bizSystem!=null&&bizSystem!=""){
		userRebateAdjust.editParam.bizSystem=bizSystem;
	}else{
		userRebateAdjust.editParam.bizSystem=null;
	}
	if(userName!=null&&userName!=""){
		userRebateAdjust.editParam.userName=userName;
	}else{
		userRebateAdjust.editParam.userName=null;
		swal("用户名不能为空");
		return;
	}
	managerUserAction.queryUserLowerNumberByName(userRebateAdjust.editParam,function(r){
		if(r[0]=="ok"){
			$("#sl").text("共查询到所有下级会员"+r[2]+"个");
			userRebateAdjust.editParam.sl=r[2];
			userRebateAdjust.showRegisterDetail(r[1]);
			userRebateAdjust.editParam.bizSystem=r[1].bizSystem;
			$("#addExtendLinkButton").removeAttr("disabled");
		}else{
			swal({ title: "提示", text: ""+r[1],type: "warning"});
			$($("input[name='box']")).each(function(i,idx){
				var id=$(idx).val();
				$("input[id^='"+id+"']").attr("disabled",true);
				$("input[id^='"+id+"']").val("");
				$(this).iCheck('uncheck');
				$("button[id^='"+id+"']").attr("disabled",true);
			});
			userRebateAdjust.editParam.userName=null;
			userRebateAdjust.editParam.bizSystem=null;
			$("#addExtendLinkButton").attr("disabled","disabled");
		}
	});
}

/**
 * 展示奖金模式详情的信息
 */
UserRebateAdjust.prototype.showRegisterDetail = function(user) {
	$("#sscrebateValue").val(user.sscRebate);
	$("#syxwrebateValue").val(user.syxwRebate);
	$("#ksrebateValue").val(user.ksRebate);
	$("#pk10rebateValue").val(user.pk10Rebate);
	$("#dpcrebateValue").val(user.dpcRebate);
	sscOpenUserHighest =user.sscRebate;
	syxwOpenUserHighest =user.syxwRebate; 
	dpcOpenUserHighest =user.dpcRebate;
	pk10OpenUserHighest =user.pk10Rebate;
	ksOpenUserHighest =user.ksRebate;
};

/**
 * 修改奖金模式
 */
UserRebateAdjust.prototype.updateUserAllLowerLevelsByName=function(){
	if(userRebateAdjust.editParam.bizSystem==null||userRebateAdjust.editParam.userName==null){
		swal("请先查询上级用户");
		return;
	}
	$("input[name='box']:checked").each(function(i,idx){
		var id=$(idx).val();
		var rebateValue=$("input[id^='"+id+"']").val();
		if(rebateValue==""){
			swal("模式值不能为空");
			return;
		}
		if(id=="ssc"){
			userRebateAdjust.editParam.sscRebate=rebateValue;
		}
		if(id=="syxw"){
			userRebateAdjust.editParam.syxwRebate=rebateValue;
		}
		if(id=="ks"){
			userRebateAdjust.editParam.ksRebate=rebateValue;
		}
		if(id=="pk10"){
			userRebateAdjust.editParam.pk10Rebate=rebateValue;
		}
		if(id=="dpc"){
			userRebateAdjust.editParam.dpcRebate=rebateValue;
		}
	})
	$($("input[name='box']").not("input:checked")).each(function(i,idx){
		var id=$(idx).val();
		if(id=="ssc"){
			userRebateAdjust.editParam.sscRebate=null;
		}
		if(id=="syxw"){
			userRebateAdjust.editParam.syxwRebate=null;
		}
		if(id=="ks"){
			userRebateAdjust.editParam.ksRebate=null;
		}
		if(id=="pk10"){
			userRebateAdjust.editParam.pk10Rebate=null;
		}
		if(id=="dpc"){
			userRebateAdjust.editParam.dpcRebate=null;
		}
	});
	userRebateAdjust.editParam.code=1;
	if($("input[name='box']:checked").length>0){
		managerUserAction.updateUserAllLowerLevelsByName(userRebateAdjust.editParam,function(r){
			if(r[0]=="ok"){
				swal({ title: "提示", text: "修改成功,共调整了"+r[1]+"会员的配额",type: "success"});
			}else{
				swal(r[1]);
			}
		});
	}
}



/**
 * 计算用户自身保留返点
 */
UserRebateAdjust.prototype.calculateRebateByAwardModelInterval = function(highRebate, lowRebate, highestAwardModel) {
	var rebate = (highRebate - lowRebate)/highestAwardModel;
	rebate = (rebate * 100).toFixed(2);
	return rebate;
}