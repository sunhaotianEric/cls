function UserPointPage(){}
var userPoint= new UserPointPage();

userPoint.param = {
   userId : null
};

$(document).ready(function() {
	userPoint.getUser(userPoint.param.userId);
});

/**
 * 获取用户
 */
UserPointPage.prototype.getUser = function(userId){
	if(userId == null || userId == ""){
		alert("参数传递错误.");
		return;
	}
	managerUserAction.getUser(userId,function(r){
		if (r[0] != null && r[0] == "ok") {
			userPoint.showUserMsg(r[1]);
		}else if (r[0] != null && r[0] == "error") {
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "获取用户信息请求失败.");
		}
    });
};

/**
 * 显示用户信息
 */
UserPointPage.prototype.showUserMsg = function(userMsg){
	$("#username").text(userMsg.username);
	$("#userPoint").text(userMsg.point);
	
	if(userMsg.dailiLevel == 'DIRECTAGENCY'){ //如果是直属代理的话,则可增加推广费用的功能
		var obj=document.getElementById("operationType");
		obj.options.add(new Option("增加推广积分","addExtend"));
		obj.options.add(new Option("减少推广积分","reduceExtend"));
	}
};


/**
 * 更新用户的积分数据
 */
UserPointPage.prototype.userPoint = function(){
	var moneyCount = $("#pointCount").val();
	var operationType = $("#operationType").val();
	if(moneyCount == null || moneyCount == ""){
		alert("积分不能为空的哦");
		return;
	}
	if(moneyCount <= 0){
		alert("金额输入不合法.");
		return
	}
	if(operationType == null || operationType == ""){
		alert("请选择是要续费还是扣费.");
		return;
	}
	
	var updateParam = {};
	updateParam.id = userPoint.param.userId;
	updateParam.operationType = operationType;
	updateParam.operationValue = moneyCount;
	managerUserAction.userPoint(updateParam,function(r){
		if (r[0] != null && r[0] == "ok") {
			alert("操作成功.");
			window.location.href= contextPath + '/managerzaizst/user/user_list.html';
		}else if (r[0] != null && r[0] == "error") {
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "用户续费请求失败.");
		}
    });
};

