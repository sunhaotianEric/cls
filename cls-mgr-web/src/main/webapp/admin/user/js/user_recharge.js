function UserRechargePage(){}
var userRechargePage = new UserRechargePage();

/**
 * 查询参数
 */
userRechargePage.param = {
	
};

$(document).ready(function(){
	$(".i-checks").iCheck({
		checkboxClass:"icheckbox_square-green",
		radioClass:"iradio_square-green",
	})
});
UserRechargePage.prototype.renewTreatment=function(){
	var bizSystem=$("#bizSystem").val();
	var userName=$("#userName").val();
	var moneyCount = $("#moneyCount").val();
	var operationType = $("input[type='radio']:checked").val()
	var description=$("#description").val();
	var operatePassword = $("#operatePassword").val();
	if(currentUser.bizSystem=='SUPER_SYSTEM'){
	if(bizSystem == null || bizSystem == ""){
		swal("请选择业务系统.");
		return;
	}
	}
	if(userName == null || userName == ""){
		swal("会员名不能为空.");
		return;
	}
	if(moneyCount == null || moneyCount == ""){
		swal("金额不能为空的哦");
		return;
	}
	if(moneyCount <= 0){
		swal("金额输入不合法.");
		return;
	}
	if(isNaN(moneyCount)){
		swal("金额只能为数字");
		return;
	}
	if(operationType == null || operationType == ""){
		swal("请选择是要续费还是扣费.");
		return;
	}
	if(operatePassword == null || operatePassword == ""){
		swal("请输入操作密码"); 
		return;
	}
	if(userName.charAt(userName.length-1,userName.length)==","){
		userName=userName.substring(0,userName.length-1);
	}
	userName.replace("，",",");
	userName.replace(new RegExp(',+',"gm"),',');
	var ary=userName;
	for (var int = 0; int < userName.split(",").length; int++) {
		if(ary.replace(userName.split(",")[int]+",","").indexOf(userName.split(",")[int]+",")>-1&&userName.split(",")[int]!=""){
			swal("存在重复的会员账号，请重新输入");
			return;
		}
	}
	   swal({
           title: "确认执行此操作吗？",
           text: "请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "确定",
           closeOnConfirm: false
       },
       function() {	
    	
		var updateParam = {};
		updateParam.bizSystem=bizSystem;
		updateParam.userName=userName;
		updateParam.operationType = operationType;
		updateParam.operationValue = moneyCount;
		updateParam.description=description;
		managerUserAction.userRenewList(updateParam, operatePassword,description, function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "操作成功",type: "success"},
						 function() {
					 /*var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
		               parent.userList.closeLayer(index);*/
			   	    }	
				  );
				 
			}else if (r[0] != null && r[0] == "error") {
				swal(r[1]);
			}else{
				swal("用户续费请求失败.");
			}
	    });
	});
}
