function UserDayCommissionReportPage(){}
var userDayCommissionReportPage = new UserDayCommissionReportPage();

/**
 * 查询参数
 */
userDayCommissionReportPage.queryParam = {
		teamLeaderName : null,
		createdDateStart : null,
		createdDateEnd : null
};



$(document).ready(function() {
	$("#createdDateStart").val(new Date().AddDays(-1).setDayStartTime().format("yyyy-MM-dd hh:mm:ss"));
	$("#createdDateEnd").val(new Date().AddDays(-1).setDayEndTime().format("yyyy-MM-dd hh:mm:ss"));
});

/**
 * 按页面条件查询数据
 */
UserDayCommissionReportPage.prototype.findUserDayOrderNumByQueryParam = function(){
	userDayCommissionReportPage.queryParam.teamLeaderName = getSearchVal("userName");
	var startimeStr = $("#createdDateStart").val();
	var endtimeStr = $("#createdDateEnd").val();
	if(startimeStr != ""){
		userDayCommissionReportPage.queryParam.createdDateStart = startimeStr.stringToDate();
	}
	if(endtimeStr != ""){
		userDayCommissionReportPage.queryParam.createdDateEnd = endtimeStr.stringToDate();
	}
	
	if(userDayCommissionReportPage.queryParam.createdDateStart!=null && $.trim(startimeStr)==""){
		userDayCommissionReportPage.queryParam.createdDateStart = null;
	}
	if(userDayCommissionReportPage.queryParam.createdDateEnd!=null && $.trim(endtimeStr)==""){
		userDayCommissionReportPage.queryParam.createdDateEnd = null;
	}
	
	//投注明细列表
	$("#reportList").html("<tr><td colspan='3'>正在加载中......</td></tr>");
	$("#reportList2").html("<tr><td colspan='3'>正在加载中......</td></tr>");
	userDayCommissionReportPage.queryConditionUserDayCommission(userDayCommissionReportPage.queryParam);
};

var afterNumberLotteryWindow;

/**
 * 刷新列表数据
 */
UserDayCommissionReportPage.prototype.refreshUserDayCommissionReportPages = function(res1, res2){
	var str = "";
	var totalMoney = 0;
	
	var userDayCommissionListObj = $("#reportList");
	userDayCommissionListObj.html("");
    if(isEmpty(res1) || res1.length == 0){
    	str += "<tr>";
    	str += "  <td colspan='3'>没有符合条件的记录！</td>";
    	str += "</tr>";
    	userDayCommissionListObj.append(str);
    	str = "";
    }else{
    	//记录数据显示
    	for(var i = 0 ; i < res1.length; i++){
    		var userOrder = res1[i];
    		totalMoney += userOrder.money;
    		str += "<tr>";
    		str += "  <td>"+ userOrder.userName +"</td>";
    		str += "  <td>"+ userOrder.baseValue.toFixed(commonPage.param.fixVaue) +"</td>";
    		str += "  <td>"+ userOrder.money.toFixed(commonPage.param.fixVaue) +"</td>";
    		str += "</tr>";
    		userDayCommissionListObj.append(str);
    		str = "";
    	}
    }
    var userDayCommissionListObj2 = $("#reportList2");
	userDayCommissionListObj2.html("");
    if(isEmpty(res2) || res2.length == 0){
    	str += "<tr>";
    	str += "  <td colspan='3'>没有符合条件的记录！</td>";
    	str += "</tr>";
    	userDayCommissionListObj2.append(str);
    	str = "";
    }else{
    	//记录数据显示
    	for(var i = 0 ; i < res2.length; i++){
    		var userOrder = res2[i];
    		totalMoney += userOrder.money;
    		str += "<tr>";
    		str += "  <td>"+ userOrder.userName +"</td>";
    		str += "  <td>"+ userOrder.baseValue.toFixed(commonPage.param.fixVaue) +"</td>";
    		str += "  <td>"+ userOrder.money.toFixed(commonPage.param.fixVaue) +"</td>";
    		str += "</tr>";
    		userDayCommissionListObj2.append(str);
    		str = "";
    	}
    }
    //总统计
	str += "<tr style='color:red'>";
	str += "  <td>总统计</td>";	
	str += "  <td></td>";
	str += "  <td>"+ totalMoney.toFixed(commonPage.param.fixVaue) +"</td>";
	str += "</tr>";
	userDayCommissionListObj2.append(str);
	str = "";
    
};



/**
 * 条件查询投注记录
 */
UserDayCommissionReportPage.prototype.queryConditionUserDayCommission = function(queryParam){
	managerOrderAction.getUserDayCommissionByQuery(queryParam,function(r){
		if (r[0] != null && r[0] == "ok") {
			userDayCommissionReportPage.refreshUserDayCommissionReportPages(r[1], r[2]);
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询投注记录数据失败.");
		}
    });
};

