function UserRenewPage(){}
var userRenew= new UserRenewPage();

userRenew.param = {
   userId : null
};

$(document).ready(function() {
	userRenew.getUser(userRenew.param.userId);
	$("#explain p").hide();
	$("#explain p#all").show();
	$("#operationType").on("change",function(){
		$("#explain p").hide();
		var operationType = $("#operationType").val();
		if(operationType=="SYSTEM_ADD_MONEY"){
			$("#explain p#SYSTEM_ADD_MONEY").show();
		}else if(operationType=="RECHARGE_MANUAL"){
			$("#explain p#RECHARGE_MANUAL").show();
		}else if(operationType=="MANAGE_REDUCE_MONEY"){
			$("#explain p#MANAGE_REDUCE_MONEY").show();
		}else if(operationType=="SYSTEM_REDUCE_MONEY"){
			$("#explain p#SYSTEM_REDUCE_MONEY").show();
		}else if(operationType=="RECHARGE_PRESENT"){
			$("#explain p#RECHARGE_PRESENT").show();
		}else if(operationType=="ACTIVITIES_MONEY"){
			$("#explain p#ACTIVITIES_MONEY").show();
		}else if(operationType=="SYSTEM_REDUCE_MONEY_FOR_RECHARGE_PRESENT"){
			$("#explain p#SYSTEM_REDUCE_MONEY_FOR_RECHARGE_PRESENT").show();
		}else if(operationType=="SYSTEM_REDUCE_MONEY_FOR_ACTIVITIES"){
			$("#explain p#SYSTEM_REDUCE_MONEY_FOR_ACTIVITIES").show();
		}else{
			$("#explain p#all").show();
		}
	});
});

/**
 * 获取用户
 */
UserRenewPage.prototype.getUser = function(userId){
	if(userId == null || userId == ""){
		swal("参数传递错误.");
		return;
	}
	managerUserAction.getUser(userId,function(r){
		if (r[0] != null && r[0] == "ok") {
			userRenew.showUserMsg(r[1]);
		}else if (r[0] != null && r[0] == "error") {
			swal(r[1]);
		}else{
			swal("获取用户信息请求失败.");
		}
    });
};

/**
 * 显示用户信息
 */
UserRenewPage.prototype.showUserMsg = function(userMsg){
	$("#username").text(userMsg.userName);
	$("#userMoney").text(userMsg.canWithdrawMoney);
	$("#iceMoney").text(userMsg.iceMoney);
	$("#money").text(userMsg.money);
	
//	if(userMsg.dailiLevel == 'DIRECTAGENCY'){ //如果是直属代理的话,则可增加推广费用的功能
//		var obj=document.getElementById("operationType");
//		obj.options.add(new Option("增加推广金额","addExtend"));
//		obj.options.add(new Option("减少推广金额","reduceExtend"));
//	}
};


/**
 * 更新用户的余额数据
 */
UserRenewPage.prototype.userRenew = function(){
	var moneyCount = $("#moneyCount").val();
	var operationType = $("#operationType").val();
	var operatePassword = $("#operatePassword").val();
	var description=$("#description").val();
	if(moneyCount == null || moneyCount == ""){
		swal("金额不能为空的哦");
		return;
	}
	if(moneyCount <= 0){
		swal("金额输入不合法.");
		return
	}
	if(operationType == null || operationType == ""){
		swal("请选择是要续费还是扣费.");
		return;
	}
	if(operatePassword == null || operatePassword == ""){
		swal("请输入操作密码"); 
		return;
	}
	   swal({
           title: "确认执行此操作吗？",
           text: "请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "确定",
           closeOnConfirm: false
       },
       function() {
	   let id = parseInt($(".confirm").attr("tabindex"))
	   $(".confirm").attr("tabindex",2)
	   if(id !==1){
		   return
	   }
    	   
		var updateParam = {};
		updateParam.id = userRenew.param.userId;
		updateParam.operationType = operationType;
		updateParam.operationValue = moneyCount;
		managerUserAction.userRenew(updateParam, operatePassword,description, function(r){
			  $(".confirm").attr("tabindex",1)
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "操作成功",type: "success"},
						 function() {
					 var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
		               parent.userList.closeLayer(index);
			   	    }	
				  );
				 
			}else if (r[0] != null && r[0] == "error") {
				swal(r[1]);
			}else{
				swal("用户续费请求失败.");
			}
	    });
	});
};

