function UserBankListPage(){}
var userBankListPage = new UserBankListPage();

userBankListPage.param = {
   userBankId : null
};

//分页参数
userBankListPage.pageParam = {
		    pageNo : 1,
		    pageSize : defaultPageSize
};
/**
 * 查询参数
 */
userBankListPage.queryParam = {
	bizSystem : null,
	userName : null,
	bankCardNum : null,
	bankAccountName : null,
	belongDateStart:null
};

$(document).ready(function() {
//	if(currentUser.bizSystem=='SUPER_SYSTEM'){
//		userBankListPage.getallbizSystem('bizSystem');	
//	 }
	userBankListPage.initTableData();
});

UserBankListPage.prototype.initTableData = function(){

	userBankListPage.queryParam = getFormObj($("#userBankForm"));
	userBankListPage.table = $('#userBankTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"ajax":function (data, callback, settings) {
			userBankListPage.pageParam.pageSize = data.length;//页面显示记录条数
			userBankListPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerUserAction.queryUserBanks(userBankListPage.queryParam,userBankListPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询用户银行卡数据失败.");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": "bizSystemName","bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "userName",
		            	render: function(data, type, row, meta) {	
		            		return "<a href='javascript:void(0)' style='color:blue;text-decoration:underline;'  onclick='commonPage.showUserMsgPage(\""+data+"\","+row.userId+")'>"+ row.userName +"</a>";
	            		}
		            },
		            {"data": "bankName"},
		            {"data": "bankAccountName"},
		            {"data": "subbranchName",render: function(data, type, row, meta) {
	            		 var str="";
		             		if(data=="" || data==null){
		             			 str="未填写";
		             		}else{
		             			 str=data;
		             		}
		             		 return str ;
		                 }
		            },
		            {"data": "bankCardNum"},
		            {"data": "province"},
		            {"data": "city"},
		            {"data": "area",
		            	render: function(data, type, row, meta) {
	            		 var str="";
		             		if(data=="" || data==null){
		             			 str="未填写";
		             		}else{
		             			 str=data;
		             		}
		             		 return str ;
		                 }
		            },
		            {"data": "createDateStr"},
					{"data": "locked",
		            	 render: function(data, type, row, meta) {
		            		 var str="";
		             		if(data==1){
		             			 str="锁定";
		             		}else if(data==0){
		             			 str="未锁定";
		             		}
		             		 return str ;
		                 }
                    },
		            {
	                	 "data": "id",
	                	 render: function(data, type, row, meta) {
	                		 var str="";
	                		 if(row.locked==1){
	                			 str+="<a class='btn btn-warning btn-sm btn-primary' onclick='userBankListPage.lockStateCard("+data+","+"0)'><i class='fa fa-unlock' ></i>&nbsp;解锁</a>&nbsp;";
	                		 }else if(row.locked==0){
	                			 str+="<a class='btn btn-sm btn-sm btn-primary' onclick='userBankListPage.lockStateCard("+data+","+"1)'><i class='fa fa-lock' ></i>&nbsp;锁定</a>&nbsp;";
	                		 }
	                		 str+="<a class='btn btn-danger btn-sm btn-del' onclick='userBankListPage.delBankCard("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>&nbsp;";
	                		 return str ;
	                	 }
	                }
	            ]
	}).api(); 
};
/**
 * 加载所有的登陆日志数据
 */
UserBankListPage.prototype.getTotalUserBanksForQuery = function(){
	var bizSystem = $("#bizSystem").val();
	var bankNum = $("#bankNum").val();
	var bankAccount = $("#bankAccount").val();
	var userName = $("#userName").val();
	var belongDateStart=$("#belongDateStart").val();
	if(bizSystem == ""){
		userBankListPage.queryParam.bizSystem = null;
	}else{
		userBankListPage.queryParam.bizSystem = bizSystem;
	}
	if(bankNum == ""){
		userBankListPage.queryParam.bankCardNum = null;
	}else{
		userBankListPage.queryParam.bankCardNum = bankNum;
	}
	if(bankAccount == ""){
		userBankListPage.queryParam.bankAccountName = null;
	}else{
		userBankListPage.queryParam.bankAccountName = bankAccount;
	}
	if(userName == ""){
		userBankListPage.queryParam.userName = null;
	}else{
		userBankListPage.queryParam.userName = userName;
	}
	if(belongDateStart!=""){
		userBankListPage.queryParam.belongDateStart=new Date(belongDateStart);
	}else{
		userBankListPage.queryParam.belongDateStart=null;
	}
	userBankListPage.table.ajax.reload();
};

/**
 * 查找资金明细
 */
UserBankListPage.prototype.queryTotalUserBanks = function(queryParam,pageNo){
	managerUserAction.queryUserBanks(queryParam,pageNo,function(r){
		if (r[0] != null && r[0] == "ok") {
			userBankListPage.refreshUserbanksPages(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查找用户总共消费统计请求失败.");
		}
    });
};
/**
 * 确认删除银行卡数据
 */
UserBankListPage.prototype.delBankCard = function(id){
	swal({
        title: "您确定要删除银行卡数据吗",
        text: "请谨慎操作！",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        cancelButtonText: "取消",
        confirmButtonText: "删除",
        closeOnConfirm: false
    },
    function() {
    	managerUserAction.delBankCard(id,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "删除成功",type: "success"});
				userBankListPage.table.ajax.reload();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("删除银行卡数据失败.");
			}
	    });
 	   
    });
	
};
/**
 * 解锁或者锁定银行卡
 */
UserBankListPage.prototype.lockStateCard = function(id,lock){
	var str="";
	if(lock==1){
		str+="您确定要锁定银行卡吗";
	}else if(lock==0){
		str+="您确定要解锁银行卡吗";
	}
	swal({
        title: str,
        text: "请谨慎操作！",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        cancelButtonText: "取消",
        confirmButtonText: "确认",
        closeOnConfirm: false
    },
    function() {
    	managerUserAction.lockStateCard(id,lock,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal({ title: "提示", text: "操作成功",type: "success"});
				userBankListPage.table.ajax.reload();
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("删除银行卡数据失败.");
			}
	    });
 	   
    });
	
};

/**
 * 新增
 */
UserBankListPage.prototype.addUserBank=function(){
	
	 layer.open({
         type: 2,
         title: '银行卡新增',
         maxmin: false,
         shadeClose: true,
         //点击遮罩关闭层
         area: ['70%', '50%'],
         content: contextPath + "/managerzaizst/user/user_bank_list_edit.html",
         cancel: function(index){ 
        	 userBankListPage.table.ajax.reload();
      	   }
     });
}

UserBankListPage.prototype.closeLayer=function(index){
	layer.close(index);
	userBankListPage.table.ajax.reload();
}