function UserAddPage(){}
var userAdd= new UserAddPage();

userAdd.addParam = {
		
};

$(document).ready(function() {
	userAdd.getAwardModel("sscRebate");
});

/**
 * 添加新用户
 */
UserAddPage.prototype.saveUser = function(){
	
	var dailiLevel = $("#dailiLevel").val();
	var username = $("#username").val();
	var password = $("#password").val();
	var qq = $("#qq").val();
	var phone = $("#phone").val();
	var sscRebate = $("#sscRebate").val();
	var ffcRebate = $("#ffcRebate").val();
	var syxwrebate = $("#syxwrebate").val();
	var ksrebate = $("#ksrebate").val();
	var klsfrebate = $("#klsfrebate").val();	
	var dpcrebate = $("#dpcrebate").val();		
	
	if(username == null || username == ""){
		swal("用户名不能为空.");
		return;
	}

	var reg= /^[A-Za-z]+$/;
	var regUserName = /^[a-zA-Z0-9]+$/;
	if(!reg.test(username.substr(0,1)) || username.length < 4 || username.length > 16
			|| !regUserName.test(username)){
		swal("用户名输入不合法,必须填写,4~16位字母或数字，首位为字母");
		return;
	}

	if(password == null || password == ""){
		swal("密码不能为空.");
		return;
	}
	if(password.length < 6 || password.length > 15){
		swal("密码必须填写,6-15个字符，建议使用混合数字、字母大小写.");
		return;
	}
	
	if(phone == null || phone == ""){
		swal("手机号码不能为空.");
		return;
	}
	if(phone.length != 11){
		swal("手机号码长度不正确.");
		return;
	}
	if(sscRebate== null || sscRebate == ""){
		swal("奖金模式不为空.");
		return;
	}
	
	userAdd.addParam.dailiLevel = dailiLevel;
	userAdd.addParam.userName = username;
	userAdd.addParam.password = password;
	userAdd.addParam.qq = qq;
	userAdd.addParam.phone = phone;
	
	userAdd.addParam.sscRebate = sscRebate;
	userAdd.addParam.ffcRebate = ffcRebate;
	userAdd.addParam.syxwrebate = syxwrebate;
	userAdd.addParam.ksrebate = ksrebate;
	userAdd.addParam.klsfrebate = klsfrebate;
	userAdd.addParam.dpcrebate = dpcrebate;
	
	//添加用户
	managerUserAction.addUser(userAdd.addParam,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({
		           title: "保存成功",
		           text: "您已经保存了这条信息。",
		           type: "success"
		       },
		       function() {
		    	  
		    	   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
		    	   if(index!=undefined){
	                 parent.userList.closeLayer(index);
		    	   }
		   	   });
		}else if (r[0] != null && r[0] == "error") {
			swal(r[1]);
		}else{
			swal("添加用户请求失败.");
		}
    });

	
};

/**
 * 初始化奖金模式
 */
UserAddPage.prototype.getAwardModel = function(id){
	var selDom = $("#"+id);
	managerBizSystemAction.getAwardModel(function(r){
		if (r[0] != null && r[0] == "ok") {
			highestAwardModel=r[1].sscHighestAwardModel;
			lowestAwardModel=r[1].sscLowestAwardModel;
			for(var i=0;i<(highestAwardModel-lowestAwardModel);i=i+2){
				selDom.append("<option value='"+(highestAwardModel-i)+"'>"+(highestAwardModel-i)+"</option>");	
			}	
		}else if (r[0] != null && r[0] == "error") {
			swal(r[1]);
		}else{
			swal("请求失败.");
		}});
	
}
