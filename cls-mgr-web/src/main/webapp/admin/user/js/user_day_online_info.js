function UserDayOnlineInfoPage(){}
var userDayOnlineInfoPage = new UserDayOnlineInfoPage();

/**
 * 查询参数
 */
userDayOnlineInfoPage.queryParam = {
		dayOnlineInfoDateStart : null,
		dayOnlineInfoDateEnd : null,
};

//分页参数
userDayOnlineInfoPage.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize
};
userDayOnlineInfoPage.dayInfoReports = {
		loginTotalCount:0,
		pcLoginTotalCount:0,
		mobileLoginTotalCount:0,
		lotteryUserCount:0,
		pcLotteryUserCount:0,
		mobileLotteryUserCount:0
};
$(document).ready(function() {
	userDayOnlineInfoPage.initTableData();
});


/**
 * 按页面条件查询数据
 */
UserDayOnlineInfoPage.prototype.findUserDayOnlineInfoPageByQueryParam = function(){
	userDayOnlineInfoPage.queryParam={};
	var dayOnlineInfoDateStart = $("#dayOnlineInfoDateStart").val();
	var dayOnlineInfoDateEnd = $("#dayOnlineInfoDateEnd").val();
	var bizSystem = $("#bizSystem").val();
	if(dayOnlineInfoDateStart != ""){
		userDayOnlineInfoPage.queryParam.dayOnlineInfoDateStart = dayOnlineInfoDateStart.stringToDate();
	}
	if(dayOnlineInfoDateEnd != ""){
		userDayOnlineInfoPage.queryParam.dayOnlineInfoDateEnd = dayOnlineInfoDateEnd.stringToDate();
	}
	if(bizSystem != ""){
		userDayOnlineInfoPage.queryParam.bizSystem = bizSystem;
	}
	
	userDayOnlineInfoPage.table.ajax.reload();;
};

/**
 * 初始化列表数据
 */
UserDayOnlineInfoPage.prototype.initTableData = function() {
	userDayOnlineInfoPage.queryParam={};
	var dayOnlineInfoDateStart = $("#dayOnlineInfoDateStart").val();
	var dayOnlineInfoDateEnd = $("#dayOnlineInfoDateEnd").val();
	var bizSystem = $("#bizSystem").val();
	if(dayOnlineInfoDateStart != ""){
		userDayOnlineInfoPage.queryParam.dayOnlineInfoDateStart = dayOnlineInfoDateStart.stringToDate();
	}
	if(dayOnlineInfoDateEnd != ""){
		userDayOnlineInfoPage.queryParam.dayOnlineInfoDateEnd = dayOnlineInfoDateEnd.stringToDate();
	}
	if(bizSystem != ""){
		userDayOnlineInfoPage.queryParam.bizSystem = bizSystem;
	}
	
	userDayOnlineInfoPage.table = $('#userDayOnlineInfoTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"scrollX": true,
		"iDisplayLength" : defaultPageSize,
		"fnDrawCallback": function (oSettings) {  
			userDayOnlineInfoPage.getTotalRes(userDayOnlineInfoPage.dayInfoReports,"总统计");   
        },
		"ajax":function (data, callback, settings) {
			userDayOnlineInfoPage.pageParam.pageSize = data.length;//页面显示记录条数
			userDayOnlineInfoPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerUserOnlineAction.getUserDayOnlineInfoListPage(userDayOnlineInfoPage.queryParam,userDayOnlineInfoPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
					if(userDayOnlineInfoPage.dayInfoReports != null){
						userDayOnlineInfoPage.dayInfoReports = r[2];	
					}
					
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
                    {"data": "bizSystemName","bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "belongDateStr"},
		            {"data": "loginTotalCount"},
		            {"data": "pcLoginTotalCount"},
		            {"data": "mobileLoginTotalCount"},
		            {"data": "appLoginTotalCount"},
		            {"data": "lotteryUserCount"},
		            {"data": "pcLotteryUserCount"},
		            {"data": "mobileLotteryUserCount"},
		            {"data": "appLotteryUserCount"}
	            ]
	}).api(); 
}


/**
 * 进行数据统计的计算，返回tr字符串拼接
 * @param datas
 * @param totalDesc  单页统计或总统计
 */
UserDayOnlineInfoPage.prototype.getTotalRes = function(datas, totalDesc) {
	var str = "";
	str += "<tr style='color:red'>";
    str += "  <td colspan='1'>"+totalDesc+":</td>";
    str += "  <td colspan='1'></td>";
	str += " ";
	str += "  <td>"+ userDayOnlineInfoPage.dayInfoReports.loginTotalCount +"</td>";
	str += "  <td>"+ userDayOnlineInfoPage.dayInfoReports.pcLoginTotalCount +"</td>";
	str += "  <td>"+ userDayOnlineInfoPage.dayInfoReports.mobileLoginTotalCount +"</td>";
	str += "  <td>"+ userDayOnlineInfoPage.dayInfoReports.appLoginTotalCount +"</td>";
	str += "  <td>"+ userDayOnlineInfoPage.dayInfoReports.lotteryUserCount +"</td>";
	str += "  <td>"+ userDayOnlineInfoPage.dayInfoReports.pcLotteryUserCount +"</td>";
	str += "  <td>"+ userDayOnlineInfoPage.dayInfoReports.mobileLotteryUserCount +"</td>";
	str += "  <td>"+ userDayOnlineInfoPage.dayInfoReports.appLotteryUserCount +"</td>";
    str += "</tr>";
    $("#userDayOnlineInfoTable tbody").append(str);
    //return str;
}