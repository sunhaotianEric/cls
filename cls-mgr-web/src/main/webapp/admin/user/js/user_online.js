function UserOnlinePage(){}
var userOnlinePage = new UserOnlinePage();

userOnlinePage.param = {
   userOnlineList : new Array()	
};

/**
 * 查询参数
 */
userOnlinePage.queryParam = {
		teamLeaderName : null,
		userName : null,
		registerDateStart : null,
		registerDateEnd : null,
		balanceStart : null,
		balanceEnd : null,
		dailiLevel:null,
		onlineType:"PC",  //在线方式   电脑或手机
		orderSort:null,
		isOnline:true,
		isQuerySub:false   //是否同时查询下级用户
};

//分页参数
userOnlinePage.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize
};

$(document).ready(function() {
	userOnlinePage.initTableData();
});


/**
 * 按页面条件查询数据
 */
UserOnlinePage.prototype.findUserOnlinePageByQueryParam = function(){
	
	userOnlinePage.queryParam.userName = $("#teamUserListUserName").val();
	userOnlinePage.queryParam.balanceStart = $("#teamUserListBalanceStart").val();
	userOnlinePage.queryParam.balanceEnd = $("#teamUserListBalanceEnd").val();
	userOnlinePage.queryParam.dailiLevel = getSearchVal("teamUserListDailiLevel");
	userOnlinePage.queryParam.orderSort = getSearchVal("teamUserListOrderSort");
	userOnlinePage.queryParam.onlineType = getSearchVal("onlineType");
	userOnlinePage.queryParam.bizSystem = getSearchVal("bizSystem");
	var order =  $('#teamUserListOrder').is(':checked');
	if(userOnlinePage.queryParam.orderSort != null && order) {
		userOnlinePage.queryParam.orderSort = parseInt(userOnlinePage.queryParam.orderSort) + 1;
	} 
	
	var registerDateStart = $("#teamUserListRegisterDateStart").val();
	var registerDateEnd = $("#teamUserListRegisterDateEnd").val();
	if(registerDateStart != ""){
		userOnlinePage.queryParam.registerDateStart = registerDateStart.stringToDate();
	}
	if(registerDateEnd != ""){
		userOnlinePage.queryParam.registerDateEnd = registerDateEnd.stringToDate();
	}
	if(userOnlinePage.queryParam.userName != null && $.trim(userOnlinePage.queryParam.userName)==""){
		userOnlinePage.queryParam.userName = null;
	} else {
		userOnlinePage.queryParam.isQuerySub = true;
	}
	if(userOnlinePage.queryParam.balanceStart != null && $.trim(userOnlinePage.queryParam.balanceStart)==""){
		userOnlinePage.queryParam.balanceStart = null;
	}
	if(userOnlinePage.queryParam.balanceEnd != null && $.trim(userOnlinePage.queryParam.balanceEnd)==""){
		userOnlinePage.queryParam.balanceEnd = null;
	}
	if(userOnlinePage.queryParam.registerDateStart!=null && $.trim(registerDateStart)==""){
		userOnlinePage.queryParam.registerDateStart = null;
	}
	if(userOnlinePage.queryParam.registerDateEnd!=null && $.trim(registerDateEnd)==""){
		userOnlinePage.queryParam.registerDateEnd = null;
	}
	
	userOnlinePage.table.ajax.reload();
};

/**
 * 初始化列表数据
 */
UserOnlinePage.prototype.initTableData = function() {
	userOnlinePage.queryParam = getFormObj($("#queryForm"));
	userOnlinePage.table = $('#userOnlineTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"scrollX": true,
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			userOnlinePage.pageParam.pageSize = data.length;//页面显示记录条数
			userOnlinePage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerUserOnlineAction.getUserOnlineListPage(userOnlinePage.queryParam,userOnlinePage.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": null},
		            {"data": "bizSystemName","bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "userName"},
		            {"data": "money",
		            	  render: function(data, type, row, meta) {
		            		  return row.money.toFixed(commonPage.param.fixVaue);
		            		}
		            },
		            {"data": "addTimeStr"},
		            {"data": "isOnline",
		            	  render: function(data, type, row, meta) {
		            		  var str="";
		            			if(row.isOnline != null && row.isOnline == 1){
		                			str += "是";
		                		}else{
		                			str += "否";
		                		}
		            			return str;
		            		}
		            },
		           
		            {"data": "logTimeStr"},
		           /* {"data": "teamMember"}*/
		          
	            ]
	}).api(); 
}
