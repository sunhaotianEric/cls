function UserConsumeReportPage(){}
var userConsumeReport = new UserConsumeReportPage();

userConsumeReport.param = {
};

//分页参数
userConsumeReport.pageParam = {
        pageNo : 1,
        pageSize : defaultPageSize
};
userConsumeReport.reports=null;
userConsumeReport.totalUser=null;
/**
 * 查询参数
 */
userConsumeReport.queryParam = {
		userName : null,
		createdDateStart : null,
		createdDateEnd : null
};

$(document).ready(function() {
	userConsumeReport.initTableData(); //根据条件,查找资金明细
});
	
/**
 * 加载所有的登陆日志数据
 */
UserConsumeReportPage.prototype.getTotalUsersConsumeReportsForQuery = function(){
	userConsumeReport.queryParam = {};
	var userName = $("#userName").val();
	userConsumeReport.queryParam.bizSystem = getSearchVal("bizSystem");;
	if(userName == ""){
		userConsumeReport.queryParam.userName = null;
	}else{
		userConsumeReport.queryParam.userName = userName;
	}
	
	userConsumeReport.table.ajax.reload();
};



/**
 * 初始化列表数据
 */
UserConsumeReportPage.prototype.initTableData = function() {
//	userConsumeReport.queryParam = getFormObj($("#queryForm"));
	userConsumeReport.table = $('#userMoneyTotalTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"scrollX": true,
		"fnDrawCallback": function (oSettings) {  
	           userConsumeReport.refreshConsumeReportsPages(userConsumeReport.reports,userConsumeReport.totalUser);
	        },
		"ajax":function (data, callback, settings) {
			userConsumeReport.pageParam.pageSize = data.length;//页面显示记录条数
			userConsumeReport.pageParam.pageNo = (data.start / data.length)+1;//当前页码
	    	managerUserAction.queryTotalUsersConsumeReport(userConsumeReport.queryParam,userConsumeReport.pageParam,function(r){
				//封装返回数据
				var returnData = {
					recordsTotal : 0,
					recordsFiltered : 0,
					data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
					userConsumeReport.reports=r[1].pageContent;
					userConsumeReport.totalUser=r[2];
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询数据失败!");
				}
				callback(returnData);
			});
		},
		"columns": [
                    {"data": "bizSystemName","bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "userName",
		            	  render: function(data, type, row, meta) {
		            		   return "<a href='javascript:void(0)' style='color:blue;text-decoration:underline;'  onclick='commonPage.showUserMsgPage(this,"+row.userId+")'>"+ row.userName +"</a>";
		            		}
		            },
		            
		            {"data": null,
		            	  render: function(data, type, row, meta) {
		            		return row.money.toFixed(commonPage.param.fixVaue);
		            	}
		            },
		            {"data": null,
		            	  render: function(data, type, row, meta) {
		            		return (row.totalRecharge).toFixed(commonPage.param.fixVaue);
		            	}
		            },  
		            {"data": null,
		            	  render: function(data, type, row, meta) {
		            		return row.totalRechargePresent.toFixed(commonPage.param.fixVaue);
		            	}
		            },
		            {"data": null,
		            	  render: function(data, type, row, meta) {
		            		return row.totalActivitiesMoney.toFixed(commonPage.param.fixVaue);
		            	}
		            },
		            {"data": null,
		            	  render: function(data, type, row, meta) {
		            		return row.totalSystemAddMoney.toFixed(commonPage.param.fixVaue);
		            	}
		            },
		            {"data": null,
		            	  render: function(data, type, row, meta) {
		            		return row.totalSystemReduceMoney.toFixed(commonPage.param.fixVaue);
		            	}
		            },
		            {"data": null,
		            	  render: function(data, type, row, meta) {
		            		return row.totalWithdraw.toFixed(commonPage.param.fixVaue);
		            	}
		            },
		            {"data": null,
		            	  render: function(data, type, row, meta) {
		            		return row.totalWithdarwFee.toFixed(commonPage.param.fixVaue);
		            	}
		            },
		            /*{"data": null,
		            	  render: function(data, type, row, meta) {
		            		return row.totalRegister.toFixed(commonPage.param.fixVaue);
		            	}
		            },*/
		            {"data": null,
		            	  render: function(data, type, row, meta) {
		            		return row.totalPayTranfer.toFixed(commonPage.param.fixVaue);
		            	}
		            },
		            {"data": null,
		            	  render: function(data, type, row, meta) {
		            		return row.totalIncomeTranfer.toFixed(commonPage.param.fixVaue);
		            	}
		            },
		            /*{"data": null,
		            	  render: function(data, type, row, meta) {
		            		return row.totalExtend.toFixed(commonPage.param.fixVaue);
		            	}
		            },*/
		            {"data": null,
		            	  render: function(data, type, row, meta) {
		            		return (row.totalLottery - row.totalLotteryStopAfterNumber - row.totalLotteryRegression).toFixed(commonPage.param.fixVaue);
		            	}
		            },
		            {"data": null,
		            	  render: function(data, type, row, meta) {
		            		return row.totalWin.toFixed(commonPage.param.fixVaue);
		            	}
		            },
		            {"data": null,
		            	  render: function(data, type, row, meta) {
		            		return row.totalRebate.toFixed(commonPage.param.fixVaue);
		            	}
		            },
		            {"data": null,
		            	  render: function(data, type, row, meta) {
		            		return row.totalPercentage.toFixed(commonPage.param.fixVaue);
		            	}
		            },
		            {"data": null,
		            	  render: function(data, type, row, meta) {
		            		return row.totalHalfMonthBonus.toFixed(commonPage.param.fixVaue);
		            	}
		            },
		           /* {"data": null,
		            	  render: function(data, type, row, meta) {
		            		return row.totalDayBonus.toFixed(commonPage.param.fixVaue);
		            	}
		            },
		            {"data": null,
		            	  render: function(data, type, row, meta) {
		            		return row.totalMonthSalary.toFixed(commonPage.param.fixVaue);
		            	}
		            },*/
		            {"data": null,
		            	  render: function(data, type, row, meta) {
		            		return row.totalDaySalary.toFixed(commonPage.param.fixVaue);
		            	}
		            },
		            /*{"data": null,
		            	  render: function(data, type, row, meta) {
		            		return row.totalDayCommission.toFixed(commonPage.param.fixVaue);
		            	}
		            },
		            {"data": null,
		            	  render: function(data, type, row, meta) {
		            		return (row.totalMonthSalary + row.totalDaySalary + row.totalDayCommission + row.totalHalfMonthBonus + row.totalDayBonus).toFixed(commonPage.param.fixVaue);
		            	}
		            },*/
		            {"data": null,
		            	  render: function(data, type, row, meta) {
		            		//个人盈利计算： 总充值赠送+总活动彩金+总系统续费+总中奖+总返点+总提成+总转账转入+总契约分红+总日工资-总投注-总转账转出-总系统扣费-总手续费
		            		return (row.totalRechargePresent + row.totalActivitiesMoney + row.totalSystemAddMoney
		            	            + row.totalWin + row.totalRebate + row.totalPercentage
		            	            + row.totalIncomeTranfer + row.totalHalfMonthBonus + row.totalDaySalary
		            	            - (row.totalLottery - row.totalLotteryStopAfterNumber - row.totalLotteryRegression )- row.totalPayTranfer - row.totalSystemReduceMoney - row.totalWithdarwFee).toFixed(commonPage.param.fixVaue);
		            	}
		            }
	            ]
	}).api(); 
}



/**
 * 刷新列表数据
 */
UserConsumeReportPage.prototype.refreshConsumeReportsPages = function(reports,totalUser){
	if(reports.length != 0){
		var reportListObj = $("#userMoneyTotalTable tbody");
		var str = "";
		var totalMoney = 0;
		var totalRecharge = 0;
		var totalRechargePresent = 0;
		var totalActivitiesMoney = 0;
		var totalSystemAddMoney = 0;
		var totalSystemReduceMoney = 0;
		var totalWithDraw = 0;
		var totalWithDrawFee= 0;
		//var totalRegister = 0;
		var totalPayTranfer = 0;
		var totalIncomeTranfer = 0;
		//var totalExtend = 0;
		var totalLottery = 0;
		var totalShopPoint = 0;
		var totalWin = 0;
		var totalRebate = 0;
		var totalPercentage = 0;
		var totalHalfMonthBonus = 0;
		var totalDayBonus = 0;
		var totalMonthSalary = 0;
		var totalDaySalary = 0;
		var totalDayCommission = 0;
		var totalGain = 0;
		
		//记录数据显示
		for(var i = 0; i < reports.length; i++){
			var report = reports[i];
			totalMoney      += report.money;
			totalRecharge   += report.totalRecharge;
			totalRechargePresent += report.totalRechargePresent;
			totalActivitiesMoney += report.totalActivitiesMoney;
			totalSystemAddMoney += report.totalSystemAddMoney;
			totalSystemReduceMoney += report.totalSystemReduceMoney;
			totalWithDraw   += report.totalWithdraw;
			totalWithDrawFee += report.totalWithdarwFee;
			//totalRegister   += report.totalRegister;
			totalPayTranfer += report.totalPayTranfer;
			totalIncomeTranfer += report.totalIncomeTranfer;
			//totalExtend     += report.totalExtend;
			totalLottery    += report.totalLottery - report.totalLotteryStopAfterNumber - report.totalLotteryRegression;
			totalWin        += report.totalWin;
			totalRebate     += report.totalRebate;
			totalPercentage += report.totalPercentage;
			totalHalfMonthBonus += report.totalHalfMonthBonus;
			totalDayBonus += report.totalDayBonus;
			totalMonthSalary += report.totalMonthSalary;
			totalDaySalary += report.totalDaySalary;
			totalDayCommission += report.totalDayCommission;
			totalGain       += report.totalRechargePresent + report.totalActivitiesMoney + report.totalSystemAddMoney
			+ report.totalWin + report.totalRebate + report.totalPercentage
			+ report.totalIncomeTranfer + report.totalHalfMonthBonus + report.totalDayCommission
			- (report.totalLottery - report.totalLotteryStopAfterNumber - report.totalLotteryRegression )- report.totalPayTranfer - report.totalSystemReduceMoney;
		}  
		
		str += "<tr style='color:red'>";
		str += "  <td>当前页总计</td>";
		if(currentUser.bizSystem=='SUPER_SYSTEM'){
			str += "  <td></td>";
		}
		str += "  <td>"+ totalMoney.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ totalRecharge.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ totalRechargePresent.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ totalActivitiesMoney.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ totalSystemAddMoney.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ totalSystemReduceMoney.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ totalWithDraw.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ totalWithDrawFee.toFixed(commonPage.param.fixVaue) +"</td>";
		/*str += "  <td>"+ totalRegister.toFixed(commonPage.param.fixVaue) +"</td>";*/
		str += "  <td>"+ totalPayTranfer.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ totalIncomeTranfer.toFixed(commonPage.param.fixVaue) +"</td>";
		/*str += "  <td>"+ totalExtend.toFixed(commonPage.param.fixVaue) +"</td>";*/
		/*str += "  <td>"+ remainExtend.toFixed(commonPage.param.fixVaue) +"</td>";*/
		str += "  <td>"+ totalLottery.toFixed(commonPage.param.fixVaue) +"</td>";
		//str += "  <td>"+ totalShopPoint.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ totalWin.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ totalRebate.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ totalPercentage.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ totalHalfMonthBonus.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ totalDaySalary.toFixed(commonPage.param.fixVaue) +"</td>";
		str += "  <td>"+ totalGain.toFixed(commonPage.param.fixVaue) +"</td>";
		reportListObj.append(str);
		str = "";
		
		
		str += "<tr style='color:red'>";
		str += "  <td>所有用户总计</td>";
		if(currentUser.bizSystem=='SUPER_SYSTEM'){
			str += "  <td></td>";
		}
		if(totalUser==null)
		{
			str += "<td>0.000</td><td>0.000</td><td>0.000</td><td>0.000</td><td>0.000</td><td>0.000</td><td>0.000</td>";
			str += "<td>0.000</td><td>0.000</td><td>0.000</td><td>0.000</td><td>0.000</td><td>0.000</td><td>0.000</td>";
		}else{
			str += "  <td>"+ totalUser.money.toFixed(commonPage.param.fixVaue) +"</td>";
			str += "  <td>"+ totalUser.totalRecharge.toFixed(commonPage.param.fixVaue) +"</td>";
			str += "  <td>"+ totalUser.totalRechargePresent.toFixed(commonPage.param.fixVaue) +"</td>";
			str += "  <td>"+ totalUser.totalActivitiesMoney.toFixed(commonPage.param.fixVaue) +"</td>";
			str += "  <td>"+ totalUser.totalSystemAddMoney.toFixed(commonPage.param.fixVaue) +"</td>";
			str += "  <td>"+ totalUser.totalSystemReduceMoney.toFixed(commonPage.param.fixVaue) +"</td>";
			str += "  <td>"+ totalUser.totalWithdraw.toFixed(commonPage.param.fixVaue) +"</td>";
			str += "  <td>"+ totalUser.totalWithdarwFee.toFixed(commonPage.param.fixVaue) +"</td>";
			/*str += "  <td>"+ totalUser.totalRegister.toFixed(commonPage.param.fixVaue) +"</td>";*/
			str += "  <td>"+ totalUser.totalPayTranfer.toFixed(commonPage.param.fixVaue) +"</td>";
			str += "  <td>"+ totalUser.totalIncomeTranfer.toFixed(commonPage.param.fixVaue) +"</td>";
			/*str += "  <td>"+ totalUser.totalExtend.toFixed(commonPage.param.fixVaue) +"</td>";*/
			/*str += "  <td>"+ totalUser.remainExtend.toFixed(commonPage.param.fixVaue) +"</td>";*/
			str += "  <td>"+ (totalUser.totalLottery - totalUser.totalLotteryStopAfterNumber - totalUser.totalLotteryRegression).toFixed(commonPage.param.fixVaue) +"</td>";
			//str += "  <td>"+ totalUser.totalShopPoint.toFixed(commonPage.param.fixVaue) +"</td>";
			str += "  <td>"+ totalUser.totalWin.toFixed(commonPage.param.fixVaue) +"</td>";
			str += "  <td>"+ totalUser.totalRebate.toFixed(commonPage.param.fixVaue) +"</td>";
			str += "  <td>"+ totalUser.totalPercentage.toFixed(commonPage.param.fixVaue) +"</td>";
			str += "  <td>"+ totalUser.totalHalfMonthBonus.toFixed(commonPage.param.fixVaue) +"</td>";
			/*str += "  <td>"+ totalUser.totalDayBonus.toFixed(commonPage.param.fixVaue) +"</td>";
	str += "  <td>"+ totalUser.totalMonthSalary.toFixed(commonPage.param.fixVaue) +"</td>";*/
			str += "  <td>"+ totalUser.totalDaySalary.toFixed(commonPage.param.fixVaue) +"</td>";
			/*str += "  <td>"+ totalUser.totalDayCommission.toFixed(commonPage.param.fixVaue) +"</td>";*/
			//盈利(充值赠送  + 系统续费  + 中奖 + 返点 + 提成 + 收入转账 + 月分红 + 日工资 - 投注  - 支出转账 - 系统扣费 )  
			str += "  <td>"+ (totalUser.totalRechargePresent + totalUser.totalActivitiesMoney + totalUser.totalSystemAddMoney 
					+ totalUser.totalWin + totalUser.totalRebate + totalUser.totalPercentage
					+ totalUser.totalIncomeTranfer + totalUser.totalHalfMonthBonus + totalUser.totalDaySalary
					- (totalUser.totalLottery - totalUser.totalLotteryStopAfterNumber - totalUser.totalLotteryRegression) - totalUser.totalPayTranfer - totalUser.totalSystemReduceMoney).toFixed(commonPage.param.fixVaue) +"</td>";
		}
		str += "</tr>"; 
		reportListObj.append(str);
	}

};
