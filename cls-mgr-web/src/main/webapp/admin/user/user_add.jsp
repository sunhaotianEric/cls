<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>  
    <head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>直属会员添加</title>
     <link rel="shortcut icon" href="favicon.ico">
     <jsp:include page="/admin/include/include.jsp"></jsp:include>

</head>
    <body class="gray-bg">
        <div class="wrapper wrapper-content animated fadeIn">
            <div class="row">
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">会员类别</span>
                        <select class="ipt form-control" id='dailiLevel' disabled="disabled">
	                        <option value='DIRECTAGENCY' selected="selected">直属代理</option>
	                        <option value='GENERALAGENCY'>总代理</option>
	                        <option value='REGULARMEMBERS'>普通会员</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">登录帐户名</span>
                        <input type="text" id='username'  class="form-control"></div>
                </div>
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">登录密码</span>
                        <input type="password" id='password' class="form-control"></div>
                </div>
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">用户QQ号</span>
                        <input type="text" id='qq' class="form-control"></div>
                </div>
                <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">手机号码</span>
                        <input type="text" id='phone' class="form-control"></div>
                </div>
                 <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">奖金模式</span>
                        <select class="ipt form-control" id="sscRebate" name="sscRebate">
                         </select>
                        </div>
                </div>
          <!--        <div class="col-sm-6">
                    <div class="input-group m-b">
                        <span class="input-group-addon">分红比例</span>
                        <input type="text" id='bonusScale' name="bonusScale" class="form-control" onkeyup="checkNum(this)"></div>
                </div> -->
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <button type="button" class="btn btn-w-m btn-white btn-add"  onclick="userAdd.saveUser()">提 交</button></div>
                    </div>
                </div>
            </div>
        </div>
<!-- js -->
<script type="text/javascript" src="<%=path%>/admin/user/js/user_add.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript' src='<%=path%>/dwr/interface/managerUserAction.js'></script>
<script type='text/javascript' src='<%=path%>/dwr/interface/managerBizSystemAction.js'></script>
</body>
</html>

