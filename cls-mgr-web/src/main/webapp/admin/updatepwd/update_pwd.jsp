<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

  <title>修改密码</title>
  <jsp:include page="/admin/include/include.jsp"></jsp:include>
  <link rel="shortcut icon" href="favicon.ico">
</head>
<body class="gray-bg">
<form class="form-horizontal">
    <div class="wrapper wrapper-content animated fadeIn">
        <div class="row">
        	<input type="hidden" id="userId">
            <div class="col-sm-12">
                <div class="input-group m-b">
                    <span class="input-group-addon">原始密码</span>
                    <input type="password" value="" class="form-control" id="oldPassWordId"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="input-group m-b">
                    <span class="input-group-addon">新密码</span>
                    <input type="password" value="" class="form-control" id="newPassWordId"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="input-group m-b">
                    <span class="input-group-addon">确认密码</span>
                    <input type="password" value="" class="form-control" id="confirmPassWordId"></div>
            </div>
        </div>
         <div class="col-sm-12">
             <div class="row">
                 <div class="col-sm-6 text-center" style="width:50%;float:left">
                     <button type="button" class="btn btn-w-m btn-white" onclick="updatePwdPage.updatePwd()">提 交</button></div>
                 <div class="col-sm-6 text-center" style="width:50%;float:left">
                     <button type="reset" class="btn btn-w-m btn-white">重 置</button></div>
             </div>
         </div>
    </div>
</form>
    <script src="<%=path%>/js/jquery.min.js?v=2.1.4"></script>
    <script src="<%=path%>/js/bootstrap.min.js?v=3.3.6"></script>
    <script src="<%=path%>/js/content.min.js?v=1.0.0"></script>
    <script src="<%=path%>/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<%=path%>/js/plugins/layer/laydate/laydate.js"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerAdminAction.js'></script>
    <!-- js -->
   	<script type="text/javascript" src="<%=path%>/admin/updatepwd/js/updatepwd.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
</body>
</html>