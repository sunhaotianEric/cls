function UpdatePwdPage(){};

var updatePwdPage = new UpdatePwdPage();

updatePwdPage.param={
	id:null,
	password:null,
	newpassword:null,
	surepassword:null
}

$(document).ready(function(){
	$("#userId").val(currentUser.id)
	
	$("#oldPassWordId").blur(function(){
		var password = $("#oldPassWordId").val();
		if(password == null || password.trim() == ""){
			swal("请输入原始密码");
			return;
		}
	});
	
	$("#newPassWordId").blur(function(){
		var newPassWord = $("#newPassWordId").val();
		if(newPassWord == null || newPassWord == "" || newPassWord.length < 6 || newPassWord.length > 15){
			swal("6-15个字符，建议使用字母、数字组合");
		}
	});
	
	$("#confirmPassWordId").blur(function(){
		var newPassWord = $("#newPassWordId").val();
		var surepassword = $("#confirmPassWordId").val();
		if(newPassWord != surepassword){
			swal("两次密码输入不一致");
		}
	})
});

UpdatePwdPage.prototype.updatePwd=function(){
	var id =$("#userId").val();
	var password = $("#oldPassWordId").val();
	var newPassWord = $("#newPassWordId").val();
	var surepassword = $("#confirmPassWordId").val();
	if(password == null || password.trim() == ""){
		swal("请输入原始密码");
		return;
	}
	if(newPassWord == null || newPassWord.trim() == ""){
		swal("请输入新密码");
		return;
	}
	if(surepassword == null || surepassword.trim() == ""){
		swal("请输入确认密码");
		return;
	}
	
	Admin={}
	Admin.id=id;
	Admin.password=password;
	Admin.newpassword=newPassWord;
	Admin.surepassword=surepassword;
	managerAdminAction.updatePassword(Admin,function(r){
		if (r[0] != null && r[0] == "ok") {
			var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
			parent.indexPage.closeUpdatePwd(index);
//			window.location.href= contextPath + "/managerzaizst/login.html";
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("操作失败.");
		}
    
	});
}
