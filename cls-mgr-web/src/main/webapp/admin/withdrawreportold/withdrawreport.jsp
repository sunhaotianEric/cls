<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>充值报表</title>
<jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
<body>
	<div class="animated fadeIn">
		<div class="ibox-content">
			<div class="row m-b-sm m-t-sm">
				<form class="form-horizontal" id="withdrawreportForm">
					<c:choose>
						<c:when test="${admin.bizSystem=='SUPER_SYSTEM'}">
							<div class="row">
								<div class="col-sm-3">
									<div class="input-group m-b">
										<span class="input-group-addon">商户</span>
										<cls:bizSel name="bizSystem" id="bizSystem"
											options="class:ipt form-control" />

									</div>
								</div>
								<div class="col-sm-3">
									<div class="input-group m-b">
										<span class="input-group-addon">用户名</span> <input type="text"
											value="" class="form-control" name=userName id="userName">

									</div>
								</div>
								<div class="col-sm-3">
									<div class="input-group m-b">
										<span class="input-group-addon">订单号</span> <input type="text"
											value="" class="form-control" name=serialNumber
											id="serialNumber">
									</div>
								</div>
								<div class="col-sm-3">
									<div class="input-group m-b">
										<span class="input-group-addon">充值状态</span> <select
											class="form-control" id="rechargeStatus">
											<option value="">==请选择==</option>
											<option value='DEALING'>处理中</option>
											<option value='WITHDRAW_SUCCESS' selected="selected">提现成功</option>
											<option value='WITHDRAW_FAIL'>提现失败</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-5">
									<div class="input-group m-b">
										<span class="input-group-addon">时间始终</span>
										<div class="input-daterange input-group">
											<input class="form-control layer-date" placeholder="开始日期"
												onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"
												name="createdDateStart" id="createdDateStart"> <span
												class="input-group-addon">到</span> <input
												class="form-control layer-date" placeholder="结束日期"
												onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"
												name="createdDateEnd" id="createdDateEnd">
										</div>
									</div>
								</div>
								<div class="col-sm-3 pl0">
									<div class="form-group nomargin text-left">
										<button type="button" class="btn btn-xs btn-info"
											onclick="rechargeorderPage.getAllRechargeOrdersForQuery(1)">今
											天</button>
										<button type="button" class="btn btn-xs btn-danger"
											onclick="rechargeorderPage.getAllRechargeOrdersForQuery(-1)">昨
											天</button>
										<button type="button" class="btn btn-xs btn-warning"
											onclick="rechargeorderPage.getAllRechargeOrdersForQuery(-7)">最近7天</button>
										<br />
										<button type="button" class="btn btn-xs btn-success"
											onclick="rechargeorderPage.getAllRechargeOrdersForQuery(30)">本
											月</button>
										<button type="button" class="btn btn-xs btn-primary"
											onclick="rechargeorderPage.getAllRechargeOrdersForQuery(-30)">上
											月</button>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group nomargin text-right">
										<button type="button"
											class="btn btn-outline btn-default btn-js"
											onclick="rechargeorderPage.pageParam.pageNo = 1;rechargeorderPage.getAllRechargeOrdersForQuery()">
											<i class="fa fa-search"></i>&nbsp;查 询
										</button>
									</div>
								</div>
							</div>
						</c:when>
						<c:otherwise>
							<div class="row">
								<div class="col-sm-4">
									<div class="input-group m-b">
										<span class="input-group-addon">充值状态</span> <select
											class="form-control" id="rechargeStatus">
											<option value="">==请选择==</option>
											<option value='DEALING'>处理中</option>
											<option value='WITHDRAW_SUCCESS' selected="selected">提现成功</option>
											<option value='WITHDRAW_FAIL'>提现失败</option>
										</select>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="input-group m-b">
										<span class="input-group-addon">用户名</span> <input type="text"
											value="" class="form-control" name=userName id="userName">

									</div>
								</div>
								<div class="col-sm-4">
									<div class="input-group m-b">
										<span class="input-group-addon">订单号</span> <input type="text"
											value="" class="form-control" name=serialNumber
											id="serialNumber">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-5">
									<div class="input-group m-b">
										<span class="input-group-addon">时间始终</span>
										<div class="input-daterange input-group">
											<input class="form-control layer-date" placeholder="开始日期"
												onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss',max: laydate.now(-1)})"
												name="createdDateStart" id="createdDateStart"> <span
												class="input-group-addon">到</span> <input
												class="form-control layer-date" placeholder="结束日期"
												onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss',max: laydate.now(-1)})"
												name="createdDateEnd" id="createdDateEnd">
										</div>
									</div>
								</div>
								<div class="col-sm-3 pl0">
									<div class="form-group nomargin text-left">
										<button type="button" class="btn btn-xs btn-info"
											onclick="rechargeorderPage.getAllRechargeOrdersForQuery(1)">今
											天</button>
										<button type="button" class="btn btn-xs btn-danger"
											onclick="rechargeorderPage.getAllRechargeOrdersForQuery(-1)">昨
											天</button>
										<button type="button" class="btn btn-xs btn-warning"
											onclick="rechargeorderPage.getAllRechargeOrdersForQuery(-7)">最近7天</button>
										<br />
										<button type="button" class="btn btn-xs btn-success"
											onclick="rechargeorderPage.getAllRechargeOrdersForQuery(30)">本
											月</button>
										<button type="button" class="btn btn-xs btn-primary"
											onclick="rechargeorderPage.getAllRechargeOrdersForQuery(-30)">上
											月</button>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group nomargin text-right">
										<button type="button"
											class="btn btn-outline btn-default btn-js"
											onclick="rechargeorderPage.pageParam.pageNo = 1;rechargeorderPage.getAllRechargeOrdersForQuery()">
											<i class="fa fa-search"></i>&nbsp;查 询
										</button>
									</div>
								</div>

							</div>
						</c:otherwise>
					</c:choose>
					<!-- //</div> -->
				</form>
			</div>
			<div class="jqGrid_wrapper">
				<div class="ui-jqgrid ">
					<div class="ui-jqgrid-view">
						<div class="ui-jqgrid-hdiv">
							<div class="ui-jqgrid-hbox" style="padding-right: 0">
								<table id="withdrawreportTable"
									class="ui-jqgrid-htable ui-common-table table table-bordered">
									<thead>
										<tr class="ui-jqgrid-labels">
											<th class="ui-th-column ui-th-ltr">商户</th>
											<th class="ui-th-column ui-th-ltr">用户名</th>
											<th class="ui-th-column ui-th-ltr">订单号</th>
											<!--<th>商户类型</th>预留-->
											<th class="ui-th-column ui-th-ltr">申请金额</th>
											<th class="ui-th-column ui-th-ltr">到账金额</th>
											<th class="ui-th-column ui-th-ltr">手续费</th>
											<th class="ui-th-column ui-th-ltr">充值时间</th>
											<th class="ui-th-column ui-th-ltr">状态</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<!-- js -->
	<script type="text/javascript"
		src="<%=path%>/admin/withdrawreportold/js/withdrawreport.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
	<!-- dwr -->
	<script type='text/javascript'
		src='<%=path%>/dwr/interface/managerRechargeWithDrawOrderAction.js'></script>
	<script type='text/javascript'
		src='<%=path%>/dwr/interface/managerUserAction.js'></script>
</body>
</html>

