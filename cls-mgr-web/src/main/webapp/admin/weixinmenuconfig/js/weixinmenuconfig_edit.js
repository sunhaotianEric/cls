function EditWeixinMenuConfigPage(){}
var editWeixinMenuConfigPage = new EditWeixinMenuConfigPage();

editWeixinMenuConfigPage.param = {
   	
};

$(document).ready(function() {
	var weixinConfigId = editWeixinMenuConfigPage.param.id;  //获取查询的微信配置ID
	if(weixinConfigId != null){
		editWeixinMenuConfigPage.editWeixinMenuConfig(weixinConfigId);
	} else {
		editWeixinMenuConfigPage.initAllWeixinConfigs();
	}
	//微信公众号改变是触发事件
	$("#weixinId").change(function(){
		$("#parentMenuId option").remove();
		if($("#menuLevel").val() == 2) {
			editWeixinMenuConfigPage.getWeixinParentMenu();
		}
	});
	
	//菜单级别改变时触发事件
	$("#menuLevel").change(function(){
		$("#parentMenuId option").remove();
		if($(this).val() == 2) {
			editWeixinMenuConfigPage.getWeixinParentMenu();
		}
	});
});

/**
 * 加载所有的微信公众号名称
 */
EditWeixinMenuConfigPage.prototype.initAllWeixinConfigs = function(){
	managerWeixinConfigAction.getAllWeixinConfigs(function(r){
		if (r[0] != null && r[0] == "ok") {
			var weixinConfigs = r[1];
			if(weixinConfigs != null && weixinConfigs.length > 0) {
				var obj=document.getElementById('weixinId');
				for(var i = 0 ; i < weixinConfigs.length; i++){
					var weixinConfig = weixinConfigs[i];
					obj.options.add(new Option(weixinConfig.name, weixinConfig.id)); 
				}
			}
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "加载微信公众号数据失败.");
		}
    });
};


/**
 * 根据微信号ID加载一级的微信菜单
 */
EditWeixinMenuConfigPage.prototype.getWeixinParentMenu = function(){
	var weixinId = $("#weixinId").val();
	if(weixinId == null) {
		alert("请选择微信公众号");
		return;
	}
	var queryParam = {};
	queryParam.weixinId = weixinId;
	queryParam.menuLevel = 1;
	managerWeixinConfigAction.getWeixinMenuConfigsByCondition(queryParam,function(r){
		if (r[0] != null && r[0] == "ok") {
			var weixinMenuConfigs = r[1];
			if(weixinMenuConfigs != null && weixinMenuConfigs.length > 0) {
				var obj=document.getElementById('parentMenuId');
				for(var i = 0 ; i < weixinMenuConfigs.length; i++){
					var weixinMenuConfig = weixinMenuConfigs[i];
					obj.options.add(new Option(weixinMenuConfig.name, weixinMenuConfig.id)); 
				}
			}
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "加载微信公众号数据失败.");
		}
    });
};

/**
 * 保存微信菜单配置信息
 */
EditWeixinMenuConfigPage.prototype.saveData = function(){
	var id = $("#menuId").val();
	var weixinId = $("#weixinId").val();
	var menuLevel = $("#menuLevel").val();
	var name = $("#name").val();
	var parentMenuId = $("#parentMenuId").val();
	var type = $("#type").val();
	var menuUrl = $("#menuUrl").val();
	var redirectPageUrl = $("#redirectPageUrl").val();
	var keyName = $("#keyName").val();
	
	if(weixinId == null || weixinId.trim() == "") {
		alert("请选择微信公众号!");
		return;
	}
	if(menuLevel==null || menuLevel.trim()==""){
		alert("请选择菜单级别!");
		$("#menuLevel").focus();
		return;
	} else {
		//二级菜单 父级菜单必填
		if(menuLevel == 2) {
			if(parentMenuId == null || parentMenuId == '') {
				alert("菜单级别为二级，父级菜单名称必填！");
				return;
			}
			if(type==null || type.trim()==""){
				alert("请输入菜单类型!");
				$("#type").focus();
				return;
			}
			if(menuUrl==null || menuUrl.trim()==""){
				alert("请输入菜单URL!");
				$("#menuUrl").focus();
				return;
			}
			if(redirectPageUrl ==null || redirectPageUrl.trim()==""){
				alert("请输入重定向页面的URL!");
				$("#redirectPageUrl").focus;
			}
		}
	}
	if(name==null || name.trim()==""){
		alert("请输入菜单名称!");
		$("#name").focus();
		return;
	}
	/**/
	
	
	var weixinMenuConfig = {};
	weixinMenuConfig.id = id;
	weixinMenuConfig.weixinId = weixinId;
	weixinMenuConfig.menuLevel = menuLevel;
	weixinMenuConfig.name = name;
	weixinMenuConfig.parentMenuId = parentMenuId;
	weixinMenuConfig.type = type;
	weixinMenuConfig.menuUrl = menuUrl;
	weixinMenuConfig.keyName = keyName;
	weixinMenuConfig.redirectPageUrl = redirectPageUrl;
	
	managerWeixinConfigAction.saveOrUpdateWeixinMenuConfig(weixinMenuConfig,function(r){
		if (r[0] != null && r[0] == "ok") {
			alert("保存成功");
			self.location = contextPath + "/managerzaizst/weixinmenuconfig.html";
		}else if(r[0] != null && r[0] == "error"){
			showErrorDlg(jQuery(document.body), r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "保存微信菜单配置数据失败.");
		}
    });
};


/**
 * 赋值微信菜单配置信息  com.team.lottery.mapper.weixinmenuconfig.WeixinMenuConfigMapper.selectByPrimaryKey
 */
EditWeixinMenuConfigPage.prototype.editWeixinMenuConfig = function(id){
	managerWeixinConfigAction.getWeixinMenuConfigById(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			$("#menuId").val(r[1].id);
			$("#weixinId").val(r[1].weixinId);
			$("#weixinName").val(r[1].weixinName);
			$("#menuLevel").val(r[1].menuLevel);
			$("#name").val(r[1].name);
			$("#parentMenuName").val(r[1].parentMenuName);
			$("#parentMenuId").val(r[1].parentMenuId);
			$("#type").val(r[1].type);
			$("#menuUrl").val(r[1].menuUrl);
			$("#redirectPageUrl").val(r[1].redirectPageUrl);
			$("#keyName").val(r[1].keyName);
		}else if(r[0] != null && r[0] == "error"){
			showErrorDlg(jQuery(document.body), r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "获取微信菜单配置数据失败.");
		}
    });
};