function WeixinMenuConfigListPage(){}
var weixinMenuConfigListPage = new WeixinMenuConfigListPage();

weixinMenuConfigListPage.queryParam = {
	weixinName : null,
	menuLevel : null,
	name : null
};

weixinMenuConfigListPage.param = {
   	
};

$(document).ready(function() {
	weixinMenuConfigListPage.initAllWeixinConfigs();
	weixinMenuConfigListPage.getWeixinMenuConfigsByCondition();
});

/**
 * 初始化所有的微信配置数据
 */
WeixinMenuConfigListPage.prototype.initAllWeixinConfigs = function(){
	managerWeixinConfigAction.getAllWeixinConfigs(function(r){
		if (r[0] != null && r[0] == "ok") {
			var weixinConfigs = r[1];
			if(weixinConfigs != null && weixinConfigs.length > 0) {
				var obj=document.getElementById('weixinName');
				for(var i = 0 ; i < weixinConfigs.length; i++){
					var weixinConfig = weixinConfigs[i];
					obj.options.add(new Option(weixinConfig.name, weixinConfig.name)); 
				}
			}
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询网页公告数据失败.");
		}
    });
};

/**
 * 加载所有的微信菜单配置数据
 */
WeixinMenuConfigListPage.prototype.getWeixinMenuConfigsByCondition = function(){
	weixinMenuConfigListPage.queryParam.weixinName = getSearchVal("weixinName");
	weixinMenuConfigListPage.queryParam.menuLevel = getSearchVal("menuLevel");
	weixinMenuConfigListPage.queryParam.name = getSearchVal("menuName");
	managerWeixinConfigAction.getWeixinMenuConfigsByCondition(weixinMenuConfigListPage.queryParam, function(r){
		if (r[0] != null && r[0] == "ok") {
			weixinMenuConfigListPage.refreshWeixinMenuConfigListPages(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询网页公告数据失败.");
		}
    });
};

/**
 * 刷新列表数据
 */
WeixinMenuConfigListPage.prototype.refreshWeixinMenuConfigListPages = function(weixinMenuConfigs){
	var weixinMenuConfigListObj = $("#weixinMenuConfigList");
	weixinMenuConfigListObj.html("");
	var str = "";
	if(weixinMenuConfigs != null && weixinMenuConfigs.length > 0) {
		for(var i = 0 ; i < weixinMenuConfigs.length; i++){
			var weixinMenuConfig = weixinMenuConfigs[i];
			str += "<tr>";
			str += "  <td>"+ weixinMenuConfig.weixinName +"</td>";
			var menuLevel = "";
			if(weixinMenuConfig.menuLevel == 1) {
				menuLevel = "一级";
			} else if(weixinMenuConfig.menuLevel == 2) {
				menuLevel = "二级";
			}
			str += "  <td>"+ menuLevel +"</td>";
			str += "  <td>"+ weixinMenuConfig.name +"</td>";
			var parentMenuName = "";
			if(weixinMenuConfig.parentMenuName != null) {
				parentMenuName = weixinMenuConfig.parentMenuName;
			}
			str += "  <td>"+ parentMenuName +"</td>";
			str += "  <td>"+ weixinMenuConfig.type +"</td>";
			//str += "  <td>"+ weixinMenuConfig.menuUrl +"</td>";
			
			str += "  <td><a href='javascript:void(0)' onclick='weixinMenuConfigListPage.editWeixinMenuConfig("+ weixinMenuConfig.id +")'>编辑</a>&nbsp;|&nbsp;<a href='javascript:void(0)' onclick='weixinMenuConfigListPage.delWeixinMenuConfig("+ weixinMenuConfig.id +")'>删除</a></td>";
	
			str += "</tr>";
			
			weixinMenuConfigListObj.append(str);
			str = "";
		}
	}
};

/**
 * 删除微信菜单配置数据
 */
WeixinMenuConfigListPage.prototype.delWeixinMenuConfig = function(id){
	if (confirm("确认要删除该微信号？")){
		managerWeixinConfigAction.delWeixinMenuConfig(id,function(r){
			if (r[0] != null && r[0] == "ok") {
				alert("删除成功");
				weixinMenuConfigListPage.getWeixinMenuConfigsByCondition();
			}else if(r[0] != null && r[0] == "error"){
				showErrorDlg(jQuery(document.body), r[1]);
			}else{
				showErrorDlg(jQuery(document.body), "删除微信菜单配置失败.");
			}
	    });
	}
};

/**
 * 编辑微信菜单配置数据
 */
WeixinMenuConfigListPage.prototype.editWeixinMenuConfig = function(id){
	self.location = contextPath + "/managerzaizst/weixinmenuconfig/" + id+"/weixinmenuconfig_edit.html";
};

/**
 * 添加微信菜单配置数据
 */
WeixinMenuConfigListPage.prototype.addWeixinMenuConfig = function(){
	self.location = contextPath + "/managerzaizst/weixinmenuconfig/weixinmenuconfig_add.html";
};


