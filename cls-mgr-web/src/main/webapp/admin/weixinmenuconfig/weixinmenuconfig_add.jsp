<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<html>
<head>
    <meta charset="utf-8" />
    <title>微信菜单添加</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/weixinmenuconfig/js/weixinmenuconfig_edit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerWeixinConfigAction.js'></script>
    
</head>
<body>
   <div class="alert alert-info">当前位置<b class="tip"></b>微信菜单配置管理<b class="tip"></b>微信菜单添加</div>
   	
   	<table class="tb">
         <tr>
         	<td> <a href="<%=path %>/managerzaizst/weixinmenuconfig.html">返回微信菜单配置列表</a> </td>
         </tr>
    </table>
    <form action="javascript:void(0)">
	    <table class="tb">
	   	 	<tr>
	   			<td style="text-align: right;">微信公众号名称：<input type="hidden" id="menuId"/></td>
	   			<td>
	   				<select id="weixinId">
	             	</select>
	   			</td>
	   		</tr>
	   		<tr>
	   			<td style="text-align: right;">菜单级别：</td>
	   			<td>
	   				<select id="menuLevel">
	             		<option value="1">一级</option>
	             		<option value="2">二级</option>
	             	</select>
	   			</td>
	   		</tr>
	   		<tr>
	   			<td style="text-align: right;">父级菜单名称：</td>
	   			<td>
	   			   <select id="parentMenuId">
	   			   </select>
	   			</td>
	   		</tr>
	   		<tr>
	   			<td style="text-align: right;">菜单名称：</td>
	   			<td><input type="text" id="name"/></td>
	   		</tr>
	   		<tr>
	   			<td style="text-align: right;">菜单类型：</td>
	   			<td>
	   			   <input type="text" id="type"/>
	   			</td>
	   		</tr>
	   		<tr>
	   			<td style="text-align: right;">菜单URL：</td>
	   			<td>
	   			   <input type="text" id="menuUrl"/>
	   			</td>
	   		</tr>
	   			<tr>
	   		<td style="text-align: right;">重定向页面的URL：</td>
	   			<td>
	   			   <input type="text" id="redirectPageUrl"/>
	   			</td>
	   		</tr>
	   		<tr>
	   			<td style="text-align: right;">菜单Key：</td>
	   			<td>
	   			   <input type="text" id="keyName"/>
	   			</td>
	   		</tr>
	   		
	   		<tr>
	   			<td></td>
	   			<td><input type="button" onclick="editWeixinMenuConfigPage.saveData()" value="提交"/>&nbsp;&nbsp;&nbsp;&nbsp;<input type="reset" value="重置"/></td>
	   		</tr>
	    </table>
    </form>
</body>
</html>

