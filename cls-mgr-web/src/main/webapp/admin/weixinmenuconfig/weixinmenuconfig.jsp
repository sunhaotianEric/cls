<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<html>
<head>
    <meta charset="utf-8" />
    <title>微信菜单配置管理</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/weixinmenuconfig/js/weixinmenuconfig_list.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerWeixinConfigAction.js'></script>
</head>
<body>
   <div class="alert alert-info">当前位置<b class="tip"></b>微信管理<b class="tip"></b>微信菜单配置管理</div>
   	<table class="tbform">
         <tr>
             <td class="tdl">微信名称：</td>
             <td>
             	<select id="weixinName">
             		<option value="">所有</option>
             	</select>
             </td>
             <td class="tdl">菜单等级：</td>
             <td>
                 <select id="menuLevel">
             		<option value="">所有</option>
             		<option value="1">一级</option>
             		<option value="2">二级</option>
             	</select>
             </td>
             <td class="tdl">菜单名称：</td>
             <td>
             	<input type="text" name="menuName" id="menuName"/>
             </td>
             <td colspan="2" align="left">
                    <input class="btn" id="queryButton" type="button" onclick="weixinMenuConfigListPage.getWeixinMenuConfigsByCondition();" value="查询" />
                    <input class="btn" id="addButton" type="button" onclick="weixinMenuConfigListPage.addWeixinMenuConfig();" value="添加" />
             </td>
          </tr>
     </table>
   	<table class="tb">
      <tr>
      	  <th>微信公众号名称</th>
          <th>菜单级别</th>
          <th>菜单名称</th>
          <th>父级菜单名称</th>
          <th>菜单类型</th>
          <!-- <th>菜单URL</th> -->
          <th>操作</th>
       </tr>
      <tbody id="weixinMenuConfigList">
      </tbody>
     </table>
    
</body>
</html>

