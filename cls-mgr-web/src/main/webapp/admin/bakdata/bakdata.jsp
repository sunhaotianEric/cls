<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<html>
<head>
    <meta charset="utf-8" />
    <title>数据清空</title>
    <jsp:include page="/admin/include/include.jsp"></jsp:include>
    <!-- js -->
    <script type="text/javascript" src="<%=path%>/admin/bakdata/js/bakdata.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
    <!-- dwr -->
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerSystemAction.js'></script>
    <script type='text/javascript' src='<%=path%>/dwr/interface/managerLoginLogAction.js'></script>
</head>
<body>
    <div class="alert alert-info">当前位置<b class="tip"></b>网站信息管理<b class="tip"></b>数据清空</div>
   	<table class="tbform list">
		<thead>
            <tr class="tr">
                <th colspan="6">业务数据清理<i class="tip-up"></i></th>
            </tr>
        </thead>
		<tbody>
			<tr>
				<td colspan="3" align="left">
					<br>
					<input type="checkbox" name="order"/>投注历史
					<input type="checkbox" name="moneyDetail"/>资金明细
					<input type="checkbox" name="notes"/>收件箱
					<input type="checkbox" name="drawOrder"/>充值取现订单
					<input type="checkbox" name="login"/>登陆日志
					<input type="checkbox" name="lotteryCode"/>开奖号码
					<input type="checkbox" name="operateLog"/>操作日志
					<input type="checkbox" name="useMoneyLog"/>用户资金日志
					<input type="checkbox" name="lotteryCache"/>投注缓存
					<input type="checkbox" name="recordLog"/>操作明细
					<br>
					<input class="Wdate" onClick="WdatePicker()" size="20" readonly="readonly" type="text" id="delNormalEndTime" />
					<input class="btn" id="delNormalButton" type="button" onclick="bakDataPage.deleteAgoDataNormalFromThisTime()" value="删除" />
					<input class="btn" type="button" value="全选" id="checkAllNormal"/>
					<input class="btn" type="button" value="取消全选" id="uncheckAllNormal"/>
					 注：常规数据精确删除(只能删除<%= ConstantUtil.DELETE_AGO_DATA_NORMAL %>天之前的数据)
				</td>
			</tr>
			<tr>
				<td colspan="3" align="left">
					<br>
					<input type="checkbox" name="treatmentApply"/> 代理福利申请
					<input type="checkbox" name="treatmentOrder"/> 代理福利订单
					<input type="checkbox" name="quotaDetail"/> 配额明细
					<br>
					<input class="Wdate" onClick="WdatePicker()" size="20" readonly="readonly" type="text" id="delUnNormalEndTime" />
					<input class="btn" id="delUnNormalButton" type="button" onclick="bakDataPage.deleteAgoDataUnNormalFromThisTime()" value="删除" />
					<input class="btn" type="button" value="全选" id="checkAllUnNormal"/>
					<input class="btn" type="button" value="取消全选" id="uncheckAllUnNormal"/>
					 注：非常规数据删除(只能删除<%= ConstantUtil.DELETE_AGO_DATA_NUNORMAL %>天之前的数据)
					 
					
				</td>
			</tr>
		</tbody>
	</table>
	<table class="tbform list">
		<thead>
            <tr class="tr">
                <th colspan="6">登录日志数据清理<i class="tip-up"></i></th>
            </tr>
        </thead>
		<tbody>
			<tr>
  			<td colspan="3" align="left">
             	删除登录日期:&nbsp;&nbsp;<input class="Wdate" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" size="21" readonly="readonly" type="text" id="delstartime" />
             	至<input id="delendtime" class="Wdate" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" size="21" readonly="readonly" type="text" />&nbsp;&nbsp;的数据 <input class="btn" id="find" type="button" onclick="bakDataPage.delLoginLog()" value="删除" />
             </td>
		</tbody>
	</table>
	
	<table class="tbform list">
		<thead>
            <tr class="tr">
                <th colspan="6">用户数据清理<i class="tip-down"></i></th>
            </tr>
        </thead>      
	    <tbody style="display: none;">  
	        <tr>
	         <td colspan="3" align="left">
	           <select id='dayCount' >
	             <option value="30">30天</option>
	             <option value="45">45天</option>
	             <option value="60">60天</option>
	             <option value="90">90天</option>
	             <option value="120">120天</option>
	             <option value="150">150天</option>
	             <option value="180">180天</option>
	             <option value="210">210天</option>
	             <option value="240">240天</option>
	             <option value="270">270天</option>
	             <option value="300">300天</option>
	             <option value="330">330天</option>
	             <option value="360">360天</option>
	             <option value="450">450天</option>
	             <option value="540">540天</option>
	             <option value="600">600天</option>
	             <option value="720">720天</option>             
	           </select>   
	           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                                      
	           <input class="btn" type="button" id='dayCountButton' value="删除" onclick="bakDataPage.deleteNoMoneyForNumberOfDays()" />
	                               注：删除多少天以上没有登陆且余额为0的会员或者代理,记录保存,会员无效。
	         </td>
	        </tr>
	        <tr>
	         <td colspan="3" align="left">
	           <select id='dayOneCount' >
	             <option value="30">30天</option>
	             <option value="45">45天</option>
	             <option value="60">60天</option>
	             <option value="90">90天</option>
	             <option value="120">120天</option>
	             <option value="150">150天</option>
	             <option value="180">180天</option>
	             <option value="210">210天</option>
	             <option value="240">240天</option>
	             <option value="270">270天</option>
	             <option value="300">300天</option>
	             <option value="330">330天</option>
	             <option value="360">360天</option>
	             <option value="450">450天</option>
	             <option value="540">540天</option>
	             <option value="600">600天</option>
	             <option value="720">720天</option>   
	           </select>   
	           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                                         
	           <input class="btn" type="button" id='dayOneCountButton' value="删除" onclick="bakDataPage.deleteForNumberOfDays()" />
	                                 注：删除多少天以上没有登陆的会员或者代理,不管有没有余额,记录保存,会员无效。
	         </td>
	        </tr>
	        <tr>
	         <td colspan="3" align="left">
	           <input type="checkbox" name="userSafetyQuestions"/>删除绑定安全问题的会员
	           <input type="checkbox" name="userBank"/>删除绑定银卡的会员
	          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="btn" type="button" value="全选" id="everycheckAllNormal"/>
			   <input class="btn" type="button" value="取消全选" id="everyUncheckAllNormal"/>
	         	<br>
	                <input class="Wdate" onClick="WdatePicker()" size="20" readonly="readonly" type="text" id="delEveryNormalEndTime" />
	                                   模式：<input type="text" name="sscRebate" id="sscRebate"/>
	           <input class="btn" type="button" id='everyCountButton' value="彻底删除无效会员" onclick="bakDataPage.everyDeleteUsers()" />
	            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	                                    注：彻底删除无效会员及会员绑定的银行卡信息和会员设置的安全问题，删除的数据将不能恢复，请谨慎操作，切记，切记!
	         </td>
	        </tr>  
       </tbody>
     </table>
</body>
</html>

