function BakDataPage(){}
var bakDataPage = new BakDataPage();

bakDataPage.param = {};

$(document).ready(function() {

	var _this = $('.list').find('thead');
	//折叠
	_this.click(function () {
		var i = $(this).find('i');
		if (i.attr('class') == 'tip-down') { i.attr('class', 'tip-up'); } else { i.attr('class', 'tip-down'); }
		$(this).parent().find('tbody').toggle();
	});

	//常规数据全选
	$("#checkAllNormal").click(function(){
		$("[name='order']").attr("checked",'true');
		$("[name='moneyDetail']").attr("checked",'true');
		$("[name='notes']").attr("checked",'true');
		$("[name='drawOrder']").attr("checked",'true');
		$("[name='login']").attr("checked",'true');
		$("[name='lotteryCode']").attr("checked",'true');
		$("[name='operateLog']").attr("checked",'true');
		$("[name='useMoneyLog']").attr("checked",'true');
		$("[name='lotteryCache']").attr("checked",'true');
		$("[name='recordLog']").attr("checked",'true');
	});

	//常规数据取消全选
	$("#uncheckAllNormal").click(function(){
		$("[name='order']").removeAttr("checked");
		$("[name='moneyDetail']").removeAttr("checked");
		$("[name='notes']").removeAttr("checked");
		$("[name='drawOrder']").removeAttr("checked");
		$("[name='login']").removeAttr("checked");
		$("[name='lotteryCode']").removeAttr("checked");
		$("[name='operateLog']").removeAttr("checked");
		$("[name='useMoneyLog']").removeAttr("checked");
		$("[name='lotteryCache']").removeAttr("checked");
		$("[name='recordLog']").removeAttr("checked");
	});

	//非常规数据全选
	$("#checkAllUnNormal").click(function(){
		$("[name='treatmentApply']").attr("checked",'true');
		$("[name='treatmentOrder']").attr("checked",'true');
		$("[name='quotaDetail']").attr("checked",'true');
	});

	//非常规数据取消全选
	$("#uncheckAllUnNormal").click(function(){
		$("[name='treatmentApply']").removeAttr("checked");
		$("[name='treatmentOrder']").removeAttr("checked");
		$("[name='quotaDetail']").removeAttr("checked");
	});

	//彻底删除数据全选
	$("#everycheckAllNormal").click(function(){
		$("[name='userSafetyQuestions']").attr("checked",'true');
		$("[name='userBank']").attr("checked",'true');
	});

	//彻底删除数据取消全选
	$("#everyUncheckAllNormal").click(function(){
		$("[name='userSafetyQuestions']").removeAttr("checked");
		$("[name='userBank']").removeAttr("checked");
	});
});



/**
 * 删除常规数据
 */
BakDataPage.prototype.deleteAgoDataNormalFromThisTime = function(){
	if($("#delNormalEndTime").val() == ""){
		alert("时间未选择");
		return;
	}
	$("#delNormalEndTime").attr('disabled','disabled');
	$("#delNormalButton").val("删除中.....");
	var delEndTime = new Date($("#delNormalEndTime").val());
	var deleteDataInfo = {};
	if($("[name='order']").prop('checked')) {
		deleteDataInfo.isDeleteOrder = 1;
	}
	if($("[name='moneyDetail']").prop('checked')) {
		deleteDataInfo.isDeleteMoneyDetail = 1;
	}
	if($("[name='notes']").prop('checked')) {
		deleteDataInfo.isDeleteNotes = 1;
	}
	if($("[name='drawOrder']").prop('checked')) {
		deleteDataInfo.isDeleteDrawOrder = 1;
	}
	if($("[name='login']").prop('checked')) {
		deleteDataInfo.isDeleteLogin = 1;
	}
	if($("[name='lotteryCode']").prop('checked')) {
		deleteDataInfo.isDeleteLotteryCode = 1;
	}
	if($("[name='operateLog']").prop('checked')) {
		deleteDataInfo.isDeleteOperateLog = 1;
	}
	if($("[name='useMoneyLog']").prop('checked')) {
		deleteDataInfo.isDeleteUseMoneyLog = 1;
	}
	if($("[name='lotteryCache']").prop('checked')) {
		deleteDataInfo.isDeleteLotteryCache = 1;
	}
	if($("[name='recordLog']").prop('checked')) {
		deleteDataInfo.isDeleteRecordLog = 1;
	}
	managerSystemAction.deleteAgoDataNormalFromThisTime(delEndTime, deleteDataInfo,function(r){
		if (r[0] != null && r[0] == "ok") {
			alert("数据删除成功.");
			$("#delNormalEndTime").removeAttr("disabled");
			$("#delNormalButton").val("删除");
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
			$("#delNormalEndTime").removeAttr("disabled");
			$("#delNormalButton").val("删除");
		}else{
			showErrorDlg(jQuery(document.body), "数据删除请求失败.");
			$("#delNormalEndTime").removeAttr("disabled");
			$("#delNormalButton").val("删除");
		}
	});
};

/**
 * 删除非常规数据
 */
BakDataPage.prototype.deleteAgoDataUnNormalFromThisTime = function(){
	if($("#delUnNormalEndTime").val() == ""){
		alert("时间未选择");
		return;
	}
	$("#delUnNormalEndTime").attr('disabled','disabled');
	$("#delUnNormalButton").val("删除中.....");
	var delEndTime = new Date($("#delUnNormalEndTime").val());
	var deleteDataInfo = {};
	if($("[name='order']").prop('checked')) {
		deleteDataInfo.isDeleteTreatmentApply = 1;
	}
	if($("[name='moneyDetail']").prop('checked')) {
		deleteDataInfo.isDeleteTreatmentOrder = 1;
	}
	if($("[name='notes']").prop('checked')) {
		deleteDataInfo.isDeleteQuotaDetail = 1;
	}
	managerSystemAction.deleteAgoDataUnNormalFromThisTime(delEndTime,deleteDataInfo,function(r){
		if (r[0] != null && r[0] == "ok") {
			alert("数据删除成功.");
			$("#delUnNormalEndTime").removeAttr("disabled");
			$("#delUnNormalButton").val("删除");
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
			$("#delUnNormalEndTime").removeAttr("disabled");
			$("#delUnNormalButton").val("删除");
		}else{
			showErrorDlg(jQuery(document.body), "数据删除请求失败.");
			$("#delUnNormalEndTime").removeAttr("disabled");
			$("#delUnNormalButton").val("删除");
		}
	});
};



/**
 * 删除多少天以上没有消费及余额为0的会员
 */
BakDataPage.prototype.deleteNoMoneyForNumberOfDays = function(){
	var dayCount = $("#dayCount").val();
	if(dayCount == null || dayCount == ""){
		alert("请输入要删除的天数.");
		return;
	}

	if(confirm("确定要删除  " + dayCount + " 天没有登陆且余额为0的会员吗？")){
		$("#dayCountButton").val("删除中......");
		$("#dayCountButton").attr('disabled','disabled');
		managerSystemAction.deleteNoMoneyForNumberOfDays(dayCount,function(r){
			if (r[0] != null && r[0] == "ok") {
				alert("删除数据成功,删除用户个数:" + r[1]);
				$("#dayCountButton").val("删除");
				$("#dayCountButton").removeAttr("disabled");
			}else if(r[0] != null && r[0] == "error"){
				alert(r[1]);
				$("#dayCountButton").val("删除");
				$("#dayCountButton").removeAttr("disabled");
			}else{
				showErrorDlg(jQuery(document.body), "请求失败.");
			}
		});		
	}
};

/**
 * 删除多少天以上没有登陆的会员
 */
BakDataPage.prototype.deleteForNumberOfDays = function(){
	var dayCount = $("#dayOneCount").val();
	if(dayCount == null || dayCount == ""){
		alert("请输入要删除的天数.");
		return;
	}

	if(confirm("确定要删除  " + dayCount + " 天没有登陆的会员或者代理吗？")){
		$("#dayOneCountButton").val("删除中......");
		$("#dayOneCountButton").attr('disabled','disabled');
		managerSystemAction.deleteForNumberOfDays(dayCount,function(r){
			if (r[0] != null && r[0] == "ok") {
				alert("删除数据成功,删除用户个数:" + r[1]);
				$("#dayOneCountButton").val("删除");
				$("#dayOneCountButton").removeAttr("disabled");
			}else if(r[0] != null && r[0] == "error"){
				alert(r[1]);
				$("#dayOneCountButton").val("删除");
				$("#dayOneCountButton").removeAttr("disabled");
			}else{
				showErrorDlg(jQuery(document.body), "请求失败.");
			}
		});		
	}
};


/**
 * 彻底删除无效会员
 */
BakDataPage.prototype.everyDeleteUsers = function(){
	if($("#delEveryNormalEndTime").val() == ""){
		alert("时间未选择");
		return;
	}
	var delEndTime = new Date($("#delEveryNormalEndTime").val());
	var deleteDataInfo = {};
	deleteDataInfo.sscRebate = $("#sscRebate").val();
	alert(deleteDataInfo.sscRebate);
	/*if(sscRebate==null || sscRebate.trim()==""){
		deleteDataIfo.sscRebate="";
	}*/
	if($("[name='userSafetyQuestions']").prop('checked')) {
		deleteDataInfo.isDeleteSafetyQuestions = 1;
	}
	if($("[name='userBank']").prop('checked')) {
		deleteDataInfo.isDeleteUserBank = 1;
	}

	if(confirm("确定要彻底删除无效的会员或者代理吗?")){
		$("#everyCountButton").val("删除中......");
		$("#everyCountButton").attr('disabled','disabled');
		managerSystemAction.everyDeleteUsers(delEndTime,deleteDataInfo,function(r){
			if (r[0] != null && r[0] == "ok") {
				alert("sssssssssssssssssssss");
				alert("删除数据成功,删除用户个数:" + r[1]);
				$("#everyCountButton").val("彻底删除无效会员");
				$("#everyCountButton").removeAttr("disabled");
			}else if(r[0] != null && r[0] == "error"){
				alert(r[1]);
				$("#everyCountButton").val("彻底删除无效会员");
				$("#everyCountButton").removeAttr("disabled");
			}else{
				showErrorDlg(jQuery(document.body), "请求失败.");
			}
		});		
	}
};


/**
 * 删除登陆日志数据
 */
BakDataPage.prototype.delLoginLog = function(){
	if (confirm("确认要删除？")){
		var delstartime = new Date($("#delstartime").val());
		var delendtime = new Date($("#delendtime").val());
		if(delstartime==null || $("#delstartime").val().trim()==""){
			alert("请输入删除起始日期");
			return;
		}
		if(delendtime==null || $("#delendtime").val().trim()==""){
			alert("请输入删除截止日期");
			return;
		}
		managerLoginLogAction.delLoginLog(delstartime,delendtime,function(r){
			if (r[0] != null && r[0] == "ok") {
				alert("登录日志清除成功!");
				 location.replace(location.href); //刷新当前页面
			}else if(r[0] != null && r[0] == "error"){
				showErrorDlg(jQuery(document.body), r[1]);
			}else{
				showErrorDlg(jQuery(document.body), "删除登录日志数据失败.");
			}
	    });
	}
};