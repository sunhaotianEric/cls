<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import = "com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>  
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>商户信息管理</title>
        <jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
    <body>
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
                    <div class="ibox ">
                        <div class="ibox-content">
                            <div class="row m-b-sm m-t-sm">
                                <form class="form-horizontal" id="queryForm">
                                    <div class="row">
                                    <c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
                                        <div class="col-sm-4">
                                            <div class="input-group m-b">
                                                <span class="input-group-addon">商户</span>
                                                 <cls:bizSel name="bizSystem" id="bizSystem" options="class:ipt form-control"/>
                                   
                                            </div>
                                        </div>
                                       
                                        <div class="col-sm-4">
                                            <div class="form-group nomargin text-right">
                                                <button type="button" class="btn btn-outline btn-default btn-kj" onclick="riskRecordPage.findRiskRecord()"><i class="fa fa-search"></i>&nbsp;查 询</button></div>
                                        </div>
                                        </c:if>
                                    </div>
                                </form>
                            </div>
                            <div class="jqGrid_wrapper">
                                <div class="ui-jqgrid ">
                                    <div class="ui-jqgrid-view">
                                        <div class="ui-jqgrid-hdiv">
                                            <div class="ui-jqgrid-hbox" style="padding-right:0">
                                                <table class="ui-jqgrid-htable ui-common-table table table-bordered" id="riskrecordTable">
                                                    <thead>
                                                        <tr class="ui-jqgrid-labels">
                                                            <th class="ui-th-column ui-th-ltr">商户</th>
                                                            <th class="ui-th-column ui-th-ltr">风控类型</th>
                                                            <th class="ui-th-column ui-th-ltr">用户姓名</th>
                                                            <th class="ui-th-column ui-th-ltr">关联用户</th>
                                                            <th class="ui-th-column ui-th-ltr">风控描述</th>
                                                            <th class="ui-th-column ui-th-ltr">创建时间</th>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!-- js -->
<script type="text/javascript"
	src="<%=path%>/admin/riskrecord/js/riskrecord.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
<!-- dwr -->
<script type='text/javascript'
	src='<%=path%>/dwr/interface/managerRiskRecordAction.js'></script>
</body>
</html>

