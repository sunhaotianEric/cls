function ChatBanEditPage(){}
var chatBanEditPage = new ChatBanEditPage();

chatBanEditPage.param = {
   	
};

$(document).ready(function() {
	var chatbanId = chatBanEditPage.param.id;  //获取查询的ID
	if(chatbanId != null){
		chatBanEditPage.editChatBan(chatbanId);
	}
});
	
/**
 * 保存帮助信息
 */
ChatBanEditPage.prototype.saveData = function(){
	var id = $("#chatbanId").val();
	var bizSystem = $("#bizSystem").val();
	var roomId = $("#roomId").val();
	var userName = $("#userName").val();
	var banMinute = $("#banMinute").val();
	
	if(currentUser.bizSystem=='SUPER_SYSTEM' && (bizSystem == null || bizSystem.trim() == "")){
		swal("请选择子系统!");
		$("#bizSystemId").focus();
		return;
	}
	if(roomId==null || roomId.trim()==""){
		swal("请输入房间ID!");
		$("#roomId").focus();
		return;
	}
	if(userName==null || userName.trim()==""){
		swal("请输入用户名!");
		$("#userName").focus();
		return;
	}
	if(banMinute==null || banMinute.trim()==""){
		swal("请选择禁言时间!");
		$("#banMinute").focus();
		return;
	}
	
	var chatban = {};
	chatban.id = id;
	chatban.bizSystem = bizSystem;
	chatban.roomId = roomId;
	chatban.userName = userName;
	chatban.banMinute = banMinute;
	managerChatBanAction.saveChatBan(chatban,function(r){
		if (r[0] != null && r[0] == "ok") {
			var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
			parent.chatBanPage.closeLayer(index);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("保存帮助信息失败.");
		}
    });
};


/**
 * 赋值帮助信息
 */
ChatBanEditPage.prototype.editChatBan = function(id){
	managerChatBanAction.selectChatBan(id,function(r){
		if (r[0] != null && r[0] == "ok") {
			$("#chatbanId").val(r[1].id);
			$("#bizSystem").val(r[1].bizSystem);
			$("#roomId").val(r[1].roomId);
			$("#userName").val(r[1].userName);
			$("#banMinute").val(r[1].banMinute);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("获取帮助信息失败.");
		}
    });
};