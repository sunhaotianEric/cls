function ChatBanPage(){}
var chatBanPage = new ChatBanPage();

chatBanPage.param = {
		
};
//分页参数
chatBanPage.pageParam = {
		pageNo : 1,
	    pageSize : defaultPageSize
};
/**
 * 查询参数
 */
chatBanPage.queryParam = {
/*		bizSystem : null,
		roomid : null,
		username: null,
		ip:null*/
};
$(document).ready(function() {
	chatBanPage.initTableData(); //查询所有的充值配置
});


/**
 * 查询数据
 */
ChatBanPage.prototype.findChatBan = function(){
	var bizSystem = $("#bizSystem").val();
	if(bizSystem == ""){
		chatBanPage.queryParam.bizSystem = null;
	}else{
		chatBanPage.queryParam.bizSystem = bizSystem;
	}
	chatBanPage.table.ajax.reload();
};


/*var ws = null;
ChatBanPage.prototype.OnSocket = function(){
	ws=new WebSocket("ws://"+TServer+":"+TPort);
	ws.onopen=function(){};
	ws.onmessage=function(e){};
	ws.onclose=function(){};
	ws.onerror=function(){};
}

ChatBanPage.prototype.sendMessage = function(bizSystem,ip,roomid,userName){
	var msg = '{"type":"UChatBan","bizSystem":"'+bizSystem+'","room_id":"'+roomid+'","ip":"'+ip+'","userName":"'+userName+'","state":"0","Txt":"'+encodeURIComponent()+'"}';
	ws.send(msg);
	ws.onclose();
}*/

/**
 * 删除
 */
ChatBanPage.prototype.delChatBan = function(id){
	   swal({
           title: "您确定要删除这条信息吗",
           text: "请谨慎操作！",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           cancelButtonText: "取消",
           confirmButtonText: "删除",
           closeOnConfirm: false
       },
       function() {
    	   managerChatBanAction.delChatBanById(id,function(r){
    		   if (r[0] != null && r[0] == "ok") {
    			   swal("删除成功！", "您已经永久删除了这条信息。", "success");
    			   chatBanPage.table.ajax.reload();
    		   }else if(r[0] != null && r[0] == "error"){
    			   swal(r[1]);
    		   }else{
    			   swal("删除活动中心信息失败.");
    		   }
    	   });
       })
};

/**
 * 新增
 */
ChatBanPage.prototype.addChatBan=function()
{
	
	 layer.open({
         type: 2,
         title: '聊天室禁言设置新增',
         maxmin: false,
         shadeClose: true,
         //点击遮罩关闭层
         area: ['70%', '80%'],
         content: contextPath + "/managerzaizst/chatban/chatban_edit.html",
         cancel: function(index){ 
        	 chatBanPage.table.ajax.reload();
      	   }
     });
}
/**
 * 编辑
 */
ChatBanPage.prototype.editChatBan=function(id)
{
	
	  layer.open({
           type: 2,
           title: '聊天室禁言设置编辑',
           maxmin: false,
           shadeClose: true,
           //点击遮罩关闭层
           area: ['70%', '80%'],
           content: contextPath + "/managerzaizst/chatban/"+id+"/chatban_edit.html",
           cancel: function(index){ 
        	   chatBanPage.table.ajax.reload();
        	   }
       });
}

ChatBanPage.prototype.closeLayer=function(index){
	layer.close(index);
	chatBanPage.table.ajax.reload();
}


ChatBanPage.prototype.initTableData = function(){
	chatBanPage.queryParam = getFormObj($("#chatBanForm"));
	chatBanPage.table = $('#chatBanTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"drawCallback": function(settings) {
			var api = this.api();
            var startIndex= api.context[0]._iDisplayStart;//获取到本页开始的条数
            api.column(0).nodes().each(function(cell, i) {
            cell.innerHTML = startIndex + i + 1;
            }); 
		 },
		"ajax":function (data, callback, settings) {
			chatBanPage.pageParam.pageSize = data.length;//页面显示记录条数
			chatBanPage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
			managerChatBanAction.getAllChatBan(chatBanPage.queryParam,chatBanPage.pageParam,function(r){
				//封装返回数据
				var returnData = {
						recordsTotal : 0,
						recordsFiltered : 0,
						data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询充值配置数据失败.");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": null},
		            {"data": "bizSystemName", 
		                 "bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false},
		            {"data": "roomId"},
		            {"data": "userName"},
		            {"data": "ip"},
		            {"data": "banMinute"},
	        		{"data": "remark"},
	        		{"data": "operator"},
	        		{"data": "createDateStr"},
	        		{"data": "updateDateStr"},
		            {
		            	"data": "id",
		            	render: function(data, type, row, meta) {
		            		var str="<a class='btn btn-info btn-sm btn-bj' onclick='chatBanPage.editChatBan("+data+")'><i class='fa fa-pencil'></i>&nbsp;编辑 </a>&nbsp;&nbsp;" +
		            		"<a class='btn btn-danger btn-sm btn-del' onclick='chatBanPage.delChatBan("+data+")'><i class='fa fa-trash' ></i>&nbsp;删除 </a>";
		            		
		            		return str ;
		            	}
		            },
		            ]
	}).api(); 
}

