<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>网站公告</title>
<jsp:include page="/admin/include/include.jsp"></jsp:include>
<link rel="stylesheet"
	href="<%=path%>/resources/kindeditor-4.1.10/themes/default/default.css?v=<%=SystemConfigConstant.mgrWebRsVersion%>" />
<script charset="utf-8"
	src="<%=path%>/resources/kindeditor-4.1.10/kindeditor-min.js"></script>
<script charset="utf-8"
	src="<%=path%>/resources/kindeditor-4.1.10/lang/zh_CN.js"></script>

<%
	String id = request.getParameter("id"); //获取查询的商品ID
%>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeIn">
		<div class="row back-change">
			<div class="col-sm-12">
				<div class="ibox ">
					<div class="ibox-content">
						<form class="form-horizontal">
						<input type="hidden" id="chatbanId" />
							<c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
								<div class="col-sm-12">
									<div class="input-group m-b">
										<span class="input-group-addon">商户：</span>
										<cls:bizSel name="bizSystem" id="bizSystem"
											options="class:ipt form-control" />
									</div>
								</div>
							</c:if>
							<div class="form-group">
								<label class="col-sm-2 control-label">房间号：</label>
								<div class="col-sm-10">
									<input type="email" value="" class="form-control" id="roomId"
										size="50px">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">用户名：</label>
								<div class="col-sm-10">
									<input type="email" value="" class="form-control" id="userName"
										size="50px">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">禁言时间：</label>
								<div class="col-sm-10">
									<select class="ipt" id="banMinute">
										<option value="10">10分钟</option>
										<option value="30">30分钟</option>
										<option value="60">1小时</option>
										<option value="120">2小时</option>
										<option value="360">6小时</option>
										<option value="-1">永久</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12 ibox">
									<div class="row">
										<div class="col-sm-6 text-center">
											<button type="button" class="btn btn-w-m btn-white btn－tj"
												onclick="chatBanEditPage.saveData()">提 交</button>
										</div>
										<div class="col-sm-6 text-center">
											<button type="button" class="btn btn-w-m btn-white">重
												置</button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- js -->
	<!-- js -->
	<script type="text/javascript"
		src="<%=path%>/admin/chatban/js/chatban_edit.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
	<!-- dwr -->
	<script type='text/javascript'
		src='<%=path%>/dwr/interface/managerChatBanAction.js'></script>
	<script type="text/javascript">
    chatBanEditPage.param.id = <%=id%>;
	</script>
</body>
</html>

