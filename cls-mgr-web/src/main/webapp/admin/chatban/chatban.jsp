<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
	<title>聊天室禁言设置</title>
	<jsp:include page="/admin/include/include.jsp"></jsp:include>
	<script type="text/javascript">
	var TServer = '<%=SystemConfigConstant.chatRoomIp %>';
	var TPort = '<%=SystemConfigConstant.chatRoomPort %>';
	</script>
</head>
<body>
	<div class="animated fadeIn">
		<div class="ibox-content">
			<div class="row m-b-sm m-t-sm">
				<form class="form-horizontal" id="chatBanForm">
					<div class="row">
						<c:if test="${admin.bizSystem=='SUPER_SYSTEM'}">
							<div class="col-sm-2">
								<div class="input-group m-b">
									<span class="input-group-addon">商户</span>
									<cls:bizSel name="bizSystem" id="bizSystem"
										options="class:ipt form-control" />
								</div>
							</div>
						</c:if>
						<div class="col-sm-2">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">聊天室号</span>
                                    <input id='roomid' name="roomid" type="text" value="" class="form-control" ></div>
                        </div>
						<c:if test="${admin.bizSystem!='SUPER_SYSTEM'}">
							<div class="col-sm-2"></div>
						</c:if>
						<div class="col-sm-2">
							<div class="form-group nomargin text-right">
                                    <button type="button" style="display: inline-block; vertical-align: top;" class="btn btn-outline btn-default btn-kj biz" onclick="chatBanPage.findChatBan()"><i class="fa fa-search"></i>&nbsp;查 询</button>
                                    <button type="button" style="display: inline-block; vertical-align: top;" class="btn btn-outline btn-default btn-kj" onclick="chatBanPage.addChatBan()"><i class="fa fa-plus"></i>&nbsp;新增</button>
                            </div>
						</div>
					</div>
				</form>
			</div>
			<div class="jqGrid_wrapper">
				<div class="ui-jqgrid ">
					<div class="ui-jqgrid-view">
						<div class="ui-jqgrid-hdiv">
							<div class="ui-jqgrid-hbox" style="padding-right: 0">
								<table id="chatBanTable"
									class="ui-jqgrid-htable ui-common-table table table-bordered">
									<thead>
										<tr class="ui-jqgrid-labels">
											<th class="ui-th-column ui-th-ltr">序号</th>
											<th class="ui-th-column ui-th-ltr">商户</th>
											<th class="ui-th-column ui-th-ltr">聊天室号</th>
											<th class="ui-th-column ui-th-ltr">用户名</th>
											<th class="ui-th-column ui-th-ltr">ip</th>
											<th class="ui-th-column ui-th-ltr">禁言时间</th>
											<th class="ui-th-column ui-th-ltr">备注</th>
											<th class="ui-th-column ui-th-ltr">管理员</th>
											<th class="ui-th-column ui-th-ltr">创建时间</th>
											<th class="ui-th-column ui-th-ltr">更新时间</th>
											<th class="ui-th-column ui-th-ltr">操作</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<script type="text/javascript"
		src="<%=path%>/admin/chatban/js/chatban.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
	<!-- dwr -->
	<script type='text/javascript'
		src='<%=path%>/dwr/interface/managerChatBanAction.js'></script>
</body>
</html>

