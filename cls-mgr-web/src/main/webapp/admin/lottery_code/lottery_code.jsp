<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="com.team.lottery.enums.ELotteryKind"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="cls" uri="/WEB-INF/cls.tld"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>彩票期号管理</title>
	<link rel="shortcut icon" href="favicon.ico">
	<jsp:include page="/admin/include/include.jsp"></jsp:include>
</head>
<body>
	<div class="animated fadeIn">
		<div class="ibox-content">
			<div class="row m-b-sm m-t-sm">
				<form class="form-horizontal" id="lotteryCodeForm">
					<div class="row">
						<div class="col-sm-3">
							<div class="input-group m-b">
								<span class="input-group-addon">选择彩种</span>
								<select id='lotteryTypeRegression' class="ipt form-control" name="lotteryName">
								</select>
								<%--   <select id="lotteryName" name="lotteryName" class="ipt form-control" style="">
                                      <option value="">＝＝请选择＝＝</option>
                                      <option value="CQSSC">重庆时时彩</option>
                                      <option value="JXSSC">江西时时彩</option>
                                      <option value="TJSSC">天津时时彩</option>
                                      <option value="XJSSC">新疆时时彩</option>
                                      <option value="HLJSSC">黑龙江时时彩</option>
                                      <option value="GDSYXW">广东11选5</option>
                                      <option value="SDSYXW">山东11选5</option>
                                      <option value="JXSYXW">江西11选5</option>
                                      <option value="CQSYXW">重庆11选5</option>
                                      <option value="FJSYXW">福建11选5</option>
                                      <option value="JSKS">江苏快3</option>
                                      <option value="FJKS">福建快3</option>
                                      <option value="AHKS">安徽快3</option>
                                      <option value="HBKS">湖北快3</option>
                                      <option value="JLKS">吉林快3</option>
                                      <option value="BJKS">北京快3</option>
                                      <option value="GXKS">广西快3</option>
                                      <option value="GSKS">甘肃快3</option>
                                      <option value="SHKS">上海快3</option>
                                      <option value="GDKLSF">广东快乐十分</option>
                                      <option value="SHSSLDPC">上海时时乐</option>
                                      <option value="FCSDDPC">福彩3D</option>
                                      <option value="BJPK10">北京PK10</option>
                                      <option value="HGFFC">韩国1.5分彩</option>
                                      <option value="XJPLFC">新加坡2分彩</option>
                                      <option value="TWWFC">台湾五分彩</option>
                                      <option value="XYEB">幸运28</option>
                                      <option value="LHC">六合彩</option>
                                   </select> --%>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="input-group m-b">
								<span class="input-group-addon">期号</span> <input type="text"
									value="" class="form-control" name="lotteryNum">
							</div>
						</div>
						<div class="col-sm-5">
							<div class="input-group m-b">
								<span class="input-group-addon">日期</span>
								<div class="input-daterange input-group" id="datepicker">
									<input class="form-control layer-date" placeholder="开始日期"
										name="startime"
										onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})">
									<span class="input-group-addon">到</span> <input
										class="form-control layer-date" placeholder="结束日期"
										name="endtime"
										onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})">
								</div>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group text-right">
								<button type="button" class="btn btn-outline btn-default btn-js"
									onclick="lotteryCodePage.queryConditionLotteryCodes()">
									<i class="fa fa-search"></i>&nbsp;查 询
								</button>
							<c:if test="${admin.bizSystem == 'SUPER_SYSTEM'}">
								<button type="button" class="btn btn-outline btn-default btn-js"
									onclick="lotteryCodePage.synQueryConditionLotteryCodes()">
									<i class="fa fa-arrows"></i>&nbsp;同步
								</button>
							</c:if>
							</div>
						</div>	
					</div>
				</form>
			</div>
			<div class="jqGrid_wrapper">
				<div class="ui-jqgrid ">
					<div class="ui-jqgrid-view">
						<div class="ui-jqgrid-hdiv">
							<div class="ui-jqgrid-hbox" style="padding-right: 0">
								<table id="lotteryCodeTable"
									class="ui-jqgrid-htable ui-common-table table table-bordered">
									<thead>
										<tr class="ui-jqgrid-labels">
											<th class="ui-th-column ui-th-ltr">彩种</th>
											<th class="ui-th-column ui-th-ltr">期号</th>
											<th class="ui-th-column ui-th-ltr">开奖结果</th>
											<th class="ui-th-column ui-th-ltr">开奖时间</th>
											<th class="ui-th-column ui-th-ltr">录入时间</th>
											<th class="ui-th-column ui-th-ltr">是否开奖处理</th>
											<th class="ui-th-column ui-th-ltr">操作</th>
										</tr>
									</thead>
									<tbody id="lotteryCodeList">
										<!-- <tr role="row" tabindex="-1" class="jqgrow ui-row-ltr">
                                                    <td>北京快3</td>
                                                    <td>62838</td>
                                                    <td>3,6,6</td>
                                                    <td>2016-11-23 20:46:56</td>
                                                    <td>2016-11-23 20:46:56</td>
                                                    <td>是</td>
                                                    <td>
                                                        <a class="btn btn-danger btn-sm btn-del">
                                                            <i class="fa fa-trash"></i>删除</a>
                                                    </td>
                                                </tr>
                                                <tr role="row" tabindex="-1" class="jqgrow ui-row-ltr">
                                                    <td>北京快3</td>
                                                    <td>62838</td>
                                                    <td>3,6,6</td>
                                                    <td>2016-11-23 20:46:56</td>
                                                    <td>2016-11-23 20:46:56</td>
                                                    <td>是</td>
                                                    <td>
                                                        <a class="btn btn-danger btn-sm btn-del">
                                                            <i class="fa fa-trash"></i>删除</a>
                                                    </td>
                                                </tr>
                                                <tr role="row" tabindex="-1" class="jqgrow ui-row-ltr">
                                                    <td>北京快3</td>
                                                    <td>62838</td>
                                                    <td>3,6,6</td>
                                                    <td>2016-11-23 20:46:56</td>
                                                    <td>2016-11-23 20:46:56</td>
                                                    <td>是</td>
                                                    <td>
                                                        <a class="btn btn-danger btn-sm btn-del">
                                                            <i class="fa fa-trash"></i>删除</a>
                                                    </td>
                                                </tr> -->
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- <div class="row">
                                <div class="col-sm-6">
                                    <div class="dataTables_info" id="editable_info" role="alert" aria-live="polite" aria-relevant="all">当前第1页/共5页，总共100条记录</div></div>
                                <div class="col-sm-6">
                                    <div class="dataTables_paginate paging_simple_numbers" id="editable_paginate">
                                        
                                        <ul class="pagination">
                                        	<li class="paginate_button previous disabled" aria-controls="editable" tabindex="0" id="editable_previous">
                                                <a href="#">首页</a></li>
                                            <li class="paginate_button previous disabled" aria-controls="editable" tabindex="0" id="editable_previous">
                                                <a href="#">上一页</a></li>
                                            <li class="paginate_button active" aria-controls="editable" tabindex="0">
                                                <a href="#">1</a></li>
                                            <li class="paginate_button " aria-controls="editable" tabindex="0">
                                                <a href="#">2</a></li>
                                            <li class="paginate_button " aria-controls="editable" tabindex="0">
                                                <a href="#">3</a></li>
                                            <li class="paginate_button " aria-controls="editable" tabindex="0">
                                                <a href="#">4</a></li>
                                            <li class="paginate_button " aria-controls="editable" tabindex="0">
                                                <a href="#">5</a></li>
                                            <li class="paginate_button " aria-controls="editable" tabindex="0">
                                                <a href="#">6</a></li>
                                            <li class="paginate_button next" aria-controls="editable" tabindex="0" id="editable_next">
                                                <a href="#">下一页</a></li>
                                            <li class="paginate_button next" aria-controls="editable" tabindex="0" id="editable_next">
                                                <a href="#">尾页</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div> -->
				</div>
			</div>
		</div>
	</div>

	<!-- dwr -->
	<script type='text/javascript'
		src='<%=path%>/dwr/interface/managerLotteryCodeAction.js'></script>
	<!-- js -->
	<script type="text/javascript"
		src="<%=path%>/admin/lottery_code/js/lottery_code.js?v=<%=SystemConfigConstant.mgrWebRsVersion%>"></script>
	<script type='text/javascript' src='<%=path%>/dwr/interface/managerOrderAction.js'></script>
</body>

</html>
