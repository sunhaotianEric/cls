window.lotteryName='';
function LotteryCodePage(){}
var lotteryCodePage = new LotteryCodePage();

/**
 * 查询参数
 */
lotteryCodePage.queryParam = {
	lotteryName : null,
	lotteryNum : null,
	startime : null,
	endtime : null
};

//分页参数
lotteryCodePage.pageParam = {
    pageNo : 1,
    pageSize : defaultPageSize
};

function HandUpdatePage(){}
var handUpdatePage = new HandUpdatePage();
handUpdatePage.param = {
};
/**
 * 加载彩种下拉
 */
HandUpdatePage.prototype.getAllTypes = function(){
	managerOrderAction.getOfficialLotteryKinds(function(r){
		if (r[0] != null && r[0] == "ok") {
			handUpdatePage.refreshTypes(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("查询彩种类型失败.");
		}
	});
};
/**
 * 展示彩种下拉框
 */
HandUpdatePage.prototype.refreshTypes = function(detailTypeMaps){
	var obj3=document.getElementById('lotteryTypeRegression');
	obj3.options.add(new Option("=请选择=",""));
	for(var key in detailTypeMaps){
		obj3.options.add(new Option(detailTypeMaps[key],key));
	}

};

$(document).ready(function() {
	lotteryCodePage.initTableData();
	handUpdatePage.getAllTypes();
	/*$("#lotteryName").find("option[value='JYKS']").remove();
    $("#lotteryName").find("option[value='JLFFC']").remove();
    $("#lotteryName").find("option[value='JSPK10']").remove();
    $("#lotteryName").find("option[value='SFSSC']").remove();
    $("#lotteryName").find("option[value='WFSSC']").remove();
    $("#lotteryName").find("option[value='SHFSSC']").remove();
    $("#lotteryName").find("option[value='SFKS']").remove();
    $("#lotteryName").find("option[value='WFKS']").remove();
    $("#lotteryName").find("option[value='SFPK10']").remove();
    $("#lotteryName").find("option[value='WFPK10']").remove();
    $("#lotteryName").find("option[value='SHFPK10']").remove();*/
});

/**
 * 初始化列表数据
 */
LotteryCodePage.prototype.initTableData = function() {
	lotteryCodePage.queryParam = getFormObj($("#lotteryCodeForm"));
	lotteryCodePage.table = $('#lotteryCodeTable').dataTable({
		"oLanguage" : dataTableLanguage,
		"bProcessing" : false,
		"bServerSide" : true,
		"iDisplayLength" : defaultPageSize,
		"ajax":function (data, callback, settings) {
			lotteryCodePage.pageParam.pageSize = data.length;//页面显示记录条数
			lotteryCodePage.pageParam.pageNo = (data.start / data.length)+1;//当前页码
			    	managerLotteryCodeAction.getAllLotteryCode(lotteryCodePage.queryParam,lotteryCodePage.pageParam,function(r){
				//封装返回数据
				var returnData = {
						recordsTotal : 0,
						recordsFiltered : 0,
						data : null
				};
				if (r[0] != null && r[0] == "ok") {
					returnData.recordsTotal = r[1].totalRecNum;//返回数据全部记录
					returnData.recordsFiltered = r[1].totalRecNum;//后台不实现过滤功能，每次查询均视作全部结果
					returnData.data = r[1].pageContent;//返回的数据列表
				}else if(r[0] != null && r[0] == "error"){
					swal(r[1]);
				}else{
					swal("查询奖金期号数据失败.");
				}
				callback(returnData);
			});
		},
		"columns": [
		            {"data": "lotteryTypeDes"},
		            {"data": "lotteryNum"},
		            {"data": "codesStr"},
		            {"data": "kjtimeStr"},
		            {"data": "addtimeStr"},
		            {
		            	"data": "prostateDeal",
		            	render: function(data, type, row, meta) {
		            		if (data == "1") {
		            			return '是';
		            		} else {
		            			return '否';
		            		}
		            		return data;
	            		}
		            },
		            {
	                	 "data": "id",
	                	 render: function(data, type, row, meta) {
	                		 return '<a class="btn btn-danger btn-sm btn-del" onclick="lotteryCodePage.deleteLotteryCodeById('+data+')"><i class="fa fa-trash"></i>&nbsp;删除</a>';
	                	 },"bVisible" : currentUser.bizSystem=='SUPER_SYSTEM'?true:false
	                }
	            ]
	}).api(); 
}

/**
 * 条件查询奖金期号
 */
LotteryCodePage.prototype.queryConditionLotteryCodes = function(){
	lotteryCodePage.queryParam = getFormObj($("#lotteryCodeForm"));
	lotteryCodePage.table.ajax.reload();
};


/**
 * 同步条件查询奖金期号
 */
LotteryCodePage.prototype.synQueryConditionLotteryCodes = function(){
	lotteryCodePage.queryParam = getFormObj($("#lotteryCodeForm"));
	window.lotteryName = $('#lotteryName').val();
	if(window.lotteryName==null || window.lotteryName==''){
		swal({
	           title: "请选择彩种",
	           text: "否则无法继续操作！",
	           type: "warning",
	           showCancelButton: true,
	           confirmButtonColor: "#DD6B55",
	           cancelButtonText: "取消",
	           confirmButtonText: "OK",
	           closeOnConfirm: true
	           
	       },
	       function() {
	       });
	}
	if(window.lotteryName!=null && window.lotteryName!=''){
		managerLotteryCodeAction.resetRedisLotteryCode(lotteryCodePage.queryParam,function(r){
			if (r[0] != null && r[0] == "ok") {
				swal("同步奖金期号数据到redis成功.");
			}else if(r[0] != null && r[0] == "error"){
				swal(r[1]);
			}else{
				swal("同步奖金期号数据到redis失败.");
			}
		})
	}
};


/**
 * 删除对应的开奖数据
 */
LotteryCodePage.prototype.deleteLotteryCodeById = function(lotteryCodeId){
	LotteryCodePage.syn = false;
	managerLotteryCodeAction.deleteLotteryCodeById(lotteryCodeId,function(r){
		if (r[0] != null && r[0] == "ok") {
			swal({ title: "提示", text: "删除成功",type: "success"});
			lotteryCodePage.table.ajax.reload();
		}else if(r[0] != null && r[0] == "error"){
			swal(r[1]);
		}else{
			swal("查询奖金期号数据失败.");
		}
    });
};


