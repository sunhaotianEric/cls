

/*
 *  getFormObjStr  getFormObj
 *  获取表单值，并转为字符串
 *  param:  $form       表单的jQuery对象
 *          exception   排除不需要的项
 *  author: zsk 2014-07-31 
 */
;(function(window) {

    /*
     *  调用方法: CommonScript.getFormObjStr($form, exception);
     *  getFormObjStr  getFormObj
     *  获取表单值，并转为字符串
     *  param:  $form       表单的jQuery对象
     *          exception   排除不需要的项
     *  author: zsk 2014-07-31 
     */
  window.getFormObj = function($form, exception) {
        var param = {};
        $("[name]", $form).each(function() {

            // 如果name为空不执行：修改：yyp 2016-03-25
            if ($(this).attr('name')) {    
                var $this = $(this);
                //处理日期类型的时间
                if($this.hasClass("layer-date")) {
                	var dateStr = $this.val();
                	param[$this.attr("name")] = dateStr.stringToDate();
                } else {
                	if ($this.attr("type") == "radio") {
                		if ($this.prop("checked")) {
                			param[$this.attr("name")] = $.trim($this.val());
                		}
                	}else if($this.attr("type") == "checkbox"){
                		if ($this.prop("checked")) {
                			param[$this.attr("name")] === undefined ? 
                					param[$this.attr("name")] = $.trim($this.val()) : 
                						param[$this.attr("name")] += (',' + $.trim($this.val()));
                		}
                	} else {
                		param[$this.attr("name")] = $.trim($this.val()) == "" ? null : $.trim($this.val());
                	}
                }
            }
        });
        if(exception){ // delete exception
            for(var i = 0, j = exception.length; i < j; i++){
                delete param[exception[i]];
            }
        }
        
        return param;
    }; 
     
 window.getFormObjStr = function($form, exception) {
        var paramObj = window.getFormObj($form, exception);
        return JSON.stringify(paramObj);
    };

}(window));







 

