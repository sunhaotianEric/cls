package com.team.lottery.scriptsession;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.directwebremoting.ScriptSession;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.directwebremoting.event.ScriptSessionEvent;
import org.directwebremoting.event.ScriptSessionListener;

import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ConstantUtil;


/**
 * 在线ScriptSession(SSL)监听管理器.
 * 在SSL中,每当DWR工厂在生成一个新的ScriptSession(SS)时,将被SSL捕获
 * SSL对捕获的SS进行初始化赋值
 * 其中在SS属性中的SessionLabel.CurrentSesionID赋予当前用户的SessinID
 * 并在改用户的Session属性中的SessionLabel.CurrentScriptSessionID赋予当前的SSL
 * 然后在SessionLabel.CurrentPage中赋予当前SS的操作界面地址
 * 
 * 并且开始激活SSL插件中的sessionCreated方法
 * 
 * 当
 * @author 熊浩华 - ibmsz
 *
 * 2009-8-18:下午03:11:55-
 */
public class CustomScriptSessionListener  implements ScriptSessionListener  {

	//维护一个Map key为session的Id， value为ScriptSession对象  
    public static Map<String, ScriptSession> scriptSessionMap; 
//	public static HttpSessionLabel SessionLabel = null;new HttpSessionLabel();
	
	static{
		scriptSessionMap = new HashMap<String, ScriptSession>();
	}

	@Override
	public void sessionCreated(ScriptSessionEvent event) {
		WebContext webContext = WebContextFactory. get();  
        HttpSession session = webContext.getSession();  
        ScriptSession scriptSession = event.getSession();  
        
		if(BaseDwrUtil.getCurrentAdmin() != null){
//			if((scriptSession.getPage().endsWith("/managerzaizst/rechargeorder/rechargeorder.html") || 
//	        		 scriptSession.getPage().endsWith("/managerzaizst/withdraworder/withdraworder.html"))){
//				scriptSessionMap.put(session.getId(), scriptSession);     //添加scriptSession 
//	            OrderNotify orderNotify = this.getOrderNotify(event.getSession());
//	            if(orderNotify == null){
//	            	orderNotify = new OrderNotify();
//	            }
//	        	orderNotify.setAdminId(BaseDwrUtil.getCurrentAdmin().getId());
//	        	orderNotify.setAdminName(BaseDwrUtil.getCurrentAdmin().getUsername());
//	        	orderNotify.setState(BaseDwrUtil.getCurrentAdmin().getState());
//	         	orderNotify.setCurrID(event.getSession().getId());
//	        	event.getSession().setAttribute(ConstantUtil.ORDER_NOTIFY_SCRIPT_SESSION, orderNotify);
//			}
		}
	}

	@Override
	public void sessionDestroyed(ScriptSessionEvent event) {
		WebContext webContext = WebContextFactory. get();  
        HttpSession session = webContext.getSession();  
        ScriptSession scriptSession = scriptSessionMap.remove(session.getId());  //移除scriptSession  
        event.getSession().removeAttribute(ConstantUtil.ORDER_NOTIFY_SCRIPT_SESSION);
//      event.getSession().invalidate();
	}  
    
	/** 
     * 获取所有ScriptSession 
     */  
    public static Collection<ScriptSession> getScriptSessions(){  
           return scriptSessionMap.values();  
    }  
    
//	public OrderNotify getOrderNotify(ScriptSession scriptSession) {
//		if(scriptSession == null){
//			return null;
//		}else if(scriptSession.getAttribute(ConstantUtil.ORDER_NOTIFY_SCRIPT_SESSION) == null){
//			return null; 
//		}else {
//			return (OrderNotify)scriptSession.getAttribute(ConstantUtil.ORDER_NOTIFY_SCRIPT_SESSION);
//		}
//	}
//	
	public static class HttpSessionLabel{
		
		private final String CurrentScriptSessionID = "DWR3.CurrScriptSession";
		
		private final String CurrentScritpUserName = "DWR.Chat.UserName";
		
		private final String CurrentSesionID = "DWR3.CurrSessionID";

		private final String CurrentPage = "DWR3.CurrPath";
		
		/**
		 * 获取当前SessionScript的在线页面
		 * @return currentPage
		 */
		public String getCurrentPage() {
			return CurrentPage;
		}

		/**
		 * 获取Session中的当前ScriptSession的ID
		 * @return currentScriptSessionID
		 */
		public String getCurrentScriptSessionID() {
			return CurrentScriptSessionID;
		}

		/**
		 * 获取当前用户名称
		 * @return currentScritpUserName
		 */
		public String getCurrentScritpUserName() {
			return CurrentScritpUserName;
		}

		/**
		 * 获取ScriptSession中的HttpSessionID
		 * @return currentSesionID
		 */
		public String getCurrentSesionID() {
			return CurrentSesionID;
		}
		
	}
}