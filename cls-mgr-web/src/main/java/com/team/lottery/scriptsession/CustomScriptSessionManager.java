package com.team.lottery.scriptsession;



public class CustomScriptSessionManager extends org.directwebremoting.impl.DefaultScriptSessionManager  {

	public CustomScriptSessionManager(){
		try {
			this.addScriptSessionListener(new CustomScriptSessionListener());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
