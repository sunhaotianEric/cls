package com.team.lottery.redis.mq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MgrRedisPubSubListener extends  RedisPubSubListener{
	private static Logger logger = LoggerFactory.getLogger(ClsRedisPubSubListener.class);
	
	@Override
	public void onMessage(String channel, String message) {
		try {
	
		} catch (Exception e) {
			logger.error(e.getMessage());
		}	
		logger.info("onMessage: channel[" + channel + "], message[" + message + "]");  
		}
}
