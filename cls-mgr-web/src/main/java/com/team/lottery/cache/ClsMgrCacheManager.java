package com.team.lottery.cache;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.ehcache.EhCacheCacheManager;

import com.team.lottery.extvo.AdminDomainQuery;
import com.team.lottery.service.AdminDomainService;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.vo.AdminDomain;

import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;

public class ClsMgrCacheManager {

	private static Logger logger = LoggerFactory.getLogger(ClsMgrCacheManager.class);
	
	//后台域名管理缓存name
	public static final String MGR_BIZ_SYSTEM_DOMAIN_CACHE = "mgrBizSystemDomainCache";
	
	private static EhCacheCacheManager cacheManager;
	
	static {
		cacheManager = ApplicationContextUtil.getBean(EhCacheCacheManager.class);
	}
	
	private static AdminDomainService adminDomainService = ApplicationContextUtil.getBean(AdminDomainService.class);
	
	/**
	 * 初始后台域名IP授权缓存
	 */
	public static void initMgrBizSystemDomainCache() {
		
		AdminDomainQuery query=new AdminDomainQuery();
		List<AdminDomain> adminBizSystemDomains =  adminDomainService.getAllAdminDomain(query);
		
		Cache cache = cacheManager.getCacheManager().getCache(MGR_BIZ_SYSTEM_DOMAIN_CACHE);
		if(cache != null) {
			cache.removeAll();
			if(CollectionUtils.isNotEmpty(adminBizSystemDomains)) {
				for(AdminDomain adminDomain : adminBizSystemDomains) {
					Element element = new Element(adminDomain.getId(), adminDomain);
					cache.put(element);
				}
			}
		}
		logger.info("初始化后台域名IP授权缓存结束...");
	}
}
