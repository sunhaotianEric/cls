package com.team.lottery.tag;


import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import com.team.lottery.util.EumnUtil;

public class EnumTag extends SimpleTagSupport {

	public void doTag() throws JspException, IOException {
		JspWriter out = getJspContext().getOut();
		Class enumClass = null;
		try {
			enumClass = Class.forName(className);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		Map<String,String> enumMap=EumnUtil.parseEumn(enumClass);
		String selectHtml=this.initSelectTagHtml(options,style, enumMap);
		out.println(selectHtml);
	}
	
    /**
     * 初始化select标签
     * @param options
     * @param enumMap
     * @return
     */
	private String initSelectTagHtml(String options,String style,Map<String,String> enumMap)
	{
		options=options==null?"":options;
		style=style==null?"":style;
		String[] optionArr=options.split(",");
		StringBuffer selectOptions=new StringBuffer("<select id='"+id+"' name='"+name+"' ");
		for(String option:optionArr){
			String[] arr=option.split(":");
			if(arr.length>=2)
			{
				if("id".equals(arr[0])||"name".equals(arr[0]))
				{
					continue;
				}
				//事件带on的事件
				if(arr[0].startsWith("on")){
					selectOptions.append(arr[0]+"=\""+arr[1]+"\" ");	
				}else{
					selectOptions.append(arr[0]+"='"+arr[1].replaceAll("'", "")+"' ");	
				}
				
			}
		}
		
		selectOptions.append(" style=\""+style.replaceAll("\"", "'")+"\" >");
		if(emptyOption)
		{
			selectOptions.append("<option value=''>＝＝请选择＝＝</option> "); 	
		}
		for(Entry<String, String> entry:enumMap.entrySet()){
			//System.out.println(entry.getKey()+"--->"+entry.getValue()); 
			selectOptions.append("<option value='"+entry.getKey()+"'>"+entry.getValue()+"</option> "); 
	    } 
		selectOptions.append("</select>");
		return selectOptions.toString();
	}
	private String id;
	
	private String name;
	/**
	 * 属性参数：值，如：options="id:hhhh,name:zhuang,width:90px,height:88"
	 */
	private String options;
	
	/**
	 * 枚举类
	 */
	private String className;

	/**
	 * style属性
	 */
	private String style;
	
	/**
	 * 是否显示空选项
	 */
	private Boolean emptyOption=true;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOptions() {
		return options;
	}

	public void setOptions(String options) {
		this.options = options;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	
	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public Boolean getEmptyOption() {
		return emptyOption;
	}

	public void setEmptyOption(Boolean emptyOption) {
		this.emptyOption = emptyOption;
	}
	
	
	
	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		Class onwClass = Class.forName("com.runoob.EAHKSKind");

		System.out.println(onwClass.isEnum());
		Map<String, String> enumMap = EumnUtil.parseEumn(onwClass);
		String str = new EnumTag().initSelectTagHtml("id:'hhsshh',name:zhuang,width:90px,height:88,onclick:hhh('djkkk')", "width:\"90px'",
				enumMap);
		System.out.println(str);
	}
	
}
