package com.team.lottery.tag;


import java.io.IOException;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.service.BizSystemService;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.vo.BizSystem;

public class BizSystemTag extends SimpleTagSupport {


	public void doTag() throws JspException, IOException {
		JspWriter out = getJspContext().getOut();
	
		List<BizSystem> bizSystemlist=getAllEnableBizSystem();
		String selectHtml=this.initSelectTagHtml(options,style, bizSystemlist);
		out.println(selectHtml);
	}
	
    /**
     * 初始化select标签
     * @param options
     * @param enumMap
     * @return
     */
	private String initSelectTagHtml(String options,String style,List<BizSystem> bizSystemlist)
	{
		options=options==null?"":options;
		style=style==null?"":style;
		String[] optionArr=options.split(",");
		StringBuffer selectOptions=new StringBuffer("<select id='"+id+"' name='"+name+"' ");
		for(String option:optionArr){
			String[] arr=option.split(":");
			if(arr.length>=2)
			{
				if("id".equals(arr[0])||"name".equals(arr[0]))
				{
					continue;
				}
				//事件带on的事件
				if(arr[0].startsWith("on")){
					selectOptions.append(arr[0]+"=\""+arr[1]+"\" ");	
				}else{
					selectOptions.append(arr[0]+"='"+arr[1].replaceAll("'", "")+"' ");	
				}
				
			}
		}
		
		selectOptions.append(" style=\""+style.replaceAll("\"", "'")+"\" >");
		if(emptyOption) {
			selectOptions.append("<option value=''>＝＝请选择＝＝</option> "); 	
		}
		if(hasSuperSystemOption) {
			selectOptions.append("<option value='"+ ConstantUtil.SUPER_SYSTEM +"'>超级系统</option> "); 
		}
		for(BizSystem bizSystem:bizSystemlist){
			selectOptions.append("<option value='"+bizSystem.getBizSystem()+"'>"+bizSystem.getBizSystemName()+"</option> "); 
	    } 
		selectOptions.append("</select>");
		return selectOptions.toString();
	}
	
	 /**
     * 查找查询启用中的业务系统 
     * @return
     */
    public  List<BizSystem> getAllEnableBizSystem()
    {	 
    	List<BizSystem> bizSystemlist=null;
    	//从缓存上直接读取
    	bizSystemlist=ClsCacheManager.getAllBizSystem();
    	if(bizSystemlist.size()<1){
    		BizSystemService bizSystemService=ApplicationContextUtil.getBean(BizSystemService.class);
    		//从数据库直接取
        	bizSystemlist= bizSystemService.getAllEnableBizSystem();	
   	  }
    	
    	return bizSystemlist;
    }
    
	
	private String id;
	
	private String name;
	/**
	 * 属性参数：值，如：options="id:hhhh,name:zhuang,width:90px,height:88"
	 */
	private String options;
	
	

	/**
	 * style属性
	 */
	private String style;
	
	/**
	 * 是否显示空选项
	 */
	private Boolean emptyOption=true;
	
	/**
	 * 是否显示空选项
	 */
	private Boolean hasSuperSystemOption=false;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOptions() {
		return options;
	}

	public void setOptions(String options) {
		this.options = options;
	}
	
	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}
	
	public Boolean getEmptyOption() {
		return emptyOption;
	}

	public void setEmptyOption(Boolean emptyOption) {
		this.emptyOption = emptyOption;
	}
	
	public Boolean getHasSuperSystemOption() {
		return hasSuperSystemOption;
	}

	public void setHasSuperSystemOption(Boolean hasSuperSystemOption) {
		this.hasSuperSystemOption = hasSuperSystemOption;
	}

	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
	    
		}
	
}
