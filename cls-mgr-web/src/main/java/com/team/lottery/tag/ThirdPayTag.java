package com.team.lottery.tag;

import java.io.IOException;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.service.BizSystemService;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.vo.BizSystem;
import com.team.lottery.vo.ThirdPayConfig;

public class ThirdPayTag extends SimpleTagSupport {

	public void doTag() throws JspException, IOException {
		JspWriter out = getJspContext().getOut();

		List<ThirdPayConfig> thirdPayConfigList = getAllThirdPayConfig();
		String selectHtml = this.initSelectTagHtml(options, style, thirdPayConfigList);
		out.println(selectHtml);
	}

	/**
	 * 初始化select标签
	 * 
	 * @param options
	 * @param enumMap
	 * @return
	 */
	private String initSelectTagHtml(String options, String style, List<ThirdPayConfig> thirdPayConfigList) {
		options = options == null ? "" : options;
		style = style == null ? "" : style;
		String[] optionArr = options.split(",");
		StringBuffer selectOptions = new StringBuffer("<select id='" + id + "' name='" + name + "' ");
		for (String option : optionArr) {
			String[] arr = option.split(":");
			if (arr.length >= 2) {
				if ("id".equals(arr[0]) || "name".equals(arr[0])) {
					continue;
				}
				// 事件带on的事件
				if (arr[0].startsWith("on")) {
					selectOptions.append(arr[0] + "=\"" + arr[1] + "\" ");
				} else {
					selectOptions.append(arr[0] + "='" + arr[1].replaceAll("'", "") + "' ");
				}

			}
		}

		selectOptions.append(" style=\"" + style.replaceAll("\"", "'") + "\" >");
		
		if(emptyOption){
			selectOptions.append("<option value=''>＝＝请选择＝＝</option> "); 	
		}
		for (ThirdPayConfig thirdPayConfig : thirdPayConfigList) {
			selectOptions.append("<option value='" + thirdPayConfig.getChargeType() + "'>" + thirdPayConfig.getChargeDes() + "</option> ");
		}
		selectOptions.append("</select>");
		return selectOptions.toString();
	}

	/**
	 * 查找查询启用中的业务系统
	 * 
	 * @return
	 */
	public List<ThirdPayConfig> getAllThirdPayConfig() {
		List<ThirdPayConfig> thirdPayConfiglist = null;
		ThirdPayConfigService thirdPayConfigService = ApplicationContextUtil.getBean(ThirdPayConfigService.class);
		thirdPayConfiglist = thirdPayConfigService.getThirdPayConfig();

		// 从缓存上直接读取
		// bizSystemlist=ClsCacheManager.getAllBizSystem();
		if (thirdPayConfiglist != null && thirdPayConfiglist.size() > 0) {
			return thirdPayConfiglist;
		}

		return thirdPayConfiglist;
	}

	private String id;

	private String name;
	/**
	 * 属性参数：值，如：options="id:hhhh,name:zhuang,width:90px,height:88"
	 */
	private String options;

	/**
	 * style属性
	 */
	private String style;

	/**
	 * 是否显示空选项
	 */
	private Boolean emptyOption = true;

	/**
	 * 是否显示空选项
	 */
	private Boolean hasSuperSystemOption = false;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOptions() {
		return options;
	}

	public void setOptions(String options) {
		this.options = options;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public Boolean getEmptyOption() {
		return emptyOption;
	}

	public void setEmptyOption(Boolean emptyOption) {
		this.emptyOption = emptyOption;
	}

	public Boolean getHasSuperSystemOption() {
		return hasSuperSystemOption;
	}

	public void setHasSuperSystemOption(Boolean hasSuperSystemOption) {
		this.hasSuperSystemOption = hasSuperSystemOption;
	}

	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException {

	}

}
