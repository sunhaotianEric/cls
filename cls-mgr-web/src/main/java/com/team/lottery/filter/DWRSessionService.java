package com.team.lottery.filter;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import org.directwebremoting.ScriptBuffer;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.directwebremoting.extend.Calls;
import org.directwebremoting.extend.Replies;
import org.directwebremoting.impl.DefaultRemoter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.vo.Admin;

public class DWRSessionService extends DefaultRemoter {
	
	private static Logger logger = LoggerFactory.getLogger(DWRSessionService.class);

	public Replies execute(Calls calls) {
		HttpServletRequest request = BaseDwrUtil.getRequest();
		Enumeration paramNames = request.getParameterNames();
		while (paramNames.hasMoreElements()) {
			String paramName = (String) paramNames.nextElement();
			String[] paramValues = request.getParameterValues(paramName);
			if (paramValues.length == 1) {
				String paramValue = paramValues[0];
				if (paramValue.length() != 0) {
					logger.debug("参数：" + paramName + "=" + paramValue);
				}
			}
		}

		String pathInfo = request.getPathInfo();
		if (!(pathInfo.startsWith("/call/plaincall/__System.generateId.dwr"))
				&& !(pathInfo.startsWith("/call/plaincall/__System.pageLoaded.dwr"))) {
			if (pathInfo.equals("/call/plaincall/managerAdminAction.login.dwr")
					|| pathInfo.equals("/call/plaincall/managerAdminAction.sendSecretCode.dwr")) {
				return super.execute(calls);
			} else if (pathInfo.contains("manager")) {
				Admin admin = BaseDwrUtil.getCurrentAdmin();
			//	if (admin == null || !SingleLoginForRedis.isOnline(SystemPropeties.loginType + "." + admin.getBizSystem(), admin.getUsername())) { // 判断管理员账号是否在线
				if (admin == null) {
				   logOutForManager();
					return super.execute(new Calls());
				}
			} else {
				return super.execute(calls);
			}
		}
		return super.execute(calls);
	}

	private void logOutForManager() {
		logger.debug("dwr session过期");
		WebContext wct = WebContextFactory.get();
		HttpServletRequest request = BaseDwrUtil.getRequest();
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath()
				+ "/";
		wct.getScriptSession().addScript(new ScriptBuffer("window.parent.location.href='" + basePath + "/managerzaizst/login.html'"));
	}
}
