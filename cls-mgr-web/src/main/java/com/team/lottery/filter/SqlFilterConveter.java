package com.team.lottery.filter;

import org.directwebremoting.ConversionException;
import org.directwebremoting.convert.StringConverter;
import org.directwebremoting.extend.ConverterManager;
import org.directwebremoting.extend.InboundVariable;
import org.directwebremoting.extend.OutboundContext;
import org.directwebremoting.extend.OutboundVariable;

import com.team.lottery.util.XSSFilterUtil;

public class SqlFilterConveter extends StringConverter {

	@Override
	public void setConverterManager(ConverterManager converterManager) {
		super.setConverterManager(converterManager);
	}
	
	@Override
	public Object convertInbound(Class<?> paramType, InboundVariable data)
			throws ConversionException {  
		//字符串sql注入攻击特殊字符拦截
		if(super.convertInbound(paramType, data) != null){
			//return SqlFilteUtil.getSqlFilterStr(super.convertInbound(paramType, data).toString());
			 return XSSFilterUtil.getSqlFilterStr(super.convertInbound(paramType, data).toString());
			
		}else{
			return super.convertInbound(paramType, data);
		}
	}
	
	@Override
	public OutboundVariable convertOutbound(Object data, OutboundContext outctx)
			throws ConversionException {
		return super.convertOutbound(data, outctx);
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}
	
	
	
	
}
