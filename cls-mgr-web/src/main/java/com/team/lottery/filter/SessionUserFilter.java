package com.team.lottery.filter;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.IPUtil;


public class SessionUserFilter implements Filter {

	private static Logger log = LoggerFactory.getLogger(SessionUserFilter.class);

	//不需要拦截的url放在set当中用set数据防止重复
	private static Set<String> allowUrl=new HashSet<String>();

	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
						 FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		HttpSession session = req.getSession();
		String serverName = req.getServerName();
		String url = req.getServletPath();
		//String basePath1 = req.getScheme()+"://"+req.getServerName()+":"+req.getServerPort() + req.getContextPath()+"/";
		String basePath = req.getContextPath();
		//int reqPort = req.getServerPort();
		String userIp = IPUtil.getIpAddr(req);

		//log.info("url:" + url);
		//log.info("basePath1:" + basePath1);
		//log.info("basePath:" + basePath);
		//log.info("reqPort:" + reqPort);


		String loginPageUrl = basePath+"/managerzaizst/login.html";

		//如果直接访问jsp文件，则直接拦截到后台的登陆页面 
		if(url.contains(".jsp")){
			res.sendRedirect(loginPageUrl);
			return;
		}


//	    if(url.contains("managerzaizst") && !url.contains("login") && !url.contains("error")&&!url.contains("illegalAddress.html")){  // 
//	       	if(session.getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_ADMIN) == null){
//	 	    	Boolean flag = ClsCacheManager.isAllowIPByDomainIp(serverName, userIp);
//	 	 	    if(!flag){
//	 	 	    	res.sendRedirect( basePath+"/illegalAddress.html");
//	 	 	    	return;
//	 	 	    }
//		    	 res.sendRedirect(loginPageUrl);
//	    	}
//	    }else{
//	    	if(!url.contains("index.html") && !url.contains("login") && !url.contains("error")&&!url.contains("illegalAddress.html") ){
//		    	if(session.getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER) == null){
//		    		 if(!url.contains("illegalAddress.html")){
//		    		    	Boolean flag = ClsCacheManager.isAllowIPByDomainIp(serverName, userIp);
//		    		 	    if(!flag){
//		    		 	    	res.sendRedirect( basePath+"/illegalAddress.html");
//		    		 	    	return;
//		    		 	    }
//		    		    }
//			    	res.sendRedirect(loginPageUrl);
//		    	}else {
//		    		if(!url.contains("returnurl.jsp")){
//				    	res.sendRedirect(loginPageUrl);
//		    		}
//		    	}
//	    	}
//	    }
//	    if(!url.contains("illegalAddress.html")){
//	    	Boolean flag = ClsCacheManager.isAllowIPByDomainIp(serverName, userIp);
//	 	    if(!flag){
//	 	    	res.sendRedirect( basePath+"/illegalAddress.html");
//	 	    	return;
//	 	    }
//	    }


		/*if(!isInAllowUrl(url)) {
			//判断是否在限制域名IP
			Boolean flag = ClsCacheManager.isAllowIPByDomainIp(serverName, userIp);
				if(!flag&&!userIp.equals("0:0:0:0:0:0:0:1")){
				log.info("当前访问ip[{}],访问的路径[{}]，不满足授权条件，非法访问", userIp, url);
				res.sendRedirect( basePath+"/illegal.html");
				return;
			}

			//判断后台用户是否登录
			if(session.getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_ADMIN) == null){
				res.sendRedirect(loginPageUrl);
				return;
			}
		}else{
			//跳出循环条件，登录页跟非法访问提示页使用
			if(!url.contains("illegal.html")){
				Boolean flag = ClsCacheManager.isAllowIPByDomainIp(serverName, userIp);
				if (serverName.equals("localhost")) flag=true;
				if(!flag){
					log.info("当前访问ip[{}],访问的路径[{}]，不满足授权条件，非法访问", userIp, url);
					res.sendRedirect( basePath+"/illegal.html");
					return;
				}
			}
		}*/

		chain.doFilter(request, response);
	}

	/**
	 * url判断方法：foreach遍历set元素与传入的url进行判断如果有包含的话返回true;
	 * 如果遍历完了没有包含的话返回false!
	 * @param
	 * @return
	 */
	public boolean isInAllowUrl(String url) {
		for(String s:allowUrl) {
			if(url.contains(s)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 在初始化方法中,向set当中添加不需要拦截的url
	 *
	 *
	 */
	@Override
	public void init(FilterConfig arg0) throws ServletException {
		allowUrl.add("illegal.html");
		allowUrl.add("login.html");
		allowUrl.add("/druid");
	}

}
