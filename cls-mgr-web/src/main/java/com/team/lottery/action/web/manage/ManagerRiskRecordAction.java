package com.team.lottery.action.web.manage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.Page;
import com.team.lottery.service.RiksRecordService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.RiskRecord;

@Controller("managerRiskRecordAction")
public class ManagerRiskRecordAction {
	@Autowired
	private RiksRecordService riksRecordService;
	
    /**
     * 查询
     * @return
     */
    public Object[] getAllRiskRecordByQueryPage(RiskRecord query,Page page){
    	if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	if(!(ConstantUtil.SUPER_SYSTEM).equals(currentAdmin.getBizSystem()))
    	{
    		query.setBizSystem(currentAdmin.getBizSystem());
    	}
    	page=riksRecordService.getAllRiskRecordByQueryPage(query, page);
		return AjaxUtil.createReturnValueSuccess(page);
    }
}
