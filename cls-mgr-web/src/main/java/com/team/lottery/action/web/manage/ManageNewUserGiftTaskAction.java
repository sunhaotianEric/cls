package com.team.lottery.action.web.manage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.NewUserGiftTaskQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.NewUserGiftTaskService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.vo.Admin;

/**
 * 后台管理新手礼包设置界面
 * @author QaQ
 *
 */
@Controller("manageNewUserGiftTaskAction")
public class ManageNewUserGiftTaskAction {
	//private static Logger log = LoggerFactory.getLogger(ManageNewUserGiftTaskAction.class);
	@Autowired
	private NewUserGiftTaskService newUserGiftTaskService;
	
	/**
	 * 根据查询对象返回查询NewUserGiftTask返回的集合
	 * @param query 查询对象
	 * @param page	返回的对象
	 * @return
	 */
	public Object[] getAllNewUserGiftTask(NewUserGiftTaskQuery query,Page page){
		if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	if(!ConstantUtil.SUPER_SYSTEM.equals(currentAdmin.getBizSystem())){
    		query.setBizSystem(currentAdmin.getBizSystem());
    	}
    	//查询所有的NewUserGiftTask对象并且分页
    	page = newUserGiftTaskService.getAllNewUserGiftTaskService(query, page);
    	return  AjaxUtil.createReturnValueSuccess(page);
	}
	
}
