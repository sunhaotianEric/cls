package com.team.lottery.action.web.common;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.extvo.ImageUploadResult;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.HttpClientUtil;
import com.team.lottery.util.JSONUtils;

public class UploadFileForJsonServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger logger = LoggerFactory.getLogger(UploadFileForJsonServlet.class);
	private static final String imgurl = SystemConfigConstant.imgServerUploadUrl + ConstantUtil.FILE_UPLOAD_RESOURCE;

	// private static final String
	// imgurl="http://localhost:8080/ttt/upload_json.jsp";
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	@SuppressWarnings("rawtypes")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter out = response.getWriter();
		// 文件保存目录路径
		// tomcat 6 不支持该方法
		// String savePath = request.getServletContext().getRealPath("/") +
		// "upload/";
		String savePath = request.getSession().getServletContext().getRealPath("/") + "upload/";
		logger.info("图片上传服务器路径路径savePath:{}", savePath);
		// 文件保存目录URL
		String dirfile = request.getParameter("dirfile");
		logger.info("图片上传服务器路径路径dirfile:{}", dirfile);

		// 定义允许上传的文件扩展名
		HashMap<String, String> extMap = new HashMap<String, String>();
		extMap.put("image", "gif,jpg,jpeg,png,bmp");
		extMap.put("flash", "swf,flv");
		extMap.put("media", "swf,flv,mp3,wav,wma,wmv,mid,avi,mpg,asf,rm,rmvb");
		extMap.put("file", "doc,docx,xls,xlsx,ppt,htm,html,txt,zip,rar,gz,bz2");

		// 最大文件大小
		long maxSize = 1000000;

		response.setContentType("text/html; charset=UTF-8");

		if (!ServletFileUpload.isMultipartContent(request)) {
			out.println(getError("请选择文件。"));
			return;
		}
		// 检查目录,没有就创建,有就不创建.
		File uploadDir = new File(savePath);
		if (!uploadDir.exists()) {
			boolean mkdir = uploadDir.mkdir();
			if (mkdir) {
				logger.info("图片上传服务器上传目录创建成功.");
			} else {
				logger.error("图片上传服务器上传目录创建失败.");
				out.println(getError("上传图片目录创建异常,请联系管理员!"));
				return;
			}
		}
		// 检查目录写权限
		if (!uploadDir.canWrite()) {
			out.println(getError("上传目录没有写权限。"));
			logger.error("图片上传服务器上传目录没有写权限.");
			return;
		}

		String dirName = request.getParameter("dir");
		if (dirName == null) {
			dirName = "image";
			logger.error("图片上传服务器上传dirName为null");
		}
		if (!extMap.containsKey(dirName)) {
			out.println(getError("目录名不正确。"));
			logger.error("图片上传服务器上传目录名不正确。");
			return;
		}
		// 创建文件夹
		savePath += dirName + "/";
		File saveDirFile = new File(savePath);
		if (!saveDirFile.exists()) {
			saveDirFile.mkdirs();
		}
		// 给上传的文件命名,用时间去命名
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String ymd = sdf.format(new Date());
		savePath += ymd + "/";
		File dirFile = new File(savePath);
		if (!dirFile.exists()) {
			dirFile.mkdirs();
		}

		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		upload.setHeaderEncoding("UTF-8");
		List items = null;
		try {
			items = upload.parseRequest(request);
		} catch (FileUploadException e1) {
			e1.printStackTrace();
		}
		Iterator itr = items.iterator();
		while (itr.hasNext()) {
			FileItem item = (FileItem) itr.next();
			String fileName = item.getName();
			if (!item.isFormField()) {
				// 检查文件大小
				if (item.getSize() > maxSize) {
					out.println(getError("上传文件大小超过限制。"));
					return;
				}
				// 检查扩展名
				String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
				if (!Arrays.<String> asList(extMap.get(dirName).split(",")).contains(fileExt)) {
					out.println(getError("上传文件扩展名是不允许的扩展名。\n只允许" + extMap.get(dirName) + "格式。"));
					logger.error("图片上传服务器上传文件扩展名是不允许的扩展名。");
					return;
				}

				SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
				String newFileName = df.format(new Date()) + "_" + new Random().nextInt(1000) + "." + fileExt;
				ImageUploadResult result = null;
				try {
					File uploadedFile = new File(savePath, newFileName);
					item.write(uploadedFile);
					String picSavePath = dirfile;
					String fullSavePath = "/" + picSavePath + "/" + newFileName;
					Map<String, String> params = new HashMap<String, String>();
					params.put("path", picSavePath);
					params.put("fileName", newFileName);
					String uploadResult = HttpClientUtil.doPostFileUpload(imgurl, uploadedFile, params);
					logger.info("上传图片结果：{}", uploadResult);
					result = JSONUtils.toBean(uploadResult, ImageUploadResult.class);
					if (result == null) {
						out.println(getError("文件上传图片服务器错误,原因:返回结果为空!"));
						logger.error("文件上传图片服务器错误,原因:返回结果为空!");
					}
					if ("error".equals(result.getResult())) {
						String mString = result.getMsg();
						out.println(getError("文件上传图片服务器错误,原因:" + mString));
						logger.error("文件上传图片服务器错误,原因:" + mString);
					}
					// 将上传好的图片路径输出到富文本编辑器中用于回显.
					JSONObject objs = new JSONObject();
					objs.put("error", 0);
					objs.put("url", SystemConfigConstant.imgServerUrl + fullSavePath);
					out.println(objs.toString());
					logger.info("上传网站内容图片到图片服务器成功，删除本地图片文件");
					uploadedFile.delete();
				} catch (Exception e) {
					e.printStackTrace();
					out.println(getError("上传文件失败。"));
					logger.error("文件上传图片服务上传出现异常!");
					return;
				}

			}
		}
	}

	private String getError(String message) {
		JSONObject obj = new JSONObject();
		obj.put("error", 1);
		obj.put("message", message);
		return obj.toString();
	}

}
