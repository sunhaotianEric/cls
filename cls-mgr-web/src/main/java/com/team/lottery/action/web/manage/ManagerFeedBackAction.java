package com.team.lottery.action.web.manage;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.Page;
import com.team.lottery.service.FeedBackService;
import com.team.lottery.service.NotesService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.FeedBack;
import com.team.lottery.vo.Notes;

@Controller("managerFeedBackAction")
public class ManagerFeedBackAction {
	
	@Autowired
	private FeedBackService feedbackService;
	@Autowired
	private NotesService notesService;

	/**
	 * 分页查询
	 * @param query
	 * @param page
	 * @return
	 */
	public Object[] getAllFeedBack(FeedBack query,Page page){
		if(query==null||page==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		Admin currentUser = BaseDwrUtil.getCurrentAdmin();
      	String bizSystem = currentUser.getBizSystem();
      	if(!"SUPER_SYSTEM".equals(bizSystem)){
			query.setBizSystem(bizSystem);
		}
		page=feedbackService.getAllFeedBack(query, page);
		return AjaxUtil.createReturnValueSuccess(page);
	}
	
	/**
	 * 根据ID查询
	 * @param id
	 * @return
	 */
	public Object[] selectFeedBack(Integer id){
		FeedBack record=feedbackService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(record);
	}
	
	/**
	 * 修改
	 * @param record
	 * @return
	 */
/*	public Object[] updateFeedBack(FeedBack record){
		if(record==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		feedbackService.updateByPrimaryKeySelective(record);
		return AjaxUtil.createReturnValueSuccess();
	}*/
	
	/**
	 * 根据ID删除
	 * @param id
	 * @return
	 */
	public Object[] delFeedBackById(Integer id){
		if(id==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		feedbackService.deleteByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess();
	}
	
	/**
	 * 保存
	 * @param record
	 * @return
	 */
/*	public Object[] saveFeedBack(FeedBack record){
    	if(record == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	Integer id = record.getId();
    	if(id == null){
    		record.setCreateTime(new Date());
    		feedbackService.insertSelective(record);
    	}else{
    		record.setUpeateTime(new Date());
    		feedbackService.updateByPrimaryKeySelective(record);
    	}
    	return AjaxUtil.createReturnValueSuccess();
    }*/
	
	/**
	 * 处理反馈信息并给用户发送私信
	 * @param id
	 * @param content
	 * @param textarea
	 * @return
	 */
	public Object[] enableFeedBack(Integer id, String content, String textarea) {
		if (id == null) {
			return AjaxUtil.createReturnValueError("参数传递错误");
		}
		FeedBack feedback = feedbackService.selectByPrimaryKey(id);
		Integer dealstate = feedback.getDealstate();
		if (dealstate != 0) {
			return AjaxUtil.createReturnValueError("该条数据已处理!");
		}
		FeedBack record = new FeedBack();
		record.setId(id);
		record.setDealstate(1);
		record.setUpdateAdmin(BaseDwrUtil.getCurrentAdmin().getUsername());
		int result = feedbackService.updateByPrimaryKeySelective(record);
		if (result > 0) {
			String bizsystem = feedback.getBizSystem();
			Integer userid = feedback.getUserId();
			String username = feedback.getUserName();
			Notes notes = new Notes();
			notes.setBizSystem(bizsystem);
			notes.setFromUserName("SYSTEM");
			notes.setToUserId(userid);
			notes.setToUserName(username);
			notes.setType("SYSTEM");
			notes.setEnabled(1);
			notes.setSub("反馈信息");
			notes.setStatus("NO_READ");
			Integer parentid = 0;
			notes.setParentId(parentid.longValue());
			if (!StringUtils.isEmpty(content) || !StringUtils.isEmpty(textarea)) {
				notes.setBody(content + textarea);
			}else {
				notes.setBody("发送的反馈信息已处理，感谢您的反馈！");
			}
			notes.setCreatedDate(new Date());
			//给用户发送私信
			notesService.insertSelective(notes);
			return AjaxUtil.createReturnValueSuccess();
		} else {
			return AjaxUtil.createReturnValueError("添加失败，请重新添加");
		}
	}
}
