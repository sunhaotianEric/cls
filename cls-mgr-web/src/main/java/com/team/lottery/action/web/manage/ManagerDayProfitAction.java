package com.team.lottery.action.web.manage;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.DayProfitQuery;
import com.team.lottery.extvo.GainComparable;
import com.team.lottery.extvo.LotteryGainComparable;
import com.team.lottery.extvo.MoneyGainComparable;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.WinGainComparable;
import com.team.lottery.service.BizSystemService;
import com.team.lottery.service.DayProfitService;
import com.team.lottery.system.job.DayProfileJob;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ClsDateHelper;
import com.team.lottery.util.DateUtil;
import com.team.lottery.vo.BizSystem;
import com.team.lottery.vo.DayProfit;



@Controller("managerDayProfitAction")
public class ManagerDayProfitAction {
	@Autowired
	public DayProfitService dayProfitService;
	
	@Autowired
	public BizSystemService bizSystemService;
	/**
	 * 根据创建时间查询平台盈利数据
	 * @param query
	 * @return
	 * @throws ParseException 
	 */
	public Object[] getAllDayProfit(DayProfitQuery query){
		if (query.getBelongDateStart() == null || query.getBelongDateEnd() == null) {
			return AjaxUtil.createReturnValueError("请选择正确的日期!");
		}
		//计算 普通会员 代理余额  默认就取代理报表的enddate
		BigDecimal agencyMoney = BigDecimal.ZERO;
		BigDecimal regularmembersMoney = BigDecimal.ZERO;
		DayProfit dayProfit = new DayProfit();
		Calendar currentDate = new GregorianCalendar();
		Calendar calendarNow = Calendar.getInstance();
		Date nowDate = new Date();
		calendarNow.setTime(nowDate);
		Date nowFiveHoursDate = ClsDateHelper.getClsReportLastTime(nowDate);
		Date startDateTime = DateUtil.getNowStartTimeByStart(query.getBelongDateStart());
		Date endDateTime = DateUtil.getNowStartTimeByEnd(query.getBelongDateEnd());
		query.setBelongDateStart(startDateTime);
		query.setBelongDateEnd(endDateTime);
		String bizSystem = BaseDwrUtil.getCurrentAdmin().getBizSystem();
		if (!bizSystem.equals("SUPER_SYSTEM")) {
			query.setBizSystem(bizSystem);
		}
		try {
			DayProfileJob dayProfileJob = ApplicationContextUtil.getBean("dayProfileJob");
//			dayProfileJob.setBizSystem(query.getBizSystem());
//			dayProfileJob.setIsInsertTable(false);
			Long day = (query.getBelongDateEnd().getTime() - query.getBelongDateStart().getTime()) / (24 * 60 * 60 * 1000);
			// 当前时间小于五点，就算前一天数据统计出来了，也是查询实时相当于昨天3点到现在时间，1天多的实时数据
			if (nowDate.before(nowFiveHoursDate)) {
				int nowDateInt = calendarNow.get(Calendar.DAY_OF_MONTH);
				calendarNow.setTime(endDateTime);
				int endDateInt = calendarNow.get(Calendar.DAY_OF_MONTH);
				// 当前时间是要今天或者昨天才有如下判断
				if (day < 2 && (nowDateInt == endDateInt|| (nowDateInt - 1) == endDateInt)) {
					if (day == 0) {// 只查询今天数据
						dayProfileJob.setNowTime(endDateTime);
						 dayProfit = dayProfileJob.getProfitByBelongDate(dayProfileJob.getNowTime(),query.getBizSystem(),false);
						 //重新计算盈率
						 DayProfit.executeDayProfitForWinGain(dayProfit);
						return AjaxUtil.createReturnValueSuccess(dayProfit);
					} else {
						// 计算今天实时数据
						dayProfileJob.setNowTime(endDateTime);
						 dayProfit = dayProfileJob.getProfitByBelongDate(dayProfileJob.getNowTime(),query.getBizSystem(),false);
						 //默认取今天代理余额
						 agencyMoney = dayProfit.getAgencyMoney();
						 regularmembersMoney = dayProfit.getRegularmembersMoney();
						 if((nowDate.getDay() - 1) == endDateTime.getDay()){//如果是结束昨天的话，开始是前天
							currentDate.setTime(endDateTime);
							currentDate.add(Calendar.DAY_OF_MONTH, (int) (-1));
							query.setBelongDateEnd(currentDate.getTime()); // 查询已生成数据的最后时间
							// 验证数据是否丢失
							String resultStr = dayProfitService.checkDataComplete(query);
							if (resultStr.length() > 0) {
								throw new Exception(resultStr.substring(1) + "数据不完整！");
							}
							List<DayProfit> dayProfitList = dayProfitService.getAllDayProfitByBelongDate(query);
							if (dayProfitList.size() > 0) {
								dayProfit.addDayProfit(dayProfitList.get(0));
							}
						}else{
							// 计算昨天实时数据
							currentDate.setTime(endDateTime);
							currentDate.add(Calendar.DAY_OF_MONTH, -1);
							dayProfileJob.setNowTime(currentDate.getTime());
							DayProfit yesterdayProfit = dayProfileJob.getProfitByBelongDate(dayProfileJob.getNowTime(),query.getBizSystem(),false);
							// 两个实时数据相加，得到实时数据
							dayProfit.addDayProfit(yesterdayProfit);
						}
						//重新计算盈率
						DayProfit.executeDayProfitForWinGain(dayProfit);
						//重新设置代理余额
						dayProfit.setAgencyMoney(agencyMoney);
						dayProfit.setRegularmembersMoney(regularmembersMoney);
						return AjaxUtil.createReturnValueSuccess(dayProfit);
					}

				} else if ((nowDate.getDay() == endDateTime.getDay() || (nowDate.getDay() - 1) == endDateTime.getDay())) {// 超过两天，已部分查实时的，一部分查询出已生成数据
					currentDate.setTime(endDateTime);
					if (nowDate.getDay() == endDateTime.getDay()) {
						currentDate.add(Calendar.DAY_OF_MONTH, (int) (-2));
					} else {
						currentDate.add(Calendar.DAY_OF_MONTH, (int) (-1));
					}

					query.setBelongDateEnd(currentDate.getTime()); // 查询已生成数据的最后时间
					// 验证数据是否丢失
					String resultStr = dayProfitService.checkDataComplete(query);
					if (resultStr.length() > 0) {
						throw new Exception(resultStr.substring(1) + "数据不完整！");
					}
					List<DayProfit> dayProfitList = dayProfitService.getAllDayProfitByBelongDate(query);

					/**
					 * 查询现在数据的时间
					 */
					if (nowDate.getDay() == endDateTime.getDay()) {
						// 计算今天实时数据
						dayProfileJob.setNowTime(endDateTime);
						 dayProfit = dayProfileJob.getProfitByBelongDate(dayProfileJob.getNowTime(),query.getBizSystem(),false);
						 //默认取今天代理余额
						 agencyMoney = dayProfit.getAgencyMoney();
						 regularmembersMoney = dayProfit.getRegularmembersMoney();
						 // 计算昨天实时数据
						currentDate.setTime(endDateTime);
						currentDate.add(Calendar.DAY_OF_MONTH, -1);
						dayProfileJob.setNowTime(currentDate.getTime());
						DayProfit yesterdayProfit = dayProfileJob.getProfitByBelongDate(dayProfileJob.getNowTime(),query.getBizSystem(),false);
						// 两个实时数据相加，得到实时数据
						dayProfit.addDayProfit(yesterdayProfit);
						if (dayProfitList.size() > 0) {
							dayProfit.addDayProfit(dayProfitList.get(0));
						}
						//重新计算盈率
						DayProfit.executeDayProfitForWinGain(dayProfit);
						//重新设置代理余额
						dayProfit.setAgencyMoney(agencyMoney);
						dayProfit.setRegularmembersMoney(regularmembersMoney);
						return AjaxUtil.createReturnValueSuccess(dayProfit);
					} else {
						// 计算实时数据
						dayProfileJob.setNowTime(endDateTime);
						 dayProfit = dayProfileJob.getProfitByBelongDate(dayProfileJob.getNowTime(),query.getBizSystem(),false);
						 //默认取今天代理余额
						 agencyMoney = dayProfit.getAgencyMoney();
						 regularmembersMoney = dayProfit.getRegularmembersMoney();
						 if (dayProfitList.size() > 0) {
							dayProfit.addDayProfit(dayProfitList.get(0));
						}
						//重新计算盈率
						DayProfit.executeDayProfitForWinGain(dayProfit);
						//重新设置代理余额
						dayProfit.setAgencyMoney(agencyMoney);
						dayProfit.setRegularmembersMoney(regularmembersMoney);
						return AjaxUtil.createReturnValueSuccess(dayProfit);
					}

				} else {
					// 验证数据是否丢失
					String resultStr = dayProfitService.checkDataComplete(query);
					if (resultStr.length() > 0) {
						throw new Exception(resultStr.substring(1) + "数据不完整！");
					}
					List<DayProfit> dayProfitList = dayProfitService.getAllDayProfitByBelongDate(query);

					if (dayProfitList.size() > 0) {
						//计算代理余额
						startDateTime = DateUtil.getNowStartTimeByStart(query.getBelongDateEnd());
						query.setBelongDateStart(startDateTime);
						List<DayProfit> agentDayProfitList = dayProfitService.getAllDayProfitByBelongDate(query);
						if(agentDayProfitList.size()>0){
							 agencyMoney = agentDayProfitList.get(0).getAgencyMoney(); //默认是最后一天的代理余额
							 regularmembersMoney = agentDayProfitList.get(0).getRegularmembersMoney();
						}
						dayProfit = dayProfitList.get(0);
						//重新计算盈率
						DayProfit.executeDayProfitForWinGain(dayProfit);
						
						dayProfit.setAgencyMoney(agencyMoney);
						dayProfit.setRegularmembersMoney(regularmembersMoney);
						return AjaxUtil.createReturnValueSuccess(dayProfit);
					} else {
						return AjaxUtil.createReturnValueError("数据为空！");
					}
				}
			//查询时间在5:00之后  查询的数据为当日的实时数据加上查询前一天的数据
			} else {
				if (nowDate.getDay() == endDateTime.getDay()) {
					
					if (day == 0) {// 只查询今天数据
						// 查询现在数据的时间
						dayProfileJob.setNowTime(endDateTime);
						dayProfit = dayProfileJob.getProfitByBelongDate(dayProfileJob.getNowTime(),query.getBizSystem(),false);
						//重新计算盈率
						DayProfit.executeDayProfitForWinGain(dayProfit);
						return AjaxUtil.createReturnValueSuccess(dayProfit);
					}else{
						currentDate.setTime(endDateTime);
						currentDate.add(Calendar.DAY_OF_MONTH, -1);
						query.setBelongDateEnd(currentDate.getTime()); // 查询已生成数据的最后时间
						// 验证数据是否丢失
						String resultStr = dayProfitService.checkDataComplete(query);
						if (resultStr.length() > 0) {
							throw new Exception(resultStr.substring(1) + "数据不完整！");
						}
						List<DayProfit> dayProfitList = dayProfitService.getAllDayProfitByBelongDate(query);

						// 查询现在数据的时间
						dayProfileJob.setNowTime(endDateTime);
					    dayProfit = dayProfileJob.getProfitByBelongDate(dayProfileJob.getNowTime(),query.getBizSystem(),false);
					     //默认取今天代理余额
						 agencyMoney = dayProfit.getAgencyMoney();
						 regularmembersMoney = dayProfit.getRegularmembersMoney();
					    if (dayProfitList.size() > 0) {
							dayProfit.addDayProfit(dayProfitList.get(0));
						}
						//重新计算盈率
						 DayProfit.executeDayProfitForWinGain(dayProfit);
						//重新设置代理余额
						dayProfit.setAgencyMoney(agencyMoney);
						dayProfit.setRegularmembersMoney(regularmembersMoney);
						return AjaxUtil.createReturnValueSuccess(dayProfit);
					}
					
					
				} else {
					// 验证数据是否丢失
					String resultStr = dayProfitService.checkDataComplete(query);
					if (resultStr.length() > 0) {
						throw new Exception(resultStr.substring(1) + "数据不完整！");
					}
					List<DayProfit> dayProfitList = dayProfitService.getAllDayProfitByBelongDate(query);
					if (dayProfitList.size() > 0) {
						dayProfit = dayProfitList.get(0);
						//计算代理余额
						startDateTime = DateUtil.getNowStartTimeByStart(query.getBelongDateEnd());
						query.setBelongDateStart(startDateTime);
						List<DayProfit> agentDayProfitList = dayProfitService.getAllDayProfitByBelongDate(query);
						if(agentDayProfitList.size()>0){
							 agencyMoney = agentDayProfitList.get(0).getAgencyMoney(); //默认是最后一天的代理余额
							 regularmembersMoney = agentDayProfitList.get(0).getRegularmembersMoney();
						}
						//重新计算盈率
						DayProfit.executeDayProfitForWinGain(dayProfit);
						//重新设置代理余额
						dayProfit.setAgencyMoney(agencyMoney);
						dayProfit.setRegularmembersMoney(regularmembersMoney);
						return AjaxUtil.createReturnValueSuccess(dayProfit);
					} else {
						return AjaxUtil.createReturnValueError("数据为空！");
					}
				}

			}
		} catch (Exception e) {
			return AjaxUtil.createReturnValueError(e.getMessage());
		}

		// if(dayProfitList.size() == 0){
		// return AjaxUtil.createReturnValueError("未查到盈亏数据，请检查询日期!");
		// }
		
	}
	

	
	
	/**
     * 分页查询平台赢利总览
	 * @param query
	 * @param page
	 * @return
	 */
	public Object[] getDayProfiyPandect(DayProfitQuery query, Page page){
		if(query == null || page == null){
			return AjaxUtil.createReturnValueError("参数错误!");
		}
		
		if(query.getBelongDateStart() == null || query.getBelongDateEnd() == null){
			return AjaxUtil.createReturnValueError("请选择正确的日期!");
		}
		
		Date startDateTime = DateUtil.getNowStartTimeByStart(query.getBelongDateStart());
		Date endDateTime = DateUtil.getNowStartTimeByEnd(query.getBelongDateEnd());
		query.setBelongDateStart(startDateTime);
		query.setBelongDateEnd(endDateTime);
		
			
		if(query.getGainModel() == null){
			query.setGainModel(1);
		}
		Page profiyPandect = null;
		//Page profiyPandect = dayProfitService.getDayProfiyPandect(query, page);
		try{
		 profiyPandect = dayProfitDataHandle(query,page);
		if(page.getPageContent() == null){
			return AjaxUtil.createReturnValueError("未查到盈亏数据，请检查询日期!");
		}
		}catch (Exception e){
			return AjaxUtil.createReturnValueError(e.getMessage());
		}
		return AjaxUtil.createReturnValueSuccess(profiyPandect);
	}

	
	/**
	 * 盈亏总览 实时与历史合并处理
	 * @param query
	 * @param list
	 * @return
	 * @throws Exception 
	 */
	private Page  dayProfitDataHandle(DayProfitQuery query,Page page) throws Exception{
		List<DayProfit> list = new ArrayList<DayProfit>();
		Calendar currentDate = new GregorianCalendar();
		Date nowDate = new Date();
		Date nowFiveHoursDate = ClsDateHelper.getClsReportLastTime(nowDate);
		Date startDateTime = query.getBelongDateStart();
		Date endDateTime = query.getBelongDateEnd();
		try {
			
			DayProfileJob dayProfileJob = ApplicationContextUtil.getBean("dayProfileJob");
			List<BizSystem> bizList = new ArrayList<BizSystem>();
			//有传bizSystem过来，查单个系统，否则查多个系统
			if(query.getBizSystem() != null && !"".equals(query.getBizSystem())){
				BizSystem bizSystem  = new BizSystem();
				bizSystem.setBizSystem(query.getBizSystem());
				bizList.add(bizSystem);
				
			}else{
				bizList = bizSystemService.getAllBizSystem();
			}
			
			Long day = (query.getBelongDateEnd().getTime() - query.getBelongDateStart().getTime()) / (24 * 60 * 60 * 1000);
			for(BizSystem biz:bizList){
				query.setBizSystem(biz.getBizSystem());
				DayProfit dayProfit = new DayProfit();
				
			// 当前时间小于五点，就算前一天数据统计出来了，也是查询实时相当于昨天3点到现在时间，1天多的实时数据
			if (nowDate.before(nowFiveHoursDate)) {
				// 当前时间是要今天或者昨天才有如下判断
				if (day < 2 && (nowDate.getDay() == endDateTime.getDay()|| (nowDate.getDay() - 1) == endDateTime.getDay())) {
					if (day == 0) {// 只查询今天数据
						dayProfileJob.setNowTime(endDateTime);
						 dayProfit = dayProfileJob.getProfitByBelongDate(dayProfileJob.getNowTime(),query.getBizSystem(),false);
						 //重新计算盈率
						 DayProfit.executeDayProfitForWinGain(dayProfit);
					} else {
						// 计算今天实时数据
						dayProfileJob.setNowTime(endDateTime);
						 dayProfit = dayProfileJob.getProfitByBelongDate(dayProfileJob.getNowTime(),query.getBizSystem(),false);
						if((nowDate.getDay() - 1) == endDateTime.getDay()){//如果是结束昨天的话，开始是前天
							currentDate.setTime(endDateTime);
							currentDate.add(Calendar.DAY_OF_MONTH, (int) (-1));
							query.setBelongDateEnd(currentDate.getTime()); // 查询已生成数据的最后时间
							// 验证数据是否丢失
							String resultStr = dayProfitService.checkDataComplete(query);
							if (resultStr.length() > 0) {
								throw new Exception(resultStr.substring(1) + "数据不完整！");
							}
							List<DayProfit> dayProfitList = dayProfitService.getDayProfiyByQuery(query);
							if (dayProfitList.size() > 0) {
								dayProfit.addDayProfit(dayProfitList.get(0));
							}
						}else{
							// 计算昨天实时数据
							currentDate.setTime(endDateTime);
							currentDate.add(Calendar.DAY_OF_MONTH, -1);
							dayProfileJob.setNowTime(currentDate.getTime());
							DayProfit yesterdayProfit = dayProfileJob.getProfitByBelongDate(dayProfileJob.getNowTime(),query.getBizSystem(),false);
							// 两个实时数据相加，得到实时数据
							dayProfit.addDayProfit(yesterdayProfit);
						}
						//重新计算盈率
						DayProfit.executeDayProfitForWinGain(dayProfit);
					}

				} else if ((nowDate.getDay() == endDateTime.getDay() || (nowDate.getDay() - 1) == endDateTime.getDay())) {// 超过两天，已部分查实时的，一部分查询出已生成数据
					currentDate.setTime(endDateTime);
					if (nowDate.getDay() == endDateTime.getDay()) {
						currentDate.add(Calendar.DAY_OF_MONTH, (int) (-2));
					} else {
						currentDate.add(Calendar.DAY_OF_MONTH, (int) (-1));
					}

					query.setBelongDateEnd(currentDate.getTime()); // 查询已生成数据的最后时间
					// 验证数据是否丢失
					String resultStr = dayProfitService.checkDataComplete(query);
					if (resultStr.length() > 0) {
						throw new Exception(resultStr.substring(1) + "数据不完整！");
					}
					List<DayProfit> dayProfitList = dayProfitService.getDayProfiyByQuery(query);

					/**
					 * 查询现在数据的时间
					 */
					if (nowDate.getDay() == endDateTime.getDay()) {
						// 计算今天实时数据
						dayProfileJob.setNowTime(endDateTime);
						 dayProfit = dayProfileJob.getProfitByBelongDate(dayProfileJob.getNowTime(),query.getBizSystem(),false);
						// 计算昨天实时数据
						currentDate.setTime(endDateTime);
						currentDate.add(Calendar.DAY_OF_MONTH, -1);
						dayProfileJob.setNowTime(currentDate.getTime());
						DayProfit yesterdayProfit = dayProfileJob.getProfitByBelongDate(dayProfileJob.getNowTime(),query.getBizSystem(),false);
						// 两个实时数据相加，得到实时数据
						dayProfit.addDayProfit(yesterdayProfit);
						if (dayProfitList.size() > 0) {
							dayProfit.addDayProfit(dayProfitList.get(0));
						}
						//重新计算盈率
						DayProfit.executeDayProfitForWinGain(dayProfit);
					} else {
						// 计算实时数据
						dayProfileJob.setNowTime(endDateTime);
						 dayProfit = dayProfileJob.getProfitByBelongDate(dayProfileJob.getNowTime(),query.getBizSystem(),false);
						if (dayProfitList.size() > 0) {
							dayProfit.addDayProfit(dayProfitList.get(0));
						}
						//重新计算盈率
						DayProfit.executeDayProfitForWinGain(dayProfit);
					}

				} else {
					// 验证数据是否丢失
					String resultStr = dayProfitService.checkDataComplete(query);
					if (resultStr.length() > 0) {
						throw new Exception(resultStr.substring(1) + "数据不完整！");
					}
					List<DayProfit> dayProfitList = dayProfitService.getDayProfiyByQuery(query);

					if (dayProfitList.size() > 0) {
						dayProfit = dayProfitList.get(0);
						//重新计算盈率
						DayProfit.executeDayProfitForWinGain(dayProfit);
					} 
				}
			//查询时间在5:00之后  查询的数据为当日的实时数据加上查询前一天的数据
			} else {
				if (nowDate.getDay() == endDateTime.getDay()) {
					
					if (day == 0) {// 只查询今天数据
						// 查询现在数据的时间
						dayProfileJob.setNowTime(endDateTime);
						dayProfit = dayProfileJob.getProfitByBelongDate(dayProfileJob.getNowTime(),query.getBizSystem(),false);
						//重新计算盈率
						DayProfit.executeDayProfitForWinGain(dayProfit);
					
					}else{
						currentDate.setTime(endDateTime);
						currentDate.add(Calendar.DAY_OF_MONTH, -1);
						query.setBelongDateEnd(currentDate.getTime()); // 查询已生成数据的最后时间
						// 验证数据是否丢失
						String resultStr = dayProfitService.checkDataComplete(query);
						if (resultStr.length() > 0) {
							throw new Exception(resultStr.substring(1) + "数据不完整！");
						}
						List<DayProfit> dayProfitList = dayProfitService.getDayProfiyByQuery(query);

						// 查询现在数据的时间
						dayProfileJob.setNowTime(endDateTime);
					    dayProfit = dayProfileJob.getProfitByBelongDate(dayProfileJob.getNowTime(),query.getBizSystem(),false);
						if (dayProfitList.size() > 0) {
							dayProfit.addDayProfit(dayProfitList.get(0));
						}
						//重新计算盈率
						 DayProfit.executeDayProfitForWinGain(dayProfit);
					
					}
					
					
				} else {
					// 验证数据是否丢失
					String resultStr = dayProfitService.checkDataComplete(query);
					if (resultStr.length() > 0) {
						throw new Exception(resultStr.substring(1) + "数据不完整！");
					}
					List<DayProfit> dayProfitList = dayProfitService.getDayProfiyByQuery(query);
					if (dayProfitList.size() > 0) {
						dayProfit = dayProfitList.get(0);
						//重新计算盈率
						DayProfit.executeDayProfitForWinGain(dayProfit);
					
					}
				}

			}
			dayProfit.setBizSystem(biz.getBizSystem());
			list.add(dayProfit);
		}
		 //排序	
		  if(query.getGainModel() == 1){
				Comparator<DayProfit> comparator = new GainComparable();
				comparator=Collections.reverseOrder(comparator);//降序
				Collections.sort(list,comparator);  
		  }else if(query.getGainModel() == 2){
			    Comparator<DayProfit> comparator = new WinGainComparable();
				comparator=Collections.reverseOrder(comparator);//降序
				Collections.sort(list,comparator); 
		  }else if(query.getGainModel() == 3){
			    Comparator<DayProfit> comparator = new LotteryGainComparable();
				comparator=Collections.reverseOrder(comparator);//降序
				Collections.sort(list,comparator); 
		  }else{
			    Comparator<DayProfit> comparator = new MoneyGainComparable();
				comparator=Collections.reverseOrder(comparator);//降序
				Collections.sort(list,comparator); 
		  }
			
		 //分页	
		 page.setTotalRecNum(list.size());
		 Integer pageEndRow = 0;
		 Integer pageStartRow = 0;
        if (page.getPageSize() * page.getPageNo() < page.getTotalRecNum()) {// 判断是否为最后一页 
            pageEndRow = page.getPageSize() * page.getPageNo(); 
            pageStartRow = pageEndRow - page.getPageSize(); 
        } else { 
            pageEndRow = page.getTotalRecNum(); 
            pageStartRow = page.getPageSize() * (page.getTotalPageNum() - 1); 
        } 
		 list.subList(pageStartRow, pageEndRow);
		 
		 page.setPageContent(list);
		} catch (Exception e) {
			throw e;
		}
		return page;
		
	}
	
	
//	public static void main(String[] args) {
//		List<DayProfit> dayProfitList = new ArrayList<DayProfit>();
//		DayProfit dayProfit = new DayProfit();
//		dayProfit.setGain(new BigDecimal("5.55"));
//		dayProfitList.add(dayProfit);
//		dayProfit = new DayProfit();
//		dayProfit.setGain(new BigDecimal("4.5"));
//		dayProfitList.add(dayProfit);
//		dayProfit = new DayProfit();
//		dayProfit.setGain(new BigDecimal("5.51"));
//		dayProfitList.add(dayProfit);
//		dayProfit = new DayProfit();
//		dayProfit.setGain(new BigDecimal("11"));
//		dayProfitList.add(dayProfit);
//		Comparator<DayProfit> comparator = new GainComparable();
//		comparator=Collections.reverseOrder(comparator);
//		Collections.sort(dayProfitList,comparator);
//		for(DayProfit daProfit : dayProfitList){
//			System.out.println(daProfit.getGain());
//		}
//	}
}



