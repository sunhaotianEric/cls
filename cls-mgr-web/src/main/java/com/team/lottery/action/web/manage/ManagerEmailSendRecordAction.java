package com.team.lottery.action.web.manage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.Page;
import com.team.lottery.service.EmailSendRecordService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.vo.EmailSendRecord;

@Controller("managerEmailSendRecordAction")
public class ManagerEmailSendRecordAction {

	@Autowired
	private EmailSendRecordService emailSendRecordService;
	
	/**
	 * 分页查询
	 * @param query
	 * @param page
	 * @return
	 */
	public Object[] getAllEmailSendRecord(EmailSendRecord query,Page page){
		if(query==null||page==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		String bizSystem=BaseDwrUtil.getCurrentAdmin().getBizSystem();
		if(!bizSystem.equals(ConstantUtil.SUPER_SYSTEM)){
			query.setBizSystem(bizSystem);
		}
		page=emailSendRecordService.getAllEmailSendRecord(query, page);
		return AjaxUtil.createReturnValueSuccess(page);
	}
	
	/**
	 * 根据ID查询
	 * @param id
	 * @return
	 */
	public Object[] selectEmailSendRecord(Long id){
		EmailSendRecord record=emailSendRecordService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(record);
	}
	
	/**
	 * 修改
	 * @param record
	 * @return
	 */
	public Object[] updateEmailSendRecord(EmailSendRecord record){
		if(record==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
			emailSendRecordService.updateByPrimaryKeySelective(record);
		return AjaxUtil.createReturnValueSuccess();
	}
	
	/**
	 * 添加
	 * @param query
	 * @return
	 */
	public Object[] insertEmailSendRecord(EmailSendRecord record){
		if(record==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		emailSendRecordService.insertEmailSendRecord(record);
		return AjaxUtil.createReturnValueSuccess();
	}
	
	/**
	 * 根据ID删除
	 * @param id
	 * @return
	 */
	public Object[] deleteEmailSendRecord(Long id){
		if(id==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		emailSendRecordService.deleteByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess();
	}
	
	
}
