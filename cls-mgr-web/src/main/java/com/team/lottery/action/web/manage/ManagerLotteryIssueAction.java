package com.team.lottery.action.web.manage;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.enums.EAdminOperateLogModel;
import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.enums.ERedisChannel;
import com.team.lottery.extvo.LotteryIssueAdjustVo;
import com.team.lottery.extvo.LotteryIssueQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.AdminOperateLogService;
import com.team.lottery.service.LotteryIssueService;
import com.team.lottery.system.message.MessageQueueCenter;
import com.team.lottery.system.message.messagetype.LotteryIssueCacheMessage;
import com.team.lottery.util.AdminOperateUtil;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.LotteryIssue;

@Controller("managerLotteryIssueAction")
public class ManagerLotteryIssueAction {
	private static Logger logger = LoggerFactory.getLogger(ManagerLotteryIssueAction.class);
	@Autowired
	private LotteryIssueService lotteryIssueService;
	@Autowired
	private AdminOperateLogService adminOperateLogService;
	
    /**
     * 查找所有的开奖时间数据
     * @return
     */
    public Object[] getAllLotteryIssue(LotteryIssueQuery query,Page page){
    	if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	page = lotteryIssueService.getAllLotteryIssueByQueryPage(query,page);
		return AjaxUtil.createReturnValueSuccess(page);
    }
    
    /**
     * 修改保存lotteryIssue
     * @param lotteryIssue
     * @return
     */
    public Object[] saveOrUpdateLotteryIssue(LotteryIssue lotteryIssue){
    	try {
    		Integer id = lotteryIssue.getId();
    		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    		String operateDesc ="";
    		String operationIp = BaseDwrUtil.getRequestIp();
    		if(id == null){
    			ELotteryKind lotteryType = ELotteryKind.valueOf(lotteryIssue.getLotteryType());
    			String description = lotteryType.getDescription();
    			lotteryIssue.setLotteryName(description);
    			 operateDesc = AdminOperateUtil.constructMessage("lottery_issue_manage_add_log", currentAdmin.getUsername(),lotteryIssue.getLotteryType(),
    					lotteryIssue.getLotteryNum(),lotteryIssue.getBegintimeStr(),lotteryIssue.getEndtimeStr());
    			 	//保存日志
    	    		logger.info(operateDesc);
    	    		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.LOTTERY_ISSUE_MANAGE, operateDesc);
    	    		
    			 lotteryIssueService.insertSelective(lotteryIssue);
    		}else{
    			LotteryIssue oldLotteryIssue = lotteryIssueService.selectByPrimaryKey(id);
    			operateDesc = AdminOperateUtil.constructMessage("lottery_issue_manage_edit_log", currentAdmin.getUsername(),lotteryIssue.getLotteryType(),
    					lotteryIssue.getLotteryNum(),oldLotteryIssue.getLotteryNum(),lotteryIssue.getLotteryNum(),lotteryIssue.getBegintimeStr(),oldLotteryIssue.getBegintimeStr(),lotteryIssue.getEndtimeStr(),oldLotteryIssue.getBegintimeStr());
    			//保存日志
        		logger.info(operateDesc);
        		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.LOTTERY_ISSUE_MANAGE, operateDesc);
    			
        		lotteryIssueService.updateByPrimaryKeySelective(lotteryIssue);
    		}
    		
    		//通知前台刷新缓存
    		LotteryIssueCacheMessage lotteryIssueCacheMessage = new LotteryIssueCacheMessage();
    		lotteryIssueCacheMessage.setLotteryKindStr(lotteryIssue.getLotteryType());
    		MessageQueueCenter.putRedisMessage(lotteryIssueCacheMessage, ERedisChannel.CLSCACHE.name());
    		
    		return AjaxUtil.createReturnValueSuccess();
			
		} catch (Exception e) {
			return AjaxUtil.createReturnValueError(e.getMessage());
		}
    }
    
    /**
     * 根据id获取lotteryIssue
     * @param id
     * @return
     */
    public Object[] getLotteryIssueByid(Integer id){
    	if(id == null){
    		return AjaxUtil.createReturnValueError("参数错误");
    	}
    	LotteryIssue lotteryIssue = lotteryIssueService.selectByPrimaryKey(id);
    	return AjaxUtil.createReturnValueSuccess(lotteryIssue);
    }
    
    /**
     * 删除
     * @param id
     * @return
     */
    public Object[] delLotteryIssue(Integer id){
    	if(id == null){
    		return AjaxUtil.createReturnValueError("参数错误");
    	}
    	LotteryIssue lotteryIssue = lotteryIssueService.selectByPrimaryKey(id);
    	if(lotteryIssue != null){
    		//保存日志
    		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    		String operateDesc = AdminOperateUtil.constructMessage("lottery_issue_manage_del_log", currentAdmin.getUsername(),lotteryIssue.getLotteryType(),lotteryIssue.getLotteryNum());
    		String operationIp = BaseDwrUtil.getRequestIp();
    		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.LOTTERY_ISSUE_MANAGE, operateDesc);
    		logger.info(operateDesc);
    		
    		lotteryIssueService.deleteByPrimaryKey(id);
    	}
		//通知前台刷新缓存
		LotteryIssueCacheMessage lotteryIssueCacheMessage = new LotteryIssueCacheMessage();
		lotteryIssueCacheMessage.setLotteryKindStr(lotteryIssue.getLotteryType());
		MessageQueueCenter.putRedisMessage(lotteryIssueCacheMessage, ERedisChannel.CLSCACHE.name());
    	return AjaxUtil.createReturnValueSuccess();
    }
    
	/**
     *  调整倒计时时间
     * @return
     */
    public Object[] updateTimeBylotteryType(LotteryIssueAdjustVo record){
    	if(record == null){
    		return AjaxUtil.createReturnValueError("参数错误");
    	}
    	//保存日志
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	String operateDesc = AdminOperateUtil.constructMessage("lottery_issue_manage_adjust_log", currentAdmin.getUsername(),record.getLotteryType(),record.getAdjustMode()==0?"延迟":"提前",record.getAdjustmenTime().toString());
    	logger.info(operateDesc);
    	String operationIp = BaseDwrUtil.getRequestIp();
    	adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.LOTTERY_ISSUE_MANAGE, operateDesc);
    	
    	lotteryIssueService.updateTimeBylotteryType(record);
    	
		//通知前台刷新缓存
		LotteryIssueCacheMessage lotteryIssueCacheMessage = new LotteryIssueCacheMessage();
		lotteryIssueCacheMessage.setLotteryKindStr(record.getLotteryType());
		MessageQueueCenter.putRedisMessage(lotteryIssueCacheMessage, ERedisChannel.CLSCACHE.name());
    	return AjaxUtil.createReturnValueSuccess();
    }
}
