package com.team.lottery.action.web.manage;

import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.enums.EAdminOperateLogModel;
import com.team.lottery.enums.EPlatformChargePayStatus;
import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.platformchargeorder.PlatformChargeOrderMapper;
import com.team.lottery.service.AdminOperateLogService;
import com.team.lottery.service.PlatformChargeOrderService;
import com.team.lottery.util.AdminOperateUtil;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.DateUtils;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.PlatformChargeOrder;

@Controller("managerPlatformChangeOrderAction")
public class ManagerPlatformChangeOrderAction {
	

	@Autowired
	private PlatformChargeOrderService platformChargeOrderService;
	@Autowired
	private AdminOperateLogService adminOperateLogService;
	@Autowired
	private PlatformChargeOrderMapper platformChargeOrderMapper;
	
    /**
     * 分页查找
     * @return
     */
    public Object[] getPlatformChangeOrderByQueryPage(PlatformChargeOrder query,Page page){
      	if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
 
        Admin admin=BaseDwrUtil.getCurrentAdmin();
        if(!"SUPER_SYSTEM".equals(admin.getBizSystem())){
        	query.setBizSystem(admin.getBizSystem());
        }
        //当前日期上个月最后一天
        if(query.getBelongDateStart()!=null){
        	Date belongDateStart=query.getBelongDateStart();
        	Calendar calendar = Calendar.getInstance();
        	calendar.setTime(belongDateStart);
        	calendar.set(Calendar.DAY_OF_MONTH, 1);
        	calendar.add(Calendar.MILLISECOND, -1);
        	query.setBelongDateStart(calendar.getTime());
        }
        
        //当前日期下个月的第一天
        if(query.getBelongDateEnd()!=null){
        	Date belongDateEnd=query.getBelongDateEnd();
        	Calendar calendar = Calendar.getInstance();
        	calendar.setTime(belongDateEnd);
        	calendar.add(Calendar.MONTH, 1);
        	calendar.set(Calendar.DAY_OF_MONTH, 1);
        	query.setBelongDateEnd(calendar.getTime());
        }
    	page = platformChargeOrderService.getPlatformChargeOrderByQueryPage(query,page);
    	PlatformChargeOrder  order= platformChargeOrderService.getPlatformChargeOrderByQuerySum(query);
		return AjaxUtil.createReturnValueSuccess(page,order==null?null:order.getMoney(),order==null?null:order.getGain());
    }
    

    
    /**
     * 查找
     * @return
     */
    public Object[] getPlatformChangeOrderById(Long id){
    	PlatformChargeOrder platformChargeOrder =  platformChargeOrderService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(platformChargeOrder);
    }
    
    /**
     * 删除
     * @return
     */
    public Object[] delPlatformChangeOrder(Long id){
    	if(id == null){
    		return AjaxUtil.createReturnValueError("参数错误");
    	}
    	PlatformChargeOrder platformChargeOrder = platformChargeOrderService.selectByPrimaryKey(id);
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	//保存日志
		String operateDesc = AdminOperateUtil.constructMessage("platformCharge_del", currentAdmin.getUsername(), String.valueOf(platformChargeOrder.getId()), platformChargeOrder.getBizSystemName(),
				platformChargeOrder.getChargeTypeStr(), platformChargeOrder.getBelongDateStr());
		String operationIp = BaseDwrUtil.getRequestIp();
		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.PLATFORM_CHARGE, operateDesc);
		
    	int result=platformChargeOrderService.deleteByPrimaryKey(id);
    	if(result>0) {
    		return AjaxUtil.createReturnValueSuccess();
    	}else {
    		return AjaxUtil.createReturnValueError();
    	}
    }

    /**
     * 保存与更新
     * @return
     */
    public Object[] saveOrUpdatePlatformChangeOrder(PlatformChargeOrder record){
    	try{
    		Long id = record.getId();
    		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    		String operationIp = BaseDwrUtil.getRequestIp();
        	if(id==null){
        		record.setCreateTime(new Date());
        		record.setUpdateAdmin(BaseDwrUtil.getCurrentAdmin().getUsername());
        		record.setPayStatus(EPlatformChargePayStatus.PAYMENT.getCode());
        		String belongDateStr = DateUtils.getDateToStrByFormat(record.getBelongDate(), "yyyy-MM");
        		record.setBelongDateStr(belongDateStr);
        		
        		//保存日志
        		String operateDesc = AdminOperateUtil.constructMessage("platformCharge_add", currentAdmin.getUsername(), record.getBizSystemName(),
        				record.getChargeTypeStr(), String.valueOf(record.getMoney()), record.getBelongDateStr());
        		
        		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.PLATFORM_CHARGE, operateDesc);
        		
        		platformChargeOrderService.insertSelective(record);
        	//更新
        	}else{
        		record.setUpdateAdmin(BaseDwrUtil.getCurrentAdmin().getUsername());
        		record.setUpdateTime(new Date());
        		//不更新归属日期
        		record.setBelongDateStr(null);
        		PlatformChargeOrder platformChargeOrder = platformChargeOrderService.selectByPrimaryKey(record.getId());
         		
        		//保存日志
        		String operateDesc = AdminOperateUtil.constructMessage("platformCharge_edit", currentAdmin.getUsername(), platformChargeOrder.getBizSystemName(),
        				String.valueOf(platformChargeOrder.getMoney()), String.valueOf(record.getMoney()), String.valueOf(platformChargeOrder.getId()), 
        				platformChargeOrder.getChargeTypeStr(), platformChargeOrder.getBelongDateStr());
        		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.PLATFORM_CHARGE, operateDesc);
        		
        		platformChargeOrderService.updateByPrimaryKeySelective(record);
        		
        	}        	
    		return AjaxUtil.createReturnValueSuccess();
    	}catch(Exception e){
    		return AjaxUtil.createReturnValueError(e.getMessage());
    	}
    	
    }
    
    
    /**
     * 收取费用
     * @return
     */
	public Object[] getPlatformChangeOrderForMoney(Long id) {
		try {
			Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
			PlatformChargeOrder platformChargeOrder = platformChargeOrderMapper.selectByPrimaryKey(id);
			platformChargeOrder.setPayStatus(EPlatformChargePayStatus.PAYMENT_SUCCESS.name());
			platformChargeOrder.setUpdateTime(new Date());
			platformChargeOrder.setUpdateAdmin(currentAdmin.getUsername());
			platformChargeOrderMapper.updateByPrimaryKeySelective(platformChargeOrder);

			// 保存日志
			String operateDesc = AdminOperateUtil.constructMessage("platformCharge_charge", currentAdmin.getUsername(),
					String.valueOf(platformChargeOrder.getId()), platformChargeOrder.getBizSystemName(),
					platformChargeOrder.getChargeTypeStr(), platformChargeOrder.getBelongDateStr());
			String operationIp = BaseDwrUtil.getRequestIp();
			adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.PLATFORM_CHARGE, operateDesc);
			platformChargeOrderService.getPlatformChangeOrderForMoney(platformChargeOrder, currentAdmin);
			return AjaxUtil.createReturnValueSuccess();
		} catch (Exception e) {
			return AjaxUtil.createReturnValueError(e.getMessage());
		}
	}
    /**
     * 不收取费用
     * @return
     */
	public Object[] getPlatformChangeOrderPaymentClose(Long id) {
		try {
			Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
			PlatformChargeOrder platformChargeOrder = platformChargeOrderMapper.selectByPrimaryKey(id);
			platformChargeOrder.setPayStatus(EPlatformChargePayStatus.PAYMENT_CLOSE.name());
			platformChargeOrder.setUpdateTime(new Date());
			platformChargeOrder.setUpdateAdmin(currentAdmin.getUsername());
			platformChargeOrderMapper.updateByPrimaryKeySelective(platformChargeOrder);

			// 保存日志
			String operateDesc = AdminOperateUtil.constructMessage("platformCharge_chargeClose",
					currentAdmin.getUsername(), String.valueOf(platformChargeOrder.getId()),
					platformChargeOrder.getBizSystemName(), platformChargeOrder.getChargeTypeStr(),
					platformChargeOrder.getBelongDateStr());
			String operationIp = BaseDwrUtil.getRequestIp();
			adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.PLATFORM_CHARGE, operateDesc);
			platformChargeOrderService.getPlatformChangeOrderPaymentClose(platformChargeOrder, currentAdmin);
			return AjaxUtil.createReturnValueSuccess();
		} catch (Exception e) {
			return AjaxUtil.createReturnValueError(e.getMessage());
		}
	}
}
