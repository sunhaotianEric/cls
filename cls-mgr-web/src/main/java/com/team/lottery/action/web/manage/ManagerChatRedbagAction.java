package com.team.lottery.action.web.manage;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.Page;
import com.team.lottery.service.ChatRedbagInfoService;
import com.team.lottery.service.ChatRedbagService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.ChatRedbag;
import com.team.lottery.vo.ChatRedbagInfo;

@Controller("managerChatRedbagAction")
public class ManagerChatRedbagAction {

	@Autowired
	private ChatRedbagService chatRedbagService;//红包
	@Autowired
	private ChatRedbagInfoService chatRedbagInfoService;
	
	/**
	 * 发红包记录分页查询
	 * @param record
	 * @param page
	 * @return
	 * @throws ParseException 
	 */
	public Object[] getChatRedbagPage(ChatRedbag record,Page page){
		if(record==null||page==null){
			return AjaxUtil.createReturnValueError("参数传递错误");
		}
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		if(!"SUPER_SYSTEM".equals(currentAdmin.getBizSystem())&&record.getBizSystem()==null){
			record.setBizSystem(currentAdmin.getBizSystem());
		}
		page=chatRedbagService.getChatRedbagPage(record, page);
		return AjaxUtil.createReturnValueSuccess(page);
	}
	
	/**
	 * 抢红包记录分页查询
	 * @param record
	 * @param page
	 * @return
	 * @throws ParseException 
	 */
	public Object[] getChatRedbaginfoPage(ChatRedbagInfo record,Page page){
		if(record==null||page==null){
			return AjaxUtil.createReturnValueError("参数传递错误");
		}
/*		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		if(!"SUPER_SYSTEM".equals(currentAdmin.getBizSystem())&&record.getBizSystem()==null){
			record.setBizSystem(currentAdmin.getBizSystem());
		}*/
		page=chatRedbagInfoService.getChatRedbaginfoPage(record, page);
		return AjaxUtil.createReturnValueSuccess(page);
	}
	
	/**
	 * 查询抢过红包的用户
	 * @param rbid
	 * @return
	 */
	public Object[] getChatRedbaginfoByrid(Integer rbid){
		if(rbid==null){
			return AjaxUtil.createReturnValueError("参数传递错误");
		}
		return AjaxUtil.createReturnValueSuccess(chatRedbagInfoService.getChatRedbaginfoByrid(rbid));
	}
}
