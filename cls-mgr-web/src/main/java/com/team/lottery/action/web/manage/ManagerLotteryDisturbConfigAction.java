package com.team.lottery.action.web.manage;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.service.LotteryDisturbConfigService;
import com.team.lottery.service.UserService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.LotteryDisturbConfig;
import com.team.lottery.vo.User;

@Controller("managerLotteryDisturbConfigAction")
public class ManagerLotteryDisturbConfigAction {
	
	@Autowired
	private LotteryDisturbConfigService lotteryDisturbConfigService;
	@Autowired
	private UserService userService;
	
	/**
	 * 查找所有干扰配置
	 * @return
	 */
	public Object[] getLotteryDisturbConfig(){
		List<LotteryDisturbConfig> allLotteryDisturbConfigs = lotteryDisturbConfigService.queryAllLotteryDisturbConfigs();
		return AjaxUtil.createReturnValueSuccess(allLotteryDisturbConfigs);
	}
	
	/**
	 * 根据id查找干扰配置
	 * @param id
	 * @return
	 */
	public Object[] getLotteryDisturbConfigById(Long id){
		if(id == null){
			return AjaxUtil.createReturnValueError("参数错误！");
		}
		LotteryDisturbConfig disturbConfig = lotteryDisturbConfigService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(disturbConfig);
	}
	/**
	 *  删除干扰配置
	 * @param id
	 * @return
	 */
	public Object[] delLotteryDisturbCconfigById(Long id){
		if(id == null){
			return AjaxUtil.createReturnValueError("参数错误！");
		}
		lotteryDisturbConfigService.deleteByPrimaryKey(id);
		
		return AjaxUtil.createReturnValueSuccess("删除成功！");
	}
	
	/**
	 * 保存干扰配置
	 * @return
	 */
	public Object[] saveLotteryDisturbConfig(LotteryDisturbConfig lotteryDisturbConfig){
		Long id = lotteryDisturbConfig.getId();
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		if("SUPER_SYSTEM".equals(currentAdmin.getBizSystem())){
			return AjaxUtil.createReturnValueError("您是SUPER_SYSTEM，没有权限操作！");	
		}
		//根据保存的用户名查找当前的用户是否存在
		User user = userService.getUserByName(currentAdmin.getBizSystem(),lotteryDisturbConfig.getUserName());
		if(user == null){
			return AjaxUtil.createReturnValueError("没有该用户名！");
		}
		lotteryDisturbConfig.setUserId(user.getId());
		if(id == null){
			lotteryDisturbConfigService.insertSelective(lotteryDisturbConfig);
		}else{
			lotteryDisturbConfigService.updateByPrimaryKeySelective(lotteryDisturbConfig);
		}
		
		return AjaxUtil.createReturnValueSuccess("保存成功！");
	}
}
