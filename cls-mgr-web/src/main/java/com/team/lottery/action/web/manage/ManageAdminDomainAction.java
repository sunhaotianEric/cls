package com.team.lottery.action.web.manage;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.enums.EAdminOperateLogModel;
import com.team.lottery.enums.ERedisChannel;
import com.team.lottery.extvo.AdminDomainQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.message.messagetype.AdminDomainMessage;
import com.team.lottery.service.AdminDomainService;
import com.team.lottery.service.AdminOperateLogService;
import com.team.lottery.system.message.MessageQueueCenter;
import com.team.lottery.util.AdminOperateUtil;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.AdminDomain;

@Controller("manageAdminDomainAction")
public class ManageAdminDomainAction {
	private static Logger logger = LoggerFactory.getLogger(ManageAdminDomainAction.class);
	@Autowired
	public AdminDomainService adminDomainService;
	@Autowired
	private AdminOperateLogService adminOperateLogService;
	
	/**
	 *  分页查询域名IP授权管理 
	 * @return
	 */
	public Object[] getAllAdminDomainPage(AdminDomainQuery adminDomainQuery,Page page){
		if(page == null){
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		
		Admin currentUser = BaseDwrUtil.getCurrentAdmin();
      	String currentBizSystem = currentUser.getBizSystem();
      	   	if(adminDomainQuery.getBizSystem() == null || "".equals(adminDomainQuery.getBizSystem())){
    		if("SUPER_SYSTEM".equals(adminDomainQuery.getBizSystem())){
    			adminDomainQuery.setBizSystem(currentBizSystem);
    		}
    	}
		
      	  Page adminDomainPage = adminDomainService.getAllAdminDomainPage(adminDomainQuery,page);
  		
  		return AjaxUtil.createReturnValueSuccess(adminDomainPage);
	}
	
	/**
	 * 保存后台域名IP授权管理
	 * @param adminDomain
	 * @return
	 */
	public Object[] saveAdminDomain(AdminDomain adminDomain){
		if(adminDomain == null){
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		Integer id = adminDomain.getId();
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		if(!currentAdmin.getBizSystem().equals("SUPER_SYSTEM")){
			adminDomain.setBizSystem(currentAdmin.getBizSystem());
		}
		String operationIp = BaseDwrUtil.getRequestIp();
		int successCount = 0;
		String failRecord = "";
		String successRecord = "";
		if(id == null){
			String allowIps = adminDomain.getAllowIps();
			String domains = adminDomain.getDomains();
			String[] allowIpsList = allowIps.split(",");
			String[] domainsList = domains.split(",");
			
			AdminDomain adminDomain1 = new AdminDomain();
			adminDomain1.setCreateTime(new Date());
			adminDomain1.setCreateAdmin(currentAdmin.getUsername());
			adminDomain1.setEnable(1);
			AdminDomainQuery query = new AdminDomainQuery();
			for(int i=0;i<domainsList.length;i++){
				String domain = domainsList[i];
				query.setDomains(domain);
				query.setBizSystem(adminDomain.getBizSystem());
				adminDomain1.setBizSystem(adminDomain.getBizSystem());
				AdminDomain adminAllowByBizSystem = adminDomainService.getAdminDomainByBizSystem(query);
				//只保存未保存的domain
				if(adminAllowByBizSystem == null){
					adminDomain1.setDomains(domain);
					adminDomain1.setCreateTime(new Date());
					adminDomain1.setCreateAdmin(currentAdmin.getUsername());
					adminDomainService.insertSelective(adminDomain1);
					successRecord += domain+ ",";
					successCount ++;
				}else{
					failRecord += domain + ",";
				}
			}
			
			AdminDomainQuery queryAllowIps = new AdminDomainQuery();
			AdminDomain adminDomain2 = new AdminDomain();
			for(int j=0;j<allowIpsList.length;j++){
				String domain = domainsList[j];
				String allowIp = allowIpsList[j];
				query.setDomains(domain);
				queryAllowIps.setDomains(domain);
				queryAllowIps.setBizSystem(adminDomain.getBizSystem());
				
				AdminDomain adminDomainByBizSystem = adminDomainService.getAdminDomainByBizSystem(query);
				AdminDomain adminAllowIpByBizSystem = adminDomainService.getAdminDomainByBizSystem(queryAllowIps);
				
				//只保存未保存的ip
				if(adminDomainByBizSystem != null && adminDomainByBizSystem.getAllowIps() == null){
					adminDomain2.setId(adminDomainByBizSystem.getId());
					adminDomain2.setAllowIps(allowIp);
					adminDomain2.setUpdateTime(new Date());
					adminDomain2.setUpdateAdmin(currentAdmin.getUsername());
					adminDomainService.updateByPrimaryKeySelective(adminDomain2);
					successRecord += allowIp+ ",";
				}else if(adminAllowIpByBizSystem == null){
					adminDomain2.setAllowIps(allowIp);
					adminDomain2.setCreateTime(new Date());
					adminDomain2.setCreateAdmin(currentAdmin.getUsername());
					adminDomainService.insertSelective(adminDomain2);
					successRecord += allowIp+ ",";
				}else{
					failRecord += allowIp + ",";
				}
			}
			
			String failRecords = failRecord == ""? "" :  ",添加失败域名或IP:"+failRecord.substring(0,failRecord.length()-1);
			
			String operateDesc = AdminOperateUtil.constructMessage("adminDomain_add", currentAdmin.getUsername(),adminDomain.getBizSystem(),successRecord==""?"":successRecord.substring(0,successRecord.length()-1));
			
			adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.ADMIN_DOMAIN, operateDesc);
    		//通知Redis刷新域名管理缓存
			this.refreshCache();
			return AjaxUtil.createReturnValueSuccess("成功添加" +successCount+"条"+failRecords);
		}else{
			adminDomain.setUpdateTime(new Date());
			adminDomain.setUpdateAdmin(currentAdmin.getUsername());
			AdminDomain oldAdminDomain = adminDomainService.selectByPrimaryKey(adminDomain.getId());
			successCount = adminDomainService.updateByPrimaryKeySelective(adminDomain);
			
			String operateDesc = AdminOperateUtil.constructMessage("adminDomain_edit", currentAdmin.getUsername(),adminDomain.getBizSystem(),oldAdminDomain.getDomains(),adminDomain.getDomains(),oldAdminDomain.getAllowIps(),adminDomain.getAllowIps());
			adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.ADMIN_DOMAIN, operateDesc);
    		//发布redis消息,准备更新缓存
			this.refreshCache();
			return AjaxUtil.createReturnValueSuccess("成功更新" +successCount+"条");
		}
	}
	
	/**
	 * 查询adminDomin
	 * @param id
	 * @return
	 */
	public Object[] getAdminAllowById(Integer id){
		if(id == null){
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		AdminDomain adminDomain = adminDomainService.selectByPrimaryKey(id);
		
		return AjaxUtil.createReturnValueSuccess(adminDomain);
	}
	
	/**
	 * 删除记录
	 * @param id
	 * @return
	 */
	public Object[] delAdminAllowById(Integer id){
		if(id == null){
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		AdminDomain oldAdminDomain = adminDomainService.selectByPrimaryKey(id);
		
		String operateDesc = AdminOperateUtil.constructMessage("adminDomain_del", currentAdmin.getUsername(),oldAdminDomain.getBizSystem(),oldAdminDomain.getDomains()==null?"":oldAdminDomain.getDomains(),
				oldAdminDomain.getAllowIps()==null?"":oldAdminDomain.getAllowIps());
		String operationIp = BaseDwrUtil.getRequestIp();
		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.ADMIN_DOMAIN, operateDesc);
		adminDomainService.deleteByPrimaryKey(id);
		//发布redis消息,准备更新缓存
		this.refreshCache();
		return AjaxUtil.createReturnValueSuccess();
	}
	
	/**
	 * 启用域名
	 * @param id
	 * @return
	 */
	public Object[] enableAdminDomain(Integer id){
		if(id == null){
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		
		AdminDomain adminDomain = new AdminDomain();
		adminDomain.setId(id);
		adminDomain.setEnable(1);
		
		AdminDomain oldAdminDomain = adminDomainService.selectByPrimaryKey(id);
		
		String operateDesc = AdminOperateUtil.constructMessage("adminDomain_open", currentAdmin.getUsername(),oldAdminDomain.getBizSystem(),oldAdminDomain.getDomains()==null?"":oldAdminDomain.getDomains(),
				oldAdminDomain.getAllowIps()==null?"":oldAdminDomain.getAllowIps());
		String operationIp = BaseDwrUtil.getRequestIp();
		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.ADMIN_DOMAIN, operateDesc);
		
		adminDomainService.updateByPrimaryKeySelective(adminDomain);
		//发布redis消息,准备更新缓存
		this.refreshCache();
		return AjaxUtil.createReturnValueSuccess();
	}
	
	/**
	 * 停用域名
	 * @param id
	 * @return
	 */
	public Object[] disAbleAdminDomain(Integer id){
		if(id == null){
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		
		AdminDomain adminDomain = new AdminDomain();
		adminDomain.setId(id);
		adminDomain.setEnable(0);
		
		AdminDomain oldAdminDomain = adminDomainService.selectByPrimaryKey(id);
		
		String operateDesc = AdminOperateUtil.constructMessage("adminDomain_close", currentAdmin.getUsername(),oldAdminDomain.getBizSystem(),oldAdminDomain.getDomains()==null?"":oldAdminDomain.getDomains(),
				oldAdminDomain.getAllowIps()==null?"":oldAdminDomain.getAllowIps());
		String operationIp = BaseDwrUtil.getRequestIp();
		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null,"", EAdminOperateLogModel.ADMIN_DOMAIN, operateDesc);
		
		adminDomainService.updateByPrimaryKeySelective(adminDomain);
		//发布redis消息,准备更新缓存
		this.refreshCache();
		return AjaxUtil.createReturnValueSuccess();
	}
	
	/**
	 * 刷新域名管理缓存.
	 * @return
	 */
    private Object[] refreshCache(){
    	//通知系统刷新域名Redis缓存G
		try {
			AdminDomainMessage adminDomainMessage = new AdminDomainMessage();
			adminDomainMessage.setType(ERedisChannel.CLSCACHE.name());
			MessageQueueCenter.putRedisMessage(adminDomainMessage,ERedisChannel.CLSCACHE.name());
		} catch (Exception e) {
			logger.error("后台域名IP授权缓存刷新出现异常.");
			AjaxUtil.createReturnValueError("后台域名IP授权缓存刷新异常.");
		}
	 return AjaxUtil.createReturnValueSuccess();
    }
}
