package com.team.lottery.action.web.manage;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;

import com.team.lottery.enums.ERedisChannel;
import com.team.lottery.extvo.LotteryWinLhcParam;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.LotteryWinLhcService;
import com.team.lottery.system.message.LHCRefreshCacheLocalMessage;
import com.team.lottery.system.message.MessageQueueCenter;
import com.team.lottery.system.message.messagetype.LotterySwitchMessage;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.LotteryWinLhc;

@Controller("managerLotteryWinLhcAction")
public class ManagerLotteryWinLhcAction {

	@Autowired
	private LotteryWinLhcService lotteryWinLhcService;

	/**
	 * 
	 * @param query
	 * @param page
	 * @return
	 */
	public Object[] getLotteryWinLhcByPage(LotteryWinLhc query, Page page) {
		if (query == null || page == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		Admin currentUser = BaseDwrUtil.getCurrentAdmin();
		String bizSystem = currentUser.getBizSystem();
		if (query.getBizSystem() == null) {
			if (!"SUPER_SYSTEM".equals(bizSystem)) {
				query.setBizSystem(bizSystem);
			}
		}
		Page winLhcByPage = lotteryWinLhcService.getLotteryWinLhcByPage(query, page);

		return AjaxUtil.createReturnValueSuccess(winLhcByPage);
	}

	/**
	 * 
	 * @param query
	 * @param page
	 * @return
	 */
	public Object[] getLotteryWinLhcByQueryCondition(LotteryWinLhc query) {
		if (query == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		Admin currentUser = BaseDwrUtil.getCurrentAdmin();
		String bizSystem = currentUser.getBizSystem();
		if (query.getBizSystem() == null) {
			if (!"SUPER_SYSTEM".equals(bizSystem)) {
				query.setBizSystem(bizSystem);
			}
		}
		List<LotteryWinLhc> list = lotteryWinLhcService.getLotteryWinLhcByKind(query);
		return AjaxUtil.createReturnValueSuccess(list);
	}

	/**
	 * 
	 * @param lotteryWinLhc
	 * @return
	 */
	public Object[] updateLotteryWinLhcByCodes(LotteryWinLhcParam lotteryWinLhcParam) {

		try {
			if (lotteryWinLhcParam == null || lotteryWinLhcParam.getLotteryWinLhcArr() == null) {
				return AjaxUtil.createReturnValueError("参数传递错误.");
			}
			lotteryWinLhcService.bathUpdateByPrimaryKey(lotteryWinLhcParam.getLotteryWinLhcArr());
			// 修改赔率成功后，通知本地缓存修改数据
			refreshLhcPlaykindWinCache();
			return AjaxUtil.createReturnValueSuccess();
		} catch (Exception e) {
			return AjaxUtil.createReturnValueError(e.getMessage());
		}
	}

	/**
	 * 根据业务系统刷新彩种开关缓存.
	 * 
	 * @param bizSystem
	 * @return
	 */
	private void refreshLhcPlaykindWinCache() {
		// 通知系统刷新缓存
		LHCRefreshCacheLocalMessage lhcRefreshCacheLocalMessage = new LHCRefreshCacheLocalMessage();
		lhcRefreshCacheLocalMessage.setType(ERedisChannel.CLSCACHE.name());
		MessageQueueCenter.putRedisMessage(lhcRefreshCacheLocalMessage, ERedisChannel.CLSCACHE.name());
	}

	/**
	 * 
	 * @param lotteryWinLhc
	 * @return
	 */
	public Object[] updateLotteryWinLhc(LotteryWinLhc lotteryWinLhc) {
		if (lotteryWinLhc == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		try {
			Integer id = lotteryWinLhc.getId();
			String bizSystem = BaseDwrUtil.getCurrentAdmin().getBizSystem();
			if (!"SUPER_SYSTEM".equals(bizSystem)) {
				lotteryWinLhc.setBizSystem(bizSystem);
			}

			List<LotteryWinLhc> lotteryWinLhcList = lotteryWinLhcService.getLotteryWinLhcByLotteryTypeProtype(lotteryWinLhc);
			Integer order = 0;
			if (!CollectionUtils.isEmpty(lotteryWinLhcList)) {
				Iterator<LotteryWinLhc> iterator = lotteryWinLhcList.iterator();
				while (iterator.hasNext()) {
					LotteryWinLhc nextLotteryWinLhc = iterator.next();
					if (order < nextLotteryWinLhc.getOrders()) {
						order = nextLotteryWinLhc.getOrders();
					}
				}
				// orders只更新/插入数据库里不存在的
				if (id == null) {
					lotteryWinLhc.setOrders(order + 1);
				}
			}
			if (id == null) {
				lotteryWinLhc.setOrders(order + 1);
				lotteryWinLhcService.insertSelective(lotteryWinLhc);
			} else {
				lotteryWinLhcService.updateByPrimaryKeySelective(lotteryWinLhc);
			}

			return AjaxUtil.createReturnValueSuccess();
		} catch (Exception e) {
			return AjaxUtil.createReturnValueError(e.getMessage());
		}
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Object[] getLotteryWinLhcById(Integer id) {
		if (id == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		LotteryWinLhc selectByPrimaryKey = lotteryWinLhcService.selectByPrimaryKey(id);

		return AjaxUtil.createReturnValueSuccess(selectByPrimaryKey);
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Object[] deleteLotteryWinLhcById(Integer id) {
		if (id == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		lotteryWinLhcService.deleteByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess();

	}

}
