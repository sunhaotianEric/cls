package com.team.lottery.action.web.manage;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.UserQuotaService;
import com.team.lottery.service.UserRegisterService;
import com.team.lottery.service.UserService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.AwardModelUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.AwardModelConfig;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserRegister;

@Controller("managerUserRegisterAction")
public class ManagerUserRegisterAction {

	
	@Autowired
	private UserRegisterService userRegisterService;
	@Autowired
	private UserQuotaService userQuotaService;
	@Autowired
	private UserService userService;
	
	/**
	 * 查询推广数据
	 * @return
	 */
	public Object[] queryUserRegister(UserRegister query,Page page){
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	//非SUPER_SYSTEM,只能查询本业务系统的用户
    	if(!"SUPER_SYSTEM".equals(currentAdmin.getBizSystem()))
    	{
    		query.setBizSystem(currentAdmin.getBizSystem());
    	}
    	page=userRegisterService.queryUserRegister(query,page);
    	
		return AjaxUtil.createReturnValueSuccess(page);
	}
	
	/**
	 * 查询单个推广链接数据
	 * @return
	 */
	public Object[] getRegisterLink(Integer id){
		UserRegister userRegister=userRegisterService.selectByPrimaryKey(id);
    	User user=userService.getUserByName(userRegister.getBizSystem(), userRegister.getUserName());
		return AjaxUtil.createReturnValueSuccess(userRegister,user);
	}
	
	/**
     * 获取业务系统开立模式的最高最低值
     * @return
     */
    public Object[] loadUserRebate(){
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	AwardModelConfig awardModelConfig = ClsCacheManager.getAwardModelConfigByCode(currentAdmin.getBizSystem());
		return AjaxUtil.createReturnValueSuccess(awardModelConfig);
    }
    
    /**
     * 修改推广链接参数
     * @return
     */
    public Object[] updateUserRegisterLink(UserRegister userRegister){
    	if(userRegister == null || userRegister.getId() == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	
    	if(userRegister.getSscRebate() == null || "".equals(userRegister.getSscRebate())){
    		return AjaxUtil.createReturnValueError("时时彩模式不能为空！");
    	}
    	if(userRegister.getSyxwRebate() == null || "".equals(userRegister.getSyxwRebate())){
    		return AjaxUtil.createReturnValueError("十一选五模式不能为空！");
    	}
    	if(userRegister.getKsRebate() == null || "".equals(userRegister.getKsRebate())){
    		return AjaxUtil.createReturnValueError("快三模式不能为空！");
    	}
    	if(userRegister.getPk10Rebate() == null || "".equals(userRegister.getPk10Rebate())){
    		return AjaxUtil.createReturnValueError("PK10模式不能为空！");
    	}
    	if(userRegister.getDpcRebate() == null || "".equals(userRegister.getDpcRebate())){
    		return AjaxUtil.createReturnValueError("低频彩模式不能为空！");
    	}
    	if(userRegister.getInvitationCode()==null ||"".equals(userRegister.getInvitationCode())){
    		return AjaxUtil.createReturnValueError("邀请码不能为空！");
    	}
    	
    	UserRegister oldUserRegister = userRegisterService.selectByPrimaryKey(userRegister.getId());
    	
    	//查询是否存在
    	UserRegister invitationCount=userRegisterService.getUserRegisterLinkByInvitationCode(userRegister.getInvitationCode(), oldUserRegister.getBizSystem());
    	if(invitationCount!=null){
    		if(invitationCount.getId().intValue()!=userRegister.getId().intValue()){
    			return AjaxUtil.createReturnValueError("邀请码已存在,请重新输入");
    		}
    	}
    	
    	User currentUser=userService.getUserByName(oldUserRegister.getBizSystem(),oldUserRegister.getUserName());
    	String linkSet = oldUserRegister.getLinkSets();
    	String[] linkSets =  linkSet.split(ConstantUtil.REGISTER_SPLIT);
    	StringBuffer registerSb = new StringBuffer();
    	//代理类型
    	registerSb.append(linkSets[0]);
    	
    	//模式校验和拼接推广链接,如果未设置该彩种的奖金模式,则按系统的最低奖金模式进行设置
    	User newRegisterUser = new User();
    	//根据时时彩模式   自定义其他彩种的模式   使用分开控制
    	newRegisterUser.setSscRebate(userRegister.getSscRebate());
    	newRegisterUser.setFfcRebate(AwardModelUtil.getModelBySsc(userRegister.getSscRebate(), ELotteryTopKind.FFC));
    	newRegisterUser.setSyxwRebate(userRegister.getSyxwRebate());
    	newRegisterUser.setKsRebate(userRegister.getKsRebate());
    	newRegisterUser.setPk10Rebate(userRegister.getPk10Rebate());
    	newRegisterUser.setDpcRebate(userRegister.getDpcRebate());
    	newRegisterUser.setTbRebate(AwardModelUtil.getModelBySsc(userRegister.getSscRebate(), ELotteryTopKind.TB));
    	newRegisterUser.setLhcRebate(AwardModelUtil.getModelBySsc(userRegister.getSscRebate(), ELotteryTopKind.LHC));
    	newRegisterUser.setKlsfRebate(AwardModelUtil.getModelBySsc(userRegister.getSscRebate(), ELotteryTopKind.KLSF));
    	
    	if(userRegister.getSscRebate() != null){  //时时彩
    		Object [] result = AwardModelUtil.userAwardModelCheck(newRegisterUser,currentUser);
    		if(result[0] == ConstantUtil.DWR_AJAX_RESULT_ERROR){
    			return result;
    		}
    	}
    	//注册用户模式
    	registerSb.append(ConstantUtil.REGISTER_SPLIT).append(newRegisterUser.getSscRebate());
    	registerSb.append(ConstantUtil.REGISTER_SPLIT).append(newRegisterUser.getFfcRebate());
    	registerSb.append(ConstantUtil.REGISTER_SPLIT).append(newRegisterUser.getSyxwRebate());
    	registerSb.append(ConstantUtil.REGISTER_SPLIT).append(newRegisterUser.getKsRebate());
    	registerSb.append(ConstantUtil.REGISTER_SPLIT).append(newRegisterUser.getPk10Rebate());
    	registerSb.append(ConstantUtil.REGISTER_SPLIT).append(newRegisterUser.getDpcRebate());
    	registerSb.append(ConstantUtil.REGISTER_SPLIT).append(newRegisterUser.getTbRebate());
    	registerSb.append(ConstantUtil.REGISTER_SPLIT).append(newRegisterUser.getLhcRebate());
    	registerSb.append(ConstantUtil.REGISTER_SPLIT).append(newRegisterUser.getKlsfRebate());
    	if(oldUserRegister.getBonusScale() != null){
			registerSb.append(ConstantUtil.REGISTER_SPLIT).append(oldUserRegister.getBonusScale());
		}else{
    		registerSb.append(ConstantUtil.REGISTER_SPLIT).append(ConstantUtil.REGISTER_NULL_REPLACE);
		}
		
		//赠送资金  目前为空
    	registerSb.append(ConstantUtil.REGISTER_SPLIT).append(ConstantUtil.REGISTER_NULL_REPLACE);
    	userRegister.setLinkSets(registerSb.toString());
    	// 设置代理类型.
    	userRegister.setDailiLevel(linkSets[0]);
    	// 设置时时彩模式.
    	userRegister.setSscRebate(newRegisterUser.getSscRebate());
    	// 设置分分彩模式.
    	userRegister.setFfcRebate(newRegisterUser.getFfcRebate());
    	// 设置十一选五模式.
    	userRegister.setSyxwRebate(newRegisterUser.getSyxwRebate());
    	// 设置快三模式.
    	userRegister.setKsRebate(newRegisterUser.getKsRebate());
    	// 设置PK10模式.
    	userRegister.setPk10Rebate(newRegisterUser.getPk10Rebate());
    	// 设置低频彩模式.
    	userRegister.setDpcRebate(newRegisterUser.getDpcRebate());
    	// 设置投宝模式.
    	userRegister.setTbRebate(newRegisterUser.getTbRebate());
    	// 设置六合彩模式.
    	userRegister.setLhcRebate(newRegisterUser.getLhcRebate());
    	// 设置快乐十分模式.
    	userRegister.setKlsfRebate(newRegisterUser.getKlsfRebate());
    	// 设置赠送金额.
    	userRegister.setPresentMoney(BigDecimal.ZERO);
    	// 修改时间.
    	userRegister.setUpdatedDate(new Date());
    	// 修改数据.
    	userRegisterService.updateByPrimaryKeySelective(userRegister);
    	return AjaxUtil.createReturnValueSuccess();
    }
    
    /**
     * 删除推广链接
     * @return
     */
    public Object[] delRegtister(Integer id){
    	userRegisterService.delRegister(id);
    	return AjaxUtil.createReturnValueSuccess();
    }
}
