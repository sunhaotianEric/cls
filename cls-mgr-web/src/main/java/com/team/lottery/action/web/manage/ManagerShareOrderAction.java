package com.team.lottery.action.web.manage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.Page;
import com.team.lottery.service.ShareOrderService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.vo.ShareOrder;

@Controller("managerShareOrderAction")
public class ManagerShareOrderAction {

	@Autowired
	private ShareOrderService shareOrderService;
	
	public Object[] getShareOrderPage(ShareOrder shareOrder,Page page){
		if(shareOrder==null||page==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		page=shareOrderService.getShareOrderPage(shareOrder, page);
		return AjaxUtil.createReturnValueSuccess(page);
	}
}
