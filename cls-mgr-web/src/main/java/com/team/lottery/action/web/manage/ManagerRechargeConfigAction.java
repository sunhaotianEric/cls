package com.team.lottery.action.web.manage;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.enums.EAdminOperateLogModel;
import com.team.lottery.enums.EFundPayType;
import com.team.lottery.enums.EFundRefType;
import com.team.lottery.extvo.ChargePayVo;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.PayBankVo;
import com.team.lottery.extvo.RechargeConfigVo;
import com.team.lottery.service.AdminOperateLogService;
import com.team.lottery.service.ChargePayService;
import com.team.lottery.service.PayBankService;
import com.team.lottery.service.RechargeConfigService;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.util.AdminOperateUtil;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.RechargeConfig;
import com.team.lottery.vo.ThirdPayConfig;

@Controller("managerRechargeConfigAction")
public class ManagerRechargeConfigAction {

	private static Logger log = LoggerFactory.getLogger(ManagerRechargeConfigAction.class);

	@Autowired
	private ChargePayService chargePayService;
	@Autowired
	private AdminOperateLogService adminOperateLogService;
	@Autowired
	private PayBankService payBankService;
	@Autowired
	private RechargeConfigService rechargeConfigService;
	@Autowired
	private ThirdPayConfigService thirdPayConfigService;

	/**
	 * 查找所有的充值配置信息
	 * 
	 * @return
	 */
	public Object[] getAllRechargeConfig(RechargeConfigVo query) {
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		if (!ConstantUtil.SUPER_SYSTEM.equals(currentAdmin.getBizSystem())) {
			query.setBizSystem(currentAdmin.getBizSystem());
		}
		List<RechargeConfig> rechargeConfig = rechargeConfigService.getAllRechargeConfig(query);
		return AjaxUtil.createReturnValueSuccess(rechargeConfig);
	}

	/**
	 * 开启充值配置
	 * 
	 * @return
	 */
	public Object[] openRechargeConfig(Long id) {
		// 保存操作日志
		saveRechargeConfigLog("rechargeConfig_open", null, id);
		rechargeConfigService.updateRechargeConfigEnabled(id, 1);
		return AjaxUtil.createReturnValueSuccess();
	}

	/**
	 * 关闭充值配置
	 * 
	 * @return
	 */
	public Object[] closeRechargeConfig(Long id) {
		// 保存操作日志
		saveRechargeConfigLog("rechargeConfig_close", null, id);
		rechargeConfigService.updateRechargeConfigEnabled(id, 0);
		return AjaxUtil.createReturnValueSuccess();
	}

	/**
	 * 保存充值配置操作的日志
	 * 
	 * @param key
	 * @param domain
	 */
	private void saveRechargeConfigLog(String key, RechargeConfig rechargeConfig, Long id) {
		try {
			if (rechargeConfig == null) {
				rechargeConfig = rechargeConfigService.selectByPrimaryKey(id);
			}
			Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
			EFundPayType payType = EFundPayType.valueOf(rechargeConfig.getPayType());
			EFundRefType refType = EFundRefType.valueOf(rechargeConfig.getRefType());
			String operateDesc = AdminOperateUtil.constructMessage(key, currentAdmin.getUsername(), rechargeConfig.getId().toString(), payType.getDescription(),
					refType.getDescription());
			String operationIp = BaseDwrUtil.getRequestIp();
			adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.RECHARGECONFIG_MANAGE,
					operateDesc);
		} catch (Exception e) {
			// 保存日志出现异常 不进行处理
			log.error("修改充值配置，保存添加操作日志出错", e);
		}
	}

	/**
	 * 删除指定的充值配置
	 * 
	 * @return
	 */
	public Object[] delRechargeConfig(Long id) {
		try {
			Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
			RechargeConfig rechargeConfig = rechargeConfigService.selectByPrimaryKey(id);
			EFundPayType payType = EFundPayType.valueOf(rechargeConfig.getPayType());
			EFundRefType refType = EFundRefType.valueOf(rechargeConfig.getRefType());
			String operateDesc = AdminOperateUtil.constructMessage("rechargeConfig_del", currentAdmin.getUsername(), rechargeConfig.getBizSystem(), payType.getDescription(),
					refType.getDescription());
			String operationIp = BaseDwrUtil.getRequestIp();
			adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.RECHARGECONFIG_MANAGE,
					operateDesc);
		} catch (Exception e) {
			// 保存日志出现异常 不进行处理
			log.error("删除充值配置，保存添加操作日志出错", e);
		}
		rechargeConfigService.deleteByPrimaryKey(id);
		// 通知前端应用刷新缓存就够了
		/*
		 * try { NotifyFrontInterface.notifyFrontRefreshCache(EFrontCacheType.
		 * FRONT_INDEX_CACHE_INFO.getCode()); } catch (Exception e) {
		 * log.error(e.getMessage());
		 * AjaxUtil.createReturnValueError(e.getMessage()); }
		 */
		return AjaxUtil.createReturnValueSuccess();
	}

	/**
	 * 保存
	 * 
	 * @return
	 */
	public Object[] saveOrUpdateRechargeConfig(RechargeConfig rechargeConfig) {
		try {
			Long id = rechargeConfig.getId();

			Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
			if (!ConstantUtil.SUPER_SYSTEM.equals(currentAdmin.getBizSystem())) {
				rechargeConfig.setBizSystem(currentAdmin.getBizSystem());
			}

			EFundPayType payType = EFundPayType.valueOf(rechargeConfig.getPayType());
			EFundRefType refType = EFundRefType.valueOf(rechargeConfig.getRefType());
			String enabledStr = rechargeConfig.getEnabled() == 1 ? "启用" : "关闭";
			String bankName = "";
			String bankUserName = "";
			ThirdPayConfig thirdPayType = null;
			if (rechargeConfig.getRefType().equals(EFundRefType.TRANSFER.getCode())) {
				bankName = rechargeConfig.getBankName();
				bankUserName = rechargeConfig.getBankUserName();
			} else {
				if (rechargeConfig.getChargeType() != null) {
					//thirdPayType = EFundThirdPayType.valueOf(rechargeConfig.getChargeType());
					thirdPayType=thirdPayConfigService.thirdPayConfigByChargeType(rechargeConfig.getChargeType());
					
				}
			}
			/*
			 * // 新增的时候做数据校验 if
			 * (rechargeConfig.getShowUserVipLevel()!=0&&rechargeConfig
			 * .getShowUserVipLevels()!=null) { return
			 * AjaxUtil.createReturnValueError("请将VI"); }
			 */
			String checkUserVipLevels = checkUserVipLevels(rechargeConfig);
			if (checkUserVipLevels != null) {
				return AjaxUtil.createReturnValueError(checkUserVipLevels);
			}
			if (id == null) {
				// 新增的时候判断输入的VIP等级和VIP是否有同时输入;
				if (!rechargeConfig.getShowUserVipLevels().isEmpty() && rechargeConfig.getShowUserVipLevel() != 0) {
					return AjaxUtil.createReturnValueError("请将VIP等级设置为0!");
				}
				Integer showUserVipLevel = rechargeConfig.getShowUserVipLevel();
				if (showUserVipLevel instanceof Integer) {
					rechargeConfig.setCreateTime(new Date());
					rechargeConfig.setCreateAdmin(currentAdmin.getUsername());
					// 保存日志
					String operateDesc = AdminOperateUtil.constructMessage("rechargeConfig_add", currentAdmin.getUsername(), rechargeConfig.getBizSystem(), payType.getDescription(),
							refType.getDescription(), thirdPayType == null ? "" : thirdPayType.getChargeDes(), bankName, bankUserName, rechargeConfig.getShowName(), enabledStr);
					String operationIp = BaseDwrUtil.getRequestIp();
					adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.RECHARGECONFIG_MANAGE,
							operateDesc);

					rechargeConfigService.insert(rechargeConfig);
				} else {
					AjaxUtil.createReturnValueError("VIP数据类型错误,应该为正整数");
				}

			} else {

				RechargeConfig oldRechargeConfig = rechargeConfigService.selectByPrimaryKey(rechargeConfig.getId());
				EFundPayType oldPayType = EFundPayType.valueOf(oldRechargeConfig.getPayType());
				EFundRefType oldRefType = EFundRefType.valueOf(oldRechargeConfig.getRefType());
				String oldEnabledStr = oldRechargeConfig.getEnabled() == 1 ? "启用" : "关闭";
				String oldBankName = "";
				String oldBankUserName = "";
				ThirdPayConfig oldThirdPayType = null;

				if (oldRechargeConfig.getRefType().equals(EFundRefType.TRANSFER.getCode())) {
					oldBankName = oldRechargeConfig.getBankName();
					oldBankUserName = oldRechargeConfig.getBankUserName();
				} else {
					if (oldRechargeConfig.getChargeType() != null) {
						oldThirdPayType = thirdPayConfigService.thirdPayConfigByChargeType(oldRechargeConfig.getChargeType());
						
					}
				}
				// 保存日志
				String operateDesc = AdminOperateUtil.constructMessage("rechargeConfig_edit", currentAdmin.getUsername(), oldRechargeConfig.getBizSystem(),
						rechargeConfig.getBizSystem(), oldPayType.getDescription(), payType.getDescription(), oldRefType.getDescription(), refType.getDescription(),
						oldThirdPayType == null ? "" : oldThirdPayType.getChargeDes(), thirdPayType == null ? "" : thirdPayType.getChargeDes(), oldBankName, bankName,
						oldBankUserName, bankUserName, oldRechargeConfig.getShowName(), rechargeConfig.getShowName(), oldEnabledStr, enabledStr);
				String operationIp = BaseDwrUtil.getRequestIp();
				adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.RECHARGECONFIG_MANAGE,
						operateDesc);

				rechargeConfig.setUpdateTime(new Date());
				rechargeConfig.setUpdateAdmin(currentAdmin.getUsername());
				rechargeConfigService.updateByPrimaryKey(rechargeConfig);
			}
			/*
			 * //通知前端应用刷新缓存就够了 try {
			 * NotifyFrontInterface.notifyFrontRefreshCache
			 * (EFrontCacheType.FRONT_INDEX_CACHE_INFO.getCode()); } catch
			 * (Exception e) { log.error(e.getMessage());
			 * AjaxUtil.createReturnValueError(e.getMessage()); }
			 */
			return AjaxUtil.createReturnValueSuccess();
		} catch (Exception e) {
			return AjaxUtil.createReturnValueError(e.getMessage());
		}

	}

	/**
	 * 查找指定的充值配置
	 * 
	 * @return
	 */
	public Object[] getRechargeConfigById(Long id) {
		RechargeConfig rechargeConfig = rechargeConfigService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(rechargeConfig);
	}

	/**
	 * 根据关联方式类型查询相关关联支付类型
	 * 
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public Object[] getRefIdByRefType(String payType, String refType, String bizSystem) {
		List list = new ArrayList();
		PayBankVo query = new PayBankVo();
		query.setBizSystem(bizSystem);
		if (payType.equals("BANK_TRANSFER")) {
			query.setPayType("BANK_TRANSFER");
			list = payBankService.getAllPayBanks(query);
		} else if (payType.equals("QUICK_PAY")) {
			ChargePayVo chargePayVo = new ChargePayVo();
			chargePayVo.setBizSystem(bizSystem);
			list = chargePayService.getAllChargePays(chargePayVo);
		} else if (payType.equals("ALIPAY") || payType.equals("ALIPAYWAP")) {
			if (refType.equals("TRANSFER")) {
				query.setPayType("ALIPAY");
				list = payBankService.getAllPayBanks(query);
			} else if (refType.equals("THIRDPAY")) {
				ChargePayVo chargePayVo = new ChargePayVo();
				chargePayVo.setBizSystem(bizSystem);
				list = chargePayService.getAllChargePays(chargePayVo);
			}

		} else if (payType.equals("WEIXIN") || payType.equals("WEIXINWAP")) {
			if (refType.equals("TRANSFER")) {
				query.setPayType("WEIXIN");
				list = payBankService.getAllPayBanks(query);
			} else if (refType.equals("THIRDPAY")) {
				ChargePayVo chargePayVo = new ChargePayVo();
				chargePayVo.setBizSystem(bizSystem);
				list = chargePayService.getAllChargePays(chargePayVo);
			}

		} else if (payType.equals("QQPAY") || payType.equals("QQPAYWAP")) {
			if (refType.equals("TRANSFER")) {
				query.setPayType("QQPAY");
				list = payBankService.getAllPayBanks(query);
			} else if (refType.equals("THIRDPAY")) {
				ChargePayVo chargePayVo = new ChargePayVo();
				chargePayVo.setBizSystem(bizSystem);
				list = chargePayService.getAllChargePays(chargePayVo);
			}

		} else if (payType.equals("UNIONPAY")) {
			if (refType.equals("TRANSFER")) {
				query.setPayType("UNIONPAY");
				list = payBankService.getAllPayBanks(query);
			} else if (refType.equals("THIRDPAY")) {
				ChargePayVo chargePayVo = new ChargePayVo();
				chargePayVo.setBizSystem(bizSystem);
				list = chargePayService.getAllChargePays(chargePayVo);
			}

		} else if (payType.equals("JDPAY")) {
			if (refType.equals("TRANSFER")) {
				query.setPayType("JDPAY");
				list = payBankService.getAllPayBanks(query);
			} else if (refType.equals("THIRDPAY")) {
				ChargePayVo chargePayVo = new ChargePayVo();
				chargePayVo.setBizSystem(bizSystem);
				list = chargePayService.getAllChargePays(chargePayVo);
			}

		} else if (payType.equals("WEIXINBARCODE")) {
			if (refType.equals("TRANSFER")) {
				query.setPayType("JDPAY");
				list = payBankService.getAllPayBanks(query);
			} else if (refType.equals("THIRDPAY")) {
				ChargePayVo chargePayVo = new ChargePayVo();
				chargePayVo.setBizSystem(bizSystem);
				list = chargePayService.getAllChargePays(chargePayVo);
			}

		}
		return AjaxUtil.createReturnValueSuccess(list);
	}

	/**
	 * 查询
	 * 
	 * @return
	 */
	public Object[] getAllRechargeConfig(RechargeConfig query, Page page) {
		if (query == null || page == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		if (!ConstantUtil.SUPER_SYSTEM.equals(currentAdmin.getBizSystem())) {
			query.setBizSystem(currentAdmin.getBizSystem());
		}
		page = rechargeConfigService.getAllRechargeConfigQueryPage(query, page);
		return AjaxUtil.createReturnValueSuccess(page);
	}

	/**
	 * 后台校验输入的VIP集合
	 * 
	 * @param rechargeConfig
	 * @return
	 */
	public String checkUserVipLevels(RechargeConfig rechargeConfig) {

		// 获取到当前vip等级
		Integer ishowUserVipLevel = rechargeConfig.getShowUserVipLevel();
		// 将获取到的Integer转换为String
		String showUserVipLevel = Integer.toString(ishowUserVipLevel);

		// 获取到当前vip等级集合
		String showUserVipLevels = rechargeConfig.getShowUserVipLevels();
		// 如果是修改的话才去做这一步判断

		// 重复判断,先在结尾加上逗号
		String newshowUserVipLevels = showUserVipLevels + ",";
		// 判断元素是否重复
		String[] split2 = newshowUserVipLevels.split(",");

		// 如果是修改的话才执行这一步操作
		if (!rechargeConfig.getShowUserVipLevels().isEmpty()) {
			String dataToString = getVIPSString(split2);
			if (rechargeConfig.getId() != null && showUserVipLevels != null && ishowUserVipLevel != 0) {
				return "请将VIP等级设置为0";
			}
			if (!dataToString.equals(showUserVipLevels)) {
				return "检查VIP等级组格式是否正确,数字之间用逗号隔开";
			}
			// 检查格式是否正确(是否是正整数,并且不以0开头的两位数)
			Boolean checkNumType = checkNumType(split2);

			if (!checkNumType) {
				return "VIP组数据格式错误,只能是数字并且以逗号隔开";
			}
			// 通过set判断元素是否需重复
			HashSet<String> newSet = new HashSet<String>();

			// 判断VIP等级组中元素是否重复
			for (String string2 : split2) {
				boolean add = newSet.add(string2);
				if (!add) {
					return "添加VIP等级组重复";
				}
			}

			// 判断等级组是否设置,如果设置,就提示用户将VIP设置为0
			for (String string2 : split2) {
				if (string2 == showUserVipLevel) {
					return "请将VIP设置为0";
				}
			}

			// 判断当前VIP集合是否以逗号结尾
			String substring = showUserVipLevels.substring(showUserVipLevels.length() - 1);

			// 判断当前VIP等级集合中是包含单个VIP等级,如果包含就请用户将VIP等级设置为0;
			boolean contains = substring.contains(",");
			if (contains) {
				return "VIP等级组结尾不能有逗号";
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	/**
	 * 判断字符串是是否重复
	 * 
	 * @param str
	 * @return
	 */
	public boolean isUnique(String str) {
		if (str == null || str.isEmpty()) {
			return false;
		}
		char[] elements = str.toCharArray();
		for (char e : elements) {
			if (str.indexOf(e) != str.lastIndexOf(e)) {
				return true;
			}
		}
		return false;
	}

	// 拼接字符串
	public String getString(String[] split) {
		StringBuilder sb = new StringBuilder();
		for (String ele : split) {
			sb.append(ele);
		}
		return sb.toString();
	}

	/**
	 * 将字符串用逗号隔开
	 * 
	 * @param s
	 * @return
	 */
	public String dataToString(String s) {
		String str = "";
		for (int i = 0; i < s.length(); i++) {
			if (i == s.length() - 1) {
				str += s.substring(i, i + 1);
				break;
			} else {
				str += s.substring(i, i + 1);
				str += ",";
			}
		}
		return str;
	}

	/**
	 * 该方法用于判断字符串数组是否是由数字并且不以0开头的正整数;
	 * 
	 * @param arr
	 * @return
	 */
	private boolean checkNumType(String[] arr) {

		// 定义一个boolean初始值为true
		boolean b = true;
		for (int i = 0; i < arr.length; i++) {
			// 如果不是数字的话返回成false;
			if (!StringUtils.isNumeric(arr[i])) {
				return false;
			}
			// 如果为0开头长度为2的话也返回false;
			else if (arr[i].length() >= 2 && arr[i].startsWith("0")) {
				return false;
			}
		}
		return b;
	}

	// 将当前数组以逗号拼接
	private String getVIPSString(String[] arr) {
		String str = "";
		for (int i = 0; i < arr.length; i++) {
			str += arr[i] + ",";
		}

		return str.substring(0, str.length() - 1);
	}

}
