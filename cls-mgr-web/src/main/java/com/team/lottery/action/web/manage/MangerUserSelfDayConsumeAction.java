package com.team.lottery.action.web.manage;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.UserDayConsumeQuery;
import com.team.lottery.service.UserSelfDayConsumeService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.DateUtil;
import com.team.lottery.vo.UserSelfDayConsume;

@Controller("mangerUserSelfDayConsumeAction")
public class MangerUserSelfDayConsumeAction {
	
	@Autowired
	private UserSelfDayConsumeService userSelfDayConsumeService;
	
	public Object[] getUserSelfDayConsumePage(UserDayConsumeQuery query, Page page){
		if(query == null || page == null){
			return AjaxUtil.createReturnValueError("参数错误！");
		}
		
		Date startDateTime = DateUtil.getNowStartTimeByStart(query.getBelongDateStart());
		Date endDateTime = DateUtil.getNowStartTimeByEnd(query.getBelongDateEnd());
		query.setBelongDateStart(startDateTime);
		query.setBelongDateEnd(endDateTime);
		
		String bizSystem = BaseDwrUtil.getCurrentAdmin().getBizSystem();
		if(!bizSystem.equals(ConstantUtil.SUPER_SYSTEM)){
			query.setBizSystem(bizSystem);
		}
		Page userSelfDayConsumePage = userSelfDayConsumeService.getUserSelfDayConsumePage(query, page);
		UserSelfDayConsume totalUser = userSelfDayConsumeService.getUserSelfDayConsume(query);
		
		if(userSelfDayConsumePage == null){
			return AjaxUtil.createReturnValueSuccess("暂未查询到用户盈利信息");
		}
		return AjaxUtil.createReturnValueSuccess(userSelfDayConsumePage,totalUser);
	}
	
}
