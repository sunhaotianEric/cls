package com.team.lottery.action.web.manage;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.Page;
import com.team.lottery.service.AwardModelConfigService;
import com.team.lottery.service.QuotaControlService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.AwardModelConfig;
import com.team.lottery.vo.QuotaControl;

@Controller("managerQuotaControlAction")
public class ManagerQuotaControlAction {
	@Autowired
	private QuotaControlService quotaControlService;
	
	@Autowired
	private AwardModelConfigService awardModelConfigService;
    /**
     * 查找
     * @return
     */
    public Object[] getAllQuotaControl(QuotaControl query,Page page){
    	if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	if(!ConstantUtil.SUPER_SYSTEM.equals(currentAdmin.getBizSystem())){
    		query.setBizSystem(currentAdmin.getBizSystem());
		}
    	page=quotaControlService.getAllQuotaControlByQueryPage(query, page);
       return AjaxUtil.createReturnValueSuccess(page);
		
    }

    
    /**
     * 删除
     * @return
     */
    public Object[] delQuotaControlById(Long id){
    	
    	quotaControlService.deleteByPrimaryKey(id);
    	return AjaxUtil.createReturnValueSuccess();
    }
    
    /**
     * 新增和修改
     * @param systemConfig
     * @return
     */
    public Object[] saveOrUpdateQuotaControl(QuotaControl record)
    {
    	try{
    	if(record == null){
    		return AjaxUtil.createReturnValueError("参数传递错误");
    	}
    	if(record.getId()!=null&&!"".equals(record.getId()))
    	{
    		return this.updateQuotaControl(record);
    	}else{
    		return this.addQuotaControl(record);
    	}
    	}catch(Exception e){
        	return AjaxUtil.createReturnValueError("操作失败，请检查是否添加了重复模式导致失败的！");
    	}
    }
    
    /**
     * 新增
     * @param bizSystem
     * @return
     */
	public Object[] addQuotaControl(QuotaControl record) {
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		if(record == null){
    		return AjaxUtil.createReturnValueError("参数传递错误");
    	}
	 	if(!ConstantUtil.SUPER_SYSTEM.equals(currentAdmin.getBizSystem())){
	 		record.setBizSystem(currentAdmin.getBizSystem());
		}
		record.setOpenEqualLevelNum(record.getOpenLowerLevelNum());
		record.setCreateAdmin(currentAdmin.getUsername());
		record.setUpdateAdmin(currentAdmin.getUsername());
		record.setCreateTime(new Date());
		record.setUpdateTime(new Date());
		int result = quotaControlService.insertSelective(record);
		if(result>0){
		  return AjaxUtil.createReturnValueSuccess(); 
		}else{
		  return AjaxUtil.createReturnValueError();
		}
		
	
	}
	
    /**
     * 修改
     * @param bizSystem
     * @return
     */
	public Object[] updateQuotaControl(QuotaControl record) {
		if (record == null) {
			return AjaxUtil.createReturnValueError("参数传递错误");
		}
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		record.setCreateAdmin(currentAdmin.getUsername());
		record.setUpdateAdmin(currentAdmin.getUsername());
		record.setUpdateTime(new Date());
		int result = quotaControlService.updateByPrimaryKeySelective(record);
		if(result>0){
		  return AjaxUtil.createReturnValueSuccess(); 
		}else{
		  return AjaxUtil.createReturnValueError();
		}
	}
    
	  /**
     * 修改
     * @param id
     * @return
     */
	public Object[] getQuotaControlById(Long id) {
		if (id == null) {
			return AjaxUtil.createReturnValueError("参数传递错误");
		}
		QuotaControl quotaControl = quotaControlService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(quotaControl); 
		
	}
	
    /**
     * 根据uid查询可设置业务的最高最低模式
     * @return
     */
    public  Object[] getAwardModel(String bizSystem)
    {
    	AwardModelConfig awardModelConfig =null;
    	if(bizSystem == null&& "".equals(bizSystem)){
    		return AjaxUtil.createReturnValueError("参数传递错误");
    	}
   
		try {
			awardModelConfig= new AwardModelConfig();
			awardModelConfig.setBizSystem(bizSystem);
			List<AwardModelConfig>  list= awardModelConfigService.getAwardModelConfigByQuery(awardModelConfig);
			if(list.size()>0){
				awardModelConfig =	list.get(0);
			}
			
		} catch (Exception e) {
			AjaxUtil.createReturnValueError(e.getMessage());
		}

    	return AjaxUtil.createReturnValueSuccess(awardModelConfig);
    }
	
}
