package com.team.lottery.action.web.manage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.AdminLogQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.AdminLogService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.vo.Admin;

@Controller("managerAdminLogAction")
public class ManagerAdminLogAction {

	@Autowired
	private AdminLogService adminLogService;
	
    /**
     * 查找所有的登陆日志数据
     * @return
     */
    public Object[] getAllAdminLog(AdminLogQuery query,Page page){
    	if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
       	Admin currentUser = BaseDwrUtil.getCurrentAdmin();
      	String bizSystem = currentUser.getBizSystem();
    	if(query.getBizSystem() == null){
    		if(!"SUPER_SYSTEM".equals(bizSystem)){
    			query.setBizSystem(bizSystem);
    		}
    	}
    	page = adminLogService.getAllAdminLogsByQueryPage(query,page);
		return AjaxUtil.createReturnValueSuccess(page);
    }

    
    /**
     * 删除登陆日志
     * @return
     */
/*    public Object[] delLoginLog(Date from,Date to){
    	try{
    		String bizSystem=ConstantUtil.SUPER_SYSTEM;
    		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    		if(!ConstantUtil.SUPER_SYSTEM.equals(currentAdmin.getBizSystem()))
        	{
    			bizSystem=currentAdmin.getBizSystem();
        	}
    		loginLogService.delLoginLogsByDay(bizSystem,from, to);
    	}catch(Exception e){
        	return AjaxUtil.createReturnValueError(e.getMessage());
    	}
    	
    	return AjaxUtil.createReturnValueSuccess();
    }*/
    
}
