package com.team.lottery.action.web.manage;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.enums.EUserDailLiLevel;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.SalaryOrderQuery;
import com.team.lottery.service.DaySalaryOrderService;
import com.team.lottery.service.UserService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.DaySalaryOrder;
import com.team.lottery.vo.User;

@Controller("managerDaySaralyOrderAction")
public class ManagerDaySaralyOrderAction {
	
	private static Logger logger = LoggerFactory.getLogger(ManagerDaySaralyOrderAction.class);
	
	@Autowired
	private DaySalaryOrderService daySalaryOrderService;
	@Autowired
	private UserService userService;

	/**
	 * 查找所有的日工资订单信息
	 * 
	 * @return
	 */
	public Object[] selectSalaryOrderByUserList(
			SalaryOrderQuery salaryOrderQuery, Page page) {
		/* daySalaryConfigVo.setState(1); */
		if (salaryOrderQuery == null || page == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		Admin admin = BaseDwrUtil.getCurrentAdmin();
		// 特殊情况处理--逾期为发放

		if (!"SUPER_SYSTEM".equals(admin.getBizSystem())) {
			salaryOrderQuery.setBizSystem(admin.getBizSystem());
		}
		page = daySalaryOrderService.selectSalaryOrderByUserList(salaryOrderQuery, page);
		return AjaxUtil.createReturnValueSuccess(page);
	}
	
	/**
	 * 发工资
	 * 
	 * @return
	 */
	public Object[] releaseDaysalary(Long id) {
		try {
			if (id == null) {
				return AjaxUtil.createReturnValueError("参数传递错误.");
			}
			DaySalaryOrder daySalaryOrder = daySalaryOrderService.selectByPrimaryKey(id);
			User fromUser = userService.selectByPrimaryKey(daySalaryOrder.getFromUserId());
			boolean isForceRelease = false;
			//订单的发放者不为超级管理员时，为强制发放操作
			if(!EUserDailLiLevel.SUPERAGENCY.getCode().equals(fromUser.getDailiLevel())) {
				isForceRelease = true;
			}
			Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
			logger.info("当前管理员[{}}],发放了ID[{}],业务系统[{}],用户[{}],归属日期为[{}]的日工资，日工资金额为[{}]", currentAdmin.getUsername(),
					daySalaryOrder.getId(), daySalaryOrder.getBizSystem(), daySalaryOrder.getUserName(), daySalaryOrder.getBelongDateStr(), 
					daySalaryOrder.getMoney());
			daySalaryOrderService.releaseDaysalary(id, isForceRelease);
			return AjaxUtil.createReturnValueSuccess();
		} catch (Exception e) {
			return AjaxUtil.createReturnValueError(e.getMessage());
		}
	}
	/**
	 * 取相应的日工资订单
	 * 
	 * @return
	 */
	public Object[] getDaySaralyOrderById(Long id) {
		DaySalaryOrder daySalaryOrder = daySalaryOrderService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(daySalaryOrder);
	}
	
	/**
	 * 保存相应的订单记录
	 * 
	 * @return
	 */
	public Object[] saveOrUpdateDaySaralyOrder(DaySalaryOrder daySalaryOrder) {

		try{
    		Long id = daySalaryOrder.getId();
    		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
        	if(id==null){
        		logger.info("当前管理员[{}],插入了一条日工资记录", currentAdmin.getUsername());
        		daySalaryOrderService.insertSelective(daySalaryOrder);
        	}else{
        		logger.info("当前管理员[{}],更新了id为[{}]的日工资记录", currentAdmin.getUsername(), daySalaryOrder.getId());
        	    daySalaryOrder.setUpdateDate(new Date());
        		daySalaryOrderService.updateByPrimaryKeySelective(daySalaryOrder);
        	}
    		return AjaxUtil.createReturnValueSuccess();
    	}catch(Exception e){
    		return AjaxUtil.createReturnValueError(e.getMessage());
    	}
    	
	}
}
