package com.team.lottery.action.web.manage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.AdminOperateLogQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.AdminOperateLogService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;

@Controller("managerAdminOperateLogAction")
public class ManagerAdminOperateLogAction {

	@Autowired
	private AdminOperateLogService adminOperateLogService;
	
	 /**
     * 查找所有的系统数据
     * @return
     */
    public Object[] getAllAdminOperateLogs(AdminOperateLogQuery query,Page page){
    	if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	String currentSystem = BaseDwrUtil.getCurrentAdmin().getBizSystem();
    	if(!currentSystem.equals("SUPER_SYSTEM")){
    		query.setBizSystem(currentSystem);
    	}
    	page = adminOperateLogService.getAllAdminOperateLogByQueryPage(query,page);
		return AjaxUtil.createReturnValueSuccess(page);
    }
    
}
