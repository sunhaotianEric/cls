package com.team.lottery.action.web.manage;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.service.LhcIssueConfigService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.vo.LhcIssueConfig;

@Controller("managerLhcIssueConfigAction")
public class ManagerLhcIssueConfigAction {
	@Autowired
	private LhcIssueConfigService lhcIssueConfigService;
	
	
	/**
	 *  跟新六合期数配置
	 * @param lhcIssueConfig
	 * @return
	 */
	public Object[] updateLhcIssueConfig(LhcIssueConfig lhcIssueConfig){
		if(lhcIssueConfig == null){
    		return AjaxUtil.createReturnValueError("参数传递错误");
    	}
		Long id = lhcIssueConfig.getId();
		lhcIssueConfig.setUpdateTime(new Date());
		if(id == null){
			lhcIssueConfigService.insertSelective(lhcIssueConfig);
		}else{
			lhcIssueConfigService.updateByPrimaryKeySelective(lhcIssueConfig);
		}
		return AjaxUtil.createReturnValueSuccess();
	}
	
	
	/**
	 * 获取配置
	 * @return
	 */
	public Object[] getLhcIssueConfig(String lotteryName){
		LhcIssueConfig lhcIssueConfig = lhcIssueConfigService.getLhcIssueConfig(lotteryName);
		if(lhcIssueConfig == null){
			return AjaxUtil.createReturnValueError("暂无配置！");
		}
		
		return AjaxUtil.createReturnValueSuccess(lhcIssueConfig);
	}
	
}
