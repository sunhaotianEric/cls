package com.team.lottery.action.web.manage;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.enums.EAdminOperateLogModel;
import com.team.lottery.enums.ERedisChannel;
import com.team.lottery.enums.EUserDailLiLevel;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.UserQuery;
import com.team.lottery.service.AdminOperateLogService;
import com.team.lottery.service.AdminService;
import com.team.lottery.service.AwardModelConfigService;
import com.team.lottery.service.BizSystemBonusConfigService;
import com.team.lottery.service.BizSystemInfoService;
import com.team.lottery.service.BizSystemService;
import com.team.lottery.service.HelpService;
import com.team.lottery.service.HelpsService;
import com.team.lottery.service.HomePicService;
import com.team.lottery.service.LotteryControlConfiService;
import com.team.lottery.service.LotteryWinLhcService;
import com.team.lottery.service.LotteryWinXyebService;
import com.team.lottery.service.UserRegisterService;
import com.team.lottery.service.UserService;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.system.message.MessageQueueCenter;
import com.team.lottery.system.message.messagetype.BizSystemConfigMessage;
import com.team.lottery.system.message.messagetype.BizSystemInfoMessage;
import com.team.lottery.system.message.messagetype.BizSystemMessage;
import com.team.lottery.util.AdminOperateUtil;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.AwardModelUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.MD5PasswordUtil;
import com.team.lottery.util.UserNameUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.AwardModelConfig;
import com.team.lottery.vo.BizSystem;
import com.team.lottery.vo.BizSystemBonusConfig;
import com.team.lottery.vo.BizSystemConfig;
import com.team.lottery.vo.BizSystemInfo;
import com.team.lottery.vo.Help;
import com.team.lottery.vo.Helps;
import com.team.lottery.vo.HomePic;
import com.team.lottery.vo.LotteryControlConfig;
import com.team.lottery.vo.LotteryWinLhc;
import com.team.lottery.vo.LotteryWinXyeb;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserRegister;

@Controller("managerBizSystemAction")
public class ManagerBizSystemAction {
	private static Logger logger = LoggerFactory.getLogger(ManagerBizSystemAction.class);
	@Autowired
	private BizSystemService bizSystemService;

	@Autowired
	private BizSystemInfoService bizSystemInfoService;
	@Autowired
	private BizSystemBonusConfigService bizSystemBonusConfigService;
	@Autowired
	private UserService userService;

	@Autowired
	private AdminService adminService;

	@Autowired
	private AdminOperateLogService adminOperateLogService;

	@Autowired
	private HomePicService homePicService;
	
	@Autowired
	private HelpService helpervice;
	@Autowired
	private HelpsService helpsService;
	
	@Autowired
	private AwardModelConfigService awardModelConfigService;

	@Autowired
	private LotteryWinLhcService lotteryWinLhcService;
	@Autowired
	private LotteryWinXyebService lotteryWinXyebService;
	@Autowired
	private LotteryControlConfiService lotteryControlConfiService;
	@Autowired
	private UserRegisterService userRegisterService;

	/**
	 * 查找业务系统管理
	 * 
	 * @return
	 */
	public Object[] getAllBizSystem(BizSystem query, Page page) {
		if (query == null || page == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		page = bizSystemService.getAllBizSystemByQueryPage(query, page);

		List<BizSystem> list = bizSystemService.getALLBizSystemIncome(query);
		if (list.size() > 0) {
			return AjaxUtil.createReturnValueSuccess(page, list.get(0));
		} else {
			return AjaxUtil.createReturnValueSuccess(page, null);
		}

	}


	/**
	 * 新增和修改
	 * 
	 * @param
	 * @return
	 */
	public Object[] saveOrUpdateBizSysten(BizSystem bizSystems) {
		try {
			Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
			if (!ConstantUtil.SUPER_SYSTEM.equals(currentAdmin.getBizSystem())) {
				return AjaxUtil.createReturnValueError("sorry!,您没有权限操作！");
			}
			if (bizSystems == null) {
				return AjaxUtil.createReturnValueError("参数传递错误");
			}
			if (bizSystems.getId() != null) {
				return this.updateBizSystem(bizSystems);
			} else {
				return this.addBizSystem(bizSystems);
			}
		} catch (Exception e) {
			return AjaxUtil.createReturnValueError(e.getMessage());
		}
	}

	/**
	 * 新增
	 * 
	 * @param bizSystem
	 * @return
	 */
	public Object[] addBizSystem(BizSystem bizSystem) {
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		if (bizSystem == null) {
			return AjaxUtil.createReturnValueError("参数传递错误");
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.MONTH, 1);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		bizSystem.setLastRenewTime(null);
		bizSystem.setSecretCode(UUID.randomUUID().toString());
		bizSystem.setExpireTime(calendar.getTime());
		bizSystem.setTotalMaintainMoney(new BigDecimal(0));
		bizSystem.setTotalBonusMoney(new BigDecimal(0));
		bizSystem.setCreateTime(new Date());
		bizSystem.setUpdateTime(new Date());
		bizSystem.setUpdateAdmin(currentAdmin.getUsername());
		Date expireTime = bizSystem.getExpireTime();
		String expireTimeStr = sdf.format(expireTime);
		try {
			// 保存日志
			String operateDesc = AdminOperateUtil.constructMessage("baseSystem_add", currentAdmin.getUsername(),
					bizSystem.getBizSystemName(), expireTimeStr, bizSystem.getLowestAwardModel().toString(),
					bizSystem.getHighestAwardModel().toString(), bizSystem.getEnable() == 1 ? "启用" : "关闭");
			String operationIp = BaseDwrUtil.getRequestIp();
			adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(),
					currentAdmin.getUsername(), null, "", EAdminOperateLogModel.SYSTEM_CONFIG, operateDesc);
		} catch (Exception e) {
			e.printStackTrace();
		}
		int result = bizSystemService.saveBizSystem(bizSystem, currentAdmin);
		if (result > 0) {
			HomePic homePic = new HomePic();
			homePic.setBizSystem(bizSystem.getBizSystem());
			homePic.setIsShow(1);
			homePic.setHomePicUrl("http://" + SystemConfigConstant.picHost + "/images/index/defaultIndex.jpg");
			homePic.setPicType("PC");
			// 插入首页图片
			homePicService.insertSelective(homePic);
			homePic = new HomePic();
			homePic.setBizSystem(bizSystem.getBizSystem());
			homePic.setIsShow(1);
			homePic.setHomePicUrl("http://" + SystemConfigConstant.picHost + "/images/index/defaultMobileIndex.jpg");
			homePic.setPicType("MOBILE");
			// 插入首页图片
			homePicService.insertSelective(homePic);
			// 初始化六合彩数据
			LotteryWinLhc lhcQuery = new LotteryWinLhc();
			lhcQuery.setBizSystem(bizSystem.getBizSystem());
			lotteryWinLhcService.initLotteryLhcCode(lhcQuery);
			// 初始化幸运28数据
			LotteryWinXyeb xyebQuery = new LotteryWinXyeb();
			xyebQuery.setBizSystem(bizSystem.getBizSystem());
			lotteryWinXyebService.initLotteryXyebCode(xyebQuery);
			// 初始化系统自身彩种设置
			LotteryControlConfig lotteryControlConfig = new LotteryControlConfig();
			lotteryControlConfig.setBizSystem(bizSystem.getBizSystem());
			lotteryControlConfig.setEnabled(0);
			lotteryControlConfig.setJlffcProfitModel(0);
			lotteryControlConfig.setJyksProfitModel(0);
			lotteryControlConfig.setXylhcProfitModel(0);
			lotteryControlConfig.setSfsscProfitModel(0);
			lotteryControlConfig.setShfsscProfitModel(0);
			lotteryControlConfig.setWfsscProfitModel(0);
			lotteryControlConfig.setSfksProfitModel(0);
			lotteryControlConfig.setWfksProfitModel(0);
			lotteryControlConfig.setSfpk10ProfitModel(0);
			lotteryControlConfig.setWfpk10ProfitModel(0);
			lotteryControlConfig.setShfpk10ProfitModel(0);
			lotteryControlConfig.setJssscProfitModel(0);
			lotteryControlConfig.setBjsscProfitModel(0);
			lotteryControlConfig.setGdsscProfitModel(0);
			lotteryControlConfig.setScsscProfitModel(0);
			lotteryControlConfig.setShsscProfitModel(0);
			lotteryControlConfig.setSdsscProfitModel(0);
			lotteryControlConfig.setLcqsscProfitModel(0);
			lotteryControlConfig.setCreateTime(new Date());
			lotteryControlConfig.setUpdateTime(new Date());
			lotteryControlConfig.setUpdateAdmin(currentAdmin.getUsername());
			lotteryControlConfiService.insertSelective(lotteryControlConfig);
			// 把更新缓存操作，放置到消息队列中
			bizSystem = bizSystemService.getBizSystemByCondition(bizSystem);
			BizSystemMessage bizSystemMessage = new BizSystemMessage();
			bizSystemMessage.setBizSystem(bizSystem);
			bizSystemMessage.setOperateType("1");
			MessageQueueCenter.putRedisMessage(bizSystemMessage, ERedisChannel.CLSCACHE.name());
			// 插入成功后，前台系统用户添加一个业务系统超级管理员 和游客代理直属
			User user = new User();
			user.setUserName(UserNameUtil.getSuperUserNameByBizSystem(bizSystem.getBizSystem()));
			user.setBizSystem(bizSystem.getBizSystem());
			user = userService.getUserForUnique(user);
			if (user != null) {
				return AjaxUtil.createReturnValueError(bizSystem.getBizSystem() + "超级管理员已经有了不用在添加！");
			} else {
				user = new User();
				user.setUserName(UserNameUtil.getSuperUserNameByBizSystem(bizSystem.getBizSystem()));
				user.setBizSystem(bizSystem.getBizSystem());
				user.setBonusState(2);
				user.setSalaryState(1);
				user.setPassword(SystemConfigConstant.defaultAdminPassword);
				user.setSafePassword(MD5PasswordUtil.GetMD5Code(SystemConfigConstant.defaultAdminPassword));
				user.setRegfrom("");
				user.setBonusScale(bizSystem.getBonusScale());
				user.setSscRebate(bizSystem.getHighestAwardModel());
				// 根据时时彩的最高值比例设置其他值
				AwardModelUtil.setUserRebateInfo(user, bizSystem.getHighestAwardModel());
				user.setDailiLevel(EUserDailLiLevel.SUPERAGENCY.name());
				try {
					// 超级代理
					userService.saveUser(user);
					// 游客直属
					user.setRegfrom(user.getUserName() + "&");
					user.setUserName(ConstantUtil.GUEST_ADMIN);
					user.setDailiLevel(EUserDailLiLevel.DIRECTAGENCY.name());
					user.setPassword(SystemConfigConstant.defaultAdminPassword);
					user.setIsTourist(1);
					userService.saveUser(user);
					// 增加GUEST_ADMIN一个邀请码
					user = userService.getUserByName(bizSystem.getBizSystem(), user.getUserName());
					UserRegister userRegister = new UserRegister();
					StringBuffer registerSb = new StringBuffer();
					// 代理类型
					registerSb.append(EUserDailLiLevel.REGULARMEMBERS.name());
					userRegister.setDailiLevel(EUserDailLiLevel.REGULARMEMBERS.name());
					// 注册用户模式
					registerSb.append(ConstantUtil.REGISTER_SPLIT).append(user.getSscRebate());
					registerSb.append(ConstantUtil.REGISTER_SPLIT).append(user.getFfcRebate());
					registerSb.append(ConstantUtil.REGISTER_SPLIT).append(user.getSyxwRebate());
					registerSb.append(ConstantUtil.REGISTER_SPLIT).append(user.getKsRebate());
					registerSb.append(ConstantUtil.REGISTER_SPLIT).append(user.getPk10Rebate());
					registerSb.append(ConstantUtil.REGISTER_SPLIT).append(user.getDpcRebate());
					registerSb.append(ConstantUtil.REGISTER_SPLIT).append(user.getTbRebate());
					registerSb.append(ConstantUtil.REGISTER_SPLIT).append(user.getLhcRebate());
					registerSb.append(ConstantUtil.REGISTER_SPLIT).append(user.getKlsfRebate());
					userRegister.setSscrebate(user.getSscRebate());
					userRegister.setSyxwRebate(user.getSyxwRebate());
					userRegister.setKsRebate(user.getKsRebate());
					userRegister.setPk10Rebate(user.getPk10Rebate());
					userRegister.setDpcRebate(user.getDpcRebate());
					// 分红比例
					registerSb.append(ConstantUtil.REGISTER_SPLIT).append(ConstantUtil.REGISTER_NULL_REPLACE);
					// 赠送资金 目前为空
					registerSb.append(ConstantUtil.REGISTER_SPLIT).append(ConstantUtil.REGISTER_NULL_REPLACE);
					String code = ConstantUtil.GUEST_CODE;
					userRegister.setInvitationCode(code);
					userRegister.setLinkSets(registerSb.toString());
					userRegister.setUserId(user.getId());
					userRegister.setUserName(user.getUserName());
					userRegister.setBizSystem(user.getBizSystem());
					userRegisterService.insertSelective(userRegister);
				} catch (Exception e) {
					return AjaxUtil.createReturnValueError(e.getMessage());
				}
			}
			// 添加后台超级管理员
			Admin admin = new Admin();
			admin.setBizSystem(bizSystem.getBizSystem());
			admin.setUsername(UserNameUtil.getSuperUserNameByBizSystem(bizSystem.getBizSystem()));
			admin.setState(0);
			admin.setPassword(SystemConfigConstant.defaultAdminPassword);
			admin.setOperatePassword(SystemConfigConstant.defaultAdminPassword);
			List<Admin> admins = adminService.getAllAdminByUsername(admin);
			if (admins != null && admins.size() > 0) {
				return AjaxUtil.createReturnValueError(admin.getUsername() + "管理员已经存在");
			}
			adminService.insertSelective(admin);
			//插入帮助信息
			List<Help> helpBybizSystem = helpervice.getAllHelpsBybizSystem(ConstantUtil.SUPER_SYSTEM);
			for(Help h : helpBybizSystem){
				Help help = new Help();
				BeanUtils.copyProperties(h, help);
				help.setBizSystem(bizSystem.getBizSystem());
				help.setCreateDate(new Date());
				helpervice.insertSelective(help);
			}
			// 批量插入帮助信息
			List<Helps> helpsBybizSystem = helpsService.getHelpsByBizSystem(ConstantUtil.SUPER_SYSTEM);
			for (Helps help : helpsBybizSystem) {
				Helps helps = new Helps();
				BeanUtils.copyProperties(help, helps);
				helps.setBizSystem(bizSystem.getBizSystem());
				helps.setCreateTime(new Date());
				helps.setCreateAdmin(BaseDwrUtil.getCurrentAdmin().getUsername());
				helpsService.insertSelective(helps);
			}
			// 发送业务系统配置常量缓存通知
			BizSystemConfigMessage bizSystemConfigMessage = new BizSystemConfigMessage();
			BizSystemConfig bizSystemConfig = new BizSystemConfig();
			bizSystemConfig.setBizSystem(bizSystem.getBizSystem());
			bizSystemConfigMessage.setBizSystemConfig(bizSystemConfig);
			MessageQueueCenter.putRedisMessage(bizSystemConfigMessage, ERedisChannel.CLSCACHE.name());
			// 把更新缓存操作，放置到消息队列中
			BizSystemInfoMessage bizSystemInfoMessage = new BizSystemInfoMessage();
			BizSystemInfo bizSystemInfo = new BizSystemInfo();
			bizSystemInfo.setBizSystem(bizSystem.getBizSystem());
			bizSystemInfoMessage.setBizSystemInfo(bizSystemInfo);
			bizSystemInfoMessage.setOperateType("1");
			MessageQueueCenter.putRedisMessage(bizSystemInfoMessage, ERedisChannel.CLSCACHE.name());
			return AjaxUtil.createReturnValueSuccess();
		} else {
			return AjaxUtil.createReturnValueError("添加失败，请重新添加");
		}
	}

	/**
	 * 修改
	 * 
	 * @param bizSystem
	 * @return
	 */
	public Object[] updateBizSystem(BizSystem bizSystem) {
		if (bizSystem == null) {
			return AjaxUtil.createReturnValueError("参数传递错误");
		}
		BizSystem oldBizSystem = bizSystemService.selectByPrimaryKey(bizSystem.getId());
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		bizSystem.setUpdateTime(new Date());
		bizSystem.setUpdateAdmin(currentAdmin.getUsername());
		bizSystem.setExpireTime(oldBizSystem.getExpireTime());
		// 保存日志
		Date expireTime = bizSystem.getExpireTime();
		Date oldExpireTime = oldBizSystem.getExpireTime();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String expireTimeStr = sdf.format(expireTime);
		String oldExpireTimeStr = sdf.format(oldExpireTime);
		String operateDesc = AdminOperateUtil.constructMessage("baseSystem_edit", currentAdmin.getUsername(),
				oldBizSystem.getBizSystemName(), bizSystem.getBizSystemName(), oldExpireTimeStr, expireTimeStr,
				oldBizSystem.getLowestAwardModel().toString(), bizSystem.getLowestAwardModel().toString(),
				oldBizSystem.getHighestAwardModel().toString(), bizSystem.getHighestAwardModel().toString(),
				oldBizSystem.getEnable() == 1 ? "启用" : "关闭", bizSystem.getEnable() == 1 ? "启用" : "关闭");
		String operationIp = BaseDwrUtil.getRequestIp();
		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(),
				currentAdmin.getUsername(), null, "", EAdminOperateLogModel.SYSTEM_CONFIG, operateDesc);

		int result = bizSystemService.updateByPrimaryKeySelective(bizSystem);
		if (result > 0) {

			// 业务系统名称修改，同步刷新bizSystemInfo，同时刷新缓存！
			if (!oldBizSystem.getBizSystemName().equals(bizSystem.getBizSystemName())) {
				BizSystemInfo bizSystemInfo = new BizSystemInfo();
				bizSystemInfo.setBizSystem(bizSystem.getBizSystem());
				bizSystemInfo = bizSystemInfoService.getBizSystemInfoByCondition(bizSystemInfo);

				// 把更新缓存操作，放置到消息队列中
				BizSystemInfoMessage bizSystemInfoMessage = new BizSystemInfoMessage();
				bizSystemInfoMessage.setBizSystemInfo(bizSystemInfo);
				bizSystemInfoMessage.setOperateType("0");
				MessageQueueCenter.putRedisMessage(bizSystemInfoMessage, ERedisChannel.CLSCACHE.name());

			}
			// 把更新缓存操作，放置到消息队列中
			BizSystemMessage bizSystemMessage = new BizSystemMessage();
			bizSystemMessage.setBizSystem(bizSystem);
			bizSystemMessage.setOperateType("0");
			MessageQueueCenter.putRedisMessage(bizSystemMessage, ERedisChannel.CLSCACHE.name());
			if (oldBizSystem.getHighestAwardModel().compareTo(bizSystem.getHighestAwardModel()) != 0) {
				UserQuery userQuery = new UserQuery();
				userQuery.setBizSystem(bizSystem.getBizSystem());
				userQuery.setDailiLevel(EUserDailLiLevel.SUPERAGENCY.getCode());

				List<User> userList = userService.getUsersByCondition(userQuery);

				if (userList.size() > 0) {
					User user = userList.get(0);
					// user.setBonusScale(bizSystem.getBonusScale());
					user.setSscRebate(bizSystem.getHighestAwardModel());
					// 根据时时彩的最高值比例设置其他值
					AwardModelUtil.setUserRebateInfo(user, bizSystem.getHighestAwardModel());
					try {
						userService.updateByPrimaryKeyWithVersionSelective(user);
					} catch (UnEqualVersionException e) {
						return AjaxUtil.createReturnValueError(e.getMessage());
					}
				}
			}

			return AjaxUtil.createReturnValueSuccess();
		} else {
			return AjaxUtil.createReturnValueError("添加失败，请重新添加");
		}
	}

	/**
	 * 根据id获取对象
	 * 
	 * @param id
	 * @return
	 */
	public Object[] getBizSystemById(Long id) {
		if (id == null) {
			return AjaxUtil.createReturnValueError("参数传递错误");
		}

		BizSystem bizSystem = bizSystemService.selectByPrimaryKey(id);
		BizSystemBonusConfig bizSystemBonusConfig = new BizSystemBonusConfig();
		bizSystemBonusConfig.setBizSystem(bizSystem.getBizSystem());
		List<BizSystemBonusConfig> bizSystemBonusConfigList = bizSystemBonusConfigService
				.getBizSystemBonusConfigByCondition(bizSystemBonusConfig);
		return AjaxUtil.createReturnValueSuccess(bizSystem, bizSystemBonusConfigList);
	}

	/**
	 * 查找查询启用中的业务系统
	 * 
	 * @return
	 */
	public Object[] getAllEnableBizSystem() {
		List<BizSystem> bizSystemlist = null;
		// 从缓存上直接读取
		bizSystemlist = ClsCacheManager.getAllBizSystem();
		if (bizSystemlist.size() < 1) {
			logger.debug("缓存中没拿到，从数据库直接取了！");
			// 从数据库直接取
			bizSystemlist = bizSystemService.getAllEnableBizSystem();
		}
		return AjaxUtil.createReturnValueSuccess(bizSystemlist);
	}

	/**
	 * 根据bizSystemName查询的业务系统
	 * 
	 * @return
	 */
	public Object[] getBizSystem() {
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		BizSystem biz = new BizSystem();
		biz.setBizSystem(currentAdmin.getBizSystem());

		biz = bizSystemService.getBizSystemByCondition(biz);
		return AjaxUtil.createReturnValueSuccess(biz);
	}

	/**
	 * 根据uid查询可设置业务的最高最低模式
	 * 
	 * @return
	 */
	public Object[] getAwardModel(Integer userid) {
		AwardModelConfig awardModelConfig = null;
		if (userid != null) {
			User user = userService.selectByPrimaryKey(userid);
			try {
				awardModelConfig = getWithEditAwardModel(user);
			} catch (Exception e) {
				AjaxUtil.createReturnValueError(e.getMessage());
			}
		} else {// 后台添加直属会员用的
			String bizSystem = BaseDwrUtil.getCurrentAdmin().getBizSystem();
			awardModelConfig = ClsCacheManager.getAwardModelConfigByCode(bizSystem);
		}
		return AjaxUtil.createReturnValueSuccess(awardModelConfig);
	}

	/**
	 * 获取可编辑的模式
	 * 
	 * @return
	 * @throws Exception
	 */
	private AwardModelConfig getWithEditAwardModel(User user) throws Exception {
		AwardModelConfig awardModelConfig = new AwardModelConfig();
		awardModelConfig.setBizSystem(user.getBizSystem());
		List<AwardModelConfig> list = awardModelConfigService.getAwardModelConfigByQuery(awardModelConfig);
		if (list.size() < 1) {
			throw new Exception("没有相应系统模式奖金配置记录，请联系客服添加！");
		}
		awardModelConfig = list.get(0);
		// 如果是超级代理直接返回，最高，和它直接下级的最低，没有下级就给最低
		if (EUserDailLiLevel.SUPERAGENCY.name().equals(user.getDailiLevel())) {
			String downRegfrom = user.getUserName() + ConstantUtil.REG_FORM_SPLIT;
			// 返回是时时彩的模式而已
			Integer lowestRebate = userService.getMaxRebate(downRegfrom, user.getBizSystem(), 1);
			if (lowestRebate != null) {
				awardModelConfig.setSscLowestAwardModel(new BigDecimal("" + lowestRebate));
			}
			// 是会员时，最高为上级代理最高，下级可以到最低
		} else if (EUserDailLiLevel.REGULARMEMBERS.name().equals(user.getDailiLevel())) {
			String parentUserName = UserNameUtil.getUpLineUserName(user.getRegfrom());
			User parentUser = userService.getUserByName(user.getBizSystem(), parentUserName);
			awardModelConfig.setSscHighestAwardModel(parentUser.getSscRebate());
		} else {
			String parentUserName = UserNameUtil.getUpLineUserName(user.getRegfrom());
			User parentUser = userService.getUserByName(user.getBizSystem(), parentUserName);
			awardModelConfig.setSscHighestAwardModel(parentUser.getSscRebate());

			String downRegfrom = user.getRegfrom() + user.getUserName() + ConstantUtil.REG_FORM_SPLIT;
			// 返回是时时彩的模式而已
			Integer lowestRebate = userService.getMaxRebate(downRegfrom, user.getBizSystem(), 1);
			if (lowestRebate != null) {
				awardModelConfig.setSscLowestAwardModel(new BigDecimal("" + lowestRebate));
			}
		}

		return awardModelConfig;
	}

	/**
	 * 启用停用
	 * 
	 * @param
	 * @return
	 */
	public Object[] enableBizSystem(Long id, Integer flag) {
		if (id == null || flag == null) {
			return AjaxUtil.createReturnValueError("参数传递错误");
		}
		BizSystem oldBizSystem = bizSystemService.selectByPrimaryKey(id);
		BizSystem bizSystem = new BizSystem();
		bizSystem.setId(id);
		bizSystem.setEnable(flag);
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();

		// 保存日志
		String operateDesc = AdminOperateUtil.constructMessage("baseSystem_modifyState", currentAdmin.getUsername(),
				bizSystem.getEnable() == 1 ? "启用" : "停用", oldBizSystem.getBizSystem());
		String operationIp = BaseDwrUtil.getRequestIp();
		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(),
				currentAdmin.getUsername(), null, "", EAdminOperateLogModel.SYSTEM_CONFIG, operateDesc);

		int result = bizSystemService.updateByPrimaryKeySelective(bizSystem);
		if (result > 0) {
			oldBizSystem.setEnable(flag);
			// 把更新缓存操作，放置到消息队列中
			BizSystemMessage bizSystemMessage = new BizSystemMessage();
			bizSystemMessage.setBizSystem(oldBizSystem);
			bizSystemMessage.setOperateType("0");
			MessageQueueCenter.putRedisMessage(bizSystemMessage, ERedisChannel.CLSCACHE.name());

			return AjaxUtil.createReturnValueSuccess();
		} else {
			return AjaxUtil.createReturnValueError("添加失败，请重新添加");
		}
	}

	/**
	 * 重置秘钥
	 *
	 * @param id biz_system id flag
	 * @return
	 */
	public Object[] resetSecretCode(Long id) {
		if (id == null ) {
			return AjaxUtil.createReturnValueError("参数传递错误");
		}
		BizSystem oldBizSystem = bizSystemService.selectByPrimaryKey(id);
		BizSystem bizSystem = new BizSystem();
		bizSystem.setId(id);
		bizSystem.setSecretCode(UUID.randomUUID().toString());
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();

		// 保存日志
		String operateDesc = AdminOperateUtil.constructMessage("baseSystem_resetSecretCode", currentAdmin.getUsername(),
				bizSystem.getSecretCode(), oldBizSystem.getBizSystem());
		String operationIp = BaseDwrUtil.getRequestIp();
		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(),
				currentAdmin.getUsername(), null, "", EAdminOperateLogModel.SYSTEM_CONFIG, operateDesc);

		int result = bizSystemService.updateByPrimaryKeySelective(bizSystem);
		if (result > 0) {
			oldBizSystem.setSecretCode(bizSystem.getSecretCode());
			// 把更新缓存操作，放置到消息队列中
			BizSystemMessage bizSystemMessage = new BizSystemMessage();
			bizSystemMessage.setBizSystem(oldBizSystem);
			bizSystemMessage.setOperateType("0");
			MessageQueueCenter.putRedisMessage(bizSystemMessage, ERedisChannel.CLSCACHE.name());

			return AjaxUtil.createReturnValueSuccess();
		} else {
			return AjaxUtil.createReturnValueError("添加失败，请重新添加");
		}
	}

	/**
	 * 删除业务系统
	 * 
	 * @return
	 */
	@Transactional
	public Object[] delbizSystemByAllId(Long id, String password) {
		if (id == null || password == null || password == "") {
			return AjaxUtil.createReturnValueError("参数错误");
		}
		
		try {
			Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
			BizSystem bizSystem = bizSystemService.selectByPrimaryKey(id);
			if(bizSystem == null) {
				return AjaxUtil.createReturnValueError("参数错误");
			}
			// 保存日志
			String operateDesc = AdminOperateUtil.constructMessage("baseSystem_del", currentAdmin.getUsername(),
					bizSystem.getBizSystemName(), bizSystem.getLowestAwardModel().toString(),
					bizSystem.getHighestAwardModel().toString(), bizSystem.getBonusScale().toString());
			String operationIp = BaseDwrUtil.getRequestIp();
			adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(),
					currentAdmin.getUsername(), null, "", EAdminOperateLogModel.SYSTEM_CONFIG, operateDesc);
			
			if (currentAdmin.getBizSystem().equals(ConstantUtil.SUPER_SYSTEM)
					&& currentAdmin.getUsername().equals("ericSun")) {
				if ("e3D4f5".equals(password)) {
					
					bizSystemService.delBizSystemByAllBizSystem(bizSystem.getBizSystem());
					
					// 把更新缓存操作，放置到消息队列中
					BizSystemMessage bizSystemMessage = new BizSystemMessage();

					bizSystemMessage.setBizSystem(bizSystem);
					bizSystemMessage.setOperateType("-1");
					MessageQueueCenter.putRedisMessage(bizSystemMessage, ERedisChannel.CLSCACHE.name());
				} else {
					logger.info("当前管理员[{}]删除业务系统执行密码错误", currentAdmin.getUsername());
					return AjaxUtil.createReturnValueError("执行密码错误");
				}
			} else {
				logger.info("当前管理员[{}]删除业务系统权限错误", currentAdmin.getUsername());
				return AjaxUtil.createReturnValueError("您无权删除业务系统");
			}
			
			return AjaxUtil.createReturnValueSuccess("删除成功");
		} catch (Exception e) {
			logger.error("删除业务系统失败", e);
			return AjaxUtil.createReturnValueError("删除失败");
		}
	}
}
