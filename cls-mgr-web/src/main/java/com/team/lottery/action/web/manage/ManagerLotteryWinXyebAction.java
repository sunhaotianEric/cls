package com.team.lottery.action.web.manage;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;

import com.team.lottery.enums.EXYEBKind;
import com.team.lottery.extvo.LotteryWinXyebParam;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.LotteryWinXyebService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.LotteryWinXyeb;

@Controller("managerLotteryWinXyebAction")
public class ManagerLotteryWinXyebAction {
	
	@Autowired
	private LotteryWinXyebService lotteryWinXyebService;
	
	/**
	 * 
	 * @param query
	 * @param page
	 * @return
	 */
	public Object[] getLotteryWinXyebByPage(LotteryWinXyeb query,Page page){
		if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
     	Admin currentUser = BaseDwrUtil.getCurrentAdmin();
      	String bizSystem = currentUser.getBizSystem();
    	if(query.getBizSystem() == null){
    		if(!"SUPER_SYSTEM".equals(bizSystem)){
    			query.setBizSystem(bizSystem);
    		}
    	}
    	Page winXyebByPage = lotteryWinXyebService.getLotteryWinXyebByPage(query, page);
    	
    	return AjaxUtil.createReturnValueSuccess(winXyebByPage);
	}
	
	/**
	 * 
	 * @param query
	 * @param page
	 * @return
	 */
	public Object[] getLotteryWinXyebByQueryCondition(LotteryWinXyeb query){
		if(query == null  ){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
     	Admin currentUser = BaseDwrUtil.getCurrentAdmin();
      	String bizSystem = currentUser.getBizSystem();
    	if(query.getBizSystem() == null){
    		if(!"SUPER_SYSTEM".equals(bizSystem)){
    			query.setBizSystem(bizSystem);
    		}
    	}
    	query.setLotteryTypeProtype(EXYEBKind.TM.getCode());
    	List<LotteryWinXyeb>  tmlist= lotteryWinXyebService.getLotteryWinXyebByKind(query);
    	query.setLotteryTypeProtype(EXYEBKind.TMSB.getCode());
    	List<LotteryWinXyeb>  tmsblist= lotteryWinXyebService.getLotteryWinXyebByKind(query);
    	query.setLotteryTypeProtype(EXYEBKind.HH.getCode());
    	List<LotteryWinXyeb>  hhlist= lotteryWinXyebService.getLotteryWinXyebByKind(query);
    	query.setLotteryTypeProtype(EXYEBKind.BS.getCode());
    	List<LotteryWinXyeb>  bslist= lotteryWinXyebService.getLotteryWinXyebByKind(query);
    	query.setLotteryTypeProtype(EXYEBKind.BZ.getCode());
    	List<LotteryWinXyeb>  bzlist= lotteryWinXyebService.getLotteryWinXyebByKind(query);
    	return AjaxUtil.createReturnValueSuccess(tmlist,tmsblist,hhlist,bslist,bzlist);
	}
	

	/**
	 * 
	 * @param lotteryWinXyeb
	 * @return
	 */
	public Object[] updateLotteryWinXyebByCodes(LotteryWinXyebParam lotteryWinXyebParam){
	
		try {
			if(lotteryWinXyebParam == null || lotteryWinXyebParam.getLotteryWinXyebArr() == null ){
	    		return AjaxUtil.createReturnValueError("参数传递错误.");
	    	}
			lotteryWinXyebService.bathUpdateByPrimaryKey(lotteryWinXyebParam.getLotteryWinXyebArr());		
			return AjaxUtil.createReturnValueSuccess();
		} catch (Exception e) {
			return AjaxUtil.createReturnValueError(e.getMessage());
		}
	}	
	
	
	
	
	/**
	 * 
	 * @param lotteryWinXyeb
	 * @return
	 */
	public Object[] updateLotteryWinXyeb(LotteryWinXyeb lotteryWinXyeb){
		if(lotteryWinXyeb == null){
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		try {
			Integer id = lotteryWinXyeb.getId();
			String bizSystem = BaseDwrUtil.getCurrentAdmin().getBizSystem();
			if(!"SUPER_SYSTEM".equals(bizSystem)){
				lotteryWinXyeb.setBizSystem(bizSystem);
			}
			
			List<LotteryWinXyeb> lotteryWinXyebList = lotteryWinXyebService.getLotteryWinXyebByLotteryTypeProtype(lotteryWinXyeb);
			Integer order = 0;
			if(!CollectionUtils.isEmpty(lotteryWinXyebList)){
				Iterator<LotteryWinXyeb> iterator = lotteryWinXyebList.iterator();
				while(iterator.hasNext()){
					 LotteryWinXyeb nextLotteryWinXyeb = iterator.next();
					if(order < nextLotteryWinXyeb.getOrders()){
						order = nextLotteryWinXyeb.getOrders();
					}
				}
				//orders只更新/插入数据库里不存在的
				if(id==null){
					lotteryWinXyeb.setOrders(order + 1);
				}
			}
			if(id==null){
				lotteryWinXyeb.setOrders(order + 1);
				lotteryWinXyebService.insertSelective(lotteryWinXyeb);
			}else{
				lotteryWinXyebService.updateByPrimaryKeySelective(lotteryWinXyeb);
			}
			
			return AjaxUtil.createReturnValueSuccess();
		} catch (Exception e) {
			return AjaxUtil.createReturnValueError(e.getMessage());
		}
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public Object[] getLotteryWinXyebById(Integer id){
		if(id == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
		LotteryWinXyeb selectByPrimaryKey = lotteryWinXyebService.selectByPrimaryKey(id);
		
		return AjaxUtil.createReturnValueSuccess(selectByPrimaryKey);
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public Object[] deleteLotteryWinXyebById(Integer id){
		if(id == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
		lotteryWinXyebService.deleteByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess();
		
	}
	
	
}
