package com.team.lottery.action.web.manage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.directwebremoting.io.FileTransfer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.enums.ERedisChannel;
import com.team.lottery.extvo.ImageUploadResult;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.BizSystemInfoService;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.system.message.MessageQueueCenter;
import com.team.lottery.system.message.messagetype.BizSystemInfoMessage;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.HttpClientUtil;
import com.team.lottery.util.JSONUtils;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.BizSystemInfo;

@Controller("managerBizSystemInfoAction")
public class ManagerBizSystemInfoAction {

	private static Logger logger = LoggerFactory.getLogger(ManagerBizSystemInfoAction.class);
	@Autowired
	private BizSystemInfoService bizSystemInfoService;
	
    /**
     * 查询
     * @return
     */
    public Object[] getAllBizSystemInfo(BizSystemInfo query,Page page){
    	if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	if(!(ConstantUtil.SUPER_SYSTEM).equals(currentAdmin.getBizSystem()))
    	{
    		query.setBizSystem(currentAdmin.getBizSystem());
    	}
    	page=bizSystemInfoService.getAllBizSystemInfoByQueryPage(query, page);
		return AjaxUtil.createReturnValueSuccess(page);
    }

    
    /**
     * 根据登录用户是超级管理员获取对象
     * @param id
     * @return
     */
    public Object[] getBizSystemInfoByLoginUser() {
    	
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	if(!(ConstantUtil.SUPER_SYSTEM).equals(currentAdmin.getBizSystem())){
    		return AjaxUtil.createReturnValueError("sorry！不是超级管理员，不能查看该界面！");
    	}
    	BizSystemInfo query =new BizSystemInfo();
    	query.setBizSystem(currentAdmin.getBizSystem());
    	BizSystemInfo bizSystemInfo=bizSystemInfoService.getBizSystemInfoByCondition(query);
		return AjaxUtil.createReturnValueSuccess(bizSystemInfo);
    }
    
    
    /**
     * 删除
     * @return
     */
	public Object[] delBizSystemInfoById(Long id) {
		try {
			int result = bizSystemInfoService.deleteByPrimaryKey(id);
			if (result <= 0) {
				// 把更新缓存操作，放置到消息队列中
				BizSystemInfoMessage bizSystemInfoMessage = new BizSystemInfoMessage();
				bizSystemInfoMessage.setBizSystemInfo(null);
				bizSystemInfoMessage.setOperateType("-1");
				MessageQueueCenter.putRedisMessage(bizSystemInfoMessage, ERedisChannel.CLSCACHE.name());

				return AjaxUtil.createReturnValueError("删除失败！");
			}
		} catch (Exception e) {
			return AjaxUtil.createReturnValueError(e.getMessage());
		}

		return AjaxUtil.createReturnValueSuccess();
	}
    
    /**
     * 新增和修改
     * @param systemConfig
     * @return
     */
    public Object[] saveOrUpdateBizSystenInfo(BizSystemInfo bizSystemInfo) {
    	try{
	    	if(bizSystemInfo == null){
	    		return AjaxUtil.createReturnValueError("参数传递错误");
	    	}
	    	if(bizSystemInfo.getId() != null) {
	    		return this.updateBizSystemInfo(bizSystemInfo);
	    	}else{
	    		return this.addBizSystemInfo(bizSystemInfo);
	    	}
    	}catch(Exception e){
        	return AjaxUtil.createReturnValueError(e.getMessage());
    	}
    }
    
    /**
     * 新增
     * @param bizSystem
     * @return
     */
    public Object[] addBizSystemInfo(BizSystemInfo bizSystemInfo) {
    	if(bizSystemInfo == null){
    		return AjaxUtil.createReturnValueError("参数传递错误");
    	}
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	bizSystemInfo.setCreateTime(new Date());
    	bizSystemInfo.setUpdateTime(new Date());
    	bizSystemInfo.setUpdateAdmin(currentAdmin.getUsername());;
        int result=bizSystemInfoService.insertSelective(bizSystemInfo);
        if(result>0){
            //把更新缓存操作，放置到消息队列中
	        BizSystemInfoMessage bizSystemInfoMessage=new BizSystemInfoMessage();
	      
	        bizSystemInfoMessage.setBizSystemInfo(bizSystemInfo);;
	        bizSystemInfoMessage.setOperateType("1");
	        MessageQueueCenter.putRedisMessage(bizSystemInfoMessage, ERedisChannel.CLSCACHE.name());
	        
          return AjaxUtil.createReturnValueSuccess();
        }else{
          return AjaxUtil.createReturnValueError("添加失败，请重新添加");
        }
    }
    
    /**
     * 修改
     * @param bizSystem
     * @return
     */
    public Object[] updateBizSystemInfo(BizSystemInfo bizSystemInfo) {
		if (bizSystemInfo == null) {
			return AjaxUtil.createReturnValueError("参数传递错误");
		}
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		bizSystemInfo.setUpdateTime(new Date());
		bizSystemInfo.setUpdateAdmin(currentAdmin.getUsername());
		int result = 0;
		try {
			result = bizSystemInfoService.updateByPrimaryKeySelective(bizSystemInfo);
		} catch (Exception e) {
			logger.error("更新异常", e);
		}
		if (result > 0) {
			// 把更新缓存操作，放置到消息队列中
			BizSystemInfoMessage bizSystemInfoMessage = new BizSystemInfoMessage();
			bizSystemInfoMessage.setBizSystemInfo(bizSystemInfo);
			bizSystemInfoMessage.setOperateType("0");
			MessageQueueCenter.putRedisMessage(bizSystemInfoMessage, ERedisChannel.CLSCACHE.name());
			return AjaxUtil.createReturnValueSuccess();
		} else {
			return AjaxUtil.createReturnValueError("添加失败，请重新添加");
		}
	}
    
    /**
     * 根据id获取对象
     * @param id
     * @return
     */
	public Object[] getBizSystemInfoById(Long id) {
		BizSystemInfo bizSystemInfo = new BizSystemInfo();
		// 判断权限显示界面
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		if (id != null && !currentAdmin.getBizSystem().equals(ConstantUtil.SUPER_SYSTEM)) {
			if (currentAdmin.getState() == 0) {
				bizSystemInfo.setBizSystem(currentAdmin.getBizSystem());
				bizSystemInfo = bizSystemInfoService.getBizSystemInfoByCondition(bizSystemInfo);
				return AjaxUtil.createReturnValueSuccess(bizSystemInfo);
			}
		}
		bizSystemInfo = bizSystemInfoService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(bizSystemInfo, SystemConfigConstant.imgServerUrl);
	}
    
    /**
     * 获取彩种类型
     * @return
     */
    public Object[] getLotteryTypes(){
    	ELotteryKind[] lotteryTypes = ELotteryKind.values();
    	Map<String,String> lotteryMaps = new HashMap<String,String>();
    	for(ELotteryKind type : lotteryTypes){
    		if(type.getIsShow()==1){
    		lotteryMaps.put(type.getCode(), type.getDescription());
    		}
    	}
	
		return AjaxUtil.createReturnValueSuccess(lotteryMaps);
    }
    
    /**
   	 * 文件上传
   	 * 
   	 * @return
   	 */
   	public Object[] uploadImgFile(FileTransfer fileTransfer, String dirName, String newFileName, String bizSystem) {
   		try {
   			Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
   	    	if(!currentAdmin.getBizSystem().equals(ConstantUtil.SUPER_SYSTEM)){
   	    		bizSystem = currentAdmin.getBizSystem();
   	    	}
   	    	
   			//判断上传文件的类型
   			String uploadFileName = fileTransfer.getFilename();
   			int index = uploadFileName.indexOf(".");
   			String fileType = "";
   			if(index > 0) {
   				fileType = uploadFileName.substring(index);
   				if(!fileType.equalsIgnoreCase(".jpg") && !fileType.equalsIgnoreCase(".png") && !fileType.equalsIgnoreCase(".ico")) {
   					return AjaxUtil.createReturnValueError("上传图片文件类型错误，请重新选择");
   				}
   			}
   			if(StringUtils.isEmpty(fileType)) {
   				return AjaxUtil.createReturnValueError("上传图片文件类型错误，请重新选择");
   			}
   			
   			//生成文件名称  根据日期
   			String newFileNametime = DateUtil.getDate(new Date(), "yyyyMMddHHmmssSS");
//   			System.out.println("2222221:" + newFileNametime);
   			newFileName = newFileNametime + newFileName + fileType;
   			
   			WebContext webContext = WebContextFactory.get();
   			// String realtivepath = webContext.getServletContext().getContextPath()
   			// + "/upload/";
   			String saveurl = webContext.getHttpServletRequest().getSession()
   					.getServletContext().getRealPath("/upload");
   			File fileDir = new File(saveurl);
   			if (!fileDir.exists()) {
   				fileDir.mkdirs();
   			}
   			String saveFileUrl = saveurl + "/" + newFileName;
   			File file = new File(saveFileUrl);
   			InputStream uploadFile = fileTransfer.getInputStream(); 
   			int available = uploadFile.available();
   			byte[] b = new byte[available];
   			FileOutputStream foutput = new FileOutputStream(file);
   			uploadFile.read(b);
   			foutput.write(b);
   			foutput.flush();
   			foutput.close();
   			uploadFile.close();
   			
   			logger.info("接收网站内容图片上传到本地成功，本地图片路径地址[{}],开始上传到图片服务器...", saveFileUrl);
			
			//使用http接口上传图片
   			String picSavePath = bizSystem + "/" + dirName;
			String fullSavePath = picSavePath + "/" + newFileName;
			Map<String, String> params = new HashMap<String, String>();
			params.put("path", picSavePath);
			params.put("fileName", newFileName);
			String uploadResult = HttpClientUtil.doPostFileUpload(SystemConfigConstant.imgServerUploadUrl+ConstantUtil.FILE_UPLOAD_RESOURCE, file, params);
			logger.info("上传图片结果：{}", uploadResult);
			
			ImageUploadResult result = JSONUtils.toBean(uploadResult, ImageUploadResult.class);
			if(result == null) {
				return AjaxUtil.createReturnValueError("文件上传图片服务器错误，原因:返回结果为空");
			} 
			if("error".equals(result.getResult())) {
				String mString = result.getMsg();
				return AjaxUtil.createReturnValueError("文件上传图片服务器错误，原因:" + mString);
			}
   			
   			//资源服务器上图片访问url地址
//   			String fileUrl = "";
//   			try{
//   				//上传文件至ftp
//   				fileUrl = SftpClientUtil.uploadFileToSFTP(saveFileUrl, dirName, newFileName);
//   			} catch (Exception e) {
//   				logger.error("上传文件至ftp出错", e);
//   				return AjaxUtil.createReturnValueError("上传文件至图片服务器失败");
//   			}
			logger.info("上传网站内容图片到图片服务器成功，删除本地图片文件");
   			//删除文件
   			file.delete();
   			
   			return AjaxUtil.createReturnValueSuccess(fullSavePath, SystemConfigConstant.imgServerUrl);
   		} catch(Exception e){
   			logger.error("文件上传失败", e);
   			return AjaxUtil.createReturnValueError("文件上传失败");
   		}
   		
   	}
   	
  
}
