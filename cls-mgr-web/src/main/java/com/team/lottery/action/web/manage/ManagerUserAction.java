package com.team.lottery.action.web.manage;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.directwebremoting.io.FileTransfer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.activemq.UserKickOutSender;
import com.team.lottery.enums.EAdminOperateLogModel;
import com.team.lottery.enums.EAdminPasswordErrorLog;
import com.team.lottery.enums.EAdminPasswordErrorType;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.enums.EUserDailLiLevel;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.AdminPasswordErrorLogQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.TeamUserQuery;
import com.team.lottery.extvo.UserBankQueryVo;
import com.team.lottery.extvo.UserInfoExportVO;
import com.team.lottery.extvo.UserKickOutMessageVo;
import com.team.lottery.extvo.UserQuery;
import com.team.lottery.service.AdminOperateLogService;
import com.team.lottery.service.AdminPasswordErrorLogService;
import com.team.lottery.service.AdminService;
import com.team.lottery.service.BizSystemService;
import com.team.lottery.service.LevelSytemService;
import com.team.lottery.service.PasswordErrorLogService;
import com.team.lottery.service.UserBankService;
import com.team.lottery.service.UserMoneyTotalService;
import com.team.lottery.service.UserQuotaService;
import com.team.lottery.service.UserRegisterService;
import com.team.lottery.service.UserService;
import com.team.lottery.system.SingleLoginForRedis;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.system.message.MessageQueueCenter;
import com.team.lottery.system.message.messagetype.BankCardMessage;
import com.team.lottery.system.message.messagetype.SendMailMessage;
import com.team.lottery.util.AdminOperateUtil;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.AwardModelUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.ExcelUtil;
import com.team.lottery.util.ExcelUtil.ExcelExportData;
import com.team.lottery.util.ImportExcelUtil;
import com.team.lottery.util.MD5PasswordUtil;
import com.team.lottery.util.RegxUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.util.UserNameUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.AdminPasswordErrorLog;
import com.team.lottery.vo.BizSystem;
import com.team.lottery.vo.LevelSystem;
import com.team.lottery.vo.Login;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserBank;
import com.team.lottery.vo.UserMoneyTotal;
import com.team.lottery.vo.UserRegister;

@Controller("managerUserAction")
public class ManagerUserAction {

    @Autowired
    private UserService userService;
    @Autowired
    private AdminService adminService;
    @Autowired
    private UserBankService userBankService;
    @Autowired
    private AdminOperateLogService adminOperateLogService;
    @Autowired
    private UserQuotaService userQuotaService;
    @Autowired
    private UserMoneyTotalService userMoneyTotalService;
    @Autowired
    private BizSystemService bizSystemService;
    @Autowired
    private UserRegisterService userRegisterService;
    @Autowired
    private LevelSytemService levelSytemService;
    @Autowired
    private AdminPasswordErrorLogService adminPasswordErrorLogService;
    @Autowired
    private PasswordErrorLogService passwordErrorLogService;
    @Autowired
    private UserKickOutSender userKickOutSender;


    private static Logger log = LoggerFactory.getLogger(ManagerUserAction.class);

    /**
     * 添加直属用户
     *
     * @return
     */
    public Object[] addUser(User user) {
        if (user == null) {
            return AjaxUtil.createReturnValueError("user参数传递错误.");
        } else if (StringUtils.isEmpty(user.getUserName())) {
            return AjaxUtil.createReturnValueError("用户名不能为空.");
        } else if (StringUtils.isEmpty(user.getPassword())) {
            return AjaxUtil.createReturnValueError("用户密码不能为空.");
        }

        if (!Pattern.compile(ConstantUtil.USER_NAME_CHAR_FILTER).matcher(user.getUserName()).matches()) {
            return AjaxUtil.createReturnValueError("用户名只能是数字和字母.");
        }
        if (user.getUserName().length() < 4 || user.getUserName().length() > 16) {
            return AjaxUtil.createReturnValueError("用户名长度不正确.");
        }
        if (!Character.isLetter(user.getUserName().charAt(0))) {
            return AjaxUtil.createReturnValueError("用户名首位必须为字母.");
        }
        if (user.getPassword().length() < 6 || user.getPassword().length() > 15) {
            return AjaxUtil.createReturnValueError("密码长度不正确.");
        }
        if (user.getUserName().contains(ConstantUtil.REG_FORM_SPLIT)) {
            return AjaxUtil.createReturnValueError("用户名字符不合法.");
        }
        Admin admin = BaseDwrUtil.getCurrentAdmin();
        if ("SUPER_SYSTEM".equals(admin.getBizSystem())) {
            return AjaxUtil.createReturnValueError("你是SUPER_SYSTEM人员，您没有权限添加直属，请用对应的系统管理员账号.");
        }
        user.setBizSystem(admin.getBizSystem());

        User nameUser = new User(user.getUserName());
        nameUser.setBizSystem(admin.getBizSystem());
        nameUser = userService.getUserForUnique(nameUser);
        if (nameUser != null) {
            return AjaxUtil.createReturnValueError("该用户名已经被使用了,请修改.");
        }

        AwardModelUtil.setUserRebateInfo(user, user.getSscRebate());

        // 奖金模式最大值校验
        BizSystem bizSystem = new BizSystem();
        bizSystem.setBizSystem(admin.getBizSystem());
        bizSystem = bizSystemService.getBizSystemByCondition(bizSystem);
        if (user.getSscRebate().compareTo(bizSystem.getLowestAwardModel()) < 0
                || user.getSscRebate().compareTo(bizSystem.getHighestAwardModel()) > 0) {
            return AjaxUtil.createReturnValueError("奖金模式超过系统的极限设置！");
        }

        // 验证代理参数
        if (EUserDailLiLevel.valueOf(user.getDailiLevel()) == null) {
            return AjaxUtil.createReturnValueError("代理级别的参数传递错误.");
        }

//		if (admin.getState() != Admin.SUPER_STATE) {
//			return AjaxUtil.createReturnValueError("您没有权限添加直属，请用对应的管理员账号.");
//		}
        // 得到前台超级代理的用户
        UserQuery userQuery = new UserQuery();
        userQuery.setBizSystem(admin.getBizSystem());
        userQuery.setDailiLevel(EUserDailLiLevel.SUPERAGENCY.getCode());
        ;
        List<User> superagenceUserList = userService.getUsersByCondition(userQuery);
        if (superagenceUserList.size() < 1) {
            return AjaxUtil.createReturnValueError("没有对应的前台超级代理用户！请联系管理人员添加！");
        }
        User superagenceUser = superagenceUserList.get(0);
        user.setRegfrom(superagenceUser.getUserName() + ConstantUtil.REG_FORM_SPLIT); // 设置推荐人
        try {
            userService.saveUser(user);
        } catch (Exception e1) {
            return AjaxUtil.createReturnValueError(e1.getMessage());
        }

        // 操作日志保存
        String recordKey = "addUser_log";
        Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
        String sscrebete = user.getSscRebate() != null ? user.getSscRebate().toString() : "";
        String ffcrebate = user.getFfcRebate() != null ? user.getFfcRebate().toString() : "";
        String syxwrebate = user.getSyxwRebate() != null ? user.getSyxwRebate().toString() : "";
        String klsfrebate = user.getKlsfRebate() != null ? user.getKlsfRebate().toString() : "";
        String dpcrebate = user.getDpcRebate() != null ? user.getDpcRebate().toString() : "";
        String operateDesc = AdminOperateUtil.constructMessage(recordKey, currentAdmin.getUsername(), user.getUserName(),
                user.getDailiLevelStr(), user.getQq(), user.getPhone(), sscrebete, ffcrebate, syxwrebate, klsfrebate, dpcrebate);
        String operationIp = BaseDwrUtil.getRequestIp();
        adminOperateLogService.saveAdminOperateLog(admin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(),
                user.getId(), user.getUserName(), EAdminOperateLogModel.USER, operateDesc);

        return AjaxUtil.createReturnValueSuccess();
    }

    /**
     * 分页查询用户列表
     *
     * @param user
     * @return
     */
    @SuppressWarnings("unchecked")
    public Object[] userList(UserQuery query, Page page) {
        log.info("打印用户名:" + query.getUsername());
        Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
        if (query == null || page == null) {
            return AjaxUtil.createReturnValueError("参数传递错误.");
        }
        // 非SUPER_SYSTEM,只能查询本业务系统的用户
        if (!"SUPER_SYSTEM".equals(currentAdmin.getBizSystem())) {
            query.setBizSystem(currentAdmin.getBizSystem());
        }
        page = userService.getAllUsersByQueryPage(query, page);
        List<User> users = (List<User>) page.getPageContent();
        // 设置在线会员状态
        SingleLoginForRedis.setUsersOnlineStatus(users);

        return AjaxUtil.createReturnValueSuccess(page);
    }

    /**
     * 分页查询用户列表
     *
     * @param user
     * @return
     */
    public Object[] userFirstRechargeList(UserQuery query, Page page) {
        Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
        if (query == null || page == null) {
            return AjaxUtil.createReturnValueError("参数传递错误.");
        }

        // 非SUPER_SYSTEM,只能查询本业务系统的用户
        if (!"SUPER_SYSTEM".equals(currentAdmin.getBizSystem())) {
            query.setBizSystem(currentAdmin.getBizSystem());
        }
        page = userService.getAllUsersByQueryfirstRechargeTimePage(query, page);

        return AjaxUtil.createReturnValueSuccess(page);
    }

    /**
     * 获取指定用户
     *
     * @param userId
     * @return
     */
    public Object[] getUser(Integer userId, String userName) {

        if (userId == null && StringUtils.isEmpty(userName)) {
            return AjaxUtil.createReturnValueError("参数传递错误.");
        }
        User user = null;
        if (userId != null) {
            user = userService.selectByPrimaryKey(userId);
        }
        // 直接传ID,不走从用户名获取用户
        // if(!StringUtils.isEmpty(userName)) {
        // user = userService.getUserByName(bizSystem,userName);
        // }

        if (user == null) {
            return AjaxUtil.createReturnValueError("该用户不存在");
        }

        User higherLevelUser = null;
        String[] regFormUsers = user.getRegfrom().split(ConstantUtil.REG_FORM_SPLIT);
        // 超级代理以下才有的
        if (regFormUsers.length > 1 && user.getRegfrom() != null && !"".equals(user.getRegfrom())) {

            String higherLevelUserName = regFormUsers[0];
            higherLevelUser = userService.getUserByName(user.getBizSystem(), higherLevelUserName);
            if (higherLevelUser != null) {
                higherLevelUser.setPassword(null);
                higherLevelUser.setSafePassword(null);
            }
        }

        user.setPassword(null);
        user.setSafePassword(null);
        Integer onlineStatus = SingleLoginForRedis.getUserOnlineStatuByName(user.getBizSystem(), user.getUserName().trim(), BaseDwrUtil
                .getSession().getId());
        user.setIsOnline(onlineStatus);
        UserMoneyTotal userMoneyTotal = new UserMoneyTotal();
        userMoneyTotal.setUserId(user.getId());
        userMoneyTotal.setBizSystem(user.getBizSystem());
        userMoneyTotal = userMoneyTotalService.getTotalUsersConsumeReport(userMoneyTotal);
        List<UserBank> userBanks = userBankService.getAllUserBanksByUserId(userId);
        return AjaxUtil.createReturnValueSuccess(user, higherLevelUser, userBanks, userMoneyTotal);
    }

    /**
     * 删除指定用户
     *
     * @param userId
     * @return
     */
    public Object[] deleteUser(Integer userId) {
        if (userId == null) {
            return AjaxUtil.createReturnValueError("参数传递错误.");
        }
        User user = userService.selectByPrimaryKey(userId);
        if (user == null) {
            return AjaxUtil.createReturnValueError("参数传递错误.");
        }
        User editorUser = new User();
        editorUser.setId(userId);
        editorUser.setState(0);
        userService.updateByPrimaryKeySelective(editorUser);

        return AjaxUtil.createReturnValueSuccess();
    }

    /**
     * 删除指定游客
     *
     * @param userId
     * @return
     */
    public Object[] delTouristUser(Integer userId) {
        if (userId == null) {
            return AjaxUtil.createReturnValueError("参数传递错误.");
        }
        Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
        User user = userService.selectByPrimaryKey(userId);
        if (user == null) {
            return AjaxUtil.createReturnValueError("用户不存在.");
        }

        if (!user.getUserName().equals("GUEST_ADMIN")) {
            List<User> list = userService.getRegsByUser(user.getUserName() + "&", null, false, user.getBizSystem());
            if (list != null && list.size() > 0) {
                return AjaxUtil.createReturnValueError("该游客有下级用户不能删除！");
            }
            userService.delTouristUser(userId);
            log.info("管理员" + currentAdmin.getUsername() + "删除了业务系统[" + user.getBizSystemName() + "]的游客:[" + user.getUserName() + "]");
        } else {
            return AjaxUtil.createReturnValueError("游客管理员不能删除！");
        }

        return AjaxUtil.createReturnValueSuccess();
    }

    /**
     * 批量删除游客账户相关信息
     *
     * @param userId
     * @return
     */
    public Object[] delAllTouristUser(User user) {
        if (user == null) {
            return AjaxUtil.createReturnValueError("参数传递错误.");
        }
        Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
        if (!currentAdmin.getBizSystem().equals("SUPER_SYSTEM")) {
            user.setBizSystem(currentAdmin.getBizSystem());
        }
        userService.delAllTouristUser(user);
        log.info("管理员" + currentAdmin.getUsername() + "删除了业务系统[" + user.getBizSystemName() + "][" + user.getAddTimeStr() + "]前注册的游客账户相关信息");
        return AjaxUtil.createReturnValueSuccess();
    }

    /**
     * 编辑会员信息
     *
     * @param userId
     * @return
     */
    public Object[] editUser(User user) {
        if (user == null || user.getId() == null) {
            return AjaxUtil.createReturnValueError("参数传递错误.");
        }

        User oldUser = userService.selectByPrimaryKey(user.getId());
        user.setRegfrom(oldUser.getRegfrom());
        user.setBizSystem(oldUser.getBizSystem());

        if (oldUser.getIsTourist() == 0 && (!user.getVipLevel().equals("") || user.getPoint() != null)) {
            return AjaxUtil.createReturnValueError("正式会员不能编辑VIP等级或积分！");
        }

        String parentUserName = UserNameUtil.getUpLineUserName(oldUser.getRegfrom());
        User parentUser = userService.getUserByName(oldUser.getBizSystem(), parentUserName);

        if (!oldUser.getDailiLevel().equals(EUserDailLiLevel.SUPERAGENCY.name())) {

            if (!oldUser.getDailiLevel().equals(EUserDailLiLevel.DIRECTAGENCY.name())) {

                // 验证奖金模式
                String checkStr = this.checkRebate(parentUser, user, oldUser);
                if (checkStr != null && !"".equals(checkStr)) {
                    return AjaxUtil.createReturnValueError(checkStr);
                }

                // 会员变代理
                if (oldUser.getDailiLevel().equals(EUserDailLiLevel.REGULARMEMBERS.name())
                        && !user.getDailiLevel().equals(EUserDailLiLevel.REGULARMEMBERS.name())
                        && !user.getDailiLevel().equals(EUserDailLiLevel.DIRECTAGENCY.name())) {

                    // 父是直属
                    if (parentUser != null && parentUser.getDailiLevel().equals(EUserDailLiLevel.DIRECTAGENCY.name())) {
                        user.setDailiLevel(EUserDailLiLevel.GENERALAGENCY.getCode());
                    }
                    // 父是总代
                    if (parentUser != null && parentUser.getDailiLevel().equals(EUserDailLiLevel.GENERALAGENCY.name())) {
                        user.setDailiLevel(EUserDailLiLevel.ORDINARYAGENCY.getCode());
                    }
                }
                // else
                // if(oldUser.getDailiLevel().equals(EUserDailLiLevel.ORDINARYAGENCY.name())){
                // //父是直属
                // if(parentUser.getDailiLevel().equals(EUserDailLiLevel.DIRECTAGENCY.name())
                // &&parentUser.getSscRebate().subtract(user.getSscRebate()).compareTo(new
                // BigDecimal("2"))<=0){
                // user.setDailiLevel(EUserDailLiLevel.GENERALAGENCY.getCode());
                // }
                // }
            } else {
                // 验证奖金模式
                String checkStr = this.checkRebate(parentUser, user, oldUser);
                if (checkStr != null && !"".equals(checkStr)) {
                    return AjaxUtil.createReturnValueError(checkStr);
                }
            }
        }

        // 与注册链接对比
        String checkStr = this.checkRegisterRebate(user);
        if (checkStr != null && !"".equals(checkStr)) {
            return AjaxUtil.createReturnValueError(checkStr);
        }

        User editorUser = new User();
        editorUser.setId(user.getId());
        // 设定用户的模式
        // AwardModelUtil.setUserRebateInfo(editorUser, user.getSscRebate());
        editorUser.setSscRebate(user.getSscRebate());
        editorUser.setSyxwRebate(user.getSyxwRebate());
        editorUser.setKsRebate(user.getKsRebate());
        editorUser.setPk10Rebate(user.getPk10Rebate());
        editorUser.setDpcRebate(user.getDpcRebate());
        // 分分彩和骰宝，快乐十分，六合跟时时彩模式一起变化
        editorUser.setFfcRebate(AwardModelUtil.getModelBySsc(user.getSscRebate(), ELotteryTopKind.FFC));
        editorUser.setTbRebate(AwardModelUtil.getModelBySsc(user.getSscRebate(), ELotteryTopKind.TB));
        editorUser.setLhcRebate(AwardModelUtil.getModelBySsc(user.getSscRebate(), ELotteryTopKind.LHC));
        editorUser.setKlsfRebate(AwardModelUtil.getModelBySsc(user.getSscRebate(), ELotteryTopKind.KLSF));

        editorUser.setUserName(user.getUserName());
        editorUser.setBizSystem(oldUser.getBizSystem());
        editorUser.setPhone(user.getPhone());
        editorUser.setIdentityid(user.getIdentityid());
        editorUser.setBirthday(user.getBirthday());
        editorUser.setLoginLock(user.getLoginLock());
        editorUser.setWithdrawLock(user.getWithdrawLock());
        editorUser.setDailiLevel(user.getDailiLevel());
        editorUser.setQq(user.getQq());
        editorUser.setTrueName(user.getTrueName());
        editorUser.setSex(user.getSex());
        editorUser.setEmail(user.getEmail());
        editorUser.setProvince(user.getProvince());
        editorUser.setCity(user.getCity());
        editorUser.setAddress(user.getAddress());
        editorUser.setStarLevel(user.getStarLevel());
        // 只有游客才处理积分和等级
        if (oldUser.getIsTourist() == 1) {
            if (BigDecimal.ZERO.compareTo(user.getPoint()) < 0 || BigDecimal.ZERO.compareTo(user.getPoint()) == 0) {
                editorUser.setPoint(user.getPoint() == null ? BigDecimal.ZERO : user.getPoint());
            } else {
                return AjaxUtil.createReturnValueError("积分不能小于0！");
            }
            editorUser.setVipLevel(user.getVipLevel() == "" ? "" : user.getVipLevel());
            LevelSystem levelSystem = new LevelSystem();
            levelSystem.setBizSystem(editorUser.getBizSystem());
            // 设置称号
            List<LevelSystem> levelSystemByBizSysgtem = levelSytemService.getLevelSystemByBizSysgtem(levelSystem);
            for (LevelSystem level : levelSystemByBizSysgtem) {
                if (level.getLevel() == Integer.parseInt(user.getVipLevel())) {
                    editorUser.setLevelName(level.getLevelName());
                    break;
                }
            }
        }

        // 最后验证模式有没低于,高于系统设置
        Object[] result = AwardModelUtil.userAwardModelCheck(editorUser, parentUser);

        if (ConstantUtil.DWR_AJAX_RESULT_ERROR.equals(result[0])) {
            return AjaxUtil.createReturnValueError(result[1]);
        }

        String modifyStr = getModifyStr(oldUser, editorUser);
        Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
        String operateDesc = AdminOperateUtil
                .constructMessage("editUser_log", currentAdmin.getUsername(), oldUser.getUserName(), modifyStr);
        // 操作日志保存
        String operationIp = BaseDwrUtil.getRequestIp();
        adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(),
                currentAdmin.getUsername(), oldUser.getId(), oldUser.getUserName(), EAdminOperateLogModel.USER, operateDesc);

        // 改变真实姓名的话，需要发送邮件通知
        if (StringUtils.isEmpty(oldUser.getTrueName()) && !StringUtils.isEmpty(editorUser.getTrueName())
                || !StringUtils.isEmpty(oldUser.getTrueName()) && !oldUser.getTrueName().equals(editorUser.getTrueName())) {
            // 发送邮件通知，放置到消息队列中
            SendMailMessage mailMessage = new SendMailMessage();
            mailMessage.setSubject("系统告警(用户姓名编辑告警)");
            mailMessage.setContent(operateDesc);
            MessageQueueCenter.putMessage(mailMessage);

            // 发送短信通知
            /*
             * String res = SmsUtil.SendSms(operateDesc);
             * if("error".equals(res)) { return
             * AjaxUtil.createReturnValueError("系统错误，请重试"); }
             */
        }

        try {
            // 模式更改时调整配额
            userQuotaService.updateUserQuotaBySscrebateChange(editorUser, oldUser);
            userService.updateByPrimaryKeySelective(editorUser);
        } catch (Exception e) {
            return AjaxUtil.createReturnValueError(e.getMessage());
        }

        // TODO 置使锁定的用户退出单点登录
        // FrontUserOnlineInterface.deleteAlreadyEnter(oldUser.getUserName());

        return AjaxUtil.createReturnValueSuccess();
    }

    /**
     * 验证注册链接模式，编辑低于注册连接，要先修改注册链接
     *
     * @param userId
     */
    private String checkRegisterRebate(User user) {
        // 先验证各个模式不为null
        if (user.getSscRebate() == null || "".equals(user.getSscRebate())) {
            return "时时彩模式不能为空！";
        }
        // if(user.getFfcRebate() == null || "".equals(user.getFfcRebate())){
        // return "分分彩模式不能为空！";
        // }
        if (user.getSyxwRebate() == null || "".equals(user.getSyxwRebate())) {
            return "十一选五模式不能为空！";
        }
        if (user.getKsRebate() == null || "".equals(user.getKsRebate())) {
            return "快三模式不能为空！";
        }
        if (user.getPk10Rebate() == null || "".equals(user.getPk10Rebate())) {
            return "PK10模式不能为空！";
        }
        if (user.getDpcRebate() == null || "".equals(user.getDpcRebate())) {
            return "低频彩模式不能为空！";
        }
        List<UserRegister> list = userRegisterService.getUserRegisterLinks(user.getId());
        // GENERALAGENCY&1958&1958.00&1939.00&1958.00&1958.00&1958.00&1958.00&1958.00&1958.00&-&-&
        // 注册代理级别&时时彩模式&分分彩模式&十一选五模式&快三模式&PK10模式&低频彩模式&骰宝模式&六合彩模式&快乐十分模式&分红比例&注册赠送资金
        for (UserRegister userRegister : list) {
            String[] linkSets = userRegister.getLinkSets().split("&");
            if (user.getSscRebate().compareTo(new BigDecimal(linkSets[1])) < 0) {
                return "时时彩模式不能低于推广链接时时彩模式[" + linkSets[1] + "],请重新修改。谢谢！";
            }
            // if(user.getFfcRebate().compareTo(new BigDecimal(linkSets[2]))<0){
            // return "分分彩模式不能低于推广链接分分彩模式["+linkSets[2]+"],请重新修改。谢谢！";
            // }
            if (user.getSyxwRebate().compareTo(new BigDecimal(linkSets[3])) < 0) {
                return "十一选五彩模式不能低于推广链接十一选五模式[" + linkSets[3] + "],请重新修改。谢谢！";
            }
            if (user.getKsRebate().compareTo(new BigDecimal(linkSets[4])) < 0) {
                return "快三模式不能低于推广链接快三模式[" + linkSets[4] + "],请重新修改。谢谢！";
            }
            if (user.getPk10Rebate().compareTo(new BigDecimal(linkSets[5])) < 0) {
                return "PK10模式不能低于推广链接PK10模式[" + linkSets[5] + "],请重新修改。谢谢！";
            }
            if (user.getDpcRebate().compareTo(new BigDecimal(linkSets[6])) < 0) {
                return "低频彩模式不能低于推广链接低频彩模式[" + linkSets[6] + "],请重新修改。谢谢！";
            }

        }

        return "";
    }

    /**
     * 验证模式
     *
     * @param parentUser
     * @param user
     * @return
     */
    private String checkRebate(User parentUser, User user, User olduser) {

        // 先验证各个模式不为null
        if (user.getSscRebate() == null || "".equals(user.getSscRebate())) {
            return "时时彩模式不能为空！";
        }
        // if(user.getFfcRebate() == null || "".equals(user.getFfcRebate())){
        // return "分分彩模式不能为空！";
        // }
        if (user.getSyxwRebate() == null || "".equals(user.getSyxwRebate())) {
            return "十一选五模式不能为空！";
        }
        if (user.getKsRebate() == null || "".equals(user.getKsRebate())) {
            return "快三模式不能为空！";
        }
        if (user.getPk10Rebate() == null || "".equals(user.getPk10Rebate())) {
            return "PK10模式不能为空！";
        }
        if (user.getDpcRebate() == null || "".equals(user.getDpcRebate())) {
            return "低频彩模式不能为空！";
        }
        // 验证不能超过上级最高模式
        if (parentUser != null) {
            if (parentUser.getSscRebate().compareTo(user.getSscRebate()) < 0) {
                return "时时彩模式超过了上级用户最高模式了，上级最高模式：" + parentUser.getSscRebate();
            }
            // if(parentUser.getFfcRebate().compareTo(user.getFfcRebate())<0){
            // return "分分彩模式超过了上级用户最高模式了，上级最高模式："+parentUser.getFfcRebate();
            // }
            if (parentUser.getSyxwRebate().compareTo(user.getSyxwRebate()) < 0) {
                return "十一选五模式超过了上级用户最高模式了，上级最高模式：" + parentUser.getSyxwRebate();
            }
            if (parentUser.getKsRebate().compareTo(user.getKsRebate()) < 0) {
                return "快三模式超过了上级用户最高模式了，上级最高模式：" + parentUser.getKsRebate();
            }
            if (parentUser.getPk10Rebate().compareTo(user.getPk10Rebate()) < 0) {
                return "PK10模式超过了上级用户最高模式了，上级最高模式：" + parentUser.getPk10Rebate();
            }
            if (parentUser.getDpcRebate().compareTo(user.getDpcRebate()) < 0) {
                return "低频彩模式超过了上级用户最高模式了，上级最高模式：" + parentUser.getDpcRebate();
            }
        }

        String regfurm = user.getRegfrom() + user.getUserName() + "&";
        Integer maxRebate = userService.getMaxRebate(regfurm, user.getBizSystem(), 1);
        if (user.getSscRebate().compareTo(olduser.getSscRebate()) != 0 && maxRebate != null
                && user.getSscRebate().compareTo(new BigDecimal("" + maxRebate)) < 0) {
            return "时时彩模式不低于下级用户最高模式了，下级最高模式：" + maxRebate;
        }
        // maxRebate=userService.getMaxRebate(regfurm, user.getBizSystem(),2);
        // if(maxRebate!=null&&user.getFfcRebate().compareTo(new
        // BigDecimal(""+maxRebate))<0){
        // return "分分彩模式不低于下级用户最高模式了，下级最高模式："+maxRebate;
        // }
        maxRebate = userService.getMaxRebate(regfurm, user.getBizSystem(), 3);
        if (user.getSyxwRebate().compareTo(olduser.getSyxwRebate()) != 0 && maxRebate != null
                && user.getSyxwRebate().compareTo(new BigDecimal("" + maxRebate)) < 0) {
            return "十一选五模式不低于下级用户最高模式了，下级最高模式：" + maxRebate;
        }
        maxRebate = userService.getMaxRebate(regfurm, user.getBizSystem(), 4);
        if (user.getKsRebate().compareTo(olduser.getKsRebate()) != 0 && maxRebate != null
                && user.getKsRebate().compareTo(new BigDecimal("" + maxRebate)) < 0) {
            return "快三模式不低于下级用户最高模式了，下级最高模式：" + maxRebate;
        }
        maxRebate = userService.getMaxRebate(regfurm, user.getBizSystem(), 5);
        if (user.getPk10Rebate().compareTo(olduser.getPk10Rebate()) != 0 && maxRebate != null
                && user.getPk10Rebate().compareTo(new BigDecimal("" + maxRebate)) < 0) {
            return "pk10模式不低于下级用户最高模式了，下级最高模式：" + maxRebate;
        }
        maxRebate = userService.getMaxRebate(regfurm, user.getBizSystem(), 6);
        if (user.getDpcRebate().compareTo(olduser.getDpcRebate()) != 0 && maxRebate != null
                && user.getDpcRebate().compareTo(new BigDecimal("" + maxRebate)) < 0) {
            return "低频彩模式不低于下级用户最高模式了，下级最高模式：" + maxRebate;
        }

        return "";

    }

    /**
     * 获取用户编辑修改的项
     *
     * @param oldUser
     * @param editUser
     * @return
     */
    private String getModifyStr(User oldUser, User editUser) {
        StringBuffer modifyStr = new StringBuffer();
        if (oldUser != null && editUser != null) {
            // 修改了手机号码
            if (StringUtils.isEmpty(oldUser.getPhone()) && !StringUtils.isEmpty(editUser.getPhone())
                    || !StringUtils.isEmpty(oldUser.getPhone()) && !oldUser.getPhone().equals(editUser.getPhone())) {
                modifyStr.append(AdminOperateUtil.constructMessage("editPhone_log", oldUser.getPhone(), editUser.getPhone()));
            }
            // 修改了身份证号
            if (StringUtils.isEmpty(oldUser.getIdentityid()) && !StringUtils.isEmpty(editUser.getIdentityid())
                    || !StringUtils.isEmpty(oldUser.getIdentityid()) && !oldUser.getIdentityid().equals(editUser.getIdentityid())) {
                modifyStr.append(AdminOperateUtil.constructMessage("editIdentity_log", oldUser.getIdentityid(), editUser.getIdentityid()));
            }
            // 修改了生日
            if (StringUtils.isEmpty(oldUser.getBirthday()) && !StringUtils.isEmpty(editUser.getBirthday())
                    || !StringUtils.isEmpty(oldUser.getBirthday()) && !oldUser.getBirthday().equals(editUser.getBirthday())) {
                modifyStr.append(AdminOperateUtil.constructMessage("editBirthday_log", oldUser.getBirthday(), editUser.getBirthday()));
            }
            // 登陆锁定
            if (oldUser.getLoginLock() != editUser.getLoginLock()) {
                String oldLoginLock = oldUser.getLoginLock() == 0 ? "未锁定" : "锁定";
                String newLoginLock = editUser.getLoginLock() == 0 ? "未锁定" : "锁定";
                modifyStr.append(AdminOperateUtil.constructMessage("editIsLock_log", oldLoginLock, newLoginLock));
            }
            // 提现锁定
            if (oldUser.getWithdrawLock() != editUser.getWithdrawLock()) {
                String oldWithdrawLock = oldUser.getWithdrawLock() == 0 ? "未锁定" : "锁定";
                String newWithdrawLock = editUser.getWithdrawLock() == 0 ? "未锁定" : "锁定";
                modifyStr.append(AdminOperateUtil.constructMessage("editIsLock_log", oldWithdrawLock, newWithdrawLock));
            }
            // 代理级别
            if (!oldUser.getDailiLevel().equals(editUser.getDailiLevel())) {
                modifyStr.append(AdminOperateUtil.constructMessage("editDailiLevel_log", oldUser.getDailiLevelStr(),
                        editUser.getDailiLevelStr()));
            }
            // 时时彩返点
            if (oldUser.getSscRebate().compareTo(editUser.getSscRebate()) != 0) {
                modifyStr.append(AdminOperateUtil.constructMessage("editSscRebate_log", oldUser.getSscRebate().toString(), editUser
                        .getSscRebate().toString()));
            }
            // 分分彩返点
            if (oldUser.getFfcRebate().compareTo(editUser.getFfcRebate()) != 0) {
                modifyStr.append(AdminOperateUtil.constructMessage("editFfcRebate_log", oldUser.getFfcRebate().toString(), editUser
                        .getFfcRebate().toString()));
            }
            // 11选5返点
            if (oldUser.getSyxwRebate().compareTo(editUser.getSyxwRebate()) != 0) {
                modifyStr.append(AdminOperateUtil.constructMessage("editSyxwRebate_log", oldUser.getSyxwRebate().toString(), editUser
                        .getSyxwRebate().toString()));
            }
            // 修改了QQ
            if (StringUtils.isEmpty(oldUser.getQq()) && !StringUtils.isEmpty(editUser.getQq()) || !StringUtils.isEmpty(oldUser.getQq())
                    && !oldUser.getQq().equals(editUser.getQq())) {
                modifyStr.append(AdminOperateUtil.constructMessage("editQq_log", oldUser.getQq(), editUser.getQq()));
            }
            // 修改了真实姓名
            if (StringUtils.isEmpty(oldUser.getTrueName()) && !StringUtils.isEmpty(editUser.getTrueName())
                    || !StringUtils.isEmpty(oldUser.getTrueName()) && !oldUser.getTrueName().equals(editUser.getTrueName())) {
                modifyStr.append(AdminOperateUtil.constructMessage("editTruename_log", oldUser.getTrueName(), editUser.getTrueName()));
            }
            // 修改了性别
            if (oldUser.getSex() != null && oldUser.getSex() != editUser.getSex()) {
                String oldSex = oldUser.getSex() == 0 ? "女" : "男";
                String newSex = editUser.getSex() == 0 ? "女" : "男";
                modifyStr.append(AdminOperateUtil.constructMessage("editSex_log", oldSex, newSex));
            }
            // 修改了电子邮件
            if (StringUtils.isEmpty(oldUser.getEmail()) && !StringUtils.isEmpty(editUser.getEmail())
                    || !StringUtils.isEmpty(oldUser.getEmail()) && !oldUser.getEmail().equals(editUser.getEmail())) {
                modifyStr.append(AdminOperateUtil.constructMessage("editEmail_log", oldUser.getEmail(), editUser.getEmail()));
            }
            // 修改了星级
            if (oldUser.getStarLevel() != editUser.getStarLevel()) {
                String oldStarLevel = oldUser.getStarLevel() == null ? "" : oldUser.getStarLevel().toString();
                String newStarLevel = editUser.getStarLevel() == null ? "" : editUser.getStarLevel().toString();
                modifyStr.append(AdminOperateUtil.constructMessage("editStarLevel_log", oldStarLevel, newStarLevel));
            }

        }
        return modifyStr.toString();
    }

    /**
     * 用户续费或者扣费
     *
     * @param user
     * @return
     */
    public Object[] userRenew(User user, String operatePassword, String description) {
        if (user == null || user.getId() == null || StringUtils.isEmpty(user.getOperationType())
                || StringUtils.isEmpty(user.getOperationValue()) || StringUtils.isEmpty(operatePassword)) {
            return AjaxUtil.createReturnValueError("参数传递错误.");
        }
        BigDecimal operateValue = new BigDecimal(user.getOperationValue());
        if (operateValue.compareTo(new BigDecimal(0)) <= 0) {
            return AjaxUtil.createReturnValueError("金额必须大于0.");
        }
        Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
        // 获取当前admin，并判断当前admin状态是否为客服管理员，是的话提示
        Integer state = currentAdmin.getState();
        if (state == 3) {
            return AjaxUtil.createReturnValueError("客服管理员不能操作此功能！");
        }
        Admin dbAdmin = adminService.selectByPrimaryKey(currentAdmin.getId());
        if (dbAdmin.getLoginLock() == 1) {
            return AjaxUtil.createReturnValueError("您的账号已经被锁定,请联系超级管理员");
        }
        // 当管理员操作管理员密码错误时超过规定的次数的时候进行锁定处理.
        String loginIp = BaseDwrUtil.getRequestIp();
        Date nowDate = new Date();
        Date nowDateStart = DateUtil.getNowStartTimeByStart(nowDate);
        Date nowDateEnd = DateUtil.getNowStartTimeByEnd(nowDate);
        // 新建查询对象用于查询密码错误次数.
        AdminPasswordErrorLogQuery query = new AdminPasswordErrorLogQuery();
        query.setAdminId(currentAdmin.getId());
        query.setBizSystem(currentAdmin.getBizSystem());
        query.setErrorType(EAdminPasswordErrorType.OPERATE_PASSWORD_ERROR.getCode());
        query.setLoginIp(loginIp);
        query.setIsDeleted(0);
        query.setCreatedDateStart(nowDateStart);
        query.setCreatedDateEnd(nowDateEnd);
        // 查询记录数.
        int errorCount = adminPasswordErrorLogService.getAllAdminPasswordErrorLogByQueryPageCount(query);
        // 通过错误密码记录来确定管理员是否要被锁定!!
        if (errorCount >= SystemConfigConstant.maxLoginCount) {
            log.info("IP[" + loginIp + "]，管理员[" + dbAdmin.getUsername() + "]操作管理员时候," + SystemConfigConstant.maxLoginCount + "次，锁定该用户");
            Admin updateAdmin = new Admin();
            updateAdmin.setId(dbAdmin.getId());
            updateAdmin.setLoginLock(1);
            // 锁定管理员
            adminService.updateByPrimaryKeySelective(updateAdmin);
            adminPasswordErrorLogService.updateAdminPasswordErrorLogByErrorType(ConstantUtil.SUPER_SYSTEM, loginIp, dbAdmin.getUsername(),
                    EAdminPasswordErrorType.PASSWORD_ERROR.getCode());
            log.info("系统[" + dbAdmin.getBizSystem() + "]中的管理员[" + dbAdmin.getUsername() + "]因为操作时候密码错误多次,被锁定!!!");
            // 当前用户密码错误失败过多，用户被锁定，请联系客服解锁
            return AjaxUtil.createReturnValueError("当前管理员输入密码错误次数过多，已被锁定");
        }

        if (!MD5PasswordUtil.GetMD5Code(operatePassword).equals(dbAdmin.getOperatePassword())) {
            // 新建管理员密码错误日志对象,用于校验管理员是否应该被锁定.
            AdminPasswordErrorLog adminPasswordErrorLog = new AdminPasswordErrorLog();
            adminPasswordErrorLog.setAdminId(currentAdmin.getId());
            adminPasswordErrorLog.setAdminName(currentAdmin.getUsername());
            adminPasswordErrorLog.setBizSystem(currentAdmin.getBizSystem());
            adminPasswordErrorLog.setCreatedDate(new Date());
            adminPasswordErrorLog.setErrorType(EAdminPasswordErrorType.OPERATE_PASSWORD_ERROR.getCode());
            adminPasswordErrorLog.setIsDeleted(0);
            adminPasswordErrorLog.setLoginIp(loginIp);
            adminPasswordErrorLog.setSourceType(EAdminPasswordErrorLog.ADMIN_OPERATION_USER_PWD_ERROR.getCode());
            adminPasswordErrorLog.setSourceDes(EAdminPasswordErrorLog.ADMIN_OPERATION_USER_PWD_ERROR.getDescription());
            // 新建管理员错误密码记录对象.
            adminPasswordErrorLogService.insertSelective(adminPasswordErrorLog);
            return AjaxUtil.createReturnValueError("当前管理员操作密码错误,错误超过" + SystemConfigConstant.maxLoginCount + "次,将被锁定!!!");
        } else {
            // 密码正确,就软删除之前的所有的错误密码记录.
            adminPasswordErrorLogService.updateAdminPasswordErrorLogByErrorType(currentAdmin.getBizSystem(), loginIp,
                    currentAdmin.getUsername(), EAdminPasswordErrorType.OPERATE_PASSWORD_ERROR.getCode());
        }
        try {
            userService.userRenew(user.getId(), user.getOperationType(), user.getOperationValue(), description, currentAdmin);
        } catch (UnEqualVersionException e) {
            log.error(e.getMessage(), e);
            return AjaxUtil.createReturnValueError("当前系统繁忙，请稍后重试");
        } catch (Exception e) {
            return AjaxUtil.createReturnValueError(e.getMessage());
        }

        // 操作日志保存
        String operationTypeName = getOperationTypeName(user.getOperationType());
        if (!StringUtils.isEmpty(operationTypeName)) { // 类型名称不为空
            User operationUser = userService.selectByPrimaryKey(user.getId());
            String operateDesc = AdminOperateUtil.constructMessage("xufei_log", currentAdmin.getUsername(), operationTypeName,
                    operationUser.getUserName(), operateValue.setScale(3).toString());
            String operationIp = BaseDwrUtil.getRequestIp();
            adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(),
                    currentAdmin.getUsername(), user.getId(), operationUser.getUserName(), EAdminOperateLogModel.USER_XUFEI, operateDesc);
        }

        return AjaxUtil.createReturnValueSuccess();
    }

    private String getOperationTypeName(String operationType) {
        String operationTypeName = "";
        if (operationType.equals("RECHARGE")) {
            operationTypeName = "系统充值";
        } else if (operationType.equals("RECHARGE_PRESENT")) {
            operationTypeName = "充值赠送";
        } else if (operationType.equals("ACTIVITIES_MONEY")) {
            operationTypeName = "活动彩金";
        } else if (operationType.equals("SYSTEM_REDUCE_MONEY_FOR_ACTIVITIES")) { // 系统扣费(活动彩金)
            operationTypeName = "活动彩金扣费";
        } else if (operationType.equals("SYSTEM_REDUCE_MONEY_FOR_RECHARGE")) { // 系统扣费(系统充值)
            operationTypeName = "系统扣费(系统充值)";
        } else if (operationType.equals("SYSTEM_REDUCE_MONEY_FOR_RECHARGE_PRESENT")) { // 系统扣费(充值赠送)
            operationTypeName = "充值赠送扣费";
        } else if (operationType.equals("SYSTEM_REDUCE_MONEY_FOR_QUICK_RECHARGE")) { // 系统扣费(快捷充值)
            operationTypeName = "系统扣费(快捷充值)";
        } else if (operationType.equals("SYSTEM_REDUCE_MONEY_FOR_NET_RECHARGE")) { // 系统扣费(网银充值
            // )
            operationTypeName = "系统扣费(网银充值)";
        } else if (operationType.equals("SYSTEM_REDUCE_MONEY_FOR_WIN_RETURN")) { // 系统扣费(中奖扣除)
            operationTypeName = "系统扣费(中奖扣除)";
        } else if (operationType.equals("WAGES_PRESENT")) { // 工资派送
            operationTypeName = "工资派送";
        } else if (operationType.equals("WIN")) { // 中奖归还
            operationTypeName = "中奖归还";
        } else if (operationType.equals("SYSTEM_ADD_MONEY")) { // 人工存入
            operationTypeName = "人工存入";
        } else if (operationType.equals("SYSTEM_REDUCE_MONEY")) { // 误存提出
            operationTypeName = "误存提出";
        } else if (operationType.equals("MANAGE_REDUCE_MONEY")) { // 行政提出RECHARGE_MANUAL
            operationTypeName = "行政提出";
        } else if (operationType.equals("RECHARGE_MANUAL")) { // 人工存款
            operationTypeName = "人工存款";
        } else {
            throw new RuntimeException("类型未配置.");
        }
        return operationTypeName;
    }

    /**
     * 用户积分操作 暂时无用
     *
     * @param user
     * @return
     */
    public Object[] userPoint(User user) {
        if (user == null || user.getId() == null || StringUtils.isEmpty(user.getOperationValue())) {
            return AjaxUtil.createReturnValueError("参数传递错误.");
        }
        BigDecimal operateValue = new BigDecimal(user.getOperationValue());
        if (operateValue.compareTo(new BigDecimal(0)) <= 0) {
            return AjaxUtil.createReturnValueError("积分数目必须大于0.");
        }
        /*
         * try { userService.userPoint(user.getId(), user.getOperationType(),
         * user.getOperationValue()); } catch(UnEqualVersionException e) {
         * log.error(e.getMessage(), e); return
         * AjaxUtil.createReturnValueError("当前系统繁忙，请稍后重试"); } catch (Exception
         * e) { return AjaxUtil.createReturnValueError(e.getMessage()); }
         */
        return AjaxUtil.createReturnValueSuccess();
    }

    /**
     * 用户返点设置
     *
     * @param user
     * @return
     */
    public Object[] userRebateSet(User user) {
        if (user == null || user.getId() == null) {
            return AjaxUtil.createReturnValueError("参数传递错误.");
        }
        User operationUser = userService.selectByPrimaryKey(user.getId());
        if (operationUser != null) {
            User editorUser = new User();
            editorUser.setId(operationUser.getId());
            editorUser.setSscRebate(user.getSscRebate());
            editorUser.setFfcRebate(user.getFfcRebate());
            editorUser.setSyxwRebate(user.getSyxwRebate());
            editorUser.setKsRebate(user.getKsRebate());
            editorUser.setKlsfRebate(user.getKlsfRebate());
            editorUser.setDpcRebate(user.getDpcRebate());
            userService.updateByPrimaryKeySelective(editorUser);
        } else {
            return AjaxUtil.createReturnValueError("参数传递错误.");
        }
        return AjaxUtil.createReturnValueSuccess();
    }

    /**
     * 查找所有用户的结算报表
     *
     * @return
     */
    public Object[] queryTotalUsersConsumeReport(UserMoneyTotal query, Page page) {
        if (query == null || page == null) {
            return AjaxUtil.createReturnValueError("参数传递错误.");
        }
        Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
        if (!ConstantUtil.SUPER_SYSTEM.equals(currentAdmin.getBizSystem())) {
            query.setBizSystem(currentAdmin.getBizSystem());
        }

        page = userMoneyTotalService.getAllUserMoneyTotalByQueryPage(query, page);
        UserMoneyTotal totalUser = userMoneyTotalService.getTotalUsersConsumeReport(query);
        return AjaxUtil.createReturnValueSuccess(page);
    }

    /**
     * 变更会员推荐人关系链
     *
     * @param fromUserName
     * @param toUserName
     * @return
     */
    public Object[] updateUsersRegFrom(String fromUserName, String toUserName, String bizSystem) {
        if (bizSystem == null || fromUserName == null || toUserName == null) {
            return AjaxUtil.createReturnValueError("参数传递错误.");
        }
        User fromUser = userService.getUserByName(bizSystem, fromUserName);
        User toUser = userService.getUserByName(bizSystem, toUserName);
        if (fromUser == null || toUser == null) {
            return AjaxUtil.createReturnValueError("用户名不存在.");
        }
        // userService.updateUsersRegFrom(fromUser, toUser);
        return AjaxUtil.createReturnValueSuccess();
    }

    /**
     * 锁定指定用户
     *
     * @param userId
     * @return
     */
    public Object[] lockUser(Integer userId) {
        if (userId == null) {
            return AjaxUtil.createReturnValueError("参数传递错误.");
        }
        User dealUser = new User();
        dealUser.setId(userId);
        dealUser.setLoginLock(1);
        userService.updateByPrimaryKeySelective(dealUser);
        dealUser = userService.selectByPrimaryKey(userId);
        // 置使锁定的用户退出单点登录
//		SingleLoginForRedis.deleteAlreadyEnter(null, Login.LOGTYPE_PC + "." + dealUser.getBizSystem(), dealUser.getUserName());
//		SingleLoginForRedis.deleteAlreadyEnter(null, Login.LOGTYPE_MOBILE + "." + dealUser.getBizSystem(), dealUser.getUserName());
//		SingleLoginForRedis.deleteAlreadyEnter(null, Login.LOGTYPE_APP + "." + dealUser.getBizSystem(), dealUser.getUserName());

        UserKickOutMessageVo userKickOutMessage = new UserKickOutMessageVo(dealUser.getBizSystem(), dealUser.getUserName());
        userKickOutSender.sendMessage(userKickOutMessage);

        // 操作日志保存
        Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
        String operateDesc = AdminOperateUtil.constructMessage("user_lock_log", currentAdmin.getUsername(), dealUser.getUserName());
        String operationIp = BaseDwrUtil.getRequestIp();
        adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(),
                currentAdmin.getUsername(), dealUser.getId(), dealUser.getUserName(), EAdminOperateLogModel.USER, operateDesc);
        return AjaxUtil.createReturnValueSuccess();
    }

    /**
     * 解锁指定用户
     *
     * @param userId
     * @return
     */
    public Object[] unlockUser(Integer userId) {
        if (userId == null) {
            return AjaxUtil.createReturnValueError("参数传递错误.");
        }
        User dealUser = new User();
        dealUser.setId(userId);
        dealUser.setLoginLock(0);
        userService.updateByPrimaryKeySelective(dealUser);
        // 根据用户ID删除日志记录.
        passwordErrorLogService.updatePasswordErrorLogByUserIdForPwd(userId);
        // 操作日志保存
        Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
        User operationUser = userService.selectByPrimaryKey(userId);
        String operateDesc = AdminOperateUtil.constructMessage("user_unlock_log", currentAdmin.getUsername(), operationUser.getUserName());
        String operationIp = BaseDwrUtil.getRequestIp();
        adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(),
                currentAdmin.getUsername(), operationUser.getId(), operationUser.getUserName(), EAdminOperateLogModel.USER, operateDesc);
        return AjaxUtil.createReturnValueSuccess();
    }

    /**
     * 锁定指定用户
     *
     * @param userId
     * @return
     */
    public Object[] lockWithdraw(Integer userId) {
        if (userId == null) {
            return AjaxUtil.createReturnValueError("参数传递错误.");
        }
        User dealUser = new User();
        dealUser.setId(userId);
        dealUser.setWithdrawLock(1);
        userService.updateByPrimaryKeySelective(dealUser);
        User user = userService.selectByPrimaryKey(userId);
        // 操作日志保存
        Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
        String operateDesc = AdminOperateUtil.constructMessage("user_withdrawLock_log", currentAdmin.getUsername(), user.getUserName());
        String operationIp = BaseDwrUtil.getRequestIp();
        adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(),
                currentAdmin.getUsername(), user.getId(), user.getUserName(), EAdminOperateLogModel.USER, operateDesc);
        return AjaxUtil.createReturnValueSuccess();
    }

    /**
     * 解锁指定用户
     *
     * @param userId
     * @return
     */
    public Object[] unlockWithdraw(Integer userId) {
        if (userId == null) {
            return AjaxUtil.createReturnValueError("参数传递错误.");
        }
        User dealUser = new User();
        dealUser.setId(userId);
        dealUser.setWithdrawLock(0);
        userService.updateByPrimaryKeySelective(dealUser);
        passwordErrorLogService.updatePasswordErrorLogByUserIdForSafePwd(userId);
        // 操作日志保存
        Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
        User operationUser = userService.selectByPrimaryKey(userId);
        String operateDesc = AdminOperateUtil.constructMessage("user_withdrawUnlock_log", currentAdmin.getUsername(),
                operationUser.getUserName());
        String operationIp = BaseDwrUtil.getRequestIp();
        adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(),
                currentAdmin.getUsername(), operationUser.getId(), operationUser.getUserName(), EAdminOperateLogModel.USER, operateDesc);
        return AjaxUtil.createReturnValueSuccess();
    }

    /**
     * 初始化指定用户的密码
     *
     * @param userId
     * @return
     */
    public Object[] resetUserPass(Integer userId) {
        if (userId == null) {
            return AjaxUtil.createReturnValueError("参数传递错误.");
        }
        String result = StringUtils.getRandomString(8);
        User dealUser = new User();
        dealUser.setId(userId);
        dealUser.setPassword(MD5PasswordUtil.GetMD5Code(result));
        userService.updateByPrimaryKeySelective(dealUser);
        passwordErrorLogService.updatePasswordErrorLogByUserIdForSafePwd(userId);

        // 操作日志保存
        Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
        User operationUser = userService.selectByPrimaryKey(userId);
        String operateDesc = AdminOperateUtil
                .constructMessage("resetUserPass_log", currentAdmin.getUsername(), operationUser.getUserName());
        String operationIp = BaseDwrUtil.getRequestIp();
        adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(),
                currentAdmin.getUsername(), operationUser.getId(), operationUser.getUserName(), EAdminOperateLogModel.USER, operateDesc);
        return AjaxUtil.createReturnValueSuccess(result);
    }

    /**
     * 初始化指定用户的安全密码
     *
     * @param userId
     * @return
     */
    public Object[] resetUserPass1(Integer userId) {
        if (userId == null) {
            return AjaxUtil.createReturnValueError("参数传递错误.");
        }

        String result = StringUtils.getRandomString(8);
        // 根据用户ID删除用户错误日志记录.
        passwordErrorLogService.delPasswordErrorLogByUserIdForCheckSavePassWord(userId);

        // 操作日志保存
        Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
        User operationUser = userService.selectByPrimaryKey(userId);
        String safePassword = operationUser.getSafePassword();
        if (StringUtils.isEmpty(safePassword)) {
            return AjaxUtil.createReturnValueError("当前用户还没有设置安全密码,不能初始化安全密码.");
        }

        // 更新用户信息安全密码信息.
        User dealUser = new User();
        dealUser.setId(userId);
        dealUser.setSafePassword(MD5PasswordUtil.GetMD5Code(result));
        userService.updateByPrimaryKeySelective(dealUser);

        String operateDesc = AdminOperateUtil.constructMessage("resetUserPass1_log", currentAdmin.getUsername(),
                operationUser.getUserName());
        String operationIp = BaseDwrUtil.getRequestIp();
        adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(),
                currentAdmin.getUsername(), operationUser.getId(), operationUser.getUserName(), EAdminOperateLogModel.USER, operateDesc);

        return AjaxUtil.createReturnValueSuccess(result);
    }

    /**
     * 查询用户银行卡信息
     *
     * @param bankQueryVo
     * @return
     */
    public Object[] queryUserBanks(UserBankQueryVo bankQueryVo, Page page) {
        // if(bankQueryVo == null ||
        // (StringUtils.isEmpty(bankQueryVo.getUserName()) &&
        // StringUtils.isEmpty(bankQueryVo.getBankNum()) &&
        // StringUtils.isEmpty(bankQueryVo.getBankAccount()))){
        // return AjaxUtil.createReturnValueError("请输入查询条件.");
        // }

        Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
        if (!(ConstantUtil.SUPER_SYSTEM).equals(currentAdmin.getBizSystem())) {
            bankQueryVo.setBizSystem(currentAdmin.getBizSystem());
        }
        page = userBankService.getAllUserBanksByQueryPage(bankQueryVo, page);
        // List<UserBank> userBanks =
        // userBankService.getUserBanksByUserName(bankQueryVo);
        return AjaxUtil.createReturnValueSuccess(page);
    }

    /**
     * 删除我的银行卡
     *
     * @return
     */
    public Object[] delBankCard(Long id) {
        UserBank userBank = userBankService.selectByPrimaryKey(id);
        if (userBank != null) {

            Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
            String operateDesc = AdminOperateUtil.constructMessage("delBankCard_log", currentAdmin.getUsername(), userBank.getUserName(),
                    userBank.getBankAccountName(), userBank.getBankCardNum());
            String operationIp = BaseDwrUtil.getRequestIp();
            // 操作日志保存
            adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(),
                    currentAdmin.getUsername(), userBank.getUserId(), userBank.getUserName(), EAdminOperateLogModel.PAYBANK, operateDesc);

            // 发送邮件通知，放置到消息队列中
            SendMailMessage mailMessage = new SendMailMessage();
            mailMessage.setSubject("系统告警(用户银行卡编辑告警)");
            mailMessage.setContent(operateDesc);
            MessageQueueCenter.putMessage(mailMessage);

            // 发送短信通知
            /*
             * String result = SmsUtil.SendSms(operateDesc);
             * if("error".equals(result)) { return
             * AjaxUtil.createReturnValueError("系统错误，请重试"); }
             */

            userBankService.deleteByPrimaryKey(id);
        }

        return AjaxUtil.createReturnValueSuccess();
    }

    /**
     * 保存用户银行卡信息
     *
     * @param bankQueryVo
     * @return
     */
    public Object[] addUserBanks(UserBank userBank) {

        if (userBank == null) {
            return AjaxUtil.createReturnValueError("参数传递错误.");
        }

        if (StringUtils.isEmpty(userBank.getBankName())) {
            return AjaxUtil.createReturnValueError("请选择开户银行");
        }
        if (StringUtils.isEmpty(userBank.getProvince())) {
            return AjaxUtil.createReturnValueError("请选择开户所在省份");
        }
        if (StringUtils.isEmpty(userBank.getCity())) {
            return AjaxUtil.createReturnValueError("请选择开户所在城市");
        }
        if (StringUtils.isEmpty(userBank.getBankAccountName())) {
            return AjaxUtil.createReturnValueError("请填写开户人");
        }
        if (StringUtils.isEmpty(userBank.getBankCardNum())) {
            return AjaxUtil.createReturnValueError("请填写银行卡号");
        }
        if (userBank.getBankCardNum().length() < 15 || userBank.getBankCardNum().length() > 20) {
            return AjaxUtil.createReturnValueError("银行卡号由15-19位数字组成");
        }
        if (StringUtils.isEmpty(userBank.getBankType())) {
            return AjaxUtil.createReturnValueError("请选择银行类型");
        }
        userBank.setState(1);
        userBank.setLocked(0);
        Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();

        String operateDesc = AdminOperateUtil
                .constructMessage("userbankAdd_log", currentAdmin.getUsername(), userBank.getUserName());
        String operationIp = BaseDwrUtil.getRequestIp();
        adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(),
                currentAdmin.getUsername(), userBank.getUserId(), userBank.getUserName(), EAdminOperateLogModel.USER_BANK, operateDesc);

        BankCardMessage bankCardMessage = new BankCardMessage();
        bankCardMessage.setBizSystem(userBank.getBizSystem());
        bankCardMessage.setBankType(userBank.getBankType());
        bankCardMessage.setBankName(userBank.getBankName());
        bankCardMessage.setBankAccountName(userBank.getBankAccountName());
        bankCardMessage.setBankCardNum(userBank.getBankCardNum());
        bankCardMessage.setUserName(userBank.getUserName());
        bankCardMessage.setUserId(userBank.getUserId());
		/*bankCardMessage.setRelationUserId(userIdStr);
		bankCardMessage.setRelationUserName(userNameStr);*/
        //bankCardMessage.setUnusualBankCard("用户["+userNameStr+"]银行卡姓名同名");
        MessageQueueCenter.putMessage(bankCardMessage);
        userBankService.insert(userBank);
        return AjaxUtil.createReturnValueSuccess();
    }

    /**
     * 判断用户名是否存在
     *
     * @param userId
     * @param state
     * @return
     */
    public Object[] judgeUserExist(UserInfoExportVO userInfoExportVO) {
        if (userInfoExportVO == null || StringUtils.isEmpty(userInfoExportVO.getUserName())
                || StringUtils.isEmpty(userInfoExportVO.getBizSystem())) {
            return AjaxUtil.createReturnValueError("参数传递错误.");
        }

        User user = userService.getUserByName(userInfoExportVO.getBizSystem(), userInfoExportVO.getUserName());
        if (user == null) {
            return AjaxUtil.createReturnValueError("输入的用户名不存在");
        }
        return AjaxUtil.createReturnValueSuccess();
    }

    /**
     * 导出用户信息
     *
     * @param userName
     * @param request
     * @param response
     * @return
     */
    public FileTransfer userInfoExport(UserInfoExportVO userInfoExportVO, HttpServletRequest request, HttpServletResponse response) {
        if (userInfoExportVO == null || StringUtils.isEmpty(userInfoExportVO.getUserName())) {
            log.error("参数传递错误");
            return null;
        }
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            List<User> users = new ArrayList<User>();
            TeamUserQuery userQuery = new TeamUserQuery();
            userQuery.setBizSystem(userInfoExportVO.getBizSystem());
            userQuery.setTeamLeaderName(ConstantUtil.REG_FORM_SPLIT + userInfoExportVO.getUserName() + ConstantUtil.REG_FORM_SPLIT);
            userQuery.setIsQuerySub(true);
            users = userService.getAllTeamUserList(userQuery);

            String fileName = userInfoExportVO.getUserName() + "所有下级用户联系方式-" + DateUtil.getDate(new Date(), "yyyy-MM-dd") + ".xls";
            ExcelExportData excelExportData = this.constructUserInfoExcelData(fileName, users, userInfoExportVO);
            fileName = new String(fileName.getBytes("GBK"), "iso8859-1");
            bos = ExcelUtil.export2Stream(excelExportData);
            return new FileTransfer(fileName, "application/msexcel", bos.toByteArray());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    /**
     * 根据用户名构建表格数据
     *
     * @param users
     * @return
     */
    private ExcelExportData constructUserInfoExcelData(String fileName, List<User> users, UserInfoExportVO userInfoExportVO) {
        List<String[]> columNames = new ArrayList<String[]>();
        List<String[]> fieldNames = new ArrayList<String[]>();

        List<String> sheetColumNames = new ArrayList<String>();
        List<String> sheetFieldNames = new ArrayList<String>();
        sheetColumNames.add("用户名");
        sheetFieldNames.add("userName");
        if (userInfoExportVO.getHasTruename()) {
            sheetColumNames.add("真实姓名");
            sheetFieldNames.add("trueName");
        }
        if (userInfoExportVO.getHasAddress()) {
            sheetColumNames.add("住址");
            sheetFieldNames.add("address");
        }
        if (userInfoExportVO.getHasIdentityid()) {
            sheetColumNames.add("身份证号码");
            sheetFieldNames.add("identityid");
        }
        if (userInfoExportVO.getHasPhone()) {
            sheetColumNames.add("手机号码");
            sheetFieldNames.add("phone");
        }
        if (userInfoExportVO.getHasQq()) {
            sheetColumNames.add("QQ");
            sheetFieldNames.add("qq");
        }
        if (userInfoExportVO.getHasEmail()) {
            sheetColumNames.add("邮箱");
            sheetFieldNames.add("email");
        }
        if (userInfoExportVO.getHasAddtime()) {
            sheetColumNames.add("注册时间");
            sheetFieldNames.add("addTimeStr");
        }
        columNames.add(sheetColumNames.toArray(new String[sheetColumNames.size()]));
        fieldNames.add(sheetFieldNames.toArray(new String[sheetFieldNames.size()]));

        // 对user资料，为空设置为无
        if (CollectionUtils.isNotEmpty(users)) {
            for (User u : users) {
                if (StringUtils.isEmpty(u.getTrueName())) {
                    u.setTrueName("无");
                }
                if (StringUtils.isEmpty(u.getIdentityid())) {
                    u.setIdentityid("无");
                }
                if (StringUtils.isEmpty(u.getPhone())) {
                    u.setPhone("无");
                }
                if (StringUtils.isEmpty(u.getQq())) {
                    u.setQq("无");
                }
                if (StringUtils.isEmpty(u.getEmail())) {
                    u.setEmail("无");
                }
                String address = "";
                if (StringUtils.isEmpty(u.getProvince()) && StringUtils.isEmpty(u.getCity()) && StringUtils.isEmpty(u.getAddress())) {
                    address = "无";
                } else {
                    if (!StringUtils.isEmpty(u.getProvince())) {
                        address += u.getProvince();
                    }
                    if (!StringUtils.isEmpty(u.getCity())) {
                        address += u.getCity();
                    }
                    if (!StringUtils.isEmpty(u.getAddress())) {
                        address += u.getAddress();
                    }
                }
                u.setAddress(address);
            }
        }
        LinkedHashMap<String, List<?>> map = new LinkedHashMap<String, List<?>>();
        map.put(fileName, users);

        ExcelExportData setInfo = new ExcelExportData();
        setInfo.setDataMap(map);
        setInfo.setFieldNames(fieldNames);
        setInfo.setTitles(new String[]{});
        setInfo.setColumnNames(columNames);

        // 将需要导出的数据输出到文件
        return setInfo;
    }

    /**
     * 查询用户在线人数数据
     *
     * @return
     */
    public Object[] getUsersCount(String bizSystem) {
        List<BizSystem> list = new ArrayList<BizSystem>();

        if (ConstantUtil.SUPER_SYSTEM.equals(BaseDwrUtil.getCurrentAdmin().getBizSystem())) {
            if (bizSystem == null || "".equals(bizSystem)) {
                list = bizSystemService.getAllBizSystem();
            } else {
                BizSystem biz = new BizSystem();
                biz.setBizSystem(bizSystem);
                biz = bizSystemService.getBizSystemByCondition(biz);
                list.add(biz);
            }

        } else {
            BizSystem biz = new BizSystem();
            biz.setBizSystem(BaseDwrUtil.getCurrentAdmin().getBizSystem());
            list.add(biz);

        }
        // 总在线人数（前端用户）
        int pcUsersCount = SingleLoginForRedis.getAllLoginUsers(list, Login.LOGTYPE_PC);
        // 手机端用户
        int mobileLoginUsersCount = SingleLoginForRedis.getAllLoginUsers(list, Login.LOGTYPE_MOBILE);
        // 手机端用户
        int appLoginUsersCount = SingleLoginForRedis.getAllLoginUsers(list, Login.LOGTYPE_APP);

        return AjaxUtil.createReturnValueSuccess(pcUsersCount, mobileLoginUsersCount, appLoginUsersCount);
        // return AjaxUtil.createReturnValueSuccess(1, 1);
    }

    /**
     * 会员强制退出
     *
     * @return
     */
    public Object[] logoutUser(String bizSystem, String userName) {
        Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
        // if(currentAdmin.getState() != Admin.SUPER_STATE) {
        // return AjaxUtil.createReturnValueError("对不起，您不能进行此操作");
        // }
//		SingleLoginForRedis.loginOutByUser(Login.LOGTYPE_PC + "." + bizSystem, userName);
//		SingleLoginForRedis.loginOutByUser(Login.LOGTYPE_MOBILE + "." + bizSystem, userName);
//		SingleLoginForRedis.loginOutByUser(Login.LOGTYPE_APP + "." + bizSystem, userName);
        log.info(currentAdmin.getUsername() + "管理员对" + bizSystem + "系统用户" + userName + "进行强制退出!");
        UserKickOutMessageVo userKickOutMessage = new UserKickOutMessageVo(bizSystem, userName);
        userKickOutSender.sendMessage(userKickOutMessage);
        return AjaxUtil.createReturnValueSuccess();
    }

    /**
     * 查找所有用户的结算报表
     *
     * @return
     */
    public Object[] userStatisticalRankings(UserQuery query, Page page) {
        if (query == null || page == null) {
            return AjaxUtil.createReturnValueError("参数传递错误.");
        }
        Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
        if (!ConstantUtil.SUPER_SYSTEM.equals(currentAdmin.getBizSystem())) {
            query.setBizSystem(currentAdmin.getBizSystem());
        }
        query.setIsTourist(0);
        page = userMoneyTotalService.getUserStatisticalRankingsPage(query, page);
        /*
         * UserMoneyTotal totalUser =
         * userMoneyTotalService.getTotalUsersConsumeReport(query);
         */
        return AjaxUtil.createReturnValueSuccess(page);
    }

    /**
     * 一键加星
     *
     * @param query
     * @param sl
     * @return
     */
    public Object[] updateUserStarLevelByUserId(UserQuery query, Integer sl) {
        if (query == null) {
            return AjaxUtil.createReturnValueError("参数传递错误.");
        }
        Integer i = 0;
        try {
            String ids = userMoneyTotalService.queryUserStatisticalRankingsIds(query);
            i = userMoneyTotalService.updateUserStarLevelByUserId(sl, ids);
        } catch (Exception e) {
            return AjaxUtil.createReturnValueError("一键加星失败");
        }
        return AjaxUtil.createReturnValueSuccess(i);
    }

    /**
     * 根据业务系统和用户获取用户
     *
     * @return 返回查询的用户和该用户底下所有下级数量
     */
    public Object[] queryUserLowerNumberByName(User query) {
        // 获取当前登陆的用户
        Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
        if (!ConstantUtil.SUPER_SYSTEM.equals(currentAdmin.getBizSystem())) {
            query.setBizSystem(currentAdmin.getBizSystem());
        }
        User user = userService.getUserByName(query.getBizSystem(), query.getUserName());
        int sl = 0;
        if (user != null) {
            TeamUserQuery team = new TeamUserQuery();
            team.setBizSystem(query.getBizSystem());
            team.setIsQuerySub(true);
            if (!"".equals(user.getRegfrom()) && user.getRegfrom() != null) {
                team.setTeamLeaderName("&" + user.getUserName() + "&");
            } else {
                team.setTeamLeaderName(user.getUserName() + "&");
            }
            sl = userService.getAllTeamUserList(team).size();
        } else {
            return AjaxUtil.createReturnValueError("用户不存在");
        }
        return AjaxUtil.createReturnValueSuccess(user, sl);
    }

    /**
     * 感觉业务系统和上级名称修改其所有下级的奖金模式
     *
     * @param user
     * @return
     */
    public Object[] updateUserAllLowerLevelsByName(User user) {
        if (user == null) {
            return AjaxUtil.createReturnValueError("参数传递错误");
        }
        String operateDesc = "";
        Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
        if (user.getSscRebate() == null && user.getKsRebate() == null && user.getSyxwRebate() == null && user.getPk10Rebate() == null
                && user.getDpcRebate() == null && user.getCode() == 1) {
            return AjaxUtil.createReturnValueError("请选择要修改的模式");
        } else if (user.getCode() == 1) {
            String msg = "";
            if (user.getSscRebate() != null) {
                msg += "时时彩奖金模式调整为[" + user.getSscRebate() + "]，";
                user.setFfcRebate(user.getSscRebate());
                msg += "分分彩奖金模式调整为[" + user.getFfcRebate() + "]，";
            }
            if (user.getKsRebate() != null) {
                msg += "快三奖金模式调整为[" + user.getKsRebate() + "]，";
            }
            if (user.getSyxwRebate() != null) {
                msg += "11选5奖金模式调整为[" + user.getSyxwRebate() + "]，";
            }
            if (user.getPk10Rebate() != null) {
                msg += "PK10奖金模式调整为[" + user.getPk10Rebate() + "]，";
            }
            if (user.getDpcRebate() != null) {
                msg += "低频次奖金模式调整为[" + user.getDpcRebate() + "]，";
            }
            msg = msg.substring(0, msg.length() - 1);
            operateDesc = AdminOperateUtil.constructMessage("rebate_adjust", currentAdmin.getUsername(), user.getUserName(), msg);
        }
        if (user.getStarLevel() == null && user.getCode() == 2) {
            return AjaxUtil.createReturnValueError("请输入要修改的星级数");
        } else if (user.getCode() == 2) {
            operateDesc = AdminOperateUtil.constructMessage("star_level_adjust", currentAdmin.getUsername(), user.getUserName(), user
                    .getStarLevel().toString());
        }
        String operationIp = BaseDwrUtil.getRequestIp();
        // 记录日志
        adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(),
                currentAdmin.getUsername(), user.getId(), user.getUserName(), EAdminOperateLogModel.USER, operateDesc);

        if (!ConstantUtil.SUPER_SYSTEM.equals(currentAdmin.getBizSystem())) {
            user.setBizSystem(currentAdmin.getBizSystem());
        }
        User users = userService.getUserByName(user.getBizSystem(), user.getUserName());
        if (users != null) {
            if (!"".equals(users.getRegfrom()) && users.getRegfrom() != null) {
                user.setUserName("&" + user.getUserName() + "&");
            } else {
                user.setUserName(user.getUserName() + "&");
            }
        }
        int i = userService.updateUserAllLowerLevelsByName(user);
        return AjaxUtil.createReturnValueSuccess(i);
    }

    /**
     * 导入excel会员数据
     *
     * @param user
     * @return
     */
    public Object[] uploadExcelFile(FileTransfer fileTransfer, String bizSystem, String userName) {
        if (StringUtils.isEmpty(userName)) {
            return AjaxUtil.createReturnValueError("参数错误！");
        }

        String currentSystem = BaseDwrUtil.getCurrentAdmin().getBizSystem();
        if (!"SUPER_SYSTEM".equals(currentSystem)) {
            bizSystem = currentSystem;
        }

        User leaderUser = userService.getUserByName(bizSystem, userName);
        if (leaderUser != null) {
            String dailiLevel = leaderUser.getDailiLevel();
            if (dailiLevel.equals(EUserDailLiLevel.REGULARMEMBERS.getCode())) {
                return AjaxUtil.createReturnValueError("请输入代理用户名！");
            }
        } else {
            return AjaxUtil.createReturnValueError("用户名不存在！");
        }

        List<User> userList = new ArrayList<User>();
        // 导入失败人数
        int importFailureCount = 0;
        // 导入成功人数
        int importSuccessCount = 0;
        try {

            WebContext webContext = WebContextFactory.get();
            String realPath = webContext.getServletContext().getRealPath("/upload");

            File fileDir = new File(realPath);
            if (!fileDir.exists()) {
                fileDir.mkdirs();
            }
            String saveFileUrl = realPath + "/" + fileTransfer.getFilename();
            File saveFile = new File(saveFileUrl);
            InputStream uploadFile = fileTransfer.getInputStream();
            int available = uploadFile.available();
            byte[] b = new byte[available];
            FileOutputStream foutput = new FileOutputStream(saveFile);
            uploadFile.read(b);
            foutput.write(b);
            foutput.flush();
            foutput.close();
            uploadFile.close();

            File file = new File(saveFileUrl);
            List<List<Object>> dataList = ImportExcelUtil.importExcel(file);
            for (int i = 0; i < dataList.size(); i++) {
                User user = new User();

                String newUserName = "";
                String newPassword = "";
                String moneyStr = "";
                String newTrueName = "";
                String newSafePassword = "";
                String mobile = "";

                for (int j = 0; j < dataList.get(i).size(); j++) {
                    if (j == 0) {
                        newUserName = (String) dataList.get(i).get(0);
                    } else if (j == 1) {
                        newPassword = (String) dataList.get(i).get(1);
                    } else if (j == 2) {
                        moneyStr = (String) dataList.get(i).get(2);
                    } else if (j == 3) {
                        newTrueName = (String) dataList.get(i).get(3);
                    } else if (j == 4) {
                        newSafePassword = (String) dataList.get(i).get(4);
                    }
                }

                /*
                 * String newUserName = (String) dataList.get(i).get(0); String
                 * newPassword = (String) dataList.get(i).get(1); String
                 * newMoney = (String) dataList.get(i).get(2); String
                 * newTrueName = (String) dataList.get(i).get(3); String
                 * newSafePassword = (String) dataList.get(i).get(4);
                 */

                // 参数验证
                if (StringUtils.isEmpty(newUserName) || StringUtils.isEmpty(newPassword)) {
                    log.info("用户名或者密码不能为空.");
                    importFailureCount++;
                    continue;
                }
                // 用户名只能是数字和字母.
                if (!Pattern.compile(ConstantUtil.USER_NAME_CHAR_FILTER).matcher(newUserName).matches()) {
                    log.info("[" + newUserName + "]用户名只能是数字和字母.");
                    importFailureCount++;
                    continue;
                } else {
                    if (bizSystem.equals("lc")) {
                        if (RegxUtil.isPhoneNumber(newUserName)) {
                            mobile = newUserName;
                            newUserName = "lc" + newUserName;
                        }

                    }
                }

                // 验证余额是否为数字
                BigDecimal newMoney = BigDecimal.ZERO;
                if (moneyStr != null) {
                    boolean isMoney = moneyStr.matches("^[-+]?(([0-9]+)([.]([0-9]+))?|([.]([0-9]+))?)$");
                    if (isMoney) {
                        newMoney = new BigDecimal(moneyStr);
                    } else {
                        log.info("[" + newUserName + "]余额不合法.");
                        importFailureCount++;
                        continue;
                    }
                }

                if (newUserName.length() < 4 || newUserName.length() > 16) {
                    log.info("[" + newUserName + "]用户名长度不正确");
                    importFailureCount++;
                    continue;
                }
                if (!Character.isLetter(newUserName.charAt(0))) {
                    log.info("[" + newUserName + "]用户名首位必须为字母.");
                    importFailureCount++;
                    continue;
                }
                if (newPassword.length() < 6 || newPassword.length() > 15) {
                    log.info("[" + newUserName + "]密码长度不正确.");
                    importFailureCount++;
                    continue;
                }
                if (newUserName.contains(ConstantUtil.REG_FORM_SPLIT)) {
                    log.info("[" + newUserName + "]用户名字符不合法.");
                    importFailureCount++;
                    continue;
                }

                user.setBizSystem(bizSystem);
                user.setUserName(newUserName);
                user.setPassword(newPassword);
                user.setMoney(newMoney);
                user.setTrueName(newTrueName);
                if (bizSystem.equals("lc")) {
                    user.setPhone(mobile);
                }
                if (newSafePassword != null && !"".equals(newSafePassword)) {
                    String safePassword = MD5PasswordUtil.GetMD5Code(newSafePassword);
                    user.setSafePassword(safePassword);
                    // 设置是否有安全密码为1.(1表示已经设置,0表示没有设置).
                    user.setHaveSafePassword(1);
                }

                user.setSscRebate(AwardModelUtil.getModelBySsc(leaderUser.getSscRebate(), ELotteryTopKind.SSC));
                user.setFfcRebate(AwardModelUtil.getModelBySsc(leaderUser.getSscRebate(), ELotteryTopKind.FFC));
                user.setKsRebate(AwardModelUtil.getModelBySsc(leaderUser.getSscRebate(), ELotteryTopKind.FFC));
                user.setTbRebate(AwardModelUtil.getModelBySsc(leaderUser.getSscRebate(), ELotteryTopKind.TB));
                user.setLhcRebate(AwardModelUtil.getModelBySsc(leaderUser.getSscRebate(), ELotteryTopKind.LHC));
                user.setKlsfRebate(AwardModelUtil.getModelBySsc(leaderUser.getSscRebate(), ELotteryTopKind.KLSF));
                user.setSyxwRebate(AwardModelUtil.getModelBySsc(leaderUser.getSscRebate(), ELotteryTopKind.SYXW));
                user.setPk10Rebate(AwardModelUtil.getModelBySsc(leaderUser.getSscRebate(), ELotteryTopKind.PK10));
                user.setDpcRebate(AwardModelUtil.getModelBySsc(leaderUser.getSscRebate(), ELotteryTopKind.DPC));
                user.setDailiLevel(EUserDailLiLevel.REGULARMEMBERS.getCode());
                user.setRegfrom(leaderUser.getRegfrom() + leaderUser.getUserName() + ConstantUtil.REG_FORM_SPLIT); // 设置推荐人

                if (user.getSscRebate().divideAndRemainder(new BigDecimal("2"))[1].compareTo(BigDecimal.ZERO) != 0) {
                    return AjaxUtil.createReturnValueError("模式值只能输入偶数！");
                }
                if (user.getUserName().trim().indexOf("GUEST_") == 0 && leaderUser.getIsTourist() == 1) {
                    return AjaxUtil.createReturnValueError("用户名不能以‘GUEST_’开头，请更换用户名");
                }
                if (leaderUser.getIsTourist() == 1) {
                    user.setIsTourist(1);
                }
                // 用户名是否重复的校验
                User tempuser = userService.getUserByName(user.getBizSystem(), user.getUserName());
                if (tempuser != null) {
                    log.info("[" + user.getUserName() + "]该用户名已经被注册了.");
                    importFailureCount++;
                    continue;
                }

                userList.add(user);

            }

        } catch (IOException e) {
            log.error("导入失败.", e);
            return AjaxUtil.createReturnValueError("导入失败.");
        }

        try {
            Iterator<User> iterator = userList.iterator();
            while (iterator.hasNext()) {
                User user = iterator.next();
                // 添加用户
                userService.saveUser(user);
                importSuccessCount++;
                // 更新团队人数
                User dealUser = null;
                String[] regFroms = user.getRegfrom().split(ConstantUtil.REG_FORM_SPLIT);
                for (int i = 0; i < regFroms.length; i++) {
                    User regFromUser = userService.getUserByName(leaderUser.getBizSystem(), regFroms[i]);
                    if (regFromUser != null) {
                        dealUser = new User();
                        dealUser.setId(regFromUser.getId());
                        dealUser.setTeamMemberCount(regFromUser.getTeamMemberCount());
                        dealUser.setDownMemberCount(regFromUser.getDownMemberCount());
                        if (i == (regFroms.length - 1)) {
                            dealUser.setTeamMemberCount(dealUser.getTeamMemberCount() + 1);
                            dealUser.setDownMemberCount(dealUser.getDownMemberCount() + 1);
                        } else {
                            dealUser.setTeamMemberCount(dealUser.getTeamMemberCount() + 1);
                        }
                        userService.updateByPrimaryKeySelective(dealUser);
                    }
                }
            }
        } catch (Exception e) {
            log.error("添加用户出错", e);
            return AjaxUtil.createReturnValueError("添加用户出错");
        }
        return AjaxUtil.createReturnValueSuccess("成功导入【" + importSuccessCount + "】人，导入失败【" + importFailureCount + "】人");
    }

    /**
     * 批量用户续费或者扣费
     *
     * @param user
     * @return
     */
    public Object[] userRenewList(User user, String operatePassword, String description) {
        if (user == null || StringUtils.isEmpty(user.getOperationType()) || StringUtils.isEmpty(user.getOperationValue())
                || StringUtils.isEmpty(operatePassword)) {
            return AjaxUtil.createReturnValueError("参数传递错误.");
        }
        BigDecimal operateValue = new BigDecimal(user.getOperationValue());
        if (operateValue.compareTo(new BigDecimal(0)) <= 0) {
            return AjaxUtil.createReturnValueError("金额必须大于0.");
        }
        Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
        Admin dbAdmin = adminService.selectByPrimaryKey(currentAdmin.getId());

        // 当管理员操作管理员密码错误时超过规定的次数的时候进行锁定处理.
        String loginIp = BaseDwrUtil.getRequestIp();
        Date nowDate = new Date();
        Date nowDateStart = DateUtil.getNowStartTimeByStart(nowDate);
        Date nowDateEnd = DateUtil.getNowStartTimeByEnd(nowDate);
        // 新建查询对象用于查询密码错误次数.
        AdminPasswordErrorLogQuery query = new AdminPasswordErrorLogQuery();
        query.setAdminId(currentAdmin.getId());
        query.setBizSystem(currentAdmin.getBizSystem());
        query.setErrorType(EAdminPasswordErrorType.OPERATE_PASSWORD_ERROR.getCode());
        query.setLoginIp(loginIp);
        query.setIsDeleted(0);
        query.setCreatedDateStart(nowDateStart);
        query.setCreatedDateEnd(nowDateEnd);

        int errorCount = adminPasswordErrorLogService.getAllAdminPasswordErrorLogByQueryPageCount(query);
        if (errorCount >= SystemConfigConstant.maxLoginCount) {
            log.info("IP[" + loginIp + "]，管理员[" + dbAdmin.getUsername() + "]操作续费的时候,密码错误" + SystemConfigConstant.maxLoginCount + "次，锁定该用户");
            Admin updateAdmin = new Admin();
            updateAdmin.setId(dbAdmin.getId());
            updateAdmin.setLoginLock(1);
            adminService.updateByPrimaryKeySelective(updateAdmin);
            adminPasswordErrorLogService.updateAdminPasswordErrorLogByErrorType(ConstantUtil.SUPER_SYSTEM, loginIp, dbAdmin.getUsername(),
                    EAdminPasswordErrorType.PASSWORD_ERROR.getCode());
            // 当前用户密码错误失败过多，用户被锁定，请联系客服解锁
            return AjaxUtil.createReturnValueError("当前管理员输入密码错误次数过多，已被锁定");
        }
        // 新建密码错误查询对象.
        // 校验操作密码
        if (dbAdmin != null && !MD5PasswordUtil.GetMD5Code(operatePassword).equals(dbAdmin.getOperatePassword())) {
            // 数据库新增密码错误对象,用于锁定管理员.
            AdminPasswordErrorLog adminPasswordErrorLog = new AdminPasswordErrorLog();
            adminPasswordErrorLog.setAdminId(currentAdmin.getId());
            adminPasswordErrorLog.setAdminName(currentAdmin.getUsername());
            adminPasswordErrorLog.setBizSystem(currentAdmin.getBizSystem());
            adminPasswordErrorLog.setCreatedDate(new Date());
            adminPasswordErrorLog.setErrorType(EAdminPasswordErrorType.OPERATE_PASSWORD_ERROR.getCode());
            adminPasswordErrorLog.setIsDeleted(0);
            adminPasswordErrorLog.setLoginIp(loginIp);
            adminPasswordErrorLog.setSourceType(EAdminPasswordErrorLog.ADMIN_OPERATION_USER_PWD_ERROR.getCode());
            adminPasswordErrorLog.setSourceDes(EAdminPasswordErrorLog.ADMIN_OPERATION_USER_PWD_ERROR.getDescription());
            adminPasswordErrorLogService.insertSelective(adminPasswordErrorLog);
            return AjaxUtil.createReturnValueError("当前管理员操作密码错误,错误超过" + SystemConfigConstant.maxLoginCount + "次,将被锁定!!!");
        } else {
            adminPasswordErrorLogService.updateAdminPasswordErrorLogByErrorType(currentAdmin.getBizSystem(), loginIp,
                    currentAdmin.getUsername(), EAdminPasswordErrorType.OPERATE_PASSWORD_ERROR.getCode());
        }
        if (!currentAdmin.getBizSystem().equals(ConstantUtil.SUPER_SYSTEM)) {
            user.setBizSystem(currentAdmin.getBizSystem());
        }
        // 用于放置不存在用户
        String errorName = "";
        // 分解多个用户
        String[] name = user.getUserName().split(",");
        // 再一次验证会员账号重复问题
        String ary = user.getUserName();
        for (int i = 0; i < user.getUserName().split(",").length; i++) {
            if (ary.replace(user.getUserName().split(",")[i] + ",", "").indexOf(user.getUserName().split(",")[i] + ",") > -1
                    && user.getUserName().split(",")[i] != "") {
                return AjaxUtil.createReturnValueError("存在重复的会员账号，请重新输入");
            }
        }
        // 循环判断用户是否存在，不存在就存入errorName中用于提示
        for (int i = 0; i < name.length; i++) {
            User queryUser = userService.getUserByName(user.getBizSystem(), name[i]);
            if (queryUser == null) {
                errorName += name[i] + ",";
            } else {
                if (queryUser.getIsTourist() == 1) {
                    errorName += name[i] + ",";
                }
            }
        }
        if (errorName != "") {
            return AjaxUtil.createReturnValueError("用户名：" + errorName + "不存在");
        }
        // 循环操作多个用户
        for (int i = 0; i < name.length; i++) {
            User queryUser = new User();
            try {
                // 查询用户获取其ID
                queryUser = userService.getUserByName(user.getBizSystem(), name[i]);
                userService.userRenew(queryUser.getId(), user.getOperationType(), user.getOperationValue(), description, currentAdmin);
            } catch (UnEqualVersionException e) {
                log.error(e.getMessage(), e);
                return AjaxUtil.createReturnValueError("当前系统繁忙，请稍后重试");
            } catch (Exception e) {
                return AjaxUtil.createReturnValueError(e.getMessage());
            }

        }
        // 操作日志保存
        String operationTypeName = getOperationTypeName(user.getOperationType());
        if (!StringUtils.isEmpty(operationTypeName)) { // 类型名称不为空
            String operateDesc = AdminOperateUtil.constructMessage("xufei_log", currentAdmin.getUsername(), operationTypeName,
                    user.getUserName(), operateValue.setScale(3).toString());
            String operationIp = BaseDwrUtil.getRequestIp();
            adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(),
                    currentAdmin.getUsername(), user.getId(), user.getUserName(), EAdminOperateLogModel.USER_XUFEI, operateDesc);
        }
        return AjaxUtil.createReturnValueSuccess();
    }

    /**
     * 解锁或者锁定银行卡
     *
     * @return
     */
    public Object[] lockStateCard(Long id, Integer lock) {
        UserBank userBank = userBankService.selectByPrimaryKey(id);
        if (userBank != null) {

            Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
            String operateDesc = AdminOperateUtil.constructMessage("lockStateCard_log", currentAdmin.getUsername(), userBank.getUserName(),
                    userBank.getBankAccountName(), userBank.getBankCardNum());
            String operationIp = BaseDwrUtil.getRequestIp();
            // 操作日志保存
            adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(),
                    currentAdmin.getUsername(), userBank.getUserId(), userBank.getUserName(), EAdminOperateLogModel.PAYBANK, operateDesc);

            // 发送邮件通知，放置到消息队列中
            /*
             * SendMailMessage mailMessage = new SendMailMessage();
             * mailMessage.setSubject("系统告警(用户银行卡编辑告警)");
             * mailMessage.setContent(operateDesc);
             * MessageQueueCenter.putMessage(mailMessage);
             */

            // 发送短信通知
            /*
             * String result = SmsUtil.SendSms(operateDesc);
             * if("error".equals(result)) { return
             * AjaxUtil.createReturnValueError("系统错误，请重试"); }
             */

            userBank.setLocked(lock);
            userBankService.updateByPrimaryKeySelective(userBank);
        }

        return AjaxUtil.createReturnValueSuccess();
    }

}
