package com.team.lottery.action.web.manage;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.enums.EAdminOperateLogModel;
import com.team.lottery.enums.EMeasureType;
import com.team.lottery.service.AdminOperateLogService;
import com.team.lottery.service.UserBonusConfigService;
import com.team.lottery.util.AdminOperateUtil;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.UserBonusConfig;

@Controller("managerUserBonusConfigAction")
public class ManagerUserBonusConfigAction {
	
	private static Logger logger = LoggerFactory.getLogger(ManagerUserBonusConfigAction.class);

	@Autowired
	private UserBonusConfigService userBonusConfigService;
	@Autowired
	private AdminOperateLogService adminOperateLogService;
	
    /**
     * 查找所有的分红配置
     * @return
     */
    public Object[] getAllUserBonusConfigs(){
		List<UserBonusConfig> bonusConfigs =  userBonusConfigService.getAllUserBonusConfigs();
		return AjaxUtil.createReturnValueSuccess(bonusConfigs);
    }
    
    /**
     * 查找指定的分红配置
     * @return
     */
    public Object[] getUserBonusConfigById(Long id){
    	UserBonusConfig bonusConfig =  userBonusConfigService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(bonusConfig);
    }
    
    /**
     * 删除指定的分红配置
     * @return
     */
    public Object[] delUserBonusConfig(Long id){
    	//保存操作日志
    	try {
    		UserBonusConfig bonusConfig = userBonusConfigService.selectByPrimaryKey(id);
    		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    		String measureType = EMeasureType.valueOf(bonusConfig.getMeasureType()).getDescription();
			String operateDesc = AdminOperateUtil.constructMessage("treatment_bonus_config_del_log", currentAdmin.getUsername(), measureType,
					bonusConfig.getDailiLevelStr(), String.valueOf(bonusConfig.getScale()), String.valueOf(bonusConfig.getScale2()));
			String operationIp = BaseDwrUtil.getRequestIp();
			adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.BONUS_CONFIG, operateDesc);
		} catch (Exception e) {
			//保存日志出现异常 不进行处理
			logger.error("新增分红配置，保存添加操作日志出错", e);
		}
    	userBonusConfigService.deleteByPrimaryKey(id);
    	return AjaxUtil.createReturnValueSuccess();
    }
    
   
    /**
     * 保存分红配置
     * @return
     */
    public Object[] saveOrUpdateUserBonusConfig(UserBonusConfig bonusConfig){
    	try{
    		Long id = bonusConfig.getId();
        	if(id==null){
        		//保存操作日志
            	try {
            		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
            		String measureType = EMeasureType.valueOf(bonusConfig.getMeasureType()).getDescription();
        			String operateDesc = AdminOperateUtil.constructMessage("treatment_bonus_config_add_log", currentAdmin.getUsername(), measureType,
        					bonusConfig.getDailiLevelStr(), String.valueOf(bonusConfig.getScale()), String.valueOf(bonusConfig.getScale2()));
        			String operationIp = BaseDwrUtil.getRequestIp();
        			adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.BONUS_CONFIG, operateDesc);
        		} catch (Exception e) {
        			//保存日志出现异常 不进行处理
        			logger.error("删除分红配置，保存添加操作日志出错", e);
        		}
            	
        		userBonusConfigService.insertSelective(bonusConfig);
        	}else{
        		//保存操作日志
            	try {
            		UserBonusConfig oldUserBonusConfig = userBonusConfigService.selectByPrimaryKey(id);
            		//拼接修改字符串
            		StringBuffer itemChangeLogDesc = new StringBuffer();
            		if(!oldUserBonusConfig.getDailiLevel().equals(bonusConfig.getDailiLevel())) {
            			itemChangeLogDesc.append(AdminOperateUtil.constructMessage("treatment_bonus_config_item_update_log", "用户代理级别", oldUserBonusConfig.getDailiLevelStr(), bonusConfig.getDailiLevel()));
            		}
            		if(oldUserBonusConfig.getMeasurement().compareTo(bonusConfig.getMeasurement()) != 0) {
            			itemChangeLogDesc.append(AdminOperateUtil.constructMessage("treatment_bonus_config_item_update_log", "量值", String.valueOf(oldUserBonusConfig.getMeasurement().setScale(3)), String.valueOf(bonusConfig.getMeasurement().setScale(3))));
            		}
            		if(oldUserBonusConfig.getScale().compareTo(bonusConfig.getScale()) != 0) {
            			itemChangeLogDesc.append(AdminOperateUtil.constructMessage("treatment_bonus_config_item_update_log", "实际值", String.valueOf(oldUserBonusConfig.getScale().setScale(3)), String.valueOf(bonusConfig.getScale().setScale(3))));
            		}
            		if(oldUserBonusConfig.getScale2() != null && bonusConfig.getScale2() != null && oldUserBonusConfig.getScale2().compareTo(bonusConfig.getScale2()) != 0) {
            			itemChangeLogDesc.append(AdminOperateUtil.constructMessage("treatment_bonus_config_item_update_log", "实际值2", String.valueOf(oldUserBonusConfig.getScale2().setScale(3)), String.valueOf(bonusConfig.getScale2().setScale(3))));
            		}
            		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
            		String measureType = EMeasureType.valueOf(bonusConfig.getMeasureType()).getDescription();
        			String operateDesc = AdminOperateUtil.constructMessage("treatment_bonus_config_update_log", currentAdmin.getUsername(), measureType, itemChangeLogDesc.toString());
        			String operationIp = BaseDwrUtil.getRequestIp();
        			adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.BONUS_CONFIG, operateDesc);
        		} catch (Exception e) {
        			//保存日志出现异常 不进行处理
        			logger.error("更新分红配置，保存添加操作日志出错", e);
        		}
        		
        		userBonusConfigService.updateByPrimaryKeySelective(bonusConfig);
        	}
    		return AjaxUtil.createReturnValueSuccess();
    	}catch(Exception e){
    		return AjaxUtil.createReturnValueError(e.getMessage());
    	}
    }
    
    
}
