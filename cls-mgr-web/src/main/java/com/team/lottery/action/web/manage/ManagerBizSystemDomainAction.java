package com.team.lottery.action.web.manage;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.enums.EAdminOperateLogModel;
import com.team.lottery.enums.ERedisChannel;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.AdminOperateLogService;
import com.team.lottery.service.BizSystemDomainService;
import com.team.lottery.system.message.MessageQueueCenter;
import com.team.lottery.system.message.messagetype.BizSystemDomainMessage;
import com.team.lottery.util.AdminOperateUtil;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.BizSystemDomain;

@Controller("managerBizSystemDomainAction")
public class ManagerBizSystemDomainAction {

	@Autowired
	private BizSystemDomainService bizSystemDomainService;
	@Autowired
	private AdminOperateLogService adminOperateLogService;
	
    /**
     * 查询
     * @return
     */
    public Object[] getAllBizSystemDomain(BizSystemDomain query,Page page){
    	if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	if(!"SUPER_SYSTEM".equals(currentAdmin.getBizSystem()))
    	{
    		query.setBizSystem(currentAdmin.getBizSystem());
    	}
    	
    	page=bizSystemDomainService.getAllBizSystemByQueryPage(query, page);
		return AjaxUtil.createReturnValueSuccess(page);
    }

    
    /**
     * 删除
     * @return
     */
    public Object[] delBizSystemDomainById(Long id){
    	BizSystemDomain bizSystemDomain=null;
    	try{
    		 bizSystemDomain=bizSystemDomainService.selectByPrimaryKey(id);
    		 Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    		 //保存日志
			 String operateDesc = AdminOperateUtil.constructMessage("domain_del", currentAdmin.getUsername(), bizSystemDomain.getDomainUrl());
			 String operationIp = BaseDwrUtil.getRequestIp();
			 adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.DOMAIN, operateDesc);
			 
	    	 int result=bizSystemDomainService.deleteByPrimaryKey(id);
	    	 if(result<=0)
	         {
	         return AjaxUtil.createReturnValueError("删除失败！");
	         }
    	}catch(Exception e){
        	return AjaxUtil.createReturnValueError(e.getMessage());
    	}
    	 try {
		    	 //把更新缓存操作，放置到消息队列中
		        BizSystemDomainMessage bizSystemDomainMessage=new BizSystemDomainMessage();
		  
		        bizSystemDomainMessage.setBizSystemDomain(bizSystemDomain);
		        bizSystemDomainMessage.setOperateType("-1");
//		        bizSystemDomainMessage.setType("-1");
//		        bizSystemDomainMessage.setData("1");
		        MessageQueueCenter.putRedisMessage(bizSystemDomainMessage, ERedisChannel.CLSCACHE.name());
		} catch (Exception e) {
			
		}
        
       
        
    	return AjaxUtil.createReturnValueSuccess();
    }
    
    /**
     * 新增和修改
     * @param systemConfig
     * @return
     */
    public Object[] saveOrUpdateBizSystenDomain(BizSystemDomain bizSystemDomain)
    {
    	try{
    	if(bizSystemDomain == null){
    		return AjaxUtil.createReturnValueError("参数传递错误");
    	}
    	if(bizSystemDomain.getId()!=null&&!"".equals(bizSystemDomain.getId()))
    	{
    		if(bizSystemDomain.getJumpMobileDomainUrl()==null){
    			bizSystemDomain.setJumpMobileDomainUrl("");
    		}
    		return this.updateBizSystemDomain(bizSystemDomain);
    	}else{
    		return this.addBizSystemDomain(bizSystemDomain);
    	}
    	}catch(Exception e){
        	return AjaxUtil.createReturnValueError(e.getMessage());
    	}
    }
    
    /**
     * 新增
     * @param bizSystem
     * @return
     */
    public Object[] addBizSystemDomain(BizSystemDomain bizSystemDomain)
    {
    	if(bizSystemDomain == null){
    		return AjaxUtil.createReturnValueError("参数传递错误");
    	}
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	
    	if(bizSystemDomain.getApplyStatus()==null)
    	{
    		bizSystemDomain.setApplyStatus(0);
    	}
    	bizSystemDomain.setCreateTime(new Date());
    	bizSystemDomain.setUpdateTime(new Date());
    	bizSystemDomain.setUpdateAdmin(currentAdmin.getUsername());;
    	 //保存日志
    	String operateDesc = AdminOperateUtil.constructMessage("domain_add", currentAdmin.getUsername(), bizSystemDomain.getDomainUrl());
    	String operationIp = BaseDwrUtil.getRequestIp();
    	adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.DOMAIN, operateDesc);
    
		int result=bizSystemDomainService.insertSelective(bizSystemDomain);
        if(result>0)
        {
        	//状态为通过，然后是启用的放入缓存中
        	if(bizSystemDomain.getApplyStatus()==1&&bizSystemDomain.getEnable()==1){	
        	    //把更新缓存操作，放置到消息队列中
	        	bizSystemDomain=bizSystemDomainService.getAllBizSystemDomain(bizSystemDomain).get(0);
		       try{ 
		        	BizSystemDomainMessage bizSystemDomainMessage=new BizSystemDomainMessage();
			       
			        bizSystemDomainMessage.setBizSystemDomain(bizSystemDomain);;
			        bizSystemDomainMessage.setOperateType("1");
//			        bizSystemDomainMessage.setType("-1");
//			        bizSystemDomainMessage.setData("1");
			        MessageQueueCenter.putRedisMessage(bizSystemDomainMessage, ERedisChannel.CLSCACHE.name());
				} catch (Exception e) {
					
				}
        	 }
            return AjaxUtil.createReturnValueSuccess();
        }else{
          return AjaxUtil.createReturnValueError("添加失败，请重新添加");
        }
		//return AjaxUtil.createReturnValueSuccess();
    }
    /**
     * 修改
     * @param bizSystem
     * @return
     * @throws Exception 
     */
    public Object[] updateBizSystemDomain(BizSystemDomain bizSystemDomain) throws Exception
    {
    	if(bizSystemDomain == null){
    		return AjaxUtil.createReturnValueError("参数传递错误");
    	}
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	bizSystemDomain.setUpdateTime(new Date());
    	bizSystemDomain.setUpdateAdmin(currentAdmin.getUsername());
    	 //保存日志
    	String operateDesc = AdminOperateUtil.constructMessage("domain_edit", currentAdmin.getUsername(), bizSystemDomain.getDomainUrl());
    	String operationIp = BaseDwrUtil.getRequestIp();
    	adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.DOMAIN, operateDesc);
    	
    	int result=bizSystemDomainService.updateByPrimaryKeySelective(bizSystemDomain);
    	 if(result>0){
    		   try{ 
		        	BizSystemDomainMessage bizSystemDomainMessage=new BizSystemDomainMessage();
			        
			        bizSystemDomainMessage.setBizSystemDomain(bizSystemDomain);;
			        bizSystemDomainMessage.setOperateType("0");
//			        bizSystemDomainMessage.setType("-1");
//			        bizSystemDomainMessage.setData("1");
			        MessageQueueCenter.putRedisMessage(bizSystemDomainMessage, ERedisChannel.CLSCACHE.name());
				} catch (Exception e) {
					
				}
            return AjaxUtil.createReturnValueSuccess();
         }else{
            return AjaxUtil.createReturnValueError("添加失败，请重新添加");
         }
    }
    /**
     * 根据id获取对象
     * @param id
     * @return
     */
    public Object[] getBizSystemDomainById(Long id)
    {
    	if(id == null){
    		return AjaxUtil.createReturnValueError("参数传递错误");
    	}
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	BizSystemDomain bizSystemDomain=bizSystemDomainService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(bizSystemDomain,currentAdmin);
    }
    
    
    /**
     * 获取对象当前用户
     * @param id
     * @return
     */
    public Object[] getCurrentAdmin()
    {
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		return AjaxUtil.createReturnValueSuccess(currentAdmin);
    }
    
    /**
     *根据查询条件查询所有记录
     * @param id
     * @return
     */
    public Object[] selectAllBizSystemDomain(BizSystemDomain query)
    {
    	List<BizSystemDomain> list=bizSystemDomainService.getAllBizSystemDomain(query);
		return AjaxUtil.createReturnValueSuccess(list);
    }
    
    /**
     * 更新缓存
     * @return
     * 
     */
    public Object[] updatecache(){
    	try {
    		BizSystemDomainMessage bizSystemDomainMessage=new BizSystemDomainMessage();
            bizSystemDomainMessage.setType("2");
            MessageQueueCenter.putRedisMessage(bizSystemDomainMessage, ERedisChannel.CLSCACHE.name());
        	return AjaxUtil.createReturnValueSuccess();
		} catch (Exception e) {
			return AjaxUtil.createReturnValueError();
		}
    }
}
