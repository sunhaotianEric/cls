package com.team.lottery.action.web.manage;

import java.text.ParseException;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.Page;
import com.team.lottery.service.ChatUserSendredbagAuthService;
import com.team.lottery.service.UserService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.ChatUserSendredbagAuth;
import com.team.lottery.vo.User;

@Controller("managerChatUserSendredbagAuthAction")
public class ManagerChatUserSendredbagAuthAction {

	@Autowired
	private ChatUserSendredbagAuthService chatUserSendredbagAuthService;
	@Autowired
	private UserService  userService;
	
	/**
	 * 分页查询
	 * @param record
	 * @param page
	 * @return
	 * @throws ParseException 
	 */
	public Object[] getChatUserSendredbagAuthPage(ChatUserSendredbagAuth record,Page page){
		if(record==null||page==null){
			return AjaxUtil.createReturnValueError("参数传递错误");
		}
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		if(!"SUPER_SYSTEM".equals(currentAdmin.getBizSystem())&&record.getBizSystem()==null){
			record.setBizSystem(currentAdmin.getBizSystem());
		}
		page=chatUserSendredbagAuthService.getChatUserSendredbagAuthPage(record, page);
		return AjaxUtil.createReturnValueSuccess(page);
	}
	
    /**
     * 添加修改
     * @return
     */
    public Object[] saveOrUpdateChatUserSendredbagAuth(ChatUserSendredbagAuth record){
    	
    	if(record==null){
			return AjaxUtil.createReturnValueError("参数传递错误");
		}
    	if(record.getUserName() ==  null ||"".equals(record.getUserName()) || record.getAllowSendMoney() == null ||"".equals(record.getAllowSendMoney()) || record.getRoomid() == null ||"".equals(record.getRoomid()) ){
			return AjaxUtil.createReturnValueError("参数传递错误");
		}
    	try{
    		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    		if(!"SUPER_SYSTEM".equals(currentAdmin.getBizSystem())){
    			record.setBizSystem(currentAdmin.getBizSystem());
    		}
    		//判断用户是否重复添加
    		User user = userService.getUserByName(record.getBizSystem(), record.getUserName());
    		if(user == null){
    			return AjaxUtil.createReturnValueError("请输入正确的用户名!");
    		}
    		int conut = chatUserSendredbagAuthService.getChatUserSendredbagAuthCountForUpdate(record);
			if(conut > 0){
				return AjaxUtil.createReturnValueError("已经添加聊天"+record.getRoomid()+"的用户"+record.getUserName()+"权限,不用重复添加!");
			}
    		if(record.getId()!=null){
    			
    			record.setUpdateAdmin(currentAdmin.getUsername());
    			record.setUpdateDate(new Date());
    			chatUserSendredbagAuthService.updateByPrimaryKeySelective(record);
    		}else{
    			record.setUpdateAdmin(currentAdmin.getUsername());
    			record.setCreateDate(new Date());
    			chatUserSendredbagAuthService.insertSelective(record);
    		}
    	}catch(Exception e){
        	return AjaxUtil.createReturnValueError(e.getMessage());
    	}
    	
    	return AjaxUtil.createReturnValueSuccess();
    }
    
    /**
     * 删
     * @return
     */
    public Object[] selectById(Integer id){
    	ChatUserSendredbagAuth chatUserSendredbagAuth =  null;
    	try{
    		chatUserSendredbagAuth = chatUserSendredbagAuthService.selectByPrimaryKey(id);
    	}catch(Exception e){
        	return AjaxUtil.createReturnValueError(e.getMessage());
    	} 
    	return AjaxUtil.createReturnValueSuccess(chatUserSendredbagAuth);
    }
    /**
     * 删
     * @return
     */
    public Object[] deleteById(Integer id){
    	try{
    		int result = chatUserSendredbagAuthService.deleteByPrimaryKey(id);
    		if(result > 0){
    			return AjaxUtil.createReturnValueSuccess();
    		}else{
    			return AjaxUtil.createReturnValueError();
    		}	
    	}catch(Exception e){
        	return AjaxUtil.createReturnValueError(e.getMessage());
    	}    	
    }
}
