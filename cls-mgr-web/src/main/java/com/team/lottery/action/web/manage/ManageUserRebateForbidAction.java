package com.team.lottery.action.web.manage;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.Page;
import com.team.lottery.service.UserRebateForbIdService;
import com.team.lottery.service.UserService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserRebateForbid;

@Controller("manageUserRebateForbidAction")
public class ManageUserRebateForbidAction {
	
	@Autowired
	public UserRebateForbIdService userRebateForbIdService;
	@Autowired
	public UserService userService;
	
	
	/**
	 * 分页查询返点禁止名单
	 * @param bizSystem
	 * @param userName
	 * @param page
	 * @return
	 */
	public Object[] getAllUserRebateForbid (String bizSystem,String userName, Page page){
		if(page == null){
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		Admin currentUser = BaseDwrUtil.getCurrentAdmin();
      	String currentBizSystem = currentUser.getBizSystem();
      	   	if(bizSystem == null || "".equals(bizSystem)){
    		if("SUPER_SYSTEM".equals(bizSystem)){
    			bizSystem = currentBizSystem;
    		}
    	}
		Page allUserRebateForbid = userRebateForbIdService.getAllUserRebateForbid(bizSystem, userName,page);
		
		return AjaxUtil.createReturnValueSuccess(allUserRebateForbid);
	}
	
	/**
	 * 保存返点禁止名单
	 * @param userRebateForbid
	 * @return
	 */
	public Object[] saveUserRebateForbid(UserRebateForbid userRebateForbid){
		if(userRebateForbid == null){
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		
		Integer id = userRebateForbid.getId();
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		if(!currentAdmin.getBizSystem().equals("SUPER_SYSTEM")){
			userRebateForbid.setBizSystem(currentAdmin.getBizSystem());
		}
		
		int successCount = 0;
		String failName = "";
		if(id == null){
			String userNames = userRebateForbid.getUserName();
			String[] allUserName = userNames.split(",");
			for(int i=0;i<allUserName.length;i++){
				String name = allUserName[i];
				User user = userService.getUserByName(userRebateForbid.getBizSystem(), name);
				if(user != null){
					UserRebateForbid userRebateForbidByUserId = userRebateForbIdService.getUserRebateForbidByUserId(user.getId());
					//只保存未保存的名单
					if(userRebateForbidByUserId == null){
						userRebateForbid.setUserId(user.getId());
						userRebateForbid.setCreateTime(new Date());
						userRebateForbid.setUserName(name);
						userRebateForbid.setCreateAdmin(currentAdmin.getUsername());
						userRebateForbIdService.insertSelective(userRebateForbid);
						successCount ++;
					}else{
						 failName += ""+ name + ",";
					}
				}else{
					 failName += name + ",";
				}
			}
			String failNames = failName == ""? "" :  ",添加失败名单:"+failName.substring(0,failName.length()-1);
			return AjaxUtil.createReturnValueSuccess("成功添加" +successCount+"人"+failNames);
		}else{
			userRebateForbid.setCreateTime(new Date());
			userRebateForbid.setCreateAdmin(currentAdmin.getUsername());
			successCount = userRebateForbIdService.updateByPrimaryKeySelective(userRebateForbid);
			return AjaxUtil.createReturnValueSuccess("成功更新" +successCount+"人");
		}
	}
	
	public Object[] getUserRebateForbid(Integer id){
		UserRebateForbid userRebateForbid = userRebateForbIdService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(userRebateForbid);
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	  public Object[] delUserRebateForbid(Integer id){
		  userRebateForbIdService.deleteByPrimaryKey(id);
		  
		  return AjaxUtil.createReturnValueSuccess();
	  }
}
