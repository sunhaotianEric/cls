package com.team.lottery.action.web.manage;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.enums.ERedisChannel;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.ChatBanService;
import com.team.lottery.service.ChatRoomService;
import com.team.lottery.system.message.MessageQueueCenter;
import com.team.lottery.system.message.messagetype.ChatBanMessage;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.ChatBan;
import com.team.lottery.vo.ChatRoom;

@Controller("managerChatBanAction")
public class ManagerChatBanAction {

	/*
	 * private static Logger logger =
	 * LoggerFactory.getLogger(ManagerChatBanAction.class);
	 */

	@Autowired
	private ChatBanService chatBanService;
	@Autowired
	private ChatRoomService chatRoomService;

	/**
	 * 查找所有
	 * 
	 * @return
	 */
	public Object[] getAllChatBan(ChatBan query, Page page) {
		if(query==null||page==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		page = chatBanService.getAllChatBanQueryPage(query, page);
		return AjaxUtil.createReturnValueSuccess(page);
	}

	/**
	 * 添加
	 * 
	 * @return
	 */
	public Object[] saveOrUpdateChatBan(ChatBan record) {
		 String str = null;
		try {
			Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
			if (!"SUPER_SYSTEM".equals(currentAdmin.getBizSystem())) {
				record.setBizSystem(currentAdmin.getBizSystem());
			}
			record.setOperator(currentAdmin.getUsername());
			if (record.getId() != null) {
				record.setUpdateDate(new Date());
				chatBanService.updateByPrimaryKeySelective(record);
			} else {
				record.setCreateDate(new Date());
				chatBanService.insertSelective(record);
			}
			//发送 websocket 字符串
			ChatBanMessage chatBanMessage=new ChatBanMessage();
			chatBanMessage.setType("0");
			chatBanMessage.setChatBan(record);
			MessageQueueCenter.putRedisMessage(chatBanMessage, ERedisChannel.CLSCACHE.name());
		} catch (Exception e) {
			return AjaxUtil.createReturnValueError(e.getMessage());
		}
		return AjaxUtil.createReturnValueSuccess(str);
	}

	/**
	 * 删除收款银行
	 * 
	 * @return
	 */
	public Object[] delChatBan(Integer id) {
		int result = 0;
		ChatBan record=chatBanService.selectByPrimaryKey(id);
		try {
			result = chatBanService.deleteByPrimaryKey(id);
		} catch (Exception e) {
			return AjaxUtil.createReturnValueError(e.getMessage());
		}
		if(result > 0){
			ChatBanMessage chatBanMessage=new ChatBanMessage();
			chatBanMessage.setType("-1");
			chatBanMessage.setChatBan(record);
			MessageQueueCenter.putRedisMessage(chatBanMessage, ERedisChannel.CLSCACHE.name());
		}
		
		return AjaxUtil.createReturnValueSuccess(record);
	}

	public Object[] getChatBanById(Integer id) {

		ChatBan chatBan = chatBanService.selectByPrimaryKey(id);

		return AjaxUtil.createReturnValueSuccess(chatBan);
	}

	public Object[] getChatRoomBizSystem(String bizSystem) {
		ChatRoom chatRoom = new ChatRoom();
		chatRoom.setBizSystem(bizSystem);
		List<ChatRoom> list = chatRoomService.getChatRoomList(chatRoom);

		return AjaxUtil.createReturnValueSuccess(list);
	}
	
	/**
	 * 根据ID查询
	 * @param id
	 * @return
	 */
	public Object[] selectChatBan(Integer id){
		ChatBan record=chatBanService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(record);
	}
	
	/**
	 * 修改
	 * @param record
	 * @return
	 */
/*	public Object[] updateFeedBack(FeedBack record){
		if(record==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		feedbackService.updateByPrimaryKeySelective(record);
		return AjaxUtil.createReturnValueSuccess();
	}*/
	
	/**
	 * 根据ID删除
	 * @param id
	 * @return
	 */
	public Object[] delChatBanById(Integer id){
		if(id==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		chatBanService.deleteByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess();
	}
	
	/**
	 * 保存
	 * @param record
	 * @return
	 */
	public Object[] saveChatBan(ChatBan record){
    	if(record == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	Integer id = record.getId();
    	if(id == null){
    		record.setCreateDate(new Date());
    		record.setOperator(BaseDwrUtil.getCurrentAdmin().getUsername());
    		chatBanService.insertSelective(record);
    	}else{
    		record.setUpdateDate(new Date());
    		record.setOperator(BaseDwrUtil.getCurrentAdmin().getUsername());
    		chatBanService.updateByPrimaryKeySelective(record);
    	}
    	return AjaxUtil.createReturnValueSuccess();
    }
}
