package com.team.lottery.action.web.manage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.PayNotifyLogService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.PayNotifyLog;

@Controller("managerPayNotifyLogAction")
public class ManagerPayNotifyLogAction {
	@Autowired
	private PayNotifyLogService payNotifyLogService;
	
    /**
     * 查询
     * @return
     */
    public Object[] getAllPayNotifyLog(PayNotifyLog query,Page page){
    	if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	/*Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	if(!(ConstantUtil.SUPER_SYSTEM).equals(currentAdmin.getBizSystem()))
    	{
    		query.setBizSystem(currentAdmin.getBizSystem());
    	}*/
    	page=payNotifyLogService.getAllPayNotifyLogByQueryPage(query, page);
		return AjaxUtil.createReturnValueSuccess(page);
    }
}
