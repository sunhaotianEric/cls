package com.team.lottery.action.web.manage;

import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.service.UserService;
import com.team.lottery.system.job.AutoDataDeleteJob;
import com.team.lottery.system.job.AutoDataProcesJob;
import com.team.lottery.system.job.DayActivityJob;
import com.team.lottery.system.job.DayOnlineInfoJob;
import com.team.lottery.system.job.DayProfileJob;
import com.team.lottery.system.job.LotteryCodeTrendJob;
import com.team.lottery.system.job.LotteryDayGainJob;
import com.team.lottery.system.job.MonthBonusJob;
import com.team.lottery.system.job.PlatformChargeOrderJob;
import com.team.lottery.system.job.QuartzJob;
import com.team.lottery.system.job.UserDayConsumeJob;
import com.team.lottery.system.job.UserDayMoneyJob;
import com.team.lottery.system.job.UserHourConsumeJob;
import com.team.lottery.system.job.UserSelfDayConsumeJob;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.DateUtil;

@Controller("managerTreatmentOrderAction")
public class ManagerTreatmentOrderAction {
	
	private static Logger logger = LoggerFactory.getLogger(ManagerTreatmentOrderAction.class);
	@Autowired
	private UserService userService;
    
    
    
    /**
     * 代理福利手动结算
     * @param treatmentType
     * @param dealTime
     * @return
     */
    public Object[] treatmentApplyManual(String treatmentType, Boolean isReleaseMoneySwitch, Date dealTime,String releasePolicy,String bizSystem) {
    	if(treatmentType == null || dealTime == null) {
    		return AjaxUtil.createReturnValueError("参数出错");
    	}
    	//判断时间  不能是今天日期或之后
    	Date nowDateStart = DateUtil.getNowStartTimeByStart(new Date());
    	//半月分红和月工资可以选择今天的日期
    	nowDateStart =  DateUtil.getNowStartTimeByEnd(new Date());
    	try {
    	if(dealTime.after(nowDateStart)) {
    		return AjaxUtil.createReturnValueError("手动结算时间不能选择今天之后的日期");
    	}
    	
    	Calendar nowCalendar = Calendar.getInstance();
    	Calendar dealTimeCalendar = Calendar.getInstance();
    	dealTimeCalendar.setTime(dealTime);
    	//数据库查询的归属日期
    	Date queryBelongDate = DateUtil.getNowStartTimeByStart(dealTime);
    	
    	QuartzJob job = null;
    	if("HALF_MONTH_BOUNS".equals(treatmentType)) {
    		Integer dealDay = dealTimeCalendar.get(Calendar.DAY_OF_MONTH);
    		Integer dealMonth = dealTimeCalendar.get(Calendar.MONTH);
    		Date date = new Date();
    		dealTimeCalendar.setTime(date);
    		Integer currentMonth = dealTimeCalendar.get(Calendar.MONTH);
    		// 不是当月才能调度任务.
			/*if(currentMonth != dealMonth) {*/
				if (dealDay != MonthBonusJob.OPEN_UP_MONTH_BOUNS_TIME) {
					return AjaxUtil.createReturnValueError("分红手动结算日期必须在每月1日");
				}
				job = ApplicationContextUtil.getBean("monthBonusJob");
			//是每月的16号
			/*} else {
    			return AjaxUtil.createReturnValueError("结算必须选择上月!");
    		}*/
    	} else if("DAY_ACTIVITY".equals(treatmentType)) {
    		job = ApplicationContextUtil.getBean("dayActivityJob");
    	} else if("DAY_SALARY".equals(treatmentType)){
    		job = ApplicationContextUtil.getBean("daySalaryJob");
    	}
    	if(job != null) {
	    	job.setNowTime(dealTime);
	    	//根据页面设置是否需要生成订单数据
	    	job.setIsReleaseMoney(isReleaseMoneySwitch);
	    	job.setBizSystem(bizSystem);
	    	job.setReleasePolicy(releasePolicy);
	    	job.execute();
    	 }
    	} catch (Exception e) {
    		return AjaxUtil.createReturnValueError(e.getMessage());	
		}
		return AjaxUtil.createReturnValueSuccess("手工结算成功");
    }
    
    
    /**
     * 用户团队日盈利手动结算
     * @param treatmentType
     * @param dealTime
     * @return
     */
    public Object[] userDayConsumeManual( Date dayConsumeTime,  Integer dayConsumeDays,String bizSystem) {
    	if(dayConsumeTime == null || dayConsumeDays == null) {
    		return AjaxUtil.createReturnValueError("参数出错");
    	}
    	//判断时间  不能是今天日期或之后
    	Date nowDateStart = DateUtil.getNowStartTimeByStart(new Date());
    	if(dayConsumeTime.after(nowDateStart)) {
    		return AjaxUtil.createReturnValueError("手动结算时间必须要在今天之前");
    	}
    	
		//可以查询的开始时间
        Date queryStartDate = DateUtil.addDaysToDate(nowDateStart, -1 * ConstantUtil.DELETE_AGO_DATA_NORMAL);
        if(queryStartDate.after(dayConsumeTime)) {
        	return AjaxUtil.createReturnValueError("手动计算用户团队日盈亏开始时间只能在当前时间"+ConstantUtil.DELETE_AGO_DATA_NORMAL+"天之内");
        }
    	
    	/*Runnable runnable = new Runnable() {
			
			@Override
			public void run() {
				UserDayConsumeJob job = ApplicationContextUtil.getBean("userDayConsumeJob");
				
				job.reCalcuteUserDayConsume(dayConsumeTime, dayConsumeDays);
			}
		};
		Thread thread = new Thread(runnable);
		thread.start();*/
		
		UserDayConsumeJob job = ApplicationContextUtil.getBean("userDayConsumeJob");
		job.setBizSystem(bizSystem);
		job.reCalcuteUserDayConsume(dayConsumeTime, dayConsumeDays);
    	
		return AjaxUtil.createReturnValueSuccess("手工结算用户团队日盈亏成功");
    }
    
    /**
     * 用户自身日盈利手动结算
     * @param treatmentType
     * @param dealTime
     * @return
     */
    public Object[] userSelfDayConsumeManual(Date dayMoneyTime, Integer dayMoneyDays,String bizSystem) {
    	if(dayMoneyTime == null || dayMoneyDays == null) {
    		return AjaxUtil.createReturnValueError("参数出错");
    	}
    	//判断时间  不能是今天日期或之后
    	Date nowDateStart = DateUtil.getNowStartTimeByStart(new Date());
    	if(dayMoneyTime.after(nowDateStart)) {
    		return AjaxUtil.createReturnValueError("手动结算时间必须要在今天之前");
    	}
    	
		//可以查询的开始时间
        Date queryStartDate = DateUtil.addDaysToDate(nowDateStart, -1 * ConstantUtil.DELETE_AGO_DATA_NORMAL);
        if(queryStartDate.after(dayMoneyTime)) {
        	return AjaxUtil.createReturnValueError("手动计算用户自身日余额开始时间只能在当前时间"+ConstantUtil.DELETE_AGO_DATA_NORMAL+"天之内");
        }
    	
		UserSelfDayConsumeJob job = ApplicationContextUtil.getBean("userSelfDayConsumeJob");
		
		job.reCalcuteUserDayConsume(dayMoneyTime, dayMoneyDays);
		job.setBizSystem(bizSystem);
		return AjaxUtil.createReturnValueSuccess("手工结算用户自身日盈亏成功");
    }
    
    /**
     * 用户自身日盈利手动结算
     * @param treatmentType
     * @param dealTime
     * @return
     */
    public Object[] userDayMoneyManual(final Date dayMoneyTime, final Integer dayMoneyDays) {
    	if(dayMoneyTime == null || dayMoneyDays == null) {
    		return AjaxUtil.createReturnValueError("参数出错");
    	}
    	//判断时间  不能是今天日期或之后
    	Date nowDateStart = DateUtil.getNowStartTimeByStart(new Date());
    	if(dayMoneyTime.after(nowDateStart)) {
    		return AjaxUtil.createReturnValueError("手动结算时间必须要在今天之前");
    	}
    	
		//可以查询的开始时间
        Date queryStartDate = DateUtil.addDaysToDate(nowDateStart, -1 * ConstantUtil.DELETE_AGO_DATA_NORMAL);
        if(queryStartDate.after(dayMoneyTime)) {
        	return AjaxUtil.createReturnValueError("手动计算用户团队日余额开始时间只能在当前时间"+ConstantUtil.DELETE_AGO_DATA_NORMAL+"天之内");
        }
    	
        UserDayMoneyJob job = ApplicationContextUtil.getBean("userDayMoneyJob");
		
		job.reCalcuteUserDayMoney(dayMoneyTime, dayMoneyDays);
    	
		return AjaxUtil.createReturnValueSuccess("手工结算用户团队日余额成功");
    }
    
    /**
     * 用户团队小时盈利手动结算
     * @param treatmentType
     * @param dealTime
     * @return
     */
    public Object[] userHourConsumeManual(final Date hourConsumeTime, final Integer hourConsumeCount,String bizSystem) {
    	if(hourConsumeTime == null || hourConsumeCount == null) {
    		return AjaxUtil.createReturnValueError("参数出错");
    	}
    	//判断时间 只能是今天日期
    	Date nowDateStart = DateUtil.getNowStartTimeByStart(new Date());
    	if(nowDateStart.after(hourConsumeTime)) {
    		return AjaxUtil.createReturnValueError("手动结算时间必须要在今天");
    	}
    	
        if(hourConsumeCount > 23) {
        	return AjaxUtil.createReturnValueError("手动计算用户小时数不能超过23");
        }
    	
		UserHourConsumeJob job = ApplicationContextUtil.getBean("userHourConsumeJob");
		
		job.reCalcuteUserHourConsume(hourConsumeTime, hourConsumeCount);
		job.setBizSystem(bizSystem);
		return AjaxUtil.createReturnValueSuccess("手工结算用户团队小时盈亏成功");
 }
    

	/**
	 * 平台盈利手动结算
	 * @param treatmentType
	 * @param dealTime
	 * @return
	 */
    public Object[] dayProfitManual( Date startDate,  Integer days,String bizSystem) {
		if(startDate == null || days == null) {
			return AjaxUtil.createReturnValueError("参数出错");
		}
		//判断时间  不能是今天日期或之后
		Date nowDateStart = DateUtil.getNowStartTimeByStart(new Date());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startDate);
		calendar.add(Calendar.DAY_OF_MONTH, days-1);
		if(startDate.after(nowDateStart)) {
			return AjaxUtil.createReturnValueError("手动结算时间必须要在今天之前");
		}else if(calendar.getTime().after(nowDateStart)){
			return AjaxUtil.createReturnValueError("计算天数的时间不能大于今天！");
	     //有包含计算昨天必须在这今天三点之后
		}else if(calendar.getTime().getDay()==nowDateStart.getDay()&&new Date().getHours()<3){
				return AjaxUtil.createReturnValueError("计算天数(包含计算昨天)必须在今天三点之后结算！");
		}
		
		DayProfileJob job = ApplicationContextUtil.getBean("dayProfileJob");
		job.setBizSystem(bizSystem);
		job.reCalcuteDayProfit(startDate, days);
		
		return AjaxUtil.createReturnValueSuccess("手工结算平台盈亏成功");
	
	}
    
    
	/**
	 * 平台盈利手动结算
	 * @param treatmentType
	 * @param dealTime
	 * @return
	 */
    public Object[] lotteryDayGainManual( Date startDate,  Integer days,String bizSystem) {
		if(startDate == null || days == null) {
			return AjaxUtil.createReturnValueError("参数出错");
		}
		//判断时间  不能是今天日期或之后
		Date nowDateStart = DateUtil.getNowStartTimeByStart(new Date());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startDate);
		calendar.add(Calendar.DAY_OF_MONTH, days-1);
		
		
		
		if(startDate.after(nowDateStart)) {
			return AjaxUtil.createReturnValueError("手动结算时间必须要在今天之前");
		}else if(calendar.getTime().after(nowDateStart)){
			return AjaxUtil.createReturnValueError("计算天数的时间不能大于今天！");
	    //有包含计算昨天必须在这今天三点之后
		}else if(calendar.getTime().getDay()==nowDateStart.getDay()&&new Date().getHours()<3){
			return AjaxUtil.createReturnValueError("计算天数(包含计算昨天)必须在今天三点之后结算！");
		}
		
		LotteryDayGainJob job = ApplicationContextUtil.getBean("lotteryDayGainJob");
		job.setBizSystem(bizSystem);
		job.reCalcuteDayProfit(startDate, days);
		
		return AjaxUtil.createReturnValueSuccess("手工结算平台盈亏成功");
	
	}
    
    /**
     * 用户团队日盈利手动结算
     * @param treatmentType
     * @param dealTime
     * @return
     */
    public Object[] platformManual( Date platformTime, String bizSystem) {
    	if(platformTime == null) {
    		return AjaxUtil.createReturnValueError("参数出错");
    	}
    	//判断时间  不能是今天日期或之后
    	Date nowDate = new Date();
    	if(platformTime.getMonth()==nowDate.getMonth()) {
    		return AjaxUtil.createReturnValueError("只能结算这月之前的数据！");
    	}
    	
		
		PlatformChargeOrderJob job = ApplicationContextUtil.getBean("platformChargeOrderJob");
		job.setBizSystem(bizSystem);
		job.setNowTime(platformTime);
		job.execute();
    	
		return AjaxUtil.createReturnValueSuccess("手工结算平台收费生成订单成功");
    }
    
	/**
	 * 活动彩金手动结算
	 * @param treatmentType
	 * @param dealTime
	 * @return
	 */
    public Object[] activityMoneyManual( Date startDate,  Integer days,String bizSystem) {
		if(startDate == null || days == null) {
			return AjaxUtil.createReturnValueError("参数出错");
		}
		//判断时间  不能是今天日期或之后
		Date nowDateStart = DateUtil.getNowStartTimeByStart(new Date());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startDate);
		calendar.add(Calendar.DAY_OF_MONTH, days-1);
		
		
		
		if(startDate.after(nowDateStart)) {
			return AjaxUtil.createReturnValueError("手动结算时间必须要在今天之前");
		}else if(calendar.getTime().after(nowDateStart)){
			return AjaxUtil.createReturnValueError("计算天数的时间不能大于今天！");
	    //有包含计算昨天必须在这今天三点之后
		}else if(calendar.getTime().getDay()==nowDateStart.getDay()&&new Date().getHours()<3){
			return AjaxUtil.createReturnValueError("计算天数(包含计算昨天)必须在今天三点之后结算！");
		}
		
		DayActivityJob job = ApplicationContextUtil.getBean("dayActivityJob");
		job.setBizSystem(bizSystem);
		job.reCalcuteDayProfit(startDate, days);
		return AjaxUtil.createReturnValueSuccess("手工结算平台盈亏成功");
	
	}
    
   
    
    /**
     * 每日在线统计
     * @param bizSystem
     * @param dateStar
     * @return
     */
    public Object[] syncChatRoomDayOnlineinfoJob(String bizSystem,Date dateStar){
    	if(dateStar == null) {
			return AjaxUtil.createReturnValueError("参数出错");
		}
		//判断时间  不能是今天日期或之后
		Date nowDateStart = DateUtil.getNowStartTimeByStart(new Date());
		
		if(dateStar.after(nowDateStart)) {
			return AjaxUtil.createReturnValueError("手动结算时间必须要在今天之前");
		}
		
		DayOnlineInfoJob job = ApplicationContextUtil.getBean("dayOnlineInfoJob");
		job.setBizSystem(bizSystem);
		job.setNowTime(dateStar);
		job.execute();
    	return AjaxUtil.createReturnValueSuccess("手动统计每日在线数据完成");
    }
    
    /**
     * 删除数据
     * @param bizSystem
     * @param dateStar
     * @return
     */
    public Object[] dataDelete(){
		
		AutoDataDeleteJob job = ApplicationContextUtil.getBean("autoDataDeleteJob");
		job.execute();
    	return AjaxUtil.createReturnValueSuccess("手动删除数据已完成");
    }
    
    /**
     * 处理当前奖金模式数据
     * @param bizSystem
     * @param dateStar
     * @return
     */
    public Object[] dataProcess(String bizSystem){
    	AutoDataProcesJob job = ApplicationContextUtil.getBean("autoDataProcesJob");
    	job.setBizSystem(bizSystem);
    	job.execute();
    	return AjaxUtil.createReturnValueSuccess("手动执行["+bizSystem+"]系统,用户头像批量处理已完成");
    }
    
    /**
     * 处理六合彩走势图
     * @return
     */
    public Object[] dataCode(){
    	LotteryCodeTrendJob job = ApplicationContextUtil.getBean("lotteryCodeTrendJob");
//    	job.setBizSystem(bizSystem);
    	job.execute();
    	return AjaxUtil.createReturnValueSuccess("手动处理批量六合彩走势图已完成");
    }
}