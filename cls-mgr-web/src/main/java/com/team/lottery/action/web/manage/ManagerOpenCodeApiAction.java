package com.team.lottery.action.web.manage;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.enums.ERedisChannel;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.OpenCodeApiService;
import com.team.lottery.system.message.MessageQueueCenter;
import com.team.lottery.system.message.messagetype.OpenCodeApiMessage;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.util.URLConnectUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.OpenCodeApi;

@Controller("managerOpenCodeApiAction")
public class ManagerOpenCodeApiAction {
	
	private static Logger log = LoggerFactory.getLogger(ManagerOpenCodeApiAction.class);

	@Autowired
	private OpenCodeApiService openCodeApiService;

    
	  /**
     * 查找page
     * @return
     */
    public Object[] getOpenCodeApiPage(OpenCodeApi query,Page page){
    	if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	page=openCodeApiService.getOpenCodeApiByQueryPage(query, page);
    	return AjaxUtil.createReturnValueSuccess(page);
		
    }

    
    /**
     * 新增和修改
     * @param systemConfig
     * @return
     */
    public Object[] saveOrUpdateOpenCodeApi(OpenCodeApi openCodeApi)
    {
    	try{
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		if(!ConstantUtil.SUPER_SYSTEM.equals(currentAdmin.getBizSystem()))
		{
			return AjaxUtil.createReturnValueError("sorry!,您没有权限操作！");
		}
    	if(openCodeApi == null){
    		return AjaxUtil.createReturnValueError("参数传递错误");
    	}
    	if(openCodeApi.getId()!=null&&!"".equals(openCodeApi.getId()))
    	{
    		return this.updateBizSystem(openCodeApi);
    	}else{
    		return this.addOpenCodeApi(openCodeApi);
    	}
    	}catch(Exception e){
        	return AjaxUtil.createReturnValueError(e.getMessage());
    	}
    }
    
    /**
     * 新增
     * @param 
     * @return
     */
	public Object[] addOpenCodeApi(OpenCodeApi openCodeApi) {
		if (openCodeApi == null) {
			return AjaxUtil.createReturnValueError("参数传递错误");
		}	
		//初始化插入默认正式接口
		openCodeApi.setCurrentApiUrl(openCodeApi.getApiUrl());
		openCodeApi.setCurrentApiType(openCodeApi.getApiType());
		openCodeApi.setCreateTime(new Date());
		openCodeApi.setUpdateTime(new Date());
		int result = openCodeApiService.insertSelective(openCodeApi);
		if (result > 0) {
			return AjaxUtil.createReturnValueSuccess();
		} else {
			return AjaxUtil.createReturnValueError("添加失败，请重新添加");
		}
		// return AjaxUtil.createReturnValueSuccess();
	}
	
    /**
     * 修改
     * @param bizSystem
     * @return
     */
	public Object[] updateBizSystem(OpenCodeApi openCodeApi) {
		if (openCodeApi == null) {
			return AjaxUtil.createReturnValueError("参数传递错误");
		}
		openCodeApi.setUpdateTime(new Date());
		int result = openCodeApiService.updateByPrimaryKeySelective(openCodeApi);
		if (result > 0) {
			return AjaxUtil.createReturnValueSuccess();
		} else {
			return AjaxUtil.createReturnValueError("添加失败，请重新添加");
		}
	}
    
    
    
    /**
     * 删除
     * @return
     */
    public Object[] delOpenCodeApiById(Integer id){
    	try{
	    	 int result=openCodeApiService.deleteByPrimaryKey(id);
	    	 if(result<=0){
	             return AjaxUtil.createReturnValueError("删除失败！");
	         }
    	}catch(Exception e){
        	return AjaxUtil.createReturnValueError(e.getMessage());
    	}
    	
 
    	return AjaxUtil.createReturnValueSuccess();
    }
	
    /**
     * 根据id获取对象
     * @param id
     * @return
     */
    public Object[] getOpenCodeApiById(Integer id){
     	if(id == null){
    		return AjaxUtil.createReturnValueError("参数传递错误");
    	}
     	OpenCodeApi openCodeApi= openCodeApiService.selectByPrimaryKey(id);
    	return AjaxUtil.createReturnValueSuccess(openCodeApi);
    }
    
    
    /**
     * 替换接口
     * @param id
     * @return
     */
    public Object[] changeOpenCodeApiUrlAndType(Integer id){
    	if(id == null){
    		return AjaxUtil.createReturnValueError("参数传递错误");
    	}
    	
     	OpenCodeApi openCodeApi= openCodeApiService.selectByPrimaryKey(id);
     	if(openCodeApi.getCurrentApiUrl().equals(openCodeApi.getBackApiUrl())){
     		openCodeApi.setCurrentApiUrl(openCodeApi.getApiUrl());
     		openCodeApi.setCurrentApiType(openCodeApi.getApiType());
     	}else{
     		openCodeApi.setCurrentApiUrl(openCodeApi.getBackApiUrl());
     		openCodeApi.setCurrentApiType(openCodeApi.getBackApiType());
     	}
     	int result =  openCodeApiService.updateByPrimaryKeySelective(openCodeApi);
     	if(result > 0){
     		List<OpenCodeApi> list = new ArrayList<OpenCodeApi>();
     		list.add(openCodeApi);
     		OpenCodeApiMessage openCodeApiMessage = new OpenCodeApiMessage();
     		openCodeApiMessage.setList(list);
     		MessageQueueCenter.putRedisMessage(openCodeApiMessage, ERedisChannel.OPENCODECACHE.name());
     	}
     	return AjaxUtil.createReturnValueSuccess();
    }
    
    
    
    /**
     * 替换接口
     * @param id
     * @return
     */
    public Object[] refreshOpenCodeApiUrlAndType(){

    	OpenCodeApi query = new OpenCodeApi();
    	List<OpenCodeApi> list = openCodeApiService.getOpenCodeApiByLotteryKind(query);   
    	log.info("单个开奖接口切换");
    	OpenCodeApiMessage openCodeApiMessage = new OpenCodeApiMessage();
 		openCodeApiMessage.setList(list);
 		MessageQueueCenter.putRedisMessage(openCodeApiMessage, ERedisChannel.OPENCODECACHE.name());
     	
     	return AjaxUtil.createReturnValueSuccess();
    }
    
    /**
     * 替换接口2   参数为彩种信息
     * @param kind
     * @return
     */
    public Object[] refreshOpenCodeApiUrlAndTypeByLotteryKind(ELotteryKind lotteryKind, Integer interfaceType){
    	if(lotteryKind == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	OpenCodeApi query = new OpenCodeApi();
    	query.setLotteryKind(lotteryKind.getCode());
    	List<OpenCodeApi> list = openCodeApiService.getOpenCodeApiByLotteryKind(query);   
    	String result="";
    	String url="";
    	try {
    		if(interfaceType==0){
    			url=list.get(0).getApiUrl();
    		}else if(interfaceType==1){
    			url=list.get(0).getBackApiUrl();
    		}
    		if(StringUtils.isEmpty(url)){
    			return AjaxUtil.createReturnValueError("查询接口地址返回空字符串");
    		}
			result = URLConnectUtil.getStringFromURL(url);
			if(StringUtils.isEmpty(result)){
				return AjaxUtil.createReturnValueError("查询接口数据返回为空字符串.");
			}
		} catch (Exception e) {
			log.error("查询接口数据出现IO异常", e);
			return AjaxUtil.createReturnValueError("查询接口数据出现IO异常");
		}
    	return  AjaxUtil.createReturnValueSuccess(result);
    }
    
    /**
     * 一键替换接口
     * @param id
     * @return
     */
    public Object[] changeOpenCodeApiUrlAndTypeAll(){
    	try {
    		OpenCodeApi openCodeApi=new OpenCodeApi();
    		List<OpenCodeApi> openCodeApis= openCodeApiService.getOpenCodeApiByLotteryKind(openCodeApi);
    		log.info("一键切换开奖接口切换");
    		for(int i=0;i<openCodeApis.size();i++){
    			openCodeApi=openCodeApis.get(i);
    			if(!openCodeApi.getCurrentApiUrl().equals(openCodeApi.getApiUrl())){
    				openCodeApi.setCurrentApiUrl(openCodeApi.getApiUrl());
    				openCodeApi.setCurrentApiType(openCodeApi.getApiType());
    				int result =  openCodeApiService.updateByPrimaryKeySelective(openCodeApi);
    				if(result > 0){
    					List<OpenCodeApi> list = new ArrayList<OpenCodeApi>();
    					list.add(openCodeApi);
    					OpenCodeApiMessage openCodeApiMessage = new OpenCodeApiMessage();
    					openCodeApiMessage.setList(list);
    					MessageQueueCenter.putRedisMessage(openCodeApiMessage, ERedisChannel.OPENCODECACHE.name());
    				}
    			}
    		}
    		return AjaxUtil.createReturnValueSuccess();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return AjaxUtil.createReturnValueError("一键切换失败");
		}
    }
}
