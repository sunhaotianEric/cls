package com.team.lottery.action.web.manage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.Page;
import com.team.lottery.service.ChatlogService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.Chatlog;

@Controller("managerChatLogAction")
public class ManagerChatLogAction {

	@Autowired
	private ChatlogService chatlogService;
	
	public Object[] getChatLogPage(Chatlog query,Page page){
		if(query==null||page==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		Admin currentAdmin=BaseDwrUtil.getCurrentAdmin();
		if(!currentAdmin.getBizSystem().equals(ConstantUtil.SUPER_SYSTEM)){
			query.setBizSystem(currentAdmin.getBizSystem());
		}
		page=chatlogService.getChatLogPage(query, page);
		return AjaxUtil.createReturnValueSuccess(page);
	}
	
	/**
	 * 根据ID删除聊天记录
	 * @param id
	 * @return
	 */
	public Object[] delChatLogById(Long id){
		chatlogService.deleteByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess();
	}
}
