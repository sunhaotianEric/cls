package com.team.lottery.action.web.manage;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.team.lottery.autowithdraw.interfaces.impl.*;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections.CollectionUtils;
import org.directwebremoting.io.FileTransfer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.autowithdraw.interfaces.IAutoWithDrawService;
import com.team.lottery.autowithdraw.result.ResultForAutoWithDraw;
import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.enums.EAdminOperateLogModel;
import com.team.lottery.enums.EFundAutoWithdrawDealStatus;
import com.team.lottery.enums.EFundOperateType;
import com.team.lottery.enums.EFundWithDrawStatus;
import com.team.lottery.enums.EWithdrawAutoStatus;
import com.team.lottery.enums.EWithdrawAutoType;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.RechargeWithDrawInfoExportVO;
import com.team.lottery.extvo.RechargeWithDrawOrderQuery;
import com.team.lottery.service.AdminOperateLogService;
import com.team.lottery.service.BizSystemService;
import com.team.lottery.service.ChargePayService;
import com.team.lottery.service.QuickBankTypeService;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.service.WithdrawAutoConfigService;
import com.team.lottery.service.WithdrawAutoLogService;
import com.team.lottery.service.WithdrawOrderService;
import com.team.lottery.util.AdminOperateUtil;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.ExcelUtil;
import com.team.lottery.util.ExcelUtil.ExcelExportData;
import com.team.lottery.util.HttpClientUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.util.ToolKit;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.BizSystem;
import com.team.lottery.vo.ChargePay;
import com.team.lottery.vo.QuickBankType;
import com.team.lottery.vo.ThirdPayConfig;
import com.team.lottery.vo.WithdrawAutoConfig;
import com.team.lottery.vo.WithdrawAutoLog;
import com.team.lottery.vo.WithdrawOrder;

import net.sf.json.JSONObject;

@Controller("managerWithDrawOrderAction")
public class ManagerWithDrawOrderAction {

	private static Logger logger = LoggerFactory.getLogger(ManagerWithDrawOrderAction.class);

	@Autowired
	private WithdrawOrderService withDrawOrderService;
	@Autowired
	private BizSystemService bizSystemService;
	@Autowired
	private WithdrawAutoLogService withdrawAutoLogService;
	@Autowired
	private WithdrawAutoConfigService withdrawAutoConfigService;
	@Autowired
	private ChargePayService chargePayService;
	@Autowired
	private QuickBankTypeService quickBankTypeService;
	@Autowired
	private ThirdPayConfigService thirdPayConfigService;
	@Autowired
	private AdminOperateLogService adminOperateLogService;

	/**
	 * 查找符合条件的取现记录
	 *
	 * @return
	 */
	public Object[] getAllWithDrawOrdersByQuery(RechargeWithDrawOrderQuery query, Page page) {
		if (query == null || page == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		Admin admin = BaseDwrUtil.getCurrentAdmin();
		/*
		 * if(admin.getState() != null && admin.getState().equals(0)){
		 * //如果是超级管理员,则可以查看所有的充值记录 }else{ query.setAdminId(admin.getId()); }
		 */
		query.setOperateType(EFundOperateType.WITHDRAW.name());

		if (!"SUPER_SYSTEM".equals(admin.getBizSystem())) {
			query.setBizSystem(admin.getBizSystem());
		}
		// 查询列表
		page = withDrawOrderService.getWithDrawOrdersByQueryPage(query, page);
		return AjaxUtil.createReturnValueSuccess(page);
	}

	/**
	 * 取现申请锁定
	 *
	 * @param rechargeId
	 * @param presentStr
	 * @return
	 */
	public Object[] setWithDrawOrdersLocked(Long rechargeId) {
		if (rechargeId == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		try {
			Admin admin = BaseDwrUtil.getCurrentAdmin();
			WithdrawOrder withdrawOrder = withDrawOrderService.selectByPrimaryKey(rechargeId);
			if (withdrawOrder == null) {
				throw new Exception("该订单不存在.");
			}
			if (withdrawOrder.getDealStatus().equals(EFundWithDrawStatus.LOCKED.name())) {
				throw new Exception("该订单已经被管理员[" + withdrawOrder.getLockAdmin() + "]锁定");
			}
			if (withdrawOrder.getDealStatus().equals(EFundWithDrawStatus.WITHDRAW_SUCCESS.name())) {
				throw new Exception("该订单已经取现成功了,无法锁定.");
			}
			if (withdrawOrder.getDealStatus().equals(EFundWithDrawStatus.WITHDRAW_FAIL.name())) {
				throw new Exception("该订单已经取现关闭了,无法锁定.");
			}
			String operateDesc = AdminOperateUtil.constructMessage("withdrawOrder_locking", admin.getUsername(),
					withdrawOrder.getUserName(), withdrawOrder.getSerialNumber());
			// String operateDesc
			// ="管理员["+admin.getUsername()+"],
			// 把会员["+withdrawOrder.getUserName()+"]的订单号为["+withdrawOrder.getSerialNumber()+"]进行取现申请锁定操作!";
			adminOperateLogService.saveAdminOperateLog(admin.getBizSystem(), BaseDwrUtil.getRequestIp(), admin.getId(), admin.getUsername(),
					withdrawOrder.getUserId(), withdrawOrder.getUserName(), EAdminOperateLogModel.WITHDRAW_OPERATE, operateDesc);
			withDrawOrderService.setWithDrawOrdersLocked(withdrawOrder, admin);
		} catch (Exception e) {
			return AjaxUtil.createReturnValueError(e.getMessage());
		}
		return AjaxUtil.createReturnValueSuccess();
	}

	/**
	 * 取现申请成功
	 *
	 * @param rechargeId
	 * @param presentStr
	 * @return
	 */
	public Object[] setWithDrawOrdersPaySuccess(Long rechargeId) {
		if (rechargeId == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		try {
			Admin admin = BaseDwrUtil.getCurrentAdmin();
			WithdrawOrder withdrawOrder = withDrawOrderService.selectByPrimaryKey(rechargeId);
			if (withdrawOrder == null) {
				throw new Exception("该订单不存在.");
			}
			if (!EFundWithDrawStatus.LOCKED.name().equals(withdrawOrder.getDealStatus())) {
				throw new Exception("该订单还未被锁定，请先进行锁定操作");
			}
			if (withdrawOrder.getDealStatus().equals(EFundWithDrawStatus.WITHDRAW_SUCCESS.name())) {
				throw new Exception("该订单已经取现成功了,无法操作");
			}
			if (withdrawOrder.getDealStatus().equals(EFundWithDrawStatus.WITHDRAW_FAIL.name())) {
				throw new Exception("该订单已经取现关闭了,无法操作");
			}
			if (!withdrawOrder.getLockAdmin().equals(admin.getUsername())) {
				throw new Exception("该订单被管理员[" + withdrawOrder.getLockAdmin() + "]锁定,您无法进行确认取现操作");
			}
			String operateDesc = AdminOperateUtil.constructMessage("withdrawOrder_success", admin.getUsername(),
					withdrawOrder.getUserName(), withdrawOrder.getSerialNumber());
			// String operateDesc
			// ="管理员["+admin.getUsername()+"],
			// 把会员["+withdrawOrder.getUserName()+"]的订单号为["+withdrawOrder.getSerialNumber()+"]进行取现申请成功操作!";
			adminOperateLogService.saveAdminOperateLog(admin.getBizSystem(), BaseDwrUtil.getRequestIp(), admin.getId(), admin.getUsername(),
					withdrawOrder.getUserId(), withdrawOrder.getUserName(), EAdminOperateLogModel.WITHDRAW_OPERATE, operateDesc);
			withDrawOrderService.setWithDrawOrdersPaySuccess(withdrawOrder, admin);
		} catch (UnEqualVersionException e) {
			logger.error(e.getMessage(), e);
			return AjaxUtil.createReturnValueError("当前系统繁忙，请稍后重试");
		} catch (Exception e) {
			return AjaxUtil.createReturnValueError(e.getMessage());
		}
		return AjaxUtil.createReturnValueSuccess();
	}

	/**
	 * 取现申请订单关闭
	 *
	 * @param rechargeId
	 * @return
	 */
	public Object[] setWithDrawOrdersPayClose(Long rechargeId, String analyseValue) {
		if (rechargeId == null || StringUtils.isEmpty(analyseValue)) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		if (analyseValue.length() > 300) {
			return AjaxUtil.createReturnValueError("关闭订单的原因分析字符过多,系统不支持处理.");
		}
		try {
			Admin admin = BaseDwrUtil.getCurrentAdmin();
			WithdrawOrder withdrawOrder = withDrawOrderService.selectByPrimaryKey(rechargeId);
			if (withdrawOrder == null) {
				throw new Exception("该订单不存在.");
			}
			if (!EFundWithDrawStatus.LOCKED.name().equals(withdrawOrder.getDealStatus())) {
				throw new Exception("该订单还未被锁定，请先进行锁定操作");
			}
			if (withdrawOrder.getDealStatus().equals(EFundWithDrawStatus.WITHDRAW_SUCCESS.name())) {
				throw new Exception("该订单已经取现成功了,无法操作");
			}
			if (withdrawOrder.getDealStatus().equals(EFundWithDrawStatus.WITHDRAW_FAIL.name())) {
				throw new Exception("该订单已经取现关闭了,无法操作");
			}
			if (!withdrawOrder.getLockAdmin().equals(admin.getUsername())) {
				throw new Exception("该订单被管理员[" + withdrawOrder.getLockAdmin() + "]锁定,您无法进行订单关闭操作");
			}
			String operateDesc = AdminOperateUtil.constructMessage("withdrawOrder_close", admin.getUsername(), withdrawOrder.getUserName(),
					withdrawOrder.getSerialNumber());
			// String operateDesc
			// ="管理员["+admin.getUsername()+"],
			// 把会员["+withdrawOrder.getUserName()+"]的订单号为["+withdrawOrder.getSerialNumber()+"]进行取现申请订单关闭操作!";
			adminOperateLogService.saveAdminOperateLog(admin.getBizSystem(), BaseDwrUtil.getRequestIp(), admin.getId(), admin.getUsername(),
					withdrawOrder.getUserId(), withdrawOrder.getUserName(), EAdminOperateLogModel.WITHDRAW_OPERATE, operateDesc);
			withDrawOrderService.setWithDrawOrdersPayClose(withdrawOrder, analyseValue, admin);
		} catch (UnEqualVersionException e) {
			logger.error(e.getMessage(), e);
			return AjaxUtil.createReturnValueError("当前系统繁忙，请稍后重试");
		} catch (Exception e) {
			return AjaxUtil.createReturnValueError(e.getMessage());
		}
		return AjaxUtil.createReturnValueSuccess();
	}

	/**
	 * 查找查询启用中的业务系统
	 *
	 * @return
	 */
	public Object[] getAllEnableBizSystem() {
		List<BizSystem> bizSystemlist = null;
		// 从缓存上直接读取
		bizSystemlist = ClsCacheManager.getAllBizSystem();
		if (bizSystemlist.size() < 1) {
			logger.debug("缓存中没拿到，从数据库直接取了！");
			// 从数据库直接取
			bizSystemlist = bizSystemService.getAllEnableBizSystem();
		}
		return AjaxUtil.createReturnValueSuccess(bizSystemlist);
	}

	/**
	 * 导出用户信息
	 *
	 * @param userName
	 * @param request
	 * @param response
	 * @return
	 */
	public FileTransfer rechargeWithDrawInfoExport(RechargeWithDrawOrderQuery query) {

		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			/* query.setOperateType(EFundOperateType.RECHARGE.name()); */
			List<WithdrawOrder> orderList = new ArrayList<WithdrawOrder>();
			orderList = withDrawOrderService.getWithDrawOrders(query);
			String fileName = "D:\\" + "TXDD" + DateUtil.getDate(new Date(), "yyyy-MM-dd") + ".xls";
			// String fileName = "D:\\" + "CZDD" + DateUtil.getDate(new Date(),
			// "yyyy-MM-dd") + ".xls";
			RechargeWithDrawInfoExportVO rechargeWithDrawInfoExportVO = new RechargeWithDrawInfoExportVO();
			rechargeWithDrawInfoExportVO.setSerialNumber(true);
			rechargeWithDrawInfoExportVO.setUserName(true);
			rechargeWithDrawInfoExportVO.setApplyValue(true);
			rechargeWithDrawInfoExportVO.setAccountValue(true);
			rechargeWithDrawInfoExportVO.setCreatedDateStr(true);
			rechargeWithDrawInfoExportVO.setStatusStr(true);
			rechargeWithDrawInfoExportVO.setOperateDes(true);
			rechargeWithDrawInfoExportVO.setFromType(true);
			rechargeWithDrawInfoExportVO.setBankUserName(true);
			rechargeWithDrawInfoExportVO.setUpdateAdmin(true);
			rechargeWithDrawInfoExportVO.setUpdateDateStr(true);
			rechargeWithDrawInfoExportVO.setBankCardNum(true);
			rechargeWithDrawInfoExportVO.setBankName(true);
			rechargeWithDrawInfoExportVO.setBankAccountName(true);
			rechargeWithDrawInfoExportVO.setSubbranchName(true);
			rechargeWithDrawInfoExportVO.setFeeValue(true);
			String str = "TXDD";
			Admin admin = BaseDwrUtil.getCurrentAdmin();
			ExcelExportData excelExportData = this.constructRechargeWithDrawInfoExcelData(admin.getBizSystem(), str, orderList,
					rechargeWithDrawInfoExportVO);
			fileName = new String(fileName.getBytes("GBK"), "iso8859-1");
			bos = ExcelUtil.export2Stream(excelExportData);
			return new FileTransfer(fileName, "application/msexcel", bos.toByteArray());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return null;
	}

	/**
	 * 根据用户名构建表格数据
	 *
	 * @param users
	 * @return
	 */
	private ExcelExportData constructRechargeWithDrawInfoExcelData(String bizSystem, String fileName, List<WithdrawOrder> orderList,
																   RechargeWithDrawInfoExportVO rechargeWithDrawInfoExportVO) {
		List<String[]> columNames = new ArrayList<String[]>();
		List<String[]> fieldNames = new ArrayList<String[]>();

		List<String> sheetColumNames = new ArrayList<String>();
		List<String> sheetFieldNames = new ArrayList<String>();
		if (bizSystem.equals("SUPER_SYSTEM")) {
			sheetColumNames.add("业务系统");
			sheetFieldNames.add("bizSystemName");
		}
		if (rechargeWithDrawInfoExportVO.getSerialNumber()) {
			sheetColumNames.add("订单号");
			sheetFieldNames.add("serialNumber");
		}
		if (rechargeWithDrawInfoExportVO.getUserName()) {
			sheetColumNames.add("用户名");
			sheetFieldNames.add("userName");
		}
		if (rechargeWithDrawInfoExportVO.getApplyValue()) {
			sheetColumNames.add("充值金额");
			sheetFieldNames.add("applyValue");
		}

		if (rechargeWithDrawInfoExportVO.getFeeValue()) {
			sheetColumNames.add("手续费");
			sheetFieldNames.add("feeValue");
		}
		if (rechargeWithDrawInfoExportVO.getAccountValue()) {
			sheetColumNames.add("到账金额");
			sheetFieldNames.add("accountValue");
		}
		if (rechargeWithDrawInfoExportVO.getCreatedDateStr()) {
			sheetColumNames.add("充值时间");
			sheetFieldNames.add("createdDateStr");
		}
		if (rechargeWithDrawInfoExportVO.getStatusStr()) {
			sheetColumNames.add("状态");
			sheetFieldNames.add("statusStr");
		}

		if (rechargeWithDrawInfoExportVO.getFromType()) {
			sheetColumNames.add("订单来源");
			sheetFieldNames.add("fromType");
		}
		if (rechargeWithDrawInfoExportVO.getBankAccountName()) {
			sheetColumNames.add("收款姓名");
			sheetFieldNames.add("bankAccountName");
		}
		if (rechargeWithDrawInfoExportVO.getBankName()) {
			sheetColumNames.add("银行名称");
			sheetFieldNames.add("bankName");
		}
		if (rechargeWithDrawInfoExportVO.getSubbranchName()) {
			sheetColumNames.add("支行名称");
			sheetFieldNames.add("subbranchName");
		}
		if (rechargeWithDrawInfoExportVO.getBankCardNum()) {
			sheetColumNames.add("收款卡号");
			sheetFieldNames.add("bankCardNum");
		}

		if (rechargeWithDrawInfoExportVO.getUpdateAdmin()) {
			sheetColumNames.add("操作人");
			sheetFieldNames.add("updateAdmin");
		}
		if (rechargeWithDrawInfoExportVO.getUpdateDateStr()) {
			sheetColumNames.add("操作时间");
			sheetFieldNames.add("updateDateStr");
		}
		columNames.add(sheetColumNames.toArray(new String[sheetColumNames.size()]));
		fieldNames.add(sheetFieldNames.toArray(new String[sheetFieldNames.size()]));

		// 对user资料，为空设置为无
		if (CollectionUtils.isNotEmpty(orderList)) {
			/*
			 * for(Order u : orderList) { if(StringUtils.isEmpty(u.getTrueName())) {
			 * u.setTrueName("无"); } if(StringUtils.isEmpty(u.getIdentityid())) {
			 * u.setIdentityid("无"); } if(StringUtils.isEmpty(u.getPhone())) {
			 * u.setPhone("无"); } if(StringUtils.isEmpty(u.getQq())) { u.setQq("无"); }
			 * if(StringUtils.isEmpty(u.getEmail())) { u.setEmail("无"); } String address =
			 * ""; if(StringUtils.isEmpty(u.getProvince()) &&
			 * StringUtils.isEmpty(u.getCity()) &&StringUtils.isEmpty(u.getAddress())) {
			 * address = "无"; } else { if(!StringUtils.isEmpty(u.getProvince())) { address
			 * += u.getProvince(); } if(!StringUtils.isEmpty(u.getCity())) { address +=
			 * u.getCity(); } if(!StringUtils.isEmpty(u.getAddress())) { address +=
			 * u.getAddress(); } } u.setAddress(address); }
			 */
		}
		LinkedHashMap<String, List<?>> map = new LinkedHashMap<String, List<?>>();
		map.put(fileName, orderList);

		ExcelExportData setInfo = new ExcelExportData();
		setInfo.setDataMap(map);
		setInfo.setFieldNames(fieldNames);
		setInfo.setTitles(new String[] {});
		setInfo.setColumnNames(columNames);

		// 将需要导出的数据输出到文件
		return setInfo;
	}

	/**
	 * 查找符合条件的充值记录(快捷充值)
	 *
	 * @return
	 */
	public Object[] getAllWithDrawOrdersByQueryForReport(RechargeWithDrawOrderQuery query, Page page) {
		if (query == null || page == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		Admin admin = BaseDwrUtil.getCurrentAdmin();
		String bizSysterm = query.getBizSystem();
		if (bizSysterm == null && !"SUPER_SYSTEM".equals(admin.getBizSystem())) {
			query.setBizSystem(admin.getBizSystem());
		}
		query.setAdminId(null);

		// 查询列表
		page = withDrawOrderService.getWithDrawOrdersByQueryPage(query, page);
		WithdrawOrder totalRechargeOrder = withDrawOrderService.getWithDrawOrdersByTotal(query);
		return AjaxUtil.createReturnValueSuccess(page, totalRechargeOrder);
	}

	/**
	 * 自动出款
	 *
	 * @param rechargeId
	 * @param presentStr
	 * @return
	 */
	public Object[] setWithDrawAutoOrdersPaySuccess(Long rechargeId) {
		if (rechargeId == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		try {

			WithdrawOrder rechargeWithDrawOrder = withDrawOrderService.selectByPrimaryKey(rechargeId);
			Admin admin = BaseDwrUtil.getCurrentAdmin();
			// MD5加密串
			/* String key="932888676AF3405FB969E5DFA24CFCF9"; */
			String key = "";
			// 商户公共密钥
			String publicKey = "";
			Long chargePayId = 0L;
			String merNo = "";
			String callBackUrl = "";
			String memberId = "";
			ChargePay chargePay = null;
			WithdrawAutoConfig config = withdrawAutoConfigService.selectWithdrawAutoConfig(rechargeWithDrawOrder.getBizSystem());
			if (config != null) {
				chargePayId = config.getChargePayId();
				chargePay = chargePayService.selectByPrimaryKey(chargePayId);
				if (chargePay != null) {
					key = chargePay.getSign();
					merNo = chargePay.getMemberId();
					callBackUrl = chargePay.getReturnUrl();
					publicKey = chargePay.getPublickey();
					memberId = chargePay.getMemberId();
				}
				if (rechargeWithDrawOrder.getApplyValue().compareTo(config.getHighestMoney()) > 0) {
					return AjaxUtil.createReturnValueError(config.getBizSystemName() + "系统自动出款的金额不能超过" + config.getHighestMoney() + "元!");
				}

			} else {
				return AjaxUtil.createReturnValueError("系统未配置自动出款设置或者相关自动出款设置已关闭!");
			}
			WithdrawAutoLog queryWithdrawAutoLog = new WithdrawAutoLog();
			queryWithdrawAutoLog.setSerialNumber(rechargeWithDrawOrder.getSerialNumber());
			List<WithdrawAutoLog> withdrawAutoLogList = withdrawAutoLogService.getWithdrawAutoLogs(queryWithdrawAutoLog);
			if (withdrawAutoLogList != null && withdrawAutoLogList.size() > 0) {
				return AjaxUtil.createReturnValueError("该提现订单已经发起自动出款!不能在再次发起!");
			}

			if ("CHENGXINTONG".equals(config.getThirdPayType())) {

				Map<String, String> metaSignMap = new TreeMap<String, String>();
				metaSignMap.put("version", "1.0");
				metaSignMap.put("partner", merNo);
				metaSignMap.put("bankType", "BANK");
				metaSignMap.put("paymoney", rechargeWithDrawOrder.getApplyValue().toString());
				metaSignMap.put("bankAccount", rechargeWithDrawOrder.getBankCardNum());
				metaSignMap.put("bankName", rechargeWithDrawOrder.getBankName());
				metaSignMap.put("bankUserName", rechargeWithDrawOrder.getBankAccountName());
				metaSignMap.put("tradeCode", rechargeWithDrawOrder.getSerialNumber());
				metaSignMap.put("timestamp", String.valueOf(System.currentTimeMillis()));
				metaSignMap.put("callbackurl", callBackUrl);
				String Md5key = chargePay.getSign();
				String md5Str = "version=" + metaSignMap.get("version") + "&partner=" + metaSignMap.get("partner") + "&bankType="
						+ metaSignMap.get("bankType") + "&paymoney=" + metaSignMap.get("paymoney") + "&bankAccount="
						+ metaSignMap.get("bankAccount") + "&bankName=" + metaSignMap.get("bankName") + "&bankUserName="
						+ metaSignMap.get("bankUserName") + "&tradeCode=" + metaSignMap.get("tradeCode") + "&timestamp="
						+ metaSignMap.get("timestamp") + "&callbackurl=" + metaSignMap.get("callbackurl");
				// String md5 =new String();//MD5签名格式
				logger.info("md5Str:" + md5Str + Md5key);
				String Signature = DigestUtils.md5Hex(md5Str + Md5key);
				metaSignMap.put("sign", Signature);
				md5Str = md5Str + "&sign=" + metaSignMap.get("sign");

				String sBuilder = "";
				Collection<String> keysetFrom = metaSignMap.keySet();
				List listForm = new ArrayList<String>(keysetFrom);
				for (int i = 0; i < listForm.size(); i++) {
					if (i == 0) {
						sBuilder += listForm.get(i) + "=" + metaSignMap.get(listForm.get(i)).toString();
					} else {
						String str = "&" + listForm.get(i) + "=" + metaSignMap.get(listForm.get(i)).toString();
						sBuilder += str;
					}
				}

				String resultJsonStr = HttpClientUtil.doPost(chargePay.getAddress(), metaSignMap);

				logger.info("诚信通自动出款返回信息:" + resultJsonStr);
				// 检查状态
				JSONObject resultJsonObj = JSONObject.fromObject(resultJsonStr);
				String success = resultJsonObj.getString("success");
				if (success.equals("true")) {

					WithdrawAutoLog withdrawAutoLog = new WithdrawAutoLog();
					withdrawAutoLog.setAdminName(rechargeWithDrawOrder.getLockAdmin());
					withdrawAutoLog.setBizSystem(rechargeWithDrawOrder.getBizSystem());
					withdrawAutoLog.setBankAccountName(rechargeWithDrawOrder.getBankAccountName());
					withdrawAutoLog.setBankCardNum(rechargeWithDrawOrder.getBankCardNum());
					withdrawAutoLog.setBankType(rechargeWithDrawOrder.getBankType());
					withdrawAutoLog.setBankName(rechargeWithDrawOrder.getBankName());
					ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("CHENGXINTONG");
					withdrawAutoLog.setChargeDes(thirdPayConfig.getChargeDes());
					withdrawAutoLog.setChargeType(thirdPayConfig.getChargeType());
					withdrawAutoLog.setCreatedDate(new Date());
					withdrawAutoLog.setDealStatus(EWithdrawAutoStatus.SENDING.name());
					withdrawAutoLog.setMemberId(memberId);
					withdrawAutoLog.setSerialNumber(rechargeWithDrawOrder.getSerialNumber());
					withdrawAutoLog.setType(EWithdrawAutoType.MANUAL.name());
					withdrawAutoLog.setUserName(rechargeWithDrawOrder.getUserName());
					withdrawAutoLog.setUserId(rechargeWithDrawOrder.getUserId());
					withdrawAutoLog.setWithdrowOrderId(rechargeWithDrawOrder.getId());
					withdrawAutoLog.setMoney(rechargeWithDrawOrder.getApplyValue());
					withdrawAutoLogService.insertSelective(withdrawAutoLog);
					rechargeWithDrawOrder.setAutoWithdrawDealStatus(EFundAutoWithdrawDealStatus.DEALING.name());
					withDrawOrderService.updateByPrimaryKey(rechargeWithDrawOrder);
					String operateDesc = AdminOperateUtil.constructMessage("withdrawOrder_locking", admin.getUsername(),
							rechargeWithDrawOrder.getUserName(), rechargeWithDrawOrder.getSerialNumber());
					// String operateDesc
					// ="管理员["+admin.getUsername()+"],
					// 把会员["+rechargeWithDrawOrder.getUserName()+"]的订单号为["+rechargeWithDrawOrder.getSerialNumber()+"]进行取现自动出款操作!";
					adminOperateLogService.saveAdminOperateLog(admin.getBizSystem(), BaseDwrUtil.getRequestIp(), admin.getId(),
							admin.getUsername(), rechargeWithDrawOrder.getUserId(), rechargeWithDrawOrder.getUserName(),
							EAdminOperateLogModel.WITHDRAW_OPERATE, operateDesc);
					return AjaxUtil.createReturnValueSuccess(resultJsonObj.getString("msg"));
				} else {
					return AjaxUtil.createReturnValueError(resultJsonObj.getString("msg"));
				}
			}
			// 通过接口的形式调用.
			IAutoWithDrawService autoWithDrawService = null;
			if ("KUAIHUITONGPAY".equals(config.getThirdPayType())) {
				autoWithDrawService = ApplicationContextUtil.getBean(KuaiHuiTongAutoWithDrawService.class);
			} else if ("PPPAY".equals(config.getThirdPayType())) {
				autoWithDrawService = ApplicationContextUtil.getBean(PPAutoWithDrawService.class);
				// 3721代付
			} else if ("HIPAY3721".equals(config.getThirdPayType())) {
				// 判断如果有小数点的情况，则不允许出款
				if (new BigDecimal(rechargeWithDrawOrder.getApplyValue().intValue()).compareTo(rechargeWithDrawOrder.getApplyValue()) !=0 ){
					return AjaxUtil.createReturnValueError("当前自动出款方式不允许有小数数值的出款");
				}
				autoWithDrawService = ApplicationContextUtil.getBean(Hipay3721AutoWithDrawService.class);
				// 虎鲸代付
			} else if ("HUJINDPAY".equals(config.getThirdPayType())) {
				autoWithDrawService = ApplicationContextUtil.getBean(HujingAutoWithDrawService.class);
				// 无忧代付
			} else if ("WUYOU".equals(config.getThirdPayType())) {
				autoWithDrawService = ApplicationContextUtil.getBean(WuyouAutoWithDrawService.class);
			}else if ("STARTOUT".equals(config.getThirdPayType())){
				autoWithDrawService = ApplicationContextUtil.getBean(StartAutoWithDrawService.class);
			}
			else {
				// String key="932888676AF3405FB969E5DFA24CFCF9";
				Map<String, String> metaSignMap = new TreeMap<String, String>();
				metaSignMap.put("orderNum", rechargeWithDrawOrder.getSerialNumber());
				metaSignMap.put("version", "V3.1.0.0");
				metaSignMap.put("charset", "UTF-8");

				// 需要经常修改的参数
				// 银行代码、商户号、银行账户开户名、卡号、金额（分）、支付结果通知地址
				String bankCode = "";
				QuickBankType queryBankType = new QuickBankType();
				queryBankType.setChargeType(chargePay.getChargeType());
				queryBankType.setBankType(rechargeWithDrawOrder.getBankType());
				List<QuickBankType> QuickBankTypeList = quickBankTypeService.getQuickBankTypeQuery(queryBankType);
				if (QuickBankTypeList != null && QuickBankTypeList.size() == 0) {
					throw new Exception("该银行类型不支持自动出款请联系客服!");
				} else {
					bankCode = QuickBankTypeList.get(0).getPayId();
				}
				metaSignMap.put("bankCode", bankCode);
				/* metaSignMap.put("merNo","JTZF800735"); */
				metaSignMap.put("merNo", merNo);
				metaSignMap.put("bankAccountName", rechargeWithDrawOrder.getBankAccountName());
				metaSignMap.put("bankAccountNo", rechargeWithDrawOrder.getBankCardNum());
				// 使用分进行提交
				String OrderMoney = String.valueOf(rechargeWithDrawOrder.getApplyValue().multiply(new BigDecimal(100)).intValue());
				metaSignMap.put("amount", OrderMoney);
				// metaSignMap.put("callBackUrl",
				// "http://mgr.boss288.com:8086/front/pay/jiutong/returnurl.jsp");
				metaSignMap.put("callBackUrl", callBackUrl);

				// 生成参数加入参数列表
				String metaSignJsonStr = ToolKit.mapToJson(metaSignMap);
				String sign = ToolKit.MD5(metaSignJsonStr + key, metaSignMap.get("charset"));
				metaSignMap.put("sign", sign);
				// 数据公钥加密
				logger.info("publicKey:" + publicKey);
				logger.info("metaSignMap:" + metaSignMap);
				byte[] dataStr = ToolKit.encryptByPublicKey(ToolKit.mapToJson(metaSignMap).getBytes("UTF-8"), publicKey);
				logger.info("dataStr:" + dataStr);
				String param = Base64.encodeBase64String(dataStr);
				logger.info("param:" + param);
				String data = URLEncoder.encode(param, "UTF-8");
				/* String url = "http://api.jiutongpay.com.cn/api/remit.action"; */
				String url = chargePay.getAddress();
				// 发起代付请求
				String resultJsonStr = ToolKit.request(url,
						"data=" + data + "&merchNo=" + metaSignMap.get("merNo") + "&version=" + metaSignMap.get("version"));
				logger.info("久通自动出款返回信息:" + resultJsonStr);
				// 检查状态
				JSONObject resultJsonObj = JSONObject.fromObject(resultJsonStr);
				String stateCode = resultJsonObj.getString("stateCode");
				if (stateCode.equals("00")) {

					WithdrawAutoLog withdrawAutoLog = new WithdrawAutoLog();
					withdrawAutoLog.setAdminName(rechargeWithDrawOrder.getLockAdmin());
					withdrawAutoLog.setBizSystem(rechargeWithDrawOrder.getBizSystem());
					withdrawAutoLog.setBankAccountName(rechargeWithDrawOrder.getBankAccountName());
					withdrawAutoLog.setBankCardNum(rechargeWithDrawOrder.getBankCardNum());
					withdrawAutoLog.setBankType(rechargeWithDrawOrder.getBankType());
					withdrawAutoLog.setBankName(rechargeWithDrawOrder.getBankName());
					ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("JIUTONG");
					withdrawAutoLog.setChargeDes(thirdPayConfig.getChargeDes());
					withdrawAutoLog.setChargeType(thirdPayConfig.getChargeType());
					withdrawAutoLog.setCreatedDate(new Date());
					withdrawAutoLog.setDealStatus(EWithdrawAutoStatus.SENDING.name());
					withdrawAutoLog.setMemberId(memberId);
					withdrawAutoLog.setSerialNumber(rechargeWithDrawOrder.getSerialNumber());
					withdrawAutoLog.setType(EWithdrawAutoType.MANUAL.name());
					withdrawAutoLog.setUserName(rechargeWithDrawOrder.getUserName());
					withdrawAutoLog.setUserId(rechargeWithDrawOrder.getUserId());
					withdrawAutoLog.setWithdrowOrderId(rechargeWithDrawOrder.getId());
					withdrawAutoLog.setMoney(rechargeWithDrawOrder.getApplyValue());
					withdrawAutoLogService.insertSelective(withdrawAutoLog);
					rechargeWithDrawOrder.setAutoWithdrawDealStatus(EFundAutoWithdrawDealStatus.DEALING.name());
					withDrawOrderService.updateByPrimaryKey(rechargeWithDrawOrder);
					String operateDesc = AdminOperateUtil.constructMessage("withdrawOrder_locking", admin.getUsername(),
							rechargeWithDrawOrder.getUserName(), rechargeWithDrawOrder.getSerialNumber());
					// String operateDesc
					// ="管理员["+admin.getUsername()+"],
					// 把会员["+rechargeWithDrawOrder.getUserName()+"]的订单号为["+rechargeWithDrawOrder.getSerialNumber()+"]进行取现自动出款操作!";
					adminOperateLogService.saveAdminOperateLog(admin.getBizSystem(), BaseDwrUtil.getRequestIp(), admin.getId(),
							admin.getUsername(), rechargeWithDrawOrder.getUserId(), rechargeWithDrawOrder.getUserName(),
							EAdminOperateLogModel.WITHDRAW_OPERATE, operateDesc);
					return AjaxUtil.createReturnValueSuccess(resultJsonObj.getString("msg"));
				} else {
					return AjaxUtil.createReturnValueError(resultJsonObj.getString("msg"));
				}
			}

			ResultForAutoWithDraw autoWithDrawResult = autoWithDrawService.autoWithDraw(rechargeWithDrawOrder, chargePay);
			if (autoWithDrawResult.isStatus()) {
				return AjaxUtil.createReturnValueSuccess(autoWithDrawResult.getMessage());
			} else {
				return AjaxUtil.createReturnValueError(autoWithDrawResult.getMessage());
			}

		} catch (Exception e) {
			logger.error("自动出款错误:", e);
			return AjaxUtil.createReturnValueError("自动出款错误");

		}

	}

	/**
	 * 取充值提现订单最新的订单号(新方法)
	 *
	 * @return
	 */
	public Object[] getUptodateSerialNumber(RechargeWithDrawOrderQuery query) {
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		if (!"SUPER_SYSTEM".equals(currentAdmin.getBizSystem())) {
			query.setBizSystem(currentAdmin.getBizSystem());
		}
		String serialNumber = withDrawOrderService.getUptodateSerialNumber(query);
		return AjaxUtil.createReturnValueSuccess(serialNumber);

	}
}
