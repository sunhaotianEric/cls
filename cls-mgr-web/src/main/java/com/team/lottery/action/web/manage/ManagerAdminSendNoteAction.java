package com.team.lottery.action.web.manage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.Page;
import com.team.lottery.service.AdminRecvNoteService;
import com.team.lottery.service.AdminSendNoteService;
import com.team.lottery.service.BizSystemService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.AdminRecvNote;
import com.team.lottery.vo.AdminSendNote;

@Controller("managerAdminSendNoteAction")
public class ManagerAdminSendNoteAction {

	@Autowired
	private AdminSendNoteService adminSendNoteService;
	@Autowired
	private AdminRecvNoteService adminRecvNoteService;
	@Autowired
	private BizSystemService bizSystemService;
	
	/**
	 * 分页查询发件信息
	 * @param record
	 * @param page
	 * @return
	 */
	public Object[] getAdminSendNotePage(AdminSendNote record,Page page){
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		record.setFromAdminId(currentAdmin.getId());
		if("SUPER_SYSTEM".equals(currentAdmin.getBizSystem())){
			record.setFromBizSystem(currentAdmin.getBizSystem());
		}
		return AjaxUtil.createReturnValueSuccess(adminSendNoteService.getAdminSendNotePage(record, page));
	}
	
	/**
	 * 添加发件信息
	 * @param record
	 * @return
	 */
	public Object[] insertSelective(AdminSendNote record,String bizSystem){
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		record.setFromAdminId(currentAdmin.getId());
		record.setFromAdminName(currentAdmin.getUsername());
		record.setFromBizSystem(currentAdmin.getBizSystem());
		record.setReadedCount(0);
		int i=adminSendNoteService.insertSelective(record);
		if(i>0){
			String[] id=record.getToAdminIds().split(",");
			String[] name=record.getToAdminNames().split(",");
			String[] bizSystems=bizSystem.split(",");
			String msg="";
			for (int j = 0; j < id.length; j++) {
				if(!"".equals(name[j])){
					AdminRecvNote adminRecvNote=new AdminRecvNote();
					adminRecvNote.setSendNoteId(Integer.parseInt(record.getId().toString()));
					adminRecvNote.setFromAdminId(record.getFromAdminId());
					adminRecvNote.setFromAdminName(record.getFromAdminName());
					adminRecvNote.setFromBizSystem(record.getFromBizSystem());
					adminRecvNote.setToAdminName(name[j].trim());
					adminRecvNote.setToAdminId(Integer.parseInt(id[j].trim()));
					adminRecvNote.setToBizSystem(bizSystems[j].trim());
					adminRecvNote.setSub(record.getSub());
					adminRecvNote.setContent(record.getContent());
					adminRecvNote.setReadStatus("未读");
					//根据存的发件数据添加收件箱数据
					int k=adminRecvNoteService.insertSelective(adminRecvNote);
					if(k==0){
						msg+=name[j]+",";
					}
				}
			}
			if(!msg.equals("")){
				return AjaxUtil.createReturnValueError("发送给"+msg+"的信件失败");
			}
		}
		return AjaxUtil.createReturnValueSuccess("已发送");
	}
	
	/**
	 * 根据ID查询发件信息
	 * @param id
	 * @return
	 */
	public Object[] selectByPrimaryKey(Long id){
		return AjaxUtil.createReturnValueSuccess(adminSendNoteService.selectByPrimaryKey(id));
	}
	
	/**
	 * 修改发件信息
	 * @param record
	 * @return
	 */
	public int updateByPrimaryKeySelective(AdminSendNote record){
		AdminSendNote records=adminSendNoteService.selectByPrimaryKey(record.getId());
		if(records!=null){
			record.setReadedAdminIds(records.getReadedAdminIds()+record.getReadedAdminIds()+",");
			record.setReadedCount(records.getReadedCount()+1);
			return adminSendNoteService.updateByPrimaryKeySelective(record);
		}
		return 0;
	}
	
	/**
	 * 根据ID删除发件信息
	 * @param id
	 * @return
	 */
	public Object[] deleteByPrimaryKey(Long id){
		return AjaxUtil.createReturnValueSuccess(adminSendNoteService.deleteByPrimaryKey(id)>0?"删除成功":"删除失败");
	}
	
	/**
	 * 根据业务系统和角色查询管理员
	 * @param admin
	 * @return
	 */
	public Object[] getAllAdminByQuery(Admin admin){
		if(admin==null){
			return AjaxUtil.createReturnValueError("参数传递错误");
		}
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		admin.setId(currentAdmin.getId());
		return AjaxUtil.createReturnValueSuccess(adminSendNoteService.getAllAdminByQuery(admin));
	}
	
	/**
	 * 查询所有的业务系统
	 * @return
	 */
	public Object[] getAllBizSystem(){
		return AjaxUtil.createReturnValueSuccess(bizSystemService.getAllBizSystem());
	}
}
