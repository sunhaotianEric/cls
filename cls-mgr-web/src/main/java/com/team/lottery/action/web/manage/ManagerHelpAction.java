package com.team.lottery.action.web.manage;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.enums.EHelpType;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.HelpService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.vo.Help;

@Controller("managerHelpAction")
public class ManagerHelpAction {

	@Autowired
	private HelpService helpService;
	
    /**
     * 查找所有的帮助中心
     * @return
     */
    public Object[] getAllHelps(Help query,Page page){
    	if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	
    	String bizSystem = BaseDwrUtil.getCurrentAdmin().getBizSystem();
    	if(!"SUPER_SYSTEM".equals(bizSystem)){
    		query.setBizSystem(bizSystem);
    	}
    	page=helpService.getAllHelps(query, page);
		
		return AjaxUtil.createReturnValueSuccess(page);
    }
    
    /**
     * 查找指定的帮助中心
     * @return
     */
    public Object[] getHelpById(Long id){
    	Help help =  helpService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(help);
    }
    
    /**
     * 删除指定的帮助中心
     * @return
     */
    public Object[] delHelp(Long id){
    	helpService.deleteByPrimaryKey(id);
    	return AjaxUtil.createReturnValueSuccess();
    }

    /**
     * 保存帮助中心
     * @return
     */
    public Object[] saveOrUpdateHelp(Help help){
    	try{
    		Long id = help.getId();
    		help.setTypeDes(EHelpType.valueOf(help.getType()).getDescription());
    		String bizSystem = BaseDwrUtil.getCurrentAdmin().getBizSystem();
        	if(!"SUPER_SYSTEM".equals(bizSystem)){
        		help.setBizSystem(bizSystem);
        	}
        	if(id==null){
        		help.setCreateDate(new Date());
        		helpService.insert(help);
        	}else{
        		helpService.updateByPrimaryKeySelective(help);
        	}
        	
    		return AjaxUtil.createReturnValueSuccess();
    	}catch(Exception e){
    		return AjaxUtil.createReturnValueError(e.getMessage());
    	}
    	
    }
}
