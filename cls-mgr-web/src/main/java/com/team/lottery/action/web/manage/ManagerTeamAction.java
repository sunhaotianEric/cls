package com.team.lottery.action.web.manage;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.enums.EUserDailLiLevel;
import com.team.lottery.extvo.MoneyDetailQuery;
import com.team.lottery.extvo.OrderQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.TeamUserQuery;
import com.team.lottery.extvo.UserConsumeReportVo;
import com.team.lottery.extvo.UserDayConsumeQuery;
import com.team.lottery.extvo.UserQuery;
import com.team.lottery.service.MoneyDetailService;
import com.team.lottery.service.UserDayConsumeService;
import com.team.lottery.service.UserMoneyTotalService;
import com.team.lottery.service.UserSelfDayConsumeService;
import com.team.lottery.service.UserService;
import com.team.lottery.system.SingleLoginForRedis;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.Login;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserDayConsume;
import com.team.lottery.vo.UserMoneyTotal;
import com.team.lottery.vo.UserSelfDayConsume;

@Controller("managerTeamAction")
public class ManagerTeamAction {
	
	@Autowired
	private UserService userService;
	@Autowired
	private MoneyDetailService moneyDetailService;
	@Autowired
	private UserMoneyTotalService userMoneyTotalService;
	@Autowired
	private UserDayConsumeService userDayConsumeService;
	@Autowired
	private UserSelfDayConsumeService userSelfDayConsumeService;
	 /**
     * 查找团队列表的会员数据
     * @return
     */
    @SuppressWarnings("unchecked")
	public Object[] getTeamUserListPage(TeamUserQuery query,Page page){
    	if(query == null || page == null ){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    
        if(query.getTeamLeaderName() != null && !StringUtils.isEmpty(query.getTeamLeaderName())){
        	String leaderName = query.getTeamLeaderName();
        	if(leaderName.length() < 3){
        		return AjaxUtil.createReturnValueError("对不起,参数传递错误.");
        	}
        	leaderName = leaderName.substring(0,leaderName.length() - 2); //真实领导人用户名
    	
        	User leaderUser = userService.getUserByName(query.getBizSystem(),leaderName);
        	if(leaderUser == null){
        		return AjaxUtil.createReturnValueError("对不起,该用户不存在.");
        	}
        	String [] regFroms =leaderUser.getRegfrom().split(ConstantUtil.REG_FORM_SPLIT);
            if(regFroms == null || regFroms.length < 1){
        		return AjaxUtil.createReturnValueError("对不起,该用户的推荐注册有误.");
            }
            if(EUserDailLiLevel.SUPERAGENCY.name().equals(leaderUser.getDailiLevel())){
              query.setTeamLeaderName(leaderName + ConstantUtil.REG_FORM_SPLIT);
            }else{
              query.setTeamLeaderName(ConstantUtil.REG_FORM_SPLIT + leaderName + ConstantUtil.REG_FORM_SPLIT);
            }
        }else{
        	return AjaxUtil.createReturnValueError("对不起,参数传递错误.");
        }
        //设置查询在线用户列表条件
        if(query.getIsOnline() != null && query.getIsOnline() == true) {
        	query.setOnlineUserNames(SingleLoginForRedis.getFrontLoginUserNams(Login.LOGTYPE_PC+"."+query.getBizSystem()));
        }
         List<String> dailiList=new ArrayList<String>();
         
         if(query.getDailiLevel()!=null&&query.getDailiLevel().equals("ORDINARYAGENCY")){
        	 dailiList.add("ORDINARYAGENCY");
        	 dailiList.add("GENERALAGENCY");
        	 query.setDailiLevels(dailiList);
        	 query.setDailiLevel(null);
        }
        userService.getTeamUserListByQueryPage(query, page);
        
        //在线会员
        List<User> users = (List<User>)page.getPageContent();
        //设置在线会员状态
        SingleLoginForRedis.setUsersOnlineStatus(users);
        for(User user : users){
        	Set<String> pcsets=SingleLoginForRedis.getRedisSetsOnlyNames(Login.LOGTYPE_PC+"."+user.getBizSystem());
        	if(pcsets.contains(user.getUserName())){
    		user.setIsOnline(1);
    	    }
        	Set<String> mobilesets=SingleLoginForRedis.getRedisSetsOnlyNames(Login.LOGTYPE_MOBILE+"."+user.getBizSystem());
        	if(mobilesets.contains(user.getUserName())){
    		user.setIsOnline(1);
    	    }

        }
		return AjaxUtil.createReturnValueSuccess(page);
    }
    
    /**
     * 分页查找团队消费报表
     * @param query
     * @param pageNo
     * @return
     */
    public Object[] getTeamConsumeReportForQuery(UserDayConsumeQuery query,Page page){
    	if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		if(!"SUPER_SYSTEM".equals(currentAdmin.getBizSystem())){
			query.setBizSystem(currentAdmin.getBizSystem());	
		}
//    	Page page = new Page(SystemSet.managerPageSize);
//    	page.setPageNo(pageNo);
    	String leaderName = query.getUserName();
    	User leaderUser =  null;
    	String regform = "";
    	//团队领导人验证
        if(leaderName != null && !StringUtils.isEmpty(leaderName)){
       	
        	leaderUser = userService.getUserByName(query.getBizSystem(),leaderName);
        	if(leaderUser == null){
        		return AjaxUtil.createReturnValueError("对不起,该用户不存在.");
        	}
        	String [] regFroms =leaderUser.getRegfrom().split(ConstantUtil.REG_FORM_SPLIT);
            if(regFroms == null || regFroms.length < 1){
        		return AjaxUtil.createReturnValueError("对不起,该用户的推荐注册有误.");
            }
          
        }else{
        	return AjaxUtil.createReturnValueError("用户名必输!");
        }
        regform = leaderUser.getRegfrom()+leaderUser.getUserName()+"&";
        query.setRegfrom(regform);
        //查询时间的判断
        Date nowDateStart = new Date();
        Date yesterDateStart = DateUtil.addDaysToDate(nowDateStart, -1);
        yesterDateStart = DateUtil.getNowStartTimeByStart(yesterDateStart);
        int hourTime = DateUtil.getHourOfDay(nowDateStart);
        int minuteTime = DateUtil.getMis(nowDateStart);
        if(hourTime < ConstantUtil.DAY_COMSUME_END_HOUR || (hourTime == ConstantUtil.DAY_COMSUME_END_HOUR && minuteTime < ConstantUtil.DAY_COMSUME_END_MINUTE)) {
        	nowDateStart = DateUtil.addDaysToDate(nowDateStart, -1);
        }
        nowDateStart = DateUtil.getNowStartTimeByStart(nowDateStart);
        nowDateStart = DateUtil.addHoursToDate(nowDateStart, ConstantUtil.DAY_COMSUME_END_HOUR);
        nowDateStart = DateUtil.addMinutesToDate(nowDateStart, ConstantUtil.DAY_COMSUME_END_MINUTE);

		  //判断可以查询的时间
        Date canQueryStartDate = DateUtil.addDaysToDate(new Date(), -1 * ConstantUtil.DELETE_AGO_DATA_NORMAL);
        if(canQueryStartDate.after(query.getCreatedDateStart())) {
        	return AjaxUtil.createReturnValueError("查询盈亏报表的起始时间只能在当前时间"+ConstantUtil.DELETE_AGO_DATA_NORMAL+"天之内");
        }
		Calendar historyReportDate = Calendar.getInstance();
		historyReportDate.setTime(query.getCreatedDateStart());
		historyReportDate.set(Calendar.HOUR_OF_DAY, ConstantUtil.DAY_COMSUME_END_HOUR);
		historyReportDate.set(Calendar.MINUTE, ConstantUtil.DAY_COMSUME_END_MINUTE);
		historyReportDate.set(Calendar.SECOND, 0);
		query.setBelongDateStart(historyReportDate.getTime());
		query.setCreatedDateStart(historyReportDate.getTime());
		historyReportDate.setTime(query.getCreatedDateEnd());
		historyReportDate.add(Calendar.DAY_OF_MONTH, 1);
		historyReportDate.set(Calendar.HOUR_OF_DAY, ConstantUtil.DAY_COMSUME_END_HOUR);
		historyReportDate.set(Calendar.MINUTE, (ConstantUtil.DAY_COMSUME_END_MINUTE - 1));
		historyReportDate.set(Calendar.SECOND, 59);
		historyReportDate.set(Calendar.MILLISECOND, 999);
		query.setBelongDateEnd(historyReportDate.getTime());
		query.setCreatedDateEnd(historyReportDate.getTime());
		query.setUserName(null);
		Map<String,UserDayConsume> resultMaps = new LinkedHashMap<String, UserDayConsume>();
		List<UserDayConsume> list = userDayConsumeService.getUserDayConsumeByCondition2(query);
		
		TeamUserQuery teamUserQuery = new TeamUserQuery();
		teamUserQuery.setBizSystem(query.getBizSystem());
		teamUserQuery.setRegisterDateStart(query.getCreatedDateStart());
		teamUserQuery.setRegisterDateEnd(query.getCreatedDateEnd());
		OrderQuery orderQuery =  new OrderQuery();
		orderQuery.setBizSystem(query.getBizSystem());
		orderQuery.setCreatedDateStart(query.getCreatedDateStart());
		orderQuery.setCreatedDateEnd(query.getCreatedDateEnd());

		if(resultMaps.get(leaderName) == null){
			
			query.setUserName(leaderName);
			UserDayConsume leaderUserDayConsume = new UserDayConsume();
			UserSelfDayConsume userSelfDayConsume = userSelfDayConsumeService.getUserSelfDayConsume(query);
			if(userSelfDayConsume != null){
				BeanUtils.copyProperties(userSelfDayConsume, leaderUserDayConsume);
			}else{
				leaderUserDayConsume.setUserId(leaderUser.getId());
				leaderUserDayConsume.setUserName(query.getUserName());
				leaderUserDayConsume.setMoney(new BigDecimal(0)); //用户余额
				leaderUserDayConsume.setRecharge(new BigDecimal(0));
				leaderUserDayConsume.setRechargepresent(new BigDecimal(0));
				leaderUserDayConsume.setActivitiesmoney(new BigDecimal(0));  //活动彩金
		    	
				leaderUserDayConsume.setWithdraw(new BigDecimal(0));
				leaderUserDayConsume.setWithdrawfee(new BigDecimal(0));
				leaderUserDayConsume.setLottery(new BigDecimal(0));
				leaderUserDayConsume.setGain(new BigDecimal(0));
				leaderUserDayConsume.setWin(new BigDecimal(0));
				leaderUserDayConsume.setRebate(new BigDecimal(0));
				leaderUserDayConsume.setPercentage(new BigDecimal(0));
				leaderUserDayConsume.setHalfmonthbonus(new BigDecimal(0));
				leaderUserDayConsume.setDaybonus(new BigDecimal(0));
				leaderUserDayConsume.setDaysalary(new BigDecimal(0));
				leaderUserDayConsume.setMonthsalary(new BigDecimal(0));
				leaderUserDayConsume.setDaycommission(new BigDecimal(0));
				leaderUserDayConsume.setPaytranfer(new BigDecimal(0));
				leaderUserDayConsume.setIncometranfer(new BigDecimal(0));
				leaderUserDayConsume.setSystemaddmoney(new BigDecimal(0));
				leaderUserDayConsume.setSystemreducemoney(new BigDecimal(0));
				leaderUserDayConsume.setBizSystem(query.getBizSystem());
				leaderUserDayConsume.setRegCount(0);
				leaderUserDayConsume.setLotteryCount(0);
				leaderUserDayConsume.setFirstRechargeCount(0);
			}
			
			
			teamUserQuery.setTeamLeaderName(regform);
        	teamUserQuery.setUserName(leaderName);
        	orderQuery.setTeamLeaderName(regform);
        	orderQuery.setUserName(leaderName);
    		Integer regCount = userService.getTeamUserRegistersByQueryCount(teamUserQuery);
    		Integer firstRechargeCount = userService.getTeamUserfirstrechargeTimeByQueryCount(teamUserQuery);
    		Integer lotteryCount = userService.getTeamUserLotteryByQueryCount(orderQuery);
    		leaderUserDayConsume.setRegCount(regCount);
    		leaderUserDayConsume.setFirstRechargeCount(firstRechargeCount);
    		leaderUserDayConsume.setLotteryCount(lotteryCount);
    		resultMaps.put(leaderUserDayConsume.getUserName(), leaderUserDayConsume);
		}
		
		
        for(UserDayConsume userDayConsume:list){
        	if(resultMaps.get(userDayConsume.getUserName()) == null){
	        	teamUserQuery.setTeamLeaderName(userDayConsume.getRegfrom()+userDayConsume.getUserName()+"&");
	        	teamUserQuery.setUserName(userDayConsume.getUserName());
	        	orderQuery.setTeamLeaderName(userDayConsume.getRegfrom()+userDayConsume.getUserName()+"&");
	        	orderQuery.setUserName(userDayConsume.getUserName());
	    		Integer regCount = userService.getTeamUserRegistersByQueryCount(teamUserQuery);
	    		Integer firstRechargeCount = userService.getTeamUserfirstrechargeTimeByQueryCount(teamUserQuery);
	    	    Integer lotteryCount = userService.getTeamUserLotteryByQueryCount(orderQuery);
	    	    userDayConsume.setRegCount(regCount);
	    	    userDayConsume.setFirstRechargeCount(firstRechargeCount);
	    	    userDayConsume.setLotteryCount(lotteryCount);
	        	resultMaps.put(userDayConsume.getUserName(), userDayConsume);
        	}
        }
        
        //补充只注册,没有投注,首充操作没有产生报表记录
//        TeamUserQuery teQuery = new TeamUserQuery();
//        teQuery.setBizSystem(leaderUser.getBizSystem());
//        teQuery.setTeamLeaderName(leaderUser.getRegfrom()+leaderUser.getUserName()+"&");
//        teQuery.setRegisterDateStart(query.getCreatedDateStart());
//        teQuery.setRegisterDateEnd(query.getCreatedDateEnd());
//        List<UserDayConsume> tList = userDayConsumeService.getAllTeamUserList2(teQuery);
//        for(UserDayConsume emptyUserDayConsume:tList){
//        	if(resultMaps.get(emptyUserDayConsume.getUserName())==null){
//        		emptyUserDayConsume.setMoney(new BigDecimal(0)); //用户余额
//        		emptyUserDayConsume.setRecharge(new BigDecimal(0));
//				emptyUserDayConsume.setRechargepresent(new BigDecimal(0));
//				emptyUserDayConsume.setActivitiesmoney(new BigDecimal(0));  //活动彩金
//				emptyUserDayConsume.setWithdraw(new BigDecimal(0));
//				emptyUserDayConsume.setWithdrawfee(new BigDecimal(0));
//				emptyUserDayConsume.setLottery(new BigDecimal(0));
//				emptyUserDayConsume.setGain(new BigDecimal(0));
//				emptyUserDayConsume.setWin(new BigDecimal(0));
//				emptyUserDayConsume.setRebate(new BigDecimal(0));
//				emptyUserDayConsume.setPercentage(new BigDecimal(0));
//				emptyUserDayConsume.setHalfmonthbonus(new BigDecimal(0));
//				emptyUserDayConsume.setDaybonus(new BigDecimal(0));
//				emptyUserDayConsume.setDaysalary(new BigDecimal(0));
//				emptyUserDayConsume.setMonthsalary(new BigDecimal(0));
//				emptyUserDayConsume.setDaycommission(new BigDecimal(0));
//				emptyUserDayConsume.setPaytranfer(new BigDecimal(0));
//				emptyUserDayConsume.setIncometranfer(new BigDecimal(0));
//				emptyUserDayConsume.setSystemaddmoney(new BigDecimal(0));
//				emptyUserDayConsume.setSystemreducemoney(new BigDecimal(0));
//				emptyUserDayConsume.setBizSystem(query.getBizSystem());
//				if(emptyUserDayConsume.getRegCount() >0 || emptyUserDayConsume.getFirstRechargeCount() >0 || emptyUserDayConsume.getLotteryCount() >0){
//					resultMaps.put(emptyUserDayConsume.getUserName(), emptyUserDayConsume);
//				}	
//        	}
//        }
		return AjaxUtil.createReturnValueSuccess(resultMaps);
    }
    
    /**
     * 分页查找团队消费报表
     * @param query
     * @param pageNo
     * @return
     */
    public Object[] getTeamConsumeReportForQuery_old(MoneyDetailQuery query,Page page){
    	if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		if(!"SUPER_SYSTEM".equals(currentAdmin.getBizSystem())){
			query.setBizSystem(currentAdmin.getBizSystem());	
		}
//    	Page page = new Page(SystemSet.managerPageSize);
//    	page.setPageNo(pageNo);
    	String leaderName = query.getTeamLeaderName();
    	//团队领导人验证
        if(leaderName != null && !StringUtils.isEmpty(leaderName)){
        	if(leaderName.length() < 3){
        		return AjaxUtil.createReturnValueError("对不起,参数传递错误.");
        	}
        	leaderName = leaderName.substring(0,leaderName.length() - 2); //真实领导人用户名
        	
        	User leaderUser = userService.getUserByName(query.getBizSystem(),leaderName);
        	if(leaderUser == null){
        		return AjaxUtil.createReturnValueError("对不起,该用户不存在.");
        	}
        	String [] regFroms =leaderUser.getRegfrom().split(ConstantUtil.REG_FORM_SPLIT);
            if(regFroms == null || regFroms.length < 1){
        		return AjaxUtil.createReturnValueError("对不起,该用户的推荐注册有误.");
            }
            
            //后台团队盈亏报表根据用户名查询
            if(!StringUtils.isEmpty(query.getUserName())) {
            	
        		
            	User queryUser = userService.getUserByName(query.getBizSystem(),query.getUserName());
            	if(queryUser == null) {
            		return AjaxUtil.createReturnValueError("查询的用户名不存在");
            	}
            	String [] queryUserRegFroms =queryUser.getRegfrom().split(ConstantUtil.REG_FORM_SPLIT);
            	if(regFroms == null || regFroms.length < 1){
            		return AjaxUtil.createReturnValueError("对不起,查询用户的推荐注册有误.");
                }
            	boolean isSubLine = false;
            	for(String regForm : queryUserRegFroms) {
            		if(leaderName.equals(regForm)) {
            			isSubLine = true;
            			break;
            		}
            	}
            	if(!isSubLine) {
            		return AjaxUtil.createReturnValueError("对不起,查询用户的不是在当前用户的下级");
            	} else {
            		 leaderName = query.getUserName();
            	}
            }
            if(EUserDailLiLevel.SUPERAGENCY.getCode().equals(leaderUser.getDailiLevel())) {
        		query.setTeamLeaderName(leaderUser.getUserName() + ConstantUtil.REG_FORM_SPLIT);
        	} else {
        		query.setTeamLeaderName(ConstantUtil.REG_FORM_SPLIT + leaderUser.getUserName() + ConstantUtil.REG_FORM_SPLIT);
        	}
            query.setRealLeaderName(leaderName);
        }else{
        	return AjaxUtil.createReturnValueError("用户名必输!");
        }
        
      //查询时间的判断
        Date nowDateStart = new Date();
        Date yesterDateStart = DateUtil.addDaysToDate(nowDateStart, -1);
        yesterDateStart = DateUtil.getNowStartTimeByStart(yesterDateStart);
        int hourTime = DateUtil.getHourOfDay(nowDateStart);
        int minuteTime = DateUtil.getMis(nowDateStart);
        if(hourTime < ConstantUtil.DAY_COMSUME_END_HOUR || (hourTime == ConstantUtil.DAY_COMSUME_END_HOUR && minuteTime < ConstantUtil.DAY_COMSUME_END_MINUTE)) {
        	nowDateStart = DateUtil.addDaysToDate(nowDateStart, -1);
        }
        nowDateStart = DateUtil.getNowStartTimeByStart(nowDateStart);
        nowDateStart = DateUtil.addHoursToDate(nowDateStart, ConstantUtil.DAY_COMSUME_END_HOUR);
        nowDateStart = DateUtil.addMinutesToDate(nowDateStart, ConstantUtil.DAY_COMSUME_END_MINUTE);

		//历史报表查询时间处理
		if(MoneyDetailQuery.REPORT_TYPE_HISTORY == query.getReportType()) {
			//判断可以查询的时间
	        Date canQueryStartDate = DateUtil.addDaysToDate(new Date(), -1 * ConstantUtil.DELETE_AGO_DATA_NORMAL);
	        if(canQueryStartDate.after(query.getCreatedDateStart())) {
	        	return AjaxUtil.createReturnValueError("查询历史盈亏报表的起始时间只能在当前时间"+ConstantUtil.DELETE_AGO_DATA_NORMAL+"天之内");
	        }
	        if(nowDateStart.before(query.getCreatedDateEnd())) {
	        	return AjaxUtil.createReturnValueError("查询历史盈亏报表的结束时间超出盈亏结算时间");
	        }
	        
			Calendar historyReportDate = Calendar.getInstance();
			historyReportDate.setTime(query.getCreatedDateStart());
			historyReportDate.set(Calendar.HOUR_OF_DAY, ConstantUtil.DAY_COMSUME_END_HOUR);
			historyReportDate.set(Calendar.MINUTE, ConstantUtil.DAY_COMSUME_END_MINUTE);
			historyReportDate.set(Calendar.SECOND, 0);
			query.setCreatedDateStart(historyReportDate.getTime());
			
			historyReportDate.setTime(query.getCreatedDateEnd());
			historyReportDate.set(Calendar.HOUR_OF_DAY, ConstantUtil.DAY_COMSUME_END_HOUR);
			historyReportDate.set(Calendar.MINUTE, (ConstantUtil.DAY_COMSUME_END_MINUTE - 1));
			historyReportDate.set(Calendar.SECOND, 59);
			historyReportDate.set(Calendar.MILLISECOND, 999);
			query.setCreatedDateEnd(historyReportDate.getTime());
		//实时报表查询时间处理
		} else if(MoneyDetailQuery.REPORT_TYPE_TODAY == query.getReportType()) {
			//实时报表只允许查询两天
			if(query.getCreatedDateStart().before(yesterDateStart)) {
				return AjaxUtil.createReturnValueError("实时盈亏报表的起始时间只能查询两天以内的数据");
			}
			int queryStartMinuteTime = DateUtil.getMis(query.getCreatedDateStart());
			int queryStartSecTime = DateUtil.getSec(query.getCreatedDateStart());
			if(queryStartSecTime != 0 || queryStartMinuteTime != 0) {
				return AjaxUtil.createReturnValueError("为保证数据有效,实时盈亏报表的起始时间请使用整点查询");
			}
		}
        
    	Map<String, UserConsumeReportVo> resultMaps = moneyDetailService.getConsumeReportForReam(query,leaderName);
    	
    	/*List<MoneyDetail> moneyDetails = moneyDetailService.queryTeamMoneyDetailsByCondition(query);
   		//进行计算
   		Map<String,UserConsumeReportVo> consumeReportMaps = moneyDetailService.getMoneyDetailForuserMap(moneyDetails, query.getRealLeaderName());
   		UserConsumeReportVo userConsumeReportVo = consumeReportMaps.get(query.getRealLeaderName());
   		if(userConsumeReportVo != null) {
   			System.out.println(userConsumeReportVo.getGain());
   		}*/
    	
   //	 page = moneyDetailService.getConsumeReport(query, page);
		return AjaxUtil.createReturnValueSuccess(resultMaps);
    }
    
    
    /**
     * 查找所有用户的总额报表
     * @return
     */
    public Object[] getTeamAccountReportForQuery(UserQuery query,Page page){
    	if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	String leaderName = "";

    	//团队领导人验证
        if(query.getTeamLeaderName() != null && !StringUtils.isEmpty(query.getTeamLeaderName())){
        	leaderName = query.getTeamLeaderName();
        	if(leaderName.length() < 3){
        		return AjaxUtil.createReturnValueError("对不起,参数传递错误.");
        	}
        	leaderName = leaderName.substring(0,leaderName.length() - 2); //真实领导人用户名
        
        	User leaderUser = userService.getUserByName(query.getBizSystem(),leaderName);
        	if(leaderUser == null){
        		return AjaxUtil.createReturnValueError("对不起,该用户不存在.");
        	}
        	String [] regFroms =leaderUser.getRegfrom().split(ConstantUtil.REG_FORM_SPLIT);
            if(regFroms == null || regFroms.length < 1){
        		return AjaxUtil.createReturnValueError("对不起,该用户的推荐注册有误.");
            }
            query.setTeamLeaderName(ConstantUtil.REG_FORM_SPLIT + leaderName + ConstantUtil.REG_FORM_SPLIT);
        }else{
        	return AjaxUtil.createReturnValueError("参数传递错误.");
        }
       UserMoneyTotal userMoneyTotal=new UserMoneyTotal();
       userMoneyTotal.setBizSystem(query.getBizSystem());
       userMoneyTotal.setUserName(query.getUsername());
       userMoneyTotal.setTeamLeaderName(query.getTeamLeaderName());
       
    	page = userMoneyTotalService.getAllUserMoneyTotalByQueryPage(userMoneyTotal,page);
    	UserMoneyTotal totalUser = userMoneyTotalService.getTotalUsersConsumeReport(userMoneyTotal);
		return AjaxUtil.createReturnValueSuccess(page,totalUser);

    }
	
}
