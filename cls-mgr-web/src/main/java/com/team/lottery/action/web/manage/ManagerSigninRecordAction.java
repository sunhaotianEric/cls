package com.team.lottery.action.web.manage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.SigninRecrodQuery;
import com.team.lottery.service.SigninRecordService;
import com.team.lottery.service.UserService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.SigninRecord;
import com.team.lottery.vo.User;

@Controller("managerSigninRecordAction")
public class ManagerSigninRecordAction {

	@Autowired
	private SigninRecordService signinRecordService;
	@Autowired
	private UserService userService;
	
	/**
	 * 分页查询签到记录
	 * @param query
	 * @param page
	 * @return
	 */
	public Object[] querySigninRecord(SigninRecrodQuery query,Page page){
		if(query==null||page==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		Admin currentAdmin=BaseDwrUtil.getCurrentAdmin();
		if(!currentAdmin.getBizSystem().equals(ConstantUtil.SUPER_SYSTEM)){
			query.setBizSystem(currentAdmin.getBizSystem());
		}
		page=signinRecordService.querySigninRecord(query, page);
		return AjaxUtil.createReturnValueSuccess(page);
	}
	
	/**
	 * 根据ID查询单个签到记录信息
	 * @param id
	 * @return
	 */
	public Object[] querySigninRecordByid(int id){
		if(id>0){
			SigninRecord record=signinRecordService.querySigninRecordByid(id);
			return AjaxUtil.createReturnValueSuccess(record);
		}else{
			return AjaxUtil.createReturnValueError("参数错误");
		}
	}
	
	/**
	 * 添加签到记录
	 * @param record
	 * @return
	 */
	public Object[] savaSigninRecord(SigninRecord record){
		if(record==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		Admin currentAdmin=BaseDwrUtil.getCurrentAdmin();
		if(!currentAdmin.getBizSystem().equals(ConstantUtil.SUPER_SYSTEM)){
			record.setBizSystem(currentAdmin.getBizSystem());
		}
		User user=userService.getUserByName(record.getBizSystem(),record.getUserName());
		if(user==null){
			return AjaxUtil.createReturnValueError("用户不存在");
		}
		try {
			record.setUserId(user.getId());
			record.setSigninDate(record.getSigninTime());
			signinRecordService.savaSingninRecord(record);
			return AjaxUtil.createReturnValueSuccess();
		} catch (Exception e) {
			return AjaxUtil.createReturnValueError("新增失败");
		}
	}
	
	/**
	 * 根据ID修改签到记录
	 * @param record
	 * @return
	 */
	public Object[] updateSigninRecordById(SigninRecord record){
		if(record==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		signinRecordService.updateSigninRecord(record);
		return AjaxUtil.createReturnValueSuccess("修改成功");
	}
	
	/**
	 * 根据ID删除签到记录
	 * @param id
	 * @return
	 */
	public Object[] delSigninRecordByid(int id){
		if(id>0){
			signinRecordService.delSigninRecordById(id);
			return AjaxUtil.createReturnValueSuccess("删除成功");
		}else{
			return AjaxUtil.createReturnValueError("参数错误");
		}
	}
}
