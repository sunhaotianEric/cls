package com.team.lottery.action.web.manage;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.Page;
import com.team.lottery.service.GameRuleService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.vo.GameRule;

@Controller("manageGameRuleAction")
public class ManageGameRuleAction {
	
	@Autowired
	private GameRuleService gameRuleService;
	
	/**
	 * 分页查询
	 * @param query
	 * @param page
	 * @return
	 */
	public Object[] getAllGameRulePage(GameRule query,Page page){
		if(query==null||page==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		page=gameRuleService.getAllGameRule(query, page);
		return AjaxUtil.createReturnValueSuccess(page);
	}
	
	/**
	 * 根据ID查询
	 * @param id
	 * @return
	 */
	public Object[] selectGameRule(Integer id){
		GameRule record=gameRuleService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(record);
	}
	
	/**
	 * 修改
	 * @param record
	 * @return
	 */
	public Object[] updateGameRule(GameRule record){
		if(record==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		gameRuleService.updateByPrimaryKeySelective(record);
		return AjaxUtil.createReturnValueSuccess();
	}
	
	/**
	 * 根据ID删除
	 * @param id
	 * @return
	 */
	public Object[] delGameRule(Integer id){
		if(id==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		gameRuleService.deleteByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess();
	}
	
	/**
	 * 保存
	 * @param record
	 * @return
	 */
	public Object[] saveGameRule(GameRule record){
    	if(record == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	Integer id = record.getId();
    	if(id == null){
    		record.setCreateDate(new Date());
    		gameRuleService.insertSelective(record);
    	}else{
    		record.setUpdateDate(new Date());
    		gameRuleService.updateByPrimaryKeySelective(record);
    	}
    	return AjaxUtil.createReturnValueSuccess();
    }

}
