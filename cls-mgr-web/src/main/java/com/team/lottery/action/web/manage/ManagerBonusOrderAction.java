package com.team.lottery.action.web.manage;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.enums.EBonusOrderStatus;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.BonusOrderService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.BonusOrder;

@Controller("managerBonusOrderAction")
public class ManagerBonusOrderAction {
	
	@Autowired
	private BonusOrderService bounBonusOrderService;
	
	
	 /**
     * 根据条件查找
     * @return
     */
    public Object[] getBonusOrders(BonusOrder query,Page page){
    	if(query == null||page==null){
    		return AjaxUtil.createReturnValueError("参数传递错误！");	
    	}
    	Admin admin=BaseDwrUtil.getCurrentAdmin();
    	//特殊情况处理--逾期为发放
    	if(query.getReleaseStatus()!=null&&query.getReleaseStatus().equals(EBonusOrderStatus.OUTTIME_NOT_RELEASE.name())){
    		query.setIsQueryOuttimeNotRelease(true);
    	}
    	if(!"SUPER_SYSTEM".equals(admin.getBizSystem())){
    		query.setBizSystem(admin.getBizSystem());
    	}
    	
    	page=bounBonusOrderService.getMgrBonusOrderByQueryPage(query, page);
		return AjaxUtil.createReturnValueSuccess(page);
    }
    
    
    /**
     * 发放契约分红
     * @return
     */
    public Object[] releaseBounsMoney(Long id,String releaseStatus){
    	if(id == null||releaseStatus==null){
    		return AjaxUtil.createReturnValueError("参数传递错误！");	
    	}
    	Admin admin=BaseDwrUtil.getCurrentAdmin();
    	BonusOrder query=new BonusOrder();
    	query.setId(id);
    	query.setReleaseStatus(releaseStatus);
    	
    	int result=0;
    	try {
			 result=bounBonusOrderService.managerReleaseBounsMoney(query,admin);
		} catch (Exception e) {
			result=0;
			return AjaxUtil.createReturnValueError("发放失败:"+e.getMessage());
		}
    	if(result>0){
    		return AjaxUtil.createReturnValueSuccess();
    	}else{
    		return AjaxUtil.createReturnValueError("发放失败:");
    	}
		
    }
    
    
    /**
     * 根据条件查找
     * @return
     */
    public Object[] getBonusOrderById(Long id){
    	if(id == null){
    		return AjaxUtil.createReturnValueError("参数传递错误！");	
    	}
    	BonusOrder bonusOrder=new BonusOrder();
    	bonusOrder.setId(id);
    	List<BonusOrder> list=bounBonusOrderService.getBonusOrderByQuery(bonusOrder);
    	if(list.size()>0){
    	   return AjaxUtil.createReturnValueSuccess(list.get(0));
    	}else{
    		return AjaxUtil.createReturnValueError();
    	}
    }	
    /**
     * 更新保存
     * @return
     */
    public Object[] saveOrUpdateBonusOrder(BonusOrder bonusOrder){
    	//BonusOrder bonusOrder=new BonusOrder();
    	if(bonusOrder == null||bonusOrder.getId()==null){
    		return AjaxUtil.createReturnValueError("参数传递错误！");	
    	}

    	if(bonusOrder.getGain().compareTo(BigDecimal.ZERO)<0){
	    	if(bonusOrder.getGain().multiply(new BigDecimal("-1")).multiply(bonusOrder.getBonusScale()).compareTo(bonusOrder.getMoney())==0){
	    		return AjaxUtil.createReturnValueError("盈亏乘以分红比例必须等于应发分红！请重新输入！");
	    	}
    	}else {
    		if(bonusOrder.getMoney().compareTo(BigDecimal.ZERO)>0){
    			return AjaxUtil.createReturnValueError("盈亏为正数，分红应该为0！");	
    		}	
    	}
   
    	
    	int result=bounBonusOrderService.updateByPrimaryKeySelective(bonusOrder);
    	if(result>0){
    	   return AjaxUtil.createReturnValueSuccess();
    	}else{
    		return AjaxUtil.createReturnValueError();
    	}
    }	
}
