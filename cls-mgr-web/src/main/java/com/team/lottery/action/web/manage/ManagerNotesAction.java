package com.team.lottery.action.web.manage;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.enums.EUserDailLiLevel;
import com.team.lottery.extvo.NotesQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.SendNoteQueryVo;
import com.team.lottery.service.NotesService;
import com.team.lottery.service.UserService;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.User;

@Controller("managerNotesAction")
public class ManagerNotesAction {
	
	private Logger logger = LoggerFactory.getLogger(ManagerNotesAction.class);

	@Autowired
	private NotesService notesService;
	@Autowired
	private UserService userService;
	
	
	/**
     * 查找所有的客服数据
     * @return
     */
    public Object[] getAllNotes(NotesQuery query,Integer pageNo){
    	if(query == null || pageNo == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	Page page = new Page(SystemConfigConstant.defaultMgrPageSize);
    	page.setPageNo(pageNo);
    	page = notesService.getAllNotesPage(query,page);
		return AjaxUtil.createReturnValueSuccess(page);
    }
    
    /**
     * 发送站内信
     * @return
     */
    public Object[] userNoteSend(SendNoteQueryVo sendVo){
    	if(sendVo == null || sendVo.getSendType() == null || sendVo.getSub() == null || sendVo.getBody() == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	List<User> sendNoteUsers = null;
    	Admin admin=BaseDwrUtil.getCurrentAdmin();
    	if(!"SUPER_SYSTEM".equals(admin.getBizSystem())){
    		sendVo.setBizSystem(admin.getBizSystem());;
    	}
    	if(sendVo.getSendType() == 1) {
    		sendVo.setSscrebate(null);
    		sendNoteUsers = userService.getUsersBySendNoteQuery(sendVo);
    	} else if(sendVo.getSendType() == 2) {
    		List<String> dailiLevels = new ArrayList<String>();
    		dailiLevels.add(sendVo.getDailiLevel());
    		//小于等于
    		if(sendVo.getFilterCondition() == 2) {
	    		if(EUserDailLiLevel.DIRECTAGENCY.getCode().equals(sendVo.getDailiLevel())) {
	    			dailiLevels.add(EUserDailLiLevel.GENERALAGENCY.getCode());
	    			dailiLevels.add(EUserDailLiLevel.ORDINARYAGENCY.getCode());
	    			dailiLevels.add(EUserDailLiLevel.REGULARMEMBERS.getCode());
	    		} else if(EUserDailLiLevel.GENERALAGENCY.getCode().equals(sendVo.getDailiLevel())) {
	    			dailiLevels.add(EUserDailLiLevel.ORDINARYAGENCY.getCode());
	    			dailiLevels.add(EUserDailLiLevel.REGULARMEMBERS.getCode());
	    		} else if(EUserDailLiLevel.ORDINARYAGENCY.getCode().equals(sendVo.getDailiLevel())) {
	    			dailiLevels.add(EUserDailLiLevel.REGULARMEMBERS.getCode());
	    		}
	    	//大于等于
    		} else if(sendVo.getFilterCondition() == 3) {
    			if(EUserDailLiLevel.REGULARMEMBERS.getCode().equals(sendVo.getDailiLevel())) {
    				dailiLevels.add(EUserDailLiLevel.DIRECTAGENCY.getCode());
    				dailiLevels.add(EUserDailLiLevel.GENERALAGENCY.getCode());
	    			dailiLevels.add(EUserDailLiLevel.ORDINARYAGENCY.getCode());
	    		} else if(EUserDailLiLevel.ORDINARYAGENCY.getCode().equals(sendVo.getDailiLevel())) {
	    			dailiLevels.add(EUserDailLiLevel.DIRECTAGENCY.getCode());
    				dailiLevels.add(EUserDailLiLevel.GENERALAGENCY.getCode());
	    		} else if(EUserDailLiLevel.GENERALAGENCY.getCode().equals(sendVo.getDailiLevel())) {
	    			dailiLevels.add(EUserDailLiLevel.DIRECTAGENCY.getCode());
	    		} 
    		}
    		sendVo.setDailiLevels(dailiLevels);
    		sendVo.setStarLevel(null);
    		sendVo.setSscrebate(null);
    		sendNoteUsers = userService.getUsersBySendNoteQuery(sendVo);
    	} else if(sendVo.getSendType() == 3) {
    		sendVo.setStarLevel(null);
    		sendNoteUsers = userService.getUsersBySendNoteQuery(sendVo);
    	} else if(sendVo.getSendType() == 4) {
    		if(sendVo.getBizSystem()==null || "".equals(sendVo.getBizSystem())){
    			return AjaxUtil.createReturnValueError("按用户名发送需要指定业务系统，谢谢！");	
    		}
    		User sendUser = userService.getUserByName(sendVo.getBizSystem(),sendVo.getUserName());
    		if(sendUser != null) {
    			sendNoteUsers = new ArrayList<User>();
    			sendNoteUsers.add(sendUser);
    		}
    	}
    	int sendUserCount = 0;
    	if(CollectionUtils.isNotEmpty(sendNoteUsers)) {
    		sendUserCount = sendNoteUsers.size();
    		try {
    			logger.info("管理员["+BaseDwrUtil.getCurrentAdmin().getUsername()+"]向["+sendUserCount+"]个用户发送了站内信通知，"
    					+ "主题["+sendVo.getSub()+"],内容["+sendVo.getBody()+"]");
				notesService.sendNoteToUsers(sendNoteUsers, sendVo);
			} catch (Exception e) {
				logger.error("发送站内信消息失败...", e);
				AjaxUtil.createReturnValueError("发送站内信消息失败");
			}
    	}
		return AjaxUtil.createReturnValueSuccess(sendUserCount);
    }
    
    /**
     * 分页查询列表
     * @param notes
     * @return
     */
	public Object[] notesList(NotesQuery query,Page page){
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	//非SUPER_SYSTEM,只能查询本业务系统的用户
    	if(!"SUPER_SYSTEM".equals(currentAdmin.getBizSystem()))
    	{
    		query.setBizSystem(currentAdmin.getBizSystem());
    	}    	
    	page = notesService.getAllNotesByQueryPage(query,page);
		return AjaxUtil.createReturnValueSuccess(page);
    }
    
    /**
     * 根据ID删除站内信息
     * @param userId
     * @return
     */
    public Object[] deleteNotes(Long notesId){
    	if(notesId == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	notesService.deleteByPrimaryKey(notesId);
		return AjaxUtil.createReturnValueSuccess();
    }
    
}
