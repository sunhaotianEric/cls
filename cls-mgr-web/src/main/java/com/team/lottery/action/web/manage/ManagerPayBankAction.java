package com.team.lottery.action.web.manage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.directwebremoting.io.FileTransfer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.enums.EAdminOperateLogModel;
import com.team.lottery.enums.EBankInfo;
import com.team.lottery.extvo.ImageUploadResult;
import com.team.lottery.extvo.PayBankVo;
import com.team.lottery.extvo.RechargeConfigVo;
import com.team.lottery.service.AdminOperateLogService;
import com.team.lottery.service.AdminService;
import com.team.lottery.service.PayBankService;
import com.team.lottery.service.RechargeConfigService;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.system.message.MessageQueueCenter;
import com.team.lottery.system.message.messagetype.SendMailMessage;
import com.team.lottery.util.AdminOperateUtil;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.HttpClientUtil;
import com.team.lottery.util.JSONUtils;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.PayBank;
import com.team.lottery.vo.RechargeConfig;

@Controller("managerPayBankAction")
public class ManagerPayBankAction {
	
	private static Logger logger = LoggerFactory.getLogger(ManagerPayBankAction.class);

	@Autowired
	private PayBankService payBankService;
	@Autowired
	private AdminService adminService;
	@Autowired
	private AdminOperateLogService adminOperateLogService;
	@Autowired
	private RechargeConfigService rechargeConfigService;
	
    /**
     * 查找所有的收款银行数据
     * @return
     */
    public Object[] getAllPayBank(PayBankVo query){
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	/*String adminName = null;
    	if(currentAdmin.getState() != 0){
    		adminName = currentAdmin.getUsername();
    		query.setAdminName(adminName);
    	}*/
    	if(query.getBankName()!=null)
    	{
    		query.setBankName(EBankInfo.valueOf(query.getBankName()).getDescription());
    	}
    	if(!"SUPER_SYSTEM".equals(currentAdmin.getBizSystem()))
    	{
    		query.setBizSystem(currentAdmin.getBizSystem());
    	}
		List<PayBank> payBanks =  payBankService.getAllPayBanks(query);
		return AjaxUtil.createReturnValueSuccess(payBanks);
    }

    /**
     * 添加收款银行 
     * @return
     */
    public Object[] saveOrUpdatePayBank(PayBank record){
    	try{
    		/*Admin adminTmp = new Admin();
    		adminTmp.setUsername(record.getAdminName());
    		List<Admin> selectAdmins = adminService.getAllAdminByUsername(adminTmp);
    		if(selectAdmins!=null && selectAdmins.size()!=0){
    			Admin selectAdmin = selectAdmins.get(0);
    			record.setAdminId(selectAdmin.getId());
    		}else{
    			return AjaxUtil.createReturnValueError("管理员数据有误，请检查后重试");
    		}*/
    		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    		record.setBankName(EBankInfo.valueOf(record.getBankType()).getDescription());
    		if(!"SUPER_SYSTEM".equals(currentAdmin.getBizSystem()))
    		{
    			record.setBizSystem(currentAdmin.getBizSystem());
    		}
    		if(record.getId()!=null){
    			//保存操作日志
    			savePayBankEditRecordLog(record, record.getId());
    			record.setUpdateAdmin(currentAdmin.getUsername());
    			record.setUpdateTime(new Date());
    			payBankService.updateByPrimaryKeySelective(record);
    		}else{
    			//保存操作日志
    			record.setAdminId(currentAdmin.getId());
    			record.setAdminName(currentAdmin.getUsername());
    			record.setCreateTime(new Date());
    			record.setYetUse(0);
    			savePayBankRecordLog("paybank_add", record, null);
    			payBankService.insertSelective(record);
    		}
    	}catch(Exception e){
        	return AjaxUtil.createReturnValueError(e.getMessage());
    	}
    	
    	return AjaxUtil.createReturnValueSuccess();
    }
    
    /**
     * 删除收款银行 
     * @return
     */
    public Object[] delPayBank(Long id){
    	try{
    		//保存操作日志
			savePayBankRecordLog("paybank_del", null, id);
			RechargeConfigVo query=new RechargeConfigVo();
			query.setRefId(id);
			PayBank payBank=payBankService.selectByPrimaryKey(id);
			if(payBank.getBizSystem()!=null && !payBank.getBizSystem().equals(""))
			{
				 query.setBizSystem(payBank.getBizSystem());
			}
		   
    		
			List<RechargeConfig> list=rechargeConfigService.selectRechargeConfig(query);
			if(list != null && list.size()>0){
	    		return AjaxUtil.createReturnValueError("该收款银行已经绑定相关充值配置！请先删除相关的充值配置或者重新绑定其他收款银行!");
	    	}
    		payBankService.deleteByPrimaryKey(id);
    	}catch(Exception e){
        	return AjaxUtil.createReturnValueError(e.getMessage());
    	}
    	
    	return AjaxUtil.createReturnValueSuccess();
    }
    
    /**
     * 开启收款银行 
     * @return
     */
    public Object[] openPayBank(Long id){
    	try{
    		PayBank oldPayBank = payBankService.selectByPrimaryKey(id);
    		//保存操作日志
			savePayBankRecordLog("paybank_open", oldPayBank, null);
    		oldPayBank.setIsDisabled(1);
    		payBankService.updateByPrimaryKey(oldPayBank);
    	}catch(Exception e){
        	return AjaxUtil.createReturnValueError(e.getMessage());
    	}
    	
    	return AjaxUtil.createReturnValueSuccess();
    }
    
    /**
     * 关闭收款银行 
     * @return
     */
    public Object[] closePayBank(Long id){
    	try{
    		PayBank oldPayBank = payBankService.selectByPrimaryKey(id);
    		//保存操作日志
			savePayBankRecordLog("paybank_close", oldPayBank, null);
    		oldPayBank.setIsDisabled(0);
    		payBankService.updateByPrimaryKey(oldPayBank);
    	}catch(Exception e){
        	return AjaxUtil.createReturnValueError(e.getMessage());
    	}
    	
    	return AjaxUtil.createReturnValueSuccess();
    }
    
    /**
     * @deprecated 批量保存使用
     * @param payBanks
     * @return
     */
    public Object[] updateAllPayBanks(List<PayBank> payBanks){
    	if(payBanks == null){
    		return AjaxUtil.createReturnValueError("参数传递错误");
    	}
    	for(PayBank payBank : payBanks){
    		Admin admin = new Admin();
    		admin.setUsername(payBank.getAdminName());
    		List<Admin> updateAdmins = adminService.getAllAdminByUsername(admin);
    		if(updateAdmins.size() == 0){
    			return AjaxUtil.createReturnValueError("管理员不存在，请检查后重新设置");
    		}else{
    			payBank.setAdminId(updateAdmins.get(0).getId());
    		}
    		payBank.setBankName(EBankInfo.valueOf(payBank.getBankType()).getDescription());
    		payBankService.updateByPrimaryKeySelective(payBank);
    	}
		return AjaxUtil.createReturnValueSuccess();
    }
    
    /**
     * 初始化管理员银行卡管理
     * @param payBanks
     * @return
     */
    public Object[] initForPayBankPage(){
    	List<Admin> initAdmins = adminService.getAllAdmins();
    	EBankInfo [] bankInfos = EBankInfo.values();
    	String codes[] = new String[bankInfos.length];
    	String descriptions[] = new String[bankInfos.length];
    	for(int i=0;i<bankInfos.length;i++){
    		EBankInfo bankInfo = bankInfos[i];
    		codes[i] = bankInfo.getCode();
    		descriptions[i] = bankInfo.getDescription();
    	}
    	return AjaxUtil.createReturnValueSuccess(initAdmins,codes,descriptions);
    }
    
    /**
     * 查找指定的银行卡数据
     * @return
     */
    public Object[] getPayBankById(Long id){
    	PayBank payBank =  payBankService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(payBank, SystemConfigConstant.imgServerUrl);
    }
    
    
    
    /**
     * 保存银行卡操作的日志
     * @param key
     * @param domain
     * @throws Exception 
     */
    private void savePayBankRecordLog(String key, PayBank payBank, Long id) throws Exception {
    	if(payBank == null) {
    		payBank = payBankService.selectByPrimaryKey(id);
    	}
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	String disableStatus = payBank.getIsDisabled() == 1 ? "开启" : "关闭";
    	String operateDesc = AdminOperateUtil.constructMessage(key, currentAdmin.getUsername(), payBank.getAdminName(), payBank.getBankName(), 
    			payBank.getSubbranchName(), payBank.getBankId(), payBank.getBankUserName(), payBank.getAliasName(), disableStatus);
    	try {
    		String operationIp = BaseDwrUtil.getRequestIp();
    		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.PAYBANK, operateDesc);
    	} catch (Exception e) {
    		//保存日志出现异常 不进行处理
    		logger.error("修改银行卡信息，保存添加操作日志出错", e);
    	}
    	
    	//发送邮件通知，放置到消息队列中
		/*SendMailMessage mailMessage = new SendMailMessage();
		mailMessage.setSubject("系统告警(银行卡编辑告警)");
		mailMessage.setContent(operateDesc);
		MessageQueueCenter.putMessage(mailMessage);
    			
    	//发送短信通知
    	String result = SmsUtil.SendSms(operateDesc);
    	if("error".equals(result)) {
    		throw new Exception("系统错误，请重试");
    	}*/
    }
    
    /**
     * 保存银行卡编辑操作的操作日志
     * @param key
     * @param domain
     * @throws Exception 
     */
    private void savePayBankEditRecordLog(PayBank payBank, Long id) throws Exception {
    	
		PayBank oldPayBank = payBankService.selectByPrimaryKey(id);
		StringBuffer modifyStr = new StringBuffer();
		if(payBank.getAdminName().equals(oldPayBank.getAdminName())) {
			
		}
		//修改了银行名称
		if(StringUtils.isEmpty(oldPayBank.getBankName()) && !StringUtils.isEmpty(payBank.getBankName())
		  ||!StringUtils.isEmpty(oldPayBank.getBankName()) && !oldPayBank.getBankName().equals(payBank.getBankName())) {
			modifyStr.append(AdminOperateUtil.constructMessage("paybank_edit_bankName", oldPayBank.getBankName(), payBank.getBankName()));
		}
		//修改了支行名称
		if(StringUtils.isEmpty(oldPayBank.getSubbranchName()) && !StringUtils.isEmpty(payBank.getSubbranchName())
		  ||!StringUtils.isEmpty(oldPayBank.getSubbranchName()) && !oldPayBank.getSubbranchName().equals(payBank.getSubbranchName())) {
			modifyStr.append(AdminOperateUtil.constructMessage("paybank_edit_subBranchName", oldPayBank.getSubbranchName(), payBank.getSubbranchName()));
		}
		//修改了银行卡号
		if(StringUtils.isEmpty(oldPayBank.getBankId()) && !StringUtils.isEmpty(payBank.getBankId())
		  ||!StringUtils.isEmpty(oldPayBank.getBankId()) && !oldPayBank.getBankId().equals(payBank.getBankId())) {
			modifyStr.append(AdminOperateUtil.constructMessage("paybank_edit_bankId", oldPayBank.getBankId(), payBank.getBankId()));
		}
		//修改了卡号别名
		if(StringUtils.isEmpty(oldPayBank.getAliasName()) && !StringUtils.isEmpty(payBank.getAliasName())
		  ||!StringUtils.isEmpty(oldPayBank.getAliasName()) && !oldPayBank.getAliasName().equals(payBank.getAliasName())) {
			modifyStr.append(AdminOperateUtil.constructMessage("paybank_edit_aliasName", oldPayBank.getAliasName(), payBank.getAliasName()));
		}
		//修改了开户名称
		if(StringUtils.isEmpty(oldPayBank.getBankUserName()) && !StringUtils.isEmpty(payBank.getBankUserName())
		  ||!StringUtils.isEmpty(oldPayBank.getBankUserName()) && !oldPayBank.getBankUserName().equals(payBank.getBankUserName())) {
			modifyStr.append(AdminOperateUtil.constructMessage("paybank_edit_bankUserName", oldPayBank.getBankUserName(), payBank.getBankUserName()));
		}
		//修改了是启用状态
		if(oldPayBank.getIsDisabled().compareTo(payBank.getIsDisabled()) != 0) {
			String oldDisableStatus = oldPayBank.getIsDisabled() == 1 ? "开启" : "关闭";
			String disableStatus = payBank.getIsDisabled() == 1 ? "开启" : "关闭";
			modifyStr.append(AdminOperateUtil.constructMessage("paybank_edit_isDisabled", oldDisableStatus, disableStatus));
		}
		
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		
		//保存日志
		String operateDesc = AdminOperateUtil.constructMessage("paybank_edit", currentAdmin.getUsername(), modifyStr.toString());
		String operationIp = BaseDwrUtil.getRequestIp();
		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.PAYBANK, operateDesc);
    	
    	//发送邮件通知，放置到消息队列中
		SendMailMessage mailMessage = new SendMailMessage();
		mailMessage.setSubject("系统告警(银行卡编辑告警)");
		mailMessage.setContent(operateDesc);
		MessageQueueCenter.putMessage(mailMessage);
		
		//发送短信通知
    	/*String result = SmsUtil.SendSms(operateDesc);
    	if("error".equals(result)) {
    		throw new Exception("系统错误，请重试");
    	}*/
    }
    
    
    /**
	 * 文件上传
	 * 
	 * @return
	 */
	public Object[] uploadImgFile(FileTransfer fileTransfer, String bizSystem) {
		try {
			Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    		if(!"SUPER_SYSTEM".equals(currentAdmin.getBizSystem())) {
    			bizSystem = currentAdmin.getBizSystem();
    		}
			
			//判断上传文件的类型
			String uploadFileName = fileTransfer.getFilename();
			int index = uploadFileName.indexOf(".");
			String fileType = "";
			if(index > 0) {
				fileType = uploadFileName.substring(index);
				if(!fileType.equalsIgnoreCase(".jpg") && !fileType.equalsIgnoreCase(".png")) {
					return AjaxUtil.createReturnValueError("上传图片文件类型错误，请重新选择");
				}
			}
			if(StringUtils.isEmpty(fileType)) {
				return AjaxUtil.createReturnValueError("上传图片文件类型错误，请重新选择");
			}
			
			//生成文件名称  根据日期
			String newFileName = DateUtil.getDate(new Date(), "yyyyMMddHHmmssSS");
			newFileName += fileType;
			
			WebContext webContext = WebContextFactory.get();
			// String realtivepath = webContext.getServletContext().getContextPath()
			// + "/upload/";
			String saveurl = webContext.getHttpServletRequest().getSession()
					.getServletContext().getRealPath("/upload");
			File fileDir = new File(saveurl);
			if (!fileDir.exists()) {
				fileDir.mkdirs();
			}
			String saveFileUrl = saveurl + "/" + newFileName;
			File file = new File(saveFileUrl);
			InputStream uploadFile = fileTransfer.getInputStream(); 
			int available = uploadFile.available();
			byte[] b = new byte[available];
			FileOutputStream foutput = new FileOutputStream(file);
			uploadFile.read(b);
			foutput.write(b);
			foutput.flush();
			foutput.close();
			uploadFile.close();
			logger.info("接收收款图片上传到本地成功，本地图片路径地址[{}],开始上传到图片服务器...", saveFileUrl);
			
			//使用http接口上传图片
			String picSavePath = bizSystem + "/bankcode";
			String fullSavePath = picSavePath + "/" + newFileName;
			Map<String, String> params = new HashMap<String, String>();
			params.put("path", picSavePath);
			params.put("fileName", newFileName);
			String uploadResult = HttpClientUtil.doPostFileUpload(SystemConfigConstant.imgServerUploadUrl+ConstantUtil.FILE_UPLOAD_RESOURCE, file, params);
			logger.info("上传图片结果：{}", uploadResult);
			
			ImageUploadResult result = JSONUtils.toBean(uploadResult, ImageUploadResult.class);
			if(result == null) {
				return AjaxUtil.createReturnValueError("文件上传图片服务器错误，原因:返回结果为空");
			} 
			if("error".equals(result.getResult())) {
				String mString = result.getMsg();
				return AjaxUtil.createReturnValueError("文件上传图片服务器错误，原因:" + mString);
			}
			//资源服务器上图片访问url地址
//			String fileUrl = "";
//			try{
//				//上传文件至ftp
//   				fileUrl = SftpClientUtil.uploadFileToSFTP(saveFileUrl, "bank_barcode", newFileName);
//			} catch (Exception e) {
//				logger.error("上传文件至ftp出错", e);
//				return AjaxUtil.createReturnValueError("上传文件至图片服务器失败");
//			}
			logger.info("上传收款图片到图片服务器成功，删除本地图片文件");
			//删除文件
			file.delete();
			
			return AjaxUtil.createReturnValueSuccess(fullSavePath, SystemConfigConstant.imgServerUrl);
		} catch(Exception e){
			logger.error("文件上传失败", e);
			return AjaxUtil.createReturnValueError("文件上传失败");
		}
		
	}
	
}
