package com.team.lottery.action.web.manage;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.OrderQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.TeamUserQuery;
import com.team.lottery.extvo.UserDayConsumeQuery;
import com.team.lottery.service.UserDayConsumeService;
import com.team.lottery.service.UserService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.DateUtil;
import com.team.lottery.vo.UserDayConsume;

@Controller("manageAgentReportAction")
public class ManageAgentReportAction {
	
	@Autowired
	private UserDayConsumeService userDayConsumeService;
	@Autowired
	private UserService userService;
	/**
	 * 代理报表分页查询
	 * @param query
	 * @param page
	 * @return
	 */
	public Object[] getAgentReport(UserDayConsumeQuery query,Page page){
		if(query==null||page==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		Calendar startDate = Calendar.getInstance();
		Calendar endDate = Calendar.getInstance();
		startDate.setTime(query.getBelongDateStart());
		startDate.set(Calendar.HOUR_OF_DAY, ConstantUtil.DAY_COMSUME_END_HOUR);
		startDate.set(Calendar.MINUTE, ConstantUtil.DAY_COMSUME_END_MINUTE);
		startDate.set(Calendar.SECOND, 0);
		
		endDate.setTime(query.getBelongDateEnd());
		endDate.add(Calendar.DAY_OF_MONTH, 1);
		endDate.set(Calendar.HOUR_OF_DAY, ConstantUtil.DAY_COMSUME_END_HOUR);
		endDate.set(Calendar.MINUTE, ConstantUtil.DAY_COMSUME_END_MINUTE);
		endDate.set(Calendar.SECOND, 0);
		//统计注册人数，首冲人数，投注人数
		TeamUserQuery teamUserQuery = new TeamUserQuery();
		teamUserQuery.setRegisterDateStart(startDate.getTime());
		teamUserQuery.setRegisterDateEnd(endDate.getTime());
		OrderQuery orderQuery =  new OrderQuery();
		orderQuery.setCreatedDateStart(startDate.getTime());
		orderQuery.setCreatedDateEnd(endDate.getTime());
	
		Date startDateTime = DateUtil.getNowStartTimeByStart(query.getBelongDateStart());
		Date endDateTime = DateUtil.getNowStartTimeByEnd(query.getBelongDateEnd());
		query.setBelongDateStart(startDateTime);
		query.setBelongDateEnd(endDateTime);
	
		
		String bizSystem = BaseDwrUtil.getCurrentAdmin().getBizSystem();
		if(!bizSystem.equals(ConstantUtil.SUPER_SYSTEM)){
			query.setBizSystem(bizSystem);
		}
		page=userDayConsumeService.getAgentReportPage(query, page);
		List<UserDayConsume> list = (List<UserDayConsume>) page.getPageContent();
		for(UserDayConsume userDayConsume:list){
			teamUserQuery.setUserName(userDayConsume.getUserName());
			teamUserQuery.setTeamLeaderName(userDayConsume.getRegfrom()+userDayConsume.getUserName()+"&");
			orderQuery.setUserName(userDayConsume.getUserName());
			orderQuery.setTeamLeaderName(userDayConsume.getRegfrom()+userDayConsume.getUserName()+"&");
			Integer regCount = userService.getTeamUserRegistersByQueryCount(teamUserQuery);
			Integer firstRechargeCount = userService.getTeamUserfirstrechargeTimeByQueryCount(teamUserQuery);
		    Integer lotteryCount = userService.getTeamUserLotteryByQueryCount(orderQuery);
		    userDayConsume.setRegCount(regCount);
		    userDayConsume.setFirstRechargeCount(firstRechargeCount);
		    userDayConsume.setLotteryCount(lotteryCount);
		}
		return AjaxUtil.createReturnValueSuccess(page);
	}
}
