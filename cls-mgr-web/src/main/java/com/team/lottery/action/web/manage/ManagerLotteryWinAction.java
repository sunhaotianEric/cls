package com.team.lottery.action.web.manage;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.LotteryWinQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.LotteryWinService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.vo.LotteryWin;

@Controller("managerLotteryWinAction")
public class ManagerLotteryWinAction {

	@Autowired
	private LotteryWinService lotteryWinService;
	
    /**
     * 查找所有的奖金配置数据
     * @return
     */
    public Object[] getAllLotteryWin(LotteryWinQuery query,Page page){
    	if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	page = lotteryWinService.getAllLotteryWinByQueryPage(query,page);
		return AjaxUtil.createReturnValueSuccess(page);
    }

    /**
     * 添加奖金配置 
     * @return
     */
    public Object[] addLotteryWin(LotteryWin record){
    	String bizSystem = BaseDwrUtil.getCurrentAdmin().getBizSystem();
    	if(!bizSystem.equals("SUPER_SYSTEM")){
    		return AjaxUtil.createReturnValueError("亲,你暂时还没权限添加");
    	}
    	
    	try{
    		lotteryWinService.insertSelective(record);
    	}catch(Exception e){
        	return AjaxUtil.createReturnValueError(e.getMessage());
    	}
    	
    	return AjaxUtil.createReturnValueSuccess();
    }
    
    /**
     * 删除奖金配置 
     * @return
     */
    public Object[] delLotteryWin(Integer id){
    	String bizSystem = BaseDwrUtil.getCurrentAdmin().getBizSystem();
    	if(!bizSystem.equals("SUPER_SYSTEM")){
    		return AjaxUtil.createReturnValueError("亲,你暂时还没权限删除");
    	}
    	
    	try{
    		lotteryWinService.deleteByPrimaryKey(id);
    	}catch(Exception e){
        	return AjaxUtil.createReturnValueError(e.getMessage());
    	}
    	
    	return AjaxUtil.createReturnValueSuccess();
    }
    
    public Object[] getLotteryWinById(Integer id){
    	LotteryWin lotteryWin = lotteryWinService.selectByPrimaryKey(id);
    	return AjaxUtil.createReturnValueSuccess(lotteryWin);
    }
    
    /**
     * 修改奖金配置 
     * @param payBanks
     * @return
     */
    public Object[] updateLotteryWin(LotteryWin lotteryWin){
    	
    	String bizSystem = BaseDwrUtil.getCurrentAdmin().getBizSystem();
    	if(!bizSystem.equals("SUPER_SYSTEM")){
    		return AjaxUtil.createReturnValueError("亲,你暂时还没权限修改");
    	}
    	
    	if(lotteryWin == null){
    		return AjaxUtil.createReturnValueError("参数传递错误");
    	}
    	lotteryWinService.updateByPrimaryKeySelective(lotteryWin);
		return AjaxUtil.createReturnValueSuccess();
    }
    
    
}
