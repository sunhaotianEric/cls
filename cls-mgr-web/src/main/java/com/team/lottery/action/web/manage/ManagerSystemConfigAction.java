package com.team.lottery.action.web.manage;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.enums.EAdminOperateLogModel;
import com.team.lottery.enums.ERedisChannel;
import com.team.lottery.extvo.Page;
import com.team.lottery.redis.JedisUtils;
import com.team.lottery.service.AdminOperateLogService;
import com.team.lottery.service.SystemConfigService;
import com.team.lottery.system.message.MessageQueueCenter;
import com.team.lottery.system.message.messagetype.SystemConfigMessage;
import com.team.lottery.util.AdminOperateUtil;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.SystemConfig;

@Controller("managerSystemConfigAction")
public class ManagerSystemConfigAction {
	private static Logger log = LoggerFactory.getLogger(ManagerSystemConfigAction.class);
	@Autowired
	private SystemConfigService systemConfigService;
	@Autowired
	private AdminOperateLogService adminOperateLogService;
    /**
     * 查找网站系统参数配置
     * @return
     */
    public Object[] getAllsystemConfig(SystemConfig query,Page page){
    	if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	page=systemConfigService.getAllSystemConfigByQueryPage(query, page);
		return AjaxUtil.createReturnValueSuccess(page);
    }

    
    /**
     * 删除
     * @return
     */
    public Object[] delSystemConfigById(Long id){
    	SystemConfig systemConfig=null;
    	try{
    		systemConfig=systemConfigService.selectByPrimaryKey(id);
	    	 int result=systemConfigService.deleteByPrimaryKey(id);
	    	 if(result<=0){
	    		 return AjaxUtil.createReturnValueError("删除失败！");
	         }
    	}catch(Exception e){
        	return AjaxUtil.createReturnValueError(e.getMessage());
    	}
    	//通知系统刷新缓存
		try {
			//对应value设置为null
			systemConfig.setConfigValue(null);
			SystemConfigMessage systemConfigMessage=new SystemConfigMessage();
			systemConfigMessage.setSystemConfig(systemConfig);
			MessageQueueCenter.putRedisMessage(systemConfigMessage, ERedisChannel.CLSCACHE.name());
		} catch (Exception e) {
			AjaxUtil.createReturnValueError(e.getMessage());
		}
    	return AjaxUtil.createReturnValueSuccess();
    }
    
    /**
     * 新增和修改
     * @param systemConfig
     * @return
     */
    public Object[] saveOrUpdateSystenConfig(SystemConfig systemConfig)
    {
    	try{
    	if(systemConfig == null){
    		return AjaxUtil.createReturnValueError("参数传递错误");
    	}
    	if(systemConfig.getId()!=null&&!"".equals(systemConfig.getId()))
    	{
    		return this.updateSystemConfig(systemConfig);
    	}else{
    		return this.addSystemConfig(systemConfig);
    	}
    	}catch(Exception e){
        	return AjaxUtil.createReturnValueError(e.getMessage());
    	}
    	
    }
    
    /**
     * 新增
     * @param systemConfig
     * @return
     */
    public Object[] addSystemConfig(SystemConfig systemConfig)
    {
    	if(systemConfig == null){
    		return AjaxUtil.createReturnValueError("参数传递错误");
    	}
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	systemConfig.setCreateDate(new Date());
    	systemConfig.setUpdateDate(new Date());
    	systemConfig.setUpdateAdmin(currentAdmin.getUsername());;
        int result=systemConfigService.insertSelective(systemConfig);
        if(result>0){
        	//通知系统刷新缓存
    		try {
    			SystemConfigMessage systemConfigMessage=new SystemConfigMessage();
    			systemConfigMessage.setSystemConfig(systemConfig);
				MessageQueueCenter.putRedisMessage(systemConfigMessage, ERedisChannel.CLSCACHE.name());

    		} catch (Exception e) {
    			AjaxUtil.createReturnValueError(e.getMessage());
    		}
        	return AjaxUtil.createReturnValueSuccess();
        }else if(result==-2){
        	return AjaxUtil.createReturnValueError("参数名已经存在，请重新添加");
        }else{
        	return AjaxUtil.createReturnValueError("，添加失败，请重新添加");
        }
		//return AjaxUtil.createReturnValueSuccess();
    }
    /**
     * 修改
     * @param systemConfig
     * @return
     */
    public Object[] updateSystemConfig(SystemConfig systemConfig)
    {
    	if(systemConfig == null){
    		return AjaxUtil.createReturnValueError("参数传递错误");
    	}
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	systemConfig.setUpdateDate(new Date());
    	systemConfig.setUpdateAdmin(currentAdmin.getUsername());;
    	int result=systemConfigService.updateByPrimaryKeySelective(systemConfig);
    	 if(result>0){
    			//通知系统刷新缓存
    			try {
    				SystemConfigMessage systemConfigMessage=new SystemConfigMessage();
        			systemConfigMessage.setSystemConfig(systemConfig);
    				MessageQueueCenter.putRedisMessage(systemConfigMessage, ERedisChannel.CLSCACHE.name());

    			} catch (Exception e) {
    				AjaxUtil.createReturnValueError(e.getMessage());
    			}
    		 return AjaxUtil.createReturnValueSuccess();
         }else if(result==-2){
        	 return AjaxUtil.createReturnValueError("参数名已经存在，请重新添加");
         }else{
        	 return AjaxUtil.createReturnValueError("，添加失败，请重新添加");
         }
    }
    
    /**
     * 根据id获取对象
     * @param id
     * @return
     */
    public Object[] getSystemConfigById(Long id)
    {
    	if(id == null){
    		return AjaxUtil.createReturnValueError("参数传递错误");
    	}
    	
    	SystemConfig systemConfig=systemConfigService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(systemConfig);
    }
    
    /**
     * 获取系统默认值
     * @return
     */
    public Object[] getAllSystemConfig(){
    	List<SystemConfig> allSystemConfig = systemConfigService.getAllSystemConfig();
    	Map<String,SystemConfig> map = new HashMap<String,SystemConfig>();
    	Iterator<SystemConfig> iterator = allSystemConfig.iterator();
    	while(iterator.hasNext()){
    		SystemConfig nextSystemConfig = iterator.next();
    		String configKey = nextSystemConfig.getConfigKey();
//    		String configValue = nextSystemConfig.getConfigValue();
    		map.put(configKey, nextSystemConfig);
    	}
    	
	    if(map.isEmpty()){
	   		 return AjaxUtil.createReturnValueError("暂未搜索到系统配置！");
	   	}
    	return AjaxUtil.createReturnValueSuccess(map);
    }
    
    /**
     * 修改系统默认值
     * @param map
     * @return
     */
    public Object[] updateDefaultSystemConfig(Map<String, String> map){
    	if(map.isEmpty()){
    		return AjaxUtil.createReturnValueError("参数传递错误");
    	}
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	try {
    	int result = 0;
    	SystemConfig systemConfig = new SystemConfig();
    	for(String m: map.keySet()){
    		if(m == null || m == ""){
    			log.info("修改系统默认值,参数不存在跳过！");
    			continue;
    		}
    		SystemConfig config = systemConfigService.getSystemConfigByKey(m);
    		if(config != null){
    			//只有改变值彩给他更新， 幸运快三和幸运分分彩盈利模式 例外 特殊处理
    			if(!config.getConfigValue().equals(map.get(m))||"jlffcProfitModel".equals(config.getConfigKey()) || "jyksProfitModel".equals(config.getConfigKey())){
    				
    				systemConfig.setId(config.getId());
    				systemConfig.setConfigKey(config.getConfigKey());
    				systemConfig.setConfigValue(map.get(m));
    				systemConfig.setUpdateDate(new Date());
    				systemConfig.setUpdateAdmin(currentAdmin.getUsername());
    				result=systemConfigService.updateByPrimaryKeySelective(systemConfig);
    				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    				String expireTimeStr = sdf.format(systemConfig.getUpdateDate());			
    				if("jlffcProfitModel".equals(config.getConfigKey()) || "jyksProfitModel".equals(config.getConfigKey())){    					
    					log.info("管理员[{}]修改了[{}]的盈利模式值，从[{}]改为[{}]", currentAdmin.getUsername(), config.getConfigKey(), config.getConfigValue(), map.get(m));
    					//自身彩种 存入redis 
    					String resultStr=JedisUtils.set(systemConfig.getConfigKey(), systemConfig.getConfigValue(), 0);
    					if(!"OK".equals(resultStr)) {
        					return AjaxUtil.createReturnValueError("redis更新失败！保存失败！");
        				}
    					if(!config.getConfigValue().equals(map.get(m))){
	    					String modelName = "jlffcProfitModel".equals(config.getConfigKey()) == true ?"幸运分分彩模式":"幸运快三模式";
	    					String oldProfitModel   = "0".equals(config.getConfigValue())?"随机模式":"1".equals(config.getConfigValue())?"随机盈利模式":"绝对盈利模式";
	    					String updateProfitModel   = "0".equals(systemConfig.getConfigValue())?"随机模式":"1".equals(systemConfig.getConfigValue())?"随机盈利模式":"绝对盈利模式";
	    					String operateDesc = AdminOperateUtil.constructMessage("profitModel_update_log", currentAdmin.getUsername(),expireTimeStr,modelName,
	    							oldProfitModel+"["+config.getConfigValue()+"]",updateProfitModel+"["+systemConfig.getConfigValue()+"]");
	    					String operationIp = BaseDwrUtil.getRequestIp();
	    					adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.SYSTEM_CONFIG, operateDesc);
    					}
    				}
    				
    				
    				
    				
    				if(result < 0) {
    					return AjaxUtil.createReturnValueError("修改失败，请重新添加");
    				}
    			}
    		}else{
    			//新增系统配置默认值
    			systemConfig.setConfigKey(m);
    			systemConfig.setConfigValue(map.get(m));
    			systemConfig.setEnable(1);
    			systemConfig.setCreateDate(new Date());
    			systemConfig.setUpdateAdmin(currentAdmin.getUsername());
    			systemConfigService.insertSelective(systemConfig);
    		}
    	 } 
	  
			SystemConfigMessage systemConfigMessage=new SystemConfigMessage();
			systemConfigMessage.setSystemConfig(null);
			MessageQueueCenter.putRedisMessage(systemConfigMessage, ERedisChannel.CLSCACHE.name());
		
		} catch (Exception e) {
			AjaxUtil.createReturnValueError(e.getMessage());
		}
    	return AjaxUtil.createReturnValueSuccess();
    }
    
    /**
     * 一键刷新
     * @return
     */
    public Object[] refreshCache(){
    	//通知系统刷新缓存
		try {
			SystemConfigMessage systemConfigMessage=new SystemConfigMessage();
			systemConfigMessage.setType(ERedisChannel.CLSCACHE.name());
			systemConfigMessage.setSystemConfig(null);
			 MessageQueueCenter.putRedisMessage(systemConfigMessage, ERedisChannel.CLSCACHE.name());

		} catch (Exception e) {
			AjaxUtil.createReturnValueError(e.getMessage());
		}
	 return AjaxUtil.createReturnValueSuccess();
    }
}
