package com.team.lottery.action.web.manage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.directwebremoting.io.FileTransfer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.extvo.LotteryCodeXyImportQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.LotteryCodeXyImportService;
import com.team.lottery.service.lotterykind.LotteryKindService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ImportExcelUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.LotteryCodeXyImport;

/**
 * 幸运开奖号码导入相关方法
 * 
 * @author Jamine
 *
 */
@Controller("managerLotteryCodeXyImportAction")
public class ManagerLotteryCodeXyImportAction {
	private static Logger logger = LoggerFactory.getLogger(ManagerLotteryCodeXyImportAction.class);
	@Autowired
	private LotteryCodeXyImportService lotteryCodeXyImportService;

	/**
	 * 从数据库查询所有的lotteryCodeXyImport对象
	 * 
	 * @param query
	 * @param page
	 * @return
	 */
	public Object[] getAllLotteryCodeXyImport(LotteryCodeXyImportQuery query, Page page) {
		try {
			// 判断参数是否为空.
			if (query == null || page == null) {
				return AjaxUtil.createReturnValueError("参数传递错误.");
			}
			// 判断当前登录的管理员是超级管理员还是子系统管理员.并且设置查询对象.
			String bizSystem = BaseDwrUtil.getCurrentAdmin().getBizSystem();
			if (!"SUPER_SYSTEM".equals(bizSystem)) {
				query.setBizSystem(bizSystem);
			}
			// 返回Page对象,前端做分页操作.(page里面包括了数据类容和数据条数).
			page = lotteryCodeXyImportService.getAllLotteryCodeXyImportByQueryPage(query, page);
			return AjaxUtil.createReturnValueSuccess(page);
		} catch (Exception e) {
			logger.error("查询幸运号码导入数据出现异常:" + e);
			return AjaxUtil.createReturnValueError("查询幸运号码导入数据出现异常");
		}

	}

	/**
	 * 标记对应幸运开奖号码为失效状态.
	 * 
	 * @param id
	 *            需要修改数据的对应的ID.
	 * @return
	 */
	public Object[] changeUsedStatus(Integer id) {
		// 判断参数是否为空.
		if (id == null) {
			return AjaxUtil.createReturnValueError("参数传递错误!");
		}
		// 通过ID查询出对应的彩票开奖数据,判断这条数据是否失效.
		LotteryCodeXyImport lotteryCodeXyImport = lotteryCodeXyImportService.selectByPrimaryKey(id);
		// 判断通过ID查询出来的数据是否为空.
		if (lotteryCodeXyImport == null) {
			return AjaxUtil.createReturnValueError("操作失败,请联系管理员!");
		}
		// 获取查询出来的幸运彩票开奖导入数据使用状态(0是未使用,1是已使用,2是已失效.).
		Integer usedStatus = lotteryCodeXyImport.getUsedStatus();
		// 状态不为0的时候不能标记为失效.(0是未使用).
		if (usedStatus != 0) {
			return AjaxUtil.createReturnValueError("该条数据不能标记为失效!");
		}
		// 获取当前管理员对象.
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		if (currentAdmin == null) {
			return AjaxUtil.createReturnValueError("请先登录!");
		}
		String currentAdminName = currentAdmin.getUsername();
		// 新建更新对象.
		LotteryCodeXyImport record = new LotteryCodeXyImport();
		// 设置更新对象的ID.
		record.setId(id);
		// 设置更新对象的使用状态为失效.
		record.setUsedStatus(2);
		// 设置修改人.
		record.setUpdateAdmin(currentAdminName);
		// 修改使用状态.
		Integer i = lotteryCodeXyImportService.updateByPrimaryKeySelective(record);

		// i为1的时候修改成功.
		if (i == 1) {
			logger.info("管理员:[" + currentAdminName + "]标记了[" + lotteryCodeXyImport.getBizSystem() + "]系统中的,幸运彩票开奖导入数据期号为[" + lotteryCodeXyImport.getIssueNo() + "]的数据为失效状态!");
			return AjaxUtil.createReturnValueSuccess();
		} else {
			return AjaxUtil.createReturnValueError("操作失败,请联系管理员!");
		}
	}

	/**
	 * 幸运开奖号码文件导入.
	 * 
	 * @param fileTransfer
	 * @param bizSystem
	 * @param lotteryType
	 * @return
	 */
	public Object[] uploadExcelFile(FileTransfer fileTransfer, String bizSystem, String lotteryType) {
		// 判断参数是否为空.
		if (StringUtils.isEmpty(lotteryType)) {
			return AjaxUtil.createReturnValueError("参数错误！");
		}
		// 判断当前是超级系统还是子系统.
		String currentSystem = BaseDwrUtil.getCurrentAdmin().getBizSystem();
		if (!"SUPER_SYSTEM".equals(currentSystem)) {
			bizSystem = currentSystem;
		}
		// 获取当前的处理管理员
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		// 新建List集合用于存储数据.
		List<LotteryCodeXyImport> lotteryCodeXyImportList = new ArrayList<LotteryCodeXyImport>();

		// 导入失败数量.
		int importFailureCount = 0;
		// 导入成功失败.
		int importSuccessCount = 0;
		try {
			WebContext webContext = WebContextFactory.get();
			String realPath = webContext.getServletContext().getRealPath("/upload");

			File fileDir = new File(realPath);
			if (!fileDir.exists()) {
				fileDir.mkdirs();
			}
			String saveFileUrl = realPath + "/" + fileTransfer.getFilename();
			File saveFile = new File(saveFileUrl);
			InputStream uploadFile = fileTransfer.getInputStream();
			int available = uploadFile.available();
			byte[] b = new byte[available];
			FileOutputStream foutput = new FileOutputStream(saveFile);
			uploadFile.read(b);
			foutput.write(b);
			foutput.flush();
			foutput.close();
			uploadFile.close();

			File file = new File(saveFileUrl);
			List<List<Object>> dataList = ImportExcelUtil.importExcel(file);
			for (int i = 0; i < dataList.size(); i++) {
				LotteryCodeXyImport lotteryCodeXyImport = new LotteryCodeXyImport();
				// 开奖期号.
				String issueNo = "";
				// 开奖号码.
				String openCode = "";
				for (int j = 0; j < dataList.get(i).size(); j++) {
					if (j == 0) {
						issueNo = (String) dataList.get(i).get(0);
					} else if (j == 1) {
						openCode = (String) dataList.get(i).get(1);
					}
				}
				// 参数验证
				if (StringUtils.isEmpty(issueNo) || StringUtils.isEmpty(openCode)) {
					logger.info("期号和开奖号码不能为空");
					importFailureCount++;
					return AjaxUtil.createReturnValueError("期号和开奖号码不能为空.请检查后重新导入!");
				}
				// 校验期号必须为数字
				boolean numeric = StringUtils.isNumeric(issueNo);
				if (!numeric) {
					logger.info("期号输入错误格式");
					return AjaxUtil.createReturnValueError("请检查期号是否输入正确!");
				}
				ELotteryKind kind = ELotteryKind.valueOf(lotteryType);
				LotteryKindService lotteryKindService = (LotteryKindService) ApplicationContextUtil.getBean(kind.getServiceClassName());
				boolean lotteryCodeCheckResult = lotteryKindService.lotteryCodesCheckByOpenCode(openCode);
				if (!lotteryCodeCheckResult) {
					logger.info("开奖的幸运号码格式不正确");
					importFailureCount++;
					return AjaxUtil.createReturnValueError("开奖的幸运号码格式不正确.请检查后重新导入!");
				}

				// 设置系统.
				lotteryCodeXyImport.setBizSystem(bizSystem);
				// 设置彩种类型.
				lotteryCodeXyImport.setLotteryType(lotteryType);
				// 设置期号.
				lotteryCodeXyImport.setIssueNo(issueNo);
				// 设置开奖号码.
				lotteryCodeXyImport.setOpenCode(openCode);
				// 设置创建时间.
				lotteryCodeXyImport.setCreateTime(new Date());
				// 获取当前管理员名字.
				lotteryCodeXyImport.setCreateAdmin(currentAdmin.getUsername());
				// 设置使用状态.
				lotteryCodeXyImport.setUsedStatus(0);
				lotteryCodeXyImportList.add(lotteryCodeXyImport);
				logger.info("管理员[" + currentAdmin.getUsername() + "]导入了幸运开奖数据彩种为[" + lotteryCodeXyImport.getLotteryType() + "],对应的系统为" + lotteryCodeXyImport.getBizSystem()
						+ "期号为[" + lotteryCodeXyImport.getIssueNo() + "],开奖号码为[" + lotteryCodeXyImport.getOpenCode() + "]");

			}
		} catch (Exception e) {
			logger.error("导入失败.", e);
			return AjaxUtil.createReturnValueError("导入失败.");
		}
		try {
			// 进行事务操作.
			importSuccessCount = lotteryCodeXyImportService.insertListSelective(lotteryCodeXyImportList);

		} catch (Exception e) {
			logger.error("添加幸运开奖号码出错", e);
			return AjaxUtil.createReturnValueError("添加幸运开奖号码出错");
		}
		return AjaxUtil.createReturnValueSuccess("成功导入【" + importSuccessCount + "】条幸运开奖号码数据，导入失败【" + importFailureCount + "】条幸运开奖号码数据");
	}
}
