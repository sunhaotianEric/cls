package com.team.lottery.action.web.manage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.Page;
import com.team.lottery.service.SmsSendRecordService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.vo.SmsSendRecord;

@Controller("managerSmsSendRecordAction")
public class ManagerSmsSendRecordAction {

	@Autowired
	private SmsSendRecordService smsSendRecordService;
	
	/**
	 * 分页查询
	 * @param query
	 * @param page
	 * @return
	 */
	public Object[] getAllSmsSendRecord(SmsSendRecord query,Page page){
		if(query==null||page==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		String bizSystem=BaseDwrUtil.getCurrentAdmin().getBizSystem();
		if(!bizSystem.equals(ConstantUtil.SUPER_SYSTEM)){
			query.setBizSystem(bizSystem);
		}
		page=smsSendRecordService.getAllSmsSendRecord(query, page);
		return AjaxUtil.createReturnValueSuccess(page);
	}
	
	/**
	 * 根据ID查询
	 * @param id
	 * @return
	 */
	public Object[] selectSmsSendRecord(Long id){
		SmsSendRecord record=smsSendRecordService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(record);
	}
	
	/**
	 * 修改
	 * @param record
	 * @return
	 */
	public Object[] updateSmsSendRecord(SmsSendRecord record){
		if(record==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		smsSendRecordService.updateByPrimaryKeySelective(record);
		return AjaxUtil.createReturnValueSuccess();
	}
	
	/**
	 * 添加
	 * @param query
	 * @return
	 */
	public Object[] insertSmsSendRecord(SmsSendRecord record){
		if(record==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		smsSendRecordService.insertSelective(record);
		return AjaxUtil.createReturnValueSuccess();
	}
	
	/**
	 * 根据ID删除
	 * @param id
	 * @return
	 */
	public Object[] deleteSmsSendRecord(Long id){
		if(id==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		smsSendRecordService.deleteByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess();
	}
	
}
