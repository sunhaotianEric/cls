package com.team.lottery.action.web.manage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.Page;
import com.team.lottery.service.AdminRecvNoteService;
import com.team.lottery.service.AdminSendNoteService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.AdminRecvNote;
import com.team.lottery.vo.AdminSendNote;

@Controller("managerAdminRecvNoteAction")
public class ManagerAdminRecvNoteAction {

	@Autowired
	private AdminRecvNoteService adminRecvNoteService;
	@Autowired
	private AdminSendNoteService adminSendNoteService;
	
	/**
	 * 收件箱分页查询
	 * @param record
	 * @param page
	 * @return
	 */
	public Object[] getAdminRecvNoteBytomainid(AdminRecvNote record,Page page){
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		record.setToAdminId(currentAdmin.getId());
		page=adminRecvNoteService.getAdminRecvNoteBytomainid(record, page);
		return AjaxUtil.createReturnValueSuccess(page);
	}
	
	/**
	 * 添加收件箱数据
	 * @param record
	 * @return
	 */
	public Object[] insertSelective(AdminRecvNote record){
		return AjaxUtil.createReturnValueSuccess(adminRecvNoteService.insertSelective(record));
	}
	
	/**
	 * 根据ID修改收件箱数据
	 * @param record
	 * @return
	 */
	public Object[] updateByPrimaryKeySelective(AdminRecvNote record){
		return AjaxUtil.createReturnValueSuccess(adminRecvNoteService.updateByPrimaryKeySelective(record));
	}
	
	/**
	 * 根据ID删除收件数据
	 * @param id
	 * @return
	 */
	public Object[] deleteByPrimaryKey(Long id){
		int i=adminRecvNoteService.deleteByPrimaryKey(id);
		if(i==0){
			return AjaxUtil.createReturnValueError("删除失败");
		}
		return AjaxUtil.createReturnValueSuccess("删除成功");
	}
	
	/**
	 * 根据收件箱ID查询收件箱详情
	 * @param id
	 * @return
	 */
	public Object[] selectByPrimaryKey(Long id){
		//查询单个收件箱数据
		AdminRecvNote record=adminRecvNoteService.selectByPrimaryKey(id);
		//存在就修改状态
		if(record!=null&&record.getReadStatus().equals("未读")){
			AdminRecvNote records=new AdminRecvNote();
			records.setId(id);
			records.setReadStatus("已读");
			int i=adminRecvNoteService.updateByPrimaryKeySelective(records);
			//修改成功则修改发件箱数据
			if(i>0){
				AdminSendNote adminSendNote=new AdminSendNote();
				adminSendNote.setId(Long.parseLong(record.getSendNoteId().toString()));
				adminSendNote.setReadedAdminIds(record.getToAdminName());
				adminSendNoteService.updateByPrimaryKeySelective(adminSendNote);
			}else{
				return AjaxUtil.createReturnValueError("修改阅读状态失败，请联系管理员");
			}
		}
		return AjaxUtil.createReturnValueSuccess(record);
	}
	
	/**
	 * 根据登录人获取其所有已读或者未读的收件信息
	 * @return
	 */
	public Object[] getAllAdminRecvNoteBytomainid(String root){
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		AdminRecvNote adminRecvNote=new AdminRecvNote();
		adminRecvNote.setToAdminId(currentAdmin.getId());
		adminRecvNote.setReadStatus(root);
		return AjaxUtil.createReturnValueSuccess(adminRecvNoteService.getAllAdminRecvNoteBytomainid(adminRecvNote));
	}
}
