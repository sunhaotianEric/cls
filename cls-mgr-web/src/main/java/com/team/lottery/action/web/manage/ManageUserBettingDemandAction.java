package com.team.lottery.action.web.manage;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.enums.EUserBettingDemandStatus;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.UserBettingDemandQuery;
import com.team.lottery.service.UserBettingDemandService;
import com.team.lottery.service.UserService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserBettingDemand;

@Controller("manageUserBettingDemandAction")
public class ManageUserBettingDemandAction {
	
	private static Logger logger = LoggerFactory.getLogger(ManageUserBettingDemandAction.class);
	
	@Autowired
	public UserBettingDemandService userBettingDemandService;
	@Autowired
	public UserService userService;
	
	
	/**
	 * 分页查询打码要求记录
	 * @param bizSystem
	 * @param userName
	 * @param page
	 * @return
	 */
	public Object[] getAllUserBettingDemandPage (UserBettingDemandQuery query, Page page){
		if(page == null){
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		Admin currentUser = BaseDwrUtil.getCurrentAdmin();
		if(!"SUPER_SYSTEM".equals(currentUser.getBizSystem())){
			query.setBizSystem(currentUser.getBizSystem());
		}
		page = userBettingDemandService.getAllUserBettingDemandPage(query, page);
		return AjaxUtil.createReturnValueSuccess(page);
	}
	
	/**
	 * 保存更新打码记录
	 * @param userRebateForbid
	 * @return
	 */
	public Object[] saveUserBettingDemand(UserBettingDemand userBettingDemand){
		if(userBettingDemand == null){
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		
		Integer id = userBettingDemand.getId();
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		if(!currentAdmin.getBizSystem().equals("SUPER_SYSTEM")){
			userBettingDemand.setBizSystem(currentAdmin.getBizSystem());
		}
		
		if(id == null){
			User user = userService.getUserByName(userBettingDemand.getBizSystem(), userBettingDemand.getUserName());
			if(user == null) {
				return AjaxUtil.createReturnValueError("保存的用户不存在");
			}
			//查询是否有用户进行中的打码要求，有不允许再新加
			UserBettingDemand goingBettingDemand = userBettingDemandService.getUserGoingBettingDemand(user.getId());
			if(goingBettingDemand != null) {
				return AjaxUtil.createReturnValueError("当前用户["+ user.getUserName() +"]还有未完成的打码要求，不能重复添加");
			}
			logger.info("管理员[{}]新增业务系统[{}]用户[{}]的打码要求，要求打码量[{}]", currentAdmin.getUsername(), 
				  	userBettingDemand.getBizSystem(), userBettingDemand.getUserName(),userBettingDemand.getRequiredBettingMoney());
			userBettingDemand.setUserId(user.getId());
			userBettingDemand.setCreateDate(new Date());
			int res = userBettingDemandService.insertSelective(userBettingDemand);
			if(res > 0) {
				return AjaxUtil.createReturnValueSuccess("保存成功");
			} else {
				return AjaxUtil.createReturnValueError("保存失败");
			}
		}else{
			UserBettingDemand dbUserBettingDemand = userBettingDemandService.selectByPrimaryKey(id);
			if(dbUserBettingDemand == null) {
				return AjaxUtil.createReturnValueError("编辑的记录不存在");
			}
			logger.info("管理员[{}]编辑了业务系统[{}]用户[{}]的打码要求，增加的要求打码量[{}],增加的完成打码量[{}]", currentAdmin.getUsername(), 
				  	userBettingDemand.getBizSystem(), userBettingDemand.getUserName(),userBettingDemand.getAddRequiredBettingMoney(),
				  	userBettingDemand.getAddDoneBettingMoney());
			dbUserBettingDemand.setRequiredBettingMoney(dbUserBettingDemand.getRequiredBettingMoney().add(userBettingDemand.getAddRequiredBettingMoney()));
			dbUserBettingDemand.setDoneBettingMoney(dbUserBettingDemand.getDoneBettingMoney().add(userBettingDemand.getAddDoneBettingMoney()));
			dbUserBettingDemand.setUpdateDate(new Date());
			userBettingDemandService.updateByPrimaryKeySelective(dbUserBettingDemand);
			return AjaxUtil.createReturnValueSuccess("更新成功");
		}
	}
	
	/**
	 * 获取用户打码记录
	 * @param id
	 * @return
	 */
	public Object[] getUserBettingDemand(Integer id){
		UserBettingDemand userBettingDemand = userBettingDemandService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(userBettingDemand);
	}
	
	/**
	 * 删除用户打码记录
	 * @param id
	 * @return
	 */
	  public Object[] delUserBettingDemand(Integer id){
		  UserBettingDemand userBettingDemand = userBettingDemandService.selectByPrimaryKey(id);
		  if(userBettingDemand == null) {
			  return AjaxUtil.createReturnValueError("删除失败，此记录不存在");
		  }
		  Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		  logger.info("管理员[{}]强制完成了业务系统[{}]用户[{}]的打码要求，打码要求记录id[{}],要求打码量[{}]", currentAdmin.getUsername(), 
				  	userBettingDemand.getBizSystem(), userBettingDemand.getUserName(), userBettingDemand.getId(), userBettingDemand.getRequiredBettingMoney());
		  userBettingDemandService.deleteByPrimaryKey(id);
		  return AjaxUtil.createReturnValueSuccess();
	  }
	  
	/**
	 * 管理员强制完成打码记录
	 * @param id
	 * @return
	 */
	  public Object[] forceEnd(Integer id){
		  UserBettingDemand userBettingDemand = userBettingDemandService.selectByPrimaryKey(id);
		  if(userBettingDemand == null) {
			  return AjaxUtil.createReturnValueError("强制完成失败，此记录不存在");
		  }
		  Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		  logger.info("管理员[{}]强制完成了业务系统[{}]用户[{}]的打码要求，打码要求记录id[{}],要求打码量[{}]", currentAdmin.getUsername(), 
				  	userBettingDemand.getBizSystem(), userBettingDemand.getUserName(), userBettingDemand.getId(), userBettingDemand.getRequiredBettingMoney());
		  userBettingDemand.setDoneStatus(EUserBettingDemandStatus.FORCE_END.getCode());
		  userBettingDemand.setUpdateDate(new Date());
		  userBettingDemandService.updateByPrimaryKeySelective(userBettingDemand);
		  return AjaxUtil.createReturnValueSuccess();
	  }
	  
	  
}
