package com.team.lottery.action.web.manage;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.UserQuotaVo;
import com.team.lottery.service.UserQuotaService;
import com.team.lottery.service.UserService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.vo.AwardModelConfig;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserQuota;

@Controller("managerUserQuotaAction")
public class ManagerUserQuotaAction {
	

	@Autowired
	private UserQuotaService userQuotaService;
	@Autowired
	private UserService userService;
	
	
	 /**
     * 获取用户的配额信息
     * @param userId
     * @return
     */
    public Object[] loadUserQuota(Integer userId){
    	if(userId == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	List<UserQuota> userQuotaList = userQuotaService.getQuotaByUserId(userId);
    	Page page= new Page();
    	page.setPageNo(1);
    	page.setPageSize(1);
    	page.setTotalRecNum(1);
    	page.setPageContent(userQuotaList);
    	return AjaxUtil.createReturnValueSuccess(page);
    }
    
    /**
     * 更新用户的配额信息
     * @param userId
     * @return
     */
    public Object[] updateUserQuota(UserQuotaVo userQuotaVo){
    	if(userQuotaVo == null||userQuotaVo.getUserQuotaList()== null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
//    	UserQuota uq = userQuotaService.getQuotaByUserId(userQuota.getUserId());
//    	if(uq==null){
//    		return AjaxUtil.createReturnValueError("sorry,您代理等级不允许编辑配额数！");	
//    	}	
    	userQuotaService.batchUpdateUserQuota(userQuotaVo.getUserQuotaList());
		return AjaxUtil.createReturnValueSuccess();
    }
    
    
    /**
     * 添加用户的配额信息
     * @param userId
     * @return
     */
    public Object[] addUserQuota(UserQuota userQuota){
    	if(userQuota == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	User user = userService.getUserByName(userQuota.getBizSystem(),userQuota.getUserName());
    	userQuota.setUserId(user.getId());
    	List<UserQuota> userQuotaList = userQuotaService.getQuotaByUserId(user.getId());
    	for(UserQuota uq:userQuotaList){
    		if(uq.getSscRebate().compareTo(userQuota.getSscRebate())==0){
    			return AjaxUtil.createReturnValueError("已经有该配置模式了！请重新添加");
    		}
    	}
    	
    	userQuota.setCreatedDate(new Date());
    	userQuota.setUpdateDate(new Date());
    	userQuotaService.insertSelective(userQuota);
		return AjaxUtil.createReturnValueSuccess();
    }
    
    
    /**
     * 获取用户的配额信息
     * @param userId
     * @return
     */
    public Object[] getUserQuotaByUserName(String userName,String bizSystem){
    	if(StringUtils.isBlank(userName)||StringUtils.isBlank(bizSystem)){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
	
    	User user = userService.getUserByName(bizSystem,userName);
    	if(user == null) {
    		return AjaxUtil.createReturnValueError("查询的用户名不存在");
    	}
    	List<UserQuota> userQuotaList = userQuotaService.getQuotaByUserId(user.getId());
    	if(userQuotaList.size()==0){
    		return AjaxUtil.createReturnValueError("查询用户对应的配额记录为空！");	
    	}
    	Page page= new Page();
    	page.setPageNo(1);
    	page.setPageSize(1);
    	page.setTotalRecNum(1);
    	page.setPageContent(userQuotaList);

    	 return AjaxUtil.createReturnValueSuccess(page);
    
    }
    
    /**
     * 删除
     * @param id
     * @return
     */
    public Object[] deleteUserQuota(Long id){
    	if(id == null){
    		 return AjaxUtil.createReturnValueError("参数错误！");	
    	}
    	int result = userQuotaService.deleteByPrimaryKey(id);
    	if(result>0){
    		return AjaxUtil.createReturnValueSuccess("删除成功！");	
    	}else{
    		return AjaxUtil.createReturnValueError("删除失败！");	
    	}
    }
    
    
    /**
     * 获取用户的配额信息
     * @param userId
     * @return
     */
    public Object[] getUserQuotaInfoByUserName(String userName,String bizSystem){
    	if(StringUtils.isBlank(userName)||StringUtils.isBlank(bizSystem)){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    
	
    	User user = userService.getUserByName(bizSystem,userName);
    	if(user == null) {
    		return AjaxUtil.createReturnValueError("查询的用户名不存在");
    	}
    	//设置查询有配额的直接下级模式注册情况
    	user.setRegfrom((user.getRegfrom()==null?"":user.getRegfrom())+user.getUserName()+"&");
    	List<User> userCountQuotaList=userService.userquotaCount(user);
    	if(userCountQuotaList.size()==0){
    		return AjaxUtil.createReturnValueError("查询用户对应的配额记录为空！");	
    	}
    	
    	Page page= new Page();
    	page.setPageNo(1);
    	page.setPageSize(1);
    	page.setTotalRecNum(1);
    	page.setPageContent(userCountQuotaList);
    	return AjaxUtil.createReturnValueSuccess(page);
    }
    
    /**
     * 根据bizsystem查询可设置业务的最高最低模式
     * @return
     */
    public  Object[] getAwardModel(String bizSystem)
    {
    	AwardModelConfig awardModelConfig =null;
		awardModelConfig = ClsCacheManager.getAwardModelConfigByCode(bizSystem);
    	return AjaxUtil.createReturnValueSuccess(awardModelConfig);
    }
    
}
