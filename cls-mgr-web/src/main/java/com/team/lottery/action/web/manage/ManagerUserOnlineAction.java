package com.team.lottery.action.web.manage;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.DayOnlineInfoQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.TeamUserQuery;
import com.team.lottery.service.BizSystemService;
import com.team.lottery.service.DayOnlineInfoService;
import com.team.lottery.service.LoginLogService;
import com.team.lottery.service.OrderService;
import com.team.lottery.service.UserService;
import com.team.lottery.system.SingleLoginForRedis;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.BizSystem;
import com.team.lottery.vo.DayOnlineInfo;
import com.team.lottery.vo.Login;
import com.team.lottery.vo.User;

@Controller("managerUserOnlineAction")
public class ManagerUserOnlineAction {
	
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private DayOnlineInfoService dayOnlineInfoService;
	
	@Autowired
	private LoginLogService loginLogService;
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private BizSystemService bizSystemService;
	 /**
     * 查找在线会员数据
     * @return
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
     */
    public Object[] getUserOnlineListPage(TeamUserQuery query,Page page) {
    	if(query == null || page == null ){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	  TeamUserQuery teamQuery=null;
          List<String> names=null;
        List<TeamUserQuery> querys=new ArrayList<TeamUserQuery>();
        try {
        	Admin currentAdmin=BaseDwrUtil.getCurrentAdmin();
        	if(!ConstantUtil.SUPER_SYSTEM.equals(currentAdmin.getBizSystem())){
        		query.setBizSystem(currentAdmin.getBizSystem());
    		}
        	
        if(query.getBizSystem()==null||"".equals(query.getBizSystem())){
        List<BizSystem> list=bizSystemService.getAllBizSystem();
      
        for(BizSystem bizSystem:list){
        //设置查询在线用户列表条件
       if(Login.LOGTYPE_MOBILE.equals(query.getOnlineType())) {
        	 names=SingleLoginForRedis.getFrontLoginUserNams(Login.LOGTYPE_MOBILE+"."+bizSystem.getBizSystem());
        	 teamQuery=new TeamUserQuery();
			teamQuery.setRegisterDateStart(query.getRegisterDateStart());
			teamQuery.setRegisterDateEnd(query.getRegisterDateEnd());;
		     teamQuery.setDailiLevel(query.getDailiLevel());
		     teamQuery.setUserName(query.getUserName());
		     teamQuery.setBalanceStart(teamQuery.getBalanceStart());
		     teamQuery.setBalanceEnd(teamQuery.getBalanceEnd());
        	 teamQuery.setOnlineUserNames(names);
        	 teamQuery.setIsQueryLike(true);
        	 teamQuery.setBizSystem(bizSystem.getBizSystem());
        	 querys.add(teamQuery);
        } else if(Login.LOGTYPE_PC.equals(query.getOnlineType())){
        	 names=SingleLoginForRedis.getFrontLoginUserNams(Login.LOGTYPE_PC+"."+bizSystem.getBizSystem());

        	 teamQuery=new TeamUserQuery();
    		teamQuery.setRegisterDateStart(query.getRegisterDateStart());
			teamQuery.setRegisterDateEnd(query.getRegisterDateEnd());;
		     teamQuery.setDailiLevel(query.getDailiLevel());
		     teamQuery.setUserName(query.getUserName());
		     teamQuery.setBalanceStart(teamQuery.getBalanceStart());
		     teamQuery.setBalanceEnd(teamQuery.getBalanceEnd());
        	 teamQuery.setOnlineUserNames(names);
        	 teamQuery.setIsQueryLike(true);
        	 teamQuery.setBizSystem(bizSystem.getBizSystem());
        	 querys.add(teamQuery);
        }else{
         	 names=SingleLoginForRedis.getFrontLoginUserNams(Login.LOGTYPE_MOBILE+"."+bizSystem.getBizSystem());
 
        	 teamQuery=new TeamUserQuery();
    		teamQuery.setRegisterDateStart(query.getRegisterDateStart());
			teamQuery.setRegisterDateEnd(query.getRegisterDateEnd());;
		     teamQuery.setDailiLevel(query.getDailiLevel());
		     teamQuery.setUserName(query.getUserName());
		     teamQuery.setBalanceStart(teamQuery.getBalanceStart());
		     teamQuery.setBalanceEnd(teamQuery.getBalanceEnd());
        	 teamQuery.setOnlineUserNames(names);
        	 teamQuery.setIsQueryLike(true);
        	 teamQuery.setBizSystem(bizSystem.getBizSystem());
        	 querys.add(teamQuery);
        	 
        	 names=SingleLoginForRedis.getFrontLoginUserNams(Login.LOGTYPE_PC+"."+bizSystem.getBizSystem());
 
        	 teamQuery=new TeamUserQuery();
    		teamQuery.setRegisterDateStart(query.getRegisterDateStart());
			teamQuery.setRegisterDateEnd(query.getRegisterDateEnd());;
		     teamQuery.setDailiLevel(query.getDailiLevel());
		     teamQuery.setUserName(query.getUserName());
		     teamQuery.setBalanceStart(teamQuery.getBalanceStart());
		     teamQuery.setBalanceEnd(teamQuery.getBalanceEnd());
        	 teamQuery.setOnlineUserNames(names);
        	 teamQuery.setIsQueryLike(true);
        	 teamQuery.setBizSystem(bizSystem.getBizSystem());
        	 querys.add(teamQuery); 
        }
      
        }
        }else{
            if(Login.LOGTYPE_MOBILE.equals(query.getOnlineType())) {
           	 names=SingleLoginForRedis.getFrontLoginUserNams(Login.LOGTYPE_MOBILE+"."+query.getBizSystem());
           	 teamQuery=new TeamUserQuery();
         	teamQuery.setRegisterDateStart(query.getRegisterDateStart());
			teamQuery.setRegisterDateEnd(query.getRegisterDateEnd());;
		     teamQuery.setDailiLevel(query.getDailiLevel());
		     teamQuery.setUserName(query.getUserName());
		     teamQuery.setBalanceStart(teamQuery.getBalanceStart());
		     teamQuery.setBalanceEnd(teamQuery.getBalanceEnd());
           	 teamQuery.setOnlineUserNames(names);
           	 teamQuery.setIsQueryLike(true);
           	 teamQuery.setBizSystem(query.getBizSystem());
           	 querys.add(teamQuery);
           } else if(Login.LOGTYPE_PC.equals(query.getOnlineType())){
           	 names=SingleLoginForRedis.getFrontLoginUserNams(Login.LOGTYPE_PC+"."+query.getBizSystem());
           	 teamQuery=new TeamUserQuery();
         	teamQuery.setRegisterDateStart(query.getRegisterDateStart());
			teamQuery.setRegisterDateEnd(query.getRegisterDateEnd());;
		     teamQuery.setDailiLevel(query.getDailiLevel());
		     teamQuery.setUserName(query.getUserName());
		     teamQuery.setBalanceStart(teamQuery.getBalanceStart());
		     teamQuery.setBalanceEnd(teamQuery.getBalanceEnd());
           	 teamQuery.setOnlineUserNames(names);
           	 teamQuery.setIsQueryLike(true);
           	 teamQuery.setBizSystem(query.getBizSystem());
           	 querys.add(teamQuery);
           }else{
              names=SingleLoginForRedis.getFrontLoginUserNams(Login.LOGTYPE_MOBILE+"."+query.getBizSystem());
           	 teamQuery=new TeamUserQuery();
         	 teamQuery.setRegisterDateStart(query.getRegisterDateStart());
			 teamQuery.setRegisterDateEnd(query.getRegisterDateEnd());;
		     teamQuery.setDailiLevel(query.getDailiLevel());
		     teamQuery.setUserName(query.getUserName());
		     teamQuery.setBalanceStart(teamQuery.getBalanceStart());
		     teamQuery.setBalanceEnd(teamQuery.getBalanceEnd());
           	 teamQuery.setOnlineUserNames(names);
           	 teamQuery.setIsQueryLike(true);
           	 teamQuery.setBizSystem(query.getBizSystem());
           	 querys.add(teamQuery);
           	 
           	 names=SingleLoginForRedis.getFrontLoginUserNams(Login.LOGTYPE_PC+"."+query.getBizSystem());
           	 teamQuery=new TeamUserQuery();
         	teamQuery.setRegisterDateStart(query.getRegisterDateStart());
			teamQuery.setRegisterDateEnd(query.getRegisterDateEnd());;
		     teamQuery.setDailiLevel(query.getDailiLevel());
		     teamQuery.setUserName(query.getUserName());
		     teamQuery.setBalanceStart(teamQuery.getBalanceStart());
		     teamQuery.setBalanceEnd(teamQuery.getBalanceEnd());
           	 teamQuery.setOnlineUserNames(names);
           	 teamQuery.setIsQueryLike(true);
           	 teamQuery.setBizSystem(query.getBizSystem());
           	 querys.add(teamQuery); 
           }
        }
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
			
        userService.getOnlineUserListByQueryPage(querys, page);
        
        //在线会员
        @SuppressWarnings("unchecked")
		List<User> users = (List<User>)page.getPageContent();
        //设置在线会员状态
        //FrontUserOnlineInterface.setUsersOnlineStatus(users);
        for(User user : users){
        	Set<String> pcsets=SingleLoginForRedis.getRedisSetsOnlyNames(Login.LOGTYPE_PC+"."+user.getBizSystem());
        	if(pcsets.contains(user.getUserName())){
    		user.setIsOnline(1);
    	    }
        	Set<String> mobilesets=SingleLoginForRedis.getRedisSetsOnlyNames(Login.LOGTYPE_MOBILE+"."+user.getBizSystem());
        	if(mobilesets.contains(user.getUserName())){
    		user.setIsOnline(1);
    	    }
        }
		return AjaxUtil.createReturnValueSuccess(page);
    }
    
    /**
     * 查找每日在线统计数据
     * @return
     */
    public Object[] getUserDayOnlineInfoListPage(DayOnlineInfoQuery query,Page page){
    	if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		if(!"SUPER_SYSTEM".equals(currentAdmin.getBizSystem())){
			query.setBizSystem(currentAdmin.getBizSystem());	
		}
    
    	page=dayOnlineInfoService.getUserDayOnlineInfoPage(query, page);
    	DayOnlineInfo dayOnlineInfo=dayOnlineInfoService.getUserDayOnlineInfoTotal(query);
    	return AjaxUtil.createReturnValueSuccess(page,dayOnlineInfo);
    }
    
    public static void main(String[] args) {
		Set<User> sets=new HashSet<User>();
		User user=new User();
		user.setId(1);sets.add(user);
		User user1=new User();user1.setId(1);sets.add(user1);
		System.out.println(sets.size());
		
	}
}
