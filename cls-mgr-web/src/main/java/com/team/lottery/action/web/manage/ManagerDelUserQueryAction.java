package com.team.lottery.action.web.manage;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.DeleteUserQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.QueryDeleteUserSevice;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.vo.UserDel;

@Controller("managerDelUserQueryAction")
public class ManagerDelUserQueryAction {
	@Autowired
	private QueryDeleteUserSevice queryDeleteUserSevice;

	/**
	 * 查找满足条件的用户
	 * @param query
	 * @return
	 */
	public Object[] getdelUserByParameter(DeleteUserQuery query){
		List<UserDel> getdelUserByParameter = queryDeleteUserSevice.getdelUser(query);
		return AjaxUtil.createReturnValueSuccess(getdelUserByParameter);

	}
	/**
	 * 分页查询用户列表
	 * @param user
	 * @return
	 */
	public Object[] delUserList(DeleteUserQuery query,Integer pageNo){
		if(query == null || pageNo == null){
			return AjaxUtil.createReturnValueError("参数传递错误");
		}
		Page page = new Page(SystemConfigConstant.defaultMgrPageSize);
		page.setPageNo(pageNo);
		Page allUsersByQueryPage = queryDeleteUserSevice.getAllUsersByQueryPage(query,page);
		page = allUsersByQueryPage;

		return AjaxUtil.createReturnValueSuccess(page);
	}

	/**
	 * 恢复被删会员
	 * @param Id
	 * @return
	 */
	public Object[] deleteRegressionById(Integer Id){
		if(Id == null){
			return AjaxUtil.createReturnValueError("参数传递错误");
		}
		try {
			
			queryDeleteUserSevice.resumeMember(Id);
		} catch (Exception e) {
			return AjaxUtil.createReturnValueError("用户名已存在，该用户不能被恢复！");
		}
		return AjaxUtil.createReturnValueSuccess();
	}

}
