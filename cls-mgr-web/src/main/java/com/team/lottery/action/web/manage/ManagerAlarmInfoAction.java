package com.team.lottery.action.web.manage;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.Page;
import com.team.lottery.service.AlarmInfoService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.AlarmInfo;

@Controller("managerAlarmInfoAction")
public class ManagerAlarmInfoAction {

	private static HashMap<String,Object> map=new HashMap<String, Object>();
	@Autowired
	private AlarmInfoService alarmInfoService;
	
	/**
	 * 告警信息分页查询
	 * @param alamrminfo
	 * @param page
	 * @return
	 */
	public Object getAlarminfoPage(AlarmInfo alarminfo,Page page){
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		if(!"SUPER_SYSTEM".equals(currentAdmin.getBizSystem())){
			alarminfo.setBizSystem(currentAdmin.getBizSystem());
		}
		page=alarmInfoService.getAlarminfoPage(alarminfo, page);
		return AjaxUtil.createReturnValueSuccess(page);
	}
	
	/**
	 * 删除告警信息
	 * @param id
	 * @return
	 */
	public Object delAlarminfoById(Integer id){
		alarmInfoService.deleteByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess();
	}
	
	/**
	 * 查询10秒内的最新告警信息
	 * @return
	 */
	public Object queryAlarmInfoBytime(){
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar c = new GregorianCalendar();
		Date date = new Date();
		c.setTime(date);//设置参数时间
		c.add(Calendar.SECOND,-10);//把日期往后增加SECOND 秒.整数往后推,负数往前移动
		date=c.getTime(); //这个时间就是日期往后推一天的结果
		String str = df.format(date);
		try {
			Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
			AlarmInfo alarminfo=new AlarmInfo();
			alarminfo.setCreateDate(df.parse(str));
			alarminfo.setBizSystem(currentAdmin.getBizSystem());
			
			List<AlarmInfo> list=alarmInfoService.queryAlarmInfoBytime(alarminfo);//查询到数据
			if(list.size()>0){
				Iterator<AlarmInfo> it=list.iterator();//迭代器避免删除错误
				while (it.hasNext()) {
					AlarmInfo ala=it.next();
					int id=Integer.parseInt((map.get(ala.getBizSystem())==null?0:map.get(currentAdmin.getBizSystem())).toString());//获取map里面存储的ID
					if(map!=null&&id>0){//符合条件map里面有值   存在相对应KEY
						if(ala.getId()<=id){
							it.remove();
						}else{
							map.put(ala.getBizSystem(),ala.getId());
						}
					}else{
						map.put(ala.getBizSystem(),ala.getId());
					}
				}
			}
			return AjaxUtil.createReturnValueSuccess(list);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			return AjaxUtil.createReturnValueError("时间转换错误");
		}
	}
}
