package com.team.lottery.action.web.manage;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.service.*;
import com.team.lottery.vo.*;
import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.directwebremoting.io.FileTransfer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.stereotype.Controller;

import com.team.lottery.enums.EAdminOperateLogModel;
import com.team.lottery.enums.ELHCKind;
import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.enums.EOrderStatus;
import com.team.lottery.enums.EProstateStatus;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.LhcOrderReportQuery;
import com.team.lottery.extvo.LhcOrderReportVo;
import com.team.lottery.extvo.OrderInfoExportVO;
import com.team.lottery.extvo.OrderQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.util.AdminOperateUtil;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.ExcelUtil;
import com.team.lottery.util.ExcelUtil.ExcelExportData;

import static com.team.lottery.cache.ClsCacheManager.LOTTERY_SWITCH_CACHE;

@Controller("managerOrderAction")
public class ManagerOrderAction {

	private static Logger logger = LoggerFactory.getLogger(ManagerOrderAction.class);

	@Autowired
	private OrderService orderService;
	@Autowired
	private LotteryCacheService lotteryCacheService;
	@Autowired
	private AdminOperateLogService adminOperateLogService;
	@Autowired
	private LotteryWinLhcService lotteryWinLhcService;
	@Autowired
	private LotterySwitchService lotterySwitchService;
	private static EhCacheCacheManager cacheManager;


	/**
	 * 查找符合条件的投注记录
	 * @return
	 */
	public Object[] getAllOrdersByQuery(OrderQuery query, Page page) {
		if (query == null || page == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}

		if (query.getOrderSort() == null) {
			query.setOrderSort(8);
		}
		query.setQueryCode(0);
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		if (!ConstantUtil.SUPER_SYSTEM.equals(currentAdmin.getBizSystem())) {
			query.setBizSystem(currentAdmin.getBizSystem());
		}
		page = orderService.getOrdersByQueryPage(query, page);
		Order totalOrder = orderService.getOrdersTotalByQuery(query);
		return AjaxUtil.createReturnValueSuccess(page, totalOrder);
	}

	/**
	 * 彩票开奖处理
	 * @date 2019年3月12日
	 * @return
	 */
	public Object[] getOfficialLotteryKinds() {
		ELotteryKind[] lotteryTypes = ELotteryKind.values();
		Map<String, String> lotteryMaps = new LinkedHashMap<String, String>();
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		String bizSystem = currentAdmin.getBizSystem();
		// 从缓存中获取参数.
		List<LotterySwitch> lotterySwitchByBizSystem = ClsCacheManager.getLotterySwitchByBizSystem(bizSystem);
		for (LotterySwitch lotterySwitch : lotterySwitchByBizSystem) {
			boolean isOpen = lotterySwitch.getLotteryStatus() == 1;
			if(isOpen){
				for (ELotteryKind type : lotteryTypes){
					boolean equals = type.getCode().equals(lotterySwitch.getLotteryType());
					if (type.isOfficial()&&equals) {
						lotteryMaps.put(type.getCode(), type.getDescription());
					}
				}
			}
		}


		return AjaxUtil.createReturnValueSuccess(lotteryMaps);
	}

	/**
	 * 幸运彩开奖处理 
	 * @date 2019年3月12日
	 * @return
	 */
	public Object[] getPrivateLotteryTypes() {
		ELotteryKind[] lotteryTypes = ELotteryKind.values();
		Map<String, String> lotteryMaps = new LinkedHashMap<String, String>();
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		String bizSystem = currentAdmin.getBizSystem();
		// 从缓存中获取参数.
		List<LotterySwitch> lotterySwitchByBizSystem = ClsCacheManager.getLotterySwitchByBizSystem(bizSystem);
		for (LotterySwitch lotterySwitch : lotterySwitchByBizSystem) {
			boolean isOpen = lotterySwitch.getLotteryStatus() == 1;
			if(isOpen){
				for (ELotteryKind type : lotteryTypes){
					boolean equals = type.getCode().equals(lotterySwitch.getLotteryType());
					if (!type.isOfficial()&&equals) {
						lotteryMaps.put(type.getCode(), type.getDescription());
					}
				}
			}
		}
		return AjaxUtil.createReturnValueSuccess(lotteryMaps);
	}

	/**
	 * 获取所有的彩种类型
	 * @return
	 */
	public Object[] getAllTypes() {
		ELotteryKind[] lotteryTypes = ELotteryKind.values();
		Map<String, String> lotteryMaps = new LinkedHashMap<String, String>();

		for (ELotteryKind type : lotteryTypes) {
			if (type.getIsShow() == 0) {
				continue;
			}
			lotteryMaps.put(type.getCode(), type.getDescription());
		}
		return AjaxUtil.createReturnValueSuccess(lotteryMaps);
	}

	/**
	 * 获取订单详情
	 * @param orderId
	 * @return
	 */
	public Object[] getOrderCodesDes(Long orderId) {
		if (orderId == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		Order order = orderService.selectByPrimaryKey(orderId);
		if (order != null) {
			order.setCodesList(orderService.getCodesDes(order.getCodes(), order.getLotteryType()));
		}
		return AjaxUtil.createReturnValueSuccess(order);
	}

	/**
	 * 订单比对
	 * @return
	 */
	public Object[] comparisonOrderCodes(Long orderId) {
		if (orderId == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		Order order = orderService.selectByPrimaryKey(orderId);
		List<LotteryCache> lotteryCaches = lotteryCacheService.getLotteryCacheMsg(orderId);
		if (lotteryCaches == null) {
			return AjaxUtil.createReturnValueError("投注缓存中,未找到该投注号码.");
		}
		if (lotteryCaches.size() == 0) {
			return AjaxUtil.createReturnValueError("投注缓存中,未找到该条投注记录.");
		}
		if (lotteryCaches.size() != 1) {
			return AjaxUtil.createReturnValueError("投注缓存中,出现多条该投注记录.");
		}
		LotteryCache lotteryCache = lotteryCaches.get(0);
		if (order.getCodes() != null && lotteryCache.getCodes() != null && order.getCodes().equals(lotteryCache.getCodes())) {
			return AjaxUtil.createReturnValueSuccess();
		} else {
			if (order != null) {
				order.setCodesList(orderService.getCodesDes(order.getCodes(), order.getLotteryType()));
				lotteryCache.setCodesList(orderService.getCodesDes(lotteryCache.getCodes(), order.getLotteryType()));
			}
			return AjaxUtil.createReturnValueError(order, lotteryCache);
		}

	}

	/**
	 * 获取普通投注记录的信息
	 * @return
	 * @throws Exception 
	 */
	public Object[] getOrderById(Long orderId) {
		if (orderId == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		Order order = orderService.getOrdertById(orderId);

		order.setCodesList(orderService.getCodesDesByKindReplace(order.getCodes(), order.getLotteryType(), order.getBizSystem()));
		if (new Date().getTime() > order.getCloseDate().getTime()) {
			order.setStatus(EOrderStatus.END.name());
		}
		return AjaxUtil.createReturnValueSuccess(order);
	}

	/**
	 * 获取追号记录的详细信息
	 * @param lotteryId
	 * @return
	 * @throws Exception 
	 */
	public Object[] getAfterNumberOrders(String lotteryId) {
		if (lotteryId == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		List<Order> orders;
		try {
			orders = orderService.getAfterNumberOrders(lotteryId);
		} catch (Exception e) {
			return AjaxUtil.createReturnValueError(e.getMessage());
		}
		return AjaxUtil.createReturnValueSuccess(orders);
	}

	/**
	 * 未开奖的投注回退
	 * @return
	 */
	public Object[] lotteryRegressionForNotDraw(Long orderId) {
		if (orderId == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		try {
			Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
			logger.info("管理员[{}]进行订单未开奖回退，处理订单id[{}]", currentAdmin.getUsername(), orderId);
			orderService.lotteryRegressionForNotDraw(orderId);
		} catch (UnEqualVersionException e) {
			logger.error(e.getMessage(), e);
			return AjaxUtil.createReturnValueError("当前系统繁忙，请稍后重试");
		} catch (Exception e) {
			return AjaxUtil.createReturnValueError(e.getMessage());
		}
		return AjaxUtil.createReturnValueSuccess();
	}

	/**
	 * 投注强制回退
	 * @return
	 */
	public Object[] lotteryRegression(Long orderId) {
		if (orderId == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		try {
			Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
			logger.info("管理员[{}]进行订单投注强制回退，处理订单id[{}]", currentAdmin.getUsername(), orderId);
			orderService.lotteryRegression(orderId);
		} catch (UnEqualVersionException e) {
			logger.error(e.getMessage(), e);
			return AjaxUtil.createReturnValueError("当前系统繁忙，请稍后重试");
		} catch (Exception e) {
			return AjaxUtil.createReturnValueError(e.getMessage());
		}
		return AjaxUtil.createReturnValueSuccess();
	}

	/**
	 * 未开奖回退一键处理
	 * @return
	 */
	public Object[] hendLotteryRegressionDeal(ELotteryKind kind, String expect) {
		if (kind == null || expect == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		// 操作日志保存
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		String lotteryKindName = ELotteryKind.valueOf(kind.name()).getDescription();
		String operateDesc = AdminOperateUtil.constructMessage("hendLotteryRegressionDeal_log", currentAdmin.getUsername(), lotteryKindName,
				expect);
		String operationIp = BaseDwrUtil.getRequestIp();
		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(),
				currentAdmin.getUsername(), null, "", EAdminOperateLogModel.HAND_UPDATE_DEAL, operateDesc);
		int count = 0;
		try {
			count = orderService.hendLotteryRegressionDeal(kind, expect);
		} catch (UnEqualVersionException e1) {
			logger.error("未开奖回退一键处理，更新用户记录时发生版本号不一致，请重试", e1);
			return AjaxUtil.createReturnValueError("未开奖回退一键处理，更新用户记录时发生版本号不一致，请重试");
		} catch (Exception e1) {
			logger.error("未开奖回退一键处理失败", e1);
			return AjaxUtil.createReturnValueError("未开奖回退一键处理失败");
		}

		return AjaxUtil.createReturnValueSuccess(count);
	}

	/**
	 * 未开奖回退一键处理-幸运彩
	 * @return
	 */
	public Object[] hendLotteryRegressionDealForSelfKind(String bizSystem, ELotteryKind kind, String expect) {
		if (kind == null || expect == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		// 操作日志保存
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		String lotteryKindName = ELotteryKind.valueOf(kind.name()).getDescription();
		if (StringUtils.isBlank(bizSystem)) {
			lotteryKindName = "所有业务系统." + lotteryKindName;
		} else {
			lotteryKindName = bizSystem + "." + lotteryKindName;
		}
		String operateDesc = AdminOperateUtil.constructMessage("hendLotteryRegressionDeal_log", currentAdmin.getUsername(), lotteryKindName,
				expect);
		String operationIp = BaseDwrUtil.getRequestIp();
		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(),
				currentAdmin.getUsername(), null, "", EAdminOperateLogModel.HAND_UPDATE_DEAL, operateDesc);
		int count = 0;
		try {
			count = orderService.hendLotteryRegressionDeal(bizSystem, kind, expect);
		} catch (UnEqualVersionException e1) {
			logger.error("未开奖回退一键处理，更新用户记录时发生版本号不一致，请重试", e1);
			return AjaxUtil.createReturnValueError("未开奖回退一键处理，更新用户记录时发生版本号不一致，请重试");
		} catch (Exception e1) {
			logger.error("未开奖回退一键处理失败", e1);
			return AjaxUtil.createReturnValueError("未开奖回退一键处理失败");
		}

		return AjaxUtil.createReturnValueSuccess(count);
	}

	/**
	 * 导出订单信息
	 * @param userName
	 * @param request
	 * @param response
	 * @return
	 */
	public FileTransfer orderInfoExport(OrderQuery query) {
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		logger.info("管理员[{}]，导出了注单报表", currentAdmin.getUsername());
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			List<Order> orderList = new ArrayList<Order>();
			orderList = orderService.getOrderByCondition(query);
			for (Order order : orderList) {
				if (order.getFromType().equals("PC")) {
					order.setFromType("电脑");
				} else if (order.getFromType().equals("MOBILE")) {
					order.setFromType("手机web");
				} else if (order.getFromType().equals("APP")) {
					order.setFromType("APP");
				}
			}
			String fileName = "D:\\" + "TZDD" + DateUtil.getDate(new Date(), "yyyy-MM-dd") + ".xls";
			OrderInfoExportVO orderInfoExportVO = new OrderInfoExportVO();
			orderInfoExportVO.setLotteryId(true);
			orderInfoExportVO.setLotteryTypeDes(true);
			orderInfoExportVO.setExpect(true);
			orderInfoExportVO.setUserName(true);
			orderInfoExportVO.setPayMoney(true);
			orderInfoExportVO.setMultiple(true);
			orderInfoExportVO.setAfterNumber(true);
			orderInfoExportVO.setStatusStr(true);
			orderInfoExportVO.setProstateStatusStr(true);
			orderInfoExportVO.setWinCost(true);
			orderInfoExportVO.setCreatedDateStr(true);
			orderInfoExportVO.setFromType(true);
			Admin admin = BaseDwrUtil.getCurrentAdmin();
			ExcelExportData excelExportData = this.constructOrderInfoExcelData(admin.getBizSystem(), "TZDD", orderList, orderInfoExportVO);
			fileName = new String(fileName.getBytes("GBK"), "iso8859-1");
			bos = ExcelUtil.export2Stream(excelExportData);
			return new FileTransfer(fileName, "application/msexcel", bos.toByteArray());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return null;
	}

	/**
	 * 根据订单构建表格数据
	 * @param users
	 * @return
	 */
	private ExcelExportData constructOrderInfoExcelData(String bizSystem, String fileName, List<Order> orderList,
			OrderInfoExportVO orderInfoExportVO) {
		List<String[]> columNames = new ArrayList<String[]>();
		List<String[]> fieldNames = new ArrayList<String[]>();

		List<String> sheetColumNames = new ArrayList<String>();
		List<String> sheetFieldNames = new ArrayList<String>();
		if (bizSystem.equals("SUPER_SYSTEM")) {
			sheetColumNames.add("业务系统");
			sheetFieldNames.add("bizSystemName");
		}
		if (orderInfoExportVO.getLotteryId()) {
			sheetColumNames.add("注单号");
			sheetFieldNames.add("lotteryId");
		}
		if (orderInfoExportVO.getLotteryTypeDes()) {
			sheetColumNames.add("彩种");
			sheetFieldNames.add("lotteryTypeDes");
		}
		if (orderInfoExportVO.getExpect()) {
			sheetColumNames.add("期号");
			sheetFieldNames.add("expect");
		}
		if (orderInfoExportVO.getUserName()) {
			sheetColumNames.add("用户名");
			sheetFieldNames.add("userName");
		}
		if (orderInfoExportVO.getPayMoney()) {
			sheetColumNames.add("金额");
			sheetFieldNames.add("payMoney");
		}
		if (orderInfoExportVO.getMultiple()) {
			sheetColumNames.add("倍数");
			sheetFieldNames.add("multiple");
		}
		if (orderInfoExportVO.getAfterNumber()) {
			sheetColumNames.add("追号");
			sheetFieldNames.add("afterNumber");
		}
		if (orderInfoExportVO.getStatusStr()) {
			sheetColumNames.add("订单状态");
			sheetFieldNames.add("statusStr");
		}
		if (orderInfoExportVO.getProstateStatusStr()) {
			sheetColumNames.add("开奖状态");
			sheetFieldNames.add("prostateStatusStr");
		}
		if (orderInfoExportVO.getWinCost()) {
			sheetColumNames.add("奖金");
			sheetFieldNames.add("winCost");
		}
		if (orderInfoExportVO.getCreatedDateStr()) {
			sheetColumNames.add("下注时间");
			sheetFieldNames.add("createdDateStr");
		}
		if (orderInfoExportVO.getFromType()) {
			sheetColumNames.add("来源");
			sheetFieldNames.add("fromType");
		}

		columNames.add(sheetColumNames.toArray(new String[sheetColumNames.size()]));
		fieldNames.add(sheetFieldNames.toArray(new String[sheetFieldNames.size()]));

		// 对资料，为空设置为无
//        if(CollectionUtils.isNotEmpty(orderList)) {
//        }
		LinkedHashMap<String, List<?>> map = new LinkedHashMap<String, List<?>>();
		map.put(fileName, orderList);

		ExcelExportData setInfo = new ExcelExportData();
		setInfo.setDataMap(map);
		setInfo.setFieldNames(fieldNames);
		setInfo.setTitles(new String[] {});
		setInfo.setColumnNames(columNames);

		// 将需要导出的数据输出到文件
		return setInfo;
	}

	/**
	 * 查询六合彩报表数据
	 * @return
	 */
	public Object[] queryLhcOrderReport(LhcOrderReportQuery query) {
		if (query == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		if (StringUtils.isEmpty(query.getExpect())) {
			return AjaxUtil.createReturnValueError("期号必须填写");
		}
		// 查询订单表
		OrderQuery orderQuery = new OrderQuery();
		orderQuery.setLotteryType(ELotteryKind.LHC.getCode());
		orderQuery.setExpect(query.getExpect());
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		if (!ConstantUtil.SUPER_SYSTEM.equals(currentAdmin.getBizSystem())) {
			orderQuery.setBizSystem(currentAdmin.getBizSystem());
		} else {
			orderQuery.setBizSystem(query.getBizSystem());
		}
		// 只查询以下几个状态的订单
		List<String> listProstate = new ArrayList<String>();
		listProstate.add(EProstateStatus.NOT_WINNING.getCode());
		listProstate.add(EProstateStatus.WINNING.getCode());
		listProstate.add(EProstateStatus.DEALING.getCode());
		orderQuery.setProstateList(listProstate);
		List<Order> orders = orderService.getOrderByCondition(orderQuery);
		// 查询赔率表
		String lotteryTypeProtype = query.getLotteryTypeProtype();
		LotteryWinLhc lotteryWinQuery = new LotteryWinLhc();
		lotteryWinQuery.setLotteryTypeProtype(lotteryTypeProtype);
		if (!ConstantUtil.SUPER_SYSTEM.equals(currentAdmin.getBizSystem())) {
			lotteryWinQuery.setBizSystem(currentAdmin.getBizSystem());
		} else {
			lotteryWinQuery.setBizSystem(query.getBizSystem());
		}
		Map<String, LhcOrderReportVo> codeReportMap = new LinkedHashMap<String, LhcOrderReportVo>();
		// 多个号码组合的玩法，不需要查询赔率表
		boolean needQuery = true;
		String[] needNotQueryProtypes = new String[] { "LMSQZ", "LMSZ22", "LMSZ23", "LMEQZ", "LMEZT", "LMEZ2", "LMTC", "LMTC", "LMSZ1",
				"HX2", "HX3", "HX4", "HX5", "HX6", "HX7", "HX8", "HX9", "HX10", "HX11", "LXELZ", "LXSLZ", "LXSILZ", "LXWLZ", "LXELBZ",
				"LXSLBZ", "LXSILBZ", "WSLELZ", "WSLSLZ", "WSLSILZ", "WSLELBZ", "WSLSLBZ", "WSLSILBZ", "QBZ5", "QBZ6", "QBZ7", "QBZ8",
				"QBZ9", "QBZ10", "QBZ11", "QBZ12" };
		for (int i = 0; i < needNotQueryProtypes.length; i++) {
			String needNotQueryProtype = needNotQueryProtypes[i];
			if (lotteryTypeProtype.equals(needNotQueryProtype)) {
				needQuery = false;
				break;
			}
		}
		if (needQuery) {
			List<LotteryWinLhc> lotteryWinLhcs = lotteryWinLhcService.getLotteryWinLhcByKind(lotteryWinQuery);
			if (CollectionUtils.isNotEmpty(lotteryWinLhcs)) {
				for (LotteryWinLhc lotteryWinLhc : lotteryWinLhcs) {
					LhcOrderReportVo data = new LhcOrderReportVo();
					data.setCode(lotteryWinLhc.getCode());
					data.setCodeDes(lotteryWinLhc.getCodeDes());
					data.setLotteryMoney(BigDecimal.ZERO);
					data.setWinMoney(BigDecimal.ZERO);
					data.setNum(0);
					codeReportMap.put(lotteryWinLhc.getCode(), data);
				}
			}
		}
		Map<String, LhcOrderReportVo> allReport = new LinkedHashMap<String, LhcOrderReportVo>();
		// 从订单统计各玩法投注号码人数
		if (CollectionUtils.isNotEmpty(orders)) {
			for (Order order : orders) {
				String lotteryCodes = order.getCodes();
				String winCostStrs = order.getWinCostStr();
				String[] lotteryCodeArrays = lotteryCodes.split(ConstantUtil.SOURCECODE_PROTYPE_SPLIT);
				String[] winCostStrArrays = new String[] {};
				if (winCostStrs != null) {
					winCostStrArrays = winCostStrs.split(ConstantUtil.SOURCECODE_SPLIT_KS);
				}
				for (int i = 0; i < lotteryCodeArrays.length; i++) {
					String lotteryCodeProtype = lotteryCodeArrays[i];
					int startPosition = lotteryCodeProtype.indexOf("[");
					int endPosition = lotteryCodeProtype.indexOf("]");
					// 玩法
					String kindPlay = lotteryCodeProtype.substring(startPosition + 1, endPosition);

					int beishuStartPosition = lotteryCodeProtype.lastIndexOf("[");
					// int beishuEndPosition =lotteryCodeProtype.lastIndexOf("]");
					// String beishuStr = lotteryCodeProtype.substring(beishuStartPosition +
					// 1,beishuEndPosition);
					int moneyStartPosition = lotteryCodeProtype.lastIndexOf("{");
					int moneyEndPosition = lotteryCodeProtype.lastIndexOf("}");
					// 金额
					String moneyStr = lotteryCodeProtype.substring(moneyStartPosition + 1, moneyEndPosition);
					BigDecimal lotteryMoney = new BigDecimal(moneyStr);
					// 中奖金额
					BigDecimal winMoney = BigDecimal.ZERO;
					if (winCostStrArrays.length > 0) {
						String winMoneyStr = winCostStrArrays[i];
						winMoney = new BigDecimal(winMoneyStr);
					}
					// 号码
					String code = lotteryCodeProtype.substring(endPosition + 1, beishuStartPosition);

					// 统计所有玩法报表
					LhcOrderReportVo kindPlayReport = allReport.get(kindPlay);
					if (kindPlayReport == null) {
						kindPlayReport = new LhcOrderReportVo();
						kindPlayReport.setKindPlay(kindPlay);
						kindPlayReport.setKindPlayDes(ELHCKind.valueOf(kindPlay).getDescription());
						kindPlayReport.addLotteryMoney(lotteryMoney);
						kindPlayReport.addWinMoney(winMoney);
						kindPlayReport.addUserName(order.getUserName());
						allReport.put(kindPlay, kindPlayReport);
					} else {
						kindPlayReport.addLotteryMoney(lotteryMoney);
						kindPlayReport.addWinMoney(winMoney);
						kindPlayReport.addUserName(order.getUserName());
					}

					// 统计当前玩法号码报表
					if (kindPlay.equals(lotteryTypeProtype)) {
						LhcOrderReportVo codeReport = codeReportMap.get(code);
						if (codeReport == null) {
							codeReport = new LhcOrderReportVo();
							codeReport.setCode(code);
							codeReport.setCodeDes(code);
							codeReport.addLotteryMoney(lotteryMoney);
							codeReport.addWinMoney(winMoney);
							codeReport.addUserName(order.getUserName());
							codeReportMap.put(code, codeReport);
						} else {
							codeReport.addLotteryMoney(lotteryMoney);
							codeReport.addWinMoney(winMoney);
							codeReport.addUserName(order.getUserName());
						}
					}
				}
			}
		}
		// map转为List数据
		List<LhcOrderReportVo> codeReportArray = new ArrayList<LhcOrderReportVo>(codeReportMap.values());
		List<LhcOrderReportVo> allReportArray = new ArrayList<LhcOrderReportVo>(allReport.values());
		return AjaxUtil.createReturnValueSuccess(codeReportArray, allReportArray);
	}
}
