package com.team.lottery.action.web.manage;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.Page;
import com.team.lottery.service.HelpsService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.vo.Helps;

@Controller("managerHelpsAction")
public class ManagerHelpsAction {
	
	private static Logger log = LoggerFactory.getLogger(ManagerHelpsAction.class);

	@Autowired
	private HelpsService helpsService;

	/**
	 * 查找所有的帮助中心
	 * 
	 * @return
	 */
	public Object[] getAllHelps(Helps query, Page page) {
		if (query == null || page == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		String bizSystem = BaseDwrUtil.getCurrentAdmin().getBizSystem();
		if (!"SUPER_SYSTEM".equals(bizSystem)) {
			query.setBizSystem(bizSystem);
		}
		page = helpsService.getAllHelps(query, page);
		return AjaxUtil.createReturnValueSuccess(page);
	}

	/**
	 * 查找指定的帮助中心
	 * 
	 * @return
	 */
	public Object[] getHelpsById(Long id) {
		if(id==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		Helps helps = helpsService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(helps);
	}

	/**
	 * 删除指定的帮助中心
	 * 
	 * @return
	 */
	public Object[] delHelps(Long id) {
		if(id==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		helpsService.deleteByPrimaryKey(id);
		log.info("删除了ID为："+ id + "的信息");
		return AjaxUtil.createReturnValueSuccess();
	}

	/**
	 * 保存帮助中心
	 * 
	 * @return
	 */
	public Object[] saveOrUpdateHelps(Helps helps) {
		if (helps == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		Long id = helps.getId();
		if (id == null) {
			helps.setCreateTime(new Date());
			helps.setCreateAdmin(BaseDwrUtil.getCurrentAdmin().getUsername());
			helpsService.insertSelective(helps);
			log.info("添加了ID为"+ id + "的信息");
		} else {
			helps.setUpdateTime(new Date());
			helps.setUpdateAdmin(BaseDwrUtil.getCurrentAdmin().getUsername());
			helpsService.updateByPrimaryKeySelective(helps);
			log.info("修改了ID为"+ id + "的信息");
		}
		return AjaxUtil.createReturnValueSuccess();
	}
	
	/**
     * 启用停用
     */
	public Object[] HelpsIsShow(Long id,Integer flag) {
		if (id == null||flag==null) {
			return AjaxUtil.createReturnValueError("参数传递错误");
		}
		Helps helps = new Helps();
		helps.setId(id);
		helps.setIsShow(flag);
		int result = helpsService.updateByPrimaryKeySelective(helps);
		if (result > 0) {
			return AjaxUtil.createReturnValueSuccess();
		} else {
			return AjaxUtil.createReturnValueError("添加失败，请重新添加");
		}
	}
}
