package com.team.lottery.action.web.manage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.directwebremoting.io.FileTransfer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.ImageUploadResult;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.ActivityService;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.HttpClientUtil;
import com.team.lottery.util.JSONUtils;
import com.team.lottery.vo.Activity;
import com.team.lottery.vo.Admin;

@Controller("managerActivityAction")
public class ManagerActivityAction {
	
	private static Logger logger = LoggerFactory.getLogger(ManagerActivityAction.class);

	@Autowired
	private ActivityService activityService;
	
    /**
     * 分页查找所有的活动中心
     * @return
     */
    public Object[] getActivitysByQueryPage(Activity query,Page page){
      	if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
      	Admin currentUser = BaseDwrUtil.getCurrentAdmin();
      	String bizSystem = currentUser.getBizSystem();
    	if(query.getBizSystem() == null){
    		if(!"SUPER_SYSTEM".equals(bizSystem)){
    			query.setBizSystem(bizSystem);
    		}
    	}
    	page = activityService.getActivitysByQueryPage(query,page);
    	
		return AjaxUtil.createReturnValueSuccess(page,bizSystem);
    }
    
    /**
     * 查找当前业务系统
     * @return
     */
    public Object[] getcurrentBizSystem(){
    	Admin currentUser = BaseDwrUtil.getCurrentAdmin();
    	String bizSystem = currentUser.getBizSystem();
		return AjaxUtil.createReturnValueSuccess(bizSystem);
    }
    
    /**
     * 查找指定的活动中心
     * @return
     */
    public Object[] getActivityById(Long id){
    	Activity activityCore =  activityService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(activityCore, SystemConfigConstant.imgServerUrl);
    }
    
    /**
     * 删除指定的活动中心
     * @return
     */
    public Object[] delActivity(Long id){
    	activityService.deleteByPrimaryKey(id);
    	return AjaxUtil.createReturnValueSuccess();
    }

    /**
     * 保存活动中心
     * @return
     */
    public Object[] saveOrUpdateActivity(Activity activity){
    	try{
    		Long id = activity.getId();
    		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    		if(!currentAdmin.getBizSystem().equals("SUPER_SYSTEM")){
    			activity.setBizSystem(currentAdmin.getBizSystem());
    		}
    		
        	if(id==null){
        		activity.setCreateDate(new Date());
        		activity.setCreateAdmin(currentAdmin.getUsername());
        		activityService.insertSelective(activity);
        	}else{
        		activity.setUpdateAdmin(currentAdmin.getUsername());
        		activity.setUpdateDate(new Date());
        		activityService.updateByPrimaryKeySelective(activity);
        	}
    		return AjaxUtil.createReturnValueSuccess();
    	}catch(Exception e){
    		return AjaxUtil.createReturnValueError(e.getMessage());
    	}
    }
    
    /**
     * 启用活动
     * @param id
     * @return
     */
    public Object[] enableActivity(Long id){
    	if(id == null){
    		return AjaxUtil.createReturnValueError("参数错误！");
    	}
    	Activity activity = activityService.selectByPrimaryKey(id);
    	if(activity.getStatus() != 1){
    		activity.setStatus(1);
    		activityService.updateByPrimaryKeySelective(activity);
    	}else{
    		return AjaxUtil.createReturnValueError("该活动已启用状态！");
    	}
    	return AjaxUtil.createReturnValueSuccess();
    }
    
    /**
     * 关闭活动
     * @param id
     * @return
     */
    public Object[] disEnableActivity(Long id){
    	if(id == null){
    		return AjaxUtil.createReturnValueError("参数错误！");
    	}
    	Activity activity = activityService.selectByPrimaryKey(id);
    	if(activity.getStatus() != 0){
    		activity.setStatus(0);
    		activityService.updateByPrimaryKeySelective(activity);
    	}else{
    		return AjaxUtil.createReturnValueError("该活动已关闭状态！");
    	}
    	return AjaxUtil.createReturnValueSuccess();
    }
    
    /**
	 * 文件上传
	 * 
	 * @return
	 */
	public Object[] uploadImgFile(FileTransfer fileTransfer, String bizSystem, String picType) {
		try {
			//判断上传文件的类型
			String uploadFileName = fileTransfer.getFilename();
			int index = uploadFileName.indexOf(".");
			String fileType = "";
			if(index > 0) {
				fileType = uploadFileName.substring(index);
				if(!fileType.equalsIgnoreCase(".jpg") && !fileType.equalsIgnoreCase(".png")) {
					return AjaxUtil.createReturnValueError("上传图片文件类型错误，请重新选择");
				}
			}
			if(StringUtils.isEmpty(fileType)) {
				return AjaxUtil.createReturnValueError("上传图片文件类型错误，请重新选择");
			}
			
			//生成文件名称  根据日期
			String newFileName = DateUtil.getDate(new Date(), "yyyyMMddHHmmssSS");
			newFileName += fileType;
			
			WebContext webContext = WebContextFactory.get();
			// String realtivepath = webContext.getServletContext().getContextPath()
			// + "/upload/";
			String saveurl = webContext.getHttpServletRequest().getSession()
					.getServletContext().getRealPath("/upload");
			File fileDir = new File(saveurl);
			if (!fileDir.exists()) {
				fileDir.mkdirs();
			}
			String saveFileUrl = saveurl + "/" + newFileName;
			File file = new File(saveFileUrl);
			InputStream uploadFile = fileTransfer.getInputStream(); 
			int available = uploadFile.available();
			byte[] b = new byte[available];
			FileOutputStream foutput = new FileOutputStream(file);
			uploadFile.read(b);
			foutput.write(b);
			foutput.flush();
			foutput.close();
			uploadFile.close();
			
			logger.info("接收活动图片上传到本地成功，本地图片路径地址[{}],开始上传到图片服务器...", saveFileUrl);
			
			//使用http接口上传图片
			String picSavePath = bizSystem + "/" + picType.toLowerCase() + "/activity";
			String fullSavePath = picSavePath + "/" + newFileName;
			Map<String, String> params = new HashMap<String, String>();
			params.put("path", picSavePath);
			params.put("fileName", newFileName);
			String uploadResult = HttpClientUtil.doPostFileUpload(SystemConfigConstant.imgServerUploadUrl+ConstantUtil.FILE_UPLOAD_RESOURCE, file, params);
			logger.info("上传图片结果：{}", uploadResult);
			
			ImageUploadResult result = JSONUtils.toBean(uploadResult, ImageUploadResult.class);
			if(result == null) {
				return AjaxUtil.createReturnValueError("文件上传图片服务器错误，原因:返回结果为空");
			} 
			if("error".equals(result.getResult())) {
				String mString = result.getMsg();
				return AjaxUtil.createReturnValueError("文件上传图片服务器错误，原因:" + mString);
			}
			//资源服务器上图片访问url地址
//   			String fileUrl = "";
//			try{
//				//上传文件至ftp
//   				fileUrl = SftpClientUtil.uploadFileToSFTP(saveFileUrl, "activity", newFileName);
//			} catch (Exception e) {
//				logger.error("上传文件至ftp出错", e);
//				return AjaxUtil.createReturnValueError("上传文件至图片服务器失败");
//			}
			logger.info("上传活动图片到图片服务器成功，删除本地图片文件");
			//删除文件
			file.delete();
			
			return AjaxUtil.createReturnValueSuccess(fullSavePath, SystemConfigConstant.imgServerUrl);
		} catch(Exception e){
			logger.error("文件上传失败", e);
			return AjaxUtil.createReturnValueError("文件上传失败");
		}
		
	}
	
}
