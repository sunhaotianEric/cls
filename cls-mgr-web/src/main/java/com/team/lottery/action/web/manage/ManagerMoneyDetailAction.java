package com.team.lottery.action.web.manage;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.enums.EMoneyDetailType;
import com.team.lottery.extvo.MoneyDetailQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.MoneyDetailService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.MoneyDetail;

@Controller("managerMoneyDetailAction")
public class ManagerMoneyDetailAction {
    
	@Autowired
	private MoneyDetailService moneyDetailService;
	
    /**
     * 查找符合条件的投注记录
     * @return
     */
    public Object[] getAllMoneyDetailsByQuery(MoneyDetailQuery query,Page page ){
    	if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	
    	Admin currentAdmin=BaseDwrUtil.getCurrentAdmin();
    	if(!ConstantUtil.SUPER_SYSTEM.equals(currentAdmin.getBizSystem())){
    		query.setBizSystem(currentAdmin.getBizSystem());
		}
    	page = moneyDetailService.getAllMoneyDetailsByQueryPage(query, page);
      	MoneyDetail moneyDetailTotal = moneyDetailService.getAllMoneyDetailsTotal(query);
    	
		return AjaxUtil.createReturnValueSuccess(page,moneyDetailTotal);
    }
    
    /**
     * 获取所有的资金明细类型
     * @return
     */
    public Object[] getAllMoneyDetailsDetailTypes(){
    	EMoneyDetailType[] detailTypes = EMoneyDetailType.values();
    	Map<String,String> maps = new HashMap<String,String>();
    	for(EMoneyDetailType type : detailTypes){
    		maps.put(type.getCode(), type.getDescription());
    	}
		return AjaxUtil.createReturnValueSuccess(maps);
    }
    

    /**
     * 获取可分红的最新开始时间
     * @return
     */
    public Object[] getSystemLastestCanBonusStartDate(){
    	Admin currentAdmin=BaseDwrUtil.getCurrentAdmin();
    	if(ConstantUtil.SUPER_SYSTEM.equals(currentAdmin.getBizSystem()))
    	{
    		return AjaxUtil.createReturnValueSuccess("您是SuperSystem用户，暂无权限查询这个！");
    	}
    	MoneyDetail moneyDetail = moneyDetailService.getLastestCanBonusRecord(currentAdmin.getBizSystem());
		return AjaxUtil.createReturnValueSuccess(moneyDetail);
    }
    
    /**
     * 系统分红
     * @return
     */
/*    public Object[] systemBonus(Date endDate){
    	if(endDate == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	//得到真正的结束时间
    	endDate = DateUtil.getNowStartTimeByEnd(endDate);
    	MoneyDetail moneyDetail = moneyDetailService.getLastestCanBonusRecord();
    	if(moneyDetail == null){
    		return AjaxUtil.createReturnValueError("当前无可分红的资金明细记录.");
    	}
    	List<MoneyDetail> bonusMoneyDetails = moneyDetailService.bonusDeal(moneyDetail.getCreatedDate(), endDate);
		return AjaxUtil.createReturnValueSuccess(bonusMoneyDetails);
    }*/
}
