package com.team.lottery.action.web.manage;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.enums.EAdminOperateLogModel;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.AdminOperateLogService;
import com.team.lottery.service.AnnounceService;
import com.team.lottery.service.BizSystemService;
import com.team.lottery.util.AdminOperateUtil;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.Announce;
import com.team.lottery.vo.BizSystem;

@Controller("managerAnnounceAction")
public class ManagerAnnounceAction {
	
	private static Logger log = LoggerFactory.getLogger(ManagerAnnounceAction.class);

	@Autowired
	private AnnounceService announceService;
	@Autowired
	private AdminOperateLogService adminOperateLogService;
	@Autowired
	private BizSystemService bizSystemService;
	
    /**
     * 查找所有的网站公告
     * @return
     */
    public Object[] getAllAnnounces(){
		List<Announce> announces =  announceService.getAllAnnounces();
		return AjaxUtil.createReturnValueSuccess(announces);
    }
    
    /**
     * 查找指定的网站公告
     * @return
     */
    public Object[] getAnnounceById(Integer id){
    	Announce announce =  announceService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(announce);
    }
    
    /**
     * 删除指定的网站公告
     * @return
     */
    public Object[] delAnnounce(Integer id){
    	try {
    		Announce announce = announceService.getAnnounceById(id);
    		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
			String operateDesc = AdminOperateUtil.constructMessage("announce_del", currentAdmin.getUsername(), announce.getTitle());
			String operationIp = BaseDwrUtil.getRequestIp();
			adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.BIZ_SYSTEM_CONFIG, operateDesc);
		} catch (Exception e) {
			//保存日志出现异常 不进行处理
			log.error("删除网站公告，保存添加操作日志出错", e);
		}
    	announceService.deleteByPrimaryKey(id);
    	return AjaxUtil.createReturnValueSuccess();
    }
    
    /**
     * 展示指定的网站公告
     * @return
     */
    public Object[] showAnnounce(Integer id){
    	try{
    		Announce announce =  announceService.selectByPrimaryKey(id);
        	announce.setYetshow(1);
        	announceService.updateByPrimaryKeySelective(announce);
        	return AjaxUtil.createReturnValueSuccess();
    	}catch(Exception e){
    		return AjaxUtil.createReturnValueError(e.getMessage());
    	}
    }
    
    /**
     * 关闭展示指定的网站公告
     * @return
     */
    public Object[] unshowAnnounce(Integer id){
    	try{
    		Announce announce =  announceService.selectByPrimaryKey(id);
        	announce.setYetshow(0);
        	announceService.updateByPrimaryKeySelective(announce);
        	return AjaxUtil.createReturnValueSuccess();
    	}catch(Exception e){
    		return AjaxUtil.createReturnValueError(e.getMessage());
    	}
    }
    
    /**
     * 保存网站公告
     * @return
     */
    public Object[] saveOrUpdateAnnounces(Announce announce){
    	try{
    		Integer id = announce.getId();
    		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    		announce.setUpdateAdmin(currentAdmin.getUsername());
    		String bizSysterm=announce.getBizSystem();
    		if(bizSysterm==null || bizSysterm.equals("SUPER_SYSTEM"))
    		{
    			bizSysterm=currentAdmin.getBizSystem();
    		}
    		announce.setBizSystem(bizSysterm);
    		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    		Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
    		int showdays = announce.getShowDays();
    		if (showdays == -1) {
    			announce.setEndShowDate(sdf.parse("2099-01-01 00:00:00"));
			} else{
				calendar.add(Calendar.DAY_OF_MONTH, +showdays);
				announce.setEndShowDate(calendar.getTime());
			}
    		String operationIp = BaseDwrUtil.getRequestIp();
        	if(id==null){
        		announce.setAddtime(new Date());
        		announce.setYetshow(1);
        		String operateDesc = AdminOperateUtil.constructMessage("announce_add", currentAdmin.getUsername(), announce.getTitle());
        		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.BIZ_SYSTEM_CONFIG, operateDesc);
        		announceService.insertSelective(announce);
        	}else{
        		try {
        			String operateDesc = AdminOperateUtil.constructMessage("announce_edit", currentAdmin.getUsername(), announce.getTitle());
        			adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.BIZ_SYSTEM_CONFIG, operateDesc);
        		} catch (Exception e) {
        			//保存日志出现异常 不进行处理
        			log.error("编辑网站公告，保存添加操作日志出错", e);
        		}
        		announceService.updateByPrimaryKeySelective(announce);
        	}
    		return AjaxUtil.createReturnValueSuccess();
    	}catch(Exception e){
    		return AjaxUtil.createReturnValueError(e.getMessage());
    	}
    	
    }
    
    /**
     * 查找查询启用中的业务系统 
     * @return
     */
    public  Object[] getAllEnableBizSystem()
    {
    	List<BizSystem> bizSystemlist=null;
    	//从缓存上直接读取
    	bizSystemlist=ClsCacheManager.getAllBizSystem();
    	if(bizSystemlist.size()<1){
    		log.debug("缓存中没拿到，从数据库直接取了！");
    		//从数据库直接取
        	bizSystemlist= bizSystemService.getAllEnableBizSystem();	
    	}
    	return AjaxUtil.createReturnValueSuccess(bizSystemlist);
    }
    /**
     * 查询
     * @return
     */
    public Object[] getAllAnnounce(Announce query,Page page){
    	if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	if(!"SUPER_SYSTEM".equals(currentAdmin.getBizSystem()))
    	{
    		query.setBizSystem(currentAdmin.getBizSystem());
    	}
    	
    	page=announceService.getAllAnnounceQueryPage(query, page);
		return AjaxUtil.createReturnValueSuccess(page);
    }
    
    /**
     * 置顶指定的网站公告
     * @return
     */
    public Object[] showAnnounceTopping(Integer id){
    	try{
    		Announce announce =  announceService.selectByPrimaryKey(id);
        	announce.setTopping(1);
        	announceService.updateByPrimaryKeySelective(announce);
        	return AjaxUtil.createReturnValueSuccess();
    	}catch(Exception e){
    		return AjaxUtil.createReturnValueError(e.getMessage());
    	}
    }
    
    /**
     * 取消置顶指定的网站公告
     * @return
     */
    public Object[] unshowAnnounceTopping(Integer id){
    	try{
    		Announce announce =  announceService.selectByPrimaryKey(id);
        	announce.setTopping(0);
        	announceService.updateByPrimaryKeySelective(announce);
        	return AjaxUtil.createReturnValueSuccess();
    	}catch(Exception e){
    		return AjaxUtil.createReturnValueError(e.getMessage());
    	}
    }
}
