package com.team.lottery.action.web.manage;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.enums.EAdminOperateLogModel;
import com.team.lottery.extvo.ChargePayVo;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.WithdrawAutoConfigQuery;
import com.team.lottery.service.AdminOperateLogService;
import com.team.lottery.service.AdminService;
import com.team.lottery.service.ChargePayService;
import com.team.lottery.service.WithdrawAutoConfigService;
import com.team.lottery.util.AdminOperateUtil;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.ChargePay;
import com.team.lottery.vo.WithdrawAutoConfig;

/**
 * 自动出款设置相关Action.
 * 
 * @author Jamine
 *
 */
@Controller("managerWithDrawAutoConfigAction")
public class ManagerWithDrawAutoConfigAction {
	@Autowired
	private WithdrawAutoConfigService withdrawAutoConfigService;
	@Autowired
	private AdminOperateLogService adminOperateLogService;
	@Autowired
	private ChargePayService chargePayService;
	@Autowired
	private AdminService adminService;

	/**
	 * 分页查询所有的自动出款配置.
	 * 
	 * @param query
	 * @param page
	 * @return
	 */
	public Object[] getAllWithdrawAutoConfigByQuery(WithdrawAutoConfigQuery query, Page page) {
		// 判断前端传入参数是否为空
		if (query == null || page == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		// 获取当前的登录对象(如果为超级管理员就查询所有的自动出款对象)
		Admin admin = BaseDwrUtil.getCurrentAdmin();
		if (!"SUPER_SYSTEM".equals(admin.getBizSystem())) {
			query.setBizSystem(admin.getBizSystem());
		}
		page = withdrawAutoConfigService.getWithdrawAutoConfigByQueryPage(query, page);
		return AjaxUtil.createReturnValueSuccess(page);
	}

	/**
	 * 单条自动出入款配置开启关闭(非启用停用);
	 * 
	 * @param id
	 * @param flag
	 * @return
	 */
	public Object[] enableWithdrawAutoConfigSwitch(Integer id, Integer flag) {
		// 判断前端参数是否传递正确.
		if (id == null || flag == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		// 通过ID查询出对应的自动出款对象.
		WithdrawAutoConfig withdrawAutoConfigOld = withdrawAutoConfigService.selectByPrimaryKey(id);
		WithdrawAutoConfig withdrawAutoConfig = new WithdrawAutoConfig();
		withdrawAutoConfig.setId(id);
		withdrawAutoConfig.setAutoWithdrawSwitch(flag);
		// 打印日志.(保存相关的操作日志)
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		String operateDesc = AdminOperateUtil.constructMessage("withdrawAutoConfig_enabled_switch_log", currentAdmin.getUsername(), flag == 1 ? "开启" : "关闭",
				withdrawAutoConfigOld.getThirdPayTypeDesc());
		String operationIp = BaseDwrUtil.getRequestIp();
		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.AUTOCONFIG_MANAGE, operateDesc);
		// 更新对象相关信息.
		int result = withdrawAutoConfigService.updateByPrimaryKeySelective(withdrawAutoConfig);
		if (result > 0) {
			return AjaxUtil.createReturnValueSuccess();
		} else {
			return AjaxUtil.createReturnValueError("操作失败请稍后再试");
		}

	}
	
	/**
	 * 启用停用;
	 * 
	 * @param id
	 * @param flag
	 * @return
	 */
	public Object[] enableWithdrawAutoConfig(Integer id, Integer flag) {
		// 判断前端参数是否传递正确.
		if (id == null || flag == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		
		// 通过ID查询出对应的自动出款对象.
		WithdrawAutoConfig withdrawAutoConfigOld = withdrawAutoConfigService.selectByPrimaryKey(id);
		// 开启的时候需要做一个系统只能开启一个自动出款配置校验.
		if (flag == 1) {
			String bizSystem = withdrawAutoConfigOld.getBizSystem();
			WithdrawAutoConfig withdrawAutoConfig = withdrawAutoConfigService.getWithdrawAutoConfigByBizSystem(bizSystem);
			if (withdrawAutoConfig != null) {
				// 当前系统开启了自动充值对象,就去比较是不是操作的当前充值配置对象
				if (!withdrawAutoConfigOld.getThirdPayType().equals(withdrawAutoConfig.getThirdPayType())) {
					return AjaxUtil.createReturnValueError("每个系统只能开启一个自动出款配置,请停用其他配置后在操作.");
				}
			}
		}
		WithdrawAutoConfig withdrawAutoConfig = new WithdrawAutoConfig();
		withdrawAutoConfig.setId(id);
		withdrawAutoConfig.setEnabled(flag);
		// 打印日志.(保存相关的操作日志)
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		String operateDesc = AdminOperateUtil.constructMessage("withdrawAutoConfig_enabled_log", currentAdmin.getUsername(), flag == 1 ? "启用" : "停用",
				withdrawAutoConfigOld.getThirdPayTypeDesc());
		String operationIp = BaseDwrUtil.getRequestIp();
		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.AUTOCONFIG_MANAGE, operateDesc);
		// 更新对象相关信息.
		int result = withdrawAutoConfigService.updateByPrimaryKeySelective(withdrawAutoConfig);
		if (result > 0) {
			return AjaxUtil.createReturnValueSuccess();
		} else {
			return AjaxUtil.createReturnValueError("操作失败请稍后再试");
		}

	}

	/**
	 * 通过id查询出对应的WithdrawAutoConfig对象.
	 * 
	 * @param id
	 * @return
	 */
	public Object[] getWithdrawAutoConfigById(Integer id) {
		// 判断参数是否正确
		if (id == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		// 通过ID查询出对应的自动出款对象配置对象.
		WithdrawAutoConfig withdrawAutoConfig = withdrawAutoConfigService.selectByPrimaryKey(id);
		if (withdrawAutoConfig != null) {
			return AjaxUtil.createReturnValueSuccess(withdrawAutoConfig);
		} else {
			return AjaxUtil.createReturnValueError("自动出款配置查询错误.");
		}
	}
	
	/**
	 * 通过id查询出对应的WithdrawAutoConfig对象.
	 * 
	 * @param id
	 * @return
	 */
	public Object[] getAllRechargePayByBizSystem(String bizSystem) {
		// 判断参数是否正确.
		/*if (bizSystem == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}*/
		// 新建查询对象.
		ChargePayVo chargePayVo = new ChargePayVo();
		if(bizSystem==null){
			Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
			bizSystem=currentAdmin.getBizSystem();
		}
		chargePayVo.setBizSystem(bizSystem);
		List<ChargePay> chargePays = chargePayService.getAllChargePays(chargePayVo);
		return AjaxUtil.createReturnValueSuccess(chargePays);
	}

	/**
	 * 新增或者修改自动出款对象配置.
	 * 
	 * @param withdrawAutoConfig
	 * @return
	 */
	public Object[] saveOrUpdateWithdrawAutoConfig(WithdrawAutoConfig withdrawAutoConfig) {
		// 判断参数是否正确.
		if (withdrawAutoConfig == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		Long chargePayId = withdrawAutoConfig.getChargePayId();
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		ChargePay chargePay = chargePayService.selectByPrimaryKey(chargePayId);
		// 每个系统只能开启一个自动出款配置信息.
		Integer enabled = withdrawAutoConfig.getEnabled();
		
		// 获取Id来判断用户是新增还是修改.
		Integer id = withdrawAutoConfig.getId();
		// 查询出第三方支付对象相关信息.
		
		withdrawAutoConfig.setMemberId(chargePay.getMemberId());
		withdrawAutoConfig.setThirdPayType(chargePay.getChargeType());
		withdrawAutoConfig.setThirdPayTypeDesc(chargePay.getChargeDes());
		withdrawAutoConfig.setUpdateAdmin(currentAdmin.getUsername());
		//查询出管理员对象相关信息.
		Integer dealAdminId = withdrawAutoConfig.getDealAdminId();
		Admin admin = adminService.selectByPrimaryKey(dealAdminId);
		withdrawAutoConfig.setDealAdminUsername(admin.getUsername());
		if (!"SUPER_SYSTEM".equals(currentAdmin.getBizSystem())) {
			withdrawAutoConfig.setBizSystem(currentAdmin.getBizSystem());
		}
		if (id != null) {
			if (enabled == 1) {
				String bizSystem = withdrawAutoConfig.getBizSystem();
				// 根据系统查询开启对应系统是否有开启的自动出款配置.
				WithdrawAutoConfig currentWithdrawAutoConfig = withdrawAutoConfigService.getWithdrawAutoConfigByBizSystem(bizSystem);
				if (currentWithdrawAutoConfig != null) {
					// 当前系统开启了自动充值对象,就去比较是不是操作的当前充值配置对象
					if (!chargePay.getChargeType().equals(currentWithdrawAutoConfig.getThirdPayType())) {
						return AjaxUtil.createReturnValueError("每个系统只能开启一个自动出款配置,请停用其他配置后在操作.");
					}
				}
			}
			withdrawAutoConfig.setUpdateTime(new Date());
			int i = withdrawAutoConfigService.updateByPrimaryKeySelective(withdrawAutoConfig);
			if (i > 0) {
				return AjaxUtil.createReturnValueSuccess();
			} else {
				return AjaxUtil.createReturnValueError();
			}
		} else {
			try {
				if (enabled == 1) {
					String bizSystem = withdrawAutoConfig.getBizSystem();
					// 根据系统查询开启对应系统是否有开启的自动出款配置.
					WithdrawAutoConfig currentWithdrawAutoConfig = withdrawAutoConfigService.getWithdrawAutoConfigByBizSystem(bizSystem);
					if (currentWithdrawAutoConfig != null) {
						// 当前系统开启了自动充值对象,就去比较是不是操作的当前充值配置对象
						if (!withdrawAutoConfig.getThirdPayType().equals(currentWithdrawAutoConfig.getThirdPayType())) {
							return AjaxUtil.createReturnValueError("每个系统只能开启一个自动出款配置,请停用其他配置后在操作.");
						}
					}
				}
				withdrawAutoConfig.setCreateTime(new Date());
				int i = withdrawAutoConfigService.insertSelective(withdrawAutoConfig);
				if (i > 0) {
					return AjaxUtil.createReturnValueSuccess();
				} else {
					return AjaxUtil.createReturnValueError();
				}
			} catch (Exception e) {
				return AjaxUtil.createReturnValueError("新增失败");
			}
			
		}
	}
	
	/**
	 * 根据bizSystem查询出对应的充值配置对象
	 * @param bizSystem
	 * @return
	 */
	public Object[] getWithdrawAutoConfig() {
		// 通过bizSystem查询出对应的自动出款对象配置对象.	
		Admin admin = BaseDwrUtil.getCurrentAdmin();
		WithdrawAutoConfig withdrawAutoConfig = withdrawAutoConfigService.getWithdrawAutoConfig(admin.getBizSystem());
		if(withdrawAutoConfig == null){
			return AjaxUtil.createReturnValueSuccess(admin.getBizSystem());
		}
		return AjaxUtil.createReturnValueSuccess(withdrawAutoConfig);
	}

	/**
	 * 通过id查询出对应的WithdrawAutoConfig对象.
	 * 
	 * @param id
	 * @return
	 */
	public Object[] getAllDealadminByBizSystem(String bizSystem) {
		// 判断参数是否正确.
		/*if (bizSystem == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}*/
		
		if(bizSystem==null){
			Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
			bizSystem=currentAdmin.getBizSystem();
		}
		Admin admin = new Admin();
		admin.setBizSystem(bizSystem);
		List<Admin> admins = adminService.getAllAdminByBizSystem(admin);
		return AjaxUtil.createReturnValueSuccess(admins);
	}
}
