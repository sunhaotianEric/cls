package com.team.lottery.action.web.manage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.Page;
import com.team.lottery.service.LevelSytemService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.LevelSystem;

@Controller("manageLevelSytemAction")
public class ManageLevelSytemAction {

	@Autowired
	private LevelSytemService levelSytemService;
	
	/**
	 * 分页查询会员等级机制
	 * @param query
	 * @return
	 */
	public Object[] getLevelSytem(LevelSystem query,Page page){
		if(query==null||page==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		String bizSystem=BaseDwrUtil.getCurrentAdmin().getBizSystem();
		if(!bizSystem.equals(ConstantUtil.SUPER_SYSTEM)){
			query.setBizSystem(bizSystem);
		}
		page=levelSytemService.getLevelSytem(query,page);
		return AjaxUtil.createReturnValueSuccess(page);
	}
	
	/**
	 * 根据ID查询单个会员等级机制信息
	 * @param id
	 * @return
	 */
	public Object[] getLevelSystemByid(int id){
		LevelSystem query=levelSytemService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(query);
	}
	
	/**
	 * 修改会员等级机制
	 * @param querylist
	 * @return
	 */
	public Object[] updateLevelSystem(LevelSystem query){
		if(query==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		int i=levelSytemService.getLevelSystemBylevel(query);
		if(i>0){
			return AjaxUtil.createReturnValueError("已存在相同的等级");
		}else{
			Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
			String bizSystem= currentAdmin.getBizSystem();
			if(!bizSystem.equals(ConstantUtil.SUPER_SYSTEM)){
				query.setBizSystem(null);
				query.setLevel(null);
				query.setLevelName(null);
			}
			levelSytemService.updateLevelSystem(query, currentAdmin);
		}
		return AjaxUtil.createReturnValueSuccess();
	}
	
	/**
	 * 添加会员等级机制
	 * @param query
	 * @return
	 */
	public Object[] addLevelSystem(LevelSystem query){
		if(query==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		int i=levelSytemService.getLevelSystemBylevel(query);
		if(i>0){
			return AjaxUtil.createReturnValueError("已存在相同的等级");
		}else{
			levelSytemService.addLevelSystem(query);
		}
		return AjaxUtil.createReturnValueSuccess();
	}
	
	/**
	 * 根据ID删除会员等级机制
	 * @param id
	 * @return
	 */
	public Object[] delLevelSystem(Integer id){
		if(id==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		levelSytemService.deleteByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess();
	}
}
