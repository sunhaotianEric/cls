package com.team.lottery.action.web.manage;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.Page;
import com.team.lottery.service.SigninConfigService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.SigninConfig;

@Controller("managerSignInConfigAction")
public class ManagerSignInConfigAction {
	@Autowired
	private SigninConfigService signinConfigService;
	
	/**
     * 查找所有的快捷支付信息
     * @return
     */
    public Object[] getAllSignInConfig(SigninConfig query,Page page){
    	if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	if(!(ConstantUtil.SUPER_SYSTEM).equals(currentAdmin.getBizSystem()))
    	{
    		query.setBizSystem(currentAdmin.getBizSystem());
    	}
    	page=signinConfigService.getAllSignInConfigByQueryPage(query, page);
		return AjaxUtil.createReturnValueSuccess(page);
    }


    /**
     * 删除指定的快捷支付设置
     * @return
     */
    public Object[] delSignInConfig(Integer id){
    	
    	signinConfigService.deleteByPrimaryKey(id);
    	
    	return AjaxUtil.createReturnValueSuccess();
    }
    /**
     * 保存签到设置
     * @return
     */
    public Object[] saveOrUpdateSignInConfig(SigninConfig signinConfig){
    	try{
    		Integer id = signinConfig.getId();
    		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    		if(!ConstantUtil.SUPER_SYSTEM.equals(currentAdmin.getBizSystem())){
    			signinConfig.setBizSystem(currentAdmin.getBizSystem());
        	}
        	if(id==null){
        		signinConfig.setCreateTime(new Date());
        		List<SigninConfig> querySigninConfig=signinConfigService.selectByQuery(signinConfig);
        		if(querySigninConfig!=null && querySigninConfig.size()>0){
        			return AjaxUtil.createReturnValueError("你所输入的签到设置已经存在,请重新输入!");
        		}
        			
        		signinConfigService.insert(signinConfig);
        	}else{
        		signinConfig.setUpdateTime(new Date());
        		signinConfig.setUpdateAdmin(currentAdmin.getUsername());
        		signinConfigService.updateByPrimaryKeySelective(signinConfig);
        	}
        	/*//通知前端应用刷新缓存就够了
        	try {
    			NotifyFrontInterface.notifyFrontRefreshCache(EFrontCacheType.FRONT_INDEX_CACHE_INFO.getCode());
    		} catch (Exception e) {
    			log.error(e.getMessage());
    			AjaxUtil.createReturnValueError(e.getMessage());
    		}*/
    		return AjaxUtil.createReturnValueSuccess();
    	}catch(Exception e){
    		return AjaxUtil.createReturnValueError(e.getMessage());
    	}
    	
    }
    /**
     * 查找指定的快捷支付设置
     * @return
     */
    public Object[] getSignInConfigById(Integer id){
    	SigninConfig signinConfig =  signinConfigService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(signinConfig);
    }
}
