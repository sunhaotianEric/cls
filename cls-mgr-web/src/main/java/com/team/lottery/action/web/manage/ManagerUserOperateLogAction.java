package com.team.lottery.action.web.manage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.UserOperateLogQuery;
import com.team.lottery.service.UserOperateLogService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;

@Controller("managerUserOperateLogAction")
public class ManagerUserOperateLogAction {

	@Autowired
	private UserOperateLogService userOperateLogService;
	
	 /**
     * 查找所有的系统数据
     * @return
     */
    public Object[] getAllUserOperateLogs(UserOperateLogQuery query,Page page){
    	if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	String currentSystem = BaseDwrUtil.getCurrentAdmin().getBizSystem();
    	if(!currentSystem.equals("SUPER_SYSTEM")){
    		query.setBizSystem(currentSystem);
    	}
    	page = userOperateLogService.getAllUserOperateLogByQueryPage(query,page);
		return AjaxUtil.createReturnValueSuccess(page);
    }
    
}
