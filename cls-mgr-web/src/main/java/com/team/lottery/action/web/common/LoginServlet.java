package com.team.lottery.action.web.common;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.util.ConstantUtil;

/**
 * 登录请求实现页面跳转，暂时无用，由过滤器直接处理掉
 * @deprecated
 */
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private static Logger log = LoggerFactory.getLogger(LoginServlet.class);

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    Object user = request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	    log.info("域名地址:" + request.getContextPath());
    	log.info("访问的basePath" + request.getContextPath());
	    if (user != null) {
//	    	request.getRequestDispatcher("gameBet/index.html").forward(request, response);
	        response.sendRedirect(request.getContextPath() + "/managerzaizst/index.html");
	    }else{
//	    	request.getRequestDispatcher("gameBet/login.html").forward(request, response);
	        response.sendRedirect(request.getContextPath() + "/managerzaizst/login.html");
	    }
	}

}
