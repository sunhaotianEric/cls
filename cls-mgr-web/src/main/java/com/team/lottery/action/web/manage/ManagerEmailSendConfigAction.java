package com.team.lottery.action.web.manage;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.Page;
import com.team.lottery.service.EmailSendConfigService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.vo.EmailSendConfig;


@Controller("managerEmailSendConfigAction")
public class ManagerEmailSendConfigAction {

	@Autowired
	private EmailSendConfigService emailSendConfigService;
	
	/**
	 * 分页查询
	 * @param query
	 * @param page
	 * @return
	 */
	public Object[] getAllEmailSendConfig(EmailSendConfig query,Page page){
		if(query==null||page==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		page=emailSendConfigService.getAllEmailSendConfig(query, page);
		return AjaxUtil.createReturnValueSuccess(page);
	}
	
	/**
	 * 根据ID查询
	 * @param id
	 * @return
	 */
	public Object[] selectEmailSendConfig(Integer id){
		EmailSendConfig record=emailSendConfigService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(record);
	}
	
	/**
	 * 修改
	 * @param record
	 * @return
	 */
	public Object[] updateEmailSendConfig(EmailSendConfig record){
		if(record==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		emailSendConfigService.updateByPrimaryKeySelective(record);
		return AjaxUtil.createReturnValueSuccess();
	}
	
	/**
	 * 根据ID删除
	 * @param id
	 * @return
	 */
	public Object[] delEmailSendConfigById(Integer id){
		if(id==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		emailSendConfigService.deleteByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess();
	}
	
	/**
	 * 保存
	 * @param record
	 * @return
	 */
	public Object[] saveEmailSendConfig(EmailSendConfig record){
    	if(record == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	Integer id = record.getId();
    	if(id == null){
    		record.setCreateTime(new Date());
    		emailSendConfigService.insertSelective(record);
    	}else{
    		record.setUpdateTime(new Date());
    		emailSendConfigService.updateByPrimaryKeySelective(record);
    	}
    	return AjaxUtil.createReturnValueSuccess();
    }
	
	/**
     * 启用停用
     */
	public Object[] enableEmail(Integer id,Integer flag) {
		if (id == null||flag==null) {
			return AjaxUtil.createReturnValueError("参数传递错误");
		}
		EmailSendConfig emailSendConfig = new EmailSendConfig();
		emailSendConfig.setId(id);
		emailSendConfig.setEnable(flag);
		int result = emailSendConfigService.updateByPrimaryKeySelective(emailSendConfig);
		if (result > 0) {
			return AjaxUtil.createReturnValueSuccess();
		} else {
			return AjaxUtil.createReturnValueError("添加失败，请重新添加");
		}
	}
}
