package com.team.lottery.action.web.manage;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.directwebremoting.io.FileTransfer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.enums.EDayConsumeActivityOrderStatus;
import com.team.lottery.extvo.DayConsumeActityOrderInfoExportVO;
import com.team.lottery.extvo.DayConsumeActityOrderQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.DayConsumeActityConfigService;
import com.team.lottery.service.DayConsumeActityOrderService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.ExcelUtil;
import com.team.lottery.util.ExcelUtil.ExcelExportData;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.DayConsumeActityConfig;
import com.team.lottery.vo.DayConsumeActityOrder;

@Controller("managerDayConsumeActityOrderAction")
public class ManagerDayConsumeActityOrderAction {
	
	@Autowired
	private DayConsumeActityOrderService dayConsumeActityOrderService;
	
	@Autowired
	private DayConsumeActityConfigService dayConsumeActityConfigService;
	
	/**
	 * 分页查流水返送订单
	 * @param query
	 * @param page
	 * @return
	 */
	public Object[] getDayConsumeActityOrderPage(DayConsumeActityOrderQuery query,Page page){
		if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
		
    	String bizSystem = BaseDwrUtil.getCurrentAdmin().getBizSystem();
    	if(!"SUPER_SYSTEM".equals(bizSystem)){
    		query.setBizSystem(bizSystem);
    	}
    	Page dayConsumeActivitysPage = dayConsumeActityOrderService.getDayConsumeActityOrderPage(query, page);
    	return AjaxUtil.createReturnValueSuccess(dayConsumeActivitysPage);
	}
	
	 /**
     * 发放流水返送金额
     * @param id
     * @return
     */
    public Object[] giveActityMoney(Long id){
    	//获取当前admin，并判断当前admin状态是否为客服管理员，是的话提示
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	Integer state = currentAdmin.getState();
    	if (state == 3) {
			return AjaxUtil.createReturnValueError("客服管理员不能操作此功能！");
		}
		try {
			dayConsumeActityOrderService.giveActityMoney(id, currentAdmin);
		} catch (Exception e) {
			return AjaxUtil.createReturnValueError("赠送活动金失败");
		}
		
    	return AjaxUtil.createReturnValueSuccess();
    }
    /**
     * 分分彩发放流水返送金额
     * @param id
     * @return
     */
    public Object[] giveActityFfcMoney(Long id){
    	//获取当前admin，并判断当前admin状态是否为客服管理员，是的话提示
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	Integer state = currentAdmin.getState();
    	if (state == 3) {
			return AjaxUtil.createReturnValueError("客服管理员不能操作此功能！");
		}
		try {
			dayConsumeActityOrderService.giveActityFfcMoney(id, currentAdmin);
		} catch (Exception e) {
			return AjaxUtil.createReturnValueError("赠送活动金失败");
		}
		
    	return AjaxUtil.createReturnValueSuccess();
    }
    
    /**
     * 一键发放流水返送金额
     * @param query
     * @return
     */
    public Object[] onekeyGiveActityMoney(DayConsumeActityOrderQuery query){
    	if(query == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	String bizSystem = currentAdmin.getBizSystem();
    	if(!"SUPER_SYSTEM".equals(bizSystem)){
    		query.setBizSystem(bizSystem);
    	}
    	//处理订单数
    	int count = 0;
		try {
	    	List<DayConsumeActityOrder> orderBybelongDate = dayConsumeActityOrderService.getDayConsumeActityOrderBybelongDate(query);
	    	if(CollectionUtils.isNotEmpty(orderBybelongDate)){
	    		for(DayConsumeActityOrder order:orderBybelongDate){
	    			if(order.getReleaseStatus().equals(EDayConsumeActivityOrderStatus.APPLIED.getCode())){
		    			dayConsumeActityOrderService.giveActityMoney(order.getId(), currentAdmin);
		    			count ++;
	    			}
	    			if(order.getFfcReleaseStatus().equals(EDayConsumeActivityOrderStatus.APPLIED.getCode())){
		    			dayConsumeActityOrderService.giveActityFfcMoney(order.getId(), currentAdmin);
			    		count ++;
	    			}
	    		}
	    	}
			
		} catch (Exception e) {
			return AjaxUtil.createReturnValueError("赠送流水返送金额失败");
		}
		
    	return AjaxUtil.createReturnValueSuccess(count);
    }
    
	/**
     * 修改流水返送订单
     * @param dayConsumeActityOrder
     * @return
     */
    public Object[] updateDayConsumeActityOrder(DayConsumeActityOrder dayConsumeActityOrder){
    	if(dayConsumeActityOrder == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		if(!currentAdmin.getBizSystem().equals("SUPER_SYSTEM")){
			dayConsumeActityOrder.setBizSystem(currentAdmin.getBizSystem());
		}
		dayConsumeActityOrder.setUpdateDate(new Date());
		dayConsumeActityOrderService.updateByPrimaryKeySelective(dayConsumeActityOrder);
		
    	return AjaxUtil.createReturnValueSuccess();
    }
    
    
    /**
     * 查找订单
     * @param Long
     * @return
     */
    public Object[] getDayConsumeActityOrderById(Long id){
    	if(id == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	
    	DayConsumeActityOrder dayConsumeActityOrder = dayConsumeActityOrderService.selectByPrimaryKey(id);
    	return AjaxUtil.createReturnValueSuccess(dayConsumeActityOrder);
    }
    
    /**
     * 删除订单
     * @param Long
     * @return
     */
    public Object[] delDayConsumeActityOrder(Long id){
    	if(id == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	
    	dayConsumeActityOrderService.deleteByPrimaryKey(id);
    	return AjaxUtil.createReturnValueSuccess();
    }
    public  Object[] getBizSystemByDayConsumeActivityConfig(DayConsumeActityConfig query){
    	List<DayConsumeActityConfig> list=dayConsumeActityConfigService.getDayConsumeActityConfigsQuery(query);
    	if(list==null || list.size()==0){
    		list=null;
    	}
    	return  AjaxUtil.createReturnValueSuccess(list);
    }
    /**
     * 导出用户信息
     * @param userName
     * @param request
     * @param response
     * @return
     */
    public FileTransfer dayConsumeActityOrderInfoExport(DayConsumeActityOrderQuery query) {
    	
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            List<DayConsumeActityOrder> orderList = new ArrayList<DayConsumeActityOrder>();
            if(query.getBelongDate() != null){
        		// 初始化时设置 日期和时间模式
        		Calendar calendar = new GregorianCalendar();
        		calendar.setTime(query.getBelongDate());
        		calendar.set(Calendar.HOUR_OF_DAY, 23);  
        		calendar.set(Calendar.MINUTE, 59);  
        		calendar.set(Calendar.SECOND, 59);  
        		Date endTime = calendar.getTime();
        		query.setBelongDateStart(query.getBelongDate());
        		query.setBelongDateEnd(endTime);
        	}
            orderList = dayConsumeActityOrderService.getDayConsumeActityOrderByCondition(query);
            for(DayConsumeActityOrder dayConsumeActityOrder:orderList){
            	if(dayConsumeActityOrder.getReleaseStatus().equals("NOT_APPLY")){
            		dayConsumeActityOrder.setReleaseStatus("未申请");
            	}else if(dayConsumeActityOrder.getReleaseStatus().equals("APPLIED")){
            		dayConsumeActityOrder.setReleaseStatus("已申请");
            	}else if(dayConsumeActityOrder.getReleaseStatus().equals("RELEASED")){
            		dayConsumeActityOrder.setReleaseStatus("已发放");
            	}else if(dayConsumeActityOrder.getReleaseStatus().equals("NOTALLOWED_APPLY")){
            		dayConsumeActityOrder.setReleaseStatus("不可申请");
            	}
            	
            	if(dayConsumeActityOrder.getFfcReleaseStatus().equals("NOT_APPLY")){
            		dayConsumeActityOrder.setFfcReleaseStatus("未申请");
            	}else if(dayConsumeActityOrder.getFfcReleaseStatus().equals("APPLIED")){
            		dayConsumeActityOrder.setFfcReleaseStatus("已申请");
            	}else if(dayConsumeActityOrder.getFfcReleaseStatus().equals("RELEASED")){
            		dayConsumeActityOrder.setFfcReleaseStatus("已发放");
            	}else if(dayConsumeActityOrder.getFfcReleaseStatus().equals("NOTALLOWED_APPLY")){
            		dayConsumeActityOrder.setFfcReleaseStatus("不可申请");
            	}
            }
            String fileName = "D:\\" + "TSFSDD" + DateUtil.getDate(new Date(), "yyyy-MM-dd") + ".xls";
            DayConsumeActityOrderInfoExportVO dayConsumeActityOrderInfoExportVO=new DayConsumeActityOrderInfoExportVO ();
            dayConsumeActityOrderInfoExportVO.setSerialNumber(true);
            dayConsumeActityOrderInfoExportVO.setUserName(true);
            dayConsumeActityOrderInfoExportVO.setMoney(true);
            dayConsumeActityOrderInfoExportVO.setLotteryMoney(true);
            dayConsumeActityOrderInfoExportVO.setFfcLotteryMoney(true);
            dayConsumeActityOrderInfoExportVO.setFfcMoney(true);
            dayConsumeActityOrderInfoExportVO.setBelongDateStr(true);
            dayConsumeActityOrderInfoExportVO.setReleaseStatus(true);
            dayConsumeActityOrderInfoExportVO.setFfcReleaseStatus(true);
            dayConsumeActityOrderInfoExportVO.setAppliyDateStr(true);
            Admin admin = BaseDwrUtil.getCurrentAdmin();
            ExcelExportData excelExportData = this.constructDayConsumeActityOrderInfoExcelData(admin.getBizSystem(),"TSFSDD", orderList, dayConsumeActityOrderInfoExportVO);
            fileName = new String(fileName.getBytes("GBK"),"iso8859-1");
            bos = ExcelUtil.export2Stream(excelExportData);
            return new FileTransfer(fileName, "application/msexcel", bos.toByteArray());  
        } catch (Exception e) {
            /*log.error(e.getMessage(), e);*/
        } 
        return null;
    }
    
    /**
     * 根据用户名构建表格数据
     * @param users
     * @return
     */
    private ExcelExportData constructDayConsumeActityOrderInfoExcelData(String bizSystem,String fileName, List<DayConsumeActityOrder> orderList, DayConsumeActityOrderInfoExportVO dayConsumeActityOrderInfoExportVO) {
    	List<String[]> columNames = new ArrayList<String[]>();
    	List<String[]> fieldNames = new ArrayList<String[]>();
    	
    	List<String> sheetColumNames = new ArrayList<String>();
    	List<String> sheetFieldNames = new ArrayList<String>();
    	if(bizSystem.equals("SUPER_SYSTEM")){
    		sheetColumNames.add("业务系统");
        	sheetFieldNames.add("bizSystemName");
    	}
    	if(dayConsumeActityOrderInfoExportVO.getSerialNumber()) {
    		sheetColumNames.add("订单号");
        	sheetFieldNames.add("serialNumber");
    	}
    	if(dayConsumeActityOrderInfoExportVO.getUserName()) {
    		sheetColumNames.add("用户名");
        	sheetFieldNames.add("userName");
    	}
    	if(dayConsumeActityOrderInfoExportVO.getMoney()) {
    		sheetColumNames.add("领取金额");
        	sheetFieldNames.add("money");
    	}
    	if(dayConsumeActityOrderInfoExportVO.getLotteryMoney()) {
    		sheetColumNames.add("投注金额");
        	sheetFieldNames.add("lotteryMoney");
    	}
    	if(dayConsumeActityOrderInfoExportVO.getFfcLotteryMoney()) {
    		sheetColumNames.add("分分彩投注额");
        	sheetFieldNames.add("ffcLotteryMoney");
    	}
    	if(dayConsumeActityOrderInfoExportVO.getFfcMoney()) {
    		sheetColumNames.add("分分彩领取金额");
        	sheetFieldNames.add("ffcMoney");
    	}
    	if(dayConsumeActityOrderInfoExportVO.getBelongDateStr()) {
    		sheetColumNames.add("归属日期");
        	sheetFieldNames.add("belongDateStr");
    	}
    	if(dayConsumeActityOrderInfoExportVO.getReleaseStatus()) {
    		sheetColumNames.add("发放处理状态");
        	sheetFieldNames.add("releaseStatus");
    	}
    	if(dayConsumeActityOrderInfoExportVO.getFfcReleaseStatus()) {
    		sheetColumNames.add("分分彩发放处理状态");
        	sheetFieldNames.add("ffcReleaseStatus");
    	}
    	if(dayConsumeActityOrderInfoExportVO.getAppliyDateStr()) {
    		sheetColumNames.add("申请时间");
        	sheetFieldNames.add("appliyDateStr");
    	}
    	
        columNames.add(sheetColumNames.toArray(new String[sheetColumNames.size()]));
        fieldNames.add(sheetFieldNames.toArray(new String[sheetFieldNames.size()]));

        //对user资料，为空设置为无
        if(CollectionUtils.isNotEmpty(orderList)) {
        	/*for(Order u : orderList) {
        		if(StringUtils.isEmpty(u.getTrueName())) {
        			u.setTrueName("无");
        		}
        		if(StringUtils.isEmpty(u.getIdentityid())) {
        			u.setIdentityid("无");
        		}
        		if(StringUtils.isEmpty(u.getPhone())) {
        			u.setPhone("无");
        		}
        		if(StringUtils.isEmpty(u.getQq())) {
        			u.setQq("无");
        		}
        		if(StringUtils.isEmpty(u.getEmail())) {
        			u.setEmail("无");
        		}
        		String address = "";
        		if(StringUtils.isEmpty(u.getProvince()) && StringUtils.isEmpty(u.getCity()) &&StringUtils.isEmpty(u.getAddress())) {
        			address = "无";
        		} else {
        			if(!StringUtils.isEmpty(u.getProvince())) {
        				address += u.getProvince();
        			}
        			if(!StringUtils.isEmpty(u.getCity())) {
        				address += u.getCity();
        			}
        			if(!StringUtils.isEmpty(u.getAddress())) {
        				address += u.getAddress();
        			}
        		}
        		u.setAddress(address);
        	}*/
        }
        LinkedHashMap<String, List<?>> map = new LinkedHashMap<String, List<?>>();
        map.put(fileName, orderList);
        
        
        ExcelExportData setInfo = new ExcelExportData();
        setInfo.setDataMap(map);
        setInfo.setFieldNames(fieldNames);
        setInfo.setTitles(new String[] {});
        setInfo.setColumnNames(columNames);

        // 将需要导出的数据输出到文件
        return setInfo;
    }
}
