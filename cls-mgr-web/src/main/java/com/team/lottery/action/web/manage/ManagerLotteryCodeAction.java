package com.team.lottery.action.web.manage;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.extvo.LotteryCodeQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.SystemLotteryInterfaceUrlInfo;
import com.team.lottery.service.LotteryCodeService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.util.URLConnectUtil;
import com.team.lottery.vo.LotteryCode;

@Controller("managerLotteryCodeAction")
public class ManagerLotteryCodeAction {
	
	private static Logger log = LoggerFactory.getLogger(ManagerLotteryCodeAction.class);

	@Autowired
	private LotteryCodeService lotteryCodeService;
	
    /**
     * 查找所有的奖金期号数据
     * @return
     */
    public Object[] getAllLotteryCode(LotteryCodeQuery query,Page page){
    	if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	page = lotteryCodeService.getAllLotteryCodeByQueryPage(query,page);
		return AjaxUtil.createReturnValueSuccess(page);
    }
    
    /**
     * 同步数据库到redis并查找所有的奖金期号数据
     * @return
     */
    public Object[] resetRedisLotteryCode(LotteryCodeQuery query){
    	if(query == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	if (StringUtils.isEmpty(query.getLotteryName())) {
    		return AjaxUtil.createReturnValueError("参数LotteryName为空.");
		}
    	List<LotteryCode> lotteryCodes = lotteryCodeService.getNearestSixHundredLotteryCode(query.getLotteryName());
    	Boolean state = lotteryCodeService.resetRedisLotteryCode(lotteryCodes, query.getLotteryName());
    	if (state == true) {
    		return AjaxUtil.createReturnValueSuccess(state);
		}else {
			return AjaxUtil.createReturnValueError(state);
		}
    }
    
    /**
     * 删除开奖数据
     * @return
     */
    public Object[] deleteLotteryCodeById(Integer lotteryCodeId){
    	//根据彩种获得最新期的开奖信息
		LotteryCode lotteryCode = lotteryCodeService.selectByPrimaryKey(lotteryCodeId);
		//删除数据库开奖记录
		int res = lotteryCodeService.deleteByPrimaryKey(lotteryCodeId);
		if(res > 0) {
			//联动redis删除
	    	boolean delRedisSuccess = lotteryCodeService.delRedisLotteryCode(lotteryCode);
	    	if(!delRedisSuccess){
	    		AjaxUtil.createReturnValueError("数据库删除成功，redis数据删除失败！");
	    	}
		}
    	
		return AjaxUtil.createReturnValueSuccess();
    }
    
    
    /**
     * 查询开奖接口数据
     * @param interfaceType 开奖接口的类型   0正式接口   1备用接口
     * @return
     */
    public Object[] queryInterfaceData(ELotteryKind lotteryKind, Integer interfaceType){
    	if(lotteryKind == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	String result = "";
    	try {
    		String url = SystemLotteryInterfaceUrlInfo.getUrlByLotteryKind(lotteryKind, interfaceType);
    		if(StringUtils.isEmpty(url)) {
    			return AjaxUtil.createReturnValueError("获取该彩种接口配置url为空");
    		}
			result = URLConnectUtil.getStringFromURL(url);
			if(StringUtils.isEmpty(result)){
				return AjaxUtil.createReturnValueError("查询接口数据返回为空字符串.");
			}
		} catch (Exception e) {
			log.error("查询接口数据出现IO异常", e);
			return AjaxUtil.createReturnValueError("查询接口数据出现IO异常");
		}
		return AjaxUtil.createReturnValueSuccess(result);
    }
}
