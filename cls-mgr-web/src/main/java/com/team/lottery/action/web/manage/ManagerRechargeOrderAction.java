package com.team.lottery.action.web.manage;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.directwebremoting.io.FileTransfer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.enums.EAdminOperateLogModel;
import com.team.lottery.enums.EFundOperateType;
import com.team.lottery.enums.EFundRechargeStatus;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.RechargeWithDrawInfoExportVO;
import com.team.lottery.extvo.RechargeWithDrawOrderQuery;
import com.team.lottery.service.AdminOperateLogService;
import com.team.lottery.service.RechargeOrderService;
import com.team.lottery.service.UserService;
import com.team.lottery.util.AdminOperateUtil;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.ExcelUtil;
import com.team.lottery.util.ExcelUtil.ExcelExportData;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.User;

/**
 * 后台充值订单模块相关Action方法.
 * 
 * @author Jamine
 *
 */
@Controller("managerRechargeOrderAction")
public class ManagerRechargeOrderAction {
	private static Logger logger = LoggerFactory.getLogger(ManagerRechargeOrderAction.class);
	@Autowired
	private RechargeOrderService rechargeOrderService;
	@Autowired
	private AdminOperateLogService adminOperateLogService;
	@Autowired
	private UserService userService;

	/**
	 * 查找符合条件的充值记录(网银充值)
	 * 
	 * @return
	 */
	public Object[] getAllRechargeOrdersByQuery(RechargeWithDrawOrderQuery query, Page page) {
		if (query == null || page == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		Admin admin = BaseDwrUtil.getCurrentAdmin();
		query.setOperateType(EFundOperateType.RECHARGE.name());
		if (!"SUPER_SYSTEM".equals(admin.getBizSystem())) {
			query.setBizSystem(admin.getBizSystem());
		}
		// 查询列表
		page = rechargeOrderService.getRechargeOrdersByQueryPage(query, page);

		return AjaxUtil.createReturnValueSuccess(page);
	}

	/**
	 * 导出用户充值相关信息
	 * 
	 * @param userName
	 * @param request
	 * @param response
	 * @return
	 */
	public FileTransfer rechargeOrderInfoExport(RechargeWithDrawOrderQuery query) {

		try {
			// 创建文件输出流对象.
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			// 创建订单List对象.
			List<RechargeOrder> orderList = new ArrayList<RechargeOrder>();
			// 通过条件查询出RechargeOrder对象.
			orderList = rechargeOrderService.getRechargeOrders(query);
			String fileName = "";
			fileName = "D:\\" + "CZDD" + DateUtil.getDate(new Date(), "yyyy-MM-dd") + ".xls";
			RechargeWithDrawInfoExportVO rechargeWithDrawInfoExportVO = new RechargeWithDrawInfoExportVO();
			rechargeWithDrawInfoExportVO.setSerialNumber(true);
			rechargeWithDrawInfoExportVO.setUserName(true);
			rechargeWithDrawInfoExportVO.setApplyValue(true);
			rechargeWithDrawInfoExportVO.setAccountValue(true);
			rechargeWithDrawInfoExportVO.setCreatedDateStr(true);
			rechargeWithDrawInfoExportVO.setStatusStr(true);
			rechargeWithDrawInfoExportVO.setOperateDes(true);
			rechargeWithDrawInfoExportVO.setRefTypeStr(true);
			rechargeWithDrawInfoExportVO.setFromType(true);
			rechargeWithDrawInfoExportVO.setPostscript(true);
			rechargeWithDrawInfoExportVO.setUpdateAdmin(true);
			rechargeWithDrawInfoExportVO.setUpdateDateStr(true);
			rechargeWithDrawInfoExportVO.setBankCardNum(true);
			rechargeWithDrawInfoExportVO.setBankName(true);
			rechargeWithDrawInfoExportVO.setBankAccountName(true);
			rechargeWithDrawInfoExportVO.setSubbranchName(true);
			String str = "CZDD";
			Admin admin = BaseDwrUtil.getCurrentAdmin();
			ExcelExportData excelExportData = this.constructRechargeInfoExcelData(admin.getBizSystem(), str, orderList,
					rechargeWithDrawInfoExportVO);
			fileName = new String(fileName.getBytes("GBK"), "iso8859-1");
			bos = ExcelUtil.export2Stream(excelExportData);
			return new FileTransfer(fileName, "application/msexcel", bos.toByteArray());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	/**
	 * 确认到账
	 * 
	 * @param rechargeId
	 * @param presentStr
	 * @return
	 */
	public Object[] setRechargerOrdersPaySuccess(Long rechargeId, String rechargePresent, String bettingDemand, String rechargeGiftScale,
			int operate) {
		// 判断参数是否为空.
		if (rechargeId == null || StringUtils.isEmpty(rechargePresent)) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		// 获取当前管理员等级,通过管理员等级来判断当前管理员是否可以执行当前操作.
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		Integer state = currentAdmin.getState();
		if (state == 3) {
			return AjaxUtil.createReturnValueError("客服管理员不能操作此功能！");
		}
		try {
			RechargeOrder rechargeOrderOld = rechargeOrderService.selectByPrimaryKey(rechargeId);
			if (rechargeOrderOld == null) {
				throw new Exception("该订单不存在.");
			}
			if (rechargeOrderOld.getDealStatus().equals(EFundRechargeStatus.PAYMENT_SUCCESS.name())) {
				throw new Exception("该订单已经支付成功,不能重新确认收账.");
			}
			User orderUser = userService.selectByPrimaryKey(rechargeOrderOld.getUserId());

			if (orderUser == null) {
				throw new Exception("当前用户不存在.");
			}
			
			if (operate == 1) {
				String operateDesc =AdminOperateUtil.constructMessage("rechargeOrder_confirm", currentAdmin.getUsername(), rechargeOrderOld.getUserName(), rechargeOrderOld.getSerialNumber());
//				String operateDesc = "管理员[" + currentAdmin.getUsername() + "], 把会员[" + rechargeOrderOld.getUserName() + "]的订单号为["
//						+ rechargeOrderOld.getSerialNumber() + "]进行确认订单操作!";
				adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), BaseDwrUtil.getRequestIp(), currentAdmin.getId(),
						currentAdmin.getUsername(), rechargeOrderOld.getUserId(), rechargeOrderOld.getUserName(), EAdminOperateLogModel.RECHARGE_OPERATE, operateDesc);
			} else if (operate == 2) {
				String operateDesc =AdminOperateUtil.constructMessage("rechargeOrder_force", currentAdmin.getUsername(), rechargeOrderOld.getUserName(), rechargeOrderOld.getSerialNumber());
//				String operateDesc = "管理员[" + currentAdmin.getUsername() + "], 把会员[" + rechargeOrderOld.getUserName() + "]的订单号为["
//						+ rechargeOrderOld.getSerialNumber() + "]进行强制入账操作!";
				adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), BaseDwrUtil.getRequestIp(), currentAdmin.getId(),
						currentAdmin.getUsername(), rechargeOrderOld.getUserId(), rechargeOrderOld.getUserName(), EAdminOperateLogModel.RECHARGE_OPERATE, operateDesc);
			}
			RechargeOrder rechargeOrder = rechargeOrderService.setRechargerOrdersPaySuccess(rechargeOrderOld, rechargePresent,
					bettingDemand, rechargeGiftScale, orderUser,currentAdmin);
			return AjaxUtil.createReturnValueSuccess(rechargeOrder);
		} catch (UnEqualVersionException e) {
			logger.error("确认到账失败", e);
			return AjaxUtil.createReturnValueError("当前系统繁忙，请稍后重试");
		} catch (Exception e) {
			logger.error("确认到账异常", e);
			return AjaxUtil.createReturnValueError("确认到账失败,请重新刷新列表");
		}
	}

	/**
	 * 订单关闭(通过订单ID去处理订单)
	 * 
	 * @param rechargeId
	 * @return
	 */
	public Object[] setRechargerOrdersPayClose(Long rechargeId) {
		// 判断参数是否为空.
		if (rechargeId == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		// 获取当前Admin的权限等级,如果等级等于三,那么就不能执行该操作.
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		Integer state = currentAdmin.getState();
		if (state == 3) {
			return AjaxUtil.createReturnValueError("客服管理员不能操作此功能！");
		}
		// 根据ID查询出对应的充值订单.已经到账成功的订单不能关闭订单.
		RechargeOrder rechargeOrderOld = rechargeOrderService.selectByPrimaryKey(rechargeId);
		if (rechargeOrderOld == null) {
			return AjaxUtil.createReturnValueError("操作异常,根据ID查询出来的订单为空!");
		}
		// 获取订单的支付状态.
		String dealStatus = rechargeOrderOld.getDealStatus();
		if (EFundRechargeStatus.PAYMENT_SUCCESS.getCode().equals(dealStatus)) {
			return AjaxUtil.createReturnValueError("当前订单已经到账成功,不能关闭,请刷新页面!");
		}
		try {
			
			
			
			if (rechargeOrderOld.getDealStatus().equals(EFundRechargeStatus.PAYMENT_CLOSE.name())) {
				throw new Exception("该订单已经关闭,不能再次关闭.");
			}
			String operateDesc = AdminOperateUtil.constructMessage("rechargeOrder_close", currentAdmin.getUsername(), rechargeOrderOld.getUserName(), rechargeOrderOld.getSerialNumber());
//			String operateDesc = "管理员[" + currentAdmin.getUsername() + "], 把会员[" + rechargeOrderOld.getUserName() + "]的订单号为["
//					+ rechargeOrderOld.getSerialNumber() + "]进行订单关闭操作!";
			adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), BaseDwrUtil.getRequestIp(), currentAdmin.getId(),
					currentAdmin.getUsername(), rechargeOrderOld.getUserId(), rechargeOrderOld.getUserName(), EAdminOperateLogModel.RECHARGE_OPERATE, operateDesc);
			// 请求应该使用事务处理,涉及到多张表.
			RechargeOrder rechargeOrder = rechargeOrderService.setRechargerOrdersPayClose(rechargeOrderOld,currentAdmin);
			
			return AjaxUtil.createReturnValueSuccess(rechargeOrder);
		} catch (Exception e) {
			logger.error("订单关闭异常", e);
			return AjaxUtil.createReturnValueError("订单关闭失败,请重新刷新列表");
		}
	}

	/**
	 * 根据充值订单设定打码量
	 * 
	 * @param rechargeId
	 * @param bettingDemand
	 * @return
	 */
	public Object[] setUserBettingDemand(Long rechargeId, String bettingDemand) {
		// 判断前端参数是否正确.
		if (rechargeId == null || StringUtils.isEmpty(bettingDemand)) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		// 获取当前Admin,并且判断是否为客服,如果是的话就不能执行该操作.
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		Integer state = currentAdmin.getState();
		if (state == 3) {
			return AjaxUtil.createReturnValueError("客服管理员不能操作此功能！");
		}
		try {
			rechargeOrderService.setUserBettingDemand(rechargeId, bettingDemand);
			return AjaxUtil.createReturnValueSuccess();
		} catch (Exception e) {
			return AjaxUtil.createReturnValueError(e.getMessage());
		}

	}

	/**
	 * 查找符合条件的充值记录(快捷充值)
	 * 
	 * @return
	 */
	public Object[] getAllRechargeOrdersByQueryForReport(RechargeWithDrawOrderQuery query, Page page) {
		if (query == null || page == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		Admin admin = BaseDwrUtil.getCurrentAdmin();
		String bizSysterm = query.getBizSystem();
		if (bizSysterm == null && !"SUPER_SYSTEM".equals(admin.getBizSystem())) {
			query.setBizSystem(admin.getBizSystem());
		}
		query.setAdminId(null);

		// 查询列表
		page = rechargeOrderService.getRechargeOrdersByQueryPage(query, page);
		RechargeOrder totalRechargeOrder = rechargeOrderService.getRechargeOrdersByTotal(query);
		return AjaxUtil.createReturnValueSuccess(page, totalRechargeOrder);
	}

	/**
	 * 取充值提现订单最新的订单号(新方法)
	 * 
	 * @return
	 */
	public Object[] getUptodateSerialNumber(RechargeWithDrawOrderQuery query) {
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		if (!"SUPER_SYSTEM".equals(currentAdmin.getBizSystem())) {
			query.setBizSystem(currentAdmin.getBizSystem());
		}
		String serialNumber = rechargeOrderService.getUptodateSerialNumber(query);
		return AjaxUtil.createReturnValueSuccess(serialNumber);

	}

	/**
	 * 根据用户名构建表格数据
	 * 
	 * @param users
	 * @return
	 */
	private ExcelExportData constructRechargeInfoExcelData(String bizSystem, String fileName, List<RechargeOrder> orderList,
			RechargeWithDrawInfoExportVO rechargeWithDrawInfoExportVO) {
		List<String[]> columNames = new ArrayList<String[]>();
		List<String[]> fieldNames = new ArrayList<String[]>();

		List<String> sheetColumNames = new ArrayList<String>();
		List<String> sheetFieldNames = new ArrayList<String>();
		if (bizSystem.equals("SUPER_SYSTEM")) {
			sheetColumNames.add("业务系统");
			sheetFieldNames.add("bizSystemName");
		}
		if (rechargeWithDrawInfoExportVO.getSerialNumber()) {
			sheetColumNames.add("订单号");
			sheetFieldNames.add("serialNumber");
		}
		if (rechargeWithDrawInfoExportVO.getUserName()) {
			sheetColumNames.add("用户名");
			sheetFieldNames.add("userName");
		}
		if (rechargeWithDrawInfoExportVO.getApplyValue()) {
			sheetColumNames.add("充值金额");
			sheetFieldNames.add("applyValue");
		}
		if (rechargeWithDrawInfoExportVO.getAccountValue()) {
			sheetColumNames.add("到账金额");
			sheetFieldNames.add("accountValue");
		}
		if (rechargeWithDrawInfoExportVO.getCreatedDateStr()) {
			sheetColumNames.add("充值时间");
			sheetFieldNames.add("createdDateStr");
		}
		if (rechargeWithDrawInfoExportVO.getStatusStr()) {
			sheetColumNames.add("状态");
			sheetFieldNames.add("statusStr");
		}
		if (rechargeWithDrawInfoExportVO.getOperateDes()) {
			sheetColumNames.add("充值方式");
			sheetFieldNames.add("operateDes");
		}
		if (rechargeWithDrawInfoExportVO.getRefTypeStr()) {
			sheetColumNames.add("订单类型");
			sheetFieldNames.add("refTypeStr");
		}

		if (rechargeWithDrawInfoExportVO.getFromType()) {
			sheetColumNames.add("订单来源");
			sheetFieldNames.add("fromType");
		}
		if (rechargeWithDrawInfoExportVO.getPostscript()) {
			sheetColumNames.add("附言");
			sheetFieldNames.add("postscript");
		}
		if (rechargeWithDrawInfoExportVO.getBankAccountName()) {
			sheetColumNames.add("收款姓名");
			sheetFieldNames.add("bankAccountName");
		}
		if (rechargeWithDrawInfoExportVO.getBankName()) {
			sheetColumNames.add("银行名称");
			sheetFieldNames.add("bankName");
		}
		if (rechargeWithDrawInfoExportVO.getSubbranchName()) {
			sheetColumNames.add("支行名称");
			sheetFieldNames.add("subbranchName");
		}
		if (rechargeWithDrawInfoExportVO.getBankCardNum()) {
			sheetColumNames.add("收款卡号");
			sheetFieldNames.add("bankCardNum");
		}

		if (rechargeWithDrawInfoExportVO.getUpdateAdmin()) {
			sheetColumNames.add("操作人");
			sheetFieldNames.add("updateAdmin");
		}
		if (rechargeWithDrawInfoExportVO.getUpdateDateStr()) {
			sheetColumNames.add("操作时间");
			sheetFieldNames.add("updateDateStr");
		}
		columNames.add(sheetColumNames.toArray(new String[sheetColumNames.size()]));
		fieldNames.add(sheetFieldNames.toArray(new String[sheetFieldNames.size()]));

		// 对user资料，为空设置为无
		if (CollectionUtils.isNotEmpty(orderList)) {
			/*
			 * for(Order u : orderList) {
			 * if(StringUtils.isEmpty(u.getTrueName())) { u.setTrueName("无"); }
			 * if(StringUtils.isEmpty(u.getIdentityid())) {
			 * u.setIdentityid("无"); } if(StringUtils.isEmpty(u.getPhone())) {
			 * u.setPhone("无"); } if(StringUtils.isEmpty(u.getQq())) {
			 * u.setQq("无"); } if(StringUtils.isEmpty(u.getEmail())) {
			 * u.setEmail("无"); } String address = "";
			 * if(StringUtils.isEmpty(u.getProvince()) &&
			 * StringUtils.isEmpty(u.getCity())
			 * &&StringUtils.isEmpty(u.getAddress())) { address = "无"; } else {
			 * if(!StringUtils.isEmpty(u.getProvince())) { address +=
			 * u.getProvince(); } if(!StringUtils.isEmpty(u.getCity())) {
			 * address += u.getCity(); }
			 * if(!StringUtils.isEmpty(u.getAddress())) { address +=
			 * u.getAddress(); } } u.setAddress(address); }
			 */
		}
		LinkedHashMap<String, List<?>> map = new LinkedHashMap<String, List<?>>();
		map.put(fileName, orderList);

		ExcelExportData setInfo = new ExcelExportData();
		setInfo.setDataMap(map);
		setInfo.setFieldNames(fieldNames);
		setInfo.setTitles(new String[] {});
		setInfo.setColumnNames(columNames);

		// 将需要导出的数据输出到文件
		return setInfo;
	}
}
