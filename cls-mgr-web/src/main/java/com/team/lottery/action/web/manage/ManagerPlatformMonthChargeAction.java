package com.team.lottery.action.web.manage;

import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.Page;
import com.team.lottery.service.PlatformMonthChargeService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.PlatformChargeOrder;
import com.team.lottery.vo.PlatformMonthCharge;

@Controller("managerPlatformMonthChargeAction")
public class ManagerPlatformMonthChargeAction {

	@Autowired
	private PlatformMonthChargeService platformMonthChargeService;
	
	public Object[] queryPlatformMonthChargePage(PlatformChargeOrder query,Page page) {
		if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
 
        Admin admin=BaseDwrUtil.getCurrentAdmin();
        if(!"SUPER_SYSTEM".equals(admin.getBizSystem())){
        	return AjaxUtil.createReturnValueError("当前用户无权限查看");
        }
        
        //当前日期上个月最后一天
        if(query.getBelongDateStart()!=null){
        	Date belongDateStart=query.getBelongDateStart();
        	Calendar calendar = Calendar.getInstance();
        	calendar.setTime(belongDateStart);
        	calendar.set(Calendar.DAY_OF_MONTH, 1);
        	calendar.add(Calendar.MILLISECOND, -1);
        	query.setBelongDateStart(calendar.getTime());
        }
        
        //当前日期下个月的第一天
        if(query.getBelongDateEnd()!=null){
        	Date belongDateEnd=query.getBelongDateEnd();
        	Calendar calendar = Calendar.getInstance();
        	calendar.setTime(belongDateEnd);
        	calendar.add(Calendar.MONTH, 1);
        	calendar.set(Calendar.DAY_OF_MONTH, 1);
        	query.setBelongDateEnd(calendar.getTime());
        }
        
    	page = platformMonthChargeService.queryPlatformMonthChargePage(query,page);
    	PlatformMonthCharge sumMonthCharge = platformMonthChargeService.queryPlatformMonthChargeSum(query);
		return AjaxUtil.createReturnValueSuccess(page, sumMonthCharge);
	}
}
