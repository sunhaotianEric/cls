package com.team.lottery.action.web.manage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.Page;
import com.team.lottery.service.ChatRoomConfigService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.vo.ChatroomConfig;

@Controller("manageChatRoomConfigAction")
public class ManageChatRoomConfigAction {

	@Autowired
	public ChatRoomConfigService chatRoomConfigService;
	
	/**
	 * 分页查询数据
	 * @param chatroomConfig
	 * @param page
	 * @return
	 */
	public Object[] getAllChatRoomConfig(ChatroomConfig chatroomConfig,Page page){
		if(page==null){
			return AjaxUtil.createReturnValueError("参数传递错误");
		}
		page=chatRoomConfigService.getAllChatRoomConfig(chatroomConfig, page);
		return AjaxUtil.createReturnValueSuccess(page);
	}
	
	/**
	 * 根据ID查询单个数据库配置信息
	 * @param id
	 * @return
	 */
	public Object[] getChatRoomConfigById(int id){
		ChatroomConfig chatroomConfig=chatRoomConfigService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(chatroomConfig);
	}
	
	/**
	 * 添加或者修改
	 * @param chatroomConfig
	 * @return
	 */
	public Object[] saveOrUpdateChatRoomConfig(ChatroomConfig chatroomConfig){
		if(chatroomConfig==null){
			return AjaxUtil.createReturnValueError("参数传递错误");
		}
		if(chatroomConfig.getId()>0){
			ChatroomConfig list=chatRoomConfigService.getChatRoomConfig(chatroomConfig);
			if(list!=null){
				return AjaxUtil.createReturnValueError("该业务系统数据库配置已存在");
			}
			chatRoomConfigService.updateByPrimaryKeySelective(chatroomConfig);
		}else{
			ChatroomConfig list=chatRoomConfigService.getChatRoomConfig(chatroomConfig);
			if(list!=null){
				return AjaxUtil.createReturnValueError("该业务系统数据库配置已存在");
			}
			chatRoomConfigService.insertSelective(chatroomConfig);
		}
		return AjaxUtil.createReturnValueSuccess();
	}
	
	/**
	 * 根据ID删除数据库配置
	 * @param id
	 * @return
	 */
	public Object[] delChatRoomConfigById(int id){
		chatRoomConfigService.delChatRoomConfigById(id);
		return AjaxUtil.createReturnValueSuccess();
	}
}
