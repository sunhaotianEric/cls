package com.team.lottery.action.web.manage;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.enums.EAdminOperateLogModel;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.AdminOperateLogService;
import com.team.lottery.service.DayConsumeActityConfigService;
import com.team.lottery.util.AdminOperateUtil;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.DayConsumeActityConfig;

@Controller("manageDayConsumeActivityConfigAction")
public class ManageDayConsumeActivityConfigAction {

	@Autowired
	private DayConsumeActityConfigService dayconsumeactityconfigService;
	@Autowired
	private AdminOperateLogService adminOperateLogService;

	/**
	 * 查找所有的 配置
	 * 
	 * @return
	 */
	public Object[] getDayConsumeActityConfigsPage(DayConsumeActityConfig query, Page page) {
		if (query == null || page == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}

		String bizSystem = BaseDwrUtil.getCurrentAdmin().getBizSystem();
		if (!"SUPER_SYSTEM".equals(bizSystem)) {
			query.setBizSystem(bizSystem);
		}
		page = dayconsumeactityconfigService.getDayConsumeActityConfigsPage(query, page);

		return AjaxUtil.createReturnValueSuccess(page);
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Object[] getDayConsumeActityConfigById(Long id) {
		DayConsumeActityConfig dayConsumeActityConfig = dayconsumeactityconfigService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(dayConsumeActityConfig);
	}

	/**
	 * 
	 * @param dayConsumeActityConfig
	 * @return
	 */
	public Object[] saveDayConsumeActityConfig(DayConsumeActityConfig dayConsumeActityConfig) {
		if (dayConsumeActityConfig == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		Long id = dayConsumeActityConfig.getId();
		String operationIp = BaseDwrUtil.getRequestIp();
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		if (!currentAdmin.getBizSystem().equals("SUPER_SYSTEM")) {
			dayConsumeActityConfig.setBizSystem(currentAdmin.getBizSystem());
		}
		if (id == null) {
			dayConsumeActityConfig.setCreateDate(new Date());
			dayConsumeActityConfig.setCreateAdmin(currentAdmin.getUsername());
			dayconsumeactityconfigService.insertSelective(dayConsumeActityConfig);
			String operateDesc = AdminOperateUtil.constructMessage("day_consume_activity_config_add", currentAdmin.getUsername());

			adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(),
					currentAdmin.getUsername(), null, "", EAdminOperateLogModel.RECHARGECONFIG_MANAGE, operateDesc);
		} else {
			dayConsumeActityConfig.setUpdateDate(new Date());
			dayConsumeActityConfig.setUpdateAdmin(currentAdmin.getUsername());
			dayconsumeactityconfigService.updateByPrimaryKeySelective(dayConsumeActityConfig);
			String operateDesc = AdminOperateUtil.constructMessage("day_consume_activity_config_edit", currentAdmin.getUsername());

			adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(),
					currentAdmin.getUsername(), null, "", EAdminOperateLogModel.RECHARGECONFIG_MANAGE, operateDesc);
		}
		return AjaxUtil.createReturnValueSuccess();
	}

	/**
	 * 
	 * @param Long
	 * @return
	 */
	public Object[] delDayConsumeActityConfig(Long id) {
		if (id == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		String operationIp = BaseDwrUtil.getRequestIp();
		dayconsumeactityconfigService.deleteByPrimaryKey(id);
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		String operateDesc = AdminOperateUtil.constructMessage("day_consume_activity_config_del", currentAdmin.getUsername());
		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(),
				currentAdmin.getUsername(), null, "", EAdminOperateLogModel.RECHARGECONFIG_MANAGE, operateDesc);
		return AjaxUtil.createReturnValueSuccess();
	}
}
