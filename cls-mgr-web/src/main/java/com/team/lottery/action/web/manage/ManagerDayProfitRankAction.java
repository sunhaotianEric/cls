package com.team.lottery.action.web.manage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.DayProfitRankQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.DayProfitRankService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.vo.DayProfitRank;

@Controller("managerDayProfitRankAction")
public class ManagerDayProfitRankAction {

	@Autowired
	private DayProfitRankService dayProfitRankService;
	
	 /**
     * 查找所有的系统数据
     * @return
     */
    public Object[] getAllDayProfitRanks(DayProfitRankQuery query,Page page){
    	if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	String currentSystem = BaseDwrUtil.getCurrentAdmin().getBizSystem();
    	if(!currentSystem.equals("SUPER_SYSTEM")){
    		query.setBizSystem(currentSystem);
    	}
    	page = dayProfitRankService.getAllDayProfitRankByQueryPage(query,page);
		return AjaxUtil.createReturnValueSuccess(page);
    }
    
	public Object[] getDayProfitRankById(Integer id) {
		if(id==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		DayProfitRank record = dayProfitRankService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(record);
	}
    
	/**
	 * 修改
	 * @param record
	 * @return
	 */
	public Object[] updateByPrimaryKeySelective(DayProfitRank record){
		if(record==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		dayProfitRankService.updateByPrimaryKeySelective(record);
		DayProfitRank dayProfitRank = dayProfitRankService.selectByPrimaryKey(record.getId());
		dayProfitRankService.resetBonusList(dayProfitRank.getBizSystem(), dayProfitRank.getBelongDate());
		return AjaxUtil.createReturnValueSuccess();
	}
    
}
