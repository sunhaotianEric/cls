package com.team.lottery.action.web.manage;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.Page;
import com.team.lottery.service.CourseService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.vo.Course;

@Controller("managerCourseAction")
public class ManagerCourseAction {
	
	@Autowired
	private CourseService courseService;

	/**
	 * 分页查询
	 * @param query
	 * @param page
	 * @return
	 */
	public Object[] getAllCourse(Course query,Page page){
		if(query==null||page==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		page=courseService.getAllCourse(query, page);
		return AjaxUtil.createReturnValueSuccess(page);
	}
	
	/**
	 * 根据ID查询
	 * @param id
	 * @return
	 */
	public Object[] selectCourse(Integer id){
		Course record=courseService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(record);
	}
	
	/**
	 * 修改
	 * @param record
	 * @return
	 */
/*	public Object[] updateFeedBack(FeedBack record){
		if(record==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		feedbackService.updateByPrimaryKeySelective(record);
		return AjaxUtil.createReturnValueSuccess();
	}*/
	
	/**
	 * 根据ID删除
	 * @param id
	 * @return
	 */
	public Object[] delCourseById(Integer id){
		if(id==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		courseService.deleteByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess();
	}
	
	/**
	 * 保存
	 * @param record
	 * @return
	 */
	public Object[] saveCourse(Course record){
    	if(record == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	Integer id = record.getId();
    	if(id == null){
    		record.setCreateDate(new Date());
    		record.setCreateAdmin(BaseDwrUtil.getCurrentAdmin().getUsername());
    		courseService.insertSelective(record);
    	}else{
    		record.setUpdateDate(new Date());
    		record.setUpdateAdmin(BaseDwrUtil.getCurrentAdmin().getUsername());
    		courseService.updateByPrimaryKeySelective(record);
    	}
    	return AjaxUtil.createReturnValueSuccess();
    }
	
	/**
     * 启用停用
     */
	public Object[] enableCourse(Integer id,Integer flag) {
		if (id == null||flag==null) {
			return AjaxUtil.createReturnValueError("参数传递错误");
		}
		Course course = new Course();
		course.setId(id);
		course.setEnable(flag);
		int result = courseService.updateByPrimaryKeySelective(course);
		if (result > 0) {
			return AjaxUtil.createReturnValueSuccess();
		} else {
			return AjaxUtil.createReturnValueError("添加失败，请重新添加");
		}
	}
	
}
