package com.team.lottery.action.web.manage;

import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.ChatLevelAuthService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.ChatLevelAuth;

@Controller("managerChatLevelAuthAction")
public class ManagerChatLevelAuthAction {
	
	/*private static Logger logger = LoggerFactory.getLogger(ManagerChatLevelAuthAction.class);*/

	@Autowired
	private ChatLevelAuthService chatLevelAuthService;
	
	/**
     * 查询
     * @return
     */
    public Object[] getAllChatLevelAuth(ChatLevelAuth query,Page page){
    	if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	if(!ConstantUtil.SUPER_SYSTEM.equals(currentAdmin.getBizSystem())){
    		query.setBizSystem(currentAdmin.getBizSystem());
    	}
    	page=chatLevelAuthService.getAllChatLevelAuthQueryPage(query, page);
		return AjaxUtil.createReturnValueSuccess(page);
    }

    /**
     * 添加修改
     * @return
     */
    public Object[] saveOrUpdateChatLevelAuth(ChatLevelAuth record){
    	try{
    		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    		if(!"SUPER_SYSTEM".equals(currentAdmin.getBizSystem())){
    			record.setBizSystem(currentAdmin.getBizSystem());
    		}
    		if(record.getId()!=null){
    			//保存操作日志
    			record.setUpdateDate(new Date());
    			chatLevelAuthService.updateByPrimaryKeySelective(record);
    		}else{
    			//保存操作日志
    			record.setCreateDate(new Date());
    			chatLevelAuthService.insertSelective(record);
    		}
    	}catch(Exception e){
        	return AjaxUtil.createReturnValueError(e.getMessage());
    	}
    	
    	return AjaxUtil.createReturnValueSuccess();
    }
    
    public Object[] getChatLevelAuthById(Integer id){
    	
     ChatLevelAuth chatLevelAuth=chatLevelAuthService.selectByPrimaryKey(Long.valueOf(String.valueOf(id)));
    		
    	
    	
    	return AjaxUtil.createReturnValueSuccess(chatLevelAuth);
    }

    
    
    
    

	
}
