package com.team.lottery.action.web.manage;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.enums.EAdminOperateLogModel;
import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.enums.ERedisChannel;
import com.team.lottery.service.AdminOperateLogService;
import com.team.lottery.service.LotterySwitchService;
import com.team.lottery.system.message.MessageQueueCenter;
import com.team.lottery.system.message.messagetype.LotterySwitchMessage;
import com.team.lottery.util.AdminOperateUtil;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.LotterySwitch;

/**
 * 新彩种开关.相关方法.
 * 
 * @author jamine
 *
 */

@Controller("managerLotterySwitchAction")
public class ManagerLotterySwitchAction {

	@Autowired
	private LotterySwitchService lotterySwitchService;
	@Autowired
	private AdminOperateLogService adminOperateLogService;

	/**
	 * 根据业务系统获取彩种开关.
	 * 
	 * @param bizSystem 业务系统.
	 * @return
	 */
	public Object[] getAllLotterySwitchsByBizSystem(String bizSystem) {

		// 查询参数不能为空.
		if (StringUtils.isEmpty(bizSystem)) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}

		// 校验查询是否符合权限.
		String currentBizSystem = BaseDwrUtil.getCurrentAdmin().getBizSystem();
		// 如果当前系统不是超级系统,并且传入的参数为超级系统,提示用户错误.
		if (!currentBizSystem.equals("SUPER_SYSTEM") && bizSystem.equals("SUPER_SYSTEM")) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}

		// 根据系统查询对应的彩种开关.
		List<LotterySwitch> allLotterySwitchsByBizSystem = lotterySwitchService.getAllLotterySwitchsByBizSystem(bizSystem);
		return AjaxUtil.createReturnValueSuccess(allLotterySwitchsByBizSystem);
	}

	/**
	 * 根据系统和彩种,更新彩种开关.
	 * 
	 * @return
	 */
	public Object[] updateLotterySwitchsByBizSystem(List<LotterySwitch> lotterySwitchs, String bizSystem) {
		
		// 校验参数是否合格,
		if (StringUtils.isEmpty(bizSystem) || CollectionUtils.isEmpty(lotterySwitchs)) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		// 获取当前登录管理员对象.判断是否登录
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		if (currentAdmin == null) {
			return AjaxUtil.createReturnValueError("请刷新页面重新登录.");
		}
		
		// 校验查询是否符合权限.如果当前系统不是超级系统,并且传入的参数为超级系统,提示用户错误.
		String currentBizSystem = currentAdmin.getBizSystem();
		if (!currentBizSystem.equals("SUPER_SYSTEM") && bizSystem.equals("SUPER_SYSTEM")) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		
		// 根据系统查询出全部的彩种开关数据.
		List<LotterySwitch> allLotterySwitchsByBizSystem = lotterySwitchService.getAllLotterySwitchsByBizSystem(bizSystem);
		
		// 新建List用于存储要更新的数据.
		List<LotterySwitch> updateLotterySwitchs = new ArrayList<LotterySwitch>();
		for (LotterySwitch lotterySwitch : allLotterySwitchsByBizSystem) {
			for (LotterySwitch updatelotterySwitch : lotterySwitchs) {
				if (lotterySwitch.getLotteryType().equals(updatelotterySwitch.getLotteryType())) {
					// 判断状态是否修改,已经修改才进行更新.
					if(!lotterySwitch.getLotteryStatus().equals(updatelotterySwitch.getLotteryStatus())||lotterySwitch.getClosingTime()!=updatelotterySwitch.getClosingTime()) {
						// 保存操作日志.
			    		String operateDesc = AdminOperateUtil.constructMessage("lottery_switch_manage_edit", currentAdmin.getUsername(),bizSystem,updatelotterySwitch.getLotteryType(),String.valueOf(updatelotterySwitch.getLotteryStatus()));
			    		String operationIp = BaseDwrUtil.getRequestIp();
			    		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.LOTTERY_SWITCH, operateDesc);
			    		// 更新彩种状态.
			    		lotterySwitch.setLotteryStatus(updatelotterySwitch.getLotteryStatus());
			    		lotterySwitch.setUpdateDate(new Date());
			    		lotterySwitch.setUpdateAdmin(currentAdmin.getUsername());
						lotterySwitch.setClosingTime(updatelotterySwitch.getClosingTime());
			    		updateLotterySwitchs.add(lotterySwitch);
			    		// 更新成功,删除原有对象
			    		lotterySwitchs.remove(updatelotterySwitch);
			    		break;
					}else {
						//没有修改,直接跳过当前循环.
			    		lotterySwitchs.remove(updatelotterySwitch);
			    		break;
					}
				}
	    		
			}
		}
		// 新增List用于存储要新增的数据.
		List<LotterySwitch> newLotterySwitchs = new ArrayList<LotterySwitch>();
		// 如果没有数据,就添加数据.
		if (lotterySwitchs.size() > 0) {
			LotterySwitch newLotterySwitch = null;
			for (LotterySwitch lotterySwitch : lotterySwitchs) {
				newLotterySwitch = new LotterySwitch();
				newLotterySwitch.setBizSystem(bizSystem);
				newLotterySwitch.setCreateDate(new Date());
				// 没有选择状态默认为0.
				if (lotterySwitch.getLotteryStatus() == null) {
					newLotterySwitch.setLotteryStatus(0);
				}else {
					newLotterySwitch.setLotteryStatus(lotterySwitch.getLotteryStatus());
				}
				newLotterySwitch.setLotteryType(lotterySwitch.getLotteryType());
				String lotteryType = lotterySwitch.getLotteryType();
				newLotterySwitch.setLotteryTypeDes(ELotteryKind.valueOf(lotteryType).getDescription());
				newLotterySwitch.setUpdateAdmin(BaseDwrUtil.getCurrentAdmin().getUsername());
				newLotterySwitch.setClosingTime(lotterySwitch.getClosingTime());
				newLotterySwitchs.add(newLotterySwitch);
				// 保存操作日志.
	    		String operateDesc = AdminOperateUtil.constructMessage("lottery_switch_manage_add", currentAdmin.getUsername(),bizSystem,newLotterySwitch.getLotteryType(),String.valueOf(newLotterySwitch.getLotteryStatus()));
	    		String operationIp = BaseDwrUtil.getRequestIp();
	    		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.LOTTERY_SWITCH, operateDesc);
			}
			
		}
		// 当需要修改的数据或者新增的数据大小不为空的时候.
		if (updateLotterySwitchs.size() > 0 || newLotterySwitchs.size() > 0) {
			lotterySwitchService.updateLotterySwitchsByBizSystem(updateLotterySwitchs,newLotterySwitchs);
			// 有修改或者新增对的时候,通知Redis刷新缓存.
			this.refreshCache(bizSystem);
		}
		return AjaxUtil.createReturnValueSuccess();
	}
	
	/**
	 * 根据业务系统刷新彩种开关缓存.
	 * @param bizSystem
	 * @return
	 */
    private Object[] refreshCache(String bizSystem){
    	//通知系统刷新缓存
		try {
			if(bizSystem == null || "".equals(bizSystem)){
				AjaxUtil.createReturnValueError("请选择要刷新的业务系统");
			}
			LotterySwitchMessage lotterySwitchMessage = new LotterySwitchMessage();
			lotterySwitchMessage.setType(ERedisChannel.CLSCACHE.name());
			lotterySwitchMessage.setBizSystem(bizSystem);
			MessageQueueCenter.putRedisMessage(lotterySwitchMessage,ERedisChannel.CLSCACHE.name());

		} catch (Exception e) {
			AjaxUtil.createReturnValueError(e.getMessage());
		}
	 return AjaxUtil.createReturnValueSuccess();
    }

}
