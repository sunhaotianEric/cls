package com.team.lottery.action.web.manage;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.enums.EAdminOperateLogModel;
import com.team.lottery.redis.JedisUtils;
import com.team.lottery.service.AdminOperateLogService;
import com.team.lottery.service.LotteryControlConfiService;
import com.team.lottery.service.SystemConfigService;
import com.team.lottery.util.AdminOperateUtil;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.LotteryControlConfig;
import com.team.lottery.vo.SystemConfig;

import redis.clients.jedis.Jedis;

@Controller("managerLotteryControlConfigAction")
public class ManagerLotteryControlConfigAction {

	private static Logger log=LoggerFactory.getLogger(ManagerLotteryControlConfigAction.class);
	
	@Autowired
	private LotteryControlConfiService lotteryControlConfiService;
	@Autowired
	private SystemConfigService systemConfigService;
	@Autowired
	private AdminOperateLogService adminOperateLogService;
	
	/**
	 * 获取业务系统盈利模式数据
	 * @return
	 */
	public Object[] getLotteryControlConfig(LotteryControlConfig record){
		Admin admin=BaseDwrUtil.getCurrentAdmin();
		LotteryControlConfig record1=new LotteryControlConfig();
		if(!"SUPER_SYSTEM".equals(admin.getBizSystem())){
			record.setBizSystem(admin.getBizSystem());
		}
		List<LotteryControlConfig> list=lotteryControlConfiService.getAllLotteryControlConfig(record);
		
		if("SUPER_SYSTEM".equals(admin.getBizSystem())){
			record1.setBizSystem(admin.getBizSystem());
			record1=lotteryControlConfiService.queryLotteryControlConfigBybizSystem(record1);
			list.add(record1);
		}
		
		SystemConfig query=new SystemConfig();
		query.setConfigKey("lotteryControlConfig");
		query=systemConfigService.getSystemConfigByKey("lotteryControlConfig");
		
		return AjaxUtil.createReturnValueSuccess(list,query);
	}
	
	/**
	 * 根据业务系统查询
	 * @param bizSystem
	 * @return
	 */
	public Object[] getLotteryControlConfigBybizSystem(){
		Admin admin=BaseDwrUtil.getCurrentAdmin();
		LotteryControlConfig record=new LotteryControlConfig();
		record.setBizSystem(admin.getBizSystem());
		List<LotteryControlConfig> list=lotteryControlConfiService.getAllLotteryControlConfig(record);
		if(list.size()>0){
			record=list.get(0);
		}else{
			record=new LotteryControlConfig();
		}
		return AjaxUtil.createReturnValueSuccess(record);
	}
	
	/**
	 * 编辑
	 * @return
	 */
	public Object[] saveLotteryControlConfig(List<LotteryControlConfig> recordlist){
		//查询修改控制权限
		SystemConfig query=new SystemConfig();
		query.setConfigKey("lotteryControlConfig");
		query=systemConfigService.getSystemConfigByKey("lotteryControlConfig");
		
		Jedis jedis = null;
		try {
			jedis = JedisUtils.getResource();
			Admin admin = BaseDwrUtil.getCurrentAdmin();
			for(int i=0;i<recordlist.size();i++){
				LotteryControlConfig record=recordlist.get(i);
				//不是超级系统管理员的，只能修改自己系统的记录
				if(!"SUPER_SYSTEM".equals(admin.getBizSystem()) && !record.getBizSystem().equals(admin.getBizSystem())) {
					return AjaxUtil.createReturnValueError();
				}
				//1 子系统控制或超级系统 才修改
				if("1".equals(query.getConfigValue())||"SUPER_SYSTEM".equals(record.getBizSystem())){
					if(record.getId()!=0){
						//查询原本的数据进行对比，一样的就不修改，不同的在修改
						LotteryControlConfig dbrecord=lotteryControlConfiService.queryLotteryControlConfigByid(record.getId());
						//修改模式操作
						if(record.getJlffcProfitModel()!=null&&record.getJyksProfitModel()!=null&&record.getJspk10ProfitModel()!=null&&record.getJlffcLargestWinGain()!=null
								&&record.getJlffcLowestWinGain()!=null&&record.getJyksLowestWinGain()!=null&&record.getJyksLargestWinGain()!=null&&record.getJspk10LowestWinGain()!=null
								&&record.getJspk10LargestWinGain()!=null&&record.getXylhcProfitModel()!=null&&record.getXylhcLargestWinGain()!=null&&record.getXylhcLowestWinGain()!=null
								&&record.getSfsscProfitModel()!=null&&record.getSfsscLowestWinGain()!=null&&record.getSfsscLargestWinGain()!=null
								&&record.getShfsscProfitModel()!=null&&record.getShfsscLowestWinGain()!=null&&record.getShfsscLargestWinGain()!=null
								&&record.getWfsscProfitModel()!=null&&record.getWfsscLowestWinGain()!=null&&record.getWfsscLargestWinGain()!=null
								&&record.getLcqsscProfitModel()!=null&&record.getLcqsscLowestWinGain()!=null&&record.getLcqsscLargestWinGain()!=null
								&&record.getWfsyxwProfitModel()!=null&&record.getWfsyxwLowestWinGain()!=null&&record.getWfsyxwLargestWinGain()!=null
								&&record.getSfksProfitModel()!=null&&record.getSfksLowestWinGain()!=null&&record.getSfksLargestWinGain()!=null
								&&record.getWfksProfitModel()!=null&&record.getWfksLowestWinGain()!=null&&record.getWfksLargestWinGain()!=null
								&&record.getSfsyxwProfitModel()!=null&&record.getSfsyxwLowestWinGain()!=null&&record.getSfsyxwLargestWinGain()!=null
								&&record.getSfpk10ProfitModel()!=null&&record.getSfpk10LowestWinGain()!=null&&record.getSfpk10LargestWinGain()!=null
								&&record.getWfpk10ProfitModel()!=null&&record.getWfpk10LowestWinGain()!=null&&record.getWfpk10LargestWinGain()!=null
								&&record.getShfpk10ProfitModel()!=null&&record.getShfpk10LowestWinGain()!=null&&record.getShfpk10LargestWinGain()!=null
								){
							//redis缓存更新
							if(dbrecord.getShfsscProfitModel()==record.getShfsscProfitModel()){
								record.setShfsscProfitModel(null);
							}else{
								String jg=jedis.set("SHFSSC."+record.getBizSystem(),record.getShfsscProfitModel()+"&"+dbrecord.getEnabled());
								if(!"OK".equals(jg)){
									return AjaxUtil.createReturnValueError("redis更新缓存失败！保存失败");
								}
								saveRecordLog("SHFSSC",record.getShfsscProfitModel(),dbrecord);
							}
							if(dbrecord.getSfsscProfitModel()==record.getSfsscProfitModel()){
								record.setSfsscProfitModel(null);
							}else{
								String jg=jedis.set("SFSSC."+record.getBizSystem(),record.getSfsscProfitModel()+"&"+dbrecord.getEnabled());
								if(!"OK".equals(jg)){
									return AjaxUtil.createReturnValueError("redis更新缓存失败！保存失败");
								}
								saveRecordLog("SFSSC",record.getSfsscProfitModel(),dbrecord);
							}
							
							if(dbrecord.getWfsyxwProfitModel()==record.getWfsyxwProfitModel()){
								record.setWfsyxwProfitModel(null);
							}else{
								String jg=jedis.set("WFSYXW."+record.getBizSystem(),record.getWfsyxwProfitModel()+"&"+dbrecord.getEnabled());
								if(!"OK".equals(jg)){
									return AjaxUtil.createReturnValueError("redis更新缓存失败！保存失败");
								}
								saveRecordLog("WFSYXW",record.getWfsyxwProfitModel(),dbrecord);
							}
							
							if(dbrecord.getSfsyxwProfitModel()==record.getSfsyxwProfitModel()){
								record.setSfsyxwProfitModel(null);
							}else{
								String jg=jedis.set("SFSYXW."+record.getBizSystem(),record.getSfsyxwProfitModel()+"&"+dbrecord.getEnabled());
								if(!"OK".equals(jg)){
									return AjaxUtil.createReturnValueError("redis更新缓存失败！保存失败");
								}
								saveRecordLog("SFSYXW",record.getSfsyxwProfitModel(),dbrecord);
							}
							
							if(dbrecord.getWfsscProfitModel()==record.getWfsscProfitModel()){
								record.setWfsscProfitModel(null);
							}else{
								String jg=jedis.set("WFSSC."+record.getBizSystem(),record.getWfsscProfitModel()+"&"+dbrecord.getEnabled());
								if(!"OK".equals(jg)){
									return AjaxUtil.createReturnValueError("redis更新缓存失败！保存失败");
								}
								saveRecordLog("WFSSC",record.getWfsscProfitModel(),dbrecord);
							}
							
							if(dbrecord.getLcqsscProfitModel()==record.getLcqsscProfitModel()){
								record.setLcqsscProfitModel(null);
							}else{
								String jg=jedis.set("LCQSSC."+record.getBizSystem(),record.getLcqsscProfitModel()+"&"+dbrecord.getEnabled());
								if(!"OK".equals(jg)){
									return AjaxUtil.createReturnValueError("redis更新缓存失败！保存失败");
								}
								saveRecordLog("LCQSSC",record.getLcqsscProfitModel(),dbrecord);
							}
							
							if(dbrecord.getSfksProfitModel()==record.getSfksProfitModel()){
								record.setSfksProfitModel(null);
							}else{
								String jg=jedis.set("SFKS."+record.getBizSystem(),record.getSfksProfitModel()+"&"+dbrecord.getEnabled());
								if(!"OK".equals(jg)){
									return AjaxUtil.createReturnValueError("redis更新缓存失败！保存失败");
								}
								saveRecordLog("SFKS",record.getSfksProfitModel(),dbrecord);
							}
							if(dbrecord.getWfksProfitModel()==record.getWfksProfitModel()){
								record.setWfksProfitModel(null);
							}else{
								String jg=jedis.set("WFKS."+record.getBizSystem(),record.getWfksProfitModel()+"&"+dbrecord.getEnabled());
								if(!"OK".equals(jg)){
									return AjaxUtil.createReturnValueError("redis更新缓存失败！保存失败");
								}
								saveRecordLog("WFKS",record.getWfksProfitModel(),dbrecord);
							}
							if(dbrecord.getJlffcProfitModel()==record.getJlffcProfitModel()){
								record.setJlffcProfitModel(null);
							}else{
								String jg=jedis.set("JLFFC."+record.getBizSystem(),record.getJlffcProfitModel()+"&"+dbrecord.getEnabled());
								if(!"OK".equals(jg)){
									return AjaxUtil.createReturnValueError("redis更新缓存失败！保存失败");
								}
								saveRecordLog("JLFFC",record.getJlffcProfitModel(),dbrecord);
							}
							if(dbrecord.getJyksProfitModel()==record.getJyksProfitModel()){
								record.setJyksProfitModel(null);
							}else{
								String jg=jedis.set("JYKS."+record.getBizSystem(),record.getJlffcProfitModel()+"&"+dbrecord.getEnabled());
								if(!"OK".equals(jg)){
									return AjaxUtil.createReturnValueError("redis更新缓存失败！保存失败");
								}
								saveRecordLog("JYKS",record.getJyksProfitModel(),dbrecord);
							}
							if(dbrecord.getJspk10ProfitModel()==record.getJspk10ProfitModel()){
								record.setJspk10ProfitModel(null);
							}else{
								String jg=jedis.set("JSPK10."+record.getBizSystem(),record.getJspk10ProfitModel()+"&"+dbrecord.getEnabled());
								if(!"OK".equals(jg)){
									return AjaxUtil.createReturnValueError("redis更新缓存失败！保存失败");
								}
								saveRecordLog("JSPK10",record.getJspk10ProfitModel(),dbrecord);
							}
							if(dbrecord.getSfpk10ProfitModel()==record.getSfpk10ProfitModel()){
								record.setSfpk10ProfitModel(null);
							}else{
								String jg=jedis.set("SFPK10."+record.getBizSystem(),record.getSfpk10ProfitModel()+"&"+dbrecord.getEnabled());
								if(!"OK".equals(jg)){
									return AjaxUtil.createReturnValueError("redis更新缓存失败！保存失败");
								}
								saveRecordLog("SFPK10",record.getSfpk10ProfitModel(),dbrecord);
							}
							if(dbrecord.getWfpk10ProfitModel()==record.getWfpk10ProfitModel()){
								record.setWfpk10ProfitModel(null);
							}else{
								String jg=jedis.set("WFPK10."+record.getBizSystem(),record.getWfpk10ProfitModel()+"&"+dbrecord.getEnabled());
								if(!"OK".equals(jg)){
									return AjaxUtil.createReturnValueError("redis更新缓存失败！保存失败");
								}
								saveRecordLog("WFPK10",record.getWfpk10ProfitModel(),dbrecord);
							}
							if(dbrecord.getShfpk10ProfitModel()==record.getShfpk10ProfitModel()){
								record.setShfpk10ProfitModel(null);
							}else{
								String jg=jedis.set("SHFPK10."+record.getBizSystem(),record.getShfpk10ProfitModel()+"&"+dbrecord.getEnabled());
								if(!"OK".equals(jg)){
									return AjaxUtil.createReturnValueError("redis更新缓存失败！保存失败");
								}
								saveRecordLog("SHFPK10",record.getShfpk10ProfitModel(),dbrecord);
							}
							if(dbrecord.getXylhcProfitModel()==record.getXylhcProfitModel()){
								record.setXylhcProfitModel(null);
							}else{
								String jg=jedis.set("XYLHC."+record.getBizSystem(),record.getXylhcProfitModel()+"&"+dbrecord.getEnabled());
								if(!"OK".equals(jg)){
									return AjaxUtil.createReturnValueError("redis更新缓存失败！保存失败");
								}
								saveRecordLog("XYLHC",record.getXylhcProfitModel(),dbrecord);
							}
							
							if(record.getSfksLargestWinGain().compareTo(record.getSfksLowestWinGain()) == 1 && record.getSfksLowestWinGain().compareTo(BigDecimal.ZERO) == 1
									&& record.getSfksLargestWinGain().intValue() <= 100){
								if(record.getSfksLowestWinGain().intValue()==dbrecord.getSfksLowestWinGain().intValue()){
									record.setSfksLowestWinGain(null);
								}else{
									String jg=jedis.set("SFKS."+record.getBizSystem(),record.getSfksLowestWinGain()+"&"+record.getEnabled());
									if(!"OK".equals(jg)){
										return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败");
									}
									saveRecordLog("sfksLowestWinGain",record.getSfksLowestWinGain().intValue(),record);
								}
								
								if(record.getSfksLargestWinGain().intValue()==dbrecord.getSfksLargestWinGain().intValue()){
									record.setSfksLargestWinGain(null);
								}else{
									String jg=jedis.set("SFKS."+record.getBizSystem(),record.getSfksLargestWinGain()+"&"+record.getEnabled());
									if(!"OK".equals(jg)){
										return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败");
									}
									saveRecordLog("sfksLargestWinGain",record.getSfksLargestWinGain().intValue(),record);
								}
							}else{
								return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败,数值不合法");
							}
							if(record.getWfksLargestWinGain().compareTo(record.getWfksLowestWinGain()) == 1 && record.getWfksLowestWinGain().compareTo(BigDecimal.ZERO) == 1
									&& record.getWfksLargestWinGain().intValue() <= 100){
								if(record.getWfksLowestWinGain().intValue()==dbrecord.getWfksLowestWinGain().intValue()){
									record.setWfksLowestWinGain(null);
								}else{
									String jg=jedis.set("WFKS."+record.getBizSystem(),record.getWfksLowestWinGain()+"&"+record.getEnabled());
									if(!"OK".equals(jg)){
										return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败");
									}
									saveRecordLog("wfksLowestWinGain",record.getWfksLowestWinGain().intValue(),record);
								}
								
								if(record.getWfksLargestWinGain().intValue()==dbrecord.getWfksLargestWinGain().intValue()){
									record.setWfksLargestWinGain(null);
								}else{
									String jg=jedis.set("WFKS."+record.getBizSystem(),record.getWfksLargestWinGain()+"&"+record.getEnabled());
									if(!"OK".equals(jg)){
										return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败");
									}
									saveRecordLog("wfksLargestWinGain",record.getWfksLargestWinGain().intValue(),record);
								}
							}else{
								return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败,数值不合法");
							}
							if(record.getShfsscLargestWinGain().compareTo(record.getShfsscLowestWinGain()) == 1 && record.getShfsscLowestWinGain().compareTo(BigDecimal.ZERO) == 1
									&& record.getShfsscLargestWinGain().intValue() <= 100){
								if(record.getShfsscLowestWinGain().intValue()==dbrecord.getShfsscLowestWinGain().intValue()){
									record.setShfsscLowestWinGain(null);
								}else{
									String jg=jedis.set("SHFSSC."+record.getBizSystem(),record.getShfsscLowestWinGain()+"&"+record.getEnabled());
									if(!"OK".equals(jg)){
										return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败");
									}
									saveRecordLog("shfsscLowestWinGain",record.getShfsscLowestWinGain().intValue(),record);
								}
								
								if(record.getShfsscLargestWinGain().intValue()==dbrecord.getShfsscLargestWinGain().intValue()){
									record.setShfsscLargestWinGain(null);
								}else{
									String jg=jedis.set("SHFSSC."+record.getBizSystem(),record.getShfsscLargestWinGain()+"&"+record.getEnabled());
									if(!"OK".equals(jg)){
										return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败");
									}
									saveRecordLog("shfsscLargestWinGain",record.getShfsscLargestWinGain().intValue(),record);
								}
							}else{
								return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败,数值不合法");
							}
							if(record.getSfsscLargestWinGain().compareTo(record.getSfsscLowestWinGain()) == 1 && record.getSfsscLowestWinGain().compareTo(BigDecimal.ZERO) == 1
									&& record.getSfsscLargestWinGain().intValue() <= 100){
								if(record.getSfsscLowestWinGain().intValue()==dbrecord.getSfsscLowestWinGain().intValue()){
									record.setSfsscLowestWinGain(null);
								}else{
									String jg=jedis.set("SFSSC."+record.getBizSystem(),record.getSfsscLowestWinGain()+"&"+record.getEnabled());
									if(!"OK".equals(jg)){
										return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败");
									}
									saveRecordLog("sfsscLowestWinGain",record.getSfsscLowestWinGain().intValue(),record);
								}
								
								if(record.getSfsscLargestWinGain().intValue()==dbrecord.getSfsscLargestWinGain().intValue()){
									record.setSfsscLargestWinGain(null);
								}else{
									String jg=jedis.set("SFSSC."+record.getBizSystem(),record.getSfsscLargestWinGain()+"&"+record.getEnabled());
									if(!"OK".equals(jg)){
										return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败");
									}
									saveRecordLog("sfsscLargestWinGain",record.getSfsscLargestWinGain().intValue(),record);
								}
							}else{
								return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败,数值不合法");
							}
							if(record.getWfsscLargestWinGain().compareTo(record.getWfsscLowestWinGain()) == 1 && record.getWfsscLowestWinGain().compareTo(BigDecimal.ZERO) == 1
									&& record.getWfsscLargestWinGain().intValue() <= 100){
								if(record.getWfsscLowestWinGain().intValue()==dbrecord.getWfsscLowestWinGain().intValue()){
									record.setWfsscLowestWinGain(null);
								}else{
									String jg=jedis.set("WFSSC."+record.getBizSystem(),record.getWfsscLowestWinGain()+"&"+record.getEnabled());
									if(!"OK".equals(jg)){
										return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败");
									}
									saveRecordLog("wfsscLowestWinGain",record.getWfsscLowestWinGain().intValue(),record);
								}
								
								if(record.getWfsscLargestWinGain().intValue()==dbrecord.getWfsscLargestWinGain().intValue()){
									record.setWfsscLargestWinGain(null);
								}else{

									String jg=jedis.set("WFSSC."+record.getBizSystem(),record.getWfsscLargestWinGain()+"&"+record.getEnabled());
									if(!"OK".equals(jg)){
										return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败");
									}
									saveRecordLog("wfsscLargestWinGain",record.getWfsscLargestWinGain().intValue(),record);
								}
							}else{
								return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败,数值不合法");
							}
							
							if(record.getLcqsscLargestWinGain().compareTo(record.getLcqsscLowestWinGain()) == 1 && record.getLcqsscLowestWinGain().compareTo(BigDecimal.ZERO) == 1
									&& record.getLcqsscLargestWinGain().intValue() <= 100){
								if(record.getLcqsscLowestWinGain().intValue()==dbrecord.getLcqsscLowestWinGain().intValue()){
									record.setLcqsscLowestWinGain(null);
								}else{
									String jg=jedis.set("LCQSSC."+record.getBizSystem(),record.getLcqsscLowestWinGain()+"&"+record.getEnabled());
									if(!"OK".equals(jg)){
										return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败");
									}
									saveRecordLog("lcqsscLowestWinGain",record.getLcqsscLowestWinGain().intValue(),record);
								}
								
								if(record.getLcqsscLargestWinGain().intValue()==dbrecord.getLcqsscLargestWinGain().intValue()){
									record.setLcqsscLargestWinGain(null);
								}else{

									String jg=jedis.set("LCQSSC."+record.getBizSystem(),record.getLcqsscLargestWinGain()+"&"+record.getEnabled());
									if(!"OK".equals(jg)){
										return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败");
									}
									saveRecordLog("lcqsscLargestWinGain",record.getLcqsscLargestWinGain().intValue(),record);
								}
							}else{
								return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败,数值不合法");
							}
							
							if(record.getWfsyxwLargestWinGain().compareTo(record.getWfsyxwLowestWinGain()) == 1 && record.getWfsyxwLowestWinGain().compareTo(BigDecimal.ZERO) == 1
									&& record.getWfsyxwLargestWinGain().intValue() <= 100){
								if(record.getWfsyxwLowestWinGain().intValue()==dbrecord.getWfsyxwLowestWinGain().intValue()){
									record.setWfsyxwLowestWinGain(null);
								}else{
									String jg=jedis.set("WFSYXW."+record.getBizSystem(),record.getWfsyxwLowestWinGain()+"&"+record.getEnabled());
									if(!"OK".equals(jg)){
										return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败");
									}
									saveRecordLog("wfsyxwLowestWinGain",record.getWfsyxwLowestWinGain().intValue(),record);
								}
								
								if(record.getWfsyxwLargestWinGain().intValue()==dbrecord.getWfsyxwLargestWinGain().intValue()){
									record.setWfsyxwLargestWinGain(null);
								}else{
									String jg=jedis.set("WFSYXW."+record.getBizSystem(),record.getWfsyxwLargestWinGain()+"&"+record.getEnabled());
									if(!"OK".equals(jg)){
										return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败");
									}
									saveRecordLog("wfsyxwLargestWinGain",record.getWfsyxwLargestWinGain().intValue(),record);
								}
							}else{
								return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败,数值不合法");
							}
							
							if(record.getJlffcLargestWinGain().compareTo(record.getJlffcLowestWinGain()) == 1 && record.getJlffcLowestWinGain().compareTo(BigDecimal.ZERO) == 1
									&& record.getJlffcLargestWinGain().intValue() <= 100){
								if(record.getJlffcLowestWinGain().intValue()==dbrecord.getJlffcLowestWinGain().intValue()){
									record.setJlffcLowestWinGain(null);
								}else{
									String jg=jedis.set("JLFFC."+record.getBizSystem(),record.getJlffcLowestWinGain()+"&"+record.getEnabled());
									if(!"OK".equals(jg)){
										return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败");
									}
									saveRecordLog("jlffcLowestWinGain",record.getJlffcLowestWinGain().intValue(),record);
								}
								
								if(record.getJlffcLargestWinGain().intValue()==dbrecord.getJlffcLargestWinGain().intValue()){
									record.setJlffcLargestWinGain(null);
								}else{
									String jg=jedis.set("JLFFC."+record.getBizSystem(),record.getJlffcLargestWinGain()+"&"+record.getEnabled());
									if(!"OK".equals(jg)){
										return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败");
									}
									saveRecordLog("jlffcLargestWinGain",record.getJlffcLargestWinGain().intValue(),record);
								}
							}else{
								return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败,数值不合法");
							}
							if(record.getJyksLargestWinGain().compareTo(record.getJyksLowestWinGain()) == 1 && record.getJyksLowestWinGain().compareTo(BigDecimal.ZERO) == 1 
									&& record.getJyksLargestWinGain().intValue() <= 100){
								if(record.getJyksLowestWinGain().intValue()==dbrecord.getJyksLowestWinGain().intValue()){
									record.setJyksLowestWinGain(null);
								}else{
									String jg=jedis.set("JSKS."+record.getBizSystem(),record.getJyksLowestWinGain()+"&"+record.getEnabled());
									if(!"OK".equals(jg)){
										return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败");
									}
									saveRecordLog("jsksLowestWinGain",record.getJyksLowestWinGain().intValue(),record);
								}
								
								if(record.getJyksLargestWinGain().intValue()==dbrecord.getJyksLargestWinGain().intValue()){
									record.setJyksLargestWinGain(null);
								}else{
									String jg=jedis.set("JYKS."+record.getBizSystem(),record.getJyksLargestWinGain()+"&"+record.getEnabled());
									if(!"OK".equals(jg)){
										return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败");
									}
									saveRecordLog("jsksLargestWinGain",record.getJyksLargestWinGain().intValue(),record);
								}
							}else{
								return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败,数值不合法");
							}
							
							if(record.getJspk10LargestWinGain().compareTo(record.getJspk10LowestWinGain()) == 1 && record.getJspk10LowestWinGain().compareTo(BigDecimal.ZERO) == 1
									&& record.getJspk10LargestWinGain().intValue() <= 100){
								if(record.getJspk10LowestWinGain().intValue()==dbrecord.getJspk10LowestWinGain().intValue()){
									record.setJspk10LowestWinGain(null);
								}else{
									String jg=jedis.set("JSPK10."+record.getBizSystem(),record.getJspk10LowestWinGain()+"&"+record.getEnabled());
									if(!"OK".equals(jg)){
										return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败");
									}
									saveRecordLog("jspk10LowestWinGain",record.getJspk10LowestWinGain().intValue(),record);
								}
								
								if(record.getJspk10LargestWinGain().intValue()==dbrecord.getJspk10LargestWinGain().intValue()){
									record.setJspk10LargestWinGain(null);
								}else{
									String jg=jedis.set("JSPK10."+record.getBizSystem(),record.getJspk10LargestWinGain()+"&"+record.getEnabled());
									if(!"OK".equals(jg)){
										return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败");
									}
									saveRecordLog("jspk10LargestWinGain",record.getJspk10LargestWinGain().intValue(),record);
								}
							}else{
								return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败,数值不合法");
							}
							
							if(record.getSfpk10LargestWinGain().compareTo(record.getSfpk10LowestWinGain()) == 1 && record.getSfpk10LowestWinGain().compareTo(BigDecimal.ZERO) == 1
									&& record.getSfpk10LargestWinGain().intValue() <= 100){
								if(record.getSfpk10LowestWinGain().intValue()==dbrecord.getSfpk10LowestWinGain().intValue()){
									record.setSfpk10LowestWinGain(null);
								}else{
									String jg=jedis.set("SFPK10."+record.getBizSystem(),record.getSfpk10LowestWinGain()+"&"+record.getEnabled());
									if(!"OK".equals(jg)){
										return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败");
									}
									saveRecordLog("sfpk10LowestWinGain",record.getSfpk10LowestWinGain().intValue(),record);
								}
								
								if(record.getSfpk10LargestWinGain().intValue()==dbrecord.getSfpk10LargestWinGain().intValue()){
									record.setSfpk10LargestWinGain(null);
								}else{
									String jg=jedis.set("SFPK10."+record.getBizSystem(),record.getSfpk10LargestWinGain()+"&"+record.getEnabled());
									if(!"OK".equals(jg)){
										return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败");
									}
									saveRecordLog("sfpk10LargestWinGain",record.getSfpk10LargestWinGain().intValue(),record);
								}
							}else{
								return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败,数值不合法");
							}
							
							
							if(record.getWfpk10LargestWinGain().compareTo(record.getWfpk10LowestWinGain()) == 1 && record.getWfpk10LowestWinGain().compareTo(BigDecimal.ZERO) == 1
									&& record.getWfpk10LargestWinGain().intValue() <= 100){
								if(record.getWfpk10LowestWinGain().intValue()==dbrecord.getWfpk10LowestWinGain().intValue()){
									record.setWfpk10LowestWinGain(null);
								}else{
									String jg=jedis.set("WFPK10."+record.getBizSystem(),record.getWfpk10LowestWinGain()+"&"+record.getEnabled());
									if(!"OK".equals(jg)){
										return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败");
									}
									saveRecordLog("wfpk10LowestWinGain",record.getWfpk10LowestWinGain().intValue(),record);
								}
								
								if(record.getWfpk10LargestWinGain().intValue()==dbrecord.getWfpk10LargestWinGain().intValue()){
									record.setWfpk10LargestWinGain(null);
								}else{
									String jg=jedis.set("WFPK10."+record.getBizSystem(),record.getWfpk10LargestWinGain()+"&"+record.getEnabled());
									if(!"OK".equals(jg)){
										return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败");
									}
									saveRecordLog("wfpk10LargestWinGain",record.getWfpk10LargestWinGain().intValue(),record);
								}
							}else{
								return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败,数值不合法");
							}
							
							if(record.getShfpk10LargestWinGain().compareTo(record.getShfpk10LowestWinGain()) == 1 && record.getShfpk10LowestWinGain().compareTo(BigDecimal.ZERO) == 1
									&& record.getShfpk10LargestWinGain().intValue() <= 100){
								if(record.getShfpk10LowestWinGain().intValue()==dbrecord.getShfpk10LowestWinGain().intValue()){
									record.setShfpk10LowestWinGain(null);
								}else{
									String jg=jedis.set("SHFPK10."+record.getBizSystem(),record.getShfpk10LowestWinGain()+"&"+record.getEnabled());
									if(!"OK".equals(jg)){
										return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败");
									}
									saveRecordLog("shfpk10LowestWinGain",record.getShfpk10LowestWinGain().intValue(),record);
								}
								
								if(record.getShfpk10LargestWinGain().intValue()==dbrecord.getShfpk10LargestWinGain().intValue()){
									record.setShfpk10LargestWinGain(null);
								}else{
									String jg=jedis.set("SHFPK10."+record.getBizSystem(),record.getShfpk10LargestWinGain()+"&"+record.getEnabled());
									if(!"OK".equals(jg)){
										return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败");
									}
									saveRecordLog("shfpk10LargestWinGain",record.getShfpk10LargestWinGain().intValue(),record);
								}
							}else{
								return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败,数值不合法");
							}
							
							if(record.getXylhcLargestWinGain().compareTo(record.getXylhcLowestWinGain()) == 1 && record.getXylhcLowestWinGain().compareTo(BigDecimal.ZERO) == 1
									&& record.getXylhcLargestWinGain().intValue() <= 100){
								if(record.getXylhcLowestWinGain().intValue()==dbrecord.getXylhcLowestWinGain().intValue()){
									record.setXylhcLowestWinGain(null);
								}else{
									String jg=jedis.set("XYLHC."+record.getBizSystem(),record.getXylhcLowestWinGain()+"&"+record.getEnabled());
									if(!"OK".equals(jg)){
										return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败");
									}
									saveRecordLog("xylhcLowestWinGain",record.getXylhcLowestWinGain().intValue(),record);
								}
								
								if(record.getXylhcLargestWinGain().intValue()==dbrecord.getXylhcLargestWinGain().intValue()){
									record.setXylhcLargestWinGain(null);
								}else{
									String jg=jedis.set("XYLHC."+record.getBizSystem(),record.getXylhcLargestWinGain()+"&"+record.getEnabled());
									if(!"OK".equals(jg)){
										return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败");
									}
									saveRecordLog("xylhcLargestWinGain",record.getXylhcLargestWinGain().intValue(),record);
								}
							}else{
								return AjaxUtil.createReturnValueError("业务系统["+dbrecord.getBizSystemName()+"]记录保存失败,数值不合法");
							}
							
						}else if(record.getEnabled()!=null){//启用和关闭操作
							String jg=jedis.set("JLFFC."+dbrecord.getBizSystem(),dbrecord.getJlffcProfitModel()+"&"+record.getEnabled());
							String jg1=jedis.set("JYKS."+dbrecord.getBizSystem(),dbrecord.getJyksProfitModel()+"&"+record.getEnabled());
							String jg2=jedis.set("JSPK10."+dbrecord.getBizSystem(),dbrecord.getJspk10ProfitModel()+"&"+record.getEnabled());
							String jg36=jedis.set("XYLHC."+dbrecord.getBizSystem(),dbrecord.getXylhcProfitModel()+"&"+record.getEnabled());
							String jg37=jedis.set("SFSSC."+dbrecord.getBizSystem(),dbrecord.getSfsscProfitModel()+"&"+record.getEnabled());
							String jg29=jedis.set("WFSSC."+dbrecord.getBizSystem(),dbrecord.getWfsscProfitModel()+"&"+record.getEnabled());
							String jg30=jedis.set("SHFSSC."+dbrecord.getBizSystem(),dbrecord.getShfsscProfitModel()+"&"+record.getEnabled());
							String jg31=jedis.set("SFKS."+dbrecord.getBizSystem(),dbrecord.getSfksProfitModel()+"&"+record.getEnabled());
							String jg32=jedis.set("WFKS."+dbrecord.getBizSystem(),dbrecord.getWfksProfitModel()+"&"+record.getEnabled());
							String jg33=jedis.set("SFPK10."+dbrecord.getBizSystem(),dbrecord.getSfpk10ProfitModel()+"&"+record.getEnabled());
							String jg34=jedis.set("WFPK10."+dbrecord.getBizSystem(),dbrecord.getWfpk10ProfitModel()+"&"+record.getEnabled());
							String jg35=jedis.set("SHFPK10."+dbrecord.getBizSystem(),dbrecord.getShfpk10ProfitModel()+"&"+record.getEnabled());
							String jg38=jedis.set("LCQSSC."+dbrecord.getBizSystem(),dbrecord.getLcqsscProfitModel()+"&"+record.getEnabled());
							
							String jg3=jedis.set("JLFFC."+dbrecord.getBizSystem(),dbrecord.getJlffcLowestWinGain()+"&"+record.getEnabled());
							String jg4=jedis.set("JLFFC."+dbrecord.getBizSystem(),dbrecord.getJlffcLargestWinGain()+"&"+record.getEnabled());
							
							String jg5=jedis.set("JYKS."+dbrecord.getBizSystem(),dbrecord.getJyksLowestWinGain()+"&"+record.getEnabled());
							String jg6=jedis.set("JYKS."+dbrecord.getBizSystem(),dbrecord.getJyksLargestWinGain()+"&"+record.getEnabled());
							
							String jg7=jedis.set("JSPK10."+dbrecord.getBizSystem(),dbrecord.getJspk10LowestWinGain()+"&"+record.getEnabled());
							String jg8=jedis.set("JSPK10."+dbrecord.getBizSystem(),dbrecord.getJspk10LargestWinGain()+"&"+record.getEnabled());
							
							String jg9=jedis.set("XYLHC."+dbrecord.getBizSystem(),dbrecord.getXylhcLowestWinGain()+"&"+record.getEnabled());
							String jg10=jedis.set("XYLHC."+dbrecord.getBizSystem(),dbrecord.getXylhcLargestWinGain()+"&"+record.getEnabled());
							
							String jg11=jedis.set("SFSSC."+dbrecord.getBizSystem(),dbrecord.getSfsscLowestWinGain()+"&"+record.getEnabled());
							String jg12=jedis.set("SFSSC."+dbrecord.getBizSystem(),dbrecord.getSfsscLargestWinGain()+"&"+record.getEnabled());
							
							String jg13=jedis.set("WFSSC."+dbrecord.getBizSystem(),dbrecord.getWfsscLowestWinGain()+"&"+record.getEnabled());
							String jg14=jedis.set("WFSSC."+dbrecord.getBizSystem(),dbrecord.getWfsscLargestWinGain()+"&"+record.getEnabled());
							
							String jg15=jedis.set("SFKS."+dbrecord.getBizSystem(),dbrecord.getSfksLowestWinGain()+"&"+record.getEnabled());
							String jg16=jedis.set("SFKS."+dbrecord.getBizSystem(),dbrecord.getSfksLargestWinGain()+"&"+record.getEnabled());
							
							String jg17=jedis.set("WFKS."+dbrecord.getBizSystem(),dbrecord.getWfksLowestWinGain()+"&"+record.getEnabled());
							String jg18=jedis.set("WFKS."+dbrecord.getBizSystem(),dbrecord.getWfksLargestWinGain()+"&"+record.getEnabled());
							
							String jg19=jedis.set("SHFSSC."+dbrecord.getBizSystem(),dbrecord.getShfsscLowestWinGain()+"&"+record.getEnabled());
							String jg20=jedis.set("SHFSSC."+dbrecord.getBizSystem(),dbrecord.getShfsscLargestWinGain()+"&"+record.getEnabled());
							
							String jg21=jedis.set("SFSYXW."+dbrecord.getBizSystem(),dbrecord.getSfsyxwLowestWinGain()+"&"+record.getEnabled());
							String jg22=jedis.set("WFSYXW."+dbrecord.getBizSystem(),dbrecord.getWfsyxwLargestWinGain()+"&"+record.getEnabled());
							
							String jg23=jedis.set("SFPK10."+dbrecord.getBizSystem(),dbrecord.getSfpk10LowestWinGain()+"&"+record.getEnabled());
							String jg24=jedis.set("SFPK10."+dbrecord.getBizSystem(),dbrecord.getSfpk10LargestWinGain()+"&"+record.getEnabled());
							
							String jg25=jedis.set("WFPK10."+dbrecord.getBizSystem(),dbrecord.getWfpk10LowestWinGain()+"&"+record.getEnabled());
							String jg26=jedis.set("WFPK10."+dbrecord.getBizSystem(),dbrecord.getWfpk10LargestWinGain()+"&"+record.getEnabled());
							
							String jg27=jedis.set("SHFPK10."+dbrecord.getBizSystem(),dbrecord.getShfpk10LowestWinGain()+"&"+record.getEnabled());
							String jg28=jedis.set("SHFPK10."+dbrecord.getBizSystem(),dbrecord.getShfpk10LargestWinGain()+"&"+record.getEnabled());
							
							String jg39=jedis.set("LCQSSC."+dbrecord.getBizSystem(),dbrecord.getLcqsscLowestWinGain()+"&"+record.getEnabled());
							String jg40=jedis.set("LCQSSC."+dbrecord.getBizSystem(),dbrecord.getLcqsscLargestWinGain()+"&"+record.getEnabled());
							
							if(!"OK".equals(jg)&&!"OK".equals(jg1)&&!"OK".equals(jg2)&&!"OK".equals(jg3)
									&&!"OK".equals(jg4)&&!"OK".equals(jg5)&&!"OK".equals(jg6)&&!"OK".equals(jg7)&&!"OK".equals(jg8)&&!"OK".equals(jg9)&&!"OK".equals(jg10)
									&&!"OK".equals(jg11)&&!"OK".equals(jg12)&&!"OK".equals(jg13)&&!"OK".equals(jg14)&&!"OK".equals(jg15)&&!"OK".equals(jg16)&&!"OK".equals(jg17)
									&&!"OK".equals(jg18)&&!"OK".equals(jg19)&&!"OK".equals(jg20) &&!"OK".equals(jg21)&&!"OK".equals(jg22)&&!"OK".equals(jg23)&&!"OK".equals(jg24)
									&&!"OK".equals(jg25)&&!"OK".equals(jg26)&&!"OK".equals(jg27)&&!"OK".equals(jg28)
									&&!"OK".equals(jg29)&&!"OK".equals(jg30)&&!"OK".equals(jg31)&&!"OK".equals(jg32)
									&&!"OK".equals(jg33)&&!"OK".equals(jg34)&&!"OK".equals(jg35)&&!"OK".equals(jg36)&&!"OK".equals(jg37)&&!"OK".equals(jg38)&&!"OK".equals(jg39)&&!"OK".equals(jg40)
									){
								return AjaxUtil.createReturnValueError("redis更新缓存失败！保存失败");
							}
							saveRecordLog("enabled",record.getEnabled(),dbrecord);
						}
						
						record.setUpdateAdmin(admin.getBizSystem());
						record.setUpdateTime(new Date());
						lotteryControlConfiService.saveLotteryControlConfig(record);
					}else{
						//数据库不存在就新增	
						record.setJlffcProfitModel(0);
						record.setJyksProfitModel(0);
						record.setJspk10ProfitModel(0);
						record.setSfpk10ProfitModel(0);
						record.setWfpk10ProfitModel(0);
						record.setShfpk10ProfitModel(0);
						record.setXylhcProfitModel(0);
						record.setSfsscProfitModel(0);
						record.setWfsscProfitModel(0);
						record.setLcqsscProfitModel(0);
						record.setSfksProfitModel(0);
						record.setWfksProfitModel(0);
						record.setShfsscProfitModel(0);
						record.setSfsyxwProfitModel(0);
						record.setWfsyxwProfitModel(0);
						record.setCreateTime(new Date());
						String jg=jedis.set("JLFFC."+record.getBizSystem(),record.getJlffcProfitModel()+"&"+record.getEnabled());
						String jg1=jedis.set("JYKS."+record.getBizSystem(),record.getJyksProfitModel()+"&"+record.getEnabled());
						String jg2=jedis.set("JSPK10."+record.getBizSystem(),record.getJspk10ProfitModel()+"&"+record.getEnabled());
						String jg36=jedis.set("XYLHC."+record.getBizSystem(),record.getXylhcProfitModel()+"&"+record.getEnabled());
						String jg37=jedis.set("SFSSC."+record.getBizSystem(),record.getSfsscProfitModel()+"&"+record.getEnabled());
						String jg29=jedis.set("WFSSC."+record.getBizSystem(),record.getWfsscProfitModel()+"&"+record.getEnabled());
						String jg38=jedis.set("LCQSSC."+record.getBizSystem(),record.getLcqsscProfitModel()+"&"+record.getEnabled());
						
						String jg30=jedis.set("SHFSSC."+record.getBizSystem(),record.getShfsscProfitModel()+"&"+record.getEnabled());
						String jg31=jedis.set("SFKS."+record.getBizSystem(),record.getSfksProfitModel()+"&"+record.getEnabled());
						String jg32=jedis.set("WFKS."+record.getBizSystem(),record.getWfksProfitModel()+"&"+record.getEnabled());
						String jg33=jedis.set("SFPK10."+record.getBizSystem(),record.getSfpk10ProfitModel()+"&"+record.getEnabled());
						String jg34=jedis.set("WFPK10."+record.getBizSystem(),record.getWfpk10ProfitModel()+"&"+record.getEnabled());
						String jg35=jedis.set("SHFPK10."+record.getBizSystem(),record.getShfpk10ProfitModel()+"&"+record.getEnabled());
						
						String jg3=jedis.set("JLFFC."+record.getBizSystem(),record.getJlffcLowestWinGain()+"&"+record.getEnabled());
						String jg4=jedis.set("JLFFC."+record.getBizSystem(),record.getJlffcLargestWinGain()+"&"+record.getEnabled());
						
						String jg5=jedis.set("JYKS."+record.getBizSystem(),record.getJyksLowestWinGain()+"&"+record.getEnabled());
						String jg6=jedis.set("JYKS."+record.getBizSystem(),record.getJyksLargestWinGain()+"&"+record.getEnabled());
						
						String jg7=jedis.set("JSPK10."+record.getBizSystem(),record.getJspk10LowestWinGain()+"&"+record.getEnabled());
						String jg8=jedis.set("JSPK10."+record.getBizSystem(),record.getJspk10LargestWinGain()+"&"+record.getEnabled());
						
						String jg9=jedis.set("XYLHC."+record.getBizSystem(),record.getXylhcLowestWinGain()+"&"+record.getEnabled());
						String jg10=jedis.set("XYLHC."+record.getBizSystem(),record.getXylhcLargestWinGain()+"&"+record.getEnabled());
						
						String jg11=jedis.set("SFSSC."+record.getBizSystem(),record.getSfsscLowestWinGain()+"&"+record.getEnabled());
						String jg12=jedis.set("SFSSC."+record.getBizSystem(),record.getSfsscLargestWinGain()+"&"+record.getEnabled());
						
						String jg13=jedis.set("WFSSC."+record.getBizSystem(),record.getWfsscLowestWinGain()+"&"+record.getEnabled());
						String jg14=jedis.set("WFSSC."+record.getBizSystem(),record.getWfsscLargestWinGain()+"&"+record.getEnabled());
						
						String jg15=jedis.set("SFKS."+record.getBizSystem(),record.getSfksLowestWinGain()+"&"+record.getEnabled());
						String jg16=jedis.set("SFKS."+record.getBizSystem(),record.getSfksLargestWinGain()+"&"+record.getEnabled());
						
						String jg17=jedis.set("WFKS."+record.getBizSystem(),record.getWfksLowestWinGain()+"&"+record.getEnabled());
						String jg18=jedis.set("WFKS."+record.getBizSystem(),record.getWfksLargestWinGain()+"&"+record.getEnabled());
						
						String jg19=jedis.set("SHFSSC."+record.getBizSystem(),record.getShfsscLowestWinGain()+"&"+record.getEnabled());
						String jg20=jedis.set("SHFSSC."+record.getBizSystem(),record.getShfsscLargestWinGain()+"&"+record.getEnabled());
						
						String jg21=jedis.set("SFSYXW."+record.getBizSystem(),record.getSfsyxwLowestWinGain()+"&"+record.getEnabled());
						String jg22=jedis.set("WFSYXW."+record.getBizSystem(),record.getWfsyxwLargestWinGain()+"&"+record.getEnabled());
						
						String jg23=jedis.set("SFPK10."+record.getBizSystem(),record.getSfpk10LowestWinGain()+"&"+record.getEnabled());
						String jg24=jedis.set("SFPK10."+record.getBizSystem(),record.getSfpk10LargestWinGain()+"&"+record.getEnabled());
						
						String jg25=jedis.set("WFPK10."+record.getBizSystem(),record.getWfpk10LowestWinGain()+"&"+record.getEnabled());
						String jg26=jedis.set("WFPK10."+record.getBizSystem(),record.getWfpk10LargestWinGain()+"&"+record.getEnabled());
						
						String jg27=jedis.set("SHFPK10."+record.getBizSystem(),record.getShfpk10LowestWinGain()+"&"+record.getEnabled());
						String jg28=jedis.set("SHFPK10."+record.getBizSystem(),record.getShfpk10LargestWinGain()+"&"+record.getEnabled());
						
						String jg39=jedis.set("LCQSSC."+record.getBizSystem(),record.getLcqsscLowestWinGain()+"&"+record.getEnabled());
						String jg40=jedis.set("LCQSSC."+record.getBizSystem(),record.getLcqsscLargestWinGain()+"&"+record.getEnabled());
						
						if(!"OK".equals(jg)&&!"OK".equals(jg1)&&!"OK".equals(jg2)&&!"OK".equals(jg3)&&!"OK".equals(jg4)
								&&!"OK".equals(jg5)&&!"OK".equals(jg6)&&!"OK".equals(jg7)&&!"OK".equals(jg8)&&!"OK".equals(jg9)&&!"OK".equals(jg10)
								&&!"OK".equals(jg11)&&!"OK".equals(jg12)&&!"OK".equals(jg13)&&!"OK".equals(jg14)&&!"OK".equals(jg15)&&!"OK".equals(jg16)&&!"OK".equals(jg17)
								&&!"OK".equals(jg18)&&!"OK".equals(jg19)&&!"OK".equals(jg20) &&!"OK".equals(jg21)&&!"OK".equals(jg22)&&!"OK".equals(jg23)&&!"OK".equals(jg24)
								&&!"OK".equals(jg25)&&!"OK".equals(jg26)&&!"OK".equals(jg27)&&!"OK".equals(jg28)
								&&!"OK".equals(jg29)&&!"OK".equals(jg30)&&!"OK".equals(jg31)&&!"OK".equals(jg32)
								&&!"OK".equals(jg33)&&!"OK".equals(jg34)&&!"OK".equals(jg35)&&!"OK".equals(jg36)&&!"OK".equals(jg37)&&!"OK".equals(jg38)&&!"OK".equals(jg39)&&!"OK".equals(jg40)
								){
							return AjaxUtil.createReturnValueError("redis更新缓存失败！保存失败");
						}
						lotteryControlConfiService.insertSelective(record);
					}
				}
			}
			return AjaxUtil.createReturnValueSuccess();
		} catch (Exception e) {
			// 归还资源
			JedisUtils.returnBrokenResource(jedis);
			return AjaxUtil.createReturnValueError();
		}finally{
			// 归还资源
			JedisUtils.returnResource(jedis);
		}
	}
	
	/**
	 * 保存日志
	 */
	public void saveRecordLog(String key,Integer value,LotteryControlConfig records){
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		String operateDesc="";
		if(key.equals("JLFFC")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_config_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),"幸运分分彩",models(records.getJlffcProfitModel()),models(value));
		}else if(key.equals("JYKS")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_config_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),"幸运快3",models(records.getJyksProfitModel()),models(value));
		}else if(key.equals("JSPK10")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_config_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),"极速pk10",models(records.getJspk10ProfitModel()),models(value));
		}else if(key.equals("SFPK10")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_config_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),"三分pk10",models(records.getSfpk10ProfitModel()),models(value));
		}else if(key.equals("WFPK10")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_config_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),"五分pk10",models(records.getWfpk10ProfitModel()),models(value));
		}else if(key.equals("SHFPK10")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_config_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),"十分pk10",models(records.getShfpk10ProfitModel()),models(value));
		}else if(key.equals("XYLHC")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_config_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),"幸运六合彩",models(records.getXylhcProfitModel()),models(value));
		}else if(key.equals("SFSSC")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_config_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),"三分时时彩",models(records.getSfsscProfitModel()),models(value));
		}else if(key.equals("WFSSC")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_config_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),"五分时时彩",models(records.getWfsscProfitModel()),models(value));
		}else if(key.equals("LCQSSC")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_config_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),"老重庆时时彩",models(records.getLcqsscProfitModel()),models(value));
		}else if(key.equals("SFKS")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_config_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),"三分快三",models(records.getSfksProfitModel()),models(value));
		}else if(key.equals("WFKS")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_config_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),"五分快三",models(records.getWfksProfitModel()),models(value));
		}else if(key.equals("SHFSSC")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_config_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),"十分时时彩",models(records.getShfsscProfitModel()),models(value));
		}else if(key.equals("enabled")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_config_enabled_log", currentAdmin.getUsername(),value==0?"关闭":"启用",records.getBizSystemName());
		}else if(key.equals("shfsscLowestWinGain")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_configLowestWinGain_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),lotteryWinGain(key),records.getShfsscLowestWinGain().toString());
		}else if(key.equals("shfsscLargestWinGain")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_configLargestWinGain_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),lotteryWinGain(key),records.getShfsscLargestWinGain().toString());
		}else if(key.equals("sfsscLowestWinGain")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_configLowestWinGain_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),lotteryWinGain(key),records.getSfsscLowestWinGain().toString());
		}else if(key.equals("sfsscLargestWinGain")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_configLargestWinGain_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),lotteryWinGain(key),records.getSfsscLargestWinGain().toString());
		}else if(key.equals("sfsyxwLowestWinGain")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_configLowestWinGain_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),lotteryWinGain(key),records.getSfsyxwLowestWinGain().toString());
		}else if(key.equals("sfsyxwLargestWinGain")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_configLargestWinGain_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),lotteryWinGain(key),records.getSfsyxwLargestWinGain().toString());
		}else if(key.equals("wfsyxwLowestWinGain")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_configLowestWinGain_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),lotteryWinGain(key),records.getWfsyxwLowestWinGain().toString());
		}else if(key.equals("wfsyxwLargestWinGain")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_configLargestWinGain_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),lotteryWinGain(key),records.getWfsyxwLargestWinGain().toString());
		}else if(key.equals("wfsscLowestWinGain")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_configLowestWinGain_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),lotteryWinGain(key),records.getWfsscLowestWinGain().toString());
		}else if(key.equals("wfsscLargestWinGain")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_configLargestWinGain_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),lotteryWinGain(key),records.getWfsscLargestWinGain().toString());
		}else if(key.equals("lcqsscLowestWinGain")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_configLowestWinGain_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),lotteryWinGain(key),records.getLcqsscLowestWinGain().toString());
		}else if(key.equals("lcqsscLargestWinGain")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_configLargestWinGain_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),lotteryWinGain(key),records.getLcqsscLargestWinGain().toString());
		}else if(key.equals("sfksLowestWinGain")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_configLowestWinGain_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),lotteryWinGain(key),records.getSfksLowestWinGain().toString());
		}else if(key.equals("sfksLargestWinGain")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_configLargestWinGain_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),lotteryWinGain(key),records.getSfksLargestWinGain().toString());
		}else if(key.equals("wfksLowestWinGain")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_configLowestWinGain_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),lotteryWinGain(key),records.getWfksLowestWinGain().toString());
		}else if(key.equals("wfksLargestWinGain")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_configLargestWinGain_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),lotteryWinGain(key),records.getWfksLargestWinGain().toString());
		}else if(key.equals("jlffcLowestWinGain")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_configLowestWinGain_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),lotteryWinGain(key),records.getJlffcLowestWinGain().toString());
		}else if(key.equals("jlffcLargestWinGain")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_configLargestWinGain_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),lotteryWinGain(key),records.getJlffcLargestWinGain().toString());
		}else if(key.equals("jsksLowestWinGain")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_configLowestWinGain_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),lotteryWinGain(key),records.getJyksLowestWinGain().toString());
		}else if(key.equals("jsksLargestWinGain")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_configLargestWinGain_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),lotteryWinGain(key),records.getJyksLargestWinGain().toString());
		}else if(key.equals("jspk10LowestWinGain")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_configLowestWinGain_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),lotteryWinGain(key),records.getJspk10LowestWinGain().toString());
		}else if(key.equals("jspk10LargestWinGain")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_configLargestWinGain_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),lotteryWinGain(key),records.getJspk10LargestWinGain().toString());
		}else if(key.equals("sfpk10LowestWinGain")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_configLowestWinGain_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),lotteryWinGain(key),records.getSfpk10LowestWinGain().toString());
		}else if(key.equals("sfpk10LargestWinGain")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_configLargestWinGain_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),lotteryWinGain(key),records.getSfpk10LargestWinGain().toString());
		}else if(key.equals("wfpk10LowestWinGain")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_configLowestWinGain_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),lotteryWinGain(key),records.getWfpk10LowestWinGain().toString());
		}else if(key.equals("wfpk10LargestWinGain")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_configLargestWinGain_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),lotteryWinGain(key),records.getWfpk10LargestWinGain().toString());
		}else if(key.equals("shfpk10LowestWinGain")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_configLowestWinGain_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),lotteryWinGain(key),records.getShfpk10LowestWinGain().toString());
		}else if(key.equals("shfpk10LargestWinGain")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_configLargestWinGain_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),lotteryWinGain(key),records.getShfpk10LargestWinGain().toString());
		}else if(key.equals("xylhcLowestWinGain")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_configLargestWinGain_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),lotteryWinGain(key),records.getXylhcLowestWinGain().toString());
		}else if(key.equals("xylhcLargestWinGain")){
			operateDesc = AdminOperateUtil.constructMessage("lottery_control_configLargestWinGain_sy_log", currentAdmin.getUsername(),records.getBizSystemName(),lotteryWinGain(key),records.getXylhcLargestWinGain().toString());
		}
		try {
			String operationIp = BaseDwrUtil.getRequestIp();
			//保存日志
			adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.BIZ_SYSTEM_CONFIG, operateDesc);
		} catch (Exception e) {
			log.error("日志记录操作失败！", e);
		}
	}
	
	public String models(Integer value){
		String str="";
		if(value==0){
			str="随机模式";
		}else if(value==1){
			str="随机盈利模式";
		}else if(value==2){
			str="绝对盈利模式";
		}else if(value==3){
			str="嬴率控制模式";
		}
		return str;
	}
	
	public String lotteryWinGain(String key){
		String str="";
		if(key.equals("jlffcLowestWinGain")){
			str="幸运分分彩";
		}else if(key.equals("jlffcLargestWinGain")){
			str="幸运分分彩";
		}else if(key.equals("jsksLowestWinGain")){
			str="幸运快3";
		}else if(key.equals("jsksLargestWinGain")){
			str="幸运快3";
		}else if(key.equals("jspk10LowestWinGain")){
			str="极速PK10";
		}else if(key.equals("jspk10LargestWinGain")){
			str="极速PK10";
		}else if(key.equals("sfpk10LowestWinGain")){
			str="三分PK10";
		}else if(key.equals("sfpk10LargestWinGain")){
			str="三分PK10";
		}else if(key.equals("wfpk10LowestWinGain")){
			str="五分PK10";
		}else if(key.equals("wfpk10LargestWinGain")){
			str="五分PK10";
		}else if(key.equals("shfpk10LowestWinGain")){
			str="十分PK10";
		}else if(key.equals("shfpk10LargestWinGain")){
			str="十分PK10";
		}else if(key.equals("xylhcLargestWinGain")){
			str="幸运六合彩";
		}else if(key.equals("xylhcLowestWinGain")){
			str="幸运六合彩";
		}else if(key.equals("shfsscLargestWinGain")){
			str="十分时时彩";
		}else if(key.equals("shfsscLowestWinGain")){
			str="十分时时彩";
		}else if(key.equals("sfsscLargestWinGain")){
			str="三分时时彩";
		}else if(key.equals("sfsscLowestWinGain")){
			str="三分时时彩";
		}else if(key.equals("wfsscLargestWinGain")){
			str="五分时时彩";
		}else if(key.equals("wfsscLowestWinGain")){
			str="五分时时彩";
		}else if(key.equals("lcqsscLargestWinGain")){
			str="老重庆时时彩";
		}else if(key.equals("lcqsscLowestWinGain")){
			str="老重庆时时彩";
		}else if(key.equals("sfksLargestWinGain")){
			str="三分快三";
		}else if(key.equals("sfksLowestWinGain")){
			str="三分快三";
		}else if(key.equals("wfksLargestWinGain")){
			str="五分快三";
		}else if(key.equals("wfksLowestWinGain")){
			str="五分快三";
		}else if(key.equals("wfsyxwLargestWinGain")){
			str="五分11选5";
		}else if(key.equals("wfsyxwLowestWinGain")){
			str="五分11选5";
		}else if(key.equals("sfsyxwLargestWinGain")){
			str="三分11选5";
		}else if(key.equals("sfsyxwLowestWinGain")){
			str="三分11选5";
		}
		return str;
	}
}
