package com.team.lottery.action.web.manage;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.enums.EAdminOperateLogModel;
import com.team.lottery.extvo.ChargePayVo;
import com.team.lottery.extvo.RechargeConfigVo;
import com.team.lottery.service.AdminOperateLogService;
import com.team.lottery.service.ChargePayService;
import com.team.lottery.service.RechargeConfigService;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.util.AdminOperateUtil;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.ChargePay;
import com.team.lottery.vo.RechargeConfig;
import com.team.lottery.vo.ThirdPayConfig;

@Controller("managerChargePayAction")
public class ManagerChargePayAction {

	private static Logger log = LoggerFactory.getLogger(ManagerChargePayAction.class);
	
	@Autowired
	private ChargePayService chargePayService;
	@Autowired
	private ThirdPayConfigService thirdPayConfigService;
	@Autowired
	private AdminOperateLogService adminOperateLogService;
	@Autowired
	private RechargeConfigService rechargeConfigService;
    /**
     * 查找所有的快捷支付信息
     * @return
     */
    public Object[] getAllChargepays(ChargePayVo query){
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	if(!ConstantUtil.SUPER_SYSTEM.equals(currentAdmin.getBizSystem()))
    	{
    		query.setBizSystem(currentAdmin.getBizSystem());
    	}
		List<ChargePay> chargePays = chargePayService.selectAllChargePays(query);
		return AjaxUtil.createReturnValueSuccess(chargePays);
    }

    
    /**
     * 开启支付商户号
     * @return
     */
    public Object[] openChargepay(Long id){
    	//保存操作日志
    	saveChargepayRecordLog("chargepay_open", null, id);
    	chargePayService.updateChargePayEnabled(id, 1);
    	return AjaxUtil.createReturnValueSuccess();
    }
    
    /**
     * 关闭支付商户号
     * @return
     */
    public Object[] closeChargepay(Long id){
    	//保存操作日志
    	saveChargepayRecordLog("chargepay_close", null, id);
    	RechargeConfigVo query=new RechargeConfigVo();
		query.setRefId(id);
		ChargePay chargePay=chargePayService.selectByPrimaryKey(id);
		if(chargePay.getBizSystem()!=null && !chargePay.getBizSystem().equals(""))
		{
			 query.setBizSystem(chargePay.getBizSystem());
		}
	   
		
		List<RechargeConfig> list=rechargeConfigService.selectRechargeConfigQuickPay(query);
		if(list!=null && list.size()>0)
		{
			for(RechargeConfig rechargeConfig:list)
			{
				rechargeConfigService.updateRechargeConfigEnabled(rechargeConfig.getId(),0);
			}
			
		}
    	chargePayService.updateChargePayEnabled(id, 0);
    	return AjaxUtil.createReturnValueSuccess();
    }
    
    /**
     * 保存快捷支付操作的日志
     * @param key
     * @param domain
     */
    private void saveChargepayRecordLog(String key, ChargePay chargePay, Long id) {
    	try {
    		if(chargePay == null) {
    			chargePay = chargePayService.selectByPrimaryKey(id);
    		}
    		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    		String operateDesc = AdminOperateUtil.constructMessage(key, currentAdmin.getUsername(),chargePay.getChargeDes(), chargePay.getMemberId());
    		String operationIp = BaseDwrUtil.getRequestIp();
    		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.CHARGE_PAY, operateDesc);
    	} catch (Exception e) {
    		//保存日志出现异常 不进行处理
    		log.error("修改快捷支付设置，保存添加操作日志出错", e);
    	}
    }
    /**
     * 删除指定的快捷支付设置
     * @return
     */
    public Object[] delChargePay(Long id){
    	try {
    		ChargePay chargePay = chargePayService.selectByPrimaryKey(id);
    		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
			//EFundThirdPayType fundThirdPayType = EFundThirdPayType.valueOf(chargePay.getChargeType());
			String operateDesc = AdminOperateUtil.constructMessage("chargePay_del", currentAdmin.getUsername(),chargePay.getBizSystem(),chargePay.getChargeDes(),chargePay.getMemberId());
			String operationIp = BaseDwrUtil.getRequestIp();
			adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.CHARGE_PAY, operateDesc);
		} catch (Exception e) {
			//保存日志出现异常 不进行处理
			log.error("删除快捷支付设置，保存添加操作日志出错", e);
		}
    	chargePayService.deleteByPrimaryKey(id);
    	//通知前端应用刷新缓存就够了
    	/*try {
			NotifyFrontInterface.notifyFrontRefreshCache(EFrontCacheType.FRONT_INDEX_CACHE_INFO.getCode());
		} catch (Exception e) {
			log.error(e.getMessage());
			AjaxUtil.createReturnValueError(e.getMessage());
		}*/
    	return AjaxUtil.createReturnValueSuccess();
    }
    /**
     * 保存快捷支付设置
     * @return
     */
    public Object[] saveOrUpdateChargePay(ChargePay chargePay){
    	try{
    		Long id = chargePay.getId();
    		String chargeType=chargePay.getChargeType();
    		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    		if(!ConstantUtil.SUPER_SYSTEM.equals(currentAdmin.getBizSystem())){
    			chargePay.setBizSystem(currentAdmin.getBizSystem());
        	}
        	if(id==null){
        		ThirdPayConfig record=new ThirdPayConfig();
        		record.setChargeType(chargeType);
        		List<ThirdPayConfig> list=thirdPayConfigService.queryThirdPayConfig(record);
        		if(list!=null && list.size()>0){
        			ThirdPayConfig payconfig=list.get(0);
        			chargePay.setAddress(payconfig.getAddress());
        			chargePay.setChargeDes(payconfig.getChargeDes());
        			chargePay.setInterfaceVersion(payconfig.getInterfaceVersion());
        			chargePay.setPayUrl(payconfig.getPayUrl());
        			chargePay.setNotifyUrl(payconfig.getNotifyUrl());
        			//设置supportHttps状态
        			chargePay.setSupportHttps(payconfig.getSupportHttps());
        			chargePay.setReturnUrl(payconfig.getReturnUrl());
        			chargePay.setSignType(payconfig.getSignType());
        			chargePay.setPostUrl(payconfig.getPostUrl());
        		}
        		else
        		{
        			
        	    return AjaxUtil.createReturnValueError(chargePay.getChargeDes()+"还未设置第三方支付相关参数;所以不能设置该快捷支付!");
        	    	
        		}
        		chargePay.setCreateTime(new Date());
        		chargePay.setCreateAdmin(currentAdmin.getUsername());
        		chargePay.setApplyStatus(1);
        		//保存日志
        		//EFundThirdPayType fundThirdPayType = EFundThirdPayType.valueOf(chargePay.getChargeType());
        		Integer enabled = chargePay.getEnabled();
        		String enabledStr = "";
        		if(enabled == 1){
        			enabledStr = "启用";
        		}else{
        			enabledStr = "关闭";
        		}
        		String operateDesc = AdminOperateUtil.constructMessage("chargePay_add", currentAdmin.getUsername(),chargePay.getBizSystem(),chargePay.getChargeDes(),chargePay.getMemberId(), enabledStr);
        		String operationIp = BaseDwrUtil.getRequestIp();
        		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.CHARGE_PAY, operateDesc);
        	
        		chargePayService.insert(chargePay);
        	}else{
        		//保存日志
        		ChargePay oldChargePay = chargePayService.selectByPrimaryKey(chargePay.getId());
        		Integer oldEnabled = chargePay.getEnabled();  
        		//EFundThirdPayType oldThirdPayType = EFundThirdPayType.valueOf(oldChargePay.getChargeType());
        		
        		//EFundThirdPayType fundThirdPayType = EFundThirdPayType.valueOf(chargePay.getChargeType());
        		Integer enabled = chargePay.getEnabled();
        		String enabledStr = "";
        		String oldEnabledStr = "";
        		if(enabled == 1 && oldEnabled == 1){
        			oldEnabledStr = enabledStr = "启用";
        		}else{
        			oldEnabledStr = enabledStr = "关闭";
        		}
        		String operateDesc = AdminOperateUtil.constructMessage("chargePay_edit", currentAdmin.getUsername(),oldChargePay.getBizSystem(), chargePay.getBizSystem(),
        				oldChargePay.getChargeDes(),chargePay.getChargeDes(), oldChargePay.getMemberId(), chargePay.getMemberId(),oldEnabledStr,enabledStr);
        		String operationIp = BaseDwrUtil.getRequestIp();
        		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.CHARGE_PAY, operateDesc);
        		
        		chargePay.setUpdateTime(new Date());
        		chargePay.setUpdateAdmin(currentAdmin.getUsername());
        		chargePayService.updateByPrimaryKeySelective(chargePay);
        	}
        	/*//通知前端应用刷新缓存就够了
        	try {
    			NotifyFrontInterface.notifyFrontRefreshCache(EFrontCacheType.FRONT_INDEX_CACHE_INFO.getCode());
    		} catch (Exception e) {
    			log.error(e.getMessage());
    			AjaxUtil.createReturnValueError(e.getMessage());
    		}*/
    		return AjaxUtil.createReturnValueSuccess();
    	}catch(Exception e){
    		return AjaxUtil.createReturnValueError(e.getMessage());
    	}
    	
    }
    /**
     * 查找指定的快捷支付设置
     * @return
     */
    public Object[] getChargePayById(Long id){
    	ChargePay chargePay =  chargePayService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(chargePay);
    }
}
