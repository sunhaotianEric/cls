package com.team.lottery.action.web.manage;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.enums.EAdminOperateLogModel;
import com.team.lottery.enums.EAdminPasswordErrorLog;
import com.team.lottery.enums.EAdminPasswordErrorType;
import com.team.lottery.extvo.AdminPasswordErrorLogQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.message.messagetype.AdminLoginMessage;
import com.team.lottery.service.AdminLogService;
import com.team.lottery.service.AdminOperateLogService;
import com.team.lottery.service.AdminPasswordErrorLogService;
import com.team.lottery.service.AdminService;
import com.team.lottery.service.BizSystemService;
import com.team.lottery.system.BizSystemConfigManager;
import com.team.lottery.system.BizSystemConfigVO;
import com.team.lottery.system.MgrAdminOnlineOperater;
import com.team.lottery.system.SingleLoginForRedis;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.system.SystemPropeties;
import com.team.lottery.system.message.MessageQueueCenter;
import com.team.lottery.util.AdminOperateUtil;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.MD5PasswordUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.AdminLog;
import com.team.lottery.vo.AdminPasswordErrorLog;
import com.team.lottery.vo.BizSystem;

@Controller("managerAdminAction")
public class ManagerAdminAction {

	private static Logger log = LoggerFactory.getLogger(ManagerAdminAction.class);

	@Autowired
	private AdminService adminService;
	@Autowired
	private AdminOperateLogService adminOperateLogService;
	@Autowired
	private AdminLogService adminLogService;
	@Autowired
	private BizSystemService bizSystemService;
	@Autowired
	private AdminPasswordErrorLogService adminPasswordErrorLogService;

	/**
	 * 管理员登录
	 * 
	 * @return
	 */
	@SuppressWarnings("unused")
	public Object[] login(Admin admin) {
		if (admin == null || StringUtils.isEmpty(admin.getUsername()) || StringUtils.isEmpty(admin.getPassword()) || StringUtils.isEmpty(admin.getCheckCode())) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		// 秘密验证码
		if (StringUtils.isEmpty(admin.getSecretCode())) {
			return AjaxUtil.createReturnValueError("秘密验证码不为空.");
		}
		// 是否关闭后台登陆
		if (admin.getState() != Admin.SUPER_STATE && SystemConfigConstant.managerLoginSwich.equals(0)) {
			return AjaxUtil.createReturnValueError("亲,您来晚了,后台已经关闭了.");
		}
		// 记录登录日志
		HttpServletRequest request = BaseDwrUtil.getRequest();
		HttpSession session = request.getSession();
		StringBuffer url = request.getRequestURL();
		String tempContextUrl = url.delete(url.length() - request.getRequestURI().length(), url.length()).append("/").toString();
		String loginInfo = tempContextUrl + "-----" + request.getHeaders("User-Agent").nextElement().toString();
		String loginIp = BaseDwrUtil.getRequestIp();
		String serverName = request.getServerName();
		int port = request.getServerPort();
		AdminLog adminLog = new AdminLog();
		adminLog.setAdminName(admin.getUsername());
		adminLog.setLoginIp(loginIp);
		adminLog.setLoginDomain(serverName);
		adminLog.setLoginPort(port);
		adminLog.setBrowser(loginInfo);

		List<Admin> adminUsers = adminService.getAllAdminByUsername(admin);
		Admin dbAdmin = null;
		if (CollectionUtils.isNotEmpty(adminUsers)) {
			dbAdmin = adminUsers.get(0);
		}
		// 查询不到默认是超级系统
		adminLog.setAdminId(dbAdmin == null ? 0 : dbAdmin.getId());
		adminLog.setBizSystem(dbAdmin == null ? ConstantUtil.SUPER_SYSTEM : dbAdmin.getBizSystem());
		try {
			adminLogService.insertSelective(adminLog);
		} catch (Exception e) {
			log.error("插入登陆日志失败", e);
		}

		// 放置登录消息队列,异步获取登陆地区
		AdminLoginMessage admininMessage = new AdminLoginMessage();
		admininMessage.setIp(adminLog.getLoginIp());
		admininMessage.setLoginId(adminLog.getId());
		admininMessage.setAdminName(admin.getUsername());
		admininMessage.setSessionId(session.getId());
		admininMessage.setBizSystem(adminLog.getBizSystem());
		MessageQueueCenter.putMessage(admininMessage);

		if (dbAdmin == null) {
			return AjaxUtil.createReturnValueError("sorry!账号或密码错误！");
		}

		if (dbAdmin.getLoginLock() == 1) {
			return AjaxUtil.createReturnValueError("亲,您的账号被锁定了.");
		}

		// 登陆错误次数超出6次，锁定当前用户
		Date nowDate = new Date();
		Date nowDateStart = DateUtil.getNowStartTimeByStart(nowDate);
		Date nowDateEnd = DateUtil.getNowStartTimeByEnd(nowDate);
		AdminPasswordErrorLogQuery query = new AdminPasswordErrorLogQuery();
		query.setBizSystem(ConstantUtil.SUPER_SYSTEM);
		query.setAdminName(admin.getUsername());
		query.setLoginIp(loginIp);
		query.setErrorType(EAdminPasswordErrorType.PASSWORD_ERROR.getCode());
		query.setSourceType(EAdminPasswordErrorLog.ADMIN_LOGIN_PWD_ERROR.getCode());
		query.setCreatedDateStart(nowDateStart);
		query.setCreatedDateEnd(nowDateEnd);
		query.setIsDeleted(0);
		int errcount = adminPasswordErrorLogService.getAllAdminPasswordErrorLogByQueryPageCount(query);
		if (errcount >= SystemConfigConstant.maxLoginCount) {
			if (dbAdmin != null) {
				log.info("IP[" + loginIp + "]，管理员[" + dbAdmin.getUsername() + "]登陆次数超出" + SystemConfigConstant.maxLoginCount + "次，锁定该用户");
				Admin updateAdmin = new Admin();
				updateAdmin.setId(dbAdmin.getId());
				updateAdmin.setLoginLock(1);
				adminService.updateByPrimaryKeySelective(updateAdmin);
				log.info("系统[" + dbAdmin.getBizSystem() + "]中的管理员[" + dbAdmin.getUsername() + "]因为操作时候密码错误多次,被锁定!!!");
				// 当前用户密码错误失败过多，用户被锁定，请联系客服解锁
				return AjaxUtil.createReturnValueError("当前管理员输入密码错误次数过多，已被锁定");
			}
		}

		// 当前验证码
		String checkCode = BaseDwrUtil.getCurrentCheckCode();
		if (StringUtils.isEmpty(checkCode)) {
			return AjaxUtil.createReturnValueError("验证码不存在.");
		}
		if (!admin.getCheckCode().equalsIgnoreCase(checkCode)) {
			return AjaxUtil.createReturnValueError("验证码填写不正确.");
		}

		// 将秘密校验码错误保存在session中
		String sessionid = "secretCode" + dbAdmin.getId();
		if (ConstantUtil.SUPER_SYSTEM.equals(dbAdmin.getBizSystem())) {
			// 超级系统管理员取系统常量配置数据
			if (!admin.getSecretCode().equals(SystemConfigConstant.secretCode)) {
				if (BaseDwrUtil.getSessionAttribute(sessionid) == null) {
					BaseDwrUtil.setSessionAttribute(sessionid, new Integer(1));
				}
				Integer secretCount = (Integer) BaseDwrUtil.getSessionAttribute(sessionid);

				if (secretCount > 3) {
					Admin updateAdmin = new Admin();
					updateAdmin.setLoginLock(1);
					updateAdmin.setId(dbAdmin.getId());
					adminService.updateByPrimaryKeySelective(updateAdmin);
					BaseDwrUtil.removeSessionAttribute(sessionid);
					return AjaxUtil.createReturnValueError("秘密验证码填写错误超过3次,账号已经被锁定！");
				} else {
					secretCount = secretCount + 1;
					BaseDwrUtil.setSessionAttribute(sessionid, secretCount);
					return AjaxUtil
							.createReturnValueError("秘密验证码验证失败,秘密验证填写超过3次将被锁定！你当前还能输入" + (4 - secretCount) + "次");
				}
			}
		} else {
			// 判断当前登陆的管理员所在的业务系统是否处于禁用状态
			Admin queryAdmin = new Admin();
			queryAdmin.setUsername(admin.getUsername());
			List<Admin> list = adminService.getAdminByCondition(queryAdmin);
			if (list != null && list.size() > 0) {
				Admin adminCurrent = list.get(0);
				BizSystem queryBizSystem = new BizSystem();
				queryBizSystem.setBizSystem(adminCurrent.getBizSystem());
				BizSystem bizSystem = bizSystemService.getBizSystemByCondition(queryBizSystem);
				if (bizSystem != null) {
					if (bizSystem.getEnable() == 0) {
						return AjaxUtil.createReturnValueError(bizSystem.getBizSystemName() + "系统已经禁用，目前无法登录！");
					}
				}
			}

			// 非超级系统取业务系统配置表数据
			BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(dbAdmin.getBizSystem());
			if (bizSystemConfigVO == null) {
				return AjaxUtil.createReturnValueError("根据[{}]系统查询出来的配置信息为空,请联系客服人员.",dbAdmin.getBizSystem());
			}
			if (bizSystemConfigVO.getSecretCode() == null) {
				return AjaxUtil.createReturnValueError("本系统没有秘密验证码，请联系客服人员添加！");
			}
			if (!admin.getSecretCode().equals(bizSystemConfigVO.getSecretCode())) {
				if (BaseDwrUtil.getSessionAttribute(sessionid) == null) {
					BaseDwrUtil.setSessionAttribute(sessionid, new Integer(1));
				}
				Integer secretCount = (Integer) BaseDwrUtil.getSessionAttribute(sessionid);
				BaseDwrUtil.setSessionAttribute(sessionid, secretCount);

				if (secretCount > 3) {
					Admin updateAdmin = new Admin();
					updateAdmin.setLoginLock(1);
					updateAdmin.setId(dbAdmin.getId());
					adminService.updateByPrimaryKeySelective(updateAdmin);
					BaseDwrUtil.removeSessionAttribute(sessionid);
					return AjaxUtil.createReturnValueError("秘密验证码填写错误超过3次,账号已经被锁定！");
				} else {
					secretCount = secretCount + 1;
					BaseDwrUtil.setSessionAttribute(sessionid, secretCount);
					return AjaxUtil
							.createReturnValueError("秘密验证码验证失败,秘密验证填写超过3次将被锁定！你当前还能输入" + (4 - secretCount) + "次");
				}
			}
		}
		// 秘密验证码输对移除计数
		BaseDwrUtil.removeSessionAttribute(sessionid);

		boolean loginSuccess = adminService.loginCheck(admin, loginIp);
		if (loginSuccess) {
			// 将用户存在session当中
			dbAdmin.setPassword("");
			dbAdmin.setOperatePassword("");
			BaseDwrUtil.setCurrentAdmin(dbAdmin);
			// 登陆成功后删除管理员登陆错误日志
			adminPasswordErrorLogService.updateAdminPasswordErrorLogByErrorType(ConstantUtil.SUPER_SYSTEM, loginIp,
					dbAdmin.getUsername(), EAdminPasswordErrorType.PASSWORD_ERROR.getCode());
			
			String sessionId = BaseDwrUtil.getSession().getId();
			// 新增管理员在线信息,并踢出之前相同管理员，新增新的sessionId记录
			MgrAdminOnlineOperater.addAdminOnline(SystemPropeties.loginType + "." + dbAdmin.getBizSystem(), admin.getUsername(), sessionId);
			
			return AjaxUtil.createReturnValueSuccess();
		} else {
			// 刷新验证码
			BaseDwrUtil.refreshCurrentCheckCode();
			return AjaxUtil.createReturnValueError("sorry!账号或密码错误！");
		}
	}

	/**
	 * 发送秘密验证码
	 * 
	 * @param admin
	 * @return
	 */
	public Object[] sendSecretCode() {
		/*
		 * MailSendUtil sendUtil = new MailSendUtil(); String result =
		 * StringUtils.getRandomStringForEasy(6);
		 * BaseDwrUtil.setCurrentSecretKey(result); try {
		 * sendUtil.sendMailByRand("密码校验码", result); } catch (Exception e) { return
		 * AjaxUtil.createReturnValueError("发送失败，请重试"); }
		 */
		return AjaxUtil.createReturnValueSuccess();
	}

	/**
	 * 查找所有的管理员数据
	 * 
	 * @return
	 */
	public Object[] getAllAdmin() {
		List<Admin> admins = adminService.getAllAdmins();
		for (Admin admin : admins) {
			Set<String> sets = SingleLoginForRedis.getRedisSets(SystemPropeties.loginType + "." + admin.getBizSystem());
			//判断redis集合中是否包含了管理员用户名，有则判断为在线状态
			if (sets.contains(admin.getUsername())) {
				admin.setIsOnline(1);
			}
		}
		return AjaxUtil.createReturnValueSuccess(admins);
	}

	/**
	 * 查找所有的管理员数据 有分页的
	 * 
	 * @return
	 */
	public Object[] getAllAdmin(Admin query, Page page) {
		if (query == null || page == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		if (!"SUPER_SYSTEM".equals(currentAdmin.getBizSystem())) {
			query.setBizSystem(currentAdmin.getBizSystem());
		}
		page = adminService.getAllAdminByQueryPage(query, page);
		return AjaxUtil.createReturnValueSuccess(page);
	}

	/**
	 * 新增和修改
	 * 
	 * @param systemConfig
	 * @return
	 */
	public Object[] saveOrUpdateAdmin(Admin admin, String currentOperatePassword) {
		try {
			if (admin == null) {
				return AjaxUtil.createReturnValueError("参数传递错误");
			}
			if (admin.getId() != null && !"".equals(admin.getId())) {
				return this.editAdmin(admin, currentOperatePassword);
			} else {
				return this.addAdmin(admin, currentOperatePassword);
			}
		} catch (Exception e) {
			return AjaxUtil.createReturnValueError(e.getMessage());
		}
	}

	/**
	 * 添加管理员数据
	 * 
	 * @return
	 */
	public Object[] addAdmin(Admin admin, String currentOperatePassword) {
		try {
			Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
			Admin dbAdmin = adminService.selectByPrimaryKey(currentAdmin.getId());
			if (!currentAdmin.getState().equals(Admin.SUPER_STATE)) {
				if(!currentAdmin.getBizSystem().equals("SUPER_SYSTEM")){
					return AjaxUtil.createReturnValueError("对不起，您无权编辑管理员");
				}
			}
			// 判断当前管理员是否被锁定!
			if (dbAdmin.getLoginLock() == 1) {
				return AjaxUtil.createReturnValueError("您的账号已经被锁定,请联系超级管理员");
			}
			// 当管理员操作管理员密码错误时超过规定的次数的时候进行锁定处理.
			String loginIp = BaseDwrUtil.getRequestIp();
			Date nowDate = new Date();
			Date nowDateStart = DateUtil.getNowStartTimeByStart(nowDate);
			Date nowDateEnd = DateUtil.getNowStartTimeByEnd(nowDate);
			// 新建查询对象用于查询密码错误次数.
			AdminPasswordErrorLogQuery query = new AdminPasswordErrorLogQuery();
			query.setAdminId(currentAdmin.getId());
			query.setBizSystem(currentAdmin.getBizSystem());
			query.setErrorType(EAdminPasswordErrorType.OPERATE_PASSWORD_ERROR.getCode());
			query.setLoginIp(loginIp);
			query.setIsDeleted(0);
			query.setCreatedDateStart(nowDateStart);
			query.setCreatedDateEnd(nowDateEnd);
			// 查询记录数.
			int errorCount = adminPasswordErrorLogService.getAllAdminPasswordErrorLogByQueryPageCount(query);
			// 通过错误密码记录来确定管理员是否要被锁定!!
			if (errorCount >= SystemConfigConstant.maxLoginCount) {
				log.info("IP[" + loginIp + "]，管理员[" + dbAdmin.getUsername() + "]操作管理员时候," + SystemConfigConstant.maxLoginCount + "次，锁定该用户");
				Admin updateAdmin = new Admin();
				updateAdmin.setId(dbAdmin.getId());
				updateAdmin.setLoginLock(1);
				// 锁定管理员
				adminService.updateByPrimaryKeySelective(updateAdmin);
				adminPasswordErrorLogService.updateAdminPasswordErrorLogByErrorType(ConstantUtil.SUPER_SYSTEM, loginIp, dbAdmin.getUsername(), EAdminPasswordErrorType.PASSWORD_ERROR.getCode());
				log.info("系统[" + dbAdmin.getBizSystem() + "]中的管理员[" + dbAdmin.getUsername() + "]因为操作时候密码错误多次,被锁定!!!");
				// 当前用户密码错误失败过多，用户被锁定，请联系客服解锁
				return AjaxUtil.createReturnValueError("当前管理员输入密码错误次数过多，已被锁定");
			}

			if (!MD5PasswordUtil.GetMD5Code(currentOperatePassword).equals(dbAdmin.getOperatePassword())) {
				// 新建管理员密码错误日志对象,用于校验管理员是否应该被锁定.
				AdminPasswordErrorLog adminPasswordErrorLog = new AdminPasswordErrorLog();
				adminPasswordErrorLog.setAdminId(currentAdmin.getId());
				adminPasswordErrorLog.setAdminName(currentAdmin.getUsername());
				adminPasswordErrorLog.setBizSystem(currentAdmin.getBizSystem());
				adminPasswordErrorLog.setCreatedDate(new Date());
				adminPasswordErrorLog.setErrorType(EAdminPasswordErrorType.OPERATE_PASSWORD_ERROR.getCode());
				adminPasswordErrorLog.setIsDeleted(0);
				adminPasswordErrorLog.setLoginIp(loginIp);
				adminPasswordErrorLog.setSourceType(EAdminPasswordErrorLog.ADMIN_OPERATION_ADMIN_PWD_ERROR.getCode());
				adminPasswordErrorLog.setSourceDes(EAdminPasswordErrorLog.ADMIN_OPERATION_ADMIN_PWD_ERROR.getDescription());
				// 新建管理员错误密码记录对象.
				adminPasswordErrorLogService.insertSelective(adminPasswordErrorLog);
				return AjaxUtil.createReturnValueError("当前管理员操作密码错误,错误超过" + SystemConfigConstant.maxLoginCount + "次,将被锁定!!!");
			} else {
				// 密码正确,就软删除之前的所有的错误密码记录.
				adminPasswordErrorLogService.updateAdminPasswordErrorLogByErrorType(currentAdmin.getBizSystem(),loginIp, currentAdmin.getUsername(), EAdminPasswordErrorType.OPERATE_PASSWORD_ERROR.getCode());
			}
			List<Admin> admins = adminService.getAllAdminByUsername(admin);
			if (admins != null && admins.size() > 0) {
				return AjaxUtil.createReturnValueError(admin.getUsername() + "管理员已经存在");
			}
			if (!admin.getBizSystem().equals("SUPER_SYSTEM") && admin.getState() == 0) {
				Integer supercount = adminService.getSuperManagerCountByBizSystem(admin);
				if (supercount > 0) {
					return AjaxUtil.createReturnValueError("该业务系统已经有超级管理员！不能再添加！");
				}
			}
			admin.setUpdateTime(new Date());
			admin.setUpdateAdmin(currentAdmin.getUsername());
			// 添加操作日志
			saveAdminRecordLog("admin_add", admin, null);
			adminService.insertSelective(admin);

		} catch (Exception e) {
			return AjaxUtil.createReturnValueError(e.getMessage());
		}

		return AjaxUtil.createReturnValueSuccess();
	}

	/**
	 * 管理员联动角色变换
	 * 
	 * @return
	 */
	public Object[] onChangeState(Admin admin) {
		try {
			List<Admin> admins = adminService.getAllAdminByUsername(admin);
			if (admins == null || admins.size() == 0) {
				return AjaxUtil.createReturnValueError(admin.getUsername() + "管理员不存在");
			}
			return AjaxUtil.createReturnValueSuccess(admins.get(0));
		} catch (Exception e) {
			return AjaxUtil.createReturnValueError(e.getMessage());
		}
	}

	/**
	 * 修改管理员数据
	 * 
	 * @return
	 */
	public Object[] editAdmin(Admin admin, String currentOperatePassword) {
		try {
			Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
			Admin dbAdmin = adminService.selectByPrimaryKey(currentAdmin.getId());
			if (!currentAdmin.getState().equals(Admin.SUPER_STATE)) {
				if(!currentAdmin.getBizSystem().equals("SUPER_SYSTEM")){
					return AjaxUtil.createReturnValueError("对不起，您无权编辑管理员");
				}

			}
			// 判断当前管理员是否被锁定!
			if (dbAdmin.getLoginLock() == 1) {
				return AjaxUtil.createReturnValueError("您的账号已经被锁定,请联系超级管理员");
			}
			// 当管理员操作管理员密码错误时超过规定的次数的时候进行锁定处理.
			String loginIp = BaseDwrUtil.getRequestIp();
			Date nowDate = new Date();
			Date nowDateStart = DateUtil.getNowStartTimeByStart(nowDate);
			Date nowDateEnd = DateUtil.getNowStartTimeByEnd(nowDate);
			// 新建查询对象用于查询密码错误次数.
			AdminPasswordErrorLogQuery query = new AdminPasswordErrorLogQuery();
			query.setAdminId(currentAdmin.getId());
			query.setBizSystem(currentAdmin.getBizSystem());
			query.setErrorType(EAdminPasswordErrorType.OPERATE_PASSWORD_ERROR.getCode());
			query.setLoginIp(loginIp);
			query.setIsDeleted(0);
			query.setCreatedDateStart(nowDateStart);
			query.setCreatedDateEnd(nowDateEnd);
			// 查询记录数.
			int errorCount = adminPasswordErrorLogService.getAllAdminPasswordErrorLogByQueryPageCount(query);
			// 通过错误密码记录来确定管理员是否要被锁定!!
			if (errorCount >= SystemConfigConstant.maxLoginCount) {
				log.info("IP[" + loginIp + "]，管理员[" + dbAdmin.getUsername() + "]操作管理员时候," + SystemConfigConstant.maxLoginCount + "次，锁定该用户");
				Admin updateAdmin = new Admin();
				updateAdmin.setId(dbAdmin.getId());
				updateAdmin.setLoginLock(1);
				// 锁定管理员
				adminService.updateByPrimaryKeySelective(updateAdmin);
				adminPasswordErrorLogService.updateAdminPasswordErrorLogByErrorType(ConstantUtil.SUPER_SYSTEM, loginIp, dbAdmin.getUsername(), EAdminPasswordErrorType.PASSWORD_ERROR.getCode());
				log.info("系统[" + dbAdmin.getBizSystem() + "]中的管理员[" + dbAdmin.getUsername() + "]因为操作时候密码错误多次,被锁定!!!");
				// 当前用户密码错误失败过多，用户被锁定，请联系客服解锁
				return AjaxUtil.createReturnValueError("当前管理员输入密码错误次数过多，已被锁定");
			}

			if (!MD5PasswordUtil.GetMD5Code(currentOperatePassword).equals(dbAdmin.getOperatePassword())) {
				// 新建管理员密码错误日志对象,用于校验管理员是否应该被锁定.
				AdminPasswordErrorLog adminPasswordErrorLog = new AdminPasswordErrorLog();
				adminPasswordErrorLog.setAdminId(currentAdmin.getId());
				adminPasswordErrorLog.setAdminName(currentAdmin.getUsername());
				adminPasswordErrorLog.setBizSystem(currentAdmin.getBizSystem());
				adminPasswordErrorLog.setCreatedDate(new Date());
				adminPasswordErrorLog.setErrorType(EAdminPasswordErrorType.OPERATE_PASSWORD_ERROR.getCode());
				adminPasswordErrorLog.setIsDeleted(0);
				adminPasswordErrorLog.setLoginIp(loginIp);
				adminPasswordErrorLog.setSourceType(EAdminPasswordErrorLog.ADMIN_OPERATION_ADMIN_PWD_ERROR.getCode());
				adminPasswordErrorLog.setSourceDes(EAdminPasswordErrorLog.ADMIN_OPERATION_ADMIN_PWD_ERROR.getDescription());
				// 新建管理员错误密码记录对象.
				adminPasswordErrorLogService.insertSelective(adminPasswordErrorLog);
				return AjaxUtil.createReturnValueError("当前管理员操作密码错误,错误超过" + SystemConfigConstant.maxLoginCount + "次,将被锁定!!!");
			} else {
				// 密码正确,就软删除之前的所有的错误密码记录.
				adminPasswordErrorLogService.updateAdminPasswordErrorLogByErrorType(currentAdmin.getBizSystem(),loginIp, currentAdmin.getUsername(), EAdminPasswordErrorType.OPERATE_PASSWORD_ERROR.getCode());
			}
			// 判断权限，一个一个系统应该只有一个超级管理员
			Admin oldadmin = adminService.selectByPrimaryKey(admin.getId());
			if (!oldadmin.getBizSystem().equals("SUPER_SYSTEM") && oldadmin.getState() != 0 && admin.getState() == 0) {
				Integer supercount = adminService.getSuperManagerCountByBizSystem(admin);
				if (supercount > 0) {
					return AjaxUtil.createReturnValueError("该业务系统已经有超级管理员！不能再添加！");
				}
			}

			admin.setUpdateTime(new Date());
			admin.setUpdateAdmin(currentAdmin.getUsername());
			admin.setState(admin.getState());
			// 添加操作日志
			saveAdminRecordLog("admin_edit", admin, null);
			adminService.updateByPrimaryKeySelective(admin);

			// 踢出管理员
			MgrAdminOnlineOperater.kickOutAdmin(SystemPropeties.loginType + "." + admin.getBizSystem(), admin.getUsername());
		} catch (Exception e) {
			return AjaxUtil.createReturnValueError(e.getMessage());
		}

		return AjaxUtil.createReturnValueSuccess("操作成功");
	}

	/**
	 * 修改管理员登陆密码
	 * 
	 * @return
	 */
	public Object[] editAdminToLoginPass(Admin admin, String currentOperatePassword) {
		try {
			Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
			Admin dbAdmin = adminService.selectByPrimaryKey(currentAdmin.getId());
			if (!currentAdmin.getState().equals(Admin.SUPER_STATE)) {
				if(!currentAdmin.getBizSystem().equals("SUPER_SYSTEM")){
					return AjaxUtil.createReturnValueError("对不起，您无权编辑管理员");
				}
			}
			// 判断当前管理员是否被锁定!
			if (dbAdmin.getLoginLock() == 1) {
				return AjaxUtil.createReturnValueError("您的账号已经被锁定,请联系超级管理员");
			}
			// 当管理员操作管理员密码错误时超过规定的次数的时候进行锁定处理.
			String loginIp = BaseDwrUtil.getRequestIp();
			Date nowDate = new Date();
			Date nowDateStart = DateUtil.getNowStartTimeByStart(nowDate);
			Date nowDateEnd = DateUtil.getNowStartTimeByEnd(nowDate);
			// 新建查询对象用于查询密码错误次数.
			AdminPasswordErrorLogQuery query = new AdminPasswordErrorLogQuery();
			query.setAdminId(currentAdmin.getId());
			query.setBizSystem(currentAdmin.getBizSystem());
			query.setErrorType(EAdminPasswordErrorType.OPERATE_PASSWORD_ERROR.getCode());
			query.setLoginIp(loginIp);
			query.setIsDeleted(0);
			query.setCreatedDateStart(nowDateStart);
			query.setCreatedDateEnd(nowDateEnd);
			// 查询记录数.
			int errorCount = adminPasswordErrorLogService.getAllAdminPasswordErrorLogByQueryPageCount(query);
			// 通过错误密码记录来确定管理员是否要被锁定!!
			if (errorCount >= SystemConfigConstant.maxLoginCount) {
				log.info("IP[" + loginIp + "]，管理员[" + dbAdmin.getUsername() + "]操作管理员时候," + SystemConfigConstant.maxLoginCount + "次，锁定该用户");
				Admin updateAdmin = new Admin();
				updateAdmin.setId(dbAdmin.getId());
				updateAdmin.setLoginLock(1);
				// 锁定管理员
				adminService.updateByPrimaryKeySelective(updateAdmin);
				adminPasswordErrorLogService.updateAdminPasswordErrorLogByErrorType(ConstantUtil.SUPER_SYSTEM, loginIp,dbAdmin.getUsername(), EAdminPasswordErrorType.PASSWORD_ERROR.getCode());
				log.info("系统[" + dbAdmin.getBizSystem() + "]中的管理员[" + dbAdmin.getUsername() + "]因为操作时候密码错误多次,被锁定!!!");
				// 当前用户密码错误失败过多，用户被锁定，请联系客服解锁
				return AjaxUtil.createReturnValueError("当前管理员输入密码错误次数过多，已被锁定");
			}

			if (!MD5PasswordUtil.GetMD5Code(currentOperatePassword).equals(dbAdmin.getOperatePassword())) {
				// 新建管理员密码错误日志对象,用于校验管理员是否应该被锁定.
				AdminPasswordErrorLog adminPasswordErrorLog = new AdminPasswordErrorLog();
				adminPasswordErrorLog.setAdminId(currentAdmin.getId());
				adminPasswordErrorLog.setAdminName(currentAdmin.getUsername());
				adminPasswordErrorLog.setBizSystem(currentAdmin.getBizSystem());
				adminPasswordErrorLog.setCreatedDate(new Date());
				adminPasswordErrorLog.setErrorType(EAdminPasswordErrorType.OPERATE_PASSWORD_ERROR.getCode());
				adminPasswordErrorLog.setIsDeleted(0);
				adminPasswordErrorLog.setLoginIp(loginIp);
				adminPasswordErrorLog.setSourceType(EAdminPasswordErrorLog.ADMIN_OPERATION_ADMIN_PWD_ERROR.getCode());
				adminPasswordErrorLog.setSourceDes(EAdminPasswordErrorLog.ADMIN_OPERATION_ADMIN_PWD_ERROR.getDescription());
				// 新建管理员错误密码记录对象.
				adminPasswordErrorLogService.insertSelective(adminPasswordErrorLog);
				return AjaxUtil.createReturnValueError("当前管理员操作密码错误,错误超过" + SystemConfigConstant.maxLoginCount + "次,将被锁定!!!");
			} else {
				// 密码正确,就软删除之前的所有的错误密码记录.
				adminPasswordErrorLogService.updateAdminPasswordErrorLogByErrorType(currentAdmin.getBizSystem(),loginIp, currentAdmin.getUsername(), EAdminPasswordErrorType.OPERATE_PASSWORD_ERROR.getCode());
			}

			// 判断权限，一个一个系统应该只有一个超级管理员
			Admin oldadmin = adminService.selectByPrimaryKey(admin.getId());
			if (oldadmin.getState() != 0 && admin.getState() == 0) {

				Integer supercount = adminService.getSuperManagerCountByBizSystem(admin);
				if (supercount > 0) {
					return AjaxUtil.createReturnValueError("该业务系统已经有超级管理员！不能再添加！");
				}
			}

			admin.setUpdateTime(new Date());
			admin.setUpdateAdmin(currentAdmin.getUsername());
			// 如果密码字段不为空，则设置密码
			if (!StringUtils.isEmpty(admin.getPassword())) {
				admin.setPassword(MD5PasswordUtil.GetMD5Code(admin.getPassword()));
			} else {
				return AjaxUtil.createReturnValueError("密码不能为空！");
			}

			// 添加操作日志
			saveAdminRecordLog("admin_edit", admin, null);
			adminService.updateByPrimaryKeySelective(admin);

			// 踢出管理员
			MgrAdminOnlineOperater.kickOutAdmin(SystemPropeties.loginType + "." + admin.getBizSystem(), admin.getUsername());

		} catch (Exception e) {
			return AjaxUtil.createReturnValueError(e.getMessage());
		}

		return AjaxUtil.createReturnValueSuccess("操作成功");
	}

	/**
	 * 修改管理员操作密码
	 * 
	 * @return
	 */
	public Object[] editAdminToHandlePass(Admin admin, String currentOperatePassword) {
		try {
			Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
			Admin dbAdmin = adminService.selectByPrimaryKey(currentAdmin.getId());
			if (!currentAdmin.getState().equals(Admin.SUPER_STATE)) {
				if(!currentAdmin.getBizSystem().equals("SUPER_SYSTEM")){
					return AjaxUtil.createReturnValueError("对不起，您无权编辑管理员");
				}
			}
			// 判断管理员是否被锁定!!!
			if (dbAdmin.getLoginLock() == 1) {
				return AjaxUtil.createReturnValueError("您的账号已经被锁定,请联系超级管理员");
			}
			// 当管理员操作管理员密码错误时超过规定的次数的时候进行锁定处理.
			String loginIp = BaseDwrUtil.getRequestIp();
			Date nowDate = new Date();
			Date nowDateStart = DateUtil.getNowStartTimeByStart(nowDate);
			Date nowDateEnd = DateUtil.getNowStartTimeByEnd(nowDate);
			// 新建查询对象用于查询密码错误次数.
			AdminPasswordErrorLogQuery query = new AdminPasswordErrorLogQuery();
			query.setAdminId(currentAdmin.getId());
			query.setBizSystem(currentAdmin.getBizSystem());
			query.setErrorType(EAdminPasswordErrorType.OPERATE_PASSWORD_ERROR.getCode());
			query.setLoginIp(loginIp);
			query.setIsDeleted(0);
			query.setCreatedDateStart(nowDateStart);
			query.setCreatedDateEnd(nowDateEnd);
			// 查询记录数.
			int errorCount = adminPasswordErrorLogService.getAllAdminPasswordErrorLogByQueryPageCount(query);
			// 通过错误密码记录来确定管理员是否要被锁定!!
			if (errorCount >= SystemConfigConstant.maxLoginCount) {
				log.info("IP[" + loginIp + "]，管理员[" + dbAdmin.getUsername() + "]操作管理员时候," + SystemConfigConstant.maxLoginCount + "次，锁定该用户");
				Admin updateAdmin = new Admin();
				updateAdmin.setId(dbAdmin.getId());
				updateAdmin.setLoginLock(1);
				// 锁定管理员
				adminService.updateByPrimaryKeySelective(updateAdmin);
				adminPasswordErrorLogService.updateAdminPasswordErrorLogByErrorType(ConstantUtil.SUPER_SYSTEM, loginIp,dbAdmin.getUsername(), EAdminPasswordErrorType.PASSWORD_ERROR.getCode());
				log.info("系统[" + dbAdmin.getBizSystem() + "]中的管理员[" + dbAdmin.getUsername() + "]因为操作时候密码错误多次,被锁定!!!");
				// 当前用户密码错误失败过多，用户被锁定，请联系客服解锁
				return AjaxUtil.createReturnValueError("当前管理员输入密码错误次数过多，已被锁定");
			}

			if (!MD5PasswordUtil.GetMD5Code(currentOperatePassword).equals(dbAdmin.getOperatePassword())) {
				// 新建管理员密码错误日志对象,用于校验管理员是否应该被锁定.
				AdminPasswordErrorLog adminPasswordErrorLog = new AdminPasswordErrorLog();
				adminPasswordErrorLog.setAdminId(currentAdmin.getId());
				adminPasswordErrorLog.setAdminName(currentAdmin.getUsername());
				adminPasswordErrorLog.setBizSystem(currentAdmin.getBizSystem());
				adminPasswordErrorLog.setCreatedDate(new Date());
				adminPasswordErrorLog.setErrorType(EAdminPasswordErrorType.OPERATE_PASSWORD_ERROR.getCode());
				adminPasswordErrorLog.setIsDeleted(0);
				adminPasswordErrorLog.setLoginIp(loginIp);
				adminPasswordErrorLog.setSourceType(EAdminPasswordErrorLog.ADMIN_OPERATION_ADMIN_PWD_ERROR.getCode());
				adminPasswordErrorLog.setSourceDes(EAdminPasswordErrorLog.ADMIN_OPERATION_ADMIN_PWD_ERROR.getDescription());
				// 新建管理员错误密码记录对象.
				adminPasswordErrorLogService.insertSelective(adminPasswordErrorLog);
				return AjaxUtil.createReturnValueError("当前管理员操作密码错误,错误超过" + SystemConfigConstant.maxLoginCount + "次,将被锁定!!!");
			} else {
				// 密码正确,就软删除之前的所有的错误密码记录.
				adminPasswordErrorLogService.updateAdminPasswordErrorLogByErrorType(currentAdmin.getBizSystem(),loginIp, currentAdmin.getUsername(), EAdminPasswordErrorType.OPERATE_PASSWORD_ERROR.getCode());
			}			
			// 判断权限，一个一个系统应该只有一个超级管理员
			Admin oldadmin = adminService.selectByPrimaryKey(admin.getId());
			if (oldadmin.getState() != 0 && admin.getState() == 0) {
				Integer supercount = adminService.getSuperManagerCountByBizSystem(admin);
				if (supercount > 0) {
					return AjaxUtil.createReturnValueError("该业务系统已经有超级管理员！不能再添加！");
				}
			}

			admin.setUpdateTime(new Date());
			admin.setUpdateAdmin(currentAdmin.getUsername());

			// 如果新操作密码字段不为空，则设置新操作密码
			if (!StringUtils.isEmpty(admin.getOperatePassword())) {
				admin.setOperatePassword(MD5PasswordUtil.GetMD5Code(admin.getOperatePassword()));
			} else {
				return AjaxUtil.createReturnValueError("操作密码不能为空！");
			}
			// 添加操作日志
			saveAdminRecordLog("admin_edit", admin, null);
			adminService.updateByPrimaryKeySelective(admin);

			// 踢出管理员
			MgrAdminOnlineOperater.kickOutAdmin(SystemPropeties.loginType + "." + admin.getBizSystem(), admin.getUsername());

		} catch (Exception e) {
			return AjaxUtil.createReturnValueError(e.getMessage());
		}

		return AjaxUtil.createReturnValueSuccess("操作成功");
	}

	/**
	 * 删除管理员
	 * 
	 * @return
	 */
	public Object[] delAdmin(Integer id) {
		try {
			Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
			Admin delAdmin = adminService.selectByPrimaryKey(id);
			if (delAdmin == null) {
				return AjaxUtil.createReturnValueError("该管理员账号不存在.");
			}
			if (!currentAdmin.getState().equals(Admin.SUPER_STATE)) {
				if(!currentAdmin.getBizSystem().equals("SUPER_SYSTEM")){
					return AjaxUtil.createReturnValueError("对不起，您无权编辑管理员");
				}
			}
			// 添加操作日志
			saveAdminRecordLog("admin_del", delAdmin, null);
			// 踢出管理员
			MgrAdminOnlineOperater.kickOutAdmin(SystemPropeties.loginType + "." + delAdmin.getBizSystem(), delAdmin.getUsername());
			adminService.deleteByPrimaryKey(id);

		} catch (Exception e) {
			return AjaxUtil.createReturnValueError(e.getMessage());
		}

		return AjaxUtil.createReturnValueSuccess();
	}

	/**
	 * 使管理员退出
	 * 
	 * @return
	 */
	public Object[] logoutAdmin(String bizSystem, String userName) {
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		if (!currentAdmin.getState().equals(Admin.SUPER_STATE)) {
			return AjaxUtil.createReturnValueError("对不起，您不能进行此操作");
		}
		
		// 踢出管理员
		MgrAdminOnlineOperater.kickOutAdmin(SystemPropeties.loginType + "." + bizSystem, userName);

		return AjaxUtil.createReturnValueSuccess();
	}

	/**
	 * 管理员退出
	 * 
	 * @return
	 */
	public Object[] logout() {

		// 这里不需要调用删除redis在线信息，会话销毁会由SessionListener的sessionDestroyed方法调用
//		// 管理员在线记录的key
//		String onlineAdminSetKey = SystemPropeties.loginType + "." + BaseDwrUtil.getCurrentAdmin().getBizSystem();
//		String sessionId = BaseDwrUtil.getSession().getId();
//		// 删除管理员在线信息，并删除sessionId记录
//		MgrAdminOnlineOperater.delAdminOnline(onlineAdminSetKey,  BaseDwrUtil.getCurrentAdmin().getUsername(), sessionId);

		log.info("当前管理员[{}],所属业务系统[{}]手动点击退出登录", BaseDwrUtil.getCurrentAdmin().getUsername(), BaseDwrUtil.getCurrentAdmin().getBizSystem());
		BaseDwrUtil.removeCurrentAdmin();
		return AjaxUtil.createReturnValueSuccess();
	}

	/**
	 * 编辑管理员登陆解锁
	 * 
	 * @param id
	 * @return
	 */
	public Object[] loginAuthorityOn(Integer id) {
		if (id == null) {
			return AjaxUtil.createReturnValueError("参数错误！");
		}

		Admin adminUser = adminService.selectByPrimaryKey(id);

		if (adminUser == null) {
			return AjaxUtil.createReturnValueError("未找到该管理员信息！");
		}

		try {
			// 添加操作日志
			saveAdminRecordLog("Admin_loginLockOn", adminUser, null);
			// 根据管理员名称删除密码错误日志记录
			adminPasswordErrorLogService.updateAdminPasswordErrorLogByAdminName(adminUser.getUsername());
			Admin admin = new Admin();
			admin.setId(id);
			admin.setLoginLock(0);
			adminService.updateByPrimaryKeySelective(admin);

		} catch (Exception e) {
			log.info("编辑管理员锁定失败！" + e);
			e.printStackTrace();
		}

		return AjaxUtil.createReturnValueSuccess();
	}

	/**
	 * 编辑管理员登陆锁定
	 * 
	 * @param id
	 * @return
	 */
	public Object[] loginAuthorityOff(Integer id) {
		if (id == null) {
			return AjaxUtil.createReturnValueError("参数错误！");
		}
		Admin adminUser = adminService.selectByPrimaryKey(id);
		if (adminUser != null) {
			if (adminUser.getUsername().equals("lotterychenhsh")) {
				return AjaxUtil.createReturnValueError("超级管理员无法被锁定");
			}
		} else {
			return AjaxUtil.createReturnValueError("未找到该管理员信息！");
		}

		try {
			// 添加操作日志
			saveAdminRecordLog("Admin_loginLockOff", adminUser, null);

			Admin admin = new Admin();
			admin.setId(id);
			admin.setLoginLock(1);
			adminService.updateByPrimaryKeySelective(admin);
			admin = adminService.selectByPrimaryKey(id);
			
			// 踢出管理员
			MgrAdminOnlineOperater.kickOutAdmin(SystemPropeties.loginType + "." + admin.getBizSystem(), admin.getUsername());

		} catch (Exception e) {
			log.info("编辑管理员锁定失败！" + e);
			e.printStackTrace();
		}

		return AjaxUtil.createReturnValueSuccess();
	}

	/**
	 * 获取当前管理员数据
	 * 
	 * @return
	 */
	public Object[] getCurrentAdmin() {
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		return AjaxUtil.createReturnValueSuccess(currentAdmin.getUsername());
	}

	/**
	 * 修改管理员数据
	 * 
	 * @return
	 */
	public Object[] updatePassword(Admin admin) {
		if (admin == null) {
			return AjaxUtil.createReturnValueError("参数传递错误");
		}
		if (StringUtils.isEmpty(admin.getPassword())) {
			return AjaxUtil.createReturnValueError("旧密码不正确");
		}
		if (StringUtils.isEmpty(admin.getNewpassword())) {
			return AjaxUtil.createReturnValueError("请输入新密码");
		}
		if (StringUtils.isEmpty(admin.getSurepassword())) {
			return AjaxUtil.createReturnValueError("请输入确认新密码");
		}
		if (!admin.getNewpassword().equals(admin.getSurepassword())) {
			return AjaxUtil.createReturnValueError("新密码与确认密码不一致");
		}
		try {
			Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
			Admin oldAdmin = new Admin();
			oldAdmin.setId(currentAdmin.getId());
			oldAdmin.setUsername(currentAdmin.getUsername());
			oldAdmin.setPassword(MD5PasswordUtil.GetMD5Code(admin.getPassword()));
			List<Admin> loginAdmins = adminService.getAdminByCondition(oldAdmin);

			if (loginAdmins.size() == 0) {
				return AjaxUtil.createReturnValueError("原始密码不正确");
			} else {
				oldAdmin.setId(currentAdmin.getId());
				oldAdmin.setPassword(MD5PasswordUtil.GetMD5Code(admin.getNewpassword()));
				// 保存日志
				String operateDesc = AdminOperateUtil.constructMessage("admin_password_edit", currentAdmin.getUsername());
				String operationIp = BaseDwrUtil.getRequestIp();
				adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(),
						currentAdmin.getUsername(), oldAdmin.getId(), oldAdmin.getUsername(), EAdminOperateLogModel.ADMIN, operateDesc);

				adminService.updateByPrimaryKeySelective(oldAdmin);
				
				// 踢出管理员
				MgrAdminOnlineOperater.kickOutAdmin(SystemPropeties.loginType + "." + currentAdmin.getBizSystem(), currentAdmin.getUsername());
			}
		} catch (Exception e) {
			return AjaxUtil.createReturnValueError(e.getMessage());
		}

		return AjaxUtil.createReturnValueSuccess("操作成功");
	}

	/**
	 * 保存管理员操作的日志
	 * 
	 * @param key
	 * @param domain
	 * @throws Exception
	 */
	private void saveAdminRecordLog(String key, Admin admin, Integer id) throws Exception {
		try {
			if (admin == null) {
				admin = adminService.selectByPrimaryKey(id);
			}
			Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
			String adminState = "超级管理员";
			if (admin.getState() == 1) {
				adminState = "财务管理员";
			} else if (admin.getState() == 2) {
				adminState = "客服管理员";
			}
			String operateDesc = AdminOperateUtil.constructMessage(key, currentAdmin.getUsername(), admin.getUsername(),adminState);
			String operationIp = BaseDwrUtil.getRequestIp();
			adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), admin.getId(), admin.getUsername(), EAdminOperateLogModel.ADMIN, operateDesc);
		} catch (Exception e) {
			// 保存日志出现异常 不进行处理
			log.error("添加管理员，保存添加操作日志出错", e);
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * 根据id获取对象
	 * 
	 * @param id
	 * @return
	 */
	public Object[] getAdminById(Integer id) {
		if (id == null) {
			return AjaxUtil.createReturnValueError("参数传递错误");
		}
		Admin admin = adminService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(admin);
	}

	/**
	 * 查询管理员数据
	 * 
	 * @return
	 */
	public Object[] getAdminByCondition(Admin admin) {
		if (admin == null) {
			return AjaxUtil.createReturnValueError("参数传递错误");
		}
		List<Admin> loginAdmins = new ArrayList<Admin>();
		try {
			loginAdmins = adminService.getAdminByCondition(admin);
			if (loginAdmins != null && loginAdmins.size() == 0) {
				return AjaxUtil.createReturnValueError("该业务系统未设置相关的系统管理员");
			}
		} catch (Exception e) {
			return AjaxUtil.createReturnValueError(e.getMessage());
		}

		return AjaxUtil.createReturnValueSuccess(loginAdmins);
	}
}
