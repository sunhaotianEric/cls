package com.team.lottery.action.web.manage;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.LotteryCodeXyQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.BizSystemService;
import com.team.lottery.service.LotteryCodeXyService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.BizSystem;
import com.team.lottery.vo.LotteryCodeXy;

@Controller("managerLotteryCodeXyAction")
public class ManagerLotteryCodeXyAction {
	
	private static Logger log = LoggerFactory.getLogger(ManagerLotteryCodeXyAction.class);

	@Autowired
	private LotteryCodeXyService lotteryCodeXyService;
	
	@Autowired
	private BizSystemService bizSystemService;
	
    /**
     * 查找所有的奖金期号数据
     * @return
     */
    public Object[] getAllLotteryCodeXy(LotteryCodeXyQuery query,Page page){
    	if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	String bizSystem = BaseDwrUtil.getCurrentAdmin().getBizSystem();
    	if(!"SUPER_SYSTEM".equals(bizSystem)){
    		query.setBizSystem(bizSystem);
    	}
    	
    	page = lotteryCodeXyService.getAllLotteryCodeXyByQueryPage(query,page);
		return AjaxUtil.createReturnValueSuccess(page);
    }
    
    /**
     * 同步数据库到redis并查找所有的奖金期号数据
     * @return
     */
    public Object[] resetRedisLotteryCodeXy(LotteryCodeXyQuery query){
    	Boolean state = false;
    	if(query == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	if (StringUtils.isEmpty(query.getLotteryName())) {
    		return AjaxUtil.createReturnValueError("参数LotteryName为空.");
		}
    	String bizSystem = BaseDwrUtil.getCurrentAdmin().getBizSystem();
    	if(!"SUPER_SYSTEM".equals(bizSystem)){
    		query.setBizSystem(bizSystem);
    	}
    	String getBizSystem = query.getBizSystem();
    	//发生异常的业务bizSystems，返回到页面提示给用户
    	Set<String> exceptionBizSystems = new TreeSet<String>();
    	if (StringUtils.isEmpty(getBizSystem)) {//全部数据同步
    		List<BizSystem> bizSystems = bizSystemService.getAllEnableBizSystem();
    		for (BizSystem b : bizSystems) {
    			//实际数据中每天晚上会定期删除数据，for循环借接口获取数据不多
    			List<LotteryCodeXy> lotteryCodeXies = lotteryCodeXyService.getNearestSixHundredLotteryCode(query.getLotteryName(), b.getBizSystem());
    			state = lotteryCodeXyService.resetRedisLotteryCodeXy(lotteryCodeXies,b.getBizSystem(),query.getLotteryName());
    			if (state == false) {
//				return AjaxUtil.createReturnValueError(state);
    				exceptionBizSystems.add("("+b.getBizSystem()+")");
    			}
    		}
		}else {//同步getBizSystem单个业务系统的数据
			List<LotteryCodeXy> lotteryCodeXies = lotteryCodeXyService.getNearestSixHundredLotteryCode(query.getLotteryName(), getBizSystem);
			state = lotteryCodeXyService.resetRedisLotteryCodeXy(lotteryCodeXies,getBizSystem,query.getLotteryName());
			if (state == false) {
				exceptionBizSystems.add("("+getBizSystem+")");
			}
		}
    	if (CollectionUtils.isNotEmpty(exceptionBizSystems)) {
    		return AjaxUtil.createReturnValueError(exceptionBizSystems);
		}
    	return AjaxUtil.createReturnValueSuccess(exceptionBizSystems);
    }
    
    /**
     * 删除开奖数据
     * @return
     */
    public Object[] deleteLotteryCodeXyById(Integer lotteryCodeId){
    	//根据彩种获得最新期的开奖信息
		LotteryCodeXy lotteryCodeXy = lotteryCodeXyService.selectByPrimaryKey(lotteryCodeId);
		//删除数据库开奖记录
		if(lotteryCodeXy != null){
			int res = lotteryCodeXyService.deleteByPrimaryKey(lotteryCodeId);
			if(res > 0) {
				//联动redis删除
				boolean delRedisSuccess = lotteryCodeXyService.delRedisLotteryCode(lotteryCodeXy);
				if(!delRedisSuccess){
					AjaxUtil.createReturnValueError("数据库删除成功，redis数据删除失败！");
				}
			}
		}else{
			return AjaxUtil.createReturnValueError("未获得该开奖信息.");
		}
    	
		return AjaxUtil.createReturnValueSuccess();
    }
    
    
}
