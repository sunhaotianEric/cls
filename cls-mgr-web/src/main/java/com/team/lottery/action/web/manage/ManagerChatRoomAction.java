package com.team.lottery.action.web.manage;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.Page;
import com.team.lottery.service.ChatRoomService;
import com.team.lottery.service.UserService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.vo.ChatRoom;
import com.team.lottery.vo.User;

@Controller("managerChatRoomAction")
public class ManagerChatRoomAction {

	@Autowired
	private ChatRoomService chatRoomService;
	@Autowired
	private UserService userService;
	
	/**
	 * 分页查询
	 * @param chatRoom
	 * @param page
	 * @return
	 */
	public Object[] getChatRoom(ChatRoom chatRoom,Page page){
		if(chatRoom==null||page==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		page=chatRoomService.getChatRoom(chatRoom, page);
		return AjaxUtil.createReturnValueSuccess(page);
	}
	
	/**
	 * 查询单个聊天室数据
	 * @param chatRoom
	 * @return
	 */
	public Object[] getChatRoomById(ChatRoom chatRoom){
		if(chatRoom==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		chatRoom=chatRoomService.getChatRoomById(chatRoom.getId());
		User user=userService.getUserByName(chatRoom.getBizSystem(),chatRoom.getAgent());
		int i=0;
		if(user.getDailiLevel().equals("SUPERAGENCY")){
			i=1;
		}
		return AjaxUtil.createReturnValueSuccess(chatRoom,i);
	}
	
	/**
	 * 保存聊天室数据
	 * @param chatRoom
	 * @return
	 */
	public Object[] saveChatRoom(ChatRoom chatRoom){
		if(chatRoom==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		User user=userService.getUserByName(chatRoom.getBizSystem(),chatRoom.getAgent());
		if(!user.getDailiLevel().equals("SUPERAGENCY")){
			//查询该业务系统是否已开通超级代理聊天室
			ChatRoom chat=new ChatRoom();
			chat.setBizSystem(chatRoom.getBizSystem());
			chat=chatRoomService.getChatRoomOnUser(chat,0,null);
			if(chat==null){
				return AjaxUtil.createReturnValueError("请先开通超级代理的聊天室");
			}
		}
		//验证房间归属人级别
		String dailiLevel=user.getDailiLevel();
		if(dailiLevel.equals("ORDINARYAGENCY")||dailiLevel.equals("REGULARMEMBERS")){
			return AjaxUtil.createReturnValueError("聊天室归属代理不能为普通代理或普通会员");
		}
		//验证同业务系统是否存在一个代理用户开多个房间
		ChatRoom room=new ChatRoom();
		room.setBizSystem(chatRoom.getBizSystem());
		room.setAgent(chatRoom.getAgent());
		if(chatRoomService.getChatRoomByRoomId(room).size()>0){
			return AjaxUtil.createReturnValueError("该归属代理已存在");
		}
		//验证房间号是否唯一
		List<ChatRoom> ChatRoomlist=chatRoomService.getChatRoomByRoomId(chatRoom);
		if(ChatRoomlist.size()>0){
			return AjaxUtil.createReturnValueError("房间号已存在");
		}else{
			chatRoomService.saveChatRoom(chatRoom);
		}
		return AjaxUtil.createReturnValueSuccess("保存成功");
	}
	
	/**
	 * 根据ID修改聊天室
	 * @param chatRoom
	 * @return
	 */
	public Object[] updateChatRoomByid(ChatRoom chatRoom){
		if(chatRoom==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		//验证房间归属人级别
		User user=userService.getUserByName(chatRoom.getBizSystem(),chatRoom.getAgent());
		String dailiLevel=user.getDailiLevel();
		if(dailiLevel.equals("ORDINARYAGENCY")||dailiLevel.equals("REGULARMEMBERS")){
			return AjaxUtil.createReturnValueError("房间归属代理不能为普通代理或普通会员");
		}
		//验证同业务系统是否存在一个代理用户开多个房间
		ChatRoom room=new ChatRoom();
		room.setBizSystem(chatRoom.getBizSystem());
		room.setAgent(chatRoom.getAgent());
		room.setId(chatRoom.getId());
		if(chatRoomService.getChatRoomByRoomId(room).size()>0){
			return AjaxUtil.createReturnValueError("该归属代理已存在");
		}
		//验证房间号是否唯一
		List<ChatRoom> ChatRoomlist=chatRoomService.getChatRoomByRoomId(chatRoom);
		if(ChatRoomlist.size()>0){
			return AjaxUtil.createReturnValueError("房间号已存在");
		}else{
			chatRoomService.updateChatRoomByid(chatRoom);
		}
		return AjaxUtil.createReturnValueSuccess("修改成功");
	}
	
	/**
	 * 根据ID删除聊天室
	 * @param id
	 * @return
	 */
	public Object[] delChatRoom(Long id){
		if(id==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		ChatRoom chat=chatRoomService.getChatRoomById(id);
		//查询该业务系统是否已开通超级代理聊天室
		ChatRoom chatRoom=new ChatRoom();
		chatRoom.setBizSystem(chat.getBizSystem());
		chatRoom=chatRoomService.getChatRoomOnUser(chatRoom,0,null);
		if(chatRoom.getAgent().equals(chat.getAgent())){
			return AjaxUtil.createReturnValueError("超级代理聊天室禁止删除");
		}
		chatRoomService.delChatRoom(id);
		return AjaxUtil.createReturnValueSuccess();
	}
	
}
