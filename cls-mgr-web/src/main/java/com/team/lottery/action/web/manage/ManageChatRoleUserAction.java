package com.team.lottery.action.web.manage;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.Page;
import com.team.lottery.service.ChatRoleUserService;
import com.team.lottery.service.UserService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.vo.ChatRoleUser;
import com.team.lottery.vo.User;

@Controller("manageChatRoleUserAction")
public class ManageChatRoleUserAction {
	
	@Autowired
	private ChatRoleUserService chatRoleUserService;
	@Autowired
	private UserService userService;
	
	/**
	 * 分页查询
	 * @param query
	 * @param page
	 * @return
	 */
	public Object[] getAllChatRoleUserPage(ChatRoleUser query,Page page){
		if(query==null||page==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		page=chatRoleUserService.getAllChatRoleUser(query, page);
		return AjaxUtil.createReturnValueSuccess(page);
	}
	
	/**
	 * 根据ID查询
	 * @param id
	 * @return
	 */
	public Object[] selectChatRoleUser(Integer id){
		ChatRoleUser record=chatRoleUserService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(record);
	}
	
	/**
	 * 修改
	 * @param record
	 * @return
	 */
	public Object[] updateChatRoleUser(ChatRoleUser record){
		if(record==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		chatRoleUserService.updateByPrimaryKeySelective(record);
		return AjaxUtil.createReturnValueSuccess();
	}
	
	/**
	 * 根据ID删除
	 * @param id
	 * @return
	 */
	public Object[] delChatRoleUser(Integer id){
		if(id==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		chatRoleUserService.deleteByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess();
	}
	
	/**
	 * 保存
	 * @param record
	 * @return
	 */
	public Object[] saveChatRoleUser(ChatRoleUser record){
    	if(record == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	Integer id = record.getId();
    	if(id == null){
    		String bizSystem = record.getBizSystem();
    		String username = record.getUserName();
    		User user = userService.getUserByName(bizSystem, username);
    		int userid = user.getId();
    		record.setUserId(userid);
    		record.setCreateDate(new Date());
    		chatRoleUserService.insertSelective(record);
    	}else{
    		record.setUpdateDate(new Date());
    		chatRoleUserService.updateByPrimaryKeySelective(record);
    	}
    	return AjaxUtil.createReturnValueSuccess();
    }

}
