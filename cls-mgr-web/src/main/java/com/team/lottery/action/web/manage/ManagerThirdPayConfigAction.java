package com.team.lottery.action.web.manage;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.enums.EAdminOperateLogModel;
import com.team.lottery.service.AdminOperateLogService;
import com.team.lottery.service.ChargePayService;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.util.AdminOperateUtil;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.ChargePay;
import com.team.lottery.vo.ThirdPayConfig;

@Controller("managerThirdPayConfigAction")
public class ManagerThirdPayConfigAction {

	private static Logger log = LoggerFactory.getLogger(ManagerThirdPayConfigAction.class);
	
	@Autowired
	private ThirdPayConfigService thirdPayConfigService;
	@Autowired
	private AdminOperateLogService adminOperateLogService;
	@Autowired
	private ChargePayService chargePayService;
	
	/**
     * 查找所有第三方支付设置
     * @return
     */
    public Object[] getAllThirdPayConfig(){
		List<ThirdPayConfig> thirdPayConfig =  thirdPayConfigService.getThirdPayConfig();
		return AjaxUtil.createReturnValueSuccess(thirdPayConfig);
    }
    
    /**
     * 删除指定的第三方支付设置
     * @return
     */
    public Object[] delThirdPayConfig(Long id){
    	ThirdPayConfig thirdPayConfig = thirdPayConfigService.selectByPrimaryKey(id);
    	
    	//删除之前先查询是否还存在其他系统存在该快捷支付
		List<ChargePay> canPayQuicksByType = chargePayService.selectByChargeType(thirdPayConfig.getChargeType());
		if (canPayQuicksByType.size()!=0) {
			return AjaxUtil.createReturnValueError("删除失败,其它系统还存在该快捷支付");
		}else{
			try {
				Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
	    		//EFundThirdPayType fundThirdPayType = EFundThirdPayType.valueOf(thirdPayConfig.getChargeType());
				String operateDesc = AdminOperateUtil.constructMessage("thirdPayConfig_del", currentAdmin.getUsername(),thirdPayConfig.getChargeDes());
				String operationIp = BaseDwrUtil.getRequestIp();
				adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.CHARGE_PAY, operateDesc);
				thirdPayConfigService.deleteByPrimaryKey(id);
		    	
			} catch (Exception e) {
				//保存日志出现异常 不进行处理
				log.error("删除第三方支付设置，保存添加操作日志出错", e);
			}
			return AjaxUtil.createReturnValueSuccess("删除成功");
		}
    }
    
    /**
     * 保存第三方支付设置
     * @return
     */
    @Transactional
    public Object[] saveOrUpdateThirdPayConfig(ThirdPayConfig thirdPayConfig){
    	try{
    		Long id = thirdPayConfig.getId();
    		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
        	if(id==null){
        		thirdPayConfig.setCreateTime(new Date());
        		thirdPayConfig.setCreateAdmin(currentAdmin.getUsername());
        		//保存操作日志
            	//EFundThirdPayType fundThirdPayType = EFundThirdPayType.valueOf(thirdPayConfig.getChargeType());
            	Integer enabled = thirdPayConfig.getEnabled();
            	String enabledStr="";
            	if(enabled == 1){
            		enabledStr = "启用";
            	}else{
            		enabledStr = "关闭";
            	}
        		String operateDesc = AdminOperateUtil.constructMessage("thirdPayConfig_add", currentAdmin.getUsername(),thirdPayConfig.getChargeDes(),enabledStr);
        		String operationIp = BaseDwrUtil.getRequestIp();
        		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.CHARGE_PAY, operateDesc);
        	    String chargeType=thirdPayConfig.getChargeType();
        	    List<ThirdPayConfig>  thirdPayConfigList=thirdPayConfigService.queryThirdPayConfig(thirdPayConfig);
        	    if(thirdPayConfigList!=null && thirdPayConfigList.size()>0)
        	    {
        	    	return AjaxUtil.createReturnValueError(thirdPayConfig.getChargeDes()+"已经设置第三方支付相关参数;不能重复设置!");
        	    }
        		thirdPayConfigService.insertSelective(thirdPayConfig);
        	}else{
        		//保存操作日志
            	//EFundThirdPayType fundThirdPayType = EFundThirdPayType.valueOf(thirdPayConfig.getChargeType());
            	Integer enabled = thirdPayConfig.getEnabled();
            	String enabledStr="";
            	if(enabled == 1){
            		enabledStr = "启用";
            	}else{
            		enabledStr = "关闭";
            	}
        		String operateDesc = AdminOperateUtil.constructMessage("thirdPayConfig_edit", currentAdmin.getUsername(),thirdPayConfig.getChargeDes(),enabledStr);
        		String operationIp = BaseDwrUtil.getRequestIp();
        		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.CHARGE_PAY, operateDesc);
        		
        		thirdPayConfig.setUpdateTime(new Date());
        		thirdPayConfig.setUpdateAdmin(currentAdmin.getUsername());
        		
        		thirdPayConfigService.updateByPrimaryKeySelectiveAndChargeType(thirdPayConfig);
        		
        		//thirdPayConfigService.updateByPrimaryKeySelective(thirdPayConfig);
        		//chargePayService.updateByChargeType(thirdPayConfig);
        	}
    		return AjaxUtil.createReturnValueSuccess();
    	}catch(Exception e){
    		return AjaxUtil.createReturnValueError(e.getMessage());
    	}
    	
    }
    
    /**
     * 查找指定的第三方支付设置
     * @return
     */
    public Object[] getThirdPayConfigById(Long id){
    	ThirdPayConfig thirdPayConfig=null;
    	try {
    		 thirdPayConfig =  thirdPayConfigService.selectByPrimaryKey(id);
		} catch (Exception e) {
			System.out.print(e);
		}
    	
		return AjaxUtil.createReturnValueSuccess(thirdPayConfig);
    }
    
    /**
     * 开启启用状态
     * @return
     */
    public Object[] openThirdPayConfig(Long id){
    	//保存操作日志
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	ThirdPayConfig thirdPayConfig = thirdPayConfigService.selectByPrimaryKey(id);
    	//EFundThirdPayType fundThirdPayType = EFundThirdPayType.valueOf(thirdPayConfig.getChargeType());
    	String operateDesc = AdminOperateUtil.constructMessage("ThirdPayConfig_open", currentAdmin.getUsername(),thirdPayConfig.getChargeDes());
    	String operationIp = BaseDwrUtil.getRequestIp();
    	adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.CHARGE_PAY, operateDesc);
    	thirdPayConfigService.updateThirdPayConfigEnabled(id, 1);
    	return AjaxUtil.createReturnValueSuccess();
    }
    
    /**
     * 关闭启用状态
     * @return
     */
    public Object[] closeThirdPayConfig(Long id){
    	//保存操作日志
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	ThirdPayConfig thirdPayConfig = thirdPayConfigService.selectByPrimaryKey(id);
    	//EFundThirdPayType fundThirdPayType = EFundThirdPayType.valueOf(thirdPayConfig.getChargeType());
    	String operateDesc = AdminOperateUtil.constructMessage("ThirdPayConfig_close", currentAdmin.getUsername(),thirdPayConfig.getChargeDes());
    	String operationIp = BaseDwrUtil.getRequestIp();
    	adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.CHARGE_PAY, operateDesc);
    	thirdPayConfigService.updateThirdPayConfigEnabled(id, 0);
    	return AjaxUtil.createReturnValueSuccess();
    }
    
}
