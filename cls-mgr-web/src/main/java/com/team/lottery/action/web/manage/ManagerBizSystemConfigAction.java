package com.team.lottery.action.web.manage;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;

import com.team.lottery.enums.EAdminOperateLogModel;
import com.team.lottery.enums.ERedisChannel;
import com.team.lottery.extvo.LotterySetMsg;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.AdminOperateLogService;
import com.team.lottery.service.BizSystemConfigService;
import com.team.lottery.system.message.MessageQueueCenter;
import com.team.lottery.system.message.messagetype.BizSystemConfigMessage;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.BizSystemConfig;

@Controller("managerBizSystemConfigAction")
public class ManagerBizSystemConfigAction {
	private static Logger log = LoggerFactory.getLogger(ManagerBizSystemConfigAction.class);
	@Autowired
	private BizSystemConfigService bizSystemConfigService;
	@Autowired
	private AdminOperateLogService adminOperateLogService;
    /**
     * 查找网站系统参数配置
     * @return
     */
    public Object[] getAllBizSystemConfig(BizSystemConfig query,Page page){
    	if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	if(!"SUPER_SYSTEM".equals(currentAdmin.getBizSystem()))
    	{
    		query.setBizSystem(currentAdmin.getBizSystem());
    	}
    	page=bizSystemConfigService.getAllBizSystemConfigByQueryPage(query, page);
		return AjaxUtil.createReturnValueSuccess(page);
    }

    
    /**
     * 删除
     * @return
     */
    public Object[] delBizSystemConfigById(Long id){
    	BizSystemConfig bizSystemConfig=null;
    	try{
    	 bizSystemConfig=bizSystemConfigService.selectByPrimaryKey(id);
    	 int result=bizSystemConfigService.deleteByPrimaryKey(id);
    	 if(result<=0){
           return AjaxUtil.createReturnValueError("删除失败！");
         }
    	}catch(Exception e){
        	return AjaxUtil.createReturnValueError(e.getMessage());
    	}
      	//通知系统刷新缓存
		try {
			BizSystemConfigMessage bizSystemConfigMessage=new BizSystemConfigMessage();
			bizSystemConfigMessage.setBizSystemConfig(bizSystemConfig);
			MessageQueueCenter.putRedisMessage(bizSystemConfigMessage, ERedisChannel.CLSCACHE.name());
		} catch (Exception e) {
			AjaxUtil.createReturnValueError(e.getMessage());
		}
        return AjaxUtil.createReturnValueSuccess();
    }
    
    /**
     * 新增和修改
     * @param systemConfig
     * @return
     */
    public Object[] saveOrUpdateBizSystenConfig(BizSystemConfig bizSystemConfig)
    {
    	try{
    	if(bizSystemConfig == null){
    		return AjaxUtil.createReturnValueError("参数传递错误");
    	}
    	if(bizSystemConfig.getId()!=null&&!"".equals(bizSystemConfig.getId()))
    	{
    		return this.updateBizSystemConfig(bizSystemConfig);
    	}else{
    		return this.addBizSystemConfig(bizSystemConfig);
    	}
    	}catch(Exception e){
        	return AjaxUtil.createReturnValueError(e.getMessage());
    	}
    }
    
    /**
     * 新增
     * @param systemConfig
     * @return
     */
    public Object[] addBizSystemConfig(BizSystemConfig bizsystemConfig)
    {
    	if(bizsystemConfig == null){
    		return AjaxUtil.createReturnValueError("参数传递错误");
    	}
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	bizsystemConfig.setCreateDate(new Date());
    	bizsystemConfig.setUpdateDate(new Date());
    	bizsystemConfig.setUpdateAdmin(currentAdmin.getUsername());;
        int result=bizSystemConfigService.insertSelective(bizsystemConfig);
        if(result>0){
         	//通知系统刷新缓存
    		try {
    			BizSystemConfigMessage bizSystemConfigMessage=new BizSystemConfigMessage();
    			bizSystemConfigMessage.setBizSystemConfig(bizsystemConfig);
    			MessageQueueCenter.putRedisMessage(bizSystemConfigMessage, ERedisChannel.CLSCACHE.name());
    		} catch (Exception e) {
    			AjaxUtil.createReturnValueError(e.getMessage());
    		}
          return AjaxUtil.createReturnValueSuccess();
        }else if(result==-2){
         return AjaxUtil.createReturnValueError("参数名已经存在，请重新添加");
        }else{
          return AjaxUtil.createReturnValueError("，添加失败，请重新添加");
        }
		//return AjaxUtil.createReturnValueSuccess();
    }
    /**
     * 修改
     * @param systemConfig
     * @return
     */
    public Object[] updateBizSystemConfig(BizSystemConfig bizSystemConfig)
    {
    	if(bizSystemConfig == null){
    		return AjaxUtil.createReturnValueError("参数传递错误");
    	}
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	bizSystemConfig.setUpdateDate(new Date());
    	bizSystemConfig.setUpdateAdmin(currentAdmin.getUsername());;
    	int result=bizSystemConfigService.updateByPrimaryKeySelective(bizSystemConfig);
    	 if(result>0){
    		 	//通知系统刷新缓存
     		try {
     			BizSystemConfigMessage bizSystemConfigMessage=new BizSystemConfigMessage();
     			bizSystemConfigMessage.setBizSystemConfig(bizSystemConfig);
     			MessageQueueCenter.putRedisMessage(bizSystemConfigMessage, ERedisChannel.CLSCACHE.name());
     		} catch (Exception e) {
     			AjaxUtil.createReturnValueError(e.getMessage());
     		}
    		 return AjaxUtil.createReturnValueSuccess();
         }else if(result==-2){
        	 return AjaxUtil.createReturnValueError("参数名已经存在，请重新添加");
         }else{
        	 return AjaxUtil.createReturnValueError("，添加失败，请重新添加");
         }
    }
    /**
     * 根据id获取对象
     * @param id
     * @return
     */
    public Object[] getBizSystemConfigById(Long id)
    {
    	if(id == null){
    		return AjaxUtil.createReturnValueError("参数传递错误");
    	}
    	
    	BizSystemConfig bizSystemConfig=bizSystemConfigService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(bizSystemConfig);
    }
    
    
    
    /**
     * 提交更新彩种配置信息 
     * @return
     */
    public Object[] updateLotterySetting(LotterySetMsg lotterySetMsg){
     	if(lotterySetMsg == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
     	Admin admin=BaseDwrUtil.getCurrentAdmin();
     	bizSystemConfigService.saveOrUpdateLotterySetConfig(lotterySetMsg,admin.getBizSystem());
    	
    	//通知前端系统刷新缓存
		try {
			BizSystemConfigMessage bizSystemConfigMessage=new BizSystemConfigMessage();
			BizSystemConfig bizSystemConfig=new BizSystemConfig();
			bizSystemConfig.setBizSystem(admin.getBizSystem());
 			bizSystemConfigMessage.setBizSystemConfig(bizSystemConfig);
 			MessageQueueCenter.putRedisMessage(bizSystemConfigMessage, ERedisChannel.CLSCACHE.name());
		} catch (Exception e) {
			AjaxUtil.createReturnValueError(e.getMessage());
		}
    	
		return AjaxUtil.createReturnValueSuccess();
    }
    
    /**
     * 获取彩种配置信息 
     * @return
     */
    public Object[] getLotterySetting(){
    	Admin admin=BaseDwrUtil.getCurrentAdmin();
    	Map<String,String> configMap=bizSystemConfigService.getBizSystemConfigByLikeValue("is%Open",admin.getBizSystem());
    	
        if(configMap.size() ==0){
    		return AjaxUtil.createReturnValueError("当前不存在系统配置信息的记录.");
        }
      
        LotterySetMsg lotterySetMsg = new LotterySetMsg();
        try {
			BeanUtils.populate(lotterySetMsg, configMap);
		} catch (IllegalAccessException e) {
			return AjaxUtil.createReturnValueError("系统出错，请联系管理员！"+e.getMessage());
		} catch (InvocationTargetException e) {
			return AjaxUtil.createReturnValueError("系统出错，请联系管理员！"+e.getMessage());
		}
 	    
        
		return AjaxUtil.createReturnValueSuccess(lotterySetMsg);
    }
    
    /**
     * 提交更新彩种配置信息  根据参数传递
     * @return
     */
    public Object[] updateLotterySettingByBizSystem(LotterySetMsg lotterySetMsg,String bizSystem){
     	if(lotterySetMsg == null || bizSystem == null || "".equals(bizSystem)){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
     	bizSystemConfigService.saveOrUpdateLotterySetConfig(lotterySetMsg,bizSystem);
    	
    	//通知前端系统刷新缓存
		try {
			BizSystemConfigMessage bizSystemConfigMessage=new BizSystemConfigMessage();
			BizSystemConfig bizSystemConfig=new BizSystemConfig();
			bizSystemConfig.setBizSystem(bizSystem);
 			bizSystemConfigMessage.setBizSystemConfig(bizSystemConfig);
 			MessageQueueCenter.putRedisMessage(bizSystemConfigMessage, ERedisChannel.CLSCACHE.name());
		} catch (Exception e) {
			AjaxUtil.createReturnValueError(e.getMessage());
		}
    	
		return AjaxUtil.createReturnValueSuccess();
    }
    
    
    
    /**
     * 获取彩种配置信息 根据参数传递
     * @return
     */
    public Object[] getLotterySettingByBizSystem(String bizSystem){
    	if("".equals(bizSystem) || bizSystem == null){
    		return AjaxUtil.createReturnValueError("请选择业务系统！");
    	}
    	Map<String,String> configMap=bizSystemConfigService.getBizSystemConfigByLikeValue("is%Open",bizSystem);
    	
        if(configMap.size() ==0){
    		return AjaxUtil.createReturnValueError("当前不存在系统配置信息的记录.");
        }
      
        LotterySetMsg lotterySetMsg = new LotterySetMsg();
        try {
			BeanUtils.populate(lotterySetMsg, configMap);
		} catch (IllegalAccessException e) {
			return AjaxUtil.createReturnValueError("系统出错，请联系管理员！"+e.getMessage());
		} catch (InvocationTargetException e) {
			return AjaxUtil.createReturnValueError("系统出错，请联系管理员！"+e.getMessage());
		}
 	    
        
		return AjaxUtil.createReturnValueSuccess(lotterySetMsg);
    }
    
    
    /**
     * 根据bizsystem查询
     * @param bizSystem
     * @return
     */
    public Object[] getBizSystemConfigByQuery(String bizSystem){
    	if(bizSystem == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	
    	String currentBizSystem = BaseDwrUtil.getCurrentAdmin().getBizSystem();
    	List<BizSystemConfig> bizSystemConfigByQuery = null;
    	if(!currentBizSystem.equals("SUPER_SYSTEM") || "".equals(bizSystem)){
    		bizSystemConfigByQuery = bizSystemConfigService.getBizSystemConfigByQuery(currentBizSystem);
    	}else{
    		bizSystemConfigByQuery = bizSystemConfigService.getBizSystemConfigByQuery(bizSystem);
    	}
    	if(CollectionUtils.isEmpty(bizSystemConfigByQuery)){
    		 return AjaxUtil.createReturnValueError("暂未搜索到系统配置！");
    	}
    	return AjaxUtil.createReturnValueSuccess(bizSystemConfigByQuery);
    }
    
    /**
     * 修改系统默认值
     * @param map
     * @return
     */
    public Object[] updateDefaultSystemConfig(Map<String, String> map,String bizSystem){
    	if(map.isEmpty()){
    		return AjaxUtil.createReturnValueError("参数传递错误");
    	}
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	
    	if(!currentAdmin.getBizSystem().equals("SUPER_SYSTEM")){
    		bizSystem = currentAdmin.getBizSystem();
    	}else if(bizSystem.equals("") || bizSystem == null){
    		return AjaxUtil.createReturnValueError("参数传递错误");
    	}
    	
    	BizSystemConfig bizSystemConfig = new  BizSystemConfig();
    	for(String m: map.keySet()){
    		if(m == null){
    			log.info("修改系统默认值,参数不存在跳过！");
    			continue;
    		}
    		log.debug("新增修改保存key : [{}], value : [{}]", m, map.get(m));
    		BizSystemConfig config = bizSystemConfigService.getBizSystemConfigByKey(m,bizSystem);
    		String operateDesc = "";
    		String operationIp = BaseDwrUtil.getRequestIp();
    		if(config != null){
    			//只更新重新设置的值
    			if(!config.getConfigValue().equals(map.get(m))){
    				log.info("改变值key : [{}], value 从[{}]改为[{}]", m, config.getConfigValue(), map.get(m));
    				operateDesc ="改变值key : ["+m+"], value 从["+config.getConfigValue()+"]改为["+map.get(m)+"]";
    				adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.BIZ_SYSTEM_CONFIG, operateDesc);
    				bizSystemConfig.setId(config.getId());
    				bizSystemConfig.setConfigKey(config.getConfigKey());
    				bizSystemConfig.setConfigValue(map.get(m)==null?"":map.get(m).trim());
    				//进入可提现余额投注额倍数必须大于0
    				if("withDrawLotteryMultiple".equals(bizSystemConfig.getConfigKey())){
    					if(Float.parseFloat(bizSystemConfig.getConfigValue()) < 0){
    						return AjaxUtil.createReturnValueError("进入可提现余额投注额倍数不合法！");
    					}
    				}
    				bizSystemConfig.setUpdateDate(new Date());
    				bizSystemConfig.setUpdateAdmin(currentAdmin.getUsername());
    				bizSystemConfigService.updateByPrimaryKeySelective(bizSystemConfig);
    			}
    		}else{
    			log.info("新增值key : [{}], value : [{}]", m, map.get(m));
    			operateDesc ="新增值key : ["+m+"], value : ["+map.get(m)+"]";
    			adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.BIZ_SYSTEM_CONFIG, operateDesc);
				//新增系统配置默认值
				bizSystemConfig.setConfigKey(m);
		    	bizSystemConfig.setConfigValue(map.get(m));
		    	bizSystemConfig.setBizSystem(bizSystem);
		    	bizSystemConfig.setEnable(1);
		    	bizSystemConfig.setCreateDate(new Date());
		    	bizSystemConfig.setUpdateAdmin(currentAdmin.getUsername());
		    	bizSystemConfigService.insertSelective(bizSystemConfig);
		 	
			}
    	 } 
    	//通知系统刷新缓存
    	try {
    		BizSystemConfigMessage bizSystemConfigMessage=new BizSystemConfigMessage();
    		BizSystemConfig systemConfig=new BizSystemConfig();
    		systemConfig.setBizSystem(bizSystem);
    		bizSystemConfigMessage.setBizSystemConfig(systemConfig);
    		MessageQueueCenter.putRedisMessage(bizSystemConfigMessage, ERedisChannel.CLSCACHE.name());
    	} catch (Exception e) {
    		AjaxUtil.createReturnValueError(e.getMessage());
    	}
    	return AjaxUtil.createReturnValueSuccess();
    }
    
    /**
     * 一键刷新
     * @return
     */
    public Object[] refreshCache(String bizSystem){
    	//通知系统刷新缓存
		try {
			if(bizSystem == null || "".equals(bizSystem)){
				AjaxUtil.createReturnValueError("请选择要刷新的业务系统");
			}
			BizSystemConfig biz = new BizSystemConfig();
			biz.setBizSystem(bizSystem);
			BizSystemConfigMessage bizSystemConfigMessage=new BizSystemConfigMessage();
			bizSystemConfigMessage.setType(ERedisChannel.CLSCACHE.name());
			bizSystemConfigMessage.setBizSystemConfig(biz);
			MessageQueueCenter.putMessage(bizSystemConfigMessage);

		} catch (Exception e) {
			AjaxUtil.createReturnValueError(e.getMessage());
		}
	 return AjaxUtil.createReturnValueSuccess();
    }
    
}
