package com.team.lottery.action.web.manage;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.enums.EAdminOperateLogModel;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.AdminOperateLogService;
import com.team.lottery.service.QuickBankTypeService;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.util.AdminOperateUtil;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.QuickBankType;
import com.team.lottery.vo.ThirdPayConfig;

@Controller("managerQuickBankTypeAction")
public class ManagerQuickBankTypeAction {

	private static Logger log = LoggerFactory.getLogger(ManagerQuickBankTypeAction.class);

	@Autowired
	private AdminOperateLogService adminOperateLogService;
	
	@Autowired
	private QuickBankTypeService quickBankTypeService;
	
	@Autowired
	private ThirdPayConfigService thirdPayConfigService;

  

    
    /**
     * 开启第三方支付银行代码配置
     * @return
     */
    public Object[] openQuickBankType(Long id){
    	//保存操作日志
    	saveQuickBankTypeLog("quickBankType_open", null, id);
    	quickBankTypeService.updateQuickBankTypeEnabled(id, 1);
    	return AjaxUtil.createReturnValueSuccess();
    }
    
    /**
     * 关闭第三方支付银行代码配置
     * @return
     */
    public Object[] closeQuickBankType(Long id){
    	//保存操作日志
    	saveQuickBankTypeLog("quickBankType_close", null, id);
    	quickBankTypeService.updateQuickBankTypeEnabled(id, 0);
    	return AjaxUtil.createReturnValueSuccess();
    }
    
    /**
     * 保存第三方支付银行代码操作的日志
     * @param key
     * @param domain
     */
    private void saveQuickBankTypeLog(String key, QuickBankType rechargeConfig, Long id) {
    	try {
    		if(rechargeConfig == null) {
    			rechargeConfig = quickBankTypeService.selectByPrimaryKey(id);
    		}
    		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    		String operateDesc = AdminOperateUtil.constructMessage(key, currentAdmin.getUsername(), rechargeConfig.getId().toString());
    		String operationIp = BaseDwrUtil.getRequestIp();
    		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.RECHARGECONFIG_MANAGE, operateDesc);
    	} catch (Exception e) {
    		//保存日志出现异常 不进行处理
    		log.error("修改第三方支付银行代码，保存添加操作日志出错", e);
    	}
    }
    /**
     * 删除指定的第三方支付银行代码
     * @return
     */
    public Object[] delQuickBankType(Long id){
    	try {
    		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    		QuickBankType quickBankType = quickBankTypeService.selectByPrimaryKey(id);
    		//EFundThirdPayType fundThirdPayType = EFundThirdPayType.valueOf(quickBankType.getChargeType());
    		ThirdPayConfig thirdPayConfig=thirdPayConfigService.thirdPayConfigByChargeType(quickBankType.getChargeType());
    		
			String operateDesc = AdminOperateUtil.constructMessage("quickBankType_del", currentAdmin.getUsername(), thirdPayConfig.getChargeDes());
			String operationIp = BaseDwrUtil.getRequestIp();
			adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.RECHARGECONFIG_MANAGE, operateDesc);
		} catch (Exception e) {
			//保存日志出现异常 不进行处理
			log.error("删除第三方支付银行代码，保存添加操作日志出错", e);
		}
    	quickBankTypeService.deleteByPrimaryKey(id);
    	//通知前端应用刷新缓存就够了
    	/*try {
			NotifyFrontInterface.notifyFrontRefreshCache(EFrontCacheType.FRONT_INDEX_CACHE_INFO.getCode());
		} catch (Exception e) {
			log.error(e.getMessage());
			AjaxUtil.createReturnValueError(e.getMessage());
		}*/
    	return AjaxUtil.createReturnValueSuccess();
    }
    /**
     * 保存
     * @return
     */
    public Object[] saveOrUpdateQuickBankType(QuickBankType quickBankType){
    	try{
    		Long id = quickBankType.getId();
    		
    		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    		
    		//EFundThirdPayType fundThirdPayType = EFundThirdPayType.valueOf(quickBankType.getChargeType());
    		ThirdPayConfig thirdPayConfig=thirdPayConfigService.thirdPayConfigByChargeType(quickBankType.getChargeType());
    		
    		Integer enabled = quickBankType.getEnabled();
    		String enabledStr ="";
    		if(enabled == 1){
    			enabledStr = "启用";
    		}else{
    			enabledStr = "关闭";
    		}
    		String operationIp = BaseDwrUtil.getRequestIp();	
        	if(id==null){
        		try {
        		String operateDesc = AdminOperateUtil.constructMessage("quickBankType_add", currentAdmin.getUsername(),thirdPayConfig.getChargeDes(),quickBankType.getBankName(),enabledStr);
        		
        		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.RECHARGECONFIG_MANAGE, operateDesc);
        		} catch (Exception e) {
        			//保存日志出现异常 不进行处理
        			log.error("新增第三方支付银行代码，保存添加操作日志出错", e);
        		}
        		quickBankTypeService.insert(quickBankType);
        	}else{
        		try {
        			String operateDesc = AdminOperateUtil.constructMessage("quickBankType_edit", currentAdmin.getUsername(),thirdPayConfig.getChargeDes(),enabledStr);
        			adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.RECHARGECONFIG_MANAGE, operateDesc);
        		} catch (Exception e) {
        			//保存日志出现异常 不进行处理
        			log.error("编辑第三方支付银行代码，保存添加操作日志出错", e);
        		}
        		
        		quickBankTypeService.updateByPrimaryKeySelective(quickBankType);
        	}
        	/*//通知前端应用刷新缓存就够了
        	try {
    			NotifyFrontInterface.notifyFrontRefreshCache(EFrontCacheType.FRONT_INDEX_CACHE_INFO.getCode());
    		} catch (Exception e) {
    			log.error(e.getMessage());
    			AjaxUtil.createReturnValueError(e.getMessage());
    		}*/
    		return AjaxUtil.createReturnValueSuccess();
    	}catch(Exception e){
    		return AjaxUtil.createReturnValueError(e.getMessage());
    	}
    	
    }
    /**
     * 查找指定的第三方支付银行代码
     * @return
     */
    public Object[] getQuickBankTypeById(Long id){
    	QuickBankType rechargeConfig =  quickBankTypeService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(rechargeConfig);
    }
   
    /**
     * 查询
     * @return
     */
    public Object[] getAllQuickBankType(QuickBankType query,Page page){
    	if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	
    	
    	page=quickBankTypeService.getAllQuickBankTypeQueryPage(query, page);
		return AjaxUtil.createReturnValueSuccess(page);
    }
}
