package com.team.lottery.action.web.manage;


import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.enums.EProstateStatus;
import com.team.lottery.extvo.LotteryDayGainQuery;
import com.team.lottery.extvo.OrderQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.BizSystemService;
import com.team.lottery.service.LotteryDayGainService;
import com.team.lottery.service.OrderService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ClsDateHelper;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.DateUtil;
import com.team.lottery.vo.BizSystem;
import com.team.lottery.vo.LotteryDayGain;
import com.team.lottery.vo.Order;

@Controller("managerLotteryDayGainAction")
public class ManagerLotteryDayGainAction {
	@Autowired
	private LotteryDayGainService lotteryDayGainService;
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private BizSystemService bizSystemService;
	
	public Object getLotteryDayGainPage(LotteryDayGainQuery query, Page page) throws Exception {
		if (query == null || page == null) {
			return AjaxUtil.createReturnValueError("参数错误！");
		}

		Date startDateTime = DateUtil.getNowStartTimeByStart(query.getBelongDateStart());
		Date endDateTime = DateUtil.getNowStartTimeByEnd(query.getBelongDateEnd());
		query.setBelongDateStart(startDateTime);
		query.setBelongDateEnd(endDateTime);

		Date reportLastTime = ClsDateHelper.getClsReportLastTime(new Date());
		Date nowDate = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String strDate = df.format(nowDate);

		String bizSystem = BaseDwrUtil.getCurrentAdmin().getBizSystem();
		if (!bizSystem.equals("SUPER_SYSTEM")) {
			query.setBizSystem(bizSystem);
		}

		if (query.getGainModel() == null) {
			query.setGainModel(1);
		}

		//根据查询日期判断历史数据是否遗漏
		String str = lotteryDayGainService.getDateStr(query);
		if (!str.equals("")) {
			return AjaxUtil.createReturnValueError(str);
		}

		/*Page gainPage = new Page();*/

		// 如果查询结束时间等于今天的话
		if (strDate.equals(df.format(query.getBelongDateEnd()))) {
			// 如果查询结束时间小于今天的5点,则查询昨天的数据
			//:TODO 报表数据可能没出来，需要查询昨天一天到现在时间的实时数据，历史数据应当是昨天之前历史数据
			if (reportLastTime.compareTo(nowDate) > 0) {
				Calendar currentDate = new GregorianCalendar();
				currentDate.setTime(query.getBelongDateEnd());
				currentDate.add(Calendar.DATE, -2);
				query.setBelongDateEnd(currentDate.getTime());
				
				List<LotteryDayGain> historyList = lotteryDayGainService.getLotteryDayGain(query);
				
				currentDate.add(Calendar.DATE, 1);
				String hisBizSystem = query.getBizSystem();
				Date startTime = ClsDateHelper.getClsStartTime(currentDate.getTime());
				Date endTime = new Date();

				// 设置归属时间置为昨天的12:00:00
				Date belongDate = ClsDateHelper.getClsMiddleTime(currentDate.getTime());
				// 实时数据与历史数据进行合并
				List<LotteryDayGain> lotteryDayGainList = this.getOrderLotteryDayGain(historyList, hisBizSystem,startTime,endTime,belongDate);
				if(query.getGainModel()==1){
					Collections.sort(lotteryDayGainList, new Comparator<LotteryDayGain>(){  
			            public int compare(LotteryDayGain o1, LotteryDayGain o2) {  
			              
			                if(o1.getGain().compareTo(o2.getGain())==1){  
			                    return 1;  
			                }  
			                if(o1.getGain().compareTo(o2.getGain())==0){  
			                    return 0;  
			                }  
			                return -1;  
			            }  
			        }); 
				}else if(query.getGainModel()==2){
					Collections.sort(lotteryDayGainList, new Comparator<LotteryDayGain>(){  
			            public int compare(LotteryDayGain o1, LotteryDayGain o2) {  
			              
			                if(o1.getWinGain().compareTo(o2.getWinGain())==1){  
			                    return 1;  
			                }  
			                if(o1.getWinGain().compareTo(o2.getWinGain())==0){  
			                    return 0;  
			                }  
			                return -1;  
			            }  
			        }); 
				}
				this.setOrderLotteryDayGainToPage(page, lotteryDayGainList);
				/*gainPage = lotteryDayGainService.getLotteryDayGainPage(query, page);*/
			}else if (reportLastTime.compareTo(nowDate) <= 0) {// 如果查询结束时间大于今天的5点,则查询今天3点到现在的数据进行合并
				/*gainPage = lotteryDayGainService.getLotteryDayGainPage(query, page);*/
				// 查询某一时间段的历史记录
				//:TODO 大于今天的5点  查询截止时间要设置为昨天23:59:59 防止多余数据合并
				Calendar currentDate = new GregorianCalendar();
				currentDate.setTime(query.getBelongDateEnd());
				currentDate.add(Calendar.DATE, -1);
				query.setBelongDateEnd(DateUtil.getNowStartTimeByEnd(currentDate.getTime()));
				List<LotteryDayGain> historyList = lotteryDayGainService.getLotteryDayGain(query);
				String hisBizSystem = query.getBizSystem();
				Calendar calendar = Calendar.getInstance();
				Date yesterday = calendar.getTime();

				yesterday = calendar.getTime();
				Date startTime = ClsDateHelper.getClsStartTime(yesterday);
				Date endTime = yesterday;

				// 设置归属时间置为昨天的12:00:00
				Date belongDate = ClsDateHelper.getClsMiddleTime(yesterday);
				// 实时数据与历史数据进行合并
				List<LotteryDayGain> lotteryDayGainList = this.getOrderLotteryDayGain(historyList, hisBizSystem,startTime,endTime,belongDate);
				if(query.getGainModel()==1){
					Collections.sort(lotteryDayGainList, new Comparator<LotteryDayGain>(){  
			            public int compare(LotteryDayGain o1, LotteryDayGain o2) {  
			              
			                if(o1.getGain().compareTo(o2.getGain())==1){  
			                    return 1;  
			                }  
			                if(o1.getGain().compareTo(o2.getGain())==0){  
			                    return 0;  
			                }  
			                return -1;  
			            }  
			        }); 
				}else if(query.getGainModel()==2){
					Collections.sort(lotteryDayGainList, new Comparator<LotteryDayGain>(){  
			            public int compare(LotteryDayGain o1, LotteryDayGain o2) {  
			              
			                if(o1.getWinGain().compareTo(o2.getWinGain())==1){  
			                    return 1;  
			                }  
			                if(o1.getWinGain().compareTo(o2.getWinGain())==0){  
			                    return 0;  
			                }  
			                return -1;  
			            }  
			        }); 
				}
				this.setOrderLotteryDayGainToPage(page, lotteryDayGainList);

			}
			
		}else{
			// 如果查询结束时间不等于今天的话,则查历史数据
			//:TODO 如果有查询昨天的数据，当前时间在05:00之前，报表数据可能没出来，需要查询昨天一天到现在时间的实时数据，历史数据应当是昨天之前历史数据
			if (reportLastTime.compareTo(nowDate) > 0) {
				Calendar currentDate = new GregorianCalendar();
				currentDate.setTime(query.getBelongDateEnd());
				currentDate.add(Calendar.DATE, -1);
				query.setBelongDateEnd(currentDate.getTime());
				List<LotteryDayGain> historyList = null;
				String hisBizSystem = query.getBizSystem();
				Date startTime = ClsDateHelper.getClsStartTime(currentDate
						.getTime());
				Date endTime = new Date();

				// 设置归属时间置为昨天的12:00:00
				Date belongDate = ClsDateHelper.getClsMiddleTime(currentDate
						.getTime());
				// 实时数据与历史数据进行合并
				List<LotteryDayGain> lotteryDayGainList = this
						.getOrderLotteryDayGain(historyList, hisBizSystem,
								startTime, endTime, belongDate);
				this.setOrderLotteryDayGainToPage(page, lotteryDayGainList);
			}else {
				page = lotteryDayGainService.getLotteryDayGainPage(query, page);
			}
			
		}

		if (page.getPageContent() == null) {
			return AjaxUtil.createReturnValueError("未查到盈亏数据，请检查询日期!");
		}
		return AjaxUtil.createReturnValueSuccess(page);
	}
	
	
	/**
	 * 历史数据与实时数据合并
	 * @param historyLotteryDayGain
	 * @param bizSystem
	 */
	public List<LotteryDayGain> getOrderLotteryDayGain(List<LotteryDayGain> historyLotteryDayGain, String bizSystem,Date startDateTime,Date endDateTime,Date belongDate) {

		List<LotteryDayGain> list = new ArrayList<LotteryDayGain>();
		/*Calendar calendar = Calendar.getInstance();
		Date yesterday = calendar.getTime();

		yesterday = calendar.getTime();
		Date startDateTime = ClsDateHelper.getClsStartTime(yesterday);
		Date endDateTime = yesterday;*/

		// 设置归属时间置为昨天的12:00:00
		/*Date belongDate = ClsDateHelper.getClsMiddleTime(yesterday);*/

		List<BizSystem> bizList = new ArrayList<BizSystem>();
		// this.bizSystem有值，单业务系统
		if (bizSystem == null) {
			// 查询所有业务系统
			bizList = bizSystemService.getAllBizSystem();
		} else {
			BizSystem biz = new BizSystem();
			biz.setBizSystem(bizSystem);
			biz = bizSystemService.getBizSystemByCondition(biz);
			bizList.add(biz);
		}

		for (BizSystem bz : bizList) {
			//超级系统不参与生成数据
			if(ConstantUtil.SUPER_SYSTEM.equals(bz.getBizSystem())) {
				continue;
			}
			OrderQuery query = new OrderQuery();
			//有效订单
			query.setEnabled(1);
			query.setCreatedDateStart(startDateTime);
			query.setCreatedDateEnd(endDateTime);
			query.setBizSystem(bz.getBizSystem());

			// 遍历有显示的彩种,构建保存数据对象
			ELotteryKind[] lotteryKinds = ELotteryKind.values();
			List<LotteryDayGain> lotteryDayGains = new ArrayList<LotteryDayGain>();
			for (ELotteryKind lotteryKind : lotteryKinds) {
				// 只统计有显示的彩种
				if (lotteryKind.getIsShow() == 1) {
					LotteryDayGain lotteryDayGain = new LotteryDayGain();
					lotteryDayGain.setBizSystem(bz.getBizSystem());
					lotteryDayGain.setLotteryType(lotteryKind.getCode());
					lotteryDayGain.setBelongDate(belongDate);
					lotteryDayGain.setPayMoney(BigDecimal.ZERO);
					lotteryDayGain.setNotWinPayMoney(BigDecimal.ZERO);
					lotteryDayGain.setWinPayMoney(BigDecimal.ZERO);
					lotteryDayGain.setWinMoney(BigDecimal.ZERO);
					lotteryDayGain.setWinGain(BigDecimal.ZERO);
					lotteryDayGain.setCreateTime(new Date());
					lotteryDayGains.add(lotteryDayGain);
				}
			}
			// 构建Map对象
			Map<String, LotteryDayGain> lotteryDayGainMap = new HashMap<String, LotteryDayGain>();
			for (LotteryDayGain lotteryDayGain : lotteryDayGains) {
				lotteryDayGainMap.put(lotteryDayGain.getLotteryType(), lotteryDayGain);
			}
			// 构建Map对象
			Map<String, LotteryDayGain> historyDayGainMap = new HashMap<String, LotteryDayGain>();
			for (LotteryDayGain hisDayGain : historyLotteryDayGain) {
				if (bz.getBizSystem().equals(hisDayGain.getBizSystem())) {
					historyDayGainMap.put(hisDayGain.getLotteryType(), hisDayGain);
				}

			}
			// 数据库查询各彩种处理类型总额
			/*
			 * List<LotteryDayGain> sumLotteryDayGain=new
			 * ArrayList<LotteryDayGain>();
			 */
			List<Order> orders = orderService.getLotteryDayGain(query);
			if (CollectionUtils.isNotEmpty(orders)) {
				for (Order order : orders) {
					LotteryDayGain lotteryDayGain = lotteryDayGainMap.get(order.getLotteryType());
					LotteryDayGain hisLotteryDayGain = historyDayGainMap.get(order.getLotteryType());
					if (lotteryDayGain != null && hisLotteryDayGain != null) {
						// 未中奖
						if (EProstateStatus.NOT_WINNING.getCode().equals(order.getProstate())) {
							lotteryDayGain.setNotWinPayMoney(lotteryDayGain.getNotWinPayMoney().add(order.getPayMoney())
									.add(hisLotteryDayGain.getNotWinPayMoney()));
							lotteryDayGain.setPayMoney(lotteryDayGain.getPayMoney().add(order.getPayMoney())
									.add(hisLotteryDayGain.getPayMoney()));
							// 中奖
						} else if (EProstateStatus.WINNING.getCode().equals(order.getProstate())) {
							lotteryDayGain.setWinPayMoney(lotteryDayGain.getWinPayMoney().add(order.getPayMoney())
									.add(hisLotteryDayGain.getWinMoney()));
							lotteryDayGain.setPayMoney(lotteryDayGain.getPayMoney().add(order.getPayMoney())
									.add(hisLotteryDayGain.getPayMoney()));
							lotteryDayGain.setWinMoney(lotteryDayGain.getWinMoney().add(order.getWinCost())
									.add(hisLotteryDayGain.getWinMoney()));
						}
					} else if (lotteryDayGain != null && hisLotteryDayGain == null) {
						if (EProstateStatus.NOT_WINNING.getCode().equals(order.getProstate())) {
							lotteryDayGain.setNotWinPayMoney(lotteryDayGain.getNotWinPayMoney().add(order.getPayMoney()));
							lotteryDayGain.setPayMoney(lotteryDayGain.getPayMoney().add(order.getPayMoney()));
							// 中奖
						} else if (EProstateStatus.WINNING.getCode().equals(order.getProstate())) {
							lotteryDayGain.setWinPayMoney(lotteryDayGain.getWinPayMoney().add(order.getPayMoney()));
							lotteryDayGain.setPayMoney(lotteryDayGain.getPayMoney().add(order.getPayMoney()));
							lotteryDayGain.setWinMoney(lotteryDayGain.getWinMoney().add(order.getWinCost()));
						}
					}
					/* sumLotteryDayGain.add(lotteryDayGain); */
				}
			} else {
				if (historyLotteryDayGain != null && historyLotteryDayGain.size() > 0) {
					for (LotteryDayGain lotteryDayGain : lotteryDayGains) {
						LotteryDayGain lottery = lotteryDayGainMap.get(lotteryDayGain.getLotteryType());
						LotteryDayGain hisLotteryDayGain = historyDayGainMap.get(lotteryDayGain.getLotteryType());
						if (lottery != null && hisLotteryDayGain != null) {

							lotteryDayGain.setNotWinPayMoney(hisLotteryDayGain.getNotWinPayMoney());
							lotteryDayGain.setPayMoney(hisLotteryDayGain.getPayMoney());
							lotteryDayGain.setWinPayMoney(hisLotteryDayGain.getWinPayMoney());
							lotteryDayGain.setWinMoney(hisLotteryDayGain.getWinMoney());
						}
					}

				}
			}

			// 计算盈利，赢率
			for (LotteryDayGain lotteryDayGain : lotteryDayGains) {
				// 盈利 投注-中奖
				lotteryDayGain.setGain(lotteryDayGain.getPayMoney().subtract(lotteryDayGain.getWinMoney()));
				// 赢率 盈利/投注
				if (lotteryDayGain.getPayMoney().compareTo(BigDecimal.ZERO) != 0) {
					lotteryDayGain.setWinGain(lotteryDayGain.getGain().divide(lotteryDayGain.getPayMoney(), 2, BigDecimal.ROUND_HALF_UP));
				} else {
					lotteryDayGain.setWinGain(BigDecimal.ZERO);
				}
				list.add(lotteryDayGain);
			}
		}
		return list;

	}
	
	/**
	 * 设置Page
	 * @param page
	 * @param lotteryDayGainList
	 */
  public void setOrderLotteryDayGainToPage(Page page,List<LotteryDayGain> lotteryDayGainList){
		
		List<LotteryDayGain> list = new ArrayList<LotteryDayGain>();
		Integer totalRecNum = lotteryDayGainList.size();
		if (lotteryDayGainList.size() > page.getPageSize()) {
			Integer pageSum = lotteryDayGainList.size() - (page.getPageNo() - 1) * page.getPageSize();
			if (pageSum <= page.getPageSize()) {
				pageSum = lotteryDayGainList.size();
			} else {
				pageSum = page.getPageNo() * page.getPageSize();
			}
			list = lotteryDayGainList.subList((page.getPageNo() - 1) * page.getPageSize(), pageSum);
		}
		Iterator<LotteryDayGain> iterator = list.iterator();
		while (iterator.hasNext()) {
			LotteryDayGain lotteryDayGain = iterator.next();
			String lotteryType = lotteryDayGain.getLotteryType();
			ELotteryKind eLotteryKind = ELotteryKind.valueOf(lotteryType);
			lotteryDayGain.setLotteryType(eLotteryKind.getDescription());
		}
		page.setTotalRecNum(totalRecNum);
		page.setPageContent(list);
	}
}
