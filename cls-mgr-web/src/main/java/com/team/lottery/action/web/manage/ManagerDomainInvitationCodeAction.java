package com.team.lottery.action.web.manage;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.enums.EAdminOperateLogModel;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.AdminOperateLogService;
import com.team.lottery.service.DomainInvitationCodeService;
import com.team.lottery.service.UserRegisterService;
import com.team.lottery.util.AdminOperateUtil;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.DomainInvitationCode;
import com.team.lottery.vo.UserRegister;

@Controller("managerDomainInvitationCodeAction")
public class ManagerDomainInvitationCodeAction {

	private static Logger log = LoggerFactory.getLogger(ManagerDomainInvitationCodeAction.class);

	@Autowired
	private AdminOperateLogService adminOperateLogService;

	@Autowired
	private DomainInvitationCodeService domainInvitationCodeService;

	@Autowired
	private UserRegisterService userRegisterService;

	/**
	 * 开启邀请码域名设置配置
	 * 
	 * @return
	 */
	public Object[] openDomainInvitationCode(Long id) {
		// 保存操作日志
		saveDomainInvitationCodeLog("DomainInvitationCode_open", null, id);
		domainInvitationCodeService.updateDomainInvitationCodeEnabled(id, 1);
		return AjaxUtil.createReturnValueSuccess();
	}

	/**
	 * 关闭邀请码域名设置配置
	 * 
	 * @return
	 */
	public Object[] closeDomainInvitationCode(Long id) {
		// 保存操作日志
		saveDomainInvitationCodeLog("DomainInvitationCode_close", null, id);
		domainInvitationCodeService.updateDomainInvitationCodeEnabled(id, 0);
		return AjaxUtil.createReturnValueSuccess();
	}

	/**
	 * 保存邀请码域名设置日志
	 * 
	 * @param key
	 * @param domain
	 */
	private void saveDomainInvitationCodeLog(String key, DomainInvitationCode domainInvitationCode, Long id) {
		try {
			if (domainInvitationCode == null) {
				domainInvitationCode = domainInvitationCodeService.selectByPrimaryKey(id);
			}
			Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
			String operateDesc = AdminOperateUtil.constructMessage(key, currentAdmin.getUsername(),
					domainInvitationCode.getId().toString());
			String operationIp = BaseDwrUtil.getRequestIp();
			adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(),
					currentAdmin.getUsername(), null, "", EAdminOperateLogModel.DOMAININVITATIONCODE, operateDesc);
		} catch (Exception e) {
			// 保存日志出现异常 不进行处理
			log.error("保存添加操作日志出错", e);
		}
	}

	/**
	 * 删除指定的邀请码域名设置
	 * 
	 * @return
	 */
	public Object[] delDomainInvitationCode(Long id) {
		try {
			Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
			DomainInvitationCode domainInvitationCode = domainInvitationCodeService.selectByPrimaryKey(id);
			String operateDesc = AdminOperateUtil.constructMessage("domainInvitationCode_del", currentAdmin.getUsername(),
					domainInvitationCode.getBizSystem());
			String operationIp = BaseDwrUtil.getRequestIp();
			adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(),
					currentAdmin.getUsername(), null, "", EAdminOperateLogModel.DOMAININVITATIONCODE, operateDesc);
		} catch (Exception e) {
			// 保存日志出现异常 不进行处理
			log.error("删除配置，保存添加操作日志出错", e);
		}
		domainInvitationCodeService.deleteByPrimaryKey(id);
		// 通知前端应用刷新缓存就够了
		/*
		 * try { NotifyFrontInterface.notifyFrontRefreshCache(EFrontCacheType.
		 * FRONT_INDEX_CACHE_INFO.getCode()); } catch (Exception e) {
		 * log.error(e.getMessage());
		 * AjaxUtil.createReturnValueError(e.getMessage()); }
		 */
		return AjaxUtil.createReturnValueSuccess();
	}

	/**
	 * 保存
	 * 
	 * @return
	 */
	public Object[] saveOrUpdateDomainInvitationCode(DomainInvitationCode domainInvitationCode) {
		try {
			Long id = domainInvitationCode.getId();

			Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();

			Integer enabled = domainInvitationCode.getEnable();
			String enabledStr = "";
			if (enabled == 1) {
				enabledStr = "启用";
			} else {
				enabledStr = "关闭";
			}
			String operationIp = BaseDwrUtil.getRequestIp();
			if (id == null) {
				try {
					String operateDesc = AdminOperateUtil.constructMessage("domainInvitationCode_add",
							currentAdmin.getUsername(), domainInvitationCode.getInvitationCode(),
							domainInvitationCode.getDomainUrl(), enabledStr);
					adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(),
							currentAdmin.getUsername(), null, "", EAdminOperateLogModel.DOMAININVITATIONCODE, operateDesc);
				} catch (Exception e) {
					// 保存日志出现异常 不进行处理
					log.error("新增域名邀请码设置，保存添加操作日志出错", e);
				}
				domainInvitationCode.setCreateTime(new Date());
				domainInvitationCode.setCreateAdmin(currentAdmin.getUsername());
				domainInvitationCodeService.insert(domainInvitationCode);
			} else {
				try {
					String operateDesc = AdminOperateUtil.constructMessage("domainInvitationCode_edit",
							currentAdmin.getUsername(), domainInvitationCode.getInvitationCode(),
							domainInvitationCode.getDomainUrl(), enabledStr);
					adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(),
							currentAdmin.getUsername(), null, "", EAdminOperateLogModel.DOMAININVITATIONCODE, operateDesc);
				} catch (Exception e) {
					// 保存日志出现异常 不进行处理
					log.error("编辑域名邀请码设置，保存添加操作日志出错", e);
				}
				domainInvitationCode.setUpdateAdmin(currentAdmin.getUsername());
				domainInvitationCode.setUpdateTime(new Date());
				domainInvitationCodeService.updateByPrimaryKeySelective(domainInvitationCode);
			}
			/*
			 * //通知前端应用刷新缓存就够了 try {
			 * NotifyFrontInterface.notifyFrontRefreshCache
			 * (EFrontCacheType.FRONT_INDEX_CACHE_INFO.getCode()); } catch
			 * (Exception e) { log.error(e.getMessage());
			 * AjaxUtil.createReturnValueError(e.getMessage()); }
			 */
			return AjaxUtil.createReturnValueSuccess();
		} catch (Exception e) {
			return AjaxUtil.createReturnValueError(e.getMessage());
		}

	}

	/**
	 * 查找指定的第三方支付银行代码
	 * 
	 * @return
	 */
	public Object[] getDomainInvitationCodeById(Long id) {
		DomainInvitationCode domainInvitationCode = domainInvitationCodeService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(domainInvitationCode);
	}

	/**
	 * 查询
	 * 
	 * @return
	 */
	public Object[] getAllDomainInvitationCode(DomainInvitationCode query, Page page) {
		if (query == null || page == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		if (!ConstantUtil.SUPER_SYSTEM.equals(currentAdmin.getBizSystem())) {
			query.setBizSystem(currentAdmin.getBizSystem());
		}
		page = domainInvitationCodeService.getAllDomainInvitationCode(query, page);
		return AjaxUtil.createReturnValueSuccess(page);
	}

	/**
	 * 查询
	 * 
	 * @return
	 */
	public Object[] getUserRegisterLinkByInvitationCode(String invitationCode, String bizSystem) {

		UserRegister userRegister = userRegisterService.getUserRegisterLinkByInvitationCode(invitationCode, bizSystem);
		return AjaxUtil.createReturnValueSuccess(userRegister);
	}
}
