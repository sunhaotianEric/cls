package com.team.lottery.action.web.manage;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.Page;
import com.team.lottery.service.SmsSendConfigService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.vo.SmsSendConfig;

@Controller("managerSmsSendConfigAction")
public class ManagerSmsSendConfigAction {
	@Autowired
	private SmsSendConfigService smsSendConfigService;
	
	/**
	 * 分页查询
	 * @param query
	 * @param page
	 * @return
	 */
	public Object[] getAllSmsSendConfig(SmsSendConfig query,Page page){
		if(query==null||page==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		page=smsSendConfigService.getAllSmsSendConfig(query, page);
		return AjaxUtil.createReturnValueSuccess(page);
	}
	
	/**
	 * 根据ID查询
	 * @param id
	 * @return
	 */
	public Object[] selectSmsSendConfig(Integer id){
		SmsSendConfig record=smsSendConfigService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(record);
	}
	
	/**
	 * 修改
	 * @param record
	 * @return
	 */
	public Object[] updateSmsSendConfig(SmsSendConfig record){
		if(record==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		smsSendConfigService.updateByPrimaryKeySelective(record);
		return AjaxUtil.createReturnValueSuccess();
	}
	
	/**
	 * 根据ID删除
	 * @param id
	 * @return
	 */
	public Object[] delSmsSendConfigById(Integer id){
		if(id==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		smsSendConfigService.deleteByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess();
	}
	
	/**
	 * 保存
	 * @param record
	 * @return
	 */
	public Object[] saveSmsSendConfig(SmsSendConfig record){
    	if(record == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	Integer id = record.getId();
    	if(id == null){
    		record.setCreateTime(new Date());
    		smsSendConfigService.insertSelective(record);
    	}else{
    		record.setUpdateTime(new Date());
    		smsSendConfigService.updateByPrimaryKeySelective(record);
    	}
    	return AjaxUtil.createReturnValueSuccess();
    }
	
	/**
     * 启用停用
     */
	public Object[] enableSms(Integer id,Integer flag) {
		if (id == null||flag==null) {
			return AjaxUtil.createReturnValueError("参数传递错误");
		}
		SmsSendConfig smsSendConfig = new SmsSendConfig();
		smsSendConfig.setId(id);
		smsSendConfig.setEnable(flag);
		int result = smsSendConfigService.updateByPrimaryKeySelective(smsSendConfig);
		if (result > 0) {
			return AjaxUtil.createReturnValueSuccess();
		} else {
			return AjaxUtil.createReturnValueError("添加失败，请重新添加");
		}
	}
}
