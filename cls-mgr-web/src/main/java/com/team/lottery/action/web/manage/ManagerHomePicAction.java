package com.team.lottery.action.web.manage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.directwebremoting.io.FileTransfer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.enums.EAdminOperateLogModel;
import com.team.lottery.enums.EFrontCacheType;
import com.team.lottery.extvo.ImageUploadResult;
import com.team.lottery.extvo.Page;
import com.team.lottery.service.AdminOperateLogService;
import com.team.lottery.service.BizSystemService;
import com.team.lottery.service.HomePicService;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.system.message.MessageQueueCenter;
import com.team.lottery.system.messagetype.HomePicturesMessage;
import com.team.lottery.util.AdminOperateUtil;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.HttpClientUtil;
import com.team.lottery.util.JSONUtils;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.BizSystem;
import com.team.lottery.vo.HomePic;

@Controller("managerHomePicAction")
public class ManagerHomePicAction {
	private static Logger logger = LoggerFactory.getLogger(ManagerHomePicAction.class);
	@Autowired
	private HomePicService homePicService;
	@Autowired
	private AdminOperateLogService adminOperateLogService;
	@Autowired
	private BizSystemService bizSystemService;
	
	public Object getHomePicByQueryPage(HomePic query,Page page){
     	if(query == null || page == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
     	Admin currentUser = BaseDwrUtil.getCurrentAdmin();
      	String bizSystem = currentUser.getBizSystem();
//    	if(query.getBizSystem() == null){
    		if(!"SUPER_SYSTEM".equals(bizSystem)){
    			query.setBizSystem(bizSystem);
    		}
//    	}
    	page = homePicService.getHomePicByQueryPage(query, page);
		return AjaxUtil.createReturnValueSuccess(page);
	}
	

    /**
     * 保存活动中心
     * @return
     */
    public Object[] saveOrUpdateHomePic(HomePic homePic){
    	try{
    		Long id = homePic.getId();
    		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    		
        	if(id==null){
        		if(!"SUPER_SYSTEM".equals(currentAdmin.getBizSystem())){
        			homePic.setBizSystem(currentAdmin.getBizSystem());
        		}
        		homePic.setCreateTime(new Date());
        		homePic.setCreateAdmin(currentAdmin.getUsername());
        		saveHomePicRecordLog("save",homePic);
        		homePicService.insertSelective(homePic);
        	}else{
        		homePic.setUpdateTime(new Date());
        		homePic.setUpdateAdmin(currentAdmin.getUsername());
        		saveHomePicRecordLog("update",homePic);
        		homePicService.updateByPrimaryKeySelective(homePic);
        	}
        	//通知redis刷新缓存就够了
        	try {
        		HomePicturesMessage message = new HomePicturesMessage();
        		message.setType(EFrontCacheType.HOME_PICTUS_CACHE_INFO.getCode());
        		message.setData("1");
        		message.setBizSystem(homePic.getBizSystem());
        		MessageQueueCenter.putMessage(message);
        		
    		} catch (Exception e) {
    			logger.error(e.getMessage());
    			AjaxUtil.createReturnValueError(e.getMessage());
    		}
    		return AjaxUtil.createReturnValueSuccess();
    	}catch(Exception e){
    		return AjaxUtil.createReturnValueError(e.getMessage());
    	}
    	
    }
    
    /**
     * 删除
     * @param id
     * @return
     */
    public 	Object[] delHomePicConfig(Long id){
    	if(id == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	HomePic homePicConfig = homePicService.selectByPrimaryKey(id);
    	saveHomePicRecordLog("del",homePicConfig);
    	homePicService.deleteByPrimaryKey(id);
    	
    	//通知redis刷新缓存就够了
    	try {
    		HomePicturesMessage message = new HomePicturesMessage();
    		message.setType(EFrontCacheType.HOME_PICTUS_CACHE_INFO.getCode());
    		message.setData("1");
    		message.setBizSystem(homePicConfig.getBizSystem());
    		MessageQueueCenter.putMessage(message);
    		
		} catch (Exception e) {
			logger.error(e.getMessage());
			AjaxUtil.createReturnValueError(e.getMessage());
		}
    	
    	return AjaxUtil.createReturnValueSuccess();
    }
    
    /**
     * 启用显示
     * @param id
     * @return
     */
    public Object[] enableHomePicConfig(Long id){
    	if(id == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	
    	HomePic homePicConfig = homePicService.selectByPrimaryKey(id);
    	
    	if(homePicConfig.getIsShow() == 1){
    		return AjaxUtil.createReturnValueSuccess("该配置已经是启用状态");
    	}else{
    		homePicConfig.setIsShow(1);
    	}
    	saveHomePicRecordLog("open",homePicConfig);
    	homePicService.updateByPrimaryKeySelective(homePicConfig);
    	//通知redis刷新缓存就够了
    	try {
    		HomePicturesMessage message = new HomePicturesMessage();
    		message.setType(EFrontCacheType.HOME_PICTUS_CACHE_INFO.getCode());
    		message.setData("1");
    		message.setBizSystem(homePicConfig.getBizSystem());
    		MessageQueueCenter.putMessage(message);
    		
		} catch (Exception e) {
			logger.error(e.getMessage());
			AjaxUtil.createReturnValueError(e.getMessage());
		}
    	
    	return AjaxUtil.createReturnValueSuccess();
    }
    
    /**
     * 关闭显示
     * @param id
     * @return
     */
    public Object[] disnableHomePicConfig(Long id){
    	if(id == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	
    	HomePic homePicConfig = homePicService.selectByPrimaryKey(id);
    	
    	if(homePicConfig.getIsShow() == 0){
    		return AjaxUtil.createReturnValueSuccess("该配置已经是停用状态");
    	}else{
    		homePicConfig.setIsShow(0);
    	}
    	saveHomePicRecordLog("close",homePicConfig);
    	homePicService.updateByPrimaryKeySelective(homePicConfig);
    	
    	//通知redis刷新缓存就够了
    	try {
    		HomePicturesMessage message = new HomePicturesMessage();
    		message.setType(EFrontCacheType.HOME_PICTUS_CACHE_INFO.getCode());
    		message.setData("1");
    		message.setBizSystem(homePicConfig.getBizSystem());
    		MessageQueueCenter.putMessage(message);
    		
		} catch (Exception e) {
			logger.error(e.getMessage());
			AjaxUtil.createReturnValueError(e.getMessage());
		}
    	return AjaxUtil.createReturnValueSuccess();
    }
    
    /**
     * 刷新首页图片缓存
     * @return
     */
    public Object[] refreshHomePicCache (){
    	try {
    		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    		logger.info("管理员[{}]刷新首页图片缓存", currentAdmin.getUsername());
    		
    		if("SUPER_SYSTEM".equals(currentAdmin.getBizSystem())){
    			List<BizSystem> allBizSystem = bizSystemService.getAllBizSystem();
    			Iterator<BizSystem> iterator = allBizSystem.iterator();
    			while(iterator.hasNext()){
    				HomePicturesMessage message = new HomePicturesMessage();
    				message.setType(EFrontCacheType.HOME_PICTUS_CACHE_INFO.getCode());
    				message.setData("1");
    				String bizSystem = iterator.next().getBizSystem();
    				message.setBizSystem(bizSystem);
    				MessageQueueCenter.putMessage(message);
    			}
    		}else{
    			HomePicturesMessage message = new HomePicturesMessage();
				message.setType(EFrontCacheType.HOME_PICTUS_CACHE_INFO.getCode());
				message.setData("1");
    			message.setBizSystem(currentAdmin.getBizSystem());
				MessageQueueCenter.putMessage(message);
    		}
        		
		} catch (Exception e) {
			logger.error(e.getMessage());
			AjaxUtil.createReturnValueError(e.getMessage());
		}
    	return AjaxUtil.createReturnValueSuccess();
    }
    
    /**
	 * 保存首页图片操作日志
	 * @param root
	 * @param homePic
	 */
	private void saveHomePicRecordLog(String root,HomePic homePic){
		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
		String type=homePic.getPicType()=="MOBILE"?"手机端":"PC端";
		String operateDesc="";
		if(root.equals("save")){
			operateDesc=AdminOperateUtil.constructMessage("homePic_add_log",currentAdmin.getUsername(),type,homePic.getHomePicUrl());
		}else if(root.equals("update")){
			operateDesc=AdminOperateUtil.constructMessage("homePic_update_log",currentAdmin.getUsername(),type,homePic.getId().toString(),homePic.getHomePicUrl());
		}else if(root.equals("del")){
			operateDesc=AdminOperateUtil.constructMessage("homePic_del_log",currentAdmin.getUsername(),type,homePic.getId().toString(),homePic.getHomePicUrl());
		}else if(root.equals("open")){
			operateDesc=AdminOperateUtil.constructMessage("homePic_open_log",currentAdmin.getUsername(),type,homePic.getId().toString());
		}else if(root.equals("close")){
			operateDesc=AdminOperateUtil.constructMessage("homePic_close_log",currentAdmin.getUsername(),type,homePic.getId().toString());
		}
		logger.info(operateDesc);
		try {
			String operationIp = BaseDwrUtil.getRequestIp();
			adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.HOMEPIC_MANAGE, operateDesc);
    	} catch (Exception e) {
    		//保存日志出现异常 不进行处理
    		logger.error("管理员操作首页图片，保存添加操作日志出错", e);
    	}
	}
    
    /**
     * 查找当前业务系统
     * @return
     */
    public Object[] getcurrentBizSystem(){
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	String bizSystem = currentAdmin.getBizSystem();
		return AjaxUtil.createReturnValueSuccess(bizSystem);
    }
    
    /**
     * 查找指定的首页图片信息
     * @return
     */
    public Object[] getHgomePicById(Long id){
    	 HomePic hgomePic = homePicService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(hgomePic, SystemConfigConstant.imgServerUrl);
    }
	
	
    /**
	 * 文件上传
	 * @return
	 */
	public Object[] uploadImgFile(FileTransfer fileTransfer, String bizSystem, String picType) {
		try {
			//判断上传文件的类型
			String uploadFileName = fileTransfer.getFilename();
			int index = uploadFileName.indexOf(".");
			String fileType = "";
			if(index > 0) {
				fileType = uploadFileName.substring(index);
				if(!fileType.equalsIgnoreCase(".jpg") && !fileType.equalsIgnoreCase(".png")) {
					return AjaxUtil.createReturnValueError("上传图片文件类型错误，请重新选择");
				}
			}
			if(StringUtils.isEmpty(fileType)) {
				return AjaxUtil.createReturnValueError("上传图片文件类型错误，请重新选择");
			}
			
			//生成文件名称  根据日期
			String newFileName = DateUtil.getDate(new Date(), "yyyyMMddHHmmssSS");
			newFileName += fileType;
			
			WebContext webContext = WebContextFactory.get();
			// String realtivepath = webContext.getServletContext().getContextPath()
			// + "/upload/";
			String saveurl = webContext.getHttpServletRequest().getSession()
					.getServletContext().getRealPath("/upload");
			File fileDir = new File(saveurl);
			if (!fileDir.exists()) {
				fileDir.mkdirs();
			}
			String saveFileUrl = saveurl + "/" + newFileName;
			File file = new File(saveFileUrl);
			InputStream uploadFile = fileTransfer.getInputStream(); 
			int available = uploadFile.available();
			byte[] b = new byte[available];
			FileOutputStream foutput = new FileOutputStream(file);
			uploadFile.read(b);
			foutput.write(b);
			foutput.flush();
			foutput.close();
			uploadFile.close();
			logger.info("接收首页图片上传到本地成功，本地图片路径地址[{}],开始上传到图片服务器...", saveFileUrl);
			
			//使用http接口上传图片
			String picSavePath = bizSystem + "/" + picType.toLowerCase() + "/banner";
			String fullSavePath = picSavePath + "/" + newFileName;
			Map<String, String> params = new HashMap<String, String>();
			params.put("path", picSavePath);
			params.put("fileName", newFileName);
			String uploadResult = HttpClientUtil.doPostFileUpload(SystemConfigConstant.imgServerUploadUrl+ConstantUtil.FILE_UPLOAD_RESOURCE, file, params);
			logger.info("上传图片结果：{}", uploadResult);
			
			ImageUploadResult result = JSONUtils.toBean(uploadResult, ImageUploadResult.class);
			if(result == null) {
				return AjaxUtil.createReturnValueError("文件上传图片服务器错误，原因:返回结果为空");
			} 
			if("error".equals(result.getResult())) {
				String mString = result.getMsg();
				return AjaxUtil.createReturnValueError("文件上传图片服务器错误，原因:" + mString);
			}
			//sftp方式接口上传图片
//			String fileUrl = "";
//			try{
//				//上传文件至ftp
//				fileUrl = SftpClientUtil.uploadFileToSFTP(saveFileUrl, "index", newFileName);
//			} catch (Exception e) {
//				logger.error("上传文件至ftp出错", e);
//				return AjaxUtil.createReturnValueError("上传文件至图片服务器失败");
//			}
			logger.info("上传首页图片到图片服务器成功，删除本地图片文件");
			//删除文件
			file.delete();
			
			return AjaxUtil.createReturnValueSuccess(fullSavePath, SystemConfigConstant.imgServerUrl);
		} catch(Exception e){
			logger.error("文件上传失败", e);
			return AjaxUtil.createReturnValueError("文件上传失败");
		}
		
	}
	
}
