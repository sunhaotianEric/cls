package com.team.lottery.action.web.manage;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.directwebremoting.io.FileTransfer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.enums.EAdminOperateLogModel;
import com.team.lottery.enums.EFrontCacheType;
import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.enums.EProstateStatus;
import com.team.lottery.enums.ERedisChannel;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.DeleteAgoDataInfo;
import com.team.lottery.extvo.LotterySetMsg;
import com.team.lottery.extvo.OrderQuery;
import com.team.lottery.service.AdminOperateLogService;
import com.team.lottery.service.CommonOpenCodeService;
import com.team.lottery.service.LotteryCodeService;
import com.team.lottery.service.LotteryCodeXyService;
import com.team.lottery.service.LotteryService;
import com.team.lottery.service.OrderService;
import com.team.lottery.service.SystemConfigService;
import com.team.lottery.service.SystemService;
import com.team.lottery.service.UserService;
import com.team.lottery.service.lotterykind.LotteryKindService;
import com.team.lottery.system.MainOpenDealThread;
import com.team.lottery.system.SystemCache;
import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.MessageQueueCenter;
import com.team.lottery.system.message.messagetype.SystemConfigMessage;
import com.team.lottery.util.AdminOperateUtil;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.BeanUtilsExtends;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryCodeXy;
import com.team.lottery.vo.Order;
import com.team.lottery.vo.User;

@Controller("managerSystemAction")
public class ManagerSystemAction {
	private static Logger logger = LoggerFactory
			.getLogger(ManagerSystemAction.class);

	@Autowired
	private SystemService systemService;
	@Autowired
	private LotteryCodeService lotteryCodeService;
	@Autowired
	private LotteryCodeXyService lotteryCodeXyService;
	@Autowired
	private UserService userService;
	@Autowired
	private AdminOperateLogService adminOperateLogService;
	@Autowired
	private OrderService orderService;
    @Autowired
	private SystemConfigService systemConfigService;
    @Autowired
    private LotteryService lotteryService;
    
	private static Logger log = LoggerFactory.getLogger(ManagerSystemAction.class);
	
    
    /**
     * 提交更新彩种配置信息 
     * @return
     */
    public Object[] updateLotterySetting(LotterySetMsg lotterySetMsg){
     	if(lotterySetMsg == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
        systemConfigService.saveOrUpdateLotterySetConfig(lotterySetMsg);
    	
    	//通知前端系统刷新缓存
		try {
			SystemConfigMessage message=new SystemConfigMessage();
   			MessageQueueCenter.putRedisMessage(message, ERedisChannel.CLSCACHE.name());
		} catch (Exception e) {
			log.error(e.getMessage());
			AjaxUtil.createReturnValueError(e.getMessage());
		}
    	
		return AjaxUtil.createReturnValueSuccess();
    }
    
    /**
     * 保存彩种设置变化的操作
     * @param newSetMsg
     * @param oldSetMsgStr
     */
    @SuppressWarnings("unused")
	private void saveLotterySettingRecordLog(LotterySetMsg newSetMsg, String oldSetMsgStr) {
    	LotterySetMsg oldSetMsg = new LotterySetMsg();
    	try {
    		oldSetMsg.parseLotterySet(oldSetMsgStr);
		} catch (Exception e) {
			log.error("解析原有的彩种设置字符串出错，内容：" + oldSetMsgStr, e);
		}
    	//开启彩种列表
    	Set<String> openLotteryTypes = new HashSet<String>();
    	//关闭彩种列表
    	Set<String> closeLotteryTypes = new HashSet<String>();
    	if(!newSetMsg.getIsCQSSCOpen().equals(oldSetMsg.getIsCQSSCOpen())) {
    		if(newSetMsg.getIsCQSSCOpen() == 1) {
    			openLotteryTypes.add(ELotteryKind.CQSSC.getDescription());
    		} else {
    			closeLotteryTypes.add(ELotteryKind.CQSSC.getDescription());
    		}
    	}
    	if(!newSetMsg.getIsJXSSCOpen().equals(oldSetMsg.getIsJXSSCOpen())) {
    		if(newSetMsg.getIsJXSSCOpen() == 1) {
    			openLotteryTypes.add(ELotteryKind.JXSSC.getDescription());
    		} else {
    			closeLotteryTypes.add(ELotteryKind.JXSSC.getDescription());
    		}
    	}
    	if(!newSetMsg.getIsHLJSSCOpen().equals(oldSetMsg.getIsHLJSSCOpen())) {
    		if(newSetMsg.getIsHLJSSCOpen() == 1) {
    			openLotteryTypes.add(ELotteryKind.HLJSSC.getDescription());
    		} else {
    			closeLotteryTypes.add(ELotteryKind.HLJSSC.getDescription());
    		}
    	}
    	if(!newSetMsg.getIsXJSSCOpen().equals(oldSetMsg.getIsXJSSCOpen())) {
    		if(newSetMsg.getIsXJSSCOpen() == 1) {
    			openLotteryTypes.add(ELotteryKind.XJSSC.getDescription());
    		} else {
    			closeLotteryTypes.add(ELotteryKind.XJSSC.getDescription());
    		}
    	}
    	if(!newSetMsg.getIsJLFFCOpen().equals(oldSetMsg.getIsJLFFCOpen())) {
    		if(newSetMsg.getIsJLFFCOpen() == 1) {
    			openLotteryTypes.add(ELotteryKind.JLFFC.getDescription());
    		} else {
    			closeLotteryTypes.add(ELotteryKind.JLFFC.getDescription());
    		}
    	}
    	if(!newSetMsg.getIsGDKLSFOpen().equals(oldSetMsg.getIsGDKLSFOpen())) {
    		if(newSetMsg.getIsGDKLSFOpen() == 1) {
    			openLotteryTypes.add(ELotteryKind.GDKLSF.getDescription());
    		} else {
    			closeLotteryTypes.add(ELotteryKind.GDKLSF.getDescription());
    		}
    	}
    	if(!newSetMsg.getIsGDSYXWOpen().equals(oldSetMsg.getIsGDSYXWOpen())) {
    		if(newSetMsg.getIsGDSYXWOpen() == 1) {
    			openLotteryTypes.add(ELotteryKind.GDSYXW.getDescription());
    		} else {
    			closeLotteryTypes.add(ELotteryKind.GDSYXW.getDescription());
    		}
    	}
    	if(!newSetMsg.getIsSHSSLDPCOpen().equals(oldSetMsg.getIsSHSSLDPCOpen())) {
    		if(newSetMsg.getIsSHSSLDPCOpen() == 1) {
    			openLotteryTypes.add(ELotteryKind.SHSSLDPC.getDescription());
    		} else {
    			closeLotteryTypes.add(ELotteryKind.SHSSLDPC.getDescription());
    		}
    	}
    	if(!newSetMsg.getIsSDSYXWOpen().equals(oldSetMsg.getIsSDSYXWOpen())) {
    		if(newSetMsg.getIsSDSYXWOpen() == 1) {
    			openLotteryTypes.add(ELotteryKind.SDSYXW.getDescription());
    		} else {
    			closeLotteryTypes.add(ELotteryKind.SDSYXW.getDescription());
    		}
    	}
    	if(!newSetMsg.getIsJXSYXWOpen().equals(oldSetMsg.getIsJXSYXWOpen())) {
    		if(newSetMsg.getIsJXSYXWOpen() == 1) {
    			openLotteryTypes.add(ELotteryKind.JXSYXW.getDescription());
    		} else {
    			closeLotteryTypes.add(ELotteryKind.JXSYXW.getDescription());
    		}
    	}
    	if(!newSetMsg.getIsFJSYXWOpen().equals(oldSetMsg.getIsFJSYXWOpen())) {
    		if(newSetMsg.getIsFJSYXWOpen() == 1) {
    			openLotteryTypes.add(ELotteryKind.FJSYXW.getDescription());
    		} else {
    			closeLotteryTypes.add(ELotteryKind.FJSYXW.getDescription());
    		}
    	}
    	if(!newSetMsg.getIsCQSYXWOpen().equals(oldSetMsg.getIsCQSYXWOpen())) {
    		if(newSetMsg.getIsCQSYXWOpen() == 1) {
    			openLotteryTypes.add(ELotteryKind.CQSYXW.getDescription());
    		} else {
    			closeLotteryTypes.add(ELotteryKind.CQSYXW.getDescription());
    		}
    	}
    	if(!newSetMsg.getIsTJSSCOpen().equals(oldSetMsg.getIsTJSSCOpen())) {
    		if(newSetMsg.getIsTJSSCOpen() == 1) {
    			openLotteryTypes.add(ELotteryKind.TJSSC.getDescription());
    		} else {
    			closeLotteryTypes.add(ELotteryKind.TJSSC.getDescription());
    		}
    	}
    	if(!newSetMsg.getIsFC3DDPCOpen().equals(oldSetMsg.getIsFC3DDPCOpen())) {
    		if(newSetMsg.getIsFC3DDPCOpen() == 1) {
    			openLotteryTypes.add(ELotteryKind.FCSDDPC.getDescription());
    		} else {
    			closeLotteryTypes.add(ELotteryKind.FCSDDPC.getDescription());
    		}
    	}
    	if(!newSetMsg.getIsJSKSOpen().equals(oldSetMsg.getIsJSKSOpen())) {
    		if(newSetMsg.getIsJSKSOpen() == 1) {
    			openLotteryTypes.add(ELotteryKind.JSKS.getDescription());
    		} else {
    			closeLotteryTypes.add(ELotteryKind.JSKS.getDescription());
    		}
    	}
    	if(!newSetMsg.getIsAHKSOpen().equals(oldSetMsg.getIsAHKSOpen())) {
    		if(newSetMsg.getIsAHKSOpen() == 1) {
    			openLotteryTypes.add(ELotteryKind.AHKS.getDescription());
    		} else {
    			closeLotteryTypes.add(ELotteryKind.AHKS.getDescription());
    		}
    	}
    	if(!newSetMsg.getIsBJKSOpen().equals(oldSetMsg.getIsBJKSOpen())) {
    		if(newSetMsg.getIsBJKSOpen() == 1) {
    			openLotteryTypes.add(ELotteryKind.BJKS.getDescription());
    		} else {
    			closeLotteryTypes.add(ELotteryKind.BJKS.getDescription());
    		}
    	}
    	if(!newSetMsg.getIsHBKSOpen().equals(oldSetMsg.getIsHBKSOpen())) {
    		if(newSetMsg.getIsHBKSOpen() == 1) {
    			openLotteryTypes.add(ELotteryKind.HBKS.getDescription());
    		} else {
    			closeLotteryTypes.add(ELotteryKind.HBKS.getDescription());
    		}
    	}
    	if(!newSetMsg.getIsJLKSOpen().equals(oldSetMsg.getIsJLKSOpen())) {
    		if(newSetMsg.getIsJLKSOpen() == 1) {
    			openLotteryTypes.add(ELotteryKind.JLKS.getDescription());
    		} else {
    			closeLotteryTypes.add(ELotteryKind.JLKS.getDescription());
    		}
    	}
    	if(!newSetMsg.getIsGXKSOpen().equals(oldSetMsg.getIsGXKSOpen())) {
    		if(newSetMsg.getIsGXKSOpen() == 1) {
    			openLotteryTypes.add(ELotteryKind.GXKS.getDescription());
    		} else {
    			closeLotteryTypes.add(ELotteryKind.GXKS.getDescription());
    		}
    	}
    	if(!newSetMsg.getIsBJPK10Open().equals(oldSetMsg.getIsBJPK10Open())) {
    		if(newSetMsg.getIsBJPK10Open() == 1) {
    			openLotteryTypes.add(ELotteryKind.BJPK10.getDescription());
    		} else {
    			closeLotteryTypes.add(ELotteryKind.BJPK10.getDescription());
    		}
    	}
    	if(!newSetMsg.getIsHGFFCOpen().equals(oldSetMsg.getIsHGFFCOpen())){
    		if(newSetMsg.getIsHGFFCOpen() == 1) {
    			openLotteryTypes.add(ELotteryKind.HGFFC.getDescription());
    		} else {
    			closeLotteryTypes.add(ELotteryKind.HGFFC.getDescription());
    		}
    	}
    	if(!newSetMsg.getIsXJPLFCOpen().equals(oldSetMsg.getIsXJPLFCOpen())){
    		if(newSetMsg.getIsXJPLFCOpen() == 1) {
    			openLotteryTypes.add(ELotteryKind.XJPLFC.getDescription());
    		} else {
    			closeLotteryTypes.add(ELotteryKind.XJPLFC.getDescription());
    		}
    	}
    	if(!newSetMsg.getIsTWWFCOpen().equals(oldSetMsg.getIsTWWFCOpen())){
    		if(newSetMsg.getIsTWWFCOpen() == 1) {
    			openLotteryTypes.add(ELotteryKind.TWWFC.getDescription());
    		} else {
    			closeLotteryTypes.add(ELotteryKind.TWWFC.getDescription());
    		}
    	}
    	try {
    		
    		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    		StringBuffer operateDesc = new StringBuffer();
    		//拼接开启的彩种
    		if(openLotteryTypes.size() > 0) {
    			StringBuffer openTypeStr = new StringBuffer();
    			for(String type : openLotteryTypes) {
    				openTypeStr.append(type).append(",");
    			}
    			//截取最后一个逗号
    			operateDesc.append(AdminOperateUtil.constructMessage("lotteryset_open_log", currentAdmin.getUsername(), openTypeStr.substring(0, openTypeStr.length() - 1)));
    		}
    		//拼接关闭的彩种
    		if(closeLotteryTypes.size() > 0) {
    			StringBuffer closeTypeStr = new StringBuffer();
    			for(String type : closeLotteryTypes) {
    				closeTypeStr.append(type).append(",");
    			}
    			//截取最后一个逗号
    			operateDesc.append(AdminOperateUtil.constructMessage("lotteryset_close_log", currentAdmin.getUsername(), closeTypeStr.substring(0, closeTypeStr.length() - 1)));
    		}
    		if(!StringUtils.isEmpty(operateDesc.toString())) {
    			String operationIp = BaseDwrUtil.getRequestIp();
    			adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.SYSTEM_CONFIG, operateDesc.toString());
    		}
    	} catch (Exception e) {
    		//保存日志出现异常 不进行处理
    		log.error("更改彩票彩种设置，保存添加操作日志出错", e);
    	}
    }

    /**
     * 获取彩种配置信息 
     * @return
     */
    public Object[] getLotterySetting(){
    	Map<String,String> configMap=systemConfigService.getSystemConfigByLikeValue("is%Open");
    	
        if(configMap.size() ==0){
    		return AjaxUtil.createReturnValueError("当前不存在系统配置信息的记录.");
        }
      
        LotterySetMsg lotterySetMsg = new LotterySetMsg();
        try {
			BeanUtils.populate(lotterySetMsg, configMap);
		} catch (IllegalAccessException e) {
			return AjaxUtil.createReturnValueError("系统出错，请联系管理员！"+e.getMessage());
		} catch (InvocationTargetException e) {
			return AjaxUtil.createReturnValueError("系统出错，请联系管理员！"+e.getMessage());
		}
    
        
        //缓存数据初始化
  		SystemCache.loadLotteryConfigCacheData();
  		
		return AjaxUtil.createReturnValueSuccess(lotterySetMsg);
    }

    /**
     * 更新缓存数据
     * @return
     */
    public Object[] updateCacheData(){
		//缓存数据初始化
		SystemCache.loadSystemConfigCacheData();

		//通知前端系统刷新缓存
		try {
			BaseMessage message=new BaseMessage();
			message.setType(EFrontCacheType.SYS_CONFIG_DATA.getCode());
			message.setData("1");
   			MessageQueueCenter.putRedisMessage(message, ERedisChannel.CLSCACHE.name());

		} catch (Exception e) {
			log.error(e.getMessage());
			AjaxUtil.createReturnValueError(e.getMessage());
		}
		return AjaxUtil.createReturnValueSuccess();
    }

    /**
     * 手动开奖处理
     * @return
     */
    public Object[] hendUpdateDeal(ELotteryKind kind,String openCode,String expect){
    	if(kind == null || openCode == null || expect == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
		LotteryCode lotteryCode= null;
		String[] nums = openCode.split(ConstantUtil.OPENCODE_PLIT);
		log.info("开奖类型:" + kind.name());
		log.info("开奖号码:" + openCode);
		LotteryKindService lotteryKindService = (LotteryKindService)ApplicationContextUtil.getBean(kind.getServiceClassName());
		log.info("实例:" + lotteryKindService);
		boolean lotteryCodeCheckResult = lotteryKindService.lotteryCodesCheckByOpenCode(openCode);
        if(!lotteryCodeCheckResult){
    		return AjaxUtil.createReturnValueError("开奖号码审核未通过.");
        }
		lotteryCode = lotteryCodeService.getLotteryCodeByKindAndExpect(kind.name(), expect);
		if(lotteryCode != null){
    		return AjaxUtil.createReturnValueError("该期号开奖号码已经存在了,不能再次开奖.");
		}
		//操作日志保存
		Admin currentAdmin = null;
		try {
			currentAdmin = BaseDwrUtil.getCurrentAdmin();
    		String lotteryKindName = ELotteryKind.valueOf(kind.name()).getDescription();
    		String operateDesc = AdminOperateUtil.constructMessage("hendUpdateDeal_log", currentAdmin.getUsername(), lotteryKindName, expect, openCode);
    		String operationIp = BaseDwrUtil.getRequestIp();
    		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.HAND_UPDATE_DEAL, operateDesc);
    	} catch (Exception e) {
    		//保存日志出现异常 不进行处理
    		log.error("手动开奖处理，保存添加操作日志出错", e);
    	}
		
		lotteryCode = new LotteryCode();
		lotteryCode.setLotteryNum(expect);
		lotteryCode.setKjtime(new Date());
		lotteryCode.setAddtime(new Date());
		lotteryCode.setLotteryName(kind.name());
		if(nums.length > 0){
			lotteryCode.setNumInfo1(nums[0]);
		}
		if(nums.length > 1){
			lotteryCode.setNumInfo2(nums[1]);
		}
		if(nums.length > 2){
			lotteryCode.setNumInfo3(nums[2]);
		}
		if(nums.length > 3){
			lotteryCode.setNumInfo4(nums[3]);
		}		
		if(nums.length > 4){
			lotteryCode.setNumInfo5(nums[4]);
		}
		if(nums.length > 5){
			lotteryCode.setNumInfo6(nums[5]);
		}
		if(nums.length > 6){
			lotteryCode.setNumInfo7(nums[6]);
		}
		if(nums.length > 7){
			lotteryCode.setNumInfo8(nums[7]);
		}
		if(nums.length > 8){
			lotteryCode.setNumInfo9(nums[8]);
		}		
		if(nums.length > 9){
			lotteryCode.setNumInfo10(nums[9]);
		}
		lotteryCode.setProstateDeal(0);  //默认设置未还未进行中奖的处理
    	
		try {
			CommonOpenCodeService.prostate(lotteryCode);
		} catch (Exception e) {
			log.error(e.getMessage());
	    	return AjaxUtil.createReturnValueError(e.getMessage());
		}
		
    	return AjaxUtil.createReturnValueSuccess();
    }
    
    /**
     * 部分未开奖处理
     * @return
     */
    public Object[] hendPartUpdateDeal(ELotteryKind kind, String expect){
    	if(kind == null || expect == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	LotteryCode lotteryCode = lotteryCodeService.getLotteryCodeByKindAndExpect(kind.getCode(), expect);
    	if(lotteryCode == null) {
    		return AjaxUtil.createReturnValueError("当前彩种期号开奖号码不存在，请使用手动开奖处理");
    	}
    	//操作日志保存
    	String lotteryKindName = ELotteryKind.valueOf(kind.name()).getDescription();
    	try {
    		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    		String operateDesc = AdminOperateUtil.constructMessage("hendPartUpdateDeal_log", currentAdmin.getUsername(), lotteryKindName, expect);
    		String operationIp = BaseDwrUtil.getRequestIp();
    		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.HAND_UPDATE_DEAL, operateDesc);
    	} catch (Exception e) {
    		//保存日志出现异常 不进行处理
    		log.error("未开奖开奖处理，保存添加操作日志出错", e);
    	}
    	//设置扩展字段值
    	lotteryCode.setLotteryType(lotteryCode.getLotteryName());
    	
    	OrderQuery query = new OrderQuery();
    	query.setExpect(lotteryCode.getLotteryNum());  //期号
    	query.setLotteryType(kind.getCode()); //彩种
    	query.setProstateStatus(EProstateStatus.DEALING.name()); //处理中
    	query.setOrderSort(5);
    	//拼接开奖号码
    	String openCodeStr = lotteryCode.getOpencodeStr(); 
		
    	//查询需要处理未追号的订单
    	List<Order> dealOrders = orderService.getOrderByCondition(query);
    	
    	//查询到有订单起新线程处理中奖订单
    	int count = 0;
		if(CollectionUtils.isNotEmpty(dealOrders)) {
			count = dealOrders.size();
    		log.info("新增开奖处理,彩种["+lotteryKindName+"],期号["+lotteryCode.getLotteryNum()+"],订单数["+dealOrders.size()+"]");
			MainOpenDealThread mainOpenDealThread = new MainOpenDealThread(lotteryCode, dealOrders, openCodeStr);
    		mainOpenDealThread.start();
		}
		return AjaxUtil.createReturnValueSuccess(count);
    }
    
	
    /**
     * 错误开奖处理
     * @return
     */
    public Object[] hendErrUpdateDeal(ELotteryKind kind,String openCode,String expect){
    	if(kind == null || openCode == null || expect == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
		String[] nums = openCode.split(ConstantUtil.OPENCODE_PLIT);
		log.info("开奖类型:" + kind.name());
		log.info("开奖号码:" + openCode);
		LotteryKindService lotteryKindService = (LotteryKindService)ApplicationContextUtil.getBean(kind.getServiceClassName());
		log.info("实例:" + lotteryKindService);
		boolean lotteryCodeCheckResult = lotteryKindService.lotteryCodesCheckByOpenCode(openCode);
        if(!lotteryCodeCheckResult){
    		return AjaxUtil.createReturnValueError("开奖号码审核未通过.");
        }
		//操作日志保存
        Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
        String lotteryKindName = ELotteryKind.valueOf(kind.name()).getDescription();
        String operateDesc = AdminOperateUtil.constructMessage("hendErrUpdateDeal_log", currentAdmin.getUsername(), lotteryKindName, expect, openCode);
        String operationIp = BaseDwrUtil.getRequestIp();
        adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.HAND_UPDATE_DEAL, operateDesc);
		
        LotteryCode lotteryCode= null;
		lotteryCode = new LotteryCode();
		lotteryCode.setLotteryNum(expect);
		lotteryCode.setKjtime(new Date());
		lotteryCode.setAddtime(new Date());
		lotteryCode.setLotteryName(kind.name());
		if(nums.length > 0){
			lotteryCode.setNumInfo1(nums[0]);
		}
		if(nums.length > 1){
			lotteryCode.setNumInfo2(nums[1]);
		}
		if(nums.length > 2){
			lotteryCode.setNumInfo3(nums[2]);
		}
		if(nums.length > 3){
			lotteryCode.setNumInfo4(nums[3]);
		}		
		if(nums.length > 4){
			lotteryCode.setNumInfo5(nums[4]);
		}
		if(nums.length > 5){
			lotteryCode.setNumInfo6(nums[5]);
		}
		if(nums.length > 6){
			lotteryCode.setNumInfo7(nums[6]);
		}
		if(nums.length > 7){
			lotteryCode.setNumInfo8(nums[7]);
		}
		if(nums.length > 8){
			lotteryCode.setNumInfo9(nums[8]);
		}		
		if(nums.length > 9){
			lotteryCode.setNumInfo10(nums[9]);
		}	
		lotteryCode.setProstateDeal(0);  //默认设置未还未进行中奖的处理
		List<User> withdrawLockUsers = new ArrayList<User>();
		try {
			withdrawLockUsers = lotteryService.lotteryErrOpenCodeDeal(kind, lotteryCode);
		} catch (UnEqualVersionException e1) {
			log.error("错误开奖处理失败，更新用户记录时发生版本号不一致，请重试", e1);
			return AjaxUtil.createReturnValueError("错误开奖处理失败，更新用户记录时发生版本号不一致，请重试");
		} catch (Exception e1) {
			log.error("错误开奖处理失败", e1);
			return AjaxUtil.createReturnValueError("错误开奖处理失败");
		}
		
    	return AjaxUtil.createReturnValueSuccess(withdrawLockUsers);
    }
    
    
    /**
     * 下面是幸运快三和幸运分分彩手动开奖处理
     *
     */
    /**
     * 手动开奖处理-幸运彩
     * @return
     */
    public Object[] hendUpdateDealForSelfKind(String bizSystem,ELotteryKind kind,String openCode,String expect){
    	if(bizSystem== null || kind == null || openCode == null || expect == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	if(!"SUPER_SYSTEM".equals(currentAdmin.getBizSystem())){
    		bizSystem = currentAdmin.getBizSystem();
    	}
		LotteryCodeXy lotteryCode= null;
		String[] nums = openCode.split(ConstantUtil.OPENCODE_PLIT);
		log.info("业务系统:" + bizSystem);
		log.info("开奖类型:" + kind.name());
		log.info("开奖号码:" + openCode);
		LotteryKindService lotteryKindService = (LotteryKindService)ApplicationContextUtil.getBean(kind.getServiceClassName());
		log.info("实例:" + lotteryKindService);
		boolean lotteryCodeCheckResult = lotteryKindService.lotteryCodesCheckByOpenCode(openCode);
        if(!lotteryCodeCheckResult){
    		return AjaxUtil.createReturnValueError("开奖号码审核未通过.");
        }
		lotteryCode = lotteryCodeXyService.getLotteryCodeByKindAndExpect(kind.name(), expect,bizSystem);
		if(lotteryCode != null){
    		return AjaxUtil.createReturnValueError("该期号开奖号码已经存在了,不能再次开奖.");
		}
		//操作日志保存
		try {
    		String lotteryKindName = ELotteryKind.valueOf(kind.name()).getDescription();
    		String operateDesc = AdminOperateUtil.constructMessage("hendUpdateDeal_log", currentAdmin.getUsername(), bizSystem+"."+lotteryKindName, expect, openCode);
    		String operationIp = BaseDwrUtil.getRequestIp();
    		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.HAND_UPDATE_DEAL, operateDesc);
    	} catch (Exception e) {
    		//保存日志出现异常 不进行处理
    		log.error("手动开奖处理，保存添加操作日志出错", e);
    	}
		
		lotteryCode = new LotteryCodeXy();
		lotteryCode.setLotteryNum(expect);
		lotteryCode.setKjtime(new Date());
		lotteryCode.setAddtime(new Date());
		lotteryCode.setLotteryName(kind.name());
		lotteryCode.setBizSystem(bizSystem);
		if(nums.length > 0){
			lotteryCode.setNumInfo1(nums[0]);
		}
		if(nums.length > 1){
			lotteryCode.setNumInfo2(nums[1]);
		}
		if(nums.length > 2){
			lotteryCode.setNumInfo3(nums[2]);
		}
		if(nums.length > 3){
			lotteryCode.setNumInfo4(nums[3]);
		}		
		if(nums.length > 4){
			lotteryCode.setNumInfo5(nums[4]);
		}
		if(nums.length > 5){
			lotteryCode.setNumInfo6(nums[5]);
		}
		if(nums.length > 6){
			lotteryCode.setNumInfo7(nums[6]);
		}
		if(nums.length > 7){
			lotteryCode.setNumInfo8(nums[7]);
		}
		if(nums.length > 8){
			lotteryCode.setNumInfo9(nums[8]);
		}		
		if(nums.length > 9){
			lotteryCode.setNumInfo10(nums[9]);
		}
		lotteryCode.setProstateDeal(0);  //默认设置未还未进行中奖的处理
		try {
			CommonOpenCodeService.prostateForSelfKind(lotteryCode);
		} catch (Exception e) {
			log.error(e.getMessage());
	    	return AjaxUtil.createReturnValueError(e.getMessage());
		}
		
    	return AjaxUtil.createReturnValueSuccess();
    }
    
    /**
     * 部分未开奖处理-幸运彩
     * @return
     */
    public Object[] hendPartUpdateDealForSelfKind(String bizSystem,ELotteryKind kind, String expect){
    	if(kind == null || expect == null || bizSystem == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	if(!"SUPER_SYSTEM".equals(currentAdmin.getBizSystem())){
    		bizSystem = currentAdmin.getBizSystem();
    	}
    	
    	LotteryCodeXy lotteryCode = lotteryCodeXyService.getLotteryCodeByKindAndExpect(kind.getCode(), expect,bizSystem);
    	if(lotteryCode == null) {
    		return AjaxUtil.createReturnValueError("当前彩种期号开奖号码不存在，请使用手动开奖处理");
    	}
    	
    	//操作日志保存
		try {
    		String lotteryKindName = ELotteryKind.valueOf(kind.name()).getDescription();
    		String operateDesc = AdminOperateUtil.constructMessage("hendPartUpdateDeal_log", currentAdmin.getUsername(), bizSystem+"."+lotteryKindName, expect);
    		String operationIp = BaseDwrUtil.getRequestIp();
    		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.HAND_UPDATE_DEAL, operateDesc);
    	} catch (Exception e) {
    		//保存日志出现异常 不进行处理
    		log.error("部分未开奖处理，保存添加操作日志出错", e);
    	}
    	
    	OrderQuery query = new OrderQuery();
    	query.setExpect(lotteryCode.getLotteryNum());  //期号
    	query.setLotteryType(kind.getCode()); //彩种
    	query.setProstateStatus(EProstateStatus.DEALING.name()); //处理中
    	query.setOrderSort(5);
    	query.setBizSystem(bizSystem);
    	//拼接开奖号码
    	String openCodeStr = lotteryCode.getOpencodeStr(); 
		
    	//查询需要处理未追号的订单
    	List<Order> dealOrders = orderService.getOrderByCondition(query);
    	
    	//查询到有订单起新线程处理中奖订单
    	int count = 0;
		if(CollectionUtils.isNotEmpty(dealOrders)) {
			count = dealOrders.size();
    		log.info(bizSystem+"新增开奖处理,彩种["+lotteryCode.getLotteryName()+"],期号["+lotteryCode.getLotteryNum()+"],订单数["+dealOrders.size()+"]");
    		LotteryCode lotteryCode2 = new LotteryCode();
    		BeanUtilsExtends.copyProperties(lotteryCode2, lotteryCode);
    		MainOpenDealThread mainOpenDealThread = new MainOpenDealThread(lotteryCode2, dealOrders, openCodeStr);
    		mainOpenDealThread.start();
		}
		
		return AjaxUtil.createReturnValueSuccess(count);
    }
    
    
    /**
     * 错误开奖处理-幸运彩
     * @return
     */
    public Object[] hendErrUpdateDealForSelfKind(String bizSystem,ELotteryKind kind,String openCode,String expect){
    	if(bizSystem== null || kind == null || openCode == null || expect == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    	if(!"SUPER_SYSTEM".equals(currentAdmin.getBizSystem())){
    		bizSystem = currentAdmin.getBizSystem();
    	}
		LotteryCodeXy lotteryCode= null;
		String[] nums = openCode.split(ConstantUtil.OPENCODE_PLIT);
		log.info("业务系统:" + bizSystem);
		log.info("开奖类型:" + kind.name());
		log.info("开奖号码:" + openCode);
		LotteryKindService lotteryKindService = (LotteryKindService)ApplicationContextUtil.getBean(kind.getServiceClassName());
		log.info("实例:" + lotteryKindService);
		boolean lotteryCodeCheckResult = lotteryKindService.lotteryCodesCheckByOpenCode(openCode);
        if(!lotteryCodeCheckResult){
    		return AjaxUtil.createReturnValueError("开奖号码审核未通过.");
        }
		//操作日志保存
        String lotteryKindName = ELotteryKind.valueOf(kind.name()).getDescription();
        String operateDesc = AdminOperateUtil.constructMessage("hendErrUpdateDeal_log", currentAdmin.getUsername(), bizSystem+"."+lotteryKindName, expect, openCode);
        String operationIp = BaseDwrUtil.getRequestIp();
        adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.HAND_UPDATE_DEAL, operateDesc);
		
        lotteryCode = new LotteryCodeXy();
		lotteryCode.setLotteryNum(expect);
		lotteryCode.setKjtime(new Date());
		lotteryCode.setAddtime(new Date());
		lotteryCode.setLotteryName(kind.name());
		lotteryCode.setBizSystem(bizSystem);
		if(nums.length > 0){
			lotteryCode.setNumInfo1(nums[0]);
		}
		if(nums.length > 1){
			lotteryCode.setNumInfo2(nums[1]);
		}
		if(nums.length > 2){
			lotteryCode.setNumInfo3(nums[2]);
		}
		if(nums.length > 3){
			lotteryCode.setNumInfo4(nums[3]);
		}		
		if(nums.length > 4){
			lotteryCode.setNumInfo5(nums[4]);
		}
		if(nums.length > 5){
			lotteryCode.setNumInfo6(nums[5]);
		}
		if(nums.length > 6){
			lotteryCode.setNumInfo7(nums[6]);
		}
		if(nums.length > 7){
			lotteryCode.setNumInfo8(nums[7]);
		}
		if(nums.length > 8){
			lotteryCode.setNumInfo9(nums[8]);
		}		
		if(nums.length > 9){
			lotteryCode.setNumInfo10(nums[9]);
		}	
		lotteryCode.setProstateDeal(0);  //默认设置未还未进行中奖的处理
		
		List<User> withdrawLockUsers = new ArrayList<User>();
		try {
			withdrawLockUsers = lotteryService.lotteryErrOpenCodeDealForSelfKind(bizSystem, kind, lotteryCode);
		} catch (UnEqualVersionException e1) {
			log.error("错误开奖处理失败，更新用户记录时发生版本号不一致，请重试", e1);
			return AjaxUtil.createReturnValueError("错误开奖处理失败，更新用户记录时发生版本号不一致，请重试");
		} catch (Exception e1) {
			log.error("错误开奖处理失败", e1);
			return AjaxUtil.createReturnValueError("错误开奖处理失败");
		}
    	return AjaxUtil.createReturnValueSuccess(withdrawLockUsers);
    }
    
    
    
    /**
     * 删除某个时间之前的数据
     * @return
     */
    public Object[] deleteAgoDataFromThisTime(Date endTime){
    	if(endTime == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	
    	//保存操作日志
    	try {
    		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    		String operateDesc = AdminOperateUtil.constructMessage("cleandata_biz", currentAdmin.getUsername(), sdf.format(endTime));
    		String operationIp = BaseDwrUtil.getRequestIp();
    		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.BAKDATA_MANAGE, operateDesc);
    	} catch (Exception e) {
    		//保存日志出现异常 不进行处理
    		log.error("删除某个时间之前的数据，保存添加操作日志出错", e);
    	}
    	
		try {
		   systemService.deleteAgoDataFromThisTime(endTime);
		} catch (Exception e) {
	    	return AjaxUtil.createReturnValueError(e.getMessage());
		}
    	return AjaxUtil.createReturnValueSuccess();
    }
    
    
    /**
     * 删除常规数据在某个时间之前的数据
     * @return
     */
    public Object[] deleteAgoDataNormalFromThisTime(Date endTime, DeleteAgoDataInfo deleteDataInfo){
    	if(endTime == null || deleteDataInfo == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	
    	//校验必填
    	if(deleteDataInfo.getIsDeleteOrder() == null && 
    			deleteDataInfo.getIsDeleteMoneyDetail() == null && deleteDataInfo.getIsDeleteNotes() == null &&
    			deleteDataInfo.getIsDeleteDrawOrder() == null && deleteDataInfo.getIsDeleteLogin() == null &&
    			deleteDataInfo.getIsDeleteLotteryCode() == null && deleteDataInfo.getIsDeleteOperateLog() == null && 
    			deleteDataInfo.getIsDeleteUseMoneyLog() == null && deleteDataInfo.getIsDeleteLotteryCache() == null &&
    			deleteDataInfo.getIsDeleteRecordLog() == null) {
    		return AjaxUtil.createReturnValueError("必须选择删除的内容");
    	}
    	
    	//判断时间
    	Date endDate = DateUtil.getNowStartTimeByEnd(endTime);
    	if(endDate.getTime() > DateUtil.getNowStartTimeByStart(DateUtil.addDaysToDate(new Date(), -ConstantUtil.DELETE_AGO_DATA_NORMAL)).getTime()){
    		return AjaxUtil.createReturnValueError("" + ConstantUtil.DELETE_AGO_DATA_NORMAL.toString() + "天之前的数据不能删除.");
    	}
    	
    	//保存操作日志
    	try {
    		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    		String operateDesc = AdminOperateUtil.constructMessage("cleandata_biz", currentAdmin.getUsername(), sdf.format(endTime));
    		String operationIp = BaseDwrUtil.getRequestIp();
    		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.BAKDATA_MANAGE, operateDesc  + getDelInfoStr(deleteDataInfo));
    	} catch (Exception e) {
    		//保存日志出现异常 不进行处理
    		log.error("删除某个时间之前的数据，保存添加操作日志出错", e);
    	}
    	
		try {
		   systemService.deleteAgoDataFromThisTime(deleteDataInfo, endTime);
		} catch (Exception e) {
	    	return AjaxUtil.createReturnValueError(e.getMessage());
		}
    	return AjaxUtil.createReturnValueSuccess();
    }
    
    /**
     * 删除非常规数据在某个时间之前的数据
     * @return
     */
    public Object[] deleteAgoDataUnNormalFromThisTime(Date endTime, DeleteAgoDataInfo deleteDataInfo){
    	if(endTime == null || deleteDataInfo == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	
    	//校验必填
    	if(deleteDataInfo.getIsDeleteTreatmentApply() == null && 
    			deleteDataInfo.getIsDeleteTreatmentOrder() == null &&
    					deleteDataInfo.getIsDeleteQuotaDetail() == null) {
    		return AjaxUtil.createReturnValueError("必须选择删除的内容");
    	}
    	
    	//判断时间
    	Date endDate = DateUtil.getNowStartTimeByEnd(endTime);
    	if(endDate.getTime() > DateUtil.getNowStartTimeByStart(DateUtil.addDaysToDate(new Date(), -ConstantUtil.DELETE_AGO_DATA_NUNORMAL)).getTime()){
    		return AjaxUtil.createReturnValueError("" + ConstantUtil.DELETE_AGO_DATA_NUNORMAL.toString() + "天之前的数据不能删除.");
    	}
    	
    	
    	
    	//保存操作日志
    	try {
    		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    		String operateDesc = AdminOperateUtil.constructMessage("cleandata_biz", currentAdmin.getUsername(), sdf.format(endTime));
    		String operationIp = BaseDwrUtil.getRequestIp();
    		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.BAKDATA_MANAGE, operateDesc + getDelInfoStr(deleteDataInfo));
    	} catch (Exception e) {
    		//保存日志出现异常 不进行处理
    		log.error("删除某个时间之前的数据，保存添加操作日志出错", e);
    	}
    	
		try {
		   systemService.deleteAgoDataFromThisTime(deleteDataInfo, endTime);
		} catch (Exception e) {
	    	return AjaxUtil.createReturnValueError(e.getMessage());
		}
    	return AjaxUtil.createReturnValueSuccess();
    }
    
    private String getDelInfoStr(DeleteAgoDataInfo deleteDataInfo) {
    	StringBuffer sb = new StringBuffer();
    	sb.append(",删除了[");
    	if(deleteDataInfo.getIsDeleteOrder() != null && deleteDataInfo.getIsDeleteOrder() == 1) {
    		sb.append("投注历史,");
		}
		if(deleteDataInfo.getIsDeleteMoneyDetail() != null && deleteDataInfo.getIsDeleteMoneyDetail() == 1) {
			sb.append("资金明细,");
		}
		if(deleteDataInfo.getIsDeleteNotes() != null && deleteDataInfo.getIsDeleteNotes() == 1) {
			sb.append("收件箱,");
		}
		if(deleteDataInfo.getIsDeleteDrawOrder() != null && deleteDataInfo.getIsDeleteDrawOrder() == 1) {
			sb.append("充值取现订单,");
		}
		if(deleteDataInfo.getIsDeleteLogin() != null && deleteDataInfo.getIsDeleteLogin() == 1) {
			sb.append("登录日志,");
		}
		if(deleteDataInfo.getIsDeleteLotteryCode() != null && deleteDataInfo.getIsDeleteLotteryCode() == 1) {
			sb.append("开奖号码,");
		}
		if(deleteDataInfo.getIsDeleteOperateLog() != null && deleteDataInfo.getIsDeleteOperateLog() == 1) {
			sb.append("操作日志,");
		}
		if(deleteDataInfo.getIsDeleteUseMoneyLog() != null && deleteDataInfo.getIsDeleteUseMoneyLog() == 1) {
			sb.append("用户资金日志,");
		}
		if(deleteDataInfo.getIsDeleteLotteryCache() != null && deleteDataInfo.getIsDeleteLotteryCache() == 1) {
			sb.append("投注缓存,");
		}
		if(deleteDataInfo.getIsDeleteRecordLog() != null && deleteDataInfo.getIsDeleteRecordLog() == 1) {
			sb.append("操作明细,");
		}
		
		if(deleteDataInfo.getIsDeleteTreatmentApply() != null && deleteDataInfo.getIsDeleteTreatmentApply() == 1) {
			sb.append("代理福利申请,");
		}
		if(deleteDataInfo.getIsDeleteTreatmentOrder() != null && deleteDataInfo.getIsDeleteTreatmentOrder() == 1) {
			sb.append("代理福利订单,");
		}
		if(deleteDataInfo.getIsDeleteQuotaDetail() != null && deleteDataInfo.getIsDeleteQuotaDetail() == 1) {
			sb.append("配额明细,");
		}
		sb.substring(0, sb.length() -1);
		sb.append("]");
		return sb.toString();
    }
    
    /**
     * 删除多少天以上没有登陆及余额为0的会员
     * @param dayCount
     * @return
     */
    public Object[] deleteNoMoneyForNumberOfDays(Integer dayCount){
    	if(dayCount == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	if(dayCount < 30) {
    		return AjaxUtil.createReturnValueError("删除天数传递错误.");
    	}
    	//保存操作日志
    	try {
    		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    		String operateDesc = AdminOperateUtil.constructMessage("cleandata_noMoneyForNumberOfDays", currentAdmin.getUsername(), String.valueOf(dayCount));
    		String operationIp = BaseDwrUtil.getRequestIp();
    		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.BAKDATA_MANAGE, operateDesc);
    	} catch (Exception e) {
    		//保存日志出现异常 不进行处理
    		log.error("删除多少天以上没有登陆及余额为0的会员，保存添加操作日志出错", e);
    	}
    	Integer deleteCount = userService.deleteNoMoneyForNumberOfDays(dayCount);
		return AjaxUtil.createReturnValueSuccess(deleteCount);
    }
    
    /**
     * 删除多少天以上没有登陆的会员
     * @param dayCount
     * @return
     */
    public Object[] deleteForNumberOfDays(Integer dayCount){
    	if(dayCount == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	//保存操作日志
    	try {
    		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    		String operateDesc = AdminOperateUtil.constructMessage("cleandata_numberOfDays", currentAdmin.getUsername(), String.valueOf(dayCount));
    		String operationIp = BaseDwrUtil.getRequestIp();
    		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.BAKDATA_MANAGE, operateDesc);
    	} catch (Exception e) {
    		//保存日志出现异常 不进行处理
    		log.error("删除多少天以上没有登陆的会员，保存添加操作日志出错", e);
    	}
    	Integer deleteCount = userService.deleteForNumberOfDays(dayCount);
		return AjaxUtil.createReturnValueSuccess(deleteCount);
    }
    
    /**
     * 彻底删除无效会员
     * @return
     */
    public Object[] everyDeleteUsers(Date endTime, DeleteAgoDataInfo deleteDataInfo){
    	if(endTime == null || deleteDataInfo == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	//校验必填
    	if(deleteDataInfo.getSscRebate() == null ||
    			deleteDataInfo.getIsDeleteSafetyQuestions() == null ||
    			deleteDataInfo.getIsDeleteUserBank() == null) {
    		return AjaxUtil.createReturnValueError("必须选择删除的内容");
    	}
    	
    	//判断时间
    	Date endDate = DateUtil.getNowStartTimeByEnd(endTime);
    	if(endDate.getTime() > DateUtil.getNowStartTimeByStart(DateUtil.addDaysToDate(new Date(), -ConstantUtil.DELETE_AGO_DATA_EVERY)).getTime()){
    		return AjaxUtil.createReturnValueError("" + ConstantUtil.DELETE_AGO_DATA_EVERY.toString() + "天之前的数据不能删除.");
    	}
    	//判断模式是否存在
    	//ConstantUtil.SSC_COMPUTE_VALUE
    	//SystemSet.SSCLowestAwardModel
    	/*if(deleteDataInfo.getSscRebate().compareTo(ConstantUtil.SSC_COMPUTE_VALUE) > 0 || deleteDataInfo.getSscRebate().compareTo(SystemSet.SSCLowestAwardModel) < 0 ){
    		return AjaxUtil.createReturnValueError("当前输入模式不合法");
    	}*/
    	
    	//保存操作日志
    	try {
    		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    		String operateDesc = AdminOperateUtil.constructMessage("cleandata_everyDeleteUsers", currentAdmin.getUsername());
    		String operationIp = BaseDwrUtil.getRequestIp();
    		adminOperateLogService.saveAdminOperateLog(currentAdmin.getBizSystem(), operationIp, currentAdmin.getId(), currentAdmin.getUsername(), null, "", EAdminOperateLogModel.BAKDATA_MANAGE, operateDesc);
    	} catch (Exception e) {
    		//保存日志出现异常 不进行处理
    		log.error("彻底删除无效会员，保存添加操作日志出错", e);
    	}
    	
    	Integer deleteCount = userService.everyDeleteUsers(deleteDataInfo, endTime);
		return AjaxUtil.createReturnValueSuccess(deleteCount);
    }
    
    /**
     * 重启服务器
     * @deprecated 暂时无用
     * @return
     */
    public Object[] restartServer(String restartPass) {
    	if(StringUtils.isEmpty(restartPass)) {
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	if("jinying999".equals(restartPass)) {
    		Admin currentAdmin = BaseDwrUtil.getCurrentAdmin();
    		//需要超级后台系统
    		if(currentAdmin.getState() != Admin.SUPER_STATE) {
    			String shpath="/home/jinying126/serverrestart.sh";   //程序路径

    			log.info("开始执行重启shell命令");
    			boolean res = execCommand(shpath);
    			log.info("执行重启shell命令结束");
    			if(res) {
    				return AjaxUtil.createReturnValueSuccess();
    			} else {
    				return AjaxUtil.createReturnValueError("执行重启命令失败");
    			}
    		    
    		} else {
    			return AjaxUtil.createReturnValueError("当前用户无法进行重启操作.");
    		}
    	} else {
    		return AjaxUtil.createReturnValueError("重启密码错误.");
    	}
    }
    
    private boolean execCommand(String cmd) {
        Process process = null;        
        try {
              process = Runtime.getRuntime().exec(cmd);
              process.waitFor();             
        } catch (Exception e) {        
              return false;
        } finally {
            try {
                  process.destroy();             
            } catch (Exception e) {        
              }
        }
          return true;
    }
	/**
	 * 获取下载的文件名
	 * @return
	 */
	public Object[] getDownLoadFileName() {
		List<String> listName = new ArrayList<String>();
		if (ConstantUtil.FILE_PATH == null) {
			return AjaxUtil.createReturnValueError("参数传递错误.");
		}
		try {
			File file = new File(ConstantUtil.FILE_PATH);

			File[] tempList = file.listFiles();
			for (int i = 0; i < tempList.length; i++) {
				if (tempList[i].isFile()) {
					listName.add(tempList[i].getName());
				}
			}

		} catch (Exception e) {
			logger.error("读取文件内容出错", e);
		}

		return AjaxUtil.createReturnValueSuccess(listName);

	}

	/**
	 * 文件下载
	 * @param fileName
	 * @return
	 */
	public FileTransfer downLoadFile(String fileName) {
		File file = new File(ConstantUtil.FILE_PATH + "\\" + fileName);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		FileInputStream fis = null;
		BufferedInputStream bis = null;
		try {
			if (file.isDirectory()) {
				return null;
			} else if (file.isFile()) {
				fis = new FileInputStream(file);
				bis = new BufferedInputStream(fis);// 缓冲字节输入流

				fileName = new String(fileName.getBytes("GBK"), "iso8859-1");

				byte[] data = new byte[1024];
				int len1 = -1;

				while ((len1 = bis.read(data)) != -1) {
					bos.write(data, 0, len1);
				}
			}
		} catch (FileNotFoundException e) {
			logger.error("文件未找到", e);
		} catch (UnsupportedEncodingException e) {
			logger.error("转码失败",e);
		} catch (IOException e) {
			logger.error("IO流异常", e);

		} finally {
			try {
				if(bis != null) {
					bis.close();
				}
				if(fis != null) {
					fis.close();
				}
			} catch (IOException e) {
				logger.error("输入流关闭失败", e);
			}
		}

		return new FileTransfer(fileName, "text/plain", bos.toByteArray());
	}
	
	/**
	 * 去掉特殊开奖号码前面的0，处理开奖
	 * @param openCode
	 * @return
	 */
	private String getOpenCode(String openCode){
		String openCodeStr = "";
		String[] ocs = openCode.split(",");
		for (int i = 0; i < ocs.length; i++) {
			if (i<ocs.length-1) {
				if (ocs[i].startsWith("0")) {
					openCodeStr = openCodeStr + ocs[i].substring(1) + ",";
				} else {
					openCodeStr = openCodeStr + ocs[i] + ",";
				}
			}else {
				if (ocs[i].startsWith("0")) {
					openCodeStr = openCodeStr + ocs[i].substring(1);
				} else {
					openCodeStr = openCodeStr + ocs[i];
				}
			}
		}
		return openCodeStr;
	}
}
