package com.team.lottery.action.web.manage;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.team.lottery.extvo.Page;
import com.team.lottery.service.WithdrawAutoLogService;
import com.team.lottery.util.AjaxUtil;
import com.team.lottery.vo.WithdrawAutoLog;

@Controller("mangerWithdrawAutoLogAction")
public class MangerWithdrawAutoLogAction {

	@Autowired
	private WithdrawAutoLogService WithdrawAutoLogService;
	
	/**
	 * 分页查询
	 * @param query
	 * @param page
	 * @return
	 */
	public Object[] getAllWithdrawAutoLog(WithdrawAutoLog query,Page page){
		if(query==null||page==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		page=WithdrawAutoLogService.getAllWithdrawAutoLog(query, page);
		return AjaxUtil.createReturnValueSuccess(page);
	}
	
	/**
	 * 根据ID查询
	 * @param id
	 * @return
	 */
	public Object[] selectWithdrawAutoLog(Long id){
		WithdrawAutoLog record=WithdrawAutoLogService.selectByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess(record);
	}
	
	/**
	 * 修改
	 * @param record
	 * @return
	 */
	public Object[] updateWithdrawAutoLog(WithdrawAutoLog record){
		if(record==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		WithdrawAutoLogService.updateByPrimaryKeySelective(record);
		return AjaxUtil.createReturnValueSuccess();
	}
	
	/**
	 * 根据ID删除
	 * @param id
	 * @return
	 */
	public Object[] delWithdrawAutoLog(Long id){
		if(id==null){
			return AjaxUtil.createReturnValueError("参数错误");
		}
		WithdrawAutoLogService.deleteByPrimaryKey(id);
		return AjaxUtil.createReturnValueSuccess();
	}
	
	/**
	 * 保存
	 * @param record
	 * @return
	 */
/*	public Object[] saveWithdrawAutoLog(WithdrawAutoLog record){
    	if(record == null){
    		return AjaxUtil.createReturnValueError("参数传递错误.");
    	}
    	Long id = record.getId();
    	if(id == null){
    		record.setCreatedDate(new Date());
    		WithdrawAutoLogService.insertSelective(record);
    	}else{
    		record.setUpdatedDate(new Date());
    		WithdrawAutoLogService.updateByPrimaryKeySelective(record);
    	}
    	return AjaxUtil.createReturnValueSuccess();
    }*/
}
