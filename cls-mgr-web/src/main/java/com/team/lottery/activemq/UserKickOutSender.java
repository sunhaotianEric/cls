package com.team.lottery.activemq;

import java.io.Serializable;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

import com.team.lottery.extvo.UserKickOutMessageVo;
import com.team.lottery.util.ApplicationContextUtil;

/**
 * 用户踢出操作发送队列
 * @author luocheng
 *
 */
@Component
public class UserKickOutSender {

	private static Logger logger = LoggerFactory.getLogger(UserKickOutSender.class);
	
	// 要发送的队列集合
	@Value("${activemq.user_kickout_queues}")
	private String userKickOutQueues;
	
	
	/**
	 * 发送幸运彩数据到MQ
	 * 
	 * @param object
	 */
	public void sendMessage(final UserKickOutMessageVo message) {

		try {
			
			// 改用spring-jms来实现
			JmsTemplate jmsTemplate = ApplicationContextUtil.getBean(JmsTemplate.class);
			if (jmsTemplate != null) {
				if(StringUtils.isNoneBlank(userKickOutQueues)) {
					String[] userKickOutQueueArrys = userKickOutQueues.split(",");
					for(String userKickOutQueue : userKickOutQueueArrys) {
						// 往发送队列发送消息
						logger.info("往队列[{}]发送踢出用户消息, 业务系统[{}],用户名[{}]", userKickOutQueue
								, message.getBizSystem(), message.getUserName());
						jmsTemplate.send(userKickOutQueue, new MessageCreator() {

							@Override
							public Message createMessage(Session mysession) throws JMSException {
								return mysession.createObjectMessage((Serializable) message);
							}
						});
					}
				} else {
					logger.error("要发送的踢出队列集合配置为空");
				}
				
			} else {
				throw new Exception("获取jmsTemplate为空");
			}

		} catch (Exception e) {
			logger.error("UserKickOutSender发送消息失败", e);
		}
	}

}
