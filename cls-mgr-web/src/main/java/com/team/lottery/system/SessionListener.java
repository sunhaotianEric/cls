package com.team.lottery.system;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.User;

public class SessionListener implements HttpSessionListener{
	
	private static Logger logger = LoggerFactory.getLogger(SessionListener.class);

	@Override
	public void sessionCreated(HttpSessionEvent se) {
		String sessionId = se.getSession().getId();
		logger.info("sessionCreated sessionId:{}", sessionId);
//		if (logger.isDebugEnabled()) {
//			logger.debug("sessionCreated sessionId:{}", sessionId);
//		}
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		HttpSession session = se.getSession();
		String sessionId = se.getSession().getId();
		
//		User loginUser = (User) session.getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
//		if (loginUser!=null) {
//			String userName = loginUser.getUserName();
//			String loginType = loginUser.getLoginType();
//			String bizSystem = loginUser.getBizSystem();
//			OnlineUserManager.delOnlineUser(loginType, bizSystem, userName, sessionId);
//		}
		logger.info("sessionDestroyed sessionId:{}", sessionId);
//		if (logger.isDebugEnabled()) {
//			logger.debug("sessionDestroyed sessionId:{}", sessionId);
//		}
		Admin currentAdmin = (Admin)session.getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_ADMIN);
		if(currentAdmin != null) {
			String onlineAdminSetKey = SystemPropeties.loginType + "." + currentAdmin.getBizSystem();
			// 删除管理员在线信息，并删除sessionId记录
			MgrAdminOnlineOperater.delAdminOnline(onlineAdminSetKey, currentAdmin.getUsername(), sessionId);
		}
		session.invalidate();
	}

}
