package com.team.lottery.system;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.session.data.redis.RedisOperationsSessionRepository;

import com.team.lottery.redis.JedisUtils;
import com.team.lottery.util.ApplicationContextUtil;

/**
 * 后台管理员在线操作对象
 * @author luocheng
 *
 */
public class MgrAdminOnlineOperater {
	
	private static Logger logger = LoggerFactory.getLogger(MgrAdminOnlineOperater.class);

	//存放后台会话ID集合的redis Map的key名称
	public static final String mgrSessionIdMapKey = "mgrSessionIdMap";
	
	/**
	 * 新增管理员在线信息,并踢出之前相同管理员，新增新的sessionId记录
	 */
	public static void addAdminOnline(String key, String adminName, String newSessionId) {
		//新增在线管理员记录
	    SingleLoginForRedis.sAddRedisSet(key, adminName);
	    logger.info("新增在线登陆redis记录,key={},value={}", key, adminName);
	    
	    //判断是否已经有管理员的sessionId在线，有的话进行踢出
	    Map<String, String> mgrSessionIdMap = JedisUtils.getMap(mgrSessionIdMapKey);
	    if(mgrSessionIdMap != null) {
		    String adminSessionId = mgrSessionIdMap.get(adminName);
		    // 如果新的会话ID和原来存储的会话ID不一致才处理，一致有可能是已经登录又重新再登录的情况
		    if(StringUtils.isNoneBlank(adminSessionId) && !adminSessionId.equals(newSessionId)) {
		    	//踢出操作
		    	RedisOperationsSessionRepository sessionRepository = ApplicationContextUtil.getBean(RedisOperationsSessionRepository.class);
		    	sessionRepository.delete(adminSessionId);
	    		
		    	//移除旧的sessionId记录
		    	JedisUtils.mapRemove(mgrSessionIdMapKey, adminName);
		    	logger.info("移除已有的sessionId记录,key[{}],value[{}]", adminName, adminSessionId);
		    }
	    }
	    //存储新用户sessionId
	    Map<String, String> newSessionRecord = new HashMap<String, String>();
	    newSessionRecord.put(adminName, newSessionId);
	    JedisUtils.mapPut(mgrSessionIdMapKey, newSessionRecord);
	    logger.info("新增新的sessionId记录,key[{}],value[{}]", adminName, newSessionId);
	}
	
	/**
	 * 删除管理员在线信息，并删除sessionId记录
	 * @param adminName
	 * @param sessionId
	 */
	public static void delAdminOnline(String key, String adminName, String sessionId) {
		//删除在线管理员记录
	    logger.info("删除在线登陆redis记录,key={},value={}", key, adminName);
		SingleLoginForRedis.sRemoveRedisSet(key, adminName);
		
		//移除已有的管理员sessionId记录
		Map<String, String> mgrSessionIdMap = JedisUtils.getMap(mgrSessionIdMapKey);
	    if(mgrSessionIdMap != null) {
		    String adminSessionId = mgrSessionIdMap.get(adminName);
		    if(StringUtils.isNoneBlank(adminSessionId)) {
		    	//移除旧的sessionId记录
		    	JedisUtils.mapRemove(mgrSessionIdMapKey, adminName);
		    	logger.info("移除已有的sessionId记录,key[{}],value[{}]", adminName, adminSessionId);
		    }
	    }
	}
	
	/**
	 * 踢出管理员，并删除在线信息和sessionId记录
	 * @param key
	 * @param adminName
	 */
	public static void kickOutAdmin(String key, String adminName) {
		//判断是否已经有管理员的sessionId在线，有的话进行踢出
	    Map<String, String> mgrSessionIdMap = JedisUtils.getMap(mgrSessionIdMapKey);
	    if(mgrSessionIdMap != null) {
		    String adminSessionId = mgrSessionIdMap.get(adminName);
		    if(StringUtils.isNoneBlank(adminSessionId)) {
		    	//踢出操作
		    	RedisOperationsSessionRepository sessionRepository = ApplicationContextUtil.getBean(RedisOperationsSessionRepository.class);
		    	sessionRepository.delete(adminSessionId);
	    		
		    	//移除旧的sessionId记录
		    	JedisUtils.mapRemove(mgrSessionIdMapKey, adminName);
		    	logger.info("移除已有的sessionId记录,key[{}],value[{}]", adminName, adminSessionId);
		    }
	    }
	    
	    //删除在线管理员记录
	    logger.info("删除在线登陆redis记录,key={},value={}", key, adminName);
		SingleLoginForRedis.sRemoveRedisSet(key, adminName);
	
	}
	
	
		
}
