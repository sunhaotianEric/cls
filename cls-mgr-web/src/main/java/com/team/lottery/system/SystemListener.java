package com.team.lottery.system;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.cache.ClsMgrCacheManager;
import com.team.lottery.enums.ERedisChannel;
import com.team.lottery.redis.mq.RedisThread;
import com.team.lottery.system.cache.LhcWinKindPlayCache;
import com.team.lottery.system.message.MessageQueueThread;
import com.team.lottery.util.GetApplicationPropertiesValueUtil;


public class SystemListener implements ServletContextListener{

	private static Logger logger = LoggerFactory.getLogger(SystemListener.class);
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		WebApplicationContext app = WebApplicationContextUtils.getWebApplicationContext(arg0.getServletContext());
		
		//缓存数据初始化
		SystemCache.loadAllCacheData();
		
				
		//初始化当前系统常量配置
		SystemPropeties.serverNum = "2";
		SystemPropeties.loginType = "ADMIN";

		// 初始化静态常量.
		String key = "officialLotteryOpen";
		String propertiesValue = GetApplicationPropertiesValueUtil.getPropertiesValue(key);
		GetApplicationPropertiesValueUtil.OFFICIALLOTTERYOPEN = propertiesValue;
		logger.info("根据配置文件application.properties中到根据key值[{}]得到value值[{}]",key,GetApplicationPropertiesValueUtil.OFFICIALLOTTERYOPEN);
		
		//初始化缓存
		ClsCacheManager.initCache();
		ClsMgrCacheManager.initMgrBizSystemDomainCache();
		//初始化业务系统参数配置
		BizSystemConfigManager.initAllBizSystemConfig();
		//初始化六合彩的彩种赔率
		LhcWinKindPlayCache.initLhcWinKindPlay();
		
		//启动消息接收处理线程
		MessageQueueThread messageQueueThread = new MessageQueueThread();
		messageQueueThread.start();
		
		//启动订阅线程
		new Thread(new RedisThread(ERedisChannel.CLSCACHE.name())).start();
		
	}

}
