package com.team.lottery.util.issue;

import java.util.Date;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.mapper.lotteryissue.LotteryIssueMapper;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.DateUtils;
import com.team.lottery.vo.LotteryIssue;

/**
 * 五分时时彩开售时间生成工具类
 * @author luocheng
 *
 */
public class LotteryIssueUtilForWfssc {
	public static LotteryIssueMapper lotteryIssueMapper;
	
	static{
		String[] locations = {"spring/applicationContext*.xml"};
		ApplicationContext context = new ClassPathXmlApplicationContext(locations);
		
		lotteryIssueMapper = context.getBean(LotteryIssueMapper.class);
	}
	
	public static void main(String[] args) {
		// 五分时时彩
        String dateControlStr = ELotteryKind.WFSSC.getDateControlStr();
        // 第一期开始销售时间
        Date startDate = DateUtils.getDateByStrFormat(dateControlStr + " " + "00:00:00", "yyyy-MM-dd HH:mm:ss");
        // 时间间隔 5分钟
        int timeMinInterval = 5; 
        // 总期数
        int totalIssueNo = 288;
        // 每期开始时间
        Date issueStartDate = startDate;
        for(int num = 1; num <= totalIssueNo; num++){
    		LotteryIssue lotteryIssue = new LotteryIssue();
    		lotteryIssue.setLotteryName(ELotteryKind.WFSSC.getDescription());
    		lotteryIssue.setLotteryType(ELotteryKind.WFSSC.getCode());
    		// 每期结束时间
    		Date issueEndDate = DateUtil.addMinutesToDate(issueStartDate, timeMinInterval);
    		lotteryIssue.setBegintime(issueStartDate);
    		lotteryIssue.setEndtime(issueEndDate);
    		lotteryIssue.setIssale(1);
    		// 期号字符串处理，补足三位期数
    		String lotteryNum = "";
    		if(num < 10){
    			lotteryNum = "00" + num;
    		} else if(num < 100){
    			lotteryNum = "0" + num;
    		}else{
    			lotteryNum = "" + num;
    		}
    		lotteryIssue.setLotteryNum(lotteryNum);
    		lotteryIssue.setState(1);
    		lotteryIssueMapper.insertSelective(lotteryIssue);
    		System.out.println("五分时时彩当前插入第{"+ lotteryNum +"}期");
    		// 每次循环结束，将结束时间放到下一期开始时间
    		issueStartDate = issueEndDate;
        }
		
	}
}
