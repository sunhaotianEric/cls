package com.team.lottery.util.issue;

import java.util.Date;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.mapper.lotterynumber.LotteryNumberMapper;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.DateUtils;
import com.team.lottery.vo.LotteryNumber;

/**
 * 老重庆时时彩开售时间生成工具类-lotteryNumber
 * @author listgoo
 * @Description: 
 * @date: 2019年3月31日
 */
public class LotteryNumberUtilForLcqssc {
	
	public static LotteryNumberMapper lotteryNumberMapper;
	
	static{
		String[] locations = {"spring/applicationContext*.xml"};
		ApplicationContext context = new ClassPathXmlApplicationContext(locations);
		
		lotteryNumberMapper = context.getBean(LotteryNumberMapper.class);
	}
	
	public static void main(String[] args) {
		// 老重庆时时彩
        String dateControlStr = ELotteryKind.LCQSSC.getDateControlStr();
        // 第一期开始销售时间
        Date startDate = DateUtils.getDateByStrFormat(dateControlStr + " " + "00:04:50", "yyyy-MM-dd HH:mm:ss");
     // 第1~24期每5分钟一期，第24~96期每10分钟一期，第96~120期每5分钟一期
        int timeMinInterval_5 = 5; 
        int timeMinInterval_10 = 10;  
        // 总期数
        int totalIssueNo = 120;
        // 每期开始时间
        Date issueStartDate = startDate;
        for(int num = 1; num <= totalIssueNo; num++){
    		LotteryNumber lotteryIssue = new LotteryNumber();
    		lotteryIssue.setLotteryName(ELotteryKind.LCQSSC.getDescription());
    		lotteryIssue.setLotteryType(ELotteryKind.LCQSSC.getCode());
    		// 每期结束时间
    		Date issueEndDate = null;
    		if (num == 24) {
    			issueStartDate = DateUtils.getDateByStrFormat(dateControlStr + " " + "09:59:20", "yyyy-MM-dd HH:mm:ss");
    		}
    		if (num == 96) {
    			issueStartDate = DateUtils.getDateByStrFormat(dateControlStr + " " + "21:59:50", "yyyy-MM-dd HH:mm:ss");
    		}
    		if ((num < 24) || (num > 95)) {
    			issueEndDate = DateUtil.addMinutesToDate(issueStartDate, timeMinInterval_5);
			}else {
				issueEndDate = DateUtil.addMinutesToDate(issueStartDate, timeMinInterval_10);
			}
    		lotteryIssue.setBeginTime(issueStartDate);
    		lotteryIssue.setEndTime(issueEndDate);
    		lotteryIssue.setOrders(num);
    		// 期号字符串处理，补足三位期数
    		String lotteryNum = "";
    		if(num < 10){
    			lotteryNum = "00" + num;
    		} else if(num < 100){
    			lotteryNum = "0" + num;
    		}else{
    			lotteryNum = "" + num;
    		}
    		lotteryIssue.setLotteryNum(lotteryNum);
    		lotteryNumberMapper.insertSelective(lotteryIssue);
    		System.out.println("老重庆时时彩当前插入第{"+ lotteryNum +"}期"+" 开始时间:"
    				+DateUtils.getDateToStrBySdfDate(lotteryIssue.getBeginTime())
    				+" 结束时间:"+DateUtils.getDateToStrBySdfDate(lotteryIssue.getEndTime()));
    		// 每次循环结束，将结束时间放到下一期开始时间
    		issueStartDate = issueEndDate;
        }
		
	}
}
