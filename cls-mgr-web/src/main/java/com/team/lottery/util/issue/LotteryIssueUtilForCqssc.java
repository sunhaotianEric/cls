package com.team.lottery.util.issue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.mapper.lotteryissue.LotteryIssueMapper;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.DateUtils;
import com.team.lottery.vo.LotteryIssue;

/**
 * 重庆时时彩
 * @author luocheng
 *
 */
public class LotteryIssueUtilForCqssc {

	public static LotteryIssueMapper lotteryIssueMapper;
	
	static{
		String[] locations = {"spring/applicationContext*.xml"};
		ApplicationContext context = new ClassPathXmlApplicationContext(locations);
		
		lotteryIssueMapper = context.getBean(LotteryIssueMapper.class);
	}
	
	public static void main(String[] args) {
		
		// 重庆时时彩
        String dateControlStr = ELotteryKind.CQSSC.getDateControlStr();
        // 第一期开始销售时间  接前一天的最后一期
        Date startDate = DateUtils.getDateByStrFormat(dateControlStr + " " + "23:48:30", "yyyy-MM-dd HH:mm:ss");
        startDate = DateUtil.addDaysToDate(startDate, -1);
        // 时间间隔 20分钟
        int timeMinInterval = 20; 
        // 总期数
        int totalIssueNo = 59;
        // 每期开始时间
        Date issueStartDate = startDate;
        for(int num = 1; num <= totalIssueNo; num++){
    		LotteryIssue lotteryIssue = new LotteryIssue();
    		lotteryIssue.setLotteryName(ELotteryKind.CQSSC.getDescription());
    		lotteryIssue.setLotteryType(ELotteryKind.CQSSC.getCode());
    		// 每期结束时间
    		Date issueEndDate = DateUtil.addMinutesToDate(issueStartDate, timeMinInterval);
    		// 第一期特殊处理
    		if(num == 1) {
    			issueEndDate = DateUtils.getDateByStrFormat(dateControlStr + " " + "00:28:30", "yyyy-MM-dd HH:mm:ss");
    		}
    		lotteryIssue.setBegintime(issueStartDate);
    		lotteryIssue.setEndtime(issueEndDate);
    		lotteryIssue.setIssale(1);
    		// 期号字符串处理，补足三位期数
    		String lotteryNum = "";
    		if(num < 10){
    			lotteryNum = "00" + num;
    		} else if(num < 100){
    			lotteryNum = "0" + num;
    		}else{
    			lotteryNum = "" + num;
    		}
    		lotteryIssue.setLotteryNum(lotteryNum);
    		lotteryIssue.setState(1);
    		lotteryIssueMapper.insertSelective(lotteryIssue);
    		System.out.println("重庆时时彩当前插入第{"+ lotteryNum +"}期");
    		// 每次循环结束，将结束时间放到下一期开始时间
    		issueStartDate = issueEndDate;
    		// 第10期开始，即当天三点之后的时间
    		if(num == 9) {
    			issueStartDate = DateUtils.getDateByStrFormat(dateControlStr + " " + "07:08:30", "yyyy-MM-dd HH:mm:ss");
    		}
        }
	}
}
