package com.team.lottery.util.issue;

import java.util.Date;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.mapper.lotterynumber.LotteryNumberMapper;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.DateUtils;
import com.team.lottery.vo.LotteryNumber;

/**
 * 五分pk10开售时间生成工具类-lotteryNumber
 * @author listgoo
 *
 * @date 2019年3月14日
 */
public class LotteryNumberUtilForWfpk10 {
	public static LotteryNumberMapper lotteryNumberMapper;
	
	static{
		String[] locations = {"spring/applicationContext*.xml"};
		ApplicationContext context = new ClassPathXmlApplicationContext(locations);
		
		lotteryNumberMapper = context.getBean(LotteryNumberMapper.class);
	}
	
	public static void main(String[] args) {
		// 五分pk10
        String dateControlStr = ELotteryKind.WFPK10.getDateControlStr();
        // 第一期开始销售时间
        Date startDate = DateUtils.getDateByStrFormat(dateControlStr + " " + "00:05:10", "yyyy-MM-dd HH:mm:ss");
        // 时间间隔5分钟
        int timeMinInterval = 5; 
        // 总期数
        int totalIssueNo = 288;
        // 每期开始时间
        Date issueStartDate = startDate;
        for(int num = 1; num <= totalIssueNo; num++){
    		LotteryNumber lotteryIssue = new LotteryNumber();
    		lotteryIssue.setLotteryName(ELotteryKind.WFPK10.getDescription());
    		lotteryIssue.setLotteryType(ELotteryKind.WFPK10.getCode());
    		// 每期结束时间
    		Date issueEndDate = DateUtil.addMinutesToDate(issueStartDate, timeMinInterval);
    		lotteryIssue.setBeginTime(issueStartDate);
    		lotteryIssue.setEndTime(issueEndDate);
    		lotteryIssue.setOrders(num);
    		// 期号字符串处理，补足三位期数
    		String lotteryNum = "";
    		if(num < 10){
    			lotteryNum = "00" + num;
    		} else if(num < 100){
    			lotteryNum = "0" + num;
    		}else{
    			lotteryNum = "" + num;
    		}
    		lotteryIssue.setLotteryNum(lotteryNum);
    		lotteryNumberMapper.insertSelective(lotteryIssue);
    		System.out.println("五分pk10当前插入第{"+ lotteryNum +"}期");
    		// 每次循环结束，将结束时间放到下一期开始时间
    		issueStartDate = issueEndDate;
        }
		
	}
}
