package com.team.lottery.util;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.directwebremoting.ScriptSession;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.User;

public class BaseDwrUtil {

	public static HttpServletRequest getRequest() {
		WebContext ctx = WebContextFactory.get();
		return ctx.getHttpServletRequest();
	}

	public static HttpServletResponse getResponse() {
		WebContext ctx = WebContextFactory.get();
		return ctx.getHttpServletResponse();
	}

	public static HttpSession getSession() {
		WebContext ctx = WebContextFactory.get();
		return ctx.getSession();
	}

	public static HttpSession getSession(boolean isNewSession) {
		WebContext ctx = WebContextFactory.get();
		return ctx.getSession();
	}

	public static void setRequestAttribute(String key, Object value) {
		getRequest().setAttribute(key, value);
	}

	public static Object getRequestAttribute(String key) {
		return getRequest().getAttribute(key);
	}

	public static void removeRequestAttribute(String key) {
		getRequest().removeAttribute(key);
	}

	public static void setSessionAttribute(String key, Object value) {
		getSession().setAttribute(key, value);
	}

	public static Object getSessionAttribute(String key) {
		return getSession().getAttribute(key);
	}

	public static void removeSessionAttribute(String key) {
		getSession().removeAttribute(key);
	}

	public static ServletContext getServletContext() {
		WebContext ctx = WebContextFactory.get();
		return ctx.getServletContext();
	}

	public static ScriptSession getScriptSession() {
		WebContext ctx = WebContextFactory.get();
		return ctx.getScriptSession();
	}

	// 设置登录的用户
	public static void setCurrentUser(User user) {
		user.setPassword(null);
		user.setSafePassword(null);
		setSessionAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER, user);
	}

	// 获取当前用户
	public static User getCurrentUser() {
		User user = (User) BaseDwrUtil
				.getSessionAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
		return user;
	}
	
	//获取当前业务系统简称
	public static String getCurrentSystem() {
		String curretSystem = (String)BaseDwrUtil.getSessionAttribute(ConstantUtil.CURRENT_SYSTEM);
		if(StringUtils.isEmpty(curretSystem)) {
			String serverName = getRequest().getHeader("REMOTE-HOST");
			if(StringUtils.isEmpty(serverName)) serverName =getRequest().getServerName();
			curretSystem = ClsCacheManager.getBizSystemByDomainUrl(serverName);
		}
		return curretSystem;
	}

	// 删除当前用户
	public static void removeCurrentUser() {
		BaseDwrUtil
				.removeSessionAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
		BaseDwrUtil.getSession().invalidate();
	}

	// 设置登录的admin
	public static void setCurrentAdmin(Admin admin) {
		admin.setPassword(null);
		setSessionAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_ADMIN, admin);
	}

	// 获取登录admin
	public static Admin getCurrentAdmin() {
		Admin staff = (Admin) BaseDwrUtil
				.getSessionAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_ADMIN);
		return staff;
	}

	// 删除当前admin
	public static void removeCurrentAdmin() {
		BaseDwrUtil
				.removeSessionAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_ADMIN);
		BaseDwrUtil.getSession().invalidate();
	}
	
	// 设置登录的秘密编码
	public static void setCurrentSecretKey(String code) {
		setSessionAttribute(ConstantUtil.MAIL_USER_SIGN, code);
	}

	// 获取登录秘密编码
	public static String getCurrentSecretKey() {
		String secret = (String) BaseDwrUtil.getSessionAttribute(ConstantUtil.MAIL_USER_SIGN);
		return secret;
	}

	// 删除当前秘密编码
	public static void removeCurrentSecretKey() {
		BaseDwrUtil.removeSessionAttribute(ConstantUtil.MAIL_USER_SIGN);
	}

	// 获取当前的验证码
	public static String getCurrentCheckCode() {
		return (String) BaseDwrUtil
				.getSessionAttribute(ConstantUtil.CHECK_CODE_STR);
	}

	// 刷新当前验证码
	public static void refreshCurrentCheckCode() {
		RandomValidateCode randomValidateCode = new RandomValidateCode();
		try {
			randomValidateCode.refreshRandcode(BaseDwrUtil.getRequest(),
					BaseDwrUtil.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 获取用户的客户端请求IP   
	public static String getRequestIp() {
		HttpServletRequest request = BaseDwrUtil.getRequest();
		String ip = request.getHeader("x-forwarded-for");
		//多级反向代理会有多个ip值，此时取第一个ip值
		if(ip != null && ip.length() > 0) {
			String[] ips = ip.split(",");
			//默认取第一个ip
			ip = ips[0].trim();
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}
	
	// 获取用户的客户端请求IP
	public static String getRequestIp(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		//多级反向代理会有多个ip值，此时取第一个ip值
		if(ip != null && ip.length() > 0) {
			String[] ips = ip.split(",");
			//默认取第一个ip
			ip = ips[0].trim();
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}

	// 获取用户的客户端请求IP
	public static String getByRequestIp(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		//多级反向代理会有多个ip值，此时取第一个ip值
		if(ip != null && ip.length() > 0) {
			String[] ips = ip.split(",");
			//默认取第一个ip
			ip = ips[0].trim();
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}

	/**
	 * 获取mac地址
	 * 
	 * @param ip
	 * @return
	 */
	public static String getMACAddress(InetAddress ia) throws Exception {
		// 获得网络接口对象（即网卡），并得到mac地址，mac地址存在于一个byte数组中。
		byte[] mac = NetworkInterface.getByInetAddress(ia).getHardwareAddress();
		// 下面代码是把mac地址拼装成String
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < mac.length; i++) {
			if (i != 0) {
				sb.append("-");
			}
			// mac[i] & 0xFF 是为了把byte转化为正整数
			String s = Integer.toHexString(mac[i] & 0xFF);
			sb.append(s.length() == 1 ? 0 + s : s);
		}
		// 把字符串所有小写字母改为大写成为正规的mac地址并返回
		String result = sb.toString().toUpperCase();
		if(StringUtils.isEmpty(result)){
			result = UUID.randomUUID().toString();
		}
		return result;
	}
}
