package com.team.lottery.util;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.extvo.LotteryCodeQuery;
import com.team.lottery.mapper.lotterycode.LotteryCodeMapper;
import com.team.lottery.mapper.lotterycodetrend.LotteryCodeTrendMapper;
import com.team.lottery.service.LotteryCodeService;
import com.team.lottery.service.LotteryCodeTrendService;
import com.team.lottery.service.codetrend.LHCCodeTrendServiceImpl;
import com.team.lottery.service.lotterykindplay.CodeTrendService;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryCodeTrend;

public class DealCodeTrendLhcUtil {

	public static Logger log = LoggerFactory.getLogger(DealCodeTrendLhcUtil.class);
	
	public static LotteryCodeMapper lotteryCodeMapper;
	
	public static LotteryCodeTrendMapper lotteryCodeTrendMapper;
	
	static{
		String[] locations = {"spring/applicationContext*.xml"};
		ApplicationContext context = new ClassPathXmlApplicationContext(locations);
		
		lotteryCodeMapper = context.getBean(LotteryCodeMapper.class);
		lotteryCodeTrendMapper = context.getBean(LotteryCodeTrendMapper.class);
	}
	
	public static void main(String[] args) {
		LotteryCodeQuery query = new LotteryCodeQuery();
		query.setLotteryName(ELotteryKind.LHC.getCode());
		List<LotteryCode> list = lotteryCodeMapper.getAllLotteryCodesByQuery(query);
		for (LotteryCode lotteryCode : list) {
			String kind=lotteryCode.getLotteryName();
			String openCode = lotteryCode.getOpencodeStr();
			String expect = lotteryCode.getLotteryNum();
			//过滤已经保存的
			LotteryCodeTrend lotteryCodeTrend2 = lotteryCodeTrendMapper.selectByKindAndExpect(kind, expect);
			if (lotteryCodeTrend2 !=null) {
				continue;
			}
			/**
			 * 开奖后记录走势图数据
			 */
			try {
				log.info("开奖后记录走势图数据开始,期号[{}],彩种[{}],开奖号码[{}]",expect,kind,openCode);
				LotteryCodeTrendService lotteryCodeTrendService = (LotteryCodeTrendService)ApplicationContextUtil.getBean("lotteryCodeTrendService");
				LHCCodeTrendServiceImpl lHCCodeTrendServiceImpl = (LHCCodeTrendServiceImpl)ApplicationContextUtil.getBean("lHCCodeTrendServiceImpl");
				LotteryCodeTrend lotteryCodeTrend = new LotteryCodeTrend();
				lotteryCodeTrend.setOpenCode(openCode);
				lotteryCodeTrend.setLotteryName(kind);
				lotteryCodeTrend.setLotteryNum(expect);
				lotteryCodeTrend.setAddtime(new Date());
				lotteryCodeTrend.setKjtime(lotteryCode.getKjtime());
				String serviceClassName = EumnUtil.getServiceClassNameCodeTrend(kind);
				SystemConfigConstant.springFestivalStartDate = "2019-02-04 00:00:00";
				if (StringUtils.isNotBlank(serviceClassName)) {
					String lastNumPosition = "";
					lotteryCodeTrend.setNumPosition(lastNumPosition);
					lotteryCodeTrend.setPlaykindContent(lHCCodeTrendServiceImpl.getPlaykindContent(openCode));
				}
				lotteryCodeTrendService.insertSelective(lotteryCodeTrend);
				log.info("开奖后记录走势图数据结束");
			} catch (Exception e) {
				log.info("开奖后记录走势图数据异常:"+e);
			}
		}
	}
}
