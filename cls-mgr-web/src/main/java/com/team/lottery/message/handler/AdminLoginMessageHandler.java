package com.team.lottery.message.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.message.messagetype.AdminLoginMessage;
import com.team.lottery.service.AdminLogService;
import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.BaseMessageHandler;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.util.IPUtil;
import com.team.lottery.vo.AdminLog;

public class AdminLoginMessageHandler extends BaseMessageHandler {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5269966949190255216L;
	private static Logger logger = LoggerFactory.getLogger(AdminLoginMessageHandler.class);

	@SuppressWarnings("unused")
	@Override
	public void dealMessage(BaseMessage message) {
		try {
			AdminLoginMessage admininMessage = (AdminLoginMessage)message;
			AdminLogService adminLogService = ApplicationContextUtil.getBean("adminLogService");
			Integer loginId = admininMessage.getLoginId();
			String ip = admininMessage.getIp();
			String adminName = admininMessage.getAdminName();
			String bizSystem = admininMessage.getBizSystem();
			String loginType = admininMessage.getLoginType();
			String sessionId = admininMessage.getSessionId();
			//当前线程休眠0.5秒，保证获取到login记录
			try {
				Thread.sleep(500);
			} catch (Exception e) {
				logger.error("登陆消息线程休眠发生错误...", e);
			}
			AdminLog adminLog = adminLogService.selectByPrimaryKey(loginId);
			
			String address = IPUtil.getAddressesPcOnline(ip);
			logger.info("业务系统["+ bizSystem +"],管理员["+ adminName +"]进行登录，ip["+ ip +"],登录地点["+ address +"]");
			if(adminLog == null){
				logger.info("未找到该用户登录记录");
				return;
			}
			adminLog.setAddress(address);
			adminLogService.updateByPrimaryKeySelective(adminLog);
		    
//			if(loginType != null && !loginType.equals(Login.LOGTYPE_ADMIN)) {
//		    	//通知其他前端系统删除登录
//				SingleLoginForRedis.deleteAlreadyEnter(sessionId, SystemPropeties.loginType + "." + loginLog.getBizSystem(), loginLog.getUserName());
//				
//				//单点登录逻辑处理
//				SingleLoginForRedis.isAlreadyEnter(loginLog.getUserName(),loginLog.getBizSystem(), loginType, sessionId);
//			}
		} catch (Exception e) {
			logger.error("处理登陆消息发生错误...", e);
		}
	}

}
