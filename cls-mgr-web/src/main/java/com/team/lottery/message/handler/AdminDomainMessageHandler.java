package com.team.lottery.message.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.cache.ClsMgrCacheManager;
import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.BaseMessageHandler;
/**
 * 处理后台域名管理消息的Handler处理器.
 * @author jamine
 *
 */
public class AdminDomainMessageHandler extends BaseMessageHandler{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3956836343110925678L;
	private static Logger logger = LoggerFactory.getLogger(AdminDomainMessageHandler.class);
	
	/**
	 * 处理消息
	 */
	@Override
	public void dealMessage(BaseMessage message) {
		try {
			logger.info("接受redis订阅消息，开始更新AdminDomain的ehcache缓存...");
			ClsMgrCacheManager.initMgrBizSystemDomainCache();
		} catch (Exception e) {
			logger.error("处理AdminDomain的ehcache缓存发生错误...", e);
		}
	}

}
