package com.team.lottery.message.messagetype;

import com.team.lottery.message.handler.AdminLoginMessageHandler;
import com.team.lottery.system.message.BaseMessage;


/**
 * 用户登录消息对象
 * @author luocheng
 *
 */
public class AdminLoginMessage extends BaseMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8782533829248615014L;
	private String adminName;
	private String bizSystem;
	private String ip;
	//登录记录的ID
	private Integer loginId;
	private String loginType;
	private String sessionId;
	
	
	
	public AdminLoginMessage() {
		this.handler = new AdminLoginMessageHandler();
	}
	
	public String getAdminName() {
		return adminName;
	}
	
	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}


	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}

	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public Integer getLoginId() {
		return loginId;
	}
	public void setLoginId(Integer loginId) {
		this.loginId = loginId;
	}
	public String getLoginType() {
		return loginType;
	}
	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
}
