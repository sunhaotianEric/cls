package com.team.lottery.message.messagetype;

import com.team.lottery.message.handler.AdminDomainMessageHandler;
import com.team.lottery.system.message.BaseMessage;
/**
 * 后台域名授权管理消息中心.
 * @author jamine
 *
 */
public class AdminDomainMessage extends BaseMessage{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4449421431199892889L;

	public AdminDomainMessage() {
		this.handler=new AdminDomainMessageHandler();
	}
}
