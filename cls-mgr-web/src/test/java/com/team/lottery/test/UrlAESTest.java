package com.team.lottery.test;

import org.junit.Test;

import com.team.lottery.util.AES;

import java.util.UUID;

public class UrlAESTest {
	
	private static String password = "xiangniu999";
	//private static String password = "19951128";

	@Test
	public void encryptContent() {
		String content = "jdbc:mysql://127.0.0.1:3306/cls?useUnicode=true&characterEncoding=utf-8&autoReconnect=true&allowMultiQueries=true";
		String encryptResultStr2 = AES.encrypt(content, password);
		System.out.println("加密后2：" + encryptResultStr2);
	}
	
	@Test
	public void decryptContent() {
		String content = "DD4ADA90771ED7E8B2CC8D7476110487";
		String decryptResult2 = AES.decrypt(content, "80873ffb-6694-40d7-854d-05c6da354916");
		System.out.println("解密后2：" + new String(decryptResult2));
	}

	public static void main(String[] args) {
		UUID uuid = UUID.randomUUID();
		System.out.println(uuid);
	}
}
