package com.team.lottery.mapper.orderrecord;

import com.team.lottery.vo.OrderAfterRecord;

public interface OrderAfterRecordMapper {
    int deleteByPrimaryKey(Long id);

    int insert(OrderAfterRecord record);

    int insertSelective(OrderAfterRecord record);

    OrderAfterRecord selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(OrderAfterRecord record);

    int updateByPrimaryKey(OrderAfterRecord record);
    
    //查询追号订单的状态记录
    public OrderAfterRecord getOrderAfterRecord(String lotteryId);
}