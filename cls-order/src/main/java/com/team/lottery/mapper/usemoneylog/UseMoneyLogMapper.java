package com.team.lottery.mapper.usemoneylog;

import java.util.List;

import com.team.lottery.vo.UseMoneyLog;

public interface UseMoneyLogMapper {
    int deleteByPrimaryKey(Long id);

    int insert(UseMoneyLog record);

    int insertSelective(UseMoneyLog record);

    UseMoneyLog selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UseMoneyLog record);

    int updateByPrimaryKey(UseMoneyLog record);
    
    /**
     * 根据订单号,查询对应的扣除的可提现金额
     * @param lotteryId
     * @return
     */
    public UseMoneyLog getUseMoneylogBylotteryId(String lotteryId);
    
    /**
     * 获取追号的所有可回收的金额
     * @param lotteryId
     * @return
     */
    public List<UseMoneyLog> getUseMoneylogsBylotteryIdLike(String lotteryId);
    
    /**
     * 根据订单号,查询对应的扣除的可提现金额
     * @param lotteryId
     * @return
     */
    public UseMoneyLog getUseMoneylogBylotteryIdForForce(String lotteryId);
    
    /**
     * 获取追号的所有可回收的金额
     * @param lotteryId
     * @return
     */
    public List<UseMoneyLog> getUseMoneylogsBylotteryIdLikeForForce(String lotteryId);
}