package com.team.lottery.mapper.order;

import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.DayOnlineTypeCountVo;
import com.team.lottery.extvo.OrderQuery;
import com.team.lottery.vo.Order;
import com.team.lottery.vo.User;

public interface OrderMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Order record);

    int insertSelective(Order record);

    Order selectByPrimaryKey(Long id);
    
    Order selectByExpect(String expect);

    int updateByPrimaryKeySelective(Order record);

    int updateByPrimaryKey(Order record);
    
    
    /**
     * 获取用户的有效投注列表条数
     * @param query
     * @return
     */
    public Integer getEffectiveLotterysByQueryPageCount(@Param("query")OrderQuery query);
    
    /**
     * 统计用户的有效
     * @param query
     * @return
     */
    public Integer getEffectiveLotterysTotalByQuery(@Param("query")OrderQuery query);
    
    /**
     * 获取用户的有效投注列表
     * @param query
     * @return
     */
    public List<Order> getEffectiveLotterysByQueryPage(@Param("query")OrderQuery query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
    
    /**
     * 查询用户的今日投注记录(不包含回退和撤单)
     * @param query
     * @return
     */
    public List<Order> getTodayLotteryByUser(@Param("query")OrderQuery query);
    
    /**
     * 查询用户的今日追号投注记录
     * @param query
     * @return
     */
    public List<Order> getTodayLotteryAfterByUser(@Param("query")OrderQuery query);
    
    /**
     * 查询符合条件的某些订单
     * @param order
     * @return
     */
    public List<Order> getOrderByCondition(@Param("query") OrderQuery query);
    
    /**
     * 查询符合条件的记录条数
     * @param query
     * @return
     */
    public Integer getAllOrdersByQueryPageCount(@Param("query")OrderQuery query);
    
    /**
     * 分页条件查询下注记录
     * @return
     */
	List<Order> getAllOrdersByQueryPage(@Param("query")OrderQuery query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);

	/**
	 * 根据订单id查找队赢得投注记录
	 * @param id
	 * @return
	 */
	public Order getOrdertById(Long orderId);
	
	/**
	 * 根据订单编号查找对应的投注记录
	 * @param lotteryId
	 * @return
	 */
	public List<Order> getOrdersByLotteryId(String lotteryId);

	public Integer getAllOrderAfterNumsByQueryPageCount(@Param("query")OrderQuery query);

	/**
     * 分页条件查询追号记录
     * @return
     */
	List<Order> getAllOrderAfterNumsByQueryPage(@Param("query")OrderQuery query, @Param("startNum")Integer startIndex,
			@Param("pageNum")Integer pageSize);

    
	/**
     * 查询用户投注历史记录条数
     * @param query
     * @return
     */
    public Integer getUserOrdersByQueryPageCount(@Param("query")OrderQuery query);
    
    /**
     * 分页查询用户追号记录总条数
     * @return
     */
	List<Order> getUserOrdersByQueryPage(@Param("query")OrderQuery query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
	
	public Integer getUserOrderAfterNumsByQueryPageCount(@Param("query")OrderQuery query);

	/**
     * 分页条件查询追号记录
     * @return
     */
	List<Order> getUserOrderAfterNumsByQueryPage(@Param("query")OrderQuery query, @Param("startNum")Integer startIndex,
			@Param("pageNum")Integer pageSize);
	
	int updateOrderRegfrom(User user);
	
	/**
	 * 查询每日投注人数(按登录类型统计)
	 * @param query
	 * @return
	 */
	List<DayOnlineTypeCountVo> getDayLotteryTypeCount(@Param("query")OrderQuery query);
	
	/**
	 * 查询每日投注人数
	 * @param query
	 * @return
	 */
	Integer getDayLotteryCount(@Param("query")OrderQuery query);
	
	/**
	 * 查询每日彩种盈利情况 
	 * @param query
	 * @return
	 */
	List<Order> getLotteryDayGain(@Param("query")OrderQuery query);
	
	/**
	 * 返回六合彩，每期的投注总额
	 * @param query
	 * @return
	 */
	Order getLhcLotteryMoney(@Param("query")OrderQuery query);
	
	
	 /**
     * 统计用户的有效
     * @param query
     * @return
     */
    public Integer getTeamUserLotteryByQueryCount(@Param("query")OrderQuery query);

	/**
	 * 根据条件统计某些彩种的总投注额
	 * 
	 * @param query
	 * @return
	 */
    public BigDecimal getDayConsumeOrderByLottery(@Param("query")OrderQuery query);
    
	/**
	 * 根据条件查询充值订单总额.
	 * @param query
	 * @return
	 */
	public Order getOrdersTotalByQuery(@Param("query") OrderQuery query);
}