package com.team.lottery.mapper.orderamount;

import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.OrderDayNumVo;
import com.team.lottery.extvo.OrderQuery;
import com.team.lottery.vo.OrderAmount;

public interface OrderAmountMapper {
    int deleteByPrimaryKey(Long id);

    int insert(OrderAmount record);

    int insertSelective(OrderAmount record);

    OrderAmount selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(OrderAmount record);

    int updateByPrimaryKey(OrderAmount record);
    

	/**
	 * 查询代理用户日量记录总条数
	 * @param query
	 * @return
	 */
	public Integer getUserDayOrderNumByQueryPageCount(@Param("query")OrderQuery query);

	/**
     * 分页条件查询代理用户的日量
     * @return
     */
	List<OrderDayNumVo> getUserDayOrderNumByQueryPage(@Param("query")OrderQuery query, @Param("startNum")Integer startIndex,
			@Param("pageNum")Integer pageSize);
	
	/**
	 * 条件查询代理用户的总日量 
	 * @param query
	 * @return
	 */
	public OrderDayNumVo getTotalUserDayOrderNum(@Param("query")OrderQuery query);
	
    
    /**
     * 查询某个时间段的所有有效投注记录
     * @return
     */
	List<OrderAmount> getUserTeamEffectiveLotterys(@Param("query")OrderQuery query);
	
	/**
     * 查询某个时间段的所有有效投注记录总和
     * @return
     */
	BigDecimal getUserTeamTotalEffectiveLotterys(@Param("query")OrderQuery query);
    
    /**
	 * 查询所有用户的有效投注量 即用户的消费，用于佣金计算
	 * @param query
	 * @return
	 */
	List<OrderAmount> getUserEffectiveLotterys(@Param("query")OrderQuery query);
	
	 /**
     * 获取用户的有效投注列表条数
     * @param query
     * @return
     */
    public Integer getEffectiveLotterysByQueryPageCount(@Param("query")OrderQuery query);
    
	/**
     * 统计用户的有效投注列表
     * @param query
     * @return
     */
    public Integer getEffectiveLotterysTotalByQuery(@Param("query")OrderQuery query);
    
    /**
     * 获取用户的有效投注列表
     * @param query
     * @return
     */
    public List<OrderAmount> getEffectiveLotterysByQueryPage(@Param("query")OrderQuery query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
    
    /**
     * 根据期号和类型删除记录
     * @param expect
     * @param lotteryType
     * @return
     */
    public int deleteByExceptAndType(@Param("expect")String expect, @Param("lotteryType")String lotteryType); 
}