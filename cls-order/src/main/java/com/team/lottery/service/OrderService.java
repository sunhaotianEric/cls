package com.team.lottery.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.enums.EDPCKind;
import com.team.lottery.enums.EKSKind;
import com.team.lottery.enums.ELHCKind;
import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.enums.EMoneyDetailType;
import com.team.lottery.enums.EOrderStatus;
import com.team.lottery.enums.EPK10Kind;
import com.team.lottery.enums.EProstateStatus;
import com.team.lottery.enums.ESSCKind;
import com.team.lottery.enums.ESYXWKind;
import com.team.lottery.enums.EXYEBKind;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.DayOnlineTypeCountVo;
import com.team.lottery.extvo.OrderQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.UserTotalMoneyByTypeVo;
import com.team.lottery.mapper.order.OrderMapper;
import com.team.lottery.util.AwardModelUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.MakeOrderNum;
import com.team.lottery.util.OrderDetailUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryWinLhc;
import com.team.lottery.vo.LotteryWinXyeb;
import com.team.lottery.vo.MoneyDetail;
import com.team.lottery.vo.Order;
import com.team.lottery.vo.OrderAfterRecord;
import com.team.lottery.vo.UseMoneyLog;
import com.team.lottery.vo.User;

@Service("orderService")
public class OrderService {

	private static Logger logger = LoggerFactory.getLogger(OrderService.class);

	@Autowired
	private OrderMapper orderMapper;
	@Autowired
	private UserService userService;
	@Autowired
	private MoneyDetailService moneyDetailService;
	@Autowired
	private UseMoneyLogService useMoneyLogService;
	@Autowired
	private OrderAfterRecordService orderAfterRecordService;
	@Autowired
	private UserMoneyTotalService userMoneyTotalService;
	@Autowired
	private LotteryWinLhcService lotteryWinLhcService;
	@Autowired
	private LotteryWinXyebService lotteryWinXy28Service;

	private static Map<String, String> lhcPlayDescMap = new HashMap<String, String>();
	private static Map<String, String> xy28PlayDescMap = new HashMap<String, String>();

	public int deleteByPrimaryKey(Long id) {
		return orderMapper.deleteByPrimaryKey(id);
	}

	public int insert(Order record) {
		return orderMapper.insert(record);
	}

	public int insertSelective(Order record) {
		record.setCreatedDate(new Date());
		record.setUpdateDate(new Date());
		setMoneyScale(record);
		return orderMapper.insertSelective(record);
	}

	public Order selectByPrimaryKey(Long id) {
		return orderMapper.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(Order record) {
		setMoneyScale(record);
		record.setUpdateDate(new Date());
		return orderMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(Order record) {
		setMoneyScale(record);
		return orderMapper.updateByPrimaryKey(record);
	}

	/**
	 * 控制所有金额的精度
	 * 
	 * @param record
	 */
	public void setMoneyScale(Order record) {
		if (record.getPayMoney() != null) {
			record.setPayMoney(record.getPayMoney().setScale(ConstantUtil.BIGDECIMAL_SCAL, BigDecimal.ROUND_HALF_UP));
		}
		if (record.getPayPoint() != null) {
			record.setPayPoint(record.getPayPoint().setScale(ConstantUtil.BIGDECIMAL_SCAL, BigDecimal.ROUND_HALF_UP));
		}
		if (record.getWinCost() != null) {
			record.setWinCost(record.getWinCost().setScale(ConstantUtil.BIGDECIMAL_SCAL, BigDecimal.ROUND_HALF_UP));
		}
	}

	/**
	 * 获取用户的有效投注列表
	 * 
	 * @param query
	 * @return
	 */
	public Page getEffectiveLotterysByQueryPage(OrderQuery query, Page page) {
		page.setTotalRecNum(orderMapper.getEffectiveLotterysByQueryPageCount(query));
		page.setPageContent(orderMapper.getEffectiveLotterysByQueryPage(query, page.getStartIndex(), page.getPageSize()));
		return page;
	}

	/**
	 * 获取用户的有效投注列表条数
	 * 
	 * @param query
	 * @return
	 */
	public Integer getEffectiveLotterysByQueryPageCount(OrderQuery query) {
		return orderMapper.getEffectiveLotterysByQueryPageCount(query);
	}

	/**
	 * 查询用户的今日投注记录(不包含回退和撤单)
	 * 
	 * @param query
	 * @return
	 */
	public List<Order> getTodayLotteryByUser(OrderQuery query) {
		return orderMapper.getTodayLotteryByUser(query);
	}

	/**
	 * 查询用户的今日追号投注记录
	 * 
	 * @param query
	 * @return
	 */
	public List<Order> getTodayLotteryAfterByUser(OrderQuery query) {
		return orderMapper.getTodayLotteryAfterByUser(query);
	}

	/**
	 * 统计用户的有效
	 * 
	 * @param query
	 * @return
	 */
	public Integer getEffectiveLotterysTotalByQuery(OrderQuery query) {
		return orderMapper.getEffectiveLotterysTotalByQuery(query);
	}

	/**
	 * 查询符合条件的某些订单
	 * 
	 * @param order
	 * @return
	 */
	public List<Order> getOrderByCondition(OrderQuery query) {

		return orderMapper.getOrderByCondition(query);
	}

	/**
	 * 是否使用积分支付的逻辑
	 * 
	 * @param order
	 * @param lotteryPlayKindCodeMoney
	 * @param user
	 */
	public void payMoneyOrPointLogic(Order order, BigDecimal lotteryPlayKindCodeMoney, User user) {
		BigDecimal pointToMoney = user.getPoint().divide(new BigDecimal("1")); // 计算积分价值多少钱
		if (pointToMoney.compareTo(lotteryPlayKindCodeMoney) >= 0) { // 如果积分价值大于需要支付的金额,则使用积分进行支付
			order.setPayMoney(new BigDecimal(0));
			order.setPayPoint(lotteryPlayKindCodeMoney.multiply(new BigDecimal("1"))); // 需要花费的积分
		} else {
			order.setPayMoney(lotteryPlayKindCodeMoney.subtract(pointToMoney)); // 用完积分需要支付的金额
			order.setPayPoint(user.getPoint()); // 需要花费的积分
		}
	}

	/**
	 * 查找所有的下注记录数据
	 * 
	 * @return
	 */
	public Page getOrdersByQueryPage(OrderQuery query, Page page) {
		page.setTotalRecNum(orderMapper.getAllOrdersByQueryPageCount(query));
		page.setPageContent(orderMapper.getAllOrdersByQueryPage(query, page.getStartIndex(), page.getPageSize()));
		return page;
	}

	/**
	 * 查找指定page的订单
	 * 
	 * @param query
	 * @param page
	 * @return
	 */
	public Page getAllOrdersByQueryPage(OrderQuery query, Page page) {
		page.setPageContent(orderMapper.getAllOrdersByQueryPage(query, page.getStartIndex(), page.getPageSize()));
		return page;
	}

	/**
	 * 获取普通投注记录的信息
	 * 
	 * @return
	 * @throws Exception
	 */
	public Order getNotAfterNumberOrder(String lotteryId) throws Exception {
		List<Order> orders = orderMapper.getOrdersByLotteryId(lotteryId);
		if (orders == null) {
			throw new Exception("系统未找到该投注记录的信息");
		} else if (orders.size() != 1) {
			throw new Exception("该投注信息记录有重复订单.");
		}
		Order order = orders.get(0);
		if (order != null) {
			order.setCodesList(this.getCodesDesByKindReplace(order.getCodes(), order.getLotteryType(), order.getBizSystem()));
		}
		return order;
	}

	/**
	 * 获取追号记录的详细信息
	 * 
	 * @param lotteryId
	 * @return
	 * @throws Exception
	 */
	public List<Order> getAfterNumberOrders(String lotteryId) throws Exception {
		List<Order> orders = orderMapper.getOrdersByLotteryId(lotteryId);

		if (CollectionUtils.isEmpty(orders)) {
			throw new Exception("系统未找到该投注记录的信息");
		}

		for (int i = 0; i < orders.size(); i++) {
			Order order = orders.get(0);
			order.setCodesList(this.getCodesDesByKindReplace(order.getCodes(), order.getLotteryType(), order.getBizSystem()));
			if (new Date().getTime() >= order.getCloseDate().getTime()) { // 如果已经结束的期号,则将订单改为结束
				order.setStatus(EOrderStatus.END.name());
			}
		}
		return orders;
	}

	/**
	 * 撤单处理
	 * 
	 * @param orderId
	 * @throws Exception
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void stopOrderById(String orderId, User currentUser) throws UnEqualVersionException, Exception {
		Order order = this.selectByPrimaryKey(Long.parseLong(orderId));
		if (order == null) {
			throw new Exception("该订单数据不存在.");
		}
		logger.info("业务系统[{}],用户[{}]进行未开奖之前撤单, 撤单订单号[{}], orderId[{}]", currentUser.getBizSystem(), currentUser.getUserName(),
				order.getLotteryId(), orderId);
		if (order.getUserId().intValue() != currentUser.getId().intValue()) {
			throw new Exception("您没有权限修改该订单.");
		}

		if (new Date().getTime() > order.getCloseDate().getTime()) {
			throw new Exception("该投注订单期号的投注时间已经结束,您不能再撤单了.");
		}

		if (order.getStatus().equals(EOrderStatus.GOING.name())) {
			// mysql更新订单
			order.setProstate(EProstateStatus.REGRESSION.name()); // 撤单（投注回退）
			order.setStatus(EOrderStatus.END.name());
			this.updateByPrimaryKeySelective(order);

			// User orderUser =
			// userService.selectByPrimaryKeyForUpdate(order.getUserId());
			User orderUser = userService.selectByPrimaryKey(order.getUserId());
			if (order.getPayMoney().compareTo(BigDecimal.ZERO) > 0) {
				// 游客投注的订单和资金明细要设置为无效！
				Integer enabled = orderUser.getIsTourist() == 1 ? 0 : 1;

				MoneyDetail orderUserMoneyDetail = new MoneyDetail();
				orderUserMoneyDetail.setEnabled(enabled);
				orderUserMoneyDetail.setUserId(orderUser.getId()); // 用户id
				orderUserMoneyDetail.setOrderId(order.getId()); // 订单id
				orderUserMoneyDetail.setUserName(orderUser.getUserName()); // 用户名
				orderUserMoneyDetail.setBizSystem(orderUser.getBizSystem());
				orderUserMoneyDetail.setOperateUserName(ConstantUtil.SYSTEM_OPERATE_NAME); // 操作人员
				// 撤单单号
				String lotteryid = MakeOrderNum.makeLROrderNum(MakeOrderNum.LR, order.getLotteryId(), order.getExpect());
				orderUserMoneyDetail.setLotteryId(lotteryid); // 订单单号
				orderUserMoneyDetail.setLotteryType(order.getLotteryType());
				orderUserMoneyDetail.setLotteryTypeDes(order.getLotteryTypeDes());
				orderUserMoneyDetail.setExpect(order.getExpect());
				orderUserMoneyDetail.setWinLevel(null);
				orderUserMoneyDetail.setWinCost(null);
				orderUserMoneyDetail.setWinCostRule(null);
				orderUserMoneyDetail.setDetailType(EMoneyDetailType.LOTTERY_REGRESSION.name()); // 投注退单
				orderUserMoneyDetail.setDetailDes(order.getDetailDes());
				orderUserMoneyDetail.setLotteryModel(order.getLotteryModel());
				orderUserMoneyDetail.setIncome(order.getPayMoney());
				orderUserMoneyDetail.setPay(new BigDecimal(0));
				orderUserMoneyDetail.setDescription("用户投注撤单:" + order.getPayMoney());
				orderUserMoneyDetail.setBalanceBefore(orderUser.getMoney()); // 支出前金额
				orderUserMoneyDetail.setBalanceAfter(orderUser.getMoney().add(order.getPayMoney())); // 支出后的金额
				moneyDetailService.insertSelective(orderUserMoneyDetail); // 保存资金明细

				// 用户撤单增加用户余额
				orderUser.addMoney(order.getPayMoney(), false);
				// 是否有钱进入可提现金额
				UseMoneyLog useMoneyLog = useMoneyLogService.getUseMoneylogBylotteryId(order.getLotteryId() + "_" + order.getId());
				if (useMoneyLog != null) {
					orderUser.addCanWithdrawMoney(useMoneyLog.getUseMoney());
				}
				userService.updateByPrimaryKeyWithVersionSelective(orderUser);
				userMoneyTotalService.addUserTotalMoneyByType(orderUser, EMoneyDetailType.LOTTERY_REGRESSION, order.getPayMoney());
			}

		} else {
			throw new Exception("该投注订单已经开奖了，不能撤销订单的哦.");
		}

	}

	/**
	 * 停止追号事件
	 * 
	 * @param orderId
	 * @throws Exception
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void stopAfterNumber(String lotteryId, User currentUser) throws UnEqualVersionException, Exception {
		List<Order> orders = this.getAfterNumberOrders(lotteryId);
		if (orders == null || orders.size() == 0) {
			throw new Exception("该订单的追号记录不存在.");
		}
		logger.info("业务系统[{}],用户[{}]停止追号,lotteryId[{}]", currentUser.getBizSystem(), currentUser.getUserName(), lotteryId);

		// 订单是否进入可提现金额
		List<UseMoneyLog> useMoneyLogs = useMoneyLogService.getUseMoneylogsBylotteryIdLike(orders.get(0).getLotteryId() + "_");
		UseMoneyLog useMoneyLog = null;
		BigDecimal afterNumberMoney = new BigDecimal(0);
		BigDecimal useMoneyTotal = new BigDecimal(0);
		String orderAfterRecordLotteryId = null;

		for (Order order : orders) {
			if (order.getUserId().intValue() != currentUser.getId().intValue()) {
				throw new Exception("您没有权限修改该订单.");
			}

			if (order.getCloseDate().getTime() > new Date().getTime() && order.getStatus().equals(EOrderStatus.GOING.name())) {
				order.setProstate(EProstateStatus.REGRESSION.name()); // 停止处理的订单
				order.setStatus(EOrderStatus.END.name()); // 停止追号
				this.updateByPrimaryKeySelective(order);
				afterNumberMoney = afterNumberMoney.add(order.getPayMoney());
				int useMoneyLogIndex = useMoneyLogs.indexOf(new UseMoneyLog(order.getLotteryId() + "_" + order.getId()));
				if (useMoneyLog != null && useMoneyLogIndex != -1) {
					useMoneyLog = useMoneyLogs.get(useMoneyLogIndex);
					useMoneyTotal = useMoneyTotal.add(useMoneyLog.getUseMoney());
				}
			}

			if (orderAfterRecordLotteryId == null) {
				orderAfterRecordLotteryId = order.getLotteryId();
			}
		}

		// 如果订单追号,则更新追号订单的已投和已追状态
		if (!StringUtils.isEmpty(orderAfterRecordLotteryId)) {
			OrderAfterRecord orderAfterRecord = orderAfterRecordService.getOrderAfterRecord(orderAfterRecordLotteryId);
			orderAfterRecord.setAfterStatus(3);
			orderAfterRecordService.updateByPrimaryKeySelective(orderAfterRecord);
		}
		List<UserTotalMoneyByTypeVo> userTotalMoneyByTypeVos = new ArrayList<UserTotalMoneyByTypeVo>();
		if (orders.size() > 0) {
			Order firstOrder = orders.get(0);
			// User orderUser =
			// userService.selectByPrimaryKeyForUpdate(firstOrder.getUserId());
			User orderUser = userService.selectByPrimaryKey(firstOrder.getUserId());
			if (afterNumberMoney.compareTo(new BigDecimal(0)) > 0) {
				// 游客投注的订单和资金明细要设置为无效！
				Integer enabled = orderUser.getIsTourist() == 1 ? 0 : 1;

				MoneyDetail orderUserMoneyDetail = new MoneyDetail();
				orderUserMoneyDetail.setEnabled(enabled);
				orderUserMoneyDetail.setUserId(orderUser.getId()); // 用户id
				orderUserMoneyDetail.setOrderId(firstOrder.getId()); // 订单id
				orderUserMoneyDetail.setUserName(orderUser.getUserName()); // 用户名
				orderUserMoneyDetail.setBizSystem(orderUser.getBizSystem());
				orderUserMoneyDetail.setOperateUserName(orderUser.getUserName()); // 操作人员
				// 停止追号，只是一个单的化就用原投注订单号
				String lotteryid = MakeOrderNum.makeLROrderNum(MakeOrderNum.LS, firstOrder.getLotteryId(), "000000");
				orderUserMoneyDetail.setLotteryId(lotteryid); // 订单单号
				orderUserMoneyDetail.setLotteryType(firstOrder.getLotteryType());
				orderUserMoneyDetail.setLotteryTypeDes(firstOrder.getLotteryTypeDes());
				orderUserMoneyDetail.setExpect(firstOrder.getExpect());
				orderUserMoneyDetail.setWinLevel(null);
				orderUserMoneyDetail.setWinCost(null);
				orderUserMoneyDetail.setWinCostRule(null);
				orderUserMoneyDetail.setDetailType(EMoneyDetailType.LOTTERY_STOP_AFTER_NUMBER.name()); // 资金类型为投注
				orderUserMoneyDetail.setDetailDes(firstOrder.getDetailDes());
				orderUserMoneyDetail.setLotteryModel(firstOrder.getLotteryModel());
				orderUserMoneyDetail.setIncome(afterNumberMoney);
				orderUserMoneyDetail.setPay(new BigDecimal(0));
				orderUserMoneyDetail.setDescription("用户追号撤单:" + afterNumberMoney);
				orderUserMoneyDetail.setBalanceBefore(orderUser.getMoney()); // 支出前金额
				orderUserMoneyDetail.setBalanceAfter(orderUser.getMoney().add(afterNumberMoney)); // 支出后的金额
				moneyDetailService.insertSelective(orderUserMoneyDetail); // 保存资金明细
				// 针对盈亏报表多线程不会事物回滚处理方式
				UserTotalMoneyByTypeVo userTotalMoneyByTypeVo = new UserTotalMoneyByTypeVo(orderUser,
						EMoneyDetailType.LOTTERY_STOP_AFTER_NUMBER, afterNumberMoney);
				userTotalMoneyByTypeVos.add(userTotalMoneyByTypeVo);
				// userMoneyTotalService.addUserTotalMoneyByType(orderUser,
				// EMoneyDetailType.LOTTERY_STOP_AFTER_NUMBER,
				// afterNumberMoney);
			}
			// 停止追号增加用户余额
			orderUser.addMoney(afterNumberMoney, false);
			orderUser.addCanWithdrawMoney(useMoneyTotal);
			userService.updateByPrimaryKeyWithVersionSelective(orderUser);
			// 循环遍历插入报表总额和盈亏报表
			for (UserTotalMoneyByTypeVo userTotalMoneyByTypeVo : userTotalMoneyByTypeVos) {
				userMoneyTotalService.addUserTotalMoneyByType(userTotalMoneyByTypeVo.getUser(), userTotalMoneyByTypeVo.getDetailType(),
						userTotalMoneyByTypeVo.getOperationValue());
			}
		}
	}

	/**
	 * 订单号码详情获取
	 * 
	 * @param orderId
	 * @return
	 */
	public List<Map<String, String>> getCodesDes(String codes, String lotteryType) {
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		Map<String, String> maps = null;

		if (codes != null) {
			if (!StringUtils.isEmpty(codes)) {
				String[] lotteryCodes = codes.split(ConstantUtil.SOURCECODE_PROTYPE_SPLIT);
				for (String lotteryCode : lotteryCodes) {
					maps = new HashMap<String, String>();

					int startPosition = lotteryCode.indexOf("[");
					int endPosition = lotteryCode.indexOf("]");
					int beishuStartPosition = lotteryCode.lastIndexOf("[");
					int beishuEndPosition = lotteryCode.lastIndexOf("]");
					int payMoneyStartPosition = lotteryCode.lastIndexOf("{");
					int payMoneyEndPosition = lotteryCode.lastIndexOf("}");
					int cathecticCountStartPosition = lotteryCode.lastIndexOf("(");
					int cathecticCountEndPosition = lotteryCode.lastIndexOf(")");

					String kindPlay = lotteryCode.substring(startPosition + 1, endPosition);
					String code = lotteryCode.substring(endPosition + 1, beishuStartPosition);
					String beishu = "    倍数:[" + lotteryCode.substring(beishuStartPosition + 1, beishuEndPosition) + "]";
					String payMoney = "    金额:[" + lotteryCode.substring(payMoneyStartPosition + 1, payMoneyEndPosition) + "]";
					String cathecticCount = "    投注数目:[" + lotteryCode.substring(cathecticCountStartPosition + 1, cathecticCountEndPosition)
							+ "]";

					// 幸运分分彩、韩国1.5分彩、新加坡2分彩、台湾5分彩、三分时时彩、五分时时彩、十分时时彩、江西时时彩、黑龙江时时彩、重庆时时彩、天津时时彩、新疆时时彩,五分11选5,三分11选5,老重庆时时彩
					if (lotteryType.equals(ELotteryKind.JLFFC.name()) || lotteryType.equals(ELotteryKind.HGFFC.name())
							|| lotteryType.equals(ELotteryKind.XJPLFC.name()) || lotteryType.equals(ELotteryKind.TWWFC.name())
							|| lotteryType.equals(ELotteryKind.SFSSC.name()) || lotteryType.equals(ELotteryKind.WFSSC.name())
							|| lotteryType.equals(ELotteryKind.SHFSSC.name()) || lotteryType.equals(ELotteryKind.JXSSC.name())
							|| lotteryType.equals(ELotteryKind.HLJSSC.name()) || lotteryType.equals(ELotteryKind.CQSSC.name())
							|| lotteryType.equals(ELotteryKind.TJSSC.name()) || lotteryType.equals(ELotteryKind.XJSSC.name())
							|| lotteryType.equals(ELotteryKind.WFSYXW.name()) || lotteryType.equals(ELotteryKind.SFSYXW.name())
							|| lotteryType.equals(ELotteryKind.JSSSC.name()) || lotteryType.equals(ELotteryKind.BJSSC.name())
							|| lotteryType.equals(ELotteryKind.GDSSC.name()) || lotteryType.equals(ELotteryKind.SCSSC.name())
							|| lotteryType.equals(ELotteryKind.SHSSC.name()) || lotteryType.equals(ELotteryKind.SDSSC.name())
							|| lotteryType.equals(ELotteryKind.LCQSSC.name())) {
						ESSCKind currentPlayKind = ESSCKind.valueOf(kindPlay);
						// 前二大小单双和后二大小单双投注号码处理
						if (currentPlayKind != null && currentPlayKind.equals(ESSCKind.QEDXDS) || currentPlayKind.equals(ESSCKind.HEDXDS)) {
							maps.put(currentPlayKind.getDescription(), dxdsCodeDeal(code) + beishu + payMoney + cathecticCount);
						} else if (currentPlayKind != null) {
							maps.put(currentPlayKind.getDescription(), code + beishu + payMoney + cathecticCount);
						}
					}
					// 山东11选5、广东11选5、江西11选5、福建11选5、重庆11选5,五分11选5,三分11选5
					else if (lotteryType.equals(ELotteryKind.SDSYXW.name()) || lotteryType.equals(ELotteryKind.GDSYXW.name())
							|| lotteryType.equals(ELotteryKind.JXSYXW.name()) || lotteryType.equals(ELotteryKind.FJSYXW.name())
							|| lotteryType.equals(ELotteryKind.CQSYXW.name()) || lotteryType.equals(ELotteryKind.WFSYXW.name())
							|| lotteryType.equals(ELotteryKind.WFSYXW.name())) {
						ESYXWKind currentPlayKind = ESYXWKind.valueOf(kindPlay);
						if (currentPlayKind != null && currentPlayKind.equals(ESYXWKind.QWDDS)) {
							maps.put(currentPlayKind.getDescription(), qwddsCodeDeal(code) + beishu + payMoney + cathecticCount);
						} else if (currentPlayKind != null) {
							maps.put(currentPlayKind.getDescription(), code + beishu + payMoney + cathecticCount);
						}
					}
					// 江苏快三、福建快三、安徽快三、湖北快三、吉林快三、北京快三、广西快三、上海快三、甘肃快三、幸运快三、三分快三、五分快三
					else if (lotteryType.equals(ELotteryKind.JSKS.name()) || lotteryType.equals(ELotteryKind.FJKS.name())
							|| lotteryType.equals(ELotteryKind.AHKS.name()) || lotteryType.equals(ELotteryKind.HBKS.name())
							|| lotteryType.equals(ELotteryKind.JLKS.name()) || lotteryType.equals(ELotteryKind.BJKS.name())
							|| lotteryType.equals(ELotteryKind.GXKS.name()) || lotteryType.equals(ELotteryKind.SHKS.name())
							|| lotteryType.equals(ELotteryKind.GSKS.name()) || lotteryType.equals(ELotteryKind.JYKS.name())
							|| lotteryType.equals(ELotteryKind.SFKS.name()) || lotteryType.equals(ELotteryKind.WFKS.name())) {
						EKSKind currentPlayKind = EKSKind.valueOf(kindPlay);
						if (currentPlayKind.equals(EKSKind.DXDS)) {
							maps.put(currentPlayKind.getDescription(), dxdsCodeDeal(code) + beishu + payMoney + cathecticCount);
						} else {
							maps.put(currentPlayKind.getDescription(), code + beishu + payMoney + cathecticCount);
						}
					}
					// 北京pk10、极速pk10、幸运飞艇、三分pk10、五分pk10、十分pk10
					else if (lotteryType.equals(ELotteryKind.BJPK10.name()) || lotteryType.equals(ELotteryKind.JSPK10.name())
							|| lotteryType.equals(ELotteryKind.XYFT.name()) || lotteryType.equals(ELotteryKind.SFPK10.name())
							|| lotteryType.equals(ELotteryKind.WFPK10.name()) || lotteryType.equals(ELotteryKind.SHFPK10.name())) {
						EPK10Kind currentPlayKind = EPK10Kind.valueOf(kindPlay);
						if (currentPlayKind.equals(EPK10Kind.DSGJ) || currentPlayKind.equals(EPK10Kind.DSYJ)
								|| currentPlayKind.equals(EPK10Kind.DSJJ) || currentPlayKind.equals(EPK10Kind.DSDSM)
								|| currentPlayKind.equals(EPK10Kind.DSDWM) || currentPlayKind.equals(EPK10Kind.DXGJ)
								|| currentPlayKind.equals(EPK10Kind.DXYJ) || currentPlayKind.equals(EPK10Kind.DXJJ)
								|| currentPlayKind.equals(EPK10Kind.DXDSM) || currentPlayKind.equals(EPK10Kind.DXDWM)) {
							maps.put(currentPlayKind.getDescription(), dxdsCodeDeal(code) + beishu + payMoney + cathecticCount);
						} else if (currentPlayKind.equals(EPK10Kind.LHD1VS10) || currentPlayKind.equals(EPK10Kind.LHD2VS9)
								|| currentPlayKind.equals(EPK10Kind.LHD3VS8) || currentPlayKind.equals(EPK10Kind.LHD4VS7)
								|| currentPlayKind.equals(EPK10Kind.LHD5VS6)) {
							maps.put(currentPlayKind.getDescription(), lhdCodeDeal(code) + beishu + payMoney + cathecticCount);
						} else {
							maps.put(currentPlayKind.getDescription(), code + beishu + payMoney + cathecticCount);
						}
					}
					// 幸运六合彩、香港六合彩、澳门六合彩
					else if (lotteryType.equals(ELotteryKind.LHC.name()) ||
							lotteryType.equals(ELotteryKind.AMLHC.name()) ||
							lotteryType.equals(ELotteryKind.XYLHC.name())) {
						ELHCKind currentPlayKind = ELHCKind.valueOf(kindPlay);
						maps.put(currentPlayKind.getDescription(), code + beishu + payMoney + cathecticCount);
					} else if (lotteryType.equals(ELotteryKind.XYEB.name())) { // 幸运28
						EXYEBKind currentPlayKind = EXYEBKind.valueOf(kindPlay);
						maps.put(currentPlayKind.getDescription(), code + beishu + payMoney + cathecticCount);
					} else if (lotteryType.equals(ELotteryKind.FCSDDPC.name())) { // 福彩3D
						EDPCKind currentPlayKind = EDPCKind.valueOf(kindPlay);
						// 前二大小单双和后二大小单双投注号码处理
						if (currentPlayKind != null && currentPlayKind.equals(EDPCKind.QEDXDS) || currentPlayKind.equals(EDPCKind.HEDXDS)) {
							maps.put(currentPlayKind.getDescription(), dxdsCodeDeal(code) + beishu + payMoney + cathecticCount);
						} else if (currentPlayKind != null) {
							maps.put(currentPlayKind.getDescription(), code + beishu + payMoney + cathecticCount);
						}
					} else {
						throw new RuntimeException("该彩种未配置");
					}
					list.add(maps);
				}
			}
		}
		return list;
	}

	/**
	 * 订单号码详情玩法名称替换
	 * 
	 * @param orderId
	 * @return
	 */
	public List<Map<String, String>> getCodesDesByKindReplace(String codes, String lotteryType, String bizSystem) {
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		Map<String, String> maps = null;

		if (codes != null) {
			if (!StringUtils.isEmpty(codes)) {
				String[] lotteryCodes = codes.split(ConstantUtil.SOURCECODE_PROTYPE_SPLIT);
				StringBuffer codeResult = new StringBuffer();
				for (String lotteryCode : lotteryCodes) {
					maps = new HashMap<String, String>();
					int startPosition = lotteryCode.indexOf("[");
					int endPosition = lotteryCode.indexOf("]");
					String kindPlay = lotteryCode.substring(startPosition + 1, endPosition);
					String code = lotteryCode.substring(endPosition + 1, lotteryCode.length());
					// 重庆时时彩、天津时时彩、新疆时时彩、江西时时彩、黑龙江时时彩
					if (lotteryType.equals(ELotteryKind.CQSSC.name()) || lotteryType.equals(ELotteryKind.TJSSC.name())
							|| lotteryType.equals(ELotteryKind.XJSSC.name()) || lotteryType.equals(ELotteryKind.JXSSC.name())
							|| lotteryType.equals(ELotteryKind.HLJSSC.name())) {
						ESSCKind currentPlayKind = ESSCKind.valueOf(kindPlay);
						// 五球/龙虎/梭哈、斗牛、后三总和、中三总和、前三总和判断
						String codeStr = "";
						if (kindPlay.equals("ZHWQLHSH") || kindPlay.equals("ZHQS") || kindPlay.equals("ZHZS") || kindPlay.equals("ZHHS")
								|| kindPlay.equals("ZHDN")) {
							Map<String, String> map = OrderDetailUtil.getCodesDesByKindReplace(kindPlay);
							String[] codeArrayStrings = code.split("\\[")[0].split(",");
							for (int i = 0; i < codeArrayStrings.length; i++) {
								if (i == codeArrayStrings.length - 1) {
									codeStr += map.get(codeArrayStrings[i]);
								} else {
									codeStr += map.get(codeArrayStrings[i]) + ",";
								}
							}
						}
						if (currentPlayKind != null) {
							// 前二大小单双和后二大小单双投注号码处理
							if (currentPlayKind.equals(ESSCKind.QEDXDS) || currentPlayKind.equals(ESSCKind.HEDXDS)) {
								codeResult.append(dxdsCodeDeal(code.substring(0, code.indexOf("["))))
										.append(code.substring(code.indexOf("["), code.length()));
								maps.put(currentPlayKind.getDescription(), codeResult.toString());
							} else {
								if (StringUtils.isNotBlank(codeStr)) {
									code += "codeStr" + codeStr;// 处理特殊判断
								}
								maps.put(currentPlayKind.getDescription(), code);
							}
						}
					}
					// 幸运分分彩、韩国1.5分彩、新加坡2分彩、台湾5分彩、三分时时彩、五分时时彩,老重庆时时彩
					else if (lotteryType.equals(ELotteryKind.JLFFC.name()) || lotteryType.equals(ELotteryKind.HGFFC.name())
							|| lotteryType.equals(ELotteryKind.XJPLFC.name()) || lotteryType.equals(ELotteryKind.TWWFC.name())
							|| lotteryType.equals(ELotteryKind.SFSSC.name()) || lotteryType.equals(ELotteryKind.WFSSC.name())
							|| lotteryType.equals(ELotteryKind.JSSSC.name()) || lotteryType.equals(ELotteryKind.BJSSC.name())
							|| lotteryType.equals(ELotteryKind.GDSSC.name()) || lotteryType.equals(ELotteryKind.SCSSC.name())
							|| lotteryType.equals(ELotteryKind.SHSSC.name()) || lotteryType.equals(ELotteryKind.SDSSC.name())
							|| lotteryType.equals(ELotteryKind.SHFSSC.name()) || lotteryType.equals(ELotteryKind.LCQSSC.name())) {
						ESSCKind currentPlayKind = ESSCKind.valueOf(kindPlay);
						// 五球/龙虎/梭哈、斗牛、后三总和、中三总和、前三总和判断
						String codeStr = "";
						if (kindPlay.equals("ZHWQLHSH") || kindPlay.equals("ZHQS") || kindPlay.equals("ZHZS") || kindPlay.equals("ZHHS")
								|| kindPlay.equals("ZHDN")) {
							Map<String, String> map = OrderDetailUtil.getCodesDesByKindReplace(kindPlay);
							String[] codeArrayStrings = code.split("\\[")[0].split(",");
							for (int i = 0; i < codeArrayStrings.length; i++) {
								if (i == codeArrayStrings.length - 1) {
									codeStr += map.get(codeArrayStrings[i]);
								} else {
									codeStr += map.get(codeArrayStrings[i]) + ",";
								}
							}
						}
						// 前二大小单双和后二大小单双投注号码处理
						if (currentPlayKind.equals(ESSCKind.QEDXDS) || currentPlayKind.equals(ESSCKind.HEDXDS)) {
							codeResult.append(dxdsCodeDeal(code.substring(0, code.indexOf("["))))
									.append(code.substring(code.indexOf("["), code.length()));
							maps.put(currentPlayKind.getDescription(), codeResult.toString());
						} else {
							if (StringUtils.isNotBlank(codeStr)) {
								code += "codeStr" + codeStr;// 处理特殊判断
							}
							maps.put(currentPlayKind.getDescription(), code);
						}
					}
					// 山东11选5、广东11选5、江西11选5、福建11选5、重庆11选5,五分11选5,三分11选五
					else if (lotteryType.equals(ELotteryKind.SDSYXW.name()) || lotteryType.equals(ELotteryKind.GDSYXW.name())
							|| lotteryType.equals(ELotteryKind.JXSYXW.name()) || lotteryType.equals(ELotteryKind.FJSYXW.name())
							|| lotteryType.equals(ELotteryKind.CQSYXW.name()) || lotteryType.equals(ELotteryKind.WFSYXW.name())
							|| lotteryType.equals(ELotteryKind.SFSYXW.name())) { //
						ESYXWKind currentPlayKind = ESYXWKind.valueOf(kindPlay);
						if (currentPlayKind != null) {
							// 前二大小单双和后二大小单双投注号码处理
							if (currentPlayKind.equals(ESYXWKind.QWDDS)) {
								codeResult.append(qwddsCodeDeal(code.substring(0, code.indexOf("["))))
										.append(code.substring(code.indexOf("["), code.length()));
								maps.put(currentPlayKind.getDescription(), codeResult.toString());
							} else {
								maps.put(currentPlayKind.getDescription(), code);
							}
						}
					}
					// 江苏快三、福建快三、安徽快三、湖北快三、吉林快三、北京快三、广西快三、上海快三、甘肃快三、幸运快三、三分快三、五分快三
					else if (lotteryType.equals(ELotteryKind.JSKS.name()) || lotteryType.equals(ELotteryKind.FJKS.name())
							|| lotteryType.equals(ELotteryKind.AHKS.name()) || lotteryType.equals(ELotteryKind.HBKS.name())
							|| lotteryType.equals(ELotteryKind.JLKS.name()) || lotteryType.equals(ELotteryKind.BJKS.name())
							|| lotteryType.equals(ELotteryKind.GXKS.name()) || lotteryType.equals(ELotteryKind.SHKS.name())
							|| lotteryType.equals(ELotteryKind.GSKS.name()) || lotteryType.equals(ELotteryKind.JYKS.name())
							|| lotteryType.equals(ELotteryKind.SFKS.name()) || lotteryType.equals(ELotteryKind.WFKS.name())) {
						EKSKind currentPlayKind = EKSKind.valueOf(kindPlay);
						// 大小单双号码处理
						if (currentPlayKind.equals(EKSKind.DXDS)) {
							codeResult.append(dxdsCodeDeal(code.substring(0, code.indexOf("["))))
									.append(code.substring(code.indexOf("["), code.length()));
							maps.put(currentPlayKind.getDescription(), codeResult.toString());
						} else {
							maps.put(currentPlayKind.getDescription(), code);
						}
					}
					// 北京pk10、极速pk10、幸运飞艇、三分pk10、五分pk10、十分pk10
					else if (lotteryType.equals(ELotteryKind.BJPK10.name()) || lotteryType.equals(ELotteryKind.JSPK10.name())
							|| lotteryType.equals(ELotteryKind.XYFT.name()) || lotteryType.equals(ELotteryKind.SFPK10.name())
							|| lotteryType.equals(ELotteryKind.WFPK10.name()) || lotteryType.equals(ELotteryKind.SHFPK10.name())) {
						EPK10Kind currentPlayKind = EPK10Kind.valueOf(kindPlay);
						// 大小单双号码处理
						if (currentPlayKind.equals(EPK10Kind.DSGJ) || currentPlayKind.equals(EPK10Kind.DSYJ)
								|| currentPlayKind.equals(EPK10Kind.DSJJ) || currentPlayKind.equals(EPK10Kind.DSDSM)
								|| currentPlayKind.equals(EPK10Kind.DSDWM) || currentPlayKind.equals(EPK10Kind.DXGJ)
								|| currentPlayKind.equals(EPK10Kind.DXYJ) || currentPlayKind.equals(EPK10Kind.DXJJ)
								|| currentPlayKind.equals(EPK10Kind.DXDSM) || currentPlayKind.equals(EPK10Kind.DXDWM)
								|| currentPlayKind.equals(EPK10Kind.DXGJ)) {
							codeResult.append(dxdsCodeDeal(code.substring(0, code.indexOf("["))))
									.append(code.substring(code.indexOf("["), code.length()));
							maps.put(currentPlayKind.getDescription(), codeResult.toString());
							// 龙虎斗号码处理
						} else if (currentPlayKind.equals(EPK10Kind.LHD1VS10) || currentPlayKind.equals(EPK10Kind.LHD2VS9)
								|| currentPlayKind.equals(EPK10Kind.LHD3VS8) || currentPlayKind.equals(EPK10Kind.LHD4VS7)
								|| currentPlayKind.equals(EPK10Kind.LHD5VS6)) {
							codeResult.append(lhdCodeDeal(code.substring(0, code.indexOf("["))))
									.append(code.substring(code.indexOf("["), code.length()));
							maps.put(currentPlayKind.getDescription(), codeResult.toString());
						} else {
							maps.put(currentPlayKind.getDescription(), code);
						}
					}
					// 香港六合彩、澳门六合彩、幸运六合彩
					else if (lotteryType.equals(ELotteryKind.LHC.name()) ||
							lotteryType.equals(ELotteryKind.AMLHC.name()) ||
							lotteryType.equals(ELotteryKind.XYLHC.name())) {
						ELHCKind currentPlayKind = ELHCKind.valueOf(kindPlay);
						maps.put(currentPlayKind.getDescription(), getLhcCodeDesc(bizSystem, kindPlay, code.substring(0, code.indexOf("[")))
								+ code.substring(code.indexOf("["), code.length()));
					} else if (lotteryType.equals(ELotteryKind.XYEB.name())) { // 幸运28
						EXYEBKind currentPlayKind = EXYEBKind.valueOf(kindPlay);
						maps.put(currentPlayKind.getDescription(),
								getXy28CodeDesc(bizSystem, kindPlay, code.substring(0, code.indexOf("[")))
										+ code.substring(code.indexOf("["), code.length()));
					} else if (lotteryType.equals(ELotteryKind.FCSDDPC.name())) { // 福彩3D
						EDPCKind currentPlayKind = EDPCKind.valueOf(kindPlay);
						// 前二大小单双和后二大小单双投注号码处理
						if (currentPlayKind.equals(EDPCKind.QEDXDS) || currentPlayKind.equals(EDPCKind.HEDXDS)) {
							codeResult.append(dxdsCodeDeal(code.substring(0, code.indexOf("["))))
									.append(code.substring(code.indexOf("["), code.length()));
							maps.put(currentPlayKind.getDescription(), codeResult.toString());
						} else {
							maps.put(currentPlayKind.getDescription(), code);
						}
					} else {
						throw new RuntimeException("该彩种未配置");
					}
					list.add(maps);
					codeResult.setLength(0);
				}
			}
		}
		return list;
	}

	/**
	 * 时时彩大小单双特殊玩法的号码处理
	 * 
	 * @param code
	 * @return
	 */
	private String dxdsCodeDeal(String code) {
		if (code != null) {
			code = code.replaceAll("1", "大");
			code = code.replaceAll("2", "小");
			code = code.replaceAll("3", "单");
			code = code.replaceAll("4", "双");
			return code;
		} else {
			return "";
		}
	}

	/**
	 * 获取六合彩玩法描述
	 * 
	 * @param code
	 * @return
	 */
	private String getLhcCodeDesc(String bizSystem, String palyKind, String code) {

		if (lhcPlayDescMap.size() == 0) {
			this.initLotteryWinLhcDesc(bizSystem);
		}
		String[] codes = code.split(",");
		String codesStr = "";
		for (String c : codes) {
			codesStr += "," + lhcPlayDescMap.get(palyKind + c);
		}
		if (codesStr.length() > 0) {
			return codesStr.substring(1);
		} else {
			return codesStr;
		}
	}

	/**
	 * 获取幸运28玩法描述
	 * 
	 * @param code
	 * @return
	 */
	private String getXy28CodeDesc(String bizSystem, String palyKind, String code) {

		if (xy28PlayDescMap.size() == 0) {
			this.initLotteryWinXy28Desc(bizSystem);
		}
		if (EXYEBKind.TMSB.name().equals(palyKind)) {
			palyKind = EXYEBKind.TM.name();
		}

		String[] codes = code.split(",");
		String codesStr = "";
		for (String c : codes) {
			codesStr += "," + xy28PlayDescMap.get(palyKind + c);
		}
		if (codesStr.length() > 0) {
			return codesStr.substring(1);
		} else {
			return codesStr;
		}
	}

	/**
	 * 根据种类获取玩法描述
	 * 
	 * @param query
	 * @return
	 */
	private Map<String, String> initLotteryWinLhcDesc(String bizSystem) {
		LotteryWinLhc query = new LotteryWinLhc();
		query.setBizSystem(bizSystem);
		List<LotteryWinLhc> list = lotteryWinLhcService.getLotteryWinLhcByKind(query);
		for (int i = 0; i < list.size(); i++) {
			LotteryWinLhc lotteryWinLhc = list.get(i);
			lhcPlayDescMap.put(lotteryWinLhc.getLotteryTypeProtype() + lotteryWinLhc.getCode(), lotteryWinLhc.getCodeDes());
		}

		return lhcPlayDescMap;
	}

	/**
	 * 根据种类获取玩法描述
	 * 
	 * @param query
	 * @return
	 */
	private Map<String, String> initLotteryWinXy28Desc(String bizSystem) {
		LotteryWinXyeb query = new LotteryWinXyeb();
		query.setBizSystem(bizSystem);
		List<LotteryWinXyeb> list = lotteryWinXy28Service.getLotteryWinXyebByKind(query);
		for (int i = 0; i < list.size(); i++) {
			LotteryWinXyeb lotteryWinXyeb = list.get(i);
			xy28PlayDescMap.put(lotteryWinXyeb.getLotteryTypeProtype() + lotteryWinXyeb.getCode(), lotteryWinXyeb.getCodeDes());
		}

		return xy28PlayDescMap;
	}

	/**
	 * 龙虎斗特殊玩法的号码处理
	 * 
	 * @param code
	 * @return
	 */
	private String lhdCodeDeal(String code) {
		if (code != null) {
			code = code.replaceAll("1", "龙");
			code = code.replaceAll("2", "虎");
			return code;
		} else {
			return "";
		}
	}

	/**
	 * 11选5趣味定单双号码特殊处理
	 * 
	 * @param code
	 * @return
	 */
	private String qwddsCodeDeal(String code) {
		if (code != null) {
			code = code.replaceAll("01", "5单0双");
			code = code.replaceAll("02", "4单1双");
			code = code.replaceAll("03", "3单2双");
			code = code.replaceAll("04", "2单3双");
			code = code.replaceAll("05", "1单4双");
			code = code.replaceAll("06", "0单5双");
			return code;
		} else {
			return "";
		}
	}

	/**
	 * 未开奖的回退
	 * 
	 * @param orderId
	 * @throws Exception
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void lotteryRegressionForNotDraw(Long orderId) throws UnEqualVersionException, Exception {
		Order order = this.selectByPrimaryKey(orderId);
		if (order == null) {
			throw new Exception("该订单数据不存在.");
		}

		// 只有在订单未进行开奖处理的订单,才能进行未中奖的回退
		if (order.getStatus().equals(EOrderStatus.END.name())) {
			throw new Exception("该订单已经结束了,不能进行回退了.");
		}
		if (!order.getProstate().equals(EProstateStatus.DEALING.name())) {
			throw new Exception("该订单已经进行中奖处理了,不能进行未中奖的回退");
		}
		List<UserTotalMoneyByTypeVo> userTotalMoneyByTypeVos = new ArrayList<UserTotalMoneyByTypeVo>();
		User orderUser = userService.selectByPrimaryKey(order.getUserId());
		if (order.getPayMoney().compareTo(new BigDecimal(0)) > 0) {
			MoneyDetail orderUserMoneyDetail = new MoneyDetail();
			orderUserMoneyDetail.setUserId(orderUser.getId()); // 用户id
			orderUserMoneyDetail.setOrderId(order.getId()); // 订单id
			orderUserMoneyDetail.setBizSystem(orderUser.getBizSystem());
			orderUserMoneyDetail.setUserName(orderUser.getUserName()); // 用户名
			orderUserMoneyDetail.setOperateUserName(ConstantUtil.SYSTEM_OPERATE_NAME); // 操作人员
			orderUserMoneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.LR)); // 订单单号
			orderUserMoneyDetail.setWinLevel(null);
			orderUserMoneyDetail.setWinCost(null);
			orderUserMoneyDetail.setWinCostRule(null);
			orderUserMoneyDetail.setDetailType(EMoneyDetailType.LOTTERY_REGRESSION.name()); // 投注退单
			orderUserMoneyDetail.setDetailDes(order.getDetailDes());
			orderUserMoneyDetail.setLotteryModel(order.getLotteryModel());
			orderUserMoneyDetail.setIncome(order.getPayMoney());
			orderUserMoneyDetail.setPay(new BigDecimal(0));
			orderUserMoneyDetail.setDescription("管理员未开奖回退，投注金额归还:" + order.getPayMoney());
			orderUserMoneyDetail.setBalanceBefore(orderUser.getMoney()); // 支出前金额
			orderUserMoneyDetail.setBalanceAfter(orderUser.getMoney().add(order.getPayMoney())); // 支出后的金额
			moneyDetailService.insertSelective(orderUserMoneyDetail); // 保存资金明细
			// 针对盈亏报表多线程不会事物回滚处理方式
			UserTotalMoneyByTypeVo userTotalMoneyByTypeVo = new UserTotalMoneyByTypeVo(orderUser, EMoneyDetailType.LOTTERY_REGRESSION,
					order.getPayMoney());
			userTotalMoneyByTypeVos.add(userTotalMoneyByTypeVo);
			// userMoneyTotalService.addUserTotalMoneyByType(orderUser,
			// EMoneyDetailType.LOTTERY_REGRESSION, order.getPayMoney());
			// 未开奖回退增加用户余额
			orderUser.addMoney(order.getPayMoney(), false);
		}
		// 改订单支付的可提现额度
		UseMoneyLog useMoneyLog = useMoneyLogService.getUseMoneylogBylotteryId(order.getLotteryId() + "_" + order.getId());
		if (useMoneyLog != null && useMoneyLog.getUseMoney().compareTo(new BigDecimal(0)) > 0) {
			// 还原投注使用的可提现额度
			orderUser.addCanWithdrawMoney(useMoneyLog.getUseMoney());
		}

		// 更新用户资金
		userService.updateByPrimaryKeyWithVersionSelective(orderUser);

		// 更新订单数据
		order.setProstate(EProstateStatus.NO_LOTTERY_BACKSPACE.name()); // 停止处理的订单
		order.setStatus(EOrderStatus.END.name());
		order.setUpdateDate(new Date());
		this.updateByPrimaryKeySelective(order);
		// 循环遍历插入报表总额和盈亏报表
		for (UserTotalMoneyByTypeVo userTotalMoneyByTypeVo : userTotalMoneyByTypeVos) {
			userMoneyTotalService.addUserTotalMoneyByType(userTotalMoneyByTypeVo.getUser(), userTotalMoneyByTypeVo.getDetailType(),
					userTotalMoneyByTypeVo.getOperationValue());
		}
	}

	/**
	 * 未开奖回退一键处理
	 * 
	 * @param orderId
	 * @throws Exception
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int hendLotteryRegressionDeal(ELotteryKind kind, String expect) throws UnEqualVersionException, Exception {
		int dealCount = 0;
		List<UserTotalMoneyByTypeVo> userTotalMoneyByTypeVos = new ArrayList<UserTotalMoneyByTypeVo>();
		OrderQuery query = new OrderQuery();
		query.setExpect(expect); // 期号
		query.setLotteryType(kind.name()); // 彩种
		query.setOrderSort(5);
		query.setProstateStatus(EProstateStatus.DEALING.getCode());

		// 查询需要处理未追号的订单
		List<Order> dealOrders = this.getOrderByCondition(query);
		if (CollectionUtils.isNotEmpty(dealOrders)) {
			logger.info("查询出该彩种期号的未开奖订单总数为[" + dealOrders.size() + "]");

			for (Order order : dealOrders) {

				// 只有在订单未进行开奖处理的订单,才能进行未中奖的回退
				if (order.getStatus().equals(EOrderStatus.END.name())) {
					logger.error("处理未开奖回退发现订单id[" + order.getId() + "],订单号[" + order.getLotteryId() + "]已经结束了,不能进行回退了");
					continue;
				}
				if (!order.getProstate().equals(EProstateStatus.DEALING.name())) {
					logger.error("处理未开奖回退发现订单id[" + order.getId() + "],订单号[" + order.getLotteryId() + "]已经进行中奖处理了,不能进行未中奖的回退");
					continue;
				}

				User orderUser = userService.selectByPrimaryKey(order.getUserId());
				if (order.getPayMoney().compareTo(new BigDecimal(0)) > 0) {
					MoneyDetail orderUserMoneyDetail = new MoneyDetail();
					orderUserMoneyDetail.setUserId(orderUser.getId()); // 用户id
					orderUserMoneyDetail.setOrderId(order.getId()); // 订单id
					orderUserMoneyDetail.setUserName(orderUser.getUserName()); // 用户名
					orderUserMoneyDetail.setBizSystem(orderUser.getBizSystem());
					orderUserMoneyDetail.setOperateUserName(ConstantUtil.SYSTEM_OPERATE_NAME); // 操作人员
					orderUserMoneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.LR)); // 订单单号
					orderUserMoneyDetail.setWinLevel(null);
					orderUserMoneyDetail.setWinCost(null);
					orderUserMoneyDetail.setWinCostRule(null);
					orderUserMoneyDetail.setDetailType(EMoneyDetailType.LOTTERY_REGRESSION.name()); // 投注退单
					orderUserMoneyDetail.setDetailDes(order.getDetailDes());
					orderUserMoneyDetail.setLotteryModel(order.getLotteryModel());
					orderUserMoneyDetail.setIncome(order.getPayMoney());
					orderUserMoneyDetail.setPay(new BigDecimal(0));
					orderUserMoneyDetail.setDescription("管理员未开奖回退，投注金额归还:" + order.getPayMoney());
					orderUserMoneyDetail.setBalanceBefore(orderUser.getMoney()); // 支出前金额
					orderUserMoneyDetail.setBalanceAfter(orderUser.getMoney().add(order.getPayMoney())); // 支出后的金额
					moneyDetailService.insertSelective(orderUserMoneyDetail); // 保存资金明细
					// 针对盈亏报表多线程不会事物回滚处理方式
					UserTotalMoneyByTypeVo userTotalMoneyByTypeVo = new UserTotalMoneyByTypeVo(orderUser,
							EMoneyDetailType.LOTTERY_REGRESSION, order.getPayMoney());
					userTotalMoneyByTypeVos.add(userTotalMoneyByTypeVo);
					// userMoneyTotalService.addUserTotalMoneyByType(orderUser,
					// EMoneyDetailType.LOTTERY_REGRESSION,
					// order.getPayMoney());
					// 未开奖回退增加用户余额
					orderUser.addMoney(order.getPayMoney(), false);
				}
				// 改订单支付的可提现额度
				UseMoneyLog useMoneyLog = useMoneyLogService.getUseMoneylogBylotteryId(order.getLotteryId() + "_" + order.getId());
				if (useMoneyLog != null && useMoneyLog.getUseMoney().compareTo(new BigDecimal(0)) > 0) {
					orderUser.addCanWithdrawMoney(useMoneyLog.getUseMoney());
				}

				// 更新用户资金
				userService.updateByPrimaryKeyWithVersionSelective(orderUser);

				// 更新订单数据
				order.setProstate(EProstateStatus.NO_LOTTERY_BACKSPACE.name()); // 停止处理的订单
				order.setStatus(EOrderStatus.END.name());
				order.setUpdateDate(new Date());

				this.updateByPrimaryKeySelective(order);
				dealCount++;
			}
		}
		// 循环遍历插入报表总额和盈亏报表
		for (UserTotalMoneyByTypeVo userTotalMoneyByTypeVo : userTotalMoneyByTypeVos) {
			userMoneyTotalService.addUserTotalMoneyByType(userTotalMoneyByTypeVo.getUser(), userTotalMoneyByTypeVo.getDetailType(),
					userTotalMoneyByTypeVo.getOperationValue());
		}
		return dealCount;
	}

	/**
	 * 未开奖回退一键处理-幸运彩
	 * 
	 * @param orderId
	 * @throws Exception
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int hendLotteryRegressionDeal(String bizSystem, ELotteryKind kind, String expect) throws UnEqualVersionException, Exception {
		int dealCount = 0;
		List<UserTotalMoneyByTypeVo> userTotalMoneyByTypeVos = new ArrayList<UserTotalMoneyByTypeVo>();
		OrderQuery query = new OrderQuery();
		query.setExpect(expect); // 期号
		query.setLotteryType(kind.name()); // 彩种
		query.setOrderSort(5);
		query.setProstateStatus(EProstateStatus.DEALING.getCode());
		// 业务系统不带时，支持处理所有业务系统
		if (StringUtils.isNotBlank(bizSystem)) {
			query.setBizSystem(bizSystem);
		}
		// 查询需要处理未追号的订单
		List<Order> dealOrders = this.getOrderByCondition(query);
		if (CollectionUtils.isNotEmpty(dealOrders)) {
			logger.info("查询出该彩种期号的未开奖订单总数为[" + dealOrders.size() + "]");

			for (Order order : dealOrders) {

				// 只有在订单未进行开奖处理的订单,才能进行未中奖的回退
				if (order.getStatus().equals(EOrderStatus.END.name())) {
					logger.error("处理未开奖回退发现订单id[" + order.getId() + "],订单号[" + order.getLotteryId() + "]已经结束了,不能进行回退了");
					continue;
				}
				if (!order.getProstate().equals(EProstateStatus.DEALING.name())) {
					logger.error("处理未开奖回退发现订单id[" + order.getId() + "],订单号[" + order.getLotteryId() + "]已经进行中奖处理了,不能进行未中奖的回退");
					continue;
				}

				User orderUser = userService.selectByPrimaryKey(order.getUserId());
				if (order.getPayMoney().compareTo(new BigDecimal(0)) > 0) {
					MoneyDetail orderUserMoneyDetail = new MoneyDetail();
					orderUserMoneyDetail.setUserId(orderUser.getId()); // 用户id
					orderUserMoneyDetail.setOrderId(order.getId()); // 订单id
					orderUserMoneyDetail.setUserName(orderUser.getUserName()); // 用户名
					orderUserMoneyDetail.setBizSystem(orderUser.getBizSystem());
					orderUserMoneyDetail.setOperateUserName(ConstantUtil.SYSTEM_OPERATE_NAME); // 操作人员
					orderUserMoneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.LR)); // 订单单号
					orderUserMoneyDetail.setWinLevel(null);
					orderUserMoneyDetail.setWinCost(null);
					orderUserMoneyDetail.setWinCostRule(null);
					orderUserMoneyDetail.setDetailType(EMoneyDetailType.LOTTERY_REGRESSION.name()); // 投注退单
					orderUserMoneyDetail.setDetailDes(order.getDetailDes());
					orderUserMoneyDetail.setLotteryModel(order.getLotteryModel());
					orderUserMoneyDetail.setIncome(order.getPayMoney());
					orderUserMoneyDetail.setPay(new BigDecimal(0));
					orderUserMoneyDetail.setDescription("管理员未开奖回退，投注金额归还:" + order.getPayMoney());
					orderUserMoneyDetail.setBalanceBefore(orderUser.getMoney()); // 支出前金额
					orderUserMoneyDetail.setBalanceAfter(orderUser.getMoney().add(order.getPayMoney())); // 支出后的金额
					moneyDetailService.insertSelective(orderUserMoneyDetail); // 保存资金明细
					// 针对盈亏报表多线程不会事物回滚处理方式
					UserTotalMoneyByTypeVo userTotalMoneyByTypeVo = new UserTotalMoneyByTypeVo(orderUser,
							EMoneyDetailType.LOTTERY_REGRESSION, order.getPayMoney());
					userTotalMoneyByTypeVos.add(userTotalMoneyByTypeVo);
					// userMoneyTotalService.addUserTotalMoneyByType(orderUser,
					// EMoneyDetailType.LOTTERY_REGRESSION,
					// order.getPayMoney());
					// 未开奖回退增加用户余额
					orderUser.addMoney(order.getPayMoney(), false);
				}
				// 改订单支付的可提现额度
				UseMoneyLog useMoneyLog = useMoneyLogService.getUseMoneylogBylotteryId(order.getLotteryId() + "_" + order.getId());
				if (useMoneyLog != null && useMoneyLog.getUseMoney().compareTo(new BigDecimal(0)) > 0) {
					orderUser.addCanWithdrawMoney(useMoneyLog.getUseMoney());
				}

				// 更新用户资金
				userService.updateByPrimaryKeyWithVersionSelective(orderUser);

				// 更新订单数据
				order.setProstate(EProstateStatus.NO_LOTTERY_BACKSPACE.name()); // 停止处理的订单
				order.setStatus(EOrderStatus.END.name());
				order.setUpdateDate(new Date());

				this.updateByPrimaryKeySelective(order);
				dealCount++;
			}
		}
		// 循环遍历插入报表总额和盈亏报表
		for (UserTotalMoneyByTypeVo userTotalMoneyByTypeVo : userTotalMoneyByTypeVos) {
			userMoneyTotalService.addUserTotalMoneyByType(userTotalMoneyByTypeVo.getUser(), userTotalMoneyByTypeVo.getDetailType(),
					userTotalMoneyByTypeVo.getOperationValue());
		}
		return dealCount;

	}

	/**
	 * 强制回退
	 * 
	 * @param orderId
	 * @throws Exception
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void lotteryRegression(Long orderId) throws UnEqualVersionException, Exception {
		Order order = this.selectByPrimaryKey(orderId);
		if (order == null) {
			throw new Exception("该订单数据不存在.");
		}
		if (!order.getProstate().equals(EProstateStatus.DEALING.name()) && !order.getProstate().equals(EProstateStatus.NOT_WINNING.name())
				&& !order.getProstate().equals(EProstateStatus.WINNING.name())) {
			throw new Exception("该订单不支持强制回退,改订单已经撤单过了.");
		}
		// Calendar calendar = Calendar.getInstance();
		// calendar.setTime(new Date());
		// Date endTime = ClsDateHelper.getClsEndTime(new Date());
		// //当期时间小于2:59:59
		// if(calendar.getTime().before(endTime)){
		// calendar.add(Calendar.DAY_OF_MONTH, -1);
		// Date startTime = ClsDateHelper.getClsStartTime(new Date());
		// //比昨天的三点之前订单强制回退失败
		// if(order.getCreatedDate().before(startTime)){
		// throw new Exception("强制回退只能当天处理！请手工核算去扣费！");
		// }
		// }else{
		// //订单强制回退，只能在当天强制回退
		// if(order.getCreatedDate().before(endTime)){
		// throw new Exception("强制回退只能当天处理！请手工核算去扣费！");
		// }
		// }

		List<UserTotalMoneyByTypeVo> userTotalMoneyByTypeVos = new ArrayList<UserTotalMoneyByTypeVo>();
		User orderUser = userService.selectByPrimaryKey(order.getUserId());
		if (order.getPayMoney().compareTo(BigDecimal.ZERO) > 0) {
			MoneyDetail orderUserMoneyDetail = new MoneyDetail();
			orderUserMoneyDetail.setUserId(orderUser.getId()); // 用户id
			orderUserMoneyDetail.setOrderId(order.getId()); // 订单id
			orderUserMoneyDetail.setBizSystem(orderUser.getBizSystem());
			orderUserMoneyDetail.setUserName(orderUser.getUserName()); // 用户名
			orderUserMoneyDetail.setOperateUserName(ConstantUtil.SYSTEM_OPERATE_NAME); // 操作人员
			orderUserMoneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.LR)); // 订单单号
			orderUserMoneyDetail.setWinLevel(null);
			orderUserMoneyDetail.setWinCost(null);
			orderUserMoneyDetail.setWinCostRule(null);
			orderUserMoneyDetail.setDetailType(EMoneyDetailType.LOTTERY_REGRESSION.name()); // 投注退单
			orderUserMoneyDetail.setDetailDes(order.getDetailDes());
			orderUserMoneyDetail.setLotteryModel(order.getLotteryModel());
			orderUserMoneyDetail.setIncome(order.getPayMoney());
			orderUserMoneyDetail.setPay(new BigDecimal(0));
			orderUserMoneyDetail.setDescription("管理员订单强制回退,投注金额归还:" + order.getPayMoney());
			orderUserMoneyDetail.setBalanceBefore(orderUser.getMoney()); // 支出前金额
			orderUserMoneyDetail.setBalanceAfter(orderUser.getMoney().add(order.getPayMoney())); // 支出后的金额
			moneyDetailService.insertSelective(orderUserMoneyDetail); // 保存资金明细
			// 针对盈亏报表多线程不会事物回滚处理方式
			UserTotalMoneyByTypeVo userTotalMoneyByTypeVo = new UserTotalMoneyByTypeVo(orderUser, EMoneyDetailType.LOTTERY_REGRESSION,
					order.getPayMoney());
			userTotalMoneyByTypeVos.add(userTotalMoneyByTypeVo);
			// userMoneyTotalService.addUserTotalMoneyByType(orderUser,
			// EMoneyDetailType.LOTTERY_REGRESSION, order.getPayMoney());
			// 强制退的增加用户余额
			orderUser.addMoney(order.getPayMoney(), false);
		}

		if (order.getWinCost().compareTo(BigDecimal.ZERO) > 0) {
			if (order.getWinCost().compareTo(orderUser.getMoney()) > 0) {
				throw new Exception("用户余额不足！强制回退失败！若需要后续扣除中奖金额，请手动将用户提现锁定");
			}
			MoneyDetail orderUserWinMoneyDetail = new MoneyDetail();
			orderUserWinMoneyDetail.setUserId(orderUser.getId()); // 用户id
			orderUserWinMoneyDetail.setOrderId(order.getId()); // 订单id
			orderUserWinMoneyDetail.setBizSystem(orderUser.getBizSystem());
			orderUserWinMoneyDetail.setUserName(orderUser.getUserName()); // 用户名
			orderUserWinMoneyDetail.setOperateUserName(ConstantUtil.SYSTEM_OPERATE_NAME); // 操作人员
			orderUserWinMoneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.ZJ)); // 订单单号
			orderUserWinMoneyDetail.setWinLevel(null);
			orderUserWinMoneyDetail.setWinCost(null);
			orderUserWinMoneyDetail.setWinCostRule(null);
			orderUserWinMoneyDetail.setDetailType(EMoneyDetailType.WIN.name()); // 投注退单
			orderUserWinMoneyDetail.setDetailDes(order.getDetailDes());
			orderUserWinMoneyDetail.setLotteryModel(order.getLotteryModel());
			orderUserWinMoneyDetail.setPay(order.getWinCost());
			orderUserWinMoneyDetail.setIncome(new BigDecimal(0));
			orderUserWinMoneyDetail.setDescription("管理员订单强制回退,中奖金额扣除:" + order.getWinCost());
			orderUserWinMoneyDetail.setBalanceBefore(orderUser.getMoney()); // 支出前金额
			orderUserWinMoneyDetail.setBalanceAfter(orderUser.getMoney().subtract(order.getWinCost())); // 支出后的金额
			moneyDetailService.insertSelective(orderUserWinMoneyDetail); // 保存资金明细
			// 针对盈亏报表多线程不会事物回滚处理方式
			UserTotalMoneyByTypeVo userTotalMoneyByTypeVo = new UserTotalMoneyByTypeVo(orderUser, EMoneyDetailType.WIN,
					new BigDecimal("-" + order.getWinCost()));
			userTotalMoneyByTypeVos.add(userTotalMoneyByTypeVo);
			// 中奖金额，回退扣减
			// userMoneyTotalService.addUserTotalMoneyByType(orderUser,
			// EMoneyDetailType.WIN, new BigDecimal("-"+order.getWinCost()));
			// 将奖金从可提现的额度扣除
			orderUser.reduceMoney(order.getWinCost(), true);
		}

		// 改订单支付的可提现额度
		UseMoneyLog useMoneyLog = useMoneyLogService.getUseMoneylogBylotteryIdForForce(order.getLotteryId() + "_" + order.getId());
		if (useMoneyLog != null && useMoneyLog.getUseMoney().compareTo(new BigDecimal(0)) > 0) {
			// 还原用户的可提现额度
			orderUser.addCanWithdrawMoney(useMoneyLog.getUseMoney());
		}
		// 自身的返点扣除,在有中奖处理的情况下
		if (order.getProstate().equals(EProstateStatus.NOT_WINNING.name()) || order.getProstate().equals(EProstateStatus.WINNING.name())) {
			// 当时的订单用户,主要是模式
			User currentOrderUser = this.setOrderUser(order);
			BigDecimal currentUserRebateValue = AwardModelUtil.calculateOrderRebateMoney(order, currentOrderUser, null);
			if (currentUserRebateValue.compareTo(new BigDecimal(0)) > 0) {
				if (currentUserRebateValue.compareTo(orderUser.getMoney()) > 0) {
					throw new Exception("用户余额不足！强制回退失败！");
				}
				MoneyDetail orderUserRebateDetail = new MoneyDetail();
				orderUserRebateDetail.setUserId(orderUser.getId()); // 用户id
				orderUserRebateDetail.setOrderId(order.getId()); // 订单id
				orderUserRebateDetail.setBizSystem(orderUser.getBizSystem());
				orderUserRebateDetail.setUserName(orderUser.getUserName()); // 用户名
				orderUserRebateDetail.setOperateUserName(ConstantUtil.SYSTEM_OPERATE_NAME); // 操作人员
				orderUserRebateDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.FS)); // 订单单号
				orderUserRebateDetail.setWinLevel(null);
				orderUserRebateDetail.setWinCost(null);
				orderUserRebateDetail.setWinCostRule(null);
				orderUserRebateDetail.setDetailType(EMoneyDetailType.REBATE.name()); // 投注退单
				orderUserRebateDetail.setDetailDes(order.getDetailDes());
				orderUserRebateDetail.setLotteryModel(order.getLotteryModel());
				orderUserRebateDetail.setPay(currentUserRebateValue);
				orderUserRebateDetail.setIncome(new BigDecimal(0));
				orderUserRebateDetail.setDescription("管理员订单异常回退:" + currentUserRebateValue);
				orderUserRebateDetail.setBalanceBefore(orderUser.getMoney()); // 支出前金额
				orderUserRebateDetail.setBalanceAfter(orderUser.getMoney().subtract(currentUserRebateValue)); // 支出后的金额
				moneyDetailService.insertSelective(orderUserRebateDetail); // 保存资金明细

				// 用户自身返点的扣除
				orderUser.reduceMoney(currentUserRebateValue, true);
				// 针对盈亏报表多线程不会事物回滚处理方式
				UserTotalMoneyByTypeVo userTotalMoneyByTypeVo = new UserTotalMoneyByTypeVo(orderUser, EMoneyDetailType.REBATE,
						new BigDecimal("-" + currentUserRebateValue));
				userTotalMoneyByTypeVos.add(userTotalMoneyByTypeVo);
				// userMoneyTotalService.addUserTotalMoneyByType(orderUser,
				// EMoneyDetailType.REBATE, new
				// BigDecimal("-"+currentUserRebateValue));

			}
		}

		// 更新用户资金
		userService.updateByPrimaryKeyWithVersionSelective(orderUser);
		// 更新订单数据
		order.setProstate(EProstateStatus.UNUSUAL_BACKSPACE.name()); // 停止处理的订单
		order.setStatus(EOrderStatus.END.name());
		this.updateByPrimaryKeySelective(order);

		// 将账号锁定
		// User dealUser = new User();
		// dealUser.setId(orderUser.getId());
		// dealUser.setUserName(orderUser.getUserName());
		// dealUser.setLoginLock(1);
		// userService.updateByPrimaryKeySelective(dealUser);
		// 循环遍历插入报表总额和盈亏报表
		for (UserTotalMoneyByTypeVo userTotalMoneyByTypeVo : userTotalMoneyByTypeVos) {
			userMoneyTotalService.addUserTotalMoneyByType(userTotalMoneyByTypeVo.getUser(), userTotalMoneyByTypeVo.getDetailType(),
					userTotalMoneyByTypeVo.getOperationValue());
		}
		// 置使锁定的用户退出单点登录
		// SingleLoginForRedis.deleteAlreadyEnter(null,Login.LOGTYPE_PC+"."+dealUser.getBizSystem(),
		// dealUser.getUserName());
		// SingleLoginForRedis.deleteAlreadyEnter(null,Login.LOGTYPE_MOBILE+"."+dealUser.getBizSystem(),
		// dealUser.getUserName());
	}

	/**
	 * 订单用户的模式数据
	 * 
	 * @return
	 */
	private User setOrderUser(Order order) {
		User orderUser = new User();
		if (order.getLotteryType().toUpperCase().contains(ELotteryTopKind.SSC.name())) { // 时时彩返点
			orderUser.setSscRebate(order.getCurrentUserModel());
		} else if (order.getLotteryType().toUpperCase().contains(ELotteryTopKind.FFC.name())) { // 分分彩返点
			orderUser.setFfcRebate(order.getCurrentUserModel());
		} else if (order.getLotteryType().toUpperCase().contains(ELotteryTopKind.SYXW.name())) { // 11选5返点
			orderUser.setSyxwRebate(order.getCurrentUserModel());
		} else if (order.getLotteryType().toUpperCase().contains(ELotteryTopKind.DPC.name())) { // 低频彩
			orderUser.setDpcRebate(order.getCurrentUserModel());
		} else if (order.getLotteryType().toUpperCase().contains(ELotteryTopKind.KS.name())) { // 快三
			orderUser.setKsRebate(order.getCurrentUserModel());
		} else if (order.getLotteryType().toUpperCase().contains(ELotteryTopKind.PK10.name())) { // 北京pk10
			orderUser.setPk10Rebate(order.getCurrentUserModel());
		} else if (order.getLotteryType().toUpperCase().contains(ELotteryTopKind.LHC.name())) { // 六合彩
			// 没六合
		} else {
			throw new RuntimeException("未实现该彩种的返点");
		}
		return orderUser;
	}

	/**
	 * 根据订单id查找队赢得投注记录
	 * 
	 * @param id
	 * @return
	 */
	public Order getOrdertById(Long orderId) {
		return orderMapper.getOrdertById(orderId);
	}

	public static void main(String[] args) {
		// OrderService orderService = new OrderService();
		// System.out.println(orderService.getUniqueLotteryId("1960", "SSC"));
		// Date endTime = ClsDateHelper.getClsEndTime(new Date());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.set(Calendar.HOUR_OF_DAY, 3);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);

		System.out.println(calendar.get(Calendar.HOUR_OF_DAY));
		System.out.println(calendar.getTime());
	}

	/**
	 * 查找当前自己用户的追号记录
	 * 
	 * @return
	 */
	public Page getOrderAfterNumsByQueryPage(OrderQuery query, Page page) {
		page.setTotalRecNum(orderMapper.getAllOrderAfterNumsByQueryPageCount(query));
		page.setPageContent(orderMapper.getAllOrderAfterNumsByQueryPage(query, page.getStartIndex(), page.getPageSize()));
		return page;
	}

	/**
	 * 查找用户投注历史数据
	 * 
	 * @return
	 */
	public Page getUserOrdersByQueryPage(OrderQuery query, Page page) {
		try {
			page.setTotalRecNum(orderMapper.getUserOrdersByQueryPageCount(query));
			page.setPageContent(orderMapper.getUserOrdersByQueryPage(query, page.getStartIndex(), page.getPageSize()));
		} catch (Exception e) {
			logger.error("查询用户投注历史记录出错", e);
		}
		return page;
	}

	/**
	 * 查找用户的追号记录数据
	 * 
	 * @return
	 */
	public Page getUserAfterNumsByQueryPage(OrderQuery query, Page page) {
		page.setTotalRecNum(orderMapper.getUserOrderAfterNumsByQueryPageCount(query));
		page.setPageContent(orderMapper.getUserOrderAfterNumsByQueryPage(query, page.getStartIndex(), page.getPageSize()));
		return page;
	}

	public int updateOrderRegfrom(User user) {
		return orderMapper.updateOrderRegfrom(user);
	}

	/**
	 * 查询每日投注人数(按登录类型统计)
	 * 
	 * @param query
	 * @return
	 */
	public List<DayOnlineTypeCountVo> getDayLotteryTypeCount(OrderQuery query) {
		return orderMapper.getDayLotteryTypeCount(query);
	}

	/**
	 * 查询每日投注人数
	 * 
	 * @param query
	 * @return
	 */
	public Integer getDayLotteryCount(OrderQuery query) {
		return orderMapper.getDayLotteryCount(query);
	}

	/**
	 * 查询每日彩种盈利情况
	 * 
	 * @param query
	 * @return
	 */
	public List<Order> getLotteryDayGain(OrderQuery query) {
		return orderMapper.getLotteryDayGain(query);
	}

	/**
	 * 返回六合彩，每期的投注总额
	 * 
	 * @param query
	 * @return
	 */
	public Order getLhcLotteryMoney(OrderQuery query) {

		return orderMapper.getLhcLotteryMoney(query);
	}

	/**
	 * 统计用户的有效
	 * 
	 * @param query
	 * @return
	 */
	public Integer getTeamUserLotteryByQueryCount(OrderQuery query) {
		return orderMapper.getTeamUserLotteryByQueryCount(query);
	}

	/**
	 * 根据条件统计某些彩种的总投注额
	 * 
	 * @param query
	 * @return
	 */
	public BigDecimal getDayConsumeOrderByLottery(OrderQuery query) {
		return orderMapper.getDayConsumeOrderByLottery(query);
	}

	/**
	 * 根据条件查询订单总额
	 * @param query
	 * @return
	 */
	public Order getOrdersTotalByQuery(OrderQuery query) {
		return orderMapper.getOrdersTotalByQuery(query);
	}
}
