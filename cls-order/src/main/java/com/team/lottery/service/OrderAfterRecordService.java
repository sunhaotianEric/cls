package com.team.lottery.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.mapper.orderrecord.OrderAfterRecordMapper;
import com.team.lottery.vo.OrderAfterRecord;

@Service("orderAfterRecordService")
public class OrderAfterRecordService {

	@Autowired
	private OrderAfterRecordMapper orderAfterRecordMapper;
	
    public int deleteByPrimaryKey(Long id){
    	return orderAfterRecordMapper.deleteByPrimaryKey(id);
    }

    public int insert(OrderAfterRecord record){
    	return orderAfterRecordMapper.insert(record);
    }

    public int insertSelective(OrderAfterRecord record){
    	return orderAfterRecordMapper.insertSelective(record);
    }

    public OrderAfterRecord selectByPrimaryKey(Long id){
    	return orderAfterRecordMapper.selectByPrimaryKey(id);
    }

    public int updateByPrimaryKeySelective(OrderAfterRecord record){
    	return orderAfterRecordMapper.updateByPrimaryKeySelective(record);
    }

    public int updateByPrimaryKey(OrderAfterRecord record){
    	return orderAfterRecordMapper.updateByPrimaryKey(record);
    }
    
    /**
     * 查询追号订单的状态记录
     * @param lotteryId
     * @return
     */
    public OrderAfterRecord getOrderAfterRecord(String lotteryId){
    	return orderAfterRecordMapper.getOrderAfterRecord(lotteryId);
    }
}
