package com.team.lottery.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.mapper.usemoneylog.UseMoneyLogMapper;
import com.team.lottery.vo.UseMoneyLog;

@Service("useMoneyLogService")
public class UseMoneyLogService {

	@Autowired
	private UseMoneyLogMapper useMoneyLogMapper;
	
    public int deleteByPrimaryKey(Long id){
    	return useMoneyLogMapper.deleteByPrimaryKey(id);
    }

    public int insert(UseMoneyLog record){
    	return useMoneyLogMapper.insert(record);
    }

    public int insertSelective(UseMoneyLog record){
    	record.setCreatedDate(new Date());
    	return useMoneyLogMapper.insertSelective(record);    	
    }

    public UseMoneyLog selectByPrimaryKey(Long id){
    	return useMoneyLogMapper.selectByPrimaryKey(id);
    }

    public int updateByPrimaryKeySelective(UseMoneyLog record){
    	return useMoneyLogMapper.updateByPrimaryKeySelective(record);
    }

    public int updateByPrimaryKey(UseMoneyLog record){
    	return useMoneyLogMapper.updateByPrimaryKey(record);
    }
    
    /**
     * 根据订单号,查询对应的扣除的可提现金额
     * @param lotteryId
     * @return
     */
    public UseMoneyLog getUseMoneylogBylotteryId(String lotteryId){
    	return useMoneyLogMapper.getUseMoneylogBylotteryId(lotteryId);
    }
    
    /**
     * 获取追号的所有可回收的金额
     * @param lotteryId
     * @return
     */
    public List<UseMoneyLog> getUseMoneylogsBylotteryIdLike(String lotteryId){
    	return useMoneyLogMapper.getUseMoneylogsBylotteryIdLike(lotteryId);
    }
    
    /**
     * 根据订单号,查询对应的扣除的可提现金额
     * @param lotteryId
     * @return
     */
    public UseMoneyLog getUseMoneylogBylotteryIdForForce(String lotteryId){
    	return useMoneyLogMapper.getUseMoneylogBylotteryIdForForce(lotteryId);
    }
    
    /**
     * 获取追号的所有可回收的金额
     * @param lotteryId
     * @return
     */
    public List<UseMoneyLog> getUseMoneylogsBylotteryIdLikeForForce(String lotteryId){
    	return useMoneyLogMapper.getUseMoneylogsBylotteryIdLikeForForce(lotteryId);
    }
}
