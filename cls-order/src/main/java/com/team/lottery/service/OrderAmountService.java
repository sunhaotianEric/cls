package com.team.lottery.service;

import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.OrderDayNumVo;
import com.team.lottery.extvo.OrderQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.orderamount.OrderAmountMapper;
import com.team.lottery.vo.OrderAmount;

@Service("orderAmountService")
public class OrderAmountService {
	
	@Autowired
	private OrderAmountMapper orderAmountMapper;
	
    public int deleteByPrimaryKey(Long id){
    	return orderAmountMapper.deleteByPrimaryKey(id);
    }

    public int insert(OrderAmount record){
    	return orderAmountMapper.insert(record);
    }
    
    public int insertSelective(OrderAmount record){
    	return orderAmountMapper.insertSelective(record);
    }
    
    public OrderAmount selectByPrimaryKey(Long id){
    	return orderAmountMapper.selectByPrimaryKey(id);
    }

    public int updateByPrimaryKeySelective(OrderAmount record){
    	return orderAmountMapper.updateByPrimaryKeySelective(record);
    }

    public int updateByPrimaryKey(OrderAmount record){
    	return orderAmountMapper.updateByPrimaryKey(record);
    }
    
    /**
     * 查询用户的日量
     * @return
     */
	public Page getUserDayOrderNumByQueryPage(OrderQuery query, Page page) {
		page.setTotalRecNum(orderAmountMapper.getUserDayOrderNumByQueryPageCount(query));
		page.setPageContent(orderAmountMapper.getUserDayOrderNumByQueryPage(query,page.getStartIndex(),page.getPageSize()));
    	return page;
	}
	
	/**
	 * 条件查询代理用户的总日量 
	 * @param query
	 * @return
	 */
	public OrderDayNumVo getTotalUserDayOrderNum(OrderQuery query) {
		return orderAmountMapper.getTotalUserDayOrderNum(query);
	}
	
    
    /**
     * 查询某个时间段的用户团队(包括自己)所有有效投注记录
     * @return
     */
	public List<OrderAmount> getUserTeamEffectiveLotterys(OrderQuery query) {
		return orderAmountMapper.getUserTeamEffectiveLotterys(query);
	}
	
	/**
     * 查询某个时间段的用户团队(包括自己)所有有效投注记录总和
     * @return
     */
	public BigDecimal getUserTeamTotalEffectiveLotterys(OrderQuery query) {
		return orderAmountMapper.getUserTeamTotalEffectiveLotterys(query);
	}

    /**
	 * 查询所有用户的有效投注量 即用户的消费，用于佣金计算
	 * @param query
	 * @return
	 */
	public List<OrderAmount> getUserEffectiveLotterys(@Param("query")OrderQuery query) {
		return orderAmountMapper.getUserEffectiveLotterys(query);
	}
	
	 /**
     * 获取用户的有效投注列表
     * @param query
     * @return
     */
    public Page getEffectiveLotterysByQueryPage(OrderQuery query,Page page){
		page.setTotalRecNum(orderAmountMapper.getEffectiveLotterysByQueryPageCount(query));
		page.setPageContent(orderAmountMapper.getEffectiveLotterysByQueryPage(query,page.getStartIndex(),page.getPageSize()));
    	return page;
    }
    
    /**
     * 统计用户的有效投注列表
     * @param query
     * @return
     */
    public Integer getEffectiveLotterysTotalByQuery(OrderQuery query){
    	return orderAmountMapper.getEffectiveLotterysTotalByQuery(query);
    }
    
    /**
     * 根据期号和类型删除记录
     * @param expect
     * @param lotteryType
     * @return
     */
    public int deleteByExceptAndType(String expect, String lotteryType){
    	return orderAmountMapper.deleteByExceptAndType(expect, lotteryType);
    }
}
