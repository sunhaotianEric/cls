package com.team.lottery.extvo;

import java.util.Date;

public class AdminOperateLogQuery {

	private String bizSystem;

    private Integer adminId;

    private String adminName;

    private Integer userId;

    private String userName;

    private String modelName;
    
    private Date logtimeStart;
    
    private Date logtimeEnd;

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}

	public Integer getAdminId() {
		return adminId;
	}

	public void setAdminId(Integer adminId) {
		this.adminId = adminId;
	}

	public String getAdminName() {
		return adminName;
	}

	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public Date getLogtimeStart() {
		return logtimeStart;
	}

	public void setLogtimeStart(Date logtimeStart) {
		this.logtimeStart = logtimeStart;
	}

	public Date getLogtimeEnd() {
		return logtimeEnd;
	}

	public void setLogtimeEnd(Date logtimeEnd) {
		this.logtimeEnd = logtimeEnd;
	}
    
}
