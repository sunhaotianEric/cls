package com.team.lottery.extvo;

import java.util.Date;

/**
 * 后台管理员密码错误查询对象.
 * 
 * @author jamine
 *
 */
public class AdminPasswordErrorLogQuery {
	// 系统.
	private String bizSystem;
	// 管理员ID.
	private Integer adminId;
	// 管理员名称.
	private String adminName;
	// 错误类型(PASSWORD_ERROR,OPERATE_PASSWORD_ERROR).登陆密码错误,操作密码错误.
	private String errorType;
	// 错误来源.
	private String sourceType;
	// 操作IP地址.
	private String loginIp;
	// 起始时间
	private Date createdDateStart;
	// 结束时间
	private Date createdDateEnd;
	// 是否删除(0是未删除,1是已删除).
	private Integer isDeleted;

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}

	public Integer getAdminId() {
		return adminId;
	}

	public void setAdminId(Integer adminId) {
		this.adminId = adminId;
	}

	public String getAdminName() {
		return adminName;
	}

	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}

	public String getErrorType() {
		return errorType;
	}

	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}

	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	public String getLoginIp() {
		return loginIp;
	}

	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}

	public Date getCreatedDateStart() {
		return createdDateStart;
	}

	public void setCreatedDateStart(Date createdDateStart) {
		this.createdDateStart = createdDateStart;
	}

	public Date getCreatedDateEnd() {
		return createdDateEnd;
	}

	public void setCreatedDateEnd(Date createdDateEnd) {
		this.createdDateEnd = createdDateEnd;
	}

	public Integer getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}
}
