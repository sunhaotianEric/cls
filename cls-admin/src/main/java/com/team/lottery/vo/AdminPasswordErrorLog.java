package com.team.lottery.vo;

import java.util.Date;

public class AdminPasswordErrorLog {
	// 唯一ID.
    private Long id;
    // 系统.
    private String bizSystem;
    // 管理员ID.
    private Integer adminId;
    // 管理员用户名.
    private String adminName;
    // 错误类型.
    private String errorType;
    // 错误来源.
    private String sourceType;
    // 操作IP
    private String loginIp;
    // 错误描述.
    private String sourceDes;
    // 创建日期.
    private Date createdDate;
    // 更新时间.
    private Date updateDate;
    // 是否删除(0是未删除,1是已经删除.)
    private Integer isDeleted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public Integer getAdminId() {
        return adminId;
    }

    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName == null ? null : adminName.trim();
    }

    public String getErrorType() {
        return errorType;
    }

    public void setErrorType(String errorType) {
        this.errorType = errorType == null ? null : errorType.trim();
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType == null ? null : sourceType.trim();
    }

    public String getLoginIp() {
        return loginIp;
    }

    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp == null ? null : loginIp.trim();
    }

    public String getSourceDes() {
        return sourceDes;
    }

    public void setSourceDes(String sourceDes) {
        this.sourceDes = sourceDes == null ? null : sourceDes.trim();
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }
}