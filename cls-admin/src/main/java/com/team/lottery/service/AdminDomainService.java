package com.team.lottery.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.extvo.AdminDomainQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.admindomain.AdminDomainMapper;
import com.team.lottery.vo.AdminDomain;

@Service
public class AdminDomainService {
	@Autowired
	public AdminDomainMapper adminDomainMapper;
	
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int deleteByPrimaryKey(Integer id){
		return adminDomainMapper.deleteByPrimaryKey(id);
	}

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int insert(AdminDomain record){
		return adminDomainMapper.insert(record);
	}

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int insertSelective(AdminDomain record){
		return adminDomainMapper.insertSelective(record);
	}

	public AdminDomain selectByPrimaryKey(Integer id){
		return adminDomainMapper.selectByPrimaryKey(id);
	}

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int updateByPrimaryKeySelective(AdminDomain record){
		return adminDomainMapper.updateByPrimaryKeySelective(record);
	}

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int updateByPrimaryKeyWithBLOBs(AdminDomain record){
		return adminDomainMapper.updateByPrimaryKeyWithBLOBs(record);
	}

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int updateByPrimaryKey(AdminDomain record){
		return adminDomainMapper.updateByPrimaryKey(record);
		
	}
    
    
    /**
     * 根据子系统和域名,ip查询
     * @param bizSystem
     * @param domains
     * @return
     */
	public AdminDomain getAdminDomainByBizSystem(AdminDomainQuery adminDomainQuery){
		return adminDomainMapper.getAdminDomainByBizSystem(adminDomainQuery);
	}
    /**
     * 查询所有的
     * @param bizSystem
     * @param domains
     * @return
     */
	public List<AdminDomain> getAllAdminDomain(AdminDomainQuery adminDomainQuery){
		return adminDomainMapper.getAllAdminDomain(adminDomainQuery);
	};
	/**
	 * 分页查询域名IP授权管理 
	 * @param bizSystem
	 * @param domains
	 * @param page
	 * @return
	 */
	public Page getAllAdminDomainPage(AdminDomainQuery adminDomainQuery,Page page){
		page.setTotalRecNum(adminDomainMapper.getAllAdminDomainPageCount(adminDomainQuery));
		page.setPageContent(adminDomainMapper.getAllAdminDomainPage(adminDomainQuery, page.getStartIndex(),page.getPageSize()));
		return page;
	}
	
	
	
}
