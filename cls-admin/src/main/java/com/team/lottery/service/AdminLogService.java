package com.team.lottery.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.extvo.AdminLogQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.adminlog.AdminLogMapper;
import com.team.lottery.vo.AdminLog;

@Service("adminLogService")
public class AdminLogService {
	
	@Autowired
	private AdminLogMapper adminLogMapper;
	
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int deleteByPrimaryKey(Integer id){
		return adminLogMapper.deleteByPrimaryKey(id);
		
	}
	
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int insertSelective(AdminLog record){
		record.setLoginTime(new Date());
		return adminLogMapper.insertSelective(record);
		
	}

    public AdminLog selectByPrimaryKey(Integer id){
		return adminLogMapper.selectByPrimaryKey(id);
    	
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(AdminLog record){
		return adminLogMapper.updateByPrimaryKeySelective(record);
    	
    }
    
    /**
	 * 查找所有的登陆日志数据
	 * 
	 * @return
	 */
	public Page getAllAdminLogsByQueryPage(AdminLogQuery query, Page page) {
		page.setTotalRecNum(adminLogMapper.getAllAdminLogByQueryPageCount(query));
		page.setPageContent(adminLogMapper.getAllAdminLogsByQueryPage(query, page.getStartIndex(), page.getPageSize()));
		return page;
	}

}
