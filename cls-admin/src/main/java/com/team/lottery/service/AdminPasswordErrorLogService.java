package com.team.lottery.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.extvo.AdminPasswordErrorLogQuery;
import com.team.lottery.mapper.adminpassworderrorlog.AdminPasswordErrorLogMapper;
import com.team.lottery.vo.AdminPasswordErrorLog;

/**
 * 后台管理员密码错误记录对象Service实现类.
 * 
 * @author jamine
 *
 */
@Service
public class AdminPasswordErrorLogService {

	@Autowired
	public AdminPasswordErrorLogMapper adminPasswordErrorLogMapper;

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int deleteByPrimaryKey(Long id) {
		return adminPasswordErrorLogMapper.deleteByPrimaryKey(id);
	}

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int insert(AdminPasswordErrorLog record) {
		return adminPasswordErrorLogMapper.insert(record);
	}

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int insertSelective(AdminPasswordErrorLog record) {
		return adminPasswordErrorLogMapper.insertSelective(record);
	}

	public AdminPasswordErrorLog selectByPrimaryKey(Long id) {
		return adminPasswordErrorLogMapper.selectByPrimaryKey(id);
	}

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int updateByPrimaryKeySelective(AdminPasswordErrorLog record) {
		return adminPasswordErrorLogMapper.updateByPrimaryKeySelective(record);
	}

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int updateByPrimaryKey(AdminPasswordErrorLog record) {
		return adminPasswordErrorLogMapper.updateByPrimaryKey(record);

	}

	/**
	 * 根据条件查询对应的管理员密码错误日志对象.
	 * 
	 * @param query
	 * @return
	 */
	public int getAllAdminPasswordErrorLogByQueryPageCount(AdminPasswordErrorLogQuery query) {
		return adminPasswordErrorLogMapper.getAllAdminPasswordErrorLogByQueryPageCount(query);
	}

	/**
	 * 根据IP软删除记录.
	 * 
	 * @param superSystem
	 * @param loginIp
	 * @param username
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void updateAdminPasswordErrorLogByErrorType(String superSystem, String loginIp, String username,
			String errorType) {
		adminPasswordErrorLogMapper.updateAdminPasswordErrorLogByErrorType(superSystem, loginIp, username, errorType);
	}
	
	/**
	 * 根据用户名删除密码错误日志记录
	 * @param adminId
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void updateAdminPasswordErrorLogByAdminName(String adminName) {
		adminPasswordErrorLogMapper.updateAdminPasswordErrorLogByAdminName(adminName);
	}
	
	
}
