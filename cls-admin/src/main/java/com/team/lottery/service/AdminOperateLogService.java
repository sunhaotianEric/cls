package com.team.lottery.service;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.enums.EAdminOperateLogModel;
import com.team.lottery.extvo.AdminOperateLogQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.adminoperatelog.AdminOperateLogMapper;
import com.team.lottery.vo.AdminOperateLog;

@Service("adminOperateLogService")
public class AdminOperateLogService {

	private static Logger log = LoggerFactory.getLogger(AdminOperateLogService.class);

	@Autowired
	private AdminOperateLogMapper adminOperateLogMapper;
	
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int deleteByPrimaryKey(Long id){
    	return adminOperateLogMapper.deleteByPrimaryKey(id);
    }

	@Transactional(readOnly = false, rollbackFor = Exception.class)
    public int insert(AdminOperateLog adminOperateLog){
    	return adminOperateLogMapper.insert(adminOperateLog);
    }

	@Transactional(readOnly = false, rollbackFor = Exception.class)
    public int insertSelective(AdminOperateLog adminOperateLog){
    	return adminOperateLogMapper.insertSelective(adminOperateLog);
    }

    public AdminOperateLog selectByPrimaryKey(Long id){
    	return adminOperateLogMapper.selectByPrimaryKey(id);
    }
    
    /**
     * 保存后台操作日志处理
     * @param bizSystem
     * @param adminId
     * @param adminName
     * @param userName
     * @param model
     * @param operateDesc
     */
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void saveAdminOperateLog(String bizSystem, String operationIp, Integer adminId, String adminName, Integer userId, String userName, EAdminOperateLogModel model, String operateDesc){
    	try {
    		AdminOperateLog adminOperateLog = new AdminOperateLog();
    		adminOperateLog.setBizSystem(bizSystem);
    		adminOperateLog.setAdminName(adminName);
    		adminOperateLog.setUserName(userName);
    		adminOperateLog.setModelName(model.getName());
    		adminOperateLog.setOperateDesc(operateDesc);
    		adminOperateLog.setAdminId(adminId);
    		adminOperateLog.setUserId(userId);
    		adminOperateLog.setOperationIp(operationIp);
    		adminOperateLog.setCreateDate(new Date());
	        log.info("业务系统[{}],操作人[{}],被操作人[{}],操作模块[{}],操作内容[{}]", bizSystem, adminName, userName, model.getDescription(), operateDesc);
	        adminOperateLogMapper.insertSelective(adminOperateLog);
    	} catch (Exception e) {
			//保存日志出现异常 不进行处理
			log.error("保存添加操作日志出错,sourceName:{},targetName:{},operateDesc:{}", adminName, userName, operateDesc, e);
		}
    }
	
    /**
     * 查找所有的登陆后台日志数据
     * @return
     */
    public Page getAllAdminOperateLogByQueryPage(AdminOperateLogQuery query,Page page){
		page.setTotalRecNum(adminOperateLogMapper.getAllAdminOperateLogQueryPageCount(query));
		List<AdminOperateLog> adminOperateLogQueryPage = adminOperateLogMapper.getAllAdminOperateLogQueryPage(query,page.getStartIndex(),page.getPageSize());
		for(AdminOperateLog adminOperateLog : adminOperateLogQueryPage){
			String modelName = adminOperateLog.getModelName();
			EAdminOperateLogModel eRecordLogModel = EAdminOperateLogModel.valueOf(modelName);
			adminOperateLog.setModelName(eRecordLogModel.getDescription());
		}
		page.setPageContent(adminOperateLogQueryPage);
		
    	return page;
    }
}
