package com.team.lottery.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.enums.EAdminPasswordErrorLog;
import com.team.lottery.enums.EAdminPasswordErrorType;
import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.admin.AdminMapper;
import com.team.lottery.system.SingleLoginForRedis;
import com.team.lottery.system.SystemPropeties;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.MD5PasswordUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.AdminPasswordErrorLog;

@Service("adminService")
public class AdminService {

	private static Logger log = LoggerFactory.getLogger(AdminService.class);

	@Autowired
	private AdminMapper adminMapper;

	@Autowired
	private AdminPasswordErrorLogService adminPasswordErrorLogService;

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int deleteByPrimaryKey(Integer id) {
		return adminMapper.deleteByPrimaryKey(id);
	}

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int insert(Admin record) {
		return adminMapper.insert(record);
	}

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int insertSelective(Admin record) {
		record.setPassword(MD5PasswordUtil.GetMD5Code(record.getPassword()));
		if(StringUtils.isNotEmpty(record.getOperatePassword())){
			record.setOperatePassword(MD5PasswordUtil.GetMD5Code(record.getOperatePassword()));
		}
		record.setCreateDate(new Date());
		return adminMapper.insertSelective(record);
	}

	public Admin selectByPrimaryKey(Integer id) {
		return adminMapper.selectByPrimaryKey(id);
	}

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int updateByPrimaryKeySelective(Admin record) {
		return adminMapper.updateByPrimaryKeySelective(record);
	}

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int updateByPrimaryKey(Admin record) {
		return adminMapper.updateByPrimaryKey(record);
	}

	/**
	 * 登录校验
	 * 
	 * @return
	 */
	public boolean loginCheck(Admin admin, String loginIp) {
		admin.setPassword(MD5PasswordUtil.GetMD5Code(admin.getPassword()));
		List<Admin> admins = adminMapper.getAdminByCondition(admin);

		if (admins == null || admins.size() == 0) {
			// 错误登陆密码记录
			AdminPasswordErrorLog adminPasswordErrorLog = new AdminPasswordErrorLog();
			adminPasswordErrorLog.setAdminId(0);
			adminPasswordErrorLog.setAdminName(admin.getUsername());
			adminPasswordErrorLog.setBizSystem(ConstantUtil.SUPER_SYSTEM);
			adminPasswordErrorLog.setLoginIp(loginIp);
			adminPasswordErrorLog.setErrorType(EAdminPasswordErrorType.PASSWORD_ERROR.getCode());
			adminPasswordErrorLog.setSourceType(EAdminPasswordErrorLog.ADMIN_LOGIN_PWD_ERROR.getCode());
			adminPasswordErrorLog.setSourceDes(EAdminPasswordErrorLog.ADMIN_LOGIN_PWD_ERROR.getDescription());
			adminPasswordErrorLog.setCreatedDate(new Date());
			adminPasswordErrorLog.setIsDeleted(0);
			adminPasswordErrorLogService.insertSelective(adminPasswordErrorLog);

			return false;
		} else if (admins.size() > 1) {
			return false;
		}
		Admin dbAdmin = admins.get(0);
		log.info("管理员[{}]，业务系统[{}]登陆成功", dbAdmin.getUsername(), dbAdmin.getBizSystem());
		return true;
	}

	/**
	 * 查找所有的管理员数据
	 * 
	 * @return
	 */
	public List<Admin> getAllAdmins() {
		return adminMapper.getAllAdmins();
	}

	/**
	 * 根据账号查找管理员
	 * 
	 * @return
	 */
	public List<Admin> getAllAdminByUsername(Admin admin) {
		return adminMapper.getAdminByUsername(admin);
	}

	/**
	 * 根据条件查找管理员用户
	 * 
	 * @param admin
	 * @return
	 */
	public List<Admin> getAdminByCondition(Admin admin) {
		return adminMapper.getAdminByCondition(admin);
	}

	/**
	 * 分页查找所有的管理员用户
	 * 
	 * @return
	 */
	public Page getAllAdminByQueryPage(Admin query, Page page) {
		page.setTotalRecNum(adminMapper.getAllAdminByQueryPageCount(query));
		List<Admin> admins = adminMapper.getAllAdminByQueryPage(query, page.getStartIndex(), page.getPageSize());
		List<Admin> adminList = new ArrayList<Admin>();
		for (Admin admin : admins) {
			Set<String> sets = SingleLoginForRedis.getRedisSets(SystemPropeties.loginType + "." + admin.getBizSystem());
			if (query.getIsOnline() != null) {
				if (query.getIsOnline().equals(1)) {
					if (sets.contains(admin.getUsername())) {
						admin.setIsOnline(1);
						adminList.add(admin);
					}
				} else {
					if (!sets.contains(admin.getUsername())) {
						adminList.add(admin);
					}
				}
			} else {
				adminList.add(admin);
				if (sets.contains(admin.getUsername())) {
					admin.setIsOnline(1);
				}

			}

		}
		page.setPageContent(adminList);
		return page;
	}

	/**
	 * 查询该系统的超级管理员个数
	 * 
	 * @param query
	 * @return
	 */
	public Integer getSuperManagerCountByBizSystem(Admin query) {
		return adminMapper.getSuperManagerCountByBizSystem(query);
	}

	/**
	 * 根据业务系统和角色查询管理员
	 * 
	 * @param query
	 * @return
	 */
	public List<Admin> getAllAdminByQuery(Admin query) {
		return adminMapper.getAllAdminByQuery(query);

	}

	/**
	 * 根据业务系统查询管理员
	 * 
	 * @param query
	 * @return
	 */
	public List<Admin> getAllAdminByBizSystem(Admin query) {
		return adminMapper.getAllAdminByBizSystem(query);

	}
}
