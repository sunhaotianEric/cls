package com.team.lottery.mapper.adminpassworderrorlog;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.AdminPasswordErrorLogQuery;
import com.team.lottery.vo.AdminPasswordErrorLog;

/**
 * 后台管理员密码错误记录对象Mapper接口.
 * 
 * @author jamine
 *
 */
public interface AdminPasswordErrorLogMapper {

	int deleteByPrimaryKey(Long id);

	int insert(AdminPasswordErrorLog record);

	int insertSelective(AdminPasswordErrorLog record);

	AdminPasswordErrorLog selectByPrimaryKey(Long id);

	int updateByPrimaryKeySelective(AdminPasswordErrorLog record);

	int updateByPrimaryKey(AdminPasswordErrorLog record);

	/**
	 * 根据条件查询对应的后台管理员密码错误日志.
	 * 
	 * @param query
	 * @return
	 */
	int getAllAdminPasswordErrorLogByQueryPageCount(@Param("query") AdminPasswordErrorLogQuery query);

	/**
	 * 软删除密码错误日志.
	 * @param superSystem
	 * @param loginIp
	 * @param username
	 */
	void updateAdminPasswordErrorLogByErrorType(@Param("bizSystem")String bizSystem, @Param("loginIp")String loginIp, @Param("adminName")String adminName , @Param("errorType")String errorType);
	
	/**
	 * 根据管理员名称删除管理员错误密码错误日志.
	 * @param adminId
	 */
	void updateAdminPasswordErrorLogByAdminName(@Param("adminName")String adminName);
	
}