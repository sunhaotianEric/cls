package com.team.lottery.mapper.adminoperatelog;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.AdminOperateLogQuery;
import com.team.lottery.vo.AdminOperateLog;

public interface AdminOperateLogMapper {
    int deleteByPrimaryKey(Long id);

    int insert(AdminOperateLog record);

    int insertSelective(AdminOperateLog record);

    AdminOperateLog selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(AdminOperateLog record);

    int updateByPrimaryKeyWithBLOBs(AdminOperateLog record);

    int updateByPrimaryKey(AdminOperateLog record);
    
    /**
     * 查询符合条件的记录条数
     * @param query
     * @return
     */
    public Integer getAllAdminOperateLogQueryPageCount(AdminOperateLogQuery query);
    
    /**
     * 分页条件查询登陆日志
     * @return
     */
    public List<AdminOperateLog> getAllAdminOperateLogQueryPage(@Param("query")AdminOperateLogQuery query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
}