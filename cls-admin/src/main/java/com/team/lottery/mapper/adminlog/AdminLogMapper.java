package com.team.lottery.mapper.adminlog;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.AdminLogQuery;
import com.team.lottery.vo.AdminLog;

public interface AdminLogMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(AdminLog record);

    int insertSelective(AdminLog record);

    AdminLog selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(AdminLog record);

    int updateByPrimaryKey(AdminLog record);
    
    /**
     * 查询符合条件的记录条数
     * @param query
     * @return
     */
    public Integer getAllAdminLogByQueryPageCount(AdminLogQuery query);
    
    /**
     * 分页条件查询登陆日志
     * @return
     */
	List<AdminLog> getAllAdminLogsByQueryPage(@Param("query")AdminLogQuery query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
}