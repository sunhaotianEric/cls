package com.team.lottery.vo;

import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.util.DateUtils;
/**
 * 用户反馈vo
 * @author Owner
 *
 */
public class FeedBack {
    private Integer id;

    private String bizSystem;

    private Integer dealstate;

    private Integer userId;

    private String userName;

    private Date createTime;

    private Date upeateTime;

    private String updateAdmin;

    private String content;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public Integer getDealstate() {
        return dealstate;
    }

    public void setDealstate(Integer dealstate) {
        this.dealstate = dealstate;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpeateTime() {
        return upeateTime;
    }

    public void setUpeateTime(Date upeateTime) {
        this.upeateTime = upeateTime;
    }

    public String getUpdateAdmin() {
        return updateAdmin;
    }

    public void setUpdateAdmin(String updateAdmin) {
        this.updateAdmin = updateAdmin == null ? null : updateAdmin.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }
    
    //创建时间
    public String getCreateTimeStr(){
    	return DateUtils.getDateToStrByFormat(this.getCreateTime(), "yyyy/MM/dd HH:mm:ss");
    }
    //修改时间
    public String getUpdateTimeStr(){
    	return DateUtils.getDateToStrByFormat(this.getUpeateTime(), "yyyy/MM/dd HH:mm:ss");
    }
	//业务系统
	public String getBizSystemName() {
	        return this.bizSystem==null?"":ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
	}
}