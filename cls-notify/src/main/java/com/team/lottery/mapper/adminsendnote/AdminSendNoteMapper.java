package com.team.lottery.mapper.adminsendnote;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.AdminSendNote;

public interface AdminSendNoteMapper {
    int deleteByPrimaryKey(Long id);

    int insert(AdminSendNote record);

    int insertSelective(AdminSendNote record);

    AdminSendNote selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(AdminSendNote record);

    int updateByPrimaryKeyWithBLOBs(AdminSendNote record);

    int updateByPrimaryKey(AdminSendNote record);
    
    List<AdminSendNote> getAdminSendNoteByfromAdminid(@Param("query")AdminSendNote record,@Param("startNum")int startIndex, @Param("pageNum")Integer pageSize);
    
    int getAdminSendNoteByfromAdminidCount(@Param("query")AdminSendNote record);
}