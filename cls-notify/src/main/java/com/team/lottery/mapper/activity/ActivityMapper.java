package com.team.lottery.mapper.activity;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.Activity;

public interface ActivityMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Activity record);

    int insertSelective(Activity record);

    Activity selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Activity record);

    int updateByPrimaryKey(Activity record);

	List<Activity> getAllActivitys(@Param("status")Integer status,@Param("bizSystem")String bizSystem);
	
	List<Activity> getAllActivitysByBizSystem(String bizSystem);
	
	Integer getActivityByQueryPageCount(@Param("query")Activity query); 
	
 	List<Activity> getActivitysByQueryPage(@Param("query")Activity query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);

 	/**
 	 * 查询签到活动对象.
 	 * @return
 	 */
	Activity getSignConfigActivity(@Param("query")Activity query); 
 	
}