package com.team.lottery.mapper.announce;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.Announce;

public interface AnnounceMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Announce record);

    int insertSelective(Announce record);

    Announce selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Announce record);

    int updateByPrimaryKeyWithBLOBs(Announce record);

    
    /**
     * 查找所有的网站公告
     * @return
     */
    public List<Announce> getAllAnnounces();

    /**
     * 加载首页的几条公告数据
     * @return
     */
	public List<Announce> getIndexAnnounce(@Param("bizSystem")String bizSystem,@Param("size")Integer size);

	/**
	 * 根据条件查询公告
	 * @return
	 */
	public Integer getAllAnnouncesPageCount(@Param("query")Announce query);

	List<Announce> getAllAnnouncesPage(@Param("query")Announce query,@Param("startNum")int startIndex, @Param("pageNum")Integer pageSize);

	/**
	 * 获取首页展示的最新网站公告
	 * @param query
	 * @return
	 */
	public Announce getNewAnnounces(@Param("bizSystem")String bizSystem);
	
}