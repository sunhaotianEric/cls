package com.team.lottery.mapper.homepic;


import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.HomePic;

public interface HomePicMapper {
    
    int deleteByPrimaryKey(Long id);

    int insert(HomePic record);

    int insertSelective(HomePic record);

    HomePic selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(HomePic record);

    int updateByPrimaryKey(HomePic record);
    
    int getHomePicByQueryPageCount(@Param("query")HomePic query);
    
    List<HomePic> getHomePicByQueryPage(@Param("query")HomePic query,@Param("startNum")Integer startIndex,@Param("pageNum")Integer pageSize);
    
    List<HomePic> getAllHomePic();
    
    List<HomePic> getAllHomePicByBizSystem(String bizSystem);
}