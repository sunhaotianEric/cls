package com.team.lottery.mapper.helps;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.Helps;

public interface HelpsMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Helps record);

    int insertSelective(Helps record);

    Helps selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Helps record);

    int updateByPrimaryKeyWithBLOBs(Helps record);

    int updateByPrimaryKey(Helps record);
    
    /**
     * 分页查询
     * @param query
     * @param startIndex
     * @param pageSize
     * @return
     */
    public List<Helps> getAllHelpsPage(@Param("query")Helps query, @Param("startNum")Integer startIndex, @Param("pageNum")Integer pageSize);
	
    /**
     * 查询条数
     * @param query
     * @return
     */
	public Integer getAllHelpsPageCount(@Param("query")Helps query);
	

	/**
	 * 查找所有的帮助中心数据
	 * @param bizSystem
	 * @param startIndex
	 * @param pageSize
	 * @return
	 */
	public List<Helps> getAllHelpsByQuery(@Param("bizSystem")String bizSystem);
	
	/**
	 * 根据业务系统查找帮助信息
	 * @param bizSystem
	 * @return
	 */
	public List<Helps> getHelpsByBizSystem(@Param("bizSystem")String bizSystem);

}