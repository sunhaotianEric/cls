package com.team.lottery.mapper.notes;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.NotesQuery;
import com.team.lottery.vo.Notes;

public interface NotesMapper {
    int deleteByPrimaryKey(Long noteid);

    int insert(Notes record);

    int insertSelective(Notes record);

    Notes selectByPrimaryKey(Long noteid);

    int updateByPrimaryKeySelective(Notes record);

    int updateByPrimaryKeyWithBLOBs(Notes record);

    int updateByPrimaryKey(Notes record);
    
    /**
     * 条件查下符合条件的所有记录
     */
    public List<Notes> getAllNotes(@Param("query")NotesQuery query);
    
    /**
     * 分页查询我的收件箱记录条数
     * @param query
     * @return
     */
    public Integer getToMeAllNotesPageCount(@Param("query")NotesQuery query);

    /**
     * 分页查询我的收件箱记录
     * @param query
     * @param startIndex
     * @param pageSize
     * @return
     */
	List<Notes> getToMeAllNotesPage(@Param("query")NotesQuery query, @Param("startNum")int startIndex,
			@Param("pageNum")Integer pageSize);
    
	/**
	 * 分页条件查下所有符合条件的记录数
	 * @param query
	 * @return
	 */
	public Integer getAllNotesByQueryPageCount(@Param("query")NotesQuery query);

	/**
	 * 分页条件查下所有符合条件的记录
	 * @param query
	 * @param startIndex
	 * @param pageSize
	 * @return
	 */
	List<Notes> getAllNotesByQueryPage(@Param("query")NotesQuery query, @Param("startNum")int startIndex,
			@Param("pageNum")Integer pageSize);
	
	/**
	 * 将消息记录标记为已读
	 */
	public void updateToUserNameForYetRead(@Param("query")NotesQuery query);
	
	/**
	 * 删除该会话
	 * @param query
	 */
	public void updateToUserNameForDelete(@Param("query")NotesQuery query);
	
	
	/**
	 * 查询消息记录的列表数据
	 * @param noteId
	 * @return
	 */
	public List<Notes> getNoteDetailList(Long noteId);
}