package com.team.lottery.mapper.feedback;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.FeedBack;

public interface FeedBackMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(FeedBack record);

    int insertSelective(FeedBack record);

    FeedBack selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(FeedBack record);

    int updateByPrimaryKeyWithBLOBs(FeedBack record);

    int updateByPrimaryKey(FeedBack record);
    
    /**
     * 分页条件
     * @param query
     * @param startNum
     * @param pageNum
     * @return
     */
    public List<FeedBack> getAllFeedBackPage(@Param("query")FeedBack query, @Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
    
    /**
     * 查询所有
     * @param query
     * @return
     */
	public int  getFeedBackCount(@Param("query")FeedBack query);
    
//    /**
//     * 查找所有的反馈信息
//     * @param bizSystem
//     * @return
//     */
//    public List<FeedBack> getAllFeedBackByQuery(@Param("bizSystem")String bizSystem);
}