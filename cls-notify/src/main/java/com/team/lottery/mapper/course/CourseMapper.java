package com.team.lottery.mapper.course;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.Course;

public interface CourseMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Course record);

    int insertSelective(Course record);

    Course selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Course record);

    int updateByPrimaryKeyWithBLOBs(Course record);

    int updateByPrimaryKey(Course record);
    
    /**
     * 分页条件
     * @param query
     * @param startNum
     * @param pageNum
     * @return
     */
    public List<Course> getAllCoursePage(@Param("query")Course query, @Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
    
    /**
     * 查询所有
     * @param query
     * @return
     */
	public int  getCourseCount(@Param("query")Course query);
	
    public Course getCourseById(Integer id);
}