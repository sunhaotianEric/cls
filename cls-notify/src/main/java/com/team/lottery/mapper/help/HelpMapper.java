package com.team.lottery.mapper.help;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.Help;

public interface HelpMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Help record);

    int insertSelective(Help record);

    Help selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Help record);

    int updateByPrimaryKeyWithBLOBs(Help record);

    int updateByPrimaryKey(Help record);

	public List<Help> getAllHelps(@Param("bizSystem")String bizSystem);

	
	List<Help> getAllHelpsPage(@Param("query")Help query, @Param("startNum")Integer startIndex, @Param("pageNum")Integer pageSize);
	
	public Integer getAllHelpsPageCount(@Param("query")Help query);
	
	
	public Integer getAllHelpsByQueryPageCount(@Param("bizSystem")String bizSystem, @Param("type")String type);

	List<Help> getAllHelpsByQueryPage(@Param("bizSystem")String bizSystem, @Param("type")String type, @Param("startNum")Integer startIndex, @Param("pageNum")Integer pageSize);
	
	public Integer getAllHelpsByQueryPageCountCJWT(@Param("bizSystem")String bizSystem);
	
	List<Help> getAllHelpsByQueryPageCJWT(@Param("bizSystem")String bizSystem, @Param("startNum")Integer startIndex, @Param("pageNum")Integer pageSize);
}