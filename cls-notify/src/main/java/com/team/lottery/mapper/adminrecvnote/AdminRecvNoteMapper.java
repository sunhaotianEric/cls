package com.team.lottery.mapper.adminrecvnote;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.AdminRecvNote;

public interface AdminRecvNoteMapper {
    int deleteByPrimaryKey(Long id);

    int insert(AdminRecvNote record);

    int insertSelective(AdminRecvNote record);

    AdminRecvNote selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(AdminRecvNote record);

    int updateByPrimaryKeyWithBLOBs(AdminRecvNote record);

    int updateByPrimaryKey(AdminRecvNote record);
    
    List<AdminRecvNote> getAdminRecvNoteBytomainid(@Param("query")AdminRecvNote record,@Param("startNum")int startIndex, @Param("pageNum")Integer pageSize);
    
    int getAdminRecvNoteBytomainidCount(@Param("query")AdminRecvNote record);
    
    List<AdminRecvNote> getAllAdminRecvNoteBytomainid(@Param("query")AdminRecvNote record);
    
}