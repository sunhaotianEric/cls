package com.team.lottery.system.message;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.Jedis;

import com.team.lottery.redis.JedisUtils;
import com.team.lottery.service.HomePicService;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.system.messagetype.HomePicturesMessage;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.vo.HomePic;
import com.team.lottery.vo.Login;


public class HomePictusHandler extends BaseMessageHandler {
	
	private static final long serialVersionUID = 1L;
	private static Logger logger = LoggerFactory.getLogger(HomePictusHandler.class);
	
	@Override
	public void dealMessage(BaseMessage message) {
		logger.info("接收到通知，开始刷新首页图片redis缓存数据...");
		Jedis jedis = null;
		try {
			HomePicturesMessage homePicMessage = (HomePicturesMessage)message;
			String bizSystem = homePicMessage.getBizSystem();
			
			//加载启用的首页图片
			HomePicService homePicService = ApplicationContextUtil.getBean("homePicService");
			List<HomePic> homePicCores = homePicService.getAllHomePicByBizSystem(bizSystem);
			jedis = JedisUtils.getResource();
			
			//删除所有首页的图片
			String pcKey = bizSystem + ".index.pc";
			String mobileKey = bizSystem + ".index.mobile";
			jedis.del(pcKey);
			jedis.del(mobileKey);
			
			//首页的图片存入redis
			for(HomePic h : homePicCores){
				 pcKey = h.getBizSystem()+".index.pc";
				 mobileKey = h.getBizSystem() + ".index.mobile";
				String pcValue = "";
				String mobileValue = "";
				if(StringUtils.isNotBlank(h.getHomePicLinkUrl())){
					//图片地址 图片域名+图片路径
					if(h.getPicType().equals(Login.LOGTYPE_PC)){
						pcValue = SystemConfigConstant.imgServerUrl + h.getHomePicUrl()+"|"+h.getHomePicLinkUrl();
					}else{
						mobileValue = SystemConfigConstant.imgServerUrl + h.getHomePicUrl()+"|"+h.getHomePicLinkUrl();
					}
				}else{
					if(h.getPicType().equals(Login.LOGTYPE_PC)){
						pcValue = SystemConfigConstant.imgServerUrl + h.getHomePicUrl();
					}else{
						mobileValue = SystemConfigConstant.imgServerUrl + h.getHomePicUrl();
					}
				}
				
				if(pcKey != null && pcValue != null && pcValue != ""){
					jedis.rpush(pcKey, pcValue);
				}
				
				if(mobileKey!= null && mobileValue != null && mobileValue != ""){
					jedis.rpush(mobileKey, mobileValue);
				}
				
			}
			
		} catch (Exception e) {
    		logger.error("向redis写入数据失败", e);
    		JedisUtils.returnBrokenResource(jedis);
    	} finally{
    		JedisUtils.returnResource(jedis);
    	}
		
		logger.info("刷新首页图片redis缓存数据结束...");
	}


}
