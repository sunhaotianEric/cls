package com.team.lottery.system.messagetype;

import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.HomePictusHandler;

public class HomePicturesMessage extends BaseMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String homePicUrl;
	private String homePicLinkUrl;
	private String bizSystem;
	
	public HomePicturesMessage(){
		this.handler = new HomePictusHandler();
	}

	public String getHomePicUrl() {
		return homePicUrl;
	}

	public void setHomePicUrl(String homePicUrl) {
		this.homePicUrl = homePicUrl;
	}

	public String getHomePicLinkUrl() {
		return homePicLinkUrl;
	}

	public void setHomePicLinkUrl(String homePicLinkUrl) {
		this.homePicLinkUrl = homePicLinkUrl;
	}

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}
