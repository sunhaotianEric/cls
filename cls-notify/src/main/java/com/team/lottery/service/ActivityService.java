package com.team.lottery.service;


import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.activity.ActivityMapper;
import com.team.lottery.vo.Activity;


@Service("activityService")
public class ActivityService {
	
	private static Logger logger = LoggerFactory.getLogger(ActivityService.class);
	
	@Autowired
	private ActivityMapper activityMapper;
	
	
	
    public int deleteByPrimaryKey(Long id){
    	return activityMapper.deleteByPrimaryKey(id);
    }

    public int insert(Activity record){
    	return activityMapper.insert(record);
    }

    public int insertSelective(Activity record){
    	return activityMapper.insertSelective(record);
    }

    public Activity selectByPrimaryKey(Long id){
    	return activityMapper.selectByPrimaryKey(id);
    }
    
    public int updateByPrimaryKeySelective(Activity record){
    	return activityMapper.updateByPrimaryKeySelective(record);
    }

	/**
     * 查找所有前台展示的活动信息
     * @return
     */
	public List<Activity> getAllActivitys(Integer status, String bizSystem) {
		List<Activity> activities = new ArrayList<Activity>();
		try {
			activities = activityMapper.getAllActivitys(status,bizSystem);
		} catch (Exception e) {
			logger.error("查询活动失败", e);
		}
		return activities;
	}
	
	/**
	 * 根据业务系统查找所有启用的活动
	 * @param bizSystem
	 * @return
	 */
	public List<Activity> getAllActivityByBizSystem(String bizSystem){
		return activityMapper.getAllActivitysByBizSystem(bizSystem);
	}

	/**
     * 分页查找所有的活动
     * @return
     */
	public Page getActivitysByQueryPage(Activity query,Page page){
		page.setTotalRecNum(activityMapper.getActivityByQueryPageCount(query));
		page.setPageContent(activityMapper.getActivitysByQueryPage(query, page.getStartIndex(), page.getPageSize()));
		return page;
	}

	/**
	 * 该方法只能单独查询出签到活动对象.
	 * @return
	 */
	public Activity getSignConfigActivity(Activity query) {
		return activityMapper.getSignConfigActivity(query);
	}
}
