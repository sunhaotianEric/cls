package com.team.lottery.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.announce.AnnounceMapper;
import com.team.lottery.vo.Announce;



@Service("announceService")
public class AnnounceService {

	@Autowired
	private AnnounceMapper announceMapper;
	
    public int deleteByPrimaryKey(Integer id){
    	return announceMapper.deleteByPrimaryKey(id);
    }

    public int insertSelective(Announce record){
    	return announceMapper.insertSelective(record);
    }

    public Announce selectByPrimaryKey(Integer id){
    	return announceMapper.selectByPrimaryKey(id);
    }
    
    public int updateByPrimaryKeySelective(Announce record){
    	return announceMapper.updateByPrimaryKeySelective(record);
    }
    
    /**
     * 查找所有的网站公告
     * @return
     */
    public List<Announce> getAllAnnounces(){
    	return announceMapper.getAllAnnounces();
    }
    
    /**
     * 获取首页展示的网站公告
     * @return
     */
    public List<Announce> getIndexAnnounce(String bizSystem,Integer size){
    	return announceMapper.getIndexAnnounce(bizSystem,size);
    }
    
    /**
     * 查找指定的网站公告
     * @return
     */
    public Announce getAnnounceById(Integer id){
    	return announceMapper.selectByPrimaryKey(id);
    }
    
    /**
     * 根据bizsystem分页查找网站公告表
     * @param query
     * @param page
     * @return
     */
    public Page getAllAnnounceQueryPage(Announce query,Page page) {
		page.setTotalRecNum(announceMapper.getAllAnnouncesPageCount(query));
		page.setPageContent(announceMapper.getAllAnnouncesPage(query,page.getStartIndex(),page.getPageSize()));
    	return page;
	}
    
    /**
     * 获取首页展示的网站公告，24小时显示
     * @return
     */
    public Announce getNewAnnounces(String bizSystem){
    	return announceMapper.getNewAnnounces(bizSystem);
    }
    
}
