package com.team.lottery.service;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.enums.ENoteStatus;
import com.team.lottery.enums.ENoteType;
import com.team.lottery.extvo.NoteVo;
import com.team.lottery.extvo.NotesQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.SendNoteQueryVo;
import com.team.lottery.mapper.notes.NotesMapper;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.vo.Notes;
import com.team.lottery.vo.User;


/**
 * 信息服务类
 * @author chenhsh
 *
 */
@Service("notesService")
public class NotesService {
	
	private static Logger logger = LoggerFactory.getLogger(NotesService.class);
	
	@Autowired
	private NotesMapper notesMapper;
	

    public int deleteByPrimaryKey(Long noteid){
    	return notesMapper.deleteByPrimaryKey(noteid);
    }

    public int insertSelective(Notes record){
    	record.setCreatedDate(new Date());
    	return notesMapper.insertSelective(record);
    }

    public Notes selectByPrimaryKey(Long noteid){
    	return notesMapper.selectByPrimaryKey(noteid);
    }

    public int updateByPrimaryKeySelective(Notes record){
    	return notesMapper.updateByPrimaryKeySelective(record);
    }

    public int updateByPrimaryKeyWithBLOBs(Notes record){
    	return notesMapper.updateByPrimaryKeyWithBLOBs(record);
    }
    
    /**
     * 分页查询 我的收件箱
     * @param query
     * @param page
     * @return
     */
    public Page getToMeAllNotes(NotesQuery query, Page page) {
    	page.setTotalRecNum(notesMapper.getToMeAllNotesPageCount(query));
		page.setPageContent(notesMapper.getToMeAllNotesPage(query,page.getStartIndex(),page.getPageSize()));
    	return page;
    }


    /**
     * 查找符合条件的数据
     * @param query
     * @return
     */
	public Integer getAllNotesByQueryPageCount(NotesQuery query){
		return notesMapper.getAllNotesByQueryPageCount(query);
	}
    
    /**
     * 分页查询符合条件的数据
     * @param page 
     * @param query 
     * @return
     */
    public Page getAllNotesPage(NotesQuery query, Page page){
    	page.setTotalRecNum(notesMapper.getAllNotesByQueryPageCount(query));
		page.setPageContent(notesMapper.getAllNotesByQueryPage(query,page.getStartIndex(),page.getPageSize()));
    	return page;
    }
    
    
    /**
     * 发送消息
     * @throws Exception 
     */
	@Transactional(readOnly=false,rollbackFor=Exception.class)
    public void sendNote(NoteVo noteVo,User currentUser,User receiveDaiUser) throws Exception{
		if(noteVo.getSendType().equals("to_up") && receiveDaiUser != null){
			Notes notes = new Notes();
			
			notes.setFromUserId(currentUser.getId());
			notes.setFromUserName(currentUser.getUserName());
			notes.setBizSystem(currentUser.getBizSystem());
			notes.setToUserId(receiveDaiUser.getId());
			notes.setToUserName(receiveDaiUser.getUserName());
			notes.setSub(noteVo.getSub());
			notes.setType(ENoteType.DOWN_UP.name());
            notes.setStatus(ENoteStatus.NO_READ.name());
            notes.setParentId(0l);
            notes.setBody(noteVo.getBody());
            logger.info("业务系统[{}],用户[{}]向用户[{}]编写了站内信,类型[{}]，主题[{}],内容[{}]", 
            		notes.getBizSystem(), notes.getFromUserName(), notes.getToUserName(), notes.getType(), notes.getSub(), notes.getBody());
            this.insertSelective(notes);			
    	}else if(noteVo.getSendType().equals("to_down")){
    		List<String> toUserNames = noteVo.getToUserNames();
    		if(toUserNames == null || toUserNames.size() == 0){
    			throw new Exception("未正确选择要发送的下级人员.");    		
    		}
    		Notes notes = new Notes();
    		notes.setFromUserId(currentUser.getId());
    		notes.setBizSystem(currentUser.getBizSystem());
    		notes.setFromUserName(currentUser.getUserName());
    		notes.setToUserName(receiveDaiUser.getUserName());
    		notes.setToUserId(receiveDaiUser.getId());
    		notes.setSub(noteVo.getSub());
    		notes.setType(ENoteType.UP_DOWN.name());
    		notes.setStatus(ENoteStatus.NO_READ.name());
    		notes.setParentId(0l);
    		notes.setBody(noteVo.getBody());
    		logger.info("业务系统[{}],用户[{}]向用户[{}]编写了站内信,类型[{}]，主题[{}],内容[{}]", 
            		notes.getBizSystem(), notes.getFromUserName(), notes.getToUserName(), notes.getType(), notes.getSub(), notes.getBody());
    		this.insertSelective(notes);	
    	}else{
           throw new Exception("未找到该发送类型.");    		
    	}
    }
    
	
	/**
	 * 将消息记录标记为已读
	 */
	public void updateToUserNameForYetRead(@Param("query")NotesQuery query){
		notesMapper.updateToUserNameForYetRead(query);
	}
	
	
	/**
	 * 删除该会话
	 * @param query
	 */
	public void updateToUserNameForDelete(@Param("query")NotesQuery query){
		notesMapper.updateToUserNameForDelete(query);
	}

	/**
	 * 查询消息记录的列表数据
	 * @param noteId
	 * @return
	 */
	public List<Notes> getNoteDetailList(Long noteId){
		return notesMapper.getNoteDetailList(noteId);
	}
	
	/**
     * 条件查下符合条件的所有记录
     * @param query
     * @return
     */
	public List<Notes> getAllNotes(NotesQuery query){
		return notesMapper.getAllNotes(query);
	}
	
	 /**
     * 向用户列表发送消息
     * @throws Exception 
     */
	@Transactional(readOnly=false,rollbackFor=Exception.class)
    public void sendNoteToUsers(List<User> users, SendNoteQueryVo sendVo) throws Exception{
		for(User u : users) {
			 //发送系统消息
	        Notes notes = new Notes();
	        notes.setParentId(0l);
	        notes.setEnabled(1);
	        notes.setToUserId(u.getId());
	        notes.setToUserName(u.getUserName());
	        notes.setBizSystem(u.getBizSystem());
	        notes.setFromUserName(ConstantUtil.SYSTEM_OPERATE_NAME);
	        notes.setSub(sendVo.getSub());
	        notes.setBody(sendVo.getBody()); 
	        notes.setType(ENoteType.SYSTEM.name()); 
	        notes.setStatus(ENoteStatus.NO_READ.name());
	        notes.setCreatedDate(new Date());
	        this.insertSelective(notes); 
		}
    }

	/**
	 * 查询所有的站内信息
	 * @param query
	 * @param page
	 * @return
	 */
	public Page getAllNotesByQueryPage(NotesQuery query,Page page){
		page.setTotalRecNum(notesMapper.getAllNotesByQueryPageCount(query));
		page.setPageContent(notesMapper.getAllNotesByQueryPage(query,page.getStartIndex(),page.getPageSize()));
		return page;
	}

}
