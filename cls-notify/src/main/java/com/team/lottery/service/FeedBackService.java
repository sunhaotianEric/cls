package com.team.lottery.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.feedback.FeedBackMapper;
import com.team.lottery.vo.FeedBack;

@Service("feedbackService")
public class FeedBackService {
	
	@Autowired
	private FeedBackMapper feedbackMapper;
	
	public int deleteByPrimaryKey(Integer id){
		return feedbackMapper.deleteByPrimaryKey(id);
	}
	
    public int insertSelective(FeedBack record){
		return feedbackMapper.insertSelective(record);
    }

    public FeedBack selectByPrimaryKey(Integer id){
		return feedbackMapper.selectByPrimaryKey(id);
    }

    public int updateByPrimaryKeySelective(FeedBack record){
		return feedbackMapper.updateByPrimaryKeySelective(record);
    }
    
    /**
     * 分页查询
     * @param query
     * @param page
     * @return
     */
    public Page getAllFeedBack(FeedBack query,Page page) {
		page.setTotalRecNum(feedbackMapper.getFeedBackCount(query));
		page.setPageContent(feedbackMapper.getAllFeedBackPage(query, page.getStartIndex(),page.getPageSize()));
		return page ;
	}
    
//    /**
//     * 查找所有的帮助中心数据
//     * @return
//     */
//	public List<FeedBack> getFeedBackByQuery(String bizSystem) {
//		List<FeedBack> feedbackList = feedbackMapper.getAllFeedBackByQuery(bizSystem);
//    	return feedbackList;
//	}
}
