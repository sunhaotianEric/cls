package com.team.lottery.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.adminrecvnote.AdminRecvNoteMapper;
import com.team.lottery.vo.AdminRecvNote;

@Service("adminRecvNoteService")
public class AdminRecvNoteService {

	@Autowired
	private AdminRecvNoteMapper adminRecvNoteMapper;
	
	/**
	 * 收件箱分页查询
	 * @param record
	 * @param page
	 * @return
	 */
	public Page getAdminRecvNoteBytomainid(AdminRecvNote record,Page page){
		page.setTotalRecNum(adminRecvNoteMapper.getAdminRecvNoteBytomainidCount(record));
		page.setPageContent(adminRecvNoteMapper.getAdminRecvNoteBytomainid(record, page.getStartIndex(),page.getPageSize()));
		return page;
	}
	
	/**
	 * 添加收件箱数据
	 * @param record
	 * @return
	 */
	public int insertSelective(AdminRecvNote record){
		record.setCreatedDate(new Date());
		return adminRecvNoteMapper.insertSelective(record);
	};
	
	/**
	 * 根据ID修改收件箱数据
	 * @param record
	 * @return
	 */
	public int updateByPrimaryKeySelective(AdminRecvNote record){
		record.setUpdateDate(new Date());
		return adminRecvNoteMapper.updateByPrimaryKeySelective(record);
	};
	
	/**
	 * 根据ID删除收件箱
	 * @param id
	 * @return
	 */
	public int deleteByPrimaryKey(Long id){
		return adminRecvNoteMapper.deleteByPrimaryKey(id);
	}
	
	/**
	 * 根据ID查询收件箱数据
	 * @param id
	 * @return
	 */
	public AdminRecvNote selectByPrimaryKey(Long id){
		return adminRecvNoteMapper.selectByPrimaryKey(id);
	}
	
	/**
	 * 根据收件人ID查询其所有收件数据(暂时使用收件人ID，后期根据跳转陆续增加)
	 * @param record
	 * @return
	 */
	public List<AdminRecvNote> getAllAdminRecvNoteBytomainid(AdminRecvNote record){
		return adminRecvNoteMapper.getAllAdminRecvNoteBytomainid(record);
	}
	
}
