package com.team.lottery.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.admin.AdminMapper;
import com.team.lottery.mapper.adminsendnote.AdminSendNoteMapper;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.AdminSendNote;

@Service("AdminSendNoteService")
public class AdminSendNoteService {

	@Autowired
	private AdminSendNoteMapper adminSendNoteMapper;
	@Autowired
	private AdminMapper adminMapper;
	
	/**
	 * 分页查询发件数据
	 * @param record
	 * @param page
	 * @return
	 */
	public Page getAdminSendNotePage(AdminSendNote record,Page page){
		page.setTotalRecNum(adminSendNoteMapper.getAdminSendNoteByfromAdminidCount(record));
		page.setPageContent(adminSendNoteMapper.getAdminSendNoteByfromAdminid(record,page.getStartIndex(),page.getPageSize()));
		return page;
	}
	
	/**
	 * 添加发件信息
	 * @param record
	 * @return
	 */
	public int insertSelective(AdminSendNote record){
		record.setCreatedDate(new Date());
		return adminSendNoteMapper.insertSelective(record);
	}
	
	/**
	 * 修改发件信息
	 * @param record
	 * @return
	 */
	public int updateByPrimaryKeySelective(AdminSendNote record){
		AdminSendNote records=this.selectByPrimaryKey(record.getId());
		if(records!=null){
			String ids="";
			if(records.getReadedAdminIds()==null){
				ids=record.getReadedAdminIds()+",";
			}else{
				ids=records.getReadedAdminIds()+record.getReadedAdminIds()+",";
			}
			record.setReadedAdminIds(ids);
			record.setReadedCount(records.getReadedCount()+1);
			record.setUpdateDate(new Date());
			return adminSendNoteMapper.updateByPrimaryKeySelective(record);
		}
		return 0;
	}
	
	/**
	 * 根据ID 查询发件信息
	 * @param id
	 * @return
	 */
	public AdminSendNote selectByPrimaryKey(Long id){
		return adminSendNoteMapper.selectByPrimaryKey(id);
	}
	
	/**
	 * 根据ID删除发件信息
	 * @param id
	 * @return
	 */
	public int deleteByPrimaryKey(Long id){
		return adminSendNoteMapper.deleteByPrimaryKey(id);
	}
	
	/**
	 * 根据业务系统和角色查询管理员
	 * @param admin
	 * @return
	 */
	public List<Admin> getAllAdminByQuery(Admin admin){
    	return adminMapper.getAllAdminByQuery(admin);
    }
}
