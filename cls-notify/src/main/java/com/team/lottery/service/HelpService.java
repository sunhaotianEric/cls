package com.team.lottery.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.help.HelpMapper;
import com.team.lottery.vo.Help;



@Service("helpService")
public class HelpService {

	@Autowired
	private HelpMapper helpMapper;
	
    public int deleteByPrimaryKey(Long id){
    	return helpMapper.deleteByPrimaryKey(id);
    }

    public int insert(Help record){
    	return helpMapper.insert(record);
    }

    public int insertSelective(Help record){
    	return helpMapper.insertSelective(record);
    }

    public Help selectByPrimaryKey(Long id){
    	return helpMapper.selectByPrimaryKey(id);
    }
    
    public int updateByPrimaryKeySelective(Help record){
    	return helpMapper.updateByPrimaryKeySelective(record);
    }
    
    /**
     * 根据业务系统查找帮助信息
     * @param bizSystem
     * @return
     */
    public List<Help> getAllHelpsBybizSystem(String bizSystem){
    	return helpMapper.getAllHelps(bizSystem);
    }
    
    /**
     * 根据查询条件查找所有的帮助信息
     * @return
     */
    public Page getAllHelps(Help query,Page page){
    	page.setTotalRecNum(helpMapper.getAllHelpsPageCount(query));
		page.setPageContent(helpMapper.getAllHelpsPage(query,page.getStartIndex(),page.getPageSize()));
    	return page;
    }

	/**
	 * 查找常见问题的帮助中心数据
	 * @return
	 */
	public Page getHelpsByQueryPageCJWT(String bizSystem, Page page) {
		page.setTotalRecNum(helpMapper.getAllHelpsByQueryPageCountCJWT(bizSystem));
		List<Help> helpList = helpMapper.getAllHelpsByQueryPageCJWT(bizSystem,page.getStartIndex(),page.getPageSize());
		for(Help help : helpList){
			String essentialsDes = help.getEssentialsDes();
			if(essentialsDes.length() > 25){
				String strTmp = essentialsDes.substring(0, 25);
				help.setEssentialsDes(strTmp+"...");
			}
		}
		page.setPageContent(helpList);
		return page;
	}

    /**
     * 查找所有的帮助中心数据
     * @return
     */
	public Page getHelpsByQueryPage(String bizSystem, String type, Page page) {
		page.setTotalRecNum(helpMapper.getAllHelpsByQueryPageCount(bizSystem, type));
		List<Help> helpList = helpMapper.getAllHelpsByQueryPage(bizSystem,type,page.getStartIndex(),page.getPageSize());
		for(Help help : helpList){
			String essentialsDes = help.getEssentialsDes();
			if(essentialsDes.length() > 25){
				String strTmp = essentialsDes.substring(0, 25);
				help.setEssentialsDes(strTmp+"...");
			}
		}
		page.setPageContent(helpList);
    	return page;
	}
	
    /**
     * 获取首页展示的网站公告
     * @return
     *//*
    public List<Announce> getShowAnnounces(){
    	return announceMapper.getShowAnnounces();
    }
    
    *//**
     * 分页查找网站公告表
     * @param query
     * @param page
     * @return
     *//*
    public Page getAllAnnounceWinByQueryPage(Page page) {
		page.setTotalRecNum(announceMapper.getAllAnnouncesByQueryPageCount());
		page.setPageContent(announceMapper.getAllAnnouncesByQueryPage(page.getStartIndex(),page.getPageSize()));
    	return page;
	}
    
    *//**
     * 查找指定的网站公告
     * @return
     *//*
    public Announce getAllAnnounceById(Integer id){
    	return announceMapper.selectByPrimaryKey(id);
    }*/
}
