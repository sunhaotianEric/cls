package com.team.lottery.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.course.CourseMapper;
import com.team.lottery.vo.Course;

@Service("courseService")
public class CourseService {
	
	@Autowired
	private CourseMapper courseMapper;
	
	public int deleteByPrimaryKey(Integer id){
		return courseMapper.deleteByPrimaryKey(id);
	}
	
    public int insertSelective(Course record){
		return courseMapper.insertSelective(record);
    }

    public Course selectByPrimaryKey(Integer id){
		return courseMapper.selectByPrimaryKey(id);
    }

    public int updateByPrimaryKeySelective(Course record){
		return courseMapper.updateByPrimaryKeySelective(record);
    }
    
    /**
     * 分页查询
     * @param query
     * @param page
     * @return
     */
    public Page getAllCourse(Course query,Page page) {
		page.setTotalRecNum(courseMapper.getCourseCount(query));
		page.setPageContent(courseMapper.getAllCoursePage(query, page.getStartIndex(),page.getPageSize()));
		return page ;
	}
    
    /**
     * 查找
     * @return
     */
	public Course getCourseById(Integer id) {
    	return courseMapper.getCourseById(id);
	}
}
