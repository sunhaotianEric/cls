package com.team.lottery.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.helps.HelpsMapper;
import com.team.lottery.vo.Helps;

@Service("helpsService")
public class HelpsService {

	@Autowired
	private HelpsMapper helpsMapper;
	
	public int deleteByPrimaryKey(Long id){
		return helpsMapper.deleteByPrimaryKey(id);
	}
	
    public int insertSelective(Helps record){
		return helpsMapper.insertSelective(record);
    }

    public Helps selectByPrimaryKey(Long id){
		return helpsMapper.selectByPrimaryKey(id);
    }

    public int updateByPrimaryKeySelective(Helps record){
		return helpsMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * 根据查询条件查找所有的帮助信息
     * @return
     */
    public Page getAllHelps(Helps query,Page page){
    	page.setTotalRecNum(helpsMapper.getAllHelpsPageCount(query));
		page.setPageContent(helpsMapper.getAllHelpsPage(query,page.getStartIndex(),page.getPageSize()));
    	return page;
    }

    /**
     * 查找所有的帮助中心数据
     * @return
     */
	public List<Helps> getHelpsByQuery(String bizSystem) {
		List<Helps> helpsList = helpsMapper.getAllHelpsByQuery(bizSystem);
    	return helpsList;
	}
	
	/**
     * 根据业务系统查找帮助信息
     * @param bizSystem
     * @return
     */
    public List<Helps> getHelpsByBizSystem(String bizSystem){
    	return helpsMapper.getHelpsByBizSystem(bizSystem);
    }
    
}
