package com.team.lottery.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.homepic.HomePicMapper;
import com.team.lottery.vo.HomePic;

@Service("homePicService")
public class HomePicService {
	
	@Autowired
	private HomePicMapper homePicMapper;
	
	
   public int deleteByPrimaryKey(Long id){
	  return homePicMapper.deleteByPrimaryKey(id);
   }

   public  int insert(HomePic record){
	   return homePicMapper.insert(record);
   }

   public  int insertSelective(HomePic record){
	   return homePicMapper.insertSelective(record);
   }

   public  HomePic selectByPrimaryKey(Long id){
	   return homePicMapper.selectByPrimaryKey(id);
   }

   public  int updateByPrimaryKeySelective(HomePic record){
	   return homePicMapper.updateByPrimaryKeySelective(record);
   }

   public int updateByPrimaryKey(HomePic record){
	   return homePicMapper.updateByPrimaryKey(record);
   }
	
   public Page getHomePicByQueryPage(HomePic query,Page page){
	   page.setTotalRecNum(homePicMapper.getHomePicByQueryPageCount(query));
	   page.setPageContent(homePicMapper.getHomePicByQueryPage(query, page.getStartIndex(), page.getPageSize()));
	   return page;
   }
   
   public List<HomePic> getAllHomePicByBizSystem(String bizSystem){
	   return homePicMapper.getAllHomePicByBizSystem(bizSystem);
   }
   
   public List<HomePic> getAllHomePicConfig(){
		return homePicMapper.getAllHomePic();
	}
	
}
