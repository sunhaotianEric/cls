package com.team.lottery.dto;

import java.util.ArrayList;
import java.util.List;

import com.team.lottery.vo.Course;

public class CourseDto {

	private String title;
	
	private String content;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	/**
	 * 将Course对象装换为CourseDto对象.
	 * @param course
	 * @return
	 */
	public static CourseDto transToDto(Course course) {
		CourseDto courseDto = null;
		if(course != null){
			courseDto = new CourseDto();
			courseDto.setTitle(course.getTitle());
			courseDto.setContent(course.getContent());
		}
		return courseDto;
	}
     
	/**
	 * 将CourseList对象装换为CourseDtoList对象.
	 * @param courses
	 * @return
	 */
	public static List<CourseDto> transToDtoList(List<Course> courses) {
		List<CourseDto> courseDtos = new ArrayList<CourseDto>();
		for (Course course : courses) {
			courseDtos.add(transToDto(course));
		}
		return courseDtos;
	}
}
