package com.team.lottery.system.job;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.team.lottery.enums.EMoneyDetailType;
import com.team.lottery.enums.EOrderStatus;
import com.team.lottery.enums.EProstateStatus;
import com.team.lottery.extvo.MoneyDetailQuery;
import com.team.lottery.extvo.OrderQuery;
import com.team.lottery.service.MoneyDetailService;
import com.team.lottery.service.OrderService;
import com.team.lottery.util.DateUtil;
import com.team.lottery.vo.MoneyDetail;
import com.team.lottery.vo.Order;

/**
 * @deprecated  无用
 * @author luocheng
 *
 */
@Component("autoMarkMoneyDetailJob")
public class AutoMarkMoneyDetailJob {

	private static Logger log = LoggerFactory.getLogger(AutoMarkMoneyDetailJob.class);
	
	@Autowired
	private MoneyDetailService moneyDetailService;
	@Autowired
	private OrderService orderService;

	
	public void execute() {
		log.info("开始特殊标记未开奖的资金明细记录...");
		Long startLongTime = System.currentTimeMillis();
		Calendar calendar = Calendar.getInstance();
		Date queryOrderDateEnd = calendar.getTime();
		calendar.add(Calendar.MINUTE, -30);
		Date queryOrderDateStart = calendar.getTime();
		log.info("查询未开奖订单开始时间：" + DateUtil.getDate(queryOrderDateStart, "yyyy-MM-dd HH:mm:ss"));
		log.info("查询未开奖订单结束时间：" + DateUtil.getDate(queryOrderDateEnd, "yyyy-MM-dd HH:mm:ss"));
		//查询需要处理未追号的订单
		OrderQuery query = new OrderQuery();
    	query.setProstateStatus(EProstateStatus.DEALING.getCode());
    	query.setOrderStatus(EOrderStatus.GOING.getCode());
    	query.setCreatedDateStart(queryOrderDateStart);
    	query.setCreatedDateEnd(queryOrderDateEnd);
    	List<Order> dealOrders = orderService.getOrderByCondition(query);
    	if(CollectionUtils.isNotEmpty(dealOrders)) {
    		log.info("查询出未开奖的订单数为["+dealOrders.size()+"]");
    		for(Order dealOrder : dealOrders) {
    			MoneyDetailQuery moneyDetailQuery = new MoneyDetailQuery();
    			moneyDetailQuery.setOrderId(dealOrder.getId());
    			moneyDetailQuery.setLotteryType(EMoneyDetailType.LOTTERY.getCode());
    			moneyDetailQuery.setCreatedDateStart(queryOrderDateStart);
    			moneyDetailQuery.setCreatedDateEnd(queryOrderDateEnd);
    			List<MoneyDetail> moneyDetails = moneyDetailService.queryMoneyDetailsByCondition(moneyDetailQuery);
    			if(CollectionUtils.isNotEmpty(moneyDetails)) {
    				for(MoneyDetail md : moneyDetails) {
    					md.setYetBonus(MoneyDetail.NOT_YETBONUS);
    					log.info("更新资金明细标记为未进入分红计算中,id["+md.getId()+"],订单号id["+dealOrder.getId()+"],金额["+md.getPay()+"]");
    					moneyDetailService.updateByPrimaryKeySelective(md);
    				}
    			}
    		}
    	} else {
    		log.info("查询出未开奖的订单数为0");
    	}
    	Long endLongTime = System.currentTimeMillis();
    	log.info("特殊标记未开奖的资金明细记录结束,耗时["+(endLongTime - startLongTime)+"]ms...");
	}
}
