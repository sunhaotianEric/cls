package com.team.lottery.system.job;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.enums.EPlatformChargePayStatus;
import com.team.lottery.enums.EPlatformChargeType;
import com.team.lottery.extvo.DayProfitQuery;
import com.team.lottery.service.BizSystemBonusConfigService;
import com.team.lottery.service.BizSystemService;
import com.team.lottery.service.DayProfitService;
import com.team.lottery.service.PlatformChargeOrderService;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.vo.BizSystem;
import com.team.lottery.vo.BizSystemBonusConfig;
import com.team.lottery.vo.DayProfit;
import com.team.lottery.vo.PlatformChargeOrder;

@Component("platformChargeOrderJob")
public class PlatformChargeOrderJob extends QuartzJob{
	
	private static Logger log = LoggerFactory.getLogger(PlatformChargeOrderJob.class);
	
    
	@Autowired
	private BizSystemService bizSystemService;
	
	@Autowired
	private PlatformChargeOrderService platformChargeOrderService;
	
	@Autowired
	private DayProfitService dayProfitService;
	@Autowired
	private BizSystemBonusConfigService bizSystemBonusConfigService;
	
	@Transactional(readOnly=false,rollbackFor=Exception.class)
	public void execute() {
		try {
			log.info("开始执行月维护分红费用计算");
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
			Calendar calendar = Calendar.getInstance();
			if (nowTime != null) {// 手动是选择的是要统计的当前月
				calendar.setTime(nowTime);
				calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
			} else { // 定时器跑是每月的1号，然后跑的是上个月的数据！
				calendar.set(Calendar.DAY_OF_MONTH, 1);
				calendar.add(Calendar.DATE, -1);
			}
			Date curdate = new Date();
			if (curdate.getMonth() == calendar.get(Calendar.MONTH)) {
				log.info("不能跑当前月的数据！");
				return;
			}

			// 获取查询的开始日期和结束日期
			Calendar queryCalendar = Calendar.getInstance();
			queryCalendar.setTime(calendar.getTime());
			queryCalendar.set(Calendar.DAY_OF_MONTH, 1);
			// 归属时间置为十二点
			queryCalendar.set(Calendar.HOUR_OF_DAY, 12);
			queryCalendar.set(Calendar.MINUTE, 0);
			queryCalendar.set(Calendar.SECOND, 0);
			queryCalendar.set(Calendar.MILLISECOND, 0);
			Date belongStartDate = queryCalendar.getTime();
			queryCalendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
			Date belongEndDate = queryCalendar.getTime();
			String belongDateStr = sdf.format(calendar.getTime());
			List<BizSystem> bizList = new ArrayList<BizSystem>();
			// this.bizSystem有值，单业务系统
			if (this.bizSystem == null || "".equals(this.bizSystem)) {
				// 查询所有业务系统
				bizList = bizSystemService.getAllBizSystem();
			} else {
				BizSystem biz = new BizSystem();
				biz.setBizSystem(this.bizSystem);
				biz = bizSystemService.getBizSystemByCondition(biz);
				bizList.add(biz);
			}

			for (BizSystem bizSystem : bizList) {
				// 超级系统不处理
				if ("SUPER_SYSTEM".equals(bizSystem.getBizSystem())) {
					continue;
				}
				// 停用的系统不生成收费
				if (bizSystem.getEnable() == 0) {
					continue;
				}
				PlatformChargeOrder paChargeOrderQuery = new PlatformChargeOrder();
				paChargeOrderQuery.setBizSystem(bizSystem.getBizSystem());
				paChargeOrderQuery.setBelongDateStr(belongDateStr);
				int result = platformChargeOrderService.getPlatformChargeOrderByQueryPageCount(paChargeOrderQuery);
				if (result > 0) {
					log.info("已经生成订单了不继续生成了！");
					continue;
				}
				
				// 维护费统计收取
				PlatformChargeOrder platformChargeOrder = new PlatformChargeOrder();
				platformChargeOrder.setBizSystem(bizSystem.getBizSystem());
				platformChargeOrder.setUpdateAdmin(ConstantUtil.SYSTEM_OPERATE_NAME);
				platformChargeOrder.setCreateTime(new Date());
				platformChargeOrder.setBelongDate(calendar.getTime());
				platformChargeOrder.setBelongDateStr(belongDateStr);
				platformChargeOrder.setBonusScale(null);
				platformChargeOrder.setGain(null);
				
				BigDecimal totalMaintainMoney = bizSystem.getMaintainMoney();
				platformChargeOrder.setChargeType(EPlatformChargeType.MAINTAIN.getCode());
				platformChargeOrder.setMoney(totalMaintainMoney);
				platformChargeOrder.setPayStatus(EPlatformChargePayStatus.PAYMENT.name());
				platformChargeOrderService.insertSelective(platformChargeOrder);
				
				// 分红费用计算
				SimpleDateFormat sdftemp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				DayProfitQuery dayProfitQuery = new DayProfitQuery();
				dayProfitQuery.setBelongDateStart(belongStartDate);
				dayProfitQuery.setBelongDateEnd(belongEndDate);
				dayProfitQuery.setBizSystem(bizSystem.getBizSystem());
				log.info(bizSystem.getBizSystem() + "平台收费时间:" + sdftemp.format(belongStartDate) + "--"
						+ sdftemp.format(belongEndDate));
				List<DayProfit> daList = dayProfitService.getLotteryGainProfitByBelongDate(dayProfitQuery);
				
				PlatformChargeOrder platformChargeOrderBonus = new PlatformChargeOrder();
				BigDecimal bonusScale = BigDecimal.ZERO;
				
				log.info("查询到的数据为" + daList.size() + "条");
				BigDecimal totalBonusMoney = BigDecimal.ZERO;
				if (daList.size() > 0) {
					totalBonusMoney = daList.get(0).getLotteryGain();
				}
				log.info("查询到盈利为" + totalBonusMoney);
				// 根据盈利 查出分红比例是多少
				BizSystemBonusConfig bizSystemBonusConfig = new BizSystemBonusConfig();
				bizSystemBonusConfig.setBizSystem(bizSystem.getBizSystem());
				List<BizSystemBonusConfig> BizSystemBonusConfigList = bizSystemBonusConfigService
						.getBizSystemBonusConfigByCondition(bizSystemBonusConfig);
				if (BizSystemBonusConfigList.size() <= 0) {
					log.info("查询不到" + bizSystem.getBizSystem() + "分红比例,结束当前系统插入收费订单！");
					continue;
				}
				// 根据规则算出分红比例
				for (BizSystemBonusConfig b : BizSystemBonusConfigList) {
					if (totalBonusMoney.compareTo(new BigDecimal(b.getGain())) <= 0) {
						bonusScale = b.getBonusScale().divide(new BigDecimal("100"));
						break;
					}
				}

				// 分红
				platformChargeOrderBonus.setBizSystem(bizSystem.getBizSystem());
				platformChargeOrderBonus.setUpdateAdmin(ConstantUtil.SYSTEM_OPERATE_NAME);
				platformChargeOrderBonus.setCreateTime(new Date());
				platformChargeOrderBonus.setBelongDate(calendar.getTime());
				platformChargeOrderBonus.setBelongDateStr(belongDateStr);
				platformChargeOrderBonus.setBonusScale(bonusScale);
				platformChargeOrderBonus.setGain(totalBonusMoney);

				// 分红费用统计收取
				if (totalBonusMoney.compareTo(BigDecimal.ZERO) <= 0) {// 没赚钱，不收取分红费用
					platformChargeOrderBonus.setChargeType(EPlatformChargeType.BONUS.getCode());
					platformChargeOrderBonus.setMoney(BigDecimal.ZERO);
					platformChargeOrderBonus.setPayStatus(EPlatformChargePayStatus.PAYMENT_NEEDNOT.name());
				} else {
					platformChargeOrderBonus.setChargeType(EPlatformChargeType.BONUS.getCode());
					platformChargeOrderBonus
							.setMoney(bonusScale.multiply(totalBonusMoney).setScale(0, BigDecimal.ROUND_DOWN));
					platformChargeOrderBonus.setPayStatus(EPlatformChargePayStatus.PAYMENT.name());
				}

				platformChargeOrderService.insertSelective(platformChargeOrderBonus);
			}
			
			//如果全部业务系统统计，则插入一条工资支出和服务器支出的记录
			if (this.bizSystem == null || "".equals(this.bizSystem)) {
				PlatformChargeOrder laborOrder = new PlatformChargeOrder();
				laborOrder.setBizSystem(ConstantUtil.SUPER_SYSTEM);
				laborOrder.setChargeType(EPlatformChargeType.LABOR.getCode());
				laborOrder.setMoney(BigDecimal.ZERO);
				laborOrder.setPayStatus(EPlatformChargePayStatus.PAYMENT.name());
				laborOrder.setBelongDate(calendar.getTime());
				laborOrder.setBelongDateStr(belongDateStr);
				laborOrder.setUpdateAdmin(ConstantUtil.SYSTEM_OPERATE_NAME);
				laborOrder.setCreateTime(new Date());
				platformChargeOrderService.insertSelective(laborOrder);
				
				PlatformChargeOrder equipmentOrder = new PlatformChargeOrder();
				equipmentOrder.setBizSystem(ConstantUtil.SUPER_SYSTEM);
				equipmentOrder.setChargeType(EPlatformChargeType.EQUIPMENT.getCode());
				equipmentOrder.setMoney(BigDecimal.ZERO);
				equipmentOrder.setPayStatus(EPlatformChargePayStatus.PAYMENT.name());
				equipmentOrder.setBelongDate(calendar.getTime());
				equipmentOrder.setBelongDateStr(belongDateStr);
				equipmentOrder.setUpdateAdmin(ConstantUtil.SYSTEM_OPERATE_NAME);
				equipmentOrder.setCreateTime(new Date());
				platformChargeOrderService.insertSelective(equipmentOrder);
			}

			log.info("执行月维护分红费用计算结束");
		} catch (Exception e) {
			log.error("执行月维护分红费用计算错误", e);
			throw new RuntimeException(e.getMessage());
		}
	}

		public static void main(String[] args) {
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM");
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.DAY_OF_MONTH, 1); 
			calendar.add(Calendar.DATE, -1);
			Date curdate=new Date();
			System.out.println(sdf.format(calendar.getTime()));
			
			//获取查询的开始日期和结束日期
			Calendar queryCalendar = Calendar.getInstance();
			queryCalendar.setTime(calendar.getTime());
			queryCalendar.set(Calendar.DAY_OF_MONTH, 1); 
			//归属时间置为十二点
			queryCalendar.set(Calendar.HOUR_OF_DAY, 12);
			queryCalendar.set(Calendar.MINUTE, 0);
			queryCalendar.set(Calendar.SECOND, 0);
			Date belongStartDate= queryCalendar.getTime();
			queryCalendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));  
			Date belongEndDate= queryCalendar.getTime();
			SimpleDateFormat sdftemp=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			System.out.println(sdftemp.format(belongStartDate)+"--"+sdftemp.format(belongEndDate));
		}

	
}
