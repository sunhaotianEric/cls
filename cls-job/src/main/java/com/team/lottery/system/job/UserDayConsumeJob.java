package com.team.lottery.system.job;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.extvo.MoneyDetailQuery;
import com.team.lottery.extvo.OrderQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.TeamUserQuery;
import com.team.lottery.extvo.UserConsumeReportVo;
import com.team.lottery.extvo.UserDayConsumeQuery;
import com.team.lottery.extvo.UserQuery;
import com.team.lottery.service.BizSystemService;
import com.team.lottery.service.MoneyDetailService;
import com.team.lottery.service.OrderService;
import com.team.lottery.service.UserDayConsumeService;
import com.team.lottery.service.UserService;
import com.team.lottery.util.ClsDateHelper;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.BizSystem;
import com.team.lottery.vo.MoneyDetail;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserDayConsume;

/**
 * 每天计算用户团队盈利的job
 * @author gs
 *
 */
@Component("userDayConsumeJob")
public class UserDayConsumeJob extends QuartzJob {

	private static Logger logger = LoggerFactory.getLogger(UserDayConsumeJob.class);
	//分页处理处理资金明细条数
	private static final Integer DEAL_MONEYDETAIL_NUM = 2000;
	
	@Autowired
	private UserService userService;
	@Autowired
	private OrderService orderService;
	@Autowired
	private MoneyDetailService moneyDetailService;
	@Autowired
	private UserDayConsumeService userDayConsumeService;
	@Autowired
	private BizSystemService bizSystemService;
	 
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=false,rollbackFor=Exception.class)
	public void execute() {
		try {
			logger.info("开始计算所有用户团队盈利数据...");
			Long startDealTime = System.currentTimeMillis();

			Calendar calendar = Calendar.getInstance();
			Date yesterday = calendar.getTime();
			if (nowTime != null) {
				calendar.setTime(nowTime);
			} else {
				// 获取昨天的日期
				calendar.add(Calendar.DATE, -1);
			}

			yesterday = calendar.getTime();
			//获取查询的起始结束时间 03:00:00 - 02:59:59
			Date startDateTime = ClsDateHelper.getClsStartTime(yesterday);
			Date endDateTime = ClsDateHelper.getClsEndTime(yesterday);
			logger.info("查询计算的起始时间{},结束时间{}", DateUtil.parseDate(startDateTime, "yyyy-MM-dd HH:mm:ss"), DateUtil.parseDate(endDateTime, "yyyy-MM-dd HH:mm:ss"));

			//归属时间置为十二点
			calendar.set(Calendar.HOUR_OF_DAY, 12);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			Date belongDateTime = calendar.getTime();

			List<BizSystem> bizList = new ArrayList<BizSystem>();
			// this.bizSystem有值，单业务系统
			if (this.bizSystem == null || "".equals(this.bizSystem)) {
				// 查询所有业务系统
				bizList = bizSystemService.getAllBizSystem();
			} else {
				BizSystem biz = new BizSystem();
				biz.setBizSystem(this.bizSystem);
				biz = bizSystemService.getBizSystemByCondition(biz);
				bizList.add(biz);
			}

			for (BizSystem bizSystem : bizList) {

				logger.info("开始查询处理业务系统[{}]的数据",bizSystem.getBizSystem());
				/**
				 * 根据bizsystem来和时间判断是否有数据，有数据代表已经结算完成！
				 */
				UserDayConsumeQuery userDayConsumeQuery = new UserDayConsumeQuery();
				userDayConsumeQuery.setBizSystem(bizSystem.getBizSystem());
				userDayConsumeQuery.setBelongDate(belongDateTime);
				List<UserDayConsume> list = userDayConsumeService.getUserDayConsumeByCondition(userDayConsumeQuery);
				if (list.size() > 0) {
					logger.info("业务系统" + bizSystem.getBizSystem() + "的" + belongDateTime + "数据已经存在，跳过继续执行下一个业务系统！");
					continue;
				}

				// 查询用户数据
				logger.info("开始查询分页所有的用户数据...");
				Map<String, User> allUsersMap = new HashMap<String, User>();

				UserQuery userquery = new UserQuery();
				userquery.setQueryBySscRebate(1);
				userquery.setState(null); // 加载所有状态的用户
				userquery.setBizSystem(bizSystem.getBizSystem());
				//非游客用户
				userquery.setIsTourist(0);
				Page userPage = new Page(DEAL_MONEYDETAIL_NUM); // 处理用户单次处理2000条数据
				int userPageNo = 1;
				userPage.setPageNo(userPageNo);
				logger.info("获取第一页用户数据进行处理，当前第[" + userPageNo + "]页...");
				userService.getAllUsersByQueryPage(userquery, userPage);
				List<User> users = (List<User>) userPage.getPageContent();
				while (CollectionUtils.isNotEmpty(users)) {

					for (User user : users) {
						allUsersMap.put(user.getUserName(), user);
					}

					// 用户数据小于2000,已经是最后一页了
					if (users.size() < DEAL_MONEYDETAIL_NUM) {
						logger.info("处理最后一页用户数据");
						break;
					} else {
						// 获取下一页数据
						userPageNo++;
						logger.info("获取下一页用户数据进行处理，当前第[" + userPageNo + "]页...");
						userPage.setPageNo(userPageNo);
						userService.getAllUsersByQueryPage(userquery, userPage);
						users = (List<User>) userPage.getPageContent();
					}
				}
				logger.info("查询到所有的用户数据记录数为[" + allUsersMap.size() + "]...");

				logger.info("要查询资金明细的开始时间：" + DateUtil.parseDate(startDateTime, "yyyy-MM-dd HH:mm:ss") + " 结束时间："
						+ DateUtil.parseDate(endDateTime, "yyyy-MM-dd HH:mm:ss"));
				// 构建资金明细查询对象
				MoneyDetailQuery query = new MoneyDetailQuery();
				query.setCreatedDateStart(startDateTime);
				query.setCreatedDateEnd(endDateTime);
				query.setBizSystem(bizSystem.getBizSystem());
				query.setEnabled(1);
				Page page = new Page(DEAL_MONEYDETAIL_NUM); // 资金明细数据单次处理2000条数据
				int pageNo = 1;
				page.setPageNo(pageNo);
				logger.info("获取第一页资金明细数据进行处理，当前第[" + pageNo + "]页...");

				moneyDetailService.getMoneyDetailsByQueryPage(query, page);
				List<MoneyDetail> moneyDetails = (List<MoneyDetail>) page.getPageContent();
				String[] regFromUserNames = null;
				// 存放每页资金明细计算后的结果
				List<Map<String, UserConsumeReportVo>> allConsumeReportMaps = new ArrayList<Map<String, UserConsumeReportVo>>();
				Map<String, UserConsumeReportVo> consumeReportMapsResult = new LinkedHashMap<String, UserConsumeReportVo>();
				
				while (CollectionUtils.isNotEmpty(moneyDetails)) {
					// 进行资金明细拷贝
					List<MoneyDetail> totalMoneyDetails = new ArrayList<MoneyDetail>();
					for (MoneyDetail moneyDetail : moneyDetails) {
						// 未分红不计入盈亏
						/*
						 * if(MoneyDetail.NOT_YETBONUS.compareTo(moneyDetail.
						 * getYetBonus()) == 0) { continue; }
						 */
						regFromUserNames = moneyDetail.getRegfrom().split(ConstantUtil.REG_FORM_SPLIT);
						for (String regFromUserName : regFromUserNames) {
							if (StringUtils.isEmpty(regFromUserName)) {
								continue;
							}
							totalMoneyDetails.add(moneyDetail.copy(regFromUserName));
						}
						totalMoneyDetails.add(moneyDetail);
					}

					// 计算当前页的资金明细
					Map<String, UserConsumeReportVo> consumeReportMaps = moneyDetailService.getMoneyDetailForuserMap(totalMoneyDetails,
							null);
					allConsumeReportMaps.add(consumeReportMaps);

					// 资金明细数据小于2000,已经是最后一页了
					if (moneyDetails.size() < DEAL_MONEYDETAIL_NUM) {
						logger.info("处理最后一页资金明细数据");
						break;
					} else {
						// 获取下一页数据
						pageNo++;
						logger.info("获取下一页资金明细数据处理，当前第[" + pageNo + "]页...");
						page.setPageNo(pageNo);
						moneyDetailService.getMoneyDetailsByQueryPage(query, page);
						moneyDetails = (List<MoneyDetail>) page.getPageContent();
					}
				}

				mergeConsumeReportMaps(consumeReportMapsResult, allConsumeReportMaps);

				// 保存到用户团队日盈亏表
				dealSaveUserDayConsume(consumeReportMapsResult, allUsersMap, belongDateTime,startDateTime,endDateTime);

			}

			Long endDealTime = System.currentTimeMillis();
			logger.info("计算所有用户团队盈利数据结束,共耗时[" + (endDealTime - startDealTime) + "]ms...");
		} catch (Exception e) {
			logger.error("计算所有用户团队盈利数据出现异常...", e);
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * 合并每页的消费记录到总记录
	 * @param consumeReportMapsResult
	 * @param allConsumeReportMaps
	 */
	private void mergeConsumeReportMaps(Map<String,UserConsumeReportVo> consumeReportMapsResult, List<Map<String,UserConsumeReportVo>> allConsumeReportMaps) {
		if(CollectionUtils.isNotEmpty(allConsumeReportMaps)) {
			for(Map<String, UserConsumeReportVo> map : allConsumeReportMaps) {
				Iterator<String> keyIterator = map.keySet().iterator();
				while(keyIterator.hasNext()) {
					String key = keyIterator.next();
					UserConsumeReportVo userConsumeReportVo = map.get(key);
					UserConsumeReportVo userConsumeReportOldVo = consumeReportMapsResult.get(key);
					//有记录则进行合并
					if(userConsumeReportOldVo != null) {
						userConsumeReportOldVo.setBizSystem(userConsumeReportOldVo.getBizSystem());
						userConsumeReportOldVo.setMoney(userConsumeReportOldVo.getMoney().add(userConsumeReportVo.getMoney()));
						userConsumeReportOldVo.setRecharge(userConsumeReportOldVo.getRecharge().add(userConsumeReportVo.getRecharge()));
						userConsumeReportOldVo.setRechargePresent(userConsumeReportOldVo.getRechargePresent().add(userConsumeReportVo.getRechargePresent()));
						userConsumeReportOldVo.setActivitiesMoney(userConsumeReportOldVo.getActivitiesMoney().add(userConsumeReportVo.getActivitiesMoney()));
					
						userConsumeReportOldVo.setWithDraw(userConsumeReportOldVo.getWithDraw().add(userConsumeReportVo.getWithDraw()));
						userConsumeReportOldVo.setWithDrawFee(userConsumeReportOldVo.getWithDrawFee().add(userConsumeReportVo.getWithDrawFee()));
						userConsumeReportOldVo.setExtend(userConsumeReportOldVo.getExtend().add(userConsumeReportVo.getExtend()));
					
						userConsumeReportOldVo.setRegister(userConsumeReportOldVo.getRegister().add(userConsumeReportVo.getRegister()));
						userConsumeReportOldVo.setLottery(userConsumeReportOldVo.getLottery().add(userConsumeReportVo.getLottery()));
						
						userConsumeReportOldVo.setWin(userConsumeReportOldVo.getWin().add(userConsumeReportVo.getWin()));
						userConsumeReportOldVo.setRebate(userConsumeReportOldVo.getRebate().add(userConsumeReportVo.getRebate()));
						userConsumeReportOldVo.setPercentage(userConsumeReportOldVo.getPercentage().add(userConsumeReportVo.getPercentage()));
						userConsumeReportOldVo.setPayTranfer(userConsumeReportOldVo.getPayTranfer().add(userConsumeReportVo.getPayTranfer()));
						userConsumeReportOldVo.setIncomeTranfer(userConsumeReportOldVo.getIncomeTranfer().add(userConsumeReportVo.getIncomeTranfer()));
						userConsumeReportOldVo.setHalfMonthBonus(userConsumeReportOldVo.getHalfMonthBonus().add(userConsumeReportVo.getHalfMonthBonus()));
						userConsumeReportOldVo.setDayBonus(userConsumeReportOldVo.getDayBonus().add(userConsumeReportVo.getDayBonus()));
						userConsumeReportOldVo.setMonthSalary(userConsumeReportOldVo.getMonthSalary().add(userConsumeReportVo.getMonthSalary()));
						userConsumeReportOldVo.setDaySalary(userConsumeReportOldVo.getDaySalary().add(userConsumeReportVo.getDaySalary()));
						userConsumeReportOldVo.setDayCommission(userConsumeReportOldVo.getDayCommission().add(userConsumeReportVo.getDayCommission()));
						userConsumeReportOldVo.setGain(userConsumeReportOldVo.getGain().add(userConsumeReportVo.getGain()));
						userConsumeReportOldVo.setSystemaddmoney(userConsumeReportOldVo.getSystemaddmoney().add(userConsumeReportVo.getSystemaddmoney()));
						userConsumeReportOldVo.setSystemreducemoney(userConsumeReportOldVo.getSystemreducemoney().add(userConsumeReportVo.getSystemreducemoney()));
					} else {
						consumeReportMapsResult.put(key, userConsumeReportVo);
					}
				}
			}
		}
	}
	
	/**
	 * 处理保存单个用户团队的日盈利数据
	 * @param user
	 * @param query
	 */
	private void dealSaveUserDayConsume(Map<String,UserConsumeReportVo> consumeReportMapsResult, Map<String, User> allUsersMap, Date belongDate,Date startDateTime, Date endDateTime) {
		Iterator<String> keyIterator = consumeReportMapsResult.keySet().iterator();
		while(keyIterator.hasNext()) {
			String key = keyIterator.next();
			UserConsumeReportVo userConsumeReportVo = consumeReportMapsResult.get(key);
			if(userConsumeReportVo != null) {
				UserDayConsume userDayConsume = new UserDayConsume();
				BeanUtils.copyProperties(userConsumeReportVo, userDayConsume);
				userDayConsume.setRechargepresent(userConsumeReportVo.getRechargePresent());
				userDayConsume.setActivitiesmoney(userConsumeReportVo.getActivitiesMoney());
				userDayConsume.setBizSystem(userConsumeReportVo.getBizSystem());
				userDayConsume.setWithdraw(userConsumeReportVo.getWithDraw());
				userDayConsume.setWithdrawfee(userConsumeReportVo.getWithDrawFee());
				userDayConsume.setSystemaddmoney(userConsumeReportVo.getSystemaddmoney());
				userDayConsume.setSystemreducemoney(userConsumeReportVo.getSystemreducemoney());
				userDayConsume.setPaytranfer(userConsumeReportVo.getPayTranfer());
				userDayConsume.setIncometranfer(userConsumeReportVo.getIncomeTranfer());
				userDayConsume.setHalfmonthbonus(userConsumeReportVo.getHalfMonthBonus());
				userDayConsume.setDaybonus(userConsumeReportVo.getDayBonus());
				userDayConsume.setMonthsalary(userConsumeReportVo.getMonthSalary());
				userDayConsume.setDaysalary(userConsumeReportVo.getDaySalary());
				userDayConsume.setDaycommission(userConsumeReportVo.getDayCommission());
				userDayConsume.setBelongDate(belongDate);
				userDayConsume.setCreateDate(new Date());
				
				//保存用户id和refrom
				User user = allUsersMap.get(key);
				if(user != null) {
					userDayConsume.setUserId(user.getId());
					userDayConsume.setRegfrom(user.getRegfrom());
				   //统计注册人数，首冲人数，投注人数
					TeamUserQuery query = new TeamUserQuery();
					query.setBizSystem(user.getBizSystem());
					query.setRegisterDateStart(startDateTime);
					query.setRegisterDateEnd(endDateTime);
					query.setTeamLeaderName(user.getRegfrom()+user.getUserName()+"&");
					OrderQuery orderQuery =  new OrderQuery();
					orderQuery.setBizSystem(user.getBizSystem());
					orderQuery.setTeamLeaderName(user.getRegfrom()+user.getUserName()+"&");
					orderQuery.setCreatedDateStart(startDateTime);
					orderQuery.setCreatedDateEnd(endDateTime);
					Integer regCount = userService.getTeamUserRegistersByQueryCount(query);
					Integer firstRechargeCount = userService.getTeamUserfirstrechargeTimeByQueryCount(query);
				    Integer lotteryCount = orderService.getTeamUserLotteryByQueryCount(orderQuery);
				    userDayConsume.setRegCount(regCount);
				    userDayConsume.setFirstRechargeCount(firstRechargeCount);
				    userDayConsume.setLotteryCount(lotteryCount);
				} else {
					logger.error("新增用户团队日盈亏数据出现异常,用户名["+userDayConsume.getUserName()+"]对应的用户表查询不到数据...");
					continue;
				}
				
				userDayConsumeService.insertSelective(userDayConsume);
			}
		}
	}
	
	/**
	 * 处理保存单个用户团队的日盈利数据 
	 * @deprecated	 之前使用的方法
	 * @param user
	 * @param query
	 */
	private void dealSaveUserDayConsume(User user, MoneyDetailQuery query, Date belongDate) {
		String leaderName = user.getUserName();
        query.setTeamLeaderName(ConstantUtil.REG_FORM_SPLIT + leaderName + ConstantUtil.REG_FORM_SPLIT);
        query.setRealLeaderName(leaderName);
		List<MoneyDetail> moneyDetails = moneyDetailService.queryMoneyDetailsByCondition(query);
		//查询到的资金明细为空  则不处理
		if(CollectionUtils.isEmpty(moneyDetails)) {
			return;
		}
		//团队资金拷贝到当前领导人上
		Map<String,UserConsumeReportVo> consumeReportMaps = moneyDetailService.getMoneyDetailForuserMap(moneyDetails,  leaderName);
		UserConsumeReportVo userConsumeReportVo = consumeReportMaps.get(leaderName);
		if(userConsumeReportVo != null) {
			UserDayConsume userDayConsume = new UserDayConsume();
			BeanUtils.copyProperties(userConsumeReportVo, userDayConsume);
			userDayConsume.setRechargepresent(userConsumeReportVo.getRechargePresent());
			userDayConsume.setActivitiesmoney(userConsumeReportVo.getActivitiesMoney());
			userDayConsume.setBizSystem(userConsumeReportVo.getBizSystem());
			userDayConsume.setWithdraw(userConsumeReportVo.getWithDraw());
			userDayConsume.setWithdrawfee(userConsumeReportVo.getWithDrawFee());
			userDayConsume.setSystemaddmoney(userConsumeReportVo.getSystemaddmoney());
			userDayConsume.setSystemreducemoney(userConsumeReportVo.getSystemreducemoney());
			userDayConsume.setPaytranfer(userConsumeReportVo.getPayTranfer());
			userDayConsume.setIncometranfer(userConsumeReportVo.getIncomeTranfer());
			userDayConsume.setHalfmonthbonus(userConsumeReportVo.getHalfMonthBonus());
			userDayConsume.setDaybonus(userConsumeReportVo.getDayBonus());
			userDayConsume.setMonthsalary(userConsumeReportVo.getMonthSalary());
			userDayConsume.setDaysalary(userConsumeReportVo.getDaySalary());
			userDayConsume.setDaycommission(userConsumeReportVo.getDayCommission());
			userDayConsume.setBelongDate(belongDate);
			userDayConsume.setCreateDate(new Date());
			userDayConsume.setUserId(user.getId());
			userDayConsume.setRegfrom(user.getRegfrom());
			
			userDayConsumeService.insertSelective(userDayConsume);
		}
	}
	
	/**
	 * 根据起始日期和要计算的天数，重新计算用户团队盈利数据
	 * @param startDate
	 * @param days 表示要计算的天数，如果为1表示要计算的是startDate那天的数据
	 */
	public void reCalcuteUserDayConsume(Date startDate, int days) {
		logger.info("重新计算所有用户团队盈利数据  起始时间["+DateUtil.parseDate(startDate, "yyyy-MM-dd HH:mm:ss")+"],计算天数["+ days +"]");
		
		//获得当天0小时0分0秒的时间
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		Date nowDayTime = calendar.getTime();
		
		//设置为开始计算的时间
		calendar.setTime(startDate);
		for(int i = 0; i < days; i++) {
			Date calcuteDate = calendar.getTime();
			
			//计算日期判断
			if(calcuteDate.after(nowDayTime)) {
				logger.info("执行所有用户团队盈利计算起始时间不能在今天之后,结束计算...");
				return;
			}
			
			this.setNowTime(calcuteDate);
			this.execute();
			
			calendar.add(Calendar.DAY_OF_MONTH, 1);
		}
		
	}

}
