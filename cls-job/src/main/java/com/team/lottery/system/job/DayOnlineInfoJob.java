package com.team.lottery.system.job;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.extvo.DayOnlineTypeCountVo;
import com.team.lottery.extvo.LoginLogQuery;
import com.team.lottery.extvo.OrderQuery;
import com.team.lottery.service.BizSystemService;
import com.team.lottery.service.DayOnlineInfoService;
import com.team.lottery.service.LoginLogService;
import com.team.lottery.service.OrderService;
import com.team.lottery.util.DateUtil;
import com.team.lottery.vo.BizSystem;
import com.team.lottery.vo.DayOnlineInfo;
import com.team.lottery.vo.Login;

/**
 * 执行每日在线信息统计定时任务，主要是每日登录数据和投注数据的收录
 * @author gs
 *
 */
@Component("dayOnlineInfoJob")
public class DayOnlineInfoJob  extends QuartzJob{
	
	private static Logger logger = LoggerFactory.getLogger(DayOnlineInfoJob.class);
	
	@Autowired
	private DayOnlineInfoService dayOnlineInfoService;
	
	@Autowired
	private LoginLogService loginLogService;
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private BizSystemService bizSystemService;
	
	@Transactional(readOnly=false,rollbackFor=Exception.class)
	public void execute() {
		logger.info("开始执行每日在线信息统计...");
		List<BizSystem> bizList = new ArrayList<BizSystem>();
		//this.bizSystem有值，单业务系统
		if(this.bizSystem==null||"".equals(this.bizSystem)){
			//查询所有业务系统
			bizList=bizSystemService.getAllBizSystem();
		}else{
			BizSystem biz=new BizSystem();
			biz.setBizSystem(this.bizSystem);
			biz=bizSystemService.getBizSystemByCondition(biz);
			bizList.add(biz);
		}
		for (BizSystem bizSystem : bizList) {
			Date nowDate = new Date();
			// 获取昨天的日期
			Date queryDate = DateUtil.addDaysToDate(nowDate, -1);
			if(this.nowTime!=null){
				queryDate=this.nowTime;
			}
			

			DayOnlineInfo dayOnlineInfo = dayOnlineInfoService.selectByBelongDate(queryDate, bizSystem.getBizSystem());
			// 如果为空则新增
			boolean isdayOnlineInfoNull = false;
			if (dayOnlineInfo == null) {
				isdayOnlineInfoNull = true;
				dayOnlineInfo = new DayOnlineInfo();
				dayOnlineInfo.setBelongDate(queryDate);
				dayOnlineInfo.setBizSystem(bizSystem.getBizSystem());
			}

			// 查询设置昨天的登录人数
			LoginLogQuery query = new LoginLogQuery();
			query.setLogtimeStart(DateUtil.getNowStartTimeByStart(queryDate));
			query.setLogtimeEnd(DateUtil.getNowStartTimeByEnd(queryDate));
			query.setBizSystem(bizSystem.getBizSystem());
			//只查有效（非游客）
			query.setEnabled(1);
			logger.info("每日在线信息统计查询开始时间:[" + DateUtil.parseDate(query.getLogtimeStart(), "yyyy-MM-dd HH:mm:ss") + "]   结束时间:["
					+ DateUtil.parseDate(query.getLogtimeEnd(), "yyyy-MM-dd HH:mm:ss") + "]");

			List<DayOnlineTypeCountVo> loginCounts = loginLogService.getDayLoginTypeCount(query);
			if (CollectionUtils.isNotEmpty(loginCounts)) {
				for (DayOnlineTypeCountVo loginCount : loginCounts) {
					if (Login.LOGTYPE_MOBILE.equals(loginCount.getOnlineType())) {
						dayOnlineInfo.setMobileLoginTotalCount(loginCount.getCountNum());
						logger.info("查询出手机登录人数为:" + loginCount.getCountNum());
					} else if (Login.LOGTYPE_PC.equals(loginCount.getOnlineType())) {
						dayOnlineInfo.setPcLoginTotalCount(loginCount.getCountNum());
						logger.info("查询出电脑登录人数为:" + loginCount.getCountNum());
					} else if(Login.LOGTYPE_APP.equals(loginCount.getOnlineType())){
						dayOnlineInfo.setAppLoginTotalCount(loginCount.getCountNum());
						logger.info("查询出APP登录人数为:" + loginCount.getCountNum());
					}
				}
			}
			if (dayOnlineInfo.getMobileLoginTotalCount() == null) {
				dayOnlineInfo.setMobileLoginTotalCount(0);
			}
			if (dayOnlineInfo.getPcLoginTotalCount() == null) {
				dayOnlineInfo.setPcLoginTotalCount(0);
			}
			if (dayOnlineInfo.getAppLoginTotalCount() == null) {
				dayOnlineInfo.setAppLoginTotalCount(0);
			}
			Integer loginTotalCount = loginLogService.getDayLoginCount(query);
			logger.info("查询出总登录人数为:" + loginTotalCount);
			dayOnlineInfo.setLoginTotalCount(loginTotalCount);

			// 查询设置昨天的投注人数
			OrderQuery orderQuery = new OrderQuery();
			orderQuery.setCreatedDateStart(query.getLogtimeStart());
			orderQuery.setCreatedDateEnd(query.getLogtimeEnd());
			orderQuery.setBizSystem(bizSystem.getBizSystem());
			//只查有效（非游客）
			orderQuery.setEnabled(1);
			List<DayOnlineTypeCountVo> lotteryCounts = orderService.getDayLotteryTypeCount(orderQuery);
			if (CollectionUtils.isNotEmpty(lotteryCounts)) {
				for (DayOnlineTypeCountVo lotteryCount : lotteryCounts) {
					if (Login.LOGTYPE_MOBILE.equals(lotteryCount.getOnlineType())) {
						dayOnlineInfo.setMobileLotteryUserCount(lotteryCount.getCountNum());
						logger.info("查询出手机投注人数为:" + lotteryCount.getCountNum());
					} else if (Login.LOGTYPE_PC.equals(lotteryCount.getOnlineType())) {
						dayOnlineInfo.setPcLotteryUserCount(lotteryCount.getCountNum());
						logger.info("查询出电脑投注人数为:" + lotteryCount.getCountNum());
					} else if (Login.LOGTYPE_APP.equals(lotteryCount.getOnlineType())) {
						dayOnlineInfo.setAppLotteryUserCount(lotteryCount.getCountNum());
						logger.info("查询出电脑投注人数为:" + lotteryCount.getCountNum());
					}
				}
			}
			if (dayOnlineInfo.getMobileLotteryUserCount() == null) {
				dayOnlineInfo.setMobileLotteryUserCount(0);
			}
			if (dayOnlineInfo.getPcLotteryUserCount() == null) {
				dayOnlineInfo.setPcLotteryUserCount(0);
			}
			if (dayOnlineInfo.getAppLotteryUserCount() == null) {
				dayOnlineInfo.setAppLotteryUserCount(0);
			}
			Integer lotteryUserCount = orderService.getDayLotteryCount(orderQuery);
			logger.info("查询出总投注人数为:" + lotteryUserCount);
			dayOnlineInfo.setLotteryUserCount(lotteryUserCount);
			// 需新增
			if (isdayOnlineInfoNull) {
				dayOnlineInfoService.insertSelective(dayOnlineInfo);
			} else {
				dayOnlineInfoService.updateByPrimaryKeySelective(dayOnlineInfo);
			}
		}
		logger.info("执行每日在线信息统计结束...");
	}
}
