package com.team.lottery.system.job;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.enums.EConsumeActivityType;
import com.team.lottery.enums.EDayConsumeActivityOrderStatus;
import com.team.lottery.extvo.OrderQuery;
import com.team.lottery.extvo.UserDayConsumeQuery;
import com.team.lottery.service.BizSystemService;
import com.team.lottery.service.DayConsumeActityConfigService;
import com.team.lottery.service.DayConsumeActityOrderService;
import com.team.lottery.service.OrderService;
import com.team.lottery.service.UserSelfDayConsumeService;
import com.team.lottery.util.ClsDateHelper;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.MakeOrderNum;
import com.team.lottery.vo.BizSystem;
import com.team.lottery.vo.DayConsumeActityConfig;
import com.team.lottery.vo.DayConsumeActityOrder;
import com.team.lottery.vo.UserSelfDayConsume;

@Component("dayActivityJob")
public class DayActivityJob extends QuartzJob {
	
	private static Logger log = LoggerFactory.getLogger(DayActivityJob.class);

	@Autowired
	private BizSystemService bizSystemService;
	@Autowired
	private UserSelfDayConsumeService userSelfDayConsumeService;
	@Autowired
	private DayConsumeActityOrderService dayConsumeActityOrderService;
	@Autowired
	private DayConsumeActityConfigService dayConsumeActityConfigService;
	@Autowired
	private OrderService orderService;
	
	@Transactional(readOnly=false,rollbackFor=Exception.class)
	public void execute() {
	try {
		log.info("开始执行每日流水返送金额计算...");
		Long startLongTime = System.currentTimeMillis();
		Calendar calendar = Calendar.getInstance();
		Date yesterday = calendar.getTime();
		if(nowTime != null) {
			calendar.setTime(nowTime);
		} else {
			//获取前一天的日期
			calendar.add(Calendar.DAY_OF_MONTH, -1);
		}
		yesterday = calendar.getTime();
		//获取查询的起始结束时间 03:00:00 - 02:59:59
		Date startDateTime = ClsDateHelper.getClsStartTime(yesterday);
		Date endDateTime = ClsDateHelper.getClsEndTime(yesterday);
		log.info("查询下注记录开始时间{},查询下注记录结束时间{}", DateUtil.parseDate(startDateTime, "yyyy-MM-dd HH:mm:ss"), DateUtil.parseDate(endDateTime, "yyyy-MM-dd HH:mm:ss"));
		
		//设置归属时间置为昨天的12:00:00
		Date belongDate = ClsDateHelper.getClsMiddleTime(yesterday);
		
		List<BizSystem> bizList = new ArrayList<BizSystem>();
	    //this.bizSystem有值，单业务系统
		if(StringUtils.isBlank(this.bizSystem)){
			//查询所有业务系统
			bizList=bizSystemService.getAllBizSystem();
		}else{
			BizSystem biz=new BizSystem();
			biz.setBizSystem(this.bizSystem);
			biz=bizSystemService.getBizSystemByCondition(biz);
			bizList.add(biz);
		}
		
		for(BizSystem biz: bizList){
			//查询时间段内的所有有效投注记录
			UserDayConsumeQuery userDayConsumeQuery = new UserDayConsumeQuery();
			userDayConsumeQuery.setBelongDate(belongDate);
			userDayConsumeQuery.setBizSystem(biz.getBizSystem());			
			//统计所有有效投注记录总金额
			//Map<Integer, UserOrderPayVO> userOrderPayMap = getUserOrderPayMap(userDayConsumeQuery, allUsers);
			List<UserSelfDayConsume> userSelfDayConsumeList = userSelfDayConsumeService.getUserSelfDayConsumeListByCondition(userDayConsumeQuery);
			//构建日活动申请列表
			List<DayConsumeActityOrder> treatmentApplies = constructTreatmentApply(userSelfDayConsumeList, biz.getBizSystem(), belongDate, startDateTime, endDateTime);
			
			//需要生成订单数据
			//if(isGenTreatmentOrder) {
			//批量插入订单
			int count = dayConsumeActityOrderService.batchTreatmentApply(treatmentApplies);
			log.info("执行插入订单结束,共插入["+count+"]条订单记录...");
			//}
			
			//数据库保存
//			dayConsumeActityOrderService.insertBatch(treatmentApplies);
			log.info("业务系统："+ biz.getBizSystem() +"执行日活动申请计算结束,共插入["+treatmentApplies.size()+"]条记录");
		}
		Long endLongTime = System.currentTimeMillis();
		log.info("执行每日流水返送申请计算结束,耗时["+(endLongTime - startLongTime)+"]ms...");
		
		}  catch (Exception e) {
		log.info("执行每日流水返送计算出现错误", e);
		throw new RuntimeException(e.getMessage());
		}
	}
	
	/**
   	 * 构建活动奖金申请
   	 * @param halfMonthBonusConsume
   	 * @param Users
   	 * @param zongDaiUsers
   	 * @return
   	 */
   	public List<DayConsumeActityOrder> constructTreatmentApply(List<UserSelfDayConsume> userSelfDayConsumeList, String bizSystem, Date belongDate,Date startDateTime,Date endDateTime) {
   		List<DayConsumeActityOrder> treatmentApplies = new ArrayList<DayConsumeActityOrder>();
   		Date creatDate = new Date();
		
   		//正常的活动配置
		DayConsumeActityConfig query=new DayConsumeActityConfig();
		query.setBizSystem(bizSystem);
		query.setActivityType(EConsumeActivityType.NORMAL.name());
		query.setOrderBy("asc");
		List<DayConsumeActityConfig> actityConfigs = dayConsumeActityConfigService.getDayConsumeActityConfigsQuery(query);
		if(CollectionUtils.isNotEmpty(actityConfigs)) {
			for(DayConsumeActityConfig config : actityConfigs) {
				log.info("当前业务系统[{}],当前的活动配置投注额[{}]，可以领取[{}]", bizSystem, config.getLotteryNum(), config.getMoney());
			}
		}
		
		//分分彩活动配置
//		DayConsumeActityConfig queryFfc =new DayConsumeActityConfig();
//		queryFfc.setBizSystem(bizSystem);
//		queryFfc.setActivityType(EConsumeActivityType.FFC.name());	
//		List<DayConsumeActityConfig> listFfc = dayConsumeActityConfigService.getDayConsumeActityConfigsQuery(queryFfc);
			
   		//会员计算处理
   		for(UserSelfDayConsume userSelfDayConsume : userSelfDayConsumeList) {
   			if(userSelfDayConsume != null) {
   				String orderId = MakeOrderNum.makeOrderNum(MakeOrderNum.CJ);
	   			//计算发放日金额
   				BigDecimal money = getDayActivityByOrderNum(actityConfigs, userSelfDayConsume.getLottery(),userSelfDayConsume.getBizSystem());
   				
   				//计算分分彩的总投注金额
   				BigDecimal moneyFfcSum=BigDecimal.ZERO;
//   				if(listFfc!=null && listFfc.size()>0){
//   					moneyFfcSum=this.getDayActivityByffc(userSelfDayConsume.getUserId(), userSelfDayConsume.getBizSystem(), startDateTime, endDateTime);
//   				}
   				//达到日发放要求
	   			if(money.compareTo(BigDecimal.ZERO) > 0 || moneyFfcSum.compareTo(BigDecimal.ZERO) > 0) {
	   				DayConsumeActityOrder dayConsumeActityOrder = new DayConsumeActityOrder();
	   				dayConsumeActityOrder.setSerialNumber(orderId);
	   				dayConsumeActityOrder.setUserId(userSelfDayConsume.getUserId());
	   				dayConsumeActityOrder.setUserName(userSelfDayConsume.getUserName());
	   				dayConsumeActityOrder.setBizSystem(userSelfDayConsume.getBizSystem());
	   				dayConsumeActityOrder.setMoney(money);
	   				dayConsumeActityOrder.setLotteryMoney(userSelfDayConsume.getLottery());  //记录计算基准值
	   				dayConsumeActityOrder.setBelongDate(belongDate);
	   				dayConsumeActityOrder.setCreatedDate(creatDate);
	   				dayConsumeActityOrder.setReleaseStatus(EDayConsumeActivityOrderStatus.NOT_APPLY.getCode());
	   				
	   				BigDecimal moneyFfc=BigDecimal.ZERO;
	   			    /*List<DayConsumeActityConfig> listFfc=dayConsumeActityConfigService.getDayConsumeActityConfigsQuery(query);*/
//	   				if(listFfc!=null && listFfc.size()>0){
//	   					for(DayConsumeActityConfig config :listFfc){
//	   						if(moneyFfcSum.compareTo(config.getLotteryNum())>0){
//	   							moneyFfc=config.getMoney();
//	   						}
//	   					}
//	   				}
	   				if(moneyFfc.compareTo(BigDecimal.ZERO)>0){
	   					dayConsumeActityOrder.setFfcReleaseStatus(EDayConsumeActivityOrderStatus.NOT_APPLY.getCode());
	   				}else{
	   					dayConsumeActityOrder.setFfcReleaseStatus(EDayConsumeActivityOrderStatus.NOTALLOWED_APPLY.getCode());
	   				}
	   				dayConsumeActityOrder.setFfcLotteryMoney(moneyFfcSum);
	   				dayConsumeActityOrder.setFfcMoney(moneyFfc);
	   				
	   				log.info("当前业务系统[{}],用户名[{}],当前投注金额[{}],当前可以领取的金额[{}],领取状态[{}]", dayConsumeActityOrder.getBizSystem(), 
	   						dayConsumeActityOrder.getUserName(), dayConsumeActityOrder.getLotteryMoney(), dayConsumeActityOrder.getMoney(), dayConsumeActityOrder.getReleaseStatus());
	   				treatmentApplies.add(dayConsumeActityOrder);
	   			}
   			}
   		}
		return treatmentApplies;
   	}
   	
	/**
	 * 根据日量获取对应的每日活动奖金数值
	 * 日量从小到大排序获得对应奖金值
	 * @param orderNum 日量值
	 * @param bizSystem
	 * @return 未达到要求返回0
	 */
	public BigDecimal getDayActivityByOrderNum(List<DayConsumeActityConfig> actityConfigs, BigDecimal orderNum,String bizSystem) {
		BigDecimal dayActivityMoney = BigDecimal.ZERO;
		for(DayConsumeActityConfig conf:actityConfigs) {
			if(orderNum.compareTo(conf.getLotteryNum()) >= 0) {
				dayActivityMoney = conf.getMoney();
			} 
		}
		return dayActivityMoney;
   	}
	
	/**
	 * 手动重新计算活动彩金
	 * @param startDate
	 * @param days
	 */
	public void reCalcuteDayProfit(Date startDate, int days){
		
		log.info("重新计算活动彩金金额数据  起始时间["+DateUtil.parseDate(startDate, "yyyy-MM-dd HH:mm:ss")+"],计算天数["+ days +"]");
	
		
		//获得当天0小时0分0秒的时间
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		Date nowDayTime = calendar.getTime();
		
		//设置为开始计算的时间
		calendar.setTime(startDate);
		//设置为开始计算的时间
		calendar.setTime(startDate);
		for(int i = 0; i < days; i++) {
			Date calcuteDate = calendar.getTime();
			
			//计算日期判断
			if(calcuteDate.after(nowDayTime)) {
				log.info("执行计算活动彩金金额数据起始时间不能在今天之后,结束计算...");
				return;
			}
			
			this.setNowTime(calcuteDate);
			this.execute();
			
			calendar.add(Calendar.DAY_OF_MONTH, 1);
		}
		
	}
	/**
	 * 根据日量获取对应的每日活动奖金数值
	 * 日量从小到大排序获得对应奖金值
	 * @param orderNum 日量值
	 * @param bizSystem
	 * @return 未达到要求返回0
	 */
	public BigDecimal getDayActivityByffc(Integer userId,String bizSystem,Date startDateTime,Date endDateTime) {
		//BigDecimal dayActivityByffc = BigDecimal.ZERO;
		OrderQuery query=new OrderQuery();
		query.setBizSystem(bizSystem);
		query.setUserId(userId);
		query.setCreatedDateStart(startDateTime);
		query.setCreatedDateEnd(endDateTime);
		List<String> listLottery=new ArrayList<String>();
		listLottery.add("JSPK10");
		listLottery.add("JYKS");
		listLottery.add("JLFFC");
		listLottery.add("SFSSC");
		listLottery.add("WFSSC");
		listLottery.add("SHFSSC");
		listLottery.add("SFKS");
		listLottery.add("WFKS");
		listLottery.add("SFPK10");
		listLottery.add("WFPK10");
		listLottery.add("SHFPK10");
		query.setLotteryTypeList(listLottery);
		List<String> listProstate=new ArrayList<String>();
		listProstate.add("NOT_WINNING");
		listProstate.add("WINNING");
		query.setProstateList(listProstate);
		query.setStatus("END");
		query.setCreatedDateStart(startDateTime);
		query.setCreatedDateEnd(endDateTime);
		BigDecimal dayActivityByffc= orderService.getDayConsumeOrderByLottery(query);
		if(dayActivityByffc==null){
			dayActivityByffc=BigDecimal.ZERO;
		}
		return dayActivityByffc;
   	}
}
