package com.team.lottery.system.job;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.enums.EBonusOrderStatus;
import com.team.lottery.enums.EMoneyDetailType;
import com.team.lottery.enums.ENoteStatus;
import com.team.lottery.enums.ENoteType;
import com.team.lottery.enums.EReleasePolicy;
import com.team.lottery.enums.EUserDailLiLevel;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.UserConsumeReportVo;
import com.team.lottery.extvo.UserDayConsumeQuery;
import com.team.lottery.extvo.UserQuery;
import com.team.lottery.service.BizSystemService;
import com.team.lottery.service.BonusConfigService;
import com.team.lottery.service.BonusOrderService;
import com.team.lottery.service.MoneyDetailService;
import com.team.lottery.service.NotesService;
import com.team.lottery.service.UserDayConsumeService;
import com.team.lottery.service.UserMoneyTotalService;
import com.team.lottery.service.UserService;
import com.team.lottery.system.BizSystemConfigManager;
import com.team.lottery.system.BizSystemConfigVO;
import com.team.lottery.system.UserVersionExceptionThread;
import com.team.lottery.util.ClsDateHelper;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.MakeOrderNum;
import com.team.lottery.util.UserNameUtil;
import com.team.lottery.vo.BizSystem;
import com.team.lottery.vo.BonusConfig;
import com.team.lottery.vo.BonusOrder;
import com.team.lottery.vo.MoneyDetail;
import com.team.lottery.vo.Notes;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserDayConsume;

/**
 * 月分红调度执行任务(每月1号执行任务.).
 * @Description: 
 * @Author: Jamine.
 * @CreateTime: 2019-05-22 01:57:06.
 */
@Component("monthBonusJob")
public class MonthBonusJob extends QuartzJob {
	
	private static Logger log = LoggerFactory.getLogger(MonthBonusJob.class);
	
	@Autowired
	private MoneyDetailService moneyDetailService;
	
	@Autowired
	private UserService userService;

	@Autowired
	private UserDayConsumeService userDayConsumeService;
	
	@Autowired
	private BonusConfigService bonusConfigService;
	
	@Autowired
	private BonusOrderService bonusOrderService;
	
	@Autowired
	private BizSystemService bizSystemService;
	
	@Autowired
	private NotesService notesService;
	
	@Autowired
	private UserMoneyTotalService userMoneyTotalService;
	
	//执行每月分红调度任务(为每月1号).
	public static final Integer OPEN_UP_MONTH_BOUNS_TIME = 1; 

	@Transactional(readOnly=false,rollbackFor=Exception.class)
	public void execute() {
		try {
			log.info("开始执行契约分红计算...");

			Long startLongTime = System.currentTimeMillis();
			Calendar calendar = Calendar.getInstance();
			if (nowTime != null) {
				calendar.setTime(nowTime);
			}
			Date nowDate = calendar.getTime();

			Date startTime = nowDate;
			Date endTime = nowDate;
			// 月分红归属日期均为上个月1号.
			Date belongDate = nowDate;

			Date shouldReleaseDate = ClsDateHelper.getClsEndTime(calendar.getTime());
			// 每月1日执行调度任务.
			if (OPEN_UP_MONTH_BOUNS_TIME == calendar.get(Calendar.DAY_OF_MONTH)) {
				calendar.set(Calendar.HOUR_OF_DAY, 2);
				calendar.set(Calendar.MINUTE, 59);
				calendar.set(Calendar.SECOND, 59);
				calendar.set(Calendar.MILLISECOND, 999);
				endTime = ClsDateHelper.getClsEndTime(calendar.getTime());
				calendar.add(Calendar.MONTH, -1);
				calendar.set(Calendar.DAY_OF_MONTH, 1);
				startTime = ClsDateHelper.getClsStartTime(calendar.getTime());
				belongDate = DateUtil.getNowStartTimeByStart(calendar.getTime());
			}  else {
				log.info("执行月分红计算结束,调度时间不是每月1日");
				return;
			}
			List<BizSystem> bizList = new ArrayList<BizSystem>();
			// this.bizSystem有值，单业务系统
			if (this.bizSystem == null || "".equals(this.bizSystem)) {
				// 查询所有业务系统
				bizList = bizSystemService.getAllBizSystem();
			} else {
				BizSystem biz = new BizSystem();
				biz.setBizSystem(this.bizSystem);
				biz = bizSystemService.getBizSystemByCondition(biz);
				bizList.add(biz);
			}
			
			for (BizSystem bizSystem : bizList) {

				// 禁用的系统不处理
				if (bizSystem.getEnable() == 0) {
					continue;
				}

				// 查看系统是否已生成数据了
				BonusOrder queryCount = new BonusOrder();
				queryCount.setBelongDate(belongDate);
				queryCount.setBizSystem(bizSystem.getBizSystem());
				int result = bonusOrderService.getBonusOrderByQueryCount(queryCount);
				if (result > 0) {
					log.info("查到业务系统[{}]数据已经生成，不继续执行！", bizSystem.getBizSystem());
					continue;
				}

				// 查询有签订契约的用户
				UserQuery userQuery = new UserQuery();
				userQuery.setIsQueryHasBonus(true);
				userQuery.setBizSystem(bizSystem.getBizSystem());
				List<User> dealUsers = userService.getUsersByCondition(userQuery);
				
				UserDayConsumeQuery query = new UserDayConsumeQuery();
				query.setCreatedDateStart(startTime);
				query.setCreatedDateEnd(endTime);
				query.setBizSystem(bizSystem.getBizSystem());
				log.info("查询业务系统[{}]契约分红数据开始时间：{} ,结束时间：{}", bizSystem.getBizSystem(), 
						DateUtil.getDate(startTime, "yyyy-MM-dd HH:mm:ss"), DateUtil.getDate(endTime, "yyyy-MM-dd HH:mm:ss"));
				Map<String, UserConsumeReportVo> monthBonusConsume = getUserConsumeOfHalfMonthBonus(query, dealUsers);

				// 生成订单
				int orderCount = this.makeBonusOrder(dealUsers, monthBonusConsume, belongDate, shouldReleaseDate);
				log.info("业务系统[{}]生成分红订单数[{}]", bizSystem.getBizSystem(), orderCount);
				
				// 获取契约分红发放策略
				BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(bizSystem.getBizSystem());
				// 默认为手动发放策略
				String releasePolicyBonus = EReleasePolicy.MANANUAL_RELEASE.name();
				if (bizSystemConfigVO != null) {
					if (bizSystemConfigVO.getReleasePolicyBonus() != null && !"".equals(bizSystemConfigVO.getReleasePolicyBonus())) {
						releasePolicyBonus = bizSystemConfigVO.getReleasePolicyBonus();
					}
				}
				// 如果releasePolicy有值，就直接用它
				if (releasePolicy != null && !"".equals(releasePolicy)) {
					releasePolicyBonus = releasePolicy;
				}
				// 是否同时发放契约分红
				if (this.isReleaseMoney) {
					log.info("业务系统[{}],当前发放策略为[{}]", bizSystem.getBizSystem(), releasePolicyBonus);
					//手动发放不进行任何操作
					if (!EReleasePolicy.MANANUAL_RELEASE.name().equals(releasePolicyBonus)) {
						User adminUser = userService.getUserByName(bizSystem.getBizSystem(), UserNameUtil.getSuperUserNameByBizSystem(bizSystem.getBizSystem()));
						this.settleMoney(adminUser, releasePolicyBonus, belongDate);
					} 
				}
			}
			Long endLongTime = System.currentTimeMillis();
			log.info("执行契约分红计算结束,耗时[" + (endLongTime - startLongTime) + "]ms...");
		} catch (Exception e) {
			log.error("执行契约分红计算出现错误", e);
			throw new RuntimeException(e.getMessage());
		}

	}
	
   	
 	/**
   	 * 生成订单记录，入库
   	 */
	public int makeBonusOrder(List<User> allUsers, Map<String, UserConsumeReportVo> monthBonusConsume, Date belongDate, Date shouldReleaseDate) {
		//生成订单数量
		int count = 0;
		BonusOrder bonusOrder = null;
		for (User user : allUsers) {
			UserConsumeReportVo userConsumeReportVo = monthBonusConsume.get(user.getUserName());

			// 查询用户的契约规则
			BonusConfig query = new BonusConfig();
			query.setUserId(user.getId());
			query.setType(BonusConfig.BONUS_TYPE_CURRENT);
			List<BonusConfig> list = bonusConfigService.getBonusConfigByQuery(query);
			// 没有建立双方同意契约,不进行订单生成
			if (list.size() <= 0) {
				continue;
			}
			Integer parentUserId = list.get(0).getCreateUserId();
			bonusOrder = new BonusOrder();
			bonusOrder.setSerialNumber(MakeOrderNum.makeOrderNum(MakeOrderNum.FH));
			bonusOrder.setBizSystem(user.getBizSystem());
			bonusOrder.setUserId(user.getId());
			bonusOrder.setUserName(user.getUserName());
			
			// 查出签订契约的用户
			User parentUser = userService.selectByPrimaryKey(parentUserId);
			bonusOrder.setFromUserId(parentUser.getId());
			bonusOrder.setFromUserName(parentUser.getUserName());
			if (userConsumeReportVo != null) {
				bonusOrder.setGain(userConsumeReportVo.getGain());
				bonusOrder.setLotteryMoney(userConsumeReportVo.getLottery());
			} else {
				bonusOrder.setGain(BigDecimal.ZERO);
				bonusOrder.setLotteryMoney(BigDecimal.ZERO);
			}

			bonusOrder.setShouldReleaseDate(shouldReleaseDate);
			bonusOrder.setBelongDate(belongDate);
			bonusOrder.setCreatedDate(new Date());
			/**
			 * 根据契约规则来设置统计金钱
			 */

			// gain数据盈利，不分红
			if (bonusOrder.getGain().compareTo(BigDecimal.ZERO) >= 0) {
				bonusOrder.setMoney(BigDecimal.ZERO);
				bonusOrder.setBonusScale(list.get(0).getValue());
				bonusOrder.setReleaseStatus(EBonusOrderStatus.NEEDNOT_RELEASE.name());
				//bonusOrder.setReleaseDate(shouldReleaseDate);
			} else {
				
				// 根据盈亏来计算分红比例.
				BigDecimal gain = userConsumeReportVo.getGain();
				// j=是属于哪个方案下的比例，金额值
				int j = 0;
				for (int i = 0; i < list.size(); i++) {
					if ((gain.abs().divide(new BigDecimal("10000"))).compareTo(list.get(i).getScale()) < 0) {
						break;
					}
					j = i;
				}
				BonusConfig bonusConfig = list.get(j);
				// 根据分红比例，算出应发放的金额
				BigDecimal money = BigDecimal.ZERO.subtract(
						userConsumeReportVo.getGain().multiply(bonusConfig.getValue()).divide(new BigDecimal("100")));
				bonusOrder.setMoney(money);
				bonusOrder.setBonusScale(bonusConfig.getValue());
				bonusOrder.setReleaseStatus(EBonusOrderStatus.NOT_RELEASE.name());

			}
			bonusOrderService.insertSelective(bonusOrder);
			count++;
		}
		return count;
	}
	
   
   	/**
   	 * 递归契约分红
   	 * @param list 用户,
   	 * @param releasePolicy 结算策略
   	 * @param daiLiType 代理类型
   	 * @throws Exception 
   	 */
	private void settleMoney(User parentUser, String releasePolicy, Date belongDate) {
		log.info("开始处理业务系统[{}],用户名[{}]的下级契约分红记录", parentUser.getBizSystem(), parentUser.getUserName());
		//查找有契约分红,保底分红 的下级记录
		BonusConfig bonusConfigQuery = new BonusConfig();
		bonusConfigQuery.setBizSystem(parentUser.getBizSystem());
		bonusConfigQuery.setCreateUserId(parentUser.getId());
		bonusConfigQuery.setType(BonusConfig.BONUS_TYPE_CURRENT);
		bonusConfigQuery.setScale(BigDecimal.ZERO);
		List<BonusConfig> subBonusConfigs = bonusConfigService.getBonusConfigByQuery(bonusConfigQuery);
		if(CollectionUtils.isEmpty(subBonusConfigs)) {
			return;
		}
		//所有要递归处理的下级用户
		List<User> subUsers = new ArrayList<User>();
		for(BonusConfig bonusConfig : subBonusConfigs) {
			User subUser = userService.selectByPrimaryKey(bonusConfig.getUserId());
			if(subUser != null) {
				subUsers.add(subUser);
			}
		}
		
		//开始处理下级的分红订单
		BonusOrder subOrderQuery = new BonusOrder();
		subOrderQuery.setBelongDate(belongDate);
		subOrderQuery.setBizSystem(parentUser.getBizSystem());
		subOrderQuery.setFromUserId(parentUser.getId());
		// 只找尚未发放的，逾期未发放，请手动发放
		subOrderQuery.setReleaseStatus(EBonusOrderStatus.NOT_RELEASE.name());
		List<BonusOrder> subBonusOrders = bonusOrderService.getBonusOrderByQuery(subOrderQuery);
		if(CollectionUtils.isNotEmpty(subBonusOrders)) {
			for(BonusOrder bonusOrder : subBonusOrders) {
				User user = userService.selectByPrimaryKey(bonusOrder.getUserId());
				// 是超级管理员的，进行发放处理，不扣减超级管理员余额
				if(EUserDailLiLevel.SUPERAGENCY.getCode().equals(parentUser.getDailiLevel())) {
					
					MoneyDetail dailiUserMoneyDetail = new MoneyDetail();
					dailiUserMoneyDetail.setUserId(user.getId()); // 用户id
					dailiUserMoneyDetail.setOrderId(null); // 订单id
					dailiUserMoneyDetail.setBizSystem(user.getBizSystem());
					dailiUserMoneyDetail.setUserName(user.getUserName()); // 用户名
					dailiUserMoneyDetail.setOperateUserName(ConstantUtil.SYSTEM_OPERATE_NAME); // 操作人员
					dailiUserMoneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.FH)); // 订单单号
					dailiUserMoneyDetail.setWinLevel(null);
					dailiUserMoneyDetail.setWinCost(null);
					dailiUserMoneyDetail.setWinCostRule(null);
					dailiUserMoneyDetail.setDetailType(EMoneyDetailType.HALF_MONTH_BOUNS.name());
					dailiUserMoneyDetail.setIncome(bonusOrder.getMoney());
					dailiUserMoneyDetail.setPay(new BigDecimal(0));
					dailiUserMoneyDetail.setBalanceBefore(user.getMoney());
					dailiUserMoneyDetail.setBalanceAfter(user.getMoney().add(bonusOrder.getMoney())); // 支出后的金额
					dailiUserMoneyDetail.setDescription("契约分红:" + bonusOrder.getMoney());
					
					user.addMoney(bonusOrder.getMoney(), true);
					int res = 0;
					try {
						res = userService.updateByPrimaryKeyWithVersionSelective(user);
					} catch (UnEqualVersionException e) {
						log.error("分红处理发现用户表版本号不一致，开启新线程处理", e);
						UserVersionExceptionThread thread = new UserVersionExceptionThread(user.getId(), bonusOrder.getMoney(), UserVersionExceptionThread.ADD_TYPE, true, dailiUserMoneyDetail);
						thread.start();
					}
					if(res == 1) {
						moneyDetailService.insertSelective(dailiUserMoneyDetail); // 保存资金明细
					}
					
					try {
						userMoneyTotalService.addUserTotalMoneyByType(user, EMoneyDetailType.HALF_MONTH_BOUNS, bonusOrder.getMoney());
					} catch (Exception e) {
						log.error(e.getMessage());
					}

					// 更新契约订单发放的状态值
					bonusOrder.setReleaseStatus(EBonusOrderStatus.RELEASED.name());
					bonusOrder.setReleaseDate(new Date());
					bonusOrderService.updateByPrimaryKeySelective(bonusOrder);
				} else {
					//非超级管理员的分红发放，先从发放者扣除余额，不够发放则停止
					User newParentUser = userService.selectByPrimaryKey(parentUser.getId());
					//金额不够，提现锁定当前用户
					if(newParentUser.getMoney().compareTo(bonusOrder.getMoney()) < 0) {
						log.info("自动逐级发放--发放余额不足,将对" + newParentUser.getUserName() + "进行提现锁定！也影响其他级代理用户发放！");
						User updateUser = new User();
						updateUser.setId(parentUser.getId());
						updateUser.setWithdrawLock(1);
						userService.updateByPrimaryKeySelective(updateUser);
						
						// 自动逐级发放,当发到到某一个用户的余额不足，就直接停止发放(包括它的下级的下级之类用户)，并对他进行提现锁定
						if (releasePolicy.equals(EReleasePolicy.AUTO_DOWN_RELEASE.name())) {
							//移除用户的递归处理
							User removeUser = null;
							for(User user2 : subUsers) {
								if(user2.getId().equals(bonusOrder.getUserId())) {
									removeUser = user2;
									break;
								}
							}
							if(removeUser != null);
							subUsers.remove(removeUser);
						// 自动优先发放当发到到某一个用户的余额不足，就直接停止发放(不影响它的下级的下级之类用户发放)，并对他进行提现锁定
						} else if (releasePolicy.equals(EReleasePolicy.AUTO_ACCOUNT_FIRST_RELEASE.name())) {
							
						}
						
					}
					// 当前分红，父级用户余额扣减
					MoneyDetail dailiUserMoneyDetail = new MoneyDetail();
					dailiUserMoneyDetail.setUserId(parentUser.getId()); // 用户id
					dailiUserMoneyDetail.setOrderId(null); // 订单id
					dailiUserMoneyDetail.setBizSystem(parentUser.getBizSystem());
					dailiUserMoneyDetail.setUserName(parentUser.getUserName()); // 用户名
					dailiUserMoneyDetail.setOperateUserName(ConstantUtil.SYSTEM_OPERATE_NAME); // 操作人员
					dailiUserMoneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.FH)); // 订单单号
					dailiUserMoneyDetail.setWinLevel(null);
					dailiUserMoneyDetail.setWinCost(null);
					dailiUserMoneyDetail.setWinCostRule(null);
					dailiUserMoneyDetail.setDetailType(EMoneyDetailType.HALF_MONTH_BOUNS.name());
					dailiUserMoneyDetail.setIncome(new BigDecimal(0));
					dailiUserMoneyDetail.setPay(bonusOrder.getMoney());
					dailiUserMoneyDetail.setBalanceBefore(parentUser.getMoney());
					dailiUserMoneyDetail.setBalanceAfter(parentUser.getMoney().subtract(bonusOrder.getMoney())); // 支出后的金额
					dailiUserMoneyDetail.setDescription("契约分红支出:" + bonusOrder.getMoney());
					
					newParentUser.reduceMoney(bonusOrder.getMoney(), true);
					
					int res = 0;
					try {
						res = userService.updateByPrimaryKeyWithVersionSelective(newParentUser);
					} catch (UnEqualVersionException e) {
						log.error("分红处理发现用户表版本号不一致，开启新线程处理", e);
						UserVersionExceptionThread thread = new UserVersionExceptionThread(newParentUser.getId(), bonusOrder.getMoney(), UserVersionExceptionThread.REDUCE_TYPE, true, dailiUserMoneyDetail);
						thread.start();
					}
					if(res == 1) {
						moneyDetailService.insertSelective(dailiUserMoneyDetail); // 保存资金明细
					}
					
					try {
						userMoneyTotalService.addUserTotalMoneyByType(newParentUser, EMoneyDetailType.HALF_MONTH_BOUNS, BigDecimal.ZERO.subtract(bonusOrder.getMoney()));
					} catch (Exception e) {
						log.error(e.getMessage());
					}

					// 当前用分红，用户余额增加
					dailiUserMoneyDetail = new MoneyDetail();
					dailiUserMoneyDetail.setUserId(user.getId()); // 用户id
					dailiUserMoneyDetail.setOrderId(null); // 订单id
					dailiUserMoneyDetail.setBizSystem(user.getBizSystem());
					dailiUserMoneyDetail.setUserName(user.getUserName()); // 用户名
					dailiUserMoneyDetail.setOperateUserName(parentUser.getUserName()); // 操作人员
					dailiUserMoneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.FH)); // 订单单号
					dailiUserMoneyDetail.setWinLevel(null);
					dailiUserMoneyDetail.setWinCost(null);
					dailiUserMoneyDetail.setWinCostRule(null);
					dailiUserMoneyDetail.setDetailType(EMoneyDetailType.HALF_MONTH_BOUNS.name());
					dailiUserMoneyDetail.setIncome(bonusOrder.getMoney());
					dailiUserMoneyDetail.setPay(new BigDecimal(0));
					dailiUserMoneyDetail.setBalanceBefore(user.getMoney());
					dailiUserMoneyDetail.setBalanceAfter(user.getMoney().add(bonusOrder.getMoney())); // 支出后的金额
					dailiUserMoneyDetail.setDescription("契约分红:" + bonusOrder.getMoney());
					
					user.addMoney(bonusOrder.getMoney(), true);
					res = 0;
					try {
						res = userService.updateByPrimaryKeyWithVersionSelective(user);
					} catch (UnEqualVersionException e) {
						log.error("分红处理发现用户表版本号不一致，开启新线程处理", e);
						UserVersionExceptionThread thread = new UserVersionExceptionThread(user.getId(), bonusOrder.getMoney(), UserVersionExceptionThread.ADD_TYPE, true, dailiUserMoneyDetail);
						thread.start();
					}
					if(res == 1) {
						moneyDetailService.insertSelective(dailiUserMoneyDetail); // 保存资金明细
						
					}
					
					try {
						userMoneyTotalService.addUserTotalMoneyByType(user, EMoneyDetailType.HALF_MONTH_BOUNS, bonusOrder.getMoney());
					} catch (Exception e) {
						log.error(e.getMessage());
					}
					
					// 更新契约订单发放的状态值
					bonusOrder.setReleaseStatus(EBonusOrderStatus.RELEASED.name());
					bonusOrder.setReleaseDate(new Date());
					bonusOrderService.updateByPrimaryKeySelective(bonusOrder);
				}
				
				//发送系统消息
		        Notes notes = new Notes();
		        notes.setParentId(0l);
		        notes.setEnabled(1);
		        notes.setToUserId(user.getId());
		        notes.setToUserName(user.getUserName());
		        notes.setBizSystem(user.getBizSystem());
		        notes.setFromUserName(ConstantUtil.SYSTEM_OPERATE_NAME);
		        notes.setSub("[契约分红发放通知]");
		        notes.setBody("您的上级发放了归属日期["+ bonusOrder.getStartDateStr() +"-"+ bonusOrder.getEndDateStr() +"]的契约分红，金额为："+ bonusOrder.getMoney()); 
		        notes.setType(ENoteType.SYSTEM.name()); 
		        notes.setStatus(ENoteStatus.NO_READ.name());
		        notes.setCreatedDate(new Date());
		        notesService.insertSelective(notes);
			}
		}
		
		//进入递归处理
		for(User user : subUsers) {
			this.settleMoney(user, releasePolicy, belongDate);
		}
	}
   	
   	
   	
   	/**
   	 * 遍历查询获取所有用户的消费盈利情况
   	 * @param query
   	 * @return
   	 */
   	public Map<String,UserConsumeReportVo> getUserConsumeOfHalfMonthBonus(UserDayConsumeQuery query ,List<User> users){ 
   		
   		Map<String,UserConsumeReportVo> consumeReportMaps = new HashMap<String, UserConsumeReportVo>();
   		for(User user : users) {
   			UserConsumeReportVo userConsumeReportVo = null;
   			
   			UserDayConsumeQuery dayConsumeQuery = new UserDayConsumeQuery();
   	   		dayConsumeQuery.setCreatedDateStart(query.getCreatedDateStart());
   	   		dayConsumeQuery.setCreatedDateEnd(query.getCreatedDateEnd());
   	   		dayConsumeQuery.setUserName(user.getUserName());
   	   		dayConsumeQuery.setBizSystem(query.getBizSystem());
   	   		List<UserDayConsume> userDayConsumes = userDayConsumeService.getUserDayConsumeByCondition(dayConsumeQuery);
   	   		if(CollectionUtils.isNotEmpty(userDayConsumes)) {
   	   			if(userDayConsumes.size() > 1) {
   	   				log.error("查询用户的团队消费数据有误，查询结果大于2条");
   	   			}
   	   			UserDayConsume userDayConsume = userDayConsumes.get(0);
   	   			userConsumeReportVo = new UserConsumeReportVo(user.getUserName());
   	   			userConsumeReportVo.setUserId(user.getId());
   	   			userConsumeReportVo.setGain(userDayConsume.getGain());
   	   			userConsumeReportVo.setLottery(userDayConsume.getLottery());
   	   			userConsumeReportVo.setBizSystem(userDayConsume.getBizSystem());
   	   			userConsumeReportVo.setRegfrom(userDayConsume.getRegfrom());
   	   			
   	   		}
   	   		
   			if(userConsumeReportVo != null) {
   				consumeReportMaps.put(user.getUserName(), userConsumeReportVo);
   			}
   		}
   		
        return consumeReportMaps;
        
   	}
   	
   	
   	
   
   	
   
   	
   	
   
   	
 	
 	
	
	public static void main(String[] args) {
		Calendar calendar = Calendar.getInstance();
		Date nowDate = calendar.getTime();
		
		Date startTime = nowDate;
		Date endTime = nowDate;
		//半月分红归属日期   上半月归属时间为当月1号  下半月归属时间为上个月16号
		Date belongDate = nowDate;
		
		Date shouldReleaseDate=ClsDateHelper.getClsEndTime(calendar.getTime());
		//是每月的1号
		if(1 == calendar.get(Calendar.DAY_OF_MONTH)) {
			calendar.set(Calendar.HOUR_OF_DAY, 2);  
			calendar.set(Calendar.MINUTE, 59);  
	    	calendar.set(Calendar.SECOND, 59);  
	    	calendar.set(Calendar.MILLISECOND, 999);
			endTime = calendar.getTime();
			calendar.add(Calendar.MONTH, -1);
			calendar.set(Calendar.DAY_OF_MONTH, 16);
			startTime = ClsDateHelper.getClsStartTime(calendar.getTime());
			belongDate = DateUtil.getNowStartTimeByStart(calendar.getTime());
		//是每月的16号
		}  else {
			log.info("执行半月分红计算结束,调度时间不是1号或16号");
			return;
		}
		
		System.out.println(belongDate);
	}
}
