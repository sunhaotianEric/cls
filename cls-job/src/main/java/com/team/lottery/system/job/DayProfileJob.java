package com.team.lottery.system.job;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.team.lottery.enums.EFundPayType;
import com.team.lottery.enums.EMoneyDetailType;
import com.team.lottery.enums.EUserDailLiLevel;
import com.team.lottery.extvo.MoneyDetailQuery;
import com.team.lottery.extvo.OrderQuery;
import com.team.lottery.extvo.RechargeWithDrawOrderQuery;
import com.team.lottery.extvo.TeamUserQuery;
import com.team.lottery.service.BizSystemService;
import com.team.lottery.service.BonusOrderService;
import com.team.lottery.service.DayProfitService;
import com.team.lottery.service.DaySalaryOrderService;
import com.team.lottery.service.MoneyDetailService;
import com.team.lottery.service.OrderService;
import com.team.lottery.service.RechargeOrderService;
import com.team.lottery.service.UserService;
import com.team.lottery.service.WithdrawOrderService;
import com.team.lottery.util.ClsDateHelper;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.UserNameUtil;
import com.team.lottery.vo.BizSystem;
import com.team.lottery.vo.DayProfit;
import com.team.lottery.vo.MoneyDetail;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.User;
import com.team.lottery.vo.WithdrawOrder;

@Component("dayProfileJob")
public class DayProfileJob extends QuartzJob {
	
	private static Logger logger = LoggerFactory.getLogger(DayProfileJob.class);
	
	/**
	 * 是否插入数据库中，针对今日实时统计
	 */
	private Boolean isInsertTable = true;
	
	@Autowired
	public DayProfitService dayProfitService;
	@Autowired
	private RechargeOrderService rechargeWithDrawOrderService;
	@Autowired
	private MoneyDetailService moneyDetailService;
	@Autowired
	private UserService userService;
	@Autowired
	private BizSystemService bizSystemService;
	@Autowired
	private OrderService orderService;
	@Autowired
	private BonusOrderService bonusOrderService;
	@Autowired
	private DaySalaryOrderService daySalaryOrderService;
	@Autowired
	private RechargeOrderService rechargeOrderService;
	@Autowired
	private WithdrawOrderService withdrawOrderService;
	
	public Boolean getIsInsertTable() {
		return isInsertTable;
	}

	public void setIsInsertTable(Boolean isInsertTable) {
		this.isInsertTable = isInsertTable;
	}

	/* (non-Javadoc)
	 * @see com.team.lottery.system.job.QuartzJob#execute()
	 */
	@Override
	public void execute() {
		logger.info("开始计算平台盈利数据...");
		Long startDealTime = System.currentTimeMillis();
		Calendar calendar = Calendar.getInstance();
		Date yesterday = calendar.getTime();
		if (nowTime != null) {
			calendar.setTime(nowTime);
		} else {
			// 获取昨天的日期
			calendar.add(Calendar.DATE, -1);
		}
		yesterday = calendar.getTime();
	    this.getProfitByBelongDate(yesterday,this.bizSystem,this.isInsertTable);
	    Long endDealTime = System.currentTimeMillis();
		logger.info("计算所有平台盈利数据结束,共耗时[" + (endDealTime - startDealTime) + "]ms...");
	}
	
	/**
	 * 手动重新计算平台盈亏数据
	 * @param startDate
	 * @param days
	 */
	public void reCalcuteDayProfit(Date startDate, int days){
		
		logger.info("重新计算平台盈利数据  起始时间["+DateUtil.parseDate(startDate, "yyyy-MM-dd HH:mm:ss")+"],计算天数["+ days +"]");
	
		
		//获得当天0小时0分0秒的时间
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		Date nowDayTime = calendar.getTime();
		
		//设置为开始计算的时间
		calendar.setTime(startDate);
		//设置为开始计算的时间
		calendar.setTime(startDate);
		for(int i = 0; i < days; i++) {
			Date calcuteDate = calendar.getTime();
			
			//计算日期判断
			if(calcuteDate.after(nowDayTime)) {
				logger.info("执行计算平台盈利数据计算起始时间不能在今天之后,结束计算...");
				return;
			}
			
			this.setNowTime(calcuteDate);
			this.execute();
			
			calendar.add(Calendar.DAY_OF_MONTH, 1);
		}
		
	}
	
	
	
	/**
	 * 注，这个方法包含查询实时的盈亏报表，摘抄原因逻辑，只是让它有返回值，不插入数据库
	 * 获取统计的盈亏报表，实时查询时候isInsertTable =false不插入数据库,同事也代表不打印日志！
	 */
	public DayProfit getProfitByBelongDate(Date yesterday,String defaultBizSystem,Boolean isInsertTable) {

		try {
			Date startDateTime = ClsDateHelper.getClsStartTime(yesterday);
			Date endDateTime = ClsDateHelper.getClsEndTime(yesterday);
			if(isInsertTable){//没插入数据库，不打印日志
			logger.info("计算的开始时间：[{}]，结束时间：[{}]", DateUtil.parseDate(startDateTime, "yyyy-MM-dd HH:mm:ss"), DateUtil.parseDate(endDateTime, "yyyy-MM-dd HH:mm:ss"));
			}
			// 设置归属时间置为昨天的12:00:00
			Date belongDate = ClsDateHelper.getClsMiddleTime(yesterday);

			// 查询 网银充值,快捷充值,微信充值,支付宝充值 取款手续费
			RechargeWithDrawOrderQuery query = new RechargeWithDrawOrderQuery();
			query.setCreatedDateStart(startDateTime);
			query.setCreatedDateEnd(endDateTime);

			// 获得资金明细
			MoneyDetailQuery moneyDetailQuery = new MoneyDetailQuery();
			moneyDetailQuery.setCreatedDateStart(startDateTime);
			moneyDetailQuery.setCreatedDateEnd(endDateTime);

			TeamUserQuery newUserQuery = new TeamUserQuery();
			newUserQuery.setRegisterDateStart(startDateTime);
			newUserQuery.setRegisterDateEnd(endDateTime);

			OrderQuery orderQuery = new OrderQuery();
			orderQuery.setCreatedDateStart(startDateTime);
			orderQuery.setCreatedDateEnd(endDateTime);
			//有效订单
			orderQuery.setEnabled(1);
			List<BizSystem> bizList = new ArrayList<BizSystem>();
			// this.bizSystem有值，单业务系统
			if (StringUtils.isBlank(defaultBizSystem)) {
				// 查询所有业务系统
				bizList = bizSystemService.getAllBizSystem();
			} else {
				BizSystem biz = new BizSystem();
				biz.setBizSystem(defaultBizSystem);
				biz = bizSystemService.getBizSystemByCondition(biz);
				bizList.add(biz);
			}
			DayProfit dayProfit = new DayProfit();
			DayProfit dayProfitTotal = new DayProfit();
			for (BizSystem bz : bizList) {
				if ("SUPER_SYSTEM".equals(bz.getBizSystem())) {
					continue;
				}
				String bizSystem = bz.getBizSystem();
				if(isInsertTable){//没插入数据库，不打印日志
				logger.info("查询处理业务系统{}的盈亏...", bizSystem);
				}
				query.setBizSystem(bizSystem);
				moneyDetailQuery.setBizSystem(bizSystem);
				newUserQuery.setBizSystem(bizSystem);
				orderQuery.setBizSystem(bizSystem);

				dayProfit = new DayProfit();
				dayProfit.setBizSystem(bizSystem);

				// 按类型统计各充值类型、取现的申请总额，手续费总额,笔数
				List<RechargeOrder> rechargeOrderForSuccess = rechargeOrderService.getTotalValueSuccessOrderByType(query);
				List<WithdrawOrder> withdrawOrderForSuccess = withdrawOrderService.getTotalValueSuccessOrderByType(query);
				BigDecimal totalBankTransferMoney = BigDecimal.ZERO;
				BigDecimal totalQuickPayMoney = BigDecimal.ZERO;
				BigDecimal totalWeixinMoney = BigDecimal.ZERO;
				BigDecimal totalAlipayMoney = BigDecimal.ZERO;
				BigDecimal totalWithdrawMoney = BigDecimal.ZERO;
				BigDecimal totalWithdrawFeeMoney = BigDecimal.ZERO;
				BigDecimal totalRechargeQqpay = BigDecimal.ZERO;
				BigDecimal totalRechargeJdpay = BigDecimal.ZERO;
				BigDecimal totalRechargeUnionpay = BigDecimal.ZERO;

				Integer rechargeCount = 0;
				Integer withdrawCount = 0;
				if (CollectionUtils.isNotEmpty(rechargeOrderForSuccess)) {
					Iterator<RechargeOrder> iterator = rechargeOrderForSuccess.iterator();
					while (iterator.hasNext()) {
						RechargeOrder drawOrder = iterator.next();
						     // 统计充值
							// 充值笔数
							rechargeCount += drawOrder.getNum();
							if (drawOrder.getPayType().equals(EFundPayType.BANK_TRANSFER.name())) {
								// 网银汇款
								totalBankTransferMoney = totalBankTransferMoney.add(drawOrder.getApplyValue());
							} else if (drawOrder.getPayType().equals(EFundPayType.QUICK_PAY.name())) {
								// 快捷支付
								totalQuickPayMoney = totalQuickPayMoney.add(drawOrder.getApplyValue());
							} else if (drawOrder.getPayType().equals(EFundPayType.WEIXIN.name()) || drawOrder.getPayType().equals(EFundPayType.WEIXINWAP.name()) || drawOrder.getPayType().equals(EFundPayType.WEIXINBARCODE.name())) {
								// 微信支付
								totalWeixinMoney = totalWeixinMoney.add(drawOrder.getApplyValue());
							} else if (drawOrder.getPayType().equals(EFundPayType.ALIPAY.name()) || drawOrder.getPayType().equals(EFundPayType.ALIPAYWAP.name())) {
								// 支付宝
								totalAlipayMoney = totalAlipayMoney.add(drawOrder.getApplyValue());
							}else if (drawOrder.getPayType().equals(EFundPayType.JDPAY.name())) {
								// 京东扫码
								totalRechargeJdpay = totalRechargeJdpay.add(drawOrder.getApplyValue());
							}else if (drawOrder.getPayType().equals(EFundPayType.UNIONPAY.name())) {
								// 银联扫码
								totalRechargeUnionpay = totalRechargeUnionpay.add(drawOrder.getApplyValue());
							}else if (drawOrder.getPayType().equals(EFundPayType.QQPAY.name()) || drawOrder.getPayType().equals(EFundPayType.QQPAYWAP.name())) {
								// qq钱包
								totalRechargeQqpay = totalRechargeQqpay.add(drawOrder.getApplyValue());
							}
					}

				}
				
				if (CollectionUtils.isNotEmpty(withdrawOrderForSuccess)) {
					Iterator<WithdrawOrder> iterator = withdrawOrderForSuccess.iterator();
					while (iterator.hasNext()) {
						WithdrawOrder drawOrder = iterator.next();
						// 统计取现 , 取现手续费
							totalWithdrawMoney = totalWithdrawMoney.add(drawOrder.getApplyValue());
							totalWithdrawFeeMoney = totalWithdrawFeeMoney.add(drawOrder.getFeeValue());
							withdrawCount += drawOrder.getNum();
						
							
					}

				}
				
				
				dayProfit.setRechargeBankTransfer(totalBankTransferMoney.setScale(ConstantUtil.BIGDECIMAL_SCAL_MONEY, BigDecimal.ROUND_HALF_DOWN));
				dayProfit.setRechargeQuickPay(totalQuickPayMoney.setScale(ConstantUtil.BIGDECIMAL_SCAL_MONEY, BigDecimal.ROUND_HALF_DOWN));
				dayProfit.setRechargeWeixin(totalWeixinMoney.setScale(ConstantUtil.BIGDECIMAL_SCAL_MONEY, BigDecimal.ROUND_HALF_DOWN));
				dayProfit.setRechargeAlipay(totalAlipayMoney.setScale(ConstantUtil.BIGDECIMAL_SCAL_MONEY, BigDecimal.ROUND_HALF_DOWN));
				dayProfit.setRechargeQqpay(totalRechargeQqpay.setScale(ConstantUtil.BIGDECIMAL_SCAL_MONEY, BigDecimal.ROUND_HALF_DOWN));
				dayProfit.setRechargeUnionpay(totalRechargeUnionpay.setScale(ConstantUtil.BIGDECIMAL_SCAL_MONEY, BigDecimal.ROUND_HALF_DOWN));
				dayProfit.setRechargeJdpay(totalRechargeJdpay.setScale(ConstantUtil.BIGDECIMAL_SCAL_MONEY, BigDecimal.ROUND_HALF_DOWN));
				// 取现的统计这里是订单的统计值
				dayProfit.setWithdraw(totalWithdrawMoney.setScale(ConstantUtil.BIGDECIMAL_SCAL_MONEY, BigDecimal.ROUND_HALF_DOWN));
				dayProfit.setWithdrawFee(totalWithdrawFeeMoney.setScale(ConstantUtil.BIGDECIMAL_SCAL_MONEY, BigDecimal.ROUND_HALF_DOWN));
				dayProfit.setRechargeCount(rechargeCount);
				dayProfit.setWithdrawCount(withdrawCount);
				// 充值的总额统计这里是订单的统计值 
				BigDecimal totalRechargeMoney = totalBankTransferMoney.add(totalQuickPayMoney).add(totalWeixinMoney).add(totalAlipayMoney)
						.add(totalRechargeQqpay).add(totalRechargeUnionpay).add(totalRechargeJdpay);
				if(isInsertTable){//没插入数据库，不打印日志
				logger.info("订单统计充值金额：{},提现金额：{},提现手续费金额：{}", totalRechargeMoney, totalWithdrawMoney, totalWithdrawFeeMoney);
				}
				dayProfit.setRecharge(totalRechargeMoney.setScale(ConstantUtil.BIGDECIMAL_SCAL_MONEY, BigDecimal.ROUND_HALF_DOWN));

				// 按类型统计各充值类型、取现的人数
				List<RechargeOrder> userCountRechargeOrders = rechargeOrderService.getUserCountSuccessOrderByType(query);
				List<WithdrawOrder> userCountWithDrawOrders = withdrawOrderService.getUserCountSuccessOrderByType(query);
				List<RechargeOrder> userCountRechargeWithDrawOrdersSpecial = rechargeOrderService.getUserCountSuccessOrderByTypeSpecial(query);
				Integer rechargeBankTransferUserCount = 0;
				Integer rechargeQuickPayUserCount = 0;
				Integer rechargeWeixinUserCount = 0;
				Integer rechargeAlipayUserCount = 0;
				Integer rechargeUserCount = 0;
				Integer withdrawUserCount = 0;
				Integer rechargeQqpayUserCount = 0;
				Integer rechargeJdpayUserCount = 0;
				Integer rechargeUnionpayUserCount = 0;
				
				if (CollectionUtils.isNotEmpty(userCountRechargeOrders)) {
					for (RechargeOrder drawOrder : userCountRechargeOrders) {
						// 统计各充值人数
							if (drawOrder.getPayType().equals(EFundPayType.BANK_TRANSFER.name())) {
								// 网银汇款
								rechargeBankTransferUserCount = drawOrder.getNum();
							} else if (drawOrder.getPayType().equals(EFundPayType.QUICK_PAY.name())) {
								// 快捷支付
								rechargeQuickPayUserCount = drawOrder.getNum();
							} 
							/*else if (drawOrder.getPayType().equals(EFundPayType.WEIXIN.name())) {
								// 微信支付
								rechargeWeixinUserCount = drawOrder.getNum();
							} else if (drawOrder.getPayType().equals(EFundPayType.ALIPAY.name())) {
								// 支付宝
								rechargeAlipayUserCount = drawOrder.getNum();
							} else if (drawOrder.getPayType().equals(EFundPayType.QQPAY.name())) {
								// qq钱包
								rechargeQqpayUserCount = drawOrder.getNum();
							}*/
							else if (drawOrder.getPayType().equals(EFundPayType.JDPAY.name())) {
								// 京东扫码
								rechargeJdpayUserCount = drawOrder.getNum();
							} else if (drawOrder.getPayType().equals(EFundPayType.UNIONPAY.name())) {
								// 银联扫码
								rechargeUnionpayUserCount = drawOrder.getNum();
							}
						/* else if (drawOrder.getOperateType().equals(EMoneyDetailType.WITHDRAW.name())) {
							// 统计取现人数
							withdrawUserCount = drawOrder.getNum();
						}*/
					}

				}
				
				if (CollectionUtils.isNotEmpty(userCountWithDrawOrders)) {
					for (WithdrawOrder drawOrder : userCountWithDrawOrders) {
							// 统计取现人数
							withdrawUserCount = drawOrder.getNum();
						
					}

				}
				
				
				
				if (CollectionUtils.isNotEmpty(userCountRechargeWithDrawOrdersSpecial)) {
					for (RechargeOrder drawOrder : userCountRechargeWithDrawOrdersSpecial) {
						// 统计各充值人数
							if (drawOrder.getPayType().equals(EFundPayType.WEIXIN.name()) || drawOrder.getPayType().equals(EFundPayType.WEIXINWAP.name()) || drawOrder.getPayType().equals(EFundPayType.WEIXINBARCODE.name())) {
								// 微信支付
								rechargeWeixinUserCount += drawOrder.getNum();
							} else if (drawOrder.getPayType().equals(EFundPayType.ALIPAY.name()) || drawOrder.getPayType().equals(EFundPayType.ALIPAYWAP.name())) {
								// 支付宝
								rechargeAlipayUserCount += drawOrder.getNum();
							} else if (drawOrder.getPayType().equals(EFundPayType.QQPAY.name()) || drawOrder.getPayType().equals(EFundPayType.QQPAYWAP.name())) {
								// qq钱包
								rechargeQqpayUserCount += drawOrder.getNum();
							}
					}

				}

				// 统计充值、取现的人数 这里重复统计，为了统计充值的人数
				userCountRechargeOrders = rechargeOrderService.getUserCountSuccessOrderByOperateType(query);
				userCountWithDrawOrders = withdrawOrderService.getUserCountSuccessOrderByOperateType(query);
				if (CollectionUtils.isNotEmpty(userCountRechargeOrders)) {
					for (RechargeOrder drawOrder : userCountRechargeOrders) {
						rechargeUserCount = drawOrder.getNum();
					}
				}
				
				if (CollectionUtils.isNotEmpty(userCountWithDrawOrders)) {
					for (WithdrawOrder drawOrder : userCountWithDrawOrders) {
							withdrawUserCount = drawOrder.getNum();
					}
				}
				dayProfit.setRechargeBankTransferUserCount(rechargeBankTransferUserCount);
				dayProfit.setRechargeQuickPayUserCount(rechargeQuickPayUserCount);
				dayProfit.setRechargeAlipayUserCount(rechargeAlipayUserCount);
				dayProfit.setRechargeWeixinUserCount(rechargeWeixinUserCount);
				dayProfit.setRechargeUserCount(rechargeUserCount);
				dayProfit.setWithdrawUserCount(withdrawUserCount);
				dayProfit.setRechargeQqpayUserCount(rechargeQqpayUserCount);
				dayProfit.setRechargeJdpayUserCount(rechargeJdpayUserCount);
				dayProfit.setRechargeUnionpayUserCount(rechargeUnionpayUserCount);

				BigDecimal totalLotteryMoney = BigDecimal.ZERO;
				BigDecimal totalWinMoney = BigDecimal.ZERO;
				BigDecimal totalLotteryRegressionMoney = BigDecimal.ZERO;
				BigDecimal totaLotteryStopAfterNumberMoney = BigDecimal.ZERO;
				BigDecimal totalPercentageMoney = BigDecimal.ZERO;
				BigDecimal totalRebateMoney = BigDecimal.ZERO;
				BigDecimal totalRechargeManual = BigDecimal.ZERO;
				totalRechargeMoney = BigDecimal.ZERO;
				BigDecimal totalRechargePresentMoney = BigDecimal.ZERO;
				BigDecimal totalActivitiesMoney = BigDecimal.ZERO;
				BigDecimal totalSystemAddMoney = BigDecimal.ZERO;
				BigDecimal totalSystemReduceMoney = BigDecimal.ZERO;
				BigDecimal totalManageReduceMoney = BigDecimal.ZERO;
				totalWithdrawMoney = BigDecimal.ZERO;
				totalWithdrawFeeMoney = BigDecimal.ZERO;

				BigDecimal totalIncomeTranferMoney = BigDecimal.ZERO;
				BigDecimal totalPayTranferMoney = BigDecimal.ZERO;

				BigDecimal totalDayBounsMoney = BigDecimal.ZERO;
				BigDecimal totalMonthSalaryMoney = BigDecimal.ZERO;
				BigDecimal totalHalfMonthBounsMoney = BigDecimal.ZERO;
				BigDecimal totalDayCommissionMoney = BigDecimal.ZERO;
				BigDecimal totalDaySalaryMoney = BigDecimal.ZERO;
				//只查询有效去统计
				moneyDetailQuery.setEnabled(1);
				// 按时间查询各类型资金明细总额
				List<MoneyDetail> usersIncomeAndExpenses = moneyDetailService.getTotalMoneyByDetailType(moneyDetailQuery);
				if (CollectionUtils.isNotEmpty(usersIncomeAndExpenses)) {
					Iterator<MoneyDetail> iterator = usersIncomeAndExpenses.iterator();
					while (iterator.hasNext()) {
						// 对资金明细进行处理，根据各自类型获取income或者pay值，有的资金明细会同时有income,pay值，需要同时处理
						MoneyDetail nextMoneyDetail = iterator.next();
						if (nextMoneyDetail.getDetailType().equals(EMoneyDetailType.LOTTERY.name())) {
							totalLotteryMoney = totalLotteryMoney.add(nextMoneyDetail.getPay());
						} else if (nextMoneyDetail.getDetailType().equals(EMoneyDetailType.WIN.name())) {
							totalWinMoney = totalWinMoney.add(nextMoneyDetail.getIncome());
						} else if (nextMoneyDetail.getDetailType().equals(EMoneyDetailType.LOTTERY_REGRESSION.name())) {// 投注回退
							totalLotteryRegressionMoney = totalLotteryRegressionMoney.add(nextMoneyDetail.getIncome());
						} else if (nextMoneyDetail.getDetailType().equals(EMoneyDetailType.LOTTERY_STOP_AFTER_NUMBER.name())) {// 追号回退
							totaLotteryStopAfterNumberMoney = totaLotteryStopAfterNumberMoney.add(nextMoneyDetail.getIncome());
						} else if (nextMoneyDetail.getDetailType().equals(EMoneyDetailType.REBATE.name())) {
							totalRebateMoney = totalRebateMoney.add(nextMoneyDetail.getIncome());
						} else if (nextMoneyDetail.getDetailType().equals(EMoneyDetailType.PERCENTAGE.name())) {
							totalPercentageMoney = totalPercentageMoney.add(nextMoneyDetail.getIncome());
						} else if (nextMoneyDetail.getDetailType().equals(EMoneyDetailType.RECHARGE.name())) {
							// 充值关闭会有对应pay值
							totalRechargeMoney = totalRechargeMoney.add(nextMoneyDetail.getIncome()).subtract(nextMoneyDetail.getPay());
						} else if (nextMoneyDetail.getDetailType().equals(EMoneyDetailType.RECHARGE_PRESENT.name())) {
							// 充值赠送扣减 会有对应的pay值
							totalRechargePresentMoney = totalRechargePresentMoney.add(nextMoneyDetail.getIncome()).subtract(nextMoneyDetail.getPay());
						} else if (nextMoneyDetail.getDetailType().equals(EMoneyDetailType.ACTIVITIES_MONEY.name())) {
							// 活动彩金扣减 会有对应的pay值
							totalActivitiesMoney = totalActivitiesMoney.add(nextMoneyDetail.getIncome()).subtract(nextMoneyDetail.getPay());
						} else if (nextMoneyDetail.getDetailType().equals(EMoneyDetailType.SYSTEM_ADD_MONEY.name())) {
							totalSystemAddMoney = totalSystemAddMoney.add(nextMoneyDetail.getIncome());
						} else if (nextMoneyDetail.getDetailType().equals(EMoneyDetailType.SYSTEM_REDUCE_MONEY.name())) {
							totalSystemReduceMoney = totalSystemReduceMoney.add(nextMoneyDetail.getPay());
						} else if (nextMoneyDetail.getDetailType().equals(EMoneyDetailType.WITHDRAW.name())) {
							// 提现关闭会有对应income值
							totalWithdrawMoney = totalWithdrawMoney.add(nextMoneyDetail.getPay()).subtract(nextMoneyDetail.getIncome());
						} else if (nextMoneyDetail.getDetailType().equals(EMoneyDetailType.WITHDRAW_FEE.name())) {
							// 提现关闭会有对应income值
							totalWithdrawFeeMoney = totalWithdrawFeeMoney.add(nextMoneyDetail.getPay()).subtract(nextMoneyDetail.getIncome());
						} else if (nextMoneyDetail.getDetailType().equals(EMoneyDetailType.INCOME_TRANFER.name())) {
							totalIncomeTranferMoney = totalIncomeTranferMoney.add(nextMoneyDetail.getIncome());
						} else if (nextMoneyDetail.getDetailType().equals(EMoneyDetailType.PAY_TRANFER.name())) {
							totalPayTranferMoney = totalPayTranferMoney.add(nextMoneyDetail.getPay());
						} else if (nextMoneyDetail.getDetailType().equals(EMoneyDetailType.DAY_BOUNS.name())) {
							totalDayBounsMoney = totalDayBounsMoney.add(nextMoneyDetail.getIncome());
						} else if (nextMoneyDetail.getDetailType().equals(EMoneyDetailType.HALF_MONTH_BOUNS.name())) {
							totalHalfMonthBounsMoney = totalHalfMonthBounsMoney.add(nextMoneyDetail.getIncome());
						} else if (nextMoneyDetail.getDetailType().equals(EMoneyDetailType.DAY_COMMISSION.name())) {
							totalDayCommissionMoney = totalDayCommissionMoney.add(nextMoneyDetail.getIncome());
						} else if (nextMoneyDetail.getDetailType().equals(EMoneyDetailType.DAY_SALARY.name())) {
							totalDaySalaryMoney = totalDaySalaryMoney.add(nextMoneyDetail.getIncome());
						} else if (nextMoneyDetail.getDetailType().equals(EMoneyDetailType.MONTH_SALARY.name())) {
							totalMonthSalaryMoney = totalMonthSalaryMoney.add(nextMoneyDetail.getIncome());
						}else if (nextMoneyDetail.getDetailType().equals(EMoneyDetailType.RECHARGE_MANUAL.name())) {
							totalRechargeManual = totalRechargeManual.add(nextMoneyDetail.getIncome());
						}else if (nextMoneyDetail.getDetailType().equals(EMoneyDetailType.MANAGE_REDUCE_MONEY.name())) {
							totalManageReduceMoney = totalManageReduceMoney.add(nextMoneyDetail.getPay());
						}

					}
				}
				//总充值 要加上 人工存款
				totalRechargeMoney = totalRechargeMoney.add(totalRechargeManual);
				dayProfit.setLottery(totalLotteryMoney.setScale(ConstantUtil.BIGDECIMAL_SCAL_MONEY, BigDecimal.ROUND_HALF_DOWN));
				dayProfit.setLotteryRegression(totalLotteryRegressionMoney.setScale(ConstantUtil.BIGDECIMAL_SCAL_MONEY, BigDecimal.ROUND_HALF_DOWN));
				dayProfit.setLotteryStopAfterNumber(totaLotteryStopAfterNumberMoney.setScale(ConstantUtil.BIGDECIMAL_SCAL_MONEY, BigDecimal.ROUND_HALF_DOWN));
				dayProfit.setWin(totalWinMoney.setScale(ConstantUtil.BIGDECIMAL_SCAL_MONEY, BigDecimal.ROUND_HALF_DOWN));
				dayProfit.setRebate(totalRebateMoney.setScale(ConstantUtil.BIGDECIMAL_SCAL_MONEY, BigDecimal.ROUND_HALF_DOWN));
				dayProfit.setPercentage(totalPercentageMoney.setScale(ConstantUtil.BIGDECIMAL_SCAL_MONEY, BigDecimal.ROUND_HALF_DOWN));
				// 充值
				dayProfit.setRecharge(totalRechargeMoney.setScale(ConstantUtil.BIGDECIMAL_SCAL_MONEY, BigDecimal.ROUND_HALF_DOWN));
				// 其他存入总额：
				dayProfit.setRechargePresent(totalRechargePresentMoney.setScale(ConstantUtil.BIGDECIMAL_SCAL_MONEY, BigDecimal.ROUND_HALF_DOWN));
				dayProfit.setActivitiesMoney(totalActivitiesMoney.setScale(ConstantUtil.BIGDECIMAL_SCAL_MONEY, BigDecimal.ROUND_HALF_DOWN));
				dayProfit.setSystemAddMoney(totalSystemAddMoney.setScale(ConstantUtil.BIGDECIMAL_SCAL_MONEY, BigDecimal.ROUND_HALF_DOWN));
				dayProfit.setSystemReduceMoney(totalSystemReduceMoney.setScale(ConstantUtil.BIGDECIMAL_SCAL_MONEY, BigDecimal.ROUND_HALF_DOWN));
				dayProfit.setRechargeManual(totalRechargeManual.setScale(ConstantUtil.BIGDECIMAL_SCAL_MONEY, BigDecimal.ROUND_HALF_DOWN));
				dayProfit.setManageReduceMoney(totalManageReduceMoney.setScale(ConstantUtil.BIGDECIMAL_SCAL_MONEY, BigDecimal.ROUND_HALF_DOWN));
				// dayProfit.setDayBonus(totalDayBounsMoney);
				// dayProfit.setMonthSalary(totalMonthSalaryMoney);
				// dayProfit.setHalfMonthBonus(totalHalfMonthBounsMoney);
				// dayProfit.setDayCommission(totalDayCommissionMoney);
				// dayProfit.setDaySalary(totalDaySalaryMoney);
				// dayProfit.setDayActivity(totalActivitiesMoney);

				dayProfit.setTransfer(totalIncomeTranferMoney.setScale(ConstantUtil.BIGDECIMAL_SCAL_MONEY, BigDecimal.ROUND_HALF_DOWN));
				// 实际取现金额
				dayProfit.setWithdraw(totalWithdrawMoney.setScale(ConstantUtil.BIGDECIMAL_SCAL_MONEY, BigDecimal.ROUND_HALF_DOWN));
				// 取现手续费
				dayProfit.setWithdrawFee(totalWithdrawFeeMoney.setScale(ConstantUtil.BIGDECIMAL_SCAL_MONEY, BigDecimal.ROUND_HALF_DOWN));
				if(isInsertTable){//没插入数据库，不打印日志
				logger.info("资金明细统计充值金额：{},提现金额：{},提现手续费金额：{}", totalRechargeMoney, totalWithdrawMoney, totalWithdrawFeeMoney);
				}
				
				// 查询平台发放的分红与日工资
				String superUserName = UserNameUtil.getSuperUserNameByBizSystem(bizSystem);
				BigDecimal bonusOrderMoney = bonusOrderService.queryBonusOrderTotalMoney(bizSystem, superUserName, startDateTime, endDateTime);
				dayProfit.setHalfMonthBonus(bonusOrderMoney);
				BigDecimal daySalaryMoney = daySalaryOrderService.queryDaySalaryOrderTotalMoney(bizSystem, superUserName, startDateTime, endDateTime);
				dayProfit.setDaySalary(daySalaryMoney);

				// 投注盈利(投注-投注撤单-追号撤单-中奖额)
				BigDecimal realLottery = totalLotteryMoney.subtract(totalLotteryRegressionMoney).subtract(totaLotteryStopAfterNumberMoney);

				BigDecimal totalLotteryGain = realLottery.subtract(totalWinMoney);
				dayProfit.setLotteryGain(totalLotteryGain);
				// 赢率
				BigDecimal winGain = BigDecimal.ZERO;
				if (realLottery.compareTo(BigDecimal.ZERO) != 0) {
					winGain = realLottery.subtract(totalWinMoney).divide(realLottery, ConstantUtil.BIGDECIMAL_SCAL_MONEY, BigDecimal.ROUND_HALF_DOWN);
				}
				dayProfit.setWinGain(winGain.setScale(ConstantUtil.BIGDECIMAL_SCAL_MONEY, BigDecimal.ROUND_HALF_DOWN));

				// 资金盈利（存款总额-真实取款总额）
				dayProfit.setMoneyGain(totalRechargeMoney.subtract(totalWithdrawMoney));
				// 当日盈利（投注盈利-充值赠送-活动彩金-系统续费+行政提出+提现手续费-直属日工资-直属契约分红 - 投注返点总额） 现在不加人工误存
				BigDecimal totalGain = totalLotteryGain.subtract(totalRechargePresentMoney).subtract(totalActivitiesMoney).subtract(totalSystemAddMoney)
						//.add(totalSystemReduceMoney)
						.add(totalManageReduceMoney)//行政提出
						.add(totalWithdrawFeeMoney).subtract(totalHalfMonthBounsMoney).subtract(totalDaySalaryMoney)
						.subtract(daySalaryMoney).subtract(bonusOrderMoney)
						.subtract(totalPercentageMoney == null?BigDecimal.ZERO:totalPercentageMoney).subtract(totalRebateMoney == null?BigDecimal.ZERO:totalRebateMoney);
				dayProfit.setGain(totalGain);

				// 统计各资金明细的用户数
				List<MoneyDetail> userCountMoneyDetails = moneyDetailService.getUserCountByDetailType(moneyDetailQuery);
				Integer lotteryUserCount = 0;
				Integer winUserCount = 0;
				Integer lotteryRegressionUserCount = 0;
				Integer lotteryStopAfterNumberUserCount = 0;
				Integer rebateUserCount = 0;
				Integer percentageUserCount = 0;
				Integer rechargePresentUserCount = 0;
				Integer activitiesMoneyUserCount = 0;
				Integer systemAddMoneyUserCount = 0;
				Integer systemReduceMoneyUserCount = 0;
				Integer manageReduceMoneyUserCount = 0;
				Integer rechargeManualUserCount = 0;
				// 提现手续费人数 由资金明细统计
				Integer withdrawFeeUserCount = 0;
				if (CollectionUtils.isNotEmpty(userCountMoneyDetails)) {
					for (MoneyDetail md : userCountMoneyDetails) {
						if (md.getDetailType().equals(EMoneyDetailType.LOTTERY.name())) {
							lotteryUserCount = md.getUserCount();
						} else if (md.getDetailType().equals(EMoneyDetailType.WIN.name())) {
							winUserCount = md.getUserCount();
						} else if (md.getDetailType().equals(EMoneyDetailType.LOTTERY_REGRESSION.name())) {// 投注回退
							lotteryRegressionUserCount = md.getUserCount();
						} else if (md.getDetailType().equals(EMoneyDetailType.LOTTERY_STOP_AFTER_NUMBER.name())) {// 追号回退
							lotteryStopAfterNumberUserCount = md.getUserCount();
						} else if (md.getDetailType().equals(EMoneyDetailType.REBATE.name())) {
							rebateUserCount = md.getUserCount();
						} else if (md.getDetailType().equals(EMoneyDetailType.PERCENTAGE.name())) {
							percentageUserCount = md.getUserCount();
						} else if (md.getDetailType().equals(EMoneyDetailType.RECHARGE.name())) {
						} else if (md.getDetailType().equals(EMoneyDetailType.RECHARGE_PRESENT.name())) {
							rechargePresentUserCount = md.getUserCount();
						} else if (md.getDetailType().equals(EMoneyDetailType.ACTIVITIES_MONEY.name())) {
							activitiesMoneyUserCount = md.getUserCount();
						} else if (md.getDetailType().equals(EMoneyDetailType.SYSTEM_ADD_MONEY.name())) {
							systemAddMoneyUserCount = md.getUserCount();
						} else if (md.getDetailType().equals(EMoneyDetailType.SYSTEM_REDUCE_MONEY.name())) {
							systemReduceMoneyUserCount = md.getUserCount();
						} else if (md.getDetailType().equals(EMoneyDetailType.WITHDRAW.name())) {
						} else if (md.getDetailType().equals(EMoneyDetailType.WITHDRAW_FEE.name())) {
							withdrawFeeUserCount = md.getUserCount();
						} else if (md.getDetailType().equals(EMoneyDetailType.RECHARGE_MANUAL.name())) {//人工存款
							rechargeManualUserCount = md.getUserCount();
						} else if (md.getDetailType().equals(EMoneyDetailType.MANAGE_REDUCE_MONEY.name())) {//行政提出
							manageReduceMoneyUserCount = md.getUserCount();
						}else if (md.getDetailType().equals(EMoneyDetailType.INCOME_TRANFER.name())) {
						} else if (md.getDetailType().equals(EMoneyDetailType.PAY_TRANFER.name())) {
						} else if (md.getDetailType().equals(EMoneyDetailType.DAY_BOUNS.name())) {
						} else if (md.getDetailType().equals(EMoneyDetailType.HALF_MONTH_BOUNS.name())) {
						} else if (md.getDetailType().equals(EMoneyDetailType.DAY_COMMISSION.name())) {
						} else if (md.getDetailType().equals(EMoneyDetailType.DAY_SALARY.name())) {
						} else if (md.getDetailType().equals(EMoneyDetailType.MONTH_SALARY.name())) {
						}
					}
				}
				dayProfit.setLotteryUserCount(lotteryUserCount);
				dayProfit.setWinUserCount(winUserCount);
				dayProfit.setLotteryRegressionUserCount(lotteryRegressionUserCount);
				dayProfit.setLotteryStopAfterNumberUserCount(lotteryStopAfterNumberUserCount);
				dayProfit.setRebateUserCount(rebateUserCount);
				dayProfit.setPercentageUserCount(percentageUserCount);
				dayProfit.setRechargePresentUserCount(rechargePresentUserCount);
				dayProfit.setActivitiesMoneyUserCount(activitiesMoneyUserCount);
				dayProfit.setSystemAddMoneyUserCount(systemAddMoneyUserCount);
				dayProfit.setSystemReduceMoneyUserCount(systemReduceMoneyUserCount);
				dayProfit.setWithdrawFeeUserCount(withdrawFeeUserCount);
				dayProfit.setRechargeManualUserCount(rechargeManualUserCount);
				dayProfit.setManageReduceMoneyUserCount(manageReduceMoneyUserCount);
				// 查询有效投注单量
				Integer lotteryNumCount = orderService.getEffectiveLotterysByQueryPageCount(orderQuery);
				dayProfit.setLotteryNumCount(lotteryNumCount);

				// 根据代理类型统计用户余额总额
				List<User> totalByLevel = userService.getTotalMoneyByDailiLevel(bizSystem);
				BigDecimal totalAgencyMoney = BigDecimal.ZERO;
				BigDecimal totalMemberMoney = BigDecimal.ZERO;
				if (CollectionUtils.isNotEmpty(totalByLevel)) {
					Iterator<User> iterator = totalByLevel.iterator();
					while (iterator.hasNext()) {
						User user = iterator.next();
						if (EUserDailLiLevel.SUPERAGENCY.name().equals(user.getDailiLevel())) {// 超级管理员
							continue;
							// 玩家余额总额
						} else if (EUserDailLiLevel.REGULARMEMBERS.name().equals(user.getDailiLevel())) {
							totalMemberMoney = totalMemberMoney.add(user.getMoney());
							// 代理余额总额
						} else if (EUserDailLiLevel.DIRECTAGENCY.name().equals(user.getDailiLevel()) || EUserDailLiLevel.GENERALAGENCY.name().equals(user.getDailiLevel())
								|| EUserDailLiLevel.ORDINARYAGENCY.name().equals(user.getDailiLevel())) {
							totalAgencyMoney = totalAgencyMoney.add(user.getMoney());
						}
					}
				}
				dayProfit.setAgencyMoney(totalAgencyMoney.setScale(ConstantUtil.BIGDECIMAL_SCAL_MONEY, BigDecimal.ROUND_HALF_DOWN));
				dayProfit.setRegularmembersMoney(totalMemberMoney.setScale(ConstantUtil.BIGDECIMAL_SCAL_MONEY, BigDecimal.ROUND_HALF_DOWN));
				//查询非游客用户
	    		newUserQuery.setIsTourist(0);
				// 查询新注册用户的人数
				Integer newAddUserCount = userService.getNewUsersCount(newUserQuery);
				dayProfit.setNewaddUserCount(newAddUserCount);
				//首充人数
				Integer firstRechargeUserCount = userService.getUserCountByFirstRecharger(bizSystem, startDateTime, endDateTime,null);
				dayProfit.setFirstRechargeUserCount(firstRechargeUserCount);
				// 创建时间
				dayProfit.setCreatedDate(new Date());

				// 盈利归属时间 ：昨天
				dayProfit.setBelongDate(belongDate);
				logger.info("dayProfit bizSystem:" + dayProfit.getBizSystem());
				if (isInsertTable) {
					dayProfitService.insertSelective(dayProfit);
				}
				dayProfitTotal.addDayProfit(dayProfit);
			}

			
			return dayProfitTotal;
		} catch (Exception e) {
			//e.printStackTrace();
			if(isInsertTable){//没插入数据库，不打印日志
			logger.error("平台盈亏插入失败：" + e);
			}
			throw new RuntimeException(e);
		}

	}

}
