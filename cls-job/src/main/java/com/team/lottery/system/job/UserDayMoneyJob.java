package com.team.lottery.system.job;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.UserQuery;
import com.team.lottery.service.UserDayMoneyService;
import com.team.lottery.service.UserService;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.DateUtil;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserDayMoney;

@Component("userDayMoneyJob")
public class UserDayMoneyJob extends QuartzJob {

	private static Logger logger = LoggerFactory.getLogger(UserDayMoneyJob.class);
	
	@Autowired
	private UserService userService;
	@Autowired
	private UserDayMoneyService userDayMoneyService;
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=false,rollbackFor=Exception.class)
	@Override
	public void execute() {
		try {
			
			logger.info("开始计算所有用户团队余额数据...");
			Long startDealTime = System.currentTimeMillis();
			
			Calendar calendar = Calendar.getInstance();
			Date yesterday = calendar.getTime();
			if(nowTime != null) {
				calendar.setTime(nowTime);
			}  else {
				//获取昨天的日期
				calendar.add(Calendar.DAY_OF_MONTH, -1);
			}
			yesterday = calendar.getTime();
			Date belongDateTime = DateUtil.getNowStartTimeByEnd(yesterday);
			logger.info("计算的时间["+DateUtil.parseDate(yesterday, "yyyy-MM-dd HH:mm:ss")+"]");
			
			//查询用户数据
			logger.info("开始查询所有的用户数据...");
			/*UserQuery userquery = new UserQuery();
			userquery.setState(null);
	   		List<User> allUsers = userService.getUsersByCondition(userquery);*/
	   		Map<String, User> allUsersMap = new HashMap<String, User>();
	   		Map<String, UserDayMoney> userDayMoneyMap = new HashMap<String, UserDayMoney>();
	   		String [] regFromUserNames = null;
	   		
	   		UserQuery userquery = new UserQuery();
	   		userquery.setQueryBySscRebate(1);
	   		userquery.setState(null);  //加载所有状态的用户
	   		Page page = new Page(1000);   //处理用户单次处理1000条数据
	   		int pageNo = 1;
	    	page.setPageNo(pageNo);
	    	logger.info("获取第一页用户数据进行处理，当前第["+pageNo+"]页...");
	   		userService.getAllUsersByQueryPage(userquery, page);
	   		List<User> users = (List<User>)page.getPageContent();
	   		while(CollectionUtils.isNotEmpty(users)) {
	   			for(User user : users) {
	   				//用户余额为零不添加处理
	   				if(user.getMoney().compareTo(BigDecimal.ZERO) == 0) {
	   					continue;
	   				}
	   				allUsersMap.put(user.getUserName(), user);
	   				UserDayMoney userDayMoney = new UserDayMoney();
	   				userDayMoney.setUserId(user.getId());
	   				userDayMoney.setUserName(user.getUserName());
	   				userDayMoney.setRegfrom(user.getRegfrom());
	   				userDayMoney.setMoney(user.getMoney());
	   				userDayMoney.setTeamMoney(user.getMoney());
	   				userDayMoney.setBelongDate(belongDateTime);
	   				userDayMoney.setCreateDate(new Date());
	   				userDayMoneyMap.put(user.getUserName(), userDayMoney);
	   			}
	   			
	   			//用户数据小于1000,已经是最后一页了
	   			if(users.size() < 1000) {
	   				logger.info("处理最后一页用户数据");
	   				break;
	   			} else {
	   				//获取下一页数据
	   				pageNo++;
	   				logger.info("获取下一页用户数据进行处理，当前第["+pageNo+"]页...");
	   				page.setPageNo(pageNo);
	   				userService.getAllUsersByQueryPage(userquery, page);
	   				users = (List<User>)page.getPageContent();
	   			}
	   		}
	   		
	   		//保存进数据库 
	   		Iterator<String> usersIterator = allUsersMap.keySet().iterator();
			while(usersIterator.hasNext()) {
				String key = usersIterator.next();
				User user = allUsersMap.get(key);
				regFromUserNames = user.getRegfrom().split(ConstantUtil.REG_FORM_SPLIT);
       			for(String regFromUserName : regFromUserNames){
       				if(regFromUserName.equals(ConstantUtil.FRONT_ADMIN_USER_NAME)){
       				   continue;	
       				}
       				UserDayMoney userDayMoney = userDayMoneyMap.get(regFromUserName);
       				//累计金额
       				if(userDayMoney != null) {
       					userDayMoney.setTeamMoney(userDayMoney.getTeamMoney().add(user.getMoney()));
       				}
       			} 
			}
	   		
	   		/*if(CollectionUtils.isNotEmpty(allUsers)) {
	   			//第一次遍历放入map
	   			for(User user : allUsers) {
	   				allUsersMap.put(user.getUsername(), user);
	   				UserDayMoney userDayMoney = new UserDayMoney();
	   				userDayMoney.setUserId(user.getId());
	   				userDayMoney.setUserName(user.getUsername());
	   				userDayMoney.setRegfrom(user.getRegfrom());
	   				userDayMoney.setMoney(user.getMoney());
	   				userDayMoney.setTeamMoney(user.getMoney());
	   				userDayMoney.setBelongDate(belongDateTime);
	   				userDayMoney.setCreateDate(new Date());
	   				userDayMoneyMap.put(user.getUsername(), userDayMoney);
	   			}
	   			logger.info("查询到所有的用户数据记录数为["+allUsers.size()+"]...");
	   			//第二次遍历  资金拷贝
	   			for(User user : allUsers) {
	   				regFromUserNames = user.getRegfrom().split(ConstantUtil.REG_FORM_SPLIT);
	       			for(String regFromUserName : regFromUserNames){
	       				if(regFromUserName.equals(ConstantUtil.ADMIN_USER_NAME)){
	       				   continue;	
	       				}
	       				UserDayMoney userDayMoney = userDayMoneyMap.get(regFromUserName);
	       				//累计金额
	       				if(userDayMoney != null) {
	       					userDayMoney.setTeamMoney(userDayMoney.getTeamMoney().add(user.getMoney()));
	       				}
	       			} 
	   			}
	   		}*/
			
	   		//保存进数据库 
	   		Iterator<String> keyIterator = userDayMoneyMap.keySet().iterator();
			while(keyIterator.hasNext()) {
				String key = keyIterator.next();
				UserDayMoney userDayMoney = userDayMoneyMap.get(key);
				userDayMoneyService.insertSelective(userDayMoney);
			}
	   		
			Long endDealTime = System.currentTimeMillis();
			logger.info("计算所有用户团队余额数据结束,共耗时["+(endDealTime - startDealTime)+"]ms...");
		} catch (Exception e) {
			logger.error("计算所有用户团队余额数据错误...",e);
			throw new RuntimeException(e);
		}
	}
	
	
	/**
	 * 根据起始日期和要计算的天数，重新计算用户团队余额数据
	 * @param startDate
	 * @param days 表示要计算的天数，如果为1表示要计算的是startDate那天的数据
	 */
	public void reCalcuteUserDayMoney(Date startDate, int days) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		logger.info("重新计算所有用户团队余额数据  起始时间["+DateUtil.parseDate(startDate, "yyyy-MM-dd HH:mm:ss")+"],计算天数["+ days +"]");
		
		
		calendar.setTime(startDate);
		for(int i = 0; i < days; i++) {
			Date calcuteDate = calendar.getTime();
			
			//计算日期判断
			if(startDate.after(calcuteDate)) {
				logger.info("执行所有用户团队余额计算起始时间不能在今天之后,结束计算...");
				return;
			}
			
			this.setNowTime(calcuteDate);
			this.execute();
			
			calendar.add(Calendar.DAY_OF_MONTH, 1);
		}
		
	}

}
