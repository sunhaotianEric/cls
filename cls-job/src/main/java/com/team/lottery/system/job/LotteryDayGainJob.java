package com.team.lottery.system.job;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.enums.EProstateStatus;
import com.team.lottery.extvo.OrderQuery;
import com.team.lottery.service.BizSystemService;
import com.team.lottery.service.LotteryDayGainService;
import com.team.lottery.service.OrderService;
import com.team.lottery.util.ClsDateHelper;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.DateUtil;
import com.team.lottery.vo.BizSystem;
import com.team.lottery.vo.LotteryDayGain;
import com.team.lottery.vo.Order;

/**
 * 执行每日彩种投注统计
 * @deprecated 改用别的方式实现，实时计算
 * @author gs
 *
 */
@Component("lotteryDayGainJob")
public class LotteryDayGainJob extends QuartzJob {
	
	private static Logger logger = LoggerFactory.getLogger(LotteryDayGainJob.class);
	
	@Autowired
	private BizSystemService bizSystemService;
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private LotteryDayGainService lotteryDayGainService;

	@Transactional(readOnly=false,rollbackFor=Exception.class)
	public void execute() {
		try {
			logger.info("开始执行每日彩种投注统计计算...");
			Long startLongTime = System.currentTimeMillis();
			
			Calendar calendar = Calendar.getInstance();
			Date yesterday = calendar.getTime();
			if(nowTime != null) {
				calendar.setTime(nowTime);
			} else {
				//获取昨天的日期
				calendar.add(Calendar.DATE, -1);
			}
			
			yesterday = calendar.getTime();
			Date startDateTime = ClsDateHelper.getClsStartTime(yesterday);
			Date endDateTime = ClsDateHelper.getClsEndTime(yesterday);
			logger.info("计算的开始时间：[{}]，结束时间：[{}]",DateUtil.parseDate(startDateTime, "yyyy-MM-dd HH:mm:ss"),DateUtil.parseDate(endDateTime, "yyyy-MM-dd HH:mm:ss"));
			
			//设置归属时间置为昨天的12:00:00
			Date belongDate = ClsDateHelper.getClsMiddleTime(yesterday);
			
			List<BizSystem> bizList = new ArrayList<BizSystem>();
		    //this.bizSystem有值，单业务系统
			if(StringUtils.isBlank(this.bizSystem)){
				//查询所有业务系统
				 bizList=bizSystemService.getAllBizSystem();
			}else{
				 BizSystem biz=new BizSystem();
				biz.setBizSystem(this.bizSystem);
				biz=bizSystemService.getBizSystemByCondition(biz);
				bizList.add(biz);
			}
			
			for(BizSystem bz : bizList) {
				//超级系统不参与生成数据
				if(ConstantUtil.SUPER_SYSTEM.equals(bz.getBizSystem())) {
					continue;
				}
				OrderQuery query = new OrderQuery();
				query.setCreatedDateStart(startDateTime);
				query.setCreatedDateEnd(endDateTime);
				query.setBizSystem(bz.getBizSystem());
				//有效订单
				query.setEnabled(1);
				//遍历有显示的彩种,构建保存数据对象
				ELotteryKind[] lotteryKinds = ELotteryKind.values();
				List<LotteryDayGain> lotteryDayGains = new ArrayList<LotteryDayGain>();
				for(ELotteryKind lotteryKind : lotteryKinds) {
					//只统计有显示的彩种
					if(lotteryKind.getIsShow() == 1) {
						LotteryDayGain lotteryDayGain = new LotteryDayGain();
						lotteryDayGain.setBizSystem(bz.getBizSystem());
						lotteryDayGain.setLotteryType(lotteryKind.getCode());
						lotteryDayGain.setBelongDate(belongDate);
						lotteryDayGain.setPayMoney(BigDecimal.ZERO);
						lotteryDayGain.setNotWinPayMoney(BigDecimal.ZERO);
						lotteryDayGain.setWinPayMoney(BigDecimal.ZERO);
						lotteryDayGain.setWinMoney(BigDecimal.ZERO);
						lotteryDayGain.setWinGain(BigDecimal.ZERO);
						lotteryDayGain.setCreateTime(new Date());
						lotteryDayGains.add(lotteryDayGain);
					}
				}
				//构建Map对象
				Map<String, LotteryDayGain> lotteryDayGainMap = new HashMap<String, LotteryDayGain>();
				for(LotteryDayGain lotteryDayGain : lotteryDayGains) {
					lotteryDayGainMap.put(lotteryDayGain.getLotteryType(), lotteryDayGain);
				}
				
				//数据库查询各彩种处理类型总额
				List<Order> orders = orderService.getLotteryDayGain(query);
				if(CollectionUtils.isNotEmpty(orders)) {
					for(Order order : orders) {
						LotteryDayGain lotteryDayGain = lotteryDayGainMap.get(order.getLotteryType());
						if(lotteryDayGain != null) {
							//未中奖
							if(EProstateStatus.NOT_WINNING.getCode().equals(order.getProstate())) {
								lotteryDayGain.setNotWinPayMoney(lotteryDayGain.getNotWinPayMoney().add(order.getPayMoney()));
								lotteryDayGain.setPayMoney(lotteryDayGain.getPayMoney().add(order.getPayMoney()));
							//中奖
							} else if(EProstateStatus.WINNING.getCode().equals(order.getProstate())) {
								lotteryDayGain.setWinPayMoney(lotteryDayGain.getWinPayMoney().add(order.getPayMoney()));
								lotteryDayGain.setPayMoney(lotteryDayGain.getPayMoney().add(order.getPayMoney()));
								lotteryDayGain.setWinMoney(lotteryDayGain.getWinMoney().add(order.getWinCost()));
							}
						}
					}
				}
				
				//计算盈利，赢率
				for(LotteryDayGain lotteryDayGain : lotteryDayGains) {
					//盈利  投注-中奖
					lotteryDayGain.setGain(lotteryDayGain.getPayMoney().subtract(lotteryDayGain.getWinMoney()));
					//赢率  盈利/投注
					if(lotteryDayGain.getPayMoney().compareTo(BigDecimal.ZERO) != 0) {
						lotteryDayGain.setWinGain(lotteryDayGain.getGain().divide(lotteryDayGain.getPayMoney(),2, BigDecimal.ROUND_HALF_UP));
					} else {
						lotteryDayGain.setWinGain(BigDecimal.ZERO);
					}
					lotteryDayGainService.insertSelective(lotteryDayGain);
				}
			}
			
			Long endLongTime = System.currentTimeMillis();
			logger.info("执行每日彩种投注统计计算计算结束,耗时["+(endLongTime - startLongTime)+"]ms...");
		} catch (Exception e) {
			logger.error("执行每日彩种投注统计计算计算出现错误", e);
			throw new RuntimeException(e.getMessage());
		}
		
	}

	/**
	 * 手动重新计算赢率
	 * @param startDate
	 * @param days
	 */
	public void reCalcuteDayProfit(Date startDate, int days){
		
		logger.info("重新计算每日赢率数据  起始时间["+DateUtil.parseDate(startDate, "yyyy-MM-dd HH:mm:ss")+"],计算天数["+ days +"]");
	
		
		//获得当天0小时0分0秒的时间
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		Date nowDayTime = calendar.getTime();
		
		//设置为开始计算的时间
		calendar.setTime(startDate);
		//设置为开始计算的时间
		calendar.setTime(startDate);
		for(int i = 0; i < days; i++) {
			Date calcuteDate = calendar.getTime();
			
			//计算日期判断
			if(calcuteDate.after(nowDayTime)) {
				logger.info("执行计算每日赢率数据起始时间不能在今天之后,结束计算...");
				return;
			}
			
			this.setNowTime(calcuteDate);
			this.execute();
			
			calendar.add(Calendar.DAY_OF_MONTH, 1);
		}
		
	}
	
	public BizSystemService getBizSystemService() {
		return bizSystemService;
	}

	public void setBizSystemService(BizSystemService bizSystemService) {
		this.bizSystemService = bizSystemService;
	}

	public OrderService getOrderService() {
		return orderService;
	}

	public void setOrderService(OrderService orderService) {
		this.orderService = orderService;
	}

	public LotteryDayGainService getLotteryDayGainService() {
		return lotteryDayGainService;
	}

	public void setLotteryDayGainService(LotteryDayGainService lotteryDayGainService) {
		this.lotteryDayGainService = lotteryDayGainService;
	}
	
}
