package com.team.lottery.system.job;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.enums.EBonusOrderStatus;
import com.team.lottery.enums.EDaySalaryOrderStatus;
import com.team.lottery.enums.EMoneyDetailType;
import com.team.lottery.enums.ENoteStatus;
import com.team.lottery.enums.ENoteType;
import com.team.lottery.enums.EReleasePolicy;
import com.team.lottery.enums.EUserDailLiLevel;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.DaySalaryConfigVo;
import com.team.lottery.extvo.SalaryOrderQuery;
import com.team.lottery.extvo.UserConsumeReportVo;
import com.team.lottery.extvo.UserDayConsumeQuery;
import com.team.lottery.extvo.UserQuery;
import com.team.lottery.service.BizSystemService;
import com.team.lottery.service.DaySalaryConfigService;
import com.team.lottery.service.DaySalaryOrderService;
import com.team.lottery.service.MoneyDetailService;
import com.team.lottery.service.NotesService;
import com.team.lottery.service.UserDayConsumeService;
import com.team.lottery.service.UserMoneyTotalService;
import com.team.lottery.service.UserService;
import com.team.lottery.system.BizSystemConfigManager;
import com.team.lottery.system.BizSystemConfigVO;
import com.team.lottery.system.UserVersionExceptionThread;
import com.team.lottery.util.ClsDateHelper;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.MakeOrderNum;
import com.team.lottery.util.UserNameUtil;
import com.team.lottery.vo.BizSystem;
import com.team.lottery.vo.DaySalaryConfig;
import com.team.lottery.vo.DaySalaryOrder;
import com.team.lottery.vo.MoneyDetail;
import com.team.lottery.vo.Notes;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserDayConsume;

/**
 * 执行日工资计算job
 * @author gs
 *
 */
@Component("daySalaryJob")
public class DaySalaryJob extends QuartzJob {

	private static Logger logger = LoggerFactory.getLogger(DaySalaryJob.class);
	
	@Autowired
	private UserService userService;
	@Autowired
	private UserDayConsumeService userDayConsumeService;
	@Autowired
	private DaySalaryConfigService daySalaryConfigService;
	@Autowired
	private DaySalaryOrderService daySalaryOrderService;
	@Autowired
	private MoneyDetailService moneyDetailService;
	@Autowired
	private UserMoneyTotalService userMoneyTotalService;
	@Autowired
	private BizSystemService bizSystemService;
	@Autowired
	private NotesService notesService;
	
	
	@Transactional(readOnly=false,rollbackFor=Exception.class)
	public void execute() {
		try {
			logger.info("开始执行日工资计算...");
			Long startLongTime = System.currentTimeMillis();
			Calendar calendar = Calendar.getInstance();
			if (nowTime != null) {
				calendar.setTime(nowTime);
			} else {
				// 获取前一天的日期
				calendar.add(Calendar.DATE, -1);
			}
			
			Date yesterday = calendar.getTime();
			Date startTime = ClsDateHelper.getClsStartTime(yesterday);
			Date endTime = ClsDateHelper.getClsEndTime(yesterday);
			logger.info("计算的开始时间：[{}]，结束时间：[{}]",DateUtil.parseDate(startTime, "yyyy-MM-dd HH:mm:ss"),DateUtil.parseDate(endTime, "yyyy-MM-dd HH:mm:ss"));

			// 归属时间
			Date belongDate = ClsDateHelper.getClsMiddleTime(yesterday);

			List<BizSystem> bizList = new ArrayList<BizSystem>();
			// this.bizSystem有值，单业务系统
			if (this.bizSystem == null || "".equals(this.bizSystem)) {
				// 查询所有业务系统
				bizList = bizSystemService.getAllBizSystem();
			} else {
				BizSystem biz = new BizSystem();
				biz.setBizSystem(this.bizSystem);
				biz = bizSystemService.getBizSystemByCondition(biz);
				bizList.add(biz);
			}
			
			for (BizSystem bizSystem : bizList) {
				// 禁用的系统不处理
				if (bizSystem.getEnable() == 0) {
					continue;
				}
				
				// 查看系统是否已生成数据了
				SalaryOrderQuery salaryOrderQuery = new SalaryOrderQuery();
				salaryOrderQuery.setBelongDate(belongDate);
				salaryOrderQuery.setBizSystem(bizSystem.getBizSystem());
				List<DaySalaryOrder> daySalaryOrders = daySalaryOrderService.querySalaryOrdersByCondition(salaryOrderQuery);
				if(CollectionUtils.isNotEmpty(daySalaryOrders)) {
					logger.info("查到业务系统[{}]数据已经生成，不继续执行！", bizSystem.getBizSystem());
					continue;
				}
				
				// 查询有日工资设定的用户
				UserQuery userQuery = new UserQuery();
				userQuery.setIsQueryHasSalary(true);
				userQuery.setBizSystem(bizSystem.getBizSystem());
				List<User> dealUsers = userService.getUsersByCondition(userQuery);
				
				UserDayConsumeQuery query = new UserDayConsumeQuery();
				query.setCreatedDateStart(startTime);
				query.setCreatedDateEnd(endTime);
				query.setBizSystem(bizSystem.getBizSystem());
				logger.info("查询业务系统[{}]日工资数据开始时间：{} ,结束时间：{}", bizSystem.getBizSystem(), 
						DateUtil.getDate(startTime, "yyyy-MM-dd HH:mm:ss"), DateUtil.getDate(endTime, "yyyy-MM-dd HH:mm:ss"));
				Map<String, UserConsumeReportVo> daySalaryConsume = getUserConsumeOfDaySalary(query, dealUsers);
				
				// 生成订单
				int orderCount = this.makeDaySalaryOrder(dealUsers, daySalaryConsume, belongDate);
				logger.info("业务系统[{}]生成日工资订单数[{}]", bizSystem.getBizSystem(), orderCount);
				
				
				// 根据map的key值取相应的日工资分发策略,如果该业务系统没有设置日工资分发策略,就取默认值!
				String daySaralyPolicy = EReleasePolicy.MANANUAL_RELEASE.getCode();
				BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(bizSystem.getBizSystem());
				if (bizSystemConfigVO != null) {
					if (bizSystemConfigVO.getReleasePolicyDaySaraly() != null && !"".equals(bizSystemConfigVO.getReleasePolicyDaySaraly())) {
						daySaralyPolicy = bizSystemConfigVO.getReleasePolicyDaySaraly();
					}
				}
				if (this.releasePolicy != null && !"".equals(releasePolicy)) {
					daySaralyPolicy = this.releasePolicy;
				}
				// 是否同时发放日工资
				if (this.isReleaseMoney) {
					logger.info("业务系统[{}],当前发放策略为[{}]", bizSystem.getBizSystem(), daySaralyPolicy);
					//手动发放不进行任何操作
					if (!EReleasePolicy.MANANUAL_RELEASE.name().equals(daySaralyPolicy)) {
						User adminUser = userService.getUserByName(bizSystem.getBizSystem(), UserNameUtil.getSuperUserNameByBizSystem(bizSystem.getBizSystem()));
						this.settleMoney(adminUser, daySaralyPolicy, belongDate);
					} 
				}
			}

			Long endLongTime = System.currentTimeMillis();
			logger.info("执行日工资计算结束,耗时[" + (endLongTime - startLongTime) + "]ms...");
		} catch (Exception e) {
			logger.info("执行日工资计算出现错误", e);
			throw new RuntimeException(e.getMessage());
		}
	}
	
	/**
	 * 生成订单记录
	 * @param dealUsers
	 * @param daySalaryConsume
	 * @param belongDate
	 * @return
	 */
 	private int makeDaySalaryOrder(List<User> dealUsers, Map<String, UserConsumeReportVo> daySalaryConsume, Date belongDate) {
 		
 		//生成订单数量
		int count = 0;
		for (User user : dealUsers) {
			//获取投注额（日量）
			UserConsumeReportVo userDayConsume = daySalaryConsume.get(user.getUserName());
			BigDecimal lottery = BigDecimal.ZERO;
			if(userDayConsume != null) {
				lottery = userDayConsume.getLottery();
			}
			
			DaySalaryConfigVo daySalaryConfigVo = new DaySalaryConfigVo();
			daySalaryConfigVo.setUserId(user.getId());
			List<DaySalaryConfig> daySalaryConfigList = daySalaryConfigService.getAllDaySalaryConfig(daySalaryConfigVo);
			if (daySalaryConfigList != null && daySalaryConfigList.size() > 0) {
				String type = daySalaryConfigList.get(0).getType();
				Integer state = daySalaryConfigList.get(0).getState();
				
				//有配置日工资设置才插入记录
				DaySalaryOrder daySalaryOrder = new DaySalaryOrder();
				daySalaryOrder.setBelongDate(belongDate);
				daySalaryOrder.setBizSystem(user.getBizSystem());
				daySalaryOrder.setCreatedDate(new Date());
				daySalaryOrder.setFromUserId(daySalaryConfigList.get(0).getCreateUserId());
				daySalaryOrder.setFromUserName(daySalaryConfigList.get(0).getCreateUserName());
				daySalaryOrder.setUserId(user.getId());
				daySalaryOrder.setUserName(user.getUserName());
				daySalaryOrder.setUpdateDate(new Date());
				
				// 如果当前用户的日工资配置为启用状态向日工资表插入一条记录
				if (state == 1) {
					BigDecimal money = BigDecimal.ZERO;
					// 如果当前用户的日工资配置类型为固定配置
					if (type.equals("FIXED")) {
						money = lottery.divide(BigDecimal.valueOf(10000))
								.multiply(daySalaryConfigList.get(0).getValue());
						daySalaryOrder.setRemark("固定比例模式，每一万日量" + daySalaryConfigList.get(0).getValue().intValue() + "元");
						
					}
					// 如果当前用户的日工资配置类型为消费比例
					else if (type.equals("COMSUME")) {
						int i = 0;
						for (DaySalaryConfig daySalaryConfig : daySalaryConfigList) {
							if (lottery.compareTo(daySalaryConfig.getScale()) >= 0) {
								BigDecimal returnMoney = daySalaryConfigList.get(i).getValue().multiply(new BigDecimal("100"));
								money = lottery.multiply(daySalaryConfigList.get(i).getValue());
								daySalaryOrder.setRemark("日量达到" + daySalaryConfig.getScale().intValue() + "私返" + returnMoney.intValue() + "%");
								i++;
							}
						}
					}
					
					daySalaryOrder.setLotteryMoney(lottery);
					daySalaryOrder.setMoney(money);
					daySalaryOrder.setSerialNumber(MakeOrderNum.makeOrderNum(MakeOrderNum.GZ));
					if(money.compareTo(BigDecimal.ZERO) == 0) {
						daySalaryOrder.setReleaseStatus(EDaySalaryOrderStatus.NEEDNOT_RELEASE.getCode());
					} else {
						daySalaryOrder.setReleaseStatus(EDaySalaryOrderStatus.NOT_RELEASE.getCode());
					}
					daySalaryOrderService.insertSelective(daySalaryOrder);
					count++;
				} else {
					// 如果当前用户的日工资配置为停用状态也向日工资表插入一条记录
					daySalaryOrder.setLotteryMoney(lottery);
					daySalaryOrder.setMoney(BigDecimal.ZERO);
					daySalaryOrder.setSerialNumber(MakeOrderNum.makeOrderNum(MakeOrderNum.GZ));
					daySalaryOrder.setReleaseStatus(EDaySalaryOrderStatus.NEEDNOT_RELEASE.getCode());
					daySalaryOrder.setRemark("日工资设置为停用状态");
					daySalaryOrderService.insertSelective(daySalaryOrder);
					count++;
				}
			}
			
		}
		return count;
	}


	/**
   	 * 遍历查询获取所有用户的消费盈利情况
   	 * @param query
   	 * @return
   	 */
   	public Map<String,UserConsumeReportVo> getUserConsumeOfDaySalary(UserDayConsumeQuery query ,List<User> users){ 
   		
   		Map<String,UserConsumeReportVo> consumeReportMaps = new HashMap<String, UserConsumeReportVo>();
   		for(User user : users) {
   			UserConsumeReportVo userConsumeReportVo = null;
   			
   			UserDayConsumeQuery dayConsumeQuery = new UserDayConsumeQuery();
   	   		dayConsumeQuery.setCreatedDateStart(query.getCreatedDateStart());
   	   		dayConsumeQuery.setCreatedDateEnd(query.getCreatedDateEnd());
   	   		dayConsumeQuery.setUserName(user.getUserName());
   	   		dayConsumeQuery.setBizSystem(query.getBizSystem());
   	   		List<UserDayConsume> userDayConsumes = userDayConsumeService.getUserDayConsumeByCondition(dayConsumeQuery);
   	   		if(CollectionUtils.isNotEmpty(userDayConsumes)) {
   	   			if(userDayConsumes.size() > 1) {
   	   				logger.error("查询用户的团队消费数据有误，查询结果大于2条");
   	   			}
   	   			UserDayConsume userDayConsume = userDayConsumes.get(0);
   	   			userConsumeReportVo = new UserConsumeReportVo(user.getUserName());
   	   			userConsumeReportVo.setUserId(user.getId());
   	   			userConsumeReportVo.setGain(userDayConsume.getGain());
   	   			userConsumeReportVo.setLottery(userDayConsume.getLottery());
   	   			userConsumeReportVo.setBizSystem(userDayConsume.getBizSystem());
   	   			userConsumeReportVo.setRegfrom(userDayConsume.getRegfrom());
   	   			
   	   		}
   	   		
   			if(userConsumeReportVo != null) {
   				consumeReportMaps.put(user.getUserName(), userConsumeReportVo);
   			}
   		}
   		
        return consumeReportMaps;
        
   	}
   	
   	
	/**
   	 * 递归处理日工资
   	 * @param list 用户,
   	 * @param releasePolicy 结算策略
   	 * @param daiLiType 代理类型
   	 * @throws Exception 
   	 */
	private void settleMoney(User parentUser, String releasePolicy, Date belongDate) {
		logger.info("开始处理业务系统[{}],用户名[{}]的下级日工资记录", parentUser.getBizSystem(), parentUser.getUserName());
		
		//查找下级有工资设置的记录
		DaySalaryConfigVo daySalaryConfigVo = new DaySalaryConfigVo();
		daySalaryConfigVo.setBizSystem(parentUser.getBizSystem());
		daySalaryConfigVo.setCreateUserId(parentUser.getId());
		List<DaySalaryConfig> daySalaryConfigList = daySalaryConfigService.getAllDaySalaryConfig(daySalaryConfigVo);
		if(CollectionUtils.isEmpty(daySalaryConfigList)) {
			return;
		}
		Set<String> subUserNames = new HashSet<String>();
		for(DaySalaryConfig daySalaryConfig : daySalaryConfigList) {
			subUserNames.add(daySalaryConfig.getUserName());
		}
		
		
		//所有要递归处理的下级用户
		List<User> subUsers = new ArrayList<User>();
		for(String userName : subUserNames) {
			User subUser = userService.getUserByName(parentUser.getBizSystem(), userName);
			if(subUser != null) {
				subUsers.add(subUser);
			}
		}
		
		//开始处理下级的日工资订单
		SalaryOrderQuery salaryOrderQuery = new SalaryOrderQuery();
		salaryOrderQuery.setBizSystem(parentUser.getBizSystem());
		salaryOrderQuery.setFromUserId(parentUser.getId());
		salaryOrderQuery.setBelongDate(belongDate);
		//只查找未发放的订单
		salaryOrderQuery.setReleaseStatus(EDaySalaryOrderStatus.NOT_RELEASE.getCode());
		List<DaySalaryOrder> subDaySalaryOrders = daySalaryOrderService.querySalaryOrdersByCondition(salaryOrderQuery);
		if(CollectionUtils.isNotEmpty(subDaySalaryOrders)) {
			for(DaySalaryOrder daySalaryOrder : subDaySalaryOrders) {
				User user = userService.selectByPrimaryKey(daySalaryOrder.getUserId());
				// 是超级管理员的，进行发放处理，不扣减超级管理员余额
				if(EUserDailLiLevel.SUPERAGENCY.getCode().equals(parentUser.getDailiLevel())) {
					
					MoneyDetail dailiUserMoneyDetail = new MoneyDetail();
					dailiUserMoneyDetail.setEnabled((user.getIsTourist() == 1 ? 0 : 1));
					dailiUserMoneyDetail.setUserId(user.getId()); // 用户id
					dailiUserMoneyDetail.setOrderId(null); // 订单id
					dailiUserMoneyDetail.setBizSystem(user.getBizSystem());
					dailiUserMoneyDetail.setUserName(user.getUserName()); // 用户名
					dailiUserMoneyDetail.setOperateUserName(ConstantUtil.SYSTEM_OPERATE_NAME); // 操作人员
					dailiUserMoneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.GZ)); // 订单单号
					dailiUserMoneyDetail.setWinLevel(null);
					dailiUserMoneyDetail.setWinCost(null);
					dailiUserMoneyDetail.setWinCostRule(null);
					dailiUserMoneyDetail.setDetailType(EMoneyDetailType.DAY_SALARY.name());
					dailiUserMoneyDetail.setIncome(daySalaryOrder.getMoney());
					dailiUserMoneyDetail.setPay(new BigDecimal(0));
					dailiUserMoneyDetail.setBalanceBefore(user.getMoney());
					dailiUserMoneyDetail.setBalanceAfter(user.getMoney().add(daySalaryOrder.getMoney())); // 支出后的金额
					dailiUserMoneyDetail.setDescription("日工资收入:" + daySalaryOrder.getMoney());
					
					user.addMoney(daySalaryOrder.getMoney(), true);
					int res = 0;
					try {
						res = userService.updateByPrimaryKeyWithVersionSelective(user);
					} catch (UnEqualVersionException e) {
						logger.error("日工资处理发现用户表版本号不一致，开启新线程处理", e);
						UserVersionExceptionThread thread = new UserVersionExceptionThread(user.getId(), daySalaryOrder.getMoney(), UserVersionExceptionThread.ADD_TYPE, true, dailiUserMoneyDetail);
						thread.start();
					}
					if(res == 1) {
						moneyDetailService.insertSelective(dailiUserMoneyDetail); // 保存资金明细
					}
					
					try {
						userMoneyTotalService.addUserTotalMoneyByType(user, EMoneyDetailType.DAY_SALARY, daySalaryOrder.getMoney());
					} catch (Exception e) {
						logger.error(e.getMessage());
					}

					// 更新日工资发放的状态值
					daySalaryOrder.setReleaseStatus(EBonusOrderStatus.RELEASED.name());
					daySalaryOrder.setReleaseDate(new Date());
					daySalaryOrder.setUpdateDate(new Date());
					daySalaryOrderService.updateByPrimaryKeySelective(daySalaryOrder);
				} else {
					//非超级管理员的日工资发放，先从发放者扣除余额，不够发放则停止
					User newParentUser = userService.selectByPrimaryKey(parentUser.getId());
					//金额不够，提现锁定当前用户
					if(newParentUser.getMoney().compareTo(daySalaryOrder.getMoney()) < 0) {
						logger.info("自动逐级发放--发放余额不足,将对" + newParentUser.getUserName() + "进行提现锁定！也影响其他级代理用户发放！");
						User updateUser = new User();
						updateUser.setId(parentUser.getId());
						updateUser.setWithdrawLock(1);
						userService.updateByPrimaryKeySelective(updateUser);
						
						// 自动逐级发放,当发到到某一个用户的余额不足，就直接停止发放(包括它的下级的下级之类用户)，并对他进行提现锁定
						if (releasePolicy.equals(EReleasePolicy.AUTO_DOWN_RELEASE.name())) {
							//移除用户的递归处理
							User removeUser = null;
							for(User user2 : subUsers) {
								if(user2.getId().equals(daySalaryOrder.getUserId())) {
									removeUser = user2;
									break;
								}
							}
							if(removeUser != null);
							subUsers.remove(removeUser);
						// 自动优先发放当发到到某一个用户的余额不足，就直接停止发放(不影响它的下级的下级之类用户发放)，并对他进行提现锁定
						} else if (releasePolicy.equals(EReleasePolicy.AUTO_ACCOUNT_FIRST_RELEASE.name())) {
							
						}
						
					}
					// 当前日工资，父级用户余额扣减
					MoneyDetail dailiUserMoneyDetail = new MoneyDetail();
					dailiUserMoneyDetail.setEnabled((parentUser.getIsTourist() == 1 ? 0 : 1));
					dailiUserMoneyDetail.setUserId(parentUser.getId()); // 用户id
					dailiUserMoneyDetail.setOrderId(null); // 订单id
					dailiUserMoneyDetail.setBizSystem(parentUser.getBizSystem());
					dailiUserMoneyDetail.setUserName(parentUser.getUserName()); // 用户名
					dailiUserMoneyDetail.setOperateUserName(ConstantUtil.SYSTEM_OPERATE_NAME); // 操作人员
					dailiUserMoneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.GZ)); // 订单单号
					dailiUserMoneyDetail.setWinLevel(null);
					dailiUserMoneyDetail.setWinCost(null);
					dailiUserMoneyDetail.setWinCostRule(null);
					dailiUserMoneyDetail.setDetailType(EMoneyDetailType.DAY_SALARY.name());
					dailiUserMoneyDetail.setIncome(new BigDecimal(0));
					dailiUserMoneyDetail.setPay(daySalaryOrder.getMoney());
					dailiUserMoneyDetail.setBalanceBefore(parentUser.getMoney());
					dailiUserMoneyDetail.setBalanceAfter(parentUser.getMoney().subtract(daySalaryOrder.getMoney())); // 支出后的金额
					dailiUserMoneyDetail.setDescription("日工资支出:" + daySalaryOrder.getMoney());
					
					newParentUser.reduceMoney(daySalaryOrder.getMoney(), true);
					
					int res = 0;
					try {
						res = userService.updateByPrimaryKeyWithVersionSelective(newParentUser);
					} catch (UnEqualVersionException e) {
						logger.error("日工资处理发现用户表版本号不一致，开启新线程处理", e);
						UserVersionExceptionThread thread = new UserVersionExceptionThread(newParentUser.getId(), daySalaryOrder.getMoney(), UserVersionExceptionThread.REDUCE_TYPE, true, dailiUserMoneyDetail);
						thread.start();
					}
					if(res == 1) {
						moneyDetailService.insertSelective(dailiUserMoneyDetail); // 保存资金明细
					}
					
					try {
						userMoneyTotalService.addUserTotalMoneyByType(newParentUser, EMoneyDetailType.DAY_SALARY, BigDecimal.ZERO.subtract(daySalaryOrder.getMoney()));
					} catch (Exception e) {
						logger.error(e.getMessage());
					}

					// 当前用日工资发放，用户余额增加
					dailiUserMoneyDetail = new MoneyDetail();
					dailiUserMoneyDetail.setEnabled((user.getIsTourist() == 1 ? 0 : 1));
					dailiUserMoneyDetail.setUserId(user.getId()); // 用户id
					dailiUserMoneyDetail.setOrderId(null); // 订单id
					dailiUserMoneyDetail.setBizSystem(user.getBizSystem());
					dailiUserMoneyDetail.setUserName(user.getUserName()); // 用户名
					dailiUserMoneyDetail.setOperateUserName(parentUser.getUserName()); // 操作人员
					dailiUserMoneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.GZ)); // 订单单号
					dailiUserMoneyDetail.setWinLevel(null);
					dailiUserMoneyDetail.setWinCost(null);
					dailiUserMoneyDetail.setWinCostRule(null);
					dailiUserMoneyDetail.setDetailType(EMoneyDetailType.DAY_SALARY.name());
					dailiUserMoneyDetail.setIncome(daySalaryOrder.getMoney());
					dailiUserMoneyDetail.setPay(new BigDecimal(0));
					dailiUserMoneyDetail.setBalanceBefore(user.getMoney());
					dailiUserMoneyDetail.setBalanceAfter(user.getMoney().add(daySalaryOrder.getMoney())); // 支出后的金额
					dailiUserMoneyDetail.setDescription("日工资收入:" + daySalaryOrder.getMoney());
					
					user.addMoney(daySalaryOrder.getMoney(), true);
					res = 0;
					try {
						res = userService.updateByPrimaryKeyWithVersionSelective(user);
					} catch (UnEqualVersionException e) {
						logger.error("日工资发放处理发现用户表版本号不一致，开启新线程处理", e);
						UserVersionExceptionThread thread = new UserVersionExceptionThread(user.getId(), daySalaryOrder.getMoney(), UserVersionExceptionThread.ADD_TYPE, true, dailiUserMoneyDetail);
						thread.start();
					}
					if(res == 1) {
						moneyDetailService.insertSelective(dailiUserMoneyDetail); // 保存资金明细
						
					}
					
					try {
						userMoneyTotalService.addUserTotalMoneyByType(user, EMoneyDetailType.DAY_SALARY, daySalaryOrder.getMoney());
					} catch (Exception e) {
						logger.error(e.getMessage());
					}
					
					// 更新契约订单发放的状态值
					daySalaryOrder.setReleaseStatus(EBonusOrderStatus.RELEASED.name());
					daySalaryOrder.setReleaseDate(new Date());
					daySalaryOrder.setUpdateDate(new Date());
					daySalaryOrderService.updateByPrimaryKeySelective(daySalaryOrder);
				}
				
				 //发送系统消息
		        Notes notes = new Notes();
		        notes.setParentId(0l);
		        notes.setEnabled(1);
		        notes.setToUserId(user.getId());
		        notes.setToUserName(user.getUserName());
		        notes.setBizSystem(user.getBizSystem());
		        notes.setFromUserName(ConstantUtil.SYSTEM_OPERATE_NAME);
		        notes.setSub("[日工资发放通知]");
		        notes.setBody("您的上级发放了归属日期["+ daySalaryOrder.getBelongDateStr() +"]的日工资，金额为："+ daySalaryOrder.getMoney()); 
		        notes.setType(ENoteType.SYSTEM.name()); 
		        notes.setStatus(ENoteStatus.NO_READ.name());
		        notes.setCreatedDate(new Date());
		        notesService.insertSelective(notes);
			}
		}
		
		//进入递归处理
		for(User user : subUsers) {
			this.settleMoney(user, releasePolicy, belongDate);
		}
	}
   	
}
