package com.team.lottery.system.job;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.team.lottery.service.SystemConfigService;
import com.team.lottery.vo.SystemConfig;

/**
 * 新年年历底执行job
 * @author luocheng
 *springFestivalStartDateId
 */
@Component("yearEndDateJob")
public class YearEndDateJob {

	private static Logger logger = LoggerFactory.getLogger(YearEndDateJob.class);
	
	@Autowired
	private SystemConfigService systemConfigService;
	
	public void execute() {
		try {
			logger.info("开始定时执行新年时间结束任务...");
			//:TODO
			//北京pk10  北京快三  幸运28 基准期数 基准日期自动变动
			List<SystemConfig> systemConfigs = systemConfigService.getSystemConfig();
			for (SystemConfig record : systemConfigs) {
				if(record.getConfigKey().equals("bjpk10BaseDate")){
					//当ConfigKey为北京pk10改变北京pk10的日期
					record = systemConfigService.getSystemConfigByKey("bjpk10BaseDate");
					record.setConfigValue("2019-01-01");
					systemConfigService.updateByPrimaryKeySelective(record);
				}
				if(record.getConfigKey().equals("pk10BasetExpect")){
					//当ConfigKey为北京pk10改变北京pk10的期数
					record = systemConfigService.getSystemConfigByKey("pk10BasetExpect");
					record.setConfigValue("723305");
					systemConfigService.updateByPrimaryKeySelective(record);
				}
				if(record.getConfigKey().equals("bjksBaseDate")){
					//当ConfigKey为北京快三改变北京pk10的日期
					record = systemConfigService.getSystemConfigByKey("bjksBaseDate");
					record.setConfigValue("2019-01-01");
					systemConfigService.updateByPrimaryKeySelective(record);
				}
				if(record.getConfigKey().equals("bjksBasetExpect")){
					//当ConfigKey为北京快三改变北京pk10的期数
					record = systemConfigService.getSystemConfigByKey("bjksBasetExpect");
					record.setConfigValue("129926");
					systemConfigService.updateByPrimaryKeySelective(record);
				}
				if(record.getConfigKey().equals("xyebBaseDate")){
					//当ConfigKey为幸运28改变北京pk10的日期
					record = systemConfigService.getSystemConfigByKey("xyebBaseDate");
					record.setConfigValue("2019-01-01");
					systemConfigService.updateByPrimaryKeySelective(record);
				}
				if(record.getConfigKey().equals("xyebBasetExpect")){
					//当ConfigKey为幸运28改变北京pk10的期数
					record = systemConfigService.getSystemConfigByKey("xyebBasetExpect");
					record.setConfigValue("929282");
					systemConfigService.updateByPrimaryKeySelective(record);
				}
				//台湾五分彩
				if(record.getConfigKey().equals("twwfcBaseDate")){
					//当ConfigKey为台湾五分彩改变北京pk10的日期
					record = systemConfigService.getSystemConfigByKey("twwfcBaseDate");
					record.setConfigValue("2019-01-01");
					systemConfigService.updateByPrimaryKeySelective(record);
				}
				if(record.getConfigKey().equals("twwfcBasetExpect")){
					//当ConfigKey为台湾五分彩改变北京pk10的期数
					record = systemConfigService.getSystemConfigByKey("twwfcBasetExpect");
					record.setConfigValue("108000000");
					systemConfigService.updateByPrimaryKeySelective(record);
				}
				//修改春节时间
				if(record.getConfigKey().equals("springFestivalStartDate")){
					record = systemConfigService.getSystemConfigByKey("springFestivalStartDate");
					record.setConfigValue("2019-02-05");
					systemConfigService.updateByPrimaryKeySelective(record);
				}
				if(record.getConfigKey().equals("springFestivalEndDate")){
					record = systemConfigService.getSystemConfigByKey("springFestivalEndDate");
					record.setConfigValue("2019-02-11");
					systemConfigService.updateByPrimaryKeySelective(record);
				}
				
			}
			
		} catch (Exception e) {
			logger.error("定时执行新年时间结束任务失败...",e);
			throw new RuntimeException(e);
		}
		
	}
}
