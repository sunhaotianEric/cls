package com.team.lottery.system.job;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.team.lottery.extvo.UserDayConsumeQuery;
import com.team.lottery.service.BizSystemService;
import com.team.lottery.service.DayProfitRankService;
import com.team.lottery.service.UserSelfDayConsumeService;
import com.team.lottery.service.UserService;
import com.team.lottery.util.ClsDateHelper;
import com.team.lottery.util.StringUtils;
import com.team.lottery.util.UserHeadImgUtil;
import com.team.lottery.util.UserNameUtil;
import com.team.lottery.vo.BizSystem;
import com.team.lottery.vo.DayProfitRank;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserSelfDayConsume;

/**
 * 获取今天中奖金额前十的用户!
 * 
 * @author Jamine
 *
 */
@Component("dayProfitRankJob")
public class DayProfitRankJob {
	private static Logger logger = LoggerFactory.getLogger(DayProfitRankJob.class);

	@Autowired
	private BizSystemService bizSystemService;
	@Autowired
	private UserSelfDayConsumeService userSelfDayConsumeService;
	@Autowired
	private UserService userService;
	@Autowired
	private DayProfitRankService dayProfitRankService;

	/**
	 * 需要调度的任务.
	 */
	public void execute() {
		logger.info("开始执行每日前十奖金榜调度任务...");
		// 获取所有的业务系统.
		List<BizSystem> bizList = bizSystemService.getAllBizSystem();

		// 根据业务系统从kr_user_day_self_consume查询10条中奖金额最高的数据.
		for (BizSystem bizSystem : bizList) {
			// 当系统为启用状态的时候才执行操作.
			if (bizSystem.getEnable() == 1) {
				logger.info("开始执行每日前十奖金榜插入数据,系统[" + bizSystem.getBizSystem() + "]...");
				// 根据系统获取当日中奖总金额前十的数据.
				UserDayConsumeQuery query = new UserDayConsumeQuery();
				query.setBizSystem(bizSystem.getBizSystem());
				// 设置查询时间范围.
				query.setBelongDateStart(ClsDateHelper.getClsRangeStartTime(1));
				query.setBelongDateEnd(ClsDateHelper.getClsRangeEndTime(1));
				// 查询今日中奖金额前十的数据.
				List<UserSelfDayConsume> userSelfDayConsumes = userSelfDayConsumeService.getUserSelfDayConsumeTopTen(query);
				// 当查询出来的userSelfDayConsumeList数据不足十条的时候,就添加假数据.
				List<DayProfitRank> dayProfitRankList = new ArrayList<DayProfitRank>();
				// 归属时间置为十二点,该方法执行时间不可能超过晚上12点.
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(new Date());
				calendar.set(Calendar.HOUR_OF_DAY, 12);
				calendar.set(Calendar.MINUTE, 0);
				calendar.set(Calendar.SECOND, 0);
				calendar.set(Calendar.MILLISECOND, 0);
				Date belongDateTime = calendar.getTime();
				// 将金额大于30万的数据加入新的集合
				for (UserSelfDayConsume userSelfDayConsume : userSelfDayConsumes) {
					if (userSelfDayConsume.getWin().compareTo(new BigDecimal("300000")) >= 0) {
						DayProfitRank dayProfitRank = new DayProfitRank();
						// 归属日期.
						dayProfitRank.setBelongDate(userSelfDayConsume.getBelongDate());
						// 系统简写.
						dayProfitRank.setBizSystem(userSelfDayConsume.getBizSystem());
						// 中奖金额.
						dayProfitRank.setBonus(userSelfDayConsume.getWin());
						// 创建日期.
						dayProfitRank.setCreateDate(new Date());

						// 根据ID查询出对应的用户信息.
						User user = userService.selectByPrimaryKey(userSelfDayConsume.getUserId());
						if (user != null) {
							// 设置头像.
							dayProfitRank.setHeadImg(user.getHeadImg());
							// 设置昵称.
							if (StringUtils.isEmpty(user.getNick())) {
								dayProfitRank.setNickName("");
							} else {
								dayProfitRank.setNickName(user.getNick());
							}
							// 设置排名.
							dayProfitRank.setRanking(null);
							// 设置用户ID
							dayProfitRank.setUserId(userSelfDayConsume.getUserId());
							// 设置用户名.
							dayProfitRank.setUserName(user.getUserName());
						} else {
							// 设置头像.
							dayProfitRank.setHeadImg(UserHeadImgUtil.getUserHeadImg());
							// 设置昵称.
							dayProfitRank.setNickName("");
							// 设置排名.
							dayProfitRank.setRanking(null);
							// 设置用户ID
							dayProfitRank.setUserId(-1);
							// 设置用户名.
							dayProfitRank.setUserName(UserNameUtil.getRandomUsername());
						}
						logger.info("真数据!用户名:[" + dayProfitRank.getUserName() + "]的金额为[:" + dayProfitRank.getBonus() + "]");
						dayProfitRankList.add(dayProfitRank);
					}
				}
				logger.info("真数据!金额大于30万的数据有:[" + dayProfitRankList.size() + "]条");
				logger.info("假数据!金额大于30万的数据有:["+ String.valueOf(10 - dayProfitRankList.size()) + "]条");
				if (dayProfitRankList.size() < 10) {
					int i = 10 - dayProfitRankList.size();
					for (int j = 0; j < i; j++) {
						DayProfitRank dayProfitRank = new DayProfitRank();
						// 设置归属时间.
						dayProfitRank.setBelongDate(belongDateTime);
						// 设置系统.
						dayProfitRank.setBizSystem(bizSystem.getBizSystem());
						// 设置随机金额
						dayProfitRank.setBonus(DayProfitRankJob.getRandomBonusMoney());
						// 设置创建时间.
						dayProfitRank.setCreateDate(new Date());
						// 设置随机头像.
						dayProfitRank.setHeadImg(UserHeadImgUtil.getUserHeadImg());
						// 设置昵称.
						dayProfitRank.setNickName("");
						// 设置排名.(暂不设置.)
						dayProfitRank.setRanking(null);
						// 设置用户ID为-1;
						dayProfitRank.setUserId(-1);
						// 设置随机用户名.
						dayProfitRank.setUserName(UserNameUtil.getRandomUsername());
						// 将数据添加到list集合中,用于排序.
						dayProfitRankList.add(dayProfitRank);
						logger.info("假数据!用户名:[" + dayProfitRank.getUserName() + "]的金额为:[" + dayProfitRank.getBonus() + "]");
					}
				}

				// List根据Bonus排序.排序完毕过后进行排名设置,并且保存到数据库
				List<DayProfitRank> rankWithBonusList = DayProfitRankJob.getRankWithBonusList(dayProfitRankList);
				Integer ranking = 10;
				for (DayProfitRank dayProfitRank : rankWithBonusList) {
					dayProfitRank.setRanking(ranking);
					int i = dayProfitRankService.insertSelective(dayProfitRank);
					if (i == 1) {
						ranking--;
						// 新增成功,打印日志.
						logger.info("执行每日前十奖金榜往数据库插入数据插入结果[成功]插入的用户名为:[" + dayProfitRank.getUserName() + "]排名为:[" + dayProfitRank.getRanking() + "]金额为:[" + dayProfitRank.getBonus() + "],系统为:["+ dayProfitRank.getBizSystem() + "]");
					} else {
						logger.info("执行每日前十奖金榜往数据库插入数据插入结果[失败]");
					}
				}
			}
		}
		logger.info("执行每日前十奖金榜结束...");

	}

	/**
	 * 获取随机金额.
	 * 
	 * @return
	 */
	private static BigDecimal getRandomBonusMoney() {
		// 随机金额的范围为300000~1200000
		Integer bonusMin = 300000;
		Integer bonusMax = 1200000;
		Random random = new Random();
		// 随机生成300000到1200000之间的整数.
		Integer bonus = random.nextInt(bonusMax) % (bonusMax - bonusMin + 1) + bonusMin;
		BigDecimal randomBonusMoney = new BigDecimal(bonus);
		return randomBonusMoney;
	}

	/**
	 * 根据dayProfitRanks中的Bonus字段排序.
	 * 
	 * @param dayProfitRank
	 * @return
	 */
	private static List<DayProfitRank> getRankWithBonusList(List<DayProfitRank> dayProfitRank) {
		// 重写Comparator中的compare方法
		Collections.sort(dayProfitRank, new Comparator<DayProfitRank>() {
			@Override
			public int compare(DayProfitRank o1, DayProfitRank o2) {
				return o1.getBonus().compareTo(o2.getBonus());
			}
		});
		return dayProfitRank;
	}

}
