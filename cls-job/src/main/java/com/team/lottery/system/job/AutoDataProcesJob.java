package com.team.lottery.system.job;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.team.lottery.extvo.UserQuery;
import com.team.lottery.mapper.user.UserMapper;
import com.team.lottery.mapper.userregister.UserRegisterMapper;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.UserHeadImgUtil;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserRegister;

@Component("autoDataProcesJob")
public class AutoDataProcesJob extends QuartzJob {
	private static Logger log = LoggerFactory.getLogger(AutoDataProcesJob.class);

	@Autowired
	private UserRegisterMapper userRegisterMapper;
	@Autowired
	private UserMapper userMapper;

	@Override
	public void execute() {
		this.updateUserRegisterRebate();
	}

	/**
	 * 后台系统-定时任务手工结算-修复数据-全局修改用户注册链接信息,新增字段.
	 */
	public void updateUserRegisterRebate() {
		try {
			log.info("开始执行用户推广链接更新...");
			// 获取所有用户的推广链接.
			List<UserRegister> userRegisters = userRegisterMapper.getAllUserRegisterLinks();
			// 通过forech循环,循环出单个进行跟新操作.
			for (UserRegister userRegister : userRegisters) {
				// 获取获取对应的linkSets.
				String linkSet = userRegister.getLinkSets();
				// 对linkSet进行拆分.获取到对应的模式值.
				String[] linkSets = linkSet.split(ConstantUtil.REGISTER_SPLIT);
				// 设置代理类型.
				userRegister.setDailiLevel(linkSets[0]);
				// 设置时时彩模式.
				userRegister.setSscRebate(new BigDecimal(linkSets[1]));
				// 设置分分彩模式.
				userRegister.setFfcRebate(new BigDecimal(linkSets[2]));
				// 设置十一选五模式.
				userRegister.setSyxwRebate(new BigDecimal(linkSets[3]));
				// 设置快三模式.
				userRegister.setKsRebate(new BigDecimal(linkSets[4]));
				// 设置PK10模式.
				userRegister.setPk10Rebate(new BigDecimal(linkSets[5]));
				// 设置低频彩模式.
				userRegister.setDpcRebate(new BigDecimal(linkSets[6]));
				// 设置投宝模式.
				userRegister.setTbRebate(new BigDecimal(linkSets[7]));
				// 设置六合彩模式.
				userRegister.setLhcRebate(new BigDecimal(linkSets[8]));
				// 设置快乐十分模式.
				userRegister.setKlsfRebate(new BigDecimal(linkSets[9]));
				// 设置赠送金额
				userRegister.setPresentMoney(BigDecimal.ZERO);
				// :TODO 修改数据库.
				userRegisterMapper.updateByPrimaryKeySelective(userRegister);
				log.info("业务系统[" + userRegister.getBizSystem() + "]中的用户[" + userRegister.getUserName() + "]修改的数据ID为["+userRegister.getId()+"}设置代理类型[" + linkSets[0] + "]" + "设置时时彩模式[" + linkSets[1] + "]" + "分分彩模式["
						+ linkSets[2] + "]" + "十一选五模式[" + linkSets[3] + "]" + "快三模式[" + linkSets[4] + "]" + "PK10模式[" + linkSets[5] + "]低频彩模式[" + linkSets[6] + "]投宝模式["
						+ linkSets[7] + "]六合彩模式[" + linkSets[8] + "]快乐十分模式[" + linkSets[9] + "]赠送金额[" + BigDecimal.ZERO + "]");
			}
		} catch (Exception e) {
			log.error("执行处理用户推广链接更新出现异常....");
		}

	}

	/**
	 * 后台系统-定时任务手工结算-修复数据-全局设置对应系统的用户头像!
	 */
	public void updateUserHeadImg() {
		try {
			log.info("开始执行处理用户头像随机生成...");
			// 获取当前系统.
			String bizSystem = this.bizSystem;
			// 新建查询对象.
			UserQuery query = new UserQuery();
			// 设置查询字段.
			query.setBizSystem(bizSystem);
			// 查询出对应系统的所有用户.
			List<User> users = userMapper.getUsersByCondition(query);
			for (User user : users) {
				// 使用UserHeadImgUtil工具类随机生成用户头像,并设置.
				String userHeadImg = UserHeadImgUtil.getUserHeadImg();
				user.setHeadImg(userHeadImg);
				log.info("[" + bizSystem + "]系统中的" + user.getUserName() + "用户,生成随机头像[" + userHeadImg + "]");
			}
			userMapper.updateUserHeadImg(users);
			log.info("执行处理用户头像随机生成结束...");

		} catch (Exception e) {
			log.info("执行处理用户头像随机生成失败....");
		}
	}

	/**
	 * 后台系统-定时任务手工结算-修复数据-全局设置时对应系统的奖金模式!
	 */
	public void updateBatchLinkSet() {
		try {
			log.info("开始执行处理当前奖金模式数据任务...");
			String bizSystem = this.bizSystem;
			List<UserRegister> userRegisters = userRegisterMapper.getUserRegisterLinksWithBizSystem(bizSystem);
			for (UserRegister userRegister : userRegisters) {
				String linkSets = userRegister.getLinkSets();
				userRegister.setLinkSets(modifyLinkSets(userRegister.getLinkSets()));
				log.info("id是[" + userRegister.getId() + "] 邀请码是[" + userRegister.getInvitationCode() + "] 的linkSets(奖金模式)修改前是[" + linkSets + "] 修改后是["
						+ userRegister.getLinkSets() + "]");
			}
			userRegisterMapper.updateBatchLinkSets(userRegisters);
			log.info("执行处理当前奖金模式数据任务结束...");
		} catch (Exception e) {
			log.info("执行处理当前奖金模式数据任务失败", e);
		}
	}

	// 处理linkSets当前奖金模式配置，
	// 时时彩模式&分分彩模式&十一选五模式&快三模式&PK10模式&骰宝模式&六合彩模式&快乐十分模式 数值加10
	// &低频彩模式数值不变
	private static String modifyLinkSets(String linkSets) {
		if (StringUtils.isBlank(linkSets)) {
			return "";
		}
		// 注册代理级别&时时彩模式&分分彩模式&十一选五模式&快三模式&PK10模式&低频彩模式&骰宝模式&六合彩模式&快乐十分模式&分红比例&注册赠送资金
		String[] linkSetsArrays = linkSets.split("&");
		String linkSets_ = linkSetsArrays[0] + "&" // 注册代理级别
				+ String.valueOf(Double.valueOf(linkSetsArrays[1]).intValue() + 10) + "&" // 时时彩模式
				+ String.valueOf(Double.valueOf(linkSetsArrays[2]).intValue() + 10) + "&" // 分分彩模式
				+ String.valueOf(Double.valueOf(linkSetsArrays[3]).intValue() + 10) + "&" // 十一选五模式
				+ String.valueOf(Double.valueOf(linkSetsArrays[4]).intValue() + 10) + "&" // 快三模式
				+ String.valueOf(Double.valueOf(linkSetsArrays[5]).intValue() + 10) + "&" // PK10模式
				+ String.valueOf(Double.valueOf(linkSetsArrays[6]).intValue()) + "&" // 低频彩模式
				+ String.valueOf(Double.valueOf(linkSetsArrays[7]).intValue() + 10) + "&" // 骰宝模式
				+ String.valueOf(Double.valueOf(linkSetsArrays[8]).intValue() + 10) + "&" // 六合彩模式
				+ String.valueOf(Double.valueOf(linkSetsArrays[9]).intValue() + 10) + "&" // 快乐十分模式
				+ linkSetsArrays[10] + "&" // 分红比例
				+ linkSetsArrays[11]; // 注册赠送资金
		return linkSets_;
	}
}
