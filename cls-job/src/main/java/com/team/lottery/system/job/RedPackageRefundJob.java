package com.team.lottery.system.job;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.service.ChatRedbagService;
import com.team.lottery.util.DateUtil;
import com.team.lottery.vo.ChatRedbag;

/**
 * 红包回退Job
 * 
 * @author Owner
 *
 */
@Component("redPackageRefundJob")
public class RedPackageRefundJob extends QuartzJob {

	private static Logger log = LoggerFactory.getLogger(RedPackageRefundJob.class);

	@Autowired
	private ChatRedbagService chatRedbagService;

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void execute() {
		Date nowDate = new Date();
		Date endDate = DateUtil.addDaysToDate(nowDate, -1);
		List<ChatRedbag> chatRedbagList = chatRedbagService.getChatRedbagByStatus(endDate);
		for (int i = 0; i < chatRedbagList.size(); i++) {
			ChatRedbag chatRedbag = chatRedbagList.get(i);
			// 如果红包未领取则默认未0
			if (chatRedbag.getExpireMoney() == null) {
				chatRedbag.setExpireMoney(new BigDecimal(0));
			}
			BigDecimal money = chatRedbag.getMoney().subtract(chatRedbag.getReceivedMoney());
			if (money != new BigDecimal(0) && chatRedbag.getStatus() == 0) {
				chatRedbag.setStatus(2);
				chatRedbag.setExpireMoney(money);
				try {
					// 处理剩余金额转账转入红包发送人账号里并返回受影响行数
					int k = chatRedbagService.saveRedbagReturn(chatRedbag.getUserName(), chatRedbag.getBizSystem(),
							chatRedbag.getExpireMoney());
					if (k > 0) {
						// 修改该红包为已过期及回退金额
						chatRedbagService.updateByPrimaryKeySelective(chatRedbag);
						log.info("用户[" + chatRedbag.getUserName() + "]发的红包已过期回退金额：" + chatRedbag.getExpireMoney()
								+ ",回退成功时间[" + sdf.format(endDate) + "]...");
					}
				} catch (Exception e) {
					log.error("用户[" + chatRedbag.getUserName() + "]发的红包已过期回退金额：" + chatRedbag.getExpireMoney() + ",回退失败时间["
							+ sdf.format(endDate) + "]...", e);
				}
			} else {
				// 红包被领完的修改该红包为已过期，不做资金回退处理
				chatRedbag.setStatus(2);
				chatRedbagService.updateByPrimaryKeySelective(chatRedbag);
				log.error("用户[" + chatRedbag.getUserName() + "]发的红包已过期,修改状态为过期，修改时间[" + sdf.format(endDate) + "]...");
			}
		}

	}
}
