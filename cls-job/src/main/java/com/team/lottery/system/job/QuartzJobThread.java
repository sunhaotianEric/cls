package com.team.lottery.system.job;


/**
 * 手动结算代理福利计算线程
 * @author gs
 *
 */
public class QuartzJobThread extends Thread {

	private QuartzJob job;

	@Override
	public void run() {
		if(job != null) {
			job.execute();
		}
	}
	
	public void setJob(QuartzJob job) {
		this.job = job;
	}
	
	
}
