package com.team.lottery.system.job;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.extvo.MoneyDetailQuery;
import com.team.lottery.extvo.UserConsumeReportVo;
import com.team.lottery.extvo.UserDayConsumeQuery;
import com.team.lottery.service.BizSystemService;
import com.team.lottery.service.MoneyDetailService;
import com.team.lottery.service.UserSelfDayConsumeService;
import com.team.lottery.service.UserService;
import com.team.lottery.util.ClsDateHelper;
import com.team.lottery.util.DateUtil;
import com.team.lottery.vo.BizSystem;
import com.team.lottery.vo.MoneyDetail;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserSelfDayConsume;

/**
 * 每天计算用户自身盈利的job
 * @author gs
 *
 */
@Component("userSelfDayConsumeJob")
public class UserSelfDayConsumeJob extends QuartzJob {

	private static Logger logger = LoggerFactory.getLogger(UserSelfDayConsumeJob.class);
	
	@Autowired
	private UserService userService;
	@Autowired
	private MoneyDetailService moneyDetailService;
	@Autowired
	private UserSelfDayConsumeService userSelfDayConsumeService;
	@Autowired
	private BizSystemService bizSystemService;
	
	 
	@Override
	@Transactional(readOnly=false,rollbackFor=Exception.class)
	public void execute() {
		try {
			logger.info("开始计算所有用户自身日盈利数据...");
			Long startDealTime = System.currentTimeMillis();
			
			Calendar calendar = Calendar.getInstance();
			Date yesterday = calendar.getTime();
			if(nowTime != null) {
				calendar.setTime(nowTime);
			}  else {
				//获取昨天的日期
				calendar.add(Calendar.DATE, -1);
			}
			
			yesterday = calendar.getTime();
			logger.info("计算的时间["+DateUtil.parseDate(yesterday, "yyyy-MM-dd HH:mm:ss")+"]");
			
			//获取查询的起始结束时间 03:00:00 - 02:59:59
			Date startDateTime = ClsDateHelper.getClsStartTime(yesterday);
			Date endDateTime = ClsDateHelper.getClsEndTime(yesterday);
			logger.info("查询计算的起始时间{},结束时间{}", DateUtil.parseDate(startDateTime, "yyyy-MM-dd HH:mm:ss"), DateUtil.parseDate(endDateTime, "yyyy-MM-dd HH:mm:ss"));
			
			//归属时间置为十二点
			calendar.set(Calendar.HOUR_OF_DAY, 12);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			Date belongDateTime = calendar.getTime();
			
	   		//拥有资金明细的用户  用于后面个人资金明细的计算
			logger.info("要查询资金明细的开始时间：" + DateUtil.parseDate(startDateTime, "yyyy-MM-dd HH:mm:ss") 
					+ " 结束时间：" + DateUtil.parseDate(endDateTime, "yyyy-MM-dd HH:mm:ss"));
			List<BizSystem> bizList = new ArrayList<BizSystem>();
		    //this.bizSystem有值，单业务系统
			if(this.bizSystem==null||"".equals(this.bizSystem)){
				//查询所有业务系统
				bizList=bizSystemService.getAllBizSystem();
			}else{
				BizSystem biz=new BizSystem();
				biz.setBizSystem(this.bizSystem);
				biz=bizSystemService.getBizSystemByCondition(biz);
				bizList.add(biz);
			}
			
		    for(BizSystem bizSystem:bizList){
		    	logger.info("开始查询处理业务系统[{}]的数据",bizSystem.getBizSystem());
				//构建资金明细查询对象
				MoneyDetailQuery query = new MoneyDetailQuery();
				query.setCreatedDateStart(startDateTime);
				query.setCreatedDateEnd(endDateTime);
				query.setBizSystem(bizSystem.getBizSystem());
				query.setEnabled(1);
				//查找有资金明细的用户
				List<String> users = moneyDetailService.getUsersHasMoneyDetail(query);
				List<UserConsumeReportVo> userConsumeReportVos = new ArrayList<UserConsumeReportVo>();
				
				//进行前天未开奖资金明细特殊处理
		    	/*logger.info("获取前天未开奖资金明细记录");
		    	List<MoneyDetail> notYetBonusMoneyDetails = moneyDetailService.getYesterdayNotYetBonus(yesterday, null);
		    	Map<String, List<MoneyDetail>> userNotYetBonusMoneyDetailMap = new HashMap<String, List<MoneyDetail>>();
		    	if(CollectionUtils.isNotEmpty(notYetBonusMoneyDetails)) {
		    		for(MoneyDetail md : notYetBonusMoneyDetails) {
		    			List<MoneyDetail> userNotBonusMoneyDetailList = userNotYetBonusMoneyDetailMap.get(md.getUserName());
		    			if(userNotBonusMoneyDetailList == null) {
		    				userNotBonusMoneyDetailList = new ArrayList<MoneyDetail>();
		    				userNotYetBonusMoneyDetailMap.put(md.getUserName(), userNotBonusMoneyDetailList);
		    			} 
		    			userNotBonusMoneyDetailList.add(md);
		    		}
		    	}*/
				
				if(CollectionUtils.isNotEmpty(users)) {
					logger.info("查询出来的有资金明细的用户数量为["+users.size()+"]");
					for(String usersName : users) {
						
						/**
			    		 * 根据bizsystem来和时间,用户名判断是否有数据，有数据代表已经结算完成！
			    		 */
						UserDayConsumeQuery userDayConsumeQuery=new UserDayConsumeQuery();
						userDayConsumeQuery.setBizSystem(bizSystem.getBizSystem());
						userDayConsumeQuery.setBelongDate(belongDateTime);
						userDayConsumeQuery.setUserName(usersName);
				        UserSelfDayConsume userSelfDayConsume=userSelfDayConsumeService.getUserSelfDayConsumeByCondition(userDayConsumeQuery);
						if(userSelfDayConsume!=null&&userSelfDayConsume.getId()!=null){
							logger.info("业务系统"+bizSystem.getBizSystem()+"的"+belongDateTime+"数据已经存在，跳过继续执行下一个业务系统！");
							continue;
						}
						
						//按用户查询自身的资金明细
						query.setUserName(usersName);
						query.setBizSystem(bizSystem.getBizSystem());
						List<MoneyDetail> moneyDetails = moneyDetailService.queryMoneyDetailsByCondition(query);
						
						//添加进未分红资金明细
						/*List<MoneyDetail> userNotBonusMoneyDetailList = userNotYetBonusMoneyDetailMap.get(usersName);
						if(CollectionUtils.isNotEmpty(userNotBonusMoneyDetailList)) {
							moneyDetails.addAll(userNotBonusMoneyDetailList);
						}*/
						
						//计算当前页的资金明细
			    		Map<String,UserConsumeReportVo> consumeReportMaps = moneyDetailService.getMoneyDetailForuserMap(moneyDetails, null);
			    		UserConsumeReportVo userConsumeReportVo = consumeReportMaps.get(usersName);
			    		if(userConsumeReportVo != null) {
			    			userConsumeReportVos.add(userConsumeReportVo);
			    		}
					}
				}
		    	
		    	//保存到用户自身日盈亏表
		    	dealSaveUserDayConsume(userConsumeReportVos, belongDateTime,bizSystem.getBizSystem());
			}
			Long endDealTime = System.currentTimeMillis();
			logger.info("计算所有用户自身日盈利数据结束,共耗时["+(endDealTime - startDealTime)+"]ms...");
		} catch (Exception e) {
			logger.error("计算所有用户自身日盈利数据出现异常...", e);
			throw new RuntimeException(e);
		}
	}
	
	
	/**
	 * 处理保存单个用户团队的日盈利数据
	 * @param user
	 * @param query
	 */
	private void dealSaveUserDayConsume(List<UserConsumeReportVo> userConsumeReportVos, Date belongDate, String bizSystem) {
		Iterator<UserConsumeReportVo> iterator = userConsumeReportVos.iterator();
		while(iterator.hasNext()) {
			UserConsumeReportVo userConsumeReportVo = iterator.next();
			if(userConsumeReportVo != null) {
				UserSelfDayConsume userDayConsume = new UserSelfDayConsume();
				BeanUtils.copyProperties(userConsumeReportVo, userDayConsume);
				userDayConsume.setBizSystem(bizSystem);
				userDayConsume.setRechargepresent(userConsumeReportVo.getRechargePresent());
				userDayConsume.setActivitiesmoney(userConsumeReportVo.getActivitiesMoney());
				userDayConsume.setWithdraw(userConsumeReportVo.getWithDraw());
				userDayConsume.setWithdrawfee(userConsumeReportVo.getWithDrawFee());
				userDayConsume.setPaytranfer(userConsumeReportVo.getPayTranfer());
				userDayConsume.setIncometranfer(userConsumeReportVo.getIncomeTranfer());
				userDayConsume.setHalfmonthbonus(userConsumeReportVo.getHalfMonthBonus());
				userDayConsume.setDaybonus(userConsumeReportVo.getDayBonus());
				userDayConsume.setMonthsalary(userConsumeReportVo.getMonthSalary());
				userDayConsume.setDaysalary(userConsumeReportVo.getDaySalary());
				userDayConsume.setDaycommission(userConsumeReportVo.getDayCommission());
				userDayConsume.setSystemaddmoney(userConsumeReportVo.getSystemaddmoney());
				userDayConsume.setSystemreducemoney(userConsumeReportVo.getSystemreducemoney());
				userDayConsume.setBelongDate(belongDate);
				userDayConsume.setCreateDate(new Date());
				
				//保存用户id和refrom
				User user = userService.getUserByName(bizSystem, userConsumeReportVo.getUserName());
				if(user != null) {
					userDayConsume.setUserId(user.getId());
					userDayConsume.setRegfrom(user.getRegfrom());
					//userDayConsume.setMoney(user.getMoney());
				} else {
					logger.error("新增用户自身日盈亏数据出现异常,用户名["+userDayConsume.getUserName()+"]对应的用户表查询不到数据...");
					continue;
				}
				
				userSelfDayConsumeService.insertSelective(userDayConsume);
			}
		}
	}
	
	
	/**
	 * 根据起始日期和要计算的天数，重新计算用户团队盈利数据
	 * @param startDate
	 * @param days 表示要计算的天数，如果为1表示要计算的是startDate那天的数据
	 */
	public void reCalcuteUserDayConsume(Date startDate, int days) {
		//System.setProperty("user.timezone","Asia/Shanghai"); 
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		Date nowDayTime = calendar.getTime();
		logger.info("重新计算所有用户团队盈利数据  起始时间["+DateUtil.parseDate(startDate, "yyyy-MM-dd HH:mm:ss")+"],计算天数["+ days +"]");
		
		
		calendar.setTime(startDate);
		for(int i = 0; i < days; i++) {
			Date calcuteDate = calendar.getTime();
			
			//计算日期判断
			if(calcuteDate.after(nowDayTime)) {
				logger.info("执行所有用户团队盈利计算起始时间不能在今天之后,结束计算...");
				return;
			}
			
			this.setNowTime(calcuteDate);
			this.execute();
			
			calendar.add(Calendar.DAY_OF_MONTH, 1);
		}
		
		
		
		
	}

}
