package com.team.lottery.system.job;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.extvo.LotteryCodeQuery;
import com.team.lottery.mapper.lotterycode.LotteryCodeMapper;
import com.team.lottery.mapper.lotterycodetrend.LotteryCodeTrendMapper;
import com.team.lottery.service.LotteryCodeTrendService;
import com.team.lottery.service.codetrend.LHCCodeTrendServiceImpl;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.util.EumnUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryCodeTrend;

/**
 * 处理六合彩走势图执行job
 * 
 * @author Owner
 * @deprecated  暂时不知道啥用处
 *
 */
@Component("lotteryCodeTrendJob")
public class LotteryCodeTrendJob {
	private static Logger logger = LoggerFactory.getLogger(LotteryCodeTrendJob.class);

	@Autowired
	public LotteryCodeMapper lotteryCodeMapper;
	@Autowired
	public LotteryCodeTrendMapper lotteryCodeTrendMapper;

	public void execute() {
		LotteryCodeQuery query = new LotteryCodeQuery();
		query.setLotteryName(ELotteryKind.LHC.getCode());
		List<LotteryCode> list = lotteryCodeMapper.getAllLotteryCodesByQuery(query);
		for (LotteryCode lotteryCode : list) {
			String kind = lotteryCode.getLotteryName();
			String openCode = lotteryCode.getOpencodeStr();
			String expect = lotteryCode.getLotteryNum();
			// 过滤已经保存的
			LotteryCodeTrend lotteryCodeTrend2 = lotteryCodeTrendMapper.selectByKindAndExpect(kind, expect);
			if (lotteryCodeTrend2 != null) {
				continue;
			}
			/**
			 * 开奖后记录走势图数据
			 */
			try {
				logger.info("开奖后记录走势图数据开始,期号[{}],彩种[{}],开奖号码[{}]", expect, kind, openCode);
				LotteryCodeTrendService lotteryCodeTrendService = (LotteryCodeTrendService) ApplicationContextUtil
						.getBean("lotteryCodeTrendService");
				LHCCodeTrendServiceImpl lHCCodeTrendServiceImpl = (LHCCodeTrendServiceImpl) ApplicationContextUtil
						.getBean("lHCCodeTrendServiceImpl");
				LotteryCodeTrend lotteryCodeTrend = new LotteryCodeTrend();
				lotteryCodeTrend.setOpenCode(openCode);
				lotteryCodeTrend.setLotteryName(kind);
				lotteryCodeTrend.setLotteryNum(expect);
				lotteryCodeTrend.setAddtime(new Date());
				lotteryCodeTrend.setKjtime(lotteryCode.getKjtime());
				String serviceClassName = EumnUtil.getServiceClassNameCodeTrend(kind);
				SystemConfigConstant.springFestivalStartDate = "2019-02-04 00:00:00";
				if (StringUtils.isNotBlank(serviceClassName)) {
					String lastNumPosition = "";
					lotteryCodeTrend.setNumPosition(lastNumPosition);
					lotteryCodeTrend.setPlaykindContent(lHCCodeTrendServiceImpl.getPlaykindContent(openCode));
				}
				lotteryCodeTrendService.insertSelective(lotteryCodeTrend);
				logger.info("开奖后记录走势图数据结束");
			} catch (Exception e) {
				logger.info("开奖后记录走势图数据异常:" + e);
			}
		}
	}
}
