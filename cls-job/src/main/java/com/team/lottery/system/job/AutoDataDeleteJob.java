package com.team.lottery.system.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.team.lottery.mapper.config.ConfigMapper;
import com.team.lottery.mapper.lotterycodetrend.LotteryCodeTrendMapper;
import com.team.lottery.mapper.lotterycodexytrend.LotteryCodeXyTrendMapper;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.DateUtil;

@Component("autoDataDeleteJob")
public class AutoDataDeleteJob extends QuartzJob {
	
	private static Logger log = LoggerFactory.getLogger(AutoDataDeleteJob.class);
	
	@Autowired
	private ConfigMapper configMapper;
	@Autowired
	private LotteryCodeTrendMapper lotteryCodeTrendMapper;
	@Autowired
	private LotteryCodeXyTrendMapper lotteryCodeXyTrendMapper;

	@Override
	public void execute() {
		try {
			log.info("开始执行每日删除数据任务...");
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date nowDate = new Date();
			nowDate = DateUtil.getNowStartTimeByEnd(nowDate);
			Date endDate = DateUtil.addDaysToDate(nowDate, -2);
			log.info("删除2天前系统通知数据,时间["+sdf.format(endDate)+"]...");
			configMapper.deleteAgoDataFromThisTimeForSystemNotes(endDate);
			log.info("删除2天前用户模式使用数据,时间["+sdf.format(endDate)+"]...");
			configMapper.deleteAgoDataFromThisTimeForUserModelUsed(endDate);
			log.info("删除2天前用户团队小时盈亏数据,时间["+sdf.format(endDate)+"]...");
			configMapper.deleteAgoDataFromThisTimeForUserHourConsume(endDate);
			
			
			
			/**
			 * 删除试玩用户的一些信息  start
			 */
			endDate = DateUtil.addDaysToDate(nowDate, -1 * ConstantUtil.DELETE_AGO_DATA_GUEST);
			log.info("删除"+ConstantUtil.DELETE_AGO_DATA_GUEST+"天试玩用户的数据,时间["+sdf.format(endDate)+"]...");
			//UserMoneyTotal在删除user表之前执行
			configMapper.deleteAgoDataFromThisTimeForGuestUserMoneyTotal(endDate);
			configMapper.deleteAgoDataFromThisTimeForGuestUser(endDate);
			configMapper.deleteAgoDataFromThisTimeForGuestOrder(endDate);
			configMapper.deleteAgoDataFromThisTimeForGuestMoneyDetial(endDate);
			configMapper.deleteAgoDataFromThisTimeForGuestLoginLog(endDate);
			endDate = DateUtil.addDaysToDate(nowDate, -1 * ConstantUtil.DELETE_AGO_DATA_GUEST);
			/**
			 * 删除试玩用户的一些信息  end
			 */
			
			
			endDate = DateUtil.addDaysToDate(nowDate,-3);
			log.info("删除3天前高频彩开奖号码数据,时间["+sdf.format(endDate)+"]...");
			configMapper.deleteAgoDataFromThisTimeForLotteryCodeXy(endDate);
			
			endDate = DateUtil.addDaysToDate(nowDate, -1 * ConstantUtil.DELETE_AGO_DATA_LOWER);
			log.info("删除7天前高频彩开奖号码数据,时间["+sdf.format(endDate)+"]...");
			configMapper.deleteAgoDataFromThisTimeForLotteryCode(endDate);
			
			log.info("删除7天前用户cookie登录记录,时间["+sdf.format(endDate)+"]...");
			configMapper.deleteAgoDateFromThisTimeForUserCookieLog(endDate);
			
			//低频彩数据暂时不删除
//			endDate = DateUtil.addDaysToDate(nowDate, -1 * ConstantUtil.DELETE_AGO_DATA_LOWERCODEDATE);
//			log.info("删除180天前低频彩开奖号码数据,时间["+sdf.format(endDate)+"]...");
//			configMapper.deleteAgoDataFromThisTimeForLowLotteryCode(endDate);
			
			
			endDate = DateUtil.addDaysToDate(nowDate, -1 * ConstantUtil.DELETE_AGO_DATA_ORDER);
			log.info("删除{}天前订单数据,时间["+sdf.format(endDate)+"]...", ConstantUtil.DELETE_AGO_DATA_ORDER);
			configMapper.deleteAgoDataFromThisTimeForOrder(endDate);
			log.info("删除{}天前用户投注缓存数据,时间["+sdf.format(endDate)+"]...", ConstantUtil.DELETE_AGO_DATA_ORDER);
			configMapper.deleteAgoDataFromThisTimeForLotteryCache(endDate);
			log.info("删除{}天前用户追号投注数据,时间["+sdf.format(endDate)+"]...", ConstantUtil.DELETE_AGO_DATA_ORDER);
			configMapper.deleteAgoDataFromThisTimeForOrderAfterRecord(endDate);
			configMapper.deleteAgoDataFromThisTimeForOrderAfterAmount(endDate);
			
			
			
			endDate = DateUtil.addDaysToDate(nowDate, -1 * ConstantUtil.DELETE_AGO_DATA_MONEY_DETAIL);
			log.info("删除{}天之前的(资金明细)...", ConstantUtil.DELETE_AGO_DATA_MONEY_DETAIL);
			configMapper.deleteAgoDataFromThisTimeForMoneyDetail(endDate);
			
			endDate = DateUtil.addDaysToDate(nowDate, -1 * ConstantUtil.DELETE_AGO_DATA_LOGIN_LOG);
			log.info("删除{}天前用户登录数据,时间["+sdf.format(endDate)+"]...", -1 * ConstantUtil.DELETE_AGO_DATA_LOGIN_LOG);
			configMapper.deleteAgoDataFromThisTimeForLogin(endDate);
			
			endDate = DateUtil.addDaysToDate(nowDate, -1 * ConstantUtil.DELETE_AGO_DATA_RECHARGE_WITHDRAW_ORDER);
			log.info("删除{}天前用户充值提现订单数据,时间["+sdf.format(endDate)+"]...", ConstantUtil.DELETE_AGO_DATA_RECHARGE_WITHDRAW_ORDER);
			//configMapper.deleteAgoDataFromThisTimeForDrawOrder(endDate);
			configMapper.deleteAgoDataFromThisTimeForRechargeOrder(endDate);
			configMapper.deleteAgoDataFromThisTimeForWithdrawOrder(endDate);
			log.info("删除{}天前用户打码要求数据,时间["+sdf.format(endDate)+"]...", ConstantUtil.DELETE_AGO_DATA_RECHARGE_WITHDRAW_ORDER);
			configMapper.deleteAgoDateFromThisTimeForUserBettingDemand(endDate);
			
			Date redBagEndDate = DateUtil.addDaysToDate(nowDate, -1 * ConstantUtil.DELETE_AGO_DATA_CHAT_RED_BAG);
			log.info("删除{}天前发送红包数据,时间["+sdf.format(redBagEndDate)+"]...", ConstantUtil.DELETE_AGO_DATA_CHAT_RED_BAG);
			configMapper.deleteAgoDateFromThisTimeForChatRedBag(redBagEndDate);
			log.info("删除{}天前红包领取记录数据,时间["+sdf.format(redBagEndDate)+"]...", ConstantUtil.DELETE_AGO_DATA_CHAT_RED_BAG);
			configMapper.deleteAgoDateFromThisTimeForChatRedBagInfo(redBagEndDate);
			log.info("删除{}天前聊天记录数据,时间["+sdf.format(redBagEndDate)+"]...", ConstantUtil.DELETE_AGO_DATA_CHAT_RED_BAG);
			configMapper.deleteAgoDateFromThisTimeForChatlog(redBagEndDate);
			
			
			endDate = DateUtil.addDaysToDate(nowDate, -1 * ConstantUtil.DELETE_AGO_DATA_NORMAL);
			log.info("删除常规数据 (收件箱  用户资金日志  操作明细 用户团队日盈亏数据  用户团队日余额  每日流水返送订单),"+ConstantUtil.DELETE_AGO_DATA_NORMAL+"天之前数据,时间["+sdf.format(endDate)+"]... ");
			
			configMapper.deleteAgoDataFromThisTimeForNotes(endDate);
			configMapper.deleteAgoDataFromThisTimeForUseMoneyLog(endDate);
			//暂时不删除操作日志
			//configMapper.deleteAgoDataFromThisTimeForRecordLog(endDate);
			configMapper.deleteAgoDataFromThisTimeForUserDayConsume(endDate);
			configMapper.deleteAgoDataFromThisTimeForUserDayMoney(endDate);
			configMapper.deleteAgoDataFromThisTimeForUserSelfDayConsume(endDate);
			configMapper.deleteAgoDataFromThisTimeForDayConsumeActivityOrder(endDate);
			configMapper.deleteAgoDataFromThisTimeForUserDayLotteryCount(endDate);
			
			endDate = DateUtil.addDaysToDate(nowDate, -1 * ConstantUtil.DELETE_AGO_DATA_NUNORMAL);
			log.info("非常规数据删除 (代理福利申请   代理福利订单   配额明细),"+ConstantUtil.DELETE_AGO_DATA_NUNORMAL+"天之前数据,时间["+sdf.format(endDate)+"]...");
			configMapper.deleteAgoDataFromThisTimeForQuotaDetail(endDate);
			
			log.info("删除风控记录数据,"+ConstantUtil.DELETE_AGO_DATA_NUNORMAL+"天之前数据,时间["+sdf.format(endDate)+"]...");
			configMapper.deleteAgoDataFromThisTimeForRiskRecord(endDate);
			
			//走势图多余数据删除,除了低频彩，删除7天前数据
			log.info("删除除了低频彩其余彩种数据");
			lotteryCodeTrendMapper.deleteTrendCodeBefore7Days(new Long(7));//官方彩
			lotteryCodeXyTrendMapper.deleteTrendCodeBefore7Days(new Long(7));//幸运彩
			
			// 删除30天前的密码错误日志.
			endDate = DateUtil.addDaysToDate(nowDate, -1 * ConstantUtil.DELETE_AGO_DATA_CHAT_RED_BAG);
			log.info("删除常规数据 (密码错误操作记录,管理员密码错误记录),"+ConstantUtil.DELETE_AGO_DATA_CHAT_RED_BAG+"天之前数据,时间["+sdf.format(endDate)+"]... ");
			configMapper.deletePasswordErrorLogDateFromThisTimeForThirtyDaysAgo(endDate);
			configMapper.deleteAdminPasswordErrorLogDateFromThisTimeForThirtyDaysAgo(endDate);
			log.info("执行每日删除数据任务结束...");
			
		} catch (Exception e) {
			log.error("删除数据失败",e);
		}
	}

}
