package com.team.lottery.system.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.team.lottery.system.SystemCache;
import com.team.lottery.system.cache.LotteryIssueCache;

/**
 * 刷新缓存
 * @author Owner
 *
 */
@Component("refreshCacheJob")
public class RefreshCacheJob extends QuartzJob {
	private static Logger log = LoggerFactory.getLogger(RefreshCacheJob.class);

	@Override
	public void execute() {
		log.info("进入刷新缓存的job.....................");
		// 加载网站系统参数配置缓存，从数据库读取
		SystemCache.loadSystemConfigCacheData();
		// 加载彩种开关数据
		SystemCache.loadLotteryConfigCacheData();
		// 加载所有业务系统参数数据
		SystemCache.loadBizSystemConfigCacheData();
		// 初始化域名管理缓存
		SystemCache.refreshBizSystemDomainCache();
		// 刷新彩种开奖时间缓存
		LotteryIssueCache.refreshLotteryIssueCache();
		log.info("缓存刷新完毕..........................");
	}

}
