<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.RSA"%>
<%@page import="com.team.lottery.pay.model.JianZhengBaoPay"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
 <%@page errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.InetAddress"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="com.team.lottery.extvo.returnModel.HemaParameter"%>
<%@page import="com.team.lottery.extvo.returnModel.JianZhengBaoData"%>
<%@page import="java.util.*"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="java.net.InetAddress"%>
<%@ page import="com.team.lottery.pay.util.HttpClients"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%  

    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
    String orderMoney = request.getParameter("OrderMoney");//订单金额 
	String payId = request.getParameter("PayId");//支付的银行接口
	String payType = request.getParameter("payType");//充值类型
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String serialNumber = request.getParameter("serialNumber");
 
    
    /* User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
	 */
	

	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));
	String  orderId=MakeOrderNum.makeOrderNum("CZ");
	/* log.info("用户名称: "+currentUser.getUserName()+" 用户 ID号: "+currentUser.getId()+" 生成订单号: "+orderId+" 订单金额: "+orderMoney); */
	
	
	if(!"".equals(orderMoney)) {	
		 BigDecimal a;
		a = new BigDecimal(orderMoney); //使用分进行提交
		orderMoney=String.valueOf(a.multiply(new BigDecimal(100)).intValue());
	}
	else{
		orderMoney = ("0");
	} 
	String payUrl = chargePay.getPayUrl();//转发北付宝网关地址
	String Address = chargePay.getAddress(); //最终北付宝地址
	log.info("payUrl: "+payUrl+" Address: "+Address);
	boolean testMoney=false;
	List<String> allowMoney = new ArrayList<String>();
	allowMoney.add("3000");
	allowMoney.add("5000");
	allowMoney.add("10000");
	allowMoney.add("20000");
	allowMoney.add("30000");
	if(allowMoney.contains(orderMoney) && payType.equals("WEIXINWAP")){
		testMoney=true;
	}
	if(!testMoney && payType.equals("WEIXINWAP")){
		out.print("微信只能充值30元,50元,100元,200元,300元");
	}
	
    //时间以yyyy-MM-dd HH:mm:ss的方式表示

    //交易签名
    /* jianZhengBaoPay.setSign(Signature); */
    //log.info("md5原始串: "+md5 + "生成后的交易签名:" + Signature); */
	//log.info("实际北付宝支付网关地址["+Address +"],交易报文: "+jianZhengBaoPay);
	//北付宝支付请求对象
	
	
    Map<String, String> mapContent = new HashMap<String, String>();
    mapContent.put("tranCode", "1101");
    mapContent.put("agtId","21306174");
    mapContent.put("merId", chargePay.getMemberId());
    mapContent.put("orderAmt", orderMoney);
    mapContent.put("orderId", serialNumber);
    String str="";
    String strgoodsName = "cz";
    for (int i = 0; i < strgoodsName.length(); i++) {
        int ch = (int) strgoodsName.charAt(i);
        String s4 = Integer.toHexString(ch);
        str = str + s4;
    }
    mapContent.put("goodsName",str);
    str="";
    String strgoodsDetail = "rmb";
    for (int i = 0; i < strgoodsDetail.length(); i++) {
        int ch = (int) strgoodsDetail.charAt(i);
        String s4 = Integer.toHexString(ch);
        str = str + s4;
    }
    mapContent.put("goodsDetail", str);
    mapContent.put("notifyUrl",chargePay.getReturnUrl());
    mapContent.put("returnUrl", chargePay.getNotifyUrl());
    mapContent.put("nonceStr", "zgxmsm");
    mapContent.put("payChannel", payId);
    /* InetAddress address = InetAddress.getLocalHost();    
    String ip=address .getHostAddress().toString();
    mapContent.put("termIp", ip); */
    log.info(mapContent.toString());
    mapContent.put("stlType","T0");
    
    Date currTime = new Date();
	Map<String, String> mapHead = new HashMap<String, String>();
	String url = Address;
    String Md5key = chargePay.getSign();///////////md5密钥（KEY）
    
    String MARK = "&";
	String md5 = new String("agtId=" + mapContent.get("agtId") + MARK + "goodsDetail=" + mapContent.get("goodsDetail") + MARK
			+ "goodsName=" + mapContent.get("goodsName") + MARK + "merId=" + mapContent.get("merId") + MARK + "nonceStr="+mapContent.get("nonceStr") + MARK
			+ "notifyUrl=" + mapContent.get("notifyUrl") + MARK + "orderAmt=" + mapContent.get("orderAmt")+MARK + "orderId=" + mapContent.get("orderId")+MARK
			+ "payChannel=" + mapContent.get("payChannel") + MARK + "returnUrl=" + mapContent.get("returnUrl")+MARK + "stlType=" + mapContent.get("stlType")
			+ MARK + "tranCode=" + mapContent.get("tranCode")
			+MARK +"key="+ Md5key);//MD5签名格式
			log.info(md5);
	//MD5签名格式
	
	String Signature = md5Util.getMD5ofStr(md5).toUpperCase();
    mapHead.put("sign", Signature);
    log.info(mapHead.toString());

    JSONObject businessContext = JSONObject.fromObject(mapContent);
    JSONObject businessHead = JSONObject.fromObject(mapHead);

    log.info("发送的报文businessContext:" + businessContext.toString());
    log.info("发送的报文businessHead   :" + businessHead.toString());
    
    JSONObject jsonObject = new JSONObject();
    jsonObject.put("REQ_HEAD", businessContext.toString());
    jsonObject.put("REQ_BODY", businessHead.toString());
    log.info("发送的报文        :" + jsonObject.toString());

    JSONObject returnStr = HttpClients.doPost(url, jsonObject);
    log.info("接收的密文               ：" + returnStr);
   
    HemaParameter businessContextReturn=JSONUtils.toBean(returnStr.getString("REP_BODY"),HemaParameter.class);
    log.info("接收的报文               ：" +businessContextReturn.toString());
    String codeUrl="";
    if(businessContextReturn.getRspcode().equals("000000")){
    	codeUrl=businessContextReturn.getCodeUrl();
    	response.sendRedirect(codeUrl);
    	return;
    }
    /* request.setAttribute("payType", payType); */
    //System.out.println("接收的报文               ：" + RSA.decryptByPrivateKey(returnStr.getString("context"), memberPriKeyJava));
	
 %>
 
 <%-- <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta id="viewport" name="viewport"
	content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
<title>充值接口-提交信息处理</title>
<style>
body {
	text-align: center;
}

img {
	margin-top: 60px;
}

p {
	font-size: 36px;
}
</style>
</head>

<body>
	<img id="weixinImg" src='<%=path%>/tools/getQrCodePic?code=<%=codeUrl%>'></img>
	<img src="http://qr.topscan.com/api.php?bg=f3f3f3&fg=ff0000&gc=222222&el=l&w=684&m=30&text=<%=code%>"/>
	<p>
		请使用<span id="pay-way">
		    <c:if test="${payType == 'ALIPAY' }">支付宝</c:if>
			<c:if test="${payType == 'WEIXIN' }">微信</c:if>
			<c:if test="${payType == 'JDQB' }">京东钱包</c:if>
			<c:if test="${payType == 'QQPAY' }">QQ钱包</c:if>
			<c:if test="${payType == 'UNIONPAY' }">银联二维码</c:if>
			</span>扫描二维码以完成支付
	</p>

</body>
</html> --%>
 