<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.pay.model.FeiLongPay"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
 <%@page errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URL"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="com.team.lottery.extvo.returnModel.FeiLongPayReturnParam"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.service.RechargeConfigService"%>
<%@page import="com.team.lottery.vo.RechargeConfig"%>
<%@page import="java.net.InetAddress"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
    String orderMoneyYuan = OrderMoney;
    String serialNumber = request.getParameter("serialNumber");
 
    
    /* User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    } */
	
	Date currTime = new Date();
    //时间以yyyy-MM-dd HH:mm:ss的方式表示
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.CHINA);
    //时间以yyyyMMDDHHmmss的方式表示 
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss",Locale.CHINA);
    String webdate=new String(formatter1.format(currTime));
    String TradeDate=new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));
	
	log.info(" 生成订单号: "+serialNumber+" 订单金额: "+OrderMoney);
	
	
	/*  if (!"".equals(OrderMoney)){	
	BigDecimal a;
	a = new BigDecimal(OrderMoney).multiply(BigDecimal.valueOf(100)); //使用分进行提交
	OrderMoney=String.valueOf(a.setScale(0));
	}
	else{
		OrderMoney = ("0");
	}  */
	String payUrl = chargePay.getPayUrl();//转发支付网关地址
	String Address = chargePay.getAddress(); //最终乐美地址
	log.info("payUrl: "+payUrl+" Address: "+Address);
	
	//乐美支付请求对象
	HashMap<String,Object> mapContent = new HashMap<String,Object>();
	mapContent.put("UserID", chargePay.getMemberId());
	mapContent.put("OrderID", serialNumber);
	mapContent.put("FaceValue", OrderMoney);
	mapContent.put("ChannelID", payId);
	mapContent.put("TimeStamp", webdate);
	mapContent.put("Version","V2.0");
	InetAddress address = InetAddress.getLocalHost();    
    String ip=address .getHostAddress().toString();   
	mapContent.put("IP", ip);
	mapContent.put("ResultType", "0");
	mapContent.put("NotifyUrl", chargePay.getReturnUrl());
	mapContent.put("ResultUrl", chargePay.getNotifyUrl());
	
	
	String publicKey=chargePay.getPublickey();
	String sBuilder = "";
	
	Collection<String> keyset= mapContent.keySet();
	List list=new ArrayList<String>(keyset);
	Collections.sort(list);
	for(int i=0;i<list.size();i++){
		if(i == 0) {
			sBuilder += list.get(i)+"="+mapContent.get(list.get(i)).toString();
		}
		else {
			sBuilder += "&"+list.get(i)+"="+mapContent.get(list.get(i)).toString();
		}
	}
	//生成md5密钥签名
	String md5 =new String(sBuilder+"&key="+publicKey);//MD5签名格式
	
	String Signature = md5Util.getMD5ofStr(md5);//计算MD5值
    //交易签名
    mapContent.put("sign", Signature);
    log.info("md5原始串: "+md5 + "生成后的交易签名:" + Signature);
	log.info("实际乐美支付网关地址["+Address +"],交易报文: "+mapContent.toString());
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>
	<body onload="pay.submit()">
		<form method="post" name="pay" id="pay" action="<%=payUrl%>">
			<TABLE>
				<TR>
					<TD>
						<input name='UserID' type='hidden'  value='<%=mapContent.get("UserID")%>'/>
						<input name='OrderID' type='hidden' value='<%=mapContent.get("OrderID")%>'/>
						<input name='FaceValue' type='hidden' value='<%=mapContent.get("FaceValue")%>'/>
						<input name='ChannelID' type='hidden' value='<%=mapContent.get("ChannelID")%>'/>
						<input name='TimeStamp' type='hidden' value='<%=mapContent.get("TimeStamp")%>'/>
						<input name='Version' type='hidden' value='<%=mapContent.get("Version")%>'/>		
						<input name='IP' type='hidden' value= '<%=mapContent.get("IP")%>'/>	
						<input name='ResultType' type='hidden' value='<%=mapContent.get("ResultType")%>' />
						<input name='NotifyUrl' type='hidden' value='<%=mapContent.get("NotifyUrl")%>'/>
						<input name='ResultUrl' type='hidden' value='<%=mapContent.get("ResultUrl")%>'/>
						<input name='sign' type='hidden' value='<%=mapContent.get("sign")%>'/>
					</TD>
				</TR>
			</TABLE>
		</form>	
	</body>
</html>
 <%-- <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta id="viewport" name="viewport"
	content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
<title>充值接口-提交信息处理</title>
<style>
body {
	text-align: center;
}

#weixinImg {
	margin-top: 150px;
	width: 280px;
}

p {
	font-size: 18px;
}
</style>
</head>

<body>
	<img id="weixinImg" src='<%=codeUrl%>'></img>
	<img src="http://qr.topscan.com/api.php?bg=f3f3f3&fg=ff0000&gc=222222&el=l&w=684&m=30&text=<%=code%>"/>
	<p>
		请使用<span id="pay-way"><c:if test="${payType == 'ALIPAY' }">支付宝</c:if>
			<c:if test="${payType == 'WEIXIN' }">微信</c:if>
			<c:if test="${payType == 'QQPAY' }">QQ钱包</c:if>
			<c:if test="${payType == 'JDPAY' }">京东扫码</c:if>
			<c:if test="${payType == 'UNIONPAY' }">银联扫码</c:if></span>扫描二维码以完成支付
	</p>

</body>
</html>
 --%>