<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.pay.model.JinYangPay"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
 <%@page errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URL"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="com.team.lottery.extvo.returnModel.JinYangReturnParameter"%>
<%@page import="com.team.lottery.extvo.returnModel.JinYangData"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@page import="com.team.lottery.vo.RechargeConfig"%>
<%@page import="com.team.lottery.service.RechargeConfigService"%>
<%  

    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
    String orderMoneyYuan = OrderMoney;
    String serialNumber = request.getParameter("serialNumber");
 
    
    User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
	
	Date currTime = new Date();
    //时间以yyyy-MM-dd HH:mm:ss的方式表示
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.CHINA);
    //时间以yyyyMMDDHHmmss的方式表示 
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss",Locale.CHINA);
    String webdate=new String(formatter1.format(currTime));
    String TradeDate=new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));
	
	log.info("用户名称: "+currentUser.getUserName()+" 用户 ID号: "+currentUser.getId()+" 生成订单号: "+serialNumber+" 订单金额: "+OrderMoney);
	
	
	 if (!"".equals(OrderMoney)) 
	{	BigDecimal a;
		a = new BigDecimal(OrderMoney); //使用分进行提交
		OrderMoney=String.valueOf(a.setScale(2));
	}
	else{
		OrderMoney = ("0");
	} 
	String payUrl = chargePay.getPayUrl();//转发支付网关地址
	String Address = chargePay.getAddress(); //最终银宝地址
	log.info("payUrl: "+payUrl+" Address: "+Address);
	
	//金阳支付请求对象
	JinYangPay jinYangPay =new JinYangPay();
    //商户ID
    jinYangPay.setP1_mchtid(chargePay.getMemberId());
    //银行类型
    jinYangPay.setP2_paytype(payId);
    //商户订单号
    jinYangPay.setP3_paymoney(OrderMoney);
    //金额
    jinYangPay.setP4_orderno(serialNumber);
    //下行异步通知地址
    //chargePay.getReturnUrl()
    jinYangPay.setP5_callbackurl(chargePay.getReturnUrl());
    //下行同步通知地址
    //chargePay.getNotifyUrl()
    jinYangPay.setP6_notifyurl(chargePay.getNotifyUrl());
    //备注信息
    jinYangPay.setP7_version("v2.8");
    
    jinYangPay.setP8_signtype("1");
    jinYangPay.setP9_attach("");
    jinYangPay.setP10_appname("");
    jinYangPay.setP11_isshow("0");
    jinYangPay.setP12_orderip("");
	//生成md5密钥签名
    String Md5key = chargePay.getSign();///////////md5密钥（KEY）
	String MARK = "&";
	String md5 =new String("p1_mchtid="+jinYangPay.getP1_mchtid()+MARK+"p2_paytype="+jinYangPay.getP2_paytype()+MARK+"p3_paymoney="+jinYangPay.getP3_paymoney()+MARK+
			"p4_orderno="+jinYangPay.getP4_orderno()+MARK+"p5_callbackurl="+jinYangPay.getP5_callbackurl()+MARK+"p6_notifyurl="+jinYangPay.getP6_notifyurl()+MARK+
			"p7_version="+jinYangPay.getP7_version()+MARK+"p8_signtype="+jinYangPay.getP8_signtype()+MARK+"p9_attach="+jinYangPay.getP9_attach()+MARK+
			"p10_appname="+jinYangPay.getP10_appname()+MARK+"p11_isshow="+jinYangPay.getP11_isshow()+MARK+"p12_orderip="+jinYangPay.getP12_orderip()+chargePay.getSign());//MD5签名格式
	String Signature = md5Util.getMD5ofStr(md5);//计算MD5值
	
    //交易签名
    jinYangPay.setSign(Signature);
    log.info("md5原始串: "+md5 + "生成后的交易签名:" + Signature);
	log.info("实际金阳支付网关地址["+Address +"],交易报文: "+jinYangPay);
	
	String codeUrl="";
	if(!payType.equals("WEIXINBARCODE")){
	URL url = new URL(Address);
    String strPara="p1_mchtid="+URLEncoder.encode(jinYangPay.getP1_mchtid(), "utf-8") + "&p2_paytype="
			+ URLEncoder.encode(jinYangPay.getP2_paytype(), "utf-8") + "&p3_paymoney="
			+ URLEncoder.encode(jinYangPay.getP3_paymoney(), "utf-8") + "&p4_orderno="
			+ URLEncoder.encode(jinYangPay.getP4_orderno(), "utf-8") + "&p5_callbackurl="
			+ URLEncoder.encode(jinYangPay.getP5_callbackurl(), "utf-8") + "&p6_notifyurl="
			+ URLEncoder.encode(jinYangPay.getP6_notifyurl(), "utf-8") + "&p7_version="
			+ URLEncoder.encode(jinYangPay.getP7_version(), "utf-8") + "&p8_signtype="
			+ URLEncoder.encode(jinYangPay.getP8_signtype(), "utf-8") + "&p9_attach="
			+ URLEncoder.encode(jinYangPay.getP9_attach(), "utf-8")+"&p10_appname="
			+ URLEncoder.encode(jinYangPay.getP10_appname(), "utf-8")+"&p11_isshow="
			+ URLEncoder.encode(jinYangPay.getP11_isshow(), "utf-8")+"&p12_orderip="
			+ URLEncoder.encode(jinYangPay.getP12_orderip(), "utf-8")+"&sign="
			+ URLEncoder.encode(jinYangPay.getSign(), "utf-8");
	//openConnection函数会根据URL的协议返回不同的URLConnection子类的对象
	//这里URL是一个http,因此实际返回的是HttpURLConnection 
	HttpURLConnection httpConn = (HttpURLConnection) url
			.openConnection();

	//进行连接,实际上request要在下一句的connection.getInputStream()函数中才会真正发到 服务器****待验证
	httpConn.setDoOutput(true);
	httpConn.setDoInput(true);
	httpConn.connect();
	PrintWriter outWrite = new PrintWriter(httpConn.getOutputStream());
	
	outWrite.print(strPara);
    // flush输出流的缓冲
    outWrite.flush();
    
	// 取得输入流，并使用Reader读取
	BufferedReader reader = new BufferedReader(new InputStreamReader(
			httpConn.getInputStream(),"utf-8"));

	String lines = "";
	String str="";
	while ((str=reader.readLine()) != null) {
		lines += str; 
	}
	reader.close();
	log.info("金阳支付返回报文:" + lines);
	
	
		
	
		
	
	JinYangReturnParameter returnParameter = JSONUtils.toBean(lines,
			JinYangReturnParameter.class);
	if (returnParameter.getRspCode().equals("1")) {
		JinYangData jinYangData=returnParameter.getData();
		 codeUrl = jinYangData.getR6_qrcode();
		/* response.sendRedirect(returnParameter.getBarCode());
		return; */
	} else {
		
		out.print(returnParameter.getRspMsg());
		return;
	}
	}
	log.info("接收金阳支付转发成功,商户号[" + jinYangPay.getP1_mchtid() + "],充值金额[" + jinYangPay.getP3_paymoney()
			+ "],银行码表[" + jinYangPay.getP2_paytype() + "],提交金阳支付处理");

	request.setAttribute("payType", jinYangPay.getP2_paytype());
	request.setAttribute("codeUrl", codeUrl);
 %>
 
 <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta id="viewport" name="viewport"
	content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
<title>充值接口-提交信息处理</title>
<style>
body {
	text-align: center;
}

#weixinImg {
	margin-top: 150px;
	width: 280px;
}

p {
	font-size: 18px;
}
</style>
</head>
<c:if test="${codeUrl!=''}">
<body>
	<img id="weixinImg" src='<%=codeUrl%>'></img>
	<%-- <img src="http://qr.topscan.com/api.php?bg=f3f3f3&fg=ff0000&gc=222222&el=l&w=684&m=30&text=<%=code%>"/> --%>
	<p>
		请使用<span id="pay-way"><c:if test="${payType == 'ALIPAY' }">支付宝</c:if>
			<c:if test="${payType == 'WEIXIN' }">微信</c:if>
			<c:if test="${payType == 'QQPAY' }">QQ钱包</c:if>
			<c:if test="${payType == 'JDPAY' }">京东扫码</c:if>
			<c:if test="${payType == 'UNIONPAY' }">银联扫码</c:if></span>扫描二维码以完成支付
	</p>

</body>
</c:if>
<c:if test="${codeUrl==''}">
<body onload="pay.submit()">
<form method="post" name="pay" id="pay" action="<%=Address%>">
<TABLE>
<TR>
	<TD>
	<input name='Address' type='hidden' value= "<%=Address%>"/>
	<input name='p1_mchtid' type='hidden' value= "<%=jinYangPay.p1_mchtid%>"/>
	<input name='p2_paytype' type='hidden' value= "<%=jinYangPay.p2_paytype%>"/>
	<input name='p3_paymoney' type='hidden' value= "<%=jinYangPay.p3_paymoney%>"/>
	<input name='p4_orderno' type='hidden' value= "<%=jinYangPay.p4_orderno%>"/>
	<input name='p5_callbackurl' type='hidden' value= "<%=jinYangPay.p5_callbackurl%>"/>		
	<input name='p6_notifyurl' type='hidden' value= "<%=jinYangPay.p6_notifyurl%>" />
	<input name='p7_version' type='hidden' value= "<%=jinYangPay.p7_version%>" />
	<input name='p8_signtype' type='hidden' value= "<%=jinYangPay.p8_signtype%>" />
	<input name='p9_attach' type='hidden' value= "<%=jinYangPay.p9_attach%>" />
	<input name='p10_appname' type='hidden' value= "<%=jinYangPay.p10_appname%>" />
	<input name='p11_isshow' type='hidden' value= "<%=jinYangPay.p11_isshow%>" />
	<input name='p12_orderip' type='hidden' value= "<%=jinYangPay.p12_orderip%>" />
	<input name='sign' type='hidden' value= "<%=jinYangPay.sign%>"/>
	
	</TD>
</TR>
</TABLE>
	
</form>	

</body>
</c:if>
</html>

<%-- <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>

<body onload="pay.submit()">
<form method="post" name="pay" id="pay" action="<%=payUrl%>">
<TABLE>
<TR>
	<TD>
	<input name='Address' type='hidden' value= "<%=Address%>"/>
	<input name='p1_mchtid' type='hidden' value= "<%=jinYangPay.p1_mchtid%>"/>
	<input name='p2_paytype' type='hidden' value= "<%=jinYangPay.p2_paytype%>"/>
	<input name='p3_paymoney' type='hidden' value= "<%=jinYangPay.p3_paymoney%>"/>
	<input name='p4_orderno' type='hidden' value= "<%=jinYangPay.p4_orderno%>"/>
	<input name='p5_callbackurl' type='hidden' value= "<%=jinYangPay.p5_callbackurl%>"/>		
	<input name='p6_notifyurl' type='hidden' value= "<%=jinYangPay.p6_notifyurl%>" />
	<input name='p7_version' type='hidden' value= "<%=jinYangPay.p7_version%>" />
	<input name='p8_signtype' type='hidden' value= "<%=jinYangPay.p8_signtype%>" />
	<input name='p9_attach' type='hidden' value= "<%=jinYangPay.p9_attach%>" />
	<input name='p10_appname' type='hidden' value= "<%=jinYangPay.p10_appname%>" />
	<input name='p11_isshow' type='hidden' value= "<%=jinYangPay.p11_isshow%>" />
	<input name='p12_orderip' type='hidden' value= "<%=jinYangPay.p12_orderip%>" />
	<input name='sign' type='hidden' value= "<%=jinYangPay.sign%>"/>
	
	</TD>
</TR>
</TABLE>
	
</form>	

</body>
</html> --%>
