<%@page import="com.team.lottery.util.StringUtils"%>
<%@page import="com.alibaba.fastjson.JSON"%>
<%@page import="com.team.lottery.pay.util.RSAUtils"%>
<%@page import="com.team.lottery.pay.model.guangsupay.GuangSuPay"%>
<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.team.lottery.service.QuickBankTypeService"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.util.IPUtil"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.team.lottery.vo.QuickBankType"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="java.net.URLEncoder"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.team.lottery.service.RechargeConfigService"%>
<%@page import="com.team.lottery.vo.RechargeConfig"%>
<%@page import="com.team.lottery.pay.util.RSA"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
	String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//第三方支付码表
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	String serialNumber = request.getParameter("serialNumber"); // 获取配置ID;
	String orderMoneyYuan = OrderMoney;

	Date currTime = new Date();
	//时间以yyyy-MM-dd HH:mm:ss的方式表示
	SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);
	//时间以yyyyMMDDHHmmss的方式表示 
	SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss", Locale.CHINA);
	String webdate = new String(formatter1.format(currTime));
	String requestTime = new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService) ApplicationContextUtil.getBean("chargePayService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));

	log.info("业务系统[{}],发起了恒通支付，生成订单号[{}],支付金额[{}]元,PayId[{}],payType[{}],chargePayId[{}]", chargePay.getBizSystem(), serialNumber, OrderMoney, payId, payType, chargePayId);
	String payUrl = chargePay.getPayUrl();
	String Address = chargePay.getAddress();
	log.info("payUrl: " + payUrl + " Address: " + Address);

	//同步跳转地址
	String notifyUrl = "";
	if (chargePay.getNotifyUrl().startsWith("http://") || chargePay.getNotifyUrl().startsWith("https://")) {
		notifyUrl = chargePay.getNotifyUrl();
	} else {
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
		notifyUrl = basePath + chargePay.getNotifyUrl();
	}
	DecimalFormat df = new DecimalFormat("0.00");
	// 保留两位小数
	OrderMoney =  df.format(new BigDecimal(OrderMoney));
	
	
	String customer = chargePay.getMemberId();
	String banktype = payId;
	String amount = OrderMoney;
	String orderid = serialNumber;
	String asynbackurl = chargePay.getReturnUrl();
	String request_time = requestTime;
	String synbackurl = notifyUrl;
	String attach = "";
	String sign = "";
	
	String mark = "&";
	//商户密钥
	String key = chargePay.getSign();
	//MD5加密;
	String md5SignStr = "customer=" + customer + mark + 
						"banktype=" + banktype +mark + 
						"amount=" + amount +mark + 
						"orderid=" + orderid +mark + 
						"asynbackurl=" + asynbackurl +mark + 
						"request_time=" + request_time +mark + 
						"key=" + key;
	//进行MD5小写加密
	sign = Md5Util.getMD5ofStr(md5SignStr).toLowerCase();
	//打印日志
	log.info("md5原始串: " + md5SignStr + "加密后生成的交易签名:" + sign);

%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
<style>
body {
	text-align: center;
}

#weixinImg {
	margin-top: 150px;
	width: 280px;
}

p {
	font-size: 18px;
}
</style>
</head>

<body onload="pay.submit()">
<form method="post" name="pay" id="pay" action="<%=Address%>">
<TABLE>
<TR>
	<TD>
	<input name='customer' type='hidden' value= "<%=customer%>"/>
	<input name='banktype' type='hidden' value= "<%=banktype%>"/>
	<input name='amount' type='hidden' value= "<%=amount%>"/>
	<input name='orderid' type='hidden' value= "<%=orderid%>"/>
	<input name='asynbackurl' type='hidden' value= "<%=asynbackurl%>"/>
	<input name='request_time' type='hidden' value= "<%=request_time%>"/>		
	<input name='synbackurl' type='hidden' value= "<%=synbackurl%>" />
	<input name='attach' type='hidden' value= "<%=attach%>" />
	<input name='sign' type='hidden' value= "<%=sign%>"/>
	</TD>
</TR>
</TABLE>
	
</form>	

</body>
</html>