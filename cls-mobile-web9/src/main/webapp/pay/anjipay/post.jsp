<%@page import="com.team.lottery.pay.model.anjipay.AnJiPay"%>
<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.team.lottery.service.QuickBankTypeService"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.util.IPUtil"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.team.lottery.vo.QuickBankType"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="java.net.URLEncoder"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.team.lottery.service.RechargeConfigService"%>
<%@page import="com.team.lottery.vo.RechargeConfig"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
	String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	String serialNumber = request.getParameter("serialNumber"); // 获取配置ID;
	String orderMoneyYuan = OrderMoney;
	Date currTime = new Date();
	//时间以yyyy-MM-dd HH:mm:ss的方式表示
	SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);
	//时间以yyyyMMDDHHmmss的方式表示 
	SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss", Locale.CHINA);
	String webdate = new String(formatter1.format(currTime));
	String TradeDate = new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService) ApplicationContextUtil.getBean("chargePayService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));

	log.info("业务系统[{}],用户名[{}],发起了安吉支付，生成订单号[{}],支付金额[{}]元,PayId[{}],payType[{}],chargePayId[{}]",
	serialNumber, OrderMoney, payId, payType,
	chargePayId);
	String payUrl = chargePay.getPayUrl();
	String Address = chargePay.getAddress();
	log.info("payUrl: " + payUrl + " Address: " + Address);

	//同步跳转地址
	String notifyUrl = "";
	if (chargePay.getNotifyUrl().startsWith("http://") || chargePay.getNotifyUrl().startsWith("https://")) {
		notifyUrl = chargePay.getNotifyUrl();
	} else {
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
		+ path + "/";
		notifyUrl = basePath + chargePay.getNotifyUrl();
	}
	
	if (!"".equals(OrderMoney)) {
		BigDecimal a;
		a = new BigDecimal(OrderMoney).multiply(BigDecimal.valueOf(100)); //使用分进行提交
		OrderMoney = String.valueOf(a.setScale(0));
	} else {
		OrderMoney = ("0");
	}

	// 安吉支付发起对象.
	AnJiPay anJiPay = new AnJiPay();
	anJiPay.setAmount(OrderMoney);
	anJiPay.setBankcode(payId);
	anJiPay.setCreate_Time(TradeDate);
	anJiPay.setMerchant_ID(chargePay.getMemberId());
	anJiPay.setMerchant_Order(serialNumber);
	anJiPay.setNotice_Url(chargePay.getReturnUrl());
	anJiPay.setSign_Type("MD5");
	anJiPay.setType(payId);
	
	// 加密参数.(MD5大写)
	String mark = "&";
	String md5SignStr = "Amount=" + anJiPay.getAmount() + mark +
						"Bankcode=" + anJiPay.getBankcode() + mark +
						"Create_Time=" + anJiPay.getCreate_Time() + mark +
						"Merchant_ID=" + anJiPay.getMerchant_ID() + mark +
						"Merchant_Order=" + anJiPay.getMerchant_Order() + mark +
						"Notice_Url=" + anJiPay.getNotice_Url() + mark +
						"Type=" + anJiPay.getType() + chargePay.getSign();
	String md5Sign = Md5Util.getMD5ofStr(md5SignStr).toUpperCase();
	log.info("md5原始串: " + md5SignStr + "生成后的交易签名:" + md5Sign);
	anJiPay.setSign(md5Sign);
	log.info("安吉支付发起对象: " + anJiPay.toString());
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>

<body onload="pay.submit()">
	<form method="post" name="pay" id="pay" action="<%=payUrl%>">
		<TABLE>
			<TR>
				<TD>
					<input type='hidden' name='Amount' value= '<%=anJiPay.getAmount()%>'/>
					<input type='hidden' name='Bankcode' value= '<%=anJiPay.getBankcode()%>'/>
					<input type='hidden' name='Create_Time' value= '<%=anJiPay.getCreate_Time()%>'/>
					<input type='hidden' name='Merchant_ID' value= '<%=anJiPay.getMerchant_ID()%>'/>
					<input type='hidden' name='Merchant_Order' value= '<%=anJiPay.getMerchant_Order()%>'/>
					<input type='hidden' name='Notice_Url' value= '<%=anJiPay.getNotice_Url()%>'/>
					<input type='hidden' name='Sign' value= '<%=anJiPay.getSign()%>'/>
					<input type='hidden' name='Sign_Type' value= '<%=anJiPay.getSign_Type()%>'/>
					<input type='hidden' name='Type' value= '<%=anJiPay.getType()%>'/>
				</TD>
			</TR>
		</TABLE>
	</form>	
</body>
</html>
