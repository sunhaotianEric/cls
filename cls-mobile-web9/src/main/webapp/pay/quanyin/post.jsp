<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.RSA"%>
<%@page import="com.team.lottery.pay.model.JianZhengBaoPay"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
 <%@page errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.InetAddress"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="com.team.lottery.extvo.returnModel.QuanYinParameter"%>
<%@page import="com.team.lottery.pay.model.QuanYinPay"%>
<%@page import="java.util.*"%>
<%@page import="net.sf.json.JSONObject"%>
<%@ page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
String path = request.getContextPath();
String OrderMoney = request.getParameter("OrderMoney");//订单金额
String payId = request.getParameter("PayId");//支付的银行接口
String chargePayId = request.getParameter("chargePayId");//第三方ID号 
String payType = request.getParameter("payType");//充值类型
String orderMoneyYuan = OrderMoney;
String serialNumber = request.getParameter("serialNumber");
 
    
    User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
	
	

	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));
	String  orderId=MakeOrderNum.makeOrderNum("CZ");
	log.info("用户名称: "+currentUser.getUserName()+" 用户 ID号: "+currentUser.getId()+" 生成订单号: "+orderId+" 订单金额: "+OrderMoney);
	
	
	/* if(!"".equals(OrderMoney)) {	
		 BigDecimal a;
		a = new BigDecimal(OrderMoney); //使用分进行提交
		OrderMoney=String.valueOf(a.multiply(new BigDecimal(100)).intValue());
	}
	else{
		OrderMoney = ("0");
	} */ 
	String payUrl = chargePay.getPayUrl();//转发全银网关地址
	String Address = chargePay.getAddress(); //最终全银地址
	log.info("payUrl: "+payUrl+" Address: "+Address);
	QuanYinPay quanYinPay =new QuanYinPay();
	quanYinPay.setPayKey(chargePay.getMemberId());
	quanYinPay.setProductName("cz");
	quanYinPay.setOrderNo(serialNumber);
	quanYinPay.setPayWayCode("COMMONPAY2");
	quanYinPay.setPayTypeCode(payId);
	quanYinPay.setOrderIp(null);
	Date currTime = new Date();
    //时间以yyyyMMDDHHmmss的方式表示 
    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd",Locale.CHINA);
    String orderDate=new String(formatter.format(currTime));
    quanYinPay.setOrderDate(orderDate);
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMDDHHmmss",Locale.CHINA);
    String orderTime=new String(formatter2.format(currTime));
    quanYinPay.setOrderTime(orderTime);
    quanYinPay.setNotifyUrl(chargePay.getReturnUrl());
    quanYinPay.setReturnUrl(chargePay.getNotifyUrl());
    quanYinPay.setRemark("rmb");
    quanYinPay.setOrderPeriod("100");
    quanYinPay.setOrderPrice(OrderMoney);
    
    
    String Md5key = chargePay.getSign();///////////md5密钥（KEY）
    String MARK = "&";
	String md5 = new String("notifyUrl=" + quanYinPay.getNotifyUrl() + MARK + "orderDate=" + quanYinPay.getOrderDate() + MARK
			+ "orderNo=" + quanYinPay.getOrderNo() + MARK + "orderPeriod=" + quanYinPay.getOrderPeriod() + MARK + "orderPrice="
			+ quanYinPay.getOrderPrice() + MARK + "orderTime=" + quanYinPay.getOrderTime() + MARK + "payKey=" + quanYinPay.getPayKey() + MARK
			+ "payTypeCode=" + quanYinPay.getPayTypeCode() + MARK + "payWayCode=" + quanYinPay.getPayWayCode() + MARK + "productName="
			+ quanYinPay.getProductName() + MARK + "remark=" + quanYinPay.getRemark() + MARK + "returnUrl="
			+ quanYinPay.getReturnUrl() + MARK + "paySecret=" + Md5key);//MD5签名格式
	String Signature = md5Util.getMD5ofStr(md5).toUpperCase();//计算MD5值
	quanYinPay.setSign(Signature);
	//交易签名
	
	log.info("md5原始串: "+md5 + "生成后的交易签名:" + Signature);
	log.info("全银请求对象:"+quanYinPay.toString());
	//全银支付请求对象

	Map<String, String> mapContent = new HashMap<String, String>();
	mapContent.put("notifyUrl", quanYinPay.getNotifyUrl());
	mapContent.put("orderDate", quanYinPay.getOrderDate());
	mapContent.put("orderNo", quanYinPay.getOrderNo());
	mapContent.put("orderPeriod", quanYinPay.getOrderPeriod());
	mapContent.put("orderPrice", quanYinPay.getOrderPrice());
	mapContent.put("orderTime", quanYinPay.getOrderTime());
	mapContent.put("payKey", quanYinPay.getPayKey());
	mapContent.put("payTypeCode", quanYinPay.getPayTypeCode());
	mapContent.put("payWayCode", quanYinPay.getPayWayCode());
	mapContent.put("productName", quanYinPay.getProductName());
	mapContent.put("remark", quanYinPay.getRemark());
	mapContent.put("returnUrl", quanYinPay.getReturnUrl());
	mapContent.put("sign", quanYinPay.getSign());
	
	log.info(mapContent.toString());

	String returnStr = HttpClientUtil.doPost(Address, mapContent);

	QuanYinParameter contextReturn = JSONUtils.toBean(JSONObject.fromObject(returnStr), QuanYinParameter.class);
	log.info("接收的报文               ：" + returnStr);
	String codeUrl = "";
	if (contextReturn.getResult().equals("success")) {
		codeUrl = URLEncoder.encode(contextReturn.getCode_url(),"UTF-8");
	} else {
		out.print(contextReturn.getMsg());
		return;
	}
	
	request.setAttribute("payType", payType);
	//response.sendRedirect(codeUrl);
%>
 
 <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta id="viewport" name="viewport"
	content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
<title>充值接口-提交信息处理</title>
<style>
body {
	text-align: center;
}

#weixinImg {
	margin-top: 150px;
	width: 280px;
}

p {
	font-size: 18px;
}
</style>
</head>

<body>
	<img id="weixinImg" src='<%=path%>/tools/getQrCodePic?code=<%=codeUrl%>'></img>
	<p>
		请使用<span id="pay-way">
		    <c:if test="${payType == 'ALIPAY' }">支付宝</c:if>
			<c:if test="${payType == 'WEIXIN' }">微信</c:if>
			<c:if test="${payType == 'JDQB' }">京东钱包</c:if>
			<c:if test="${payType == 'QQPAY' }">QQ钱包</c:if>
			<c:if test="${payType == 'UNIONPAY' }">银联二维码</c:if>
			</span>扫描二维码以完成支付
	</p>

</body>
</html>