<%@page import="com.team.lottery.pay.model.feizhupay.FeiZhuPay"%>
<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.team.lottery.service.QuickBankTypeService"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.util.IPUtil"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.team.lottery.vo.QuickBankType"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="java.net.URLEncoder"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.team.lottery.service.RechargeConfigService"%>
<%@page import="com.team.lottery.vo.RechargeConfig"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
	String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	String serialNumber = request.getParameter("serialNumber"); // 获取配置ID;
	String orderMoneyYuan = OrderMoney;

	ChargePayService chargePayService = (ChargePayService) ApplicationContextUtil.getBean("chargePayService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));

	log.info("发起了95支付，生成订单号[{}],支付金额[{}]分,PayId[{}],payType[{}],chargePayId[{}]",
		serialNumber, OrderMoney, payId, payType, chargePayId);
	String payUrl = chargePay.getPayUrl();
	String Address = chargePay.getAddress();
	log.info("payUrl: " + payUrl + " Address: " + Address);

	//同步跳转地址
	String notifyUrl = "";
	if (chargePay.getNotifyUrl().startsWith("http://") || chargePay.getNotifyUrl().startsWith("https://")) {
		notifyUrl = chargePay.getNotifyUrl();
	} else {
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
		+ path + "/";
		notifyUrl = basePath + chargePay.getNotifyUrl();
	}
	
	// 商户id
	String app_id = chargePay.getMemberId();
	
	// 将元转为分
	BigDecimal fenMoney = new BigDecimal(OrderMoney).multiply(new BigDecimal("100"));
	// 交易金额，单位分
	String amount = String.valueOf(fenMoney);
	// 商户订单号
	String order_no = serialNumber;
	// 支付类型
	String device = payId;
	// 通知地址，异步post通知
	String notify_url = chargePay.getReturnUrl();
	// 通知地址，同步get通知
	String return_url = notifyUrl;
	// 1.直接渲染支付视图，2.返回JSON，默认值1
	String api_type = "1";
	// 签名
	String sign = "";
	// 商户密钥
	String app_secret = chargePay.getSign();
	
	//sign加密;
	String mark = "&";
	String md5SignStr = "app_id=" + app_id + mark +
						"amount=" + amount + mark +
						"order_no=" + order_no + mark +
						"device=" + device + mark +
						"app_secret=" + app_secret + mark +
						"notify_url=" + notify_url;

	//进行MD5熊谢加密
	sign = Md5Util.getMD5ofStr(md5SignStr).toLowerCase();
	//打印日志
	log.info("md5原始串: " + md5SignStr + "生成后的交易签名:" + sign);

	// 2表示返回JSON的情况
	if("2".equals(api_type)) {
		//通过JSON格式去提交数据,以便获取第三方跳转地址
		Map<String,String> dataMap = new HashMap<String,String>();
		dataMap.put("app_id", app_id);
		dataMap.put("amount", amount);
		dataMap.put("order_no", order_no);
		dataMap.put("device", device);
		dataMap.put("notify_url", notify_url);
		dataMap.put("return_url", return_url);
		dataMap.put("api_type", api_type);
		dataMap.put("sign", sign);
		
		//获取第三方给我们返回的JSON数据做判断跳转
		String result = HttpClientUtil.doGet(Address , dataMap);
		log.info("95支付提交支付报文，返回内容:{}", result);
		Boolean isJson = JSONUtils.isJson(result);
		//最终我们要生成二维码的数据
		String payInfoNew = "";
		String basePath = "";
		if (isJson == true) {
			JSONObject jsonObject = JSONUtils.toJSONObject(result);
			if (StringUtils.isBlank(result)) {
				out.print("发起支付失败");
				return;
			}
			//获取返回的状态.0表示下单有误 1表示下单成功
			String status = jsonObject.getString("code");
			if (status.equals("1")) {
				String qrcode_url = jsonObject.getString("qrcode_url");
				
				log.info("95支付获取到的支付二维码路径为: " + qrcode_url);
				response.sendRedirect(qrcode_url);
			} else {
				out.print(result);
				return;
			}
		} else {
			out.print(result);
			log.error("95支付支付发起失败: " + result);
			return;
		}
	}
%>


<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
<style>
body {
	text-align: center;
}

#weixinImg {
	margin-top: 150px;
	width: 280px;
}

p {
	font-size: 18px;
}
</style>
</head>

<body onload="pay.submit()">
<form method="get" name="pay" id="pay" action="<%=Address%>">
<TABLE>
<TR>
	<TD>
	<input name='app_id' type='hidden' value= "<%=app_id%>"/>
	<input name='amount' type='hidden' value= "<%=amount%>"/>
	<input name='order_no' type='hidden' value= "<%=order_no%>"/>
	<input name='device' type='hidden' value= "<%=device%>"/>
	<input name='notify_url' type='hidden' value= "<%=notify_url%>"/>
	<input name='return_url' type='hidden' value= "<%=return_url%>"/>		
	<input name='api_type' type='hidden' value= "<%=api_type%>" />
	<input name='sign' type='hidden' value= "<%=sign%>" />
	
	</TD>
</TR>
</TABLE>
	
</form>	

</body>
</html>