<%@page import="com.alibaba.fastjson.JSON"%>
<%@page import="com.team.lottery.pay.util.RSAUtils"%>
<%@page import="com.team.lottery.pay.model.xinfa.XinFaPay"%>
<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.team.lottery.service.QuickBankTypeService"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.util.IPUtil"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.team.lottery.vo.QuickBankType"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="java.net.URLEncoder"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.team.lottery.service.RechargeConfigService"%>
<%@page import="com.team.lottery.vo.RechargeConfig"%>
<%@page import="com.team.lottery.pay.util.RSA"%>
<%@ page import="com.team.lottery.pay.model.doudoupay.DouDouPay" %>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
	String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	String serialNumber = request.getParameter("serialNumber"); // 获取配置ID;
	String orderMoneyYuan = OrderMoney;


	if ("".equals(OrderMoney)) {
		OrderMoney = ("0");
	}


	Date currTime = new Date();
	//时间以yyyy-MM-dd HH:mm:ss的方式表示
	SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);
	//时间以yyyyMMDDHHmmss的方式表示 
	SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss", Locale.CHINA);
	String webdate = new String(formatter1.format(currTime));
	String TradeDate = new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService) ApplicationContextUtil.getBean("chargePayService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));

	log.info("发起了豆豆支付，生成订单号[{}],支付金额[{}]分,PayId[{}],payType[{}],chargePayId[{}]", serialNumber, OrderMoney, payId, payType, chargePayId);
	String payUrl = chargePay.getPayUrl();
	String Address = chargePay.getAddress();
	log.info("payUrl: " + payUrl + " Address: " + Address);

	//同步跳转地址
	String notifyUrl = "";
	if (chargePay.getNotifyUrl().startsWith("http://") || chargePay.getNotifyUrl().startsWith("https://")) {
		notifyUrl = chargePay.getNotifyUrl();
	} else {
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
		notifyUrl = basePath + chargePay.getNotifyUrl();
	}
	// 获取当前客户端IP
	// 获取当前用户的客户端IP.
	String userIp = IPUtil.getRequestIp(request);
	// 豆豆支付对象
	DouDouPay douDouPay = new DouDouPay();
	douDouPay.setMoney(OrderMoney);
	douDouPay.setName("豆豆支付");
	douDouPay.setPid(chargePay.getMemberId());
	//设置异步通知地址
	douDouPay.setNotify_url(chargePay.getReturnUrl());
	//设置支付成功跳转地址
	douDouPay.setReturn_url(notifyUrl);
	douDouPay.setOut_trade_no(serialNumber);
	douDouPay.setType(payId);

	//MD5加密;
	String mark = "&";
	String key = chargePay.getSign();
	String md5SignStr = "money=" + douDouPay.getMoney() + mark +
			"name=" + douDouPay.getName() + mark +
			"notify_url=" + douDouPay.getNotify_url() + mark +
			"out_trade_no=" + douDouPay.getOut_trade_no() + mark +
			"pid=" + douDouPay.getPid() + mark +
			"return_url=" + douDouPay.getReturn_url() + mark +
			"type=" + douDouPay.getType() + key;
	//进行MD5小写加密
	String md5Sign = Md5Util.getMD5ofStr(md5SignStr).toLowerCase();
	//打印日志
	log.info("md5原始串: " + md5SignStr + "加密后生成的交易签名:" + md5Sign);
	douDouPay.setSign(md5Sign);
	log.info("豆豆支付发起对象: ", douDouPay.toString());

%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>
<body onload="pay.submit()">
<form method="get" name="pay" id="pay" action="<%=payUrl%>">
	<TABLE>
		<TR>
			<TD>
				<input name='pid' type='hidden'value="<%= douDouPay.getPid()%>" />
				<input name='money' type='hidden' value="<%=douDouPay.getMoney()%>" />
				<input name='out_trade_no' type='hidden' value="<%=douDouPay.getOut_trade_no() %>" />
				<input name='notify_url' type='hidden' value="<%=douDouPay.getNotify_url()%>" />
				<input name='return_url' type='hidden' value="<%=douDouPay.getReturn_url()%>" />
				<input name='name' type='hidden' value="<%=douDouPay.getName()%>" />
				<input name='type' type='hidden' value="<%=douDouPay.getType()%>" />
				<input name='sign' type='hidden' value="<%=douDouPay.getSign()%>" />
			</TD>
		</TR>
	</TABLE>
</form>
</body>
</html>