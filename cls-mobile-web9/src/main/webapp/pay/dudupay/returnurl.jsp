<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@page import="java.io.*"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="net.sf.json.JSONObject"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.extvo.returnModel.DuDuPayReturnParam"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
    //获取当前的状态
     //获取当前的状态
	BufferedReader streamReader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
	StringBuilder responseStrBuilder = new StringBuilder();
	String inputStr;
	while ((inputStr = streamReader.readLine()) != null) {
		responseStrBuilder.append(inputStr);
	}
	log.info("嘟嘟嘟支付付回调通知报文: " + responseStrBuilder.toString());
	JSONObject jsonObject = JSONUtils.toJSONObject(responseStrBuilder.toString());
	DuDuPayReturnParam duDuPayReturnParam = JSONUtils.toBean(jsonObject, DuDuPayReturnParam.class);
	Integer merchant_id = duDuPayReturnParam.getMerchant_id();
	String order_id = duDuPayReturnParam.getOrder_id();
	String amount = duDuPayReturnParam.getAmount();
	Integer pay_method = duDuPayReturnParam.getPay_method();
	String sign = duDuPayReturnParam.getSign();
    log.info("嘟嘟支付成功通知平台处理信息如下: 商户ID:"+merchant_id+" 订单号:"+order_id+"订单金额:"+amount+" 元,支付方式:"+pay_method+"签名:"+sign);
    
    RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(order_id);
	if(rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1){
		log.error("根据订单号[" + order_id + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知嘟嘟付支付报文接收成功
		response.getOutputStream().write("success".getBytes("UTF-8"));
	}
	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
	if(rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + order_id + "]查找充值订单记录为空");
		//通知嘟嘟支付报文接收成功
		response.getOutputStream().write("success".getBytes("UTF-8"));
	}		
	//计算签名
	String Md5key = rechargeWithDrawOrder.getSign();
	String MARK = "&";
	//需要加密的字符串
	String MD5Str="merchant_id="+merchant_id+MARK+"order_id="+order_id+MARK+"amount="+amount+MARK+"sign="+Md5key;
	log.info("当前MD5源码:"+MD5Str);
	//Md5加密串并且大写
	String WaitSign = Md5Util.getMD5ofStr(MD5Str);
	log.info("加密后的MD5: " + WaitSign);

	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if (WaitSign.compareTo(sign) == 0) {
		boolean dealResult = false;
		//获取当前订单的订单价格,并且将后面的0去掉!!
		String money = rechargeWithDrawOrder.getApplyValue().toString();
		if (money.indexOf(".") > 0) {
			money = money.replaceAll("0+?$", "");//去掉多余的0  
			money = money.replaceAll("[.]$", "");//如最后一位是.则去掉  
		}
		if (!StringUtils.isEmpty(amount) && amount.equals(money)) {
			BigDecimal factMoneyValue = new BigDecimal(amount);
			try {
				dealResult = rechargeWithDrawOrderService.rechargeOrderSuccessDeal(order_id, factMoneyValue);
			} catch (UnEqualVersionException e) {
				log.error("嘟嘟支付充值时发现版本号不一致,订单号[" + order_id + "]");
				//启动新进程进行多次重试
				UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, order_id,
						factMoneyValue, "");
				thread.start();
			} catch (Exception e) {
				//异常不进行重试
				log.error(e.getMessage());
			}
		} else {
			if (!amount.equals(money)) {
				log.info("嘟嘟支付宝处理错误支付结果：" + amount.equals(money));
			}
		}
		log.info("嘟嘟支付付订单处理入账结果:{}", dealResult);
		//通知嘟嘟付报文接收成功
		out.println("success");
		return;
	} else {
		log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容[" + WaitSign + "]");
		//让第三方继续补发
		out.println("success");
		return;
	}
%>