<%@page import="com.team.lottery.pay.model.QuePay"%>
<%@page import="com.team.lottery.pay.model.DuDuPay"%>
<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.team.lottery.service.QuickBankTypeService"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.vo.QuickBankType"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@page import="com.team.lottery.service.RechargeConfigService"%>
<%@page import="com.team.lottery.vo.RechargeConfig"%>
<%
	//打印日志
	Logger log = LoggerFactory.getLogger(this.getClass());
	//获取当前web路径
	String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	String orderMoneyYuan = OrderMoney;
	String serialNumber = request.getParameter("serialNumber");
	//判断当前请求页面用户是否登录,没有的话就跳往登录页面
	User currentUser = (User) request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	if (currentUser == null) {
		response.sendRedirect(path + "/gameBet/login.html");
	}
	if (orderMoneyYuan.indexOf(".") > 0) {
		orderMoneyYuan = orderMoneyYuan.replaceAll("0+?$", "");//去掉多余的0  
		orderMoneyYuan = orderMoneyYuan.replaceAll("[.]$", "");//如最后一位是.则去掉  
	}
	//获取订单提交时间
	Date currTime = new Date();
	QuickBankTypeService quickBankTypeService = (QuickBankTypeService) ApplicationContextUtil.getBean("quickBankTypeService");
	ChargePayService chargePayService = (ChargePayService) ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService) ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.getPayMemmberMsg(Long.valueOf(chargePayId));
	
	//打印日志
	log.info("业务系统[{}],用户名[{}],发起了雀付支付，生成订单号[{}],支付金额[{}],PayId[{}],payType[{}],chargePayId[{}]", currentUser.getBizSystem(), currentUser.getUserName(), serialNumber,
			OrderMoney, payId, payType, chargePayId);
	QuickBankType query = new QuickBankType();
	query.setPayId(payId);
	String bankType = "";
	List<QuickBankType> list = quickBankTypeService.getQuickBankTypeQuery(query);
	if (list.size() > 0) {
		bankType = list.get(0).getBankType();
	}

	//生成需要跳转的支付地址;
	String payUrl = chargePay.getPayUrl();//转发支付网关地址
	String Address = chargePay.getAddress(); //最终地址

	log.info("嘟嘟支付报文转发地址payUrl: " + payUrl + ",实际网关地址address: " + Address);
	//同步跳转地址
	String notifyUrl = "";
	if (chargePay.getNotifyUrl().startsWith("http://") || chargePay.getNotifyUrl().startsWith("https://")) {
		notifyUrl = chargePay.getNotifyUrl();
	} else {
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
		notifyUrl = basePath + chargePay.getNotifyUrl();
	}
	//嘟嘟支付对象
	DuDuPay duduPay = new DuDuPay();
	//设置嘟嘟支付的商户号
	duduPay.setMerchant_id(Integer.valueOf(chargePay.getMemberId()));
	//设置嘟嘟支付订单号
	duduPay.setOrder_id(serialNumber);
	//设置嘟嘟支付金额
	duduPay.setAmount(orderMoneyYuan);
	//设置异步通知地址
	duduPay.setNotify_url(chargePay.getReturnUrl());
	//设置支付成功跳转地址
	duduPay.setReturn_url(notifyUrl);
	//设置支付方式;
	duduPay.setPay_method(Integer.valueOf(payId));
	//sign加密;
	String mark = "&";
	//需要加密的字符串
	String Md5Sign = "merchant_id=" + duduPay.getMerchant_id() + mark + "order_id=" + duduPay.getOrder_id() + mark + "amount=" + duduPay.getAmount() + mark + "sign="
			+ chargePay.getSign();
	//进行MD5小写加密
	String Signature = Md5Util.getMD5ofStr(Md5Sign);
	//打印日志
	log.info("md5原始串: " + Md5Sign + "生成后的交易签名:" + Signature);
	//设置签名
	duduPay.setSign(Signature);
	log.info("交易报文: " + duduPay);
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>

<body onload="pay.submit()">
	<form method="get" name="pay" id="pay" action="<%=payUrl%>">
		<TABLE>
			<TR>
				<TD>
				<input name='order_id' type='hidden'value="<%=duduPay.getOrder_id()%>" /> 
				<input name='merchant_id' type='hidden' value="<%=duduPay.getMerchant_id()%>" /> 
				<input name='pay_method' type='hidden' value="<%=duduPay.getPay_method() %>" /> 
				<input name='amount' type='hidden' value="<%=duduPay.getAmount()%>" />
				<input name='notify_url' type='hidden' value="<%=duduPay.getNotify_url()%>" /> 
				<input name='sign' type='hidden' value="<%=duduPay.getSign()%>" />
				</TD>
			</TR>
		</TABLE>
	</form>
</body>
</html>
