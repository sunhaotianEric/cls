<%@page
		import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
		 pageEncoding="utf-8"%>
<%@page import="com.team.lottery.service.QuickBankTypeService"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.util.IPUtil"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.team.lottery.vo.QuickBankType"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="java.net.URLEncoder"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.team.lottery.service.RechargeConfigService"%>
<%@page import="com.team.lottery.vo.RechargeConfig"%>
<%@page import="java.util.Arrays"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
	String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号
	String payType = request.getParameter("payType");//充值类型
	String serialNumber = request.getParameter("serialNumber"); // 获取配置ID;
	String orderMoneyYuan = OrderMoney;
	// 微信扫码.微信wap.支付宝wap金额特殊处理.只支持30  50  100  200  300
	if (payId.equals("902") || payId.equals("914") || payId.equals("904")) {
		try {
			Integer [] canPayMone = {30,50,100,200,300};
			Integer wechatPayMoney = Integer.valueOf(OrderMoney);
			boolean isContains  = Arrays.asList(canPayMone).contains(wechatPayMoney);
			if(!isContains){
				out.print("该支付方式金额为30,50,100,200,300元,请重新发起支付");
				return;
			}
		} catch (NumberFormatException e) {
			out.print("该支付方式金额为30,50,100,200,300元,请重新发起支付");
			return;
		}
	}

	User currentUser = (User) request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	if (currentUser == null) {
		response.sendRedirect(path + "/gameBet/login.html");
	}
	Date currTime = new Date();
	//时间以yyyy-MM-dd HH:mm:ss的方式表示
	SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);
	//时间以yyyyMMDDHHmmss的方式表示
	SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss", Locale.CHINA);
	String webdate = new String(formatter1.format(currTime));
	String TradeDate = new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService) ApplicationContextUtil.getBean("chargePayService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));

	log.info("业务系统[{}],用户名[{}],发起了盛世支付，生成订单号[{}],支付金额[{}]分,PayId[{}],payType[{}],chargePayId[{}]", currentUser.getBizSystem(),
			currentUser.getUserName(), serialNumber, OrderMoney, payId, payType, chargePayId);
	String payUrl = chargePay.getPayUrl();
	String Address = chargePay.getAddress();
	log.info("payUrl: " + payUrl + " Address: " + Address);

	//同步跳转地址
	String notifyUrl = "";
	if (chargePay.getNotifyUrl().startsWith("http://") || chargePay.getNotifyUrl().startsWith("https://")) {
		notifyUrl = chargePay.getNotifyUrl();
	} else {
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
		notifyUrl = basePath + chargePay.getNotifyUrl();
	}
	// 获取当前客户端IP
	// 获取当前用户的客户端IP.
	String userIp = IPUtil.getRequestIp(request);
	//sign加密;

	SortedMap<Object, Object> postDataMap = new TreeMap<Object, Object>();
	postDataMap.put("pay_memberid", chargePay.getMemberId());
	postDataMap.put("pay_orderid", serialNumber);
	postDataMap.put("pay_applydate", webdate);
	postDataMap.put("pay_bankcode", payId);
	postDataMap.put("pay_notifyurl", chargePay.getReturnUrl());
	postDataMap.put("pay_callbackurl", notifyUrl);
	postDataMap.put("pay_amount", OrderMoney);
	String md5Sign = Md5Util.createSign("pay_md5sign", postDataMap, chargePay.getSign());

	//设置签名

	//通过JSON格式去提交数据,以便获取第三方跳转地址
	Map<String, String> dataMap = new HashMap<String, String>();
	dataMap.put("pay_memberid", chargePay.getMemberId() );
	dataMap.put("pay_orderid", serialNumber);
	dataMap.put("pay_applydate", webdate);
	dataMap.put("pay_bankcode", payId);
	dataMap.put("pay_notifyurl", chargePay.getReturnUrl());
	dataMap.put("pay_callbackurl", notifyUrl);
	dataMap.put("pay_amount", OrderMoney);
	dataMap.put("pay_md5sign", md5Sign);
	dataMap.put("pay_returnType", "html");
	dataMap.put("clientip", userIp);

	/*//获取第三方给我们返回的JSON数据做判断跳转
	String result = HttpClientUtil.doPost(Address, dataMap);
	log.info("天道支付提交支付报文，返回内容:{}", result);
	Boolean isJson = JSONUtils.isJson(result);
	//最终我们要生成二维码的数据
	String payInfoNew = "";
	String basePath = "";
	if (isJson == true) {
		JSONObject jsonObject = JSONUtils.toJSONObject(result);
		if (StringUtils.isBlank(result)) {
			out.print("发起支付失败");
			return;
		}
		//获取返回的状态.0为发起成功,其他为发起失败.
		String status = jsonObject.getString("status");
		if ("0000".equals(status)) {
			// 获取跳转地址
			String pay_info = jsonObject.getString("pay_info");
			// 跳转到对应页面..
			response.sendRedirect(pay_info);
		} else {
			out.print(result);
			return;
		}
	} else {
		out.print(result);
		log.error("天道支付支付发起失败: " + result);
		return;
	}*/
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>充值接口-提交信息处理</title>
</head>

<body onload="pay.submit()">
<form method="post" name="pay" id="pay" action="<%=Address%>">
	<TABLE>
		<TR>
			<TD>
				<input type='hidden' name='pay_memberid' value= '<%=chargePay.getMemberId()%>'/>
				<input type='hidden' name='pay_orderid' value= '<%=serialNumber%>'/>
				<input type='hidden' name='pay_applydate' value= '<%=webdate%>'/>
				<input type='hidden' name='pay_bankcode' value= '<%=payId%>'/>
				<input type='hidden' name='pay_notifyurl' value= '<%=chargePay.getReturnUrl()%>'/>
				<input type='hidden' name='pay_callbackurl' value= '<%=notifyUrl%>'/>
				<input type='hidden' name='pay_amount' value= '<%=OrderMoney%>'/>
				<input type='hidden' name='pay_md5sign' value= '<%=md5Sign%>'/>
				<input type='hidden' name='pay_returnType' value= 'html'/>
				<input type='hidden' name='clientip' value= '<%=userIp%>'/>
			</TD>
		</TR>
	</TABLE>
</form>
</body>
</html>