<%@page import="java.io.IOException"%>
<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay,java.lang.Exception"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.RSA"%>
<%@page import="com.team.lottery.pay.model.JianZhengBaoPay"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
 <%@page errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.InetAddress"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="com.team.lottery.extvo.returnModel.FengLiPayParameter"%>
<%@page import="com.team.lottery.pay.model.LaKuPay"%>
<%@page import="java.util.*"%>
<%@page import="net.sf.json.JSONObject"%>
<%@ page import="com.team.lottery.util.HttpClientUtil"%>
<%@ page import="com.team.lottery.pay.util.RC4"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@page import="java.math.RoundingMode"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
    String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
    String orderMoneyYuan = OrderMoney;
    String serialNumber = request.getParameter("serialNumber");
 
    
    User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
	
    /* if (!"".equals(OrderMoney)){	
    	BigDecimal a;
    	a = new BigDecimal(OrderMoney).multiply(BigDecimal.valueOf(100)); //使用分进行提交
    	OrderMoney=String.valueOf(a.setScale(0));
    	}
    	else{
    		OrderMoney = ("0");
    }  */
    
    DecimalFormat formater = new DecimalFormat("#0.00");
    formater.setRoundingMode(RoundingMode.FLOOR);
    String realMoney=formater.format(Float.valueOf(OrderMoney));
	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));
	log.info("用户名称: "+currentUser.getUserName()+" 用户 ID号: "+currentUser.getId()+" 生成订单号: "+serialNumber+" 订单金额: "+OrderMoney);
	

	String payUrl = chargePay.getPayUrl();//转发凤梨网关地址
	String Address = chargePay.getAddress(); //最终凤梨地址
	log.info("payUrl: "+payUrl+" Address: "+Address);
	LaKuPay laKuPay =new LaKuPay();

	laKuPay.setCustomerid(Integer.parseInt(chargePay.getMemberId()));
	laKuPay.setBankcode(null);
	laKuPay.setNotifyurl(chargePay.getReturnUrl());
	laKuPay.setReturnurl(chargePay.getNotifyUrl());
	if(payType.equals("WEIXIN")){
		laKuPay.setPaytype("wxh5");
	}else if(payType.equals("ALIPAY")){
		/* laKuPay.setPaytype("alipaywap"); */
		laKuPay.setPaytype("redalipay");
	}
	laKuPay.setRemark("cz");
	laKuPay.setSdorderno(serialNumber);
	laKuPay.setTotal_fee(new BigDecimal(realMoney));
	laKuPay.setVersion("1.0");
	
	//生成md5密钥签名version={value}&customerid={value}&total_fee={value}&sdorderno={value}&notifyurl={value}&returnurl={value}&apikey
	//生成md5密钥签名
    String Md5key = chargePay.getSign();///////////md5密钥（KEY）
	String MARK = "&";
	String md5 = new String("version=" + laKuPay.getVersion() + MARK + "customerid=" + laKuPay.getCustomerid() + MARK
			+ "total_fee=" + laKuPay.getTotal_fee() + MARK + "sdorderno=" + laKuPay.getSdorderno() + MARK + "notifyurl="
			+ laKuPay.getNotifyurl() + MARK + "returnurl=" + laKuPay.getReturnurl() + MARK + Md5key);//MD5签名格式
	String Signature = md5Util.getMD5ofStr(md5);//计算MD5值

	System.out.println("sign:" + Signature);
	laKuPay.setSign(Signature);
%>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>

<body onload="pay.submit()">
<form method="post" name="pay" id="pay" action="<%=Address%>">
<TABLE>
<TR>
	<TD>
	<input name='version' type='hidden' value= "<%=laKuPay.version%>"/>
	<input name='customerid' type='hidden' value= "<%=laKuPay.customerid%>"/>
	<input name='sdorderno' type='hidden' value= "<%=laKuPay.sdorderno%>"/>
	<input name='total_fee' type='hidden' value= "<%=laKuPay.total_fee%>"/>
	<input name='paytype' type='hidden' value= "<%=laKuPay.paytype%>"/>
	<input name='bankcode' type='hidden' value= "<%=laKuPay.bankcode%>"/>		
	<input name='notifyurl' type='hidden' value= "<%=laKuPay.notifyurl%>" />
	<input name='returnurl' type='hidden' value= "<%=laKuPay.returnurl%>" />
	<input name='remark' type='hidden' value= "<%=laKuPay.remark%>"/>
	<input name='sign' type='hidden' value= "<%=laKuPay.sign%>"/>
	</TD>
</TR>
</TABLE>
	
</form>	

</body>
</html>
 