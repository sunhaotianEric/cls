<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@page import="java.io.*"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="net.sf.json.JSONObject"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.extvo.returnModel.YinHePayReturnParam"%>
<%@ page import="com.team.lottery.pay.util.RSA"%>
<%@page import="com.team.lottery.extvo.returnModel.BeiFuBaoParameterHead"%>
<%@page import="com.team.lottery.vo.ChargePay,com.team.lottery.extvo.ChargePayVo,com.team.lottery.service.ChargePayService"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@ page import="com.team.lottery.pay.util.RC4"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
<%@ page import="org.springframework.web.multipart.MultipartHttpServletRequest"%>
<%@ page import="org.springframework.web.multipart.MultipartResolver"%>
<%@ page import="org.springframework.web.multipart.commons.CommonsMultipartResolver"%>
<%@ page import="org.springframework.web.multipart.MultipartFile"%>
<%
   	    
   		   Logger log = LoggerFactory.getLogger(this.getClass());
   	       String path = request.getContextPath(); 
   	   
   	        MultipartResolver resolver = new CommonsMultipartResolver(request.getSession().getServletContext());
   	        MultipartHttpServletRequest multipartRequest = resolver.resolveMultipart(request);
   	       
   	   		
   	    	   
   	    	String status=multipartRequest.getParameter("status");
   	    	String customerid=multipartRequest.getParameter("customerid");
   	    	String sdpayno=multipartRequest.getParameter("sdpayno");
   	    	String sdorderno=multipartRequest.getParameter("sdorderno");
   	    	String total_fee=multipartRequest.getParameter("total_fee");
   	    	String paytype=multipartRequest.getParameter("paytype");
   	    	String remark=multipartRequest.getParameter("remark");
   	    	String sign=multipartRequest.getParameter("sign");
   	    	
   	    	log.info("拉酷支付成功通知平台处理信息如下:订单号:" + sdorderno + "订单金额:" + total_fee + " 元,订单状态:" + status);

   	    	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService) ApplicationContextUtil.getBean("rechargeOrderService");
   	    	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(sdorderno);
   	    	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
   	    	Long chargePayId=null;
   	    	if(rechargeWithDrawOrders != null && rechargeWithDrawOrders.size()>0){
   	    	   chargePayId=rechargeWithDrawOrders.get(0).getChargePayId();
   	    	}
   	    	ChargePay chargePay = chargePayService.selectByPrimaryKey(chargePayId);
   	    	String api_code="";
   	    	if(chargePay!=null){
   	    		api_code=chargePay.getMemberId();
   	    	}
   	    	if (rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1) {
   	    		log.error("根据订单号[" + sdorderno + "]查找充值订单记录为空或者有两笔相同订单号订单");
   	    		//通知拉酷支付报文接收成功
   	    		response.getOutputStream().write("success".getBytes("UTF-8"));
   	    	}
   	    	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
   	    	if (rechargeWithDrawOrder == null) {
   	    		log.error("根据订单号[" + sdorderno + "]查找充值订单记录为空");
   	    		//通知拉酷支付报文接收成功
   	    		response.getOutputStream().write("success".getBytes("UTF-8"));
   	    	}
   	    	
   	    	//计算签名
   	    		// 签名customerid={value}&status={value}&sdpayno={value}&sdorderno={value}&total_fee={value}&paytype={value}&{apikey}
   	    		String Md5key = rechargeWithDrawOrder.getSign();
   	    		String MARK = "&";
   	    		String md5 = new String("customerid=" + customerid + MARK + "status=" + status + MARK
   	    				+ "sdpayno=" + sdpayno + MARK + "sdorderno=" + sdorderno + MARK + "total_fee="
   	    				+ total_fee + MARK + "paytype=" + paytype + MARK + Md5key);//MD5签名格式
   	    		// Md5加密串
   	    		String WaitSign = Md5Util.getMD5ofStr(md5);
   	    		log.info("加密后的MD5: " + WaitSign);
   	    		
   	    		
   	    	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
   	    		  if(WaitSign.compareTo(sign)==0){
   	    			boolean dealResult = false;
   	    			if(!StringUtils.isEmpty(status) && status.equals("1")){
   	    				BigDecimal factMoneyValue = new BigDecimal(total_fee);
   	    				try {
   	    					dealResult = rechargeWithDrawOrderService.rechargeOrderSuccessDeal(sdorderno,factMoneyValue);
   	    				}catch(UnEqualVersionException e) {
   	    			   		log.error("拉酷支付在线充值时发现版本号不一致,订单号["+sdorderno+"]");
   	    			   		//启动新进程进行多次重试
   	    			   		UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION
   	    			   				, sdorderno,factMoneyValue,"");
   	    			   		thread.start();
   	    				} catch(Exception e){
   	    					//异常不进行重试
   	    					log.error(e.getMessage());
   	    				}	
   	    			} else {
   	    				if(!status.equals("1")){
   	    					log.info("拉酷支付处理错误支付结果："+status);
   	    				}
   	    			}
   	    			log.info("拉酷支付订单处理入账结果:{}", dealResult);
   	    			//通知拉酷支付报文接收成功
   	    		    out.println("success");
   	    			return;
   	    		 }else{
   	    			log.info("MD5校验失败,报文返回MD5签名内容["+ sign +"],计算MD5签名内容[" + WaitSign + "]");
   	    			//让第三方继续补发
   	    		    out.println("error");
   	    			return;
   	    		}
   	    
    
%>