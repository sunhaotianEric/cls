<%@page import="com.team.lottery.pay.model.haitupay.HaiTuPay"%>
<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.team.lottery.service.QuickBankTypeService"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.util.IPUtil"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.team.lottery.vo.QuickBankType"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="java.net.URLEncoder"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.team.lottery.service.RechargeConfigService"%>
<%@page import="com.team.lottery.vo.RechargeConfig"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
	String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	String serialNumber = request.getParameter("serialNumber"); // 获取配置ID;
	String orderMoneyYuan = OrderMoney;
	if (!"".equals(OrderMoney)) 
	{	BigDecimal a;
		a = new BigDecimal(OrderMoney).multiply(BigDecimal.valueOf(100)); //使用分进行提交
		OrderMoney=String.valueOf(a.setScale(0));
	}
	else{
		OrderMoney = ("0");
	}
	User currentUser = (User) request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	if (currentUser == null) {
		response.sendRedirect(path + "/gameBet/login.html");
	}
	Date currTime = new Date();
	//时间以yyyy-MM-dd HH:mm:ss的方式表示
	SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);
	//时间以yyyyMMDDHHmmss的方式表示 
	SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss", Locale.CHINA);
	String webdate = new String(formatter1.format(currTime));
	String TradeDate = new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService) ApplicationContextUtil.getBean("chargePayService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));

	log.info("业务系统[{}],用户名[{}],发起了海图支付，生成订单号[{}],支付金额[{}]分,PayId[{}],payType[{}],chargePayId[{}]",
	currentUser.getBizSystem(), currentUser.getUserName(), serialNumber, OrderMoney, payId, payType,
	chargePayId);
	String payUrl = chargePay.getPayUrl();
	String Address = chargePay.getAddress();
	log.info("payUrl: " + payUrl + " Address: " + Address);

	//同步跳转地址
	String notifyUrl = "";
	if (chargePay.getNotifyUrl().startsWith("http://") || chargePay.getNotifyUrl().startsWith("https://")) {
		notifyUrl = chargePay.getNotifyUrl();
	} else {
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
		+ path + "/";
		notifyUrl = basePath + chargePay.getNotifyUrl();
	}
	// 获取当前客户端IP
	// 获取当前用户的客户端IP.
	String userIp = IPUtil.getRequestIp(request);
	// 海图支付对象
	HaiTuPay haiTuPay = new HaiTuPay();
	// 支付描述(使用用户名.)
	haiTuPay.setBody(currentUser.getUserName());
	// 商户号.
	haiTuPay.setMch_id(chargePay.getMemberId());
	// 随机字符串.
	haiTuPay.setNonce_str(TradeDate);
	// 回调地址.
	haiTuPay.setNotify_url(chargePay.getReturnUrl());
	// 订单号.
	haiTuPay.setOut_trade_no(serialNumber);
	// 支付码表.
	haiTuPay.setService(payId);
	// 发起用户IP
	haiTuPay.setSpbill_create_ip(userIp);
	// 支付金额(分)
	haiTuPay.setTotal_fee(OrderMoney);
	
	//MD5加密;
	String mark = "&";
	String key = chargePay.getSign();
	
	String md5SignStr = "body=" + haiTuPay.getBody() + mark +
						"mch_id=" + haiTuPay.getMch_id() + mark + 
						"nonce_str=" + haiTuPay.getNonce_str() + mark + 
						"notify_url=" + haiTuPay.getNotify_url() + mark + 
						"out_trade_no=" + haiTuPay.getOut_trade_no() + mark + 
						"service=" + haiTuPay.getService() + mark + 
						"spbill_create_ip=" + haiTuPay.getSpbill_create_ip() + mark + 
						"total_fee=" + haiTuPay.getTotal_fee() + mark + 
						"key=" + key;
						

	//进行MD5大写加密
	String md5Sign = Md5Util.getMD5ofStr(md5SignStr).toUpperCase();
	//打印日志
	log.info("md5原始串: " + md5SignStr + "加密后生成的交易签名:" + md5Sign);
	//设置签名
	haiTuPay.setSign(md5Sign);
	log.info(haiTuPay.toString());
	//通过JSON格式去提交数据,以便获取第三方跳转地址
	Map<String,String> jsonDataMap = new HashMap<String,String>();
	jsonDataMap.put("body", haiTuPay.getBody());
	jsonDataMap.put("mch_id", haiTuPay.getMch_id());
	jsonDataMap.put("nonce_str", haiTuPay.getNonce_str());
	jsonDataMap.put("notify_url", haiTuPay.getNotify_url());
	jsonDataMap.put("out_trade_no", haiTuPay.getOut_trade_no());
	jsonDataMap.put("service", haiTuPay.getService());
	jsonDataMap.put("spbill_create_ip", haiTuPay.getSpbill_create_ip());
	jsonDataMap.put("total_fee", haiTuPay.getTotal_fee());
	jsonDataMap.put("sign", haiTuPay.getSign());
	//获取第三方给我们返回的JSON数据做判断跳转
	String result = HttpClientUtil.doPost(Address, jsonDataMap);
	log.info("海图支付提交支付报文，返回内容:{}", result);
	Boolean isJson = JSONUtils.isJson(result);
	
	if (isJson == true) {
		JSONObject jsonObject = JSONUtils.toJSONObject(result);
		if (StringUtils.isBlank(result)) {
			out.print("发起支付失败");
			return;
		}
		//获取返回的状态.0为发起成功,其他为发起失败.
		String code = jsonObject.getString("code");
		if (code.equals("0")) {
			// 获取二维码支付路径.
			String qr_code = jsonObject.getString("qr_code");
			log.info("海图支付获取到的支付二维码路径为: " + qr_code);
			response.sendRedirect(qr_code);
		} else {
			out.print(result);
			return;
		}
	} else {
		out.print(result);
		log.error("海图支付支付发起失败: " + result);
		return;
	}
%>