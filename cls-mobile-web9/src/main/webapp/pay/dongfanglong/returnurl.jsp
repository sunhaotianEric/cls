<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@page import="java.io.*"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="net.sf.json.JSONObject"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.extvo.returnModel.BeiFuBaoPayReturnParam"%>
<%@ page import="com.team.lottery.pay.util.RSA"%>
<%@page import="com.team.lottery.extvo.returnModel.BeiFuBaoParameterHead"%>
<%@page import="com.team.lottery.vo.ChargePay,com.team.lottery.extvo.ChargePayVo,com.team.lottery.service.ChargePayService"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
    //获取当前的状态
	BufferedReader streamReader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
	StringBuilder responseStrBuilder = new StringBuilder();
	String inputStr;
	while ((inputStr = streamReader.readLine()) != null) {
		responseStrBuilder.append(inputStr);
	}
	String resonseStr = responseStrBuilder.toString();
	log.info("北付宝支付付回调通知报文: " + resonseStr);
	String memberOrderNumber = JSONObject.fromObject(responseStrBuilder.toString()).getString("memberOrderNumber");
	log.info("memberOrderNumber: " + memberOrderNumber);
	String context = JSONObject.fromObject(responseStrBuilder.toString()).getString("context");
	log.info("context: " + context);
	
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService) ApplicationContextUtil.getBean("rechargeOrderService");
	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(memberOrderNumber); 
	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	Long chargePayId=null;
	if(rechargeWithDrawOrders != null && rechargeWithDrawOrders.size()>0){
	   chargePayId=rechargeWithDrawOrders.get(0).getChargePayId();
	}
	/* ChargePayVo chargePayVo=new ChargePayVo();
	chargePayVo.setChargeType("BEIFUBAOPAY"); */
	//chargePayVo.setMemberId(BeiFuBaoParameterHead.getMemberNumber());
	ChargePay chargePay = chargePayService.selectByPrimaryKey(chargePayId);
	String privateKey="";
	if(chargePay!=null){
		privateKey=chargePay.getPrivatekey();
	}
    String decryptContext = RSA.decryptByPrivateKey(context, privateKey);
    log.info("北付宝支付付回调通知报文解密后: " + decryptContext);
	JSONObject jsonObject = JSONUtils.toJSONObject(decryptContext);
	String businessContext = jsonObject.getString("businessContext");
	log.info("businessContext:" + businessContext);
	String businessHead = jsonObject.getString("businessHead");
	log.info("businessHead:" + businessHead);
	
	BeiFuBaoParameterHead BeiFuBaoParameterHead = JSONUtils.toBean(JSONObject.fromObject(businessHead) ,BeiFuBaoParameterHead.class);
	log.info("[北付宝通讯报文头部]" + BeiFuBaoParameterHead.toString());
		
	BeiFuBaoPayReturnParam beiFuBaoPayReturnParam = JSONUtils.toBean(JSONObject.fromObject(businessContext), BeiFuBaoPayReturnParam.class);
	memberOrderNumber=beiFuBaoPayReturnParam.getMemberOrderNumber();
	String lxbOrderNumber=beiFuBaoPayReturnParam.getLxbOrderNumber();
	String orderStatus=beiFuBaoPayReturnParam.getOrderStatus();
	String defrayalType=beiFuBaoPayReturnParam.getDefrayalType();
	String currenciesType=beiFuBaoPayReturnParam.getCurrenciesType();
	String tradeAmount=beiFuBaoPayReturnParam.getTradeAmount();
	String tradeFee=beiFuBaoPayReturnParam.getTradeFee();
	String paymentTime=beiFuBaoPayReturnParam.getPaymentTime();
	String payBank=beiFuBaoPayReturnParam.getPayBank();
	String attach=beiFuBaoPayReturnParam.getAttach();
	String checkSimpleDate=beiFuBaoPayReturnParam.getCheckSimpleDate();
	log.info("北付宝支付成功通知平台处理信息如下:订单号:" + memberOrderNumber + "订单金额:" + tradeAmount + " 元,支付方式:" + defrayalType);

	/* RechargeWithDrawOrderService rechargeWithDrawOrderService = (RechargeWithDrawOrderService) ApplicationContextUtil.getBean("rechargeWithDrawOrderService");
	List<RechargeWithDrawOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(memberOrderNumber); */
	if (rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1) {
		log.error("根据订单号[" + memberOrderNumber + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知北付宝付支付报文接收成功
		response.getOutputStream().write("SUC".getBytes("UTF-8"));
	}
	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
	if (rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + memberOrderNumber + "]查找充值订单记录为空");
		//通知北付宝支付报文接收成功
		response.getOutputStream().write("SUC".getBytes("UTF-8"));
	}

	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
		/* if(WaitSign.compareTo(sign)==0){ */
			boolean dealResult = false;
			if(!StringUtils.isEmpty(orderStatus) && orderStatus.equals("SUCCESS")){
				BigDecimal factMoneyValue = new BigDecimal(tradeAmount).divide(new BigDecimal(100));
				try {
					dealResult = rechargeWithDrawOrderService.rechargeOrderSuccessDeal(memberOrderNumber,factMoneyValue);
				}catch(UnEqualVersionException e) {
			   		log.error("北付宝支付在线充值时发现版本号不一致,订单号["+memberOrderNumber+"]");
			   		//启动新进程进行多次重试
			   		UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION
			   				, memberOrderNumber,factMoneyValue,"");
			   		thread.start();
				} catch(Exception e){
					//异常不进行重试
					log.error(e.getMessage());
				}	
			} else {
				if(!orderStatus.equals("SUC")){
					log.info("北付宝支付处理错误支付结果："+orderStatus);
				}
			}
			log.info("北付宝支付订单处理入账结果:{}", dealResult);
			//通知北付宝支付报文接收成功
		    out.println("SUC");
			return;
		 /* }else{
			log.info("MD5校验失败,报文返回MD5签名内容["+ sign +"],计算MD5签名内容[" + WaitSign + "]");
			//让第三方继续补发
		    out.println("ERROR");
			return;
		} */
%>