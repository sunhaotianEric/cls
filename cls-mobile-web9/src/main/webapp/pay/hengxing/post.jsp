<%@page
        import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<%@page import="com.team.lottery.util.IPUtil"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.team.lottery.pay.model.hengxingpay.HengXingPay" %>
<%
    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
    String OrderMoney = request.getParameter("OrderMoney");//订单金额
    String payId = request.getParameter("PayId");//支付的银行接口
    String chargePayId = request.getParameter("chargePayId");//第三方ID号
    String payType = request.getParameter("payType");//充值类型
    String serialNumber = request.getParameter("serialNumber"); // 获取配置ID;
    if(!"".equals(OrderMoney)) {
        BigDecimal a;
        a = new BigDecimal(OrderMoney); //使用分进行提交
        OrderMoney=String.valueOf(a.multiply(new BigDecimal(100)).intValue());
    }
    else{
        OrderMoney = ("0");
    }
    Date currTime = new Date();
    //时间以yyyy-MM-dd HH:mm:ss的方式表示
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);
    //时间以yyyyMMDDHHmmss的方式表示
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss", Locale.CHINA);
    String webdate = new String(formatter1.format(currTime));
    String TradeDate = new String(formatter2.format(currTime));

    ChargePayService chargePayService = (ChargePayService) ApplicationContextUtil.getBean("chargePayService");
    ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));

    log.info("发起了恒星支付，生成订单号[{}],支付金额[{}]分,PayId[{}],payType[{}],chargePayId[{}]",
            serialNumber, OrderMoney, payId, payType, chargePayId);
    String payUrl = chargePay.getPayUrl();
    String Address = chargePay.getAddress();
    log.info("payUrl: " + payUrl + " Address: " + Address);

    //同步跳转地址
    String notifyUrl = "";
    if (chargePay.getNotifyUrl().startsWith("http://") || chargePay.getNotifyUrl().startsWith("https://")) {
        notifyUrl = chargePay.getNotifyUrl();
    } else {
        String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
                + path + "/";
        notifyUrl = basePath + chargePay.getNotifyUrl();
    }
    // 获取当前客户端IP
    // 获取当前用户的客户端IP.
    String userIp = IPUtil.getRequestIp(request);
    // 新建HengXingPay支付对象.
    HengXingPay hengXingPay = new HengXingPay();
    // 支付金额分.
    hengXingPay.setAmount(Integer.parseInt(OrderMoney));
    // 回调地址.
    hengXingPay.setBackUrl(chargePay.getReturnUrl());
    // 设置客户端IP地址.
    hengXingPay.setIp(userIp);
    // 设置商户号.
    hengXingPay.setMcnNum(chargePay.getMemberId());
    // 设置平台订单号.
    hengXingPay.setOrderId(serialNumber);
    // 设置支付码表.
    hengXingPay.setPayType(Integer.parseInt(payId));
    //sign加密;
    hengXingPay.setSign(hengXingPay.genReqSign("72cd835fd1d8b0130a6d3f1a19a2499c"));

    //打印日志
    log.info("生成后的交易签名:" + hengXingPay.getSign());

    //通过JSON格式去提交数据,以便获取第三方跳转地址
    JSONObject jsonDataMap = new JSONObject();
    jsonDataMap.put("amount", String.valueOf(hengXingPay.getAmount()));
    jsonDataMap.put("backUrl", hengXingPay.getBackUrl());
    jsonDataMap.put("mcnNum", hengXingPay.getMcnNum());
    jsonDataMap.put("orderId", hengXingPay.getOrderId());
    jsonDataMap.put("ip", hengXingPay.getIp());
    jsonDataMap.put("payType", String.valueOf(hengXingPay.getPayType()));
    jsonDataMap.put("sign", hengXingPay.getSign());
    log.info("恒星支付请求参数doPost3[{}],tostring[{}], hengXingPay[{}]", jsonDataMap, jsonDataMap.toString(), hengXingPay.getPayType());
    //获取第三方给我们返回的JSON数据做判断跳转
    String result = HttpClientUtil.doPostSSL(Address, jsonDataMap);
    log.info("恒星支付提交支付报文，返回内容:{}", result);
    Boolean isJson = JSONUtils.isJson(result);

    if (isJson == true) {
        JSONObject jsonObject = JSONUtils.toJSONObject(result);
        if (StringUtils.isBlank(result)) {
            out.print("发起支付失败");
            return;
        }
        //获取返回的状态.0为发起成功,其他为发起失败.
        String status = jsonObject.getString("status");
        if (status.equals("0")) {
            // 获取二维码支付路径.
            String qrCode = jsonObject.getString("qrCode");
            log.info("恒星支付获取到的支付二维码路径为: " + qrCode);
            response.sendRedirect(qrCode);
        } else {
            out.print(result);
            return;
        }
        log.info("恒星支付交易报文: " + hengXingPay);
    } else {
        out.print(result);
        log.error("恒星支付支付发起失败: " + result);
        return;
    }
%>