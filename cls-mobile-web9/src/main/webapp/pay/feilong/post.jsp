<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.pay.model.FeiLongPay"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
 <%@page errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URL"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="com.team.lottery.extvo.returnModel.FeiLongPayReturnParam"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@page import="com.team.lottery.vo.RechargeConfig"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.service.RechargeConfigService"%>
<%@page import="java.net.InetAddress"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	String serialNumber = request.getParameter("serialNumber"); // 获取配置ID;
    String orderMoneyYuan = OrderMoney;
 
    
    User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
	
	Date currTime = new Date();
    //时间以yyyy-MM-dd HH:mm:ss的方式表示
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.CHINA);
    //时间以yyyyMMDDHHmmss的方式表示 
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss",Locale.CHINA);
    String webdate=new String(formatter1.format(currTime));
    String TradeDate=new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));
	
	log.info("用户名称: "+currentUser.getUserName()+" 用户 ID号: "+currentUser.getId()+" 生成订单号: "+serialNumber+" 订单金额: "+OrderMoney);
	
	
	 if (!"".equals(OrderMoney)){	
	BigDecimal a;
	a = new BigDecimal(OrderMoney).multiply(BigDecimal.valueOf(100)); //使用分进行提交
	OrderMoney=String.valueOf(a.setScale(0));
	}
	else{
		OrderMoney = ("0");
	} 
	String payUrl = chargePay.getPayUrl();//转发支付网关地址
	String Address = chargePay.getAddress(); //最终菲龙地址
	log.info("payUrl: "+payUrl+" Address: "+Address);
	
	//菲龙支付请求对象
	FeiLongPay feiLongPay =new FeiLongPay();
	feiLongPay.setCompanyId(chargePay.getMemberId());
	feiLongPay.setUserOrderId(serialNumber);
	feiLongPay.setFee(OrderMoney);
	feiLongPay.setCallbackUrl(chargePay.getNotifyUrl());
	feiLongPay.setSyncUrl(chargePay.getReturnUrl());
	InetAddress address = InetAddress.getLocalHost();    
    String ip=address .getHostAddress().toString();   
    feiLongPay.setIp(ip);
    feiLongPay.setMobile(String.valueOf(currentUser.getId()));
    feiLongPay.setPayType(payId);
    feiLongPay.setItem("cz");
	//生成md5密钥签名
    String Md5key = chargePay.getSign();// md5密钥（KEY）
	String MARK = "_";
	String md5 =new String(feiLongPay.getCompanyId()+MARK+feiLongPay.getUserOrderId()+MARK+feiLongPay.getFee()+MARK+chargePay.getSign());//MD5签名格式
	String Signature = md5Util.getMD5ofStr(md5);// 计算MD5值
    //交易签名
    feiLongPay.setSign(Signature);
    log.info("md5原始串: "+md5 + "生成后的交易签名:" + Signature);
	log.info("实际菲龙支付网关地址["+Address +"],交易报文: "+feiLongPay);
	
	String codeUrl="";
	String strPara="companyId="+URLEncoder.encode(feiLongPay.getCompanyId(), "utf-8") + "&userOrderId="
			+ URLEncoder.encode(feiLongPay.getUserOrderId(), "utf-8") + "&payType="
			+ URLEncoder.encode(feiLongPay.getPayType(), "utf-8") + "&item="
			+ URLEncoder.encode(feiLongPay.getItem(), "utf-8") + "&fee="
			+ URLEncoder.encode(feiLongPay.getFee(), "utf-8") + "&callbackUrl="
			+ URLEncoder.encode(feiLongPay.getCallbackUrl(), "utf-8") + "&syncUrl="
			+ URLEncoder.encode(feiLongPay.getSyncUrl(), "utf-8") + "&ip="
			+ URLEncoder.encode(feiLongPay.getIp(), "utf-8") + "&mobile="
			+ URLEncoder.encode(feiLongPay.getMobile(), "utf-8")+"&sign="
			+ URLEncoder.encode(feiLongPay.getSign(), "utf-8");
	URL url = new URL(Address+"?"+strPara);
    
	//openConnection函数会根据URL的协议返回不同的URLConnection子类的对象
	//这里URL是一个http,因此实际返回的是HttpURLConnection 
	HttpURLConnection httpConn = (HttpURLConnection) url
	.openConnection();

	//进行连接,实际上request要在下一句的connection.getInputStream()函数中才会真正发到 服务器****待验证
	httpConn.setDoOutput(true);
	httpConn.setDoInput(true);
	httpConn.connect();
	/* PrintWriter outWrite = new PrintWriter(httpConn.getOutputStream());
	
	outWrite.print(strPara);
    // flush输出流的缓冲
    outWrite.flush(); */
	// 取得输入流，并使用Reader读取
	BufferedReader reader = new BufferedReader(new InputStreamReader(
	httpConn.getInputStream(),"utf-8"));

	String lines = "";
	String str="";
	while ((str=reader.readLine()) != null) {
		lines += str; 
	}
	reader.close();
	log.info("菲龙支付返回报文:" + lines);
	
	FeiLongPayReturnParam returnParameter = JSONUtils.toBean(lines,
			FeiLongPayReturnParam.class);
	if (returnParameter.getResult().equals("0")) {
		 codeUrl = returnParameter.getParam();
		/* response.sendRedirect(returnParameter.getBarCode());
		return; */
	} else {
		out.print(returnParameter.getMsg());
		return;
	}
	log.info("接收菲龙支付转发成功,商户号[" + feiLongPay.getCompanyId() + "],充值金额[" + feiLongPay.getFee()
	+ "],银行码表[" + feiLongPay.getPayType() + "],提交金阳支付处理");
    
	request.setAttribute("payType", payType);
	/* request.setAttribute("codeUrl", codeUrl); */
	response.sendRedirect(codeUrl);
%>
 
 <%-- <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta id="viewport" name="viewport"
	content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
<title>充值接口-提交信息处理</title>
<style>
body {
	text-align: center;
}

#weixinImg {
	margin-top: 150px;
	width: 280px;
}

p {
	font-size: 18px;
}
</style>
</head>

<body>
	<img id="weixinImg" src='<%=codeUrl%>'></img>
	<img src="http://qr.topscan.com/api.php?bg=f3f3f3&fg=ff0000&gc=222222&el=l&w=684&m=30&text=<%=code%>"/>
	<p>
		请使用<span id="pay-way"><c:if test="${payType == 'ALIPAY' }">支付宝</c:if>
			<c:if test="${payType == 'WEIXIN' }">微信</c:if>
			<c:if test="${payType == 'QQPAY' }">QQ钱包</c:if>
			<c:if test="${payType == 'JDPAY' }">京东扫码</c:if>
			<c:if test="${payType == 'UNIONPAY' }">银联扫码</c:if></span>扫描二维码以完成支付
	</p>

</body>
</html>
 --%>