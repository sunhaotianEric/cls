package com.team.lottery.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

/**
 * spring MVC 统一异常处理
 * @author luocheng
 *
 */
public class SpringHandlerExceptionResolver implements HandlerExceptionResolver{

	@Override
	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler,
			Exception ex) {
		// TODO Auto-generated method stub
		return null;
	}

//	private static Logger logger = LoggerFactory.getLogger(SpringHandlerExceptionResolver.class);
//
//    private FastJsonConfig fastJsonConfig;
//    
//	@Override
//	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler,
//			Exception ex) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//	
//	/**
//     * 这个方法是拷贝 {@link org.springframework.web.servlet.mvc.support.DefaultHandlerExceptionResolver#doResolveException},
//     * 加入自定义处理，实现对400， 404， 405， 406， 415， 500(参数问题导致)， 503的处理
//     *
//     * @param ex      异常信息
//     * @param request 当前请求对象(用于判断当前请求是否为ajax请求)
//     * @return 视图模型对象
//     */
//    private ModelAndView specialExceptionResolve(Exception ex, HttpServletRequest request) {
//        try {
//            if (ex instanceof NoSuchRequestHandlingMethodException 
//                || ex instanceof NoHandlerFoundException) {
//                return result(HttpExceptionEnum.NOT_FOUND_EXCEPTION, request);
//            }
//            else if (ex instanceof HttpRequestMethodNotSupportedException) {
//                return result(HttpExceptionEnum.NOT_SUPPORTED_METHOD_EXCEPTION, request);
//            }
//            else if (ex instanceof HttpMediaTypeNotSupportedException) {
//                return result(HttpExceptionEnum.NOT_SUPPORTED_MEDIA_TYPE_EXCEPTION, request);
//            }
//            else if (ex instanceof HttpMediaTypeNotAcceptableException) {
//                return result(HttpExceptionEnum.NOT_ACCEPTABLE_MEDIA_TYPE_EXCEPTION, request);
//            }
//            else if (ex instanceof MissingPathVariableException) {
//                return result(HttpExceptionEnum.NOT_SUPPORTED_METHOD_EXCEPTION, request);
//            }
//            else if (ex instanceof MissingServletRequestParameterException) {
//                return result(HttpExceptionEnum.MISSING_REQUEST_PARAMETER_EXCEPTION, request);
//            }
//            else if (ex instanceof ServletRequestBindingException) {
//                return result(HttpExceptionEnum.REQUEST_BINDING_EXCEPTION, request);
//            }
//            else if (ex instanceof ConversionNotSupportedException) {
//                return result(HttpExceptionEnum.NOT_SUPPORTED_CONVERSION_EXCEPTION, request);
//            }
//            else if (ex instanceof TypeMismatchException) {
//                return result(HttpExceptionEnum.TYPE_MISMATCH_EXCEPTION, request);
//            }
//            else if (ex instanceof HttpMessageNotReadableException) {
//                return result(HttpExceptionEnum.MESSAGE_NOT_READABLE_EXCEPTION, request);
//            }
//            else if (ex instanceof HttpMessageNotWritableException) {
//                return result(HttpExceptionEnum.MESSAGE_NOT_WRITABLE_EXCEPTION, request);
//            }
//            else if (ex instanceof MethodArgumentNotValidException) {
//                return result(HttpExceptionEnum.NOT_VALID_METHOD_ARGUMENT_EXCEPTION, request);
//            }
//            else if (ex instanceof MissingServletRequestPartException) {
//                return result(HttpExceptionEnum.MISSING_REQUEST_PART_EXCEPTION, request);
//            }
//            else if (ex instanceof BindException) {
//                return result(HttpExceptionEnum.BIND_EXCEPTION, request);
//            }
//            else if (ex instanceof AsyncRequestTimeoutException) {
//                return result(HttpExceptionEnum.ASYNC_REQUEST_TIMEOUT_EXCEPTION, request);
//            }
//        } catch (Exception handlerException) {
//            logger.warn("Handling of [" + ex.getClass().getName() + "] resulted in Exception", handlerException);
//        }
//        return null;
//    }
//    
//    /**
//     * 判断是否ajax请求
//     *
//     * @param request 请求对象
//     * @return true:ajax请求  false:非ajax请求
//     */
//    private boolean isAjax(HttpServletRequest request) {
//        return "XMLHttpRequest".equalsIgnoreCase(request.getHeader("X-Requested-With"));
//    }
//
//    /**
//     * 返回异常信息
//     *
//     * @param httpException 异常信息
//     * @param request 请求对象
//     * @return 模型视图对象
//     */
//    private ModelAndView result(HttpExceptionEnum httpException, HttpServletRequest request) {
//        logger.warn("请求处理失败，请求url=[{}], 失败原因 : {}", request.getRequestURI(), httpException.getMessage());
//        if (isAjax(request)) {
//            return jsonResult(httpException.getCode(), httpException.getMessage());
//        } else {
//            return normalResult(httpException.getMessage(), "/error");
//        }
//    }
//    
//    /**
//     * 返回错误页
//     *
//     * @param message 错误信息
//     * @param url     错误页url
//     * @return 模型视图对象
//     */
//    private ModelAndView normalResult(String message, String url) {
//        Map<String, String> model = new HashMap<String, String>();
//        model.put("errorMessage", message);
//        return new ModelAndView(url, model);
//    }
//
//    /**
//     * 返回错误数据
//     *
//     * @param message 错误信息
//     * @return 模型视图对象
//     */
//    private ModelAndView jsonResult(int code, String message) {
//        ModelAndView mv = new ModelAndView();
//        FastJsonJsonView view = new FastJsonJsonView();
//        view.setFastJsonConfig(fastJsonConfig);
//        view.setAttributesMap((JSONObject) JSON.toJSON(JsonResult.fail(code, message)));
//        mv.setView(view);
//        return mv;
//    }
}
