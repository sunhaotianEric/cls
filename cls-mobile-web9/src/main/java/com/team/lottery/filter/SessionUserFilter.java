package com.team.lottery.filter;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.team.lottery.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.util.ConstantUtil;

public class SessionUserFilter implements Filter {

	private static Logger log = LoggerFactory.getLogger(SessionUserFilter.class);
	// 不需要拦截的url放在set当中用set数据防止重复
	private static Set<String> allowUrl = new HashSet<String>();

	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		HttpSession session = req.getSession();

		String url = req.getServletPath();
		String scheme = request.getScheme();
		String serverName = req.getHeader("REMOTE-HOST");
		if(StringUtils.isEmpty(serverName)) serverName = req.getServerName();
		int port = req.getServerPort();
		String basePath = req.getContextPath();
		String fullUrl = scheme + "://" + serverName + ":" + port + req.getContextPath() + url;
		log.debug("filter url:{}" + fullUrl);

//		BizSystemDomain domainInfo = ClsCacheManager.getDomainInfoByUrl(serverName);

		// 强制跳转https的判断
//		if (domainInfo != null && domainInfo.getIsHttps() == 1 && !"https".equals(scheme)) {
//			basePath = "https://" + serverName + basePath;
//
//			// 进行跳转
//			if (req.getQueryString() != null && !"".equals(req.getQueryString())) {
//				res.sendRedirect(basePath + url + "?" + req.getQueryString());
//			} else {
//				res.sendRedirect(basePath + url);
//			}
//			return;
//		}


//		String bizSystem = ClsCacheManager.getBizSystemByDomainUrl(serverName);
//		Boolean isTo404Page = false;
//		if (StringUtils.isNotBlank(bizSystem)) {
//			BizSystem bizSystemVo = ClsCacheManager.getBizSystemByCode(bizSystem);
//			// 如果系统停止使用
//			if (bizSystemVo.getEnable() == 0) {
//				isTo404Page = true;
//			}
//		} else {
//			// 获取到的字符串为空
//			isTo404Page = true;
//		}
//
//		// 判断能允许访问的域名
//		if (StringUtils.isNotBlank(SystemConfigConstant.allowUrls)) {
//			String[] allowUrlStrs = SystemConfigConstant.allowUrls.split(",");
//			for (String allowStrs : allowUrlStrs) {
//				if (allowStrs.equals(serverName)) {
//					isTo404Page = false;
//				}
//			}
//		}

//		if (isTo404Page) {
//			// 若识别不到业务系统，则跳转404页面
//			if (!url.contains("404.html")) {
//				res.sendRedirect(basePath + "/404.html");
//				return;
//			}
//		} else {
//			// 网站维护判断
//			BizSystemConfigVO bizSystemConfig = BizSystemConfigManager.getBizSystemConfig(bizSystem);
//			// 系统维护中
//			if (!url.contains("maintain.html") && bizSystemConfig != null && bizSystemConfig.getMaintainSwich() != null && bizSystemConfig.getMaintainSwich() == 1) {
//				res.sendRedirect(basePath + "/gameBet/maintain.html");
//				return;
//			}
//		}

		// 邀请码处理
//		String invitationCode = request.getParameter("code");
//		if (StringUtils.isNotBlank(invitationCode)) {
//			String sessionInvitationCode = (String) session.getAttribute(ConstantUtil.INVITAION_CODE);
//			// 之前session中没有设置过，则设置新的
//			if (StringUtils.isBlank(sessionInvitationCode)) {
//				session.setAttribute(ConstantUtil.INVITAION_CODE, invitationCode);
//			} else {
//				// 之前session有设置过,比较邀请码是不是新的，不一致则设置新的
//				if (!invitationCode.equals(sessionInvitationCode)) {
//					session.setAttribute(ConstantUtil.INVITAION_CODE, invitationCode);
//				}
//			}
//		}

		// 手机端前台登录页面
		String indexPageUrl = basePath + "/404";

		if (!isInAllowUrl(url)) {
			// 只要用户未登录 直接到首页
			if (session.getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER) == null) {
				res.sendRedirect(indexPageUrl);
				return;
			}
		} 
		chain.doFilter(request, response);
	}

	/**
	 * url判断方法：foreach遍历set元素与传入的url进行判断如果有包含的话返回true; 如果遍历完了没有包含的话返回false!
	 * 
	 * @param action
	 * @return
	 */
	public boolean isInAllowUrl(String url) {
		for (String s : allowUrl) {
			if (url.contains(s)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 在初始化方法中,向set当中添加不需要拦截的url
	 *
	 *
	 */
	@Override
	public void init(FilterConfig arg0) throws ServletException {
		//以下页面废弃，页面只有支付的 pay/***/post.html的页面
//		allowUrl.add("index.html");
//		allowUrl.add("404.html");
//		allowUrl.add("login.html");
//		allowUrl.add("maintain.html");
//		allowUrl.add("hall.html");
//		allowUrl.add("activity.html");
//		allowUrl.add("activityDetail.html");
//		allowUrl.add("wininfo.html");
//		allowUrl.add("lotterycode.html");
//		allowUrl.add("lotterycodeDetail.html");
//		allowUrl.add("reg.html");
//		allowUrl.add("award_message.html");
//		allowUrl.add("appdownload.html");
//		allowUrl.add("lotterynumber.html");
//		allowUrl.add("activity_detail.html");
//		allowUrl.add("monitor.html");
//		allowUrl.add("new_user_gift.html");
//		allowUrl.add("message.html");
//		allowUrl.add("message_detail.html");
//		allowUrl.add("announce.html");
//		allowUrl.add("announce_detail.html");
//		allowUrl.add("forgetPwd.html");
//		allowUrl.add("chooseWay.html");
//		allowUrl.add("verifyWay.html");
//		allowUrl.add("resetPwd.html");
//		allowUrl.add("newUserGift.html");
//		
//		//支付通知
//		allowUrl.add("returnurl.jsp");
//		allowUrl.add("pageurl.jsp");
		
		//druid数据源监控
		allowUrl.add("/druid");
	}

}
