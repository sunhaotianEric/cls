package com.team.lottery.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class PageController {

	/**
	 * 充值跳转支付第三方post页面!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/pay/{payType}/post.html", method = {RequestMethod.POST,RequestMethod.GET})
	public String toSkipPay(@PathVariable String payType) {
		String returnPath = "/pay/" + payType + "/post";
		return returnPath;
	}
	
   /** 
    * 默认首页控制器 
    */  
    @RequestMapping("/index")  
    public String login(HttpServletRequest request, HttpServletResponse response){  
        return "index";  
    } 

}
