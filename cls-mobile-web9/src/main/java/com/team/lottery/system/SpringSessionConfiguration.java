package com.team.lottery.system;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.session.events.SessionCreatedEvent;
import org.springframework.session.events.SessionDeletedEvent;
import org.springframework.session.events.SessionExpiredEvent;

/**
 * session配置类
 * 暂时无用，目前用SessionListener来实现，
 */
//@Configuration
public class SpringSessionConfiguration {
	
	private static Logger logger = LoggerFactory.getLogger(SpringSessionConfiguration.class);
	
	/**
	 * Redis内session过期事件监听
	 */
	@EventListener
	public void onSessionExpired(SessionExpiredEvent expiredEvent) {
		String sessionId = expiredEvent.getSessionId();
		logger.info("onSessionExpired sessionId:{}", sessionId);
	}


	/**
	 * Redis内session删除事件监听
	 */
	@EventListener
	public void onSessionDeleted(SessionDeletedEvent deletedEvent) {
		String sessionId = deletedEvent.getSessionId();
		logger.info("onSessionDeleted sessionId:{}", sessionId);
	}

	/**
	* Redis内session保存事件监听
	*/
	@EventListener
	public void onSessionCreated(SessionCreatedEvent createdEvent) {
		String sessionId = createdEvent.getSessionId();
		logger.info("onSessionCreated sessionId:{}", sessionId);
	}

}
