package com.team.lottery.system;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.util.ConstantUtil;
import com.team.lottery.vo.User;

public class SessionListener implements HttpSessionListener{
	
	private static Logger logger = LoggerFactory.getLogger(SessionListener.class);

	@Override
	public void sessionCreated(HttpSessionEvent se) {
		String sessionId = se.getSession().getId();
		if (logger.isDebugEnabled()) {
			logger.debug("sessionCreated sessionId:{}", sessionId);
		}
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		HttpSession session = se.getSession();
		String sessionId = se.getSession().getId();
		User loginUser = (User) session.getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
//		if (logger.isDebugEnabled()) {
//			logger.debug("sessionDestroyed sessionId:{}", sessionId);
//		}
		logger.info("sessionDestroyed sessionId:{}", sessionId);
		if (loginUser!=null) {
			String userName = loginUser.getUserName();
			String loginType = loginUser.getLoginType();
			String bizSystem = loginUser.getBizSystem();
			// 删除在线用户redis信息
			OnlineUserManager.delOnlineUser(loginType, bizSystem, userName, sessionId);
		}
		session.invalidate();
	}

}
