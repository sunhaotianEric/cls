#配置文件目录说明
+ pro  线上环境的配置文件目录
+ xn	 虚拟环境的配置文件目录
+ test 测试环境的配置文件目录


#pro 线上环境配置文件说明
* cls-mgr-web 后台应用
		
		mgr_application.properties 替换文件 WEB-INF/classes/application.propertie
* cls-mobile-web9  手机Web应用

		mobile9_application.properties 替换文件 /WEB-INF/classes/application.properties
* cls-web9	PC站点应用

		web9_application.properties 替换文件	/WEB-INF/classes/application.properties
* cls-opencode-private	开奖应用-私彩

		opencode-private_application.properties 替换文件 /WEB-INF/classes/application.properties
		applicationContext-quartz.xml 替换文件 /WEB-INF/classes/spring/applicationContext-quartz.xml （替换调度任务，只调度私彩的开奖线程，而官方彩的调度任务则从svn上的文件读取）
* cls-opencode-public	开奖应用-官彩

		opencode-public_application.properties 替换文件 /WEB-INF/classes/application.properties
* cls-pay-gateway	支付网关

		pay-gateway_application.properties 替换文件 /WEB-INF/classes/application.properties
* cls-codeinfo  开奖网

		codeinfo_application.properties 替换文件 /WEB-INF/classes/application.properties
		
#xn 虚拟环境配置文件说明
* cls-mgr-web 后台应用
		
		application.properties 替换文件 WEB-INF/classes/application.propertie
* cls-mobile-web9  手机Web应用

		application.properties 替换文件 /WEB-INF/classes/application.properties
* cls-web9	PC站点应用

		application.properties 替换文件	/WEB-INF/classes/application.properties
* cls-opencode	开奖应用

		application.properties 替换文件 /WEB-INF/classes/application.properties
		applicationContext-mq.xml 替换文件 /WEB-INF/classes/spring/applicationContext-mq.xml(配置了读取开奖号码的ActiveMQ通道的名称)
		applicationContext-quartz.xml 替换文件 /WEB-INF/classes/spring/applicationContext-quartz.xml （替换调度任务，不调度任何开奖线程，包括官方彩和私彩的，统一从ActiveMQ通道读取）
* cls-pay-gateway	支付网关

		application.properties 替换文件 /WEB-INF/classes/application.properties
	
#test 测试环境配置文件说明
* cls-mgr-web 后台应用
		
		application.properties 替换文件 WEB-INF/classes/application.propertie
* cls-mobile-web9  手机Web应用

		application.properties 替换文件 /WEB-INF/classes/application.properties
* cls-web9	PC站点应用

		application.properties 替换文件	/WEB-INF/classes/application.properties
* cls-opencode	开奖应用

		application.properties 替换文件 /WEB-INF/classes/application.properties
		applicationContext-mq.xml 替换文件 /WEB-INF/classes/spring/applicationContext-mq.xml(配置了读取开奖号码的ActiveMQ通道的名称)
		applicationContext-quartz.xml 替换文件 /WEB-INF/classes/spring/applicationContext-quartz.xml （替换调度任务，不调度任何开奖线程，包括官方彩和私彩的，统一从ActiveMQ通道读取）
* cls-pay-gateway	支付网关

		application.properties 替换文件 /WEB-INF/classes/application.properties