黑名单相关接口说明

1. 设置某房间里的某用户在黑名单中
	接口地址: /chatblacklist/addToBlackList
	输入示例: 无
	输入参数说明: {
					"roomId":8,
					"username":"jxcm10"
				}
	输出示例: data : [
						{
						    "code": "ok"
						}
					]
	输入参数说明:
		1) roomId 房间ID
		2) username	名字
	输出参数说明: 

		
2.当前系统中解除某房间某用户黑名单
	接口地址: /chatblacklist/relieveChatBlackList
	输入示例: 无
	输入参数说明: {"roomId":8,"username":["cs001","jxcm10"]}
	输出示例:
		成功：	{
				    "code": "ok"
				}
		失败：	{
				    "code": "ok",
				    "data": [
				        "jxcm08",
				        "jxcm10"
				    ]
				}
	输入参数说明:
		0) username	名字
		1) roomId 房间ID
	输出参数说明: 
		0) username	失败的名字
	
	
3.查询当前系统的某房间黑名单数据列表
	接口地址: /chatblacklist/getChatBlackList
	输入示例: 无
	输入参数说明: {
					"roomId":8
				}
	输出示例: 
				{
				    "code": "ok",
				    "data": [
				        {
				            "id": 3,
				            "roomId": 8,
				            "bizSystem": "k8cp",
				            "userName": "cs001",
				            "userId": 139,
				            "enabled": null,
				            "createTime": 1552025437000,
				            "updateTime": null,
				            "nick": "嘻嘻嘻哈哈",
				            "vipLevel": "4",
				            "headImg": "man_03.0837f309.jpg"
				        },
				        {
				            "id": 4,
				            "roomId": 8,
				            "bizSystem": "k8cp",
				            "userName": "jxcm10",
				            "userId": 99,
				            "enabled": null,
				            "createTime": 1552025482000,
				            "updateTime": null,
				            "nick": null,
				            "vipLevel": "1",
				            "headImg": "nba_08.07b58815.jpg"
				        }
				    ]
				}
			
	输入参数说明:
		0) roomId 聊天室号
	输出参数说明: 
		0) id ID
		1) bizSystem 业务系统
		2) roomId 聊天室号
		3) userId 用户ID
		4) userName	用户名
		5) enabled 激活黑名单 
		6) createTime 记录时间
		7) updateTime 修改时间
		8) nick 昵称
		9) vipLevel 会员等级
		10) headImg 头像
				