注册相关功能模块接口说明

1. 获取用户的注册链接地址
		接口地址: /register/userExtendQuery
		输入示例: {"pageNo":"1","query":{"maxPageNum":""}}
		输入参数说明: 
			1) pageNo 当前页面第几页
			2) query 团队查询对象
		输出示例: 
		{"data":{"pageNo":1,"pageSize":15,"nextPage":false,"prePage":false,"totalRecNum":5,"totalPageNum":1,"startIndex":0,"endIndex":5,"pageContent":[{"id":186,"bizSystem":"k8cp","userId":139,"userName":"cs001","uuid":"1395","domainId":null,"domainUrl":null,"mobileDomainId":null,"mobileDomainUrl":null,"orders":5,"linkSets":"null&1960&1960.00&1940&1960&1960&1960&1960.00&1960.00&1960.00&-&-","linkContent":"/gameBet/reg.html?code=81950143","mobileLinkContent":"/gameBet/reg.html?code=81950143","linkDes":null,"setCount":-1,"useCount":0,"yetUse":0,"loseDate":null,"createdDate":1540898360000,"enabled":1,"invitationCode":"81950143","resisterDonateMoney":null,"resisterDonatePoint":null,"sscrebate":null,"ksRebate":null,"syxwRebate":null,"pk10Rebate":null,"dpcRebate":null,"dailiLevel":null,"bonusScale":null,"continueDay":null,"domainEnabled":null,"maxPageNum":null,"bizSystemName":"K8彩票","createdDateStr":"2018/10/30 19:19:20","loseDateStr":""},{"id":40,"bizSystem":"k8cp","userId":139,"userName":"cs001","uuid":"1394","domainId":null,"domainUrl":null,"mobileDomainId":null,"mobileDomainUrl":null,"orders":4,"linkSets":"REGULARMEMBERS&1948&1948.00&1929&1948&1948&1948&1948.00&1948.00&1948.00&-&-","linkContent":"/gameBet/reg.html?code=94510537","mobileLinkContent":"/cls-mobile-web/reg.jsp?code=94510537","linkDes":"","setCount":-1,"useCount":1,"yetUse":1,"loseDate":null,"createdDate":1496476883000,"enabled":1,"invitationCode":"94510537","resisterDonateMoney":null,"resisterDonatePoint":null,"sscrebate":null,"ksRebate":null,"syxwRebate":null,"pk10Rebate":null,"dpcRebate":null,"dailiLevel":null,"bonusScale":null,"continueDay":null,"domainEnabled":null,"maxPageNum":null,"bizSystemName":"K8彩票","createdDateStr":"2017/06/03 16:01:23","loseDateStr":""},{"id":39,"bizSystem":"k8cp","userId":139,"userName":"cs001","uuid":"1393","domainId":null,"domainUrl":null,"mobileDomainId":null,"mobileDomainUrl":null,"orders":3,"linkSets":"ORDINARYAGENCY&1948&1948.00&1929&1948&1948&1948&1948.00&1948.00&1948.00&-&-","linkContent":"/gameBet/reg.html?code=87553785","mobileLinkContent":"/cls-mobile-web/reg.jsp?code=87553785","linkDes":"","setCount":-1,"useCount":0,"yetUse":0,"loseDate":null,"createdDate":1496476646000,"enabled":1,"invitationCode":"87553785","resisterDonateMoney":null,"resisterDonatePoint":null,"sscrebate":null,"ksRebate":null,"syxwRebate":null,"pk10Rebate":null,"dpcRebate":null,"dailiLevel":null,"bonusScale":null,"continueDay":null,"domainEnabled":null,"maxPageNum":null,"bizSystemName":"K8彩票","createdDateStr":"2017/06/03 15:57:26","loseDateStr":""},{"id":38,"bizSystem":"k8cp","userId":139,"userName":"cs001","uuid":"1392","domainId":null,"domainUrl":null,"mobileDomainId":null,"mobileDomainUrl":null,"orders":2,"linkSets":"ORDINARYAGENCY&1948&1948.00&1929&1948&1948&1948&1948.00&1948.00&1948.00&-&-","linkContent":"/gameBet/reg.html?code=50534559","mobileLinkContent":"/gameBet/reg.jsp?code=50534559","linkDes":"","setCount":-1,"useCount":0,"yetUse":0,"loseDate":null,"createdDate":1496476576000,"enabled":1,"invitationCode":"50534559","resisterDonateMoney":null,"resisterDonatePoint":null,"sscrebate":null,"ksRebate":null,"syxwRebate":null,"pk10Rebate":null,"dpcRebate":null,"dailiLevel":null,"bonusScale":null,"continueDay":null,"domainEnabled":null,"maxPageNum":null,"bizSystemName":"K8彩票","createdDateStr":"2017/06/03 15:56:16","loseDateStr":""},{"id":30,"bizSystem":"k8cp","userId":139,"userName":"cs001","uuid":"1391","domainId":null,"domainUrl":null,"mobileDomainId":null,"mobileDomainUrl":null,"orders":1,"linkSets":"ORDINARYAGENCY&1912&1912.00&1929&1948&1948&1948&1912.00&1912.00&1912.00&-&-","linkContent":"/gameBet/reg.html?code=03111742","mobileLinkContent":"/cls-mobile-web/reg.jsp?code=03111742","linkDes":"","setCount":-1,"useCount":2,"yetUse":1,"loseDate":null,"createdDate":1494223231000,"enabled":1,"invitationCode":"03111742","resisterDonateMoney":null,"resisterDonatePoint":null,"sscrebate":null,"ksRebate":null,"syxwRebate":null,"pk10Rebate":null,"dpcRebate":null,"dailiLevel":null,"bonusScale":null,"continueDay":null,"domainEnabled":null,"maxPageNum":null,"bizSystemName":"K8彩票","createdDateStr":"2017/05/08 14:00:31","loseDateStr":""}]},"code":"ok"}
		输出参数说明: 
			1) pageNo;    //当前页号
     		2) pageSize;  //每页记录条数
     		3) nextPage;  //是否有下一页
    		4) prePage;   //是否有上一页
     		5) totalRecNum;  //总共有多少条记录
     		6) totalPageNum;//总共多少页
     		7) startIndex; //记录开始位置
    		8) endIndex;   //记录结束位置
    		9) pageContent; //该页的数据(记录明细)
    		   记录明细:
    		    id	序号
				biz_system	业务系统
				user_id	
				user_name	
				regfrom	
				money	
				gain	
				belong_date	归属日期
				create_date	
				version	版本号
				recharge	存款总额
				rechargePresent	充值赠送总额
				activitiesMoney	活动彩金赠送总额
				systemAddMoney	系统续费
				systemReduceMoney	系统扣费
				manageReduceMoney	行政提出
				withDraw	取款总额
				withDrawFee	取款手续费总额
				lottery	总共投注额 -撤单
				lottery_effective	有效投注
				lottery_regression	投注撤单
				lottery_stop_after_number	追号撤单
				win	中奖金额
				rebate	返点金额
				percentage	提成金额
				payTranfer	
				incomeTranfer	提成金额
				halfMonthBonus	半月分红
				dayBonus	日分红
				monthSalary	月工资 
				daySalary	日工资
				dayCommission	日佣金
				other	其他金额
				reg_count	
				first_recharge_count	
				lottery_count

2. 生成推广，生成邀请码
		接口地址: /register/addUserExtend
		输入示例: {"dailiLevel":"ORDINARYAGENCY","sscrebate":1960,"syxwRebate":1940,"ksRebate":1960,"dpcRebate":1960,"pk10Rebate":1960}
		输入参数说明: 
			1) sscrebate;//时时彩模式
		    2) ksRebate;//快三模式
		    3) syxwRebate;//十一选五模式
		    4) pk10Rebate;//PK10模式
		    5) dpcRebate;//低频彩模式
		    6) dailiLevel;//代理级别
		输出示例: 
		{"data":{"id":186,"bizSystem":"k8cp","userId":139,"userName":"cs001","uuid":"1395","domainId":null,"domainUrl":null,"mobileDomainId":null,"mobileDomainUrl":null,"orders":5,"linkSets":"null&1960&1960.00&1940&1960&1960&1960&1960.00&1960.00&1960.00&-&-","linkContent":"/gameBet/reg.html?code=81950143","mobileLinkContent":"/gameBet/reg.html?code=81950143","linkDes":null,"setCount":null,"useCount":null,"yetUse":null,"loseDate":null,"createdDate":1540898360122,"enabled":null,"invitationCode":"81950143","resisterDonateMoney":null,"resisterDonatePoint":null,"sscrebate":1960,"ksRebate":1960,"syxwRebate":1940,"pk10Rebate":1960,"dpcRebate":1960,"dailiLevel":null,"bonusScale":null,"continueDay":null,"domainEnabled":null,"maxPageNum":null,"bizSystemName":"K8彩票","createdDateStr":"2018/10/30 19:19:20","loseDateStr":""},"code":"ok"}
		输出参数说明: 
			id	序号
			biz_system	业务系统
			user_id	
			user_name	用户名
			uuid	UUID
			invitation_code	邀请码
			domain_id	pc链接域名id
			domain_url	pc链接域名url
			mobile_domain_id	手机端链接域名id
			mobile_domain_url	手机端链接域名url
			orders	序号
			link_sets	注册链接设置信息
			link_content	注册链接内容
			mobile_link_content	手机端注册链接内容
			link_des	备注
			set_count	设定注册人数 -1表示无限制
			use_count	使用次数
			yet_use	是否已经使用
			lose_date	失效时间,为null的情况,代表永久有效
			created_date	创建时间
			enabled	是否停用

3. 获取推广链接详情
		接口地址: /register/getUserExtendById
		输入示例: {"linkId":"186"}
		输入参数说明: 
			1) linkId : 链接详情id
		输出示例: 
		{"data":{"id":186,"bizSystem":"k8cp","userId":139,"userName":"cs001","uuid":"1395","domainId":null,"domainUrl":null,"mobileDomainId":null,"mobileDomainUrl":null,"orders":5,"linkSets":"null&1960&1960.00&1940&1960&1960&1960&1960.00&1960.00&1960.00&-&-","linkContent":"/gameBet/reg.html?code=81950143","mobileLinkContent":"/gameBet/reg.html?code=81950143","linkDes":null,"setCount":-1,"useCount":0,"yetUse":0,"loseDate":null,"createdDate":1540898360000,"enabled":1,"invitationCode":"81950143","resisterDonateMoney":null,"resisterDonatePoint":null,"sscrebate":null,"ksRebate":null,"syxwRebate":null,"pk10Rebate":null,"dpcRebate":null,"dailiLevel":null,"bonusScale":null,"continueDay":null,"domainEnabled":null,"maxPageNum":null,"bizSystemName":"K8彩票","createdDateStr":"2018/10/30 19:19:20","loseDateStr":""},"data2":[{"id":95,"bizSystem":"k8cp","domainUrl":"k8app0011.com","domainType":"PC","applyStatus":1,"isHttps":0,"jumpMobileDomainUrl":"","firstFlag":0,"auditDesc":"虚拟 测试第三方支付 HTTPS","enable":1,"createTime":1540198412000,"updateTime":1540201476000,"updateAdmin":"whuser","bizSystemName":"K8彩票"},{"id":93,"bizSystem":"k8cp","domainUrl":"k8xn.boss288.com","domainType":"PC","applyStatus":1,"isHttps":0,"jumpMobileDomainUrl":null,"firstFlag":0,"auditDesc":"","enable":1,"createTime":1539762292000,"updateTime":1539762292000,"updateAdmin":"yyz","bizSystemName":"K8彩票"},{"id":91,"bizSystem":"k8cp","domainUrl":"k8xn.boss788.com","domainType":"PC","applyStatus":1,"isHttps":0,"jumpMobileDomainUrl":null,"firstFlag":0,"auditDesc":"","enable":1,"createTime":1539759917000,"updateTime":1539759917000,"updateAdmin":"whuser","bizSystemName":"K8彩票"},{"id":96,"bizSystem":"k8cp","domainUrl":"localhost","domainType":"PC","applyStatus":1,"isHttps":0,"jumpMobileDomainUrl":null,"firstFlag":1,"auditDesc":"","enable":1,"createTime":1540609301000,"updateTime":1540609301000,"updateAdmin":"yzm","bizSystemName":"K8彩票"},{"id":89,"bizSystem":"k8cp","domainUrl":"mgr.boss288.com","domainType":"PC","applyStatus":1,"isHttps":0,"jumpMobileDomainUrl":"","firstFlag":0,"auditDesc":"","enable":1,"createTime":1536826421000,"updateTime":1537096354000,"updateAdmin":"whuser","bizSystemName":"K8彩票"}],"data3":[{"id":53,"bizSystem":"k8cp","domainUrl":"mk8.boss288.com","domainType":"MOBILE","applyStatus":1,"isHttps":0,"jumpMobileDomainUrl":"","firstFlag":1,"auditDesc":"","enable":1,"createTime":1512646516000,"updateTime":1512646930000,"updateAdmin":"luocheng","bizSystemName":"K8彩票"}],"code":"ok"}
		输出参数说明: 
			id	序号
			biz_system	业务系统
			user_id	
			user_name	用户名
			uuid	UUID
			invitation_code	邀请码
			domain_id	pc链接域名id
			domain_url	pc链接域名url
			mobile_domain_id	手机端链接域名id
			mobile_domain_url	手机端链接域名url
			orders	序号
			link_sets	注册链接设置信息
			link_content	注册链接内容
			mobile_link_content	手机端注册链接内容
			link_des	备注
			set_count	设定注册人数 -1表示无限制
			use_count	使用次数
			yet_use	是否已经使用
			lose_date	失效时间,为null的情况,代表永久有效
			created_date	创建时间
			enabled	是否停用

4. 删除用户的开户链接
		接口地址: /register/deleteUserExtend
		输入示例: {"linkId":"186"}
		输入参数说明: 
			1) linkId : 链接详情id
		输出示例: 
		{"code":"ok"}
		输出参数说明: 

5. 返回注册页面参数
		接口地址: /register/regConfig
		输入示例: 
		输入参数说明: 
		输出示例: 
		{"data":{"regShowPhone":0,"regRequiredPhone":0,"regShowEmail":0,"regRequiredEmail":0,"regShowQq":1,"regRequiredQq":0,"regShowSms":0,"path":"/cls-mobile-web8","bizSystem":"k8cp","customUrl":"http://www.53kf.com/","registerCode":"","bizSystemInfo":{"id":9,"bizSystem":"k8cp","bizSystemName":"K8彩票","templateType":"BLUE","indexLotteryKind":"XYLHC","hotLotteryKinds":"AHKS,FCSDDPC,JYKS,BJPK10,JSKS,CQSSC,TJSSC,XJSSC,JSPK10,XYLHC","indexDefaultLotteryKinds":"AHKS,JYKS,BJPK10,JLFFC,JSPK10,LHC,XYLHC","indexMobileDefaultLotteryKinds":"JYKS,JSKS,CQSSC,JSPK10,XYLHC","logoUrl":"http://42.51.195.190/http://42.51.191.174/images/logo/defaultLogo.png","headLogoUrl":"http://42.51.195.190/images/logo/20180718203423222.png","mobileLogoUrl":"http://42.51.195.190/k8cp/logo/mobileLogo.png","customUrl":"http://www.53kf.com/","telphoneNum":"12345678901","qq":"","andriodIosDownloadUrl":"","appDownLogo":"http://42.51.195.190/images/barcode/20180718203452647.png","appShowname":null,"barcodeUrl":"http://42.51.195.190/images/barcode/20180718203436892.png","mobileBarcodeUrl":"http://42.51.195.190/images/barcode/20180718203441982.png","hasApp":1,"appBarcodeUrl":"http://42.51.195.190/images/barcode/20180718203457652.png","appAndroidDownloadUrl":"","appIosBarcodeUrl":"http://42.51.195.190/images/barcode/20180718203500700.png","appStoreIosUrl":"","createTime":1487739180000,"updateTime":1536996030000,"updateAdmin":"yyz","advantageDesc":null,"footer":"COPYRIGHT @ 2016 LOTTERY CARPARINTCN.AII RTGHIS RESCRVCD.","siteScript":"","mobileSiteScript":null,"faviconUrl":"http://42.51.195.190/k8cp/info/favicon.png","rechargeSecond":6.00,"withdrawSecond":9.00,"indexLotteryKindName":"幸运六合彩","indexDefaultLotteryKindsName":"安徽快3,幸运快3,北京PK10,幸运分分彩,极速PK10,六合彩,幸运六合彩","templateName":"蓝色"},"mobileWebRsVersion":"8.8","bizSystemConfigVO":{"directagencyNum":5,"genralagencyNum":10,"ordinayagencyNum":15,"yuanModel":1,"jiaoModel":1,"fenModel":1,"liModel":null,"haoModel":null,"canlotteryhightModel":1980,"isCQSSCOpen":1,"isJXSSCOpen":1,"isHLJSSCOpen":1,"isXJSSCOpen":1,"isTJSSCOpen":1,"isJLFFCOpen":1,"isHGFFCOpen":1,"isXJPLFCOpen":1,"isTWWFCOpen":1,"isGDSYXWOpen":1,"isSDSYXWOpen":1,"isJXSYXWOpen":1,"isCQSYXWOpen":0,"isFJSYXWOpen":1,"isJSKSOpen":1,"isAHKSOpen":1,"isJLKSOpen":1,"isHBKSOpen":1,"isBJKSOpen":1,"isJYKSOpen":1,"isGXKSOpen":1,"isGSKSOpen":1,"isSHKSOpen":1,"isGDKLSFOpen":1,"isSHSSLDPCOpen":1,"isFC3DDPCOpen":1,"isLHCOpen":1,"isXYLHCOpen":1,"isBJPK10Open":1,"isJSPK10Open":1,"isJSTBOpen":1,"isAHTBOpen":1,"isJLTBOpen":1,"isHBTBOpen":1,"isBJTBOpen":1,"isXYEBOpen":1,"withDrawIsAllowNotEnough":0,"exceedWithdrawExpenses":1.5,"withDrawOneStrokeLowestMoney":100,"withDrawOneStrokeHighestMoney":500000,"withDrawEveryDayTotalMoney":300000,"withDrawEveryDayNumber":10,"exceedTransferDayLimitExpenses":5000000,"isTransferSwich":1,"withDrawLotteryMultiple":1,"releasePolicyBonus":"MANANUAL_RELEASE","maintainSwich":0,"maintainContent":"平台系统正在维护中","releasePolicyDaySaraly":"MANANUAL_RELEASE","secretCode":"123456","defaultInvitationCode":"","isClose":0,"lhcLowerLotteryMoney":2,"lhcHighestLotteryMoney":5000000,"lhcHighestExpectLotteryMoney":100000000,"lhcMaxWinMoney":250000000,"alarmPhone":"","alarmEmail":"","alarmPhoneOpen":0,"alarmEmailOpen":0,"lotteryAlarmOpen":1,"lotteryAlarmValue":2000,"winAlarmOpen":0,"winAlarmValue":5000,"rechargeAlarmOpen":0,"rechargeAlarmValue":5000,"withdrawAlarmOpen":0,"withdrawAlarmValue":1000,"regShowPhone":0,"regRequiredPhone":0,"regShowSms":0,"regShowEmail":0,"regRequiredEmail":0,"regShowQq":1,"regRequiredQq":0,"guestModel":0,"allowGuestLogin":0,"guestMoney":20000,"isOpenLevelAward":1,"isOpenSignInActivity":1,"signinRechargeMoney":0,"signinLastTime":"23:59:59","isOpenPhone":0,"isOpenEmail":0,"isFootballOpen":0,"footballUrl":"www","regDirectLogin":"1","isNewUserGiftOpen":0,"appDownGiftMoney":0,"completeInfoGiftMoney":0,"complateSafeGiftMoney":0,"lotteryGiftMoney":0,"rechargeGiftMoney":0,"extendUserGiftMoney":0},"showRegisterCode":true},"code":"ok"}
		输出参数说明: 
			regShowPhone : 注册是否显示手机号码
			regRequiredPhone : 注册手机号码是否必填
			regShowEmail : 注册是否显示邮件
			regRequiredEmail : 注册邮件是否必填
			regShowQq : 注册是否显示QQ
			regRequiredQq : 注册QQ是否必填
			regShowSms : 是否开启短信验证
			invitationCode : 邀请码
			isShowInvitaCode : 是否展示注册邀请码.
			regDirectLogin : 注册完是否登录.
			regOpen : 是否开放注册.
			
