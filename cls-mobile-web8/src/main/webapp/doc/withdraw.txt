提现功能模块接口说明

1. 加载提现需要的信息，用户银行卡，用户余额等信息
		接口地址: /withdraw/loadWithdrawInfo
		输入示例: 无
		输入参数说明: 无
		输出示例: {"data":[{"id":101,"bizSystem":"k8cp","userId":139,"userName":"cs001","province":"天津市","city":"市辖区","area":"和平区","bankType":"CCB","bankName":"中国建设银行","subbranchName":"天津支行","bankAccountName":"张三","bankCardNum":"6217234301000697825","locked":1,"state":1,"createDate":1538462081000,"updateDate":1540641029000,"deleteDate":null,"deleteCause":null,"confirmBankNum":null,"bizSystemName":"K8彩票","createDateStr":"2018-10-02"},{"id":105,"bizSystem":"k8cp","userId":139,"userName":"cs001","province":"北京","city":"东城区","area":null,"bankType":"CCB","bankName":"中国建设银行","subbranchName":"","bankAccountName":"张三","bankCardNum":"6215593901002991578","locked":0,"state":1,"createDate":1540473579000,"updateDate":1540473579000,"deleteDate":null,"deleteCause":null,"confirmBankNum":null,"bizSystemName":"K8彩票","createDateStr":"2018-10-25"}],"data2":{"id":139,"bizSystem":"k8cp","code":29,"userName":"cs001","regfrom":"k8cpAdmin&","isTourist":0,"teamMemberCount":305,"downMemberCount":25,"password":"tch483e80ha7a0hth9ey71th8cy973h673924h","safePassword":"tch483e80ha7a0hth9ey71th8cy973h673924h","haveSafePassword":1,"haveSafeQuestion":1,"nick":"计划专员","phone":"13003947889","trueName":"张三","identityid":"","birthday":"","sex":1,"qq":"34567","headImg":null,"province":null,"city":null,"address":null,"addtime":1491751133000,"firstRechargeTime":1494315283000,"firstRechargeOrderId":null,"loginLock":0,"withdrawLock":0,"state":1,"dailiLevel":"DIRECTAGENCY","vipLevel":"4","starLevel":1,"levelName":"通判","email":"","logip":"0:0:0:0:0:0:0:1%0","lastMobileLogin":null,"loginTimes":800,"lastLogoutTime":1540369334000,"lastLoginTime":1540639247000,"sscRebate":1970.00,"ffcRebate":1970.00,"syxwRebate":1970.00,"ksRebate":1970.00,"pk10Rebate":1970.00,"dpcRebate":1970.00,"tbRebate":1970.00,"klsfRebate":1970.00,"lhcRebate":1970.00,"xyebRebate":null,"point":1973.00000,"bonusScale":0.00000,"bonusState":0,"salaryState":0,"version":20567,"money":2142675.59600,"canWithdrawMoney":500.00000,"iceMoney":0.00000,"totalCanWithdrawMoney":2117216.63600,"isOnline":null,"rebateCount":null,"operationType":null,"operationValue":null,"numberOfLower":null,"totalGain":null,"checkCode":null,"surePassword":null,"oldPassword":null,"loginType":null,"oldSafePassword":null,"sureSafePassword":null,"accountValue":null,"rememberme":null,"bizSystemName":"K8彩票","dailiLevelStr":"直属代理","addTimeStr":"2017/04/09 23:18:53","logTimeStr":"2018/10/27 19:20:47","lastlogoutTimeStr":"2018/10/27 19:20:47","firstRechargeTimeStr":"2017/05/09 15:34:43"},"code":"ok"}
		输出参数说明: 
			1) data 银行卡信息
					id  银行卡id
					bankType 银行卡类型 
					bankName 银行名称
					bankCardNum 银行卡号
			2) data2 用户信息
					money 用户余额
					canWithdrawMoney 可提现金额
		错误输出示例:{"data":"SafePasswordIsNull","code":"error"}
		正确输出示例:{"data":[{"id":101,"bizSystem":"k8cp","userId":139,"userName":"cs001","province":"天津市","city":"市辖区","area":"和平区","bankType":"CCB","bankName":"中国建设银行","subbranchName":"天津支行","bankAccountName":"张**","bankCardNum":"6217************825","locked":0,"state":1,"createDate":1538462081000,"updateDate":1538462081000,"deleteDate":null,"deleteCause":null,"confirmBankNum":null,"bizSystemName":"K8彩票","createDateStr":"2018-10-02"}],"data2":150000,"data3":66333}
		错误输出参数说明:
			1) data = "SafePasswordIsNull" 表示未设置安全密码
					= "BankCardIsNull" 表示未绑定银行卡
		正确输出参数说明:
			1) data  表示银行卡信息列表
			   data2  表示当前用户的余额
			   data3  表示当前用户的可提现余额
			
2. 申请提现
		接口地址: /withdraw/withDrawApply
		输入示例: {"bankId":"101","withDrawValue":"500","password":"a123456","isWithDrawFee":0}
		输入参数说明: 
			1) bankId	银行卡id
			2) withDrawValue 申请提现金额
			3) password 安全密码
			4) isWithDrawFee 是否同意手续费 1 同意  0 默认值
		输出示例: {"code":"ok"}
		输出参数说明: 提现成功
		错误输出示例: {"data":"FeeTip","data2":100.00000,"data3":1.5,"code":"error"}
		错误输出参数说明:
			1) data 当字符串为"FeeTip"时，表示提示要收取手续费，其他字符串则是错误信息
			2) data2 要收取手续费的金额(当data字符串为"FeeTip"时有此项返回)
			3) data3 手续费费率(当data字符串为"FeeTip"时有此项返回)
			
			
3. 查询提现订单记录
		接口地址: /withdraw/withDrawOrderQuery
		输入示例: {"query":{"queryScope":1,"statuss":"DEALING","dateRange" : "-1"},"pageNo":1}
		输入参数说明: 
			1) queryScope 查询范围 1 自己, 2 直接下级 3 查询所有下级
			2) statuss 处理状态：处理中、锁定中、提现成功、提现失败
			3) dateRange 查询时间段代号,1表示今天,-1表示昨天,-7表示最近7天,30表示本月,-30表示上个月
			4) pageNo 页数
		输出示例: {"data":{"pageNo":1,"pageSize":15,"nextPage":false,"prePage":false,"totalRecNum":2,"totalPageNum":1,"startIndex":0,"endIndex":2,"pageContent":[{"id":3713,"serialNumber":"TX1810271851233703001","adminId":73,"bizSystem":"k8cp","userId":139,"userName":"cs001","lockStatus":0,"lockAdmin":null,"chargePayId":null,"applyValue":898.75000,"accountValue":0.00000,"feeValue":1.25000,"currentCanWithdrawMoney":816.63600,"operateType":"WITHDRAW","payType":null,"refType":null,"thirdPayType":null,"fromType":null,"operateDes":"取现[中国建设银行]","postscript":null,"dealStatus":"DEALING","analyse":null,"bankName":"中国建设银行","subbranchName":"中国建设银行","bankAliasName":null,"bankUserName":"张三","bankType":"CCB","bankId":"6217234301000697825","bankUrl":null,"createdDate":1540637483000,"updateDate":null,"updateAdmin":null,"sign":null,"timeDifference":null,"barcodeUrl":null,"num":null,"bizSystemName":"K8彩票","updateDateStr":"","createdDateStr":"2018/10/27 18:51:23","statusStr":"处理中","payTypeStr":"","refTypeStr":"","operateTypeStr":"取现","thirdPayTypeStr":""},{"id":3712,"serialNumber":"TX1810271845318593000","adminId":73,"bizSystem":"k8cp","userId":139,"userName":"cs001","lockStatus":0,"lockAdmin":null,"chargePayId":null,"applyValue":100.00000,"accountValue":0.00000,"feeValue":0.00000,"currentCanWithdrawMoney":100.00000,"operateType":"WITHDRAW","payType":null,"refType":null,"thirdPayType":null,"fromType":null,"operateDes":"取现[中国建设银行]","postscript":null,"dealStatus":"DEALING","analyse":null,"bankName":"中国建设银行","subbranchName":"中国建设银行","bankAliasName":null,"bankUserName":"张三","bankType":"CCB","bankId":"6217234301000697825","bankUrl":null,"createdDate":1540637132000,"updateDate":null,"updateAdmin":null,"sign":null,"timeDifference":null,"barcodeUrl":null,"num":null,"bizSystemName":"K8彩票","updateDateStr":"","createdDateStr":"2018/10/27 18:45:32","statusStr":"处理中","payTypeStr":"","refTypeStr":"","operateTypeStr":"取现","thirdPayTypeStr":""}]},"code":"ok"}
		输出参数说明:
			1) pageContent 页面内容   
					serialNumber 交易流水号
					applyValue	申请金额
					accountValue	到账金额
					dealStatus	处理状态 "DEALING","处理中" "LOCKED","锁定中" "WITHDRAW_SUCCESS","提现成功" "WITHDRAW_FAIL","提现失败"
					statusStr	处理状态中文描述
					createdDateStr 	创建时间
					
					
4. 查询提现配置信息(最低单笔提现金额,最高单笔提现金额,每天最多提现多少次.)
		接口地址: /withdraw/getWithdrawConfig
		输入示例: 无
		输入参数说明: 无
		输出参数示例:{data: 100, data2: 500000, data3: 10, code: "ok"}
		输出参数说明:
					0)data : 最低提现金额.
					1)data2 : 最高提现金额.
					2)data3 : 每天最多提现次数.
						
					