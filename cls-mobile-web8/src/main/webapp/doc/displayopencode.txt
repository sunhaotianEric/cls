开奖网功能模块接口说明

1. 根据id获取官方开奖期号详情
		接口地址: /code/getLotteryCodeById
		输入示例: 
		{"id":1852378}
		输入参数说明: 
			id  序号
		输出示例: 
		{
		    "data": {
		        "id": 1852378,
		        "lotteryNum": "2019062",
		        "lotteryName": "FCSDDPC",
		        "opencodeStr": "4,0,8",
		        "lotteryTypeDes": "福彩3D",
		        "kjtimeStr": "2019/03/10 21:21:20",
		        "bizSystem": null
		    },
		    "code": "ok"
		}
		输出参数说明: 
			id : 序号
			lotteryNum : 期号
			lotteryName : 彩种名称
			opencodeStr : 开奖号
			lotteryTypeDes : 彩种描述
			kjtimeStr : 开奖日期
			bizSystem : (官方开奖期号详情没有)

2. 根据id获取私彩开奖期号详情
		接口地址: /code/getLotteryCodeXyById
		输入示例: 
		{"id":20300802}
		输入参数说明: 
			id  序号
		输出示例: 
		{
		    "data": {
		        "id": 20300802,
		        "lotteryNum": "20190320001",
		        "lotteryName": "WFSSC",
		        "opencodeStr": "0,9,1,7,3",
		        "lotteryTypeDes": "五分时时彩",
		        "kjtimeStr": "2019/03/20 00:05:35",
		        "bizSystem": "k8cp"
		    },
		    "code": "ok"
		}
		输出参数说明: 
			id : 序号
			lotteryNum : 期号
			lotteryName : 彩种名称
			opencodeStr : 开奖号
			lotteryTypeDes : 彩种描述
			kjtimeStr : 开奖日期
			bizSystem : (私彩)业务系统

3. 获取某个彩种最新一期开奖记录
		接口地址: /code/getLastLotteryCode
		输入示例: 
		{"lotteryKind":"LHC"}
		输入参数说明: 
			lotteryKind  彩种
		输出示例: 
		{
		    "data": {
		        "id": 1852400,
		        "lotteryNum": "2019029",
		        "lotteryName": "LHC",
		        "opencodeStr": "33,38,47,27,06,21,46",
		        "lotteryTypeDes": "六合彩",
		        "kjtimeStr": "2019/03/12 21:35:18",
		        "bizSystem": null
		    },
		    "code": "ok"
		}
		输出参数说明: 
			id : 序号
			lotteryNum : 期号
			lotteryName : 彩种名称
			opencodeStr : 开奖号
			lotteryTypeDes : 彩种描述
			kjtimeStr : 开奖日期
			bizSystem : (私彩)业务系统,官彩为null

4. 分页查询获取某个彩种最新几期开奖记录
		接口地址: /code/getLastServalLotteryCodes
		输入示例: 
		{"lotteryKind":"LHC","":1}
		输入参数说明: 
			lotteryKind  彩种
			pageNo  页码
		输出示例: 
		{
		    "data": {
		        "pageNo": 1,
		        "pageSize": 15,
		        "totalRecNum": 15,
		        "pageContent": [
		            {
		                "id": 1852400,
		                "lotteryNum": "2019029",
		                "lotteryName": "LHC",
		                "opencodeStr": "33,38,47,27,06,21,46",
		                "lotteryTypeDes": "六合彩",
		                "kjtimeStr": "2019/03/12 21:35:18",
		                "bizSystem": null
		            },
		            ...
		            {
		                "id": 1851499,
		                "lotteryNum": "2019007",
		                "lotteryName": "LHC",
		                "opencodeStr": "16,18,28,35,41,36,37",
		                "lotteryTypeDes": "六合彩",
		                "kjtimeStr": "2019/01/17 21:34:33",
		                "bizSystem": null
		            }
		        ]
		    },
		    "code": "ok"
		}
		输出参数说明: 
			pageNo : 当前页码
			pageSize : 每页记录个数
			totalRecNum : 总记录个数
			pageContent :
				id : 序号
				lotteryNum : 期号
				lotteryName : 彩种名称
				opencodeStr : 开奖号
				lotteryTypeDes : 彩种描述
				kjtimeStr : 开奖日期
				bizSystem : (私彩)业务系统,官彩为null

5. 分页获取所有彩种最新几期开奖数据
		接口地址: /code/getAllLastServalLotteryCode
		输入示例: 
		{"lotteryKind":"LHC","":1}
		输入参数说明: 
			lotteryKind  彩种
			pageNo  页码
		输出示例: 
		{
		    "data": {
		        "pageNo": 1,
		        "pageSize": 15,
		        "totalRecNum": 15,
		        "totalPageNum": 1,
		        "pageContent": [
		            //官彩部分
		            {
		                "id": 1852400,
		                "lotteryNum": "2019029",
		                "lotteryName": "LHC",
		                "opencodeStr": "33,38,47,27,06,21,46",
		                "lotteryTypeDes": "六合彩",
		                "kjtimeStr": "2019/03/12 21:35:18",
		                "bizSystem": null
		            },
		            ...
		            //私彩部分
		            ...
		            {
		                "id": 1851499,
		                "lotteryNum": "2019007",
		                "lotteryName": "LHC",
		                "opencodeStr": "16,18,28,35,41,36,37",
		                "lotteryTypeDes": "六合彩",
		                "kjtimeStr": "2019/01/17 21:34:33",
		                "bizSystem": null
		            }
		        ]
		    },
		    "code": "ok"
		}
		输出参数说明: 
			pageNo : 当前页码
			pageSize : 每页记录个数
			totalRecNum : 总记录个数
			pageContent :
				id : 序号
				lotteryNum : 期号
				lotteryName : 彩种名称
				opencodeStr : 开奖号
				lotteryTypeDes : 彩种描述
				kjtimeStr : 开奖日期
				bizSystem : (私彩)业务系统,官彩为null

6. 获取所有彩种最新一期开奖数据
		接口地址: /code/getAllLastLotteryCode
		输入示例: 
		输入参数说明: 
		输出示例: 
		{
		    "data": [
		        {
		            "id": 1852399,
		            "lotteryNum": "2019064",
		            "lotteryName": "FCSDDPC",
		            "opencodeStr": "5,8,7",
		            "lotteryTypeDes": "福彩3D",
		            "kjtimeStr": "2019/03/12 21:22:27",
		            "bizSystem": null
		        },
		        {
		            "id": 1852400,
		            "lotteryNum": "2019029",
		            "lotteryName": "LHC",
		            "opencodeStr": "33,38,47,27,06,21,46",
		            "lotteryTypeDes": "六合彩",
		            "kjtimeStr": "2019/03/12 21:35:18",
		            "bizSystem": null
		        },
		        {
		            "id": 20308400,
		            "lotteryNum": "201903140039",
		            "lotteryName": "JLFFC",
		            "opencodeStr": "5,6,5,5,0",
		            "lotteryTypeDes": "幸运分分彩",
		            "kjtimeStr": "2019/03/14 00:39:35",
		            "bizSystem": "k8cp"
		        },
		        {
		            "id": 20299830,
		            "lotteryNum": "20190313480",
		            "lotteryName": "SFKS",
		            "opencodeStr": "2,5,5",
		            "lotteryTypeDes": "三分快3",
		            "kjtimeStr": "2019/03/14 00:00:35",
		            "bizSystem": "k8cp"
		        },
		        {
		            "id": 20300244,
		            "lotteryNum": "20190315003",
		            "lotteryName": "SFSSC",
		            "opencodeStr": "9,0,3,9,2",
		            "lotteryTypeDes": "三分时时彩",
		            "kjtimeStr": "2019/03/15 00:09:20",
		            "bizSystem": "k8cp"
		        },
		        {
		            "id": 20335879,
		            "lotteryNum": "20190315278",
		            "lotteryName": "SFSYXW",
		            "opencodeStr": "11,05,09,01,04",
		            "lotteryTypeDes": "三分11选5",
		            "kjtimeStr": "2019/03/15 13:54:30",
		            "bizSystem": "k8cp"
		        },
		        {
		            "id": 20300478,
		            "lotteryNum": "20190318144",
		            "lotteryName": "SHFSSC",
		            "opencodeStr": "7,7,0,2,3",
		            "lotteryTypeDes": "老重庆时时彩",
		            "kjtimeStr": "2019/03/19 00:00:45",
		            "bizSystem": "k8cp"
		        },
		        {
		            "id": 20300802,
		            "lotteryNum": "20190320001",
		            "lotteryName": "WFSSC",
		            "opencodeStr": "0,9,1,7,3",
		            "lotteryTypeDes": "五分时时彩",
		            "kjtimeStr": "2019/03/20 00:05:35",
		            "bizSystem": "k8cp"
		        },
		        {
		            "id": 20335897,
		            "lotteryNum": "20190315167",
		            "lotteryName": "WFSYXW",
		            "opencodeStr": "01,02,11,06,04",
		            "lotteryTypeDes": "五分11选5",
		            "kjtimeStr": "2019/03/15 13:55:30",
		            "bizSystem": "k8cp"
		        }
		    ],
		    "code": "ok"
		}
		输出参数说明: 
			id : 序号
			lotteryNum : 期号
			lotteryName : 彩种名称
			opencodeStr : 开奖号
			lotteryTypeDes : 彩种描述
			kjtimeStr : 开奖日期
			bizSystem : (私彩)业务系统,官彩为null

7. 获取最新期号和剩余的销售时间，倒计时
		接口地址: /code/getExpectAndDiffTime
		输入示例: 
		输入参数说明: 
		输出示例: 
		{
		    "data": {
		        "id": null,
		        "lotteryName": "六合彩",
		        "lotteryType": "LHC",
		        "timeType": null,
		        "begintime": null,
		        "endtime": 1552570080000,
		        "restday": null,
		        "issale": null,
		        "lotteryNum": "2019030",
		        "state": null,
		        "isbd": null,
		        "specialTime": 1,
		        "specialExpect": 0,
		        "lotteryNumCount": null,
		        "timeDifference": 113063,
		        "currentDateStr": null,
		        "isClose": false,
		        "begintimeStr": "",
		        "endtimeStr": "2019/03/14 21:28:00"
		    },
		    "code": "ok"
		}
		输出参数说明: 
			id : 序号
			lotteryType :   彩种
			lotteryName : 彩种描述
			endtime : 截止时间
			lotteryNum : 期号
			timeDifference : 时间差
			isClose : 是否封盘，六合彩用到
			endtimeStr : 截止时间显示
				
