<%@ page language="java" contentType="text/html; charset=utf-8"
import="com.team.lottery.system.BizSystemConfigVO,com.team.lottery.system.BizSystemConfigManager,
         com.team.lottery.vo.LotterySet,com.team.lottery.cache.ClsCacheManager,
         com.team.lottery.system.BizSystemConfigManager,com.team.lottery.vo.BizSystemInfo,
         com.team.lottery.util.LotterySetUntil,com.team.lottery.extvo.LotterySetMsg"
    pageEncoding="utf-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String serverName=  request.getServerName();//获取当前域名
	String bizSystem=ClsCacheManager.getBizSystemByDomainUrl(serverName);
	BizSystemConfigVO bizSystemConfigVO=BizSystemConfigManager.getBizSystemConfig(bizSystem);//业务系统彩种设置
	BizSystemInfo bizSystemInfo=ClsCacheManager.getBizSystemInfo(bizSystem);//热门彩种设置，首页彩种设置
	LotterySet lotterySet=new LotterySet();
	LotterySetUntil lotterySetUntil=new LotterySetUntil();
	 //热门彩种
	LotterySetMsg lotterySetMsgHot=lotterySetUntil.compareLotterySetALL(lotterySet, bizSystemConfigVO, bizSystemInfo);
	//全部彩种的
	LotterySetMsg lotterySetMsgAll=lotterySetUntil.compareLotterySetAndBizSystemConfigVO(lotterySet, bizSystemConfigVO);
	request.setAttribute("lotterySetMsgHot", lotterySetMsgHot);
	request.setAttribute("lotterySetMsgAll", lotterySetMsgAll);
	if(bizSystemInfo!=null&&bizSystemInfo.getHotLotteryKinds()!=null){
	 request.setAttribute("kinds", bizSystemInfo.getHotLotteryKinds().split(","));
	}else{
		request.setAttribute("kinds", "");
	}
%>
<!DOCTYPE html>
<html>
<head>
     <meta charset="utf-8">
     <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
     <meta name="format-detection" content="telephone=no">
     <title>购彩大厅</title>
     <link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
     <link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
     <link href="<%=path%>/css/lottery_hall.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
</head>
<body>

    <!-- header -->
    <div class="header color-dark">
        <h2 class="header-title">购彩大厅</h2>
        <div class="header-right" onclick="changeStyle(this)"><img class="icon" src="<%=path%>/images/icon/icon-threetool.png"></div>
    </div>
    <!-- header over -->

    <div class="stance"></div>
    <div class="main-nav">
        <div class="child all-lottery on" onclick="changeMain(this,0)">
            <div class="child-icon"></div>
            <h2 class="child-title">全部彩种</h2>
        </div>
        <div class="child hight-lottery" onclick="changeMain(this,1)">
            <div class="child-icon"></div>
            <h2 class="child-title">高频彩</h2>
        </div>
        <div class="child low-lottery" onclick="changeMain(this,2)">
            <div class="child-icon"></div>
            <h2 class="child-title">低频彩</h2>
        </div>
    </div>
    <!-- 全部彩种main -->
    <div class="main main-block on">
        <c:if test="${lotterySetMsgAll.isCQSSCOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/cqssc/cqssc.html','需要登录才能进行投注，是否登录？')" class="cqsscInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_ssc.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">重庆时时彩</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isTJSSCOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/tjssc/tjssc.html','需要登录才能进行投注，是否登录？')" class="tjsscInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_ssc.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">天津时时彩</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isXJSSCOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/xjssc/xjssc.html','需要登录才能进行投注，是否登录？')" class="xjsscInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_ssc.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">新疆时时彩</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isJSKSOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jsks/jsks.html','需要登录才能进行投注，是否登录？')" class="jsksInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_kuai3.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">江苏快三</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isBJKSOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/bjks/bjks.html','需要登录才能进行投注，是否登录？')" class="bjksInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_kuai3.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">北京快三</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isHBKSOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/hbks/hbks.html','需要登录才能进行投注，是否登录？')" class="hbksInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_kuai3.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">湖北快三</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isJLKSOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jlks/jlks.html','需要登录才能进行投注，是否登录？')" class="jlksInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_kuai3.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">吉林快三</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
       <c:if test="${lotterySetMsgAll.isAHKSOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/ahks/ahks.html','需要登录才能进行投注，是否登录？')" class="ahksInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_kuai3.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">安徽快三</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isJYKSOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jyks/jyks.html','需要登录才能进行投注，是否登录？')" class="jyksInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_kuai3.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">幸运快三</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
      <c:if test="${lotterySetMsgAll.isGXKSOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/gxks/gxks.html','需要登录才能进行投注，是否登录？')" class="gxksInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_kuai3.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">广西快三</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isGSKSOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/gsks/gsks.html','需要登录才能进行投注，是否登录？')" class="gsksInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_kuai3.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">甘肃快三</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isGXKSOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/shks/shks.html','需要登录才能进行投注，是否登录？')" class="shksInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_kuai3.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">上海快三</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        
        <c:if test="${lotterySetMsgAll.isFJSYXWOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/fjsyxw/fjsyxw.html','需要登录才能进行投注，是否登录？')" class="fjsyxwInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_11xuan5.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">福建11选5</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isSDSYXWOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/sdsyxw/sdsyxw.html','需要登录才能进行投注，是否登录？')" class="sdsyxwInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_11xuan5.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">山东11选5</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isJXSYXWOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jxsyxw/jxsyxw.html','需要登录才能进行投注，是否登录？')" class="jxsyxwInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_11xuan5.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">江西11选5</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isGDSYXWOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/gdsyxw/gdsyxw.html','需要登录才能进行投注，是否登录？')" class="gdsyxwInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_11xuan5.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">广东11选5</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isWFSYXWOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/wfsyxw/wfsyxw.html','需要登录才能进行投注，是否登录？')" class="wfsyxwInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_11xuan5.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">五分11选5</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isSFSYXWOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/sfsyxw/sfsyxw.html','需要登录才能进行投注，是否登录？')" class="sfsyxwInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_11xuan5.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">三分11选5</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isBJPK10Open==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/bjpks/bjpk10.html','需要登录才能进行投注，是否登录？')" class="bjpk10Info">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_pk10.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">北京PK10</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isJSPK10Open==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jspks/jspk10.html','需要登录才能进行投注，是否登录？')" class="jspk10Info">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_jspk10.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">极速PK10</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isLHCOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/lhc/lhc.html','需要登录才能进行投注，是否登录？')" class="lhcInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_liuhecai.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">六合彩</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isXYLHCOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/xylhc/xylhc.html','需要登录才能进行投注，是否登录？')" class="xylhcInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_xyliuhecai.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">幸运六合彩</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isXYEBOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/xyeb/xy28.html','需要登录才能进行投注，是否登录？')" class="xyebInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_xy28.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">幸运28</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isTWWFCOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/twwfc/twwfc.html','需要登录才能进行投注，是否登录？')" class="twwfcInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_fenfencai.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">台湾5分彩</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isJLFFCOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/xyffc/xyffc.html','需要登录才能进行投注，是否登录？')" class="jlffcInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_fenfencai.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">幸运分分彩</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/fcsddpc/fcsddpc.html','需要登录才能进行投注，是否登录？')" class="fcsddpcInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_3d.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">3D</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        <c:if test="${lotterySetMsgAll.isSFSSCOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/sfssc/sfssc.html','需要登录才能进行投注，是否登录？')" class="sfsscInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_ssc.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">三分时时彩</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isWFSSCOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/wfssc/wfssc.html','需要登录才能进行投注，是否登录？')" class="wfsscInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_ssc.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">五分时时彩</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isSFKSOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/sfks/sfks.html','需要登录才能进行投注，是否登录？')" class="sfksInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_kuai3.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">三分快三</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isWFKSOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/wfks/wfks.html','需要登录才能进行投注，是否登录？')" class="wfksInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_kuai3.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">五分快三</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
    </div>
    <!-- main over -->


    <!-- 高频彩main -->
    <div class="main main-block">
        <c:if test="${lotterySetMsgAll.isCQSSCOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/cqssc/cqssc.html','需要登录才能进行投注，是否登录？')" class="cqsscInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_ssc.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">重庆时时彩</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isTJSSCOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/tjssc/tjssc.html','需要登录才能进行投注，是否登录？')" class="tjsscInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_ssc.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">天津时时彩</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isXJSSCOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/xjssc/xjssc.html','需要登录才能进行投注，是否登录？')" class="xjsscInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_ssc.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">新疆时时彩</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        
        <c:if test="${lotterySetMsgAll.isFJSYXWOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/fjsyxw/fjsyxw.html','需要登录才能进行投注，是否登录？')" class="fjsyxwInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_11xuan5.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">福建11选5</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isWFSYXWOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/wfsyxw/wfsyxw.html','需要登录才能进行投注，是否登录？')" class="wfsyxwInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_11xuan5.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">五分11选5</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isSFSYXWOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/sfsyxw/sfsyxw.html','需要登录才能进行投注，是否登录？')" class="sfsyxwInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_11xuan5.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">三分11选5</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isSDSYXWOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/sdsyxw/sdsyxw.html','需要登录才能进行投注，是否登录？')" class="sdsyxwInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_11xuan5.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">山东11选5</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isJXSYXWOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jxsyxw/jxsyxw.html','需要登录才能进行投注，是否登录？')" class="jxsyxwInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_11xuan5.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">江西11选5</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isGDSYXWOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/gdsyxw/gdsyxw.html','需要登录才能进行投注，是否登录？')" class="gdsyxwInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_11xuan5.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">广东11选5</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>    
        <c:if test="${lotterySetMsgAll.isJSKSOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jsks/jsks.html','需要登录才能进行投注，是否登录？')" class="jsksInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_kuai3.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">江苏快三</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isBJKSOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/bjks/bjks.html','需要登录才能进行投注，是否登录？')" class="bjksInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_kuai3.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">北京快三</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isHBKSOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/hbks/hbks.html','需要登录才能进行投注，是否登录？')" class="hbksInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_kuai3.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">湖北快三</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isJLKSOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jlks/jlks.html','需要登录才能进行投注，是否登录？')" class="jlksInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_kuai3.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">吉林快三</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isJYKSOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jyks/jyks.html','需要登录才能进行投注，是否登录？')" class="jyksInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_kuai3.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">幸运快三</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
       <c:if test="${lotterySetMsgAll.isAHKSOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/ahks/ahks.html','需要登录才能进行投注，是否登录？')" class="ahksInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_kuai3.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">安徽快三</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
       <c:if test="${lotterySetMsgAll.isGXKSOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/gxks/gxks.html','需要登录才能进行投注，是否登录？')" class="gxksInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_kuai3.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">广西快三</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isGSKSOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/gsks/gsks.html','需要登录才能进行投注，是否登录？')" class="gsksInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_kuai3.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">甘肃快三</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isGXKSOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/shks/shks.html','需要登录才能进行投注，是否登录？')" class="shksInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_kuai3.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">上海快三</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isBJPK10Open==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/bjpks/bjpk10.html','需要登录才能进行投注，是否登录？')" class="bjpk10Info">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_pk10.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">北京PK10</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isJSPK10Open==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jspks/jspk10.html','需要登录才能进行投注，是否登录？')" class="jspk10Info">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_jspk10.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">极速PK10</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isXYEBOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/xyeb/xy28.html','需要登录才能进行投注，是否登录？')" class="xyebInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_xy28.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">幸运28</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isTWWFCOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/twwfc/twwfc.html','需要登录才能进行投注，是否登录？')" class="twwfcInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_fenfencai.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">台湾5分彩</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isJLFFCOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/xyffc/xyffc.html','需要登录才能进行投注，是否登录？')" class="jlffcInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_fenfencai.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">幸运分分彩</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isXYLHCOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/xylhc/xylhc.html','需要登录才能进行投注，是否登录？')" class="xylhcInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_xyliuhecai.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">幸运六合彩</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isSFSSCOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/sfssc/sfssc.html','需要登录才能进行投注，是否登录？')" class="sfsscInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_ssc.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">三分时时彩</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isWFSSCOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/wfssc/wfssc.html','需要登录才能进行投注，是否登录？')" class="wfsscInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_ssc.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">五分时时彩</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isSFKSOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/sfks/sfks.html','需要登录才能进行投注，是否登录？')" class="sfksInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_kuai3.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">三分快三</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isWFKSOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/wfks/wfks.html','需要登录才能进行投注，是否登录？')" class="wfksInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_kuai3.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">五分快三</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
    </div>
    <!-- main over -->

    <!-- 低频彩main -->
    <div class="main main-block">
    	<c:if test="${lotterySetMsgAll.isLHCOpen==1}">
        <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/lhc/lhc.html','需要登录才能进行投注，是否登录？')" class="lhcInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_liuhecai.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">六合彩</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
        <c:if test="${lotterySetMsgAll.isFC3DDPCOpen==1}">
 		<a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/fcsddpc/fcsddpc.html','需要登录才能进行投注，是否登录？')" class="fcsddpcInfo">
            <div class="line">
                <div class="lottery-icon"><img src="<%=path%>/images/lottery_logo/hall/logo_3d.png" alt=""></div>
                <div class="line-content">
                    <h2 class="lottery-title">3D</h2>
                    <p class="lottery-mun font-red"><span>加载中...</span><span></span><span></span><span></span><span></span><span></span></p>
                    <div class="lottery-timeout"><span class="newExpectNum">距离第xxxx期截止还有</span> <span class="time">加载中...</span></div>
                    <p class="lottery-periods font-red">第xxx期</p>
                </div>
                <img src="<%=path%>/images/icon/icon-hallright.png" alt="" class="line-right">
            </div>
        </a>
        </c:if>
    </div>
    <!-- main over -->

   <!-- nav-bar -->
   <jsp:include page="/front/include/navbar.jsp"></jsp:include>
   <!-- nav-bar over -->
  <jsp:include page="/front/include/include.jsp"></jsp:include>
   <script src="<%=path%>/js/hall/hall.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
  <script type="text/javascript">
  	$(".i-shop").parent().addClass("on");
  </script>
</body>
</html>