<%@ page language="java" contentType="text/html; charset=UTF-8"
	import="com.team.lottery.system.BizSystemConfigManager,com.team.lottery.system.BizSystemConfigVO,net.sf.json.JSONObject,
    com.team.lottery.cache.ClsCacheManager,com.team.lottery.vo.BizSystem,
    com.team.lottery.vo.BizSystemDomain"
	pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant"%>
<%@ page import="java.math.BigDecimal"%>
<%
	String path = request.getContextPath();
	String serverName = request.getServerName();
	String bizSystemCode = ClsCacheManager.getBizSystemByDomainUrl(serverName);
	BizSystem bizSystem = ClsCacheManager.getBizSystemByCode(bizSystemCode);
	//返回的是域名，如果有跳转手机域名，返回是手机域名，不是直接返回本域名的信息
	if (bizSystem == null) {
		bizSystem = new BizSystem();
	}
	
	BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(bizSystem.getBizSystem());
	//获取下载App赠送的金额
	BigDecimal appDownGiftMoney=bizSystemConfigVO.getAppDownGiftMoney();
	//获取完善个人信息赠送的金额;
	BigDecimal completeInfoGiftMoney=bizSystemConfigVO.getCompleteInfoGiftMoney();
	//完成投注一笔赠送的金额;有效投注;
	BigDecimal lotteryGiftMoney=bizSystemConfigVO.getLotteryGiftMoney();
	//完成首笔冲值赠送的金额;
	BigDecimal rechargeGiftMoney=bizSystemConfigVO.getRechargeGiftMoney();
	//发展一位客户赠送的金额;
	BigDecimal extendUserGiftMoney=bizSystemConfigVO.getExtendUserGiftMoney();
	
	//用户领取的总的奖金
	BigDecimal totalNewUserGiftTaskMoney=BigDecimal.ZERO.add(appDownGiftMoney).add(completeInfoGiftMoney).add(lotteryGiftMoney).add(rechargeGiftMoney).add(extendUserGiftMoney);
	//登录界面路径
	String loginPath = path + "/gameBet/login.html";
	//注册界面路径
	String regPath = path + "/gameBet/reg.html";
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="width">
    <meta name="renderer" content="webkit" />
    <title>Wap Gift Page</title>
    <%-- <jsp:include page="/front/include/include.jsp"></jsp:include> --%>
    <link href="<%=path%>/css/new_user_gift.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <style>
    .tip{position:fixed;bottom:200px;left:0px;right:0px;text-align:center;display:none;justify-content: center;-webkit-justify-content: center;align-items: flex-end;-webkit-align-items: flex-end;z-index:200;}
	.tip .tip-content{position:relative;vertical-align:middle; color:#fff;font-size:32px;padding:24px 32px;background:#242844;z-index:201;border:1px solid #3a3f69;border-radius:18px;-webkit-border-radius:18px;}
    </style>
   	<%-- <link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet"> --%>
	<script src="<%=path%>/assets/jquery/jquery-3.1.1.min.js"></script>   
    <script type="text/javascript" src="<%=path%>/js/base.js"></script>
    <script type="text/javascript" src="<%=path%>/js/activity/new_user_gift.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
    <script type="text/javascript">
  	var contextPath = '<%=path %>';
  	var totalNewUserGiftTaskMoney='<%=totalNewUserGiftTaskMoney%>';
  	</script>
  	
</head>
<body>
    <div class="wrapper">
        <div class="header"><div class="title"></div></div>
        <div class="btn-group">
            <a href=<%=loginPath%> class="btn login-btn">登录</a>
            <a href=<%=regPath%> class="btn reg-btn">注册</a>
        </div>
        <div class="main">
            <div class="gift-list">
                <div class="item">
                    <div class="top top1"></div>
                    <div class="box">
                        <div class="title">下载APP</div>
                        <div class="content">送<%=appDownGiftMoney%>元</div>
                        <div class="bottom">
                            <a class="btn goToLogin" id="appDownTaskStatus" style="display:none">去完成</a>
                            <a href="javascript:void(0)" class="btn received getMoney" id="appDownTaskStatusTake" attr-type="appDownGiftMoney" style="display:none">点击领取</a>
                            <span class="giftTip" id="appDownTaskStatusTakeStatus"></span>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="top top2"></div>
                    <div class="box">
                        <div class="title">完善个人信息</div>
                        <div class="content">送<%=completeInfoGiftMoney%>元</div>
                        <div class="bottom">
                        	<a class="btn goToLogin" id="completeInfoTaskStatus" style="display:none">去完成</a>
                            <a href="javascript:void(0)" class="btn received getMoney" id="completeInfoTaskStatusTake" attr-type="completeInfoGiftMoney" style="display:none">点击领取</a> 
                            <span class="giftTip" id="completeInfoTaskStatusTakeStatus"></span>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="top top3"></div>
                    <div class="box">
                        <div class="title">有效投注一笔</div>
                        <div class="content">送<%=lotteryGiftMoney%>元</div>
                        <div class="bottom">
                            <a class="btn goToLogin" id="lotteryTaskStatus" style="display:none">去完成</a>
                            <a href="javascript:void(0)" class="btn received getMoney"
								id="lotteryTaskStatusTake" attr-type="lotteryGiftMoney" style="display:none">点击领取</a>
							<span class="giftTip" id="lotteryTaskStatusTakeStatus"></span>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="top top4"></div>
                    <div class="box">
                        <div class="title">完成首笔充值</div>
                        <div class="content">送<%=rechargeGiftMoney%>元</div>
                        <div class="bottom">
                            <a class="btn goToLogin" id="rechargeTaskStatus" style="display:none">去完成</a>
                            <a href="javascript:void(0)" class="btn received getMoney"
								id="rechargeTaskStatusTake" attr-type="rechargeGiftMoney" style="display:none">点击领取</a>
							<span class="giftTip" id="rechargeTaskStatusTakeStatus" style="display:none"></span>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="top top5"></div>
                    <div class="box">
                        <div class="title">发展1位有效用户</div>
                        <div class="content">送<%=extendUserGiftMoney%>元</div>
                        <div class="bottom">
                            <a class="btn goToLogin" id="extendUserTaskStatus" style="display:none">去完成</a>
                            <a href="javascript:void(0)" class="btn received getMoney"
								id="extendUserTaskStatusTake" attr-type="extendUserGiftMoney" style="display:none">点击领取</a>
							<span class="giftTip" id="extendUserTaskStatusTakeStatus"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div style="clear: both;"></div>
        </div>
        <div class="gift-rules">
            <div class="top">
                <div class="line"></div>
                <div class="title">说明</div>
            </div>
            <div class="content">
                <div class="text">1. 用户邀请的多个手机号经核实不存在、过期、停机、空号、无法接通等；</div>
                <div class="text">2. 用户通过发布注册任务奖励任务获得奖励；</div>
                <div class="text">3. 用户通过技术及其他恶意手段获得奖励；</div>
                <div class="text">4. 被邀请用户不知情或否认的情况；</div>
                <div class="tip">“以上用户行为均被视为恶意刷奖励，小油菜一经核实有权取消获得的相应奖励，小油菜对本次活动拥有最终解释权。”</div>
            </div>
        </div>
        <div class="footer">
           COPYRIGHT @ 2016 LOTTERY CARPARINTCN.AII RTGHIS RESCRVCD.
        </div>
    </div>
</body>

</html>