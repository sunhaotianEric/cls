<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
 	String id = request.getParameter("id");  //获取查询ID
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="format-detection" content="telephone=no">
<title>活动详情</title>

    <link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/detail.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
</head>
<body>

<!-- header -->
<div class="header color-dark">
    <div class="header-left" onclick="window.location.href='./activity.html'"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
    <h2 class="header-title">活动中心</h2>
</div>
<!-- header over -->


<div class="stance"></div>

<!-- main -->
<div class="main ">
    <div class="container">
        <h1 class="title" id="titleId">----</h1>
        <h2 class="time" id="timeId">发布时间:----</h2>
        <div class="content">
            <img src="" alt="" id="contentImgId">
        </div>
        <div class="content" id="contentId">
           <p>*****</p>
        </div>

    </div>
    
	<div class="activityList" id="activityApplyList">
	<!-- 	<div class="child">
				<div class="child-title color-red">每日活动</div>
				<div class="child-content">
						<p>活动内容xxxxxx活动内容xxxxxx <span class="font-yellow">xxxxx</span> <span class="font-blue">xxxxx</span></p>
				</div>
				<div class="child-btn color-red over">活动结束</div>
		</div>
		<div class="child">
				<div class="child-title color-red">每日活动</div>
				<div class="child-content">
						<p>活动内容xxxxxx活动内容xxxxxx活动内容xxxxxx活动内容xxxxxx <span class="font-yellow">xxxxx</span> <span class="font-blue">xxxxx</span></p>
				</div>
				<div class="child-btn color-red">进行中</div>
		</div>
		<div class="child">
				<div class="child-title color-red">每日活动</div>
				<div class="child-content">
						<p>活动内容xxxxxx活动内容xxxxxx活动内容xxxxxx活动内容xxxxxx <span class="font-yellow">xxxxx</span> <span class="font-blue">xxxxx</span></p>
				</div>
				<div class="child-btn color-red">进行中</div>
		</div>
		<div class="child">
				<div class="child-title color-red">每日活动</div>
				<div class="child-content">
						<p>活动内容xxxxxx活动内容xxxxxx活动内容xxxxxx活动内容xxxxxx <span class="font-yellow">xxxxx</span> <span class="font-blue">xxxxx</span></p>
				</div>
				<div class="child-btn color-red">进行中</div>
		</div>
		<div class="child">
				<div class="child-title color-red">每日活动</div>
				<div class="child-content">
						<p>活动内容xxxxxx活动内容xxxxxx活动内容xxxxxx活动内容xxxxxx <span class="font-yellow">xxxxx</span> <span class="font-blue">xxxxx</span></p>
				</div>
				<div class="child-btn color-red">进行中</div>
		</div> -->
	</div>
</div>
<!-- main over -->
<div class="stance-nav"></div>

<!-- nav-bar -->
<jsp:include page="/front/include/navbar.jsp"></jsp:include>
<!-- nav-bar over -->

<jsp:include page="/front/include/include.jsp"></jsp:include>

<script src="<%=path%>/js/activity/activity_detail.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
<script type="text/javascript">
  var id = <%=id %>;
</script>
</body>
</html>