<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="format-detection" content="telephone=no">
    <title>活动中心</title>

    <link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/activity.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">

</head>

<body>

    <!-- header -->
    <div class="header color-dark">
        <h2 class="header-title">活动中心</h2>
    </div>
    <!-- header over -->
    <div class="stance"></div>

    <!-- main -->
    <div class="main">
        <ul id="activityList">
<%--             <a href="activity_detail.html">
                <li class="main-child">
                    <div class="child-content">
                        <h2 class="title">下载手机APP</h2>
                        <p class="time">活动时间：2017-04-23</p>
                        <div class="thumb"><img src="<%=path%>/images/activity/1.jpg" alt=""></div>
                        <div class="info">
                            <p>下载手机客户端APP，精彩大奖等你拿..</p>
                        </div>
                    </div>
                    <div class="child-more"><span>查看详情</span><img src="<%=path%>/images/icon/icon-pointer.png" alt=""></div>
                </li>
            </a>

            <a href="activity_detail.html">
                <li class="main-child">
                    <div class="child-content">
                        <h2 class="title">下载手机APP</h2>
                        <p class="time">活动时间：2017-04-23</p>
                        <div class="thumb"><img src="<%=path%>/images/activity/2.jpg" alt=""></div>
                        <div class="info">
                            <p>下载手机客户端APP，精彩大奖等你拿..</p>
                        </div>
                    </div>
                    <div class="child-more"><span>查看详情</span><img src="<%=path%>/images/icon/icon-pointer.png" alt=""></div>
                </li>
            </a>

 --%>

        </ul>
    </div>
    <!-- main over -->


    <!-- nav-bar -->
    <jsp:include page="/front/include/navbar.jsp"></jsp:include>
    <!-- nav-bar over -->

    <jsp:include page="/front/include/include.jsp"></jsp:include>
    <script src="<%=path%>/js/activity/activity.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
</body>

</html>