<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="com.team.lottery.cache.ClsCacheManager,com.team.lottery.vo.BizSystemInfo" %>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
    String userName =  (String)request.getSession().getAttribute("retrieveUsername");
    if(userName == null || userName.equals("")){
	response.sendRedirect(path + "/gameBet/login/retrieve.html");
	return;
}
  //获取当前域名
  	String serverName=  request.getServerName();
  	String bizSystem=ClsCacheManager.getBizSystemByDomainUrl(serverName);
  	BizSystemInfo bizSystemInfo=ClsCacheManager.getBizSystemInfo(bizSystem);
  	//客服
  	String customUrl = "";
  	if(bizSystemInfo != null) {
  		customUrl = bizSystemInfo.getCustomUrl();
  	}
%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="format-detection" content="telephone=no">
    <title></title>

    <link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <jsp:include page="/front/include/include.jsp"></jsp:include>
   <%--  <link rel="stylesheet" type="text/css" href="<%=path%>/front/css/index-retrieve-mode.css"/> --%>
</head>

<body>

    <!-- header -->
    <div class="header color-dark">
        <div class="header-left" onclick="window.history.go(-1)"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
        <h2 class="header-title">选择找回方式</h2>
        <div class="header-right" onclick="window.location.href='<%=customUrl%>'"><img src="<%=path%>/images/icon/icon-custom.png" alt=""></div>
    </div>
    <!-- header over -->



    <div class="stance"></div>

    <ul class="user-list no">
        <!-- user-list 提示信息 -->
        <li class="user-msg">
            <div class="container" >

                <p>您正在找回密码的帐号为 <span class="font-red"><%=userName%></span>，<a class="font-blue" href="<%=path%>/gameBet/forgetPwd.html">换一个帐号</a></p>

            </div>
        </li>
        <!-- user-list over 提示信息 -->
    </ul>

    <ul class="user-list" >
			<!-- user-list 单选框 -->
			<li class="user-line">
				<div class="container">

					<div class="line-title">
						<p>找回方式</p>
					</div>
					<div class="line-content" id="find_way_list">
						<!-- <div class="radio">
							<input type="radio" name="type" checked="true">
							<div class="radioCheck"></div>
							<span>选择密保问题来找回密码</span>
						</div>
                         <div class="radio">
							<input type="radio" name="type" checked="true">
							<div class="radioCheck"></div>
							<span>选择密保问题来找回密码</span>
						</div> -->
					</div>


				</div>
			</li> 
			<!-- user-list over 单选框 -->

		</ul>


    <br>
    <div class="user-btn color-red" id="nextButton">下一步</div>
    <jsp:include page="/front/include/navbar.jsp"></jsp:include>
 <!-- 业务js -->
    <script type="text/javascript" src="<%=path%>/js/forgetpwd/choose_way.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
<script type="text/javascript">
	chooseWayPage.param.id = '<%=userName %>';
</script>
</body>

</html>