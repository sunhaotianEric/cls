<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page
	import="com.team.lottery.cache.ClsCacheManager,com.team.lottery.vo.BizSystemInfo"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant"%>
<%
	String path = request.getContextPath();
	//获取当前域名
	String serverName = request.getServerName();
	String bizSystem = ClsCacheManager
			.getBizSystemByDomainUrl(serverName);
	BizSystemInfo bizSystemInfo = ClsCacheManager
			.getBizSystemInfo(bizSystem);
	//客服
	String customUrl = "";
	if (bizSystemInfo != null) {
		customUrl = bizSystemInfo.getCustomUrl();
	}
%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<meta id="viewport" name="viewport"
	content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
<meta name="format-detection" content="telephone=no">
<title></title>

<link
	href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>"
	rel="stylesheet">
<link
	href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>"
	rel="stylesheet">
<jsp:include page="/front/include/include.jsp"></jsp:include>
</head>

<body>

	<!-- header -->
	<div class="header color-dark">
		<div class="header-left" onclick="window.history.go(-1)">
			<img class="icon" src="<%=path%>/images/icon/icon-return.png">
		</div>
		<h2 class="header-title">忘记密码</h2>
		<div class="header-right"
			onclick="window.location.href='<%=customUrl%>'">
			<img src="<%=path%>/images/icon/icon-custom.png" alt="">
		</div>
	</div>
	<!-- header over -->



	<div class="stance"></div>


	<ul class="user-list">
		<!-- user-list 输入框 -->
		<li class="user-line">
			<div class="container">
				<div class="line-title">
					<p>账号</p>
				</div>
				<div class="line-content">
					<input class="inputText" type="text" placeholder="请输入账号"
						id="username">
				</div>
			</div>
		</li>
		<!-- user-list over 输入框 -->

		<!-- user-list 输入框 -->
		<li class="user-line">
			<div class="container">
				<div class="line-title">
					<p>验证码</p>
				</div>
				<div class="line-content">
					<input class="inputText" type="text" placeholder="请输入验证码"
						id="verifyCode">
				</div>
				<img class="verifyImg" style="width: 220px; height: 100px;"
					id="checkCode" onclick="checkCodeRefresh()"
					src="<%=path%>/tools/getRandomCodePic"
					<%-- src="<%=path%>/images/enroll/verify.jpg" --%> alt="">
			</div>
		</li>
		<!-- user-list over 输入框 -->
	</ul>


	<br>
	<div class="user-btn color-red" id="nextButton">下一步</div>


	<jsp:include page="/front/include/navbar.jsp"></jsp:include>
	<!-- 业务js -->
	<script type="text/javascript" src="<%=path%>/js/forgetpwd/forget_pwd.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
	<script type="text/javascript">
		function checkCodeRefresh() {
			$('#checkCode').attr('src', contextPath + "/tools/getRandomCodePic?time=" + Math.random());
		}
	</script>
</body>

</html>