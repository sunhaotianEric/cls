<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="com.team.lottery.cache.ClsCacheManager,com.team.lottery.vo.BizSystemInfo" %>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
    String userName =  (String)request.getSession().getAttribute("retrieveUsername");
    if(userName == null || userName.equals("")){
	response.sendRedirect(path + "/gameBet/login/retrieve.html");
	return;
}
  //获取当前域名
  	String serverName=  request.getServerName();
  	String bizSystem=ClsCacheManager.getBizSystemByDomainUrl(serverName);
  	BizSystemInfo bizSystemInfo=ClsCacheManager.getBizSystemInfo(bizSystem);
  	//客服
  	String customUrl = "";
  	if(bizSystemInfo != null) {
  		customUrl = bizSystemInfo.getCustomUrl();
  	}
  	String type=request.getParameter("type");
  	request.setAttribute("type",type);
%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="format-detection" content="telephone=no">
    <title></title>

    <link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <jsp:include page="/front/include/include.jsp"></jsp:include>
</head>

<body>
    <!-- header -->
    <div class="header color-dark">
        <div class="header-left" onclick="window.history.go(-1)"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
        <h2 class="header-title">进行安全验证</h2>
        <div class="header-right" onclick="window.location.href='<%=customUrl%>'"><img src="<%=path%>/images/icon/icon-custom.png" alt=""></div>
    </div>
    <!-- header over -->



    <div class="stance"></div>

    <ul class="user-list no">
        <!-- user-list 提示信息 -->
        <li class="user-msg">
            <div class="container" >

                <p>您正在找回密码的帐号为 <span class="font-red"><%=userName%></span>，<a class="font-blue" href="<%=path%>/gameBet/forgetPwd.html">换一个帐号</a></p>

            </div>
        </li>
        <!-- user-list over 提示信息 -->
    </ul>

    <ul class="user-list">
    <%-- <div class="line">
              <img src="<%=path%>/front/images/login/i1.png" />
              <input type="text" class="inputText" value="" readonly="true"  id="question"/>
              <input type="hidden" id="qType"/>
              <span class="change" id="refresh_problem">换一个</span>
          </div> --%>
        <c:if test="${type=='question' }">
        <!-- user-list 输入框 -->
        <li class="user-line">
            <div class="container">
                <div class="line-title">
                    <p>问题一</p>
                </div>
                <div class="line-content"><input class="inputText" type="text" placeholder="" id="question"><input type="hidden" id="qType"/></div>
                <!-- <div class="right font-red" id="refresh_problem">换一个</div> -->
            </div>
        </li>
        <!-- user-list over 输入框 -->
        <!-- user-list 输入框 -->
        <li class="user-line">
            <div class="container">
                <div class="line-title">
                    <p>答案一</p>
                </div>
                <div class="line-content"><input class="inputText" type="text" id="answer" placeholder="请输入答案"></div>
            </div>
        </li>
        <!-- user-list over 输入框 -->
        
        
        
        
        <!-- user-list 输入框 -->
        <li class="user-line">
            <div class="container">
                <div class="line-title">
                    <p>问题二</p>
                </div>
                <div class="line-content"><input class="inputText" type="text" placeholder="" id="question1"><input type="hidden" id="qType1"/></div>
                <!-- <div class="right font-red" id="refresh_problem">换一个</div> -->
            </div>
        </li>
        <!-- user-list over 输入框 -->
        <!-- user-list 输入框 -->
        <li class="user-line">
            <div class="container">
                <div class="line-title">
                    <p>答案二</p>
                </div>
                <div class="line-content"><input class="inputText" type="text" id="answer1" placeholder="请输入答案"></div>
            </div>
        </li>
        <!-- user-list over 输入框 -->
        
        
        
        

        <!-- user-list 输入框 -->
        <li class="user-line">
            <div class="container">
                <div class="line-title">
                    <p>验证码</p>
                </div>
                <div class="line-content"><input class="inputText" type="text" placeholder="请输入验证码" id="verifyCode"></div>
                <img class="verifyImg" style="width: 220px;height: 100px;" src="<%=path %>/tools/getRandomCodePic" id="checkCode" onclick="checkCodeRefresh()" src="<%=path %>/imageServlet" alt="验证码">
            </div>
        </li>
        <!-- user-list over 输入框 -->
        </c:if>
        <c:if test="${type=='phone' }">
        <!-- 手机短信验证 -->
        <li class="user-line">
            <div class="container">
                <div class="line-title">
                    <p>手机号码</p>
                </div>
                <div class="line-content"><input class="inputText" type="text" placeholder="请输入手机号码" id="mobile"><input type="hidden" id="mType"/></div>
                <div class="right font-red" id="btn-sendnum">发送</div>
                <span class="mobile-verify-time right">重发(<em class="countdown">60</em> s)</span>
            </div>
        </li>
        <li class="user-line" id="mobile_verifyCode_field">
            <div class="container">
                <div class="line-title">
                    <p>验证码</p>
                </div>
                <div class="line-content"><input class="inputText" type="text" placeholder="请输入验证码" id="mobile_verifyCode"></div>
            </div>
        </li>
        <!-- 手机短信验证end -->
        </c:if>
        
        <c:if test="${type=='email' }">
        	<!-- 邮箱验证码验证 -->
        	<li class="user-line">
            <div class="container">
                <div class="line-title">
                    <p>邮箱号码</p>
                </div>
                <div class="line-content"><input class="inputText" type="text" placeholder="请输入绑定邮箱" id="email"><input type="hidden" id="mType"/></div>
                <div class="right font-red" id="btn-sendEmailNum">发送</div>
                <span class="email-verify-time right">重发(<em class="countdownEmail">60</em> s)</span>
            </div>
            
             <li class="user-line" id="email_verifyCode_field">
            <div class="container">
                <div class="line-title">
                    <p>验证码</p>
                </div>
                <div class="line-content"><input class="inputText" type="text" placeholder="请输入验证码" id="email_verifyCode"></div>
            </div>
        </li>
        </li>
        
        </c:if>
    </ul>


    <br>
    <div class="user-btn color-red" id="nextButton">下一步</div>

    <jsp:include page="/front/include/navbar.jsp"></jsp:include>
    <!-- 业务js -->
    <script type="text/javascript" src="<%=path%>/js/forgetpwd/verify_way.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
    <script type="text/javascript">
    verifyWayPage.param.id = '<%=userName %>';
    verifyWayPage.param.type = '<%=type %>';
    </script>
    <script type="text/javascript">
    
	function checkCodeRefresh() {
		$('#checkCode').attr('src', contextPath + "/tools/getRandomCodePic?time="+Math.random());
	}
</script>
</body>

</html>