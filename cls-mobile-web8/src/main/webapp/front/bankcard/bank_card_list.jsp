<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="format-detection" content="telephone=no">
    <title>银行卡管理</title>
    <link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
</head>

<body>

    <!-- header -->
    <div class="header color-dark">
        <div class="header-left" onclick="window.location.href='<%=path%>/gameBet/setting.html'"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
        <h2 class="header-title">银行卡管理</h2>
    </div>
    <!-- header over -->



    <div class="stance"></div>



    <ul class="user-list" id="userBankList">

        <!-- <li class="user-line select">
            <div class="container">
                <div class="line-title"><img src="images/bankcard_logo/jianshe.png" alt=""></div>
                <div class="line-content">
                    <h2 class="title">中国建设银行</h2>
                    <p class="mun">＊＊＊＊＊＊＊＊＊＊2017</p>
                </div>
            </div>
        </li>
        <li class="user-line select">
            <div class="container">
                <div class="line-title"><img src="images/bankcard_logo/jianshe.png" alt=""></div>
                <div class="line-content">
                    <h2 class="title">中国建设银行</h2>
                    <p class="mun">＊＊＊＊＊＊＊＊＊＊2017</p>
                </div>
            </div>
        </li>
        <li class="user-line select">
            <div class="container">
                <div class="line-title"><img src="images/bankcard_logo/jianshe.png" alt=""></div>
                <div class="line-content">
                    <h2 class="title">中国建设银行</h2>
                    <p class="mun">＊＊＊＊＊＊＊＊＊＊2017</p>
                </div>
            </div>
        </li> -->
    </ul>


    <ul class="user-list no">
        <!-- user-list 提示信息 -->
        <li class="user-msg">
            <!-- <p>您还可以绑定 <span class="font-red">4</span> 张银行卡</p> -->
            <p>您已经绑定 <span class="font-blue" id="userBankCountId">0</span> 张银行卡，可以再绑定<span class="font-blue" id="userCanBindCountId">5</span>张银行卡</p>
        </li>
        <!-- user-list over 提示信息 -->
    </ul>
    <div class="user-btn color-red" onclick="window.location.href='<%=path%>/gameBet/bankCard/addBankCard.html'">添加银行卡</div>

    <ul class="user-list no">
        <!-- user-list 提示信息 -->
        <li class="user-msg" style="text-align:left;">
            <div class="container">
             <!--    <p>如果您的银行卡已锁定，不能进行银行卡信息增加或删除</p> -->
                <p>如果您对添加绑定或删除银行卡有疑问，请联系在线客服！</p>
            </div>
        </li>

        <!-- user-list over 提示信息 -->
    </ul>


    <!-- nav-bar -->
    <jsp:include page="/front/include/navbar.jsp"></jsp:include>
    <!-- nav-bar over -->
    <jsp:include page="/front/include/include.jsp"></jsp:include>
    <script type="text/javascript" src="<%=path%>/js/bankcard/bank_card_list.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
    

</body>

</html>