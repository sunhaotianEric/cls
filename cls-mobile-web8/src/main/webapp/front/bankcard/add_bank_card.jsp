<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<meta id="viewport" name="viewport"
	content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
<meta name="format-detection" content="telephone=no">
<title>银行卡添加</title>
<link
	href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>"
	rel="stylesheet">
<link
	href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>"
	rel="stylesheet">
</head>

<body>
	<!-- header -->
	<div class="header color-dark">
		<div class="header-left"
			onclick="window.location.href='<%=path%>/gameBet/bankCard/bankCardList.html'">
			<img class="icon" src="<%=path%>/images/icon/icon-return.png">
		</div>
		<h2 class="header-title">银行卡添加</h2>
	</div>
	<!-- header over -->
	<div class="stance"></div>
	<ul class="user-list">
		<!-- user-list 输入框 -->
		<li class="user-line">
			<div class="container">
				<div class="line-title">
					<p>开户人:</p>
				</div>
				<div class="line-content">
					<input class="inputText" type="text" placeholder=""
						id="bankAccount">
				</div>
			</div>
		</li>
		<!-- user-list over 输入框 -->
		<!-- user-list 下拉框 -->
		<li class="user-line select bankname"
			onclick="modal_toggle('.bank-modal')">
			<div class="container select-container">
				<div class="line-title">开户银行:</div>
				<div class="line-content" id="bankBank">
					<input type="hidden" class="value" value="CCB">
					<h2 class="title" id="bankNameStr">中国建设银行</h2>
				</div>
				<div class="right">
					<img class="icon" src="<%=path%>/images/icon/icon-pointer.png" />
				</div>
			</div>
		</li>
		<!-- user-list over 下拉框 -->
		<!-- user-list 下拉选择框 -->
		<li class="user-line">
			<div class="container">
				<div class="line-title">开户城市:</div>
				<div class="line-content" id="city">
					<select name="Province" class="Province prov inputSelect" id="s_province"></select>
	                <select name="City" class="City city inputSelect"  id="s_city" disabled="disabled"></select>
	                <!-- <select name="Area" class="Area dist inputSelect" id="s_county"></select> -->
				</div>
			</div>
		</li>
		<!-- user-list over 下拉选择框 -->
		<!-- user-list 输入框 -->
		<!-- <li class="user-line">
			<div class="container">
				<div class="line-title">
					<p>支行名称:</p>
				</div>
				<div class="line-content">
					<input class="inputText" type="text" placeholder=""
						id="subbranchName">
				</div>
			</div>
		</li> -->
		<!-- user-list over 输入框 -->
		<!-- user-list 输入框 -->
		<li class="user-line">
			<div class="container">
				<div class="line-title">
					<p>银行卡号:</p>
				</div>
				<div class="line-content">
					<input class="inputText" type="number" placeholder="" id="bankNum">
				</div>
			</div>
		</li>
		<!-- user-list over 输入框 -->
		<!-- user-list 输入框 -->
		<li class="user-line">
			<div class="container">
				<div class="line-title">
					<p>确认卡号:</p>
				</div>
				<div class="line-content">
					<input class="inputText" type="number" placeholder=""
						id="confirmBankNum">
				</div>
			</div>
		</li>
		<!-- user-list over 输入框 -->
		<!-- user-list 输入框 -->
		<li class="user-line">
			<div class="container" id="safetyCodeId">
				<div class="line-title">
					<p>安全密码:</p>
				</div>
				<div class="line-content">
					<input class="inputText" type="password" placeholder=""
						id="savePassword">
				</div>
			</div>
		</li>
		<!-- user-list over 输入框 -->
	</ul>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<div class="user-btn color-red" id="subBankBtn">添加</div>
	<!-- modal bank-modal -->
	<div class="modal bank-modal animated out">
		<!-- header -->
		<div class="header color-dark rechargewithdraw_fund">
			<div class="header-left" onclick="modal_toggle('.bank-modal')">
				<img class="icon" src="<%=path%>/images/icon/icon-return.png">
			</div>
			<h2 class="header-title">选择开户银行</h2>
		</div>
		<!-- header over -->
		<div class="stance"></div>
		<!-- user-list 下拉框 -->
		<li class="user-line select noselect">
			<div class="select-list " id="banksId"></div>
		</li>
		<!-- user-list over 下拉框 -->
		<div class="stance-nav"></div>
	</div>
	<div class="alert-bg" onclick="element_toggle(false,['.alert'])" style="z-index: 998;"></div>
    <!-- alert -->
	<div class="alert alert-cart" id="lotteryOrderDialog" style="display: none;z-index: 999;">
		<div class="alert-content">您还未设置安全密码，是否前往设置？</div>
		<div class="alert-foot">
			<div class="btn color-gray cols-50" onclick="element_toggle(false,['.alert'])">否</div>
			<div class="btn color-red cols-50" id="set" >是</div>
		</div>
	</div>
	<!-- alert over -->
	<!-- nav-bar -->
	<jsp:include page="/front/include/navbar.jsp"></jsp:include>
	<!-- nav-bar over -->
	<jsp:include page="/front/include/include.jsp"></jsp:include>
	<script src="<%=path%>/assets/cityselect/jquery.cityselect.js"></script>
	<script type="text/javascript" src="<%=path%>/js/bankcard/add_bank_card.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
</body>

</html>
