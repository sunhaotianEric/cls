<%@ page language="java" contentType="text/html; charset=UTF-8" 
  import="com.team.lottery.system.BizSystemConfigVO,com.team.lottery.system.BizSystemConfigManager,
         com.team.lottery.vo.LotterySet,com.team.lottery.cache.ClsCacheManager,
         com.team.lottery.system.BizSystemConfigManager,com.team.lottery.vo.BizSystemInfo,
         com.team.lottery.util.LotterySetUntil,com.team.lottery.extvo.LotterySetMsg"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
    String serverName=  request.getServerName();//获取当前域名
    String bizSystem=ClsCacheManager.getBizSystemByDomainUrl(serverName);
   // BizSystemConfigVO bizSystemConfigVO=BizSystemConfigManager.getBizSystemConfig(bizSystem);//业务系统彩种设置
    BizSystemInfo bizSystemInfo=ClsCacheManager.getBizSystemInfo(bizSystem);//热门彩种设置，首页彩种设置
    String kind="";
    for(int i=0;i<bizSystemInfo.getHotLotteryKinds().split(",").length;i++){
    	kind+=bizSystemInfo.getHotLotteryKinds().split(",")[i]+",";
    }
    	
    
 %>
<!DOCTYPE html>
<!-- saved from url=(0046)http://www.a555655.com/apps/kaijiang/mokj.html -->
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" name="viewport">
	<title>开奖报码</title>
	<jsp:include page="/front/include/include.jsp"></jsp:include>
	<link rel="stylesheet" type="text/css" href="<%=path%>/css/lottery_number.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
	<script src="<%=path%>/front/lotterynumber/js/lottery_number.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
	<style id="style-1-cropbar-clipper">/* 
		.en-markup-crop-options {
		    top: 18px !important;
		    left: 50% !important;
		    margin-left: -100px !important;
		    width: 200px !important;
		    border: 2px rgba(255,255,255,.38) solid !important;
		    border-radius: 4px !important;
		}
		
		.en-markup-crop-options div div:first-of-type {
		    margin-left: 0px !important;
		}
	</style>
	<script>
		var xs='<%=kind%>';
	</script>
</head>
<body>
	<div class="main">
	</div>
	<%-- <div class="ppbaomad"><!-- bjpk10 -->
		<div class="ppbaomadz"><img src="<%=path%>/front/images/lottery/logo/icon-pk10.png"></div> 
		<div class="ppbaomady">
			<div class="ppbaomadyt">
				最新开奖<span id="bjpk10-expect">649757</span>期
				<span class="y bjpk10-countdown">距下期开奖还有:<span id="kjbjpk10-next-h" class="none">00时</span><span id="kjbjpk10-next-m">2</span>分<span id="kjbjpk10-next-s">43</span>秒</span>
				<span class="y none bjpk10-lottery-time" style="display: none;"></span>
			</div>
			<div class="ppbaomadyb"><span id="expectbjpk10"><span class="btn btn-red">10</span><span class="btn btn-red">4</span><span class="btn btn-red">9</span><span class="btn btn-red">1</span><span class="btn btn-red">6</span><span class="btn btn-red">2</span><span class="btn btn-red">8</span><span class="btn btn-red">7</span><span class="btn btn-red">5</span><span class="btn btn-red">3</span></span></div>
		</div>
	</div>
	<div class="ppbaomad"><!-- cqssc -->
		<div class="ppbaomadz"><img src="<%=path%>/front/images/lottery/logo/icon-shishicai.png"></div> 
		<div class="ppbaomady">
			<div class="ppbaomadyt">
				最新开奖<span id="cqssc-expect">20171109023</span>期
				<span class="y cqssc-countdown">距下期开奖还有:<span id="kjcqssc-next-h" class="none">00时</span><span id="kjcqssc-next-m">0</span>分<span id="kjcqssc-next-s">0</span>秒</span>
				<span class="y none cqssc-lottery-time" style="display: none;"></span>
			</div>
			<div class="ppbaomadyb"><span id="expectcqssc"><span class="btn btn-red">0</span><span class="btn btn-red">4</span><span class="btn btn-red">3</span><span class="btn btn-red">8</span><span class="btn btn-red">0</span></span></div>
		</div>
	</div>
	<div class="ppbaomad"><!-- hk6 -->
		<div class="ppbaomadz"><img src="<%=path%>/front/images/lottery/logo/icon-liuhecai.png"></div> 
		<div class="ppbaomady">
			<div class="ppbaomadyt">
				最新开奖<span id="hk6-expect">2017130</span>期
				<span class="y none hk6-countdown">距下期开奖还有:<span id="kjhk6-next-m">00</span>分<span id="kjhk6-next-s">00</span>秒</span>
				<span class="y hk6-lottery-time"></span>
			</div>
			<div class="ppbaomadyb"><span id="expecthk6"><span class="btn btn-red">19</span><span class="btn btn-blue">40</span><span class="btn btn-red">35</span><span class="btn btn-red">18</span><span class="btn btn-green">3</span><span class="btn btn-red">38</span><span class="btn btn-add">+</span><span class="btn btn-red">48</span></span></div>
		</div>
	</div>
	<div class="ppbaomad"><!-- jsk3 -->
		<div class="ppbaomadz"><img src="<%=path%>/front/images/lottery/logo/icon-kuai3.png"></div> 
		<div class="ppbaomady">
			<div class="ppbaomadyt">
				最新开奖<span id="jsk3-expect">20171109014</span>期
				<span class="y jsk3-countdown" style="display: block;">距下期开奖还有:<span id="kjjsk3-next-h" class="none">00时</span><span id="kjjsk3-next-m">0</span>分<span id="kjjsk3-next-s">43</span>秒</span>
				<span class="y none jsk3-lottery-time" style="display: none;">开奖中...</span>
			</div>
			<div class="ppbaomadyb"><span id="expectjsk3"><span class="k3_no1">1</span><span class="k3_no2">2</span><span class="k3_no5">5</span></span></div>
		</div>
	</div>
	<div class="ppbaomad"><!-- fc3d -->
		<div class="ppbaomadz"><img src="<%=path%>/front/images/lottery/logo/icon-3d.png"></div> 
		<div class="ppbaomady">
			<div class="ppbaomadyt">
				最新开奖<span id="fc3d-expect">2017305</span>期
				<span class="y none fc3d-countdown">距下期开奖还有:<span id="kjfc3d-next-m">00</span>分<span id="kjfc3d-next-s">00</span>秒</span>
				<span class="y fc3d-lottery-time"></span>
			</div>
			<div class="ppbaomadyb"><span id="expectfc3d"><span class="btn btn-red">4</span><span class="btn btn-red">2</span><span class="btn btn-red">0</span></span></div>
		</div>
	</div>
	<div class="ppbaomad"><!-- xy28 -->
		<div class="ppbaomadz"><img src="<%=path%>/front/images/lottery/logo/icon-xy28.png"></div> 
		<div class="ppbaomady">
			<div class="ppbaomadyt">
				最新开奖<span id="hk6-expect">2017130</span>期
				<span class="y none hk6-countdown">距下期开奖还有:<span id="kjhk6-next-m">00</span>分<span id="kjhk6-next-s">00</span>秒</span>
				<span class="y hk6-lottery-time"></span>
			</div>
			<div class="ppbaomadyb"><span id="expecthk6"><span class="btn btn-red">19</span><span class="btn btn-add">+</span><span class="btn btn-red">40</span><span class="btn btn-add">+</span><span class="btn btn-red">35</span><span class="btn btn-equal">=</span><span class="btn btn-blue">48</span></span></div>
		</div>
	</div>
	<div class="ppbaomad"><!-- syxw -->
		<div class="ppbaomadz"><img src="<%=path%>/front/images/lottery/logo/icon-11xuan5.png"></div> 
		<div class="ppbaomady">
			<div class="ppbaomadyt">
				最新开奖<span id="cqssc-expect">20171109023</span>期
				<span class="y cqssc-countdown">距下期开奖还有:<span id="kjcqssc-next-h" class="none">00时</span><span id="kjcqssc-next-m">0</span>分<span id="kjcqssc-next-s">0</span>秒</span>
				<span class="y none cqssc-lottery-time" style="display: none;"></span>
			</div>
			<div class="ppbaomadyb"><span id="expectcqssc"><span class="btn btn-red">0</span><span class="btn btn-red">4</span><span class="btn btn-red">3</span><span class="btn btn-red">8</span><span class="btn btn-red">0</span></span></div>
		</div>
	</div>
	<div class="ppbaomad"><!-- fenfencai -->
		<div class="ppbaomadz"><img src="<%=path%>/front/images/lottery/logo/icon-fenfencai.png"></div> 
		<div class="ppbaomady">
			<div class="ppbaomadyt">
				最新开奖<span id="cqssc-expect">20171109023</span>期
				<span class="y cqssc-countdown">距下期开奖还有:<span id="kjcqssc-next-h" class="none">00时</span><span id="kjcqssc-next-m">0</span>分<span id="kjcqssc-next-s">0</span>秒</span>
				<span class="y none cqssc-lottery-time" style="display: none;"></span>
			</div>
			<div class="ppbaomadyb"><span id="expectcqssc"><span class="btn btn-red">0</span><span class="btn btn-red">4</span><span class="btn btn-red">3</span><span class="btn btn-red">8</span><span class="btn btn-red">0</span></span></div>
		</div>
	</div> --%>
	<div id="MsgSound" class="none"></div>
</body></html>