function LotteryNumber(){};
var lotteryNumber = new LotteryNumber();

lotteryNumber.param = {
	currentExpect : null, //当前期号
	isCanActiveExpect : true,
	intervalKey : null  //倒计时周期key
};
$(document).ready(function(){
	addLotteryHome();
	for(var i=0;i<xs.split(",").length;i++){
		var issue=xs.split(",")[i];
		if(issue!=""){
			//获取指定彩种的期号和开奖时间
			lotteryNumber.getActiveExpect(issue,"");
			//获取指定彩种的最新开奖号码
			lotteryNumber.getLastLotteryCode(issue);
			
			window.setInterval(lotteryNumber.getLastLotteryCode,5000,issue);
		}
	}
});
var setIntervalLastLotteryCode = function(issue){
	lotteryNumber.getLastLotteryCode(issue);
}

var addLotteryHome=function(){
	$("#lotteryhome").empty();
	for(var i=0;i<xs.split(",").length;i++){
		var issue=xs.split(",")[i];
		if(issue!=""){
			var str = ""
				+ '<div class="ppbaomad info-'+issue+'">'
				+ '	<div class="ppbaomadz"><div class="icon icon-'+lotteryNumber.Difference(issue)+'"></div></div> '
				+ '	<div class="ppbaomady">'
				+ '		<div class="ppbaomadyt">'
				+ '			<span class="lotteryhome" id="'+issue+'">&nbsp;</span>&nbsp;&nbsp;'
				+ '			<span class="number">最新开奖<span id="'+issue+'Number">&nbsp;</span></span>'
				+ '			<span class="y time">距下期开奖还有:<span id="'+issue+'time"></span>'
				+ '		</div>'
				+ '		<div class="ppbaomadyb"><span id="'+issue+'muns"></span></div>'
				+ '	</div>'
				+ '</div>';
			$('.main').append(str);
		}
	}
	$(".child-head .icon.icon-1_5fencai").attr("style","background: no-repeat left top;background-size:95px 95px;");
	$(".child-head .icon.icon-wufencai").attr("style","background: no-repeat left top;background-size:95px 95px;");
	$(".child-head .icon.icon-erfencai").attr("style","background: no-repeat left top;background-size:95px 95px;");
}

/**
 * 设置热门彩种图标
 */
LotteryNumber.prototype.Difference=function(lotteryName){
	if(lotteryName.indexOf("SSC")>-1){
		return "shishicai";
	}else if(lotteryName.indexOf("SYXW")>-1){
		return "11xuan5";
	}else if(lotteryName.indexOf("SDDPC")>-1){
		return "3d";
	}else if(lotteryName.indexOf("LHC")>-1){
		return "liuhecai";
	}else if(lotteryName.indexOf("KS")>-1){
		return "kuai3";
	}else if(lotteryName.indexOf("PK10")>-1){
		return "pk10";
	}else if(lotteryName.indexOf("HGFFC")>-1){
		return "1_5fencai";
	}else if(lotteryName.indexOf("FFC")>-1){
		return "fenfencai";
	}else if(lotteryName.indexOf("WFC")>-1){
		return "wufencai";
	}else if(lotteryName.indexOf("XJPLFC")>-1){
		return "erfencai";
	}else if(lotteryName.indexOf("XYEB")>-1){
		return "xy28";
	}
	
}

/**
 * 获取系统所有彩种当前正在投注的期号
 */
LotteryNumber.prototype.getAllActiveExpect = function(){
	if(lotteryNumber.param.isCanActiveExpect){  //判断是否可以进行期号的请求
		frontLotteryAction.getAllLotteryKindExpectAndDiffTime(function(r){
			if (r[0] != null && r[0] == "ok") {
				lotteryNumber.param.isCanActiveExpect = false;
				var issueList = r[1];
				if(issueList != null){
					for(var i=0;i<issueList.length;i++){
						var issue = issueList[i]
						lotteryNumber.getCurrentLotterydiffTimeByLotteryKind(issue);
						lotteryNumber.setAllInterval(issue);
					}
				}else{
					$(".number").html("预售中...");
				}
				
				/*if(lotteryNumber.param.diffTime > 5000){ //如果剩余时间超过5秒的
					setTimeout("lotteryNumber.controlIsCanActive()", 2000);
				}else{
					lotteryNumber.param.isCanActiveExpect = true;
				}*/
			}else if(r[0] != null && r[0] == "error"){
				alert(r[1]);
			}else{
				showErrorDlg(jQuery(document.body), "获取最新开奖号码的请求失败.");
			}
	    });
	}
};

/**
 * 获取系统当前彩种正在投注的期号
 */
LotteryNumber.prototype.getActiveExpect = function(kindName,currentExpect){
	frontLotteryAction.getExpectAndDiffTime(kindName,currentExpect,function(r){
		if (r[0] != null && r[0] == "ok") {
			var issue = r[1];
			if(issue != null && typeof(issue) != "undefind"){
				if(issue.lotteryType == "JLFFC"){
					$("#JLFFC").html("幸运分分彩");
				}else if(issue.lotteryType == "JYKS"){
					$("#JYKS").html("幸运快三");
				}else if(issue.lotteryType == "LHC"){
					$("#"+issue.lotteryType).html("六合彩");
				}else{
					$("#"+issue.lotteryType).html(issue.lotteryName);
				}
				lotteryNumber.getCurrentLotterydiffTimeByLotteryKind(issue);
				lotteryNumber.setAllInterval(issue);
			}else{
				$("#"+issue.lotteryType).html("预售中...");
			}
			
			/*if(lotteryNumber.param.diffTime > 5000){ //如果剩余时间超过5秒的
					setTimeout("lotteryNumber.controlIsCanActive()", 2000);
				}else{
					lotteryNumber.param.isCanActiveExpect = true;
				}*/
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "获取最新开奖号码的请求失败.");
		}
	});
};

LotteryNumber.prototype.getCurrentLotterydiffTimeByLotteryKind = function(issue){
	var SurplusSecond = 0;
	var diffTimeStr = "";
	if(issue.lotteryType == "CQSSC"){
		lotteryNumber.param.cqsscDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if(SurplusSecond>=3600){
			window.clearInterval(lotteryNumber.param.cqsscIntervalKey); //终止周期性倒计时
		}
		lotteryNumber.calculateTime(SurplusSecond,"CQSSC");
		//lotteryNumber.showCurrentExpect(issue,"CQSSC",false);
		lotteryNumber.param.cqsscDiffTime--;
		if(lotteryNumber.param.cqsscDiffTime < 0){
			$("#"+issue.lotteryType+"time").html("开奖中...");
			//获取最新开奖号码
			lotteryNumber.getLastLotteryCode(issue.lotteryType);
			//是否已经获取最新号码期号 开奖完毕，可以重新显示时间
			var flag = lotteryNumber.isGetNewCode(issue);
			if(flag){
				window.clearInterval(lotteryNumber.param.cqsscIntervalKey); //终止周期性倒计时
				lotteryNumber.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
			}
			
			
		};
		issue.timeDifference = lotteryNumber.param.cqsscDiffTime;
	}else if(issue.lotteryType == "JXSSC"){
		lotteryNumber.param.jxsscDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if(SurplusSecond>=3600){
			window.clearInterval(lotteryNumber.param.jxsscIntervalKey); //终止周期性倒计时
		}
		lotteryNumber.calculateTime(SurplusSecond,"JXSSC");
		//lotteryNumber.showCurrentExpect(issue,"JXSSC",false);
		lotteryNumber.param.jxsscDiffTime--;
		if(lotteryNumber.param.jxsscDiffTime < 0){
			$("#"+issue.lotteryType+"time").html("开奖中...");
			//获取最新开奖号码
			lotteryNumber.getLastLotteryCode(issue.lotteryType);
			//是否已经获取最新号码期号 开奖完毕，可以重新显示时间
			var flag = lotteryNumber.isGetNewCode(issue);
			if(flag){
			    window.clearInterval(lotteryNumber.param.jxsscIntervalKey); //终止周期性倒计时
			    lotteryNumber.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
			 }
			
		};
		issue.timeDifference = lotteryNumber.param.jxsscDiffTime;
	}else if(issue.lotteryType == "TJSSC"){
		lotteryNumber.param.tjsscDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if(SurplusSecond>=3600){
			window.clearInterval(lotteryNumber.param.tjsscIntervalKey); //终止周期性倒计时
		}
		lotteryNumber.calculateTime(SurplusSecond,"TJSSC");
		//lotteryNumber.showCurrentExpect(issue,"TJSSC",false);
		lotteryNumber.param.tjsscDiffTime--;
		if(lotteryNumber.param.tjsscDiffTime < 0){
			$("#"+issue.lotteryType+"time").html("开奖中...");
			//获取最新开奖号码
			lotteryNumber.getLastLotteryCode(issue.lotteryType);
			//是否已经获取最新号码期号 开奖完毕，可以重新显示时间
			var flag = lotteryNumber.isGetNewCode(issue);
			if(flag){
			  window.clearInterval(lotteryNumber.param.tjsscIntervalKey); //终止周期性倒计时
			  lotteryNumber.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
			}
			
		};
		issue.timeDifference = lotteryNumber.param.tjsscDiffTime;
	}else if(issue.lotteryType == "XJSSC"){
		lotteryNumber.param.xjsscDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if(SurplusSecond>=3600){
			window.clearInterval(lotteryNumber.param.xjsscIntervalKey); //终止周期性倒计时
		}
		lotteryNumber.calculateTime(SurplusSecond,"XJSSC");
		//lotteryNumber.showCurrentExpect(issue,"XJSSC",false);
		lotteryNumber.param.xjsscDiffTime--;
		if(lotteryNumber.param.xjsscDiffTime < 0){
			$("#"+issue.lotteryType+"time").html("开奖中...");
			//获取最新开奖号码
			lotteryNumber.getLastLotteryCode(issue.lotteryType);
			//是否已经获取最新号码期号 开奖完毕，可以重新显示时间
			var flag = lotteryNumber.isGetNewCode(issue);
			if(flag){
			  window.clearInterval(lotteryNumber.param.xjsscIntervalKey); //终止周期性倒计时
			  lotteryNumber.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
			}
			
		};
		issue.timeDifference = lotteryNumber.param.xjsscDiffTime;
	}else if(issue.lotteryType == "HLJSSC"){
		lotteryNumber.param.hljsscDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if(SurplusSecond>=3600){
			window.clearInterval(lotteryNumber.param.hljsscIntervalKey); //终止周期性倒计时
		}
		lotteryNumber.calculateTime(SurplusSecond,"HLJSSC");
		//lotteryNumber.showCurrentExpect(issue,"HLJSSC",false);
		lotteryNumber.param.hljsscDiffTime--;
		if(lotteryNumber.param.hljsscDiffTime < 0){
			$("#"+issue.lotteryType+"time").html("开奖中...");
			//获取最新开奖号码
			lotteryNumber.getLastLotteryCode(issue.lotteryType);
			//是否已经获取最新号码期号 开奖完毕，可以重新显示时间
			var flag = lotteryNumber.isGetNewCode(issue);
			if(flag){
			  window.clearInterval(lotteryNumber.param.hljsscIntervalKey); //终止周期性倒计时
			  lotteryNumber.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
			}
			
		};
		issue.timeDifference = lotteryNumber.param.hljsscDiffTime;
	}else if(issue.lotteryType == "JLFFC"){
		lotteryNumber.param.jlffcDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if(SurplusSecond>=3600){
			window.clearInterval(lotteryNumber.param.jlffcIntervalKey); //终止周期性倒计时
		}
		lotteryNumber.calculateTime(SurplusSecond,"JLFFC");
		//lotteryNumber.showCurrentExpect(issue,"JLFFC",false);
		lotteryNumber.param.jlffcDiffTime--;
		if(lotteryNumber.param.jlffcDiffTime < 0){
			$("#"+issue.lotteryType+"time").html("开奖中...");
			//获取最新开奖号码
			lotteryNumber.getLastLotteryCode(issue.lotteryType);
			//是否已经获取最新号码期号 开奖完毕，可以重新显示时间
			var flag = lotteryNumber.isGetNewCode(issue);
			if(flag){
			  window.clearInterval(lotteryNumber.param.jlffcIntervalKey); //终止周期性倒计时
			  lotteryNumber.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
			}
			
		};
		issue.timeDifference = lotteryNumber.param.jlffcDiffTime;
	}else if(issue.lotteryType == "HGFFC"){
		lotteryNumber.param.hgffcDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if(SurplusSecond>=3600){
			window.clearInterval(lotteryNumber.param.hgffcIntervalKey); //终止周期性倒计时
		}
		lotteryNumber.calculateTime(SurplusSecond,"HGFFC");
		lotteryNumber.showCurrentExpect(issue,"HGFFC",false);
		lotteryNumber.param.hgffcDiffTime--;
		if(lotteryNumber.param.hgffcDiffTime < 0){
			$("#"+issue.lotteryType+"time").html("开奖中...");
			//获取最新开奖号码
			lotteryNumber.getLastLotteryCode(issue.lotteryType);
			//是否已经获取最新号码期号 开奖完毕，可以重新显示时间
			var flag = lotteryNumber.isGetNewCode(issue);
			if(flag){
			  window.clearInterval(lotteryNumber.param.hgffcIntervalKey); //终止周期性倒计时
			  lotteryNumber.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
			}
			
		};
		issue.timeDifference = lotteryNumber.param.hgffcDiffTime;
	}else if(issue.lotteryType == "GDSYXW"){
		lotteryNumber.param.gdsyxwDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if(SurplusSecond>=3600){
			window.clearInterval(lotteryNumber.param.gdsyxwIntervalKey); //终止周期性倒计时
		}
		lotteryNumber.calculateTime(SurplusSecond,"GDSYXW");
		//lotteryNumber.showCurrentExpect(issue,"GDSYXW",false);
		lotteryNumber.param.gdsyxwDiffTime--;
		if(lotteryNumber.param.gdsyxwDiffTime < 0){
			$("#"+issue.lotteryType+"time").html("开奖中...");
			//获取最新开奖号码
			lotteryNumber.getLastLotteryCode(issue.lotteryType);
			//是否已经获取最新号码期号 开奖完毕，可以重新显示时间
			var flag = lotteryNumber.isGetNewCode(issue);
			if(flag){
			   window.clearInterval(lotteryNumber.param.gdsyxwIntervalKey); //终止周期性倒计时
			   lotteryNumber.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
			}
			
		};
		issue.timeDifference = lotteryNumber.param.gdsyxwDiffTime;
	}else if(issue.lotteryType == "SDSYXW"){
		lotteryNumber.param.sdsyxwDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if(SurplusSecond>=3600){
			window.clearInterval(lotteryNumber.param.sdsyxwIntervalKey); //终止周期性倒计时
		}
		lotteryNumber.calculateTime(SurplusSecond,"SDSYXW");
		//lotteryNumber.showCurrentExpect(issue,"SDSYXW",false);
		lotteryNumber.param.sdsyxwDiffTime--;
		if(lotteryNumber.param.sdsyxwDiffTime < 0){
			$("#"+issue.lotteryType+"time").html("开奖中...");
			//获取最新开奖号码
			lotteryNumber.getLastLotteryCode(issue.lotteryType);
			//是否已经获取最新号码期号 开奖完毕，可以重新显示时间
			var flag = lotteryNumber.isGetNewCode(issue);
			if(flag){
			  window.clearInterval(lotteryNumber.param.sdsyxwIntervalKey); //终止周期性倒计时
			  lotteryNumber.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
			}
			
		};
		issue.timeDifference = lotteryNumber.param.sdsyxwDiffTime;
	}else if(issue.lotteryType == "JXSYXW"){
		lotteryNumber.param.jxsyxwDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if(SurplusSecond>=3600){
			window.clearInterval(lotteryNumber.param.jxsyxwIntervalKey); //终止周期性倒计时
		}
		lotteryNumber.calculateTime(SurplusSecond,"JXSYXW");
		//lotteryNumber.showCurrentExpect(issue,"JXSYXW",false);
		lotteryNumber.param.jxsyxwDiffTime--;
		if(lotteryNumber.param.jxsyxwDiffTime < 0){
			$("#"+issue.lotteryType+"time").html("开奖中...");
			//获取最新开奖号码
			lotteryNumber.getLastLotteryCode(issue.lotteryType);
			//是否已经获取最新号码期号 开奖完毕，可以重新显示时间
			var flag = lotteryNumber.isGetNewCode(issue);
			if(flag){
			  window.clearInterval(lotteryNumber.param.jxsyxwIntervalKey); //终止周期性倒计时
			  lotteryNumber.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
			}
			
		};
		issue.timeDifference = lotteryNumber.param.jxsyxwDiffTime;
	}else if(issue.lotteryType == "FJSYXW"){
		lotteryNumber.param.fjsyxwDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if(SurplusSecond>=3600){
			window.clearInterval(lotteryNumber.param.fjsyxwIntervalKey); //终止周期性倒计时
		}
		lotteryNumber.calculateTime(SurplusSecond,"FJSYXW");
		//lotteryNumber.showCurrentExpect(issue,"FJSYXW",false);
		lotteryNumber.param.fjsyxwDiffTime--;
		if(lotteryNumber.param.fjsyxwDiffTime < 0){
			$("#"+issue.lotteryType+"time").html("开奖中...");
			//获取最新开奖号码
			lotteryNumber.getLastLotteryCode(issue.lotteryType);
			//是否已经获取最新号码期号 开奖完毕，可以重新显示时间
			var flag = lotteryNumber.isGetNewCode(issue);
			if(flag){
			  window.clearInterval(lotteryNumber.param.fjsyxwIntervalKey); //终止周期性倒计时
			  lotteryNumber.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
			}
			
		};
		issue.timeDifference = lotteryNumber.param.fjsyxwDiffTime;
	}else if(issue.lotteryType == "CQSYXW"){
		lotteryNumber.param.fjsyxwDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if(SurplusSecond>=3600){
			window.clearInterval(lotteryNumber.param.cqsyxwIntervalKey); //终止周期性倒计时
		}
		lotteryNumber.calculateTime(SurplusSecond,"CQSYXW");
		//lotteryNumber.showCurrentExpect(issue,"CQSYXW",false);
		lotteryNumber.param.cqsyxwDiffTime--;
		if(lotteryNumber.param.cqsyxwDiffTime < 0){
			$("#"+issue.lotteryType+"time").html("开奖中...");
			//获取最新开奖号码
			lotteryNumber.getLastLotteryCode(issue.lotteryType);
			//是否已经获取最新号码期号 开奖完毕，可以重新显示时间
			var flag = lotteryNumber.isGetNewCode(issue);
			if(flag){
			  window.clearInterval(lotteryNumber.param.cqsyxwIntervalKey); //终止周期性倒计时
			  lotteryNumber.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
			}
			
		};
		issue.timeDifference = lotteryNumber.param.cqsyxwDiffTime;
	}else if(issue.lotteryType == "JSKS"){
		lotteryNumber.param.jsksDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if(SurplusSecond>=3600){
			window.clearInterval(lotteryNumber.param.jsksIntervalKey); //终止周期性倒计时
		}
		lotteryNumber.calculateTime(SurplusSecond,"JSKS");
		//lotteryNumber.showCurrentExpect(issue,"JSKS",false);
		lotteryNumber.param.jsksDiffTime--;
		if(lotteryNumber.param.jsksDiffTime < 0){
			$("#"+issue.lotteryType+"time").html("开奖中...");
			//获取最新开奖号码
			lotteryNumber.getLastLotteryCode(issue.lotteryType);
			//是否已经获取最新号码期号 开奖完毕，可以重新显示时间
			var flag = lotteryNumber.isGetNewCode(issue);
			if(flag){
			  window.clearInterval(lotteryNumber.param.jsksIntervalKey); //终止周期性倒计时
			  lotteryNumber.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
			}
			
		};
		issue.timeDifference = lotteryNumber.param.jsksDiffTime;
	}else if(issue.lotteryType == "SHKS"){
		lotteryNumber.param.shksDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if(SurplusSecond>=3600){
			window.clearInterval(lotteryNumber.param.shksIntervalKey); //终止周期性倒计时
		}
		lotteryNumber.calculateTime(SurplusSecond,"SHKS");
		//lotteryNumber.showCurrentExpect(issue,"SHKS",false);
		lotteryNumber.param.shksDiffTime--;
		if(lotteryNumber.param.shksDiffTime < 0){
			$("#"+issue.lotteryType+"time").html("开奖中...");
			//获取最新开奖号码
			lotteryNumber.getLastLotteryCode(issue.lotteryType);
			//是否已经获取最新号码期号 开奖完毕，可以重新显示时间
			var flag = lotteryNumber.isGetNewCode(issue);
			if(flag){
			  window.clearInterval(lotteryNumber.param.shksIntervalKey); //终止周期性倒计时
			  lotteryNumber.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
			}
			
		};
		issue.timeDifference = lotteryNumber.param.shksDiffTime;
	}else if(issue.lotteryType == "GSKS"){
		lotteryNumber.param.gsksDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if(SurplusSecond>=3600){
			window.clearInterval(lotteryNumber.param.gsksIntervalKey); //终止周期性倒计时
		}
		lotteryNumber.calculateTime(SurplusSecond,"GSKS");
		//lotteryNumber.showCurrentExpect(issue,"GSKS",false);
		lotteryNumber.param.gsksDiffTime--;
		if(lotteryNumber.param.gsksDiffTime < 0){
			$("#"+issue.lotteryType+"time").html("开奖中...");
			//获取最新开奖号码
			lotteryNumber.getLastLotteryCode(issue.lotteryType);
			//是否已经获取最新号码期号 开奖完毕，可以重新显示时间
			var flag = lotteryNumber.isGetNewCode(issue);
			if(flag){
			  window.clearInterval(lotteryNumber.param.gsksIntervalKey); //终止周期性倒计时
			  lotteryNumber.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
			}
			
		};
		issue.timeDifference = lotteryNumber.param.gsksDiffTime;
	}else if(issue.lotteryType == "FJKS"){
		lotteryNumber.param.fjksDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if(SurplusSecond>=3600){
			window.clearInterval(lotteryNumber.param.fjksIntervalKey); //终止周期性倒计时
		}
		lotteryNumber.calculateTime(SurplusSecond,"FJKS");
		//lotteryNumber.showCurrentExpect(issue,"FJKS",false);
		lotteryNumber.param.fjksDiffTime--;
		if(lotteryNumber.param.fjksDiffTime < 0){
			$("#"+issue.lotteryType+"time").html("开奖中...");
			//获取最新开奖号码
			lotteryNumber.getLastLotteryCode(issue.lotteryType);
			//是否已经获取最新号码期号 开奖完毕，可以重新显示时间
			var flag = lotteryNumber.isGetNewCode(issue);
			if(flag){
			  window.clearInterval(lotteryNumber.param.fjksIntervalKey); //终止周期性倒计时
			  lotteryNumber.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
			}
			
		};
		issue.timeDifference = lotteryNumber.param.fjksDiffTime;
	}else if(issue.lotteryType == "AHKS"){
		lotteryNumber.param.ahksDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if(SurplusSecond>=3600){
			window.clearInterval(lotteryNumber.param.ahksIntervalKey); //终止周期性倒计时
		}
		lotteryNumber.calculateTime(SurplusSecond,"AHKS");
		//lotteryNumber.showCurrentExpect(issue,"AHKS",false);
		lotteryNumber.param.ahksDiffTime--;
		if(lotteryNumber.param.ahksDiffTime < 0){
			$("#"+issue.lotteryType+"time").html("开奖中...");
			//获取最新开奖号码
			lotteryNumber.getLastLotteryCode(issue.lotteryType);
			//是否已经获取最新号码期号 开奖完毕，可以重新显示时间
			var flag = lotteryNumber.isGetNewCode(issue);
			if(flag){
			  window.clearInterval(lotteryNumber.param.ahksIntervalKey); //终止周期性倒计时
			  lotteryNumber.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
			}
			
		};
		issue.timeDifference = lotteryNumber.param.ahksDiffTime;
	}else if(issue.lotteryType == "HBKS"){
		lotteryNumber.param.hbksDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if(SurplusSecond>=3600){
			window.clearInterval(lotteryNumber.param.hbksIntervalKey); //终止周期性倒计时
		}
		lotteryNumber.calculateTime(SurplusSecond,"HBKS");
		//lotteryNumber.showCurrentExpect(issue,"HBKS",false);
		lotteryNumber.param.hbksDiffTime--;
		if(lotteryNumber.param.hbksDiffTime < 0){
			$("#"+issue.lotteryType+"time").html("开奖中...");
			//获取最新开奖号码
			lotteryNumber.getLastLotteryCode(issue.lotteryType);
			//是否已经获取最新号码期号 开奖完毕，可以重新显示时间
			var flag = lotteryNumber.isGetNewCode(issue);
			if(flag){
			  window.clearInterval(lotteryNumber.param.hbksIntervalKey); //终止周期性倒计时
			  lotteryNumber.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
			}
			
		};
		issue.timeDifference = lotteryNumber.param.hbksDiffTime;
	}else if(issue.lotteryType == "JLKS"){
		lotteryNumber.param.jlksDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if(SurplusSecond>=3600){
			window.clearInterval(lotteryNumber.param.jlksIntervalKey); //终止周期性倒计时
		}
		lotteryNumber.calculateTime(SurplusSecond,"JLKS");
		//lotteryNumber.showCurrentExpect(issue,"JLKS",false);
		lotteryNumber.param.jlksDiffTime--;
		if(lotteryNumber.param.jlksDiffTime < 0){
			$("#"+issue.lotteryType+"time").html("开奖中...");
			//获取最新开奖号码
			lotteryNumber.getLastLotteryCode(issue.lotteryType);
			//是否已经获取最新号码期号 开奖完毕，可以重新显示时间
			var flag = lotteryNumber.isGetNewCode(issue);
			if(flag){
			  window.clearInterval(lotteryNumber.param.jlksIntervalKey); //终止周期性倒计时
			  lotteryNumber.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
			}
			
		};
		issue.timeDifference = lotteryNumber.param.jlksDiffTime;
	}else if(issue.lotteryType == "BJKS"){
		lotteryNumber.param.bjksDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if(SurplusSecond>=3600){
			window.clearInterval(lotteryNumber.param.bjksIntervalKey); //终止周期性倒计时
		}
		lotteryNumber.calculateTime(SurplusSecond,"BJKS");
		//lotteryNumber.showCurrentExpect(issue,"BJKS",false);
		lotteryNumber.param.bjksDiffTime--;
		if(lotteryNumber.param.bjksDiffTime < 0){
			$("#"+issue.lotteryType+"time").html("开奖中...");
			//获取最新开奖号码
			lotteryNumber.getLastLotteryCode(issue.lotteryType);
			//是否已经获取最新号码期号 开奖完毕，可以重新显示时间
			var flag = lotteryNumber.isGetNewCode(issue);
			if(flag){
			  window.clearInterval(lotteryNumber.param.bjksIntervalKey); //终止周期性倒计时
			  lotteryNumber.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
			}
			
		};
		issue.timeDifference = lotteryNumber.param.bjksDiffTime;
	}else if(issue.lotteryType == "JYKS"){
		lotteryNumber.param.jyksDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if(SurplusSecond>=3600){
			window.clearInterval(lotteryNumber.param.jyksIntervalKey); //终止周期性倒计时
		}
		lotteryNumber.calculateTime(SurplusSecond,"JYKS");
		//lotteryNumber.showCurrentExpect(issue,"JYKS",false);
		lotteryNumber.param.jyksDiffTime--;
		if(lotteryNumber.param.jyksDiffTime < 0){
			$("#"+issue.lotteryType+"time").html("开奖中...");
			//获取最新开奖号码
			lotteryNumber.getLastLotteryCode(issue.lotteryType);
			//是否已经获取重新开奖完了
			var flag = lotteryNumber.isGetNewCode(issue);
			if(flag){
				window.clearInterval(lotteryNumber.param.jyksIntervalKey);  //终止周期性倒计时
				lotteryNumber.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
			}
			
		};
		issue.timeDifference = lotteryNumber.param.jyksDiffTime;
	}else if(issue.lotteryType == "GXKS"){
		lotteryNumber.param.gxksDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if(SurplusSecond>=3600){
			window.clearInterval(lotteryNumber.param.gxksIntervalKey); //终止周期性倒计时
		}
		lotteryNumber.calculateTime(SurplusSecond,"GXKS");
		//lotteryNumber.showCurrentExpect(issue,"GXKS",false);
		lotteryNumber.param.gxksDiffTime--;
		if(lotteryNumber.param.gxksDiffTime < 0){
			$("#"+issue.lotteryType+"time").html("开奖中...");
			//获取最新开奖号码
			lotteryNumber.getLastLotteryCode(issue.lotteryType);
			//是否已经获取最新号码期号 开奖完毕，可以重新显示时间
			var flag = lotteryNumber.isGetNewCode(issue);
			if(flag){
			  window.clearInterval(lotteryNumber.param.gxksIntervalKey); //终止周期性倒计时
			  lotteryNumber.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
			}
			
		};
		issue.timeDifference = lotteryNumber.param.gxksDiffTime;
	}else if(issue.lotteryType == "FCSDDPC"){
		lotteryNumber.param.fcsddpcDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		lotteryNumber.calculateTime(SurplusSecond,"FCSDDPC");
		//lotteryNumber.showCurrentExpect(issue,"FCSDDPC",false);
		lotteryNumber.param.fcsddpcDiffTime--;
		if(lotteryNumber.param.fcsddpcDiffTime < 0){
			$("#"+issue.lotteryType+"time").html("开奖中...");
			//获取最新开奖号码
			lotteryNumber.getLastLotteryCode(issue.lotteryType);
			//是否已经获取最新号码期号 开奖完毕，可以重新显示时间
			var flag = lotteryNumber.isGetNewCode(issue);
			if(flag){
			  window.clearInterval(lotteryNumber.param.fcsddpcIntervalKey); //终止周期性倒计时
			  lotteryNumber.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
			}
			
		};
		issue.timeDifference = lotteryNumber.param.fcsddpcDiffTime;
	}else if(issue.lotteryType == "BJPK10"){
		lotteryNumber.param.pk10DiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if(SurplusSecond>=3600){
			window.clearInterval(lotteryNumber.param.pk10IntervalKey); //终止周期性倒计时
		}
		lotteryNumber.calculateTime(SurplusSecond,"BJPK10");
		//lotteryNumber.showCurrentExpect(issue,"BJPK10",false);
		lotteryNumber.param.pk10DiffTime--;
		if(lotteryNumber.param.pk10DiffTime < 0){
			$("#"+issue.lotteryType+"time").html("开奖中...");
			//获取最新开奖号码
			lotteryNumber.getLastLotteryCode(issue.lotteryType);
			//是否已经获取最新号码期号 开奖完毕，可以重新显示时间
			var flag = lotteryNumber.isGetNewCode(issue);
			if(flag){
			  window.clearInterval(lotteryNumber.param.pk10IntervalKey); //终止周期性倒计时
			  lotteryNumber.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
			}
			
		};
		issue.timeDifference = lotteryNumber.param.pk10DiffTime;
	}else if(issue.lotteryType == "XJPLFC"){
		lotteryNumber.param.xjplfcDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if(SurplusSecond>=3600){
			window.clearInterval(lotteryNumber.param.xjplfcIntervalKey); //终止周期性倒计时
		}
		lotteryNumber.calculateTime(SurplusSecond,"XJPLFC");
		//lotteryNumber.showCurrentExpect(issue,"XJPLFC",false);
		lotteryNumber.param.xjplfcDiffTime--;
		if(lotteryNumber.param.xjplfcDiffTime < 0){
			$("#"+issue.lotteryType+"time").html("开奖中...");
			//获取最新开奖号码
			lotteryNumber.getLastLotteryCode(issue.lotteryType);
			//是否已经获取最新号码期号 开奖完毕，可以重新显示时间
			var flag = lotteryNumber.isGetNewCode(issue);
			if(flag){
			  window.clearInterval(lotteryNumber.param.xjplfcIntervalKey); //终止周期性倒计时
			  lotteryNumber.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
			}
			
		};
		issue.timeDifference = lotteryNumber.param.xjplfcDiffTime;
	}else if(issue.lotteryType == "TWWFC"){
		lotteryNumber.param.twwfcDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if(SurplusSecond>=3600){
			window.clearInterval(lotteryNumber.param.twwfcIntervalKey); //终止周期性倒计时
		}
		lotteryNumber.calculateTime(SurplusSecond,"TWWFC");
		//lotteryNumber.showCurrentExpect(issue,"TWWFC",false);
		lotteryNumber.param.twwfcDiffTime--;
		if(lotteryNumber.param.twwfcDiffTime < 0){
			$("#"+issue.lotteryType+"time").html("开奖中...");
			//获取最新开奖号码
			lotteryNumber.getLastLotteryCode(issue.lotteryType);
			//是否已经获取最新号码期号 开奖完毕，可以重新显示时间
			var flag = lotteryNumber.isGetNewCode(issue);
			if(flag){
			  window.clearInterval(lotteryNumber.param.twwfcIntervalKey); //终止周期性倒计时
			  lotteryNumber.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
			}
			
		};
		issue.timeDifference = lotteryNumber.param.twwfcDiffTime;
	}else if(issue.lotteryType == "LHC"){
		var interval = 0;
		if(issue.isClose){
			//显示预售中
			$(".info-LHC .time").html("封盘中...");
			window.clearInterval(lotteryNumber.param.lhcIntervalKey); //终止周期性倒计时
			window.clearInterval(interval);
			lotteryNumber.getLastLotteryCode(issue.lotteryType);
		
			//1分钟重新读取服务器
		//	interval = setInterval(function(){lotteryNumber.getActiveExpect(issue.lotteryType,issue.lotteryNum)},10000);
			return;
		}
		//终止封盘时候的周期性倒计时
		window.clearInterval(interval);
		lotteryNumber.param.lhcDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		lotteryNumber.calculateTime(SurplusSecond,"LHC");
		//lotteryNumber.showCurrentExpect(issue,"LHC",false);
		lotteryNumber.param.lhcDiffTime--;
		if(lotteryNumber.param.lhcDiffTime < 0){
			$("#"+issue.lotteryType+"time").html("封盘中...");
			window.clearInterval(lotteryNumber.param.lhcIntervalKey); //终止周期性倒计时
			lotteryNumber.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
			lotteryNumber.getLastLotteryCode(issue.lotteryType);
			
		};
		issue.timeDifference = lotteryNumber.param.lhcDiffTime;
	}else if(issue.lotteryType == "XYEB"){
		lotteryNumber.param.xyebDiffTime = issue.timeDifference;
		SurplusSecond = issue.timeDifference;
		if(SurplusSecond>=3600){
			window.clearInterval(lotteryNumber.param.xyebIntervalKey); //终止周期性倒计时
		}
		lotteryNumber.calculateTime(SurplusSecond,"XYEB");
		//lotteryNumber.showCurrentExpect(issue,"XYEB",false);
		lotteryNumber.param.xyebDiffTime--;
		if(lotteryNumber.param.xyebDiffTime < 0){
			$("#"+issue.lotteryType+"time").html("开奖中...");
			//获取最新开奖号码
			lotteryNumber.getLastLotteryCode(issue.lotteryType);
			//是否已经获取最新号码期号 开奖完毕，可以重新显示时间
			var flag = lotteryNumber.isGetNewCode(issue);
			if(flag){
			  window.clearInterval(lotteryNumber.param.xyebIntervalKey); //终止周期性倒计时
			  lotteryNumber.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
			}
			
		};
		issue.timeDifference = lotteryNumber.param.xyebDiffTime;
	}
}
/**
 * 定时获取开奖最新号码
 */
LotteryNumber.prototype.isGetNewCode = function(issue){
	//当前期号  
	var expect = lotteryNumber.getExpectByKind(issue);
	var expectNumInfo = issue.lotteryType;
	var nowexpecthtml = $("#"+expectNumInfo+"Number").text();
	var expectStr = "第" + expect+ "期";
	if(expectStr == nowexpecthtml){
		return true;
	}else{
		return false;
	}
}

/**
 * 根据彩种期号显示
 * @param lotteryIssue
 * @param expectNumInfo
 */
LotteryNumber.prototype.getExpectByKind = function(lotteryIssue){
	 var lotteryNum = lotteryIssue.lotteryNum;
	 var expectNumInfo = lotteryIssue.lotteryType;
	 expectDay = lotteryNum.substring(0,lotteryNum.length - 3);
	 expectNum = lotteryNum.substring(lotteryNum.length - 3,lotteryNum.length);
	 if(expectNumInfo == "BJKS" || expectNumInfo == "FCSDDPC" || expectNumInfo == "LHC"
		 || expectNumInfo == "BJPK10" || expectNumInfo == "XJPLFC" || expectNumInfo == "TWWFC"){
		 return lotteryNum;
	 }else if(expectNumInfo == "XYEB"){
		 return expectDay+expectNum;
	 }else{
	    return expectDay + "-" + expectNum;
	 }
}

/**
 * 设置定时器
 */
LotteryNumber.prototype.setAllInterval = function(issue){
	if(issue.lotteryType == "CQSSC"){
		window.clearInterval(lotteryNumber.param.cqsscIntervalKey); //终止周期性倒计时
		lotteryNumber.param.cqsscIntervalKey = setInterval(function(){lotteryNumber.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "JXSSC"){
		window.clearInterval(lotteryNumber.param.jxsscIntervalKey); //终止周期性倒计时
		lotteryNumber.param.jxsscIntervalKey = setInterval(function(){lotteryNumber.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "TJSSC"){
		window.clearInterval(lotteryNumber.param.tjsscIntervalKey); //终止周期性倒计时
		lotteryNumber.param.tjsscIntervalKey = setInterval(function(){lotteryNumber.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "XJSSC"){
		window.clearInterval(lotteryNumber.param.xjsscIntervalKey); //终止周期性倒计时
		lotteryNumber.param.xjsscIntervalKey = setInterval(function(){lotteryNumber.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "HLJSSC"){
		window.clearInterval(lotteryNumber.param.hljsscIntervalKey); //终止周期性倒计时
		lotteryNumber.param.hljsscIntervalKey = setInterval(function(){lotteryNumber.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "JLFFC"){
		window.clearInterval(lotteryNumber.param.jlffcIntervalKey); //终止周期性倒计时
		lotteryNumber.param.jlffcIntervalKey = setInterval(function(){lotteryNumber.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "HGFFC"){
		window.clearInterval(lotteryNumber.param.hgffcIntervalKey); //终止周期性倒计时
		lotteryNumber.param.hgffcIntervalKey = setInterval(function(){lotteryNumber.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "GDSYXW"){
		window.clearInterval(lotteryNumber.param.gdsyxwIntervalKey); //终止周期性倒计时
		lotteryNumber.param.gdsyxwIntervalKey = setInterval(function(){lotteryNumber.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "SDSYXW"){
		window.clearInterval(lotteryNumber.param.sdsyxwIntervalKey); //终止周期性倒计时
		lotteryNumber.param.sdsyxwIntervalKey = setInterval(function(){lotteryNumber.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "JXSYXW"){
		window.clearInterval(lotteryNumber.param.jxsyxwIntervalKey); //终止周期性倒计时
		lotteryNumber.param.jxsyxwIntervalKey = setInterval(function(){lotteryNumber.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "FJSYXW"){
		window.clearInterval(lotteryNumber.param.fjsyxwIntervalKey); //终止周期性倒计时
		lotteryNumber.param.fjsyxwIntervalKey = setInterval(function(){lotteryNumber.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "CQSYXW"){
		window.clearInterval(lotteryNumber.param.cqsyxwIntervalKey); //终止周期性倒计时
		lotteryNumber.param.cqsyxwIntervalKey = setInterval(function(){lotteryNumber.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "JSKS"){
		window.clearInterval(lotteryNumber.param.jsksIntervalKey); //终止周期性倒计时
		lotteryNumber.param.jsksIntervalKey = setInterval(function(){lotteryNumber.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "SHKS"){
		window.clearInterval(lotteryNumber.param.shksIntervalKey); //终止周期性倒计时
		lotteryNumber.param.shksIntervalKey = setInterval(function(){lotteryNumber.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "GSKS"){
		window.clearInterval(lotteryNumber.param.gsksIntervalKey); //终止周期性倒计时
		lotteryNumber.param.gsksIntervalKey = setInterval(function(){lotteryNumber.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "FJKS"){
		window.clearInterval(lotteryNumber.param.fjksIntervalKey); //终止周期性倒计时
		lotteryNumber.param.fjksIntervalKey = setInterval(function(){lotteryNumber.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "AHKS"){
		window.clearInterval(lotteryNumber.param.ahksIntervalKey); //终止周期性倒计时
		lotteryNumber.param.ahksIntervalKey = setInterval(function(){lotteryNumber.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "HBKS"){
		window.clearInterval(lotteryNumber.param.hbksIntervalKey); //终止周期性倒计时
		lotteryNumber.param.hbksIntervalKey = setInterval(function(){lotteryNumber.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "JLKS"){
		window.clearInterval(lotteryNumber.param.jlksIntervalKey); //终止周期性倒计时
		lotteryNumber.param.jlksIntervalKey = setInterval(function(){lotteryNumber.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "BJKS"){
		window.clearInterval(lotteryNumber.param.bjksIntervalKey); //终止周期性倒计时
		lotteryNumber.param.bjksIntervalKey = setInterval(function(){lotteryNumber.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "JYKS"){
		window.clearInterval(lotteryNumber.param.jyksIntervalKey); //终止周期性倒计时
		lotteryNumber.param.jyksIntervalKey = setInterval(function(){lotteryNumber.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "GXKS"){
		window.clearInterval(lotteryNumber.param.gxksIntervalKey); //终止周期性倒计时
		lotteryNumber.param.gxksIntervalKey = setInterval(function(){lotteryNumber.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "FCSDDPC"){
		window.clearInterval(lotteryNumber.param.fcsddpcIntervalKey); //终止周期性倒计时
		lotteryNumber.param.fcsddpcIntervalKey = setInterval(function(){lotteryNumber.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "BJPK10"){
		window.clearInterval(lotteryNumber.param.pk10IntervalKey); //终止周期性倒计时
		lotteryNumber.param.pk10IntervalKey = setInterval(function(){lotteryNumber.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "XJPLFC"){
		window.clearInterval(lotteryNumber.param.xjplfcIntervalKey); //终止周期性倒计时
		lotteryNumber.param.xjplfcIntervalKey = setInterval(function(){lotteryNumber.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "TWWFC"){
		window.clearInterval(lotteryNumber.param.twwfcIntervalKey); //终止周期性倒计时
		lotteryNumber.param.twwfcIntervalKey = setInterval(function(){lotteryNumber.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "LHC"){
		window.clearInterval(lotteryNumber.param.lhcIntervalKey); //终止周期性倒计时
		lotteryNumber.param.lhcIntervalKey = setInterval(function(){lotteryNumber.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "XYEB"){
		window.clearInterval(lotteryNumber.param.xyebIntervalKey); //终止周期性倒计时
		lotteryNumber.param.xyebIntervalKey = setInterval(function(){lotteryNumber.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}
}


/**
 * 服务器时间倒计时
 */
LotteryNumber.prototype.calculateTime = function(SurplusSecond,timeInfo){
	var diffTimeStr = "";
	if(timeInfo == "FCSDDPC" || timeInfo == "LHC"){
		if(SurplusSecond>= (3600 * 24) && timeInfo != "LHC"){
			//显示预售中
			$("#"+timeInfo+"time").html("即将开盘...");
			window.clearInterval(lotteryNumber.param.fcsddpcIntervalKey); //终止周期性倒计时
		}else{
			var h = Math.floor(SurplusSecond/3600);
			if (h<10){
				h = "0"+h;
			}
			//计算剩余的分钟
			SurplusSecond = SurplusSecond - (h * 3600);
			var m = Math.floor(SurplusSecond/60);
			if (m<10){
				m = "0"+m;
			}
			var s = SurplusSecond%60;
			if(s<10){
				s = "0"+s;
			}
			
			h = h.toString();
			m = m.toString();
			s = s.toString();
			
			diffTimeStr = h+":"+ m +":" + s;
			$("#"+timeInfo+"time").html(diffTimeStr);
		}
	}else{
		if(SurplusSecond>=3600){
			$("#"+timeInfo+"time").html("即将开盘...");
		}else{
			var m = Math.floor(SurplusSecond/60);
			if (m<10){
				m = "0"+m;
			}
			var s = SurplusSecond%60;
			if(s<10){
				s = "0"+s;
			}
			
			m = m.toString();
			s = s.toString();
			var mArray = m.split("");
			var sArray = s.split("");
			
			diffTimeStr += mArray[0]+mArray[1]+"分";
			diffTimeStr += sArray[0]+sArray[1]+"秒";
			
			$("#"+timeInfo+"time").html(diffTimeStr);
		}
	}
};

/**
 * 获取所有彩种最新的开奖号码
 */
LotteryNumber.prototype.getAllLastLotteryCode = function(){
	frontLotteryCodeAction.getAllLastLotteryCode(function(r){
		if (r[0] != null && r[0] == "ok") {
			var lotteryCodeList = r[1];
			if(lotteryCodeList != null){
				for(var i=0;i < lotteryCodeList.length;i++){
					var lotteryCode = lotteryCodeList[i];
					if(lotteryCode != null){
						var lotteryName=lotteryCode.lotteryName.toString().toLowerCase();
						lotteryNumber.showCurrentlotteryMun(lotteryCode,lotteryName);
					}
				}
			}
		}else if(r[0] != null && r[0] == "error"){
			frontCommonPage.showKindlyReminder(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "获取最新开奖号码的请求失败.");
		}
    });
};

/**
 * 获取最新的开奖号码
 */
LotteryNumber.prototype.getLastLotteryCode = function(lotteryKind){
	frontLotteryCodeAction.getLastLotteryCode(lotteryKind,function(r){
		if (r[0] != null && r[0] == "ok") {
			var lotteryCode = r[1];
			if(lotteryCode != null){
				var lotteryName=lotteryCode.lotteryName.toString();
				lotteryNumber.showCurrentlotteryMun(lotteryCode,lotteryName);
			}
		}else if(r[0] != null && r[0] == "error"){
			frontCommonPage.showKindlyReminder(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "获取最新开奖号码的请求失败.");
		}
    });
};

/**
 * 开奖号码显示
 * @param lotteryIssue
 * @param expectNumInfo
 */
LotteryNumber.prototype.showCurrentlotteryMun = function(lotteryCode,lotteryMunInfo){
	//显示期号
	lotteryNumber.showCurrentExpect(lotteryCode,lotteryMunInfo,true);
	var str = "";
	if(lotteryCode.lotteryName=='BJPK10'){
		str +='<span class="btn btn-red">'+parseInt(lotteryCode.numInfo1)+'</span>';
		str +='<span class="btn btn-red">'+parseInt(lotteryCode.numInfo2)+'</span>';
		str +='<span class="btn btn-red">'+parseInt(lotteryCode.numInfo3)+'</span>';
		str +='<span class="btn btn-red">'+parseInt(lotteryCode.numInfo4)+'</span>';
		str +='<span class="btn btn-red">'+parseInt(lotteryCode.numInfo5)+'</span>';
		str +='<span class="btn btn-red">'+parseInt(lotteryCode.numInfo6)+'</span>';
		str +='<span class="btn btn-red">'+parseInt(lotteryCode.numInfo7)+'</span>';
		str +='<span class="btn btn-red">'+parseInt(lotteryCode.numInfo8)+'</span>';
		str +='<span class="btn btn-red">'+parseInt(lotteryCode.numInfo9)+'</span>';
		str +='<span class="btn btn-red">'+parseInt(lotteryCode.numInfo10)+'</span>';
		$("#"+lotteryMunInfo+"muns").html(str);
	}else if(lotteryCode.lotteryName.lastIndexOf('KS')>-1){
		str +='<span class="btn k3_no'+parseInt(lotteryCode.numInfo1)+'"></span>';
		str +='<span class="btn k3_no'+parseInt(lotteryCode.numInfo2)+'"></span>';
		str +='<span class="btn k3_no'+parseInt(lotteryCode.numInfo3)+'"></span>';
		$("#"+lotteryMunInfo+"muns").html(str);
	}else if(lotteryCode.lotteryName.lastIndexOf('DPC')>-1){
		str +='<span class="btn btn-red">'+parseInt(lotteryCode.numInfo1)+'</span>';
		str +='<span class="btn btn-red">'+parseInt(lotteryCode.numInfo2)+'</span>';
		str +='<span class="btn btn-red">'+parseInt(lotteryCode.numInfo3)+'</span>';
		$("#"+lotteryMunInfo+"muns").html(str);
	}else if(lotteryCode.lotteryName=='LHC'){
		var  colors = lotteryNumber.liuhecaiColorByCode(lotteryCode);
		str +='<span class="'+colors[0]+'">'+parseInt(lotteryCode.numInfo1)+'</span>';
		str +='<span class="'+colors[1]+'">'+parseInt(lotteryCode.numInfo2)+'</span>';
		str +='<span class="'+colors[2]+'">'+parseInt(lotteryCode.numInfo3)+'</span>';
		str +='<span class="'+colors[3]+'">'+parseInt(lotteryCode.numInfo4)+'</span>';
		str +='<span class="'+colors[4]+'">'+parseInt(lotteryCode.numInfo5)+'</span>';
		str +='<span class="'+colors[5]+'">'+parseInt(lotteryCode.numInfo6)+'</span><span class="btn btn-add"></span>';
		str +='<span class="'+colors[6]+'">'+parseInt(lotteryCode.numInfo7)+'</span>';
		$("#"+lotteryMunInfo+"muns").html(str);
	}else if(lotteryCode.lotteryName=='XYEB'){
		var sum=parseInt(lotteryCode.numInfo1)+parseInt(lotteryCode.numInfo2)+parseInt(lotteryCode.numInfo3);
		str +='<span class="btn btn-red">'+parseInt(lotteryCode.numInfo1)+'</span><span class="btn btn-add"></span>';
		str +='<span class="btn btn-red">'+parseInt(lotteryCode.numInfo2)+'</span><span class="btn btn-add"></span>';
		str +='<span class="btn btn-red">'+parseInt(lotteryCode.numInfo3)+'</span><span class="btn btn-equal"></span>';
		str +='<span class="'+changeColorByCode(sum)+'">'+sum+'</span>';
		$("#"+lotteryMunInfo+"muns").html(str);
	}else{
		str +='<span class="btn btn-red">'+parseInt(lotteryCode.numInfo1)+'</span>';
		str +='<span class="btn btn-red">'+parseInt(lotteryCode.numInfo2)+'</span>';
		str +='<span class="btn btn-red">'+parseInt(lotteryCode.numInfo3)+'</span>';
		str +='<span class="btn btn-red">'+parseInt(lotteryCode.numInfo4)+'</span>';
		str +='<span class="btn btn-red">'+parseInt(lotteryCode.numInfo5)+'</span>';
		$("#"+lotteryMunInfo+"muns").html(str);
	}
};

/**
 * 期号显示
 * @param lotteryIssue
 * @param expectNumInfo
 */
LotteryNumber.prototype.showCurrentExpect = function(lotteryIssue,expectNumInfo,oldBoolean){
	 $("#"+expectNumInfo+"Number").empty();
	 var lotteryNum = lotteryIssue.lotteryNum;
	 expectDay = lotteryNum.substring(0,lotteryNum.length - 3);
	 expectNum = lotteryNum.substring(lotteryNum.length - 3,lotteryNum.length);
	 if(expectNumInfo == "BJKS" || expectNumInfo == "FCSDDPC" || expectNumInfo == "LHC"
		 || expectNumInfo == "BJPK10" || expectNumInfo == "XJPLFC" || expectNumInfo == "TWWFC"){
		 /*if(oldBoolean){
			 $("#"+expectNumInfo+"Number").html("第" + lotteryNum+ "期");
		 }else{
			 $("#"+expectNumInfo+"Number").html("第" + lotteryNum+ "期");
		 }*/
		 $("#"+expectNumInfo+"Number").html("第" + lotteryNum+ "期");
	 }else if(expectNumInfo == "XYEB"){
		 $("#"+expectNumInfo+"Number").html("第" + expectDay+expectNum+ "期");
	 }else{
		 /*if(oldBoolean){
		 $("#"+expectNumInfo+"Number").html("第" + expectDay + "-" + expectNum+ "期");
	 }else{
		 $("#"+expectNumInfo+"Number").html("第" + expectDay + "-" + expectNum+ "期");
	 }*/
	 $("#"+expectNumInfo+"Number").html("第" + expectDay + "-" + expectNum+ "期");
 }
};


/**
 * 控制是否能发起期号的请求
 */
LotteryNumber.prototype.controlIsCanActive = function(){
	lotteryNumber.param.isCanActiveExpect = true;
};

function changeStyle(e){
    var $target = $(e),
        $main = $(".main");
    $main.hide();
    setTimeout(function($main){
        $main.show();
    },100,$main);
    if($main.hasClass('main-block')){
        $target.find(".icon").attr("src",contextPath+"/images/icon/icon-threetool.png");
        $main.removeClass('main-block');
    }else{
        $target.find(".icon").attr("src",contextPath+"/images/icon/icon-list.png");
        $main.addClass('main-block');
    }
}

function changeMain(e,index){
    var $target = $(e),
        $main = $(".main"),
        $mainNav = $(".main-nav");
    $main.removeClass("on");
    $main.eq(index).addClass("on");
    $mainNav.find(".child").removeClass("on");
    $target.addClass("on");
}
function changeColorByCode(k){
	var colors="";
	if(k==3||k==6||k==9||k==12||k==15||k==18||k==21||k==24){
		colors="btn btn-red";
	}else if(k==1||k==4||k==7||k==10||k==16||k==19||k==22||k==25){
		colors="btn btn-green";
	}else if(k==2||k==5||k==8||k==11||k==17||k==20||k==23||k==26){
		colors="btn btn-blue";
	}else if(k== 0||k==13||k==14||k==27){
		colors="btn btn-gray";
	}
	return colors;
}

lotteryNumber.boshe = {
	hongbo:[1,2,7,8,12,13,18,19,23,24,29,30,34,35,40,45,46],
	lanbo:[3,4,9,10,14,15,20,25,26,31,36,37,41,42,47,48],
	lvbo:[5,6,11,16,17,21,22,27,28,32,33,38,39,43,44,49]
}

//计算颜色
LotteryNumber.prototype.liuhecaiColorByCode = function(lotteryCode){
	var colors = new Array();
	var hong = lotteryNumber.boshe.hongbo;
	var lan = lotteryNumber.boshe.lanbo;
	var lv  = lotteryNumber.boshe.lvbo;
	
	//判断是否是红波
	for(var i=0;i<hong.length;i++){
		if(Number(hong[i])==Number(lotteryCode.numInfo1)){
			colors[0] = 'btn btn-red';
		}
		if(Number(hong[i])==Number(lotteryCode.numInfo2)){
			colors[1] = 'btn btn-red';
		}
		if(Number(hong[i])==Number(lotteryCode.numInfo3)){
			colors[2] = 'btn btn-red';
		}
		if(Number(hong[i])==Number(lotteryCode.numInfo4)){
			colors[3] = 'btn btn-red';
		}
		if(Number(hong[i])==Number(lotteryCode.numInfo5)){
			colors[4] = 'btn btn-red';
		}
		if(Number(hong[i])==Number(lotteryCode.numInfo6)){
			colors[5] = 'btn btn-red';
		}
		if(Number(hong[i])==Number(lotteryCode.numInfo7)){
			colors[6] = 'btn btn-red';
		}
	}
	
	//判断是否是蓝波
	for(var i=0;i<lan.length;i++){
		if(Number(lan[i])==Number(lotteryCode.numInfo1)){
			colors[0] = 'btn btn-blue';
		}
		if(Number(lan[i])==Number(lotteryCode.numInfo2)){
			colors[1] = 'btn btn-blue';
		}
		if(Number(lan[i])==Number(lotteryCode.numInfo3)){
			colors[2] = 'btn btn-blue';
		}
		if(Number(lan[i])==Number(lotteryCode.numInfo4)){
			colors[3] = 'btn btn-blue';
		}
		if(Number(lan[i])==Number(lotteryCode.numInfo5)){
			colors[4] = 'btn btn-blue';
		}
		if(Number(lan[i])==Number(lotteryCode.numInfo6)){
			colors[5] = 'btn btn-blue';
		}
		if(Number(lan[i])==Number(lotteryCode.numInfo7)){
			colors[6] = 'btn btn-blue';
		}
	}
	
	//判断是否是绿波
	for(var i=0;i<lv.length;i++){
		if(Number(lv[i])==Number(lotteryCode.numInfo1)){
			colors[0] = 'btn btn-green';
		}
		if(Number(lv[i])==Number(lotteryCode.numInfo2)){
			colors[1] = 'btn btn-green';
		}
		if(Number(lv[i])==Number(lotteryCode.numInfo3)){
			colors[2] = 'btn btn-green';
		}
		if(Number(lv[i])==Number(lotteryCode.numInfo4)){
			colors[3] = 'btn btn-green';
		}
		if(Number(lv[i])==Number(lotteryCode.numInfo5)){
			colors[4] = 'btn btn-green';
		}
		if(Number(lv[i])==Number(lotteryCode.numInfo6)){
			colors[5] = 'btn btn-green';
		}
		if(Number(lv[i])==Number(lotteryCode.numInfo7)){
			colors[6] = 'btn btn-green';
		}
	}
	
	return colors;
}