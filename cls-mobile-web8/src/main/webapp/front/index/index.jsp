<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.service.BizSystemDomainService"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" 
 import="com.team.lottery.system.BizSystemConfigVO,com.team.lottery.system.BizSystemConfigManager,
         com.team.lottery.vo.LotterySet,com.team.lottery.cache.ClsCacheManager,
         com.team.lottery.system.BizSystemConfigManager,com.team.lottery.vo.BizSystemInfo,net.sf.json.JSONObject,
         com.team.lottery.util.LotterySetUntil,com.team.lottery.extvo.LotterySetMsg,com.team.lottery.vo.BizSystemDomain"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String serverName=  request.getServerName();//获取当前域名
	String bizSystem=ClsCacheManager.getBizSystemByDomainUrl(serverName);
	BizSystemConfigVO bizSystemConfigVO=BizSystemConfigManager.getBizSystemConfig(bizSystem);//业务系统彩种设置
	BizSystemInfo bizSystemInfo=ClsCacheManager.getBizSystemInfo(bizSystem);//热门彩种设置，首页彩种设置
	LotterySet lotterySet=new LotterySet();
	LotterySetUntil lotterySetUntil=new LotterySetUntil();
	 //热门彩种
    LotterySetMsg lotterySetMsgHot=lotterySetUntil.compareLotterySetALL(lotterySet, bizSystemConfigVO, bizSystemInfo);
	//全部彩种的
    LotterySetMsg lotterySetMsgAll=lotterySetUntil.compareLotterySetAndBizSystemConfigVO(lotterySet, bizSystemConfigVO);
    request.setAttribute("lotterySetMsgHot", lotterySetMsgHot);
    request.setAttribute("lotterySetMsgAll", lotterySetMsgAll);
    if(bizSystemInfo!=null&&bizSystemInfo.getHotLotteryKinds()!=null){
     request.setAttribute("kinds", bizSystemInfo.getHotLotteryKinds().split(","));
    }else{
    	request.setAttribute("kinds", "");
    };
    //根据域名获取域名类型
    String domainType=ClsCacheManager.getLoginType(serverName);
 	request.setAttribute("domainType",domainType);
 	request.setAttribute("hasapp",bizSystemInfo.getHasApp());
 	JSONObject obj = JSONObject.fromObject(bizSystemConfigVO);
    String strJson = obj.toString();
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="format-detection" content="telephone=no">
    <title>首页</title>
    <link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/index.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <jsp:include page="/front/include/include.jsp"></jsp:include>
	<script>
		/* 手机浏览器类型检测 */
		$(function(){
			if (/Safari/.test(navigator.userAgent) && !/Chrome/.test(navigator.userAgent)) {
				$('.app-instruction span').addClass('more-arrow-dark');
				$('.app-instruction span').removeClass('more-dots');
			} else {
				$('.app-instruction span').addClass('more-dots');
				$('.app-instruction span').removeClass('more-arrow-dark');
			}
		})
		var sssurl='<%=bizSystemInfo.getAndriodIosDownloadUrl()%>';
	</script>
</head>
<body>

    <!-- header -->
    <div class="header color-dark indexs" id="userLogin" style="display: none">
        <div class="header-left" onclick="window.location.href='login.html'" <c:if  test="${domainType=='MOBILE' && hasapp=='1'}">style="font-size: 44px;"</c:if><c:if test="${domainType!='MOBILE'||hasapp=='0'}">style="font-size: 44px;right:150px;left:auto;"</c:if> >登录</div>
        <h2 class="header-title" id="indexTitle"></h2>
        <div class="header-right" onclick="window.location.href='<%=path%>/gameBet/reg.html'" <c:if  test="${domainType=='MOBILE' && hasapp=='1'}">style="left:150px;right:auto;font-size: 44px;"</c:if><c:if test="${domainType!='MOBILE'||hasapp=='0'}">style="font-size: 44px;right:0;left:auto;"</c:if> >注册</div>
        <c:if test="${domainType=='MOBILE' && hasapp=='1'}">
        	<div class="header-right" onclick="window.location.href='<%=path%>/gameBet/appdownload.html'" style="font-size: 52px;">APP<img class="icon" style="margin-left:12px;margin-top:-8px;" src="<%=path%>/images/icon/icon-download.png"></div>
        </c:if>
    </div>
    <div class="header color-dark indexs" id="userInfo" style="display: none">
        <div class="header-left" onclick="window.location.href='my.html'"><img class="icon" src="<%=path%>/images/icon/icon-fulluser.png"></div>
        <h2 class="header-title" id="indexTitle"><%-- <img src="<%=path%>/images/user/save/header-logo.png" alt=""> --%></h2>
    </div>
    <!-- header over -->
    <div class="stance"></div>
    <!-- banner -->
    <div class="banner">
    	<div id="homeActivityImage">
    	    <%-- <div class="banner-content" style="background-image:url(<%=path%>/images/index/banner2.jpg)"></div>
	        <div class="banner-content" style="background-image:url(<%=path%>/images/index/banner2.jpg)"></div>
	        <div class="banner-content" style="background-image:url(<%=path%>/images/index/banner2.jpg)"></div> --%>
    	</div>
        <div class="banner-nav"></div>
    </div>
    <!-- banner over -->
    <!-- notice -->
    <div class="notice" style="display: none">
        <div class="container">
            <img src="<%=path%>/images/icon/icon-volice.png" class="volice" alt="">
            <img src="<%=path%>/images/icon/icon-indexright.png" class="pointer" alt="">
            <div class="content" id="notice">
                <!-- <p></p>
                <p>平台推出移动快捷支付功能，更为快捷的支2</p>
                <p>平台推出移动快捷支付功能，更为快捷的3</p> -->
            </div>
        </div>
    </div>
    <!-- notice over -->
   

    <!-- main-nav -->
    <div class="main-nav">
        <div class="child" onclick="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/rechargeList.html','要想充值用户必须要先登录?')">
            <div class="child-icon"><img src="<%=path%>/images/index/icon1.png" alt=""></div>
            <div class="child-title"><span>充值</span></div>
        </div>
        <div class="child" onclick="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/withdraw.html','要想提现用户必须要先登录?')">
            <div class="child-icon"><img src="<%=path%>/images/index/icon2.png" alt=""></div>
            <div class="child-title"><span>提现</span></div>
        </div>
        <div class="child" onclick="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/order.html?status=bettingRecords','要查看投注记录信息用户必须要先登录?')">
            <div class="child-icon"><img src="<%=path%>/images/index/icon3.png" alt=""></div>
            <div class="child-title"><span>投注记录</span></div>
        </div>
        <div class="child" id="customServicekf">
            <div class="child-icon"><img src="<%=path%>/images/index/icon4.png" alt=""></div>
            <div class="child-title"><span>客服<span></div>
        </div>
    </div>
    <!-- main-nav over -->

    <!-- lottery -->
    <div class="main lotterys" id="ksLottery" style="display: none;">
        <div class="container">
            <h2 class="title lastnumber" id="index-lottery-info-lastnumber">江苏快三 第20160827-052期</h2>
            <div class="main-content">
                <div class="dice">
                    <div class="dice-content">
                        <img src="<%=path%>/images/dice_pic/index/1.png" id="index_lotteryNumber1" alt="">
                        <img src="<%=path%>/images/dice_pic/index/3.png" id="index_lotteryNumber2" alt="">
                        <img src="<%=path%>/images/dice_pic/index/6.png" id="index_lotteryNumber3" alt="">
                    </div>
                </div>
                <div class="bet" onclick="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jsks/jsks','去投注用户必须要先登录')">
                    <div class="bet-content">
                        <span>去投注</span>
                        <img src="<%=path%>/images/icon/icon-shopping.png" alt="" class="icon">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main lotterys" id="notKslotteryKind">
		<div class="container">
			<h2 class="title lastnumber">--彩 第000-000期</h2>
			<div class="main-content">
				<div class="dice muns muns-big">
					<div class="dice-content">
						<div class="lhc-mun" id="lotteryNumber1"><div class="mun color-red numInfo1" style="display: none;">-</div><p class="btn-title" style="display: none;">-</p><span class="symbol" style="display: none;">+</span></div>
						<div class="lhc-mun" id="lotteryNumber2"><div class="mun color-red numInfo2" style="display: none;">-</div><p class="btn-title" style="display: none;">-</p><span class="symbol" style="display: none;">+</span></div>
						<div class="lhc-mun" id="lotteryNumber3"><div class="mun color-red numInfo3" style="display: none;">-</div><p class="btn-title" style="display: none;">-</p><span class="symbol" style="display: none;">=</span></div>
						<div class="lhc-mun" id="lotteryNumber4"><div class="mun color-red numInfo4" style="display: none;">-</div><p class="btn-title" style="display: none;">-</p><span class="symbol" style="display: none;">&nbsp;</span></div>
						<div class="lhc-mun" id="lotteryNumber5"><div class="mun color-red numInfo5" style="display: none;">-</div><p class="btn-title" style="display: none;">-</p></div>
						<div class="lhc-mun" id="lotteryNumber6"><div class="mun color-red numInfo6" style="display: none;">-</div><p class="btn-title" style="display: none;">-</p></div>
						<div class="lhc-mun"><div class="mun and" style="display: none;">+</div><p class="btn-title">&nbsp;</p></div>
						<div class="lhc-mun" id="lotteryNumber7"><div class="mun color-red numInfo7" style="display: none;">-</div><p class="btn-title" style="display: none;">-</p></div>
						<div class="lhc-mun" id="lotteryNumber8"><div class="mun color-red numInfo8" style="display: none;">-</div><p class="btn-title" style="display: none;">-</p></div>
						<div class="lhc-mun" id="lotteryNumber9"><div class="mun color-red numInfo9" style="display: none;">-</div><p class="btn-title" style="display: none;">-</p></div>
						<div class="lhc-mun" id="lotteryNumber10"><div class="mun color-red numInfo10" style="display: none;">-</div><p class="btn-title" style="display: none;">-</p></div>
					</div>
				</div>

				<div class="bet" >
					<div class="bet-content">
						<span>去投注</span>
						<i class="ion-android-cart"></i>
					</div>
				</div>
			</div>
		</div>
	</div> 
    <!-- <div class="main lotterys">
		<div class="container">
			<h2 class="title">重庆时时彩 第20160827-052期</h2>
			<div class="main-content">
				<div class="dice muns muns-small">
					<div class="dice-content">
						<div class="mun color-red">02</div>
						<div class="mun color-red">04</div>
						<div class="mun color-red">06</div>
						<div class="mun color-red">04</div>
						<div class="mun color-red">06</div>
						<div class="mun color-red">02</div>
						<div class="mun color-red">04</div>
						<div class="mun color-red">06</div>
						<div class="mun color-red">04</div>
						<div class="mun color-red">06</div>
					</div>
				</div>

				<div class="bet" >
					<div class="bet-content">
						<span>去投注</span>
						<img src="images/icon/icon-shopping.png" alt="" class="icon">
					</div>
				</div>
			</div>
		</div>
	</div> -->
    <!-- lottery over -->
    <!-- footballgame -->
    <div class="footballgame"><a href="javascript:;"><img src="<%=path%>/images/index/footballgame.png" alt=""></a></div>
    <!-- footballgame end -->
    <!-- classify -->
    <div class="main classify">
        <div class="head" onclick="window.location.href='hall.html'">
            <div class="container">
                <div class="title">
                    <img src="<%=path%>/images/icon/icon-nav.png" alt="" class="icon">
                    <span>热门彩种</span>
                </div>
                <img src="<%=path%>/images/icon/icon-indexright.png" alt="" class="icon setting">
            </div>
        </div>

        <div class="main-content">
        	<c:forEach var="kind" items="${kinds}">
        	<c:if test="${lotterySetMsgHot.isCQSSCOpen==1 && kind=='CQSSC'}">
            <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/cqssc/cqssc.html','需要登录才能进行投注，是否登录？')" class="cqsscInfo">
                <div class="child">
                    <img src="<%=path%>/images/lottery_logo/hall/logo_ssc.png" alt="" class="child-logo">
                    <div class="child-content">
                        <h2 class="child-title">重庆时时彩</h2>
                        <p class="child-time">加载中...</p>
                    </div>
                </div>
            </a>
            </c:if>
            <c:if test="${lotterySetMsgHot.isTJSSCOpen==1 && kind=='TJSSC'}">
            <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/tjssc/tjssc.html','需要登录才能进行投注，是否登录？')" class="tjsscInfo">
                <div class="child">
                    <img src="<%=path%>/images/lottery_logo/hall/logo_ssc.png" alt="" class="child-logo">
                    <div class="child-content">
                        <h2 class="child-title">天津时时彩</h2>
                        <p class="child-time">加载中...</p>
                    </div>
                </div>
            </a>
            </c:if>
            <c:if test="${lotterySetMsgHot.isXJSSCOpen==1 && kind=='XJSSC'}">
            <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/xjssc/xjssc.html','需要登录才能进行投注，是否登录？')" class="xjsscInfo">
                <div class="child">
                    <img src="<%=path%>/images/lottery_logo/hall/logo_ssc.png" alt="" class="child-logo">
                    <div class="child-content">
                        <h2 class="child-title">新疆时时彩</h2>
                        <p class="child-time">加载中...</p>
                    </div>
                </div>
            </a>
            </c:if>
            <c:if test="${lotterySetMsgHot.isFJSYXWOpen==1 && kind=='FJSYXW'}">
            <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/fjsyxw/fjsyxw.html','需要登录才能进行投注，是否登录？')" class="fjsyxwInfo">
                <div class="child">
                    <img src="<%=path%>/images/lottery_logo/hall/logo_11xuan5.png" alt="" class="child-logo">
                    <div class="child-content">
                        <h2 class="child-title">福建11选5</h2>
                        <p class="child-time">加载中...</p>
                    </div>
                </div>
            </a>
            </c:if>
            <c:if test="${lotterySetMsgHot.isGDSYXWOpen==1 && kind=='GDSYXW'}">
            <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/gdsyxw/gdsyxw.html','需要登录才能进行投注，是否登录？')" class="gdsyxwInfo">
                <div class="child">
                    <img src="<%=path%>/images/lottery_logo/hall/logo_11xuan5.png" alt="" class="child-logo">
                    <div class="child-content">
                        <h2 class="child-title">广东11选5</h2>
                        <p class="child-time">加载中...</p>
                    </div>
                </div>
            </a>
            </c:if>
            <c:if test="${lotterySetMsgHot.isJXSYXWOpen==1 && kind=='JXSYXW'}">
            <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jxsyxw/jxsyxw.html','需要登录才能进行投注，是否登录？')" class="jxsyxwInfo">
                <div class="child">
                    <img src="<%=path%>/images/lottery_logo/hall/logo_11xuan5.png" alt="" class="child-logo">
                    <div class="child-content">
                        <h2 class="child-title">江西11选5</h2>
                        <p class="child-time">加载中...</p>
                    </div>
                </div>
            </a>
            </c:if>
            <c:if test="${lotterySetMsgHot.isSDSYXWOpen==1 && kind=='SDSYXW'}">
            <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/sdsyxw/sdsyxw.html','需要登录才能进行投注，是否登录？')" class="sdsyxwInfo">
                <div class="child">
                    <img src="<%=path%>/images/lottery_logo/hall/logo_11xuan5.png" alt="" class="child-logo">
                    <div class="child-content">
                        <h2 class="child-title">山东11选5</h2>
                        <p class="child-time">加载中...</p>
                    </div>
                </div>
            </a>
            </c:if>
            <c:if test="${lotterySetMsgHot.isBJPK10Open==1 && kind=='BJPK10'}"> 
            <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/bjpks/bjpk10.html','需要登录才能进行投注，是否登录？')" class="bjpk10Info">
                <div class="child">
                    <img src="<%=path%>/images/lottery_logo/hall/logo_pk10.png" alt="" class="child-logo">
                    <div class="child-content">
                        <h2 class="child-title">北京PK10</h2>
                        <p class="child-time">加载中...</p>
                    </div>
                </div>
            </a>
            </c:if>
           <c:if test="${lotterySetMsgHot.isJSPK10Open==1 && kind=='JSPK10'}"> 
            <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jspks/jspk10.html','需要登录才能进行投注，是否登录？')" class="jspk10Info">
                <div class="child">
                    <img src="<%=path%>/images/lottery_logo/hall/logo_jspk10.png" alt="" class="child-logo">
                    <div class="child-content">
                        <h2 class="child-title">极速PK10</h2>
                        <p class="child-time">加载中...</p>
                    </div>
                </div>
            </a>
            </c:if>
            <c:if test="${lotterySetMsgHot.isXYEBOpen==1 && kind=='XYEB'}"> 
            <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/xyeb/xy28.html','需要登录才能进行投注，是否登录？')" class="xyebInfo">
                <div class="child">
                    <img src="<%=path%>/images/lottery_logo/hall/logo_xy28.png" alt="" class="child-logo">
                    <div class="child-content">
                        <h2 class="child-title">幸运28</h2>
                        <p class="child-time">加载中...</p>
                    </div>
                </div>
            </a>
            </c:if>
            <c:if test="${lotterySetMsgHot.isLHCOpen==1  && kind=='LHC'}">
            <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/lhc/lhc.html','需要登录才能进行投注，是否登录？')" class="lhcInfo">
                <div class="child">
                    <img src="<%=path%>/images/lottery_logo/hall/logo_liuhecai.png" alt="" class="child-logo">
                    <div class="child-content">
                        <h2 class="child-title">香港六合彩</h2>
                        <p class="child-time">加载中...</p>
                    </div>
                </div>
            </a>
            </c:if>
            <c:if test="${lotterySetMsgHot.isXYLHCOpen==1  && kind=='XYLHC'}">
            <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/xylhc/xylhc.html','需要登录才能进行投注，是否登录？')" class="xylhcInfo">
                <div class="child">
                    <img src="<%=path%>/images/lottery_logo/hall/logo_xyliuhecai.png" alt="" class="child-logo">
                    <div class="child-content">
                        <h2 class="child-title">幸运六合彩</h2>
                        <p class="child-time">加载中...</p>
                    </div>
                </div>
            </a>
            </c:if>
            <c:if test="${lotterySetMsgHot.isJSKSOpen==1 && kind=='JSKS'}">
            <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jsks/jsks.html','需要登录才能进行投注，是否登录？')" class="jsksInfo">
                <div class="child">
                    <img src="<%=path%>/images/lottery_logo/hall/logo_kuai3.png" alt="" class="child-logo">
                    <div class="child-content">
                        <h2 class="child-title">江苏快3</h2>
                        <p class="child-time">加载中...</p>
                    </div>
                </div>
            </a>
            </c:if>
            <c:if test="${lotterySetMsgHot.isBJKSOpen==1 && kind=='BJKS'}">
            <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/bjks/bjks.html','需要登录才能进行投注，是否登录？')" class="bjksInfo">
                <div class="child">
                    <img src="<%=path%>/images/lottery_logo/hall/logo_kuai3.png" alt="" class="child-logo">
                    <div class="child-content">
                        <h2 class="child-title">北京快3</h2>
                        <p class="child-time">加载中...</p>
                    </div>
                </div>
            </a>
            </c:if>
            <c:if test="${lotterySetMsgHot.isHBKSOpen==1 && kind=='HBKS'}">
            <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/hbks/hbks.html','需要登录才能进行投注，是否登录？')" class="hbksInfo">
                <div class="child">
                    <img src="<%=path%>/images/lottery_logo/hall/logo_kuai3.png" alt="" class="child-logo">
                    <div class="child-content">
                        <h2 class="child-title">湖北快3</h2>
                        <p class="child-time">加载中...</p>
                    </div>
                </div>
            </a>
            </c:if>
            <c:if test="${lotterySetMsgHot.isAHKSOpen==1 && kind=='AHKS'}">
            <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/ahks/ahks.html','需要登录才能进行投注，是否登录？')" class="ahksInfo">
                <div class="child">
                    <img src="<%=path%>/images/lottery_logo/hall/logo_kuai3.png" alt="" class="child-logo">
                    <div class="child-content">
                        <h2 class="child-title">安徽快3</h2>
                        <p class="child-time">加载中...</p>
                    </div>
                </div>
            </a>
            </c:if>
            <c:if test="${lotterySetMsgHot.isJLKSOpen==1 && kind=='JLKS'}">
            <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jlks/jlks.html','需要登录才能进行投注，是否登录？')" class="jlksInfo">
                <div class="child">
                    <img src="<%=path%>/images/lottery_logo/hall/logo_kuai3.png" alt="" class="child-logo">
                    <div class="child-content">
                        <h2 class="child-title">吉林快3</h2>
                        <p class="child-time">加载中...</p>
                    </div>
                </div>
            </a>
            </c:if>
            <c:if test="${lotterySetMsgHot.isJYKSOpen==1 && kind=='JYKS'}">
            <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jyks/jyks.html','需要登录才能进行投注，是否登录？')" class="jyksInfo">
                <div class="child">
                    <img src="<%=path%>/images/lottery_logo/hall/logo_kuai3.png" alt="" class="child-logo">
                    <div class="child-content">
                        <h2 class="child-title">幸运快3</h2>
                        <p class="child-time">加载中...</p>
                    </div>
                </div>
            </a>
            </c:if>
           <c:if test="${lotterySetMsgHot.isGXKSOpen==1 && kind=='GXKS'}">
            <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/gxks/gxks.html','需要登录才能进行投注，是否登录？')" class="gxksInfo">
                <div class="child">
                    <img src="<%=path%>/images/lottery_logo/hall/logo_kuai3.png" alt="" class="child-logo">
                    <div class="child-content">
                        <h2 class="child-title">广西快3</h2>
                        <p class="child-time">加载中...</p>
                    </div>
                </div>
            </a>
            </c:if>
		     <c:if test="${lotterySetMsgHot.isGSKSOpen==1 && kind=='GSKS'}">
            <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/gsks/gsks.html','需要登录才能进行投注，是否登录？')" class="gsksInfo">
                <div class="child">
                    <img src="<%=path%>/images/lottery_logo/hall/logo_kuai3.png" alt="" class="child-logo">
                    <div class="child-content">
                        <h2 class="child-title">甘肃快3</h2>
                        <p class="child-time">加载中...</p>
                    </div>
                </div>
            </a>
            </c:if>
		    <c:if test="${lotterySetMsgHot.isSHKSOpen==1 && kind=='SHKS'}">
            <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/shks/shks.html','需要登录才能进行投注，是否登录？')" class="shksInfo">
                <div class="child">
                    <img src="<%=path%>/images/lottery_logo/hall/logo_kuai3.png" alt="" class="child-logo">
                    <div class="child-content">
                        <h2 class="child-title">上海快3</h2>
                        <p class="child-time">加载中...</p>
                    </div>
                </div>
            </a>
            </c:if>
            <c:if test="${lotterySetMsgHot.isTWWFCOpen==1 && kind=='TWWFC'}">
            <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/twwfc/twwfc.html','需要登录才能进行投注，是否登录？')" class="twwfcInfo">
                <div class="child">
                    <img src="<%=path%>/images/lottery_logo/hall/logo_fenfencai.png" alt="" class="child-logo">
                    <div class="child-content">
                        <h2 class="child-title">台湾5分彩</h2>
                        <p class="child-time">加载中...</p>
                    </div>
                </div>
            </a>
            </c:if>
            <c:if test="${lotterySetMsgHot.isJLFFCOpen==1 && kind=='JLFFC'}">
            <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/xyffc/xyffc.html','需要登录才能进行投注，是否登录？')" class="jlffcInfo">
                <div class="child">
                    <img src="<%=path%>/images/lottery_logo/hall/logo_fenfencai.png" alt="" class="child-logo">
                    <div class="child-content">
                        <h2 class="child-title">幸运分分彩</h2>
                        <p class="child-time">加载中...</p>
                    </div>
                </div>
            </a>
            </c:if>
           <c:if test="${lotterySetMsgHot.isFC3DDPCOpen==1 && kind=='FC3DDPC'}">
            <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/fcsddpc/fcsddpc.html','需要登录才能进行投注，是否登录？')" class="fcsddpcInfo">
                <div class="child">
                    <img src="<%=path%>/images/lottery_logo/hall/logo_3d.png" alt="" class="child-logo">
                    <div class="child-content">
                        <h2 class="child-title">3D</h2>
                        <p class="child-time">加载中...</p>
                    </div>
                </div>
            </a>
            </c:if>
           <c:if test="${lotterySetMsgHot.isSFSSCOpen==1 && kind=='SFSSC'}">
            <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/sfssc/sfssc.html','需要登录才能进行投注，是否登录？')" class="sfsscInfo">
                <div class="child">
                    <img src="<%=path%>/images/lottery_logo/hall/logo_ssc.png" alt="" class="child-logo">
                    <div class="child-content">
                        <h2 class="child-title">三分时时彩</h2>
                        <p class="child-time">加载中...</p>
                    </div>
                </div>
            </a>
            </c:if>
           <c:if test="${lotterySetMsgHot.isWFSSCOpen==1 && kind=='WFSSC'}">
            <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/wfssc/wfssc.html','需要登录才能进行投注，是否登录？')" class="wfsscInfo">
                <div class="child">
                    <img src="<%=path%>/images/lottery_logo/hall/logo_ssc.png" alt="" class="child-logo">
                    <div class="child-content">
                        <h2 class="child-title">五分时时彩</h2>
                        <p class="child-time">加载中...</p>
                    </div>
                </div>
            </a>
            </c:if>
           <c:if test="${lotterySetMsgHot.isSFKSOpen==1 && kind=='SFKS'}">
            <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/sfks/sfks.html','需要登录才能进行投注，是否登录？')" class="sfksInfo">
                <div class="child">
                    <img src="<%=path%>/images/lottery_logo/hall/logo_kuai3.png" alt="" class="child-logo">
                    <div class="child-content">
                        <h2 class="child-title">三分快三</h2>
                        <p class="child-time">加载中...</p>
                    </div>
                </div>
            </a>
            </c:if>
           <c:if test="${lotterySetMsgHot.isWFKSOpen==1 && kind=='WFKS'}">
            <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/wfks/wfks.html','需要登录才能进行投注，是否登录？')" class="wfksInfo">
                <div class="child">
                    <img src="<%=path%>/images/lottery_logo/hall/logo_kuai3.png" alt="" class="child-logo">
                    <div class="child-content">
                        <h2 class="child-title">五分快三</h2>
                        <p class="child-time">加载中...</p>
                    </div>
                </div>
            </a>
            </c:if>
            </c:forEach>
            <a href="javascript:judgeCurrentUserToLogin('hall.html','需要登录才能进行投注，是否登录？')">
                <div class="child child-more">
                    <img src="<%=path%>/images/lottery_logo/hall/logo_more.png" alt="" class="child-logo">
                    <div class="child-content">
                        <h2 class="child-title">更多彩种</h2>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <!-- classify over -->
    <!--站点统计  -->
    <%-- <div style="display: none;">
    <%=bizSystemInfo==null?"":bizSystemInfo.getSiteScript()!=null?bizSystemInfo.getSiteScript():"" %>
    </div> --%>
	<div style="display: none;">
		<%=bizSystemInfo==null?"":bizSystemInfo.getMobileSiteScript()!=null?bizSystemInfo.getMobileSiteScript():"" %>
	</div>
    <!-- nav-bar -->
    <jsp:include page="/front/include/navbar.jsp"></jsp:include>
    <!-- nav-bar over -->
    
    <!-- 收藏提示 -->
	<%-- <div class="add-to-home">
		<img class="app-icon" src="<%=path%>/images/index/logo.png">
		<div class="app-instruction">
			<p>
				先点击<span class="more-arrow-dark"></span>
			</p>
			<p>再“添加到主屏幕”</p>
		</div>
		<button class="close-btn">Close</button>
	</div> --%>
	<!-- 收藏提示end -->
	
	<!-- gift -->
	<div class="pop-btn" style="display: none"></div>
    <div class="pop" style="display: none">
        <div class="pop-cont">
            <div class="desc">
                <div class="title" id="newUserGiftTaskAlert"></div>
                <div class="text">新用户注册登录后完成相应任务即可领取礼包.</div>
            </div>
            <div class="btn-group">
                <a href="javascript:void(0);" class="receive-btn">前往领取</a>
                <a href="javascript:void(0);" class="close-btn"></a>
            </div>
        </div>
    </div>
    <!-- gift end -->
	
    <script src="<%=path%>/js/dydong.change.js"></script>
    <script src="<%=path%>/js/index/index.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
    <script type="text/javascript">
   indexPage.param.bizSystemConfigVO=<%=strJson%>;
   </script>
   <script src="<%=path%>/js/hall/hall.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
</body>

</html>