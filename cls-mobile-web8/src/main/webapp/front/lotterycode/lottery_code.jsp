<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
     <meta charset="utf-8">
     <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
     <meta name="format-detection" content="telephone=no">
<title>开奖号码列表</title>
     <link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
     <link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
     <link href="<%=path%>/css/award.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
</head>
<body>

<!-- header -->
<div class="header color-dark">
    <div class="header-title" id="header-title">
        <div class="header-title-content">
            <div class="header-title-child header-title-child1" onclick="window.location.href='<%=path%>/gameBet/wininfo.html'"">中奖信息</div>
            <div class="header-title-child header-title-child2 on" onclick="window.location.href='<%=path%>/gameBet/lotterycode.html'">开奖号码</div>
        </div>
    </div>
</div>
<!-- header over -->
<div class="stance"></div>

<div id="lotteryList">

</div>
<%-- 
<!-- main -->
<div class="main award-pk10">
    <div class="container">
        <div class="head">
            <div class="child title"><img src="<%=path%>/images/lottery_logo/hall/logo_pk10.png" alt="">PK10</div>
            <div class="child periods">第 2016090437期</div>
            <a href="lottery_pk10.html">
                <div class="child bet">立即投注 ></div>
            </a>
        </div>
        <a href="award_pk10.html">
            <div class="muns">
                <div class="mun color-red">1</div>
                <div class="mun color-red">2</div>
                <div class="mun color-red">3</div>
                <div class="mun color-red">4</div>
                <div class="mun color-red">5</div>
                <div class="mun color-red">6</div>
                <div class="mun color-red">7</div>
                <div class="mun color-red">8</div>
                <div class="mun color-red">9</div>
                <div class="mun color-red">10</div>
            </div>
            <!-- lottery-dynamic -->
            <div class="lottery-dynamic">
                <div class="titles">
                    <div class="child child1">
                        <p>期号</p>
                    </div>
                    <div class="child child2">
                        <p>开奖号</p>
                    </div>
                </div>
                <div class="content">
                    <div class="line">
                        <div class="child child1">
                            <p>20161004-1213</p>
                        </div>
                        <div class="child child2 font-red muns muns-small">
                            <div class="mun color-red">2</div>
                            <div class="mun color-red">4</div>
                            <div class="mun color-red">6</div>
                            <div class="mun color-red">0</div>
                            <div class="mun color-red">9</div>
                            <div class="mun color-red">2</div>
                            <div class="mun color-red">4</div>
                            <div class="mun color-red">6</div>
                            <div class="mun color-red">0</div>
                            <div class="mun color-red">9</div>
                        </div>
                    </div>
                    <div class="line">
                        <div class="child child1">
                            <p>20161004-1213</p>
                        </div>
                        <div class="child child2 font-blue muns muns-small">
                            <div class="mun color-red">2</div>
                            <div class="mun color-red">4</div>
                            <div class="mun color-red">6</div>
                            <div class="mun color-red">0</div>
                            <div class="mun color-red">9</div>
                            <div class="mun color-red">2</div>
                            <div class="mun color-red">4</div>
                            <div class="mun color-red">6</div>
                            <div class="mun color-red">0</div>
                            <div class="mun color-red">9</div>
                        </div>
                    </div>
                    <div class="line">
                        <div class="child child1">
                            <p>20161004-1213</p>
                        </div>
                        <div class="child child2 font-blue muns muns-small">
                            <div class="mun color-red">2</div>
                            <div class="mun color-red">4</div>
                            <div class="mun color-red">6</div>
                            <div class="mun color-red">0</div>
                            <div class="mun color-red">9</div>
                            <div class="mun color-red">2</div>
                            <div class="mun color-red">4</div>
                            <div class="mun color-red">6</div>
                            <div class="mun color-red">0</div>
                            <div class="mun color-red">9</div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>
<!-- main over -->

<!-- main -->
<div class="main award-kuaisan">
    <div class="container">
        <div class="head">
            <div class="child title"><img src="<%=path%>/images/lottery_logo/hall/logo_kuai3.png" alt="">江苏快三</div>
            <div class="child periods">第 2016090437期</div>
            <a href="lottery_kuaisan.html">
                <div class="child bet">立即投注 ></div>
            </a>
        </div>
        <a href="award_kuai3.html">
            <div class="muns">
                <img class="dice" src="<%=path%>/images/dice_pic/gray/1.png" alt="">
                <img class="dice" src="<%=path%>/images/dice_pic/gray/3.png" alt="">
                <img class="dice" src="<%=path%>/images/dice_pic/gray/6.png" alt="">
            </div>
            <!-- lottery-dynamic -->
            <div class="lottery-dynamic">
                <div class="titles">
                    <div class="child child1">
                        <p>期号</p>
                    </div>
                    <div class="child child2">
                        <p>开奖号</p>
                    </div>
                    <div class="child child3">
                        <p>和值</p>
                    </div>
                    <div class="child child4">
                        <p>形态</p>
                    </div>
                </div>
                <div class="content">
                    <div class="line">
                        <div class="child child1">
                            <p>20161016082</p>
                        </div>
                        <div class="child child2 font-blue">
                            <img class="dice" src="<%=path%>/images/dice_pic/gray/1.png" alt="">
                            <img class="dice" src="<%=path%>/images/dice_pic/gray/3.png" alt="">
                            <img class="dice" src="<%=path%>/images/dice_pic/gray/6.png" alt="">
                        </div>
                        <div class="child child3">
                            <p>12</p>
                        </div>
                        <div class="child child4">
                            <span class="child-big">大</span> | <span class="child-odd">单</span></div>
                    </div>
                    <div class="line">
                        <div class="child child1">
                            <p>20161016082</p>
                        </div>
                        <div class="child child2 font-blue">
                            <img class="dice" src="<%=path%>/images/dice_pic/gray/2.png" alt="">
                            <img class="dice" src="<%=path%>/images/dice_pic/gray/4.png" alt="">
                            <img class="dice" src="<%=path%>/images/dice_pic/gray/5.png" alt="">
                        </div>
                        <div class="child child3">
                            <p>5</p>
                        </div>
                        <div class="child child4">
                            <span class="child-small">小</span> | <span class="child-even">双</span></div>
                    </div>
                </div>
            </div>
            <!-- lottery-dynamic over -->
        </a>
    </div>
</div>
<!-- main over -->
<!-- main -->
<div class="main award-ssc">
    <div class="container">
        <div class="head">
            <div class="child title"><img src="<%=path%>/images/lottery_logo/hall/logo_ssc.png" alt="">重庆时时彩</div>
            <div class="child periods">第 2016090437期</div>
            <a href="lottery_ssc.html">
                <div class="child bet">立即投注 ></div>
            </a>
        </div>
        <a href="award_detail.html">
            <div class="muns">
                <div class="mun color-red">2</div>
                <div class="mun color-red">4</div>
                <div class="mun color-red">6</div>
                <div class="mun color-red">0</div>
                <div class="mun color-red">9</div>
            </div>
            <!-- lottery-dynamic -->
            <div class="lottery-dynamic">
                <div class="titles">
                    <div class="child child1">
                        <p>期号</p>
                    </div>
                    <div class="child child2">
                        <p>开奖号</p>
                    </div>
                    <div class="child child3">
                        <p>后三组态</p>
                    </div>
                </div>
                <div class="content">
                    <div class="line">
                        <div class="child child1">
                            <p>20161004-1213</p>
                        </div>
                        <div class="child child2 font-blue muns muns-small">
                            <div class="mun color-red">2</div>
                            <div class="mun color-red">4</div>
                            <div class="mun color-red">6</div>
                            <div class="mun color-red">0</div>
                            <div class="mun color-red">9</div>
                        </div>
                        <div class="child child3">
                            <p>组六</p>
                        </div>
                    </div>
                    <div class="line">
                        <div class="child child1">
                            <p>20161004-1213</p>
                        </div>
                        <div class="child child2 font-blue muns muns-small">
                            <div class="mun color-red">2</div>
                            <div class="mun color-red">4</div>
                            <div class="mun color-red">6</div>
                            <div class="mun color-red">0</div>
                            <div class="mun color-red">9</div>
                        </div>
                        <div class="child child3">
                            <p>组六</p>
                        </div>
                    </div>
                    <div class="line">
                        <div class="child child1">
                            <p>20161004-1213</p>
                        </div>
                        <div class="child child2 font-blue muns muns-small">
                            <div class="mun color-red">2</div>
                            <div class="mun color-red">4</div>
                            <div class="mun color-red">6</div>
                            <div class="mun color-red">0</div>
                            <div class="mun color-red">9</div>
                        </div>
                        <div class="child child3">
                            <p>组六</p>
                        </div>
                    </div>

                </div>
            </div>
            <!-- lottery-dynamic over -->
            <!-- <div class="list">
	<span class="title">第 2016-09-04-37期</span>
	<span class="mun font-blue">1</span>
	<span class="mun font-blue">2</span>
	<span class="mun font-blue">3</span>
	<span class="mun font-blue">4</span>
	<span class="mun font-blue">5</span>
</div>
<div class="list">
	<span class="title">第 2016-09-04-37期</span>
	<span class="mun font-blue">1</span>
	<span class="mun font-blue">2</span>
	<span class="mun font-blue">3</span>
	<span class="mun font-blue">4</span>
	<span class="mun font-blue">5</span>
</div> -->
        </a>
    </div>
</div>
<!-- main over -->


<!-- main -->
<div class="main award-pk10">
    <div class="container">
        <div class="head">
            <div class="child title"><img src="<%=path%>/images/lottery_logo/hall/logo_liuhecai.png" alt="">六合彩</div>
            <div class="child periods">第 2016090437期</div>
            <a href="lottery_liuhecai.html">
                <div class="child bet">立即投注 ></div>
            </a>
        </div>
        <a href="award_pk10.html">
            <div class="muns">
                <div class="mun color-red">1</div>
                <div class="mun color-red">2</div>
                <div class="mun color-red">3</div>
                <div class="mun color-blue">4</div>
                <div class="mun color-green">5</div>
                <div class="mun color-yellow">6</div>
                <div class="mun color-red">7</div>

            </div>
            <!-- lottery-dynamic -->
            <div class="lottery-dynamic">
                <div class="titles">
                    <div class="child child1">
                        <p>期号</p>
                    </div>
                    <div class="child child2">
                        <p>开奖号</p>
                    </div>
                </div>
                <div class="content">
                    <div class="line">
                        <div class="child child1">
                            <p>20161004-1213</p>
                        </div>
                        <div class="child child2 font-red muns muns-small">
                            <div class="mun color-red">2</div>
                            <div class="mun color-red">4</div>
                            <div class="mun color-yellow">6</div>
                            <div class="mun color-red">0</div>
                            <div class="mun color-blue">9</div>
                            <div class="mun color-green">2</div>
                            <div class="mun color-red">4</div>
                        </div>
                    </div>
                    <div class="line">
                        <div class="child child1">
                            <p>20161004-1213</p>
                        </div>
                        <div class="child child2 font-blue muns muns-small">
                            <div class="mun color-red">2</div>
                            <div class="mun color-red">4</div>
                            <div class="mun color-red">6</div>
                            <div class="mun color-red">0</div>
                            <div class="mun color-red">9</div>
                            <div class="mun color-red">2</div>
                            <div class="mun color-red">4</div>
                        </div>
                    </div>
                    <div class="line">
                        <div class="child child1">
                            <p>20161004-1213</p>
                        </div>
                        <div class="child child2 font-blue muns muns-small">
                            <div class="mun color-red">2</div>
                            <div class="mun color-red">4</div>
                            <div class="mun color-red">6</div>
                            <div class="mun color-red">0</div>
                            <div class="mun color-red">9</div>
                            <div class="mun color-red">2</div>
                            <div class="mun color-red">4</div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>
<!-- main over --> --%>

<!-- nav-bar -->
<jsp:include page="/front/include/navbar.jsp"></jsp:include>
<!-- nav-bar over -->


<jsp:include page="/front/include/include.jsp"></jsp:include>
<script src="<%=path%>/js/lotterycode/lottery_code.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
<script>
    animate_add(".main", "fadeInUp");
</script>
</body>
</html>