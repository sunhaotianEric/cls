<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
	String lotteryKind = request.getParameter("lotteryKindName");
%>
<!DOCTYPE html>
<html>
<head>
     <meta charset="utf-8">
     <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
     <meta name="format-detection" content="telephone=no">
<title>开奖详情</title>
     <link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
     <link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
     <link href="<%=path%>/css/award_base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
     <link href="<%=path%>/css/award_detail.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">

</head>

<body>

 <!-- header -->
 <div class="header color-dark">
     <div class="header-left" onclick="window.location.href='<%=path%>/gameBet/lotterycode.html'"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
     <h2 class="header-title"  id="lotterKindId">重庆</h2>
 </div>
 <!-- header over -->

 <div class="stance"></div>


 <!-- head -->
 <div class="main head">
     <div class="container">
         <div class="logo"><img src="<%=path%>/images/lottery_logo/hall/logo_ssc.png"  id="logoId" alt=""></div>
         <div class="right">
             <div class="periods">
                 <span class="title"  id="lotteryExpectId">第 2016-09-04-37期</span>
                 <!-- <span class="time">2016-09-10</span> -->
             </div>
             <div class="muns muns-big lhc-shengxiao" id="openCodeId">
                 <div class='lhc-item'><div class="mun color-red" id="lotteryNumber1">0</div><p class='lhc-shengxiao'>-</p></div>
                 <div class='lhc-item'><div class="mun color-red" id="lotteryNumber2">0</div><p class='lhc-shengxiao'>-</p></div>
                 <div class='lhc-item'><div class="mun color-red" id="lotteryNumber3">0</div><p class='lhc-shengxiao'>-</p></div>
                 <div class='lhc-item'><div class="mun color-red" id="lotteryNumber4">0</div><p class='lhc-shengxiao'>-</p></div>
                 <div class='lhc-item'><div class="mun color-red" id="lotteryNumber5">0</div><p class='lhc-shengxiao'>-</p></div>
                 <div class='lhc-item'><div class="mun color-red" id="lotteryNumber6">0</div><p class='lhc-shengxiao'>-</p></div>
                 <div class='lhc-item'><div class="mun and">+</div><p class='lhc-shengxiao'>&nbsp;</p></div>
                 <div class='lhc-item'><div class="mun color-red" id="lotteryNumber7">0</div><p class='lhc-shengxiao'>-</p></div>
                 <div class="mun color-red" id="lotteryNumber8">0</div>
                 <div class="mun color-red" id="lotteryNumber9">0</div>
                 <div class="mun color-red" id="lotteryNumber10">0</div>
             </div>
         </div>
     </div>
 </div>
 <!-- head over -->
<div  id="nearestTenLotteryCode">
	
</div>
 <!-- lottery-dynamic -->
<!--  <div class="main award-ssc">
     <div class="container lottery-dynamic">
         <div class="titles">
             <div class="child child1">
                 <p>期号</p>
             </div>
             <div class="child child2">
                 <p>开奖号</p>
             </div>
             <div class="child child3">
                 <p>后三组态</p>
             </div>
         </div>
         <div class="content">
             <div class="line">
                 <div class="child child1">
                     <p>20161004-1213</p>
                 </div>
                 <div class="child child2 font-blue muns muns-small">
                     <div class="mun color-red">2</div>
                     <div class="mun color-red">4</div>
                     <div class="mun color-red">6</div>
                     <div class="mun color-red">0</div>
                     <div class="mun color-red">9</div>
                 </div>
                 <div class="child child3">
                     <p>组六</p>
                 </div>
             </div>
             <div class="line">
                 <div class="child child1">
                     <p>20161004-1213</p>
                 </div>
                 <div class="child child2 font-blue muns muns-small">
                     <div class="mun color-red">2</div>
                     <div class="mun color-red">4</div>
                     <div class="mun color-red">6</div>
                     <div class="mun color-red">0</div>
                     <div class="mun color-red">9</div>
                 </div>
                 <div class="child child3">
                     <p>组六</p>
                 </div>
             </div>
             <div class="line">
                 <div class="child child1">
                     <p>20161004-1213</p>
                 </div>
                 <div class="child child2 font-blue muns muns-small">
                     <div class="mun color-red">2</div>
                     <div class="mun color-red">4</div>
                     <div class="mun color-red">6</div>
                     <div class="mun color-red">0</div>
                     <div class="mun color-red">9</div>
                 </div>
                 <div class="child child3">
                     <p>组六</p>
                 </div>
             </div>

         </div>
     </div>
 </div>
 lottery-dynamic over
 -->
<br />
<br />
<br />
<!-- <div class="user-btn return-btn color-red" onclick="window.history.go(-1)" >返回</div> -->
<div class="user-btn load-btn color-gray" style="display:none;">加载中...</div>
<!-- nav-bar -->
<jsp:include page="/front/include/navbar.jsp"></jsp:include>
<!-- nav-bar over -->

<jsp:include page="/front/include/include.jsp"></jsp:include>
<script src="<%=path%>/js/lotterycode/lottery_code_detail.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
<script type="text/javascript">
		lotteryCodeDetailPage.param.lotteryKind = "<%=lotteryKind %>";
</script>
</body>
</html>