<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="format-detection" content="telephone=no">
	<title>代理报表</title>
	<link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
	<link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
	<link href="<%=path%>/css/consume_report.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
	<style type="text/css">
		.main-child{background:#ffffff;}
	</style>
	<jsp:include page="/front/include/include.jsp"></jsp:include>
</head>
<body>
	<!-- header -->
	<div class="header color-dark">
		<div class="header-left" onclick="window.history.go(-1);"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
		<h2 class="header-title">代理报表</h2>
		<div class="header-right" onclick="show_classify('.lottery-classify1',this)"><span>今天</span><img class="icon" src="<%=path%>/images/icon/icon-pointer-white.png"></div>
	</div>
	<div class="lottery-classify lottery-classify1 animated">
		<div class="container">
			<div class="move-content">
				<div class="child color-red">
					<p data-value="today">今天</p>
				</div>
				<div class="child color-red">
					<p data-value="yesterDay">昨天</p>
				</div>
				<div class="child color-red">
					<p data-value="month">本月</p>
				</div>
				<div class="child color-red">
					<p data-value="lastmonth">上个月</p>
				</div>
			</div>
		</div>
	</div>
	<!-- header over -->
	<div class="alert-bg" onclick="element_toggle(false,['.lottery-classify','.alert'])"></div>


	<div class="stance"></div>

	<li class="user-line main">
		<div class="container">
			<div class="line-title">
				<p>用户名</p>
			</div>
			<div class="line-content">
				<input class="inputText" type="text" placeholder="请输入用户名" id="userName" name="userName">

			</div>
			<div class="right color-red btn-middle" onclick="agentReportPage.findUserConsumeReportByQueryParam(agentReportPage.param.dateRange)">确定</div>
		</div>
	</li>
	<!-- main -->
	<div class="main detail-modal out consume_report_detail" id="reportList" >
		<div class="block">
			<div class="main-child">
				<div class="child-value"><span id="recharges">0.00</span></div>
				<div class="child-title"><span>充值金额</span></div>
			</div>
			<div class="main-child">
				<div class="child-value"><span id="withDraw">0.00</span></div>
				<div class="child-title"><span>提现金额</span></div>
			</div>
			<div class="main-child">
				<div class="child-value" ><span id="lottery">0.00</span></div>
				<div class="child-title"><span>投注金额</span></div>
			</div>
		</div>
		<div class="block">
			<div class="main-child">
				<div class="child-value" ><span id="win">0.00</span></div>
				<div class="child-title"><span>中奖金额</span></div>
			</div>
			<div class="main-child">
				<div class="child-value"><span id="rebatePercentage">0.00</span></div>
				<div class="child-title"><span>团队返点</span></div>
			</div>
			<div class="main-child">
				<div class="child-value"><span id="activimoney">0.00</span></div>
				<div class="child-title"><span>活动礼金</span></div>
			</div>
			
		</div>
		<div class="block">
		    <div class="main-child">
				<div class="child-value"><span id="regCount">0.00</span></div>
				<div class="child-title"><span>注册人数</span></div>
			</div>
			<div class="main-child">
				<div class="child-value"><span id="lotteryCount">0.00</span></div>
				<div class="child-title"><span>投注人数</span></div>
			</div>
			<div class="main-child">
				<div class="child-value"><span id="firstRechargeCount">0.00</span></div>
				<div class="child-title"><span>首充人数</span></div>
			</div>
		</div>
			<div class="main-child">
				<div class="child-value"><span id="gain">0.00</span></div>
				<div class="child-title"><span>团队盈利</span></div>
			</div>
			
	</div>
	<!-- main over -->
	<!-- nav-bar -->
	<jsp:include page="/front/include/navbar.jsp"></jsp:include>
	<!-- nav-bar over -->
    <script type="text/javascript" src="<%=path%>/js/report/agent_report.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
</body>
</html>