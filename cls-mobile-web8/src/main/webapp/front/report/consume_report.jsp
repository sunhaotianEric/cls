<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
    <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="format-detection" content="telephone=no">
	<title>盈亏报表</title>
	<link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
	<link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
	<link href="<%=path%>/css/consume_report.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
</head>

<body>




	<!-- header -->
	<div class="header color-dark">
		<div class="header-left" onclick="window.history.go(-1);"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
		<h2 class="header-title">盈亏报表</h2>
		<div class="header-right" onclick="show_classify('.lottery-classify1',this)"><span>昨天</span><img class="icon" src="<%=path%>/images/icon/icon-pointer-white.png"></div>
	</div>
	<div class="lottery-classify lottery-classify1 animated">
		<div class="container">
			<div class="move-content">
				<div class="child color-red">
					<p data-value="today">今天</p>
				</div>
				<div class="child color-red">
					<p data-value="yesterDay">昨天</p>
				</div>
				<div class="child color-red">
					<p data-value="month">本月</p>
				</div>
				<div class="child color-red">
					<p data-value="lastmonth">上个月</p>
				</div>
			</div>
		</div>
	</div>
	<!-- header over -->
	<div class="alert-bg" onclick="element_toggle(false,['.lottery-classify','.alert'])"></div>


	<div class="stance"></div>

	<li class="user-line main">
		<div class="container">
			<div class="line-title">
				<p>用户名</p>
			</div>
			<div class="line-content">
				<input class="inputText" type="text" placeholder="请输入用户名" id="userName" name="userName">

			</div>
			<div class="right color-red btn-middle" onclick="consumeReportPage.getConsumeReports(consumeReportPage.queryParam)">确定</div>
		</div>
	</li>
	<!-- main -->
	<div class="main" id="reportList" > 

		<!-- <div class="titles">

			<div class="child child1">
				<p>用户名</p>
			</div>
			<div class="child child2">
				<p>投注</p>
			</div>
			<div class="child child3">
				<p>中奖</p>
			</div>
			<div class="child child4">
				<p>盈利</p>
			</div>
			<div class="child child5">
				<p>明细</p>
			</div>

		</div> -->
		<!-- <div class="line">
			<a href="#">
				<div class="child child1 font-red">
					<p>avc1</p>
				</div>
			</a>
			<div class="child child2">
				<p>24.00</p>
			</div>
			<div class="child child3">
				<p>240.00</p>
			</div>
			<div class="child child4">
				<p>300.00</p>
			</div>
			<div class="child child5 font-red" onclick="modal_toggle('.detail-modal')">
				<p>查看明细</p>
			</div>
		</div> -->
		<!-- <div class="line">
			<a href="#">
				<div class="child child1 font-red">
					<p>avc1</p>
				</div>
			</a>
			<div class="child child2">
				<p>24.00</p>
			</div>
			<div class="child child3">
				<p>240.00</p>
			</div>
			<div class="child child4">
				<p>300.00</p>
			</div>
			<div class="child child5 font-red" onclick="modal_toggle('.detail-modal')">
				<p>查看明细</p>
			</div>
		</div> -->
		<!-- <div class="line">
			<a href="#">
				<div class="child child1 font-red">
					<p>avc1</p>
				</div>
			</a>
			<div class="child child2">
				<p>24.00</p>
			</div>
			<div class="child child3">
				<p>240.00</p>
			</div>
			<div class="child child4">
				<p>300.00</p>
			</div>
			<div class="child child5 font-red" onclick="modal_toggle('.detail-modal')">
				<p>查看明细</p>
			</div>
		</div> -->
		<!-- <div class="line gray">
			<div class="child child1">
				<p>总统计</p>
			</div>
			<div class="child child2 font-red">
				<p>24.00</p>
			</div>
			<div class="child child3 font-red">
				<p>240.00</p>
			</div>
			<div class="child child4 font-red">
				<p>300.00</p>
			</div>
			<div class="child child5">
				<p>--</p>
			</div>
		</div> -->



	</div>
	<!-- main over -->
	<div class="user-btn color-red" id="showMore">显示更多</div>


	<!-- modal detail-modal -->
	<div class="modal detail-modal animated out consume_report_detail">
		<!-- header -->
		<div class="header color-dark rechargewithdraw_fund">
			<div class="header-left" onclick="modal_toggle('.detail-modal')"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
			<h2 class="header-title">查看明细</h2>
		</div>
		<!-- header over -->
		<div class="stance"></div>
		<!-- main -->
		<div class="main">
			<div class="block">
				<div class="main-child">
					<div class="child-value" ><span id="recharge">0.00</span></div>
					<div class="child-title"><span>充值金额</span></div>
				</div>
				<div class="main-child">
					<div class="child-value" ><span id="withDraw">0.00</span></div>
					<div class="child-title"><span>提现金额</span></div>
				</div>
				<div class="main-child">
					<div class="child-value"><span id="lottery">0.00</span></div>
					<div class="child-title"><span>投注金额</span></div>
				</div>
			</div>
			<div class="block">
				<div class="main-child">
					<div class="child-value"><span id="win">0.00</span></div>
					<div class="child-title"><span>中奖金额</span></div>
				</div>
				<div class="main-child">
					<div class="child-value"><span id="percentage">0.00</span></div>
					<div class="child-title"><span>团队返点</span></div>
				</div>
			   <div class="main-child">
					<div class="child-value"><span id="activitiesMoney">0.00</span></div>
					<div class="child-title"><span>活动礼金</span></div>
				</div>
				
			</div>
			<div class="block">
				<!-- <div class="main-child">
					<div class="child-value"><span id="otherMoney">0.00</span></div>
					<div class="child-title"><span>其他</span></div>
				</div> -->
			    <div class="main-child">
					<div class="child-value"><span id="adduser">0.00</span></div>
					<div class="child-title"><span>注册人数</span></div>
				</div>
				<div class="main-child">
					<div class="child-value"><span id="lotteryCount">0.00</span></div>
					<div class="child-title"><span>投注人数</span></div>
				</div>
				<div class="main-child">
					<div class="child-value"><span id="addmoney">0.00</span></div>
					<div class="child-title"><span>首充人数</span></div>
				</div>
			</div>
				
				<div class="main-child">
					<div class="child-value"><span id="gain">0.00</span></div>
					<div class="child-title"><span>团队盈利</span></div>
				</div>
		<!-- 	<div class="main-child">
				<div class="child-value"><span>0.00</span></div>
				<div class="child-title"><span>盈利</span></div>
			</div> -->
		</div>
		<!-- main over -->
		<div class="stance-nav"></div>
	</div>
	<!-- modal bank-modal over -->





	<!-- nav-bar -->
	<jsp:include page="/front/include/navbar.jsp"></jsp:include>
	<!-- nav-bar over -->


<%-- 	<script src="<%=path%>/assets/jquery/jquery-3.1.1.min.js"></script>
	<script src="<%=path%>/js/base.js"></script> --%>
	 <jsp:include page="/front/include/include.jsp"></jsp:include>
    <script type="text/javascript" src="<%=path%>/js/report/consume_report.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
</body>

</html>