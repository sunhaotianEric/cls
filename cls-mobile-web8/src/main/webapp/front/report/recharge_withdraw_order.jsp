<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant,com.team.lottery.enums.ELotteryKind" %>
<%
	String path = request.getContextPath();
%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="format-detection" content="telephone=no">
	<title>充提</title>
	<link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
	<link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
	<link href="<%=path%>/css/user_order.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
	<link href="<%=path%>/css/recharge_withdraw_order.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
</head>
<body>
	<!-- header -->
	<div class="header color-dark">
		<div class="header-left" onclick="window.history.go(-1);"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
		<h2 class="header-title">充提记录</h2>
		<div class="header-right" onclick="show_classify('.lottery-classify1',this)"><span>今天</span><img class="icon" src="<%=path%>/images/icon/icon-pointer-white.png"></div>
	</div>
	<div class="lottery-classify lottery-classify1 animated">
		<div class="container">
			<div class="move-content">
				<div class="child color-red">
					<p data-value="today">今天</p>
				</div>
				<div class="child color-red">
					<p data-value="yesterDay">昨天</p>
				</div>
				<div class="child color-red">
					<p data-value="month">本月</p>
				</div>
				<div class="child color-red">
					<p data-value="lastmonth">上个月</p>
				</div>
			</div>
		</div>
	</div>
	<!-- header over -->
	<div class="alert-bg" onclick="element_toggle(false,['.lottery-classify','.alert'])"></div>

	<div class="stance"></div>

	<div class="user-line bet-search">
		<div class="container">
			<div class="line-content w845">
				<input class="inputText" type="text" placeholder="请输入用户名" id="userName" name="userName">
			</div>
			<div class="right color-red btn-middle h110" onclick="rechargeWithdrawOrderPage.getOrderPage(rechargeWithdrawOrderPage.queryParam,rechargeWithdrawOrderPage.pageParam.pageNo)"><i class="icon-search"></i></div>
		</div>
	</div>
	<!-- main -->
	<div class="main-container"> 
		<div class="js-tab double-link">
			<a onclick="rechargeWithdrawOrderPage.getTabProstate('RECHARGE')" class="on">充值记录</a>
			<a onclick="rechargeWithdrawOrderPage.getTabProstate('WITHDRAW')">提现记录</a>
		</div>
		<div class="js-cont main">
			<div class="block" id="RECHARGE">
				<%-- <div class="empty-tip"><img src="<%=path%>/images/icon/icon-empty.png" /><p>暂无记录</p></div> --%>
				<div class="titles">
					<div class="child child1">
						<p>用户名</p>
					</div>
					<div class="child child2">
						<p>金额</p>
					</div>
					<!-- <div class="child child3">
						<p>到账金额</p>
					</div> -->
					<div class="child child4">
						<p>充值方式</p>
					</div>
					<div class="child child5">
						<p>交易时间</p>
					</div>
					<div class="child child6">
						<p>状态</p>
					</div>
	
				</div>
				<div id="rechargeOrderList">
					<!-- <div class="line">
						<div class="child child1">
							<p>1000000</p>
						</div>
						<div class="child child2">
							<p>24.000</p>
						</div>
						<div class="child child3">
							<p>24.000</p>
						</div>
						<div class="child child4">
							<p>支付宝</p>
						</div>
						<div class="child child5">
							<p>2016/09/27 11:16:33</p>
						</div>
						<div class="child child6">
							<p>进行中</p>
						</div>
					</div> -->
				</div>
			</div>
			<div class="block" id="WITHDRAW">
				<div class="titles">
					<div class="child child7_">
						<p>用户名</p>
					</div>
					<div class="child child8_">
						<p>申请金额</p>
					</div>
					<!-- <div class="child child3"><p>到账金额</p></div> -->
					<div class="child child9_">
						<p>交易时间</p>
					</div>
					<div class="child child10_">
						<p>状态</p>
					</div>
				</div>
				<div  id="withdrawOrderList">
					<!-- <div class="line">
						<div class="child child7">
							<p>1000000</p>
						</div>
						<div class="child child8">
							<p>24.000</p>
						</div>
						<div class="child child9">
							<p>2016/09/27 11:16:33</p>
						</div>
						<div class="child child10">
							<p>进行中</p>
						</div>
					</div> -->
				</div>
			</div>
		</div>
	</div>
	<!-- main over -->
	<div class="user-btn color-gray" id="showMore" style="display:none;">显示更多</div>

	<jsp:include page="/front/include/include.jsp"></jsp:include>
    <script type="text/javascript" src="<%=path%>/js/report/recharge_withdraw_order.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
</body>
</html>