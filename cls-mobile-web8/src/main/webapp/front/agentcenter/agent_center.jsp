<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="format-detection" content="telephone=no">
    <title>代理中心</title>
    <link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/user_team_treatment.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
</head>
<body>


<!-- header -->
<div class="header color-dark">
	<div class="header-left" onclick="window.history.go(-1)"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
	<h2 class="header-title">代理中心</h2>
</div>
<!-- header over -->

<div class="stance"></div>

<!-- main -->
<div class="main">
	<ul class="list">
		<a href="<%=path%>/gameBet/agentCenter/addUser.html">
			<li>
				<div class="container">
					<img src="<%=path%>/images/icon/i9.png" alt="" class="icon">
					<span>下级开户</span>
					<div class="right"><img src="<%=path%>/images/icon/icon-pointer.png" alt="" class="icon"></div>
				</div>
			</li>
		</a>
		<a href="<%=path%>/gameBet/agentCenter/extendMgr.html">
			<li>
				<div class="container">
					<img src="<%=path%>/images/icon/i8.png" alt="" class="icon">
					<span>推广管理</span>
					<div class="right"><img src="<%=path%>/images/icon/icon-pointer.png" alt="" class="icon"></div>
				</div>
			</li>
		</a>
		<a href="<%=path%>/gameBet/agentCenter/userList.html">
			<li>
				<div class="container">
					<img src="<%=path%>/images/icon/i10.png" alt="" class="icon">
					<span>团队列表</span>
					<div class="right"><img src="<%=path%>/images/icon/icon-pointer.png" alt="" class="icon"></div>
				</div>
			</li>
		</a>
		<a href="<%=path%>/gameBet/agentCenter/order.html">
			<li>
				<div class="container">
					<img src="<%=path%>/images/icon/i13.png" alt="" class="icon">
					<span>下级投注</span>
					<div class="right"><img src="<%=path%>/images/icon/icon-pointer.png" alt="" class="icon"></div>
				</div>
			</li>
		</a>
		<a href="<%=path%>/gameBet/agentCenter/moneyDetail.html">
			<li>
				<div class="container">
					<img src="<%=path%>/images/icon/i14.png" alt="" class="icon">
					<span>下级账变</span>
					<div class="right"><img src="<%=path%>/images/icon/icon-pointer.png" alt="" class="icon"></div>
				</div>
			</li>
		</a>
		<a href="<%=path%>/gameBet/report/rechargeWithdrawOrder.html">
			<li>
				<div class="container">
					<img src="<%=path%>/images/icon/i16.png" alt="" class="icon">
					<span>充提记录</span>
					<div class="right"><img src="<%=path%>/images/icon/icon-pointer.png" alt="" class="icon"></div>
				</div>
			</li>
		</a>
	</ul>

	<ul class="list">
		<a href="<%=path%>/gameBet/report/consumeReport.html">
			<li>
				<div class="container">
					<img src="<%=path%>/images/icon/i11.png" alt="" class="icon">
					<span>盈亏报表</span>
					<div class="right"><img src="<%=path%>/images/icon/icon-pointer.png" alt="" class="icon"></div>
				</div>
			</li>
		</a>
		<a href="<%=path%>/gameBet/report/agentReport.html">
			<li>
				<div class="container">
					<img src="<%=path%>/images/icon/i15.png" alt="" class="icon">
					<span>代理报表</span>
					<div class="right"><img src="<%=path%>/images/icon/icon-pointer.png" alt="" class="icon"></div>
				</div>
			</li>
		</a>
	</ul>



</div>
<!-- main over -->
<!-- nav-bar -->
<jsp:include page="/front/include/navbar.jsp"></jsp:include>
<!-- nav-bar over -->
<jsp:include page="/front/include/include.jsp"></jsp:include>
<script type="text/javascript">
	$(".i-person").parent().addClass("on");
</script>
</body>
</html>