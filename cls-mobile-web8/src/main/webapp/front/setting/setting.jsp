<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ page import="com.team.lottery.util.ConstantUtil,com.team.lottery.vo.User" %>
<%
	String path = request.getContextPath();
	User loginUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="format-detection" content="telephone=no">
    <title>设置</title>
    <link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/setting.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
</head>
<body>

<!-- header -->
<div class="header color-dark">
    <div class="header-left" onclick="window.location.href='<%=path%>/gameBet/my.html'"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
    <h2 class="header-title">设置</h2>
</div>
<!-- header over -->




<div class="stance"></div>

<!-- main -->
<div class="main">
    <ul class="list">
        <a href="<%=path%>/gameBet/safeCenter/resetPwd.html">
            <li>
                <div class="container">
                    <img src="<%=path%>/images/icon/i1.png" alt="" class="icon">
                    <span>修改登录密码</span>
                    <div class="right"><img src="<%=path%>/images/icon/icon-pointer.png" alt="" class="icon"></div>
                </div>
            </li>
        </a>
        <a href="<%=path%>/gameBet/safeCenter/setSafePwd.html" id="safetyPassword">
            <li>
                <div class="container">
                    <img src="<%=path%>/images/icon/i2.png" alt="" class="icon">
                    <span id="safetyPasswordTitle">设置安全密码</span>
                    <div class="right"><img src="<%=path%>/images/icon/icon-pointer.png" alt="" class="icon"></div>
                </div>
            </li>
        </a>
        <a href="<%=path%>/gameBet/safeCenter/bindPhone.html" id="bindmobile">
            <li>
                <div class="container">
                    <img src="<%=path%>/images/icon/i17.png" alt="" class="icon">
                    <span id="safetyPhoneTitle">绑定安全手机</span>
                    <div class="right"><img src="<%=path%>/images/icon/icon-pointer.png" alt="" class="icon"></div>
                </div>
            </li>
        </a>
        <a href="<%=path%>/gameBet/safeCenter/bindEmail.html" id="bindemail">
            <li>
                <div class="container">
                    <img src="<%=path%>/images/icon/i18.png" alt="" class="icon">
                    <span id="safetyEmailTitle">绑定安全邮件</span>
                    <div class="right"><img src="<%=path%>/images/icon/icon-pointer.png" alt="" class="icon"></div>
                </div>
            </li>
        </a>
        <a href="<%=path%>/gameBet/user_info_save_question/user_info_save_question.html" id="safetyQuestion">
            <li>
                <div class="container">
                    <img src="<%=path%>/images/icon/i3.png" alt="" class="icon">
                    <span id="safetyQuestionTitle">安全问题</span>
                    <div class="right"><img src="<%=path%>/images/icon/icon-pointer.png" alt="" class="icon"></div>
                </div>
            </li>
        </a>
    </ul>

    <ul class="list">
        <a href="<%=path%>/gameBet/bankCard/bankCardList.html">
            <li>
                <div class="container">
                    <img src="<%=path%>/images/icon/i4.png" alt="" class="icon">
                    <span>银行卡管理</span>
                    <div class="right"><img src="<%=path%>/images/icon/icon-pointer.png" alt="" class="icon"></div>
                </div>
            </li>
        </a>
    </ul>
    <ul class="list">
        <a href="<%=path%>/gameBet/awardDetail.html">
            <li>
                <div class="container">
                    <img src="<%=path%>/images/icon/i5.png" alt="" class="icon">
                    <span>奖金详情</span>
                    <div class="right"><img src="<%=path%>/images/icon/icon-pointer.png" alt="" class="icon"></div>
                </div>
            </li>
        </a>
    </ul>
    
    <%
    	if("cs001".equals(loginUser.getUserName()) || "cs002".equals(loginUser.getUserName())) {
    %>
     <ul class="list">
        <a href="<%=path%>/monitor.html">
            <li>
                <div class="container">
                    <img src="<%=path%>/images/icon/i5.png" alt="" class="icon">
                    <span>监控查看</span>
                    <div class="right"><img src="<%=path%>/images/icon/icon-pointer.png" alt="" class="icon"></div>
                </div>
            </li>
        </a>
    </ul>
    <%
    	}
    %>

    <ul class="list">
        <li>
            <div class="container">
                <img src="<%=path%>/images/icon/i6.png" alt="" class="icon">
                <span>元角模式</span>
                <div class="right toggle toggle-select " data-onclass="yellow" id="modelSetToggle">
					<!-- 设置的值调换 -->
					<input type="hidden" class="toggle-value" id="lotteryModelSet" value="JIAO">
					<div class="value" value="JIAO">元</div>
					<div class="value" value="YUAN">角</div>
					<div class="ball">元</div>
				</div>
            </div>
        </li>
        <li>
            <div class="container">
                <img src="<%=path%>/images/icon/i7.png" alt="" class="icon">
                <span>通知设置</span>
                <div class="right toggle" data-onclass="yellow" id="noticeToggle">
                    <input type="hidden" class="toggle-value" value="0">
                    <div class="value" value="0"></div>
                    <div class="value" value="1"></div>
                    <div class="ball"></div>
                </div>
            </div>
        </li>
    </ul>

    <ul class="list">
        <li>
            <div class="container" id="versionUpdateId">
                <img src="<%=path%>/images/icon/i8.png" alt="" class="icon">
                <span>版本更新</span>
                <div class="right"><img src="<%=path%>/images/icon/icon-pointer.png" alt="" class="icon"></div>
            </div>
        </li>
        <li onclick='comfirm("清除缓存","<p style=\"font-size:42px;text-align:center;\">确定是否需要清除缓存?</p>","否",function(){showTip("缓存并没清除！")},"是",function(){location.reload();})'>
            <div class="container">
                <img src="<%=path%>/images/icon/i12.png" alt="" class="icon">
                <span>清除缓存</span>
                <div class="right"><img src="<%=path%>/images/icon/icon-pointer.png" alt="" class="icon"></div>
            </div>
        </li>
    </ul>

    <!-- <ul class="list">
       <li class="logout">退出登录</li>
    </ul> -->

</div>
<!-- main over -->
<!-- nav-bar -->
<jsp:include page="/front/include/navbar.jsp"></jsp:include>
<!-- nav-bar over -->

<jsp:include page="/front/include/include.jsp"></jsp:include>
<script src="<%=path%>/js/setting/setting.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
</body>
</html>