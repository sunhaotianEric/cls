<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.BizSystemConfigVO,
com.team.lottery.system.BizSystemConfigManager,
com.team.lottery.cache.ClsCacheManager,
com.team.lottery.extvo.SystemSetMsg,
com.team.lottery.cache.ClsCacheManager,com.team.lottery.vo.BizSystemInfo,
com.team.lottery.util.ConstantUtil,
com.team.lottery.system.SystemConfigConstant
" %>    
<%
  String configContent = "";
  String customUrl = "";
  String headLogoUrl = "";
  String path = request.getContextPath();
  //系统总开关
  String maintainSwich = SystemConfigConstant.maintainSwich;
if(maintainSwich != null && "1".equals(maintainSwich)){
	
}else{
	//获取当前域名
	String serverName=  request.getServerName();
	String bizSystem=ClsCacheManager.getBizSystemByDomainUrl(serverName);
	//systemConfig
	BizSystemInfo bizSystemInfo=ClsCacheManager.getBizSystemInfo(bizSystem);
	BizSystemConfigVO bizSystemConfig=BizSystemConfigManager.getBizSystemConfig(bizSystem);
	 configContent = bizSystemConfig.getMaintainContent();
	 customUrl = bizSystemInfo.getCustomUrl();
	 headLogoUrl = bizSystemInfo.getHeadLogoUrl();
}


%>     
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>系统维护</title>
<script type="text/javascript" src="<%=path%>/assets/jquery/jquery-1.11.3.js"></script>

<link rel="stylesheet" type="text/css" href="<%=path%>/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script src="<%=path%>/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script src="<%=path%>/js/maintain/maintain.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>

<!-- common js -->
<script type="text/javascript" src="<%=path%>/js/common.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<!-- css -->
<link rel="stylesheet" type="text/css" href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/css/maintain.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<style type="text/css">
.main .logo{
 	max-height: 74%;
    max-width: 200px;
    vertical-align: middle;
}
</style>

</head>
<body class="bg-content">

<%-- <div class="main-bg" style="background-image:url(<%=path%>/images/site/bg1.jpg);"></div> --%>

<div class="main">
<div class="container">
     <%  if(headLogoUrl!=null && !"".equals(headLogoUrl)) {%>
	<img class="logo" src="<%=headLogoUrl%>" />
	<%} %>
    <div class="main-container">
    	<div class="left">
        	<div class="head"><p>系统维护中...</p></div>
            <div class="content">
            	<h2><%= configContent %></h2>
                <h3>维护期间，我们郑重承诺若账户的信息不会丢失，感谢您的支持。</h3>
                <h4>如果您还有疑问，可以联系客服解决：<a href="javascript:void(0)" data-val="<%=customUrl%>" onclick="frontCommonPage.showCustomService(this)">联系客服</a></h4>
            </div>
        </div>
        
    </div>
</div>
</div>
<script type="text/javascript">

	// 获取当前页面路径用于路径跳转;
	var address = window.location.href;
	// 修改跳转的路径为index.html;
	var locationAddress = address.replace("maintain", "index");
	
	function toIndexPage() {
		// locationAddress为跳转的新路径;
		window.location.href = locationAddress;
	}
	setTimeout("toIndexPage()", 20000);
</script>
</body>
</html>