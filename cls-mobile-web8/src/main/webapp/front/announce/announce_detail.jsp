<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
	String	id = request.getParameter("id");
%>
<!DOCTYPE html>
<html>
<head>
     <meta charset="utf-8">
     <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
     <meta name="format-detection" content="telephone=no">
<title>公告详情</title>
     <link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
     <link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
     <link href="<%=path%>/css/message_detail.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
</head>
<body>

<!-- header -->
<div class="header color-dark">
    <div class="header-left" onclick="window.history.go(-1);"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
    <h2 class="header-title">公告详情</h2>
    <div class="header-right"><%-- <img class="icon" src="<%=path%>/images/icon/icon-del.png"> --%></div>
</div>
<!-- header over -->




<div class="stance"></div>

<!-- main -->
<div class="main">
    <ul>
        <li class="main-child">
            <div class="container">
                <div class="child-content">
                    <h2 class="title" id="title">***</h2>
                    <div class="info">
                        <p id="time">---</p>
                    </div>
                </div>
                <img class="icon pointer" src="<%=path%>/images/icon/icon-pointer.png">
            </div>
            <div class="content">
                <div class="container" id="content">
                    <!-- <p>亲爱的用户：</p>
                    <p>你好，支付宝官方系统维护完毕，</p>
                    <p>目前已经恢复充值功能，给你造成不必要请见谅</p>
                    <p>你好，支付宝官方系统维护完毕，</p>
                    <p>给你造成不必要请见谅</p> -->
                </div>
            </div>
        </li>
    </ul>
</div>
<!-- main over -->

<!-- nav-bar -->
<jsp:include page="/front/include/navbar.jsp"></jsp:include>
<!-- nav-bar over -->
<jsp:include page="/front/include/include.jsp"></jsp:include>

<script src="<%=path%>/js/announce/announce_detail.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
<script type="text/javascript">
	announceDetailPage.param.id = "<%=id%>";
</script>
</body>
</html>