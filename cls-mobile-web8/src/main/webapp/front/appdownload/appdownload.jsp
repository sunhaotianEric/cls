<%@page import="java.util.regex.Pattern"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant,com.team.lottery.system.BizSystemConfigVO,com.team.lottery.system.BizSystemConfigManager,
         com.team.lottery.vo.LotterySet,com.team.lottery.cache.ClsCacheManager,
         com.team.lottery.system.BizSystemConfigManager,com.team.lottery.vo.BizSystemInfo,
         com.team.lottery.util.LotterySetUntil,com.team.lottery.extvo.LotterySetMsg"
%>
<%
	String path = request.getContextPath();
	String serverName=  request.getServerName();//获取当前域名
	String bizSystem=ClsCacheManager.getBizSystemByDomainUrl(serverName);
	BizSystemInfo bizSystemInfo=ClsCacheManager.getBizSystemInfo(bizSystem);
	int csx=0;
	if(bizSystemInfo.getAndriodIosDownloadUrl()!=null&&bizSystemInfo.getAndriodIosDownloadUrl().length()>0){
        Pattern pattern = Pattern.compile("http(s)?:\\/\\/([\\w-]+\\.)+[\\w-]+(\\/[\\w- .\\/?%&=]*)?");
		if(pattern.matcher(bizSystemInfo.getAndriodIosDownloadUrl()).matches()){
			response.sendRedirect(bizSystemInfo.getAndriodIosDownloadUrl());
		}else{
			response.sendRedirect("\\/"+bizSystemInfo.getAndriodIosDownloadUrl());
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
     <meta charset="utf-8">
     <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
     <meta name="format-detection" content="telephone=no">
     <title>APP下载</title>
     <link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
     <link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
     <style>
		body { font-size: 54px; background: url(<%=path%>/images/app-bg.png) no-repeat; background-size: 100%; padding: 0 150px; padding-top: 150px; text-align: center; color: #fff;box-sizing: border-box;-webket-box-sizing: border-box;-moz-box-sizing: border-box;-ms-box-sizing: border-box }
		.logo { line-height: 130px; margin-bottom: 120px; }
		.logo img { width: 300px; }
		.logo h2 { font-size: 100px; }
		.logo p {font-size: 50px;}
		.bottom-btn a { display: block; width: 100%; height: 150px; line-height: 150px; border-radius: 25px; border: 4px solid #fff; color: #fff; margin-top: 80px;}
		.bottom-btn a > img { height: 80px; vertical-align: middle; margin-right: 50px; margin-top: -20px; }
		.code-btn { position:relative; }
     	.code-img {display:none; position:absolute;left:50%;margin-left:-206px;bottom:180px;height: 440px;padding:20px;background:#fff;box-shadow:0 0 10px rgba(0,0,0,0.22);-webkit-box-shadow:0 0 10px rgba(0,0,0,0.22);border-radius:6px;-webkit-border-radius:6px;transition:bottom .4s ease-out,opacity .4s ease-out;}
     	.code-img img {width:400px;}
     	.code-img .point {position:absolute;bottom:-44px;width:55px;left:50%;margin-left:-32px;}
     </style>
</head>
<body>

    <!-- header -->
    <div class="header color-dark">
        <div class="header-left" onclick="window.history.go(-1);"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
        <h2 class="header-title">APP下载</h2>
        <%-- <div class="header-right" onclick="changeStyle(this)"><img class="icon" src="<%=path%>/images/icon/icon-threetool.png"></div> --%>
    </div>
    <!-- header over -->

    <div class="stance"></div>
   
	<div class=container>
		<div class="logo">
		<% if(bizSystemInfo!= null && bizSystemInfo.getAppDownLogo() != null) {%>
			<img src="<%=bizSystemInfo.getAppDownLogo() %>" />
		<%}else{%>	
		    <img src="<%=path%>/images/app-logo.png" />
		<%}%>	
			<h2><%=bizSystemInfo.getAppShowname()!=null?bizSystemInfo.getAppShowname():"" %></h2>
			<p>下载APP 再也无需输入网址</p>
		</div>
		<div class="bottom-btn">
		<% if(bizSystemInfo!= null && bizSystemInfo.getHasApp() == 1) {
			if(bizSystemInfo.getAppStoreIosUrl() != null && !"".equals(bizSystemInfo.getAppStoreIosUrl())){
			%>	
			<a href="<%=bizSystemInfo.getAppStoreIosUrl() %>" class="download-ios"><img src="<%=path%>/images/icon-ios.png" />点击下载iOS版</a>
			<%}else if(bizSystemInfo.getAppIosBarcodeUrl() != null && !"".equals(bizSystemInfo.getAppIosBarcodeUrl())){
			%>
			<a href="javascript:;" class="code-btn" id="btn-ios">
				<img src="<%=path%>/images/icon-ios.png" />点击下载iOS版
				<div class="code-img"><img src="<%=bizSystemInfo.getAppIosBarcodeUrl() %>" /><img src="<%=path%>/images/pointer.png" class="point" /></div>
			</a>
			<%		
			}
			if(bizSystemInfo.getAppAndroidDownloadUrl()!= null && !"".equals(bizSystemInfo.getAppAndroidDownloadUrl())){
			%>	
			<a href="<%=bizSystemInfo.getAppAndroidDownloadUrl() %>"><img src="<%=path%>/images/icon-android.png" />点击下载安卓版</a>
			<%}else if(bizSystemInfo.getAppBarcodeUrl() != null && !"".equals(bizSystemInfo.getAppBarcodeUrl())){
				%>
				<a href="javascript:;" class="code-btn" id="btn-android">
				<img src="<%=path%>/images/icon-android.png" />点击下载安卓版
				<div class="code-img"><img src="<%=bizSystemInfo.getAppBarcodeUrl() %>" /><img src="<%=path%>/images/pointer.png" class="point" /></div>
			    </a>
				<%		
				}
			}%>
			
			
		</div>
	</div>
	
	<script src="<%=path%>/assets/jquery/jquery-3.1.1.min.js"></script>
	<script>
     	$('#btn-ios').click(function(){
     		$(this).find('.code-img').fadeToggle('fast');
     	});
     	$('#btn-android').click(function(){
     		$(this).find('.code-img').fadeToggle('fast');
     	})
     </script>
</body>
</html>