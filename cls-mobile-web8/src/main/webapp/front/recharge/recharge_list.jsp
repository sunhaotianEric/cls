<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.service.RechargeConfigService"%>
<%@page import="com.team.lottery.extvo.RechargeConfigVo"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.util.BaseDwrUtil"%>
<%@page import="com.team.lottery.vo.RechargeConfig"%>          
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.system.SystemPropeties"%>     
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.UserNameUtil"%> 
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>   
<%
	String path = request.getContextPath();
    String requestProtocol=request.getScheme();
	//查询当前登陆用户信息
	/* User user=(User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	RechargeConfigVo rechargeConfigVo=new RechargeConfigVo();
	rechargeConfigVo.setShowUserSscRebate(user.getSscRebate());
	rechargeConfigVo.setShowUserStarLevel(user.getStarLevel());
	rechargeConfigVo.setEnabled(1);
	rechargeConfigVo.setBizSystem(user.getBizSystem());
	rechargeConfigVo.setShowType(SystemPropeties.loginType);
	//获取请求协议类型是Http或者Https
	String requestProtocol=request.getScheme();
	requestProtocol = "http";
	//查询相关的充值设置信息
	RechargeConfigService rechargeConfigService =ApplicationContextUtil.getBean("rechargeConfigService");
	List<RechargeConfig> listParent = rechargeConfigService.getAllCanUseRechargeConfig(user, rechargeConfigVo,requestProtocol);
	if(listParent!=null && listParent.size()>0){
		request.setAttribute("RechargeConfigList", listParent);
	} */
%>    
<!DOCTYPE html>
<html>

<head>
     <meta charset="utf-8">
     <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
     <meta name="format-detection" content="telephone=no">
     <title>充值方式</title>
     <link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
     <link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
     <link href="<%=path%>/css/rechargewithdraw_fund_list.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
</head>

<body>

    <!-- header -->
    <div class="header color-dark">
        <div class="header-left" onclick="window.location.href='<%=path%>/gameBet/index.html'"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
        <h2 class="header-title">请选择充值方式</h2>
    </div>
    <!-- header over -->

    <div class="stance"></div>


    <div class="content">

    </div>


    <!-- nav-bar -->
    <jsp:include page="/front/include/navbar.jsp"></jsp:include>
    <!-- nav-bar over -->

    <jsp:include page="/front/include/include.jsp"></jsp:include>
     <script src="<%=path%>/js/recharge/recharge_list.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
      <script type="text/javascript">
      rechargeListPage.param.requestProtocol ='<%=requestProtocol%>';
    </script>
</body>

</html>