<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.service.RechargeConfigService"%>
<%@page import="com.team.lottery.extvo.RechargeConfigVo"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.util.BaseDwrUtil"%>
<%@page import="com.team.lottery.vo.RechargeConfig"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.system.SystemPropeties"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant"%>
<%
	String path = request.getContextPath();
	String id = request.getParameter("id");
%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<meta id="viewport" name="viewport"
	content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
<meta name="format-detection" content="telephone=no">
<title>充值</title>
<link
	href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>"
	rel="stylesheet">
<link
	href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>"
	rel="stylesheet">
<link
	href="<%=path%>/css/rechargewithdraw_fund.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>"
	rel="stylesheet">
</head>

<body>

	<!-- header -->
	<div class="header color-dark rechargewithdraw_fund">
		<div class="header-left" id="showHeader"
			onclick="window.location.href='<%=path%>/gameBet/rechargeList.html'">
			<img class="icon" src="<%=path%>/images/icon/icon-return.png">
		</div>
		<h2 class="header-title" id="headerId" onclick="headerChange(this)"
			data-div="content1" data-paytype="BANK_TRANSFER" data-id="1"
			data-refidlist="7" data-reftype="TRANSFER"></h2>
		<!--  <h2 class="header-title">银行转账</h2> -->
	</div>
	<!-- header over -->

	<div class="stance" id="fundContent"></div>
	<div class="content on" id="content1">
		<ul class="user-list" >
			<li class="user-line" id="payDiv">
				<div class="container">
					<div class="line-title">
						<p>充值金额</p>
					</div>
					<div class="line-content">
						<input name="rechargeininput" class="inputText" type="tel"
							placeholder="请输入金额">
					</div>
				</div>
			</li>
		</ul>
		<ul class="user-list">
			<li class="user-line select" id="selectLine"><div
					class="container select-container">
					<div class="line-title">
						<img id="pic1" onclick="savepic();return false;"
							src="" alt="">
					</div>
					<div class="line-content">
						<input type="hidden" name="payId" value="">
						<h2 class="title"></h2>
					</div>
				</div></li>
		</ul>
		<br>
		<div class="user-btn color-red submitBoolean"
			onclick="rechargePage.rechargeStepValidate(this)"  name="wangyin" data-submit="false">下一步</div>
		<ul class="user-list no">
			<li class="user-msg"><div class="pay_tips">
					<p>
						单笔最低充值金额为<span name="bankLowestValue" id="lowestValue">0.00</span>元，最高<span
							name="bankHighestValue" id="highestValue">0.00</span>元
					</p>
					<p>请在15分钟内完成充值</p>
				</div></li>
		</ul>
	</div>
	<%-- <div class="content on">
        <br />
        <br />
        <ul class="user-list">
            <!-- user-list 下拉框 -->
            <li class="user-line select bankname" onclick="modal_toggle('.bank-modal')">
                <div class="container select-container">
                    <div class="line-title">选择银行</div>
                    <div class="line-title line-banklogo"><img src="<%=path%>/images/bankcard_logo/jianshe.png" alt=""></div>
                    <div class="line-content">
                        <input type="hidden" class="value" value="中国建设银行">
                        <input type="hidden" class="mun_value" value="中国建设银行">
                        <h2 class="title">中国建设银行</h2>
                    </div>
                    <div class="right"><img class="icon" src="<%=path%>/images/icon/icon-pointer.png" /></div>
                </div>
            </li>
            <!-- user-list over 下拉框 -->
        </ul>

        <ul class="user-list">
            <!-- user-list 输入框 -->
            <li class="user-line">
                <div class="container">
                    <div class="line-title">
                        <p>充值金额</p>
                    </div>
                    <div class="line-content"><input class="inputText" type="tel" placeholder="请输入金额"></div>
                </div>
            </li>
            <!-- user-list over 输入框 -->
        </ul>


        <ul class="user-list bankcard">

            <li class="user-line user-line2">
                <div class="container">
                    <div class="line-title">
                        <p>充值金额</p>
                    </div>
                    <div class="line-content">
                        <p class="font-red">100元</p>
                    </div>
                </div>
            </li>
            <li class="user-line user-line2">
                <div class="container">
                    <div class="line-title">
                        <p>收款银行</p>
                    </div>
                    <div class="line-content"><textarea class="font-black" onfocus="inputSelect(this)">中国工商银行</textarea></div>
                    <div class="right color-red btn-small" onclick="inputSelect(this)">复制</div>
                </div>
            </li>
            <li class="user-line user-line2">
                <div class="container">
                    <div class="line-title">
                        <p>收款人账号名</p>
                    </div>
                    <div class="line-content"><textarea class="font-black" onfocus="inputSelect(this)">寒冰云</textarea></div>
                    <div class="right color-red btn-small" onclick="inputSelect(this)">复制</div>
                </div>
            </li>
            <li class="user-line user-line2">
                <div class="container">
                    <div class="line-title">
                        <p>收款账号</p>
                    </div>
                    <div class="line-content"><textarea class="font-black" onfocus="inputSelect(this)">123123123123123123</textarea></div>
                    <div class="right color-red btn-small" onclick="inputSelect(this)">复制</div>
                </div>
            </li>
            <li class="user-line user-line2">
                <div class="container">
                    <div class="line-title">
                        <p>开户行地址</p>
                    </div>
                    <div class="line-content"><textarea class="font-black" onfocus="inputSelect(this)">杭州建设银行</textarea></div>
                    <div class="right color-red btn-small" onclick="inputSelect(this)">复制</div>
                </div>
            </li>
            <li class="user-line user-line2">
                <div class="container">
                    <div class="line-title">
                        <p>附言(编号)</p>
                    </div>
                    <div class="line-content"><textarea class="font-black" onfocus="inputSelect(this)">344123412</textarea></div>
                    <div class="right color-red btn-small" onclick="inputSelect(this)">复制</div>
                </div>
            </li>

        </ul>
        <ul class="user-list no">
            <!-- user-list 提示信息 -->
            <li class="user-msg">
                <div class="container">
                    <p>为了您的资金能及时到账，请务必填写附言</p>
                </div>
            </li>
            <!-- user-list over 提示信息 -->
        </ul>

        <br />
        <br />
        <div class="user-btn color-red" onclick="modal_toggle('.alert-modal1')">确认充值</div>

    </div> --%>

	<!-- modal bank-modal -->
	<div class="modal bank-modal animated out" id="bankMoel">
		<!-- header -->
		<div class="header color-dark rechargewithdraw_fund">
			<div class="header-left" onclick="modal_toggle('.bank-modal')">
				<img class="icon" src="<%=path%>/images/icon/icon-return.png">
			</div>
			<h2 class="header-title">选择银行</h2>
		</div>
		<!-- header over -->
		<div class="stance"></div>
		<!-- user-list 下拉框 -->
		<li class="user-line select noselect">
			<div class="select-list" id="banklistinfo"></div>
		</li>
		<!-- user-list over 下拉框 -->
		<div class="stance-nav"></div>
	</div>
	<!-- modal bank-modal over -->

	<!-- modal recharge-modal -->
	<div class="recharge-modal modal alert-modal animated out">
		<!-- header -->
		<div class="header color-dark rechargewithdraw_fund">
			<div class="header-left" onclick="modal_toggle('.recharge-modal')">
				<img class="icon" src="<%=path%>/images/icon/icon-return.png">
			</div>
			<h2 class="header-title" id="rechargeTip">订单提交成功</h2>
		</div>
		<!-- header over -->

		<div class="stance"></div>
		<ul class="user-list">
			<li class="user-line user-line2">
				<div class="container">
					<div class="line-title">
						<p>充值金额</p>
					</div>
					<div class="line-content">
						<input class="font-red font-black" id='alertBankAmount'
							value="加载中..." style="color: #00a8ff !important;"></input>
					</div>
				</div>
			</li>
			<!--支付宝转帐(银行) -->
			<li class="user-line user-line2 alipayTip" style="display: none;">
				<div class="container">
					<div class="line-title">
						<p>支付宝收款银行</p>
					</div>
					<div class="line-content">
						<input class="font-black" id='alipayBankName' value="加载中..."></input>
					</div>
				</div>
			</li>
			<li class="user-line user-line2 alipayTip" style="display: none;">
				<div class="container">
					<div class="line-title">
						<p>支付宝收款人账号名</p>
					</div>
					<div class="line-content">
						<input class="font-black" id='alipayBankUserName' value="加载中..."></input>
					</div>
					<button class="right color-red btn-small copy"
						id="alertBankUserNameCopy"
						data-clipboard-target="#alipayBankUserName">复制</button>
				</div>
			</li>
			<li class="user-line user-line2 alipayTip" style="display: none;">
				<div class="container">
					<div class="line-title">
						<p>支付宝收款账号</p>
					</div>
					<div class="line-content">
						<input class="font-black" id='alipayBankId' value="加载中..."></input>
					</div>
					<button class="right color-red btn-small copy" id="alertBankIdCopy"
						data-clipboard-target="#alipayBankId">复制</button>
				</div>
			</li>
			<li class="user-line user-line2 alipayTip" id="bankBranchNameLi">
				<div class="container">
					<div class="line-title">
						<p>开户行地址</p>
					</div>
					<div class="line-content">
						<input class="font-black" id='alipayBankBranchName' value="加载中..."></input>
					</div>
					<button class="right color-red btn-small copy"
						id="alertBankBranchNameCopy"
						data-clipboard-target="#alipayBankBranchName">复制</button>
				</div>
			</li>
			<li class="user-line user-line2 alipayTip">
				<div class="container">
					<div class="line-title">
						<p id="alipayfuyuanId">附言(编号)</p>
					</div>
					<div class="line-content">
						<input class="font-black" id='alipayBankscript' value="加载中..."></input>
					</div>
				</div>
			</li>
			<li class="user-line user-line2 alipayTip" style="display: none;"
				id="imgAlipay">
				<div class="container">
					<div id="barcodeDivAlipay"
						style="padding: 10px; width: 320px; text-align: center; margin: auto;">
						<img id="barcodeImgAlipay" style="width: 250px; height: 250px;"
							alt="" src="">
					</div>

				</div>
			</li>
			<!--支付宝转帐(账号) -->
			<li class="user-line user-line2 alipayAccount" style="display: none;">
				<div class="container">
					<div class="line-title">
						<p>支付宝收款人姓名</p>
					</div>
					<div class="line-content">
						<input class="font-black" id='alipayAccountBankName'
							value="加载中..."></input>
					</div>
					<button class="right color-red btn-small copy"
						id="alertBankscriptCopy"
						data-clipboard-target="#alipayAccountBankName">复制</button>
				</div>
			</li>
			<li class="user-line user-line2 alipayAccount" style="display: none;">
				<div class="container">
					<div class="line-title">
						<p>支付宝收款人账号</p>
					</div>
					<div class="line-content">
						<input class="font-black" id='alipayAccountBankId' value="加载中..."></input>
					</div>
					<button class="right color-red btn-small copy"
						id="alertBankscriptCopy"
						data-clipboard-target="#alipayAccountBankId">复制</button>
				</div>
			</li>
			<li class="user-line user-line2 alipayAccount">
				<div class="container">
					<div class="line-title">
						<p id="alipayAccountfuyuanId">附言(编号)</p>
					</div>
					<div class="line-content">
						<input class="font-black" id='alipayAccountBankscript'
							value="加载中..."></input>
					</div>

				</div>
			</li>
			<!--转帐银行 -->
			<li class="user-line user-line2 bankpayTip" style="display: none;">
				<div class="container">
					<div class="line-title">
						<p>收款银行</p>
					</div>
					<div class="line-content">
						<input class="font-black" id='alertBankName' value="加载中..."></input>
					</div>
				</div>
			</li>
			<li class="user-line user-line2 bankpayTip" style="display: none;">
				<div class="container">
					<div class="line-title">
						<p>收款人账号名</p>
					</div>
					<div class="line-content">
						<span class="font-black" id='alertBankUserName' value="加载中..."></span>
					</div>
					<button class="right color-red btn-small copy"
						id="alertBankUserNameCopy"
						data-clipboard-target="#alertBankUserName">复制</button>
				</div>
			</li>
			<li class="user-line user-line2 bankpayTip" style="display: none;">
				<div class="container">
					<div class="line-title">
						<p>收款账号</p>
					</div>
					<div class="line-content">
						<input class="font-black" id='alertBankId' value="加载中..."></input>
					</div>
					<button class="right color-red btn-small copy" id="alertBankIdCopy"
						data-clipboard-target="#alertBankId">复制</button>
				</div>
			</li>
			<li class="user-line user-line2 bankpayTip" id="bankBranchNameLi">
				<div class="container">
					<div class="line-title">
						<p>开户行地址</p>
					</div>
					<div class="line-content">
						<input class="font-black" id='alertBankBranchName' value="加载中..."></input>
					</div>
					<button class="right color-red btn-small copy"
						id="alertBankBranchNameCopy"
						data-clipboard-target="#alertBankBranchName">复制</button>
				</div>
			</li>
			<li class="user-line user-line2 bankpayTip">
				<div class="container">
					<div class="line-title">
						<p id="fuyuanId">附言(编号)</p>
					</div>
					<div class="line-content">
						<input class="font-black" id='alertBankscript' value="加载中..."></input>
					</div>
					<button class="right color-red btn-small copy"
						id="alertBankscriptCopy" data-clipboard-target="#alertBankscript">复制</button>
				</div>
			</li>
			<!--微信转帐(账号) -->
			<li class="user-line user-line2 weixinAccount" style="display: none;">
				<div class="container">
					<div class="line-title">
						<p>微信收款人昵称</p>
					</div>
					<div class="line-content">
						<input class="font-black" id='weixinalertBankUserName'
							value="加载中..."></input>
					</div>
				</div>
			</li>
			<li class="user-line user-line2 weixinAccount" style="display: none;">
				<div class="container">
					<div class="line-title">
						<p>微信收款人账号</p>
					</div>
					<div class="line-content">
						<input class="font-black" id='weixinalertBankId' value="加载中..."></input>
					</div>
				</div>
			</li>
			<li class="user-line user-line2 weixinAccount">
				<div class="container">
					<div class="line-title">
						<p id="weixinfuyuanId">附言(编号)</p>
					</div>
					<div class="line-content">
						<input class="font-black" id="weixinalertBankscript"
							value="加载中..."></input>
					</div>

				</div>
			</li>
			<li class="user-line user-line2 weixinAccount" style="display: none;"
				id="imgWeixin">
				<div class="container">
					<div id="barcodeDivWeixin"
						style="text-align: center; margin: auto;">
						<img id="barcodeImgWeixin" style="width: 400px; height: 400px;"
							alt="" src="">
					</div>

				</div>
			</li>
			<!--QQ钱包转帐(账号) -->
			<li class="user-line user-line2 qqpayAccount" style="display: none;">
				<div class="container">
					<div class="line-title">
						<p>QQ钱包收款人昵称</p>
					</div>
					<div class="line-content">
						<input class="font-black" id='qqpayalertBankUserName'
							value="加载中..."></input>
					</div>
				</div>
			</li>
			<li class="user-line user-line2 qqpayAccount" style="display: none;">
				<div class="container">
					<div class="line-title">
						<p>QQ钱包收款人账号</p>
					</div>
					<div class="line-content">
						<input class="font-black" id='qqpayalertBankId' value="加载中..."></input>
					</div>
				</div>
			</li>
			<li class="user-line user-line2 qqpayAccount">
				<div class="container">
					<div class="line-title">
						<p id="qqpayfuyuanId">附言(编号)</p>
					</div>
					<div class="line-content">
						<input class="font-black" id="qqpayalertBankscript" value="加载中..."></input>
					</div>

				</div>
			</li>

		</ul>

		<ul class="user-list no">
			<!-- user-list 提示信息 -->
			<!-- <li class="user-msg"><div class="container">
			<p>为了您的资金能及时到账，请务必填写附言</p>
			</div></li> -->
			<li class="user-msg alipayTip" style="display: none;"><div
					class="container">
					<p>为了您的资金能及时到账，请通过您的支付宝账号转帐到上方的支付宝收款银行账号</p>
				</div></li>
			<li class="user-msg alipayAccount" style="display: none;"><div
					class="container">
					<p>为了您的资金能及时到账，请通过您的支付宝账号转帐到上方的支付宝账号</p>
				</div></li>
			<li class="user-msg bankpayTip" style="display: none;">
				<div class="container">
					<p>为了您的资金能及时到账，请通过您的网银转到上方的收款银行账号</p>
				</div>
			</li>
			<li class="user-msg weixinAccount" style="display: none;"><div
					class="container">
					<p>为了您的资金能及时到账，请通过您的微信转帐到上方的微信账号</p>
				</div></li>
			<li class="user-msg qqpayAccount" style="display: none;"><div
					class="container">
					<p>为了您的资金能及时到账，请通过您的QQ钱包转帐到上方的QQ钱包账号</p>
				</div></li>
			<!-- user-list over 提示信息 -->
		</ul>

		<br /> <br />
		<div id="cancelRechargeButton"
			class="user-btn color-red cancelRechargeButton" style="display: none">取消已有订单</div>
		<div id="gotoRechargeButton"
			class="user-btn color-red gotoRechargeButton" style="display: none">前往充值</div>
		<div class="stance-nav"></div>
	</div>
	<!-- modal additional-modal over -->


	<!-- modal recharge-modal -->
	<div class="weixin-recharge-modal modal animated out">
		<!-- header -->
		<div class="header color-dark rechargewithdraw_fund">
			<div class="header-left"
				onclick="window.location.href='<%=path%>/gameBet/rechargeList.html'">
				<img class="icon" src="<%=path%>/images/icon/icon-return.png">
			</div>
			<h2 class="header-title">订单提交成功</h2>
		</div>
		<!-- header over -->
		<iframe name="submitFrame"
			src="<%=path%>/front/recharge/recharge_form.jsp"
			style="background: #fff;"> </iframe>

	</div>
	<!-- modal bank-modal over -->



	<!-- modal alert-modal1 -->
	<div class="modal alert-modal1 animated out">
		<!-- header -->
		<div class="header color-dark rechargewithdraw_fund">
			<div class="header-left" onclick="modal_toggle('.alert-modal1')">
				<img class="icon" src="<%=path%>/images/icon/icon-return.png">
			</div>
			<h2 class="header-title">订单提交成功</h2>
		</div>
		<!-- header over -->
		<div class="stance"></div>
		<ul class="user-list">

			<li class="user-line user-line2">
				<div class="container">
					<div class="line-title">
						<p>充值金额</p>
					</div>
					<div class="line-content">
						<p class="font-red">100元</p>
					</div>
				</div>
			</li>
			<li class="user-line user-line2">
				<div class="container">
					<div class="line-title">
						<p>收款银行</p>
					</div>
					<div class="line-content">
						<textarea class="font-black" onfocus="inputSelect(this)">加载中</textarea>
					</div>
					<div class="right color-red btn-small" onclick="inputSelect(this)">复制</div>
				</div>
			</li>
			<li class="user-line user-line2">
				<div class="container">
					<div class="line-title">
						<p>收款人账号名</p>
					</div>
					<div class="line-content">
						<textarea class="font-black" onfocus="inputSelect(this)">加载中</textarea>
					</div>
					<div class="right color-red btn-small" onclick="inputSelect(this)">复制</div>
				</div>
			</li>
			<li class="user-line user-line2">
				<div class="container">
					<div class="line-title">
						<p>收款账号</p>
					</div>
					<div class="line-content">
						<textarea class="font-black" onfocus="inputSelect(this)">加载中</textarea>
					</div>
					<div class="right color-red btn-small" onclick="inputSelect(this)">复制</div>
				</div>
			</li>
			<li class="user-line user-line2">
				<div class="container">
					<div class="line-title">
						<p>开户行地址</p>
					</div>
					<div class="line-content">
						<textarea class="font-black" onfocus="inputSelect(this)">加载中</textarea>
					</div>
					<div class="right color-red btn-small" onclick="inputSelect(this)">复制</div>
				</div>
			</li>
			<li class="user-line user-line2">
				<div class="container">
					<div class="line-title">
						<p>附言(编号)</p>
					</div>
					<div class="line-content">
						<textarea class="font-black" onfocus="inputSelect(this)">加载中</textarea>
					</div>
					<div class="right color-red btn-small" onclick="inputSelect(this)">复制</div>
				</div>
			</li>

		</ul>
		<ul class="user-list no">
			<!-- user-list 提示信息 -->
			<li class="user-msg">
				<div class="container">
					<p>为了您的资金能及时到账，请务必填写附言</p>
				</div>
			</li>
			<!-- user-list over 提示信息 -->
		</ul>
		<br /> <br />
		<div class="user-btn color-red">前往充值</div>
	</div>
	<!-- modal alert-modal1 over -->



	<!-- nav-bar -->
	<jsp:include page="/front/include/navbar.jsp"></jsp:include>
	<!-- nav-bar over -->

	<jsp:include page="/front/include/include.jsp"></jsp:include>
	<script src="<%=path%>/js/clipboard.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
	<script src="<%=path%>/js/recharge/recharge.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
	<script type="text/javascript">
		rechargePage.param.id = <%=id%>;
	</script>

</body>

</html>