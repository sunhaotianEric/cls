<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
    String status=request.getParameter("status");
%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="format-detection" content="telephone=no">
    <title>投注记录</title>
    <link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/user_order.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
</head>

<body>

    <!-- header -->
    <div class="header color-dark">
        <div class="header-left" onclick="window.history.go(-1);"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
        <h2 class="header-title">投注记录</h2>
    </div>
    <!-- header over -->


    <ion-content>


        <div class="stance"></div>

        <!-- main -->
        <div class="main" id="userOrderList">
            <!-- <div class="line">
                <h4 class="nodata">暂无数据</h4>
            </div>
            </a>

            <a href="user_order_detail.html">
                <div class="line">
                    <div class="periods">
                        <p class="period">20161015-68</p>
                        <p class="time">2016/09/27 11:16:33</p>
                    </div>
                    <div class="content">
                        <h2 class="title">重庆时时彩</h2>
                        <div class="info">
                            <span class="money">¥ 6.00</span>
                        </div>

                    </div>
                    <div class="status">等待开奖</div>
                </div>
            </a>
            <a href="user_order_detail_pk10.html">
                <div class="line">
                    <div class="periods">
                        <p class="period">20161015-68</p>
                        <p class="time">2016/09/27 11:16:33</p>
                    </div>
                    <div class="content">
                        <h2 class="title">北京PK10</h2>
                        <div class="info">
                            <span class="money">¥ 6.00</span>
                        </div>

                    </div>
                    <div class="status">等待开奖</div>
                </div>
            </a>
            <a href="user_order_detail_kuaisan.html">
                <div class="line">
                    <div class="periods">
                        <p class="period">20161015-68</p>
                        <p class="time">2016/09/27 11:16:33</p>
                    </div>
                    <div class="content">
                        <h2 class="title">江苏快三</h2>
                        <div class="info">
                            <span class="money">¥ 6.00</span>
                        </div>

                    </div>
                    <div class="status">等待开奖</div>
                </div>
            </a>
            <a href="user_order_detail.html">
                <div class="line open">
                    <div class="periods">
                        <p class="period">20161015-68</p>
                        <p class="time">2016/09/27 11:16:33</p>
                    </div>
                    <div class="content">
                        <h2 class="title">重庆时时彩</h2>
                        <div class="info">
                            <span class="money">¥ 6.00</span>
                        </div>

                    </div>
                    <div class="status font-red">已开奖</div>
                    <div class="status2">¥ 6.00</div>
                </div>
            </a>
            <a href="user_order_detail.html">
                <div class="line">
                    <div class="periods">
                        <p class="period">20161015-68</p>
                        <p class="time">2016/09/27 11:16:33</p>
                    </div>
                    <div class="content">
                        <h2 class="title">重庆时时彩</h2>
                        <div class="info">
                            <span class="money">¥ 6.00</span>
                        </div>

                    </div>
                    <div class="status">等待开奖</div>
                </div>
            </a>
            <a href="user_order_detail.html">
                <div class="line">
                    <div class="periods">
                        <p class="period">20161015-68</p>
                        <p class="time">2016/09/27 11:16:33</p>
                    </div>
                    <div class="content">
                        <h2 class="title">重庆时时彩</h2>
                        <div class="info">
                            <span class="money">¥ 6.00</span>
                        </div>

                    </div>
                    <div class="status">等待开奖</div>
                </div>
            </a>
            <a href="user_order_detail.html">
                <div class="line">
                    <div class="periods">
                        <p class="period">20161015-68</p>
                        <p class="time">2016/09/27 11:16:33</p>
                    </div>
                    <div class="content">
                        <h2 class="title">重庆时时彩</h2>
                        <div class="info">
                            <span class="money">¥ 6.00</span>
                        </div>

                    </div>
                    <div class="status">等待开奖</div>
                </div>
            </a> -->
        </div>
        <!-- main over -->

        <br>
        <br>


        <div class="user-btn color-gray">加载更多</div>
        <!-- nav-bar -->
        <jsp:include page="/front/include/navbar.jsp"></jsp:include>
        <!-- nav-bar over -->
        <jsp:include page="/front/include/include.jsp"></jsp:include>
        <script type="text/javascript">
        var status="<%=status%>";
        </script>
        <script type="text/javascript" src="<%=path%>/js/order/order.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>

</body>

</html>