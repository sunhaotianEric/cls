<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
    String id=request.getParameter("id");
%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="format-detection" content="telephone=no">
    <title>投注详情</title>
    <link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/user_order_detail.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
</head>

<body>

    <!-- header -->
    <div class="header color-dark">
        <div class="header-left" onclick="window.history.go(-1);"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
        <h2 class="header-title">投注详情</h2>
    </div>
    <!-- header over -->

    <!-- alert-bg -->
    <div class="alert-bg" onclick="element_toggle(false,['.alert'])"></div>
    <!-- alert-bg over -->
    <!-- alert -->
    <div class="alert">
        <div class="alert-head color-dark">
            <p>详细号码</p>
            <img src="<%=path%>/images/icon/icon-close.png" alt="" onclick="element_toggle(false,['.alert'])" class="close icon">
        </div>
        <div class="alert-content">
            <p id="alertOpencodes">开奖号:加载中</p>

        </div>
    </div>
    <!-- alert over -->


    <div class="stance"></div>

    <!-- main -->
    <div class="main">
        <div class="head">
            <div class="logo"><img src="<%=path%>/images/lottery_logo/index/logo_shishicai.png"></div>
            <div class="headcontent">
                <h2 class="title font-red" id="lotteryTypeDes">加载中</h2>
                <p class="periods" id="lotteryExpect">第XXXXXXXX期</p>
            </div>
            <div class="status" id="statusId">加载中</div>
        </div>
        <div class="content">
            <div class="container">
                <div class="line">
                    <div class="line-title">
                        <p>投注时间</p>
                    </div>
                    <div class="line-content">
                        <p id="lotteryCreateDate">0000-00-00 00:00:00</p>
                    </div>
                </div>
                <div class="line">
                    <div class="line-title">
                        <p>方案编号</p>
                    </div>
                    <div class="line-content">
                        <p id="lotteryId">-----------------</p>
                    </div>
                </div>
                <div class="line">
                    <div class="line-title">
                        <p>投注金额</p>
                    </div>
                    <div class="line-content">
                        <p id="lotteryPayMoney">加载中</p>
                    </div>
                </div>
                <div class="line no">
                    <div class="line-title full">
                        <p>开奖号码</p>
                    </div>
                    <div class="line-content full" id="opencodes">
                        <!-- <div class="mun color-red">1</div>
                        <div class="mun color-red">3</div>
                        <div class="mun color-red">6</div>
                        <div class="mun color-red">3</div>
                        <div class="mun color-red">2</div> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="foot">
            <div class="container">
                <h2 class="title">我的投注</h2>
                <div class="footcontent">
                    <div class="sub"></div>
                    <div class="lines" id="codeList">

                       <!--  <div class="line">
                            <div class="line-title font-red">
                                <p>0,9,1,2,3,4 <a class="font-gray" onclick="element_toggle(true,['.alert'])">更多</a></p>
                            </div>
                            <div class="line-content">
                                <p>五星直选</p>
                            </div>
                        </div>
                        <div class="line">
                            <div class="line-title font-red">
                                <p>0,9,1,2,3,4</p>
                            </div>
                            <div class="line-content">
                                <p>五星直选</p>
                            </div>
                        </div>
                        <div class="line">
                            <div class="child child2 font-red muns muns-middle">
                                <div class="mun color-red">2</div>
                                <div class="mun color-red">4</div>
                                <div class="mun color-red">6</div>
                                <div class="mun color-red">0</div>
                                <div class="mun color-red">9</div>
                            </div>
                            <div class="line-content">
                                <p>五星直选</p>
                            </div>
                        </div>
                        <div class="line">
                            <div class="line-title font-red">
                                <p>0,9,1,2,3,4</p>
                            </div>
                            <div class="line-content">
                                <p>五星直选</p>
                            </div>
                        </div> -->

                    </div>

                </div>
            </div>
        </div>


    </div>
    <!-- main over -->

    <br>
    <br>
    <br>
    <br>

    <div class="user-btn color-red" style="display: none" id="stopOrderButton">撤单</div>




    <!-- nav-bar -->
   <jsp:include page="/front/include/navbar.jsp"></jsp:include>
    <!-- nav-bar over -->

    <!-- <script src="assets/jquery/jquery-3.1.1.min.js"></script>
    <script src="js/base.js"></script> -->
    <jsp:include page="/front/include/include.jsp"></jsp:include>
    <script type="text/javascript">
    var id="<%=id%>";
    </script>
    
    <script type="text/javascript" src="<%=path%>/js/order/order_detail.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>


</body>

</html>