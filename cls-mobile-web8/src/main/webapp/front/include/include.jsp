<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="com.team.lottery.util.ConstantUtil,net.sf.json.JSONObject,com.team.lottery.vo.User" %>
<%@ page import="com.team.lottery.cache.ClsCacheManager,com.team.lottery.vo.BizSystemInfo" %>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>

<%
	String path = request.getContextPath();
%>



<script src="<%=path%>/assets/jquery/jquery-3.1.1.min.js"></script>
<script src="<%=path%>/js/base.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
<script src="<%=path%>/js/common.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
<%-- <script type='text/javascript' src='<%=path%>/js/lottery_base.js'></script> --%>


<!-- util js -->
<script type="text/javascript" src="<%=path%>/assets/utils/utility.js"></script>
<script type="text/javascript" src="<%=path%>/assets/utils/string_util.js"></script>
<script type="text/javascript" src="<%=path%>/assets/utils/map_util.js"></script>
<script type="text/javascript" src="<%=path%>/assets/utils/date-util.js"></script>
<script type="text/javascript" src="<%=path%>/assets/utils/common.js"></script>
<script type="text/javascript" src="<%=path%>/assets/utils/banknoluhmCheck.js"></script>
<script type="text/javascript" src="<%=path%>/assets/jszip/jszip.min.js"></script>


<%
  User user = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
  String userJson = "{}";
  if (user != null) {
     JSONObject obj = JSONObject.fromObject(user);
     userJson = obj.toString();
  }
  request.setAttribute("user", user);
  
	
	//获取当前域名
	String serverName=  request.getServerName();
	String bizSystem=ClsCacheManager.getBizSystemByDomainUrl(serverName);
	BizSystemInfo bizSystemInfo=ClsCacheManager.getBizSystemInfo(bizSystem);
	//客服
	String customUrl = "";
	if(bizSystemInfo != null) {
		customUrl = bizSystemInfo.getCustomUrl();
	}
	String faviconUrl = "";
	if(bizSystemInfo != null && bizSystemInfo.getFaviconUrl() != null){
		  	faviconUrl = bizSystemInfo.getFaviconUrl();
	}
%>
<link rel="shortcut icon" href="<%= faviconUrl%>" type="image/x-icon" />
<script type="text/javascript">
  var contextPath = '<%=path %>';
  var currentUser = <%=userJson %>;
  var customUrl = '<%=customUrl %>';
  var currentUserSscModel = currentUser.sscRebate;
  var currentUserKsModel = currentUser.ksRebate;
  var currentUserPk10Model = currentUser.pk10Rebate;
  var currentUserSyxwModel = currentUser.syxwRebate;
  var ksLowestAwardModel = <%=SystemConfigConstant.KSLowestAwardModel.doubleValue() %>;
  var pk10LowestAwardModel = <%=SystemConfigConstant.PK10LowestAwardModel.doubleValue() %>;
  var sscLowestAwardModel = <%=SystemConfigConstant.SSCLowestAwardModel %>;
  var syxwLowestAwardModel = <%=SystemConfigConstant.SYXWLowestAwardModel %>;
  //这里最低模式值先默认时时彩的，应当根据各彩种不同来调整
  var lowestAwardModel = sscLowestAwardModel;
  var exceedWithdrawExpenses = currentUser.totalCanWithdrawMoney;
  //系统最高模式值
  	var sscHighestAwardModel = <%=SystemConfigConstant.SSCHighestAwardModel%>;
  	var syxwHighestAwardModel = <%=SystemConfigConstant.SYXWHighestAwardModel%>;
  	var dpcHighestAwardModel = <%=SystemConfigConstant.DPCHighestAwardModel%>;
  	var pk10HighestAwardModel = <%=SystemConfigConstant.PK10HighestAwardModel%>;
  	var ksHighestAwardModel = <%=SystemConfigConstant.KSHighestAwardModel%>;
</script>

<script type="text/javascript">
function commonHandlerResult(result, url){
	//调试模式打开，则输出错误到前台展现
	var debug = true;
	if(typeof(result) == "string"){
		result = JSON.parse(result);
	}
	if(typeof result == "undefined" || result == null || result == "") {
		var errorString = "出现异常:数据内容返回错误-" + url;
		console.error(errorString);
		if(debug) {
			showTip(errorString);
		}
	}
	//code:nologin
	if(result.code == "nologin") {
		var errorString = "用户未登录：" + url;
		console.error(errorString);
	}
	//返回code只能是ok、error、exception
	if(result.code != "ok" && result.code != "error" && result.code != "exception" && result.code != "nologin") {
		var errorString = "出现异常:数据格式返回错误-" + url;
		console.error(errorString);
		if(debug) {
			showTip(errorString);
		}
	}
	//异常处理
	if(result.code == "exception") {
		var errorString = "出现异常:" + url;
		console.error(errorString);
		if(debug) {
			showTip(errorString);
		}
	}
}
</script>