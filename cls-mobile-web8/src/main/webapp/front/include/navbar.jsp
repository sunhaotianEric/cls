<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
%>
<div class="stance-nav"></div>
<div class="nav-bar">
    <a href="<%=path%>/gameBet/index.html">
        <div class="child">
            <div class="child-icon i-home"></div>
            <h4 class="child-title">主页</h4>
        </div>
    </a>
    <a href="<%=path%>/gameBet/hall.html">
        <div class="child">
            <div class="child-icon i-shop"></div>
            <h4 class="child-title">大厅</h4>
        </div>
    </a>
    <a href="<%=path%>/gameBet/activity.html">
        <div class="child">
            <div class="child-icon i-activity"></div>
            <h4 class="child-title">活动</h4>
        </div>
    </a>
    <a href="<%=path%>/gameBet/lotterycode.html">
        <div class="child">
            <div class="child-icon i-discover"></div>
            <h4 class="child-title">发现</h4>
        </div>
    </a>
    <a href="javascript:judgeCurrentUserToLogin('<%=path%>/gameBet/my.html','要查看我的信息用户必须要先登录?')">
        <div class="child">
            <div class="child-icon i-person"></div>
            <h4 class="child-title">个人</h4>
        </div>
    </a>
</div>