<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>  
<%
	String path = request.getContextPath();
	User user=(User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="format-detection" content="telephone=no">
    <title>提现</title>
    <link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/rechargewithdraw_withdraw.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
</head>

<body>
    <!-- header -->
    <div class="header color-dark">
        <div class="header-left" onclick="window.history.go(-1);"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
        <h2 class="header-title">提现</h2>
    </div>
    <!-- header over -->
    <!-- alert-bg -->
    <div class="alert-bg" onclick="element_toggle(false,['.alert'])"></div>
    <!-- alert-bg over -->
    <!-- alert -->
    <div class="alert">
        <div class="alert-head color-dark">
            <p>温馨提示</p>
            <i class="close ion-close-round" onclick="element_toggle(false,['.alert'])"></i>
        </div>
        <div class="alert-content">
            <p style="text-align:center;">对不起,您当前没有设置安全密码.</p>
        </div>
        <div class="alert-foot">
            <div class="btn color-red cols-50">设置安全密码</div>
            <div class="btn color-gray cols-50" onclick="element_toggle(false,['.alert'])">返回首页</div>
        </div>
    </div>
    <!-- alert over -->


    <ion-content>

        <div class="stance"></div>



        <ul class="user-list">
            <!-- user-list 普通文本 -->
            <li class="user-line">
                <div class="container">
                    <div class="line-title">
                        <p>用户名:</p>
                    </div>
                    <div class="line-content">
                        <p class="font-red" id="withdrawUserName"><%=user.getUserName()%></p>
                    </div>
                </div>
                <div class="container">
                    <div class="line-title">
                        <p>余额:</p>
                    </div>
                    <div class="line-content">
                        <p class="font-red" id="withdrowMoney">加载中...</p>
                    </div>
                </div>
            </li>
            <!-- user-list over 普通文本 -->
            <!-- user-list 下拉框 -->
            <li class="user-line select">
                <div class="container select-container">
                    <div class="line-title"><img src="" alt=""></div>
                    <div class="line-content">
                        <input type="hidden" class="value" value="">
                        <input type="hidden" class="mun_value" value="">
                        <h2 class="title"></h2>
                        <p class="mun"></p>
                    </div>
                    <div class="right"><img class="icon" src="<%=path%>/images/icon/icon-pointer.png" /></div>
                </div>
                <div class="select-list" id="withdrawbanklistinfo">
                    <!-- <div class="container">
                        <div class="line-title"><img src="images/bankcard_logo/zhaoshang.png" alt=""></div>
                        <div class="line-content">
                            <h2 class="title" value="中国招商银行">中国招商银行</h2>
                            <p class="mun" value="＊＊＊＊＊＊＊＊＊＊2017">＊＊＊＊＊＊＊＊＊＊2017</p>
                        </div>
                    </div>
                    <div class="container">
                        <div class="line-title"><img src="images/bankcard_logo/zhongguo.png" alt=""></div>
                        <div class="line-content">
                            <h2 class="title" value="中国招商银行">中国银行</h2>
                            <p class="mun" value="＊＊＊＊＊＊＊＊＊＊2017">＊＊＊＊＊＊＊＊＊＊2017</p>
                        </div>
                    </div>
                    <div class="container">
                        <div class="line-title"><img src="images/bankcard_logo/huaxia.png" alt=""></div>
                        <div class="line-content">
                            <h2 class="title" value="中国招商银行">华夏银行</h2>
                            <p class="mun" value="＊＊＊＊＊＊＊＊＊＊2017">＊＊＊＊＊＊＊＊＊＊2017</p>
                        </div>
                    </div>
                    <div class="container">
                        <div class="line-title"><img src="images/bankcard_logo/pingan.png" alt=""></div>
                        <div class="line-content">
                            <h2 class="title" value="中国招商银行">中国平安银行</h2>
                            <p class="mun" value="＊＊＊＊＊＊＊＊＊＊2011">＊＊＊＊＊＊＊＊＊＊2011</p>

                        </div>
                    </div> -->

                </div>
            </li>
            <!-- user-list over 下拉框 -->
        </ul>


        <ul class="user-list">
            <!-- user-list 输入框 -->
            <li class="user-line">
                <div class="container">
                    <div class="line-title">
                        <p>提现金额</p>
                    </div>
                    <div class="line-content"><input class="inputText" id="withDrawValue" type="number" placeholder=""></div>
                </div>
            </li>
            <li class="user-line">
		<div class="container">
			<div class="line-title"><p>安全密码</p></div>
			<div class="line-content"><input id="withDrawPassword" class="inputText" type="password"></div>
		</div>
		</li>
            <!-- user-list over 输入框 -->
        </ul>

        <ul class="user-list no">
            <!-- user-list 提示信息 -->
            <li class="user-msg">
                <div class="container">
                   <p>提现最低<span id="withDrawOneStrokeLowestMoney"></span>元，单笔最高<span id="withDrawOneStrokeHighestMoney"></span>元，</p>
                    <p>一天最多的取现次数为<span id="withDrawEveryDayNumber"></span>次</p>
                </div>
            </li>
            <!-- user-list over 提示信息 -->
        </ul>

        <div class="user-btn color-red" id="withDrawButton">提交</div>

        <!-- nav-bar -->
        <jsp:include page="/front/include/navbar.jsp"></jsp:include>
        <!-- nav-bar over -->

       <jsp:include page="/front/include/include.jsp"></jsp:include>
        <script src="<%=path%>/js/withdraw/withdraw.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>

</body>

</html>