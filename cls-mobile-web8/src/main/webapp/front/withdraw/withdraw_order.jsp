<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>

<!DOCTYPE html>

<html>
<head>
	<meta charset="utf-8">
    <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="format-detection" content="telephone=no">
	<title>提现记录</title>
	<link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
	<link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
	<link href="<%=path%>/css/user_withdraw_order.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
</head>
<body>
	<!-- header -->
	<div class="header color-dark">
		<div class="header-left" onclick="window.history.go(-1);"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
		<h2 class="header-title">提现记录</h2>
		<div class="header-right" onclick="show_classify('.lottery-classify1',this)"><span>今天</span><img class="icon" src="<%=path%>/images/icon/icon-pointer-white.png"></div>
	</div>
	<div class="lottery-classify lottery-classify1 animated">
		<div class="container">
			<div class="move-content">
				<div class="child color-red">
					<p data-value="today">今天</p>
				</div>
				<div class="child color-red">
					<p data-value="yesterDay">昨天</p>
				</div>
				<div class="child color-red">
					<p data-value="month">本月</p>
				</div>
				<div class="child color-red">
					<p data-value="lastmonth">上个月</p>
				</div>
			</div>
		</div>
	</div>
	<div class="alert-bg" onclick="element_toggle(false,['.lottery-classify','.alert'])"></div>
	<!-- header over -->
	<div class="stance"></div>

	<!-- main -->
	<div class="main">
		<a>
			<div class="titles">

				<div class="child child1">
					<p>交易流水号</p>
				</div>
				<div class="child child2">
					<p>申请金额</p>
				</div>
				<!-- <div class="child child3"><p>到账金额</p></div> -->
				<div class="child child4">
					<p>交易时间</p>
				</div>
				<div class="child child5">
					<p>状态</p>
				</div>

			</div>
		</a>
		<div  id="userWithdrawOrderList">
			<!-- <div class="child child1">
				<p>1000000</p>
			</div>
			<div class="child child2">
				<p>24.000</p>
			</div>
			<div class="child child3"><p>24.000</p></div>
			<div class="child child4">
				<p>2016/09/27 11:16:33</p>
			</div>
			<div class="child child5">
				<p>进行中</p>
			</div> -->
		</div>



	</div>
	<!-- main over -->


	<!-- nav-bar -->
	<jsp:include page="/front/include/navbar.jsp"></jsp:include>
	<!-- nav-bar over -->
    <jsp:include page="/front/include/include.jsp"></jsp:include>
    <script type="text/javascript" src="<%=path%>/js/withdraw/withdraw_order.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>

</body>

</html>