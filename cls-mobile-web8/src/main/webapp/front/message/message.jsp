<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="format-detection" content="telephone=no">
    <title>私信</title>
    <link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/message.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
</head>
<body>

<!-- header -->
<div class="header color-dark">
    <div class="header-left" onclick="window.history.go(-1);"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
    <div class="header-title" id="header-title">
        <div class="header-title-content">
            <a class="header-title-child header-title-child1" href ="<%=path%>/gameBet/announce.html"><div>公告</div></a>
            <div class="header-title-child header-title-child2 on">私信</div>
        </div>
    </div>
</div>
<!-- header over -->




<div class="stance"></div>

<!-- main -->
<div class="main">
    <ul id="noteList">
<%--         <a href="message_detail.html">
            <li class="main-child">
                <div class="container">
                    <div class="child-content">
                        <h2 class="title">时时彩欢乐大放送</h2>
                        <div class="info">
                            <p>2016-09-03 12:35:12</p>
                        </div>
                    </div>
                    <img class="icon pointer" src="<%=path%>/images/icon/icon-pointer.png">
                </div>
            </li>
        </a>
        <a href="message_detail.html">
            <li class="main-child">
                <div class="container">
                    <div class="child-content">
                        <h2 class="title">时时彩欢乐大放送</h2>
                        <div class="info">
                            <p>2016-09-03 12:35:12</p>
                        </div>
                    </div>
                    <img class="icon pointer" src="<%=path%>/images/icon/icon-pointer.png">
                </div>
            </li>
        </a>
        <a href="message_detail.html">
            <li class="main-child">
                <div class="container">
                    <div class="child-content">
                        <h2 class="title">时时彩欢乐大放送</h2>
                        <div class="info">
                            <p>2016-09-03 12:35:12</p>
                        </div>
                    </div>
                    <img class="icon pointer" src="<%=path%>/images/icon/icon-pointer.png">
                </div>
            </li>
        </a>
        <a href="message_detail.html">
            <li class="main-child">
                <div class="container">
                    <div class="child-content">
                        <h2 class="title">时时彩欢乐大放送</h2>
                        <div class="info">
                            <p>2016-09-03 12:35:12</p>
                        </div>
                    </div>
                    <img class="icon pointer" src="<%=path%>/images/icon/icon-pointer.png">
                </div>
            </li>
        </a>
        <a href="message_detail.html">
            <li class="main-child">
                <div class="container">
                    <div class="child-content">
                        <h2 class="title">时时彩欢乐大放送</h2>
                        <div class="info">
                            <p>2016-09-03 12:35:12</p>
                        </div>
                    </div>
                    <img class="icon pointer" src="<%=path%>/images/icon/icon-pointer.png">
                </div>
            </li>
        </a> --%>

    </ul>
</div>
<!-- main over -->
<!-- nav-bar -->
<jsp:include page="/front/include/navbar.jsp"></jsp:include>
<!-- nav-bar over -->
<jsp:include page="/front/include/include.jsp"></jsp:include>

<script src="<%=path%>/js/message/message.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
</body>
</html>