<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant,com.team.lottery.system.BizSystemConfigVO,com.team.lottery.system.BizSystemConfigManager,
         com.team.lottery.vo.LotterySet,com.team.lottery.cache.ClsCacheManager,
         com.team.lottery.system.BizSystemConfigManager,com.team.lottery.vo.BizSystemInfo,
         com.team.lottery.util.LotterySetUntil,com.team.lottery.extvo.LotterySetMsg"
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();

	String serverName=  request.getServerName();//获取当前域名
	String bizSystem=ClsCacheManager.getBizSystemByDomainUrl(serverName);
	BizSystemConfigVO bizSystemConfigVO=BizSystemConfigManager.getBizSystemConfig(bizSystem);//业务系统彩种设置
	LotterySet lotterySet=new LotterySet();
	LotterySetUntil lotterySetUntil=new LotterySetUntil();
	//全部彩种的
	LotterySetMsg lotterySetMsgAll=lotterySetUntil.compareLotterySetAndBizSystemConfigVO(lotterySet, bizSystemConfigVO);
	request.setAttribute("lotterySetMsgAll", lotterySetMsgAll);
	if((lotterySet.getIsJYKSOpen() != null && lotterySet.getIsJYKSOpen() == 0)||(bizSystemConfigVO.getIsJYKSOpen() != null && bizSystemConfigVO.getIsJYKSOpen() == 0)){
	    response.sendRedirect(path+"/gameBet/index.html");
	  }
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="format-detection" content="telephone=no">
    <title>幸运快三</title>
    <link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/lottery_base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/lottery_kuaisan.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/lottery_additional.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
</head>
<body>

<!-- header -->
<div class="header color-dark lottery">
    <div class="header-left" onclick="window.location.href='<%=path%>/gameBet/hall.html'"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
    <div class="header-title"  id="currentKindDiv"><span id="currentKindName">和值大小单双</span><img class="icon" src="<%=path%>/images/lottery/pointer.png"></div>
    <div class="header-right"  id="otherLotteryKind"><span>幸运</span><img class="icon" src="<%=path%>/images/icon/icon-pointer-white.png"></div>
</div>
<!-- header over -->
<!-- 各地的快三选择 -->
<div class="lottery-classify lottery-classify1 animated">
    <div class="container">
        <div class="move-content">
	         <c:if test="${lotterySetMsgAll.isJSKSOpen==1}">
	            <a href="<%=path%>/gameBet/lottery/jsks/jsks.html">
	                <div class="child color-red">
	                    <p>江苏快三</p>
	                </div>
	            </a>
	         </c:if>
			 <c:if test="${lotterySetMsgAll.isBJKSOpen==1}">
	            <a href="<%=path%>/gameBet/lottery/bjks/bjks.html">
	                <div class="child color-red">
	                    <p>北京快三</p>
	                </div>
	            </a>
	         </c:if>
	         <c:if test="${lotterySetMsgAll.isHBKSOpen==1}">
	            <a href="<%=path%>/gameBet/lottery/hbks/hbks.html">
	                <div class="child color-red">
	                    <p>湖北快三</p>
	                </div>
	            </a>
	         </c:if>
<%-- 	         <c:if test="${lotterySetMsgAll.isJLKSOpen==1}">
	           <a href="<%=path%>/gameBet/lottery/jlks/jlks.html">
	                <div class="child color-red">
	                    <p>吉林快三</p>
	                </div>
	            </a>
	         </c:if> --%>
	         <c:if test="${lotterySetMsgAll.isAHKSOpen==1}">
	            <a href="<%=path%>/gameBet/lottery/ahks/ahks.html">
	                <div class="child color-red">
	                    <p>安徽快三</p>
	                </div>
	            </a>
	         </c:if>
        	 <c:if test="${lotterySetMsgAll.isGXKSOpen==1}">
	           <a href="<%=path%>/gameBet/lottery/gxks/gxks.html">
	                <div class="child color-red">
	                    <p>广西快三</p>
	                </div>
	            </a>
	         </c:if>
        </div>
    </div>
</div>

<!-- 各种玩法选择 -->
<div class="lottery-classify lottery-classify2 animated">
	<div class="left_but"></div>
    <div class="container slide-container" style="z-index:1;">
        <div class="move-content"  id="lotteryStarKindList">
       <!--      <div class="child" data-id="HZ">
                <p>和值</p>
            </div>
            <div class="child" data-id="DXDS">
                <p>大小单双</p>
            </div> -->
            <div class="child color-red" data-id="HZDXDS"><p>和值大小单双</p></div>
            <div class="child" data-id="STHTX">
                <p>三同号通选</p>
            </div>
            <div class="child" data-id="STHDX">
                <p>三同号单选</p>
            </div>
            <div class="child" data-id="SBTH">
                <p>三不同号</p>
            </div>
            <div class="child" data-id="SLHTX">
                <p>三连号通选</p>
            </div>
            <div class="child" data-id="ETHFX">
                <p>二同号复选</p>
            </div>
            <div class="child" data-id="ETHDX">
                <p>二同号单选</p>
            </div>
            <div class="child" data-id="EBTH">
                <p>二不同号</p>
            </div>
        </div>
    </div>
    <div class="right_but"></div>
    <div class="lottery-classify-list" id="lotteryKindPlayList">
	</div>
<!--     <div class="lottery-classify-list">

        <div class="container clist clist2 animated">
            <div class="list-line">
                <div class="line-title">
                    <p></p>
                </div>
                <div class="line-content">
                    <div class="child color-red no" data-role-id="HZ">
                        <p>和值</p>
                    </div>
                </div>
            </div>
        </div>

    </div> -->
</div>
<div class="alert-bg" onclick="element_toggle(false,['.lottery_kuaisan .lottery-classify','.alert'])"></div>
<!-- alert -->
<div class="alert alert-cart"  id="lotteryOrderDialog" style="display: none">
    <div class="alert-head color-dark">
        <p>投注确认</p>
        <i class="close ion-close-round" onclick="element_toggle(false,['.alert-cart'])"></i>
    </div>
    <div class="alert-content">
        <ul>
            <li class="user-line">
                <div class="line-title">
                    <p id="lotteryKindForOrder">XXX:</p>
                </div>
                <div class="line-content">
                    <p  id="lotteryExpectDesForOrder">第XXXXX期</p>
                </div>
            </li>
            <li class="user-line">
                <div class="line-title">
                    <p >投注金额:</p>
                </div>
                <div class="line-content">
                    <p class="font-red" id="lotteryTotalPriceForOrder">0元</p>
                </div>
            </li>
            <li class="user-line">
                <div class="line-title">
                    <p>投注内容:</p>
                </div>
                <div class="line-content">
                    <p id="lotteryDetailForOrder">1,2,3,4,5</p>
                </div>
            </li>

        </ul>
    </div>
    <div class="alert-foot">
        <div class="btn color-gray cols-50" id="lotteryOrderDialogCancelButton">取消</div>
        <div class="btn color-red cols-50" id="lotteryOrderDialogSureButton">确认投注</div>
    </div>
</div>
<!-- alert over -->

    <div class="stance"></div>

    <!-- main-head -->
    <div class="main-head main">
        <div class="container">
            <div class="sub"></div>
            <div class="child lottery_run">
                <div class="child-title">
                    <span class="font-red"  id="J-lottery-info-lastnumber">---</span>
                    <span>期</span><img class="icon" src="<%=path%>/images/lottery/pointer.png">
                </div>
                <div class="child-muns">
                    <img src="<%=path%>/images/dice_pic/gray/1.png" alt="" class="dice" id="lotteryNumber1">
                    <img src="<%=path%>/images/dice_pic/gray/3.png" alt="" class="dice" id="lotteryNumber2">
                    <img src="<%=path%>/images/dice_pic/gray/6.png" alt="" class="dice" id="lotteryNumber3">
                </div>
            </div>
            <div class="child">
                <div class="child-title">
                    <span>距 </span>
                    <span class="font-red" id="nowNumber">00000000000</span>
                    <span>期 截止</span>
                </div>
                <div class="child-muns">
                    <span class="mun font-red" id="timer">00:00</span>
                </div>
            </div>
        </div>
    </div>
    <!-- main-head over -->
    <!-- lottery-dynamic -->
    <div class="lottery-dynamic"  id="nearestTenLotteryCode">
      <%--   <div class="titles">
            <div class="child child1">
                <p>期号</p>
            </div>
            <div class="child child2">
                <p>开奖号</p>
            </div>
            <div class="child child3">
                <p>和值</p>
            </div>
            <div class="child child4">
                <p>形态</p>
            </div>
        </div>
        <div class="content">
            <div class="line">
                <div class="child child1">
                    <p>20161016082</p>
                </div>
                <div class="child child2 font-red">
                    <img class="dice" src="<%=path%>/images/dice_pic/gray/2.png" alt="">
                    <img class="dice" src="<%=path%>/images/dice_pic/gray/4.png" alt="">
                    <img class="dice" src="<%=path%>/images/dice_pic/gray/5.png" alt="">
                </div>
                <div class="child child3">
                    <p>12</p>
                </div>
                <div class="child child4">
                    <span class="child-big">大</span> | <span class="child-odd">单</span></div>
            </div>
            <div class="line">
                <div class="child child1">
                    <p>20161016082</p>
                </div>
                <div class="child child2 font-red">
                    <img class="dice" src="<%=path%>/images/dice_pic/gray/1.png" alt="">
                    <img class="dice" src="<%=path%>/images/dice_pic/gray/3.png" alt="">
                    <img class="dice" src="<%=path%>/images/dice_pic/gray/6.png" alt="">
                </div>
                <div class="child child3">
                    <p>5</p>
                </div>
                <div class="child child4">
                    <span class="child-small">小</span> | <span class="child-even">双</span></div>
            </div>


        </div> --%>
    </div>
    <!-- lottery-dynamic over -->

    <ul class="user-list reminder no">
        <!-- user-list 提示信息 -->
        <li class="user-msg">
            <div class="container">
                <p id="playDes">玩法说明</p>
            </div>
        </li>
        <!-- user-list over 提示信息 -->
    </ul>

    <!-- lottery-content -->
    <div class="lottery-content" id="ballSection">
<!-- 
        <div class="lottery-child no main">
            <div class="container">
                <div class="child-content" style="text-align:center;">
                    <div class="mun2">
                        <h2 class="value font-red">大</h2>
                        <p class="info">赔率1.1</p>
                    </div>
                    <div class="mun2">
                        <h2 class="value font-red">小</h2>
                        <p class="info">赔率1.1</p>
                    </div>
                    <div class="mun2">
                        <h2 class="value font-red">单</h2>
                        <p class="info">赔率1.1</p>
                    </div>
                    <div class="mun2">
                        <h2 class="value font-red">双</h2>
                        <p class="info">赔率1.1</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="lottery-child no main">
            <div class="container">
                <div class="child-content" style="text-align:center;">
                    <div class="mun2">
                        <h2 class="value">3</h2>
                        <p class="info">赔率1.1</p>
                    </div>
                    <div class="mun2">
                        <h2 class="value">4</h2>
                        <p class="info">赔率1.1</p>
                    </div>
                    <div class="mun2">
                        <h2 class="value">5</h2>
                        <p class="info">赔率1.1</p>
                    </div>
                    <div class="mun2">
                        <h2 class="value">6</h2>
                        <p class="info">赔率1.1</p>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
    <!-- lottery-content over -->


    <div class="stance-nav"></div>
    <div class="stance-nav"></div>


    <!-- modal record-modal 投注记录模态框 -->
    <div class="record-modal modal animated out">
        <!-- header -->
        <div class="header color-dark">
            <div class="header-left" onclick="modal_toggle('.record-modal')"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
            <div class="header-title"><span>投注记录</span></div>
        </div>
        <!-- header over -->
        <div class="stance"></div>


        <div class="main" id="userTodayLotteryList">

  <!--           <div class="line">
                <div class="child child1">
                    <p>[五星_复式]1,1,3,6,7</p>
                </div>
                <div class="child child2">
                    <p>¥2.0000</p>
                </div>
                <div class="child child3">
                    <p>1注</p>
                </div>
                <div class="child child4">
                    <p>1倍</p>
                </div>
                <div class="child child5">
                    <p>194000.0000元</p>
                </div>
                <div class="child child6"><i class="ion-close-circled"></i></div>
            </div>
            <div class="line">
                <div class="child child1">
                    <p>[五星_复式]1,1,3,6,7</p>
                </div>
                <div class="child child2">
                    <p>¥2.0000</p>
                </div>
                <div class="child child3">
                    <p>1注</p>
                </div>
                <div class="child child4">
                    <p>1倍</p>
                </div>
                <div class="child child5">
                    <p>194000.0000元</p>
                </div>
                <div class="child child6"><i class="ion-close-circled"></i></div>
            </div> -->
        </div>

        <!-- main over -->

    </div>
    <!-- modal record-modal over -->



    <!-- modal additional-modal 追号模态框 -->
    <div class="additional-modal modal animated out" id="J-trace-panel">
        <!-- header -->
        <div class="header color-dark">
            <div class="header-left" onclick="modal_toggle('.additional-modal')"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
            <div class="header-title"><span>我要追号</span></div>
            <div class="header-right"><span>今天</span></div>
        </div>
        <!-- header over -->
        <div class="stance"></div>

        <!-- main -->
        <!-- user-list 单选框 -->
        <li class="user-line main">
            <div class="container">
                <div class="line-title">
                    <p>中奖后停止追号</p>
                </div>
                <div class="line-content">

                    <div class="right toggle" data-onclass="red" id="J-trace-iswintimesstop-btn">
                        <input type="hidden" id="J-trace-iswintimesstop" class="toggle-value" value="0">
                        <div class="value" value="0"></div>
                        <div class="value" value="1"></div>
                        <div class="ball"></div>
                    </div>

                </div>
            </div>
        </li>
        <!-- user-list over 单选框 -->
        <div class="main"  id="todayList" data-type="TODAY">
            <div class="titles">
                <div class="child child1">
                    <p><span>序号</span></p>
                </div>
                <div class="child child2">
                    <p>
                        <div class="checkbox" data-onclass="red">
                            <input type="hidden" class="checkbox-value" value="0">
                            <img class="icon" src="<%=path%>/images/icon/icon-right.png" />
                        </div>
                        <span>期数</span>
                        <input type="text" class="inputText" id="todayTraceExpectsInputCount" name="trace-expects-input-count" value="1">
                    </p>
                </div>
                <div class="child child3">
                    <p>
                        <input type="text" class="inputText" id="todayTraceExpectsBeishuInputCount" name='trace-expects-beishu-input' value="1">
                        <span>倍</span>
                    </p>
                </div>
                <div class="child child4">
                    <p><span>金额</span></p>
                </div>
                <div class="child child5">
                    <p><span>投注截止时间</span></p>
                </div>
            </div>
<%--             <div class="line">
                <div class="child child1">
                    <p><span>1</span></p>
                </div>
                <div class="child child2">
                    <p>
                        <div class="checkbox" data-onclass="red">
                            <input type="hidden" class="checkbox-value" value="0">
                            <img class="icon" src="<%=path%>/images/icon/icon-right.png" />
                        </div>
                        <span>160925-072</span>
                    </p>
                </div>
                <div class="child child3">
                    <p>
                        <input type="text" class="inputText" value="1">
                        <span>倍</span>
                    </p>
                </div>
                <div class="child child4">
                    <p><span>¥1132.00元</span></p>
                </div>
                <div class="child child5">
                    <p><span>2016/09/27 11:16:33</span></p>
                </div>
            </div> --%>
        </div>

        <!-- main over -->

    </div>
    <!-- modal additional-modal over -->


    <!-- lottery-bar -->
    <div class="lottery-bar lottery">

        <div class="additional">
            <div class="bg"></div>
            <div class="content">
                <div class="title">金额</div>
                <div class="multiple-select">
                    <div class="multiple-btn" id="beishuSub">－</div>
                    <div class="multiple-text"><input value="1"  id="beishu" type="tel" onfocus="onfocusFn(this);" onmouseup="onmouseupFn(event);"/></div>
                    <div class="multiple-btn" id="beishuAdd">＋</div>
                </div>
                <div class="additional-btn color-blue" id="zhuiCodeButton" style="display: none">我要追号</div>
            </div>
        </div>
        <div class="bar color-dark">
            <div class="clear no" id="clearAllCodeBtn"><span>清空</span></div>
            <div class="content" onclick="modal_toggle('.record-modal')">
                <div class="t1">已选 <span class="pour"  id="J-balls-statistics-lotteryNum">0</span> 注 <span class="money font-yellow" id="J-balls-statistics-amount">0.00</span> 元</div>
                <div class="t2"><span  id="J-balls-statistics-code">（投注列表）</span></div>
            </div>
            <div class="btn color-red" id="showLotteryMsgButton"><span>投注</span><img src="<%=path%>/images/icon/icon-shopping.png" alt="" class="icon"></div>
        </div>
    </div>
    <!-- lottery-bar over -->
    
    	
	<jsp:include page="/front/include/include.jsp"></jsp:include>
	
	<script type="text/javascript">
	var currentUserKsModel = currentUser.ksRebate;
	var ksLowestAwardModel = <%=SystemConfigConstant.KSLowestAwardModel %>;
	</script>
	
    <script src="<%=path%>/js/lottery/common/ks_data.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
    <script src="<%=path%>/js/lottery/common/lottery_common.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
    <script src="<%=path%>/js/lottery/jyks/jyks.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
</body>
</html>