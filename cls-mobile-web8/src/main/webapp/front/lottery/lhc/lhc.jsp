<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant,com.team.lottery.system.BizSystemConfigVO,com.team.lottery.system.BizSystemConfigManager,
         com.team.lottery.vo.LotterySet,com.team.lottery.cache.ClsCacheManager,
         com.team.lottery.system.BizSystemConfigManager,com.team.lottery.vo.BizSystemInfo,
         com.team.lottery.util.LotterySetUntil,com.team.lottery.extvo.LotterySetMsg"
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();

	String serverName=  request.getServerName();//获取当前域名
	String bizSystem=ClsCacheManager.getBizSystemByDomainUrl(serverName);
	BizSystemConfigVO bizSystemConfigVO=BizSystemConfigManager.getBizSystemConfig(bizSystem);//业务系统彩种设置
	LotterySet lotterySet=new LotterySet();
	LotterySetUntil lotterySetUntil=new LotterySetUntil();
	//全部彩种的
	LotterySetMsg lotterySetMsgAll=lotterySetUntil.compareLotterySetAndBizSystemConfigVO(lotterySet, bizSystemConfigVO);
	request.setAttribute("lotterySetMsgAll", lotterySetMsgAll);
	
	if((lotterySet.getIsLHCOpen() != null && lotterySet.getIsLHCOpen() == 0)||(bizSystemConfigVO.getIsLHCOpen() != null && bizSystemConfigVO.getIsLHCOpen() == 0)){
	    response.sendRedirect(path+"/gameBet/index.html");
	  }
%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
	<meta name="format-detection" content="telephone=no">
	<title>六合彩</title>

	<link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
	<link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
	<link href="<%=path%>/css/lottery_base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
	<link href="<%=path%>/css/lottery_liuhecai.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
	<link href="<%=path%>/css/lottery_additional.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
	
</head>
<body class="lottery_liuhecai">
<!-- header -->
<div class="header color-dark lottery">
	<div class="header-left" onclick="window.history.go(-1);"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
	<div class="header-title" onclick="lotteryLhcCommonPage.show_classify('.lottery-classify2',this)"><span id="currentKindName" >特码-特码A</span><img class="icon" src="<%=path%>/images/icon/icon-pointer-black.png"></div>
	<div class="header-right" onclick="lotteryLhcCommonPage.show_classify('.lottery-classify1',this)"><span>六合彩</span><img class="icon" src="<%=path%>/images/icon/icon-pointer-white.png"></div>
</div>
<!-- header over -->
<!-- 各地的时时彩选择 -->
<div class="lottery-classify lottery-classify1 animated" >
<div class="container">
	<div class="move-content">
		<!-- <a href="#"><div class="child color-red"><p>福彩3D</p></div></a> -->
		<c:if test="${lotterySetMsgAll.isJLFFCOpen==1}">
			<a href="<%=path%>/gameBet/lottery/xyffc/xyffc.html"><div class="child color-red"><p>幸运分分彩</p></div></a>
		</c:if>
		<c:if test="${lotterySetMsgAll.isCQSSCOpen==1}">
			<a href="<%=path%>/gameBet/lottery/cqssc/cqssc.html"><div class="child color-red"><p>重庆时时彩</p></div></a>
		</c:if>
		<c:if test="${lotterySetMsgAll.isFC3DDPCOpen==1}">
			<a href="<%=path%>/gameBet/lottery/fcsddpc/fcsddpc.html"><div class="child color-red"><p>福彩3D</p></div></a>
		</c:if>
		<!-- <a href="#"><div class="child color-red"><p>六合彩</p></div></a> -->
	</div>
</div>
</div>

<!-- 各种玩法选择 -->
<div class="lottery-classify lottery-classify2 animated" >
<div class="left_but"></div>
<div class="container slide-container" style="z-index:1;">
	<div class="move-content">
		<div class="child" onclick="lotteryLhcCommonPage.play_change('.clist1',this)"><p>特码</p></div>
		<div class="child" onclick="lotteryLhcCommonPage.play_change('.clist2',this)"><p>正码</p></div>
		<div class="child" onclick="lotteryLhcCommonPage.play_change('.clist3',this)"><p>正特码</p></div>
		<div class="child" onclick="lotteryLhcCommonPage.play_change('.clist4',this)"><p>正码1-6</p></div>
		<div class="child" onclick="lotteryLhcCommonPage.play_change('.clist8',this)"><p>特码生肖</p></div>
		 <div class="child" onclick="lotteryLhcCommonPage.play_change('.clist5',this)"><p>连码</p></div>
		<div class="child" onclick="lotteryLhcCommonPage.play_change('.clist6',this)"><p>半波</p></div>
		<div class="child" onclick="lotteryLhcCommonPage.play_change('.clist7',this)"><p>一肖/尾数</p></div>
		<div class="child" onclick="lotteryLhcCommonPage.play_change('.clist9',this)"><p>合肖</p></div>
		<div class="child" onclick="lotteryLhcCommonPage.play_change('.clist10',this)"><p>连肖</p></div>
		<div class="child" onclick="lotteryLhcCommonPage.play_change('.clist11',this)"><p>尾数连</p></div>
		<div class="child" onclick="lotteryLhcCommonPage.play_change('.clist12',this)"><p>全不中</p></div> 
	</div>
</div>
<div class="right_but"></div>
<div class="lottery-classify-list" >

 <div class="container clist clist1 animated">
	<div class="list-line">
		<div class="line-title"><p></p></div>
		<div class="line-content">
			<div class="child" data-role-id="TMA"><p>特A</p></div>
			<div class="child" data-role-id="TMB"><p>特B</p></div>
		</div>
	</div>
</div> 

<div class="container clist clist2 animated">
	<div class="list-line">
		<div class="line-title"><p></p></div>
		<div class="line-content">
			<div class="child" data-role-id="ZMA"><p>正码A</p></div>
			<div class="child" data-role-id="ZMB"><p>正码B</p></div>
		</div>
	</div>
</div>

<div class="container clist clist3 animated">
	<div class="list-line">
		<div class="line-title"><p></p></div>
		<div class="line-content">
			<div class="child" data-role-id="ZMT1"><p>正一特</p></div>
			<div class="child" data-role-id="ZMT2"><p>正二特</p></div>
			<div class="child" data-role-id="ZMT3"><p>正三特</p></div>
			<div class="child" data-role-id="ZMT4"><p>正四特</p></div>
			<div class="child" data-role-id="ZMT5"><p>正五特</p></div>
			<div class="child" data-role-id="ZMT6"><p>正六特</p></div>
		</div>
	</div>
</div>

<div class="container clist clist4 animated">
	<div class="list-line">
		<div class="line-title"><p></p></div>
		<div class="line-content">
			<div class="child" data-role-id="ZM16"><p>正码1-6</p></div>
		</div>
	</div>
</div>

<div class="container clist clist5 animated">
	<div class="list-line">
		<div class="line-title"><p></p></div>
		<div class="line-content">
			<div class="child" data-role-id="LMSQZ"><p>三全中</p></div>
			<div class="child" data-role-id="LMSZ22"><p>三中二之中二</p></div>
			<div class="child" data-role-id="LMSZ23"><p>三中二之中三</p></div>
			<div class="child" data-role-id="LMEQZ"><p>二全中</p></div>
			<div class="child" data-role-id="LMEZT"><p>二中特之中特</p></div>
			<div class="child" data-role-id="LMEZ2"><p>二中特之中二</p></div>
			<div class="child" data-role-id="LMTC"><p>特串</p></div>
			<div class="child" data-role-id="LMSZ1"><p>四中一</p></div>
		</div>
	</div>
</div>

<div class="container clist clist6 animated">
	<div class="list-line">
		<div class="line-title"><p></p></div>
		<div class="line-content">
			<div class="child" data-role-id="BB"><p>半波</p></div>
		</div>
	</div>
</div>

<div class="container clist clist7 animated">
	<div class="list-line">
		<div class="line-title"><p></p></div>
		<div class="line-content">
			<div class="child" data-role-id="YXWS"><p>一肖/尾数</p></div>
		</div>
	</div>
</div>
<div class="container clist clist8 animated">
	<div class="list-line">
		<div class="line-title"><p></p></div>
		<div class="line-content">
			<div class="child" data-role-id="TMSX"><p>特码生肖</p></div>
		</div>
	</div>
</div>

<div class="container clist clist9 animated">
	<div class="list-line">
		<div class="line-title"><p></p></div>
		<div class="line-content">
			<div class="child" data-role-id="HX2"><p>二肖</p></div>
			<div class="child" data-role-id="HX3"><p>三肖</p></div>
			<div class="child" data-role-id="HX4"><p>四肖</p></div>
			<div class="child" data-role-id="HX5"><p>五肖</p></div>
			<div class="child" data-role-id="HX6"><p>六肖</p></div>
			<div class="child" data-role-id="HX7"><p>七肖</p></div>
			<div class="child" data-role-id="HX8"><p>八肖</p></div>
			<div class="child" data-role-id="HX9"><p>九肖</p></div>
			<div class="child" data-role-id="HX10"><p>十肖</p></div>
			<div class="child" data-role-id="HX11"><p>十一肖</p></div>
		</div>
	</div>
</div>

<div class="container clist clist10 animated">
	<div class="list-line">
		<div class="line-title"><p></p></div>
		<div class="line-content">
			<div class="child" data-role-id="LXELZ"><p>二肖连中</p></div>
			<div class="child" data-role-id="LXSLZ"><p>三肖连中</p></div>
			<div class="child" data-role-id="LXSILZ"><p>四肖连中</p></div>
			<div class="child" data-role-id="LXWLZ"><p>五肖连中</p></div>
			<div class="child" data-role-id="LXELBZ"><p>二肖连不中</p></div>
			<div class="child" data-role-id="LXSLBZ"><p>三肖连不中</p></div>
			<div class="child" data-role-id="LXSILBZ"><p>四肖连不中</p></div>
		</div>
	</div>
</div>

<div class="container clist clist11 animated">
	<div class="list-line">
		<div class="line-title"><p></p></div>
		<div class="line-content">
			<div class="child" data-role-id="WSLELZ"><p>二尾连中</p></div>
			<div class="child" data-role-id="WSLSLZ"><p>三尾连中</p></div>
			<div class="child" data-role-id="WSLSILZ"><p>四尾连中</p></div>
			<div class="child" data-role-id="WSLELBZ"><p>二尾连不中</p></div>
			<div class="child" data-role-id="WSLSLBZ"><p>三尾连不中</p></div>
			<div class="child" data-role-id="WSLSILBZ"><p>四尾连不中</p></div>
		</div>
	</div>
</div>


<div class="container clist clist12 animated">
	<div class="list-line">
		<div class="line-title"><p></p></div>
		<div class="line-content">
			<div class="child" data-role-id="QBZ5"><p>五不中</p></div>
			<div class="child" data-role-id="QBZ6"><p>六不中</p></div>
			<div class="child" data-role-id="QBZ7"><p>七不中</p></div>
			<div class="child" data-role-id="QBZ8"><p>八不中</p></div>
			<div class="child" data-role-id="QBZ9"><p>九不中</p></div>
			<div class="child" data-role-id="QBZ10"><p>十不中</p></div>
			<div class="child" data-role-id="QBZ11"><p>十一不中</p></div>
			<div class="child" data-role-id="QBZ12"><p>十二不中</p></div>
		</div>
	</div>
</div>

</div>
</div>




<div class="alert-bg" onclick="element_toggle(false,['.lottery_liuheicai .lottery-classify','.alert'])"></div>
<!-- alert -->
 <div class="alert alert-cart" >
	<div class="alert-head color-dark">
	<p>投注确认</p>
	<i class="close ion-close-round" onclick="element_toggle(false,['.alert-cart'])"></i>
	</div>
	<div class="alert-content" id="liuhecai_msg">

		<div class="msg-list" >
        <ul>
            <li class="msg-list-li msg-list-t">
                <div class="child child1">序号</div>
                <div class="child child2">内容</div>
                <div class="child child3">赔率</div>
                <div class="child child4">下注金额</div>
            </li>
        </ul>
    </div>
		<ul>
			<li class="user-line">
				<div class="line-title"><p>期数:</p></div>
				<div class="line-content"><p>0</p></div>
			</li>
			<li class="user-line">
				<div class="line-title"><p>盘口:</p></div>
				<div class="line-content"><p>0</p></div>
			</li>
			<li class="user-line">
				<div class="line-title"><p>总下注额:</p></div>
				<div class="line-content"><p class="font-red">0元</p></div>
			</li>
		</ul>
	</div>
	<div class="alert-foot">
		<div class="btn color-gray cols-50" onclick="element_toggle(false,['.alert-cart'])">取消</div>
		<div class="btn color-red cols-50" onclick="lotteryLhcCommonPage.userLottery()">确认投注</div>
	</div>
</div>
<!-- alert over -->
<div class="layout-scroll">
<div class="cont">
	<div class="stance"></div>

	<!-- main-head -->
	<div class="main-head main">
	<div class="container m0-10">
		<div class="sub"></div>
		<div class="child lottery_run">
			<div class="child-title">
				<span class="font-blue" id="J-lottery-info-lastnumber">00000000000</span>
				<span>期</span><img class="icon" src="<%=path%>/images/icon/icon-pointer-black.png">
			</div>
			<div class="child-muns">
				<div class="lhc-mun-item"><div class="lhc-mun lhc-red small" id="lotteryNumber1" >0</div><p class="btn-title">-</p></div>
				<div class="lhc-mun-item"><div class="lhc-mun lhc-green small" id="lotteryNumber2" >0</div><p class="btn-title">-</p></div>
				<div class="lhc-mun-item"><div class="lhc-mun lhc-red small" id="lotteryNumber3" >0</div><p class="btn-title">-</p></div>
				<div class="lhc-mun-item"><div class="lhc-mun lhc-blue small" id="lotteryNumber4" >0</div><p class="btn-title">-</p></div>
				<div class="lhc-mun-item"><div class="lhc-mun lhc-green small" id="lotteryNumber5" >0</div><p class="btn-title">-</p></div>
				<div class="lhc-mun-item"><div class="lhc-mun lhc-red small" id="lotteryNumber6" >0</div><p class="btn-title">-</p></div>
				<div class="lhc-mun-item"><div class="lhc-mun black" >+</div><p class="btn-title">&nbsp;</p></div>
				<div class="lhc-mun-item"><div class="lhc-mun lhc-yellow small" id="lotteryNumber7" >0</div><p class="btn-title">-</p></div>
			</div>
		</div>
		<div class="child">
			<div class="child-title">
				<span>距 </span>
				<span class="font-blue" id="nowNumber">00000000000</span>
				<span>期 截止</span>
			</div>
			<div class="child-muns">
				<span class="mun font-blue" id="timer">00:00:00</span>
			</div>
		</div>
	</div>
	</div>
	<!-- main-head over -->
    <!-- lottery-dynamic -->
	<div class="lottery-dynamic" id="nearestTenLotteryCode">
	</div>
	<!-- lottery-dynamic over -->

	<!-- lottery-content -->
	<div class="lottery-content lottery-content-lhc" id="lhcContent">






		<div class="content-lhc">




		<div class="cols cols-25 ball-1">
			<div class="lhc-mun lhc-red">01</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-2">
			<div class="lhc-mun lhc-red">02</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-3">
			<div class="lhc-mun lhc-blue">03</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-4">
			<div class="lhc-mun lhc-blue">04</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-5">
			<div class="lhc-mun lhc-green">05</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-6">
			<div class="lhc-mun lhc-green">06</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-7">
			<div class="lhc-mun lhc-red">07</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-8">
			<div class="lhc-mun lhc-red">08</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-9">
			<div class="lhc-mun lhc-blue">09</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-10">
			<div class="lhc-mun lhc-blue">10</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-11">
			<div class="lhc-mun lhc-green">11</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-12">
			<div class="lhc-mun lhc-red">12</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-13">
			<div class="lhc-mun lhc-green">13</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-14">
			<div class="lhc-mun lhc-green">14</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-15">
			<div class="lhc-mun lhc-red">15</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-16">
			<div class="lhc-mun lhc-red">16</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-17">
			<div class="lhc-mun lhc-blue">17</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-18">
			<div class="lhc-mun lhc-blue">18</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-19">
			<div class="lhc-mun lhc-green">19</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-20">
			<div class="lhc-mun lhc-red">20</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-21">
			<div class="lhc-mun lhc-red">21</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-22">
			<div class="lhc-mun lhc-red">22</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-23">
			<div class="lhc-mun lhc-blue">23</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-24">
			<div class="lhc-mun lhc-blue">24</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-25">
			<div class="lhc-mun lhc-green">25</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-26">
			<div class="lhc-mun lhc-green">26</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-27">
			<div class="lhc-mun lhc-red">27</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-28">
			<div class="lhc-mun lhc-red">28</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-29">
			<div class="lhc-mun lhc-blue">29</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-30">
			<div class="lhc-mun lhc-blue">30</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-31">
			<div class="lhc-mun lhc-green">31</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-32">
			<div class="lhc-mun lhc-red">32</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-33">
			<div class="lhc-mun lhc-green">33</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-34">
			<div class="lhc-mun lhc-green">34</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-35">
			<div class="lhc-mun lhc-red">35</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-36">
			<div class="lhc-mun lhc-red">36</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-37">
			<div class="lhc-mun lhc-blue">37</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-38">
			<div class="lhc-mun lhc-blue">38</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-39">
			<div class="lhc-mun lhc-green">39</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-40">
			<div class="lhc-mun lhc-red">40</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-41">
			<div class="lhc-mun lhc-green">41</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-42">
			<div class="lhc-mun lhc-green">42</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-43">
			<div class="lhc-mun lhc-red">43</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-44">
			<div class="lhc-mun lhc-red">44</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-45">
			<div class="lhc-mun lhc-blue">45</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-46">
			<div class="lhc-mun lhc-blue">46</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-47">
			<div class="lhc-mun lhc-green">47</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-48">
			<div class="lhc-mun lhc-red">48</div>
			<span>00.0</span>
		</div>
		<div class="cols cols-25 ball-49">
			<div class="lhc-mun lhc-green">49</div>
			<span>00.0</span>
		</div>


		</div>



		<div class="select-lhc">
			<div class="cols cols-20" data-type="1-10" onclick="select_type('.lottery_liuhecai',this)">
				<p class="mun">1-10</p>
				<p class="mun2">00.0</p>
			</div>
			<div class="cols cols-20" data-type="11-20" onclick="select_type('.lottery_liuhecai',this)">
				<p class="mun">11-20</p>
				<p class="mun2">00.0</p>
			</div>
			<div class="cols cols-20" data-type="21-30" onclick="select_type('.lottery_liuhecai',this)">
				<p class="mun">21-30</p>
				<p class="mun2">00.0</p>
			</div>
			<div class="cols cols-20" data-type="31-40" onclick="select_type('.lottery_liuhecai',this)">
				<p class="mun">31-40</p>
				<p class="mun2">00.0</p>
			</div>
			<div class="cols cols-20" data-type="41-49" onclick="select_type('.lottery_liuhecai',this)">
				<p class="mun">41-49</p>
				<p class="mun2">00.0</p>
			</div>
			<div class="cols cols-25" data-type="dan" onclick="select_type('.lottery_liuhecai',this)">
				<p class="mun">单</p>
				<p class="mun2">00.0</p>
			</div>
			<div class="cols cols-25" data-type="shuang" onclick="select_type('.lottery_liuhecai',this)">
				<p class="mun">双</p>
				<p class="mun2">00.0</p>
			</div>
			<div class="cols cols-25" data-type="da" onclick="select_type('.lottery_liuhecai',this)">
				<p class="mun">大</p>
				<p class="mun2">00.0</p>
			</div>
			<div class="cols cols-25" data-type="xiao" onclick="select_type('.lottery_liuhecai',this)">
				<p class="mun">小</p>
				<p class="mun2">00.0</p>
			</div>
			<div class="cols cols-25" data-type="hedan" onclick="select_type('.lottery_liuhecai',this)">
				<p class="mun">合单</p>
				<p class="mun2">00.0</p>
			</div>
			<div class="cols cols-25" data-type="heshuang" onclick="select_type('.lottery_liuhecai',this)">
				<p class="mun">合双</p>
				<p class="mun2">00.0</p>
			</div>
			<div class="cols cols-25" data-type="jiaqin" onclick="select_type('.lottery_liuhecai',this)">
				<p class="mun">家禽</p>
				<p class="mun2">00.0</p>
			</div>
			<div class="cols cols-25" data-type="yeshou" onclick="select_type('.lottery_liuhecai',this)">
				<p class="mun">野兽</p>
				<p class="mun2">00.0</p>
			</div>
			<div class="cols cols-33" data-type="hong" onclick="select_type('.lottery_liuhecai',this)">
				<p class="mun">红波</p>
				<p class="mun2">00.0</p>
			</div>
			<div class="cols cols-33" data-type="lv" onclick="select_type('.lottery_liuhecai',this)">
				<p class="mun">绿波</p>
				<p class="mun2">00.0</p>
			</div>
			<div class="cols cols-34" data-type="lan" onclick="select_type('.lottery_liuhecai',this)">
				<p class="mun">蓝波</p>
				<p class="mun2">00.0</p>
			</div>
		</div>

	</div>




	<!-- <div class="lottery-content lottery-content-lhc">






		<div class="content-lhc">

			<div class="cols cols-25 ball-1">
				<div class="lhc-mun lhc-red">01</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-2">
				<div class="lhc-mun lhc-red">02</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-3">
				<div class="lhc-mun lhc-blue">03</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-4">
				<div class="lhc-mun lhc-blue">04</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-5">
				<div class="lhc-mun lhc-green">05</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-6">
				<div class="lhc-mun lhc-green">06</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-7">
				<div class="lhc-mun lhc-red">07</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-8">
				<div class="lhc-mun lhc-red">08</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-9">
				<div class="lhc-mun lhc-blue">09</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-10">
				<div class="lhc-mun lhc-blue">10</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-11">
				<div class="lhc-mun lhc-green">11</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-12">
				<div class="lhc-mun lhc-red">12</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-13">
				<div class="lhc-mun lhc-green">13</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-14">
				<div class="lhc-mun lhc-green">14</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-15">
				<div class="lhc-mun lhc-red">15</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-16">
				<div class="lhc-mun lhc-red">16</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-17">
				<div class="lhc-mun lhc-blue">17</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-18">
				<div class="lhc-mun lhc-blue">18</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-19">
				<div class="lhc-mun lhc-green">19</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-20">
				<div class="lhc-mun lhc-red">20</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-21">
				<div class="lhc-mun lhc-red">21</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-22">
				<div class="lhc-mun lhc-red">22</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-23">
				<div class="lhc-mun lhc-blue">23</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-24">
				<div class="lhc-mun lhc-blue">24</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-25">
				<div class="lhc-mun lhc-green">25</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-26">
				<div class="lhc-mun lhc-green">26</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-27">
				<div class="lhc-mun lhc-red">27</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-28">
				<div class="lhc-mun lhc-red">28</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-29">
				<div class="lhc-mun lhc-blue">29</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-30">
				<div class="lhc-mun lhc-blue">30</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-31">
				<div class="lhc-mun lhc-green">31</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-32">
				<div class="lhc-mun lhc-red">32</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-33">
				<div class="lhc-mun lhc-green">33</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-34">
				<div class="lhc-mun lhc-green">34</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-35">
				<div class="lhc-mun lhc-red">35</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-36">
				<div class="lhc-mun lhc-red">36</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-37">
				<div class="lhc-mun lhc-blue">37</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-38">
				<div class="lhc-mun lhc-blue">38</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-39">
				<div class="lhc-mun lhc-green">39</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-40">
				<div class="lhc-mun lhc-red">40</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-41">
				<div class="lhc-mun lhc-green">41</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-42">
				<div class="lhc-mun lhc-green">42</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-43">
				<div class="lhc-mun lhc-red">43</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-44">
				<div class="lhc-mun lhc-red">44</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-45">
				<div class="lhc-mun lhc-blue">45</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-46">
				<div class="lhc-mun lhc-blue">46</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-47">
				<div class="lhc-mun lhc-green">47</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-48">
				<div class="lhc-mun lhc-red">48</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-49">
				<div class="lhc-mun lhc-green">49</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 cols-no"></div>
			<div class="cols cols-25 cols-no"></div>
			<div class="cols cols-25 cols-no"></div>

			<div class="cols cols-25 cols-center">
				<span>大</span>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 cols-center">
				<span>小</span>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 cols-center">
				<span>单</span>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 cols-center">
				<span>双</span>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 cols-center">
				<span>合单</span>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 cols-center">
				<span>合双</span>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 cols-center">
				<span>大单</span>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 cols-center">
				<span>小单</span>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 cols-center">
				<span>大双</span>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 cols-center">
				<span>小双</span>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 cols-center">
				<span>红波</span>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 cols-center">
				<span>蓝波</span>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 cols-center">
				<span>绿波</span>
				<span>00.0</span>
			</div>



		</div>


	</div> -->






	<!-- <div class="lottery-content lottery-content-lhc">






		<div class="content-lhc">

		<div class="cols cols-10 cols-title">
			<span>...</span>
		</div>
		<div class="cols cols-15 cols-title">
			<span>正码一</span>
		</div>
		<div class="cols cols-15 cols-title">
			<span>正码二</span>
		</div>
		<div class="cols cols-15 cols-title">
			<span>正码三</span>
		</div>
		<div class="cols cols-15 cols-title">
			<span>正码四</span>
		</div>
		<div class="cols cols-15 cols-title">
			<span>正码五</span>
		</div>
		<div class="cols cols-15 cols-title">
			<span>正码六</span>
		</div>

		<div class="cols cols-10 cols-title">
			<span>单</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>

		<div class="cols cols-10 cols-title">
			<span>双</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>


		<div class="cols cols-10 cols-title">
			<span>大</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>

		<div class="cols cols-10 cols-title">
			<span>小</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>


		<div class="cols cols-10 cols-title lhc-red">
			<span>红波</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>

		<div class="cols cols-10 cols-title lhc-blue">
			<span>蓝波</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>

		<div class="cols cols-10 cols-title lhc-green">
			<span>绿波</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>

		<div class="cols cols-10 cols-title">
			<span>合大</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>

		<div class="cols cols-10 cols-title">
			<span>合小</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>

		<div class="cols cols-10 cols-title">
			<span>合单</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>


		<div class="cols cols-10 cols-title">
			<span>合双</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>


		<div class="cols cols-10 cols-title">
			<span>尾大</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>

		<div class="cols cols-10 cols-title">
			<span>尾小</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>
		<div class="cols cols-15">
			<span>00.0</span>
		</div>



		</div>


	</div> -->





	<!-- <div class="lottery-content lottery-content-lhc">






		<div class="content-lhc">

			<div class="cols cols-25 ball-1">
				<div class="lhc-mun lhc-red">01</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-2">
				<div class="lhc-mun lhc-red">02</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-3">
				<div class="lhc-mun lhc-blue">03</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-4">
				<div class="lhc-mun lhc-blue">04</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-5">
				<div class="lhc-mun lhc-green">05</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-6">
				<div class="lhc-mun lhc-green">06</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-7">
				<div class="lhc-mun lhc-red">07</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-8">
				<div class="lhc-mun lhc-red">08</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-9">
				<div class="lhc-mun lhc-blue">09</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-10">
				<div class="lhc-mun lhc-blue">10</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-11">
				<div class="lhc-mun lhc-green">11</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-12">
				<div class="lhc-mun lhc-red">12</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-13">
				<div class="lhc-mun lhc-green">13</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-14">
				<div class="lhc-mun lhc-green">14</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-15">
				<div class="lhc-mun lhc-red">15</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-16">
				<div class="lhc-mun lhc-red">16</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-17">
				<div class="lhc-mun lhc-blue">17</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-18">
				<div class="lhc-mun lhc-blue">18</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-19">
				<div class="lhc-mun lhc-green">19</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-20">
				<div class="lhc-mun lhc-red">20</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-21">
				<div class="lhc-mun lhc-red">21</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-22">
				<div class="lhc-mun lhc-red">22</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-23">
				<div class="lhc-mun lhc-blue">23</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-24">
				<div class="lhc-mun lhc-blue">24</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-25">
				<div class="lhc-mun lhc-green">25</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-26">
				<div class="lhc-mun lhc-green">26</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-27">
				<div class="lhc-mun lhc-red">27</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-28">
				<div class="lhc-mun lhc-red">28</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-29">
				<div class="lhc-mun lhc-blue">29</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-30">
				<div class="lhc-mun lhc-blue">30</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-31">
				<div class="lhc-mun lhc-green">31</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-32">
				<div class="lhc-mun lhc-red">32</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-33">
				<div class="lhc-mun lhc-green">33</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-34">
				<div class="lhc-mun lhc-green">34</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-35">
				<div class="lhc-mun lhc-red">35</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-36">
				<div class="lhc-mun lhc-red">36</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-37">
				<div class="lhc-mun lhc-blue">37</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-38">
				<div class="lhc-mun lhc-blue">38</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-39">
				<div class="lhc-mun lhc-green">39</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-40">
				<div class="lhc-mun lhc-red">40</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-41">
				<div class="lhc-mun lhc-green">41</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-42">
				<div class="lhc-mun lhc-green">42</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-43">
				<div class="lhc-mun lhc-red">43</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-44">
				<div class="lhc-mun lhc-red">44</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-45">
				<div class="lhc-mun lhc-blue">45</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-46">
				<div class="lhc-mun lhc-blue">46</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-47">
				<div class="lhc-mun lhc-green">47</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-48">
				<div class="lhc-mun lhc-red">48</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 ball-49">
				<div class="lhc-mun lhc-green">49</div>
				<span>00.0</span>
			</div>
			<div class="cols cols-25"></div>
			<div class="cols cols-25"></div>
			<div class="cols cols-25"></div>

			<div class="cols cols-25 cols-center">
				<span>大</span>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 cols-center">
				<span>小</span>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 cols-center">
				<span>单</span>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 cols-center">
				<span>双</span>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 cols-center">
				<span>合单</span>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 cols-center">
				<span>合双</span>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 cols-center">
				<span>大单</span>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 cols-center">
				<span>小单</span>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 cols-center">
				<span>大双</span>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 cols-center">
				<span>小双</span>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 cols-center">
				<span>红波</span>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 cols-center">
				<span>蓝波</span>
				<span>00.0</span>
			</div>
			<div class="cols cols-25 cols-center">
				<span>绿波</span>
				<span>00.0</span>
			</div>



		</div>


	</div> -->







	<!-- <div class="lottery-content lottery-content-lhc">






		<div class="content-lhc">

		<div class="cols cols-15 cols-title">
			<span>红单</span>
			<span>00.0</span>
		</div>
		<div class="cols cols-85 cols-balls cols-center">
			<div class="lhc-mun lhc-red">01</div>
			<div class="lhc-mun lhc-red">01</div>
			<div class="lhc-mun lhc-red">01</div>
			<div class="lhc-mun lhc-red">01</div>
			<div class="lhc-mun lhc-red">01</div>
			<div class="lhc-mun lhc-red">01</div>
			<div class="lhc-mun lhc-red">01</div>
			<div class="lhc-mun lhc-red">01</div>
			<div class="lhc-mun lhc-red">01</div>
			<div class="lhc-mun lhc-red">01</div>
		</div>



		<div class="cols cols-15 cols-title">
			<span>绿双</span>
			<span>00.0</span>
		</div>
		<div class="cols cols-85 cols-balls cols-center">
			<div class="lhc-mun lhc-green">01</div>
			<div class="lhc-mun lhc-green">01</div>
			<div class="lhc-mun lhc-green">01</div>
			<div class="lhc-mun lhc-green">01</div>
			<div class="lhc-mun lhc-green">01</div>
			<div class="lhc-mun lhc-green">01</div>
			<div class="lhc-mun lhc-green">01</div>
			<div class="lhc-mun lhc-green">01</div>
			<div class="lhc-mun lhc-green">01</div>
			<div class="lhc-mun lhc-green">01</div>
		</div>



		<div class="cols cols-15 cols-title">
			<span>红单</span>
			<span>00.0</span>
		</div>
		<div class="cols cols-85 cols-balls cols-center">
			<div class="lhc-mun lhc-blue">01</div>
			<div class="lhc-mun lhc-blue">01</div>
			<div class="lhc-mun lhc-blue">01</div>
			<div class="lhc-mun lhc-blue">01</div>
			<div class="lhc-mun lhc-blue">01</div>
			<div class="lhc-mun lhc-blue">01</div>
			<div class="lhc-mun lhc-blue">01</div>
			<div class="lhc-mun lhc-blue">01</div>
			<div class="lhc-mun lhc-blue">01</div>
			<div class="lhc-mun lhc-blue">01</div>
		</div>




		</div>


	</div> -->





	<!-- <div class="lottery-content lottery-content-lhc">






		<div class="content-lhc">

		<div class="cols cols-15 cols-title">
			<span>鼠</span>
			<span>00.0</span>
		</div>
		<div class="cols cols-85 cols-balls cols-center">
			<div class="lhc-mun lhc-red">01</div>
			<div class="lhc-mun lhc-red">01</div>
			<div class="lhc-mun lhc-red">01</div>
			<div class="lhc-mun lhc-red">01</div>
			<div class="lhc-mun lhc-red">01</div>
			<div class="lhc-mun lhc-red">01</div>
			<div class="lhc-mun lhc-red">01</div>
			<div class="lhc-mun lhc-red">01</div>
			<div class="lhc-mun lhc-red">01</div>
			<div class="lhc-mun lhc-red">01</div>
		</div>



		<div class="cols cols-15 cols-title">
			<span>虎</span>
			<span>00.0</span>
		</div>
		<div class="cols cols-85 cols-balls cols-center">
			<div class="lhc-mun lhc-green">01</div>
			<div class="lhc-mun lhc-green">01</div>
			<div class="lhc-mun lhc-green">01</div>
			<div class="lhc-mun lhc-green">01</div>
			<div class="lhc-mun lhc-green">01</div>
			<div class="lhc-mun lhc-green">01</div>
			<div class="lhc-mun lhc-green">01</div>
			<div class="lhc-mun lhc-green">01</div>
			<div class="lhc-mun lhc-green">01</div>
			<div class="lhc-mun lhc-green">01</div>
		</div>



		<div class="cols cols-15 cols-title">
			<span>兔</span>
			<span>00.0</span>
		</div>
		<div class="cols cols-85 cols-balls cols-center">
			<div class="lhc-mun lhc-blue">01</div>
			<div class="lhc-mun lhc-blue">01</div>
			<div class="lhc-mun lhc-blue">01</div>
			<div class="lhc-mun lhc-blue">01</div>
			<div class="lhc-mun lhc-blue">01</div>
			<div class="lhc-mun lhc-blue">01</div>
			<div class="lhc-mun lhc-blue">01</div>
			<div class="lhc-mun lhc-blue">01</div>
			<div class="lhc-mun lhc-blue">01</div>
			<div class="lhc-mun lhc-blue">01</div>
		</div>


		<div class="cols cols-20 cols-center">
			<span>0尾</span>
			<span>00.0</span>
		</div>
		<div class="cols cols-20 cols-center">
			<span>0尾</span>
			<span>00.0</span>
		</div>
		<div class="cols cols-20 cols-center">
			<span>0尾</span>
			<span>00.0</span>
		</div>
		<div class="cols cols-20 cols-center">
			<span>0尾</span>
			<span>00.0</span>
		</div>
		<div class="cols cols-20 cols-center">
			<span>0尾</span>
			<span>00.0</span>
		</div>
		<div class="cols cols-20 cols-center">
			<span>0尾</span>
			<span>00.0</span>
		</div>
		<div class="cols cols-20 cols-center">
			<span>0尾</span>
			<span>00.0</span>
		</div>
		<div class="cols cols-20 cols-center">
			<span>0尾</span>
			<span>00.0</span>
		</div>
		<div class="cols cols-20 cols-center">
			<span>0尾</span>
			<span>00.0</span>
		</div>
		<div class="cols cols-20 cols-center">
			<span>0尾</span>
			<span>00.0</span>
		</div>





		</div> -->



		<!-- <div class="lottery-content lottery-content-lhc">






			<div class="content-lhc">

			<div class="cols cols-15 cols-title">
				<span>鼠</span>
				<span>00.0</span>
			</div>
			<div class="cols cols-85 cols-balls cols-center">
				<div class="lhc-mun lhc-red">01</div>
				<div class="lhc-mun lhc-red">01</div>
				<div class="lhc-mun lhc-red">01</div>
				<div class="lhc-mun lhc-red">01</div>
				<div class="lhc-mun lhc-red">01</div>
				<div class="lhc-mun lhc-red">01</div>
				<div class="lhc-mun lhc-red">01</div>
				<div class="lhc-mun lhc-red">01</div>
				<div class="lhc-mun lhc-red">01</div>
				<div class="lhc-mun lhc-red">01</div>
			</div>



			<div class="cols cols-15 cols-title">
				<span>虎</span>
				<span>00.0</span>
			</div>
			<div class="cols cols-85 cols-balls cols-center">
				<div class="lhc-mun lhc-green">01</div>
				<div class="lhc-mun lhc-green">01</div>
				<div class="lhc-mun lhc-green">01</div>
				<div class="lhc-mun lhc-green">01</div>
				<div class="lhc-mun lhc-green">01</div>
				<div class="lhc-mun lhc-green">01</div>
				<div class="lhc-mun lhc-green">01</div>
				<div class="lhc-mun lhc-green">01</div>
				<div class="lhc-mun lhc-green">01</div>
				<div class="lhc-mun lhc-green">01</div>
			</div>



			<div class="cols cols-15 cols-title">
				<span>兔</span>
				<span>00.0</span>
			</div>
			<div class="cols cols-85 cols-balls cols-center">
				<div class="lhc-mun lhc-blue">01</div>
				<div class="lhc-mun lhc-blue">01</div>
				<div class="lhc-mun lhc-blue">01</div>
				<div class="lhc-mun lhc-blue">01</div>
				<div class="lhc-mun lhc-blue">01</div>
				<div class="lhc-mun lhc-blue">01</div>
				<div class="lhc-mun lhc-blue">01</div>
				<div class="lhc-mun lhc-blue">01</div>
				<div class="lhc-mun lhc-blue">01</div>
				<div class="lhc-mun lhc-blue">01</div>
			</div>
		</div>


	</div> -->



	<!-- lottery-content over -->


	<div class="stance-nav"></div>

	<!-- modal record-modal 投注记录模态框 -->
		<div class="record-modal modal animated out">
			<!-- header -->
			<div class="header color-dark">
				<div class="header-left" onclick="modal_toggle('.record-modal')"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
				<div class="header-title"><span>投注记录</span></div>
			</div>
			<!-- header over -->
			<div class="stance"></div>
	
			
			<div class="main" id="userTodayLotteryList">
				<div class="line">
					<div class="child child1">
						<p>彩种</p>
					</div>
					<div class="child child2">
						<p>期号</p>
					</div>
					<div class="child child3">
						<p>投注金额</p>
					</div>
					<div class="child child4">
						<p>奖金</p>
					</div>
					<div class="child child5">
						<p>状态</p>
					</div>
					<div class="child child6">
						<i class="ion-close-circled"></i>
					</div>
				</div>
				<!-- <div class="line" name="lotteryMsgRecord"
						data-lottery="147822406760273" data-type="0" data-value="151">
					<div class="child child1">
						<p>重庆时时彩</p>
					</div>
					<div class="child child2">
						<p>20161104024</p>
					</div>
					<div class="child child3">
						<p>0.200</p>
					</div>
					<div class="child child4">
						<p>0.000</p>
					</div>
					<div class="child child5">
						<p class="cp-yellow">未开奖</p>
					</div>
					<div class="child child6">
						<i class="ion-close-circled"></i>
					</div>
				</div> -->
				
			</div>
			
			<!-- main over -->
			<!-- <div class="stance-nav"></div> -->
			<div class="stance-nav"></div>
			
		</div>
</div>
</div>



<!-- lottery-bar -->
<%-- <div class="lottery">
<div class="lottery-bar">

	<div class="bar liuhecai-bar" >

		<div class="content">
			<div class="t1">已选中 <span class="pour font-yellow" id="lotteryNum" >0</span> 注</div>
		</div>
		<div class="text">
			<input type="text" class="inputText" id="shortcut-money" placeholder="输入金额">
		</div>

		<div class="btn color-red" onclick="lotteryLhcCommonPage.showLotteryMsg()" ><img src="<%=path%>/images/icon/icon-shopping.png"  alt="" class="icon"><span>投注</span></div>
		<div class="btn color-gray" data-type="reset" onclick="lhcPage.select_type('.lottery_liuhecai',this)"><span>重设</span></div>
	</div>
</div>
</div> --%>
<!-- lottery-bar over -->

<!-- lottery-bar -->
<div class="lottery-bar lottery">

	<div class="additional">
		<div class="bg"></div>
		<div class="content">
			<div class="title">金额</div>
			<div class="multiple-select">
				<div class="multiple-btn" id="beishuSub">－</div>
				<div class="multiple-text"><input data-price="10" value="10" id="shortcut-money" type="tel" onfocus="onfocusFn(this);" onmouseup="onmouseupFn(event);"/></div>
				<div class="multiple-btn" id="beishuAdd">＋</div>
			</div>
			<div class="additional-shortcut-money">
				<span>快捷金额</span>
				<div class="money-list clear">
					<a href="javascript:lhcData.colsChildSet(10);">10</a>
					<a href="javascript:lhcData.colsChildSet(20);">20</a>
					<a href="javascript:lhcData.colsChildSet(50);">50</a>
					<a href="javascript:lhcData.colsChildSet(100);">100</a>
					<a href="javascript:lhcData.colsChildSet(200);">200</a>
					<a href="javascript:lhcData.colsChildSet(500);">500</a>
					<a href="javascript:lhcData.colsChildSet(1000);">1000</a>
					<a href="javascript:lhcData.colsChildSet(2000);">2000</a>
				</div>
			</div>
		</div>
	</div>
	<div class="bar color-dark">
		<div class="clear no" data-type="reset" id="lhcReset" onclick="lhcPage.select_type('.lottery_liuhecai',this)"><span >清空</span></div>
		<div class="content" onclick="modal_toggle('.record-modal')">
			<div class="t1">已选 <span class="pour" id="J-balls-statistics-lotteryNum">0</span> 注 <span class="money font-yellow" id="J-balls-statistics-amount">0.00</span> 元</div>
			<div class="t2"><span id="J-balls-statistics-code">（投注列表）</span></div>
		</div>
		<div class="btn color-red" onclick="lotteryLhcCommonPage.showLotteryMsg()"><span>投注</span><img src="<%=path%>/images/icon/icon-shopping.png" alt="" class="icon"></div>
	</div>
</div>
<!-- lottery-bar over -->

	<jsp:include page="/front/include/include.jsp"></jsp:include>

	<script src="<%=path%>/js/lottery/lhc/lhc_data.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
	<script src="<%=path%>/js/lottery/lhc/lottery_lhc.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
	<script src="<%=path%>/js/lottery/lhc/lotteryLhc_base.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
</body>

</html>