<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant,com.team.lottery.system.BizSystemConfigVO,com.team.lottery.system.BizSystemConfigManager,
         com.team.lottery.vo.LotterySet,com.team.lottery.cache.ClsCacheManager,
         com.team.lottery.system.BizSystemConfigManager,com.team.lottery.vo.BizSystemInfo,
         com.team.lottery.util.LotterySetUntil,com.team.lottery.extvo.LotterySetMsg"
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();

	String serverName=  request.getServerName();//获取当前域名
	String bizSystem=ClsCacheManager.getBizSystemByDomainUrl(serverName);
	BizSystemConfigVO bizSystemConfigVO=BizSystemConfigManager.getBizSystemConfig(bizSystem);//业务系统彩种设置
	LotterySet lotterySet=new LotterySet();
	LotterySetUntil lotterySetUntil=new LotterySetUntil();
	//全部彩种的
	LotterySetMsg lotterySetMsgAll=lotterySetUntil.compareLotterySetAndBizSystemConfigVO(lotterySet, bizSystemConfigVO);
	request.setAttribute("lotterySetMsgAll", lotterySetMsgAll);
	
	if((lotterySet.getIsJSPK10Open() != null && lotterySet.getIsJSPK10Open() == 0)||(bizSystemConfigVO.getIsJSPK10Open() != null && bizSystemConfigVO.getIsJSPK10Open() == 0)){
	    response.sendRedirect(path+"/gameBet/index.html");
	  }
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="format-detection" content="telephone=no">
    <title>极速PK10</title>
    <link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/lottery_base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/lottery_pk10.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/lottery_additional.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <%-- <link href="<%=path%>/css/user_base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet"> --%>
</head>
<body>

<!-- header -->
<div class="header color-dark lottery">
    <div class="header-left" onclick="window.location.href='<%=path%>/gameBet/hall.html'"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
    <div class="header-title"id="currentKindDiv"><span id="currentKindName">前三 复式</span><img class="icon" src="<%=path%>/images/lottery/pointer.png"></div>
    <div class="header-right" id="otherLotteryKind"><span>极速PK10</span><img class="icon" src="<%=path%>/images/icon/icon-pointer-white.png"></div>
</div>
<!-- header over -->
<!-- 各地的时时彩选择 -->
<div class="lottery-classify lottery-classify1 animated">
    <div class="container">
        <div class="move-content">
          <c:if test="${lotterySetMsgAll.isBJPK10Open==1}">
       		<a href="<%=path%>/gameBet/lottery/bjpks/bjpk10.html">
        		<div class="child color-red">
           		 <p>北京PK10</p>
        		</div>
   	  		</a>
       	  </c:if>
        </div>
    </div>
</div>

<!-- 各种玩法选择 -->
<div class="lottery-classify lottery-classify2 animated">
	<div class="left_but"></div>
    <div class="container slide-container" style="z-index:1;">
        <div class="move-content" id="lotteryStarKindList">
            <div class="child color-red" data-id="CGJ">
                <p>猜冠军</p>
            </div>
            <div class="child" data-role-id="QYZXFS">
                <p>前一</p>
            </div>
            <div class="child" data-id="QE">
                <p>前二</p>
            </div>
            <div class="child" data-id="QS">
                <p>前三</p>
            </div>
            <div class="child" data-id="QSI">
                <p>前四</p>
            </div>
            <div class="child" data-id="QW">
                <p>前五</p>
            </div>
            <div class="child" data-role-id="DWD">
                <p>定位胆</p>
            </div>
            <div class="child" data-id="LHD">
                <p>龙虎斗</p>
            </div>
            <div class="child" data-id="DX">
                <p>大小</p>
            </div>
            <div class="child" data-id="DS">
                <p>单双</p>
            </div>
        </div>
    </div>
    <div class="right_but"></div>
 	<div class="lottery-classify-list" id="lotteryKindPlayList">
	</div>
   <!--  <div class="lottery-classify-list">

        <div class="container clist clist1 animated">
            <div class="list-line">
                <div class="line-title">
                    <p>直选</p>
                </div>
                <div class="line-content">
                    <div class="child color-red" data-role-id="WXFS">
                        <p>复式</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container clist clist2 animated">
            <div class="list-line">
                <div class="line-title">
                    <p>直选</p>
                </div>
                <div class="line-content">
                    <div class="child color-red no" data-role-id="WXFS">
                        <p>复式</p>
                    </div>
                    <div class="child color-red no" data-role-id="WXDS">
                        <p>单式</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="container clist clist7 animated">
            <div class="list-line">
                <div class="line-title">
                    <p></p>
                </div>
                <div class="line-content">
                    <div class="child color-red no" data-role-id="LHD1VS10">
                        <p>1VS10</p>
                    </div>
                    <div class="child color-red no" data-role-id="LHD1VS10">
                        <p>2VS9</p>
                    </div>
                    <div class="child color-red no" data-role-id="LHD1VS10">
                        <p>3VS8</p>
                    </div>
                    <div class="child color-red no" data-role-id="LHD1VS10">
                        <p>4VS7</p>
                    </div>
                    <div class="child color-red no" data-role-id="LHD1VS10">
                        <p>5VS6</p>
                    </div>
                </div>
            </div>

        </div>
        <div class="container clist clist8 animated">
            <div class="list-line">
                <div class="line-title">
                    <p></p>
                </div>
                <div class="line-content">
                    <div class="child color-red no" data-role-id="DXGJ">
                        <p>大小</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container clist clist9 animated">
            <div class="list-line">
                <div class="line-title">
                    <p></p>
                </div>
                <div class="line-content">
                    <div class="child color-red no" data-role-id="DSGJ">
                        <p>单双</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container clist clist10 animated">
            <div class="list-line">
                <div class="line-title">
                    <p></p>
                </div>
                <div class="line-content">
                    <div class="child color-red no" data-role-id="HZGYJ">
                        <p>冠军和值</p>
                    </div>
                </div>
            </div>
        </div>


    </div> -->
</div>
<div class="alert-bg" onclick="element_toggle(false,['.lottery-classify','.alert'])"></div>
<!-- alert -->
<div class="alert alert-cart" id="lotteryOrderDialog" style="display: none">
    <div class="alert-head color-dark">
        <p>投注确认</p>
        <i class="close ion-close-round" onclick="element_toggle(false,['.alert-cart'])"></i>
    </div>
    <div class="alert-content">
        <ul>
            <li class="user-line">
                <div class="line-title">
                    <p id="lotteryKindForOrder">XXXX:</p>
                </div>
                <div class="line-content">
                    <p id="lotteryExpectDesForOrder">第xxxx期</p>
                </div>
            </li>
            <li class="user-line">
                <div class="line-title">
                    <p>投注金额:</p>
                </div>
                <div class="line-content">
                    <p class="font-red" id="lotteryTotalPriceForOrder">0元</p>
                </div>
            </li>
            <li class="user-line">
                <div class="line-title">
                    <p>投注内容:</p>
                </div>
                <div class="line-content">
                    <p id="lotteryDetailForOrder">投注内容</p>
                </div>
            </li>

        </ul>
    </div>
    <div class="alert-foot">
        <div class="btn color-gray cols-50" id="lotteryOrderDialogCancelButton">取消</div>
        <div class="btn color-red cols-50" id="lotteryOrderDialogSureButton">确认投注</div>
    </div>
</div>
<!-- alert over -->

    <div class="stance"></div>

    <!-- main-head -->
    <div class="main-head main">
        <div class="container">
            <div class="sub"></div>
            <div class="child lottery_run">
                <div class="child-title">
                    <span class="font-red" id="J-lottery-info-lastnumber">xxxxx</span>
                    <span>期</span><img class="icon" src="<%=path%>/images/lottery/pointer.png">
                </div>
                <div class="child-muns">
                    <span class="ball color-red" id="lotteryNumber1">-</span>
                    <span class="ball color-red" id="lotteryNumber2">-</span>
                    <span class="ball color-red" id="lotteryNumber3">-</span>
                    <span class="ball color-red" id="lotteryNumber4">-</span>
                    <span class="ball color-red" id="lotteryNumber5">-</span>
                    <span class="ball color-red" id="lotteryNumber6">-</span>
                    <span class="ball color-red" id="lotteryNumber7">-</span>
                    <span class="ball color-red" id="lotteryNumber8">-</span>
                    <span class="ball color-red" id="lotteryNumber9">-</span>
                    <span class="ball color-red" id="lotteryNumber10">-</span>
                </div>
            </div>
            <div class="child">
                <div class="child-title">
                    <span>距 </span>
                    <span class="font-red" id="nowNumber">xxxx</span>
                    <span>期 截止</span>
                </div>
                <div class="child-muns">
                    <span class="mun font-red" id="timer">00:00</span>
                </div>
            </div>
        </div>
    </div>
    <!-- main-head over -->
    <!-- lottery-dynamic -->
    <div class="lottery-dynamic" id="nearestTenLotteryCode">
<!--         <div class="titles">
            <div class="child child1">
                <p>期号</p>
            </div>
            <div class="child child2">
                <p>开奖号</p>
            </div>
        </div>
        <div class="content">
            <div class="line">
                <div class="child child1">
                    <p>20161004-1213</p>
                </div>
                <div class="child child2 font-red muns muns-small">
                    <div class="mun color-red">2</div>
                    <div class="mun color-red">4</div>
                    <div class="mun color-red">6</div>
                    <div class="mun color-red">0</div>
                    <div class="mun color-red">9</div>
                    <div class="mun color-red">2</div>
                    <div class="mun color-red">4</div>
                    <div class="mun color-red">6</div>
                    <div class="mun color-red">0</div>
                    <div class="mun color-red">9</div>
                </div>
            </div>
        </div> -->
    </div>
    <!-- lottery-dynamic over -->

    <ul class="user-list reminder no">
        <!-- user-list 提示信息 -->
        <li class="user-msg">
            <div class="container">
                <p id="playDes">玩法说明<</p>
            </div>
        </li>
        <!-- user-list over 提示信息 -->
    </ul>

    <!-- lottery-content -->
    <div class="lottery-content" id="ballSection">
<!--         <div class="lottery-child no main">
            <div class="container">
                <div class="child-content" style="text-align:center;">
                    <div class="mun2">
                        <h2 class="value">15</h2>
                        <p class="info">赔率1.1</p>
                    </div>
                    <div class="mun2">
                        <h2 class="value">16</h2>
                        <p class="info">赔率1.1</p>
                    </div>
                    <div class="mun2">
                        <h2 class="value">17</h2>
                        <p class="info">赔率1.1</p>
                    </div>
                    <div class="mun2">
                        <h2 class="value">18</h2>
                        <p class="info">赔率1.1</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="lottery-child no main">
            <div class="container">
                <div class="child-content" style="text-align:center;">
                    <div class="mun2">
                        <h2 class="value">19</h2>
                        <p class="info">赔率1.1</p>
                    </div>
                    <div class="mun2 long">
                        <h2 class="value">龙</h2>
                        <p class="info">赔率1.1</p>
                    </div>

                    <div class="mun2">
                        <h2 class="value">虎</h2>
                        <p class="info">赔率1.1</p>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
    <!-- lottery-content over -->


    <div class="stance-nav"></div>
    <div class="stance-nav"></div>

    <!-- modal record-modal 投注记录模态框 -->
    <div class="record-modal modal animated out">
        <!-- header -->
        <div class="header color-dark">
            <div class="header-left" onclick="modal_toggle('.record-modal')"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
            <div class="header-title"><span>投注记录</span></div>
        </div>
        <!-- header over -->
        <div class="stance"></div>

	<!-- 	<div class="main" id="codePlanList"></div> -->
        <div class="main" id="userTodayLotteryList">

         <!--    <div class="line">
                <div class="child child1">
                    <p>[五星_复式]1,1,3,6,7</p>
                </div>
                <div class="child child2">
                    <p>¥2.0000</p>
                </div>
                <div class="child child3">
                    <p>1注</p>
                </div>
                <div class="child child4">
                    <p>1倍</p>
                </div>
                <div class="child child5">
                    <p>194000.0000元</p>
                </div>
                <div class="child child6"><i class="ion-close-circled"></i></div>
            </div>
            <div class="line">
                <div class="child child1">
                    <p>[五星_复式]1,1,3,6,7</p>
                </div>
                <div class="child child2">
                    <p>¥2.0000</p>
                </div>
                <div class="child child3">
                    <p>1注</p>
                </div>
                <div class="child child4">
                    <p>1倍</p>
                </div>
                <div class="child child5">
                    <p>194000.0000元</p>
                </div>
                <div class="child child6"><i class="ion-close-circled"></i></div>
            </div> -->

        </div>

        <!-- main over -->

    </div>
    <!-- modal record-modal over -->


    <!-- modal additional-modal 追号模态框 -->
    <div class="additional-modal modal animated out" id="J-trace-panel">
        <!-- header -->
        <div class="header color-dark">
            <div class="header-left" onclick="modal_toggle('.additional-modal')"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
            <div class="header-title"><span>我要追号</span></div>
            <div class="header-right"><span>今天</span></div>
        </div>
        <!-- header over -->
        <div class="stance"></div>

        <!-- main -->
        <!-- user-list 单选框 -->
        <li class="user-line main">
            <div class="container">
                <div class="line-title">
                    <p>中奖后停止追号</p>
                </div>
                <div class="line-content">

                    <div class="right toggle" data-onclass="red" id="J-trace-iswintimesstop-btn">
                        <input type="hidden" id="J-trace-iswintimesstop" class="toggle-value" value="0">
                        <div class="value" value="0"></div>
                        <div class="value" value="1"></div>
                        <div class="ball"></div>
                    </div>

                </div>
            </div>
        </li>
        <!-- user-list over 单选框 -->
        <div class="main" id="todayList" data-type="TODAY">
            <div class="titles">
                <div class="child child1">
                    <p><span>序号</span></p>
                </div>
                <div class="child child2">
                    <div class="checkbox" data-onclass="red">
                        <input type="hidden" class="checkbox-value" value="0">
                        <img class="icon" src="<%=path%>/images/icon/icon-right.png" />
                    </div>
                    <span>期数</span>
                    <input type="text" id="todayTraceExpectsInputCount" name="trace-expects-input-count" class="inputText" value="1">
                </div>
                <div class="child child3">
                    <p>
                        <input type="text" id="todayTraceExpectsBeishuInputCount" name='trace-expects-beishu-input' class="inputText" value="1">
                        <span>倍</span>
                    </p>
                </div>
                <div class="child child4">
                    <p><span>金额</span></p>
                </div>
                <div class="child child5">
                    <p><span>投注截止时间</span></p>
                </div>
            </div>
<!--             <div class="line">
                <div class="child child1">
                    <p><span>1</span></p>
                </div>
                <div class="child child2">
                    <div class="checkbox" data-onclass="blue">
                        <input type="hidden" class="checkbox-value" value="0">
                        <img class="icon" src="<%=path%>/images/icon/icon-right.png" />
                    </div>
                    <span>160925-072</span>
                </div>
                <div class="child child3">
                    <p>
                        <input type="text" class="inputText" value="1">
                        <span>倍</span>
                    </p>
                </div>
                <div class="child child4">
                    <p><span>¥1132.00元</span></p>
                </div>
                <div class="child child5">
                    <p><span>2016/09/27 11:16:33</span></p>
                </div>
            </div> -->
        </div>

        <!-- main over -->

    </div>
    <!-- modal additional-modal over -->




    <!-- lottery-bar -->
    <div class="lottery-bar lottery">

        <div class="additional">
            <div class="bg"></div>
            <div class="content">
                <div class="title">倍数</div>
                <div class="multiple-select">
                    <div class="multiple-btn" id="beishuSub">－</div>
                    <div class="multiple-text"><input value="1" id="beishu" type="tel" onfocus="onfocusFn(this);" onmouseup="onmouseupFn(event);"/></div>
                    <div class="multiple-btn" id="beishuAdd">＋</div>
                </div>
                <div class="additional-btn color-blue" id="zhuiCodeButton">我要追号</div>
            </div>
        </div>
        <div class="bar color-dark">
            <div class="clear no"><span id="clearAllCodeBtn">清空</span></div>
            <div class="content" onclick="modal_toggle('.record-modal')">
                <div class="t1">已选 <span class="pour" id="J-balls-statistics-lotteryNum">0</span> 注 <span class="money font-yellow" id="J-balls-statistics-amount">0.00</span> 元</div>
                <div class="t2"><span id="J-balls-statistics-code">（投注列表）</span></div>
            </div>
            <div class="btn color-red" id="showLotteryMsgButton"><span>投注</span><img src="<%=path%>/images/icon/icon-shopping.png" alt="" class="icon"></div>
        </div>
    </div>
    <!-- lottery-bar over -->
    
	<jsp:include page="/front/include/include.jsp"></jsp:include>
	
	<script type="text/javascript">
	var currentUserPk10Model = currentUser.pk10Rebate;
	var pk10LowestAwardModel = <%=SystemConfigConstant.PK10LowestAwardModel %>;
	</script>
	
    <script src="<%=path%>/js/lottery/common/pk10_data.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
    <script src="<%=path%>/js/lottery/common/lottery_common.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
    <script src="<%=path%>/js/lottery/jspks/jspk10.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
    <%-- <script src="<%=path%>/js/user_base.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script> --%>
</body>
</html>