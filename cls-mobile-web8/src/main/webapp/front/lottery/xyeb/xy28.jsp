<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant,com.team.lottery.system.BizSystemConfigVO,com.team.lottery.system.BizSystemConfigManager,
         com.team.lottery.vo.LotterySet,com.team.lottery.cache.ClsCacheManager,
         com.team.lottery.system.BizSystemConfigManager,com.team.lottery.vo.BizSystemInfo,
         com.team.lottery.util.LotterySetUntil,com.team.lottery.extvo.LotterySetMsg"
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();

	String serverName=  request.getServerName();//获取当前域名
	String bizSystem=ClsCacheManager.getBizSystemByDomainUrl(serverName);
	BizSystemConfigVO bizSystemConfigVO=BizSystemConfigManager.getBizSystemConfig(bizSystem);//业务系统彩种设置
	LotterySet lotterySet=new LotterySet();
	LotterySetUntil lotterySetUntil=new LotterySetUntil();
	//全部彩种的
	LotterySetMsg lotterySetMsgAll=lotterySetUntil.compareLotterySetAndBizSystemConfigVO(lotterySet, bizSystemConfigVO);
	request.setAttribute("lotterySetMsgAll", lotterySetMsgAll);
	
	if((lotterySet.getIsXYEBOpen() != null && lotterySet.getIsXYEBOpen() == 0)||(bizSystemConfigVO.getIsXYEBOpen() != null && bizSystemConfigVO.getIsXYEBOpen() == 0)){
	    response.sendRedirect(path+"/gameBet/index.html");
	  }
%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
	<meta name="format-detection" content="telephone=no">
	<title>幸运28</title>

	<link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
	<link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
	<link href="<%=path%>/css/lottery_base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
	<link href="<%=path%>/css/lottery_liuhecai.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
	<link href="<%=path%>/css/lottery_additional.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
	<%-- <link href="<%=path%>/css/user_base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet"> --%>
</head>
<body class="lottery_liuhecai">
<!-- header -->
<div class="header color-dark lottery">
	<div class="header-left" onclick="window.history.go(-1);"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
	<div class="header-title" onclick="lotteryCommonPage.show_classify('.lottery-classify2',this)"><span id="currentKindName" >混合</span><img class="icon" src="<%=path%>/images/icon/icon-pointer-black.png"></div>
	<div class="header-right"><span>幸运28</span></div>
</div>
<!-- header over -->

<!-- 各种玩法选择 -->
<div class="lottery-classify lottery-classify2 animated" >
<!-- <div class="left_but"></div> -->
<div class="container slide-container" style="z-index:1;">
	<div class="move-content">
        <div class="child color-red" data-id="HH" onclick="lotteryCommonPage.play_change('.move-content',this)"><p>混合</p></div>
        <div class="child" data-id="TM" onclick="lotteryCommonPage.play_change('.move-content',this)"><p>特码</p></div>
	</div>
</div>
<!-- <div class="right_but"></div> -->
</div>

<div class="alert-bg" onclick="element_toggle(false,['.lottery_liuheicai .lottery-classify','.alert'])"></div>
<!-- alert -->
 <div class="alert alert-cart" >
	<div class="alert-head color-dark">
	<p>投注确认</p>
	<i class="close ion-close-round" onclick="element_toggle(false,['.alert-cart'])"></i>
	</div>
	<div class="alert-content" id="xyeb_msg">

		<div class="msg-list" >
        <ul>
            <li class="msg-list-li msg-list-t">
                <div class="child child1">序号</div>
                <div class="child child2">内容</div>
                <div class="child child3">赔率</div>
                <div class="child child4">下注金额</div>
            </li>
        </ul>
    </div>
		<ul>
			<li class="user-line">
				<div class="line-title"><p>期数:</p></div>
				<div class="line-content"><p>0</p></div>
			</li>
			<li class="user-line">
				<div class="line-title"><p>盘口:</p></div>
				<div class="line-content"><p>0</p></div>
			</li>
			<li class="user-line">
				<div class="line-title"><p>总下注额:</p></div>
				<div class="line-content"><p class="font-red">0元</p></div>
			</li>
		</ul>
	</div>
	<div class="alert-foot">
		<div class="btn color-gray cols-50" onclick="element_toggle(false,['.alert-cart'])">取消</div>
		<div class="btn color-red cols-50" onclick="lotteryCommonPage.userLottery()">确认投注</div>
	</div>
</div>
<!-- alert over -->
<div class="layout-scroll">
<div class="cont">
	<div class="stance"></div>

	<!-- main-head -->
	<div class="main-head main">
	<div class="container">
		<div class="sub"></div>
		<div class="child lottery_run">
			<div class="child-title">
				<span class="font-red" id="J-lottery-info-lastnumber">00000000000</span>
				<span>期</span><img class="icon" src="<%=path%>/images/lottery/pointer.png">
			</div>
			<div class="child-muns">
				<div class="xyeb-mun xyeb-red big" id="lotteryNumber1" >0</div>
				<div class="xyeb-mun xyeb-green big" id="lotteryNumber2" >0</div>
				<div class="xyeb-mun xyeb-red big" id="lotteryNumber3" >0</div>
				<div class="xyeb-mun equal" >=</div>
				<div class="xyeb-mun xyeb-blue big" id="lotteryNumber4" >0</div>
			</div>
		</div>
		<div class="child">
			<div class="child-title">
				<span>距 </span>
				<span class="font-red" id="nowNumber">00000000000</span>
				<span>期 截止</span>
			</div>
			<div class="child-muns">
				<span class="mun font-red" id="timer">00:00</span>
			</div>
		</div>
	</div>
	</div>
	<!-- main-head over -->
    <!-- lottery-dynamic -->
	<div class="lottery-dynamic mt-30" id="nearestTenLotteryCode">
	</div>
	<!-- lottery-dynamic over -->

	<!-- lottery-content -->
	<div class="lottery-content lottery-content-xyeb" id="xyebContent">
		
		<!-- 混合 -->
		<div class="content-xyeb clear m0" id="initPlay2">
		    <div class="lottery-child no main">
		    	<div class="child-title"><h2>混合</h2></div>
                <div class="child-content">
                    <div class="mun2">
                        <h2 class="value">大</h2>
                        <p class="info">赔率0</p>
                    </div>
                    <div class="mun2">
                        <h2 class="value">小</h2>
                        <p class="info">赔率0</p>
                    </div>
                    <div class="mun2">
                        <h2 class="value">单</h2>
                        <p class="info">赔率0</p>
                    </div>
                    <div class="mun2">
                        <h2 class="value">双</h2>
                        <p class="info">赔率0</p>
                    </div>
                </div>
                <div class="child-content">
                    <div class="mun2">
                        <h2 class="value">大单</h2>
                        <p class="info">赔率0</p>
                    </div>
                    <div class="mun2">
                        <h2 class="value">小单</h2>
                        <p class="info">赔率0</p>
                    </div>
                    <div class="mun2">
                        <h2 class="value">大双</h2>
                        <p class="info">赔率0</p>
                    </div>
                    <div class="mun2">
                        <h2 class="value">小双</h2>
                        <p class="info">赔率0</p>
                    </div>
                </div>
                 <div class="child-content part-second">
                    <div class="mun2">
                        <h2 class="value">极大</h2>
                        <p class="info">赔率0</p>
                    </div>
                    <div class="mun2">
                        <h2 class="value">极小</h2>
                        <p class="info">赔率0</p>
                    </div>
                </div>
	        </div>
	        <div class="lottery-child no main">
	        	 <div class="child-title"><h2>波色</h2></div>
                 <div class="child-content part-third">
                    <div class="mun2 bg-red">
                        <h2 class="value">红波</h2>
                        <p class="info">赔率0</p>
                    </div>
                    <div class="mun2 bg-green">
                        <h2 class="value">绿波</h2>
                        <p class="info">赔率0</p>
                    </div>
                    <div class="mun2 bg-blue">
                        <h2 class="value">蓝波</h2>
                        <p class="info">赔率0</p>
                    </div>
                </div>
	        </div>
	        <div class="lottery-child no main">
	        	<div class="child-title"><h2>豹子</h2></div>
                <div class="child-content long">
                    <div class="mun2">
                        <h2 class="value">豹子</h2>
                        <p class="info">赔率0</p>
                    </div>
                </div>
	        </div>
		</div>
		
		<!-- 特码 -->
		<div class="content-xyeb clear hide" id="initPlay">
			<div class="cols cols-25 ball-0">
				<div class="xyeb-mun xyeb-gray">0</div>
				<span>-</span>
			</div>
			<div class="cols cols-25 ball-1">
				<div class="xyeb-mun xyeb-green">1</div>
				<span>-</span>
			</div>
			<div class="cols cols-25 ball-2">
				<div class="xyeb-mun xyeb-blue">2</div>
				<span>-</span>
			</div>
			<div class="cols cols-25 ball-3">
				<div class="xyeb-mun xyeb-red">3</div>
				<span>-</span>
			</div>
			<div class="cols cols-25 ball-4">
				<div class="xyeb-mun xyeb-green">4</div>
				<span>-</span>
			</div>
			<div class="cols cols-25 ball-5">
				<div class="xyeb-mun xyeb-blue">5</div>
				<span>-</span>
			</div>
			<div class="cols cols-25 ball-6">
				<div class="xyeb-mun xyeb-red">6</div>
				<span>-</span>
			</div>
			<div class="cols cols-25 ball-7">
				<div class="xyeb-mun xyeb-green">7</div>
				<span>-</span>
			</div>
			<div class="cols cols-25 ball-8">
				<div class="xyeb-mun xyeb-blue">8</div>
				<span>-</span>
			</div>
			<div class="cols cols-25 ball-9">
				<div class="xyeb-mun xyeb-red">9</div>
				<span>-</span>
			</div>
			<div class="cols cols-25 ball-10">
				<div class="xyeb-mun xyeb-green">10</div>
				<span>-</span>
			</div>
			<div class="cols cols-25 ball-11">
				<div class="xyeb-mun xyeb-blue">11</div>
				<span>-</span>
			</div>
			<div class="cols cols-25 ball-12">
				<div class="xyeb-mun xyeb-red">12</div>
				<span>-</span>
			</div>
			<div class="cols cols-25 ball-13">
				<div class="xyeb-mun xyeb-green">13</div>
				<span>-</span>
			</div>
			<div class="cols cols-25 ball-14">
				<div class="xyeb-mun xyeb-blue">14</div>
				<span>-</span>
			</div>
			<div class="cols cols-25 ball-15">
				<div class="xyeb-mun xyeb-red">15</div>
				<span>-</span>
			</div>
			<div class="cols cols-25 ball-16">
				<div class="xyeb-mun xyeb-green">16</div>
				<span>-</span>
			</div>
			<div class="cols cols-25 ball-17">
				<div class="xyeb-mun xyeb-blue">17</div>
				<span>-</span>
			</div>
			<div class="cols cols-25 ball-18">
				<div class="xyeb-mun xyeb-red">18</div>
				<span>-</span>
			</div>
			<div class="cols cols-25 ball-19">
				<div class="xyeb-mun xyeb-green">19</div>
				<span>-</span>
			</div>
			<div class="cols cols-25 ball-20">
				<div class="xyeb-mun xyeb-blue">20</div>
				<span>-</span>
			</div>
			<div class="cols cols-25 ball-21">
				<div class="xyeb-mun xyeb-red">21</div>
				<span>-</span>
			</div>
			<div class="cols cols-25 ball-22">
				<div class="xyeb-mun xyeb-green">22</div>
				<span>-</span>
			</div>
			<div class="cols cols-25 ball-23">
				<div class="xyeb-mun xyeb-blue">23</div>
				<span>-</span>
			</div>
			<div class="cols cols-25 ball-24">
				<div class="xyeb-mun xyeb-red">24</div>
				<span>-</span>
			</div>
			<div class="cols cols-25 ball-25">
				<div class="xyeb-mun xyeb-green">25</div>
				<span>-</span>
			</div>
			<div class="cols cols-25 ball-26">
				<div class="xyeb-mun xyeb-blue">26</div>
				<span>-</span>
			</div>
			<div class="cols cols-25 ball-27">
				<div class="xyeb-mun xyeb-gray">27</div>
				<span>-</span>
			</div>
			
		</div>

	</div>

	<!-- lottery-content over -->


	<div class="stance-nav"></div>

	<!-- modal record-modal 投注记录模态框 -->
		<div class="record-modal modal animated out">
			<!-- header -->
			<div class="header color-dark">
				<div class="header-left" onclick="modal_toggle('.record-modal')"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
				<div class="header-title"><span>投注记录</span></div>
			</div>
			<!-- header over -->
			<div class="stance"></div>
	
			<!-- <div class="main" id="codePlanList"></div> -->
			<div class="main" id="userTodayLotteryList">
				<div class="line">
					<div class="child child1">
						<p>彩种</p>
					</div>
					<div class="child child2">
						<p>期号</p>
					</div>
					<div class="child child3">
						<p>投注金额</p>
					</div>
					<div class="child child4">
						<p>奖金</p>
					</div>
					<div class="child child5">
						<p>状态</p>
					</div>
					<div class="child child6">
						<i class="ion-close-circled"></i>
					</div>
				</div>
				<!-- <div class="line" name="lotteryMsgRecord"
						data-lottery="147822406760273" data-type="0" data-value="151">
					<div class="child child1">
						<p>重庆时时彩</p>
					</div>
					<div class="child child2">
						<p>20161104024</p>
					</div>
					<div class="child child3">
						<p>0.200</p>
					</div>
					<div class="child child4">
						<p>0.000</p>
					</div>
					<div class="child child5">
						<p class="cp-yellow">未开奖</p>
					</div>
					<div class="child child6">
						<i class="ion-close-circled"></i>
					</div>
				</div> -->
				
			</div>
			
			<!-- main over -->
			<!-- <div class="stance-nav"></div> -->
			<div class="stance-nav"></div>
			
		</div>
</div>
</div>



<!-- lottery-bar -->
<%-- <div class="lottery">
<div class="lottery-bar">

	<div class="bar liuhecai-bar" >

		<div class="content">
			<div class="t1">已选中 <span class="pour font-yellow" id="lotteryNum" >0</span> 注</div>
		</div>
		<div class="text">
			<input type="text" class="inputText" id="shortcut-money" placeholder="输入金额">
		</div>

		<div class="btn color-red" onclick="lotteryLhcCommonPage.showLotteryMsg()" ><img src="<%=path%>/images/icon/icon-shopping.png"  alt="" class="icon"><span>投注</span></div>
		<div class="btn color-gray" data-type="reset" onclick="lhcPage.select_type('.lottery_liuhecai',this)"><span>重设</span></div>
	</div>
</div>
</div> --%>
<!-- lottery-bar over -->

<!-- lottery-bar -->
<div class="lottery-bar lottery">

	<div class="additional">
		<div class="bg"></div>
		<div class="content p25-0">
			<div class="title">金额</div>
			<div class="multiple-select">
				<div class="multiple-btn" id="beishuSub">－</div>
				<div class="multiple-text"><input  data-price="10" value="10" id="shortcut-money" type="tel" onfocus="onfocusFn(this);" onmouseup="onmouseupFn(event);"/></div>
				<div class="multiple-btn" id="beishuAdd">＋</div>
			</div>
			<div class="additional-shortcut-money">
				<span>快捷金额</span>
				<div class="money-list xyeb-money-list clear">
					<a href="javascript:lotteryDataDeal.colsChildSet(5);">5</a>
					<a href="javascript:lotteryDataDeal.colsChildSet(10);">10</a>
					<a href="javascript:lotteryDataDeal.colsChildSet(50);">50</a>
					<a href="javascript:lotteryDataDeal.colsChildSet(100);">100</a>
				</div>
			</div>
		</div>
	</div>
	<div class="bar color-dark">
		<div class="clear no" data-type="reset" id="XYEBReset" onclick="xyebPage.select_type()"><span >清空</span></div>
		<div class="content" onclick="modal_toggle('.record-modal')">
			<div class="t1">已选 <span class="pour" id="J-balls-statistics-lotteryNum">0</span> 注 <span class="money font-yellow" id="J-balls-statistics-amount">0.00</span> 元</div>
			<div class="t2"><span id="J-balls-statistics-code">（投注列表）</span></div>
		</div>
		<div class="btn color-red" onclick="lotteryCommonPage.showLotteryMsg()"><span>投注</span><img src="<%=path%>/images/icon/icon-shopping.png" alt="" class="icon"></div>
	</div>
</div>
<!-- lottery-bar over -->




	<jsp:include page="/front/include/include.jsp"></jsp:include>
	<script src="<%=path%>/js/lottery/xyeb/lotteryXyeb_base.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
	<script src="<%=path%>/js/lottery/xyeb/xyeb_data.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
	<script src="<%=path%>/js/lottery/xyeb/lottery_xyeb.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
	<%-- <script src="<%=path%>/js/user_base.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script> --%>
</body>

</html>