<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
    String status=request.getParameter("status");
%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="format-detection" content="telephone=no">
    <title>等级头衔</title>
    <link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/user_info_save.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <jsp:include page="/front/include/include.jsp"></jsp:include>
</head>

<body class="bg-white">

    <!-- header -->
    <div class="header color-dark fixed">
        <div class="header-left" onclick="window.history.go(-1);"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
        <h2 class="header-title">等级头衔</h2>
    </div>
    <!-- header over -->
    <div class="stance"></div>

   	<div class="main">
		 <div class="level-item">
		 	<h2>等级说明</h2>
		 	<p class="gray">共分为9个等级头衔，分别为农民、地主、知县、通判、知府、总督、巡抚、丞相和帝王。级别由成长积分决定，成长值越高等级越高，获得的奖励越高。</p>
		 	<h5><i class="icon icon-tip"></i>积分小提示：</h5>
		 	<p class="orange">每充值1元得到1个成长积分</p>
		 	<img src="<%=path%>/images/user/level-step.png" alt="" class="level-step">
		 </div>  
		 <div class="level-item pb100">
		 	<h2 class="mb60">等级奖励具体如下：</h2>
		 	<table id="levelSystemTable">
		 		<thead>
			 		<tr>
						<th>等级</th>
						<th>头衔</th>
						<th>所需积分</th>	
						<th>晋级奖励(元)</th>
						<th>跳级奖励(元)</th>
			 		</tr>
		 		</thead>
		 		<tbody>
			 		<tr>
			 			<td>0</td>
			 			<td>农民</td>
			 			<td>0</td>
			 			<td>0</td>
			 		</tr>
		 		</tbody>
		 	</table>
		 </div>	
   	</div>
	<jsp:include page="/front/include/include.jsp"></jsp:include>
	<script src="<%=path%>/js/my/user_level.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
</body>
</html>