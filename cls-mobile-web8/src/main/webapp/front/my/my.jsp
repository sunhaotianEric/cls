<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.team.lottery.util.ConstantUtil,com.team.lottery.vo.User" %>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
User user = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
request.setAttribute("user", user);
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="format-detection" content="telephone=no">
    <title>个人中心</title>
    <link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/user_info_save.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <jsp:include page="/front/include/include.jsp"></jsp:include>
</head>
<body>

<!-- header -->
<div class="header color-dark">
    <div class="header-left" onclick="window.location.href='<%=path%>/gameBet/setting.html'">设置</div>
    <h2 class="header-title" id="biz">***</h2>
    <div class="header-right" onclick="window.location.href='<%=path%>/gameBet/message.html'">
        <img class="icon" src="<%=path%>/images/icon/icon-onlin.png" />
        <div class="mun" id="msgcount"></div>
    </div>
</div>
<!-- header over -->

<!-- main-head -->
<div class="main-head">
    <div class="head-message">
        <div class="head">
            <div class="headimg"><img src="<%=path%>/images/user/save/head-img.png" alt=""></div>
            <div class="userinfo">
            	<div class="head-name"><span id="userInfNameId"></span><i class="icon icon-level1" id="iconlevel"></i><span class="level" id="levelName">农民</span></div>
            	<div class="progress-line">
            		<div class="progressbar" id="progressbar"><div class="progress" id="progress"></div></div>
            		<span class="point" id="poin">100/200</span>
            		<a href="<%=path%>/gameBet/userLevel.html">成长详情 &gt;&gt;</a>
            	</div>
            </div>
            <img src="<%=path%>/images/user/userlevel/icon-VIP1.png" id="iconvip" alt="" class="head-level">
        </div>
        <div class="foot">
           <!--  <p>情人节活动大放送&参与1</p>
            <p>情人节活动大放送&参与2</p>
            <p>情人节活动大放送&参与3</p> -->
        </div>
    </div>

    <div class="head-foot">
        <div class="head-money">
            <span class="money" id="spanBall">¥ 0.00</span>
        </div>
        <div class="head-referesh" id="refreshBall"><img src="<%=path%>/images/icon/icon-ref.png" alt=""></div>
        <div class="head-custom-msg">
            <p>随时恭候您</p>
            <p>24小时在线客服</p>
        </div>
        <div class="head-custom-btn" id="customServiceHerf">在线客服</div>
    </div>

</div>
<!-- main-head over -->
<!-- main-nav -->
<div class="main-nav">
    <div class="child" onclick="window.location.href='<%=path%>/gameBet/rechargeList.html'">
        <div class="child-icon"><img src="<%=path%>/images/user/save/icon1.png" alt=""></div>
        <div class="child-title"><span>充值</span></div>
    </div>
    <div class="child" onclick="window.location.href='<%=path%>/gameBet/withdraw.html'">
        <div class="child-icon"><img src="<%=path%>/images/user/save/icon2.png" alt=""></div>
        <div class="child-title"><span>提现</span></div>
    </div>
    <div class="child" onclick="window.location.href='<%=path%>/gameBet/moneyDetail.html'">
        <div class="child-icon"><img src="<%=path%>/images/user/save/icon3.png" alt=""></div>
        <div class="child-title"><span>明细</span></div>
    </div>
    <c:if test="${user.dailiLevel!='REGULARMEMBERS'}">
    <div class="child" onclick="window.location.href='<%=path%>/gameBet/report/consumeReport.html'">
        <div class="child-icon"><img src="<%=path%>/images/user/save/icon4.png" alt=""></div>
        <div class="child-title"><span>盈亏</span></div>
    </div>
    </c:if>
</div>
<!-- main-nav over -->
<!-- main-content -->
<div class="main-content">
    <ul class="list">
        <a href="<%=path%>/gameBet/order.html?status=bettingRecords">
            <li>
                <div class="container">
                    <img src="<%=path%>/images/user/save/i1.png" alt="" class="icon">
                    <span>投注记录</span>
                    <div class="right"><img src="<%=path%>/images/icon/icon-pointer.png" alt="" class="icon"></div>
                </div>
            </li>
        </a>
    </ul>

    <ul class="list">
        <a href="<%=path%>/gameBet/order.html?status=win">
            <li>
                <div class="container">
                    <img src="<%=path%>/images/user/save/i2.png" alt="" class="icon">
                    <span>中奖记录</span>
                    <div class="right"><img src="<%=path%>/images/icon/icon-pointer.png" alt="" class="icon"></div>
                </div>
            </li>
        </a>
        <a href="<%=path%>/gameBet/rechargeOrder.html">
            <li>
                <div class="container">
                    <img src="<%=path%>/images/user/save/i3.png" alt="" class="icon">
                    <span>充值记录</span>
                    <div class="right"><img src="<%=path%>/images/icon/icon-pointer.png" alt="" class="icon"></div>
                </div>
            </li>
        </a>
        <a href="<%=path%>/gameBet/withdrawOrder.html">
            <li>
                <div class="container">
                    <img src="<%=path%>/images/user/save/i4.png" alt="" class="icon">
                    <span>提现记录</span>
                    <div class="right"><img src="<%=path%>/images/icon/icon-pointer.png" alt="" class="icon"></div>
                </div>
            </li>
        </a>
        <c:if test="${user.dailiLevel!='REGULARMEMBERS'}">
        <a href="<%=path%>/gameBet/agentCenter.html">
            <li>
                <div class="container">
                    <img src="<%=path%>/images/user/save/i5.png" alt="" class="icon">
                    <span>代理中心</span>
                    <div class="right"><img src="<%=path%>/images/icon/icon-pointer.png" alt="" class="icon"></div>
                </div>
            </li>
        </a>
        </c:if>
        <c:if test="${user.dailiLevel!='REGULARMEMBERS'}">
        <a href="<%=path%>/gameBet/report/consumeReport.html">
            <li>
                <div class="container">
                    <img src="<%=path%>/images/user/save/i6.png" alt="" class="icon">
                    <span>盈亏报表</span>
                    <div class="right"><img src="<%=path%>/images/icon/icon-pointer.png" alt="" class="icon"></div>
                </div>
            </li>
        </a>
        </c:if>
    </ul>
    
    <ul class="list">
       <li class="logout" onclick="element_toggle(true,['.alert-bg','.alert'])">退出登录</li>
    </ul>
    
    <div class="alert-bg" onclick="element_toggle(false,['.alert'])"></div>
    <!-- alert -->
	<div class="alert alert-cart" id="lotteryOrderDialog" style="display: none">
		<div class="alert-content">是否退出登录？</div>
		<div class="alert-foot">
			<div class="btn color-gray cols-50" onclick="element_toggle(false,['.alert'])">取消</div>
			<div class="btn color-red cols-50" id="logout" >退出</div>
		</div>
	</div>
	<!-- alert over -->
</div>
<!-- main-content over -->
<!-- nav-bar -->
<jsp:include page="/front/include/navbar.jsp"></jsp:include>
<!-- nav-bar over -->

 <script src="<%=path%>/js/dydong.change.js"></script>
 <script src="<%=path%>/js/my/my.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
<script>
    animate_add(".main-head .head-message", "fadeInLeft");
    animate_add(".main-head .head-money", "fadeInLeft");
    animate_add(".main-head .head-referesh", "fadeInLeft");
    animate_add(".main-head .head-custom-msg", "fadeInRight");
    animate_add(".main-head .head-custom-btn", "fadeInRight");
    animate_add(".main-nav", "fadeInUp");
    animate_add(".main-content .list li", "fadeInUp");
</script>
</body>
</html>