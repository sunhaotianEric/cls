<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="format-detection" content="telephone=no">
    <title>奖金详情</title>
    <link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/money_award_detail.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
</head>
<body>
	<!-- header -->
	<div class="header color-dark">
	    <div class="header-left" onclick="window.history.go(-1)"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
	    <h2 class="header-title">奖金详情</h2>
	</div>
	<!-- header over -->


	<div class="stance"></div>

	<div class="main">
		<div class="main-head">
			<div class="container">
				<span class="font-red" onclick="change(0)">时时彩</span>
				<span onclick="change(1)">快三</span>
				<span onclick="change(2)">11选5</span>
				<span onclick="change(3)">PK10</span>
				<span onclick="change(4)">福彩3D</span>
			</div>
		</div>
		<div id="rebateId">
		
		</div>
		<!-- <div class="main-content" style="display:block;">
			<div class="container">
				<h2 class="main-title font-black">黑龙江时时彩</h2>
				<div class="line">
					<div class="sub"></div>
					<div class="child">
						<h2 class="money font-red">奖金 1800</h2>
						<h2 class="info font-gray">直选返点</h2>
					</div>
					<div class="child">
						<h2 class="info2 font-gray">不定位返点</h2>
					</div>
				</div>
			</div>
		</div>
		<div class="main-content">
			<div class="container">
				<h2 class="main-title font-black">11选5</h2>
				<div class="line">
					<div class="sub"></div>
					<div class="child">
						<h2 class="money font-red">奖金 1800</h2>
						<h2 class="info font-gray">直选返点</h2>
					</div>
					<div class="child">
						<h2 class="info2 font-gray">不定位返点</h2>
					</div>
				</div>
			</div>
		</div>
		<div class="main-content">
			<div class="container">
				<h2 class="main-title font-black">快乐彩</h2>
				<div class="line">
					<div class="sub"></div>
					<div class="child">
						<h2 class="money font-red">奖金 1800</h2>
						<h2 class="info font-gray">直选返点</h2>
					</div>
					<div class="child">
						<h2 class="info2 font-gray">不定位返点</h2>
					</div>
				</div>
			</div>
		</div>
		<div class="main-content">
			<div class="container">
				<h2 class="main-title font-black">快三</h2>
				<div class="line">
					<div class="sub"></div>
					<div class="child">
						<h2 class="money font-red">奖金 1800</h2>
						<h2 class="info font-gray">直选返点</h2>
					</div>
					<div class="child">
						<h2 class="info2 font-gray">不定位返点</h2>
					</div>
				</div>
			</div>
		</div>
		<div class="main-content">
			<div class="container">
				<h2 class="main-title font-black">低频</h2>
				<div class="line">
					<div class="sub"></div>
					<div class="child">
						<h2 class="money font-red">奖金 1800</h2>
						<h2 class="info font-gray">直选返点</h2>
					</div>
					<div class="child">
						<h2 class="info2 font-gray">不定位返点</h2>
					</div>
				</div>
			</div>
		</div> -->
	</div>

<!-- nav-bar -->
<jsp:include page="/front/include/navbar.jsp"></jsp:include>
<!-- nav-bar over -->
<jsp:include page="/front/include/include.jsp"></jsp:include>
<script src="<%=path%>/js/my/award_detail.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
</body>
</html>