<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
    <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="format-detection" content="telephone=no">
	<title>生成推广</title>
	<link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
	<link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
	<link href="<%=path%>/css/user_extend.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
	<link href="<%=path%>/css/consume_report_detail.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
</head>

<body>

	<!-- header -->
	<div class="header color-dark user_extend">
		<div class="header-left" onclick="window.history.go(-1);"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
		<div class="header-title" id="header-title">
			<div class="header-title-content">
				<div onclick="window.location.href='<%=path%>/gameBet/agentCenter/addExtend.html'" class="header-title-child header-title-child2 on">生成推广</div>
				<div onclick="window.location.href='<%=path%>/gameBet/agentCenter/extendMgr.html'" class="header-title-child header-title-child3 ">推广管理</div>
			</div>
		</div>
	</div>
	<!-- header over -->
	<div class="stance"></div>
	<div class="content on">
		<br>
		<br>
		<ul class="user-list">
			<!-- user-list 单选框 -->
			<li class="user-line">
				<div class="container">
					<div class="line-title">
						<p>开户类型</p>
					</div>
					<div class="line-content">
						<div class="radio">
							<input type="radio" name="userDailiType" value="ORDINARYAGENCY" checked="true"/>
							<div class="radioCheck"></div>
							<span>代理类型</span>
						</div>
						<div class="radio">
							<input type="radio" name="userDailiType" value="REGULARMEMBERS" />
							<div class="radioCheck"></div>
							<span>普通会员</span>
						</div>
					</div>
				</div>
			</li>
			<!-- user-list over 单选框 -->
		</ul>
		<ul class="user-list">
			<li class="user-line">
				<div class="container">
					<div class="line-title">
						<p>时时彩</p>
					</div>
					<div class="line-content">
						<div id="sscModeAdd" class="excel-btn excel-add"><img src="<%=path%>/images/icon/icon-addbtn.png" alt=""></div>
						<div class="excel-text"><input id='sscrebateValue' class="inputText" type="tel" value="" readonly="readonly"></div>
						<div id="sscModeReduction" class="excel-btn excel-sub"><img src="<%=path%>/images/icon/icon-subbtn.png" alt=""></div>
					</div>
					<div class="right font-red" id="sscTip">返点</div>
				</div>
			</li>
			<li class="user-line">
				<div class="container">
					<div class="line-title">
						<p>快三</p>
					</div>
					<div class="line-content">
						<div id="ksModeAdd" class="excel-btn excel-add"><img src="<%=path%>/images/icon/icon-addbtn.png" alt=""></div>
						<div class="excel-text"><input id="ksrebateValue" class="inputText" type="tel" value="" readonly="readonly"></div>
						<div id='ksModeReduction' class="excel-btn excel-sub"><img src="<%=path%>/images/icon/icon-subbtn.png" alt=""></div>
					</div>
					<div class="right font-red" id="ksTip">返点</div>
				</div>
			</li>
			<li class="user-line">
				<div class="container">
					<div class="line-title">
						<p>PK10</p>
					</div>
					<div class="line-content">
						<div id="pk10ModeAdd" class="excel-btn excel-add"><img src="<%=path%>/images/icon/icon-addbtn.png" alt=""></div>
						<div class="excel-text"><input id="pk10rebateValue" class="inputText" type="tel" value="" readonly="readonly"></div>
						<div id='pk10ModeReduction' class="excel-btn excel-sub"><img src="<%=path%>/images/icon/icon-subbtn.png" alt=""></div>
					</div>
					<div class="right font-red" id="pk10Tip">返点</div>
				</div>
			</li>
			<li class="user-line">
				<div class="container">
					<div class="line-title">
						<p>十一选五</p>
					</div>
					<div class="line-content">
						<div id="syxwModeAdd" class="excel-btn excel-add"><img src="<%=path%>/images/icon/icon-addbtn.png" alt=""></div>
						<div class="excel-text"><input id="syxwrebateValue" class="inputText" type="tel" value="" readonly="readonly"></div>
						<div id='syxwModeReduction' class="excel-btn excel-sub"><img src="<%=path%>/images/icon/icon-subbtn.png" alt=""></div>
					</div>
					<div class="right font-red" id="syxwTip">返点</div>
				</div>
			</li>
			<li class="user-line">
				<div class="container">
					<div class="line-title">
						<p>低频彩</p>
					</div>
					<div class="line-content">
						<div id="dpcModeAdd" class="excel-btn excel-add"><img src="<%=path%>/images/icon/icon-addbtn.png" alt=""></div>
						<div class="excel-text"><input id="dpcrebateValue" class="inputText" type="tel" value="" readonly="readonly"></div>
						<div id='dpcModeReduction' class="excel-btn excel-sub"><img src="<%=path%>/images/icon/icon-subbtn.png" alt=""></div>
					</div>
					<div class="right font-red" id="dpcTip">返点</div>
				</div>
			</li>
		</ul>



		<div id='addExtendLinkButton' class="user-btn color-red" onclick="!function(){modal_toggle('.user_extend_add-modal');userExtendChange('.user_extend .header-title-child2');}()">生成邀请码</div>
	</div>



	<!-- modal user_extend_add-modal -->
<%-- 	<div class="modal user_extend_add-modal animated out consume_report_detail">
		<!-- header -->
		<div class="header color-dark rechargewithdraw_fund">
			<div class="header-left" onclick="modal_toggle('.user_extend_add-modal')"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
			<h2 class="header-title">查看明细</h2>
		</div>
		<!-- header over -->
		<div class="stance"></div>

		<ul class="user-list">
			<!-- user-list 提示信息 -->
			<li class="user-msg">
				<p>当前返点 <span class="font-red">5.5</span> ,自身保留返点 <span class="font-red">2.0</span></p>
				<p>注册链接: http://www.baidu.com</p>
			</li>
			<!-- user-list over 提示信息 -->

		</ul>

		<!-- main -->
		<div class="main">

			<div class="main-child">
				<div class="child-value"><span>XDF243232</span></div>
				<div class="child-title"><span>邀请码</span></div>
			</div>
			<div class="main-child">
				<div class="child-value"><span>豪赌</span></div>
				<div class="child-title"><span>开户类型</span></div>
			</div>
			<div class="main-child">
				<div class="child-value"><span>疯狂模式</span></div>
				<div class="child-title"><span>模式</span></div>
			</div>
			<div class="main-child">
				<div class="child-value"><span>0.00</span></div>
				<div class="child-title"><span>当前返点</span></div>
			</div>
			<div class="main-child">
				<div class="child-value"><span>0.00</span></div>
				<div class="child-title"><span>自身返点</span></div>
			</div>


		</div>
		<div class="user-btn color-red" onclick="modal_toggle('.user_extend_add-modal')">确定</div>
		<!-- main over -->
		<div class="stance-nav"></div>
	</div> --%>
	<!-- modal user_extend_add-modal over -->



	<!-- nav-bar -->
	<jsp:include page="/front/include/navbar.jsp"></jsp:include>
	<!-- nav-bar over -->

	<jsp:include page="/front/include/include.jsp"></jsp:include>
    <script type="text/javascript" src="<%=path%>/js/user/add_extend.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
    
</body>

</html>