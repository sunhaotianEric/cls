<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
    <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="format-detection" content="telephone=no">
	<title>下级开户</title>
	<link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
	<link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
	<link href="<%=path%>/css/user_extend.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
</head>

<body>

	<!-- header -->
	<div class="header color-dark user_extend">
		<div class="header-left" onclick="window.history.go(-1);"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
		<h2 class="header-title" id="header-title">下级开户</h2>
	</div>
	<!-- header over -->
	<div class="stance"></div>
	<div class="content on">
		<br>
		<br>
		<ul class="user-list">
			<!-- user-list 单选框 -->
			<li class="user-line">
				<div class="container">
					<div class="line-title">
						<p>开户类型</p>
					</div>
					<div class="line-content">
						<div class="radio" id="agencyRadio">
							<input type="radio" name="userDailiType"  checked="checked" value = "ORDINARYAGENCY" id="userDailiType"/>
							<div class="radioCheck"></div>
							<span>代理</span>
						</div>
						<div class="radio" id="memberRadio">
							<input name="userDailiType"  value = "REGULARMEMBERS" id="userDailiType" type="radio" />
							<div class="radioCheck"></div>
							<span>玩家</span>
						</div>
					</div>
				</div>
			</li>
			<!-- user-list over 单选框 -->
		</ul>
		<ul class="user-list">
			<!-- user-list 输入框 -->
			<li class="user-line">
				<div class="container">
					<div class="line-title">
						<p>用户名</p>
					</div>
					<div class="line-content"><input id='addUserName' class="inputText" type="text" placeholder="4-10位字母数字,首位为字母"></div>
				</div>
			</li>
			<!-- user-list over 输入框 -->
			<!-- user-list 输入框 -->
			<li class="user-line">
				<div class="container">
					<div class="line-title">
						<p>密码</p>
					</div>
					<div class="line-content"><input id='userPassword' class="inputText" type="password" placeholder="6-15个字符,建议使用字母数字组合"></div>
				</div>
			</li>
			<!-- user-list over 输入框 -->
			<li class="user-line">
				<div class="container">
					<div class="line-title">
						<p>确认密码</p>
					</div>
					<div class="line-content"><input id='userPassword2' class="inputText" type="password" placeholder="6-15个字符,建议使用字母数字组合"></div>
				</div>
			</li>
		</ul>
		<ul class="user-list">
			<li class="user-line">
				<div class="container">
					<div class="line-title">
						<p>时时彩</p>
					</div>
					<div class="line-content">
						<div id="sscModeAdd" class="excel-btn excel-add"><img src="<%=path%>/images/icon/icon-addbtn.png" alt=""></div>
						<div class="excel-text"><input id="sscRebateValue" class="inputText" type="tel" value="" readonly="readonly"></div>
						<div id='sscModeReduction' class="excel-btn excel-sub"><img src="<%=path%>/images/icon/icon-subbtn.png" alt=""></div>
					</div>
					<div class="right font-red" id="sscTip">返点</div>
				</div>
			</li>
			<li class="user-line">
				<div class="container">
					<div class="line-title">
						<p>快三</p>
					</div>
					<div class="line-content">
						<div class="excel-btn excel-add" id="ksModeAdd"><img src="<%=path%>/images/icon/icon-addbtn.png" alt=""></div>
						<div class="excel-text"><input id="ksRebateValue" class="inputText" type="tel" value="" readonly="readonly"></div>
						<div id='ksModeReduction' class="excel-btn excel-sub"><img src="<%=path%>/images/icon/icon-subbtn.png" alt=""></div>
					</div>
					<div class="right font-red" id="ksTip">返点</div>
				</div>
			</li>
			<li class="user-line">
				<div class="container">
					<div class="line-title">
						<p>PK10</p>
					</div>
					<div class="line-content">
						<div id="pk10ModeAdd" class="excel-btn excel-add"><img src="<%=path%>/images/icon/icon-addbtn.png" alt=""></div>
						<div class="excel-text"><input id="pk10RebateValue" class="inputText" type="tel" value="" readonly="readonly"></div>
						<div id='pk10ModeReduction' class="excel-btn excel-sub"><img src="<%=path%>/images/icon/icon-subbtn.png" alt=""></div>
					</div>
					<div class="right font-red" id="pk10Tip">返点</div>
				</div>
			</li>
			<li class="user-line">
				<div class="container">
					<div class="line-title">
						<p>十一选五</p>
					</div>
					<div class="line-content">
						<div id="syxwModeAdd" class="excel-btn excel-add"><img src="<%=path%>/images/icon/icon-addbtn.png" alt=""></div>
						<div class="excel-text"><input id="syxwRebateValue" class="inputText" type="tel" value="" readonly="readonly"></div>
						<div id='syxwModeReduction' class="excel-btn excel-sub"><img src="<%=path%>/images/icon/icon-subbtn.png" alt=""></div>
					</div>
					<div class="right font-red" id="syxwTip">返点</div>
				</div>
			</li>
			<li class="user-line">
				<div class="container">
					<div class="line-title">
						<p>低频彩</p>
					</div>
					<div class="line-content">
						<div id="dpcModeAdd" class="excel-btn excel-add"><img src="<%=path%>/images/icon/icon-addbtn.png" alt=""></div>
						<div class="excel-text"><input id="dpcRebateValue" class="inputText" type="tel" value="" readonly="readonly"></div>
						<div id='dpcModeReduction' class="excel-btn excel-sub"><img src="<%=path%>/images/icon/icon-subbtn.png" alt=""></div>
					</div>
					<div class="right font-red" id="dpcTip">返点</div>
				</div>
			</li>
		</ul>

		<!-- <ul class="user-list no">
			user-list 提示信息
			<li class="user-msg">
				<p>当前返点 <span class="font-red">5.5</span> ,自身保留返点 <span class="font-red">2.0</span></p>
			</li>
			user-list over 提示信息
		</ul> -->
		<div class="user-btn color-red" id="addUserButton">确认开户</div>

	</div>

	<!-- nav-bar -->
	<jsp:include page="/front/include/navbar.jsp"></jsp:include>
	<!-- nav-bar over -->


	<!-- <script src="assets/jquery/jquery-3.1.1.min.js"></script>
	<script src="js/base.js"></script> -->
    <jsp:include page="/front/include/include.jsp"></jsp:include>
    <script type="text/javascript" src="<%=path%>/js/user/add_user.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
</body>

</html>