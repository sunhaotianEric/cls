<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
    <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="format-detection" content="telephone=no">
	<title>推广管理</title>
	<link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
	<link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
	<link href="<%=path%>/css/user_extend.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
</head>

<body>



	<!-- header -->
	<div class="header color-dark user_extend">
		<div class="header-left" onclick="window.history.go(-1);"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
		<div class="header-title" id="header-title">
			<div class="header-title-content">
				<div onclick="window.location.href='<%=path%>/gameBet/agentCenter/addExtend.html'" class="header-title-child header-title-child2">生成推广</div>
				<div onclick="window.location.href='<%=path%>/gameBet/agentCenter/extendMgr.html'" class="header-title-child header-title-child3 on">推广管理</div>
			</div>
		</div>
	</div>
	<!-- header over -->





	<div class="stance"></div>



	<div class="content on">
		
		<ul class="user-list list">
			<div class="titles">
				<div class="child child1">邀请码</div>
				<div class="child child2">生成时间</div>
				<div class="child child3">使用次数</div>
			</div>
			<div class="contents" id="extendLinkList">
				<!-- <div class="line">
					<div class="child child1 font-red" onclick="modal_toggle('.user_extend_add-modal');">892015</div>
					<div class="child child2">2016-12-08 15:38:06</div>
					<div class="child child3">注册(2)</div>
				</div>
				<div class="line">
					<div class="child child1 font-red" onclick="modal_toggle('.user_extend_add-modal');">892015</div>
					<div class="child child2">2016-12-08 15:38:06</div>
					<div class="child child3">注册(2)</div>
				</div>
				<div class="line">
					<div class="child child1 font-red" onclick="modal_toggle('.user_extend_add-modal');">892015</div>
					<div class="child child2">2016-12-08 15:38:06</div>
					<div class="child child3">注册(2)</div>
				</div>
				<div class="line">
					<div class="child child1 font-red" onclick="modal_toggle('.user_extend_add-modal');">892015</div>
					<div class="child child2">2016-12-08 15:38:06</div>
					<div class="child child3">注册(2)</div>
				</div>
				<div class="line">
					<div class="child child1 font-red" onclick="modal_toggle('.user_extend_add-modal');">892015</div>
					<div class="child child2">2016-12-08 15:38:06</div>
					<div class="child child3">注册(2)</div>
				</div>
				<div class="line">
					<div class="child child1 font-red" onclick="modal_toggle('.user_extend_add-modal');">892015</div>
					<div class="child child2">2016-12-08 15:38:06</div>
					<div class="child child3">注册(2)</div>
				</div> -->
			</div>

			<div class="user-show user-btn color-red">显示全部记录</div>

		</ul>


	</div>
<!-- modal user_extend_add-modal -->
<div class="modal user_extend_add-modal animated out consume_report_detail">
	<!-- header -->
	<div class="header color-dark rechargewithdraw_fund">
		<div class="header-left" onclick="modal_toggle('.user_extend_add-modal')"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
		<h2 class="header-title">查看明细</h2>
	</div>
	<!-- header over -->
	<div class="stance"></div>

	<ul class="user-list">
		<!-- user-list 提示信息 -->
		<li class="user-msg" id="reglink_li">
		<!-- <p>当前返点 <span class="font-blue">5.5</span> ,自身保留返点 <span class="font-blue">2.0</span></p> -->
		<p ><span >注册链接</span></p>
		<p><span style="color:#50a3ff;">http://www.baidu.com</span></p>
		<p><span style="color:#50a3ff;">http://www.baidu.com</span></p>
		</li>
		<!-- user-list over 提示信息 -->

	</ul>

	<!-- main -->
	<div class="main">
		
		<div class="main-child">
			<div class="child-value"><span id="invitationCode">XDF243232</span></div>
			<div class="child-title"><span>邀请码</span></div>
		</div>
		<div class="main-child">
			<div class="child-value"><span id="detailDaiLiType">豪赌</span></div>
			<div class="child-title"><span>开户类型</span></div>
		</div>
		<div class="main-child">
			<div class="child-value"><span id="detailSscrebate">-</span></div>
			<div class="child-title"><span>时时彩模式-返点</span></div>
		</div>
		<div class="main-child">
			<div class="child-value"><span id="detailKsrebate">-</span></div>
			<div class="child-title"><span>快三模式-返点</span></div>
		</div>
		<div class="main-child">
			<div class="child-value"><span id="detailPk10rebate">-</span></div>
			<div class="child-title"><span>PK10模式-返点</span></div>
		</div>
		<div class="main-child">
			<div class="child-value"><span id="detailSyxwrebate">-</span></div>
			<div class="child-title"><span>11选5模式-返点</span></div>
		</div>
		<div class="main-child">
			<div class="child-value"><span id="detailDpcrebate">-</span></div>
			<div class="child-title"><span>低频彩模式-返点</span></div>
		</div>
		<input type="hidden" id="deleteLinkId" value="">
	<!-- 	<div class="main-child">
			<div class="child-value"><span>0.00</span></div>
			<div class="child-title"><span>当前返点</span></div>
		</div>
		<div class="main-child">
			<div class="child-value"><span>0.00</span></div>
			<div class="child-title"><span>自身返点</span></div>
		</div> -->
		

	</div>
	<div class="user-btn color-red" id="delLinkDiv" >删除</div>
	<!-- main over -->
	<div class="stance-nav"></div>
</div>
<!-- modal user_extend_add-modal over -->


	<!-- nav-bar -->
	<jsp:include page="/front/include/navbar.jsp"></jsp:include>
	<!-- nav-bar over -->


	<%-- <script src="<%=path%>/assets/jquery/jquery-3.1.1.min.js"></script>
	<script src="<%=path%>/js/base.js"></script> --%>
	<jsp:include page="/front/include/include.jsp"></jsp:include>
    <script type="text/javascript" src="<%=path%>/js/user/extend_mgr.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
   
</body>

</html>