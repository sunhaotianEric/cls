<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="format-detection" content="telephone=no">
    <title>团队列表</title>
	<link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
	<link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
	<link href="<%=path%>/css/user_team_list_users.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
</head>
<body>

<!-- header -->
<div class="header color-dark">
	<div class="header-left" onclick="window.history.go(-1)"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
	<h2 class="header-title">团队列表</h2>
</div>
<!-- header over -->


<div class="stance"></div>

<br>
<br>
<ul class="user-list">

	<li class="user-line">
		<div class="container">
			<div class="line-title">
				<p>类型:</p>
			</div>
			<div class="line-content">
				<select class="inputSelect" name="" id="dailiLevelId">
					<option value="">全部</option>
					<option value="AGENCY">代理</option>
					<option value="REGULARMEMBERS">会员</option>
				</select>
			</div>
			<div class="line-title">
				<p>排序:</p>
			</div>
			<div class="line-content">
				<select class="inputSelect" name="" id="orderSortId">
					<option value="5">注册时间</option>
					<option value="3">账号余额</option>
					<option value="1">模式</option>
				</select>
			</div>
		</div>
	</li>
	<li class="user-line">
		<div class="container">
			<div class="line-title">
				<p>用户:</p>
			</div>
			<div class="line-content">
				<input class="inputText" type="text" placeholder="请输入用户名" id="userId">
			</div>
			<div class="right color-red btn-middle" onclick="userListPage.loadSubTeamListUser()">查询</div>
		</div>
	</li>
</ul>

<ul class="user-list list">
	<div class="titles">
		<div class="child child1">用户名</div>
		<div class="child child2">余额</div>
		<div class="child child3">级别</div>
		<div class="child child3" style="width:220px">注册时间</div>
		<div class="child child4" style="width:120px">人数</div>
		<div class="child child5" style="width:120px">模式</div>
	</div>
	<div class="contents">
		<div  id="teamUserListId">
			
		</div>
<!-- 		<div class="line">
			<a href="#">
				<div class="child child1 font-red">abc</div>
			</a>
			<div class="child child2">100.00</div>
			<div class="child child3">1</div>
			<div class="child child3">2</div>
			<div class="child child3">3</div>
			<div class="child child4">28</div>
			<div class="child child5">1954</div>
		</div>
		<div class="line">
			<a href="#">
				<div class="child child1 font-red">abc</div>
			</a>
			<div class="child child2">100.00</div>
			<div class="child child3">1</div>
			<div class="child child3">2</div>
			<div class="child child3">3</div>
			<div class="child child4">28</div>
			<div class="child child5">1954</div>
		</div>
		<div class="line">
			<a href="#">
				<div class="child child1 font-red">abc</div>
			</a>
			<div class="child child2">100.00</div>
			<div class="child child3">1</div>
			<div class="child child3">2</div>
			<div class="child child3">3</div>
			<div class="child child4">28</div>
			<div class="child child5">1954</div>
		</div>
		<div class="line">
			<a href="#">
				<div class="child child1 font-red">abc</div>
			</a>
			<div class="child child2">100.00</div>
			<div class="child child3">1</div>
			<div class="child child3">2</div>
			<div class="child child3">3</div>
			<div class="child child4">28</div>
			<div class="child child5">1954</div>
		</div>
		<div class="line">
			<a href="#">
				<div class="child child1 font-red">abc</div>
			</a>
			<div class="child child2">100.00</div>
			<div class="child child3">1</div>
			<div class="child child3">2</div>
			<div class="child child3">3</div>
			<div class="child child4">28</div>
			<div class="child child5">1954</div>
		</div> -->
	</div>

	<div class="user-btn color-red more">显示更多</div>

</ul>
<jsp:include page="/front/include/navbar.jsp"></jsp:include>
<!-- nav-bar over -->
<jsp:include page="/front/include/include.jsp"></jsp:include>

<script src="<%=path%>/js/user/user_list.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
</body>
</html>