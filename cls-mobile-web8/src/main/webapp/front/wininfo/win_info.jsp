<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%> 
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="format-detection" content="telephone=no">
    <title>中奖信息</title>
    <link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/award_message.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
</head>

<body>

    <!-- header -->
    <div class="header color-dark">
        <div class="header-title" id="header-title">
            <div class="header-title-content">
              <div class="header-title-child header-title-child1 on" onclick="window.location.href='<%=path%>/gameBet/wininfo.html'">中奖信息</div>
              <div class="header-title-child header-title-child2 " onclick="window.location.href='<%=path%>/gameBet/lotterycode.html'">开奖号码</div>
            </div>
        </div>
    </div>
    <!-- header over -->
    <div class="stance"></div>
    <!-- main -->
    <div class="main">

        <!-- <div class="line">
            <div class="line-headimg"><img src="images/headimg.png" alt=""></div>
            <div class="line-content">
                <h2>Douews:</h2>
                <p>在K8 时时彩喜中 <span class="font-red">¥112.8</span></p>
            </div>
        </div>
        <div class="line">
            <div class="line-headimg"><img src="images/headimg.png" alt=""></div>
            <div class="line-content">
                <h2>Douews:</h2>
                <p>在K8 时时彩喜中 <span class="font-red">¥112.8</span></p>
            </div>
        </div>
        <div class="line">
            <div class="line-headimg"><img src="images/headimg.png" alt=""></div>
            <div class="line-content">
                <h2>Douews:</h2>
                <p>在K8 时时彩喜中 <span class="font-red">¥112.8</span></p>
            </div>
        </div>
        <div class="line">
            <div class="line-headimg"><img src="images/headimg.png" alt=""></div>
            <div class="line-content">
                <h2>Douews:</h2>
                <p>在K8 时时彩喜中 <span class="font-red">¥112.8</span></p>
            </div>
        </div>
        <div class="line">
            <div class="line-headimg"><img src="images/headimg.png" alt=""></div>
            <div class="line-content">
                <h2>Douews:</h2>
                <p>在K8 时时彩喜中 <span class="font-red">¥112.8</span></p>
            </div>
        </div>
        <div class="line">
            <div class="line-headimg"><img src="images/headimg.png" alt=""></div>
            <div class="line-content">
                <h2>Douews:</h2>
                <p>在K8 时时彩喜中 <span class="font-red">¥112.8</span></p>
            </div>
        </div>
        <div class="line">
            <div class="line-headimg"><img src="images/headimg.png" alt=""></div>
            <div class="line-content">
                <h2>Douews:</h2>
                <p>在K8 时时彩喜中 <span class="font-red">¥112.8</span></p>
            </div>
        </div>
        <div class="line">
            <div class="line-headimg"><img src="images/headimg.png" alt=""></div>
            <div class="line-content">
                <h2>Douews:</h2>
                <p>在K8 时时彩喜中 <span class="font-red">¥112.8</span></p>
            </div>
        </div>
        <div class="line">
            <div class="line-headimg"><img src="images/headimg.png" alt=""></div>
            <div class="line-content">
                <h2>Douews:</h2>
                <p>在K8 时时彩喜中 <span class="font-red">¥112.8</span></p>
            </div>
        </div> -->


    </div>
    <!-- main over -->
    <!-- nav-bar -->
    <jsp:include page="/front/include/navbar.jsp"></jsp:include>
    <!-- nav-bar over -->

    <jsp:include page="/front/include/include.jsp"></jsp:include>
    <script>
        animate_add(".main", "fadeInUp");
    </script>
    <script src="<%=path%>/js/wininfo/win_info.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>

</body>

</html>