<%@ page contentType="text/html;charset=UTF-8" language="java"
	pageEncoding="UTF-8"%>
<%@page
	import="com.team.lottery.vo.LotterySet,com.team.lottery.cache.ClsCacheManager,com.team.lottery.system.SystemConfigConstant,
	com.team.lottery.system.BizSystemConfigVO,com.team.lottery.system.BizSystemConfigManager,java.math.BigDecimal,com.team.lottery.vo.BizSystemInfo"%>
<%@page
	import="java.text.SimpleDateFormat,com.team.lottery.system.SingleLoginForRedis"%>
<%@page import="java.util.Date"%>
<%
	String path = request.getContextPath();
	String basePath1 = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ request.getContextPath() + "/";

	String serverName = request.getServerName();//获取当前域名
	String bizSystem = ClsCacheManager
			.getBizSystemByDomainUrl(serverName);
	BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager
			.getBizSystemConfig(bizSystem);//业务系统彩种设置
	BizSystemInfo bizSystemInfo = ClsCacheManager
			.getBizSystemInfo(bizSystem);

	int genralagencyNum = bizSystemConfigVO.getGenralagencyNum();
	int directagencyNum = bizSystemConfigVO.getDirectagencyNum();
	int ordinayagencyNum = bizSystemConfigVO.getOrdinayagencyNum();

	BigDecimal canlotteryhightModel = bizSystemConfigVO
			.getCanlotteryhightModel();
	String secretCode = bizSystemConfigVO.getSecretCode();
	String defaultInvitationCode = bizSystemConfigVO
			.getDefaultInvitationCode();

	int isOpenLevelAward = bizSystemConfigVO.getIsOpenLevelAward();
	BigDecimal withDrawOneStrokeLowestMoney = bizSystemConfigVO
			.getWithDrawOneStrokeLowestMoney();
	BigDecimal withDrawOneStrokeHighestMoney = bizSystemConfigVO
			.getWithDrawOneStrokeHighestMoney();
	BigDecimal withDrawEveryDayTotalMoney = bizSystemConfigVO
			.getWithDrawEveryDayTotalMoney();
	int withDrawEveryDayNumber = bizSystemConfigVO
			.getWithDrawEveryDayNumber();

	BigDecimal exceedWithdrawExpenses = bizSystemConfigVO
			.getExceedWithdrawExpenses();
	BigDecimal exceedTransferDayLimitExpenses = bizSystemConfigVO
			.getExceedTransferDayLimitExpenses();
	int withDrawIsAllowNotEnough = bizSystemConfigVO
			.getWithDrawIsAllowNotEnough();
	String releasePolicyBonus = bizSystemConfigVO
			.getReleasePolicyBonus();
	String wreleasePolicyDaySaraly = bizSystemConfigVO
			.getReleasePolicyDaySaraly();
	int maintainSwich = bizSystemConfigVO.getMaintainSwich();
	String maintainContent = bizSystemConfigVO.getMaintainContent();
	int yuanModel = bizSystemConfigVO.getYuanModel();
	int jiaoModel = bizSystemConfigVO.getJiaoModel();
	int fenModel = bizSystemConfigVO.getFenModel();
	BigDecimal lhcLowerLotteryMoney = bizSystemConfigVO
			.getLhcLowerLotteryMoney();
	BigDecimal lhcHighestExpectLotteryMoney = bizSystemConfigVO
			.getLhcHighestExpectLotteryMoney();
	BigDecimal lhcHighestLotteryMoney = bizSystemConfigVO
			.getLhcHighestLotteryMoney();
	BigDecimal lhcMaxWinMoney = bizSystemConfigVO.getLhcMaxWinMoney();
	int isClose = bizSystemConfigVO.getIsClose();
	int lotteryAlarmOpen = bizSystemConfigVO.getLotteryAlarmOpen();
	BigDecimal lotteryAlarmValue = bizSystemConfigVO
			.getLotteryAlarmValue();
	BigDecimal rechargeAlarmValue = bizSystemConfigVO
			.getRechargeAlarmValue();
	int rechargeAlarmOpen = bizSystemConfigVO.getRechargeAlarmOpen();
	int alarmEmailOpen = bizSystemConfigVO.getAlarmEmailOpen();
	String alarmEmail = bizSystemConfigVO.getAlarmEmail();
	int winAlarmOpen = bizSystemConfigVO.getWinAlarmOpen();
	BigDecimal winAlarmValue = bizSystemConfigVO.getWinAlarmValue();
	int withdrawAlarmOpen = bizSystemConfigVO.getWithdrawAlarmOpen();
	BigDecimal withdrawAlarmValue = bizSystemConfigVO
			.getWithdrawAlarmValue();
	int alarmPhoneOpen = bizSystemConfigVO.getAlarmPhoneOpen();
	String alarmPhone = bizSystemConfigVO.getAlarmPhone();
	int regShowPhone = bizSystemConfigVO.getRegShowPhone();
	int regRequiredPhone = bizSystemConfigVO.getRegRequiredPhone();

	int regShowEmail = bizSystemConfigVO.getRegShowEmail();
	int regRequiredEmail = bizSystemConfigVO.getRegRequiredEmail();
	int regShowQq = bizSystemConfigVO.getRegShowQq();
	int regRequiredQq = bizSystemConfigVO.getRegRequiredQq();
	int guestModel = bizSystemConfigVO.getGuestModel();

	int allowGuestLogin = bizSystemConfigVO.getAllowGuestLogin();
	BigDecimal guestMoney = bizSystemConfigVO.getGuestMoney();

	if (releasePolicyBonus == "MANANUAL_RELEASE") {
		releasePolicyBonus = "手动发放";
	} else if (releasePolicyBonus == "AUTO_DOWN_RELEASE") {
		releasePolicyBonus = "自动逐级发放";
	} else {
		releasePolicyBonus = "自动优先发放";
	}

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta id="viewport" name="viewport"
	content="maximum-scale=1, user-scalable=no, width=device-width">
<meta name="format-detection" content="telephone=no">
<title>监控查看</title>
<script type="text/javascript"
	src="<%=path%>/assets/jquery/jquery-1.11.3.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		// 获取当前服务器路径用于路径跳转;
		var address = window.location.href;
		var url = document.getElementById("address");
		url.innerHTML = "当前前端获取url:"+address;
	});
</script>
</head>

<body>
	<div>

		<p>当前后端获取url:<%= basePath1 %></p> 
		<p id="address"></p>
		<input type="button" value="返回"
			onclick="window.location.href='<%=path%>/gameBet/index.html'">
	</div>



	<h1>
		<span style="color: red"><%=bizSystem%></span>系统参数配置
	</h1>
	<br />
	<p>
		总代理人数配置:<span style="color: red;"><%=genralagencyNum%></span>人
	</p>
	<p>
		直属代理人数配置:<span style="color: red;"><%=directagencyNum%></span>人
	</p>
	<p>
		普通代理人数配置:<span style="color: red;"><%=ordinayagencyNum%></span>人
	</p>
	<p>
		允许投注最高模式:<span style="color: red;"><%=canlotteryhightModel%></span>
		,默认邀请码:<span style="color: red;"><%=defaultInvitationCode%></span>
	</p>
	<p>
		秘密验证码:<span style="color: red;"><%=secretCode%> </span>,等级制度奖励:<span
			style="color: red;"><%=isOpenLevelAward%></span>
	</p>
	<p>
		单笔取现最低金额:<span style="color: red;"><%=withDrawOneStrokeLowestMoney%></span>
		,每天取现总金额:<span style="color: red;"><%=withDrawEveryDayTotalMoney%></span>
	</p>
	<p>
		单笔取现最高金额:<span style="color: red;"><%=withDrawOneStrokeHighestMoney%></span>
		,每天取现次数:<span style="color: red;"><%=withDrawEveryDayNumber%></span>
	</p>
	<p>
		提现的手续费率:<span style="color: red;"><%=exceedWithdrawExpenses%></span>
		,每日转账限额:<span style="color: red;"><%=exceedTransferDayLimitExpenses%></span>
	</p>
	<p>
		可提现不足不允许提现:<span style="color: red;"><%=withDrawIsAllowNotEnough%></span>
		,契约分红发放策略:<span style="color: red;"><%=releasePolicyBonus%></span>
	</p>
	<p>
		单笔取现最高金额:<span style="color: red;"><%=withDrawOneStrokeHighestMoney%></span>
		,平台维护开关:<span style="color: red;"><%=maintainSwich == 0 ? "关" : "开"%></span>
	</p>
	<p>
		提现的手续费率:<span style="color: red;"><%=exceedWithdrawExpenses%></span>
		,每日转账限额:<span style="color: red;"><%=exceedTransferDayLimitExpenses%></span>
	</p>
	<p>
		日工资发放策略:<span style="color: red;"><%=wreleasePolicyDaySaraly%></span>
	</p>

	<p>
		平台维护说明:<span style="color: red;"><%=maintainContent%></span> ,元模式:<span
			style="color: red;"><%=yuanModel == 0 ? "关" : "开"%></span>
	</p>
	<p>
		角模式:<span style="color: red;"><%=jiaoModel == 0 ? "关" : "开"%></span>
		,分模式:<span style="color: red;"><%=fenModel == 0 ? "关" : "开"%></span>
	</p>
	<p>
		六合彩单注最低限额:<span style="color: red;"><%=lhcLowerLotteryMoney%></span>
		六合彩单场最高投注额:<span style="color: red;"><%=lhcHighestExpectLotteryMoney%></span>
	</p>
	<p>
		六合彩单注最高限额:<span style="color: red;"><%=lhcHighestLotteryMoney%></span>
		,六合彩最高返奖限额:<span style="color: red;"><%=lhcMaxWinMoney%></span>
	</p>
	<p>
		六合是否封盘:<span style="color: red;"><%=isClose == 0 ? "关" : "开"%></span>
	</p>
	<p>
		投注监控开关:<span style="color: red;"><%=lotteryAlarmOpen == 0 ? "关" : "开"%></span>
		,投注监控金额:<span style="color: red;"><%=lotteryAlarmValue%></span>
	</p>
	<p>
		充值监控开关:<span style="color: red;"><%=rechargeAlarmOpen == 0 ? "关" : "开"%></span>
		,充值监控金额:<span style="color: red;"><%=rechargeAlarmValue%></span>
	</p>
	<p>
		是否邮件通知:<span style="color: red;"><%=alarmEmailOpen == 0 ? "关" : "开"%></span>
		,邮箱地址:<span style="color: red;"><%=alarmEmail%></span>
	</p>
	<p>
		中奖监控开关:<span style="color: red;"><%=winAlarmOpen == 0 ? "关" : "开"%></span>
		,中奖监控金额:<span style="color: red;"><%=winAlarmValue%></span>
	</p>
	<p>
		取现监控开关:<span style="color: red;"><%=withdrawAlarmOpen == 0 ? "关" : "开"%></span>
		,取现监控金额:<span style="color: red;"><%=withdrawAlarmValue%></span>
	</p>
	<p>
		是否短信通知:<span style="color: red;"><%=alarmPhoneOpen == 0 ? "否" : "是"%></span>
		,手机号码:<span style="color: red;"><%=alarmPhone%></span>
	</p>
	<p>
		注册是否显示手机号码:<span style="color: red;"><%=regShowPhone == 0 ? "否" : "是"%></span>
		,注册手机号码是否必填:<span style="color: red;"><%=regRequiredPhone == 0 ? "否" : "是"%></span>
	</p>
	<p>
		注册是否显示邮件:<span style="color: red;"><%=regShowEmail == 0 ? "否" : "是"%></span>
		,注册邮件是否必填:<span style="color: red;"><%=regRequiredEmail == 0 ? "否" : "是"%></span>
	</p>
	<p>
		注册是否显示QQ:<span style="color: red;"><%=regShowQq%></span> ,注册QQ是否必填:<span
			style="color: red;"><%=regRequiredQq == 0 ? "否" : "是"%></span>
	</p>
	<p>
		注册是否显示手机号码:<span style="color: red;"><%=regShowPhone == 0 ? "否" : "是"%></span>
		,注册手机号码是否必填:<span style="color: red;"><%=regRequiredPhone == 0 ? "否" : "是"%></span>
	</p>
	<p>
		注册是否显示邮件:<span style="color: red;"><%=regShowEmail == 0 ? "否" : "是"%></span>
		,注册邮件是否必填:<span style="color: red;"><%=regRequiredEmail == 0 ? "否" : "是"%></span>
	</p>
	<p>
		是否开启试玩功能:<span style="color: red;"><%=guestModel == 0 ? "关" : "开"%></span>
		,是否允许试玩账号登录:<span style="color: red;"><%=allowGuestLogin == 0 ? "否" : "是"%></span>
	</p>
	<p>
		注册是否显示手机号码:<span style="color: red;"><%=regShowPhone == 0 ? "否" : "是"%></span>
		,注册手机号码是否必填:<span style="color: red;"><%=regRequiredPhone == 0 ? "否" : "是"%></span>
	</p>
	<p>
		试玩账号默认金钱:<span style="color: red;"><%=guestMoney%></span>
	</p>

	<br />
	<br />
	<br />
	<h1>系统全局参数配置</h1>
	<br />
	<br />
	<p>
		默认后台系统分页条数：<span style="color: red;"><%=SystemConfigConstant.defaultMgrPageSize%></span>,默认前台系统分页条数：<span
			style="color: red;"><%=SystemConfigConstant.frontDefaultPageSize%></span>
	</p>
	<p>
		单点登陆开关：<span style="color: red;"><%=SystemConfigConstant.isStationLogin == 0 ? "关" : "开"%></span>,后台登陆开关：<span
			style="color: red;"><%=SystemConfigConstant.managerLoginSwich == 0 ? "关" : "开"%></span>
	</p>
	<p>
		追号开关：<span style="color: red;"><%=SystemConfigConstant.allowZhuihao == 0 ? "关" : "开"%></span>
	</p>
	<p>
		春节开始时间：<span style="color: red;"><%=SystemConfigConstant.springFestivalStartDate%></span>,春节结束时间：<span
			style="color: red;"><%=SystemConfigConstant.springFestivalEndDate%></span>
	</p>
	<p>
		幸运分分彩盈利模式：<span style="color: red;"><%=SystemConfigConstant.jlffcProfitModel%></span>,幸运快三盈利模式：<span
			style="color: red;"><%=SystemConfigConstant.jyksProfitModel%></span>
	</p>
	<p>
		平台维护开关：<span style="color: red;"><%=SystemConfigConstant.maintainSwich == "0" ? "关" : "开"%></span>,平台系统维护说明：<span
			style="color: red;"><%=SystemConfigConstant.maintainContent%></span>
	</p>

	<p>
		PK10基准日期：<span style="color: red;"><%=SystemConfigConstant.bjksBaseDate%></span>,PK10基准期数：<span
			style="color: red;"><%=SystemConfigConstant.pk10BasetExpect%></span>
	</p>
	<p>
		韩国1.5分彩基准日期：<span style="color: red;"><%=SystemConfigConstant.hgffcBaseDate%></span>,韩国1.5基准期数：<span
			style="color: red;"><%=SystemConfigConstant.hgffcBasetExpect%></span>
	</p>
	<p>
		北京快3基准日期：<span style="color: red;"><%=SystemConfigConstant.bjksBaseDate%></span>,北京快3基准期数：<span
			style="color: red;"><%=SystemConfigConstant.bjksBasetExpect%></span>
	</p>
	<p>
		新加坡2分彩基准日期：<span style="color: red;"><%=SystemConfigConstant.xjplfcBaseDate%></span>,新加坡2分彩基准期数：<span
			style="color: red;"><%=SystemConfigConstant.xjplfcBasetExpect%></span>
	</p>
	<p>
		台湾5分彩基准日期：<span style="color: red;"><%=SystemConfigConstant.xyebBaseDate%></span>,台湾5分彩基准期数：<span
			style="color: red;"><%=SystemConfigConstant.twwfcBasetExpect%></span>
	</p>
	<p>
		幸运28基准日期：<span style="color: red;"><%=SystemConfigConstant.xyebBaseDate%></span>,幸运28分彩基准期数：<span
			style="color: red;"><%=SystemConfigConstant.xyebBasetExpect%></span>
	</p>

	<p>
		时时彩最低奖金模式：<span style="color: red;"><%=SystemConfigConstant.SSCLowestAwardModel%></span>,时时彩最高奖金模式：<span
			style="color: red;"><%=SystemConfigConstant.SSCLowestAwardModel%></span>
	</p>
	<p>
		分分彩最低奖金模式：<span style="color: red;"><%=SystemConfigConstant.FFCLowestAwardModel%></span>,分分彩奖最高金模式：<span
			style="color: red;"><%=SystemConfigConstant.FFCHighestAwardModel%></span>
	</p>
	<p>
		11选5奖最低金模式：<span style="color: red;"><%=SystemConfigConstant.SYXWLowestAwardModel%></span>,11选5奖最高金模式：<span
			style="color: red;"><%=SystemConfigConstant.SYXWHighestAwardModel%></span>
	</p>
	<p>
		快3奖最低金模式：<span style="color: red;"><%=SystemConfigConstant.KSLowestAwardModel%></span>,快3奖最高金模式：<span
			style="color: red;"><%=SystemConfigConstant.KSHighestAwardModel%></span>
	</p>

	<p>
		PK10最低奖金模式：<span style="color: red;"><%=SystemConfigConstant.PK10LowestAwardModel%></span>,PK10最高奖金模式：<span
			style="color: red;"><%=SystemConfigConstant.PK10HighestAwardModel%></span>
	</p>
	<p>
		低频彩奖最低金模式：<span style="color: red;"><%=SystemConfigConstant.DPCLowestAwardModel%></span>,低频彩奖最高金模式：<span
			style="color: red;"><%=SystemConfigConstant.DPCHighestAwardModel%></span>
	</p>
	<p>
		骰宝最低奖金模式：<span style="color: red;"><%=SystemConfigConstant.TBLowestAwardModel%></span>,骰宝最高奖金模式：<span
			style="color: red;"><%=SystemConfigConstant.TBHighestAwardModel%></span>
	</p>
	<p>
		六合彩最低奖金模式：<span style="color: red;"><%=SystemConfigConstant.LHCLowestAwardModel%></span>,六合彩最高奖金模式：<span
			style="color: red;"><%=SystemConfigConstant.LHCHighestAwardModel%></span>
	</p>
	<p>
		快乐十分奖最低金模式：<span style="color: red;"><%=SystemConfigConstant.KLSFLowestAwardModel%></span>,快乐十分奖最高金模式：<span
			style="color: red;"><%=SystemConfigConstant.KLSFHighestAwardModel%></span>
	</p>
	<p>
		允许放过的地址：<span style="color: red;"><%=SystemConfigConstant.allowUrls%></span>,当前生肖属性：<span
			style="color: red;"><%=SystemConfigConstant.currentZodiac%></span>
	</p>
	<p>
		pc站点资源版本号：<span style="color: red;"><%=SystemConfigConstant.pcWebRsVersion%></span>,手机站点资源版本号：<span
			style="color: red;"><%=SystemConfigConstant.mobileWebRsVersion%></span>
	</p>
	<p>
		后台站点资源版本号：<span style="color: red;"><%=SystemConfigConstant.mgrWebRsVersion%></span>
	</p>
	<br />
	<br />
	<br />
	<br />
	<br />
	<h1>彩种开关</h1>

	<p>
		重庆时时彩,全局开关：<span style="color: red;"><%=LotterySet.isCQSSCOpen == 0 ? "关" : "开"%></span>,业务系统开关：<span
			style="color: red;"><%=bizSystemConfigVO.getIsCQSSCOpen() == 0 ? "关" : "开"%></span>
	</p>
	<p>
		江西时时彩,全局开关：<span style="color: red;"><%=LotterySet.isJXSSCOpen == 0 ? "关" : "开"%></span>
		,业务系统开关：<span style="color: red;"><%=bizSystemConfigVO.getIsJXSSCOpen() == 0 ? "关" : "开"%></span>
	</p>
	<p>
		黑龙江时时彩,全局开关：<span style="color: red;"><%=LotterySet.isHLJSSCOpen == 0 ? "关" : "开"%></span>
		,业务系统开关：<span style="color: red;"><%=bizSystemConfigVO.getIsHLJSSCOpen() == 0 ? "关" : "开"%></span>
	</p>
	<p>
		新疆时时彩,全局开关：<span style="color: red;"><%=LotterySet.isXJSSCOpen == 0 ? "关" : "开"%></span>
		,业务系统开关：<span style="color: red;"><%=bizSystemConfigVO.getIsXJSSCOpen() == 0 ? "关" : "开"%></span>
	</p>
	<p>
		天津时时彩,全局开关：<span style="color: red;"><%=LotterySet.isTJSSCOpen == 0 ? "关" : "开"%></span>
		,业务系统开关：<span style="color: red;"><%=bizSystemConfigVO.getIsTJSSCOpen() == 0 ? "关" : "开"%></span>
	</p>

	<p>
		台湾分分彩,全局开关：<span style="color: red;"><%=LotterySet.isJLFFCOpen == 0 ? "关" : "开"%></span>
		,业务系统开关：<span style="color: red;"><%=bizSystemConfigVO.getIsJLFFCOpen() == 0 ? "关" : "开"%></span>
	</p>
	<p>
		新加坡2分彩,全局开关：<span style="color: red;"><%=LotterySet.isXJPLFCOpen == 0 ? "关" : "开"%></span>
		,业务系统开关：<span style="color: red;"><%=bizSystemConfigVO.getIsXJPLFCOpen() == 0 ? "关" : "开"%></span>
	</p>
	<p>
		台湾5分彩,全局开关：<span style="color: red;"><%=LotterySet.isTWWFCOpen == 0 ? "关" : "开"%></span>
		, 业务系统开关：<span style="color: red;"><%=bizSystemConfigVO.getIsTWWFCOpen() == 0 ? "关" : "开"%></span>
	</p>

	<p>
		山东11选5,全局开关：<span style="color: red;"><%=LotterySet.isSDSYXWOpen == 0 ? "关" : "开"%></span>
		, 业务系统开关：<span style="color: red;"><%=bizSystemConfigVO.getIsGDSYXWOpen() == 0 ? "关" : "开"%></span>
	</p>
	<p>
		重庆11选5,全局开关：<span style="color: red;"><%=LotterySet.isCQSYXWOpen == 0 ? "关" : "开"%></span>
		, 业务系统开关：<span style="color: red;"><%=bizSystemConfigVO.getIsCQSYXWOpen() == 0 ? "关" : "开"%></span>
	</p>
	<p>
		江西11选5,全局开关：<span style="color: red;"><%=LotterySet.isJXSYXWOpen == 0 ? "关" : "开"%></span>
		, 业务系统开关：<span style="color: red;"><%=bizSystemConfigVO.getIsJXSYXWOpen() == 0 ? "关" : "开"%></span>
	</p>
	<p>
		福建11选5,全局开关：<span style="color: red;"><%=LotterySet.isFJSYXWOpen == 0 ? "关" : "开"%></span>
		, 业务系统开关：<span style="color: red;"><%=bizSystemConfigVO.getIsFJSYXWOpen() == 0 ? "关" : "开"%></span>
	</p>
	<p>
		广东11选5,全局开关：<span style="color: red;"><%=LotterySet.isGDSYXWOpen == 0 ? "关" : "开"%></span>
		, 业务系统开关：<span style="color: red;"><%=bizSystemConfigVO.getIsGDSYXWOpen() == 0 ? "关" : "开"%></span>
	</p>

	<p>
		江苏快3,全局开关：<span style="color: red;"><%=LotterySet.isJSKSOpen == 0 ? "关" : "开"%></span>
		, 业务系统开关：<span style="color: red;"><%=bizSystemConfigVO.getIsJSKSOpen() == 0 ? "关" : "开"%></span>
	</p>
	<p>
		吉林快3,全局开关：<span style="color: red;"><%=LotterySet.isJLKSOpen == 0 ? "关" : "开"%></span>
		, 业务系统开关：<span style="color: red;"><%=bizSystemConfigVO.getIsJLKSOpen() == 0 ? "关" : "开"%></span>
	</p>
	<p>
		北京快3,全局开关：<span style="color: red;"><%=LotterySet.isBJKSOpen == 0 ? "关" : "开"%></span>
		, 业务系统开关：<span style="color: red;"><%=bizSystemConfigVO.getIsBJKSOpen() == 0 ? "关" : "开"%></span>
	</p>
	<p>
		广西快3,全局开关：<span style="color: red;"><%=LotterySet.isGXKSOpen == 0 ? "关" : "开"%></span>
		, 业务系统开关：<span style="color: red;"><%=bizSystemConfigVO.getIsGXKSOpen() == 0 ? "关" : "开"%></span>
	</p>
	<p>
		安徽快3,全局开关：<span style="color: red;"><%=LotterySet.isAHKSOpen == 0 ? "关" : "开"%></span>
		, 业务系统开关：<span style="color: red;"><%=bizSystemConfigVO.getIsAHKSOpen() == 0 ? "关" : "开"%></span>
	</p>
	<p>
		湖北快3,全局开关：<span style="color: red;"><%=LotterySet.isHBKSOpen == 0 ? "关" : "开"%></span>
		, 业务系统开关：<span style="color: red;"><%=bizSystemConfigVO.getIsHBKSOpen() == 0 ? "关" : "开"%></span>
	</p>
	<p>
		幸运快3,全局开关：<span style="color: red;"><%=LotterySet.isJYKSOpen == 0 ? "关" : "开"%></span>
		, 业务系统开关：<span style="color: red;"><%=bizSystemConfigVO.getIsJYKSOpen() == 0 ? "关" : "开"%></span>
	</p>
	<p>
		甘肃快3,全局开关：<span style="color: red;"><%=LotterySet.isGSKSOpen == 0 ? "关" : "开"%></span>
		, 业务系统开关：<span style="color: red;"><%=bizSystemConfigVO.getIsGSKSOpen() == 0 ? "关" : "开"%></span>
	</p>
	<p>
		上海快3,全局开关：<span style="color: red;"><%=LotterySet.isSHKSOpen == 0 ? "关" : "开"%></span>
		, 业务系统开关：<span style="color: red;"><%=bizSystemConfigVO.getIsSHKSOpen() == 0 ? "关" : "开"%></span>
	</p>

	<p>
		福彩3D,全局开关：<span style="color: red;"><%=LotterySet.isSHSSLDPCOpen == 0 ? "关" : "开"%></span>
		, 业务系统开关：<span style="color: red;"><%=bizSystemConfigVO.getIsSHSSLDPCOpen() == 0 ? "关" : "开"%></span>
	</p>
	<p>
		六合彩,全局开关 ：<span style="color: red;"><%=LotterySet.isLHCOpen == 0 ? "关" : "开"%></span>
		, 业务系统开关：<span style="color: red;"><%=bizSystemConfigVO.getIsLHCOpen() == 0 ? "关" : "开"%></span>
	</p>
	<p>
		北京PK10,全局开关 ：<span style="color: red;"><%=LotterySet.isBJPK10Open == 0 ? "关" : "开"%></span>
		, 业务系统开关：<span style="color: red;"><%=bizSystemConfigVO.getIsBJPK10Open() == 0 ? "关" : "开"%></span>
	</p>
	<p>
		极速PK10,全局开关 ：<span style="color: red;"><%=LotterySet.isJSPK10Open == 0 ? "关" : "开"%></span>
		, 业务系统开关：<span style="color: red;"><%=bizSystemConfigVO.getIsJSPK10Open() == 0 ? "关" : "开"%></span>
	</p>
	<br />
	<h1>网站内容</h1>
	<p>
		业务系统：<span style="color: red;"><%=bizSystemInfo.getBizSystem() == null ? "null"
					: bizSystemInfo.getBizSystem()%></span>
	</p>
	<p>
		业务系统名称：<span style="color: red;"><%=bizSystemInfo.getBizSystemName() == null ? "null"
					: bizSystemInfo.getBizSystemName()%></span>
	</p>
	<p>
		模板：<span style="color: red;"><%=bizSystemInfo.getTemplateType() == null ? "null"
					: bizSystemInfo.getTemplateType()%></span>
	</p>
	<p>
		首页彩种：<span style="color: red;"><%=bizSystemInfo.getIndexLotteryKind() == null ? "null"
					: bizSystemInfo.getIndexLotteryKind()%></span>
	</p>
	<p>
		热门彩种：<span style="color: red;"><%=bizSystemInfo.getHotLotteryKinds() == null ? "null"
					: bizSystemInfo.getHotLotteryKinds()%></span>
	</p>
	<p>
		首页默认彩种：<span style="color: red;"><%=bizSystemInfo.getIndexDefaultLotteryKinds() == null ? "null"
					: bizSystemInfo.getIndexDefaultLotteryKinds()%></span>
	</p>
	<p>
		手机端首页默认彩种：<span style="color: red;"><%=bizSystemInfo.getIndexMobileDefaultLotteryKinds() == null ? "null"
					: bizSystemInfo.getIndexMobileDefaultLotteryKinds()%></span>
	</p>
	<p>
		登陆logo图片地址：<span style="color: red;"><%=bizSystemInfo.getLogoUrl() == null ? "null"
					: bizSystemInfo.getLogoUrl()%></span>
	</p>
	<p>
		页面顶部logo图片地址：<span style="color: red;"><%=bizSystemInfo.getHeadLogoUrl() == null ? "null"
					: bizSystemInfo.getHeadLogoUrl()%></span>
	</p>
	<p>
		手机端logo图片地址：<span style="color: red;"><%=bizSystemInfo.getMobileLogoUrl() == null ? "null"
					: bizSystemInfo.getMobileLogoUrl()%></span>
	</p>
	<p>
		客服地址：<span style="color: red;"><%=bizSystemInfo.getCustomUrl() == null ? "null"
					: bizSystemInfo.getCustomUrl()%></span>
	</p>
	<p>
		电话号码：<span style="color: red;"><%=bizSystemInfo.getTelphoneNum() == null ? "null"
					: bizSystemInfo.getTelphoneNum()%></span>
	</p>
	<p>
		手机APP下载地址：<span style="color: red;"><%=bizSystemInfo.getAndriodIosDownloadUrl() == null ? "null"
					: bizSystemInfo.getAndriodIosDownloadUrl()%></span>
	</p>
	<p>
		手机APP下载logo：<span style="color: red;"><%=bizSystemInfo.getAppDownLogo() == null ? "null"
					: bizSystemInfo.getAppDownLogo()%></span>
	</p>
	<p>
		手机APP显示名称：<span style="color: red;"><%=bizSystemInfo.getAppShowname() == null ? "null"
					: bizSystemInfo.getAppShowname()%></span>
	</p>
	<p>
		二维码地址：<span style="color: red;"><%=bizSystemInfo.getBarcodeUrl() == null ? "null"
					: bizSystemInfo.getBarcodeUrl()%></span>
	</p>
	<p>
		手机版二维码访问地址：<span style="color: red;"><%=bizSystemInfo.getMobileBarcodeUrl() == null ? "null"
					: bizSystemInfo.getMobileBarcodeUrl()%></span>
	</p>
	<p>
		是否有app版：<span style="color: red;"><%=bizSystemInfo.getHasApp() == null ? "null"
					: bizSystemInfo.getHasApp()%></span>
	</p>
	<p>
		安卓app端二维码地址：<span style="color: red;"><%=bizSystemInfo.getAppBarcodeUrl() == null ? "null"
					: bizSystemInfo.getAppBarcodeUrl()%></span>
	</p>
	<p>
		安卓app端下载地址：<span style="color: red;"><%=bizSystemInfo.getAppAndroidDownloadUrl() == null ? "null"
					: bizSystemInfo.getAppAndroidDownloadUrl()%></span>
	</p>
	<p>
		安卓苹果端二维码地址：<span style="color: red;"><%=bizSystemInfo.getAppIosBarcodeUrl() == null ? "null"
					: bizSystemInfo.getAppIosBarcodeUrl()%></span>
	</p>
	<p>
		安卓苹果下载地址：<span style="color: red;"><%=bizSystemInfo.getAppStoreIosUrl() == null ? "null"
					: bizSystemInfo.getAppStoreIosUrl()%></span>
	</p>
	<p>
		创建时间：<span style="color: red;"><%=bizSystemInfo.getCreateTime() == null ? "null"
					: bizSystemInfo.getCreateTime()%></span>
	</p>
	<p>
		更新时间：<span style="color: red;"><%=bizSystemInfo.getUpdateTime() == null ? "null"
					: bizSystemInfo.getUpdateTime()%></span>
	</p>
	<p>
		操作人：<span style="color: red;"><%=bizSystemInfo.getUpdateAdmin() == null ? "null"
					: bizSystemInfo.getUpdateAdmin()%></span>
	</p>
	<p>
		优势编辑：<span style="color: red;"><%=bizSystemInfo.getAdvantageDesc() == null ? "null"
					: bizSystemInfo.getAdvantageDesc()%></span>
	</p>
	<p>
		页脚内容：<span style="color: red;"><%=bizSystemInfo.getFooter() == null ? "null"
					: bizSystemInfo.getFooter()%></span>
	</p>
	<p>
		站点统计脚本：<span style="color: red;"><%=bizSystemInfo.getSiteScript() == null ? "null"
					: bizSystemInfo.getSiteScript()%></span>
	</p>
	<p>
		手机站点脚本统计：<span style="color: red;"><%=bizSystemInfo.getMobileSiteScript() == null ? "null"
					: bizSystemInfo.getMobileSiteScript()%></span>
	</p>
	<p>
		网站图标 url：<span style="color: red;"><%=bizSystemInfo.getFaviconUrl() == null ? "null"
					: bizSystemInfo.getFaviconUrl()%></span>
	</p>
	<p>
		充值描述：<span style="color: red;"><%=bizSystemInfo.getRechargeSecond() == null ? "null"
					: bizSystemInfo.getRechargeSecond()%></span>
	</p>
	<p>
		提现描述：<span style="color: red;"><%=bizSystemInfo.getWithdrawSecond() == null ? "null"
					: bizSystemInfo.getWithdrawSecond()%></span>
	</p>
	<br />
	<h1>
		机器当前时间:<span style="color: red;"><%=sdf.format(new Date())%></span>
	</h1>
	<p>
		session会话计数:<span style="color: red;"><%=SingleLoginForRedis.sessionCount%></span>
	</p>
</body>
</html>