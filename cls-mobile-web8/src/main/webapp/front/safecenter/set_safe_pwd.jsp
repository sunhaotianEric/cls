<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="format-detection" content="telephone=no">
    <title>设置安全密码</title>
    <link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
</head>
<body>

<!-- header -->
<div class="header color-dark">
    <div class="header-left" onclick="window.history.go(-1)"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
    <h2 class="header-title">设置安全密码</h2>
</div>
<!-- header over -->
<div class="stance"></div>


<br>
<br>
<br>
<!-- <ul class="user-list">
    user-list 输入框
    <li class="user-line">
        <div class="container">
            <div class="line-title">
                <p>当前密码</p>
            </div>
            <div class="line-content"><input class="inputText" type="password" id="oldPassword" placeholder="请输入当前密码"></div>
        </div>
    </li>
    user-list over 输入框
</ul> -->
<br>
<br>
<ul class="user-list">
    <!-- user-list 输入框 -->
    <li class="user-line">
        <div class="container">
            <div class="line-title">
                <p>输入新密码</p>
            </div>
            <div class="line-content"><input class="inputText" type="password" id="newPassword" placeholder=""></div>
        </div>
    </li>
    <!-- user-list over 输入框 -->
    <!-- user-list 输入框 -->
    <li class="user-line">
        <div class="container">
            <div class="line-title">
                <p>确认新密码</p>
            </div>
            <div class="line-content"><input class="inputText" type="password" id="confirmNewPassword" placeholder=""></div>
        </div>
    </li>
    <!-- user-list over 输入框 -->
</ul>


<ul class="user-list no">
    <!-- user-list 提示信息 -->
    <li class="user-msg">
        <div class="container">
            <p>由字母和数字组成6-15字符；且必须包含数字和字母，</p>
            <p>不能和登陆密码相同</p>
        </div>
    </li>
    <!-- user-list over 提示信息 -->
</ul>
<div class="user-btn color-red" id="savePassBtn">确定</div>
<!-- main over -->
<!-- nav-bar -->
<jsp:include page="/front/include/navbar.jsp"></jsp:include>
<!-- nav-bar over --> 
<jsp:include page="/front/include/include.jsp"></jsp:include>
 <script src="<%=path%>/js/safecenter/set_safe_pwd.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
</body>
</html>