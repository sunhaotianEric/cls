<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="format-detection" content="telephone=no">
    <title>修改安全邮箱</title>
    <link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
</head>
<body>

<!-- header -->
<div class="header color-dark">
    <div class="header-left" onclick="window.history.go(-1)"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
    	<h2 class="header-title">修改安全邮箱</h2>
</div>
<!-- header over -->
<div class="stance"></div>


<br>
<br>
<br>
<ul class="user-list">
    <!-- 手机邮件验证 -->
    <li class="user-line">
        <div class="container" >
            <div class="line-title" >
                <p id="emailOldMsg">原邮箱</p>
            </div>
            <div class="line-content"><input class="inputText" type="text" placeholder="请输入原绑定的邮箱地址" id="oldEmail"><input type="hidden" id="eType"/></div>
        </div>
    </li>
    <li class="user-line">
        <div class="container">
            <div class="line-title">
                <p id="emailMsg">邮箱</p>
            </div>
            <div class="line-content"><input class="inputText" type="text" placeholder="请输入您要绑定的邮箱地址" id="email"><input type="hidden" id="eType"/></div>
        </div>
    </li>
    <li class="user-line" id="email_verifyCode_field">
        <div class="container">
            <div class="line-title">
                <p>验证码</p>
            </div>
            <div class="line-content">
            	<input class="inputText" type="text" placeholder="请输入邮箱验证码" id="email_verifyCode">
            	<div class="right font-red" id="btn-sendnum">发送</div>
            	<span class="email-verify-time right">发送验证码(<em class="countdown">60</em> s)</span>
            </div>
        </div>
    </li>
   	 <li class="user-line">
	     <div class="container">
	         <div class="line-title">
	             <p>安全密码</p>
	         </div>
	         <div class="line-content"><input class="inputText" type="password" id="safepasw" placeholder=""></div>
	     </div>
	 </li>
    <!-- 手机邮件验证end -->
</ul>

<div class="user-btn color-red" id="btn-bind" onclick="bindEmailChangePage.bindEmailChange()">
	确定
</div>
<!-- main over -->
<!-- nav-bar -->
<jsp:include page="/front/include/navbar.jsp"></jsp:include>
<!-- nav-bar over --> 
<jsp:include page="/front/include/include.jsp"></jsp:include>
<script src="<%=path%>/js/safecenter/bind_email_change.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
</body>
</html>