<%@page import="com.team.lottery.util.BaseDwrUtil"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.vo.DomainInvitationCode"%>
<%@page import="com.team.lottery.service.DomainInvitationCodeService"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    import="java.math.BigDecimal,java.io.UnsupportedEncodingException,com.team.lottery.vo.BizSystem,com.team.lottery.system.BizSystemConfigVO,net.sf.json.JSONObject"
    import="com.team.lottery.cache.ClsCacheManager,com.team.lottery.service.UserRegisterService,org.apache.commons.lang.StringUtils,com.team.lottery.extvo.UserRegisterVo,com.team.lottery.util.ConstantUtil,com.team.lottery.util.ApplicationContextUtil"
    import="com.team.lottery.system.BizSystemConfigManager,com.team.lottery.system.BizSystemConfigVO,com.team.lottery.vo.BizSystemInfo,com.team.lottery.vo.BizSystemDomain"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
	String serverName = request.getServerName();
	String bizSystem = ClsCacheManager.getBizSystemByDomainUrl(serverName);

	BizSystem bizSystemConfig = ClsCacheManager.getBizSystemByCode(bizSystem);
	if (bizSystemConfig == null) {
		bizSystemConfig = new BizSystem();
	}
	request.setAttribute("bizSystem", bizSystemConfig);

	//获取当前域名
	BizSystemInfo bizSystemInfo = ClsCacheManager.getBizSystemInfo(bizSystem);
	//客服
	String customUrl = "";
	if (bizSystemInfo != null) {
		customUrl = bizSystemInfo.getCustomUrl();
	}

	//获取request中的邀请码
	String registerCode = "";
	boolean isShowRegisterCode = true;
	String context = "请输入邀请码";
	String requestRegisterCode = request.getParameter("code");
	
	// 获取当前配置对象.
	BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(bizSystem);
	JSONObject obj = JSONObject.fromObject(bizSystemConfigVO);
	String strJson = obj.toString();
	
	// 获取当前是否开启了开放注册!
	Integer regOpen = bizSystemConfigVO.getRegOpen();
	DomainInvitationCode domainInvitationCode = new DomainInvitationCode();
	domainInvitationCode.setBizSystem(bizSystem);
	domainInvitationCode.setDomainUrl(serverName);
	domainInvitationCode.setEnable(1);
	//查询相关的域名邀请码
	DomainInvitationCodeService domainInvitationCodeService = ApplicationContextUtil.getBean("domainInvitationCodeService");
	DomainInvitationCode code = domainInvitationCodeService.selectDomainInvitationCode(domainInvitationCode);
	
	// 是否开放注册.
	if(regOpen == 1){
		// 首先设置默认邀请码.
		registerCode = "";
		isShowRegisterCode = true;
		context = "请输入邀请码(非必填)";
		// 然后设置域名邀请码.
		if(code != null){
			registerCode = code.getInvitationCode();
			isShowRegisterCode = false;
			context = "请输入邀请码";
		}
		// 然后判断是否有Code邀请码.
		if(StringUtils.isNotBlank(requestRegisterCode)){
			registerCode = requestRegisterCode;
			isShowRegisterCode = true;
			context = "请输入邀请码";
		}
	
	} else {
		// 没有开放注册的话,就先查看是否有域名邀请码.
		if(code != null){
			registerCode = code.getInvitationCode();
			isShowRegisterCode = false;
			context = "请输入邀请码";
		}
		// 查看域名邀请码.
		if(StringUtils.isNotBlank(requestRegisterCode)){
			registerCode = requestRegisterCode;
			isShowRegisterCode = true;
			context = "请输入邀请码";
		}
	}
	
	Integer regShowPhone = bizSystemConfigVO.getRegShowPhone() == null ? 0 : bizSystemConfigVO.getRegShowPhone();
	Integer regRequiredPhone = bizSystemConfigVO.getRegRequiredPhone() == null ? 0 : bizSystemConfigVO.getRegRequiredPhone();
	Integer regShowEmail = bizSystemConfigVO.getRegShowEmail() == null ? 0 : bizSystemConfigVO.getRegShowEmail();
	Integer regRequiredEmail = bizSystemConfigVO.getRegRequiredEmail() == null ? 0 : bizSystemConfigVO.getRegRequiredEmail();
	Integer regShowQq = bizSystemConfigVO.getRegShowQq() == null ? 0 : bizSystemConfigVO.getRegShowQq();
	Integer regRequiredQq = bizSystemConfigVO.getRegRequiredQq() == null ? 0 : bizSystemConfigVO.getRegRequiredQq();
	Integer regShowSms = bizSystemConfigVO.getRegShowSms() == null ? 0 : bizSystemConfigVO.getRegShowSms();
%>        
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="format-detection" content="telephone=no">
    <title>用户注册</title>
     <link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
     <link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
     <jsp:include page="/front/include/include.jsp"></jsp:include>
<!-- 业务js -->
     <script type="text/javascript" src="<%=path%>/js/reg/reg.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
<script type="text/javascript">
	function checkCodeRefresh() {
		$('#CheckCode').attr('src', contextPath + "/tools/getRandomCodePic?time="+Math.random());
	}
	
	/**
	 * tip弹窗 重写base的showTip方法
	 * @param  {[type]} str   [description]
	 * @param  {[type]} delay [description]
	 * @return void
	 */
	function showTip(str,delay){
		var tipclass="tip"+new Date().getMilliseconds();
		var _delay=delay||3000;
		var html='<div class="tip '+tipclass+'" style="display: -webkit-box; display: -webkit-flex;display: -moz-box;display: -moz-flex;display: -ms-flexbox;display: flex;"><div class="tip-content"><p>'+str+'</p></div></div>';
		if($("body .view-container").length>0)
			$("body .view-container").append(html);
		else
			$("body").append(html);
		
		setTimeout(function(tipclass){
			$(".tip."+tipclass+" .tip-content").addClass('out');
			setTimeout(function(tipclass){
				$(".tip."+tipclass).remove();
			},1000,tipclass);
		},_delay,tipclass);
	}
</script>
<script type="text/javascript">
  	var contextPath = '<%=path %>';
	regPage.param.invitationCode  = '<%=registerCode %>';
	regPage.param.regShowPhone='<%=regShowPhone %>';
	regPage.param.regRequiredPhone='<%=regRequiredPhone %>';
	regPage.param.regShowEmail='<%=regShowEmail %>';
	regPage.param.regRequiredEmail='<%=regRequiredEmail %>';
	regPage.param.regShowQq='<%=regShowQq %>';
	regPage.param.regRequiredQq='<%=regRequiredQq %>';
	regPage.param.regShowSms='<%=regShowSms %>';
	var customUrl = '<%=customUrl %>';
	regPage.param.bizSystemConfigVO=<%=strJson%>;
</script>
</head>
<body>

<!-- header -->
<div class="header color-dark">
   
    <!-- <h2 class="header-title">账号注册</h2>
    <div class="header-right" id="customUrlRegId">客服</div> -->
    <div class="header-left" onclick="window.history.go(-1);"><img class="icon" src="<%=path %>/images/icon/icon-return.png"></div>
        <h2 class="header-title">账号注册</h2>
        <div class="header-right" id="customUrlRegId" onclick="window.location.href='<%=path%>/gameBet/reg.html'"><img src="<%=path %>/images/icon/icon-custom.png" alt=""></div>
</div>
<!-- header over -->
    
        <div class="stance"></div>
          <ul class="user-list"  id="invitationCode_line">
            <!-- user-list 输入框 -->
            <li class="user-line" id="invitationCode_line"
            	<% if(isShowRegisterCode){ %> 
	         		style="display: block;"
	         	<% } else {%> 
	         		style="display: none;"
	         	 <%} %>
            >
            <div class="container">
                <div class="line-title"><p>邀请码</p></div>
                <div class="line-content">
                	<% if(StringUtils.isNotBlank(registerCode)){ %>
			            <input type="text" class="inputText" id="invitationCode" value="<%=registerCode%>" placeholder="<%=context%>">
			        <% }else{ %>
			        	<input type="text" class="inputText" id="invitationCode" placeholder="<%=context%>">
			        <% } %>
                </div>
            </div>
            </li>
            <!-- user-list over 输入框 -->
        </ul>
        <ul class="user-list">
            <!-- user-list 输入框 -->
            <li class="user-line">
            <div class="container">
                <div class="line-title"><p>账号</p></div>
                <div class="line-content">
	                <input id="username" class="inputText" type="text" placeholder="请输入账号">
	                <input type="hidden" class="inputText" id="bizSystem" value="<%=bizSystem%>">
                </div>
            </div>
            </li>
            <li class="user-line">
            <div class="container">
                <div class="line-title"><p>密码</p></div>
                <div class="line-content"><input id="password" class="inputText" type="password" placeholder="请输入密码"></div>
            </div>
            </li>
            <!-- user-list over 输入框 -->
            <!-- user-list 输入框 -->
            <li class="user-line">
            <div class="container">
                <div class="line-title"><p>确认密码</p></div>
                <div class="line-content"><input id="password2" class="inputText" type="password" placeholder="请输入密码"></div>
            </div>
            </li>
            <!-- user-list over 输入框 -->
            <!-- user-list 输入框 -->
            <% if(regShowPhone==1){ %>
            <li class="user-line">
            <div class="container">
                <div class="line-title"><p>手机号</p></div>
                <div class="line-content por">
	                <input id="mobile" class="inputText" type="text" placeholder="请输入手机号">
	                 <% if(regShowSms == 1){ %>
                	<button type="submit" class="btn-sendmobile">发送</button>
                	<span class="mobile-verify-time">发送验证码(<em class="countdown">60</em> s)</span>
                	 <%} %>
                </div>
            </div>
            </li>
            <% } %>
            <% if(regShowPhone==1 && regShowSms == 1){ %>
            <li class="user-line mobile-verifycode-line">
	            <div class="container">
	                <div class="line-title"><p>短信验证码</p></div>
	                <div class="line-content">
		                <input id="mobile-verifycode" class="inputText" type="text" placeholder="请输入短信验证码">
	                </div>
	            </div>
            </li>
            <%} %>
        	<% if(regShowQq==1){ %>
            <li class="user-line">
            <div class="container">
                <div class="line-title"><p>联系QQ</p></div>
                <div class="line-content">
	                <input id="qq" class="inputText" type="text" placeholder="请输入QQ">
                </div>
            </div>
            </li>
            <% } %>
            <% if(regShowEmail==1){ %>
            <li class="user-line">
            <div class="container">
                <div class="line-title"><p>Email</p></div>
                <div class="line-content">
	                <input id="email" class="inputText" type="text" placeholder="请输入邮箱地址">
                </div>
            </div>
            </li>
            <% } %>

            <!-- user-list over 输入框 -->
            <!-- user-list 输入框 -->
            <li class="user-line">
            <div class="container">
                <div class="line-title"><p>验证码</p></div>
                <div class="line-content"><input id='verifycode' class="inputText" type="text" placeholder="请输入验证码"></div>
                <img class="verifyImg" style="width: 238px;height: 93px;"  alt="验证码" title="点击更换" id="CheckCode" onclick="checkCodeRefresh()" src="<%=path %>/tools/getRandomCodePic">
            </div>
            </li>
            <!-- user-list over 输入框 -->
        </ul>

        
        <ul class="user-list no">
            <!-- user-list 提示信息 -->
            <li class="user-msg"><div class="container" style="text-align:right;">
            <a href="<%=path%>/gameBet/login.html"><p class="font-red">已有账号?立即登录</p></a>
            </div></li>
            <!-- user-list over 提示信息 -->
        </ul>
        <br>
        <div class="user-btn color-red"  id='regButton' >注册</div>
        <div style="display: none;">
			<%=bizSystemInfo==null?"":bizSystemInfo.getMobileSiteScript()!=null?bizSystemInfo.getMobileSiteScript():"" %>
		</div>
        <div style="display: none;"> 
	        <script src="https://s19.cnzz.com/z_stat.php?id=1272446067&web_id=1272446067" language="JavaScript"></script>
        </div>
</body>  
</html>