<%@page import="com.team.lottery.vo.BizSystemInfo"%>
<%@page import="com.team.lottery.cache.ClsCacheManager"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page import="com.team.lottery.util.ConstantUtil,com.team.lottery.vo.User" %>
<%@ page import="com.team.lottery.system.SystemConfigConstant,com.team.lottery.system.BizSystemConfigVO,com.team.lottery.system.BizSystemConfigManager" %>
<%
    String userName = "";
    String passWord = "";
    String loginType = "";
	String path = request.getContextPath();
	String url =  request.getRequestURL().toString();
	loginType = request.getParameter("loginType")== null ? "":request.getParameter("loginType");
    User user = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(!"".equals(loginType)){
	    //代表已经，userLogout = 退出标示
	    if(user != null && !"userLogout".equals(loginType)){
	    	response.sendRedirect(path+"/gameBet/index.html");
	    }else{
	    	  userName = request.getParameter("userName") == null ? "":request.getParameter("userName");
	    	  passWord = request.getParameter("passWord")== null ? "":request.getParameter("passWord");
	    	  
	    } 
    }
	String serverName=request.getServerName();
    String bizSystem=ClsCacheManager.getBizSystemByDomainUrl(serverName);
    BizSystemConfigVO bizSystemConfigVO=BizSystemConfigManager.getBizSystemConfig(bizSystem);
	String  placeholder = "用户名";
  	if(bizSystemConfigVO.getIsOpenPhone() == 1){
  		placeholder += "/手机号";
  	}
      if(bizSystemConfigVO.getIsOpenEmail() == 1){
      	placeholder += "/邮箱";
  	}
    request.setAttribute("bizSystemConfigVO", bizSystemConfigVO);
    BizSystemInfo bizSystemInfo=ClsCacheManager.getBizSystemInfo(bizSystem);
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="format-detection" content="telephone=no">
	<title>登陆</title>
	<link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
	<link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
	<link href="<%=path%>/css/login.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
</head>
<body>
	<!-- header -->
	<div class="header color-dark">
		<div class="header-left" onclick="window.history.go(-1);"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
		<div class="header-right" ><img class="icon" id="customUrlId" src="<%=path%>/images/icon/icon-custom.png"></div>
	</div>
	<!-- header over -->

	<!-- main -->
	<div class="main">
			<div class="head">
					<div class="head-logo"><img src="" alt=""></div>
			</div>
			<div class="content">
					<div class="line por">
						<img class="icon" src="<%=path%>/images/icon/icon-loginuser.png" alt="">
						<input type="text" id="quc_account" class="inputText" placeholder="<%=placeholder %>" >
						<!-- <i class="icon-ardown"></i> -->
					</div>
					<div class="line">
						<img class="icon" src="<%=path%>/images/icon/icon-password.png" alt="">
						<input type="password" id="quc_password" class="inputText" placeholder="密码" >
					</div>
					<div class="line" style="display: none;"  id="checkCodeId">
						<img class="icon" src="<%=path%>/images/icon/icon-verify.png" alt="">
						<input class="inputText" type="text" placeholder="请输入验证码" style="width:60%" id="quc_phrase">
						<img class="verify" style="width: 160px;height: 100px;" src="" onclick='loginPage.checkCodeRefresh()' id="CheckCode" >
					</div>
					<div class="rememberme">
						<!-- <input type="checkbox" name="rememberme" id="rememberme" value="yes" style="height: 40px;width: 40px;"> <span>记住密码</span> -->
						<a href="<%=path%>/gameBet/forgetPwd.html" class="btn-forget"><span class="forget">忘记密码?</span></a>
					</div>
					<br>
					<br>
					<div class="button" id="loginButton">登录</div>
			</div>
			<p class="go-enroll">没有账号? <a href="<%=path%>/gameBet/reg.html">立即注册</a></p>
			<%if(bizSystemConfigVO.getGuestModel()==1){ %>
			<p class="go-enroll mt45"><a href="javascript:;" id="showTouristDialogButton">点击试玩</a></p>
			<%} %>
	</div>
	<!-- main over -->
	
	<div class="alert-bg" onclick="element_toggle(false,['.alert','.alert-record-account'])"></div>
	<!-- alert -->
	<div class="alert alert-cart"  id="touristDialog" style="display: none">
	    <div class="alert-head color-dark">
	        <p>温馨提示</p>
	        <i class="close ion-close-round" onclick="element_toggle(false,['.alert-cart'])"></i>
	    </div>
	    <div class="alert-content">
	        <ul class="tryplay">
	            <li class="user-line">
		            <div class="container clear">
		                <div class="line-title">
		                    <img src="<%=path%>/images/icon/icon-verify.png">
		                </div>
		                <div class="line-content"><input class="inputText" type="text" placeholder="请输入验证码" id="touristVerifycode"></div>
		                <img class="verifyImg" style="width: 220px;height: 100px;" id="touristCheckcode" onclick="checkCodeRefresh()" src="<%=path%>/tools/getRandomCodePic"  alt="">
		            </div>
		        </li>
	            <li class="user-line no-border">
	                <div class="line-content w100">
	                    <p>试玩功能仅提供投注体验使用，为了更好的为您提供服务，请点击注册按钮注册真实用户，完善个人信息。</p>
	                </div>
	            </li>
	        </ul>
	    </div>
	    <div class="alert-foot">
	        <div class="btn color-gray cols-50" id="touristDialogCancelButton">取消</div>
	        <div class="btn color-red cols-50" onclick="touRist()" id="touristDialogSureButton">确认</div>
	    </div>
	</div>
	<div style="display: none;">
		<%=bizSystemInfo==null?"":bizSystemInfo.getMobileSiteScript()!=null?bizSystemInfo.getMobileSiteScript():"" %>
	</div>
	<!-- alert over -->
	
	<!-- 选择登录帐号alert -->
	<div class="alert-record-account">
		<ul>
			<li>选择登录帐号</li>
			<li class="choose-user" id="cookieUserName">***<i class="icon-close"></i></li>
		</ul>
		<ul>
			<li class="btn-close">取消</li>
		</ul>
	</div>
	<!-- 选择登录帐号alert end -->

   <script type="text/javascript">
    var userName = '<%=userName%>';
    var passWord = '<%=passWord%>';
    var loginType = '<%=loginType%>';
  
   </script>

	<jsp:include page="/front/include/include.jsp"></jsp:include>
	<script src="<%=path%>/js/jquery.cookie.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
	
	<script src="<%=path%>/js/login/login.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
	
	<script type="text/javascript">
		function checkCodeRefresh() {
			$('#touristCheckcode').attr('src', contextPath + "/tools/getRandomCodePic?time=" + Math.random());
		}
		//确认试玩处理方法
		function touRist(){
	    	var code=$("#touristVerifycode").val();
	    	if(code==""||code==null){
	    		showTip("请输入验证码");
	    		return;
	    	}
	    	var jsonData = {
    			"code" : code,
    		}
	    	//游客登录试玩
	    	$.ajax({
	    		type : "POST",
	    		url : contextPath + "/user/guestUserMobileLogin",
	    		contentType : 'application/json;charset=UTF-8',
	    		data: JSON.stringify(jsonData),
	    		success : function(result) {
	    			commonHandlerResult(result, this.url);
	    			if( result.code == "ok"){
	    				$(".close").click();
		    			showTip("祝您试玩愉快，2秒后自动刷新页面！");
		    			setTimeout("window.location.href= '"+contextPath+"/gameBet/index.html'", 1000);
	    			}else if(result.code == "error"){
	    				showTip(result.data);
	    			}
	    			checkCodeRefresh();
	    		}
	    	});
	 	}
	</script>
</body>
</html>