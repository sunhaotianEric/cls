<%@page import="com.team.lottery.pay.model.LianYingPay"%>
<%@page import="com.team.lottery.pay.model.TianTianKuaiPay"%>
<%@page import="com.team.lottery.pay.model.TianTianKuaiPayReturn"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="java.net.URLEncoder"%>
<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.team.lottery.service.QuickBankTypeService"%>
<%@page import="com.team.lottery.util.BaseDwrUtil"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.vo.RechargeWithDrawOrder"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundThirdPayType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.vo.QuickBankType"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="javax.servlet.http.HttpServletResponse"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
	//打印日志
	Logger log = LoggerFactory.getLogger(this.getClass());
	//获取当前web路径
	String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	log.info("支付方式:" + payType);
	String orderMoneyYuan = OrderMoney;
	//判断当前请求页面用户是否登录,没有的话就跳往登录页面
	User currentUser = (User) request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	if (currentUser == null) {
		response.sendRedirect(path + "/gameBet/login.html");
	}
	//获取订单提交时间
	Date currTime = new Date();
	QuickBankTypeService quickBankTypeService = (QuickBankTypeService) ApplicationContextUtil.getBean("quickBankTypeService");
	ChargePayService chargePayService = (ChargePayService) ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService) ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.getPayMemmberMsg(Long.valueOf(chargePayId));
	//生成订单Id
	String orderId = MakeOrderNum.makeOrderNum("CZ");
	//打印日志
	log.info("业务系统[{}],用户名[{}],发起了天天快付，生成订单号[{}],支付金额[{}],PayId[{}],payType[{}],chargePayId[{}]", currentUser.getBizSystem(), currentUser.getUserName(), orderId,
			OrderMoney, payId, payType, chargePayId);

	QuickBankType query = new QuickBankType();
	query.setPayId(payId);
	String bankType = "";
	List<QuickBankType> list = quickBankTypeService.getQuickBankTypeQuery(query);
	if (list.size() > 0) {
		bankType = list.get(0).getBankType();
	}
	//生成需要跳转的支付地址;
	String payUrl = chargePay.getPayUrl();//转发支付网关地址
	String Address = chargePay.getAddress(); //最终地址

	log.info("天天快付报文转发地址payUrl: " + payUrl + ",实际网关地址address: " + Address);
	//同步跳转地址
	String notifyUrl = "";
	if (chargePay.getNotifyUrl().startsWith("http://") || chargePay.getNotifyUrl().startsWith("https://")) {
		notifyUrl = chargePay.getNotifyUrl();
	} else {
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
		notifyUrl = basePath + chargePay.getNotifyUrl();
	}

	//设置天天快付支付对象
	TianTianKuaiPay tianTianKuaiPay = new TianTianKuaiPay();
	//设置字符集.
	tianTianKuaiPay.setCharset("UTF-8");
	//商品描述.
	tianTianKuaiPay.setGoods_desc(currentUser.getUserName());
	//设置商户号.
	tianTianKuaiPay.setMerchant_id(chargePay.getMemberId());
	//设置随机字符串.
	tianTianKuaiPay.setNonce_str(String.valueOf(currTime.getTime()));
	//处理订单地址.
	tianTianKuaiPay.setNotify_url(chargePay.getReturnUrl());
	//设置订单号.
	tianTianKuaiPay.setOut_trade_no(orderId);
	//设置跳转地址.
	tianTianKuaiPay.setReturn_url(chargePay.getNotifyUrl());
	//设置支付方式.
	tianTianKuaiPay.setService(payId);
	//设置加密方式.
	tianTianKuaiPay.setSign_type(chargePay.getSignType());
	//设置交易金额.
	tianTianKuaiPay.setTotal_amount(OrderMoney);
	//设置版本号.
	tianTianKuaiPay.setVersion(chargePay.getInterfaceVersion());
	
	//拼接字段并且加密.
	String mark = "&";
	String md5Key = 
			"charset=" + tianTianKuaiPay.getCharset() + mark + 
			"goods_desc=" + tianTianKuaiPay.getGoods_desc() + mark + 
			"merchant_id=" + tianTianKuaiPay.getMerchant_id() + mark + 
			"nonce_str=" + tianTianKuaiPay.getNonce_str() + mark + 
			"notify_url=" + tianTianKuaiPay.getNotify_url() + mark + 
			"out_trade_no=" + tianTianKuaiPay.getOut_trade_no() + mark + 
			"return_url=" + tianTianKuaiPay.getReturn_url()+ mark + 
			"service=" + tianTianKuaiPay.getService() + mark + 
			"sign_type="+tianTianKuaiPay.getSign_type()+ mark + 
			"total_amount=" + tianTianKuaiPay.getTotal_amount() + mark + 
			"version=" +tianTianKuaiPay.getVersion()+mark+
			"key="+chargePay.getSign();
	//使用MD5加密并且大写
	String signature = Md5Util.getMD5ofStr(md5Key, "UTF-8").toUpperCase();
	//设置签名
	tianTianKuaiPay.setSign(signature);
	//打印日志
	log.info("md5原始串: " + md5Key + "生成后的交易签名:" + signature);

	//冲值订单
	RechargeOrder rechargeOrder = new RechargeOrder();
	//用户ID
	rechargeOrder.setUserId(currentUser.getId());
	//快捷ID
	rechargeOrder.setChargePayId(chargePay.getId());
	//用户名称
	rechargeOrder.setUserName(currentUser.getUserName());
	//申请金额
	rechargeOrder.setApplyValue(new BigDecimal(orderMoneyYuan));
	//订单创建时间
	rechargeOrder.setCreatedDate(new Date());
	//充值类型
	rechargeOrder.setPayType(payType);
	//第三方充值类型
	rechargeOrder.setThirdPayType(EFundThirdPayType.TIANTIANKUAIPAY.name());
	//操作描述
	rechargeOrder.setOperateDes(EFundPayType.valueOf(payType).getDescription());
	//流水号
	rechargeOrder.setSerialNumber(orderId);
	rechargeOrder.setPostscript("");
	rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT.name());
	rechargeOrder.setBankName("");
	rechargeOrder.setSubbranchName("");
	rechargeOrder.setBankAccountName("");
	rechargeOrder.setBankCardNum("");
	rechargeOrder.setBankType("");
	rechargeOrder.setBizSystem(currentUser.getBizSystem());
	rechargeOrder.setRefType(EFundRefType.THIRDPAY.name());
	rechargeOrder.setFromType(currentUser.getLoginType());
	//保存新的订单到数据库中
	rechargeWithDrawOrderService.insertSelective(rechargeOrder);
	Map<String, String> params=new HashMap<String, String>();
	params.put("charset", tianTianKuaiPay.getCharset());
	params.put("goods_desc", tianTianKuaiPay.getGoods_desc());
	params.put("merchant_id",tianTianKuaiPay.getMerchant_id());
	params.put("nonce_str", tianTianKuaiPay.getNonce_str());
	params.put("notify_url",tianTianKuaiPay.getNotify_url());
	params.put("out_trade_no", tianTianKuaiPay.getOut_trade_no());
	params.put("return_url", tianTianKuaiPay.getReturn_url());
	params.put("service", tianTianKuaiPay.getService());
	params.put("sign_type", tianTianKuaiPay.getSign_type());
	params.put("total_amount", tianTianKuaiPay.getTotal_amount());
	params.put("version", tianTianKuaiPay.getVersion());
	params.put("sign", tianTianKuaiPay.getSign());
	String lines= HttpClientUtil.doPost(Address,params);
	log.info("天天快付返回JSON:" + lines);
	String codeUrl="";
	TianTianKuaiPayReturn returnParameter = JSONUtils.toBean(lines, TianTianKuaiPayReturn.class);
	if(returnParameter !=null){
		//获取实际支付地址
		String payInfoOld = returnParameter.getPay_info();
		String payInfoNew = payInfoOld.replace("\\","");
		log.info("实际支付地址为:"+ payInfoNew);
		response.sendRedirect(payInfoNew);
	}
	log.info("交易报文: " + tianTianKuaiPay);
%>

<%-- 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>

<body onload="pay.submit()">
	<form method="post" name="pay" id="pay" action="<%=payUrl%>">
		<TABLE>
			<TR>
				<TD>
				<input name='charset' type='hidden'value="<%=tianTianKuaiPay.getCharset()%>" /> 
				<input name='goods_desc' type='hidden' value="<%=tianTianKuaiPay.getGoods_desc()%>" /> 
				<input name='merchant_id' type='hidden' value="<%=tianTianKuaiPay.getMerchant_id()%>" /> 
				<input name='nonce_str' type='hidden' value="<%=tianTianKuaiPay.getNonce_str()%>" /> 
				<input name='notify_url' type='hidden' value="<%=tianTianKuaiPay.getNotify_url()%>" />
				<input name='out_trade_no' type='hidden' value="<%=tianTianKuaiPay.getOut_trade_no()%>" /> 
				<input name='return_url' type='hidden' value="<%=tianTianKuaiPay.getReturn_url()%>" /> 
				<input name='service' type='hidden' value="<%=tianTianKuaiPay.getService()%>" /> 
				<input name='sign_type' type='hidden' value="<%=tianTianKuaiPay.getSign_type()%>" /> 
				<input name='total_amount' type='hidden' value="<%=tianTianKuaiPay.getTotal_amount()%>" /> 
				<input name='version' type='hidden' value="<%=tianTianKuaiPay.getVersion()%>" /> 
				<input name='sign' type='hidden' value="<%=tianTianKuaiPay.getSign()%>" />
				</TD>
			</TR>
		</TABLE>
	</form>
</body>
</html> --%>
