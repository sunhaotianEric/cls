<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@page import="com.team.lottery.service.QuickBankTypeService"%>
<%@page import="com.team.lottery.util.BaseDwrUtil"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.vo.RechargeWithDrawOrder"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundThirdPayType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.pay.model.QianBaiTongPay"%>
<%@page import="com.team.lottery.vo.QuickBankType"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
<%  

    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
    String orderMoneyYuan = OrderMoney;
    
    User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
	
	Date currTime = new Date();
    //时间以yyyy-MM-dd HH:mm:ss的方式表示
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.CHINA);
    //时间以yyyyMMDDHHmmss的方式表示 
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss",Locale.CHINA);
    String webdate=new String(formatter1.format(currTime));
    String TradeDate=new String(formatter2.format(currTime));
    QuickBankTypeService quickBankTypeService = (QuickBankTypeService)ApplicationContextUtil.getBean("quickBankTypeService");
	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.getPayMemmberMsg(Long.valueOf(chargePayId));
	String  orderId = MakeOrderNum.makeOrderNum("CZ");
	log.info("用户名称: "+currentUser.getUserName()+" 用户 ID号: "+currentUser.getId()+" 生成订单号: "+orderId+" 订单金额: "+OrderMoney);
	QuickBankType query = new QuickBankType();
	query.setPayId(payId);
	String bankType = "";
	List<QuickBankType>  list = quickBankTypeService.getQuickBankTypeQuery(query);
	if(list.size() > 0){
		bankType = list.get(0).getBankType();
	}
	
	String payUrl = chargePay.getPayUrl();//借贷混合
	String Address = chargePay.getAddress(); //最终地址
	log.info("payUrl: "+payUrl+" Address: "+Address);
	//钱佰通请求对象
	QianBaiTongPay qianBaiFuPay = new QianBaiTongPay();
	qianBaiFuPay.setX1_Amount(OrderMoney);
	qianBaiFuPay.setX2_BillNo(orderId);
	qianBaiFuPay.setX3_MerNo(chargePay.getMemberId());
	qianBaiFuPay.setX4_ReturnURL(chargePay.getReturnUrl());
	qianBaiFuPay.setX5_NotifyURL(chargePay.getNotifyUrl());
	qianBaiFuPay.setX7_PaymentType(bankType);
	qianBaiFuPay.setX8_MerRemark("");
	String ip = BaseDwrUtil.getRequestIp(request);
	qianBaiFuPay.setX9_ClientIp(ip);
	qianBaiFuPay.setIsApp("");

	String Md5key = md5Util.getMD5ofStr(chargePay.getSign()).toUpperCase();///////////md5密钥（KEY）
	String md5 =new String("X1_Amount="+qianBaiFuPay.getX1_Amount()+"&X2_BillNo="+qianBaiFuPay.getX2_BillNo()+"&X3_MerNo="+qianBaiFuPay.getX3_MerNo()+"&X4_ReturnURL="+qianBaiFuPay.getX4_ReturnURL() +"&"+Md5key);//MD5签名格式
	String Signature = md5Util.getMD5ofStr(md5).toUpperCase();//计算MD5值
	log.info("md5原始串: "+md5);
	log.info("交易签名: "+Signature);
	qianBaiFuPay.setX6_MD5info(Signature);
	log.info("交易报文: "+qianBaiFuPay);

    //冲值订单
	RechargeOrder rechargeOrder = new RechargeOrder();
	//用户ID
	rechargeOrder.setUserId(currentUser.getId());
	//快捷ID
	rechargeOrder.setChargePayId(chargePay.getId());
	//用户名称
	rechargeOrder.setUserName(currentUser.getUserName());
	//申请金额
	rechargeOrder.setApplyValue(new BigDecimal(orderMoneyYuan));
	//订单创建时间
	rechargeOrder.setCreatedDate(new Date());
	//充值类型
	rechargeOrder.setPayType(payType);
	//第三方充值类型
	rechargeOrder.setThirdPayType(EFundThirdPayType.GTPAY.name());
	//操作描述
	rechargeOrder.setOperateDes(EFundPayType.valueOf(payType).getDescription());
	//流水号
	rechargeOrder.setSerialNumber(orderId);
	rechargeOrder.setPostscript("");
	rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT.name());
	rechargeOrder.setBankName("");
	rechargeOrder.setSubbranchName("");
	rechargeOrder.setBankAccountName("");
	rechargeOrder.setBankCardNum("");
	rechargeOrder.setBankType(bankType);
	rechargeOrder.setBizSystem(currentUser.getBizSystem());
	rechargeOrder.setRefType(EFundRefType.THIRDPAY.name());
	rechargeOrder.setFromType(currentUser.getLoginType());
	rechargeWithDrawOrderService.insertSelective(rechargeOrder);
 %>
 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>

<body onload="pay.submit()">
<form method="post" name="pay" id="pay" action="<%=payUrl%>">
<TABLE>
<TR>
	<TD>
	<input name='X3_MerNo' type='hidden' value= "<%=qianBaiFuPay.getX3_MerNo()%>"/>
	<input name='X1_Amount' type='hidden' value= "<%=qianBaiFuPay.getX1_Amount()%>"/>
	<input name='X2_BillNo' type='hidden' value= "<%=qianBaiFuPay.getX2_BillNo()%>"/>
	<input name='X4_ReturnURL' type='hidden' value= "<%=qianBaiFuPay.getX4_ReturnURL()%>"/>
	<input name='X5_NotifyURL' type='hidden' value= "<%=qianBaiFuPay.getX5_NotifyURL()%>"/>		
	<input name='X6_MD5info' type='hidden' value= "<%=qianBaiFuPay.getX6_MD5info()%>"/>
	<input name='X7_PaymentType' type='hidden' value= "<%=qianBaiFuPay.getX7_PaymentType()%>"/>
	<input name='X8_MerRemark' type='hidden' value= "<%=qianBaiFuPay.getX8_MerRemark()%>"/>		
	<input name="isApp" type="hidden"  value="">	
	</TD>
</TR>
</TABLE>
	
</form>	

</body>
</html>
