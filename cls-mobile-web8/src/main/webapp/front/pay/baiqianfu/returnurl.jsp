<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.vo.RechargeWithDrawOrder"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@page import="java.io.*"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.team.lottery.extvo.returnModel.QianBaiTongReturnParameter"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
    BufferedReader  streamReader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));  
    StringBuilder responseStrBuilder = new StringBuilder();  
    String inputStr;  
    while ((inputStr = streamReader.readLine()) != null) {
    	responseStrBuilder.append(inputStr);   
    }
    log.info("接收钱佰付回调通知报文: "+responseStrBuilder.toString());
    JSONObject jsonObject = JSONUtils.toJSONObject(responseStrBuilder.toString());
    QianBaiTongReturnParameter qianBaiTongReturn = JSONUtils.toBean(jsonObject, QianBaiTongReturnParameter.class);
	String MerNo = qianBaiTongReturn.getMerNo();
	String Amount = qianBaiTongReturn.getAmount();
	String BillNo = qianBaiTongReturn.getBillNo();
	String Succeed = qianBaiTongReturn.getSucceed();
	String MD5info = qianBaiTongReturn.getMD5info();
	String Result = qianBaiTongReturn.getResult();
	String MerRemark = qianBaiTongReturn.getMerRemark();
	
	log.info("钱佰付支付成功通知平台处理信息如下:商户订单号: "+MerNo+" 订单结果: "+Succeed+" 订单金额: "+Amount+" 商务订单号: "+BillNo+"MD5签名: "+MD5info);
    
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("RechargeOrderService");
	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(BillNo);
	if(rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1){
		log.error("根据订单号[" + BillNo + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知赢钱佰付报文接收成功
	    out.println("SUCCESS");
		return;
	}
	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
	if(rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + BillNo + "]查找充值订单记录为空");
		//通知钱佰付报文接收成功
	    out.println("SUCCESS");
		return;
	}
	
	//签名
	String Md5key = md5Util.getMD5ofStr(rechargeWithDrawOrder.getSign()).toUpperCase();
	String MARK = "&";
	String md5 = "Amount="+Amount+"&BillNo="+BillNo+"&MerNo="+MerNo+"&Succeed="+Succeed +"&" +Md5key;
	log.info(md5);
	
	//Md5加密串
	String WaitSign = md5Util.getMD5ofStr(md5).toUpperCase();
	log.info("WaitSign " + WaitSign);

	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if (WaitSign.compareTo(MD5info) == 0) {
		boolean dealResult = false;
		if (!StringUtils.isEmpty(Succeed) && ("88").equals(Succeed)) {
			BigDecimal factMoneyValue = new BigDecimal(Amount);
			try {
				dealResult = rechargeWithDrawOrderService.rechargeOrderSuccessDeal(BillNo, factMoneyValue);
			} catch (UnEqualVersionException e) {
				log.error("钱佰付支付充值时发现版本号不一致,订单号[" + BillNo + "]");
				//启动新进程进行多次重试
				UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
						UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION,
						BillNo, factMoneyValue, "");
				thread.start();
			} catch (Exception e) {
				//异常不进行重试
				log.error(e.getMessage());
			}
		} else {
			if (!("88").equals(Succeed)) {
				log.info("钱佰付支付处理错误支付结果：" + Succeed);
			}
		}
		log.info("钱佰付订单处理入账结果:{}", dealResult);
		//通知赢通宝报文接收成功
		out.println("SUCCESS");
		return;
	} else {
		log.info("MD5校验失败,报文返回MD5签名内容[" + MD5info + "],计算MD5签名内容["
				+ WaitSign + "]");
		//让第三方继续补发
		out.println("fail");
		return;
	}
%>