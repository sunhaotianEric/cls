<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage=""%>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.vo.RechargeWithDrawOrder"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.pay.util.RequestGetParamUtils"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page
	import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@page import="java.io.*"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.team.lottery.extvo.returnModel.BaDaPayReturnParam"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
	String path = request.getContextPath();
	//获取当前的状态
	//获取订单状态
	Map<String, String> map = RequestGetParamUtils.getParams(request);
	String status = map.get("status");
	//获取商户编号
	String customerid = map.get("customerid");
	//获取平台订单号
	String sdpayno = map.get("sdpayno");
	//获取商户订单号
	String sdorderno = map.get("sdorderno");
	//获取交易金额
	String total_fee = map.get("total_fee");
	//获取支付类型
	String paytype = map.get("paytype");
	//获取签名
	String sign = map.get("sign");
	log.info("易付支付成功通知平台处理信息如下: 订单状态:" + status + " 商户编号:" + customerid + "平台订单号:" + sdpayno + "商户订单号:" + sdorderno + "交易金额:" + total_fee + "支付类型:" + paytype + "签名:"
			+ sign);

	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService) ApplicationContextUtil.getBean("rechargeOrderService");
	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(sdorderno);
	if (rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1) {
		log.error("根据订单号[" + sdorderno + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知易付支付报文接收成功
		response.getOutputStream().write("success".getBytes("UTF-8"));
	}
	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
	if (rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + sdorderno + "]查找充值订单记录为空");
		//通知易付支付报文接收成功
		response.getOutputStream().write("success".getBytes("UTF-8"));
	}
	//计算签名
	String Md5key = rechargeWithDrawOrder.getSign();
	String md5 = "customerid=" + customerid + "&status=" + status + "&sdpayno=" + sdpayno + "&sdorderno=" + sdorderno + "&total_fee=" + total_fee + "&paytype="+paytype
			+ "&" + Md5key;
	log.info("当前MD5源码:" + md5);

	//Md5加密串
	String WaitSign = Md5Util.getMD5ofStr(md5);
	log.info("加密后的MD5: " + WaitSign);

	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if (WaitSign.compareTo(sign) == 0) {
		boolean dealResult = false;
		if (!StringUtils.isEmpty(status) && ("1").equals(status)) {
			BigDecimal factMoneyValue = new BigDecimal(status);
			try {
				dealResult = rechargeWithDrawOrderService.rechargeOrderSuccessDeal(sdorderno, factMoneyValue);
			} catch (UnEqualVersionException e) {
				log.error("易付支付充值时发现版本号不一致,订单号[" + sdorderno + "]");
				//启动新进程进行多次重试
				UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, sdorderno,
						factMoneyValue, "");
				thread.start();
			} catch (Exception e) {
				//异常不进行重试
				log.error(e.getMessage());
			}
		} else {
			if (!("1").equals(status)) {
				log.info("易付支付处理错误支付结果：" + status);
			}
		}
		log.info("易付付订单处理入账结果:{}", dealResult);
		//通知接收成功
		out.println("success");
		return;
	} else {
		log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容[" + WaitSign + "]");
		//让第三方继续补发
		out.println("success");
		return;
	}
%>