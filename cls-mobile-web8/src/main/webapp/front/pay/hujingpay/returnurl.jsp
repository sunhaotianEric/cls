<%@page import="com.team.lottery.pay.model.hujingpay.HuJingPayReturn"%>
<%@page import="com.team.lottery.pay.model.muming.MuMingPayReturn"%>
<%@page import="com.team.lottery.pay.model.hujingpay.RSAUtil"%>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage=""%>
<%@page import="com.team.lottery.pay.model.jindouyun.JinDouYunPayReturn"%>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.vo.RechargeWithDrawOrder"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page
	import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page
	import="com.team.lottery.vo.ChargePay,com.team.lottery.extvo.ChargePayVo,com.team.lottery.service.ChargePayService"%>
<%@page import="java.math.*"%>
<%@page import="java.io.*"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="net.sf.json.JSONObject"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.extvo.returnModel.DuDuPayReturnParam"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
	String path = request.getContextPath();
	// 商户号.
	String merchantId = request.getParameter("merchantId");
	// 商户订单号.
	String merchantOrderId = request.getParameter("merchantOrderId");
	// 商户对应用户ID
	String merchantUid = request.getParameter("merchantUid");
	// 支付金额
	String money = request.getParameter("money");
	// 订单号.
	String orderId = request.getParameter("orderId");
	// 支付金额.
	String payAmount = request.getParameter("payAmount");
	// 支付时间.
	String payTime = request.getParameter("payTime");
	// 支付方式.
	String payType = request.getParameter("payType");
	// 签名.
	String sign = request.getParameter("sign");
	// 新建虎鲸回调对象.
	HuJingPayReturn huJingPayReturn = new HuJingPayReturn();
	huJingPayReturn.setMerchantId(merchantId);
	huJingPayReturn.setMerchantOrderId(merchantOrderId);
	huJingPayReturn.setMerchantUid(merchantUid);
	huJingPayReturn.setMoney(money);
	huJingPayReturn.setOrderId(orderId);
	huJingPayReturn.setPayAmount(payAmount);
	huJingPayReturn.setPayTime(payTime);
	huJingPayReturn.setPayType(payType);
	// 打印日志.通知成功.
	log.info("虎鲸报文信息:" + huJingPayReturn.toString());

	RechargeOrderService rechargeOrderService = (RechargeOrderService) ApplicationContextUtil.getBean("rechargeOrderService");
	List<RechargeOrder> rechargeOrders = rechargeOrderService.getRechargeOrderBySerialNumber(merchantOrderId);
	ChargePayService chargePayService = (ChargePayService) ApplicationContextUtil.getBean("chargePayService");
	if (rechargeOrders == null || rechargeOrders.size() != 1) {
		log.error("根据订单号[" + merchantOrderId + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知虎鲸支付报文接收成功
		out.print("success");
	}
	RechargeOrder rechargeWithDrawOrder = rechargeOrders.get(0);
	if (rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + merchantOrderId + "]查找充值订单记录为空");
		//通知虎鲸支付报文接收成功
		out.print("success");
	}
	// 查询出充值配置对象.
	Long chargePayId = null;
	if (rechargeOrders != null && rechargeOrders.size() > 0) {
		chargePayId = rechargeOrders.get(0).getChargePayId();
	}
	ChargePay chargePay = chargePayService.selectByPrimaryKey(chargePayId);
	String privateKey = "";
	if (chargePay != null) {
		privateKey = chargePay.getPrivatekey();
	}
	//获取秘钥.
	String Md5key = rechargeWithDrawOrder.getSign();
	String MARK = "&";
	//拼接加密字符串.
	String rsaStr = "merchantId=" + merchantId + MARK + 
					"merchantOrderId=" + merchantOrderId + MARK + 
					"merchantUid=" + merchantUid + MARK + 
					"money=" + money + MARK + 
					"orderId=" + orderId + MARK + 
					"payAmount=" + payAmount + MARK + 
					"payTime=" + payTime + MARK + 
					"payType=" + payType;
	log.info("虎鲸需要RSA加密的字符串为[" + rsaStr + "]");
	boolean ret = false;
	try {
		// 使用公钥进行RSA解密
		ret = RSAUtil.rsaVerify(chargePay.getPublickey(), sign, rsaStr);
		// 结果为true的时候进行入账处理.
		if (ret = true) {
			BigDecimal factMoneyValue = new BigDecimal(money);
			boolean dealResult = false;
			try {
				dealResult = rechargeOrderService.rechargeOrderSuccessDeal(merchantOrderId, factMoneyValue);
			} catch (UnEqualVersionException e) {
				log.error("虎鲸支付充值时发现版本号不一致,订单号[" + merchantOrderId + "]");
				//启动新进程进行多次重试
				UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION,
						merchantOrderId, factMoneyValue, "");
				thread.start();
			} catch (Exception e) {
				//异常不进行重试
				log.error(e.getMessage());
			}
			log.info("虎鲸支付订单处理入账结果:{}", dealResult);
			//通知凡客付报文接收成功
			out.println("success");
			return;
		} else {
			log.info("RSA校验失败,报文返回签名内容[" + sign + "],计算RSA签名内容结果[" + ret + "]");
			out.println("success");
			return;
		}
	} catch (Exception e) {
		e.printStackTrace();
	}
%>