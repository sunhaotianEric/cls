<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@page import="com.team.lottery.util.BaseDwrUtil"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.vo.RechargeWithDrawOrder"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundThirdPayType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.pay.model.ShanfuPay"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
<%  

    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
    String orderMoneyYuan = OrderMoney;
    
    User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
	
	Date currTime = new Date();
    //时间以yyyy-MM-dd HH:mm:ss的方式表示
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.CHINA);
    //时间以yyyyMMDDHHmmss的方式表示 
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss",Locale.CHINA);
    String webdate=new String(formatter1.format(currTime));
    String TradeDate=new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.getPayMemmberMsg(Long.valueOf(chargePayId));
	String  orderId = MakeOrderNum.makeOrderNum("CZ");
	log.info("用户名称: "+currentUser.getUserName()+" 用户 ID号: "+currentUser.getId()+" 生成订单号: "+orderId+" 订单金额: "+OrderMoney);
	
	
	if (!"".equals(OrderMoney)) 
	{	BigDecimal a;
		a = new BigDecimal(OrderMoney).multiply(BigDecimal.valueOf(100)); //使用分进行提交
		OrderMoney=String.valueOf(a.setScale(0));
	}
	else{
		OrderMoney = ("0");
	}
	String payUrl = chargePay.getPayUrl();//转发支付网关地址
	String Address = chargePay.getAddress(); //最终闪付地址
	log.info("payUrl: "+payUrl+" Address: "+Address);
	
	//宝付请求对象
	ShanfuPay shanfuPay =new ShanfuPay();
	//商户 ID
    shanfuPay.setMemberID(chargePay.getMemberId());
	//终端 ID
    shanfuPay.setTerminalID(chargePay.getTerminalId());
	//接口版本固定值:4.0
    shanfuPay.setInterfaceVersio(chargePay.getInterfaceVersion());
	//加密类型固定值:1
    shanfuPay.setKeyType("1");
	//银行通道编号
    shanfuPay.setPayID(payId);
	//订单日期
    shanfuPay.setTradeDate(TradeDate);
	//订单号
    shanfuPay.setTransID(orderId);
	//订单金额
    shanfuPay.setOrderMoney(OrderMoney);
	//商品名称
    shanfuPay.setProductName("");
	//商品数量
    shanfuPay.setAmount("1");
	//用户名称
    shanfuPay.setUsername(currentUser.getUserName());
	//附加字段
    shanfuPay.setAdditionalInfo(currentUser.getBizSystem());
	//通知类型
    shanfuPay.setNoticeType("1");
	//页面返回地址
    shanfuPay.setPageUrl(chargePay.getNotifyUrl());
	//交易通知地址
    shanfuPay.setReturnUrl(chargePay.getReturnUrl());
	
	//生成md5密钥签名
    String Md5key = chargePay.getSign();///////////md5密钥（KEY）
	String MARK = "|";
	String md5 =new String(shanfuPay.getMemberID()+MARK+payId+MARK+TradeDate+MARK+shanfuPay.getTransID()+MARK+OrderMoney+MARK+shanfuPay.getPageUrl()+MARK+shanfuPay.getReturnUrl()+MARK+shanfuPay.getNoticeType()+MARK+Md5key);//MD5签名格式
	String Signature = md5Util.getMD5ofStr(md5);//计算MD5值
	
    //交易签名
    shanfuPay.setMd5Sign(Signature);
    log.info("md5原始串: "+md5 + "生成后的交易签名:" + Signature);
    //冲值订单
	RechargeOrder rechargeOrder = new RechargeOrder();
	//用户ID
	rechargeOrder.setUserId(currentUser.getId());
	//快捷ID
	rechargeOrder.setChargePayId(chargePay.getId());
	//用户名称
	rechargeOrder.setUserName(currentUser.getUserName());
	//申请金额
	rechargeOrder.setApplyValue(new BigDecimal(orderMoneyYuan));
	//订单创建时间
	rechargeOrder.setCreatedDate(new Date());
	//充值类型
	rechargeOrder.setPayType(payType);
	//第三方充值类型
	rechargeOrder.setThirdPayType(EFundThirdPayType.SHANFU.name());
	//操作描述
	rechargeOrder.setOperateDes(EFundPayType.valueOf(payType).getDescription());
	//流水号
	rechargeOrder.setSerialNumber(orderId);
	rechargeOrder.setPostscript("");
	rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT.name());
	rechargeOrder.setBankName("");
	rechargeOrder.setSubbranchName("");
	rechargeOrder.setBankAccountName("");
	rechargeOrder.setBankCardNum("");
	rechargeOrder.setBankType("");
	rechargeOrder.setBizSystem(currentUser.getBizSystem());
	rechargeOrder.setRefType(EFundRefType.THIRDPAY.name());
	rechargeOrder.setFromType(currentUser.getLoginType());
	rechargeWithDrawOrderService.insertSelective(rechargeOrder);
	log.info("提交中转支付网关地址["+payUrl+"],实际闪付支付网关地址["+Address +"],交易报文: "+shanfuPay);
 %>
 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>

<body onload="pay.submit()">
<form method="post" name="pay" id="pay" action="<%=payUrl%>">
<TABLE>
<TR>
	<TD>
	<input name='Address' type='hidden' value= "<%=Address%>"/>
	<input name='MemberID' type='hidden' value= "<%=shanfuPay.MemberID%>"/>
	<input name='TerminalID' type='hidden' value= "<%=shanfuPay.TerminalID%>"/>
	<input name='InterfaceVersion' type='hidden' value= "<%=shanfuPay.InterfaceVersio%>"/>
	<input name='KeyType' type='hidden' value= "<%=shanfuPay.KeyType%>"/>
	<input name='PayID' type='hidden' value= "<%=shanfuPay.PayID%>"/>		
	<input name='TradeDate' type='hidden' value= "<%=shanfuPay.TradeDate%>" />
	<input name='TransID' type='hidden' value= "<%=shanfuPay.TransID%>" />
	<input name='OrderMoney' type='hidden' value= "<%=shanfuPay.OrderMoney%>"/>
	<input name='ProductName' type='hidden' value= "<%=shanfuPay.ProductName%>"/>
	<input name='Amount' type='hidden' value= "<%=shanfuPay.Amount%>"/>
	<input name='Username' type='hidden' value= "<%=shanfuPay.Username%>"/>
	<input name='AdditionalInfo' type='hidden' value= "<%=shanfuPay.AdditionalInfo%>"/>
	<input name='PageUrl' type='hidden' value= "<%=shanfuPay.PageUrl%>"/>
	<input name='ReturnUrl' type='hidden' value= "<%=shanfuPay.ReturnUrl%>"/>	
	<input name='Signature' type='hidden' value="<%=shanfuPay.Md5Sign%>"/>
	<input name='NoticeType' type='hidden' value= "<%=shanfuPay.NoticeType%>"/>
	</TD>
</TR>
</TABLE>
	
</form>	

</body>
</html>
