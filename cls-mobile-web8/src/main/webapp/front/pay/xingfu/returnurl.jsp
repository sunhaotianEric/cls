<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.vo.RechargeWithDrawOrder"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String service = request.getParameter("service");//接口名字
	String merId = request.getParameter("merId");//商户号
	String tradeNo = request.getParameter("tradeNo");//商户订单号
	String tradeDate = request.getParameter("tradeDate");//商户交易日期
	String opeNo = request.getParameter("opeNo");//支付平台订单号
	String opeDate = request.getParameter("opeDate");//支付平台订单日期
	String amount = request.getParameter("amount");//支付金额	
	String status = request.getParameter("status");//订单状态
	String extra = request.getParameter("extra");//商户参数
	String payTime = request.getParameter("payTime");//支付时间
	String sign = request.getParameter("sign");//MD5签名
	String notifyType = request.getParameter("notifyType");//通知类型
	log.info("service:"+service+"merId:"+merId+"tradeNo:"+tradeNo+"tradeDate:"+tradeDate+"opeNo:"+opeNo+"opeDate:"+
			opeDate+"amount:"+amount+"status:"+status+"extra:"+extra+"payTime:"+payTime+"sign:"+sign+"notifyType:"+notifyType);
	log.info("星付支付成功通知平台处理信息如下: 商户号: "+merId+" 订单号: "+tradeNo+" 支付结果: "+status+" 实际成功金额: "+
			amount+" 支付完成时间: "+tradeDate+" 第三方返回MD5签名: "+sign);
	
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(tradeNo);
	if(rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1){
		log.error("根据订单号[" + tradeNo + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知星付接收报文成功	
	    out.println("SUCCESS");
		return;
	}
	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
	if(rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + tradeNo + "]查找充值订单记录为空");
		//通知星付接收报文成功
	    out.println("SUCCESS");
		return;
	}

	//签名
	String Md5key = rechargeWithDrawOrder.getSign();
	String MARK = "&";
	String md5 = "service=" + service + MARK + "merId=" + merId + MARK + "tradeNo=" + tradeNo + MARK + "tradeDate=" + tradeDate + MARK + "opeNo=" + opeNo + MARK
	+ "opeDate=" + opeDate + MARK + "amount=" + amount + MARK + "status=" + status
	+ MARK + "extra=" + extra+ MARK + "payTime=" + payTime+Md5key;
	//Md5加密串
	String WaitSign = md5Util.getMD5ofStr(md5).toUpperCase();

	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if(WaitSign.compareTo(sign)==0){
		boolean dealResult = false;
		if(!StringUtils.isEmpty(status) && status.equals("1")){
			//
			BigDecimal factMoneyValue = new BigDecimal(amount);
			try {
				dealResult = rechargeWithDrawOrderService.rechargeOrderSuccessDeal(tradeNo,factMoneyValue);
			}catch(UnEqualVersionException e) {
		   		log.error("星付在线充值时发现版本号不一致,订单号["+tradeNo+"]");
		   		//启动新进程进行多次重试
		   		UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION
		   				, tradeNo,factMoneyValue,"");
		   		thread.start();
			} catch(Exception e){
				//异常不进行重试
				log.error(e.getMessage());
			}
		} else {
			if(!status.equals("1")){
				log.info("星付支付处理错误支付结果："+status);
			}
		}
		log.info("星付订单处理入账结果:{}", dealResult);
		//通知星付接收报文成功
	    out.println("SUCCESS");
		
	}else{
		log.info("MD5校验失败,报文返回MD5签名内容["+ sign +"],计算MD5签名内容[" + WaitSign + "]");
		//不输出OK，让第三方继续补发
		out.println("ERROR");
	}
%>