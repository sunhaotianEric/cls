<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.team.lottery.service.QuickBankTypeService"%>
<%@page import="com.team.lottery.util.BaseDwrUtil"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.vo.RechargeWithDrawOrder"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundThirdPayType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.service.RechargeConfigService"%>
<%@page import="com.team.lottery.vo.RechargeConfig"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.team.lottery.pay.model.wannian.WanNianPay"%>
<%@page import="com.team.lottery.vo.QuickBankType"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>

<jsp:useBean id='md5Util' scope='request'
	class='com.team.lottery.pay.util.Md5Util' />
<%
	//打印日志
	Logger log = LoggerFactory.getLogger(this.getClass());
	//获取当前web路径
	String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	String serialNumber = request.getParameter("serialNumber");//充值类型
	/* double d=Double.valueOf(OrderMoney);
	DecimalFormat df = new DecimalFormat("#.00");
	OrderMoney = df.format(d); */
	
	String orderMoneyYuan = OrderMoney;
	//判断当前请求页面用户是否登录,没有的话就跳往登录页面
	User currentUser = (User) request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	if (currentUser == null) {
		response.sendRedirect(path + "/gameBet/login.html");
	}

	//获取订单提交时间
	Date currTime = new Date();
	QuickBankTypeService quickBankTypeService = (QuickBankTypeService) ApplicationContextUtil.getBean("quickBankTypeService");
	ChargePayService chargePayService = (ChargePayService) ApplicationContextUtil.getBean("chargePayService");
	/* RechargeWithDrawOrderService rechargeWithDrawOrderService = (RechargeWithDrawOrderService) ApplicationContextUtil.getBean("rechargeWithDrawOrderService"); */
	ChargePay chargePay = chargePayService.getPayMemmberMsg(Long.valueOf(chargePayId));
	//打印日志
	log.info("业务系统[{}],用户名[{}],发起了万年支付，生成订单号[{}],支付金额[{}],PayId[{}],payType[{}],chargePayId[{}]",
  			currentUser.getBizSystem(), currentUser.getUserName(), serialNumber, OrderMoney, payId, payType, chargePayId);
	QuickBankType query = new QuickBankType();
	query.setPayId(payId);
	String bankType = "";
	List<QuickBankType> list = quickBankTypeService.getQuickBankTypeQuery(query);
	if (list.size() > 0) {
		bankType = list.get(0).getBankType();
	}

	//生成需要跳转的支付地址;
	String payUrl = chargePay.getPayUrl();//转发支付网关地址
	String Address = chargePay.getAddress(); //最终地址

	log.info("万年支付报文转发地址payUrl: "+payUrl+",实际网关地址address: "+Address);

	//同步跳转地址
	String returnUrl = "";
	if (chargePay.getNotifyUrl().startsWith("http://") || chargePay.getNotifyUrl().startsWith("https://")) {
		returnUrl = chargePay.getNotifyUrl();
	} else {
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
		returnUrl = basePath + chargePay.getNotifyUrl();
	}
	
	if (!"".equals(OrderMoney)) {	
		 BigDecimal a;
		a = new BigDecimal(OrderMoney); //使用分进行提交
		OrderMoney=String.valueOf(a.multiply(new BigDecimal(100)));
	}
	else{
		OrderMoney = ("0");
	} 
	
	//设置万年支付请求对象
	WanNianPay wanNianPay = new WanNianPay();
	//设置版本号,默认版本是1.0;
	wanNianPay.setVersion(chargePay.getInterfaceVersion());
	//设置商户编号
	wanNianPay.setCpId(chargePay.getMemberId());
	//商品名
	wanNianPay.setSubject(currentUser.getUserName());
	//商品说明
	wanNianPay.setDescription(currentUser.getUserName());
	//设置商户订单号
	wanNianPay.setOrderIdCp(serialNumber);
	//设置支付类型;
	wanNianPay.setChannel(payId);
	//设置支付金额
	wanNianPay.setMoney(OrderMoney);
	//设置下行异步通知地址,返回地址
	wanNianPay.setNotifyUrl(chargePay.getReturnUrl());
	
	Date time = new Date();
    //时间以yyyyMMDDHHmmss的方式表示 
    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss",Locale.CHINA);
    String timestamp=new String(formatter.format(time));
  	//当前时间戳（13 位）
	wanNianPay.setTimestamp(timestamp);
  	
	//用户 ip
	wanNianPay.setIp(request.getRemoteAddr());
	//通道方式（固定applyqr）
	wanNianPay.setCommand("applyqr");
	//获取商户秘钥
	String MD5Key = chargePay.getSign();
	String MARK = "&";
	//需要加密的字段;
	String wanNianPayMD5 = "version=" + wanNianPay.getVersion() + MARK + 
							"cpId=" + wanNianPay.getCpId() + MARK + 
							"money=" + OrderMoney + MARK +
							"channel=" + wanNianPay.getChannel() + MARK + 
							"subject=" + wanNianPay.getSubject() + MARK + 
							"description=" + wanNianPay.getDescription() + MARK + 
							"orderIdCp=" + wanNianPay.getOrderIdCp() + MARK + 
							"notifyurl=" + wanNianPay.getNotifyUrl() + MARK + 
							"timestamp=" + wanNianPay.getTimestamp() + MARK + 
							"ip=" + wanNianPay.getIp() + MARK + 
							"command=" + wanNianPay.getCommand() + MARK + 
							MD5Key;
	String Signature = Md5Util.getMD5ofStr(wanNianPayMD5);//获取MD5值
	log.info("md5原始串: " + wanNianPayMD5 + "生成后的交易签名:" + Signature);
	//设置交易签名
	wanNianPay.setSign(Signature);
	log.info("交易报文: "+wanNianPay);
	
	Map<String, String> postDataMap = new TreeMap<String, String>();
	postDataMap.put("version", wanNianPay.getVersion());
	postDataMap.put("cpId", wanNianPay.getCpId());
	postDataMap.put("channel", wanNianPay.getChannel());
	postDataMap.put("money", wanNianPay.getMoney());
	postDataMap.put("subject", wanNianPay.getSubject());
	postDataMap.put("description", wanNianPay.getDescription());
	postDataMap.put("orderIdCp", wanNianPay.getOrderIdCp());
	postDataMap.put("notifyUrl", wanNianPay.getNotifyUrl());
	postDataMap.put("timestamp", wanNianPay.getTimestamp());
	postDataMap.put("ip", wanNianPay.getIp());
	postDataMap.put("command", wanNianPay.getCommand());
	postDataMap.put("sign", wanNianPay.getSign());
	//获取第三方给我们返回的JSON数据做判断跳转
	String result = HttpClientUtil.doPost(Address, postDataMap);
	log.info("万年付支付提交支付报文，返回内容:{}", result);
	Boolean isJson = JSONUtils.isJson(result);
	String basePath = "";
	if (isJson == true) {
		JSONObject jsonObject =  JSONUtils.toJSONObject(result);
		if (StringUtils.isBlank(result)) {
			out.print("万年付发起支付失败,请联系客服");
			return;
		}
		
		String code = jsonObject.getString("status");
		if (code.equals("0")) {
			String data = jsonObject.getString("data");
			String payInfoNew = data.replace("\\","");
			log.info("实际支付地址为:"+ payInfoNew);
			response.sendRedirect(payInfoNew);		
		}else {
			out.print(code);
			log.error("支付发起失败:" + result);
			return;
		}
	} else {
		out.print(result);
		log.error("不是json格式的数据: " + result);
		return;
	}
	log.info("交易报文: " + wanNianPay);
%>


<%-- <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>

<body onload="pay.submit()">
	<form method="post" name="pay" id="pay" action="<%=payUrl%>">
		<TABLE>
			<TR>
				<TD>
				<input name='version' type='hidden' value="<%=wanNianPay.getVersion()%>" /> 
				<input name='cpId' type='hidden' value="<%=wanNianPay.getCpId()%>" /> 
				<input name='channel' type='hidden' value="<%=wanNianPay.getChannel()%>" /> 
				<input name='money' type='hidden' value="<%=wanNianPay.getMoney()%>" /> 
				<input name='subject' type='hidden' value="<%=wanNianPay.getSubject()%>" />
				<input name='description' type='hidden' value="<%=wanNianPay.getDescription()%>" /> 
				<input name='orderIdCp' type='hidden' value="<%=wanNianPay.getOrderIdCp()%>" /> 
				<input name='notifyUrl' type='hidden' value="<%=wanNianPay.getNotifyUrl()%>" /> 
				<input name='timestamp' type='hidden' value="<%=wanNianPay.getTimestamp()%>" />
				<input name='ip' type='hidden' value="<%=wanNianPay.getIp()%>" /> 
				<input name='command' type='hidden' value="<%=wanNianPay.getCommand()%>" /> 
				<input name='sign' type='hidden' value="<%=wanNianPay.getSign()%>" />
				</TD>
			</TR>
		</TABLE>

	</form>

</body>
</html> --%>
