<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@page import="com.team.lottery.util.BaseDwrUtil"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.vo.RechargeWithDrawOrder"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundThirdPayType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.RSA"%>
<%@page import="com.team.lottery.pay.model.JianZhengBaoPay"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
 <%@page errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.InetAddress"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="com.team.lottery.extvo.returnModel.YinHeParameter"%>
<%@page import="com.team.lottery.pay.model.YinHePay"%>
<%@page import="java.util.*"%>
<%@page import="net.sf.json.JSONObject"%>
<%@ page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
    String orderMoneyYuan = OrderMoney;
 
    
    User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
	
	

	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));
	String  orderId=MakeOrderNum.makeOrderNum("CZ");
	log.info("用户名称: "+currentUser.getUserName()+" 用户 ID号: "+currentUser.getId()+" 生成订单号: "+orderId+" 订单金额: "+OrderMoney);
	
	
	/* if(!"".equals(OrderMoney)) {	
		 BigDecimal a;
		a = new BigDecimal(OrderMoney); //使用分进行提交
		OrderMoney=String.valueOf(a.multiply(new BigDecimal(100)).intValue());
	}
	else{
		OrderMoney = ("0");
	} */ 
	String payUrl = chargePay.getPayUrl();//转发银河网关地址
	String Address = chargePay.getAddress(); //最终银河地址
	log.info("payUrl: "+payUrl+" Address: "+Address);
	YinHePay yinHePay =new YinHePay();
	if(payType.equals("ALIPAYWAP")){
		yinHePay.setReturn_type("html");
	}else {
		yinHePay.setReturn_type("json");
	}
	yinHePay.setApi_code(chargePay.getMemberId());
	yinHePay.setIs_type("alipay");
	yinHePay.setNotify_url(chargePay.getReturnUrl());
	yinHePay.setReturn_url(chargePay.getNotifyUrl());
	yinHePay.setOrder_id(orderId);
	yinHePay.setPrice(OrderMoney);
	yinHePay.setMark("cz");
	Date currTime = new Date();
    //时间以yyyyMMDDHHmmss的方式表示 
    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss",Locale.CHINA);
    String webdate=new String(formatter.format(currTime));
    yinHePay.setTime(webdate);
    //时间以yyyy-MM-dd HH:mm:ss的方式表示
    String Md5key = chargePay.getSign();///////////md5密钥（KEY）
    String MARK = "&";
	String md5 =new String("api_code=" + yinHePay.getApi_code() + MARK + "is_type=" + yinHePay.getIs_type() +MARK+ "mark=" + 
    yinHePay.getMark() +MARK+"notify_url=" + yinHePay.getNotify_url()+MARK+"order_id=" + yinHePay.getOrder_id() +MARK+ "price=" + yinHePay.getPrice()+MARK+
	 "return_type=" + yinHePay.getReturn_type() + MARK+"return_url=" + yinHePay.getReturn_url() + MARK+"time=" + yinHePay.getTime() + MARK + "key=" + Md5key);//MD5签名格式
	String Signature = md5Util.getMD5ofStr(md5).toUpperCase();//计算MD5值
    yinHePay.setSign(Signature);
    //交易签名
    /* jianZhengBaoPay.setSign(Signature); */
    //log.info("md5原始串: "+md5 + "生成后的交易签名:" + Signature); */
    //冲值订单
	RechargeOrder rechargeOrder = new RechargeOrder();
	//用户ID
	rechargeOrder.setUserId(currentUser.getId());
	//快捷ID
	rechargeOrder.setChargePayId(chargePay.getId());
	//用户名称
	rechargeOrder.setUserName(currentUser.getUserName());
	//申请金额
	rechargeOrder.setApplyValue(new BigDecimal(orderMoneyYuan));
	//订单创建时间
	rechargeOrder.setCreatedDate(new Date());
	//充值类型
	rechargeOrder.setPayType(payType);
	//第三方充值类型
    rechargeOrder.setThirdPayType(EFundThirdPayType.YINHEPAY.name());
	//操作描述
	rechargeOrder.setOperateDes(EFundPayType.valueOf(payType).getDescription());
	//流水号
	rechargeOrder.setSerialNumber(orderId);
	rechargeOrder.setPostscript("");
	rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT.name());
	rechargeOrder.setBankName("");
	rechargeOrder.setSubbranchName("");
	rechargeOrder.setBankAccountName("");
	rechargeOrder.setBankCardNum("");
	rechargeOrder.setBankType("");
	rechargeOrder.setRefType(EFundRefType.THIRDPAY.name());
	rechargeOrder.setBizSystem(currentUser.getBizSystem());
	rechargeOrder.setFromType(currentUser.getLoginType());
	rechargeWithDrawOrderService.insertSelective(rechargeOrder);
	//log.info("实际银河支付网关地址["+Address +"],交易报文: "+jianZhengBaoPay);
	//银河支付请求对象
	String codeUrl = "";
	request.setAttribute("return_type", yinHePay.getReturn_type());
    if(yinHePay.getReturn_type().equals("json")){
    Map<String, String> mapContent = new HashMap<String, String>();
    mapContent.put("return_type", yinHePay.getReturn_type());
    mapContent.put("api_code", yinHePay.getApi_code());
    mapContent.put("is_type", yinHePay.getIs_type());
    mapContent.put("price", yinHePay.getPrice());
    mapContent.put("order_id", yinHePay.getOrder_id());
    mapContent.put("time", yinHePay.getTime());
    mapContent.put("mark", yinHePay.getMark());
    mapContent.put("return_url", yinHePay.getReturn_url());
    mapContent.put("notify_url", yinHePay.getNotify_url());
    mapContent.put("sign", yinHePay.getSign());
    log.info(mapContent.toString());

    String returnStr = HttpClientUtil.doPost(Address, mapContent);
    
    
    			YinHeParameter contextReturn = JSONUtils.toBean(JSONObject.fromObject(returnStr), YinHeParameter.class);
		log.info("接收的报文               ：" + returnStr);
		
		if (contextReturn.getMessages().getReturncode().equals("SUCCESS")) {
			String url = contextReturn.getPayurl().replace("\\", "");
			codeUrl = url;
			request.setAttribute("payType", payType);
			
		} else {
			out.print(contextReturn.getMessages().getReturncode());
			return;
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta id="viewport" name="viewport"
	content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
<title>充值接口-提交信息处理</title>
<style>
body {
	text-align: center;
}

img {
	margin-top: 60px;
}

p {
	font-size: 36px;
}
</style>
</head>


<c:if test="${return_type=='json'}">
<body>
	<img id="weixinImg" src='<%=path%>/tools/getQrCodePic?code=<%=codeUrl%>'></img>
	<p>
		请使用<span id="pay-way">
		    <c:if test="${payType == 'ALIPAY' }">支付宝</c:if>
			<c:if test="${payType == 'WEIXIN' }">微信</c:if>
			<c:if test="${payType == 'JDQB' }">京东钱包</c:if>
			<c:if test="${payType == 'QQPAY' }">QQ钱包</c:if>
			<c:if test="${payType == 'UNIONPAY' }">银联二维码</c:if>
			</span>扫描二维码以完成支付
	</p>
	</body>
</c:if>
<c:if test="${return_type=='html'}">
<body onload="pay.submit()">
<form method="post" name="pay" id="pay" action="<%=Address%>">
<TABLE>
<TR>
	<TD>
	<input name='return_type' type='hidden' value= "<%=yinHePay.return_type%>"/>
	<input name='api_code' type='hidden' value= "<%=yinHePay.api_code%>"/>
	<input name='is_type' type='hidden' value= "<%=yinHePay.is_type%>"/>
	<input name='price' type='hidden' value= "<%=yinHePay.price%>"/>
	<input name='order_id' type='hidden' value= "<%=yinHePay.order_id%>"/>
	<input name='time' type='hidden' value= "<%=yinHePay.time%>"/>
	<input name='mark' type='hidden' value= "<%=yinHePay.mark%>"/>
	<input name='return_url' type='hidden' value= "<%=yinHePay.return_url%>"/>
	<input name='notify_url' type='hidden' value= "<%=yinHePay.notify_url%>"/>
	<input name='sign' type='hidden' value= "<%=yinHePay.sign%>"/>
	</TD>
</TR>
</TABLE>
</form>
</body>	
</c:if>
</html>