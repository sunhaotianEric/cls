<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.vo.RechargeWithDrawOrder"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@page import="java.io.*"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="net.sf.json.JSONObject"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.extvo.returnModel.YinHePayReturnParam"%>
<%@ page import="com.team.lottery.pay.util.RSA"%>
<%@page import="com.team.lottery.extvo.returnModel.BeiFuBaoParameterHead"%>
<%@page import="com.team.lottery.vo.ChargePay,com.team.lottery.extvo.ChargePayVo,com.team.lottery.service.ChargePayService"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
    //获取当前的状态
	BufferedReader streamReader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
	StringBuilder responseStrBuilder = new StringBuilder();
	String inputStr;
	while ((inputStr = streamReader.readLine()) != null) {
		responseStrBuilder.append(inputStr);
	}
	String resonseStr = responseStrBuilder.toString();
	log.info("银河支付回调通知报文: " + resonseStr);
	/* String order_id = jsonObject.getString("order_id");//是您在发起支付接口传入的您的自定义订单号
    String paysapi_id = jsonObject.getString("paysapi_id"); //是此订单在Api服务器上的唯一编号
    String price = jsonObject.getString("price"); //支付金额
    String real_price = jsonObject.getString("real_price"); //实际支付金额
    String mark = jsonObject.getString("mark"); //描述
    String sign = jsonObject.getString("sign"); //签名认证串
	
	String code = jsonObject.getString("code"); //订单状态 0 未处理 1 交易成功 2 支付失败 3 关闭交易 4 支付超时
    String is_type = jsonObject.getString("is_type") */
	YinHePayReturnParam yinHePayReturnParam = JSONUtils.toBean(JSONObject.fromObject(resonseStr), YinHePayReturnParam.class);
	String order_id=yinHePayReturnParam.getOrder_id();
	String paysapi_id=yinHePayReturnParam.getPaysapi_id();
	String price=yinHePayReturnParam.getPrice();
	String real_price=yinHePayReturnParam.getReal_price();
	String mark=yinHePayReturnParam.getMark();
	String sign=yinHePayReturnParam.getSign();
	String code=yinHePayReturnParam.getCode();
	String is_type=yinHePayReturnParam.getIs_type();
	log.info("银河支付成功通知平台处理信息如下:订单号:" + order_id + "订单金额:" + price + " 元,支付方式:" + is_type);

	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService) ApplicationContextUtil.getBean("rechargeOrderService");
	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(order_id);
	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	Long chargePayId=null;
	if(rechargeWithDrawOrders != null && rechargeWithDrawOrders.size()>0){
	   chargePayId=rechargeWithDrawOrders.get(0).getChargePayId();
	}
	ChargePay chargePay = chargePayService.selectByPrimaryKey(chargePayId);
	String api_code="";
	if(chargePay!=null){
		api_code=chargePay.getMemberId();
	}
	if (rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1) {
		log.error("根据订单号[" + order_id + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知银河付支付报文接收成功
		response.getOutputStream().write("SUCCESS".getBytes("UTF-8"));
	}
	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
	if (rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + order_id + "]查找充值订单记录为空");
		//通知银河支付报文接收成功
		response.getOutputStream().write("SUCCESS".getBytes("UTF-8"));
	}
	
	//计算签名
		String Md5key = rechargeWithDrawOrder.getSign();
		String MARK = "&";
		//需要加密的字符串
		String MD5Str = "api_code=" + api_code + MARK + "code=" + code + MARK + "is_type=" + is_type +MARK+ "mark=" + mark+MARK+ "order_id=" + order_id +MARK+ "paysapi_id=" + paysapi_id +MARK
				+ "price=" + price +MARK+ "real_price=" + real_price + MARK + "key=" + Md5key;
		log.info("当前MD5源码:" + MD5Str);
		//Md5加密串并且大写
		String WaitSign = Md5Util.getMD5ofStr(MD5Str).toUpperCase();
		log.info("加密后的MD5: " + WaitSign);
	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
		  if(WaitSign.compareTo(sign)==0){
			boolean dealResult = false;
			if(!StringUtils.isEmpty(code) && code.equals("1")){
				BigDecimal factMoneyValue = new BigDecimal(price);
				try {
					dealResult = rechargeWithDrawOrderService.rechargeOrderSuccessDeal(order_id,factMoneyValue);
				}catch(UnEqualVersionException e) {
			   		log.error("银河支付在线充值时发现版本号不一致,订单号["+order_id+"]");
			   		//启动新进程进行多次重试
			   		UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION
			   				, order_id,factMoneyValue,"");
			   		thread.start();
				} catch(Exception e){
					//异常不进行重试
					log.error(e.getMessage());
				}	
			} else {
				if(!code.equals("1")){
					log.info("银河支付处理错误支付结果："+code);
				}
			}
			log.info("银河支付订单处理入账结果:{}", dealResult);
			//通知银河支付报文接收成功
		    out.println("SUCCESS");
			return;
		 }else{
			log.info("MD5校验失败,报文返回MD5签名内容["+ sign +"],计算MD5签名内容[" + WaitSign + "]");
			//让第三方继续补发
		    out.println("ERROR");
			return;
		}
%>