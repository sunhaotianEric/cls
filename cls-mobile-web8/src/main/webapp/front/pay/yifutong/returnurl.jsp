<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.vo.RechargeWithDrawOrder"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%  
    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
    String memberid=request.getParameter("memberid");
    String orderid=request.getParameter("orderid");
    String amount=request.getParameter("amount");
    String datetime=request.getParameter("datetime");
    String returncode=request.getParameter("returncode");
    String transaction_id = request.getParameter("transaction_id");
    String attach=request.getParameter("attach");
    String sign=request.getParameter("sign");
	log.info("易付通支付成功通知平台处理信息如下: 商户ID: "+memberid+" 商户订单号: "+orderid+" 订单金额: "+amount+" 交易时间 "+datetime+" MD5签名: "+sign);
	RechargeOrderService rechargeOrderService = (RechargeOrderService) ApplicationContextUtil.getBean("rechargeOrderService"); 
	List<RechargeOrder> rechargeOrders = rechargeOrderService.getRechargeOrderBySerialNumber(orderid);
	
	if(rechargeOrders == null || rechargeOrders.size() != 1){
		log.error("根据订单号[" + orderid + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知易付通支付报文接收成功
	    out.print("OK");
		return;
	}
	RechargeOrder rechargeOrder = rechargeOrders.get(0);
	if(rechargeOrder == null) {
		log.error("根据订单号[" + orderid + "]查找充值订单记录为空");
		//通知易付通支付报文接收成功
	    out.print("OK");
		return;
	}
	
	//签名
	String Md5key = rechargeOrder.getSign();
	String SignTemp="amount="+amount+"&datetime="+datetime+"&memberid="+memberid+"&orderid="+orderid+"&returncode="+returncode+"&transaction_id="+transaction_id+"&key="+Md5key+"";
	//Md5加密串
	String WaitSign = Md5Util.getMD5ofStr(SignTemp).toUpperCase();
	log.info("易付通入账处理需要加密的字符串:[" + SignTemp + "]加密后的字符串为:[" + WaitSign + "]");

	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if(WaitSign.compareTo(sign)==0){
		boolean dealResult = false;
		if(!StringUtils.isEmpty(returncode) && returncode.equals("00")){
			BigDecimal factMoneyValue = new BigDecimal(amount);
			try {
				dealResult = rechargeOrderService.rechargeOrderSuccessDeal(orderid,factMoneyValue);
			}catch(UnEqualVersionException e) {
		   		log.error("易付通支付在线充值时发现版本号不一致,订单号["+orderid+"]");
		   		//启动新进程进行多次重试
		   		UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION
		   				, orderid,factMoneyValue,"");
		   		thread.start();
			} catch(Exception e){
				//异常不进行重试
				log.error(e.getMessage());
			}	
		} else {
			if(!returncode.equals("00")){
				log.info("易付通支付处理错误支付结果："+returncode);
			}
		}
		log.info("易付通支付订单处理入账结果:{}", dealResult);
		//通知易付通支付报文接收成功
	    out.print("OK");
		return;
	}else{
		log.info("MD5校验失败,报文返回MD5签名内容["+ sign +"],计算MD5签名内容[" + WaitSign + "]");
		//让第三方继续补发
	    out.print("error");
		return;
	}
%>