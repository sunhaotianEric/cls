<%@page import="com.team.lottery.pay.model.jindouyun.JinDouYunPay"%>
<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.team.lottery.service.QuickBankTypeService"%>
<%@page import="com.team.lottery.util.BaseDwrUtil"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.vo.RechargeWithDrawOrder"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundThirdPayType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.team.lottery.vo.QuickBankType"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="java.net.URLEncoder"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.team.lottery.service.RechargeConfigService"%>
<%@page import="com.team.lottery.vo.RechargeConfig"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
	String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	String serialNumber = request.getParameter("serialNumber"); // 获取配置ID;
	String orderMoneyYuan = OrderMoney;

	User currentUser = (User) request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	if (currentUser == null) {
		response.sendRedirect(path + "/gameBet/login.html");
	}
	Date currTime = new Date();
    //时间以yyyy-MM-dd HH:mm:ss的方式表示
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.CHINA);
    //时间以yyyyMMDDHHmmss的方式表示 
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss",Locale.CHINA);
    String webdate=new String(formatter1.format(currTime));
    String TradeDate=new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));
	
	log.info("业务系统[{}],用户名[{}],发起了筋斗云支付，生成订单号[{}],支付金额[{}],PayId[{}],payType[{}],chargePayId[{}]",
  			currentUser.getBizSystem(), currentUser.getUserName(), serialNumber, OrderMoney, payId, payType, chargePayId);
	String payUrl = chargePay.getPayUrl();
	String Address = chargePay.getAddress();
	log.info("payUrl: "+payUrl+" Address: "+Address);
	
	//同步跳转地址
	String notifyUrl = "";
	if (chargePay.getNotifyUrl().startsWith("http://") || chargePay.getNotifyUrl().startsWith("https://")) {
		notifyUrl = chargePay.getNotifyUrl();
	} else {
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
		notifyUrl = basePath + chargePay.getNotifyUrl();
	}
	
	// 筋斗云支付对象.
	JinDouYunPay jinDouYunPay = new JinDouYunPay();
	// 设置商户号.
	jinDouYunPay.setMerchantCode(chargePay.getMemberId());
	// 设置商户订单号.
	jinDouYunPay.setMerchantOrderNo(serialNumber);
	// 设置随机数.
	jinDouYunPay.setNonceStr(String.valueOf(currTime.getTime()));
	// 设置回调地址.
	jinDouYunPay.setNotifyUrl(chargePay.getReturnUrl());
	// 设置支付金额.
	jinDouYunPay.setOrderAmount(OrderMoney);
	// 设置产品代码.(ALIPAY_PERSON)固定.
	jinDouYunPay.setProductCode(payId);
	// 设置标题,默认使用当前用户的用户名.
	jinDouYunPay.setSubject(currentUser.getUserName());
	
	
	
	//sign加密;
	String mark = "&";
	//加密原始串.
	String md5SignStr =  "merchantCode=" + jinDouYunPay.getMerchantCode() + mark + 
						 "merchantOrderNo=" + jinDouYunPay.getMerchantOrderNo() + mark + 
						 "nonceStr=" + jinDouYunPay.getNonceStr() + mark + 
						 "notifyUrl=" + jinDouYunPay.getNotifyUrl() + mark + 
						 "orderAmount=" + jinDouYunPay.getOrderAmount() + mark + 
						 "productCode=" + jinDouYunPay.getProductCode() + mark + 
						 "subject=" + jinDouYunPay.getSubject() + mark + 
						 "key=" + chargePay.getSign();

	//进行MD5大写加密
	String md5Sign = Md5Util.getMD5ofStr(md5SignStr).toUpperCase();
	//打印日志
	log.info("md5原始串: " + md5SignStr + "生成后的交易签名:" + md5Sign);
	//设置签名
	jinDouYunPay.setSign(md5Sign);

	//通过JSON格式去提交数据,以便获取第三方跳转地址
	Map<String, String> postDataMap = new TreeMap<String, String>();
	postDataMap.put("merchantCode", jinDouYunPay.getMerchantCode());
	postDataMap.put("merchantOrderNo", jinDouYunPay.getMerchantOrderNo());
	postDataMap.put("nonceStr", jinDouYunPay.getNonceStr());
	postDataMap.put("notifyUrl", jinDouYunPay.getNotifyUrl());
	postDataMap.put("orderAmount", jinDouYunPay.getOrderAmount());
	postDataMap.put("productCode", jinDouYunPay.getProductCode());
	postDataMap.put("subject", jinDouYunPay.getSubject());
	postDataMap.put("sign", jinDouYunPay.getSign());
	//获取第三方给我们返回的JSON数据做判断跳转
	String result = HttpClientUtil.doPost(Address, postDataMap);
	log.info("筋斗云支付提交支付报文，返回内容:{}", result);
	Boolean isJson = JSONUtils.isJson(result);
	//最终我们要生成二维码的数据
	String payInfoNew = "";
	String basePath = "";
	if (isJson == true) {
		JSONObject jsonObject = JSONUtils.toJSONObject(result);
		if (StringUtils.isBlank(result)) {
			out.print("发起支付失败");
			return;
		}
		//获取返回的状态.1为发起成功,-1为发起失败.
		String status = jsonObject.getString("retCode");
		if (status.equals("0000")) {
			//获取二维码支付路径.
			String qRCodePath = jsonObject.getString("urlData");
			basePath = path + "/" + "tools/getQrCodePic?code=" + URLEncoder.encode(qRCodePath, "utf-8");
			request.setAttribute("payType", payType);
		} else {
			out.print(result);
			return;
		}
		log.info("筋斗云交易报文: " + jinDouYunPay);
	} else {
		out.print(result);
		log.error("筋斗云支付发起失败: " + result);
		return;
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
<style>
body {
	text-align: center;
}

#weixinImg {
	margin-top: 150px;
	width: 500px;
}

p {
	font-size: 48px;
}
</style>
</head>

<c:if test="${basePath!=''}">
	<body>
		<img id="weixinImg" src='<%=basePath%>'></img>
		<%-- <img src="http://qr.topscan.com/api.php?bg=f3f3f3&fg=ff0000&gc=222222&el=l&w=684&m=30&text=<%=code%>"/> --%>
		<p>
			请使用手机截图进行截图二维码,并且使用<span id="pay-way"><c:if test="${payType == 'ALIPAY' }">支付宝</c:if>
				<c:if test="${payType == 'WEIXIN' }">微信</c:if> <c:if
					test="${payType == 'QQPAY' }">QQ钱包</c:if> <c:if
					test="${payType == 'JDPAY' }">京东扫码</c:if> <c:if
					test="${payType == 'UNIONPAY' }">银联扫码</c:if></span>进行相册识别扫码!
		</p>
	</body>
</c:if>
</html>
