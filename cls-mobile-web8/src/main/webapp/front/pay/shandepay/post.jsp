<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.team.lottery.util.BaseDwrUtil"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.vo.RechargeWithDrawOrder"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundThirdPayType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.pay.model.shandepay.ShanDePayBase"%>
<%@page import="com.team.lottery.pay.model.shandepay.ShanDePay"%>
<%@page import="com.team.lottery.pay.model.shandepay.ShanDePayQr"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@page import="com.team.lottery.util.DateUtil"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@page import="com.team.lottery.pay.util.shandeutil.CertUtil"%>
<%@page import="com.team.lottery.pay.util.shandeutil.CryptoUtil"%>
<%@page import="com.team.lottery.pay.util.shandeutil.SandPayUtil"%>
<%@page import="com.alibaba.fastjson.JSONObject"%>
<%@page import="com.alibaba.fastjson.JSON"%>
<%@page import="org.apache.commons.codec.binary.Base64"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@page import="com.team.lottery.vo.RechargeConfig"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.team.lottery.service.RechargeConfigService"%>
<jsp:useBean id='md5Util' scope='request'
	class='com.team.lottery.pay.util.Md5Util' />
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
	String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	String serialNumber = request.getParameter("serialNumber");//充值类型
	String orderMoneyYuan = OrderMoney;

	ChargePayService chargePayService = (ChargePayService) ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService) ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.getPayMemmberMsg(Long.valueOf(chargePayId));
	User currentUser = (User) request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);

	// 获取项目中公钥秘钥位置.
	String publicKeyPath = "classpath:" + chargePay.getTerminalId() + ".cer";
	String privateKeyPath = "classpath:" + chargePay.getTerminalId() + ".pfx";
	String keyPassword = "123456";
	// 初始化证书.
	try {
		log.info("初始化证书开始...");
		CertUtil.init(publicKeyPath, privateKeyPath, keyPassword);
		log.info("初始化证书完成...");
	} catch (Exception e) {
		log.info("初始化证书失败...");
		out.print(e.toString());
		e.printStackTrace();
	}
	// 获取杉德快捷支付地址.
	String payUrl = chargePay.getPayUrl();
	if (currentUser == null) {
		response.sendRedirect(path + "/gameBet/login.html");
	}

	log.info("业务系统[{}],用户名[{}],发起了杉德支付，生成订单号[{}],支付金额[{}],PayId[{}],payType[{}],chargePayId[{}]", currentUser.getBizSystem(), currentUser.getUserName(), serialNumber,
			OrderMoney, payId, payType, chargePayId);

	if (!"".equals(OrderMoney)) {
		BigDecimal a;
		a = new BigDecimal(OrderMoney); //使用分进行提交
		OrderMoney = String.valueOf(a.multiply(new BigDecimal(100)).intValue());
	}
	String sandAmountMoney = SandPayUtil.getSandAmountMoney(OrderMoney);
	// 杉德公共请求参数.

	// 组织支付HEAD域
	ShanDePayBase shanDePayBase = new ShanDePayBase();
	// 获取接口版本号,默认为1.0
	shanDePayBase.setVersion(chargePay.getInterfaceVersion());
	// 需要测试的流程.(方法,跟请求接口地址不一样.)
	shanDePayBase.setMethod("sandpay.trade.precreate"); // 需要测试的流程
	// 产品编号.(00000016是一键快捷,00000018后台绑卡快捷)
	shanDePayBase.setProductId("00000012"); // 测试外部产品号
	// 接入的商户类型(1是普通商户平台,2是平台商户平台)
	shanDePayBase.setAccessType("1");
	// 商户号(商户ID)
	shanDePayBase.setMid(chargePay.getMemberId()); // 测试商户号
	// 接入端口(PC端还是mobile端7是PC端,8是mobile端.)
	shanDePayBase.setChannelType("07");
	// 请求时间
	shanDePayBase.setReqTime(DateUtil.getDate(new Date(), "yyyyMMddHHmmss"));

	/* ShanDePay shanDePay = new ShanDePay();
	shanDePay.setUserId(String.valueOf(currentUser.getId()));
	shanDePay.setOrderCode(serialNumber);
	shanDePay.setOrderTime(DateUtil.getDate(new Date(), "yyyyMMddHHmmss"));
	shanDePay.setCurrencyCode("156");
	shanDePay.setTotalAmount(sandAmountMoney);
	shanDePay.setSubject(currentUser.getUserName());
	shanDePay.setBody(currentUser.getUserName());
	shanDePay.setNotifyUrl(chargePay.getReturnUrl());
	shanDePay.setFrontUrl(chargePay.getNotifyUrl()); */
	ShanDePayQr shanDePayQr = new ShanDePayQr();
	shanDePayQr.setNotifyUrl(chargePay.getReturnUrl());
	shanDePayQr.setOrderCode(serialNumber);
	shanDePayQr.setPayTool("0403");
	shanDePayQr.setSubject(currentUser.getUserName());
	shanDePayQr.setTotalAmount(sandAmountMoney);
	log.info("杉德正在发起快捷支付...");
	JSONObject header = new JSONObject();
	header.put("version", shanDePayBase.getVersion());
	header.put("method", shanDePayBase.getMethod());
	header.put("productId", shanDePayBase.getProductId());
	header.put("accessType", shanDePayBase.getAccessType());
	header.put("mid", shanDePayBase.getMid());
	header.put("channelType", shanDePayBase.getChannelType());
	header.put("reqTime", shanDePayBase.getReqTime());
	JSONObject body = new JSONObject();
	body.put("notifyUrl", shanDePayQr.getNotifyUrl());
	body.put("orderCode", shanDePayQr.getOrderCode());
	body.put("payTool", shanDePayQr.getPayTool());
	body.put("subject", shanDePayQr.getSubject());
	body.put("totalAmount", shanDePayQr.getTotalAmount());

	// 初始请求参数Data格式.
	JSONObject data = SandPayUtil.getSandData(shanDePayBase, shanDePayQr);
	// 初始化Sign参数格式.
	String sign = SandPayUtil.getSandSign(data);
	log.info("支付地址为:" + payUrl);
	JSONObject json = SandPayUtil.requestServer(header, body, payUrl);
	String returnBody = json.getString("body");
	JSONObject parseObject = JSONObject.parseObject(returnBody);
	String qrCode = parseObject.getString("qrCode");
	String qrCodeUrl = URLEncoder.encode(qrCode, "UTF-8");
	log.info("杉德扫码返回结果:" + json.toJSONString());
	log.info("杉德银联扫码生成的二维码地址qrCodeUrl为:" + qrCodeUrl);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
<head>
<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport"
			content="width=device-width,initial-scale=1.0,maximum-scale=1.0, user-scalable=0">
			<title>Title</title>
			<style>
body, p {
	margin: 0;
	padding: 0;
}

body {
	background-color: rgb(245, 245, 245);
}

img {
	width: 100%;
	display: block;
	vertical-align: middle;
}

.ro_body {
	display: flex;
	flex-flow: column;
	align-items: center;
	margin-top: 10%;
	font-size: 40px;
}

.ro_body_img {
	display: flex;
	flex-flow: column;
	align-items: center;
	width: 50%;
	box-shadow: 0 0 20px #b4b4b4;
	border-radius: 5px;
	padding: 6%;
	background-color: white;
}

.ro_body_img p {
	padding-top: 10px;
}

.ro_body_img div {
	padding: 0 10px;
}

.ro_body_bank {
	color: rgb(243, 178, 91);
}

.icon {
	color: #c7c5c5;
}

.ro_body_bankList {
	margin-top: 15%;
	display: flex;
	flex-flow: column;
	align-items: center;
}

.ro_body_bank {
	padding: 0 5px;
}

.ro_body_buttom {
	color: gray;
}
</style>
</head>
</head>
<body>
	<div class="ro_body">
		<div class="ro_body_img">
			<div>
				<img src="<%=path%>/tools/getQrCodePic?code=<%=qrCodeUrl%>">
			</div>
			<p>长按保存二维码</p>
		</div>
		<div class="ro_body_msg ro_body_bankList">
			<p>
				<span class="icon">—</span>请使用(<span class="ro_body_bank">银联APP,银行APP,云闪付APP</span>)<span
					class="icon">—</span>
			</p>
			<p>扫描二维码完成支付</p>
		</div>
		<div class="ro_body_msg ro_body_bankList">
			<p class="ro_body_buttom">支持中国各大银行APP扫码</p>
		</div>
	</div>
</body>
</html>
