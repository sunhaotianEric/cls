<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@page import="com.team.lottery.util.BaseDwrUtil"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.vo.RechargeWithDrawOrder"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundThirdPayType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@page import="javax.net.ssl.*"%>
<%@page import="javax.net.ssl.X509TrustManager"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.pay.model.ShunFuPay"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
 <%@page errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URL"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="com.team.lottery.extvo.returnModel.ShunFuReturnPayUrl"%>
<%@page import="java.net.InetAddress"%>
<%@page import="com.team.lottery.pay.util.PayDemo"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%  
    InetAddress addr = InetAddress.getLocalHost();
    String ip=addr.getHostAddress().toString();//获得本机IP
    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额 
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
    String orderMoneyYuan = OrderMoney;
	
	
    
    User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
	
	Date currTime = new Date();
    //时间以yyyy-MM-dd HH:mm:ss的方式表示
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.CHINA);
    //时间以yyyyMMDDHHmmss的方式表示 
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss",Locale.CHINA);
    String webdate=new String(formatter1.format(currTime));
    String TradeDate=new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));
	String  orderId=MakeOrderNum.makeOrderNum("CZ");
	log.info("用户名称: "+currentUser.getUserName()+" 用户 ID号: "+currentUser.getId()+" 生成订单号: "+orderId+" 订单金额: "+OrderMoney);
	
	
	 if (!"".equals(OrderMoney)) {	
		 BigDecimal a;
		a = new BigDecimal(OrderMoney); //使用分进行提交
		OrderMoney=String.valueOf(a.multiply(new BigDecimal(100)).intValue());
	}
	else{
		OrderMoney = ("0");
	} 
	String payUrl = chargePay.getPayUrl();//转发支付网关地址
	String Address = chargePay.getAddress(); //最终瞬付地址
	log.info("payUrl: "+payUrl+" Address: "+Address);
	
	//瞬付支付请求对象
	ShunFuPay shunFuPay =new ShunFuPay();
	shunFuPay.setAmount(OrderMoney);
	shunFuPay.setCallBackUrl(chargePay.getReturnUrl());
	shunFuPay.setCallBackViewUrl(chargePay.getNotifyUrl());
	shunFuPay.setClientIP(ip);
	shunFuPay.setGoodsInfo("goodsInfo");
	shunFuPay.setMerNo(chargePay.getMemberId());
	shunFuPay.setOrderNo(orderId);
	
	shunFuPay.setRandom("Yzs0");
	if(payType.equals("WEIXIN")){
		payId="WX";
	}else if(payType.equals("ALIPAY")){
		payId="ZFB";
	}else if(payType.equals("QQPAY")){
		payId="QQ";
	}else if(payType.equals("UNIONPAY")){
		payId="YL";
	}else if(payType.equals("JDPAY")){
		payId="JDQB_WAP";
	}else if(payType.equals("WEIXINWAP")){
		payId="WX_WAP";
	}else if(payType.equals("ALIPAYWAP")){
		payId="ZFB_WAP";
	}else if(payType.equals("QQPAYWAP")){
		payId="QQ_WAP";
	}else {
		payId="KJ_WAP";
	}
	shunFuPay.setPayNetway(payId);
	
	/* xinMa.setNonce_str(paramUtil.CreateNoncestr(32)); */
	//生成md5密钥签名
    String Md5key = chargePay.getSign();///////////md5密钥（KEY）
    Map<String, String> metaSignMap = new TreeMap<String, String>();
	metaSignMap.put("amount",shunFuPay.getAmount()); 
	metaSignMap.put("callBackUrl",shunFuPay.getCallBackUrl());
	metaSignMap.put("callBackViewUrl",shunFuPay.getCallBackViewUrl());
	metaSignMap.put("clientIP",shunFuPay.getClientIP());
	metaSignMap.put("goodsInfo",shunFuPay.getGoodsInfo());
	metaSignMap.put("merNo",shunFuPay.getMerNo());
	metaSignMap.put("orderNo",shunFuPay.getOrderNo());
	metaSignMap.put("payNetway",shunFuPay.getPayNetway());
	metaSignMap.put("random",shunFuPay.getRandom());
	JSONObject jsonObject=new JSONObject();
	jsonObject.putAll(metaSignMap);
	String jsonObjectStr =JSONUtils.toJSONString(metaSignMap);
	
	String md5 =new String(jsonObjectStr+chargePay.getSign());//MD5签名格式
	String Signature = md5Util.getMD5ofStr(md5).toUpperCase();//计算MD5值
	
    //交易签名
    shunFuPay.setSign(Signature);
    metaSignMap.put("sign", Signature);
    jsonObject.putAll(metaSignMap);
    jsonObjectStr=jsonObject.toString();
    log.info("md5原始串: "+md5 + "生成后的交易签名:" + Signature);
    //冲值订单
	RechargeOrder rechargeOrder = new RechargeOrder();
	//用户ID
	rechargeOrder.setUserId(currentUser.getId());
	//快捷ID
	rechargeOrder.setChargePayId(chargePay.getId());
	//用户名称
	rechargeOrder.setUserName(currentUser.getUserName());
	//申请金额
	rechargeOrder.setApplyValue(new BigDecimal(orderMoneyYuan));
	//订单创建时间
	rechargeOrder.setCreatedDate(new Date());
	//充值类型
	rechargeOrder.setPayType(payType);
	//第三方充值类型
    rechargeOrder.setThirdPayType(EFundThirdPayType.SHUNFU.name());
	//操作描述
	rechargeOrder.setOperateDes(EFundPayType.valueOf(payType).getDescription());
	//流水号
	rechargeOrder.setSerialNumber(orderId);
	rechargeOrder.setPostscript("");
	rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT.name());
	rechargeOrder.setBankName("");
	rechargeOrder.setSubbranchName("");
	rechargeOrder.setBankAccountName("");
	rechargeOrder.setBankCardNum("");
	rechargeOrder.setBankType("");
	rechargeOrder.setRefType(EFundRefType.THIRDPAY.name());
	rechargeOrder.setBizSystem(currentUser.getBizSystem());
	rechargeOrder.setFromType(currentUser.getLoginType());
	rechargeWithDrawOrderService.insertSelective(rechargeOrder);
	log.info("实际瞬付支付网关地址["+Address +"],交易报文: "+shunFuPay);
	
	/* Map<String, String> req = new TreeMap<String, String>();
	req.put("data",jsonObjectStr); */
	/* String reqStr = JSONUtils.toJSONString(req); */
	/* String req="data=" + jsonObjectStr;
	System.out.println(req);*/
	String codeUrl=null;
	ShunFuReturnPayUrl shunFuReturnPayUrl=PayDemo.pay(request,response,shunFuPay, chargePay.getSign(),Address);
	if(!shunFuReturnPayUrl.getResultCode().equals("00")){
		out.println(shunFuReturnPayUrl.getResultMsg());
		return;
	}else{
		 codeUrl=shunFuReturnPayUrl.getQrcodeInfo();
		
	}
 %>
 
 


 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>

<body onload="pay.submit()">
<form method="post" name="pay" id="pay" action="<%=payUrl%>">
<TABLE>
<TR>
	<TD>
	<input name='codeUrl' type='hidden' value= "<%=codeUrl%>"/>
	<input name='payType' type='hidden' value= "<%=payType%>"/>
	
	</TD>
</TR>
</TABLE>
	
</form>	

</body>
</html>
