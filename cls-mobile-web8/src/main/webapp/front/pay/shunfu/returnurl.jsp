<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" trimDirectiveWhitespaces="true"%>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.vo.RechargeWithDrawOrder"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.extvo.returnModel.XinMaSendParameter"%>
<%@page import="com.team.lottery.extvo.returnModel.ShunFuReturnParameter"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="java.io.*"%>
<%@page import="net.sf.json.JSONObject"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.MD5s'/>
<%@page import="com.team.lottery.pay.util.PayDemo"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%  
    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
   
    //log.info("接收瞬付回调通知报文: "+responseStrBuilder.toString());
    try{
    	/* JSONObject jsonObject =PayDemo.result(request, response);
        if(jsonObject==null){
        	log.info("瞬付签名校验失败!");
        	return;
        } */
    	String data = request.getParameter("data");
        log.info("收到瞬付支付报文: "+data);
		JSONObject jsonObj = JSONObject.fromObject(data);
		ShunFuReturnParameter shunFuReturnParameter=JSONUtils.toBean(jsonObj, ShunFuReturnParameter.class);
		log.info("收到返回报文对象: "+shunFuReturnParameter);
    	String merNo = shunFuReturnParameter.getMerNo();//商户号
    	String payNetway = shunFuReturnParameter.getPayNetway();//支付网关
    	String orderNo = shunFuReturnParameter.getOrderNo();//订单号
    	String amount = shunFuReturnParameter.getAmount();//金额
    	String resultCode = shunFuReturnParameter.getResultCode();//支付状态
    	String payDate = shunFuReturnParameter.getPayDate();//支付时间
    	String signStr = shunFuReturnParameter.getSign(); ///签名
    	String goodsName = shunFuReturnParameter.getGoodsName();//商品名称 
		
		
		
		
		Map<String, String> metaSignMap = new TreeMap<String, String>();
		metaSignMap.put("merNo", shunFuReturnParameter.getMerNo());
		metaSignMap.put("payNetway", shunFuReturnParameter.getPayNetway());
		metaSignMap.put("orderNo", shunFuReturnParameter.getOrderNo());
		metaSignMap.put("amount", shunFuReturnParameter.getAmount());
		metaSignMap.put("goodsName", shunFuReturnParameter.getGoodsName());
		metaSignMap.put("resultCode", shunFuReturnParameter.getResultCode());// 支付状态
		metaSignMap.put("payDate", shunFuReturnParameter.getPayDate());// yyyy-MM-dd
		
		RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
    	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(shunFuReturnParameter.getOrderNo());
    	if(rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1){
    		log.error("根据订单号[" + shunFuReturnParameter.getOrderNo() + "]查找充值订单记录为空或者有两笔相同订单号订单");
    		//通知瞬付支付报文接收成功
    		response.getOutputStream().write("000000".getBytes("UTF-8"));
    	}
    	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
    	if(rechargeWithDrawOrder == null) {
    		log.error("根据订单号[" + shunFuReturnParameter.getOrderNo() + "]查找充值订单记录为空");
    		//通知瞬付支付报文接收成功
    		response.getOutputStream().write("000000".getBytes("UTF-8"));
    	}															// HH:mm:ss
		String jsonStr = PayDemo.mapToJson(metaSignMap);
		String sign = PayDemo.MD5(jsonStr.toString() + rechargeWithDrawOrder.getSign(), "UTF-8");
		
        /*ShunFuReturnParameter shunFuReturnParameter=JSONUtils.toBean(jsonObject, ShunFuReturnParameter.class);
    	String merNo = shunFuReturnParameter.getMerNo();//商户号
    	String payNetway = shunFuReturnParameter.getPayNetway();//支付网关
    	String orderNo = shunFuReturnParameter.getOrderNo();//订单号
    	String amount = shunFuReturnParameter.getAmount();//金额
    	String resultCode = shunFuReturnParameter.getResultCode();//支付状态
    	String payDate = shunFuReturnParameter.getPayDate();//支付时间
    	String sign = shunFuReturnParameter.getSign();//签名
    	String goodsInfo = shunFuReturnParameter.getGoodsInfo();//商品名称 */
    	/* String attachContent = xinMAReturn.getAttachContent();//备注信息 */
    	log.info("瞬付支付成功通知平台处理信息如下: 商户ID: "+merNo+" 商户订单号: "+orderNo+" 订单结果: "+resultCode+" 订单金额: "+amount+" 支付网关: "+payNetway+"MD5签名: "+sign);
        
     
    	
    	//签名
       

    	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
    	if(sign.compareTo(jsonObj.getString("sign"))==0){
    		boolean dealResult = false;
    		if(!StringUtils.isEmpty(resultCode) && resultCode.equals("00")){
    			BigDecimal factMoneyValue = new BigDecimal(amount).divide(new BigDecimal(100));
    			try {
    				dealResult = rechargeWithDrawOrderService.rechargeOrderSuccessDeal(orderNo,factMoneyValue);
    			}catch(UnEqualVersionException e) {
    		   		log.error("瞬付支付在线充值时发现版本号不一致,订单号["+orderNo+"]");
    		   		//启动新进程进行多次重试
    		   		UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION
    		   				, orderNo,factMoneyValue,"");
    		   		thread.start();
    			} catch(Exception e){
    				//异常不进行重试
    				log.error(e.getMessage());
    			}	
    		} else {
    			if(!resultCode.equals("00")){
    				log.info("瞬付支付处理错误支付结果："+resultCode);
    				return;
    			}
    		}
    		log.info("瞬付支付订单处理入账结果:{}", dealResult);
    		 //通知瞬付支付报文接收成功
    	    /* response.getOutputStream().write("000000".getBytes()); */
    	    response.getOutputStream().write("000000".getBytes("UTF-8"));
    		
    	}else{
    		log.info("MD5校验失败,报文返回MD5签名内容["+ jsonObj.getString("sign") +"],计算MD5签名内容[" + sign + "]");
    		//让第三方继续补发
    		response.getOutputStream().write("000000".getBytes("UTF-8"));
    	}
    	
    }catch(Exception e){
    	log.error(e.getMessage());
    }
    
%>