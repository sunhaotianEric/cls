<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@page import="com.team.lottery.util.BaseDwrUtil"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.vo.RechargeWithDrawOrder"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundThirdPayType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@page import="javax.net.ssl.*"%>
<%@page import="javax.net.ssl.X509TrustManager"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.pay.model.JinHaoPay"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
 <%@page errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URL"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="com.team.lottery.extvo.returnModel.JinHaoReturnPayUrl"%>
<%@page import=" com.team.lottery.pay.util.MerchantApiUtil"%>
<%@page import=" com.team.lottery.pay.util.SimpleHttpUtils"%>
<%@page import="java.net.InetAddress"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
	InetAddress addr = InetAddress.getLocalHost();
    String ip=addr.getHostAddress().toString();//获得本机IP
    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额 
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
    String orderMoneyYuan = OrderMoney;
	
	
    
    User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
	
	Date currTime = new Date();
    //时间以yyyy-MM-dd HH:mm:ss的方式表示
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.CHINA);
    //时间以yyyyMMDDHHmmss的方式表示 
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss",Locale.CHINA);
    String webdate=new String(formatter1.format(currTime));
    String TradeDate=new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));
	String  orderId=MakeOrderNum.makeOrderNum("CZ");
	log.info("用户名称: "+currentUser.getUserName()+" 用户 ID号: "+currentUser.getId()+" 生成订单号: "+orderId+" 订单金额: "+OrderMoney);
	
	
	 /* if (!"".equals(OrderMoney)) {	
		 BigDecimal a;
		a = new BigDecimal(OrderMoney); //使用分进行提交
		OrderMoney=String.valueOf(a.multiply(new BigDecimal(100)));
	}
	else{
		OrderMoney = ("0");
	} */ 
	String payUrl = chargePay.getPayUrl();//转发支付网关地址
	String Address = chargePay.getAddress(); //最终金好地址
	log.info("payUrl: "+payUrl+" Address: "+Address);
	
	//金好支付请求对象
	JinHaoPay jinHaoPay =new JinHaoPay();
	jinHaoPay.setPayKey(chargePay.getMemberId());
	jinHaoPay.setProductName("支付");
	jinHaoPay.setProductType(payId);
	jinHaoPay.setOrderIp(ip);
	jinHaoPay.setOrderPrice(OrderMoney);
	Date orderTime = new Date();
	String orderTimeStr = new SimpleDateFormat("yyyyMMddHHmmss").format(orderTime);
	jinHaoPay.setOrderTime(orderTimeStr);
	jinHaoPay.setOutTradeNo(orderId);
	jinHaoPay.setNotifyUrl(chargePay.getNotifyUrl());
	jinHaoPay.setReturnUrl(chargePay.getReturnUrl());
	jinHaoPay.setRemark("aa");
	if(payType.equals("QUICK_PAY")){
		jinHaoPay.setBankAccountType("PRIVATE_DEBIT_ACCOUNT");
		jinHaoPay.setBankCode(payId);
	}
	
	/*xinMa.setNonce_str(paramUtil.CreateNoncestr(32));
	//生成md5密钥签名
    String Md5key = chargePay.getSign();///////////md5密钥（KEY）
	String MARK = "&";
    String md5Str="";
   
    
    md5Str="callbackUrl="+juHePay.getCallbackUrl()+MARK+"memberCode="+juHePay.getMemberCode()+MARK+
  
    
	String md5 =new String(md5Str);//MD5签名格式
	String Signature = md5Util.getMD5ofStr(md5);//计算MD5值
    //交易签名
    juHePay.setSignStr(Signature);*/
    //log.info("md5原始串: "+md5 + "生成后的交易签名:" + Signature);
    //冲值订单
	RechargeOrder rechargeOrder = new RechargeOrder();
	//用户ID
	rechargeOrder.setUserId(currentUser.getId());
	//快捷ID
	rechargeOrder.setChargePayId(chargePay.getId());
	//用户名称
	rechargeOrder.setUserName(currentUser.getUserName());
	//申请金额
	rechargeOrder.setApplyValue(new BigDecimal(orderMoneyYuan));
	//订单创建时间
	rechargeOrder.setCreatedDate(new Date());
	//充值类型
	rechargeOrder.setPayType(payType);
	//第三方充值类型
    rechargeOrder.setThirdPayType(EFundThirdPayType.JINHAO.name());
	//操作描述
	rechargeOrder.setOperateDes(EFundPayType.valueOf(payType).getDescription());
	//流水号
	rechargeOrder.setSerialNumber(orderId);
	rechargeOrder.setPostscript("");
	rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT.name());
	rechargeOrder.setBankName("");
	rechargeOrder.setSubbranchName("");
	rechargeOrder.setBankAccountName("");
	rechargeOrder.setBankCardNum("");
	rechargeOrder.setBankType("");
	rechargeOrder.setRefType(EFundRefType.THIRDPAY.name());
	rechargeOrder.setBizSystem(currentUser.getBizSystem());
	rechargeOrder.setFromType(currentUser.getLoginType());
	rechargeWithDrawOrderService.insertSelective(rechargeOrder);
	log.info("实际金好支付网关地址["+Address +"],交易报文: "+jinHaoPay);
	
	String codeUrl="";
	if(payType.equals("WEIXIN")||payType.equals("ALIPAY")||payType.equals("QQPAY")||payType.equals("UNIONPAY")||payType.equals("QUICK_PAY")){
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("payKey", jinHaoPay.getPayKey());// 商户支付Key
		paramMap.put("orderPrice", jinHaoPay.getOrderPrice()); //支付金额
		 // 订单编号
		paramMap.put("outTradeNo", jinHaoPay.getOutTradeNo());
		paramMap.put("productType",jinHaoPay.getProductType());//B2C T0支付
	    // 订单时间
		paramMap.put("orderTime", jinHaoPay.getOrderTime());
		paramMap.put("productName", jinHaoPay.getProductName());// 商品名称
		 // 下单IP
		paramMap.put("orderIp", ip);
	     // 页面通知返回url
		paramMap.put("returnUrl", jinHaoPay.getNotifyUrl());
		 // 后台消息通知Url
		paramMap.put("notifyUrl", jinHaoPay.getReturnUrl());
		
		if(payType.equals("QUICK_PAY")){
	    paramMap.put("productType","50000103");
	    paramMap.put("bankCode", jinHaoPay.getBankCode());//银行编码
	    paramMap.put("bankAccountType", jinHaoPay.getBankAccountType());
	    paramMap.put("mobile","1");
	    Address="http://gateway.tfhmye.top/b2cPay/initPay";
	    jinHaoPay.setProductType("50000103");
		}
		paramMap.put("remark", "aa");
		///// 签名及生成请求API的方法///
		String sign = md5Util.getSign(paramMap,"&paySecret=", chargePay.getSign());
		paramMap.put("sign", sign);
		jinHaoPay.setSign(sign);
		System.out.println(paramMap);
		
        if(!payType.equals("QUICK_PAY")){
        	String payResult = SimpleHttpUtils.httpPost(Address, paramMap);
	
		
		JinHaoReturnPayUrl jinHaoReturnPayUrl=JSONUtils.toBean(payResult,JinHaoReturnPayUrl.class);
			if (jinHaoReturnPayUrl != null) {
				if (jinHaoReturnPayUrl.getResultCode().equals("0000")) {
					codeUrl = jinHaoReturnPayUrl.getPayMessage();
					
					log.info("接收金好支付转发成功,商户号[" + jinHaoPay.getPayKey()
							+ "],充值金额[" + jinHaoPay.getOrderPrice()
							+ "],银行码表[" + jinHaoPay.getProductType()
							+ "],提交金好支付处理");
				} else {
					out.println(jinHaoReturnPayUrl.getErrMsg());
					log.info("金好支付返回错误信息: "
							+ jinHaoReturnPayUrl.getErrMsg());
				}
			} else {
				log.info("金好支付返回参数为空");
				return;
			}
		}
		//System.out.println(payResult);
	}
	request.setAttribute("payType", payType);
	/* else{
		
		log.info("接收聚合支付转发成功,商户号[" + juHePay.getMemberCode() + "],充值金额[" + juHePay.getPayMoney()
				+ "],银行码表[" + juHePay.getPayType() + "],提交聚合支付处理");
	} */

	/* request.setAttribute("payType",xinMa.getPay_type()); */
%>
 
 


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>

<c:choose>
	<c:when test="${payType=='WEIXIN' || payType=='ALIPAY' || payType=='QQPAY'}">
		<body onload="pay.submit()">
			<form method="post" name="pay" id="pay" action="<%=payUrl%>">
				<TABLE>
					<TR>
						<TD><input name='codeUrl' type='hidden' value="<%=codeUrl%>"/>
							<input name='payType' type='hidden' value="<%=payType%>"/></TD>
					</TR>
				</TABLE>

			</form>

		</body>
	</c:when>
    <c:when test="${payType=='QUICK_PAY'}">
		<body onload="pay.submit()">
			<form method="post" name="pay" id="pay" action="<%=Address%>">
				<TABLE>
					<TR>
						<TD><input name='payKey' type='hidden' value="<%=jinHaoPay.payKey%>"/>
							<input name='orderPrice' type='hidden' value="<%=jinHaoPay.orderPrice%>"/>
							<input name='outTradeNo' type='hidden' value="<%=jinHaoPay.outTradeNo%>"/>
							<input name='productType' type='hidden' value="<%=jinHaoPay.productType%>"/>
							<input name='orderTime' type='hidden' value="<%=jinHaoPay.orderTime%>"/>
							<input name='productName' type='hidden' value="<%=jinHaoPay.productName%>"/>
							<input name='orderIp' type='hidden' value="<%=jinHaoPay.orderIp%>"/>
							<input name='bankCode' type='hidden' value="<%=jinHaoPay.bankCode%>"/>
							<input name='bankAccountType' type='hidden' value="<%=jinHaoPay.bankAccountType%>"/>
							<input name='returnUrl' type='hidden' value="<%=jinHaoPay.notifyUrl%>"/>
							<input name='notifyUrl' type='hidden' value="<%=jinHaoPay.returnUrl%>"/>
							<input name='remark' type='hidden' value="<%=jinHaoPay.remark%>"/>
							<input name='mobile' type='hidden' value="1"/>
							<input name='sign' type='hidden' value="<%=jinHaoPay.sign%>"/>
							</TD>
					</TR>
				</TABLE>
			</form>
		</body>
	</c:when>
	

	<c:otherwise>
	</c:otherwise>
</c:choose>
</html>
