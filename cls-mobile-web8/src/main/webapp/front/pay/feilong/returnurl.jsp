<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.vo.RechargeWithDrawOrder"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%  
    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String orderId = request.getParameter("orderId");//菲龙订单号
	String userOrderId = request.getParameter("userOrderId");//商户订单号
	String companyId = request.getParameter("companyId");//商户号
	String outOrderId = request.getParameter("outOrderId");//三方交易单号
	String fee = request.getParameter("fee");//金额
	String payResult = request.getParameter("payResult");//支付结果
	String timestamp = request.getParameter("timestamp");//时间戳
	String sign = request.getParameter("sign");//MD5签名
	log.info("菲龙支付成功通知平台处理信息如下: 商户ID: "+companyId+" 商户订单号: "+userOrderId+" 订单结果: "+payResult+" 订单金额: "+fee+" 菲龙商务订单号: "+orderId+" MD5签名: "+sign);
    
	/* RechargeWithDrawOrderService rechargeWithDrawOrderService = (RechargeWithDrawOrderService)ApplicationContextUtil.getBean("rechargeWithDrawOrderService");
	List<RechargeWithDrawOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(userOrderId); */
	RechargeOrderService rechargeOrderService = (RechargeOrderService) ApplicationContextUtil.getBean("rechargeOrderService"); 
	List<RechargeOrder> rechargeOrders = rechargeOrderService.getRechargeOrderBySerialNumber(userOrderId);
	
	if(rechargeOrders == null || rechargeOrders.size() != 1){
		log.error("根据订单号[" + userOrderId + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知菲龙支付报文接收成功
	    out.print("ok");
		return;
	}
	RechargeOrder rechargeOrder = rechargeOrders.get(0);
	if(rechargeOrder == null) {
		log.error("根据订单号[" + userOrderId + "]查找充值订单记录为空");
		//通知菲龙支付报文接收成功
	    out.print("ok");
		return;
	}
	
	//签名
	String Md5key = rechargeOrder.getSign();
	String MARK = "_";
	String md5 =new String(companyId+MARK+userOrderId+MARK+fee+MARK+Md5key);
	//Md5加密串
	String WaitSign = Md5Util.getMD5ofStr(md5);

	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if(WaitSign.compareTo(sign)==0){
		boolean dealResult = false;
		if(!StringUtils.isEmpty(payResult) && payResult.equals("0")){
			BigDecimal factMoneyValue = new BigDecimal(fee).divide(new BigDecimal("100"));
			try {
				dealResult = rechargeOrderService.rechargeOrderSuccessDeal(userOrderId,factMoneyValue);
			}catch(UnEqualVersionException e) {
		   		log.error("菲龙支付在线充值时发现版本号不一致,订单号["+userOrderId+"]");
		   		//启动新进程进行多次重试
		   		UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION
		   				, userOrderId,factMoneyValue,"");
		   		thread.start();
			} catch(Exception e){
				//异常不进行重试
				log.error(e.getMessage());
			}	
		} else {
			if(!payResult.equals("0")){
				log.info("菲龙支付处理错误支付结果："+payResult);
			}
		}
		log.info("菲龙支付订单处理入账结果:{}", dealResult);
		//通知菲龙支付报文接收成功
	    out.print("ok");
		return;
	}else{
		log.info("MD5校验失败,报文返回MD5签名内容["+ sign +"],计算MD5签名内容[" + WaitSign + "]");
		//让第三方继续补发
	    out.print("error");
		return;
	}
%>