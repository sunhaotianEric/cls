<%@page import="com.team.lottery.pay.model.muming.MuMingPayReturn"%>
<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="com.team.lottery.pay.model.jindouyun.JinDouYunPayReturn"%>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.vo.RechargeWithDrawOrder"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@page import="java.io.*"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="net.sf.json.JSONObject"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.extvo.returnModel.DuDuPayReturnParam"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
    // 支付成功回调响应码.
	String status = request.getParameter("status");
    // 平台订单号
	String merchant_order_no = request.getParameter("merchant_order_no");
    // 支付时间.
	String payment_time = request.getParameter("payment_time");
    // 支付金额.
	String trade_amount = request.getParameter("trade_amount");
    // 商户号.
	String merchant_no = request.getParameter("merchant_no");
    // 签名.
	String sign = request.getParameter("sign");
    // 新建慕名回调对象.
    MuMingPayReturn muMingPayReturn = new MuMingPayReturn();
    muMingPayReturn.setMerchant_no(merchant_no);
    muMingPayReturn.setMerchant_order_no(merchant_order_no);
    muMingPayReturn.setPayment_time(payment_time);
    muMingPayReturn.setSign(sign);
    muMingPayReturn.setStatus(status);
    muMingPayReturn.setTrade_amount(trade_amount);
    // 打印日志.通知成功.
	log.info(muMingPayReturn.toString());

	RechargeOrderService rechargeOrderService = (RechargeOrderService) ApplicationContextUtil.getBean("rechargeOrderService"); 
	List<RechargeOrder> rechargeOrders = rechargeOrderService.getRechargeOrderBySerialNumber(merchant_order_no);
	if (rechargeOrders == null || rechargeOrders.size() != 1) {
		log.error("根据订单号[" + merchant_order_no + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知慕名云支付报文接收成功
		out.print("success");
	}
	RechargeOrder rechargeWithDrawOrder = rechargeOrders.get(0);
	if (rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + merchant_order_no + "]查找充值订单记录为空");
		//通知慕名云支付报文接收成功
		out.print("success");
	}
	//获取秘钥.
	String Md5key = rechargeWithDrawOrder.getSign();
	String MARK = "&";
	//拼接加密字符串.
	String mD5Str = "merchant_no=" + merchant_no + MARK +
					"merchant_order_no=" + merchant_order_no + MARK +
					"payment_time=" + payment_time + MARK + 
					"status=" + status + MARK + 
					"trade_amount=" + trade_amount + MARK + 
					"key=" + Md5key;
	
	log.info("当前MD5源码:" + mD5Str);
	//Md5加密串并且大写
	String WaitSign = Md5Util.getMD5ofStr(mD5Str);
	log.info("加密后的MD5: " + WaitSign);

	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if (WaitSign.compareTo(sign) == 0) {
		boolean dealResult = false;
		if (!StringUtils.isEmpty(status) && status.equals("2")) {
			BigDecimal factMoneyValue = new BigDecimal(trade_amount);
			try {
				dealResult = rechargeOrderService.rechargeOrderSuccessDeal(merchant_order_no, factMoneyValue);
			} catch (UnEqualVersionException e) {
				log.error("慕名支付充值时发现版本号不一致,订单号[" + merchant_order_no + "]");
				//启动新进程进行多次重试
				UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, merchant_order_no,
						factMoneyValue, "");
				thread.start();
			} catch (Exception e) {
				//异常不进行重试
				log.error(e.getMessage());
			}
		} else {
			if (!status.equals("2")) {
				log.info("慕名支付处理错误支付结果payState为：" +  status);
			}
		}
		log.info("慕名支付订单处理入账结果:{}", dealResult);
		//通知慕名付报文接收成功
		out.println("success");
		return;
	} else {
		log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容[" + WaitSign + "]");
		out.println("success");
		return;
	}
%>