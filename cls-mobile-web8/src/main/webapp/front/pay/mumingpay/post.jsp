<%@page import="com.team.lottery.pay.model.muming.MuMingPay"%>
<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.team.lottery.service.QuickBankTypeService"%>
<%@page import="com.team.lottery.util.BaseDwrUtil"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.vo.RechargeWithDrawOrder"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundThirdPayType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.team.lottery.vo.QuickBankType"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="java.net.URLEncoder"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.team.lottery.service.RechargeConfigService"%>
<%@page import="com.team.lottery.vo.RechargeConfig"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
	String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	String serialNumber = request.getParameter("serialNumber"); // 获取配置ID;
	String orderMoneyYuan = OrderMoney;

	User currentUser = (User) request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	if (currentUser == null) {
		response.sendRedirect(path + "/gameBet/login.html");
	}
	Date currTime = new Date();
    //时间以yyyy-MM-dd HH:mm:ss的方式表示
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.CHINA);
    //时间以yyyyMMDDHHmmss的方式表示 
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss",Locale.CHINA);
    String webdate=new String(formatter1.format(currTime));
    String TradeDate=new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));
	
	log.info("业务系统[{}],用户名[{}],发起了慕名支付，生成订单号[{}],支付金额[{}],PayId[{}],payType[{}],chargePayId[{}]",
  			currentUser.getBizSystem(), currentUser.getUserName(), serialNumber, OrderMoney, payId, payType, chargePayId);
	String payUrl = chargePay.getPayUrl();
	String Address = chargePay.getAddress();
	log.info("payUrl: "+payUrl+" Address: "+Address);
	
	//同步跳转地址
	String notifyUrl = "";
	if (chargePay.getNotifyUrl().startsWith("http://") || chargePay.getNotifyUrl().startsWith("https://")) {
		notifyUrl = chargePay.getNotifyUrl();
	} else {
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
		notifyUrl = basePath + chargePay.getNotifyUrl();
	}
	
	// 慕名支付对象.
	MuMingPay muMingPay = new MuMingPay();
	// 设置商户号.
	muMingPay.setMerchant_no(chargePay.getMemberId());
	// 设置商户订单号.
	muMingPay.setMerchant_order_no(serialNumber);
	// 设置回调地址.
	muMingPay.setNotify_url(chargePay.getReturnUrl());
	// 设置码表.
	muMingPay.setPay_type(payId);
	// 设置同步跳转地址.
	muMingPay.setReturn_url(chargePay.getNotifyUrl());
	// 设置支付金额.
	muMingPay.setTrade_amount(OrderMoney);
	//sign加密;
	String mark = "&";
	//加密原始串.
	String md5SignStr =  "merchant_no=" + muMingPay.getMerchant_no() + mark + 
						 "merchant_order_no=" + muMingPay.getMerchant_order_no() + mark + 
						 "notify_url=" + muMingPay.getNotify_url() + mark + 
						 "pay_type=" + muMingPay.getPay_type() + mark + 
						 "return_url=" + muMingPay.getReturn_url() + mark + 
						 "trade_amount=" + muMingPay.getTrade_amount() + mark + 
						 "key=" + chargePay.getSign();

	//进行MD5小写加密
	String md5Sign = Md5Util.getMD5ofStr(md5SignStr);
	//打印日志
	log.info("md5原始串: " + md5SignStr + "生成后的交易签名:" + md5Sign);
	//设置签名
	muMingPay.setSign(md5Sign);

	//通过JSON格式去提交数据,以便获取第三方跳转地址
	Map<String, String> postDataMap = new TreeMap<String, String>();
	postDataMap.put("merchant_no", muMingPay.getMerchant_no());
	postDataMap.put("merchant_order_no", muMingPay.getMerchant_order_no());
	postDataMap.put("notify_url", muMingPay.getNotify_url());
	postDataMap.put("pay_type", muMingPay.getPay_type());
	postDataMap.put("return_url", muMingPay.getReturn_url());
	postDataMap.put("trade_amount", muMingPay.getTrade_amount());
	postDataMap.put("sign", muMingPay.getSign());
	//获取第三方给我们返回的JSON数据做判断跳转
	String result = HttpClientUtil.doPost(Address, postDataMap);
	log.info("慕名支付提交支付报文，返回内容:{}", result);
	Boolean isJson = JSONUtils.isJson(result);
	//最终我们要生成二维码的数据
	String payInfoNew = "";
	String basePath = "";
	if (isJson == true) {
		JSONObject jsonObject = JSONUtils.toJSONObject(result);
		if (StringUtils.isBlank(result)) {
			out.print("发起支付失败");
			return;
		}
		//获取返回的状态.1为发起成功,-1为发起失败.
		String status = jsonObject.getString("status");
		if (status.equals("1")) {
			//获取二维码支付路径.
			String data = jsonObject.getString("data");
			JSONObject jsonObjectData = JSONUtils.toJSONObject(data);
			String qRCodePath =  jsonObjectData.getString("pay_url");
			log.info("慕名支付,返回的支付地址为:" + qRCodePath);
			// 从返回的路径中获取是否是https请求,如果不是https请求就处理为https请求.
			int i = qRCodePath.indexOf("https");
			String pay_url = qRCodePath;
			// String.indexOf方法,返回-1就表示没有对应的字符串.
			if (i == -1) {
				pay_url = qRCodePath.replace("http", "https");
			}
			log.info("慕名支付,返回的支付地址处理过后的地址为:" + pay_url);
			response.sendRedirect(pay_url);
		} else {
			out.print(result);
			return;
		}
		log.info("慕名交易报文: " + muMingPay);
	} else {
		out.print(result);
		log.error("慕名支付发起失败: " + result);
		return;
	}
%>
