<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.vo.RechargeWithDrawOrder"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@page import="java.io.*"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="net.sf.json.JSONObject"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.extvo.returnModel.DuDuPayReturnParam"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
    
    //商户号.
	String memberid = request.getParameter("memberid");
    //订单号.
	String orderid = request.getParameter("orderid");
    //支付金额.
	String amount = request.getParameter("amount");
    //交易流水号.
	String transaction_id = request.getParameter("transaction_id");
    //交易时间.
	String datetime = request.getParameter("datetime");
    //交易状态00为成功.
	String returncode = request.getParameter("returncode");
    //签名
	String sign = request.getParameter("sign");
    
	log.info("和谐支付成功通知平台处理信息如下: 商户ID:" + memberid + " 订单号:" + orderid + "订单金额:" + amount + " 元," + "签名:" + sign);

	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService) ApplicationContextUtil.getBean("rechargeOrderService");
	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(orderid);
	if (rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1) {
		log.error("根据订单号[" + orderid + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知和谐支付报文接收成功
		response.getOutputStream().write("OK".getBytes("UTF-8"));
	}
	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
	if (rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + orderid + "]查找充值订单记录为空");
		//通知和谐支付报文接收成功
		response.getOutputStream().write("OK".getBytes("UTF-8"));
	}
	//获取秘钥.
	String apiKey = rechargeWithDrawOrder.getSign();
	String MARK = "&";
	
	//拼接加密字符串.
	String mD5Str = "amount=" + amount +MARK + 
					"datetime=" + datetime + MARK + 
					"memberid=" + memberid + MARK + 
					"orderid=" + orderid + MARK + 
					"returncode=" + returncode + MARK + 
					"transaction_id=" + transaction_id + MARK + 
					"key=" + apiKey;
	
	log.info("当前MD5源码:" + mD5Str);
	//Md5加密串并且大写
	String WaitSign = Md5Util.getMD5ofStr(mD5Str).toUpperCase();
	log.info("加密后的MD5: " + WaitSign);

	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if (WaitSign.compareTo(sign) == 0) {
		boolean dealResult = false;
		if (!StringUtils.isEmpty(returncode) && returncode.equals("00")) {
			BigDecimal factMoneyValue = new BigDecimal(amount);
			try {
				dealResult = rechargeWithDrawOrderService.rechargeOrderSuccessDeal(orderid, factMoneyValue);
			} catch (UnEqualVersionException e) {
				log.error("和谐支付充值时发现版本号不一致,订单号[" + orderid + "]");
				//启动新进程进行多次重试
				UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, orderid,
						factMoneyValue, "");
				thread.start();
			} catch (Exception e) {
				//异常不进行重试
				log.error(e.getMessage());
			}
		} else {
			if (!returncode.equals("00")) {
				log.info("和谐支付处理错误支付结果：" +  returncode.equals("00"));
			}
		}
		log.info("和谐支付付订单处理入账结果:{}", dealResult);
		//通知凡客付报文接收成功
		out.println("Ok");
		return;
	} else {
		log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容[" + WaitSign + "]");
		out.println("OK");
		return;
	}
%>