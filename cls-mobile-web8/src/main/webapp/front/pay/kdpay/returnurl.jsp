<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.team.lottery.vo.RechargeWithDrawOrder"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
<%  
    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String P_UserId = request.getParameter("P_UserId");//商户ID
	String P_OrderId = request.getParameter("P_OrderId");//商户订单号
	String P_FaceValue = request.getParameter("P_FaceValue");//面值/金额
	String P_ChannelId = request.getParameter("P_ChannelId");//充值类型ID
	String P_PayMoney = request.getParameter("P_PayMoney");//实际充值金额
	String P_Subject = request.getParameter("P_Subject");//产品名称
	String P_Price = request.getParameter("P_Price");//产品价格
	String P_Quantity = request.getParameter("P_Quantity");//产品数量	
	String P_Description = request.getParameter("P_Description");//银行ID
	String P_Notic = request.getParameter("P_Notic");//用户附加信息
	String P_ErrCode = request.getParameter("P_ErrCode");//错误代码(点击查看)
	String P_ErrMsg = request.getParameter("P_ErrMsg");//错误描述
	String P_PostKey = request.getParameter("P_PostKey");//签名认证串
	/* BigDecimal payMoney = new BigDecimal(P_PayMoney).setScale(2);
	P_PayMoney = String.valueOf(payMoney); */
	String P_CardId = "";
	String P_CardPass = "";
	log.info("口袋支付成功通知平台处理信息如下: 商户号: "+P_UserId+" 商户订单号: "+P_OrderId+" 错误代码: "+P_ErrCode+" 错误描述: "+P_ErrMsg+" 实际成功金额: "+
			P_FaceValue+" 第三方返回签名认证串: "+P_PostKey);
	
	
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(P_OrderId);
	if(rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1){
		log.error("根据订单号[" + P_OrderId + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知口袋报文接收成功
	    out.println("errCode=0");
		return;
	}
	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
	if(rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + P_OrderId + "]查找充值订单记录为空");
		//通知口袋报文接收成功
	    out.println("errCode=0");
		return;
	}
	
	String SalfStr = rechargeWithDrawOrder.getSign();
	String plain = P_UserId + "|" + P_OrderId + "|" + P_CardId + "|" + P_CardPass + "|" + P_FaceValue + "|" + P_ChannelId + "|" + SalfStr;
	String P_PostKeyCurrent = md5Util.getMD5ofStr(plain, "gb2312").toLowerCase();	
	
	if(P_PostKey.compareTo(P_PostKeyCurrent)==0){
		boolean dealResult = false;
		if(!StringUtils.isEmpty(P_ErrCode) && P_ErrCode.equals("0")) {
			BigDecimal factMoneyValue = new BigDecimal(P_FaceValue);
			try {
				dealResult = rechargeWithDrawOrderService.rechargeOrderSuccessDeal(P_OrderId,factMoneyValue);
			}catch(UnEqualVersionException e) {
		   		log.error("口袋在线充值时发现版本号不一致,订单号["+P_OrderId+"]");
		   		//启动新进程进行多次重试
		   		UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION
		   				, P_OrderId,factMoneyValue,"");
		   		thread.start();
			} catch(Exception e){
				//异常不进行重试
				log.error(e.getMessage());
			}
		} else {
			if(!P_ErrCode.equals("0")){
				log.info("口袋支付处理错误代码："+P_ErrCode+" 错误描述："+P_ErrMsg);
			}
		}
		
		log.info("口袋订单处理入账结果:{}", dealResult);
		//通知口袋报文接收成功
	    out.println("errCode=0");
		return;
	}else{
		log.info("MD5校验失败,报文返回MD5签名内容["+ P_PostKey +"],计算MD5签名内容[" + P_PostKeyCurrent + "]");
		//让第三方继续补发
	    out.println("errCode=1");
		return;
	}
%>