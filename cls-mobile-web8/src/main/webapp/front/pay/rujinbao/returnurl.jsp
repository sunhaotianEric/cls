<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.vo.RechargeWithDrawOrder"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%  
    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String merchant_no = request.getParameter("merchant_no");//商户号
	String version = request.getParameter("version");//版本号
	String out_trade_no = request.getParameter("out_trade_no");//商户订单号
	String payment_type = request.getParameter("payment_type");//支付类型
	String payment_bank = request.getParameter("payment_bank");//支付银行
	String trade_no = request.getParameter("trade_no");//支付平台内部的订单号
	String trade_status=request.getParameter("trade_status");//支付结果
	String notify_time = request.getParameter("notify_time");//通知时间
	String body = request.getParameter("body");//商品描述
	String total_fee = request.getParameter("total_fee");//交易金额
	String obtain_fee = request.getParameter("obtain_fee");//到账金额
	String sign = request.getParameter("sign");//MD5签名结果	
	log.info("入金宝支付成功通知平台处理信息如下:商户订单号: "+out_trade_no+" 订单结果: "+trade_status+" 订单金额: "+total_fee+" 入金宝商务订单号: "+trade_no+" 商品描述: "+
			body+"MD5签名: "+sign);
    
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(out_trade_no);
	if(rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1){
		log.error("根据订单号[" + out_trade_no + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知入金宝报文接收成功
	    out.println("SUCCESS");
		return;
	}
	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
	if(rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + out_trade_no + "]查找充值订单记录为空");
		//通知入金宝报文接收成功
	    out.println("SUCCESS");
		return;
	}
	
	//签名
	String Md5key = rechargeWithDrawOrder.getSign();
	String MARK = "&";
	String md5 = "body=" + body + MARK + "merchant_no=" + merchant_no + MARK + "notify_time=" + notify_time+ MARK +
			     "obtain_fee=" + obtain_fee + MARK + "out_trade_no=" + out_trade_no + MARK + "payment_bank=" + payment_bank+ MARK+
			     "payment_type=" + payment_type + MARK + "total_fee=" + total_fee + MARK + "trade_no=" + trade_no+ MARK+
			     "trade_status=" + trade_status + MARK + "version=" + version + Md5key;
	log.info("MD5字符串： "+md5);
	//Md5加密串
	String WaitSign = md5Util.getMD5ofStr(md5);

	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if(WaitSign.compareTo(sign)==0){
		boolean dealResult = false;
		if(!StringUtils.isEmpty(trade_status) && trade_status.equals("SUCCESS")){
			BigDecimal factMoneyValue = new BigDecimal(total_fee);
			try {
				dealResult = rechargeWithDrawOrderService.rechargeOrderSuccessDeal(out_trade_no,factMoneyValue);
			}catch(UnEqualVersionException e) {
		   		log.error("入金宝支付充值时发现版本号不一致,订单号["+out_trade_no+"]");
		   		//启动新进程进行多次重试
		   		UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION
		   				, out_trade_no,factMoneyValue,"");
		   		thread.start();
			} catch(Exception e){
				//异常不进行重试
				log.error(e.getMessage());
			}	
		} else {
			if(!trade_status.equals("0")){
				log.info("入金宝支付处理错误支付结果："+trade_status);
			}
		}
		log.info("入金宝订单处理入账结果:{}", dealResult);
		//通知入金宝报文接收成功
	    out.println("SUCCESS");
		return;
	}else{
		log.info("MD5校验失败,报文返回MD5签名内容["+ sign +"],计算MD5签名内容[" + WaitSign + "]");
		//让第三方继续补发
	    out.println("fail");
		return;
	}
%>