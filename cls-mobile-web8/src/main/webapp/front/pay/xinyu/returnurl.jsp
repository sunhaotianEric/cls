<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.vo.RechargeWithDrawOrder"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@ page import="com.team.lottery.extvo.returnModel.XinYuReturnParameter"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
<%@page import="com.team.lottery.util.JaxbUtil"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%  
    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	/* String partner = request.getParameter("partner");//商户ID
	String ordernumber = request.getParameter("ordernumber");//商户订单号
	String orderstatus = request.getParameter("orderstatus");//订单结果
	String paymoney = request.getParameter("paymoney");//订单金额
	String sysnumber = request.getParameter("sysnumber");//金阳支付订单号
	String attach = request.getParameter("attach");//备注信息
	String sign = request.getParameter("sign");//MD5签名 */
	StringBuilder body = new StringBuilder();
   
        BufferedReader br = request.getReader();
        while(true){
            String info = br.readLine();
            if(info == null){
                break;
            }
            body.append(info);
        }
        br.close();
        log.error("信誉返回的参数:"+body.toString());

	XinYuReturnParameter xinYuReturnParameter=JaxbUtil.transformToObject(XinYuReturnParameter.class,body.toString());
	log.info("信誉支付成功通知平台处理信息如下: 商户ID: "+xinYuReturnParameter.getMch_id()+" 商户订单号: "+xinYuReturnParameter.getOut_trade_no()+
			" 订单结果: "+xinYuReturnParameter.getResult_code()+" 订单金额: "+xinYuReturnParameter.getTotal_fee()+"MD5签名: "+xinYuReturnParameter.getSign());
	
    if(xinYuReturnParameter.getResult_code().equals("0")){
    	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(xinYuReturnParameter.getOut_trade_no());
	if(rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1){
		log.error("根据订单号[" + xinYuReturnParameter.getOut_trade_no() + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知信誉支付报文接收成功
	    out.println("success");
		return;
	}
	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
	if(rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + xinYuReturnParameter.getOut_trade_no() + "]查找充值订单记录为空");
		//通知信誉支付报文接收成功
	    out.println("success");
		return;
	}
	
	//签名
	String Md5key = rechargeWithDrawOrder.getSign();
	String MARK = "&";
	String md5 = "charset=" +xinYuReturnParameter.getCharset()+ MARK  + "fee_type=" + xinYuReturnParameter.getFee_type() +MARK+
			     "mch_id=" + xinYuReturnParameter.getMch_id() + MARK +  "nonce_str=" + xinYuReturnParameter.getNonce_str() + MARK + "out_trade_no=" + xinYuReturnParameter.getOut_trade_no() +MARK+
			     "pay_result=" + xinYuReturnParameter.getPay_result() + MARK + "result_code=" + xinYuReturnParameter.getResult_code() + MARK + "sign_type=" + xinYuReturnParameter.getSign_type() +MARK+
			     "status=" + xinYuReturnParameter.getStatus() + MARK + "time_end=" + xinYuReturnParameter.getTime_end()+MARK+"total_fee=" + xinYuReturnParameter.getTotal_fee() + MARK + "trade_type=" + xinYuReturnParameter.getTrade_type() + MARK + "transaction_id=" + xinYuReturnParameter.getTransaction_id() +MARK+
			     "version=" + xinYuReturnParameter.getVersion() + MARK+"key="+ Md5key;
	//Md5加密串
	String WaitSign = md5Util.getMD5ofStr(md5).toUpperCase();
	 log.error("原始加密串:"+md5);
	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if(WaitSign.compareTo(xinYuReturnParameter.getSign())==0){
		boolean dealResult = false;
		if(!StringUtils.isEmpty(xinYuReturnParameter.getResult_code()) && xinYuReturnParameter.getResult_code().equals("0")){
			BigDecimal factMoneyValue = new BigDecimal(xinYuReturnParameter.getTotal_fee()).divide( new BigDecimal(100));
			try {
				dealResult = rechargeWithDrawOrderService.rechargeOrderSuccessDeal(xinYuReturnParameter.getOut_trade_no(),factMoneyValue);
				log.info("信誉入账成功");
			}catch(UnEqualVersionException e) {
		   		log.error("信誉支付在线充值时发现版本号不一致,订单号["+xinYuReturnParameter.getOut_trade_no()+"]");
		   		//启动新进程进行多次重试
		   		UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION
		   				, xinYuReturnParameter.getOut_trade_no(),factMoneyValue,"");
		   		thread.start();
			} catch(Exception e){
				//异常不进行重试
				log.error(e.getMessage());
			}	
		} else {
			if(!xinYuReturnParameter.getResult_code().equals("0")){
				log.info("信誉支付处理错误支付结果："+xinYuReturnParameter.getErr_msg());
			}
		}
		log.info("信誉支付订单处理入账结果:{}", dealResult);
		//通知信誉支付报文接收成功
	    out.println("success");
		return;
	}else{
		log.info("MD5校验失败,报文返回MD5签名内容["+ xinYuReturnParameter.getSign() +"],计算MD5签名内容[" + WaitSign + "]");
		//让第三方继续补发
	    out.println("error");
		return;
	}
    }else{
    	log.info("信誉支付错误代码:"+xinYuReturnParameter.getErr_code()+"信誉支付错误代码描述:"+xinYuReturnParameter.getErr_msg()+"信誉支付返回信息描述:"+xinYuReturnParameter.getMessage());
    }
%>