<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.vo.RechargeWithDrawOrder"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<jsp:useBean id='digestUtil' scope='request' class='com.team.lottery.pay.util.DigestUtil'/>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%  
    request.setCharacterEncoding("GB2312");
    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
    String p1_MerId 	= request.getParameter("p1_MerId"); //p1_MerId	商户编号
	String r0_Cmd 		= request.getParameter("r0_Cmd"); //r0_Cmd	业务类型
	String r1_Code	 	= request.getParameter("r1_Code"); //r1_Code	支付结果
	String r2_TrxId 	= request.getParameter("r2_TrxId"); //r2_TrxId	易到交易流水号
	String r3_Amt 		= request.getParameter("r3_Amt"); //r3_Amt	支付金额
	String r4_Cur 		= request.getParameter("r4_Cur"); //r4_Cur	交易币种
	String r5_Pid		= request.getParameter("r5_Pid"); //r5_Pid	商品名称
	String r6_Order 	= request.getParameter("r6_Order"); //r6_Order	商户订单号
	String r7_Uid 		= request.getParameter("r7_Uid"); //r7_Uid		固定留空
	String r8_MP 		= request.getParameter("r8_MP"); //r8_MP	商户扩展信息
	String r9_BType 	= request.getParameter("r9_BType"); //r9_BType	交易结果返回类型
	String hmac 		= request.getParameter("hmac"); //hmac	签名数据
	log.info("银宝支付成功通知平台处理信息如下: 商户ID: "+p1_MerId+" 商户订单号: "+r6_Order+" 订单结果: "+r1_Code+" 订单金额: "+r3_Amt+" hmac签名: "+hmac);
    
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(r6_Order);
	if(rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1){
		log.error("根据订单号[" + r6_Order + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知易到支付报文接收成功
	    // 产品通用接口支付成功返回-浏览器重定向
			if(r9_BType.equals("1")) {
				out.println("callback方式:产品通用接口支付成功返回-浏览器重定向");
				// 产品通用接口支付成功返回-服务器点对点通讯
			} else if(r9_BType.equals("2")) {
				// 如果在发起交易请求时	设置使用应答机制时，必须应答以"success"开头的字符串，大小写不敏感
				out.println("SUCCESS");
			  // 产品通用接口支付成功返回-电话支付返回		
			}
		return;
	}
	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
	if(rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + r6_Order + "]查找充值订单记录为空");
		//通知易到支付报文接收成功
	   // 产品通用接口支付成功返回-浏览器重定向
			if(r9_BType.equals("1")) {
				out.println("callback方式:产品通用接口支付成功返回-浏览器重定向");
				// 产品通用接口支付成功返回-服务器点对点通讯
			} else if(r9_BType.equals("2")) {
				// 如果在发起交易请求时	设置使用应答机制时，必须应答以"success"开头的字符串，大小写不敏感
				out.println("SUCCESS");
			  // 产品通用接口支付成功返回-电话支付返回		
			}
		return;
	}
	
	//签名
	/* String Md5key = rechargeWithDrawOrder.getSign();
	String MARK = "&";
	String md5 = "partner=" + partner + MARK + "ordernumber=" + ordernumber + MARK + "orderstatus=" + orderstatus + MARK + "paymoney=" + paymoney + Md5key;
	//Md5加密串
	String WaitSign = md5Util.getMD5ofStr(md5); */
	String stringA = p1_MerId+r0_Cmd+r1_Code+r2_TrxId+r3_Amt+r4_Cur+r5_Pid+r6_Order+r7_Uid+r8_MP+r9_BType;
    String verifyHmac = digestUtil.hmacSign(stringA, rechargeWithDrawOrder.getSign());

	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if(verifyHmac.compareTo(hmac)==0){
		boolean dealResult = false;
		if(!StringUtils.isEmpty(r1_Code) && r1_Code.equals("1")){
			BigDecimal factMoneyValue = new BigDecimal(r3_Amt);
			try {
				dealResult = rechargeWithDrawOrderService.rechargeOrderSuccessDeal(r6_Order,factMoneyValue);
			}catch(UnEqualVersionException e) {
		   		log.error("易到支付在线充值时发现版本号不一致,订单号["+r6_Order+"]");
		   		//启动新进程进行多次重试
		   		UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION
		   				, r6_Order,factMoneyValue,"");
		   		thread.start();
			} catch(Exception e){
				//异常不进行重试
				log.error(e.getMessage());
			}
			
			// 产品通用接口支付成功返回-浏览器重定向
			if(r9_BType.equals("1")) {
				out.println("callback方式:产品通用接口支付成功返回-浏览器重定向");
				// 产品通用接口支付成功返回-服务器点对点通讯
			} else if(r9_BType.equals("2")) {
				// 如果在发起交易请求时	设置使用应答机制时，必须应答以"success"开头的字符串，大小写不敏感
				out.println("SUCCESS");
			  // 产品通用接口支付成功返回-电话支付返回		
			}
			// 下面页面输出是测试时观察结果使用
			out.println("<br>交易成功!<br>商家订单号:" + r6_Order + "<br>支付金额:" + r3_Amt + "<br>易到支付交易流水号:" + r2_TrxId);
			
		} else {
			if(!r1_Code.equals("1")){
				log.info("易到支付处理错误支付结果："+r1_Code);
			}
		}
		log.info("易到支付订单处理入账结果:{}", dealResult);
		//通知银宝报文接收成功
	   /*  out.println("ok"); */
		return;
	}else{
		out.println("交易签名被篡改!");
		//让第三方继续补发
	   /*  out.println("error"); */
		return;
	}
%>