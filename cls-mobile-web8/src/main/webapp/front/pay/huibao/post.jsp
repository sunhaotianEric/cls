<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.util.BaseDwrUtil"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.vo.RechargeWithDrawOrder"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundThirdPayType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.pay.model.huibao.HuiBaoPay"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String orderMoney = request.getParameter("OrderMoney");//订单金额 
	String payId = request.getParameter("PayId");//支付的银行接口
	String payType = request.getParameter("payType");//充值类型
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	
	if(StringUtils.isBlank(orderMoney) || StringUtils.isBlank(chargePayId)) {
		out.print("参数传递错误");
		return;
	}
    
	//用户登录校验
	User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
	//充值金额校验
    BigDecimal money = new BigDecimal(orderMoney);
	money = money.setScale(2, BigDecimal.ROUND_DOWN);
    
    //生成订单号
    String orderId=MakeOrderNum.makeOrderNum("CZ");
	log.info("业务系统[{}],用户名[{}],发起了惠宝支付，生成订单号[{}],支付金额[{}],PayId[{}],payType[{}],chargePayId[{}]",
			currentUser.getBizSystem(), currentUser.getUserName(), orderId, orderMoney, payId, payType, chargePayId);
	
	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));
    
	
	String payUrl = chargePay.getPayUrl();//转发支付网关地址
	String address = chargePay.getAddress(); //实际惠宝地址
	log.info("惠宝支付报文转发地址payUrl: "+payUrl+",实际网关地址address: "+address);
	
	//同步跳转地址
	String returnUrl = "";
	if(chargePay.getNotifyUrl().startsWith("http://") || chargePay.getNotifyUrl().startsWith("https://")) {
		returnUrl = chargePay.getNotifyUrl();
	} else {
		String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
		returnUrl = basePath + chargePay.getNotifyUrl();
	}
	
	//惠宝支付请求对象
	HuiBaoPay huibaoPay = new HuiBaoPay();
	huibaoPay.setVersion(chargePay.getInterfaceVersion());
	huibaoPay.setCustomerid(chargePay.getMemberId());
	huibaoPay.setSdorderno(orderId);
	huibaoPay.setTotal_fee(money.toString());
	huibaoPay.setPaytype(payId);
	huibaoPay.setBankcode("");
	//异步通知URL
	huibaoPay.setNotifyurl(chargePay.getReturnUrl());
	//同步跳转URL
	huibaoPay.setReturnurl(returnUrl);
	huibaoPay.setRemark(currentUser.getUserName());
	huibaoPay.setGet_code("0");
	
	
	//生成md5密钥签名
    String signKey = chargePay.getSign();///////////md5密钥（KEY）
    String infoStr = "version=" + huibaoPay.getVersion() + "&customerid=" + huibaoPay.getCustomerid() + "&total_fee=" + huibaoPay.getTotal_fee() + 
    	"&sdorderno=" + huibaoPay.getSdorderno() + "&notifyurl=" + huibaoPay.getNotifyurl() + "&returnurl=" + huibaoPay.getReturnurl() + 
        "&" + signKey;
    //infoStr = "476D420B9ED639577FF1AF356A6FF231+10+Ali+E32A96991CF9A27593FE273870B680D9+http://www.baidu.com/notifyUrl+http://www.baidu.com/returnUrl+77F41EE149299425A00E55EDCEEE2AC0";
    String signature = Md5Util.getMD5ofStr(infoStr);//计算MD5值
    huibaoPay.setSign(signature);
    log.info("惠宝支付报文签名处理,md5原始串: "+ infoStr + ",生成后的交易签名:" + signature);
    log.info("惠宝支付交易报文内容: "+huibaoPay);
    
	
    //构建订单
	RechargeOrder rechargeOrder = new RechargeOrder();
	//用户ID
	rechargeOrder.setUserId(currentUser.getId());
	//快捷ID
	rechargeOrder.setChargePayId(chargePay.getId());
	//用户名称
	rechargeOrder.setUserName(currentUser.getUserName());
	//申请金额
	rechargeOrder.setApplyValue(money);
	//订单创建时间
	rechargeOrder.setCreatedDate(new Date());
	//充值类型
	rechargeOrder.setPayType(payType);
	//第三方充值类型
    rechargeOrder.setThirdPayType(EFundThirdPayType.HUIBAO.name());
	//操作描述
	rechargeOrder.setOperateDes(EFundPayType.valueOf(payType).getDescription());
	//流水号
	rechargeOrder.setSerialNumber(orderId);
	rechargeOrder.setPostscript("");
	rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT.name());
	rechargeOrder.setBankName("");
	rechargeOrder.setSubbranchName("");
	rechargeOrder.setBankAccountName("");
	rechargeOrder.setBankCardNum("");
	rechargeOrder.setBankType("");
	rechargeOrder.setRefType(EFundRefType.THIRDPAY.name());
	rechargeOrder.setBizSystem(currentUser.getBizSystem());
	rechargeOrder.setFromType(currentUser.getLoginType());
	rechargeWithDrawOrderService.insertSelective(rechargeOrder);
	  
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>

<body onload="pay.submit()">
<form method="post" name="pay" id="pay" action="<%=payUrl%>">
	<input name='version' type='hidden' value= "<%= huibaoPay.getVersion()%>"/>
	<input name='customerid' type='hidden' value= "<%= huibaoPay.getCustomerid()%>"/>
	<input name='sdorderno' type='hidden' value= "<%= huibaoPay.getSdorderno()%>"/>
	<input name='total_fee' type='hidden' value= "<%= huibaoPay.getTotal_fee()%>"/>
	<input name='paytype' type='hidden' value= "<%= huibaoPay.getPaytype()%>"/>
	<input name='bankcode' type='hidden' value= "<%= huibaoPay.getBankcode()%>"/>
	<input name='notifyurl' type='hidden' value= "<%= huibaoPay.getNotifyurl()%>"/>
	<input name='returnurl' type='hidden' value= "<%= huibaoPay.getReturnurl()%>"/>
	<input name='remark' type='hidden' value= "<%= huibaoPay.getRemark()%>"/>
	<input name='get_code' type='hidden' value= "<%= huibaoPay.getGet_code()%>"/>
	<input name='sign' type='hidden' value= "<%= huibaoPay.getSign()%>"/>
</form>	

</body>
</html>
