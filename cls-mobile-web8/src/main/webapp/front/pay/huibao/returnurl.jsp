<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" trimDirectiveWhitespaces="true"%>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.vo.RechargeWithDrawOrder"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%  
    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
   
    String status = request.getParameter("status");	
    String customerid = request.getParameter("customerid");
    String sdpayno = request.getParameter("sdpayno");
    String sdorderno = request.getParameter("sdorderno");
    String total_fee = request.getParameter("total_fee");
    String paytype = request.getParameter("paytype");
    String remark = request.getParameter("remark");
    String sign = request.getParameter("sign");
    String orderNumber = sdorderno;
    
 	log.info("收到惠宝支付结果通知报文内容: status:{},customerid{},sdpayno:{},sdorderno:{},total_fee:{},paytype:{},remark:{},sign:{}",
 			status, customerid, sdpayno, sdorderno, total_fee, paytype, remark, sign);
    try{
		
    	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
    	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(orderNumber);
    	if(rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1){
    		log.error("根据订单号[" + orderNumber + "]查找充值订单记录为空或者有两笔相同订单号订单");
    		//通知惠宝支付报文接收成功
    		response.getOutputStream().write("success".getBytes("UTF-8"));
    	}
    	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
    	if(rechargeWithDrawOrder == null) {
    		log.error("根据订单号[" + orderNumber + "]查找充值订单记录为空");
    		//通知惠宝支付报文接收成功
    		response.getOutputStream().write("success".getBytes("UTF-8"));
    	}		
    	//计算当前信息的签名
		String infoStr = "customerid=" + customerid + "&status=" + status + "&sdpayno=" + sdpayno + "&sdorderno=" + sdorderno +
						"&total_fee=" + total_fee + "&paytype=" + paytype + "&" + rechargeWithDrawOrder.getSign();
		String calSign = Md5Util.getMD5ofStr(infoStr);
		log.info("报文返回MD5签名内容["+ sign +"],计算MD5签名内容[" + calSign + "]");
    	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
    	if(calSign.compareTo(sign)==0){
    		boolean dealResult = false;
    		if(!StringUtils.isEmpty(status) && status.equals("1")){
    			BigDecimal factMoneyValue = new BigDecimal(total_fee);
    			try {
    				dealResult = rechargeWithDrawOrderService.rechargeOrderSuccessDeal(orderNumber,factMoneyValue);
    			}catch(UnEqualVersionException e) {
    		   		log.error("惠宝支付在线充值时发现版本号不一致,订单号["+orderNumber+"]");
    		   		//启动新进程进行多次重试
    		   		UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION
    		   				, orderNumber,factMoneyValue,"");
    		   		thread.start();
    			} catch(Exception e){
    				//异常不进行重试
    				log.error(e.getMessage());
    			}	
    		} else {
    			if(!status.equals("1")){
    				log.info("惠宝支付处理错误支付结果："+status);
    				return;
    			}
    		}
    		log.info("惠宝支付订单处理入账结果:{}", dealResult);
    		 //通知惠宝支付报文接收成功
    	    /* response.getOutputStream().write("SUCCESS".getBytes()); */
    	    response.getOutputStream().write("success".getBytes("UTF-8"));
    		
    	}else{
    		log.info("惠宝支付MD5校验失败");
    		//让第三方继续补发
    		response.getOutputStream().write("success".getBytes("UTF-8"));
    	}
    	
    }catch(Exception e){
    	log.error(e.getMessage());
    }
    
%>