<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>s
<%@page import="com.team.lottery.util.BaseDwrUtil"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.vo.RechargeWithDrawOrder"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundThirdPayType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.RSA"%>
<%@page import="com.team.lottery.pay.model.JianZhengBaoPay"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.MD5Utils'/>
 <%@page errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.InetAddress"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="com.team.lottery.extvo.returnModel.YinHeParameter"%>
<%@page import="com.team.lottery.pay.model.XiongMaoPay"%>
<%@page import="java.util.*"%>
<%@page import="net.sf.json.JSONObject"%>
<%@ page import="com.team.lottery.util.HttpClientUtil"%>
<%@ page import="com.team.lottery.pay.util.RC4"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%  

    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
    String orderMoneyYuan = OrderMoney;
 
    
    User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
	
	

	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));
	String  orderId=MakeOrderNum.makeOrderNum("CZ");
	log.info("用户名称: "+currentUser.getUserName()+" 用户 ID号: "+currentUser.getId()+" 生成订单号: "+orderId+" 订单金额: "+OrderMoney);
	

	String payUrl = chargePay.getPayUrl();//转发银河网关地址
	String Address = chargePay.getAddress(); //最终银河地址
	log.info("payUrl: "+payUrl+" Address: "+Address);
	XiongMaoPay xiongMaoPay =new XiongMaoPay();
	
	xiongMaoPay.setAccount_id(chargePay.getMemberId());
	xiongMaoPay.setContent_type("text");
	xiongMaoPay.setThoroughfare("alipay_auto");
	xiongMaoPay.setOut_trade_no(orderId);
	xiongMaoPay.setRobin("2");
	xiongMaoPay.setAmount(orderMoneyYuan);
	xiongMaoPay.setCallback_url(chargePay.getReturnUrl());
	xiongMaoPay.setSuccess_url(chargePay.getNotifyUrl());
	xiongMaoPay.setError_url(chargePay.getNotifyUrl());
	xiongMaoPay.setType("2");
	xiongMaoPay.setKeyId("");
	String data = orderMoneyYuan +  orderId;

	System.out.println("data:" + data);

	String md5Crypt = md5Util.md5(data.getBytes());

	System.out.println("md5Crypt:" + md5Crypt);

	byte[] rc4_string = RC4.encry_RC4_byte(md5Crypt, chargePay.getSign());

	System.out.println("rc4_string:" + rc4_string);

	String sign = md5Util.md5(rc4_string);

	System.out.println("sign:" + sign);
	xiongMaoPay.setSign(sign);

    HashMap<String, String> params = new HashMap<String, String>();
	params.put("account_id", xiongMaoPay.getAccount_id());// 商户ID
	params.put("content_type", xiongMaoPay.getContent_type());// 网页类型
	params.put("thoroughfare", xiongMaoPay.getThoroughfare());// 支付通道
	params.put("out_trade_no", xiongMaoPay.getOut_trade_no());// 订单信息
	params.put("robin", xiongMaoPay.getRobin());// 轮训状态 //2开启1关闭
	params.put("amount", xiongMaoPay.getAmount());// 支付金额
	params.put("callback_url", xiongMaoPay.getCallback_url());// 异步通知url
	params.put("success_url", xiongMaoPay.getSuccess_url());// 支付成功后跳转到url
	params.put("error_url", xiongMaoPay.getError_url());// 支付失败后跳转到url
	    
	params.put("sign", xiongMaoPay.getSign());// 签名算法
	params.put("type", xiongMaoPay.getType());// 支付类型 //1为微信，2为支付宝
	params.put("keyId", xiongMaoPay.getKeyId());// 设备KEY 轮询无需填写
    
	//
	String order = HttpClientUtil.doPost("https://www.needmall.cn/gateway/index/checkpoint.do", params);
	// 获取结果
	System.out.println("result:" + order);

	JSONObject object = JSONObject.fromObject(order);
	JSONObject object2 = object.getJSONObject("data");
	String order_id = object2.getString("order_id");
	HashMap<String, String> paramsGet = new HashMap<String, String>();
	paramsGet.put("content_type", xiongMaoPay.getContent_type());// 商户ID
	paramsGet.put("id", order_id);// 网页类型
	String result = HttpClientUtil.doGet("https://www.needmall.cn/gateway/pay/service.do", paramsGet);
	System.out.println("result:" + result);
    
 %>
 
<%-- <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta id="viewport" name="viewport"
	content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
<title>充值接口-提交信息处理</title>
<style>
body {
	text-align: center;
}

img {
	margin-top: 60px;
}

p {
	font-size: 36px;
}
</style>
</head>

<body>
	<img id="weixinImg" src='<%=path%>/tools/getQrCodePic?code=<%=codeUrl%>'></img>
	<p>
		请使用<span id="pay-way">
		    <c:if test="${payType == 'ALIPAY' }">支付宝</c:if>
			<c:if test="${payType == 'WEIXIN' }">微信</c:if>
			<c:if test="${payType == 'JDQB' }">京东钱包</c:if>
			<c:if test="${payType == 'QQPAY' }">QQ钱包</c:if>
			<c:if test="${payType == 'UNIONPAY' }">银联二维码</c:if>
			</span>扫描二维码以完成支付
	</p>

</body>
</html> --%>
