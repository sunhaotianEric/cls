<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.vo.RechargeWithDrawOrder"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@page import="java.io.*"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="net.sf.json.JSONObject"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.extvo.returnModel.DuDuPayReturnParam"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
    
    //订单状态: 1支付成功,其他支付失败,
	String result = request.getParameter("result");
    //秀付订单号.
	String sp_linkid = request.getParameter("sp_linkid");
    //商户订单号.
	String sp_billno = request.getParameter("sp_billno");
    //订单金额.
	String price = request.getParameter("price");
    //实际支付金额.
	String realprice = request.getParameter("realprice");
    //备注信息.
	String sp_extdata = request.getParameter("sp_extdata");
    //MD5签名
	String sign = request.getParameter("sign");
    
	log.info("秀付支付成功通知平台处理信息如下:  秀付商户订单号:" + sp_linkid + "平台订单号:" + sp_billno + " 充值金额:" + price +"签名:" + sign);

	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService) ApplicationContextUtil.getBean("rechargeOrderService");
	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(sp_billno);
	if (rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1) {
		log.error("根据订单号[" + sp_billno + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知凡客支付报文接收成功
		response.getOutputStream().write("ok".getBytes("UTF-8"));
	}
	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
	if (rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + sp_billno + "]查找充值订单记录为空");
		//通知秀付支付报文接收成功
		response.getOutputStream().write("ok".getBytes("UTF-8"));
	}
	//获取秘钥.
	String Md5key = rechargeWithDrawOrder.getSign();
	//拼接加密字符串.
	String mD5Str = result +
					sp_linkid+
					sp_billno+
					price+
					realprice+
					sp_extdata+
					Md5key;
	
	log.info("当前MD5源码:" + mD5Str);
	//Md5加密串并且大写
	String WaitSign = Md5Util.getMD5ofStr(mD5Str);
	log.info("加密后的MD5: " + WaitSign);

	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if (WaitSign.compareTo(sign) == 0) {
		boolean dealResult = false;
		if (!StringUtils.isEmpty(result) && result.equals("1")) {
			BigDecimal factMoneyValue = new BigDecimal(price);
			try {
				dealResult = rechargeWithDrawOrderService.rechargeOrderSuccessDeal(sp_billno, factMoneyValue);
			} catch (UnEqualVersionException e) {
				log.error("秀付支付充值时发现版本号不一致,订单号[" + price + "]");
				//启动新进程进行多次重试
				UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, sp_billno,
						factMoneyValue, "");
				thread.start();
			} catch (Exception e) {
				//异常不进行重试
				log.error(e.getMessage());
			}
		} else {
			if (!result.equals("1")) {
				log.info("秀付支付处理错误支付结果：" +  result.equals("1"));
			}
		}
		log.info("秀付支付付订单处理入账结果:{}", dealResult);
		//通知凡客付报文接收成功
		out.println("ok");
		return;
	} else {
		log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容[" + WaitSign + "]");
		out.println("ok");
		return;
	}
%>