<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.vo.RechargeWithDrawOrder"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@page import="java.io.*"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.team.lottery.extvo.returnModel.BaDaPayReturnParam"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
    //获取当前的状态
    //获取商户ID
    String partner=request.getParameter("partner");
    //获取订单号
    String ordernumber=request.getParameter("ordernumber");
  	//获取支付状态
    String orderstatus=request.getParameter("orderstatus");
    //获取订单金额
    String paymoney=request.getParameter("paymoney");
    //获取当前时间戳
    String timestamp=request.getParameter("timestamp");
  	//获取系统中的订单账号
    String sysnumber=request.getParameter("sysnumber");
    //获取备注信息
    String attach=request.getParameter("attach");
    //获取签名
    String sign=request.getParameter("sign");
    log.info("八达付支付成功通知平台处理信息如下: 商户ID:"+partner+" 订单号:"+ordernumber+"支付状态:"+orderstatus+"订单金额:"+paymoney+"时间戳:"+timestamp+"系统中订单号:"+sysnumber+"签名:"+sign);
    
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(ordernumber);
	if(rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1){
		log.error("根据订单号[" + ordernumber + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知八达付支付报文接收成功
		response.getOutputStream().write("ok".getBytes("UTF-8"));
	}
	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
	if(rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + ordernumber + "]查找充值订单记录为空");
		//通知八达付支付报文接收成功
		response.getOutputStream().write("ok".getBytes("UTF-8"));
	}		
	//计算签名
	String Md5key = rechargeWithDrawOrder.getSign();
	String MARK = "&";
	String md5 = "partner="+partner+"&ordernumber="+ordernumber+"&orderstatus="+orderstatus+"&paymoney="+paymoney +"&timestamp="+timestamp+Md5key;
	log.info("当前MD5源码:"+md5);
	
	//Md5加密串
	String WaitSign = Md5Util.getMD5ofStr(md5);
	log.info("加密后的MD5: " + WaitSign);

	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if (WaitSign.compareTo(sign) == 0) {
		boolean dealResult = false;
		if (!StringUtils.isEmpty(orderstatus) && ("1").equals(orderstatus)) {
			BigDecimal factMoneyValue = new BigDecimal(paymoney);
			try {
				dealResult = rechargeWithDrawOrderService.rechargeOrderSuccessDeal(ordernumber, factMoneyValue);
			} catch (UnEqualVersionException e) {
				log.error("八达付支付充值时发现版本号不一致,订单号[" + ordernumber + "]");
				//启动新进程进行多次重试
				UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
						UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION,
						ordernumber, factMoneyValue, "");
				thread.start();
			} catch (Exception e) {
				//异常不进行重试
				log.error(e.getMessage());
			}
		} else {
			if (!("ok").equals(orderstatus)) {
				log.info("八达付支付处理错误支付结果：" + orderstatus);
			}
		}
		log.info("八达付付订单处理入账结果:{}", dealResult);
		//通知八达付报文接收成功
		out.println("ok");
		return;
	} else {
		log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容["
				+ WaitSign + "]");
		//让第三方继续补发
		out.println("err");
		return;
	}
%>