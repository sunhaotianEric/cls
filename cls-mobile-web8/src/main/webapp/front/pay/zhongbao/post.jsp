<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@page import="com.team.lottery.util.BaseDwrUtil"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.vo.RechargeWithDrawOrder"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundThirdPayType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.pay.model.ZhongBaoPay"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
<%  

    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
    String orderMoneyYuan = OrderMoney;
    
    User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
	
	Date currTime = new Date();
    //时间以yyyy-MM-dd HH:mm:ss的方式表示
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.CHINA);
    //时间以yyyyMMDDHHmmss的方式表示 
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss",Locale.CHINA);
    String webdate=new String(formatter1.format(currTime));
    String TradeDate=new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.getPayMemmberMsg(Long.valueOf(chargePayId));
	String  orderId = MakeOrderNum.makeOrderNum("CZ");
	log.info("用户名称: "+currentUser.getUserName()+" 用户 ID号: "+currentUser.getId()+" 生成订单号: "+orderId+" 订单金额: "+OrderMoney);
	
 	if(payType.equals("ALIPAY")){
		 payId="1006";
	}
 /* 	else if(payType.equals("WEIXIN")){
		 payId="1007";
	}  */ 


	/* if (!"".equals(OrderMoney)) 
	{	BigDecimal a;
		a = new BigDecimal(OrderMoney); //使用分进行提交
		OrderMoney=String.valueOf(a.setScale(2));
	}
	else{
		OrderMoney = ("0");
	} */
	String payUrl = chargePay.getPayUrl();//借贷混合
	String Address = chargePay.getAddress(); //最终地址
	log.info("payUrl: "+payUrl+" Address: "+Address);
	//众宝请求对象
	ZhongBaoPay zhongBaoPay = new ZhongBaoPay();
	zhongBaoPay.setCustomer(chargePay.getMemberId());
	zhongBaoPay.setBanktype(payId);
	zhongBaoPay.setAmount(OrderMoney);
	zhongBaoPay.setOrderid(orderId);
	zhongBaoPay.setAsynbackurl(chargePay.getReturnUrl());
	zhongBaoPay.setRequest_time(""+System.currentTimeMillis()/1000);//精确到秒
	zhongBaoPay.setSynbackurl(chargePay.getNotifyUrl());
	zhongBaoPay.setOnlyqr("");//不需要返回二维码
	zhongBaoPay.setAttach("");
	String Md5key = chargePay.getSign();///////////md5密钥（KEY）
	String md5 =new String("customer="+zhongBaoPay.getCustomer()+"&banktype="+zhongBaoPay.getBanktype()+"&amount="+zhongBaoPay.getAmount()+"&orderid="+zhongBaoPay.getOrderid()+"&asynbackurl="+zhongBaoPay.getAsynbackurl()+"&request_time="+zhongBaoPay.getRequest_time()+"&key="+Md5key);//MD5签名格式
	String Signature = md5Util.getMD5ofStr(md5, "GB2312").toLowerCase();//计算MD5值
	log.info("md5原始串: "+md5);
	log.info("交易签名: "+Signature);
	zhongBaoPay.setSign(Signature);
	log.info("交易报文: "+zhongBaoPay);
    //冲值订单
	RechargeOrder rechargeOrder = new RechargeOrder();
	//用户ID
	rechargeOrder.setUserId(currentUser.getId());
	//快捷ID
	rechargeOrder.setChargePayId(chargePay.getId());
	//用户名称
	rechargeOrder.setUserName(currentUser.getUserName());
	//申请金额
	rechargeOrder.setApplyValue(new BigDecimal(orderMoneyYuan));
	//订单创建时间
	rechargeOrder.setCreatedDate(new Date());
	//充值类型
	rechargeOrder.setPayType(payType);
	//第三方充值类型
	rechargeOrder.setThirdPayType(EFundThirdPayType.ZBPAY.name());
	//操作描述
	rechargeOrder.setOperateDes(EFundPayType.valueOf(payType).getDescription());
	//流水号
	rechargeOrder.setSerialNumber(orderId);
	rechargeOrder.setPostscript("");
	rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT.name());
	rechargeOrder.setBankName("");
	rechargeOrder.setSubbranchName("");
	rechargeOrder.setBankAccountName("");
	rechargeOrder.setBankCardNum("");
	rechargeOrder.setBankType("");
	rechargeOrder.setBizSystem(currentUser.getBizSystem());
	rechargeOrder.setRefType(EFundRefType.THIRDPAY.name());
	rechargeOrder.setFromType(currentUser.getLoginType());
	rechargeWithDrawOrderService.insertSelective(rechargeOrder);
 %>
 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>

<body onload="pay.submit()">
<form method="post" name="pay" id="pay" action="<%=payUrl%>">
<TABLE>
<TR>
	<TD>
	<input name='Address' type='hidden' value= "<%=Address%>"/>
	<input name='customer' type='hidden' value= "<%=zhongBaoPay.getCustomer()%>"/>
	<input name='banktype' type='hidden' value= "<%=zhongBaoPay.getBanktype()%>"/>
	<input name='amount' type='hidden' value= "<%=zhongBaoPay.getAmount()%>"/>
	<input name='orderid' type='hidden' value= "<%=zhongBaoPay.getOrderid()%>"/>
	<input name='asynbackurl' type='hidden' value= "<%=zhongBaoPay.getAsynbackurl()%>"/>		
	<input name='request_time' type='hidden' value= "<%=zhongBaoPay.getRequest_time()%>"/>
	<input name='synbackurl' type='hidden' value= "<%=zhongBaoPay.getSynbackurl()%>"/>
	<input name='onlyqr' type='hidden' value= "<%=zhongBaoPay.getOnlyqr()%>"/>			
	<input name='attach' type='hidden' value= "<%=zhongBaoPay.getAttach()%>" />
	<input name='sign' type='hidden' value= "<%=zhongBaoPay.getSign()%>" />

	</TD>
</TR>
</TABLE>
	
</form>	

</body>
</html>
