<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.util.BaseDwrUtil"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.vo.RechargeWithDrawOrder"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundThirdPayType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.pay.model.pujin.PuJinPay"%>
<%@page import="com.team.lottery.pay.model.pujin.PuJinPayReturn"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String orderMoney = request.getParameter("OrderMoney");//订单金额 
	String payId = request.getParameter("PayId");//支付的银行接口
	String payType = request.getParameter("payType");//充值类型
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	
	if(StringUtils.isBlank(orderMoney) || StringUtils.isBlank(chargePayId)) {
		out.print("参数传递错误");
		return;
	}
    
	//用户登录校验
	User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
    
    //生成订单号
    String orderId=MakeOrderNum.makeOrderNum("CZ");
	log.info("业务系统[{}],用户名[{}],发起了普金支付，生成订单号[{}],支付金额[{}],PayId[{}],payType[{}],chargePayId[{}]",
			currentUser.getBizSystem(), currentUser.getUserName(), orderId, orderMoney, payId, payType, chargePayId);
	
	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));
    
	//充值金额校验
    BigDecimal money = new BigDecimal(orderMoney);
	
	String payUrl = chargePay.getPayUrl();//转发支付网关地址
	String address = chargePay.getAddress(); //实际普金地址
	log.info("普金支付报文转发地址payUrl: "+payUrl+",实际网关地址address: "+address);
	
	//普金支付请求对象
	PuJinPay pujinPay = new PuJinPay();
	pujinPay.setMoney(orderMoney);
	pujinPay.setPayType(payId);
	pujinPay.setAppId(chargePay.getMemberId());
	pujinPay.setOrderNumber(orderId);
	pujinPay.setReturnUrl(chargePay.getNotifyUrl());
	pujinPay.setNotifyUrl(chargePay.getReturnUrl());
	
	//生成md5密钥签名
    String signKey = chargePay.getSign();///////////md5密钥（KEY）
    String infoStr = pujinPay.getAppId() + pujinPay.getMoney() + pujinPay.getPayType() + 
    	pujinPay.getOrderNumber() + pujinPay.getNotifyUrl() + pujinPay.getReturnUrl() + 
        signKey;
    //infoStr = "476D420B9ED639577FF1AF356A6FF231+10+Ali+E32A96991CF9A27593FE273870B680D9+http://www.baidu.com/notifyUrl+http://www.baidu.com/returnUrl+77F41EE149299425A00E55EDCEEE2AC0";
    String signature = Md5Util.getMD5ofStr(infoStr).toUpperCase();//计算MD5值
    pujinPay.setSignature(signature);
    log.info("普金支付报文签名处理,md5原始串: "+ infoStr + ",生成后的交易签名:" + signature);
    log.info("普金支付交易报文内容: "+pujinPay);
    
	
    //构建订单
	RechargeOrder rechargeOrder = new RechargeOrder();
	//用户ID
	rechargeOrder.setUserId(currentUser.getId());
	//快捷ID
	rechargeOrder.setChargePayId(chargePay.getId());
	//用户名称
	rechargeOrder.setUserName(currentUser.getUserName());
	//申请金额
	rechargeOrder.setApplyValue(money);
	//订单创建时间
	rechargeOrder.setCreatedDate(new Date());
	//充值类型
	rechargeOrder.setPayType(payType);
	//第三方充值类型
    rechargeOrder.setThirdPayType(EFundThirdPayType.PUJIN.name());
	//操作描述
	rechargeOrder.setOperateDes(EFundPayType.valueOf(payType).getDescription());
	//流水号
	rechargeOrder.setSerialNumber(orderId);
	rechargeOrder.setPostscript("");
	rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT.name());
	rechargeOrder.setBankName("");
	rechargeOrder.setSubbranchName("");
	rechargeOrder.setBankAccountName("");
	rechargeOrder.setBankCardNum("");
	rechargeOrder.setBankType("");
	rechargeOrder.setRefType(EFundRefType.THIRDPAY.name());
	rechargeOrder.setBizSystem(currentUser.getBizSystem());
	rechargeOrder.setFromType(currentUser.getLoginType());
	rechargeWithDrawOrderService.insertSelective(rechargeOrder);
	
    Map<String, String> postDataMap = new TreeMap<String, String>();
    postDataMap.put("appId", pujinPay.getAppId()); 
    postDataMap.put("money", pujinPay.getMoney());
    postDataMap.put("payType", pujinPay.getPayType());
    postDataMap.put("orderNumber", pujinPay.getOrderNumber());
    postDataMap.put("notifyUrl", pujinPay.getNotifyUrl());
    postDataMap.put("returnUrl", pujinPay.getReturnUrl());
    postDataMap.put("signature", pujinPay.getSignature());

	//发起支付请求
	String result = HttpClientUtil.doPost(payUrl, postDataMap);
	log.info("普金支付提交支付报文，返回内容:{}", result);
	if(StringUtils.isBlank(result)) {
		out.print("发起支付失败");
		return;
	} 
	PuJinPayReturn payReturn = JSONUtils.toBean(result, PuJinPayReturn.class);
	if(payReturn != null){
		if("true".equals(payReturn.success)) {
			String data = payReturn.data;
			response.sendRedirect(data);
		} else {
			out.print(payReturn.msg);
			return;
		}
	}else{
		out.print("发起支付失败,解析报文出错");
		return;
	}
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>

<body onload="">
<form method="post" name="pay" id="pay">
<table>
<tr>
	<td>
	</td>
</tr>
</table>
	
</form>	

</body>
</html>
