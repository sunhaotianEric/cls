<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.vo.RechargeWithDrawOrder"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.pay.model.YinBangPay"%>
<%@page import="com.team.lottery.util.GsonUtil"%>
<%@page import="com.team.lottery.util.Base64Local"%>
<%@page import="com.team.lottery.util.SecurityRSAPay"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String encParam =request.getParameter("encParam");
	String merId =request.getParameter("merId");
	String version =request.getParameter("version");
	String sign =request.getParameter("sign");
	
	//验签
	boolean  flag = SecurityRSAPay.verify(Base64Local.decode(encParam),Base64Local.decode(YinBangPay.serverPublicKey), Base64Local.decode(sign));
	if(!flag){
		log.info("银邦支付通知处理验签失败");
		return ;
	}	
	String respData =new String(SecurityRSAPay.decryptByPrivateKey(Base64Local.decode(encParam), Base64Local.decode(YinBangPay.privateKey)));
	
	log.info("银邦支付成功通知平台处理信息如下:业务参数: "+respData);
	Map<String ,String> map=new HashMap<String, String>();
	map = GsonUtil.fromJson(respData, Map.class);
	//String respCode=map.get("respCode");	//返回码返回1000表示成功。当dq_code为1000时，订单状态才有效。
	String orderId=map.get("orderId");	//商户订单号	字符串	商户订单号
	String payOrderId=map.get("payOrderId");	//支付订单号	字符串	支付订单号
	String order_state=map.get("order_state");	//订单状态
	String money=map.get("money");	//交易金额
	String payReturnTime=map.get("payReturnTime");	//付款时间	
	String selfParam=map.get("selfParam");	//自定义参数	
	String payType	=map.get("payType");//支付方式
	String payTypeDesc=map.get("payTypeDesc");	//支付方式描述
	money = new BigDecimal(money).divide(new BigDecimal("100")).toString();
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(orderId);
	if(rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1){
		log.error("根据订单号[" + orderId + "]查找充值订单记录为空或者有两笔相同订单号订单");
		return;
	}
	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
	if(rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + orderId + "]查找充值订单记录为空");
		return;
	}
	
	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if ("1003".equals(order_state)) {
		boolean dealResult = false;
			BigDecimal factMoneyValue = new BigDecimal(money);
			try {
				dealResult = rechargeWithDrawOrderService
						.rechargeOrderSuccessDeal(orderId,
								factMoneyValue);
			} catch (UnEqualVersionException e) {
				log.error("高通支付充值时发现版本号不一致,订单号[" + orderId + "]");
				//启动新进程进行多次重试
				UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
						UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION,
						orderId, factMoneyValue, "");
				thread.start();
			} catch (Exception e) {
				//异常不进行重试
				log.error(e.getMessage());
			}
		
		log.info("高通订单处理入账结果:{}", dealResult);
		//通知赢通宝报文接收成功
		out.println("SUCCESS");
		return;
	} else {
		log.info("订单状态无效效order_state="+order_state);
		return;
	}
%>