<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@page import="com.team.lottery.service.QuickBankTypeService"%>
<%@page import="com.team.lottery.util.BaseDwrUtil"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.vo.RechargeWithDrawOrder"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundThirdPayType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.pay.model.YinBangPay"%>
<%@page import="com.team.lottery.vo.QuickBankType"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.util.GsonUtil"%>
<%@page import="com.team.lottery.util.Base64Local"%>
<%@page import="com.team.lottery.util.SecurityRSAPay"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%  

    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
    String orderMoneyYuan = OrderMoney;
    
    User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
    OrderMoney  = new BigDecimal(OrderMoney).multiply(new BigDecimal("100")).toString();
	Date currTime = new Date();
    //时间以yyyy-MM-dd HH:mm:ss的方式表示
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.CHINA);
    //时间以yyyyMMDDHHmmss的方式表示 
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss",Locale.CHINA);
    String webdate=new String(formatter1.format(currTime));
    String TradeDate=new String(formatter2.format(currTime));
    QuickBankTypeService quickBankTypeService = (QuickBankTypeService)ApplicationContextUtil.getBean("quickBankTypeService");
	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.getPayMemmberMsg(Long.valueOf(chargePayId));
	String  orderId = MakeOrderNum.makeOrderNum("CZ");
	log.info("用户名称: "+currentUser.getUserName()+" 用户 ID号: "+currentUser.getId()+" 生成订单号: "+orderId+" 订单金额: "+OrderMoney);
	QuickBankType query = new QuickBankType();
	query.setPayId(payId);
	String bankType = "";
	List<QuickBankType>  list = quickBankTypeService.getQuickBankTypeQuery(query);
	if(list.size() > 0){
		bankType = list.get(0).getBankType();
	}
	if("1006".equals(payId)){
		payId = "1008";
	}else if("1013".equals(payId)){
		payId = "1014";
	}
	
	String payUrl = chargePay.getPayUrl();//借贷混合
	String Address = chargePay.getAddress(); //最终地址
	log.info("payUrl: "+payUrl+" Address: "+Address);
	//业务参数
	YinBangPay yinBangPay = new YinBangPay();
	yinBangPay.setBusinessOrdid(orderId);
	yinBangPay.setOrderName("充值");
	yinBangPay.setMerId(chargePay.getMemberId());
	yinBangPay.setTerId(chargePay.getTerminalId());
	yinBangPay.setTradeMoney(OrderMoney);
	yinBangPay.setSelfParam("");
	yinBangPay.setPayType(payId);
	yinBangPay.setAppSence("1001");
	yinBangPay.setAsynURL(chargePay.getReturnUrl());
	yinBangPay.setSyncURL(chargePay.getNotifyUrl());
	
	String json = GsonUtil.toJson(yinBangPay);
	
	
	log.info("业务参数: "+json);
	//服务器公钥加密
	byte by[] = SecurityRSAPay.encryptByPublicKey(json.getBytes("utf-8"), Base64Local.decode(YinBangPay.serverPublicKey));

	String baseStrDec=   Base64Local.encodeToString(by, true); 
	
	//自己的私钥签名
	byte signBy[]=SecurityRSAPay.sign(by, Base64Local.decode(YinBangPay.privateKey));
	String sign =Base64Local.encodeToString(signBy,true);
	log.info("交易报文: "+sign);

    //冲值订单
	RechargeOrder rechargeOrder = new RechargeOrder();
	//用户ID
	rechargeOrder.setUserId(currentUser.getId());
	//快捷ID
	rechargeOrder.setChargePayId(chargePay.getId());
	//用户名称
	rechargeOrder.setUserName(currentUser.getUserName());
	//申请金额
	rechargeOrder.setApplyValue(new BigDecimal(orderMoneyYuan));
	//订单创建时间
	rechargeOrder.setCreatedDate(new Date());
	//充值类型
	rechargeOrder.setPayType(payType);
	//第三方充值类型
	rechargeOrder.setThirdPayType(EFundThirdPayType.YBPAY.name());
	//操作描述
	rechargeOrder.setOperateDes(EFundPayType.valueOf(payType).getDescription());
	//流水号
	rechargeOrder.setSerialNumber(orderId);
	rechargeOrder.setPostscript("");
	rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT.name());
	rechargeOrder.setBankName("");
	rechargeOrder.setSubbranchName("");
	rechargeOrder.setBankAccountName("");
	rechargeOrder.setBankCardNum("");
	rechargeOrder.setBankType(bankType);
	rechargeOrder.setBizSystem(currentUser.getBizSystem());
	rechargeOrder.setRefType(EFundRefType.THIRDPAY.name());
	rechargeWithDrawOrderService.insertSelective(rechargeOrder);
 %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>

<body onload="pay.submit()">
<form method="post" name="pay" id="pay" action="<%=payUrl%>">
<TABLE>
<TR>
	<TD>
	<input name='Address' type='hidden' value= "<%=Address%>"/>
	<input name='encParam' type='hidden' value= "<%=baseStrDec%>"/>
	<input name='sign' type='hidden' value= "<%=sign%>"/>
	<input name='merId' type='hidden' value= "<%=yinBangPay.getMerId()%>"/>
	<input name='version' type='hidden' value= "<%=chargePay.getInterfaceVersion()%>"/>		
	</TD>
</TR>
</TABLE>
	
</form>	

</body>
</html>
