<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="format-detection" content="telephone=no">
    <title>安全问题</title>
    <link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
    <link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
</head>
<body>

<!-- header -->
<div class="header color-dark">
    <div class="header-left" onclick="window.history.go(-1)"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
    <h2 class="header-title">设置安全问题</h2>
</div>
<!-- header over -->
<div class="stance"></div>

<ul class="user-list">
    <!-- user-list 输入框 -->
    <li class="user-line">
        <div class="container">
            <div class="line-title">
                <p>问题1</p>
            </div>
            <div class="line-content">
                <select class="inputSelect long" name="" id="dna_ques_one">
					<option value="">请选择问题</option>
				</select>
            </div>
        </div>
    </li>
    <!-- user-list over 输入框 -->
    <!-- user-list 输入框 -->
    <li class="user-line">
        <div class="container">
            <div class="line-title">
                <p>答案</p>
            </div>
            <div class="line-content"><input class="inputText" type="text" id="answerOne" placeholder=""></div>
        </div>
    </li>
    <!-- user-list over 输入框 -->
</ul>
<ul class="user-list">
    <!-- user-list 输入框 -->
    <li class="user-line">
        <div class="container">
            <div class="line-title">
                <p>问题2</p>
            </div>
            <div class="line-content">
                <select class="inputSelect long" name="" id="dna_ques_two">
					<option value="">请选择问题</option>
				</select>
            </div>
        </div>
    </li>
    <li class="user-line">
        <div class="container">
            <div class="line-title">
                <p>答案</p>
            </div>
            <div class="line-content"><input class="inputText" type="text"  id="answerTwo" placeholder=""></div>
        </div>
    </li>
    <!-- user-list over 输入框 -->
</ul>
<ul class="user-list">
    <!-- user-list 输入框 -->
    <li class="user-line">
        <div class="container">
            <div class="line-title">
                <p>问题3</p>
            </div>
            <div class="line-content">
                <select class="inputSelect long" name="" id="dna_ques_three">
					<option value="">请选择问题</option>
				</select>
            </div>
        </div>
    </li>
    <!-- user-list over 输入框 -->
    <!-- user-list 输入框 -->
    <li class="user-line">
        <div class="container">
            <div class="line-title">
                <p>答案</p>
            </div>
            <div class="line-content"><input class="inputText" type="text"  id="answerThree" placeholder=""></div>
        </div>
    </li>
    <!-- user-list over 输入框 -->
</ul>
<ul class="user-list">
    <!-- user-list 输入框 -->
    <li class="user-line">
        <div class="container">
            <div class="line-title">
                <p>安全密码</p>
            </div>
            <div class="line-content"><input class="inputText" type="password"  id="password" placeholder=""></div>
        </div>
    </li>
    <!-- user-list over 输入框 -->
</ul>


<ul class="user-list no">
    <!-- user-list 提示信息 -->
    <li class="user-msg">
        <div class="container">
            <p>答案包含4-16字符，可由中文 字母数字组成</p>
        </div>
    </li>
    <!-- user-list over 提示信息 -->
</ul>

<div class="user-btn color-red" id="sQuestionBtn">保存</div>
<ul class="user-list no">
    <!-- user-list 提示信息 -->
    <li class="user-msg">
        <div class="container">
            <p id="safetyPasswordTip"></p>
        </div>
    </li>
    <!-- user-list over 提示信息 -->
</ul>
<!-- nav-bar -->
<jsp:include page="/front/include/navbar.jsp"></jsp:include>
<!-- nav-bar over -->
<jsp:include page="/front/include/include.jsp"></jsp:include>

<script src="<%=path%>/js/safequestion/set_safe_question.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
</body>
</html>