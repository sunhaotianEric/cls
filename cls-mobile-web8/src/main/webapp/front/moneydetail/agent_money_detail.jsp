<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant,com.team.lottery.enums.EMoneyDetailType" %>
<%
	String path = request.getContextPath();
%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta id="viewport" name="viewport" content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="format-detection" content="telephone=no">
	<title>下级账变</title>
	<link href="<%=path%>/css/base.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
	<link href="<%=path%>/css/base.an.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
	<link href="<%=path%>/css/user_order.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
	<link href="<%=path%>/css/user_team_lowerlevel.css?v=<%=SystemConfigConstant.mobileWebRsVersion%>" rel="stylesheet">
</head>
<body class="user_order money-detail">
	<!-- header -->
	<div class="header color-dark">
		<div class="header-left" onclick="window.history.go(-1);"><img class="icon" src="<%=path%>/images/icon/icon-return.png"></div>
		<h2 class="header-title">下级账变明细</h2>
		<div class="header-right" onclick="show_classify('.lottery-classify1',this)"><span>今天</span><img class="icon" src="<%=path%>/images/icon/icon-pointer-white.png"></div>
	</div>
	<div class="lottery-classify lottery-classify1 animated">
		<div class="container">
			<div class="move-content">
				<div class="child color-red">
					<p data-value="today">今天</p>
				</div>
				<div class="child color-red">
					<p data-value="yesterDay">昨天</p>
				</div>
				<div class="child color-red">
					<p data-value="week">最近7天</p>
				</div>
			</div>
		</div>
	</div>
	<!-- header over -->
	<div class="alert-bg" onclick="element_toggle(false,['.lottery-classify','.alert'])"></div>

	<div class="stance"></div>

	<div class="user-line bet-search">
		<div class="container">
			<div class="line-content">
				<input class="inputText" type="text" placeholder="下级用户名" id="userName" name="userName">
			</div>
			<div class="lottery-name">
				<span class="text"><p data-value="">全部</p></span>
				<i class="icon-arrow-down"></i>
				<!-- <ul class="options">
					<li>全部</li>
					<li class="on">奖金派送</li>
				</ul> -->
				<ul class="options" id="selectList">
						<li><p data-value="">全部</p></li>
                        <%
                        EMoneyDetailType [] eMoneyDetails = EMoneyDetailType.values();
			                   for(EMoneyDetailType eMoneyDetail : eMoneyDetails){
			                      if(eMoneyDetail.getIsShow() == 1){
			            %>
			              <li><p data-value='<%=eMoneyDetail.getCode() %>'><%=eMoneyDetail.getDescription() %></p></li>
			            <%
			               }
			             }
			            %>
                 </ul>
			</div>
			<div class="right color-red btn-middle" onclick="moneyDetailPage.getUserBetPage(moneyDetailPage.queryParam,moneyDetailPage.pageParam.pageNo)"><i class="icon-search"></i></div>
		</div>
	</div>
	<!-- main -->
	<div class="main-container"> 
		<ul class="money-detail-list" id="userTeamMoneyDetail">
		</ul>
	</div>
	<!-- main over -->
	
	<div class="user-btn color-gray" id="showMore" style="display:none;">显示更多</div>

	<jsp:include page="/front/include/include.jsp"></jsp:include>
    <script type="text/javascript" src="<%=path%>/js/moneydetail/agent_money_detail.js?v=<%=SystemConfigConstant.mobileWebRsVersion%>"></script>
</body>
</html>