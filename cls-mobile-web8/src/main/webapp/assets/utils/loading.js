function Loading(){}
var loadingPage = new Loading();

/**
 * 开启加载框
 */
Loading.prototype.openLoadingDialog = function(){
	return $.dialog({
		title: '进度提示',
		content: "url:"+contextPath+"/admin/resource/img/loading.gif",
		fixed: true,
		drag: false,
		resize: false,
		fixed: true,
		max:false,
		min:false,
		lock:true,
		time: null,
		cancel: false,
		width: 90,
		height: 'auto'
    });
};

/**
 * 关闭加载框
 */
Loading.prototype.closeLoadingDialog = function(dialogObj){
	if(dialogObj != null){
		dialogObj.close();
	}
};


