function FrontCommonPage(){}
var frontCommonPage = new FrontCommonPage();

/**
 * 公共参数值
 */
frontCommonPage.param = {
	 fixVaue : 3,
	 fixVaue_rechargewithdraw : 2
};

	
/**
 * 打开前台客服弹窗
 */
FrontCommonPage.prototype.showCustomService = function(urlEle){
	var customServiceUrl = $(urlEle).attr("data-val");
	window.location.href=customServiceUrl;

};

/**
 * 判断当前用户是否登录！进行相应的跳转！
 */
function judgeCurrentUserToLogin(url,content){
	// 判断当前登录用户是否为空
	if ($.isEmptyObject(currentUser)) {
		window.location.href = contextPath + "/gameBet/login.html";
		/*
		comfirm("提示", content, "取消", function() {
			return false;
		}, "确认登录", function() {
			window.location.href = contextPath + "/gameBet/login.html";
		});*/

	} else {
		window.location.href = url;
	}
	    
}

/**
 * 根据彩种类型处理期号
 */
FrontCommonPage.prototype.getLotteryExpect = function(lotteryType,expect){
	var lotteryExpect ="";
	
	var isLotteryExpectSpecial = false;
	//投注的彩种旗号是否特殊处理
	if(lotteryType == 'FCSDDPC'  || lotteryType == 'BJKS' || lotteryType == 'BJPK10' || lotteryType == 'XJPLFC' || lotteryType == 'TWWFC' || lotteryType == 'HGFFC'){
		isLotteryExpectSpecial = true;
	}
	
	//旗号特殊处理
	if(isLotteryExpectSpecial){
		lotteryExpect = expect;
	}else if(lotteryType == 'LHC'){
		lotteryExpect = expect.substring(0,4) + "-" + expect.substring(4,expect.length);
	}else{
		lotteryExpect = expect.substring(0,8) + "-" + expect.substring(8,expect.length);
	}
	return lotteryExpect;
};

/**
 * 添加cookid保存 username
 */
FrontCommonPage.prototype.addCookie=function(name,passWord){
	var cookie_name=$.cookie("cookie_name");
	var date = new Date();
	date.setTime(date.getTime()+30*24*60*60*1000);//只能这么写，10表示10秒钟
	if(cookie_name==null){
		$.cookie("cookie_name",name,{ expires:date});
		$.cookie("cookie_passWord",passWord,{ expires:date});
	}else{
		if(cookie_name!=name){
			$.cookie("cookie_name",name,{ expires:date});
			$.cookie("cookie_passWord",passWord,{ expires:date});
		}
	}
}
/**
 * 获取cookid里的用户名
 */
FrontCommonPage.prototype.getCookieName=function(){
	var cookie_name=$.cookie("cookie_name");
	return cookie_name;
}
/**
 * 获取cookid里的用户名ID
 */
FrontCommonPage.prototype.getCookiepassWord=function(){
	var cookie_passWord=$.cookie("cookie_passWord");
	return cookie_passWord;
}
/**
 * 删除当前cookie
 */
FrontCommonPage.prototype.delCookie=function(){
	$.cookie('cookie_name', null);
	$.cookie('cookie_passWord', null);
	window.location.reload();
}