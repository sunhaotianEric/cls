function ActivityDetailPage() {
};
var activityDetailPage = new ActivityDetailPage();


$(document).ready(function() {
	$(".i-activity").parent().addClass("on");

	activityDetailPage.showActivity(id);

	animate_add(".main", "fadeInUp");

});

/**
 * 赋值活动公告信息
 */
ActivityDetailPage.prototype.showActivity = function(id) {
	var jsonData = {
		"id" : id
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/activity/getActivityById",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				$("#titleId").html(result.data.title);
				$("#timeId").html("发布时间:" + result.data.startDateStr);
				var image = result.data.mobileImagePath;
				if (typeof (image) != "undefined" && image != null) {
					$("#contentImgId").attr("src", result.data.mobileImagePath);
				} else {
					$("#contentImgId").parent().hide();
				}
				$("#contentId").html("<p>" + result.data.des + "</p>");
				if (result.data.type == "DAY_CONSUME_ACTIVITY") {
					// 加载日活动配置
					activityDetailPage.refreshDayConsumeConfigs(result.data.dayConsumeActityConfigs, result.data.dayConsumeActityFfcConfigs);
				} else if (result.data.type == "SIGNIN_ACTIVITY") {
					// 加载签到活动配置
					activityDetailPage.refreshSigninConfigConfigs(result.data.signinConfigs);
				}
			} else if (result.code == "error") {
				showTip(result.data);
			}
		}

	});
};

/**
 * 刷新日活动列表数据
 */
ActivityDetailPage.prototype.refreshDayConsumeConfigs = function(dayConsumeActityConfigs, dayConsumeActityFfcConfigs) {

	var activityListObj = $("#activityApplyList");
	activityListObj.html("");

	var str = "";

	// 加载日活动配置信息
	for (var j = 0; j < dayConsumeActityConfigs.length; j++) {
		var dayConsumeActityConfig = dayConsumeActityConfigs[j];
		// 流水返送的状态
		var receiveStatus = dayConsumeActityConfig.receiveStatus;
		var receiveState = null;
		if (receiveStatus == "NOT_APPLY") {
			receiveState = "立即领取";
		} else if (receiveStatus == "APPLIED") {
			receiveState = "已申请";
		} else if (receiveStatus == "RELEASED") {
			receiveState = "已发放"
		} else if (receiveStatus == "NOTALLOWED_APPLY") {
			receiveState = "未达到";
		}

		str += '<div class="child">'
		str += '	<div class="child-title color-red">每日活动</div>'
		str += '	<div class="child-content">'
		str += '	<p>会员昨天投注量达到 <span class="font-yellow">' + dayConsumeActityConfig.lotteryNum + '</span> 可领取<span class="font-red">' + dayConsumeActityConfig.money
				+ '</span></p>'
		str += '	</div>'
		if (receiveStatus == "NOT_APPLY") {
			str += '	<div class="child-btn color-red activity on" data-value="' + dayConsumeActityConfig.money + '" data-type="' + dayConsumeActityConfig.activityType + '">'
					+ receiveState + '</div>'
		} else if (receiveStatus == "APPLIED") {
			str += '	<div class="child-btn color-yellow activity on">' + receiveState + '</div>'
		} else if (receiveStatus == "RELEASED") {
			str += '	<div class="child-btn color-green activity on">' + receiveState + '</div>'
		} else {
			str += '	<div class="child-btn color-gray activity on">' + receiveState + '</div>'
		}
		str += '</div>'
	}
	for (var j = 0; j < dayConsumeActityFfcConfigs.length; j++) {
		var dayConsumeActityConfigsFfc = dayConsumeActityFfcConfigs[j];
		// 获取分分彩流水返送的状态
		var ffcReceiveStatus = dayConsumeActityConfigsFfc.receiveStatus;
		var ffcReceiveState = null;
		if (ffcReceiveStatus == "NOT_APPLY") {
			ffcReceiveState = "立即领取";
		} else if (ffcReceiveStatus == "APPLIED") {
			ffcReceiveState = "已申请";
		} else if (ffcReceiveStatus == "RELEASED") {
			ffcReceiveState = "已发放"
		} else if (ffcReceiveStatus == "NOTALLOWED_APPLY") {
			ffcReceiveState = "未达到";
		}

		str += '<div class="child">';
		str += '	<div class="child-title color-red">分分彩活动</div>';
		str += '	<div class="child-content">';
		str += '	<p>会员分分彩昨天投注量达到 <span class="font-yellow">' + dayConsumeActityConfigsFfc.lotteryNum + '</span> 签到送钱<span class="font-red">' + dayConsumeActityConfigsFfc.money
				+ '</span></p>';
		str += '	</div>'
		if (ffcReceiveStatus == "NOT_APPLY") {
			str += '	<div class="child-btn color-red activity on" data-value="' + dayConsumeActityConfigsFfc.money + '" data-type="' + dayConsumeActityConfigsFfc.activityType
					+ '">' + ffcReceiveState + '</div>';
		} else if (ffcReceiveStatus == "APPLIED") {
			str += '	<div class="child-btn color-yellow activity on">' + ffcReceiveState + '</div>';
		} else if (ffcReceiveStatus == "RELEASED") {
			str += '	<div class="child-btn color-green activity on">' + ffcReceiveState + '</div>';
		} else {
			str += '	<div class="child-btn color-gray activity on">' + ffcReceiveState + '</div>';
		}

		str += '</div>';

	}
	activityListObj.append(str);
	// 给按钮绑定事件.
	activityDetailPage.bindDayConsumeBtn();
	str = "";
	// 每日活动自动显示出来
	$(".child-content").show();
};

/**
 * 刷新签到活动列表数据
 */
ActivityDetailPage.prototype.refreshSigninConfigConfigs = function(signinConfigConfigs) {

	var activityListObj = $("#activityApplyList");
	activityListObj.html("");

	var str = "";
	str += '<div class="bigtitle"><h2>活动内容</h2></div>'
	str += '<div class="sign-item clear">'

	// 加载签到活动配置信息
	for (var j = 0; j < signinConfigConfigs.length; j++) {
		var signinConfigConfig = signinConfigConfigs[j];
		// 获取状态!
		var signState = signinConfigConfig.signState;
		str += '<div class="child">'
		str += '	<div class="child-title">第' + (j + 1) + '天</div>'
		str += '	<div class="child-content">'
		if (signState == "APPLIED") {
			str += '	<p class="por"><img src="' + contextPath + '/images/activity/signed.png" alt=""><span class="money">' + signinConfigConfig.money + '元</span></p>' // 签到是signed.png
			str += '	</div>'
			str += '	<div class="child-btn signEvent signed on" data-value="' + signinConfigConfig.signinDay + '">已签到</div>'// signed已签到按钮背景色,signin点击签到背景色
		} else if (signState == "DISAPPLIED") {
			str += '	<p class="por"><img src="' + contextPath + '/images/activity/sign.png" alt=""><span class="money">' + signinConfigConfig.money + '元</span></p>' // 签到是signed.png
			str += '	</div>'
			str += '	<div class="child-btn signEvent sign on" data-value="' + signinConfigConfig.signinDay + '">未签到</div>'// signed已签到按钮背景色,signin点击签到背景色
		} else if (signState == "SIGNIN") {
			str += '	<p class="por"><img src="' + contextPath + '/images/activity/sign.png" alt=""><span class="money">' + signinConfigConfig.money + '元</span></p>' // 签到是signed.png
			str += '	</div>'
			str += '	<div class="child-btn signEvent sign on" data-value="' + signinConfigConfig.signinDay + '" data-status= "' + signState + '">点击签到</div>'// signed已签到按钮背景色,signin点击签到背景色
		}
		str += '</div>'
	}
	str += '</div>'
	activityListObj.append(str);

	str = "";
	// 每日活动自动显示出来
	$(".child-content").show();
	// 签到活动赠送金金额
	activityDetailPage.bindSigninBtn();

};

// 初始化申请领取按钮
ActivityDetailPage.prototype.bindDayConsumeBtn = function() {
	$(".activity").each(function(i) {
		money = $(this).attr("data-value");
		money = parseInt(money);
		var type = $(this).attr("data-type");
		if (type == "NORMAL") {
			$(this).click(function() {
				$(this).unbind("click");
				// 进行每日活动赠送金申请,领取
				activityDetailPage.dayConsumeActityApply("NORMAL", $(this));
			});
		} else if (type == "FFC") {
			$(this).click(function() {
				$(this).unbind("click");
				// 进行每日活动赠送金申请,领取
				activityDetailPage.dayConsumeActityApply("FFC", $(this));
			});
		}

	});

}

/**
 * 进行每日活动赠送金申请,领取
 */
ActivityDetailPage.prototype.dayConsumeActityApply = function(type, thisBtn) {
	var jsonData = {
		"type" : type,
	}
	$.ajax({
		type : "POST",
		url : contextPath + "/activity/dayConsumeActityApply",
		data : JSON.stringify(jsonData),// 将对象序列化成JSON字符串;
		dataType : "json",// 设置请求参数类型
		contentType : 'application/json;charset=utf-8',// 设置请求头信息;
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				showTip(result.data);
				thisBtn.removeClass('color-red');
				thisBtn.addClass('color-yellow');
				thisBtn.html("已申请");
			} else if (result.code == "error") {
				showTip(result.data);
			}
		}
	});
};

// 初始化签到领取按钮
ActivityDetailPage.prototype.bindSigninBtn = function() {
	$(".signEvent").each(function(i) {
		var signinDay = $(this).attr("data-value");
		signinDay = parseInt(signinDay);
		var signState = $(this).attr("data-status");
		if (signState == "SIGNIN") {
			$(this).unbind("click");
			$(this).addClass("signin");
			$(this).removeClass("signed");

			$(this).click(function() {
				// 进行签到活动赠送金申请,领取
				activityDetailPage.signinApply(signinDay, $(this));
			});
		}

	});
}

/**
 * 进行签到活动赠送金申请,领取
 */
ActivityDetailPage.prototype.signinApply = function(signinDay, thisBtn) {
	var jsonData = {
		"continueDay" : signinDay
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/activity/signinApply",
		data : JSON.stringify(jsonData),// 将对象序列化成JSON字符串;
		dataType : "json",// 设置请求参数类型
		contentType : 'application/json;charset=utf-8',// 设置请求头信息;
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				showTip(result.data)
				thisBtn.html("已签到");
				thisBtn.unbind("click");
				thisBtn.removeClass("signin");
				thisBtn.addClass("signed");
				thisBtn.parent().find('.por img').attr('src', contextPath + '/images/activity/signed.png');

			} else {
				if (result.code == "error") {
					showTip(result.data)
					thisBtn.unbind("click");
					thisBtn.click(function() {
						// 进行签到活动赠送金申请,领取
						activityDetailPage.signinApply(signinDay, thisBtn);
					});
				}
			}
		}
	})
};
