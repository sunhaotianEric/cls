function ActivityPage() {
}
var activityPage = new ActivityPage();

activityPage.queryParam = {

};

$(document).ready(function() {
	$(".i-activity").parent().addClass("on");
	activityPage.getAllActivitys();

});

/**
 * 加载所有的活动中心
 */

ActivityPage.prototype.getAllActivitys = function() {
	// 使用ajax请求数据
	$.ajax({
		type : "POST",
		contentType : 'application/json;charset=utf-8',//设置请求头信息;
		url : contextPath + "/activity/getAllActivity",
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				activityPage.refreshActivityPages(result.data);
			} else if (result.code == "error") {
				showTip("未查询到活动信息!");
			}
		}
	})
};

/**
 * 刷新列表数据
 */
ActivityPage.prototype.refreshActivityPages = function(activitys) {
	var activityListObj = $("#activityList");

	activityListObj.html("");
	var str = "";

	if (activitys.length == 0) {
		str += '<div class="line">';
		str += '<h4 class="nodata">暂无数据</h4>';
		str += '</div>';
		activityListObj.append(str);
	} else {
		// 记录数据显示
		for (var i = 0; i < activitys.length; i++) {
			var activity = activitys[i];
			var title = activity.title;
//			var des = activity.des;
			// 标题过长进行截取
			if (title.length > 18) {
				title = title.substr(0, 18) + "...";
			}
//			des = des.replace(/<[^>]+>/g, "");
//			des = des.replace(/&nbsp;/g, " ");
//			des = $.trim(des);
//			// 简介过长进行截取
//			if (des.length > 18) {
//				des = des.substr(0, 25) + "...";
//			}
			var image = "icon1.png";

			var index = i % 3;

			if (index == 0) {
				image = "icon1.png";
			} else if (index == 1) {
				image = "icon2.png";
			} else if (index == 2) {
				image = "icon3.png";
			}

			str += '  <a href="' + contextPath + '/gameBet/activityDetail.html?id=' + activity.id + '">'
			str += '    <li class="main-child animated">'
			str += '      <div class="child-content">'
			str += '          <h2 class="title">' + title + '</h2>'
			str += '          <p class="time">活动时间：' + activity.createDateStr + "--" + activity.endDateStr + '</p>'
			str += '          <div class="thumb"><img src="' + activity.mobileImagePath + '" alt=""></div>'
//			str += '          <div class="info">'
//			str += '              <p>' + des + '</p>'
//			str += '          </div>'
			str += '      </div>'
			str += '      <div class="child-more"><span>查看详情</span><img src="' + contextPath + '/images/icon/icon-pointer.png" alt=""></div>'
			str += '    </li>'
			str += '  </a>'

			activityListObj.append(str);
			str = "";
		}
		animate_add(".main-child", "fadeInUp");
	}
};
