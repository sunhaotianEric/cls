function ChooseWayPage() {

}

var chooseWayPage = new ChooseWayPage();

chooseWayPage.param = {
		
};
$(document).ready(function() {
	chooseWayPage.initPage();
	$(".quc-service").unbind("mouseover").mouseover(function() {
		$(this).addClass("quc-service-open")
	});
	$(".quc-service").unbind("mouseout").mouseout(function() {
		$(this).removeClass("quc-service-open")
	});
	$("#nextButton").click(function() {
		chooseWayPage.nextPage()
	});
	$("body").keydown(function(event) {
		var curKey = event.which;
		if (curKey == "13") {
			chooseWayPage.nextPage()
		}
	})
	
});

ChooseWayPage.prototype.initPage = function() {
	var username = chooseWayPage.param.id;
	var jsonData = {"username" : username}
	$.ajax({
		type : "POST",
		url : contextPath + "/forgetpwd/loadfindPwdWay",
		data : JSON.stringify(jsonData),// 将对象序列化成JSON字符串;
		dataType : "json",// 设置请求参数类型
		contentType : 'application/json;charset=utf-8',// 设置请求头信息;
		success : function(result) {
			commonHandlerResult(result, this.url);
			if(result.code == "ok"){
				var findWayListObj = $("#find_way_list");
				findWayListObj.html("");
				var str = "";
				var qCount = result.data;
				var email = result.data2;
				var phone=result.data3;
				if (email != null && email != ""){
					str += "    <div class='radio'>";
					str += "        <input type='radio' name='secType' value='email' >";
					str += "		<div class='radioCheck'></div>";
					str += "        <span >选择通过邮箱来找回密码</span>";
					str += "    </div>";
				}
				if (qCount > 0) {
					str += "    <div class='radio'>";
					str += "        <input type='radio' name='secType' value='mQ' >";
					str += "		<div class='radioCheck'></div>";
					str += "        <span >选择密保问题来找回密码</span>";
					str += "    </div>";
				}
				if (phone != null && phone != "") {
					str += "    <div class='radio'>";
					str += "        <input type='radio' name='secType' value='phone' >";
					str += "		<div class='radioCheck'></div>";
					str += "        <span >选择通过手机来找回密码</span>";
					str += "    </div>";
				}
				str += "    <div class='radio'>";
				str += "        <input type='radio' name='secType' value='kefu' >";
				str += "		<div class='radioCheck'></div>";
				str += "        <span >通过在线客服找回密码</span>";
				str += "    </div>";
				findWayListObj.append(str);
			}else if(result.code == "error"){
				showTip(result.data);
			}
		}
	});
};
ChooseWayPage.prototype.nextPage = function() {
	$("#nextButton").unbind("click");
	var secType = $('input[name="secType"]:checked').val();
	if (secType == "email"){
		window.location.href = contextPath
				+ "/gameBet/forgetPwd/verifyWay.html?type=email"
	}else if (secType == "mQ") {
		window.location.href = contextPath
				+ "/gameBet/forgetPwd/verifyWay.html?type=question"
	}else if(secType == "phone"){
		window.location.href = contextPath
				+ "/gameBet/forgetPwd/verifyWay.html?type=phone"
	} else if(secType == "kefu") {
		window.location.href = customUrl;
	} else {
		if (secType == "mE") {
			showTip("未配置");
			chooseWayPage.resetNextButton();
			return
		} else {
			showTip("请选择找回密码方式");
			chooseWayPage.resetNextButton();
			return
		}
	}
};
ChooseWayPage.prototype.resetNextButton = function() {
	$("#nextButton").click(function() {
		chooseWayPage.nextPage()
	})
};