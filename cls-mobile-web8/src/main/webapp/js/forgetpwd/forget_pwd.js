function ForgetPwdPage() {
	
}
var forgetPwdPage = new ForgetPwdPage();

forgetPwdPage.param = {
		
};

/**
 * 初始化页面.
 */
$(document).ready(function() {
    $("#username").unbind("blur").blur(function() {
        var username = $("#username").val();
        if (username == null || $.trim(username) == "") {
            showTip("请输入用户名");
        } 
    });
	
    $("#verifyCode").unbind("blur").blur(function() {
        var verifyCode = $("#verifyCode").val();
        if (verifyCode == null || $.trim(verifyCode) == "") {
            showTip("请输入验证码");
        }
    });
    $("#nextButton").click(function() {
    	forgetPwdPage.nextPage()
    });
    $("body").keydown(function(event) {
        var curKey = event.which;
        if (curKey == "13") {
        	forgetPwdPage.nextPage()
        }
    })
});

ForgetPwdPage.prototype.initPage = function() {
	
};

ForgetPwdPage.prototype.nextPage = function() {
    $("#nextButton").unbind("click");
    var userName = $("#username").val();
    var verifyCode = $("#verifyCode").val();
    if (userName == null || userName == "") {
        showTip("请输入用户名");
        forgetPwdPage.resetNextButton();
        return
    }
    if (verifyCode == null || verifyCode == "") {
    	showTip("请输入验证码");
    	forgetPwdPage.resetNextButton();
        return
    }
    var user = {};
    user.userName = userName;
    user.checkCode = verifyCode;
    
    $.ajax({
		type : "POST",
		url : contextPath + "/forgetpwd/forgetPwdNext",
		data : JSON.stringify(user),// 将对象序列化成JSON字符串;
		dataType : "json",// 设置请求参数类型
		contentType : 'application/json;charset=utf-8',// 设置请求头信息;
		success : function(result) {
			commonHandlerResult(result, this.url);
			if(result.code == "ok"){
				 window.location.href = contextPath + "/gameBet/forgetPwd/chooseWay.html"
			}else{
				 if (result.code == "error") {
		                showTip(result.data);
		                forgetPwdPage.resetNextButton()
		            }else {
		                showTip("数据请求失败，请重试");
		                forgetPwdPage.resetNextButton()
		            }
			}
		}
	});
};

ForgetPwdPage.prototype.resetNextButton = function() {
    $("#nextButton").click(function() {
    	forgetPwdPage.nextPage()
    })
};