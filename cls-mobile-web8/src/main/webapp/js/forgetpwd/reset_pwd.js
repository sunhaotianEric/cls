function ResetPwdPage() {
	
}

var resetPwdPage = new ResetPwdPage();

resetPwdPage.param = {
		
};

/**
 * 初始化页面
 */
$(document).ready(function() {
	
	resetPwdPage.initPage();
	$(".quc-service").unbind("mouseover").mouseover(function() {
		$(this).addClass("quc-service-open")
	});
	$(".quc-service").unbind("mouseout").mouseout(function() {
		$(this).removeClass("quc-service-open")
	});
	$("#password").unbind("focus").focus(function() {
		$("#password_field").css("-webkit-box-shadow","");
		$("#password_tip").hide()
	});
	$("#password").unbind("blur").blur(function() {
		var password = $("#password").val();
		if (password == null || $.trim(password) == "") {
			$("#password_field").css("-webkit-box-shadow","0px 0 8px red");
			showTip("请输入新密码");
		}
	});
	$("#comfirmPassword").unbind("focus").focus(function() {
		$("#comfirmPassword_field").css("-webkit-box-shadow","");
		$("#comfirmPassword_tip").hide()
	});
	$("#comfirmPassword").unbind("blur").blur(function() {
		var password = $("#password").val();
		if (password == null || $.trim(password) == "") {
			$("#comfirmPassword_field").css("-webkit-box-shadow","0px 0 8px red");
			showTip("请输入确认密码");
		}
	});
	$("#nextButton").click(function() {
		resetPwdPage.nextPage()
	});
	$("body").keydown(function(event) {
		var curKey = event.which;
		if (curKey == "13") {
			resetPwdPage.nextPage()
		}
	})
});

ResetPwdPage.prototype.initPage = function() {
	
};

ResetPwdPage.prototype.nextPage = function() {
	$("#nextButton").unbind("click");
	var password = $("#password").val();
	var comfirmPassword = $("#comfirmPassword").val();
	if (password == null || password == "") {
		$("#password_field").css("-webkit-box-shadow","0px 0 8px red");
		showTip("请输入新密码");
		resetPwdPage.resetNextButton();
		return
	} else {
		if (password.length < 6 || password.length > 15) {
			$("#password_field").css("-webkit-box-shadow","0px 0 8px red");
			showTip("密码必须为6-15个字符");
			resetPwdPage.resetNextButton();
			return
		}
	}
	if (password != comfirmPassword) {
		$("#password_field").css("-webkit-box-shadow","0px 0 8px red");
		showTip("新密码和确认密码不一致");
		$("#comfirmPassword_field").css("-webkit-box-shadow","0px 0 8px red");
		showTip("新密码和确认密码不一致");
		resetPwdPage.resetNextButton();
		return
	}
	// 后端需要传输的参数.
	var user = {};
	user.password = password;
	user.surePassword = comfirmPassword;
	user.userName = resetPwdPage.param.id;
	var jsonData = {
		"user" : user,
	};
	// 向后端发送请求.
	$.ajax({
		type : "POST",
		// 请求路径.
		url : contextPath + "/forgetpwd/resetPwd",
		// 请求参数.
		data : JSON.stringify(jsonData),// 将对象序列化成JSON字符串;
		dataType : "json",// 设置请求参数类型
		contentType : 'application/json;charset=utf-8',// 设置请求头信息;
		// 请求成功返回.
		success : function(result) {
			commonHandlerResult(result, this.url);
			if(result.code == "ok"){
				showTip("设置成功...3秒后自动跳转到登陆页面...");
				setTimeout("window.location.href= contextPath + '/gameBet/login.html'",3000);
			}else if (result.code == "error") {
				showTip("未通过安全校验，请重试");
				resetPwdPage.resetNextButton();
			}
		}
	});
};

ResetPwdPage.prototype.resetNextButton = function() {
	$("#nextButton").click(function() {
		resetPwdPage.nextPage()
	})
};