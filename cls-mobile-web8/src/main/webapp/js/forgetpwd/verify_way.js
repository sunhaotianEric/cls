function VerifyWayPage() {
	
}

var verifyWayPage = new VerifyWayPage();

verifyWayPage.param = {

};

/**
 * 初始化页面
 */
$(document).ready(function() {
	verifyWayPage.initPage();
	$(".quc-service").unbind("mouseover").mouseover(function() {
		$(this).addClass("quc-service-open")
	});
	$(".quc-service").unbind("mouseout").mouseout(function() {
		$(this).removeClass("quc-service-open")
	});
	$("#answer").unbind("focus").focus(function() {
		$("#answer_field").css("-webkit-box-shadow","");
		$("#answer_tip").hide();
	});
	$("#answer").unbind("blur").blur(function() {
		var answer = $("#answer").val();
		if (answer == null || $.trim(answer) == "") {
			$("#answer_field").css("-webkit-box-shadow","0px 0 8px red");
			/*
			 * $("#answer_tip").show(); $("#answer_tip").text("请输入答案")
			 */
			showTip("请输入答案");
		}
	});
	$("#verifyCode").unbind("focus").focus(function() {
		$("#verifyCode_field").css("-webkit-box-shadow","");
		$("#verifyCode_tip").hide()
	});
	$("#verifyCode").unbind("blur").blur(function() {
		var verifyCode = $("#verifyCode").val();
		if (verifyCode == null || $.trim(verifyCode) == "") {
			$("#verifyCode_field").css("-webkit-box-shadow","0px 0 8px red");
			$("#verifyCode_tip").show()
		}
	});
	$("#nextButton").click(function() {
		verifyWayPage.nextPage()
	});
	$("body").keydown(function(event) {
		var curKey = event.which;
		if (curKey == "13") {
			verifyWayPage.nextPage()
		}
	});
	
	// 手机安全验证-找回密码
	$('#btn-sendnum').on('click', function(){
		verifyWayPage.sendTextMessages();
	});
	
	//
	$('#btn-sendEmailNum').on('click', function(){
		verifyWayPage.sendEmailCode();
	});
});

/**
 * 初始化页面
 */
VerifyWayPage.prototype.initPage = function() {
	var username = verifyWayPage.param.id;
	var jsonData = {"userName" : username};
	if(verifyWayPage.param.type == "question") {
	$.ajax({
		type : "POST",
		url : contextPath + "/forgetpwd/loadQuestions",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if(result.code == "ok"){
				var questions = result.data;
				if (questions == null || questions.length == 0) {
					showTip("数据有误,请重试");
				} else {
					verifyWayPage.param.questions = questions;
					$("#question").val(questions[0].question);
					$("#qType").val(questions[0].type)
					$("#question1").val(questions[1].question);
					$("#qType1").val(questions[1].type)
				}
			}else if (result.code == "error") {
				showTip("用户名有误,请重试");
			}
		}
	});
   }
};

/**
 * 页面格式展示,通过type判断.
 */
VerifyWayPage.prototype.nextPage = function() {
	$("#nextButton").unbind("click");
	if(verifyWayPage.param.type=="question"){
		var answer = $("#answer").val();
		var type = $("#qType").val();
		var answer1 = $("#answer1").val();
		var type1 = $("#qType1").val();
		var verifyCode = $("#verifyCode").val();
		
		//校验参数是否合法.
		if (answer == null || answer == "") {
			$("#answer_field").css("-webkit-box-shadow","0px 0 8px red");
			showTip("请输入答案");
			verifyWayPage.resetNextButton();
			return
		} else {
			if (answer.length < 1 || answer.length > 30) {
				$("#answer_field").css("-webkit-box-shadow","0px 0 8px red");
				showTip("答案必须为1-30个字符");
				verifyWayPage.resetNextButton();
				return
			}
		}
		if (answer1 == null || answer1 == "") {
			$("#answer_field").css("-webkit-box-shadow","0px 0 8px red");
			showTip("请输入答案");
			verifyWayPage.resetNextButton();
			return
		} else {
			if (answer1.length < 1 || answer1.length > 30) {
				$("#answer_field").css("-webkit-box-shadow","0px 0 8px red");
				showTip("答案必须为1-30个字符");
				verifyWayPage.resetNextButton();
				return
			}
		}
		if (verifyCode == null || verifyCode == "" || verifyCode.length > 4) {
			$("#verifyCode_field").css("-webkit-box-shadow","0px 0 8px red");
			showTip("验证码有误");
			verifyWayPage.resetNextButton();
			return
		}
		
		//封装参数.
		var userQuestionList = new Array();
		var userQuestion = {};
		var userQuestion2 = {};
		userQuestion.type = type;
		userQuestion.answer = answer;
		userQuestion2.type = type1;
		userQuestion2.answer = answer1;
		userQuestionList.push(userQuestion);
		userQuestionList.push(userQuestion2);
		var jsonData = {
			"questionList" : userQuestionList,
			"verifyCode" : verifyCode,
			"username" : verifyWayPage.param.id
			
		};
		$.ajax({
			type : "POST",
			url : contextPath + "/forgetpwd/verifyQuestion",
			data : JSON.stringify(jsonData),
			dataType : "json",// 设置请求参数类型
			contentType : 'application/json;charset=utf-8',
			success : function(result) {
				commonHandlerResult(result, this.url);
				if(result.code != null && result.code == "ok"){
					window.location.href = contextPath + "/gameBet/forgetPwd/resetPwd.html"
				}else if(result.code == "error"){
					if (result.code != null && result.code == "error") {
						showTip(result.data);
						verifyWayPage.resetNextButton()
					} else {
						showTip("操作失败，请重试");
						verifyWayPage.resetNextButton()
					}
					checkCodeRefresh();
				}
			}
		});
	}else if(verifyWayPage.param.type=="phone"){
		//获取参数值.
		var vcode = $("#mobile_verifyCode").val();
		var mobile = $("#mobile").val();
		//校验参数格式是否错误.
		if(vcode==null||vcode==""){
			showTip("验证码不能为空");
			verifyWayPage.resetNextButton()
			return;
		}
		//封装参数.
		var jsonData = {
						"vcode" : vcode,
						"userName":verifyWayPage.param.id,
						"phone":mobile
						};
		$.ajax({
			type : "POST",
			url : contextPath + "/forgetpwd/verifyPhone",
			data : JSON.stringify(jsonData),// 将对象序列化成JSON字符串;
			dataType : "json",// 设置请求参数类型
			contentType : 'application/json;charset=utf-8',// 设置请求头信息;
			success : function(result) {
				commonHandlerResult(result, this.url);
				if(result.code == "ok"){
					window.location.href = contextPath+ "/gameBet/forgetPwd/resetPwd.html"
				}else if(result.code == "error"){
					showTip(result.data);
					verifyWayPage.resetNextButton()
				}
				checkCodeRefresh()
			}
		})
	} else if (verifyWayPage.param.type=="email") {
		// 获取参数值.
		var email = $("#email").val();
		var emailVerifyCode = $("#email_verifyCode").val();
		// 校验参数是否为空.
		if(emailVerifyCode==null || emailVerifyCode==""){
			showTip("邮箱不能为空");
			verifyWayPage.resetNextButton()
			return;
		}
		if(email == null || email == ""){
			showTip("邮箱不能为空");
			verifyWayPage.resetNextButton()
			return;
		}
		
		// 封装参数.
		var jsonData = {
			"email" : email,
			"emailCode" : emailVerifyCode,
			"userName" : verifyWayPage.param.id,
		};
		
		$.ajax({
			type : "POST",
			url : contextPath + "/forgetpwd/checkEmailCode",
			data : JSON.stringify(jsonData),// 将对象序列化成JSON字符串;
			dataType : "json",// 设置请求参数类型
			contentType : 'application/json;charset=utf-8',// 设置请求头信息;
			success : function(result) {
				commonHandlerResult(result, this.url);
				if(result.code == "ok"){
					window.location.href = contextPath+ "/gameBet/forgetPwd/resetPwd.html"
				}else if(result.code == "error"){
					showTip(result.data);
					verifyWayPage.resetNextButton()
				}
				checkCodeRefresh()
			}
		})
		
		
	}
};
VerifyWayPage.prototype.resetNextButton = function() {
	$("#nextButton").click(function() {
		verifyWayPage.nextPage()
	})
};

/**
 * 发送手机验证码.
 */
VerifyWayPage.prototype.sendTextMessages=function(){
	var mobile=$("#mobile").val();
	if(mobile==null||mobile==""){
		showTip("手机号不能空");
		return;
	}
	var jsonData = {"mobile": mobile};
	// 发送验证码请求
	$.ajax({
		type : "POST",
		url : contextPath + "/forgetpwd/sendSmsVerifycode",
		data : JSON.stringify(jsonData),// 将对象序列化成JSON字符串;
		dataType : "json",// 设置请求参数类型
		contentType : 'application/json;charset=utf-8',// 设置请求头信息;
		success : function(result) {
			commonHandlerResult(result, this.url);
			if(result.code == "ok"){
				showTip("短信已发送，请注意查收");
				$(this).hide();
				$('.mobile-verify-time').show();
				$('#mobile_verifyCode_field').show();
				
				// 手机验证码倒计时60秒
				var i = 60;
				var interval = setInterval(function(){
					i = i - 1;
					$('.mobile-verify-time .countdown').text(i);
					if(i == 0){
						clearInterval(interval);
						$('.mobile-verify-time').hide();
						$('#btn-sendnum').show();
					}
				},1000)
			}else if(result.code == "error"){
				showTip(result.data);
				return;
			}
		}
	});
}

/**
 * 发送邮箱验证码
 */
VerifyWayPage.prototype.sendEmailCode = function() {
	var email=$("#email").val();
	if(email==null||email==""){
		showTip("邮箱账号不能为空!");
		return;
	}
	
	var jsonData = {
		"email" : email,
		"userName" : verifyWayPage.param.id,
	};
	
	$(this).hide();
	$('.email-verify-time').show();
	$('#email_verifyCode_field').show();
	
	// 手机验证码倒计时60秒
	var i = 60;
	var interval = setInterval(function(){
		i = i - 1;
		$('.email-verify-time .countdownEmail').text(i);
		if(i == 0){
			clearInterval(interval);
			$('.email-verify-time').hide();
			$('#btn-sendEmailNum').show();
		}
	},1000)
	// 发送邮箱验证码.
	$.ajax({
		type : "POST",
		url : contextPath + "/forgetpwd/sendEmailCode",
		data : JSON.stringify(jsonData),// 将对象序列化成JSON字符串;
		dataType : "json",// 设置请求参数类型
		contentType : 'application/json;charset=utf-8',// 设置请求头信息;
		success : function(result) {
			commonHandlerResult(result, this.url);
			if(result.code == "ok"){
				showTip(result.data);
			}else if(result.code == "error"){
				showTip(result.data);
				return;
			}
		}
	});
	
}