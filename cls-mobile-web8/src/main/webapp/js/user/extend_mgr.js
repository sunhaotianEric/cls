/*
 * 推广管理
 */
function ExtendMgrPage() {
}
var extendMgrPage = new ExtendMgrPage();
var quotaMap= new JS_OBJECT_MAP();
extendMgrPage.param = {
	linkList : null,
	deleteLinkId : null
// 当前用户的注册列表
};

/**
 * 查询参数
 */
extendMgrPage.queryParam = {
	userId : null
};


extendMgrPage.sscParam = {
	    sscInterval : null,
	    sscOpenUserHighest : null,
	    sscOpenUserLowest : null,
	    sscOpenUserHighestRebateValue : null,
	    sscOpenUserLowestRebateValue : null
	};

extendMgrPage.syxwParam = {
		syxwInterval : null,
		syxwOpenUserHighest : null,
		syxwOpenUserLowest : null,
		syxwHighestAwardModel : null
};

extendMgrPage.dpcParam = {
		dpcInterval : null,
		dpcOpenUserHighest : null,
		dpcOpenUserLowest : null,
		dpcHighestAwardModel : null
};

extendMgrPage.pk10Param = {
		pk10Interval : null,
		pk10OpenUserHighest : null,
		pk10OpenUserLowest : null,
		pk10HighestAwardModel : null
};

extendMgrPage.ksParam = {
		ksInterval : null,
		ksOpenUserHighest : null,
		ksOpenUserLowest : null,
		ksHighestAwardModel : null
};
/**
 * 添加参数
 */
extendMgrPage.addParam = {
	linkDes : null,
	domainId : null,
	continueDay : null,
	whetherZongdai : null,
	bonusScale : null,
	resisterDonateMoney : null,
	resisterDonatePoint : null,
	sscRebateValue : null,
	ffcRebateValue : null,
	syxwRebateValue : null,
	ksRebateValue : null,
	klsfRebateValue : null,
	dpcRebateValue : null,
	sscrebate : null,
	ffcrebate : null,
	syxwrebate : null,
	ksrebate : null,
	klsfrebate : null,
	dpcrebate : null,
	pk10Rebate:null
};


//分页参数
extendMgrPage.pageParam = {
        queryMethodParam : null,
        pageNo : 1,
        pageMaxNo : 1,  //最大的页数
        skipHtmlStr : ""
};

$(document).ready(function() {
	extendMgrPage.sscParam.sscOpenUserHighest=currentUser.sscRebate;
	extendMgrPage.sscParam.sscInterval = 2;
	extendMgrPage.syxwParam.syxwOpenUserHighest=currentUser.syxwRebate;
	extendMgrPage.syxwParam.syxwInterval = 2;
	extendMgrPage.dpcParam.dpcOpenUserHighest=currentUser.dpcRebate;
	extendMgrPage.dpcParam.dpcInterval = 2;
	extendMgrPage.pk10Param.pk10OpenUserHighest=currentUser.pk10Rebate;
	extendMgrPage.pk10Param.pk10Interval = 2;
	extendMgrPage.ksParam.ksOpenUserHighest=currentUser.ksRebate;
	extendMgrPage.ksParam.ksInterval = 2;
	
	extendMgrPage.addParam.sscrebate = extendMgrPage.sscParam.sscOpenUserHighest;
	extendMgrPage.addParam.sscrebate = extendMgrPage.addParam.sscrebate-2 ;
	extendMgrPage.addParam.ksRebate = extendMgrPage.ksParam.ksOpenUserHighest;
	extendMgrPage.addParam.ksRebate = extendMgrPage.addParam.ksRebate-2;
	extendMgrPage.addParam.syxwRebate = extendMgrPage.syxwParam.syxwOpenUserHighest;
	extendMgrPage.addParam.syxwRebate = extendMgrPage.addParam.syxwRebate-2;
	extendMgrPage.addParam.pk10Rebate = extendMgrPage.pk10Param.pk10OpenUserHighest;
	extendMgrPage.addParam.pk10Rebate = extendMgrPage.addParam.pk10Rebate-2;
	extendMgrPage.addParam.dpcRebate = extendMgrPage.dpcParam.dpcOpenUserHighest;
	extendMgrPage.addParam.dpcRebate = extendMgrPage.addParam.dpcRebate-2;
	$("#sscrebateValue").val(extendMgrPage.addParam.sscrebate);
	$("#ksrebateValue").val(extendMgrPage.addParam.ksRebate);
	$("#syxwrebateValue").val(extendMgrPage.addParam.syxwRebate);
	$("#pk10rebateValue").val(extendMgrPage.addParam.pk10Rebate);
	$("#dpcrebateValue").val(extendMgrPage.addParam.dpcRebate);
	
	//时时彩模式事件
	$("#sscModeAdd").unbind("click").click(function() {
		var sscrebateValue = Number($("#sscrebateValue").val());
		if(sscrebateValue%2!=0){
			showTip("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		extendMgrPage.addParam.sscRebate = sscrebateValue;
		if(extendMgrPage.addParam.sscRebate > extendMgrPage.sscParam.sscOpenUserHighest){
			showTip("大于时时彩模式["+ extendMgrPage.sscParam.sscOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(extendMgrPage.addParam.sscRebate < extendMgrPage.sscParam.sscOpenUserLowest){
			showTip("小于时时彩模式["+ extendMgrPage.sscParam.sscOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		if(extendMgrPage.addParam.sscRebate == extendMgrPage.sscParam.sscOpenUserHighest){
			showTip("当前时时彩模式已经是最高值了.");
			return;
		}
		
		extendMgrPage.addParam.sscrebate += extendMgrPage.sscParam.sscInterval; 
		$("#sscrebateValue").val(extendMgrPage.addParam.sscrebate);
		extendMgrPage.showRemainNumBySscRebate(extendMgrPage.addParam.sscrebate);
		$("#sscTip").text('');
		$("#sscTip").text("返点"+extendMgrPage.calculateRebateByAwardModelInterval(extendMgrPage.sscParam.sscOpenUserHighest, extendMgrPage.addParam.sscRebate, extendMgrPage.sscParam.sscHighestAwardModel)+"%");
	});
	$("#sscModeReduction").unbind("click").click(function() {
		var sscrebateValue = Number($("#sscrebateValue").val());
		if(sscrebateValue%2!=0){
			showTip("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		extendMgrPage.addParam.sscRebate = sscrebateValue;
		if(extendMgrPage.addParam.sscRebate > extendMgrPage.sscParam.sscOpenUserHighest){
			showTip("大于时时彩模式最高值了.请重新输入！");
			return;
		}
		if(extendMgrPage.addParam.sscRebate < extendMgrPage.sscParam.sscOpenUserLowest){
			showTip("小于时时彩模式最低值了.请重新输入！");
			return;
		}
		if(extendMgrPage.addParam.sscRebate == extendMgrPage.sscParam.sscOpenUserLowest){
			showTip("当前时时彩模式已经是最低值了.");
			return;
		}
		extendMgrPage.addParam.sscrebate -= extendMgrPage.sscParam.sscInterval; 
		$("#sscrebateValue").val(extendMgrPage.addParam.sscrebate);
		extendMgrPage.showRemainNumBySscRebate(extendMgrPage.addParam.sscrebate);
		$("#sscTip").text('');
		$("#sscTip").text("返点"+extendMgrPage.calculateRebateByAwardModelInterval(extendMgrPage.sscParam.sscOpenUserHighest, extendMgrPage.addParam.sscRebate, extendMgrPage.sscParam.sscHighestAwardModel)+"%");
	});
	
	//快三模式事件
	$("#ksModeAdd").unbind("click").click(function() {
		var ksrebateValue = Number($("#ksrebateValue").val());
		if(ksrebateValue%2!=0){
			showTip("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		extendMgrPage.addParam.ksRebate = ksrebateValue;
		if(extendMgrPage.addParam.ksRebate > extendMgrPage.ksParam.ksOpenUserHighest){
			showTip("大于快三模式["+ extendMgrPage.ksParam.ksOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(extendMgrPage.addParam.ksRebate < extendMgrPage.ksParam.ksOpenUserLowest){
			showTip("小于快三模式["+ extendMgrPage.ksParam.ksOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		if(extendMgrPage.addParam.ksRebate == extendMgrPage.ksParam.ksOpenUserHighest){
			showTip("当前快三模式已经是最高值了.");
			return;
		}
		
		extendMgrPage.addParam.ksRebate += extendMgrPage.ksParam.ksInterval; 
		$("#ksrebateValue").val(extendMgrPage.addParam.ksRebate);
		$("#ksTip").text('');
		$("#ksTip").text("返点"+extendMgrPage.calculateRebateByAwardModelInterval(extendMgrPage.ksParam.ksOpenUserHighest, extendMgrPage.addParam.ksRebate, extendMgrPage.ksParam.ksHighestAwardModel)+"%");
	});
	
	$("#ksModeReduction").unbind("click").click(function() {
		var ksrebateValue = Number($("#ksrebateValue").val());
		if(ksrebateValue%2!=0){
			showTip("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		extendMgrPage.addParam.ksRebate = ksrebateValue;
		if(extendMgrPage.addParam.ksRebate > extendMgrPage.ksParam.ksOpenUserHighest){
			showTip("大于快三模式最高值了.请重新输入！");
			return;
		}
		if(extendMgrPage.addParam.ksRebate < extendMgrPage.ksParam.ksOpenUserLowest){
			showTip("小于快三模式最低值了.请重新输入！");
			return;
		}
		
		
		extendMgrPage.addParam.ksRebate -= extendMgrPage.ksParam.ksInterval;
		$("#ksrebateValue").val(extendMgrPage.addParam.ksRebate);
		$("#ksTip").text('');
		$("#ksTip").text("返点"+extendMgrPage.calculateRebateByAwardModelInterval(extendMgrPage.ksParam.ksOpenUserHighest, extendMgrPage.addParam.ksRebate, extendMgrPage.ksParam.ksHighestAwardModel)+"%");
	});
	
	//十一选五模式事件
	$("#syxwModeAdd").unbind("click").click(function() {
		var syxwrebateValue = Number($("#syxwrebateValue").val());
		/*if(syxwrebateValue%2!=0){
			showTip("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}*/
		extendMgrPage.addParam.syxwRebate = syxwrebateValue;
		if(extendMgrPage.addParam.syxwRebate > extendMgrPage.syxwParam.syxwOpenUserHighest){
			showTip("大于十一选五模式["+ extendMgrPage.syxwParam.syxwOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(extendMgrPage.addParam.syxwRebate < extendMgrPage.syxwParam.syxwOpenUserLowest){
			showTip("小于十一选五模式["+ extendMgrPage.syxwParam.syxwOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		if(extendMgrPage.addParam.syxwRebate == extendMgrPage.syxwParam.syxwOpenUserHighest){
			showTip("当前十一选五模式已经是最高值了.");
			return;
		}
		
		extendMgrPage.addParam.syxwRebate += extendMgrPage.syxwParam.syxwInterval; 
		$("#syxwrebateValue").val(extendMgrPage.addParam.syxwRebate);
		$("#syxwTip").text('');
		$("#syxwTip").text("返点"+extendMgrPage.calculateRebateByAwardModelInterval(extendMgrPage.syxwParam.syxwOpenUserHighest, extendMgrPage.addParam.syxwRebate, extendMgrPage.syxwParam.syxwHighestAwardModel)+"%");
	});
	
	$("#syxwModeReduction").unbind("click").click(function() {
		var syxwrebateValue = Number($("#syxwrebateValue").val());
		/*if(syxwrebateValue%2!=0){
			showTip("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}*/
		extendMgrPage.addParam.syxwRebate = syxwrebateValue;
		if(extendMgrPage.addParam.syxwRebate > extendMgrPage.syxwParam.syxwOpenUserHighest){
			showTip("大于十一选五模式最高值了.请重新输入！");
			return;
		}
		if(extendMgrPage.addParam.syxwRebate < extendMgrPage.syxwParam.syxwOpenUserLowest){
			showTip("小于十一选五模式最低值了.请重新输入！");
			return;
		}
		if(extendMgrPage.addParam.syxwRebate == extendMgrPage.syxwParam.syxwOpenUserLowest){
			showTip("当前十一选五模式已经是最低值了.");
			return;
		}
		
		extendMgrPage.addParam.syxwRebate -= extendMgrPage.syxwParam.syxwInterval;
		$("#syxwrebateValue").val(extendMgrPage.addParam.syxwRebate);
		$("#syxwTip").text('');
		$("#syxwTip").text("返点"+extendMgrPage.calculateRebateByAwardModelInterval(extendMgrPage.syxwParam.syxwOpenUserHighest, extendMgrPage.addParam.syxwRebate, extendMgrPage.syxwParam.syxwHighestAwardModel)+"%");
	});
	
	//pk10模式事件
	$("#pk10ModeAdd").unbind("click").click(function() {
		var pk10rebateValue = Number($("#pk10rebateValue").val());
		if(pk10rebateValue%2!=0){
			showTip("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		extendMgrPage.addParam.pk10Rebate = pk10rebateValue;
		if(extendMgrPage.addParam.pk10Rebate > extendMgrPage.pk10Param.pk10OpenUserHighest){
			showTip("大于PK10模式["+ extendMgrPage.pk10Param.pk10OpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(extendMgrPage.addParam.pk10Rebate < extendMgrPage.pk10Param.pk10OpenUserLowest){
			showTip("小于PK10模式["+ extendMgrPage.pk10Param.pk10OpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		if(extendMgrPage.addParam.pk10Rebate == extendMgrPage.pk10Param.pk10OpenUserHighest){
			showTip("当前PK10模式已经是最高值了.");
			return;
		}
		
		extendMgrPage.addParam.pk10Rebate += extendMgrPage.pk10Param.pk10Interval; 
		$("#pk10rebateValue").val(extendMgrPage.addParam.pk10Rebate);
		$("#pk10Tip").html('');
		$("#pk10Tip").append("返点"+extendMgrPage.calculateRebateByAwardModelInterval(extendMgrPage.pk10Param.pk10OpenUserHighest, extendMgrPage.addParam.pk10Rebate, extendMgrPage.pk10Param.pk10HighestAwardModel)+"%");
	});
	
	$("#pk10ModeReduction").unbind("click").click(function() {
		var pk10rebateValue = Number($("#pk10rebateValue").val());
		if(pk10rebateValue%2!=0){
			showTip("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		extendMgrPage.addParam.pk10Rebate = pk10rebateValue;
		if(extendMgrPage.addParam.pk10Rebate > extendMgrPage.pk10Param.pk10OpenUserHighest){
			showTip("大于PK10模式最高值了.请重新输入！");
			return;
		}
		if(extendMgrPage.addParam.pk10Rebate < extendMgrPage.pk10Param.pk10OpenUserLowest){
			showTip("小于PK10模式最低值了.请重新输入！");
			return;
		}
		if(extendMgrPage.addParam.pk10Rebate == extendMgrPage.pk10Param.pk10OpenUserLowest){
			showTip("当前PK10模式已经是最低值了.");
			return;
		}
		
		extendMgrPage.addParam.pk10Rebate -= extendMgrPage.pk10Param.pk10Interval;
		$("#pk10rebateValue").val(extendMgrPage.addParam.pk10Rebate);
		$("#pk10Tip").html('');
		$("#pk10Tip").append("返点"+extendMgrPage.calculateRebateByAwardModelInterval(extendMgrPage.pk10Param.pk10OpenUserHighest, extendMgrPage.addParam.pk10Rebate, extendMgrPage.pk10Param.pk10HighestAwardModel)+"%");
	});
	
	//低频彩模式事件
	$("#dpcModeAdd").unbind("click").click(function() {
		var dpcrebateValue = Number($("#dpcrebateValue").val());
		if(dpcrebateValue%2!=0){
			showTip("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		extendMgrPage.addParam.dpcRebate = dpcrebateValue;
		if(extendMgrPage.addParam.dpcRebate > extendMgrPage.dpcParam.dpcOpenUserHighest){
			showTip("大于低频彩模式["+ extendMgrPage.dpcParam.dpcOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(extendMgrPage.addParam.dpcRebate < extendMgrPage.dpcParam.dpcOpenUserLowest){
			showTip("小于低频彩模式["+ extendMgrPage.dpcParam.dpcOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		if(extendMgrPage.addParam.dpcRebate == extendMgrPage.dpcParam.dpcOpenUserHighest){
			showTip("当前低频彩模式已经是最高值了.");
			return;
		}
		
		extendMgrPage.addParam.dpcRebate += extendMgrPage.dpcParam.dpcInterval; 
		$("#dpcrebateValue").val(extendMgrPage.addParam.dpcRebate);
		$("#dpcTip").text('');
		$("#dpcTip").text("返点"+extendMgrPage.calculateRebateByAwardModelInterval(extendMgrPage.dpcParam.dpcOpenUserHighest, extendMgrPage.addParam.dpcRebate, extendMgrPage.dpcParam.dpcHighestAwardModel)+"%");
	});
	
	//
	$("#dpcModeReduction").unbind("click").click(function() {
		var dpcrebateValue = Number($("#dpcrebateValue").val());
		if(dpcrebateValue%2!=0){
			showTip("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		extendMgrPage.addParam.dpcRebate = dpcrebateValue;
		if(extendMgrPage.addParam.dpcRebate > extendMgrPage.dpcParam.dpcOpenUserHighest){
			showTip("大于低频彩模式最高值了.请重新输入！");
			return;
		}
		if(extendMgrPage.addParam.dpcRebate < extendMgrPage.dpcParam.dpcOpenUserLowest){
			showTip("小于低频彩模式最低值了.请重新输入！");
			return;
		}
		if(extendMgrPage.addParam.dpcRebate == extendMgrPage.dpcParam.dpcOpenUserLowest){
			showTip("当前低频彩模式已经是最低值了.");
			return;
		}
		
		extendMgrPage.addParam.dpcRebate -= extendMgrPage.dpcParam.dpcInterval;
		$("#dpcrebateValue").val(extendMgrPage.addParam.dpcRebate);
		$("#dpcTip").text('');
		$("#dpcTip").text("返点"+extendMgrPage.calculateRebateByAwardModelInterval(extendMgrPage.dpcParam.dpcOpenUserHighest, extendMgrPage.addParam.dpcRebate, extendMgrPage.dpcParam.dpcHighestAwardModel)+"%");
		
	});
	
	// 添加推广链接事件
	$("#addExtendLinkButton").unbind("click").click(function() {
		extendMgrPage.addUserExtendLink(); // 添加注册链接
	});
	
	//加载用户的配额信息
	extendMgrPage.loadUserQuota();

	// 加载系统可注册的域名地址
//	extendMgrPage.getSystemDomainList();

	extendMgrPage.loadUserRebate();
	
	// 前往查找推广链接
	extendMgrPage.getUserRegisterLinksByUid();
	
	//删除
	$("#delLinkDiv").unbind("click").click(function(){
		extendMgrPage.deleteRegisterLinkEvent();
	
	});
	
});







function userExtendPostion(){
	var headerWidth=0;
	$(".user_extend .header-title-child").each(function(i,n){
		headerWidth+=parseInt($(this).css("width"));
	});
	$(".user_extend .header-title-content").css("width",headerWidth);
}
function userExtendChange(e){
	var index=$(e).index(".user_extend .header-title-child");
	$(".user_extend .header-title-child").removeClass('on');
	$(e).addClass('on');
	$(".user_extend .content").removeClass('on');
	$(".user_extend .content:eq("+index+")").addClass('on');
}

/**
 * 模式都是+2，-2的
 * @param status
 * @param e
 */
function excelToggle(status,e){
	var parent=$(e).closest('.line-content');
	var val=parseFloat(parent.find(".excel-text .inputText").val());
	//status>0?val++:val--;
	status>0?val+=2:val-=2;
	parent.find(".excel-text .inputText").val(val)
}


/**
 * 添加用户的注册链接地址
 */
ExtendMgrPage.prototype.addUserExtendLink = function() {

	var userDailiType = document.getElementsByName('userDailiType'); 
	for(var i=0;i<userDailiType.length;i++){ 
		if(userDailiType[i].checked){ 
			extendMgrPage.addParam.dailiLevel = userDailiType[i].value; 
		} 
	} 
	//是否选择代理的校验
	if(extendMgrPage.addParam.dailiLevel == null){
		frontCommonPage.showKindlyReminder("对不起,您还没选择代理的类型值.");
		return;
	}

	// 有效期
	extendMgrPage.addParam.continueDay = $("#continueDay").val();
	if (extendMgrPage.addParam.continueDay == null
			|| extendMgrPage.addParam.continueDay == "") {
		extendMgrPage.addParam.continueDay = null;
	}
	
	// 注册赠送资金
	extendMgrPage.addParam.resisterDonateMoney = null;
	// 注册赠送积分
	extendMgrPage.addParam.resisterDonatePoint = null;  //默认不赠送积分
	//extendMgrPage.addParam.resisterDonatePoint = $("#resisterDonatePoint").val();
    //链接备注
	extendMgrPage.addParam.linkDes = $("#linkDes").val();
	if(extendMgrPage.addParam.linkDes == null || extendMgrPage.addParam.linkDes == ""){
		extendMgrPage.addParam.linkDes = "";
	}else{
		if(extendMgrPage.addParam.linkDes.length > 200){
			showTip("对不起,您填写的链接备注内容超出系统要求,最多200个字符.");
            return;
		}
	}
	
	var parma={}
	if(extendMgrPage.addParam.linkDes!=null && extendMgrPage.addParam.linkDes!=""){
		parma.linkDes=extendMgrPage.addParam.linkDes;
	}
    if(extendMgrPage.addParam.domainId!=null && extendMgrPage.addParam.domainId!=""){
    	parma.domainId=extendMgrPage.addParam.domainId;
	}
    if(extendMgrPage.addParam.continueDay!=null && extendMgrPage.addParam.continueDay!=""){
		parma.continueDay=extendMgrPage.addParam.continueDay;
	}
    if(extendMgrPage.addParam.whetherZongdai!=null && extendMgrPage.addParam.whetherZongdai!=""){
    	parma.whetherZongdai=extendMgrPage.addParam.whetherZongdai;
	}
    if(extendMgrPage.addParam.bonusScale!=null && extendMgrPage.addParam.bonusScale!=""){
    	parma.bonusScale=extendMgrPage.addParam.bonusScale;
	}
    if(extendMgrPage.addParam.resisterDonateMoney!=null && extendMgrPage.addParam.resisterDonateMoney!=""){
		parma.resisterDonateMoney=extendMgrPage.addParam.resisterDonateMoney;
	}
    if(extendMgrPage.addParam.resisterDonatePoint!=null && extendMgrPage.addParam.resisterDonatePoint!=""){
    	parma.resisterDonatePoint=extendMgrPage.addParam.resisterDonatePoint;
	}
    if(extendMgrPage.addParam.sscRebateValue!=null && extendMgrPage.addParam.sscRebateValue!=""){
		parma.sscRebateValue=extendMgrPage.addParam.sscRebateValue;
	}
    if(extendMgrPage.addParam.ffcRebateValue!=null && extendMgrPage.addParam.ffcRebateValue!=""){
    	parma.ffcRebateValue=extendMgrPage.addParam.ffcRebateValue;
	}
    if(extendMgrPage.addParam.syxwRebateValue!=null && extendMgrPage.addParam.syxwRebateValue!=""){
		parma.syxwRebateValue=extendMgrPage.addParam.syxwRebateValue;
	}
    if(extendMgrPage.addParam.ksRebateValue!=null && extendMgrPage.addParam.ksRebateValue!=""){
    	parma.ksRebateValue=extendMgrPage.addParam.ksRebateValue;
	}
    if(extendMgrPage.addParam.klsfRebateValue!=null && extendMgrPage.addParam.klsfRebateValue!=""){
		parma.klsfRebateValue=extendMgrPage.addParam.klsfRebateValue;
	}
    if(extendMgrPage.addParam.dpcRebateValue!=null && extendMgrPage.addParam.dpcRebateValue!=""){
    	parma.dpcRebateValue=extendMgrPage.addParam.dpcRebateValue;
	}
    if(extendMgrPage.addParam.sscrebate!=null && extendMgrPage.addParam.sscrebate!=""){
		parma.sscrebate=extendMgrPage.addParam.sscrebate;
	}
    if(extendMgrPage.addParam.ffcrebate!=null && extendMgrPage.addParam.ffcrebate!=""){
    	parma.ffcrebate=extendMgrPage.addParam.ffcrebate;
	}
    if(extendMgrPage.addParam.syxwRebate!=null && extendMgrPage.addParam.syxwRebate!=""){
		parma.syxwRebate=extendMgrPage.addParam.syxwRebate;
	}
    if(extendMgrPage.addParam.ksRebate!=null && extendMgrPage.addParam.ksRebate!=""){
    	parma.ksRebate=extendMgrPage.addParam.ksRebate;
	}
    if(extendMgrPage.addParam.klsfrebate!=null && extendMgrPage.addParam.klsfrebate!=""){
		parma.klsfrebate=extendMgrPage.addParam.klsfrebate;
	}
    if(extendMgrPage.addParam.dpcRebate!=null && extendMgrPage.addParam.dpcRebate!=""){
    	parma.dpcRebate=extendMgrPage.addParam.dpcRebate;
	}
    if(extendMgrPage.addParam.pk10Rebate!=null && extendMgrPage.addParam.pk10Rebate!=""){
		parma.pk10Rebate=extendMgrPage.addParam.pk10Rebate;
	}
    
	$.ajax({
		type: "POST",
        url: contextPath +"/register/addUserExtend",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(parma), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		var  link = r[1] = result.data;
        		showTip("邀请码生成成功！");
				$(".user_extend .header-title-child").removeClass('on');
				$(".user_extend .header-title-child2").addClass('on');
				$(".user_extend .content").removeClass('on');
				$(".user_extend .content:eq(1)").addClass('on');
				extendMgrPage.getUserRegisterLinksByUid();
        	}else if(result.code == "error"){
        		var err = result.data;
        		showTip(err);
        	}
        }
	});
};

/**
 * 加载用户的所有注册地址
 */
ExtendMgrPage.prototype.getUserRegisterLinksByUid = function() {
	extendMgrPage.queryParam.userId=currentUser.id;
	extendMgrPage.queryParam.maxPageNum = null;
	$("#showAllDiv").show();
	extendMgrPage.getUserRegisterLinks(extendMgrPage.queryParam,extendMgrPage.pageParam.pageNo);
};


/**
 * 加载用户的所有注册地址
 */
ExtendMgrPage.prototype.getAllUserRegisterLinks = function() {
	extendMgrPage.queryParam.userId=currentUser.id;
	extendMgrPage.queryParam.maxPageNum = extendMgrPage.pageParam.totalRecNum;
	$("#showAllDiv").hide();
	extendMgrPage.getUserRegisterLinks(extendMgrPage.queryParam,extendMgrPage.pageParam.pageNo);
};

/**
 * 请求加载加载用户的所有注册地址
 */
ExtendMgrPage.prototype.getUserRegisterLinks = function(queryParam,pageNo) {
	var param={
		maxPageNum : null
	}
	
    if(queryParam.maxPageNum!=null && queryParam.maxPageNum!=""){
    	param.maxPageNum=queryParam.maxPageNum;
	}
	var jsonData = {
		"query" : param,
		"pageNo" : pageNo,
	};
	$.ajax({
		type: "POST",
        url: contextPath +"/register/userExtendQuery",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(jsonData), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		var data = result.data;
        		extendMgrPage.showUserRegisterLinks(data);
        	}else if(result.code == "error"){
        		var err = result.data;
        		showTip(err);
        	}
        }
	});
};
/**
 * 展示用户的所有可注册地址
 */
ExtendMgrPage.prototype.showUserRegisterLinks = function(page) {
	$('.user-show').html("显示更多");
	var extendLinkListObj = $("#extendLinkList");
	 var registerList = page.pageContent;
	 extendMgrPage.pageParam.totalRecNum=page.totalRecNum;
	var str = "";
//	extendLinkListObj.html("");
	if (registerList.length == 0) {
		extendLinkListObj.html("");
		str += "<div class='line'>";
		str += "  <div class=\"child\" style='text-align: center;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style='text-align: center;display: inline-block;'>您还没有可推广的链接.</p></div>";
		str += "</div>";
		extendLinkListObj.append(str);
		$(".user-show").hide();
	} 
	if (registerList.length > 0) {

		extendMgrPage.param.linkList = registerList;

		for (var i = 0; i < registerList.length; i++) {
			var register = registerList[i];
			str += " <div class='line'>";
			str += " <div class='child child1 font-blue' onclick='extendMgrPage.registerDetail(\""+register.id+"\")'>"+(register.invitationCode==null?"":register.invitationCode)+"</div>";
			str += " <div class='child child2'>" + register.createdDateStr + "</div>";	
			str += "  <div class='child child3'>" + register.useCount + "</div>";
			 str += "</div>";
				extendLinkListObj.append(str);
				str = "";
		}
	}
	
	// 如果当前页数小于总页数，显示加载更多
	if (page.pageNo < page.totalPageNum) {
		$(".user-show").show();
		$(".user-show").html("加载更多");
		// 添加加载更多事件
		$(".user-show").unbind("click").click(function() {
			extendMgrPage.pageParam.pageNo++;
			extendMgrPage.getAllUserRegisterLinks();
		});
	}else {
		$(".user-show").hide();
	}

	if (page.totalPageNum == 1) {
		$(".user-show").hide();
	}
	
};


/**
 * 删除用户注册链接按钮事件
 * 
 * @param index
 */
ExtendMgrPage.prototype.deleteRegisterLinkEvent = function() {
	
	extendMgrPage.param.deleteLinkId = $("#deleteLinkId").val();
	if(comfirm("温馨提示！","确认删除本条记录？","取消",extendMgrPage.cancleFun,"确定",extendMgrPage.deleteRegisterLink)){
		
	}
	
};

ExtendMgrPage.prototype.cancleFun = function() {
	showTip("取消成功！");
};

/**
 * 删除用户注册链接
 * 
 * @param index
 */
ExtendMgrPage.prototype.deleteRegisterLink = function() {
	var param={};
	if(extendMgrPage.param.deleteLinkId!=null && extendMgrPage.param.deleteLinkId!=""){
    	param.linkId=extendMgrPage.param.deleteLinkId;
	}
	$.ajax({
		type: "POST",
        url: contextPath +"/register/deleteUserExtend",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(param), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		/*var data = result.data;*/
        		window.location.href =contextPath+"/gameBet/agentCenter/extendMgr.html";
        	}else if(result.code == "error"){
        		var err = result.data;
        		showTip(err);
        	}
        }
	});
};

/**
 * 展示注册详情的信息
 */
ExtendMgrPage.prototype.registerDetail = function(linkid) {
	extendMgrPage.param.linkId=linkid;
	
	$("#deleteLinkId").val(linkid);
	extendMgrPage.getExtendLink();
};

/**
 * 获取推广链接 
 */
ExtendMgrPage.prototype.getExtendLink = function(){
	var param={}
	if(extendMgrPage.param.linkId!=null && extendMgrPage.param.linkId!=""){
    	param.linkId=extendMgrPage.param.linkId;
	}
	$.ajax({
		type: "POST",
        url: contextPath +"/register/getUserExtendById",
        contentType : "application/json;charset=UTF-8",
        data:JSON.stringify(param),
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		//var data=result.data;
        		//extendMgrPage.showRegisterDetail(r[1],r[2],r[3]);
    			var userLink = result.data;
    			var extendLink = userLink;
    			var linkDomain = extendLink.linkContent.substring(0, extendLink.linkContent.indexOf("/reg"));
    			//var mobileDetailLinkName = linkDomain + "/mobile/user/mobile_reg.html?param1=" + extendLink.uuid;
    			$("#reglink_li").html("");
    			var str="<p ><span >注册链接</span></p>";
    			str+='<p><span style="color:#50a3ff;">'+extendLink.linkContent+'</span></p>';
    			$("#reglink_li").append(str);
    			
    			$("#invitationCode").text(extendLink.invitationCode);
    			$("#khlxSpan").text(extendLink.invitationCode);
    			$("#msSpan").text(extendLink.invitationCode);
    			
    			
    			// 时时彩
    			$("#detailSscrebate").text(extendLink.sscRebateValue+"%");

    			// 快三
    			$("#detailKsrebate").text(extendLink.ksRebateValue+"%");
    			
    			// pk10
    			$("#detailPk10rebate").text(extendLink.pk10RebateValue+"%");
    			
    			// 11选5
    			$("#detailSyxwrebate").text(extendLink.syxwRebateValue+"%");
    			
    			// 低频彩
    			$("#detailDpcrebate").text(extendLink.dpcRebateValue+"%");
    			$("#detailDaiLiType").text(extendLink.dailiLevel);
    			/*
    			// 是否是总代
    			if (registerLinkArray[0] != "-") {
    				if(registerLinkArray[0] == "DIRECTAGENCY"){
    					$("#detailDaiLiType").text("直属代理");
    				}else if(registerLinkArray[0] == "GENERALAGENCY"){
    					$("#detailDaiLiType").text("代理"); //总代理
    				}else if(registerLinkArray[0] == "ORDINARYAGENCY"){
    					$("#detailDaiLiType").text("代理");
    				}else if(registerLinkArray[0] == "REGULARMEMBERS"){
    					$("#detailDaiLiType").text("普通会员");
    				}else{
    					showTip("未知的代理类型");
    				}
    			} else {
    				$("#detailDaiLiType").text("普通会员");
    			}*/
    			
    			if (extendLink.loseDateStr == "") {
    				$("#detailLoseDate").text("永久有效");
    			} else {
    				$("#detailLoseDate").text(extendLink.loseDateStr);
    			}
    			
    			$("#detailCreateDate").text(extendLink.createdDateStr);
    			if(extendLink.linkDes == null || extendLink.linkDes == ""){
    				$("#detailDes").text("-");
    			}else{
    				$("#detailDes").text(extendLink.linkDes);
    			}
    	 
    			modal_toggle(".user_extend_add-modal");
        	}else if(result.code == "error"){
        		var err = result.data;
        		showTip(err);
        	}
        }
	})
};


/**
 * 展示注册详情的信息
 */
ExtendMgrPage.prototype.showRegisterDetail = function(userLink) {
		var extendLink = userLink;
		var linkSets = extendLink.linkSets;
		var registerLinkArray = linkSets.split("&");
		var linkDomain = extendLink.linkContent.substring(0, extendLink.linkContent.indexOf("/reg"));
		//var mobileDetailLinkName = linkDomain + "/mobile/user/mobile_reg.html?param1=" + extendLink.uuid;
		$("#reglink_li").html("");
		var str="<p ><span >注册链接</span></p>";
		str+='<p><span style="color:#50a3ff;">'+extendLink.linkContent+'</span></p>';
		
		$("#reglink_li").append(str);
		
		$("#invitationCode").text(extendLink.invitationCode);
		$("#khlxSpan").text(extendLink.invitationCode);
		$("#msSpan").text(extendLink.invitationCode);
		
		
		// 时时彩
		$("#detailSscrebate").text(extendLink.sscRebateValue+"%");

		
		$("#detailDaiLiType").text(extendLink.dailiLevel);
		// 是否是总代
		/*if (registerLinkArray[0] != "-") {
			if(registerLinkArray[0] == "DIRECTAGENCY"){
				$("#detailDaiLiType").text("直属代理");
			}else if(registerLinkArray[0] == "GENERALAGENCY"){
				$("#detailDaiLiType").text("代理"); //总代理
			}else if(registerLinkArray[0] == "ORDINARYAGENCY"){
				$("#detailDaiLiType").text("代理");
			}else if(registerLinkArray[0] == "REGULARMEMBERS"){
				$("#detailDaiLiType").text("普通会员");
			}else{
				showTip("未知的代理类型");
			}
		} else {
			$("#detailDaiLiType").text("普通会员");
		}*/
		
		if (extendLink.loseDateStr == "") {
			$("#detailLoseDate").text("永久有效");
		} else {
			$("#detailLoseDate").text(extendLink.loseDateStr);
		}
		
		$("#detailCreateDate").text(extendLink.createdDateStr);
		if(extendLink.linkDes == null || extendLink.linkDes == ""){
			$("#detailDes").text("-");
		}else{
			$("#detailDes").text(extendLink.linkDes);
		}
 
		modal_toggle(".user_extend_add-modal");
};



/**
 * 获取本系统的最高最低模式
 */
ExtendMgrPage.prototype.loadUserRebate = function(){
	$.ajax({
		type: "POST",
        url: contextPath +"/system/loadSystemRebate",
        contentType :"application/json;charset=UTF-8",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		var data=result.data;
        		extendMgrPage.sscParam.sscOpenUserLowest=data.sscLowestAwardModel;
    			extendMgrPage.syxwParam.syxwOpenUserLowest=data.syxwLowestAwardModel;
    			extendMgrPage.dpcParam.dpcOpenUserLowest=data.dpcLowestAwardModel;
    			extendMgrPage.pk10Param.pk10OpenUserLowest=data.pk10LowestAwardModel;
    			extendMgrPage.ksParam.ksOpenUserLowest=data.ksLowestAwardModel;
    			extendMgrPage.sscParam.sscHighestAwardModel = sscHighestAwardModel;
    			extendMgrPage.syxwParam.syxwHighestAwardModel = syxwHighestAwardModel;
    			extendMgrPage.dpcParam.dpcHighestAwardModel = dpcHighestAwardModel;
    			extendMgrPage.pk10Param.pk10HighestAwardModel = pk10HighestAwardModel;
    			extendMgrPage.ksParam.ksHighestAwardModel = ksHighestAwardModel;
    			$("#sscTip").text('');
    			$("#sscTip").text("返点"+extendMgrPage.calculateRebateByAwardModelInterval(extendMgrPage.sscParam.sscOpenUserHighest, extendMgrPage.addParam.sscrebate, extendMgrPage.sscParam.sscHighestAwardModel)+"%");
    			$("#ksTip").text('');
    			$("#ksTip").text("返点"+extendMgrPage.calculateRebateByAwardModelInterval(extendMgrPage.ksParam.ksOpenUserHighest, extendMgrPage.addParam.ksRebate, extendMgrPage.ksParam.ksHighestAwardModel)+"%");
    			$("#syxwTip").text('');
    			$("#syxwTip").text("返点"+extendMgrPage.calculateRebateByAwardModelInterval(extendMgrPage.syxwParam.syxwOpenUserHighest, extendMgrPage.addParam.syxwRebate, extendMgrPage.syxwParam.syxwHighestAwardModel)+"%");
    			$("#pk10Tip").text('');
    			$("#pk10Tip").text("返点"+extendMgrPage.calculateRebateByAwardModelInterval(extendMgrPage.pk10Param.pk10OpenUserHighest, extendMgrPage.addParam.pk10Rebate, extendMgrPage.pk10Param.pk10HighestAwardModel)+"%");
    			$("#dpcTip").text('');
    			$("#dpcTip").text("返点"+extendMgrPage.calculateRebateByAwardModelInterval(extendMgrPage.dpcParam.dpcOpenUserHighest, extendMgrPage.addParam.dpcRebate, extendMgrPage.dpcParam.dpcHighestAwardModel)+"%");
        	}else if(result.code == "error"){
        		var err = result.data;
        		showTip(err);
        	}
        }
	});
}

/**
 *  根据模式显示当前配额数
 */
ExtendMgrPage.prototype.showRemainNumBySscRebate= function(sscRebate){
	var sscRebateRemainNum=quotaMap.get("sscRebate"+sscRebate);
	if(sscRebateRemainNum!=null){
		$("#quotaTip").show();
		
		$("#quotaLowLevel").text(sscRebateRemainNum);
	}else{
		$("#quotaTip").hide();
	}
}
/**
 * 加载配额信息
 */
ExtendMgrPage.prototype.loadUserQuota = function(){
	$.ajax({
		type: "POST",
        url: contextPath +"/user/loadUserQuota",
        contentType :"application/json;charset=UTF-8",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		var data=result.data;
        		var userQuotaList = data.pageContent;
    			for(var i=0;i< userQuotaList.length;i++){
    				var userQuota = userQuotaList[i];
    				quotaMap.put("sscRebate"+userQuota.sscRebate,userQuota.remainNum);
    			}
    			extendMgrPage.showRemainNumBySscRebate(extendMgrPage.sscParam.sscOpenUserHighest-2);
        	}else if(result.code == "error"){
        		var err = result.data;
        		showTip(err);
        	}
        }
	});
};

/**
 * 计算用户自身保留返点
 */
ExtendMgrPage.prototype.calculateRebateByAwardModelInterval = function(highRebate, lowRebate, highestAwardModel) {
	var rebate = (highRebate - lowRebate)/highestAwardModel;
	rebate = (rebate * 100).toFixed(2);
	return rebate;
}