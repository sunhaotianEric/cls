/**
 * 生成推广
 */
function AddExtendPage() {
}
var addExtendPage = new AddExtendPage();
var quotaMap= new JS_OBJECT_MAP();
addExtendPage.param = {
	linkList : null,
	deleteLinkId : null
// 当前用户的注册列表
};

/**
 * 查询参数
 */
addExtendPage.queryParam = {
	userId : null
};


addExtendPage.sscParam = {
	    sscInterval : 2,
	    sscOpenUserHighest : null,
	    sscOpenUserLowest : null,
	    sscOpenUserHighestRebateValue : null,
	    sscOpenUserLowestRebateValue : null
	};

addExtendPage.syxwParam = {
		syxwInterval : 2,
		syxwOpenUserHighest : null,
		syxwOpenUserLowest : null,
		syxwHighestAwardModel : null
};

addExtendPage.dpcParam = {
		dpcInterval : 2,
		dpcOpenUserHighest : null,
		dpcOpenUserLowest : null,
		dpcHighestAwardModel : null
};

addExtendPage.pk10Param = {
		pk10Interval : 2,
		pk10OpenUserHighest : null,
		pk10OpenUserLowest : null,
		pk10HighestAwardModel : null
};

addExtendPage.ksParam = {
		ksInterval : 2,
		ksOpenUserHighest : null,
		ksOpenUserLowest : null,
		ksHighestAwardModel : null
};
/**
 * 添加参数
 */
addExtendPage.addParam = {
	linkDes : null,
	domainId : null,
	continueDay : null,
	whetherZongdai : null,
	bonusScale : null,
	resisterDonateMoney : null,
	resisterDonatePoint : null,
	sscRebateValue : null,
	ffcRebateValue : null,
	syxwRebateValue : null,
	ksRebateValue : null,
	klsfRebateValue : null,
	dpcRebateValue : null,
	sscrebate : null,
	ffcrebate : null,
	syxwrebate : null,
	ksrebate : null,
	klsfrebate : null,
	dpcrebate : null,
	pk10Rebate:null
};


//分页参数
addExtendPage.pageParam = {
        queryMethodParam : null,
        pageNo : 1,
        pageMaxNo : 1,  //最大的页数
        skipHtmlStr : ""
};

$(document).ready(function() {
	addExtendPage.sscParam.sscOpenUserHighest=currentUser.sscRebate;
	addExtendPage.syxwParam.syxwOpenUserHighest=currentUser.syxwRebate;
	addExtendPage.dpcParam.dpcOpenUserHighest=currentUser.dpcRebate;
	addExtendPage.pk10Param.pk10OpenUserHighest=currentUser.pk10Rebate;
	addExtendPage.ksParam.ksOpenUserHighest=currentUser.ksRebate;
	
	addExtendPage.addParam.sscrebate = addExtendPage.sscParam.sscOpenUserHighest-addExtendPage.sscParam.sscInterval;
	addExtendPage.addParam.ksRebate = addExtendPage.ksParam.ksOpenUserHighest-addExtendPage.ksParam.ksInterval;
	addExtendPage.addParam.syxwRebate = addExtendPage.syxwParam.syxwOpenUserHighest-addExtendPage.syxwParam.syxwInterval;
	addExtendPage.addParam.pk10Rebate = addExtendPage.pk10Param.pk10OpenUserHighest-addExtendPage.pk10Param.pk10Interval;
	addExtendPage.addParam.dpcRebate = addExtendPage.dpcParam.dpcOpenUserHighest-addExtendPage.dpcParam.dpcInterval;
	
	$("#sscrebateValue").val(addExtendPage.sscParam.sscOpenUserHighest-addExtendPage.sscParam.sscInterval);
	$("#ksrebateValue").val(addExtendPage.ksParam.ksOpenUserHighest-addExtendPage.ksParam.ksInterval);
	$("#syxwrebateValue").val(addExtendPage.syxwParam.syxwOpenUserHighest-addExtendPage.syxwParam.syxwInterval);
	$("#pk10rebateValue").val(addExtendPage.pk10Param.pk10OpenUserHighest-addExtendPage.pk10Param.pk10Interval);
	$("#dpcrebateValue").val(addExtendPage.dpcParam.dpcOpenUserHighest-addExtendPage.dpcParam.dpcInterval);
	
	//时时彩模式事件
	$("#sscModeAdd").unbind("click").click(function() {
		var sscrebateValue = Number($("#sscrebateValue").val());
		if(sscrebateValue%2!=0){
			showTip("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		if(sscrebateValue > addExtendPage.sscParam.sscOpenUserHighest){
			showTip("大于时时彩模式["+ addExtendPage.sscParam.sscOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(sscrebateValue < addExtendPage.sscParam.sscOpenUserLowest){
			showTip("小于时时彩模式["+ addExtendPage.sscParam.sscOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		if(sscrebateValue == addExtendPage.sscParam.sscOpenUserHighest){
			showTip("当前时时彩模式已经是最高值了.");
			return;
		}
		
		sscrebateValue += addExtendPage.sscParam.sscInterval; 
		addExtendPage.addParam.sscrebate = sscrebateValue;
		$("#sscrebateValue").val(sscrebateValue);
		addExtendPage.showRemainNumBySscRebate(sscrebateValue);
		$("#sscTip").text('');
		$("#sscTip").text("返点"+addExtendPage.calculateRebateByAwardModelInterval(addExtendPage.sscParam.sscOpenUserHighest, sscrebateValue, addExtendPage.sscParam.sscHighestAwardModel)+"%");
	});
	$("#sscModeReduction").unbind("click").click(function() {
		var sscrebateValue = Number($("#sscrebateValue").val());
		if(sscrebateValue%2!=0){
			showTip("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		if(sscrebateValue > addExtendPage.sscParam.sscOpenUserHighest){
			showTip("大于时时彩模式最高值了.请重新输入！");
			return;
		}
		if(sscrebateValue < addExtendPage.sscParam.sscOpenUserLowest){
			showTip("小于时时彩模式最低值了.请重新输入！");
			return;
		}
		if(sscrebateValue == addExtendPage.sscParam.sscOpenUserLowest){
			showTip("当前时时彩模式已经是最低值了.");
			return;
		}
		sscrebateValue -= addExtendPage.sscParam.sscInterval; 
		addExtendPage.addParam.sscrebate = sscrebateValue;
		$("#sscrebateValue").val(sscrebateValue);
		addExtendPage.showRemainNumBySscRebate(sscrebateValue);
		$("#sscTip").text('');
		$("#sscTip").text("返点"+addExtendPage.calculateRebateByAwardModelInterval(addExtendPage.sscParam.sscOpenUserHighest, sscrebateValue, addExtendPage.sscParam.sscHighestAwardModel)+"%");
	});
	
	//快三模式事件
	$("#ksModeAdd").unbind("click").click(function() {
		var ksrebateValue = Number($("#ksrebateValue").val());
		if(ksrebateValue%2!=0){
			showTip("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		if(ksrebateValue > addExtendPage.ksParam.sscOpenUserHighest){
			showTip("大于快三模式["+ addExtendPage.ksParam.ksOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(ksrebateValue < addExtendPage.ksParam.sscOpenUserLowest){
			showTip("小于快三模式["+ addExtendPage.ksParam.ksOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		if(ksrebateValue == addExtendPage.ksParam.ksOpenUserHighest){
			showTip("当前快三模式已经是最高值了.");
			return;
		}
		
		ksrebateValue += addExtendPage.ksParam.ksInterval; 
		addExtendPage.addParam.ksRebate = ksrebateValue;
		$("#ksrebateValue").val(ksrebateValue);
		$("#ksTip").text('');
		$("#ksTip").text("返点"+addExtendPage.calculateRebateByAwardModelInterval(addExtendPage.ksParam.ksOpenUserHighest, ksrebateValue, addExtendPage.ksParam.ksHighestAwardModel)+"%");
	});
	
	$("#ksModeReduction").unbind("click").click(function() {
		var ksrebateValue = Number($("#ksrebateValue").val());
		if(ksrebateValue%2!=0){
			showTip("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		if(ksrebateValue > addExtendPage.ksParam.ksOpenUserHighest){
			showTip("大于快三模式最高值了.请重新输入！");
			return;
		}
		if(ksrebateValue < addExtendPage.ksParam.ksOpenUserLowest){
			showTip("小于快三模式最低值了.请重新输入！");
			return;
		}
		
		
		ksrebateValue -= addExtendPage.ksParam.ksInterval;
		addExtendPage.addParam.ksRebate = ksrebateValue;
		$("#ksrebateValue").val(ksrebateValue);
		$("#ksTip").text('');
		$("#ksTip").text("返点"+addExtendPage.calculateRebateByAwardModelInterval(addExtendPage.ksParam.ksOpenUserHighest, ksrebateValue, addExtendPage.ksParam.ksHighestAwardModel)+"%");
	});
	
	//十一选五模式事件
	$("#syxwModeAdd").unbind("click").click(function() {
		var syxwrebateValue = Number($("#syxwrebateValue").val());
		/*if(syxwrebateValue%2!=0){
			showTip("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}*/
		if(syxwrebateValue > addExtendPage.syxwParam.syxwOpenUserHighest){
			showTip("大于十一选五模式["+ addExtendPage.syxwParam.syxwOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(syxwrebateValue < addExtendPage.syxwParam.syxwOpenUserLowest){
			showTip("小于十一选五模式["+ addExtendPage.syxwParam.syxwOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		if(syxwrebateValue == addExtendPage.syxwParam.syxwOpenUserHighest){
			showTip("当前十一选五模式已经是最高值了.");
			return;
		}
		
		syxwrebateValue += addExtendPage.syxwParam.syxwInterval; 
		addExtendPage.addParam.syxwRebate = syxwrebateValue;
		$("#syxwrebateValue").val(syxwrebateValue);
		$("#syxwTip").text('');
		$("#syxwTip").text("返点"+addExtendPage.calculateRebateByAwardModelInterval(addExtendPage.syxwParam.syxwOpenUserHighest, syxwrebateValue, addExtendPage.syxwParam.syxwHighestAwardModel)+"%");
	});
	
	$("#syxwModeReduction").unbind("click").click(function() {
		var syxwrebateValue = Number($("#syxwrebateValue").val());
		/*if(syxwrebateValue%2!=0){
			showTip("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}*/
		if(syxwrebateValue > addExtendPage.syxwParam.syxwOpenUserHighest){
			showTip("大于十一选五模式最高值了.请重新输入！");
			return;
		}
		if(syxwrebateValue < addExtendPage.syxwParam.syxwOpenUserLowest){
			showTip("小于十一选五模式最低值了.请重新输入！");
			return;
		}
		if(syxwrebateValue == addExtendPage.syxwParam.syxwOpenUserLowest){
			showTip("当前十一选五模式已经是最低值了.");
			return;
		}
		
		syxwrebateValue -= addExtendPage.syxwParam.syxwInterval;
		addExtendPage.addParam.syxwRebate = syxwrebateValue;
		$("#syxwrebateValue").val(syxwrebateValue);
		$("#syxwTip").text('');
		$("#syxwTip").text("返点"+addExtendPage.calculateRebateByAwardModelInterval(addExtendPage.syxwParam.syxwOpenUserHighest, syxwrebateValue, addExtendPage.syxwParam.syxwHighestAwardModel)+"%");
	});
	
	//pk10模式事件
	$("#pk10ModeAdd").unbind("click").click(function() {
		var pk10rebateValue = Number($("#pk10rebateValue").val());
		if(pk10rebateValue%2!=0){
			showTip("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		if(pk10rebateValue > addExtendPage.pk10Param.pk10OpenUserHighest){
			showTip("大于PK10模式["+ addExtendPage.pk10Param.pk10OpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(pk10rebateValue < addExtendPage.pk10Param.pk10OpenUserLowest){
			showTip("小于PK10模式["+ addExtendPage.pk10Param.pk10OpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		if(pk10rebateValue == addExtendPage.pk10Param.pk10OpenUserHighest){
			showTip("当前PK10模式已经是最高值了.");
			return;
		}
		
		pk10rebateValue += addExtendPage.pk10Param.pk10Interval; 
		addExtendPage.addParam.pk10Rebate = pk10rebateValue;
		$("#pk10rebateValue").val(pk10rebateValue);
		$("#pk10Tip").html('');
		$("#pk10Tip").append("返点"+addExtendPage.calculateRebateByAwardModelInterval(addExtendPage.pk10Param.pk10OpenUserHighest, pk10rebateValue, addExtendPage.pk10Param.pk10HighestAwardModel)+"%");
	});
	
	$("#pk10ModeReduction").unbind("click").click(function() {
		var pk10rebateValue = Number($("#pk10rebateValue").val());
		if(pk10rebateValue%2!=0){
			showTip("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		if(pk10rebateValue > addExtendPage.pk10Param.pk10OpenUserHighest){
			showTip("大于PK10模式最高值了.请重新输入！");
			return;
		}
		if(pk10rebateValue < addExtendPage.pk10Param.pk10OpenUserLowest){
			showTip("小于PK10模式最低值了.请重新输入！");
			return;
		}
		if(pk10rebateValue == addExtendPage.pk10Param.pk10OpenUserLowest){
			showTip("当前PK10模式已经是最低值了.");
			return;
		}
		
		pk10rebateValue -= addExtendPage.pk10Param.pk10Interval;
		addExtendPage.addParam.pk10Rebate = pk10rebateValue;
		$("#pk10rebateValue").val(pk10rebateValue);
		$("#pk10Tip").html('');
		$("#pk10Tip").append("返点"+addExtendPage.calculateRebateByAwardModelInterval(addExtendPage.pk10Param.pk10OpenUserHighest, pk10rebateValue, addExtendPage.pk10Param.pk10HighestAwardModel)+"%");
	});
	
	//低频彩模式事件
	$("#dpcModeAdd").unbind("click").click(function() {
		var dpcrebateValue = Number($("#dpcrebateValue").val());
		if(dpcrebateValue%2!=0){
			showTip("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		if(dpcrebateValue > addExtendPage.dpcParam.dpcOpenUserHighest){
			showTip("大于低频彩模式["+ addExtendPage.dpcParam.dpcOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(dpcrebateValue < addExtendPage.dpcParam.dpcOpenUserLowest){
			showTip("小于低频彩模式["+ addExtendPage.dpcParam.dpcOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		if(dpcrebateValue == addExtendPage.dpcParam.dpcOpenUserHighest){
			showTip("当前低频彩模式已经是最高值了.");
			return;
		}
		
		dpcrebateValue += addExtendPage.dpcParam.dpcInterval; 
		addExtendPage.addParam.dpcRebate = dpcrebateValue;
		$("#dpcrebateValue").val(dpcrebateValue);
		$("#dpcTip").text('');
		$("#dpcTip").text("返点"+addExtendPage.calculateRebateByAwardModelInterval(addExtendPage.dpcParam.dpcOpenUserHighest, dpcrebateValue, addExtendPage.dpcParam.dpcHighestAwardModel)+"%");
	});
	
	//
	$("#dpcModeReduction").unbind("click").click(function() {
		var dpcrebateValue = Number($("#dpcrebateValue").val());
		if(dpcrebateValue%2!=0){
			showTip("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		if(dpcrebateValue > addExtendPage.dpcParam.dpcOpenUserHighest){
			showTip("大于低频彩模式最高值了.请重新输入！");
			return;
		}
		if(dpcrebateValue < addExtendPage.dpcParam.dpcOpenUserLowest){
			showTip("小于低频彩模式最低值了.请重新输入！");
			return;
		}
		if(dpcrebateValue == addExtendPage.dpcParam.dpcOpenUserLowest){
			showTip("当前低频彩模式已经是最低值了.");
			return;
		}
		
		dpcrebateValue -= addExtendPage.dpcParam.dpcInterval;
		addExtendPage.addParam.dpcRebate = dpcrebateValue;
		$("#dpcrebateValue").val(dpcrebateValue);
		$("#dpcTip").text('');
		$("#dpcTip").text("返点"+addExtendPage.calculateRebateByAwardModelInterval(addExtendPage.dpcParam.dpcOpenUserHighest, dpcrebateValue, addExtendPage.dpcParam.dpcHighestAwardModel)+"%");
		
	});
	
	// 添加推广链接事件
	$("#addExtendLinkButton").unbind("click").click(function() {
		addExtendPage.addUserExtendLink(); // 添加注册链接
	});
	
	//加载用户的配额信息
	addExtendPage.loadUserQuota();

	// 加载系统可注册的域名地址
//	addExtendPage.getSystemDomainList();

	addExtendPage.loadUserRebate();
	
	// 前往查找推广链接
	addExtendPage.getUserRegisterLinksByUid();
	
	//删除
	$("#delLinkDiv").unbind("click").click(function(){
		addExtendPage.deleteRegisterLinkEvent();
	
	});
	
});







function userExtendPostion(){
	var headerWidth=0;
	$(".user_extend .header-title-child").each(function(i,n){
		headerWidth+=parseInt($(this).css("width"));
	});
	$(".user_extend .header-title-content").css("width",headerWidth);
}
function userExtendChange(e){
	var index=$(e).index(".user_extend .header-title-child");
	$(".user_extend .header-title-child").removeClass('on');
	$(e).addClass('on');
	$(".user_extend .content").removeClass('on');
	$(".user_extend .content:eq("+index+")").addClass('on');
}

/**
 * 添加用户的注册链接地址
 */
AddExtendPage.prototype.addUserExtendLink = function() {

	var userDailiType = document.getElementsByName('userDailiType'); 
	for(var i=0;i<userDailiType.length;i++){ 
		if(userDailiType[i].checked){ 
			addExtendPage.addParam.dailiLevel = userDailiType[i].value; 
		} 
	} 
	//是否选择代理的校验
	if(addExtendPage.addParam.dailiLevel == null){
		showTip("对不起,您还没选择代理的类型值.");
		return;
	}

	// 有效期
	addExtendPage.addParam.continueDay = $("#continueDay").val();
	if (addExtendPage.addParam.continueDay == null
			|| addExtendPage.addParam.continueDay == "") {
		addExtendPage.addParam.continueDay = null;
	}
	
	// 注册赠送资金
	addExtendPage.addParam.resisterDonateMoney = null;
	// 注册赠送积分
	addExtendPage.addParam.resisterDonatePoint = null;  //默认不赠送积分
	//addExtendPage.addParam.resisterDonatePoint = $("#resisterDonatePoint").val();
    //链接备注
	addExtendPage.addParam.linkDes = $("#linkDes").val();
	if(addExtendPage.addParam.linkDes == null || addExtendPage.addParam.linkDes == ""){
		addExtendPage.addParam.linkDes = "";
	}else{
		if(addExtendPage.addParam.linkDes.length > 200){
			showTip("对不起,您填写的链接备注内容超出系统要求,最多200个字符.");
            return;
		}
	}
	
	var param={}
	if(addExtendPage.addParam.dailiLevel!=null && addExtendPage.addParam.dailiLevel!=""){
		param.dailiLevel=addExtendPage.addParam.dailiLevel;
	}
	if(addExtendPage.addParam.linkDes!=null && addExtendPage.addParam.linkDes!=""){
		param.linkDes=addExtendPage.addParam.linkDes;
	}
    if(addExtendPage.addParam.domainId!=null && addExtendPage.addParam.domainId!=""){
    	param.domainId=addExtendPage.addParam.domainId;
	}
    if(addExtendPage.addParam.continueDay!=null && addExtendPage.addParam.continueDay!=""){
    	param.continueDay=addExtendPage.addParam.continueDay;
	}
    if(addExtendPage.addParam.whetherZongdai!=null && addExtendPage.addParam.whetherZongdai!=""){
    	param.whetherZongdai=addExtendPage.addParam.whetherZongdai;
	}
    if(addExtendPage.addParam.bonusScale!=null && addExtendPage.addParam.bonusScale!=""){
    	param.bonusScale=addExtendPage.addParam.bonusScale;
	}
    if(addExtendPage.addParam.resisterDonateMoney!=null && addExtendPage.addParam.resisterDonateMoney!=""){
    	param.resisterDonateMoney=addExtendPage.addParam.resisterDonateMoney;
	}
    if(addExtendPage.addParam.resisterDonatePoint!=null && addExtendPage.addParam.resisterDonatePoint!=""){
    	param.resisterDonatePoint=addExtendPage.addParam.resisterDonatePoint;
	}
    if(addExtendPage.addParam.sscRebateValue!=null && addExtendPage.addParam.sscRebateValue!=""){
    	param.sscRebateValue=addExtendPage.addParam.sscRebateValue;
	}
    if(addExtendPage.addParam.ffcRebateValue!=null && addExtendPage.addParam.ffcRebateValue!=""){
    	param.ffcRebateValue=addExtendPage.addParam.ffcRebateValue;
	}
    if(addExtendPage.addParam.syxwRebateValue!=null && addExtendPage.addParam.syxwRebateValue!=""){
    	param.syxwRebateValue=addExtendPage.addParam.syxwRebateValue;
	}
    if(addExtendPage.addParam.ksRebateValue!=null && addExtendPage.addParam.ksRebateValue!=""){
    	param.ksRebateValue=addExtendPage.addParam.ksRebateValue;
	}
    if(addExtendPage.addParam.klsfRebateValue!=null && addExtendPage.addParam.klsfRebateValue!=""){
    	param.klsfRebateValue=addExtendPage.addParam.klsfRebateValue;
	}
    if(addExtendPage.addParam.dpcRebateValue!=null && addExtendPage.addParam.dpcRebateValue!=""){
    	param.dpcRebateValue=addExtendPage.addParam.dpcRebateValue;
	}
    if(addExtendPage.addParam.sscrebate!=null && addExtendPage.addParam.sscrebate!=""){
    	param.sscrebate=addExtendPage.addParam.sscrebate;
	}
    if(addExtendPage.addParam.ffcrebate!=null && addExtendPage.addParam.ffcrebate!=""){
    	param.ffcrebate=addExtendPage.addParam.ffcrebate;
	}
    if(addExtendPage.addParam.syxwRebate!=null && addExtendPage.addParam.syxwRebate!=""){
    	param.syxwRebate=addExtendPage.addParam.syxwRebate;
	}
    if(addExtendPage.addParam.ksRebate!=null && addExtendPage.addParam.ksRebate!=""){
    	param.ksRebate=addExtendPage.addParam.ksRebate;
	}
    if(addExtendPage.addParam.klsfrebate!=null && addExtendPage.addParam.klsfrebate!=""){
    	param.klsfrebate=addExtendPage.addParam.klsfrebate;
	}
    if(addExtendPage.addParam.dpcRebate!=null && addExtendPage.addParam.dpcRebate!=""){
    	param.dpcRebate=addExtendPage.addParam.dpcRebate;
	}
    if(addExtendPage.addParam.pk10Rebate!=null && addExtendPage.addParam.pk10Rebate!=""){
    	param.pk10Rebate=addExtendPage.addParam.pk10Rebate;
	}
    
	$.ajax({
		type: "POST",
        url: contextPath +"/register/addUserExtend",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(param), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		var  link = result.data;
        		showTip("邀请码生成成功！");
				$(".user_extend .header-title-child").removeClass('on');
				$(".user_extend .header-title-child2").addClass('on');
				$(".user_extend .content").removeClass('on');
				$(".user_extend .content:eq(1)").addClass('on');
				addExtendPage.getUserRegisterLinksByUid();
        	}else if(result.code == "error"){
        		var err = result.data;
        		showTip(err);
        	}
        }
	});
};

/**
 * 加载用户的所有注册地址
 */
AddExtendPage.prototype.getUserRegisterLinksByUid = function() {
	addExtendPage.queryParam.userId=currentUser.id;
	addExtendPage.queryParam.maxPageNum = null;
	$("#showAllDiv").show();
	addExtendPage.getUserRegisterLinks(addExtendPage.queryParam,addExtendPage.pageParam.pageNo);
};


/**
 * 加载用户的所有注册地址
 */
AddExtendPage.prototype.getAllUserRegisterLinks = function() {
	addExtendPage.queryParam.userId=currentUser.id;
	addExtendPage.queryParam.maxPageNum = addExtendPage.pageParam.totalRecNum;
	$("#showAllDiv").hide();
	addExtendPage.getUserRegisterLinks(addExtendPage.queryParam,addExtendPage.pageParam.pageNo);
};

/**
 * 请求加载加载用户的所有注册地址
 */
AddExtendPage.prototype.getUserRegisterLinks = function(queryParam,pageNo) {
	var param={
			maxPageNum : null
		};
	
    if(queryParam.maxPageNum!=null && queryParam.maxPageNum!=""){
    	param.maxPageNum=queryParam.maxPageNum;
	}
    
    var jsonData = {
    		"query" : param,
    		"pageNo" : pageNo,
    	};
	$.ajax({
		type: "POST",
        url: contextPath +"/register/userExtendQuery",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(jsonData), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		var data = result.data;
        		addExtendPage.showUserRegisterLinks(data);
        	}else if(result.code == "error"){
        		var err = result.data;
        		showTip(err);
        	}
        }
	})
};
/**
 * 展示用户的所有可注册地址
 */
AddExtendPage.prototype.showUserRegisterLinks = function(page) {
	var extendLinkListObj = $("#extendLinkList");
	 var registerList = page.pageContent;
	 addExtendPage.pageParam.totalRecNum=page.totalRecNum;
	var str = "";
	extendLinkListObj.html("");
	if (registerList.length > 0) {

		addExtendPage.param.linkList = registerList;

		for (var i = 0; i < registerList.length; i++) {
			var register = registerList[i];
			str += " <div class='line'>";
			str += " <div class='child child1 font-blue' onclick='addExtendPage.registerDetail(\""+register.id+"\")'>"+(register.invitationCode==null?"":register.invitationCode)+"</div>";
			str += " <div class='child child2'>" + register.createdDateStr + "</div>";	
			str += "  <div class='child child3'>" + register.useCount + "</div>";
			 str += "</div>";
				extendLinkListObj.append(str);
				str = "";
		}
	} else {
		str += "<div class='line'>";
		str += "  <div class=\"child\" style='text-align: center;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style='text-align: center;display: inline-block;'>您还没有可推广的链接.</p></div>";
		str += "</div>";
		extendLinkListObj.append(str);
		$("#showAllDiv").hide();
	}
	
};


/**
 * 删除用户注册链接按钮事件
 * 
 * @param index
 */
AddExtendPage.prototype.deleteRegisterLinkEvent = function() {
	
	addExtendPage.param.deleteLinkId = $("#deleteLinkId").val();
	if(comfirm("温馨提示！","确认删除本条记录？","取消",addExtendPage.cancleFun,"确定",addExtendPage.deleteRegisterLink)){
		
	}
	
};

AddExtendPage.prototype.cancleFun = function() {
	showTip("取消成功！");
};

/**
 * 删除用户注册链接
 * 
 * @param index
 */
AddExtendPage.prototype.deleteRegisterLink = function() {
	/*frontUserExtendAction.deleteUserRegisterLink(addExtendPage.param.deleteLinkId, function(r) {
		if (r[0] != null && r[0] == "ok") {
			
			addExtendPage.getUserRegisterLinksByUid(); // 重新加载用户的注册链接地址
			modal_toggle('.user_extend_add-modal')
			
		} else if (r[0] != null && r[0] == "error") {
			showTip(r[1]);
		} else {
			showTip("删除注册链接请求失败.");
		}
	});*/
	var param={}
	if(addExtendPage.param.deleteLinkId!=null && addExtendPage.param.deleteLinkId!=""){
		param.linkId=addExtendPage.param.deleteLinkId;
	}
	$.ajax({
		type: "POST",
        url: contextPath +"/register/deleteUserExtend",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(param), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		/*var data = result.data;*/
        		addExtendPage.getUserRegisterLinksByUid(); // 重新加载用户的注册链接地址
    			modal_toggle('.user_extend_add-modal')
        	}else if(result.code == "error"){
        		var err = result.data;
        		showTip(err);
        	}
        }
	});
};

/**
 * 展示注册详情的信息
 */
AddExtendPage.prototype.registerDetail = function(linkid) {
	addExtendPage.param.linkId=linkid;
	
	$("#deleteLinkId").val(linkid);
	addExtendPage.getExtendLink();
};

/**
 * 获取推广链接 
 */
AddExtendPage.prototype.getExtendLink = function(){
	var param={}
	if(addExtendPage.param.linkId!=null && addExtendPage.param.linkId!=""){
		param.linkId=addExtendPage.param.linkId;
	}
	$.ajax({
		type: "POST",
        url: contextPath +"/register/getUserExtendById",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(param), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		/*var data = result.data;*/
        		addExtendPage.showRegisterDetail(result.data,result.data2,result.data3);
        	}else if(result.code == "error"){
        		var err = result.data;
        		showTip(err);
        	}
        }
	});
};


/**
 * 展示注册详情的信息
 */
AddExtendPage.prototype.showRegisterDetail = function(userLink,pcDomains,mobileDomains) {
		var extendLink = userLink;
		var linkSets = extendLink.linkSets;
		var registerLinkArray = linkSets.split("&");
		var linkDomain = extendLink.linkContent.substring(0, extendLink.linkContent.indexOf("/reg"));
		//var mobileDetailLinkName = linkDomain + "/mobile/user/mobile_reg.html?param1=" + extendLink.uuid;
		$("#reglink_li").html("");
		var str="<p ><span >注册链接</span></p>";
		str+='<p><span style="color:#50a3ff;">'+extendLink.registerLinks+'</span></p>';
		$("#reglink_li").append(str);
		
		$("#invitationCode").text(extendLink.invitationCode);
		$("#khlxSpan").text(extendLink.invitationCode);
		$("#msSpan").text(extendLink.invitationCode);
		
		
		// 时时彩
		if (registerLinkArray[1] != "-") {
			$("#detailSscrebate").text(registerLinkArray[1]);
		} else {
			$("#detailSscrebate").text(registerLinkArray[1]);
		}

		
		$("#detailDaiLiType").text(extendLink.dailiLevel);
		// 是否是总代
		/*if (registerLinkArray[0] != "-") {
			if(registerLinkArray[0] == "DIRECTAGENCY"){
				$("#detailDaiLiType").text("直属代理");
			}else if(registerLinkArray[0] == "GENERALAGENCY"){
				$("#detailDaiLiType").text("代理"); //总代理
			}else if(registerLinkArray[0] == "ORDINARYAGENCY"){
				$("#detailDaiLiType").text("代理");
			}else if(registerLinkArray[0] == "REGULARMEMBERS"){
				$("#detailDaiLiType").text("普通会员");
			}else{
				showTip("未知的代理类型");
			}
		} else {
			$("#detailDaiLiType").text("普通会员");
		}*/
		
		if (extendLink.loseDateStr == "") {
			$("#detailLoseDate").text("永久有效");
		} else {
			$("#detailLoseDate").text(extendLink.loseDateStr);
		}
		
		$("#detailCreateDate").text(extendLink.createdDateStr);
		if(extendLink.linkDes == null || extendLink.linkDes == ""){
			$("#detailDes").text("-");
		}else{
			$("#detailDes").text(extendLink.linkDes);
		}
 
		modal_toggle(".user_extend_add-modal");
};



/**
 * 获取本系统的最高最低模式
 */
AddExtendPage.prototype.loadUserRebate = function(){
	$.ajax({
		type: "POST",
        url: contextPath +"/system/loadSystemRebate",
        contentType :"application/json;charset=UTF-8",
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		var data=result.data;
        		addExtendPage.sscParam.sscOpenUserLowest=data.sscLowestAwardModel;
    			addExtendPage.syxwParam.syxwOpenUserLowest=data.syxwLowestAwardModel;
    			addExtendPage.dpcParam.dpcOpenUserLowest=data.dpcLowestAwardModel;
    			addExtendPage.pk10Param.pk10OpenUserLowest=data.pk10LowestAwardModel;
    			addExtendPage.ksParam.ksOpenUserLowest=data.ksLowestAwardModel;
    			addExtendPage.sscParam.sscHighestAwardModel = sscHighestAwardModel;
    			addExtendPage.syxwParam.syxwHighestAwardModel = syxwHighestAwardModel;
    			addExtendPage.dpcParam.dpcHighestAwardModel = dpcHighestAwardModel;
    			addExtendPage.pk10Param.pk10HighestAwardModel = pk10HighestAwardModel;
    			addExtendPage.ksParam.ksHighestAwardModel = ksHighestAwardModel;
    			$("#sscTip").text('');
    			$("#sscTip").text("返点"+addExtendPage.calculateRebateByAwardModelInterval(addExtendPage.sscParam.sscOpenUserHighest, addExtendPage.addParam.sscrebate, addExtendPage.sscParam.sscHighestAwardModel)+"%");
    			$("#ksTip").text('');
    			$("#ksTip").text("返点"+addExtendPage.calculateRebateByAwardModelInterval(addExtendPage.ksParam.ksOpenUserHighest, addExtendPage.addParam.ksRebate, addExtendPage.ksParam.ksHighestAwardModel)+"%");
    			$("#syxwTip").text('');
    			$("#syxwTip").text("返点"+addExtendPage.calculateRebateByAwardModelInterval(addExtendPage.syxwParam.syxwOpenUserHighest, addExtendPage.addParam.syxwRebate, addExtendPage.syxwParam.syxwHighestAwardModel)+"%");
    			$("#pk10Tip").text('');
    			$("#pk10Tip").text("返点"+addExtendPage.calculateRebateByAwardModelInterval(addExtendPage.pk10Param.pk10OpenUserHighest, addExtendPage.addParam.pk10Rebate, addExtendPage.pk10Param.pk10HighestAwardModel)+"%");
    			$("#dpcTip").text('');
    			$("#dpcTip").text("返点"+addExtendPage.calculateRebateByAwardModelInterval(addExtendPage.dpcParam.dpcOpenUserHighest, addExtendPage.addParam.dpcRebate, addExtendPage.dpcParam.dpcHighestAwardModel)+"%");
        	}else if(result.code == "error"){
        		var err = result.data;
        		showTip(err);
        	}
        }
	});
}

/**
 *  根据模式显示当前配额数
 */
AddExtendPage.prototype.showRemainNumBySscRebate= function(sscRebate){
	var sscRebateRemainNum=quotaMap.get("sscRebate"+sscRebate);
	if(sscRebateRemainNum!=null){
		$("#quotaTip").show();
		
		$("#quotaLowLevel").text(sscRebateRemainNum);
	}else{
		$("#quotaTip").hide();
	}
}
/**
 * 加载配额信息
 */
AddExtendPage.prototype.loadUserQuota = function(){
	$.ajax({
		type: "POST",
        url: contextPath +"/user/loadUserQuota",
        contentType :"application/json;charset=UTF-8",
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		var data=result.data;
        		var userQuotaList = data.pageContent;
    			for(var i=0;i< userQuotaList.length;i++){
    				var userQuota = userQuotaList[i];
    				quotaMap.put("sscRebate"+userQuota.sscRebate,userQuota.remainNum);
    			}
    			addExtendPage.showRemainNumBySscRebate(addExtendPage.sscParam.sscOpenUserHighest-2);
        	}else if(result.code == "error"){
        		var err = result.data;
        		showTip(err);
        	}
        }
	});
};

/**
 * 计算用户自身保留返点
 */
AddExtendPage.prototype.calculateRebateByAwardModelInterval = function(highRebate, lowRebate, highestAwardModel) {
	var rebate = (highRebate - lowRebate)/highestAwardModel;
	rebate = (rebate * 100).toFixed(2);
	return rebate;
}