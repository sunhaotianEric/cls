function UserListPage(){};

var userListPage = new UserListPage();

userListPage.param = {
		teamLeaderName : null,
		userName : null,
		registerDateStart : null,
		registerDateEnd : null,
		balanceStart : null,
		balanceEnd : null,
		dailiLevel:null,
		orderSort:null,
		isOnline:null,
		isQuerySub:false   //是否同时查询下级用户
		
};

userListPage.pageParam={
	     pageNo : 1,
        pageMaxNo : 1,  //最大的页数
        skipHtmlStr : ""
};

$(document).ready(function(){
	$(".i-person").parent().addClass("on");
	userListPage.loadSubTeamListUser();
});
userListPage.param = {
		teamLeaderName : null,
		userName : null,
		registerDateStart : null,
		registerDateEnd : null,
		balanceStart : null,
		balanceEnd : null,
		dailiLevel:null,
		orderSort:null,
		isOnline:null,
		isQuerySub:false   //是否同时查询下级用户
		
};
UserListPage.prototype.getTeamuserListPage = function(teamUserQuery){
	var param={
		teamLeaderName:null,
		userName:null,
		registerDateStart:null,
		registerDateEnd:null,
		balanceStart:null,
		balanceEnd:null,
		dailiLevel:null,
		orderSort:null,
		isOnline:null,
		isQuerySub:null,
		orderSort:null
	};
	if(teamUserQuery.teamLeaderName!=null && teamUserQuery.teamLeaderName!=""){
		param.teamLeaderName=teamUserQuery.teamLeaderName;
	}
    if(teamUserQuery.userName!=null && teamUserQuery.userName!=""){
    	param.userName=teamUserQuery.userName;
	}
    if(teamUserQuery.registerDateStart!=null && teamUserQuery.registerDateStart!=""){
    	param.registerDateStart=new Date(teamUserQuery.registerDateStart);
	}
    if(teamUserQuery.registerDateEnd!=null && teamUserQuery.registerDateEnd!=""){
    	param.registerDateEnd=new Date(teamUserQuery.registerDateEnd);
	}
    if(teamUserQuery.balanceStart!=null && teamUserQuery.balanceStart!=""){
    	param.balanceStart=teamUserQuery.balanceStart;
	}
    if(teamUserQuery.balanceEnd!=null && teamUserQuery.balanceEnd!=""){
    	param.balanceEnd=teamUserQuery.balanceEnd;
	}
    if(teamUserQuery.dailiLevel!=null && teamUserQuery.dailiLevel!=""){
    	param.dailiLevel=teamUserQuery.dailiLevel;
	}
    if(teamUserQuery.orderSort!=null && teamUserQuery.orderSort!=""){
    	param.orderSort=teamUserQuery.orderSort;
	}
    if(teamUserQuery.isOnline!=null && teamUserQuery.isOnline!=""){
    	param.isOnline=teamUserQuery.isOnline;
	}
    if(teamUserQuery.isQuerySub!=null && teamUserQuery.isQuerySub!=""){
    	param.isQuerySub=teamUserQuery.isQuerySub;
	}
    if(userListPage.pageParam.pageNo!=null && userListPage.pageParam.pageNo!=""){
    	param.pageNo=userListPage.pageParam.pageNo;
	}
	var jsonData = {
		"query" : param,
		"pageNo" : userListPage.pageParam.pageNo,
	};
	$.ajax({
		type: "POST",
        url: contextPath +"/user/userListQuery",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(jsonData), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		var data = result.data;
        		userListPage.refreshUserTeamListUsersPages(data);
        	}else if(result.code == "error"){
        		var err = result.data;
        		showTip(err);
        	}
        }
	});
}

/**
 * 刷新列表
 */
UserListPage.prototype.refreshUserTeamListUsersPages=function(page){
	var teamUserListObj = $("#teamUserListId");
	var teamUserList = page.pageContent;
	var str = "";
//	$("#teamUserListId").html("");
	if(teamUserList.length == 0){
		str += '<div class="line">';
    	str += 		'<h4 class="nodata">暂无数据</h4>';
    	str += '</div>';
    	teamUserListObj.append(str);
    	str="";
    	$(".more").hide();
	}else{
		
		for(var i=0; i < teamUserList.length; i++){
			var teamUser = teamUserList[i];
			
			str += '<div class="line">'
				if(teamUser.teamMemberCount > 0){
					str += '		<a href="javascript:void(0)"><div class="child child1 font-blue" onclick="userListPage.loadSubTeamListUser(\''+teamUser.userName+'\',\''+teamUser.regfrom+teamUser.userName+'\')">'+ teamUser.userName +'</div></a>';
				}else{
					str += '		<a href="javascript:void(0)"><div class="child child1">'+ teamUser.userName +'</div></a>';
				}
			str += '		<div class="child child2">'+ teamUser.money +'</div>';
			//只展示会员和代理
			
			str += '		<div class="child child3">'+ teamUser.level +'</div>';
			
			
			str += '		<div class="child child3" style="width:220px">'+ teamUser.addTimeStr.substring(0,10) +'</div>';
			str += '		<div class="child child4" style="width:120px">'+ teamUser.teamMemberCount +'</div>';
			str += '		<div class="child child5" style="width:120px">'+ teamUser.sscRebate +'</div>';
			str += '</div>'
		}
		teamUserListObj.append(str);
		str="";
		userListPage.pageParam.pageMaxNo = teamUserList.pageMaxNo;
		if(teamUserList.length < 15){
			$(".more").hide();
		}else{
			$(".more").show();
			$(".more").unbind("click").click(function(){
				userListPage.pageParam.pageNo ++;
				userListPage.getTeamuserListPage(teamUserQuery);
				
			});
		}
	}
}

/**
 * 加载子节点的方法,同时处理导航链接
 */
UserListPage.prototype.loadSubTeamListUser = function(teamLeaderName, regfrom){
	var userName ="";
	if(typeof(teamLeaderName) == "undefined" &&  $("#userId").val() != ""){
		userName = $("#userId").val();
	}
	
	$("#userId").val(teamLeaderName);
	$("#subTeamLink").html("");
	teamUserQuery={};
	
	if(teamLeaderName == "" || typeof(teamLeaderName) == "undefined") {
		teamUserQuery.teamLeaderName = null; 
	} else {
		teamUserQuery.teamLeaderName = teamLeaderName;
	}
	
	var dailiLevel = $("#dailiLevelId").val();
	var orderSort = $("#orderSortId").val();
	//有填写用户名查询全部下级
	var isQuerySub = false;
	if(userName != null && userName != ""){
		isQuerySub = true;
	}
	if(dailiLevel.trim() == ""){
		dailiLevel = null;
	}
	
	if(userName == ""){
		userName = null;
	}
	teamUserQuery.userName = userName;
	teamUserQuery.dailiLevel= dailiLevel;
	teamUserQuery.orderSort = orderSort;
	teamUserQuery.balanceStart=null;
	teamUserQuery.balanceEnd = null;
	teamUserQuery.registerDateStart = null;
	teamUserQuery.registerDateEnd = null;
	teamUserQuery.isOnline = false;
	teamUserQuery.isQuerySub = isQuerySub;
	if(regfrom != undefined && regfrom != null) {
		regfrom = regfrom.substring(regfrom.indexOf("&" +currentUser.userName +"&"));
		var parentUserNames = regfrom.split("&");
		//用于构造每级的regFrom
		var subRegfrom = "";
		var subLegth = 0;
		if(parentUserNames.length > 0) {
			//最后一个不处理
			subLegth = parentUserNames.length - 1;
		}
		for(var i = 1; i < subLegth; i++) {
			if(i == 1) {
				subRegfrom = parentUserNames[0] + "&";
			}
			subRegfrom += parentUserNames[i] + "&";
			//第三级下面才开始加链接
			if(i > 1) {
				var linkBlock = "<span>&nbsp;>&nbsp;<a href='javascript:void(0)' onclick='userListPage.loadSubTeamListUser(\""+parentUserNames[i]+"\", \""+subRegfrom+"\")'>"+parentUserNames[i]+"</a></span>";
				$("#subTeamLink").append(linkBlock);
			}
		}
	}
	 $("#teamUserListId").html("");
	//查询页数置为1
	 userListPage.pageParam.pageNo = 1;
	 userListPage.getTeamuserListPage(teamUserQuery);
	//userTeamListUsers.findUserTeamListUsersByQueryParam(teamLeaderName + "00");
};
