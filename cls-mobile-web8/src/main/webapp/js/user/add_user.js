function AddUserPage() {}
var userAddPage = new AddUserPage();

//用户配额Map
var quotaMap= new JS_OBJECT_MAP();

//时时彩模式参数
userAddPage.sscParam = {
	//模式差值
    sscInterval : 2,
    //当前用户时时彩奖金模式值
    sscUserAwardModel : null,
    //系统时时彩最低奖金模式值
    sscLowestAwardModel : null,
    //系统时时彩最高奖金模式值
    sscHighestAwardModel : null
};

userAddPage.ksParam = {
	ksInterval : 2,
	ksUserAwardModel : null,
	ksLowestAwardModel : null,
	ksHighestAwardModel : null
};

userAddPage.pk10Param = {
	pk10Interval : 2,
	pk10UserAwardModel : null,
	pk10LowestAwardModel : null,
	pk10HighestAwardModel : null
};

userAddPage.syxwParam = {
	syxwInterval : 2,
	syxwUserAwardModel : null,
	syxwLowestAwardModel : null,
	syxwHighestAwardModel : null
};

userAddPage.dpcParam = {
	dpcInterval : 2,
	dpcUserAwardModel : null,
	dpcLowestAwardModel : null,
	dpcHighestAwardModel : null
};


$(document).ready(function() {
	animate_add(".user_team_user_addCtrl .main","fadeInUp");
	
	userAddPage.sscParam.sscUserAwardModel = currentUser.sscRebate;
	userAddPage.ksParam.ksUserAwardModel = currentUser.ksRebate;
	userAddPage.pk10Param.pk10UserAwardModel = currentUser.pk10Rebate;
	userAddPage.syxwParam.syxwUserAwardModel = currentUser.syxwRebate;
	userAddPage.dpcParam.dpcUserAwardModel = currentUser.dpcRebate;
	
	//开户奖金模式默认当前用户模式减去模式差值
	$("#sscRebateValue").val(userAddPage.sscParam.sscUserAwardModel - userAddPage.sscParam.sscInterval);
	$("#ksRebateValue").val(userAddPage.ksParam.ksUserAwardModel - userAddPage.ksParam.ksInterval);
	$("#pk10RebateValue").val(userAddPage.pk10Param.pk10UserAwardModel - userAddPage.pk10Param.pk10Interval);
	$("#syxwRebateValue").val(userAddPage.syxwParam.syxwUserAwardModel - userAddPage.syxwParam.syxwInterval);
	$("#dpcRebateValue").val(userAddPage.dpcParam.dpcUserAwardModel - userAddPage.dpcParam.dpcInterval);
	
	$("#addUserName").blur(function(){
		var value = $(this).val();
		var reg= /^[A-Za-z]+$/;
		if(value == null || value == "" || !reg.test(value.substr(0,1)) || value.length < 4 || value.length > 10){
			showTip("用户名必须填写,4~10位字母或数字，首位为字母");
			return;
		}
		userAddPage.userDuplicate();
	});

	$("#userPassword").blur(function(){
		var value = $(this).val();
		if(value == null || value == "" || value.length < 6 || value.length > 15){
			showTip("密码必须填写,6-15个字符，建议使用字母、数字组合，混合大小写");
			return;
		}
	});

	$("#userPassword2").blur(function(){
		var value = $(this).val();
		if(value == null || value == "" || value.length < 6 || value.length > 15){
			showTip("确认密码必须填写,6-15个字符，建议使用字母、数字组合，混合大小写");
			return;
		}
	});

	// 添加用户事件
	$("#addUserButton").unbind("click").click(function() {
		$(this).attr('disabled','disabled');
		userAddPage.addUser(); // 添加用户事件
	});
	
	
	//时时彩模式增加事件
	$("#sscModeAdd").unbind("click").click(function() {
		var sscRebateValue = Number($("#sscRebateValue").val());
		if(sscRebateValue % 2 != 0){
			showTip("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		if(sscRebateValue == userAddPage.sscParam.sscUserAwardModel){
			showTip("当前时时彩模式已经是最高值了！");
			return;
		}
		sscRebateValue += userAddPage.sscParam.sscInterval; 
		if(sscRebateValue > userAddPage.sscParam.sscUserAwardModel){
			showTip("不能大于时时彩模式["+ userAddPage.sscParam.sscUserAwardModel +"]最高值.请重新输入！");
			return;
		}
		
		$("#sscRebateValue").val(sscRebateValue);
		//时时彩模式变动显示配额用户数
		userAddPage.showRemainNumBySscRebate(sscRebateValue);
		$("#sscTip").text("返点"+userAddPage.calculateRebate(userAddPage.sscParam.sscUserAwardModel, sscRebateValue, userAddPage.sscParam.sscHighestAwardModel)+"%");
	});
	
	//时时彩模式减少事件
	$("#sscModeReduction").unbind("click").click(function() {
		var sscRebateValue = Number($("#sscRebateValue").val());
		if(sscRebateValue % 2 != 0){
			showTip("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		if(sscRebateValue == userAddPage.sscParam.sscLowestAwardModel){
			showTip("当前时时彩模式已经是最低值了！");
			return;
		}
		sscRebateValue -= userAddPage.sscParam.sscInterval;
		if(sscRebateValue < userAddPage.sscParam.sscLowestAwardModel){
			showTip("不能小于时时彩模式["+ userAddPage.sscParam.sscLowestAwardModel +"]最低值.请重新输入！");
			return;
		}
		
		$("#sscRebateValue").val(sscRebateValue);
		//时时彩模式变动显示配额用户数
		userAddPage.showRemainNumBySscRebate(sscRebateValue);
		$("#sscTip").text("返点"+userAddPage.calculateRebate(userAddPage.sscParam.sscUserAwardModel, sscRebateValue, userAddPage.sscParam.sscHighestAwardModel)+"%");
	});
	
	//快三模式增加事件
	$("#ksModeAdd").unbind("click").click(function() {
		var ksRebateValue = Number($("#ksRebateValue").val());
		if(ksRebateValue % 2 != 0){
			showTip("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		if(ksRebateValue == userAddPage.ksParam.ksUserAwardModel){
			showTip("当前快三模式已经是最高值了！");
			return;
		}
		ksRebateValue += userAddPage.ksParam.ksInterval; 
		if(ksRebateValue > userAddPage.ksParam.ksUserAwardModel){
			showTip("大于快三模式["+ userAddPage.ksParam.ksUserAwardModel +"]最高值了.请重新输入！");
			return;
		}
		
		$("#ksRebateValue").val(ksRebateValue);
		$("#ksTip").text("返点"+userAddPage.calculateRebate(userAddPage.ksParam.ksUserAwardModel, ksRebateValue, userAddPage.ksParam.ksHighestAwardModel)+"%");
	});
	
	//快三模式减少事件
	$("#ksModeReduction").unbind("click").click(function() {
		var ksRebateValue = Number($("#ksRebateValue").val());
		if(ksRebateValue % 2 != 0){
			showTip("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		if(ksRebateValue == userAddPage.ksParam.ksLowestAwardModel){
			showTip("当前快三模式已经是最低值了！");
			return;
		}
		ksRebateValue -= userAddPage.ksParam.ksInterval;
		if(ksRebateValue < userAddPage.ksParam.ksLowestAwardModel){
			showTip("小于快三模式["+ userAddPage.ksParam.ksLowestAwardModel +"]最低值了.请重新输入！");
			return;
		}
		
		$("#ksRebateValue").val(ksRebateValue);
		$("#ksTip").text("返点"+userAddPage.calculateRebate(userAddPage.ksParam.ksUserAwardModel, ksRebateValue, userAddPage.ksParam.ksHighestAwardModel)+"%");
	});
	
	//pk10模式增加事件
	$("#pk10ModeAdd").unbind("click").click(function() {
		var pk10RebateValue = Number($("#pk10RebateValue").val());
		if(pk10RebateValue % 2 != 0){
			showTip("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		if(pk10RebateValue == userAddPage.pk10Param.pk10UserAwardModel){
			showTip("当前PK10模式已经是最高值了！");
			return;
		}
		pk10RebateValue += userAddPage.pk10Param.pk10Interval; 
		if(pk10RebateValue > userAddPage.pk10Param.pk10UserAwardModel){
			showTip("大于PK10模式["+ userAddPage.pk10Param.pk10UserAwardModel +"]最高值了.请重新输入！");
			return;
		}
		
		$("#pk10RebateValue").val(pk10RebateValue);
		$("#pk10Tip").text("返点"+userAddPage.calculateRebate(userAddPage.pk10Param.pk10UserAwardModel, pk10RebateValue, userAddPage.pk10Param.pk10HighestAwardModel)+"%");
	});
	
	//pk10模式减少事件
	$("#pk10ModeReduction").unbind("click").click(function() {
		var pk10RebateValue = Number($("#pk10RebateValue").val());
		if(pk10RebateValue % 2 != 0){
			showTip("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		if(pk10RebateValue == userAddPage.pk10Param.pk10LowestAwardModel){
			showTip("当前PK10模式已经是最低值了！");
			return;
		}
		pk10RebateValue -= userAddPage.pk10Param.pk10Interval;
		if(pk10RebateValue < userAddPage.pk10Param.pk10LowestAwardModel){
			showTip("小于PK10模式["+ userAddPage.pk10Param.pk10LowestAwardModel +"]最低值了.请重新输入！");
			return;
		}
		
		$("#pk10RebateValue").val(pk10RebateValue);
		$("#pk10Tip").text("返点"+userAddPage.calculateRebate(userAddPage.pk10Param.pk10UserAwardModel, pk10RebateValue, userAddPage.pk10Param.pk10HighestAwardModel)+"%");
	});
	
	//十一选五模式增加事件
	$("#syxwModeAdd").unbind("click").click(function() {
		var syxwRebateValue = Number($("#syxwRebateValue").val());
		/*if(syxwRebateValue%2!=0){
			showTip("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}*/
		if(syxwRebateValue == userAddPage.syxwParam.syxwUserAwardModel){
			showTip("当前十一选五模式已经是最高值了！");
			return;
		}
		syxwRebateValue += userAddPage.syxwParam.syxwInterval; 
		if(syxwRebateValue > userAddPage.syxwParam.syxwUserAwardModel){
			showTip("大于十一选五模式["+ userAddPage.syxwParam.syxwUserAwardModel +"]最高值了.请重新输入！");
			return;
		}
		
		
		$("#syxwRebateValue").val(syxwRebateValue);
		$("#syxwTip").text("返点"+userAddPage.calculateRebate(userAddPage.syxwParam.syxwUserAwardModel, syxwRebateValue, userAddPage.syxwParam.syxwHighestAwardModel)+"%");
	});
	
	$("#syxwModeReduction").unbind("click").click(function() {
		var syxwRebateValue = Number($("#syxwRebateValue").val());
		/*if(syxwRebateValue%2!=0){
			showTip("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}*/
		if(syxwRebateValue == userAddPage.syxwParam.syxwLowestAwardModel){
			showTip("当前十一选五模式已经是最低值了！");
			return;
		}
		syxwRebateValue -= userAddPage.syxwParam.syxwInterval;
		if(syxwRebateValue < userAddPage.syxwParam.syxwLowestAwardModel){
			showTip("小于十一选五模式["+ userAddPage.syxwParam.syxwLowestAwardModel +"]最低值了.请重新输入！");
			return;
		}
		
		$("#syxwRebateValue").val(syxwRebateValue);
		$("#syxwTip").text("返点"+userAddPage.calculateRebate(userAddPage.syxwParam.syxwUserAwardModel, syxwRebateValue, userAddPage.syxwParam.syxwHighestAwardModel)+"%");
	});
	
	
	
	//低频彩模式增加事件
	$("#dpcModeAdd").unbind("click").click(function() {
		var dpcRebateValue = Number($("#dpcRebateValue").val());
		if(dpcRebateValue % 2 != 0){
			showTip("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		if(dpcRebateValue == userAddPage.dpcParam.dpcUserAwardModel){
			showTip("当前低频彩模式已经是最高值了！");
			return;
		}
		dpcRebateValue += userAddPage.dpcParam.dpcInterval; 
		if(dpcRebateValue > userAddPage.dpcParam.dpcUserAwardModel){
			showTip("大于低频彩模式["+ userAddPage.dpcParam.dpcUserAwardModel +"]最高值了.请重新输入！");
			return;
		}
		
		$("#dpcRebateValue").val(dpcRebateValue);
		$("#dpcTip").text("返点"+userAddPage.calculateRebate(userAddPage.dpcParam.dpcUserAwardModel, dpcRebateValue, userAddPage.dpcParam.dpcHighestAwardModel)+"%");
	});
	
	//低频彩模式减少事件
	$("#dpcModeReduction").unbind("click").click(function() {
		var dpcRebateValue = Number($("#dpcRebateValue").val());
		if(dpcRebateValue % 2 != 0){
			showTip("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		if(dpcRebateValue == userAddPage.dpcParam.dpcLowestAwardModel){
			showTip("当前低频彩模式已经是最低值了！");
			return;
		}
		dpcRebateValue -= userAddPage.dpcParam.dpcInterval;
		if(dpcRebateValue < userAddPage.dpcParam.dpcLowestAwardModel){
			showTip("小于低频彩模式["+ userAddPage.dpcParam.dpcLowestAwardModel +"]最低值了.请重新输入！");
			return;
		}
		
		$("#dpcRebateValue").val(dpcRebateValue);
		$("#dpcTip").text("返点"+userAddPage.calculateRebate(userAddPage.dpcParam.dpcUserAwardModel, dpcRebateValue, userAddPage.dpcParam.dpcHighestAwardModel)+"%");
		
	});
	
	//加载系统奖金模式最高最低值
	userAddPage.loadUserRebate();
	//加载用户的配额信息
	userAddPage.loadUserQuota();
	
});
/**
 * 添加用户
 */
AddUserPage.prototype.addUser = function() {
	
	var userName = $("#addUserName").val();
	var password = $("#userPassword").val();
	var surePassword = $("#userPassword2").val();
	
	//校验校验
	var reg= /^[A-Za-z]+$/;
	var regUserName = /^[a-zA-Z0-9]+$/;
	
	if(userName == null || userName == "" || !reg.test(userName.substr(0,1)) || userName.length < 4 || userName.length > 10
			|| !regUserName.test(userName)){
		showTip("用户名必须填写,4~10位字母或数字，首位为字母!");
		$("#addUserButton").removeAttr("disabled");
		return;
	}
	
	//密码校验
	if(password == null || password == "" || password.length < 6 || password.length > 15){
		showTip("密码必须填写,6-15个字符，建议使用字母、数字组合，混合大小写");
		$("#addUserButton").removeAttr("disabled");
		return;
	}
	
	//密码2校验
	if(surePassword == null || surePassword == "" || surePassword.length < 6 || surePassword.length > 15){
		showTip("确认密码必须填写,6-15个字符，建议使用字母、数字组合，混合大小写");
		$("#addUserButton").removeAttr("disabled");
		return;
	}
	
	//两个密码是否一致校验
	if(password != surePassword){
		showTip("两次密码输入不一致.");
		$("#addUserButton").removeAttr("disabled");
		return;
	}
	
	var userDailiType = document.getElementsByName('userDailiType'); 
	var dailiLevel = "";
	for(var i=0;i<userDailiType.length;i++){ 
		if(userDailiType[i].checked){ 
			dailiLevel = userDailiType[i].value; 
		} 
	} 

	//是否选择代理的校验
	if(dailiLevel == null || dailiLevel == ""){
		showTip("您还没选择开户类型!");
		return;
	}
	
	var sscRebateValue = Number($("#sscRebateValue").val());
	var ksRebateValue = Number($("#ksRebateValue").val());
	var pk10RebateValue = Number($("#pk10RebateValue").val());
	var syxwRebateValue = Number($("#syxwRebateValue").val());
	var dpcRebateValue = Number($("#dpcRebateValue").val());
	
	var param={}
	param.userName = userName;
	param.password = password;
	param.surePassword = surePassword;
	param.dailiLevel = dailiLevel;
	param.sscRebate = sscRebateValue;
	param.ksRebate = ksRebateValue;
	param.pk10Rebate = pk10RebateValue;
	param.syxwRebate = syxwRebateValue;
	param.dpcRebate = dpcRebateValue;
    
    $.ajax({
		type: "POST",
        url: contextPath +"/user/addUser",
        contentType :"application/json;charset=UTF-8",
        data: JSON.stringify(param), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		showTip("恭喜您,下级开户成功.");
    			window.location.href= contextPath + "/gameBet/agentCenter/userList.html";
        	}else if(result.code == "error"){
        		var err = result.data;
        		showTip(err);
        	}
        }
	});
};

/**
 * 验证用户名
 */
AddUserPage.prototype.userDuplicate = function(){
	var userName = $("#addUserName").val();
	var param={};
	param.userName=userName;
	$.ajax({
		type: "POST",
        url: contextPath +"/user/userDuplicate",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(param), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        	}else if(result.code == "error"){
        		/*var err = result.data;*/
        		showTip("必须填写,4~10位字母或数字，首位为字母");
        	}
        }
	});
};

/**
 * 加载系统奖金模式最高最低值
 */
AddUserPage.prototype.loadUserRebate = function(){
	$.ajax({
		type: "POST",
        url: contextPath +"/system/loadSystemRebate",
        contentType :"application/json;charset=UTF-8", 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		var data=result.data;
        		userAddPage.sscParam.sscLowestAwardModel = data.sscLowestAwardModel;
        		userAddPage.sscParam.sscHighestAwardModel = data.sscHighestAwardModel;
    		
        		userAddPage.ksParam.ksLowestAwardModel = data.ksLowestAwardModel;
        		userAddPage.ksParam.ksHighestAwardModel = data.ksHighestAwardModel;
        		
        		userAddPage.pk10Param.pk10LowestAwardModel = data.pk10LowestAwardModel;
        		userAddPage.pk10Param.pk10HighestAwardModel = data.pk10HighestAwardModel;
        		
        		userAddPage.syxwParam.syxwLowestAwardModel = data.syxwLowestAwardModel;
        		userAddPage.syxwParam.syxwHighestAwardModel = data.syxwHighestAwardModel;
        		
        		userAddPage.dpcParam.dpcLowestAwardModel = data.dpcLowestAwardModel;
    			userAddPage.dpcParam.dpcHighestAwardModel = data.dpcHighestAwardModel;
    			
    			$("#sscTip").text("返点"+userAddPage.calculateRebate(userAddPage.sscParam.sscUserAwardModel, userAddPage.sscParam.sscUserAwardModel - userAddPage.sscParam.sscInterval, userAddPage.sscParam.sscHighestAwardModel)+"%");
    			$("#ksTip").text("返点"+userAddPage.calculateRebate(userAddPage.ksParam.ksUserAwardModel, userAddPage.ksParam.ksUserAwardModel - userAddPage.ksParam.ksInterval, userAddPage.ksParam.ksHighestAwardModel)+"%");
    			$("#pk10Tip").text("返点"+userAddPage.calculateRebate(userAddPage.pk10Param.pk10UserAwardModel, userAddPage.pk10Param.pk10UserAwardModel - userAddPage.pk10Param.pk10Interval, userAddPage.pk10Param.pk10HighestAwardModel)+"%");
    			$("#syxwTip").text("返点"+userAddPage.calculateRebate(userAddPage.syxwParam.syxwUserAwardModel, userAddPage.syxwParam.syxwUserAwardModel - userAddPage.syxwParam.syxwInterval, userAddPage.syxwParam.syxwHighestAwardModel)+"%");
    			$("#dpcTip").text("返点"+userAddPage.calculateRebate(userAddPage.dpcParam.dpcUserAwardModel, userAddPage.dpcParam.dpcUserAwardModel - userAddPage.dpcParam.dpcInterval, userAddPage.dpcParam.dpcHighestAwardModel)+"%");
        	}else if(result.code == "error"){
        		showTip(result.data);
        	}
        }
	});
}

/**
 *  根据模式显示当前配额数
 */
AddUserPage.prototype.showRemainNumBySscRebate= function(sscRebate){
	var sscRebateRemainNum=quotaMap.get("sscRebate"+sscRebate);
	if(sscRebateRemainNum!=null){
		$("#quotaTip").show();
		$("#quotaLowLevel").text(sscRebateRemainNum);
	}else{
		$("#quotaTip").hide();
	}
}

/**
 * 加载配额信息
 */
AddUserPage.prototype.loadUserQuota = function(){
	$.ajax({
		type: "POST",
        url: contextPath +"/user/loadUserQuota",
        contentType :"application/json;charset=UTF-8",
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		var data=result.data;
        		var userQuotaList = data.pageContent;
    			for(var i=0;i< userQuotaList.length;i++){
    				var userQuota = userQuotaList[i];
    				quotaMap.put("sscRebate"+userQuota.sscRebate,userQuota.remainNum);
    			}
    			userAddPage.showRemainNumBySscRebate(userAddPage.sscParam.sscUserAwardModel - userAddPage.sscParam.sscInterval);
        	}else if(result.code == "error"){
        		showTip(result.data);
        	}
        }
	});
};

/**
 * 计算用户自身保留返点
 */
AddUserPage.prototype.calculateRebate = function(highRebate, lowRebate, highestAwardModel) {
	var rebate = (highRebate - lowRebate)/highestAwardModel;
	rebate = (rebate * 100).toFixed(2);
	return rebate;
}