/**
 * 改变展示比例
 * @type {[type]}
 */
var documentWidth=$(window).width();
var scale=documentWidth*0.3/1080;
$("#viewport").attr("content","initial-scale="+scale+", maximum-scale=1, user-scalable=no, width=device-width, target-densitydpi=device-dpi, viewport-fit=cover");

/**
 * 通用组件JS
 */
$(function(){
	/**
	 * toggle
	 */
	$(".toggle").click(function(){
		var value,html,on_status,on_class;
		on_class=$(this).attr('data-onclass');
		on_status=$(this).hasClass('on')?0:1;
		value=$(this).find(".value").eq(on_status).attr("value");
		html=$(this).find(".value").eq(on_status).html();
		if($(this).hasClass('on')){
			$(this).removeClass('on').removeClass("color-"+on_class);
			$(this).find(".ball").removeClass("font-"+on_class);
		}else{
			$(this).addClass('on').addClass("color-"+on_class);
			$(this).find(".ball").addClass("font-"+on_class);
		}
		$(this).find(".ball").html(html);
		$(this).find(".toggle-value").val(value);
	});
	
	/**
	 * checkbox
	 */
	$(".checkbox").click(function(){
		var value,html,on_status,on_class;
		on_class=$(this).attr('data-onclass');
		on_status=$(this).hasClass('on')?0:1;

		if($(this).hasClass('on')){
			$(this).removeClass('on').removeClass("color-"+on_class);
		}else{
			$(this).addClass('on').addClass("color-"+on_class);
		}
		$(this).find(".checkbox-value").val(on_status);
	});
	/**
	 * select
	 */
	$(".select").click(function(){
		$(this).find(".select-list").toggleClass('on');
	});
	$(".select-list .container").click(function(){
		var value=$(this).find(".title").attr("value");
		var html=$(this).find(".title").html();
		var parent=$(this).closest(".select");
		parent.find(".select-container .value").val(value);
		parent.find(".select-container .title").html(html);
	});
	$(".alert").each(function(i,n){
		var height=$(n).height();
		$(n).css({
			"height":height+"px",
			"bottom":"0px"
		});
	});
	
	// 直播间嵌套app，ios滚动不流畅解决方案
	//$("body").children().not(".header,.nav-bar,.lottery-bar").wrapAll("<div class='wrapper' style='-webkit-overflow-scrolling: touch; overflow: auto;height:100%;'><div></div></div>");
});


function toggle(){
	$(".toggle").unbind();
	$(".toggle").click(function(){
		var value,html,on_status,on_class;
		on_class=$(this).attr('data-onclass');
		on_status=$(this).hasClass('on')?0:1;
		value=$(this).find(".value").eq(on_status).attr("value");
		html=$(this).find(".value").eq(on_status).html();
		if($(this).hasClass('on')){
			$(this).removeClass('on').removeClass("color-"+on_class);
			$(this).find(".ball").removeClass("font-"+on_class);
		}else{
			$(this).addClass('on').addClass("color-"+on_class);
			$(this).find(".ball").addClass("font-"+on_class);
		}
		$(this).find(".ball").html(html);
		$(this).find(".toggle-value").val(value);
	});
}

/**
 * checkbox
 */
function checkbox(){
	$(".checkbox").unbind();
	$(".checkbox").click(function(){
		var value,html,on_status,on_class;
		on_class=$(this).attr('data-onclass');
		on_status=$(this).hasClass('on')?0:1;

		if($(this).hasClass('on')){
			$(this).removeClass('on').removeClass("color-"+on_class);
		}else{
			$(this).addClass('on').addClass("color-"+on_class);
		}
		$(this).find(".checkbox-value").val(on_status);
	});
}

/**
 * [comfirm description]
 * @param  {[type]} title     [description]
 * @param  {[type]} content   [description]
 * @param  {[type]} cancleTip [description]
 * @param  {[type]} cancleFun [description]
 * @param  {[type]} okTip     [description]
 * @param  {[type]} okFun     [description]
 * @return {[void]}           [description]
 */
function comfirm(title,content,cancleTip,cancleFun,okTip,okFun){

	var alertclass="alert"+new Date().getMilliseconds();

	var title='<div class="alert-head color-dark"><p>'+title+'</p><img class="close icon" src="'+contextPath+'/images/icon/icon-close.png" onclick="element_toggle(false,[\'.'+alertclass+'\'])" /></div>';
	var content='<div class="alert-content">'+content+'</div>';
	var foot='<div class="alert-foot"><div class="btn color-gray cols-50 cancleBtn" >'+cancleTip+'</div><div class="btn color-blue cols-50 okBtn" >'+okTip+'</div></div>';
	var html='<div class="alert alert-cart '+alertclass+'" >'+title+content+foot+'</div>';
	$("body>ion-nav-view.view-container .pane[nav-view='active'] ion-view[nav-view='active']").append(html);

	$("."+alertclass+" .cancleBtn").bind("click",function(){
		element_toggle(false,["."+alertclass]);
		cancleFun();
	})
	$("."+alertclass+" .okBtn").bind("click",function(){
		okFun();
	})

	element_toggle(true,["."+alertclass]);
}

/**
 * [alert_bg description]
 * @param  boolean bool 是否开启弹窗背景
 * @return void
 */
function element_toggle(bool,element,parent){
	var _parent=typeof(bool)=="undefined"?parent:$("body");
	var _bool=bool;

	//var _bool=typeof(bool)=="undefined"?true:bool;
	$.each(element,function(i,n){
		if(_bool==true) _parent.find(n).show();
		else if(_bool==false) _parent.find(n).hide();
		else if(_bool=="boolean"||typeof(_bool)=="string")
			_parent.find(n).css("display")=="none"?_parent.find(n).css("display","block"):_parent.find(n).css("display","none");
	});
	if(_bool==true||_bool=="true") _parent.find(".alert-bg").stop(false,true).fadeIn(500);
	else if(_bool==false||_bool=="false") _parent.find(".alert-bg").stop(false,true).fadeOut(500);
	else if(_bool=="boolean") _parent.find(".alert-bg").stop(false,true).fadeToggle(500);
	$(".alert").each(function(i,n){
		var height=0;
		if($(n).find(".alert-head").length>0)height+=$(n).find(".alert-head").height();
		if($(n).find(".alert-content").length>0){
			height+=$(n).find(".alert-content").height();
			height+=parseInt($(n).find(".alert-content").css("padding-top"));
			height+=parseInt($(n).find(".alert-content").css("padding-bottom"));
		}
		if($(n).find(".alert-foot").length>0)height+=$(n).find(".alert-foot").height();

		$(n).css({
			"height":height+"px",
			"bottom":"0px"
		});
	});
}

/**
 * 动画效果增加
 * @param  element element 需要加动画的对象
 * @param  String animateName 动画效果名
 * @param  Float delayTime   延时时间差
 * @param  Float delayStart   延时执行
 * @return void
 */
function animate_add(element,animateName,delayTime,delayStart){
	var _delayTime=delayTime||0.2;
	var _animateName=animateName||"fadeInLeft";
	var _delayStart=delayStart||0;
	$(element).each(function(i,n){
		var t=_delayStart+_delayTime*i;
		$(n).addClass("animated").css({
			'-webkit-animation-name': _animateName,
			'animation-name': _animateName,
			"-webkit-animation-delay":t+"s",
			"animation-delay":t+"s",
		});
	});
}


/**
 * tip弹窗
 * @param  {[type]} str   [description]
 * @param  {[type]} delay [description]
 * @return void
 */
function showTip(str,delay){
	var tipclass="tip"+new Date().getMilliseconds();
	var _delay=delay||1000;
	var html='<div class="tip '+tipclass+'" style="display: -webkit-box; display: -webkit-flex;display: -moz-box;display: -moz-flex;display: -ms-flexbox;display: flex;"><div class="tip-content"><p>'+str+'</p></div></div>';
	if($("body .view-container").length>0)
		$("body .view-container").append(html);
	else
		$("body").append(html);

	setTimeout(function(tipclass){
		$(".tip."+tipclass+" .tip-content").addClass('out');
		setTimeout(function(tipclass){
			$(".tip."+tipclass).remove();
		},1000,tipclass);
	},_delay,tipclass);
}

function diceAn(ele,delay){
	var _delay=delay||1000;
	$(ele).addClass("on");
	setTimeout(function(){
		$(ele).removeClass("on");
	},_delay);
}


/**
 * 模态窗体弹出
 * @param  {[type]} element 模态窗对象
 * @return {[type]}         [description]
 */
function modal_toggle(element){

	var parent=$("body");
	var header=parent.children('.header');
	var timeout;



	if(parent.find(element).hasClass('out')){
		clearTimeout(timeout);
		header.hide();
		parent.find(element).removeClass('out').show();
	}else{
		timeout=setTimeout(function(parent){
			parent.find(element).hide();
		},1000,parent);
		parent.find(element).addClass('out');
		header.show();
		// setTimeout(function(e){
		// 	parent.find(element).hide();
		// },1000,element);
	}
}

/**
 * [comfirm description]
 * @param  {[type]} title     [description]
 * @param  {[type]} content   [description]
 * @param  {[type]} cancleTip [description]
 * @param  {[type]} cancleFun [description]
 * @param  {[type]} okTip     [description]
 * @param  {[type]} okFun     [description]
 * @return {[void]}           [description]
 */
function comfirm(title,content,cancleTip,cancleFun,okTip,okFun){

	var alertclass="alert"+new Date().getMilliseconds();

	var title='<div class="alert-head color-dark"><p>'+title+'</p><img class="close icon" src="'+contextPath+'/images/icon/icon-close.png" onclick="element_toggle(false,[\'.'+alertclass+'\'])" /></div>';
	var content='<div class="alert-content">'+content+'</div>';
	var foot='<div class="alert-foot"><div class="btn color-gray cols-50 cancleBtn" >'+cancleTip+'</div><div class="btn color-red cols-50 okBtn" >'+okTip+'</div></div>';
	var html='<div class="alert alert-cart '+alertclass+'" >'+title+content+foot+'</div>';
	$("body").append(html);

	$("."+alertclass+" .cancleBtn").bind("click",function(){
		element_toggle(false,["."+alertclass]);
		cancleFun();
	})
	$("."+alertclass+" .okBtn").bind("click",function(){
		okFun();
	})

	element_toggle(true,["."+alertclass]);
}



function selectBank(logo,bankname){
		$(".bankname .line-banklogo img").attr("src",logo);
		$(".bankname .title").html(bankname);
		$(".bankname .value").val(bankname);
		modal_toggle('.bank-modal');
}

/**
 * select
 */
function select(){
	$(".select").unbind();
	$(".select-list .container").unbind();
	$(".select").click(function(){
		$(this).find(".select-list").toggleClass('on');
	});
	$(".select-list .container").click(function(){
		var value=$(this).find(".title").attr("value");
		var html=$(this).find(".title").html();
		var parent=$(this).closest(".select");
		parent.find(".select-container .value").val(value);
		parent.find(".select-container .title").html(html);
	});
	
}

//显示玩法或者票种
function show_classify(ele,e){
	var parent=$(e).closest(".pane[nav-view='active']");
	var display=$(ele).css("display");
	var style="boolean";
	style=display=="none"?"true":"boolean";
	parent.find(".lottery-classify").not(ele).hide();
	element_toggle(style,[ele],parent);
}

