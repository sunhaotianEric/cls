function AwardDetailPage(){};
var awardDetailPage= new AwardDetailPage();

awardDetailPage.parem={
	
}

$(document).ready(function(){
	awardDetailPage.getUserMsg();
});

/**
 * 获取用户奖金详情
 */
AwardDetailPage.prototype.getUserMsg = function(){
	
	$.ajax({
		type : "POST",
		url : contextPath + "/user/getCurrentUserInfo",
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if( result.code == "ok"){
				var resultData  = result.data;
				awardDetailPage.refreshUsersRebate(resultData);
			}else if(result.code == "error"){
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

AwardDetailPage.prototype.refreshUsersRebate = function(userMsg){
	var rebateIdObj	= $("#rebateId");
	var str = "";
	if(typeof(userMsg) != "undefined"){
		
		str += 	'<div class="main-content"  style="display:block;"><div class="container">';
		str += 		'<h2 class="main-title font-black">重庆时时彩</h2>';
		str += 		'<div class="line">';
		str += 			'<div class="sub"></div>';
		str += 			'<div class="child">';
		str += 				'<h2 class="money font-red">奖金 '+userMsg.sscRebate+'</h2>';
		str += 				'<h2 class="info font-gray">直选返点</h2>';
		str += 			'</div>';
		str += 			'<div class="child">';
		str += 				'<h2 class="money font-red">奖金 '+userMsg.sscRebate+'</h2>';
		str += 				'<h2 class="info2 font-gray">不定位返点</h2>';
		str += 			'</div>';
		str += 		'</div>';
		str += 		'<br><br>';
		str += 		'<h2 class="main-title font-black">天津时时彩</h2>';
		str += 		'<div class="line">';
		str += 			'<div class="sub"></div>';
		str += 			'<div class="child">';
		str += 				'<h2 class="money font-red">奖金 '+userMsg.sscRebate+'</h2>';
		str += 				'<h2 class="info font-gray">直选返点</h2>';
		str += 			'</div>';
		str += 			'<div class="child">';
		str += 				'<h2 class="money font-red">奖金 '+userMsg.sscRebate+'</h2>';
		str += 				'<h2 class="info2 font-gray">不定位返点</h2>';
		str += 			'</div>';
		str += 		'</div>';
		str += 		'<br><br>';
		str += 		'<h2 class="main-title font-black">新疆时时彩</h2>';
		str += 		'<div class="line">';
		str += 			'<div class="sub"></div>';
		str += 			'<div class="child">';
		str += 				'<h2 class="money font-red">奖金 '+userMsg.sscRebate+'</h2>';
		str += 				'<h2 class="info font-gray">直选返点</h2>';
		str += 			'</div>';
		str += 			'<div class="child">';
		str += 				'<h2 class="money font-red">奖金 '+userMsg.sscRebate+'</h2>';
		str += 				'<h2 class="info2 font-gray">不定位返点</h2>';
		str += 			'</div>';
		str += 		'</div>';
		str += 		'<br><br>';
		str += 		'<h2 class="main-title font-black">幸运分分彩</h2>';
		str += 		'<div class="line">';
		str += 			'<div class="sub"></div>';
		str += 			'<div class="child">';
		str += 				'<h2 class="money font-red">奖金 '+userMsg.ffcRebate+'</h2>';
		str += 				'<h2 class="info font-gray">直选返点</h2>';
		str += 			'</div>';
		str += 			'<div class="child">';
		str += 				'<h2 class="money font-red">奖金 '+userMsg.ffcRebate+'</h2>';
		str += 				'<h2 class="info2 font-gray">不定位返点</h2>';
		str += 			'</div>';
		str += 		'</div>';
		
		str += 	'</div></div>';
		
/*		str += 	'<div class="main-content"><div class="container">';
		str += 	'</div></div>';*/
		
		
		str += 	'<div class="main-content"><div class="container">';
		str += 		'<h2 class="main-title font-black">江苏快三</h2>';
		str += 		'<div class="line">';
		str += 			'<div class="sub"></div>';
		str += 			'<div class="child">';
		str += 				'<h2 class="money font-red">奖金 '+userMsg.ksRebate+'</h2>';
//		str += 				'<h2 class="info font-gray">直选返点'+userMsg.ksRebate+'</h2>';
		str += 			'</div>';
		str += 			'<div class="child">';
		str += 				'<h2 class="info2 font-gray">直选返点</h2>';
		str += 			'</div>';
		str += 		'</div>';
		str += 		'<h2 class="main-title font-black">安徽快三</h2>';
		str += 		'<div class="line">';
		str += 			'<div class="sub"></div>';
		str += 			'<div class="child">';
		str += 				'<h2 class="money font-red">奖金 '+userMsg.ksRebate+'</h2>';
//		str += 				'<h2 class="info font-gray">直选返点'+userMsg.ksRebate+'</h2>';
		str += 			'</div>';
		str += 			'<div class="child">';
		str += 				'<h2 class="info2 font-gray">直选返点</h2>';
		str += 			'</div>';
		str += 		'</div>';
		str += 		'<h2 class="main-title font-black">湖北快三</h2>';
		str += 		'<div class="line">';
		str += 			'<div class="sub"></div>';
		str += 			'<div class="child">';
		str += 				'<h2 class="money font-red">奖金 '+userMsg.ksRebate+'</h2>';
//		str += 				'<h2 class="info font-gray">直选返点'+userMsg.ksRebate+'</h2>';
		str += 			'</div>';
		str += 			'<div class="child">';
		str += 				'<h2 class="info2 font-gray">直选返点</h2>';
		str += 			'</div>';
		str += 		'</div>';
		str += 		'<h2 class="main-title font-black">北京快三</h2>';
		str += 		'<div class="line">';
		str += 			'<div class="sub"></div>';
		str += 			'<div class="child">';
		str += 				'<h2 class="money font-red">奖金 '+userMsg.ksRebate+'</h2>';
//		str += 				'<h2 class="info font-gray">直选返点'+userMsg.ksRebate+'</h2>';
		str += 			'</div>';
		str += 			'<div class="child">';
		str += 				'<h2 class="info2 font-gray">直选返点</h2>';
		str += 			'</div>';
		str += 		'</div>';
		str += 		'<h2 class="main-title font-black">幸运快三</h2>';
		str += 		'<div class="line">';
		str += 			'<div class="sub"></div>';
		str += 			'<div class="child">';
		str += 				'<h2 class="money font-red">奖金 '+userMsg.ksRebate+'</h2>';
//		str += 				'<h2 class="info font-gray">直选返点'+userMsg.ksRebate+'</h2>';
		str += 			'</div>';
		str += 			'<div class="child">';
		str += 				'<h2 class="info2 font-gray">直选返点</h2>';
		str += 			'</div>';
		str += 		'</div>';
		str += 	'</div></div>';
		
		str += 	'<div class="main-content"><div class="container">';
		str += 		'<h2 class="main-title font-black">山东11选5</h2>';
		str += 		'<div class="line">';
		str += 			'<div class="sub"></div>';
		str += 			'<div class="child">';
		str += 				'<h2 class="money font-red">奖金 '+userMsg.syxwRebate+'</h2>';
		str += 				'<h2 class="info font-gray">直选返点'+userMsg.syxwRebate+'</h2>';
		str += 			'</div>';
		str += 			'<div class="child">';
		str += 				'<h2 class="info2 font-gray">不定位返点'+userMsg.syxwRebate+'</h2>';
		str += 			'</div>';
		str += 		'</div>';
		str += 		'<br><br>';
		str += 		'<h2 class="main-title font-black">广东11选5</h2>';
		str += 		'<div class="line">';
		str += 			'<div class="sub"></div>';
		str += 			'<div class="child">';
		str += 				'<h2 class="money font-red">奖金 '+userMsg.syxwRebate+'</h2>';
		str += 				'<h2 class="info font-gray">直选返点'+userMsg.syxwRebate+'</h2>';
		str += 			'</div>';
		str += 			'<div class="child">';
		str += 				'<h2 class="info2 font-gray">不定位返点'+userMsg.syxwRebate+'</h2>';
		str += 			'</div>';
		str += 		'</div>';
		str += 		'<br><br>';
		str += 		'<h2 class="main-title font-black">江西11选5</h2>';
		str += 		'<div class="line">';
		str += 			'<div class="sub"></div>';
		str += 			'<div class="child">';
		str += 				'<h2 class="money font-red">奖金 '+userMsg.syxwRebate+'</h2>';
		str += 				'<h2 class="info font-gray">直选返点'+userMsg.syxwRebate+'</h2>';
		str += 			'</div>';
		str += 			'<div class="child">';
		str += 				'<h2 class="info2 font-gray">不定位返点'+userMsg.syxwRebate+'</h2>';
		str += 			'</div>';
		str += 		'</div>';
		str += 		'<br><br>';
		str += 		'<h2 class="main-title font-black">福建11选5</h2>';
		str += 		'<div class="line">';
		str += 			'<div class="sub"></div>';
		str += 			'<div class="child">';
		str += 				'<h2 class="money font-red">奖金 '+userMsg.syxwRebate+'</h2>';
		str += 				'<h2 class="info font-gray">直选返点'+userMsg.syxwRebate+'</h2>';
		str += 			'</div>';
		str += 			'<div class="child">';
		str += 				'<h2 class="info2 font-gray">不定位返点'+userMsg.syxwRebate+'</h2>';
		str += 			'</div>';
		str += 		'</div>';
		str += 	'</div></div>';
		
		str += 	'<div class="main-content"><div class="container">';
		str += 		'<h2 class="main-title font-black">北京PK10</h2>';
		str += 		'<div class="line">';
		str += 			'<div class="sub"></div>';
		str += 			'<div class="child">';
		str += 				'<h2 class="money font-red">奖金 '+userMsg.pk10Rebate+'</h2>';
//		str += 				'<h2 class="info font-gray">直选返点'+userMsg.pk10Rebate+'</h2>';
		str += 			'</div>';
		str += 			'<div class="child">';
		str += 				'<h2 class="info2 font-gray">直选返点</h2>';
		str += 			'</div>';
		str += 		'</div>';
		str += 	'</div></div>';
		
		str += 	'<div class="main-content"><div class="container">';
		str += 		'<h2 class="main-title font-black">低频</h2>';
		str += 		'<div class="line">';
		str += 			'<div class="sub"></div>';
		str += 			'<div class="child">';
		str += 				'<h2 class="money font-red">奖金 '+userMsg.dpcRebate+'</h2>';
		str += 				'<h2 class="info font-gray">直选返点'+userMsg.dpcRebate+'</h2>';
		str += 			'</div>';
		str += 			'<div class="child">';
		str += 				'<h2 class="info2 font-gray">不定位返点'+userMsg.dpcRebate+'</h2>';
		str += 			'</div>';
		str += 		'</div>';
		str += 	'</div></div>';
		
		rebateIdObj.append(str);
	}
};

function change(index){
	$(".main-head span").removeClass('font-red');
	$(".main-head span:eq("+index+")").addClass('font-red');
	$(".main-content").hide();
	$(".main-content:eq("+index+")").show();
}