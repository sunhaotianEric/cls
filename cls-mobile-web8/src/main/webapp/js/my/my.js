function MyPage() {

}

var myPage = new MyPage();

var message = new change();

myPage.param = {

};

$(document).ready(function() {
	$(".i-person").parent().addClass("on");

	$(".main-head .head-foot .head-referesh img").click(function() {
		var parent = $(this).closest('.head-foot');
		$(this).addClass("on");
		parent.find(".money").html("加载中...");
		setTimeout(function(parent) {
			parent.find(".head-referesh img").removeClass('on');
			// parent.find(".money").html("100.00");
		}, 2000, parent);
	});

	// 刷新余额
	$("#refreshBall").click(function() {
		setTimeout("myPage.refreshUserBalance()", 2016);
	});

	// 打开客服
	$("#customServiceHerf").click(function() {
		window.location.href = customUrl;
	});
	if (currentUser != null) {
		// 余额
		myPage.refreshUserBalance();
		// 加载头部消息数据
		myPage.getHeadNoReadMsg();
	}

	if (currentUser.dailiLevel == "REGULARMEMBERS") {
		$(".main-content a:eq(8)").hide();
		$(".main-content a:eq(9)").hide();
	}
	myPage.getNewActivitys();

	$("#biz").html(currentUser.bizSystemName);
	$("#userInfNameId").text(currentUser.userName);
	myPage.getUserLevelSystem();
	// 用户退出
	$("#logout").unbind("click").click(function() {
		myPage.userLogout();
	});

})

/**
 * 加载所有的活动中心
 */
MyPage.prototype.getNewActivitys = function() {

	$.ajax({
		type : "POST",
		url : contextPath + "/activity/getAllActivity",
		contentType : 'application/json;charset=utf-8',// 设置请求头信息;
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				myPage.refreshActivityPages(result.data);
			} else if (result.code == "error") {
				showTip(result.data);
			}
		}
	});
};

/**
 * 刷新列表数据
 */
MyPage.prototype.refreshActivityPages = function(activitys) {
	if (activitys != null && activitys.length > 0) {
		var title = "";
		var str = "";
		for (var i = 0; i < 3; i++) {
			var activity = activitys[i];
			if (activity != null && typeof (activity) != "undefined") {
				if (activity.title.length > 22) {
					title = activity.title.substring(0, 20) + "..."
				} else {
					title = activity.title;
				}
				str += "<a href='" + contextPath + "/gameBet/activityDetail.html?id=" + activity.id + "'><p style='color:#000'>" + title + "</p></a>";
			}
		}
		$(".foot").html(str);

		message.init({
			"element_move" : ".main-head .head-message .foot",
			"element" : ".main-head .head-message .foot p",
			"position_style" : "loop",
			"autoplay_time" : 2000,
			"autoplay" : true
		});
	} else {
		$(".foot").hide();
	}

}

/**
 * 刷新用户余额
 */
MyPage.prototype.refreshUserBalance = function() {

	$.ajax({
		type : "POST",
		url : contextPath + "/user/getCurrentUser",
		contentType : 'application/json;charset=UTF-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var u = result.data;
				$("#spanBall").text("¥ " + u.money.toFixed(2));
				if (u.dailiLevel != "REGULARMEMBERS") {
					$(".agencyCenter").show();
					$(".profit").show();
				}
			} else if (result.code == "error") {
				showTip(result.data);
			}
		}
	});
};

/**
 * 加载头部消息数据
 */
MyPage.prototype.getHeadNoReadMsg = function() {
	$.ajax({
		type : "POST",
		url : contextPath + "/note/getNoteToMeAndSystem",
		contentType : 'application/json;charset=UTF-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				if ((result.data + result.data2) > 0) {
					$("#msgcount").text(result.data + result.data2);
					$("#msgcount").addClass("color-red");
				}
			} else if (result.code == "error") {
				showTip(result.data);
			}
		}
	});
};

/**
 * 用户退出
 */
MyPage.prototype.userLogout = function() {

	$.ajax({
		type : "POST",
		url : contextPath + "/user/userLogout",
		contentType : 'application/json;charset=UTF-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				window.location.href = contextPath + "/gameBet/login.html";
			} else if (result.code == "error") {
				showTip(result.data);
			}
		}
	});
};

/**
 * 获取用户等级
 */
MyPage.prototype.getUserLevelSystem = function() {
	$.ajax({
		type : "POST",
		url : contextPath + "/system/getUserLevelSystem",
		contentType : 'application/json;charset=UTF-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				if (result.data2.vipLevel < result.data.length) {
					var width = result.data2.point / result.data[result.data2.vipLevel].point * 100;
					if (width > 100) {
						width = 100;
					}
					$("#progress").css("width", width + "%");
					$("#poin").html(result.data2.point + "/" + result.data[result.data2.vipLevel].point);
				} else if (result.data2.vipLevel == result.data.length) {
					var width = result.data2.point / result.data[result.data2.vipLevel - 1].point * 100;
					if (width > 100) {
						width = 100;
					}
					$("#progress").css("width", width + "%");
					$("#poin").html(result.data2.point + "/" + result.data[result.data2.vipLevel - 1].point);
				}
				$("#levelName").html(result.data2.levelName);
				$("#iconlevel").attr("class", "icon icon-level" + result.data2.vipLevel);
				$("#iconvip").attr("src", contextPath + "/images/user/userlevel/icon-VIP" + result.data2.vipLevel + ".png");
			} else if (result.code == "error") {
				showTip(result.data);
			}
		}
	});

}