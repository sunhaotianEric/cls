function UserLevelPage(){
	
}

var userLevelPage = new UserLevelPage();

$(document).ready(function(){
	userLevelPage.getUserLevelSystem();
});

/**
 * 获取当前系统下的等级机制.
 */
UserLevelPage.prototype.getUserLevelSystem=function(){
	//使用ajax请求
	$.ajax({
		type : "POST",
		url : contextPath + "/system/getUserLevelSystem",
		contentType : 'application/json;charset=UTF-8', 
		success : function(result) {
			commonHandlerResult(result, this.url);
			if( result.code == "ok"){
				var tables=$("#levelSystemTable tbody");
				tables.empty();
				var resultData = result.data;
				for(var i=0;i<resultData.length;i++){
					var level=resultData[i];
					var tr="<tr>";
						tr+="<td>VIP"+level.level+"</td>";
						tr+="<td>"+level.levelName+"</td>";
						tr+="<td>"+level.point+"</td>";
						tr+="<td>"+level.promotionAward+"</td>";
						tr+="<td>"+level.skipAward+"</td>";
						tr+="</tr>";
					tables.append(tr);
				}
			}else if(result.code == "error"){
				showTip(r[1]);
			}
		}
	});
}