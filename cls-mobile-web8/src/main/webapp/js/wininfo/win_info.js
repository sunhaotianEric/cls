function WinInfoPage() {

}

var winInfoPage = new WinInfoPage();

// 全局数组
var listData;
// 元素下标
var order = 0;

/**
 * 初始化页面
 */
$(document).ready(function() {
	/*
	 * setInterval(function() { winInfoPage.insertData(); },3000);
	 */
	$(".i-discover").parent().addClass("on");
	// 加载页面数据
	winInfoPage.insertData();
	// 滚动事件
	setInterval(function() {
		winInfoPage.showData(listData);
	}, 2000);

})

/**
 * 查询数据
 */
WinInfoPage.prototype.insertData = function() {

	$.ajax({
		type : "POST",
		url : contextPath + "/index/getNearestWinLottery",
		contentType : 'application/json;charset=utf-8',// 设置请求头信息;
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code != null && result.code == "ok") {
				// 后台加载完数据将值，赋给全局数组
				listData = result.data;
				// 元素下标取数组的长度减一
				order = result.data.length - 1;
				for (var i = 0; i < listData.length; i++) {
					var win = listData[i];
					$(".main").addClass("insetData");
					var imageId = Math.ceil(Math.random() * 20);
					$(".main").prepend(
							'<div class="line insetData" ><div class="line-headimg"><img src=\"' + contextPath + '\\images\\user\\info\\' + imageId + '.jpg"' + 'alt=""></div>'
									+ '<div class="line-content"><h2>' + win.userName + '</h2>' + '<p>在' + win.lotteryTypeDes + '喜中 <span class="font-red">¥' + win.bonus
									+ '</span></p></div></div>');

					$(".main").find(".insetData").stop(false, true).slideDown(500);

					setTimeout(function() {
						$(".main").removeClass("insetData");
						$(".main").find(".line").removeClass("insetData");
					}, 2000);
				}

			} else if (result.code != null && result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			} else {
				showErrorDlg(jQuery(document.body), "获取中奖排行的请求失败.");
			}
		}
	});

};

/**
 * 展示数据
 * 
 * @param val
 */
WinInfoPage.prototype.showData = function(val) {
	var winList = val;
	/* $(".main").html(""); */
	var str = "";
	/* for(var i = 0; i <20; i++){ */
	if (order == 0) {
		order = listData.length - 1;
	}
	// 清除页面上最后一个DIV
	$(".main").find("div.line")[listData.length - 1].remove();
	// 选择数组当中的相应的数据
	var win = winList[listData.length - 1 - order]
	$(".main").addClass("insetData");
	var imageId = Math.ceil(Math.random() * 8);
	$(".main").prepend(
			'<div class="line insetData" ><div class="line-headimg"><img src=\"' + contextPath + '\\images\\user\\info\\' + imageId + '.jpg"' + 'alt=""></div>'
					+ '<div class="line-content"><h2>' + win.userName + '</h2>' + '<p>在' + win.lotteryTypeDes + '喜中 <span class="font-red">¥' + win.bonus
					+ '</span></p></div></div>');

	$(".main").find(".insetData").stop(false, true).slideDown(500);

	setTimeout(function() {
		$(".main").removeClass("insetData");
		$(".main").find(".line").removeClass("insetData");
	}, 2000);

	order--;

	/* } */

}
