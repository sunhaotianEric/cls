function AddBankCardPage() {
}
var addBankCardPage = new AddBankCardPage();

addBankCardPage.param = {
	bankname : "",
	bankvalue : ""
};

$(document).ready(function() {

	// 初始化省份城市联动
	// addBankCardPage.selectArea($scope);
	/* _init_area(); */
	$("#city").citySelect({prov:"北京", city:"东城区"});
	// 初始化页面
	addBankCardPage.initPage();

	addBankCardPage.getBanksInfo();

	$("#subBankBtn").click(function() {
		addBankCardPage.doSaveBankInfo();
	});

	$("#bankAccount").blur(function() {
		var bankAccount = $("#bankAccount").val();
		if (bankAccount == null || $.trim(bankAccount) == "") {
			showTip("开户人姓名必须填写");
		}
	});

	$("#bankBank").blur(function() {
		var bankBank = $("#bankBank input").val();
		if (bankBank == null || $.trim(bankBank) == "") {
			showTip("请选择开户银行");
		}
	});

	$("#s_province").blur(function() {
		var province = $("#s_province").val();
		if (province == null || $.trim(province) == "") {
			showTip("请选择省份");
		}
		if ($.trim(province) == "省份") {
			showTip("请选择省份");
		}
	});

	$("#s_city").blur(function() {
		var city = $("#s_city").val();
		if (city == null || $.trim(city) == "") {
			showTip("请选择地级市");
		}
		if ($.trim(city) == "地级市") {
			showTip("请选择地级市");
		}
	});

	$("#bankNameStr").blur(function() {
		var bankName = $("#bankNameStr").html();
		if (bankName == null || $.trim(bankName) == "") {
			showTip("请填写支行名称");
		} else {
			if (bankName.length > 50) {
				showTip("支行名称必须为0-50个字符");
			}
		}
	});

	$("#bankNum").blur(function() {
		var bankNum = $("#bankNum").val();
		if (bankNum == null || $.trim(bankNum) == "") {
			showTip("请填写银行卡号");
		}
		if (luhmCheck(bankNum)) {
		} else {
			showTip("您的银行卡号输入有误，请仔细检查");
		}
	});

	$("#confirmBankNum").blur(function() {
		var bankNum = $("#bankNum").val();
		var confirmBankNum = $("#confirmBankNum").val();
		if (confirmBankNum == null || $.trim(confirmBankNum) == "") {
			showTip("请填写确认银行卡号");
		} else if (confirmBankNum != bankNum) {
			showTip("银行卡号和确认卡号不一致");
		}
	});

	$("#savePassword").blur(function() {
		var savePassword = $("#savePassword").val();
		if (savePassword == null || $.trim(savePassword) == "") {
			showTip("请填写安全密码");
		}
	});

});

/**
 * 初始化页面
 */
AddBankCardPage.prototype.initPage = function() {
	$.ajax({
		type : "POST",
		url : contextPath + "/userbank/getTrueName",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var trueName = result.data;
				if (trueName == null || $.trim(trueName) == "") {

				} else {
					$("#bankAccount").val(trueName);
					$("#bankAccount").attr("readonly", "readonly");
				}
			} else if (result.code == "error") {
				showTip(result.data);
				return;
			}
		}
	});

	$.ajax({
		type : "POST",
		url : contextPath + "/usersafe/checkIsSetSafePassword",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				
			} else if (result.code == "error") {
				
				//$("#subBankBtn").hide();
				//showTip("请先设置安全密码..三秒后跳转");
				//var str = "window.location.href='" + contextPath + "/gameBet/setting.html'";
				//setTimeout(str, 3000);
				// 显示确认框
				$(".alert-bg, .alert").show();
				$("#set").on("click",function(){
					window.location.href= contextPath + "/gameBet/setting.html";
				})
				
				/*showErrorDlg(jQuery(document.body), "操作失败，请重试");*/
			}
		}
	});
};

/**
 * 保存银行卡数据
 */
AddBankCardPage.prototype.doSaveBankInfo = function() {
	$("#subBankBtn").html("提交中...");

	var bankType = $("#bankBank input").val();
	var province = $("#s_province").val();
	var city = $("#s_city").val();
	/*var area = $("#s_county").val();*/
	var bankName = $("#bankNameStr").html();
	/*var subbranchName = $("#subbranchName").val();*/
	var bankAccount = $("#bankAccount").val();
	var bankNum = $("#bankNum").val();
	var confirmBankNum = $("#confirmBankNum").val();
	var savePassword = $("#savePassword").val();

	if (bankAccount == null || $.trim(bankAccount) == "") {
		showTip("开户人姓名必须填写");
		$("#subBankBtn").html("添加");
		return;
	}
	if (bankType == null || $.trim(bankType) == "") {
		showTip("请选择开户银行");
		$("#subBankBtn").html("添加");
		return;
	}
	if ($.trim(province) == "省份" || $.trim(city) == "地级市") {
		showTip("请选择开户所在城市");
		$("#subBankBtn").html("添加");
		return;
	}
	if (province == null || $.trim(province) == "" || city == null || $.trim(city) == "") {
		showTip("请选择开户所在城市");
		$("#subBankBtn").html("添加");
		return;
	}
	if (bankName == null || $.trim(bankName) == "") {
		showTip("请填选开户银行");
		$("#subBankBtn").html("添加");
		return;
	}
	/*if (subbranchName == null || $.trim(subbranchName) == "") {
		showTip("请填写支行名称");
		$("#subBankBtn").html("添加");
		return;
	} else {
		if (subbranchName.length > 50) {
			showTip("支行名称必须为0-50个字符");
			$("#subBankBtn").html("添加");
			return;
		}
	}*/
	if (bankNum == null || $.trim(bankNum) == "") {
		showTip("请填写银行卡号");
		$("#subBankBtn").html("添加");
		return;
	}
	if (luhmCheck(bankNum)) {
	} else {
		showTip("您的银行卡号输入有误，请仔细检查");
		$("#subBankBtn").html("添加");
		return;
	}
	var reg = new RegExp("^[0-9]*$");
	if (bankNum.length < 15 || bankNum.length > 20 || !reg.test(bankNum)) {
		showTip("银行卡号由15-19位数字组成");
		$("#subBankBtn").html("添加");
		return;
	}
	if (confirmBankNum == null || $.trim(confirmBankNum) == "") {
		showTip("请填写确认银行卡号");
		$("#subBankBtn").html("添加");
		return;
	}
	if (confirmBankNum != bankNum) {
		showTip("银行卡号和确认银行卡号不一致");
		$("#subBankBtn").html("添加");
		return;
	}
	if (savePassword == null || $.trim(savePassword) == "") {
		showTip("请填写安全密码");
		$("#subBankBtn").html("添加");
		return;
	}
	/*var user = {
		"safePassword" : savePassword,
		"trueName" : bankAccount
	};*/
	var userBank = {
		"bankType" : bankType,
		"province" : province,
		"city" : city,
		"bankName" : bankName,
		"bankAccountName" : bankAccount,
		"bankCardNum" : bankNum,
		"confirmBankNum" : confirmBankNum
	};
	var jsonData = {
		"safePwd" : savePassword,
		"userBank" : userBank,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/userbank/saveBankCard",
		data : JSON.stringify(jsonData),//将对象序列化成JSON字符串;
		dataType : "json",//设置请求参数类型
		contentType : 'application/json;charset=utf-8',//设置请求头信息;
		success : function(result) {
			commonHandlerResult(result, this.url);
			$("#subBankBtn").html("保存");
			if (result.code == "ok") {
				showTip("银行卡添加成功..2秒后自动跳转页面");
				$("#subBankBtn").html("添加");
				var str = "window.location.href='" + contextPath + "/gameBet/bankCard/bankCardList.html'";
				setTimeout(str, 2000);
			} else if (result.code == "error") {
				showTip(result.data);
				$("#subBankBtn").html("添加银行");
			}
		}
	});
};

// 获取所有的银行信息
AddBankCardPage.prototype.getBanksInfo = function() {
	$.ajax({
		type : "POST",
		url : contextPath + "/userbank/getAllBankInfos",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				addBankCardPage.refreshBanksInfo(result.data);
			} else if (result.code == "error") {
				showTip(result.data);
			}
		}
	});
};

AddBankCardPage.prototype.refreshBanksInfo = function(banks) {
	var banksObj = $("#banksId");
	banksObj.html("");
	var str = "";

	for ( var bankInfo in banks) {
		if (banks.hasOwnProperty(bankInfo)) {

			var bankImg = addBankCardPage.getBankImg(bankInfo);
			str += '<div class="container" onclick ="addBankCardPage.selectBank(\'' + bankInfo + '\',\'' + banks[bankInfo] + '\')">';
			str += '<div class="line-title"><img src=\"' + contextPath + '\\images\\bankcard_logo\\' + bankImg + '" alt=""></div>';
			str += '<div class="line-content">';
			str += '<h2 class="title" value="' + banks[bankInfo] + '">' + bankInfo + '</h2>';
			str += '</div>';
			str += '</div>';

			banksObj.append(str);
			str = "";

		}
		;
	}
};

/**
 * 开户银行
 * 
 * @param bankname
 */
AddBankCardPage.prototype.selectBank = function(bankname, bankvalue) {

	$("#bankNameStr").html(bankname);
	$('#bankBank input:hidden').val(bankvalue);
	modal_toggle('.bank-modal');
};
/*
 * guangfa.png huaxia.png weixin.png yinlian.jpg zhifubao.png
 */

AddBankCardPage.prototype.getBankImg = function(bankInfo) {
	if (typeof (bankInfo) == "undefined" || bankInfo == "") {
		return;
	}
	var img = "";
	if (bankInfo == "招商银行") {
		img = "zhaoshang.png";
	} else if (bankInfo == "中国建设银行") {
		img = "jianshe.png";
	} else if (bankInfo == "中国光大银行") {
		img = "gaungda.png";
	} else if (bankInfo == "平安银行") {
		img = "pingan.png";
	} else if (bankInfo == "中信银行") {
		img = "zhongxin.png";
	} else if (bankInfo == "兴业银行") {
		img = "xingye.png";
	} else if (bankInfo == "中国农业银行") {
		img = "nongye.png";
	} else if (bankInfo == "中国银行") {
		img = "zhongguo.png";
	} else if (bankInfo == "中国工商银行") {
		img = "gongshang.png";
	} else if (bankInfo == "交通银行") {
		img = "jiaotong.png";
	} else if (bankInfo == "中国邮政储蓄银行") {
		img = "youzheng.png";
	} else if (bankInfo == "中国民生银行") {
		img = "minsheng.png";
	} else {
		img = "yinlian.png";
	}
	return img;
};
