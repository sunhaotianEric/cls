function BankCardListPage() {
}
var bankCardListPage = new BankCardListPage();

bankCardListPage.param = {

};

$(document).ready(function() {
	//检测是否设置安全密码
	bankCardListPage.checkIsSetSafePassword();
	
	select();
	
	bankCardListPage.getBankCards();


});

/**
 * 绑定银行卡之前检测是否设置安全密码
 */
BankCardListPage.prototype.checkIsSetSafePassword = function(){
	$.ajax({
		type : "POST",
		url : contextPath + "/usersafe/checkIsSetSafePassword",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				return;
			} else if (result.code == "error") {
				showTip("请先设置安全密码...三秒后离开此页面");
				setTimeout("window.location.href= contextPath + '/gameBet/safeCenter/setSafePwd.html'", 3000);
				return;
			}
		}
	});
};

/**
 * 查询银行卡数据
 */
BankCardListPage.prototype.getBankCards = function() {
	$.ajax({
		type : "POST",
		url : contextPath + "/userbank/getUserBankCards",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				bankCardListPage.refreshBankCardListPages(result.data);
			} else if (result.code == "error") {
				showTip(result.data);
			}
		}
	});
};

/**
 * 刷新列表数据
 */
BankCardListPage.prototype.refreshBankCardListPages = function(list) {
	var userBankListObj = $("#userBankList");

	userBankListObj.html("");
	var str = "";
	var userBankLists = list;
	if (userBankLists.length == 0) {
		/*showTip("您尚未绑定任何银行卡！");*/
		// userBankListObj.append(str);
	} else {
		// 记录数据显示
		for (var i = 0; i < userBankLists.length; i++) {
			var userBankList = userBankLists[i];

			var bankType = userBankList.bankType;
			var bankPicName = bankCardListPage.getBankPic(bankType);
			str += '<li class="user-line select">';
			str += '<div class="container">';
			str += '<div class="line-title"><img src=\"' + contextPath + '\\images\\bankcard_logo\\' + bankPicName + '" alt=""></div>';
			str += '<div class="line-content">';
			str += '<h2 class="title">' + userBankList.bankName + '</h2>';
			str += '<p class="mun">' + userBankList.bankCardNum + '</p>';
			str += '</div>';
			str += '</div>';
			str += '</li>';

			userBankListObj.append(str);
			str = "";
		}
		
		$("#userBankCountId").text(userBankLists.length);
		$("#userCanBindCountId").text(5-userBankLists.length);
	}

};


/**
 * 银行卡log
 */
BankCardListPage.prototype.getBankPic = function(bankType) {
	var bankPicName = "";
	if (bankType == "BOC") {
		bankPicName = "zhongguo.png";
	} else if (bankType == "ICBC") {
		bankPicName = "gongshang.png";
	} else if (bankType == "ABC") {
		bankPicName = "nongye.png";
	} else if (bankType == "CCB") {
		bankPicName = "jianshe.png";
	} else if (bankType == "COMM") {
		bankPicName = "jiaotong.png";
	} else if (bankType == "CMB") {
		bankPicName = "zhaoshang.png";
	} else if (bankType == "PSBC") {
		bankPicName = "pingan.png";
	} else if (bankType == "CEB") {
		bankPicName = "gaungda.png";
	} else if (bankType == "CMBC") {
		bankPicName = "minsheng.png";
	} else if (bankType == "CIB") {
		bankPicName = "xingye.png";
	} else {
		bankPicName = "yinlian.png";
	}
	return bankPicName;
};
