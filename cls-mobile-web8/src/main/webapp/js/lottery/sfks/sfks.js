
function JsksPage(){}
var jsksPage = new JsksPage();
//数据集合
jsksPage.showdatas = {
		codePlans : new Array(),
		subKindVOs : new Array()
}
//基本参数
jsksPage.param = {
	unitPrice : 1,  //每注价格
	kindDes : "三分快三",
	kindName : 'SFKS',  //彩种
	kindNameType : 'KS', //大彩种拼写
	currentLotteryWins : new JS_OBJECT_MAP(),  //奖金映射 
	currentLotteryWinsLoaded : false,  //奖金映射是否加载完毕
	ommitData : null,  //遗漏数据
	hotColdData : null, //冷热数据
	ballNum :""
};

//投注参数
jsksPage.lotteryParam = {
    currentKindKey : 'HZDXDS',    //当前所选择的玩法模式,默认是和值大小单双
	currentLotteryCount : 0,       //当前投注数目
	currentLotteryPrice : 0,       //当前投注价格,按元来计算
	currentLotteryTotalCount : 0,  //当前总投注数目
	codes : null,                  //投注号码
	codesStastic : new Array(),
	codesStr : ""   //投注下方显示的号码
};

$(document).ready(function(){
	//设置数据处理器  默认时时彩的
	lotteryCommonPage.lottertyDataDeal = ksData;
	//将这个投注实例传至common js当中
	lotteryCommonPage.currentLotteryKindInstance = jsksPage; 
	//设置投注的模式
	lotteryCommonPage.lotteryParam.modelValue = currentUserKsModel;
	lowestAwardModel = ksLowestAwardModel;
	//获取当前彩种的所有奖金列表
	lotteryCommonPage.currentLotteryKindInstance.getLotteryWins(); 
	//获取当前投注模式  元角模式
	lotteryCommonPage.loadUserModelSet();
	
	setTimeout("jsksPage.initLottery()", 1);
/*	document.getElementById("codePlanList").style.display="none";
	$("#change-title").unbind("click").click(function(event){
		var value = document.getElementById("change-title").getAttribute("value");
		if(value=="0"){//投注记录>计划列表
			document.getElementById("change-title").setAttribute("value","1");
			$("#change-title").html("计划列表");
			document.getElementById("userTodayLotteryList").style.display="none";
			document.getElementById("codePlanList").style.display="block";
		}else if(value=="1"){//计划列表>投注记录
			document.getElementById("change-title").setAttribute("value","0");
			$("#change-title").html("投注记录");
			document.getElementById("codePlanList").style.display="none";
			document.getElementById("userTodayLotteryList").style.display="block";
		}
	});
	jsksPage.codePlanList(jsksPage.param);*/
});


/**
 * 获取该彩种计划列表
 */
JsksPage.prototype.codePlanList = function(param){
	var jsonData = {"lotteryType":param.kindName,"size":13};
	$.ajax({
		type : "POST",
		//设置Ajax请求改为同步请求!
		async: false,
		url : contextPath + "/codeplan/getCodePlan",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var codePlanList = result.data.codePlans;
				var subKindVOs = result.data.subKindVOs;
				jsksPage.showdatas.codePlans = result.data.codePlans;
				jsksPage.showdatas.subKindVOs = result.data.subKindVOs;
//				$("#codePlanList").html("<div class='siftings-line'><div colspan='12'>正在加载...</div></div>");
				jsksPage.showData(codePlanList,subKindVOs);
			} else if (result.code == "error") {
				showTip(result.data);
			}
		}
	});
}

/**
 * 
 */
JsksPage.prototype.showData = function(codePlans,subKindVOs,subkind){
	if(codePlans.length > 0){
		//彩种游戏下方的游戏记录
		var codePlanListObj = $("#codePlanList");
		codePlanListObj.html("");
		codePlanListObj.append("<div class='description' style='font-size: 40px;margin-left: 43%;'>江苏快3预测</div>");
		//加载彩种
		var subKindhtml = "";
		for(var j=0;j<subKindVOs.length;j++){
			subKindhtml += "<li><p data-value='"+subKindVOs[j].code
			+"' style='font-size: 28px;'>"+subKindVOs[j].description+"</p></li>";
		}
		//判断是否第一次加载彩种
		var subkindvo = "";
		if(subkind==null || subkind=="undefined"){
			subkindvo += "<input type='hidden' class='select-save' id='lotteryKind' value='"+subKindVOs[0].code+"' />"+
			"<p class='select-title'>"+subKindVOs[0].description+"</p>";
		}else {
			subkindvo += "<input type='hidden' class='select-save' id='lotteryKind' value='"+subkind.code+"' />"+
			"<p class='select-title child2extend'>"+subkind.description+"</p>";
		}
		var str = "";
		str += "<div class='line'>";
		str += "	<div class='child child1extend'><p>期数</p></div>";
		str += "	<div class='child child2extend'><div class='line-content select-content sort' id='sortlist'>"+subkindvo+
		"<ul class='select-list myList' id='lotteryKinds' style='display: none;'>"+subKindhtml+"</ul></div></div>";
		str += "	<div class='child child3extend'><p>开奖号码</p></div>";
		str += "	<div class='child child4extend'><p>期号</p></div>";
		subkindvo = "";
		subKindhtml = "";
		str += "	<div class='child child5extend'><p>中奖状态</p></div>";
		str += "</div>";
		codePlanListObj.append(str);
		str = "";
		var playkindvalue = $("#lotteryKind").val();
		for(var i = 0; i < codePlans.length; i++){
			var codeplan = codePlans[i];
			if(codeplan.playkind == playkindvalue){
				str += "<div class='line'>";
				str += "	<div class='child child1extend'><p>"+ codeplan.expectsPeriod +"</p></div>";
				var kongge1 = "";
				if(codeplan.codesDesc.length>20){
					for(var j=0;j<codeplan.codesDesc.length/4;j++){
						kongge1 = kongge1 + '&nbsp;';
					}
				}
				var kongge2 = "";
				if(codeplan.openCode.length>20){
					for(var j=0;j<codeplan.openCode.length/4;j++){
						kongge2 = kongge2 + '&nbsp;';
					}
				}
				if(codeplan.codesDesc.length>20){
					str += "	<div class='child child2extend'><p  style='font-size: 28px;'>"+ kongge1 + codeplan.codesDesc.substring(0,codeplan.codesDesc.length/2)+'<br/>'+ kongge1
					+codeplan.codesDesc.substring(codeplan.codesDesc.length/2,codeplan.codesDesc.length) +"</p></div>";
				}else {
					str += "	<div class='child child2extend'><p  style='font-size: 28px;'>"+ codeplan.codesDesc +"</p></div>";
				}
				if(codeplan.openCode=="" || codeplan.enabled==0){
					str += "	<div class='child child3extend'><p>"+codeplan.prostateDesc+"</p></div>";
				}else{
					if(codeplan.openCode.length>20){
						str += "	<div class='child child3extend'><p  style='font-size: 28px;'>"+  kongge2 + codeplan.openCode.substring(0,codeplan.openCode.length/2)+'<br/>'+  kongge2
						+codeplan.openCode.substring(codeplan.openCode.length/2,codeplan.openCode.length) +"</p></div>";
					}else {
						str += "	<div class='child child3extend'><p>【"+ codeplan.openCode +"】</p></div>";
					}
				}
				if(codeplan.expectShort==""){
					str += "	<div class='child child4extend'><p>"+codeplan.prostateDesc+"</p></div>";
				}else{
					str += "	<div class='child child4extend'><p>"+ codeplan.expectShort +"期</p></div>";
				}
				str += "	<div class='child child5extend'><p class='cp-yellow'>"+codeplan.prostateDesc+"</p></div>";
				str += "</div>";
				codePlanListObj.append(str);
			}
			str = "";
		}
		$(".select-title").unbind("click").click(function(event){
			if(document.getElementById('lotteryKinds').getAttribute('style')=="display: none;"){
				document.getElementById('lotteryKinds').setAttribute('style','display: block;')
			}else{
				document.getElementById('lotteryKinds').setAttribute('style','display: none;')
			}
		});
		$("#lotteryKinds li").unbind("click").click("unbind",function(){
			var parent_select=$(this).closest(".select-list");
			parent_select.find("li").removeClass("on");
			$(this).addClass("on");
			var parent=$(this).closest(".select-content");
			var val=$(this).html();
			
			var html=$(val).html();
			var valLen=$(val).html().length;
			if(!isNaN(parseInt(html)))valLen=valLen/2;
			valLen=valLen*16+64;
			parent.css("width",valLen+"px")
			
			var realVal = $(this).find("p").attr("data-value");
			parent.find(".select-save").val(realVal);
			parent.find(".select-title").html(val);
			var subkind = {"code":realVal,"description":html};
			jsksPage.showData(jsksPage.showdatas.codePlans,jsksPage.showdatas.subKindVOs,subkind);
		});
	}
}


/**
 * 获取开奖号码前台展现的字符串
 */
JsksPage.prototype.getOpenCodeStr = function(openCodeNum){
	var openCodeStr = "";
	if(isNotEmpty(openCodeNum)){
		var expectDay = openCodeNum.substring(0,openCodeNum.length - 3);
		var expectNum = openCodeNum.substring(openCodeNum.length - 3,openCodeNum.length);
		openCodeStr = expectDay + "-" + expectNum;
	}
	return openCodeStr;
};

/**
 * 查找奖金对照表
 */
JsksPage.prototype.getLotteryWins = function(){
	//还未加载奖金的
	if(!jsksPage.param.currentLotteryWinsLoaded) {
		var jsonData = {
				"lotteryKind" : jsksPage.param.kindName,
			};
		$.ajax({
			type : "POST",
			// 设置请求为同步请求
			async : false,
			url : contextPath + "/lottery/getLotteryWins",
			data : JSON.stringify(jsonData),
			dataType : "json",
			contentType : 'application/json;charset=utf-8',
			success : function(result) {
				commonHandlerResult(result, this.url);
				var resultData = result.data;
				if (result.code == "ok") {
					for ( var key in resultData) {
						// 将奖金对照表缓存
						jsksPage.param.currentLotteryWins.put(key, resultData[key]);
					}
					jsksPage.param.currentLotteryWinsLoaded = true;
				} else if (result.code == "error") {
					showTip(resultData);
				}
			}
		});
	}
};

/**
 * 展示当前正在投注的期号
 */
JsksPage.prototype.showCurrentLotteryCode = function(lotteryIssue){
	if(lotteryIssue != null){
		var openCodeStr = jsksPage.getOpenCodeStr(lotteryIssue.lotteryNum);
		$('#nowNumber').text(openCodeStr);
	}
};

/**
 * 展示最新的开奖号码
 */
JsksPage.prototype.showLastLotteryCode = function(lotteryCode){
	if(lotteryCode != null){
		var lotteryNum = lotteryCode.lotteryNum;
		var openCodeStr = jsksPage.getOpenCodeStr(lotteryCode.lotteryNum);
		
		$('#J-lottery-info-lastnumber').text(openCodeStr);

		//快三展示最新的开奖号码的图片
//		lotteryCommonPage.showLastLotteryPictureCode(lotteryCode,null);
		jsksPage.showLastLotteryPictureCode(lotteryCode);
	}
};


/**
 * 展示最新的开奖号码的图片
 */
JsksPage.prototype.showLastLotteryPictureCode = function(lotteryCode){
	if(lotteryCode == null || typeof(lotteryCode)=="undefined"){
		return;
	}
	
	//先移除开奖号码样式（更新开奖号码）
	$('#lotteryNumber1').attr("src","");
	if(lotteryCode.numInfo1 == "1"){
		$('#lotteryNumber1').attr("src",""+contextPath+"/images/dice_pic/gray/1.png");
	}else if(lotteryCode.numInfo1 == "2"){
		$('#lotteryNumber1').attr("src",""+contextPath+"/images/dice_pic/gray/2.png");
	}else if(lotteryCode.numInfo1 == "3"){
		$('#lotteryNumber1').attr("src",""+contextPath+"/images/dice_pic/gray/3.png");
	}else if(lotteryCode.numInfo1 == "4"){
		$('#lotteryNumber1').attr("src",""+contextPath+"/images/dice_pic/gray/4.png");
	}else if(lotteryCode.numInfo1 == "5"){
		$('#lotteryNumber1').attr("src",""+contextPath+"/images/dice_pic/gray/5.png");
	}else if(lotteryCode.numInfo1 == "6"){
		$('#lotteryNumber1').attr("src",""+contextPath+"/images/dice_pic/gray/6.png");
	}
	
	//先移除开奖号码样式（更新开奖号码）
	$('#lotteryNumber2').attr("src","");
	if(lotteryCode.numInfo2 == "1"){
		$('#lotteryNumber2').attr("src",""+contextPath+"/images/dice_pic/gray/1.png");
	}else if(lotteryCode.numInfo2 == "2"){
		$('#lotteryNumber2').attr("src",""+contextPath+"/images/dice_pic/gray/2.png");
	}else if(lotteryCode.numInfo2 == "3"){
		$('#lotteryNumber2').attr("src",""+contextPath+"/images/dice_pic/gray/3.png");
	}else if(lotteryCode.numInfo2 == "4"){
		$('#lotteryNumber2').attr("src",""+contextPath+"/images/dice_pic/gray/4.png");
	}else if(lotteryCode.numInfo2 == "5"){
		$('#lotteryNumber2').attr("src",""+contextPath+"/images/dice_pic/gray/5.png");
	}else if(lotteryCode.numInfo2 == "6"){
		$('#lotteryNumber2').attr("src",""+contextPath+"/images/dice_pic/gray/6.png");
	}
	
	//先移除开奖号码样式（更新开奖号码）
	$('#lotteryNumber3').attr("src","");
	if(lotteryCode.numInfo3 == "1"){
		$('#lotteryNumber3').attr("src",""+contextPath+"/images/dice_pic/gray/1.png");
	}else if(lotteryCode.numInfo3 == "2"){
		$('#lotteryNumber3').attr("src",""+contextPath+"/images/dice_pic/gray/2.png");
	}else if(lotteryCode.numInfo3 == "3"){
		$('#lotteryNumber3').attr("src",""+contextPath+"/images/dice_pic/gray/3.png");
	}else if(lotteryCode.numInfo3 == "4"){
		$('#lotteryNumber3').attr("src",""+contextPath+"/images/dice_pic/gray/4.png");
	}else if(lotteryCode.numInfo3 == "5"){
		$('#lotteryNumber3').attr("src",""+contextPath+"/images/dice_pic/gray/5.png");
	}else if(lotteryCode.numInfo3 == "6"){
		$('#lotteryNumber3').attr("src",""+contextPath+"/images/dice_pic/gray/6.png");
	}
};

/**
 * 选好号码事件，添加号码到投注池中
 */
JsksPage.prototype.userLotteryNumForAddOrder = function(){
	//判断是否单式玩法
	var kindKey = jsksPage.lotteryParam.currentKindKey;
		//需要走单式的验证
//		jsksPage.singleLotteryNum();
		return jsksPage.userLotteryNum();
};

/**
 * 用户添加投注号码（复式）
 */
JsksPage.prototype.userLotteryNum = function(){
	
	if(jsksPage.lotteryParam.codes == null || jsksPage.lotteryParam.codes.length == 0){
		showTip("号码选择不完整，请重新选择！");
		return false;
	}
	
	//添加进投注统计号码池 金额\注数\倍数\最高奖金\玩法描述\号码
	var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
	var currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get(jsksPage.param.kindNameType +"_"+jsksPage.lotteryParam.currentKindKey);
	
	//如果处于修改投注号码的状态
	if(lotteryCommonPage.lotteryParam.currentIsUpdateCodeStastic && lotteryCommonPage.lotteryParam.updateCodeStasticIndex != null){
		var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[lotteryCommonPage.lotteryParam.updateCodeStasticIndex];
		codeStastic[0] = jsksPage.lotteryParam.currentLotteryPrice;
		codeStastic[1] = jsksPage.lotteryParam.currentLotteryCount;
		codeStastic[2] = lotteryCommonPage.lotteryParam.beishu;
		codeStastic[3] = lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu);
		codeStastic[4] = lotteryCommonPage.lottertyDataDeal.lotteryKindMap.get(jsksPage.lotteryParam.currentKindKey);
		codeStastic[5] = jsksPage.lotteryParam.codes;
		codeStastic[6] = jsksPage.param.kindNameType +"_"+jsksPage.lotteryParam.currentKindKey;
		codeStastic[7] = codeStastic[7];

		//倍数更新
		lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStastic[7]); //先清除原来的投注号码的倍数
		lotteryCommonPage.lotteryParam.lotteryMultipleMap.put(codeStastic[7],lotteryCommonPage.lotteryParam.beishu);

		//将所选号码和玩法的映射放置map中
		var lotteryKindMap =  new JS_OBJECT_MAP();
		lotteryKindMap.put(jsksPage.lotteryParam.currentKindKey,jsksPage.lotteryParam.codes);
		lotteryCommonPage.lotteryParam.lotteryMap.remove(codeStastic[7]); //先清除原来的
		lotteryCommonPage.lotteryParam.lotteryMap.put(codeStastic[7],lotteryKindMap);
		
		//清空当前选中的号码
		//lotteryCommonPage.clearCurrentCodes();			
	
	}else{
		var isYetHave = false;
		var yetIndex = null;
		var codeStastics = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic;
		for(var i = codeStastics.length - 1; i >= 0 ; i--){
			 var codeStastic = codeStastics[i];
		     var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
			 if(kindKey == jsksPage.lotteryParam.currentKindKey && codeStastic[5].toString() == jsksPage.lotteryParam.codes){
				 isYetHave = true;
				 yetIndex = i;
				 break;
			 }
		}
		
		if(isYetHave && yetIndex != null){
			showTip("您选择的号码在号码篮已存在，将直接进行倍数累加!");
			var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[yetIndex];
			codeStastic[0] = (parseFloat(codeStastic[0]) + parseFloat(jsksPage.lotteryParam.currentLotteryPrice)).toFixed(lotteryCommonPage.param.toFixedValue);
			codeStastic[1] = codeStastic[1];
			codeStastic[2] = parseInt(codeStastic[2]) + lotteryCommonPage.lotteryParam.beishu;
			codeStastic[3] = (parseFloat(codeStastic[3]) + parseFloat(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu))).toFixed(lotteryCommonPage.param.toFixedValue);
			codeStastic[4] = lotteryCommonPage.lottertyDataDeal.lotteryKindMap.get(jsksPage.lotteryParam.currentKindKey);
			codeStastic[5] = jsksPage.lotteryParam.codes;
			codeStastic[6] = jsksPage.param.kindNameType +"_"+jsksPage.lotteryParam.currentKindKey;
			codeStastic[7] = codeStastic[7];
			
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStastic[7]); //先清除原来的投注号码的倍数
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put(codeStastic[7],codeStastic[2]);
		}else{
			jsksPage.lotteryParam.currentLotteryTotalCount++;  //投注数目递增
			//将所选号码和玩法的映射放置map中
			var lotteryKindMap =  new JS_OBJECT_MAP();
			lotteryKindMap.put(jsksPage.lotteryParam.currentKindKey,jsksPage.lotteryParam.codes);
			lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_"+jsksPage.lotteryParam.currentLotteryTotalCount,lotteryKindMap);
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_"+jsksPage.lotteryParam.currentLotteryTotalCount,lotteryCommonPage.lotteryParam.beishu);
			
			//进入投注池当中
			var codeRecord = new Array();
			codeRecord.push(jsksPage.lotteryParam.currentLotteryPrice);
			codeRecord.push(jsksPage.lotteryParam.currentLotteryCount);
			codeRecord.push(lotteryCommonPage.lotteryParam.beishu);
			codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu));
			codeRecord.push(lotteryCommonPage.lottertyDataDeal.lotteryKindMap.get(jsksPage.lotteryParam.currentKindKey));
			codeRecord.push(jsksPage.lotteryParam.codes);
			codeRecord.push(jsksPage.param.kindNameType +"_"+jsksPage.lotteryParam.currentKindKey); //标识玩法
			codeRecord.push("lottery_id_"+jsksPage.lotteryParam.currentLotteryTotalCount); //标识该条记录的lotteryMap
			jsksPage.lotteryParam.codesStastic.push(codeRecord);			
		}
	}
	
	//选好号码统计金额注数
	lotteryCommonPage.codeStastics(); 
	return true;
};



/**
 * 根据彩种玩法,获取所选的号码
 * @param kindKey
 */
JsksPage.prototype.getCodesByKindKey = function(){
    var codes = "";
    var lotteryCount = 0;
    
	if(jsksPage.lotteryParam.currentKindKey == 'HZ' || jsksPage.lotteryParam.currentKindKey == 'SBTHBZ' 
		|| jsksPage.lotteryParam.currentKindKey == 'EBTHBZ' || jsksPage.lotteryParam.currentKindKey == 'CYG'
		|| jsksPage.lotteryParam.currentKindKey == 'DXDS' || jsksPage.lotteryParam.currentKindKey == 'HZDXDS'){
		
	    var numArrays = new Array();
        //获取 选择的号码
	    var numsStr = "";
		//获取 选择的号码
		$(".munbtn").each(function(i){
			var lis = $(this);
//				var numsStr = "";
			for(var i = 0; i < lis.length; i++){
				var li = lis[i];
				var aNum = $(li);
				if($(aNum).hasClass('on')){
					numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				}
			}
			
		});
		
		//去除最后一个分隔符
		if(numsStr.substring(numsStr.length - 1, numsStr.length) == lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT){
			numsStr = numsStr.substring(0, numsStr.length -1);
		}
		
		//不能进行空串处理
		if(numsStr != ""){
			numArrays.push(numsStr);
		}
		
		//处理投注下方显示的号码
		var codesDes = "";
		for(var i = 0; i < numArrays.length; i++){
			var numArray = numArrays[i];
			if(numArray.length == 0){ //为空
				continue;
			}
		    codesDes = codesDes + numArray + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		}
		codesDes = codesDes.substring(0, codesDes.length -1);
		jsksPage.lotteryParam.codesStr = codesDes;
		codes = codesDes;
		 //号码数目
		lotteryCount = codesDes.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length;
		if(jsksPage.lotteryParam.currentKindKey == 'HZ'
			|| jsksPage.lotteryParam.currentKindKey == 'CYG'){
			if(lotteryCount < 1){ //号码数目不足
				jsksPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				jsksPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}		
			codes = codesDes;
		}else if(jsksPage.lotteryParam.currentKindKey == 'SBTHBZ'){
			if(lotteryCount < 3){ //号码数目不足
				jsksPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				jsksPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
			//计算投注的注数
			lotteryCount =  jsksPage.getLotteryCount(jsksPage.lotteryParam.currentKindKey, lotteryCount);
			codes = codesDes;
		} else if(jsksPage.lotteryParam.currentKindKey == 'EBTHBZ') {
			if(lotteryCount < 2){ //号码数目不足
				jsksPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				jsksPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
			//计算投注的注数
			lotteryCount =  jsksPage.getLotteryCount(jsksPage.lotteryParam.currentKindKey, lotteryCount);
			codes = codesDes;
		}else if(jsksPage.lotteryParam.currentKindKey == 'DXDS'){ 
			if(numArrays.length == 0) {
				ahksPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				ahksPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
		   var num1Arrays = numArrays[0].split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT);
		   
		   if(num1Arrays.length < 2){
			   jsksPage.lotteryParam.currentLotteryCount = 1; //大小单双一次只能一注
			   lotteryCount = 1;
			   var codeDesc = codes;
			   codeDesc = codeDesc.replaceAll("1","大");
			   codeDesc = codeDesc.replaceAll("2","小");
			   codeDesc = codeDesc.replaceAll("3","单");
			   codeDesc = codeDesc.replaceAll("4","双");
			   jsksPage.lotteryParam.codesStr = codeDesc;
			   codes = codesDes;
			}else{
				if(numArrays == null || numArrays.length == 0) {
					jsksPage.lotteryParam.codesStr = $(".ball-number-current").text();
				} else {
					var codeDesc = numArrays[0];
					codeDesc = codeDesc.replaceAll("1","大");
				    codeDesc = codeDesc.replaceAll("2","小");
				    codeDesc = codeDesc.replaceAll("3","单");
				    codeDesc = codeDesc.replaceAll("4","双");
				    jsksPage.lotteryParam.codesStr = codeDesc;
				}
				jsksPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				jsksPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			};		
		}else if(jsksPage.lotteryParam.currentKindKey == 'HZDXDS'){
			jsksPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			jsksPage.lotteryParam.codes = null;  //号码置为空
			lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			return;
		} else{
		  showTip("未知参数1.");	
		}
	} else if(jsksPage.lotteryParam.currentKindKey == 'STHTX' || jsksPage.lotteryParam.currentKindKey == 'SLHTX') {
		var isChoose = false;
		//获取选择的号码
		$(".content").each(function(i){
			var lis = $(this).children();
			var numsStr = "";
			for(var i = 0; i < lis.length; i++){
				var li = lis[i];
				var aNum = $(li);
				if($(aNum).hasClass('on')){
					isChoose = true;
					break;
				}
			}
		});
		if(isChoose) {
			lotteryCount = 1;
			if(jsksPage.lotteryParam.currentKindKey == 'STHTX') {
				codes = lotteryCommonPage.lottertyDataDeal.sthtx_code;
			} else if(jsksPage.lotteryParam.currentKindKey == 'SLHTX') {
				codes = lotteryCommonPage.lottertyDataDeal.slhtx_code;
			}
		} else {
			jsksPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			jsksPage.lotteryParam.codes = null;  //号码置为空
			lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			return;
		}
	} else if(jsksPage.lotteryParam.currentKindKey == 'STHDX') {
		var numsStr = "";
        //获取选择的号码
		$(".lottery-child .container .child-content").each(function(i){  
			var aNums = $(this).children();
			for(var i=0;i<aNums.length;i++){
				var li = aNums[i];
				aNum = $(li);
				if($(aNum).hasClass('on')){
					numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT_KS;
				}
			}
		});
		//去除最后一个分隔符
		if(numsStr.substring(numsStr.length - 1, numsStr.length) == lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT_KS){
			numsStr = numsStr.substring(0, numsStr.length -1);
		}
		
		//空串拦截
		if(numsStr == ""){
		   jsksPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
		   jsksPage.lotteryParam.codes = null;  //号码置为空
		   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
		   return;
		}
		
		var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT_KS).length; //号码数目
		if(codeCount < 1){ //号码数目不足
			jsksPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			jsksPage.lotteryParam.codes = null;  //号码置为空
			lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			return;
		}			
		lotteryCount =  codeCount;
		codes = numsStr;//记录投注号码
	} else if(jsksPage.lotteryParam.currentKindKey == 'SBTHDT' || jsksPage.lotteryParam.currentKindKey == 'EBTHDT') {
		var numArrays = new Array();
		var num1Str = "";
		var num2Str = "";
		//获取选择的号码
		$(".child-content .content").each(function(i){
			var lis = $(this).children();
			for(var i = 0; i < lis.length; i++){
				var li = lis[i];
				var aNum = $(li);
				if($(aNum).hasClass('on')){
					if($(aNum).attr('name') == "ballNum_0_match"){
						num1Str += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
					}else if($(aNum).attr('name') == "ballNum_1_match"){
						num2Str += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
					}
				}
			}
			
		});
		
		//去除最后一个分隔符
		if(num1Str != ""){
			if(num1Str.substring(num1Str.length - 1, num1Str.length) == lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT){
				num1Str = num1Str.substring(0, num1Str.length -1);
			}
			if(num1Str != ""){
				numArrays.push(num1Str);
			}
		}
		if(num2Str != ""){
			if(num2Str.substring(num2Str.length - 1, num2Str.length) == lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT){
				num2Str = num2Str.substring(0, num2Str.length -1);
			}
			if(num2Str != ""){
				numArrays.push(num2Str);
			}
		}
		
		//不能进行空串处理
		
		if(numArrays.length < 2) {
			jsksPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			jsksPage.lotteryParam.codes = null;  //号码置为空
			lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			return;
		}
		var dCodeCount = 0;
		var tCodeCount = 0;
		//统计号码
		for(var i = 0; i < numArrays.length; i++){
			var numArray = numArrays[i];
			if(numArray.length == 0){ //为空,表示号码选择未合法
				jsksPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				jsksPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
			if(i == 0) {
				dCodeCount = numArray.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length;
			} else if(i == 1) {
				tCodeCount = numArray.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length;
			}
			codes = codes + numArray + "#";
		}
		codes = codes.substring(0, codes.length -1);
		//计算胆拖投注的注数
		lotteryCount =  jsksPage.getLotteryCount(jsksPage.lotteryParam.currentKindKey, dCodeCount, tCodeCount);
	} else if(jsksPage.lotteryParam.currentKindKey == 'ETHFX'){
		var numsStr = "";
        //获取选择的号码
		$(".container .child-content").each(function(){
			var lis = $(this).children();
			for(var i = 0; i < lis.length; i++){
				var li = lis[i]; 
				var aNum = $(li);
				if($(aNum).hasClass('on')){
				numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				}
			}
		});
		
		//去除最后一个分隔符
		if(numsStr.substring(numsStr.length - 1, numsStr.length) == lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT){
			numsStr = numsStr.substring(0, numsStr.length -1);
		}
		
		//空串拦截
		if(numsStr == ""){
		   jsksPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
		   jsksPage.lotteryParam.codes = null;  //号码置为空
		   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
		   return;
		}
		
		var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
		if(codeCount < 1){ //号码数目不足
			jsksPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			jsksPage.lotteryParam.codes = null;  //号码置为空
			lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			return;
		}		
		lotteryCount =  codeCount;
		codes = numsStr;//记录投注号码
	} else if(jsksPage.lotteryParam.currentKindKey == 'ETHDX') {
		var numArrays = new Array();
		//获取选择的号码
		var num1Str = "";
		var num2Str = "";
		//获取选择的号码
		$(".child-content .content").each(function(i){
			var lis = $(this).children();
			for(var i = 0; i < lis.length; i++){
				var li = lis[i];
				var aNum = $(li);
				if($(aNum).hasClass('on')){
					if($(aNum).attr('name') == "ballNum_0_match"){
						num1Str += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
					}else if($(aNum).attr('name') == "ballNum_1_match"){
						num2Str += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
					}
				}
			}
			
		});
		
		//去除最后一个分隔符
		if(num1Str != ""){
			if(num1Str.substring(num1Str.length - 1, num1Str.length) == lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT){
				num1Str = num1Str.substring(0, num1Str.length -1);
			}
			if(num1Str != ""){
				numArrays.push(num1Str);
			}
		}
		if(num2Str != ""){
			if(num2Str.substring(num2Str.length - 1, num2Str.length) == lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT){
				num2Str = num2Str.substring(0, num2Str.length -1);
			}
			if(num2Str != ""){
				numArrays.push(num2Str);
			}
		}
		
		if(numArrays.length < 2) {
			jsksPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			jsksPage.lotteryParam.codes = null;  //号码置为空
			lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			return;
		}
		var dCodeCount = 0;
		var tCodeCount = 0;
		//统计号码
		for(var i = 0; i < numArrays.length; i++){
			var numArray = numArrays[i];
			if(numArray.length == 0){ //为空,表示号码选择未合法
				jsksPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				jsksPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
			if(i == 0) {
				dCodeCount = numArray.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length;
			} else if(i == 1) {
				tCodeCount = numArray.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length;
			}
			codes = codes + numArray + "#";
		}
		codes = codes.substring(0, codes.length -1);
		//计算投注的注数
		lotteryCount =  jsksPage.getLotteryCount(jsksPage.lotteryParam.currentKindKey, dCodeCount, tCodeCount);
	} else if(jsksPage.lotteryParam.currentKindKey == 'ETHDX') {
		var numArrays = new Array();
		//获取选择的号码
		$(".l-mun").each(function(){
			var lis = $(this).children();
			var numsStr = "";
			for(var i = 0; i < lis.length; i++){
				var li = lis[i];
				var aNum = $(li);
				if($(aNum).hasClass('on')){
					numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				}
			}
			//去除最后一个分隔符
			if(numsStr.substring(numsStr.length - 1, numsStr.length) == lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT){
				numsStr = numsStr.substring(0, numsStr.length -1);
			}
			
			//不能进行空串处理
			if(numsStr != ""){
				numArrays.push(numsStr);
			}
			
		});
		if(numArrays.length < 2) {
			jsksPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			jsksPage.lotteryParam.codes = null;  //号码置为空
			lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			return;
		}
		var dCodeCount = 0;
		var tCodeCount = 0;
		//统计号码
		for(var i = 0; i < numArrays.length; i++){
			var numArray = numArrays[i];
			if(numArray.length == 0){ //为空,表示号码选择未合法
				jsksPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				jsksPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
			if(i == 0) {
				dCodeCount = numArray.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length;
			} else if(i == 1) {
				tCodeCount = numArray.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length;
			}
			codes = codes + numArray + "#";
		}
		codes = codes.substring(0, codes.length -1);
		//计算投注的注数
		lotteryCount =  jsksPage.getLotteryCount(jsksPage.lotteryParam.currentKindKey, dCodeCount, tCodeCount);
	}else{
		lotteryCount = 0;
		codes = null;
		showTip("玩法参数传递不合法.");
	}
	
	//存储当前统计的投注数目
	jsksPage.lotteryParam.currentLotteryCount = lotteryCount;  
	//当前投注号码
	jsksPage.lotteryParam.codes = codes;
	//统计当前投注数目和价格
	lotteryCommonPage.currentCodesStastic();
	
};

/**
 * 获取投注的注数
 */
JsksPage.prototype.getLotteryCount = function(currentKindKey, dCodeCount, tCodeCount){
	var lotteryCount = 0;
	if(currentKindKey == "SBTHBZ") {
		if(dCodeCount == 3) {
			lotteryCount =  1;
		} else if(dCodeCount == 4) {
			lotteryCount =  4;
		} else if(dCodeCount == 5) {
			lotteryCount =  10;
		} else if(dCodeCount == 6) {
			lotteryCount =  20;
		} 
	} else if(currentKindKey == "SBTHDT") {
		if(dCodeCount > 2 || tCodeCount == 6) {
			lotteryCount = 0;
			return lotteryCount;
		}
		if(dCodeCount == 1) {
			if(tCodeCount == 2) {
				lotteryCount = 1;
			} else if(tCodeCount == 3) {
				lotteryCount = 3;
			} else if(tCodeCount == 4) {
				lotteryCount = 6;
			} else if(tCodeCount == 5) {
				lotteryCount = 10;
			}   
		} else if(dCodeCount == 2) {
			if(tCodeCount == 1) {
				lotteryCount = 1;
			} else if(tCodeCount == 2) {
				lotteryCount = 2;
			} else if(tCodeCount == 3) {
				lotteryCount = 3;
			} else if(tCodeCount == 4) {
				lotteryCount = 4;
			} 
		}
	} else if(currentKindKey == "ETHDX") {
		lotteryCount = dCodeCount * tCodeCount;
	} else if(currentKindKey == "EBTHBZ") {
		if(dCodeCount == 2) {
			lotteryCount =  1;
		} else if(dCodeCount == 3) {
			lotteryCount =  3;
		} else if(dCodeCount == 4) {
			lotteryCount =  6;
		} else if(dCodeCount == 5) {
			lotteryCount =  10;
		} else if(dCodeCount == 6) {
			lotteryCount =  15;
		}  
	} else if(currentKindKey == "EBTHDT") {
		if(dCodeCount > 1 || tCodeCount == 6) {
			lotteryCount = 0;
			return lotteryCount;
		}
		if(dCodeCount == 1) {
			if(tCodeCount == 1) {
				lotteryCount = 1;
			}else if(tCodeCount == 2) {
				lotteryCount = 2;
			} else if(tCodeCount == 3) {
				lotteryCount = 3;
			} else if(tCodeCount == 4) {
				lotteryCount = 4;
			} else if(tCodeCount == 5) {
				lotteryCount = 5;
			}   
		} 
	}
	return lotteryCount;
};

/**
 * 控制胆拖选择的号码不能相同
 * @param ballDomElement 选择的元素
 */
JsksPage.prototype.controlNotSameBall = function(ballDomElement){
	//根据当前元素的name属性判别大小单双
	if(jsksPage.lotteryParam.currentKindKey == 'HZ' || jsksPage.lotteryParam.currentKindKey == 'DXDS' ||
			jsksPage.lotteryParam.currentKindKey == 'HZDXDS'){
		//如果当前选中的name是ballNum_0_match认为是大小单双
		if($(ballDomElement).attr("name") == "ballNum_0_match") {
			jsksPage.lotteryParam.currentKindKey = "DXDS";
		} else {
			jsksPage.lotteryParam.currentKindKey = "HZ";
		}
	}
	
	//二不同号胆拖，三不同号胆拖，二同号单选  需要做控制
	if(jsksPage.lotteryParam.currentKindKey == 'SBTHDT' || jsksPage.lotteryParam.currentKindKey == 'EBTHDT'
		|| jsksPage.lotteryParam.currentKindKey == 'ETHDX') {
		var dataRealVal = $(ballDomElement).attr("data-realvalue");
		var ballName = $(ballDomElement).attr("name");
		//取得行数
		var ballIndex = ballName.substring(8, 9);
		var ballOtherIndex = "";
		if(ballIndex == "0") {
			ballOtherIndex = "1";
			//二同号单选值号码特殊处理
			if(jsksPage.lotteryParam.currentKindKey == 'ETHDX') {
				dataRealVal = dataRealVal.substring(0, 1);
			}
		} else if(ballIndex == "1") {
			ballOtherIndex = "0";
			//二同号单选值号码特殊处理
			if(jsksPage.lotteryParam.currentKindKey == 'ETHDX') {
				dataRealVal = dataRealVal + dataRealVal;
			}
		}
		//对选号进行控制，胆码拖码不能相同
		$(".child-content .content div[name=ballNum_"+ballOtherIndex+"_match]").each(function(i){
			if($(this).hasClass('on')){
				var otherDataRealVal = $(this).attr("data-realvalue");
				//判断如果值相等，则移除选中样式
				if(dataRealVal == otherDataRealVal) {
					$(this).removeClass("on");
				}
			}
		});
	} else if(jsksPage.lotteryParam.currentKindKey == 'DXDS') {
		var dataRealVal = $(ballDomElement).attr("data-realvalue");
		//对选号进行控制，大小单双不能相同
		$(".munbtn").each(function(i){
			if($(this).hasClass('on')){
				var otherDataRealVal = $(this).attr("data-realvalue");
				//判断如果值相等，则移除选中样式
				if(dataRealVal != otherDataRealVal) {
					$(this).removeClass("on");
				}
			}
		});
	} else if(jsksPage.lotteryParam.currentKindKey == 'HZ') {
		//和值清除大小单双的号码
		var dataRealValName = $(ballDomElement).attr("name");
		
		//对选号进行控制，大小单双不能相同
		$(".munbtn").each(function(i){
			if($(this).attr('name') == "ballNum_0_match"){
				//判断如果值相等，则移除选中样式
				$(this).removeClass("on");
			}
		});
	}
};


/**
 * 号码拼接
 * @param num
 */
JsksPage.prototype.codeStastics = function(codeStastic,index){
	 
	 var codeStr = "";
	 var codeDesc = codeStastic[5]; 
	 codeDesc = codeDesc.toString();
	 var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
	 if(kindKey == "DXDS"){
		 codeDesc = codeDesc.replaceAll("1","大");
		 codeDesc = codeDesc.replaceAll("2","小");
		 codeDesc = codeDesc.replaceAll("3","单");
		 codeDesc = codeDesc.replaceAll("4","双");
	 }
	 if(codeDesc.length < 15){
		 codeStr += "<span>"+codeDesc.replace(/\&/g," ")+"</span>";
	 }else{
		 codeStr +=  "<span>" + codeDesc.substring(0,15).replace(/\&/g," ") + "...</span>";
		 codeStr += "<a onclick='event.stopPropagation();jsksPage.showDsMsgDialog(this,"+index+")'>详情</a>";
		 codeStr += "</span>";
	 }
	 
	 var str = "";
	 //单式不需要更新操作
     var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
	 if(kindKey == 'WXDS' || kindKey == 'SXDS' || kindKey == 'QSZXDS' || kindKey == 'HSZXDS'
			|| kindKey == 'QEZXDS' || kindKey == "QEZXDS_G" || kindKey == "HEZXDS" || kindKey == "HEZXDS_G"
			|| kindKey == "RXSIZXDS" || kindKey == "RXEZXDS" || kindKey == "RXSZXDS"){
		 str += "<li>";
	 }else{
		 str += "<li onclick='lotteryCommonPage.updateCodeStastic(this,"+index+")'>";
	 }
	 str += "<div class='li-child li-title'>" + codeStastic[4] + codeStr + "</div>";
	 str += "<div class='li-child li-money'>¥"+ codeStastic[0] + "元</div>";
	 str += "<div class='li-child li-strip'>" + codeStastic[1] + "注</div>";
	 str += "<div class='li-child li-multiple'>" + codeStastic[2] + "倍</div>";
	 str += "<div class='li-child li-addmoney'>" + codeStastic[3] + "元</div>";
	 str += "<div class='li-child li-close' data-value='"+index+"' name='deleteCurrentCodeStastic'><img src='"+ contextPath +"/front/images/lottery/close.png' /></div>" 
	 str += "</li>";
	 return str;
};

/**
 * 展现投注号码
 */
JsksPage.prototype.showDsMsgDialog = function(obj,index){
	var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[index];
	var kindKeyTop = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
	
	$("#kindPlayDes").text(codeStastic[4]);
	if(kindKeyTop == "WXDS" || kindKeyTop == "WXFS"
		|| kindKeyTop == "WXZX120" || kindKeyTop == "WXZX60" || kindKeyTop == "WXZX30"
		|| kindKeyTop == "WXZX20" || kindKeyTop == "WXZX10" || kindKeyTop == "WXZX5"
		|| kindKeyTop == "WXYMBDW" || kindKeyTop == "WXEMBDW" || kindKeyTop == "WXSMBDW"
		|| kindKeyTop == "WXDWD" || kindKeyTop == "RXE" || kindKeyTop == "RXS" || kindKeyTop == "RXSI"    
		|| kindKeyTop == "RXSIZXDS" || kindKeyTop == "RXEZXDS" || kindKeyTop == "RXSZXDS"
		){     
		$("#kindPlayPositionDes").text("位置：万位、千位、百位、十位、个位");
	}else if(kindKeyTop == "SXDS" || kindKeyTop == "SXFS" || kindKeyTop == "SXZX24"
		     || kindKeyTop == "SXZX12" || kindKeyTop == "SXZX6" || kindKeyTop == "SXZX4"
		     || kindKeyTop == "SXYMBDW" || kindKeyTop == "SXEMBDW"){
		$("#kindPlayPositionDes").text("位置：千位、百位、十位、个位");
	}else if(kindKeyTop == "QSZXFS" || kindKeyTop == "QSZXDS" || kindKeyTop == "QSZXHZ"
		|| kindKeyTop == "QSZXHZ_G" || kindKeyTop == "QSZS" || kindKeyTop == "QSZL"
		|| kindKeyTop == "QSYMBDW" || kindKeyTop == "QSEMBDW"){
		$("#kindPlayPositionDes").text("位置：万位、千位、百位");
	}else if(kindKeyTop == "HSZXFS" || kindKeyTop == "HSZXDS" || kindKeyTop == "HSZXHZ"
		|| kindKeyTop == "HSZXHZ_G" || kindKeyTop == "HSZS" || kindKeyTop == "HSZL"
		|| kindKeyTop == "HSYMBDW" || kindKeyTop == "HSEMBDW"){
		$("#kindPlayPositionDes").text("位置：百位、十位、个位");
	}else if(kindKeyTop == "QEZXDS" || kindKeyTop == "QEZXFS" || kindKeyTop == "QEZXHZ"
		|| kindKeyTop == "QEZXFS_G" || kindKeyTop == "QEZXDS_G" || kindKeyTop == "QEZXHZ_G"
			|| kindKeyTop == "QEDXDS"){ 
		$("#kindPlayPositionDes").text("位置：万位、千位");
	}else if(kindKeyTop == "HEZXFS" || kindKeyTop == "HEZXDS" || kindKeyTop == "HEZXHZ"
		|| kindKeyTop == "HEZXFS_G" || kindKeyTop == "HEZXDS_G" || kindKeyTop == "HEZXHZ_G"
			|| kindKeyTop == "HEDXDS"){
		$("#kindPlayPositionDes").text("位置：十位、个位"); 
	}else{
		showTip("该单式玩法的位置还没配置.");
		return;
	}

	
	var modelTxt = $("#returnPercent").text();
	if(lotteryCommonPage.lotteryParam.awardModel == 'YUAN'){
		$("#kindPlayModel").text("元模式，" + modelTxt);
	}else if(lotteryCommonPage.lotteryParam.awardModel == 'JIAO'){
		$("#kindPlayModel").text("角模式，" + modelTxt);
	}else if(lotteryCommonPage.lotteryParam.awardModel == 'FEN'){
		$("#kindPlayModel").text("分模式，" + modelTxt);
	}else if(lotteryCommonPage.lotteryParam.awardModel == 'LI'){
		$("#kindPlayModel").text("厘模式，" + modelTxt);
	}else{
      showTip("该投注模式不合法.");
	}

	$("#kindPlayPriceMsg").text(codeStastic[1] + "注，" + lotteryCommonPage.lotteryParam.beishu + "倍, 共计" + codeStastic[0] + "元");
	$("#kindPlayCodeDes").val(codeStastic[5].replace(/\&/g," "));
	 
	//展示单式投注号码对话框
	$("#dsContentShowDialog").stop(false,true).delay(200).fadeIn(500);
	$(".alert-msg-bg").stop(false,true).fadeIn(500);
};

/**
 * 获取当前开奖号码的和值显示
 */
JsksPage.prototype.getLotteryCodeStatus = function(codeStr){
	//控制显示当前号码的形态
    var codeArray = codeStr.split(",");
    var code1 = codeArray[0];
    var code2 = codeArray[1];
    var code3 = codeArray[2];
    var kshz = parseInt(code1) + parseInt(code2) + parseInt(code3);
    return kshz;
};


/**
 * 显示当前投注号码
 * @param codeStastic
 * @param index
 */
JsksPage.prototype.showCurrentPlayKindCode = function(codesArray){
	//判断是否有位数的概念
	var isBallNumMatch = true;
	var kindKey = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey;
	if(kindKey == 'WXDS' || kindKey == 'WXZX120' || kindKey == 'WXYMBDW' || kindKey == 'WXEMBDW'
		|| kindKey == 'WXSMBDW' || kindKey == "SXDS" || kindKey == "SXZX24" || kindKey == "SXYMBDW" || kindKey == "SXEMBDW"
		|| kindKey == "QSZXDS" || kindKey == "QSZXHZ" || kindKey == "QSZXHZ_G" || kindKey == "QSZS" || kindKey == "QSZL" || kindKey == "QSYMBDW" || kindKey == "QSEMBDW"
		|| kindKey == "HSZXDS" || kindKey == "HSZXHZ" || kindKey == "HSZXHZ_G" || kindKey == "HSZS" || kindKey == "HSZL" || kindKey == "HSYMBDW" || kindKey == "HSEMBDW"
		|| kindKey == "QEZXDS" || kindKey == "QEZXHZ" || kindKey == "QEZXFS_G" || kindKey == "QEZXDS_G" || kindKey == "QEZXHZ_G"
		|| kindKey == "HEZXDS" || kindKey == "HEZXHZ" || kindKey == "HEZXFS_G" || kindKey == "HEZXDS_G" || kindKey == "HEZXHZ_G"){
		isBallNumMatch = false;
	}

	if(isBallNumMatch){
		for(var i = 0; i < codesArray.length; i++){
			if(codesArray[i].indexOf(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT) == -1){
	          	$(".l-mun div[name='ballNum_"+i+"_match']").each(function(){
	                if($(this).attr("data-realvalue") == codesArray[i]){
	            		$(this).toggleClass("on");
	                }
	        	}); 
			}else{
				var codesWeiArray = codesArray[i].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
	            for(var j = 0; j < codesWeiArray.length; j++){
	            	var codeWei = codesWeiArray[j];
	    			if(codeWei != lotteryCommonPage.lotteryParam.CODE_REPLACE){
	    	          	$(".l-mun div[name='ballNum_"+i+"_match']").each(function(){
	                        if($(this).attr("data-realvalue") == codeWei){
	                    		$(this).toggleClass("on");
	                        }
	                	});   				
	    			}
	            }
			}
		}		
	}else{
		for(var i = 0; i < codesArray.length; i++){
			if(codesArray[i].indexOf(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT) == -1){
	          	$(".l-mun div").each(function(){
	                if($(this).attr("data-realvalue") == codesArray[i]){
	            		$(this).toggleClass("on");
	                }
	        	}); 
			}else{
				var codesWeiArray = codesArray[i].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
	            for(var j = 0; j < codesWeiArray.length; j++){
	            	var codeWei = codesWeiArray[j];
	    			if(codeWei != lotteryCommonPage.lotteryParam.CODE_REPLACE){
	    	          	$(".l-mun div[name='ballNum_"+i+"_match']").each(function(){
	                        if($(this).attr("data-realvalue") == codeWei){
	                    		$(this).toggleClass("on");
	                        }
	                	});   				
	    			}
	            }
			}
		}
	}
};

/**
 * 机选
 */
JsksPage.prototype.JxCodes = function(num){
	var kindKey = jsksPage.lotteryParam.currentKindKey;
	var beishu = lotteryCommonPage.lotteryParam.beishu;
	var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
	
	var jxmax;
	var jxmin;
	var codes;
	
	//大小单双
	var dxds_code_array = new Array(1,2,3,4);
	
	//直选和值
	if (kindKey == "QSZXHZ" || kindKey == "HSZXHZ"){
		jxmin = 0;
		jxmax = 27;
	}
	//直选和值、组三和值
	else if (kindKey == "QSZXHZ_G" ||kindKey == "HSZXHZ_G"){
		jxmin = 1;
		jxmax = 26;
	}
	//前二直选和值 和 后二直选和值
	else if (kindKey == "QEZXHZ" || kindKey == "HEZXHZ"){
		jxmin = 0;
		jxmax = 18;
	}
	//前二组选和值和后二组选和值
	else if (kindKey == "QEZXHZ_G" || kindKey == "HEZXHZ_G"){
		jxmin = 1;
		jxmax = 17;
	}
	//大小单双
	else if (kindKey == "DXDS"){
		jxmin = 1;
		jxmax = 4;
	}
	else{
		jxmin = 0;
		jxmax = 9;
	}
	
	for(var i = 0;i < num; i++){
	   //五星
	   if(kindKey == 'WXFS' || kindKey == 'WXDS'){
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			var code3 = GetRndNum(jxmin,jxmax);
			var code4 = GetRndNum(jxmin,jxmax);
			var code5 = GetRndNum(jxmin,jxmax);
			
			codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code4 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + code5;
			
	   }else if(kindKey == 'WXZX120'){
		    var codeArray =  GetRndNumByAppoint(jxmin,jxmax,5,0);
			codes = codeArray[0] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
					codeArray[1] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
					codeArray[2] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
					codeArray[3] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + codeArray[4];
	   }else if(kindKey == 'WXZX60'){
		   var codeArray =  GetRndNumByAppoint(jxmin,jxmax,1,3);
			codes = codeArray[0] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			codeArray[1] + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT +
			codeArray[2] + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT +
			codeArray[3];
	   }else if(kindKey == 'WXZX30'){
		   var codeArray =  GetRndNumByAppoint(jxmin,jxmax,2,1);
			codes = codeArray[0] + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT + 
			codeArray[1] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			codeArray[2];
	   }else if(kindKey == 'WXZX20' || kindKey == 'SXZX12'){
		   var codeArray =  GetRndNumByAppoint(jxmin,jxmax,1,2);
			codes = codeArray[0] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			codeArray[1] + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT +
			codeArray[2];
	   }else if(kindKey == 'WXZX10' || kindKey == 'WXZX5' || kindKey == 'SXZX4'){
		   var codeArray =  GetRndNumByAppoint(jxmin,jxmax,1,1);
			codes = codeArray[0] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			        codeArray[1];
	   }else if(kindKey == 'SXZX6'){
		   var codeArray =  GetRndNumByAppoint(jxmin,jxmax,1,1);
			codes = codeArray[0] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			        codeArray[1];
	   }else if(kindKey == 'WXYMBDW' || kindKey == 'SXYMBDW' || kindKey == 'QSZXHZ' || kindKey == 'QSZXHZ_G'
		       || kindKey == 'QSYMBDW' || kindKey == 'HSZXHZ' || kindKey == 'HSZXHZ_G' 
		       || kindKey == 'HSYMBDW'
		       || kindKey == 'QEZXHZ' || kindKey == 'QEZXHZ_G'
		       || kindKey == 'HEZXHZ' || kindKey == 'HEZXHZ_G'){
			var code1 = GetRndNum(jxmin,jxmax);
			codes = code1;
	   }else if(kindKey == 'WXEMBDW' || kindKey == 'SXEMBDW' || kindKey == 'QSZS'
		       || kindKey == 'QSEMBDW' || kindKey == 'HSZS'
		       || kindKey == 'HSEMBDW' 
		       || kindKey == 'QEZXFS_G' || kindKey == 'QEZXDS_G'
		       || kindKey == 'HEZXFS_G' || kindKey == 'HEZXDS_G'
		       || kindKey == 'QEDXDS' || kindKey == 'HEDXDS'){
		   var codeArray =  GetRndNumByAppoint(jxmin,jxmax,2,0);
			codes = codeArray[0] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			codeArray[1];
	   }else if(kindKey == 'WXSMBDW' || kindKey == 'QSZL'|| kindKey == 'HSZL'){ 
		   var codeArray =  GetRndNumByAppoint(jxmin,jxmax,3,0);
			codes = codeArray[0] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			codeArray[1] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			codeArray[2];
	   }else if(kindKey == 'SXFS'){
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			var code3 = GetRndNum(jxmin,jxmax);
			var code4 = GetRndNum(jxmin,jxmax);
			codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code4;
	   }else if(kindKey == 'SXDS'){
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			var code3 = GetRndNum(jxmin,jxmax);
			var code4 = GetRndNum(jxmin,jxmax);
			codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code4;
	   }else if(kindKey == 'SXZX24'){
		    var codeArray =  GetRndNumByAppoint(jxmin,jxmax,4,0);
			var code1 = codeArray[0];
			var code2 = codeArray[1];
			var code3 = codeArray[2];
			var code4 = codeArray[3];
			codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code4;
	   }else if(kindKey == 'QSZXFS' || kindKey == 'QSZXDS'){
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			var code3 = GetRndNum(jxmin,jxmax);
			codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        lotteryCommonPage.lotteryParam.CODE_REPLACE;
	   }else if(kindKey == 'HSZXFS' || kindKey == 'HSZXDS'){
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			var code3 = GetRndNum(jxmin,jxmax);
			codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				    code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code3;
	   }else if(kindKey == 'QEZXFS' || kindKey == 'QEZXDS'){
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
	                code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
	                lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
	                lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
	                lotteryCommonPage.lotteryParam.CODE_REPLACE;
	   }else if(kindKey == 'HEZXFS' || kindKey == 'HEZXDS'){
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
	                lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
	                lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
		            code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
	                code2;
	   }else if(kindKey == 'YXQY'){
			var code1 = GetRndNum(jxmin,jxmax);
			codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                    lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
                    lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                    lotteryCommonPage.lotteryParam.CODE_REPLACE;
	   }else if(kindKey == 'YXHY'){
			var code1 = GetRndNum(jxmin,jxmax);
			codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                    lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                    lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                    lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                    code1;
	   }else if(kindKey == 'WXDWD'){
		    var position = GetRndNum(0,4);
			var code1 = GetRndNum(jxmin,jxmax);
			if(position == 0){
				codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        code1;
			}else if(position == 1){
				codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE;				
			}else if(position == 2){
				codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE;				
			}else if(position == 3){
				codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE;						
			}else if(position == 4){
				codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +				
				        code1;
			}else{
				showTip("未知的号码位数");
			}
	   }else if(kindKey == 'RXE' || kindKey == 'RXEZXDS'){
		    var position1 = GetRndNum(0,3);
		    var position2 = GetRndNum(position1+1,4);
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			if(position1 < position2){
				var temp = position1;
				position1 = position2;
				position2 = temp;
			}
			
			codes = "";
            for(var k = 0; k < position2;k++){
         	   codes += lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            }				
            codes += code1;
            for(var j = position2; j < position1; j++){
            	if(j == position2){
            		codes += lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            	}else{
            		codes += lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            	}
            }
            codes += code2;
            for(var z = position1; z <= 4;z++){
            	if(z == position1){
            		codes += lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            	}else{
            		codes += lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            	}
            }
            
			if(codes.substring(codes.length - 1, codes.length) == lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT){
				codes = codes.substring(0, codes.length -1);
			}
	   }else if(kindKey == 'RXS' || kindKey == 'RXSZXDS'){
		    var position1 = GetRndNum(0,1);
		    var position2 = GetRndNum(3,4);
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			var code3 = GetRndNum(jxmin,jxmax);
			
			if(position1 < position2){
				var temp = position1;
				position1 = position2;
				position2 = temp;
			}
			
			codes = "";
            for(var k = 0; k < position2;k++){
         	   codes += lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            }				
            codes += code1;
            for(var v = position2; v < 2; v++){
            	if(v == position2){
            		codes += lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            	}else{
            		codes += lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            	}
            }
            codes += code2;
            for(var j = 2; j < position1; j++){
            	if(j == 2){
            		codes += lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            	}else{
            		codes += lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            	}
            }
            codes += code3;
            for(var z = position1; z <= 4;z++){
            	if(z == position1){
            		codes += lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            	}else{
            		codes += lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            	}
            }
			if(codes.substring(codes.length - 1, codes.length) == lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT){
				codes = codes.substring(0, codes.length -1);
			}
	   }else if(kindKey == 'RXSI' || kindKey == 'RXSIZXDS'){
		    var position = GetRndNum(0,4);
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			var code3 = GetRndNum(jxmin,jxmax);
			var code4 = GetRndNum(jxmin,jxmax);
			if(position == 0){
				codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        code4;
			}else if(position == 1){
				codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
					    lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
					    code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
					    code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
					    code4;				
			}else if(position == 2){
				codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        code4;				
			}else if(position == 3){
				codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        code4;						
			}else if(position == 4){
				codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        code4 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +				
                        lotteryCommonPage.lotteryParam.CODE_REPLACE;
			}else{
				showTip("未知的号码位数");
			}
	   }
	   
	   //任选号码随机的特殊处理
	   if(kindKey == 'RXSIZXDS' || kindKey == 'RXSZXDS' || kindKey == 'RXEZXDS'){
		    codes = codes.replace(/\-,/g,"").replace(/\,-/g,"");
	   }
	   
		//先校验投注蓝中,是否有该投注号码
		var isYetHave = false;
		var yetIndex = null;
		var codeStastics = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic;
		for(var j = codeStastics.length - 1; j >= 0 ; j--){
			 var codeStastic = codeStastics[j];
		     var kindKeyTemp = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
			 if(kindKeyTemp == jsksPage.lotteryParam.currentKindKey && codeStastic[5].toString() == codes){
				 isYetHave = true;
				 yetIndex = j;
				 break;
			 }
		}

		var currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get(jsksPage.param.kindNameType +"_"+jsksPage.lotteryParam.currentKindKey);
		if(isYetHave && yetIndex != null){
			showTip("您选择的号码在号码篮已存在，将直接进行倍数累加!");
			var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[yetIndex];
			jsksPage.lotteryParam.currentLotteryPrice = lotteryCommonPage.getAwardByLotteryModel(1 * lotteryCommonPage.currentLotteryKindInstance.param.unitPrice * beishu);
			codeStastic[0] = (parseFloat(codeStastic[0]) + parseFloat(jsksPage.lotteryParam.currentLotteryPrice)).toFixed(lotteryCommonPage.param.toFixedValue);
			codeStastic[1] = codeStastic[1];
			codeStastic[2] = parseInt(codeStastic[2]) + lotteryCommonPage.lotteryParam.beishu;
			codeStastic[3] = (parseFloat(codeStastic[3]) + parseFloat(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu))).toFixed(lotteryCommonPage.param.toFixedValue);
			codeStastic[4] = lotteryCommonPage.lottertyDataDeal.lotteryKindMap.get(jsksPage.lotteryParam.currentKindKey);
			codeStastic[5] = codes;
			codeStastic[6] = jsksPage.param.kindNameType +"_"+jsksPage.lotteryParam.currentKindKey;
			codeStastic[7] = codeStastic[7];
			
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStastic[7]); //先清除原来的投注号码的倍数
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put(codeStastic[7],codeStastic[2]);
		}else{
		    jsksPage.lotteryParam.currentLotteryTotalCount++;
			//将所选号码和玩法的映射放置map中
			var lotteryKindMap =  new JS_OBJECT_MAP();
			//将所选号码和玩法的映射放置map中
		    if(kindKey == 'RXSIZXDS' || kindKey == 'RXSZXDS' || kindKey == 'RXEZXDS'){
				var positionCode = "";
				$("input[name='ds_position']").each(function(){
					if(this.checked){
						positionCode += "1,";
					}else{
						positionCode += "0,";
					}
				});
				positionCode = positionCode.substring(0, positionCode.length - 1);
				lotteryKindMap.put(jsksPage.lotteryParam.currentKindKey,codes + "_" + positionCode);
		    }else{
				lotteryKindMap.put(jsksPage.lotteryParam.currentKindKey,codes);
		    }
			lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_"+jsksPage.lotteryParam.currentLotteryTotalCount,lotteryKindMap);
			//投注倍数存储
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_"+jsksPage.lotteryParam.currentLotteryTotalCount,beishu);

			//进入投注池当中,五星默认是一注
			var codeRecord = new Array();
		    var lotteryCount = 0;
		    kindKey = jsksPage.lotteryParam.currentKindKey;
			if(kindKey == "QSZXHZ" || kindKey == "HSZXHZ"){
				lotteryCount = lotteryCommonPage.lottertyDataDeal.getLotteryCountByShanZhiHeZhi(codes);
			}else if(kindKey == "QSZXHZ_G" || kindKey == "HSZXHZ_G"){
				lotteryCount = lotteryCommonPage.lottertyDataDeal.getLotteryCountByShanZuHeZhi(codes);
			}else if(kindKey == "QEZXHZ" || kindKey == "HEZXHZ"){
				lotteryCount = lotteryCommonPage.lottertyDataDeal.getLotteryCountByErZhiHeZhi(codes);
			}else if(kindKey == "QEZXHZ_G" || kindKey == "HEZXHZ_G"){
				lotteryCount = lotteryCommonPage.lottertyDataDeal.getLotteryCountByErZuHeZhi(codes);
			}else{
				lotteryCount = 1;
			}
			codeRecord.push(lotteryCommonPage.getAwardByLotteryModel(lotteryCount * lotteryCommonPage.currentLotteryKindInstance.param.unitPrice * beishu));
			codeRecord.push(lotteryCount);
			codeRecord.push(beishu);
			codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * beishu));
			codeRecord.push(lotteryCommonPage.lottertyDataDeal.lotteryKindMap.get(jsksPage.lotteryParam.currentKindKey));
			codeRecord.push(codes);
			codeRecord.push(jsksPage.param.kindNameType +"_"+jsksPage.lotteryParam.currentKindKey); //标识玩法
			codeRecord.push("lottery_id_"+jsksPage.lotteryParam.currentLotteryTotalCount); //标识该条记录的lotteryMap
			jsksPage.lotteryParam.codesStastic.push(codeRecord); 
			codes = "";
		}
	};
	
	lotteryCommonPage.codeStastics();  //显示随机投注结果
};

/**
 * 单式投注数目控制
 * @param eventType
 */
JsksPage.prototype.lotteryCountStasticForDs = function(contentLength){
	//任选四直选单式  //存储注数目
	if(jsksPage.lotteryParam.currentKindKey == 'RXSIZXDS'
		||  jsksPage.lotteryParam.currentKindKey == 'RXEZXDS'
		||  jsksPage.lotteryParam.currentKindKey == 'RXSZXDS'){  //需要乘以对应位置的方案数目
		jsksPage.lotteryParam.currentLotteryCount = contentLength * lotteryCommonPage.lottertyDataDeal.rxParam.dsAllowCount;	
	}else{
		jsksPage.lotteryParam.currentLotteryCount = contentLength;				
	}
};

/**
 * 时时彩彩种格式化
 * @param isCut 格式化之后对多余的数字是否截取
 */
JsksPage.prototype.formatNum = function(isCut){
	var kindKey = jsksPage.lotteryParam.currentKindKey;
	var pastecontent = $("#lr_editor").val();
	
	pastecontent=pastecontent.replace(/[^\d]/g,'');
	if (pastecontent == ""){
		$("#lr_editor").val("");
		//设置当前投注数目或者投注价格
		/*$('#J-balls-statistics-lotteryNum').text("0");  //显示投注
		$('#J-balls-statistics-amount').text(parseFloat("0").toFixed(lotteryCommonPage.param.toFixedValue));
		$('#J-balls-statistics-code').text("(投注内容)");*/
		return false;
	}
	var len= pastecontent.length;
	var num="";
	var numtxt="";
	var n=0;
	var maxnum=1;
	if(kindKey == 'WXDS'){
		maxnum=5;
	}else if(kindKey == 'SXDS' || kindKey == 'RXSIZXDS'){
		maxnum=4;
	}
	else if(kindKey == 'QSZXDS' || kindKey == 'HSZXDS' || kindKey == 'RXSZXDS'){  
		maxnum=3;
	}
	else if(kindKey == 'QEZXDS' || kindKey == "QEZXDS_G" || kindKey == "HEZXDS" || kindKey == "HEZXDS_G" || kindKey == "RXEZXDS"){  
		maxnum=2;
	}else{
		showTip("未配置该单式的控制.");
	}
	//下方显示的号码
	var numsStr = "";
	for(var i=0; i<len; i++){
		if(i%maxnum==0){
			n=1;
		}
		else{
			n = n + 1;
		}
		var currentChar = pastecontent.substr(i,1);
		//截取处理的
		if(isCut) {
			if(n<maxnum){
				num = num + currentChar+",";
				numsStr = numsStr + currentChar+",";
			}
			else{
				num = num + currentChar;
				numtxt = numtxt + num + "\n";
				num = "";
				numsStr = numsStr + currentChar + " ";
			}
		} else {
			//不需要截取多余处理的
			if(n<maxnum){
				numtxt = numtxt + currentChar+",";
				numsStr = numsStr + currentChar+",";
			}
			else{
				numtxt = numtxt + currentChar + "\n";
				numsStr = numsStr + currentChar + " ";
			}
		}
	}
	numsStr = numsStr.substring(0, numsStr.length - 1);
	//如果格式化截取多余的  内容为空，显示号码也为空
	if(isCut) {
		if(numtxt == "" || numtxt.length == 0) {
			numsStr = "";
		}
	} else {
		numtxt = numtxt.substring(0, numtxt.length - 1);
	}
	//处理投注下方显示的号码
	jsksPage.lotteryParam.codesStr = numsStr;
	$("#lr_editor").val(numtxt);
	return true;
};

/**
 * 单式的选号投注
 */
JsksPage.prototype.singleLotteryNum = function(){
	var kindKey = jsksPage.lotteryParam.currentKindKey;
	
	//如果是五星或者四星或者任选玩法的单式录入
	if(kindKey == 'WXDS' || kindKey == 'SXDS' 
		|| kindKey == 'RXSIZXDS' || kindKey == 'RXEZXDS' || kindKey == 'RXSZXDS'){
		var editorVal = $("#lr_editor").val();
		if(editorVal.indexOf("说明") != -1){
			showTip("请先填写投注号码");
			return false;
		}
		lotteryCommonPage.dealForDsFormat();  //首先格式化,避免直接点击选号码的事件
		var pastecontent = $("#lr_editor").val();
		if(pastecontent == ""){
			showTip("请先填写投注号码");
			return false;
		}
		pastecontent = pastecontent.replace(/\r\n/g,"$");
		pastecontent = pastecontent.replace(/\n/g,"$");
		pastecontent = pastecontent.replace(/\s/g,"");
		pastecontent = pastecontent.replace(/(\$)\1+/g, '$1');
		if(pastecontent.substr(pastecontent.length-1,1)=="$"){
			pastecontent = pastecontent.substr(0,pastecontent.length-1);
		}
		
		pastecontent = pastecontent.replace(/\$/g,"&");
		var pastecontentArr = pastecontent;
		var pastecontentLength = pastecontent.split("&").length;
		/*if(kindKey == 'WXDS'){
			if(jsksPage.lotteryParam.currentLotteryCount > 80000){ //100000
				showTip("当前玩法最多支持80000注单式内容，请调整！");
		        return;
			}
		}else if(kindKey == 'SXDS'){
			if(jsksPage.lotteryParam.currentLotteryCount > 8000){ //10000
				showTip("当前玩法最多支持8000注单式内容，请调整！");
		        return;
			}
		}else if(kindKey == 'RXSIZXDS' || kindKey == 'RXEZXDS' || kindKey == 'RXSZXDS'){
			if(kindKey == 'RXSIZXDS'){
				if(jsksPage.lotteryParam.currentLotteryCount > 40000){ //50000
					showTip("当前玩法最多支持40000注单式内容，请调整！");
			        return;
				}else if(jsksPage.lotteryParam.currentLotteryCount == 0){
					showTip("当前没有投注数目，请查看.");
			        return;
				}
			}else if(kindKey == 'RXEZXDS'){
				if(jsksPage.lotteryParam.currentLotteryCount > 2000){
					showTip("当前玩法最多支持2000注单式内容，请调整！");
			        return;
				}else if(jsksPage.lotteryParam.currentLotteryCount == 0){
					showTip("当前没有投注数目，请查看.");
			        return;
				}
			}else if(kindKey == 'RXSZXDS'){
				if(jsksPage.lotteryParam.currentLotteryCount > 8000){ //10000
					showTip("当前玩法最多支持8000注单式内容，请调整！");
			        return;
				}else if(jsksPage.lotteryParam.currentLotteryCount == 0){
					showTip("当前没有投注数目，请查看.");
			        return;
				}
			}else{
				showTip("未配置");
			}
		}else{
			showTip("未配置");
		}*/
		
		
		var errzhushu = "";
		var errzhushumsg = "[格式错误]";
		var reg1,reg2;
		if(kindKey == 'WXDS'){  //五星单式 1,2,3,4,5   
			reg1 = /^((?:(?:[0-9]),){4}(?:[0-9])[&]){0,}(?:(?:[0-9]),){4}(?:[0-9])$/;
		}else if(kindKey == 'SXDS'){ //四星直选单式
			reg1 = /^((?:(?:[0-9]),){3}(?:[0-9])[&]){0,}(?:(?:[0-9]),){3}(?:[0-9])$/;
		}else if(kindKey == 'RXSIZXDS'){ //任选四直选单式
			reg1 = /^((?:(?:[0-9]),){3}(?:[0-9])[&]){0,}(?:(?:[0-9]),){3}(?:[0-9])$/;
		}else if(kindKey == 'RXSZXDS'){ //任选三直选单式
			reg1 = /^((?:(?:[0-9]),){2}(?:[0-9])[&]){0,}(?:(?:[0-9]),){2}(?:[0-9])$/;
		}else if(kindKey == 'RXEZXDS'){ //任选二直选单式
			reg1 = /^((?:(?:[0-9]),){1}(?:[0-9])[&]){0,}(?:(?:[0-9]),){1}(?:[0-9])$/;
		}else{
			showTip("未配置该类型的单式");
		}
		
		//号码校验
		if(!reg1.test(pastecontentArr)){
			showTip("单式号码格式不合法.");
			return false;
		}
		
		//位数选取判断
		var positionCode = "";
		$("input[name='ds_position']").each(function(){
			if(this.checked){
				positionCode += "1,";
			}else{
				positionCode += "0,";
			}
		});
		positionCode = positionCode.substring(0, positionCode.length - 1);
		jsksPage.lotteryParam.codes = pastecontentArr;
		
		var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
		var currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get(jsksPage.param.kindNameType +"_"+jsksPage.lotteryParam.currentKindKey);
		lotteryCommonPage.lotteryParam.beishu = parseInt(lotteryCommonPage.lotteryParam.beishu);
		jsksPage.lotteryParam.currentLotteryTotalCount++;
		
		//将所选号码和玩法的映射放置map中
		var lotteryKindMap =  new JS_OBJECT_MAP();
		if(kindKey == 'RXSIZXDS' || kindKey == 'RXEZXDS' || kindKey == 'RXSZXDS'){
			lotteryKindMap.put(jsksPage.lotteryParam.currentKindKey,jsksPage.lotteryParam.codes + "_" + positionCode);
		}else{
			lotteryKindMap.put(jsksPage.lotteryParam.currentKindKey,jsksPage.lotteryParam.codes);
		}
		lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_"+jsksPage.lotteryParam.currentLotteryTotalCount,lotteryKindMap);
		//投注倍数存储
		lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_"+jsksPage.lotteryParam.currentLotteryTotalCount,lotteryCommonPage.lotteryParam.beishu);
		
		//进入投注池当中
		var codeRecord = new Array();
		codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((jsksPage.lotteryParam.currentLotteryCount * jsksPage.param.unitPrice * lotteryCommonPage.lotteryParam.beishu).toFixed(lotteryCommonPage.param.toFixedValue)));
		codeRecord.push(jsksPage.lotteryParam.currentLotteryCount);
		codeRecord.push(lotteryCommonPage.lotteryParam.beishu);
		codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu));
		codeRecord.push(lotteryCommonPage.lottertyDataDeal.lotteryKindMap.get(jsksPage.lotteryParam.currentKindKey));
		codeRecord.push(jsksPage.lotteryParam.codes);
		codeRecord.push(jsksPage.param.kindNameType +"_"+jsksPage.lotteryParam.currentKindKey); //标识玩法
		codeRecord.push("lottery_id_"+jsksPage.lotteryParam.currentLotteryTotalCount); 
		jsksPage.lotteryParam.codesStastic.push(codeRecord);	
		
		//选好号码的结束事件
		lotteryCommonPage.addCodeEnd(); 
		$("#lr_editor").val("");//清空注码
	//前三后三前二后二单式玩法的录入
	}else{
		var editorVal = $("#lr_editor").val();
		if(editorVal.indexOf("说明") != -1){
			showTip("请先填写投注号码");
			return false;
		}
		
		lotteryCommonPage.dealForDsFormat();  //首先格式化,避免直接点击选号码的事件
		var pastecontent = $("#lr_editor").val();
		if(pastecontent == ""){
			showTip("请先填写投注号码");
			return false;
		}
		pastecontent = pastecontent.replace(/\r\n/g,"$");
		pastecontent = pastecontent.replace(/\n/g,"$");
		pastecontent = pastecontent.replace(/\s/g,"");
		pastecontent = pastecontent.replace(/(\$)\1+/g, '$1');
		if(pastecontent.substr(pastecontent.length-1,1)=="$"){
			pastecontent = pastecontent.substr(0,pastecontent.length-1);
		}
		var pastecontentArr = pastecontent.split("$");
		/*if(kindKey == 'QSZXDS' || kindKey == 'HSZXDS'){ //三星直选单式 和 三星组选单式
			if(jsksPage.lotteryParam.currentLotteryCount > 800){  //800
				showTip("当前玩法最多支持800注单式内容，请调整！");
		        return;
			}
		}else if(kindKey == 'QEZXDS' || kindKey == "QEZXDS_G" || kindKey == "HEZXDS" || kindKey == "HEZXDS_G"){ //二星直选单式 和 二星组选单式
			if(jsksPage.lotteryParam.currentLotteryCount > 80){  //80
				showTip("当前玩法最多支持80注单式内容，请调整！");
		        return;
			}
		}
		if(pastecontentArr.length > 2000){
			showTip("当前玩法最多支持2000注单式内容，请调整！");
	        return;
		}*/

		var errzhushu = "";
		var errzhushumsg = "[格式错误]";
		var reg1,reg2;
		if(kindKey == 'WXDS'){  //五星单式 1,2,3,4,5
			reg1 = /^(?:(?:[0-9]),){4}(?:[0-9])\s*$/;
			reg2 = /^\d{5}\s*$/;
		}else if(kindKey == 'SXDS'){ //四星直选单式
			reg1 = /^(?:(?:[0-9]),){3}(?:[0-9])\s*$/;
			reg2 = /^\d{5}\s*$/;
		}else if(kindKey == 'QSZXDS' || kindKey == 'HSZXDS'){ //三星直选单式 和 三星组选单式
			reg1 = /^(?:(?:[0-9]),){2}(?:[0-9])\s*$/;
			reg2 = /^\d{3}\s*$/;
		}else if(kindKey == 'QEZXDS' || kindKey == "QEZXDS_G" || kindKey == "HEZXDS" || kindKey == "HEZXDS_G"){ //二星直选单式 和 二星组选单式
			reg1 = /^(?:(?:[0-9]),)(?:[0-9])\s*$/;
			reg2 = /^\d{2}\s*$/;
		}else if(kindKey == 'RXSIZXDS'){
			if(!lotteryCommonPage.lottertyDataDeal.rxParam.isRxDsAllow){
				showTip("您还未选择任选四单式的位置,无法添加号码");
				return;
			}
			reg1 = /^(?:(?:[0-9]),){3}(?:[0-9])\s*$/;
			reg2 = /^\d{5}\s*$/;
		}else if(kindKey == 'RXEZXDS'){
			if(!lotteryCommonPage.lottertyDataDeal.rxParam.isRxDsAllow){
				showTip("您还未选择任选二单式的位置,无法添加号码");
				return;
			}
			reg1 = /^(?:(?:[0-9]),)(?:[0-9])\s*$/;
			reg2 = /^\d{2}\s*$/;
		}else if(kindKey == 'RXSZXDS'){
			if(!lotteryCommonPage.lottertyDataDeal.rxParam.isRxDsAllow){
				showTip("您还未选择任选三单式的位置,无法添加号码");
				return;
			}
			reg1 = /^(?:(?:[0-9]),){2}(?:[0-9])\s*$/;
			reg2 = /^\d{3}\s*$/;
		}else{
			showTip("未配置该类型的单式");
		}
		
		var isAppendCode = false;
		var appendCodeArray = new Array();
		var dsCodeCodes = "";  //单式号码拼接

		for(var i=0;i<pastecontentArr.length;i++){
			var value1 = pastecontentArr[i];
			if(!reg1.test(value1)&&!reg2.test(value1)){
				if(errzhushu==""){
					errzhushu = "第"+(i+1).toString()+"注";
				}else{
					errzhushu = errzhushu+" <br/>"+"第"+(i+1).toString()+"注";
				}
				continue;
			}

			if(kindKey == 'QEZXDS_G' || kindKey == 'HEZXDS_G'){ //2星组组选单式
				var lsstr=value1;
				sstr=lsstr.replace(/,/g,"");
				a = sstr.substr(0,1);
				b = sstr.substr(1,1);
				if (a==b){
					if(errzhushu==""){
						errzhushu = "第"+(i+1).toString()+"注";
					}else{
						errzhushu = errzhushu+"、"+"第"+(i+1).toString()+"注";
					}
				}
			}
			
	        //单式号码拼接
	        dsCodeCodes += pastecontentArr[i] + "  ";
	        
		}
		
		//显示错误注号码
		if(errzhushu!=""){
			showTip(errzhushu+"  "+"号码有误，请核对！");
			return false;
		}
		
		var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
		var currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get(jsksPage.param.kindNameType +"_"+jsksPage.lotteryParam.currentKindKey);
		lotteryCommonPage.lotteryParam.beishu = parseInt(lotteryCommonPage.lotteryParam.beishu);
		jsksPage.lotteryParam.currentLotteryCount = 1;

		//单式记录标识
		var dsCodeRecordArray = new Array();
		var dsLotteryCount = 0;
		for(var i=0;i<pastecontentArr.length;i++){
			var value = pastecontentArr[i];	
			if(value == ""){
				continue;
			}
			var text ="";
			if(kindKey == 'WXDS' ){
				value = value;
			}else if(kindKey == 'SXDS' ){//四星星直选单式
				value = "-,"+value;
			}else if(kindKey == 'QSZXDS' ){//前三星直选单式
				value = value + ",-,-";
			}else if(kindKey == 'HSZXDS' ){//后三星直选单式
				value = "-,-," + value;
			}else if(kindKey == 'QEZXDS'){//前二直选单式
				value = value + ",-,-,-";
			}else if(kindKey == 'HEZXDS'){//后二直选单式
				value = "-,-,-," + value;
			}else if(kindKey == 'QEZXDS_G' || kindKey == 'HEZXDS_G' || kindKey == 'RXSIZXDS' || kindKey == 'RXEZXDS' || kindKey == 'RXSZXDS'){ //任选的玩法,已经拼接好投注号码
				value = value;
			}else{
				showTip("该单式号码未配置");
				return false;
			}
			
			jsksPage.lotteryParam.currentLotteryTotalCount++;
			lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryCount = 1;
			jsksPage.lotteryParam.codes = value;
				
			//将所选号码和玩法的映射放置map中
			var lotteryKindMap =  new JS_OBJECT_MAP();
			lotteryKindMap.put(jsksPage.lotteryParam.currentKindKey,jsksPage.lotteryParam.codes);
			lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_"+jsksPage.lotteryParam.currentLotteryTotalCount,lotteryKindMap);
			//投注倍数存储
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_"+jsksPage.lotteryParam.currentLotteryTotalCount,lotteryCommonPage.lotteryParam.beishu);
			
			jsksPage.lotteryParam.codes = null;

			dsCodeRecordArray.push("lottery_id_"+jsksPage.lotteryParam.currentLotteryTotalCount);
			dsLotteryCount++;  //统计数目
		}
		
		//进入投注池当中
		var codeRecord = new Array();
		codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((dsLotteryCount * jsksPage.param.unitPrice * lotteryCommonPage.lotteryParam.beishu).toFixed(lotteryCommonPage.param.toFixedValue)));
		codeRecord.push(dsLotteryCount);
		codeRecord.push(lotteryCommonPage.lotteryParam.beishu);
		codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu));
		codeRecord.push(lotteryCommonPage.lottertyDataDeal.lotteryKindMap.get(jsksPage.lotteryParam.currentKindKey));
		codeRecord.push(dsCodeCodes);
		codeRecord.push(jsksPage.param.kindNameType +"_"+jsksPage.lotteryParam.currentKindKey); //标识玩法
		codeRecord.push(dsCodeRecordArray); 
		jsksPage.lotteryParam.codesStastic.push(codeRecord);	
		
		//选好号码的结束事件
		lotteryCommonPage.addCodeEnd(); 
		$("#lr_editor").val("");//清空注码
	}
	return true;
};


/**
 * 初始化彩种信息
 */
JsksPage.prototype.initLottery = function(){
	//先加载所有的玩法
    lotteryCommonPage.lottertyDataDeal.loadLotteryKindPlay();
    
    //取消追号处理
    lotteryCommonPage.lotteryParam.isZhuiCode = false;
    
    //设置玩法描述数据
    lotteryCommonPage.lottertyDataDeal.setKindPlayDes();
    
    //选择默认的玩法，默认是和值大小单双
    jsksPage.lotteryParam.currentKindKey = "HZDXDS";
	lotteryCommonPage.lottertyDataDeal.lotteryKindPlayChoose(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey);
	
	//动画效果
	animate_add(".main-head","fadeInDown",0);
	animate_add(".lottery-content","fadeInUp",0);
	animate_add(".lottery-bar .bar","fadeInUp",0);
	
    //添加星种选中事件
	$("#lotteryStarKindList .child").unbind("click").click(function(){
		var roleId = $(this).attr("data-id");
		//如果有包含玩法属性
		if(isNotEmpty(roleId) && lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey != roleId && roleId != "SBTH" && roleId != "EBTH") {
			//清空当前选中号码
			lotteryCommonPage.clearCurrentCodes();
			lotteryCommonPage.lottertyDataDeal.lotteryKindPlayChoose(roleId);
			 $("#lotteryStarKindList .child").removeClass("color-red");
			 $(this).addClass("color-red");
			//隐藏下拉玩法列表
			element_toggle(false,['.lottery-classify','.alert']);
		//加载星种玩法下拉
		} else {
			var kindPlayClass = $(this).attr("data-id");
			var parent=$(this).closest("body");
			parent.find(".lottery-classify2 .move-content .child").removeClass('color-red');
			$(this).addClass('color-red');
			parent.find(".lottery-classify2 .clist").removeClass('in').addClass('out');
			parent.find("."+kindPlayClass).addClass('in').removeClass('out');
		}
	});
    //添加玩法选中事件
	$("#lotteryKindPlayList .line-content .child").unbind("click").click(function(){
		var roleId = $(this).attr("data-role-id");
		//已经是当前玩法了，不需要处理
		if(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey == roleId) {
			return;
		}
		
	   //清空当前选中号码
	   lotteryCommonPage.clearCurrentCodes();
	   
	   //控制玩法的号码展示
	   lotteryCommonPage.lottertyDataDeal.lotteryKindPlayChoose(roleId);
	   
	   //隐藏下拉玩法列表
	   element_toggle(false,['.lottery-classify']);
	});
	
	//点击当前玩法显示下拉其他玩法
	$("#currentKindDiv").unbind("click").click(function(){
		var e = $(this);
		var ele = ".lottery-classify2";
		var parent=$(this).closest("body");
		var display=$(ele).css("display");
		var style="boolean";
		style=display=="none"?"true":"boolean";
		parent.find(".lottery-classify").not(ele).hide();
		element_toggle(style,[ele],parent);
		
		//触发星种下拉
		$("#lotteryStarKindList .child").each(function(){
			if($(this).hasClass('color-red')) {
				var kindPlayClass = $(this).attr("data-id");
				var parent=$(this).closest("body");
				parent.find(".lottery-classify2 .clist").removeClass('in').addClass('out');
				parent.find("."+kindPlayClass).addClass('in').removeClass('out');
				return false;
			}
		});
	});
	
	//绑定选择其他地区彩种事件
	$("#otherLotteryKind").unbind("click").click(function(){
		var ele = '.lottery-classify1';
		var parent=$(this).closest("body");
		var display=$(ele).css("display");
		var style="boolean";
		style=display=="none"?"true":"boolean";
		parent.find(".lottery-classify").not(ele).hide();
		element_toggle(style,[ele],parent);
	});
	
	//点击开奖号码显示开奖记录事件
	$(".lottery_run").unbind("click").click(function(){
		var parent=$("body");
		if(parent.find(".lottery-dynamic").hasClass('in')){
			parent.find(".lottery-dynamic").removeClass('in').addClass('out');
		}else{
			parent.find(".lottery-dynamic").removeClass('out').addClass('in');
		}
	});
	
	
	//倍数改变事件
	lotteryCommonPage.beishuEvent();
	
	//倍数减少事件
	$("#beishuSub").unbind("click").click(function(){
		var beishuStr = $('#beishu').val();
		var beishuTemp = parseInt(beishuStr);
		beishuTemp--;
		if(beishuTemp <= 0) {
			showTip("当前倍数已经是最低了");
			return;
		}
		$("#beishu").val(beishuTemp);
		lotteryCommonPage.beishuEventContent();
	});
	
	//倍数增加事件
	$("#beishuAdd").unbind("click").click(function(){
		var beishuStr = $('#beishu').val();
		var beishuTemp = parseInt(beishuStr);
		beishuTemp++;
		if(beishuTemp > 1000000) {
			showTip("倍数最高1000000");
			return;
		}
		$("#beishu").val(beishuTemp);
		lotteryCommonPage.beishuEventContent();
	});
	

	//确认投注-展示投注信息事件
	$("#showLotteryMsgButton").unbind("click").click(function(){
		//非追号类型 处理添加号码到投注池
		if(!lotteryCommonPage.lotteryParam.isZhuiCode) {
			lotteryCommonPage.currentLotteryKindInstance.userLotteryNumForAddOrder();
		}
		lotteryCommonPage.showLotteryMsg();
	});
	//关闭投注信息事件
    $("#lotteryOrderDialogCancelButton").unbind("click").click(function(){
    	lotteryCommonPage.hideLotteryMsg();
	});
    
    //关闭投注信息事件
    $(".close").unbind("click").click(function(){
    	lotteryCommonPage.hideLotteryMsg();
    });
    
	//确认投注-提交后台投注事件
	$("#lotteryOrderDialogSureButton").unbind("click").click(function(){
		lotteryCommonPage.userLottery();
	});
	
	//机选一注事件
	$("#randomOneBtn").unbind("click").click(function(){
		lotteryCommonPage.randomOneEvent();
	});
	
	//清空投注池号码
	$("#clearAllCodeBtn").unbind("click").click(function(){
		lotteryCommonPage.clearCodeStasticEvent();
		/*comfirm("提示","确认删除号码篮内全部内容吗?","取消",function(){},"确认",
				lotteryCommonPage.clearCodeStasticEvent);*/
		
	});	
	
	//点击追号取消追号事件
	$("#zhuiCodeButton").unbind("click").click(function(param,showParam){
		lotteryCommonPage.changeTozhuiCodeEvent();
	});	
	
	 
	//获取最新的开奖号码
	lotteryCommonPage.getLastLotteryCode();
	//获取当前彩种的最新期号和投注倒计时
	lotteryCommonPage.getActiveExpect();
	//加载今天和明天的期号
    lotteryCommonPage.getAllExpectsByTodayAndTomorrow();
	//近10期的开奖记录
	lotteryCommonPage.getNearestTenLotteryCode();
	//获取用户今日投注记录
	lotteryCommonPage.getTodayLotteryByUser();
	
	//每隔10秒钟请求一次获取最新的开奖号码
	if(lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JLFFC" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "HGFFC"){
		window.setInterval("lotteryCommonPage.getLastLotteryCode()",5000);  //分分彩读取最新的开奖号码
		//window.setInterval("lotteryCommonPage.getNearestTenLotteryCode()",5000);
		window.setInterval("lotteryCommonPage.getActiveExpect()",30000); //30秒重新读取服务器的时间
	}else{
		window.setInterval("lotteryCommonPage.getLastLotteryCode()",10000);  //其他彩种读取最新的开奖号码
		//window.setInterval("lotteryCommonPage.getNearestTenLotteryCode()",10000);
		window.setInterval("lotteryCommonPage.getActiveExpect()",60000); //1分钟重新读取服务器的时间
	}
    
};
