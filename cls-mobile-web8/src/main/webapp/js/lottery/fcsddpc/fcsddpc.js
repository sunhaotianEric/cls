function FcsddpcPage() {
}
var fcsddpcPage = new FcsddpcPage();

// 基本参数
fcsddpcPage.param = {
	unitPrice : 2, // 每注价格
	kindDes : "福彩3D",
	kindName : 'FCSDDPC', // 彩种
	kindNameType : 'DPC', // 大彩种拼写
	currentLotteryWins : new JS_OBJECT_MAP(), // 奖金映射
	currentLotteryWinsLoaded : false, // 奖金映射是否加载完毕
	ommitData : null, // 遗漏数据
	hotColdData : null
// 冷热数据
};

// 投注参数
fcsddpcPage.lotteryParam = {
	lotteryKindMap : new JS_OBJECT_MAP(),
	currentKindKey : 'QEZXFS', // 当前所选择的玩法模式,默认是五星复式
	currentLotteryCount : 0, // 当前投注数目
	currentLotteryPrice : 0, // 当前投注价格,按元来计算
	currentLotteryTotalCount : 0, // 当前总投注数目
	codes : null, // 投注号码
	codesStastic : new Array(),
	codesStr : "" // 投注下方显示的号码
};

/**
 * 获取开奖号码前台展现的字符串
 */
FcsddpcPage.prototype.getOpenCodeStr = function(openCodeNum) {
	var openCodeStr = "";
	if (isNotEmpty(openCodeNum)) {
		var expectDay = openCodeNum.substring(0, openCodeNum.length - 3);
		var expectNum = openCodeNum.substring(openCodeNum.length - 3, openCodeNum.length);
		openCodeStr = expectDay + expectNum;
	}
	return openCodeStr;
};

/**
 * 展示当前正在投注的期号
 */
FcsddpcPage.prototype.showCurrentLotteryCode = function(lotteryIssue) {
	if (lotteryIssue != null) {
		var openCodeStr = fcsddpcPage.getOpenCodeStr(lotteryIssue.lotteryNum);
		$('#nowNumber').text(openCodeStr);
	}
};

/**
 * 展示最新的开奖号码
 */
FcsddpcPage.prototype.showLastLotteryCode = function(lotteryCode) {
	if (lotteryCode != null) {
		var lotteryNum = lotteryCode.lotteryNum;
		var openCodeStr = fcsddpcPage.getOpenCodeStr(lotteryCode.lotteryNum);

		$('#J-lottery-info-lastnumber').text(openCodeStr);

		var openCodeStr = "";
		openCodeStr += "<span class='ball color-red' id='lotteryNumber1'>" + lotteryCode.numInfo1 + "</span>";
		openCodeStr += "<span class='ball color-red' id='lotteryNumber2'>" + lotteryCode.numInfo2 + "</span>";
		openCodeStr += "<span class='ball color-red' id='lotteryNumber3'>" + lotteryCode.numInfo3 + "</span>";
		$(".lottery_run .child-muns").html(openCodeStr);

	}
};

/**
 * 查找奖金对照表
 */
FcsddpcPage.prototype.getLotteryWins = function() {
	// 还未加载奖金的
	if (!fcsddpcPage.param.currentLotteryWinsLoaded) {
		var jsonData = {
				"lotteryKind" : fcsddpcPage.param.kindName,
			};
		$.ajax({
			type : "POST",
			// 设置请求为同步请求
			async : false,
			url : contextPath + "/lottery/getLotteryWins",
			data : JSON.stringify(jsonData),
			dataType : "json",
			contentType : 'application/json;charset=utf-8',
			success : function(result) {
				commonHandlerResult(result, this.url);
				var resultData = result.data;
				if (result.code == "ok") {
					for ( var key in resultData) {
						// 将奖金对照表缓存
						fcsddpcPage.param.currentLotteryWins.put(key, resultData[key]);
					}
					fcsddpcPage.param.currentLotteryWinsLoaded = true;
				} else if (result.code == "error") {
					showTip(resultData);
				}
			}
		});
	}
};

// 服务器时间倒计时
FcsddpcPage.prototype.calculateTime = function() {
	var SurplusSecond = lotteryCommonPage.param.diffTime;
	if (SurplusSecond >= (3600 * 24)) {
		// 显示预售中
		$("#timer").hide();
		$("#presellTip").show();
		window.clearInterval(lotteryCommonPage.param.intervalKey); // 终止周期性倒计时
	} else {
		var h = Math.floor(SurplusSecond / 3600);
		if (h < 10) {
			h = "0" + h;
		}
		// 计算剩余的分钟
		SurplusSecond = SurplusSecond - (h * 3600);
		var m = Math.floor(SurplusSecond / 60);
		if (m < 10) {
			m = "0" + m;
		}
		var s = SurplusSecond % 60;
		if (s < 10) {
			s = "0" + s;
		}

		h = h.toString();
		m = m.toString();
		s = s.toString();
		$("#timer").html(h + ":" + m + ":" + s);
	}
	lotteryCommonPage.param.diffTime--;
	if (lotteryCommonPage.param.diffTime < 0) {
		// 弹出窗口控制
		$("#lotteryOrderDialogCancelButton").trigger("click");
		frontCommonPage.closeKindlyReminder();
		var str = "";
		str += "第";
		str += lotteryCommonPage.param.currentExpect + "期已截止,<br/>"
		str += "投注时请注意期号!";
		showTip(str);

		window.clearInterval(lotteryCommonPage.param.intervalKey); // 终止周期性倒计时
		lotteryCommonPage.getActiveExpect(); // 倒计时结束重新请求最新期号
		lotteryCommonPage.getAllExpectsByTodayAndTomorrow(); // 重新加载今天和明天的期号
	}
	;
};

/**
 * 选好号码的事件，进行分开控制
 */
FcsddpcPage.prototype.userLotteryNumForAddOrder = function() {
	var kindKey = fcsddpcPage.lotteryParam.currentKindKey;
	if (kindKey == 'SXDS' || kindKey == 'QEZXDS' || kindKey == 'QEZXDS_G' || kindKey == 'HEZXDS' || kindKey == 'HEZXDS_G' || kindKey == 'RXEZXDS') {
		// 需要走单式的验证
		return fcsddpcPage.singleLotteryNum();
	} else {
		return fcsddpcPage.userLotteryNum();
	}
};

/**
 * 用户选择投注号码
 */
FcsddpcPage.prototype.userLotteryNum = function() {

	if (fcsddpcPage.lotteryParam.codes == null || fcsddpcPage.lotteryParam.codes.length == 0) {
		showTip("号码选择不完整，请重新选择！");
		return;
	}

	// 添加进投注统计号码池 金额\注数\倍数\最高奖金\玩法描述\号码
	var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
	var currentLotteryWin = fcsddpcPage.param.currentLotteryWins.get(fcsddpcPage.param.kindNameType + "_" + fcsddpcPage.lotteryParam.currentKindKey);

	// 如果处于修改投注号码的状态
	if (lotteryCommonPage.lotteryParam.currentIsUpdateCodeStastic && lotteryCommonPage.lotteryParam.updateCodeStasticIndex != null) {
		var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[lotteryCommonPage.lotteryParam.updateCodeStasticIndex];
		codeStastic[0] = fcsddpcPage.lotteryParam.currentLotteryPrice;
		codeStastic[1] = fcsddpcPage.lotteryParam.currentLotteryCount;
		codeStastic[2] = lotteryCommonPage.lotteryParam.beishu;
		codeStastic[3] = lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode)
				* lotteryCommonPage.lotteryParam.beishu);
		codeStastic[4] = fcsddpcPage.lotteryParam.lotteryKindMap.get(fcsddpcPage.lotteryParam.currentKindKey);
		codeStastic[5] = fcsddpcPage.lotteryParam.codes;
		codeStastic[6] = fcsddpcPage.param.kindNameType + "_" + fcsddpcPage.lotteryParam.currentKindKey;
		codeStastic[7] = codeStastic[7];

		// 倍数更新
		lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStastic[7]); // 先清除原来的投注号码的倍数
		lotteryCommonPage.lotteryParam.lotteryMultipleMap.put(codeStastic[7], lotteryCommonPage.lotteryParam.beishu);

		// 将所选号码和玩法的映射放置map中
		var lotteryKindMap = new JS_OBJECT_MAP();
		lotteryKindMap.put(fcsddpcPage.lotteryParam.currentKindKey, fcsddpcPage.lotteryParam.codes);
		lotteryCommonPage.lotteryParam.lotteryMap.remove(codeStastic[7]); // 先清除原来的
		lotteryCommonPage.lotteryParam.lotteryMap.put(codeStastic[7], lotteryKindMap);

		// 清空当前选中的号码
		lotteryCommonPage.clearCurrentCodes();
	} else {
		var isYetHave = false;
		var yetIndex = null;
		var codeStastics = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic;
		for (var i = codeStastics.length - 1; i >= 0; i--) {
			var codeStastic = codeStastics[i];
			var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_") + 1, codeStastic[6].length);
			if (kindKey == fcsddpcPage.lotteryParam.currentKindKey && codeStastic[5].toString() == fcsddpcPage.lotteryParam.codes) {
				isYetHave = true;
				yetIndex = i;
				break;
			}
		}

		if (isYetHave && yetIndex != null) {
			showTip("您选择的号码在号码篮已存在，将直接进行倍数累加!");
			var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[yetIndex];
			codeStastic[0] = (parseFloat(codeStastic[0]) + parseFloat(fcsddpcPage.lotteryParam.currentLotteryPrice)).toFixed(lotteryCommonPage.param.toFixedValue);
			codeStastic[1] = codeStastic[1];
			codeStastic[2] = parseInt(codeStastic[2]) + lotteryCommonPage.lotteryParam.beishu;
			codeStastic[3] = (parseFloat(codeStastic[3]) + parseFloat(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel)
					* currentLotteryWin.specCode)
					* lotteryCommonPage.lotteryParam.beishu))).toFixed(lotteryCommonPage.param.toFixedValue);
			codeStastic[4] = fcsddpcPage.lotteryParam.lotteryKindMap.get(fcsddpcPage.lotteryParam.currentKindKey);
			codeStastic[5] = fcsddpcPage.lotteryParam.codes;
			codeStastic[6] = fcsddpcPage.param.kindNameType + "_" + fcsddpcPage.lotteryParam.currentKindKey;
			codeStastic[7] = codeStastic[7];

			lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStastic[7]); // 先清除原来的投注号码的倍数
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put(codeStastic[7], codeStastic[2]);
		} else {
			fcsddpcPage.lotteryParam.currentLotteryTotalCount++; // 投注数目递增
			// 将所选号码和玩法的映射放置map中
			var lotteryKindMap = new JS_OBJECT_MAP();
			lotteryKindMap.put(fcsddpcPage.lotteryParam.currentKindKey, fcsddpcPage.lotteryParam.codes);
			lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_" + fcsddpcPage.lotteryParam.currentLotteryTotalCount, lotteryKindMap);
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_" + fcsddpcPage.lotteryParam.currentLotteryTotalCount, lotteryCommonPage.lotteryParam.beishu);

			// 进入投注池当中
			var codeRecord = new Array();
			codeRecord.push(fcsddpcPage.lotteryParam.currentLotteryPrice);
			codeRecord.push(fcsddpcPage.lotteryParam.currentLotteryCount);
			codeRecord.push(lotteryCommonPage.lotteryParam.beishu);
			codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode)
					* lotteryCommonPage.lotteryParam.beishu));
			codeRecord.push(fcsddpcPage.lotteryParam.lotteryKindMap.get(fcsddpcPage.lotteryParam.currentKindKey));
			codeRecord.push(fcsddpcPage.lotteryParam.codes);
			codeRecord.push(fcsddpcPage.param.kindNameType + "_" + fcsddpcPage.lotteryParam.currentKindKey); // 标识玩法
			codeRecord.push("lottery_id_" + fcsddpcPage.lotteryParam.currentLotteryTotalCount); // 标识该条记录的lotteryMap
			fcsddpcPage.lotteryParam.codesStastic.push(codeRecord);
		}
	}

	// 选好号码统计金额注数
	lotteryCommonPage.codeStastics();
	return true;
};

/**
 * 根据彩种玩法,获取所选的号码
 * 
 * @param kindKey
 */
FcsddpcPage.prototype.getCodesByKindKey = function() {
	var codes = "";
	var lotteryCount = 0;
	// 五星复式校验
	if (fcsddpcPage.lotteryParam.currentKindKey == 'SXFS' || fcsddpcPage.lotteryParam.currentKindKey == 'QEZXFS' || fcsddpcPage.lotteryParam.currentKindKey == 'HEZXFS'
			|| fcsddpcPage.lotteryParam.currentKindKey == 'QEDXDS' || fcsddpcPage.lotteryParam.currentKindKey == 'HEDXDS') {
		var numArrays = new Array();
		// 获取 选择的号码
		$(".child-content .content").each(function(i) {
			var lis = $(this).children();
			var numsStr = "";
			for (var i = 0; i < lis.length; i++) {
				var li = lis[i];
				var aNum = $(li);
				if ($(aNum).hasClass('on')) {
					numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT;
				}
			}
			// 去除最后一个分隔符
			if (numsStr.substring(numsStr.length - 1, numsStr.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT) {
				numsStr = numsStr.substring(0, numsStr.length - 1);
			}

			// 复式不能进行空串处理
			if (fcsddpcPage.lotteryParam.currentKindKey != 'SXFS' && fcsddpcPage.lotteryParam.currentKindKey != 'QEZXFS' && fcsddpcPage.lotteryParam.currentKindKey != 'HEZXFS') {
				if (numsStr != "") {
					numArrays.push(numsStr);
				}
			} else {
				numArrays.push(numsStr);
			}

		});

		if (fcsddpcPage.lotteryParam.currentKindKey == 'SXFS' || fcsddpcPage.lotteryParam.currentKindKey == 'QEZXFS' || fcsddpcPage.lotteryParam.currentKindKey == 'HEZXFS') {

			// 后二直选复式
			if (fcsddpcPage.lotteryParam.currentKindKey == 'HEZXFS') {
				codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
			}

			lotteryCount = 1;
			// 统计号码
			for (var i = 0; i < numArrays.length; i++) {
				var numArray = numArrays[i];
				if (numArray.length == 0) { // 为空,表示号码选择未合法
					fcsddpcPage.lotteryParam.currentLotteryCount = 0;// 此时未选注
					fcsddpcPage.lotteryParam.codes = null; // 号码置为空
					lotteryCommonPage.currentCodesStastic(); // 统计当前投注数目和价格
					return;
				}

				codes = codes + numArray + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				// 统计当前数目
				lotteryCount = numArray.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length * lotteryCount;
			}

			// 前二直选复式
			if (fcsddpcPage.lotteryParam.currentKindKey == 'QEZXFS') {
				codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
			}
			codes = codes.substring(0, codes.length - 1);
		} else if (fcsddpcPage.lotteryParam.currentKindKey == 'QEDXDS' || fcsddpcPage.lotteryParam.currentKindKey == 'HEDXDS') {
			if (numArrays.length == 2) { // 总共只有两位数
				var num1Arrays = numArrays[0].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
				var num2Arrays = numArrays[1].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
				if ((num1Arrays == null || num2Arrays == null) || (num1Arrays.length != 1 || num2Arrays.length != 1) || (num1Arrays[0].length == 0 || num2Arrays[0].length == 0)) {
					fcsddpcPage.lotteryParam.currentLotteryCount = 0;// 此时未选注
					fcsddpcPage.lotteryParam.codes = null; // 号码置为空
					lotteryCommonPage.currentCodesStastic(); // 统计当前投注数目和价格
					return;
				} else {
					fcsddpcPage.lotteryParam.currentLotteryCount = 1; // 大小单双一次只能一注
					lotteryCount = 1;
					codes = num1Arrays[0] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + num2Arrays[0];
				}
			} else {
				fcsddpcPage.lotteryParam.currentLotteryCount = 0;// 此时未选注
				fcsddpcPage.lotteryParam.codes = null; // 号码置为空
				lotteryCommonPage.currentCodesStastic(); // 统计当前投注数目和价格
				return;
			}
		} else {
			alert("未知参数1.");
		}
	} else if (fcsddpcPage.lotteryParam.currentKindKey == 'SXDWD' || fcsddpcPage.lotteryParam.currentKindKey == 'RXE') {
		var lotteryNumMap = new JS_OBJECT_MAP(); // 选择号码位数映射
		// 获取 选择的号码
		$(".child-content .content .munbtn").each(function(i) {
			var lis = $(this).parent().children();
			var numsStr = "";
			for (var i = 0; i < lis.length; i++) {
				var li = lis[i];
				var aNum = $(li);
				if ($(aNum).hasClass('on')) {
					numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT;
					lotteryNumMap.remove($(aNum).attr('name'));
					lotteryNumMap.put($(aNum).attr('name'), numsStr);
				}
			}
		});
		// 标识是否有选择对应的位数
		var totalCount = 0;
		var isBallNum_2_match = false;
		var isBallNum_3_match = false;
		var isBallNum_4_match = false;

		if (fcsddpcPage.lotteryParam.currentKindKey == 'SXDWD') {
			var lotteryNumMapKeys = lotteryNumMap.keys();
			// 字符串截取
			for (var i = 0; i < lotteryNumMapKeys.length; i++) {
				var mapValue = lotteryNumMap.get(lotteryNumMapKeys[i]);
				// 去除最后一个分隔符
				if (mapValue.substring(mapValue.length - 1, mapValue.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT) {
					mapValue = mapValue.substring(0, mapValue.length - 1);
				}
				lotteryNumMap.remove(lotteryNumMapKeys[i]);
				lotteryNumMap.put(lotteryNumMapKeys[i], mapValue);

				if (lotteryNumMapKeys[i] == "ballNum_2_match") {
					isBallNum_2_match = true;
				} else if (lotteryNumMapKeys[i] == "ballNum_3_match") {
					isBallNum_3_match = true;
				} else if (lotteryNumMapKeys[i] == "ballNum_4_match") {
					isBallNum_4_match = true;
				} else {
					alert("未知的号码位数");
				}
				totalCount += mapValue.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length;
			}
		} else if (fcsddpcPage.lotteryParam.currentKindKey == 'RXE') {
			var lotteryNumMapKeys = lotteryNumMap.keys();
			if (lotteryNumMapKeys.length >= 2) {
				for (var i = 0; i < lotteryNumMapKeys.length; i++) {
					for (var j = (i + 1); j < lotteryNumMapKeys.length; j++) {
						var map1Value = lotteryNumMap.get(lotteryNumMapKeys[i]);
						var map2Value = lotteryNumMap.get(lotteryNumMapKeys[j]);

						if (lotteryNumMapKeys[i] == "ballNum_2_match") {
							isBallNum_2_match = true;
						} else if (lotteryNumMapKeys[i] == "ballNum_3_match") {
							isBallNum_3_match = true;
						} else if (lotteryNumMapKeys[i] == "ballNum_4_match") {
							isBallNum_4_match = true;
						} else {
							alert("未知的号码位数");
						}

						if (lotteryNumMapKeys[j] == "ballNum_2_match") {
							isBallNum_2_match = true;
						} else if (lotteryNumMapKeys[j] == "ballNum_3_match") {
							isBallNum_3_match = true;
						} else if (lotteryNumMapKeys[j] == "ballNum_4_match") {
							isBallNum_4_match = true;
						} else {
							alert("未知的号码位数");
						}

						// 去除最后一个分隔符
						if (map1Value.substring(map1Value.length - 1, map1Value.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT) {
							map1Value = map1Value.substring(0, map1Value.length - 1);
						}
						if (map2Value.substring(map2Value.length - 1, map2Value.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT) {
							map2Value = map2Value.substring(0, map2Value.length - 1);
						}
						lotteryNumMap.remove(lotteryNumMapKeys[i]);
						lotteryNumMap.put(lotteryNumMapKeys[i], map1Value);
						lotteryNumMap.remove(lotteryNumMapKeys[j]);
						lotteryNumMap.put(lotteryNumMapKeys[j], map2Value);

						var oneLength = map1Value.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length;
						var twoLength = map2Value.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length;

						totalCount += (oneLength * twoLength);
					}
				}
			}
		} else {
			alert("未知参数3");
		}

		// 统计投注数目
		if (totalCount == 0) {
			fcsddpcPage.lotteryParam.currentLotteryCount = 0;// 此时未选注
			fcsddpcPage.lotteryParam.codes = null; // 号码置为空
			lotteryCommonPage.currentCodesStastic(); // 统计当前投注数目和价格
			return;
		} else {
			lotteryCount = totalCount;
		}

		// 百位
		if (isBallNum_2_match) {
			codes = codes + lotteryNumMap.get("ballNum_2_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		} else {
			codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		}
		// 十位
		if (isBallNum_3_match) {
			codes = codes + lotteryNumMap.get("ballNum_3_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		} else {
			codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		}
		// 个位
		if (isBallNum_4_match) {
			codes = codes + lotteryNumMap.get("ballNum_4_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		} else {
			codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		}
		codes = codes.substring(0, codes.length - 1);
	} else if (fcsddpcPage.lotteryParam.currentKindKey == 'SXZXHZ' || fcsddpcPage.lotteryParam.currentKindKey == 'SXZXHZ_G' || fcsddpcPage.lotteryParam.currentKindKey == 'SXZS'
			|| fcsddpcPage.lotteryParam.currentKindKey == 'SXZL' || fcsddpcPage.lotteryParam.currentKindKey == 'SXYMBDW' || fcsddpcPage.lotteryParam.currentKindKey == 'SXEMBDW'
			|| fcsddpcPage.lotteryParam.currentKindKey == 'QEZXHZ' || fcsddpcPage.lotteryParam.currentKindKey == 'QEZXHZ_G'
			|| fcsddpcPage.lotteryParam.currentKindKey == 'QEZXFS_G' || fcsddpcPage.lotteryParam.currentKindKey == 'HEZXHZ'
			|| fcsddpcPage.lotteryParam.currentKindKey == 'HEZXHZ_G' || fcsddpcPage.lotteryParam.currentKindKey == 'HEZXFS_G') {
		var numsStr = "";
		// 获取 选择的号码
		$(".child-content .content .munbtn").each(function(i) {
			var aNum = $(this);
			if ($(aNum).hasClass('on')) {
				numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
			}
		});
		// 去除最后一个分隔符
		if (numsStr.substring(numsStr.length - 1, numsStr.length) == lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT) {
			numsStr = numsStr.substring(0, numsStr.length - 1);
		}

		// 空串拦截
		if (numsStr == "") {
			fcsddpcPage.lotteryParam.currentLotteryCount = 0;// 此时未选注
			fcsddpcPage.lotteryParam.codes = null; // 号码置为空
			lotteryCommonPage.currentCodesStastic(); // 统计当前投注数目和价格
			return;
		}

		// 五星组选120
		if (fcsddpcPage.lotteryParam.currentKindKey == 'SXYMBDW') {
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; // 号码数目
			if (codeCount < 1) { // 号码数目不足
				fcsddpcPage.lotteryParam.currentLotteryCount = 0;// 此时未选注
				fcsddpcPage.lotteryParam.codes = null; // 号码置为空
				lotteryCommonPage.currentCodesStastic(); // 统计当前投注数目和价格
				return;
			}
			lotteryCount = codeCount;
		} else if (fcsddpcPage.lotteryParam.currentKindKey == 'SXEMBDW' || fcsddpcPage.lotteryParam.currentKindKey == 'QEZXFS_G'
				|| fcsddpcPage.lotteryParam.currentKindKey == 'HEZXFS_G') {
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; // 号码数目
			if (codeCount < 2) { // 号码数目不足
				fcsddpcPage.lotteryParam.currentLotteryCount = 0;// 此时未选注
				fcsddpcPage.lotteryParam.codes = null; // 号码置为空
				lotteryCommonPage.currentCodesStastic(); // 统计当前投注数目和价格
				return;
			}
			lotteryCount = (codeCount * (codeCount - 1)) / (2 * 1);
		} else if (fcsddpcPage.lotteryParam.currentKindKey == 'SXZS') {
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; // 号码数目
			if (codeCount < 2) { // 号码数目不足
				fcsddpcPage.lotteryParam.currentLotteryCount = 0;// 此时未选注
				fcsddpcPage.lotteryParam.codes = null; // 号码置为空
				lotteryCommonPage.currentCodesStastic(); // 统计当前投注数目和价格
				return;
			}
			lotteryCount = (codeCount * (codeCount - 1));
		} else if (fcsddpcPage.lotteryParam.currentKindKey == 'SXZL') {
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; // 号码数目
			if (codeCount < 3) { // 号码数目不足
				fcsddpcPage.lotteryParam.currentLotteryCount = 0;// 此时未选注
				fcsddpcPage.lotteryParam.codes = null; // 号码置为空
				lotteryCommonPage.currentCodesStastic(); // 统计当前投注数目和价格
				return;
			}
			lotteryCount = (codeCount * (codeCount - 1) * (codeCount - 2)) / (3 * 2 * 1);
		} else if (fcsddpcPage.lotteryParam.currentKindKey == 'QEZXHZ_G' || fcsddpcPage.lotteryParam.currentKindKey == 'HEZXHZ_G') {
			var codeArray = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT); // 号码数目
			if (numsStr == "" || codeArray == null || codeArray.length == 0) { // 号码数目不足
				fcsddpcPage.lotteryParam.currentLotteryCount = 0;// 此时未选注
				fcsddpcPage.lotteryParam.codes = null; // 号码置为空
				lotteryCommonPage.currentCodesStastic(); // 统计当前投注数目和价格
				return;
			}
			for (var i = 0; i < codeArray.length; i++) {
				lotteryCount += lotteryCommonPage.lottertyDataDeal.getLotteryCountByErZuHeZhi(codeArray[i]);
			}
		} else if (fcsddpcPage.lotteryParam.currentKindKey == 'QEZXHZ' || fcsddpcPage.lotteryParam.currentKindKey == 'HEZXHZ') {
			var codeArray = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT); // 号码数目
			if (numsStr == "" || codeArray == null || codeArray.length == 0) { // 号码数目不足
				fcsddpcPage.lotteryParam.currentLotteryCount = 0;// 此时未选注
				fcsddpcPage.lotteryParam.codes = null; // 号码置为空
				lotteryCommonPage.currentCodesStastic(); // 统计当前投注数目和价格
				return;
			}
			for (var i = 0; i < codeArray.length; i++) {
				lotteryCount += lotteryCommonPage.lottertyDataDeal.getLotteryCountByErZhiHeZhi(codeArray[i]);
			}
		} else if (fcsddpcPage.lotteryParam.currentKindKey == 'SXZXHZ') {
			var codeArray = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT); // 号码数目
			if (numsStr == "" || codeArray == null || codeArray.length == 0) { // 号码数目不足
				fcsddpcPage.lotteryParam.currentLotteryCount = 0;// 此时未选注
				fcsddpcPage.lotteryParam.codes = null; // 号码置为空
				lotteryCommonPage.currentCodesStastic(); // 统计当前投注数目和价格
				return;
			}
			for (var i = 0; i < codeArray.length; i++) {
				lotteryCount += lotteryCommonPage.lottertyDataDeal.getLotteryCountByShanZhiHeZhi(codeArray[i]);
			}
		} else if (fcsddpcPage.lotteryParam.currentKindKey == 'SXZXHZ_G') {
			var codeArray = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT); // 号码数目
			if (numsStr == "" || codeArray == null || codeArray.length == 0) { // 号码数目不足
				fcsddpcPage.lotteryParam.currentLotteryCount = 0;// 此时未选注
				fcsddpcPage.lotteryParam.codes = null; // 号码置为空
				lotteryCommonPage.currentCodesStastic(); // 统计当前投注数目和价格
				return;
			}
			for (var i = 0; i < codeArray.length; i++) {
				lotteryCount += lotteryCommonPage.lottertyDataDeal.getLotteryCountByShanZuHeZhi(codeArray[i]);
			}
		} else {
			alert("未知参数2.");
		}
		codes = numsStr;// 记录投注号码
	} else {
		lotteryCount = 0;
		codes = null;
		alert("玩法参数传递不合法.");
	}

	// 存储当前统计的投注数目
	fcsddpcPage.lotteryParam.currentLotteryCount = lotteryCount;
	// 当前投注号码
	fcsddpcPage.lotteryParam.codes = codes;
	// 统计当前投注数目和价格
	lotteryCommonPage.currentCodesStastic();
};

/**
 * 号码拼接
 * 
 * @param num
 */
FcsddpcPage.prototype.codeStastics = function(codeStastic, index) {

	var codeStr = "";
	var codeDesc = codeStastic[5];
	codeDesc = codeDesc.toString();
	var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_") + 1, codeStastic[6].length);
	if (kindKey == "QEDXDS" || kindKey == "HEDXDS") {
		codeDesc = codeDesc.replaceAll("1", "大");
		codeDesc = codeDesc.replaceAll("2", "小");
		codeDesc = codeDesc.replaceAll("3", "单");
		codeDesc = codeDesc.replaceAll("4", "双");
	}
	if (codeDesc.length < 15) {
		codeStr += "<span>" + codeDesc.replace(/\&/g, " ") + "</span>";
	} else {
		codeStr += "<span>" + codeDesc.substring(0, 15).replace(/\&/g, " ") + "...</span>";
		codeStr += "<a onclick='event.stopPropagation();fcsddpcPage.showDsMsgDialog(this," + index + ")'>详情</a>";
		codeStr += "</span>";
	}

	var str = "";
	// 单式不需要更新操作
	var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_") + 1, codeStastic[6].length);
	if (kindKey == 'SXDS' || kindKey == 'QEZXDS' || kindKey == "QEZXDS_G" || kindKey == "HEZXDS" || kindKey == "HEZXDS_G" || kindKey == "RXEZXDS") {
		str += "<li>";
	} else {
		str += "<li onclick='lotteryCommonPage.updateCodeStastic(this," + index + ")'>";
	}
	str += "<div class='li-child li-title'>" + codeStastic[4] + codeStr + "</div>";
	str += "<div class='li-child li-money'>¥" + codeStastic[0] + "元</div>";
	str += "<div class='li-child li-strip'>" + codeStastic[1] + "注</div>";
	str += "<div class='li-child li-multiple'>" + codeStastic[2] + "倍</div>";
	str += "<div class='li-child li-addmoney'>" + codeStastic[3] + "元</div>";
	str += "<div class='li-child li-close' data-value='" + index + "' name='deleteCurrentCodeStastic'><img src='" + contextPath + "/front/images/lottery/close.png' /></div>"
	str += "</li>";
	return str;
};

/**
 * 展现投注号码
 */
FcsddpcPage.prototype.showDsMsgDialog = function(obj, index) {
	var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[index];
	var kindKeyTop = codeStastic[6].substring(codeStastic[6].indexOf("_") + 1, codeStastic[6].length);

	$("#kindPlayDes").text(codeStastic[4]);
	if (kindKeyTop == "SXDS" || kindKeyTop == "SXFS" || kindKeyTop == "SXZXHZ" || kindKeyTop == "SXZXHZ_G" || kindKeyTop == "SXZS" || kindKeyTop == "SXZL"
			|| kindKeyTop == "SXYMBDW" || kindKeyTop == "SXEMBDW" || kindKeyTop == "SXDWD" || kindKeyTop == "RXE" || kindKeyTop == "RXEZXDS") {
		$("#kindPlayPositionDes").text("位置：百位、十位、个位");
	} else if (kindKeyTop == "QEZXFS" || kindKeyTop == "QEZXDS" || kindKeyTop == "QEZXHZ" || kindKeyTop == "QEZXFS_G" || kindKeyTop == "QEZXDS_G" || kindKeyTop == "QEZXHZ_G"
			|| kindKeyTop == "QEDXDS") {
		$("#kindPlayPositionDes").text("位置：百位、十位");
	} else if (kindKeyTop == "HEZXFS" || kindKeyTop == "HEZXDS" || kindKeyTop == "HEZXHZ" || kindKeyTop == "HEZXFS_G" || kindKeyTop == "HEZXDS_G" || kindKeyTop == "HEZXHZ_G"
			|| kindKeyTop == "HEDXDS") {
		$("#kindPlayPositionDes").text("位置：十位、个位");
	} else {
		showTip("该单式玩法的位置还没配置.");
		return;
	}

	var modelTxt = $("#returnPercent").text();
	if (lotteryCommonPage.lotteryParam.awardModel == 'YUAN') {
		$("#kindPlayModel").text("元模式，" + modelTxt);
	} else if (lotteryCommonPage.lotteryParam.awardModel == 'JIAO') {
		$("#kindPlayModel").text("角模式，" + modelTxt);
	} else if (lotteryCommonPage.lotteryParam.awardModel == 'FEN') {
		$("#kindPlayModel").text("分模式，" + modelTxt);
	} else if (lotteryCommonPage.lotteryParam.awardModel == 'LI') {
		$("#kindPlayModel").text("厘模式，" + modelTxt);
	} else {
		showTip("该投注模式不合法.");
	}

	$("#kindPlayPriceMsg").text(codeStastic[1] + "注，" + lotteryCommonPage.lotteryParam.beishu + "倍, 共计" + codeStastic[0] + "元");
	$("#kindPlayCodeDes").val(codeStastic[5].replace(/\&/g, " "));

	// 展示单式投注号码对话框
	$("#dsContentShowDialog").stop(false, true).delay(200).fadeIn(500);
	$(".alert-msg-bg").stop(false, true).fadeIn(500);
};

/**
 * 获取当前开奖号码的组态显示 组三组六
 */
FcsddpcPage.prototype.getLotteryCodeStatus = function(codeStr) {
	// 控制显示当前号码的形态
	var codeArray = codeStr.split(",");
	var code1 = codeArray[0];
	var code2 = codeArray[1];
	var code3 = codeArray[2];
	var code4 = codeArray[3];
	var code5 = codeArray[4];

	// 后三形态
	var codeStatus = "";
	if ((code3 == code4 && code3 != code5) || (code3 == code5 && code4 != code5) || (code4 == code5 && code3 != code4)) {
		codeStatus = "组三";
	} else if ((code3 != code4) && (code3 != code5) && (code4 != code5)) {
		codeStatus = "组六";
	} else if (code3 == code4 && code4 == code5) {
		codeStatus = "豹子";
	}

	return codeStatus;

};

/**
 * 显示当前投注号码
 * 
 * @param codeStastic
 * @param index
 */
FcsddpcPage.prototype.showCurrentPlayKindCode = function(codesArray) {
	// 判断是否有位数的概念
	var isBallNumMatch = true;
	var kindKey = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey;
	if (kindKey == 'SXDS' || kindKey == 'SXYMBDW' || kindKey == 'SXEMBDW' || kindKey == "SXZXHZ" || kindKey == "SXZXHZ_G" || kindKey == "SXZS" || kindKey == "SXZL"
			|| kindKey == "QEZXDS" || kindKey == "QEZXHZ" || kindKey == "QEZXFS_G" || kindKey == "QEZXDS_G" || kindKey == "QEZXHZ_G" || kindKey == "HEZXDS" || kindKey == "HEZXHZ"
			|| kindKey == "HEZXFS_G" || kindKey == "HEZXDS_G" || kindKey == "HEZXHZ_G") {
		isBallNumMatch = false;
	}

	if (isBallNumMatch) {
		for (var i = 0; i < codesArray.length; i++) {
			if (codesArray[i].indexOf(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT) == -1) {
				$(".l-mun div[name='ballNum_" + i + "_match']").each(function() {
					if ($(this).attr("data-realvalue") == codesArray[i]) {
						$(this).toggleClass("on");
					}
				});
			} else {
				var codesWeiArray = codesArray[i].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
				for (var j = 0; j < codesWeiArray.length; j++) {
					var codeWei = codesWeiArray[j];
					if (codeWei != lotteryCommonPage.lotteryParam.CODE_REPLACE) {
						$(".l-mun div[name='ballNum_" + i + "_match']").each(function() {
							if ($(this).attr("data-realvalue") == codeWei) {
								$(this).toggleClass("on");
							}
						});
					}
				}
			}
		}
	} else {
		for (var i = 0; i < codesArray.length; i++) {
			if (codesArray[i].indexOf(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT) == -1) {
				$(".l-mun div").each(function() {
					if ($(this).attr("data-realvalue") == codesArray[i]) {
						$(this).toggleClass("on");
					}
				});
			} else {
				var codesWeiArray = codesArray[i].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
				for (var j = 0; j < codesWeiArray.length; j++) {
					var codeWei = codesWeiArray[j];
					if (codeWei != lotteryCommonPage.lotteryParam.CODE_REPLACE) {
						$(".l-mun div[name='ballNum_" + i + "_match']").each(function() {
							if ($(this).attr("data-realvalue") == codeWei) {
								$(this).toggleClass("on");
							}
						});
					}
				}
			}
		}
	}
};

/**
 * 单式投注数目控制
 * 
 * @param eventType
 */
FcsddpcPage.prototype.lotteryCountStasticForDs = function(contentLength) {
	// 任选二直选单式 //存储注数目
	if (fcsddpcPage.lotteryParam.currentKindKey == 'RXEZXDS') { // 需要乘以对应位置的方案数目
		fcsddpcPage.lotteryParam.currentLotteryCount = contentLength * lotteryCommonPage.lottertyDataDeal.rxParam.dsAllowCount;
	} else {
		fcsddpcPage.lotteryParam.currentLotteryCount = contentLength;
	}
};

/**
 * 显示当前玩法的分页
 * 
 * @param codeStastic
 * @param index
 */
FcsddpcPage.prototype.showCurrentPlayKindPage = function(kindKeyTop) {
	// 先清除星种的选择
	$('.classify-list').each(function() {
		$(this).parent().removeClass("on");
	});
	$('.classify-list').each(function() {
		if (kindKeyTop.indexOf("DXDS") != -1) { // 大小单双额外处理
			$(".classify-list[id='DXDS']").parent().addClass("on");
			return;
		} else if (kindKeyTop.indexOf("SXDWD") != -1) {
			$(".classify-list[id='YX']").parent().addClass("on");
			return;
		} else {
			if (kindKeyTop.indexOf($(this).attr('id')) != -1) {
				$(this).parent().addClass("on");
				return;
			}
		}
	});
};

/**
 * 福彩3D彩种格式化
 * 
 * @param isCut
 *            格式化之后对多余的数字是否截取
 */
FcsddpcPage.prototype.formatNum = function(isCut) {
	var kindKey = fcsddpcPage.lotteryParam.currentKindKey;
	var pastecontent = $("#lr_editor").val();

	pastecontent = pastecontent.replace(/[^\d]/g, '');
	if (pastecontent == "") {
		$("#lr_editor").val("");
		// 设置当前投注数目或者投注价格
		/*
		 * $('#J-balls-statistics-lotteryNum').text("0"); //显示投注
		 * $('#J-balls-statistics-amount').text(parseFloat("0").toFixed(lotteryCommonPage.param.toFixedValue));
		 * $('#J-balls-statistics-code').text("(投注内容)");
		 */
		return false;
	}
	var len = pastecontent.length;
	var num = "";
	var numtxt = "";
	var n = 0;
	var maxnum = 1;

	if (kindKey == 'SXDS') {
		maxnum = 3;
	} else if (kindKey == 'QEZXDS' || kindKey == "QEZXDS_G" || kindKey == "HEZXDS" || kindKey == "HEZXDS_G" || kindKey == "RXEZXDS") {
		maxnum = 2;
	} else {
		showTip("未配置该单式的控制.");
	}

	// 下方显示的号码
	var numsStr = "";
	for (var i = 0; i < len; i++) {
		if (i % maxnum == 0) {
			n = 1;
		} else {
			n = n + 1;
		}
		var currentChar = pastecontent.substr(i, 1);
		// 截取处理的
		if (isCut) {
			if (n < maxnum) {
				num = num + currentChar + ",";
				numsStr = numsStr + currentChar + ",";
			} else {
				num = num + currentChar;
				numtxt = numtxt + num + "\n";
				num = "";
				numsStr = numsStr + currentChar + " ";
			}
		} else {
			// 不需要截取多余处理的
			if (n < maxnum) {
				numtxt = numtxt + currentChar + ",";
				numsStr = numsStr + currentChar + ",";
			} else {
				numtxt = numtxt + currentChar + "\n";
				numsStr = numsStr + currentChar + " ";
			}
		}
	}
	numsStr = numsStr.substring(0, numsStr.length - 1);
	// 如果格式化截取多余的 内容为空，显示号码也为空
	if (isCut) {
		if (numtxt == "" || numtxt.length == 0) {
			numsStr = "";
		}
	} else {
		numtxt = numtxt.substring(0, numtxt.length - 1);
	}
	// 处理投注下方显示的号码
	fcsddpcPage.lotteryParam.codesStr = numsStr;
	$("#lr_editor").val(numtxt);
	return true;
};

/**
 * 单式的选号投注
 */
FcsddpcPage.prototype.singleLotteryNum = function() {
	var kindKey = fcsddpcPage.lotteryParam.currentKindKey;

	// 如果是任选二的单式录入
	if (kindKey == 'RXEZXDS') {
		var editorVal = $("#lr_editor").val();
		if (editorVal.indexOf("说明") != -1) {
			showTip("请先填写投注号码");
			return false;
		}
		lotteryCommonPage.dealForDsFormat(); // 首先格式化,避免直接点击选号码的事件
		var pastecontent = $("#lr_editor").val();
		if (pastecontent == "") {
			showTip("请先填写投注号码");
			return false;
		}
		pastecontent = pastecontent.replace(/\r\n/g, "$");
		pastecontent = pastecontent.replace(/\n/g, "$");
		pastecontent = pastecontent.replace(/\s/g, "");
		pastecontent = pastecontent.replace(/(\$)\1+/g, '$1');
		if (pastecontent.substr(pastecontent.length - 1, 1) == "$") {
			pastecontent = pastecontent.substr(0, pastecontent.length - 1);
		}

		pastecontent = pastecontent.replace(/\$/g, "&");
		var pastecontentArr = pastecontent;
		var pastecontentLength = pastecontent.split("&").length;

		if (kindKey == 'RXEZXDS') {
			if (fcsddpcPage.lotteryParam.currentLotteryCount > 2000) {
				showTip("当前玩法最多支持2000注单式内容，请调整！");
				return;
			} else if (fcsddpcPage.lotteryParam.currentLotteryCount == 0) {
				showTip("当前没有投注数目，请查看.");
				return;
			}
		}

		var errzhushu = "";
		var errzhushumsg = "[格式错误]"
		var reg1, reg2;

		if (kindKey == 'RXEZXDS') { // 任选二直选单式
			reg1 = /^((?:(?:[0-9]),){1}(?:[0-9])[&]){0,}(?:(?:[0-9]),){1}(?:[0-9])$/;
		} else {
			alert("未配置该类型的单式");
		}

		// 号码校验
		if (!reg1.test(pastecontentArr)) {
			showTip("单式号码格式不合法.");
			return false;
		}

		// 位数选取判断
		var positionCode = "";
		$("input[name='ds_position']").each(function() {
			if (this.checked) {
				positionCode += "1,";
			} else {
				positionCode += "0,";
			}
		});
		positionCode = positionCode.substring(0, positionCode.length - 1);
		fcsddpcPage.lotteryParam.codes = pastecontentArr;

		var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
		var currentLotteryWin = fcsddpcPage.param.currentLotteryWins.get(fcsddpcPage.param.kindNameType + "_" + fcsddpcPage.lotteryParam.currentKindKey);
		lotteryCommonPage.lotteryParam.beishu = parseInt(lotteryCommonPage.lotteryParam.beishu);
		fcsddpcPage.lotteryParam.currentLotteryTotalCount++;

		// 将所选号码和玩法的映射放置map中
		var lotteryKindMap = new JS_OBJECT_MAP();
		if (kindKey == 'RXEZXDS') {
			lotteryKindMap.put(fcsddpcPage.lotteryParam.currentKindKey, fcsddpcPage.lotteryParam.codes + "_" + positionCode);
		} else {
			lotteryKindMap.put(fcsddpcPage.lotteryParam.currentKindKey, fcsddpcPage.lotteryParam.codes);
		}
		lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_" + fcsddpcPage.lotteryParam.currentLotteryTotalCount, lotteryKindMap);
		// 投注倍数存储
		lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_" + fcsddpcPage.lotteryParam.currentLotteryTotalCount, lotteryCommonPage.lotteryParam.beishu);

		// 进入投注池当中
		var codeRecord = new Array();
		codeRecord.push(lotteryCommonPage
				.getAwardByLotteryModel((fcsddpcPage.lotteryParam.currentLotteryCount * fcsddpcPage.param.unitPrice * lotteryCommonPage.lotteryParam.beishu)
						.toFixed(lotteryCommonPage.param.toFixedValue)));
		codeRecord.push(fcsddpcPage.lotteryParam.currentLotteryCount);
		codeRecord.push(lotteryCommonPage.lotteryParam.beishu);
		codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode)
				* lotteryCommonPage.lotteryParam.beishu));
		codeRecord.push(fcsddpcPage.lotteryParam.lotteryKindMap.get(fcsddpcPage.lotteryParam.currentKindKey));
		codeRecord.push(fcsddpcPage.lotteryParam.codes);
		codeRecord.push(fcsddpcPage.param.kindNameType + "_" + fcsddpcPage.lotteryParam.currentKindKey); // 标识玩法
		codeRecord.push("lottery_id_" + fcsddpcPage.lotteryParam.currentLotteryTotalCount);
		fcsddpcPage.lotteryParam.codesStastic.push(codeRecord);

		// 选好号码统计金额注数
		lotteryCommonPage.codeStastics();
		$("#lr_editor").val("");// 清空注码
	} else {
		// 如果是三星的单式录入
		var editorVal = $("#lr_editor").val();
		if (editorVal.indexOf("说明") != -1) {
			showTip("请先填写投注号码");
			// alert("请先填写投注号码");
			return false;
		}

		lotteryCommonPage.dealForDsFormat(); // 首先格式化,避免直接点击选号码的事件
		var pastecontent = $("#lr_editor").val();
		if (pastecontent == "") {
			showTip("请先填写投注号码");
			// alert("请先填写投注号码");
			return false;
		}
		pastecontent = pastecontent.replace(/\r\n/g, "$");
		pastecontent = pastecontent.replace(/\n/g, "$");
		pastecontent = pastecontent.replace(/\s/g, "");
		pastecontent = pastecontent.replace(/(\$)\1+/g, '$1');
		if (pastecontent.substr(pastecontent.length - 1, 1) == "$") {
			pastecontent = pastecontent.substr(0, pastecontent.length - 1);
		}
		var pastecontentArr = pastecontent.split("$");
		if (pastecontentArr.length > 2000) {
			showTip("当前玩法最多支持2000注单式内容，请调整！");
			return;
		}

		var errzhushu = "";
		var errzhushumsg = "[格式错误]"
		var reg1, reg2;
		if (kindKey == 'SXDS') { // 五星单式 1,2,3,4,5
			reg1 = /^(?:(?:[0-9]),){2}(?:[0-9])\s*$/;
			reg2 = /^\d{3}\s*$/;
		} else if (kindKey == 'QEZXDS' || kindKey == "QEZXDS_G" || kindKey == "HEZXDS" || kindKey == "HEZXDS_G") { // 二星直选单式
																													// 和
																													// 二星组选单式
			reg1 = /^(?:(?:[0-9]),)(?:[0-9])\s*$/;
			reg2 = /^\d{2}\s*$/;
		} else if (kindKey == 'RXEZXDS') {
			if (!lotteryCommonPage.lottertyDataDeal.param.isRxDsAllow) {
				showTip("您还未选择任选二单式的位置,无法添加号码");
				return;
			}
			reg1 = /^(?:(?:[0-9]),)(?:[0-9])\s*$/;
			reg2 = /^\d{2}\s*$/;
		} else {
			alert("未配置该类型的单式");
		}

		var isAppendCode = false;
		var appendCodeArray = new Array();
		var dsCodeCodes = ""; // 单式号码拼接

		for (var i = 0; i < pastecontentArr.length; i++) {
			var value1 = pastecontentArr[i];
			if (!reg1.test(value1) && !reg2.test(value1)) {
				if (errzhushu == "") {
					errzhushu = "第" + (i + 1).toString() + "注";
				} else {
					errzhushu = errzhushu + " <br/>" + "第" + (i + 1).toString() + "注";
				}
				continue;
			}

			if (kindKey == 'QEZXDS_G' || kindKey == 'HEZXDS_G') { // 2星组组选单式
				var lsstr = value1;
				sstr = lsstr.replace(/,/g, "");
				a = sstr.substr(0, 1);
				b = sstr.substr(1, 1);
				if (a == b) {
					if (errzhushu == "") {
						errzhushu = "第" + (i + 1).toString() + "注";
					} else {
						errzhushu = errzhushu + "、" + "第" + (i + 1).toString() + "注";
					}
				}
			}

			// 单式号码拼接
			dsCodeCodes += pastecontentArr[i] + "  ";

			var position1 = null;
			var position2 = null;
			var position3 = null;
			var position4 = null;
			var position5 = null;
			var positionCount = 2;

			// 任选单式特殊处理,追加不同位置的单式号码
			if (kindKey == 'RXEZXDS') { // 任选二
				$("input[name='ds_position']").each(function() {
					positionCount++;
					if (this.checked) {
						if (positionCount == 1) {
							position1 = $(this).attr("data-position");
						} else if (positionCount == 2) {
							position2 = $(this).attr("data-position");
						} else if (positionCount == 3) {
							position3 = $(this).attr("data-position");
						} else if (positionCount == 4) {
							position4 = $(this).attr("data-position");
						} else if (positionCount == 5) {
							position5 = $(this).attr("data-position");
						} else {
							alert("该位数不正常.");
						}
					}
				});

				if (lotteryCommonPage.lottertyDataDeal.param.dsAllowCount > 1) { // 方案数目不止一个的情况
					var codeValue = pastecontentArr[i];
					appendCodeArray.push(codeValue);
					isAppendCode = true;

					pastecontentArr[i] = "";
				} else {
					pastecontentArr[i] = fcsddpcPage.rxDsCodeJoin(position1, position2, position3, null, null, pastecontentArr[i]);
				}
			}
		}

		// 任选二、如果需要追加号码
		if (kindKey == 'RXEZXDS' && isAppendCode && appendCodeArray.length > 0) {
			for (var i = 0; i < appendCodeArray.length; i++) {
				if (position3 != null && position4 != null) { // 3 4
					pastecontentArr.push(fcsddpcPage.rxDsCodeJoin(position3, position4, null, null, null, appendCodeArray[i]));
				}
				if (position3 != null && position5 != null) { // 3 5
					pastecontentArr.push(fcsddpcPage.rxDsCodeJoin(position3, null, position5, null, null, appendCodeArray[i]));
				}
				if (position4 != null && position5 != null) { // 4 5
					pastecontentArr.push(fcsddpcPage.rxDsCodeJoin(null, position4, position5, null, null, appendCodeArray[i]));
				}
			}
		}

		// 显示错误注号码
		if (errzhushu != "") {
			showTip(errzhushu + "  " + "号码有误，请核对！");
			// alert(errzhushu+" "+"号码有误，请核对！");
			return false;
		}

		var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
		var currentLotteryWin = fcsddpcPage.param.currentLotteryWins.get(fcsddpcPage.param.kindNameType + "_" + fcsddpcPage.lotteryParam.currentKindKey);
		// var beishu = $('#beishu').attr('data-value');
		lotteryCommonPage.lotteryParam.beishu = parseInt(lotteryCommonPage.lotteryParam.beishu);
		fcsddpcPage.lotteryParam.currentLotteryCount = 1;

		// 单式记录标识
		var dsCodeRecordArray = new Array();
		var dsLotteryCount = 0;
		for (var i = 0; i < pastecontentArr.length; i++) {
			var value = pastecontentArr[i];
			if (value == "") {
				continue;
			}
			var text = "";
			if (kindKey == 'SXDS') {
				value = value;
			} else if (kindKey == 'QEZXDS') {// 前二直选单式
				value = value + ",-";
			} else if (kindKey == 'HEZXDS') {// 后二直选单式
				value = "-," + value;
			} else if (kindKey == 'QEZXDS_G' || kindKey == 'HEZXDS_G' || kindKey == 'RXEZXDS') { // 任选的玩法,已经拼接好投注号码
				value = value;
			} else {
				alert("该单式号码未配置");
				return false;
			}

			fcsddpcPage.lotteryParam.currentLotteryTotalCount++;
			lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryCount = 1;
			fcsddpcPage.lotteryParam.codes = value;

			// 将所选号码和玩法的映射放置map中
			var lotteryKindMap = new JS_OBJECT_MAP();
			lotteryKindMap.put(fcsddpcPage.lotteryParam.currentKindKey, fcsddpcPage.lotteryParam.codes);
			lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_" + fcsddpcPage.lotteryParam.currentLotteryTotalCount, lotteryKindMap);
			// 投注倍数存储
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_" + fcsddpcPage.lotteryParam.currentLotteryTotalCount, lotteryCommonPage.lotteryParam.beishu);

			fcsddpcPage.lotteryParam.codes = null;

			dsCodeRecordArray.push("lottery_id_" + fcsddpcPage.lotteryParam.currentLotteryTotalCount);
			dsLotteryCount++; // 统计数目
		}

		// 进入投注池当中
		var codeRecord = new Array();
		codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((dsLotteryCount * fcsddpcPage.param.unitPrice * lotteryCommonPage.lotteryParam.beishu)
				.toFixed(lotteryCommonPage.param.toFixedValue)));
		codeRecord.push(dsLotteryCount);
		codeRecord.push(lotteryCommonPage.lotteryParam.beishu);
		codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode)
				* lotteryCommonPage.lotteryParam.beishu));
		codeRecord.push(fcsddpcPage.lotteryParam.lotteryKindMap.get(fcsddpcPage.lotteryParam.currentKindKey));
		codeRecord.push(dsCodeCodes);
		codeRecord.push(fcsddpcPage.param.kindNameType + "_" + fcsddpcPage.lotteryParam.currentKindKey); // 标识玩法
		codeRecord.push(dsCodeRecordArray);
		fcsddpcPage.lotteryParam.codesStastic.push(codeRecord);

		// 选好号码统计金额注数
		lotteryCommonPage.codeStastics();
		$("#lr_editor").val("");// 清空注码
	}

	return true;
};

/**
 * 任选号码拼接
 */
FcsddpcPage.prototype.rxDsCodeJoin = function(position1, position2, position3, position4, position5, codeValue) {
	var codeArray = codeValue.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT);
	var codeStr = "";
	var codeArrayCount = 0;
	if (position1 != null) { // 第一位
		codeStr += codeArray[codeArrayCount];
		codeArrayCount++;
	} else {
		codeStr += "-";
	}
	if (position2 != null) { // 第二位
		codeStr += "," + codeArray[codeArrayCount];
		codeArrayCount++;
	} else {
		codeStr += ",-";
	}
	if (position3 != null) { // 第三位
		codeStr += "," + codeArray[codeArrayCount];
		codeArrayCount++;
	} else {
		codeStr += ",-";
	}
	if (position4 != null) { // 第四位
		codeStr += "," + codeArray[codeArrayCount];
		codeArrayCount++;
	} else {
		codeStr += ",-";
	}
	if (position5 != null) { // 第五位
		codeStr += "," + codeArray[codeArrayCount];
		codeArrayCount++;
	} else {
		codeStr += ",-";
	}
	return codeStr;
};

/**
 * 初始化彩种信息
 */
FcsddpcPage.prototype.initLottery = function() {
	// 先加载所有的玩法
	lotteryCommonPage.lottertyDataDeal.loadLotteryKindPlay();

	// 取消追号处理
	lotteryCommonPage.lotteryParam.isZhuiCode = false;

	// 设置玩法描述数据
	lotteryCommonPage.lottertyDataDeal.setKindPlayDes();

	// 选择默认的玩法，默认是五星定位胆
	fcsddpcPage.lotteryParam.currentKindKey = "QEZXFS";
	lotteryCommonPage.lottertyDataDeal.lotteryKindPlayChoose(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey);

	// 动画效果
	animate_add(".main-head", "fadeInDown", 0);
	animate_add(".lottery-content", "fadeInUp", 0);
	animate_add(".lottery-bar .bar", "fadeInUp", 0);

	// 添加星种选中事件
	$("#lotteryStarKindList .child").unbind("click").click(function() {
		var roleId = $(this).attr("data-role-id");
		// 如果有包含玩法属性
		if (isNotEmpty(roleId) && lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey != roleId) {
			// 清空当前选中号码
			lotteryCommonPage.clearCurrentCodes();
			lotteryCommonPage.lottertyDataDeal.lotteryKindPlayChoose(roleId);
			// 加载星种玩法下拉
		} else {
			var kindPlayClass = $(this).attr("data-id");
			var parent = $(this).closest("body");
			parent.find(".lottery-classify2 .move-content .child").removeClass('color-red');
			$(this).addClass('color-red');
			parent.find(".lottery-classify2 .clist").removeClass('in').addClass('out');
			parent.find("." + kindPlayClass).addClass('in').removeClass('out');
		}
	});
	// 添加玩法选中事件
	$("#lotteryKindPlayList .line-content .child").unbind("click").click(function() {
		var roleId = $(this).attr("data-role-id");
		// 已经是当前玩法了，不需要处理
		if (lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey == roleId) {
			return;
		}

		// 清空当前选中号码
		lotteryCommonPage.clearCurrentCodes();

		// 控制玩法的号码展示
		lotteryCommonPage.lottertyDataDeal.lotteryKindPlayChoose(roleId);

		// 隐藏下拉玩法列表
		element_toggle(false, [ '.lottery-classify' ]);
	});

	// 点击当前玩法显示下拉其他玩法
	$("#currentKindDiv").unbind("click").click(function() {
		var e = $(this);
		var ele = ".lottery-classify2";
		var parent = $(e).closest("body");
		var display = $(ele).css("display");
		var style = "boolean";
		style = display == "none" ? "true" : "boolean";
		parent.find(".lottery-classify").not(ele).hide();
		element_toggle(style, [ ele ], parent);

		// 触发星种下拉
		$("#lotteryStarKindList .child").each(function() {
			if ($(this).hasClass('color-red')) {
				var kindPlayClass = $(this).attr("data-id");
				var parent = $(this).closest("body");
				parent.find(".lottery-classify2 .clist").removeClass('in').addClass('out');
				parent.find("." + kindPlayClass).addClass('in').removeClass('out');
				return false;
			}
		});
	});

	// 绑定选择其他地区彩种事件
	$("#otherLotteryKind").unbind("click").click(function() {
		var ele = '.lottery-classify1';
		var parent = $(this).closest("body");
		var display = $(ele).css("display");
		var style = "boolean";
		style = display == "none" ? "true" : "boolean";
		parent.find(".lottery-classify").not(ele).hide();
		element_toggle(style, [ ele ], parent);
	});

	// 点击开奖号码显示开奖记录事件
	$(".lottery_run").unbind("click").click(function() {
		var parent = $("body");
		if (parent.find(".lottery-dynamic").hasClass('in')) {
			parent.find(".lottery-dynamic").removeClass('in').addClass('out');
		} else {
			parent.find(".lottery-dynamic").removeClass('out').addClass('in');
		}
	});

	// 倍数改变事件
	lotteryCommonPage.beishuEvent();

	// 倍数减少事件
	$("#beishuSub").unbind("click").click(function() {
		var beishuStr = $('#beishu').val();
		var beishuTemp = parseInt(beishuStr);
		beishuTemp--;
		if (beishuTemp <= 0) {
			showTip("当前倍数已经是最低了");
			return;
		}
		$("#beishu").val(beishuTemp);
		lotteryCommonPage.beishuEventContent();
	});

	// 倍数增加事件
	$("#beishuAdd").unbind("click").click(function() {
		var beishuStr = $('#beishu').val();
		var beishuTemp = parseInt(beishuStr);
		beishuTemp++;
		if (beishuTemp > 1000000) {
			showTip("倍数最高1000000");
			return;
		}
		$("#beishu").val(beishuTemp);
		lotteryCommonPage.beishuEventContent();
	});

	// 确认投注-展示投注信息事件
	$("#showLotteryMsgButton").unbind("click").click(function() {
		// 非追号类型 处理添加号码到投注池
		if (!lotteryCommonPage.lotteryParam.isZhuiCode) {
			lotteryCommonPage.currentLotteryKindInstance.userLotteryNumForAddOrder();
		}
		lotteryCommonPage.showLotteryMsg();
	});
	// 关闭投注信息事件
	$("#lotteryOrderDialogCancelButton").unbind("click").click(function() {
		lotteryCommonPage.hideLotteryMsg();
	});

	// 关闭投注信息事件
	$(".close").unbind("click").click(function() {
		lotteryCommonPage.hideLotteryMsg();
	});

	// 确认投注-提交后台投注事件
	$("#lotteryOrderDialogSureButton").unbind("click").click(function() {
		lotteryCommonPage.userLottery();
	});

	// 机选一注事件
	$("#randomOneBtn").unbind("click").click(function() {
		lotteryCommonPage.randomOneEvent();
	});

	// 清空投注池号码
	$("#clearAllCodeBtn").unbind("click").click(function() {
		lotteryCommonPage.clearCodeStasticEvent();
		/*
		 * comfirm("提示","确认删除号码篮内全部内容吗?","取消",function(){},"确认",
		 * lotteryCommonPage.clearCodeStasticEvent);
		 */

	});

	// 点击追号取消追号事件
	$("#zhuiCodeButton").unbind("click").click(function(param, showParam) {
		lotteryCommonPage.changeTozhuiCodeEvent();
	});

	// 获取最新的开奖号码
	lotteryCommonPage.getLastLotteryCode();
	// 获取当前彩种的最新期号和投注倒计时
	lotteryCommonPage.getActiveExpect();
	// 加载今天和明天的期号
	lotteryCommonPage.getAllExpectsByTodayAndTomorrow();
	// 近10期的开奖记录
	lotteryCommonPage.getNearestTenLotteryCode();
	// 获取用户今日投注记录
	lotteryCommonPage.getTodayLotteryByUser();

	// 每隔10秒钟请求一次获取最新的开奖号码
	if (lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JLFFC" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "HGFFC") {
		window.setInterval("lotteryCommonPage.getLastLotteryCode()", 5000); // 分分彩读取最新的开奖号码
		// window.setInterval("lotteryCommonPage.getNearestTenLotteryCode()",5000);
		window.setInterval("lotteryCommonPage.getActiveExpect()", 30000); // 30秒重新读取服务器的时间
	} else {
		window.setInterval("lotteryCommonPage.getLastLotteryCode()", 10000); // 其他彩种读取最新的开奖号码
		// window.setInterval("lotteryCommonPage.getNearestTenLotteryCode()",10000);
		window.setInterval("lotteryCommonPage.getActiveExpect()", 60000); // 1分钟重新读取服务器的时间
	}
};

// 玩法描述映射值
fcsddpcPage.lotteryParam.lotteryKindMap.put("SXFS", "[三星_复式]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("SXDS", "[三星_单式]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("SXZXHZ", "[三星_直选和值]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("SXZXHZ_G", "[三星_组选和值]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("SXZS", "[三星_组三]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("SXZL", "[三星_组六]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("SXYMBDW", "[三星_一码不定位]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("SXEMBDW", "[三星_二码不定位]");

fcsddpcPage.lotteryParam.lotteryKindMap.put("QEZXFS", "[前二_直选复式]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("QEZXDS", "[前二_直选单式]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("QEZXHZ", "[前二_直选和值]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("QEZXFS_G", "[前二_组选复式]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("QEZXDS_G", "[前二_组选单式]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("QEZXHZ_G", "[前二_组选和值]");

fcsddpcPage.lotteryParam.lotteryKindMap.put("HEZXFS", "[后二_直选复式]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("HEZXDS", "[后二_直选单式]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("HEZXHZ", "[后二_直选和值]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("HEZXFS_G", "[后二_组选复式]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("HEZXDS_G", "[后二_组选单式]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("HEZXHZ_G", "[后二_组选和值]");

fcsddpcPage.lotteryParam.lotteryKindMap.put("SXDWD", "[三星_定位胆]");

fcsddpcPage.lotteryParam.lotteryKindMap.put("QEDXDS", "[大小单双_前二复式]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("HEDXDS", "[大小单双_后二复式]");

fcsddpcPage.lotteryParam.lotteryKindMap.put("RXE", "[任选二直选复式]");
fcsddpcPage.lotteryParam.lotteryKindMap.put("RXEZXDS", "[任选二_直选单式]");

$(document).ready(function() {

	// 设置数据处理器 默认时时彩的
	lotteryCommonPage.lottertyDataDeal = dpcData;
	// 将这个投注实例传至common js当中
	lotteryCommonPage.currentLotteryKindInstance = fcsddpcPage;
	// 设置投注的模式
	lotteryCommonPage.lotteryParam.modelValue = currentUserDpcModel;
	lowestAwardModel = dPCLowestAwardModel;
	// 获取当前彩种的所有奖金列表
	lotteryCommonPage.currentLotteryKindInstance.getLotteryWins();
	// 获取当前投注模式 元角模式
	lotteryCommonPage.loadUserModelSet();
	setTimeout("fcsddpcPage.initLottery()", 100);
});
