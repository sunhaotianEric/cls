function DpcData(){}
var dpcData = new DpcData();

//任选参数
dpcData.rxParam = {
	isRxDsAllow : true,  //是否允许任选单式,由位数来进行控制
    dsAllowCount : 0,
};


//通用的号码数组
dpcData.common_code_array = new Array(0,1,2,3,4,5,6,7,8,9);
//前三或者后三直选和值
dpcData.qszxhz_code_array = new Array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27);
//前三或者后三组选和值
dpcData.qszxhz_g_code_array = new Array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26);
//前二或者后二直选和值
dpcData.qezxhz_code_array = new Array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18);
//前二或者后二组选和值
dpcData.qezxhz_g_code_array = new Array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17);
//大小单双
dpcData.dxds_code_array = new Array(1,2,3,4);

//////////////////玩法//////////////////// 
dpcData.plays = {};

//三星玩法
dpcData.plays.SX_PLAY = "";
dpcData.plays.SX_PLAY += "<div class='container clist SX animated'>";
dpcData.plays.SX_PLAY += "	<div class='list-line'>";
dpcData.plays.SX_PLAY += "		<div class='line-title'><p>直选</p></div>";
dpcData.plays.SX_PLAY += "		<div class='line-content'>";
dpcData.plays.SX_PLAY += "			<div class='child color-red no' data-role-id='SXFS'><p>复式</p></div>";
dpcData.plays.SX_PLAY += "			<div class='child color-red no' data-role-id='SXDS'><p>单式</p></div>";
dpcData.plays.SX_PLAY += "			<div class='child color-red no' data-role-id='SXZXHZ'><p>和值</p></div>";
dpcData.plays.SX_PLAY += "		</div>";
dpcData.plays.SX_PLAY += "	</div>";
dpcData.plays.SX_PLAY += "	<div class='list-line'>";
dpcData.plays.SX_PLAY += "		<div class='line-title'><p>组选</p></div>";
dpcData.plays.SX_PLAY += "		<div class='line-content'>";
dpcData.plays.SX_PLAY += "			<div class='child color-red no' data-role-id='SXZXHZ_G'><p>和值</p></div>";
dpcData.plays.SX_PLAY += "			<div class='child color-red no' data-role-id='SXZS'><p>三星组三</p></div>";
dpcData.plays.SX_PLAY += "			<div class='child color-red no' data-role-id='SXZL'><p>三星组六</p></div>";
dpcData.plays.SX_PLAY += "		</div>";
dpcData.plays.SX_PLAY += "	</div>";
dpcData.plays.SX_PLAY += "	<div class='list-line'>";
dpcData.plays.SX_PLAY += "		<div class='line-title'><p>不定位</p></div>";
dpcData.plays.SX_PLAY += "		<div class='line-content'>";
dpcData.plays.SX_PLAY += "			<div class='child color-red no' data-role-id='SXYMBDW'><p>一码不定位</p></div>";
dpcData.plays.SX_PLAY += "			<div class='child color-red no' data-role-id='SXEMBDW'><p>二码不定位</p></div>";
dpcData.plays.SX_PLAY += "		</div>";
dpcData.plays.SX_PLAY += "	</div>";
dpcData.plays.SX_PLAY += "</div>";

//前二玩法
dpcData.plays.QE_PLAY = "";
dpcData.plays.QE_PLAY += "<div class='container clist QE animated'>";
dpcData.plays.QE_PLAY += "	<div class='list-line'>";
dpcData.plays.QE_PLAY += "		<div class='line-title'><p>直选</p></div>";
dpcData.plays.QE_PLAY += "		<div class='line-content'>";
dpcData.plays.QE_PLAY += "			<div class='child color-red no' data-role-id='QEZXFS'><p>复式</p></div>";
dpcData.plays.QE_PLAY += "			<div class='child color-red no' data-role-id='QEZXDS'><p>单式</p></div>";
dpcData.plays.QE_PLAY += "			<div class='child color-red no' data-role-id='QEZXHZ'><p>和值</p></div>";
dpcData.plays.QE_PLAY += "		</div>";
dpcData.plays.QE_PLAY += "	</div>";
dpcData.plays.QE_PLAY += "	<div class='list-line'>";
dpcData.plays.QE_PLAY += "		<div class='line-title'><p>组选</p></div>";
dpcData.plays.QE_PLAY += "		<div class='line-content'>";
dpcData.plays.QE_PLAY += "			<div class='child color-red no' data-role-id='QEZXFS_G'><p>复式</p></div>";
dpcData.plays.QE_PLAY += "			<div class='child color-red no' data-role-id='QEZXDS_G'><p>单式</p></div>";
dpcData.plays.QE_PLAY += "			<div class='child color-red no' data-role-id='QEZXHZ_G'><p>和值</p></div>";
dpcData.plays.QE_PLAY += "		</div>";
dpcData.plays.QE_PLAY += "	</div>";
dpcData.plays.QE_PLAY += "</div>";

//后二玩法
dpcData.plays.HE_PLAY = "";
dpcData.plays.HE_PLAY += "<div class='container clist HE animated'>";
dpcData.plays.HE_PLAY += "	<div class='list-line'>";
dpcData.plays.HE_PLAY += "		<div class='line-title'><p>直选</p></div>";
dpcData.plays.HE_PLAY += "		<div class='line-content'>";
dpcData.plays.HE_PLAY += "			<div class='child color-red no' data-role-id='HEZXFS'><p>复式</p></div>";
dpcData.plays.HE_PLAY += "			<div class='child color-red no' data-role-id='HEZXDS'><p>单式</p></div>";
dpcData.plays.HE_PLAY += "			<div class='child color-red no' data-role-id='HEZXHZ'><p>和值</p></div>";
dpcData.plays.HE_PLAY += "		</div>";
dpcData.plays.HE_PLAY += "	</div>";
dpcData.plays.HE_PLAY += "	<div class='list-line'>";
dpcData.plays.HE_PLAY += "		<div class='line-title'><p>组选</p></div>";
dpcData.plays.HE_PLAY += "		<div class='line-content'>";
dpcData.plays.HE_PLAY += "			<div class='child color-red no' data-role-id='HEZXFS_G'><p>复式</p></div>";
dpcData.plays.HE_PLAY += "			<div class='child color-red no' data-role-id='HEZXDS_G'><p>单式</p></div>";
dpcData.plays.HE_PLAY += "			<div class='child color-red no' data-role-id='HEZXHZ_G'><p>和值</p></div>";
dpcData.plays.HE_PLAY += "		</div>";
dpcData.plays.HE_PLAY += "	</div>";
dpcData.plays.HE_PLAY += "</div>";

//一星玩法
dpcData.plays.YX_PLAY = "";
dpcData.plays.YX_PLAY += "<div class='container clist YX animated'>";
dpcData.plays.YX_PLAY += "	<div class='list-line'>";
dpcData.plays.YX_PLAY += "		<div class='line-title'><p>直选</p></div>";
dpcData.plays.YX_PLAY += "		<div class='line-content'>";
dpcData.plays.YX_PLAY += "			<div class='child color-red no' data-role-id='SXDWD'><p>定位胆</p></div>";
dpcData.plays.YX_PLAY += "		</div>";
dpcData.plays.YX_PLAY += "	</div>";
dpcData.plays.YX_PLAY += "</div>";

//任选
dpcData.plays.RX_PLAY = "";
dpcData.plays.RX_PLAY += "<div class='container clist RX animated'>";
dpcData.plays.RX_PLAY += "	<div class='list-line'>";
dpcData.plays.RX_PLAY += "		<div class='line-title'><p>任选二</p></div>";
dpcData.plays.RX_PLAY += "		<div class='line-content'>";
dpcData.plays.RX_PLAY += "			<div class='child color-red no' data-role-id='RXE'><p>直选复式</p></div>";
dpcData.plays.RX_PLAY += "			<div class='child color-red no' data-role-id='RXEZXDS'><p>直选单式</p></div>";
dpcData.plays.RX_PLAY += "		</div>";
dpcData.plays.RX_PLAY += "	</div>";
dpcData.plays.RX_PLAY += "</div>";

//大小单双
dpcData.plays.DXDS_PLAY = "";
dpcData.plays.DXDS_PLAY += "<div class='container clist DXDS animated'>";
dpcData.plays.DXDS_PLAY += "	<div class='list-line'>";
dpcData.plays.DXDS_PLAY += "		<div class='line-title'><p>直选</p></div>";
dpcData.plays.DXDS_PLAY += "		<div class='line-content'>";
dpcData.plays.DXDS_PLAY += "			<div class='child color-red no' data-role-id='QEDXDS'><p>前二大小单双</p></div>";
dpcData.plays.DXDS_PLAY += "			<div class='child color-red no' data-role-id='HEDXDS'><p>后二大小单双</p></div>";
dpcData.plays.DXDS_PLAY += "		</div>";
dpcData.plays.DXDS_PLAY += "	</div>";
dpcData.plays.DXDS_PLAY += "</div>";


//////////////////玩法描述////////////////////
dpcData.des = {};
//三星
dpcData.des.SXFS_DES, dpcData.des.SXDS_DES, dpcData.des.SXZXHZ_DES, dpcData.des.SXZXHZ_G_DES, dpcData.des.SXZS_DES, dpcData.des.SXZL_DES, dpcData.des.SXYMBDW_DES, dpcData.des.SXEMBDW_DES = "请稍后........";
//三星单式投注样例
dpcData.des.SXDS_SAMPLE = "<p>345353688</p>";


//前二
dpcData.des.QEZXFS_DES, dpcData.des.QEZXDS_DES, dpcData.des.QEZXHZ_DES, dpcData.des.QEZXFS_G_DES, dpcData.des.QEZXDS_G_DES, dpcData.des.QEZXHZ_G_DES = "请稍后........";
//前二直选单式投注样例
dpcData.des.QEZXDS_SAMPLE = "<p>653397</p>";
//前二组选单式投注样例
dpcData.des.QEZXDS_G_SAMPLE = "<p>122356</p>";

//后二
dpcData.des.HEZXFS_DES, dpcData.des.HEZXDS_DES, dpcData.des.HEZXHZ_DES, dpcData.des.HEZXFS_G_DES, dpcData.des.HEZXDS_G_DES, dpcData.des.HEZXHZ_G_DES = "请稍后........";
//后二直选单式投注样例
dpcData.des.HEZXDS_SAMPLE = "<p>112378</p>";
//后二组选单式投注样例
dpcData.des.HEZXDS_G_SAMPLE = "<p>563198</p>";

//前二大小单双 后二大小单双
dpcData.des.QEDXDS_DES, dpcData.des.HEDXDS_DES = "请稍后........";

//一星 定位胆
dpcData.des.SXDWD_DES = "请稍后........";

//任选
dpcData.des.RXY_DES, dpcData.des.RXE_DES, dpcData.des.RXEZXDS_DES  = "请稍后........";
//任选三直选单式样例
dpcData.des.RXEZXDS_SAMPLE = "<p>261845</p>";


//玩法描述映射值
dpcData.lotteryKindMap = new JS_OBJECT_MAP();

dpcData.lotteryKindMap.put("SXFS", "三星-直选复式");
dpcData.lotteryKindMap.put("SXDS", "三星-直选单式");
dpcData.lotteryKindMap.put("SXZXHZ", "三星-直选和值");
dpcData.lotteryKindMap.put("SXZXHZ_G", "三星-组选和值");
dpcData.lotteryKindMap.put("SXZS", "三星-组选三星组三");
dpcData.lotteryKindMap.put("SXZL", "三星-组选三星组六");
dpcData.lotteryKindMap.put("SXYMBDW", "三星-一码不定位");
dpcData.lotteryKindMap.put("SXEMBDW", "三星-二码不定位");

dpcData.lotteryKindMap.put("QEZXFS", "前二-直选复式");
dpcData.lotteryKindMap.put("QEZXDS", "前二-直选单式");
dpcData.lotteryKindMap.put("QEZXHZ", "前二-直选和值");
dpcData.lotteryKindMap.put("QEZXFS_G", "前二-组选复式");
dpcData.lotteryKindMap.put("QEZXDS_G", "前二-组选单式");
dpcData.lotteryKindMap.put("QEZXHZ_G", "前二-组选和值");

dpcData.lotteryKindMap.put("HEZXFS", "后二-直选复式");
dpcData.lotteryKindMap.put("HEZXDS", "后二-直选单式");
dpcData.lotteryKindMap.put("HEZXHZ", "后二-直选和值");
dpcData.lotteryKindMap.put("HEZXFS_G", "后二-组选复式");
dpcData.lotteryKindMap.put("HEZXDS_G", "后二-组选单式");
dpcData.lotteryKindMap.put("HEZXHZ_G", "后二-组选和值");

dpcData.lotteryKindMap.put("SXDWD", "定位胆");

dpcData.lotteryKindMap.put("QEDXDS", "前二-大小单双");
dpcData.lotteryKindMap.put("HEDXDS", "后二-大小单双");


dpcData.lotteryKindMap.put("RXE", "任选二-复式");
dpcData.lotteryKindMap.put("RXEZXDS", "任选二-单式");

/**
 * 加载所有的玩法
 */
DpcData.prototype.loadLotteryKindPlay = function() {
	$("#lotteryKindPlayList").append(dpcData.plays.SX_PLAY);
	$("#lotteryKindPlayList").append(dpcData.plays.QE_PLAY);
	$("#lotteryKindPlayList").append(dpcData.plays.HE_PLAY);
	$("#lotteryKindPlayList").append(dpcData.plays.YX_PLAY);
	$("#lotteryKindPlayList").append(dpcData.plays.RX_PLAY);
	$("#lotteryKindPlayList").append(dpcData.plays.DXDS_PLAY);
}


/**
 * 玩法选择显示描述
 * @param roleId
 */
DpcData.prototype.showPlayDes = function(roleId) {
	var playDes = "";
	  if(roleId == 'SXFS'){
		  $("#playDes").html(dpcData.des.SXFS_DES);
	  }else if(roleId == 'SXDS'){
		  $("#playDes").html(dpcData.des.SXDS_DES);
		  $("#dsModelSample").html(dpcData.des.SXDS_SAMPLE);
	  }else if(roleId == 'SXZXHZ'){
		  $("#playDes").html(dpcData.des.SXZXHZ_DES);
	  }else if(roleId == 'SXZXHZ_G'){
		  $("#playDes").html(dpcData.des.SXZXHZ_G_DES);
	  }else if(roleId == 'SXZS'){
		  $("#playDes").html(dpcData.des.SXZS_DES);
	  }else if(roleId == 'SXZL'){
		  $("#playDes").html(dpcData.des.SXZL_DES);
	  }else if(roleId == 'SXYMBDW'){
		  $("#playDes").html(dpcData.des.SXYMBDW_DES);
	  }else if(roleId == 'SXEMBDW'){
		  $("#playDes").html(dpcData.des.SXEMBDW_DES);
	  }else if(roleId == 'QEZXFS'){
		  $("#playDes").html(dpcData.des.QEZXFS_DES);
	  }else if(roleId == 'QEZXDS'){
		  $("#playDes").html(dpcData.des.QEZXDS_DES);
		  $("#dsModelSample").html(dpcData.des.QEZXDS_SAMPLE);
	  }else if(roleId == 'QEZXHZ'){
		  $("#playDes").html(dpcData.des.QEZXHZ_DES);
	  }else if(roleId == 'QEZXFS_G'){
		  $("#playDes").html(dpcData.des.QEZXFS_G_DES);
	  }else if(roleId == 'QEZXDS_G'){  
		  $("#playDes").html(dpcData.des.QEZXDS_G_DES);
		  $("#dsModelSample").html(dpcData.des.QEZXDS_G_SAMPLE);
	  }else if(roleId == 'QEZXHZ_G'){
		  $("#playDes").html(dpcData.des.QEZXHZ_G_DES);
	  }else if(roleId == 'HEZXFS'){
		  $("#playDes").html(dpcData.des.HEZXFS_DES);
	  }else if(roleId == 'HEZXDS'){
		  $("#playDes").html(dpcData.des.HEZXDS_DES);
		  $("#dsModelSample").html(dpcData.des.HEZXDS_SAMPLE);
	  }else if(roleId == 'HEZXHZ'){
		  $("#playDes").html(dpcData.des.HEZXHZ_DES);
	  }else if(roleId == 'HEZXFS_G'){
		  $("#playDes").html(dpcData.des.HEZXFS_G_DES);
	  }else if(roleId == 'HEZXDS_G'){
		  $("#playDes").html(dpcData.des.HEZXDS_G_DES);
		  $("#dsModelSample").html(dpcData.des.HEZXDS_G_SAMPLE);
	  }else if(roleId == 'HEZXHZ_G'){
		  $("#playDes").html(dpcData.des.HEZXHZ_G_DES);
	  }else if(roleId == 'QEDXDS'){
		  $("#playDes").html(dpcData.des.QEDXDS_DES);
	  }else if(roleId == 'HEDXDS'){
		  $("#playDes").html(dpcData.des.HEDXDS_DES);
	  }else if(roleId == 'SXDWD'){
		  $("#playDes").html(dpcData.des.SXDWD_DES);
	  }else if(roleId == 'RXE'){
		  $("#playDes").html(dpcData.des.RXE_DES);
	  }else if(roleId == 'RXEZXDS'){
		  $("#playDes").html(dpcData.des.RXEZXDS_DES);
		  $("#dsModelSample").html(dpcData.des.RXEZXDS_SAMPLE);
	  }else{
		  showTip("不可知的彩种玩法描述.");
	  }
	  
//	$("#playDes").html(playDes);
};

/**
 * 玩法选择
 * @param roleId
 */
DpcData.prototype.lotteryKindPlayChoose = function(roleId) {
	var desArray = new Array("万位", "千位", "百位", "十位", "个位");
	var emptyDesArray = new Array("");
	if (roleId == 'SXFS') {
		dpcData.ballSectionCommon(2, 4, desArray, null, true);
	} else if (roleId == 'SXDS') {
		dpcData.dsBallSectionCommon(roleId);
	} else if (roleId == 'SXZXHZ') {
		dpcData.ballSectionCommon(0, 0, emptyDesArray, roleId, false);
	} else if (roleId == 'SXZXHZ_G') {
		dpcData.ballSectionCommon(0, 0, emptyDesArray, roleId, false);
	} else if (roleId == 'SXZS') {
		dpcData.ballSectionCommon(0, 0, emptyDesArray, null, false);
	} else if (roleId == 'SXZL') {
		dpcData.ballSectionCommon(0, 0, emptyDesArray, null, false);
	} else if (roleId == 'SXYMBDW') {
		dpcData.ballSectionCommon(0, 0, emptyDesArray, null, false);
	} else if (roleId == 'SXEMBDW') {
		dpcData.ballSectionCommon(0, 0, emptyDesArray, null, false);
	} else if (roleId == 'QEZXFS') {
		dpcData.ballSectionCommon(2, 3, desArray, null, true);
	} else if (roleId == 'QEZXDS') {
		dpcData.dsBallSectionCommon(roleId);
	} else if (roleId == 'QEZXHZ') {
		dpcData.ballSectionCommon(0, 0, emptyDesArray, roleId, false);
	} else if (roleId == 'QEZXFS_G') {
		dpcData.ballSectionCommon(0, 0, emptyDesArray, null, false);
	} else if (roleId == 'QEZXDS_G') {
		dpcData.dsBallSectionCommon(roleId);
	} else if (roleId == 'QEZXHZ_G') {
		dpcData.ballSectionCommon(0, 0, emptyDesArray, roleId, false);
	} else if (roleId == 'HEZXFS') {
		dpcData.ballSectionCommon(3, 4, desArray, null, true);
	} else if (roleId == 'HEZXDS') {
		dpcData.dsBallSectionCommon(roleId);
	} else if (roleId == 'HEZXHZ') {
		dpcData.ballSectionCommon(0, 0, emptyDesArray, roleId, false);
	} else if (roleId == 'HEZXFS_G') {
		dpcData.ballSectionCommon(0, 0, emptyDesArray, null, false);
	} else if (roleId == 'HEZXDS_G') {
		dpcData.dsBallSectionCommon(roleId);
	} else if (roleId == 'HEZXHZ_G') {
		dpcData.ballSectionCommon(0, 0, emptyDesArray, roleId, false);
	} else if (roleId == 'QEDXDS') {
		desArray = new Array("百位", "十位");
		dpcData.ballSectionCommon(0, -1, desArray, roleId, false);
	} else if (roleId == 'HEDXDS') {
		desArray = new Array("十位", "个位");
		dpcData.ballSectionCommon(0, -1, desArray, roleId, false);
	} else if (roleId == 'SXDWD') {
		dpcData.ballSectionCommon(2, 4, desArray, null, true);
	} else if (roleId == 'RXE') {
		dpcData.ballSectionCommon(2, 4, desArray, null, false);
	} else if (roleId == 'RXEZXDS') { // 任选二直选单式
		dpcData.dsBallSectionCommon(roleId);
	} else {
		alert("不可知的彩种玩法.");
	}
	
	//存储当前切换的玩法模式
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey = roleId;
	
	//显示玩法名称
	var currentKindName = dpcData.lotteryKindMap.get(roleId);
    $("#currentKindName").text(currentKindName);

	//显示玩法说明
	dpcData.showPlayDes(roleId);
	
	//拼接完，进行事件的重新申明
	lotteryCommonPage.dealForEvent();
	//获取当前的玩法并赋值赔率  斗牛
	var kindKey=lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey;
	var kindNameType=lotteryCommonPage.currentLotteryKindInstance.param.kindNameType;
	var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
	if(kindKey=="ZHWQLHSH"||kindKey=="ZHDN"){
		$("p[id^="+kindNameType+"_"+kindKey+"]").each(function(i,idx){
			var currentLotteryWin=lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get($(idx).attr("id"));
			var pl=parseFloat((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode)/2).toFixed(3);
			pl=pl.substring(0,pl.length-1);
			$(this).html("赔率："+pl);
		});
	}else if(kindKey=="ZHQS"||kindKey=="ZHZS"||kindKey=="ZHHS"){
		/*$("span[id^=SSC_"+kindKey+"]").each(function(i,idx){
			var currentLotteryWin=lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get($(idx).attr("id"));
			$(this).html("赔率："+currentLotteryWin.winMoney);
		});*/
		$("p[id^="+kindNameType+"_"+kindKey+"]").each(function(i,idx){
			var currentLotteryWin=lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get($(idx).attr("id"));
			var pl=parseFloat((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode)/2).toFixed(3);
			pl=pl.substring(0,pl.length-1);
			$(this).html("赔率："+pl);
		});
	}
};





/**
 * 复式玩法的号码拼接
 * desArray 描述的数组
 * specialKind 特殊玩法
 * isOmitColdDataDeal 是否遗漏和冷热数据的处理
 */
DpcData.prototype.ballSectionCommon = function(numCountStart,numCountEnd,desArray,specialKind,isOmitColdDataDeal){
	var content = "";
	
	if(specialKind == null){
		
		//大小奇偶按钮字符串
		var dxjoStr='		<div class="content">'+
					'			<div class="mun" data-role-type="big">大</div>'+
					'			<div class="mun" data-role-type="small">小</div>'+
					'			<div class="mun" data-role-type="odd">单</div>'+
					'			<div class="mun" data-role-type="even">双</div>'+
					'			<div class="mun" data-role-type="all">全</div>'+
					'			<div class="mun" data-role-type="empty">清</div>'+
					'		</div>';
		
		for(var i = numCountStart;i <= numCountEnd; i++){
			content += '<div class="lottery-child main"><div class="container">';
			var numDes = desArray[i];
			if(isNotEmpty(numDes)) {
				content += '<div class="child-head">';
				content += '	<div class="title"><p>' + numDes + '</p></div>';
				//大小单双
				content += dxjoStr;
				//遗漏冷热元素处理
				if(isOmitColdDataDeal) {
					content += "";
				}
				content += '</div>';
			}
			content += '<div class="child-content">';
			//只有单行的情况 号码排列居中
			if(numCountStart == numCountEnd) {
				content += '	<div class="content long2">';
			} else {
				content += '	<div class="content">';
			}
			
			//0 - 9的号码
			for(var j = 0; j < dpcData.common_code_array.length; j++){
				content += '<div class="munbtn mun" data-realvalue="'+ dpcData.common_code_array[j] +'" name="ballNum_'+i+'_match">'+ dpcData.common_code_array[j] +'</div>';
				//遗漏冷热元素处理
				if(isOmitColdDataDeal) {
					content += "";
				}
			}
			content += '	</div>';
			content += '</div>';
			content += '</div>';
			content += '</div>';
		}
	}else{ 
		var hz_code_array = null;
		if(specialKind == "SXZXHZ"){
			//0 - 27的号码
			hz_code_array = dpcData.qszxhz_code_array;
		}else if(specialKind == "SXZXHZ_G"){
			//1 - 26的号码
			hz_code_array = dpcData.qszxhz_g_code_array;
		}else if(specialKind == "QEZXHZ" || specialKind == "HEZXHZ"){
			//0 - 18的号码
			hz_code_array = dpcData.qezxhz_code_array;
		}else if(specialKind == "QEZXHZ_G" || specialKind == "HEZXHZ_G"){
			//1 - 17的号码
			hz_code_array = dpcData.qezxhz_g_code_array
		}
		
		if(hz_code_array != null) {
			content += '<div class="lottery-child main"><div class="container">';
			var numDes = desArray[i];
			if(isNotEmpty(numDes)) {
				content += '<div class="child-head">';
				content += '	<div class="title"><p>' + numDes + '</p></div>';
				//遗漏冷热元素处理
				if(isOmitColdDataDeal) {
					content += "";
				}
				content += '</div>';
			}
			content += '<div class="child-content">';
			content += '	<div class="content long2">';
			for(var j = 0; j < hz_code_array.length; j++){
				content += '<div class="munbtn mun" data-realvalue="'+ hz_code_array[j] +'" name="ballNum_'+i+'_match">'+ hz_code_array[j] +'</div>';
				//遗漏冷热元素处理
				if(isOmitColdDataDeal) {
					content += "";
				}
			}
			content += '	</div>';
			content += '</div>';
			content += '</div>';
			content += '</div>';
		} else if(specialKind == "QEDXDS" || specialKind == "HEDXDS"){
			for(var i = 0; i < desArray.length; i++){
				content += '<div class="lottery-child no main"><div class="container">';
				var numDes = desArray[i];
				content += '<div class="child-content">';
				if(isNotEmpty(numDes)) {
					content += '<div class="title"><p>' + numDes + '</p></div>';
					//遗漏冷热元素处理
					if(isOmitColdDataDeal) {
						content += "";
					}
				}
				content += '	<div class="content">';
				//1 - 4的号码
				for(var j = 0; j < dpcData.dxds_code_array.length; j++){
					content += '<div class="munbtn mun2" data-realvalue="'+ dpcData.dxds_code_array[j] +'" name="ballNum_'+i+'_match">';
					content += '<h2 class="value">';
					if(dpcData.dxds_code_array[j] == 1){
						content += "大";
					}else if(dpcData.dxds_code_array[j] == 2){
						content += "小";
					}else if(dpcData.dxds_code_array[j] == 3){
						content += "单";
					}else if(dpcData.dxds_code_array[j] == 4){
						content += "双";
					}
					content += '</h2>';
					//遗漏冷热元素处理
					if(isOmitColdDataDeal) {
						content += "";
					}
					content += '</div>';
				}
				content += '	</div>';
				content += '</div>';
				content += '</div>';
				content += '</div>';
			}
		}else{
			showTip("未找到该玩法的号码.");
		}
	}
    $("#ballSection").html(content);
    //任选框隐藏
    $("#rxWeiCheck").hide();
    //隐藏单式的导入框
    $("#importDsBtn").hide();
    
    //进行遗漏冷热的数值处理
    if(isOmitColdDataDeal){
    	$("#ommitHotColdBtn").show();
    } else {
    	$("#ommitHotColdBtn").hide();
    }
};

/**
 * 单式玩法的号码拼接
 * specialKind 特殊玩法
 */
DpcData.prototype.dsBallSectionCommon = function(specialKind){
	
	var editorStr = "说明：\n";
	editorStr += "1、请参照标准格式样本格式录入或上传方案。\n";
	editorStr += "2、每一注号码之间的间隔符支持 回车 空格[ ] 逗号[,] 分号[;] 冒号[:] 竖线 [|]。\n";
	editorStr += "3、文件格式必须是.txt格式。\n";
	editorStr += "4、文件较大时会导致上传时间较长，请耐心等待！\n";
	editorStr += "5、将文件拖入文本框即可快速实现文件上传功能。\n";
	editorStr += "6、导入文本内容后将覆盖文本框中现有的内容。";

	var rxContent = '<div class="lottery-select">'+
	'<div class="container lottery-select-list">'+
	'	<label for="">'+
	'		<input type="checkbox" name="ds_position" onclick="lotteryCommonPage.lottertyDataDeal.dsPositionCheckFunc(this);" data-position="1" checked="checked">'+
	'		<span>万位</span>'+
	'	</label>'+
	'	<label for="">'+
	'		<input type="checkbox" name="ds_position" onclick="lotteryCommonPage.lottertyDataDeal.dsPositionCheckFunc(this);" data-position="2" checked="checked">'+
	'		<span>千位</span>'+
	'	</label>'+
	'	<label for="">'+
	'		<input type="checkbox" name="ds_position" onclick="lotteryCommonPage.lottertyDataDeal.dsPositionCheckFunc(this);" data-position="3" checked="checked">'+
	'		<span>百位</span>'+
	'	</label>'+
	'	<label for="">'+
	'		<input type="checkbox" name="ds_position" onclick="lotteryCommonPage.lottertyDataDeal.dsPositionCheckFunc(this);" data-position="4" checked="checked">'+
	'		<span>十位</span>'+
	'	</label>'+
	'	<label for="">'+
	'		<input type="checkbox" name="ds_position" onclick="lotteryCommonPage.lottertyDataDeal.dsPositionCheckFunc(this);" data-position="5" checked="checked">'+
	'		<span>个位</span>'+
	'	</label>'+
	'</div>'+
	'<div class="container lottery-select-msg" >'+
	'	<p>温馨提示：您选择了<span class="font-yellow" id="pnum">5</span>个位置，系统自动根据位置合成<span class="font-yellow" id="fnum">10</span>个方案</p>'+
	'</div>'+
	'</div>';
	
	var content = '	<div class="lottery-child main"><div class="container">'+
	'		<div class="lottery-textarea">'+
	'			<textarea id="lr_editor" placeholder="'+ editorStr +'"></textarea>'+
	'		</div>'+
	'	</div></div>';
	
    //任选玩法的话
    if(specialKind == "RXSIZXDS" || specialKind == "RXEZXDS" || specialKind == "RXSZXDS" ) {
    	//拼接加上任选的内容
    	content =  rxContent + content;
    	
    	dpcData.rxParam.isRxDsAllow = true;
    	//全选
    	$("input[name='ds_position']").each(function(){
    		this.checked = true;
    	});
    	if(specialKind == "RXSIZXDS"){
    		dpcData.rxParam.dsAllowCount = 5;
    		$("#fnum").text(5);
    	}else if(specialKind == "RXEZXDS" || specialKind == "RXSZXDS"){
    		dpcData.rxParam.dsAllowCount = 10;
    		$("#fnum").text(10); 
    	}
    }
    $("#ballSection").html(content);
    
    //添加单式的事件处理
    lotteryCommonPage.addDsEvent();
}

DpcData.prototype.zhBallSectionCommon=function(specialKind){//斗牛
	var content="";
	var kindNameType=lotteryCommonPage.currentLotteryKindInstance.param.kindNameType;
	if(specialKind=="ZHWQLHSH"){
		content='<div class="lottery-content animated" style="animation-name: fadeInUp; animation-delay: 0s;">'+
			'<div class="lottery-child no main wqzh">'+
				'<div class="container">'+
					'<div class="child-content">'+
						'<div class="title"><span>五球总和</span></div>'+
						'<div class="content">'+
							'<div class="munbtn mun2" data-realvalue="1" name="ballNum_0_match">'+
								'<p class="range">23-45</p>'+
								'<h2 class="value">大</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHWQLHSH">赔率：加载中...</p>'+
							'</div>'+
							'<div class="munbtn mun2" data-realvalue="2" name="ballNum_1_match">'+
								'<p class="range">0-22</p>'+
								'<h2 class="value">小</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHWQLHSH_2">赔率：加载中...</p>'+
							'</div>'+
							'<div class="munbtn mun2" data-realvalue="3" name="ballNum_2_match">'+
								'<h2 class="value">单</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHWQLHSH_3">赔率：加载中...</p>'+
							'</div>'+
							'<div class="munbtn mun2" data-realvalue="4" name="ballNum_1_match">'+
								'<h2 class="value">双</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHWQLHSH_4">赔率：加载中...</p>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>'+
			'<div class="lottery-content animated" style="animation-name: fadeInUp; animation-delay: 0s;">'+
				'<div class="lottery-child no main wqzh">'+
					'<div class="container">'+
						'<div class="child-content">'+
							'<div class="title"><span>梭哈</span></div>'+
							'<div class="content">'+
								'<div class="munbtn mun2" data-realvalue="5" name="ballNum_0_match">'+
									'<h2 class="value">五条</h2>'+
									'<p class="info" id="'+kindNameType+'_ZHWQLHSH_5">赔率：加载中...</p>'+
								'</div>'+
								'<div class="munbtn mun2" data-realvalue="6" name="ballNum_1_match">'+
									'<h2 class="value">顺子</h2>'+
									'<p class="info" id="'+kindNameType+'_ZHWQLHSH_6">赔率：加载中...</p>'+
								'</div>'+
								'<div class="munbtn mun2" data-realvalue="7" name="ballNum_2_match">'+
									'<h2 class="value">对子</h2>'+
									'<p class="info" id="'+kindNameType+'_ZHWQLHSH_7">赔率：加载中...</p>'+
								'</div>'+
								'<div class="munbtn mun2" data-realvalue="8" name="ballNum_1_match">'+
									'<h2 class="value">半顺</h2>'+
									'<p class="info" id="'+kindNameType+'_ZHWQLHSH_8">赔率：加载中...</p>'+
								'</div>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>'+
				'<div class="lottery-child no main">'+
					'<div class="container">'+
						'<div class="child-content">'+
							'<div class="content">'+
								'<div class="munbtn mun2" data-realvalue="9" name="ballNum_0_match">'+
									'<h2 class="value">三条</h2>'+
									'<p class="info" id="'+kindNameType+'_ZHWQLHSH_9">赔率：加载中...</p>'+
								'</div>'+
								'<div class="munbtn mun2" data-realvalue="10" name="ballNum_1_match">'+
									'<h2 class="value">两对</h2>'+
									'<p class="info" id="'+kindNameType+'_ZHWQLHSH_10">赔率：加载中...</p>'+
								'</div>'+
								'<div class="munbtn mun2" data-realvalue="11" name="ballNum_2_match">'+
									'<h2 class="value">一对</h2>'+
									'<p class="info" id="'+kindNameType+'_ZHWQLHSH_11">赔率：加载中...</p>'+
								'</div>	'+
								'<div class="munbtn mun2" data-realvalue="12" name="ballNum_1_match">'+
									'<h2 class="value">散号</h2>'+
									'<p class="info" id="'+kindNameType+'_ZHWQLHSH_12">赔率：加载中...</p>'+
								'</div>'+
							'</div>'+		
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>'+
			'<div class="lottery-child no main wqzh">'+
				'<div class="container">'+
					'<div class="child-content">'+
						'<div class="title"><span>龙虎</span></div>'	+
						'<div class="content">'+
							'<div class="munbtn mun2" data-realvalue="13" name="ballNum_0_match">'+
								'<p class="range">万位数</p>'+
								'<h2 class="value">龙</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHWQLHSH_13">赔率：加载中...</p>'+
							'</div>'+
							'<div class="munbtn mun2" data-realvalue="14" name="ballNum_1_match">'+
								'<p class="range">个位数</p>'+
								'<h2 class="value">虎</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHWQLHSH_14">赔率：加载中...</p>'+
							'</div>'+
							'<div class="munbtn mun2" data-realvalue="15" name="ballNum_2_match">'+
								'<h2 class="value">和</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHWQLHSH_15">赔率：加载中...</p>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>'+
		'</div>';
	}else if(specialKind=="ZHDN"){
		content='<div class="lottery-child no main">'+
					'<div class="container">'+
						'<div class="child-content" style="text-align:center;">'+
							'<div class="munbtn mun2" data-realvalue="1" name="ballNum_0_match">'+
								'<h2 class="value">牛1</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHDN">赔率：加载中...</p>'+
							'</div>'+
							'<div class="munbtn mun2" data-realvalue="2" name="ballNum_1_match">'+
								'<h2 class="value">牛2</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHDN_2">赔率：加载中...</p>'+
							'</div>'+
							'<div class="munbtn mun2" data-realvalue="3" name="ballNum_2_match">'+
								'<h2 class="value">牛3</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHDN_3">赔率：加载中...</p>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>'+
				'<div class="lottery-child no main">'+
					'<div class="container">'+
						'<div class="child-content" style="text-align:center;">'+
							'<div class="munbtn mun2" data-realvalue="4" name="ballNum_3_match">'+
								'<h2 class="value">牛4</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHDN_4">赔率：加载中...</p>'+
							'</div>'+
							'<div class="munbtn mun2" data-realvalue="5" name="ballNum_4_match">'+
								'<h2 class="value">牛5</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHDN_5">赔率：加载中...</p>'+
							'</div>'+
							'<div class="munbtn mun2" data-realvalue="6" name="ballNum_5_match">'+
								'<h2 class="value">牛6</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHDN_6">赔率：加载中...</p>'+
							'</div>	'+
						'</div>'+
					'</div>'+
				'</div>'+
				'<div class="lottery-child no main">'+
					'<div class="container">'+
						'<div class="child-content" style="text-align:center;">'+
							'<div class="munbtn mun2" data-realvalue="7" name="ballNum_3_match">'+
								'<h2 class="value">牛7</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHDN_7">赔率：加载中...</p>'+
							'</div>'+
							'<div class="munbtn mun2" data-realvalue="8" name="ballNum_4_match">'+
								'<h2 class="value">牛8</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHDN_8">赔率：加载中...</p>'+
							'</div>'+
							'<div class="munbtn mun2" data-realvalue="9" name="ballNum_5_match">'+
								'<h2 class="value">牛9</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHDN_9">赔率：加载中...</p>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>'+
				'<div class="lottery-child no main">'+
					'<div class="container">'+
						'<div class="child-content" style="text-align:center;">'+
							'<div class="munbtn mun2" data-realvalue="10" name="ballNum_3_match">'+
								'<h2 class="value">牛牛</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHDN_10">赔率：加载中...</p>'+
							'</div>'+
							'<div class="munbtn mun2" data-realvalue="11" name="ballNum_4_match">'+
								'<h2 class="value">牛大</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHDN_11">赔率：加载中...</p>'+
							'</div>'+
							'<div class="munbtn mun2" data-realvalue="12" name="ballNum_5_match">'+
								'<h2 class="value">牛小</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHDN_12">赔率：加载中...</p>'+
							'</div>	'+
						'</div>'+
					'</div>'+
				'</div>'+
				'<div class="lottery-child no main">'+
					'<div class="container">'+
						'<div class="child-content" style="text-align:center;">'+
							'<div class="munbtn mun2" data-realvalue="13" name="ballNum_3_match">'+
								'<h2 class="value">牛单</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHDN_13">赔率：加载中...</p>'+
							'</div>'+
							'<div class="munbtn mun2" data-realvalue="14" name="ballNum_4_match">'+
								'<h2 class="value">牛双</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHDN_14">赔率：加载中...</p>'+
							'</div>'+
							'<div class="munbtn mun2" data-realvalue="15" name="ballNum_5_match">'+
								'<h2 class="value">无牛</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHDN_15">赔率：加载中...</p>'+
							'</div>	'+
						'</div>'+
					'</div>'+
				'</div>';
	}else if(specialKind=="ZHQS"||specialKind=="ZHZS"||specialKind=="ZHHS"){
		content='<div class="lottery-child main qszh">'+
			'<div class="container">'+
				'<div class="child-content">'+
					'<div class="content">'+
						'<div class="munbtn mun" data-realvalue="0" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'">0</div>'+
						'<div class="munbtn mun" data-realvalue="1" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_2">1</div>'+
						'<div class="munbtn mun" data-realvalue="2" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_3">2</div>'+
						'<div class="munbtn mun" data-realvalue="3" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_4">3</div>'+
						'<div class="munbtn mun" data-realvalue="4" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_5">4</div>'+
						'<div class="munbtn mun" data-realvalue="5" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_6">5</div>'+
						'<div class="munbtn mun" data-realvalue="6" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_7">6</div>'+
						'<div class="munbtn mun" data-realvalue="7" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_8">7</div>'+
						'<div class="munbtn mun" data-realvalue="8" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_9">8</div>'+
						'<div class="munbtn mun" data-realvalue="9" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_10">9</div>'+
						'<div class="munbtn mun" data-realvalue="10" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_11">10</div>'+
						'<div class="munbtn mun" data-realvalue="11" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_12">11</div>'+
						'<div class="munbtn mun" data-realvalue="12" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_13">12</div>'+
						'<div class="munbtn mun" data-realvalue="13" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_14">13</div>'+
						'<div class="munbtn mun" data-realvalue="14" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_15">14</div>'+
						'<div class="munbtn mun" data-realvalue="15" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_16">15</div>'+
						'<div class="munbtn mun" data-realvalue="16" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_17">16</div>'+
						'<div class="munbtn mun" data-realvalue="17" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_18">17</div>'+
						'<div class="munbtn mun" data-realvalue="18" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_19">18</div>'+
						'<div class="munbtn mun" data-realvalue="19" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_20">19</div>'+
						'<div class="munbtn mun" data-realvalue="20" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_21">20</div>'+
						'<div class="munbtn mun" data-realvalue="21" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_22">21</div>'+
						'<div class="munbtn mun" data-realvalue="22" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_23">22</div>'+
						'<div class="munbtn mun" data-realvalue="23" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_24">23</div>'+
						'<div class="munbtn mun" data-realvalue="24" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_25">24</div>'+
						'<div class="munbtn mun" data-realvalue="25" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_26">25</div>'+
						'<div class="munbtn mun" data-realvalue="26" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_27">26</div>'+
						'<div class="munbtn mun" data-realvalue="27" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_28">27</div>'+
					'</div>'+
				'</div>'+
			'</div>'+
		'</div>'+
		'<div class="lottery-child main qszh m20-18">'+
			'<div class="container">'+
				'<div class="child-content">'+
					'<div class="munbtn mun2 ml35" data-realvalue="28" name="ballNum_0_match">'+
						'<p class="range">23-45</p>'+
						'<h2 class="value">大</h2>'+
						'<p class="info" id="'+kindNameType+'_'+specialKind+'_29">赔率：加载中...</p>'+
					'</div>'+
					'<div class="munbtn mun2" data-realvalue="29" name="ballNum_1_match">'+
						'<p class="range">0-22</p>'+
						'<h2 class="value">小</h2>'+
						'<p class="info" id="'+kindNameType+'_'+specialKind+'_30">赔率：加载中...</p>'+
					'</div>'+
					'<div class="munbtn mun2" data-realvalue="30" name="ballNum_2_match">'+
						'<h2 class="value">单</h2>'+
						'<p class="info" id="'+kindNameType+'_'+specialKind+'_31">赔率：加载中...</p>'+
					'</div>	'+
					'<div class="munbtn mun2" data-realvalue="31" name="ballNum_1_match">'+
						'<h2 class="value">双</h2>'+
						'<p class="info" id="'+kindNameType+'_'+specialKind+'_32">赔率：加载中...</p>'+
					'</div>'+
				'</div>'+
			'</div>'+
		'</div>'+
		'<div class="lottery-child main qszh m20-18">'+
			'<div class="container">'+
				'<div class="child-content">'+
					'<div class="munbtn mun2 ml35" data-realvalue="32" name="ballNum_0_match">'+
						'<h2 class="value">0-3</h2>'+
						'<p class="info" id="'+kindNameType+'_'+specialKind+'_33">赔率：加载中...</p>'+
					'</div>'+
					'<div class="munbtn mun2" data-realvalue="33" name="ballNum_1_match">'+
						'<h2 class="value">4-7</h2>'+
						'<p class="info" id="'+kindNameType+'_'+specialKind+'_34">赔率：加载中...</p>'+
					'</div>'+
					'<div class="munbtn mun2" data-realvalue="34" name="ballNum_2_match">'+
						'<h2 class="value">8-11</h2>'+
						'<p class="info" id="'+kindNameType+'_'+specialKind+'_35">赔率：加载中...</p>'+
					'</div>	'+
					'<div class="munbtn mun2" data-realvalue="35" name="ballNum_1_match">'+
						'<h2 class="value">12-15</h2>'+
						'<p class="info" id="'+kindNameType+'_'+specialKind+'_36">赔率：加载中...</p>'+
					'</div>'+
					'<div class="munbtn mun2" data-realvalue="36" name="ballNum_0_match">'+
						'<h2 class="value">16-19</h2>'+
						'<p class="info" id="'+kindNameType+'_'+specialKind+'_37">赔率：加载中...</p>'+
					'</div>'+
					'<div class="munbtn mun2 ml35" data-realvalue="37" name="ballNum_1_match">'+
						'<h2 class="value">20-23</h2>'+
						'<p class="info" id="'+kindNameType+'_'+specialKind+'_38">赔率：加载中...</p>'+
					'</div>'+
					'<div class="munbtn mun2" data-realvalue="38" name="ballNum_2_match">'+
						'<h2 class="value">24-27</h2>'+
						'<p class="info" id="'+kindNameType+'_'+specialKind+'_39">赔率：加载中...</p>'+
					'</div>'+
				'</div>'+
			'</div>'+
		'</div>'+
		'<div class="lottery-child main qszh m20-18">'+
			'<div class="container">'+
				'<div class="child-content">'+
					'<div class="munbtn mun2 ml35" data-realvalue="39" name="ballNum_0_match">'+
						'<h2 class="value">豹子</h2>'+
						'<p class="info" id="'+kindNameType+'_'+specialKind+'_40">赔率：加载中...</p>'+
					'</div>'+
					'<div class="munbtn mun2" data-realvalue="40" name="ballNum_1_match">'+
						'<h2 class="value">顺子</h2>'+
						'<p class="info" id="'+kindNameType+'_'+specialKind+'_41">赔率：加载中...</p>'+
					'</div>'+
					'<div class="munbtn mun2" data-realvalue="41" name="ballNum_2_match">'+
						'<h2 class="value">对子</h2>'+
						'<p class="info" id="'+kindNameType+'_'+specialKind+'_42">赔率：加载中...</p>'+
					'</div>	'+
					'<div class="munbtn mun2" data-realvalue="42" name="ballNum_1_match">'+
						'<h2 class="value">半顺</h2>'+
						'<p class="info" id="'+kindNameType+'_'+specialKind+'_43">赔率：加载中...</p>'+
					'</div>'+
					'<div class="munbtn mun2" data-realvalue="43" name="ballNum_1_match">'+
						'<h2 class="value">杂六</h2>'+
						'<p class="info" id="'+kindNameType+'_'+specialKind+'_44">赔率：加载中...</p>'+
					'</div>'+
				'</div>'+
			'</div>'+
		'</div>';
	}
	
	$("#ballSection").html(content);
}

/**
 * 计算当前用户最高模式下的奖金
 */
DpcData.prototype.calucateCurrentUserHighestWin = function(kindKey){
	if(lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.size() == 0){
		return "奖金未完全获取,请再次操作或者刷新页面";
	}
	var currentLotteryWin;
	currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get(kindKey);
	if(currentLotteryWin == null){
		return "未配置该玩法的奖金.";
	}
	var result = (currentLotteryWin.winMoney + (currentUserSscModel - sscLowestAwardModel) * currentLotteryWin.specCode) * 1;	
    return result;
};

/**
 * 获取当前用户最高模式下的奖金
 */
DpcData.prototype.getKindPlayLotteryWin = function(kindKey){
	if(lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.size() == 0){
		return "奖金未完全获取,请稍作等待或者再次操作.";
	}
	var currentLotteryWin;
	currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get(lotteryCommonPage.currentLotteryKindInstance.param.kindNameType+ "_" +kindKey);
	if(currentLotteryWin == null){
		return "未配置该玩法的奖金.";
	}
	var result = (currentLotteryWin.winMoney + (currentUserSscModel - sscLowestAwardModel) * currentLotteryWin.specCode) * 1;	
    return result.toFixed(lotteryCommonPage.param.toFixedValue);
};

/**
 * 设置当前用户最高模式下的奖金和玩法描述
 */
DpcData.prototype.setKindPlayDes = function(){	//三星复式
	var SXFS_WIN = dpcData.getKindPlayLotteryWin("SXFS");
	dpcData.des.SXFS_DES = "<div class='title'>每位至少选择一个号码，号码和位置都对应即中奖，最高奖金\""+ SXFS_WIN +"\"元";
	dpcData.des.SXFS_DES += "<span>，选号示例：</span>投注：678开奖：678奖金：\""+ SXFS_WIN +"\"元";

	//三星单式
	var SXDS_WIN = dpcData.getKindPlayLotteryWin("SXDS");
	dpcData.des.SXDS_DES = "<div class='title'>每位至少选择一个号码，号码和位置都对应即中奖，最高奖金\""+ SXDS_WIN +"\"元";
	dpcData.des.SXDS_DES += "<span>，选号示例：</span>投注：345开奖：345奖金：\""+ SXDS_WIN +"\"元";

	//三星直选和值
	var SXZXHZ_WIN = dpcData.getKindPlayLotteryWin("SXZXHZ");
	dpcData.des.SXZXHZ_DES = "<div class='title'>至少选择一个和值，竞猜开奖号码三位数字之和，最高奖金\""+ SXZXHZ_WIN +"\"元";
	dpcData.des.SXZXHZ_DES += "<span>，选号示例：</span>投注：和值1开奖：**1,*1*,1**奖金：\""+ SXZXHZ_WIN +"\"元";

	//三星组选和值
	var SXZXHZ_G_WIN = dpcData.getKindPlayLotteryWin("SXZXHZ_G");
	var SXZXHZ_G_2_WIN = dpcData.getKindPlayLotteryWin("SXZXHZ_G_2");
	dpcData.des.SXZXHZ_G_DES = "<div class='title'>至少选择一个和值，竞猜开奖号码三位数字之和(不含豹子号)，最高奖金\""+ SXZXHZ_G_WIN +"\"元";
	dpcData.des.SXZXHZ_G_DES += "<span>，选号示例：</span>投注：和值6开奖：(1)015(不限顺序),奖金：\""+ SXZXHZ_G_2_WIN +"\"元(2)033(不限顺序),奖金：\""+ SXZXHZ_G_WIN +"\"元";

	//三星组三
	var SXZS_WIN = dpcData.getKindPlayLotteryWin("SXZS");
	dpcData.des.SXZS_DES = "<div class='title'>从0-9中选择2个数字组成两注，所选号码与开奖号码的三位相同，顺序不限，最高奖金\""+ SXZS_WIN +"\"元";
	dpcData.des.SXZS_DES += "<span>，选号示例：</span>投注：113开奖：113(不限顺序)奖金：\""+ SXZS_WIN +"\"元";

	//前三组六
	var SXZL_WIN = dpcData.getKindPlayLotteryWin("SXZL");
	dpcData.des.SXZL_DES = "<div class='title'>从0-9中任意选择3个号码组成一注，所选号码与开奖号码的三位相同，顺序不限，最高奖金\""+ SXZL_WIN +"\"元";
	dpcData.des.SXZL_DES += "<span>，选号示例：</span>投注：123开奖：123(不限顺序)奖金：\""+ SXZL_WIN +"\"元";

	//三星一码不定位
	var SXYMBDW_WIN = dpcData.getKindPlayLotteryWin("SXYMBDW");
	dpcData.des.SXYMBDW_DES = "<div class='title'> 从0-9中至少选择1个号码投注，竞猜开奖号码三位中包含这个号码，包含即中奖，最高奖金\""+ SXYMBDW_WIN +"\"元";
	dpcData.des.SXYMBDW_DES += "<span>，选号示例：</span>投注：1开奖：1xx(不限顺序)奖金：\""+ SXYMBDW_WIN +"\"元";

	//三星二码不定位
	var SXEMBDW_WIN = dpcData.getKindPlayLotteryWin("SXEMBDW");
	dpcData.des.SXEMBDW_DES = "<div class='title'> 从0-9中至少选择2个号码投注，竞猜开奖号码三位中包含这2个号码，包含即中奖，最高奖金\""+ SXEMBDW_WIN +"\"元";
	dpcData.des.SXEMBDW_DES += "<span>，选号示例：</span>投注：1,2开奖：12x(不限顺序)奖金：\""+ SXEMBDW_WIN +"\"元";

	//前二直选复式
	var QEZXFS_WIN = dpcData.getKindPlayLotteryWin("QEZXFS");	
	dpcData.des.QEZXFS_DES = "<div class='title'> 每位至少选择一个号码，竞猜开奖号码的前二位，号码和位置都对应即中奖，最高奖金\""+ QEZXFS_WIN +"\"元";
	dpcData.des.QEZXFS_DES += "<span>，选号示例：</span>投注：56*开奖：56*奖金：\""+ QEZXFS_WIN +"\"元";

	//前二直选单式
	var QEZXDS_WIN = dpcData.getKindPlayLotteryWin("QEZXDS");
	dpcData.des.QEZXDS_DES = "<div class='title'> 每位至少选择一个号码，竞猜开奖号码的前二位，号码和位置都对应即中奖，最高奖金\""+ QEZXDS_WIN +"\"元";
	dpcData.des.QEZXDS_DES += "<span>，选号示例：</span>投注：12*开奖：12*奖金：\""+ QEZXDS_WIN +"\"元";

	//前二直选和值
	var QEZXHZ_WIN = dpcData.getKindPlayLotteryWin("QEZXHZ");
	dpcData.des.QEZXHZ_DES = "<div class='title'> 至少选择一个和值，竞猜开奖号码前二位数字之和，最高奖金\""+ QEZXHZ_WIN +"\"元";
	dpcData.des.QEZXHZ_DES += "<span>，选号示例：</span>投注：和值1开奖：01*,10*奖金：\""+ QEZXHZ_WIN +"\"元";

	//前二组选复式
	var QEZXFS_G_WIN = dpcData.getKindPlayLotteryWin("QEZXFS_G");
	dpcData.des.QEZXFS_G_DES = "<div class='title'> 从0-9中选择2个数字组成一注，所选号码与开奖号码的前二位相同，顺序不限，最高奖金\""+ QEZXFS_G_WIN +"\"元";
	dpcData.des.QEZXFS_G_DES += "<span>，选号示例：</span>投注：1,2开奖：12*(不限顺序)奖金：\""+ QEZXFS_G_WIN +"\"元";

	//前二组选单式
	var QEZXDS_G_WIN = dpcData.getKindPlayLotteryWin("QEZXDS_G");
	dpcData.des.QEZXDS_G_DES = "<div class='title'> 从0-9中选择2个数字组成一注，所选号码与开奖号码的前二位相同，顺序不限，最高奖金\""+ QEZXDS_G_WIN +"\"元";
	dpcData.des.QEZXDS_G_DES += "<span>，选号示例：</span>投注：5,6开奖：56*(不限顺序)奖金：\""+ QEZXDS_G_WIN +"\"元";

	//前二组选和值
	var QEZXHZ_G_WIN = dpcData.getKindPlayLotteryWin("QEZXHZ_G");
	dpcData.des.QEZXHZ_G_DES = "<div class='title'> 所选数值等于开奖号码的前二位数字相加之和（不含对子），最高奖金\""+ QEZXHZ_G_WIN +"\"元";
	dpcData.des.QEZXHZ_G_DES += "<span>，选号示例：</span>投注：和值1开奖：10*(不限顺序)奖金：\""+ QEZXHZ_G_WIN +"\"元";

	//后二直选复式
	var HEZXFS_WIN = dpcData.getKindPlayLotteryWin("HEZXFS");
	dpcData.des.HEZXFS_DES = "<div class='title'> 每位至少选择一个号码，竞猜开奖号码的后二位，号码和位置都对应即中奖，最高奖金\""+ HEZXFS_WIN +"\"元";
	dpcData.des.HEZXFS_DES += "<span>，选号示例：</span>投注：*56开奖：*56奖金：\""+ HEZXFS_WIN +"\"元";

	//后二直选单式
	var HEZXDS_WIN = dpcData.getKindPlayLotteryWin("HEZXDS");
	dpcData.des.HEZXDS_DES = "<div class='title'> 每位至少选择一个号码，竞猜开奖号码的后二位，号码和位置都对应即中奖，最高奖金\""+ HEZXDS_WIN +"\"元";
	dpcData.des.HEZXDS_DES += "<span>，选号示例：</span>投注：*12开奖：*12奖金：\""+ HEZXDS_WIN +"\"元";

	//后二直选和值
	var HEZXHZ_WIN = dpcData.getKindPlayLotteryWin("HEZXHZ");
	dpcData.des.HEZXHZ_DES = "<div class='title'> 至少选择一个和值，竞猜开奖号码后二位数字之和，最高奖金\""+ HEZXHZ_WIN +"\"元";
	dpcData.des.HEZXHZ_DES += "<span>，选号示例：</span>投注：和值1开奖：*01,*10奖金：\""+ HEZXHZ_WIN +"\"元";

	//后二组选复式
	var HEZXFS_G_WIN = dpcData.getKindPlayLotteryWin("HEZXFS_G");
	dpcData.des.HEZXFS_G_DES = "<div class='title'> 从0-9中选择2个数字组成一注，所选号码与开奖号码的后二位相同，顺序不限，最高奖金\""+ HEZXFS_G_WIN +"\"元";
	dpcData.des.HEZXFS_G_DES += "<span>，选号示例：</span>投注：1,2开奖：*12(不限顺序)奖金：\""+ HEZXFS_G_WIN +"\"元";

	//后二组选单式
	var HEZXDS_G_WIN = dpcData.getKindPlayLotteryWin("HEZXDS_G");
	dpcData.des.HEZXDS_G_DES = "<div class='title'> 从0-9中选择2个数字组成一注，所选号码与开奖号码的后二位相同，顺序不限，最高奖金\""+ HEZXDS_G_WIN +"\"元";
	dpcData.des.HEZXDS_G_DES += "<span>，选号示例：</span>投注：5,6开奖：*56(不限顺序)奖金：\""+ HEZXDS_G_WIN +"\"元";

	//后二组选和值
	var HEZXHZ_G_WIN = dpcData.getKindPlayLotteryWin("HEZXHZ_G");
	dpcData.des.HEZXHZ_G_DES = "<div class='title'> 所选数值等于开奖号码的后二位数字相加之和（不含对子），最高奖金\""+ HEZXHZ_G_WIN +"\"元";
	dpcData.des.HEZXHZ_G_DES += "<span>，选号示例：</span>投注：和值1开奖：*10(不限顺序)奖金：\""+ HEZXHZ_G_WIN +"\"元";

	//前二大小单双
	var QEDXDS_WIN = dpcData.getKindPlayLotteryWin("QEDXDS");
	dpcData.des.QEDXDS_DES = "<div class='title'> 对应百位以及十位选择大小单双，0、2、4 、6、8 为双；1、3、5、7、9 为单；0、1、2、3、4 为小；5、6、7、8、9 为大。最高奖金\""+ QEDXDS_WIN +"\"元";
	dpcData.des.QEDXDS_DES += "<span>，选号示例：</span>投注：大大、大双、单大、单双开奖：98*奖金：\""+ QEDXDS_WIN +"\"元";

	//后二大小单双
	var HEDXDS_WIN = dpcData.getKindPlayLotteryWin("HEDXDS");
	dpcData.des.HEDXDS_DES = "<div class='title'> 对应个位以及十位选择大小单双，0、2、4 、6、8 为双；1、3、5、7、9 为单；0、1、2、3、4 为小；5、6、7、8、9 为大。最高奖金\""+ HEDXDS_WIN +"\"元";
	dpcData.des.HEDXDS_DES += "<span>，选号示例：</span>投注：大大、大双、单大、单双开奖：*98奖金：\""+ HEDXDS_WIN +"\"元";

	//三星定位胆
	var SXDWD_WIN = dpcData.getKindPlayLotteryWin("SXDWD");
	dpcData.des.SXDWD_DES = "<div class='title'> 从百位、十位、个位任意位置上至少选择1个号码，选号与相同位置上的开奖号码一致，最高奖金\""+ SXDWD_WIN +"\"元";
	dpcData.des.SXDWD_DES += "<span>，选号示例：</span>投注：1**开奖：1**奖金：\""+ SXDWD_WIN +"\"元";
	
	//任选二
	var RXE_WIN = dpcData.getKindPlayLotteryWin("RXE");
	dpcData.des.RXE_DES = "<div class='title'> 从百位、十位、个位中至少两位上各选1个号码，选号与相同位置上的开奖号码都一致，最高奖金\""+ RXE_WIN +"\"元";
	dpcData.des.RXE_DES += "<span>，选号示例：</span>投注：1*3开奖：1*3奖金：\""+ RXE_WIN +"\"元";
	
	//任选二直选单式
	var RXEZXDS_WIN = dpcData.getKindPlayLotteryWin("RXEZXDS");
	dpcData.des.RXEZXDS_DES = "<div class='title'> 从百位、十位、个位中至少四位上各选择1个号码，选号与相同位置上的开奖号码一致，最高奖金\""+ RXEZXDS_WIN +"\"元";
	dpcData.des.RXEZXDS_DES += "<span>，选号示例：</span>投注：1*2开奖：1*2奖金：\""+ RXEZXDS_WIN +"\"元";
	//展示当前玩法的描述
	dpcData.showPlayDes(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey);
};

/**
 * 任选位置复选框点击事件
 */
DpcData.prototype.dsPositionCheckFunc = function(obj){
	var positionCount = 0;
	$("input[name='ds_position']").each(function(){
		if(this.checked){
			positionCount++;
		}
	});
	
    if(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey == 'RXSIZXDS'){
		//判断当前是否符合该单式的处理方式
		if(positionCount == 4){  //选择4位的时候,有一个方案
			dpcData.rxParam.isRxDsAllow = true;
			dpcData.rxParam.dsAllowCount = 1;
			$("#pnum").text(positionCount);
			$("#fnum").text(1);  
		}else if(positionCount == 5){
			dpcData.rxParam.isRxDsAllow = true;
			dpcData.rxParam.dsAllowCount = 5;
			$("#pnum").text(positionCount);
			$("#fnum").text(5);  
		}else{
			dpcData.rxParam.isRxDsAllow = false;
			dpcData.rxParam.dsAllowCount = 0;
			$("#pnum").text(positionCount);
			$("#fnum").text(0);  
		}
    }else if(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey == 'RXEZXDS'){
		//判断当前是否符合该单式的处理方式
		if(positionCount == 2){  //选择2位的时候,有一个方案
			dpcData.rxParam.isRxDsAllow = true;
			dpcData.rxParam.dsAllowCount = 1;
			$("#pnum").text(positionCount);
			$("#fnum").text(1);  
		}else if(positionCount == 3){
			dpcData.rxParam.isRxDsAllow = true;
			dpcData.rxParam.dsAllowCount = 3;
			$("#pnum").text(positionCount);
			$("#fnum").text(3);  
		}else if(positionCount == 4){
			dpcData.rxParam.isRxDsAllow = true;
			dpcData.rxParam.dsAllowCount = 6;
			$("#pnum").text(positionCount);
			$("#fnum").text(6);  
		}else if(positionCount == 5){
			dpcData.rxParam.isRxDsAllow = true;
			dpcData.rxParam.dsAllowCount = 10;
			$("#pnum").text(positionCount);
			$("#fnum").text(10);  
		}else{
			dpcData.rxParam.isRxDsAllow = false;
			dpcData.rxParam.dsAllowCount = 0;
			$("#pnum").text(positionCount);
			$("#fnum").text(0);  
		}
    }else if(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey == 'RXSZXDS'){
		//判断当前是否符合该单式的处理方式
		if(positionCount == 3){  //选择2位的时候,有一个方案
			dpcData.rxParam.isRxDsAllow = true;
			dpcData.rxParam.dsAllowCount = 1;
			$("#pnum").text(positionCount);
			$("#fnum").text(1);  
		}else if(positionCount == 4){
			dpcData.rxParam.isRxDsAllow = true;
			dpcData.rxParam.dsAllowCount = 4;
			$("#pnum").text(positionCount);
			$("#fnum").text(4);  
		}else if(positionCount == 5){
			dpcData.rxParam.isRxDsAllow = true;
			dpcData.rxParam.dsAllowCount = 10;
			$("#pnum").text(positionCount);
			$("#fnum").text(10);  
		}else{
			dpcData.rxParam.isRxDsAllow = false;
			dpcData.rxParam.dsAllowCount = 0;
			$("#pnum").text(positionCount);
			$("#fnum").text(0);  
		}
    }else{
    	showTip("该直选单式未配置");
    }
    
    //格式化,统计该单式数目
    lotteryCommonPage.dealForDsFormat(false);
};


/**
 * 前三或者后三直选和值,注数
 */
DpcData.prototype.getLotteryCountByShanZhiHeZhi = function(everyPositionCodesInt){
	var cathecticCount = 0;
	if(everyPositionCodesInt == 0){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 1){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 2){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 3){
		cathecticCount += 10;
	}else if(everyPositionCodesInt == 4){
		cathecticCount += 15;
	}else if(everyPositionCodesInt == 5){
		cathecticCount += 21;
	}else if(everyPositionCodesInt == 6){
		cathecticCount += 28;
	}else if(everyPositionCodesInt == 7){
		cathecticCount += 36;
	}else if(everyPositionCodesInt == 8){
		cathecticCount += 45;
	}else if(everyPositionCodesInt == 9){
		cathecticCount += 55;
	}else if(everyPositionCodesInt == 10){
		cathecticCount += 63;
	}else if(everyPositionCodesInt == 11){
		cathecticCount += 69;
	}else if(everyPositionCodesInt == 12){
		cathecticCount += 73;
	}else if(everyPositionCodesInt == 13){
		cathecticCount += 75;
	}else if(everyPositionCodesInt == 14){
		cathecticCount += 75;
	}else if(everyPositionCodesInt == 15){
		cathecticCount += 73;
	}else if(everyPositionCodesInt == 16){
		cathecticCount += 69;
	}else if(everyPositionCodesInt == 17){
		cathecticCount += 63;
	}else if(everyPositionCodesInt == 18){
		cathecticCount += 55;
	}else if(everyPositionCodesInt == 19){
		cathecticCount += 45;
	}else if(everyPositionCodesInt == 20){
		cathecticCount += 36;
	}else if(everyPositionCodesInt == 21){
		cathecticCount += 28;
	}else if(everyPositionCodesInt == 22){
		cathecticCount += 21;
	}else if(everyPositionCodesInt == 23){
		cathecticCount += 15;
	}else if(everyPositionCodesInt == 24){
		cathecticCount += 10;
	}else if(everyPositionCodesInt == 25){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 26){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 27){
		cathecticCount += 1;
	}else{
		showTip("无该和值的号码");
	}
	return cathecticCount;	
};

/**
 * 前三或者后三直选和值,注数
 */
DpcData.prototype.getLotteryCountByShanZuHeZhi = function(everyPositionCodesInt){
	var cathecticCount = 0;
    if(everyPositionCodesInt == 1){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 2){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 3){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 4){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 5){
		cathecticCount += 5;
	}else if(everyPositionCodesInt == 6){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 7){
		cathecticCount += 8;
	}else if(everyPositionCodesInt == 8){
		cathecticCount += 10;
	}else if(everyPositionCodesInt == 9){
		cathecticCount += 11;
	}else if(everyPositionCodesInt == 10){
		cathecticCount += 13;
	}else if(everyPositionCodesInt == 11){
		cathecticCount += 14;
	}else if(everyPositionCodesInt == 12){
		cathecticCount += 14;
	}else if(everyPositionCodesInt == 13){
		cathecticCount += 15;
	}else if(everyPositionCodesInt == 14){
		cathecticCount += 15;
	}else if(everyPositionCodesInt == 15){
		cathecticCount += 14;
	}else if(everyPositionCodesInt == 16){
		cathecticCount += 14;
	}else if(everyPositionCodesInt == 17){
		cathecticCount += 13;
	}else if(everyPositionCodesInt == 18){
		cathecticCount += 11;
	}else if(everyPositionCodesInt == 19){
		cathecticCount += 10;
	}else if(everyPositionCodesInt == 20){
		cathecticCount += 8;
	}else if(everyPositionCodesInt == 21){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 22){
		cathecticCount += 5;
	}else if(everyPositionCodesInt == 23){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 24){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 25){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 26){
		cathecticCount += 1;
	}else{
		showTip("无该和值的号码");
	}
	return cathecticCount;	
};

/**
 * 前二或者后二直选和值,注数
 */
DpcData.prototype.getLotteryCountByErZhiHeZhi = function(everyPositionCodesInt){
	var cathecticCount = 0;
	if(everyPositionCodesInt == 0){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 1){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 2){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 3){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 4){
		cathecticCount += 5;
	}else if(everyPositionCodesInt == 5){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 6){
		cathecticCount += 7;
	}else if(everyPositionCodesInt == 7){
		cathecticCount += 8;
	}else if(everyPositionCodesInt == 8){
		cathecticCount += 9;
	}else if(everyPositionCodesInt == 9){
		cathecticCount += 10;
	}else if(everyPositionCodesInt == 10){
		cathecticCount += 9;
	}else if(everyPositionCodesInt == 11){
		cathecticCount += 8;
	}else if(everyPositionCodesInt == 12){
		cathecticCount += 7;
	}else if(everyPositionCodesInt == 13){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 14){
		cathecticCount += 5;
	}else if(everyPositionCodesInt == 15){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 16){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 17){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 18){
		cathecticCount += 1;
	}else{
		showTip("无该和值的号码");
	}
	return cathecticCount;
};

/**
 * 前三或者后三直选和值,注数
 */
DpcData.prototype.getLotteryCountByErZuHeZhi = function(everyPositionCodesInt){
	var cathecticCount = 0;
	if(everyPositionCodesInt == 1){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 2){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 3){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 4){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 5){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 6){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 7){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 8){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 9){
		cathecticCount += 5;
	}else if(everyPositionCodesInt == 10){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 11){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 12){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 13){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 14){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 15){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 16){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 17){
		cathecticCount += 1;
	}else{
		showTip("无该和值的号码");
	}
	return cathecticCount;
};
