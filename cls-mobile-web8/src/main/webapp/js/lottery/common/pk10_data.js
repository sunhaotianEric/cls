function Pk10Data(){}
var pk10Data = new Pk10Data();

//任选参数
pk10Data.rxParam = {
	isRxDsAllow : true,  //是否允许任选单式,由位数来进行控制
    dsAllowCount : 0,
};

//通用的号码数组
pk10Data.common_code_array = new Array("01","02","03","04","05","06","07","08","09","10");
//龙虎斗
pk10Data.lhd_code_array = new Array(1,2);
//大小和单双
pk10Data.dxds_code_array = new Array(1,2,3,4);
//和值
pk10Data.hzgyj_code_array = new Array(3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19);

//////////////////玩法//////////////////// 

pk10Data.plays = {};
//前二玩法
pk10Data.plays.QE_PLAY = "";
pk10Data.plays.QE_PLAY += "<div class='container clist QE animated' >";
pk10Data.plays.QE_PLAY += "	<div class='list-line'>";
pk10Data.plays.QE_PLAY += "		<div class='line-title'><p>直选</p></div>";
pk10Data.plays.QE_PLAY += "		<div class='line-content'>";
pk10Data.plays.QE_PLAY += "			<div class='child color-red' data-role-id='QEZXFS'><p>复式</p></div>";
pk10Data.plays.QE_PLAY += "			<div class='child color-red no' data-role-id='QEZXDS'><p>单式</p></div>";
pk10Data.plays.QE_PLAY += "		</div>";
pk10Data.plays.QE_PLAY += "	</div>";
pk10Data.plays.QE_PLAY += "</div>";

//前三玩法
pk10Data.plays.QS_PLAY = "";
pk10Data.plays.QS_PLAY += "<div class='container clist QS animated' >";
pk10Data.plays.QS_PLAY += "	<div class='list-line'>";
pk10Data.plays.QS_PLAY += "		<div class='line-title'><p>直选</p></div>";
pk10Data.plays.QS_PLAY += "		<div class='line-content'>";
pk10Data.plays.QS_PLAY += "			<div class='child color-red' data-role-id='QSZXFS'><p>复式</p></div>";
pk10Data.plays.QS_PLAY += "			<div class='child color-red no' data-role-id='QSZXDS'><p>单式</p></div>";
pk10Data.plays.QS_PLAY += "		</div>";
pk10Data.plays.QS_PLAY += "	</div>";
pk10Data.plays.QS_PLAY += "</div>";

//前四玩法
pk10Data.plays.QSI_PLAY = "";
pk10Data.plays.QSI_PLAY += "<div class='container clist QSI animated' >";
pk10Data.plays.QSI_PLAY += "	<div class='list-line'>";
pk10Data.plays.QSI_PLAY += "		<div class='line-title'><p>直选</p></div>";
pk10Data.plays.QSI_PLAY += "		<div class='line-content'>";
pk10Data.plays.QSI_PLAY += "			<div class='child color-red' data-role-id='QSIZXFS'><p>复式</p></div>";
pk10Data.plays.QSI_PLAY += "			<div class='child color-red no' data-role-id='QSIZXDS'><p>单式</p></div>";
pk10Data.plays.QSI_PLAY += "		</div>";
pk10Data.plays.QSI_PLAY += "	</div>";
pk10Data.plays.QSI_PLAY += "</div>";

//前五玩法
pk10Data.plays.QW_PLAY = "";
pk10Data.plays.QW_PLAY += "<div class='container clist QW animated' >";
pk10Data.plays.QW_PLAY += "	<div class='list-line'>";
pk10Data.plays.QW_PLAY += "		<div class='line-title'><p>直选</p></div>";
pk10Data.plays.QW_PLAY += "		<div class='line-content'>";
pk10Data.plays.QW_PLAY += "			<div class='child color-red' data-role-id='QWZXFS'><p>复式</p></div>";
pk10Data.plays.QW_PLAY += "			<div class='child color-red no' data-role-id='QWZXDS'><p>单式</p></div>";
pk10Data.plays.QW_PLAY += "		</div>";
pk10Data.plays.QW_PLAY += "	</div>";
pk10Data.plays.QW_PLAY += "</div>";

//龙虎斗玩法
pk10Data.plays.LHD_PLAY = "";
pk10Data.plays.LHD_PLAY += "<div class='container clist LHD animated'>";
pk10Data.plays.LHD_PLAY += "	<div class='list-line'>";
pk10Data.plays.LHD_PLAY += "		<div class='line-title'><p>直选</p></div>";
pk10Data.plays.LHD_PLAY += "		<div class='line-content'>";
pk10Data.plays.LHD_PLAY += "			<div class='child color-red no' data-role-id='LHD1VS10'><p>1VS10</p></div>";
pk10Data.plays.LHD_PLAY += "			<div class='child color-red no' data-role-id='LHD2VS9'><p>2VS9</p></div>";
pk10Data.plays.LHD_PLAY += "			<div class='child color-red no' data-role-id='LHD3VS8'><p>3VS8</p></div>";
pk10Data.plays.LHD_PLAY += "			<div class='child color-red no' data-role-id='LHD4VS7'><p>4VS7</p></div>";
pk10Data.plays.LHD_PLAY += "			<div class='child color-red no' data-role-id='LHD5VS6'><p>5VS6</p></div>";
pk10Data.plays.LHD_PLAY += "		</div>";
pk10Data.plays.LHD_PLAY += "	</div>";
pk10Data.plays.LHD_PLAY += "</div>";

//大小玩法
pk10Data.plays.DX_PLAY = "";
pk10Data.plays.DX_PLAY += "<div class='container clist DX animated'>";
pk10Data.plays.DX_PLAY += "	<div class='list-line'>";
pk10Data.plays.DX_PLAY += "		<div class='line-title'><p>直选</p></div>";
pk10Data.plays.DX_PLAY += "		<div class='line-content'>";
pk10Data.plays.DX_PLAY += "			<div class='child color-red no' data-role-id='DXGJ'><p>冠军</p></div>";
pk10Data.plays.DX_PLAY += "			<div class='child color-red no' data-role-id='DXYJ'><p>亚军</p></div>";
pk10Data.plays.DX_PLAY += "			<div class='child color-red no' data-role-id='DXJJ'><p>季军</p></div>";
pk10Data.plays.DX_PLAY += "			<div class='child color-red no' data-role-id='DXDSM'><p>第四名</p></div>";
pk10Data.plays.DX_PLAY += "			<div class='child color-red no' data-role-id='DXDWM'><p>第五名</p></div>";
pk10Data.plays.DX_PLAY += "		</div>";
pk10Data.plays.DX_PLAY += "	</div>";
pk10Data.plays.DX_PLAY += "</div>";

//单双玩法
pk10Data.plays.DS_PLAY = "";
pk10Data.plays.DS_PLAY += "<div class='container clist DS animated'>";
pk10Data.plays.DS_PLAY += "	<div class='list-line'>";
pk10Data.plays.DS_PLAY += "		<div class='line-title'><p>直选</p></div>";
pk10Data.plays.DS_PLAY += "		<div class='line-content'>";
pk10Data.plays.DS_PLAY += "			<div class='child color-red no' data-role-id='DSGJ'><p>冠军</p></div>";
pk10Data.plays.DS_PLAY += "			<div class='child color-red no' data-role-id='DSYJ'><p>亚军</p></div>";
pk10Data.plays.DS_PLAY += "			<div class='child color-red no' data-role-id='DSJJ'><p>季军</p></div>";
pk10Data.plays.DS_PLAY += "			<div class='child color-red no' data-role-id='DSDSM'><p>第四名</p></div>";
pk10Data.plays.DS_PLAY += "			<div class='child color-red no' data-role-id='DSDWM'><p>第五名</p></div>";
pk10Data.plays.DS_PLAY += "		</div>";
pk10Data.plays.DS_PLAY += "	</div>";
pk10Data.plays.DS_PLAY += "</div>";


//和值玩法
pk10Data.plays.HZ_PLAY = "";
pk10Data.plays.HZ_PLAY += "<div class='container clist HZ animated'>";
pk10Data.plays.HZ_PLAY += "	<div class='list-line'>";
pk10Data.plays.HZ_PLAY += "		<div class='line-title'><p>直选</p></div>";
pk10Data.plays.HZ_PLAY += "		<div class='line-content'>";
pk10Data.plays.HZ_PLAY += "			<div class='child color-red no' data-role-id='HZGYJ'><p>冠亚和值</p></div>";
pk10Data.plays.HZ_PLAY += "		</div>";
pk10Data.plays.HZ_PLAY += "	</div>";
pk10Data.plays.HZ_PLAY += "</div>";


//和值大小单双玩法
pk10Data.plays.CGJ_PLAY = "";
pk10Data.plays.CGJ_PLAY += "<div class='container clist CGJ animated'>";
pk10Data.plays.CGJ_PLAY += "	<div class='list-line'>";
pk10Data.plays.CGJ_PLAY += "		<div class='line-title'><p>直选</p></div>";
pk10Data.plays.CGJ_PLAY += "		<div class='line-content'>";
pk10Data.plays.CGJ_PLAY += "			<div class='child color-red no' data-role-id='CGJ'><p>猜冠军</p></div>";
pk10Data.plays.CGJ_PLAY += "		</div>";
pk10Data.plays.CGJ_PLAY += "	</div>";
pk10Data.plays.CGJ_PLAY += "</div>";


//////////////////玩法描述////////////////////
pk10Data.des = {};

pk10Data.des.QYZXFS_DES = "玩法加载中，请稍后........";
pk10Data.des.QYZXDS_DES = "玩法加载中，请稍后........";

pk10Data.des.QEZXFS_DES = "玩法加载中，请稍后........";
pk10Data.des.QEZXDS_DES = "玩法加载中，请稍后........";

pk10Data.des.QSZXFS_DES = "玩法加载中，请稍后........";
pk10Data.des.QSZXDS_DES = "玩法加载中，请稍后........";

pk10Data.des.QSIZXFS_DES = "玩法加载中，请稍后........";
pk10Data.des.QSIZXDS_DES = "玩法加载中，请稍后........";

pk10Data.des.QWZXFS_DES = "玩法加载中，请稍后........";
pk10Data.des.QWZXDS_DES = "玩法加载中，请稍后........";

pk10Data.des.DWDS_DES = "玩法加载中，请稍后........";

pk10Data.des.LHD1VS10_DES = "玩法加载中，请稍后........";
pk10Data.des.LHD2VS9_DES = "玩法加载中，请稍后........";
pk10Data.des.LHD3VS8_DES = "玩法加载中，请稍后........";
pk10Data.des.LHD4VS7_DES = "玩法加载中，请稍后........";
pk10Data.des.LHD5VS6_DES = "玩法加载中，请稍后........";

pk10Data.des.DXGJ_DES = "玩法加载中，请稍后........";
pk10Data.des.DXYJJ_DES = "玩法加载中，请稍后........";
pk10Data.des.DXJJ_DES = "玩法加载中，请稍后........";
pk10Data.des.DXDSM_DES = "玩法加载中，请稍后........";
pk10Data.des.DXDWM_DES = "玩法加载中，请稍后........";

pk10Data.des.DSGJ_DES = "玩法加载中，请稍后........";
pk10Data.des.DSYJ_DES = "玩法加载中，请稍后........";
pk10Data.des.DSJJ_DES = "玩法加载中，请稍后........";
pk10Data.des.DSDSM_DES = "玩法加载中，请稍后........";
pk10Data.des.DSDWM_DES = "玩法加载中，请稍后........";

pk10Data.des.HZGYJ_DES  = "玩法加载中，请稍后........";

//玩法描述映射值
pk10Data.lotteryKindMap = new JS_OBJECT_MAP();

pk10Data.lotteryKindMap.put("QYZXFS", "前一直选复式");
pk10Data.lotteryKindMap.put("QYZXDS", "前一直选单式");

pk10Data.lotteryKindMap.put("QEZXFS", "前二直选复式");
pk10Data.lotteryKindMap.put("QEZXDS", "前二直选单式");

pk10Data.lotteryKindMap.put("QSZXFS", "前三直选复式");
pk10Data.lotteryKindMap.put("QSZXDS", "前三直选单式");

pk10Data.lotteryKindMap.put("QSIZXFS", "前四直选复式");
pk10Data.lotteryKindMap.put("QSIZXDS", "前四直选单式");

pk10Data.lotteryKindMap.put("QWZXFS", "前五直选复式");
pk10Data.lotteryKindMap.put("QWZXDS", "前五直选单式");

pk10Data.lotteryKindMap.put("DWD", "定位胆");

//龙虎斗
pk10Data.lotteryKindMap.put("LHD1VS10", "龙虎斗-1VS10");
pk10Data.lotteryKindMap.put("LHD2VS9", "龙虎斗-2VS9");
pk10Data.lotteryKindMap.put("LHD3VS8", "龙虎斗-3VS8");
pk10Data.lotteryKindMap.put("LHD4VS7", "龙虎斗-4VS7");
pk10Data.lotteryKindMap.put("LHD5VS6", "龙虎斗-5VS6");

//大小
pk10Data.lotteryKindMap.put("DXGJ", "大小-冠军");
pk10Data.lotteryKindMap.put("DXYJ", "大小-亚军");
pk10Data.lotteryKindMap.put("DXJJ", "大小-季军");
pk10Data.lotteryKindMap.put("DXDSM", "大小-第四名");
pk10Data.lotteryKindMap.put("DXDWM", "大小-第五名");

//单双
pk10Data.lotteryKindMap.put("DSGJ", "单双-冠军");
pk10Data.lotteryKindMap.put("DSYJ", "单双-亚军");
pk10Data.lotteryKindMap.put("DSJJ", "单双-季军");
pk10Data.lotteryKindMap.put("DSDSM", "单双-第四名");
pk10Data.lotteryKindMap.put("DSDWM", "单双-第五名");

//冠亚和值
pk10Data.lotteryKindMap.put("HZGYJ", "冠亚和值");
//猜冠军
pk10Data.lotteryKindMap.put("CGJ", "猜冠军");

/**
 * 加载所有的玩法
 */
Pk10Data.prototype.loadLotteryKindPlay = function() {
	$("#lotteryKindPlayList").append(pk10Data.plays.QE_PLAY);
	$("#lotteryKindPlayList").append(pk10Data.plays.QS_PLAY);
	$("#lotteryKindPlayList").append(pk10Data.plays.QSI_PLAY);
	$("#lotteryKindPlayList").append(pk10Data.plays.QW_PLAY);
	$("#lotteryKindPlayList").append(pk10Data.plays.LHD_PLAY);
	$("#lotteryKindPlayList").append(pk10Data.plays.DX_PLAY);
	$("#lotteryKindPlayList").append(pk10Data.plays.DS_PLAY);
	$("#lotteryKindPlayList").append(pk10Data.plays.HZ_PLAY);
	$("#lotteryKindPlayList").append(pk10Data.plays.CGJ_PLAY);
};


/**
 * 玩法选择显示描述
 * @param roleId
 */
Pk10Data.prototype.showPlayDes = function(roleId) {
	var playDes = "";
	
	  if(roleId == 'QYZXFS'){
		  playDes = pk10Data.des.QYZXFS_DES;
	  }else if(roleId == 'QEZXFS'){
		  playDes = pk10Data.des.QEZXFS_DES;
	  }else if(roleId == 'QEZXDS'){
		  playDes = pk10Data.des.QEZXDS_DES;
	  }else if(roleId == 'QSZXFS'){
		  playDes = pk10Data.des.QSZXFS_DES;
	  }else if(roleId == 'QSZXDS'){
		  playDes = pk10Data.des.QSZXDS_DES;
	  }else if(roleId == 'QSIZXFS'){
		  playDes = pk10Data.des.QSIZXFS_DES;
	  }else if(roleId == 'QSIZXDS'){
		  playDes = pk10Data.des.QSIZXDS_DES;
	  }else if(roleId == 'QWZXFS'){
		  playDes = pk10Data.des.QWZXFS_DES;
	  }else if(roleId == 'QWZXDS'){
		  playDes = pk10Data.des.QWZXDS_DES;
	  }else if(roleId == 'DWD'){
		  playDes = pk10Data.des.DWD_DES;
	  }else if(roleId == 'LHD1VS10'){
		  playDes = pk10Data.des.LHD1VS10_DES;
	  }else if(roleId == 'LHD2VS9'){
		  playDes = pk10Data.des.LHD2VS9_DES;
	  }else if(roleId == 'LHD3VS8'){
		  playDes = pk10Data.des.LHD3VS8_DES;
	  }else if(roleId == 'LHD4VS7'){
		  playDes = pk10Data.des.LHD4VS7_DES;
	  }else if(roleId == 'LHD5VS6'){
		  playDes = pk10Data.des.LHD5VS6_DES;
	  }else if(roleId == 'DXGJ'){
		  playDes = pk10Data.des.DXGJ_DES;
	  }else if(roleId == 'DXYJ'){
		  playDes = pk10Data.des.DXYJ_DES;
	  }else if(roleId == 'DXJJ'){
		  playDes = pk10Data.des.DXJJ_DES;
	  }else if(roleId == 'DXDSM'){
		  playDes = pk10Data.des.DXDSM_DES;
	  }else if(roleId == 'DXDWM'){
		  playDes = pk10Data.des.DXDWM_DES;
	  }else if(roleId == 'DSGJ'){
		  playDes = pk10Data.des.DSGJ_DES;
	  }else if(roleId == 'DSYJ'){
		  playDes = pk10Data.des.DSYJ_DES;
	  }else if(roleId == 'DSJJ'){
		  playDes = pk10Data.des.DSJJ_DES;
	  }else if(roleId == 'DSDSM'){
		  playDes = pk10Data.des.DSDSM_DES;
	  }else if(roleId == 'DSDWM'){
		  playDes = pk10Data.des.DSDWM_DES;
	  }else if(roleId == 'HZGYJ'){
		  playDes = pk10Data.des.HZGYJ_DES;
	  }else if(roleId == 'CGJ'){
		  playDes = pk10Data.des.HZGYJ_DES;
	  }else{
		  showTip("不可知的彩种玩法描述.");
	  }
	
	$("#playDes").html(playDes);
};

/**
 * 玩法选择
 * @param roleId
 */
Pk10Data.prototype.lotteryKindPlayChoose = function(roleId) {
	
		var desArray = new Array("第一名", "第二名", "第三名", "第四名", "第五名","第六名", "第七名", "第八名", "第九名", "第十名");
		var emptyDesArray = new Array("");
		
	  if(roleId == 'QYZXFS'){
		  pk10Data.ballSectionCommon(0,0,desArray,null,true);
	  }else if(roleId == 'QEZXFS'){
		  pk10Data.ballSectionCommon(0,1,desArray,null,true);
	  }else if(roleId == 'QEZXDS'){
		  pk10Data.dsBallSectionCommon(roleId);
	  }else if(roleId == 'QSZXFS'){
		  pk10Data.ballSectionCommon(0,2,desArray,null,true);
	  }else if(roleId == 'QSZXDS'){
		  pk10Data.dsBallSectionCommon(roleId);
	  }else if(roleId == 'QSIZXFS'){  //前四直选复式
		  pk10Data.ballSectionCommon(0,3,desArray,null,true);
	  }else if(roleId == 'QSIZXDS'){  //前四直选单式
		  pk10Data.dsBallSectionCommon(roleId);
	  }else if(roleId == 'QWZXFS'){  //前五直选复式
		  pk10Data.ballSectionCommon(0,4,desArray,null,true);
	  }else if(roleId == 'QWZXDS'){  //前五直选单式
		  pk10Data.dsBallSectionCommon(roleId);
	  }else if(roleId == 'DWD'){  //定位胆一到五名直选
		  pk10Data.ballSectionCommon(0,9,desArray,null,true);
	  }else if(roleId == 'LHD1VS10'){  //龙虎斗直选
		  pk10Data.ballSectionCommon(0,1,emptyDesArray,"LHD1VS10",false);
	  }else if(roleId == 'LHD2VS9'){  //龙虎斗直选
		  pk10Data.ballSectionCommon(0,1,emptyDesArray,"LHD2VS9",false);
	  }else if(roleId == 'LHD3VS8'){  //龙虎斗直选
		  pk10Data.ballSectionCommon(0,1,emptyDesArray,"LHD3VS8",false);
	  }else if(roleId == 'LHD4VS7'){  //龙虎斗直选
		  pk10Data.ballSectionCommon(0,1,emptyDesArray,"LHD4VS7",false);
	  }else if(roleId == 'LHD5VS6'){  //龙虎斗名直选
		  pk10Data.ballSectionCommon(0,1,emptyDesArray,"LHD5VS6",false);
	  }else if(roleId == 'DXGJ'){  //大小冠军直选
		  pk10Data.ballSectionCommon(0,1,emptyDesArray,"DXGJ",false);
	  }else if(roleId == 'DXYJ'){  //大小亚军直选
		  pk10Data.ballSectionCommon(0,1,emptyDesArray,"DXYJ");
	  }else if(roleId == 'DXJJ'){  //大小季军直选
		  pk10Data.ballSectionCommon(0,1,emptyDesArray,"DXJJ");
	  }else if(roleId == 'DXDSM'){  //大小第四名直选
		  pk10Data.ballSectionCommon(0,1,emptyDesArray,"DXDSM");
	  }else if(roleId == 'DXDWM'){  //大小第五名直选
		  pk10Data.ballSectionCommon(0,1,emptyDesArray,"DXDWM");
	  }else if(roleId == 'DSGJ'){  //单双冠军直选
		  pk10Data.ballSectionCommon(0,1,emptyDesArray,"DSGJ");
	  }else if(roleId == 'DSYJ'){  //单双亚军直选
		  pk10Data.ballSectionCommon(0,1,emptyDesArray,"DSYJ");
	  }else if(roleId == 'DSJJ'){  //单双季军直选
		  pk10Data.ballSectionCommon(0,1,emptyDesArray,"DSJJ");
	  }else if(roleId == 'DSDSM'){  //单双第四名直选
		  pk10Data.ballSectionCommon(0,1,emptyDesArray,"DSDSM");
	  }else if(roleId == 'DSDWM'){  //单双第五名直选
		  pk10Data.ballSectionCommon(0,1,emptyDesArray,"DSDWM");
	  }else if(roleId == 'HZGYJ'){  //单双第五名直选
		  pk10Data.ballSectionCommon(0,1,emptyDesArray,"HZGYJ",false);
	  }else if(roleId == 'CGJ'){  //单双第五名直选
		  pk10Data.ballSectionCommon(0,1,emptyDesArray,"CGJ",false);
	  }else{
		  showTip("不可知的彩种玩法.");
	  }  
	
	//存储当前切换的玩法模式
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey = roleId;
	
	//显示玩法名称
	var currentKindName = pk10Data.lotteryKindMap.get(roleId);
    $("#currentKindName").text(currentKindName);

	//显示玩法说明
	pk10Data.showPlayDes(roleId);
	
	//拼接完，进行事件的重新申明
	lotteryCommonPage.dealForEvent();
};





/**
 * 复式玩法的号码拼接
 * desArray 描述的数组
 * specialKind 特殊玩法
 * isOmitColdDataDeal 是否遗漏和冷热数据的处理
 */
Pk10Data.prototype.ballSectionCommon = function(numCountStart,numCountEnd,desArray,specialKind,isOmitColdDataDeal){
	var content = "";
	
	if(specialKind == null){
		
		//大小奇偶按钮字符串
		var dxjoStr='		<div class="content">'+
					'			<div class="mun" data-role-type="big">大</div>'+
					'			<div class="mun" data-role-type="small">小</div>'+
					'			<div class="mun" data-role-type="odd">单</div>'+
					'			<div class="mun" data-role-type="even">双</div>'+
					'			<div class="mun" data-role-type="all">全</div>'+
					'			<div class="mun" data-role-type="empty">清</div>'+
					'		</div>';
		
		for(var i = numCountStart;i <= numCountEnd; i++){
			content += '<div class="lottery-child main"><div class="container">';
			var numDes = desArray[i];
			if(isNotEmpty(numDes)) {
				content += '<div class="child-head">';
				content += '	<div class="title"><p>' + numDes + '</p></div>';
				//大小单双
				content += dxjoStr;
				//遗漏冷热元素处理
				if(isOmitColdDataDeal) {
					content += "";
				}
				content += '</div>';
			}
			content += '<div class="child-content">';
			//只有单行的情况 号码排列居中
			/*if(numCountStart == numCountEnd) {
				content += '	<div class="content long2">';
			} else {
				content += '	<div class="content">';
			}*/
			content += '	<div class="content">';
			//01 - 10的号码
			for(var j = 0; j < pk10Data.common_code_array.length; j++){
				content += '<div class="munbtn mun" data-realvalue="'+ pk10Data.common_code_array[j] +'" name="ballNum_'+i+'_match">'+ pk10Data.common_code_array[j] +'</div>';
				//遗漏冷热元素处理
				if(isOmitColdDataDeal) {
					content += "";
				}
			}
			content += '	</div>';
			content += '</div>';
			content += '</div>';
			content += '</div>';
		}
	}else{ 
		var hz_code_array = null;
		dxds_code_array = null;
		
		if(specialKind == "HZGYJ"){
			//3 - 19的号码
			hz_code_array = pk10Data.hzgyj_code_array;
		}else if(specialKind == "CGJ"){
			//和值大小单双
			hz_dxds_code_array = pk10Data.dxds_code_array;
			hz_code_array = pk10Data.hzgyj_code_array;
			lhd_code_array = pk10Data.lhd_code_array;
		}
		
		if(hz_dxds_code_array != null && specialKind == "CGJ"){

			for(var i = 0; i < desArray.length; i++){
				content += '<div class="lottery-child no main"><div class="container">';
				var numDes = desArray[i];
				content += '<div class="child-content" style="text-align:center;">';
		/*		if(isNotEmpty(numDes)) {
					content += '<div class="title"><p>' + numDes + '</p></div>';
					//遗漏冷热元素处理
					if(isOmitColdDataDeal) {
						content += "";
					}
				}*/
				//1 - 4的号码
				for(var j = 0; j < pk10Data.dxds_code_array.length; j++){
					if(pk10Data.dxds_code_array[j] == 1){
						content += '<div class="mun2">';
						content += '<h2 class="value font-blue munbtn" data-realvalue="'+ pk10Data.dxds_code_array[j] +'" name="ballNum_0_match" >大</h2>';
						content += '<p class="info"></p>';
						content += '</div>';
					}else if(pk10Data.dxds_code_array[j] == 2){
						content += '<div class="mun2">';
						content += '<h2 class="value font-blue munbtn" data-realvalue="'+ pk10Data.dxds_code_array[j] +'" name="ballNum_0_match" >小</h2>';
						content += '<p class="info"></p>';
						content += '</div>';
					}else if(pk10Data.dxds_code_array[j] == 3){
						content += '<div class="mun2">';
						content += '<h2 class="value font-blue munbtn" data-realvalue="'+ pk10Data.dxds_code_array[j] +'" name="ballNum_1_match" >单</h2>';
						content += '<p class="info"></p>';
						content += '</div>';
					}else if(pk10Data.dxds_code_array[j] == 4){
						content += '<div class="mun2">';
						content += '<h2 class="value font-blue munbtn" data-realvalue="'+ pk10Data.dxds_code_array[j] +'" name="ballNum_1_match" >双</h2>';
						content += '<p class="info"></p>';
						content += '</div>';
					}
				
				}
				content += '</div>';
				content += '</div>';
				content += '</div>';
			}
		
		}
		if(hz_code_array != null) {
			
			if(specialKind == "CGJ"){
				
				for(var j = 0; j < hz_code_array.length; j++){
					if(j%4 == 0){
						content += '<div class="lottery-child no main"><div class="container">';
						var numDes = desArray[i];
						content += '<div class="child-content" style="text-align:center;">';
					}
					content += '<div class="mun2">';
					content += '<h2 class="value munbtn" data-realvalue="'+ hz_code_array[j] +'" name="ballNum_2_match" >'+ hz_code_array[j] +'</h2>';
					content += '<p class="info"></p>';
					content += '</div>';
					if(lhd_code_array != null){
						if(j > 15){
							if(pk10Data.lhd_code_array[0] == 1){
								content += '<div class="mun2  long">';
								content += '<h2 class="value munbtn" data-realvalue="'+ lhd_code_array[0] +'" name="ballNum_3_match" >龙</h2>';
								content += '<p class="info"></p>';
								content += '</div>';
							}
							if(pk10Data.lhd_code_array[1] == 2){
								content += '<div class="mun2">';
								content += '<h2 class="value munbtn" data-realvalue="'+ lhd_code_array[1] +'" name="ballNum_3_match" >虎</h2>';
								content += '<p class="info"></p>';
								content += '</div>';
							}
						}
					}
					if(j%4 == 3){
						content += '		</div>';
						content += '	</div>';
						content += '</div>';
					}
				}
			}else{
				content += '<div class="lottery-child main"><div class="container">';
				var numDes = desArray[i];
				if(isNotEmpty(numDes)) {
					content += '<div class="child-head">';
					content += '	<div class="title"><p>' + numDes + '</p></div>';
					//遗漏冷热元素处理
					if(isOmitColdDataDeal) {
						content += "";
					}
					content += '</div>';
				}
				content += '<div class="child-content">';
				content += '	<div class="content long2">';
				for(var k = 0; k < hz_code_array.length; k++){
					content += '<div class="munbtn mun" data-realvalue="'+ hz_code_array[k] +'" name="ballNum_2_match">'+ hz_code_array[k] +'</div>';
					//遗漏冷热元素处理
					if(isOmitColdDataDeal) {
						content += "";
					}
				}
				content += '	</div>';
				content += '</div>';
				content += '</div>';
				content += '</div>';
			}
		}else if(specialKind == "LHD1VS10" || specialKind == "LHD2VS9"
			|| specialKind == "LHD3VS8" || specialKind == "LHD4VS7" || specialKind == "LHD5VS6"){


			content += '<div class="lottery-child main vs"><div class="container">';
			content += '<div class="child-content">';
				
			if(pk10Data.lhd_code_array[0] == 1){
				content += '<div class="ball long-ball munbtn" name="ballNum_0_match" data-realvalue='+ pk10Data.lhd_code_array[0]+ '></div>';
				content += '<img src="'+contextPath+'/images/lottery_logo/vs.png" alt="" class="logo-vs">';
			}
			if(pk10Data.lhd_code_array[1] == 2){
				content +='<div class="ball hu-ball munbtn" name="ballNum_0_match" data-realvalue='+pk10Data.lhd_code_array[1]+'></div>';
			}
			
			content += '</div>';
			content += '</div>';
			content += "</div>";
			
		} else if(specialKind == "DXGJ" || specialKind == "DXYJ"
			|| specialKind == "DXJJ" || specialKind == "DXDSM" || specialKind == "DXDWM"){
			for(var i = 0; i < desArray.length; i++){
				content += '<div class="lottery-child main vs"><div class="container">';
				var numDes = desArray[i];
				content += '<div class="child-content">';
				if(isNotEmpty(numDes)) {
					content += '<div class="title"><p>' + numDes + '</p></div>';
					//遗漏冷热元素处理
					if(isOmitColdDataDeal) {
						content += "";
					}
				}
				//1 - 2的号码
				for(var j = 0; j < pk10Data.dxds_code_array.length-2; j++){
					if(pk10Data.dxds_code_array[j] == 1){
						content += '<div class="ball font-blue munbtn" data-realvalue="'+ pk10Data.dxds_code_array[j] +'" name="ballNum_'+i+'_match" >大</div>';
						content += '<img src="'+contextPath+'/images/lottery_logo/vs.png" alt="" class="logo-vs">';
					}else if(pk10Data.dxds_code_array[j] == 2){
						content += '<div class="ball font-blue munbtn"  data-realvalue="'+ pk10Data.dxds_code_array[j] +'" name="ballNum_'+i+'_match">小</div>';
					}
				
				}
				content += '	</div>';
				content += '	</div>';
				content += '</div>';
				content += '</div>';
			}
		} else if(specialKind == "DSGJ" || specialKind == "DSYJ"
			|| specialKind == "DSJJ" || specialKind == "DSDSM" || specialKind == "DSDWM"){
			for(var i = 0; i < desArray.length; i++){
				content += '<div class="lottery-child main vs"><div class="container">';
				var numDes = desArray[i];
				content += '<div class="child-content">';
				if(isNotEmpty(numDes)) {
					content += '<div class="title"><p>' + numDes + '</p></div>';
					//遗漏冷热元素处理
					if(isOmitColdDataDeal) {
						content += "";
					}
				}
				//2 - 4的号码
				for(var j = 2; j < 4; j++){
					if(pk10Data.dxds_code_array[j] == 3){
						content += '<div class="ball font-blue munbtn" data-realvalue="'+ pk10Data.dxds_code_array[j] +'" name="ballNum_1_match" >单</div>';
						content += '<img src="'+contextPath+'/images/lottery_logo/vs.png" alt="" class="logo-vs">';
					}else if(pk10Data.dxds_code_array[j] == 4){
						content += '<div class="ball font-blue munbtn"  data-realvalue="'+ pk10Data.dxds_code_array[j] +'" name="ballNum_1_match" >双</div>';
					}
				}
				content += '	</div>';
				content += '	</div>';
				content += '</div>';
				content += '</div>';
			}
		}else{
			showTip("未找到该玩法的号码.");
		}
	}
	
    $("#ballSection").html(content);
    //猜冠军赔率
    pk10Data.getCurrentLotteryWin(specialKind);
    //任选框隐藏
    $("#rxWeiCheck").hide();
    //隐藏单式的导入框
    $("#importDsBtn").hide();
    
    //进行遗漏冷热的数值处理
    if(isOmitColdDataDeal){
    	$("#ommitHotColdBtn").show();
    } else {
    	$("#ommitHotColdBtn").hide();
    }
};

/**
 * 单式玩法的号码拼接
 * specialKind 特殊玩法
 */
Pk10Data.prototype.dsBallSectionCommon = function(specialKind){
	
	var editorStr = "说明：\n";
	editorStr += "1、请参照标准格式样本格式录入或上传方案。\n";
	editorStr += "2、每一注号码之间的间隔符支持 回车 空格[ ] 逗号[,] 分号[;] 冒号[:] 竖线 [|]。\n";
	editorStr += "3、文件格式必须是.txt格式。\n";
	editorStr += "4、文件较大时会导致上传时间较长，请耐心等待！\n";
	editorStr += "5、将文件拖入文本框即可快速实现文件上传功能。\n";
	editorStr += "6、导入文本内容后将覆盖文本框中现有的内容。";

	var rxContent = '<div class="lottery-select">'+
	'<div class="container lottery-select-list">'+
	'	<label for="">'+
	'		<input type="checkbox" name="ds_position" onclick="lotteryCommonPage.lottertyDataDeal.dsPositionCheckFunc(this);" data-position="1" checked="checked">'+
	'		<span>万位</span>'+
	'	</label>'+
	'	<label for="">'+
	'		<input type="checkbox" name="ds_position" onclick="lotteryCommonPage.lottertyDataDeal.dsPositionCheckFunc(this);" data-position="2" checked="checked">'+
	'		<span>千位</span>'+
	'	</label>'+
	'	<label for="">'+
	'		<input type="checkbox" name="ds_position" onclick="lotteryCommonPage.lottertyDataDeal.dsPositionCheckFunc(this);" data-position="3" checked="checked">'+
	'		<span>百位</span>'+
	'	</label>'+
	'	<label for="">'+
	'		<input type="checkbox" name="ds_position" onclick="lotteryCommonPage.lottertyDataDeal.dsPositionCheckFunc(this);" data-position="4" checked="checked">'+
	'		<span>十位</span>'+
	'	</label>'+
	'	<label for="">'+
	'		<input type="checkbox" name="ds_position" onclick="lotteryCommonPage.lottertyDataDeal.dsPositionCheckFunc(this);" data-position="5" checked="checked">'+
	'		<span>个位</span>'+
	'	</label>'+
	'</div>'+
	'<div class="container lottery-select-msg" >'+
	'	<p>温馨提示：您选择了<span class="font-yellow" id="pnum">5</span>个位置，系统自动根据位置合成<span class="font-yellow" id="fnum">10</span>个方案</p>'+
	'</div>'+
	'</div>';
	
	var content = '	<div class="lottery-child main"><div class="container">'+
	'		<div class="lottery-textarea">'+
	'			<textarea id="lr_editor" placeholder="'+ editorStr +'"></textarea>'+
	'		</div>'+
	'	</div></div>';
	
    $("#ballSection").html(content);
    
    //添加单式的事件处理
    lotteryCommonPage.addDsEvent();
};

/**
 * 计算当前用户最高模式下的奖金
 */
Pk10Data.prototype.calucateCurrentUserHighestWin = function(kindKey){
	if(lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.size() == 0){
		return "奖金未完全获取,请再次操作或者刷新页面";
	}
	var currentLotteryWin;
	currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get(kindKey);
	if(currentLotteryWin == null){
		return "未配置该玩法的奖金.";
	}
	var result = (currentLotteryWin.winMoney + (currentUserPk10Model - pk10LowestAwardModel) * currentLotteryWin.specCode) * 1;	
    return result;
};

/**
 * 获取当前用户最高模式下的奖金
 */
Pk10Data.prototype.getKindPlayLotteryWin = function(kindKey){
	if(lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.size() == 0){
		return "奖金未完全获取,请稍作等待或者再次操作.";
	}
	var currentLotteryWin;
	currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get("PK10" + "_" + kindKey);
	if(currentLotteryWin == null){
		return "未配置该玩法的奖金.";
	}
	var result = (currentLotteryWin.winMoney + (currentUserPk10Model - pk10LowestAwardModel) * currentLotteryWin.specCode) * 1;	
    return result.toFixed(lotteryCommonPage.param.toFixedValue);
};

/**
 * 猜冠军根据玩法和号码获取奖金
 */
Pk10Data.prototype.getCurrentLotteryWin = function(kindKey) {
	if(kindKey == 'CGJ'){
		$("#ballSection .munbtn").each(function(){
			var ballNumName =$(this).attr("name");
			if(ballNumName == "ballNum_0_match"){
				var DXGJ_WIN = pk10Data.getKindPlayLotteryWin("DXGJ");
				$(this).next().text("赔率："+ DXGJ_WIN);
			}else if(ballNumName == "ballNum_1_match"){
				var DSGJ_WIN = pk10Data.getKindPlayLotteryWin("DSGJ");
				$(this).next().text("赔率："+ DSGJ_WIN);
			}else if(ballNumName == "ballNum_3_match"){
				var LHD1VS10_WIN = pk10Data.getKindPlayLotteryWin("LHD1VS10");
				$(this).next().text("赔率："+ LHD1VS10_WIN);
			}
			
			var codeNum =$(this).attr("data-realvalue");
			if(ballNumName != "ballNum_1_match") {
				if(codeNum == '3' || codeNum == '4' ||  codeNum == '18' || codeNum == '19') {
					$(this).next().text("赔率："+ pk10Data.getKindPlayLotteryWin("HZGYJ"));
				} else if(codeNum == '5' || codeNum == '6' || codeNum == '16' || codeNum == '17') {
					$(this).next().text("赔率："+ pk10Data.getKindPlayLotteryWin("HZGYJ_2"));
				} else if(codeNum == '7' || codeNum == '8' || codeNum == '14' || codeNum == '15') {
					$(this).next().text("赔率："+ pk10Data.getKindPlayLotteryWin("HZGYJ_3"));
				} else if(codeNum == '9' || codeNum == '10'|| codeNum == '12' || codeNum == '13') {
					$(this).next().text("赔率："+ pk10Data.getKindPlayLotteryWin("HZGYJ_4"));
				} else if(codeNum == '11') {
					$(this).next().text("赔率："+ pk10Data.getKindPlayLotteryWin("HZGYJ_5"));
				};
			};
		});
	};
};

/**
 * 设置当前用户最高模式下的奖金和玩法描述
 */
Pk10Data.prototype.setKindPlayDes = function(){

	//前一直选复式
	var QYZXFS_WIN = pk10Data.getKindPlayLotteryWin("QYZXFS");
	pk10Data.des.QYZXFS_DES = "<div class='title'>选一个或多个号码，选号包含开奖号码第一位即中奖，中奖。\""+QYZXFS_WIN+"\"元</div>";
	
	//前二直选复式
	var QEZXFS_WIN = pk10Data.getKindPlayLotteryWin("QEZXFS");	
	pk10Data.des.QEZXFS_DES = "<div class='title'> 从第一名、第二名各选一个或多个号码，号码和位置都对应开奖号即中奖\""+ QEZXFS_WIN +"\"元</div>";
	
	//前二直选单式
	var QEZXDS_WIN = pk10Data.getKindPlayLotteryWin("QEZXDS");
	pk10Data.des.QEZXDS_DES = "<div class='title'> 从第一名、第二名各选一个或多个号码，号码和位置都对应开奖号即中奖\""+ QEZXDS_WIN +"\"元</div>";

	//前三复式
	var QSZXFS_WIN = pk10Data.getKindPlayLotteryWin("QSZXFS");
	pk10Data.des.QSZXFS_DES = "<div class='title'>从第一名、第二名、第三名各选一个或多个号码，号码和位置都对应开奖号即中奖\""+ QSZXFS_WIN +"\"元</div>";
	
	//前三单式
	var QSZXDS_WIN = pk10Data.getKindPlayLotteryWin("QSZXDS");
	pk10Data.des.QSZXDS_DES = "<div class='title'>从第一名、第二名、第三名各选一个或多个号码，号码和位置都对应开奖号即中奖\""+ QSZXDS_WIN +"\"元</div>";

	//前四复式
	var QSIZXFS_WIN = pk10Data.getKindPlayLotteryWin("QSIZXFS");
	pk10Data.des.QSIZXFS_DES = "<div class='title'> 从第一名、第二名、第三名、第四名各选一个或多个号码，号码和位置都对应开奖号即中奖，最高奖金\""+ QSIZXFS_WIN +"\"元</div>";

	//前四单式
	var QSIZXDS_WIN = pk10Data.getKindPlayLotteryWin("QSIZXDS");
	pk10Data.des.QSIZXDS_DES = "<div class='title'> 从第一名、第二名、第三名、第四名各选一个或多个号码，号码和位置都对应开奖号即中奖，最高奖金\""+ QSIZXDS_WIN +"\"元</div>";

	//前五复式
	var QWZXFS_WIN = pk10Data.getKindPlayLotteryWin("QWZXFS");
	pk10Data.des.QWZXFS_DES = "<div class='title'> 从第一名、第二名、第三名、第四名、第五名各选一个或多个号码，号码和位置都对应开奖号即中奖，最高奖金\""+ QWZXFS_WIN +"\"元</div>";
	
	//前五单式
	var QWZXDS_WIN = pk10Data.getKindPlayLotteryWin("QWZXDS");
	pk10Data.des.QWZXDS_DES = "<div class='title'> 从第一名、第二名、第三名、第四名、第五名各选一个或多个号码，号码和位置都对应开奖号即中奖，最高奖金\""+ QWZXDS_WIN +"\"元</div>";

	//定位胆
	var DWD_WIN = pk10Data.getKindPlayLotteryWin("DWD");
	pk10Data.des.DWD_DES = "<div class='title'>在任意名次上至少选择一个号码，号码和位置都对应开奖号即中奖。最高奖金\""+ DWD_WIN +"\"元</div>";
	
	//龙虎斗1Vs10
	var LHD1VS10_WIN = pk10Data.getKindPlayLotteryWin("LHD1VS10");
	pk10Data.des.LHD1VS10_DES = "<div class='title'> 开奖结果的位置比较车号大小。位置靠前的车号大于位置靠后的车号则为龙，反之为虎。最高奖金\""+LHD1VS10_WIN +"\"元</div>";
	
	//龙虎斗2Vs9
	var LHD2VS9_WIN = pk10Data.getKindPlayLotteryWin("LHD1VS9");
	pk10Data.des.LHD2VS9_DES = "<div class='title'> 开奖结果的位置比较车号大小。位置靠前的车号大于位置靠后的车号则为龙，反之为虎。最高奖金\""+LHD2VS9_WIN +"\"元</div>";
	
	//龙虎斗3Vs8
	var LHD3VS8_WIN = pk10Data.getKindPlayLotteryWin("LHD3VS8");
	pk10Data.des.LHD3VS8_DES = "<div class='title'> 开奖结果的位置比较车号大小。位置靠前的车号大于位置靠后的车号则为龙，反之为虎。最高奖金\""+LHD3VS8_WIN +"\"元</div>";
	
	//龙虎斗4Vs7
	var LHD4VS7_WIN = pk10Data.getKindPlayLotteryWin("LHD4VS7");
	pk10Data.des.LHD4VS7_DES = "<div class='title'> 开奖结果的位置比较车号大小。位置靠前的车号大于位置靠后的车号则为龙，反之为虎。最高奖金\""+LHD4VS7_WIN +"\"元</div>";
	
	//龙虎斗5Vs6
	var LHD5VS6_WIN = pk10Data.getKindPlayLotteryWin("LHD5VS6");
	pk10Data.des.LHD5VS6_DES = "<div class='title'> 开奖结果的位置比较车号大小。位置靠前的车号大于位置靠后的车号则为龙，反之为虎。最高奖金\""+LHD5VS6_WIN +"\"元</div>";
	
	//大小冠军
	var DXGJ_WIN = pk10Data.getKindPlayLotteryWin("DXGJ");
	pk10Data.des.DXGJ_DES = "<div class='title'>选择大或小进行投注，只要开奖的第一名车号的大小(注：1,2,3,4,5为小；6,7,8,9,10为大)与所选项一致即中奖，最高奖金\""+ DXGJ_WIN +"\"元</div>";
	
	//大小亚军
	var DXYJ_WIN = pk10Data.getKindPlayLotteryWin("DXYJ");
	pk10Data.des.DXYJ_DES = "<div class='title'>选择大或小进行投注，只要开奖的第一名车号的大小(注：1,2,3,4,5为小；6,7,8,9,10为大)与所选项一致即中奖，最高奖金\""+ DXYJ_WIN +"\"元</div>";
	
	//大小季军
	var DXJJ_WIN = pk10Data.getKindPlayLotteryWin("DXJJ");
	pk10Data.des.DXJJ_DES = "<div class='title'>选择大或小进行投注，只要开奖的第一名车号的大小(注：1,2,3,4,5为小；6,7,8,9,10为大)与所选项一致即中奖，最高奖金\""+ DXJJ_WIN +"\"元</div>";
	
	//大小第四名
	var DXDSM_WIN = pk10Data.getKindPlayLotteryWin("DXDSM");
	pk10Data.des.DXDSM_DES = "<div class='title'>选择大或小进行投注，只要开奖的第一名车号的大小(注：1,2,3,4,5为小；6,7,8,9,10为大)与所选项一致即中奖，最高奖金\""+ DXDSM_WIN +"\"元</div>";
	
	//大小第五名
	var DXDWM_WIN = pk10Data.getKindPlayLotteryWin("DXDWM");
	pk10Data.des.DXDWM_DES = "<div class='title'>选择大或小进行投注，只要开奖的第一名车号的大小(注：1,2,3,4,5为小；6,7,8,9,10为大)与所选项一致即中奖，最高奖金\""+ DXDWM_WIN +"\"元</div>";
	
	//单双冠军
	var DSGJ_WIN = pk10Data.getKindPlayLotteryWin("DSGJ");
	pk10Data.des.DSGJ_DES = "<div class='title'>选择单或双进行投注，只要开奖的第一名车号的单双(注：1,3,5,7,9为单；2,4,6,8,10为双)与所选项一致即中奖，最高奖金\""+ DSGJ_WIN +"\"元</div>";
	
	//单双亚军
	var DSYJ_WIN = pk10Data.getKindPlayLotteryWin("DSYJ");
	pk10Data.des.DSYJ_DES = "<div class='title'>选择单或双进行投注，只要开奖的第一名车号的单双(注：1,3,5,7,9为单；2,4,6,8,10为双)与所选项一致即中奖，最高奖金\""+ DSYJ_WIN +"\"元</div>";
	
	//单双季军
	var DSJJ_WIN = pk10Data.getKindPlayLotteryWin("DSJJ");
	pk10Data.des.DSJJ_DES = "<div class='title'>选择单或双进行投注，只要开奖的第一名车号的单双(注：1,3,5,7,9为单；2,4,6,8,10为双)与所选项一致即中奖，最高奖金\""+ DSJJ_WIN +"\"元</div>";
	
	//单双第四名
	var DSDSM_WIN = pk10Data.getKindPlayLotteryWin("DSDSM");
	pk10Data.des.DSDSM_DES = "<div class='title'>选择单或双进行投注，只要开奖的第一名车号的单双(注：1,3,5,7,9为单；2,4,6,8,10为双)与所选项一致即中奖，最高奖金\""+ DSDSM_WIN +"\"元</div>";
	
	//单双第五名
	var DSDWM_WIN = pk10Data.getKindPlayLotteryWin("DSDWM");
	pk10Data.des.DSDWM_DES = "<div class='title'>选择单或双进行投注，只要开奖的第一名车号的单双(注：1,3,5,7,9为单；2,4,6,8,10为双)与所选项一致即中奖，最高奖金\""+ DSDWM_WIN +"\"元</div>";
	
	var HZGYJ_WIN = pk10Data.getKindPlayLotteryWin("HZGYJ");
	pk10Data.des.HZGYJ_DES = "<div class='title'>大小单双玩法:依据开奖号码第一名号大小(1~5为小;6~8为大;1、3、5、7、9为单;2、4、6、8、10为双)；3~19玩法:所选数值等于开奖号码的冠军、第二名二个数字相加之和；龙虎玩法:根据开奖号码第一名和最后一名比较，第一名大于最后一名为龙，反之为虎</div>";
};

/**
 * 任选位置复选框点击事件
 */
Pk10Data.prototype.dsPositionCheckFunc = function(obj){
	var positionCount = 0;
	$("input[name='ds_position']").each(function(){
		if(this.checked){
			positionCount++;
		}
	});
	
    if(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey == 'RXSIZXDS'){
		//判断当前是否符合该单式的处理方式
		if(positionCount == 4){  //选择4位的时候,有一个方案
			pk10Data.rxParam.isRxDsAllow = true;
			pk10Data.rxParam.dsAllowCount = 1;
			$("#pnum").text(positionCount);
			$("#fnum").text(1);  
		}else if(positionCount == 5){
			pk10Data.rxParam.isRxDsAllow = true;
			pk10Data.rxParam.dsAllowCount = 5;
			$("#pnum").text(positionCount);
			$("#fnum").text(5);  
		}else{
			pk10Data.rxParam.isRxDsAllow = false;
			pk10Data.rxParam.dsAllowCount = 0;
			$("#pnum").text(positionCount);
			$("#fnum").text(0);  
		}
    }else if(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey == 'RXEZXDS'){
		//判断当前是否符合该单式的处理方式
		if(positionCount == 2){  //选择2位的时候,有一个方案
			pk10Data.rxParam.isRxDsAllow = true;
			pk10Data.rxParam.dsAllowCount = 1;
			$("#pnum").text(positionCount);
			$("#fnum").text(1);  
		}else if(positionCount == 3){
			pk10Data.rxParam.isRxDsAllow = true;
			pk10Data.rxParam.dsAllowCount = 3;
			$("#pnum").text(positionCount);
			$("#fnum").text(3);  
		}else if(positionCount == 4){
			pk10Data.rxParam.isRxDsAllow = true;
			pk10Data.rxParam.dsAllowCount = 6;
			$("#pnum").text(positionCount);
			$("#fnum").text(6);  
		}else if(positionCount == 5){
			pk10Data.rxParam.isRxDsAllow = true;
			pk10Data.rxParam.dsAllowCount = 10;
			$("#pnum").text(positionCount);
			$("#fnum").text(10);  
		}else{
			pk10Data.rxParam.isRxDsAllow = false;
			pk10Data.rxParam.dsAllowCount = 0;
			$("#pnum").text(positionCount);
			$("#fnum").text(0);  
		}
    }else if(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey == 'RXSZXDS'){
		//判断当前是否符合该单式的处理方式
		if(positionCount == 3){  //选择2位的时候,有一个方案
			pk10Data.rxParam.isRxDsAllow = true;
			pk10Data.rxParam.dsAllowCount = 1;
			$("#pnum").text(positionCount);
			$("#fnum").text(1);  
		}else if(positionCount == 4){
			pk10Data.rxParam.isRxDsAllow = true;
			pk10Data.rxParam.dsAllowCount = 4;
			$("#pnum").text(positionCount);
			$("#fnum").text(4);  
		}else if(positionCount == 5){
			pk10Data.rxParam.isRxDsAllow = true;
			pk10Data.rxParam.dsAllowCount = 10;
			$("#pnum").text(positionCount);
			$("#fnum").text(10);  
		}else{
			pk10Data.rxParam.isRxDsAllow = false;
			pk10Data.rxParam.dsAllowCount = 0;
			$("#pnum").text(positionCount);
			$("#fnum").text(0);  
		}
    }else{
    	showTip("该直选单式未配置");
    }
    
    //格式化,统计该单式数目
    lotteryCommonPage.dealForDsFormat(false);
};
