
function LotteryCommonPage(){}
var lotteryCommonPage = new LotteryCommonPage();

lotteryCommonPage.currentLotteryKindInstance = null;
lotteryCommonPage.param = {
		diffTime : 0,	  //时间倒计时剩余时间	
		currentExpect : null, //当前投注期号
		currentLastOpenExpect : null, //当前最新的开奖期号
		intervalKey : null,  //倒计时周期key
		toFixedValue : 2, //小数点保留4为数字
		isCanActiveExpect : true
}; 

/**
 * 投注参数
 */
lotteryCommonPage.lotteryParam = {
		SOURCECODE_SPLIT : ",",
		SOURCECODE_SPLIT_KS : "&",
		CODE_REPLACE : "-",
		SOURCECODE_ARRANGE_SPLIT : "|",
		currentTotalLotteryCount : 0,
		currentTotalLotteryPrice : 0.0000,
		currentZhuiCodeTotalLotteryPrice : 0.0000, //追号的总共价格
		currentZhuiCodeTotalLotteryCount : 0,
		currentIsUpdateCodeStastic : false,
		updateCodeStasticIndex : null,
		todayTraceList : new Array(),
		tomorrowTraceList : new Array(),
		isZhuiCode : false,  //是否追号
		//currentLotteryWins : new JS_OBJECT_MAP(),  //奖金映射 
	    lotteryMap : new JS_OBJECT_MAP(),  //投注映射
	    lotteryMultipleMap : new JS_OBJECT_MAP(),  //投注倍数映射
	    modelValue : null,
	    awardModel : "YUAN",
	    beishu : 1 //默认是1倍
};

lotteryCommonPage.kindKeyValue={
		summationDescriptionMap:new JS_OBJECT_MAP()
};


/**
 * 系统投注逻辑处理
 */
LotteryCommonPage.prototype.userLottery = function(){
	if($("#lotteryOrderDialogSureButton").attr('disabled') == "disabled") {
		showTip("当前订单还未下注成功，请勿重复提交");
		return false;
	}
	$("#lotteryOrderDialogSureButton").text("提交中.....");
	//追号类型的投注
	if(lotteryCommonPage.lotteryParam.isZhuiCode){
	    lotteryCommonPage.userLotteryForAfterNumber();
	    return;
	}

	//非追号类型的投注
	var arr = lotteryCommonPage.lotteryParam.lotteryMap.keys();
	if(arr.length ==  0 || lotteryCommonPage.lotteryParam.currentTotalLotteryPrice <= 0){
		lotteryCommonPage.hideLotteryMsg();
		showTip("您还没选择投注号码,不能下注的哦.");
		return false;
	}
	var resultArray = new Array();
	for(var i = 0; i < arr.length; i++){
		 //resultArray[i] = new Array();
		 var lotteryKindMap = lotteryCommonPage.lotteryParam.lotteryMap.get(arr[i]);
		 var kindArrs = lotteryKindMap.keys();
		 var count = 0;
		 if(kindArrs.length != 1){
			showTip("投注参数不正确.");
            return;
		 }
		 for(var j = 0; j < kindArrs.length; j++){
			 var playKindStr = kindArrs[j];
			 var playKindCodes = lotteryKindMap.get(kindArrs[j]);
			 var lotteryMultiple = lotteryCommonPage.lotteryParam.lotteryMultipleMap.get(arr[i]);
			 //isZipOpen = false;
			 if(typeof(isZipOpen)=='undefined' || isZipOpen) {
				 //五星单式四星单式字符串压缩处理
				 if(lotteryCommonPage.currentLotteryKindInstance.param.kindName == "CQSSC" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JLFFC"
					 || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "HLJSSC" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JXSSC"
						 || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "TJSSC" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "XJSSC"
						 || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "HGFFC") {
					 if(playKindStr == "WXDS" || playKindStr == "SXDS") {
						 var zip = new JSZip();  
						 //把要压缩的数据以data.txt的文件名加入到压缩包中。
						 zip.file("data.txt", playKindCodes); 
						 //得到压缩后的内容。默认使用DEFLATE压缩算法，并对压缩后的二进制数据进行BASE64编码。
						 var content = zip.generate();
						 //alert("压缩完毕,压缩后内容1:" + content);
						 var contentEncode = encodeURIComponent(content);
						 //alert("压缩完毕,压缩后内容2:" + contentEncode);
						 playKindCodes = contentEncode;
					 }
				 //北京PK10前五直选复式 前四直选复式压缩
				 } else if(lotteryCommonPage.currentLotteryKindInstance.param.kindName == "BJPK10" 
					 || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JSPK10") {
					 if(playKindStr == "QWZXDS" || playKindStr == "QSIZXDS") {
						 var zip = new JSZip();  
						 //把要压缩的数据以data.txt的文件名加入到压缩包中。
						 zip.file("data.txt", playKindCodes); 
						 //得到压缩后的内容。默认使用DEFLATE压缩算法，并对压缩后的二进制数据进行BASE64编码。
						 var content = zip.generate();
						 //alert("压缩完毕,压缩后内容1:" + content);
						 var contentEncode = encodeURIComponent(content);
						 //alert("压缩完毕,压缩后内容2:" + contentEncode);
						 playKindCodes = contentEncode;
					 }
				 }
			 }
				 
			 var resultArrayDetail = {};
			 resultArrayDetail.playKindStr = playKindStr;
			 resultArrayDetail.playKindCodes = playKindCodes;
			 resultArrayDetail.lotteryMultiple = lotteryMultiple;
			 resultArray[i] = resultArrayDetail;
			 
		 };
	}
	
	var lotteryOrder = {};
	lotteryOrder.lotteryKind = lotteryCommonPage.currentLotteryKindInstance.param.kindName;  //投注彩种
	lotteryOrder.model = lotteryCommonPage.lotteryParam.awardModel;
    lotteryOrder.awardModel = lotteryCommonPage.lotteryParam.modelValue;
    lotteryOrder.isLotteryByPoint = 0;
    lotteryOrder.playKindMsgs = resultArray;
    lotteryOrder.expect = lotteryCommonPage.param.currentExpect;
	$.ajax({
		type : "POST",
		url : contextPath + "/lottery/userLottery",
		//data : lotteryOrder,
        data : JSON.stringify(lotteryOrder),
        dataType : "json",//设置请求参数类型
		contentType : 'application/json;charset=utf-8',//设置请求头信息;
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				$("#lotteryOrderDialogSureButton").attr('disabled','disabled');
				lotteryCommonPage.lotteryEnd(); //投注结束动作
				showTip("投注下单成功.");
				lotteryCommonPage.getTodayLotteryByUser();
			} else if (result.code == "error") {
				if(result.data == "BalanceNotEnough") {
					showTip("对不起您的余额不足,请及时充值.");
				} else if(result.data == "BanksIsNull") {
					showTip("对不起,系统检测到您还没添加银行卡信息,为了方便中奖提款,请立即前往绑定.");
				} else {
					if(result.data.indexOf("投注期号不正确.") != -1){
						lotteryCommonPage.getAllExpectsByTodayAndTomorrow(); //重新加载今天和明天的期号				
						showTip(result.data);
					}else{
						showTip(result.data);
					}
				}
			}
		}
	});
};

/**
 * 系统投注逻辑处理(追号投注)
 */
LotteryCommonPage.prototype.userLotteryForAfterNumber = function(){
	var arr = lotteryCommonPage.lotteryParam.lotteryMap.keys();
	if(arr.length ==  0){
		lotteryCommonPage.hideLotteryMsg();
		showTip("请先选择要投注的内容.");
        return;		
	}
	if(lotteryCommonPage.lotteryParam.currentZhuiCodeTotalLotteryPrice <= 0){
		lotteryCommonPage.hideLotteryMsg();
		showTip("未选择追号的期号.");
		return;
	}
	
	var resultArray = new Array();
	for(var i = 0; i < arr.length; i++){
		 //resultArray[i] = new Array();
		 var lotteryKindMap = lotteryCommonPage.lotteryParam.lotteryMap.get(arr[i]);
		 var kindArrs = lotteryKindMap.keys();
		 if(kindArrs.length != 1){
			showTip("投注参数不正确.");
	        return;
		 }
		 var count = 0;
		 for(var j = 0; j < kindArrs.length; j++){
			 var playKindStr = kindArrs[j];
			 var playKindCodes = lotteryKindMap.get(kindArrs[j]);
			 var lotteryMultiple = lotteryCommonPage.lotteryParam.lotteryMultipleMap.get(arr[i]);
			 isZipOpen = false;
			 if(typeof(isZipOpen)=='undefined' || isZipOpen) {
				 //五星单式四星单式字符串压缩处理
				 if(lotteryCommonPage.currentLotteryKindInstance.param.kindName == "CQSSC" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JLFFC"
					 || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "HLJSSC" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JXSSC"
						 || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "TJSSC" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "XJSSC"
						 || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "HGFFC") {
					 if(playKindStr == "WXDS" || playKindStr == "SXDS") {
						 var zip = new JSZip();  
						 //把要压缩的数据以data.txt的文件名加入到压缩包中。
						 zip.file("data.txt", playKindCodes); 
						 //得到压缩后的内容。默认使用DEFLATE压缩算法，并对压缩后的二进制数据进行BASE64编码。
						 var content = zip.generate();
						 //alert("压缩完毕,压缩后内容1:" + content);
						 var contentEncode = encodeURIComponent(content);
						 //alert("压缩完毕,压缩后内容2:" + contentEncode);
						 playKindCodes = contentEncode;
					 }
				 //北京PK10前五直选复式 前四直选复式压缩
				 } else if(lotteryCommonPage.currentLotteryKindInstance.param.kindName == "BJPK10"
					 || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JSPK10") {
					 if(playKindStr == "QWZXDS" || playKindStr == "QSIZXDS") {
						 var zip = new JSZip();  
						 //把要压缩的数据以data.txt的文件名加入到压缩包中。
						 zip.file("data.txt", playKindCodes); 
						 //得到压缩后的内容。默认使用DEFLATE压缩算法，并对压缩后的二进制数据进行BASE64编码。
						 var content = zip.generate();
						 //alert("压缩完毕,压缩后内容1:" + content);
						 var contentEncode = encodeURIComponent(content);
						 //alert("压缩完毕,压缩后内容2:" + contentEncode);
						 playKindCodes = contentEncode;
					 }
				 }
			 }
			 var resultArrayDetail = {};
			 resultArrayDetail.playKindStr = playKindStr;
			 resultArrayDetail.playKindCodes = playKindCodes;
			 resultArrayDetail.lotteryMultiple = lotteryMultiple;
			 resultArray[i] = resultArrayDetail;
			 
//			 resultArray[i][count] = playKindStr;
//			 count++;
//			 resultArray[i][count] = playKindCodes;
//			 count++;
//			 resultArray[i][count] = lotteryMultiple.toString();
		 };
	}
	
	var lotteryOrderForAfterNumber = {};
	lotteryOrderForAfterNumber.lotteryKind = lotteryCommonPage.currentLotteryKindInstance.param.kindName;  //投注彩种
	lotteryOrderForAfterNumber.model = lotteryCommonPage.lotteryParam.awardModel;
	lotteryOrderForAfterNumber.awardModel = lotteryCommonPage.lotteryParam.modelValue;
	lotteryOrderForAfterNumber.isLotteryByPoint = 0;
	lotteryOrderForAfterNumber.playKindMsgs = resultArray; 
	lotteryOrderForAfterNumber.stopAfterNumber = $("#J-trace-iswintimesstop").val();   
	lotteryOrderForAfterNumber.orderExpects = new Array();

	var afterNumberCount = 0;
	$("input[name='trace-expect-input']").each(function(){
		if($(this).val() == 1){
			var lotteryOrderExpect = {};
		    lotteryOrderExpect.afterNumberType = $(this).closest(".main").attr("data-type");
		    lotteryOrderExpect.expect = $(this).attr("data-value");
		    lotteryOrderExpect.multiple = $(this).closest(".line").find("input[name='trace-expect-beishu-input']").val();
		    lotteryOrderExpect.lotteryKind = lotteryCommonPage.currentLotteryKindInstance.param.kindName;
		    lotteryOrderForAfterNumber.orderExpects.push(lotteryOrderExpect);
		    afterNumberCount++;			
		}
	});
	
	if(afterNumberCount == 0){
		lotteryCommonPage.hideLotteryMsg();
		showTip("请选择追号的期数.");
		return;
	}
	
	$.ajax({
		type : "POST",
		url : contextPath + "/lottery/userLotteryForAfterNumber",
		contentType : "application/json",
        data : JSON.stringify(lotteryOrderForAfterNumber),
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				lotteryCommonPage.lotteryEnd(); //投注结束动作
				showTip("投注下单成功.");
			} else if (result.code == "error") {
				if(result.data == "BalanceNotEnough") {
					lotteryCommonPage.hideLotteryMsg();
					comfirm("温馨提示", "对不起您的余额不足,是否要前往充值页面充值?", "取消", function(){}, "确定", function(){
						//调用跳往充值页面事件
						lotteryCommonPage.goToRechargePage();
					});
				} else {
					if(result.data.indexOf("投注期号不正确.") != -1){
						lotteryCommonPage.getAllExpectsByTodayAndTomorrow(); //重新加载今天和明天的期号				
						showTip(result.data);
					}else{
						showTip(result.data);
					}
				}
			}
		}
	});
};


/**
 * 获取系统当前正在投注的期号
 */
LotteryCommonPage.prototype.getActiveExpect = function(){
	if(lotteryCommonPage.param.isCanActiveExpect){  //判断是否可以进行期号的请求
		var jsonData = {
			"lotteryKind" : lotteryCommonPage.currentLotteryKindInstance.param.kindName
		};
		$.ajax({
			type : "POST",
			url : contextPath + "/lotteryinfo/getExpectAndDiffTime",
			data : JSON.stringify(jsonData),
			dataType : "json",
			contentType : 'application/json;charset=utf-8',
			success : function(result) {
				commonHandlerResult(result, this.url);
				if (result.code == "ok") {
					lotteryCommonPage.param.isCanActiveExpect = false;
					var issue = result.data;
					if(issue != null){
						if(issue.specialExpect == 2){
							$("#timer").text("暂停销售");
						}else{
							//展示当前期号
							lotteryCommonPage.currentLotteryKindInstance.showCurrentLotteryCode(issue);
							//判断当前期号是否发生变化
							if(lotteryCommonPage.param.currentExpect == null || lotteryCommonPage.param.currentExpect != issue.lotteryNum) {
								//记录当前期号
								lotteryCommonPage.param.currentExpect = issue.lotteryNum; 
							}
							//当前期号剩余时间倒计时
							lotteryCommonPage.param.diffTime = issue.timeDifference;
							
							window.clearInterval(lotteryCommonPage.param.intervalKey); //终止周期性倒计时
		
							//如果是特殊的时间段
		                    if(issue.specialTime == 1){ //特殊时长
		                    	lotteryCommonPage.currentLotteryKindInstance.calculateTime(); 
		    					lotteryCommonPage.param.intervalKey = setInterval("lotteryCommonPage.currentLotteryKindInstance.calculateTime()",1000);	
		                    }else{
		    					lotteryCommonPage.calculateTime();  //支持最多一个小时
		    					lotteryCommonPage.param.intervalKey = setInterval("lotteryCommonPage.calculateTime()",1000);	
		                    }
						}
					}else{
						//显示预售中
						$("#timer").text("预售中");
					}
					
					if(lotteryCommonPage.param.diffTime > 5000){ //如果剩余时间超过5秒的
						setTimeout("lotteryCommonPage.controlIsCanActive()", 2000);
					}else{
						lotteryCommonPage.param.isCanActiveExpect = true;
					}
				} else if (result.code == "error") {
					showTip(result.data);
				}
			}
		});
		
	}
};

/**
 * 控制是否能发起期号的请求
 */
LotteryCommonPage.prototype.controlIsCanActive = function(){
	lotteryCommonPage.param.isCanActiveExpect = true;
};

//服务器时间倒计时
LotteryCommonPage.prototype.calculateTime = function(){
	var SurplusSecond = lotteryCommonPage.param.diffTime;
	if(SurplusSecond>=3600){
		//显示预售中
		$("#timer").text("预售中");
		window.clearInterval(lotteryCommonPage.param.intervalKey); //终止周期性倒计时
	} else{
		$("#timer").show();
		var m = Math.floor(SurplusSecond/60);
		if (m<10){
			m = "0"+m;
		}
		var s = SurplusSecond%60;
		if(s<10){
			s = "0"+s;
		}

		m = m.toString();
		s = s.toString();
		var timerStr = m + ":" + s;
		$("#timer").text(timerStr);
	}
	lotteryCommonPage.param.diffTime--;
	if(lotteryCommonPage.param.diffTime < 0){
		//弹出窗口控制
		$("#lotteryOrderDialogCancelButton").trigger("click");
		var str = "";
		str += "第";
		str += lotteryCommonPage.currentLotteryKindInstance.getOpenCodeStr(lotteryCommonPage.param.currentExpect);
		str += "期已截止,<br/>";
		str += "投注时请注意期号!";
		
		showTip(str);
		
		window.clearInterval(lotteryCommonPage.param.intervalKey); //终止周期性倒计时
		lotteryCommonPage.getActiveExpect();  //倒计时结束重新请求最新期号
		lotteryCommonPage.getAllExpectsByTodayAndTomorrow(); //重新加载今天和明天的期号
	};
};


/**
 * 获取最新的开奖号码
 */
LotteryCommonPage.prototype.getLastLotteryCode = function(){
	var jsonData =  {
		"lotteryKind" : lotteryCommonPage.currentLotteryKindInstance.param.kindName
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/code/getLastLotteryCode",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var lotteryCode = result.data;
				lotteryCommonPage.currentLotteryKindInstance.showLastLotteryCode(lotteryCode);
				
				var lotteryNum = null;
				if(lotteryCode != null) {
					lotteryNum = lotteryCode.lotteryNum;
				}
				//当开奖号码发生变化
				if(lotteryCommonPage.param.currentLastOpenExpect == null || lotteryCommonPage.param.currentLastOpenExpect != lotteryNum){
					//动画展示开奖号码
					lotteryCommonPage.getLotteryAnimation();
					//重新获取近10期的开奖记录
					lotteryCommonPage.getNearestTenLotteryCode();
				}
				lotteryCommonPage.param.currentLastOpenExpect = lotteryNum;  //存储当前期号	
			} else if (result.code != null && result.code == "error") {
				showTip(result.data);
			}
		}
	});
};

/**
 * 开奖结果动画设置
 */
LotteryCommonPage.prototype.getLotteryAnimation = function(){
	//lottery_run(".lottery_ssc .lottery_run .child-muns",'<span class="ball color-red">1</span><span class="ball color-red">2</span><span class="ball color-red">3</span><span class="ball color-red">3</span><span class="ball color-red">4</span>');
	animate_add($(".lottery_ssc .lottery_run .child-muns").children());
}

/**
 * 获取今日投注数据
 */
LotteryCommonPage.prototype.getTodayLotteryByUser = function(){
	$.ajax({
		type : "POST",
		url : contextPath + "/order/getTodayLotteryByUser",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var todayLotteryList = result.data;
				if(todayLotteryList.length > 0){
					
					//彩种游戏下方的游戏记录
					var userOrderListObj = $("#userTodayLotteryList");
					userOrderListObj.html("");
					
					var str = "";
					str += "<div class='line'>";
					str += "	<div class='child child1'><p>彩种</p></div>";
					str += "	<div class='child child2'><p>期号</p></div>";
					str += "	<div class='child child3'><p>投注金额</p></div>";
					str += "	<div class='child child4'><p>奖金</p></div>";
					str += "	<div class='child child5'><p>状态</p></div>";
					str += "	<div class='child child6'><i class='ion-close-circled'></i></div>";
					str += "</div>";
					userOrderListObj.append(str);
					str = "";
					
					for(var i = 0; i < todayLotteryList.length; i++){
						//显示5条最近投注记录
						if(i > 4) { 
							break;
						}
						var userOrder = todayLotteryList[i];
			    		var lotteryIdStr = userOrder.lotteryId;
			    		
			    		//订单状态特殊处理
			    		var prostateStatus = userOrder.prostateStatusStr;
			    		if(userOrder.prostate == "REGRESSION" || userOrder.prostate == "END_STOP_AFTER_NUMBER" ||
			    				userOrder.prostate == "NO_LOTTERY_BACKSPACE" || userOrder.prostate == "UNUSUAL_BACKSPACE" ||
			    				userOrder.prostate == "STOP_DEALING") {
			    			prostateStatus = "已撤单";
			    		}
			    		
			    		str += "<div class='line' name='lotteryMsgRecord' data-lottery='"+userOrder.lotteryId+"'  data-type='"+userOrder.stopAfterNumber+"' data-value='"+userOrder.id+"'>";
						str += "	<div class='child child1'><p>"+ userOrder.lotteryTypeDes +"</p></div>";
						str += "	<div class='child child2'><p>"+ userOrder.expect +"</p></div>";
						str += "	<div class='child child3'><p>"+ userOrder.payMoney.toFixed(frontCommonPage.param.fixVaue) +"</p></div>";
						str += "	<div class='child child4'><p>"+ userOrder.winCost.toFixed(frontCommonPage.param.fixVaue) +"</p></div>";
						if(userOrder.prostate == 'DEALING'){
							str += "	<div class='child child5'><p class='cp-yellow'>"+ prostateStatus +"</p></div>";
						}else if(userOrder.prostate == 'NOT_WINNING'){
							str += "	<div class='child child5'><p class='cp-red'>"+ prostateStatus +"</p></div>";
						}else {
							str += "	<div class='child child5'><p class='cp-green'>"+ prostateStatus +"</p></div>";
						}
						str += "	<div class='child child6'><i class='ion-close-circled'></i></div>";
						str += "</div>";
						userOrderListObj.append(str);
						str = "";
					}
					
					var afterNumberLotteryWindow;
					
				}else{
					$("#userTodayLotteryList").html("<div class='line'><p class='no-msg'>还没有投注记录哟~</p></div>");
				}
				
				//查看投注详情
				$("a[name='lotteryMsgRecord']").unbind("click").click(function(event){
					var orderId = $(this).attr("data-value");
					var dataType = $(this).attr("data-type");
					var width = 800;
					var height = 400;
					var left = parseInt((screen.availWidth/2) - (width/2));//屏幕居中
					var top = parseInt((screen.availHeight/2) - (height/2));
					var windowFeatures = "width=" + width + ",height=" + height + ",status,resizable,scrollbars=yes,left=" + left + ",top=" + top;
					
					if(dataType == 0){
						var lotteryhrefStr = contextPath + "/gameBet/lotterymsg/"+orderId+"/lotterymsg.html";
					}else{
						var dataLottery = $(this).attr("data-lottery");
						var lotteryhrefStr = contextPath + "/gameBet/lotteryafternumbermsg/"+dataLottery+"/lotteryafternumbermsg.html";
					}
					afterNumberLotteryWindow = window.open(lotteryhrefStr, "subWind", windowFeatures);
					afterNumberLotteryWindow.focus();
				});
			} else if (result.code == "error") {
				showTip(result.data);
			}
		}
	});
}


/**
 * 获取最近十期的开奖号码
 */
LotteryCommonPage.prototype.getNearestTenLotteryCode = function(){
	var jsonData = {
			"lotteryKind" : lotteryCommonPage.currentLotteryKindInstance.param.kindName
	}
	$.ajax({
		type : "POST",
		url : contextPath + "/code/getNearestTenLotteryCode",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var lotteryCodeList = result.data;
				lotteryCommonPage.showNearestTenLotteryCode(lotteryCodeList);
			} else if (result.code == "error") {
				showTip(result.data);
			}
		}
	});
	
}

/**
 * 刷新最近十期的开奖号码的显示
 */
LotteryCommonPage.prototype.showNearestTenLotteryCode = function(lotteryCodeList){
	if(lotteryCodeList != null && lotteryCodeList.length > 0){
		$("#nearestTenLotteryCode").html("");
		var str = "";
		str += "<div class='titles'>";
		str += "	<div class='child child1'><p>期号</p></div>";
		str += "	<div class='child child2'><p>开奖号</p></div>";
		if(lotteryCommonPage.currentLotteryKindInstance.param.kindName != "BJPK10"
			&& lotteryCommonPage.currentLotteryKindInstance.param.kindName != "JSPK10" 
				&& lotteryCommonPage.currentLotteryKindInstance.param.kindName != "FCSDDPC"){
			//快三类型
			if(lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JSKS" || 
						lotteryCommonPage.currentLotteryKindInstance.param.kindName == "AHKS" || 
						lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JLKS" || 
						lotteryCommonPage.currentLotteryKindInstance.param.kindName == "HBKS" || 
						lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JYKS" ||
						lotteryCommonPage.currentLotteryKindInstance.param.kindName == "GXKS" ||
						lotteryCommonPage.currentLotteryKindInstance.param.kindName == "GSKS" ||
						lotteryCommonPage.currentLotteryKindInstance.param.kindName == "SHKS" ||
						lotteryCommonPage.currentLotteryKindInstance.param.kindName == "BJKS"){
				str += "	<div class='child child3'><p>和值</p></div>";
				str += "	<div class='child child4'><p>形态</p></div>";
			//时时彩类型    十一选五暂未处理
			} else {
				str += "	<div class='child child3'><p>后三形态</p></div>";
			}
		}
		str += "</div>";
		
		str += "<div class='content'>";
		for(var i = 0; i < lotteryCodeList.length; i++){
			//只加载5条记录
			if(i > 4) {
				break;
			}
			var lotteryCode = lotteryCodeList[i];
			var openCodeStr = lotteryCommonPage.currentLotteryKindInstance.getOpenCodeStr(lotteryCode.lotteryNum);
			str += "	<div class='line'>";
			str += "		<div class='child child1'><p>"+openCodeStr+"</p></div>";
			
			
			if(lotteryCommonPage.currentLotteryKindInstance.param.kindName != "BJPK10"
				&& lotteryCommonPage.currentLotteryKindInstance.param.kindName != "JSPK10"){
				var codeStatus = lotteryCommonPage.currentLotteryKindInstance.getLotteryCodeStatus(lotteryCode.codesStr);
				//快三类型
				if(lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JSKS" ||
						lotteryCommonPage.currentLotteryKindInstance.param.kindName == "AHKS" || 
						lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JLKS" || 
						lotteryCommonPage.currentLotteryKindInstance.param.kindName == "HBKS" ||
						lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JYKS" ||
						lotteryCommonPage.currentLotteryKindInstance.param.kindName == "GXKS" ||
						lotteryCommonPage.currentLotteryKindInstance.param.kindName == "BJKS"){
					str += "		<div class='child child2 font-blue'>";
					str += "			<img src='"+contextPath+"/images/dice_pic/gray/1.png' alt='' class='dice' id='lotteryNumber1_"+i+"'>";
					str += "			<img src='"+contextPath+"/images/dice_pic/gray/3.png' alt='' class='dice' id='lotteryNumber2_"+i+"'>";
					str += "			<img src='"+contextPath+"/images/dice_pic/gray/6.png' alt='' class='dice' id='lotteryNumber3_"+i+"'>";
					str += "		</div>";
					
					//codeStatus在快3的时候是和值
					str += "<div class='child child3'><p>"+codeStatus+"</p></div>";
				    //获取当前开奖号码的和值组态显示 大小
					str += "<div class='child child4'>";
				    if(codeStatus <= 10){
				    	str += "<span class='child-small'>小</span> ";
				    }else{
				    	str += "<span class='child-big'>大</span> ";
				    }
				    //获取当前开奖号码的和值组态显示 单双
				    if(codeStatus % 2 == 0) {
				    	str += "| <span class='child-even'>双</span></li>";
				    } else {
				    	str += "| <span class='child-odd'>单</span></li>";
				    }
				    str += "</div>";
				 //DPC类型
				}else if(lotteryCommonPage.currentLotteryKindInstance.param.kindName == "FCSDDPC"){
					str += "		<div class='child child2 font-blue muns muns-small'>";
					str += "			<div class='mun color-red'>"+lotteryCode.numInfo1+"</div>";
					str += "			<div class='mun color-red'>"+lotteryCode.numInfo2+"</div>";
					str += "			<div class='mun color-red'>"+lotteryCode.numInfo3+"</div>";
					str += "		</div>";
				//时时彩类型
				}else{
					str += "		<div class='child child2 font-blue muns muns-small'>";
					str += "			<div class='mun color-red'>"+lotteryCode.numInfo1+"</div>";
					str += "			<div class='mun color-red'>"+lotteryCode.numInfo2+"</div>";
					str += "			<div class='mun color-red'>"+lotteryCode.numInfo3+"</div>";
					str += "			<div class='mun color-red'>"+lotteryCode.numInfo4+"</div>";
					str += "			<div class='mun color-red'>"+lotteryCode.numInfo5+"</div>";
					str += "		</div>";
					str += "<div class='child child3'><p>"+codeStatus+"</p></div>";
				}
			} else {
				//pk10
				str += "		<div class='child child2 font-blue muns muns-small'>";
				str += "			<div class='mun color-red'>"+lotteryCode.numInfo1+"</div>";
				str += "			<div class='mun color-red'>"+lotteryCode.numInfo2+"</div>";
				str += "			<div class='mun color-red'>"+lotteryCode.numInfo3+"</div>";
				str += "			<div class='mun color-red'>"+lotteryCode.numInfo4+"</div>";
				str += "			<div class='mun color-red'>"+lotteryCode.numInfo5+"</div>";
				str += "			<div class='mun color-red'>"+lotteryCode.numInfo6+"</div>";
				str += "			<div class='mun color-red'>"+lotteryCode.numInfo7+"</div>";
				str += "			<div class='mun color-red'>"+lotteryCode.numInfo8+"</div>";
				str += "			<div class='mun color-red'>"+lotteryCode.numInfo9+"</div>";
				str += "			<div class='mun color-red'>"+lotteryCode.numInfo10+"</div>";
				str += "		</div>";
			}
			str += "	</div>";
			
		}
		str += "</div>";
		$("#nearestTenLotteryCode").html(str);
		lotteryCommonPage.showLastLottery(lotteryCodeList);
		
	}else{
		$("#nearestTenLotteryCode").html("<ul class='c-list'><span>未加载到开奖记录</span></ul>");
	}
};

/**
 * 快三展示最新的开奖号码的图片
 */
LotteryCommonPage.prototype.showLastLottery = function(lotteryCodeList){
	for(var i = 0; i < lotteryCodeList.length; i++){
		//只加载5条记录
		if(i > 4) {
			break;
		}
		var lotteryCode = lotteryCodeList[i];
		//快三展示最新的开奖号码的图片
		lotteryCommonPage.showLastLotteryPictureCode(lotteryCode,i);
	}
};

/**
 *刷新快三最近三期开奖号码的图片
 */
LotteryCommonPage.prototype.showLastLotteryPictureCode = function(lotteryCode,i){
	if(lotteryCode == null || typeof(lotteryCode)=="undefined" || typeof(i)=="undefined"){
		return;
	}

	var id_a ="";
	var id_b ="";
	var id_c ="";
	if(i == null){
		id_a = "lotteryNumber1";
		id_b = "lotteryNumber2";
		id_c = "lotteryNumber3";
	}else{
		id_a = "lotteryNumber1_"+i;
		id_b = "lotteryNumber2_"+i;
		id_c = "lotteryNumber3_"+i;
	}
	
	
	//先移除开奖号码样式（更新开奖号码）
	$('#'+id_a).attr("src","");
	if(lotteryCode.numInfo1 == "1"){
		$('#'+id_a).attr("src",""+contextPath+"/images/dice_pic/gray/1.png");
	}else if(lotteryCode.numInfo1 == "2"){
		$('#'+id_a).attr("src",""+contextPath+"/images/dice_pic/gray/2.png");
	}else if(lotteryCode.numInfo1 == "3"){
		$('#'+id_a).attr("src",""+contextPath+"/images/dice_pic/gray/3.png");
	}else if(lotteryCode.numInfo1 == "4"){
		$('#'+id_a).attr("src",""+contextPath+"/images/dice_pic/gray/4.png");
	}else if(lotteryCode.numInfo1 == "5"){
		$('#'+id_a).attr("src",""+contextPath+"/images/dice_pic/gray/5.png");
	}else if(lotteryCode.numInfo1 == "6"){
		$('#'+id_a).attr("src",""+contextPath+"/images/dice_pic/gray/6.png");
	}
	
	//先移除开奖号码样式（更新开奖号码）
	$('#'+id_b).attr("src","");
	if(lotteryCode.numInfo2 == "1"){
		$('#'+id_b).attr("src",""+contextPath+"/images/dice_pic/gray/1.png");
	}else if(lotteryCode.numInfo2 == "2"){
		$('#'+id_b).attr("src",""+contextPath+"/images/dice_pic/gray/2.png");
	}else if(lotteryCode.numInfo2 == "3"){
		$('#'+id_b).attr("src",""+contextPath+"/images/dice_pic/gray/3.png");
	}else if(lotteryCode.numInfo2 == "4"){
		$('#'+id_b).attr("src",""+contextPath+"/images/dice_pic/gray/4.png");
	}else if(lotteryCode.numInfo2 == "5"){
		$('#'+id_b).attr("src",""+contextPath+"/images/dice_pic/gray/5.png");
	}else if(lotteryCode.numInfo2 == "6"){
		$('#'+id_b).attr("src",""+contextPath+"/images/dice_pic/gray/6.png");
	}
	
	//先移除开奖号码样式（更新开奖号码）
	$('#'+id_c).attr("src","");
	if(lotteryCode.numInfo3 == "1"){
		$('#'+id_c).attr("src",""+contextPath+"/images/dice_pic/gray/1.png");
	}else if(lotteryCode.numInfo3 == "2"){
		$('#'+id_c).attr("src",""+contextPath+"/images/dice_pic/gray/2.png");
	}else if(lotteryCode.numInfo3 == "3"){
		$('#'+id_c).attr("src",""+contextPath+"/images/dice_pic/gray/3.png");
	}else if(lotteryCode.numInfo3 == "4"){
		$('#'+id_c).attr("src",""+contextPath+"/images/dice_pic/gray/4.png");
	}else if(lotteryCode.numInfo3 == "5"){
		$('#'+id_c).attr("src",""+contextPath+"/images/dice_pic/gray/5.png");
	}else if(lotteryCode.numInfo3 == "6"){
		$('#'+id_c).attr("src",""+contextPath+"/images/dice_pic/gray/6.png");
	}
};


/**
 * 查找奖金对照表
 */
LotteryCommonPage.prototype.getLotteryWins = function(){
	
	$.ajax({
		type : "POST",
		url : contextPath + "/lottery/getLotteryWins",
		async: false,
		data : {
			"lotteryKind" : lotteryCommonPage.currentLotteryKindInstance.param.kindName
		},
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				for (var key in result.data) {  
					//将奖金对照表缓存
					lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.put(key,result.data[key]);
		        }  
				//设置玩法描述数据
				lotteryCommonPage.lottertyDataDeal.setKindPlayDes();
			} else if (result.code == "error") {
				showTip(result.data);
			}
		}
	});
};

/**
 * 查询用户当前的投注模式
 */
LotteryCommonPage.prototype.loadUserModelSet = function(){
	
	$.ajax({
		type : "POST",
		url : contextPath + "/lottery/loadUserModelSet",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var model = result.data.mobileLotteryModel;
				lotteryCommonPage.lotteryParam.awardModel = model;
			} else if (result.code == "error") {
				showTip("加载用户投注模式设置失败，请重试");
			}
		}
	});
}

/**
 * 倍数或者投注模式或者奖金模式改变，投注池更新
 */
LotteryCommonPage.prototype.codeStasticsByBeishuOrAwarModelOrLotteryModel = function(){
	 var codeStastics = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic;
	 var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
	 //更新投注池判断是否含有一星到四星的毫模式注数存在
	 if(lotteryCommonPage.lotteryParam.awardModel == "HAO") {
		 //存储将被移除的序号
		 var deleteCodeStatics = new Array();
		 for(var i = 0; i < codeStastics.length; i++){
			 var codeStastic = codeStastics[i];
			 var sscType = codeStastic[6].substring(0, codeStastic[6].indexOf("_"));
			 var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
			 //星种  截取前两个字符
			 var lotteryKind = kindKey.substr(0, 2); 
			 if(sscType == 'SSC' || sscType == 'FFC') {
				 //一星玩法名称为WXDWD 特殊处理
				 if(kindKey != 'WXFS' && kindKey != 'WXDS') {
					 deleteCodeStatics.push(i);
				 }
			 }
		 }
		 //毫模式注数移除  删除从后往前删
		 if(deleteCodeStatics.length > 0) {
			 for(var i = deleteCodeStatics.length - 1; i >= 0; i--) {
				 lotteryCommonPage.deleteCurrentCodeStastic(deleteCodeStatics[i]);
			 }
			 showTip("当前投注中含有不支持的毫模式玩法的注数,将被移除");
		 }
	 //更新投注池判断是否含有一星到三星的厘模式注数存在
	 } else if (lotteryCommonPage.lotteryParam.awardModel == "LI") {
		 //存储将被移除的序号
		 var deleteCodeStatics = new Array();
		 for(var i = 0; i < codeStastics.length; i++){
			 var codeStastic = codeStastics[i];
			 var sscType = codeStastic[6].substring(0, codeStastic[6].indexOf("_"));
			 var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
			 //星种  截取前两个字符
			 var lotteryKind = kindKey.substr(0, 2); 
			 if(sscType == 'SSC' || sscType == 'FFC') {
				 //非厘模式支持的玩法
				 if(kindKey != 'WXFS' && kindKey != 'WXDS' && kindKey != 'SXFS'&& kindKey != 'SXDS'
					 && kindKey != 'RXSI' && kindKey != 'RXSIZXDS') {
					 deleteCodeStatics.push(i);
				 }
			 } else if(sscType == 'PK10') {
				//非厘模式支持的玩法
				 if(kindKey != 'QWZXDS' && kindKey != 'QWZXFS') {
					 deleteCodeStatics.push(i);
				 }
			 }
		 }
		 //厘模式注数移除  删除从后往前删
		 if(deleteCodeStatics.length > 0) {
			 for(var i = deleteCodeStatics.length - 1; i >= 0; i--) {
				 lotteryCommonPage.deleteCurrentCodeStastic(deleteCodeStatics[i]);
			 }
			 showTip("当前投注中含有不支持厘模式玩法注数,将被移除");
		 }
	 } 
	 
	 if(lotteryCommonPage.lotteryParam.isZhuiCode){
		 var beishu = lotteryCommonPage.lotteryParam.beishu;
		 for(var i = 0; i < codeStastics.length; i++){
			 //更新投注金额
			 var codeStastic = codeStastics[i];
			 codeStastic[2] = beishu;
			 codeStastic[0] = lotteryCommonPage.getAwardByLotteryModel(codeStastic[1] * lotteryCommonPage.currentLotteryKindInstance.param.unitPrice * beishu);
			 //更新最高奖金
		     var currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get(codeStastic[6]);
		     //江苏快三,PK10获取奖金特殊处理,包含和值
		     if(lotteryCommonPage.currentLotteryKindInstance.param.kindName.indexOf("KS") > 0 || lotteryCommonPage.currentLotteryKindInstance.param.kindName == 'BJPK10' 
		    	 || lotteryCommonPage.currentLotteryKindInstance.param.kindName == 'JSPK10') {
		    	 var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
		    	 var kindName = lotteryCommonPage.currentLotteryKindInstance.param.kindName;
		    	 if(kindName.indexOf("KS") > 0){
		    		 currentLotteryWin = lotteryCommonPage.getKSCurrentLotteryWin(kindKey, codeStastic[5]);
		    	 }else if(kindName == 'BJPK10' || kindName == 'JSPK10'){
		    		 currentLotteryWin = lotteryCommonPage.getPK10CurrentLotteryWin(kindName,kindKey, codeStastic[5]);
		    	 }
//		    	 currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.getCurrentLotteryWin(kindKey, codeStastic[5]);
		     }
		     codeStastic[3] = lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * beishu);
		 }
	 }else{
		 for(var i = 0; i < codeStastics.length; i++){
			 //更新投注金额
			 var codeStastic = codeStastics[i];
			 var beishu = codeStastic[2];
			 codeStastic[0] = lotteryCommonPage.getAwardByLotteryModel(codeStastic[1] * lotteryCommonPage.currentLotteryKindInstance.param.unitPrice * beishu);
			 //更新最高奖金
		     var currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get(codeStastic[6]);
		     //江苏快三,PK10获取奖金特殊处理,包含和值
		     if(lotteryCommonPage.currentLotteryKindInstance.param.kindName.indexOf("KS") > 0 || lotteryCommonPage.currentLotteryKindInstance.param.kindName == 'BJPK10' 
		    	 || lotteryCommonPage.currentLotteryKindInstance.param.kindName == 'JSPK10') {
		    	 var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
		    	 var kindName = lotteryCommonPage.currentLotteryKindInstance.param.kindName;
		    	 if(kindName.indexOf("KS") > 0){
		    		 currentLotteryWin = lotteryCommonPage.getKSCurrentLotteryWin(kindKey, codeStastic[5]);
		    	 }else if(kindName == 'BJPK10' || kindName == 'JSPK10'){
		    		 currentLotteryWin = lotteryCommonPage.getPK10CurrentLotteryWin(kindName,kindKey, codeStastic[5]);
		    	 }
//		    	 currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.getCurrentLotteryWin(kindKey, codeStastic[5]);
		     }
		     codeStastic[3] = lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * beishu);
		 }
	 }
	 lotteryCommonPage.codeStastics();
};

/**
 * 快三根据玩法和号码获取奖金
 */
LotteryCommonPage.prototype.getKSCurrentLotteryWin = function(kindKey, codeNum) {
	var currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get(lotteryCommonPage.currentLotteryKindInstance.param.kindNameType +"_"+kindKey);
	if(kindKey == 'HZ') {
		if(codeNum == '3' || codeNum == '18') {
			currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get("KS_HZ");
		} else if(codeNum == '4' || codeNum == '17') {
			currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get("KS_HZ_2");
		} else if(codeNum == '5' || codeNum == '16') {
			currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get("KS_HZ_3");
		} else if(codeNum == '6' || codeNum == '15') {
			currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get("KS_HZ_4");
		} else if(codeNum == '7' || codeNum == '14') {
			currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get("KS_HZ_5");
		} else if(codeNum == '8' || codeNum == '13') {
			currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get("KS_HZ_6");
		} else if(codeNum == '9' || codeNum == '12') {
			currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get("KS_HZ_7");
		} else if(codeNum == '10' || codeNum == '11') {
			currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get("KS_HZ_8");
		}
	}
	return currentLotteryWin;
};

/**
 * 北京PK10,极速PK10  冠亚和值根据玩法和号码获取奖金
 */
LotteryCommonPage.prototype.getPK10CurrentLotteryWin = function(kindName,kindKey, codeNum) {
	var currentLotteryWin ="";
	if(kindName == 'BJPK10'){
		currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get(bjpk10Page.param.kindNameType +"_"+kindKey);
	}else{
		currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get(jspk10Page.param.kindNameType +"_"+kindKey);
	}
	if(kindKey == 'HZGYJ') {
		if(codeNum == '3' || codeNum == '4' ||  codeNum == '18' || codeNum == '19') {
			currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get("PK10_HZGYJ");
		} else if(codeNum == '5' || codeNum == '6' || codeNum == '16' || codeNum == '17') {
			currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get("PK10_HZGYJ_2");
		} else if(codeNum == '7' || codeNum == '8' || codeNum == '14' || codeNum == '15') {
			currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get("PK10_HZGYJ_3");
		} else if(codeNum == '9' || codeNum == '10'|| codeNum == '12' || codeNum == '13') {
			currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get("PK10_HZGYJ_4");
		} else if(codeNum == '11') {
			currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get("PK10_HZGYJ_5");
		}
	}
	return currentLotteryWin;
};


/**
 * 按钮事件添加，重新生成按钮元素后，需要调用此方法
 */
LotteryCommonPage.prototype.dealForEvent = function(){
	
	//号码选择事件
	$("#ballSection .child-content .munbtn").unbind("click").click(function(){
		
		//判断追号模式不可改变投注内容
		if(lotteryCommonPage.lotteryParam.isZhuiCode){
			showTip("追号模式下不可改变投注内容，若需更改，请先取消追号");
			return;
		}
		if((lotteryCommonPage.currentLotteryKindInstance.param.kindName == "BJPK10"
			|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JSPK10") && $(this).hasClass("ball")){
			$(this).toggleClass("color-red");
		}else if((lotteryCommonPage.currentLotteryKindInstance.param.kindName == "BJPK10"
			|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JSPK10") && $(this).hasClass("value")){
			$(this).parent().toggleClass("on");
		}else{
			$(this).toggleClass("on");
		}
		//删除控制该号码按钮事件清空
		$(this).parent().next().find("div").each(function(i){
			$(this).removeClass("on");
		});
		
		//如果是快三和PK10，则需控制同号的选择
		if(lotteryCommonPage.currentLotteryKindInstance.param.kindName.indexOf("KS") > 0
			|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == "BJPK10"
			|| lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JSPK10") {
			lotteryCommonPage.currentLotteryKindInstance.controlNotSameBall(this); 
		}
		
		//对号码进行统计 
		lotteryCommonPage.currentLotteryKindInstance.getCodesByKindKey(); 
	});
	
	//控制号码选择的事件
	$("#ballSection .child-head .mun").unbind("click").click(function(){
		
		//判断追号模式不可改变投注内容
		if(lotteryCommonPage.lotteryParam.isZhuiCode){
			showTip("追号模式下不可改变投注内容，若需更改，请先取消追号");
			return;
		}
		
		if($(this).hasClass("on")){
			return;
		}
		
		//清空大小奇偶按钮的选中状态
		$(this).parent().children().each(function(i){
			$(this).removeClass("on");
		});
		$(this).addClass("on");
		
		//号码的算法处理
		var ballText = $(this).attr("data-role-type");
		var ballContentLis = $(this).closest(".container").find(".child-content .munbtn");
		//先清除所有的选择
        for(var i = 0;i < ballContentLis.length; i++){
     	   var ballContentLi = ballContentLis[i];
     	   $(ballContentLi).removeClass("on");
        }
        
		if(ballText == 'all'){
           for(var i = 0;i < ballContentLis.length; i++){
        	   var ballContentLi = ballContentLis[i];
        	   $(ballContentLi).addClass("on");
           }
		}else if(ballText == 'big'){
			   var currentMax = ballContentLis.length - 1;
			   var divide = currentMax/2;
	           for(var i = 0;i < ballContentLis.length; i++){
	        	   var ballContentLi = ballContentLis[i];
                   if(i >= divide){
    	        	   var numa = $(ballContentLi);
	        		   $(numa).addClass("on");
                   }
	           }
		}else if(ballText == 'small'){
			   var currentMax = ballContentLis.length - 1;
			   var divide = currentMax/2;
	           for(var i = 0;i < ballContentLis.length; i++){
	        	   var ballContentLi = ballContentLis[i];
                   if(i < divide){
    	        	   var numa = $(ballContentLi);
	        		   $(numa).addClass("on");
                   }
	           }
		}else if(ballText == 'odd'){
	           for(var i = 0;i < ballContentLis.length; i++){
	        	   var ballContentLi = ballContentLis[i];
	        	   var numa = $(ballContentLi);
	        	   var numValue = numa.attr("data-realvalue");
	        	   if(numValue%2 != 0){
	        		   $(numa).addClass("on");
	        	   }
	           }
		}else if(ballText == 'even'){
	           for(var i = 0;i < ballContentLis.length; i++){
	        	   var ballContentLi = ballContentLis[i];
	        	   var numa = $(ballContentLi);
	        	   var numValue = numa.attr("data-realvalue");
	        	   if(numValue%2 == 0){
	        		   $(numa).addClass("on");
	        	   }
	           }
		}else if(ballText == 'empty'){

		}
		//对号码进行统计 
		lotteryCommonPage.currentLotteryKindInstance.getCodesByKindKey(); 
	});
	
};


/**
 * 号码统计
 * 号码显示统计,由当前的倍数、奖金模式、投注模式决定,顺序如下
 * 金额\注数\倍数\最高奖金\玩法描述\号码
 */
LotteryCommonPage.prototype.codeStastics = function(){
	 var codeStastics = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic;
	 var codesStasticObj = $("#J-balls-order-container");
	 codesStasticObj.html("");
	 var str = "";
	 var lotteryCount = 0;
	 var lotteryPrice = 0.0;
 
	 for(var i = codeStastics.length - 1; i >= 0 ; i--){
		 var codeStastic = codeStastics[i];
		 str = lotteryCommonPage.currentLotteryKindInstance.codeStastics(codeStastic,i);
		 codesStasticObj.append(str);
		 
		 lotteryCount += codeStastic[1];
		 lotteryPrice += parseFloat(codeStastic[0]);
	 }
	 var totalPrice = lotteryPrice.toFixed(lotteryCommonPage.param.toFixedValue);
	 $("#J-gameOrder-lotterys-num").text(lotteryCount);
	 $("#J-gameOrder-amount").text(totalPrice);
	 lotteryCommonPage.lotteryParam.currentTotalLotteryCount = lotteryCount;
	 lotteryCommonPage.lotteryParam.currentTotalLotteryPrice = totalPrice;  //记录当前的投注总金额
	 
	 //如果追号的话,进行追号的统计
	 if(lotteryCommonPage.lotteryParam.isZhuiCode){
		lotteryCommonPage.statisticsZhuiCode();
	 }
	 
};

/**
 * 从号码统计中删除当前数据
 */
LotteryCommonPage.prototype.deleteCurrentCodeStastic = function(index){
	var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[index];
	if(codeStastic[7] instanceof Array){
		var codeStasticArray = codeStastic[7];
		for(var i = 0; i < codeStasticArray.length; i++){
			var codeStasticRecord = codeStasticArray[i];
			lotteryCommonPage.lotteryParam.lotteryMap.remove(codeStasticRecord);
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStasticRecord);
		}
	}else{
		lotteryCommonPage.lotteryParam.lotteryMap.remove(codeStastic[7]);
		lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStastic[7]);
	}
	
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic.splice(index, 1);
	//重新统计注数金额
	lotteryCommonPage.codeStastics();
    
	//如果是删除正在修改的注码
	if(lotteryCommonPage.lotteryParam.currentIsUpdateCodeStastic &&
			lotteryCommonPage.lotteryParam.updateCodeStasticIndex == index){
		//清空当前选中号码
		lotteryCommonPage.clearCurrentCodes();
	}
};


/**
 * 更新投注号码
 */
LotteryCommonPage.prototype.updateCodeStastic = function(obj,index){
	//样式控制
	$("#J-balls-order-container li").each(function(){
       $(this).removeClass("game-order-current");	   
	});	
	$(obj).addClass("game-order-current");	 
	//清空当前选中号码
	lotteryCommonPage.clearCurrentCodes();
	//设置为修改
	$("#J-add-order").html("<span>改好了</span>");

	var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[index];
	var kindKeyTop = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
	
	//玩法选中
	lotteryCommonPage.lottertyDataDeal.lotteryKindPlayChoose(kindKeyTop);
	
	
	//号码选中
	var codeResult = new Array();
	var codes = codeStastic[5];
	var codesArray = codes.toString().split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT);
	
	//显示当前玩法的号码
	lotteryCommonPage.currentLotteryKindInstance.showCurrentPlayKindCode(codesArray);
	
	//必须放在trigger之后
	lotteryCommonPage.lotteryParam.currentIsUpdateCodeStastic = true;
	lotteryCommonPage.lotteryParam.updateCodeStasticIndex = index;
	
	//对号码进行统计 
	lotteryCommonPage.currentLotteryKindInstance.getCodesByKindKey();  
	
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryCount = codeStastic[1];
	lotteryCommonPage.lotteryParam.beishu = codeStastic[2];
	//显示注码当前倍数
	$("#beishu").val(codeStastic[2]);
	lotteryCommonPage.currentCodesStastic();
};

/**
 * 显示当前投注金额跟注数 
 */
LotteryCommonPage.prototype.currentCodesStastic = function(){
	var beishu = lotteryCommonPage.lotteryParam.beishu;
	//存储当前统计的投注总额
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryPrice = lotteryCommonPage.getAwardByLotteryModel(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryCount * lotteryCommonPage.currentLotteryKindInstance.param.unitPrice * beishu);  
	
	//alert(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryCount)
	
	$('#J-balls-statistics-lotteryNum').text(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryCount);  //显示投注
	$('#J-balls-statistics-amount').text(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryPrice);
	//投注的描述如果超过14个字符
	var codesStr = "";
	if(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStr != null) {
		if(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStr.length > 14) {
			codesStr = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStr.substring(0, 14) + "...";
		} else if(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStr.length == 0){
			codesStr = "(投注内容)";
		} else {
			codesStr = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStr;
		}
	} 
	var kindkey=lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey;
	if(kindkey=="ZHWQLHSH"||kindkey=="ZHDN"||kindkey=="ZHQS"||kindkey=="ZHZS"||kindkey=="ZHHS"){
		codesStr="";
		for(var k=0;k<lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStr.split(",").length;k++){
			codesStr+=lotteryCommonPage.kindKeyValue.summationDescriptionMap.get(kindkey).get(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStr.split(",")[k]);
			if(k<lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStr.split(",").length-1){
				codesStr+=",";
			}
		}
		if(codesStr.length > 14) {
			codesStr = codesStr.substring(0, 14) + "...";
		}else if(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStr.length == 0){
			codesStr = "(投注内容)";
		}
	}
	$('#J-balls-statistics-code').text(codesStr);
	
	//投注数目大于0  则显示倍数栏
	if(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryCount > 0) {
		if($(".lottery-bar .additional").css('display') == "none") {
			$(".lottery-bar .additional").show();
			$("#beishu").focus();
		}
	} else {
		$(".lottery-bar .additional").hide();
	}
};

/**
 * 清空当前选中号码
 */
LotteryCommonPage.prototype.clearCurrentCodes = function(){
	$(".mun").removeClass("on");
	$(".mun2").removeClass("on");
	$(".munbtn").removeClass("on");
	$(".munbtn").removeClass("color-red");
	//单式号码清空
	$("#lr_editor").val("");
	//单式输入框改为可编辑
	$("#lr_editor").removeAttr("readonly");
	
	//取消追号处理
	lotteryCommonPage.lotteryParam.isZhuiCode = true;
	lotteryCommonPage.changeTozhuiCodeEvent();
	
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codes= null;
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStr = "";
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryCount = 0;
	
	//倍数默认1
	$("#beishu").val(1);
	lotteryCommonPage.lotteryParam.beishu = 1; 
	
	lotteryCommonPage.currentCodesStastic();
	
	//切换到添加号码模式
	lotteryCommonPage.lotteryParam.currentIsUpdateCodeStastic = false;
	lotteryCommonPage.lotteryParam.updateCodeStasticIndex = null;
};

/**
 * 清空投注池内容
 */
LotteryCommonPage.prototype.clearCodeStasticEvent = function(){

	lotteryCommonPage.clearCurrentCodes();

	lotteryCommonPage.lotteryParam.lotteryMap.clear();
	lotteryCommonPage.lotteryParam.lotteryMultipleMap.clear();
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic = new Array();
	lotteryCommonPage.codeStastics();
};

/**
 * 获取投注模式下对应的奖金金额
 * 默认按元传递
 */
LotteryCommonPage.prototype.getAwardByLotteryModel = function(award){
    var lotteryModel = lotteryCommonPage.lotteryParam.awardModel;
	if(lotteryModel == 'YUAN'){
	}else if(lotteryModel == 'JIAO'){
		award = award * 0.1;
	}else if(lotteryModel == 'FEN'){
		award = award * 0.01;
	}else if(lotteryModel == 'LI'){
		award = award * 0.001;
	}else if(lotteryModel == 'HAO'){
		award = award * 0.0001;
	}else{
		showTip("未配置对应的投注模式");
	}
	return parseFloat(award).toFixed(lotteryCommonPage.param.toFixedValue);
};

/**
 * 绑定改变倍数事件
 */
LotteryCommonPage.prototype.beishuEvent = function(){
	var beishuElement = document.getElementById("beishu"); 
	if("\v"=="v") {
		beishuElement.attachEvent("onpropertychange",function(e){
			 if(e.propertyName!='value'){
				 return;
			 } 
			 lotteryCommonPage.beishuEventContent();
		});
	}else{ 
		beishuElement.addEventListener("input",lotteryCommonPage.beishuEventContent,false); 
	} 
	
};

/**
 * 倍数值改变处理
 * @returns {Boolean}
 */
LotteryCommonPage.prototype.beishuEventContent = function(){
	var beishuObj = $('#beishu');
    var beishuStr = $('#beishu').val();
	var re = /^[1-9]+[0-9]*]*$/;
    if (!re.test(beishuStr)){
    	//beishuObj.val("1");  //this.value.replace(/[^0-9]/g,'') 1
		//lotteryCommonPage.lotteryParam.beishu = 1;
    	return false;
    }
    var beishuLength = beishuStr.length;
	if(beishuLength > 0){
		 //倍数限制在9999
		 var beishuTemp = parseInt(beishuStr);
		 if(beishuTemp > 1000000){
			 beishuObj.val(1000000);
		 }
		 lotteryCommonPage.lotteryParam.beishu = parseInt(beishuStr);
		 lotteryCommonPage.currentCodesStastic();
	}
};

/**
 * 选好号码的结束动作
 */
LotteryCommonPage.prototype.addCodeEnd = function(){
	
	//清空当前选中号码
	lotteryCommonPage.clearCurrentCodes();
	//号码显示统计,由当前的倍数、奖金模式、投注模式决定
	lotteryCommonPage.codeStastics();
};


/**
 * 投注结束动作
 */
LotteryCommonPage.prototype.lotteryEnd = function(){
	
	lotteryCommonPage.lotteryParam.currentTotalLotteryCount = 0; 
	lotteryCommonPage.lotteryParam.currentTotalLotteryPrice = 0.000; 
	lotteryCommonPage.lotteryParam.currentZhuiCodeTotalLotteryPrice = 0.000; 
	lotteryCommonPage.lotteryParam.currentZhuiCodeTotalLotteryCount = 0; 
	lotteryCommonPage.lotteryParam.currentIsUpdateCodeStastic = false; 
	lotteryCommonPage.lotteryParam.updateCodeStasticIndex = null; 
	lotteryCommonPage.lotteryParam.lotteryMap = new JS_OBJECT_MAP(); 
	lotteryCommonPage.lotteryParam.lotteryMultipleMap = new JS_OBJECT_MAP(); 
	lotteryCommonPage.lotteryParam.beishu = 1; 
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryCount = 0;
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryPrice = 0;
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryTotalCount = 0;
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codes = null;
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic = new Array();

	//倍数还原
	$("#beishu").val("1");
	//追号还原
	lotteryCommonPage.lotteryParam.isZhuiCode = false; 
	$("#beishuSub").show();
	$("#beishuAdd").show();
	$("#beishu").removeAttr("disabled");
	$("#zhuiCodeButton").text("我要追号");
	//隐藏追号内容
	if(!$(".additional-modal").hasClass("out")) {
		modal_toggle('.additional-modal');
	}
	
	$("input[name='trace-expect-input']").each(function(i){
		 if($(this).val() == 1){
			 $(this).parent().trigger("click");
		 }
	 });
	
	//清空投注池内容
	lotteryCommonPage.clearCodeStasticEvent();
	
};

/**
 * 展示投注信息
 */
LotteryCommonPage.prototype.showLotteryMsg = function(){
	
	$("#lotteryKindForOrder").text(lotteryCommonPage.currentLotteryKindInstance.param.kindDes);
	var expectDes = "";
	if(lotteryCommonPage.lotteryParam.isZhuiCode){
		var arr = lotteryCommonPage.lotteryParam.lotteryMap.keys();
		if(arr.length ==  0){
			showTip("请先选择要投注的内容.");
	        return false;		
		}
		if(lotteryCommonPage.lotteryParam.currentZhuiCodeTotalLotteryPrice <= 0){
			showTip("未选择追号的期号.");
			return false;
		}
		var startExpect = $("input[name='trace-expect-input'][value='1']:first").attr("data-value");
		var endExpect = $("input[name='trace-expect-input'][value='1']:last").attr("data-value");
		var totalCount = $("input[name='trace-expect-input'][value='1']").length;
		expectDes = "第 " + startExpect + " 至 " + endExpect + " 共<span style='colr:red;'>" + totalCount + "</span>期";
	}else{
		if(lotteryCommonPage.lotteryParam.currentTotalLotteryPrice <= 0){
			showTip("您还没选择投注号码,不能下注的哦.");
			return false;
		}
		
		//期号控制
		if(lotteryCommonPage.param.currentExpect != null){
			expectDes = "第 " + lotteryCommonPage.param.currentExpect + "期";
		}else{
			expectDes = "<span style='color:#f8a525'>当前无可投注的期号</span>";
		}
	}
	$("#lotteryExpectDesForOrder").html(expectDes);
	
	var codeStr = "";
	var codeStastics = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic;
	for(var i = codeStastics.length - 1; i >= 0 ; i--){
		var codeStastic = codeStastics[i];
	    var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
		if(kindKey == "QEDXDS" || kindKey == "HEDXDS" || kindKey == "DXDS"
			|| kindKey == "DXGJ" || kindKey == "DXYJ" || kindKey == "DXJJ"  || kindKey == "DXDSM" || kindKey == "DXDWM"
			|| kindKey == "DSGJ" || kindKey == "DSYJ" || kindKey == "DSJJ"  || kindKey == "DSDSM" || kindKey == "DSDWM"){
			 var codeDesc = codeStastic[5];
			 codeDesc = codeDesc.replaceAll("1","大");
			 codeDesc = codeDesc.replaceAll("2","小");
			 codeDesc = codeDesc.replaceAll("3","单");
			 codeDesc = codeDesc.replaceAll("4","双");
			codeStr += "("+ codeStastic[4] + ")  " +codeDesc + "\n";
		}else if(kindKey == "LHD1VS10" || kindKey == "LHD2VS9"
			|| kindKey == "LHD3VS8"  || kindKey == "LHD4VS7" || kindKey == "LHD5VS6"){
			 var codeDesc = codeStastic[5];
			 codeDesc = codeDesc.replaceAll("1","龙");
			 codeDesc = codeDesc.replaceAll("2","虎");
			codeStr += "(" + codeStastic[4] + ")  " +codeDesc + "\n";
		}else if(kindKey == "QWDDS"){
			 var codeDesc = codeStastic[5];
			 codeDesc = codeDesc.replaceAll("01","5单0双");
			 codeDesc = codeDesc.replaceAll("02","4单1双");
			 codeDesc = codeDesc.replaceAll("03","3单2双");
			 codeDesc = codeDesc.replaceAll("04","2单3双");
			 codeDesc = codeDesc.replaceAll("05","1单4双");
			 codeDesc = codeDesc.replaceAll("06","0单5双");
			codeStr += "(" + codeStastic[4] + ")  " +codeDesc + "\n";
		}else if(kindKey=="ZHWQLHSH"||kindKey=="ZHDN"||kindKey=="ZHQS"||kindKey=="ZHZS"||kindKey=="ZHHS"){//斗牛
			codeStr+= "(" + codeStastic[4] + ")  ";
			for(var k=0;k<codeStastic[5].split(",").length;k++){
				codeStr+=lotteryCommonPage.kindKeyValue.summationDescriptionMap.get(kindKey).get(codeStastic[5].split(",")[k]) + "\n";
				if(k<codeStastic[5].split(",").length-1){
					codeStr+=",";
				}
			}
		}else{
			codeStr += "(" + codeStastic[4] + ")  " +codeStastic[5] + "\n";
		}
	}
	codeStr = codeStr.replace(/\&/g," ");
	if(codeStr.length > 33) {
		codesStr = codeStr.substring(0, 33) + "...";
	}
	$("#lotteryDetailForOrder").html(codeStr);
	
	var totalPrice = 0.000;
	if(lotteryCommonPage.lotteryParam.isZhuiCode){
		totalPrice = lotteryCommonPage.lotteryParam.currentZhuiCodeTotalLotteryPrice;
	}else{
		totalPrice = lotteryCommonPage.lotteryParam.currentTotalLotteryPrice;
	}
	$("#lotteryTotalPriceForOrder").text(parseFloat(totalPrice).toFixed(lotteryCommonPage.param.toFixedValue));
	$("#lotteryPayAccountForOrder").text(currentUser.username);
	
	//使用毫模式，增加提醒
	if(lotteryCommonPage.lotteryParam.awardModel == "HAO") {
		$("#haoTip").show();
	}
	
	element_toggle(true,['#lotteryOrderDialog']);
	return true;
}

/**
 * 关闭投注信息并清空投注池
 */
LotteryCommonPage.prototype.hideLotteryMsg = function(){
	
	//清空投注池数据
	lotteryCommonPage.lotteryParam.lotteryMap.clear();
	lotteryCommonPage.lotteryParam.lotteryMultipleMap.clear();
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic = new Array();
	
	element_toggle(false,['#lotteryOrderDialog']);
	
	$("#lotteryOrderDialogSureButton").removeAttr("disabled");
	$("#lotteryOrderDialogSureButton").text("确认投注");
}

/**
 * 机选一注事件
 */
LotteryCommonPage.prototype.randomOneEvent = function(){
	
	//清空当前选中的号码
	lotteryCommonPage.clearCurrentCodes();
	
	lotteryCommonPage.lotteryParam.currentTotalLotteryCount += 1;
	lotteryCommonPage.currentLotteryKindInstance.JxCodes(1);
	$('#J-gameOrder-lotterys-num').text(lotteryCommonPage.lotteryParam.currentTotalLotteryCount);
}




/**
 * 点击追号事件
 */
LotteryCommonPage.prototype.changeTozhuiCodeEvent = function(){
	
	//判断当前追号模式
	if(lotteryCommonPage.lotteryParam.isZhuiCode) {
		//取消追号处理
		lotteryCommonPage.lotteryParam.isZhuiCode = false;
		//先删除投注池的号码
		lotteryCommonPage.hideLotteryMsg();
		lotteryCommonPage.currentCodesStastic();
		
		$("#beishuSub").show();
		$("#beishuAdd").show();
		$("#beishu").removeAttr("disabled");
		$("#zhuiCodeButton").text("我要追号");
		
		//单式输入框改为可编辑
		$("#lr_editor").removeAttr("readonly");
		
		//还原注数和价格
		$("#J-gameOrder-lotterys-num").text(lotteryCommonPage.lotteryParam.currentTotalLotteryCount);
		$("#J-gameOrder-amount").text(parseFloat(lotteryCommonPage.lotteryParam.currentTotalLotteryPrice).toFixed(lotteryCommonPage.param.toFixedValue));
		
		//隐藏追号内容
		if(!$(".additional-modal").hasClass("out")) {
			modal_toggle('.additional-modal');
		}
	} else {
		//先处理添加号码到投注池
		var addSuccess = lotteryCommonPage.currentLotteryKindInstance.userLotteryNumForAddOrder();
		if(!addSuccess) {
			return;
		}
		
		//变更为追号处理
		if(lotteryCommonPage.lotteryParam.currentTotalLotteryPrice <= 0){
			showTip("对不起,请先投注,才能追号.");
			return;
		}
		
		if(lotteryCommonPage.lotteryParam.awardModel == "HAO") {
			showTip("毫模式下追号功能暂无法使用");
			return;
		}
		if(lotteryCommonPage.lotteryParam.awardModel == "LI") {
			showTip("厘模式下追号功能暂无法使用");
			return;
		}
		
		$("#zhuiCodeButton").text("取消追号");
		lotteryCommonPage.lotteryParam.isZhuiCode = true;
		
		//单式输入框改为不可编辑
		$("#lr_editor").attr("readonly", "readonly");
		
		//清除当前选中的号码
		//lotteryCommonPage.clearCurrentCodes();
		//
		lotteryCommonPage.lotteryParam.currentTotalLotteryPrice = (lotteryCommonPage.lotteryParam.currentTotalLotteryPrice / lotteryCommonPage.lotteryParam.beishu).toFixed(lotteryCommonPage.param.toFixedValue);
		
		//追号的默认倍数是1
		lotteryCommonPage.lotteryParam.beishu = 1;
		$("#beishu").val(1);
		$("#beishu").attr("disabled", "disabled");
		//隐藏增加倍数
		$("#beishuSub").hide();
		$("#beishuAdd").hide();
		//统计当前投注数目和价格
		lotteryCommonPage.currentCodesStastic();
		$("#beishuEdit").hide();
		$("#beishuStatic").show();
		
		//默认属于中奖停止追号
		$("#J-trace-iswintimesstop").val("1");
		
		//将对应的投注号码倍数设置为1
		var multipleMapArrays = lotteryCommonPage.lotteryParam.lotteryMultipleMap.keys();
		for(var i = 0 ; i < multipleMapArrays.length; i++){
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.set(multipleMapArrays[i],lotteryCommonPage.lotteryParam.beishu);
		}
		
		//默认选中第一期
		var firstExpect = $("#todayList").find(".line:eq(0)");
		//第一期选中
		var checkExpect = firstExpect.find(".checkbox");
		var on_class=checkExpect.attr('data-onclass');
		checkExpect.addClass('on').addClass("color-"+on_class);
		checkExpect.find(".checkbox-value").val(1);
		
		var expectInput = $(firstExpect).find("input[name='trace-expect-input']");
		var currentExpectLabel = $(firstExpect).find(".li-money");
		if(expectInput.length > 0){
			$(firstExpect).find("input[name='trace-expect-beishu-input']").val(1);
			$(firstExpect).find("input[name='trace-expect-beishu-input']").removeAttr("disabled");
			//var currentExpectPrice = (lotteryCommonPage.lotteryParam.currentTotalLotteryPrice / lotteryCommonPage.lotteryParam.beishu).toFixed(lotteryCommonPage.param.toFixedValue);
			var currentExpectPrice = lotteryCommonPage.lotteryParam.currentTotalLotteryPrice;
			$(currentExpectLabel).html("¥"+currentExpectPrice+"元");
		}
		
		
		//更新投注池
		lotteryCommonPage.codeStasticsByBeishuOrAwarModelOrLotteryModel();
		//展示追号内容
		if($(".additional-modal").hasClass("out")) {
			modal_toggle('.additional-modal');
		}
	}
}



/**
 * 加载今天和明天的数据
 */
LotteryCommonPage.prototype.getAllExpectsByTodayAndTomorrow = function(){
	var jsonData = {
		"lotteryKind" : lotteryCommonPage.currentLotteryKindInstance.param.kindName
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/lotteryinfo/getAllExpectsByTodayAndTomorrow",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				lotteryCommonPage.toExchangeTodayAndTomorrowData(result.data);
			} else if (result.code == "error") {
				showTip(result.data);
			}
		}
	})
};

/**
 * 转变今天和明天的数据处理
 * @param resultMaps
 */
LotteryCommonPage.prototype.toExchangeTodayAndTomorrowData = function(resultMaps){
	for(var key in resultMaps){
		var lotteryIssues = resultMaps[key];
		if(key == 'TODAY'){
			lotteryCommonPage.lotteryParam.todayTraceList = lotteryIssues;
			lotteryIssues = lotteryCommonPage.lotteryParam.todayTraceList.slice(0,10);
		}else if(key == 'TOMORROW'){
			lotteryCommonPage.lotteryParam.tomorrowTraceList = lotteryIssues;
			lotteryIssues = lotteryCommonPage.lotteryParam.tomorrowTraceList.slice(0,10);
		}else{
			showTip("未知的追号日期类型");
			break;
		}
		lotteryCommonPage.showAllExpectsByTodayAndTomorrowData(lotteryIssues,key);
	}  
};

/**
 * 展示今天和明天的追号期数
 */
LotteryCommonPage.prototype.showAllExpectsByTodayAndTomorrowData = function(lotteryIssues,key, traceExpectsCount, traceExpectsBeishuCount){
	var todayList = $("#todayList");
	var tomorrowList = $("#tomorrowList");
	var str = "";
	if(key == 'TODAY'){
		todayList.html("");
	}else if(key == 'TOMORROW'){
		tomorrowList.html("");
	}else{
		alert("未知的追号日期类型");
		return;
	}
	if(isEmpty(traceExpectsCount)) {
		traceExpectsCount = 1;
	}
	if(isEmpty(traceExpectsBeishuCount)) {
		traceExpectsBeishuCount = 1;
	}
	
	str += "<div class='titles'>";
	str += "	<div class='child child1'><p><span>序号</span></p></div>";
	str += "	<div class='child child2'><p>";
	str += "		<div class='checkbox' data-onclass='red'>";
	str += "		<input type='hidden' name='trace-expects-input' class='checkbox-value' value='0'>";
	str += "		<img class='icon' src='"+contextPath+"/images/icon/icon-right.png' />";
	str += "		</div>";
	str += "		<span>期数</span>";
	str += "		<input type='text' id='todayTraceExpectsInputCount' name='trace-expects-input-count' class='inputText' value='"+traceExpectsCount+"'>";
	str += "	</p></div>";
	str += "	<div class='child child3'><p>";
	str += "		<input type='text' id='todayTraceExpectsBeishuInputCount' name='trace-expects-beishu-input' class='inputText' value='"+traceExpectsBeishuCount+"'>";
	str += "		<span>倍</span>";
	str += "	</p></div>";
	str += "	<div class='child child4'><p><span>金额</span></p></div>";
	str += "	<div class='child child5'><p><span>投注截止时间</span></p></div>";
	str += "</div>";
	
	for(var i = 0;i < lotteryIssues.length; i++){
	    var lotteryIssue = lotteryIssues[i];
	    var expectDay = lotteryIssue.currentDateStr;
	    var expectNum = lotteryIssue.lotteryNum;
		
		//福彩3D追号期数特殊处理
		var openCodeStr = "";
		if(lotteryIssue.lotteryType != 'FCSDDPC' && lotteryIssue.lotteryType != 'BJPK10' && 
				lotteryIssue.lotteryType != 'HGFFC' && lotteryIssue.lotteryType != 'BJKS'  && 
				lotteryIssue.lotteryType != 'XJPLFC'  && lotteryIssue.lotteryType != 'TWWFC'){
			openCodeStr = expectDay + "-" + expectNum;
		}else{
			openCodeStr = expectNum;
		}
		
		str += "<div class='line'>";
		str += "	<div class='child child1'><p><span>"+ (i + 1) +"</span></p></div>";
		str += "	<div class='child child2'><p>";
		str += "	<div class='checkbox' data-onclass='red'>";
		str += "		<input type='hidden' name='trace-expect-input' data-value='"+openCodeStr+"' class='checkbox-value' value='0'>";
		str += "		<img class='icon' src='"+contextPath+"/images/icon/icon-right.png' />";
		str += "		</div>";
		str += "		<span>"+openCodeStr+"</span>";
		str += "	</p></div>";
		str += "	<div class='child child3'><p>";
		str += "		<input type='text' class='inputText' name='trace-expect-beishu-input' disabled='disabled' value='0'>";
		str += "		<span>倍</span>";
		str += "	</p></div>";
		str += "	<div class='child child4'><p><span class='li-money'>¥0.00元</span></p></div>";
		str += "	<div class='child child5'><p><span>"+lotteryIssue.endtimeStr+"</span></p></div>";
		str += "</div>";
		
		if(key == 'TODAY'){
			todayList.append(str);	
		}else if(key == 'TOMORROW'){
			tomorrowList.append(str);	
		}else{
			showTip("未知的追号日期类型");
			break;
		}
		str = "";
	}
	
	//默认选中第一期
	var firstExpect = $("#todayList").find(".line:eq(0)");
	//第一期选中
	var checkExpect = firstExpect.find(".checkbox");
	var on_class=checkExpect.attr('data-onclass');
	checkExpect.addClass('on').addClass("color-"+on_class);
	checkExpect.find(".checkbox-value").val(1);
	
	var expectInput = $(firstExpect).find("input[name='trace-expect-input']");
	var currentExpectLabel = $(firstExpect).find(".li-money");
	if(expectInput.length > 0){
		$(firstExpect).find("input[name='trace-expect-beishu-input']").val(1);
		$(firstExpect).find("input[name='trace-expect-beishu-input']").removeAttr("disabled");
		var currentExpectPrice = (lotteryCommonPage.lotteryParam.currentTotalLotteryPrice * 1).toFixed(lotteryCommonPage.param.toFixedValue);;
		$(currentExpectLabel).html("¥"+currentExpectPrice+"元");
	}
	
	lotteryCommonPage.dealEventForZhuiCode();  //追号的事件处理
	lotteryCommonPage.statisticsZhuiCode(); //统计追号结果
};


/**
 * 添加追号页面事件
 */
LotteryCommonPage.prototype.dealEventForZhuiCode = function(){
	//追号复选框勾选事件
	$("#J-trace-panel .line .checkbox").unbind("click").click(function(){
		var value,html,on_status,on_class;
		on_class=$(this).attr('data-onclass');
		on_status=$(this).hasClass('on')?0:1;
		$(this).find(".checkbox-value").val(on_status);

		var beishuInput = $(this).closest(".line").find("input[name='trace-expect-beishu-input']");
		var currentExpectLabel = $(this).closest(".line").find(".li-money");
		
		if($(this).hasClass('on')){
			//取消选中
			$(this).removeClass('on').removeClass("color-"+on_class);
			$(beishuInput).val(0);
			$(beishuInput).attr("data-value",0); 
			$(beishuInput).attr('disabled','disabled');
			var currentExpectPrice = parseFloat("0").toFixed(lotteryCommonPage.param.toFixedValue);
			$(currentExpectLabel).html("¥"+currentExpectPrice+"元");
		}else{
			//选中
			$(this).addClass('on').addClass("color-"+on_class);
			if(lotteryCommonPage.lotteryParam.currentTotalLotteryPrice <= 0){
				this.checked = false;
				showTip("对不起,请先投注,才能追号.");
				return;
			}
			$(beishuInput).val("1"); //设置起始倍数为1  
			$(beishuInput).attr("data-value",1); 
			$(beishuInput).removeAttr("disabled");
			var beishu = $(beishuInput).val();
			var currentExpectPrice = lotteryCommonPage.lotteryParam.currentTotalLotteryPrice * beishu;
			currentExpectPrice = currentExpectPrice.toFixed(lotteryCommonPage.param.toFixedValue);
			$(currentExpectLabel).html("¥"+currentExpectPrice+"元");		
		}
		//投注数目和投注金额的统计
		lotteryCommonPage.statisticsZhuiCode();
		
	});
	
	//列表中每一期追号倍数改变的事件
	$("input[name='trace-expect-beishu-input']").each(function(index){
		var beishuExpectElement = this; 
		if("\v"=="v") {
			beishuExpectElement.attachEvent("onpropertychange",function(e){
				 if(e.propertyName!='value'){
					 return;
				 } 
				 lotteryCommonPage.expectBeishuEvent(index);
			});
		}else{ 
			beishuExpectElement.addEventListener("input",function(){
				lotteryCommonPage.expectBeishuEvent(index);
			},false); 
		} 
	});
	
	
	//追号选择所有的期数
	$("#J-trace-panel .titles .checkbox").unbind("click").click(function(){
		if(lotteryCommonPage.lotteryParam.currentTotalLotteryPrice <= 0){
	        this.checked = false;
	        showTip("对不起,请先投注,才能追号.");
			return;
		}
		//勾选未选中的复选框
		var expectInputs = $(this).closest(".main").find(".line .checkbox");
		$(expectInputs).each(function(i){
			if(!$(this).hasClass("on")){
				$(this).trigger("click");
			}
		});
	});	
	
	//追号期号上方总期数改变时，改变追号列表期数事件
	$("input[name='trace-expects-input-count']").each(function(index){
		var todayTraceExpectsInputCountElement = this;
		if("\v"=="v") {
			todayTraceExpectsInputCountElement.attachEvent("onpropertychange",function(e){
				 if(e.propertyName!='value'){
					 return;
				 }
				 lotteryCommonPage.traceExpectsBeishuInputEventForToday(index);
			});
		}else{ 
			todayTraceExpectsInputCountElement.addEventListener("input",function(){
				lotteryCommonPage.traceExpectsBeishuInputEventForToday(index);
			},false); 
		} 
	});
	
	//追号期号上方总倍数改变时，改变追号列表倍数事件
	$("input[name='trace-expects-beishu-input']").each(function(index){
		var expectBeishuElement = this;
		if("\v"=="v") {
			expectBeishuElement.attachEvent("onpropertychange",function(e){
				 if(e.propertyName!='value'){
					 return;
				 }
				 lotteryCommonPage.traceExpectsBeishuInputEvent(index);
			});
		}else{ 
			expectBeishuElement.addEventListener("input",function(){
				lotteryCommonPage.traceExpectsBeishuInputEvent(index);
			},false); 
		} 
	});
	
	//勾选是否中奖停止追号事件
	$("#J-trace-iswintimesstop-btn").unbind();
	$("#J-trace-iswintimesstop-btn").click(function(){
		var value,html,on_status,on_class;
		on_class=$(this).attr('data-onclass');
		on_status=$(this).hasClass('on')?0:1;
		value=$(this).find(".value").eq(on_status).attr("value");
		html=$(this).find(".value").eq(on_status).html();
		if($(this).hasClass('on')){
			$(this).removeClass('on').removeClass("color-"+on_class);
			$(this).find(".ball").removeClass("font-"+on_class);
		}else{
			$(this).addClass('on').addClass("color-"+on_class);
			$(this).find(".ball").addClass("font-"+on_class);
		}
		$(this).find(".ball").html(html);
		$(this).find(".toggle-value").val(value);
	});
};

/**
 * 列表中每一期追号倍数改变的事件
 */
LotteryCommonPage.prototype.expectBeishuEvent = function(index){
	var expectBeiShuObj = $("input[name='trace-expect-beishu-input']").eq(index);
	var expectBeiShuStr = expectBeiShuObj.val();
	
	var re = /^[1-9]+[0-9]*]*$/;
    if (!re.test(expectBeiShuStr)){
    	//var value = $(expectBeiShuObj).val();
    	//$(expectBeiShuObj).val("1");  //value.replace(/[^0-9]/g,'')  1
		//$(expectBeiShuObj).attr("data-value",1); 
    	return false;
    }
    alert(lotteryCommonPage.lotteryParam.lotteryMap.keys().length);
    //倍数限制在9999
	var beishuTemp = parseInt($(expectBeiShuObj).val());
	if(beishuTemp > 9999){
		$(expectBeiShuObj).val(9999);
		$(expectBeiShuObj).attr('data-value',9999);
	 }
	 
	var currentExpectLabel = $(expectBeiShuObj).closest(".line").find(".li-money");
	var beishu = $(expectBeiShuObj).val();
	$(expectBeiShuObj).attr("data-value",beishu); 
	var currentExpectPrice = lotteryCommonPage.lotteryParam.currentTotalLotteryPrice * beishu;
	currentExpectPrice = currentExpectPrice.toFixed(lotteryCommonPage.param.toFixedValue);
	$(currentExpectLabel).html("¥"+currentExpectPrice+"元");
	//统计追号结果
	lotteryCommonPage.statisticsZhuiCode();
};



/**
 * 追号期号上方总期数改变时，改变追号列表期数事件
 */
LotteryCommonPage.prototype.traceExpectsBeishuInputEventForToday = function(index){
	var traceExpectsInputCountObj;
    if(index == 0){
    	todayTraceExpectsInputCountObj = $("#todayTraceExpectsInputCount");
    }else{
    	todayTraceExpectsInputCountObj = $("#tommorrowTraceExpectsInputCount");
    }	
    var todayTraceExpectsInputCountStr = todayTraceExpectsInputCountObj.val();
	
	if(lotteryCommonPage.lotteryParam.currentTotalLotteryPrice <= 0){
        this.checked = false;
        showTip("对不起,请先投注,才能追号.");
		return;
	}
	var re = /^[1-9]+[0-9]*]*$/;
    if (!re.test(todayTraceExpectsInputCountStr)){
    	var value = $(todayTraceExpectsInputCountObj).val();
    	$(todayTraceExpectsInputCountObj).val(value.replace(/[^0-9]/g,''));
    	return false;
    }
	var expectCount = $(todayTraceExpectsInputCountObj).val();
	if(index != 0){
		var lotteryIssues = lotteryCommonPage.lotteryParam.tomorrowTraceList.slice(0,expectCount);
		lotteryCommonPage.showAllExpectsByTodayAndTomorrowData(lotteryIssues,'TOMORROW',expectCount);
	}else {
		var lotteryIssues = lotteryCommonPage.lotteryParam.todayTraceList.slice(0,expectCount);
		lotteryCommonPage.showAllExpectsByTodayAndTomorrowData(lotteryIssues,'TODAY',expectCount);
	}
	
	//这里的期数对象已经被重新生成，需要重新获取才可以
	if(index == 0){
    	todayTraceExpectsInputCountObj = $("#todayTraceExpectsInputCount");
    }else{
    	todayTraceExpectsInputCountObj = $("#tommorrowTraceExpectsInputCount");
    }
	var expectInputs = $(todayTraceExpectsInputCountObj).closest(".main").find(".line .checkbox");
	var count = 0;
	for(var i = 0; i < expectInputs.length; i++){
	   var expectInput = expectInputs[i];
	   if(expectCount >= count){
		   if(!$(expectInput).hasClass("on")){
				$(expectInput).trigger("click");
			}		   
	   }else{
		   break;
	   }
	   count++;
	}
	return true;
};

/**
 * 追号期号上方总倍数改变时，改变追号列表倍数事件
 */
LotteryCommonPage.prototype.traceExpectsBeishuInputEvent = function(index){
	var traceExpectsBeishuInputCountObj;
    if(index == 0){
    	traceExpectsBeishuInputCountObj = $("#todayTraceExpectsBeishuInputCount");
    }else{
    	traceExpectsBeishuInputCountObj = $("#tommorrowTraceExpectsBeishuInputCount");
    }	
    var todayTraceExpectsInputCountStr = traceExpectsBeishuInputCountObj.val();
    
	var re = /^[1-9]+[0-9]*]*$/;
    if (!re.test((todayTraceExpectsInputCountStr))){
    	//var value = $(traceExpectsBeishuInputCountObj).val();
    	//$(traceExpectsBeishuInputCountObj).val("1");  //1
		//$(traceExpectsBeishuInputCountObj).attr("data-value",1);
		return false;
    }
    
    //倍数限制在9999
	var beishuTemp = parseInt($(traceExpectsBeishuInputCountObj).val());
	if(beishuTemp > 9999){
		$(traceExpectsBeishuInputCountObj).val(9999);
		$(traceExpectsBeishuInputCountObj).attr('data-value',9999);
	}
	
	var beishu = $(traceExpectsBeishuInputCountObj).val();
    $(traceExpectsBeishuInputCountObj).attr("data-value",beishu); 
	var beishuInputs = $(traceExpectsBeishuInputCountObj).closest(".main").find("input[name='trace-expect-beishu-input']");
	$(beishuInputs).each(function(index){
		//如果当前期号勾选的话才改变
		var expectCheck = $(this).closest(".line").find(".checkbox");
		if(expectCheck.length > 0 && expectCheck.hasClass("on")) {
			$(this).val(beishu);
			lotteryCommonPage.expectBeishuEvent(index);
		}
	});
};


/**
 * 统计追号的总价格
 */
LotteryCommonPage.prototype.statisticsZhuiCode = function(){
	 //当前有投注
	 if(lotteryCommonPage.lotteryParam.isZhuiCode && lotteryCommonPage.lotteryParam.currentTotalLotteryPrice > 0){
		 lotteryCommonPage.lotteryParam.currentZhuiCodeTotalLotteryPrice = 0.000;
		 lotteryCommonPage.lotteryParam.currentZhuiCodeTotalLotteryCount = 0;
		 
		 var todayDD = $("div[id='todayList']").find(".line");
		 $(todayDD).each(function(i){
		    var expect = $(this).find("input[name='trace-expect-input']");
		    if(expect.val() == 1){
			    var expectBeiShu = $(this).find("input[name='trace-expect-beishu-input']").val();
			    lotteryCommonPage.lotteryParam.currentZhuiCodeTotalLotteryPrice += lotteryCommonPage.lotteryParam.currentTotalLotteryPrice * expectBeiShu; 
			    lotteryCommonPage.lotteryParam.currentZhuiCodeTotalLotteryCount += lotteryCommonPage.lotteryParam.currentTotalLotteryCount;
		    }
		 });

		 var tomorrowDD = $("div[id='tomorrowList']").find(".line");
		 $(tomorrowDD).each(function(i){
		    var expect = $(this).find("input[name='trace-expect-input']");
		    if(expect.val() == 1){
			    var expectBeiShu = $(this).find("input[name='trace-expect-beishu-input']").val();
			    lotteryCommonPage.lotteryParam.currentZhuiCodeTotalLotteryPrice += lotteryCommonPage.lotteryParam.currentTotalLotteryPrice * expectBeiShu; 
			    lotteryCommonPage.lotteryParam.currentZhuiCodeTotalLotteryCount += lotteryCommonPage.lotteryParam.currentTotalLotteryCount;
		    }
		 });
		 
		 lotteryCommonPage.lotteryParam.currentZhuiCodeTotalLotteryPrice = lotteryCommonPage.lotteryParam.currentZhuiCodeTotalLotteryPrice.toFixed(lotteryCommonPage.param.toFixedValue);
		 //$("#J-gameOrder-lotterys-num").text(lotteryCommonPage.lotteryParam.currentZhuiCodeTotalLotteryCount);
		 //$("#J-gameOrder-amount").text(parseFloat(lotteryCommonPage.lotteryParam.currentZhuiCodeTotalLotteryPrice).toFixed(lotteryCommonPage.param.toFixedValue));
		 $("#J-balls-statistics-lotteryNum").text(lotteryCommonPage.lotteryParam.currentZhuiCodeTotalLotteryCount);
		 $("#J-balls-statistics-amount").text(parseFloat(lotteryCommonPage.lotteryParam.currentZhuiCodeTotalLotteryPrice).toFixed(lotteryCommonPage.param.toFixedValue));
	 }
};

/**
 * 添加单式处理的事件
 */
LotteryCommonPage.prototype.addDsEvent = function(){
	
	//获得焦点事件
	$("#lr_editor").focus(function(){
		var editorVal = $("#lr_editor").val();
		if(editorVal.indexOf("说明") != -1){
			   $("#lr_editor").val("");
	    }
	});
	//失去焦点事件
	$("#lr_editor").blur(function(){
		var editorVal = $("#lr_editor").val();
		if(editorVal != null || editorVal != ""){
			lotteryCommonPage.dealForDsFormat(true);
			var editorValFormatAfter = $("#lr_editor").val();
			if(editorValFormatAfter == null || editorValFormatAfter == ""){
				$("#lr_editor").val("");
			}
		}else{
			$("#lr_editor").val("");
		}
	});
	
	var dsEditorElement = document.getElementById("lr_editor");
	if("\v"=="v") {
		dsEditorElement.attachEvent("onpropertychange",function(e){
			 if(e.propertyName!='value'){
				 return;
			 } 
			 lotteryCommonPage.dsInputEvent();
		});
	}else{ 
		dsEditorElement.addEventListener("input",function(){
			lotteryCommonPage.dsInputEvent();
		},false); 
	} 
	
	//删除重复项
	$("#dsRemoveSame").unbind("click").click(function(){
		var pastecontent = $("#lr_editor").val();
		if(pastecontent == ""){
			alert("请先填写投注号码");
			return;
		}
		pastecontent = pastecontent.replace(/\r\n/g,"$");
		pastecontent = pastecontent.replace(/\n/g,"$");
		pastecontent = pastecontent.replace(/\s/g,"");
		pastecontent = pastecontent.replace(/(\$)\1+/g, '$1');
		if(pastecontent.substr(pastecontent.length-1,1)=="$")
		{
			pastecontent = pastecontent.substr(0,pastecontent.length-1);
		}
		var pastecontentArr = pastecontent.split("$");
		var results = new Array();
		for(var i=0;i<pastecontentArr.length;i++){
			var value1 = pastecontentArr[i];
			var isHave = false;
			for(var j = 0; j < results.length; j++){
			   if(results[j] == value1){
				   isHave = true;
				   break;
			   }
			}
			if(!isHave){
				results.push(value1);
			}
		}
		var str = "";
		for(var j = 0; j < results.length; j++){
			str += results[j] + "\n";
	    }
		$("#lr_editor").val(str);
		$("#lr_editor").trigger("blur");
	});
	
	//清空单式内容
	$("#dsRemoveAll").unbind("click").click(function(){
		$("#lr_editor").val("");
		$("#lr_editor").trigger("blur");
	});
}

/**
 * 单式内容输入事件
 */
LotteryCommonPage.prototype.dsInputEvent = function(){
	var editorVal = $("#lr_editor").val();
	if(editorVal != null || editorVal != ""){
		lotteryCommonPage.dealForDsFormat(false);
		var editorValFormatAfter = $("#lr_editor").val();
		if(editorValFormatAfter == null || editorValFormatAfter == ""){
			$("#lr_editor").val("");
		}
	}else{
		$("#lr_editor").val("");
	}
}



/**
 * 单式导入文件
 */
LotteryCommonPage.prototype.uploadFile = function(){ 
	//是否上传服务器进行处理
	var realUpload = false;
	if (typeof FileReader == "undified") { 
		//$.alert("您老的浏览器不行了！,使用上传"); 
		realUpload = true;
	} else {
		var resultFile = document.getElementById("lotteryFile").files[0]; 
		if (resultFile) { 
			var reader = new FileReader(); 
			reader.readAsText(resultFile, "utf-8"); 
			reader.onloadend = function (e) { 
				$("#lr_editor").val(e.target.result);
				$("#lr_editor").trigger("blur");
			} 
			var objFile = document.getElementById("lotteryFile");
			//objFile.outerHTML=objFile.outerHTML.replace(/(value=\").+\"/i,"$1\"");
			objFile.outerHTML += '';   
			objFile.value ="";  
			$('#lotteryFile').on("change", function () {
				lotteryCommonPage.uploadFile();
			});
		} 
	}
	
	if(realUpload) {
	    var file=dwr.util.getValue("lotteryFile");  
	    var fileArr =  new Array();//注意这里是用的集合，无论一个附件还是多个附件都可以了  
	    fileArr[0] = file;  
//	    frontLotteryAction.fileUploadForDwr(fileArr,function(result){
//	    	if(result[0] != null && result[0] == "ok"){
//	        	$("#lr_editor").val(result[1]);
//	    	}else if(result[0] != null && result[0] == "error"){
//				showTip(result.data);
//	    	}
//			var content = "<input name='file' accept='.txt' name='attach' type='file' id='lotteryFile' size='40' hidefocus='true' value='导入' style='outline:none;-ms-filter:progid:DXImageTransform.Microsoft.Alpha(Opacity=0);filter:alpha(opacity=0);opacity: 0;position:absolute;top:0px; left:0px; width:107px; height:30px;z-index:1;background:#000'>";
//			$("#inputFileForLottery").html(content);
//			$('#lotteryFile').on("change", function () {
//				lotteryCommonPage.uploadFile();
//			});
//			$("#lr_editor").trigger("blur");
//	    }); 
	}
}  


/**
 * 单式格式化处理
 */
LotteryCommonPage.prototype.dealForDsFormat = function(isCut){
	var formatResult = lotteryCommonPage.currentLotteryKindInstance.formatNum(isCut); //各个彩种的单式格式化
	if(formatResult){
		var pastecontent = $("#lr_editor").val();
		if(pastecontent != ""){
			pastecontent = pastecontent.replace(/\r\n/g,"$");
			pastecontent = pastecontent.replace(/\n/g,"$");
			pastecontent = pastecontent.replace(/\s/g,"");
			pastecontent = pastecontent.replace(/(\$)\1+/g, '$1');
			if(pastecontent.substr(pastecontent.length-1,1)=="$"){
				pastecontent = pastecontent.substr(0,pastecontent.length-1);
			}
			//单式投注数目统计
			lotteryCommonPage.currentLotteryKindInstance.lotteryCountStasticForDs(pastecontent.split("$").length);
		} else {
			lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryCount = 0;
		}	
		//显示当前预计投注数目和金额
		lotteryCommonPage.currentCodesStastic();
	}
};

/**
 * 号码去重复
 * @param eventType
 * @returns
 */
LotteryCommonPage.prototype.distinct = function(k){
	k = k.replace(/0([1-9])/g, '$1');
	k = k.split(',').sort(function(a,b){return parseInt(a, 10)-parseInt(b, 10)}).join(',')+',';
	var syydj=/(\d+,)\1/;
	return syydj.test(k);
}

/**
 * 左右滑动
 */
$(document).ready(function(){
	
	//左右滑动按钮
	$('.left_but').on('click', function() {
        var scrollLeft = $('.slide-container').scrollLeft();
        $('.slide-container').scrollLeft(scrollLeft - 300);
    })
    $('.right_but').on('click', function() {
        var scrollLeft = $('.slide-container').scrollLeft();
        $('.slide-container').scrollLeft(scrollLeft + 300);
    })
});

/**
 * 选中倍数（金额）光标选中处理
 * @param tag
 * @returns
 */
function onfocusFn(tag) {  
	tag.selectionStart = 0;   //选中区域左边界
	tag.selectionEnd = tag.value.length; //选中区域右边界
    //tag.select();  
} 

/**
 * 阻止safari浏览器默认事件
 * @param eventTag
 * @returns
 */
function onmouseupFn(eventTag) {  
    var event = eventTag||window.event;    
    event.preventDefault();  
}  


var zHWQLHSHMap=new JS_OBJECT_MAP();
zHWQLHSHMap.put("1","大");
zHWQLHSHMap.put("2","小");
zHWQLHSHMap.put("3","单");
zHWQLHSHMap.put("4","双");
zHWQLHSHMap.put("5","五条");
zHWQLHSHMap.put("6","四条");
zHWQLHSHMap.put("7","葫芦");
zHWQLHSHMap.put("8","顺子");
zHWQLHSHMap.put("9","三条");
zHWQLHSHMap.put("10","两对");
zHWQLHSHMap.put("11","一对");
zHWQLHSHMap.put("12","散号");
zHWQLHSHMap.put("13","龙");
zHWQLHSHMap.put("14","虎");
zHWQLHSHMap.put("15","和");
var zHQSMap=new JS_OBJECT_MAP();
zHQSMap.put("0","0");
zHQSMap.put("1","1");
zHQSMap.put("2","2");
zHQSMap.put("3","3");
zHQSMap.put("4","4");
zHQSMap.put("5","5");
zHQSMap.put("6","6");
zHQSMap.put("7","7");
zHQSMap.put("8","8");
zHQSMap.put("9","9");
zHQSMap.put("10","10");
zHQSMap.put("11","11");
zHQSMap.put("12","12");
zHQSMap.put("13","13");
zHQSMap.put("14","14");
zHQSMap.put("15","15");
zHQSMap.put("16","16");
zHQSMap.put("17","17");
zHQSMap.put("18","18");
zHQSMap.put("19","19");
zHQSMap.put("20","20");
zHQSMap.put("21","21");
zHQSMap.put("22","22");
zHQSMap.put("23","23");
zHQSMap.put("24","24");
zHQSMap.put("25","25");
zHQSMap.put("26","26");
zHQSMap.put("27","27");
zHQSMap.put("28","小");
zHQSMap.put("29","大");
zHQSMap.put("30","单");
zHQSMap.put("31","双");
zHQSMap.put("32","0-3");
zHQSMap.put("33","4-7");
zHQSMap.put("34","8-11");
zHQSMap.put("35","12-15");
zHQSMap.put("36","16-19");
zHQSMap.put("37","20-23");
zHQSMap.put("38","24-27");
zHQSMap.put("39","豹子");
zHQSMap.put("40","顺子");
zHQSMap.put("41","对子");
zHQSMap.put("42","半顺");
zHQSMap.put("43","杂六");
var zHDNMap=new JS_OBJECT_MAP();
zHDNMap.put("1","牛1");
zHDNMap.put("2","牛2");
zHDNMap.put("3","牛3");
zHDNMap.put("4","牛4");
zHDNMap.put("5","牛5");
zHDNMap.put("6","牛6");
zHDNMap.put("7","牛7");
zHDNMap.put("8","牛8");
zHDNMap.put("9","牛9");
zHDNMap.put("10","牛牛");
zHDNMap.put("11","牛大");
zHDNMap.put("12","牛小");
zHDNMap.put("13","牛单");
zHDNMap.put("14","牛双");
zHDNMap.put("15","无牛");
lotteryCommonPage.kindKeyValue.summationDescriptionMap.put("ZHWQLHSH",zHWQLHSHMap);
lotteryCommonPage.kindKeyValue.summationDescriptionMap.put("ZHQS",zHQSMap);
lotteryCommonPage.kindKeyValue.summationDescriptionMap.put("ZHZS",zHQSMap);
lotteryCommonPage.kindKeyValue.summationDescriptionMap.put("ZHHS",zHQSMap);
lotteryCommonPage.kindKeyValue.summationDescriptionMap.put("ZHDN",zHDNMap);

/**
 * 余额不足跳往的页面!
 */
LotteryCommonPage.prototype.goToRechargePage = function(){
	setTimeout(function() {
		window.location.href = contextPath + "/gameBet/rechargeList.html";
	}, 0);
}

