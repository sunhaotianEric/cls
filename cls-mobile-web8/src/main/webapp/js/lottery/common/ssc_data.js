function SscData(){}
var sscData = new SscData();

//任选参数
sscData.rxParam = {
	isRxDsAllow : true,  //是否允许任选单式,由位数来进行控制
    dsAllowCount : 0,
};


//通用的号码数组
sscData.common_code_array = new Array(0,1,2,3,4,5,6,7,8,9);
//前三或者后三直选和值
sscData.qszxhz_code_array = new Array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27);
//前三或者后三组选和值
sscData.qszxhz_g_code_array = new Array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26);
//前二或者后二直选和值
sscData.qezxhz_code_array = new Array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18);
//前二或者后二组选和值
sscData.qezxhz_g_code_array = new Array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17);
//大小单双
sscData.dxds_code_array = new Array(1,2,3,4);

//////////////////玩法//////////////////// 
sscData.plays = {};
//五星玩法
sscData.plays.WX_PLAY = "";
sscData.plays.WX_PLAY += "<div class='container clist WX animated' >";
sscData.plays.WX_PLAY += "	<div class='list-line'>";
sscData.plays.WX_PLAY += "		<div class='line-title'><p>直选</p></div>";
sscData.plays.WX_PLAY += "		<div class='line-content'>";
sscData.plays.WX_PLAY += "			<div class='child color-red' data-role-id='WXFS'><p>复式</p></div>";
sscData.plays.WX_PLAY += "			<div class='child color-red no' data-role-id='WXDS'><p>单式</p></div>";
sscData.plays.WX_PLAY += "		</div>";
sscData.plays.WX_PLAY += "	</div>";
sscData.plays.WX_PLAY += "	<div class='list-line'>";
sscData.plays.WX_PLAY += "		<div class='line-title'><p>组选</p></div>";
sscData.plays.WX_PLAY += "		<div class='line-content'>";
sscData.plays.WX_PLAY += "			<div class='child color-red no' data-role-id='WXZX120'><p>五星组选120</p></div>";
sscData.plays.WX_PLAY += "			<div class='child color-red no' data-role-id='WXZX60'><p>五星组选60</p></div>";
sscData.plays.WX_PLAY += "			<div class='child color-red no' data-role-id='WXZX30'><p>五星组选30</p></div>";
sscData.plays.WX_PLAY += "			<div class='child color-red no' data-role-id='WXZX20'><p>五星组选20</p></div>";
sscData.plays.WX_PLAY += "			<div class='child color-red no' data-role-id='WXZX10'><p>五星组选10</p></div>";
sscData.plays.WX_PLAY += "			<div class='child color-red no' data-role-id='WXZX5'><p>五星组选5</p></div>";
sscData.plays.WX_PLAY += "		</div>";
sscData.plays.WX_PLAY += "	</div>";
sscData.plays.WX_PLAY += "	<div class='list-line'>";
sscData.plays.WX_PLAY += "		<div class='line-title'><p>不定位</p></div>";
sscData.plays.WX_PLAY += "		<div class='line-content'>";
sscData.plays.WX_PLAY += "			<div class='child color-red no' data-role-id='WXYMBDW'><p>一码不定位</p></div>";
sscData.plays.WX_PLAY += "			<div class='child color-red no' data-role-id='WXEMBDW'><p>二码不定位</p></div>";
sscData.plays.WX_PLAY += "			<div class='child color-red no' data-role-id='WXSMBDW'><p>三码不定位</p></div>";
sscData.plays.WX_PLAY += "		</div>";
sscData.plays.WX_PLAY += "	</div>";
sscData.plays.WX_PLAY += "</div>";


//四星玩法
sscData.plays.SX_PLAY = "";
sscData.plays.SX_PLAY += "<div class='container clist SX animated'>";
sscData.plays.SX_PLAY += "	<div class='list-line'>";
sscData.plays.SX_PLAY += "		<div class='line-title'><p>直选</p></div>";
sscData.plays.SX_PLAY += "		<div class='line-content'>";
sscData.plays.SX_PLAY += "			<div class='child color-red no' data-role-id='SXFS'><p>复式</p></div>";
sscData.plays.SX_PLAY += "			<div class='child color-red no' data-role-id='SXDS'><p>单式</p></div>";
sscData.plays.SX_PLAY += "		</div>";
sscData.plays.SX_PLAY += "	</div>";
sscData.plays.SX_PLAY += "	<div class='list-line'>";
sscData.plays.SX_PLAY += "		<div class='line-title'><p>组选</p></div>";
sscData.plays.SX_PLAY += "		<div class='line-content'>";
sscData.plays.SX_PLAY += "			<div class='child color-red no' data-role-id='SXZX24'><p>四星组选24</p></div>";
sscData.plays.SX_PLAY += "			<div class='child color-red no' data-role-id='SXZX12'><p>四星组选12</p></div>";
sscData.plays.SX_PLAY += "			<div class='child color-red no' data-role-id='SXZX6'><p>四星组选6</p></div>";
sscData.plays.SX_PLAY += "			<div class='child color-red no' data-role-id='SXZX4'><p>四星组选4</p></div>";
sscData.plays.SX_PLAY += "		</div>";
sscData.plays.SX_PLAY += "	</div>";
sscData.plays.SX_PLAY += "	<div class='list-line'>";
sscData.plays.SX_PLAY += "		<div class='line-title'><p>不定位</p></div>";
sscData.plays.SX_PLAY += "		<div class='line-content'>";
sscData.plays.SX_PLAY += "			<div class='child color-red no' data-role-id='SXYMBDW'><p>一码不定位</p></div>";
sscData.plays.SX_PLAY += "			<div class='child color-red no' data-role-id='SXEMBDW'><p>二码不定位</p></div>";
sscData.plays.SX_PLAY += "		</div>";
sscData.plays.SX_PLAY += "	</div>";
sscData.plays.SX_PLAY += "</div>";

//前三玩法
sscData.plays.QS_PLAY = "";
sscData.plays.QS_PLAY += "<div class='container clist QS animated'>";
sscData.plays.QS_PLAY += "	<div class='list-line'>";
sscData.plays.QS_PLAY += "		<div class='line-title'><p>直选</p></div>";
sscData.plays.QS_PLAY += "		<div class='line-content'>";
sscData.plays.QS_PLAY += "			<div class='child color-red no' data-role-id='QSZXFS'><p>复式</p></div>";
sscData.plays.QS_PLAY += "			<div class='child color-red no' data-role-id='QSZXDS'><p>单式</p></div>";
sscData.plays.QS_PLAY += "			<div class='child color-red no' data-role-id='QSZXHZ'><p>和值</p></div>";
sscData.plays.QS_PLAY += "		</div>";
sscData.plays.QS_PLAY += "	</div>";
sscData.plays.QS_PLAY += "	<div class='list-line'>";
sscData.plays.QS_PLAY += "		<div class='line-title'><p>组选</p></div>";
sscData.plays.QS_PLAY += "		<div class='line-content'>";
sscData.plays.QS_PLAY += "			<div class='child color-red no' data-role-id='QSZXHZ_G'><p>和值</p></div>";
sscData.plays.QS_PLAY += "			<div class='child color-red no' data-role-id='QSZS'><p>组三</p></div>";
sscData.plays.QS_PLAY += "			<div class='child color-red no' data-role-id='QSZL'><p>组六</p></div>";
sscData.plays.QS_PLAY += "		</div>";
sscData.plays.QS_PLAY += "	</div>";
sscData.plays.QS_PLAY += "	<div class='list-line'>";
sscData.plays.QS_PLAY += "		<div class='line-title'><p>不定位</p></div>";
sscData.plays.QS_PLAY += "		<div class='line-content'>";
sscData.plays.QS_PLAY += "			<div class='child color-red no' data-role-id='QSYMBDW'><p>一码不定位</p></div>";
sscData.plays.QS_PLAY += "			<div class='child color-red no' data-role-id='QSEMBDW'><p>二码不定位</p></div>";
sscData.plays.QS_PLAY += "		</div>";
sscData.plays.QS_PLAY += "	</div>";
sscData.plays.QS_PLAY += "</div>";

//后三玩法
sscData.plays.HS_PLAY = "";
sscData.plays.HS_PLAY += "<div class='container clist HS animated'>";
sscData.plays.HS_PLAY += "	<div class='list-line'>";
sscData.plays.HS_PLAY += "		<div class='line-title'><p>直选</p></div>";
sscData.plays.HS_PLAY += "		<div class='line-content'>";
sscData.plays.HS_PLAY += "			<div class='child color-red no' data-role-id='HSZXFS'><p>复式</p></div>";
sscData.plays.HS_PLAY += "			<div class='child color-red no' data-role-id='HSZXDS'><p>单式</p></div>";
sscData.plays.HS_PLAY += "			<div class='child color-red no' data-role-id='HSZXHZ'><p>和值</p></div>";
sscData.plays.HS_PLAY += "		</div>";
sscData.plays.HS_PLAY += "	</div>";
sscData.plays.HS_PLAY += "	<div class='list-line'>";
sscData.plays.HS_PLAY += "		<div class='line-title'><p>组选</p></div>";
sscData.plays.HS_PLAY += "		<div class='line-content'>";
sscData.plays.HS_PLAY += "			<div class='child color-red no' data-role-id='HSZXHZ_G'><p>和值</p></div>";
sscData.plays.HS_PLAY += "			<div class='child color-red no' data-role-id='HSZS'><p>组三</p></div>";
sscData.plays.HS_PLAY += "			<div class='child color-red no' data-role-id='HSZL'><p>组六</p></div>";
sscData.plays.HS_PLAY += "		</div>";
sscData.plays.HS_PLAY += "	</div>";
sscData.plays.HS_PLAY += "	<div class='list-line'>";
sscData.plays.HS_PLAY += "		<div class='line-title'><p>不定位</p></div>";
sscData.plays.HS_PLAY += "		<div class='line-content'>";
sscData.plays.HS_PLAY += "			<div class='child color-red no' data-role-id='HSYMBDW'><p>一码不定位</p></div>";
sscData.plays.HS_PLAY += "			<div class='child color-red no' data-role-id='HSEMBDW'><p>二码不定位</p></div>";
sscData.plays.HS_PLAY += "		</div>";
sscData.plays.HS_PLAY += "	</div>";
sscData.plays.HS_PLAY += "</div>";

//前二玩法
sscData.plays.QE_PLAY = "";
sscData.plays.QE_PLAY += "<div class='container clist QE animated'>";
sscData.plays.QE_PLAY += "	<div class='list-line'>";
sscData.plays.QE_PLAY += "		<div class='line-title'><p>直选</p></div>";
sscData.plays.QE_PLAY += "		<div class='line-content'>";
sscData.plays.QE_PLAY += "			<div class='child color-red no' data-role-id='QEZXFS'><p>复式</p></div>";
sscData.plays.QE_PLAY += "			<div class='child color-red no' data-role-id='QEZXDS'><p>单式</p></div>";
sscData.plays.QE_PLAY += "			<div class='child color-red no' data-role-id='QEZXHZ'><p>和值</p></div>";
sscData.plays.QE_PLAY += "		</div>";
sscData.plays.QE_PLAY += "	</div>";
sscData.plays.QE_PLAY += "	<div class='list-line'>";
sscData.plays.QE_PLAY += "		<div class='line-title'><p>组选</p></div>";
sscData.plays.QE_PLAY += "		<div class='line-content'>";
sscData.plays.QE_PLAY += "			<div class='child color-red no' data-role-id='QEZXFS_G'><p>复式</p></div>";
sscData.plays.QE_PLAY += "			<div class='child color-red no' data-role-id='QEZXDS_G'><p>单式</p></div>";
sscData.plays.QE_PLAY += "			<div class='child color-red no' data-role-id='QEZXHZ_G'><p>和值</p></div>";
sscData.plays.QE_PLAY += "		</div>";
sscData.plays.QE_PLAY += "	</div>";
sscData.plays.QE_PLAY += "</div>";

//后二玩法
sscData.plays.HE_PLAY = "";
sscData.plays.HE_PLAY += "<div class='container clist HE animated'>";
sscData.plays.HE_PLAY += "	<div class='list-line'>";
sscData.plays.HE_PLAY += "		<div class='line-title'><p>直选</p></div>";
sscData.plays.HE_PLAY += "		<div class='line-content'>";
sscData.plays.HE_PLAY += "			<div class='child color-red no' data-role-id='HEZXFS'><p>复式</p></div>";
sscData.plays.HE_PLAY += "			<div class='child color-red no' data-role-id='HEZXDS'><p>单式</p></div>";
sscData.plays.HE_PLAY += "			<div class='child color-red no' data-role-id='HEZXHZ'><p>和值</p></div>";
sscData.plays.HE_PLAY += "		</div>";
sscData.plays.HE_PLAY += "	</div>";
sscData.plays.HE_PLAY += "	<div class='list-line'>";
sscData.plays.HE_PLAY += "		<div class='line-title'><p>组选</p></div>";
sscData.plays.HE_PLAY += "		<div class='line-content'>";
sscData.plays.HE_PLAY += "			<div class='child color-red no' data-role-id='HEZXFS_G'><p>复式</p></div>";
sscData.plays.HE_PLAY += "			<div class='child color-red no' data-role-id='HEZXDS_G'><p>单式</p></div>";
sscData.plays.HE_PLAY += "			<div class='child color-red no' data-role-id='HEZXHZ_G'><p>和值</p></div>";
sscData.plays.HE_PLAY += "		</div>";
sscData.plays.HE_PLAY += "	</div>";
sscData.plays.HE_PLAY += "</div>";

//一星玩法
sscData.plays.YX_PLAY = "";
sscData.plays.YX_PLAY += "<div class='container clist YX animated'>";
sscData.plays.YX_PLAY += "	<div class='list-line'>";
sscData.plays.YX_PLAY += "		<div class='line-title'><p>直选</p></div>";
sscData.plays.YX_PLAY += "		<div class='line-content'>";
sscData.plays.YX_PLAY += "			<div class='child color-red no' data-role-id='WXDWD'><p>定位胆</p></div>";
sscData.plays.YX_PLAY += "		</div>";
sscData.plays.YX_PLAY += "	</div>";
sscData.plays.YX_PLAY += "</div>";

//任选
sscData.plays.RX_PLAY = "";
sscData.plays.RX_PLAY += "<div class='container clist RX animated'>";
sscData.plays.RX_PLAY += "	<div class='list-line'>";
sscData.plays.RX_PLAY += "		<div class='line-title'><p>任选二</p></div>";
sscData.plays.RX_PLAY += "		<div class='line-content'>";
sscData.plays.RX_PLAY += "			<div class='child color-red no' data-role-id='RXE'><p>直选复式</p></div>";
sscData.plays.RX_PLAY += "			<div class='child color-red no' data-role-id='RXEZXDS'><p>直选单式</p></div>";
sscData.plays.RX_PLAY += "		</div>";
sscData.plays.RX_PLAY += "	</div>";
sscData.plays.RX_PLAY += "	<div class='list-line'>";
sscData.plays.RX_PLAY += "		<div class='line-title'><p>任选三</p></div>";
sscData.plays.RX_PLAY += "		<div class='line-content'>";
sscData.plays.RX_PLAY += "			<div class='child color-red no' data-role-id='RXS'><p>直选复式</p></div>";
sscData.plays.RX_PLAY += "			<div class='child color-red no' data-role-id='RXSZXDS'><p>直选单式</p></div>";
sscData.plays.RX_PLAY += "		</div>";
sscData.plays.RX_PLAY += "	</div>";
sscData.plays.RX_PLAY += "	<div class='list-line'>";
sscData.plays.RX_PLAY += "		<div class='line-title'><p>任选四</p></div>";
sscData.plays.RX_PLAY += "		<div class='line-content'>";
sscData.plays.RX_PLAY += "			<div class='child color-red no' data-role-id='RXSI'><p>直选复式</p></div>";
sscData.plays.RX_PLAY += "			<div class='child color-red no' data-role-id='RXSIZXDS'><p>直选单式</p></div>";
sscData.plays.RX_PLAY += "		</div>";
sscData.plays.RX_PLAY += "	</div>";
sscData.plays.RX_PLAY += "</div>";

//大小单双
sscData.plays.DXDS_PLAY = "";
sscData.plays.DXDS_PLAY += "<div class='container clist DXDS animated'>";
sscData.plays.DXDS_PLAY += "	<div class='list-line'>";
sscData.plays.DXDS_PLAY += "		<div class='line-title'><p>直选</p></div>";
sscData.plays.DXDS_PLAY += "		<div class='line-content'>";
sscData.plays.DXDS_PLAY += "			<div class='child color-red no' data-role-id='QEDXDS'><p>前二大小单双</p></div>";
sscData.plays.DXDS_PLAY += "			<div class='child color-red no' data-role-id='HEDXDS'><p>后二大小单双</p></div>";
sscData.plays.DXDS_PLAY += "		</div>";
sscData.plays.DXDS_PLAY += "	</div>";
sscData.plays.DXDS_PLAY += "</div>";

//斗牛
sscData.plays.ZH_PLAY = "";
sscData.plays.ZH_PLAY += "<div class='container clist ZH animated'>";
sscData.plays.ZH_PLAY += "	<div class='list-line'>";
sscData.plays.ZH_PLAY += "		<div class='line-title'><p>直选</p></div>";
sscData.plays.ZH_PLAY += "		<div class='line-content'>";
sscData.plays.ZH_PLAY += "			<div class='child color-red no' data-role-id='ZHWQLHSH'><p>五球总和/龙虎/梭哈</p></div>";
sscData.plays.ZH_PLAY += "			<div class='child color-red no' data-role-id='ZHDN'><p>斗牛</p></div>";
sscData.plays.ZH_PLAY += "			<div class='child color-red no' data-role-id='ZHQS'><p>前三总和</p></div>";
sscData.plays.ZH_PLAY += "			<div class='child color-red no' data-role-id='ZHZS'><p>中三总和</p></div>";
sscData.plays.ZH_PLAY += "			<div class='child color-red no' data-role-id='ZHHS'><p>后三总和</p></div>";
sscData.plays.ZH_PLAY += "		</div>";
sscData.plays.ZH_PLAY += "	</div>";
sscData.plays.ZH_PLAY += "</div>";

//////////////////玩法描述////////////////////
sscData.des = {};
//五星
sscData.des.WXFS_DES = "玩法加载中，请稍后........"; 
sscData.des.WXDS_DES = "玩法加载中，请稍后........"; 
sscData.des.WXZX120_DES = "玩法加载中，请稍后........";
sscData.des.WXZX60_DES = "玩法加载中，请稍后........";
sscData.des.WXZX30_DES = "玩法加载中，请稍后........";
sscData.des.WXZX20_DES = "玩法加载中，请稍后........";
sscData.des.WXZX10_DES = "玩法加载中，请稍后........";
sscData.des.WXZX5_DES = "玩法加载中，请稍后........";
sscData.des.WXYMBDW_DES = "玩法加载中，请稍后........";
sscData.des.WXEMBDW_DES  = "玩法加载中，请稍后........";
sscData.des.WXSMBDW_DES = "玩法加载中，请稍后........";


//四星
sscData.des.SXFS_DES = "玩法加载中，请稍后........";
sscData.des.SXDS_DES = "玩法加载中，请稍后........";
sscData.des.SXZX24_DES = "玩法加载中，请稍后........";
sscData.des.SXZX12_DES = "玩法加载中，请稍后........";
sscData.des.SXZX6_DES = "玩法加载中，请稍后........";
sscData.des.SXZX4_DES = "玩法加载中，请稍后........";
sscData.des.SXYMBDW_DES = "玩法加载中，请稍后........";
sscData.des.SXEMBDW_DES = "玩法加载中，请稍后........";


//前三
sscData.des.QSZXFS_DES = "玩法加载中，请稍后........";
sscData.des.QSZXDS_DES = "玩法加载中，请稍后........";
sscData.des.QSZXHZ_DES = "玩法加载中，请稍后........";
sscData.des.QSZXHZ_G_DES = "玩法加载中，请稍后........";
sscData.des.QSZS_DES = "玩法加载中，请稍后........";
sscData.des.QSZL_DES = "玩法加载中，请稍后........";
sscData.des.QSYMBDW_DES = "玩法加载中，请稍后........";
sscData.des.QSEMBDW_DES = "玩法加载中，请稍后........";


//后三
sscData.des.HSZXFS_DES = "玩法加载中，请稍后........";
sscData.des.HSZXDS_DES = "玩法加载中，请稍后........";
sscData.des.HSZXHZ_DES = "玩法加载中，请稍后........";
sscData.des.HSZXHZ_G_DES = "玩法加载中，请稍后........";
sscData.des.HSZS_DES = "玩法加载中，请稍后........";
sscData.des.HSZL_DES = "玩法加载中，请稍后........";
sscData.des.HSYMBDW_DES = "玩法加载中，请稍后........";
sscData.des.HSEMBDW_DES = "玩法加载中，请稍后........";


//前二
sscData.des.QEZXFS_DES = "玩法加载中，请稍后........";
sscData.des.QEZXDS_DES = "玩法加载中，请稍后........";
sscData.des.QEZXHZ_DES = "玩法加载中，请稍后........";
sscData.des.QEZXFS_G_DES = "玩法加载中，请稍后........";
sscData.des.QEZXDS_G_DES = "玩法加载中，请稍后........";
sscData.des.QEZXHZ_G_DES = "玩法加载中，请稍后........";


//后二
sscData.des.HEZXFS_DES = "玩法加载中，请稍后........";
sscData.des.HEZXDS_DES = "玩法加载中，请稍后........";
sscData.des.HEZXHZ_DES = "玩法加载中，请稍后........";
sscData.des.HEZXFS_G_DES = "玩法加载中，请稍后........";
sscData.des.HEZXDS_G_DES = "玩法加载中，请稍后........";
sscData.des.HEZXHZ_G_DES = "玩法加载中，请稍后........";

//一星前一
sscData.des.YXQY_DES = "玩法加载中，请稍后........";
sscData.des.YXHY_DES = "玩法加载中，请稍后........";


//前二大小单双
sscData.des.QEDXDS_DES = "玩法加载中，请稍后........";
sscData.des.HEDXDS_DES = "玩法加载中，请稍后........";


//任选一
sscData.des.RXY_DES = "玩法加载中，请稍后........";
sscData.des.RXE_DES = "玩法加载中，请稍后........";
sscData.des.RXS_DES = "玩法加载中，请稍后........";
sscData.des.RXSI_DES = "玩法加载中，请稍后........";
sscData.des.RXSIZXDS_DES = "玩法加载中，请稍后........";
sscData.des.RXEZXDS_DES = "玩法加载中，请稍后........";
sscData.des.RXSZXDS_DES = "玩法加载中，请稍后........";

//总和斗牛
sscData.des.ZHWQLHSH_DES = "玩法加载中，请稍后........";
sscData.des.ZHQS_DES = "玩法加载中，请稍后........";
sscData.des.ZHZS_DES = "玩法加载中，请稍后........";
sscData.des.ZHHS_DES = "玩法加载中，请稍后........";
sscData.des.ZHDN_DES = "玩法加载中，请稍后........";

//玩法描述映射值
sscData.lotteryKindMap = new JS_OBJECT_MAP();
sscData.lotteryKindMap.put("WXFS", "五星复式");
sscData.lotteryKindMap.put("WXDS", "五星单式");
sscData.lotteryKindMap.put("WXZX120", "五星组选120");
sscData.lotteryKindMap.put("WXZX60", "五星组选60");
sscData.lotteryKindMap.put("WXZX30", "五星组选30");
sscData.lotteryKindMap.put("WXZX20", "五星组选20");
sscData.lotteryKindMap.put("WXZX10", "五星组选10");
sscData.lotteryKindMap.put("WXZX5", "五星组选5");
sscData.lotteryKindMap.put("WXYMBDW", "五星一码不定位");
sscData.lotteryKindMap.put("WXEMBDW", "五星二码不定位");
sscData.lotteryKindMap.put("WXSMBDW", "五星三码不定位");

sscData.lotteryKindMap.put("SXFS", "四星复式");
sscData.lotteryKindMap.put("SXDS", "四星单式");
sscData.lotteryKindMap.put("SXZX24", "四星组选24");
sscData.lotteryKindMap.put("SXZX12", "四星组选12");
sscData.lotteryKindMap.put("SXZX6", "四星组选6");
sscData.lotteryKindMap.put("SXZX4", "四星组选4");
sscData.lotteryKindMap.put("SXYMBDW", "四星一码不定位");
sscData.lotteryKindMap.put("SXEMBDW", "四星二码不定位");

sscData.lotteryKindMap.put("QSZXFS", "前三复式");
sscData.lotteryKindMap.put("QSZXDS", "前三单式");
sscData.lotteryKindMap.put("QSZXHZ", "前三直选和值");
sscData.lotteryKindMap.put("QSZXHZ_G", "前三组选和值");
sscData.lotteryKindMap.put("QSZS", "前三组三");
sscData.lotteryKindMap.put("QSZL", "前三组六");
sscData.lotteryKindMap.put("QSYMBDW", "前三一码不定位");
sscData.lotteryKindMap.put("QSEMBDW", "前三二码不定位");

sscData.lotteryKindMap.put("HSZXFS", "后三复式");
sscData.lotteryKindMap.put("HSZXDS", "后三单式");
sscData.lotteryKindMap.put("HSZXHZ", "后三直选和值");
sscData.lotteryKindMap.put("HSZXHZ_G", "后三组选和值");
sscData.lotteryKindMap.put("HSZS", "后三组三");
sscData.lotteryKindMap.put("HSZL", "后三组六");
sscData.lotteryKindMap.put("HSYMBDW", "后三一码不定位");
sscData.lotteryKindMap.put("HSEMBDW", "后三二码不定位");

sscData.lotteryKindMap.put("QEZXFS", "前二直选复式");
sscData.lotteryKindMap.put("QEZXDS", "前二直选单式");
sscData.lotteryKindMap.put("QEZXHZ", "前二直选和值");
sscData.lotteryKindMap.put("QEZXFS_G", "前二组选复式");
sscData.lotteryKindMap.put("QEZXDS_G", "前二组选单式");
sscData.lotteryKindMap.put("QEZXHZ_G", "前二组选和值");

sscData.lotteryKindMap.put("HEZXFS", "后二直选复式");
sscData.lotteryKindMap.put("HEZXDS", "后二直选单式");
sscData.lotteryKindMap.put("HEZXHZ", "后二直选和值");
sscData.lotteryKindMap.put("HEZXFS_G", "后二组选复式");
sscData.lotteryKindMap.put("HEZXDS_G", "后二组选单式");
sscData.lotteryKindMap.put("HEZXHZ_G", "后二组选和值");

sscData.lotteryKindMap.put("WXDWD", "定位胆");
//sscData.lotteryKindMap.put("YXQY", "一星前一");
//sscData.lotteryKindMap.put("YXHY", "一星后一");

sscData.lotteryKindMap.put("QEDXDS", "大小单双前二复式");
sscData.lotteryKindMap.put("HEDXDS", "大小单双后二复式");

//sscData.lotteryKindMap.put("RXY", "任选一");
sscData.lotteryKindMap.put("RXSIZXDS", "任选四直选单式");
sscData.lotteryKindMap.put("RXEZXDS", "任选二直选单式");
sscData.lotteryKindMap.put("RXSZXDS", "任选三直选单式");
sscData.lotteryKindMap.put("RXE", "任选二直选复式");
sscData.lotteryKindMap.put("RXS", "任选三直选复式");
sscData.lotteryKindMap.put("RXSI", "任选四直选复式");

sscData.lotteryKindMap.put("ZHWQLHSH", "五球/龙虎/梭哈");//斗牛
sscData.lotteryKindMap.put("ZHQS", "前三总和");
sscData.lotteryKindMap.put("ZHZS", "中三总和");
sscData.lotteryKindMap.put("ZHHS", "后三总和");
sscData.lotteryKindMap.put("ZHDN", "斗牛");

/**
 * 加载所有的玩法
 */
SscData.prototype.loadLotteryKindPlay = function() {
	$("#lotteryKindPlayList").append(sscData.plays.WX_PLAY);
	$("#lotteryKindPlayList").append(sscData.plays.SX_PLAY);
	$("#lotteryKindPlayList").append(sscData.plays.QS_PLAY);
	$("#lotteryKindPlayList").append(sscData.plays.HS_PLAY);
	$("#lotteryKindPlayList").append(sscData.plays.QE_PLAY);
	$("#lotteryKindPlayList").append(sscData.plays.HE_PLAY);
	$("#lotteryKindPlayList").append(sscData.plays.YX_PLAY);
	$("#lotteryKindPlayList").append(sscData.plays.RX_PLAY);
	$("#lotteryKindPlayList").append(sscData.plays.DXDS_PLAY);
	$("#lotteryKindPlayList").append(sscData.plays.ZH_PLAY);//斗牛
}


/**
 * 玩法选择显示描述
 * @param roleId
 */
SscData.prototype.showPlayDes = function(roleId) {
	var playDes = "";
	if (roleId == 'WXFS') {
		playDes = sscData.des.WXFS_DES;
	} else if (roleId == 'WXDS') {
		playDes = sscData.des.WXDS_DES;
	} else if (roleId == 'WXZX120') {
		playDes = sscData.des.WXZX120_DES;
	} else if (roleId == 'WXZX60') {
		playDes = sscData.des.WXZX60_DES;
	} else if (roleId == 'WXZX30') {
		playDes = sscData.des.WXZX30_DES;
	} else if (roleId == 'WXZX20') {
		playDes = sscData.des.WXZX20_DES;
	} else if (roleId == 'WXZX10') {
		playDes = sscData.des.WXZX10_DES;
	} else if (roleId == 'WXZX5') {
		playDes = sscData.des.WXZX5_DES;
	} else if (roleId == 'SXFS') {
		playDes = sscData.des.SXFS_DES;
	} else if (roleId == 'SXDS') {
		playDes = sscData.des.SXDS_DES;
	} else if (roleId == 'SXZX24') {
		playDes = sscData.des.SXZX24_DES;
	} else if (roleId == 'SXZX12') {
		playDes = sscData.des.SXZX12_DES;
	} else if (roleId == 'SXZX6') {
		playDes = sscData.des.SXZX6_DES;
	} else if (roleId == 'SXZX4') {
		playDes = sscData.des.SXZX4_DES;
	} else if (roleId == 'QSZXFS') {
		playDes = sscData.des.QSZXFS_DES;
	} else if (roleId == 'QSZXDS') {
		playDes = sscData.des.QSZXDS_DES;
	} else if (roleId == 'QSZXHZ') {
		playDes = sscData.des.QSZXHZ_DES;
	} else if (roleId == 'QSZXHZ_G') {
		playDes = sscData.des.QSZXHZ_G_DES;
	} else if (roleId == 'QSZS') {
		playDes = sscData.des.QSZS_DES;
	} else if (roleId == 'QSZL') {
		playDes = sscData.des.QSZL_DES;
	} else if (roleId == 'HSZXFS') {
		playDes = sscData.des.HSZXFS_DES;
	} else if (roleId == 'HSZXDS') {
		playDes = sscData.des.HSZXDS_DES;
	} else if (roleId == 'HSZXHZ') {
		playDes = sscData.des.HSZXHZ_DES;
	} else if (roleId == 'HSZXHZ_G') {
		playDes = sscData.des.HSZXHZ_G_DES;
	} else if (roleId == 'HSZS') {
		playDes = sscData.des.HSZS_DES;
	} else if (roleId == 'HSZL') {
		playDes = sscData.des.HSZL_DES;
	} else if (roleId == 'QEZXFS') {
		playDes = sscData.des.QEZXFS_DES;
	} else if (roleId == 'QEZXDS') {
		playDes = sscData.des.QEZXDS_DES;
	} else if (roleId == 'QEZXHZ') {
		playDes = sscData.des.QEZXHZ_DES;
	} else if (roleId == 'QEZXFS_G') {
		playDes = sscData.des.QEZXFS_G_DES;
	} else if (roleId == 'QEZXDS_G') {
		playDes = sscData.des.QEZXDS_G_DES;
	} else if (roleId == 'QEZXHZ_G') {
		playDes = sscData.des.QEZXHZ_G_DES;
	} else if (roleId == 'HEZXFS') {
		playDes = sscData.des.HEZXFS_DES;
	} else if (roleId == 'HEZXDS') {
		playDes = sscData.des.HEZXDS_DES;
	} else if (roleId == 'HEZXHZ') {
		playDes = sscData.des.HEZXHZ_DES;
	} else if (roleId == 'HEZXFS_G') {
		playDes = sscData.des.HEZXFS_G_DES;
	} else if (roleId == 'HEZXDS_G') {
		playDes = sscData.des.HEZXDS_G_DES;
	} else if (roleId == 'HEZXHZ_G') {
		playDes = sscData.des.HEZXHZ_G_DES;
	} else if (roleId == 'YXQY') {
		playDes = sscData.des.YXQY_DES;
	} else if (roleId == 'YXHY') {
		playDes = sscData.des.YXHY_DES;
	} else if (roleId == 'WXYMBDW') {
		playDes = sscData.des.WXYMBDW_DES;
	} else if (roleId == 'WXEMBDW') {
		playDes = sscData.des.WXEMBDW_DES;
	} else if (roleId == 'WXSMBDW') {
		playDes = sscData.des.WXSMBDW_DES;
	} else if (roleId == 'SXYMBDW') {
		playDes = sscData.des.SXYMBDW_DES;
	} else if (roleId == 'SXEMBDW') {
		playDes = sscData.des.SXEMBDW_DES;
	} else if (roleId == 'QSYMBDW') {
		playDes = sscData.des.QSYMBDW_DES;
	} else if (roleId == 'QSEMBDW') {
		playDes = sscData.des.QSEMBDW_DES;
	} else if (roleId == 'HSYMBDW') {
		playDes = sscData.des.HSYMBDW_DES;
	} else if (roleId == 'HSEMBDW') {
		playDes = sscData.des.HSEMBDW_DES;
	} else if (roleId == 'QEDXDS') {
		playDes = sscData.des.QEDXDS_DES;
	} else if (roleId == 'HEDXDS') {
		playDes = sscData.des.HEDXDS_DES;
	} else if (roleId == 'WXDWD') {
		playDes = sscData.des.WXDWD_DES;
	} else if (roleId == 'RXE') {
		playDes = sscData.des.RXE_DES;
	} else if (roleId == 'RXS') {
		playDes = sscData.des.RXS_DES;
	} else if (roleId == 'RXSI') {
		playDes = sscData.des.RXSI_DES;
	} else if (roleId == 'RXSIZXDS') {
		playDes = sscData.des.RXSIZXDS_DES;
	} else if (roleId == 'RXEZXDS') {
		playDes = sscData.des.RXEZXDS_DES;
	} else if (roleId == 'RXSZXDS') {
		playDes = sscData.des.RXSZXDS_DES;
	} else if(roleId =='ZHWQLHSH' ){//斗牛
		playDes = sscData.des.ZHWQLHSH_DES;
	} else if(roleId =='ZHDN' ){
		playDes = sscData.des.ZHDN_DES;
	} else if(roleId =='ZHQS' ){
		playDes = sscData.des.ZHQS_DES;
	} else if(roleId =='ZHZS' ){
		playDes = sscData.des.ZHZS_DES;
	} else if(roleId =='ZHHS' ){
		playDes = sscData.des.ZHHS_DES;
	} else {
		showTip("不可知的彩种玩法描述.");
		return;
	}
	$("#playDes").html(playDes);
};

/**
 * 玩法选择
 * @param roleId
 */
SscData.prototype.lotteryKindPlayChoose = function(roleId) {

	var desArray = new Array("万位", "千位", "百位", "十位", "个位");
	var emptyDesArray = new Array("");
	if (roleId == 'WXFS') {
		sscData.ballSectionCommon(0, 4, desArray, null, true);
	} else if (roleId == 'WXDS') {
		sscData.dsBallSectionCommon(roleId);
	} else if (roleId == 'WXZX120') {
		sscData.ballSectionCommon(0, 0, emptyDesArray, null, false);
	} else if (roleId == 'WXZX60') {
		desArray = new Array("二重号位", "单号位");
		sscData.ballSectionCommon(0, 1, desArray, null, false);
	} else if (roleId == 'WXZX30') {
		desArray = new Array("二重号位", "单号位");
		sscData.ballSectionCommon(0, 1, desArray, null, false);
	} else if (roleId == 'WXZX20') {
		desArray = new Array("三重号位", "单号位");
		sscData.ballSectionCommon(0, 1, desArray, null, false);
	} else if (roleId == 'WXZX10') {
		desArray = new Array("三重号位", "二重号位");
		sscData.ballSectionCommon(0, 1, desArray, null, false);
	} else if (roleId == 'WXZX5') {
		desArray = new Array("四重号位", "单号位");
		sscData.ballSectionCommon(0, 1, desArray, null, false);
	} else if (roleId == 'WXYMBDW' || roleId == 'WXEMBDW'
			|| roleId == 'WXSMBDW') {
		sscData.ballSectionCommon(0, 0, emptyDesArray, null, false);
	} else if (roleId == 'SXFS') {
		sscData.ballSectionCommon(1, 4, desArray, null, true);
	} else if (roleId == 'SXDS') {
		sscData.dsBallSectionCommon(roleId);
	} else if (roleId == 'SXZX24') {
		sscData.ballSectionCommon(0, 0, emptyDesArray, null, false);
	} else if (roleId == 'SXZX12') {
		desArray = new Array("二重号位", "单号位");
		sscData.ballSectionCommon(0, 1, desArray, null, false);
	} else if (roleId == 'SXZX6') {
		desArray = new Array("二重号位");
		sscData.ballSectionCommon(0, 0, desArray, null, false);
	} else if (roleId == 'SXZX4') {
		desArray = new Array("三重号位", "单号位");
		sscData.ballSectionCommon(0, 1, desArray, null, false);
	} else if (roleId == 'SXYMBDW' || roleId == 'SXEMBDW') {
		sscData.ballSectionCommon(0, 0, emptyDesArray, null, false);
	} else if (roleId == 'QSZXFS') {
		sscData.ballSectionCommon(0, 2, desArray, null, true);
	} else if (roleId == 'QSZXDS') {
		sscData.dsBallSectionCommon(roleId);
	} else if (roleId == 'QSZXHZ') {
		sscData.ballSectionCommon(0, 0, emptyDesArray, roleId, false);
	} else if (roleId == 'QSZXHZ_G') {
		desArray = new Array("选号区");
		sscData.ballSectionCommon(0, 0, desArray, roleId, false);
	} else if (roleId == 'QSZS' || roleId == 'QSZL') {
		sscData.ballSectionCommon(0, 0, emptyDesArray, null, false);
	} else if (roleId == 'QSYMBDW' || roleId == 'QSEMBDW') {
		sscData.ballSectionCommon(0, 0, emptyDesArray, null, false);
	} else if (roleId == 'HSZXFS') {
		sscData.ballSectionCommon(2, 4, desArray, null, true);
	} else if (roleId == 'HSZXDS') {
		sscData.dsBallSectionCommon(roleId);
	} else if (roleId == 'HSZXHZ') {
		sscData.ballSectionCommon(0, 0, emptyDesArray, roleId, false);
	} else if (roleId == 'HSZXHZ_G') {
		sscData.ballSectionCommon(0, 0, emptyDesArray, roleId, false);
	} else if (roleId == 'HSZS' || roleId == 'HSZL') {
		sscData.ballSectionCommon(0, 0, emptyDesArray, null, false);
	} else if (roleId == 'HSYMBDW' || roleId == 'HSEMBDW') {
		sscData.ballSectionCommon(0, 0, emptyDesArray, null, false);
	} else if (roleId == 'QEZXFS') {
		sscData.ballSectionCommon(0, 1, desArray, null, false);
	} else if (roleId == 'QEZXDS') {
		sscData.dsBallSectionCommon(roleId);
	} else if (roleId == 'QEZXHZ') {
		sscData.ballSectionCommon(0, 0, emptyDesArray, roleId, false);
	} else if (roleId == 'QEZXFS_G') {
		sscData.ballSectionCommon(0, 0, emptyDesArray, null, false);
	} else if (roleId == 'QEZXDS_G') {
		sscData.dsBallSectionCommon(roleId);
	} else if (roleId == 'QEZXHZ_G') {
		sscData.ballSectionCommon(0, 0, emptyDesArray, roleId, false);
	} else if (roleId == 'HEZXFS') {
		sscData.ballSectionCommon(3, 4, desArray, null, true);
	} else if (roleId == 'HEZXDS') {
		sscData.dsBallSectionCommon(roleId);
	} else if (roleId == 'HEZXHZ') {
		sscData.ballSectionCommon(0, 0, emptyDesArray, roleId, false);
	} else if (roleId == 'HEZXFS_G') {
		sscData.ballSectionCommon(0, 0, emptyDesArray, null, false);
	} else if (roleId == 'HEZXDS_G') {
		sscData.dsBallSectionCommon(roleId);
	} else if (roleId == 'HEZXHZ_G') {
		sscData.ballSectionCommon(0, 0, emptyDesArray, roleId, false);
	} else if (roleId == 'YXQY') {
		sscData.ballSectionCommon(0, 0, desArray, null, true);
	} else if (roleId == 'YXHY') {
		sscData.ballSectionCommon(4, 4, desArray, null, true);
	} else if (roleId == 'QEDXDS') {
		desArray = new Array("万位", "千位");
		sscData.ballSectionCommon(0, -1, desArray, roleId, false);
	} else if (roleId == 'HEDXDS') {
		desArray = new Array("十位", "个位");
		sscData.ballSectionCommon(0, -1, desArray, roleId, false);
	} else if (roleId == 'WXDWD' || roleId == 'RXE' || roleId == 'RXS'
			|| roleId == 'RXSI') {
		sscData.ballSectionCommon(0, 4, desArray, null, true);
	} else if (roleId == 'RXSIZXDS') { // 任选四直选单式
		sscData.dsBallSectionCommon(roleId);
	} else if (roleId == 'RXEZXDS') { // 任选四直选单式
		sscData.dsBallSectionCommon(roleId);
	} else if (roleId == 'RXSZXDS') { // 任选四直选单式
		sscData.dsBallSectionCommon(roleId);
	} else if(roleId =='ZHWQLHSH' ){//斗牛
		sscData.zhBallSectionCommon(roleId);
	} else if(roleId =='ZHDN' ){
		sscData.zhBallSectionCommon(roleId);
	} else if(roleId =='ZHQS' ){
		sscData.zhBallSectionCommon(roleId);
	} else if(roleId =='ZHZS' ){
		sscData.zhBallSectionCommon(roleId);
	} else if(roleId =='ZHHS' ){
		sscData.zhBallSectionCommon(roleId);
	}else {
		showTip("不可知的彩种玩法.");
	}
	
	//存储当前切换的玩法模式
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey = roleId;
	
	//显示玩法名称
	var currentKindName = sscData.lotteryKindMap.get(roleId);
    $("#currentKindName").text(currentKindName);

	//显示玩法说明
	sscData.showPlayDes(roleId);
	
	//拼接完，进行事件的重新申明
	lotteryCommonPage.dealForEvent();
	//获取当前的玩法并赋值赔率  斗牛
	var kindKey=lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey;
	var kindNameType=lotteryCommonPage.currentLotteryKindInstance.param.kindNameType;
	var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
	if(kindKey=="ZHWQLHSH"||kindKey=="ZHDN"){
		$("p[id^="+kindNameType+"_"+kindKey+"]").each(function(i,idx){
			var currentLotteryWin=lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get($(idx).attr("id"));
			var pl=parseFloat((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode)/2).toFixed(3);
			pl=pl.substring(0,pl.length-1);
			$(this).html("赔率："+pl);
		});
	}else if(kindKey=="ZHQS"||kindKey=="ZHZS"||kindKey=="ZHHS"){
		/*$("span[id^=SSC_"+kindKey+"]").each(function(i,idx){
			var currentLotteryWin=lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get($(idx).attr("id"));
			$(this).html("赔率："+currentLotteryWin.winMoney);
		});*/
		$("p[id^="+kindNameType+"_"+kindKey+"]").each(function(i,idx){
			var currentLotteryWin=lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get($(idx).attr("id"));
			var pl=parseFloat((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode)/2).toFixed(3);
			pl=pl.substring(0,pl.length-1);
			$(this).html("赔率："+pl);
		});
	}
};





/**
 * 复式玩法的号码拼接
 * desArray 描述的数组
 * specialKind 特殊玩法
 * isOmitColdDataDeal 是否遗漏和冷热数据的处理
 */
SscData.prototype.ballSectionCommon = function(numCountStart,numCountEnd,desArray,specialKind,isOmitColdDataDeal){
	var content = "";
	
	if(specialKind == null){
		
		//大小奇偶按钮字符串
		var dxjoStr='		<div class="content">'+
					'			<div class="mun" data-role-type="big">大</div>'+
					'			<div class="mun" data-role-type="small">小</div>'+
					'			<div class="mun" data-role-type="odd">单</div>'+
					'			<div class="mun" data-role-type="even">双</div>'+
					'			<div class="mun" data-role-type="all">全</div>'+
					'			<div class="mun" data-role-type="empty">清</div>'+
					'		</div>';
		
		for(var i = numCountStart;i <= numCountEnd; i++){
			content += '<div class="lottery-child main"><div class="container">';
			var numDes = desArray[i];
			if(isNotEmpty(numDes)) {
				content += '<div class="child-head">';
				content += '	<div class="title"><p>' + numDes + '</p></div>';
				//大小单双
				content += dxjoStr;
				//遗漏冷热元素处理
				if(isOmitColdDataDeal) {
					content += "";
				}
				content += '</div>';
			}
			content += '<div class="child-content">';
			//只有单行的情况 号码排列居中
			if(numCountStart == numCountEnd) {
				content += '	<div class="content long2">';
			} else {
				content += '	<div class="content">';
			}
			
			//0 - 9的号码
			for(var j = 0; j < sscData.common_code_array.length; j++){
				content += '<div class="munbtn mun" data-realvalue="'+ sscData.common_code_array[j] +'" name="ballNum_'+i+'_match">'+ sscData.common_code_array[j] +'</div>';
				//遗漏冷热元素处理
				if(isOmitColdDataDeal) {
					content += "";
				}
			}
			content += '	</div>';
			content += '</div>';
			content += '</div>';
			content += '</div>';
		}
	}else{ 
		var hz_code_array = null;
		if(specialKind == "QSZXHZ" || specialKind == "HSZXHZ"){
			//0 - 27的号码
			hz_code_array = sscData.qszxhz_code_array;
		}else if(specialKind == "QEZXHZ" || specialKind == "HEZXHZ"){
			//0 - 18的号码
			hz_code_array = sscData.qezxhz_code_array;
		}else if(specialKind == "QEZXHZ_G" || specialKind == "HEZXHZ_G"){
			//1 - 17的号码
			hz_code_array = sscData.qezxhz_g_code_array
		}else if(specialKind == "QSZXHZ_G" || specialKind == "HSZXHZ_G"){
			//1 - 27的号码
			hz_code_array = sscData.qszxhz_g_code_array;
		}
		
		if(hz_code_array != null) {
			content += '<div class="lottery-child main"><div class="container">';
			var numDes = desArray[i];
			if(isNotEmpty(numDes)) {
				content += '<div class="child-head">';
				content += '	<div class="title"><p>' + numDes + '</p></div>';
				//遗漏冷热元素处理
				if(isOmitColdDataDeal) {
					content += "";
				}
				content += '</div>';
			}
			content += '<div class="child-content">';
			content += '	<div class="content long2">';
			for(var j = 0; j < hz_code_array.length; j++){
				content += '<div class="munbtn mun" data-realvalue="'+ hz_code_array[j] +'" name="ballNum_'+i+'_match">'+ hz_code_array[j] +'</div>';
				//遗漏冷热元素处理
				if(isOmitColdDataDeal) {
					content += "";
				}
			}
			content += '	</div>';
			content += '</div>';
			content += '</div>';
			content += '</div>';
		} else if(specialKind == "QEDXDS" || specialKind == "HEDXDS"){
			for(var i = 0; i < desArray.length; i++){
				content += '<div class="lottery-child no main"><div class="container">';
				var numDes = desArray[i];
				content += '<div class="child-content">';
				if(isNotEmpty(numDes)) {
					content += '<div class="title"><p>' + numDes + '</p></div>';
					//遗漏冷热元素处理
					if(isOmitColdDataDeal) {
						content += "";
					}
				}
				content += '	<div class="content">';
				//1 - 4的号码
				for(var j = 0; j < sscData.dxds_code_array.length; j++){
					content += '<div class="munbtn mun2" data-realvalue="'+ sscData.dxds_code_array[j] +'" name="ballNum_'+i+'_match">';
					content += '<h2 class="value">';
					if(sscData.dxds_code_array[j] == 1){
						content += "大";
					}else if(sscData.dxds_code_array[j] == 2){
						content += "小";
					}else if(sscData.dxds_code_array[j] == 3){
						content += "单";
					}else if(sscData.dxds_code_array[j] == 4){
						content += "双";
					}
					content += '</h2>';
					//遗漏冷热元素处理
					if(isOmitColdDataDeal) {
						content += "";
					}
					content += '</div>';
				}
				content += '	</div>';
				content += '</div>';
				content += '</div>';
				content += '</div>';
			}
		}else{
			showTip("未找到该玩法的号码.");
		}
	}
    $("#ballSection").html(content);
    //任选框隐藏
    $("#rxWeiCheck").hide();
    //隐藏单式的导入框
    $("#importDsBtn").hide();
    
    //进行遗漏冷热的数值处理
    if(isOmitColdDataDeal){
    	$("#ommitHotColdBtn").show();
    } else {
    	$("#ommitHotColdBtn").hide();
    }
};

/**
 * 单式玩法的号码拼接
 * specialKind 特殊玩法
 */
SscData.prototype.dsBallSectionCommon = function(specialKind){
	
	var editorStr = "说明：\n";
	editorStr += "1、请参照标准格式样本格式录入或上传方案。\n";
	editorStr += "2、每一注号码之间的间隔符支持 回车 空格[ ] 逗号[,] 分号[;] 冒号[:] 竖线 [|]。\n";
	editorStr += "3、文件格式必须是.txt格式。\n";
	editorStr += "4、文件较大时会导致上传时间较长，请耐心等待！\n";
	editorStr += "5、将文件拖入文本框即可快速实现文件上传功能。\n";
	editorStr += "6、导入文本内容后将覆盖文本框中现有的内容。";

	var rxContent = '<div class="lottery-select">'+
	'<div class="container lottery-select-list">'+
	'	<label for="">'+
	'		<input type="checkbox" name="ds_position" onclick="lotteryCommonPage.lottertyDataDeal.dsPositionCheckFunc(this);" data-position="1" checked="checked">'+
	'		<span>万位</span>'+
	'	</label>'+
	'	<label for="">'+
	'		<input type="checkbox" name="ds_position" onclick="lotteryCommonPage.lottertyDataDeal.dsPositionCheckFunc(this);" data-position="2" checked="checked">'+
	'		<span>千位</span>'+
	'	</label>'+
	'	<label for="">'+
	'		<input type="checkbox" name="ds_position" onclick="lotteryCommonPage.lottertyDataDeal.dsPositionCheckFunc(this);" data-position="3" checked="checked">'+
	'		<span>百位</span>'+
	'	</label>'+
	'	<label for="">'+
	'		<input type="checkbox" name="ds_position" onclick="lotteryCommonPage.lottertyDataDeal.dsPositionCheckFunc(this);" data-position="4" checked="checked">'+
	'		<span>十位</span>'+
	'	</label>'+
	'	<label for="">'+
	'		<input type="checkbox" name="ds_position" onclick="lotteryCommonPage.lottertyDataDeal.dsPositionCheckFunc(this);" data-position="5" checked="checked">'+
	'		<span>个位</span>'+
	'	</label>'+
	'</div>'+
	'<div class="container lottery-select-msg" >'+
	'	<p>温馨提示：您选择了<span class="font-yellow" id="pnum">5</span>个位置，系统自动根据位置合成<span class="font-yellow" id="fnum">10</span>个方案</p>'+
	'</div>'+
	'</div>';
	
	var content = '	<div class="lottery-child main"><div class="container">'+
	'		<div class="lottery-textarea">'+
	'			<textarea id="lr_editor" placeholder="'+ editorStr +'"></textarea>'+
	'		</div>'+
	'	</div></div>';
	
    //任选玩法的话
    if(specialKind == "RXSIZXDS" || specialKind == "RXEZXDS" || specialKind == "RXSZXDS" ) {
    	//拼接加上任选的内容
    	content =  rxContent + content;
    	
    	sscData.rxParam.isRxDsAllow = true;
    	//全选
    	$("input[name='ds_position']").each(function(){
    		this.checked = true;
    	});
    	if(specialKind == "RXSIZXDS"){
    		sscData.rxParam.dsAllowCount = 5;
    		$("#fnum").text(5);
    	}else if(specialKind == "RXEZXDS" || specialKind == "RXSZXDS"){
    		sscData.rxParam.dsAllowCount = 10;
    		$("#fnum").text(10); 
    	}
    }
    $("#ballSection").html(content);
    
    //添加单式的事件处理
    lotteryCommonPage.addDsEvent();
}

SscData.prototype.zhBallSectionCommon=function(specialKind){//斗牛
	var content="";
	var kindNameType=lotteryCommonPage.currentLotteryKindInstance.param.kindNameType;
	if(specialKind=="ZHWQLHSH"){
		content='<div class="lottery-content animated" style="animation-name: fadeInUp; animation-delay: 0s;">'+
			'<div class="lottery-child no main wqzh">'+
				'<div class="container">'+
					'<div class="child-content">'+
						'<div class="title"><span>五球总和</span></div>'+
						'<div class="content">'+
							'<div class="munbtn mun2" data-realvalue="1" name="ballNum_0_match">'+
								'<p class="range">23-45</p>'+
								'<h2 class="value">大</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHWQLHSH">赔率：加载中...</p>'+
							'</div>'+
							'<div class="munbtn mun2" data-realvalue="2" name="ballNum_1_match">'+
								'<p class="range">0-22</p>'+
								'<h2 class="value">小</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHWQLHSH_2">赔率：加载中...</p>'+
							'</div>'+
							'<div class="munbtn mun2" data-realvalue="3" name="ballNum_2_match">'+
								'<h2 class="value">单</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHWQLHSH_3">赔率：加载中...</p>'+
							'</div>'+
							'<div class="munbtn mun2" data-realvalue="4" name="ballNum_1_match">'+
								'<h2 class="value">双</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHWQLHSH_4">赔率：加载中...</p>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>'+
			'<div class="lottery-content animated" style="animation-name: fadeInUp; animation-delay: 0s;">'+
				'<div class="lottery-child no main wqzh">'+
					'<div class="container">'+
						'<div class="child-content">'+
							'<div class="title"><span>梭哈</span></div>'+
							'<div class="content">'+
								'<div class="munbtn mun2" data-realvalue="5" name="ballNum_0_match">'+
									'<h2 class="value">五条</h2>'+
									'<p class="info" id="'+kindNameType+'_ZHWQLHSH_5">赔率：加载中...</p>'+
								'</div>'+
								'<div class="munbtn mun2" data-realvalue="6" name="ballNum_1_match">'+
									'<h2 class="value">四条</h2>'+
									'<p class="info" id="'+kindNameType+'_ZHWQLHSH_6">赔率：加载中...</p>'+
								'</div>'+
								'<div class="munbtn mun2" data-realvalue="7" name="ballNum_2_match">'+
									'<h2 class="value">葫芦</h2>'+
									'<p class="info" id="'+kindNameType+'_ZHWQLHSH_7">赔率：加载中...</p>'+
								'</div>'+
								'<div class="munbtn mun2" data-realvalue="8" name="ballNum_1_match">'+
									'<h2 class="value">顺子</h2>'+
									'<p class="info" id="'+kindNameType+'_ZHWQLHSH_8">赔率：加载中...</p>'+
								'</div>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>'+
				'<div class="lottery-child no main">'+
					'<div class="container">'+
						'<div class="child-content">'+
							'<div class="content">'+
								'<div class="munbtn mun2" data-realvalue="9" name="ballNum_0_match">'+
									'<h2 class="value">三条</h2>'+
									'<p class="info" id="'+kindNameType+'_ZHWQLHSH_9">赔率：加载中...</p>'+
								'</div>'+
								'<div class="munbtn mun2" data-realvalue="10" name="ballNum_1_match">'+
									'<h2 class="value">两对</h2>'+
									'<p class="info" id="'+kindNameType+'_ZHWQLHSH_10">赔率：加载中...</p>'+
								'</div>'+
								'<div class="munbtn mun2" data-realvalue="11" name="ballNum_2_match">'+
									'<h2 class="value">一对</h2>'+
									'<p class="info" id="'+kindNameType+'_ZHWQLHSH_11">赔率：加载中...</p>'+
								'</div>	'+
								'<div class="munbtn mun2" data-realvalue="12" name="ballNum_1_match">'+
									'<h2 class="value">散号</h2>'+
									'<p class="info" id="'+kindNameType+'_ZHWQLHSH_12">赔率：加载中...</p>'+
								'</div>'+
							'</div>'+		
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>'+
			'<div class="lottery-child no main wqzh">'+
				'<div class="container">'+
					'<div class="child-content">'+
						'<div class="title"><span>龙虎</span></div>'	+
						'<div class="content">'+
							'<div class="munbtn mun2" data-realvalue="13" name="ballNum_0_match">'+
								'<p class="range">万位数</p>'+
								'<h2 class="value">龙</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHWQLHSH_13">赔率：加载中...</p>'+
							'</div>'+
							'<div class="munbtn mun2" data-realvalue="14" name="ballNum_1_match">'+
								'<p class="range">个位数</p>'+
								'<h2 class="value">虎</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHWQLHSH_14">赔率：加载中...</p>'+
							'</div>'+
							'<div class="munbtn mun2" data-realvalue="15" name="ballNum_2_match">'+
								'<h2 class="value">和</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHWQLHSH_15">赔率：加载中...</p>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>'+
		'</div>';
	}else if(specialKind=="ZHDN"){
		content='<div class="lottery-child no main">'+
					'<div class="container">'+
						'<div class="child-content" style="text-align:center;">'+
							'<div class="munbtn mun2" data-realvalue="1" name="ballNum_0_match">'+
								'<h2 class="value">牛1</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHDN">赔率：加载中...</p>'+
							'</div>'+
							'<div class="munbtn mun2" data-realvalue="2" name="ballNum_1_match">'+
								'<h2 class="value">牛2</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHDN_2">赔率：加载中...</p>'+
							'</div>'+
							'<div class="munbtn mun2" data-realvalue="3" name="ballNum_2_match">'+
								'<h2 class="value">牛3</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHDN_3">赔率：加载中...</p>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>'+
				'<div class="lottery-child no main">'+
					'<div class="container">'+
						'<div class="child-content" style="text-align:center;">'+
							'<div class="munbtn mun2" data-realvalue="4" name="ballNum_3_match">'+
								'<h2 class="value">牛4</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHDN_4">赔率：加载中...</p>'+
							'</div>'+
							'<div class="munbtn mun2" data-realvalue="5" name="ballNum_4_match">'+
								'<h2 class="value">牛5</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHDN_5">赔率：加载中...</p>'+
							'</div>'+
							'<div class="munbtn mun2" data-realvalue="6" name="ballNum_5_match">'+
								'<h2 class="value">牛6</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHDN_6">赔率：加载中...</p>'+
							'</div>	'+
						'</div>'+
					'</div>'+
				'</div>'+
				'<div class="lottery-child no main">'+
					'<div class="container">'+
						'<div class="child-content" style="text-align:center;">'+
							'<div class="munbtn mun2" data-realvalue="7" name="ballNum_3_match">'+
								'<h2 class="value">牛7</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHDN_7">赔率：加载中...</p>'+
							'</div>'+
							'<div class="munbtn mun2" data-realvalue="8" name="ballNum_4_match">'+
								'<h2 class="value">牛8</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHDN_8">赔率：加载中...</p>'+
							'</div>'+
							'<div class="munbtn mun2" data-realvalue="9" name="ballNum_5_match">'+
								'<h2 class="value">牛9</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHDN_9">赔率：加载中...</p>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>'+
				'<div class="lottery-child no main">'+
					'<div class="container">'+
						'<div class="child-content" style="text-align:center;">'+
							'<div class="munbtn mun2" data-realvalue="10" name="ballNum_3_match">'+
								'<h2 class="value">牛牛</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHDN_10">赔率：加载中...</p>'+
							'</div>'+
							'<div class="munbtn mun2" data-realvalue="11" name="ballNum_4_match">'+
								'<h2 class="value">牛大</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHDN_11">赔率：加载中...</p>'+
							'</div>'+
							'<div class="munbtn mun2" data-realvalue="12" name="ballNum_5_match">'+
								'<h2 class="value">牛小</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHDN_12">赔率：加载中...</p>'+
							'</div>	'+
						'</div>'+
					'</div>'+
				'</div>'+
				'<div class="lottery-child no main">'+
					'<div class="container">'+
						'<div class="child-content" style="text-align:center;">'+
							'<div class="munbtn mun2" data-realvalue="13" name="ballNum_3_match">'+
								'<h2 class="value">牛单</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHDN_13">赔率：加载中...</p>'+
							'</div>'+
							'<div class="munbtn mun2" data-realvalue="14" name="ballNum_4_match">'+
								'<h2 class="value">牛双</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHDN_14">赔率：加载中...</p>'+
							'</div>'+
							'<div class="munbtn mun2" data-realvalue="15" name="ballNum_5_match">'+
								'<h2 class="value">无牛</h2>'+
								'<p class="info" id="'+kindNameType+'_ZHDN_15">赔率：加载中...</p>'+
							'</div>	'+
						'</div>'+
					'</div>'+
				'</div>';
	}else if(specialKind=="ZHQS"||specialKind=="ZHZS"||specialKind=="ZHHS"){
		content='<div class="lottery-child main qszh">'+
			'<div class="container">'+
				'<div class="child-content">'+
					'<div class="content">'+
						'<div class="munbtn mun" data-realvalue="0" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'">0</div>'+
						'<div class="munbtn mun" data-realvalue="1" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_2">1</div>'+
						'<div class="munbtn mun" data-realvalue="2" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_3">2</div>'+
						'<div class="munbtn mun" data-realvalue="3" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_4">3</div>'+
						'<div class="munbtn mun" data-realvalue="4" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_5">4</div>'+
						'<div class="munbtn mun" data-realvalue="5" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_6">5</div>'+
						'<div class="munbtn mun" data-realvalue="6" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_7">6</div>'+
						'<div class="munbtn mun" data-realvalue="7" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_8">7</div>'+
						'<div class="munbtn mun" data-realvalue="8" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_9">8</div>'+
						'<div class="munbtn mun" data-realvalue="9" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_10">9</div>'+
						'<div class="munbtn mun" data-realvalue="10" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_11">10</div>'+
						'<div class="munbtn mun" data-realvalue="11" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_12">11</div>'+
						'<div class="munbtn mun" data-realvalue="12" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_13">12</div>'+
						'<div class="munbtn mun" data-realvalue="13" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_14">13</div>'+
						'<div class="munbtn mun" data-realvalue="14" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_15">14</div>'+
						'<div class="munbtn mun" data-realvalue="15" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_16">15</div>'+
						'<div class="munbtn mun" data-realvalue="16" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_17">16</div>'+
						'<div class="munbtn mun" data-realvalue="17" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_18">17</div>'+
						'<div class="munbtn mun" data-realvalue="18" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_19">18</div>'+
						'<div class="munbtn mun" data-realvalue="19" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_20">19</div>'+
						'<div class="munbtn mun" data-realvalue="20" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_21">20</div>'+
						'<div class="munbtn mun" data-realvalue="21" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_22">21</div>'+
						'<div class="munbtn mun" data-realvalue="22" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_23">22</div>'+
						'<div class="munbtn mun" data-realvalue="23" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_24">23</div>'+
						'<div class="munbtn mun" data-realvalue="24" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_25">24</div>'+
						'<div class="munbtn mun" data-realvalue="25" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_26">25</div>'+
						'<div class="munbtn mun" data-realvalue="26" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_27">26</div>'+
						'<div class="munbtn mun" data-realvalue="27" name="ballNum_0_match" id="'+kindNameType+'_'+specialKind+'_28">27</div>'+
					'</div>'+
				'</div>'+
			'</div>'+
		'</div>'+
		'<div class="lottery-child main qszh m20-18">'+
			'<div class="container">'+
				'<div class="child-content">'+
					'<div class="munbtn mun2 ml35" data-realvalue="29" name="ballNum_0_match">'+
						'<p class="range">14-27</p>'+
						'<h2 class="value">大</h2>'+
						'<p class="info" id="'+kindNameType+'_'+specialKind+'_30">赔率：加载中...</p>'+
					'</div>'+
					'<div class="munbtn mun2" data-realvalue="28" name="ballNum_1_match">'+
						'<p class="range">0-13</p>'+
						'<h2 class="value">小</h2>'+
						'<p class="info" id="'+kindNameType+'_'+specialKind+'_29">赔率：加载中...</p>'+
					'</div>'+
					'<div class="munbtn mun2" data-realvalue="30" name="ballNum_2_match">'+
						'<h2 class="value">单</h2>'+
						'<p class="info" id="'+kindNameType+'_'+specialKind+'_31">赔率：加载中...</p>'+
					'</div>	'+
					'<div class="munbtn mun2" data-realvalue="31" name="ballNum_1_match">'+
						'<h2 class="value">双</h2>'+
						'<p class="info" id="'+kindNameType+'_'+specialKind+'_32">赔率：加载中...</p>'+
					'</div>'+
				'</div>'+
			'</div>'+
		'</div>'+
		'<div class="lottery-child main qszh m20-18">'+
			'<div class="container">'+
				'<div class="child-content">'+
					'<div class="munbtn mun2 ml35" data-realvalue="32" name="ballNum_0_match">'+
						'<h2 class="value">0-3</h2>'+
						'<p class="info" id="'+kindNameType+'_'+specialKind+'_33">赔率：加载中...</p>'+
					'</div>'+
					'<div class="munbtn mun2" data-realvalue="33" name="ballNum_1_match">'+
						'<h2 class="value">4-7</h2>'+
						'<p class="info" id="'+kindNameType+'_'+specialKind+'_34">赔率：加载中...</p>'+
					'</div>'+
					'<div class="munbtn mun2" data-realvalue="34" name="ballNum_2_match">'+
						'<h2 class="value">8-11</h2>'+
						'<p class="info" id="'+kindNameType+'_'+specialKind+'_35">赔率：加载中...</p>'+
					'</div>	'+
					'<div class="munbtn mun2" data-realvalue="35" name="ballNum_1_match">'+
						'<h2 class="value">12-15</h2>'+
						'<p class="info" id="'+kindNameType+'_'+specialKind+'_36">赔率：加载中...</p>'+
					'</div>'+
					'<div class="munbtn mun2" data-realvalue="36" name="ballNum_0_match">'+
						'<h2 class="value">16-19</h2>'+
						'<p class="info" id="'+kindNameType+'_'+specialKind+'_37">赔率：加载中...</p>'+
					'</div>'+
					'<div class="munbtn mun2 ml35" data-realvalue="37" name="ballNum_1_match">'+
						'<h2 class="value">20-23</h2>'+
						'<p class="info" id="'+kindNameType+'_'+specialKind+'_38">赔率：加载中...</p>'+
					'</div>'+
					'<div class="munbtn mun2" data-realvalue="38" name="ballNum_2_match">'+
						'<h2 class="value">24-27</h2>'+
						'<p class="info" id="'+kindNameType+'_'+specialKind+'_39">赔率：加载中...</p>'+
					'</div>'+
				'</div>'+
			'</div>'+
		'</div>'+
		'<div class="lottery-child main qszh m20-18">'+
			'<div class="container">'+
				'<div class="child-content">'+
					'<div class="munbtn mun2 ml35" data-realvalue="39" name="ballNum_0_match">'+
						'<h2 class="value">豹子</h2>'+
						'<p class="info" id="'+kindNameType+'_'+specialKind+'_40">赔率：加载中...</p>'+
					'</div>'+
					'<div class="munbtn mun2" data-realvalue="40" name="ballNum_1_match">'+
						'<h2 class="value">顺子</h2>'+
						'<p class="info" id="'+kindNameType+'_'+specialKind+'_41">赔率：加载中...</p>'+
					'</div>'+
					'<div class="munbtn mun2" data-realvalue="41" name="ballNum_2_match">'+
						'<h2 class="value">对子</h2>'+
						'<p class="info" id="'+kindNameType+'_'+specialKind+'_42">赔率：加载中...</p>'+
					'</div>	'+
					'<div class="munbtn mun2" data-realvalue="42" name="ballNum_1_match">'+
						'<h2 class="value">半顺</h2>'+
						'<p class="info" id="'+kindNameType+'_'+specialKind+'_43">赔率：加载中...</p>'+
					'</div>'+
					'<div class="munbtn mun2" data-realvalue="43" name="ballNum_1_match">'+
						'<h2 class="value">杂六</h2>'+
						'<p class="info" id="'+kindNameType+'_'+specialKind+'_44">赔率：加载中...</p>'+
					'</div>'+
				'</div>'+
			'</div>'+
		'</div>';
	}
	
	$("#ballSection").html(content);
}

/**
 * 计算当前用户最高模式下的奖金
 */
SscData.prototype.calucateCurrentUserHighestWin = function(kindKey){
	if(lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.size() == 0){
		return "奖金未完全获取,请再次操作或者刷新页面";
	}
	var currentLotteryWin;
	currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get(kindKey);
	if(currentLotteryWin == null){
		return "未配置该玩法的奖金.";
	}
	var result = (currentLotteryWin.winMoney + (currentUserSscModel - sscLowestAwardModel) * currentLotteryWin.specCode) * 1;	
    return result;
};

/**
 * 获取当前用户最高模式下的奖金
 */
SscData.prototype.getKindPlayLotteryWin = function(kindKey){
	if(lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.size() == 0){
		return "奖金未完全获取,请稍作等待或者再次操作.";
	}
	var currentLotteryWin;
	currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get(lotteryCommonPage.currentLotteryKindInstance.param.kindNameType+ "_" +kindKey);
	if(currentLotteryWin == null){
		return "未配置该玩法的奖金.";
	}
	var result = (currentLotteryWin.winMoney + (currentUserSscModel - sscLowestAwardModel) * currentLotteryWin.specCode) * 1;	
    return result.toFixed(lotteryCommonPage.param.toFixedValue);
};

/**
 * 设置当前用户最高模式下的奖金和玩法描述
 */
SscData.prototype.setKindPlayDes = function(){
	//五星复式
	var WXFS_WIN = sscData.getKindPlayLotteryWin("WXFS");
	sscData.des.WXFS_DES = "<div class='title'>每位至少选择一个号码，竞猜开奖号码的全部五位，号码和位置都对应即中奖，最高奖金\""+WXFS_WIN+"\"元</div>";

	//五星单式
	var WXDS_WIN = sscData.getKindPlayLotteryWin("WXDS");
	sscData.des.WXDS_DES = "<div class='title'>每位至少选择一个号码，竞猜开奖号码的全部五位，号码和位置都对应即中奖，最高奖金\""+WXDS_WIN+"\"元</div>";

	//五星组选120
	var WXZX120_WIN = sscData.getKindPlayLotteryWin("WXZX120");
	sscData.des.WXZX120_DES = "<div class='title'>至少选择五个号码投注，竞猜开奖号码的全部五位，号码一致顺序不限即中奖，最高奖金\""+ WXZX120_WIN +"\"元</div>";

	//五星组选60
	var WXZX60_WIN = sscData.getKindPlayLotteryWin("WXZX60");
	sscData.des.WXZX60_DES = "<div class='title'>至少选择1个二重号码和3个单号号码组成一注，竞猜开奖号码的全部五位，号码一致顺序不限即中奖，最高奖金\""+ WXZX60_WIN +"\"元</div>";

	//五星组选30
	var WXZX30_WIN = sscData.getKindPlayLotteryWin("WXZX30");
	sscData.des.WXZX30_DES = "<div class='title'> 至少选择2个二重号码和1个单号号码组成一注，竞猜开奖号码的全部五位，号码一致顺序不限即中奖，最高奖金\""+ WXZX30_WIN +"\"元</div>";

	//五星组选20
	var WXZX20_WIN = sscData.getKindPlayLotteryWin("WXZX20");
	sscData.des.WXZX20_DES = "<div class='title'>至少选择1个三重号码和2个单号号码组成一注，竞猜开奖号码的全部五位，号码一致顺序不限即中奖，最高奖金\""+ WXZX20_WIN +"\"元</div>";

	//五星组选10
	var WXZX10_WIN = sscData.getKindPlayLotteryWin("WXZX10");
	sscData.des.WXZX10_DES = "<div class='title'>至少选择1个三重号码和1个二重号码组成一注，竞猜开奖号码的全部五位，号码一致顺序不限即中奖，最高奖金\""+ WXZX10_WIN +"\"元</div>";

	//五星组选5
	var WXZX5_WIN = sscData.getKindPlayLotteryWin("WXZX5");
	sscData.des.WXZX5_DES = "<div class='title'>至少选择1个四重号码和1个单号号码组成一注，竞猜开奖号码的全部五位，号码一致顺序不限即中奖，最高奖金\""+ WXZX5_WIN +"\"元</div>";

	//五星一码不定位
	var WXYMBDW_WIN = sscData.getKindPlayLotteryWin("WXYMBDW");
	sscData.des.WXYMBDW_DES = "<div class='title'>从0-9中至少选择1个号码投注，竞猜开奖号码中包含这个号码，包含即中奖，最高奖金\""+ WXYMBDW_WIN +"\"元</div>";

	//五星二码不定位
	var WXEMBDW_WIN = sscData.getKindPlayLotteryWin("WXEMBDW");
	sscData.des.WXEMBDW_DES = "<div class='title'>从0-9中至少选择2个号码投注，竞猜开奖号码中包含这2个号码，包含即中奖，最高奖金\""+ WXEMBDW_WIN +"\"元</div>";

	//五星三码不定位
	var WXSMBDW_WIN = sscData.getKindPlayLotteryWin("WXSMBDW");
	sscData.des.WXSMBDW_DES = "<div class='title'>从0-9中至少选择3个号码投注，竞猜开奖号码中包含这3个号码，包含即中奖，最高奖金\""+ WXSMBDW_WIN +"\"元</div>";

	//四星复式
	var SXFS_WIN = sscData.getKindPlayLotteryWin("SXFS");
	sscData.des.SXFS_DES = "<div class='title'>每位至少选择一个号码，竞猜开奖号码的后四位，号码和位置都对应即中奖，最高奖金\""+ SXFS_WIN +"\"元</div>";

	//四星单式
	var SXDS_WIN = sscData.getKindPlayLotteryWin("SXDS");
	sscData.des.SXDS_DES = "<div class='title'>每位至少选择一个号码，竞猜开奖号码的后四位，号码和位置都对应即中奖，最高奖金\""+ SXDS_WIN +"\"元</div>";

	//四星组选24
	var SXZX24_WIN = sscData.getKindPlayLotteryWin("SXZX24");
	sscData.des.SXZX24_DES = "<div class='title'>至少选择4个号码投注，竞猜开奖号码的后4位，号码一致顺序不限即中奖，最高奖金\""+ SXZX24_WIN +"\"元</div>";

	//四星组选12
	var SXZX12_WIN = sscData.getKindPlayLotteryWin("SXZX12");
	sscData.des.SXZX12_DES = "<div class='title'>至少选择1个二重号码和2个单号号码，竞猜开奖号码的后四位，号码一致顺序不限即中奖，最高奖金\""+ SXZX12_WIN +"\"元</div>";

	//四星组选6
	var SXZX6_WIN = sscData.getKindPlayLotteryWin("SXZX6");
	sscData.des.SXZX6_DES = "<div class='title'>至少选择2个二重号码，竞猜开奖号码的后四位，号码一致顺序不限即中奖，最高奖金\""+ SXZX6_WIN +"\"元</div>";

	//四星组选4
	var SXZX4_WIN = sscData.getKindPlayLotteryWin("SXZX4");
	sscData.des.SXZX4_DES = "<div class='title'>至少选择1个三重号码和1个单号号码，竞猜开奖号码的后四位，号码一致顺序不限即中奖，最高奖金\""+ SXZX4_WIN +"\"元</div>";

	//四星一码不定位
	var SXYMBDW_WIN = sscData.getKindPlayLotteryWin("SXYMBDW");
	sscData.des.SXYMBDW_DES = "<div class='title'>从0-9中至少选择1个号码投注，竞猜开奖号码后四位中包含这个号码，包含即中奖，最高奖金\""+ SXYMBDW_WIN +"\"元</div>";

	//四星二码不定位
	var SXEMBDW_WIN = sscData.getKindPlayLotteryWin("SXEMBDW");
	sscData.des.SXEMBDW_DES = "<div class='title'>从0-9中至少选择2个号码投注，竞猜开奖号码后四位中包含这2个号码，包含即中奖，最高奖金\""+ SXEMBDW_WIN +"\"元</div>";

	//前三复式
	var QSZXFS_WIN = sscData.getKindPlayLotteryWin("QSZXFS");
	sscData.des.QSZXFS_DES = "<div class='title'>每位至少选择一个号码，竞猜开奖号码的前三位，号码和位置都对应即中奖，最高奖金\""+ QSZXFS_WIN +"\"元</div>";

	//前三单式
	var QSZXDS_WIN = sscData.getKindPlayLotteryWin("QSZXDS");
	sscData.des.QSZXDS_DES = "<div class='title'>每位至少选择一个号码，竞猜开奖号码的前三位，号码和位置都对应即中奖，最高奖金\""+ QSZXDS_WIN +"\"元</div>";

	//前三直选和值
	var QSZXHZ_WIN = sscData.getKindPlayLotteryWin("QSZXHZ");
	sscData.des.QSZXHZ_DES = "<div class='title'>至少选择一个和值，竞猜开奖号码前三位数字之和，最高奖金\""+ QSZXHZ_WIN +"\"元</div>";

	//前三组选和值
	var QSZXHZ_G_WIN = sscData.getKindPlayLotteryWin("QSZXHZ_G");
	var QSZXHZ_G_2_WIN = sscData.getKindPlayLotteryWin("QSZXHZ_G_2");
	sscData.des.QSZXHZ_G_DES = "<div class='title'>至少选择一个和值，竞猜开奖号码前三位数字之和(不含豹子号)，一等奖组三奖金\""+ QSZXHZ_G_WIN +"\"元，二等奖组六奖金\""+ QSZXHZ_G_2_WIN +"\"元</div>";

	//前三组三
	var QSZS_WIN = sscData.getKindPlayLotteryWin("QSZS");
	sscData.des.QSZS_DES = "<div class='title'>从0-9中选择2个数字组成两注，所选号码与开奖号码的前三位相同，顺序不限，最高奖金\""+ QSZS_WIN +"\"元</div>";

	//前三组六
	var QSZL_WIN = sscData.getKindPlayLotteryWin("QSZL");
	sscData.des.QSZL_DES = "<div class='title'>从0-9中任意选择3个号码组成一注，所选号码与开奖号码的前三位相同，顺序不限，最高奖金\""+ QSZL_WIN +"\"元</div>";

	//前三一码不定位
	var QSYMBDW_WIN = sscData.getKindPlayLotteryWin("QSYMBDW");
	sscData.des.QSYMBDW_DES = "<div class='title'> 从0-9中至少选择1个号码投注，竞猜开奖号码前三位中包含这个号码，包含即中奖，最高奖金\""+ QSYMBDW_WIN +"\"元</div>";

	//前三二码不定位
	var QSEMBDW_WIN = sscData.getKindPlayLotteryWin("QSEMBDW");
	sscData.des.QSEMBDW_DES = "<div class='title'> 从0-9中至少选择2个号码投注，竞猜开奖号码前三位中包含这2个号码，包含即中奖，最高奖金\""+ QSEMBDW_WIN +"\"元</div>";

	//后三复式
	var HSZXFS_WIN = sscData.getKindPlayLotteryWin("HSZXFS");
	sscData.des.HSZXFS_DES = "<div class='title'>每位至少选择一个号码，竞猜开奖号码的后三位，号码和位置都对应即中奖，最高奖金\""+ HSZXFS_WIN +"\"元</div>";

	//后三单式
	var HSZXDS_WIN = sscData.getKindPlayLotteryWin("HSZXDS");
	sscData.des.HSZXDS_DES = "<div class='title'>每位至少选择一个号码，竞猜开奖号码的后三位，号码和位置都对应即中奖，最高奖金\""+ HSZXDS_WIN +"\"元</div>";

	//后三直选和值
	var HSZXHZ_WIN = sscData.getKindPlayLotteryWin("HSZXHZ");
	sscData.des.HSZXHZ_DES = "<div class='title'>至少选择一个和值，竞猜开奖号码后三位数字之和，最高奖金\""+ HSZXHZ_WIN +"\"元</div>";

	//后三组选和值
	var HSZXHZ_G_WIN = sscData.getKindPlayLotteryWin("HSZXHZ_G");
	var HSZXHZ_G_2_WIN = sscData.getKindPlayLotteryWin("HSZXHZ_G_2");
	sscData.des.HSZXHZ_G_DES = "<div class='title'>至少选择一个和值，竞猜开奖号码后三位数字之和(不含豹子号)，一等奖组三奖金\""+ HSZXHZ_G_WIN +"\"元，二等奖组六奖金\""+ HSZXHZ_G_2_WIN +"\"元</div>";

	//后三组三
	var HSZS_WIN = sscData.getKindPlayLotteryWin("HSZS");
	sscData.des.HSZS_DES = "<div class='title'>从0-9中选择2个数字组成两注，所选号码与开奖号码的后三位相同，顺序不限，最高奖金\""+ HSZS_WIN +"\"元</div>";

	//后三组六
	var HSZL_WIN = sscData.getKindPlayLotteryWin("HSZL");
	sscData.des.HSZL_DES = "<div class='title'>从0-9中任意选择3个号码组成一注，所选号码与开奖号码的后三位相同，顺序不限，最高奖金\""+ HSZL_WIN +"\"元</div>";

	//后三一码不定位
	var HSYMBDW_WIN = sscData.getKindPlayLotteryWin("HSYMBDW");
	sscData.des.HSYMBDW_DES = "<div class='title'> 从0-9中至少选择1个号码投注，竞猜开奖号码后三位中包含这个号码，包含即中奖，最高奖金\""+ HSYMBDW_WIN +"\"元</div>";

	//后三二码不定位
	var HSEMBDW_WIN = sscData.getKindPlayLotteryWin("HSEMBDW");
	sscData.des.HSEMBDW_DES = "<div class='title'> 从0-9中至少选择2个号码投注，竞猜开奖号码后三位中包含这2个号码，包含即中奖，最高奖金\""+ HSEMBDW_WIN +"\"元</div>";

	//前二直选复式
	var QEZXFS_WIN = sscData.getKindPlayLotteryWin("QEZXFS");	
	sscData.des.QEZXFS_DES = "<div class='title'> 每位至少选择一个号码，竞猜开奖号码的前二位，号码和位置都对应即中奖，最高奖金\""+ QEZXFS_WIN +"\"元</div>";

	//前二直选单式
	var QEZXDS_WIN = sscData.getKindPlayLotteryWin("QEZXDS");
	sscData.des.QEZXDS_DES = "<div class='title'> 每位至少选择一个号码，竞猜开奖号码的前二位，号码和位置都对应即中奖，最高奖金\""+ QEZXDS_WIN +"\"元</div>";

	//前二直选和值
	var QEZXHZ_WIN = sscData.getKindPlayLotteryWin("QEZXHZ");
	sscData.des.QEZXHZ_DES = "<div class='title'> 至少选择一个和值，竞猜开奖号码前二位数字之和，最高奖金\""+ QEZXHZ_WIN +"\"元</div>";

	//前二组选复式
	var QEZXFS_G_WIN = sscData.getKindPlayLotteryWin("QEZXFS_G");
	sscData.des.QEZXFS_G_DES = "<div class='title'> 从0-9中选择2个数字组成一注，所选号码与开奖号码的前二位相同，顺序不限，最高奖金\""+ QEZXFS_G_WIN +"\"元</div>";

	//前二组选单式
	var QEZXDS_G_WIN = sscData.getKindPlayLotteryWin("QEZXDS_G");
	sscData.des.QEZXDS_G_DES = "<div class='title'> 从0-9中选择2个数字组成一注，所选号码与开奖号码的前二位相同，顺序不限，最高奖金\""+ QEZXDS_G_WIN +"\"元</div>";

	//前二组选和值
	var QEZXHZ_G_WIN = sscData.getKindPlayLotteryWin("QEZXHZ_G");
	sscData.des.QEZXHZ_G_DES = "<div class='title'> 所选数值等于开奖号码的前二位数字相加之和（不含对子），最高奖金\""+ QEZXHZ_G_WIN +"\"元</div>";

	//后二直选复式
	var HEZXFS_WIN = sscData.getKindPlayLotteryWin("HEZXFS");
	sscData.des.HEZXFS_DES = "<div class='title'> 每位至少选择一个号码，竞猜开奖号码的后二位，号码和位置都对应即中奖，最高奖金\""+ HEZXFS_WIN +"\"元</div>";

	//后二直选单式
	var HEZXDS_WIN = sscData.getKindPlayLotteryWin("HEZXDS");
	sscData.des.HEZXDS_DES = "<div class='title'> 每位至少选择一个号码，竞猜开奖号码的后二位，号码和位置都对应即中奖，最高奖金\""+ HEZXDS_WIN +"\"元</div>";

	//后二直选和值
	var HEZXHZ_WIN = sscData.getKindPlayLotteryWin("HEZXHZ");
	sscData.des.HEZXHZ_DES = "<div class='title'> 至少选择一个和值，竞猜开奖号码后二位数字之和，最高奖金\""+ HEZXHZ_WIN +"\"元</div>";

	//后二组选复式
	var HEZXFS_G_WIN = sscData.getKindPlayLotteryWin("HEZXFS_G");
	sscData.des.HEZXFS_G_DES = "<div class='title'> 从0-9中选择2个数字组成一注，所选号码与开奖号码的后二位相同，顺序不限，最高奖金\""+ HEZXFS_G_WIN +"\"元</div>";

	//后二组选单式
	var HEZXDS_G_WIN = sscData.getKindPlayLotteryWin("HEZXDS_G");
	sscData.des.HEZXDS_G_DES = "<div class='title'> 从0-9中选择2个数字组成一注，所选号码与开奖号码的后二位相同，顺序不限，最高奖金\""+ HEZXDS_G_WIN +"\"元</div>";

	//后二组选和值
	var HEZXHZ_G_WIN = sscData.getKindPlayLotteryWin("HEZXHZ_G");
	sscData.des.HEZXHZ_G_DES = "<div class='title'> 所选数值等于开奖号码的后二位数字相加之和（不含对子），最高奖金\""+ HEZXHZ_G_WIN +"\"元</div>";

	//一星前一
	var YXQY_WIN = sscData.getKindPlayLotteryWin("YXQY");
	sscData.des.YXQY_DES = "<div class='title'> 至少选择一个号码，竞猜开奖号码的前一位，号码和位置都对应即中奖，最高奖金\""+ YXQY_WIN +"\"元</div>";

	//一星后一
	var YXHY_WIN = sscData.getKindPlayLotteryWin("YXHY");
	sscData.des.YXHY_DES = "<div class='title'> 至少选择一个号码，竞猜开奖号码的后一位，号码和位置都对应即中奖，最高奖金\""+ YXHY_WIN +"\"元</div>";

	//前二大小单双
	var QEDXDS_WIN = sscData.getKindPlayLotteryWin("QEDXDS");
	sscData.des.QEDXDS_DES = "<div class='title'> 对应万位以及千位选择大小单双，0、2、4 、6、8 为双；1、3、5、7、9 为单；0、1、2、3、4 为小；5、6、7、8、9 为大。最高奖金\""+ QEDXDS_WIN +"\"元</div>";

	//后二大小单双
	var HEDXDS_WIN = sscData.getKindPlayLotteryWin("HEDXDS");
	sscData.des.HEDXDS_DES = "<div class='title'> 对应个位以及十位选择大小单双，0、2、4 、6、8 为双；1、3、5、7、9 为单；0、1、2、3、4 为小；5、6、7、8、9 为大。最高奖金\""+ HEDXDS_WIN +"\"元</div>";

	//五行定位胆
	var WXDWD_WIN = sscData.getKindPlayLotteryWin("WXDWD");
	sscData.des.WXDWD_DES = "<div class='title'> 从万位、千位、百位、十位、个位任意位置上至少选择1个号码，选号与相同位置上的开奖号码一致，最高奖金\""+ WXDWD_WIN +"\"元</div>";
	
	//任选二
	var RXE_WIN = sscData.getKindPlayLotteryWin("RXE");
	sscData.des.RXE_DES = "<div class='title'> 从万位、千位、百位、十位、个位中至少两位上各选1个号码，选号与相同位置上的开奖号码都一致，最高奖金\""+ RXE_WIN +"\"元</div>";
	
	//任选三
	var RXS_WIN = sscData.getKindPlayLotteryWin("RXS");
	sscData.des.RXS_DES = "<div class='title'> 从万位、千位、百位、十位、个位中至少三位上各选择1个号码，选号与相同位置上的开奖号码都一致，最高奖金\""+ RXS_WIN +"\"元</div>";
	
	//任选四
	var RXSI_WIN = sscData.getKindPlayLotteryWin("RXSI");
	sscData.des.RXSI_DES = "<div class='title'> 从万位、千位、百位、十位、个位中至少四位上各选择1个号码，选号与相同位置上的开奖号码一致，最高奖金\""+ RXSI_WIN +"\"元</div>";
	
	//任选四直选单式
	var RXSIZXDS_WIN = sscData.getKindPlayLotteryWin("RXSIZXDS");
	sscData.des.RXSIZXDS_DES = "<div class='title'> 从万位、千位、百位、十位、个位中至少四位上各选择1个号码，选号与相同位置上的开奖号码一致，最高奖金\""+ RXSIZXDS_WIN +"\"元</div>";
	
	//任选二直选单式
	var RXEZXDS_WIN = sscData.getKindPlayLotteryWin("RXEZXDS");
	sscData.des.RXEZXDS_DES = "<div class='title'> 从万位、千位、百位、十位、个位中至少四位上各选择1个号码，选号与相同位置上的开奖号码一致，最高奖金\""+ RXEZXDS_WIN +"\"元</div>";
	
	//任选三直选单式
	var RXSZXDS_WIN = sscData.getKindPlayLotteryWin("RXSZXDS");
	sscData.des.RXSZXDS_DES = "<div class='title'> 从万位、千位、百位、十位、个位中至少四位上各选择1个号码，选号与相同位置上的开奖号码一致，最高奖金\""+ RXSZXDS_WIN +"\"元</div>";
	
	sscData.des.ZHWQLHSH_DES="<div class='title'>依据开奖的五个号码进行组合，形成多种玩法" +
								"(" +
								"①五球总和：开奖的五个号码相加的总和;" +
								"②梭哈：开奖号码不分顺序，依照开奖号码组成[五条][四条][葫芦][顺子][三条][两对][一对][散号]" +
								"③龙虎：万位为龙，个位为虎，以[万位]、[个位]2个号码相比较)" +
							 "</div>";
	sscData.des.ZHDN_DES="<div class='title'>依据开奖的五个号码为基础，任意三个号码加总和为0或者10的倍数，剩余两个号码相加之和的个位数为点数，1点为牛1,依此类推.无法组合时为无牛</div>";
	sscData.des.ZHQS_DES="<div class='title'>依据开奖的五个号码为基础，前三个号码(123**  1+2+3=6)的总和值0~27作为开奖依据</div>";
	sscData.des.ZHZS_DES="<div class='title'>依据开奖的五个号码为基础，中间三个号码(*123*  1+2+3=6)的总和值0~27作为开奖依据</div>";
	sscData.des.ZHHS_DES="<div class='title'>依据开奖的五个号码为基础，后面个号码(**123  1+2+3=6)的总和值0~27作为开奖依据</div>";
};

/**
 * 任选位置复选框点击事件
 */
SscData.prototype.dsPositionCheckFunc = function(obj){
	var positionCount = 0;
	$("input[name='ds_position']").each(function(){
		if(this.checked){
			positionCount++;
		}
	});
	
    if(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey == 'RXSIZXDS'){
		//判断当前是否符合该单式的处理方式
		if(positionCount == 4){  //选择4位的时候,有一个方案
			sscData.rxParam.isRxDsAllow = true;
			sscData.rxParam.dsAllowCount = 1;
			$("#pnum").text(positionCount);
			$("#fnum").text(1);  
		}else if(positionCount == 5){
			sscData.rxParam.isRxDsAllow = true;
			sscData.rxParam.dsAllowCount = 5;
			$("#pnum").text(positionCount);
			$("#fnum").text(5);  
		}else{
			sscData.rxParam.isRxDsAllow = false;
			sscData.rxParam.dsAllowCount = 0;
			$("#pnum").text(positionCount);
			$("#fnum").text(0);  
		}
    }else if(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey == 'RXEZXDS'){
		//判断当前是否符合该单式的处理方式
		if(positionCount == 2){  //选择2位的时候,有一个方案
			sscData.rxParam.isRxDsAllow = true;
			sscData.rxParam.dsAllowCount = 1;
			$("#pnum").text(positionCount);
			$("#fnum").text(1);  
		}else if(positionCount == 3){
			sscData.rxParam.isRxDsAllow = true;
			sscData.rxParam.dsAllowCount = 3;
			$("#pnum").text(positionCount);
			$("#fnum").text(3);  
		}else if(positionCount == 4){
			sscData.rxParam.isRxDsAllow = true;
			sscData.rxParam.dsAllowCount = 6;
			$("#pnum").text(positionCount);
			$("#fnum").text(6);  
		}else if(positionCount == 5){
			sscData.rxParam.isRxDsAllow = true;
			sscData.rxParam.dsAllowCount = 10;
			$("#pnum").text(positionCount);
			$("#fnum").text(10);  
		}else{
			sscData.rxParam.isRxDsAllow = false;
			sscData.rxParam.dsAllowCount = 0;
			$("#pnum").text(positionCount);
			$("#fnum").text(0);  
		}
    }else if(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey == 'RXSZXDS'){
		//判断当前是否符合该单式的处理方式
		if(positionCount == 3){  //选择2位的时候,有一个方案
			sscData.rxParam.isRxDsAllow = true;
			sscData.rxParam.dsAllowCount = 1;
			$("#pnum").text(positionCount);
			$("#fnum").text(1);  
		}else if(positionCount == 4){
			sscData.rxParam.isRxDsAllow = true;
			sscData.rxParam.dsAllowCount = 4;
			$("#pnum").text(positionCount);
			$("#fnum").text(4);  
		}else if(positionCount == 5){
			sscData.rxParam.isRxDsAllow = true;
			sscData.rxParam.dsAllowCount = 10;
			$("#pnum").text(positionCount);
			$("#fnum").text(10);  
		}else{
			sscData.rxParam.isRxDsAllow = false;
			sscData.rxParam.dsAllowCount = 0;
			$("#pnum").text(positionCount);
			$("#fnum").text(0);  
		}
    }else{
    	showTip("该直选单式未配置");
    }
    
    //格式化,统计该单式数目
    lotteryCommonPage.dealForDsFormat(false);
};


/**
 * 前三或者后三直选和值,注数
 */
SscData.prototype.getLotteryCountByShanZhiHeZhi = function(everyPositionCodesInt){
	var cathecticCount = 0;
	if(everyPositionCodesInt == 0){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 1){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 2){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 3){
		cathecticCount += 10;
	}else if(everyPositionCodesInt == 4){
		cathecticCount += 15;
	}else if(everyPositionCodesInt == 5){
		cathecticCount += 21;
	}else if(everyPositionCodesInt == 6){
		cathecticCount += 28;
	}else if(everyPositionCodesInt == 7){
		cathecticCount += 36;
	}else if(everyPositionCodesInt == 8){
		cathecticCount += 45;
	}else if(everyPositionCodesInt == 9){
		cathecticCount += 55;
	}else if(everyPositionCodesInt == 10){
		cathecticCount += 63;
	}else if(everyPositionCodesInt == 11){
		cathecticCount += 69;
	}else if(everyPositionCodesInt == 12){
		cathecticCount += 73;
	}else if(everyPositionCodesInt == 13){
		cathecticCount += 75;
	}else if(everyPositionCodesInt == 14){
		cathecticCount += 75;
	}else if(everyPositionCodesInt == 15){
		cathecticCount += 73;
	}else if(everyPositionCodesInt == 16){
		cathecticCount += 69;
	}else if(everyPositionCodesInt == 17){
		cathecticCount += 63;
	}else if(everyPositionCodesInt == 18){
		cathecticCount += 55;
	}else if(everyPositionCodesInt == 19){
		cathecticCount += 45;
	}else if(everyPositionCodesInt == 20){
		cathecticCount += 36;
	}else if(everyPositionCodesInt == 21){
		cathecticCount += 28;
	}else if(everyPositionCodesInt == 22){
		cathecticCount += 21;
	}else if(everyPositionCodesInt == 23){
		cathecticCount += 15;
	}else if(everyPositionCodesInt == 24){
		cathecticCount += 10;
	}else if(everyPositionCodesInt == 25){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 26){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 27){
		cathecticCount += 1;
	}else{
		showTip("无该和值的号码");
	}
	return cathecticCount;	
};

/**
 * 前三或者后三直选和值,注数
 */
SscData.prototype.getLotteryCountByShanZuHeZhi = function(everyPositionCodesInt){
	var cathecticCount = 0;
    if(everyPositionCodesInt == 1){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 2){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 3){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 4){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 5){
		cathecticCount += 5;
	}else if(everyPositionCodesInt == 6){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 7){
		cathecticCount += 8;
	}else if(everyPositionCodesInt == 8){
		cathecticCount += 10;
	}else if(everyPositionCodesInt == 9){
		cathecticCount += 11;
	}else if(everyPositionCodesInt == 10){
		cathecticCount += 13;
	}else if(everyPositionCodesInt == 11){
		cathecticCount += 14;
	}else if(everyPositionCodesInt == 12){
		cathecticCount += 14;
	}else if(everyPositionCodesInt == 13){
		cathecticCount += 15;
	}else if(everyPositionCodesInt == 14){
		cathecticCount += 15;
	}else if(everyPositionCodesInt == 15){
		cathecticCount += 14;
	}else if(everyPositionCodesInt == 16){
		cathecticCount += 14;
	}else if(everyPositionCodesInt == 17){
		cathecticCount += 13;
	}else if(everyPositionCodesInt == 18){
		cathecticCount += 11;
	}else if(everyPositionCodesInt == 19){
		cathecticCount += 10;
	}else if(everyPositionCodesInt == 20){
		cathecticCount += 8;
	}else if(everyPositionCodesInt == 21){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 22){
		cathecticCount += 5;
	}else if(everyPositionCodesInt == 23){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 24){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 25){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 26){
		cathecticCount += 1;
	}else{
		showTip("无该和值的号码");
	}
	return cathecticCount;	
};

/**
 * 前二或者后二直选和值,注数
 */
SscData.prototype.getLotteryCountByErZhiHeZhi = function(everyPositionCodesInt){
	var cathecticCount = 0;
	if(everyPositionCodesInt == 0){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 1){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 2){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 3){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 4){
		cathecticCount += 5;
	}else if(everyPositionCodesInt == 5){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 6){
		cathecticCount += 7;
	}else if(everyPositionCodesInt == 7){
		cathecticCount += 8;
	}else if(everyPositionCodesInt == 8){
		cathecticCount += 9;
	}else if(everyPositionCodesInt == 9){
		cathecticCount += 10;
	}else if(everyPositionCodesInt == 10){
		cathecticCount += 9;
	}else if(everyPositionCodesInt == 11){
		cathecticCount += 8;
	}else if(everyPositionCodesInt == 12){
		cathecticCount += 7;
	}else if(everyPositionCodesInt == 13){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 14){
		cathecticCount += 5;
	}else if(everyPositionCodesInt == 15){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 16){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 17){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 18){
		cathecticCount += 1;
	}else{
		showTip("无该和值的号码");
	}
	return cathecticCount;
};

/**
 * 前三或者后三直选和值,注数
 */
SscData.prototype.getLotteryCountByErZuHeZhi = function(everyPositionCodesInt){
	var cathecticCount = 0;
	if(everyPositionCodesInt == 1){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 2){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 3){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 4){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 5){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 6){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 7){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 8){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 9){
		cathecticCount += 5;
	}else if(everyPositionCodesInt == 10){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 11){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 12){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 13){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 14){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 15){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 16){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 17){
		cathecticCount += 1;
	}else{
		showTip("无该和值的号码");
	}
	return cathecticCount;
};
