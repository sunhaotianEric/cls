function SyxwData(){}
var syxwData = new SyxwData();

//任选参数
syxwData.rxParam = {
	isRxDsAllow : true,  //是否允许任选单式,由位数来进行控制
    dsAllowCount : 0,
};


//通用的号码数组
syxwData.common_code_array = new Array("01","02","03","04","05","06","07","08","09","10","11");
//大小奇偶
syxwData.dxds_code_array = new Array(1,2,3,4);
//趣味定单双
syxwData.qwdds_code_array = new Array("01","02","03","04","05","06");
//趣味猜中位
syxwData.qwczw_code_array = new Array("03","04","05","06","07","08","09");

//////////////////玩法//////////////////// 
syxwData.plays = {};
//选一玩法
syxwData.plays.XY_PLAY = "";
syxwData.plays.XY_PLAY += "<div class='container clist XY animated' >";
syxwData.plays.XY_PLAY += "	<div class='list-line'>";
syxwData.plays.XY_PLAY += "		<div class='line-title'><p>复式</p></div>";
syxwData.plays.XY_PLAY += "		<div class='line-content'>";
syxwData.plays.XY_PLAY += "			<div class='child color-red' data-role-id='XYQSYMBDW'><p>前三一码不定位</p></div>";
syxwData.plays.XY_PLAY += "			<div class='child color-red no' data-role-id='XYRXYZYFS'><p>任选一中一</p></div>";
syxwData.plays.XY_PLAY += "		</div>";
syxwData.plays.XY_PLAY += "	</div>";
syxwData.plays.XY_PLAY += "	<div class='list-line'>";
syxwData.plays.XY_PLAY += "		<div class='line-title'><p>单式</p></div>";
syxwData.plays.XY_PLAY += "		<div class='line-content'>";
syxwData.plays.XY_PLAY += "			<div class='child color-red no' data-role-id='XYRXYZYDS'><p>任选一中一</p></div>";
syxwData.plays.XY_PLAY += "		</div>";
syxwData.plays.XY_PLAY += "	</div>";
syxwData.plays.XY_PLAY += "</div>";


//选二玩法
syxwData.plays.XE_PLAY = "";
syxwData.plays.XE_PLAY += "<div class='container clist XE animated'>";
syxwData.plays.XE_PLAY += "	<div class='list-line'>";
syxwData.plays.XE_PLAY += "		<div class='line-title'><p>复式</p></div>";
syxwData.plays.XE_PLAY += "		<div class='line-content'>";
syxwData.plays.XE_PLAY += "			<div class='child color-red no' data-role-id='XEQEZXFS'><p>前二直选</p></div>";
syxwData.plays.XE_PLAY += "			<div class='child color-red no' data-role-id='XEQEZXFS_G'><p>前二组选</p></div>";
syxwData.plays.XE_PLAY += "			<div class='child color-red no' data-role-id='XERXEZEFS'><p>任选二中二</p></div>";
syxwData.plays.XE_PLAY += "		</div>";
syxwData.plays.XE_PLAY += "	</div>";
syxwData.plays.XE_PLAY += "	<div class='list-line'>";
syxwData.plays.XE_PLAY += "		<div class='line-title'><p>单式</p></div>";
syxwData.plays.XE_PLAY += "		<div class='line-content'>";
syxwData.plays.XE_PLAY += "			<div class='child color-red no' data-role-id='XEQEZXDS'><p>前二直选</p></div>";
syxwData.plays.XE_PLAY += "			<div class='child color-red no' data-role-id='XEQEZXDS_G'><p>前二组选</p></div>";
syxwData.plays.XE_PLAY += "			<div class='child color-red no' data-role-id='XERXEZEDS'><p>任选二中二</p></div>";
syxwData.plays.XE_PLAY += "		</div>";
syxwData.plays.XE_PLAY += "	</div>";
syxwData.plays.XE_PLAY += "</div>";

//选三玩法
syxwData.plays.XS_PLAY = "";
syxwData.plays.XS_PLAY += "<div class='container clist XS animated'>";
syxwData.plays.XS_PLAY += "	<div class='list-line'>";
syxwData.plays.XS_PLAY += "		<div class='line-title'><p>复式</p></div>";
syxwData.plays.XS_PLAY += "		<div class='line-content'>";
syxwData.plays.XS_PLAY += "			<div class='child color-red no' data-role-id='XSQSZXFS'><p>前三直选</p></div>";
syxwData.plays.XS_PLAY += "			<div class='child color-red no' data-role-id='XSQSZXFS_G'><p>前三组选</p></div>";
syxwData.plays.XS_PLAY += "			<div class='child color-red no' data-role-id='XSRXSZSFS'><p>选三中三</p></div>";
syxwData.plays.XS_PLAY += "		</div>";
syxwData.plays.XS_PLAY += "	</div>";
syxwData.plays.XS_PLAY += "	<div class='list-line'>";
syxwData.plays.XS_PLAY += "		<div class='line-title'><p>单式</p></div>";
syxwData.plays.XS_PLAY += "		<div class='line-content'>";
syxwData.plays.XS_PLAY += "			<div class='child color-red no' data-role-id='XSQSZXDS'><p>前三直选</p></div>";
syxwData.plays.XS_PLAY += "			<div class='child color-red no' data-role-id='XSQSZXDS_G'><p>前三组选</p></div>";
syxwData.plays.XS_PLAY += "			<div class='child color-red no' data-role-id='XSRXSZSDS'><p>选三中三</p></div>";
syxwData.plays.XS_PLAY += "		</div>";
syxwData.plays.XS_PLAY += "	</div>";
syxwData.plays.XS_PLAY += "</div>";

//选四玩法
syxwData.plays.XSH_PLAY = "";
syxwData.plays.XSH_PLAY += "<div class='container clist XSH animated'>";
syxwData.plays.XSH_PLAY += "	<div class='list-line'>";
syxwData.plays.XSH_PLAY += "		<div class='line-title'><p>复式</p></div>";
syxwData.plays.XSH_PLAY += "		<div class='line-content'>";
syxwData.plays.XSH_PLAY += "			<div class='child color-red no' data-role-id='XSHRXSZSFS'><p>任选四中四</p></div>";
syxwData.plays.XSH_PLAY += "		</div>";
syxwData.plays.XSH_PLAY += "	</div>";
syxwData.plays.XSH_PLAY += "	<div class='list-line'>";
syxwData.plays.XSH_PLAY += "		<div class='line-title'><p>单式</p></div>";
syxwData.plays.XSH_PLAY += "		<div class='line-content'>";
syxwData.plays.XSH_PLAY += "			<div class='child color-red no' data-role-id='XSHRXSZSDS'><p>任选四中四</p></div>";
syxwData.plays.XSH_PLAY += "		</div>";
syxwData.plays.XSH_PLAY += "	</div>";
syxwData.plays.XSH_PLAY += "</div>";

//选五玩法
syxwData.plays.XW_PLAY = "";
syxwData.plays.XW_PLAY += "<div class='container clist XW animated'>";
syxwData.plays.XW_PLAY += "	<div class='list-line'>";
syxwData.plays.XW_PLAY += "		<div class='line-title'><p>复式</p></div>";
syxwData.plays.XW_PLAY += "		<div class='line-content'>";
syxwData.plays.XW_PLAY += "			<div class='child color-red no' data-role-id='XWRXWZWFS'><p>任选五中五</p></div>";
syxwData.plays.XW_PLAY += "		</div>";
syxwData.plays.XW_PLAY += "	</div>";
syxwData.plays.XW_PLAY += "	<div class='list-line'>";
syxwData.plays.XW_PLAY += "		<div class='line-title'><p>单式</p></div>";
syxwData.plays.XW_PLAY += "		<div class='line-content'>";
syxwData.plays.XW_PLAY += "			<div class='child color-red no' data-role-id='XWRXWZWDS'><p>任选五中五</p></div>";
syxwData.plays.XW_PLAY += "		</div>";
syxwData.plays.XW_PLAY += "	</div>";
syxwData.plays.XW_PLAY += "</div>";

//选六玩法
syxwData.plays.XL_PLAY = "";
syxwData.plays.XL_PLAY += "<div class='container clist XL animated'>";
syxwData.plays.XL_PLAY += "	<div class='list-line'>";
syxwData.plays.XL_PLAY += "		<div class='line-title'><p>复式</p></div>";
syxwData.plays.XL_PLAY += "		<div class='line-content'>";
syxwData.plays.XL_PLAY += "			<div class='child color-red no' data-role-id='XLRXLZWFS'><p>任选六中五</p></div>";
syxwData.plays.XL_PLAY += "		</div>";
syxwData.plays.XL_PLAY += "	</div>";
syxwData.plays.XL_PLAY += "	<div class='list-line'>";
syxwData.plays.XL_PLAY += "		<div class='line-title'><p>单式</p></div>";
syxwData.plays.XL_PLAY += "		<div class='line-content'>";
syxwData.plays.XL_PLAY += "			<div class='child color-red no' data-role-id='XLRXLZWDS'><p>任选六中五</p></div>";
syxwData.plays.XL_PLAY += "		</div>";
syxwData.plays.XL_PLAY += "	</div>";
syxwData.plays.XL_PLAY += "</div>";

//选七玩法
syxwData.plays.XQ_PLAY = "";
syxwData.plays.XQ_PLAY += "<div class='container clist XQ animated'>";
syxwData.plays.XQ_PLAY += "	<div class='list-line'>";
syxwData.plays.XQ_PLAY += "		<div class='line-title'><p>复式</p></div>";
syxwData.plays.XQ_PLAY += "		<div class='line-content'>";
syxwData.plays.XQ_PLAY += "			<div class='child color-red no' data-role-id='XQRXQZWFS'><p>任选七中五</p></div>";
syxwData.plays.XQ_PLAY += "		</div>";
syxwData.plays.XQ_PLAY += "	</div>";
syxwData.plays.XQ_PLAY += "	<div class='list-line'>";
syxwData.plays.XQ_PLAY += "		<div class='line-title'><p>单式</p></div>";
syxwData.plays.XQ_PLAY += "		<div class='line-content'>";
syxwData.plays.XQ_PLAY += "			<div class='child color-red no' data-role-id='XQRXQZWDS'><p>任选七中五</p></div>";
syxwData.plays.XQ_PLAY += "		</div>";
syxwData.plays.XQ_PLAY += "	</div>";
syxwData.plays.XQ_PLAY += "</div>";

//选八玩法
syxwData.plays.XB_PLAY = "";
syxwData.plays.XB_PLAY += "<div class='container clist XB animated'>";
syxwData.plays.XB_PLAY += "	<div class='list-line'>";
syxwData.plays.XB_PLAY += "		<div class='line-title'><p>复式</p></div>";
syxwData.plays.XB_PLAY += "		<div class='line-content'>";
syxwData.plays.XB_PLAY += "			<div class='child color-red no' data-role-id='XBRXBZWFS'><p>任选八中五</p></div>";
syxwData.plays.XB_PLAY += "		</div>";
syxwData.plays.XB_PLAY += "	</div>";
syxwData.plays.XB_PLAY += "	<div class='list-line'>";
syxwData.plays.XB_PLAY += "		<div class='line-title'><p>单式</p></div>";
syxwData.plays.XB_PLAY += "		<div class='line-content'>";
syxwData.plays.XB_PLAY += "			<div class='child color-red no' data-role-id='XBRXBZWDS'><p>任选八中五</p></div>";
syxwData.plays.XB_PLAY += "		</div>";
syxwData.plays.XB_PLAY += "	</div>";
syxwData.plays.XB_PLAY += "</div>";

//趣味
syxwData.plays.QW_PLAY = "";
syxwData.plays.QW_PLAY += "<div class='container clist QW animated'>";
syxwData.plays.QW_PLAY += "	<div class='list-line'>";
syxwData.plays.QW_PLAY += "		<div class='line-title'><p>趣味</p></div>";
syxwData.plays.QW_PLAY += "		<div class='line-content'>";
syxwData.plays.QW_PLAY += "			<div class='child color-red no' data-role-id='QWDDS'><p>定单双</p></div>";
syxwData.plays.QW_PLAY += "			<div class='child color-red no' data-role-id='QWCZW'><p>猜中位</p></div>";
syxwData.plays.QW_PLAY += "		</div>";
syxwData.plays.QW_PLAY += "	</div>";
syxwData.plays.QW_PLAY += "</div>";

//////////////////玩法描述////////////////////
syxwData.des = {};
//选一
syxwData.des.XYQSYMBDW_DES = "玩法加载中，请稍后........"; 
syxwData.des.XYRXYZYFS_DES = "玩法加载中，请稍后........"; 
syxwData.des.XYRXYZYDS_DES = "玩法加载中，请稍后........";


//选二
syxwData.des.XEQEZXFS_DES = "玩法加载中，请稍后........";
syxwData.des.XEQEZXFS_G_DES = "玩法加载中，请稍后........";
syxwData.des.XERXEZEFS_DES = "玩法加载中，请稍后........";
syxwData.des.XEQEZXDS_DES = "玩法加载中，请稍后........";
syxwData.des.XEQEZXDS_G_DES = "玩法加载中，请稍后........";
syxwData.des.XERXEZEDS_DES = "玩法加载中，请稍后........";


//选三
syxwData.des.XSQSZXFS_DES = "玩法加载中，请稍后........";
syxwData.des.XSQSZXFS_G_DES = "玩法加载中，请稍后........";
syxwData.des.XSRXSZSFS_DES = "玩法加载中，请稍后........";
syxwData.des.XSQSZXDS_DES = "玩法加载中，请稍后........";
syxwData.des.XSQSZXDS_G_DES = "玩法加载中，请稍后........";
syxwData.des.XSRXSZSDS_DES = "玩法加载中，请稍后........";


//选四
syxwData.des.XSHRXSZSFS_DES = "玩法加载中，请稍后........";
syxwData.des.XSHRXSZSDS_DES = "玩法加载中，请稍后........";


//选五
syxwData.des.XWRXWZWFS_DES = "玩法加载中，请稍后........";
syxwData.des.XWRXWZWDS_DES = "玩法加载中，请稍后........";


//选六
syxwData.des.XLRXLZWFS_DES = "玩法加载中，请稍后........";
syxwData.des.XLRXLZWDS_DES = "玩法加载中，请稍后........";


//选七
syxwData.des.XQRXQZWFS_DES = "玩法加载中，请稍后........";
syxwData.des.XQRXQZWDS_DES = "玩法加载中，请稍后........";


//选八
syxwData.des.XBRXBZWFS_DES = "玩法加载中，请稍后........";
syxwData.des.XBRXBZWDS_DES = "玩法加载中，请稍后........";


//趣味
syxwData.des.QWDDS_DES = "玩法加载中，请稍后........";
syxwData.des.QWCZW_DES = "玩法加载中，请稍后........";


//玩法描述映射值
syxwData.lotteryKindMap = new JS_OBJECT_MAP();
//选一
syxwData.lotteryKindMap.put("XYQSYMBDW", "选一_前三一码不定位");
syxwData.lotteryKindMap.put("XYRXYZYFS", "选一_任选一中一复式");
syxwData.lotteryKindMap.put("XYRXYZYDS", "选一_任选一中一单式");

//选二
syxwData.lotteryKindMap.put("XEQEZXFS", "选二_前二直选复式");
syxwData.lotteryKindMap.put("XEQEZXDS", "选二_前二直选单式");
syxwData.lotteryKindMap.put("XEQEZXFS_G", "选二_前二组选复式");
syxwData.lotteryKindMap.put("XEQEZXDS_G", "选二_前二组选单式");
syxwData.lotteryKindMap.put("XERXEZEFS", "选二_任选二中二复式");
syxwData.lotteryKindMap.put("XERXEZEDS", "选二_任选二中二单式");

//选三
syxwData.lotteryKindMap.put("XSQSZXFS", "选三_前三直选复式");
syxwData.lotteryKindMap.put("XSQSZXDS", "选三_前三直选单式");
syxwData.lotteryKindMap.put("XSQSZXFS_G", "选三_前三组选复式");
syxwData.lotteryKindMap.put("XSQSZXDS_G", "选三_前三组选单式");
syxwData.lotteryKindMap.put("XSRXSZSFS", "选三_任选三中三复式");
syxwData.lotteryKindMap.put("XSRXSZSDS", "选三_任选三中三单式");

//选四
syxwData.lotteryKindMap.put("XSHRXSZSFS", "选四_任选四中四复式");
syxwData.lotteryKindMap.put("XSHRXSZSDS", "选四_任选四中四单式");

//选五
syxwData.lotteryKindMap.put("XWRXWZWFS", "选五_任选五中五复式");
syxwData.lotteryKindMap.put("XWRXWZWDS", "选五_任选五中五单式");

//选六
syxwData.lotteryKindMap.put("XLRXLZWFS", "选六_任选六中五复式");
syxwData.lotteryKindMap.put("XLRXLZWDS", "选六_任选六中五单式");

//选七
syxwData.lotteryKindMap.put("XQRXQZWFS", "选七_任选七中五复式");
syxwData.lotteryKindMap.put("XQRXQZWDS", "选七_任选七中五单式");

//选八
syxwData.lotteryKindMap.put("XBRXBZWFS", "选八_任选八中五复式");
syxwData.lotteryKindMap.put("XBRXBZWDS", "选八_任选八中五单式");

//趣味
syxwData.lotteryKindMap.put("QWDDS", "趣味_定单双");
syxwData.lotteryKindMap.put("QWCZW", "趣味_猜中位");

/**
 * 加载所有的玩法
 */
SyxwData.prototype.loadLotteryKindPlay = function() {
	$("#lotteryKindPlayList").append(syxwData.plays.XY_PLAY);
	$("#lotteryKindPlayList").append(syxwData.plays.XE_PLAY);
	$("#lotteryKindPlayList").append(syxwData.plays.XS_PLAY);
	$("#lotteryKindPlayList").append(syxwData.plays.XSH_PLAY);
	$("#lotteryKindPlayList").append(syxwData.plays.XW_PLAY);
	$("#lotteryKindPlayList").append(syxwData.plays.XL_PLAY);
	$("#lotteryKindPlayList").append(syxwData.plays.XQ_PLAY);
	$("#lotteryKindPlayList").append(syxwData.plays.XB_PLAY);
	$("#lotteryKindPlayList").append(syxwData.plays.QW_PLAY);
}


/**
 * 玩法选择显示描述
 * @param roleId
 */
SyxwData.prototype.showPlayDes = function(roleId) {
	var playDes = "";
	if (roleId == 'XYQSYMBDW') {
		playDes = syxwData.des.XYQSYMBDW_DES;
	} else if (roleId == 'XYRXYZYFS') {
		playDes = syxwData.des.XYRXYZYFS_DES;
	} else if (roleId == 'XYRXYZYDS') {
		playDes = syxwData.des.XYRXYZYDS_DES;
	} else if (roleId == 'XEQEZXFS') {
		playDes = syxwData.des.XEQEZXFS_DES;
	} else if (roleId == 'XEQEZXFS_G') {
		playDes = syxwData.des.XEQEZXFS_G_DES;
	} else if (roleId == 'XERXEZEFS') {
		playDes = syxwData.des.XERXEZEFS_DES;
	} else if (roleId == 'XEQEZXDS') {
		playDes = syxwData.des.XEQEZXDS_DES;
	} else if (roleId == 'XEQEZXDS_G') {
		playDes = syxwData.des.XEQEZXDS_G_DES;
	} else if (roleId == 'XERXEZEDS') {
		playDes = syxwData.des.XERXEZEDS_DES;
	} else if (roleId == 'XSQSZXFS') {
		playDes = syxwData.des.XSQSZXFS_DES;
	} else if (roleId == 'XSQSZXFS_G') {
		playDes = syxwData.des.XSQSZXFS_G_DES;
	} else if (roleId == 'XSRXSZSFS') {
		playDes = syxwData.des.XSRXSZSFS_DES;
	} else if (roleId == 'XSQSZXDS') {
		playDes = syxwData.des.XSQSZXDS_DES;
	} else if (roleId == 'XSQSZXDS_G') {
		playDes = syxwData.des.XSQSZXDS_G_DES;
	} else if (roleId == 'XSRXSZSDS') {
		playDes = syxwData.des.XSRXSZSDS_DES;
	} else if (roleId == 'XSHRXSZSFS') {
		playDes = syxwData.des.XSHRXSZSFS_DES;
	} else if (roleId == 'XSHRXSZSDS') {
		playDes = syxwData.des.XSHRXSZSDS_DES;
	} else if (roleId == 'XWRXWZWFS') {
		playDes = syxwData.des.XWRXWZWFS_DES;
	} else if (roleId == 'XWRXWZWDS') {
		playDes = syxwData.des.XWRXWZWDS_DES;
	} else if (roleId == 'XLRXLZWFS') {
		playDes = syxwData.des.XLRXLZWFS_DES;
	} else if (roleId == 'XLRXLZWDS') {
		playDes = syxwData.des.XLRXLZWDS_DES;
	} else if (roleId == 'XQRXQZWFS') {
		playDes = syxwData.des.XQRXQZWFS_DES;
	} else if (roleId == 'XQRXQZWDS') {
		playDes = syxwData.des.XQRXQZWDS_DES;
	} else if (roleId == 'XBRXBZWFS') {
		playDes = syxwData.des.XBRXBZWFS_DES;
	} else if (roleId == 'XBRXBZWDS') {
		playDes = syxwData.des.XBRXBZWDS_DES;
	} else if (roleId == 'QWDDS') {
		playDes = syxwData.des.QWDDS_DES;
	} else if (roleId == 'QWCZW') {
		playDes = syxwData.des.QWCZW_DES;
	} else {
		showTip("不可知的彩种玩法描述.");
		return;
	}
	$("#playDes").html(playDes);
};

/**
 * 玩法选择
 * @param roleId
 */
SyxwData.prototype.lotteryKindPlayChoose = function(roleId) {

	var desArray = new Array("第一位", "第二位", "第三位", "第四位", "第五位");
	var emptyDesArray = new Array("");
	if(roleId == 'XYQSYMBDW'){
		syxwData.ballSectionCommon(0,0,emptyDesArray, null, false);
	  }else if(roleId == 'XYRXYZYFS'){
		  syxwData.ballSectionCommon(0,0,emptyDesArray, null, false);
	  }else if(roleId == 'XYRXYZYDS'){
		  syxwData.dsBallSectionCommon(roleId);
	  }else if(roleId == 'XEQEZXFS'){
		  syxwData.ballSectionCommon(0,1,desArray,null,true);
	  }else if(roleId == 'XEQEZXDS'){
		  syxwData.dsBallSectionCommon(roleId);
	  }else if(roleId == 'XEQEZXFS_G'){
		  syxwData.ballSectionCommon(0,0,emptyDesArray, null, false);
	  }else if(roleId == 'XEQEZXDS_G'){
		  syxwData.dsBallSectionCommon(roleId);
	  }else if(roleId == 'XERXEZEFS'){
		  syxwData.ballSectionCommon(0,0,emptyDesArray, null, false);
	  }else if(roleId == 'XERXEZEDS'){
		  syxwData.dsBallSectionCommon(roleId);
	  }else if(roleId == 'XSQSZXFS'){
		  syxwData.ballSectionCommon(0,2,desArray,null,true);
	  }else if(roleId == 'XSQSZXDS'){
		  syxwData.dsBallSectionCommon(roleId);
	  }else if(roleId == 'XSQSZXFS_G'){
		  syxwData.ballSectionCommon(0,0,emptyDesArray, null, false);
	  }else if(roleId == 'XSQSZXDS_G'){
		  syxwData.dsBallSectionCommon(roleId);
	  }else if(roleId == 'XSRXSZSFS'){
		  syxwData.ballSectionCommon(0,0,emptyDesArray, null, false);
	  }else if(roleId == 'XSRXSZSDS'){
		  syxwData.dsBallSectionCommon(roleId);
	  }else if(roleId == 'XSHRXSZSFS'){
		  syxwData.ballSectionCommon(0,0,emptyDesArray, null, false);
	  }else if(roleId == 'XSHRXSZSDS'){
		  syxwData.dsBallSectionCommon(roleId);
	  }else if(roleId == 'XWRXWZWFS'){
		  syxwData.ballSectionCommon(0,0,emptyDesArray, null, false);
	  }else if(roleId == 'XWRXWZWDS'){
		  syxwData.dsBallSectionCommon(roleId);
	  }else if(roleId == 'XLRXLZWFS'){
		  syxwData.ballSectionCommon(0,0,emptyDesArray,null,true);
	  }else if(roleId == 'XLRXLZWDS'){
		  syxwData.dsBallSectionCommon(roleId);
	  }else if(roleId == 'XQRXQZWFS'){
		  syxwData.ballSectionCommon(0,0,emptyDesArray, null, false);
	  }else if(roleId == 'XQRXQZWDS'){
		  syxwData.dsBallSectionCommon(roleId);
	  }else if(roleId == 'XBRXBZWFS'){
		  syxwData.ballSectionCommon(0,0,emptyDesArray, null, false);
	  }else if(roleId == 'XBRXBZWDS'){
		  syxwData.dsBallSectionCommon(roleId);
	  }else if(roleId == 'QWDDS'){
		  syxwData.ballSectionCommon(0,0,emptyDesArray,roleId,false);
	  }else if(roleId == 'QWCZW'){
		  syxwData.ballSectionCommon(0,0,emptyDesArray,roleId,false);
	  }else{
		  alert("不可知的彩种玩法.");
	  }  
	
	//存储当前切换的玩法模式
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey = roleId;
	
	//显示玩法名称
	var currentKindName = syxwData.lotteryKindMap.get(roleId);
    $("#currentKindName").text(currentKindName);

	//显示玩法说明
	syxwData.showPlayDes(roleId);
	
	//拼接完，进行事件的重新申明
	lotteryCommonPage.dealForEvent();
};





/**
 * 复式玩法的号码拼接
 * desArray 描述的数组
 * specialKind 特殊玩法
 * isOmitColdDataDeal 是否遗漏和冷热数据的处理
 */
SyxwData.prototype.ballSectionCommon = function(numCountStart,numCountEnd,desArray,specialKind,isOmitColdDataDeal){
	var content = "";
	
	if(specialKind == null || specialKind == "QWCZW"){
		
		//大小奇偶按钮字符串
		var dxjoStr='		<div class="content">'+
					'			<div class="mun" data-role-type="all">全</div>'+
					'			<div class="mun" data-role-type="big">大</div>'+
					'			<div class="mun" data-role-type="small">小</div>'+
					'			<div class="mun" data-role-type="odd">奇</div>'+
					'			<div class="mun" data-role-type="even">偶</div>'+
					'			<div class="mun" data-role-type="empty">清</div>'+
					'		</div>';
		
		for(var i = numCountStart;i <= numCountEnd; i++){
			if(i % 2 == 0) {
				content += '<div class="lottery-child main odd"><div class="container">';
			} else {
				content += '<div class="lottery-child main even"><div class="container">';
			}
			var numDes = desArray[i];
			
			if(isNotEmpty(numDes)) {
				content += '<div class="child-head">';
				content += '	<div class="title"><p>' + numDes + '</p></div>';
				content += dxjoStr;
				//遗漏冷热元素处理
				if(isOmitColdDataDeal) {
					content += "";
				}
				content += '</div>';
			}else {
				content += '<div class="child-head first-block">';
				content += dxjoStr;
				//遗漏冷热元素处理
				if(isOmitColdDataDeal) {
					content += "";
				}
				content += '</div>';
			}
			
			/*if(isNotEmpty(numDes)) {
				//content += '<div class="child-head">';
				content += '	<div class="title"><p>' + numDes + '</p></div>';
				//大小单双
				//content += dxjoStr;
				//遗漏冷热元素处理
				if(isOmitColdDataDeal) {
					content += "";
				}
				//content += '</div>';
			}*/
			content += '<div class="child-content">';
			
			//只有单行的情况 号码排列居中
			if(numCountStart == numCountEnd) {
				content += '	<div class="content first-block">';
			} else {
				content += '	<div class="content">';
			}
			
			//趣味猜中位
			if(specialKind == "QWCZW"){
				syxwData.common_code_array = syxwData.qwczw_code_array;
			}
			
			for(var j = 0; j < syxwData.common_code_array.length; j++){
				content += '<div class="munbtn mun" data-realvalue="'+ syxwData.common_code_array[j] +'" name="ballNum_'+i+'_match">'+ syxwData.common_code_array[j] +'</div>';
				//遗漏冷热元素处理
				if(isOmitColdDataDeal) {
					content += "";
				}
			}
			content += '	</div>';
			content += '</div>';
			content += '</div>';
			content += '</div>';
		}
	}else{ 
		var qw_code_array = null;
		if(specialKind == "QWDDS"){
			//趣味定单双
			qw_code_array = syxwData.qwdds_code_array;
		}
		
		if(qw_code_array != null) {
			content += '<div class="lottery-child main odd"><div class="container">';
			var numDes = desArray[i];
			if(isNotEmpty(numDes)) {
				content += '<div class="child-head">';
				content += '	<div class="title"><p>' + numDes + '</p></div>';
				//遗漏冷热元素处理
				if(isOmitColdDataDeal) {
					content += "";
				}
				content += '</div>';
			}
			content += '<div class="child-content">';
			content += '	<div class="content first-block">';
			for(var j = 0; j < qw_code_array.length; j++){
				content += '<div class="munbtn bigbtn mun" data-realvalue="'+ qw_code_array[j] +'" name="ballNum_'+i+'_match">';
				
				if(syxwData.qwdds_code_array[j] == "01"){
					content += "5单0双";
				}else if(syxwData.qwdds_code_array[j] == "02"){
					content += "4单1双";
				}else if(syxwData.qwdds_code_array[j] == "03"){
					content += "3单2双";
				}else if(syxwData.qwdds_code_array[j] == "04"){
					content += "2单3双";
				}else if(syxwData.qwdds_code_array[j] == "05"){
					content += "1单4双";
				}else if(syxwData.qwdds_code_array[j] == "06"){
					content += "0单5双";
				}
				
				//遗漏冷热元素处理
				if(isOmitColdDataDeal) {
					content += "";
				}
				content += '</div>';
			}
			content += '	</div>';
			content += '</div>';
			content += '</div>';
			content += '</div>';
		} else{
			showTip("未找到该玩法的号码.");
		}
	}
    $("#ballSection").html(content);
    //任选框隐藏
    $("#rxWeiCheck").hide();
    //隐藏单式的导入框
    $("#importDsBtn").hide();
    
    //进行遗漏冷热的数值处理
    if(isOmitColdDataDeal){
    	$("#ommitHotColdBtn").show();
    } else {
    	$("#ommitHotColdBtn").hide();
    }
};

/**
 * 单式玩法的号码拼接
 * specialKind 特殊玩法
 */
SyxwData.prototype.dsBallSectionCommon = function(specialKind){
	
	var editorStr = "说明：\n";
	editorStr += "1、请参照标准格式样本格式录入或上传方案。\n";
	editorStr += "2、每一注号码之间的间隔符支持 回车 空格[ ] 逗号[,] 分号[;] 冒号[:] 竖线 [|]。\n";
	editorStr += "3、文件格式必须是.txt格式。\n";
	editorStr += "4、文件较大时会导致上传时间较长，请耐心等待！\n";
	editorStr += "5、将文件拖入文本框即可快速实现文件上传功能。\n";
	editorStr += "6、导入文本内容后将覆盖文本框中现有的内容。";

	var rxContent = '<div class="lottery-select">'+
	'<div class="container lottery-select-list">'+
	'	<label for="">'+
	'		<input type="checkbox" name="ds_position" onclick="lotteryCommonPage.lottertyDataDeal.dsPositionCheckFunc(this);" data-position="1" checked="checked">'+
	'		<span>第一位</span>'+
	'	</label>'+
	'	<label for="">'+
	'		<input type="checkbox" name="ds_position" onclick="lotteryCommonPage.lottertyDataDeal.dsPositionCheckFunc(this);" data-position="2" checked="checked">'+
	'		<span>千位</span>'+
	'	</label>'+
	'	<label for="">'+
	'		<input type="checkbox" name="ds_position" onclick="lotteryCommonPage.lottertyDataDeal.dsPositionCheckFunc(this);" data-position="3" checked="checked">'+
	'		<span>百位</span>'+
	'	</label>'+
	'	<label for="">'+
	'		<input type="checkbox" name="ds_position" onclick="lotteryCommonPage.lottertyDataDeal.dsPositionCheckFunc(this);" data-position="4" checked="checked">'+
	'		<span>十位</span>'+
	'	</label>'+
	'	<label for="">'+
	'		<input type="checkbox" name="ds_position" onclick="lotteryCommonPage.lottertyDataDeal.dsPositionCheckFunc(this);" data-position="5" checked="checked">'+
	'		<span>个位</span>'+
	'	</label>'+
	'</div>'+
	'<div class="container lottery-select-msg" >'+
	'	<p>温馨提示：您选择了<span class="font-yellow" id="pnum">5</span>个位置，系统自动根据位置合成<span class="font-yellow" id="fnum">10</span>个方案</p>'+
	'</div>'+
	'</div>';
	
	var content = '	<div class="lottery-child main"><div class="container">'+
	'		<div class="lottery-textarea">'+
	'			<textarea id="lr_editor" placeholder="'+ editorStr +'"></textarea>'+
	'		</div>'+
	'	</div></div>';
	
	if(specialKind == "XYRXYZYDS" || specialKind == "XEQEZXDS" || specialKind == "XEQEZXDS_G"  || specialKind == "XERXEZEDS" || 
    		specialKind == "XSQSZXDS" || specialKind == "XSQSZXDS_G" || specialKind == "XSRXSZSDS"  || specialKind == "XSHRXSZSDS" || 
    		specialKind == "XWRXWZWDS" || specialKind == "XLRXLZWDS" || specialKind == "XQRXQZWDS"  || specialKind == "XBRXBZWDS"
    			) {
    	//拼接加上任选的内容
    	//content =  rxContent + content;
    	
    	//syxwData.rxParam.isRxDsAllow = true;
    	//全选
    	$("input[name='ds_position']").each(function(){
    		this.checked = true;
    	});
    	/*if(specialKind == "RXSIZXDS"){
    		syxwData.rxParam.dsAllowCount = 5;
    		$("#fnum").text(5);
    	}else if(specialKind == "RXEZXDS" || specialKind == "RXSZXDS"){
    		syxwData.rxParam.dsAllowCount = 10;
    		$("#fnum").text(10); 
    	}*/
    }else {
    	//任选框隐藏
    	$("#rxWeiCheck").hide();
    }
    $("#ballSection").html(content);
    
    //遗漏冷热隐藏
    $("#ommitHotColdBtn").hide();
    //显示单式的导入框
    $("#importDsBtn").show();
    
    //添加单式的事件处理
    lotteryCommonPage.addDsEvent();
}

/**
 * 计算当前用户最高模式下的奖金
 */
SyxwData.prototype.calucateCurrentUserHighestWin = function(kindKey){
	if(lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.size() == 0){
		return "奖金未完全获取,请再次操作或者刷新页面";
	}
	var currentLotteryWin;
	currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get(kindKey);
	if(currentLotteryWin == null){
		return "未配置该玩法的奖金.";
	}
	var result = (currentLotteryWin.winMoney + (currentUserSyxwModel - syxwLowestAwardModel) * currentLotteryWin.specCode) * 1;	
    return result;
};

/**
 * 获取当前用户最高模式下的奖金
 */
SyxwData.prototype.getKindPlayLotteryWin = function(kindKey){
	if(lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.size() == 0){
		return "奖金未完全获取,请稍作等待或者再次操作.";
	}
	var currentLotteryWin;
	currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get(lotteryCommonPage.currentLotteryKindInstance.param.kindNameType+ "_" +kindKey);
	if(currentLotteryWin == null){
		return "未配置该玩法的奖金.";
	}
	var result = (currentLotteryWin.winMoney + (currentUserSyxwModel - syxwLowestAwardModel) * currentLotteryWin.specCode) * 1;	
    return result.toFixed(lotteryCommonPage.param.toFixedValue);
};

/**
 * 设置当前用户最高模式下的奖金和玩法描述
 */
SyxwData.prototype.setKindPlayDes = function(){
	//选一前三一码不定位
	var XYQSYMBDW_WIN = syxwData.getKindPlayLotteryWin("XYQSYMBDW");
	syxwData.des.XYQSYMBDW_DES = "<div class='title'>从01-11共11个号码中选择1个号码，每注由1个号码组成，只要当期顺序摇出的第一位、第二位、第三位开奖号码中包含所选号码，即中奖，最高奖金\""+XYQSYMBDW_WIN+"\"元</div>";

	//选一任选一中一复式
	var XYRXYZYFS_WIN = syxwData.getKindPlayLotteryWin("XYRXYZYFS");
	syxwData.des.XYRXYZYFS_DES = "<div class='title'>从01-11共11个号码中选择1个号码进行购买，当期的5个开奖号码中包含所选号码，即中奖，最高奖金\""+XYRXYZYFS_WIN+"\"元</div>";

	//选一任选一中一单式
	var XYRXYZYDS_WIN = syxwData.getKindPlayLotteryWin("XYRXYZYDS");
	syxwData.des.XYRXYZYDS_DES = "<div class='title'>从01-11共11个号码中选择1个号码进行购买，当期的5个开奖号码中包含所选号码，即中奖，最高奖金\""+ XYRXYZYDS_WIN +"\"元</div>";

	//选二前二直选复式
	var XEQEZXFS_WIN = syxwData.getKindPlayLotteryWin("XEQEZXFS");
	syxwData.des.XEQEZXFS_DES = "<div class='title'>从01-11共11个号码中选择2个不重复的号码组成一注，所选号码与当期顺序摇出的5个号码中的前2个号码相同，且顺序一致，即中奖，最高奖金\""+ XEQEZXFS_WIN +"\"元</div>";

	//选二前二组选复式
	var XEQEZXFS_G_WIN = syxwData.getKindPlayLotteryWin("XEQEZXFS_G");
	syxwData.des.XEQEZXFS_G_DES = "<div class='title'> 至少选择2个二重号码和1个单号号码组成一注，竞猜开奖号码的全部五位，号码一致顺序不限即中奖，最高奖金\""+ XEQEZXFS_G_WIN +"\"元</div>";

	//选二任选二中二复式
	var XERXEZEFS_WIN = syxwData.getKindPlayLotteryWin("XERXEZEFS");
	syxwData.des.XERXEZEFS_DES = "<div class='title'>从01-11共11个号码中选择2个号码进行购买，只要当期的5个开奖号码中包含所选号码，即中奖，最高奖金\""+ XERXEZEFS_WIN +"\"元</div>";

	//选二前二直选单式
	var XEQEZXDS_WIN = syxwData.getKindPlayLotteryWin("XEQEZXDS");
	syxwData.des.XEQEZXDS_DES = "<div class='title'>从01-11共11个号码中选择2个不重复的号码组成一注，所选号码与当期顺序摇出的5个号码中的前2个号码相同，且顺序一致，即中奖，最高奖金\""+ XEQEZXDS_WIN +"\"元</div>";

	//选二前二组选单式
	var XEQEZXDS_G_WIN = syxwData.getKindPlayLotteryWin("XEQEZXDS_G");
	syxwData.des.XEQEZXDS_G_DES = "<div class='title'>从01-11共11个号码中选择2个不重复的号码组成一注，所选号码与当期顺序摇出的5个号码中的前2个号码相同，且顺序不限，即中奖，最高奖金\""+ XEQEZXDS_G_WIN +"\"元</div>";

	//选二任选二中二单式
	var XERXEZEDS_WIN = syxwData.getKindPlayLotteryWin("XERXEZEDS");
	syxwData.des.XERXEZEDS_DES = "<div class='title'>从01-11共11个号码中选择2个号码进行购买，只要当期的5个开奖号码中包含所选号码，即中奖，最高奖金\""+ XERXEZEDS_WIN +"\"元</div>";

	//选三前三直选复式
	var XSQSZXFS_WIN = syxwData.getKindPlayLotteryWin("XSQSZXFS");
	syxwData.des.XSQSZXFS_DES = "<div class='title'>从01-11共11个号码中选择3个不重复的号码组成一注，所选号码与当期顺序摇出的5个号码中的前3个号码相同，且顺序一致，即中奖，最高奖金\""+ XSQSZXFS_WIN +"\"元</div>";

	//选三前三组选复式
	var XSQSZXFS_G_WIN = syxwData.getKindPlayLotteryWin("XSQSZXFS_G");
	syxwData.des.XSQSZXFS_G_DES = "<div class='title'>从01-11共11个号码中选择3个不重复的号码组成一注，所选号码与当期顺序摇出的5个号码中的前3个号码相同，且顺序不限，即中奖，最高奖金\""+ XSQSZXFS_G_WIN +"\"元</div>";

	//选三选三中三复式
	var XSRXSZSFS_WIN = syxwData.getKindPlayLotteryWin("XSRXSZSFS");
	syxwData.des.XSRXSZSFS_DES = "<div class='title'>从01-11共11个号码中选择3个号码进行购买，只要当期的5个开奖号码中包含所选号码，即中奖，最高奖金\""+ XSRXSZSFS_WIN +"\"元</div>";
	
	//选三前三直选单式
	var XSQSZXDS_WIN = syxwData.getKindPlayLotteryWin("XSQSZXDS");
	syxwData.des.XSQSZXDS_DES = "<div class='title'>从01-11共11个号码中选择3个不重复的号码组成一注，所选号码与当期顺序摇出的5个号码中的前3个号码相同，且顺序一致，即中奖，最高奖金\""+ XSQSZXDS_WIN +"\"元</div>";

	//选三前三组选单式
	var XSQSZXDS_G_WIN = syxwData.getKindPlayLotteryWin("XSQSZXDS_G");
	syxwData.des.XSQSZXDS_G_DES = "<div class='title'>从01-11共11个号码中选择3个不重复的号码组成一注，所选号码与当期顺序摇出的5个号码中的前3个号码相同，且顺序不限，即中奖，最高奖金\""+ XSQSZXDS_G_WIN +"\"元</div>";

	//选三选三中三单式
	var XSRXSZSDS_WIN = syxwData.getKindPlayLotteryWin("XSRXSZSDS");
	syxwData.des.XSRXSZSDS_DES = "<div class='title'>从01-11共11个号码中选择3个号码进行购买，只要当期的5个开奖号码中包含所选号码，即中奖，最高奖金\""+ XSRXSZSDS_WIN +"\"元</div>";

	//选四任选四中四复式
	var XSHRXSZSFS_WIN = syxwData.getKindPlayLotteryWin("XSHRXSZSFS");
	syxwData.des.XSHRXSZSFS_DES = "<div class='title'>从01-11共11个号码中选择4个号码进行购买，只要当期的5个开奖号码中包含所选号码，即中奖，最高奖金\""+ XSHRXSZSFS_WIN +"\"元</div>";
	
	//选四任选四中四单式
	var XSHRXSZSDS_WIN = syxwData.getKindPlayLotteryWin("XSHRXSZSDS");
	syxwData.des.XSHRXSZSDS_DES = "<div class='title'>从01-11共11个号码中选择4个号码进行购买，只要当期的5个开奖号码中包含所选号码，即中奖，最高奖金\""+ XSHRXSZSDS_WIN +"\"元</div>";
	
	//选五任选五中五复式
	var XWRXWZWFS_WIN = syxwData.getKindPlayLotteryWin("XWRXWZWFS");
	syxwData.des.XWRXWZWFS_DES = "<div class='title'>从01-11共11个号码中选择5个号码进行购买，只要当期的5个开奖号码中包含所选号码，即中奖，最高奖金\""+ XWRXWZWFS_WIN +"\"元</div>";
	
	//选五任选五中五单式
	var XWRXWZWDS_WIN = syxwData.getKindPlayLotteryWin("XWRXWZWDS");
	syxwData.des.XWRXWZWDS_DES = "<div class='title'>从01-11共11个号码中选择5个号码进行购买，只要当期的5个开奖号码中包含所选号码，即中奖，最高奖金\""+ XWRXWZWDS_WIN +"\"元</div>";
	
	//选六任选六中五复式
	var XLRXLZWFS_WIN = syxwData.getKindPlayLotteryWin("XLRXLZWFS");
	syxwData.des.XLRXLZWFS_DES = "<div class='title'>每位至少选择一个号码，竞猜开奖号码的后四位，号码和位置都对应即中奖，最高奖金\""+ XLRXLZWFS_WIN +"\"元</div>";
	
	//选六任选六中五单式
	var XLRXLZWDS_WIN = syxwData.getKindPlayLotteryWin("XLRXLZWDS");
	syxwData.des.XLRXLZWDS_DES = "<div class='title'>从01-11共11个号码中选择6个号码进行购买，只要当期的5个开奖号码中包含所选号码，即中奖，最高奖金\""+ XLRXLZWDS_WIN +"\"元</div>";
	
	//选七任选七中五复式
	var XQRXQZWFS_WIN = syxwData.getKindPlayLotteryWin("XQRXQZWFS");
	syxwData.des.XQRXQZWFS_DES = "<div class='title'>从01-11共11个号码中选择6个号码进行购买，只要当期的5个开奖号码中包含所选号码，即中奖，最高奖金\""+ XQRXQZWFS_WIN +"\"元</div>";
	
	//选七任选七中五单式
	var XQRXQZWDS_WIN = syxwData.getKindPlayLotteryWin("XQRXQZWDS");
	syxwData.des.XQRXQZWDS_DES = "<div class='title'>从01-11共11个号码中选择7个号码进行购买，只要当期的5个开奖号码中包含所选号码，即中奖，最高奖金\""+ XQRXQZWDS_WIN +"\"元</div>";
	
	//选八任选八中五复式
	var XBRXBZWFS_WIN = syxwData.getKindPlayLotteryWin("XBRXBZWFS");
	syxwData.des.XBRXBZWFS_DES = "<div class='title'>从01-11共11个号码中选择8个号码进行购买，只要当期的5个开奖号码中包含所选号码，即中奖，最高奖金\""+ XBRXBZWFS_WIN +"\"元</div>";
	
	//选八任选八中五单式
	var XBRXBZWDS_WIN = syxwData.getKindPlayLotteryWin("XBRXBZWDS");
	syxwData.des.XBRXBZWDS_DES = "<div class='title'>从01-11共11个号码中选择8个号码进行购买，只要当期的5个开奖号码中包含所选号码，即中奖，最高奖金\""+ XBRXBZWDS_WIN +"\"元</div>";

	//趣味定单双
	var QWDDS_WIN = syxwData.getKindPlayLotteryWin("QWDDS");
	syxwData.des.QWDDS_DES = "<div class='title'>从6种单双个数组合中选择1种组合，当开奖号码的单双个数与所选单双组合一致，即中奖，最高奖金\""+ QWDDS_WIN +"\"元</div>";

	//趣味猜中位
	var QWCZW_WIN = syxwData.getKindPlayLotteryWin("QWCZW");
	syxwData.des.QWCZW_DES = "<div class='title'>从3-9中选择1个号码进行购买，所选号码与5个开奖号码按照大小顺序排列后的第3个号码相同，即为中奖，最高奖金\""+ QWCZW_WIN +"\"元</div>";
	
};

/**
 * 任选位置复选框点击事件
 */
SyxwData.prototype.dsPositionCheckFunc = function(obj){
	var positionCount = 0;
	$("input[name='ds_position']").each(function(){
		if(this.checked){
			positionCount++;
		}
	});
	
    if(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey == 'RXSIZXDS'){
		//判断当前是否符合该单式的处理方式
		if(positionCount == 4){  //选择4位的时候,有一个方案
			syxwData.rxParam.isRxDsAllow = true;
			syxwData.rxParam.dsAllowCount = 1;
			$("#pnum").text(positionCount);
			$("#fnum").text(1);  
		}else if(positionCount == 5){
			syxwData.rxParam.isRxDsAllow = true;
			syxwData.rxParam.dsAllowCount = 5;
			$("#pnum").text(positionCount);
			$("#fnum").text(5);  
		}else{
			syxwData.rxParam.isRxDsAllow = false;
			syxwData.rxParam.dsAllowCount = 0;
			$("#pnum").text(positionCount);
			$("#fnum").text(0);  
		}
    }else if(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey == 'RXEZXDS'){
		//判断当前是否符合该单式的处理方式
		if(positionCount == 2){  //选择2位的时候,有一个方案
			syxwData.rxParam.isRxDsAllow = true;
			syxwData.rxParam.dsAllowCount = 1;
			$("#pnum").text(positionCount);
			$("#fnum").text(1);  
		}else if(positionCount == 3){
			syxwData.rxParam.isRxDsAllow = true;
			syxwData.rxParam.dsAllowCount = 3;
			$("#pnum").text(positionCount);
			$("#fnum").text(3);  
		}else if(positionCount == 4){
			syxwData.rxParam.isRxDsAllow = true;
			syxwData.rxParam.dsAllowCount = 6;
			$("#pnum").text(positionCount);
			$("#fnum").text(6);  
		}else if(positionCount == 5){
			syxwData.rxParam.isRxDsAllow = true;
			syxwData.rxParam.dsAllowCount = 10;
			$("#pnum").text(positionCount);
			$("#fnum").text(10);  
		}else{
			syxwData.rxParam.isRxDsAllow = false;
			syxwData.rxParam.dsAllowCount = 0;
			$("#pnum").text(positionCount);
			$("#fnum").text(0);  
		}
    }else if(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey == 'RXSZXDS'){
		//判断当前是否符合该单式的处理方式
		if(positionCount == 3){  //选择2位的时候,有一个方案
			syxwData.rxParam.isRxDsAllow = true;
			syxwData.rxParam.dsAllowCount = 1;
			$("#pnum").text(positionCount);
			$("#fnum").text(1);  
		}else if(positionCount == 4){
			syxwData.rxParam.isRxDsAllow = true;
			syxwData.rxParam.dsAllowCount = 4;
			$("#pnum").text(positionCount);
			$("#fnum").text(4);  
		}else if(positionCount == 5){
			syxwData.rxParam.isRxDsAllow = true;
			syxwData.rxParam.dsAllowCount = 10;
			$("#pnum").text(positionCount);
			$("#fnum").text(10);  
		}else{
			syxwData.rxParam.isRxDsAllow = false;
			syxwData.rxParam.dsAllowCount = 0;
			$("#pnum").text(positionCount);
			$("#fnum").text(0);  
		}
    }else{
    	showTip("该直选单式未配置");
    }
    
    //格式化,统计该单式数目
    lotteryCommonPage.dealForDsFormat(false);
};


/**
 * 前三或者后三直选和值,注数
 */
SyxwData.prototype.getLotteryCountByShanZhiHeZhi = function(everyPositionCodesInt){
	var cathecticCount = 0;
	if(everyPositionCodesInt == 0){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 1){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 2){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 3){
		cathecticCount += 10;
	}else if(everyPositionCodesInt == 4){
		cathecticCount += 15;
	}else if(everyPositionCodesInt == 5){
		cathecticCount += 21;
	}else if(everyPositionCodesInt == 6){
		cathecticCount += 28;
	}else if(everyPositionCodesInt == 7){
		cathecticCount += 36;
	}else if(everyPositionCodesInt == 8){
		cathecticCount += 45;
	}else if(everyPositionCodesInt == 9){
		cathecticCount += 55;
	}else if(everyPositionCodesInt == 10){
		cathecticCount += 63;
	}else if(everyPositionCodesInt == 11){
		cathecticCount += 69;
	}else if(everyPositionCodesInt == 12){
		cathecticCount += 73;
	}else if(everyPositionCodesInt == 13){
		cathecticCount += 75;
	}else if(everyPositionCodesInt == 14){
		cathecticCount += 75;
	}else if(everyPositionCodesInt == 15){
		cathecticCount += 73;
	}else if(everyPositionCodesInt == 16){
		cathecticCount += 69;
	}else if(everyPositionCodesInt == 17){
		cathecticCount += 63;
	}else if(everyPositionCodesInt == 18){
		cathecticCount += 55;
	}else if(everyPositionCodesInt == 19){
		cathecticCount += 45;
	}else if(everyPositionCodesInt == 20){
		cathecticCount += 36;
	}else if(everyPositionCodesInt == 21){
		cathecticCount += 28;
	}else if(everyPositionCodesInt == 22){
		cathecticCount += 21;
	}else if(everyPositionCodesInt == 23){
		cathecticCount += 15;
	}else if(everyPositionCodesInt == 24){
		cathecticCount += 10;
	}else if(everyPositionCodesInt == 25){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 26){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 27){
		cathecticCount += 1;
	}else{
		showTip("无该和值的号码");
	}
	return cathecticCount;	
};

/**
 * 前三或者后三直选和值,注数
 */
SyxwData.prototype.getLotteryCountByShanZuHeZhi = function(everyPositionCodesInt){
	var cathecticCount = 0;
    if(everyPositionCodesInt == 1){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 2){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 3){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 4){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 5){
		cathecticCount += 5;
	}else if(everyPositionCodesInt == 6){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 7){
		cathecticCount += 8;
	}else if(everyPositionCodesInt == 8){
		cathecticCount += 10;
	}else if(everyPositionCodesInt == 9){
		cathecticCount += 11;
	}else if(everyPositionCodesInt == 10){
		cathecticCount += 13;
	}else if(everyPositionCodesInt == 11){
		cathecticCount += 14;
	}else if(everyPositionCodesInt == 12){
		cathecticCount += 14;
	}else if(everyPositionCodesInt == 13){
		cathecticCount += 15;
	}else if(everyPositionCodesInt == 14){
		cathecticCount += 15;
	}else if(everyPositionCodesInt == 15){
		cathecticCount += 14;
	}else if(everyPositionCodesInt == 16){
		cathecticCount += 14;
	}else if(everyPositionCodesInt == 17){
		cathecticCount += 13;
	}else if(everyPositionCodesInt == 18){
		cathecticCount += 11;
	}else if(everyPositionCodesInt == 19){
		cathecticCount += 10;
	}else if(everyPositionCodesInt == 20){
		cathecticCount += 8;
	}else if(everyPositionCodesInt == 21){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 22){
		cathecticCount += 5;
	}else if(everyPositionCodesInt == 23){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 24){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 25){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 26){
		cathecticCount += 1;
	}else{
		showTip("无该和值的号码");
	}
	return cathecticCount;	
};

/**
 * 前二或者后二直选和值,注数
 */
SyxwData.prototype.getLotteryCountByErZhiHeZhi = function(everyPositionCodesInt){
	var cathecticCount = 0;
	if(everyPositionCodesInt == 0){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 1){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 2){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 3){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 4){
		cathecticCount += 5;
	}else if(everyPositionCodesInt == 5){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 6){
		cathecticCount += 7;
	}else if(everyPositionCodesInt == 7){
		cathecticCount += 8;
	}else if(everyPositionCodesInt == 8){
		cathecticCount += 9;
	}else if(everyPositionCodesInt == 9){
		cathecticCount += 10;
	}else if(everyPositionCodesInt == 10){
		cathecticCount += 9;
	}else if(everyPositionCodesInt == 11){
		cathecticCount += 8;
	}else if(everyPositionCodesInt == 12){
		cathecticCount += 7;
	}else if(everyPositionCodesInt == 13){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 14){
		cathecticCount += 5;
	}else if(everyPositionCodesInt == 15){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 16){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 17){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 18){
		cathecticCount += 1;
	}else{
		showTip("无该和值的号码");
	}
	return cathecticCount;
};

/**
 * 前三或者后三直选和值,注数
 */
SyxwData.prototype.getLotteryCountByErZuHeZhi = function(everyPositionCodesInt){
	var cathecticCount = 0;
	if(everyPositionCodesInt == 1){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 2){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 3){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 4){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 5){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 6){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 7){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 8){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 9){
		cathecticCount += 5;
	}else if(everyPositionCodesInt == 10){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 11){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 12){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 13){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 14){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 15){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 16){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 17){
		cathecticCount += 1;
	}else{
		showTip("无该和值的号码");
	}
	return cathecticCount;
};
