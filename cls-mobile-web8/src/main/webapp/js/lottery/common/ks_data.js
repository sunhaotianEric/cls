function KsData(){}
var ksData = new KsData();

//任选参数
ksData.rxParam = {
	isRxDsAllow : true,  //是否允许任选单式,由位数来进行控制
    dsAllowCount : 0,
};


//通用的号码数组
ksData.common_code_array = new Array(1,2,3,4,5,6);
//和值
ksData.hz_code_arrays = new Array(3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18);
//三同号数组
ksData.sth_code_array = new Array('111','222','333','444','555','666');
//二同号数组
ksData.eth_code_array = new Array('11','22','33','44','55','66');
//三同号通选
ksData.sthtx_code = "111&222&333&444&555&666";
//三连号通选
ksData.slhtx_code  = "123&234&345&456";
//大小单双
ksData.dxds_code_array = new Array(1,2,3,4);

//////////////////玩法//////////////////// 
ksData.plays = {};

//三不同号玩法
ksData.plays.SBTH_PLAY = "";
ksData.plays.SBTH_PLAY += "<div class='container clist SBTH animated' >";
ksData.plays.SBTH_PLAY += "	<div class='list-line'>";
ksData.plays.SBTH_PLAY += "		<div class='line-title'><p></p></div>";
ksData.plays.SBTH_PLAY += "		<div class='line-content'>";
ksData.plays.SBTH_PLAY += "			<div class='child color-red no' data-role-id='SBTHBZ'><p>标准</p></div>";
ksData.plays.SBTH_PLAY += "			<div class='child color-red no' data-role-id='SBTHDT'><p>胆拖</p></div>";
ksData.plays.SBTH_PLAY += "		</div>";
ksData.plays.SBTH_PLAY += "	</div>";
ksData.plays.SBTH_PLAY += "</div>";

//二不同号玩法
ksData.plays.EBTH_PLAY = "";
ksData.plays.EBTH_PLAY += "<div class='container clist EBTH animated'>";
ksData.plays.EBTH_PLAY += "	<div class='list-line'>";
ksData.plays.EBTH_PLAY += "		<div class='line-title'><p></p></div>";
ksData.plays.EBTH_PLAY += "		<div class='line-content'>";
ksData.plays.EBTH_PLAY += "			<div class='child color-red no' data-role-id='EBTHBZ'><p>标准</p></div>";
ksData.plays.EBTH_PLAY += "			<div class='child color-red no' data-role-id='EBTHDT'><p>胆拖</p></div>";
ksData.plays.EBTH_PLAY += "		</div>";
ksData.plays.EBTH_PLAY += "	</div>";
ksData.plays.EBTH_PLAY += "</div>";


//////////////////玩法描述////////////////////
ksData.des = {};

ksData.des.HZ_DES = "玩法加载中，请稍后........"; 
ksData.des.HZDXDS_DES = "玩法加载中，请稍后........"; 
ksData.des.DXDS_DES = "玩法加载中，请稍后........"; 
ksData.des.STHTX_DES = "玩法加载中，请稍后........";
ksData.des.STHDX_DES = "玩法加载中，请稍后........";
ksData.des.SBTHBZ_DES = "玩法加载中，请稍后........";
ksData.des.SBTHDT_DES = "玩法加载中，请稍后........";
ksData.des.SLHTX_DES = "玩法加载中，请稍后........";
ksData.des.ETHFX_DES = "玩法加载中，请稍后........";
ksData.des.ETHDX_DES = "玩法加载中，请稍后........";
ksData.des.EBTHBZ_DES  = "玩法加载中，请稍后........";
ksData.des.EBTHDT_DES = "玩法加载中，请稍后........";
ksData.des.CYG_DES = "玩法加载中，请稍后........";


//玩法描述映射值
ksData.lotteryKindMap = new JS_OBJECT_MAP();

ksData.lotteryKindMap.put("HZ", "和值");
ksData.lotteryKindMap.put("HZDXDS", "和值大小单双");
ksData.lotteryKindMap.put("DXDS", "大小单双");
ksData.lotteryKindMap.put("STHTX", "三同号通选");
ksData.lotteryKindMap.put("STHDX", "三同号单选");
ksData.lotteryKindMap.put("SLHTX", "三连号通选");
ksData.lotteryKindMap.put("ETHFX", "二同号复选");
ksData.lotteryKindMap.put("ETHDX", "二同号单选");

ksData.lotteryKindMap.put("EBTHBZ", "二不同号标准");
ksData.lotteryKindMap.put("EBTHDT", "二不同号胆拖");

ksData.lotteryKindMap.put("SBTHBZ", "三不同号标准");
ksData.lotteryKindMap.put("SBTHDT", "三不同号胆拖");

ksData.lotteryKindMap.put("CYG", "猜一个");


/**
 * 加载所有的玩法
 */
KsData.prototype.loadLotteryKindPlay = function() {
	$("#lotteryKindPlayList").append(ksData.plays.SBTH_PLAY);
	$("#lotteryKindPlayList").append(ksData.plays.EBTH_PLAY);
};


/**
 * 玩法选择显示描述
 * @param roleId
 */
KsData.prototype.showPlayDes = function(roleId) {
	
	var playDes = "";
	
	if (roleId == 'HZ') {
		playDes = ksData.des.HZ_DES;
	}else if(roleId == "HZDXDS"){
		playDes = ksData.des.HZDXDS_DES;
	} else if (roleId == 'DXDS') {
		playDes = ksData.des.DXDS_DES;
	} else if (roleId == 'STHTX') {
		playDes = ksData.des.STHTX_DES;
	} else if (roleId == 'STHDX') {
		playDes = ksData.des.STHDX_DES;
	} else if (roleId == 'SBTHBZ') {
		playDes = ksData.des.SBTHBZ_DES;
	} else if (roleId == 'SBTHDT') {
		playDes = ksData.des.SBTHDT_DES;
	} else if (roleId == 'SLHTX') {
		playDes = ksData.des.SLHTX_DES;
	} else if (roleId == 'ETHFX') {
		playDes = ksData.des.ETHFX_DES;
	} else if (roleId == 'ETHDX') {
		playDes = ksData.des.ETHDX_DES;
	} else if (roleId == 'EBTHBZ') {
		playDes = ksData.des.EBTHBZ_DES;
	} else if (roleId == 'EBTHDT') {
		playDes = ksData.des.EBTHDT_DES;
	} else if (roleId == 'CYG') {
		playDes = ksData.des.CYG_DES;
	} else {
		showTip("不可知的彩种玩法描述.");
		return;
	}
	$("#playDes").html(playDes);
};

/**
 * 玩法选择
 * @param roleId
 */
KsData.prototype.lotteryKindPlayChoose = function(roleId) {

	var desArray = new Array("选号区");
	var emptyDesArray = new Array("");
	
	var emptyDesArray = new Array("");
	if (roleId == 'HZ') {
		ksData.ballSectionCommon(0, 0, desArray, roleId, false, true);
		//获取赔率
		ksData.updateLotteryWinBySpecialKind();
	}else if(roleId == 'HZDXDS'){
		ksData.ballSectionCommon(0, 0, desArray, roleId, false, true);
		//获取赔率
		ksData.updateLotteryWinBySpecialKind();
	}else if (roleId == 'DXDS') {
		ksData.ballSectionCommon(0, 1, desArray, roleId, false,true);
	} else if (roleId == 'STHTX') {
		ksData.ballSectionCommon(0, -1, desArray, roleId, false,true);
	} else if (roleId == 'STHDX') {
//		ksData.getOShowLotteryWinBySpecialKind(roleId);
		ksData.ballSectionCommon(0, -1, desArray, roleId, true);
	} else if (roleId == 'SBTHBZ') {
		ksData.ballSectionCommon(0, 0, desArray, null, true,roleId);
	} else if (roleId == 'SBTHDT') {
		var desArray = new Array("胆码", "拖码");
		ksData.ballSectionCommon(0, 1, desArray, roleId, true);
	} else if (roleId == 'SLHTX') {
		ksData.ballSectionCommon(0, -1, desArray, roleId, false,true);
	} else if (roleId == 'ETHFX') {
		ksData.ballSectionCommon(0, -1, desArray, roleId, true);
	} else if (roleId == 'ETHDX') {
		var desArray = new Array("同号", "不同号");
		ksData.ballSectionCommon(0, 1, desArray, roleId, true);
	} else if (roleId == 'EBTHBZ') {
		ksData.ballSectionCommon(0, 0, desArray, null, true,roleId);
	} else if (roleId == 'EBTHDT') {
		var desArray = new Array("胆码", "拖码");
		ksData.ballSectionCommon(0, 1, desArray, roleId, true);
	} else if (roleId == 'CYG') {
//		lotteryCommonPage.recoverOmmitHotColdShow(); // 遗漏的显示
		ksData.ballSectionCommon(0, 0, desArray, null, true);
	} else {
		showTip("不可知的彩种玩法.");
	}

	//存储当前切换的玩法模式
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey = roleId;
	
	//显示玩法名称
	var currentKindName = ksData.lotteryKindMap.get(roleId);
    $("#currentKindName").text(currentKindName);

	//显示玩法说明
	ksData.showPlayDes(roleId);
	
	//拼接完，进行事件的重新申明
	lotteryCommonPage.dealForEvent();
};





/**
 * 复式玩法的号码拼接
 * desArray 描述的数组
 * specialKind 特殊玩法
 * isOmitColdDataDeal 是否遗漏和冷热数据的处理
 */
KsData.prototype.ballSectionCommon = function(numCountStart,numCountEnd,desArray,specialKind,isOmitColdDataDeal,isShowLotteryWinORstandardPlay){
	var content = "";
	
	if(specialKind == null){
		// 二不同号标准和三不同号标准时赔率特殊处理即 isShowLotteryWinORstandardPlay为玩法
		var BTH_WIN = ksData.getKindPlayLotteryWin(isShowLotteryWinORstandardPlay);
		for(var i = numCountStart;i <= numCountEnd; i++){
			//0 - 6的号码
			for(var j = 0; j < ksData.common_code_array.length; j++){
				if(j%4 == 0){
					content += '<div class="lottery-content animated" style="animation-name: fadeInUp; animation-delay: 0s;"><div class="lottery-child no main"><div class="container">';
					var numDes = desArray[i];
					content += '<div class="child-content">';
					if(isNotEmpty(numDes) && j == 0) {
						content += '<div class="title"><p>' + numDes + '</p></div>';
						//遗漏冷热元素处理
						if(isOmitColdDataDeal) {
							content += "";
						}
					}
					content += '	<div class="content">';
				}
				content += '<div class="munbtn mun2" data-realvalue="'+ ksData.common_code_array[j] +'" name="ballNum_'+i+'_match"><h2 class="value">'+ ksData.common_code_array[j] +'</h2><p class="info">赔率：'+ BTH_WIN +'</p></div>';
				if(j%4 == 3){
					content += '	</div>';
					content += '</div>';
					content += '</div>';
					content += '</div>';
				}
			}
		}
	}else{
		
		var code_array = null;
		var dxds_code_array = null;
		var WIN = "";
		if(specialKind == "HZ"){
			//和值
			code_array = ksData.hz_code_arrays;
		}else if(specialKind == "HZDXDS"){
			//和值
			code_array = ksData.hz_code_arrays;
			//大小单双
			dxds_code_array = ksData.dxds_code_array;
			
		}else if(specialKind == "ETHFX"){
			//二同号复选
			code_array = ksData.eth_code_array;
			WIN = ksData.getKindPlayLotteryWin("ETHFX");
		}else if(specialKind == "STHDX"){
			//三同号单选
			WIN = ksData.getKindPlayLotteryWin("STHDX");
			code_array = ksData.sth_code_array;
		}
		
		if(code_array != null) {
			
			var numDes = desArray[i];
			//大小单双
			if(dxds_code_array != null){
				content += '<div class="lottery-child no main"><div class="container">';
				content += '<div class="child-content" style="text-align:center;">';
				
				for(var j = 0; j < dxds_code_array.length;j++){
					content += '<div class="munbtn mun2" data-realvalue="'+ dxds_code_array[j] +'" name="ballNum_0_match"><h2 class="value font-blue">';
					if(ksData.dxds_code_array[j] == 1){
						content += "大";
					}else if(ksData.dxds_code_array[j] == 2){
						content += "小";
					}else if(ksData.dxds_code_array[j] == 3){
						content += "单";
					}else if(ksData.dxds_code_array[j] == 4){
						content += "双";
					}
					 content += '</h2><p class="info"></p></div>';
				}
				content += '	</div>';
				content += '</div>';
				content += '</div>';
				content += '</div>';
			}
	
			for(var j = 0; j < code_array.length;j++){
				//和值
				if(specialKind == "HZ" || specialKind == "HZDXDS"){
					if(j%4 == 0){
						content += '<div class="lottery-child no main"><div class="container">';
						content += '<div class="child-content" style="text-align:center;">';
					}
					content += '<div class="munbtn mun2" data-realvalue="'+ code_array[j] +'" name="ballNum_1_match"><h2 class="value">'+ code_array[j] +'</h2><p class="info"></p></div>';
					if(j%4 == 3){
						content += '	</div>';
						content += '</div>';
						content += '</div>';
					};
				}else{
					//二同号复选,三同号单选
					if(j%3 == 0){
						content += '<div class="lottery-child no main"><div class="container">';
						content += '<div class="child-content" style="text-align:center;">';
					}
					content += '<div class="munbtn mun2" data-realvalue="'+ code_array[j] +'" name="ballNum_'+j+'_match"><h2 class="value">'+ code_array[j] +'</h2><p class="info">赔率：'+ WIN +'</p></div>';
					if(j%3 == 2){
						content += '	</div>';
						content += '</div>';
						content += '</div>';
						content += '</div>';
					}
				}
			}
			
		} else if(specialKind == "EBTHDT" || specialKind == "SBTHDT"  || specialKind == "ETHDX"){
			//胆拖 ，二同号单选
			for(var i = numCountStart;i <= numCountEnd; i++){
				var ETHDXTH_WIN = ksData.getKindPlayLotteryWin(specialKind);
				if(specialKind == "ETHDX" && i !=1){
					
					for(var j = 0; j < ksData.eth_code_array.length; j++){
						if(j%4 == 0){
							content += '<div class="lottery-content animated" style="animation-name: fadeInUp; animation-delay: 0s;">';
							content += '<div class="lottery-child no main"><div class="container">';
							var numDes = desArray[i];
							content += '<div class="child-content">';
							if(isNotEmpty(numDes) && j == 0 ) {
								content += '<div class="title"><p>' + numDes + '</p></div>';
								//遗漏冷热元素处理
								if(isOmitColdDataDeal) {
									content += "";
								}
							}
							content += '	<div class="content">';
						}
							//二同号单选--同号
							content += '<div class="munbtn mun2" data-realvalue="'+ ksData.eth_code_array[j] +'" name="ballNum_0_match"><h2 class="value">'+ ksData.eth_code_array[j] +'</h2><p class="info">赔率：'+ ETHDXTH_WIN +'</p></div>';
				
						if(j%4 == 3){
							content += '		</div>';
							content += '		</div>';
							content += '	</div>';
							content += '</div>';
						}
					};
				}
				if(specialKind == "ETHDX" && i !=0 || specialKind != "ETHDX"){
					if(i == 0 && ksData.specialKind != "ETHDX"){
						var DT_WIN = ksData.getKindPlayLotteryWin(specialKind);
						for(var j = 0; j < ksData.common_code_array.length; j++){
							if(j%4 == 0){
//								content += '<div class="lottery-content animated" style="animation-name: fadeInUp; animation-delay: 0s;"><div class="lottery-child no main"><div class="container">';
								content += '<div class="lottery-child no main"><div class="container">';
								var numDes = desArray[i];
								content += '<div class="child-content">';
								if(isNotEmpty(numDes) && j == 0 ) {
									content += '<div class="title"><p>' + numDes + '</p></div>';
									//遗漏冷热元素处理
									if(isOmitColdDataDeal) {
										content += "";
									}
								}
								content += '	<div class="content">';
							}
							
							content += '<div class="munbtn mun2" data-realvalue="'+ ksData.common_code_array[j] +'" name="ballNum_0_match"><h2 class="value">'+ ksData.common_code_array[j] +'</h2><p class="info">赔率：'+ DT_WIN +'</p></div>';
							
							if(j%4 == 3){
								content += '		</div>';
								content += '		</div>';
								content += '	</div>';
								content += '</div>';
							}
						}
						
					}
					if(i==1 && specialKind != "ETHDX" || specialKind == "ETHDX"){
						for(var j = 0; j < ksData.common_code_array.length; j++){
							var DX_WIN = ksData.getKindPlayLotteryWin(specialKind);
							if(j%4 == 0){
//								content += '<div class="lottery-content animated" style="animation-name: fadeInUp; animation-delay: 0s;">';
								content += '<div class="lottery-child no main"><div class="container">';
								var numDes = desArray[i];
								content += '<div class="child-content">';
								if(isNotEmpty(numDes) && j == 0 ) {
									content += '<div class="title"><p>' + numDes + '</p></div>';
									//遗漏冷热元素处理
									if(isOmitColdDataDeal) {
										content += "";
									}
								}
								content += '	<div class="content">';
							}
							content += '<div class="munbtn mun2" data-realvalue="'+ ksData.common_code_array[j] +'" name="ballNum_1_match"><h2 class="value">'+ ksData.common_code_array[j] +'</h2><p class="info"> 赔率：'+ DX_WIN +'</p></div>';
							if(j%4 == 3){
								content += '		</div>';
								content += '		</div>';
								content += '	</div>';
							}
						}
					}
					
				}
				content += '		</div>';
				content += '		</div>';
				content += '	</div>';
				content += '</div>';
			}
			
		} else if(specialKind == "STHTX" || specialKind == "SLHTX"){
			//通选
		
			for(var i = 0; i < desArray.length; i++){
				content += '<div class="lottery-child no main"><div class="container">';
				var numDes = desArray[i];
				content += '<div class="child-content">';
				if(isNotEmpty(numDes)) {
					content += '<div class="title"><p>' + numDes + '</p></div>';
					//遗漏冷热元素处理
					if(isOmitColdDataDeal) {
						content += "";
					}
				}
				content += '	<div class="content">';
				
				if(specialKind == "STHTX"){
					if(isShowLotteryWinORstandardPlay){
						var STHTX_WIN = ksData.getKindPlayLotteryWin("STHTX");
					}else{
						STHTX_WIN = 0.00;
					}
					content += '<div class="munbtn mun2 long" data-realvalue="1" name="ballNum_0_match"><h2 class="value">三同号通选</h2><p class="info"><p class="info">赔率：'+ STHTX_WIN +'</p></div>';
					
				}else if( specialKind == "SLHTX"){
					if(isShowLotteryWinORstandardPlay){
						var SLHTX_WIN = ksData.getKindPlayLotteryWin("SLHTX");
					}else{
						STHTX_WIN = 0.00;
					}
					content += '<div class="munbtn mun2 long" data-realvalue="1" name="ballNum_0_match"><h2 class="value">三连号通选</h2><p class="info"><p class="info">赔率：'+ SLHTX_WIN +'</p></div>';
				}
				content += '	</div>';
				content += "</div>";
				content += "</div>";
				content += '</div>';
			}
		} else if(specialKind == "DXDS"){
			for(var i = 0; i < desArray.length; i++){
				content += '<div class="lottery-child no main"><div class="container">';
				var numDes = desArray[i];
				content += '<div class="child-content">';
				if(isNotEmpty(numDes)) {
					content += '<div class="title"><p>' + numDes + '</p></div>';
					//遗漏冷热元素处理
					if(isOmitColdDataDeal) {
						content += "";
					}
				}
				content += '	<div class="content">';
				//1 - 4的号码
				for(var j = 0; j < ksData.dxds_code_array.length; j++){
					content += '<div class="munbtn mun2" data-realvalue="'+ ksData.dxds_code_array[j] +'" name="ballNum_0_match">';
					content += '<h2 class="value font-blue">';
					if(ksData.dxds_code_array[j] == 1){
						content += "大";
					}else if(ksData.dxds_code_array[j] == 2){
						content += "小";
					}else if(ksData.dxds_code_array[j] == 3){
						content += "单";
					}else if(ksData.dxds_code_array[j] == 4){
						content += "双";
					}
					content += '</h2>';
					var DXDS_WIN = ksData.getKindPlayLotteryWin("DXDS");
					content += '<p class="info">赔率：'+ DXDS_WIN +'</p>';
					//遗漏冷热元素处理
					if(isOmitColdDataDeal) {
						content += "";
					}
					content += '</div>';
				}
				content += '	</div>';
				content += '</div>';
				content += '</div>';
			}
		}else{
			showTip("未找到该玩法的号码.");
		}
	}
    $("#ballSection").html(content);
    //任选框隐藏
    $("#rxWeiCheck").hide();
    //隐藏单式的导入框
    $("#importDsBtn").hide();
    
    //进行遗漏冷热的数值处理
    if(isOmitColdDataDeal){
    	$("#ommitHotColdBtn").show();
    } else {
    	$("#ommitHotColdBtn").hide();
    }
};

/**
 * 获得赔率
 */
KsData.prototype.updateLotteryWinBySpecialKind = function(){
	 $("#ballSection .munbtn").each(function(){
		  code =$(this).attr("data-realvalue");
		  ksData.getShowLotteryWin(code,$(this));
	 });

};

/**
 * 显示赔率
 */
KsData.prototype.getShowLotteryWin = function(code,t){
	var ballNumName = t.attr("name");
	if(ballNumName == "ballNum_0_match"){
		var DXDS_WIN = ksData.getKindPlayLotteryWin("DXDS");
		t.find("p").text("赔率："+DXDS_WIN);
	}
	//和值
	if(code == 3 && ballNumName == "ballNum_1_match" || code == 18){
		var HZ_WIN = ksData.getKindPlayLotteryWin("HZ");
		t.find("p").text("赔率："+HZ_WIN);
	}else if(code == 4 && ballNumName == "ballNum_1_match" || code == 17){
		var HZ_WIN2 = ksData.getKindPlayLotteryWin("HZ_2");
		t.find("p").text("赔率："+HZ_WIN2);
	}else if(code == 5 || code == 16){
		var HZ_WIN3 = ksData.getKindPlayLotteryWin("HZ_3");
		t.find("p").text("赔率："+HZ_WIN3);
	}else if(code == 6 || code == 15){
		var HZ_WIN4 = ksData.getKindPlayLotteryWin("HZ_4");
		t.find("p").text("赔率："+HZ_WIN4);
	}else if(code == 7 || code == 14){
		var HZ_WIN5 = ksData.getKindPlayLotteryWin("HZ_5");
		t.find("p").text("赔率："+HZ_WIN5);
	}else if(code == 8 || code == 13){
		var HZ_WIN6 = ksData.getKindPlayLotteryWin("HZ_6");
		t.find("p").text("赔率："+HZ_WIN6);
	}else if(code == 9 || code == 12){
		var HZ_WIN7 = ksData.getKindPlayLotteryWin("HZ_7");
		t.find("p").text("赔率："+HZ_WIN7);
	}else if(code == 10 || code == 11){
		var HZ_WIN8 = ksData.getKindPlayLotteryWin("HZ_8");
		t.find("p").text("赔率："+HZ_WIN8);
	}
};

/*KsData.prototype.getOShowLotteryWinBySpecialKind = function(kind){
	 $("#ballSection .munbtn").each(function(){
		  var WIN = ksData.getKindPlayLotteryWin(kind);
		  $(this).find("p").text("赔率："+WIN);
	 });
}*/
/**
 * 单式玩法的号码拼接
 * specialKind 特殊玩法
 */
KsData.prototype.dsBallSectionCommon = function(specialKind){
	
	var editorStr = "说明：\n";
	editorStr += "1、请参照标准格式样本格式录入或上传方案。\n";
	editorStr += "2、每一注号码之间的间隔符支持 回车 空格[ ] 逗号[,] 分号[;] 冒号[:] 竖线 [|]。\n";
	editorStr += "3、文件格式必须是.txt格式。\n";
	editorStr += "4、文件较大时会导致上传时间较长，请耐心等待！\n";
	editorStr += "5、将文件拖入文本框即可快速实现文件上传功能。\n";
	editorStr += "6、导入文本内容后将覆盖文本框中现有的内容。";

	var rxContent = '<div class="lottery-select">'+
	'<div class="container lottery-select-list">'+
	'	<label for="">'+
	'		<input type="checkbox" name="ds_position" onclick="lotteryCommonPage.ksData.dsPositionCheckFunc(this);" data-position="1" checked="checked">'+
	'		<span>万位</span>'+
	'	</label>'+
	'	<label for="">'+
	'		<span>千位</span>'+
	'	</label>'+
	'	<label for="">'+
	'		<input type="checkbox" name="ds_position" onclick="lotteryCommonPage.ksData.dsPositionCheckFunc(this);" data-position="3" checked="checked">'+
	'		<span>百位</span>'+
	'	</label>'+
	'	<label for="">'+
	'		<input type="checkbox" name="ds_position" onclick="lotteryCommonPage.ksData.dsPositionCheckFunc(this);" data-position="4" checked="checked">'+
	'		<span>十位</span>'+
	'	</label>'+
	'	<label for="">'+
	'		<input type="checkbox" name="ds_position" onclick="lotteryCommonPage.ksData.dsPositionCheckFunc(this);" data-position="5" checked="checked">'+
	'		<span>个位</span>'+
	'	</label>'+
	'</div>'+
	'<div class="container lottery-select-msg" >'+
	'	<p>温馨提示：您选择了<span class="font-yellow" id="pnum">5</span>个位置，系统自动根据位置合成<span class="font-yellow" id="fnum">10</span>个方案</p>'+
	'</div>'+
	'</div>';
	
	var content = '	<div class="lottery-child main"><div class="container">'+
	'		<div class="lottery-textarea">'+
	'			<textarea id="lr_editor" placeholder="'+ editorStr +'"></textarea>'+
	'		</div>'+
	'	</div></div>';
	
    //任选玩法的话
    if(specialKind == "RXSIZXDS" || specialKind == "RXEZXDS" || specialKind == "RXSZXDS" ) {
    	//拼接加上任选的内容
    	content =  rxContent + content;
    	
    	ksData.rxParam.isRxDsAllow = true;
    	//全选
    	$("input[name='ds_position']").each(function(){
    		this.checked = true;
    	});
    	if(specialKind == "RXSIZXDS"){
    		ksData.rxParam.dsAllowCount = 5;
    		$("#fnum").text(5);
    	}else if(specialKind == "RXEZXDS" || specialKind == "RXSZXDS"){
    		ksData.rxParam.dsAllowCount = 10;
    		$("#fnum").text(10); 
    	}
    }
    $("#ballSection").html(content);
    
    //添加单式的事件处理
    lotteryCommonPage.addDsEvent();
}

/**
 * 计算当前用户最高模式下的奖金
 */
KsData.prototype.calucateCurrentUserHighestWin = function(kindKey){
	if(lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.size() == 0){
		return "奖金未完全获取,请再次操作或者刷新页面";
	}
	var currentLotteryWin;
	currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get(kindKey);
	if(currentLotteryWin == null){
		return "未配置该玩法的奖金.";
	}
	var result = (currentLotteryWin.winMoney + (currentUserKsModel - ksLowestAwardModel) * currentLotteryWin.specCode) * 1;	
    return result;
};

/**
 * 获取当前用户最高模式下的奖金
 */
KsData.prototype.getKindPlayLotteryWin = function(kindKey){
	if(lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.size() == 0){
		return "奖金未完全获取,请稍作等待或者再次操作.";
	}
	var currentLotteryWin;
	currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get(lotteryCommonPage.currentLotteryKindInstance.param.kindNameType+ "_" +kindKey);
	if(currentLotteryWin == null){
		return "未配置该玩法的奖金.";
	}
	var result = (currentLotteryWin.winMoney + (currentUserKsModel - ksLowestAwardModel) * currentLotteryWin.specCode) * 1;	
    return result.toFixed(lotteryCommonPage.param.toFixedValue);
};

/**
 * 设置当前用户最高模式下的奖金和玩法描述
 */
KsData.prototype.setKindPlayDes = function(){
	//获取赔率
	ksData.updateLotteryWinBySpecialKind();
	//和值
	var HZ_WIN = ksData.getKindPlayLotteryWin("HZ");
	var HZ_WIN2 = ksData.getKindPlayLotteryWin("HZ_2");
	var HZ_WIN3 = ksData.getKindPlayLotteryWin("HZ_3");
	var HZ_WIN4 = ksData.getKindPlayLotteryWin("HZ_4");
	var HZ_WIN5 = ksData.getKindPlayLotteryWin("HZ_5");
	var HZ_WIN6 = ksData.getKindPlayLotteryWin("HZ_6");
	var HZ_WIN7 = ksData.getKindPlayLotteryWin("HZ_7");
	var HZ_WIN8 = ksData.getKindPlayLotteryWin("HZ_8");
	ksData.des.HZ_DES = "<div class='title'>至少选择1个和值（3个号码之和）进行投注，所选和值与开奖的3个号码的和值相同即中奖，最高可中\""+HZ_WIN+"\"元</div>";
	ksData.des.HZDXDS_DES = "<div class='title'>猜3个开奖号相加的和，3-10为小，11-18为大。</div>";
	/*HZ_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	HZ_DES += "<div class='selct-demo-center' style='width: 190px';><p>投注：3</p><p>开奖：111(和值为5)</p><p>奖金：\""+HZ_WIN+"\"元</p>" +
			"奖金对照表：<p>(1)和值：3,18  &nbsp;&nbsp;中奖：\""+HZ_WIN+"\"元<br>"+
			            "<p>(2)和值：4,17  &nbsp;&nbsp;中奖：\""+HZ_WIN2+"\"元</p>"+
			            "<p>(3)和值：5,16  &nbsp;&nbsp;中奖：\""+HZ_WIN3+"\"元</p>"+
			            "<p>(4)和值：6,15  &nbsp;&nbsp;中奖：\""+HZ_WIN4+"\"元</p>"+
			            "<p>(5)和值：7,14  &nbsp;&nbsp;中奖：\""+HZ_WIN5+"\"元</p>"+
			            "<p>(6)和值：8,13  &nbsp;&nbsp;中奖：\""+HZ_WIN6+"\"元</p>"+
			            "<p>(7)和值：9,12  &nbsp;&nbsp;中奖：\""+HZ_WIN7+"\"元</p>"+
			            "<p>(8)和值：10,11 &nbsp;&nbsp;中奖：\""+HZ_WIN8+"\"元</p>"+
			            "</div></div>";*/
	
	
	//大小单双
	var DXDS_WIN = ksData.getKindPlayLotteryWin("DXDS");
	ksData.des.DXDS_DES = "<div class='title'>对开奖出来三位号码的和值,3-10为小,11-18为大,奇数为单,偶数为双，任意号码开出即中奖，单注奖金\""+DXDS_WIN+"\"元</div>";

	//三同号通选
	var STHTX_WIN = ksData.getKindPlayLotteryWin("STHTX");
	ksData.des.STHTX_DES = "<div class='title'>对所有相同的三个号码（111、222、333、444、555、666）进行投注，任意号码开出即中奖，单注奖金\""+STHTX_WIN+"\"元</div>";
	
	//三同号单选
	var STHDX_WIN = ksData.getKindPlayLotteryWin("STHDX");
	ksData.des.STHDX_DES = "<div class='title'>对相同的三个号码（111、222、333、444、555、666）中的任意一个进行投注，所选号码开出即中奖，单注奖金\""+ STHDX_WIN +"\"元</div>";
	
	//三不同号标准
	var SBTHBZ_WIN = ksData.getKindPlayLotteryWin("SBTHBZ");
	ksData.des.SBTHBZ_DES = "<div class='title'>从1～6中任选3个或多个号码，所选号码与开奖号码的3个号码相同即中奖，单注奖金\""+ SBTHBZ_WIN +"\"元<div>";
	
	//三不同号胆拖
	var SBTHDT_WIN = ksData.getKindPlayLotteryWin("SBTHDT");
	ksData.des.SBTHDT_DES	= "<div class='title'> 选1～2个胆码，选1～5个拖码，胆码加拖码不少于3个；选号与奖号相同即中奖，单注奖金\""+ SBTHDT_WIN +"\"元</div>";
	
	//三连号通选
	var SLHTX_WIN = ksData.getKindPlayLotteryWin("SLHTX");
	ksData.des.SLHTX_DES = "<div class='title'>对所有3个相连的号码（123、234、345、456）进行投注，任意号码开出即中奖，单注奖金\""+ SLHTX_WIN +"\"元</div>";
	
	//二同号复选
	var ETHFX_WIN = ksData.getKindPlayLotteryWin("ETHFX");
	ksData.des.ETHFX_DES = "<div class='title'>从11～66中任选1个或多个号码，选号与奖号（包含11～66，不限顺序）相同，即中奖\""+ ETHFX_WIN +"\"元</div>";
	
	//二同号单选
	var ETHDX_WIN = ksData.getKindPlayLotteryWin("ETHDX");
	ksData.des.ETHDX_DES = "<div class='title'>选择1对相同号码（11,22,33,44,55,66）和1个不同号码(1,2,3,4,5,6)投注，选号与奖号相同（顺序不限），即中奖\""+ ETHDX_WIN +"\"元</div>";
	
	//二不同号标准
	var EBTHBZ_WIN = ksData.getKindPlayLotteryWin("EBTHBZ");
	ksData.des.EBTHBZ_DES = "<div class='title'>从1～6中任选2个或多个号码，所选号码与开奖号码任意2个号码相同，即中奖\""+ EBTHBZ_WIN +"\"元</div>";
	
	//二不同号胆拖
	var EBTHDT_WIN = ksData.getKindPlayLotteryWin("EBTHDT");
	ksData.des.EBTHDT_DES = "<div class='title'>选1个胆码，选1～5个拖码，胆码加拖码不少于2个；选号与奖号任意2号相同即中奖，单注奖金\""+ EBTHDT_WIN +"\"元</div>";
	
	//猜一个就中奖
	var CYG_WIN = ksData.getKindPlayLotteryWin("CYG");
	ksData.des.CYG_DES = "<div class='title'>选择1个您认为一定会开出的号码，所选号码在开奖号码中出现，即中奖金\""+ CYG_WIN +"\"元</div>";
	
	
	//五星复式
	var WXFS_WIN = ksData.getKindPlayLotteryWin("WXFS");
	ksData.des.WXFS_DES = "<div class='title'>每位至少选择一个号码，竞猜开奖号码的全部五位，号码和位置都对应即中奖，最高奖金\""+WXFS_WIN+"\"元</div>";

};

/**
 * 任选位置复选框点击事件
 */
KsData.prototype.dsPositionCheckFunc = function(obj){
	var positionCount = 0;
	$("input[name='ds_position']").each(function(){
		if(this.checked){
			positionCount++;
		}
	});
	
    if(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey == 'RXSIZXDS'){
		//判断当前是否符合该单式的处理方式
		if(positionCount == 4){  //选择4位的时候,有一个方案
			ksData.rxParam.isRxDsAllow = true;
			ksData.rxParam.dsAllowCount = 1;
			$("#pnum").text(positionCount);
			$("#fnum").text(1);  
		}else if(positionCount == 5){
			ksData.rxParam.isRxDsAllow = true;
			ksData.rxParam.dsAllowCount = 5;
			$("#pnum").text(positionCount);
			$("#fnum").text(5);  
		}else{
			ksData.rxParam.isRxDsAllow = false;
			ksData.rxParam.dsAllowCount = 0;
			$("#pnum").text(positionCount);
			$("#fnum").text(0);  
		}
    }else if(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey == 'RXEZXDS'){
		//判断当前是否符合该单式的处理方式
		if(positionCount == 2){  //选择2位的时候,有一个方案
			ksData.rxParam.isRxDsAllow = true;
			ksData.rxParam.dsAllowCount = 1;
			$("#pnum").text(positionCount);
			$("#fnum").text(1);  
		}else if(positionCount == 3){
			ksData.rxParam.isRxDsAllow = true;
			ksData.rxParam.dsAllowCount = 3;
			$("#pnum").text(positionCount);
			$("#fnum").text(3);  
		}else if(positionCount == 4){
			ksData.rxParam.isRxDsAllow = true;
			ksData.rxParam.dsAllowCount = 6;
			$("#pnum").text(positionCount);
			$("#fnum").text(6);  
		}else if(positionCount == 5){
			ksData.rxParam.isRxDsAllow = true;
			ksData.rxParam.dsAllowCount = 10;
			$("#pnum").text(positionCount);
			$("#fnum").text(10);  
		}else{
			ksData.rxParam.isRxDsAllow = false;
			ksData.rxParam.dsAllowCount = 0;
			$("#pnum").text(positionCount);
			$("#fnum").text(0);  
		}
    }else if(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey == 'RXSZXDS'){
		//判断当前是否符合该单式的处理方式
		if(positionCount == 3){  //选择2位的时候,有一个方案
			ksData.rxParam.isRxDsAllow = true;
			ksData.rxParam.dsAllowCount = 1;
			$("#pnum").text(positionCount);
			$("#fnum").text(1);  
		}else if(positionCount == 4){
			ksData.rxParam.isRxDsAllow = true;
			ksData.rxParam.dsAllowCount = 4;
			$("#pnum").text(positionCount);
			$("#fnum").text(4);  
		}else if(positionCount == 5){
			ksData.rxParam.isRxDsAllow = true;
			ksData.rxParam.dsAllowCount = 10;
			$("#pnum").text(positionCount);
			$("#fnum").text(10);  
		}else{
			ksData.rxParam.isRxDsAllow = false;
			ksData.rxParam.dsAllowCount = 0;
			$("#pnum").text(positionCount);
			$("#fnum").text(0);  
		}
    }else{
    	showTip("该直选单式未配置");
    }
    
    //格式化,统计该单式数目
    lotteryCommonPage.dealForDsFormat(false);
};


/**
 * 前三或者后三直选和值,注数
 */
KsData.prototype.getLotteryCountByShanZhiHeZhi = function(everyPositionCodesInt){
	var cathecticCount = 0;
	if(everyPositionCodesInt == 0){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 1){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 2){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 3){
		cathecticCount += 10;
	}else if(everyPositionCodesInt == 4){
		cathecticCount += 15;
	}else if(everyPositionCodesInt == 5){
		cathecticCount += 21;
	}else if(everyPositionCodesInt == 6){
		cathecticCount += 28;
	}else if(everyPositionCodesInt == 7){
		cathecticCount += 36;
	}else if(everyPositionCodesInt == 8){
		cathecticCount += 45;
	}else if(everyPositionCodesInt == 9){
		cathecticCount += 55;
	}else if(everyPositionCodesInt == 10){
		cathecticCount += 63;
	}else if(everyPositionCodesInt == 11){
		cathecticCount += 69;
	}else if(everyPositionCodesInt == 12){
		cathecticCount += 73;
	}else if(everyPositionCodesInt == 13){
		cathecticCount += 75;
	}else if(everyPositionCodesInt == 14){
		cathecticCount += 75;
	}else if(everyPositionCodesInt == 15){
		cathecticCount += 73;
	}else if(everyPositionCodesInt == 16){
		cathecticCount += 69;
	}else if(everyPositionCodesInt == 17){
		cathecticCount += 63;
	}else if(everyPositionCodesInt == 18){
		cathecticCount += 55;
	}else if(everyPositionCodesInt == 19){
		cathecticCount += 45;
	}else if(everyPositionCodesInt == 20){
		cathecticCount += 36;
	}else if(everyPositionCodesInt == 21){
		cathecticCount += 28;
	}else if(everyPositionCodesInt == 22){
		cathecticCount += 21;
	}else if(everyPositionCodesInt == 23){
		cathecticCount += 15;
	}else if(everyPositionCodesInt == 24){
		cathecticCount += 10;
	}else if(everyPositionCodesInt == 25){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 26){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 27){
		cathecticCount += 1;
	}else{
		showTip("无该和值的号码");
	}
	return cathecticCount;	
};

/**
 * 前三或者后三直选和值,注数
 */
KsData.prototype.getLotteryCountByShanZuHeZhi = function(everyPositionCodesInt){
	var cathecticCount = 0;
    if(everyPositionCodesInt == 1){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 2){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 3){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 4){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 5){
		cathecticCount += 5;
	}else if(everyPositionCodesInt == 6){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 7){
		cathecticCount += 8;
	}else if(everyPositionCodesInt == 8){
		cathecticCount += 10;
	}else if(everyPositionCodesInt == 9){
		cathecticCount += 11;
	}else if(everyPositionCodesInt == 10){
		cathecticCount += 13;
	}else if(everyPositionCodesInt == 11){
		cathecticCount += 14;
	}else if(everyPositionCodesInt == 12){
		cathecticCount += 14;
	}else if(everyPositionCodesInt == 13){
		cathecticCount += 15;
	}else if(everyPositionCodesInt == 14){
		cathecticCount += 15;
	}else if(everyPositionCodesInt == 15){
		cathecticCount += 14;
	}else if(everyPositionCodesInt == 16){
		cathecticCount += 14;
	}else if(everyPositionCodesInt == 17){
		cathecticCount += 13;
	}else if(everyPositionCodesInt == 18){
		cathecticCount += 11;
	}else if(everyPositionCodesInt == 19){
		cathecticCount += 10;
	}else if(everyPositionCodesInt == 20){
		cathecticCount += 8;
	}else if(everyPositionCodesInt == 21){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 22){
		cathecticCount += 5;
	}else if(everyPositionCodesInt == 23){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 24){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 25){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 26){
		cathecticCount += 1;
	}else{
		showTip("无该和值的号码");
	}
	return cathecticCount;	
};

/**
 * 前二或者后二直选和值,注数
 */
KsData.prototype.getLotteryCountByErZhiHeZhi = function(everyPositionCodesInt){
	var cathecticCount = 0;
	if(everyPositionCodesInt == 0){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 1){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 2){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 3){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 4){
		cathecticCount += 5;
	}else if(everyPositionCodesInt == 5){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 6){
		cathecticCount += 7;
	}else if(everyPositionCodesInt == 7){
		cathecticCount += 8;
	}else if(everyPositionCodesInt == 8){
		cathecticCount += 9;
	}else if(everyPositionCodesInt == 9){
		cathecticCount += 10;
	}else if(everyPositionCodesInt == 10){
		cathecticCount += 9;
	}else if(everyPositionCodesInt == 11){
		cathecticCount += 8;
	}else if(everyPositionCodesInt == 12){
		cathecticCount += 7;
	}else if(everyPositionCodesInt == 13){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 14){
		cathecticCount += 5;
	}else if(everyPositionCodesInt == 15){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 16){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 17){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 18){
		cathecticCount += 1;
	}else{
		showTip("无该和值的号码");
	}
	return cathecticCount;
};

/**
 * 前三或者后三直选和值,注数
 */
KsData.prototype.getLotteryCountByErZuHeZhi = function(everyPositionCodesInt){
	var cathecticCount = 0;
	if(everyPositionCodesInt == 1){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 2){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 3){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 4){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 5){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 6){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 7){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 8){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 9){
		cathecticCount += 5;
	}else if(everyPositionCodesInt == 10){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 11){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 12){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 13){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 14){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 15){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 16){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 17){
		cathecticCount += 1;
	}else{
		showTip("无该和值的号码");
	}
	return cathecticCount;
};
