function LhcPage(){}
var lhcPage = new LhcPage();

//投注参数
lhcPage.lotteryParam = {
	lotteryKindMap : new JS_OBJECT_MAP(),
    currentKindKey : 'TMB',    //当前所选择的玩法模式,默认是特码A
	currentLotteryCount : 0,       //当前投注数目
	currentLotteryPrice : 0,       //当前投注价格,按元来计算
	currentLotteryTotalCount : 0,  //当前总投注数目
	codes : null,                  //投注号码
	codesStastic : new Array()
};

//基本参数
lhcPage.param = {
	unitPrice : 1,  //每注价格
	kindDes : "六合彩",
	kindName : 'LHC',  //彩种
	kindNameType : 'LHC', //大彩种拼写
	ommitData : null,  //遗漏数据
	hotColdData : null //冷热数据
};

var quickmoney = 0;
var liuhecai_interval=false;
$(document).ready(function(){
	
	//左右滑动按钮
	$('.left_but').on('click', function() {
        var scrollLeft = $('.slide-container').scrollLeft();
        $('.slide-container').scrollLeft(scrollLeft - 300);
    })
    $('.right_but').on('click', function() {
        var scrollLeft = $('.slide-container').scrollLeft();
        $('.slide-container').scrollLeft(scrollLeft + 300);
    })
	
	lotteryLhcCommonPage.currentLotteryKindInstance  = lhcPage; 
	//toggle();
	//checkbox();
	lhcPage.initPlayCodeDesc();
	lhcPage.initShengXiaoNumber();
	lhcPage.initModelNun();
	
	//加载生肖号码
	lhcPage.initShengXiaoNumber();
	
	lotteryLhcCommonPage.toggleMun();
	lotteryLhcCommonPage.lottery_init();
	
	animate_add(".main-head","fadeInDown",0);
	animate_add(".lottery-content","fadeInUp",0);
	animate_add(".lottery-bar .bar","fadeInUp",0);
	lhcPage.initFirstPlay("TMB");
	
	//点击开奖号码显示开奖记录事件
	$(".lottery_run").unbind("click").click(function(){
		var parent=$("body");
		if(parent.find(".lottery-dynamic").hasClass('in')){
			parent.find(".lottery-dynamic").removeClass('in').addClass('out');
		}else{
			parent.find(".lottery-dynamic").removeClass('out').addClass('in');
		}
	});
	
	//金额减少事件
	$("#beishuSub").unbind("click").click(function(){
		var beishuStr = $('#shortcut-money').val();
		 if(beishuStr == ""){
				showTip("请输入下注金额！");
				$("#shortcut-money").focus();
				return ;
			}
		  if(isNaN(beishuStr)){
		    	showTip("请输入数字！");
				$("#shortcut-money").focus();	
				return ;
		    }
		var beishuTemp = parseInt(beishuStr);
		beishuTemp--;
		if(beishuTemp <= 1) {
			showTip("最低下注金额2元");
			return;
		}
		$("#shortcut-money").val(beishuTemp);
		lotteryLhcCommonPage.beishuEventContent();
	});
	
	//金额增加事件
	$("#beishuAdd").unbind("click").click(function(){
		var beishuStr = $('#shortcut-money').val();
	    if(beishuStr == ""){
			showTip("请输入下注金额！");
			$("#shortcut-money").focus();
			return ;
		}
	    if(isNaN(beishuStr)){
	    	showTip("请输入数字！");
			$("#shortcut-money").focus();	
			return ;
	    }
		var beishuTemp = parseInt(beishuStr);
		beishuTemp++;
		if(beishuTemp > 9999) {
			showTip("当前金额已经是最高了");
			return;
		}
		$("#shortcut-money").val(beishuTemp);
		lotteryLhcCommonPage.beishuEventContent();
	});
	
	$("#shortcut-money").keyup(function(){
		var beishuStr = $('#shortcut-money').val();
		 if(beishuStr == ""){
				showTip("请输入下注金额！");
				$("#shortcut-money").focus();
				return ;
		 }
		  if(isNaN(beishuStr)){
		    	showTip("请输入数字！");
				$("#shortcut-money").focus();	
				return ;
		    }
		var beishuTemp = parseInt(beishuStr);
		 beishuTemp++;
		if(beishuTemp > 9999) {
			showTip("当前金额已经是最高了");
			$("#shortcut-money").val(9999);
			return;
		}
		if(beishuTemp < 2) {
			showTip("最低下注金额2元!");
			$("#shortcut-money").val(2);
			return;
		}
		quickmoney = beishuTemp;
		lotteryLhcCommonPage.beishuEventContent();
	});
	
	//开奖效果
//	if(!liuhecai_interval){
//		liuhecai_interval=true;
//		setInterval(function(){
//			lotteryLhcCommonPage.lottery_run(".lottery_liuhecai .lottery_run .child-muns",'<div class="lhc-mun lhc-red">46</div><div class="lhc-mun lhc-green">32</div><div class="lhc-mun lhc-red">34</div><div class="lhc-mun lhc-blue">15</div><div class="lhc-mun lhc-green">16</div><div class="lhc-mun lhc-red">13</div><div class="lhc-mun lhc-yellow">10</div>');
//		},60000);
//	}

});



/**
 * 初始化号码描述
 */
LhcPage.prototype.initPlayCodeDesc = function (){
	
	$.ajax({
		type : "POST",
		url : contextPath + "/lottery/initLotteryWinLhcDesc",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			var resultData = result.data;
			if(result.code == "ok"){
				lhcData.playNumDescMap = result.data;
			}else if(result.code == "error"){
				showTip(resultData);
				$("#editUserButton").removeAttr("disabled");
			}
		}
	});
}


/**
 * 初始化号码玩法
 */
LhcPage.prototype.initModelNun = function (){
	
	//maxNum:每个玩法的最大号码，lotteryCodeNum：每注需要的号码数
	//特码
	lhcData.modelMap.put("TMA",{maxNum:66,lotteryCodeNum:1,playDesc:"特码-特码A"});
	lhcData.modelMap.put("TMB",{maxNum:66,lotteryCodeNum:1,playDesc:"特码-特码B"});
	//正码
	lhcData.modelMap.put("ZMA",{maxNum:53,lotteryCodeNum:1,playDesc:"正码-正码A"});
	lhcData.modelMap.put("ZMB",{maxNum:53,lotteryCodeNum:1,playDesc:"正码-正码B"});
	//正码特
	lhcData.modelMap.put("ZMT1",{maxNum:58,lotteryCodeNum:1,playDesc:"正码特-正一特"});
	lhcData.modelMap.put("ZMT2",{maxNum:58,lotteryCodeNum:1,playDesc:"正码特-正二特"});
	lhcData.modelMap.put("ZMT3",{maxNum:58,lotteryCodeNum:1,playDesc:"正码特-正三特"});
	lhcData.modelMap.put("ZMT4",{maxNum:58,lotteryCodeNum:1,playDesc:"正码特-正四特"});
	lhcData.modelMap.put("ZMT5",{maxNum:58,lotteryCodeNum:1,playDesc:"正码特-正五特"});
	lhcData.modelMap.put("ZMT6",{maxNum:58,lotteryCodeNum:1,playDesc:"正码特-正六特"});
	//正码1-6
	lhcData.modelMap.put("ZM16",{maxNum:78,lotteryCodeNum:1,playDesc:"正码1-6"});
	//连码
	lhcData.modelMap.put("LMSQZ",{maxNum:49,lotteryCodeNum:3,playDesc:"连码-三全中"});
	lhcData.modelMap.put("LMSZ22",{maxNum:49,lotteryCodeNum:3,playDesc:"连码-三中二"});
	lhcData.modelMap.put("LMSZ23",{maxNum:49,lotteryCodeNum:3,playDesc:"连码-三中二"});
	lhcData.modelMap.put("LMEQZ",{maxNum:49,lotteryCodeNum:2,playDesc:"连码-二全中"});
	lhcData.modelMap.put("LMEZT",{maxNum:49,lotteryCodeNum:2,playDesc:"连码-二中特"});
	lhcData.modelMap.put("LMEZ2",{maxNum:49,lotteryCodeNum:2,playDesc:"连码-二中特"});
	lhcData.modelMap.put("LMTC",{maxNum:49,lotteryCodeNum:2,playDesc:"连码-特串"});
	lhcData.modelMap.put("LMSZ1",{maxNum:49,lotteryCodeNum:4,playDesc:"连码-四中一"});
	//半波
	lhcData.modelMap.put("BB",{maxNum:18,lotteryCodeNum:1,playDesc:"半波"});
	//一肖尾数
	lhcData.modelMap.put("YXWS",{maxNum:22,lotteryCodeNum:1,playDesc:"一肖/尾数"});
	//特码生肖
	lhcData.modelMap.put("TMSX",{maxNum:12,lotteryCodeNum:1,playDesc:"特码生肖"});
	//合肖
	lhcData.modelMap.put("HX2",{maxNum:12,lotteryCodeNum:2,playDesc:"合肖-二肖"});
	lhcData.modelMap.put("HX3",{maxNum:12,lotteryCodeNum:3,playDesc:"合肖-三肖"});
	lhcData.modelMap.put("HX4",{maxNum:12,lotteryCodeNum:4,playDesc:"合肖-四肖"});
	lhcData.modelMap.put("HX5",{maxNum:12,lotteryCodeNum:5,playDesc:"合肖-五肖"});
	lhcData.modelMap.put("HX6",{maxNum:12,lotteryCodeNum:6,playDesc:"合肖-六肖"});
	lhcData.modelMap.put("HX7",{maxNum:12,lotteryCodeNum:7,playDesc:"合肖-七肖"});
	lhcData.modelMap.put("HX8",{maxNum:12,lotteryCodeNum:8,playDesc:"合肖-八肖"});
	lhcData.modelMap.put("HX9",{maxNum:12,lotteryCodeNum:9,playDesc:"合肖-九肖"});
	lhcData.modelMap.put("HX10",{maxNum:12,lotteryCodeNum:10,playDesc:"合肖-十肖"});
	lhcData.modelMap.put("HX11",{maxNum:12,lotteryCodeNum:11,playDesc:"合肖-十一肖"});
	//连肖
	lhcData.modelMap.put("LXELZ",{maxNum:12,lotteryCodeNum:2,playDesc:"连肖-二肖连中"});
	lhcData.modelMap.put("LXSLZ",{maxNum:12,lotteryCodeNum:3,playDesc:"连肖-三肖连中"});
	lhcData.modelMap.put("LXSILZ",{maxNum:12,lotteryCodeNum:4,playDesc:"连肖-四肖连中"});
	lhcData.modelMap.put("LXWLZ",{maxNum:12,lotteryCodeNum:5,playDesc:"连肖-五肖连中"});
	lhcData.modelMap.put("LXELBZ",{maxNum:12,lotteryCodeNum:2,playDesc:"连肖-二肖连不中"});
	lhcData.modelMap.put("LXSLBZ",{maxNum:12,lotteryCodeNum:3,playDesc:"连肖-三肖连不中"});
	lhcData.modelMap.put("LXSILBZ",{maxNum:12,lotteryCodeNum:4,playDesc:"连肖-四肖连不中"});
	
	//尾数连
	lhcData.modelMap.put("WSLELZ",{maxNum:10,lotteryCodeNum:2,playDesc:"尾数连-二尾连中"});
	lhcData.modelMap.put("WSLSLZ",{maxNum:10,lotteryCodeNum:3,playDesc:"尾数连-三尾连中"});
	lhcData.modelMap.put("WSLSILZ",{maxNum:10,lotteryCodeNum:4,playDesc:"尾数连-四尾连中"});
	lhcData.modelMap.put("WSLELBZ",{maxNum:10,lotteryCodeNum:2,playDesc:"尾数连-二尾连不中"});
	lhcData.modelMap.put("WSLSLBZ",{maxNum:10,lotteryCodeNum:3,playDesc:"尾数连-三尾连不中"});
	lhcData.modelMap.put("WSLSILBZ",{maxNum:10,lotteryCodeNum:4,playDesc:"尾数连-四尾连不中"});
	//全不中
	lhcData.modelMap.put("QBZ5",{maxNum:49,lotteryCodeNum:5,playDesc:"全不中-五不中"});
	lhcData.modelMap.put("QBZ6",{maxNum:49,lotteryCodeNum:6,playDesc:"全不中-六不中"});
	lhcData.modelMap.put("QBZ7",{maxNum:49,lotteryCodeNum:7,playDesc:"全不中-七不中"});
	lhcData.modelMap.put("QBZ8",{maxNum:49,lotteryCodeNum:8,playDesc:"全不中-八不中"});
	lhcData.modelMap.put("QBZ9",{maxNum:49,lotteryCodeNum:9,playDesc:"全不中-九不中"});
	lhcData.modelMap.put("QBZ10",{maxNum:49,lotteryCodeNum:10,playDesc:"全不中-十不中"});
	lhcData.modelMap.put("QBZ11",{maxNum:49,lotteryCodeNum:11,playDesc:"全不中-十一不中"});
	lhcData.modelMap.put("QBZ12",{maxNum:49,lotteryCodeNum:12,playDesc:"全不中-十二不中"});
}


LhcPage.prototype.select_type = function (parent,e){
	var $el = $(e),
			type = $el.attr("data-type");
			$(e).toggleClass("on");
	var key = {},
			$el = $(e) || {},
			bool = true,
			$btns =  $(parent+" .lottery-content-lhc .select-lhc .cols.on");

	for(var i = 1;i < 50; i++) key[i] = false;

	if(type == "reset"){
			$btns.removeClass("on");
			$(parent+" .lottery-content-lhc .content-lhc .cols.on").removeClass("on");
			$("#J-balls-statistics-lotteryNum").html(0);
			$("#J-balls-statistics-amount").html(0);
			$("#J-balls-statistics-code").text("（投注列表）");
			$(".lottery-bar .additional").hide();
	}else{
	$btns.each(function(i,n){
		var type = $(n).data("type");
		switch (type) {
			// 1-10
			case "1-10":for (var i = 1; i < 11; i++) key[i]=bool; break;
			// 11-20
			case "11-20":for (var i = 11; i < 21; i++) key[i]=bool; break;
			// 21-30
			case "21-30":for (var i = 21; i < 31; i++) key[i]=bool; break;
			// 31-40
			case "31-40":for (var i = 31; i < 41; i++) key[i]=bool; break;
			// 41-49
			case "41-49":for (var i = 41; i < 49; i++) key[i]=bool; break;
			//大
			case "da":for (var i = 25; i < 50; i++) key[i]=bool;break;
			//小
			case "xiao":for (var i = 1; i < 25; i++) key[i]=bool; break;
			//单
			case "dan":for (var i = 1; i < 50; i++) if(i%2 == 1)key[i]=bool; break;
			//双
			case "shuang":for (var i = 1; i < 50; i++) if(i%2 == 0)key[i]=bool; break;
			//合单
			case "hedan":
				var k = [1,3,5,7,9,10,12,14,16,18,21,23,25,27,29,30,32,34,36,38,41,43,45,47,49];
				for (var i in k) key[k[i]]=bool;
			break;
			//合双
			case "heshuang":
				var k = [2,4,6,8,11,13,15,17,19,20,22,24,26,28,31,33,35,37,39,40,42,44,46,48];
				for (var i in k) key[k[i]]=bool;
			break;

			//家禽
			case "jiaqin":
				var k = [1,3,4,9,11,12,13,15,16,21,23,24,25,27,28,33,35,36,37,39,40,45,47,48,49];
				for (var i in k) key[k[i]]=bool;
			break;
			//野兽
			case "yeshou":
				var k = [2,5,6,7,8,10,14,17,18,19,20,22,26,29,30,31,32,34,38,41,42,43,44,46];
				for (var i in k) key[k[i]]=bool;
			break;

			//红波
			case "hong":
				var k = [1,2,7,8,12,13,18,19,23,24,29,30,34,35,40,45,46];
				for (var i in k) key[k[i]]=bool;
			break;
			//蓝波
			case "lan":
				var k = [3,4,9,10,14,15,20,25,26,31,36,37,41,42,47,48];
				for (var i in k) key[k[i]]=bool;
			break;
			//绿波
			case "lv":
				var k = [5,6,11,16,17,21,22,27,28,32,33,38,39,43,44,49];
				for (var i in k) key[k[i]]=bool;
			break;
			default:

		}
	});
	}
	for(var i in key){
		if(key[i]){
			$(parent+" .lottery-content-lhc .content-lhc .cols.ball-"+i).addClass("on");
		}
		else{
			$(parent+" .lottery-content-lhc .content-lhc .cols.ball-"+i).removeClass("on");
		}
	}

}


/**
 * 初始化生肖号码描述
 */
LhcPage.prototype.initShengXiaoNumber = function (){
	
	$.ajax({
		type : "POST",
		url : contextPath + "/code/getShengXiaoNumber",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if(result.code == "ok"){
				lotteryLhcCommonPage.lottertyDataDeal.shengxiao.shengxiaoArr = result.data;
				lotteryLhcCommonPage.lottertyDataDeal.shengxiao.jiqin = result.data2;
				lotteryLhcCommonPage.lottertyDataDeal.shengxiao.yeshou = result.data3;
			}else if(result.code == "error"){
				frontCommonPage.showKindlyReminder(result.data);
				$("#editUserButton").removeAttr("disabled");
			}
		}
	});
}

//赔率初始化加载不出来，只能重新构建
LhcPage.prototype.initFirstPlay = function(model){
	
	var mainHtml = lotteryLhcCommonPage.lottertyDataDeal.lotteryKindPlayChoose(model);
	//parent.find(".lottery-content").html(mainHtml);
	$("#lhcContent").html(mainHtml);
	lotteryLhcCommonPage.toggleMun();
	element_toggle(false,['.lottery-classify']);
	
	//加载赔率
	lotteryLhcCommonPage.lottertyDataDeal.loadLotteryKindPL(model);
	//存储当前切换的玩法模式
	lhcPage.lotteryParam.currentKindKey = model;
	
	var currentKindName = lhcData.modelMap.get(model).playDesc;
    $("#currentKindName").text(currentKindName);
}
/**
 * 展示最新的开奖号码
 */
LhcPage.prototype.showLastLotteryCode = function(lotteryCode){
	if(lotteryCode != null){
		var lotteryNum = lotteryCode.lotteryNum;
		var openCodeStr = lhcPage.getOpenCodeStr(lotteryCode.lotteryNum);
		var  colors = lhcPage.changeColorByCode(lotteryCode);
		$('#J-lottery-info-lastnumber').text(openCodeStr);
		$("#lotteryNumber1").attr("class","lhc-mun small lhc-"+colors[0]);
		$("#lotteryNumber2").attr("class","lhc-mun small lhc-"+colors[1]);
		$("#lotteryNumber3").attr("class","lhc-mun small lhc-"+colors[2]);
		$("#lotteryNumber4").attr("class","lhc-mun small lhc-"+colors[3]);
		$("#lotteryNumber5").attr("class","lhc-mun small lhc-"+colors[4]);
		$("#lotteryNumber6").attr("class","lhc-mun small lhc-"+colors[5]);
		$("#lotteryNumber7").attr("class","lhc-mun small lhc-"+colors[6]);
		
		$('#lotteryNumber1').text(lotteryCode.numInfo1);
		$('#lotteryNumber2').text(lotteryCode.numInfo2);
		$('#lotteryNumber3').text(lotteryCode.numInfo3);
		$('#lotteryNumber4').text(lotteryCode.numInfo4);
		$('#lotteryNumber5').text(lotteryCode.numInfo5);
		$('#lotteryNumber6').text(lotteryCode.numInfo6);
		$('#lotteryNumber7').text(lotteryCode.numInfo7);
		
		$('#lotteryNumber1').closest('.lhc-mun-item').find('.btn-title').text(lhcPage.returnAninal(lotteryCode.numInfo1)).addClass('lhc-'+colors[0]);
		$('#lotteryNumber2').closest('.lhc-mun-item').find('.btn-title').text(lhcPage.returnAninal(lotteryCode.numInfo2)).addClass('lhc-'+colors[1]);
		$('#lotteryNumber3').closest('.lhc-mun-item').find('.btn-title').text(lhcPage.returnAninal(lotteryCode.numInfo3)).addClass('lhc-'+colors[2]);
		$('#lotteryNumber4').closest('.lhc-mun-item').find('.btn-title').text(lhcPage.returnAninal(lotteryCode.numInfo4)).addClass('lhc-'+colors[3]);
		$('#lotteryNumber5').closest('.lhc-mun-item').find('.btn-title').text(lhcPage.returnAninal(lotteryCode.numInfo5)).addClass('lhc-'+colors[4]);
		$('#lotteryNumber6').closest('.lhc-mun-item').find('.btn-title').text(lhcPage.returnAninal(lotteryCode.numInfo6)).addClass('lhc-'+colors[5]);
		$('#lotteryNumber7').closest('.lhc-mun-item').find('.btn-title').text(lhcPage.returnAninal(lotteryCode.numInfo7)).addClass('lhc-'+colors[6]);
	}
};


LhcPage.prototype.returnAninal = function(opencode){
	var shengxiaoArr =lotteryLhcCommonPage.lottertyDataDeal.shengxiao.shengxiaoArr;
	for(var i=0;i<shengxiaoArr.length;i++){		
		for(var j=0;j<shengxiaoArr[i].length;j++){
			if(Number(opencode) == Number(shengxiaoArr[i][j])){
				return lotteryLhcCommonPage.lottertyDataDeal.shengxiao.shengxiaoKey[i];
			}
		}
	}
	return "-";
}

//计算颜色
LhcPage.prototype.changeColorByCode = function(lotteryCode){
	var colors = new Array();
	var hong = lhcData.boshe.hongbo;
	var lan = lhcData.boshe.lanbo;
	var lv  = lhcData.boshe.lvbo;
	
	//判断是否是红波
	for(var i=0;i<hong.length;i++){
		if(Number(hong[i])==Number(lotteryCode.numInfo1)){
			colors[0] = 'red';
		}
		if(Number(hong[i])==Number(lotteryCode.numInfo2)){
			colors[1] = 'red';
		}
		if(Number(hong[i])==Number(lotteryCode.numInfo3)){
			colors[2] = 'red';
		}
		if(Number(hong[i])==Number(lotteryCode.numInfo4)){
			colors[3] = 'red';
		}
		if(Number(hong[i])==Number(lotteryCode.numInfo5)){
			colors[4] = 'red';
		}
		if(Number(hong[i])==Number(lotteryCode.numInfo6)){
			colors[5] = 'red';
		}
		if(Number(hong[i])==Number(lotteryCode.numInfo7)){
			colors[6] = 'red';
		}
	}
	
	//判断是否是蓝波
	for(var i=0;i<lan.length;i++){
		if(Number(lan[i])==Number(lotteryCode.numInfo1)){
			colors[0] = 'blue';
		}
		if(Number(lan[i])==Number(lotteryCode.numInfo2)){
			colors[1] = 'blue';
		}
		if(Number(lan[i])==Number(lotteryCode.numInfo3)){
			colors[2] = 'blue';
		}
		if(Number(lan[i])==Number(lotteryCode.numInfo4)){
			colors[3] = 'blue';
		}
		if(Number(lan[i])==Number(lotteryCode.numInfo5)){
			colors[4] = 'blue';
		}
		if(Number(lan[i])==Number(lotteryCode.numInfo6)){
			colors[5] = 'blue';
		}
		if(Number(lan[i])==Number(lotteryCode.numInfo7)){
			colors[6] = 'blue';
		}
	}
	
	//判断是否是绿波
	for(var i=0;i<lv.length;i++){
		if(Number(lv[i])==Number(lotteryCode.numInfo1)){
			colors[0] = 'green';
		}
		if(Number(lv[i])==Number(lotteryCode.numInfo2)){
			colors[1] = 'green';
		}
		if(Number(lv[i])==Number(lotteryCode.numInfo3)){
			colors[2] = 'green';
		}
		if(Number(lv[i])==Number(lotteryCode.numInfo4)){
			colors[3] = 'green';
		}
		if(Number(lv[i])==Number(lotteryCode.numInfo5)){
			colors[4] = 'green';
		}
		if(Number(lv[i])==Number(lotteryCode.numInfo6)){
			colors[5] = 'green';
		}
		if(Number(lv[i])==Number(lotteryCode.numInfo7)){
			colors[6] = 'green';
		}
	}
	
	return colors;
}

/**
 * 展示当前正在投注的期号
 */
LhcPage.prototype.showCurrentLotteryCode = function(lotteryIssue){
	if(lotteryIssue != null){
		var openCodeStr = lhcPage.getOpenCodeStr(lotteryIssue.lotteryNum);
		$('#nowNumber').text(openCodeStr);
	}
};

/**
 * 获取开奖号码前台展现的字符串
 */
LhcPage.prototype.getOpenCodeStr = function(openCodeNum){
	var openCodeStr = "";
	if(isNotEmpty(openCodeNum)){
		var expectDay = openCodeNum.substring(0,openCodeNum.length - 3);
		var expectNum = openCodeNum.substring(openCodeNum.length - 3,openCodeNum.length);
		openCodeStr = expectDay + "-" + expectNum;
	}
	return openCodeStr;
};

//服务器时间倒计时
LhcPage.prototype.calculateTime = function(isClose){
	var SurplusSecond = lotteryLhcCommonPage.param.diffTime;
	
	lotteryLhcCommonPage.param.isClose = isClose;
	if(lotteryLhcCommonPage.param.isClose){ 
		//显示预售中
		$("#timer").text("封盘中");
		window.clearInterval(lotteryLhcCommonPage.param.intervalKey); //终止周期性倒计时
	}else{
		var h = Math.floor(SurplusSecond/3600);
		if (h<10){
			h = "0"+h;
		}
		//计算剩余的分钟
		SurplusSecond = SurplusSecond - (h * 3600);
		var m = Math.floor(SurplusSecond/60);
		if (m<10){
			m = "0"+m;
		}
		var s = SurplusSecond%60;
		if(s<10){
			s = "0"+s;
		}
		h = h.toString();
		m = m.toString();
		s = s.toString();
		var timerStr = h+":"+m + ":" + s;
		$("#timer").text(timerStr);
	}
	lotteryLhcCommonPage.param.diffTime--;
	if(lotteryLhcCommonPage.param.diffTime < 0){
		isClose=true;	
		window.clearInterval(lotteryLhcCommonPage.param.intervalKey); //终止周期性倒计时
		lotteryLhcCommonPage.getActiveExpect();  //倒计时结束重新请求最新期号
	};
};


