function Jspk10Page(){}
var jspk10Page = new Jspk10Page();

//数据集合
jspk10Page.showdatas = {
		codePlans : new Array(),
		subKindVOs : new Array()
}

//基本参数
jspk10Page.param = {
	unitPrice : 2,  //每注价格
	kindDes : "极速PK10",
	kindName : 'JSPK10',  //彩种
	kindNameType : 'PK10', //大彩种拼写
	currentLotteryWins : new JS_OBJECT_MAP(),  //奖金映射 
	currentLotteryWinsLoaded : false,  //奖金映射是否加载完毕
	ommitData : null,  //遗漏数据
	hotColdData : null //冷热数据
};

//投注参数
jspk10Page.lotteryParam = {
    currentKindKey : 'CGJ',    //当前所选择的玩法模式,默认是猜冠军
	currentLotteryCount : 0,       //当前投注数目
	currentLotteryPrice : 0,       //当前投注价格,按元来计算
	currentLotteryTotalCount : 0,  //当前总投注数目
	codes : null,                  //投注号码
	codesStastic : new Array(),
	codesStr : ""   //投注下方显示的号码
};

$(document).ready(function(){
	//设置数据处理器  默认时时彩的
	lotteryCommonPage.lottertyDataDeal = pk10Data;
	//将这个投注实例传至common js当中
	lotteryCommonPage.currentLotteryKindInstance = jspk10Page; 
	//设置投注的模式
	lotteryCommonPage.lotteryParam.modelValue = currentUserPk10Model;
	lowestAwardModel = pk10LowestAwardModel;
	//获取当前彩种的所有奖金列表
	lotteryCommonPage.currentLotteryKindInstance.getLotteryWins(); 
	//获取当前投注模式  元角模式
	lotteryCommonPage.loadUserModelSet();
	setTimeout("jspk10Page.initLottery()", 100);
	/*document.getElementById("codePlanList").style.display="none";
	$("#change-title").unbind("click").click(function(event){
		var value = document.getElementById("change-title").getAttribute("value");
		if(value=="0"){//投注记录>计划列表
			document.getElementById("change-title").setAttribute("value","1");
			$("#change-title").html("计划列表");
			document.getElementById("userTodayLotteryList").style.display="none";
			document.getElementById("codePlanList").style.display="block";
		}else if(value=="1"){//计划列表>投注记录
			document.getElementById("change-title").setAttribute("value","0");
			$("#change-title").html("投注记录");
			document.getElementById("codePlanList").style.display="none";
			document.getElementById("userTodayLotteryList").style.display="block";
		}
	});
	jspk10Page.codePlanList(jspk10Page.param);*/
	
});

/**
 * 获取该彩种计划列表
 */
Jspk10Page.prototype.codePlanList = function(param){
	var jsonData = {"lotteryType":param.kindName,"lateExpectNum":10};
	$.ajax({
		type : "POST",
		//设置Ajax请求改为同步请求!
		async: false,
		url : contextPath + "/codeplan/getCodePlan",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var codePlanList = result.data.codePlans;
				var subKindVOs = result.data.subKindVOs;
				jspk10Page.showdatas.codePlans = result.data.codePlans;
				jspk10Page.showdatas.subKindVOs = result.data.subKindVOs;
//				$("#codePlanList").html("<div class='siftings-line'><div colspan='12'>正在加载...</div></div>");
				jspk10Page.showData(codePlanList,subKindVOs);
			} else if (result.code == "error") {
				showTip(result.data);
			}
		}
	});
}

/**
 * 
 */
Jspk10Page.prototype.showData = function(codePlans,subKindVOs,subkind){
	if(codePlans.length > 0){
		//彩种游戏下方的游戏记录
		var codePlanListObj = $("#codePlanList");
		codePlanListObj.html("");
		codePlanListObj.append("<div class='description' style='font-size: 40px;margin-left: 43%;'>极速PK10预测</div>");
		//加载彩种
		var subKindhtml = "";
		for(var j=0;j<subKindVOs.length;j++){
			subKindhtml += "<li><p data-value='"+subKindVOs[j].code
			+"' style='font-size: 28px;'>"+subKindVOs[j].description+"</p></li>";
		}
		//判断是否第一次加载彩种
		var subkindvo = "";
		if(subkind==null || subkind=="undefined"){
			subkindvo += "<input type='hidden' class='select-save' id='lotteryKind' value='"+subKindVOs[0].code+"' />"+
			"<p class='select-title'>"+subKindVOs[0].description+"</p>";
		}else {
			subkindvo += "<input type='hidden' class='select-save' id='lotteryKind' value='"+subkind.code+"' />"+
			"<p class='select-title'>"+subkind.description+"</p>";
		}
		var str = "";
		str += "<div class='line'>";
		str += "	<div class='child child1extend'><p>期数</p></div>";
		str += "	<div class='child child2extend'><div class='line-content select-content sort' id='sortlist'>"+subkindvo+
		"<ul class='select-list myList' id='lotteryKinds' style='display: none;'>"+subKindhtml+"</ul></div></div>";
		str += "	<div class='child child3extend'><p>开奖号码</p></div>";
		str += "	<div class='child child4extend'><p>期号</p></div>";
		subkindvo = "";
		subKindhtml = "";
		str += "	<div class='child child5extend'><p>中奖状态</p></div>";
		str += "</div>";
		codePlanListObj.append(str);
		str = "";
		var playkindvalue = $("#lotteryKind").val();
		for(var i = 0; i < codePlans.length; i++){
			var codeplan = codePlans[i];
			if(codeplan.playkind == playkindvalue){
				str += "<div class='line'>";
				str += "	<div class='child child1extend'><p>"+ codeplan.expectsPeriod +"</p></div>";
				var kongge1 = "";
				if(codeplan.codesDesc.length>20){
					for(var j=0;j<codeplan.codesDesc.length/4;j++){
						kongge1 = kongge1 + '&nbsp;';
					}
				}
				var kongge2 = "";
				if(codeplan.openCode.length>20){
					for(var j=0;j<codeplan.openCode.length/4;j++){
						kongge2 = kongge2 + '&nbsp;';
					}
				}
				if(codeplan.codesDesc.length>20){
					str += "	<div class='child child2extend'><p  style='font-size: 28px;'>"+ kongge1 + codeplan.codesDesc.substring(0,codeplan.codesDesc.length/2)+'<br/>'+ kongge1
					+codeplan.codesDesc.substring(codeplan.codesDesc.length/2,codeplan.codesDesc.length) +"</p></div>";
				}else {
					str += "	<div class='child child2extend'><p  style='font-size: 28px;'>"+ codeplan.codesDesc +"</p></div>";
				}
				if(codeplan.openCode=="" || codeplan.enabled==0){
					str += "	<div class='child child3extend'><p>"+codeplan.prostateDesc+"</p></div>";
				}else{
					if(codeplan.openCode.length>20){
						str += "	<div class='child child3extend'><p  style='font-size: 28px;'>"+  kongge2 + codeplan.openCode.substring(0,codeplan.openCode.length/2)+'<br/>'+  kongge2
						+codeplan.openCode.substring(codeplan.openCode.length/2,codeplan.openCode.length) +"</p></div>";
					}else {
						str += "	<div class='child child3extend'><p>【"+ codeplan.openCode +"】</p></div>";
					}
				}
				if(codeplan.expectShort==""){
					str += "	<div class='child child4extend'><p>"+codeplan.prostateDesc+"</p></div>";
				}else{
					str += "	<div class='child child4extend'><p>"+ codeplan.expectShort +"期</p></div>";
				}
				str += "	<div class='child child5extend'><p class='cp-yellow'>"+codeplan.prostateDesc+"</p></div>";
				str += "</div>";
				codePlanListObj.append(str);
			}
			str = "";
		}
		$(".select-title").unbind("click").click(function(event){
			if(document.getElementById('lotteryKinds').getAttribute('style')=="display: none;"){
				document.getElementById('lotteryKinds').setAttribute('style','display: block;')
			}else{
				document.getElementById('lotteryKinds').setAttribute('style','display: none;')
			}
		});
		$("#lotteryKinds li").unbind("click").click("unbind",function(){
			var parent_select=$(this).closest(".select-list");
			parent_select.find("li").removeClass("on");
			$(this).addClass("on");
			var parent=$(this).closest(".select-content");
			var val=$(this).html();
			
			var html=$(val).html();
			var valLen=$(val).html().length;
			if(!isNaN(parseInt(html)))valLen=valLen/2;
			valLen=valLen*16+64;
			parent.css("width",valLen+"px")
			
			var realVal = $(this).find("p").attr("data-value");
			parent.find(".select-save").val(realVal);
			parent.find(".select-title").html(val);
			var subkind = {"code":realVal,"description":html};
			jspk10Page.showData(jspk10Page.showdatas.codePlans,jspk10Page.showdatas.subKindVOs,subkind);
		});
	}
}


/**
 * 获取开奖号码前台展现的字符串
 */
Jspk10Page.prototype.getOpenCodeStr = function(openCodeNum){
	var openCodeStr = "";
	if(isNotEmpty(openCodeNum)){
		var expectDay = openCodeNum.substring(0,openCodeNum.length - 4);
		var expectNum = openCodeNum.substring(openCodeNum.length - 4,openCodeNum.length);
		openCodeStr = expectDay +"-"+ expectNum;
	}
	return openCodeStr;
};

/**
 * 展示当前正在投注的期号
 */
Jspk10Page.prototype.showCurrentLotteryCode = function(lotteryIssue){
	if(lotteryIssue != null){
		var openCodeStr = jspk10Page.getOpenCodeStr(lotteryIssue.lotteryNum);
		$('#nowNumber').text(openCodeStr);
	}
};

/**
 * 展示最新的开奖号码
 */
Jspk10Page.prototype.showLastLotteryCode = function(lotteryCode){
	if(lotteryCode != null){
		var lotteryNum = lotteryCode.lotteryNum;
		var openCodeStr = jspk10Page.getOpenCodeStr(lotteryCode.lotteryNum);
		
		$('#J-lottery-info-lastnumber').text(openCodeStr);
		
		var openCodeStr = "";
		$(".lottery_run .child-muns").empty();
		openCodeStr += "<span class='ball color-red' id='lotteryNumber1'>" + lotteryCode.numInfo1 + "</span>";
		openCodeStr += "<span class='ball color-red' id='lotteryNumber2'>" + lotteryCode.numInfo2 + "</span>";
		openCodeStr += "<span class='ball color-red' id='lotteryNumber3'>" + lotteryCode.numInfo3 + "</span>";
		openCodeStr += "<span class='ball color-red' id='lotteryNumber4'>" + lotteryCode.numInfo4 + "</span>";
		openCodeStr += "<span class='ball color-red' id='lotteryNumber5'>" + lotteryCode.numInfo5 + "</span></br>";
		openCodeStr += "<span class='ball color-red' id='lotteryNumber5'>" + lotteryCode.numInfo6 + "</span>";
		openCodeStr += "<span class='ball color-red' id='lotteryNumber5'>" + lotteryCode.numInfo7 + "</span>";
		openCodeStr += "<span class='ball color-red' id='lotteryNumber5'>" + lotteryCode.numInfo8 + "</span>";
		openCodeStr += "<span class='ball color-red' id='lotteryNumber5'>" + lotteryCode.numInfo9 + "</span>";
		openCodeStr += "<span class='ball color-red' id='lotteryNumber5'>" + lotteryCode.numInfo10 + "</span>";
		$(".lottery_run .child-muns").append(openCodeStr);
		
	}
};


/**
 * 查找奖金对照表
 */
Jspk10Page.prototype.getLotteryWins = function(){
	//还未加载奖金的
	if(!jspk10Page.param.currentLotteryWinsLoaded) {
		var jsonData = {
				"lotteryKind" : jspk10Page.param.kindName,
			};
		$.ajax({
			type : "POST",
			// 设置请求为同步请求
			async : false,
			url : contextPath + "/lottery/getLotteryWins",
			data : JSON.stringify(jsonData),
			dataType : "json",
			contentType : 'application/json;charset=utf-8',
			success : function(result) {
				commonHandlerResult(result, this.url);
				var resultData = result.data;
				if (result.code == "ok") {
					for ( var key in resultData) {
						// 将奖金对照表缓存
						jspk10Page.param.currentLotteryWins.put(key, resultData[key]);
					}
					jspk10Page.param.currentLotteryWinsLoaded = true;
				} else if (result.code == "error") {
					showTip(resultData);
				}
			}
		});
	}
};


/**
 * 选好号码事件，添加号码到投注池中
 */
Jspk10Page.prototype.userLotteryNumForAddOrder = function(){
	//判断是否单式玩法
	var kindKey = jspk10Page.lotteryParam.currentKindKey;
	if(kindKey == 'QEZXDS' || kindKey == 'QSZXDS' || kindKey == 'QSIZXDS' || kindKey == 'QWZXDS'){  
		//需要走单式的验证
		return jspk10Page.singleLotteryNum();
	}else{
		return jspk10Page.userLotteryNum();
	}
};

/**
 * 用户添加投注号码（复式）
 */
Jspk10Page.prototype.userLotteryNum = function(){
	
	if(jspk10Page.lotteryParam.codes == null || jspk10Page.lotteryParam.codes.length == 0){
		showTip("号码选择不完整，请重新选择！");
		return false;
	}
	
	//添加进投注统计号码池 金额\注数\倍数\最高奖金\玩法描述\号码
	var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
	var currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get(jspk10Page.param.kindNameType +"_"+jspk10Page.lotteryParam.currentKindKey);
	
	//如果处于修改投注号码的状态
	if(lotteryCommonPage.lotteryParam.currentIsUpdateCodeStastic && lotteryCommonPage.lotteryParam.updateCodeStasticIndex != null){
		var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[lotteryCommonPage.lotteryParam.updateCodeStasticIndex];
		codeStastic[0] = jspk10Page.lotteryParam.currentLotteryPrice;
		codeStastic[1] = jspk10Page.lotteryParam.currentLotteryCount;
		codeStastic[2] = lotteryCommonPage.lotteryParam.beishu;
		codeStastic[3] = lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu);
		codeStastic[4] = lotteryCommonPage.lottertyDataDeal.lotteryKindMap.get(jspk10Page.lotteryParam.currentKindKey);
		codeStastic[5] = jspk10Page.lotteryParam.codes;
		codeStastic[6] = jspk10Page.param.kindNameType +"_"+jspk10Page.lotteryParam.currentKindKey;
		codeStastic[7] = codeStastic[7];

		//倍数更新
		lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStastic[7]); //先清除原来的投注号码的倍数
		lotteryCommonPage.lotteryParam.lotteryMultipleMap.put(codeStastic[7],lotteryCommonPage.lotteryParam.beishu);

		//将所选号码和玩法的映射放置map中
		var lotteryKindMap =  new JS_OBJECT_MAP();
		lotteryKindMap.put(jspk10Page.lotteryParam.currentKindKey,jspk10Page.lotteryParam.codes);
		lotteryCommonPage.lotteryParam.lotteryMap.remove(codeStastic[7]); //先清除原来的
		lotteryCommonPage.lotteryParam.lotteryMap.put(codeStastic[7],lotteryKindMap);
		
		//清空当前选中的号码
		//lotteryCommonPage.clearCurrentCodes();			
	
	}else{
		var isYetHave = false;
		var yetIndex = null;
		var codeStastics = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic;
		for(var i = codeStastics.length - 1; i >= 0 ; i--){
			 var codeStastic = codeStastics[i];
		     var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
			 if(kindKey == jspk10Page.lotteryParam.currentKindKey && codeStastic[5].toString() == jspk10Page.lotteryParam.codes){
				 isYetHave = true;
				 yetIndex = i;
				 break;
			 }
		}
		
		if(isYetHave && yetIndex != null){
			showTip("您选择的号码在号码篮已存在，将直接进行倍数累加!");
			var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[yetIndex];
			codeStastic[0] = (parseFloat(codeStastic[0]) + parseFloat(jspk10Page.lotteryParam.currentLotteryPrice)).toFixed(lotteryCommonPage.param.toFixedValue);
			codeStastic[1] = codeStastic[1];
			codeStastic[2] = parseInt(codeStastic[2]) + lotteryCommonPage.lotteryParam.beishu;
			codeStastic[3] = (parseFloat(codeStastic[3]) + parseFloat(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu))).toFixed(lotteryCommonPage.param.toFixedValue);
			codeStastic[4] = lotteryCommonPage.lottertyDataDeal.lotteryKindMap.get(jspk10Page.lotteryParam.currentKindKey);
			codeStastic[5] = jspk10Page.lotteryParam.codes;
			codeStastic[6] = jspk10Page.param.kindNameType +"_"+jspk10Page.lotteryParam.currentKindKey;
			codeStastic[7] = codeStastic[7];
			
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStastic[7]); //先清除原来的投注号码的倍数
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put(codeStastic[7],codeStastic[2]);
		}else{
			jspk10Page.lotteryParam.currentLotteryTotalCount++;  //投注数目递增
			//将所选号码和玩法的映射放置map中
			var lotteryKindMap =  new JS_OBJECT_MAP();
			lotteryKindMap.put(jspk10Page.lotteryParam.currentKindKey,jspk10Page.lotteryParam.codes);
			lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_"+jspk10Page.lotteryParam.currentLotteryTotalCount,lotteryKindMap);
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_"+jspk10Page.lotteryParam.currentLotteryTotalCount,lotteryCommonPage.lotteryParam.beishu);
			
			//进入投注池当中
			var codeRecord = new Array();
			codeRecord.push(jspk10Page.lotteryParam.currentLotteryPrice);
			codeRecord.push(jspk10Page.lotteryParam.currentLotteryCount);
			codeRecord.push(lotteryCommonPage.lotteryParam.beishu);
			codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu));
			codeRecord.push(lotteryCommonPage.lottertyDataDeal.lotteryKindMap.get(jspk10Page.lotteryParam.currentKindKey));
			codeRecord.push(jspk10Page.lotteryParam.codes);
			codeRecord.push(jspk10Page.param.kindNameType +"_"+jspk10Page.lotteryParam.currentKindKey); //标识玩法
			codeRecord.push("lottery_id_"+jspk10Page.lotteryParam.currentLotteryTotalCount); //标识该条记录的lotteryMap
			jspk10Page.lotteryParam.codesStastic.push(codeRecord);			
		}
	}
	
	//选好号码统计金额注数
	lotteryCommonPage.codeStastics(); 
	return true;
};



/**
 * 根据彩种玩法,获取所选的号码
 * @param kindKey
 */
Jspk10Page.prototype.getCodesByKindKey = function(){
    var codes = "";
    var lotteryCount = 0;
    //校验
	if(jspk10Page.lotteryParam.currentKindKey == 'QYZXFS'
		|| jspk10Page.lotteryParam.currentKindKey == 'QEZXFS'
			|| jspk10Page.lotteryParam.currentKindKey == 'QEZXDS'
			|| jspk10Page.lotteryParam.currentKindKey == 'QSZXFS'
			|| jspk10Page.lotteryParam.currentKindKey == 'QSZXDS'
			|| jspk10Page.lotteryParam.currentKindKey == 'QSIZXFS'
			|| jspk10Page.lotteryParam.currentKindKey == 'QSIZXDS'
			|| jspk10Page.lotteryParam.currentKindKey == 'QWZXFS'
			|| jspk10Page.lotteryParam.currentKindKey == 'QWZXDS'
			|| jspk10Page.lotteryParam.currentKindKey == 'DWDZXFS'
			|| jspk10Page.lotteryParam.currentKindKey == 'DWDZXDS'
			|| jspk10Page.lotteryParam.currentKindKey == 'DWD'
			|| jspk10Page.lotteryParam.currentKindKey == 'LHD1VS10'
			|| jspk10Page.lotteryParam.currentKindKey == 'LHD2VS9'
			|| jspk10Page.lotteryParam.currentKindKey == 'LHD3VS8'
			|| jspk10Page.lotteryParam.currentKindKey == 'LHD4VS7'
			|| jspk10Page.lotteryParam.currentKindKey == 'LHD5VS6'
			|| jspk10Page.lotteryParam.currentKindKey == 'DXGJ'
			|| jspk10Page.lotteryParam.currentKindKey == 'DXYJ'
			|| jspk10Page.lotteryParam.currentKindKey == 'DXJJ'
			|| jspk10Page.lotteryParam.currentKindKey == 'DXDSM'
			|| jspk10Page.lotteryParam.currentKindKey == 'DXDWM'
			|| jspk10Page.lotteryParam.currentKindKey == 'DSGJ'
			|| jspk10Page.lotteryParam.currentKindKey == 'DSYJ'
			|| jspk10Page.lotteryParam.currentKindKey == 'DSJJ'
			|| jspk10Page.lotteryParam.currentKindKey == 'DSDSM'
			|| jspk10Page.lotteryParam.currentKindKey == 'DSDWM'
			|| jspk10Page.lotteryParam.currentKindKey == 'HZGYJ'
			|| jspk10Page.lotteryParam.currentKindKey == 'CGJ'){
		
	    var numArrays = new Array();
        //获取 选择的号码
		$(".child-content .content").each(function(i){
			var numsStr = "";
			var lis = $(this).children();;
			for(var i = 0; i < lis.length; i++){
				var li = lis[i];
				var aNum = $(li);
				if($(aNum).hasClass('on')){
					numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT;
				}
			}
			
			//去除最后一个分隔符
			if(numsStr.substring(numsStr.length - 1, numsStr.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
				numsStr = numsStr.substring(0, numsStr.length -1);
			}
			//复式不能进行空串处理
			if(jspk10Page.lotteryParam.currentKindKey != 'QYZXFS'
				&& jspk10Page.lotteryParam.currentKindKey != 'QEZXFS'
					&& jspk10Page.lotteryParam.currentKindKey != 'QSZXFS'
					&& jspk10Page.lotteryParam.currentKindKey != 'QSIZXFS'
					&& jspk10Page.lotteryParam.currentKindKey != 'QWZXFS'){
				if(numsStr != ""){
					numArrays.push(numsStr);
				}
			}else{
				numArrays.push(numsStr);
			}
			
		});
		
		//猜冠军
		if(jspk10Page.lotteryParam.currentKindKey == 'CGJ'){
			$(".munbtn").each(function(i){
				var lis = $(this);
				for(var i = 0; i < lis.length; i++){
					var li = lis[i];
					var aNum = $(li);
					if($(aNum).parent().hasClass('on')){
						var ballNum = aNum.attr("name");
						if(ballNum == "ballNum_0_match"){
							jspk10Page.lotteryParam.currentKindKey = 'DXGJ';
						}else if(ballNum == "ballNum_1_match"){
							jspk10Page.lotteryParam.currentKindKey = 'DSGJ';
						}else if(ballNum == "ballNum_2_match"){
							jspk10Page.lotteryParam.currentKindKey = 'HZGYJ';
						}else if(ballNum == "ballNum_3_match"){
							jspk10Page.lotteryParam.currentKindKey = 'LHD1VS10';
						}
					}
				}
			});
			
	        //获取 选择的号码
			$(".munbtn").each(function(i){
				var numsStr = "";
				var lis = $(this);
				for(var i = 0; i < lis.length; i++){
					var li = lis[i];
					var aNum = $(li);
					if($(aNum).parent().hasClass('on')){
						numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT;
					}
				}
				
				//去除最后一个分隔符
				if(numsStr.substring(numsStr.length - 1, numsStr.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
					numsStr = numsStr.substring(0, numsStr.length -1);
				}
				
				if(numsStr != ""){
					numArrays.push(numsStr);
				}
			});
		}

		//处理投注下方显示的号码
		var codesDes = "";
		for(var i = 0; i < numArrays.length; i++){
			var numArray = numArrays[i];
			if(numArray.length == 0){ //为空
				continue;
			}
		    codesDes = codesDes + numArray + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		}
		codesDes = codesDes.substring(0, codesDes.length -1);
		jspk10Page.lotteryParam.codesStr = codesDes;
		
		if(jspk10Page.lotteryParam.currentKindKey == 'QYZXFS'
			|| jspk10Page.lotteryParam.currentKindKey == 'QEZXFS'
				|| jspk10Page.lotteryParam.currentKindKey == 'QSZXFS'
				|| jspk10Page.lotteryParam.currentKindKey == 'QSIZXFS'
				|| jspk10Page.lotteryParam.currentKindKey == 'QWZXFS'){
			
            lotteryCount = 1;
            
            //存储选中的号码
            var selNumArrays = new Array();
            var a,b,c,d,e;
            
			//统计号码
			for(var i = 0; i < numArrays.length; i++){
				var numArray = numArrays[i];
				if(numArray.length == 0){ //为空,表示号码选择未合法
					jspk10Page.lotteryParam.currentLotteryCount = 0;//此时未选注 
					jspk10Page.lotteryParam.codes = null;  //号码置为空
					lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				    return;
				}
				
				//统计当前数目
				lotteryCount = numArray.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length * lotteryCount;
				
				var everyNumArrays = numArray.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
				if(i == 0) {
					a = everyNumArrays;
				} else if(i == 1) {
					b = everyNumArrays;
				} else if(i == 2) {
					c = everyNumArrays;
				} else if(i == 3) {
					d = everyNumArrays;
				} else if(i == 4) {
					e = everyNumArrays;
				} 
			    codes = codes + numArray + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
			}
			codes = codes.substring(0, codes.length -1);
			if(jspk10Page.lotteryParam.currentKindKey == 'QYZXFS') {
				lotteryCount = a.length;
			} else if(jspk10Page.lotteryParam.currentKindKey == 'QEZXFS') {
				lotteryCount = jspk10Page.getLotteryCountByQE(a, b);
			} else if(jspk10Page.lotteryParam.currentKindKey == 'QSZXFS') {
				lotteryCount = jspk10Page.getLotteryCountByQS(a, b, c);
			} else if(jspk10Page.lotteryParam.currentKindKey == 'QSIZXFS') {
				lotteryCount = jspk10Page.getLotteryCountByQSI(a, b, c, d);
			} else if(jspk10Page.lotteryParam.currentKindKey == 'QWZXFS') {
				lotteryCount = jspk10Page.getLotteryCountByQW(a, b, c, d, e);
			}
		
		}else if(jspk10Page.lotteryParam.currentKindKey == 'QEDXDS' 
			|| jspk10Page.lotteryParam.currentKindKey == 'HEDXDS'){ 
			if(numArrays.length == 2){ //总共只有两位数
			   var num1Arrays = numArrays[0].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
			   var num2Arrays = numArrays[1].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
			   if((num1Arrays == null || num2Arrays == null) || (num1Arrays.length != 1 || num2Arrays.length != 1) || (num1Arrays[0].length == 0 || num2Arrays[0].length == 0)){
				   var codeDesc = numArrays[0] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + numArrays[1];
				   codeDesc = codeDesc.replaceAll("1","大");
				   codeDesc = codeDesc.replaceAll("2","小");
				   codeDesc = codeDesc.replaceAll("3","单");
				   codeDesc = codeDesc.replaceAll("4","双");
				   jspk10Page.lotteryParam.codesStr = codeDesc;
				   jspk10Page.lotteryParam.currentLotteryCount = 0;//此时未选注 
				   jspk10Page.lotteryParam.codes = null;  //号码置为空
				   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				   return;
			   }else{
				   jspk10Page.lotteryParam.currentLotteryCount = 1; //大小单双一次只能一注
				   lotteryCount = 1;
				   codes = num1Arrays[0] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + num2Arrays[0];
				   var codeDesc = codes;
				   codeDesc = codeDesc.replaceAll("1","大");
				   codeDesc = codeDesc.replaceAll("2","小");
				   codeDesc = codeDesc.replaceAll("3","单");
				   codeDesc = codeDesc.replaceAll("4","双");
				   jspk10Page.lotteryParam.codesStr = codeDesc;
			   }
			}else{
				if(numArrays == null || numArrays.length == 0) {
					jspk10Page.lotteryParam.codesStr = $(".ball-number-current").text();
				} else {
					var codeDesc = numArrays[0];
					codeDesc = codeDesc.replaceAll("1","大");
				    codeDesc = codeDesc.replaceAll("2","小");
				    codeDesc = codeDesc.replaceAll("3","单");
				    codeDesc = codeDesc.replaceAll("4","双");
				    jspk10Page.lotteryParam.codesStr = codeDesc;
				}
				jspk10Page.lotteryParam.currentLotteryCount = 0;//此时未选注 
				jspk10Page.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}			
		}else if(jspk10Page.lotteryParam.currentKindKey == 'DWD'){
			var lotteryNumMap = new JS_OBJECT_MAP();  //选择号码位数映射
	        //获取 选择的号码
			$(".child-content .content").each(function(i){
				var lis = $(this).children();
				var numsStr = "";
				for(var i = 0; i < lis.length; i++){
					var li = lis[i];
					var aNum = $(li);
					if($(aNum).hasClass('on')){
						numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT;
						lotteryNumMap.remove($(aNum).attr('name'));
						lotteryNumMap.put($(aNum).attr('name'),numsStr);
					}
				}
			});
			//标识是否有选择对应的位数
			var totalCount = 0;
			var isBallNum_0_match = false;
			var isBallNum_1_match = false;
			var isBallNum_2_match = false;
			var isBallNum_3_match = false;
			var isBallNum_4_match = false;
			var isBallNum_5_match = false;
			var isBallNum_6_match = false;
			var isBallNum_7_match = false;
			var isBallNum_8_match = false;
			var isBallNum_9_match = false;
		
				var lotteryNumMapKeys = lotteryNumMap.keys();
				//字符串截取
				for(var i = 0; i < lotteryNumMapKeys.length; i++){
					var mapValue = lotteryNumMap.get(lotteryNumMapKeys[i]);
					//去除最后一个分隔符
					if(mapValue.substring(mapValue.length - 1, mapValue.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
						mapValue = mapValue.substring(0, mapValue.length -1);
					}
					lotteryNumMap.remove(lotteryNumMapKeys[i]);
					lotteryNumMap.put(lotteryNumMapKeys[i],mapValue);
					
					if(lotteryNumMapKeys[i] == "ballNum_0_match"){
						isBallNum_0_match = true;
					}else if(lotteryNumMapKeys[i] == "ballNum_1_match"){
						isBallNum_1_match = true;
					}else if(lotteryNumMapKeys[i] == "ballNum_2_match"){
						isBallNum_2_match = true;
					}else if(lotteryNumMapKeys[i] == "ballNum_3_match"){
						isBallNum_3_match = true;
					}else if(lotteryNumMapKeys[i] == "ballNum_4_match"){
						isBallNum_4_match = true;
					}else if(lotteryNumMapKeys[i] == "ballNum_5_match"){
						isBallNum_5_match = true;
					}else if(lotteryNumMapKeys[i] == "ballNum_6_match"){
						isBallNum_6_match = true;
					}else if(lotteryNumMapKeys[i] == "ballNum_7_match"){
						isBallNum_7_match = true;
					}else if(lotteryNumMapKeys[i] == "ballNum_8_match"){
						isBallNum_8_match = true;
					}else if(lotteryNumMapKeys[i] == "ballNum_9_match"){
						isBallNum_9_match = true;
					}else{
						showTip("未知的号码位数");
					}
					totalCount += mapValue.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length;
				}			
			
			
				//统计投注数目
				if(totalCount == 0){
					jspk10Page.lotteryParam.currentLotteryCount = 0;//此时未选注
					jspk10Page.lotteryParam.codes = null;  //号码置为空
					lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
					return;
				}else{
					lotteryCount = totalCount;
				}
				
				//第一名
				if(isBallNum_0_match){
					codes = codes + lotteryNumMap.get("ballNum_0_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				}else{
					codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				}
		        //第二名
				if(isBallNum_1_match){
					codes = codes + lotteryNumMap.get("ballNum_1_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				}else{
					codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				}
		        //第三名
				if(isBallNum_2_match){
					codes = codes + lotteryNumMap.get("ballNum_2_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				}else{
					codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				}		
		        //第四名
				if(isBallNum_3_match){
					codes = codes + lotteryNumMap.get("ballNum_3_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT ;
				}else{
					codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				}	
		        //第五名
				if(isBallNum_4_match){
					codes = codes + lotteryNumMap.get("ballNum_4_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				}else{
					codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				}
				
				//第六名
				if(isBallNum_5_match){
					codes = codes + lotteryNumMap.get("ballNum_5_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				}else{
					codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				}
				
				//第七名
				if(isBallNum_6_match){
					codes = codes + lotteryNumMap.get("ballNum_6_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				}else{
					codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				}
				
				//第八名
				if(isBallNum_7_match){
					codes = codes + lotteryNumMap.get("ballNum_7_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				}else{
					codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				}
				
				 //第九名
				if(isBallNum_8_match){
					codes = codes + lotteryNumMap.get("ballNum_8_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				}else{
					codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				}
				
				 //第十名
				if(isBallNum_9_match){
					codes = codes + lotteryNumMap.get("ballNum_9_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				}else{
					codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				}
				
				codes = codes.substring(0, codes.length -1);
		
	}else if(jspk10Page.lotteryParam.currentKindKey == 'HZGYJ'){
		if(numArrays.length == 0){
			var numsStr = "";
			//获取 选择的号码
			$(".child-content .content .munbtn").each(function(i){
				var aNum = $(this);
				if(aNum.hasClass('on')){
					numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				}
			});
			//去除最后一个分隔符
			if(numsStr.substring(numsStr.length - 1, numsStr.length) == lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT){
				numsStr = numsStr.substring(0, numsStr.length -1);
			}
			//处理投注下方显示的号码
			jspk10Page.lotteryParam.codesStr = numsStr;
			//空串拦截
			if(numsStr == ""){
				jspk10Page.lotteryParam.currentLotteryCount = 0;//此时未选注 
				jspk10Page.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
		}
		
		
		
		lotteryCount = 0;
		if(numArrays.length == 0){ 
			jspk10Page.lotteryParam.currentLotteryCount = 0;//此时未选注 
			jspk10Page.lotteryParam.codes = null;  //号码置为空
			lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			return;
		} 
		for(var i = 0; i < numArrays.length; i++){
			var numArray = numArrays[i];
			if(numArray.length == 0){ //为空,表示号码选择未合法
				jspk10Page.lotteryParam.currentLotteryCount = 0;//此时未选注 
				jspk10Page.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
			//统计号码
			codes = codes + numArray + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT;
			//统计当前数目
			lotteryCount = lotteryCount + numArray.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length;
		}
		codes = codes.substring(0, codes.length -1);
	}else if(jspk10Page.lotteryParam.currentKindKey == 'LHD1VS10' 
			|| jspk10Page.lotteryParam.currentKindKey == 'LHD2VS9' 
			|| jspk10Page.lotteryParam.currentKindKey == 'LHD3VS8'
			|| jspk10Page.lotteryParam.currentKindKey == 'LHD4VS7'
			|| jspk10Page.lotteryParam.currentKindKey == 'LHD5VS6'
			|| jspk10Page.lotteryParam.currentKindKey == 'DXGJ'
			|| jspk10Page.lotteryParam.currentKindKey == 'DXYJ'
			|| jspk10Page.lotteryParam.currentKindKey == 'DXJJ'
			|| jspk10Page.lotteryParam.currentKindKey == 'DXDSM'
			|| jspk10Page.lotteryParam.currentKindKey == 'DXDWM'
			|| jspk10Page.lotteryParam.currentKindKey == 'DSGJ'
			|| jspk10Page.lotteryParam.currentKindKey == 'DSYJ'
			|| jspk10Page.lotteryParam.currentKindKey == 'DSJJ'
			|| jspk10Page.lotteryParam.currentKindKey == 'DSDSM'
			|| jspk10Page.lotteryParam.currentKindKey == 'DSDWM'){
		
		var kindKey = jspk10Page.lotteryParam.currentKindKey;
		
		
		 //获取 大小 单双 LHD 选择的号码
		$(".child-content .munbtn").each(function(i){
			var numsStr= "";
			var lis = $(this);
			for(var i = 0; i < lis.length; i++){
				var li = lis[i];
				var aNum = $(li);
				if($(aNum).hasClass('color-red')){
					numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT;
				}
			}
			//去除最后一个分隔符
			if(numsStr.substring(numsStr.length - 1, numsStr.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
				numsStr = numsStr.substring(0, numsStr.length -1);
			}
			//复式不能进行空串处理
			if(jspk10Page.lotteryParam.currentKindKey != 'QYZXFS'
				&& jspk10Page.lotteryParam.currentKindKey != 'QEZXFS'
					&& jspk10Page.lotteryParam.currentKindKey != 'QSZXFS'
					&& jspk10Page.lotteryParam.currentKindKey != 'QSIZXFS'
					&& jspk10Page.lotteryParam.currentKindKey != 'QWZXFS'){
				if(numsStr != ""){
					numArrays.push(numsStr);
				}
			}else{
				numArrays.push(numsStr);
			}
		});
		
		 
		if(numArrays.length == 1){ //总共只有1位数
		   var numArrays = numArrays[0].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
		   if((numArrays == null ) || (numArrays[0].length == 0 )){
				jspk10Page.lotteryParam.currentLotteryCount = 0;//此时未选注 
				jspk10Page.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
		   }else{
			   jspk10Page.lotteryParam.currentLotteryCount = 1; //大小单双 龙虎斗一次只能一注
			   lotteryCount = 1;
			   codes = numArrays[0];
			   
			   if(kindKey == "LHD1VS10" || kindKey == "LHD2VS9" || kindKey == "LHD3VS8" || kindKey == "LHD4VS7" || kindKey == "LHD5VS6"){
				   var codeDesc = codes;
				   codeDesc = codeDesc.replaceAll("1","龙");
				   codeDesc = codeDesc.replaceAll("2","虎");
				   jspk10Page.lotteryParam.codesStr = codeDesc;
				 }
				 if( kindKey == "DSGJ" || kindKey == "DSYJ" || kindKey == "DSJJ" || kindKey == "DSDSM" || kindKey == "DSDWM" 
					|| kindKey == "DXGJ" || kindKey == "DXYJ" || kindKey == "DXJJ" || kindKey == "DXDSM" || kindKey == "DXDWM"){
					 var codeDesc = codes;
					   codeDesc = codeDesc.replaceAll("1","大");
					   codeDesc = codeDesc.replaceAll("2","小");
					   codeDesc = codeDesc.replaceAll("3","单");
					   codeDesc = codeDesc.replaceAll("4","双");
					   jspk10Page.lotteryParam.codesStr = codeDesc;
				 }
		   }
		}else{
			jspk10Page.lotteryParam.currentLotteryCount = 0;//此时未选注 
			jspk10Page.lotteryParam.codes = null;  //号码置为空
			lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			return;
		}
	
	}else if(jspk10Page.lotteryParam.currentKindKey == 'CGJ'){
		jspk10Page.lotteryParam.currentLotteryCount = 0;//此时未选注 
		jspk10Page.lotteryParam.codes = null;  //号码置为空
		lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
	}else{
		  showTip("未知参数2.");	
		}
	}else{
		lotteryCount = 0;
		codes = null;
		showTip("玩法参数传递不合法.");
	}
	
	//存储当前统计的投注数目
	jspk10Page.lotteryParam.currentLotteryCount = lotteryCount;  
	//当前投注号码
	jspk10Page.lotteryParam.codes = codes;
	//统计当前投注数目和价格
	lotteryCommonPage.currentCodesStastic();
	
};

/**
 * 控制大小单双 龙虎斗选择的号码不能相同
 * @param ballDomElement 选择的元素
 */
Jspk10Page.prototype.controlNotSameBall = function(ballDomElement){
	//根据当前元素的name属性判别当前玩法
	if(jspk10Page.lotteryParam.currentKindKey == 'CGJ'|| jspk10Page.lotteryParam.currentKindKey == 'HZGYJ' || jspk10Page.lotteryParam.currentKindKey == 'DXGJ'
		|| jspk10Page.lotteryParam.currentKindKey == 'DSGJ' || jspk10Page.lotteryParam.currentKindKey == 'LHD1VS10'){
		var ballNumName = $(ballDomElement).attr("name");
		var dataRealVal = $(ballDomElement).attr("data-realvalue");
		//如果是单个的大小单双龙虎玩法，样式是有ball的样式，猜冠军是没有ball的样式的;
		var isBall = $(ballDomElement).hasClass("ball");
		if(!isBall) {
			//统一处理为猜冠军方法，在获取号码的时候再转换为对应的玩法
			jspk10Page.lotteryParam.currentKindKey = 'CGJ';
			if(ballNumName == "ballNum_0_match"){
				$(".munbtn[name='ballNum_1_match']").parent().removeClass("on");
				$(".munbtn[name='ballNum_2_match']").parent().removeClass("on");
				$(".munbtn[name='ballNum_3_match']").parent().removeClass("on");
				//对选号进行控制，大小号码不能相同
				$(".munbtn[name='ballNum_0_match']").each(function(i){
					if($(this).parent().hasClass('on')){
						var otherDataRealVal = $(this).attr("data-realvalue");
						//判断如果值不相等，则移除选中样式
						if(dataRealVal != otherDataRealVal) {
							$(this).parent().removeClass("on");
						}
					}
				});
			}else if(ballNumName == "ballNum_1_match"){
				$(".munbtn[name='ballNum_0_match']").parent().removeClass("on");
				$(".munbtn[name='ballNum_2_match']").parent().removeClass("on");
				$(".munbtn[name='ballNum_3_match']").parent().removeClass("on");
				//对选号进行控制，单双号码不能相同
				$(".munbtn[name='ballNum_1_match']").each(function(i){
					if($(this).parent().hasClass('on')){
						var otherDataRealVal = $(this).attr("data-realvalue");
						//判断如果值不相等，则移除选中样式
						if(dataRealVal != otherDataRealVal) {
							$(this).parent().removeClass("on");
						}
					}
				});
			}else if(ballNumName == "ballNum_2_match"){
				$(".munbtn[name='ballNum_0_match']").parent().removeClass("on");
				$(".munbtn[name='ballNum_1_match']").parent().removeClass("on");
				$(".munbtn[name='ballNum_3_match']").parent().removeClass("on");
			}else if(ballNumName == "ballNum_3_match"){
				$(".munbtn[name='ballNum_0_match']").parent().removeClass("on");
				$(".munbtn[name='ballNum_1_match']").parent().removeClass("on");
				$(".munbtn[name='ballNum_2_match']").parent().removeClass("on");
				//对选号进行控制，龙虎号码不能相同
				$(".munbtn[name='ballNum_3_match']").each(function(i){
					if($(this).parent().hasClass('on')){
						var otherDataRealVal = $(this).attr("data-realvalue");
						//判断如果值不相等，则移除选中样式
						if(dataRealVal != otherDataRealVal) {
							$(this).parent().removeClass("on");
						}
					}
				});
			}
			
		}
	}
	
	//龙虎斗不同号，单双不同号，大小不同号单选  需要做控制
	if(jspk10Page.lotteryParam.currentKindKey == 'LHD1VS10' || jspk10Page.lotteryParam.currentKindKey == 'LHD2VS9'
		|| jspk10Page.lotteryParam.currentKindKey == 'LHD3VS8' || jspk10Page.lotteryParam.currentKindKey == 'LHD4VS7'
		|| jspk10Page.lotteryParam.currentKindKey == 'LHD5VS6' || jspk10Page.lotteryParam.currentKindKey == 'DXGJ'
		|| jspk10Page.lotteryParam.currentKindKey == 'DXYJ' || jspk10Page.lotteryParam.currentKindKey == 'DXJJ'
		|| jspk10Page.lotteryParam.currentKindKey == 'DXDSM' || jspk10Page.lotteryParam.currentKindKey == 'DXDWM'
		|| jspk10Page.lotteryParam.currentKindKey == 'DSGJ' || jspk10Page.lotteryParam.currentKindKey == 'DSYJ' 
		|| jspk10Page.lotteryParam.currentKindKey == 'DSJJ' || jspk10Page.lotteryParam.currentKindKey == 'DSDSM'
		|| jspk10Page.lotteryParam.currentKindKey == 'DSDWM') {
		
		var dataRealVal = $(ballDomElement).attr("data-realvalue");
		var ballName = $(ballDomElement).attr("name");
		
		//对选号进行控制，胆码拖码不能相同
		$("[name='ballNum_0_match']").each(function(i){
			if($(this).hasClass('color-red')){
				var otherDataRealVal = $(this).attr("data-realvalue");
				//判断如果值相等，则移除选中样式
				if(dataRealVal != otherDataRealVal) {
					$(this).removeClass("color-red");
				}
			}
		});
	} /*else if(jspk10Page.lotteryParam.currentKindKey == 'QEZXFS' || jspk10Page.lotteryParam.currentKindKey == 'QSZXFS' 
		|| jspk10Page.lotteryParam.currentKindKey == 'QSIZXFS' || jspk10Page.lotteryParam.currentKindKey == 'QWZXFS' ) {
		var dataRealVal = $(ballDomElement).attr("data-realvalue");
		var ballName = $(ballDomElement).attr("name");
		//取得当前行数
		var ballIndex = ballName.substring(8, 9);
		var ballOtherIndexArray = new Array();
		if(jspk10Page.lotteryParam.currentKindKey == 'QEZXFS') {
			ballOtherIndexArray = new Array("0", "1");
		} else if(jspk10Page.lotteryParam.currentKindKey == 'QSZXFS') {
			ballOtherIndexArray = new Array("0", "1", "2");
		} else if(jspk10Page.lotteryParam.currentKindKey == 'QSIZXFS') {
			ballOtherIndexArray = new Array("0", "1", "2", "3");
		} else if(jspk10Page.lotteryParam.currentKindKey == 'QWZXFS') {
			ballOtherIndexArray = new Array("0", "1", "2", "3", "4");
		}  
		for(var i = 0; i < ballOtherIndexArray.length; i++) {
			var ballOtherIndex = ballOtherIndexArray[i];
			//如果是当前行，则不处理
			if(ballIndex == ballOtherIndex) {
				continue;
			}
			//对选号进行控制，胆码拖码不能相同
			$("button[name=ballNum_"+ballOtherIndex+"_match]").each(function(i){
				if($(this).hasClass('ball-number-current')){
					var otherDataRealVal = $(this).attr("data-realvalue");
					//判断如果值相等，则移除选中样式
					if(dataRealVal == otherDataRealVal) {
						$(this).removeClass("ball-number-current");
					}
				}
			});
		}
	}*/
};



/**
 * 号码拼接
 * @param num
 * 
 */
Jspk10Page.prototype.codeStastics = function(codeStastic,index){
	
	 var codeStr = "";
	 var codeDesc = codeStastic[5]; 
	 codeDesc = codeDesc.toString();
	 var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
	 if(kindKey == "LHD1VS10" || kindKey == "LHD2VS9" || kindKey == "LHD3VS8" || kindKey == "LHD4VS7" || kindKey == "LHD5VS6"){
		 codeDesc = codeDesc.replaceAll("1","龙");
		 codeDesc = codeDesc.replaceAll("2","虎");
	 }
	 if( kindKey == "DSGJ" || kindKey == "DSYJ" || kindKey == "DSJJ" || kindKey == "DSDSM" || kindKey == "DSDWM" 
		|| kindKey == "DXGJ" || kindKey == "DXYJ" || kindKey == "DXJJ" || kindKey == "DXDSM" || kindKey == "DXDWM"){
		 codeDesc = codeDesc.replaceAll("1","大");
		 codeDesc = codeDesc.replaceAll("2","小");
		 codeDesc = codeDesc.replaceAll("3","单");
		 codeDesc = codeDesc.replaceAll("4","双");
		
	 }
	 if(codeDesc.length < 15){
		 codeStr += "<span>"+codeDesc.replace(/\&/g," ")+"</span>";
	 }else{
		 codeStr +=  "<span>" + codeDesc.substring(0,9).replace(/\&/g," ") + "...</span>";
		 codeStr += "<a onclick='event.stopPropagation();jspk10Page.showDsMsgDialog(this,"+index+")'>详情</a>";
		 codeStr += "</span>";
	 }
	 var str = "";
	 //单式不需要更新操作
    var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
	 if(kindKey == 'QEZXDS' || kindKey == 'QSZXDS' || kindKey == 'QSIZXDS' || kindKey == 'QWZXDS'){
        str += "<li>";
	 }else{
		 str += "<li onclick='lotteryCommonPage.updateCodeStastic(this,"+index+")'>";
	 }
	 str += " <div class='li-child li-title'>"+codeStastic[4]+codeStr+"</div>";
	 str += " <div class='li-child li-money'>¥"+codeStastic[0]+"元</div>";
	 str += " <div class='li-child li-strip'>"+codeStastic[1]+"注</div>";
	 str += " <div class='li-child li-multiple'>"+codeStastic[2]+"倍</div>";
	 str += " <div class='li-child li-addmoney'>¥"+codeStastic[3]+"元</div>";
	 str += "<div class='li-child li-close' data-value='"+index+"' name='deleteCurrentCodeStastic'><img src='"+ contextPath +"/front/images/lottery/close.png' /></div>";
	 str += "</li>";


/*	 if(codeDesc.length < 15){
		 str += "<span class='betting-content' style='position:relative'>"+codeDesc.replace(/\&/g," ")+"</span>";
	 }else{
		 str +=  "<span class='betting-content' style='position:relative'>";
		 str +=  codeDesc.substring(0,15).replace(/\&/g," ") + "...";
		 str += "<span  class='lottery-details' onclick='event.stopPropagation();jspk10Page.showDsMsgDialog(this,"+index+")' style='color:#FFF;text-decoration:underline;'>详情</span>";
//		 str += "<span  class='lottery-details' onclick='event.stopPropagation();$(\".lottery-details-area\").hide();$(this).next().show()' style='color:#FFF;text-decoration:underline;'>详情</span>";
//		 str += "<div class='lottery-details-area' style='left: 155px; display: none;'>";
//		 str += "  <div class='num'>";
//		 str += "    <span class='multiple'>共"+codeStastic[1]+" 注</span>";
//		 str += "    <em class='close'>×</em>";
//		 str += "  </div>";
//		 str += "  <div class='list'>"+codeDesc+"</div>";
//		 str += "</div>";
		 str += "</span>";
	 }*/
	 str += "</li>";
	 return str;
};

/**
 * 更新投注号码
 */
Jspk10Page.prototype.showDsMsgDialog = function(obj,index){
	var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[index];
	var kindKeyTop = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
	
	$("#kindPlayDes").text(codeStastic[4]);

	
	var modelTxt = $("#modeTxt").text();
	if(lotteryCommonPage.lotteryParam.awardModel == 'YUAN'){
		$("#kindPlayModel").text("元模式，" + modelTxt);
	}else if(lotteryCommonPage.lotteryParam.awardModel == 'JIAO'){
		$("#kindPlayModel").text("角模式，" + modelTxt);
	}else if(lotteryCommonPage.lotteryParam.awardModel == 'FEN'){
		$("#kindPlayModel").text("分模式，" + modelTxt);
	}else{
      showTip("该投注模式不合法.");
	}

	$("#kindPlayPriceMsg").text(codeStastic[1] + "注，" + lotteryCommonPage.lotteryParam.beishu + "倍, 共计" + codeStastic[0] + "元");
	$("#kindPlayCodeDes").val(codeStastic[5].replace(/\&/g," "));
	 
	$("#shadeFloor").show();
	$("#dsContentShowDialog").show();  //展示单式投注号码对话框
};

/**
 * 获取当前开奖号码的组态显示 组三组六
 */
Jspk10Page.prototype.getLotteryCodeStatus = function(codeStr){
	//控制显示当前号码的形态
    var codeArray = codeStr.split(",");
    var code1 = codeArray[0];
    var code2 = codeArray[1];
    var code3 = codeArray[2];
    var code4 = codeArray[3];
    var code5 = codeArray[4];
    
    //后三形态
    var codeStatus = "";
    if((code3 == code4 && code3 != code5) || (code3 == code5 && code4 != code5) || (code4 == code5 && code3 != code4)){
    	codeStatus = "组三";
    }else if((code3 != code4) && (code3 != code5) && (code4 != code5)){
    	codeStatus = "组六";
    }else if(code3 == code4 && code4 == code5){
    	codeStatus = "豹子";
    }    
    
    return codeStatus;
        
};

/**
 * 显示当前投注号码
 * @param codeStastic
 * @param index
 */
Jspk10Page.prototype.showCurrentPlayKindCode = function(codesArray) {
	for ( var i = 0; i < codesArray.length; i++) {
		if (codesArray[i].indexOf(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT) == -1) {
			$(".l-mun div[name='ballNum_"+i+"_match']").each(function() {
						if ($(this).attr("data-realvalue") == codesArray[i]) {
							$(this).toggleClass("on");
						}
					});
		} else {
			var codesWeiArray = codesArray[i].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
			for ( var j = 0; j < codesWeiArray.length; j++) {
				var codeWei = codesWeiArray[j];
				if (codeWei != lotteryCommonPage.lotteryParam.CODE_REPLACE) {
					$(".l-mun div[name='ballNum_"+i+"_match']").each(function() {
						if ($(this).attr("data-realvalue") == codeWei) {
							$(this).toggleClass("on");
						}
					});
				}
			}
		}
	}
};

/**
 * 机选
 */
Jspk10Page.prototype.JxCodes = function(num){
	var kindKey = jspk10Page.lotteryParam.currentKindKey;
	var beishu = lotteryCommonPage.lotteryParam.beishu;
	var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
	
	var jxmax;
	var jxmin;
	var codes;
	
	//大小单双
	var dxds_code_array = new Array(1,2,3,4);
	
	//冠亚和值
	if (kindKey == "HZGYJ"){
		jxmin = 3;
		jxmax = 19;
	}
	
	//大小 龙虎斗
	else if (kindKey == "LHD1VS10" || kindKey == "LHD2VS9" || kindKey == "LHD3VS8" 
		   || kindKey == "LHD4VS7" || kindKey == "LHD5VS6" || kindKey == "DXGJ" 
		   || kindKey == "DXYJ"  || kindKey == "DXJJ" || kindKey == "DXDSM" || kindKey == "DXDWM"){
		jxmin = 1;
		jxmax = 2;
	//单双
	}else if(kindKey == "DSGJ" || kindKey == "DSYJ" || kindKey == "DSJJ" || kindKey == "DSDSM" 
		 || kindKey == "DSDWM"){
		jxmin = 3;
		jxmax = 4;
	}
	else{
		jxmin = 1;
		jxmax = 10;
	}
	
	for(var i = 0;i < num; i++){

		   //
		   if(kindKey == 'QYZXFS'){
				var code1 = GetRndNum(jxmin,jxmax);
				    codes =  jspk10Page.getRndCodes(code1);
		   }else if(kindKey == 'QEZXFS' || kindKey == 'QEZXDS'){
			   var codeArray =  GetRndNumByAppoint(jxmin,jxmax,2,0);
					   codes = jspk10Page.getRndCodes(codeArray[0]) + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
					   		   jspk10Page.getRndCodes(codeArray[1]);
		   }else if(kindKey == 'QSZXFS' || kindKey == 'QSZXDS'){
			   var codeArray =  GetRndNumByAppoint(jxmin,jxmax,3,0);
					   codes = jspk10Page.getRndCodes(codeArray[0]) + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
					   		   jspk10Page.getRndCodes(codeArray[1]) + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
					   		   jspk10Page.getRndCodes(codeArray[2]);
		   }else if(kindKey == 'QSIZXFS' || kindKey == 'QSIZXDS'){
			   var codeArray =  GetRndNumByAppoint(jxmin,jxmax,4,0);
					   codes = jspk10Page.getRndCodes(codeArray[0]) + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			   		   		   jspk10Page.getRndCodes(codeArray[1]) + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			   		   		   jspk10Page.getRndCodes(codeArray[2]) + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			   		           jspk10Page.getRndCodes(codeArray[3]);
		   }else if(kindKey == 'QWZXFS' || kindKey == 'QWZXDS'){
			   var codeArray =  GetRndNumByAppoint(jxmin,jxmax,5,0);
					   codes =  jspk10Page.getRndCodes(codeArray[0]) + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				   		   		jspk10Page.getRndCodes(codeArray[1]) + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
					   		    jspk10Page.getRndCodes(codeArray[2]) + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
					   		    jspk10Page.getRndCodes(codeArray[3]) + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
					            jspk10Page.getRndCodes(codeArray[4]);
		   }else if(kindKey == 'DWD'){
			   var codeArray =  GetRndNumByAppoint(jxmin,jxmax,2,1);
			           codes = jspk10Page.getRndCodes(codeArray[0]) + 
					           lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +  "-"+
					           lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +  "-"+
					           lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +  "-"+
					           lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +  "-"+
					           lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +  "-"+
					           lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +  "-"+
					           lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +  "-"+
					           lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +  "-"+
					           lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +  "-";
					           
		   }else if(kindKey == "LHD1VS10" || kindKey == "LHD2VS9" || kindKey == "LHD3VS8" || 
				    kindKey == "LHD4VS7" || kindKey == "LHD5VS6" || kindKey == "DSGJ" || kindKey == "DSYJ" || 
				    kindKey == "DSJJ" || kindKey == "DSDSM" || kindKey == "DSDWM" || kindKey == "DXGJ" || 
				    kindKey == "DXYJ" || kindKey == "DXJJ" || kindKey == "DXDSM" || kindKey == "DXDWM"){
			   var code1 = GetRndNum(jxmin,jxmax);
				   codes = code1;
		   }else if(kindKey == 'HZGYJ'){
				var code1 = GetRndNum(jxmin,jxmax);
				    codes = code1;
		   }
		   
		   
			//先校验投注蓝中,是否有该投注号码
			var isYetHave = false;
			var yetIndex = null;
			var codeStastics = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic;
			for(var j = codeStastics.length - 1; j >= 0 ; j--){
				 var codeStastic = codeStastics[j];
			     var kindKeyTemp = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
				 if(kindKeyTemp == jspk10Page.lotteryParam.currentKindKey && codeStastic[5].toString() == codes){
					 isYetHave = true;
					 yetIndex = j;
					 break;
				 }
			}
			
	 			var currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get(jspk10Page.param.kindNameType +"_"+jspk10Page.lotteryParam.currentKindKey);
			//冠亚和值奖金特殊处理
			if(kindKey == 'HZGYJ'){
					currentLotteryWin = jspk10Page.getCurrentLotteryWin(jspk10Page.lotteryParam.currentKindKey, codes);
			}
			if(isYetHave && yetIndex != null){
				showTip("您选择的号码在号码篮已存在，将直接进行倍数累加!");
				var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[yetIndex];
				jspk10Page.lotteryParam.currentLotteryPrice = lotteryCommonPage.getAwardByLotteryModel(1 * lotteryCommonPage.currentLotteryKindInstance.param.unitPrice * beishu);
				codeStastic[0] = (parseFloat(codeStastic[0]) + parseFloat(jspk10Page.lotteryParam.currentLotteryPrice)).toFixed(lotteryCommonPage.param.toFixedValue);
				codeStastic[1] = codeStastic[1];
				codeStastic[2] = parseInt(codeStastic[2]) + lotteryCommonPage.lotteryParam.beishu;
				codeStastic[3] = (parseFloat(codeStastic[3]) + parseFloat(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu))).toFixed(lotteryCommonPage.param.toFixedValue);
				codeStastic[4] = lotteryCommonPage.lottertyDataDeal.lotteryKindMap.get(jspk10Page.lotteryParam.currentKindKey);
				codeStastic[5] = codes;
				codeStastic[6] = jspk10Page.param.kindNameType +"_"+jspk10Page.lotteryParam.currentKindKey;
				codeStastic[7] = codeStastic[7];
				
				lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStastic[7]); //先清除原来的投注号码的倍数
				lotteryCommonPage.lotteryParam.lotteryMultipleMap.put(codeStastic[7],codeStastic[2]);
			}else{
			    jspk10Page.lotteryParam.currentLotteryTotalCount++;
				var lotteryKindMap =  new JS_OBJECT_MAP();
				//将所选号码和玩法的映射放置map中
			    if(kindKey == 'RXSIZXDS' || kindKey == 'RXSZXDS' || kindKey == 'RXEZXDS'){
					var positionCode = "";
					$("input[name='ds_position']").each(function(){
						if(this.checked){
							positionCode += "1,";
						}else{
							positionCode += "0,";
						}
					});
					positionCode = positionCode.substring(0, positionCode.length - 1);
					lotteryKindMap.put(jspk10Page.lotteryParam.currentKindKey,codes + "_" + positionCode);
			    }else{
					lotteryKindMap.put(jspk10Page.lotteryParam.currentKindKey,codes);
			    }
				lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_"+jspk10Page.lotteryParam.currentLotteryTotalCount,lotteryKindMap);
				//投注倍数存储
				lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_"+jspk10Page.lotteryParam.currentLotteryTotalCount,beishu);

				//进入投注池当中,五星默认是一注
				var codeRecord = new Array();
			    var lotteryCount = 0;
			    kindKey = jspk10Page.lotteryParam.currentKindKey;
			
			    lotteryCount = 1;
				codeRecord.push(lotteryCommonPage.getAwardByLotteryModel(lotteryCount * lotteryCommonPage.currentLotteryKindInstance.param.unitPrice * beishu));
				codeRecord.push(lotteryCount);
				codeRecord.push(beishu);
				codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * beishu));
				codeRecord.push(lotteryCommonPage.lottertyDataDeal.lotteryKindMap.get(jspk10Page.lotteryParam.currentKindKey));
				codeRecord.push(codes);
				codeRecord.push(jspk10Page.param.kindNameType +"_"+jspk10Page.lotteryParam.currentKindKey); //标识玩法
				codeRecord.push("lottery_id_"+jspk10Page.lotteryParam.currentLotteryTotalCount); //标识该条记录的lotteryMap
				jspk10Page.lotteryParam.codesStastic.push(codeRecord); 
				codes = "";
			}
	};
	
	lotteryCommonPage.codeStastics();  //显示随机投注结果
};

/**
 * 单式投注数目控制
 * @param eventType
 */
Jspk10Page.prototype.lotteryCountStasticForDs = function(contentLength){
	jspk10Page.lotteryParam.currentLotteryCount = contentLength;				
};


/**
 * 北京PK10彩种格式化
 */
Jspk10Page.prototype.formatNum = function(isCut){
	var kindKey = jspk10Page.lotteryParam.currentKindKey;
	var pastecontent = $("#lr_editor").val();
	
	pastecontent=pastecontent.replace(/[^\d]/g,'');
	
	if (pastecontent == ""){
		$("#lr_editor").val("");
		//设置当前投注数目或者投注价格
		/*$('#J-balls-statistics-lotteryNum').text("0");  //显示投注
		$('#J-balls-statistics-amount').text(parseFloat("0").toFixed(lotteryCommonPage.param.toFixedValue));
		$('#J-balls-statistics-code').text("(投注内容)");*/
		return false;
	}
	var len= pastecontent.length;
	var num="";
	var numtxt="";
	var n=0;
	var maxnum=1;
	if(kindKey == 'QEZXDS'){
		maxnum=4;
	}else if(kindKey == 'QSZXDS'){
		maxnum=6;
	}
	else if(kindKey == 'QSIZXDS'){  
		maxnum=8;
	}
	else if(kindKey == 'QWZXDS'){  
		maxnum=10;
	}else{
		showTip("未配置该单式的控制.");
	}
	
	//下方显示的号码
	var numsStr = "";
	for(var i=0; i<len; i = i + 2){
		if(i%maxnum==0){
			n=1;
		}
		else{
			n = n + 2;
		}
		var currentChar = pastecontent.substr(i,2);
		//截取处理的
		if(isCut) {
			if(n<maxnum-1){
				num = num + currentChar+",";
				numsStr = numsStr + currentChar+",";
			}
			else{
				num = num + currentChar;
				numtxt = numtxt + num + "\n";
				num = "";
				numsStr = numsStr + currentChar + " ";
			}
		} else {
			//不需要截取多余处理的
			if(n<maxnum-1){
				numtxt = numtxt + currentChar+",";
				numsStr = numsStr + currentChar+",";
			}
			else{
				numtxt = numtxt + currentChar + "\n";
				numsStr = numsStr + currentChar + " ";
			}
		}
	}
	numsStr = numsStr.substring(0, numsStr.length - 1);
	//如果格式化截取多余的  内容为空，显示号码也为空
	if(isCut) {
		if(numtxt == "" || numtxt.length == 0) {
			numsStr = "";
		}
	} else {
		numtxt = numtxt.substring(0, numtxt.length - 1);
	}
	//处理投注下方显示的号码
	jspk10Page.lotteryParam.codesStr = numsStr;
	$("#lr_editor").val(numtxt);
	return true;
};
/**
 * 单式的选号投注
 */
Jspk10Page.prototype.singleLotteryNum = function(){
	
	var kindKey = jspk10Page.lotteryParam.currentKindKey;
	
	//如果是五星或者四星的单式录入
	if(kindKey == 'QEZXDS' || kindKey == 'QSZXDS' 
		|| kindKey == 'QSIZXDS' || kindKey == 'QWZXDS'){
		var editorVal = $("#lr_editor").val();
		if(editorVal.indexOf("说明") != -1){
			showTip("请先填写投注号码");
			return false;
		}
		lotteryCommonPage.dealForDsFormat();  //首先格式化,避免直接点击选号码的事件
		var pastecontent = $("#lr_editor").val();
		if(pastecontent == ""){
			showTip("请先填写投注号码");
			return false;
		}
		pastecontent = pastecontent.replace(/\r\n/g,"$");
		pastecontent = pastecontent.replace(/\n/g,"$");
		pastecontent = pastecontent.replace(/\s/g,"");
		pastecontent = pastecontent.replace(/(\$)\1+/g, '$1');
		if(pastecontent.substr(pastecontent.length-1,1)=="$"){
			pastecontent = pastecontent.substr(0,pastecontent.length-1);
		}
		
		pastecontent = pastecontent.replace(/\$/g,"&");
		var pastecontentArr = pastecontent;
		var pastecontentLength = pastecontent.split("&").length;
		/*if(kindKey == 'WXDS'){
			if(jspk10Page.lotteryParam.currentLotteryCount > 80000){  //100000
				showTip("当前玩法最多支持80000注单式内容，请调整！");
		        return;
			}
		}else{
			showTip("未配置");
		}*/
		
		var errzhushu = "";
		var errzhushumsg = "[格式错误]"
		var reg1,reg2;
		if(kindKey == 'QEZXDS'){  //前二单式   
			reg1 = /^((?:(?:[0][1-9]|1[0]),){1}(?:[0][1-9]|1[0])|1[0]|[&]){1,}$/;
			/*reg1 = /^((?:(?:[1-9]|[10]),){1}(?:[1-9]|[10])|[&]){1,}$/;*/
		}else if(kindKey == 'QSZXDS'){ //前三直选单式
			reg1 = /^((?:(?:[0][1-9]|1[0]),){2}(?:[0][1-9]|1[0])|1[0]|[&]){1,}$/;
			/*reg1 = /^((?:(?:[1-9]|[10]),){2}(?:[1-9]|[10])|[&]){1,}$/;*/
		}else if(kindKey == 'QSIZXDS'){ //前四直选单式
			reg1 = /^((?:(?:[0][1-9]|1[0]),){3}(?:[0][1-9]|1[0])|1[0]|[&]){1,}$/;
			/*reg1 = /^((?:(?:[1-9]|[10]),){3}(?:[1-9]|[10])|[&]){1,}$/;*/
		}else if(kindKey == 'QWZXDS'){ //前五直选单式
			reg1 = /^((?:(?:[0][1-9]|1[0]),){4}(?:[0][1-9]|1[0])|1[0]|[&]){1,}$/;
			/*reg1 = /^((?:(?:[1-9]|[10]),){4}(?:[1-9]|[10])|[&]){1,}$/;*/
		}else{
			showTip("未配置该类型的单式");
		}
		
		//号码校验
		if(!reg1.test(pastecontentArr)){
			showTip("单式号码格式不合法.");
			return false;
		}
		
		//位数选取判断
		var positionCode = "";
		$("input[name='ds_position']").each(function(){
			if(this.checked){
				positionCode += "1,";
			}else{
				positionCode += "0,";
			}
		});
		positionCode = positionCode.substring(0, positionCode.length - 1);
		jspk10Page.lotteryParam.codes = pastecontentArr;
		
		var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
		var currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get(jspk10Page.param.kindNameType +"_"+jspk10Page.lotteryParam.currentKindKey);
		lotteryCommonPage.lotteryParam.beishu = parseInt(lotteryCommonPage.lotteryParam.beishu);
		jspk10Page.lotteryParam.currentLotteryTotalCount++;
		
		//将所选号码和玩法的映射放置map中
		var lotteryKindMap =  new JS_OBJECT_MAP();
		if(kindKey == 'RXSIZXDS' || kindKey == 'RXEZXDS' || kindKey == 'RXSZXDS'){
			lotteryKindMap.put(jspk10Page.lotteryParam.currentKindKey,jspk10Page.lotteryParam.codes + "_" + positionCode);
		}else{
			lotteryKindMap.put(jspk10Page.lotteryParam.currentKindKey,jspk10Page.lotteryParam.codes);
		}
		lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_"+jspk10Page.lotteryParam.currentLotteryTotalCount,lotteryKindMap);
		//投注倍数存储
		lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_"+jspk10Page.lotteryParam.currentLotteryTotalCount,lotteryCommonPage.lotteryParam.beishu);
		
		//进入投注池当中
		var codeRecord = new Array();
		codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((jspk10Page.lotteryParam.currentLotteryCount * jspk10Page.param.unitPrice * lotteryCommonPage.lotteryParam.beishu).toFixed(lotteryCommonPage.param.toFixedValue)));
		codeRecord.push(jspk10Page.lotteryParam.currentLotteryCount);
		codeRecord.push(lotteryCommonPage.lotteryParam.beishu);
		codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu));
		codeRecord.push(lotteryCommonPage.lottertyDataDeal.lotteryKindMap.get(jspk10Page.lotteryParam.currentKindKey));
		codeRecord.push(jspk10Page.lotteryParam.codes);
		codeRecord.push(jspk10Page.param.kindNameType +"_"+jspk10Page.lotteryParam.currentKindKey); //标识玩法
		codeRecord.push("lottery_id_"+jspk10Page.lotteryParam.currentLotteryTotalCount); 
		jspk10Page.lotteryParam.codesStastic.push(codeRecord);	
		
		//号码显示统计,由当前的倍数、奖金模式、投注模式决定
		lotteryCommonPage.codeStastics();
		//选好号码的结束事件
		//lotteryCommonPage.addCodeEnd(); 
		//$("#lr_editor").val("");//清空注码
	}
	
	return true;
};

/**
 * 冠亚和值投注数
 */
Jspk10Page.prototype.getLotteryCountBycodes = function(codes) {
	if(codes.length < 1){
		return;
	}
	var lotteryCount = 0;
	var codesArray = codes.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
	for( var i = 0; i < codesArray.length; i++){
		var numArray = codesArray[i];
		if (numArray == 3 || numArray == 4) {
			lotteryCount += 2;
		}else if (numArray == 5  || numArray == 6){
			lotteryCount += 4;
		}else if (numArray == 7  || numArray == 8){
			lotteryCount += 6;
		}else if (numArray == 9  || numArray == 10){
			lotteryCount += 8;
		}else if (numArray == 12 || numArray == 13){
			lotteryCount += 8;
		}else if (numArray == 14 || numArray == 15){
			lotteryCount += 6;
		}else if (numArray == 16 || numArray == 17){
			lotteryCount += 4;
		}else if (numArray == 18 || numArray == 19){
			lotteryCount += 2;
		}else if (numArray == 11) {
			lotteryCount += 10;
		}
	}
	return lotteryCount;
};

function in_array(search,array){
    for(var i in array){
        if(array[i]==search){
            return true;
        }
    }
    return false;
}

/**
 * 前二直选复式注数
 */
Jspk10Page.prototype.getLotteryCountByQE = function(a, b){
	if ($.isArray(a) && $.isArray(b)) {
		var count = 0;
		//存储选中的号码
		var selNumArrays = new Array();
		for(var i = 0; i < a.length; i++) {
			selNumArrays.push(a[i]);
			for(var j = 0; j < b.length; j++) {
				if(in_array(b[j], selNumArrays)) {
					continue;
				} else {
					count++;
				}
			}
			selNumArrays.pop();
		}
		return count;
	}
	return 0;
};

/**
 * 前三直选复式注数
 */
Jspk10Page.prototype.getLotteryCountByQS = function(a, b, c){
	if ($.isArray(a) && $.isArray(b) && $.isArray(c)) {
		var count = 0;
		//存储选中的号码
		var selNumArrays = new Array();
		for(var i = 0; i < a.length; i++) {
			selNumArrays.push(a[i]);
			for(var j = 0; j < b.length; j++) {
				if(in_array(b[j], selNumArrays)) {
					continue;
				} else {
					selNumArrays.push(b[j]);
					for(var k = 0; k < c.length; k++) {
						if(in_array(c[k], selNumArrays)) {
							continue;
						} else {
							count++;
						}
					}
					selNumArrays.pop();
				}
			}
			selNumArrays = new Array();
		}
		return count;
	}
	return 0;
};

/**
 * 前四直选复式注数
 */
Jspk10Page.prototype.getLotteryCountByQSI = function(a, b, c, d){
	if ($.isArray(a) && $.isArray(b) && $.isArray(c) && $.isArray(d)) {
		var count = 0;
		//存储选中的号码
		var selNumArrays = new Array();
		for(var i = 0; i < a.length; i++) {
			selNumArrays.push(a[i]);
			for(var j = 0; j < b.length; j++) {
				if(in_array(b[j], selNumArrays)) {
					continue;
				} else {
					selNumArrays.push(b[j]);
					for(var k = 0; k < c.length; k++) {
						if(in_array(c[k], selNumArrays)) {
							continue;
						} else {
							selNumArrays.push(c[k]);
							for(var l = 0; l < d.length; l++) {
								if(in_array(d[l], selNumArrays)) {
									continue;
								} else {
									count++;
								}
							}
							selNumArrays.pop();
						}
					}
					selNumArrays.pop();
				}
			}
			selNumArrays = new Array();
		}
		return count;
	}
	return 0;
};

/**
 * 前五直选复式注数
 */
Jspk10Page.prototype.getLotteryCountByQW = function(a, b, c, d, e){
	if ($.isArray(a) && $.isArray(b) && $.isArray(c) && $.isArray(d) && $.isArray(e)) {
		var count = 0;
		//存储选中的号码
		var selNumArrays = new Array();
		for(var i = 0; i < a.length; i++) {
			selNumArrays.push(a[i]);
			for(var j = 0; j < b.length; j++) {
				if(in_array(b[j], selNumArrays)) {
					continue;
				} else {
					selNumArrays.push(b[j]);
					for(var k = 0; k < c.length; k++) {
						if(in_array(c[k], selNumArrays)) {
							continue;
						} else {
							selNumArrays.push(c[k]);
							for(var l = 0; l < d.length; l++) {
								if(in_array(d[l], selNumArrays)) {
									continue;
								} else {
									selNumArrays.push(d[l]);
									for(var m = 0; m < e.length; m++) {
										if(in_array(e[m], selNumArrays)) {
											continue;
										} else {
											count++;
										}
									}
									selNumArrays.pop();
								}
							}
							selNumArrays.pop();
						}
					}
					selNumArrays.pop();
				}
			}
			selNumArrays = new Array();
		}
		return count;
	}
	return 0;
};

/**
 * 初始化彩种信息
 */
Jspk10Page.prototype.initLottery = function(){
	//先加载所有的玩法
    lotteryCommonPage.lottertyDataDeal.loadLotteryKindPlay();
    
    //取消追号处理
    lotteryCommonPage.lotteryParam.isZhuiCode = false;
    
    //设置玩法描述数据
    lotteryCommonPage.lottertyDataDeal.setKindPlayDes();
    
    //选择默认的玩法，默认是猜一个
    jspk10Page.lotteryParam.currentKindKey = "CGJ";
	lotteryCommonPage.lottertyDataDeal.lotteryKindPlayChoose(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey);

	//动画效果
	animate_add(".main-head","fadeInDown",0);
	animate_add(".lottery-content","fadeInUp",0);
	animate_add(".lottery-bar .bar","fadeInUp",0);
	
    //添加星种选中事件
	$("#lotteryStarKindList .child").unbind("click").click(function(){
		var roleId = $(this).attr("data-role-id");
		//如果有包含玩法属性
		if(isNotEmpty(roleId) && lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey != roleId) {
			//前一直选复式 ，定位胆没有下拉子选项
			if(roleId == "QYZXFS" || roleId == "DWD"){
				 $("#lotteryStarKindList .child").removeClass("color-red");
				 $(this).addClass("color-red");
				 //隐藏下拉玩法列表
				 element_toggle(false,['.lottery-classify']);
			}
			//清空当前选中号码
			lotteryCommonPage.clearCurrentCodes();
			lotteryCommonPage.lottertyDataDeal.lotteryKindPlayChoose(roleId);
			
			
		//加载星种玩法下拉
		} else {
			var kindPlayClass = $(this).attr("data-id");
			var parent=$(this).closest("body");
			parent.find(".lottery-classify2 .move-content .child").removeClass('color-red');
			$(this).addClass('color-red');
			parent.find(".lottery-classify2 .clist").removeClass('in').addClass('out');
			parent.find("."+kindPlayClass).addClass('in').removeClass('out');
		}
	});
    //添加玩法选中事件
	$("#lotteryKindPlayList .line-content .child").unbind("click").click(function(){
		var roleId = $(this).attr("data-role-id");
		 $("#lotteryKindPlayList .container .line-content").children().addClass("no");
		 $(this).removeClass("no");
		//已经是当前玩法了，不需要处理
		if(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey == roleId) {
			return;
		}
		
	   //清空当前选中号码
	   lotteryCommonPage.clearCurrentCodes();
	   
	   //控制玩法的号码展示
	   lotteryCommonPage.lottertyDataDeal.lotteryKindPlayChoose(roleId);
	   
	   //隐藏下拉玩法列表
	   element_toggle(false,['.lottery-classify']);
	});
	
	//点击当前玩法显示下拉其他玩法
	$("#currentKindDiv").unbind("click").click(function(){
		var e = $(this);
		var ele = ".lottery-classify2";
		var parent=$(this).closest("body");
		var display=$(ele).css("display");
		var style="boolean";
		style=display=="none"?"true":"boolean";
		parent.find(".lottery-classify").not(ele).hide();
		element_toggle(style,[ele],parent);
		
		//触发星种下拉
		$("#lotteryStarKindList .child").each(function(){
			if($(this).hasClass('color-red')) {
				var kindPlayClass = $(this).attr("data-id");
				var parent=$(this).closest("body");
				parent.find(".lottery-classify2 .clist").removeClass('in').addClass('out');
				parent.find("."+kindPlayClass).addClass('in').removeClass('out');
				return false;
			}
		});
	});
	
	//绑定选择其他地区彩种事件
	$("#otherLotteryKind").unbind("click").click(function(){
		var ele = '.lottery-classify1';
		var parent=$(this).closest("body");
		var display=$(ele).css("display");
		var style="boolean";
		style=display=="none"?"true":"boolean";
		parent.find(".lottery-classify").not(ele).hide();
		element_toggle(style,[ele],parent);
	});
	
	//点击开奖号码显示开奖记录事件
	$(".lottery_run").unbind("click").click(function(){
		var parent=$(this).closest("body");
		if(parent.find(".lottery-dynamic").hasClass('in')){
			parent.find(".lottery-dynamic").removeClass('in').addClass('out');
		}else{
			parent.find(".lottery-dynamic").removeClass('out').addClass('in');
		}
	});
	
	
	//倍数改变事件
	lotteryCommonPage.beishuEvent();
	
	//倍数减少事件
	$("#beishuSub").unbind("click").click(function(){
		var beishuStr = $('#beishu').val();
		var beishuTemp = parseInt(beishuStr);
		beishuTemp--;
		if(beishuTemp <= 0) {
			showTip("当前倍数已经是最低了");
			return;
		}
		$("#beishu").val(beishuTemp);
		lotteryCommonPage.beishuEventContent();
	});
	
	//倍数增加事件
	$("#beishuAdd").unbind("click").click(function(){
		var beishuStr = $('#beishu').val();
		var beishuTemp = parseInt(beishuStr);
		beishuTemp++;
		if(beishuTemp > 1000000) {
			showTip("倍数最高1000000");
			return;
		}
		$("#beishu").val(beishuTemp);
		lotteryCommonPage.beishuEventContent();
	});
	

	//确认投注-展示投注信息事件
	$("#showLotteryMsgButton").unbind("click").click(function(){
		var editorVal = $("#lr_editor").val();
		if(typeof(editorVal) != "undefined" ){
			lotteryCommonPage.dealForDsFormat(true);
		}
		//非追号类型 处理添加号码到投注池
		if(!lotteryCommonPage.lotteryParam.isZhuiCode) {
			lotteryCommonPage.currentLotteryKindInstance.userLotteryNumForAddOrder();
		}
		lotteryCommonPage.showLotteryMsg();
	});
	//关闭投注信息事件
    $(" #lotteryOrderDialogCancelButton").unbind("click").click(function(){
    	lotteryCommonPage.hideLotteryMsg();
	});
    
    //关闭投注信息事件
    $(".close").unbind("click").click(function(){
    	lotteryCommonPage.hideLotteryMsg();
    });
    
	//确认投注-提交后台投注事件
	$("#lotteryOrderDialogSureButton").unbind("click").click(function(){
		lotteryCommonPage.userLottery();
	});
	
	//机选一注事件
	$("#randomOneBtn").unbind("click").click(function(){
		lotteryCommonPage.randomOneEvent();
	});
	
	//清空投注池号码
	$("#clearAllCodeBtn").unbind("click").click(function(){
		lotteryCommonPage.clearCodeStasticEvent();
		/*comfirm("提示","确认删除号码篮内全部内容吗?","取消",function(){},"确认",
				lotteryCommonPage.clearCodeStasticEvent);*/
		
	});	
	
	//点击追号取消追号事件
	$("#zhuiCodeButton").unbind("click").click(function(param,showParam){
		lotteryCommonPage.changeTozhuiCodeEvent();
	});	
	
	$("#ballSection .lottery-child .container .child-content .ball").unbind("click").click(function(){
		
	});
	 
	//获取最新的开奖号码
	lotteryCommonPage.getLastLotteryCode();
	//获取当前彩种的最新期号和投注倒计时
	lotteryCommonPage.getActiveExpect();
	//加载今天和明天的期号
    lotteryCommonPage.getAllExpectsByTodayAndTomorrow();
	//近10期的开奖记录
	lotteryCommonPage.getNearestTenLotteryCode();
	//获取用户今日投注记录
	lotteryCommonPage.getTodayLotteryByUser();
	
	//每隔10秒钟请求一次获取最新的开奖号码
	if(lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JLFFC" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "HGFFC"){
		window.setInterval("lotteryCommonPage.getLastLotteryCode()",5000);  //分分彩读取最新的开奖号码
		//window.setInterval("lotteryCommonPage.getNearestTenLotteryCode()",5000);
		window.setInterval("lotteryCommonPage.getActiveExpect()",30000); //30秒重新读取服务器的时间
	}else{
		window.setInterval("lotteryCommonPage.getLastLotteryCode()",10000);  //其他彩种读取最新的开奖号码
		//window.setInterval("lotteryCommonPage.getNearestTenLotteryCode()",10000);
		window.setInterval("lotteryCommonPage.getActiveExpect()",60000); //1分钟重新读取服务器的时间
	}
};


Jspk10Page.prototype.getKind = function(codeplan,lotteryType,playkind,playkindDes){
	if(lotteryType.search("PK10")){
		return bjpk10Page.getPk10KindWithPlaykind(codeplan,playkind,playkindDes);
	}else if(lotteryType.search("DPC")){
		
	}else if(lotteryType.search("KS")){
		
	}else if(lotteryType.search("LHC")){
		
	}else if(lotteryType.search("SSC")){
		
	}else if(lotteryType.search("SYXW")){
		
	}else if(lotteryType.search("XYEB")){
		
	}
};

Jspk10Page.prototype.getPk10KindWithPlaykind = function(codeplans,playkind,playkindDes){
	var codeplan = codeplans.split(",");
	if(playkind=="LHD1VS10"){
		if(parseInt(codeplan[0]) > parseInt(codeplan[9])){
			return "龙";
		}else{
			return "虎";
		}
	}
	if(playkind=="LHD2VS9"){
		if(parseInt(codeplan[1]) > parseInt(codeplan[8])){
			return "龙";
		}else{
			return "虎";
		}
	}
	if(playkind=="LHD3VS8"){
		if(parseInt(codeplan[2]) > parseInt(codeplan[7])){
			return "龙";
		}else{
			return "虎";
		}
	}
	if(playkind=="LHD4VS7"){
		if(parseInt(codeplan[3]) > parseInt(codeplan[6])){
			return "龙";
		}else{
			return "虎";
		}
	}
	if(playkind=="LHD5VS6"){
		if(parseInt(codeplan[4]) > parseInt(codeplan[5])){
			return "龙";
		}else{
			return "虎";
		}
	}
	if(playkind=="DXGJ"){
		if(parseInt(codeplan[0]) > 5){
			return "大";
		}else{
			return "小";
		}
	}
	if(playkind=="DXYJ"){
		if(parseInt(codeplan[1]) > 5){
			return "大";
		}else{
			return "小";
		}
	}
	if(playkind=="DXJJ"){
		if(parseInt(codeplan[2]) > 5){
			return "大";
		}else{
			return "小";
		}
	}
	if(playkind=="DXDSM"){
		if(parseInt(codeplan[3]) > 5){
			return "大";
		}else{
			return "小";
		}
	}
	if(playkind=="DXDWM"){
		if(parseInt(codeplan[4]) > 5){
			return "大";
		}else{
			return "小";
		}
	}
	if(playkind=="DSGJ"){
		if(parseInt(codeplan[0])%2==0){
			return "双";
		}else{
			return "单";
		}
	}
	if(playkind=="DSYJ"){
		if(parseInt(codeplan[1])%2==0){
			return "双";
		}else{
			return "单";
		}
	}
	if(playkind=="DSJJ"){
		if(parseInt(codeplan[2])%2==0){
			return "双";
		}else{
			return "单";
		}
	}
	if(playkind=="DSDSM"){
		if(parseInt(codeplan[3])%2==0){
			return "双";
		}else{
			return "单";
		}
	}
	if(playkind=="DSDWM"){
		if(parseInt(codeplan[4])%2==0){
			return "双";
		}else{
			return "单";
		}
	}
	if(playkind=="HZGYJ"){
		return parseInt(codeplan[0])+parseInt(codeplan[1]);
	}
	return playkindDes;
}

