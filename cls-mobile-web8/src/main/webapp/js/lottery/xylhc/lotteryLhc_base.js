
function LotteryLhcCommonPage(){}
var lotteryLhcCommonPage = new LotteryLhcCommonPage();

lotteryLhcCommonPage.currentLotteryKindInstance = null;
lotteryLhcCommonPage.lotteryOrder = {
		lotteryKind:null,
		model:null
};
lotteryLhcCommonPage.param = {
		diffTime : 0,	  //时间倒计时剩余时间	
		currentExpect : null, //当前投注期号
		currentLastOpenExpect : null, //当前最新的开奖期号
		intervalKey : null,  //倒计时周期key
		toFixedValue : 2, //小数点保留4为数字
		isCanActiveExpect : true
}; 

/**
 * 投注参数
 */
lotteryLhcCommonPage.lotteryParam = {
		SOURCECODE_SPLIT : ",",
		SOURCECODE_SPLIT_KS : "&",
		CODE_REPLACE : "-",
		SOURCECODE_ARRANGE_SPLIT : "|",
		currentTotalLotteryCount : 0,
		currentTotalLotteryPrice : 0.0000,
		currentZhuiCodeTotalLotteryPrice : 0.0000, //追号的总共价格
		currentZhuiCodeTotalLotteryCount : 0,
		currentIsUpdateCodeStastic : false,
		updateCodeStasticIndex : null,
		todayTraceList : new Array(),
		tomorrowTraceList : new Array(),
		isZhuiCode : false,  //是否追号
		//currentLotteryWins : new JS_OBJECT_MAP(),  //奖金映射 
	    lotteryMap : new JS_OBJECT_MAP(),  //投注映射
	    lotteryMultipleMap : new JS_OBJECT_MAP(),  //投注倍数映射
	    modelValue : null,
	    awardModel : "YUAN",
	    beishu : 1 //默认是1倍
};

//设置数据处理器  默认六彩彩的
lotteryLhcCommonPage.lottertyDataDeal = lhcData;


$(document).ready(function(){
	
	
	lotteryLhcCommonPage.getTodayLotteryByUser();
	//近10期的开奖记录
	lotteryLhcCommonPage.getNearestTenLotteryCode();
	//获取当前彩种的最新期号和投注倒计时
	lotteryLhcCommonPage.getActiveExpect();
	//获取最新的开奖号码
	lotteryLhcCommonPage.getLastLotteryCode();
	window.setInterval("lotteryLhcCommonPage.getActiveExpect()",10000); //1分钟重新读取服务器
	window.setInterval("lotteryLhcCommonPage.getLastLotteryCode()",20000);  //1分钟重新读取
	
	//ios>input>focus>fixed问题
	/*var u = navigator.userAgent;
	if(!!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/)){
		$(document)
			.on('focus', '#shortcut-money', function() {
				setTimeout(function() {
					if ($('#shortcut-money').is(':focus')) {
						$('.lottery-bar,.header').css('position', 'relative');
						$('.stance-nav').css('height','0');
					}
				}, 200)
			})
			.on('blur', '#shortcut-money', function () {
				setTimeout(function() {
					if (!$('#shortcut-money').is(':focus')) {
						$('.lottery-bar,.header').css('position', 'fixed');
						$('.stance-nav').css('height','192px');
					}
				}, 500);
		})
		$('.content-lhc > div.cols').click(function() {
			setTimeout(function() {
				if ($('.content-lhc > div.cols.on').length > 0) {
					$('#shortcut-money').focus();
				}
			}, 500)
		}); 
	}*/
	
});

/**
 * 选中倍数（金额）光标选中处理
 * @param tag
 * @returns
 */
function onfocusFn(tag) {  
	tag.selectionStart = 0;   //选中区域左边界
	tag.selectionEnd = tag.value.length; //选中区域右边界
    //tag.select();  
} 

/**
 * 阻止safari浏览器默认事件
 * @param eventTag
 * @returns
 */
function onmouseupFn(eventTag) {  
    var event = eventTag||window.event;    
    event.preventDefault();  
}  

/**
* 设置滑动滚动框的宽度
* @param element parent moveContent Element
* @param element child  child Element
*/
LotteryLhcCommonPage.prototype.setMovecontentWidth = function (parent,child){
	var classify_width=0;
	$.each($(parent).find(child),function(i,n){
		var w=$(n).width();
		var m=parseInt($(n).css("margin-right"));
		classify_width=classify_width+w+m;
	});
	$(parent).css("width",classify_width+"px");
}

/**
* 选号 改变
* @return {[type]} [description]
*/
LotteryLhcCommonPage.prototype.toggleMun = function(){

	$(".lottery-child .child-content .mun").click(function(){
		$(".lottery-bar .additional").show();
		$(this).toggleClass('on');
	});
	$(".lottery-child .child-content .mun2").click(function(){
		$(".lottery-bar .additional").show();
		$(this).toggleClass('on');
	});
	$(".lottery-content-lhc .content-lhc .cols").click(function(){
		if($(this).hasClass("cols-no") || $(this).hasClass("cols-title") ){

		}else{
			$(this).toggleClass('on');
		}
	   lotteryLhcCommonPage.currentCodesStastic();
	});
}
/**
 * 获取投注注数
 */
LotteryLhcCommonPage.prototype.getLotteryNum = function(codeNum){
	var modelObject = lhcData.modelMap.get(lhcPage.lotteryParam.currentKindKey);
	if(codeNum<modelObject.lotteryCodeNum){
		showTip("至少要选择"+modelObject.lotteryCodeNum+"个号码！");
		return 0;
	}
	
	var lotteryNum = lotteryLhcCommonPage.countLotteryNum(codeNum,modelObject.lotteryCodeNum);
	return lotteryNum;
}


/**
* 开奖记录
* @return {[type]} [description]
*/
LotteryLhcCommonPage.prototype.toggleDynamic = function(){
	var parent=$(".pane[name='lottery'] .pane[nav-view='active']");
	if(parent.find(".lottery-dynamic").hasClass('in')){
		parent.find(".lottery-dynamic").removeClass('in').addClass('out');
	}else{
		parent.find(".lottery-dynamic").removeClass('out').addClass('in');
	}
}

//选择功能
LotteryLhcCommonPage.prototype.select_all = function (e){
	var parent=select_clear(e);
	parent.find(".child-content .mun").addClass("on");
}
LotteryLhcCommonPage.prototype.select_big = function (e){
	var parent=select_clear(e);
	var len=parseInt(parent.find(".child-content .mun").length/2)-1;
	parent.find(".child-content .mun").each(function(index, element) {
	    if(index>len){$(element).addClass("on");}
	});
}
LotteryLhcCommonPage.prototype.select_small = function(e){
	var parent=select_clear(e);
	var len=parseInt(parent.find(".child-content .mun").length/2)-1;
	parent.find(".child-content .mun").each(function(index, element) {
	    if(index<=len){$(element).addClass("on");}
	});
}
LotteryLhcCommonPage.prototype.select_odd = function (e){
	var parent=select_clear(e);
	parent.find(".child-content .mun").each(function(index, element) {
		var val=parseInt($(element).html());
	    if(val%2==1){$(element).addClass("on");}
	});
}
LotteryLhcCommonPage.prototype.select_even = function(e){
	var parent=select_clear(e);
	parent.find(".child-content .mun").each(function(index, element) {
		var val=parseInt($(element).html());
	    if(val%2==0){$(element).addClass("on");}
	});
}
LotteryLhcCommonPage.prototype.select_clear = function(e){
	var parent=$(e).closest(".lottery-child");
	parent.find(".mun").removeClass("on");
	$(e).addClass("on");
	return parent;
}
//增减倍数
LotteryLhcCommonPage.prototype.multiple_change = function(dirct){
	var val=parseInt($(".multiple-text input").val());
	var pic=parseInt($(".multiple-text input").attr("data-price"));
	if(dirct=="-"){
		if(val==0){return;}else{val--;}pic=pic*val;
	}
	else val++;pic=pic*val;
	$(".multiple-text input").val(val);
	$(".lottery-bar .money").html(pic.toFixed(2));
}
//切换玩法
LotteryLhcCommonPage.prototype.play_change = function(ele,e){
	$(".lottery-classify2 .move-content .child").removeClass('color-red');
	$(e).addClass('color-red');
	$(".lottery-classify2 .clist").removeClass('in').addClass('out');
	$(ele).addClass('in').removeClass('out');
}
LotteryLhcCommonPage.prototype.playcontent_change = function (){
	$(".lottery-content").html(html);
}
//显示玩法或者票种
LotteryLhcCommonPage.prototype.show_classify = function (ele,e){
	var parent=$(e).closest("body");
	var display=$(ele).css("display");
	var style="boolean";
	style=display=="none"?"true":"boolean";
	parent.find(".lottery-classify").not(ele).hide();
	element_toggle(style,[ele],parent);
}
//新彩种初始化
LotteryLhcCommonPage.prototype.lottery_init = function (){
	$(".clist .child").click(function(){
		var parent=$(this).closest('.clist');
		parent.find(".child").addClass('no');
		$(this).removeClass('no');
		var role_id=$(this).attr("data-role-id");
		var mainHtml = lotteryLhcCommonPage.lottertyDataDeal.lotteryKindPlayChoose(role_id);
		//parent.find(".lottery-content").html(mainHtml);
		$("#lhcContent").html(mainHtml);
		lotteryLhcCommonPage.toggleMun();
		element_toggle(false,['.lottery-classify']);
		
		//加载赔率
		lotteryLhcCommonPage.lottertyDataDeal.loadLotteryKindPL(role_id);
		//存储当前切换的玩法模式
		lhcPage.lotteryParam.currentKindKey = role_id;
		
		var currentKindName = lhcData.modelMap.get(role_id).playDesc;
	    $("#currentKindName").text(currentKindName);
	    
	    lotteryLhcCommonPage.lotteryEnd(); //投注结束动作
	});
}
//开奖效果
LotteryLhcCommonPage.prototype.lottery_run = function(element,html){
	$(element).html(html);
	animate_add($(element).children());
}
/**
 * codeNun =选择的号码个数
 * needNum = 玩法需要的号码个数
 */
LotteryLhcCommonPage.prototype.countLotteryNum = function(codeNun,needNum){
	codeNun = Number(codeNun); 
	needNum = Number(needNum); 
	var sum = 1;
	for(var i=1;i<=needNum;i++){
		sum = sum * codeNun/i;
		codeNun--;
	}
	return sum;
}

/**
 * 系统投注逻辑处理
 */
LotteryLhcCommonPage.prototype.userLottery = function(){
	if(lotteryLhcCommonPage.param.isClose){	
		showTip("封盘中,禁止投注！");
		return ;
	}
	$.ajax({
		type : "POST",
		url : contextPath + "/lottery/userLottery",
		data : JSON.stringify(lotteryLhcCommonPage.lotteryOrder),//将对象序列化成JSON字符串;
		dataType : "json",//设置请求参数类型
		contentType : 'application/json;charset=utf-8',//设置请求头信息;
		success : function(result) {
			commonHandlerResult(result, this.url);
			var resultData = result.data;
			if (result.code == "ok") {
				lotteryLhcCommonPage.lotteryEnd(); //投注结束动作
				showTip("投注下单成功.");
				lotteryLhcCommonPage.getTodayLotteryByUser();
			} else if (result.code == "error" && result.data == "BalanceNotEnough") {
				showTip("对不起您的余额不足,请及时充值.");
			} else if (result.code == "error" && result.data == "BanksIsNull"){
				showTip("对不起,系统检测到您还没添加银行卡信息,为了方便中奖提款,请立即前往绑定.");
			} else if (result.code == "error"){
				if(result.data.indexOf("投注期号不正确.") != -1){
					//lotteryCommonPage.getAllExpectsByTodayAndTomorrow(); //重新加载今天和明天的期号				
					showTip(result.data);
				}else{
					showTip(result.data);
				}
			} else{
				showTip("投注请求失败.");
			}
		}
	});
};


/**
 * 展示投注信息
 */
LotteryLhcCommonPage.prototype.showLotteryMsg = function(){
	if(lotteryLhcCommonPage.param.isClose){
		showTip("封盘中,禁止投注！");
		return ;
	}
    var shortcutMoney = $("#shortcut-money").val();
    if(shortcutMoney == ""){
		showTip("请输入下注金额！");
		return ;
	}
    
    if(isNaN(shortcutMoney)){
		showTip("请输入下注数字！");
		return ;
	}
    if(shortcutMoney<2){
		showTip("最低下注金额2元！");
		return ;
	}
	var model = lhcPage.lotteryParam.currentKindKey;
	var lotteryMsgHtml = '';
       lotteryMsgHtml += '<div class="msg-list" ><ul>';
       lotteryMsgHtml += '<li class="msg-list-li msg-list-t">';
	   lotteryMsgHtml += '<div class="child child1">序号</div>';
	   lotteryMsgHtml += '<div class="child child2">内容</div>';
	   lotteryMsgHtml += '<div class="child child3">赔率</div>';
	   lotteryMsgHtml += '<div class="child child4">下注金额</div></li>';
	
   var modelObject = lhcData.modelMap.get(model);	
  // lotteryLhcCommonPage.lotteryOrder = {};
   var resultArray = new Array();
   var totalMoney = 0,lotteryNum = 0;
     //复选框遍历,连码，合肖，连肖，尾数连，全不中
    if(model == 'LMSQZ' || model == 'LMSZ22'|| model == 'LMSZ23'|| model == 'LMEQZ'
    	|| model == 'LMEZT' || model == 'LMEZ2' || model == 'LMTC'|| model == 'LMSZ1'
    	|| model == 'HX2'|| model == 'HX3'|| model == 'HX4'|| model == 'HX5'
    	|| model == 'HX6'|| model == 'HX7'|| model == 'HX8'|| model == 'HX9'
    	|| model == 'HX10'|| model == 'HX11'|| model == 'WSLELZ'|| model == 'WSLSLZ'
    	|| model == 'WSLSILZ'|| model == 'WSLELBZ'|| model == 'WSLSLBZ'|| model == 'WSLSILBZ'
    	|| model == 'LXELZ'|| model == 'LXSLZ'|| model == 'LXSILZ'|| model == 'LXWLZ'
    	|| model == 'LXELBZ'|| model == 'LXSLBZ'|| model == 'LXSILBZ'|| model == 'QBZ5'
    	|| model == 'QBZ6'|| model == 'QBZ7'|| model == 'QBZ8'|| model == 'QBZ9'|| model == 'QBZ10'|| model == 'QBZ11'|| model == 'QBZ12'){
    	var codeNumDesc ="";
    	var codePl;
    	var codes = "";
    	var codePlArr = new Array();
    	var codeNun=0;
    	var j=0;
      	$(".lottery-content-lhc .content-lhc .cols").each(function(){
      		if($(this).hasClass('on')){
      			codeNun++;
      			 if(model == 'HX2'|| model == 'HX3'|| model == 'HX4'|| model == 'HX5'
      		    	|| model == 'HX6'|| model == 'HX7'|| model == 'HX8'|| model == 'HX9'
      		      	|| model == 'HX10'|| model == 'HX11'|| model == 'LXELZ'|| model == 'LXSLZ'|| model == 'LXSILZ'|| model == 'LXWLZ'
      		      	|| model == 'LXELBZ'|| model == 'LXSLBZ'|| model == 'LXSILBZ'|| model == 'WSLELZ'|| model == 'WSLSLZ'
      		      	|| model == 'WSLSILZ'|| model == 'WSLELBZ'|| model == 'WSLSLBZ'|| model == 'WSLSILBZ'){
     				var codeNum =  $(this).find("span").attr("data-role-id");
     				codes +=  codeNum+",";
     				codeNumDesc +=  lhcData.playNumDescMap[model+codeNum]+",";
     				codePl = $("#pl"+codeNum).text();
     				codePlArr[j]=codePl;	
     			}else{
	                codeNumDesc +=  $(this).find(".lhc-mun").text()+",";
	      			codes +=  $(this).find(".lhc-mun").attr("data-role-id")+",";
	      			codePl =  $(this).find("span").text();
	      			codePlArr[j]=codePl;		
     			}
      			j++;
      		}	
      	 }		
      	);
    	
    	  	
    	if(codeNun>=modelObject.lotteryCodeNum){
    		//统计注数，金钱
    		lotteryNum = lotteryLhcCommonPage.countLotteryNum(codeNun,modelObject.lotteryCodeNum);
    		totalMoney = Number(shortcutMoney)*Number(lotteryNum);
			
			var resultArrayDetail = {};
   			resultArrayDetail.playKindStr = model;//内容
   			resultArrayDetail.playKindCodes = codes.substring(0,codes.length-1);//号码
   			resultArrayDetail.lotteryMultiple = shortcutMoney;//投注金额
   			resultArray[0] = resultArrayDetail;
   			
			//lotteryDataDeal.playNumDescMap
			lotteryMsgHtml += '<li class="msg-list-li">';
			lotteryMsgHtml += '<div class="child child1">'+1+'</div>';
			lotteryMsgHtml += '<div class="child child2">'+codeNumDesc.substring(0,codeNumDesc.length-1)+'</div>';
			lotteryMsgHtml += '<div class="child child3">'+lotteryLhcCommonPage.returnMinPl(codePlArr)+'</div>';
			lotteryMsgHtml += '<div class="child child4">'+shortcutMoney+'</div></li>';
    	}else{
    		showTip("至少要选择"+modelObject.lotteryCodeNum+"个号码！");
    		
    		return ;
    	}
    //文本框遍历
    }else{
        var j=0;
    	var i=1;
    	$(".lottery-content-lhc .content-lhc .cols").each(function(){
    		if($(this).hasClass('on')){
    			lotteryNum++;
    			var codeNum;
    			var codeNumDesc;
    			var codePl;
    			if(model == 'ZM16'){
    				codeNum =  $(this).find("span").attr("data-role-id");
    				codeNumDesc =  lhcData.playNumDescMap[model+codeNum];
    				codePl =  $(this).find("span").text();
    			}else if(model == 'TMSX' || model == 'YXWS'){
    				codeNum =  $(this).find("span").attr("data-role-id");
    				codeNumDesc =  lhcData.playNumDescMap[model+codeNum];
    				codePl = $("#pl"+codeNum).text();
    			}else if(model == 'BB'){
    				codeNum =  $(this).attr("data-role-id");
    				codeNumDesc =  lhcData.playNumDescMap[model+codeNum];
    				codePl = $("#pl"+codeNum).text();
    			}else{
    				codeNumDesc =  $(this).find(".lhc-mun").text();
    				codeNum =  $(this).find(".lhc-mun").attr("data-role-id");
    				codePl =  $(this).find("span").text();
    			}
    			 
//    			resultArray[j] = new Array();
//    			resultArray[j][0] = model;//内容
//    			resultArray[j][1] = codeNum;//号码
//    			resultArray[j][2] = shortcutMoney;//投注金额
    			
    			var resultArrayDetail = {};
	   			resultArrayDetail.playKindStr = model;//内容
	   			resultArrayDetail.playKindCodes = codeNum;//号码
	   			resultArrayDetail.lotteryMultiple = shortcutMoney;//投注金额
	   			resultArray[j] = resultArrayDetail;
	   			
    			lotteryMsgHtml += '<li class="msg-list-li">';
    			lotteryMsgHtml += '<div class="child child1">'+(j+1)+'</div>';
    			lotteryMsgHtml += '<div class="child child2">'+codeNumDesc+'</div>';
    			lotteryMsgHtml += '<div class="child child3">'+codePl+'</div>';
    			lotteryMsgHtml += '<div class="child child4">'+shortcutMoney+'</div></li>';
    			totalMoney += Number(shortcutMoney);
    			j++;
    			i++;
    		}	
    	 }		
    	);
    	
    }    
  
        lotteryMsgHtml += '</ul></div>';
        lotteryMsgHtml += '<ul>';
        lotteryMsgHtml += '<li class="user-line">';
        lotteryMsgHtml += '<div class="line-title"><p>期数:</p></div>';
        lotteryMsgHtml += '<div class="line-content"><p>'+lotteryLhcCommonPage.param.currentExpect+'</p></div>';
        lotteryMsgHtml += '</li>';
        lotteryMsgHtml += '<li class="user-line">';
        lotteryMsgHtml += '<div class="line-title"><p>玩法:</p></div>';
        lotteryMsgHtml += '<div class="line-content"><p>'+modelObject.playDesc+'</p></div>';
        lotteryMsgHtml += '</li>';
        lotteryMsgHtml += '<li class="user-line">';
        lotteryMsgHtml += '<div class="line-title"><p>总注数：</p></div>';
        lotteryMsgHtml += '<div class="line-content"><p>'+lotteryNum+'</p></div>';
        lotteryMsgHtml += '</li>';
        lotteryMsgHtml += '<li class="user-line">';
        lotteryMsgHtml += '<div class="line-title"><p>总下注额：</p></div>';
        lotteryMsgHtml += '<div class="line-content"><p class="font-red">'+totalMoney+'</p></div>';
        lotteryMsgHtml += '</li></ul></div>';
        lotteryMsgHtml += '</div>';
        
        lotteryLhcCommonPage.lotteryOrder.lotteryKind = lhcPage.param.kindName;  //投注彩种
        lotteryLhcCommonPage.lotteryOrder.model = lotteryLhcCommonPage.lotteryParam.awardModel;
        lotteryLhcCommonPage.lotteryOrder.awardModel = currentUser.sscRebate;
        lotteryLhcCommonPage.lotteryOrder.isLotteryByPoint = 0;
        lotteryLhcCommonPage.lotteryOrder.playKindMsgs = resultArray; 
    //    lotteryCommonPage.lotteryOrder.expect = lotteryCommonPage.param.currentExpect;
        
        if(lotteryNum<=0){
        	showTip("请选择投注号码！");
        	return;
        }
        lotteryLhcCommonPage.lotteryOrder.expect = lotteryLhcCommonPage.param.currentExpect;
        $("#liuhecai_msg").html(lotteryMsgHtml);
        element_toggle(true,['.alert-cart']);
    
	return true;
}

/**
 * 投注结束动作
 */
LotteryLhcCommonPage.prototype.lotteryEnd = function(){
	element_toggle(false,['.alert-cart']);
	$(".lottery-bar .additional").hide();
	$("#J-balls-statistics-lotteryNum").html(0);
	$("#J-balls-statistics-amount").html(0);
	$("#J-balls-statistics-code").text("（投注列表）");
	$("#shortcut-money").val("10");
	$("#lhcReset").click();
	
};
var data;

/**
 * 计算出最低赔率！
 */
LotteryLhcCommonPage.prototype.returnMinPl = function(plArr){
	var pl = Number(plArr[0]);
	for(var i=1;i<plArr.length;i++){
		var pltemp = Number(plArr[i]);
		 if(pl > pltemp){
			 pl = pltemp;
		 }
	}
	 return pl;
};

/**
 * 获取系统当前正在投注的期号
 */
LotteryLhcCommonPage.prototype.getActiveExpect = function(){
	if(lotteryLhcCommonPage.param.isCanActiveExpect){  //判断是否可以进行期号的请求
		var jsonData = {
				"lotteryKind" : lotteryLhcCommonPage.currentLotteryKindInstance.param.kindName 
		};
		$.ajax({
			type : "POST",
			url : contextPath + "/lotteryinfo/getExpectAndDiffTime",
			data : JSON.stringify(jsonData),
			dataType : "json",
			contentType : 'application/json;charset=utf-8',
			success : function(result) {
				commonHandlerResult(result, this.url);
				var resultData = result.data;
				if (result.code == "ok") {
					lotteryLhcCommonPage.param.isCanActiveExpect = false;
					var issue = resultData;
					if(issue != null){
						//展示当前期号
						lotteryLhcCommonPage.currentLotteryKindInstance.showCurrentLotteryCode(issue);
						//判断当前期号是否发生变化
						if(lotteryLhcCommonPage.param.currentExpect == null || lotteryLhcCommonPage.param.currentExpect != issue.lotteryNum) {
							//记录当前期号
							lotteryLhcCommonPage.param.currentExpect = issue.lotteryNum; 
						}
						//当前期号剩余时间倒计时
						lotteryLhcCommonPage.param.diffTime = issue.timeDifference;					
						window.clearInterval(lotteryLhcCommonPage.param.intervalKey); //终止周期性倒计时
	                    lotteryLhcCommonPage.param.intervalKey = setInterval("lotteryLhcCommonPage.currentLotteryKindInstance.calculateTime("+issue.isClose+")",1000);	

					}else{
						$("#timer").text("封盘中");
					}
					
					if(lotteryLhcCommonPage.param.diffTime > 5000){ //如果剩余时间超过5秒的
						setTimeout("lotteryLhcCommonPage.controlIsCanActive()", 2000);
					}else{
						lotteryLhcCommonPage.param.isCanActiveExpect = true;
					}
				} else if (result.code == "error") {
					showTip(resultData);
				}
			}
		});
	}
};

/**
 * 控制是否能发起期号的请求
 */
LotteryLhcCommonPage.prototype.controlIsCanActive = function(){
	lotteryLhcCommonPage.param.isCanActiveExpect = true;
};

/**
 * 获取最新的开奖号码
 */
LotteryLhcCommonPage.prototype.getLastLotteryCode = function(){
	var jsonData = {
			"lotteryKind" : lotteryLhcCommonPage.currentLotteryKindInstance.param.kindName,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/code/getLastLotteryCode",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var lotteryCode = result.data;
				lotteryLhcCommonPage.currentLotteryKindInstance.showLastLotteryCode(lotteryCode);
				
				var lotteryNum = lotteryCode.lotteryNum;
				//当开奖号码发生变化
				if(lotteryLhcCommonPage.param.currentLastOpenExpect == null || lotteryLhcCommonPage.param.currentLastOpenExpect != lotteryNum){
					lotteryLhcCommonPage.lottery_run(".lottery_liuhecai .lottery_run .child-muns",$(".lottery_liuhecai .lottery_run .child-muns").html());
				}
				lotteryLhcCommonPage.param.currentLastOpenExpect = lotteryNum;  //存储当前期号	
			} else if (result.code == "error") {
				showTip(result.data);
			}
		}
	});
};

/**
 * 获取最近十期的开奖号码
 */
LotteryLhcCommonPage.prototype.getNearestTenLotteryCode = function(){
	var jsonData = {
			"lotteryKind" : lotteryLhcCommonPage.currentLotteryKindInstance.param.kindName,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/code/getNearestTenLotteryCode",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var lotteryCodeList = result.data;
				lotteryLhcCommonPage.showNearestTenLotteryCode(lotteryCodeList);
			} else if (result.code == "error") {
				showTip(result.data);
			}
		}
	});
}

/**
 * 刷新最近十期的开奖号码的显示
 */
LotteryLhcCommonPage.prototype.showNearestTenLotteryCode = function(lotteryCodeList){
	if(lotteryCodeList != null && lotteryCodeList.length > 0){
		$("#nearestTenLotteryCode").html("");
		var str = "";
		str += "<div class='titles'>";
		str += "	<div class='child child1'><p>期号</p></div>";
		str += "	<div class='child child2'><p>开奖号</p></div>";
		str += "</div>";
		
		str += "<div class='content'>";
		for(var i = 0; i < lotteryCodeList.length; i++){
			//只加载5条记录
			if(i > 4) {
				break;
			}
			var lotteryCode = lotteryCodeList[i];
			var openCodeStr = lotteryLhcCommonPage.currentLotteryKindInstance.getOpenCodeStr(lotteryCode.lotteryNum);
			var  colors = lhcPage.changeColorByCode(lotteryCode);
			str += "	<div class='line'>";
			str += "		<div class='child child1'><p>"+openCodeStr+"</p></div>";
			str += "		<div class='child child2 font-blue muns muns-small'>";
			str += "			<div class='lhc-mun-item'><div class='mun color-"+colors[0]+"'>"+lotteryCode.numInfo1+"</div><p class='btn-title color-"+colors[0]+"'>"+lhcPage.returnAninal(lotteryCode.numInfo1)+"</p></div>";
			str += "			<div class='lhc-mun-item'><div class='mun color-"+colors[1]+"'>"+lotteryCode.numInfo2+"</div><p class='btn-title color-"+colors[1]+"'>"+lhcPage.returnAninal(lotteryCode.numInfo2)+"</p></div>";
			str += "			<div class='lhc-mun-item'><div class='mun color-"+colors[2]+"'>"+lotteryCode.numInfo3+"</div><p class='btn-title color-"+colors[2]+"'>"+lhcPage.returnAninal(lotteryCode.numInfo3)+"</p></div>";
			str += "			<div class='lhc-mun-item'><div class='mun color-"+colors[3]+"'>"+lotteryCode.numInfo4+"</div><p class='btn-title color-"+colors[3]+"'>"+lhcPage.returnAninal(lotteryCode.numInfo4)+"</p></div>";
			str += "			<div class='lhc-mun-item'><div class='mun color-"+colors[4]+"'>"+lotteryCode.numInfo5+"</div><p class='btn-title color-"+colors[4]+"'>"+lhcPage.returnAninal(lotteryCode.numInfo5)+"</p></div>";
			str += "			<div class='lhc-mun-item'><div class='mun color-"+colors[5]+"'>"+lotteryCode.numInfo6+"</div><p class='btn-title color-"+colors[5]+"'>"+lhcPage.returnAninal(lotteryCode.numInfo6)+"</p></div>";
			str += "			<div class='lhc-mun-item'><div class='mun and'>+</div><p class='btn-title'>&nbsp;</p></div>";
			str += "			<div class='lhc-mun-item'><div class='mun color-"+colors[6]+"'>"+lotteryCode.numInfo7+"</div><p class='btn-title color-"+colors[6]+"'>"+lhcPage.returnAninal(lotteryCode.numInfo7)+"</p></div>";
			str += "		</div>";
		    str += "	</div>";
			
		}
		str += "</div>";
		$("#nearestTenLotteryCode").html(str);
	//	lotteryLhcCommonPage.showLastLottery(lotteryCodeList);
		
	}else{
		$("#nearestTenLotteryCode").html("<ul class='c-list'><span>未加载到开奖记录</span></ul>");
	}
};





/**
 * 获取今日投注数据
 */
LotteryLhcCommonPage.prototype.getTodayLotteryByUser = function(){
	$.ajax({
		type : "POST",
		url : contextPath + "/order/getTodayLotteryByUser",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var todayLotteryList = result.data;
				if(todayLotteryList.length > 0){
					//彩种游戏下方的游戏记录
					var userOrderListObj = $("#userTodayLotteryList");
					userOrderListObj.html("");
					
					var str = "";
					str += "<div class='line'>";
					str += "	<div class='child child1'><p>彩种</p></div>";
					str += "	<div class='child child2'><p>期号</p></div>";
					str += "	<div class='child child3'><p>投注金额</p></div>";
					str += "	<div class='child child4'><p>奖金</p></div>";
					str += "	<div class='child child5'><p>状态</p></div>";
					str += "	<div class='child child6'><i class='ion-close-circled'></i></div>";
					str += "</div>";
					userOrderListObj.append(str);
					str = "";
					
					for(var i = 0; i < todayLotteryList.length; i++){
						//显示5条最近投注记录
						if(i > 4) { 
							break;
						}
						var userOrder = todayLotteryList[i];
			    		var lotteryIdStr = userOrder.lotteryId;
			    		
			    		//订单状态特殊处理
			    		var prostateStatus = userOrder.prostateStatusStr;
			    		if(userOrder.prostate == "REGRESSION" || userOrder.prostate == "END_STOP_AFTER_NUMBER" ||
			    				userOrder.prostate == "NO_LOTTERY_BACKSPACE" || userOrder.prostate == "UNUSUAL_BACKSPACE" ||
			    				userOrder.prostate == "STOP_DEALING") {
			    			prostateStatus = "已撤单";
			    		}
			    		
			    		str += "<div class='line' name='lotteryMsgRecord' data-lottery='"+userOrder.lotteryId+"'  data-type='"+userOrder.stopAfterNumber+"' data-value='"+userOrder.id+"'>";
						str += "	<div class='child child1'><p>"+ userOrder.lotteryTypeDes +"</p></div>";
						str += "	<div class='child child2'><p>"+ userOrder.expect +"</p></div>";
						str += "	<div class='child child3'><p>"+ userOrder.payMoney.toFixed(frontCommonPage.param.fixVaue) +"</p></div>";
						str += "	<div class='child child4'><p>"+ userOrder.winCost.toFixed(frontCommonPage.param.fixVaue) +"</p></div>";
						if(userOrder.prostate == 'DEALING'){
							str += "	<div class='child child5'><p class='cp-yellow'>"+ prostateStatus +"</p></div>";
						}else if(userOrder.prostate == 'NOT_WINNING'){
							str += "	<div class='child child5'><p class='cp-red'>"+ prostateStatus +"</p></div>";
						}else {
							str += "	<div class='child child5'><p class='cp-green'>"+ prostateStatus +"</p></div>";
						}
						str += "	<div class='child child6'><i class='ion-close-circled'></i></div>";
						str += "</div>";
						userOrderListObj.append(str);
						str = "";
					}
					
					var afterNumberLotteryWindow;
					
				}else{
					$("#userTodayLotteryList").html("<div class='line'><p class='no-msg'>还没有投注记录哟~</p></div>");
				}
				
				//查看投注详情
				$("a[name='lotteryMsgRecord']").unbind("click").click(function(event){
					var orderId = $(this).attr("data-value");
					var dataType = $(this).attr("data-type");
					var width = 800;
					var height = 400;
					var left = parseInt((screen.availWidth/2) - (width/2));//屏幕居中
					var top = parseInt((screen.availHeight/2) - (height/2));
					var windowFeatures = "width=" + width + ",height=" + height + ",status,resizable,scrollbars=yes,left=" + left + ",top=" + top;
					
					if(dataType == 0){
						var lotteryhrefStr = contextPath + "/gameBet/lotterymsg/"+orderId+"/lotterymsg.html";
					}else{
						var dataLottery = $(this).attr("data-lottery");
						var lotteryhrefStr = contextPath + "/gameBet/lotteryafternumbermsg/"+dataLottery+"/lotteryafternumbermsg.html";
					}
					afterNumberLotteryWindow = window.open(lotteryhrefStr, "subWind", windowFeatures);
					afterNumberLotteryWindow.focus();
				});
				
			} else if (result.code == "error") {
				showTip(result.data);
			}
		}
	});
}


/**
 * 金额值改变处理
 * @returns {Boolean}
 */
LotteryLhcCommonPage.prototype.beishuEventContent = function(){
	var beishuObj = $('#beishu');
    var beishuStr = $('#shortcut-money').val();
	var re = /^[1-9]+[0-9]*]*$/;
    if (!re.test(beishuStr)){
    	//beishuObj.val("1");  //this.value.replace(/[^0-9]/g,'') 1
		//lotteryCommonPage.lotteryParam.beishu = 1;
    	return false;
    }
    var beishuLength = beishuStr.length;
	if(beishuLength > 0){
		 //倍数限制在9999
		 var beishuTemp = parseInt(beishuStr);
		 if(beishuTemp > 9999){
			 beishuObj.val(9999);
		 }
		 lotteryLhcCommonPage.currentCodesStastic();
	}
};

/**
 * 显示当前投注金额跟注数 
 */
LotteryLhcCommonPage.prototype.currentCodesStastic = function(){

		var model = lhcPage.lotteryParam.currentKindKey;
		var totalCodeNumDesc ="";
		var lotteryNum = 0;
		var codeNumDesc;
		var i=0;
		$(".lottery-content-lhc .content-lhc .cols").each(function(){
			if($(this).hasClass('on')){
				if(model == 'ZM16' || model == 'TMSX'  || model == 'YXWS'
					|| model == 'HX2'|| model == 'HX3'|| model == 'HX4'|| model == 'HX5'
	      		    || model == 'HX6'|| model == 'HX7'|| model == 'HX8'|| model == 'HX9'
      		      	|| model == 'HX10'|| model == 'HX11'|| model == 'LXELZ'|| model == 'LXSLZ'|| model == 'LXSILZ'|| model == 'LXWLZ'
      		      	|| model == 'LXELBZ'|| model == 'LXSLBZ'|| model == 'LXSILBZ'|| model == 'WSLELZ'|| model == 'WSLSLZ'
      		      	|| model == 'WSLSILZ'|| model == 'WSLELBZ'|| model == 'WSLSLBZ'|| model == 'WSLSILBZ'){
					var codeNum =  $(this).find("span").attr("data-role-id");
					codeNumDesc =  lhcData.playNumDescMap[model+codeNum];
				}else if(model == 'BB'){
					var codeNum =  $(this).attr("data-role-id");
					codeNumDesc =  lhcData.playNumDescMap[model+codeNum];
				}else{
					codeNumDesc =  $(this).find(".lhc-mun").text();
			
				}
				if(totalCodeNumDesc == ""){
					totalCodeNumDesc += codeNumDesc;
				}else{
					totalCodeNumDesc += ","+codeNumDesc;
				}
			
				if(totalCodeNumDesc != null && totalCodeNumDesc != "") {
					if(totalCodeNumDesc.length > 14) {
						totalCodeNumDesc = totalCodeNumDesc.substring(0, 14) + "...";
					} else if(totalCodeNumDesc.length == 0){
						totalCodeNumDesc = "(投注内容)";
					}
				} 
				i ++;	
			}	
		 }		
		);
			
		if(i>0){
			lotteryNum = lotteryLhcCommonPage.getLotteryNum(i);	
		} 
		//投注数目大于0  则显示倍数栏
		if(lotteryNum > 0) {
			if($(".lottery-bar .additional").css('display') == "none") {
				$(".lottery-bar .additional").show();
				$("#shortcut-money").focus();
			}
		} else {
			$(".lottery-bar .additional").hide();
		}
		   var shortcutMoney = $("#shortcut-money").val();
	    if(shortcutMoney == ""){
			//showTip("请输入下注金额！");
			//$("#shortcut-money").focus();
			
			// 第一次聚焦滚到底部
			/*var u = navigator.userAgent;
			if(!!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/)){
				var height = $('body').height();
				$(window).scrollTop(height);
				$('.lottery-bar,.header').css('position', 'relative');
				$('.stance-nav').css('height','0');
			}*/
			
			return ;
	     }
		$("#J-balls-statistics-lotteryNum").text(lotteryNum);
		$("#J-balls-statistics-amount").text(Number(shortcutMoney)*Number(lotteryNum));
		$("#J-balls-statistics-code").text(totalCodeNumDesc);
	
};
