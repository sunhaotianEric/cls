function LhcData(){}
var lhcData = new LhcData();

lhcData.param = {
		bizSystem:null
};
lhcData.modelMap = new JS_OBJECT_MAP();
lhcData.playNumDescMap = new JS_OBJECT_MAP();

lhcData.boshe = {
		hongbo:[1,2,7,8,12,13,18,19,23,24,29,30,34,35,40,45,46],
		lanbo:[3,4,9,10,14,15,20,25,26,31,36,37,41,42,47,48],
		lvbo:[5,6,11,16,17,21,22,27,28,32,33,38,39,43,44,49]
 }
lhcData.shengxiao = {
		shengxiaoKey:new Array('鼠','牛','虎','兔','龙','蛇','马','羊','猴','鸡','狗','猪'),
//		shengxiaoArr:[[10,22,34,46],[9,21,33,45],[8,20,32,44],[7,19,31,43],[6,18,30,42],[5,17,29,41],
//		              [4,16,28,40],[3,15,27,39],[2,14,26,38],[1,13,25,37,49],[12,24,36,48],[11,23,35,47]]
	    shengxiaoArr:null,
	    jiqin:null,
	    yeshou:null
	}
//波色判断
LhcData.prototype.colorByCode = function(obj) {
	  //判断是否是红波
	  var arr = lhcData.boshe.hongbo;
	  var i = arr.length;  
	    while (i--) {  
	        if (arr[i] === obj) {  
	            return 'red';  
	        }  
	    } 
	    //判断是否是红波
	    var arr = lhcData.boshe.lanbo;
	    var i = arr.length;  
	     while (i--) {  
	        if (arr[i] === obj) {  
	            return 'blue';  
	        }  
	     } 
		    //判断是否是红波
		  var arr = lhcData.boshe.lvbo;
		  var i = arr.length;  
		    while (i--) {  
		        if (arr[i] === obj) {  
		            return 'green';  
		        }  
		    } 
	    
	    return '';  
}
/**
 * 初始化1-49html,涉及玩法有正码，特码，正码特，连码，全不中
 * startpos:1,11,21,31,41
 */
LhcData.prototype.initPlay = function(model) {
	
	 var  main_lists_html = '<div class="content-lhc">';
	 
	 
	 for(var i=1;i<=49;i++){
		 var color = lhcData.colorByCode(i);
		 main_lists_html += '<div class="cols cols-25 ball-'+i+'">';
		 main_lists_html += '<div class="lhc-mun lhc-'+color+'" data-role-id="'+i+'">'+i+'</div>';
		 main_lists_html += '<span class="font-red"  id="pl'+i+'">加载中</span></div>';
	 }
		
	//根据类型去拼接后面不规则玩法	
	if(model == "TMA" || model == "TMB"){
		     main_lists_html += '<div class="cols cols-25 cols-no"></div>';
		     main_lists_html += '<div class="cols cols-25 cols-no"></div>';
			 main_lists_html += '<div class="cols cols-25 cols-no"></div>';
			 main_lists_html += '<div class="cols cols-25">';
			 main_lists_html += '<div class="lhc-mun lhc-mun-font" data-role-id="50">单</div>';
			 main_lists_html += '<span class="font-red" id="pl50">加载中</span>'; 
			 main_lists_html += '</div>';
			 main_lists_html += '<div class="cols cols-25">';
			 main_lists_html += '<div class="lhc-mun lhc-mun-font" data-role-id="51">双</div>';
			 main_lists_html += '<span class="font-red" id="pl51">加载中</span>'; 
			 main_lists_html += '</div>';
			 main_lists_html += '<div class="cols cols-25">';
			 main_lists_html += '<div class="lhc-mun lhc-mun-font" data-role-id="54">大</div>';
			 main_lists_html += '<span class="font-red" id="pl54">加载中</span>'; 
			 main_lists_html += '</div>';
			 main_lists_html += '<div class="cols cols-25">';
			 main_lists_html += '<div class="lhc-mun lhc-mun-font" data-role-id="55">小</div>';
			 main_lists_html += '<span class="font-red" id="pl55">加载中</span>'; 
			 main_lists_html += '</div>';
			 main_lists_html += '<div class="cols cols-25">';
			 main_lists_html += '<div class="lhc-mun lhc-mun-font" data-role-id="52">大单</div>';
			 main_lists_html += '<span class="font-red" id="pl52">加载中</span>'; 
			 main_lists_html += '</div>';
			 main_lists_html += '<div class="cols cols-25">';
			 main_lists_html += '<div class="lhc-mun lhc-mun-font" data-role-id="53">小单</div>';
			 main_lists_html += '<span class="font-red" id="pl53">加载中</span>'; 
			 main_lists_html += '</div>';
			 main_lists_html += '<div class="cols cols-25">';
			 main_lists_html += '<div class="lhc-mun lhc-mun-font" data-role-id="56">大双</div>';
			 main_lists_html += '<span class="font-red" id="pl56">加载中</span>'; 
			 main_lists_html += '</div>';
			 main_lists_html += '<div class="cols cols-25">';
			 main_lists_html += '<div class="lhc-mun lhc-mun-font" data-role-id="57">小双</div>';
			 main_lists_html += '<span class="font-red" id="pl57">加载中</span>'; 
			 main_lists_html += '</div>';
			 main_lists_html += '<div class="cols cols-25">';
			 main_lists_html += '<div class="lhc-mun lhc-mun-font" data-role-id="58">合单</div>';
			 main_lists_html += '<span class="font-red" id="pl58">加载中</span>'; 
			 main_lists_html += '</div>';
			 main_lists_html += '<div class="cols cols-25">';
			 main_lists_html += '<div class="lhc-mun lhc-mun-font" data-role-id="59">合双</div>';
			 main_lists_html += '<span class="font-red" id="pl59">加载中</span>'; 
			 main_lists_html += '</div>';
			 main_lists_html += '<div class="cols cols-25">';
			 main_lists_html += '<div class="lhc-mun lhc-mun-font" data-role-id="62">尾大</div>';
			 main_lists_html += '<span class="font-red" id="pl62">加载中</span>'; 
			 main_lists_html += '</div>';
			 main_lists_html += '<div class="cols cols-25">';
			 main_lists_html += '<div class="lhc-mun lhc-mun-font" data-role-id="63">尾小</div>';
			 main_lists_html += '<span class="font-red" id="pl63">加载中</span>'; 
			 main_lists_html += '</div>';
			 main_lists_html += '<div class="cols cols-25">';
			 main_lists_html += '<div class="lhc-mun lhc-mun-font" data-role-id="65">家禽</div>';
			 main_lists_html += '<span class="font-red" id="pl65">加载中</span>'; 
			 main_lists_html += '</div>';
			 main_lists_html += '<div class="cols cols-25">';
			 main_lists_html += '<div class="lhc-mun lhc-mun-font" data-role-id="66">野兽</div>';
			 main_lists_html += '<span class="font-red" id="pl66">加载中</span>'; 
			 main_lists_html += '</div>';
			 main_lists_html += '<div class="cols cols-25">';
			 main_lists_html += '<div class="lhc-mun lhc-mun-font" data-role-id="60">红波</div>';
			 main_lists_html += '<span class="font-red" id="pl60">加载中</span>'; 
			 main_lists_html += '</div>';
			 main_lists_html += '<div class="cols cols-25">';
			 main_lists_html += '<div class="lhc-mun lhc-mun-font" data-role-id="64">绿波</div>';
			 main_lists_html += '<span class="font-red" id="pl64">加载中</span>'; 
			 main_lists_html += '</div>'; 
			 main_lists_html += '<div class="cols cols-25">';
			 main_lists_html += '<div class="lhc-mun lhc-mun-font" data-role-id="61">蓝波</div>';
			 main_lists_html += '<span class="font-red" id="pl61">加载中</span>'; 
			 main_lists_html += '</div>' ;
			 main_lists_html += '</div>' ;
			 //快捷选择
//			 main_lists_html += '<div class="select-lhc">' ;
//			 main_lists_html += '<div class="cols cols-25" data-type="dan" onclick="lhcPage.select_type(\'.lottery_liuhecai\',this)">' ;			 
//			 main_lists_html += '<p class="mun">单</p>' ;
//			 main_lists_html += '</div>' ;
//			 main_lists_html += '<div class="cols cols-25" data-type="shuang" onclick="lhcPage.select_type(\'.lottery_liuhecai\',this)">' ;			 
//			 main_lists_html += '<p class="mun">双</p>' ;
//			 main_lists_html += '</div>' ;
//			 main_lists_html += '<div class="cols cols-25" data-type="da" onclick="lhcPage.select_type(\'.lottery_liuhecai\',this)">' ;			 
//			 main_lists_html += '<p class="mun">大</p>' ;
//			 main_lists_html += '</div>' ;
//			 main_lists_html += '<div class="cols cols-25" data-type="xiao" onclick="lhcPage.select_type(\'.lottery_liuhecai\',this)">' ;			 
//			 main_lists_html += '<p class="mun">小</p>' ;
//			 main_lists_html += '</div>' ;
//			 main_lists_html += '<div class="cols cols-25" data-type="hedan" onclick="lhcPage.select_type(\'.lottery_liuhecai\',this)">' ;			 
//			 main_lists_html += '<p class="mun">合单</p>' ;
//			 main_lists_html += '</div>' ;
//			 main_lists_html += '<div class="cols cols-25" data-type="heshuang" onclick="lhcPage.select_type(\'.lottery_liuhecai\',this)">' ;			 
//			 main_lists_html += '<p class="mun">合双</p>' ;
//			 main_lists_html += '</div>' ;
//			 main_lists_html += '<div class="cols cols-25" data-type="jiaqin" onclick="lhcPage.select_type(\'.lottery_liuhecai\',this)">' ;			 
//			 main_lists_html += '<p class="mun">家禽</p>' ;
//			 main_lists_html += '</div>' ;
//			 main_lists_html += '<div class="cols cols-25" data-type="yeshou" onclick="lhcPage.select_type(\'.lottery_liuhecai\',this)">' ;			 
//			 main_lists_html += '<p class="mun">野兽</p>' ;
//			 main_lists_html += '</div>' ;
//			 main_lists_html += '<div class="cols cols-25" data-type="hong" onclick="lhcPage.select_type(\'.lottery_liuhecai\',this)">' ;			 
//			 main_lists_html += '<p class="mun">红波</p>' ;
//			 main_lists_html += '</div>' ;
//			 main_lists_html += '<div class="cols cols-25" data-type="lv" onclick="lhcPage.select_type(\'.lottery_liuhecai\',this)">' ;			 
//			 main_lists_html += '<p class="mun">绿波</p>' ;
//			 main_lists_html += '</div>' ;
//			 main_lists_html += '<div class="cols cols-25" data-type="lan" onclick="lhcPage.select_type(\'.lottery_liuhecai\',this)">' ;			 
//			 main_lists_html += '<p class="mun">蓝波</p>' ;
//			 main_lists_html += '</div>' ;
	//正码玩法	
	}else if(model=="ZMA" || model=="ZMB"){
		 main_lists_html += '<div class="cols cols-25 cols-no"></div>';
	     main_lists_html += '<div class="cols cols-25 cols-no"></div>';
		 main_lists_html += '<div class="cols cols-25 cols-no"></div>';
		 main_lists_html += '<div class="cols cols-25">';
		 main_lists_html += '<div class="lhc-mun lhc-mun-font" data-role-id="50">总大</div>';
		 main_lists_html += '<span class="font-red" id="pl50">加载中</span>'; 
		 main_lists_html += '</div>';
		 main_lists_html += '<div class="cols cols-25">';
		 main_lists_html += '<div class="lhc-mun lhc-mun-font" data-role-id="51">总小</div>';
		 main_lists_html += '<span class="font-red" id="pl51">加载中</span>'; 
		 main_lists_html += '</div>';
		 main_lists_html += '<div class="cols cols-25">';
		 main_lists_html += '<div class="lhc-mun lhc-mun-font" data-role-id="52">总单</div>';
		 main_lists_html += '<span class="font-red" id="pl52">加载中</span>'; 
		 main_lists_html += '</div>';
		 main_lists_html += '<div class="cols cols-25">';
		 main_lists_html += '<div class="lhc-mun lhc-mun-font" data-role-id="53">总双</div>';
		 main_lists_html += '<span class="font-red" id="pl53">加载中</span>'; 
		 main_lists_html += '</div>';
		 main_lists_html += '</div>';
		 //快捷选择
//		 main_lists_html += '<div class="select-lhc">' ;
//		 main_lists_html += '<div class="cols cols-25" data-type="dan" onclick="lhcPage.select_type(\'.lottery_liuhecai\',this)">' ;			 
//		 main_lists_html += '<p class="mun">单</p>' ;
//		 main_lists_html += '</div>' ;
//		 main_lists_html += '<div class="cols cols-25" data-type="shuang" onclick="lhcPage.select_type(\'.lottery_liuhecai\',this)">' ;			 
//		 main_lists_html += '<p class="mun">双</p>' ;
//		 main_lists_html += '</div>' ;
//		 main_lists_html += '<div class="cols cols-25" data-type="da" onclick="lhcPage.select_type(\'.lottery_liuhecai\',this)">' ;			 
//		 main_lists_html += '<p class="mun">大</p>' ;
//		 main_lists_html += '</div>' ;
//		 main_lists_html += '<div class="cols cols-25" data-type="xiao" onclick="lhcPage.select_type(\'.lottery_liuhecai\',this)">' ;			 
//		 main_lists_html += '<p class="mun">小</p>' ;
//		 main_lists_html += '</div>' ;
//		 main_lists_html += '<div class="cols cols-25" data-type="hedan" onclick="lhcPage.select_type(\'.lottery_liuhecai\',this)">' ;			 
//		 main_lists_html += '<p class="mun">合单</p>' ;
//		 main_lists_html += '</div>' ;
//		 main_lists_html += '<div class="cols cols-25" data-type="heshuang" onclick="lhcPage.select_type(\'.lottery_liuhecai\',this)">' ;			 
//		 main_lists_html += '<p class="mun">合双</p>' ;
//		 main_lists_html += '</div>' ;
//		 main_lists_html += '<div class="cols cols-25" data-type="jiaqin" onclick="lhcPage.select_type(\'.lottery_liuhecai\',this)">' ;			 
//		 main_lists_html += '<p class="mun">家禽</p>' ;
//		 main_lists_html += '</div>' ;
//		 main_lists_html += '<div class="cols cols-25" data-type="yeshou" onclick="lhcPage.select_type(\'.lottery_liuhecai\',this)">' ;			 
//		 main_lists_html += '<p class="mun">野兽</p>' ;
//		 main_lists_html += '</div>' ;
//		 main_lists_html += '<div class="cols cols-25" data-type="hong" onclick="lhcPage.select_type(\'.lottery_liuhecai\',this)">' ;			 
//		 main_lists_html += '<p class="mun">红波</p>' ;
//		 main_lists_html += '</div>' ;
//		 main_lists_html += '<div class="cols cols-25" data-type="lv" onclick="lhcPage.select_type(\'.lottery_liuhecai\',this)">' ;			 
//		 main_lists_html += '<p class="mun">绿波</p>' ;
//		 main_lists_html += '</div>' ;
//		 main_lists_html += '<div class="cols cols-25" data-type="lan" onclick="lhcPage.select_type(\'.lottery_liuhecai\',this)">' ;			 
//		 main_lists_html += '<p class="mun">蓝波</p>' ;
//		 main_lists_html += '</div>' ;
	}else if(model=="ZMT1" || model=="ZMT2" || model=="ZMT3" || model=="ZMT4"|| model=="ZMT5"|| model=="ZMT6"){
		 main_lists_html += '<div class="cols cols-25 cols-no"></div>';
	     main_lists_html += '<div class="cols cols-25 cols-no"></div>';
		 main_lists_html += '<div class="cols cols-25 cols-no"></div>';
		 main_lists_html += '<div class="cols cols-25">';
		 main_lists_html += '<div class="lhc-mun lhc-mun-font" data-role-id="50">单</div>';
		 main_lists_html += '<span class="font-red" id="pl50">加载中</span>'; 
		 main_lists_html += '</div>';
		 main_lists_html += '<div class="cols cols-25">';
		 main_lists_html += '<div class="lhc-mun lhc-mun-font" data-role-id="51">双</div>';
		 main_lists_html += '<span class="font-red" id="pl51">加载中</span>'; 
		 main_lists_html += '</div>';
		 main_lists_html += '<div class="cols cols-25">';
		 main_lists_html += '<div class="lhc-mun lhc-mun-font" data-role-id="52">大</div>';
		 main_lists_html += '<span class="font-red" id="pl52">加载中</span>'; 
		 main_lists_html += '</div>';
		 main_lists_html += '<div class="cols cols-25">';
		 main_lists_html += '<div class="lhc-mun lhc-mun-font" data-role-id="53">小</div>';
		 main_lists_html += '<span class="font-red" id="pl53">加载中</span>'; 
		 main_lists_html += '</div>';
		 main_lists_html += '<div class="cols cols-25">';
		 main_lists_html += '<div class="lhc-mun lhc-mun-font" data-role-id="54">合单</div>';
		 main_lists_html += '<span class="font-red" id="pl54">加载中</span>'; 
		 main_lists_html += '</div>';
		 main_lists_html += '<div class="cols cols-25">';
		 main_lists_html += '<div class="lhc-mun lhc-mun-font" data-role-id="55">合双</div>';
		 main_lists_html += '<span class="font-red" id="pl55">加载中</span>'; 
		 main_lists_html += '</div>';
		 main_lists_html += '<div class="cols cols-25">';
		 main_lists_html += '<div class="lhc-mun lhc-mun-font" data-role-id="56">红波</div>';
		 main_lists_html += '<span class="font-red" id="pl56">加载中</span>'; 
		 main_lists_html += '</div>';
		 main_lists_html += '<div class="cols cols-25">';
		 main_lists_html += '<div class="lhc-mun lhc-mun-font" data-role-id="57">绿波</div>';
		 main_lists_html += '<span class="font-red" id="pl57">加载中</span>'; 
		 main_lists_html += '</div>';
		 main_lists_html += '<div class="cols cols-25">';
		 main_lists_html += '<div class="lhc-mun lhc-mun-font" data-role-id="58">蓝波</div>';
		 main_lists_html += '<span class="font-red" id="pl58">加载中</span>'; 
		 main_lists_html += '</div>';
	}
	main_lists_html += "</div>";
	
	return main_lists_html;
 	
}
//正码16html
LhcData.prototype.initZM16Play = function(model) {
	var arr =["单","双","大","小","红波","蓝波","绿波","合大","合小","合单","合双","尾大","尾小"];
	var  main_lists_html = '<div class="content-lhc">';
	     main_lists_html += '<div class="cols cols-10 cols-title">';
	     main_lists_html += '<span>...</span>';
	     main_lists_html += '</div>';
	     main_lists_html += '<div class="cols cols-15 cols-title">';
	     main_lists_html += '<span>正码一</span>';
	     main_lists_html += '</div>'
	     main_lists_html += '<div class="cols cols-15 cols-title">';
	     main_lists_html += '<span>正码二</span>';
	     main_lists_html += '</div>'
		 main_lists_html += '<div class="cols cols-15 cols-title">';
	     main_lists_html += '<span>正码三</span>';
	     main_lists_html += '</div>'
		 main_lists_html += '<div class="cols cols-15 cols-title">';
	     main_lists_html += '<span>正码四</span>';
	     main_lists_html += '</div>'
		 main_lists_html += '<div class="cols cols-15 cols-title">';
	     main_lists_html += '<span>正码五</span>';
	     main_lists_html += '</div>'
		 main_lists_html += '<div class="cols cols-15 cols-title">';
	     main_lists_html += '<span>正码六</span>';
	     main_lists_html += '</div>';
	     for(var i=1;i<=13;i++){
	    	 main_lists_html += '<div class="cols cols-10 cols-title">'; 
	    	 main_lists_html += '<span>'+arr[i-1]+'</span>'; 
	    	 main_lists_html += '</div>';
	    	 main_lists_html += '<div class="cols cols-15">'; 
	    	 main_lists_html += '<span class="font-red" id="pl'+i+'" data-role-id="'+i+'">加载中</span>'; 
	    	 main_lists_html += '</div>'; 
	    	 main_lists_html += '<div class="cols cols-15">'; 
	    	 main_lists_html += '<span class="font-red" id="pl'+(i+13)+'" data-role-id="'+(i+13)+'">加载中</span>'; 
	    	 main_lists_html += '</div>'; 
	    	 main_lists_html += '<div class="cols cols-15">'; 
	    	 main_lists_html += '<span class="font-red" id="pl'+(i+26)+'" data-role-id="'+(i+26)+'">加载中</span>'; 
	    	 main_lists_html += '</div>'; 
	    	 main_lists_html += '<div class="cols cols-15">'; 
	    	 main_lists_html += '<span class="font-red" id="pl'+(i+39)+'" data-role-id="'+(i+39)+'">加载中</span>'; 
	    	 main_lists_html += '</div>'; 
	    	 main_lists_html += '<div class="cols cols-15">'; 
	    	 main_lists_html += '<span class="font-red" id="pl'+(i+52)+'" data-role-id="'+(i+52)+'">加载中</span>'; 
	    	 main_lists_html += '</div>'; 
	    	 main_lists_html += '<div class="cols cols-15">'; 
	    	 main_lists_html += '<span class="font-red" id="pl'+(i+65)+'" data-role-id="'+(i+65)+'">加载中</span>'; 
	    	 main_lists_html += '</div>';     	 
	     }
	     main_lists_html += '</div>';
	     return main_lists_html;
}




/**
 * 根据红波，绿波，蓝波返回颜色
 */
LhcData.prototype.selectColor = function (i){
	//红波
	if(i == 1 || i ==2 || i ==7 || i ==8 || i ==12 || i ==13 || i ==18 
	   || i ==19 || i ==23 || i ==24 || i ==29 || i ==30 || i ==34 || i ==35
	   || i ==40 || i ==45 || i ==46  ){
		
		return "red";
	//绿波
	}else if(i == 5 || i ==6 || i ==11 || i ==16 || i ==17 || i ==21 || i ==22 
			   || i ==27 || i ==28 || i ==32 || i ==33  || i ==38 || i ==39 || i ==43
			   || i ==44 || i ==49  ){
		return "green";		
	//蓝波
	}else if(i == 3 || i ==4 || i ==9 || i ==10 || i ==14 || i ==15 || i ==20 
			   || i ==25 || i ==26 || i ==31 || i ==36 || i ==37 || i ==41 || i ==42
			   || i ==47 || i ==48  ){
	    return "blue";
	}
}

/**
 * 初始化html,半波，
 * 
 */
LhcData.prototype.init_banbo_Play = function(model) {
	var main_lists_html = '';
	
	    main_lists_html += '<div class="content-lhc">';
        main_lists_html += '<div class="cols cols-15 cols-title">';
        main_lists_html += '<span class="font-gray" data-role-id="1">红单 </span>';	
        main_lists_html += '<span class="font-red" id="pl1">加载中</span>';
        main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-85 cols-balls cols-center" data-role-id="1">';
	    main_lists_html += '<div class="lhc-mun lhc-red">1</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">7</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">13</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">19</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">23</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">29</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">35</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">45</div>';
	    main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-15 cols-title">';
        main_lists_html += '<span class="font-gray" data-role-id="2">红双 </span>';	
        main_lists_html += '<span class="font-red" id="pl2">加载中</span>';
        main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-85 cols-balls cols-center" data-role-id="2">';
	    main_lists_html += '<div class="lhc-mun lhc-red">2</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">8</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">12</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">18</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">24</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">30</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">34</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">40</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">46</div>';
	    main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-15 cols-title">';
        main_lists_html += '<span class="font-gray" data-role-id="3">红大 </span>';	
        main_lists_html += '<span class="font-red" id="pl3">加载中</span>';
        main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-85 cols-balls cols-center" data-role-id="3">';
	    main_lists_html += '<div class="lhc-mun lhc-red">29</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">30</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">34</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">35</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">40</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">45</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">46</div>';
	    main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-15 cols-title">';
        main_lists_html += '<span class="font-gray" data-role-id="4">红小 </span>';	
        main_lists_html += '<span class="font-red" id="pl4">加载中</span>';
        main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-85 cols-balls cols-center" data-role-id="4">';
	    main_lists_html += '<div class="lhc-mun lhc-red">1</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">2</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">7</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">8</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">12</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">13</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">18</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">19</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">23</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">24</div>';
	    main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-15 cols-title">';
        main_lists_html += '<span class="font-gray" data-role-id="5">绿单 </span>';	
        main_lists_html += '<span class="font-red" id="pl5">加载中</span>';
        main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-85 cols-balls cols-center" data-role-id="5">';
	    main_lists_html += '<div class="lhc-mun lhc-green">5</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">11</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">17</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">21</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">27</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">33</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">39</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">43</div>';
	    main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-15 cols-title">';
        main_lists_html += '<span class="font-gray" data-role-id="6">绿双 </span>';	
        main_lists_html += '<span class="font-red" id="pl6">加载中</span>';
        main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-85 cols-balls cols-center" data-role-id="6">';
	    main_lists_html += '<div class="lhc-mun lhc-green">6</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">16</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">22</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">28</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">32</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">38</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">44</div>';
	    main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-15 cols-title">';
        main_lists_html += '<span class="font-gray" data-role-id="7">绿大 </span>';	
        main_lists_html += '<span class="font-red" id="pl7">加载中</span>';
        main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-85 cols-balls cols-center" data-role-id="7">';
	    main_lists_html += '<div class="lhc-mun lhc-green">27</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">28</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">32</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">33</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">38</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">39</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">43</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">44</div>';
	    main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-15 cols-title">';
        main_lists_html += '<span class="font-gray" data-role-id="8">绿小 </span>';	
        main_lists_html += '<span class="font-red" id="pl8">加载中</span>';
        main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-85 cols-balls cols-center" data-role-id="8">';
	    main_lists_html += '<div class="lhc-mun lhc-green">5</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">6</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">11</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">16</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">17</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">21</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">22</div>';
	    main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-15 cols-title">';
        main_lists_html += '<span class="font-gray" data-role-id="9">蓝单 </span>';	
        main_lists_html += '<span class="font-red" id="pl9">加载中</span>';
        main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-85 cols-balls cols-center" data-role-id="9">';
	    main_lists_html += '<div class="lhc-mun lhc-blue">3</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">9</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">15</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">25</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">31</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">37</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">41</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">42</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">47</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">48</div>';
	    main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-15 cols-title">';
        main_lists_html += '<span class="font-gray" data-role-id="10">蓝双 </span>';	
        main_lists_html += '<span class="font-red" id="pl10">加载中</span>';
        main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-85 cols-balls cols-center" data-role-id="10">';
	    main_lists_html += '<div class="lhc-mun lhc-blue">4</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">10</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">14</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">20</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">26</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">36</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">48</div>';
	    main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-15 cols-title">';
        main_lists_html += '<span class="font-gray" data-role-id="11">蓝大 </span>';	
        main_lists_html += '<span class="font-red" id="pl11">加载中</span>';
        main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-85 cols-balls cols-center" data-role-id="11">';
	    main_lists_html += '<div class="lhc-mun lhc-blue">25</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">26</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">31</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">36</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">37</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">41</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">42</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">47</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">48</div>';
	    main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-15 cols-title">';
        main_lists_html += '<span class="font-gray" data-role-id="12">蓝小 </span>';	
        main_lists_html += '<span class="font-red" id="pl12">加载中</span>';
        main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-85 cols-balls cols-center" data-role-id="12">';
	    main_lists_html += '<div class="lhc-mun lhc-blue">3</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">4</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">9</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">10</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">14</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">15</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">20</div>';
	    main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-15 cols-title">';
        main_lists_html += '<span class="font-gray" data-role-id="13">红合单 </span>';	
        main_lists_html += '<span class="font-red" id="pl13">加载中</span>';
        main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-85 cols-balls cols-center" data-role-id="13">';
	    main_lists_html += '<div class="lhc-mun lhc-red">1</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">7</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">12</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">18</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">23</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">29</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">30</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">34</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">45</div>';
	    main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-15 cols-title">';
        main_lists_html += '<span class="font-gray" data-role-id="14">红合双 </span>';	
        main_lists_html += '<span class="font-red" id="pl14">加载中</span>';
        main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-85 cols-balls cols-center" data-role-id="14">';
	    main_lists_html += '<div class="lhc-mun lhc-red">2</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">8</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">13</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">19</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">24</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">35</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">40</div>';
	    main_lists_html += '<div class="lhc-mun lhc-red">46</div>';
	    main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-15 cols-title">';
        main_lists_html += '<span class="font-gray" data-role-id="15">绿合单 </span>';	
        main_lists_html += '<span class="font-red" id="pl15">加载中</span>';
        main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-85 cols-balls cols-center" data-role-id="15">';
	    main_lists_html += '<div class="lhc-mun lhc-green">5</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">16</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">21</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">27</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">32</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">38</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">43</div>';
	    main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-15 cols-title">';
        main_lists_html += '<span class="font-gray" data-role-id="16">绿和双 </span>';	
        main_lists_html += '<span class="font-red" id="pl16">加载中</span>';
        main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-85 cols-balls cols-center" data-role-id="16">';
	    main_lists_html += '<div class="lhc-mun lhc-green">6</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">11</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">17</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">22</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">28</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">33</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">39</div>';
	    main_lists_html += '<div class="lhc-mun lhc-green">44</div>';
	    main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-15 cols-title">';
        main_lists_html += '<span class="font-gray" data-role-id="17">蓝合单 </span>';	
        main_lists_html += '<span class="font-red" id="pl17">加载中</span>';
        main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-85 cols-balls cols-center" data-role-id="17">';
	    main_lists_html += '<div class="lhc-mun lhc-blue">3</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">9</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">10</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">14</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">25</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">36</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">41</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">47</div>';
	    main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-15 cols-title">';
        main_lists_html += '<span class="font-gray" data-role-id="18">蓝合双 </span>';	
        main_lists_html += '<span class="font-red" id="pl18">加载中</span>';
        main_lists_html += '</div>';
        main_lists_html += '<div class="cols cols-85 cols-balls cols-center" data-role-id="18">';
	    main_lists_html += '<div class="lhc-mun lhc-blue">4</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">15</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">20</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">26</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">31</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">37</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">42</div>';
	    main_lists_html += '<div class="lhc-mun lhc-blue">48</div>';
	    main_lists_html += '</div>';
	    main_lists_html += '</div>';
	  return main_lists_html;
}

//colsChildClick();
LhcData.prototype.colsChildSet = function (momey){
	var shortcutmoney = $("#shortcut-money").val();
	quickmoney = Number(shortcutmoney) + Number(momey);
	$("#shortcut-money").val(quickmoney);
	
	lotteryLhcCommonPage.currentCodesStastic();
}

/**
 * 初始化html,涉及玩法有一肖，特码生肖，合肖，连肖，
 * 
 */
LhcData.prototype.init_shengxiao_Play = function(model) {
	   var main_lists_html = '';
	
	    main_lists_html += '<div class="content-lhc">';
	    for(var i=1;i<=12;i++){
	       main_lists_html += '<div class="cols cols-15 cols-title">';
	       main_lists_html += '<span class="font-gray" >'+lhcData.shengxiao.shengxiaoKey[i-1]+' </span>';	
	       main_lists_html += '<span class="font-red" id="pl'+i+'">加载中</span>';
	       main_lists_html += '</div>';
	       main_lists_html += '<div class="cols cols-85 cols-balls cols-center"><span data-role-id="'+i+'" style="display: none;"></span>';
	       var sx = lhcData.shengxiao.shengxiaoArr[i-1];
	       for(var j=0;j<sx.length;j++){
	    	   main_lists_html += '<div class="lhc-mun lhc-'+lhcData.selectColor(sx[j])+'">'+sx[j]+'</div>';   
	       }
	       main_lists_html += '</div>';
	    }
	    if(model=='YXWS'){
	    	 main_lists_html += '<div class="cols cols-25">';
			 main_lists_html += '<div class="lhc-mun lhc-mun-font">0尾</div>';
			 main_lists_html += '<span class="font-red" id="pl13" data-role-id="13">加载中</span>'; 
			 main_lists_html += '</div>';
			 main_lists_html += '<div class="cols cols-25">';
			 main_lists_html += '<div class="lhc-mun lhc-mun-font">1尾</div>';
			 main_lists_html += '<span class="font-red" id="pl14" data-role-id="14">加载中</span>'; 
			 main_lists_html += '</div>';
			 main_lists_html += '<div class="cols cols-25">';
			 main_lists_html += '<div class="lhc-mun lhc-mun-font">2尾</div>';
			 main_lists_html += '<span class="font-red" id="pl15" data-role-id="15">加载中</span>'; 
			 main_lists_html += '</div>';
			 main_lists_html += '<div class="cols cols-25">';
			 main_lists_html += '<div class="lhc-mun lhc-mun-font">3尾</div>';
			 main_lists_html += '<span class="font-red" id="pl16" data-role-id="16">加载中</span>'; 
			 main_lists_html += '</div>';
			 main_lists_html += '<div class="cols cols-25">';
			 main_lists_html += '<div class="lhc-mun lhc-mun-font">4尾</div>';
			 main_lists_html += '<span class="font-red" id="pl17" data-role-id="17">加载中</span>'; 
			 main_lists_html += '</div>';
			 main_lists_html += '<div class="cols cols-25">';
			 main_lists_html += '<div class="lhc-mun lhc-mun-font">5尾</div>';
			 main_lists_html += '<span class="font-red" id="pl18" data-role-id="18">加载中</span>'; 
			 main_lists_html += '</div>';
			 main_lists_html += '<div class="cols cols-25">';
			 main_lists_html += '<div class="lhc-mun lhc-mun-font">6尾</div>';
			 main_lists_html += '<span class="font-red" id="pl19" data-role-id="19">加载中</span>'; 
			 main_lists_html += '</div>';
			 main_lists_html += '<div class="cols cols-25">';
			 main_lists_html += '<div class="lhc-mun lhc-mun-font">7尾</div>';
			 main_lists_html += '<span class="font-red" id="pl20" data-role-id="20">加载中</span>'; 
			 main_lists_html += '</div>';
			 main_lists_html += '<div class="cols cols-25">';
			 main_lists_html += '<div class="lhc-mun lhc-mun-font">8尾</div>';
			 main_lists_html += '<span class="font-red" id="pl21" data-role-id="21">加载中</span>'; 
			 main_lists_html += '</div>';
			 main_lists_html += '<div class="cols cols-25">';
			 main_lists_html += '<div class="lhc-mun lhc-mun-font">9尾</div>';
			 main_lists_html += '<span class="font-red" id="pl22" data-role-id="22">加载中</span>'; 
			 main_lists_html += '</div>';		            
	    }
	    
	    main_lists_html += '</div>';
	  return main_lists_html;
}

/**
 * 初始化html,涉及玩法有尾数连
 * 
 */
LhcData.prototype.init_weishulian_Play = function(model) {
	  var main_lists_html = '';
		
	    main_lists_html += '<div class="content-lhc">';
	    for(var i=1;i<=10;i++){
	       main_lists_html += '<div class="cols cols-15 cols-title">';
	       main_lists_html += '<span class="font-gray">'+(i%10)+'  </span>';	
	       main_lists_html += '<span class="font-red" id="pl'+i+'">加载中</span>';
	       main_lists_html += '</div>';
	       main_lists_html += '<div class="cols cols-85 cols-balls cols-center"><span data-role-id="'+i+'" style="display: none;"></span>';
	   
	       for(var j=0;j<=4;j++){
	    	   var codeNum = i+(j*10);
	    	   if(codeNum!=50){
	    	     main_lists_html += '<div class="lhc-mun lhc-'+lhcData.selectColor(codeNum)+'" data-role-id="'+codeNum+'">'+codeNum+'</div>';   
	    	   }
	    	  }
	       main_lists_html += '</div>';
	    }
	    main_lists_html += '</div>';
	    return main_lists_html;
}

/**
 * 玩法选择
 */
LhcData.prototype.lotteryKindPlayChoose = function(model){
	var main_lists_html="";	
	if(model=="TMA" || model=="TMB"){
		main_lists_html = lhcData.initPlay(model);

	}else if(model=="ZMA" || model=="ZMB"){
		main_lists_html = lhcData.initPlay(model);
		
	}else if(model=="ZMT1"|| model=="ZMT2" || model=="ZMT3" || model=="ZMT4" || model=="ZMT5" || model=="ZMT6" ){
		main_lists_html = lhcData.initPlay(model);

	  }else if(model=="ZM16"){
			main_lists_html = lhcData.initZM16Play();			
		}else if(model=="LMSQZ"||model=="LMSZ22"||model=="LMSZ23"||model=="LMEQZ"||model=="LMEZT"||model=="LMEZ2"||model=="LMTC" ||model=="LMSZ1"){

			main_lists_html = lhcData.initPlay(model);
		}else if(model=="BB"){
			main_lists_html = lhcData.init_banbo_Play(model);
		}else if(model=="YXWS"){
			main_lists_html = lhcData.init_shengxiao_Play(model);
		
		}else if(model=="TMSX"){
			main_lists_html = lhcData.init_shengxiao_Play(model);
		}else if(model=="HX2"||model=="HX3"||model=="HX4"||model=="HX5"||model=="HX6"
			||model=="HX7"||model=="HX8"||model=="HX9"||model=="HX10"||model=="HX11"){
			main_lists_html = lhcData.init_shengxiao_Play(model);
			
		}else if(model=="LXELZ" ||model=="LXSLZ" ||model=="LXSILZ" ||model=="LXWLZ" ||model=="LXELBZ"||model=="LXSLBZ"||model=="LXSILBZ"){

			main_lists_html = lhcData.init_shengxiao_Play(model);
			
		}else if(model=="WSLELZ" || model=="WSLSLZ" || model=="WSLSILZ"|| model=="WSLELBZ"|| model=="WSLSLBZ"|| model=="WSLSILBZ"){

			main_lists_html = lhcData.init_weishulian_Play(model);
			
		}else if(model=="QBZ5"||model=="QBZ6"||model=="QBZ7"||model=="QBZ8"||model=="QBZ9"||model=="QBZ10"||model=="QBZ11"||model=="QBZ12"){

			main_lists_html = lhcData.initPlay(model);
		}



	return main_lists_html;
}

/**
 * 加载某种玩法赔率
 */
LhcData.prototype.loadLotteryKindPL = function(model) {
	//ajax请求	
	var jsonData = {
		"lotteryTypeProtype" : model,
	}
	$.ajax({
		type : "POST",
		// 设置请求为同步请求
		async : false,
		url : contextPath + "/lottery/getLotteryWinLhcByKind",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			var resultData = result.data;
			if(result.code == "ok"){
				//需要验证赔率数据加载完全不完全
				for(var i=1;i<=resultData.length;i++){
					$("#pl"+resultData[i-1].code).text(resultData[i-1].winMoney);
				}
			}else if(result.code == "error"){
				showTip(resultData);
				$("#editUserButton").removeAttr("disabled");
			}
		}
	});
}
/**
 * 加载所有的玩法
 */
LhcData.prototype.loadLotteryKindPlay = function() {
	$("#lotteryKindPlayList").append(lhcData.plays.SBTH_PLAY);
	$("#lotteryKindPlayList").append(lhcData.plays.EBTH_PLAY);
};

