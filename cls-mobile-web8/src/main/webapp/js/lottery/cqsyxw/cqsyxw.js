function CqsyxwPage(){}
var cqsyxwPage = new CqsyxwPage();

//基本参数
cqsyxwPage.param = {
	unitPrice : 2,  //每注价格
	kindDes : "重庆11选5",
	kindName : 'CQSYXW',  //彩种
	kindNameType : 'SYXW', //大彩种拼写
	currentLotteryWins : new JS_OBJECT_MAP(),  //奖金映射 
	currentLotteryWinsLoaded : false,  //奖金映射是否加载完毕
	ommitData : null,  //遗漏数据
	hotColdData : null //冷热数据
};

//投注参数
cqsyxwPage.lotteryParam = {
    currentKindKey : 'XWRXWZWFS',    //当前所选择的玩法模式,默认是五星复式
	currentLotteryCount : 0,       //当前投注数目
	currentLotteryPrice : 0,       //当前投注价格,按元来计算
	currentLotteryTotalCount : 0,  //当前总投注数目
	codes : null,                  //投注号码
	codesStastic : new Array(),
	codesStr : ""   //投注下方显示的号码
};

/**
 * 获取开奖号码前台展现的字符串
 */
CqsyxwPage.prototype.getOpenCodeStr = function(openCodeNum){
	var openCodeStr = "";
	if(isNotEmpty(openCodeNum)){
		var expectDay = openCodeNum.substring(0,openCodeNum.length - 2);
		var expectNum = openCodeNum.substring(openCodeNum.length - 2,openCodeNum.length);
		openCodeStr = expectDay + "-" + expectNum;
	}
	return openCodeStr;
};

/**
 * 展示当前正在投注的期号
 */
CqsyxwPage.prototype.showCurrentLotteryCode = function(lotteryIssue){
	if(lotteryIssue != null){
		var openCodeStr = cqsyxwPage.getOpenCodeStr(lotteryIssue.lotteryNum);
		$('#nowNumber').text(openCodeStr);
	}
};

/**
 * 展示最新的开奖号码
 */
CqsyxwPage.prototype.showLastLotteryCode = function(lotteryCode){
	if(lotteryCode != null){
		var lotteryNum = lotteryCode.lotteryNum;
		var openCodeStr = cqsyxwPage.getOpenCodeStr(lotteryCode.lotteryNum);
		
		$('#J-lottery-info-lastnumber').text(openCodeStr);
		
		var openCodeStr = "";
		openCodeStr += "<span class='ball color-red' id='lotteryNumber1'>" + lotteryCode.numInfo1 + "</span>";
		openCodeStr += "<span class='ball color-red' id='lotteryNumber2'>" + lotteryCode.numInfo2 + "</span>";
		openCodeStr += "<span class='ball color-red' id='lotteryNumber3'>" + lotteryCode.numInfo3 + "</span>";
		openCodeStr += "<span class='ball color-red' id='lotteryNumber4'>" + lotteryCode.numInfo4 + "</span>";
		openCodeStr += "<span class='ball color-red' id='lotteryNumber5'>" + lotteryCode.numInfo5 + "</span>";
		$(".lottery_run .child-muns").html(openCodeStr);
		
	}
};

/**
 * 查找奖金对照表
 */
CqsyxwPage.prototype.getLotteryWins = function(){
	//还未加载奖金的
	if(!cqsyxwPage.param.currentLotteryWinsLoaded) {
		frontLotteryAction.getLotteryWins(cqsyxwPage.param.kindName,function(r){
			if (r[0] != null && r[0] == "ok") {
				for (var key in r[1]) {  
					//将奖金对照表缓存
					cqsyxwPage.param.currentLotteryWins.put(key,r[1][key]);
				}  
				cqsyxwPage.param.currentLotteryWinsLoaded = true;
			}else if(r[0] != null && r[0] == "error"){
				showTip(r[1]);
			}else{
				showTip("获取奖金对照表的请求失败.");
			}
		});
	}
};

/**
 * 选好号码事件，添加号码到投注池中
 */
CqsyxwPage.prototype.userLotteryNumForAddOrder = function(){
	//判断是否单式玩法
	var kindKey = cqsyxwPage.lotteryParam.currentKindKey;
	if(kindKey == 'XYRXYZYDS' || kindKey == 'XEQEZXDS' || kindKey == 'XEQEZXDS_G' || kindKey == 'XERXEZEDS'
		|| kindKey == 'XSQSZXDS' || kindKey == 'XSQSZXDS_G' || kindKey == 'XSRXSZSDS'
		|| kindKey == 'XSHRXSZSDS' || kindKey == 'XWRXWZWDS' || kindKey == 'XLRXLZWDS'
		|| kindKey == 'XQRXQZWDS' || kindKey == 'XBRXBZWDS'){  
		//需要走单式的验证
		return cqsyxwPage.singleLotteryNum();
	}else{
		return cqsyxwPage.userLotteryNum();
	}
};

/**
 * 用户添加投注号码（复式）
 */
CqsyxwPage.prototype.userLotteryNum = function(){
	
	if(cqsyxwPage.lotteryParam.codes == null || cqsyxwPage.lotteryParam.codes.length == 0){
		showTip("号码选择不完整，请重新选择！");
		return false;
	}
	
	//添加进投注统计号码池 金额\注数\倍数\最高奖金\玩法描述\号码
	var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
	var currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get(cqsyxwPage.param.kindNameType +"_"+cqsyxwPage.lotteryParam.currentKindKey);
	
	//如果处于修改投注号码的状态
	if(lotteryCommonPage.lotteryParam.currentIsUpdateCodeStastic && lotteryCommonPage.lotteryParam.updateCodeStasticIndex != null){
		var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[lotteryCommonPage.lotteryParam.updateCodeStasticIndex];
		codeStastic[0] = cqsyxwPage.lotteryParam.currentLotteryPrice;
		codeStastic[1] = cqsyxwPage.lotteryParam.currentLotteryCount;
		codeStastic[2] = lotteryCommonPage.lotteryParam.beishu;
		codeStastic[3] = lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu);
		codeStastic[4] = lotteryCommonPage.lottertyDataDeal.lotteryKindMap.get(cqsyxwPage.lotteryParam.currentKindKey);
		codeStastic[5] = cqsyxwPage.lotteryParam.codes;
		codeStastic[6] = cqsyxwPage.param.kindNameType +"_"+cqsyxwPage.lotteryParam.currentKindKey;
		codeStastic[7] = codeStastic[7];

		//倍数更新
		lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStastic[7]); //先清除原来的投注号码的倍数
		lotteryCommonPage.lotteryParam.lotteryMultipleMap.put(codeStastic[7],lotteryCommonPage.lotteryParam.beishu);

		//将所选号码和玩法的映射放置map中
		var lotteryKindMap =  new JS_OBJECT_MAP();
		lotteryKindMap.put(cqsyxwPage.lotteryParam.currentKindKey,cqsyxwPage.lotteryParam.codes);
		lotteryCommonPage.lotteryParam.lotteryMap.remove(codeStastic[7]); //先清除原来的
		lotteryCommonPage.lotteryParam.lotteryMap.put(codeStastic[7],lotteryKindMap);
		
		//清空当前选中的号码
		//lotteryCommonPage.clearCurrentCodes();			
	
	}else{
		var isYetHave = false;
		var yetIndex = null;
		var codeStastics = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic;
		for(var i = codeStastics.length - 1; i >= 0 ; i--){
			 var codeStastic = codeStastics[i];
		     var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
			 if(kindKey == cqsyxwPage.lotteryParam.currentKindKey && codeStastic[5].toString() == cqsyxwPage.lotteryParam.codes){
				 isYetHave = true;
				 yetIndex = i;
				 break;
			 }
		}
		
		if(isYetHave && yetIndex != null){
			showTip("您选择的号码在号码篮已存在，将直接进行倍数累加!");
			var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[yetIndex];
			codeStastic[0] = (parseFloat(codeStastic[0]) + parseFloat(cqsyxwPage.lotteryParam.currentLotteryPrice)).toFixed(lotteryCommonPage.param.toFixedValue);
			codeStastic[1] = codeStastic[1];
			codeStastic[2] = parseInt(codeStastic[2]) + lotteryCommonPage.lotteryParam.beishu;
			codeStastic[3] = (parseFloat(codeStastic[3]) + parseFloat(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu))).toFixed(lotteryCommonPage.param.toFixedValue);
			codeStastic[4] = lotteryCommonPage.lottertyDataDeal.lotteryKindMap.get(cqsyxwPage.lotteryParam.currentKindKey);
			codeStastic[5] = cqsyxwPage.lotteryParam.codes;
			codeStastic[6] = cqsyxwPage.param.kindNameType +"_"+cqsyxwPage.lotteryParam.currentKindKey;
			codeStastic[7] = codeStastic[7];
			
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStastic[7]); //先清除原来的投注号码的倍数
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put(codeStastic[7],codeStastic[2]);
		}else{
			cqsyxwPage.lotteryParam.currentLotteryTotalCount++;  //投注数目递增
			//将所选号码和玩法的映射放置map中
			var lotteryKindMap =  new JS_OBJECT_MAP();
			lotteryKindMap.put(cqsyxwPage.lotteryParam.currentKindKey,cqsyxwPage.lotteryParam.codes);
			lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_"+cqsyxwPage.lotteryParam.currentLotteryTotalCount,lotteryKindMap);
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_"+cqsyxwPage.lotteryParam.currentLotteryTotalCount,lotteryCommonPage.lotteryParam.beishu);
			
			//进入投注池当中
			var codeRecord = new Array();
			codeRecord.push(cqsyxwPage.lotteryParam.currentLotteryPrice);
			codeRecord.push(cqsyxwPage.lotteryParam.currentLotteryCount);
			codeRecord.push(lotteryCommonPage.lotteryParam.beishu);
			codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu));
			codeRecord.push(lotteryCommonPage.lottertyDataDeal.lotteryKindMap.get(cqsyxwPage.lotteryParam.currentKindKey));
			codeRecord.push(cqsyxwPage.lotteryParam.codes);
			codeRecord.push(cqsyxwPage.param.kindNameType +"_"+cqsyxwPage.lotteryParam.currentKindKey); //标识玩法
			codeRecord.push("lottery_id_"+cqsyxwPage.lotteryParam.currentLotteryTotalCount); //标识该条记录的lotteryMap
			cqsyxwPage.lotteryParam.codesStastic.push(codeRecord);			
		}
	}
	
	//选好号码统计金额注数
	lotteryCommonPage.codeStastics(); 
	return true;
};



/**
 * 根据彩种玩法,获取所选的号码
 * @param kindKey
 */
CqsyxwPage.prototype.getCodesByKindKey = function(){
    var codes = "";
    var lotteryCount = 0;
    //五星复式校验
    if(cqsyxwPage.lotteryParam.currentKindKey == 'XEQEZXFS'
		|| cqsyxwPage.lotteryParam.currentKindKey == 'XSQSZXFS'){   
	   
		var numArrays = new Array();
        //获取 选择的号码
		$(".child-content .content").each(function(i){
			var lis = $(this).children();
			var numsStr = "";
			for(var i = 0; i < lis.length; i++){
				var li = lis[i];
				var aNum = $(li);
				if($(aNum).hasClass('on')){
					numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT;
				}
			}
			//去除最后一个分隔符
			if(numsStr.substring(numsStr.length - 1, numsStr.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
				numsStr = numsStr.substring(0, numsStr.length -1);
			}
			if(numsStr != ""){
				numArrays.push(numsStr);
			}
		});
		
		if(cqsyxwPage.lotteryParam.currentKindKey == 'XEQEZXFS'){
    		var totalCount = 0;
			if(numArrays.length == 2){ //总共只有两位数
			   var num1Arrays = numArrays[0].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
			   var num2Arrays = numArrays[1].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
               for(var i = 0; i < num1Arrays.length; i++){
            	   for(var j = 0; j < num2Arrays.length; j++){
            		   if(num1Arrays[i] != num2Arrays[j]){
            			   totalCount++;
            		   }
            	   }
               }				   
			}else{
				cqsyxwPage.lotteryParam.currentLotteryCount = 0;//此时未选注
				cqsyxwPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}

			lotteryCount = totalCount;
			if(lotteryCount == 0){  //投注数目为0
			    cqsyxwPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				cqsyxwPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
			//统计号码
			for(var i = 0; i < numArrays.length; i++){
				var numArray = numArrays[i];
				codes = codes + numArray + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
			}
			codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT
			 			  + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT
			 			  + lotteryCommonPage.lotteryParam.CODE_REPLACE;
			
		}else if(cqsyxwPage.lotteryParam.currentKindKey == 'XSQSZXFS'){
    		var totalCount = 0;
			if(numArrays.length == 3){ //总共只有两位数
			   var num1Arrays = numArrays[0].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
			   var num2Arrays = numArrays[1].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
			   var num3Arrays = numArrays[2].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
               for(var i = 0; i < num1Arrays.length; i++){
            	   for(var j = 0; j < num2Arrays.length; j++){
                	   for(var z = 0; z < num3Arrays.length; z++){
                		   if(num1Arrays[i] != num2Arrays[j] && num2Arrays[j] != num3Arrays[z] && num2Arrays[i] != num3Arrays[z]){
                			   totalCount++;
                		   }              		   
                	   }
            	   }
               }				   
			}else{
				cqsyxwPage.lotteryParam.currentLotteryCount = 0;//此时未选注
				cqsyxwPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}

			lotteryCount = totalCount;
			if(lotteryCount == 0){  //投注数目为0
			    cqsyxwPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				cqsyxwPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
			//统计号码
			for(var i = 0; i < numArrays.length; i++){
				var numArray = numArrays[i];
				codes = codes + numArray + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
			}
			codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT
			 			  + lotteryCommonPage.lotteryParam.CODE_REPLACE;
		}else{
		  showTip("未知参数1.");	
		}
	}else if(cqsyxwPage.lotteryParam.currentKindKey == 'XYQSYMBDW'
		|| cqsyxwPage.lotteryParam.currentKindKey == 'XYRXYZYFS'
	    || cqsyxwPage.lotteryParam.currentKindKey == 'XEQEZXFS_G'
	    || cqsyxwPage.lotteryParam.currentKindKey == 'XERXEZEFS'
	    || cqsyxwPage.lotteryParam.currentKindKey == 'XSQSZXFS_G'
	    || cqsyxwPage.lotteryParam.currentKindKey == 'XSRXSZSFS'
	    || cqsyxwPage.lotteryParam.currentKindKey == 'XSHRXSZSFS'
	    || cqsyxwPage.lotteryParam.currentKindKey == 'XWRXWZWFS'
	    || cqsyxwPage.lotteryParam.currentKindKey == 'XLRXLZWFS'
	    || cqsyxwPage.lotteryParam.currentKindKey == 'XQRXQZWFS'
	    || cqsyxwPage.lotteryParam.currentKindKey == 'XBRXBZWFS'
	    || cqsyxwPage.lotteryParam.currentKindKey == 'QWDDS'
	    || cqsyxwPage.lotteryParam.currentKindKey == 'QWCZW'){    
		var numsStr = "";
		
		if(cqsyxwPage.lotteryParam.currentKindKey != 'QWDDS'){
			$(".child-content .content").each(function(){
				
				var aNums = $(this).children();
				for(var i = 0; i < aNums.length; i++){
					var aNum = aNums[i];
					if($(aNum).hasClass('on')){
						numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
					}
				}
			});
		}else{
			$(".child-content .content").each(function(){
				var aNums = $(this).children();
				for(var i = 0; i<aNums.length; i++){
					var aNum = aNums[i];
					if($(aNum).hasClass('on')){
						numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
					}
				}
				
			});
		}

		//去除最后一个分隔符
		if(numsStr.substring(numsStr.length - 1, numsStr.length) == lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT){
			numsStr = numsStr.substring(0, numsStr.length -1);
		}
		
		//空串拦截
		if(numsStr == ""){
		   cqsyxwPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
		   cqsyxwPage.lotteryParam.codes = null;  //号码置为空
		   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
		   return;
		}
		
		if(cqsyxwPage.lotteryParam.currentKindKey == 'XYQSYMBDW'
			    || cqsyxwPage.lotteryParam.currentKindKey == 'XYRXYZYFS'){
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 1){ //号码数目不足
			   cqsyxwPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   cqsyxwPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
			}			
			lotteryCount =  codeCount;   
		}else if(cqsyxwPage.lotteryParam.currentKindKey == 'XEQEZXFS_G'
			|| cqsyxwPage.lotteryParam.currentKindKey == 'XERXEZEFS'){
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 2){ //号码数目不足
			   cqsyxwPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   cqsyxwPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
		    }
			lotteryCount =  (codeCount * (codeCount - 1)) / (2 * 1);
		}else if(cqsyxwPage.lotteryParam.currentKindKey == 'XSQSZXFS_G'
			    || cqsyxwPage.lotteryParam.currentKindKey == 'XSRXSZSFS'){  
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 3){ //号码数目不足
			   cqsyxwPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   cqsyxwPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
		    }
			lotteryCount =  (codeCount * (codeCount - 1) * (codeCount - 2)) / (3 * 2 * 1);
		}else if(cqsyxwPage.lotteryParam.currentKindKey == 'XSHRXSZSFS'){
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 4){ //号码数目不足
			   cqsyxwPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   cqsyxwPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
		    }
			lotteryCount =  (codeCount * (codeCount - 1) * (codeCount - 2) * (codeCount - 3)) / (4 * 3 * 2 * 1);
		}else if(cqsyxwPage.lotteryParam.currentKindKey == 'XWRXWZWFS'){
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 5){ //号码数目不足
			   cqsyxwPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   cqsyxwPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
		    }
			lotteryCount =  (codeCount * (codeCount - 1) * (codeCount - 2) * (codeCount - 3) * (codeCount - 4)) / (5 * 4 * 3 * 2 * 1);
		}else if(cqsyxwPage.lotteryParam.currentKindKey == 'XLRXLZWFS'){
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 6){ //号码数目不足
			   cqsyxwPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   cqsyxwPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
		    }
			lotteryCount =  (codeCount * (codeCount - 1) * (codeCount - 2) * (codeCount - 3) * (codeCount - 4) * (codeCount - 5)) / (6 * 5 * 4 * 3 * 2 * 1);
		}else if(cqsyxwPage.lotteryParam.currentKindKey == 'XQRXQZWFS'){
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 7){ //号码数目不足
			   cqsyxwPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   cqsyxwPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
		    }
			lotteryCount =  (codeCount * (codeCount - 1) * (codeCount - 2) * (codeCount - 3) * (codeCount - 4) * (codeCount - 5) * (codeCount - 6)) / (7 * 6 * 5 * 4 * 3 * 2 * 1);
		}else if(cqsyxwPage.lotteryParam.currentKindKey == 'XBRXBZWFS'){
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 8){ //号码数目不足
			   cqsyxwPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   cqsyxwPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
		    }
			lotteryCount =  (codeCount * (codeCount - 1) * (codeCount - 2) * (codeCount - 3) * (codeCount - 4) * (codeCount - 5) * (codeCount - 6) * (codeCount - 7)) / (8 * 7 * 6 * 5 * 4 * 3 * 2 * 1);
		}else if(cqsyxwPage.lotteryParam.currentKindKey == 'QWDDS'){
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 1){ //号码数目不足
			   cqsyxwPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   cqsyxwPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
			}			
			lotteryCount =  codeCount; 
		}else if(cqsyxwPage.lotteryParam.currentKindKey == 'QWCZW'){
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 1){ //号码数目不足
			   cqsyxwPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   cqsyxwPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
			}			
			lotteryCount =  codeCount; 
		}else{
			showTip("未知参数2.");	
		}
		codes = numsStr;//记录投注号码			
	}else{
		lotteryCount = 0;
		codes = null;
		showTip("玩法参数传递不合法.");
	}
	
	//存储当前统计的投注数目
	cqsyxwPage.lotteryParam.currentLotteryCount = lotteryCount;  
	//当前投注号码
	cqsyxwPage.lotteryParam.codes = codes;
	cqsyxwPage.lotteryParam.codesStr = codes;
	//统计当前投注数目和价格
	lotteryCommonPage.currentCodesStastic();
	
};




/**
 * 号码拼接
 * @param num
 */
CqsyxwPage.prototype.codeStastics = function(codeStastic,index){
	 
	 var codeStr = "";
	 var codeDesc = codeStastic[5]; 
	 codeDesc = codeDesc.toString();
	 var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
	 if(kindKey == "QWDDS"){
		var codeDesc = codeStastic[5];
		codeDesc = codeDesc.replaceAll("01","5单0双");
		codeDesc = codeDesc.replaceAll("02","4单1双");
		codeDesc = codeDesc.replaceAll("03","3单2双");
		codeDesc = codeDesc.replaceAll("04","2单3双");
		codeDesc = codeDesc.replaceAll("05","1单4双");
		codeDesc = codeDesc.replaceAll("06","0单5双");
	}
	 if(codeDesc.length < 15){
		 codeStr += "<span>"+codeDesc.replace(/\&/g," ")+"</span>";
	 }else{
		 codeStr +=  "<span>" + codeDesc.substring(0,15).replace(/\&/g," ") + "...</span>";
		 codeStr += "<a onclick='event.stopPropagation();gdsyxwPage.showDsMsgDialog(this,"+index+")'>详情</a>";
		 codeStr += "</span>";
	 }
	 
	 var str = "";
	 //单式不需要更新操作
     var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
     if(kindKey == 'XYRXYZYDS' || kindKey == 'XEQEZXDS' || kindKey == 'XEQEZXDS_G' || kindKey == 'XERXEZEDS'
			|| kindKey == 'XSQSZXDS' || kindKey == "XSQSZXDS_G" || kindKey == "XSRXSZSDS"
			|| kindKey == 'XSHRXSZSDS' || kindKey == "XWRXWZWDS" || kindKey == "XLRXLZWDS"
			|| kindKey == "XQRXQZWDS" || kindKey == "XBRXBZWDS"){
		 str += "<li>";
	 }else{
		 str += "<li onclick='lotteryCommonPage.updateCodeStastic(this,"+index+")'>";
	 }
	 str += "<div class='li-child li-title'>" + codeStastic[4] + codeStr + "</div>";
	 str += "<div class='li-child li-money'>¥"+ codeStastic[0] + "元</div>";
	 str += "<div class='li-child li-strip'>" + codeStastic[1] + "注</div>";
	 str += "<div class='li-child li-multiple'>" + codeStastic[2] + "倍</div>";
	 str += "<div class='li-child li-addmoney'>" + codeStastic[3] + "元</div>";
	 str += "<div class='li-child li-close' data-value='"+index+"' name='deleteCurrentCodeStastic'><img src='"+ contextPath +"/front/images/lottery/close.png' /></div>" 
	 str += "</li>";
	 return str;
};

/**
 * 展现投注号码
 */
CqsyxwPage.prototype.showDsMsgDialog = function(obj,index){
	var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[index];
	var kindKeyTop = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
	
	$("#kindPlayDes").text(codeStastic[4]);
	if(kindKeyTop == "XYQSYMBDW"){     
		$("#kindPlayPositionDes").text("位置：第一位,第二位,第三位");
	}else if(kindKeyTop == "XYRXYZYFS" || kindKeyTop == "XYRXYZYDS" || kindKeyTop == "XERXEZEFS" || kindKeyTop == "XERXEZEDS"
		 || kindKeyTop == "XSRXSZSFS" || kindKeyTop == "XSRXSZSDS" || kindKeyTop == "XSHRXSZSFS" || kindKeyTop == "XSHRXSZSDS"
		 || kindKeyTop == "XWRXWZWFS" || kindKeyTop == "XWRXWZWDS" || kindKeyTop == "XLRXLZWFS" || kindKeyTop == "XLRXLZWDS"
		 || kindKeyTop == "XQRXQZWFS" || kindKeyTop == "XQRXQZWDS" || kindKeyTop == "XBRXBZWFS" || kindKeyTop == "XBRXBZWDS"
		 || kindKeyTop == "QWDDS" || kindKeyTop == "QWCZW"){
		$("#kindPlayPositionDes").text("位置：第一位,第二位,第三位,第四位,第五位");
	}else if(kindKeyTop == "XEQEZXFS" || kindKeyTop == "XEQEZXDS" || kindKeyTop == "XEQEZXFS_G" || kindKeyTop == "XEQEZXDS_G"){
		$("#kindPlayPositionDes").text("位置：第一位,第二位");
	}else if(kindKeyTop == "XSQSZXFS" || kindKeyTop == "XSQSZXDS" || kindKeyTop == "XSQSZXFS_G" || kindKeyTop == "XSQSZXDS_G"){
		$("#kindPlayPositionDes").text("位置：第一位,第二位,第三位");
	}else{
		alert("该单式玩法的位置还没配置.");
		return;
	}

	
	var modelTxt = $("#returnPercent").text();
	if(lotteryCommonPage.lotteryParam.awardModel == 'YUAN'){
		$("#kindPlayModel").text("元模式，" + modelTxt);
	}else if(lotteryCommonPage.lotteryParam.awardModel == 'JIAO'){
		$("#kindPlayModel").text("角模式，" + modelTxt);
	}else if(lotteryCommonPage.lotteryParam.awardModel == 'FEN'){
		$("#kindPlayModel").text("分模式，" + modelTxt);
	}else if(lotteryCommonPage.lotteryParam.awardModel == 'LI'){
		$("#kindPlayModel").text("厘模式，" + modelTxt);
	}else{
      showTip("该投注模式不合法.");
	}

	$("#kindPlayPriceMsg").text(codeStastic[1] + "注，" + lotteryCommonPage.lotteryParam.beishu + "倍, 共计" + codeStastic[0] + "元");
	$("#kindPlayCodeDes").val(codeStastic[5].replace(/\&/g," "));
	
	if(kindKeyTop == "QWDDS"){
		 var codeDesc = codeStastic[5];
		 codeDesc = codeDesc.replaceAll("01","5单0双");
		 codeDesc = codeDesc.replaceAll("02","4单1双");
		 codeDesc = codeDesc.replaceAll("03","3单2双");
		 codeDesc = codeDesc.replaceAll("04","2单3双");
		 codeDesc = codeDesc.replaceAll("05","1单4双");
		 codeDesc = codeDesc.replaceAll("06","0单5双");
		 $("#kindPlayCodeDes").val(codeDesc);
	}else{
		$("#kindPlayCodeDes").val(codeStastic[5]);
	}
	 
	//展示单式投注号码对话框
	$("#dsContentShowDialog").stop(false,true).delay(200).fadeIn(500);
	$(".alert-msg-bg").stop(false,true).fadeIn(500);
};

/**
 * 获取当前开奖号码的组态显示 单双 中位
 */
CqsyxwPage.prototype.getLotteryCodeStatus = function(codeStr){
	//控制显示当前号码的形态
    var codeArray = codeStr.split(",");
    var code1 = codeArray[0];
    var code2 = codeArray[1];
    var code3 = codeArray[2];
    var code4 = codeArray[3];
    var code5 = codeArray[4];
  
    //偶数数目
    var eventCount = 0;
    //奇数数目
    var unEventCount = 0;
    
    if(parseInt(code1) % 2 == 0){
    	eventCount++;
    }else{
    	unEventCount++;
    }
    
    if(parseInt(code2) % 2 == 0){
    	eventCount++;
    }else{
    	unEventCount++;
    }
    
    if(parseInt(code3) % 2 == 0){
    	eventCount++;
    }else{
    	unEventCount++;
    }
    
    if(parseInt(code4) % 2 == 0){
    	eventCount++;
    }else{
    	unEventCount++;
    }
    
    if(parseInt(code5) % 2 == 0){
    	eventCount++;
    }else{
    	unEventCount++;
    }
    
    var codeStatus = "";
    //中位计算
    var arrDemo = new Array();
    arrDemo[0] = parseInt(code1);
    arrDemo[1] = parseInt(code2);
    arrDemo[2] = parseInt(code3);
    arrDemo[3] = parseInt(code4);
    arrDemo[4] = parseInt(code5);
    arrDemo.sort(function(a,b){return a-b;});
    
    //中位号码
//    codeStatus ="中位："+(arrDemo[2] < 10?"0"+arrDemo[2]:arrDemo[2])+"<br/>";
    codeStatus +=unEventCount + "单" + eventCount + "双";
    
    return codeStatus;
};


/**
 * 显示当前投注号码
 * @param codeStastic
 * @param index
 */
CqsyxwPage.prototype.showCurrentPlayKindCode = function(codesArray){
	//判断是否有位数的概念
	var isBallNumMatch = true;
	var kindKey = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey;
	if(kindKey != 'XEQEZXFS' && kindKey != 'XSQSZXFS'){
		isBallNumMatch = false;
	}

	if(isBallNumMatch){
		for(var i = 0; i < codesArray.length; i++){
			if(codesArray[i].indexOf(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT) == -1){
	          	$("div[name='ballNum_"+i+"_match']").each(function(){
	                if($(this).attr("data-realvalue") == codesArray[i]){
	            		$(this).toggleClass("on");
	                }
	        	}); 
			}else{
				var codesWeiArray = codesArray[i].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
	            for(var j = 0; j < codesWeiArray.length; j++){
	            	var codeWei = codesWeiArray[j];
	    			if(codeWei != lotteryCommonPage.lotteryParam.CODE_REPLACE){
	    	          	$("div[name='ballNum_"+i+"_match']").each(function(){
	                        if($(this).attr("data-realvalue") == codeWei){
	                    		$(this).toggleClass("on");
	                        }
	                	});   				
	    			}
	            }
			}
		}		
	}else{
		for(var i = 0; i < codesArray.length; i++){
			if(codesArray[i].indexOf(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT) == -1){
	          	$("div[name='ballNum_0_match']").each(function(){
	                if($(this).attr("data-realvalue") == codesArray[i]){
	            		$(this).toggleClass("on");
	                }
	        	}); 
			}else{
				var codesWeiArray = codesArray[i].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
	            for(var j = 0; j < codesWeiArray.length; j++){
	            	var codeWei = codesWeiArray[j];
	    			if(codeWei != lotteryCommonPage.lotteryParam.CODE_REPLACE){
	    	          	$("div[name='ballNum_"+i+"_match']").each(function(){
	                        if($(this).attr("data-realvalue") == codeWei){
	                    		$(this).toggleClass("on");
	                        }
	                	});   				
	    			}
	            }
			}
		}
	}
};

/**
 * 机选
 */
CqsyxwPage.prototype.JxCodes = function(num){
	var kindKey = cqsyxwPage.lotteryParam.currentKindKey;
	var beishu = lotteryCommonPage.lotteryParam.beishu;
	var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
	
	var jxmax;
	var jxmin;
	var codes;
	
	if (kindKey == "QWDDS"){
		jxmin = 0;
		jxmax = 5;
	}else if(kindKey == "QWCZW"){
		jxmin = 0;
		jxmax = 6;
	}else{
		jxmin = 0;
		jxmax = 10;
	}


	for(var i = 0;i < num; i++){
	   //五星
		   if(kindKey == 'XYQSYMBDW' || kindKey == 'XYRXYZYFS' || kindKey == 'XYRXYZYDS'){
				var code1 = common_code_array[GetRndNum(jxmin,jxmax)];
				codes = code1;
		   }else if(kindKey == 'XEQEZXFS' || kindKey == 'XEQEZXDS'){
			    var positionArray =  GetRndNumByAppoint(jxmin,jxmax,2,0);
			    var code1 =  common_code_array[positionArray[0]];
			    var code2 =  common_code_array[positionArray[1]];
				codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		   }else if(kindKey == 'XEQEZXFS_G' || kindKey == 'XEQEZXDS_G' || kindKey == 'XERXEZEFS' || kindKey == 'XERXEZEDS'){
			    var positionArray =  GetRndNumByAppoint(jxmin,jxmax,2,0);
			    var code1 =  common_code_array[positionArray[0]];
			    var code2 =  common_code_array[positionArray[1]];
				codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code2;
		   }else if(kindKey == 'XSQSZXFS' || kindKey == 'XSQSZXDS'){
			    var positionArray =  GetRndNumByAppoint(jxmin,jxmax,3,0);
			    var code1 =  common_code_array[positionArray[0]];
			    var code2 =  common_code_array[positionArray[1]];
			    var code3 =  common_code_array[positionArray[2]];
				codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		   }else if(kindKey == 'XSQSZXFS_G' || kindKey == 'XSQSZXDS_G' || kindKey == 'XSRXSZSFS' || kindKey == 'XSRXSZSDS'){
			    var positionArray =  GetRndNumByAppoint(jxmin,jxmax,3,0);
			    var code1 =  common_code_array[positionArray[0]];
			    var code2 =  common_code_array[positionArray[1]];
			    var code3 =  common_code_array[positionArray[2]];
				codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code3;
		   }else if(kindKey == 'XSHRXSZSFS' || kindKey == 'XSHRXSZSDS'){
			    var positionArray =  GetRndNumByAppoint(jxmin,jxmax,4,0);
			    var code1 =  common_code_array[positionArray[0]];
			    var code2 =  common_code_array[positionArray[1]];
			    var code3 =  common_code_array[positionArray[2]];
			    var code4 =  common_code_array[positionArray[3]];
				codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code4;
		   }else if(kindKey == 'XWRXWZWFS' || kindKey == 'XWRXWZWDS'){
			    var positionArray =  GetRndNumByAppoint(jxmin,jxmax,5,0);
			    var code1 =  common_code_array[positionArray[0]];
			    var code2 =  common_code_array[positionArray[1]];
			    var code3 =  common_code_array[positionArray[2]];
			    var code4 =  common_code_array[positionArray[3]];
			    var code5 =  common_code_array[positionArray[4]];
				codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code4 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code5;
		   }else if(kindKey == 'XLRXLZWFS' || kindKey == 'XLRXLZWDS'){
			    var positionArray =  GetRndNumByAppoint(jxmin,jxmax,6,0);
			    var code1 =  common_code_array[positionArray[0]];
			    var code2 =  common_code_array[positionArray[1]];
			    var code3 =  common_code_array[positionArray[2]];
			    var code4 =  common_code_array[positionArray[3]];
			    var code5 =  common_code_array[positionArray[4]];
			    var code6 =  common_code_array[positionArray[5]];
				codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code4 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code5 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code6;
		   }else if(kindKey == 'XQRXQZWFS' || kindKey == 'XQRXQZWDS'){
			    var positionArray =  GetRndNumByAppoint(jxmin,jxmax,7,0);
			    var code1 =  common_code_array[positionArray[0]];
			    var code2 =  common_code_array[positionArray[1]];
			    var code3 =  common_code_array[positionArray[2]];
			    var code4 =  common_code_array[positionArray[3]];
			    var code5 =  common_code_array[positionArray[4]];
			    var code6 =  common_code_array[positionArray[5]];
			    var code7 =  common_code_array[positionArray[6]];
				codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code4 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code5 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code6 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code7;
		   }else if(kindKey == 'XBRXBZWFS' || kindKey == 'XBRXBZWDS'){
			    var positionArray =  GetRndNumByAppoint(jxmin,jxmax,8,0);
			    var code1 =  common_code_array[positionArray[0]];
			    var code2 =  common_code_array[positionArray[1]];
			    var code3 =  common_code_array[positionArray[2]];
			    var code4 =  common_code_array[positionArray[3]];
			    var code5 =  common_code_array[positionArray[4]];
			    var code6 =  common_code_array[positionArray[5]];
			    var code7 =  common_code_array[positionArray[6]];
			    var code8 =  common_code_array[positionArray[7]];
				codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code4 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code5 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code6 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code7 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
				        code8;
		   }else if(kindKey == 'QWDDS'){
			    var code1 =  qwdds_code_array[GetRndNum(jxmin,jxmax)];
				codes = code1;
		   }else if(kindKey == 'QWCZW'){
			    var code1 =  qwczw_code_array[GetRndNum(jxmin,jxmax)];
				codes = code1;
		   }else{
			   showTip("该玩法的但是未配置.");
		   }
	   
		//先校验投注蓝中,是否有该投注号码
		var isYetHave = false;
		var yetIndex = null;
		var codeStastics = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic;
		for(var j = codeStastics.length - 1; j >= 0 ; j--){
			 var codeStastic = codeStastics[j];
		     var kindKeyTemp = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
			 if(kindKeyTemp == cqsyxwPage.lotteryParam.currentKindKey && codeStastic[5].toString() == codes){
				 isYetHave = true;
				 yetIndex = j;
				 break;
			 }
		}
		
		var currentLotteryWin = null;
		if(cqsyxwPage.lotteryParam.currentKindKey == "QWDDS"){
			if(codes.indexOf("06") != -1){
				currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(cqsyxwPage.param.kindNameType +"_"+cqsyxwPage.lotteryParam.currentKindKey);
			}else if(codes.indexOf("01") != -1){
				currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(cqsyxwPage.param.kindNameType +"_"+cqsyxwPage.lotteryParam.currentKindKey+"_2");
			}else if(codes.indexOf("05") != -1){
				currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(cqsyxwPage.param.kindNameType +"_"+cqsyxwPage.lotteryParam.currentKindKey+"_3");
			}else if(codes.indexOf("02") != -1){
				currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(cqsyxwPage.param.kindNameType +"_"+cqsyxwPage.lotteryParam.currentKindKey+"_4");
			}else if(codes.indexOf("04") != -1){
				currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(cqsyxwPage.param.kindNameType +"_"+cqsyxwPage.lotteryParam.currentKindKey+"_5");
			}else if(codes.indexOf("03") != -1){
				currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(cqsyxwPage.param.kindNameType +"_"+cqsyxwPage.lotteryParam.currentKindKey+"_6");
			}else{
				showTip("QWDDS未配置该号码的奖金");
			}
		}else if(cqsyxwPage.lotteryParam.currentKindKey == "QWCZW"){
			if(codes.indexOf("03") != -1 || codes.indexOf("09") != -1){
				currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(cqsyxwPage.param.kindNameType +"_"+cqsyxwPage.lotteryParam.currentKindKey);
			}else if(codes.indexOf("04") != -1 || codes.indexOf("08") != -1){
				currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(cqsyxwPage.param.kindNameType +"_"+cqsyxwPage.lotteryParam.currentKindKey+"_2");
			}else if(codes.indexOf("05") != -1 || codes.indexOf("07") != -1){
				currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(cqsyxwPage.param.kindNameType +"_"+cqsyxwPage.lotteryParam.currentKindKey+"_3");
			}else if(codes.indexOf("06") != -1){
				currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(cqsyxwPage.param.kindNameType +"_"+cqsyxwPage.lotteryParam.currentKindKey+"_4");
			}else{
				showTip("QWCZW未配置该号码的奖金");
			}
		}else{
			currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(cqsyxwPage.param.kindNameType +"_"+cqsyxwPage.lotteryParam.currentKindKey);
		}
		
		if(isYetHave && yetIndex != null){
			frontCommonPage.showKindlyReminder("您选择的号码在号码篮已存在，将直接进行倍数累加!");
			var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[yetIndex];
			cqsyxwPage.lotteryParam.currentLotteryPrice = lotteryCommonPage.getAwardByLotteryModel(1 * lotteryCommonPage.currentLotteryKindInstance.param.unitPrice * beishu);
			codeStastic[0] = (parseFloat(codeStastic[0]) + parseFloat(cqsyxwPage.lotteryParam.currentLotteryPrice)).toFixed(lotteryCommonPage.param.toFixedValue);
			codeStastic[1] = codeStastic[1];
			codeStastic[2] = parseInt(codeStastic[2]) + lotteryCommonPage.lotteryParam.beishu;
			codeStastic[3] = (parseFloat(codeStastic[3]) + parseFloat(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu))).toFixed(lotteryCommonPage.param.toFixedValue);
			codeStastic[4] = cqsyxwPage.lotteryParam.lotteryKindMap.get(cqsyxwPage.lotteryParam.currentKindKey);
			codeStastic[5] = codes;
			codeStastic[6] = cqsyxwPage.param.kindNameType +"_"+cqsyxwPage.lotteryParam.currentKindKey;
			codeStastic[7] = codeStastic[7];
			
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStastic[7]); //先清除原来的投注号码的倍数
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put(codeStastic[7],codeStastic[2]);
		}else{
		    cqsyxwPage.lotteryParam.currentLotteryTotalCount++;
			//将所选号码和玩法的映射放置map中
			var lotteryKindMap =  new JS_OBJECT_MAP();
			lotteryKindMap.put(cqsyxwPage.lotteryParam.currentKindKey,codes);
			lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_"+cqsyxwPage.lotteryParam.currentLotteryTotalCount,lotteryKindMap);
			//投注倍数存储
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_"+cqsyxwPage.lotteryParam.currentLotteryTotalCount,beishu);

			//进入投注池当中,五星默认是一注
			var codeRecord = new Array();
			codeRecord.push(lotteryCommonPage.getAwardByLotteryModel(1 * lotteryCommonPage.currentLotteryKindInstance.param.unitPrice * beishu));
			codeRecord.push(1);
			codeRecord.push(beishu);
			codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * beishu));
			codeRecord.push(cqsyxwPage.lotteryParam.lotteryKindMap.get(cqsyxwPage.lotteryParam.currentKindKey));
			codeRecord.push(codes);
			codeRecord.push(cqsyxwPage.param.kindNameType +"_"+cqsyxwPage.lotteryParam.currentKindKey); //标识玩法
			codeRecord.push("lottery_id_"+cqsyxwPage.lotteryParam.currentLotteryTotalCount); //标识该条记录的lotteryMap
			cqsyxwPage.lotteryParam.codesStastic.push(codeRecord); 
			codes = "";
		}
	};
	
	lotteryCommonPage.codeStastics();  //显示随机投注结果
};

/**
 * 单式投注数目控制
 * @param eventType
 */
CqsyxwPage.prototype.lotteryCountStasticForDs = function(contentLength){
	cqsyxwPage.lotteryParam.currentLotteryCount = contentLength;	
};

/**
 * 时时彩彩种格式化
 * @param isCut 格式化之后对多余的数字是否截取
 */
CqsyxwPage.prototype.formatNum = function(isCut){
	var kindKey = cqsyxwPage.lotteryParam.currentKindKey;
	var pastecontent = $("#lr_editor").val();
	if (pastecontent == ""){
		$("#lr_editor").val("");
		   //设置当前投注数目或者投注价格
//		$('#J-balls-statistics-lotteryNum').text("0");  //显示投注
//		$('#J-balls-statistics-amount').text("0.000");
		return false;
	}
	
	pastecontent=pastecontent.replace(/[^\d]/g,'');
	if (pastecontent == ""){
		$("#lr_editor").val("");
		//设置当前投注数目或者投注价格
		/*$('#J-balls-statistics-lotteryNum').text("0");  //显示投注
		$('#J-balls-statistics-amount').text(parseFloat("0").toFixed(lotteryCommonPage.param.toFixedValue));
		$('#J-balls-statistics-code').text("(投注内容)");*/
		return false;
	}
	var len= pastecontent.length;
	var num="";
	var numtxt="";
	var n=0;
	var maxnum=1;
	if(kindKey == 'XYRXYZYDS'){     
		maxnum=1;
	}else if(kindKey == 'XEQEZXDS' || kindKey == 'XEQEZXDS_G' || kindKey == 'XERXEZEDS'){    
		maxnum=2;
	}else if(kindKey == 'XSQSZXDS' || kindKey == 'XSQSZXDS_G' || kindKey == 'XSRXSZSDS'){
		maxnum=3;
	}else if(kindKey == 'XSHRXSZSDS'){
		maxnum=4;
	}else if(kindKey == 'XWRXWZWDS'){
		maxnum=5;
	}else if(kindKey == 'XLRXLZWDS'){
		maxnum=6;
	}else if(kindKey == 'XQRXQZWDS'){
		maxnum=7;
	}else if(kindKey == 'XBRXBZWDS'){
		maxnum=8;
	}else{
		showTip("未配置该单式的控制.");
	}
	//下方显示的号码
	var numsStr = "";
	maxnum = maxnum * 2;
	for(var i=0; i<len; i = i + 2){
		if(i%maxnum==0){
			n=1;
		}
		else{
			n = n + 2;
		}
		var currentChar = pastecontent.substr(i,2);
		//截取处理的
		if(isCut) {
			if(n<maxnum-1){
				num = num + currentChar+",";
				numsStr = numsStr + currentChar+",";
			}
			else{
				num = num + currentChar;
				numtxt = numtxt + num + "\n";
				num = "";
				numsStr = numsStr + currentChar + " ";
			}
		} else {
			//不需要截取多余处理的
			if(n<maxnum-1){
				numtxt = numtxt + currentChar+",";
				numsStr = numsStr + currentChar+",";
			}
			else{
				numtxt = numtxt + currentChar + "\n";
				numsStr = numsStr + currentChar + " ";
			}
		}
	}
	numsStr = numsStr.substring(0, numsStr.length - 1);
	//如果格式化截取多余的  内容为空，显示号码也为空
	if(isCut) {
		if(numtxt == "" || numtxt.length == 0) {
			numsStr = "";
		}
	} else {
		numtxt = numtxt.substring(0, numtxt.length - 1);
	}
	//处理投注下方显示的号码
	cqsyxwPage.lotteryParam.codesStr = numsStr;
	$("#lr_editor").val(numtxt);
	return true;
};

/**
 * 单式的选号投注
 */
CqsyxwPage.prototype.singleLotteryNum = function(){
	var editorVal = $("#lr_editor").val();
	if(editorVal.indexOf("说明") != -1){
		showTip("请先填写投注号码");
		//		alert("请先填写投注号码");
		return false;
	}
	
	lotteryCommonPage.dealForDsFormat();  //首先格式化,避免直接点击选号码的事件
	var pastecontent = $("#lr_editor").val();
	if(pastecontent == ""){
		showTip("请先填写投注号码");
		//		alert("请先填写投注号码");
		return false;
	}
	pastecontent = pastecontent.replace(/\r\n/g,"$");
	pastecontent = pastecontent.replace(/\n/g,"$");
	pastecontent = pastecontent.replace(/\s/g,"");
	pastecontent = pastecontent.replace(/(\$)\1+/g, '$1');
	if(pastecontent.substr(pastecontent.length-1,1)=="$")
	{
		pastecontent = pastecontent.substr(0,pastecontent.length-1);
	}
	var pastecontentArr = pastecontent.split("$");
	if(pastecontentArr.length > 2000){
		showTip("最多支持2000注单式内容，请调整！");
        return;
	}
	
	var kindKey = cqsyxwPage.lotteryParam.currentKindKey;
	var errzhushu = "";
	var errzhushumsg = "[格式错误]"
	var reg1;
	
	if(kindKey == 'XYRXYZYDS'){   //选一
		reg1 = /^(?:(0[1-9]|1[01]))$/;	
	}else if(kindKey == 'XEQEZXDS' || kindKey == 'XEQEZXDS_G' || kindKey == 'XERXEZEDS'){   //选二
		reg1 = /^(?:(0[1-9]|1[01])[,\|]){1}(?:0[1-9]|1[01])$/;
	}else if(kindKey == 'XSQSZXDS' || kindKey == 'XSQSZXDS_G' || kindKey == 'XSRXSZSDS'){  //选三
		reg1 = /^(?:(0[1-9]|1[01])[,\|]){2}(?:0[1-9]|1[01])$/;
	}else if(kindKey == 'XSHRXSZSDS'){  //选四
		reg1 = /^(?:(0[1-9]|1[01])[,\|]){3}(?:0[1-9]|1[01])$/;
	}else if(kindKey == 'XWRXWZWDS'){  //选五
		reg1 = /^(?:(0[1-9]|1[01])[,\|]){4}(?:0[1-9]|1[01])$/;
	}else if(kindKey == 'XLRXLZWDS'){  //选六
		reg1 = /^(?:(0[1-9]|1[01])[,\|]){5}(?:0[1-9]|1[01])$/;
	}else if(kindKey == 'XQRXQZWDS'){  //选七
		reg1 = /^(?:(0[1-9]|1[01])[,\|]){6}(?:0[1-9]|1[01])$/;
	}else if(kindKey == 'XBRXBZWDS'){  //选八
		reg1 = /^(?:(0[1-9]|1[01])[,\|]){7}(?:0[1-9]|1[01])$/;
	}else{
		showTip("未配置该类型的单式");
	}
	
	for(var i=0;i<pastecontentArr.length;i++){
		var value1 = pastecontentArr[i];
		if(!reg1.test(value1)){
			if(errzhushu==""){
				errzhushu = "第"+(i+1).toString()+"注";
			}else{
				errzhushu = errzhushu+" <br/>"+"第"+(i+1).toString()+"注";
			}
			continue;
		}

		if(kindKey == 'XYRXYZYDS' || kindKey == 'XEQEZXDS_G' || kindKey == 'XEQEZXDS' || kindKey == 'XERXEZEDS' || kindKey == 'XSQSZXDS' || kindKey == 'XSQSZXDS_G' || kindKey == 'XSRXSZSDS' || kindKey == 'XSHRXSZSDS' || kindKey == 'XWRXWZWDS'
			|| kindKey == 'XLRXLZWDS' || kindKey == 'XQRXQZWDS' || kindKey == 'XBRXBZWDS'){   //不可有重复的号码
			if(lotteryCommonPage.distinct(value1)){
				if(errzhushu==""){
					errzhushu = "第"+(i+1).toString()+"注,存在重复号码";
				}else{
					errzhushu = errzhushu+"第"+(i+1).toString()+"注,存在重复号码";
				}
				continue;
			}
		}
	}
	
	//显示错误注号码
	if(errzhushu!=""){
		showTip(errzhushu+"  "+"号码有误，请核对！");
//		alert(errzhushu+"  "+"号码有误，请核对！");
		return false;
	}
	
	var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
	var currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get(cqsyxwPage.param.kindNameType +"_"+cqsyxwPage.lotteryParam.currentKindKey);
	//var beishu = $('#beishu').attr('data-value');
	lotteryCommonPage.lotteryParam.beishu = parseInt(lotteryCommonPage.lotteryParam.beishu);
	cqsyxwPage.lotteryParam.currentLotteryCount = 1;
	
	for(var i=0;i<pastecontentArr.length;i++){
		var value = pastecontentArr[i];	
		var text ="";
		if(kindKey == 'XEQEZXDS'){
			value = value + ",-,-,-";
		}else if(kindKey == 'XSQSZXDS' ){//四星星直选单式
			value = value + ",-,-";
		}else{
			value = value;
		}
		
		cqsyxwPage.lotteryParam.currentLotteryTotalCount++;
		lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryCount = 1;
		cqsyxwPage.lotteryParam.codes = value;
			
		//将所选号码和玩法的映射放置map中
		var lotteryKindMap =  new JS_OBJECT_MAP();
		lotteryKindMap.put(cqsyxwPage.lotteryParam.currentKindKey,cqsyxwPage.lotteryParam.codes);
		lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_"+cqsyxwPage.lotteryParam.currentLotteryTotalCount,lotteryKindMap);
		//投注倍数存储
		lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_"+cqsyxwPage.lotteryParam.currentLotteryTotalCount,lotteryCommonPage.lotteryParam.beishu);

		//进入投注池当中
		var codeRecord = new Array();
		codeRecord.push(parseFloat(lotteryCommonPage.getAwardByLotteryModel(cqsyxwPage.lotteryParam.currentLotteryCount * cqsyxwPage.param.unitPrice * lotteryCommonPage.lotteryParam.beishu)).toFixed(lotteryCommonPage.param.toFixedValue));
		codeRecord.push(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryCount);
		codeRecord.push(lotteryCommonPage.lotteryParam.beishu);
		codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu));
		codeRecord.push(lotteryCommonPage.lottertyDataDeal.lotteryKindMap.get(cqsyxwPage.lotteryParam.currentKindKey));
		codeRecord.push(cqsyxwPage.lotteryParam.codes);
		codeRecord.push(cqsyxwPage.param.kindNameType +"_"+cqsyxwPage.lotteryParam.currentKindKey); //标识玩法
		codeRecord.push("lottery_id_"+cqsyxwPage.lotteryParam.currentLotteryTotalCount); //标识该条记录的lotteryMap
		cqsyxwPage.lotteryParam.codesStastic.push(codeRecord);	
		cqsyxwPage.lotteryParam.codes = null;
		
	}
	//号码显示统计,由当前的倍数、奖金模式、投注模式决定
	lotteryCommonPage.codeStastics();
	//选好号码的结束事件
//	lotteryCommonPage.addCodeEnd(); 
	
	$("#J-add-order").removeAttr("disabled");
	$("#J-add-order").text("选好了");
	$("#lr_editor").val("");//清空注码
	
	return true;
};

/**
 * 初始化彩种信息
 */
CqsyxwPage.prototype.initLottery = function(){
	//先加载所有的玩法
    lotteryCommonPage.lottertyDataDeal.loadLotteryKindPlay();
    
    //取消追号处理
    lotteryCommonPage.lotteryParam.isZhuiCode = false;
    
    //设置玩法描述数据
    lotteryCommonPage.lottertyDataDeal.setKindPlayDes();
    
    //选择默认的玩法，默认是五星定位胆
    cqsyxwPage.lotteryParam.currentKindKey = "XWRXWZWFS";
	lotteryCommonPage.lottertyDataDeal.lotteryKindPlayChoose(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey);
	
	//动画效果
	animate_add(".main-head","fadeInDown",0);
	animate_add(".lottery-content","fadeInUp",0);
	animate_add(".lottery-bar .bar","fadeInUp",0);
	
    //添加星种选中事件
	$("#lotteryStarKindList .child").unbind("click").click(function(){
		var roleId = $(this).attr("data-role-id");
		//如果有包含玩法属性
		if(isNotEmpty(roleId) && lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey != roleId) {
			//清空当前选中号码
			lotteryCommonPage.clearCurrentCodes();
			lotteryCommonPage.lottertyDataDeal.lotteryKindPlayChoose(roleId);
		//加载星种玩法下拉
		} else {
			var kindPlayClass = $(this).attr("data-id");
			var parent=$(this).closest("body");
			parent.find(".lottery-classify2 .move-content .child").removeClass('color-red');
			$(this).addClass('color-red');
			parent.find(".lottery-classify2 .clist").removeClass('in').addClass('out');
			parent.find("."+kindPlayClass).addClass('in').removeClass('out');
		}
	});
    //添加玩法选中事件
	$("#lotteryKindPlayList .line-content .child").unbind("click").click(function(){
		var roleId = $(this).attr("data-role-id");
		//已经是当前玩法了，不需要处理
		if(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey == roleId) {
			return;
		}
		
	   //清空当前选中号码
	   lotteryCommonPage.clearCurrentCodes();
	   
	   //控制玩法的号码展示
	   lotteryCommonPage.lottertyDataDeal.lotteryKindPlayChoose(roleId);
	   
	   //隐藏下拉玩法列表
	   element_toggle(false,['.lottery-classify']);
	});
	
	//点击当前玩法显示下拉其他玩法
	$("#currentKindDiv").unbind("click").click(function(){
		var e = $(this);
		var ele = ".lottery-classify2";
		var parent=$(e).closest("body");
		var display=$(ele).css("display");
		var style="boolean";
		style=display=="none"?"true":"boolean";
		parent.find(".lottery-classify").not(ele).hide();
		element_toggle(style,[ele],parent);
		
		//触发星种下拉
		$("#lotteryStarKindList .child").each(function(){
			if($(this).hasClass('color-red')) {
				var kindPlayClass = $(this).attr("data-id");
				var parent=$(this).closest("body");
				parent.find(".lottery-classify2 .clist").removeClass('in').addClass('out');
				parent.find("."+kindPlayClass).addClass('in').removeClass('out');
				return false;
			}
		});
	});
	
	//绑定选择其他地区彩种事件
	$("#otherLotteryKind").unbind("click").click(function(){
		var ele = '.lottery-classify1';
		var parent=$(this).closest("body");
		var display=$(ele).css("display");
		var style="boolean";
		style=display=="none"?"true":"boolean";
		parent.find(".lottery-classify").not(ele).hide();
		element_toggle(style,[ele],parent);
	});
	
	//点击开奖号码显示开奖记录事件
	$(".lottery_run").unbind("click").click(function(){
		var parent=$("body");
		if(parent.find(".lottery-dynamic").hasClass('in')){
			parent.find(".lottery-dynamic").removeClass('in').addClass('out');
		}else{
			parent.find(".lottery-dynamic").removeClass('out').addClass('in');
		}
	});
	
	
	//倍数改变事件
	lotteryCommonPage.beishuEvent();
	
	//倍数减少事件
	$("#beishuSub").unbind("click").click(function(){
		var beishuStr = $('#beishu').val();
		var beishuTemp = parseInt(beishuStr);
		beishuTemp--;
		if(beishuTemp <= 0) {
			showTip("当前倍数已经是最低了");
			return;
		}
		$("#beishu").val(beishuTemp);
		lotteryCommonPage.beishuEventContent();
	});
	
	//倍数增加事件
	$("#beishuAdd").unbind("click").click(function(){
		var beishuStr = $('#beishu').val();
		var beishuTemp = parseInt(beishuStr);
		beishuTemp++;
		if(beishuTemp > 1000000) {
			showTip("倍数最高1000000");
			return;
		}
		$("#beishu").val(beishuTemp);
		lotteryCommonPage.beishuEventContent();
	});
	

	//确认投注-展示投注信息事件
	$("#showLotteryMsgButton").unbind("click").click(function(){
		//非追号类型 处理添加号码到投注池
		if(!lotteryCommonPage.lotteryParam.isZhuiCode) {
			lotteryCommonPage.currentLotteryKindInstance.userLotteryNumForAddOrder();
		}
		lotteryCommonPage.showLotteryMsg();
	});
	//关闭投注信息事件
    $("#lotteryOrderDialogCancelButton").unbind("click").click(function(){
    	lotteryCommonPage.hideLotteryMsg();
	});
    
    //关闭投注信息事件
    $(".close").unbind("click").click(function(){
    	lotteryCommonPage.hideLotteryMsg();
    });
    
	//确认投注-提交后台投注事件
	$("#lotteryOrderDialogSureButton").unbind("click").click(function(){
		lotteryCommonPage.userLottery();
	});
	
	//机选一注事件
	$("#randomOneBtn").unbind("click").click(function(){
		lotteryCommonPage.randomOneEvent();
	});
	
	//清空投注池号码
	$("#clearAllCodeBtn").unbind("click").click(function(){
		lotteryCommonPage.clearCodeStasticEvent();
		/*comfirm("提示","确认删除号码篮内全部内容吗?","取消",function(){},"确认",
				lotteryCommonPage.clearCodeStasticEvent);*/
		
	});	
	
	//点击追号取消追号事件
	$("#zhuiCodeButton").unbind("click").click(function(param,showParam){
		lotteryCommonPage.changeTozhuiCodeEvent();
	});	
	
	 
	//获取最新的开奖号码
	lotteryCommonPage.getLastLotteryCode();
	//获取当前彩种的最新期号和投注倒计时
	lotteryCommonPage.getActiveExpect();
	//加载今天和明天的期号
    lotteryCommonPage.getAllExpectsByTodayAndTomorrow();
	//近10期的开奖记录
	lotteryCommonPage.getNearestTenLotteryCode();
	//获取用户今日投注记录
	lotteryCommonPage.getTodayLotteryByUser();
	
	//每隔10秒钟请求一次获取最新的开奖号码
	if(lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JLFFC" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "HGFFC"){
		window.setInterval("lotteryCommonPage.getLastLotteryCode()",5000);  //分分彩读取最新的开奖号码
		//window.setInterval("lotteryCommonPage.getNearestTenLotteryCode()",5000);
		window.setInterval("lotteryCommonPage.getActiveExpect()",30000); //30秒重新读取服务器的时间
	}else{
		window.setInterval("lotteryCommonPage.getLastLotteryCode()",10000);  //其他彩种读取最新的开奖号码
		//window.setInterval("lotteryCommonPage.getNearestTenLotteryCode()",10000);
		window.setInterval("lotteryCommonPage.getActiveExpect()",60000); //1分钟重新读取服务器的时间
	}
};

$(document).ready(function() {
	
		//设置数据处理器  默认时时彩的
		lotteryCommonPage.lottertyDataDeal = syxwData;
		//将这个投注实例传至common js当中
		lotteryCommonPage.currentLotteryKindInstance = cqsyxwPage; 
		//设置投注的模式
		lotteryCommonPage.lotteryParam.modelValue = currentUserSyxwModel;
		lowestAwardModel = syxwLowestAwardModel;
		
		dwr.engine.setAsync(false);
		//获取当前彩种的所有奖金列表
		lotteryCommonPage.currentLotteryKindInstance.getLotteryWins(); 
		//获取当前投注模式  元角模式
		lotteryCommonPage.loadUserModelSet();
		dwr.engine.setAsync(true);
		setTimeout("cqsyxwPage.initLottery()", 100);
});