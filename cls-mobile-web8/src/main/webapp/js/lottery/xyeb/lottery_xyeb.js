function XyebPage(){}
var xyebPage = new XyebPage();
lotteryCommonPage.currentLotteryKindInstance = xyebPage; //将这个投注实例传至common js当中
var quickmoney = 0;
//基本参数
xyebPage.param = {
	unitPrice : 1,  //每注价格
	kindDes : "幸运28",
	kindName : 'XYEB',  //彩种
	kindNameType : 'XYEB', //大彩种拼写
	ommitData : null,  //遗漏数据
	hotColdData : null //冷热数据
};

//数据集合
xyebPage.showdatas = {
		codePlans : new Array(),
		subKindVOs : new Array()
}

//投注参数
xyebPage.lotteryParam = {
	lotteryKindMap : new JS_OBJECT_MAP(),
    currentKindKey : 'XYEB',    //当前所选择的玩法模式,默认是特码B
	currentLotteryCount : 0,       //当前投注数目
	currentLotteryPrice : 0,       //当前投注价格,按元来计算
	currentLotteryTotalCount : 0,  //当前总投注数目
	codes : null,                  //投注号码
	codesStastic : new Array()
};
$(document).ready(function() {
	
	$('.btn-submit').on('click',function(){
		/*$('.alert-msg-bg, .alert-xyeb-msg').show();*/
		$(".alert-xyeb-msg").stop(false,true).delay(200).fadeIn(500);
		$(".alert-msg-bg").stop(false,true).fadeIn(500);
	})
	
	// 特码、混合切换
	$('.move-content .child').click(function(){
		var index = $(this).index();
		$('#currentKindName').text($(this).text());
		$('.lottery-classify, .alert-bg').hide();
		$('.lottery-content-xyeb > .content-xyeb').eq(index).show().siblings().hide();
	})
	
	$(".lottery-child .child-content .mun2, .lottery-content-xyeb .content-xyeb .cols").click(function(){
		if($(".lottery-bar .additional").css('display') == "none") {
			$(".lottery-bar .additional").show();
			$('#shortcut-money').focus();
		}
		$(this).toggleClass('on');
		lotteryCommonPage.countLotteryNum();
	});
	//金额减少事件
	$("#beishuSub").unbind("click").click(function(){
		var beishuStr = $('#shortcut-money').val();
		 if(beishuStr == ""){
				showTip("请输入下注金额！");
				$("#shortcut-money").focus();
				return ;
			}
		  if(isNaN(beishuStr)){
		    	showTip("请输入数字！");
				$("#shortcut-money").focus();	
				return ;
		    }
		var beishuTemp = parseInt(beishuStr);
		beishuTemp--;
		if(beishuTemp <= 1) {
			showTip("最低下注金额2元");
			return;
		}
		$("#shortcut-money").val(beishuTemp);
		lotteryCommonPage.countLotteryNum();
	});
	
	//金额增加事件
	$("#beishuAdd").unbind("click").click(function(){
		var beishuStr = $('#shortcut-money').val();
	    if(beishuStr == ""){
			showTip("请输入下注金额！");
			$("#shortcut-money").focus();
			return ;
		}
	    if(isNaN(beishuStr)){
	    	showTip("请输入数字！");
			$("#shortcut-money").focus();	
			return ;
	    }
		var beishuTemp = parseInt(beishuStr);
		beishuTemp++;
		if(beishuTemp > 9999) {
			showTip("当前金额已经是最高了");
			return;
		}
		$("#shortcut-money").val(beishuTemp);
		lotteryCommonPage.countLotteryNum();
	});
	
	$("#shortcut-money").keyup(function(){
		var beishuStr = $('#shortcut-money').val();
		 if(beishuStr == ""){
				showTip("请输入下注金额！");
				$("#shortcut-money").focus();
				return ;
		 }
		  if(isNaN(beishuStr)){
		    	showTip("请输入数字！");
				$("#shortcut-money").focus();	
				return ;
		    }
		var beishuTemp = parseInt(beishuStr);
		 beishuTemp++;
		if(beishuTemp > 9999) {
			showTip("当前金额已经是最高了");
			$("#shortcut-money").val(9999);
			return;
		}
		if(beishuTemp < 2) {
			showTip("最低下注金额2元!");
			$("#shortcut-money").val(2);
			return;
		}
		quickmoney = beishuTemp;
		lotteryCommonPage.countLotteryNum();
	});
	
	//点击开奖号码显示开奖记录事件
	$(".lottery_run").unbind("click").click(function(){
		var parent=$("body");
		if(parent.find(".lottery-dynamic").hasClass('in')){
			parent.find(".lottery-dynamic").removeClass('in').addClass('out');
		}else{
			parent.find(".lottery-dynamic").removeClass('out').addClass('in');
		}
	});
	
	$(".btn-rule").click(function() {
		var tempWindow=window.open('','','top=100,left=100,width=1200,height=800');
		tempWindow.location.href = 'playrule_xyeb.html';  //对新打开的页面进行重定向
    });
	
	/*document.getElementById("codePlanList").style.display="none";
	$("#change-title").unbind("click").click(function(event){
		var value = document.getElementById("change-title").getAttribute("value");
		if(value=="0"){//投注记录>计划列表
			document.getElementById("change-title").setAttribute("value","1");
			$("#change-title").html("计划列表");
			document.getElementById("userTodayLotteryList").style.display="none";
			document.getElementById("codePlanList").style.display="block";
		}else if(value=="1"){//计划列表>投注记录
			document.getElementById("change-title").setAttribute("value","0");
			$("#change-title").html("投注记录");
			document.getElementById("codePlanList").style.display="none";
			document.getElementById("userTodayLotteryList").style.display="block";
		}
	});
	xyebPage.codePlanList(xyebPage.param);*/
});


/**
 * 获取该彩种计划列表
 */
XyebPage.prototype.codePlanList = function(param){
	var jsonData = {"lotteryType":param.kindName,"size":13};
	$.ajax({
		type : "POST",
		//设置Ajax请求改为同步请求!
		async: false,
		url : contextPath + "/codeplan/getCodePlan",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var codePlanList = result.data.codePlans;
				var subKindVOs = result.data.subKindVOs;
				xyebPage.showdatas.codePlans = result.data.codePlans;
				xyebPage.showdatas.subKindVOs = result.data.subKindVOs;
//				$("#codePlanList").html("<div class='siftings-line'><div colspan='12'>正在加载...</div></div>");
				xyebPage.showData(codePlanList,subKindVOs);
			} else if (result.code == "error") {
				showTip(result.data);
			}
		}
	});
}

/**
 * 
 */
XyebPage.prototype.showData = function(codePlans,subKindVOs,subkind){
	if(codePlans.length > 0){
		//彩种游戏下方的游戏记录
		var codePlanListObj = $("#codePlanList");
		codePlanListObj.html("");
		codePlanListObj.append("<div class='description' style='font-size: 40px;margin-left: 43%;'>北京PK10预测</div>");
		//加载彩种
		var subKindhtml = "";
		for(var j=0;j<subKindVOs.length;j++){
			subKindhtml += "<li><p data-value='"+subKindVOs[j].code
			+"' style='font-size: 28px;'>"+subKindVOs[j].description+"</p></li>";
		}
		//判断是否第一次加载彩种
		var subkindvo = "";
		if(subkind==null || subkind=="undefined"){
			subkindvo += "<input type='hidden' class='select-save' id='lotteryKind' value='"+subKindVOs[0].code+"' />"+
			"<p class='select-title'>"+subKindVOs[0].description+"</p>";
		}else {
			subkindvo += "<input type='hidden' class='select-save' id='lotteryKind' value='"+subkind.code+"' />"+
			"<p class='select-title'>"+subkind.description+"</p>";
		}
		var str = "";
		str += "<div class='line'>";
		str += "	<div class='child child1extend'><p>期数</p></div>";
		str += "	<div class='child child2extend'><div class='line-content select-content sort' id='sortlist'>"+subkindvo+
		"<ul class='select-list myList' id='lotteryKinds' style='display: none;'>"+subKindhtml+"</ul></div></div>";
		str += "	<div class='child child3extend'><p>开奖号码</p></div>";
		str += "	<div class='child child4extend'><p>期号</p></div>";
		subkindvo = "";
		subKindhtml = "";
		str += "	<div class='child child5extend'><p>中奖状态</p></div>";
		str += "</div>";
		codePlanListObj.append(str);
		str = "";
		var playkindvalue = $("#lotteryKind").val();
		for(var i = 0; i < codePlans.length; i++){
			var codeplan = codePlans[i];
			if(codeplan.playkind == playkindvalue){
				str += "<div class='line'>";
				str += "	<div class='child child1extend'><p>"+ codeplan.expectsPeriod +"</p></div>";
				var kongge1 = "";
				if(codeplan.codesDesc.length>20){
					for(var j=0;j<codeplan.codesDesc.length/4;j++){
						kongge1 = kongge1 + '&nbsp;';
					}
				}
				var kongge2 = "";
				if(codeplan.openCode.length>20){
					for(var j=0;j<codeplan.openCode.length/4;j++){
						kongge2 = kongge2 + '&nbsp;';
					}
				}
				if(codeplan.codesDesc.length>20){
					str += "	<div class='child child2extend'><p  style='font-size: 28px;'>"+ kongge1 + codeplan.codesDesc.substring(0,codeplan.codesDesc.length/2)+'<br/>'+ kongge1
					+codeplan.codesDesc.substring(codeplan.codesDesc.length/2,codeplan.codesDesc.length) +"</p></div>";
				}else {
					str += "	<div class='child child2extend'><p  style='font-size: 28px;'>"+ codeplan.codesDesc +"</p></div>";
				}
				if(codeplan.openCode=="" || codeplan.enabled==0){
					str += "	<div class='child child3extend'><p>"+codeplan.prostateDesc+"</p></div>";
				}else{
					if(codeplan.openCode.length>20){
						str += "	<div class='child child3extend'><p  style='font-size: 28px;'>"+  kongge2 + codeplan.openCode.substring(0,codeplan.openCode.length/2)+'<br/>'+  kongge2
						+codeplan.openCode.substring(codeplan.openCode.length/2,codeplan.openCode.length) +"</p></div>";
					}else {
						str += "	<div class='child child3extend'><p>【"+ codeplan.openCode +"】</p></div>";
					}
				}
				if(codeplan.expectShort==""){
					str += "	<div class='child child4extend'><p>"+codeplan.prostateDesc+"</p></div>";
				}else{
					str += "	<div class='child child4extend'><p>"+ codeplan.expectShort +"期</p></div>";
				}
				str += "	<div class='child child5extend'><p class='cp-yellow'>"+codeplan.prostateDesc+"</p></div>";
				str += "</div>";
				codePlanListObj.append(str);
			}
			str = "";
		}
		$(".select-title").unbind("click").click(function(event){
			if(document.getElementById('lotteryKinds').getAttribute('style')=="display: none;"){
				document.getElementById('lotteryKinds').setAttribute('style','display: block;')
			}else{
				document.getElementById('lotteryKinds').setAttribute('style','display: none;')
			}
		});
		$("#lotteryKinds li").unbind("click").click("unbind",function(){
			var parent_select=$(this).closest(".select-list");
			parent_select.find("li").removeClass("on");
			$(this).addClass("on");
			var parent=$(this).closest(".select-content");
			var val=$(this).html();
			
			var html=$(val).html();
			var valLen=$(val).html().length;
			if(!isNaN(parseInt(html)))valLen=valLen/2;
			valLen=valLen*16+64;
			parent.css("width",valLen+"px")
			
			var realVal = $(this).find("p").attr("data-value");
			parent.find(".select-save").val(realVal);
			parent.find(".select-title").html(val);
			var subkind = {"code":realVal,"description":html};
			xyebPage.showData(xyebPage.showdatas.codePlans,xyebPage.showdatas.subKindVOs,subkind);
		});
	}
}

XyebPage.prototype.select_type=function(){
	$(".content-xyeb .cols").removeClass("on");
	$(".content-xyeb .mun2").removeClass("on");
	lotteryCommonPage.countLotteryNum();
	$(".lottery-bar .additional").hide();
}

/**
 * 展示当前正在投注的期号
 */
XyebPage.prototype.showCurrentLotteryCode = function(lotteryIssue){
	if(lotteryIssue != null){
		var openCodeStr = xyebPage.getOpenCodeStr(lotteryIssue.lotteryNum);
		$('#nowNumber').text(openCodeStr);
	}
};

/**
 * 获取开奖号码前台展现的字符串
 */
XyebPage.prototype.getOpenCodeStr = function(openCodeNum){
	var openCodeStr = "";
	if(isNotEmpty(openCodeNum)){
//		var expectDay = openCodeNum.substring(0,openCodeNum.length - 3);
//		var expectNum = openCodeNum.substring(openCodeNum.length - 3,openCodeNum.length);
//		openCodeStr = expectDay + "-" + expectNum;
		openCodeStr = openCodeNum;
	}
	return openCodeStr;
};

/**
 * 展示最新的开奖号码
 */
XyebPage.prototype.showLastLotteryCode = function(lotteryCode){
	if(lotteryCode != null){
		var lotteryNum = lotteryCode.lotteryNum;
		var openCodeStr = xyebPage.getOpenCodeStr(lotteryCode.lotteryNum);
		$('#J-lottery-info-lastnumber').text(openCodeStr);
		
		$(".lottery_run .child-muns").empty();
		var sum=Number(lotteryCode.numInfo1)+Number(lotteryCode.numInfo2)+Number(lotteryCode.numInfo3);
		var html='<div class="xyeb-mun xyeb-red big" id="lotteryNumber1" >'+lotteryCode.numInfo1+'</div>+'+
				 '<div class="xyeb-mun xyeb-red big" id="lotteryNumber2" >'+lotteryCode.numInfo2+'</div>+'+
				 '<div class="xyeb-mun xyeb-red big" id="lotteryNumber3" >'+lotteryCode.numInfo3+'</div>'+
				 '<div class="xyeb-mun equal" >=</div>'+
				 '<div class="xyeb-mun '+xyebPage.changeColorByCode(sum)+' big" id="lotteryNumber4" >'+sum+'</div>';
		$(".lottery_run .child-muns").append(html);
	}
};

//计算颜色
XyebPage.prototype.changeColorByCode = function(k){
		var colors="";
		if(k==3||k==6||k==9||k==12||k==15||k==18||k==21||k==24){
			colors="xyeb-red";
		}else if(k==1||k==4||k==7||k==10||k==16||k==19||k==22||k==25){
			colors="xyeb-green";
		}else if(k==2||k==5||k==8||k==11||k==17||k==20||k==23||k==26){
			colors="xyeb-blue";
		}else if(k== 0||k==13||k==14||k==27){
			colors="xyeb-gray";
		}
		return colors;
}

XyebPage.prototype.returnAninal = function(opencode){
	var shengxiaoArr =lotteryDataDeal.shengxiao.shengxiaoArr;
	for(var i=0;i<shengxiaoArr.length;i++){		
		for(var j=0;j<shengxiaoArr[i].length;j++){
			if(Number(opencode) == Number(shengxiaoArr[i][j])){
				return lotteryDataDeal.shengxiao.shengxiaoKey[i];
			}
		}
	}
	return "-";
}

//服务器时间倒计时
XyebPage.prototype.calculateTime = function(isClose){
	var SurplusSecond = lotteryCommonPage.param.diffTime;
	
	lotteryCommonPage.param.isClose = isClose;
	if(lotteryCommonPage.param.isClose){ 
		//显示预售中
		$("#timer").hide();
		$("#presellTip").show();
		window.clearInterval(lotteryCommonPage.param.intervalKey); //终止周期性倒计时
	}else{
		var h = Math.floor(SurplusSecond/3600);
		if (h<10){
			h = "0"+h;
		}
		//计算剩余的分钟
		SurplusSecond = SurplusSecond - (h * 3600);
		var m = Math.floor(SurplusSecond/60);
		if (m<10){
			m = "0"+m;
		}
		var s = SurplusSecond%60;
		if(s<10){
			s = "0"+s;
		}

		h = h.toString();
		m = m.toString();
		s = s.toString();
//		$(".times .hours").html(h);
//		$(".times .minute").html(m);
//		$(".times .second").html(s);
		
		$("#timer").html(h+":"+m+":"+s);
        $("#presellTip").hide();
		$("#timer").show();
	}
	lotteryCommonPage.param.diffTime--;
	if(lotteryCommonPage.param.diffTime < 0){
		isClose=true;	
		window.clearInterval(lotteryCommonPage.param.intervalKey); //终止周期性倒计时
		lotteryCommonPage.getActiveExpect();  //倒计时结束重新请求最新期号
	};
};