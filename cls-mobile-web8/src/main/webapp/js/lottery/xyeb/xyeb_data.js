function LotteryDataDeal(){}
var lotteryDataDeal = new LotteryDataDeal();

lotteryDataDeal.param = {
	bizSystem:null
};
//
lotteryDataDeal.modelMap = new JS_OBJECT_MAP();
lotteryDataDeal.playNumDescMap = new JS_OBJECT_MAP();

/**
 * 加载某种玩法赔率
 */
LotteryDataDeal.prototype.loadLotteryKindPL = function() {
	
	$.ajax({
		type : "POST",
		// 设置请求为同步请求
		async : false,
		url : contextPath + "/lottery/getLotteryWinXyebByKind",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			lotteryCommonPage.hideLotteryMsg();
			if (result.code == "ok") {
				var resultData = result.data;
				//需要验证赔率数据加载完全不完全
				for(var i=1;i<=resultData.length;i++){
					if(i>28){
						$("#pl"+resultData[i-1].code).text("赔率："+resultData[i-1].winMoney);
					}else{
						$("#pl"+resultData[i-1].code).text(resultData[i-1].winMoney);
					}
				}
				
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
				$("#editUserButton").removeAttr("disabled");
			}
		}
	});
}

/**
 * 初始化1-49html,涉及玩法有正码，特码，正码特，连码，全不中
 * startpos:1,11,21,31,41
 */
LotteryDataDeal.prototype.initPlay = function() {
	
	var main_lists_html = '';
	for(var i=0;i<=27;i++){
			//红波
			if(i==3||i==6||i==9||i==12||i==15||i==18||i==21||i==24){
				
				main_lists_html +='<div class="cols cols-25 ball-'+i+'">'+
								  '<div class="xyeb-mun xyeb-red">'+i+'</div>'+
								  '<span id="pl'+i+'">-</span>'+
								  '</div>';
			//绿波
			}else if(i==1||i==4||i==7||i==10||i==16||i==19||i==22||i==25){
				main_lists_html +='<div class="cols cols-25 ball-'+i+'">'+
								  '<div class="xyeb-mun xyeb-green">'+i+'</div>'+
								  '<span id="pl'+i+'">-</span>'+
								  '</div>';
				
			//蓝波
			}else if(i==2||i==5||i==8||i==11||i==17||i==20||i==23||i==26){
			
				main_lists_html +='<div class="cols cols-25 ball-'+i+'">'+
								  '<div class="xyeb-mun xyeb-blue">'+i+'</div>'+
								  '<span id="pl'+i+'">-</span>'+
								  '</div>';
			//灰色
			}else if( i == 0||i==13||i==14||i==27){
				main_lists_html +='<div class="cols cols-25 ball-'+i+'">'+
								  '<div class="xyeb-mun xyeb-gray">'+i+'</div>'+
								  '<span id="pl'+i+'">-</span>'+
								  '</div>';
			}
	}
	return main_lists_html;
}

LotteryDataDeal.prototype.initPlay2=function(){
		var list=['大','小','单','双','大单','小单','大双','小双','极大','极小','红波','绿波','蓝波','豹子'];
		var list2=['29','34','30','35','31','36','32','37','33','38','39','40','41','42'];
		var k=0;
		var main_lists_html2 ="";
	   	for(var i=29;i<=42;i++){
	   		if(k==4||k==5||k==6||k==7){
	   			k++;
	   			continue;
	   		}
	   		if(i==29||i==39||i==42){
	   			main_lists_html2 +='<div class="lottery-child no main">';
	   			if(i==29){
	   				main_lists_html2 +='<div class="child-title"><h2>混合</h2>'+
	   								   '</div><div class="child-content">';
	   			}else if(i==39){
	   				main_lists_html2 +='<div class="child-title"><h2>波色</h2>'+
					   				   '</div><div class="child-content part-third">';
	   			}else if(i==42){
	   				main_lists_html2 +='<div class="child-title"><h2>豹子</h2>'+
					   				   '</div><div class="child-content long">';
	   			}
	   		}
	   		var colors="";
	   		if(i==39){
	   			colors="bg-red";
	   		}else if(i==40){
	   			colors="bg-green";
	   		}else if(i==41){
	   			colors="bg-blue";
	   		}
	   		main_lists_html2 +='<div class="mun2 '+colors+'">'+
						       '<h2 class="value">'+list[k]+'</h2>'+
						       '<p class="info" id="pl'+Number(list2[k])+'">赔率0</p>'+
						       '</div>';
	   		if(i==32||k==3||i==38||i==41||i==42){
	   			main_lists_html2 +='</div>';
	   			if(k==3){
	   				main_lists_html2 +='<div class="child-content part-second">';
	   			}else{
	   				main_lists_html2 +='<div class="child-content">';	
	   			}
	   		}
	   		
	   		if(i==38||i==41||i==42){
	   			main_lists_html2 +='</div>';
	   		}
	   		k++;
	   	}
		return main_lists_html2;
}

//colsChildClick();
LotteryDataDeal.prototype.colsChildSet = function (momey){
	var shortcutmoney = $("#shortcut-money").val();
	quickmoney = Number(shortcutmoney) + Number(momey);
	$("#shortcut-money").val(quickmoney);
	lotteryCommonPage.countLotteryNum();
}

/**
 * 玩法选择
 */
$(document).ready(function() {
	var main_lists_html="";
	var main_lists_html2="";
	main_lists_html = lotteryDataDeal.initPlay();
	main_lists_html2 = lotteryDataDeal.initPlay2();
	$("#initPlay").html(main_lists_html);
	$("#initPlay2").html(main_lists_html2);
	
	//加载赔率
	lotteryDataDeal.loadLotteryKindPL();
});