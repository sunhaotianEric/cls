function LotteryCommonPage(){}
var lotteryCommonPage = new LotteryCommonPage();

lotteryCommonPage.currentLotteryKindInstance = null;
lotteryCommonPage.param = {
		diffTime : 0,	  //时间倒计时剩余时间	
		currentExpect : null, //当前投注期号
		currentLastOpenExpect : null, //当前最新的开奖期号
		intervalKey : null,  //倒计时周期key
		toFixedValue : 4, //小数点保留4为数字
		isCanActiveExpect : true,
		isClose:false//是否封盘了
}; 
//投注订单
lotteryCommonPage.lotteryOrder = {};
/**
 * 投注参数
 */
lotteryCommonPage.lotteryParam = {
		SOURCECODE_SPLIT : ",",
		SOURCECODE_SPLIT_KS : "&",
		CODE_REPLACE : "-",
		SOURCECODE_ARRANGE_SPLIT : "|",
		currentTotalLotteryCount : 0,
		currentTotalLotteryPrice : 0.0000,
		currentZhuiCodeTotalLotteryPrice : 0.0000, //追号的总共价格
		currentZhuiCodeTotalLotteryCount : 0,
		currentIsUpdateCodeStastic : false,
		updateCodeStasticIndex : null,
		todayTraceList : new Array(),
		tomorrowTraceList : new Array(),
		isZhuiCode : false,  //是否追号
		currentLotteryWins : new JS_OBJECT_MAP(),  //奖金映射 
	    lotteryMap : new JS_OBJECT_MAP(),  //投注映射
	    lotteryMultipleMap : new JS_OBJECT_MAP(),  //投注倍数映射
	    modelValue : null,
	    awardModel : "YUAN",
	    beishu : 1 //默认是1倍
};

lotteryCommonPage.otherParam = {
		disturb : false,	 
		oriKindName : null,
		model : null,
		tip : null,
		lotteryType : null, 
		lotteryTypeDes : null,
		lotteryExpect : null,
		money : null
}; 

$(document).ready(function() {

	//近10期的开奖记录
	lotteryCommonPage.getNearestTenLotteryCode();
	//顶部导航选中处理
	$("#navChoose .nav-child").removeClass("on");
	$("#navChoose .nav-child:eq(1)").addClass("on");
	
	
	//选择地区事件
	$(".m-banner .b-child1 .btn").click(function(){
		$(".b-child-list").stop(false,true).slideToggle(500);
	});
	//切换背景事件
	$(".change-bg img").click(function(){
		var url = $(this).attr("data-val");
		$("body").css({"background-image":"url("+url+")"});
	});
	
//	//当前遗漏或者冷热值
//	lotteryCommonPage.omitClodEvent();

  
	
	//下方最近投注记录和追号记录的切换
	$("#userTodayLottery").unbind("click").click(function(){
		if($(this).hasClass("on")) {
			return;
		}
		$("#userTodayLotteryAfterList").hide();
		$("#userTodayLotteryList").show();
		$(this).addClass("on");
		$("#userTodayLotteryAfter").removeClass("on");
	});	
	$("#userTodayLotteryAfter").unbind("click").click(function(){
		if($(this).hasClass("on")) {
			return;
		}
		//显示查询中
		$("#userTodayLotteryAfterList").html("<div class='line'><p class='no-msg'>正在查询中...</p></div>");
		//lotteryCommonPage.getTodayLotteryAfterByUser();
		
		$("#userTodayLotteryList").hide();
		$("#userTodayLotteryAfterList").show();		
		$(this).addClass("on");
		$("#userTodayLottery").removeClass("on");
	});	

	//获取当前彩种的最新期号和投注倒计时
	lotteryCommonPage.getActiveExpect();
	//近10期的开奖记录
	lotteryCommonPage.getNearestTenLotteryCode();
	//获取用户今日投注记录
	lotteryCommonPage.getTodayLotteryByUser();
	//获取最新开奖号码
	lotteryCommonPage.getLastLotteryCode();
	window.setInterval("lotteryCommonPage.getLastLotteryCode()",60000);  //1分钟重新读取
	window.setInterval("lotteryCommonPage.getActiveExpect()",10000); //1分钟重新读取服务器
	
	// 顶部导航链接选中样式
	$('.header .foot .nav .child2').addClass('on');
	
	// 显示顶部换色
	$('#changecolor-show').show();
});

/**
 * 选中倍数（金额）光标选中处理
 * @param tag
 * @returns
 */
function onfocusFn(tag) {  
	tag.selectionStart = 0;   //选中区域左边界
	tag.selectionEnd = tag.value.length; //选中区域右边界
    //tag.select();  
} 

/**
 * 阻止safari浏览器默认事件
 * @param eventTag
 * @returns
 */
function onmouseupFn(eventTag) {  
    var event = eventTag||window.event;    
    event.preventDefault();
}  

//切换玩法
LotteryCommonPage.prototype.play_change = function(ele,e){
	$(".lottery-classify2 .move-content .child").removeClass('color-red');
	$(e).addClass('color-red');
	$(".lottery-classify2 .clist").removeClass('in').addClass('out');
	$(ele).addClass('in').removeClass('out');
	if($(e).attr("data-id")=="HH"){
		$(".content-xyeb .cols").removeClass("on");
	}else if($(e).attr("data-id")=="TM"){
		$(".content-xyeb .mun2").removeClass("on");
	}
	lotteryCommonPage.countLotteryNum();
}
//显示玩法或者票种
LotteryCommonPage.prototype.show_classify = function (ele,e){
	var parent=$(e).closest("body");
	var display=$(ele).css("display");
	var style="boolean";
	style=display=="none"?"true":"boolean";
	parent.find(".lottery-classify").not(ele).hide();
	element_toggle(style,[ele],parent);
}

/**
 * 系统投注逻辑处理
 */
LotteryCommonPage.prototype.userLottery = function(){
	if(lotteryCommonPage.param.isClose){
		
		showTip("封盘中,禁止投注！");
		return ;
	}
	$.ajax({
		type : "POST",
		url : contextPath + "/lottery/userLottery",
		data : JSON.stringify(lotteryCommonPage.lotteryOrder),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			var resultData = result.data;
			lotteryCommonPage.hideLotteryMsg();
			if (result.code == "ok") {
				lotteryCommonPage.lotteryEnd(); //投注结束动作
				showTip("投注下单成功.");
				lotteryCommonPage.getTodayLotteryByUser();
			} else if (result.code == "error" && result.data == "BalanceNotEnough") {
				showTip("对不起您的余额不足,请及时充值.");
			} else if (result.code == "error" && result.data == "BanksIsNull"){
				showTip("对不起,系统检测到您还没添加银行卡信息,为了方便中奖提款,请立即前往绑定.");
			} else if (result.code == "error"){
				if(result.data.indexOf("投注期号不正确.") != -1){
					//lotteryCommonPage.getAllExpectsByTodayAndTomorrow(); //重新加载今天和明天的期号				
					showTip(result.data);
				}else{
					showTip(result.data);
				}
			} else{
				showTip("投注请求失败.");
			}
		}
	});
};

/**
 * 金额值改变处理
 * @returns {Boolean}
 */
LotteryCommonPage.prototype.beishuEventContent = function(){
	var beishuObj = $('#beishu');
    var beishuStr = $('#shortcut-money').val();
	var re = /^[1-9]+[0-9]*]*$/;
    if (!re.test(beishuStr)){
    	//beishuObj.val("1");  //this.value.replace(/[^0-9]/g,'') 1
		//lotteryCommonPage.lotteryParam.beishu = 1;
    	return false;
    }
    var beishuLength = beishuStr.length;
	if(beishuLength > 0){
		 //倍数限制在9999
		 var beishuTemp = parseInt(beishuStr);
		 if(beishuTemp > 9999){
			 beishuObj.val(9999);
		 }
		 lotteryCommonPage.countLotteryNum();
	}
};

/**
 * 刷新最近十期的开奖号码的显示
 */
LotteryCommonPage.prototype.showNearestTenLotteryCode = function(lotteryCodeList){
	if(lotteryCodeList != null && lotteryCodeList.length > 0){
		$("#nearestTenLotteryCode").html("");
		var str = "";
		str += "<div class='titles'>";
		str += "	<div class='child child1'><p>期号</p></div>";
		str += "	<div class='child child2'><p>开奖号</p></div>";
		str += "</div>";
		
		str += "<div class='content'>";
		for(var i = 0; i < lotteryCodeList.length; i++){
			//只加载5条记录
			if(i > 4) {
				break;
			}
			var lotteryCode = lotteryCodeList[i];
			var openCodeStr = lotteryCommonPage.currentLotteryKindInstance.getOpenCodeStr(lotteryCode.lotteryNum);
			var sum=Number(lotteryCode.numInfo1)+Number(lotteryCode.numInfo2)+Number(lotteryCode.numInfo3);
			str += "	<div class='line'>";
			str += "		<div class='child child1'><p>"+openCodeStr+"</p></div>";
			str += "		<div class='child child2 font-blue muns muns-small'>";
			str += "			<div class='xyeb-mun big xyeb-red'>"+lotteryCode.numInfo1+"</div><span class='symbol' style='display:none;'>+</span>";
			str += "			<div class='xyeb-mun big xyeb-red'>"+lotteryCode.numInfo2+"</div><span class='symbol' style='display:none;'>+</span>";
			str += "			<div class='xyeb-mun big xyeb-red'>"+lotteryCode.numInfo3+"</div><span class='symbol' style='display:none;'>=</span>";
			str += "			<div class='xyeb-mun big "+ xyebPage.changeColorByCode(sum)+"'>"+sum+"</div>";
			str += "		</div>";
		    str += "	</div>";
		}
		str += "</div>";
		$("#nearestTenLotteryCode").html(str);
		$('.symbol').show();
	//	lotteryLhcCommonPage.showLastLottery(lotteryCodeList);
		
	}else{
		$("#nearestTenLotteryCode").html("<ul class='c-list'><span>未加载到开奖记录</span></ul>");
	}
}

/**
 * 获取系统当前正在投注的期号
 */
LotteryCommonPage.prototype.getActiveExpect = function(){
	if(lotteryCommonPage.param.isCanActiveExpect){  //判断是否可以进行期号的请求
		var jsonData = {
				"lotteryKind" :  lotteryCommonPage.currentLotteryKindInstance.param.kindName ,
		};
		$.ajax({
			type : "POST",
			url : contextPath + "/lotteryinfo/getExpectAndDiffTime",
			data : JSON.stringify(jsonData),
			dataType : "json",
			contentType : 'application/json;charset=utf-8',
			success : function(result) {
				commonHandlerResult(result, this.url);
				if (result.code == "ok") {
					lotteryCommonPage.param.isCanActiveExpect = false;
					var issue = result.data;
					if(issue != null){
						if(issue.specialExpect == 2){
							$("#timer").text("暂停销售");
						}else{
							//展示当前期号
							lotteryCommonPage.currentLotteryKindInstance.showCurrentLotteryCode(issue);
							//判断当前期号是否发生变化
							if(lotteryCommonPage.param.currentExpect == null || lotteryCommonPage.param.currentExpect != issue.lotteryNum) {
								//记录当前期号
								lotteryCommonPage.param.currentExpect = issue.lotteryNum; 
							}
							//当前期号剩余时间倒计时
							lotteryCommonPage.param.diffTime = issue.timeDifference;
							
							window.clearInterval(lotteryCommonPage.param.intervalKey); //终止周期性倒计时
		
							//如果是特殊的时间段
		                    if(issue.specialTime == 1){ //特殊时长
		                    //	lotteryCommonPage.currentLotteryKindInstance.calculateTime(issue.isClose); 
		    					lotteryCommonPage.param.intervalKey = setInterval("lotteryCommonPage.currentLotteryKindInstance.calculateTime("+issue.isClose+")",1000);	
		                    }else{
		    					lotteryCommonPage.currentLotteryKindInstance.calculateTime();  //支持最多一个小时
		    					lotteryCommonPage.param.intervalKey = setInterval("lotteryCommonPage.currentLotteryKindInstance.calculateTime()",1000);	
		                    }
						}
					}else{
						//显示预售中
						$("#timer").hide();
						$("#presellTip").show();
					}
					
					if(lotteryCommonPage.param.diffTime > 5000){ //如果剩余时间超过5秒的
						setTimeout("lotteryCommonPage.controlIsCanActive()", 2000);
					}else{
						lotteryCommonPage.param.isCanActiveExpect = true;
					}
				} else if (result.code == "error") {
					showTip(result.data);
				}
			}
		});
	}
};

/**
 * 控制是否能发起期号的请求
 */
LotteryCommonPage.prototype.controlIsCanActive = function(){
	lotteryCommonPage.param.isCanActiveExpect = true;
};

/**
 * 获取最新的开奖号码
 */
LotteryCommonPage.prototype.getLastLotteryCode = function(){
	var jsonData = {
			"lotteryKind" : lotteryCommonPage.currentLotteryKindInstance.param.kindName,	
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/code/getLastLotteryCode",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var lotteryCode = result.data;
				lotteryCommonPage.currentLotteryKindInstance.showLastLotteryCode(lotteryCode);
				var lotteryNum = lotteryCode.lotteryNum;
				//当开奖号码发生变化
				if(lotteryCommonPage.param.currentLastOpenExpect == null || lotteryCommonPage.param.currentLastOpenExpect != lotteryNum){
					//动画展示开奖号码
					lotteryCommonPage.getLotteryAnimation();
					//重新获取近10期的开奖记录
					lotteryCommonPage.getNearestTenLotteryCode();
					//获取用户今日投注记录，刷新下方的中奖状态
					lotteryCommonPage.getTodayLotteryByUser();
				}
				lotteryCommonPage.param.currentLastOpenExpect = lotteryNum;  //存储当前期号	
			} else if (result.code == "error") {
				showTip(result.data);
			}
		}
	});
};

/**
 * 开奖结果动画设置
 */
LotteryCommonPage.prototype.getLotteryAnimation = function(){
	var dtime;
	$(".m-banner .b-child2 .btns .btn").each(function(i,n){
		dtime=0.2*i;
		base.anClasAdd(".m-banner .b-child2 .btns .btn:eq("+i+")","scale","1s",dtime+"s");
	});
	var dtime1=(dtime+1)*1000;
	setTimeout(function(){
		$(".m-banner .b-child2 .btns .btn").attr("style","");
	},dtime1);
}

/**
 * 获取最近十期的开奖号码
 */
LotteryCommonPage.prototype.getNearestTenLotteryCode = function(){
	var jsonData = {
			"lotteryKind" : lotteryCommonPage.currentLotteryKindInstance.param.kindName,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/code/getNearestTenLotteryCode",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var lotteryCodeList = result.data;
				lotteryCommonPage.showNearestTenLotteryCode(lotteryCodeList);
			} else if (result.code == "error") {
				showTip(result.data);
			}
		}
	});
}

/**
 * 展示投注信息
 */
LotteryCommonPage.prototype.showLotteryMsg = function(){
	if(lotteryCommonPage.param.isClose){
		showTip("封盘中,禁止投注！");
		return ;
	}
	var shortcutMoney = $("#shortcut-money").val();
    if(shortcutMoney == ""){
		showTip("请输入下注金额！");
		return ;
	}
    
    if(isNaN(shortcutMoney)){
		showTip("请输入下注数字！");
		return ;
	}
    if(shortcutMoney<2){
		showTip("最低下注金额2元！");
		return ;
	}
	if($("#shortcut-money").val()==""){
		return;
	}
	var lotteryMsgHtml = '';
       lotteryMsgHtml += '<div class="msg-list"><ul>';
       lotteryMsgHtml += '<li class="msg-list-li msg-list-t">';
	   lotteryMsgHtml += '<div class="child child1">序号</div>';
	   lotteryMsgHtml += '<div class="child child1">玩法</div>';
	   lotteryMsgHtml += '<div class="child child2">内容</div>';
	   lotteryMsgHtml += '<div class="child child3">赔率</div>';
	   lotteryMsgHtml += '<div class="child child4">下注金额</div></li>';
	
   lotteryCommonPage.lotteryOrder = {};
   var resultArray = new Array();//存放投注信息
   var totalMoney = 0,lotteryNum = 0;//总金额、投注数
   var codePlArr = new Array();
   var root=$("#currentKindName").text();
   //文本框遍历
   var j=0;
   if(root=="特码"){
    	$(".content-xyeb .mun2").removeClass("on");
        $(".content-xyeb .cols").each(function(){
        		var money=$("#shortcut-money").val();
                var root = $(this).hasClass('on');
	       		if(root){
	       			lotteryNum++;
	       			//投注玩法
	       			var model="TM";
	       			var model2="特码";
	       			
//	       			resultArray[j] = new Array();
//	       			resultArray[j][0] = model;//玩法
//	       			resultArray[j][1] = $(this).find("span").attr("id").substring(2,6);//投注号码
//	       			resultArray[j][2] = money;//投注金额
	       			
		       		var resultArrayDetail = {};
					resultArrayDetail.playKindStr = model;//玩法
					resultArrayDetail.playKindCodes = $(this).find("span").attr("id").substring(2,6);//投注号码
					resultArrayDetail.lotteryMultiple = money;//投注金额
					resultArray[j] = resultArrayDetail;
	       			
	       			lotteryMsgHtml += '<li class="msg-list-li">';
	       			lotteryMsgHtml += '<div class="child child1">'+(j+1)+'</div>';
	       			lotteryMsgHtml += '<div class="child child1">'+model2+'</div>';
	       			lotteryMsgHtml += '<div class="child child2">'+$(this).find(".xyeb-mun").text()+'</div>';
	       			lotteryMsgHtml += '<div class="child child3">'+$(this).find("span").text()+'</div>';
	       			lotteryMsgHtml += '<div class="child child4">'+money+'</div></li>';
	       			totalMoney += Number(money);
	       			j++;
	       		}
        });
    }else if(root=="混合"){
    	$(".content-xyeb .cols").removeClass("on");
        $(".content-xyeb .mun2").each(function(){
    		var money=$("#shortcut-money").val();
            var root = $(this).hasClass('on');
       		if(root){
       			lotteryNum++;
       			//投注玩法
       			var i=$(this).find("p").attr("id").substring(2,6);
       			var model="";
       			var model2="";
       			if(i>28&&i<39){
       				model2="混合";
       				model="HH";
       			}else if(i>=39&&i<42){
       				model2="波色";
       				model="BS";
       			}else if(i==42){
       				model2="豹子";
       				model="BZ";
       			}
       			
//       			resultArray[j] = new Array();
//       			resultArray[j][0] = model;//玩法
//       			resultArray[j][1] = $(this).find("p").attr("id").substring(2,6);//投注号码
//       			resultArray[j][2] = money;//投注金额
       			
       			var resultArrayDetail = {};
				resultArrayDetail.playKindStr = model;//玩法
				resultArrayDetail.playKindCodes = $(this).find("p").attr("id").substring(2,6);//投注号码
				resultArrayDetail.lotteryMultiple = money;//投注金额
				resultArray[j] = resultArrayDetail;
       			
       			lotteryMsgHtml += '<li class="msg-list-li">';
       			lotteryMsgHtml += '<div class="child child1">'+(j+1)+'</div>';
       			lotteryMsgHtml += '<div class="child child1">'+model2+'</div>';
       			lotteryMsgHtml += '<div class="child child2">'+$(this).find(".value").text()+'</div>';
       			lotteryMsgHtml += '<div class="child child3">'+$(this).find("p").text().substring(3,10)+'</div>';
       			lotteryMsgHtml += '<div class="child child4">'+money+'</div></li>';
       			totalMoney += Number(money);
       			j++;
       		}
        });
    }    
        lotteryMsgHtml += '</ul></div>';
        lotteryMsgHtml += '<ul>';
        lotteryMsgHtml += '<li class="user-line">';
        lotteryMsgHtml += '<div class="line-title"><p>期数:</p></div>';
        lotteryMsgHtml += '<div class="line-content"><p>'+lotteryCommonPage.param.currentExpect+'</p></div>';
        lotteryMsgHtml += '</li>';
        lotteryMsgHtml += '<li class="user-line">';
        lotteryMsgHtml += '<div class="line-title"><p>总注数：</p></div>';
        lotteryMsgHtml += '<div class="line-content"><p>'+lotteryNum+'</p></div>';
        lotteryMsgHtml += '</li>';
        lotteryMsgHtml += '<li class="user-line">';
        lotteryMsgHtml += '<div class="line-title"><p>总下注额：</p></div>';
        lotteryMsgHtml += '<div class="line-content"><p class="font-red">'+totalMoney+'</p></div>';
        lotteryMsgHtml += '</li></ul></div>';
        lotteryMsgHtml += '</div>';
        
        lotteryCommonPage.lotteryOrder.lotteryKind = lotteryCommonPage.currentLotteryKindInstance.param.kindName;  //投注彩种
        lotteryCommonPage.lotteryOrder.model = lotteryCommonPage.lotteryParam.awardModel;
        lotteryCommonPage.lotteryOrder.awardModel = currentUser.sscRebate;
        lotteryCommonPage.lotteryOrder.isLotteryByPoint = 0;
        //lotteryCommonPage.lotteryOrder.lotteryPlayKindMsg = resultArray; 
        lotteryCommonPage.lotteryOrder.playKindMsgs = resultArray;
    //    lotteryCommonPage.lotteryOrder.expect = lotteryCommonPage.param.currentExpect;
        
        if(lotteryNum<=0){
        	showTip("请选择投注号码！");
        	return;
        }
        lotteryCommonPage.lotteryOrder.expect = lotteryCommonPage.param.currentExpect;
        $("#xyeb_msg").html(lotteryMsgHtml);
        element_toggle(true,['.alert-cart']);        
        
	return true;
}
/**
 * codeNun =选择的号码个数
 * needNum = 玩法需要的号码个数
 */
LotteryCommonPage.prototype.countLotteryNum = function(){
	var sl=0;
	var money=$("#shortcut-money").val();
	var msg="";
	var root=$("#currentKindName").text();
	if(root=="特码"){
		sl=$(".content-xyeb .cols.on").length;
		$(".content-xyeb .cols.on").each(function(){
			msg+=$(this).find(".xyeb-mun").text()+",";
		});
	}else if(root=="混合"){
		sl=$(".content-xyeb .mun2.on").length;
		$(".content-xyeb .mun2.on").each(function(){
			msg+=$(this).find(".value").text()+",";
		});
	}
	if(msg.length>10){
		msg=msg.substring(0,10)+"...";
	}
	if(msg==""){
		msg="(投注列表)";
	}
	$("#J-balls-statistics-lotteryNum").text(sl);
	$("#J-balls-statistics-amount").text(Number(sl)*Number(money));
	$("#J-balls-statistics-code").text(msg);
}

/**
 * 关闭投注信息
 */
LotteryCommonPage.prototype.hideLotteryMsg = function(){
	$(".alert-xyeb-msg").stop(false,true).delay(200).fadeOut(500);
	$(".alert-msg-bg").stop(false,true).fadeOut(500);	
	$("#lotteryOrderDialogSureButton").removeAttr("disabled");
	$("#lotteryOrderDialogSureButton").val("确认投注");
}
/**
 * 投注结束动作
 */
LotteryCommonPage.prototype.lotteryEnd = function(){
	element_toggle(false,['.alert-cart']);
	$(".lottery-bar .additional").hide();
	$("#J-balls-statistics-lotteryNum").html(0);
	$("#J-balls-statistics-amount").html(0);
	$("#J-balls-statistics-code").text("（投注列表）");
	$("#shortcut-money").val("2");
	$("#XYEBReset").click();
};

/**
 * 获取今日投注数据
 */
LotteryCommonPage.prototype.getTodayLotteryByUser = function(){
	
	$.ajax({
		type : "POST",
		url : contextPath + "/order/getTodayLotteryByUser",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var todayLotteryList = result.data;
				if(todayLotteryList.length > 0){
					//彩种游戏下方的游戏记录
					var userOrderListObj = $("#userTodayLotteryList");
					userOrderListObj.html("");
					str = "";
					str += "<div class='line'>";
					str += "	<div class='child child1'><p>彩种</p></div>";
					str += "	<div class='child child2'><p>期号</p></div>";
					str += "	<div class='child child3'><p>投注金额</p></div>";
					str += "	<div class='child child4'><p>奖金</p></div>";
					str += "	<div class='child child5'><p>状态</p></div>";
					str += "	<div class='child child6'><i class='ion-close-circled'></i></div>";
					str += "</div>";
					for(var i = 0; i < todayLotteryList.length; i++){
						//显示5条最近投注记录
						if(i > 4) { 
							break;
						}
						var userOrder = todayLotteryList[i];
			    		var lotteryIdStr = userOrder.lotteryId;
			    		
			    		//订单状态特殊处理
			    		var prostateStatus = userOrder.prostateStatusStr;
			    		if(userOrder.prostate == "REGRESSION" || userOrder.prostate == "END_STOP_AFTER_NUMBER" ||
			    				userOrder.prostate == "NO_LOTTERY_BACKSPACE" || userOrder.prostate == "UNUSUAL_BACKSPACE" ||
			    				userOrder.prostate == "STOP_DEALING") {
			    			prostateStatus = "已撤单";
			    		}
			    		str += "<div class='line'>";
						str += "  <div class='child child1'><p>"+ userOrder.lotteryTypeDes +"</p></div>";
			    		str += "  <div class='child child2'><p>"+ userOrder.expect +"</p></div>";
			    		str += "  <div class='child child3'><p>"+ userOrder.payMoney.toFixed(frontCommonPage.param.fixVaue) +"</p></div>";
			    		str += "  <div class='child child4'><p>"+ userOrder.winCost.toFixed(frontCommonPage.param.fixVaue) +"</p></div>";
			    		/*if(userOrder.afterNumber == 1){
			    			str += "  <span style='width:5%;'><font class='cp-yellow'>是</font></span>";
			    		}else{
			    			str += "  <span style='width:5%;'><font class='cp-green'>否</font></span>";
			    		}*/
			    		
			    		if(userOrder.prostate == 'DEALING'){
				    		str += "  <div class='child child5'><p class='cp-yellow'>"+ prostateStatus +"</p></div>";
			    		}else if(userOrder.prostate == 'NOT_WINNING'){
				    		str += "  <div class='child child5'><p class='cp-red'>"+ prostateStatus +"</p></div>";
			    		}else {
				    		str += "  <div class='child child5'><p class='cp-green'>"+ prostateStatus +"</p></div>";
			    		}
			    		str += "  <div class='child child6'><a href='"+contextPath+"/gameBet/orderDetail.html?id="+userOrder.id+"' name='lotteryMsgRecord' data-lottery='"+userOrder.lotteryId+"'  data-type='"+userOrder.stopAfterNumber+"' data-value='"+userOrder.id+"'>查看</a></div>";
			    		str += "</div>";
			    		userOrderListObj.append(str);
			    		str = "";
					}
					
					var afterNumberLotteryWindow;
					
				}else{
					$("#userTodayLotteryList").html("<div class='line'><p class='no-msg'>还没有投注记录哟~</p></div>");
				}
			} else if (result.code == "error") {
				showTip(result.data);
			}
		}
	});
}

/**
 * 获取用户的今日追号投注记录
 */
LotteryCommonPage.prototype.getTodayLotteryAfterByUser = function(){
	
	
	$.ajax({
		type : "POST",
		url : contextPath + "/order/getTodayLotteryAfterByUser",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var todayLotteryList = result.data;
				if(todayLotteryList.length > 0){
					
					//彩种游戏下方的游戏记录
					var userOrderListObj = $("#userTodayLotteryAfterList");
					userOrderListObj.html("");
					str = "";
					str += "<div class='line line-titles'>";
					str += "	<div class='child child1' style='width:170px'>订单编号</div>";
					str += "	<div class='child child2'>彩种</div>";
					str += "	<div class='child child3'>开始期数</div>";
					str += "	<div class='child child4'>追号期数</div>";
					str += "	<div class='child child5'>完成期数</div>";
					str += "	<div class='child child6' style='width:80px'>总金额</div>";
					str += "	<div class='child child7' style='width:90px'>完成金额</div>";
					str += "	<div class='child child8'>中奖金额</div>";
					str += "	<div class='child child8'>模式</div>";
					str += "	<div class='child child10' style='width:130px'>下单时间</div>";
					str += "	<div class='child child11' style='width:50px'>状态</div>";
					str += "	<div class='child child12 no' style='text-align: center;'>内容</div>";
					str += "</div>";
					for(var i = 0; i < todayLotteryList.length; i++){
						if(i > 4) {
							break;
						}
						var userOrder = todayLotteryList[i];
			    		var lotteryIdStr = userOrder.lotteryId;
			    		
			    		str += "<div class='line line-titles'>";
			    		str += "  <div class='child child1' style='width:170px'>"+ lotteryIdStr.substring(lotteryIdStr.lastIndexOf('-')+1,lotteryIdStr.length) +"</div>";
			    		str += "  <div class='child child2'>"+ userOrder.lotteryTypeDes +"</div>";
			    		str += "  <div class='child child3'>"+ userOrder.expect +"</div>";
			    		str += "  <div class='child child4'>"+ userOrder.afterNumberCount +"</div>";
			    		str += "  <div class='child child5'>"+ userOrder.yetAfterNumber +"</div>";
			    		str += "  <div class='child child6' style='width:80px'>"+ userOrder.afterNumberPayMoney.toFixed(frontCommonPage.param.fixVaue) +"</div>";
			    		str += "  <div class='child child7' style='width:90px'>"+ userOrder.yetAfterCost.toFixed(frontCommonPage.param.fixVaue) +"</div>";
			    		str += "  <div class='child child8'>"+ userOrder.winCostTotal.toFixed(frontCommonPage.param.fixVaue) +"</div>";
			    		str += "  <div class='child child8'>"+ userOrder.lotteryModelStr +"</div>";
			    		str += "  <div class='child child10' style='width:130px'>"+ userOrder.createdDateStr +"</div>";
			    		var statusStr = "&nbsp;";
			    		if(userOrder.afterStatus == 1) {
			    			statusStr = "未开始";
			    		} else if(userOrder.afterStatus == 2) {
			    			statusStr = "进行中";
			    		} else if(userOrder.afterStatus == 3) {
			    			statusStr = "已完成";
			    		}
			    		str += "  <div class='child child11'style='width:50px'>"+ statusStr +"</div>";
						str += "  <div class='child child12 no' style='text-align: center;'><a href='"+contextPath+"/gameBet/orderDetail.html?id="+userOrder.id+"' name='lotteryAfterMsgRecord' data-lottery='"+userOrder.lotteryId+"'  data-type='"+userOrder.stopAfterNumber+"' data-value='"+userOrder.id+"'>查看</a></div>";
			    		str += "</div>";
			    		
			    		userOrderListObj.append(str);
			    		str = "";
					}
					
					var afterNumberLotteryWindow;
					
				}else{
					$("#userTodayLotteryAfterList").html("<div class='line'><p class='no-msg'>还没有投注记录哟~</p></div>");
				}
				
				//查看投注详情
				$("a[name='lotteryAfterMsgRecord']").unbind("click").click(function(event){
					var orderId = $(this).attr("data-value");
					var dataType = $(this).attr("data-type");
					var width = 800;
					var height = 400;
					var left = parseInt((screen.availWidth/2) - (width/2));//屏幕居中
					var top = parseInt((screen.availHeight/2) - (height/2));
					var windowFeatures = "width=" + width + ",height=" + height + ",status,resizable,scrollbars=yes,left=" + left + ",top=" + top;
					
					if(dataType == 0){
						var lotteryhrefStr = contextPath + "/gameBet/lotterymsg/"+orderId+"/lotterymsg.html";
					}else{
						var dataLottery = $(this).attr("data-lottery");
						var lotteryhrefStr = contextPath + "/gameBet/lotteryafternumbermsg/"+dataLottery+"/lotteryafternumbermsg.html";
					}
					afterNumberLotteryWindow = window.open(lotteryhrefStr, "subWind", windowFeatures);
					afterNumberLotteryWindow.focus();
				});
			} else if (result.code == "error") {
				showTip(result.data);
			}
		}
	});
}
