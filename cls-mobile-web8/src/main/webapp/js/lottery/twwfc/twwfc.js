
function TwwfcPage(){}
var twwfcPage = new TwwfcPage();

//基本参数
twwfcPage.param = {
	unitPrice : 2,  //每注价格
	kindDes : "台湾五分彩",
	kindName : 'TWWFC',  //彩种
	kindNameType : 'FFC', //大彩种拼写
	currentLotteryWins : new JS_OBJECT_MAP(),  //奖金映射 
	currentLotteryWinsLoaded : false,  //奖金映射是否加载完毕
	ommitData : null,  //遗漏数据
	hotColdData : null //冷热数据
};

//投注参数
twwfcPage.lotteryParam = {
    currentKindKey : 'WXDWD',    //当前所选择的玩法模式,默认是五星复式
	currentLotteryCount : 0,       //当前投注数目
	currentLotteryPrice : 0,       //当前投注价格,按元来计算
	currentLotteryTotalCount : 0,  //当前总投注数目
	codes : null,                  //投注号码
	codesStastic : new Array(),
	codesStr : ""   //投注下方显示的号码
};

/**
 * 获取开奖号码前台展现的字符串
 */
TwwfcPage.prototype.getOpenCodeStr = function(openCodeNum){
	var openCodeStr = "";
	if(isNotEmpty(openCodeNum)){
		var expectDay = openCodeNum.substring(0,openCodeNum.length - 3);
		var expectNum = openCodeNum.substring(openCodeNum.length - 3,openCodeNum.length);
		openCodeStr = expectDay + "-" + expectNum;
	}
	return openCodeStr;
};

/**
 * 展示当前正在投注的期号
 */
TwwfcPage.prototype.showCurrentLotteryCode = function(lotteryIssue){
	if(lotteryIssue != null){
		var openCodeStr = twwfcPage.getOpenCodeStr(lotteryIssue.lotteryNum);
		$('#nowNumber').text(openCodeStr);
	}
};

/**
 * 展示最新的开奖号码
 */
TwwfcPage.prototype.showLastLotteryCode = function(lotteryCode){
	if(lotteryCode != null){
		var lotteryNum = lotteryCode.lotteryNum;
		var openCodeStr = twwfcPage.getOpenCodeStr(lotteryCode.lotteryNum);
		
		$('#J-lottery-info-lastnumber').text(openCodeStr);
		
		var openCodeStr = "";
		openCodeStr += "<span class='ball color-red' id='lotteryNumber1'>" + lotteryCode.numInfo1 + "</span>";
		openCodeStr += "<span class='ball color-red' id='lotteryNumber2'>" + lotteryCode.numInfo2 + "</span>";
		openCodeStr += "<span class='ball color-red' id='lotteryNumber3'>" + lotteryCode.numInfo3 + "</span>";
		openCodeStr += "<span class='ball color-red' id='lotteryNumber4'>" + lotteryCode.numInfo4 + "</span>";
		openCodeStr += "<span class='ball color-red' id='lotteryNumber5'>" + lotteryCode.numInfo5 + "</span>";
		$(".lottery_run .child-muns").html(openCodeStr);
		
	}
};

/**
 * 查找奖金对照表
 */
TwwfcPage.prototype.getLotteryWins = function(){
	//还未加载奖金的
	if(!twwfcPage.param.currentLotteryWinsLoaded) {
		var jsonData = {
			"lotteryKind" : twwfcPage.param.kindName,	
		};
		$.ajax({
			type : "POST",
			// 设置请求为同步请求
			async : false,
			url : contextPath + "/lottery/getLotteryWins",
			data : JSON.stringify(jsonData),
			dataType : "json",
			contentType : 'application/json;charset=utf-8',
			success : function(result) {
				commonHandlerResult(result, this.url);
				var resultData = result.data;
				if (result.code == "ok") {
					for ( var key in resultData) {
						// 将奖金对照表缓存
						twwfcPage.param.currentLotteryWins.put(key, resultData[key]);
					}
					twwfcPage.param.currentLotteryWinsLoaded = true;
				} else if (result.code == "error") {
					showTip(resultData);
				}
			}
		});
	}
};

/**
 * 选好号码事件，添加号码到投注池中
 */
TwwfcPage.prototype.userLotteryNumForAddOrder = function(){
	//判断是否单式玩法
	var kindKey = twwfcPage.lotteryParam.currentKindKey;
	if(kindKey == 'WXDS' || kindKey == 'SXDS' || kindKey == 'QSZXDS' || kindKey == 'HSZXDS'
		|| kindKey == 'QEZXDS' || kindKey == "QEZXDS_G" || kindKey == "HEZXDS" || kindKey == "HEZXDS_G"
		|| kindKey == "RXSIZXDS" || kindKey == "RXEZXDS" || kindKey == "RXSZXDS"
	   ){  
		//需要走单式的验证
		return twwfcPage.singleLotteryNum();
	}else{
		return twwfcPage.userLotteryNum();
	}
};

/**
 * 用户添加投注号码（复式）
 */
TwwfcPage.prototype.userLotteryNum = function(){
	
	if(twwfcPage.lotteryParam.codes == null || twwfcPage.lotteryParam.codes.length == 0){
		showTip("号码选择不完整，请重新选择！");
		return false;
	}
	
	//添加进投注统计号码池 金额\注数\倍数\最高奖金\玩法描述\号码
	var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
	var currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get(twwfcPage.param.kindNameType +"_"+twwfcPage.lotteryParam.currentKindKey);
	
	//如果处于修改投注号码的状态
	if(lotteryCommonPage.lotteryParam.currentIsUpdateCodeStastic && lotteryCommonPage.lotteryParam.updateCodeStasticIndex != null){
		var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[lotteryCommonPage.lotteryParam.updateCodeStasticIndex];
		codeStastic[0] = twwfcPage.lotteryParam.currentLotteryPrice;
		codeStastic[1] = twwfcPage.lotteryParam.currentLotteryCount;
		codeStastic[2] = lotteryCommonPage.lotteryParam.beishu;
		codeStastic[3] = lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu);
		codeStastic[4] = lotteryCommonPage.lottertyDataDeal.lotteryKindMap.get(twwfcPage.lotteryParam.currentKindKey);
		codeStastic[5] = twwfcPage.lotteryParam.codes;
		codeStastic[6] = twwfcPage.param.kindNameType +"_"+twwfcPage.lotteryParam.currentKindKey;
		codeStastic[7] = codeStastic[7];

		//倍数更新
		lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStastic[7]); //先清除原来的投注号码的倍数
		lotteryCommonPage.lotteryParam.lotteryMultipleMap.put(codeStastic[7],lotteryCommonPage.lotteryParam.beishu);

		//将所选号码和玩法的映射放置map中
		var lotteryKindMap =  new JS_OBJECT_MAP();
		lotteryKindMap.put(twwfcPage.lotteryParam.currentKindKey,twwfcPage.lotteryParam.codes);
		lotteryCommonPage.lotteryParam.lotteryMap.remove(codeStastic[7]); //先清除原来的
		lotteryCommonPage.lotteryParam.lotteryMap.put(codeStastic[7],lotteryKindMap);
		
		//清空当前选中的号码
		//lotteryCommonPage.clearCurrentCodes();			
	
	}else{
		var isYetHave = false;
		var yetIndex = null;
		var codeStastics = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic;
		for(var i = codeStastics.length - 1; i >= 0 ; i--){
			 var codeStastic = codeStastics[i];
		     var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
			 if(kindKey == twwfcPage.lotteryParam.currentKindKey && codeStastic[5].toString() == twwfcPage.lotteryParam.codes){
				 isYetHave = true;
				 yetIndex = i;
				 break;
			 }
		}
		
		if(isYetHave && yetIndex != null){
			showTip("您选择的号码在号码篮已存在，将直接进行倍数累加!");
			var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[yetIndex];
			codeStastic[0] = (parseFloat(codeStastic[0]) + parseFloat(twwfcPage.lotteryParam.currentLotteryPrice)).toFixed(lotteryCommonPage.param.toFixedValue);
			codeStastic[1] = codeStastic[1];
			codeStastic[2] = parseInt(codeStastic[2]) + lotteryCommonPage.lotteryParam.beishu;
			codeStastic[3] = (parseFloat(codeStastic[3]) + parseFloat(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu))).toFixed(lotteryCommonPage.param.toFixedValue);
			codeStastic[4] = lotteryCommonPage.lottertyDataDeal.lotteryKindMap.get(twwfcPage.lotteryParam.currentKindKey);
			codeStastic[5] = twwfcPage.lotteryParam.codes;
			codeStastic[6] = twwfcPage.param.kindNameType +"_"+twwfcPage.lotteryParam.currentKindKey;
			codeStastic[7] = codeStastic[7];
			
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStastic[7]); //先清除原来的投注号码的倍数
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put(codeStastic[7],codeStastic[2]);
		}else{
			twwfcPage.lotteryParam.currentLotteryTotalCount++;  //投注数目递增
			//将所选号码和玩法的映射放置map中
			var lotteryKindMap =  new JS_OBJECT_MAP();
			lotteryKindMap.put(twwfcPage.lotteryParam.currentKindKey,twwfcPage.lotteryParam.codes);
			lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_"+twwfcPage.lotteryParam.currentLotteryTotalCount,lotteryKindMap);
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_"+twwfcPage.lotteryParam.currentLotteryTotalCount,lotteryCommonPage.lotteryParam.beishu);
			
			//进入投注池当中
			var codeRecord = new Array();
			codeRecord.push(twwfcPage.lotteryParam.currentLotteryPrice);
			codeRecord.push(twwfcPage.lotteryParam.currentLotteryCount);
			codeRecord.push(lotteryCommonPage.lotteryParam.beishu);
			codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu));
			codeRecord.push(lotteryCommonPage.lottertyDataDeal.lotteryKindMap.get(twwfcPage.lotteryParam.currentKindKey));
			codeRecord.push(twwfcPage.lotteryParam.codes);
			codeRecord.push(twwfcPage.param.kindNameType +"_"+twwfcPage.lotteryParam.currentKindKey); //标识玩法
			codeRecord.push("lottery_id_"+twwfcPage.lotteryParam.currentLotteryTotalCount); //标识该条记录的lotteryMap
			twwfcPage.lotteryParam.codesStastic.push(codeRecord);			
		}
	}
	
	//选好号码统计金额注数
	lotteryCommonPage.codeStastics(); 
	return true;
};



/**
 * 根据彩种玩法,获取所选的号码
 * @param kindKey
 */
TwwfcPage.prototype.getCodesByKindKey = function(){
    var codes = "";
    var lotteryCount = 0;
    //五星复式校验
	if(twwfcPage.lotteryParam.currentKindKey == 'WXFS'
		|| twwfcPage.lotteryParam.currentKindKey == 'WXZX60'
		|| twwfcPage.lotteryParam.currentKindKey == 'WXZX30'
		|| twwfcPage.lotteryParam.currentKindKey == 'WXZX20'
		|| twwfcPage.lotteryParam.currentKindKey == 'WXZX10'
		|| twwfcPage.lotteryParam.currentKindKey == 'WXZX5'
		|| twwfcPage.lotteryParam.currentKindKey == 'SXFS'
		|| twwfcPage.lotteryParam.currentKindKey == 'SXZX12'
		|| twwfcPage.lotteryParam.currentKindKey == 'SXZX6'
		|| twwfcPage.lotteryParam.currentKindKey == 'SXZX4'
		|| twwfcPage.lotteryParam.currentKindKey == 'QSZXFS'
		|| twwfcPage.lotteryParam.currentKindKey == 'HSZXFS'
		|| twwfcPage.lotteryParam.currentKindKey == 'QEZXFS'	
		|| twwfcPage.lotteryParam.currentKindKey == 'HEZXFS'			
		|| twwfcPage.lotteryParam.currentKindKey == 'HEZXFS'
		|| twwfcPage.lotteryParam.currentKindKey == 'QEDXDS'
		|| twwfcPage.lotteryParam.currentKindKey == 'HEDXDS'
	){
	    var numArrays = new Array();
        //获取 选择的号码
		$(".child-content .content").each(function(i){
			var lis = $(this).children();
			var numsStr = "";
			for(var i = 0; i < lis.length; i++){
				var li = lis[i];
				var aNum = $(li);
				if($(aNum).hasClass('on')){
					numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT;
				}
			}
			//去除最后一个分隔符
			if(numsStr.substring(numsStr.length - 1, numsStr.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
				numsStr = numsStr.substring(0, numsStr.length -1);
			}
			
			//复式不能进行空串处理
			if(twwfcPage.lotteryParam.currentKindKey != 'WXFS'
				&& twwfcPage.lotteryParam.currentKindKey != 'SXFS'
				&& twwfcPage.lotteryParam.currentKindKey != 'QSZXFS'
				&& twwfcPage.lotteryParam.currentKindKey != 'HSZXFS'
				&& twwfcPage.lotteryParam.currentKindKey != 'QEZXFS'
				&& twwfcPage.lotteryParam.currentKindKey != 'HEZXFS'){
				if(numsStr != ""){
					numArrays.push(numsStr);
				}
			}else{
				numArrays.push(numsStr);
			}
			
		});
		
		//处理投注下方显示的号码
		var codesDes = "";
		for(var i = 0; i < numArrays.length; i++){
			var numArray = numArrays[i];
			if(numArray.length == 0){ //为空
				continue;
			}
		    codesDes = codesDes + numArray + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		}
		codesDes = codesDes.substring(0, codesDes.length -1);
		twwfcPage.lotteryParam.codesStr = codesDes;
		
		if(twwfcPage.lotteryParam.currentKindKey == 'WXFS'
			|| twwfcPage.lotteryParam.currentKindKey == 'SXFS'
			|| twwfcPage.lotteryParam.currentKindKey == 'QSZXFS'
			|| twwfcPage.lotteryParam.currentKindKey == 'HSZXFS'
			|| twwfcPage.lotteryParam.currentKindKey == 'QEZXFS'
			|| twwfcPage.lotteryParam.currentKindKey == 'HEZXFS'){
			
			//四星复式
			if(twwfcPage.lotteryParam.currentKindKey == 'SXFS'){
				codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
			}
			//后三直选复式
            if(twwfcPage.lotteryParam.currentKindKey == 'HSZXFS'){
				codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            }

			//后二直选复式
            if(twwfcPage.lotteryParam.currentKindKey == 'HEZXFS'){
				codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            }
            
            lotteryCount = 1;
			//统计号码
			for(var i = 0; i < numArrays.length; i++){
				var numArray = numArrays[i];
				if(numArray.length == 0){ //为空,表示号码选择未合法
					twwfcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
					twwfcPage.lotteryParam.codes = null;  //号码置为空
					lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				    return;
				}
				
			    codes = codes + numArray + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				//统计当前数目
				lotteryCount = numArray.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length * lotteryCount;
			}
			
			//前三直选复式
            if(twwfcPage.lotteryParam.currentKindKey == 'QSZXFS'){
    			codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
    			        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;	
			}
			//前二直选复式
            if(twwfcPage.lotteryParam.currentKindKey == 'QEZXFS'){
    			codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
    			        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT  +
    			        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;	
			}
			codes = codes.substring(0, codes.length -1);	
		}else if(twwfcPage.lotteryParam.currentKindKey == 'QEDXDS' 
			|| twwfcPage.lotteryParam.currentKindKey == 'HEDXDS'){ 
			if(numArrays.length == 2){ //总共只有两位数
			   var num1Arrays = numArrays[0].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
			   var num2Arrays = numArrays[1].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
			   if((num1Arrays == null || num2Arrays == null) || (num1Arrays.length != 1 || num2Arrays.length != 1) || (num1Arrays[0].length == 0 || num2Arrays[0].length == 0)){
				   var codeDesc = numArrays[0] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + numArrays[1];
				   codeDesc = codeDesc.replaceAll("1","大");
				   codeDesc = codeDesc.replaceAll("2","小");
				   codeDesc = codeDesc.replaceAll("3","单");
				   codeDesc = codeDesc.replaceAll("4","双");
				   twwfcPage.lotteryParam.codesStr = codeDesc;
				   twwfcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				   twwfcPage.lotteryParam.codes = null;  //号码置为空
				   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				   return;
			   }else{
				   twwfcPage.lotteryParam.currentLotteryCount = 1; //大小单双一次只能一注
				   lotteryCount = 1;
				   codes = num1Arrays[0] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + num2Arrays[0];
				   var codeDesc = codes;
				   codeDesc = codeDesc.replaceAll("1","大");
				   codeDesc = codeDesc.replaceAll("2","小");
				   codeDesc = codeDesc.replaceAll("3","单");
				   codeDesc = codeDesc.replaceAll("4","双");
				   twwfcPage.lotteryParam.codesStr = codeDesc;
			   }
			}else{
				if(numArrays == null || numArrays.length == 0) {
					twwfcPage.lotteryParam.codesStr = $(".ball-number-current").text();
				} else {
					var codeDesc = numArrays[0];
					codeDesc = codeDesc.replaceAll("1","大");
				    codeDesc = codeDesc.replaceAll("2","小");
				    codeDesc = codeDesc.replaceAll("3","单");
				    codeDesc = codeDesc.replaceAll("4","双");
				    twwfcPage.lotteryParam.codesStr = codeDesc;
				}
				twwfcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				twwfcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}			
		}else if(twwfcPage.lotteryParam.currentKindKey == 'WXZX60'){
    		var totalCount = 0;
			if(numArrays.length == 2){ //总共只有两位数
			   var num1Arrays = numArrays[0].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
			   var num2Str = numArrays[1].replaceAll(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT,'');
               for(var i = 0; i < num1Arrays.length; i++){
            	   var tempDanCodes = num2Str.replaceAll(num1Arrays[i],'');
            	   var codeCount = tempDanCodes.split("").length;
            	   if(codeCount >= 3){
            		  totalCount += (codeCount * (codeCount - 1) * (codeCount - 2))/6;
            	   }
               }
			}else{
				twwfcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				twwfcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}

			lotteryCount = totalCount;
			if(lotteryCount == 0){  //投注数目为0
			    twwfcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				twwfcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
			//统计号码
			for(var i = 0; i < numArrays.length; i++){
				var numArray = numArrays[i];
				codes = codes + numArray + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
			}
			codes = codes.substring(0, codes.length -1);
		}else if(twwfcPage.lotteryParam.currentKindKey == 'WXZX30'){
    		var totalCount = 0;
			if(numArrays.length == 2){ //总共只有两位数
			   var num1Arrays = numArrays[0].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
			   var num2Str = numArrays[1].replaceAll(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT,'');
               for(var i = 0; i < num1Arrays.length; i++){
                   for(var j = i + 1; j < num1Arrays.length; j++){
                	   var tempDanCodes = num2Str.replaceAll(num1Arrays[i],'').replaceAll(num1Arrays[j],'');
                	   var codeCount = tempDanCodes.split("").length;
                	   if(codeCount >= 1){
                		  totalCount += codeCount;
                	   }               	    
                   }
               }
			}else{
				twwfcPage.lotteryParam.currentLotteryCount = 0;//此时未选注
				twwfcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}

			lotteryCount = totalCount;
			if(lotteryCount == 0){  //投注数目为0
			    twwfcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				twwfcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
			//统计号码
			for(var i = 0; i < numArrays.length; i++){
				var numArray = numArrays[i];
				codes = codes + numArray + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
			}
			codes = codes.substring(0, codes.length -1);
		}else if(twwfcPage.lotteryParam.currentKindKey == 'WXZX20'
			     || twwfcPage.lotteryParam.currentKindKey == 'SXZX12'){
    		var totalCount = 0;
			if(numArrays.length == 2){ //总共只有两位数
			   var num1Arrays = numArrays[0].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
			   var num2Str = numArrays[1].replaceAll(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT,'');
               for(var i = 0; i < num1Arrays.length; i++){
            	   var tempDanCodes = num2Str.replaceAll(num1Arrays[i],'');
            	   var codeCount = tempDanCodes.split("").length;
            	   if(codeCount >= 2){
            		  totalCount += (codeCount * (codeCount - 1))/2;
            	   }
               }
			}else{
				twwfcPage.lotteryParam.currentLotteryCount = 0;//此时未选注
				twwfcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}

			lotteryCount = totalCount;
			if(lotteryCount == 0){  //投注数目为0
			    twwfcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				twwfcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
			//统计号码
			for(var i = 0; i < numArrays.length; i++){
				var numArray = numArrays[i];
				codes = codes + numArray + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
			}
			codes = codes.substring(0, codes.length -1);
		}else if(twwfcPage.lotteryParam.currentKindKey == 'WXZX10' 
			     || twwfcPage.lotteryParam.currentKindKey == 'WXZX5'
			     || twwfcPage.lotteryParam.currentKindKey == 'SXZX6'
			     || twwfcPage.lotteryParam.currentKindKey == 'SXZX4'){
    		var totalCount = 0;
			if(numArrays.length == 2 || (numArrays.length == 1 && twwfcPage.lotteryParam.currentKindKey == 'SXZX6')){ //总共只有两位数
			   if(numArrays.length == 1){
				   var num1Arrays = numArrays[0].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
				   var tempCount = num1Arrays.length;
         		   totalCount = (tempCount * (tempCount - 1))/2;
			   }else{
				   var num1Arrays = numArrays[0].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
				   var num2Str = numArrays[1].replaceAll(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT,'');
	               for(var i = 0; i < num1Arrays.length; i++){
	            	   var tempDanCodes = num2Str.replaceAll(num1Arrays[i],'');
	            	   var codeCount = tempDanCodes.split("").length;
	            	   if(codeCount >= 1){
	            		  totalCount += codeCount;
	            	   }
	               }				   
			   } 
			}else{
				twwfcPage.lotteryParam.currentLotteryCount = 0;//此时未选注
				twwfcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}

			lotteryCount = totalCount;
			if(lotteryCount == 0){  //投注数目为0
			    twwfcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				twwfcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
			
			if(twwfcPage.lotteryParam.currentKindKey == 'SXZX6'){
				//统计号码
				for(var i = 0; i < numArrays.length; i++){
					var numArray = numArrays[i];
					var codeArrays = numArray.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
					for(var j = 0; j < codeArrays.length; j++){
						codes +=  codeArrays[j] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
					}
				}
				codes = codes.substring(0, codes.length -1);
			}else{
				//统计号码
				for(var i = 0; i < numArrays.length; i++){
					var numArray = numArrays[i];
					codes = codes + numArray + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				}
				codes = codes.substring(0, codes.length -1);
			}
		}else{
		  showTip("未知参数1.");	
		}
	}else if(twwfcPage.lotteryParam.currentKindKey == 'WXDWD' || 
			 twwfcPage.lotteryParam.currentKindKey == 'RXE'  || 
			 twwfcPage.lotteryParam.currentKindKey == 'RXS'  ||
			 twwfcPage.lotteryParam.currentKindKey == 'RXSI'){
		var lotteryNumMap = new JS_OBJECT_MAP();  //选择号码位数映射
        //获取 选择的号码
		$(".child-content .content").each(function(i){
			var lis = $(this).children();
			var numsStr = "";
			for(var i = 0; i < lis.length; i++){
				var li = lis[i];
				var aNum = $(li);
				if($(aNum).hasClass('on')){
					numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT;
					lotteryNumMap.remove($(aNum).attr('name'));
					lotteryNumMap.put($(aNum).attr('name'),numsStr);
				}
			}
		}); 
		//标识是否有选择对应的位数
		var totalCount = 0;
		var isBallNum_0_match = false;
		var isBallNum_1_match = false;
		var isBallNum_2_match = false;
		var isBallNum_3_match = false;
		var isBallNum_4_match = false;
		
		if(twwfcPage.lotteryParam.currentKindKey == 'WXDWD'){
			var lotteryNumMapKeys = lotteryNumMap.keys();
			//字符串截取
			for(var i = 0; i < lotteryNumMapKeys.length; i++){
				var mapValue = lotteryNumMap.get(lotteryNumMapKeys[i]);
				//去除最后一个分隔符
				if(mapValue.substring(mapValue.length - 1, mapValue.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
					mapValue = mapValue.substring(0, mapValue.length -1);
				}
				lotteryNumMap.remove(lotteryNumMapKeys[i]);
				lotteryNumMap.put(lotteryNumMapKeys[i],mapValue);
				
				if(lotteryNumMapKeys[i] == "ballNum_0_match"){
					isBallNum_0_match = true;
				}else if(lotteryNumMapKeys[i] == "ballNum_1_match"){
					isBallNum_1_match = true;
				}else if(lotteryNumMapKeys[i] == "ballNum_2_match"){
					isBallNum_2_match = true;
				}else if(lotteryNumMapKeys[i] == "ballNum_3_match"){
					isBallNum_3_match = true;
				}else if(lotteryNumMapKeys[i] == "ballNum_4_match"){
					isBallNum_4_match = true;
				}else{
					showTip("未知的号码位数");
				}
				totalCount += mapValue.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length;
			}			
		}else if(twwfcPage.lotteryParam.currentKindKey == 'RXE'){
			var lotteryNumMapKeys = lotteryNumMap.keys();
			if(lotteryNumMapKeys.length >= 2){
				for(var i = 0; i < lotteryNumMapKeys.length; i++){
					for(var j = (i + 1); j < lotteryNumMapKeys.length;j++){
						var map1Value = lotteryNumMap.get(lotteryNumMapKeys[i]);
						var map2Value = lotteryNumMap.get(lotteryNumMapKeys[j]);

						if(lotteryNumMapKeys[i] == "ballNum_0_match"){
							isBallNum_0_match = true;
						}else if(lotteryNumMapKeys[i] == "ballNum_1_match"){
							isBallNum_1_match = true;
						}else if(lotteryNumMapKeys[i] == "ballNum_2_match"){
							isBallNum_2_match = true;
						}else if(lotteryNumMapKeys[i] == "ballNum_3_match"){
							isBallNum_3_match = true;
						}else if(lotteryNumMapKeys[i] == "ballNum_4_match"){
							isBallNum_4_match = true;
						}else{
							showTip("未知的号码位数");
						}
						if(lotteryNumMapKeys[j] == "ballNum_0_match"){
							isBallNum_0_match = true;
						}else if(lotteryNumMapKeys[j] == "ballNum_1_match"){
							isBallNum_1_match = true;
						}else if(lotteryNumMapKeys[j] == "ballNum_2_match"){
							isBallNum_2_match = true;
						}else if(lotteryNumMapKeys[j] == "ballNum_3_match"){
							isBallNum_3_match = true;
						}else if(lotteryNumMapKeys[j] == "ballNum_4_match"){
							isBallNum_4_match = true;
						}else{
							showTip("未知的号码位数");
						}
						
						//去除最后一个分隔符
						if(map1Value.substring(map1Value.length - 1, map1Value.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
							map1Value = map1Value.substring(0, map1Value.length -1);
						}
						if(map2Value.substring(map2Value.length - 1, map2Value.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
							map2Value = map2Value.substring(0, map2Value.length -1);
						}
						lotteryNumMap.remove(lotteryNumMapKeys[i]);
						lotteryNumMap.put(lotteryNumMapKeys[i],map1Value);
						lotteryNumMap.remove(lotteryNumMapKeys[j]);
						lotteryNumMap.put(lotteryNumMapKeys[j],map2Value);
						
						var oneLength = map1Value.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length;
						var twoLength = map2Value.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length;
						
						totalCount += (oneLength * twoLength);
					}
				}
			  }
			}else if(twwfcPage.lotteryParam.currentKindKey == 'RXS'){
				var lotteryNumMapKeys = lotteryNumMap.keys();
				if(lotteryNumMapKeys.length >= 3){
					for(var i = 0; i < lotteryNumMapKeys.length; i++){
						for(var j = (i + 1); j < lotteryNumMapKeys.length;j++){
							for(var z = (j + 1); z < lotteryNumMapKeys.length;z++){
								var map1Value = lotteryNumMap.get(lotteryNumMapKeys[i]);
								var map2Value = lotteryNumMap.get(lotteryNumMapKeys[j]);
								var map3Value = lotteryNumMap.get(lotteryNumMapKeys[z]);

								//去除最后一个分隔符
								if(map1Value.substring(map1Value.length - 1, map1Value.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
									map1Value = map1Value.substring(0, map1Value.length -1);
								}
								if(map2Value.substring(map2Value.length - 1, map2Value.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
									map2Value = map2Value.substring(0, map2Value.length -1);
								}
								if(map3Value.substring(map3Value.length - 1, map3Value.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
									map3Value = map3Value.substring(0, map3Value.length -1);
								}
								lotteryNumMap.remove(lotteryNumMapKeys[i]);
								lotteryNumMap.put(lotteryNumMapKeys[i],map1Value);
								lotteryNumMap.remove(lotteryNumMapKeys[j]);
								lotteryNumMap.put(lotteryNumMapKeys[j],map2Value);
								lotteryNumMap.remove(lotteryNumMapKeys[z]);
								lotteryNumMap.put(lotteryNumMapKeys[z],map3Value);
								
								var oneLength = map1Value.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length;
								var twoLength = map2Value.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length;
								var sanLength = map3Value.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length;
								
								totalCount += (oneLength * twoLength * sanLength);		
								
								if(lotteryNumMapKeys[i] == "ballNum_0_match"){
									isBallNum_0_match = true;
								}else if(lotteryNumMapKeys[i] == "ballNum_1_match"){
									isBallNum_1_match = true;
								}else if(lotteryNumMapKeys[i] == "ballNum_2_match"){
									isBallNum_2_match = true;
								}else if(lotteryNumMapKeys[i] == "ballNum_3_match"){
									isBallNum_3_match = true;
								}else if(lotteryNumMapKeys[i] == "ballNum_4_match"){
									isBallNum_4_match = true;
								}else{
									showTip("未知的号码位数");
								}
								if(lotteryNumMapKeys[j] == "ballNum_0_match"){
									isBallNum_0_match = true;
								}else if(lotteryNumMapKeys[j] == "ballNum_1_match"){
									isBallNum_1_match = true;
								}else if(lotteryNumMapKeys[j] == "ballNum_2_match"){
									isBallNum_2_match = true;
								}else if(lotteryNumMapKeys[j] == "ballNum_3_match"){
									isBallNum_3_match = true;
								}else if(lotteryNumMapKeys[j] == "ballNum_4_match"){
									isBallNum_4_match = true;
								}else{
									showTip("未知的号码位数");
								}
								
								if(lotteryNumMapKeys[z] == "ballNum_0_match"){
									isBallNum_0_match = true;
								}else if(lotteryNumMapKeys[z] == "ballNum_1_match"){
									isBallNum_1_match = true;
								}else if(lotteryNumMapKeys[z] == "ballNum_2_match"){
									isBallNum_2_match = true;
								}else if(lotteryNumMapKeys[z] == "ballNum_3_match"){
									isBallNum_3_match = true;
								}else if(lotteryNumMapKeys[z] == "ballNum_4_match"){
									isBallNum_4_match = true;
								}else{
									showTip("未知的号码位数");
								}
							}
						}
					}
				  }
				}else if(twwfcPage.lotteryParam.currentKindKey == 'RXSI'){
					var lotteryNumMapKeys = lotteryNumMap.keys();
					if(lotteryNumMapKeys.length >= 4){
						for(var i = 0; i < lotteryNumMapKeys.length; i++){
							for(var j = (i + 1); j < lotteryNumMapKeys.length;j++){
								for(var z = (j + 1); z < lotteryNumMapKeys.length;z++){
									for(var k = (z + 1); k < lotteryNumMapKeys.length;k++){
										var map1Value = lotteryNumMap.get(lotteryNumMapKeys[i]);
										var map2Value = lotteryNumMap.get(lotteryNumMapKeys[j]);
										var map3Value = lotteryNumMap.get(lotteryNumMapKeys[z]);
										var map4Value = lotteryNumMap.get(lotteryNumMapKeys[k]);

										//去除最后一个分隔符
										if(map1Value.substring(map1Value.length - 1, map1Value.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
											map1Value = map1Value.substring(0, map1Value.length -1);
										}
										if(map2Value.substring(map2Value.length - 1, map2Value.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
											map2Value = map2Value.substring(0, map2Value.length -1);
										}
										if(map3Value.substring(map3Value.length - 1, map3Value.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
											map3Value = map3Value.substring(0, map3Value.length -1);
										}
										if(map4Value.substring(map4Value.length - 1, map4Value.length) == lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT){
											map4Value = map4Value.substring(0, map4Value.length -1);
										}
										lotteryNumMap.remove(lotteryNumMapKeys[i]);
										lotteryNumMap.put(lotteryNumMapKeys[i],map1Value);
										lotteryNumMap.remove(lotteryNumMapKeys[j]);
										lotteryNumMap.put(lotteryNumMapKeys[j],map2Value);
										lotteryNumMap.remove(lotteryNumMapKeys[z]);
										lotteryNumMap.put(lotteryNumMapKeys[z],map3Value);
										lotteryNumMap.remove(lotteryNumMapKeys[k]);
										lotteryNumMap.put(lotteryNumMapKeys[k],map4Value);
										
										var oneLength = map1Value.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length;
										var twoLength = map2Value.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length;
										var sanLength = map3Value.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length;
										var siLength = map4Value.split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT).length;
										
										totalCount += (oneLength * twoLength * sanLength * siLength);		
										
										if(lotteryNumMapKeys[i] == "ballNum_0_match"){
											isBallNum_0_match = true;
										}else if(lotteryNumMapKeys[i] == "ballNum_1_match"){
											isBallNum_1_match = true;
										}else if(lotteryNumMapKeys[i] == "ballNum_2_match"){
											isBallNum_2_match = true;
										}else if(lotteryNumMapKeys[i] == "ballNum_3_match"){
											isBallNum_3_match = true;
										}else if(lotteryNumMapKeys[i] == "ballNum_4_match"){
											isBallNum_4_match = true;
										}else{
											showTip("未知的号码位数");
										}
										if(lotteryNumMapKeys[j] == "ballNum_0_match"){
											isBallNum_0_match = true;
										}else if(lotteryNumMapKeys[j] == "ballNum_1_match"){
											isBallNum_1_match = true;
										}else if(lotteryNumMapKeys[j] == "ballNum_2_match"){
											isBallNum_2_match = true;
										}else if(lotteryNumMapKeys[j] == "ballNum_3_match"){
											isBallNum_3_match = true;
										}else if(lotteryNumMapKeys[j] == "ballNum_4_match"){
											isBallNum_4_match = true;
										}else{
											showTip("未知的号码位数");
										}
										
										if(lotteryNumMapKeys[z] == "ballNum_0_match"){
											isBallNum_0_match = true;
										}else if(lotteryNumMapKeys[z] == "ballNum_1_match"){
											isBallNum_1_match = true;
										}else if(lotteryNumMapKeys[z] == "ballNum_2_match"){
											isBallNum_2_match = true;
										}else if(lotteryNumMapKeys[z] == "ballNum_3_match"){
											isBallNum_3_match = true;
										}else if(lotteryNumMapKeys[z] == "ballNum_4_match"){
											isBallNum_4_match = true;
										}else{
											showTip("未知的号码位数");
										}
										
										if(lotteryNumMapKeys[k] == "ballNum_0_match"){
											isBallNum_0_match = true;
										}else if(lotteryNumMapKeys[k] == "ballNum_1_match"){
											isBallNum_1_match = true;
										}else if(lotteryNumMapKeys[k] == "ballNum_2_match"){
											isBallNum_2_match = true;
										}else if(lotteryNumMapKeys[k] == "ballNum_3_match"){
											isBallNum_3_match = true;
										}else if(lotteryNumMapKeys[k] == "ballNum_4_match"){
											isBallNum_4_match = true;
										}else{
											showTip("未知的号码位数");
										}
									}
								}
							}
						}
			    }
		   }else{
			   showTip("未知参数3");
		   }
		
		//统计投注数目
		if(totalCount == 0){
			twwfcPage.lotteryParam.currentLotteryCount = 0;//此时未选注
			twwfcPage.lotteryParam.codes = null;  //号码置为空
			lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			return;
		}else{
			lotteryCount = totalCount;
		}
		
		//万位
		if(isBallNum_0_match){
			codes = codes + lotteryNumMap.get("ballNum_0_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		}else{
			codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		}
        //千位
		if(isBallNum_1_match){
			codes = codes + lotteryNumMap.get("ballNum_1_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		}else{
			codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		}
        //百位
		if(isBallNum_2_match){
			codes = codes + lotteryNumMap.get("ballNum_2_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		}else{
			codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		}		
        //十位
		if(isBallNum_3_match){
			codes = codes + lotteryNumMap.get("ballNum_3_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT ;
		}else{
			codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		}	
        //个位
		if(isBallNum_4_match){
			codes = codes + lotteryNumMap.get("ballNum_4_match") + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		}else{
			codes = codes + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
		}	
		codes = codes.substring(0, codes.length -1);
		//处理投注下方显示的号码
		twwfcPage.lotteryParam.codesStr = codes;
	}else if(twwfcPage.lotteryParam.currentKindKey == 'WXZX120'
		|| twwfcPage.lotteryParam.currentKindKey == 'WXYMBDW'
		|| twwfcPage.lotteryParam.currentKindKey == 'WXEMBDW'
		|| twwfcPage.lotteryParam.currentKindKey == 'WXSMBDW'
		|| twwfcPage.lotteryParam.currentKindKey == 'SXZX24'
		|| twwfcPage.lotteryParam.currentKindKey == 'SXYMBDW'		
		|| twwfcPage.lotteryParam.currentKindKey == 'SXEMBDW'
		|| twwfcPage.lotteryParam.currentKindKey == 'QSZXHZ'
	    || twwfcPage.lotteryParam.currentKindKey == 'QSZXHZ_G'
	    || twwfcPage.lotteryParam.currentKindKey == 'QSZS'
	    || twwfcPage.lotteryParam.currentKindKey == 'QSZL'
	    || twwfcPage.lotteryParam.currentKindKey == 'QSYMBDW'
	    || twwfcPage.lotteryParam.currentKindKey == 'QSEMBDW'
		|| twwfcPage.lotteryParam.currentKindKey == 'HSZXHZ'
	    || twwfcPage.lotteryParam.currentKindKey == 'HSZXHZ_G'
	    || twwfcPage.lotteryParam.currentKindKey == 'HSZS'
	    || twwfcPage.lotteryParam.currentKindKey == 'HSZL'
	    || twwfcPage.lotteryParam.currentKindKey == 'HSYMBDW'
	    || twwfcPage.lotteryParam.currentKindKey == 'HSEMBDW'
	    || twwfcPage.lotteryParam.currentKindKey == 'HSEMBDW'	    	
		|| twwfcPage.lotteryParam.currentKindKey == 'QEZXHZ'
		|| twwfcPage.lotteryParam.currentKindKey == 'QEZXFS_G'
		|| twwfcPage.lotteryParam.currentKindKey == 'QEZXHZ_G'
	    || twwfcPage.lotteryParam.currentKindKey == 'HEZXHZ'
		|| twwfcPage.lotteryParam.currentKindKey == 'HEZXFS_G'
		|| twwfcPage.lotteryParam.currentKindKey == 'HEZXHZ_G'		
		|| twwfcPage.lotteryParam.currentKindKey == 'YXQY'
		|| twwfcPage.lotteryParam.currentKindKey == 'YXHY'	
	){  
		var numsStr = "";
        //获取 选择的号码
		$(".child-content .content .munbtn").each(function(i){
			var aNum = $(this);
			if(aNum.hasClass('on')){
				numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
			}
		});
		//去除最后一个分隔符
		if(numsStr.substring(numsStr.length - 1, numsStr.length) == lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT){
			numsStr = numsStr.substring(0, numsStr.length -1);
		}
		//处理投注下方显示的号码
		twwfcPage.lotteryParam.codesStr = numsStr;
		
		//空串拦截
		if(numsStr == ""){
		   twwfcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
		   twwfcPage.lotteryParam.codes = null;  //号码置为空
		   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
		   return;
		}
		
		//五星组选120
		if(twwfcPage.lotteryParam.currentKindKey == 'WXZX120'){
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 5){ //号码数目不足
			   twwfcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   twwfcPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
		    }
			lotteryCount =  (codeCount * (codeCount - 1) * (codeCount - 2) * (codeCount - 3) * (codeCount - 4)) / (5 * 4 * 3 * 2 * 1);
		}else if(twwfcPage.lotteryParam.currentKindKey == 'WXYMBDW'
			    || twwfcPage.lotteryParam.currentKindKey == 'SXYMBDW'
			    || twwfcPage.lotteryParam.currentKindKey == 'QSYMBDW'
			    || twwfcPage.lotteryParam.currentKindKey == 'HSYMBDW'){
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 1){ //号码数目不足
			   twwfcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   twwfcPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
			}			
			lotteryCount =  codeCount;
		}else if(twwfcPage.lotteryParam.currentKindKey == 'WXEMBDW'
			    || twwfcPage.lotteryParam.currentKindKey == 'SXEMBDW'
				|| twwfcPage.lotteryParam.currentKindKey == 'QSEMBDW'
				|| twwfcPage.lotteryParam.currentKindKey == 'HSEMBDW'
				|| twwfcPage.lotteryParam.currentKindKey == 'QEZXFS_G'					
				|| twwfcPage.lotteryParam.currentKindKey == 'HEZXFS_G'
		){
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 2){ //号码数目不足
			   twwfcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   twwfcPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
		    }
			lotteryCount =  (codeCount * (codeCount - 1)) / (2 * 1);
		}else if(twwfcPage.lotteryParam.currentKindKey == 'QSZS'
		      || twwfcPage.lotteryParam.currentKindKey == 'HSZS'
		      ){
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 2){ //号码数目不足
			   twwfcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   twwfcPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
		    }
			lotteryCount =  (codeCount * (codeCount - 1));			
		}else if(twwfcPage.lotteryParam.currentKindKey == 'WXSMBDW'
			    || twwfcPage.lotteryParam.currentKindKey == 'QSZL'
			    || twwfcPage.lotteryParam.currentKindKey == 'HSZL'){
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 3){ //号码数目不足
			   twwfcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   twwfcPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
		    }
			lotteryCount =  (codeCount * (codeCount - 1) * (codeCount - 2)) / (3 * 2 * 1);
		}else if(twwfcPage.lotteryParam.currentKindKey == 'SXZX24'){
			var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
			if(codeCount < 4){ //号码数目不足
			   twwfcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			   twwfcPage.lotteryParam.codes = null;  //号码置为空
			   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			   return;
		    }
			lotteryCount =  (codeCount * (codeCount - 1) * (codeCount - 2) * (codeCount - 3)) / (4 * 3 * 2 * 1);
		}else if(twwfcPage.lotteryParam.currentKindKey == 'QSZXHZ' 
			   || twwfcPage.lotteryParam.currentKindKey == 'HSZXHZ'){
			var codeArray = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT); //号码数目
			if(numsStr == "" || codeArray == null || codeArray.length == 0){ //号码数目不足
			    twwfcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				twwfcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
            for(var i = 0; i < codeArray.length; i++){
    			lotteryCount +=  lotteryCommonPage.lottertyDataDeal.getLotteryCountByShanZhiHeZhi(codeArray[i]);
            }
		}else if(twwfcPage.lotteryParam.currentKindKey == 'QSZXHZ_G'
			  || twwfcPage.lotteryParam.currentKindKey == 'HSZXHZ_G'){
			var codeArray = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT); //号码数目
			if(numsStr == "" || codeArray == null || codeArray.length == 0){ //号码数目不足
			    twwfcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				twwfcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
            for(var i = 0; i < codeArray.length; i++){
    			lotteryCount +=  lotteryCommonPage.lottertyDataDeal.getLotteryCountByShanZuHeZhi(codeArray[i]);
            }
		}else if(twwfcPage.lotteryParam.currentKindKey == 'QEZXHZ'
			  || twwfcPage.lotteryParam.currentKindKey == 'HEZXHZ'){
			var codeArray = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT); //号码数目
			if(numsStr == "" || codeArray == null || codeArray.length == 0){ //号码数目不足
			    twwfcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				twwfcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
            for(var i = 0; i < codeArray.length; i++){
    			lotteryCount +=  lotteryCommonPage.lottertyDataDeal.getLotteryCountByErZhiHeZhi(codeArray[i]);
            }
		}else if(twwfcPage.lotteryParam.currentKindKey == 'QEZXHZ_G'
			  || twwfcPage.lotteryParam.currentKindKey == 'HEZXHZ_G'){
			var codeArray = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT); //号码数目
			if(numsStr == "" || codeArray == null || codeArray.length == 0){ //号码数目不足
			    twwfcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				twwfcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
            for(var i = 0; i < codeArray.length; i++){
    			lotteryCount +=  lotteryCommonPage.lottertyDataDeal.getLotteryCountByErZuHeZhi(codeArray[i]);
            }
		}else if(twwfcPage.lotteryParam.currentKindKey == 'YXQY'){
			var codeArray = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT); //号码数目
			if(numsStr == "" || codeArray == null || codeArray.length == 0){ //号码数目不足
			    twwfcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				twwfcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
			lotteryCount = codeArray.length;
			var temp = "";
			for(var i = 0; i < codeArray.length; i++){
				temp += codeArray[i] + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT;
			}
			numsStr = temp.substring(0, temp.length -1);
			numsStr = numsStr + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
	        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT  +
	        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
	        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;	
		}else if(twwfcPage.lotteryParam.currentKindKey == 'YXHY'){
			var codeArray = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT); //号码数目
			if(numsStr == "" || codeArray == null || codeArray.length == 0){ //号码数目不足
			    twwfcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
				twwfcPage.lotteryParam.codes = null;  //号码置为空
				lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
				return;
			}
			lotteryCount = codeArray.length;
			var temp = "";
			for(var i = 0; i < codeArray.length; i++){
				temp += codeArray[i] + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT;
			}
			numsStr = temp.substring(0, temp.length -1);
			numsStr = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
	        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT  +
	        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
	        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + numsStr;
		}else{
		  showTip("未知参数2.");	
		}
		codes = numsStr;//记录投注号码			
	}else if(twwfcPage.lotteryParam.currentKindKey=="ZHWQLHSH"||twwfcPage.lotteryParam.currentKindKey=="ZHDN"||
			twwfcPage.lotteryParam.currentKindKey=="ZHQS"||twwfcPage.lotteryParam.currentKindKey=="ZHZS"||
			twwfcPage.lotteryParam.currentKindKey=="ZHHS")
	{	//斗牛
		var numsStr = "";
		$(".munbtn").each(function(i){
			var lis = $(this);
			for(var i = 0; i < lis.length; i++){
				var li = lis[i];
				var aNum = $(li);
				if($(aNum).hasClass('on')){
					numsStr += $(aNum).attr('data-realvalue') + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
				}
			}
		});
		//去除最后一个分隔符
		if(numsStr.substring(numsStr.length - 1, numsStr.length) == lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT){
			numsStr = numsStr.substring(0, numsStr.length -1);
		}
		//空串拦截
		if(numsStr == ""){
		   twwfcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
		   twwfcPage.lotteryParam.codes = null;  //号码置为空
		   lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
		   return;
		}
		var codeCount = numsStr.split(lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT).length; //号码数目
		if(codeCount < 1){ //号码数目不足
			twwfcPage.lotteryParam.currentLotteryCount = 0;//此时未选注 
			twwfcPage.lotteryParam.codes = null;  //号码置为空
			lotteryCommonPage.currentCodesStastic(); //统计当前投注数目和价格
			return;
		}		
		lotteryCount =  codeCount;
		codes = numsStr;//记录投注号码
		//处理投注下方显示的号码
		twwfcPage.lotteryParam.codesStr = numsStr;
	}else{
		lotteryCount = 0;
		codes = null;
		showTip("玩法参数传递不合法.");
	}
	
	//存储当前统计的投注数目
	twwfcPage.lotteryParam.currentLotteryCount = lotteryCount;  
	//当前投注号码
	twwfcPage.lotteryParam.codes = codes;
	//统计当前投注数目和价格
	lotteryCommonPage.currentCodesStastic();
	
};




/**
 * 号码拼接
 * @param num
 */
TwwfcPage.prototype.codeStastics = function(codeStastic,index){
	 
	 var codeStr = "";
	 var codeDesc = codeStastic[5]; 
	 codeDesc = codeDesc.toString();
	 var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
	 if(kindKey == "QEDXDS" || kindKey == "HEDXDS"){
		 codeDesc = codeDesc.replaceAll("1","大");
		 codeDesc = codeDesc.replaceAll("2","小");
		 codeDesc = codeDesc.replaceAll("3","单");
		 codeDesc = codeDesc.replaceAll("4","双");
	 }
	 if(codeDesc.length < 15){
		 codeStr += "<span>"+codeDesc.replace(/\&/g," ")+"</span>";
	 }else{
		 codeStr +=  "<span>" + codeDesc.substring(0,15).replace(/\&/g," ") + "...</span>";
		 codeStr += "<a onclick='event.stopPropagation();twwfcPage.showDsMsgDialog(this,"+index+")'>详情</a>";
		 codeStr += "</span>";
	 }
	 
	 var str = "";
	 //单式不需要更新操作
     var kindKey = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
	 if(kindKey == 'WXDS' || kindKey == 'SXDS' || kindKey == 'QSZXDS' || kindKey == 'HSZXDS'
			|| kindKey == 'QEZXDS' || kindKey == "QEZXDS_G" || kindKey == "HEZXDS" || kindKey == "HEZXDS_G"
			|| kindKey == "RXSIZXDS" || kindKey == "RXEZXDS" || kindKey == "RXSZXDS"){
		 str += "<li>";
	 }else{
		 str += "<li onclick='lotteryCommonPage.updateCodeStastic(this,"+index+")'>";
	 }
	 str += "<div class='li-child li-title'>" + codeStastic[4] + codeStr + "</div>";
	 str += "<div class='li-child li-money'>¥"+ codeStastic[0] + "元</div>";
	 str += "<div class='li-child li-strip'>" + codeStastic[1] + "注</div>";
	 str += "<div class='li-child li-multiple'>" + codeStastic[2] + "倍</div>";
	 str += "<div class='li-child li-addmoney'>" + codeStastic[3] + "元</div>";
	 str += "<div class='li-child li-close' data-value='"+index+"' name='deleteCurrentCodeStastic'><img src='"+ contextPath +"/front/images/lottery/close.png' /></div>" 
	 str += "</li>";
	 return str;
};

/**
 * 展现投注号码
 */
TwwfcPage.prototype.showDsMsgDialog = function(obj,index){
	var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[index];
	var kindKeyTop = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
	
	$("#kindPlayDes").text(codeStastic[4]);
	if(kindKeyTop == "WXDS" || kindKeyTop == "WXFS"
		|| kindKeyTop == "WXZX120" || kindKeyTop == "WXZX60" || kindKeyTop == "WXZX30"
		|| kindKeyTop == "WXZX20" || kindKeyTop == "WXZX10" || kindKeyTop == "WXZX5"
		|| kindKeyTop == "WXYMBDW" || kindKeyTop == "WXEMBDW" || kindKeyTop == "WXSMBDW"
		|| kindKeyTop == "WXDWD" || kindKeyTop == "RXE" || kindKeyTop == "RXS" || kindKeyTop == "RXSI"    
		|| kindKeyTop == "RXSIZXDS" || kindKeyTop == "RXEZXDS" || kindKeyTop == "RXSZXDS"
		){     
		$("#kindPlayPositionDes").text("位置：万位、千位、百位、十位、个位");
	}else if(kindKeyTop == "SXDS" || kindKeyTop == "SXFS" || kindKeyTop == "SXZX24"
		     || kindKeyTop == "SXZX12" || kindKeyTop == "SXZX6" || kindKeyTop == "SXZX4"
		     || kindKeyTop == "SXYMBDW" || kindKeyTop == "SXEMBDW"){
		$("#kindPlayPositionDes").text("位置：千位、百位、十位、个位");
	}else if(kindKeyTop == "QSZXFS" || kindKeyTop == "QSZXDS" || kindKeyTop == "QSZXHZ"
		|| kindKeyTop == "QSZXHZ_G" || kindKeyTop == "QSZS" || kindKeyTop == "QSZL"
		|| kindKeyTop == "QSYMBDW" || kindKeyTop == "QSEMBDW"){
		$("#kindPlayPositionDes").text("位置：万位、千位、百位");
	}else if(kindKeyTop == "HSZXFS" || kindKeyTop == "HSZXDS" || kindKeyTop == "HSZXHZ"
		|| kindKeyTop == "HSZXHZ_G" || kindKeyTop == "HSZS" || kindKeyTop == "HSZL"
		|| kindKeyTop == "HSYMBDW" || kindKeyTop == "HSEMBDW"){
		$("#kindPlayPositionDes").text("位置：百位、十位、个位");
	}else if(kindKeyTop == "QEZXDS" || kindKeyTop == "QEZXFS" || kindKeyTop == "QEZXHZ"
		|| kindKeyTop == "QEZXFS_G" || kindKeyTop == "QEZXDS_G" || kindKeyTop == "QEZXHZ_G"
			|| kindKeyTop == "QEDXDS"){ 
		$("#kindPlayPositionDes").text("位置：万位、千位");
	}else if(kindKeyTop == "HEZXFS" || kindKeyTop == "HEZXDS" || kindKeyTop == "HEZXHZ"
		|| kindKeyTop == "HEZXFS_G" || kindKeyTop == "HEZXDS_G" || kindKeyTop == "HEZXHZ_G"
			|| kindKeyTop == "HEDXDS"){
		$("#kindPlayPositionDes").text("位置：十位、个位"); 
	}else{
		showTip("该单式玩法的位置还没配置.");
		return;
	}

	
	var modelTxt = $("#returnPercent").text();
	if(lotteryCommonPage.lotteryParam.awardModel == 'YUAN'){
		$("#kindPlayModel").text("元模式，" + modelTxt);
	}else if(lotteryCommonPage.lotteryParam.awardModel == 'JIAO'){
		$("#kindPlayModel").text("角模式，" + modelTxt);
	}else if(lotteryCommonPage.lotteryParam.awardModel == 'FEN'){
		$("#kindPlayModel").text("分模式，" + modelTxt);
	}else if(lotteryCommonPage.lotteryParam.awardModel == 'LI'){
		$("#kindPlayModel").text("厘模式，" + modelTxt);
	}else{
      showTip("该投注模式不合法.");
	}

	$("#kindPlayPriceMsg").text(codeStastic[1] + "注，" + lotteryCommonPage.lotteryParam.beishu + "倍, 共计" + codeStastic[0] + "元");
	$("#kindPlayCodeDes").val(codeStastic[5].replace(/\&/g," "));
	 
	//展示单式投注号码对话框
	$("#dsContentShowDialog").stop(false,true).delay(200).fadeIn(500);
	$(".alert-msg-bg").stop(false,true).fadeIn(500);
};

/**
 * 获取当前开奖号码的组态显示 组三组六
 */
TwwfcPage.prototype.getLotteryCodeStatus = function(codeStr){
	//控制显示当前号码的形态
    var codeArray = codeStr.split(",");
    var code1 = codeArray[0];
    var code2 = codeArray[1];
    var code3 = codeArray[2];
    var code4 = codeArray[3];
    var code5 = codeArray[4];
    
    //后三形态
    var codeStatus = "";
    if((code3 == code4 && code3 != code5) || (code3 == code5 && code4 != code5) || (code4 == code5 && code3 != code4)){
    	codeStatus = "组三";
    }else if((code3 != code4) && (code3 != code5) && (code4 != code5)){
    	codeStatus = "组六";
    }else if(code3 == code4 && code4 == code5){
    	codeStatus = "豹子";
    }    
    
    return codeStatus;
        
};


/**
 * 显示当前投注号码
 * @param codeStastic
 * @param index
 */
TwwfcPage.prototype.showCurrentPlayKindCode = function(codesArray){
	//判断是否有位数的概念
	var isBallNumMatch = true;
	var kindKey = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey;
	if(kindKey == 'WXDS' || kindKey == 'WXZX120' || kindKey == 'WXYMBDW' || kindKey == 'WXEMBDW'
		|| kindKey == 'WXSMBDW' || kindKey == "SXDS" || kindKey == "SXZX24" || kindKey == "SXYMBDW" || kindKey == "SXEMBDW"
		|| kindKey == "QSZXDS" || kindKey == "QSZXHZ" || kindKey == "QSZXHZ_G" || kindKey == "QSZS" || kindKey == "QSZL" || kindKey == "QSYMBDW" || kindKey == "QSEMBDW"
		|| kindKey == "HSZXDS" || kindKey == "HSZXHZ" || kindKey == "HSZXHZ_G" || kindKey == "HSZS" || kindKey == "HSZL" || kindKey == "HSYMBDW" || kindKey == "HSEMBDW"
		|| kindKey == "QEZXDS" || kindKey == "QEZXHZ" || kindKey == "QEZXFS_G" || kindKey == "QEZXDS_G" || kindKey == "QEZXHZ_G"
		|| kindKey == "HEZXDS" || kindKey == "HEZXHZ" || kindKey == "HEZXFS_G" || kindKey == "HEZXDS_G" || kindKey == "HEZXHZ_G"){
		isBallNumMatch = false;
	}

	if(isBallNumMatch){
		for(var i = 0; i < codesArray.length; i++){
			if(codesArray[i].indexOf(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT) == -1){
	          	$(".l-mun div[name='ballNum_"+i+"_match']").each(function(){
	                if($(this).attr("data-realvalue") == codesArray[i]){
	            		$(this).toggleClass("on");
	                }
	        	}); 
			}else{
				var codesWeiArray = codesArray[i].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
	            for(var j = 0; j < codesWeiArray.length; j++){
	            	var codeWei = codesWeiArray[j];
	    			if(codeWei != lotteryCommonPage.lotteryParam.CODE_REPLACE){
	    	          	$(".l-mun div[name='ballNum_"+i+"_match']").each(function(){
	                        if($(this).attr("data-realvalue") == codeWei){
	                    		$(this).toggleClass("on");
	                        }
	                	});   				
	    			}
	            }
			}
		}		
	}else{
		for(var i = 0; i < codesArray.length; i++){
			if(codesArray[i].indexOf(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT) == -1){
	          	$(".l-mun div").each(function(){
	                if($(this).attr("data-realvalue") == codesArray[i]){
	            		$(this).toggleClass("on");
	                }
	        	}); 
			}else{
				var codesWeiArray = codesArray[i].split(lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT);
	            for(var j = 0; j < codesWeiArray.length; j++){
	            	var codeWei = codesWeiArray[j];
	    			if(codeWei != lotteryCommonPage.lotteryParam.CODE_REPLACE){
	    	          	$(".l-mun div[name='ballNum_"+i+"_match']").each(function(){
	                        if($(this).attr("data-realvalue") == codeWei){
	                    		$(this).toggleClass("on");
	                        }
	                	});   				
	    			}
	            }
			}
		}
	}
};

/**
 * 机选
 */
TwwfcPage.prototype.JxCodes = function(num){
	var kindKey = twwfcPage.lotteryParam.currentKindKey;
	var beishu = lotteryCommonPage.lotteryParam.beishu;
	var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
	
	var jxmax;
	var jxmin;
	var codes;
	
	//大小单双
	var dxds_code_array = new Array(1,2,3,4);
	
	//直选和值
	if (kindKey == "QSZXHZ" || kindKey == "HSZXHZ"){
		jxmin = 0;
		jxmax = 27;
	}
	//直选和值、组三和值
	else if (kindKey == "QSZXHZ_G" ||kindKey == "HSZXHZ_G"){
		jxmin = 1;
		jxmax = 26;
	}
	//前二直选和值 和 后二直选和值
	else if (kindKey == "QEZXHZ" || kindKey == "HEZXHZ"){
		jxmin = 0;
		jxmax = 18;
	}
	//前二组选和值和后二组选和值
	else if (kindKey == "QEZXHZ_G" || kindKey == "HEZXHZ_G"){
		jxmin = 1;
		jxmax = 17;
	}
	//大小单双
	else if (kindKey == "QEDXDS" || kindKey == "HEDXDS"){
		jxmin = 1;
		jxmax = 4;
	}
	else{
		jxmin = 0;
		jxmax = 9;
	}
	
	for(var i = 0;i < num; i++){
	   //五星
	   if(kindKey == 'WXFS' || kindKey == 'WXDS'){
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			var code3 = GetRndNum(jxmin,jxmax);
			var code4 = GetRndNum(jxmin,jxmax);
			var code5 = GetRndNum(jxmin,jxmax);
			
			codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code4 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + code5;
			
	   }else if(kindKey == 'WXZX120'){
		    var codeArray =  GetRndNumByAppoint(jxmin,jxmax,5,0);
			codes = codeArray[0] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
					codeArray[1] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
					codeArray[2] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
					codeArray[3] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + codeArray[4];
	   }else if(kindKey == 'WXZX60'){
		   var codeArray =  GetRndNumByAppoint(jxmin,jxmax,1,3);
			codes = codeArray[0] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			codeArray[1] + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT +
			codeArray[2] + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT +
			codeArray[3];
	   }else if(kindKey == 'WXZX30'){
		   var codeArray =  GetRndNumByAppoint(jxmin,jxmax,2,1);
			codes = codeArray[0] + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT + 
			codeArray[1] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			codeArray[2];
	   }else if(kindKey == 'WXZX20' || kindKey == 'SXZX12'){
		   var codeArray =  GetRndNumByAppoint(jxmin,jxmax,1,2);
			codes = codeArray[0] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			codeArray[1] + lotteryCommonPage.lotteryParam.SOURCECODE_ARRANGE_SPLIT +
			codeArray[2];
	   }else if(kindKey == 'WXZX10' || kindKey == 'WXZX5' || kindKey == 'SXZX4'){
		   var codeArray =  GetRndNumByAppoint(jxmin,jxmax,1,1);
			codes = codeArray[0] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			        codeArray[1];
	   }else if(kindKey == 'SXZX6'){
		   var codeArray =  GetRndNumByAppoint(jxmin,jxmax,1,1);
			codes = codeArray[0] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			        codeArray[1];
	   }else if(kindKey == 'WXYMBDW' || kindKey == 'SXYMBDW' || kindKey == 'QSZXHZ' || kindKey == 'QSZXHZ_G'
		       || kindKey == 'QSYMBDW' || kindKey == 'HSZXHZ' || kindKey == 'HSZXHZ_G' 
		       || kindKey == 'HSYMBDW'
		       || kindKey == 'QEZXHZ' || kindKey == 'QEZXHZ_G'
		       || kindKey == 'HEZXHZ' || kindKey == 'HEZXHZ_G'){
			var code1 = GetRndNum(jxmin,jxmax);
			codes = code1;
	   }else if(kindKey == 'WXEMBDW' || kindKey == 'SXEMBDW' || kindKey == 'QSZS'
		       || kindKey == 'QSEMBDW' || kindKey == 'HSZS'
		       || kindKey == 'HSEMBDW' 
		       || kindKey == 'QEZXFS_G' || kindKey == 'QEZXDS_G'
		       || kindKey == 'HEZXFS_G' || kindKey == 'HEZXDS_G'
		       || kindKey == 'QEDXDS' || kindKey == 'HEDXDS'){
		   var codeArray =  GetRndNumByAppoint(jxmin,jxmax,2,0);
			codes = codeArray[0] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			codeArray[1];
	   }else if(kindKey == 'WXSMBDW' || kindKey == 'QSZL'|| kindKey == 'HSZL'){ 
		   var codeArray =  GetRndNumByAppoint(jxmin,jxmax,3,0);
			codes = codeArray[0] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			codeArray[1] + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			codeArray[2];
	   }else if(kindKey == 'SXFS'){
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			var code3 = GetRndNum(jxmin,jxmax);
			var code4 = GetRndNum(jxmin,jxmax);
			codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code4;
	   }else if(kindKey == 'SXDS'){
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			var code3 = GetRndNum(jxmin,jxmax);
			var code4 = GetRndNum(jxmin,jxmax);
			codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code4;
	   }else if(kindKey == 'SXZX24'){
		    var codeArray =  GetRndNumByAppoint(jxmin,jxmax,4,0);
			var code1 = codeArray[0];
			var code2 = codeArray[1];
			var code3 = codeArray[2];
			var code4 = codeArray[3];
			codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code4;
	   }else if(kindKey == 'QSZXFS' || kindKey == 'QSZXDS'){
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			var code3 = GetRndNum(jxmin,jxmax);
			codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        lotteryCommonPage.lotteryParam.CODE_REPLACE;
	   }else if(kindKey == 'HSZXFS' || kindKey == 'HSZXDS'){
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			var code3 = GetRndNum(jxmin,jxmax);
			codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				    code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
			        code3;
	   }else if(kindKey == 'QEZXFS' || kindKey == 'QEZXDS'){
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
	                code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
	                lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
	                lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
	                lotteryCommonPage.lotteryParam.CODE_REPLACE;
	   }else if(kindKey == 'HEZXFS' || kindKey == 'HEZXDS'){
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
	                lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
	                lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
		            code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
	                code2;
	   }else if(kindKey == 'YXQY'){
			var code1 = GetRndNum(jxmin,jxmax);
			codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
			        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                    lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT + 
                    lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                    lotteryCommonPage.lotteryParam.CODE_REPLACE;
	   }else if(kindKey == 'YXHY'){
			var code1 = GetRndNum(jxmin,jxmax);
			codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                    lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                    lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                    lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                    code1;
	   }else if(kindKey == 'WXDWD'){
		    var position = GetRndNum(0,4);
			var code1 = GetRndNum(jxmin,jxmax);
			if(position == 0){
				codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        code1;
			}else if(position == 1){
				codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE;				
			}else if(position == 2){
				codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE;				
			}else if(position == 3){
				codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE;						
			}else if(position == 4){
				codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +				
				        code1;
			}else{
				showTip("未知的号码位数");
			}
	   }else if(kindKey == 'RXE' || kindKey == 'RXEZXDS'){
		    var position1 = GetRndNum(0,3);
		    var position2 = GetRndNum(position1+1,4);
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			if(position1 < position2){
				var temp = position1;
				position1 = position2;
				position2 = temp;
			}
			
			codes = "";
            for(var k = 0; k < position2;k++){
         	   codes += lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            }				
            codes += code1;
            for(var j = position2; j < position1; j++){
            	if(j == position2){
            		codes += lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            	}else{
            		codes += lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            	}
            }
            codes += code2;
            for(var z = position1; z <= 4;z++){
            	if(z == position1){
            		codes += lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            	}else{
            		codes += lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            	}
            }
            
			if(codes.substring(codes.length - 1, codes.length) == lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT){
				codes = codes.substring(0, codes.length -1);
			}
	   }else if(kindKey == 'RXS' || kindKey == 'RXSZXDS'){
		    var position1 = GetRndNum(0,1);
		    var position2 = GetRndNum(3,4);
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			var code3 = GetRndNum(jxmin,jxmax);
			
			if(position1 < position2){
				var temp = position1;
				position1 = position2;
				position2 = temp;
			}
			
			codes = "";
            for(var k = 0; k < position2;k++){
         	   codes += lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            }				
            codes += code1;
            for(var v = position2; v < 2; v++){
            	if(v == position2){
            		codes += lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            	}else{
            		codes += lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            	}
            }
            codes += code2;
            for(var j = 2; j < position1; j++){
            	if(j == 2){
            		codes += lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            	}else{
            		codes += lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            	}
            }
            codes += code3;
            for(var z = position1; z <= 4;z++){
            	if(z == position1){
            		codes += lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            	}else{
            		codes += lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT;
            	}
            }
			if(codes.substring(codes.length - 1, codes.length) == lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT){
				codes = codes.substring(0, codes.length -1);
			}
	   }else if(kindKey == 'RXSI' || kindKey == 'RXSIZXDS'){
		    var position = GetRndNum(0,4);
			var code1 = GetRndNum(jxmin,jxmax);
			var code2 = GetRndNum(jxmin,jxmax);
			var code3 = GetRndNum(jxmin,jxmax);
			var code4 = GetRndNum(jxmin,jxmax);
			if(position == 0){
				codes = lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        code4;
			}else if(position == 1){
				codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
					    lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
					    code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
					    code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
					    code4;				
			}else if(position == 2){
				codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        code4;				
			}else if(position == 3){
				codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        lotteryCommonPage.lotteryParam.CODE_REPLACE + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        code4;						
			}else if(position == 4){
				codes = code1 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
				        code2 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        code3 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +
                        code4 + lotteryCommonPage.lotteryParam.SOURCECODE_SPLIT +				
                        lotteryCommonPage.lotteryParam.CODE_REPLACE;
			}else{
				showTip("未知的号码位数");
			}
	   }
	   
	   //任选号码随机的特殊处理
	   if(kindKey == 'RXSIZXDS' || kindKey == 'RXSZXDS' || kindKey == 'RXEZXDS'){
		    codes = codes.replace(/\-,/g,"").replace(/\,-/g,"");
	   }
	   
		//先校验投注蓝中,是否有该投注号码
		var isYetHave = false;
		var yetIndex = null;
		var codeStastics = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic;
		for(var j = codeStastics.length - 1; j >= 0 ; j--){
			 var codeStastic = codeStastics[j];
		     var kindKeyTemp = codeStastic[6].substring(codeStastic[6].indexOf("_")+1,codeStastic[6].length);
			 if(kindKeyTemp == twwfcPage.lotteryParam.currentKindKey && codeStastic[5].toString() == codes){
				 isYetHave = true;
				 yetIndex = j;
				 break;
			 }
		}

		var currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get(twwfcPage.param.kindNameType +"_"+twwfcPage.lotteryParam.currentKindKey);
		if(isYetHave && yetIndex != null){
			showTip("您选择的号码在号码篮已存在，将直接进行倍数累加!");
			var codeStastic = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic[yetIndex];
			twwfcPage.lotteryParam.currentLotteryPrice = lotteryCommonPage.getAwardByLotteryModel(1 * lotteryCommonPage.currentLotteryKindInstance.param.unitPrice * beishu);
			codeStastic[0] = (parseFloat(codeStastic[0]) + parseFloat(twwfcPage.lotteryParam.currentLotteryPrice)).toFixed(lotteryCommonPage.param.toFixedValue);
			codeStastic[1] = codeStastic[1];
			codeStastic[2] = parseInt(codeStastic[2]) + lotteryCommonPage.lotteryParam.beishu;
			codeStastic[3] = (parseFloat(codeStastic[3]) + parseFloat(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu))).toFixed(lotteryCommonPage.param.toFixedValue);
			codeStastic[4] = lotteryCommonPage.lottertyDataDeal.lotteryKindMap.get(twwfcPage.lotteryParam.currentKindKey);
			codeStastic[5] = codes;
			codeStastic[6] = twwfcPage.param.kindNameType +"_"+twwfcPage.lotteryParam.currentKindKey;
			codeStastic[7] = codeStastic[7];
			
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.remove(codeStastic[7]); //先清除原来的投注号码的倍数
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put(codeStastic[7],codeStastic[2]);
		}else{
		    twwfcPage.lotteryParam.currentLotteryTotalCount++;
			//将所选号码和玩法的映射放置map中
			var lotteryKindMap =  new JS_OBJECT_MAP();
			//将所选号码和玩法的映射放置map中
		    if(kindKey == 'RXSIZXDS' || kindKey == 'RXSZXDS' || kindKey == 'RXEZXDS'){
				var positionCode = "";
				$("input[name='ds_position']").each(function(){
					if(this.checked){
						positionCode += "1,";
					}else{
						positionCode += "0,";
					}
				});
				positionCode = positionCode.substring(0, positionCode.length - 1);
				lotteryKindMap.put(twwfcPage.lotteryParam.currentKindKey,codes + "_" + positionCode);
		    }else{
				lotteryKindMap.put(twwfcPage.lotteryParam.currentKindKey,codes);
		    }
			lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_"+twwfcPage.lotteryParam.currentLotteryTotalCount,lotteryKindMap);
			//投注倍数存储
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_"+twwfcPage.lotteryParam.currentLotteryTotalCount,beishu);

			//进入投注池当中,五星默认是一注
			var codeRecord = new Array();
		    var lotteryCount = 0;
		    kindKey = twwfcPage.lotteryParam.currentKindKey;
			if(kindKey == "QSZXHZ" || kindKey == "HSZXHZ"){
				lotteryCount = lotteryCommonPage.lottertyDataDeal.getLotteryCountByShanZhiHeZhi(codes);
			}else if(kindKey == "QSZXHZ_G" || kindKey == "HSZXHZ_G"){
				lotteryCount = lotteryCommonPage.lottertyDataDeal.getLotteryCountByShanZuHeZhi(codes);
			}else if(kindKey == "QEZXHZ" || kindKey == "HEZXHZ"){
				lotteryCount = lotteryCommonPage.lottertyDataDeal.getLotteryCountByErZhiHeZhi(codes);
			}else if(kindKey == "QEZXHZ_G" || kindKey == "HEZXHZ_G"){
				lotteryCount = lotteryCommonPage.lottertyDataDeal.getLotteryCountByErZuHeZhi(codes);
			}else{
				lotteryCount = 1;
			}
			codeRecord.push(lotteryCommonPage.getAwardByLotteryModel(lotteryCount * lotteryCommonPage.currentLotteryKindInstance.param.unitPrice * beishu));
			codeRecord.push(lotteryCount);
			codeRecord.push(beishu);
			codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * beishu));
			codeRecord.push(lotteryCommonPage.lottertyDataDeal.lotteryKindMap.get(twwfcPage.lotteryParam.currentKindKey));
			codeRecord.push(codes);
			codeRecord.push(twwfcPage.param.kindNameType +"_"+twwfcPage.lotteryParam.currentKindKey); //标识玩法
			codeRecord.push("lottery_id_"+twwfcPage.lotteryParam.currentLotteryTotalCount); //标识该条记录的lotteryMap
			twwfcPage.lotteryParam.codesStastic.push(codeRecord); 
			codes = "";
		}
	};
	
	lotteryCommonPage.codeStastics();  //显示随机投注结果
};

/**
 * 单式投注数目控制
 * @param eventType
 */
TwwfcPage.prototype.lotteryCountStasticForDs = function(contentLength){
	//任选四直选单式  //存储注数目
	if(twwfcPage.lotteryParam.currentKindKey == 'RXSIZXDS'
		||  twwfcPage.lotteryParam.currentKindKey == 'RXEZXDS'
		||  twwfcPage.lotteryParam.currentKindKey == 'RXSZXDS'){  //需要乘以对应位置的方案数目
		twwfcPage.lotteryParam.currentLotteryCount = contentLength * lotteryCommonPage.lottertyDataDeal.rxParam.dsAllowCount;	
	}else{
		twwfcPage.lotteryParam.currentLotteryCount = contentLength;				
	}
};

/**
 * 时时彩彩种格式化
 * @param isCut 格式化之后对多余的数字是否截取
 */
TwwfcPage.prototype.formatNum = function(isCut){
	var kindKey = twwfcPage.lotteryParam.currentKindKey;
	var pastecontent = $("#lr_editor").val();
	
	pastecontent=pastecontent.replace(/[^\d]/g,'');
	if (pastecontent == ""){
		$("#lr_editor").val("");
		//设置当前投注数目或者投注价格
		/*$('#J-balls-statistics-lotteryNum').text("0");  //显示投注
		$('#J-balls-statistics-amount').text(parseFloat("0").toFixed(lotteryCommonPage.param.toFixedValue));
		$('#J-balls-statistics-code').text("(投注内容)");*/
		return false;
	}
	var len= pastecontent.length;
	var num="";
	var numtxt="";
	var n=0;
	var maxnum=1;
	if(kindKey == 'WXDS'){
		maxnum=5;
	}else if(kindKey == 'SXDS' || kindKey == 'RXSIZXDS'){
		maxnum=4;
	}
	else if(kindKey == 'QSZXDS' || kindKey == 'HSZXDS' || kindKey == 'RXSZXDS'){  
		maxnum=3;
	}
	else if(kindKey == 'QEZXDS' || kindKey == "QEZXDS_G" || kindKey == "HEZXDS" || kindKey == "HEZXDS_G" || kindKey == "RXEZXDS"){  
		maxnum=2;
	}else{
		showTip("未配置该单式的控制.");
	}
	//下方显示的号码
	var numsStr = "";
	for(var i=0; i<len; i++){
		if(i%maxnum==0){
			n=1;
		}
		else{
			n = n + 1;
		}
		var currentChar = pastecontent.substr(i,1);
		//截取处理的
		if(isCut) {
			if(n<maxnum){
				num = num + currentChar+",";
				numsStr = numsStr + currentChar+",";
			}
			else{
				num = num + currentChar;
				numtxt = numtxt + num + "\n";
				num = "";
				numsStr = numsStr + currentChar + " ";
			}
		} else {
			//不需要截取多余处理的
			if(n<maxnum){
				numtxt = numtxt + currentChar+",";
				numsStr = numsStr + currentChar+",";
			}
			else{
				numtxt = numtxt + currentChar + "\n";
				numsStr = numsStr + currentChar + " ";
			}
		}
	}
	numsStr = numsStr.substring(0, numsStr.length - 1);
	//如果格式化截取多余的  内容为空，显示号码也为空
	if(isCut) {
		if(numtxt == "" || numtxt.length == 0) {
			numsStr = "";
		}
	} else {
		numtxt = numtxt.substring(0, numtxt.length - 1);
	}
	//处理投注下方显示的号码
	twwfcPage.lotteryParam.codesStr = numsStr;
	$("#lr_editor").val(numtxt);
	return true;
};

/**
 * 单式的选号投注
 */
TwwfcPage.prototype.singleLotteryNum = function(){
	var kindKey = twwfcPage.lotteryParam.currentKindKey;
	
	//如果是五星或者四星或者任选玩法的单式录入
	if(kindKey == 'WXDS' || kindKey == 'SXDS' 
		|| kindKey == 'RXSIZXDS' || kindKey == 'RXEZXDS' || kindKey == 'RXSZXDS'){
		var editorVal = $("#lr_editor").val();
		if(editorVal.indexOf("说明") != -1){
			showTip("请先填写投注号码");
			return false;
		}
		lotteryCommonPage.dealForDsFormat();  //首先格式化,避免直接点击选号码的事件
		var pastecontent = $("#lr_editor").val();
		if(pastecontent == ""){
			showTip("请先填写投注号码");
			return false;
		}
		pastecontent = pastecontent.replace(/\r\n/g,"$");
		pastecontent = pastecontent.replace(/\n/g,"$");
		pastecontent = pastecontent.replace(/\s/g,"");
		pastecontent = pastecontent.replace(/(\$)\1+/g, '$1');
		if(pastecontent.substr(pastecontent.length-1,1)=="$"){
			pastecontent = pastecontent.substr(0,pastecontent.length-1);
		}
		
		pastecontent = pastecontent.replace(/\$/g,"&");
		var pastecontentArr = pastecontent;
		var pastecontentLength = pastecontent.split("&").length;
		/*if(kindKey == 'WXDS'){
			if(twwfcPage.lotteryParam.currentLotteryCount > 80000){ //100000
				showTip("当前玩法最多支持80000注单式内容，请调整！");
		        return;
			}
		}else if(kindKey == 'SXDS'){
			if(twwfcPage.lotteryParam.currentLotteryCount > 8000){ //10000
				showTip("当前玩法最多支持8000注单式内容，请调整！");
		        return;
			}
		}else if(kindKey == 'RXSIZXDS' || kindKey == 'RXEZXDS' || kindKey == 'RXSZXDS'){
			if(kindKey == 'RXSIZXDS'){
				if(twwfcPage.lotteryParam.currentLotteryCount > 40000){ //50000
					showTip("当前玩法最多支持40000注单式内容，请调整！");
			        return;
				}else if(twwfcPage.lotteryParam.currentLotteryCount == 0){
					showTip("当前没有投注数目，请查看.");
			        return;
				}
			}else if(kindKey == 'RXEZXDS'){
				if(twwfcPage.lotteryParam.currentLotteryCount > 2000){
					showTip("当前玩法最多支持2000注单式内容，请调整！");
			        return;
				}else if(twwfcPage.lotteryParam.currentLotteryCount == 0){
					showTip("当前没有投注数目，请查看.");
			        return;
				}
			}else if(kindKey == 'RXSZXDS'){
				if(twwfcPage.lotteryParam.currentLotteryCount > 8000){ //10000
					showTip("当前玩法最多支持8000注单式内容，请调整！");
			        return;
				}else if(twwfcPage.lotteryParam.currentLotteryCount == 0){
					showTip("当前没有投注数目，请查看.");
			        return;
				}
			}else{
				showTip("未配置");
			}
		}else{
			showTip("未配置");
		}*/
		
		
		var errzhushu = "";
		var errzhushumsg = "[格式错误]"
		var reg1,reg2;
		if(kindKey == 'WXDS'){  //五星单式 1,2,3,4,5   
			reg1 = /^((?:(?:[0-9]),){4}(?:[0-9])[&]){0,}(?:(?:[0-9]),){4}(?:[0-9])$/;
		}else if(kindKey == 'SXDS'){ //四星直选单式
			reg1 = /^((?:(?:[0-9]),){3}(?:[0-9])[&]){0,}(?:(?:[0-9]),){3}(?:[0-9])$/;
		}else if(kindKey == 'RXSIZXDS'){ //任选四直选单式
			reg1 = /^((?:(?:[0-9]),){3}(?:[0-9])[&]){0,}(?:(?:[0-9]),){3}(?:[0-9])$/;
		}else if(kindKey == 'RXSZXDS'){ //任选三直选单式
			reg1 = /^((?:(?:[0-9]),){2}(?:[0-9])[&]){0,}(?:(?:[0-9]),){2}(?:[0-9])$/;
		}else if(kindKey == 'RXEZXDS'){ //任选二直选单式
			reg1 = /^((?:(?:[0-9]),){1}(?:[0-9])[&]){0,}(?:(?:[0-9]),){1}(?:[0-9])$/;
		}else{
			showTip("未配置该类型的单式");
		}
		
		//号码校验
		if(!reg1.test(pastecontentArr)){
			showTip("单式号码格式不合法.");
			return false;
		}
		
		//位数选取判断
		var positionCode = "";
		$("input[name='ds_position']").each(function(){
			if(this.checked){
				positionCode += "1,";
			}else{
				positionCode += "0,";
			}
		});
		positionCode = positionCode.substring(0, positionCode.length - 1);
		twwfcPage.lotteryParam.codes = pastecontentArr;
		
		var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
		var currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get(twwfcPage.param.kindNameType +"_"+twwfcPage.lotteryParam.currentKindKey);
		lotteryCommonPage.lotteryParam.beishu = parseInt(lotteryCommonPage.lotteryParam.beishu);
		twwfcPage.lotteryParam.currentLotteryTotalCount++;
		
		//将所选号码和玩法的映射放置map中
		var lotteryKindMap =  new JS_OBJECT_MAP();
		if(kindKey == 'RXSIZXDS' || kindKey == 'RXEZXDS' || kindKey == 'RXSZXDS'){
			lotteryKindMap.put(twwfcPage.lotteryParam.currentKindKey,twwfcPage.lotteryParam.codes + "_" + positionCode);
		}else{
			lotteryKindMap.put(twwfcPage.lotteryParam.currentKindKey,twwfcPage.lotteryParam.codes);
		}
		lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_"+twwfcPage.lotteryParam.currentLotteryTotalCount,lotteryKindMap);
		//投注倍数存储
		lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_"+twwfcPage.lotteryParam.currentLotteryTotalCount,lotteryCommonPage.lotteryParam.beishu);
		
		//进入投注池当中
		var codeRecord = new Array();
		codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((twwfcPage.lotteryParam.currentLotteryCount * twwfcPage.param.unitPrice * lotteryCommonPage.lotteryParam.beishu).toFixed(lotteryCommonPage.param.toFixedValue)));
		codeRecord.push(twwfcPage.lotteryParam.currentLotteryCount);
		codeRecord.push(lotteryCommonPage.lotteryParam.beishu);
		codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu));
		codeRecord.push(lotteryCommonPage.lottertyDataDeal.lotteryKindMap.get(twwfcPage.lotteryParam.currentKindKey));
		codeRecord.push(twwfcPage.lotteryParam.codes);
		codeRecord.push(twwfcPage.param.kindNameType +"_"+twwfcPage.lotteryParam.currentKindKey); //标识玩法
		codeRecord.push("lottery_id_"+twwfcPage.lotteryParam.currentLotteryTotalCount); 
		twwfcPage.lotteryParam.codesStastic.push(codeRecord);	
		
		//号码显示统计,由当前的倍数、奖金模式、投注模式决定
		lotteryCommonPage.codeStastics();
		//选好号码的结束事件
		//lotteryCommonPage.addCodeEnd(); 
		//$("#lr_editor").val("");//清空注码
	//前三后三前二后二单式玩法的录入
	}else{
		var editorVal = $("#lr_editor").val();
		if(editorVal.indexOf("说明") != -1){
			showTip("请先填写投注号码");
			return false;
		}
		
		lotteryCommonPage.dealForDsFormat();  //首先格式化,避免直接点击选号码的事件
		var pastecontent = $("#lr_editor").val();
		if(pastecontent == ""){
			showTip("请先填写投注号码");
			return false;
		}
		pastecontent = pastecontent.replace(/\r\n/g,"$");
		pastecontent = pastecontent.replace(/\n/g,"$");
		pastecontent = pastecontent.replace(/\s/g,"");
		pastecontent = pastecontent.replace(/(\$)\1+/g, '$1');
		if(pastecontent.substr(pastecontent.length-1,1)=="$"){
			pastecontent = pastecontent.substr(0,pastecontent.length-1);
		}
		var pastecontentArr = pastecontent.split("$");
		/*if(kindKey == 'QSZXDS' || kindKey == 'HSZXDS'){ //三星直选单式 和 三星组选单式
			if(twwfcPage.lotteryParam.currentLotteryCount > 800){  //800
				showTip("当前玩法最多支持800注单式内容，请调整！");
		        return;
			}
		}else if(kindKey == 'QEZXDS' || kindKey == "QEZXDS_G" || kindKey == "HEZXDS" || kindKey == "HEZXDS_G"){ //二星直选单式 和 二星组选单式
			if(twwfcPage.lotteryParam.currentLotteryCount > 80){  //80
				showTip("当前玩法最多支持80注单式内容，请调整！");
		        return;
			}
		}
		if(pastecontentArr.length > 2000){
			showTip("当前玩法最多支持2000注单式内容，请调整！");
	        return;
		}*/

		var errzhushu = "";
		var errzhushumsg = "[格式错误]"
		var reg1,reg2;
		if(kindKey == 'WXDS'){  //五星单式 1,2,3,4,5
			reg1 = /^(?:(?:[0-9]),){4}(?:[0-9])\s*$/;
			reg2 = /^\d{5}\s*$/;
		}else if(kindKey == 'SXDS'){ //四星直选单式
			reg1 = /^(?:(?:[0-9]),){3}(?:[0-9])\s*$/;
			reg2 = /^\d{5}\s*$/;
		}else if(kindKey == 'QSZXDS' || kindKey == 'HSZXDS'){ //三星直选单式 和 三星组选单式
			reg1 = /^(?:(?:[0-9]),){2}(?:[0-9])\s*$/;
			reg2 = /^\d{3}\s*$/;
		}else if(kindKey == 'QEZXDS' || kindKey == "QEZXDS_G" || kindKey == "HEZXDS" || kindKey == "HEZXDS_G"){ //二星直选单式 和 二星组选单式
			reg1 = /^(?:(?:[0-9]),)(?:[0-9])\s*$/;
			reg2 = /^\d{2}\s*$/;
		}else if(kindKey == 'RXSIZXDS'){
			if(!lotteryCommonPage.lottertyDataDeal.rxParam.isRxDsAllow){
				showTip("您还未选择任选四单式的位置,无法添加号码");
				return;
			}
			reg1 = /^(?:(?:[0-9]),){3}(?:[0-9])\s*$/;
			reg2 = /^\d{5}\s*$/;
		}else if(kindKey == 'RXEZXDS'){
			if(!lotteryCommonPage.lottertyDataDeal.rxParam.isRxDsAllow){
				showTip("您还未选择任选二单式的位置,无法添加号码");
				return;
			}
			reg1 = /^(?:(?:[0-9]),)(?:[0-9])\s*$/;
			reg2 = /^\d{2}\s*$/;
		}else if(kindKey == 'RXSZXDS'){
			if(!lotteryCommonPage.lottertyDataDeal.rxParam.isRxDsAllow){
				showTip("您还未选择任选三单式的位置,无法添加号码");
				return;
			}
			reg1 = /^(?:(?:[0-9]),){2}(?:[0-9])\s*$/;
			reg2 = /^\d{3}\s*$/;
		}else{
			showTip("未配置该类型的单式");
		}
		
		var isAppendCode = false;
		var appendCodeArray = new Array();
		var dsCodeCodes = "";  //单式号码拼接

		for(var i=0;i<pastecontentArr.length;i++){
			var value1 = pastecontentArr[i];
			if(!reg1.test(value1)&&!reg2.test(value1)){
				if(errzhushu==""){
					errzhushu = "第"+(i+1).toString()+"注";
				}else{
					errzhushu = errzhushu+" <br/>"+"第"+(i+1).toString()+"注";
				}
				continue;
			}

			if(kindKey == 'QEZXDS_G' || kindKey == 'HEZXDS_G'){ //2星组组选单式
				var lsstr=value1;
				sstr=lsstr.replace(/,/g,"");
				a = sstr.substr(0,1);
				b = sstr.substr(1,1);
				if (a==b){
					if(errzhushu==""){
						errzhushu = "第"+(i+1).toString()+"注";
					}else{
						errzhushu = errzhushu+"、"+"第"+(i+1).toString()+"注";
					}
				}
			}
			
	        //单式号码拼接
	        dsCodeCodes += pastecontentArr[i] + "  ";
	        
		}
		
		//显示错误注号码
		if(errzhushu!=""){
			showTip(errzhushu+"  "+"号码有误，请核对！");
			return false;
		}
		
		var currentAwardModel = lotteryCommonPage.lotteryParam.modelValue;
		var currentLotteryWin = lotteryCommonPage.currentLotteryKindInstance.param.currentLotteryWins.get(twwfcPage.param.kindNameType +"_"+twwfcPage.lotteryParam.currentKindKey);
		lotteryCommonPage.lotteryParam.beishu = parseInt(lotteryCommonPage.lotteryParam.beishu);
		twwfcPage.lotteryParam.currentLotteryCount = 1;

		//单式记录标识
		var dsCodeRecordArray = new Array();
		var dsLotteryCount = 0;
		for(var i=0;i<pastecontentArr.length;i++){
			var value = pastecontentArr[i];	
			if(value == ""){
				continue;
			}
			var text ="";
			if(kindKey == 'WXDS' ){
				value = value;
			}else if(kindKey == 'SXDS' ){//四星星直选单式
				value = "-,"+value;
			}else if(kindKey == 'QSZXDS' ){//前三星直选单式
				value = value + ",-,-";
			}else if(kindKey == 'HSZXDS' ){//后三星直选单式
				value = "-,-," + value;
			}else if(kindKey == 'QEZXDS'){//前二直选单式
				value = value + ",-,-,-";
			}else if(kindKey == 'HEZXDS'){//后二直选单式
				value = "-,-,-," + value;
			}else if(kindKey == 'QEZXDS_G' || kindKey == 'HEZXDS_G' || kindKey == 'RXSIZXDS' || kindKey == 'RXEZXDS' || kindKey == 'RXSZXDS'){ //任选的玩法,已经拼接好投注号码
				value = value;
			}else{
				showTip("该单式号码未配置");
				return false;
			}
			
			twwfcPage.lotteryParam.currentLotteryTotalCount++;
			lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryCount = 1;
			twwfcPage.lotteryParam.codes = value;
				
			//将所选号码和玩法的映射放置map中
			var lotteryKindMap =  new JS_OBJECT_MAP();
			lotteryKindMap.put(twwfcPage.lotteryParam.currentKindKey,twwfcPage.lotteryParam.codes);
			lotteryCommonPage.lotteryParam.lotteryMap.put("lottery_id_"+twwfcPage.lotteryParam.currentLotteryTotalCount,lotteryKindMap);
			//投注倍数存储
			lotteryCommonPage.lotteryParam.lotteryMultipleMap.put("lottery_id_"+twwfcPage.lotteryParam.currentLotteryTotalCount,lotteryCommonPage.lotteryParam.beishu);
			
			twwfcPage.lotteryParam.codes = null;

			dsCodeRecordArray.push("lottery_id_"+twwfcPage.lotteryParam.currentLotteryTotalCount);
			dsLotteryCount++;  //统计数目
		}
		
		//进入投注池当中
		var codeRecord = new Array();
		codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((dsLotteryCount * twwfcPage.param.unitPrice * lotteryCommonPage.lotteryParam.beishu).toFixed(lotteryCommonPage.param.toFixedValue)));
		codeRecord.push(dsLotteryCount);
		codeRecord.push(lotteryCommonPage.lotteryParam.beishu);
		codeRecord.push(lotteryCommonPage.getAwardByLotteryModel((currentLotteryWin.winMoney + (currentAwardModel - lowestAwardModel) * currentLotteryWin.specCode) * lotteryCommonPage.lotteryParam.beishu));
		codeRecord.push(lotteryCommonPage.lottertyDataDeal.lotteryKindMap.get(twwfcPage.lotteryParam.currentKindKey));
		codeRecord.push(dsCodeCodes);
		codeRecord.push(twwfcPage.param.kindNameType +"_"+twwfcPage.lotteryParam.currentKindKey); //标识玩法
		codeRecord.push(dsCodeRecordArray); 
		twwfcPage.lotteryParam.codesStastic.push(codeRecord);	
		
		//号码显示统计,由当前的倍数、奖金模式、投注模式决定
		lotteryCommonPage.codeStastics();
		//选好号码的结束事件
		//lotteryCommonPage.addCodeEnd(); 
		//$("#lr_editor").val("");//清空注码
	}
	return true;
};

/**
 * 初始化彩种信息
 */
TwwfcPage.prototype.initLottery = function(){
	
	//先加载所有的玩法
    lotteryCommonPage.lottertyDataDeal.loadLotteryKindPlay();
    
    //取消追号处理
    lotteryCommonPage.lotteryParam.isZhuiCode = false;
    
    //设置玩法描述数据
    lotteryCommonPage.lottertyDataDeal.setKindPlayDes();
    
    //选择默认的玩法，默认是五星定位胆
    twwfcPage.lotteryParam.currentKindKey = "WXDWD";
	lotteryCommonPage.lottertyDataDeal.lotteryKindPlayChoose(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey);

	//动画效果
	animate_add(".main-head","fadeInDown",0);
	animate_add(".lottery-content","fadeInUp",0);
	animate_add(".lottery-bar .bar","fadeInUp",0);
	
	//添加星种选中事件
	$("#lotteryStarKindList .child").unbind("click").click(function(){
		var roleId = $(this).attr("data-role-id");
		//如果有包含玩法属性
		if(isNotEmpty(roleId) && lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey != roleId) {
			//清空当前选中号码
			lotteryCommonPage.clearCurrentCodes();
			lotteryCommonPage.lottertyDataDeal.lotteryKindPlayChoose(roleId);
		//加载星种玩法下拉
		} else {
			var kindPlayClass = $(this).attr("data-id");
			var parent=$(this).closest("body");
			parent.find(".lottery-classify2 .move-content .child").removeClass('color-red');
			$(this).addClass('color-red');
			parent.find(".lottery-classify2 .clist").removeClass('in').addClass('out');
			parent.find("."+kindPlayClass).addClass('in').removeClass('out');
		}
	});
    //添加玩法选中事件
	$("#lotteryKindPlayList .line-content .child").unbind("click").click(function(){
		var roleId = $(this).attr("data-role-id");
		//已经是当前玩法了，不需要处理
		if(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey == roleId) {
			return;
		}
		
	   //清空当前选中号码
	   lotteryCommonPage.clearCurrentCodes();
	   
	   //控制玩法的号码展示
	   lotteryCommonPage.lottertyDataDeal.lotteryKindPlayChoose(roleId);
	   
	   //隐藏下拉玩法列表
	   element_toggle(false,['.lottery-classify']);
	});
	
	//点击当前玩法显示下拉其他玩法
	$("#currentKindDiv").unbind("click").click(function(){
		var e = $(this);
		var ele = ".lottery-classify2";
		var parent=$(e).closest("body");
		var display=$(ele).css("display");
		var style="boolean";
		style=display=="none"?"true":"boolean";
		parent.find(".lottery-classify").not(ele).hide();
		element_toggle(style,[ele],parent);
		
		//触发星种下拉
		$("#lotteryStarKindList .child").each(function(){
			if($(this).hasClass('color-red')) {
				var kindPlayClass = $(this).attr("data-id");
				var parent=$(this).closest("body");
				parent.find(".lottery-classify2 .clist").removeClass('in').addClass('out');
				parent.find("."+kindPlayClass).addClass('in').removeClass('out');
				return false;
			}
		});
	});
	
	//绑定选择其他地区彩种事件
	$("#otherLotteryKind").unbind("click").click(function(){
		var ele = '.lottery-classify1';
		var parent=$(this).closest("body");
		var display=$(ele).css("display");
		var style="boolean";
		style=display=="none"?"true":"boolean";
		parent.find(".lottery-classify").not(ele).hide();
		element_toggle(style,[ele],parent);
	});
	
	//点击开奖号码显示开奖记录事件
	$(".lottery_run").unbind("click").click(function(){
		var parent=$(this).closest("body");
		if(parent.find(".lottery-dynamic").hasClass('in')){
			parent.find(".lottery-dynamic").removeClass('in').addClass('out');
		}else{
			parent.find(".lottery-dynamic").removeClass('out').addClass('in');
		}
	});
	
	
	//倍数改变事件
	lotteryCommonPage.beishuEvent();
	
	//倍数减少事件
	$("#beishuSub").unbind("click").click(function(){
		var beishuStr = $('#beishu').val();
		var beishuTemp = parseInt(beishuStr);
		beishuTemp--;
		if(beishuTemp <= 0) {
			showTip("当前倍数已经是最低了");
			return;
		}
		$("#beishu").val(beishuTemp);
		lotteryCommonPage.beishuEventContent();
	});
	
	//倍数增加事件
	$("#beishuAdd").unbind("click").click(function(){
		var beishuStr = $('#beishu').val();
		var beishuTemp = parseInt(beishuStr);
		beishuTemp++;
		if(beishuTemp > 1000000) {
			showTip("倍数最高1000000");
			return;
		}
		$("#beishu").val(beishuTemp);
		lotteryCommonPage.beishuEventContent();
	});
	

	//确认投注-展示投注信息事件
	$("#showLotteryMsgButton").unbind("click").click(function(){
		//非追号类型 处理添加号码到投注池
		if(!lotteryCommonPage.lotteryParam.isZhuiCode) {
			lotteryCommonPage.currentLotteryKindInstance.userLotteryNumForAddOrder();
		}
		lotteryCommonPage.showLotteryMsg();
	});
	//关闭投注信息事件
    $("#lotteryOrderDialogCancelButton").unbind("click").click(function(){
    	lotteryCommonPage.hideLotteryMsg();
	});
    
    //关闭投注信息事件
    $(".close").unbind("click").click(function(){
    	lotteryCommonPage.hideLotteryMsg();
    });
    
	//确认投注-提交后台投注事件
	$("#lotteryOrderDialogSureButton").unbind("click").click(function(){
		lotteryCommonPage.userLottery();
	});
	
	//机选一注事件
	$("#randomOneBtn").unbind("click").click(function(){
		lotteryCommonPage.randomOneEvent();
	});
	
	//清空投注池号码
	$("#clearAllCodeBtn").unbind("click").click(function(){
		lotteryCommonPage.clearCodeStasticEvent();
		/*comfirm("提示","确认删除号码篮内全部内容吗?","取消",function(){},"确认",
				lotteryCommonPage.clearCodeStasticEvent);*/
		
	});	
	
	//点击追号取消追号事件
	$("#zhuiCodeButton").unbind("click").click(function(param,showParam){
		lotteryCommonPage.changeTozhuiCodeEvent();
	});	
	
	 
	//获取最新的开奖号码
	lotteryCommonPage.getLastLotteryCode();
	//获取当前彩种的最新期号和投注倒计时
	lotteryCommonPage.getActiveExpect();
	//加载今天和明天的期号
    lotteryCommonPage.getAllExpectsByTodayAndTomorrow();
	//近10期的开奖记录
	lotteryCommonPage.getNearestTenLotteryCode();
	//获取用户今日投注记录
	lotteryCommonPage.getTodayLotteryByUser();
	
	//每隔10秒钟请求一次获取最新的开奖号码
	if(lotteryCommonPage.currentLotteryKindInstance.param.kindName == "JLFFC" || lotteryCommonPage.currentLotteryKindInstance.param.kindName == "HGFFC"){
		window.setInterval("lotteryCommonPage.getLastLotteryCode()",5000);  //分分彩读取最新的开奖号码
		//window.setInterval("lotteryCommonPage.getNearestTenLotteryCode()",5000);
		window.setInterval("lotteryCommonPage.getActiveExpect()",30000); //30秒重新读取服务器的时间
	}else{
		window.setInterval("lotteryCommonPage.getLastLotteryCode()",10000);  //其他彩种读取最新的开奖号码
		//window.setInterval("lotteryCommonPage.getNearestTenLotteryCode()",10000);
		window.setInterval("lotteryCommonPage.getActiveExpect()",60000); //1分钟重新读取服务器的时间
	}
};

$(document).ready(function() {
	
		//设置数据处理器  默认时时彩的
		lotteryCommonPage.lottertyDataDeal = sscData;
		//将这个投注实例传至common js当中
		lotteryCommonPage.currentLotteryKindInstance = twwfcPage; 
		//设置投注的模式
		lotteryCommonPage.lotteryParam.modelValue = currentUserSscModel;
		lowestAwardModel = sscLowestAwardModel;
		//获取当前彩种的所有奖金列表
		lotteryCommonPage.currentLotteryKindInstance.getLotteryWins(); 
		//获取当前投注模式  元角模式
		lotteryCommonPage.loadUserModelSet();
		setTimeout("twwfcPage.initLottery()", 100);

});

