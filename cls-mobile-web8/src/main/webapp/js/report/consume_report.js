/*
 * 盈亏报表
 */
function ConsumeReportPage(){}
var consumeReportPage = new ConsumeReportPage();

consumeReportPage.queryParam = {
		userName : null,
		teamLeaderName : null,
		createdDateStart : null,
		createdDateEnd : null,
		reportType : 0,
		dateRange:-1
};

consumeReportPage.pageParam = {
        pageSize : 10,  //前台分页每页条数
        pageNo : 1,
        showPage:1,
        total:0       
};
consumeReportPage.param = {
		cousumerList:null
}

$(document).ready(function() {

   animate_add(".consume_report .main-child","fadeInUp");
   var defalutDate = new Date();
   var strDate = new Date();
   //defalutDate.setDate(1);
   defalutDate.setHours(2,59,59,999);
   consumeReportPage.queryParam.reportType = 0;
   consumeReportPage.queryParam.createdDateStart = strDate.AddDays(-1).setHours(3,0,0,0);
   consumeReportPage.queryParam.createdDateEnd =consumeReportPage.queryParam.createdDateStart;
		  
		  
   //点击切换查询盈亏报表事件
   $(".move-content").find("p").click(function(){
        var value = $(this).attr("data-value");
        var todate = new Date();
			  
		if(value=="today"){
			 consumeReportPage.queryParam.createdDateStart=todate.setHours(3,0,0,0);
		     consumeReportPage.queryParam.createdDateEnd=consumeReportPage.queryParam.createdDateStart;
		     consumeReportPage.queryParam.dateRange=1;
				  $(".header-right").find("span").html("今天");
		}else if(value=="yesterDay"){
			consumeReportPage.queryParam.createdDateStart=todate.AddDays(-1).setHours(3,0,0,0);
			 consumeReportPage.queryParam.createdDateEnd=consumeReportPage.queryParam.createdDateStart;
			 consumeReportPage.queryParam.dateRange=-1;
			 $(".header-right").find("span").html("昨天");
		}else if(value=="month"){
			 var defalutDate=new Date();
			 defalutDate.setDate(1);
			 defalutDate.setHours(3,0,0,0);
			 consumeReportPage.queryParam.createdDateStart=defalutDate;
			 consumeReportPage.queryParam.createdDateEnd=todate.setHours(2,59,59,999);
			 consumeReportPage.queryParam.dateRange=30;
			 $(".header-right").find("span").html("本月");
		}else if(value=="lastmonth"){
			 var defalutDate=new Date();
			 defalutDate.setDate(1);
			 defalutDate.AddDays(-1).setHours(3,0,0,0);
			 todate.setMonth(todate.getMonth()-1);
			 todate.setDate(1);
			 todate.setHours(3,0,0,0);
			 consumeReportPage.queryParam.createdDateStart=todate;
			 consumeReportPage.queryParam.createdDateEnd=defalutDate;
			 consumeReportPage.queryParam.dateRange=-30;
			 $(".header-right").find("span").html("上个月");
		}
		     
		var parent=$(this).closest("div.lottery-classify1");
		parent.hide();
		$(".alert-bg").stop(false,true).fadeToggle(500);
	    consumeReportPage.getConsumeReports(consumeReportPage.queryParam);
})
		  
    consumeReportPage.getConsumeReports(consumeReportPage.queryParam);
	
    //显示更多
    $("#showMore").click(function(){
			  consumeReportPage.pageParam.showPage = consumeReportPage.pageParam.showPage + 1;
			  consumeReportPage.refreshConsumeReportPages(consumeReportPage.param.cousumerList);
			  var nowData = consumeReportPage.pageParam.showPage* consumeReportPage.pageParam.pageSize;
			  if (nowData < consumeReportPage.pageParam.total){
					$(this).show();
			  }else{
					$(this).hide();
			  }
    })

});


/**
 * 加载所有的记录
 */
ConsumeReportPage.prototype.getConsumeReports = function(queryParam){
	/*var userName=$("#userName").val();
	if(userName!=null && userName!=""){
		consumeReportPage.queryParam.userName=userName;
	}else{
		consumeReportPage.queryParam.userName=null;
	}*/
	
	/*frontTeamAction.getTeamConsumeReportForQuery(queryParam,function(r){
		if (r[0] != null && r[0] == "ok") {
			consumeReportPage.refreshConsumeReportPages(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			showTip(r[1]);
		}else{
			showTip(jQuery(document.body), "查询活动中心数据失败.");
		}
    });*/
	var param={}
	var userName=$("#userName").val();
	if(userName!=null && userName!=""){
		param.userName=userName;
	}
    if(queryParam.teamLeaderName!=null && queryParam.teamLeaderName!=""){
    	param.teamLeaderName=queryParam.teamLeaderName;
	}
    if(queryParam.reportType!=null && queryParam.reportType!=""){
    	param.reportType=queryParam.reportType;
	}
    if(queryParam.dateRange!=null && queryParam.dateRange!=""){
    	param.dateRange=queryParam.dateRange;
	}
    param.createdDateStart=new Date(queryParam.createdDateStart).Format("yyyy-MM-dd");
    param.createdDateEnd=new Date(queryParam.createdDateEnd).Format("yyyy-MM-dd");
	$.ajax({
		type: "POST",
        url: contextPath +"/report/teamConsumeReportQuery",
        contentType : "application/json",
        data: JSON.stringify(param), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		var  data = result.data;
        		consumeReportPage.refreshConsumeReportPages(data);
        	}else if(result.code == "error"){
        		var err = result.data;
        		showTip(err);
        	}
        }
	});
};

/**
 * 加载所有的记录
 */
ConsumeReportPage.prototype.getConsumeReportsByUserName = function(userName){
	$("#userName").val(userName);
	$("#showMore").show();
	consumeReportPage.pageParam.pageSize = 10;
	consumeReportPage.pageParam.pageNo=1;
	consumeReportPage.pageParam.showPage=1;
	consumeReportPage.pageParam.total=0;
	consumeReportPage.getConsumeReports(consumeReportPage.queryParam,consumeReportPage.pageParam.pageNo);
	
};

/**
 * 刷新列表数据
 *
 */
ConsumeReportPage.prototype.refreshConsumeReportPages = function(report){
	var reportListObj = $("#reportList");
	reportListObj.html("");
	reportListObj.append("<div class='titles'><div class='child child1'><p>用户名</p></div><div class='child child2'><p>投注</p></div>"+
			             "<div class='child child3'><p>中奖</p></div><div class='child child4'><p>盈利</p></div><div class='child child5'><p>明细</p></div></div>");
	
	consumeReportPage.param.cousumerList=report;
	var totalLottery = parseFloat(0);
	var totalWin = parseFloat(0);
	var totalGain = parseFloat(0);
	if (report != null) {
		// 记录数据显示
		var total = 0
		for ( var key in report) {
			total++;

		}
		consumeReportPage.pageParam.total = total;
		var totalData = consumeReportPage.pageParam.pageSize
				* consumeReportPage.pageParam.showPage;
		var i = 0;
		for ( var key in report) {
			var value = report[key];
			if (i < totalData) {
				str = "";
				str = "<div class='line'><a href='javascript:consumeReportPage.getConsumeReportsByUserName(\""
					+ key
					+ "\")'><div class='child child1 font-blue'><p>"
					+ key
					+ "</p></div></a><div class='child child2'><p>"
					+ value.lottery.toFixed(frontCommonPage.param.fixVaue)
					+ "</p></div><div class='child child3'><p>"
					+ value.win.toFixed(frontCommonPage.param.fixVaue)
					+ "</p></div>"
					+ "<div class='child child4'><p>"
					+ value.gain.toFixed(frontCommonPage.param.fixVaue)
					+ "</p></div><div class='child child5 font-blue' onclick='consumeReportPage.showDiv(\""
					+ key + "\");'><p>查看明细</p></div></div>";
				reportListObj.append(str);
			}

			totalLottery += parseFloat(value.lottery.toFixed(frontCommonPage.param.fixVaue));
			totalWin += parseFloat(value.win.toFixed(frontCommonPage.param.fixVaue));
			totalGain += parseFloat(value.gain.toFixed(frontCommonPage.param.fixVaue));
			i++
		}
		   str = "";
		   str = "<div class='line gray'>"
				+ "<div class='child child1 font-yellow'><p>总统计</p></div>"
				+ "<div class='child child2 font-yellow'><p>"
				+ totalLottery.toFixed(frontCommonPage.param.fixVaue)
				+ "</p></div>" + "<div class='child child3 font-yellow'><p>"
				+ totalWin.toFixed(frontCommonPage.param.fixVaue)
				+ "</p></div>" + "<div class='child child4 font-yellow'><p>"
				+ totalGain.toFixed(frontCommonPage.param.fixVaue)
				+ "</p></div>" + "<div class='child child5 font-blue' onclick=consumeReportPage.showDiv(true)><p>报表明细</p></div>"
				+ "</div>";
		   reportListObj.append(str);
	}else {
		reportListObj.html("");
		var str = "";
		str += '<div class="line">';
		str += '<h4 class="nodata">暂无数据</h4>';
		str += '</div>';
		reportListObj.append(str);
		$(".user-btn").hide();
	}
     var nowData=consumeReportPage.pageParam.showPage * consumeReportPage.pageParam.pageSize;
	 
	 if(nowData<consumeReportPage.pageParam.total){
		 $("#showMore").show();
	 }else{
		 $("#showMore").hide();
	 }
};
//查看明细
ConsumeReportPage.prototype.showDiv = function(key){
	if(key==true){
		var recharge=0.000;
		var withDraw=0.000;
		var lottery=0.000;
		var win=0.000;
		var percentage=0.000;
		var activitiesMoney=0.000;
		var otherMoney=0.000;
		var gain=0.000;
		var lotteryCount=0;
		var regCount=0;
		var firstRechargeCount=0;
		var keys1="";
		var i=0;
		for(var keys in consumeReportPage.param.cousumerList){
			var value=consumeReportPage.param.cousumerList[keys];
			recharge+=value.recharge;
			withDraw+=value.withdraw;
			lottery+=value.lottery;
			win+=value.win;
			percentage+=value.rebate + value.percentage;

			activitiesMoney+=value.rechargepresent + value.activitiesmoney+value.systemaddmoney-value.managereducemoney
			//otherMoney+=value.systemaddmoney - value.systemreducemoney;
			gain+=value.gain;
			if(consumeReportPage.queryParam.userName==null||consumeReportPage.queryParam.userName==""){
				if(currentUser.userName==(value.userName)){
					lotteryCount=value.lotteryCount;
					regCount=value.regCount;
					firstRechargeCount=value.firstRechargeCount;
	    	    }
			}else{
				if(consumeReportPage.queryParam.userName==(value.userName)){
					lotteryCount=value.lotteryCount;
					regCount=value.regCount;
					firstRechargeCount=value.firstRechargeCount;
	    	    }
			}
		}
		
		$("#recharge").text(recharge.toFixed(frontCommonPage.param.fixVaue));
		$("#withDraw").text(withDraw.toFixed(frontCommonPage.param.fixVaue));
		$("#lottery").text(lottery.toFixed(frontCommonPage.param.fixVaue));
		$("#win").text(win.toFixed(frontCommonPage.param.fixVaue));
		$("#percentage").text(percentage.toFixed(frontCommonPage.param.fixVaue));
		$("#activitiesMoney").text(activitiesMoney.toFixed(frontCommonPage.param.fixVaue));
		//$("#otherMoney").text(otherMoney.toFixed(frontCommonPage.param.fixVaue));
		$("#gain").text(gain.toFixed(frontCommonPage.param.fixVaue));
		$("#lotteryCount").text(lotteryCount);
		$("#adduser").text(regCount);
	    $("#addmoney").text(firstRechargeCount);
	}else{
		var value=consumeReportPage.param.cousumerList[key];
		$("#recharge").text((value.recharge).toFixed(frontCommonPage.param.fixVaue));
		$("#withDraw").text((value.withdraw).toFixed(frontCommonPage.param.fixVaue));
		$("#lottery").text((value.lottery).toFixed(frontCommonPage.param.fixVaue));
		$("#win").text((value.win).toFixed(frontCommonPage.param.fixVaue));
		$("#percentage").text((value.rebate + value.percentage).toFixed(frontCommonPage.param.fixVaue));
		$("#activitiesMoney").text((value.rechargepresent + value.activitiesmoney+value.systemaddmoney-value.managereducemoney).toFixed(frontCommonPage.param.fixVaue));
		//$("#otherMoney").text((value.systemaddmoney - value.systemreducemoney).toFixed(frontCommonPage.param.fixVaue));
		$("#gain").text((value.gain).toFixed(frontCommonPage.param.fixVaue));
		$("#lotteryCount").html((value.lotteryCount==null?"0":value.lotteryCount));
		$("#adduser").text((value.regCount==null?"0":value.regCount));
	    $("#addmoney").text((value.firstRechargeCount==null?"0":value.firstRechargeCount));
	}
	 modal_toggle(".detail-modal");
	 

	//金额过长自动换行
	 $('.consume_report_detail .block').each(function(){
		   var maxHeight = $(this).find('.main-child .child-value span').eq(0).height();
		   var $block = $(this);
		   $block.find('.main-child .child-value span').each(function(){
			   if($(this).height() > maxHeight) {
				   maxHeight = $(this).height();
			   }
		   })
		   $block.find('.main-child .child-value').css('height',maxHeight);
	 })

}
//查看新加入用户和首充会员
/*ConsumeReportPage.prototype.showDiv1=function(key){
	frontTeamAction.getTeamUserConsumeCount(consumeReportPage.queryParam,key,function(r){
		if (r[0] != null && r[0] == "ok") {
			 $("#adduser").text(r[1].addUser);
		     $("#addmoney").text(r[1].addFirstMember);
		}else if(r[0] != null && r[0] == "error"){
			showTip(r[1]);
		}else{
			showTip(jQuery(document.body), "查询数据失败.");
		}
    });
}*/

//显示玩法或者票种
function show_classify(ele){
	var ele = '.lottery-classify1';
	var parent=$(this).closest("body");
	var display=$(ele).css("display");
	var style="boolean";
	style=display=="none"?"true":"boolean";
	parent.find(".lottery-classify").not(ele).hide();
	element_toggle(style,[ele],parent);
	
	//$(".lottery-classify").hide();
	//element_toggle(true,[ele]);
}
Date.prototype.Format = function (fmt) {
	    var o = {
	        "M+": this.getMonth() + 1, //月份 
	        "d+": this.getDate(), //日 
	        "h+": this.getHours(), //小时 
	        "m+": this.getMinutes(), //分 
	        "s+": this.getSeconds(), //秒 
	        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
	        "S": this.getMilliseconds() //毫秒 
	    };
	    if (/(y+)/.test(fmt))fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
	    for (var k in o){
	    if (new RegExp("(" + k + ")").test(fmt)) {fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));}
	    }
	    return fmt;
	}
