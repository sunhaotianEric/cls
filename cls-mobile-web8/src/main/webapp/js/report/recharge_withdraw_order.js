function RechargeWithdrawOrderPage() {
}
var rechargeWithdrawOrderPage = new RechargeWithdrawOrderPage();

rechargeWithdrawOrderPage.queryParam = {
		userName : null,
		lotteryType : null,
		createdDateStart : null,
		createdDateEnd : null,
		operateType : "RECHARGE",
		reportType : 0,
		queryScope:3
};

rechargeWithdrawOrderPage.pageParam = {
        pageSize : 10,  //前台分页每页条数
        pageNo : 1,
        showPage:1,
        total:0       
};

$(document).ready(function() {
	//tab切换
	$('.js-tab a').on('click',function(){
		var index = $(this).index();
		$('.js-tab a').removeClass('on');
		$(this).addClass('on');
		$('.js-cont > .block').eq(index).show().siblings().hide();
	})

    var strDate = new Date();
    rechargeWithdrawOrderPage.queryParam.reportType=1;
	rechargeWithdrawOrderPage.queryParam.createdDateStart=strDate.setHours(3,-1,60,999);
	rechargeWithdrawOrderPage.queryParam.createdDateEnd=strDate.AddDays(1).setHours(2,59,-1,999);
    //点击切换查询盈亏报表事件
    $(".move-content").find("p").click(function(){
         var value = $(this).attr("data-value");
         var todate = new Date();
 			  
 		if(value=="today"){
 			rechargeWithdrawOrderPage.queryParam.reportType=1;
 			rechargeWithdrawOrderPage.queryParam.createdDateStart=todate.setHours(3,-1,60,999);
 			rechargeWithdrawOrderPage.queryParam.createdDateEnd=todate.AddDays(1).setHours(2,59,-1,999);
 			$(".header-right").find("span").html("今天");
 		}else if(value=="yesterDay"){
 			rechargeWithdrawOrderPage.queryParam.reportType=0;
 			rechargeWithdrawOrderPage.queryParam.createdDateEnd=todate.setHours(2,59,59,999);
 			rechargeWithdrawOrderPage.queryParam.createdDateStart=todate.AddDays(-1).setHours(3,-1,60,999);
 			$(".header-right").find("span").html("昨天");
 		}else if(value=="month"){
 			rechargeWithdrawOrderPage.queryParam.reportType=0;
 			var defalutDate=new Date();
 			defalutDate.setDate(1);
 			defalutDate.setHours(3,0,0,0);
 			rechargeWithdrawOrderPage.queryParam.createdDateStart=defalutDate;
 			rechargeWithdrawOrderPage.queryParam.createdDateEnd=todate.setHours(2,59,-1,999);
 			$(".header-right").find("span").html("本月");
 		}else if(value=="lastmonth"){
 			rechargeWithdrawOrderPage.queryParam.reportType=0;
			 var defalutDate=new Date();
			 defalutDate.setDate(1);
			 defalutDate.setHours(3,0,0,0);
			 todate.setMonth(todate.getMonth()-1);
			 todate.setDate(1);
			 todate.setHours(3,0,0,0);
			 rechargeWithdrawOrderPage.queryParam.createdDateStart=todate;
			 rechargeWithdrawOrderPage.queryParam.createdDateEnd=defalutDate;
			 $(".header-right").find("span").html("上个月");
		}
 		     
 		var parent=$(this).closest("div.lottery-classify1");
 		parent.hide();
 		$(".alert-bg").stop(false,true).fadeToggle(500);
 		rechargeWithdrawOrderPage.getOrderPage(rechargeWithdrawOrderPage.queryParam,1);
    })
    rechargeWithdrawOrderPage.getOrderPage(rechargeWithdrawOrderPage.queryParam,rechargeWithdrawOrderPage.pageParam.pageNo);
});

RechargeWithdrawOrderPage.prototype.getOrderPage=function(queryParam,pageNo){
	/*var lotteryType=$(".lottery-name span p").attr("data-value");*/
	var userName=$("#userName").val();
	if(userName!=null && userName!=""){
		rechargeWithdrawOrderPage.queryParam.userName=userName;
	}else{
		rechargeWithdrawOrderPage.queryParam.userName=null;
	}
	
	rechargeWithdrawOrderPage.pageParam.pageNo=pageNo;
	if(rechargeWithdrawOrderPage.queryParam.operateType=="RECHARGE"){
		rechargeWithdrawOrderPage.queryConditionUserRechargeOrders(rechargeWithdrawOrderPage.queryParam,rechargeWithdrawOrderPage.pageParam.pageNo);
	}else if(rechargeWithdrawOrderPage.queryParam.operateType=="WITHDRAW"){
		rechargeWithdrawOrderPage.queryConditionUserWithdrawOrders(rechargeWithdrawOrderPage.queryParam,rechargeWithdrawOrderPage.pageParam.pageNo);
	}
}

RechargeWithdrawOrderPage.prototype.getTabProstate=function(prostate){
	if(prostate!=null && prostate!=""){
		rechargeWithdrawOrderPage.queryParam.operateType=prostate;
	}else{
		rechargeWithdrawOrderPage.queryParam.operateType=null;
	}
	rechargeWithdrawOrderPage.getOrderPage(rechargeWithdrawOrderPage.queryParam,1);
}
/**
 * 条件查询充值记录
 */
RechargeWithdrawOrderPage.prototype.queryConditionUserWithdrawOrders = function(queryParam,pageNo){
	var param={}
	var userName=queryParam.userName;
	if(userName!=null && userName!=""){
		param.userName=userName;
		$("#withdrawOrderList").html('');
	}
	if(queryParam.operateType!=null && queryParam.operateType!=""){
    	param.operateType=queryParam.operateType;
	}
	/*if(pageNo!=null && pageNo!=""){
		param.pageNo=pageNo;
	}*/
	if(queryParam.reportType!=null && queryParam.reportType!=""){
    	param.reportType=queryParam.reportType;
	}
	if(queryParam.queryScope!=null && queryParam.queryScope!=""){
		param.queryScope=queryParam.queryScope;
	}
    param.createdDateStart=new Date(queryParam.createdDateStart).Format("yyyy-MM-dd");
    param.createdDateEnd=new Date(queryParam.createdDateEnd).Format("yyyy-MM-dd");
    var queryStr={"query":param,"pageNo":pageNo};
	$.ajax({
		type: "POST",
        url: contextPath +"/withdraw/withDrawOrderQuery",
        contentType :"application/json;charset=UTF-8",
        data: JSON.stringify(queryStr), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		var  data = result.data;
        		if(rechargeWithdrawOrderPage.queryParam.operateType=="WITHDRAW"){
        			rechargeWithdrawOrderPage.refreshUserWithdrawOrderPages(data);
        		}
        	}else if(result.code == "error"){
        		var err = result.data;
        		showTip(err);
        	}
        }
	});
};

/**
 * 条件查询充值记录
 */
RechargeWithdrawOrderPage.prototype.queryConditionUserRechargeOrders = function(queryParam,pageNo){
	var param={}
	var userName=queryParam.userName;
	if(userName!=null && userName!=""){
		param.userName=userName;
		$("#rechargeOrderList").html('');
   	 
	}
	if(queryParam.operateType!=null && queryParam.operateType!=""){
    	param.operateType=queryParam.operateType;
	}
	/*if(pageNo!=null && pageNo!=""){
		param.pageNo=pageNo;
	}*/
	if(queryParam.reportType!=null && queryParam.reportType!=""){
    	param.reportType=queryParam.reportType;
	}
	if(queryParam.queryScope!=null && queryParam.queryScope!=""){
		param.queryScope=queryParam.queryScope;
	}
    param.createdDateStart=new Date(queryParam.createdDateStart).Format("yyyy-MM-dd");
    param.createdDateEnd=new Date(queryParam.createdDateEnd).Format("yyyy-MM-dd");
    var queryStr={"query":param,"pageNo":pageNo};
	$.ajax({
		type: "POST",
        url: contextPath +"/recharge/rechargeOrderQuery",
        contentType :"application/json;charset=UTF-8",
        data: JSON.stringify(queryStr), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		var  data = result.data;
        		if(rechargeWithdrawOrderPage.queryParam.operateType=="RECHARGE"){
        			rechargeWithdrawOrderPage.refreshUserRechargeOrderPages(data);
        		}
        	}else if(result.code == "error"){
        		var err = result.data;
        		showTip(err);
        	}
        }
	});
};
/**
 * 刷新列表数据
 */
RechargeWithdrawOrderPage.prototype.refreshUserRechargeOrderPages = function(page){
	$(".span_more").remove();
	var userWithDrawOrderListObj = $("#rechargeOrderList");
	
	var str = "";
	var userWithDrawOrders = page.pageContent;
	//如果是第一页
	if(page.pageNo == 1) {
		userWithDrawOrderListObj.html("");
		if(userWithDrawOrders.length == 0){
	    	
			str += '<div class="line">';
	    	str += 		'<h4 class="nodata">暂无数据</h4>';
	    	str += '</div>';
	    	
	    	userWithDrawOrderListObj.append(str);
	    }
	} 
    if(userWithDrawOrders != null && userWithDrawOrders.length > 0){
    	//记录数据显示
    	for(var i = 0 ; i < userWithDrawOrders.length; i++){
    		
    		var userWithDrawOrder = userWithDrawOrders[i];
    		var serialNumberStr = userWithDrawOrder.serialNumber;
    		
    		str += '<div class="line">';
    		str += 		'<div class="child child1"><p>'+ userWithDrawOrder.userName  +'</p></div>';
    		str += 		'<div class="child child2"><p>'+ (userWithDrawOrder.applyValue).toFixed(frontCommonPage.param.fixVaue)+'</p></div>';
    		/*str += 		'<div class="child child3"><p>'+ (userWithDrawOrder.accountValue + userWithDrawOrder.feeValue).toFixed(frontCommonPage.param.fixVaue)+'</p></div>';*/

      		var payWayName = userWithDrawOrder.bankType;
    		var payType = userWithDrawOrder.payType;
    		var payTypeDes ="未知";
    		if(payType == "ALIPAY"){
    			payTypeDes ="支付宝";
    		}else if(payType == "WEIXIN"){
    			payTypeDes ="微信";
    		}else if(payType == "BANK_TRANSFER"){
    			payTypeDes ="网银";
    		}else if(payType == "QUICK_PAY"){
    			payTypeDes ="快捷支付";
    		}else if(payType == "ALIPAYWAP"){
    			payTypeDes ="支付宝WAP";
    		}else if(payType == "WEIXINWAP"){
    			payTypeDes ="微信WAP";
    		}else if(payType == "QQPAY"){
    			payTypeDes ="QQ钱包";
    		}else if(payType == "QQPAYWAP"){
    			payTypeDes ="QQ钱包WAP";
    		}else if(payType == "UNIONPAY"){
    			payTypeDes ="银联扫码";
    		}else if(payType == "JDPAY"){
    			payTypeDes ="京东扫码";
    		}else if(payType == "WEIXINBARCODE"){
    			payTypeDes ="微信条形码";
    		}
    		str += 		'<div class="child child4"><p>'+ payTypeDes +'</p></div>';
    		str += 		'<div class="child child5"><p>'+ userWithDrawOrder.createdDateStr +'</p></div>';
    		
    		if(userWithDrawOrder.statusStr == "支付成功"){
    			str += 		'<div class="child child6 font-green">';
    		}else if(userWithDrawOrder.statusStr == "支付失败"){
    			str += 	'<div class="child child6 font-red">';
    		}else if(userWithDrawOrder.statusStr == "支付中"){
    			str += 	'<div class="child child6 font-yellow">';
    		}else{
    			str += 	'<div class="child child6 font-blue">';
    		}
    		str += '<p>'+ userWithDrawOrder.statusStr +'</p></div>';
    		str += '</div>';
    		
    		userWithDrawOrderListObj.append(str);
    		str = "";
    	}
    	 if(userWithDrawOrders != null && userWithDrawOrders.length == 0){
    		 userWithDrawOrderListObj.html('');
    	 }
    	//如果当前页数小于总页数，显示加载更多
    	if(page.pageNo < page.totalPageNum) {
    		$(".user-btn").show();
    		//添加加载更多事件
    		$(".user-btn").unbind("click").click(function(){
    			rechargeWithdrawOrderPage.pageParam.pageNo++;
    			rechargeWithdrawOrderPage.queryConditionUserRechargeOrders(rechargeWithdrawOrderPage.queryParam,rechargeWithdrawOrderPage.pageParam.pageNo);
    		});
    	}
    	
    	//绑定点击事件
    	$(".list_item").unbind("click").click(function(){
    		var dataVal = $(this).attr("data-val");
    		/*window.location.href = contextPath+"/gameBet/mobile/rechargewithdraw/rechargewithdraw_detail.html?param1="+dataVal+"&param2="+rechargeWithdrawOrderPage.param.queryType;*/
    	});
    }
	
};

/**
 * 刷新列表数据
 */
RechargeWithdrawOrderPage.prototype.refreshUserWithdrawOrderPages = function(page){
	$(".span_more").remove();
	var userWithDrawOrderListObj = $("#withdrawOrderList");
	
	var str = "";
	var userWithDrawOrders = page.pageContent;
	
	//如果是第一页
	if(page.pageNo == 1) {
		userWithDrawOrderListObj.html("");
		if(userWithDrawOrders.length == 0){
			str += '<div class="line">';
	    	str += 		'<h4 class="nodata">暂无数据</h4>';
	    	str += '</div>';
	       
	    	userWithDrawOrderListObj.append(str);
	    }
	} 
    if(userWithDrawOrders != null && userWithDrawOrders.length > 0){
    	//记录数据显示
    	for(var i = 0 ; i < userWithDrawOrders.length; i++){
    		
    		var userWithDrawOrder = userWithDrawOrders[i];
    		var serialNumberStr = userWithDrawOrder.serialNumber;
    		
    		str += '<div class="line">';
    		str += 		'<div class="child child1_"><p>'+ userWithDrawOrder.userName  +'</p></div>';
    		str += 		'<div class="child child2_"><p>'+ userWithDrawOrder.applyValue +'</p></div>';
    		str += 		'<div class="child child4_" ><p>'+ userWithDrawOrder.createdDateStr +'</p></div>';
    		
    		if(userWithDrawOrder.statusStr == "提现成功"){
    			str += 		'<div class="child child5_ font-green">';
    		}else if(userWithDrawOrder.statusStr == "提现失败"){
    			str += 	'<div class="child child5_ font-red">';
    		}else if(userWithDrawOrder.statusStr == "支付中"){
    			str += 	'<div class="child child5_ font-yellow">';
    		}else{
    			str += 	'<div class="child child5_ font-blue">';
    		}
    		
    		str += '<p>'+ userWithDrawOrder.statusStr +'</p></div>';
    		str += '</div>';
    		
    		userWithDrawOrderListObj.append(str);
    		str = "";
    	}
    	if(userWithDrawOrders != null && userWithDrawOrders.length == 0){
    		userWithDrawOrderListObj.html('');
    	}
    	
    	//如果当前页数小于总页数，显示加载更多
    	if(page.pageNo < page.totalPageNum) {
    		$(".user-btn").show();
    		
    		//添加加载更多事件
    		$(".user-btn").unbind("click").click(function(){
    			rechargeWithdrawOrderPage.pageParam.pageNo++;
    			rechargeWithdrawOrderPage.queryConditionUserWithdrawOrders(rechargeWithdrawOrderPage.queryParam,rechargeWithdrawOrderPage.pageParam.pageNo);
    		});
    	}
    }
	
};

Date.prototype.Format = function (fmt) {
	    var o = {
	        "M+": this.getMonth() + 1, //月份 
	        "d+": this.getDate(), //日 
	        "h+": this.getHours(), //小时 
	        "m+": this.getMinutes(), //分 
	        "s+": this.getSeconds(), //秒 
	        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
	        "S": this.getMilliseconds() //毫秒 
	    };
	    if (/(y+)/.test(fmt))fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
	    for (var k in o){
	    if (new RegExp("(" + k + ")").test(fmt)) {fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));}
	    }
	    return fmt;
	}

