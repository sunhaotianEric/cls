/*
 * 代理报表
 */
function AgentReportPage(){};
var agentReportPage=new AgentReportPage();

agentReportPage.queryParam = {
		userName : null,
		teamLeaderName : null,
		createdDateStart : null,
		createdDateEnd : null,
		reportType : 0
};
agentReportPage.param = {
		dateRange:1
}
$(document).ready(function(){
	
	   var defalutDate = new Date();
	   var strDate = new Date();
	   //defalutDate.setDate(1);
	   defalutDate.setHours(2,59,59,999);
	   agentReportPage.queryParam.reportType = 0;
	   agentReportPage.queryParam.createdDateEnd = strDate.AddDays(1).setHours(2,59,59,999);
	   agentReportPage.queryParam.createdDateStart =strDate.setHours(3,0,0,0);
			  
	   //点击切换查询盈亏报表事件
	   $(".move-content").find("p").click(function(){
	        var value = $(this).attr("data-value");
	        var todate = new Date();
				  
			if(value=="today"){
				 agentReportPage.queryParam.reportType=1;
				 agentReportPage.queryParam.createdDateStart=todate.setHours(3,0,0,0);
			     agentReportPage.queryParam.createdDateEnd=todate.AddDays(1).setHours(2,59,59,999);
				 $(".header-right").find("span").html("今天");
				 agentReportPage.param.dateRange=1;
				 agentReportPage.findUserConsumeReportByQueryParam(1);
			}else if(value=="yesterDay"){
				 agentReportPage.queryParam.reportType=0;
				 agentReportPage.queryParam.createdDateEnd=todate.setHours(2,59,59,999);
				 agentReportPage.queryParam.createdDateStart=todate.AddDays(-1).setHours(3,0,0,0);
				 $(".header-right").find("span").html("昨天");
				 agentReportPage.param.dateRange=-1;
				 agentReportPage.findUserConsumeReportByQueryParam(-1);
			}else if(value=="month"){
				 agentReportPage.queryParam.reportType=0;
				 var defalutDate=new Date();
				 defalutDate.setDate(1);
				 defalutDate.setHours(3,0,0,0);
				 agentReportPage.queryParam.createdDateStart=defalutDate;
				 agentReportPage.queryParam.createdDateEnd=todate.setHours(2,59,59,999);
				 $(".header-right").find("span").html("本月");
				 agentReportPage.param.dateRange=30;
				 agentReportPage.findUserConsumeReportByQueryParam(30);
			}else if(value=="lastmonth"){
				 agentReportPage.queryParam.reportType=0;
				 var defalutDate=new Date();
				 defalutDate.setDate(1);
				 defalutDate.setHours(3,0,0,0);
				 todate.setMonth(todate.getMonth()-1);
				 todate.setDate(1);
				 todate.setHours(3,0,0,0);
				 agentReportPage.queryParam.createdDateStart=todate;
				 agentReportPage.queryParam.createdDateEnd=defalutDate;
				 $(".header-right").find("span").html("上个月");
				 agentReportPage.param.dateRange=-30;
				 agentReportPage.findUserConsumeReportByQueryParam(-30);
			}
			     
			var parent=$(this).closest("div.lottery-classify1");
			parent.hide();
			$(".alert-bg").stop(false,true).fadeToggle(500);
	})
	agentReportPage.findUserConsumeReportByQueryParam(1);
});

/**
 * 按页面条件查询数据
 */
AgentReportPage.prototype.findUserConsumeReportByQueryParam = function(dateRange){
	agentReportPage.queryParam.userName = getSearchVal("userName");
	agentReportPage.queryConditionUserConsumeReports(agentReportPage.queryParam,dateRange);
	
};

/**
 * 刷新列表数据
 */
AgentReportPage.prototype.refreshUserConsumeReports = function(report){
	if(report!=null){
		$("#lottery").html('￥'+(report.lottery));
		$("#win").html('￥'+(report.win));
		$("#activimoney").html('￥'+report.activity);
		$("#rebatePercentage").html('￥'+report.rebate);
		$("#gain").html('￥'+report.gain);
		$("#lotteryCount").html((report.lotteryCount==null?"-":report.lotteryCount));
		$("#firstRechargeCount").html((report.firstRechargeCount==null?"-":report.firstRechargeCount));
		$("#regCount").html((report.regCount==null?"-":report.regCount));
		$("#recharges").html('￥'+report.recharge);
		$("#withDraw").html('￥'+report.withdraw);
	}else{
		$("#lottery").html('￥0.00');
		$("#win").html('￥0.00');
		$("#activimoney").html('￥0.00');
		$("#rebatePercentage").html('￥0.00');
		$("#gain").html('￥0.00');
		$("#lotteryCount").html("0");
		$("#firstRechargeCount").html("0");
		$("#regCount").html("0");
		$("#recharges").html('￥0.00');
		$("#withDraw").html('￥0.00');
	}
	
	//金额过长自动换行
	 $('.consume_report_detail .block').each(function(){
		   var maxHeight = $(this).find('.main-child .child-value span').eq(0).height();
		   var $block = $(this);
		   $block.find('.main-child .child-value span').each(function(){
			   if($(this).height() > maxHeight) {
				   maxHeight = $(this).height();
			   }
		   })
		   $block.find('.main-child .child-value').css('height',maxHeight);
	 })
};

/**
 * 条件查询代理报表
 */
AgentReportPage.prototype.queryConditionUserConsumeReports = function(queryParam,dateRange){
	/*frontTeamAction.getAgentReportForQuery(queryParam,dateRange,function(r){
		if (r[0] != null && r[0] == "ok") {
			agentReportPage.refreshUserConsumeReports(r[1]);
		}else if(r[0] != null && r[0] == "error"){
			alert(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "查询团队消费报表请求失败.");
		}
    });*/
	var param={}
	if(queryParam.userName!=null && queryParam.userName!=""){
		param.userName=queryParam.userName;
	}
    if(queryParam.teamLeaderName!=null && queryParam.teamLeaderName!=""){
    	param.teamLeaderName=queryParam.teamLeaderName;
	}
    if(queryParam.reportType!=null && queryParam.reportType!=""){
    	param.reportType=queryParam.reportType;
	}
    if(agentReportPage.param.dateRange!=null && agentReportPage.param.dateRange!=""){
    	param.dateRange=agentReportPage.param.dateRange;
	}
    param.createdDateStart=new Date(queryParam.createdDateStart).Format("yyyy-MM-dd");
    param.createdDateEnd=new Date(queryParam.createdDateEnd).Format("yyyy-MM-dd");
	$.ajax({
		type: "POST",
        url: contextPath +"/report/agentReportQuery",
        contentType : "application/json",
        data: JSON.stringify(param), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		var  data = result.data;
        		agentReportPage.refreshUserConsumeReports(data);
        	}else if(result.code == "error"){
        		var err = result.data;
        		showTip(err);
        	}
        }
	});
};

Date.prototype.Format = function (fmt) {
	    var o = {
	        "M+": this.getMonth() + 1, //月份 
	        "d+": this.getDate(), //日 
	        "h+": this.getHours(), //小时 
	        "m+": this.getMinutes(), //分 
	        "s+": this.getSeconds(), //秒 
	        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
	        "S": this.getMilliseconds() //毫秒 
	    };
	    if (/(y+)/.test(fmt))fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
	    for (var k in o){
	    if (new RegExp("(" + k + ")").test(fmt)) {fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));}
	    }
	    return fmt;
}