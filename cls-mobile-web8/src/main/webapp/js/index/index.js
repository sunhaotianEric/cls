function IndexPage() {
}
var indexPage = new IndexPage();

var banner = new change();

var notice = new change();

// 选中图片的数组集合
var selectedArray = new Array();

indexPage.param = {
	kindName : 'JSKS', // 彩种
	LotteryKindName : '江苏快3',
	position1LotteryType : null,
	position2LotteryType : null,
	position3LotteryType : null,
	position4LotteryType : null,
	position5LotteryType : null,
	position6LotteryType : null,
	bizSystemConfigVO : null
};
// 分页参数
indexPage.pageParam = {
	pageNo : 1,
	pageSize : 2
};

$(document).ready(function() {
	$(".i-home").parent().addClass("on");
	// 登录用户信息
	if (!$.isEmptyObject(currentUser)) {
		indexPage.getCurrentUserMsg();
		// 展示公告
		indexPage.getNewestAnnounce();
		$("div.header-left.line-icon").show();
		$("#userInfo").show();
		$(".notice").show();
	} else {
		$("div.header-left.line-icon").hide();
		$("#userLogin").show();
	}

	setTimeout("indexPage.initIndexLastLotteryCode()", 1000);

	// 加载所有彩种的奖金映射
	indexPage.loadAllLotteryWins();

	// 打开客服
	$("#customServicekf").click(function() {
		window.location.href = customUrl;
		/*
		 * frontCommonPage.showCustomService(); var ua = navigator.userAgent;
		 * if(ua.match(/iPhone|iPod/i) != null){ //showTip("iphone");
		 * window.location.href = customServiceUrl; }else
		 * if(ua.match(/Android/i) != null){ //android代码 //showTip("Android");
		 * frontCommonPage.showCustomService(); } else if(ua.match(/iPad/i) !=
		 * null){ //showTip("iPad"); window.location.href = customServiceUrl; }
		 */
	});

	// 注释该行，收藏提示打开
	$('.add-to-home').hide();

	// 新手大礼包
	$('.receive-btn').click(function() {
		window.location.href = contextPath + "/gameBet/newUserGift.html";
	})
	$('.close-btn').click(function() {
		$('.pop').hide();
		$('.pop-btn').show();
	})
	$('.pop-btn').click(function() {
		$('.pop').show();
		$('.pop-btn').hide();
	});
	// 是否弹出新手礼包活动弹框
	indexPage.showNewUserGift();

	// 足球外链处理
	if (indexPage.param.bizSystemConfigVO.isFootballOpen == 1) {
		$('.footballgame').show();
		$('.footballgame >a').attr("href", indexPage.param.bizSystemConfigVO.footballUrl);
		/* $('.notice').addClass('footballgame-on'); */
	} else {
		$('.footballgame').hide();
		/* $('.notice').removeClass('footballgame-on'); */
	}
});

IndexPage.prototype.initIndexLastLotteryCode = function() {

	// 获取首页展示图片
	indexPage.getHomeActivityImage();
	// 刷新用户余额
	if (!$.isEmptyObject(currentUser)) {
		$("div.header-left.line-icon").show();
		$("#indexUserName").text(currentUser.userName);
		// 加载头部消息数据
		indexPage.getHeadNoReadMsg();
		indexPage.getYestoday();
	} else {
		$("div.header-left.line-icon").hide();
	}

	// 首页彩种
	indexPage.getIndexLotteryKind();

	// 获取当前用户的自定义彩种信息
	indexPage.loadCurrentUserLotteryKind();

	objTimer = window.setInterval("indexPage.getLastLotteryCode()", 10000); // 每隔十秒读取最新的开奖号码

	// 点击上方删除图片事件
	$("#usersLotteryKindId div").click(function() {
		var index = $(this).index();
		selectedArray.splice(index, 1);
		// 更新图片
		indexPage.updateTopSelected();
	});

	// 点击下方添加收藏彩种图片事件
	$("#lotteryKindId div").click(function() {
		if (selectedArray.length < 6) {
			var have = true;
			for (var i = 0; i < selectedArray.length; i++) {
				var currentLotteryKind = selectedArray[i];
				if (currentLotteryKind === $(this).attr("data-val")) {
					showTip("彩种收藏重复了！");
					have = false;
					return;
				}
			}
			if (have) {
				selectedArray.push($(this).attr("data-val"));
				// 更新图片
				indexPage.updateTopSelected();
			}
		} else {
			showTip("可添加的自定义彩种已满!");
		}
	});

};

/**
 * 展示公告
 */
IndexPage.prototype.getNewestAnnounce = function() {
	$.ajax({
		type : "POST",
		url : contextPath + "/announces/getIndexAnnounce",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code != null && result.code == "ok") {
				var userAnnouncesList = result.data;
				if (userAnnouncesList != null && userAnnouncesList.length > 0) {
					var title = "";
					var content = "";
					var str = "";
					for (var i = 0; i < userAnnouncesList.length; i++) {
						var userAnnounce = userAnnouncesList[i];
						if (userAnnounce.title.length > 22) {
							title = userAnnounce.title.substring(0, 20) + "..."
						} else {
							title = userAnnounce.title;
						}
						if (userAnnounce.content.length > 190) {
							content = userAnnounce.content.substring(0, 190) + "..."
						} else {
							content = userAnnounce.content;
						}
						str += "<a href='" + contextPath + "/gameBet/announceDetail.html?id=" + userAnnounce.id + "'><p style='color:#000'>" + title + "</p></a>";
					}

					$("#notice").html(str);
					notice.init({
						"element_move" : ".notice .content",
						"element" : ".notice .content p",
						"position_width" : 904,
						"position_style" : "loop",
						"autoplay_time" : 2000,
						"autoplay" : true
					});
				} else {
					$(".notice").hide();
				}
			} else if (result.code != null && result.code == "error") {
				showTip(result.data);
			} else {
				showTip(jQuery(document.body), "查询网站公告数据失败.");
			}
		}
	})
};

/**
 * 显示新手大礼包 如果从r[1]获取的值为false就不弹新手礼包弹框,如果为true就弹出
 */
IndexPage.prototype.showNewUserGift = function() {
	

		$.ajax({
		type : "POST",
		url : contextPath + "/activity/isShowNewUserGift",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				if (result.data == true && result.data4 != false) {
					$(".pop").show();
				} else {
					$(".pop").hide();
				}
				// 设置右侧栏中的新手礼包显示功能
				if (result.data2 == true && result.data4 != false) {
					$(".pop-btn").show();
				} else {
					$(".pop-btn").hide();
				}
				// 设置新手礼包弹框时候的总价格为(data3)
				if (result.data3 != null) {
					$("#newUserGiftTaskAlert").text(result.data3 + "元新手大礼包!");
				} else {
					$("#newUserGiftTaskAlert").text("领取新手大礼包!");
				}
			} else if (result.code == "error") {
				showTip("系统错误");
			}
		}
	});
};

/**
 * 获取昨日中奖金额
 */
IndexPage.prototype.getYestoday = function() {
	
	$.ajax({
		type : "POST",
		url : contextPath + "/lottery/getYestodayWin",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if( result.code == "ok"){
				$("#yestodayWinId").html(result.data);
			}else if(result.code == "error"){
				showTip(result.data);
			}
		}
	});
};

/**
 * 首页彩种
 */
IndexPage.prototype.getIndexLotteryKind = function() {
		$.ajax({
				type : "POST",
				url : contextPath + "/system/getBizSystemInfo",
				contentType : 'application/json;charset=utf-8',
				success : function(result) {
					commonHandlerResult(result, this.url);
					if (result.code == "ok") {
						var resultData = result.data;
						var indexLotteryKind = resultData.indexLotteryKind;
						// 修改网站的标题
						$("#indexTitle").text(resultData.bizSystemName);
						if (indexLotteryKind != null && typeof (indexLotteryKind) != "undefined") {
							// 未开发彩种不在首页彩种之列
							if (indexLotteryKind != "FCSDDPC") {

								indexPage.param.LotteryKindName = resultData.indexLotteryKindName;
								indexPage.param.kindName = resultData.indexLotteryKind;
								var lotteryUrl = "";
								if (indexLotteryKind == "BJPK10") {
									lotteryUrl = contextPath + "\/gameBet/lottery/bjpks/" + indexLotteryKind.toString().toLowerCase() + ".html";
								} else if (indexLotteryKind == "JLFFC") {
									lotteryUrl = contextPath + "\/gameBet/lottery/xyffc/xyffc.html";
								} else if (indexLotteryKind == "XYEB") {
									lotteryUrl = contextPath + "\/gameBet/lottery/xyeb/xy28.html";
								} else if (indexLotteryKind == "JSPK10") {
									lotteryUrl = contextPath + "\/gameBet/lottery/jspks/jspk10.html";
								} else {
									lotteryUrl = contextPath + "\/gameBet/lottery/" + indexLotteryKind.toString().toLowerCase() + "/" + indexLotteryKind.toString().toLowerCase()
											+ ".html";
								}
								$(".bet").attr("onclick", "javascript:judgeCurrentUserToLogin('" + lotteryUrl + "','去投注用户必须要先登录')");
							}
						}
						// 若没有设置首页彩种执行默认首页彩种的最新的开奖号码
						if (indexPage.param.LotteryKindName == '六合彩' || indexPage.param.LotteryKindName == '幸运六合彩') {
							indexPage.initShengXiaoNumber();
						} else {
							indexPage.getLastLotteryCode();
						}

					} else if (result.code == "error") {
						showTip(result.data);
					}
				}
		});
}

/**
 * 获取最新的开奖号码
 */
IndexPage.prototype.getLastLotteryCode = function() {
	var jsonData = {
			"lotteryKind" : indexPage.param.kindName,	
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/code/getLastLotteryCode",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			var resultData = result.data;
			if (result.code == "ok") {
				indexPage.showLastLotteryCode(resultData);
			} else if (result.code == "error") {
				showTip(resultData);
			}
		}
	});
};

/**
 * 展示最新的开奖号码
 */
IndexPage.prototype.showLastLotteryCode = function(lotteryCode) {
	if (lotteryCode != null) {
		var lotteryNum = lotteryCode.lotteryNum;
		var lotteryName = indexPage.param.kindName;
		var expectDay = lotteryNum.substring(0, lotteryNum.length - 3);
		var expectNum = lotteryNum.substring(lotteryNum.length - 3, lotteryNum.length);
		if (lotteryName == "BJPK10" || lotteryName == "BJKS" || lotteryName == "HGFFC" || lotteryName == "XJPLFC" || lotteryName == "XYEB") {
			$('.lastnumber').html(indexPage.param.LotteryKindName + " 第" + lotteryNum + "期")
		} else {
			$('.lastnumber').html(indexPage.param.LotteryKindName + " 第" + expectDay + "-" + expectNum + "期");
		}

		if (typeof (lotteryName) != "undefined" && lotteryName != null) {
			if (lotteryName.indexOf("KS") > 0) {
				$("#ksLottery").show();
				$("#notKslotteryKind").hide();
				indexPage.showLastLotteryPictureCode(lotteryCode);
			} else if (lotteryName == 'LHC' || lotteryName == 'XYLHC') {
				var colors = indexPage.changeColorByCode(lotteryCode);
				$(".numInfo1, #lotteryNumber1 .btn-title").addClass("lhc-" + colors[0]);
				$(".numInfo2, #lotteryNumber2 .btn-title").addClass("lhc-" + colors[1]);
				$(".numInfo3, #lotteryNumber3 .btn-title").addClass("lhc-" + colors[2]);
				$(".numInfo4, #lotteryNumber4 .btn-title").addClass("lhc-" + colors[3]);
				$(".numInfo5, #lotteryNumber5 .btn-title").addClass("lhc-" + colors[4]);
				$(".numInfo6, #lotteryNumber6 .btn-title").addClass("lhc-" + colors[5]);
				$(".numInfo7, #lotteryNumber7 .btn-title").addClass("lhc-" + colors[6]);

				$('.numInfo1').html(lotteryCode.numInfo1);
				$('.numInfo2').html(lotteryCode.numInfo2);
				$('.numInfo3').html(lotteryCode.numInfo3);
				$('.numInfo4').html(lotteryCode.numInfo4);
				$('.numInfo5').html(lotteryCode.numInfo5);
				$('.numInfo6').html(lotteryCode.numInfo6);
				$('.numInfo7').html(lotteryCode.numInfo7);
				$('.numInfo1, .numInfo2, .numInfo3, .numInfo4, .numInfo5, .numInfo6, .numInfo7').show();

				$('#lotteryNumber1 .btn-title').text(indexPage.returnAninal(lotteryCode.numInfo1));
				$('#lotteryNumber2 .btn-title').html(indexPage.returnAninal(lotteryCode.numInfo2));
				$('#lotteryNumber3 .btn-title').html(indexPage.returnAninal(lotteryCode.numInfo3));
				$('#lotteryNumber4 .btn-title').html(indexPage.returnAninal(lotteryCode.numInfo4));
				$('#lotteryNumber5 .btn-title').html(indexPage.returnAninal(lotteryCode.numInfo5));
				$('#lotteryNumber6 .btn-title').html(indexPage.returnAninal(lotteryCode.numInfo6));
				$('#lotteryNumber7 .btn-title').html(indexPage.returnAninal(lotteryCode.numInfo7));

				$('.and').show();
				$('.btn-title').show();
				$('.dice-content').addClass('p0-20');
				$(".dice").removeClass("muns-big").addClass("muns-small");
			} else if (lotteryName == 'XYEB') {
				var sum = Number(lotteryCode.numInfo1) + Number(lotteryCode.numInfo2) + Number(lotteryCode.numInfo3);
				$('.numInfo1').html(lotteryCode.numInfo1);
				$('.numInfo2').html(lotteryCode.numInfo2);
				$('.numInfo3').html(lotteryCode.numInfo3);
				$('.numInfo4').html(sum);
				$('.numInfo1').show();
				$('.numInfo2').show();
				$('.numInfo3').show();
				$('.numInfo4').show();
				$('.numInfo4').removeClass("color-red").addClass(indexPage.changeColorByNumber(sum));
				$('.symbol').show();
			} else {

				$('.numInfo1').html(lotteryCode.numInfo1);
				$('.numInfo2').html(lotteryCode.numInfo2);
				$('.numInfo3').html(lotteryCode.numInfo3);
				$('.numInfo1').show();
				$('.numInfo2').show();
				$('.numInfo3').show();
				if (lotteryCode.numInfo4 != null && typeof (lotteryCode.numInfo4) != "undefined") {
					$('.numInfo4').html(lotteryCode.numInfo4);
					$('.numInfo4').show();
				} else {
					$(".dice").removeClass("muns-small").addClass("muns-big");
				}
				if (lotteryCode.numInfo5 != null && typeof (lotteryCode.numInfo5) != "undefined") {
					$('.numInfo5').html(lotteryCode.numInfo5);
					$('.numInfo5').show();
				} else {
					$(".dice").removeClass("muns-small").addClass("muns-big");
				}
				if (lotteryCode.numInfo6 != null && typeof (lotteryCode.numInfo6) != "undefined") {
					$('.numInfo6').html(lotteryCode.numInfo6);
					$('.numInfo6').show();
					$(".dice").removeClass("muns-big").addClass("muns-small");
				} else {
					$(".dice").removeClass("muns-small").addClass("muns-big");
				}
				if (lotteryCode.numInfo7 != null && typeof (lotteryCode.numInfo7) != "undefined") {
					$('.numInfo7').html(lotteryCode.numInfo7);
					$('.numInfo7').show();
				}
				if (lotteryCode.numInfo8 != null && typeof (lotteryCode.numInfo8) != "undefined") {
					$('.numInfo8').html(lotteryCode.numInfo8);
					$('.numInfo8').show();
				}
				if (lotteryCode.numInfo9 != null && typeof (lotteryCode.numInfo9) != "undefined") {
					$('.numInfo9').html(lotteryCode.numInfo9);
					$('.numInfo9').show();
				}
				if (lotteryCode.numInfo10 != null && typeof (lotteryCode.numInfo10) != "undefined") {
					$('.numInfo10').html(lotteryCode.numInfo10);
					$('.numInfo10').show();
				}
			}
		}
		// 快三展示最新的开奖号码的图片
		// lotteryCommonPage.showLastLotteryPictureCode(lotteryCode,null);

	} else {
		// 若设置的首页彩种没有开奖号码
		$('.lastnumber').html(indexPage.param.LotteryKindName + " 第---期")
		if (indexPage.param.LotteryKindName.indexOf("快3") > 0) {
			$("#ksLottery").show();
			$("#notKslotteryKind").hide();
		}
	}
};

IndexPage.prototype.changeColorByNumber = function(i) {
	var colors = "";
	// 红波
	if (i == 3 || i == 6 || i == 9 || i == 12 || i == 15 || i == 18 || i == 21 || i == 24) {
		colors = "color-red";
		// 绿波
	} else if (i == 1 || i == 4 || i == 7 || i == 10 || i == 16 || i == 19 || i == 22 || i == 25) {
		colors = "color-green";
		// 蓝波
	} else if (i == 2 || i == 5 || i == 8 || i == 11 || i == 17 || i == 20 || i == 23 || i == 26) {
		colors = "color-blue";
		// 灰色
	} else if (i == 0 || i == 13 || i == 14 || i == 27) {
		colors = "color-gray";
	}
	return colors;
}

/**
 * 展示快三最新的开奖号码的图片
 */
IndexPage.prototype.showLastLotteryPictureCode = function(lotteryCode) {
	if (lotteryCode == null || typeof (lotteryCode) == "undefined") {
		return;
	}

	// 先移除开奖号码样式（更新开奖号码）
	$('#index_lotteryNumber1').attr("src", "");
	if (lotteryCode.numInfo1 == "1") {
		$('#index_lotteryNumber1').attr("src", contextPath + "/images/dice_pic/index/1.png");
	} else if (lotteryCode.numInfo1 == "2") {
		$('#index_lotteryNumber1').attr("src", contextPath + "/images/dice_pic/index/2.png");
	} else if (lotteryCode.numInfo1 == "3") {
		$('#index_lotteryNumber1').attr("src", contextPath + "/images/dice_pic/index/3.png");
	} else if (lotteryCode.numInfo1 == "4") {
		$('#index_lotteryNumber1').attr("src", contextPath + "/images/dice_pic/index/4.png");
	} else if (lotteryCode.numInfo1 == "5") {
		$('#index_lotteryNumber1').attr("src", contextPath + "/images/dice_pic/index/5.png");
	} else if (lotteryCode.numInfo1 == "6") {
		$('#index_lotteryNumber1').attr("src", contextPath + "/images/dice_pic/index/6.png");
	}

	// 先移除开奖号码样式（更新开奖号码）
	$('#index_lotteryNumber2').attr("src", "");
	if (lotteryCode.numInfo2 == "1") {
		$('#index_lotteryNumber2').attr("src", contextPath + "/images/dice_pic/index/1.png");
	} else if (lotteryCode.numInfo2 == "2") {
		$('#index_lotteryNumber2').attr("src", contextPath + "/images/dice_pic/index/2.png");
	} else if (lotteryCode.numInfo2 == "3") {
		$('#index_lotteryNumber2').attr("src", contextPath + "/images/dice_pic/index/3.png");
	} else if (lotteryCode.numInfo2 == "4") {
		$('#index_lotteryNumber2').attr("src", contextPath + "/images/dice_pic/index/4.png");
	} else if (lotteryCode.numInfo2 == "5") {
		$('#index_lotteryNumber2').attr("src", contextPath + "/images/dice_pic/index/5.png");
	} else if (lotteryCode.numInfo2 == "6") {
		$('#index_lotteryNumber2').attr("src", contextPath + "/images/dice_pic/index/6.png");
	}

	// 先移除开奖号码样式（更新开奖号码）
	$('#index_lotteryNumber3').attr("src", "");
	if (lotteryCode.numInfo3 == "1") {
		$('#index_lotteryNumber3').attr("src", contextPath + "/images/dice_pic/index/1.png");
	} else if (lotteryCode.numInfo3 == "2") {
		$('#index_lotteryNumber3').attr("src", contextPath + "/images/dice_pic/index/2.png");
	} else if (lotteryCode.numInfo3 == "3") {
		$('#index_lotteryNumber3').attr("src", contextPath + "/images/dice_pic/index/3.png");
	} else if (lotteryCode.numInfo3 == "4") {
		$('#index_lotteryNumber3').attr("src", contextPath + "/images/dice_pic/index/4.png");
	} else if (lotteryCode.numInfo3 == "5") {
		$('#index_lotteryNumber3').attr("src", contextPath + "/images/dice_pic/index/5.png");
	} else if (lotteryCode.numInfo3 == "6") {
		$('#index_lotteryNumber3').attr("src", contextPath + "/images/dice_pic/index/6.png");
	}
};

/**
 * 根据数组更新上方所选的图片
 */
IndexPage.prototype.updateTopSelected = function() {
	// 先删除所有图片
	$("#usersLotteryKindId div").empty();
	if (selectedArray.length > 0) {
		for (var i = 0; i < selectedArray.length; i++) {
			var currentLotteryKind = selectedArray[i];
			var picSrc = "";
			var titel = "";
			if ("CQSSC" == currentLotteryKind) {
				picSrc = "logo_shishicai.png";
				titel = "重庆";
			} else if ("JXSSC" == currentLotteryKind) {
				picSrc = "logo_shishicai.png";
				titel = "江西";
			} else if ("TJSSC" == currentLotteryKind) {
				picSrc = "logo_shishicai.png";
				titel = "天津";
			} else if ("XJSSC" == currentLotteryKind) {
				picSrc = "logo_shishicai.png";
				titel = "新疆";
			} else if ("JSKS" == currentLotteryKind) {
				picSrc = "logo_kuaisan.png";
				titel = "江苏";
			} else if ("AHKS" == currentLotteryKind) {
				picSrc = "logo_kuaisan.png";
				titel = "安徽";
			} else if ("JYKS" == currentLotteryKind) {
				picSrc = "logo_kuaisan.png";
				titel = "幸运";
			} else if ("GXKS" == currentLotteryKind) {
				picSrc = "logo_kuaisan.png";
				titel = "广西";
			} else if ("HBKS" == currentLotteryKind) {
				picSrc = "logo_kuaisan.png";
				titel = "湖北";
			} else if ("JLKS" == currentLotteryKind) {
				picSrc = "logo_kuaisan.png";
				titel = "台湾";
			} else if ("BJKS" == currentLotteryKind) {
				picSrc = "logo_kuaisan.png";
				titel = "北京";
			} else if ("GDSYXW" == currentLotteryKind) {
				picSrc = "logo_shiyixuanwu.png";
				titel = "广东";
			} else if ("SDSYXW" == currentLotteryKind) {
				picSrc = "logo_shiyixuanwu.png";
				titel = "山东";
			} else if ("JXSYXW" == currentLotteryKind) {
				picSrc = "logo_shiyixuanwu.png";
				titel = "江西";
			} else if ("FJSYXW" == currentLotteryKind) {
				picSrc = "logo_shiyixuanwu.png";
				titel = "福建";
			} else if ("JLFFC" == currentLotteryKind) {
				picSrc = "logo_fenfencai.png";
				titel = "幸运";
			} else if ("HGFFC" == currentLotteryKind) {
				picSrc = "logo_yidianwufencai.png";
				titel = "韩国";
			} else if ("BJPK10" == currentLotteryKind) {
				picSrc = "logo_pk10.png";
				titel = "北京";
			} else if ("XYEB" == currentLotteryKind) {
				picSrc = "logo_xy28.png";
				titel = "幸运28";
			} else if ("XJPLFC" == currentLotteryKind) {
				picSrc = "logo_erfencai.png";
				titel = "新加坡";
			} else if ("TWWFC" == currentLotteryKind) {
				picSrc = "logo_wufencai.png";
				titel = "台湾";
			} else if ("FC3D" == currentLotteryKind) {
				picSrc = "logo_dipincai.png";
				titel = "福彩";
			}

			// 改变图片
			var picHtml = "<img class='img1' src='images/lottery_logo/index/" + picSrc + "'/>";
			picHtml += "<h2 class='child-title'>" + titel + "</h2>";
			picHtml += "<img src='images/icon/icon-close2.png' alt='' class='icon child-btn close-btn'>";
			// 改变图片
			$("#usersLotteryKindId div:eq(" + i + ")").html(picHtml);

		}
		$("#usersLotteryKindId div").each(function(i) {
			if ($("#usersLotteryKindId div:eq(" + i + ") img").length == 0) {
				if ($("#usersLotteryKindId div").length > 6) {
					this.remove();
				}
			}

		});

	}
};

/**
 * 保存当前用户的自定义彩种信息
 */
/*IndexPage.prototype.saveOrUpdateUserCustomLotteryKind = function() {
	for (var i = 0; i < selectedArray.length; i++) {
		if (i == 0) {
			indexPage.param.position1LotteryType = selectedArray[i];
		} else if (i == 1) {
			indexPage.param.position2LotteryType = selectedArray[i];
		} else if (i == 2) {
			indexPage.param.position3LotteryType = selectedArray[i];
		} else if (i == 3) {
			indexPage.param.position4LotteryType = selectedArray[i];
		} else if (i == 4) {
			indexPage.param.position5LotteryType = selectedArray[i];
		} else if (i == 5) {
			indexPage.param.position6LotteryType = selectedArray[i];
		}
	}
	if (selectedArray.length == 0) {
		indexPage.param.position1LotteryType = null;
		indexPage.param.position2LotteryType = null;
		indexPage.param.position3LotteryType = null;
		indexPage.param.position4LotteryType = null;
		indexPage.param.position5LotteryType = null;
		indexPage.param.position6LotteryType = null;
	} else if (selectedArray.length == 1) {
		indexPage.param.position2LotteryType = null;
		indexPage.param.position3LotteryType = null;
		indexPage.param.position4LotteryType = null;
		indexPage.param.position5LotteryType = null;
		indexPage.param.position6LotteryType = null;
	} else if (selectedArray.length == 2) {
		indexPage.param.position3LotteryType = null;
		indexPage.param.position4LotteryType = null;
		indexPage.param.position5LotteryType = null;
		indexPage.param.position6LotteryType = null;
	} else if (selectedArray.length == 3) {
		indexPage.param.position4LotteryType = null;
		indexPage.param.position5LotteryType = null;
		indexPage.param.position6LotteryType = null;
	} else if (selectedArray.length == 4) {
		indexPage.param.position5LotteryType = null;
		indexPage.param.position6LotteryType = null;
	} else if (selectedArray.length == 5) {
		indexPage.param.position6LotteryType = null;
	}
	frontCommonAction.saveOrUpdateUserCustomLotteryKind(indexPage.param, function(r) {
		if (r[0] != null && r[0] == "ok") {
			// 获取当前用户的自定义彩种信息
			indexPage.loadCurrentUserLotteryKind();
			// 保存成功后跳到主页
			indexPage.backToIndex();
		} else if (r[0] != null && r[0] == "error") {
			showTip(r[1]);
			indexPage.backToIndex();
		} else {
			showTip("保存用户自定义彩种的请求失败.");
			indexPage.backToIndex();
		}
	});
};*/

/**
 * 获取当前用户的自定义彩种信息
 */
IndexPage.prototype.loadCurrentUserLotteryKind = function() {
	
	$.ajax({
		type : "POST",
		url : contextPath + "/system/getUserCustomLotteryKind",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if( result.code == "ok"){
				var resultData = result.data;
				selectedArray.length = 0;
				if (resultData.position1LotteryType != null && resultData.position1LotteryType != "") {
					indexPage.voluationtTitleLotteryKind(resultData.position1LotteryType, 0);
					selectedArray.push(resultData.position1LotteryType);
				}
				if (resultData.position2LotteryType != null && resultData.position2LotteryType != "") {
					indexPage.voluationtTitleLotteryKind(resultData.position2LotteryType, 1);
					selectedArray.push(resultData.position2LotteryType);
				}
				if (resultData.position3LotteryType != null && resultData.position3LotteryType != "") {
					indexPage.voluationtTitleLotteryKind(resultData.position3LotteryType, 2);
					selectedArray.push(resultData.position3LotteryType);
				}
				if (resultData.position4LotteryType != null && resultData.position4LotteryType != "") {
					indexPage.voluationtTitleLotteryKind(resultData.position4LotteryType, 3);
					selectedArray.push(resultData.position4LotteryType);
				}
				if (resultData.position5LotteryType != null && resultData.position5LotteryType != "") {
					indexPage.voluationtTitleLotteryKind(resultData.position5LotteryType, 4);
					selectedArray.push(resultData.position5LotteryType);
				}

				indexPage.updateTopSelected();
			}else if(result.code == "error"){
				showTip(result.data);
			}
		}
	});
};

/**
 * 根据彩种获取对应的图片
 */
IndexPage.prototype.getPicsrcByLotteryKind = function(currentLotteryKind) {
	var picSrc = "images/lottery_logo/index/";
	if ("JSKS" == currentLotteryKind || "HBKS" == currentLotteryKind || "AHKS" == currentLotteryKind || "JLKS" == currentLotteryKind || "BJKS" == currentLotteryKind
			|| "JYKS" == currentLotteryKind || "GXKS" == currentLotteryKind) {
		picSrc += "logo_kuaisan.png";
	} else if ("CQSSC" == currentLotteryKind || "TJSSC" == currentLotteryKind || "XJSSC" == currentLotteryKind || "JXSSC" == currentLotteryKind) {
		picSrc += "logo_shishicai.png";
	} else if ("FJSYXW" == currentLotteryKind || "SDSYXW" == currentLotteryKind || "GDSYXW" == currentLotteryKind || "JXSYXW" == currentLotteryKind) {
		picSrc += "logo_shiyixuanwu.png";
	} else if ("JLFFC" == currentLotteryKind) {
		picSrc += "logo_fenfencai.png";
	} else if ("BJPK10" == currentLotteryKind) {
		picSrc += "logo_pk10.png";
	} else if ("XYEB" == currentLotteryKind) {
		picSrc += "logo_xyeb.png";
	} else if ("XJPLFC" == currentLotteryKind) {
		picSrc += "logo_erfencai.png";
	} else if ("TWWFC" == currentLotteryKind) {
		picSrc += "logo_wufencai.png";
	} else if ("HGFFC" == currentLotteryKind) {
		picSrc += "logo_yidianwufencai.png";
	} else if ("FC3D" == currentLotteryKind) {
		picSrc += "logo_dipincai.png";
	} else {
		// 使用默认的添加图片
		picSrc = "";
	}
	return picSrc;
};

/**
 * 给首页标题自定义彩种赋值
 */
IndexPage.prototype.voluationtTitleLotteryKind = function(currentLotteryKind, index) {
	if (index == 0) {
		if (!$.isEmptyObject(currentUser)) {
			$("#userKindId a img").next().text("");
			$("#userKindId a").attr("href", "javascript:showTip(\"亲! 此处请收藏彩种再点击...\")");
			$("#userKindId a").attr("data-val", "");
			$("#userKindId a img").attr("src", "");
		}
	}

	if ("JSKS" == currentLotteryKind) {
		$("#userKindId a img:eq(" + index + ")").next().text("江苏");
		$("#userKindId a img:eq(" + index + ")").attr("src", indexPage.getPicsrcByLotteryKind(currentLotteryKind));
	} else if ("HBKS" == currentLotteryKind) {
		$("#userKindId a img:eq(" + index + ")").next().text("湖北");
		$("#userKindId a img:eq(" + index + ")").attr("src", indexPage.getPicsrcByLotteryKind(currentLotteryKind));
	} else if ("AHKS" == currentLotteryKind) {
		$("#userKindId a img:eq(" + index + ")").next().text("安徽");
		$("#userKindId a img:eq(" + index + ")").attr("src", indexPage.getPicsrcByLotteryKind(currentLotteryKind));
	} else if ("JLKS" == currentLotteryKind) {
		$("#userKindId a img:eq(" + index + ")").next().text("吉林");
		$("#userKindId a img:eq(" + index + ")").attr("src", indexPage.getPicsrcByLotteryKind(currentLotteryKind));
	} else if ("JYKS" == currentLotteryKind) {
		$("#userKindId a img:eq(" + index + ")").next().text("幸运");
		$("#userKindId a img:eq(" + index + ")").attr("src", indexPage.getPicsrcByLotteryKind(currentLotteryKind));
	} else if ("BJKS" == currentLotteryKind) {
		$("#userKindId a img:eq(" + index + ")").next().text("北京");
		$("#userKindId a img:eq(" + index + ")").attr("src", indexPage.getPicsrcByLotteryKind(currentLotteryKind));
	} else if ("GXKS" == currentLotteryKind) {
		$("#userKindId a img:eq(" + index + ")").next().text("广西");
		$("#userKindId a img:eq(" + index + ")").attr("src", indexPage.getPicsrcByLotteryKind(currentLotteryKind));
	} else if ("CQSSC" == currentLotteryKind) {
		$("#userKindId a img:eq(" + index + ")").next().text("重庆");
		$("#userKindId a img:eq(" + index + ")").attr("src", indexPage.getPicsrcByLotteryKind(currentLotteryKind));
	} else if ("TJSSC" == currentLotteryKind) {
		$("#userKindId a img:eq(" + index + ")").next().text("天津");
		$("#userKindId a img:eq(" + index + ")").attr("src", indexPage.getPicsrcByLotteryKind(currentLotteryKind));
	} else if ("XJSSC" == currentLotteryKind) {
		$("#userKindId a img:eq(" + index + ")").next().text("新疆");
		$("#userKindId a img:eq(" + index + ")").attr("src", indexPage.getPicsrcByLotteryKind(currentLotteryKind));
	} else if ("JXSSC" == currentLotteryKind) {
		$("#userKindId a img:eq(" + index + ")").next().text("江西");
		$("#userKindId a img:eq(" + index + ")").attr("src", indexPage.getPicsrcByLotteryKind(currentLotteryKind));
	} else if ("JLFFC" == currentLotteryKind) {
		$("#userKindId a img:eq(" + index + ")").next().text("幸运");
		$("#userKindId a img:eq(" + index + ")").attr("src", indexPage.getPicsrcByLotteryKind(currentLotteryKind));
	} else if ("BJPK10" == currentLotteryKind) {
		$("#userKindId a img:eq(" + index + ")").next().text("北京");
		$("#userKindId a img:eq(" + index + ")").attr("src", indexPage.getPicsrcByLotteryKind(currentLotteryKind));
	} else if ("XYEB" == currentLotteryKind) {
		$("#userKindId a img:eq(" + index + ")").next().text("幸运28");
		$("#userKindId a img:eq(" + index + ")").attr("src", indexPage.getPicsrcByLotteryKind(currentLotteryKind));
	} else if ("XJPLFC" == currentLotteryKind) {
		$("#userKindId a img:eq(" + index + ")").next().text("新加坡");
		$("#userKindId a img:eq(" + index + ")").attr("src", indexPage.getPicsrcByLotteryKind(currentLotteryKind));
	} else if ("TWWFC" == currentLotteryKind) {
		$("#userKindId a img:eq(" + index + ")").next().text("台湾");
		$("#userKindId a img:eq(" + index + ")").attr("src", indexPage.getPicsrcByLotteryKind(currentLotteryKind));
	} else if ("FC3D" == currentLotteryKind) {
		$("#userKindId a img:eq(" + index + ")").next().text("福彩");
		$("#userKindId a img:eq(" + index + ")").attr("src", indexPage.getPicsrcByLotteryKind(currentLotteryKind));
	} else if ("HGFFC" == currentLotteryKind) {
		$("#userKindId a img:eq(" + index + ")").next().text("韩国");
		$("#userKindId a img:eq(" + index + ")").attr("src", indexPage.getPicsrcByLotteryKind(currentLotteryKind));
	} else if ("FJSYXW" == currentLotteryKind) {
		$("#userKindId a img:eq(" + index + ")").next().text("福建");
		$("#userKindId a img:eq(" + index + ")").attr("src", indexPage.getPicsrcByLotteryKind(currentLotteryKind));
	} else if ("JXSYXW" == currentLotteryKind) {
		$("#userKindId a img:eq(" + index + ")").next().text("江西");
		$("#userKindId a img:eq(" + index + ")").attr("src", indexPage.getPicsrcByLotteryKind(currentLotteryKind));
	} else if ("GDSYXW" == currentLotteryKind) {
		$("#userKindId a img:eq(" + index + ")").next().text("广东");
		$("#userKindId a img:eq(" + index + ")").attr("src", indexPage.getPicsrcByLotteryKind(currentLotteryKind));
	} else if ("SDSYXW" == currentLotteryKind) {
		$("#userKindId a img:eq(" + index + ")").next().text("山东");
		$("#userKindId a img:eq(" + index + ")").attr("src", indexPage.getPicsrcByLotteryKind(currentLotteryKind));
	}

	if (currentLotteryKind != null && typeof (currentLotteryKind) != "undefined") {
		var lotteryUrl = "#/lottery/" + currentLotteryKind.toString().toLowerCase();
		if ($.isEmptyObject(currentUser)) {
			$("#userKindId a:eq(" + index + ")").attr("onclick", "javascript:judgeCurrentUserToLogin('" + lotteryUrl + "','去投注用户必须要先登录')");
		} else {
			$("#userKindId a:eq(" + index + ")").attr("href", lotteryUrl);
			$("#userKindId a:eq(" + index + ")").attr("data-val", currentLotteryKind);
		}
	}
	if (currentLotteryKind != "JSKS" && currentLotteryKind != "AHKS" && currentLotteryKind != "JYKS" && currentLotteryKind != "BJKS" && currentLotteryKind != "GXKS"
			&& currentLotteryKind != "CQSSC" && currentLotteryKind != "XJSSC" && currentLotteryKind != "HBKS" && currentLotteryKind != "TJSSC" && currentLotteryKind != "BJPK10"
			&& currentLotteryKind != "JLFFC" && currentLotteryKind != "FCSDDPC") {
		$("#userKindId a:eq(" + index + ")").attr("href", "javascript:showTip(\"本彩种正在开发中，尽情期待...\")");
	}

};

/**
 * 展示首页图片
 */
IndexPage.prototype.getHomeActivityImage = function() {
	// 使用Ajax请求数据
	$.ajax({
		type : "POST",
		url : contextPath + "/index/getHomeImage",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code != null && result.code == "ok") {
				var homeActivityImageObj = $("#homeActivityImage");
				homeActivityImageObj.html("");
				var homePicConfigs = result.data;
				var str = "";
				for (var i = 0; i < homePicConfigs.length; i++) {
					var homePicConfig = homePicConfigs[i];
					if (typeof (homePicConfig.picLinkUrl) != "undifined" && homePicConfig.picLinkUrl != null) {
						var reg = /^(http|https):\/\//;
						var picUrl = homePicConfig.picLinkUrl;
						if (!reg.test(picUrl)) {
							str += '<a href="' + contextPath + "/" + picUrl + '" >';
						} else {
							str += '<a href="' + homePicConfig.picLinkUrl + '">';
						}

					} else {
						str += '<a href="javascript:void(0)">';
					}
					str += '	<div class="banner-content" style="background-image:url(' + homePicConfig.picUrl + ');"></div>';
					homeActivityImageObj.append(str);
					str = "";
				}

				banner.init({
					"element_move" : ".banner",
					"element" : ".banner .banner-content",
					"position_style" : "loop",
					"autoplay_time" : 4000,
					"autoplay" : true,
					"touch" : true,
					"touch_slider" : $(".banner")[0]
				});
			} else if (result.code != null && result.code == "error") {
				showTip(result.data);
			} else {
				showErrorDlg(jQuery(document.body), "获取网站图片的请求失败.");
			}
		}
	});
};

/**
 * 获取用户信息
 */
IndexPage.prototype.getCurrentUserMsg = function() {
	$.ajax({
		type : "POST",
		url : contextPath + "/user/getCurrentUser",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code != null && result.code == "ok") {
				currentUser = result.data;
				$("#indexUserName").text(result.data.userName);
				$("#userInfNameId").text(result.data.userName);
			} else if (result.code != null && result.code == "error") {
				showTip(result.data);
			}
		}
	})
};

/**
 * 加载头部消息数据
 */
IndexPage.prototype.getHeadNoReadMsg = function() {
	$.ajax({
		type : "POST",
		url : contextPath + "/note/getNoteToMeAndSystem",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code != null && result.code == "ok") {
				if ((result.data + result.data2) > 0) {
					$("#msgcount2").text(result.data + result.data2);
					$("#msgcount2").addClass("color-yellow");
				}
			} else if (result.code != null && result.code == "error") {
				showTip(result.data);
			} else {
				showTip(jQuery(document.body), "查找用户的收件记录条数失败.");
			}
		}
	});

};


/**
 * 返回主页
 */
IndexPage.prototype.backToIndex = function() {
	modal_toggle('.custom-modal');
};

/**
 * 加载所有彩种的奖金映射
 */
IndexPage.prototype.loadAllLotteryWins = function() {
	// cqsscPage.getLotteryWins();
	// tjsscPage.getLotteryWins();
	// twffcPage.getLotteryWins();
	// jsksPage.getLotteryWins();
	// ahksPage.getLotteryWins();
	// bjpk10Page.getLotteryWins();
};

// 六合彩参数
indexPage.shengxiao = {
	shengxiaoKey : new Array('鼠', '牛', '虎', '兔', '龙', '蛇', '马', '羊', '猴', '鸡', '狗', '猪'),
	shengxiaoArr : []
}
// 波色
indexPage.boshe = {
	hongbo : [ 1, 2, 7, 8, 12, 13, 18, 19, 23, 24, 29, 30, 34, 35, 40, 45, 46 ],
	lanbo : [ 3, 4, 9, 10, 14, 15, 20, 25, 26, 31, 36, 37, 41, 42, 47, 48 ],
	lvbo : [ 5, 6, 11, 16, 17, 21, 22, 27, 28, 32, 33, 38, 39, 43, 44, 49 ]
}

IndexPage.prototype.returnAninal = function(opencode) {
	var shengxiaoArr = indexPage.shengxiao.shengxiaoArr;
	for (var i = 0; i < shengxiaoArr.length; i++) {
		for (var j = 0; j < shengxiaoArr[i].length; j++) {
			if (Number(opencode) == Number(shengxiaoArr[i][j])) {
				return indexPage.shengxiao.shengxiaoKey[i];
			}
		}
	}
	return "-";
}

/**
 * 初始化生肖号码描述
 */
IndexPage.prototype.initShengXiaoNumber = function() {
	$.ajax({
		type : "POST",
		url : contextPath + "/code/getShengXiaoNumber",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if( result.code == "ok"){
				indexPage.shengxiao.shengxiaoArr = result.data;
				// 获取最新的开奖号码
				indexPage.getLastLotteryCode();
			}else if(result.code == "error"){
				frontCommonPage.showKindlyReminder(result.data);
				$("#editUserButton").removeAttr("disabled");
			}
		}
	});
}

// 计算颜色
IndexPage.prototype.changeColorByCode = function(lotteryCode) {
	var colors = new Array();
	var hong = indexPage.boshe.hongbo;
	var lan = indexPage.boshe.lanbo;
	var lv = indexPage.boshe.lvbo;

	// 判断是否是红波
	for (var i = 0; i < hong.length; i++) {
		if (Number(hong[i]) == Number(lotteryCode.numInfo1)) {
			colors[0] = 'red';
		}
		if (Number(hong[i]) == Number(lotteryCode.numInfo2)) {
			colors[1] = 'red';
		}
		if (Number(hong[i]) == Number(lotteryCode.numInfo3)) {
			colors[2] = 'red';
		}
		if (Number(hong[i]) == Number(lotteryCode.numInfo4)) {
			colors[3] = 'red';
		}
		if (Number(hong[i]) == Number(lotteryCode.numInfo5)) {
			colors[4] = 'red';
		}
		if (Number(hong[i]) == Number(lotteryCode.numInfo6)) {
			colors[5] = 'red';
		}
		if (Number(hong[i]) == Number(lotteryCode.numInfo7)) {
			colors[6] = 'red';
		}
	}

	// 判断是否是蓝波
	for (var i = 0; i < lan.length; i++) {
		if (Number(lan[i]) == Number(lotteryCode.numInfo1)) {
			colors[0] = 'blue';
		}
		if (Number(lan[i]) == Number(lotteryCode.numInfo2)) {
			colors[1] = 'blue';
		}
		if (Number(lan[i]) == Number(lotteryCode.numInfo3)) {
			colors[2] = 'blue';
		}
		if (Number(lan[i]) == Number(lotteryCode.numInfo4)) {
			colors[3] = 'blue';
		}
		if (Number(lan[i]) == Number(lotteryCode.numInfo5)) {
			colors[4] = 'blue';
		}
		if (Number(lan[i]) == Number(lotteryCode.numInfo6)) {
			colors[5] = 'blue';
		}
		if (Number(lan[i]) == Number(lotteryCode.numInfo7)) {
			colors[6] = 'blue';
		}
	}

	// 判断是否是绿波
	for (var i = 0; i < lv.length; i++) {
		if (Number(lv[i]) == Number(lotteryCode.numInfo1)) {
			colors[0] = 'green';
		}
		if (Number(lv[i]) == Number(lotteryCode.numInfo2)) {
			colors[1] = 'green';
		}
		if (Number(lv[i]) == Number(lotteryCode.numInfo3)) {
			colors[2] = 'green';
		}
		if (Number(lv[i]) == Number(lotteryCode.numInfo4)) {
			colors[3] = 'green';
		}
		if (Number(lv[i]) == Number(lotteryCode.numInfo5)) {
			colors[4] = 'green';
		}
		if (Number(lv[i]) == Number(lotteryCode.numInfo6)) {
			colors[5] = 'green';
		}
		if (Number(lv[i]) == Number(lotteryCode.numInfo7)) {
			colors[6] = 'green';
		}
	}

	return colors;
}
