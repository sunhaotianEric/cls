/*
*	by dong
*	http://www.dydong.com 
*/
function change(){
	
	var o={};
	return o={
		data:{
			index:0, //当前序号，默认为0
			index_last:0, //上一个序号，默认为0
			index_pre:0,	//当前序号的上一个序号
			index_next:0,	//当前序号的下一个序号
			
			mun:0, //数量总数
			todo_bool:false, //是否正在切换
			todo_callback:null,	//切换时回调函数
			
			autoplay:false, //是否开启自动切换
			autoplay_time:3000, //自动切换时间差
			autoplay_interval:"", //自动切换计时器
			autoplay_settimeout:null,//暂停计时器
			autoplay_settimeout_time:3000,//暂停时间

			btn_function:"",//切换事件方法
			btn_parent:"", //切换按钮父类对象
			btn_select:"on", //切换按钮选中按钮的CLASS
			btn_html:'<div class="child"></div>', //切换按钮HTML
			
			element_move:"", //移动的父类框
			element_parent:"", //最大父类框
			element:"", //最小的对象
			
			position_style:"loop", //loop 循环
			position_width:null, //定位的宽度
			position_callback:null, //定位时回调函数
			

			touch:false,	//是否开启手机触屏模式
			touch_slider:null,	//切换对象
			touch_start:{x:0,y:0,time:0,bool:false},	//起始点
			touch_start_callback:null,	//触屏前回调函数
			touch_move_callback:null,	//触屏移动回调函数
			touch_end:{x:0,y:0,time:0,bool:false},	//结束点
			touch_end_callback:null,	//触屏结束回调函数
			touch_skew:{x:0,y:0,time:0,bool:false},	//偏移参数


		},
		init:function(data){
			//赛选初始化数据
			$.extend(o.data, data);
			//确定数量
			if(!this.data["mun"])this.data["mun"]=$(this.data["element"]).length-1;
			//确定宽度
			if(!this.data["position_width"])this.data["position_width"]=$(this.data["element"]).width();
			//判读是否启动切换
			if(this.data["autoplay"])this.autoplay();
			//定点
			this.position();
			//判断是否开启移动端触屏切换
			if(this.data["touch"]&&!!(('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch)){
				//若没有指定对象,这绑定element_parent作为对象
				if(!this.data["touch_slider"])this.data["touch_slider"]=$(this.data["element_parent"])[0];
				this.data["touch_slider"].addEventListener('touchstart',this,false);
			}
			//获取上一条下一条序号
			this.data["index_pre"]=this.data["index"]==0?this.data["mun"]:this.data["index"]-1;
			this.data["index_next"]=this.data["index"]==this.data["mun"]?0:this.data["index"]+1;
			
		},
		//重置
		reinit:function(){
			this.remove();
			this.init(this.data);
		},
		//清除切换
		remove:function(){
			$(this.data["element_parent"]).css({"position":"relative","height":"auto"});
			$(this.data["element_move"]).css({"position":"relative","height":"auto"});
			$(this.data["element"]).css({"position":"relative","height":"auto","left":"inherit","right":"inherit","top":"inherit","bottom":"inherit"}).show();
			clearInterval(this.data["autoplay_interval"]);
			clearTimeout(this.data["autoplay_settimeout"]);
			this.data["todo_bool"]=false;
		},
		//定点
		position:function(){
			//移动内容定点
			if(this.data["position_style"]=="loop"){
				var height=$(this.data["element"]+":eq("+this.data["index"]+")").height();
				$(this.data["element_move"]).css({"height":height+"px"});
				$(this.data["element"]).css({"left":-this.data["position_width"]+"px"});
				$(this.data["element"]+":eq("+this.data["index"]+")").css({"left":"0px"});
			}
			//扩展回调函数
			if(this.data.position_callback)this.data.position_callback();
			//按钮定位
			if(this.data["btn_function"]){this.btn_position();}
		},
		//按钮初始化
		btn_position:function(){
			for(var i=0;i<=this.data["mun"];i++){
				$(this.data["btn_parent"]).append(this.data["btn_html"]);
				$(this.data["btn_parent"]).children().eq(i).attr("onclick",this.data["btn_function"]+".todo({'index':"+i+"})");
			}
			$(this.data["btn_parent"]).children().eq(this.data["index"]).addClass(this.data["btn_select"]);
		},
		//按钮切换
		btn_todo:function(data){
			var index=!isNaN(data["index"])?data["index"]:this.data["index"];
			$(this.data["btn_parent"]).children().removeClass(this.data["btn_select"]);
			$(this.data["btn_parent"]).children().eq(index).addClass(this.data["btn_select"]);
		},
		//重置定时器
		autoplay:function(){
			clearInterval(this.data["autoplay_interval"]);
			clearTimeout(this.data["autoplay_settimeout"]);
			this.data["autoplay_settimeout"]=setTimeout(function(o){
				o.data["autoplay_interval"]=setInterval(function(o){o.todo(o);},o.data["autoplay_time"],o);
			},this.data["autoplay_settimeout_time"],this);
		},
		//切换
		todo:function(data){
			//如果序号相似则不执行
			if(!isNaN(data["index"])&&this.data["index"]==data["index"])return false;
			//判断是否传入配置
			if(data["data"])this.data=data["data"];
			//判断是否正在切换
			if(this.data["todo_bool"]){return false;}else{this.data["todo_bool"]=true;}
			//设置上一个切换的序号
			if((!isNaN(data["index"])||data["direc"])&&this.data["autoplay"])this.autoplay();
			
			this.data["index_last"]=this.data["index"];
		
			//判断当前序号
			if(!isNaN(data["index"])){this.data["index"]=data["index"]}else{
				if(this.data["autoplay"])this.autoplay();
				if(data["direc"]=="-")
					this.data["index"]==0?this.data["index"]=this.data["mun"]:this.data["index"]--;
				else
					this.data["index"]==this.data["mun"]?this.data["index"]=0:this.data["index"]++;
			}
			//获取上一条下一条序号
			this.data["index_pre"]=this.data["index"]==0?this.data["mun"]:this.data["index"]-1;
			this.data["index_next"]=this.data["index"]==this.data["mun"]?0:this.data["index"]+1;
			//执行切换
			if(this.data["position_style"]=="loop"){
				//判断切换的方向
				var direc=data["direc"]?-1*parseInt(data["direc"]+"1"):-1;
				//判断是否通过序号切换
				if(!isNaN(data["index"]))direc=data["index"]-this.data["index_last"]<0?1:-1;
				var height=$(this.data["element"]+":eq("+this.data["index"]+")").height();
				$(this.data["element_move"]).css({"height":height+"px"});
				//若不是手机拖动切换或者按序号切换着执行定位
				if(isNaN(this.data["loop_bool"])||!isNaN(data["index"]))
					$(this.data["element"]+":eq("+this.data["index"]+")").css({"left":-direc*this.data["position_width"]+"px"});

				$(this.data["element"]+":eq("+this.data["index_last"]+")").stop(false,true).animate({"left":direc*this.data["position_width"]+"px"},500);
				$(this.data["element"]+":eq("+this.data["index"]+")").stop(false,true).animate({"left":"0px"},500,function(){o.data["todo_bool"]=false;});
			}
			//用于扩展的回调函数
			if(this.data.todo_callback)this.data.todo_callback();
			//按钮选中切换
			if(this.data["btn_function"]){ this.btn_todo({});}
			
			return true;
		},

 		handleEvent:function(event){
   			if(event.type == 'touchstart'){this.touch_start(event);}
   			else if(event.type == 'touchmove'){this.touch_move(event);}
   			else if(event.type == 'touchend'){this.touch_end(event);}
		},
		//滑动开始
		touch_start:function(event){
			//touches数组对象获得屏幕上所有的touch，取第一个touch
			var touch = event.targetTouches[0];
			//取第一个touch的坐标值
			this.data['touch_start'] = {x:touch.pageX,y:touch.pageY,time:+new Date,bool:true};
			this.data['touch_skew'] = {x:0,y:0,time:+new Date,bool:false};
			//这个参数判断是垂直滚动还是水平滚动		
			isScrolling = 0;
			//触屏滑动开始时执行方法
			if(this.data["position_style"]=="loop"){
				this.data["loop_bool"]=false;
			}
			//用于扩展的回调方法
			if(this.touch_start_callback)this.data.touch_start_callback();

			this.data["touch_slider"].addEventListener('touchmove',this,false);
			this.data["touch_slider"].addEventListener('touchend',this,false);
		},
		//移动
		touch_move:function(event){
			//当屏幕有多个touch或者页面被缩放过，就不执行move操作
			if(event.targetTouches.length > 1 || event.scale && event.scale !== 1) return;
			var touch = event.targetTouches[0];
			this.data['touch_end'] = {x:touch.pageX,y:touch.pageY,time:+new Date,bool:false};
			this.data['touch_skew'] = {x:touch.pageX - this.data['touch_start'].x,y:touch.pageY - this.data['touch_start'].y,time:+new Date,bool:true};
			//isScrolling为1时，表示纵向滑动，0为横向滑动
			isScrolling = Math.abs(this.data['touch_end'].x) < Math.abs(this.data['touch_end'].y) ? 1:0;
			if(isScrolling === 1){
			//阻止触摸事件的默认行为，即阻止滚屏
			//event.preventDefault();

			}else{
				event.preventDefault();
				//触屏滑动时执行方法
				if(this.data["position_style"]=="loop"){
					var index_side=this.data['touch_skew'].x > 0?this.data["index_pre"]:this.data["index_next"];
					var	direc=this.data['touch_skew'].x > 0?"+":"-";
					var direckey=parseInt(direc+"1")*-1;
					if(!this.data["loop_bool"]){
						$(this.data["element"]+":eq("+index_side+")").css({"left":direckey*this.data['position_width']+"px"});
						this.data["loop_bool"]=true;
					}
					var skew=direc+"="+Math.abs(this.data['touch_skew'].x)*0.1+"px"
					$(this.data["element"]+":eq("+index_side+")").css({"left":skew});
					$(this.data["element"]+":eq("+this.data["index"]+")").css({"left":skew});
				}
				//用于扩展的回调方法
				if(this.touch_move_callback)this.data.touch_move_callback();
			}
		},
		//滑动释放
		touch_end:function(event){
			//滑动的持续时间
			var duration = +new Date - this.data['touch_start'].time;
			var i = 0;
			if(Number(duration) > 0){
			if(isScrolling === 1){
				//判断是上移还是下移，当偏移量大于10时执行
				if(this.data['touch_skew'].y < 0){i = 1;}
				else if(this.data['touch_skew'].y > 0){i = 3;}
				//触屏滑动结束时执行方法
				if(this.data["position_style"]=="loop")this.data["loop_bool"]=false;
				//用于扩展的回调方法
		   		if(this.touch_end_callback)this.data.touch_end_callback();
			}else if(isScrolling === 0){
				//判断是左移还是右移，当偏移量大于10时执行
				if(this.data['touch_skew'].x > 0){i = 2;}else if(this.data['touch_skew'].x < 0){i = 4;}
			}
			switch(i){
				//case 1:o.scrollFunc({wheelDelta:-1});break;
				case 2:this.todo({"direc":"-"});break;
				//case 2:alert(123);break;
				//case 3:o.scrollFunc({wheelDelta:1});break;
				case 4:this.todo({"direc":"+"});break;
				default:break;
		   	};
		   	
			this.data['touch_start'] = this.data['touch_end'] = this.data['touch_skew'] = null;
			return false;
			}
   			
			//解绑事件
			this.data["touch_slider"].removeEventListener('touchmove',this,false);
			this.data["touch_slider"].removeEventListener('touchend',this,false);
		}

	}
	
}