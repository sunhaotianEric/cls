function BindPhonePage() {

}

var bindPhonePage = new BindPhonePage();


/**
 * 初始化页面
 */
$(document).ready(function() {
	bindPhonePage.checkIsSetSafePassword();
	// 手机安全验证-找回密码
	$('#btn-sendnum').on('click', function() {
		bindPhonePage.sendTextMessages();
	})
});

/**
 * 判断用户是否设置安全密码.
 */
BindPhonePage.prototype.checkIsSetSafePassword = function() {
	$.ajax({
		type : "POST",
		url : contextPath + "/usersafe/checkIsSetSafePassword",
		contentType : 'application/json;charset=utf-8',// 设置请求头信息;
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				return;
			} else if (result.code == "error") {
				showTip("请先设置安全密码...三秒后离开此页面");
				setTimeout("window.location.href= contextPath + '/gameBet/setting.html'", 3000);
			}
		}
	})
};

/**
 * 发送短信验证码
 */
BindPhonePage.prototype.sendTextMessages = function() {

	// 获取前端参数,并做非空校验.
	var mobile = $("#mobile").val();
	var type = "BIND_PHONE_TYPE";
	if (mobile == null || mobile == "") {
		showTip("手机号不能空");
		return;
	}

	// 封装参数
	var jsonData = {
		"phone" : mobile,
		"type" : type,
	};

	// 发送验证码.
	$.ajax({
		type : "POST",
		url : contextPath + "/usersafe/sendPhoneCode",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				showTip("短信已发送，请注意查收");
				$(this).hide();
				$('.mobile-verify-time').show();
				$('#mobile_verifyCode_field').show();

				// 手机验证码倒计时60秒
				var i = 60;
				var interval = setInterval(function() {
					i = i - 1;
					$('.mobile-verify-time .countdown').text(i);
					if (i == 0) {
						clearInterval(interval);
						$('.mobile-verify-time').hide();
						$('#btn-sendnum').show();
					}
				}, 1000)
			} else if (result.code == "error") {
				showTip(result.data);
				return;
			}
		}
	});
}

/**
 * 绑定新手机.(校验手机号是否被使用,校验安全密码是否正确,校验手机号和验证码是否正确).
 */
BindPhonePage.prototype.bindPhone = function() {

	// 获取前端参数,并且判断是否为空.
	var mobile = $("#mobile").val();
	if (mobile == null || mobile == "") {
		showTip("手机号不能为空");
		return;
	}
	
	var vcode = $("#mobile_verifyCode").val();
	if (vcode == null || vcode == "") {
		showTip("验证码不能为空");
		return;
	}
	
	var safepasw = $("#safepasw").val();
	if (safepasw == null || safepasw == "") {
		showTip("安全密码不能为空");
		return;
	}

	var jsonData = {
		"vcode" : vcode,
		"phone" : mobile,
		"safePassword" : safepasw,
	};

	$.ajax({
		type : "POST",
		url : contextPath + "/usersafe/bindPhone",
		data : JSON.stringify(jsonData),// 将对象序列化成JSON字符串;
		dataType : "json",// 设置请求参数类型
		contentType : 'application/json;charset=utf-8',// 设置请求头信息;
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				showTip("绑定成功，2秒后自动返回设置首页...");
				setTimeout(function() {
					window.location.href = contextPath + "/gameBet/setting.html";
				}, 1500);
			} else if (result.code == "error") {
				showTip(result.data);
			}
		}
	});

}
