function SetSafePwdPage() {
	
}

var setSafePwdPage= new SetSafePwdPage();

setSafePwdPage.param = {

};

/**
 * 初始化页面
 */
$(document).ready(function() {
	$(".i-person").parent().addClass("on");
	$("#newPassword").blur(function() {
		var userPassword = $("#newPassword").val();
		if (userPassword == null || userPassword == "" || userPassword.length < 6 || userPassword.length > 15) {
			showTip("安全密码填写不合法.");
		}
	});

	$("#confirmNewPassword").blur(function() {
		var userPassword = $("#newPassword").val();
		var userPassword2 = $("#confirmNewPassword").val();
		if (userPassword2 == null || userPassword2 == "" || userPassword2.length < 6 || userPassword2.length > 15) {
			showTip("确认密码不合法.");
		} else if (userPassword != userPassword2) {
			showTip("两次密码输入不一致.");
		}
	});
	setSafePwdPage.saveClick();

});

/**
 * 保存安全密码.
 */
SetSafePwdPage.prototype.saveClick = function() {
	$("#savePassBtn").click(function() {
		setSafePwdPage.saveInfoSavePassword();
	});
}

/**
 * 保存安全密码数据.
 */
SetSafePwdPage.prototype.saveInfoSavePassword = function() {
	

	var userPassword = $("#newPassword").val();
	var userPassword2 = $("#confirmNewPassword").val();

	if (userPassword == null || userPassword == "" || userPassword.length < 6 || userPassword.length > 15) {
		showTip("新安全密码填写不合法.");
		return;
	}

	if (userPassword2 == null || userPassword2 == "") {
		showTip("确认密码不能为空.");
		return;
	}

	if (userPassword != userPassword2) {
		showTip("两次密码输入不一致.");
		return;
	}
	$("#savePassBtn").text("提交中.....");
	$("#savePassBtn").unbind("click");
	var user = {};
	user.safePassword = userPassword;
	user.sureSafePassword = userPassword2;
	var jsonData = {
		"user" : user,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/usersafe/setSafePwd",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				showTip("设置成功...3秒后跳转页面");
				$("#savePassBtn").html("确定");
				setTimeout("window.location.href= contextPath +'/gameBet/setting.html'", 3000);
			} else if (result.code == "error") {
				showTip(result.data);
				$("#savePassBtn").html("确定");
				setSafePwdPage.saveClick();
			}
		}
	});
};
