function ResetPwdPage() {
}

var resetPwdPage= new ResetPwdPage();

resetPwdPage.param = {

};

/**
 * 初始化页面
 */
$(document).ready(function() {
	$(".i-person").parent().addClass("on");
	animate_add(".user_info_loginPasswordCtrl .main", "fadeInUp");

	$("#oldPassword").blur(function() {
		var userPasswordOld = $("#oldPassword").val();
		if (userPasswordOld == null || userPasswordOld == "" || userPasswordOld.length < 6 || userPasswordOld.length > 15) {
			showTip("密码格式不正确");
		}
	});

	$("#newPassword").blur(function() {
		var userPassword = $("#newPassword").val();
		if (userPassword == null || userPassword == "" || userPassword.length < 6 || userPassword.length > 15) {
			showTip("新密码填写不合法.");
		}
	});

	$("#confirmNewPassword").blur(function() {
		var userPassword = $("#newPassword").val();
		var userPassword2 = $("#confirmNewPassword").val();
		if (userPassword2 == null || userPassword2 == "" || userPassword2.length < 6 || userPassword2.length > 15) {
			showTip("确认密码不合法.");
		} else if (userPassword != userPassword2) {
			showTip("两次密码输入不一致.");
		}
	});

	$("#updateLoginPass").click(function() {
		resetPwdPage.saveInfoPassword();
	});

});

/**
 * 保存登录密码数据
 */
ResetPwdPage.prototype.saveInfoPassword = function() {

	var userPasswordOld = $("#oldPassword").val();
	var userPassword = $("#newPassword").val();
	var userPassword2 = $("#confirmNewPassword").val();

	if (userPasswordOld == null || userPasswordOld == "") {
		showTip("当前密码不能为空.");
		return;
	}

	if (userPassword == null || userPassword == "" || userPassword.length < 6 || userPassword.length > 15) {
		showTip("新密码填写不合法.");
		return;
	}

	if (userPassword2 == null || userPassword2 == "") {
		showTip("确认密码不能为空.");
		return;
	}

	if (userPassword != userPassword2) {
		showTip("两次密码输入不一致.");
		return;
	}
	$("#updateLoginPass").text("提交中.....");
	$("#updateLoginPass").unbind("click");
	// 设置参数对象user
	var user = {};
	user.oldPassword = userPasswordOld;
	user.password = userPassword;
	user.surePassword = userPassword2;
	var jsonData = {
		"user" : user,
	}
	// 使用Ajax请求
	$.ajax({
		type : "POST",
		url : contextPath + "/usersafe/resetPwd",
		data : JSON.stringify(jsonData),//将对象序列化成JSON字符串;
		dataType : "json",//设置请求参数类型
		contentType : 'application/json;charset=utf-8',//设置请求头信息;
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				showTip("修改成功，请重新登录..");
				setTimeout("window.location.href= contextPath + '/gameBet/login.html'", 3000);
			} else if (result.code == "error") {
				showTip(result.data);
				$("#updateLoginPass").text("确定");
				$("#updateLoginPass").click(function() {
					resetPwdPage.saveInfoPassword();
				});
			} else {
				showTip("操作失败，请重试");
				$("#updateLoginPass").text("确定");
				$("#updateLoginPass").click(function() {
					resetPwdPage.saveInfoPassword();
				});
			}
		}
	})
};
