function ResetSafePwdPage() {

}

var resetSafePwdPage = new ResetSafePwdPage();

resetSafePwdPage.param = {

};

/**
 * 初始化页面
 */
$(document).ready(function() {
	$(".i-person").parent().addClass("on");
	// 当前链接位置标注选中样式
	$("#user_info_password").addClass("selected");
	/* userLeftPage.selectMoreMenu(); */

	$("#oldPassword").blur(function() {
		var userPasswordOld = $("#oldPassword").val();
		if (userPasswordOld == null || userPasswordOld == "" || userPasswordOld.length < 6 || userPasswordOld.length > 15) {
			showTip("密码格式不正确");
		}
	});

	$("#newPassword").blur(function() {
		var userPassword = $("#newPassword").val();
		if (userPassword == null || userPassword == "" || userPassword.length < 6 || userPassword.length > 15) {
			showTip("新安全密码填写不合法.");
		}
	});

	$("#confirmNewPassword").blur(function() {
		var userPassword = $("#newPassword").val();
		var userPassword2 = $("#confirmNewPassword").val();
		if (userPassword2 == null || userPassword2 == "" || userPassword2.length < 6 || userPassword2.length > 15) {
			showTip("确认密码不合法.");
		} else if (userPassword != userPassword2) {
			showTip("两次密码输入不一致.");
		}
	});

	$("#updateSavePassBtn").click(function() {
		resetSafePwdPage.saveInfoSavePassword();
	});

});

/**
 * 保存安全密码数据
 */
ResetSafePwdPage.prototype.saveInfoSavePassword = function() {

	var userPasswordOld = $("#oldPassword").val();
	var userPassword = $("#newPassword").val();
	var userPassword2 = $("#confirmNewPassword").val();

	if (userPasswordOld == null || userPasswordOld == "") {
		showTip("当前密码不能为空.");
		return;
	}

	if (userPassword == null || userPassword == "" || userPassword.length < 6 || userPassword.length > 15) {
		showTip("新安全密码填写不合法.");
		return;
	}

	if (userPassword2 == null || userPassword2 == "") {
		showTip("确认密码不能为空.");
		return;
	}

	if (userPassword != userPassword2) {
		showTip("两次密码输入不一致.");
		return;
	}
	$("#updateSavePassBtn").text("提交中.....");
	$("#updateSavePassBtn").unbind("click");
	// 封装传递向后端传输user参数
	var user = {};
	user.oldSafePassword = userPasswordOld;
	user.safePassword = userPassword;
	user.sureSafePassword = userPassword2;

	var jsonData = {
		"user" : user,
	};
	$.ajax({
		type : "POST",
		// 请求路径
		url : contextPath + "/usersafe/resetSafePwd",
		// 请求参数
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				showTip("修改成功...3秒后跳转页面");
				$("#updateSavePassBtn").text("确定");
				// 修改成功跳转页面到设置页面
				setTimeout("window.location.href= contextPath + '/gameBet/setting.html'", 3000);
			} else if (result.code == "error") {
				showTip(result.data);
				$("#updateSavePassBtn").click(function() {
					resetSafePwdPage.saveInfoSavePassword();
				});
			} else {
				showTip(jQuery(document.body), "操作失败，请重试");
				$("#updateSavePassBtn").click(function() {
					resetSafePwdPage.saveInfoSavePassword();
				});
			}
		}
	});
};
