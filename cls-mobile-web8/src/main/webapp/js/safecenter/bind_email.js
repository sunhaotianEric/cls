function BindEmailPage() {

}

var bindEmailPage = new BindEmailPage();


/**
 * 初始化页面.
 */
$(document).ready(function() {
	bindEmailPage.checkIsSetSafePassword();
	// 手机安全验证-找回密码
	$('#btn-sendnum').on('click', function() {
		bindEmailPage.sendTextMessages();
		$(this).hide();
		$('.email-verify-time').show();
		$('#email_verifyCode_field').show();
		// 邮箱验证码倒计时60秒
		var i = 60;
		var interval = setInterval(function() {
			i = i - 1;
			$('.email-verify-time .countdown').text(i);
			if (i == 0) {
				clearInterval(interval);
				$('.email-verify-time').hide();
				$('#btn-sendnum').show();
			}
		}, 1000)
	})
});

/**
 * 判断当前用户是否设置安全密码.
 */
BindEmailPage.prototype.checkIsSetSafePassword = function() {
	$.ajax({
		type : "POST",
		url : contextPath + "/usersafe/checkIsSetSafePassword",
		contentType : 'application/json;charset=utf-8',// 设置请求头信息;
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				return;
			} else if (result.code == "error") {
				showTip("请先设置安全密码...三秒后离开此页面");
				setTimeout("window.location.href= contextPath + '/gameBet/setting.html'", 3000);
			}
		}
	});
}

/**
 * 发送邮箱验证码.
 */
BindEmailPage.prototype.sendTextMessages = function() {
	// 获取参数 ,并且判断参数是否为空.
	var email = $("#email").val();
	// 标志.
	var type = "BIND_EMAIL_TYPE";
	if (email == null || email == "") {
		showTip("邮箱地址不能空");
		return;
	}

	// 封装参数.
	var jsonData = {
		"email" : email,
		"type" : type,
	};

	// 发送邮箱验证码.
	$.ajax({
		type : "POST",
		url : contextPath + "/usersafe/sendEmailCode",
		data : JSON.stringify(jsonData),// 将对象(参数)序列化成JSON字符串;
		dataType : "json",// 设置请求参数类型
		contentType : 'application/json;charset=utf-8',// 设置请求头信息;
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				showTip("验证码已发送，请注意查收");
			} else if (result.code == "error") {
				showTip(result.data);
				return;
			}
		}
	});
}

/**
 * 绑定新邮箱(校验邮箱是否唯一,校验安全密码是否正确,校验邮箱和验证码是否匹配,);
 */
BindEmailPage.prototype.bindEmail = function() {

	// 获取参数,并且前端做校验,
	var email = $("#email").val();
	if (email == null || email == "") {
		showTip("邮箱地址不能为空");
		return;
	}
	
	var vcode = $("#email_verifyCode").val();
	if (vcode == null || vcode == "") {
		showTip("验证码不能为空");
		return;
	}
	
	var safepasw = $("#safepasw").val();
	if (safepasw == null || safepasw == "") {
		showTip("安全密码不能为空");
		return;
	}

	// 封装参数.
	var jsonData = {
		"safePassword" : safepasw,
		"vcode" : vcode,
		"email" : email,
	};

	// 绑定新邮箱.
	$.ajax({
		type : "POST",
		url : contextPath + "/usersafe/bindEmail",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				showTip("绑定成功，2秒后自动返回设置首页...");
				setTimeout(function() {
					window.location.href = contextPath + "/gameBet/setting.html";
				}, 2000);
			} else if (result.code == "error") {
				showTip(result.data);
			}
		}
	});
}
