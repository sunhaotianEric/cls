/**
 * 团队帐变列表
 */
function MoneyDetailPage() {
}
var moneyDetailPage = new MoneyDetailPage();

moneyDetailPage.param = {
	userName : null
};

/**
 * 查询参数
 */
moneyDetailPage.queryParam = {
	detailType : null,
	expect : null,
	lotteryType : null,
	createdDateStart : null,
	createdDateEnd : null,
	userName : null,
	lotteryId : null,
	lotteryKind : null,
	lotteryModel : null,
	reportType : null,
	queryScope : 1,
	dateRange :1,
};

// 分页参数
moneyDetailPage.pageParam = {
	queryMethodParam : null,
	pageNo : 1,
	pageMaxNo : 1, // 最大的页数
	skipHtmlStr : ""
};

$(document).ready(function() {
	$(".user-btn").show();
	var defalutDate = new Date();
	var strDate = new Date();
	defalutDate.setDate(1);
	defalutDate.setHours(3, 0, 0, 0);
	moneyDetailPage.queryParam.reportType = 1;
	moneyDetailPage.queryParam.dateRange = 1;
	
	/*moneyDetailPage.queryParam.createdDateStart = strDate.setHours(3, -1, 60, 999);
	moneyDetailPage.queryParam.createdDateEnd = strDate.AddDays(1).setHours(2, 59, 59, 999);*/

	// 点击切换查询盈亏报表事件
	$(".move-content").find("p").click(function() {
		var value = $(this).attr("data-value");
		var todate = new Date();

		if (value == "today") {
			moneyDetailPage.queryParam.dateRange = 1;
			moneyDetailPage.queryParam.reportType = 1;
			/*moneyDetailPage.queryParam.createdDateStart = todate.setHours(3, -1, 60, 999);
			moneyDetailPage.queryParam.createdDateEnd = todate.AddDays(1).setHours(2, 59, 59, 999);*/
			$(".header-right").find("span").html("今天");
		} else if (value == "yesterDay") {
			moneyDetailPage.queryParam.dateRange = -1,
			moneyDetailPage.queryParam.reportType = 0;
			/*moneyDetailPage.queryParam.createdDateEnd = todate.setHours(2, 59, 59, 999);
			moneyDetailPage.queryParam.createdDateStart = todate.AddDays(-1).setHours(3, -1, 60, 999);*/
			$(".header-right").find("span").html("昨天");
		} else if (value == "week") {
			moneyDetailPage.queryParam.dateRange = -7,
			moneyDetailPage.queryParam.reportType = 0;
			/*var defalutDate = new Date();
			defalutDate.setDate(1);
			defalutDate.setHours(3, 0, 0, 0);
			moneyDetailPage.queryParam.createdDateEnd = todate.AddDays(1).setHours(2, 59, 59, 999);
			moneyDetailPage.queryParam.createdDateStart = todate.AddDays(-7).setHours(3, 0, 0, 999);
			$(".header-right").find("span").html("最近七天");*/
		}

		var parent = $(this).closest("div.lottery-classify1");
		parent.hide();
		$(".alert-bg").stop(false, true).fadeToggle(500);
		moneyDetailPage.getAllUserMoneyDetails(1);
	})
	animate_add(".money_detailed_report .main", "fadeInTop");
	moneyDetailPage.pageParam.pageNo = 1;
	moneyDetailPage.getAllUserMoneyDetails(1);

	// 搜索条件-明细类型选择
	$('.lottery-name').on('click', function() {
		$(this).parent().find('.options').toggleClass('open');
	});

	$('.lottery-name .options').on('click', 'li', function() {
		var $select = $(this).parents('.lottery-name');
		$select.find('.text').html($(this).html());
		$(this).toggleClass('open');
		moneyDetailPage.getAllUserMoneyDetails(1);
	});

});

/**
 * 加载所有的账户记录
 */
MoneyDetailPage.prototype.getAllUserMoneyDetails = function(pageNo) {
	moneyDetailPage.pageParam.queryMethodParam = 'getAllUserMoneyDetails';
	var detailType = $(".lottery-name span p").attr("data-value");
	if (detailType != "") {
		moneyDetailPage.queryParam.detailType = detailType;
	} else {
		moneyDetailPage.queryParam.detailType = null;
	}
	moneyDetailPage.pageParam.pageNo = pageNo;
	if (pageNo == 1) {
		$("#userMoneyDetailList").empty();
	}
	moneyDetailPage.queryConditionUserMoneyDetails(moneyDetailPage.queryParam, moneyDetailPage.pageParam.pageNo);
};

/**
 * 条件查询积分记录
 */
MoneyDetailPage.prototype.queryConditionUserMoneyDetails = function(queryParam, pageNo) {
	var query = {
		/*"createdDateStart" : new Date(moneyDetailPage.queryParam.createdDateStart),
		"createdDateEnd" : 	new Date(moneyDetailPage.queryParam.createdDateEnd),*/
		"dateRange" : queryParam.dateRange,
		"detailType" : 	queryParam.detailType,
	};
	var jsonData = {
		"query" : query,
		"pageNo" : moneyDetailPage.pageParam.pageNo,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/moneydetail/moneyDetailQuery",
		contentType : 'application/json;charset=utf-8',
		data : JSON.stringify(jsonData),
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				moneyDetailPage.refreshMoneyDetailPages(result.data);
			} else if (result.code == "error") {
				showTip(result.data);
			}
		}
	});
};

/**
 * 刷新列表数据
 */
MoneyDetailPage.prototype.refreshMoneyDetailPages = function(page) {
	$(".user-btn").html("正在加载中...");
	var userMoneyDetailListObj = $("#userMoneyDetailList");

	var str = "";
	var userMoneyDetails = page.pageContent;
	if (userMoneyDetails.length == 0) {
		userMoneyDetailListObj.html("");
		str += '<div class="line">';
		str += '<h4 class="nodata">暂无数据</h4>';
		str += '</div>';
		userMoneyDetailListObj.append(str);
		$(".user-btn").hide();
	} else {
//		$("#userMoneyDetailList .line").remove();
		// 记录数据显示
		for (var i = 0; i < userMoneyDetails.length; i++) {
			var userMoneyDetail = userMoneyDetails[i];
			var lotteryIdStr = userMoneyDetail.lotteryId;

			str += '<li>';
			str += '    <div class="info clear">';
			str += '        <div class="info-txt fl">';
			str += '            <h2>流水号：' + lotteryIdStr.substring(lotteryIdStr.indexOf('_') + 1, lotteryIdStr.length) + '</h2>';
			str += '            <p>交易时间：' + userMoneyDetail.createDateStr + '</p>';
			str += '        </div>';
			str += '		  <div class="sender font-gray"></div>';
			if (parseFloat(parseFloat(userMoneyDetail.income).toFixed(frontCommonPage.param.fixVaue)) > 0) {
				str += '	  <div class="info-tag fr color-red">' + userMoneyDetail.detailTypeDes + '</div>';
				str += '    </div>';
				str += '    <div class="money clear">';
				str += '        <span>收入（元）<br/><em class="color-red">' + parseFloat(userMoneyDetail.income).toFixed(frontCommonPage.param.fixVaue) + '</em></span>';
				str += '        <span>支出（元）<br/><em>' + parseFloat(userMoneyDetail.pay).toFixed(frontCommonPage.param.fixVaue) + '</em></span>';
			}
			if (parseFloat(parseFloat(userMoneyDetail.pay).toFixed(frontCommonPage.param.fixVaue)) > 0) {
				str += '		  <div class="info-tag fr color-green">' + userMoneyDetail.detailTypeDes + '</div>';
				str += '    </div>';
				str += '    </div>';
				str += '    <div class="money clear">';
				str += '        <span>收入（元）<br/><em>' + parseFloat(userMoneyDetail.income).toFixed(frontCommonPage.param.fixVaue) + '</em></span>';
				str += '        <span>支出（元）<br/><em class="color-green">' + parseFloat(userMoneyDetail.pay).toFixed(frontCommonPage.param.fixVaue) + '</em></span>';
			}
			str += '        <span>余额（元）<br/><em>' + parseFloat(userMoneyDetail.balanceAfter).toFixed(frontCommonPage.param.fixVaue) + '</em></span>';
			str += '    </div>';
			str += '</li>';

			userMoneyDetailListObj.append(str);
			str = "";
		}
	}

	// 如果当前页数小于总页数，显示加载更多
	if (page.pageNo < page.totalPageNum) {
		$(".user-btn").show();
		$(".user-btn").html("加载更多");
		// 添加加载更多事件
		$(".user-btn").unbind("click").click(function() {
			moneyDetailPage.pageParam.pageNo++;
			moneyDetailPage.getAllUserMoneyDetails(moneyDetailPage.pageParam.pageNo);
		});
	}else {
		$(".user-btn").hide();
	}

	if (page.totalPageNum == 1) {
		$(".user-btn").hide();
	}
};
