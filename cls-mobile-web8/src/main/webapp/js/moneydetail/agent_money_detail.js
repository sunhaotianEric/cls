function TeamMoneyDetailPage() {
}
var moneyDetailPage = new TeamMoneyDetailPage();

moneyDetailPage.queryParam = {
		userName : null,
		//lotteryType : null,
		detailType : null,
		createdDateStart : null,
		createdDateEnd : null,
		prostateStatus : null,
		reportType : 0,
		queryScope:3,
		dageRange :1,
};

moneyDetailPage.pageParam = {
        pageSize : 10,  //前台分页每页条数
        pageNo : 1,
        showPage:1,
        total:0       
};

$(document).ready(function() {
	
	// 搜索条件-彩种选择
	$('.lottery-name').on('click', function() {
        $(this).parent().find('.options').toggleClass('open');
    });
    
    $('.lottery-name .options').on('click', 'li', function() {
        var $select = $(this).parents('.lottery-name');
        $select.find('.text').html($(this).html());
        $(this).toggleClass('open');
    });

    var defalutDate = new Date();
    var strDate = new Date();
    defalutDate.setDate(1);
    defalutDate.setHours(3, 0, 0, 0);
    /*moneyDetailPage.queryParam.reportType = 0;
    moneyDetailPage.queryParam.createdDateStart = defalutDate;
    moneyDetailPage.queryParam.createdDateEnd = strDate.setHours(2, 59, 59, 999);*/
    moneyDetailPage.queryParam.reportType = 1;
    moneyDetailPage.queryParam.dageRange = 1;
	/*moneyDetailPage.queryParam.createdDateStart=strDate.setHours(3,-1,60,999);
	moneyDetailPage.queryParam.createdDateEnd=strDate.AddDays(1).setHours(2,59,59,999);*/
	
    //点击切换查询盈亏报表事件
    $(".move-content").find("p").click(function(){
         var value = $(this).attr("data-value");
         var todate = new Date();
 			  
 		if(value=="today"){
 			moneyDetailPage.queryParam.reportType = 1;
 			moneyDetailPage.queryParam.dageRange = 1;
 			/*moneyDetailPage.queryParam.createdDateStart=todate.setHours(3,-1,60,999);
 			moneyDetailPage.queryParam.createdDateEnd=todate.AddDays(1).setHours(2,59,59,999);*/
 			$(".header-right").find("span").html("今天");
 		}else if(value=="yesterDay"){
 			moneyDetailPage.queryParam.reportType = 0;
 			moneyDetailPage.queryParam.dageRange = -1;
 			/*moneyDetailPage.queryParam.createdDateEnd=todate.setHours(2,59,59,999);
 			moneyDetailPage.queryParam.createdDateStart=todate.AddDays(-1).setHours(3,-1,60,999);*/
 			$(".header-right").find("span").html("昨天");
 		}else if(value=="week"){
 			moneyDetailPage.queryParam.reportType=0;
 			moneyDetailPage.queryParam.dageRange = -7;
 			/*var defalutDate=new Date();
 			defalutDate.setDate(1);
 			defalutDate.setHours(3,0,0,0);
 			moneyDetailPage.queryParam.createdDateEnd=todate.AddDays(1).setHours(2,59,59,999);
 			moneyDetailPage.queryParam.createdDateStart=todate.AddDays(-7).setHours(3,0,0,999);*/
 			$(".header-right").find("span").html("最近七天");
 		}
 		     
 		var parent=$(this).closest("div.lottery-classify1");
 		parent.hide();
 		$(".alert-bg").stop(false,true).fadeToggle(500);
 		moneyDetailPage.getUserBetPage(moneyDetailPage.queryParam,1);
    })
    moneyDetailPage.getUserBetPage(moneyDetailPage.queryParam,moneyDetailPage.pageParam.pageNo);
});

TeamMoneyDetailPage.prototype.getUserBetPage=function(queryParam,pageNo){
	var detailType=$(".lottery-name span p").attr("data-value");
	var userName=$("#userName").val();
	if(userName!=null && userName!=""){
		moneyDetailPage.queryParam.userName=userName;
	}else{
		moneyDetailPage.queryParam.userName=null;
	}
	if(detailType!=null && detailType!=""){
		moneyDetailPage.queryParam.detailType=detailType;
	}else{
		moneyDetailPage.queryParam.detailType=null;
	}
	moneyDetailPage.pageParam.pageNo=pageNo;
	var query = {
			"dageRange" : moneyDetailPage.queryParam.dageRange,
			/*"createdDateStart" : new Date(queryParam.createdDateStart),
			"createdDateEnd" : 	new Date(queryParam.createdDateEnd),*/
			"detailType" : 	moneyDetailPage.queryParam.detailType,
			"queryScope" : 	moneyDetailPage.queryParam.queryScope,
			"userName" : 	moneyDetailPage.queryParam.userName,
		};
	var jsonData = {
		"query" : query,
		"pageNo" : moneyDetailPage.pageParam.pageNo,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/moneydetail/moneyDetailQuery",
		contentType : 'application/json;charset=utf-8',
		data : JSON.stringify(jsonData),
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				moneyDetailPage.refreshUserOrderPages(result.data);
			} else if (result.code == "error") {
				showTip(result.data);
			}
		}
	});
}

/**
 * 刷新列表数据
 */
TeamMoneyDetailPage.prototype.refreshUserOrderPages = function(page){
	$(".user-btn").html("正在加载中...");
	var userOrderListObj = $("#userTeamMoneyDetail");
	var str = "";
	var userOrders = page.pageContent;
	
	//如果是第一页
	if(page.pageNo == 1) {
		userOrderListObj.html("");
		if(userOrders.length == 0){
			str += '<div class="empty-tip">';
			str += '<h4 class="nodata">暂无数据</h4>';
	    	str += '</div>';
	       
	    	userOrderListObj.append(str);
	    	str = "";
	    	$(".user-btn").hide();
	    };
	} 
    if(userOrders != null && userOrders.length > 0){
    	//记录数据显示
    	for(var i = 0 ; i < userOrders.length; i++){
    		var userMoneyDetail = userOrders[i];
    		var lotteryIdStr = userMoneyDetail.lotteryId;
    		
    		str += '<li>';
			str += '    <div class="info clear">';
			str += '        <div class="info-txt fl">';
			str += '			<h2>用户名：'+userMoneyDetail.userName+'</h2>';
			str += '            <p>交易时间：'+ userMoneyDetail.createDateStr +'</p>';
			str += '            <p>流水号：'+ lotteryIdStr +'</p>';
			str += '        </div>';
			if(userMoneyDetail.detailTypeDes == "奖金派送"){
				  str += 	'<div class="info-tag fr color-red">'+ userMoneyDetail.detailTypeDes +'</div>';
			}else if(userMoneyDetail.detailTypeDes == "投注扣款"){
				  str += 	'<div class="info-tag fr color-green">'+ userMoneyDetail.detailTypeDes +'</div>';
			}else if(userMoneyDetail.detailTypeDes == "投注回退"){
				  str += 	'<div class="info-tag fr color-yellow">'+ userMoneyDetail.detailTypeDes +'</div>';
			}else{
				  str += 	'<div class="info-tag fr color-blue">'+ userMoneyDetail.detailTypeDes +'</div>';
			}
			str += '    </div>';
			str += '    <div class="money clear">';
			if(parseFloat(parseFloat(userMoneyDetail.income).toFixed(frontCommonPage.param.fixVaue)) > 0){
				str += '        <span>收入（元）<br/><em class="color-red">'+ parseFloat(userMoneyDetail.income).toFixed(frontCommonPage.param.fixVaue) +'</em></span>';
			}else{
				str += '        <span>收入（元）<br/><em>'+ parseFloat(userMoneyDetail.income).toFixed(frontCommonPage.param.fixVaue) +'</em></span>';
			}
			if(parseFloat(parseFloat(userMoneyDetail.pay).toFixed(frontCommonPage.param.fixVaue)) > 0){
				str += '        <span>支出（元）<br/><em class="color-green">'+ parseFloat(userMoneyDetail.pay).toFixed(frontCommonPage.param.fixVaue) +'</em></span>';
			}else{
				str += '        <span>支出（元）<br/><em>'+ parseFloat(userMoneyDetail.pay).toFixed(frontCommonPage.param.fixVaue) +'</em></span>';
			}
			str += '        <span>余额（元）<br/><em>'+ parseFloat(userMoneyDetail.balanceAfter).toFixed(frontCommonPage.param.fixVaue) +'</em></span>';
			str += '    </div>';
			str += '</li>';
			
    		userOrderListObj.append(str);
    		str = "";
    	}
    	$("#showMore").hide();
    	if(page.pageNo < page.totalPageNum) {
    		$(".user-btn").html("加载更多");
    		$("#showMore").show();
        	//如果当前页数小于总页数，显示加载更多
    		$(".user-btn").unbind("click").click(function(){
    			moneyDetailPage.pageParam.pageNo++;
    			//userMoneyDetailPage.getAllUserMoneyDetails();
    			moneyDetailPage.getUserBetPage(moneyDetailPage.queryParam,moneyDetailPage.pageParam.pageNo);
    		});
    	}else{
    		$("#showMore").hide();
    	}
    }else{
		$("#showMore").hide();
	};
};

TeamMoneyDetailPage.prototype.getTabProstate=function(prostate){
	if(prostate!=null && prostate!=""){
		moneyDetailPage.queryParam.prostateStatus=prostate;
	}else{
		moneyDetailPage.queryParam.prostateStatus=null;
	}
	moneyDetailPage.getUserBetPage(moneyDetailPage.queryParam,1);
}
