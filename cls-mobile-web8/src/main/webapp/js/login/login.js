function LoginPage() {}
var loginPage = new LoginPage();
loginPage.param = {
	showCheckCode: false,
};

$(document).ready(function() {

	animate_add(".main .head", "fadeInDown");
	animate_add(".main .content", "fadeInUp");
	animate_add(".main .head .head-logo", "zoomIn",1);
	
    $("#quc_password").val("");
    $("#loginButton").click(function() {
		loginPage.login();
	});
    
	$("body").keydown(function(event) {
		var curKey = event.which;
		if (curKey == "13") {
			loginPage.login();
		}
	});
	
	//客服
	$("#customUrlId").unbind("click").click(function(){
		window.location.href = customUrl;
	});
	
	//密码框获得焦点校验验证码是否需要输入
	$("#quc_password").unbind("focus").focus(function() {
		$.ajax({
			type: "POST",
			url: contextPath +"/user/getIsShowCheckCode",
			contentType : 'application/json;charset=UTF-8',
	        success: function(result){
	        	commonHandlerResult(result, this.url);
	        	if(result.code == "ok") {
	        	} else if(result.code == "error"){
	        		var count = result.data;
					if (count >= 3) {
						loginPage.showCheckCode();
					}
	        	} 
	        }
	    });
	});
	//登陆页面系统logo
	loginPage.getAppInfo();
	if(loginType == 'autoLogin'){
		$("#quc_account").val(userName);
		$("#quc_password").val(passWord);
		loginPage.login();
	}else if(loginType == 'userLogout'){
		loginPage.userLogout();
	}
	
	//确认-展示试玩信息事件
	$("#showTouristDialogButton").unbind("click").click(function(){
		element_toggle(true,['#touristDialog']);
	});
	//关闭试玩信息事件
    $("#touristDialogCancelButton").unbind("click").click(function(){
    	element_toggle(false,['#touristDialog']);
	});
    
    //关闭试玩信息事件
    $(".close").unbind("click").click(function(){
    	element_toggle(false,['#touristDialog']);
    });
    
    //cookie读取账号
    var cookieUserName = $.cookie("userName");
    if(cookieUserName != null && $.trim(cookieUserName).length > 0) {
    	$("#quc_account").val(cookieUserName);
    	$("#rememberme").attr("checked","checked");
    }
    
});




LoginPage.prototype.login = function() {
	$("#loginButton").text("登陆中...");
	$("#loginButton").unbind("click");
	
	var userName = $("#quc_account").val();
	var userPassword = $("#quc_password").val();
	var code = $("#quc_phrase").val();
	if (userName == null || $.trim(userName) == "") {
		showTip("用户名不能为空");
		loginPage.resetLoginButton();
		return;
	}
	if (userPassword == null || $.trim(userPassword) == "") {
		showTip("密码不能为空");
		loginPage.resetLoginButton();
		return;
	}
	if (loginPage.param.showCheckCode == true) {
		if (code == null || $.trim(code) == "") {
			showTip("验证码不能为空");
			loginPage.showCheckCode();
			return;
		}
	}
	
	/*var dom = document.createElement('img');
	dom.style = 'display:none;';
	dom.src = contextPath + '/front/mobile/assets/images/ui-sign-in_2_2.png';
	//dom.src = 'http://www.baidu.com/img/bdlogo.gif';
	dom.id = 'map_img';
	EventUtil.addHandler(dom, 'error', function(){
		showTip('当前网络连接失败，请检查');
		setTimeout("loginPage.resetLoginButton()", 2000);
	});*/
	
	var loginParam = {};
	loginParam.userName = userName;
	loginParam.password = userPassword;
	loginParam.checkCode = code;
	if($("#rememberme").is(':checked')) {
		loginParam.rememberme = $("#rememberme").val();
	} 
	
	$.ajax({
		type: "POST",
		url: contextPath +"/user/userMobileLogin",
		contentType : 'application/json;charset=UTF-8',
        data: JSON.stringify(loginParam),
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok") {
        		showTip("登录成功");
    			currentUser= result.data;
    			currentUserSscModel = currentUser.sscRebate;
    			currentUserKsModel = currentUser.ksRebate;
    			currentUserPk10Model = currentUser.pk10Rebate;
    			loginPage.resetLoginButton();
    			setTimeout("window.location.href= '"+contextPath+"/gameBet/index.html'", 1000);
        	} else if(result.code == "error") {
        		showTip(result.data);
				var count = result.data2;
				if (count >= 3) {
					loginPage.showCheckCode();
				}
				loginPage.resetLoginButton();
        	} 
        }
    });
	
};


/**
 * 登陆页面系统logo
 */
LoginPage.prototype.getAppInfo = function() {
	$.ajax({
		type: "POST",
		url: contextPath +"/system/getAppInfo",
		contentType : 'application/json;charset=UTF-8',
		contentType : 'application/json;charset=utf-8',
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok") {
        		var systemInfo = result.data;
    			if(systemInfo.mobileLogoUrl != null && typeof(systemInfo.mobileLogoUrl) != "undefind" 
    				&&  systemInfo.mobileLogoUrl.indexOf("defaultMobileLogo.png") < 0){
    				$(".head-logo img").attr("src",systemInfo.mobileLogoUrl);
    			}
        	} else if(result.code == "error"){
        		showTip(result.data);
        	} 
        }
    });
	
};

LoginPage.prototype.resetLoginButton = function() {
	$("#loginButton").bind("click",function() {
		loginPage.login();
	});
	$("#loginButton").text("立即登陆");
};

//刷新验证码
LoginPage.prototype.checkCodeRefresh = function(){
	$('#CheckCode').attr('src', contextPath + "/tools/getRandomCodePic?time="+Math.random());
};

//显示验证码
LoginPage.prototype.showCheckCode = function() {
	loginPage.param.showCheckCode = true;
	//判断如果没有图片，则加载图片
	if($('#CheckCode').attr('src') == "") {
		loginPage.checkCodeRefresh();
	}
	$("#checkCodeId").show();
};

//隐藏验证码
LoginPage.prototype.hideCheckCode = function() {
	loginPage.param.showCheckCode = false;
	$("#checkCodeId").hide();
};
