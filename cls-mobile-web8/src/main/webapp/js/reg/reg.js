function RegPage(){}
var regPage = new RegPage();

$(document).ready(function() {
	//注册提交动作
	$("#regButton").unbind("click").click(function(event){
		regPage.register();
	});
	
	//客服
	$("#customUrlRegId").unbind("click").click(function(){
		window.location.href = customUrl;
	});
	if(regPage.param.regShowSms==1){
		$('.mobile-verifycode-line').show();
	}
	// 手机验证码发送
	$('.user-line .line-content.por .btn-sendmobile').on('click', function(){
		var mobile = $("#mobile").val();
		var reg = /^1\d{10}$/;
		if(mobile == null || $.trim(mobile) == "" || !reg.test(mobile)){
			showTip("请输入正确手机号！");
			return;
		}
		regPage.sendSmsVerifycode();
	
	});
});

regPage.param = {
	code : null,
	regShowPhone:null,
	regRequiredPhone:null,
	regShowEmail:null,
	regRequiredEmail:null,
	regShowQq:null,
	regRequiredQq:null,
	bizSystemConfigVO:null
};

/**
 * 验证用户名
 */
RegPage.prototype.userDuplicate = function(){
	var userName = $("#username").val();
	var reg= /^[A-Za-z]+$/;
	if(userName == null || $.trim(userName) == "" || !reg.test(userName.substr(0,1)) || userName.length < 4 || userName.length > 10){
		regPage.cssSetting($("#username_field"), "red");
		$("#username_tip").text("账号由4~10位字母或数字，首位为字母");
		$("#username_tip").addClass("red");
	}else{
		var param={};
		param.userName=userName;
		$.ajax({
			type: "POST",
	        url: contextPath +"/user/userDuplicate",
	        contentType :"application/json;charset=UTF-8",
	        data:JSON.stringify(param), 
	        dataType:"json",
	        success: function(result){
	        	commonHandlerResult(result, this.url);
	        	if(result.code == "ok"){
	        		$("#username_tip").text("");
					$("#username_tip").removeClass("red");
	        	}else if(result.code == "error"){
	        		$("#username_tip").addClass("red");
					$("#username_tip").text("该用户名已经被注册");
	        	}else{
	        		showErrorDlg(jQuery(document.body), "用户名是否重复校验请求失败.");
	        	}
	        }
		});
	}
};


/**
 * 发送验证码
 */
RegPage.prototype.sendSmsVerifycode = function(){
	var mobile = $("#mobile").val();
	var reg = /^1\d{10}$/;
	if(mobile == null || $.trim(mobile) == "" || !reg.test(mobile)){
		$("#mobile_line").addClass("error");
		$("#mobile_line .line-msg").text("请输入正确手机号！");
		return;
	}else{
//		param.phone=mobile;
		var jsonData={
			"phone" : mobile,
			"type" : "REG_TYPE",
		};
		$.ajax({
			type: "POST",
	        url: contextPath +"/usersafe/sendPhoneCode",
	        contentType :"application/json;charset=UTF-8",
	        data:JSON.stringify(jsonData), 
	        dataType:"json",
	        success: function(result){
	        	commonHandlerResult(result, this.url);
	        	if(result.code == "ok"){
	        		$(this).hide();
					$('.mobile-verify-time').show();
					$('.mobile-verifycode-line').show();
					// 手机验证码倒计时60秒
					var i = 60;
					var interval = setInterval(function(){
						i = i - 1;
						$('.mobile-verify-time .countdown').text(i);
						if(i == 0){
							clearInterval(interval);
							$('.mobile-verify-time').hide();
							$('.user-line .line-content.por .btn-sendmobile').show();
						}
					},1000);
	        	}else if(result.code == "error"){
	        		showTip("发送失败,请重新发送");
	        	}else{
	        		showTip("发送失败,请重新发送");
	        	}
	        }
		});
	}
};

/**
 * 会员登录验证
 */
RegPage.prototype.register = function(){
	$("#regButton").attr('disabled',false);
	$("#regButton").text("提交中.....");
	
	var userName = $("#username").val();
	var mobile = $("#mobile").val();
	var qq = $("#qq").val();
	var email=$("#email").val();
	var userPassword = $("#password").val();
	var userPassword2 = $("#password2").val();
	var code = $("#verifycode").val();
	var invitationCode = $("#invitationCode").val();
	var bizSystem = $("#bizSystem").val();
	var verifycode=$("#mobile-verifycode").val();
	var reg= /^[A-Za-z]+$/;
	var mobilereg = /^1\d{10}$/;
	var qq_reg =new RegExp("[1-9][0-9]{4,11}");
	var email_reg=/^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
	/*if(invitationCode == null || $.trim(invitationCode) == ""){
		if(regPage.param.invitationCode==null||regPage.param.invitationCode==''){
			showTip("请输入邀请码");
		}else{
			showTip("链接无效！");
		}
		$("#invitationCode").focus();
		$("#regButton").text("注册");
		return;
	}*/

	if(userName == null || $.trim(userName) == "" || !reg.test(userName.substr(0,1)) || userName.length < 4 || userName.length > 10){
		showTip("账号由4~10位字母或数字，首位为字母");
		$("#username").focus();
		$("#regButton").text("注册");
		$("#regButton").removeAttr('disabled');
		return;
	}
	
	if(regPage.param.regShowPhone==1&&regPage.param.regRequiredPhone==1){
		if(mobile == null || $.trim(mobile) == "" || !mobilereg.test(mobile)){
			showTip("请输入手机号码！");
			$("#mobile").focus();
			$("#regButton").text("注册");
			$("#regButton").removeAttr('disabled');
			return;
		}
	}
	
	if(regPage.param.regRequiredQq==1&&regPage.param.regShowQq==1){
		if(qq == null || $.trim(qq) == "" || !qq_reg.test(qq)){
			showTip("请输入qq！");
			$("#qq").focus();
			$("#regButton").text("注册");
			$("#regButton").removeAttr('disabled');
			return;
		}
	}
	
	if(regPage.param.regRequiredEmail==1&&regPage.param.regShowEmail==1){
		if(email == null || $.trim(email) == "" || !email_reg.test(email)){
			showTip("请输入邮箱地址！");
			$("#email").focus();
			$("#regButton").text("注册");
			$("#regButton").removeAttr('disabled');
			return;
		}
	}
	
	if(userPassword == null || $.trim(userPassword) == "" || userPassword.length < 6 || userPassword.length > 15){
		showTip("密码由6-15位字符组成，区分大小写");
		$("#password").focus();
		$("#regButton").text("注册");
		$("#regButton").removeAttr('disabled');
		return;
	}
	
	if(userPassword2 == null || $.trim(userPassword2) == ""){
		showTip("确认密码由6-15位字符组成，区分大小写");
		$("#password2").focus();
		$("#regButton").text("注册");
		$("#regButton").removeAttr('disabled');
		return;
	}else if(userPassword != userPassword2){
		showTip("两次密码不一致");
		$("#password2").focus();
		$("#regButton").text("注册");
		$("#regButton").removeAttr('disabled');
        return;
	}
	
	if(code == null || $.trim(code) == ""){
		showTip("请输入正确的验证码");
		$("#verifycode").focus();
		$("#regButton").text("注册");
		$("#regButton").removeAttr('disabled');
		return;
	}
	if(bizSystem == null || bizSystem == ""){
		showTip("注册链接地址不合法.");
		$("#regButton").text("注册");
		$("#regButton").removeAttr('disabled');
	    return;
	}

	
	
	var userRegister = {};
	userRegister.userName = userName;
	userRegister.userPassword = userPassword;
	userRegister.userPassword2 = userPassword2;
	userRegister.checkCode = code;
	userRegister.invitationCode = invitationCode;
	userRegister.bizSystem = bizSystem;
	userRegister.email=email;
	userRegister.qq=qq;
	userRegister.mobile=mobile;
	userRegister.mobileVcode = verifycode;
	
	$.ajax({
		type: "POST",
        url: contextPath +"/user/userRegister",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(userRegister), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		showTip("注册成功！");
    			if(regPage.param.bizSystemConfigVO.regDirectLogin==1){
    				var loginParam = {};
    				loginParam.userName = userRegister.userName;
    				loginParam.password = userRegister.userPassword;
    				$.ajax({
    					type: "POST",
    			        url: contextPath +"/user/userMobileLogin",
    			        contentType :"application/json;charset=UTF-8",
    			        data:JSON.stringify(loginParam), 
    			        dataType:"json",
    			        success: function(result){
    			        	commonHandlerResult(result, this.url);
    			        	if(result.code == "ok"){
    			        		showTip("登录成功");
    							currentUser=result.data;
    							/*if((currentUser.userName=="cskabc"||currentUser.userName=="csdown"||currentUser.userName=="cs001a")&&currentUser.bizSystem=="k8cp"){
    								window.location.href="http://www.baidu.com";
    								return;
    				    		}*/
    							currentUserSscModel = currentUser.sscRebate;
    							currentUserKsModel = currentUser.ksRebate;
    							currentUserPk10Model = currentUser.pk10Rebate;
    							//loginPage.resetLoginButton();
    							setTimeout("window.location.href= '"+contextPath+"/gameBet/index.html'", 1000);
    							
    			        	}else if(result.code == "error"){
    							showTip(result.data);
    							var count = result.data2;
    							if (count >= 3) {
    								loginPage.showCheckCode();
    							}
    							loginPage.resetLoginButton();
    			        	}else{
    			        		$("#errMsg").css("display", "block");
    							$("#errMsg").html("登录请求数据失败");
    							loginPage.resetLoginButton();
    			        	}
    			        }
    				});
    			}else{
    				setTimeout("window.location.href= '"+contextPath+"/gameBet/login.html'",1500);
    			}
        	}else if(result.code == "error"){
        		showTip(result.data);
    			$("#regButton").removeAttr('disabled');
    			$("#regButton").text("注册");
        	}else{
        		showTip(result.data);
    			$("#regButton").removeAttr('disabled');
    			$("#regButton").text("注册");
        	}
        	checkCodeRefresh();
        }
	});
};

/**
 * 样式处理
 */
RegPage.prototype.cssSetting = function(obj,addClass){
	obj.removeClass("field-click");
	obj.removeClass("field-pass");
	obj.removeClass("field-error");
	obj.addClass(addClass);
};