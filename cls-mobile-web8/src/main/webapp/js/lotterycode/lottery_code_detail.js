function LotteryCodeDetailPage() {
}

var lotteryCodeDetailPage= new LotteryCodeDetailPage();

lotteryCodeDetailPage.param = {
	loadBoolean : true,
	loadBoolean_first : true,
	lotteryKind : null
};

// 分页参数
lotteryCodeDetailPage.pageParam = {
	queryMethodParam : null,
	pageNo : 1,
	pageMaxNo : 1, // 最大的页数
	skipHtmlStr : ""
};

$(document)
		.ready(
				function() {
					$(".i-discover").parent().addClass("on");
					lotteryCodeDetailPage.param.loadBoolean = true;
					lotteryCodeDetailPage.param.loadBoolean_first = true;
					lotteryCodeDetailPage.pageParam.pageNo = 1;
					var lotteryKind = lotteryCodeDetailPage.param.lotteryKind;

					lotteryCodeDetailPage.getLotteryCodeDetail(lotteryKind, lotteryCodeDetailPage.pageParam.pageNo);
					// lotteryCodeDetailPage.pageParam.pageNo++;
					/*
					 * lotteryCodeDetailPage.getLotteryCodeDetail(lotteryKind,lotteryCodeDetailPage.pageParam.pageNo,
					 * $scope); dwr.engine.setAsync(true);
					 * $(".moreload").hide(); $(".betting").show()
					 */

					lotteryCodeDetailPage.initShengXiaoNumber();

					// 滚动事件绑定
					// var loadBool = true;
					$(window)
							.on(
									'scroll',
									function() {
										var scrollHeight = $(window).scrollTop(), windowHeight = $(window).height(), documentHeight = $(document).height(), reachBottom = (scrollHeight + windowHeight) >= documentHeight - 10;

										if (reachBottom && lotteryCodeDetailPage.param.loadBoolean) {
											// $(".return-btn").hide();
											$(".load-btn").show();
											// loadBool = false;
											lotteryCodeDetailPage.param.loadBoolean = false;

											setTimeout(function() {
												// loadBool = true;
												// $(".return-btn").show();
												// $(".load-btn").hide();

												lotteryCodeDetailPage.pageParam.pageNo++;
												// lotteryCodeDetailPage.param.loadBoolean
												// = false;
												lotteryCodeDetailPage.getLotteryCodeDetail(lotteryKind, lotteryCodeDetailPage.pageParam.pageNo);
											}, 3000);
										}

									});

				});

/**
 * 分页获取最近十期的开奖结果
 */
LotteryCodeDetailPage.prototype.getLotteryCodeDetail = function(lotteryKind, pageNo) {
	// 第一页的时候处理顶部展示
	if (pageNo == 1) {
		lotteryCodeDetailPage.showLogoImg(lotteryKind);
		$("#lotteryExpectId").text("第XXXX期");
		if (lotteryKind.indexOf("KS") > 0) {
			// $("#nearestTenLotteryCode").html("");
			var str = "";
			str += "			<img src='../images/dice_pic/gray/1.png' alt='' class='dice' id='lotteryNumber1'>";
			str += "			<img src='../images/dice_pic/gray/1.png' alt='' class='dice' id='lotteryNumber2'>";
			str += "			<img src='../images/dice_pic/gray/1.png' alt='' class='dice' id='lotteryNumber3'>";
			$("#openCodeId").html(str);
		} else if (lotteryKind == "BJPK10" || lotteryKind == "JSPK10") {
			$("#openCodeId").addClass("muns-middle");
			$('#lotteryNumber1').text("");
			$('#lotteryNumber2').text("");
			$('#lotteryNumber3').text("");
			$('#lotteryNumber4').text("");
			$('#lotteryNumber5').text("");
			$('#lotteryNumber6').text("");
			$('#lotteryNumber7').text("");
			$('#lotteryNumber8').text("");
			$('#lotteryNumber9').text("");
			$('#lotteryNumber10').text("");
		} else if (lotteryKind == "LHC") {
			$("#openCodeId").addClass("muns-middle");
			$('#lotteryNumber1').text("");
			$('#lotteryNumber2').text("");
			$('#lotteryNumber3').text("");
			$('#lotteryNumber4').text("");
			$('#lotteryNumber5').text("");
			$('#lotteryNumber6').text("");
			$('.and').show();
			$('#lotteryNumber7').text("");
			$('#lotteryNumber8').hide();
			$('#lotteryNumber9').hide();
			$('#lotteryNumber10').hide();
		} else {
			$('#lotteryNumber1').text("");
			$('#lotteryNumber2').text("");
			$('#lotteryNumber3').text("");
			$('#lotteryNumber4').text("");
			$('#lotteryNumber5').text("");
			$('#lotteryNumber6').hide();
			$('#lotteryNumber7').hide();
			$('#lotteryNumber8').hide();
			$('#lotteryNumber9').hide();
			$('#lotteryNumber10').hide();
		}
	}
	var jsonData = {
		"lotteryName" : lotteryKind,
		"pageNo" : pageNo,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/code/getNearestLotteryCode",
		data : JSON.stringify(jsonData),// 将对象序列化成JSON字符串;
		dataType : "json",// 设置请求参数类型
		contentType : 'application/json;charset=utf-8',// 设置请求头信息;
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				lotteryCodeDetailPage.refreshShowLotteryCode(lotteryKind, pageNo, result.data);
			} else if (result.code == "error") {
				showTip(result.data);
			}
		}
	});
};

/**
 * 刷新开奖号码
 * @param lotteryKind
 * @param pageNo
 * @param lotteryCodePage
 */
LotteryCodeDetailPage.prototype.refreshShowLotteryCode = function(lotteryKind, pageNo, lotteryCodePage) {
	// 加载最后一页时，隐藏加载中按钮
	if (lotteryCodePage.length < 15) {
		lotteryCodeDetailPage.param.loadBoolean = false;
	} else {
		lotteryCodeDetailPage.param.loadBoolean = true;
	}
	// 处理第一页
	if (pageNo == 1) {
		var lotteryCodeHead = {};
		if (lotteryCodePage.length > 0 && typeof (lotteryCodePage) != "undefined") {
			lotteryCodeHead = lotteryCodePage[0];

			// 特殊处理第一期的
			var lotteryExpectHead = frontCommonPage.getLotteryExpect(lotteryKind, lotteryCodeHead.lotteryNum);
			$("#lotteryExpectId").text("第" + lotteryExpectHead + "期");
			if (lotteryKind.indexOf("KS") > 0) {
				// 快三开奖图片显示
				lotteryCodeDetailPage.showHeadLastLotteryPictureCode(lotteryCodeHead);
			} else if (lotteryKind == "BJPK10" || lotteryKind == "JSPK10") {
				$("#openCodeId").addClass("muns-middle");
				$('#lotteryNumber1').text(lotteryCodeHead.numInfo1);
				$('#lotteryNumber2').text(lotteryCodeHead.numInfo2);
				$('#lotteryNumber3').text(lotteryCodeHead.numInfo3);
				$('#lotteryNumber4').text(lotteryCodeHead.numInfo4);
				$('#lotteryNumber5').text(lotteryCodeHead.numInfo5);
				$('#lotteryNumber6').text(lotteryCodeHead.numInfo6);
				$('#lotteryNumber7').text(lotteryCodeHead.numInfo7);
				$('#lotteryNumber8').text(lotteryCodeHead.numInfo8);
				$('#lotteryNumber9').text(lotteryCodeHead.numInfo9);
				$('#lotteryNumber10').text(lotteryCodeHead.numInfo10);
			} else if (lotteryKind == "LHC") {
				$("#openCodeId").addClass("muns-middle");
				$('#lotteryNumber1').text(lotteryCodeHead.numInfo1);
				$('#lotteryNumber2').text(lotteryCodeHead.numInfo2);
				$('#lotteryNumber3').text(lotteryCodeHead.numInfo3);
				$('#lotteryNumber4').text(lotteryCodeHead.numInfo4);
				$('#lotteryNumber5').text(lotteryCodeHead.numInfo5);
				$('#lotteryNumber6').text(lotteryCodeHead.numInfo6);
				$('#lotteryNumber7').text(lotteryCodeHead.numInfo7);
				$('#lotteryNumber8').hide();
				$('#lotteryNumber9').hide();
				$('#lotteryNumber10').hide();

				// 颜色
				var colors = lotteryCodeDetailPage.changeColorByCode(lotteryCodeHead);
				$('#lotteryNumber1').addClass("lhc-" + colors[0]);
				$('#lotteryNumber2').addClass("lhc-" + colors[1]);
				$('#lotteryNumber3').addClass("lhc-" + colors[2]);
				$('#lotteryNumber4').addClass("lhc-" + colors[3]);
				$('#lotteryNumber5').addClass("lhc-" + colors[4]);
				$('#lotteryNumber6').addClass("lhc-" + colors[5]);
				$('#lotteryNumber7').addClass("lhc-" + colors[6]);

				// 生肖
				$('.lhc-shengxiao').show();
				$('#lotteryNumber1').closest('.lhc-item').find('.lhc-shengxiao').text(lotteryCodeDetailPage.returnAninal(lotteryCodeHead.numInfo1)).addClass("lhc-" + colors[0]);
				$('#lotteryNumber2').closest('.lhc-item').find('.lhc-shengxiao').text(lotteryCodeDetailPage.returnAninal(lotteryCodeHead.numInfo2)).addClass("lhc-" + colors[1]);
				$('#lotteryNumber3').closest('.lhc-item').find('.lhc-shengxiao').text(lotteryCodeDetailPage.returnAninal(lotteryCodeHead.numInfo3)).addClass("lhc-" + colors[2]);
				$('#lotteryNumber4').closest('.lhc-item').find('.lhc-shengxiao').text(lotteryCodeDetailPage.returnAninal(lotteryCodeHead.numInfo4)).addClass("lhc-" + colors[3]);
				$('#lotteryNumber5').closest('.lhc-item').find('.lhc-shengxiao').text(lotteryCodeDetailPage.returnAninal(lotteryCodeHead.numInfo5)).addClass("lhc-" + colors[4]);
				$('#lotteryNumber6').closest('.lhc-item').find('.lhc-shengxiao').text(lotteryCodeDetailPage.returnAninal(lotteryCodeHead.numInfo6)).addClass("lhc-" + colors[5]);
				$('#lotteryNumber7').closest('.lhc-item').find('.lhc-shengxiao').text(lotteryCodeDetailPage.returnAninal(lotteryCodeHead.numInfo7)).addClass("lhc-" + colors[6]);
			} else {
				$('#lotteryNumber1').text(lotteryCodeHead.numInfo1);
				$('#lotteryNumber2').text(lotteryCodeHead.numInfo2);
				$('#lotteryNumber3').text(lotteryCodeHead.numInfo3);
				$('#lotteryNumber4').text(lotteryCodeHead.numInfo4);
				$('#lotteryNumber5').text(lotteryCodeHead.numInfo5);
				$('#lotteryNumber6').hide();
				$('#lotteryNumber7').hide();
				$('#lotteryNumber8').hide();
				$('#lotteryNumber9').hide();
				$('#lotteryNumber10').hide();
			}
		}
	}

	// 加载主内容
	lotteryCodeDetailPage.showMainDiv(lotteryKind, pageNo, lotteryCodePage);

	// 处理立即投注登陆判断
	var content = "需要登录才能进行投注，是否登录？";
	var passUrl = "#/lottery/" + lotteryKind.toString().toLowerCase();
	var hrefStr = 'javascript:judgeCurrentUserToLogin("' + passUrl + '","' + content + '")';
	$(".user-btn").parent().attr("href", hrefStr);

	// 如果当前页数小于总页数，显示加载更多
	// var pageContent = lotteryCodePage.pageContent;
	// 添加加载更多事件
	$(".user-btn").unbind("click").click(function() {
		lotteryCodeDetailPage.pageParam.pageNo++;
		lotteryCodeDetailPage.param.loadBoolean = false;
		lotteryCodeDetailPage.getLotteryCodeDetail(lotteryKind, lotteryCodeDetailPage.pageParam.pageNo);
	});

};

/**
 * 根据彩种显示彩种
 */
LotteryCodeDetailPage.prototype.showMainDiv = function(lotteryKind, pageNo, lotteryCodeList) {
	if (typeof (lotteryCodeList) == "undefined" || typeof (lotteryKind) == "undefined") {
		return;
	}
	var str = "";
	// 有数据而且第一页的时候处理标题头
	if (lotteryCodeList.length > 0 && pageNo == 1) {
		if (lotteryKind.indexOf("KS") > 0) {
			str += " <div class='main  award-kuaisan animated' style='animation-name: fadeInUp; animation-delay: 0.2s;'><div class='container lottery-dynamic'>";
		} else if (lotteryKind == "BJPK10" || lotteryKind == "JSPK10") {
			str += " <div class='main award_pk10 animated' style='animation-name: fadeInUp; animation-delay: 0.2s;'><div class='container lottery-dynamic'>";
		} else {
			str += " <div class='main award-ssc animated'><div class='container lottery-dynamic'>";
		}
		str += "	<div class='titles'>";
		if (lotteryKind.indexOf("KS") > 0) {
			str += "		<div class='child child1'><p>期号</p></div>";
			str += "		<div class='child child2'><p>开奖号</p></div>";
			str += "		<div class='child child3'><p>和值</p></div>";
			str += "		<div class='child child4' style='width:190px'><p>形态</p></div>";
		} else if (lotteryKind == "BJPK10" || lotteryKind == "JSPK10" || lotteryKind == "XJPLFC") {
			str += "		<div class='child child1' style='width:400px'><p>期号</p></div>";
			str += "		<div class='child child2' style='width:400px'><p>开奖号</p></div>";
		} else if (lotteryKind == "LHC") {
			str += "		<div class='child child1' style='width:300px'><p>期号</p></div>";
			str += "		<div class='child child2' style='width:400px'><p>开奖号</p></div>";
		} else {
			str += "		<div class='child child1'><p>期号</p></div>";
			str += "		<div class='child child2'><p>开奖号</p></div>";
			str += "		<div class='child child3' style='width:200px'><p>后三组态</p></div>";
		}
		str += "	</div>";
		str += "	<div class='content' id='contentId_Str'></div>";
		$("#nearestTenLotteryCode").append(str);
	}

	for (var i = 0; i < lotteryCodeList.length; i++) {
		var lotteryCode = lotteryCodeList[i];
		var lotteryExpect = frontCommonPage.getLotteryExpect(lotteryKind, lotteryCode.lotteryNum);
		var contentStr = "";
		if (lotteryKind == "BJPK10" || lotteryKind == "JSPK10") {
			contentStr += "		<div class='line'>";
			contentStr += "			<div class='child child1' style='width: 300px;'><p>" + lotteryExpect + "</p></div>";
			contentStr += "			<div class='child child2 font-blue muns muns-small'>";
			contentStr += "				<div class='mun color-red'>" + lotteryCode.numInfo1 + "</div>";
			contentStr += "				<div class='mun color-red'>" + lotteryCode.numInfo2 + "</div>";
			contentStr += "				<div class='mun color-red'>" + lotteryCode.numInfo3 + "</div>";
			contentStr += "				<div class='mun color-red'>" + lotteryCode.numInfo4 + "</div>";
			contentStr += "				<div class='mun color-red'>" + lotteryCode.numInfo5 + "</div>";
			contentStr += "				<div class='mun color-red'>" + lotteryCode.numInfo6 + "</div>";
			contentStr += "				<div class='mun color-red'>" + lotteryCode.numInfo7 + "</div>";
			contentStr += "				<div class='mun color-red'>" + lotteryCode.numInfo8 + "</div>";
			contentStr += "				<div class='mun color-red'>" + lotteryCode.numInfo9 + "</div>";
			contentStr += "				<div class='mun color-red'>" + lotteryCode.numInfo10 + "</div>";
			contentStr += "			</div>";
			contentStr += "		</div>";
		} else if (lotteryKind.indexOf("KS") > 0) {
			contentStr += "		<div class='line'>";
			contentStr += "			<div class='child child1'><p>" + lotteryExpect + "</p></div>";
			contentStr += "			<div class='child child2 font-blue'>";

			contentStr += lotteryCodeDetailPage.showLastLotteryPictureCode(lotteryCode);

			contentStr += "	</div>";
			// 和值
			var codeStatus = lotteryCodeDetailPage.getLotteryCodeStatus(lotteryCode.opencodeStr);
			contentStr += "	<div class='child child3'><p>" + codeStatus + "</p></div>";
			contentStr += "	<div class='child child4' style='width:190px'>";
			// 获取当前开奖号码的和值组态显示 大小
			if (codeStatus <= 10) {
				contentStr += "	<span class='child-small'>小</span> ";
			} else {
				contentStr += "	<span class='child-big'>大</span> ";
			}

			// 获取当前开奖号码的和值组态显示 单双
			if (codeStatus % 2 == 0) {
				contentStr += "| <span class='child-odd'>双</span></div>";
			} else {
				contentStr += "| <span class='child-even'>单</span></div>";
			}

			contentStr += "</div>";

		} else if (lotteryKind == "XJPLFC") {
			contentStr += "		<div class='line'>";
			contentStr += "			<div class='child child1'><p>" + lotteryExpect + "</p></div>";
			contentStr += "			<div class='child child2 font-blue muns muns-small'>";
			contentStr += "				<div class='mun color-red'>" + lotteryCode.numInfo1 + "</div>";
			contentStr += "				<div class='mun color-red'>" + lotteryCode.numInfo2 + "</div>";
			contentStr += "				<div class='mun color-red'>" + lotteryCode.numInfo3 + "</div>";
			contentStr += "				<div class='mun color-red'>" + lotteryCode.numInfo4 + "</div>";
			contentStr += "				<div class='mun color-red'>" + lotteryCode.numInfo5 + "</div>";
			contentStr += "			</div>";
			contentStr += "		</div>";
		} else if (lotteryKind == "LHC") {
			var colors = lotteryCodeDetailPage.changeColorByCode(lotteryCode);
			contentStr += "		<div class='line'>";
			contentStr += "			<div class='child child1 w300'><p>" + lotteryExpect + "</p></div>";
			contentStr += "			<div class='child child2 font-blue muns muns-small wauto'>";
			contentStr += "				<div class='lhc-item'><div class='mun lhc-" + colors[0] + "'>" + lotteryCode.numInfo1 + "</div><p class='lhc-item-title lhc-" + colors[0] + "'>"
					+ lotteryCodeDetailPage.returnAninal(lotteryCode.numInfo1) + "</p></div>";
			contentStr += "				<div class='lhc-item'><div class='mun lhc-" + colors[1] + "'>" + lotteryCode.numInfo2 + "</div><p class='lhc-item-title lhc-" + colors[1] + "'>"
					+ lotteryCodeDetailPage.returnAninal(lotteryCode.numInfo2) + "</p></div>";
			contentStr += "				<div class='lhc-item'><div class='mun lhc-" + colors[2] + "'>" + lotteryCode.numInfo3 + "</div><p class='lhc-item-title lhc-" + colors[2] + "'>"
					+ lotteryCodeDetailPage.returnAninal(lotteryCode.numInfo3) + "</p></div>";
			contentStr += "				<div class='lhc-item'><div class='mun lhc-" + colors[3] + "'>" + lotteryCode.numInfo4 + "</div><p class='lhc-item-title lhc-" + colors[3] + "'>"
					+ lotteryCodeDetailPage.returnAninal(lotteryCode.numInfo4) + "</p></div>";
			contentStr += "				<div class='lhc-item'><div class='mun lhc-" + colors[4] + "'>" + lotteryCode.numInfo5 + "</div><p class='lhc-item-title lhc-" + colors[4] + "'>"
					+ lotteryCodeDetailPage.returnAninal(lotteryCode.numInfo5) + "</p></div>";
			contentStr += "				<div class='lhc-item'><div class='mun lhc-" + colors[5] + "'>" + lotteryCode.numInfo6 + "</div><p class='lhc-item-title lhc-" + colors[5] + "'>"
					+ lotteryCodeDetailPage.returnAninal(lotteryCode.numInfo6) + "</p></div>";
			contentStr += "				<div class='lhc-item'><div class='mun and'>+</div><p class='lhc-item-title'>&nbsp;</p></div>";
			contentStr += "				<div class='lhc-item'><div class='mun lhc-" + colors[6] + "'>" + lotteryCode.numInfo7 + "</div><p class='lhc-item-title lhc-" + colors[6] + "'>"
					+ lotteryCodeDetailPage.returnAninal(lotteryCode.numInfo7) + "</p></div>";
			contentStr += "			</div>";
			contentStr += "		</div>";
		} else {
			contentStr += "		<div class='line'>";
			contentStr += "			<div class='child child1'><p>" + lotteryExpect + "</p></div>";
			contentStr += "			<div class='child child2 font-blue muns muns-small'>";
			contentStr += "				<div class='mun color-red'>" + lotteryCode.numInfo1 + "</div>";
			contentStr += "				<div class='mun color-red'>" + lotteryCode.numInfo2 + "</div>";
			contentStr += "				<div class='mun color-red'>" + lotteryCode.numInfo3 + "</div>";
			contentStr += "				<div class='mun color-red'>" + lotteryCode.numInfo4 + "</div>";
			contentStr += "				<div class='mun color-red'>" + lotteryCode.numInfo5 + "</div>";
			contentStr += "			</div>";
			var codeZuStatus = lotteryCodeDetailPage.getLotteryCodeZuStatus(lotteryCode.opencodeStr);
			contentStr += "		<div class='child child3' style='width:200px'><p>" + codeZuStatus + "</p></div>";
			contentStr += "		</div>";
		}

		$("#contentId_Str").append(contentStr);
		contentStr = "";

		// 隐藏加载中按钮
		$(".load-btn").hide();
	}

	// 动画效果
	if (pageNo == 1) {
		animate_add(".award_detail .main", "fadeInUp");
		animate_add(".award_detail .main-title", "fadeInUp");
		animate_add(".award_detail .user-btn", "fadeInUp");
	}
	;
};

/**
 * 获取当前开奖号码的组态显示 和值
 */
LotteryCodeDetailPage.prototype.getLotteryCodeStatus = function(codeStr) {
	// 控制显示当前号码的形态
	var codeArray = codeStr.split(",");
	var code1 = codeArray[0];
	var code2 = codeArray[1];
	var code3 = codeArray[2];
	var kshz = parseInt(code1) + parseInt(code2) + parseInt(code3);
	return kshz;
};

/**
 * 获取当前开奖号码的组态显示 组三组六
 */
LotteryCodeDetailPage.prototype.getLotteryCodeZuStatus = function(codeStr) {
	// 控制显示当前号码的形态
	var codeArray = codeStr.split(",");
	var code1 = codeArray[0];
	var code2 = codeArray[1];
	var code3 = codeArray[2];
	var code4 = codeArray[3];
	var code5 = codeArray[4];

	// 后三形态
	var codeStatus = "";
	if ((code3 == code4 && code3 != code5) || (code3 == code5 && code4 != code5) || (code4 == code5 && code3 != code4)) {
		codeStatus = "组三";
	} else if ((code3 != code4) && (code3 != code5) && (code4 != code5)) {
		codeStatus = "组六";
	} else if (code3 == code4 && code4 == code5) {
		codeStatus = "豹子";
	}

	return codeStatus;

};

/**
 * 快三展示最新的开奖号码的图片
 */
LotteryCodeDetailPage.prototype.showLastLotteryPictureCode = function(lotteryCode) {
	if (lotteryCode == null || typeof (lotteryCode) == "undefined") {
		return;
	}
	var str = "";
	if (lotteryCode.numInfo1 == "1") {
		str += "	<img class='dice' src='../images/dice_pic/gray/1.png' alt=''>";
	} else if (lotteryCode.numInfo1 == "2") {
		str += "	<img class='dice' src='../images/dice_pic/gray/2.png' alt=''>";
	} else if (lotteryCode.numInfo1 == "3") {
		str += "	<img class='dice' src='../images/dice_pic/gray/3.png' alt=''>";
	} else if (lotteryCode.numInfo1 == "4") {
		str += "	<img class='dice' src='../images/dice_pic/gray/4.png' alt=''>";
	} else if (lotteryCode.numInfo1 == "5") {
		str += "	<img class='dice' src='../images/dice_pic/gray/5.png' alt=''>";
	} else if (lotteryCode.numInfo1 == "6") {
		str += "	<img class='dice' src='../images/dice_pic/gray/6.png' alt=''>";
	}

	if (lotteryCode.numInfo2 == "1") {
		str += "	<img class='dice' src='../images/dice_pic/gray/1.png' alt=''>";
	} else if (lotteryCode.numInfo2 == "2") {
		str += "	<img class='dice' src='../images/dice_pic/gray/2.png' alt=''>";
	} else if (lotteryCode.numInfo2 == "3") {
		str += "	<img class='dice' src='../images/dice_pic/gray/3.png' alt=''>";
	} else if (lotteryCode.numInfo2 == "4") {
		str += "	<img class='dice' src='../images/dice_pic/gray/4.png' alt=''>";
	} else if (lotteryCode.numInfo2 == "5") {
		str += "	<img class='dice' src='../images/dice_pic/gray/5.png' alt=''>";
	} else if (lotteryCode.numInfo2 == "6") {
		str += "	<img class='dice' src='../images/dice_pic/gray/6.png' alt=''>";
	}

	if (lotteryCode.numInfo3 == "1") {
		str += "	<img class='dice' src='../images/dice_pic/gray/1.png' alt=''>";
	} else if (lotteryCode.numInfo3 == "2") {
		str += "	<img class='dice' src='../images/dice_pic/gray/2.png' alt=''>";
	} else if (lotteryCode.numInfo3 == "3") {
		str += "	<img class='dice' src='../images/dice_pic/gray/3.png' alt=''>";
	} else if (lotteryCode.numInfo3 == "4") {
		str += "	<img class='dice' src='../images/dice_pic/gray/4.png' alt=''>";
	} else if (lotteryCode.numInfo3 == "5") {
		str += "	<img class='dice' src='../images/dice_pic/gray/5.png' alt=''>";
	} else if (lotteryCode.numInfo3 == "6") {
		str += "	<img class='dice' src='../images/dice_pic/gray/6.png' alt=''>";
	}

	return str;
};
/**
 * 快三开奖图片显示
 */
LotteryCodeDetailPage.prototype.showHeadLastLotteryPictureCode = function(lotteryCode) {
	if (lotteryCode == null || typeof (lotteryCode) == "undefined") {
		return;
	}

	str = "";
	// 先移除开奖号码样式（更新开奖号码）
	$('#lotteryNumber1').attr("src", "");
	if (lotteryCode.numInfo1 == "1") {
		$('#lotteryNumber1').attr("src", "../images/dice_pic/gray/1.png");
	} else if (lotteryCode.numInfo1 == "2") {
		$('#lotteryNumber1').attr("src", "../images/dice_pic/gray/2.png");
	} else if (lotteryCode.numInfo1 == "3") {
		$('#lotteryNumber1').attr("src", "../images/dice_pic/gray/3.png");
	} else if (lotteryCode.numInfo1 == "4") {
		$('#lotteryNumber1').attr("src", "../images/dice_pic/gray/4.png");
	} else if (lotteryCode.numInfo1 == "5") {
		$('#lotteryNumber1').attr("src", "../images/dice_pic/gray/5.png");
	} else if (lotteryCode.numInfo1 == "6") {
		$('#lotteryNumber1').attr("src", "../images/dice_pic/gray/6.png");
	}

	// 先移除开奖号码样式（更新开奖号码）
	$('#lotteryNumber2').attr("src", "");
	if (lotteryCode.numInfo2 == "1") {
		$('#lotteryNumber2').attr("src", "../images/dice_pic/gray/1.png");
	} else if (lotteryCode.numInfo2 == "2") {
		$('#lotteryNumber2').attr("src", "../images/dice_pic/gray/2.png");
	} else if (lotteryCode.numInfo2 == "3") {
		$('#lotteryNumber2').attr("src", "../images/dice_pic/gray/3.png");
	} else if (lotteryCode.numInfo2 == "4") {
		$('#lotteryNumber2').attr("src", "../images/dice_pic/gray/4.png");
	} else if (lotteryCode.numInfo2 == "5") {
		$('#lotteryNumber2').attr("src", "../images/dice_pic/gray/5.png");
	} else if (lotteryCode.numInfo2 == "6") {
		$('#lotteryNumber2').attr("src", "../images/dice_pic/gray/6.png");
	}

	// 先移除开奖号码样式（更新开奖号码）
	$('#lotteryNumber3').attr("src", "");
	if (lotteryCode.numInfo3 == "1") {
		$('#lotteryNumber3').attr("src", "../images/dice_pic/gray/1.png");
	} else if (lotteryCode.numInfo3 == "2") {
		$('#lotteryNumber3').attr("src", "../images/dice_pic/gray/2.png");
	} else if (lotteryCode.numInfo3 == "3") {
		$('#lotteryNumber3').attr("src", "../images/dice_pic/gray/3.png");
	} else if (lotteryCode.numInfo3 == "4") {
		$('#lotteryNumber3').attr("src", "../images/dice_pic/gray/4.png");
	} else if (lotteryCode.numInfo3 == "5") {
		$('#lotteryNumber3').attr("src", "../images/dice_pic/gray/5.png");
	} else if (lotteryCode.numInfo3 == "6") {
		$('#lotteryNumber3').attr("src", "../images/dice_pic/gray/6.png");
	}
};

/**
 * 根据彩种显示彩种图片
 */
LotteryCodeDetailPage.prototype.showLogoImg = function(lotteryKind) {
	if (lotteryKind == null || typeof (lotteryKind) == "undefined") {
		return;
	}

	if (lotteryKind == "CQSSC") {
		$("#lotterKindId").html("重庆时时彩");
		$("#logoId").attr('src', '../images/lottery_logo/hall/logo_ssc.png');
	} else if (lotteryKind == "TJSSC") {
		$("#lotterKindId").html("天津时时彩");
		$("#logoId").attr('src', '../images/lottery_logo/hall/logo_ssc.png');
	} else if (lotteryKind == "XJSSC") {
		$("#lotterKindId").html("新疆时时彩");
		$("#logoId").attr('src', '../images/lottery_logo/hall/logo_ssc.png');
	} else if (lotteryKind == "JXSSC") {
		$("#lotterKindId").html("江西时时彩");
		$("#logoId").attr('src', '../images/lottery_logo/hall/logo_ssc.png');
	} else if (lotteryKind == "HLJSSC") {
		$("#lotterKindId").html("黑龙江时时彩");
		$("#logoId").attr('src', '../images/lottery_logo/hall/logo_ssc.png');
	} else if (lotteryKind == "JLFFC") {
		$("#lotterKindId").html("幸运分分彩");
		$("#logoId").attr('src', '../images/lottery_logo/hall/logo_fenfencai.png');
	} else if (lotteryKind == "TWWFC") {
		$("#lotterKindId").html("台湾5分彩");
		$("#logoId").attr('src', '../images/lottery_logo/hall/logo_wufencai.png');
	} else if (lotteryKind == "XJPLFC") {
		$("#lotterKindId").html("新加坡2分彩");
		$("#logoId").attr('src', '../images/lottery_logo/hall/logo_erfencai.png');
	} else if (lotteryKind == "HGFFC") {
		$("#lotterKindId").html("韩国1.5分彩");
		$("#logoId").attr('src', '../images/lottery_logo/hall/logo_yidianwufencai.png');
	} else if (lotteryKind == "BJKS") {
		$("#lotterKindId").html("北京快三");
		$("#logoId").attr('src', '../images/lottery_logo/hall/logo_kuai3.png');
	} else if (lotteryKind == "AHKS") {
		$("#lotterKindId").html("安徽快三");
		$("#logoId").attr('src', '../images/lottery_logo/hall/logo_kuai3.png');
	} else if (lotteryKind == "JLKS") {
		$("#lotterKindId").html("吉林快三");
		$("#logoId").attr('src', '../images/lottery_logo/hall/logo_kuai3.png');
	} else if (lotteryKind == "JSKS") {
		$("#logoId").attr('src', '../images/lottery_logo/hall/logo_kuai3.png');
		$("#lotterKindId").html("江苏快三");
	} else if (lotteryKind == "HBKS") {
		$("#logoId").attr('src', '../images/lottery_logo/hall/logo_kuai3.png');
		$("#lotterKindId").html("湖北快三");
	} else if (lotteryKind == "JYKS") {
		$("#logoId").attr('src', '../images/lottery_logo/hall/logo_kuai3.png');
		$("#lotterKindId").html("幸运快三");
	} else if (lotteryKind == "FJKS") {
		$("#logoId").attr('src', '../images/lottery_logo/hall/logo_kuai3.png');
		$("#lotterKindId").html("福建快三");
	} else if (lotteryKind == "GXKS") {
		$("#logoId").attr('src', '../images/lottery_logo/hall/logo_kuai3.png');
		$("#lotterKindId").html("广西快三");
	} else if (lotteryKind == "GSKS") {
		$("#logoId").attr('src', '../images/lottery_logo/hall/logo_kuai3.png');
		$("#lotterKindId").html("甘肃快三");
	} else if (lotteryKind == "SHKS") {
		$("#logoId").attr('src', '../images/lottery_logo/hall/logo_kuai3.png');
		$("#lotterKindId").html("上海快三");
	} else if (lotteryKind == "BJPK10") {
		$("#lotterKindId").html("北京PK10");
		$("#logoId").attr('src', '../images/lottery_logo/hall/logo_pk10.png');
	} else if (lotteryKind == "JSPK10") {
		$("#lotterKindId").html("极速PK10");
		$("#logoId").attr('src', '../images/lottery_logo/hall/logo_jspk10.png');
	} else if (lotteryKind == "FCSDDPC") {
		$("#lotterKindId").html("福彩3D");
		$("#logoId").attr('src', '../images/lottery_logo/hall/logo_3d.png');
	} else if (lotteryKind == "LHC") {
		$("#lotterKindId").html("六合彩");
		$("#logoId").attr('src', '../images/lottery_logo/hall/logo_liuhecai.png');
	} else if (lotteryKind == "FJSYXW") {
		$("#lotterKindId").html("福建11选5");
		$("#logoId").attr('src', 'images/lottery_logo/hall/logo_11xuan5.png');
	} else if (lotteryKind == "JXSYXW") {
		$("#logoId").attr('src', '../images/lottery_logo/hall/logo_11xuan5.png');
	} else if (lotteryKind == "SDSYXW") {
		$("#lotterKindId").html("山东11选5");
		$("#logoId").attr('src', '../images/lottery_logo/hall/logo_11xuan5.png');
	} else if (lotteryKind == "GDSYXW") {
		$("#lotterKindId").html("广东11选5");
		$("#logoId").attr('src', '../images/lottery_logo/hall/logo_11xuan5.png');
	} else if (lotteryKind == "CQSYXW") {
		$("#lotterKindId").html("重庆11选5");
		$("#logoId").attr('src', '../images/lottery_logo/hall/logo_11xuan5.png');
	} else {
		$("#logoId").attr('src', '');
	}
};

// 六合彩参数
lotteryCodeDetailPage.shengxiao = {
	shengxiaoKey : new Array('鼠', '牛', '虎', '兔', '龙', '蛇', '马', '羊', '猴', '鸡', '狗', '猪'),
	shengxiaoArr : []
}

// 波色
lotteryCodeDetailPage.boshe = {
	hongbo : [ 1, 2, 7, 8, 12, 13, 18, 19, 23, 24, 29, 30, 34, 35, 40, 45, 46 ],
	lanbo : [ 3, 4, 9, 10, 14, 15, 20, 25, 26, 31, 36, 37, 41, 42, 47, 48 ],
	lvbo : [ 5, 6, 11, 16, 17, 21, 22, 27, 28, 32, 33, 38, 39, 43, 44, 49 ]
}


LotteryCodeDetailPage.prototype.returnAninal = function(opencode) {
	var shengxiaoArr = lotteryCodeDetailPage.shengxiao.shengxiaoArr;
	for (var i = 0; i < shengxiaoArr.length; i++) {
		for (var j = 0; j < shengxiaoArr[i].length; j++) {
			if (Number(opencode) == Number(shengxiaoArr[i][j])) {
				return lotteryCodeDetailPage.shengxiao.shengxiaoKey[i];
			}
		}
	}
	return "-";
}

/**
 * 初始化生肖号码描述
 */
LotteryCodeDetailPage.prototype.initShengXiaoNumber = function() {
	$.ajax({
		type : "POST",
		url : contextPath + "/code/getShengXiaoNumber",
		contentType : 'application/json;charset=utf-8',// 设置请求头信息;
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				lotteryCodeDetailPage.shengxiao.shengxiaoArr = result.data;

			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
				$("#editUserButton").removeAttr("disabled");
			}
		}
	});
}

/**
 * 计算颜色
 * @param lotteryCode
 * @returns {Array}
 */
LotteryCodeDetailPage.prototype.changeColorByCode = function(lotteryCode) {
	var colors = new Array();
	var hong = lotteryCodeDetailPage.boshe.hongbo;
	var lan = lotteryCodeDetailPage.boshe.lanbo;
	var lv = lotteryCodeDetailPage.boshe.lvbo;

	// 判断是否是红波
	for (var i = 0; i < hong.length; i++) {
		if (Number(hong[i]) == Number(lotteryCode.numInfo1)) {
			colors[0] = 'red';
		}
		if (Number(hong[i]) == Number(lotteryCode.numInfo2)) {
			colors[1] = 'red';
		}
		if (Number(hong[i]) == Number(lotteryCode.numInfo3)) {
			colors[2] = 'red';
		}
		if (Number(hong[i]) == Number(lotteryCode.numInfo4)) {
			colors[3] = 'red';
		}
		if (Number(hong[i]) == Number(lotteryCode.numInfo5)) {
			colors[4] = 'red';
		}
		if (Number(hong[i]) == Number(lotteryCode.numInfo6)) {
			colors[5] = 'red';
		}
		if (Number(hong[i]) == Number(lotteryCode.numInfo7)) {
			colors[6] = 'red';
		}
	}

	// 判断是否是蓝波
	for (var i = 0; i < lan.length; i++) {
		if (Number(lan[i]) == Number(lotteryCode.numInfo1)) {
			colors[0] = 'blue';
		}
		if (Number(lan[i]) == Number(lotteryCode.numInfo2)) {
			colors[1] = 'blue';
		}
		if (Number(lan[i]) == Number(lotteryCode.numInfo3)) {
			colors[2] = 'blue';
		}
		if (Number(lan[i]) == Number(lotteryCode.numInfo4)) {
			colors[3] = 'blue';
		}
		if (Number(lan[i]) == Number(lotteryCode.numInfo5)) {
			colors[4] = 'blue';
		}
		if (Number(lan[i]) == Number(lotteryCode.numInfo6)) {
			colors[5] = 'blue';
		}
		if (Number(lan[i]) == Number(lotteryCode.numInfo7)) {
			colors[6] = 'blue';
		}
	}

	// 判断是否是绿波
	for (var i = 0; i < lv.length; i++) {
		if (Number(lv[i]) == Number(lotteryCode.numInfo1)) {
			colors[0] = 'green';
		}
		if (Number(lv[i]) == Number(lotteryCode.numInfo2)) {
			colors[1] = 'green';
		}
		if (Number(lv[i]) == Number(lotteryCode.numInfo3)) {
			colors[2] = 'green';
		}
		if (Number(lv[i]) == Number(lotteryCode.numInfo4)) {
			colors[3] = 'green';
		}
		if (Number(lv[i]) == Number(lotteryCode.numInfo5)) {
			colors[4] = 'green';
		}
		if (Number(lv[i]) == Number(lotteryCode.numInfo6)) {
			colors[5] = 'green';
		}
		if (Number(lv[i]) == Number(lotteryCode.numInfo7)) {
			colors[6] = 'green';
		}
	}

	return colors;
}
