function LotteryCodePage() {
	
}

var lotteryCodePage= new LotteryCodePage();

lotteryCodePage.param = {

};

$(document).ready(function(){
	$(".i-discover").parent().addClass("on");
	 setTimeout("lotteryCodePage.getAllLotteryCode()", 1);
	 lotteryCodePage.initShengXiaoNumber();
});


LotteryCodePage.prototype.getAllLotteryCode = function(){
	
	$.ajax({
		type : "POST",
		url : contextPath + "/code/getLotteryCodeList",
		contentType : 'application/json;charset=utf-8',//设置请求头信息;
		success : function(result) {
			commonHandlerResult(result, this.url);
			if(result.code == "ok"){
				lotteryCodePage.refreshLotteryCode(result.data);
			}else if(result.code == "error"){
				showTip(result.data);
			}
		}
	});
};

LotteryCodePage.prototype.refreshLotteryCode = function(lotteryOpenMap){
	var ww="";
	var index = 0;
	for(var key in lotteryOpenMap){
		var lotteryCodeDes = lotteryOpenMap[key];
		if(key == "XJPLFC" || key == "HGFFC"){
			continue;
		}
		index ++;
		  ww += lotteryCodePage.refreshHead(key,lotteryCodeDes ,index);
	}
	$("#lotteryList").html(ww);
};


LotteryCodePage.prototype.refreshHead = function(lotteryKind,lotteryCodeList,index){
	var str="";
	var lotteryCodeHead = lotteryCodeList[0];
	if(typeof(lotteryCodeHead) != "undefined"){
		if(lotteryKind == "BJPK10" || lotteryKind == "JSPK10"){
			str += "<div class='main award-pk10'><div class='container'>";
		}else if(lotteryKind.indexOf("KS") > 0){
			str += "<div class='main award-kuaisan'><div class='container'>";
		}else{
			str += "<div class='main award-ssc'><div class='container'>";
		}
		str += "	<div class='head'>";
		//彩种标题
		str += lotteryCodePage.showtitle(lotteryKind);
		
		var lotteryExpect = frontCommonPage.getLotteryExpect(lotteryKind,lotteryCodeHead.lotteryNum);
		str += "	<div class='child periods'>第"+ lotteryExpect +"期</div>";
		if(index > 17){
			str += "	<a href='javascript:showTip(\"本彩种正在开发中，尽情期待...\")'><div class='child bet'>立即投注 ></div></a>";
		}else{
			//str += "	<a href='#/lottery/"+lotteryKind.toString().toLowerCase() +"'><div class='child bet'>立即投注 ></div></a>";
			var passUrl="";
			if(lotteryKind == "BJPK10"){
				passUrl= contextPath +"/gameBet/lottery/bjpks/" +lotteryKind.toString().toLowerCase()+".html";
			}else if(lotteryKind == "JSPK10"){
				passUrl= contextPath +"/gameBet/lottery/jspks/" +lotteryKind.toString().toLowerCase()+".html";
			}else if(lotteryKind == "JLFFC"){
				passUrl= contextPath +"/gameBet/lottery/xyffc/xyffc.html";
			}else{
				passUrl = contextPath +"/gameBet/lottery/"+lotteryKind.toString().toLowerCase()+"/" +lotteryKind.toString().toLowerCase()+".html";
			}
			var content="需要登录才能进行投注，是否登录？";
			str += "	<a href='javascript:judgeCurrentUserToLogin("+"\""+passUrl+"\"\,\""+content+"\")'><div class='child bet'>立即投注 ></div></a>";
		}
		str += "</div>";
		str += "<a href='"+ contextPath +"/gameBet/lotterycodeDetail.html?lotteryKindName="+ lotteryKind +"'>";
		str += "<div class='muns'>";
		if(lotteryKind.indexOf("KS") > 0){
			str += lotteryCodePage.showLastLotteryPictureCode(lotteryCodeHead);
		}else if(lotteryKind == "BJPK10" || lotteryKind == "JSPK10"){
			str += "	<div class='mun color-red'>"+ lotteryCodeHead.numInfo1 +"</div>";
			str += "	<div class='mun color-red'>"+ lotteryCodeHead.numInfo2 +"</div>";	
			str += "	<div class='mun color-red'>"+ lotteryCodeHead.numInfo3 +"</div>";	
			str += "	<div class='mun color-red'>"+ lotteryCodeHead.numInfo4 +"</div>";	
			str += "	<div class='mun color-red'>"+ lotteryCodeHead.numInfo5 +"</div>";
			str += "	<div class='mun color-red'>"+ lotteryCodeHead.numInfo6 +"</div>";
			str += "	<div class='mun color-red'>"+ lotteryCodeHead.numInfo7 +"</div>";	
			str += "	<div class='mun color-red'>"+ lotteryCodeHead.numInfo8 +"</div>";	
			str += "	<div class='mun color-red'>"+ lotteryCodeHead.numInfo9 +"</div>";	
			str += "	<div class='mun color-red'>"+ lotteryCodeHead.numInfo10 +"</div>";
		}else if (lotteryKind == "LHC"){
			var  colors = lotteryCodePage.changeColorByCode(lotteryCodeHead);
			str += "				<div class='lhc-item'><div class='mun lhc-"+colors[0]+"'>"+ lotteryCodeHead.numInfo1 +"</div><p class='lhc-item-title lhc-"+colors[0]+"'>"+lotteryCodePage.returnAninal(lotteryCodeHead.numInfo1)+"</p></div>";
			str += "				<div class='lhc-item'><div class='mun lhc-"+colors[1]+"'>"+ lotteryCodeHead.numInfo2 +"</div><p class='lhc-item-title lhc-"+colors[1]+"'>"+lotteryCodePage.returnAninal(lotteryCodeHead.numInfo2)+"</p></div>";
			str += "				<div class='lhc-item'><div class='mun lhc-"+colors[2]+"'>"+ lotteryCodeHead.numInfo3 +"</div><p class='lhc-item-title lhc-"+colors[2]+"'>"+lotteryCodePage.returnAninal(lotteryCodeHead.numInfo3)+"</p></div>";
			str += "				<div class='lhc-item'><div class='mun lhc-"+colors[3]+"'>"+ lotteryCodeHead.numInfo4 +"</div><p class='lhc-item-title lhc-"+colors[3]+"'>"+lotteryCodePage.returnAninal(lotteryCodeHead.numInfo4)+"</p></div>";
			str += "				<div class='lhc-item'><div class='mun lhc-"+colors[4]+"'>"+ lotteryCodeHead.numInfo5 +"</div><p class='lhc-item-title lhc-"+colors[4]+"'>"+lotteryCodePage.returnAninal(lotteryCodeHead.numInfo5)+"</p></div>";
			str += "				<div class='lhc-item'><div class='mun lhc-"+colors[5]+"'>"+ lotteryCodeHead.numInfo6 +"</div><p class='lhc-item-title lhc-"+colors[5]+"'>"+lotteryCodePage.returnAninal(lotteryCodeHead.numInfo6)+"</p></div>";
			str += "				<div class='lhc-item'><div class='mun and'>+</div><p class='lhc-item-title'>&nbsp;</p></div>";
			str += "				<div class='lhc-item'><div class='mun lhc-"+colors[6]+"'>"+ lotteryCodeHead.numInfo7 +"</div><p class='lhc-item-title lhc-"+colors[6]+"'>"+lotteryCodePage.returnAninal(lotteryCodeHead.numInfo7)+"</p></div>";
		}else{
			str += "	<div class='mun color-red'>"+ lotteryCodeHead.numInfo1 +"</div>";
			str += "	<div class='mun color-red'>"+ lotteryCodeHead.numInfo2 +"</div>";	
			str += "	<div class='mun color-red'>"+ lotteryCodeHead.numInfo3 +"</div>";	
			str += "	<div class='mun color-red'>"+ lotteryCodeHead.numInfo4 +"</div>";	
			str += "	<div class='mun color-red'>"+ lotteryCodeHead.numInfo5 +"</div>";
		}
		str += "</div>";
		
		str += " <div class='lottery-dynamic'>";
		str += "	<div class='titles'>";
		if(lotteryKind.indexOf("KS") > 0){
			str += "		<div class='child child1'><p>期号</p></div>";
			str += "		<div class='child child2'><p>开奖号</p></div>";
			str += "		<div class='child child3'><p>和值</p></div>";
			str += "		<div class='child child4'><p>形态</p></div>";
		}else if(lotteryKind == "BJPK10" || lotteryKind == "JSPK10" || lotteryKind == "XJPLFC"){
			str += "		<div class='child child1'><p>期号</p></div>";
			str += "		<div class='child child2'><p>开奖号</p></div>";
		}else if(lotteryKind == "LHC"){
			str += "		<div class='child child1' style='width:240px'><p>期号</p></div>";
			str += "		<div class='child child2' style='width:556px'><p>开奖号</p></div>";
			str += "		<div class='child child3' style='width:204px'><p>特码</p></div>";
		}else{
			str += "		<div class='child child1'><p>期号</p></div>";
			str += "		<div class='child child2'><p>开奖号</p></div>";
			str += "		<div class='child child3'><p>后三组态</p></div>";
		}
		str += "	</div>";
		str += "	<div class='content'>";
		if(lotteryCodeList.length > 0 && lotteryCodeList.length <= 1){
			str += "</div>";
			str += "</div>";
			str += "</a></div></div>";
		}
	}else{
		str +="<div class='main award-ssc'><div class='container'>";
		str += "<div class='head'>";
		//彩种标题
		 var showtitle = lotteryCodePage.showtitle(lotteryKind);
		str += "	<div class='child title'>"+ showtitle +"</div>";
		str += "	<div class='child periods'>第 ----期</div>";
		if(index > 17){
			str += "	<a href='javascript:showTip(\"本彩种正在开发中，尽情期待...\")' ><div class='child bet'>立即投注 ></div></a>";
		}else{
			var passUrl="";
			if(lotteryKind == "BJPK10" ){
				passUrl= contextPath +"/gameBet/lottery/bjpks/" +lotteryKind.toString().toLowerCase()+".html";
			}else if ( lotteryKind == "JSPK10" ){
				passUrl= contextPath +"/gameBet/lottery/jspks/" +lotteryKind.toString().toLowerCase()+".html";
			}else if(lotteryKind == "JLFFC"){
				passUrl= contextPath +"/gameBet/lottery/xyffc/xyffc.html";
			}else{
				passUrl = contextPath +"/gameBet/lottery/"+lotteryKind.toString().toLowerCase()+"/" +lotteryKind.toString().toLowerCase()+".html";
			}
			var content="需要登录才能进行投注，是否登录？";
			str += "	<a href='javascript:judgeCurrentUserToLogin("+"\""+passUrl+"\"\,\""+content+"\")'><div class='child bet'>立即投注 ></div></a>";
		}
		str += "</div>";
		str += "<a href='javascript:void(0)'>";
		str += "<div class='muns'>";
		str += "	<div class='mun color-red'>-</div>";
		str += "	<div class='mun color-red'>-</div>";	
		str += "	<div class='mun color-red'>-</div>";	
		str += "	<div class='mun color-red'>-</div>";	
		str += "	<div class='mun color-red'>-</div>";	
		str += "</div>";
		str += "</a></div></div>";
	}

	
	
	for(var i=1; i< 3;i++){
		var lotteryCode = lotteryCodeList[i];
		if(typeof(lotteryCode) == "undefined"){
			break;
		}
		var lotteryExpect = frontCommonPage.getLotteryExpect(lotteryKind,lotteryCode.lotteryNum);
		
		if(lotteryKind == "BJPK10" || lotteryKind == "JSPK10"){
			str += "		<div class='line'>";
			str += "			<div class='child child1'><p>"+ lotteryExpect +"</p></div>";
			str += "			<div class='child child2 font-blue muns muns-small'>";
			str += "				<div class='mun color-red'>"+ lotteryCode.numInfo1 +"</div>";
			str += "				<div class='mun color-red'>"+ lotteryCode.numInfo2 +"</div>";
			str += "				<div class='mun color-red'>"+ lotteryCode.numInfo3 +"</div>";
			str += "				<div class='mun color-red'>"+ lotteryCode.numInfo4 +"</div>";
			str += "				<div class='mun color-red'>"+ lotteryCode.numInfo5 +"</div>";
			str += "				<div class='mun color-red'>"+ lotteryCode.numInfo6 +"</div>";
			str += "				<div class='mun color-red'>"+ lotteryCode.numInfo7 +"</div>";
			str += "				<div class='mun color-red'>"+ lotteryCode.numInfo8 +"</div>";
			str += "				<div class='mun color-red'>"+ lotteryCode.numInfo9 +"</div>";
			str += "				<div class='mun color-red'>"+ lotteryCode.numInfo10 +"</div>";
			str += "		</div>";
			str += "	</div>";
		}else if(lotteryKind.indexOf("KS") > 0){
			str += "		<div class='line'>";
			str += "			<div class='child child1'><p>"+ lotteryExpect +"</p></div>";
			str += "	<div class='child child2 font-blue'>";
			
			str += lotteryCodePage.showLastLotteryPictureCode(lotteryCode);
			
			str += "	</div>";
			//和值
			var codeStatus = lotteryCodePage.getLotteryCodeStatus(lotteryCode.opencodeStr);
			str += "	<div class='child child3'><p>"+ codeStatus +"</p></div>";
			str += "	<div class='child child4'>";
			 //获取当前开奖号码的和值组态显示 大小
			  if(codeStatus <= 10){
				  	str += "	<span class='child-small'>小</span> ";
			    }else{
			    	str += "	<span class='child-big'>大</span> ";
			    }
			  
			  //获取当前开奖号码的和值组态显示 单双
			   if(codeStatus % 2 == 0) {
				   str +=	"| <span class='child-odd'>双</span></div>";
			   } else {
				   str +=	"| <span class='child-even'>单</span></div>";
			   }
			  
			str += "</div>";
			
		}else if(lotteryKind == "XJPLFC"){
			str += "		<div class='line'>";
			str += "			<div class='child child1'><p>"+ lotteryExpect +"</p></div>";
			str += "			<div class='child child2 font-blue muns muns-small'>";
			str += "				<div class='mun color-red'>"+ lotteryCode.numInfo1 +"</div>";
			str += "				<div class='mun color-red'>"+ lotteryCode.numInfo2 +"</div>";
			str += "				<div class='mun color-red'>"+ lotteryCode.numInfo3 +"</div>";
			str += "				<div class='mun color-red'>"+ lotteryCode.numInfo4 +"</div>";
			str += "				<div class='mun color-red'>"+ lotteryCode.numInfo5 +"</div>";
			str += "		</div>";
			str += "	</div>";
		}else if(lotteryKind == "LHC"){
			var  colors = lotteryCodePage.changeColorByCode(lotteryCode);
			str += "		<div class='line'>";
			str += "			<div class='child child1'><p>"+ lotteryExpect +"</p></div>";
			str += "			<div class='child child2 font-blue muns muns-small' style='width:676px'>";
			str += "				<div class='mun lhc-"+colors[0]+"'>"+ lotteryCode.numInfo1 +"</div>";
			str += "				<div class='mun lhc-"+colors[1]+"'>"+ lotteryCode.numInfo2 +"</div>";
			str += "				<div class='mun lhc-"+colors[2]+"'>"+ lotteryCode.numInfo3 +"</div>";
			str += "				<div class='mun lhc-"+colors[3]+"'>"+ lotteryCode.numInfo4 +"</div>";
			str += "				<div class='mun lhc-"+colors[4]+"'>"+ lotteryCode.numInfo5 +"</div>";
			str += "				<div class='mun lhc-"+colors[5]+"'>"+ lotteryCode.numInfo6 +"</div>";
			str += "				<span class='mun and'>+</span>";
			str += "				<div class='mun lhc-"+colors[6]+"'>"+ lotteryCode.numInfo7 +"</div>";
			str += "		</div>";
			str += "	</div>";
		}else{
			str += "		<div class='line'>";
			str += "			<div class='child child1'><p>"+ lotteryExpect +"</p></div>";
			str += "			<div class='child child2 font-blue muns muns-small'>";
			str += "				<div class='mun color-red'>"+ lotteryCode.numInfo1 +"</div>";
			str += "				<div class='mun color-red'>"+ lotteryCode.numInfo2 +"</div>";
			str += "				<div class='mun color-red'>"+ lotteryCode.numInfo3 +"</div>";
			str += "				<div class='mun color-red'>"+ lotteryCode.numInfo4 +"</div>";
			str += "				<div class='mun color-red'>"+ lotteryCode.numInfo5 +"</div>";
			str += "		</div>";
			var lotteryCodeZuStatus = lotteryCodePage.getLotteryCodeZuStatus(lotteryCode.opencodeStr);
			str += "		<div class='child child3'><p>"+ lotteryCodeZuStatus +"</p></div>";
			str += "	</div>";
		}
	}	
		str += "	</div>";
		str += " </div>";
		str += "</a></div></div>";
	return str;
};

/**
 * 根据彩种显示彩种标题
 */
LotteryCodePage.prototype.showtitle = function(lotteryKind){
	if(lotteryKind == null || typeof(lotteryKind)=="undefined"){
		return;
	}
	
	var str = "";
	if(lotteryKind == "CQSSC"){
		str += "	<div class='child title'>重庆时时彩</div>";
	}else if(lotteryKind == "TJSSC"){
		str += "	<div class='child title'>天津时时彩</div>";
	}else if(lotteryKind == "XJSSC"){
		str += "	<div class='child title'>新疆时时彩</div>";
	}else if(lotteryKind == "JXSSC"){
		str += "	<div class='child title'>江西时时彩</div>";
	}else if(lotteryKind == "HLJSSC"){
		str += "	<div class='child title'>黑龙江时时彩</div>";
	}else if(lotteryKind == "JLFFC"){
		str += "	<div class='child title'>幸运分分彩</div>";
	}else if(lotteryKind == "TWWFC"){
		str += "	<div class='child title'>台湾5分彩</div>";
	}else if(lotteryKind == "XJPLFC"){
		str += "	<div class='child title'>新加坡2分彩</div>";
	}else if(lotteryKind == "HGFFC"){
		str += "	<div class='child title'>韩国1.5分彩</div>";
	}else if(lotteryKind == "BJKS"){
		str += "	<div class='child title'>北京快三</div>";
	}else if(lotteryKind == "AHKS"){
		str += "	<div class='child title'>安徽快三</div>";
	}else if(lotteryKind == "JLKS"){
		str += "	<div class='child title'>吉林快三</div>";
	}else if(lotteryKind == "JSKS"){
		str += "	<div class='child title'>江苏快三</div>";
	}else if(lotteryKind == "HBKS"){
		str += "	<div class='child title'>湖北快三</div>";
	}else if(lotteryKind == "JYKS"){
		str += "	<div class='child title'>幸运快三</div>";
	}else if(lotteryKind == "FJKS"){
		str += "	<div class='child title'>福建快三</div>";
	}else if(lotteryKind == "GXKS"){
		str += "	<div class='child title'>广西快三</div>";
	}else if(lotteryKind == "GSKS"){
		str += "	<div class='child title'>甘肃快三</div>";
	}else if(lotteryKind == "SHKS"){
		str += "	<div class='child title'>上海快三</div>";
	}else if(lotteryKind == "BJPK10"){
		str += "	<div class='child title'>北京PK10</div>";
	}else if(lotteryKind == "JSPK10"){
		str += "	<div class='child title'>极速PK10</div>";
	}else if(lotteryKind == "FCSDDPC"){
		str += "	<div class='child title'>福彩3D</div>";
	}else if(lotteryKind == "FJSYXW"){
		str += "	<div class='child title'>福建11选5</div>";
	}else if(lotteryKind == "JXSYXW"){
		str += "	<div class='child title'>江西11选5</div>";
	}else if(lotteryKind == "SDSYXW"){
		str += "	<div class='child title'>山东11选5</div>";
	}else if(lotteryKind == "GDSYXW"){
		str += "	<div class='child title'>广东11选5</div>";
	}else if(lotteryKind == "CQSYXW"){
		str += "	<div class='child title'>重庆11选5</div>";
	}else if(lotteryKind == "GDKLSF"){
		str += "	<div class='child title'>广东快乐十分</div>";
	}else if(lotteryKind == "SHSSLDPC"){
		str += "	<div class='child title'>上海时时乐</div>";
	}else if(lotteryKind == "GDKLSF"){
		str += "	<div class='child title'>广东快乐十分</div>";
	}else if(lotteryKind == "LHC"){
		str += "	<div class='child title'>香港六合彩</div>";
	}else{
		str += "	<div class='child title'>待开发中...</div>";
	}
	return str;
};
/**
 * 获取当前开奖号码的组态显示 和值
 */
LotteryCodePage.prototype.getLotteryCodeStatus = function(codeStr){
	//控制显示当前号码的形态
    var codeArray = codeStr.split(",");
    var code1 = codeArray[0];
    var code2 = codeArray[1];
    var code3 = codeArray[2];
    var kshz = parseInt(code1) + parseInt(code2) + parseInt(code3);
    return kshz;
};

/**
 * 获取当前开奖号码的组态显示 组三组六
 */
LotteryCodePage.prototype.getLotteryCodeZuStatus = function(codeStr){
	//控制显示当前号码的形态
    var codeArray = codeStr.split(",");
    var code1 = codeArray[0];
    var code2 = codeArray[1];
    var code3 = codeArray[2];
    var code4 = codeArray[3];
    var code5 = codeArray[4];
    
    //后三形态
    var codeStatus = "";
    if((code3 == code4 && code3 != code5) || (code3 == code5 && code4 != code5) || (code4 == code5 && code3 != code4)){
    	codeStatus = "组三";
    }else if((code3 != code4) && (code3 != code5) && (code4 != code5)){
    	codeStatus = "组六";
    }else if(code3 == code4 && code4 == code5){
    	codeStatus = "豹子";
    }    
    
    return codeStatus;
        
};

/**
 * 快三展示最新的开奖号码的图片
 */
LotteryCodePage.prototype.showLastLotteryPictureCode = function(lotteryCode){
	if(lotteryCode == null || typeof(lotteryCode)=="undefined"){
		return;
	}
	var str ="";
	if(lotteryCode.numInfo1 == "1"){
		str += "	<img class='dice' src='../images/dice_pic/gray/1.png' alt=''>";
	}else if(lotteryCode.numInfo1 == "2"){
		str += "	<img class='dice' src='../images/dice_pic/gray/2.png' alt=''>";
	}else if(lotteryCode.numInfo1 == "3"){
		str += "	<img class='dice' src='../images/dice_pic/gray/3.png' alt=''>";
	}else if(lotteryCode.numInfo1 == "4"){
		str += "	<img class='dice' src='../images/dice_pic/gray/4.png' alt=''>";
	}else if(lotteryCode.numInfo1 == "5"){
		str += "	<img class='dice' src='../images/dice_pic/gray/5.png' alt=''>";
	}else if(lotteryCode.numInfo1 == "6"){
		str += "	<img class='dice' src='../images/dice_pic/gray/6.png' alt=''>";
	}
	
	if(lotteryCode.numInfo2 == "1"){
		str += "	<img class='dice' src='../images/dice_pic/gray/1.png' alt=''>";
	}else if(lotteryCode.numInfo2 == "2"){
		str += "	<img class='dice' src='../images/dice_pic/gray/2.png' alt=''>";
	}else if(lotteryCode.numInfo2 == "3"){
		str += "	<img class='dice' src='../images/dice_pic/gray/3.png' alt=''>";
	}else if(lotteryCode.numInfo2 == "4"){
		str += "	<img class='dice' src='../images/dice_pic/gray/4.png' alt=''>";
	}else if(lotteryCode.numInfo2 == "5"){
		str += "	<img class='dice' src='../images/dice_pic/gray/5.png' alt=''>";
	}else if(lotteryCode.numInfo2 == "6"){
		str += "	<img class='dice' src='../images/dice_pic/gray/6.png' alt=''>";
	}
	
	if(lotteryCode.numInfo3 == "1"){
		str += "	<img class='dice' src='../images/dice_pic/gray/1.png' alt=''>";
	}else if(lotteryCode.numInfo3 == "2"){
		str += "	<img class='dice' src='../images/dice_pic/gray/2.png' alt=''>";
	}else if(lotteryCode.numInfo3 == "3"){
		str += "	<img class='dice' src='../images/dice_pic/gray/3.png' alt=''>";
	}else if(lotteryCode.numInfo3 == "4"){
		str += "	<img class='dice' src='../images/dice_pic/gray/4.png' alt=''>";
	}else if(lotteryCode.numInfo3 == "5"){
		str += "	<img class='dice' src='../images/dice_pic/gray/5.png' alt=''>";
	}else if(lotteryCode.numInfo3 == "6"){
		str += "	<img class='dice' src='../images/dice_pic/gray/6.png' alt=''>";
	}
	
	return str;
};

//六合彩参数
lotteryCodePage.shengxiao = {
		shengxiaoKey:new Array('鼠','牛','虎','兔','龙','蛇','马','羊','猴','鸡','狗','猪'),
	    shengxiaoArr: []
}
//波色
lotteryCodePage.boshe = {
		hongbo:[1,2,7,8,12,13,18,19,23,24,29,30,34,35,40,45,46],
		lanbo:[3,4,9,10,14,15,20,25,26,31,36,37,41,42,47,48],
		lvbo:[5,6,11,16,17,21,22,27,28,32,33,38,39,43,44,49]
}


LotteryCodePage.prototype.returnAninal = function(opencode){
	var shengxiaoArr =lotteryCodePage.shengxiao.shengxiaoArr;
	for(var i=0;i<shengxiaoArr.length;i++){		
		for(var j=0;j<shengxiaoArr[i].length;j++){
			if(Number(opencode) == Number(shengxiaoArr[i][j])){
				return lotteryCodePage.shengxiao.shengxiaoKey[i];
			}
		}
	}
	return "-";
}

/**
 * 初始化生肖号码描述
 */
LotteryCodePage.prototype.initShengXiaoNumber = function (){
	
	$.ajax({
		type : "POST",
		url : contextPath + "/code/getShengXiaoNumber",
		contentType : 'application/json;charset=utf-8',//设置请求头信息;
		success : function(result) {
			commonHandlerResult(result, this.url);
			if(result.code == "ok"){
				lotteryCodePage.shengxiao.shengxiaoArr = result.data;
				//获取最新的开奖号码
				lotteryCodePage.getAllLotteryCode();
			}else if(result.code == "error"){
				frontCommonPage.showKindlyReminder(result.data);
				$("#editUserButton").removeAttr("disabled");
			}
		}
	});
}

//计算颜色
LotteryCodePage.prototype.changeColorByCode = function(lotteryCode){
	var colors = new Array();
	var hong = lotteryCodePage.boshe.hongbo;
	var lan = lotteryCodePage.boshe.lanbo;
	var lv  = lotteryCodePage.boshe.lvbo;
	
	//判断是否是红波
	for(var i=0;i<hong.length;i++){
		if(Number(hong[i])==Number(lotteryCode.numInfo1)){
			colors[0] = 'red';
		}
		if(Number(hong[i])==Number(lotteryCode.numInfo2)){
			colors[1] = 'red';
		}
		if(Number(hong[i])==Number(lotteryCode.numInfo3)){
			colors[2] = 'red';
		}
		if(Number(hong[i])==Number(lotteryCode.numInfo4)){
			colors[3] = 'red';
		}
		if(Number(hong[i])==Number(lotteryCode.numInfo5)){
			colors[4] = 'red';
		}
		if(Number(hong[i])==Number(lotteryCode.numInfo6)){
			colors[5] = 'red';
		}
		if(Number(hong[i])==Number(lotteryCode.numInfo7)){
			colors[6] = 'red';
		}
	}
	
	//判断是否是蓝波
	for(var i=0;i<lan.length;i++){
		if(Number(lan[i])==Number(lotteryCode.numInfo1)){
			colors[0] = 'blue';
		}
		if(Number(lan[i])==Number(lotteryCode.numInfo2)){
			colors[1] = 'blue';
		}
		if(Number(lan[i])==Number(lotteryCode.numInfo3)){
			colors[2] = 'blue';
		}
		if(Number(lan[i])==Number(lotteryCode.numInfo4)){
			colors[3] = 'blue';
		}
		if(Number(lan[i])==Number(lotteryCode.numInfo5)){
			colors[4] = 'blue';
		}
		if(Number(lan[i])==Number(lotteryCode.numInfo6)){
			colors[5] = 'blue';
		}
		if(Number(lan[i])==Number(lotteryCode.numInfo7)){
			colors[6] = 'blue';
		}
	}
	
	//判断是否是绿波
	for(var i=0;i<lv.length;i++){
		if(Number(lv[i])==Number(lotteryCode.numInfo1)){
			colors[0] = 'green';
		}
		if(Number(lv[i])==Number(lotteryCode.numInfo2)){
			colors[1] = 'green';
		}
		if(Number(lv[i])==Number(lotteryCode.numInfo3)){
			colors[2] = 'green';
		}
		if(Number(lv[i])==Number(lotteryCode.numInfo4)){
			colors[3] = 'green';
		}
		if(Number(lv[i])==Number(lotteryCode.numInfo5)){
			colors[4] = 'green';
		}
		if(Number(lv[i])==Number(lotteryCode.numInfo6)){
			colors[5] = 'green';
		}
		if(Number(lv[i])==Number(lotteryCode.numInfo7)){
			colors[6] = 'green';
		}
	}
	
	return colors;
}

