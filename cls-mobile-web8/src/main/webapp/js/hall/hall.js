function HallPage(){
	
};

var hallPage= new HallPage();

var lhcInterval = null;

hallPage.param = {
	currentExpect : null, //当前期号
	isCanActiveExpect : true,
	intervalKey : null  //倒计时周期key
};

$(document).ready(function(){
	//获取所有彩种的期号和开奖时间
	hallPage.getAllActiveExpect();
	//获取所有彩种的最新开奖号码
	hallPage.getAllLastLotteryCode();
	window.setInterval("hallPage.getAllActiveExpect()",10000); //1分钟重新读取服务器
	window.setInterval("hallPage.getAllLastLotteryCode()",60000);  //1分钟重新读取
});

/**
 * 获取系统所有彩种当前正在投注的期号
 */
HallPage.prototype.getAllActiveExpect = function(){
	$.ajax({
		type : "POST",
		url : contextPath + "/lotteryinfo/getAllLotteryKindExpectAndDiffTime",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if( result.code == "ok"){
				hallPage.param.isCanActiveExpect = false;
				var issueList = result.data;
				if(issueList != null){
					for(var i=0;i<issueList.length;i++){
						var issue = issueList[i]
						hallPage.getCurrentLotterydiffTimeByLotteryKind(issue);
						hallPage.setAllInterval(issue);
					}
				}else{
					$(".time").html("预售中...");
				}
			}else if(result.code == "error"){
				showTip(result.data);
			}
		}
	});
};

/**
 * 获取系统当前彩种正在投注的期号
 */
HallPage.prototype.getActiveExpect = function(kindName,currentExpect){
	var jsonData = {
			"lotteryKind" : kindName,	
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/lotteryinfo/getExpectAndDiffTime",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			var resultData = result.data;
			if (result.code == "ok") {
				var issue = result.data;
				if(issue != null && typeof(issue) != "undefind"){
					if(issue.specialExpect == 2){
						//显示预售中
						$("."+kindName.toLowerCase() +"Info .time").html("暂停销售");
						$("."+kindName.toLowerCase() +"Info .child-time").html("暂停销售");
					}else{
						hallPage.getCurrentLotterydiffTimeByLotteryKind(issue);
						hallPage.setAllInterval(issue);
					}
				}else{
					$(".time").html("预售中...");
				}
			} else if (result.code == "error") {
				showTip(result.data);
			}
		}
	});
};

HallPage.prototype.getCurrentLotterydiffTimeByLotteryKind = function(issue){
	var SurplusSecond = 0;
	var diffTimeStr = "";
	if(issue.specialExpect == 2){
		//显示预售中
		$("."+issue.lotteryType.toLowerCase() +"Info .time").html("暂停销售");
		$("."+issue.lotteryType.toLowerCase() +"Info .child-time").html("暂停销售");
	}
	else{
		if(issue.lotteryType == "CQSSC"){
			hallPage.param.cqsscDiffTime = issue.timeDifference;
			SurplusSecond = issue.timeDifference;
			if(SurplusSecond>=3600){
				window.clearInterval(hallPage.param.cqsscIntervalKey); //终止周期性倒计时
			}
			hallPage.calculateTime(SurplusSecond,"cqssc");
			hallPage.showCurrentExpect(issue,"cqssc",false);
			hallPage.param.cqsscDiffTime--;
			if(hallPage.param.cqsscDiffTime < 0){
				window.clearInterval(hallPage.param.cqsscIntervalKey); //终止周期性倒计时
				hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
				//获取最新开奖号码
				hallPage.getLastLotteryCode(issue.lotteryType);
			};
			issue.timeDifference = hallPage.param.cqsscDiffTime;
		}else if(issue.lotteryType == "JXSSC"){
			hallPage.param.jxsscDiffTime = issue.timeDifference;
			SurplusSecond = issue.timeDifference;
			if(SurplusSecond>=3600){
				window.clearInterval(hallPage.param.jxsscIntervalKey); //终止周期性倒计时
			}
			hallPage.calculateTime(SurplusSecond,"jxssc");
			hallPage.showCurrentExpect(issue,"jxssc",false);
			hallPage.param.jxsscDiffTime--;
			if(hallPage.param.jxsscDiffTime < 0){
				window.clearInterval(hallPage.param.jxsscIntervalKey); //终止周期性倒计时
				hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
				hallPage.getLastLotteryCode(issue.lotteryType);
				
			};
			issue.timeDifference = hallPage.param.jxsscDiffTime;
		}else if(issue.lotteryType == "TJSSC"){
			hallPage.param.tjsscDiffTime = issue.timeDifference;
			SurplusSecond = issue.timeDifference;
			if(SurplusSecond>=3600){
				window.clearInterval(hallPage.param.tjsscIntervalKey); //终止周期性倒计时
			}
			hallPage.calculateTime(SurplusSecond,"tjssc");
			hallPage.showCurrentExpect(issue,"tjssc",false);
			hallPage.param.tjsscDiffTime--;
			if(hallPage.param.tjsscDiffTime < 0){
				window.clearInterval(hallPage.param.tjsscIntervalKey); //终止周期性倒计时
				hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
				hallPage.getLastLotteryCode(issue.lotteryType);
				
			};
			issue.timeDifference = hallPage.param.tjsscDiffTime;
		}else if(issue.lotteryType == "XJSSC"){
			hallPage.param.xjsscDiffTime = issue.timeDifference;
			SurplusSecond = issue.timeDifference;
			if(SurplusSecond>=3600){
				window.clearInterval(hallPage.param.xjsscIntervalKey); //终止周期性倒计时
			}
			hallPage.calculateTime(SurplusSecond,"xjssc");
			hallPage.showCurrentExpect(issue,"xjssc",false);
			hallPage.param.xjsscDiffTime--;
			if(hallPage.param.xjsscDiffTime < 0){
				window.clearInterval(hallPage.param.xjsscIntervalKey); //终止周期性倒计时
				hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
				hallPage.getLastLotteryCode(issue.lotteryType);
				
			};
			issue.timeDifference = hallPage.param.xjsscDiffTime;
		}else if(issue.lotteryType == "HLJSSC"){
			hallPage.param.hljsscDiffTime = issue.timeDifference;
			SurplusSecond = issue.timeDifference;
			if(SurplusSecond>=3600){
				window.clearInterval(hallPage.param.hljsscIntervalKey); //终止周期性倒计时
			}
			hallPage.calculateTime(SurplusSecond,"hljssc");
			hallPage.showCurrentExpect(issue,"hljssc",false);
			hallPage.param.hljsscDiffTime--;
			if(hallPage.param.hljsscDiffTime < 0){
				window.clearInterval(hallPage.param.hljsscIntervalKey); //终止周期性倒计时
				hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
				hallPage.getLastLotteryCode(issue.lotteryType);
				
			};
			issue.timeDifference = hallPage.param.hljsscDiffTime;
		}else if(issue.lotteryType == "SFSSC"){
			hallPage.param.sfsscDiffTime = issue.timeDifference;
			SurplusSecond = issue.timeDifference;
			if(SurplusSecond>=3600){
				window.clearInterval(hallPage.param.hljsscIntervalKey); //终止周期性倒计时
			}
			hallPage.calculateTime(SurplusSecond,"hljssc");
			hallPage.showCurrentExpect(issue,"hljssc",false);
			hallPage.param.sfsscDiffTime--;
			if(hallPage.param.sfsscDiffTime < 0){
				window.clearInterval(hallPage.param.hljsscIntervalKey); //终止周期性倒计时
				hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
				hallPage.getLastLotteryCode(issue.lotteryType);
				
			};
			issue.timeDifference = hallPage.param.sfsscDiffTime;
		}else if(issue.lotteryType == "WFSSC"){
			hallPage.param.wfsscDiffTime = issue.timeDifference;
			SurplusSecond = issue.timeDifference;
			if(SurplusSecond>=3600){
				window.clearInterval(hallPage.param.hljsscIntervalKey); //终止周期性倒计时
			}
			hallPage.calculateTime(SurplusSecond,"hljssc");
			hallPage.showCurrentExpect(issue,"hljssc",false);
			hallPage.param.wfsscDiffTime--;
			if(hallPage.param.wfsscDiffTime < 0){
				window.clearInterval(hallPage.param.hljsscIntervalKey); //终止周期性倒计时
				hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
				hallPage.getLastLotteryCode(issue.lotteryType);
				
			};
			issue.timeDifference = hallPage.param.wfsscDiffTime;
		}else if(issue.lotteryType == "JLFFC"){
			hallPage.param.jlffcDiffTime = issue.timeDifference;
			SurplusSecond = issue.timeDifference;
			if(SurplusSecond>=3600){
				window.clearInterval(hallPage.param.jlffcIntervalKey); //终止周期性倒计时
			}
			hallPage.calculateTime(SurplusSecond,"jlffc");
			hallPage.showCurrentExpect(issue,"jlffc",false);
			hallPage.param.jlffcDiffTime--;
			if(hallPage.param.jlffcDiffTime < 0){
				window.clearInterval(hallPage.param.jlffcIntervalKey); //终止周期性倒计时
				hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
				hallPage.getLastLotteryCode(issue.lotteryType);
				
			};
			issue.timeDifference = hallPage.param.jlffcDiffTime;
		}else if(issue.lotteryType == "GDSYXW"){
			hallPage.param.gdsyxwDiffTime = issue.timeDifference;
			SurplusSecond = issue.timeDifference;
			if(SurplusSecond>=3600){
				window.clearInterval(hallPage.param.gdsyxwIntervalKey); //终止周期性倒计时
			}
			hallPage.calculateTime(SurplusSecond,"gdsyxw");
			hallPage.showCurrentExpect(issue,"gdsyxw",false);
			hallPage.param.gdsyxwDiffTime--;
			if(hallPage.param.gdsyxwDiffTime < 0){
				window.clearInterval(hallPage.param.gdsyxwIntervalKey); //终止周期性倒计时
				hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
				hallPage.getLastLotteryCode(issue.lotteryType);
				
			};
			issue.timeDifference = hallPage.param.gdsyxwDiffTime;
		}else if(issue.lotteryType == "WFSYXW"){
			hallPage.param.wfsyxwDiffTime = issue.timeDifference;
			SurplusSecond = issue.timeDifference;
			if(SurplusSecond>=3600){
				window.clearInterval(hallPage.param.wfsyxwIntervalKey); //终止周期性倒计时
			}
			hallPage.calculateTime(SurplusSecond,"wfsyxw");
			hallPage.showCurrentExpect(issue,"wfsyxw",false);
			hallPage.param.wfsyxwDiffTime--;
			if(hallPage.param.wfsyxwDiffTime < 0){
				window.clearInterval(hallPage.param.wfsyxwIntervalKey); //终止周期性倒计时
				hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
				hallPage.getLastLotteryCode(issue.lotteryType);
				
			};
			issue.timeDifference = hallPage.param.wfsyxwDiffTime;
		}else if(issue.lotteryType == "SFSYXW"){
			hallPage.param.sfsyxwDiffTime = issue.timeDifference;
			SurplusSecond = issue.timeDifference;
			if(SurplusSecond>=3600){
				window.clearInterval(hallPage.param.sfsyxwIntervalKey); //终止周期性倒计时
			}
			hallPage.calculateTime(SurplusSecond,"sfsyxw");
			hallPage.showCurrentExpect(issue,"sfsyxw",false);
			hallPage.param.sfsyxwDiffTime--;
			if(hallPage.param.sfsyxwDiffTime < 0){
				window.clearInterval(hallPage.param.sfsyxwIntervalKey); //终止周期性倒计时
				hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
				hallPage.getLastLotteryCode(issue.lotteryType);
				
			};
			issue.timeDifference = hallPage.param.sfsyxwDiffTime;
		}else if(issue.lotteryType == "SDSYXW"){
			hallPage.param.sdsyxwDiffTime = issue.timeDifference;
			SurplusSecond = issue.timeDifference;
			if(SurplusSecond>=3600){
				window.clearInterval(hallPage.param.sdsyxwIntervalKey); //终止周期性倒计时
			}
			hallPage.calculateTime(SurplusSecond,"sdsyxw");
			hallPage.showCurrentExpect(issue,"sdsyxw",false);
			hallPage.param.sdsyxwDiffTime--;
			if(hallPage.param.sdsyxwDiffTime < 0){
				window.clearInterval(hallPage.param.sdsyxwIntervalKey); //终止周期性倒计时
				hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
				hallPage.getLastLotteryCode(issue.lotteryType);
				
			};
			issue.timeDifference = hallPage.param.sdsyxwDiffTime;
		}else if(issue.lotteryType == "JXSYXW"){
			hallPage.param.jxsyxwDiffTime = issue.timeDifference;
			SurplusSecond = issue.timeDifference;
			if(SurplusSecond>=3600){
				window.clearInterval(hallPage.param.jxsyxwIntervalKey); //终止周期性倒计时
			}
			hallPage.calculateTime(SurplusSecond,"jxsyxw");
			hallPage.showCurrentExpect(issue,"jxsyxw",false);
			hallPage.param.jxsyxwDiffTime--;
			if(hallPage.param.jxsyxwDiffTime < 0){
				window.clearInterval(hallPage.param.jxsyxwIntervalKey); //终止周期性倒计时
				hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
				hallPage.getLastLotteryCode(issue.lotteryType);
				
			};
			issue.timeDifference = hallPage.param.jxsyxwDiffTime;
		}else if(issue.lotteryType == "FJSYXW"){
			hallPage.param.fjsyxwDiffTime = issue.timeDifference;
			SurplusSecond = issue.timeDifference;
			if(SurplusSecond>=3600){
				window.clearInterval(hallPage.param.fjsyxwIntervalKey); //终止周期性倒计时
			}
			hallPage.calculateTime(SurplusSecond,"fjsyxw");
			hallPage.showCurrentExpect(issue,"fjsyxw",false);
			hallPage.param.fjsyxwDiffTime--;
			if(hallPage.param.fjsyxwDiffTime < 0){
				window.clearInterval(hallPage.param.fjsyxwIntervalKey); //终止周期性倒计时
				hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
				hallPage.getLastLotteryCode(issue.lotteryType);
				
			};
			issue.timeDifference = hallPage.param.fjsyxwDiffTime;
		}else if(issue.lotteryType == "JSKS"){
			hallPage.param.jsksDiffTime = issue.timeDifference;
			SurplusSecond = issue.timeDifference;
			if(SurplusSecond>=3600){
				window.clearInterval(hallPage.param.jsksIntervalKey); //终止周期性倒计时
			}
			hallPage.calculateTime(SurplusSecond,"jsks");
			hallPage.showCurrentExpect(issue,"jsks",false);
			hallPage.param.jsksDiffTime--;
			if(hallPage.param.jsksDiffTime < 0){
				window.clearInterval(hallPage.param.jsksIntervalKey); //终止周期性倒计时
				hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
				hallPage.getLastLotteryCode(issue.lotteryType);
				
			};
			issue.timeDifference = hallPage.param.jsksDiffTime;
		}else if(issue.lotteryType == "FJKS"){
			hallPage.param.fjksDiffTime = issue.timeDifference;
			SurplusSecond = issue.timeDifference;
			if(SurplusSecond>=3600){
				window.clearInterval(hallPage.param.fjksIntervalKey); //终止周期性倒计时
			}
			hallPage.calculateTime(SurplusSecond,"fjks");
			hallPage.showCurrentExpect(issue,"fjks",false);
			hallPage.param.fjksDiffTime--;
			if(hallPage.param.fjksDiffTime < 0){
				window.clearInterval(hallPage.param.fjksIntervalKey); //终止周期性倒计时
				hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
				hallPage.getLastLotteryCode(issue.lotteryType);
				
			};
			issue.timeDifference = hallPage.param.fjksDiffTime;
		}else if(issue.lotteryType == "AHKS"){
			hallPage.param.ahksDiffTime = issue.timeDifference;
			SurplusSecond = issue.timeDifference;
			if(SurplusSecond>=3600){
				window.clearInterval(hallPage.param.ahksIntervalKey); //终止周期性倒计时
			}
			hallPage.calculateTime(SurplusSecond,"ahks");
			hallPage.showCurrentExpect(issue,"ahks",false);
			hallPage.param.ahksDiffTime--;
			if(hallPage.param.ahksDiffTime < 0){
				window.clearInterval(hallPage.param.ahksIntervalKey); //终止周期性倒计时
				hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
				hallPage.getLastLotteryCode(issue.lotteryType);
				
			};
			issue.timeDifference = hallPage.param.ahksDiffTime;
		}else if(issue.lotteryType == "HBKS"){
			hallPage.param.hbksDiffTime = issue.timeDifference;
			SurplusSecond = issue.timeDifference;
			if(SurplusSecond>=3600){
				window.clearInterval(hallPage.param.hbksIntervalKey); //终止周期性倒计时
			}
			hallPage.calculateTime(SurplusSecond,"hbks");
			hallPage.showCurrentExpect(issue,"hbks",false);
			hallPage.param.hbksDiffTime--;
			if(hallPage.param.hbksDiffTime < 0){
				window.clearInterval(hallPage.param.hbksIntervalKey); //终止周期性倒计时
				hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
				hallPage.getLastLotteryCode(issue.lotteryType);
				
			};
			issue.timeDifference = hallPage.param.hbksDiffTime;
		}else if(issue.lotteryType == "JLKS"){
			hallPage.param.jlksDiffTime = issue.timeDifference;
			SurplusSecond = issue.timeDifference;
			if(SurplusSecond>=3600){
				window.clearInterval(hallPage.param.jlksIntervalKey); //终止周期性倒计时
			}
			hallPage.calculateTime(SurplusSecond,"jlks");
			hallPage.showCurrentExpect(issue,"jlks",false);
			hallPage.param.jlksDiffTime--;
			if(hallPage.param.jlksDiffTime < 0){
				window.clearInterval(hallPage.param.jlksIntervalKey); //终止周期性倒计时
				hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
				hallPage.getLastLotteryCode(issue.lotteryType);
				
			};
			issue.timeDifference = hallPage.param.jlksDiffTime;
		}else if(issue.lotteryType == "BJKS"){
			hallPage.param.bjksDiffTime = issue.timeDifference;
			SurplusSecond = issue.timeDifference;
			if(SurplusSecond>=3600){
				window.clearInterval(hallPage.param.bjksIntervalKey); //终止周期性倒计时
			}
			hallPage.calculateTime(SurplusSecond,"bjks");
			hallPage.showCurrentExpect(issue,"bjks",false);
			hallPage.param.bjksDiffTime--;
			if(hallPage.param.bjksDiffTime < 0){
				window.clearInterval(hallPage.param.bjksIntervalKey); //终止周期性倒计时
				hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
				hallPage.getLastLotteryCode(issue.lotteryType);
				
			};
			issue.timeDifference = hallPage.param.bjksDiffTime;
		}else if(issue.lotteryType == "JYKS"){
			hallPage.param.jyksDiffTime = issue.timeDifference;
			SurplusSecond = issue.timeDifference;
			if(SurplusSecond>=3600){
				window.clearInterval(hallPage.param.jyksIntervalKey); //终止周期性倒计时
			}
			hallPage.calculateTime(SurplusSecond,"jyks");
			hallPage.showCurrentExpect(issue,"jyks",false);
			hallPage.param.jyksDiffTime--;
			if(hallPage.param.jyksDiffTime < 0){
				window.clearInterval(hallPage.param.jyksIntervalKey); //终止周期性倒计时
				hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
				hallPage.getLastLotteryCode(issue.lotteryType);
				
			};
			issue.timeDifference = hallPage.param.jyksDiffTime;
		}else if(issue.lotteryType == "GXKS"){
			hallPage.param.gxksDiffTime = issue.timeDifference;
			SurplusSecond = issue.timeDifference;
			if(SurplusSecond>=3600){
				window.clearInterval(hallPage.param.gxksIntervalKey); //终止周期性倒计时
			}
			hallPage.calculateTime(SurplusSecond,"gxks");
			hallPage.showCurrentExpect(issue,"gxks",false);
			hallPage.param.gxksDiffTime--;
			if(hallPage.param.gxksDiffTime < 0){
				window.clearInterval(hallPage.param.gxksIntervalKey); //终止周期性倒计时
				hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
				hallPage.getLastLotteryCode(issue.lotteryType);
				
			};
			issue.timeDifference = hallPage.param.gxksDiffTime;
		}else if(issue.lotteryType == "GSKS"){
			hallPage.param.gsksDiffTime = issue.timeDifference;
			SurplusSecond = issue.timeDifference;
			if(SurplusSecond>=3600){
				window.clearInterval(hallPage.param.gsksIntervalKey); //终止周期性倒计时
			}
			hallPage.calculateTime(SurplusSecond,"gsks");
			hallPage.showCurrentExpect(issue,"gsks",false);
			hallPage.param.gsksDiffTime--;
			if(hallPage.param.gsksDiffTime < 0){
				window.clearInterval(hallPage.param.gsksIntervalKey); //终止周期性倒计时
				hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
				hallPage.getLastLotteryCode(issue.lotteryType);
				
			};
			issue.timeDifference = hallPage.param.gsksDiffTime;
		}else if(issue.lotteryType == "SHKS"){
			hallPage.param.shksDiffTime = issue.timeDifference;
			SurplusSecond = issue.timeDifference;
			if(SurplusSecond>=3600){
				window.clearInterval(hallPage.param.shksIntervalKey); //终止周期性倒计时
			}
			hallPage.calculateTime(SurplusSecond,"shks");
			hallPage.showCurrentExpect(issue,"shks",false);
			hallPage.param.shksDiffTime--;
			if(hallPage.param.shksDiffTime < 0){
				window.clearInterval(hallPage.param.shksIntervalKey); //终止周期性倒计时
				hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
				hallPage.getLastLotteryCode(issue.lotteryType);
				
			};
			issue.timeDifference = hallPage.param.shksDiffTime;
		}else if(issue.lotteryType == "SFKS"){
			hallPage.param.sfksDiffTime = issue.timeDifference;
			SurplusSecond = issue.timeDifference;
			if(SurplusSecond>=3600){
				window.clearInterval(hallPage.param.sfksIntervalKey); //终止周期性倒计时
			}
			hallPage.calculateTime(SurplusSecond,"sfks");
			hallPage.showCurrentExpect(issue,"sfks",false);
			hallPage.param.sfksDiffTime--;
			if(hallPage.param.sfksDiffTime < 0){
				window.clearInterval(hallPage.param.sfksIntervalKey); //终止周期性倒计时
				hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
				hallPage.getLastLotteryCode(issue.lotteryType);
				
			};
			issue.timeDifference = hallPage.param.sfksDiffTime;
		}else if(issue.lotteryType == "WFKS"){
			hallPage.param.wfksDiffTime = issue.timeDifference;
			SurplusSecond = issue.timeDifference;
			if(SurplusSecond>=3600){
				window.clearInterval(hallPage.param.wfksIntervalKey); //终止周期性倒计时
			}
			hallPage.calculateTime(SurplusSecond,"wfks");
			hallPage.showCurrentExpect(issue,"wfks",false);
			hallPage.param.wfksDiffTime--;
			if(hallPage.param.wfksDiffTime < 0){
				window.clearInterval(hallPage.param.wfksIntervalKey); //终止周期性倒计时
				hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
				hallPage.getLastLotteryCode(issue.lotteryType);
				
			};
			issue.timeDifference = hallPage.param.wfksDiffTime;
		}else if(issue.lotteryType == "FCSDDPC"){
			hallPage.param.fcsddpcDiffTime = issue.timeDifference;
			SurplusSecond = issue.timeDifference;
			hallPage.calculateTime(SurplusSecond,"fcsddpc");
			hallPage.showCurrentExpect(issue,"fcsddpc",false);
			hallPage.param.fcsddpcDiffTime--;
			if(hallPage.param.fcsddpcDiffTime < 0){
				window.clearInterval(hallPage.param.fcsddpcIntervalKey); //终止周期性倒计时
				hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
				hallPage.getLastLotteryCode(issue.lotteryType);
				
			};
			issue.timeDifference = hallPage.param.fcsddpcDiffTime;
		}else if(issue.lotteryType == "BJPK10"){
			hallPage.param.pk10DiffTime = issue.timeDifference;
			SurplusSecond = issue.timeDifference;
			if(SurplusSecond>=3600){
				window.clearInterval(hallPage.param.pk10IntervalKey); //终止周期性倒计时
			}
			hallPage.calculateTime(SurplusSecond,"bjpk10");
			hallPage.showCurrentExpect(issue,"bjpk10",false);
			hallPage.param.pk10DiffTime--;
			if(hallPage.param.pk10DiffTime < 0){
				window.clearInterval(hallPage.param.pk10IntervalKey); //终止周期性倒计时
				hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
				hallPage.getLastLotteryCode(issue.lotteryType);
				
			};
			issue.timeDifference = hallPage.param.pk10DiffTime;
		}else if(issue.lotteryType == "JSPK10"){
			hallPage.param.jspk10DiffTime = issue.timeDifference;
			SurplusSecond = issue.timeDifference;
			if(SurplusSecond>=3600){
				window.clearInterval(hallPage.param.jspk10IntervalKey); //终止周期性倒计时
			}
			hallPage.calculateTime(SurplusSecond,"jspk10");
			hallPage.showCurrentExpect(issue,"jspk10",false);
			hallPage.param.jspk10DiffTime--;
			if(hallPage.param.jspk10DiffTime < 0){
				window.clearInterval(hallPage.param.jspk10IntervalKey); //终止周期性倒计时
				hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
				hallPage.getLastLotteryCode(issue.lotteryType);
				
			};
			issue.timeDifference = hallPage.param.jspk10DiffTime;
		}else if(issue.lotteryType == "XJPLFC"){
			hallPage.param.xjplfcDiffTime = issue.timeDifference;
			SurplusSecond = issue.timeDifference;
			if(SurplusSecond>=3600){
				window.clearInterval(hallPage.param.xjplfcIntervalKey); //终止周期性倒计时
			}
			hallPage.calculateTime(SurplusSecond,"xjplfc");
			hallPage.showCurrentExpect(issue,"xjplfc",false);
			hallPage.param.xjplfcDiffTime--;
			if(hallPage.param.xjplfcDiffTime < 0){
				window.clearInterval(hallPage.param.xjplfcIntervalKey); //终止周期性倒计时
				hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
				hallPage.getLastLotteryCode(issue.lotteryType);
				
			};
			issue.timeDifference = hallPage.param.xjplfcDiffTime;
		}else if(issue.lotteryType == "TWWFC"){
			hallPage.param.twwfcDiffTime = issue.timeDifference;
			SurplusSecond = issue.timeDifference;
			if(SurplusSecond>=3600){
				window.clearInterval(hallPage.param.twwfcIntervalKey); //终止周期性倒计时
			}
			hallPage.calculateTime(SurplusSecond,"twwfc");
			hallPage.showCurrentExpect(issue,"twwfc",false);
			hallPage.param.twwfcDiffTime--;
			if(hallPage.param.twwfcDiffTime < 0){
				window.clearInterval(hallPage.param.twwfcIntervalKey); //终止周期性倒计时
				hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
				hallPage.getLastLotteryCode(issue.lotteryType);
				
			};
			issue.timeDifference = hallPage.param.twwfcDiffTime;
		}else if(issue.lotteryType == "XYEB"){
			hallPage.param.xyebDiffTime = issue.timeDifference;
			SurplusSecond = issue.timeDifference;
			if(SurplusSecond>=3600){
				window.clearInterval(hallPage.param.xyebIntervalKey); //终止周期性倒计时
			}
			hallPage.calculateTime(SurplusSecond,"xyeb");
			hallPage.showCurrentExpect(issue,"xyeb",false);
			hallPage.param.xyebDiffTime--;
			if(hallPage.param.xyebDiffTime < 0){
				window.clearInterval(hallPage.param.xyebIntervalKey); //终止周期性倒计时
				hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
				hallPage.getLastLotteryCode(issue.lotteryType);
				
			};
			issue.timeDifference = hallPage.param.xyebDiffTime;
		}else if(issue.lotteryType == "LHC"){
			if(issue.isClose){
				//显示预售中
				$(".lhcInfo .time").html("封盘中...");
				$(".lhcInfo .child-time").html("封盘中...");
				window.clearInterval(hallPage.param.lhcIntervalKey); //终止周期性倒计时
	//			if(lhcInterval == null){
	//				//1分钟重新读取服务器
	//			    lhcInterval = setInterval(function(){hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum)},10000);
	//			}
	//			
				return;
			}
	//		//终止封盘时候的周期性倒计时
	//		window.clearInterval(lhcInterval);
	//		var lhcInterval = null;
			hallPage.param.lhcDiffTime = issue.timeDifference;
			SurplusSecond = issue.timeDifference;
			hallPage.calculateTime(SurplusSecond,"lhc");
			hallPage.showCurrentExpect(issue,"lhc",false);
			hallPage.param.lhcDiffTime--;
			if(hallPage.param.lhcDiffTime < 0){
				window.clearInterval(hallPage.param.lhcIntervalKey); //终止周期性倒计时
				hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
				hallPage.getLastLotteryCode(issue.lotteryType);
				
			};
			issue.timeDifference = hallPage.param.lhcDiffTime;
		}else if(issue.lotteryType == "XYLHC"){
			if(issue.isClose){
				//显示预售中
				$(".xylhcInfo .time").html("封盘中...");
				$(".xylhcInfo .child-time").html("封盘中...");
				window.clearInterval(hallPage.param.xylhcIntervalKey); //终止周期性倒计时
	//			if(lhcInterval == null){
	//				//1分钟重新读取服务器
	//			    lhcInterval = setInterval(function(){hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum)},10000);
	//			}
	//			
				return;
			}
	//		//终止封盘时候的周期性倒计时
	//		window.clearInterval(lhcInterval);
	//		var lhcInterval = null;
			hallPage.param.xylhcDiffTime = issue.timeDifference;
			SurplusSecond = issue.timeDifference;
			hallPage.calculateTime(SurplusSecond,"xylhc");
			hallPage.showCurrentExpect(issue,"xylhc",false);
			hallPage.param.xylhcDiffTime--;
			if(hallPage.param.xylhcDiffTime < 0){
				window.clearInterval(hallPage.param.xylhcIntervalKey); //终止周期性倒计时
				hallPage.getActiveExpect(issue.lotteryType,issue.lotteryNum);  //倒计时结束重新请求最新期号
				hallPage.getLastLotteryCode(issue.lotteryType);
				
			};
			issue.timeDifference = hallPage.param.xylhcDiffTime;
		}
	}
}

/**
 * 设置定时器
 */
HallPage.prototype.setAllInterval = function(issue){
	if(issue.lotteryType == "CQSSC"){
		window.clearInterval(hallPage.param.cqsscIntervalKey); //终止周期性倒计时
		hallPage.param.cqsscIntervalKey = setInterval(function(){hallPage.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "JXSSC"){
		window.clearInterval(hallPage.param.jxsscIntervalKey); //终止周期性倒计时
		hallPage.param.jxsscIntervalKey = setInterval(function(){hallPage.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "TJSSC"){
		window.clearInterval(hallPage.param.tjsscIntervalKey); //终止周期性倒计时
		hallPage.param.tjsscIntervalKey = setInterval(function(){hallPage.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "XJSSC"){
		window.clearInterval(hallPage.param.xjsscIntervalKey); //终止周期性倒计时
		hallPage.param.xjsscIntervalKey = setInterval(function(){hallPage.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "HLJSSC"){
		window.clearInterval(hallPage.param.hljsscIntervalKey); //终止周期性倒计时
		hallPage.param.hljsscIntervalKey = setInterval(function(){hallPage.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "JLFFC"){
		window.clearInterval(hallPage.param.jlffcIntervalKey); //终止周期性倒计时
		hallPage.param.jlffcIntervalKey = setInterval(function(){hallPage.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "GDSYXW"){
		window.clearInterval(hallPage.param.gdsyxwIntervalKey); //终止周期性倒计时
		hallPage.param.gdsyxwIntervalKey = setInterval(function(){hallPage.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "SDSYXW"){
		window.clearInterval(hallPage.param.sdsyxwIntervalKey); //终止周期性倒计时
		hallPage.param.sdsyxwIntervalKey = setInterval(function(){hallPage.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "JXSYXW"){
		window.clearInterval(hallPage.param.jxsyxwIntervalKey); //终止周期性倒计时
		hallPage.param.jxsyxwIntervalKey = setInterval(function(){hallPage.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "FJSYXW"){
		window.clearInterval(hallPage.param.fjsyxwIntervalKey); //终止周期性倒计时
		hallPage.param.fjsyxwIntervalKey = setInterval(function(){hallPage.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "WFSYXW"){
		window.clearInterval(hallPage.param.wfsyxwIntervalKey); //终止周期性倒计时
		hallPage.param.wfsyxwIntervalKey = setInterval(function(){hallPage.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "SFSYXW"){
		window.clearInterval(hallPage.param.sfsyxwIntervalKey); //终止周期性倒计时
		hallPage.param.sfsyxwIntervalKey = setInterval(function(){hallPage.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	} else if(issue.lotteryType == "JSKS"){
		window.clearInterval(hallPage.param.jsksIntervalKey); //终止周期性倒计时
		hallPage.param.jsksIntervalKey = setInterval(function(){hallPage.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "FJKS"){
		window.clearInterval(hallPage.param.fjksIntervalKey); //终止周期性倒计时
		hallPage.param.fjksIntervalKey = setInterval(function(){hallPage.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "AHKS"){
		window.clearInterval(hallPage.param.ahksIntervalKey); //终止周期性倒计时
		hallPage.param.ahksIntervalKey = setInterval(function(){hallPage.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "HBKS"){
		window.clearInterval(hallPage.param.hbksIntervalKey); //终止周期性倒计时
		hallPage.param.hbksIntervalKey = setInterval(function(){hallPage.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "JLKS"){
		window.clearInterval(hallPage.param.jlksIntervalKey); //终止周期性倒计时
		hallPage.param.jlksIntervalKey = setInterval(function(){hallPage.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "BJKS"){
		window.clearInterval(hallPage.param.bjksIntervalKey); //终止周期性倒计时
		hallPage.param.bjksIntervalKey = setInterval(function(){hallPage.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "JYKS"){
		window.clearInterval(hallPage.param.jyksIntervalKey); //终止周期性倒计时
		hallPage.param.jyksIntervalKey = setInterval(function(){hallPage.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "GXKS"){
		window.clearInterval(hallPage.param.gxksIntervalKey); //终止周期性倒计时
		hallPage.param.gxksIntervalKey = setInterval(function(){hallPage.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "GSKS"){
		window.clearInterval(hallPage.param.gsksIntervalKey); //终止周期性倒计时
		hallPage.param.gsksIntervalKey = setInterval(function(){hallPage.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "SHKS"){
		window.clearInterval(hallPage.param.shksIntervalKey); //终止周期性倒计时
		hallPage.param.shksIntervalKey = setInterval(function(){hallPage.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "FCSDDPC"){
		window.clearInterval(hallPage.param.fcsddpcIntervalKey); //终止周期性倒计时
		hallPage.param.fcsddpcIntervalKey = setInterval(function(){hallPage.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "BJPK10"){
		window.clearInterval(hallPage.param.pk10IntervalKey); //终止周期性倒计时
		hallPage.param.pk10IntervalKey = setInterval(function(){hallPage.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "JSPK10"){
		window.clearInterval(hallPage.param.jspk10IntervalKey); //终止周期性倒计时
		hallPage.param.jspk10IntervalKey = setInterval(function(){hallPage.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "XJPLFC"){
		window.clearInterval(hallPage.param.xjplfcIntervalKey); //终止周期性倒计时
		hallPage.param.xjplfcIntervalKey = setInterval(function(){hallPage.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "TWWFC"){
		window.clearInterval(hallPage.param.twwfcIntervalKey); //终止周期性倒计时
		hallPage.param.twwfcIntervalKey = setInterval(function(){hallPage.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "LHC"){
		window.clearInterval(hallPage.param.lhcIntervalKey); //终止周期性倒计时
		hallPage.param.lhcIntervalKey = setInterval(function(){hallPage.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "XYEB"){
		window.clearInterval(hallPage.param.xyebIntervalKey); //终止周期性倒计时
		hallPage.param.xyebIntervalKey = setInterval(function(){hallPage.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}else if(issue.lotteryType == "XYLHC"){
		window.clearInterval(hallPage.param.xylhcIntervalKey); //终止周期性倒计时
		hallPage.param.xylhcIntervalKey = setInterval(function(){hallPage.getCurrentLotterydiffTimeByLotteryKind(issue)},1000);
	}
}


/**
 * 服务器时间倒计时
 */
HallPage.prototype.calculateTime = function(SurplusSecond,timeInfo){
	var diffTimeStr = "";
	if(timeInfo == "fcsddpc" || timeInfo == "lhc" ||  timeInfo == "xylhc"){
		if(SurplusSecond>= (3600 * 24) && timeInfo != "lhc" && timeInfo != "xylhc"){
			//显示预售中
			$("."+timeInfo +"Info .time").html("即将开盘...");
			$("."+timeInfo +"Info .child-time").html("即将开盘...");
			window.clearInterval(hallPage.param.fcsddpcIntervalKey); //终止周期性倒计时
		}else{
			var h = Math.floor(SurplusSecond/3600);
			if (h<10){
				h = "0"+h;
			}
			//计算剩余的分钟
			SurplusSecond = SurplusSecond - (h * 3600);
			var m = Math.floor(SurplusSecond/60);
			if (m<10){
				m = "0"+m;
			}
			var s = SurplusSecond%60;
			if(s<10){
				s = "0"+s;
			}
			
			h = h.toString();
			m = m.toString();
			s = s.toString();
			
			diffTimeStr = h+":"+ m +":" + s;
			if(timeInfo == "xylhc"){
				diffTimeStr=m +":" + s;
			}
			$("."+timeInfo +"Info .time").html(diffTimeStr);
			//首页的
			$("."+timeInfo +"Info .child-time").html(diffTimeStr);
		}
	}else{
		if(SurplusSecond>=3600){
			$("."+timeInfo +"Info .time").html("即将开盘...");
			$("."+timeInfo +"Info .child-time").html("即将开盘...");
		}else{
			var m = Math.floor(SurplusSecond/60);
			if (m<10){
				m = "0"+m;
			}
			var s = SurplusSecond%60;
			if(s<10){
				s = "0"+s;
			}
			
			m = m.toString();
			s = s.toString();
			var mArray = m.split("");
			var sArray = s.split("");
			
			diffTimeStr += mArray[0]+mArray[1]+":";
			diffTimeStr += sArray[0]+sArray[1];
			
			$("."+timeInfo +"Info .time").html(diffTimeStr);
			//首页的
			$("."+timeInfo +"Info .child-time").html(diffTimeStr);
		}
	}
};

/**
 * 获取所有彩种最新的开奖号码
 */
HallPage.prototype.getAllLastLotteryCode = function(){
	$.ajax({
		type : "POST",
		url : contextPath + "/code/getAllLastLotteryCode",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if( result.code == "ok"){
				var lotteryCodeList = result.data;
				if(lotteryCodeList != null){
					for(var i=0;i < lotteryCodeList.length;i++){
						var lotteryCode = lotteryCodeList[i];
						if(lotteryCode != null){
							if(lotteryCode.lotteryName!=null){
								var lotteryName=lotteryCode.lotteryName.toString().toLowerCase();
								hallPage.showCurrentlotteryMun(lotteryCode,lotteryName);
							}
						}
					}
				}
			}else if(result.code == "error"){
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 获取最新的开奖号码
 */
HallPage.prototype.getLastLotteryCode = function(lotteryKind){
	var jsonData = {
			"lotteryKind" : lotteryKind,	
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/code/getLastLotteryCode",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var lotteryCode = result.data;
				if(lotteryCode != null){
					if(lotteryCode.lotteryName!=null){
						var lotteryName=lotteryCode.lotteryName.toString().toLowerCase();
						hallPage.showCurrentlotteryMun(lotteryCode,lotteryName);
					}
				}
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 开奖号码显示
 * @param lotteryIssue
 * @param expectNumInfo
 */
HallPage.prototype.showCurrentlotteryMun = function(lotteryCode,lotteryMunInfo){
	//显示期号
	hallPage.showCurrentExpect(lotteryCode,lotteryMunInfo,true);
	var str = "";
	if(lotteryCode.lotteryName=='BJPK10' || lotteryCode.lotteryName=='JSPK10'){
		str += "<span>"+lotteryCode.numInfo1+"</span>";
		str += "<span>"+lotteryCode.numInfo2+"</span>";
		str += "<span>"+lotteryCode.numInfo3+"</span>";
		str += "<span>"+lotteryCode.numInfo4+"</span>";
		str += "<span>"+lotteryCode.numInfo5+"</span></br>";
		str += "<span>"+lotteryCode.numInfo6+"</span>";
		str += "<span>"+lotteryCode.numInfo7+"</span>";
		str += "<span>"+lotteryCode.numInfo8+"</span>";
		str += "<span>"+lotteryCode.numInfo9+"</span>";
		str += "<span>"+lotteryCode.numInfo10+"</span>";
		$("."+lotteryMunInfo +"Info .lottery-mun").html(str);
	}else if(lotteryCode.lotteryName.lastIndexOf('KS')>-1 ||
			lotteryCode.lotteryName=='XYEB' || lotteryCode.lotteryName.lastIndexOf('DPC')>-1){
		str += "<span>"+lotteryCode.numInfo1+"</span>";
		str += "<span>"+lotteryCode.numInfo2+"</span>";
		str += "<span>"+lotteryCode.numInfo3+"</span>";
		$("."+lotteryMunInfo +"Info .lottery-mun").html(str);
	}else if(lotteryCode.lotteryName=='LHC'){
		str += "<span>"+lotteryCode.numInfo1+"</span>";
		str += "<span>"+lotteryCode.numInfo2+"</span>";
		str += "<span>"+lotteryCode.numInfo3+"</span>";
		str += "<span>"+lotteryCode.numInfo4+"</span>";
		str += "<span>"+lotteryCode.numInfo5+"</span>";
		str += "<span>"+lotteryCode.numInfo6+"</span>";
		$("."+lotteryMunInfo +"Info .lottery-mun").html(str);
	}else{
		str += "<span>"+lotteryCode.numInfo1+"</span>";
		str += "<span>"+lotteryCode.numInfo2+"</span>";
		str += "<span>"+lotteryCode.numInfo3+"</span>";
		str += "<span>"+lotteryCode.numInfo4+"</span>";
		str += "<span>"+lotteryCode.numInfo5+"</span>";
		$("."+lotteryMunInfo +"Info .lottery-mun").html(str);
	}
};

/**
 * 期号显示
 * @param lotteryIssue
 * @param expectNumInfo
 */
HallPage.prototype.showCurrentExpect = function(lotteryIssue,expectNumInfo,oldBoolean){
	var lotteryNum = lotteryIssue.lotteryNum;
	 expectDay = lotteryNum.substring(0,lotteryNum.length - 3);
	 expectNum = lotteryNum.substring(lotteryNum.length - 3,lotteryNum.length);
	 if(expectNumInfo == "bjks" || expectNumInfo == "fcsddpc" || expectNumInfo == "lhc"
		 || expectNumInfo == "bjpk10" || expectNumInfo == "xjplfc" || expectNumInfo == "twwfc" || expectNumInfo == "jspk10"){
		 if(oldBoolean){
			 $("."+expectNumInfo +"Info .lottery-periods").html("第" + lotteryNum+ "期");
		 }else{
			 $("."+expectNumInfo +"Info .newExpectNum").html("距离第" + lotteryNum+ "期截止还有");
		 }
	 }else{
		 if(oldBoolean){
			 $("."+expectNumInfo +"Info .lottery-periods").html("第" + expectDay + "-" + expectNum+ "期");
		 }else{
			 $("."+expectNumInfo +"Info .newExpectNum").html("距离第" + expectDay + "-" + expectNum+ "期截止还有");
		 }
	 }
};

/**
 * 控制是否能发起期号的请求
 */
HallPage.prototype.controlIsCanActive = function(){
	hallPage.param.isCanActiveExpect = true;
};

function changeStyle(e){
    var $target = $(e),
        $main = $(".main");
    /*$main.hide();
    setTimeout(function($main){
        $main.show();
    },100,$main);*/
    if($main.hasClass('main-block')){
        $target.find(".icon").attr("src",contextPath+"/images/icon/icon-threetool.png");
        $main.removeClass('main-block');
    }else{
        $target.find(".icon").attr("src",contextPath+"/images/icon/icon-list.png");
        $main.addClass('main-block');
    }
}

function changeMain(e,index){
    var $target = $(e),
        $main = $(".main"),
        $mainNav = $(".main-nav");
    $main.removeClass("on");
    $main.eq(index).addClass("on");
    $mainNav.find(".child").removeClass("on");
    $target.addClass("on");
}