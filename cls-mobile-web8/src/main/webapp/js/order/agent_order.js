/*
 * 下级投注
 */
function AgentOrderPage() {
}
var agentOrderPage = new AgentOrderPage();

agentOrderPage.queryParam = {
		userName : null,
		lotteryType : null,
		createdDateStart : null,
		createdDateEnd : null,
		prostateStatus : null,
		reportType : 0,
		pageNo : null,
		queryScope : 3,
};

agentOrderPage.pageParam = {
        pageSize : 10,  //前台分页每页条数
        pageNo : 1,
        showPage:1,
        total:0       
};

$(document).ready(function() {
	//tab切换
	$('.js-tab a').on('click',function(){
		var index = $(this).index();
		$('.js-tab a').removeClass('on');
		$(this).addClass('on');
		$('.js-cont > .block').eq(index).show().siblings().hide();
	})
	
	// 搜索条件-彩种选择
	$('.lottery-name').on('click', function() {
        $(this).parent().find('.options').toggleClass('open');
    });
    
    $('.lottery-name .options').on('click', 'li', function() {
        var $select = $(this).parents('.lottery-name');
        $select.find('.text').html($(this).html());
        $(this).toggleClass('open');
    });

    var defalutDate = new Date();
    var strDate = new Date();
    defalutDate.setDate(1);
    defalutDate.setHours(3, 0, 0, 0);
    /*agentOrderPage.queryParam.reportType = 0;
    agentOrderPage.queryParam.createdDateStart = defalutDate;
    agentOrderPage.queryParam.createdDateEnd = strDate.setHours(2, 59, 59, 999);*/
    agentOrderPage.queryParam.reportType=1;
	agentOrderPage.queryParam.createdDateStart=strDate.setHours(3,-1,60,999);
	agentOrderPage.queryParam.createdDateEnd=strDate.AddDays(1).setHours(2,59,59,999);
	
    //点击切换查询盈亏报表事件
    $(".move-content").find("p").click(function(){
         var value = $(this).attr("data-value");
         var todate = new Date();
 			  
 		if(value=="today"){
 			agentOrderPage.queryParam.reportType=1;
 			agentOrderPage.queryParam.createdDateStart=todate.setHours(3,-1,60,999);
 			agentOrderPage.queryParam.createdDateEnd=todate.AddDays(1).setHours(2,59,59,999);
 			$(".header-right").find("span").html("今天");
 		}else if(value=="yesterDay"){
 			agentOrderPage.queryParam.reportType=0;
 			agentOrderPage.queryParam.createdDateEnd=todate.setHours(2,59,59,999);
 			agentOrderPage.queryParam.createdDateStart=todate.AddDays(-1).setHours(3,-1,60,999);
 			$(".header-right").find("span").html("昨天");
 		}else if(value=="month"){
 			agentOrderPage.queryParam.reportType=0;
 			var defalutDate=new Date();
 			defalutDate.setDate(1);
 			defalutDate.setHours(3,0,0,0);
 			agentOrderPage.queryParam.createdDateStart=defalutDate;
 			agentOrderPage.queryParam.createdDateEnd=todate.setHours(2,59,59,999);
 			$(".header-right").find("span").html("本月");
 		}
 		     
 		var parent=$(this).closest("div.lottery-classify1");
 		parent.hide();
 		$(".alert-bg").stop(false,true).fadeToggle(500);
 		agentOrderPage.getUserBetPage(agentOrderPage.queryParam,1);
    })
    agentOrderPage.getUserBetPage(agentOrderPage.queryParam,agentOrderPage.pageParam.pageNo);
});

AgentOrderPage.prototype.getUserBetPage=function(queryParam,pageNo){
	var lotteryType=$(".lottery-name span p").attr("data-value");
	var userName=$("#userName").val();
	if(userName!=null && userName!=""){
		agentOrderPage.queryParam.userName=userName;
	}else{
		agentOrderPage.queryParam.userName=null;
	}
	if(lotteryType!=null && lotteryType!=""){
		agentOrderPage.queryParam.lotteryType=lotteryType;
	}else{
		agentOrderPage.queryParam.lotteryType=null;
	}
	agentOrderPage.queryParam.pageNo=pageNo;
	var query = {
		"userName" : agentOrderPage.queryParam.userName,
		"lotteryType" : agentOrderPage.queryParam.lotteryType,
		"createdDateStart" : agentOrderPage.queryParam.createdDateStart,
		"createdDateEnd" : agentOrderPage.queryParam.createdDateEnd,
		"prostateStatus" : agentOrderPage.queryParam.prostateStatus,
		"reportType" : agentOrderPage.queryParam.reportType,
		"queryScope" : agentOrderPage.queryParam.queryScope,
	};
	var jsonData = {
		"query" : query,
		"pageNo" : agentOrderPage.queryParam.pageNo,
	};
	//查询用户下级投注
	$.ajax({
		type : "POST",
		contentType : "application/json",
		url : contextPath + "/order/orderQuery",
		data : JSON.stringify(jsonData),
		success : function(result) {
			commonHandlerResult(result, this.url);
			if(result.code == "ok"){
				agentOrderPage.refreshUserOrderPages(result.data);
			}else if(result.code == "error"){
				showErrorDlg(jQuery(document.body), "查询下级投注记录数据失败.");
			}
		}
	});
}

/**
 * 刷新列表数据
 */
AgentOrderPage.prototype.refreshUserOrderPages = function(page){
	$(".user-btn").html("正在加载中...");
	var userOrderListObj = $("#"+(agentOrderPage.queryParam.prostateStatus==null?"all":agentOrderPage.queryParam.prostateStatus));
	var str = "";
	var userOrders = page.pageContent;
	
	//如果是第一页
	if(page.pageNo == 1) {
		userOrderListObj.html("");
		if(userOrders.length == 0){
			str += '<div class="empty-tip">';
			str += '<h4 class="nodata">暂无数据</h4>';
	    	str += '</div>';
	       
	    	userOrderListObj.append(str);
	    	str = "";
	    	$(".user-btn").hide();
	    };
	} 
    if(userOrders != null && userOrders.length > 0){
    	//记录数据显示
    	for(var i = 0 ; i < userOrders.length; i++){
    		var userOrder = userOrders[i];
    		var lotteryIdStr = userOrder.lotteryId;
    		//订单状态特殊处理
    		var prostateStatusShow = userOrder.prostateStatusDes;
    		var prostateStatus = userOrder.prostateStatus;
    		if(prostateStatus == "WINNING"){
    			str += " <a href=\'"+contextPath+"\\gameBet\\orderDetail.html?id="+ userOrder.id +"\'><div class='line open'>";
    		}else{
    			str += " <a href=\'"+contextPath+"\\gameBet\\orderDetail.html?id="+ userOrder.id +"\'><div class='line'>";
    		}
    		str += "	<div class='periods'>";
    		str += "		<p class='period'>"+ userOrder.expect +"</p>";
    		str += "		<p class='time'>"+ userOrder.createdDateStr +"</p>";
    		str += "	</div>";
    		str += "	<div class='username'>"+userOrder.userName+"</div>";
    		str += "	<div class='content'>";
    		str += "		<h2 class='title'>"+ userOrder.lotteryTypeDes +"</h2>";
    		str += "		<div class='info'>";
    		str += "		<span class='money'>"+ userOrder.payMoney.toFixed(2) +"</span>";
    		str += "		</div>";
    		str += "	</div>";
    		
			if(prostateStatus == "DEALING"){
				str += "	<div class='status font-yellow'>"+prostateStatusShow+"</div>";
			}else if(prostateStatus == "WINNING"){
				str += "	<div class='status font-red'>"+prostateStatusShow+"</div>";
				str += "	<div class='status2'>¥ "+ userOrder.winCost +"</div>";
				
			}else if(prostateStatus == "NOT_WINNING"){
				str += "	<div class='status font-green'>"+prostateStatusShow+"</div>";
			}else if(prostateStatus == "ORDER_CANCELED"){
				str += "	<div class='status font-gray'>"+prostateStatusShow+"</div>";
			}
    		
    		str += " </div></a>";
    		userOrderListObj.append(str);
    		str = "";
    	}
    	
    	$(".user-btn").hide();
    	//如果当前页数小于总页数，显示加载更多
    	if(page.pageNo < page.totalPageNum) {
    		var a =$("#"+(agentOrderPage.queryParam.prostateStatus==null?"all":agentOrderPage.queryParam.prostateStatus)).find("a");
    		if(a.length > 0 ){
    			$(".user-btn").html("加载更多");
    			$(".user-btn").show();
    			//添加加载更多事件
        		$(".user-btn").unbind("click").click(function(){
        			agentOrderPage.pageParam.pageNo++;
        			agentOrderPage.getUserBetPage(agentOrderPage.queryParam,agentOrderPage.pageParam.pageNo);
        		});
    		}else{
    			$(".user-btn").hide();
				str += '<div class="line">';
		    	str += 		'<h4 class="nodata">暂无数据</h4>';
		    	str += '</div>';
		    	userOrderListObj.html(str);
		    	str = "";
    		}
    		
    	}else{
    		$(".user-btn").hide();
    	};
    };
  
	
};

AgentOrderPage.prototype.getTabProstate=function(prostate){
	if(prostate!=null && prostate!=""){
		agentOrderPage.queryParam.prostateStatus=prostate;
	}else{
		agentOrderPage.queryParam.prostateStatus=null;
	}
	agentOrderPage.getUserBetPage(agentOrderPage.queryParam,1);
}
