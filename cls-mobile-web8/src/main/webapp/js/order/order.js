function OrderPage(){}
var userOrderPage = new OrderPage();

userOrderPage.param = {
   	
};

/**
 * 查询参数
 */
userOrderPage.queryParam = {
		lotteryType : null,
		createdDateStart : null,
		createdDateEnd : null,
	    lotteryId : null,
	    userName:null,
		lotteryId:null,
		lotteryKind:null,
		lotteryModel:null,
		prostateStatus:null,
		queryScope:1,
};

//分页参数
userOrderPage.pageParam = {
        queryMethodParam : null,
        pageMaxNo : 1,  //最大的页数
		pageNo :null,
        skipHtmlStr : ""
};

$(document).ready(function() {
	// animate_add(".user_order .main .line","fadeInLeft");
	/*var status = status;*/
	userOrderPage.pageParam.pageNo = 1;
	//处理订单状态
	if(status == "notWinning"){
		status = "NOT_WINNING";
		$(".header-title").html("未中奖记录");
	}else if(status == "notOpenPrize"){
		status = "DEALING";
		$(".header-title").html("未开奖记录");
	}else if(status == "win"){
		status = "WINNING";
		$(".header-title").html("中奖记录");
	}else if(status == "bettingRecords"){
		status = "";
		$(".header-title").html("投注记录");
	}
	//进入页面查询
	userOrderPage.findUserOrderByQueryParam(status);
});

/**
 * 加载所有的投注记录
 */
OrderPage.prototype.getAllUserOrders = function(status){
	userOrderPage.pageParam.queryMethodParam = 'getAllUserOrders';
	userOrderPage.queryParam = {};
	userOrderPage.queryParam.queryScope = 1;
	userOrderPage.queryParam.prostateStatus = status==""?null:status;
	userOrderPage.queryConditionUserOrders(userOrderPage.queryParam,userOrderPage.pageParam.pageNo,status);
};


/**
 * 条件查询投注记录
 */
OrderPage.prototype.queryConditionUserOrders = function(queryParam,pageNo,status){
	var jsonData = {
		"query" : userOrderPage.queryParam,
		"pageNo" : userOrderPage.pageParam.pageNo,
	};
	$.ajax({
		type : "POST",
		contentType : 'application/json;charset=utf-8',
		url : contextPath + "/order/orderQuery",
		data : JSON.stringify(jsonData),
		success : function(result) {
			commonHandlerResult(result, this.url);
			if(result.code == "ok"){
				userOrderPage.refreshOrderPages(result.data,status);
			}else if(result.code == "error"){
				showTip(result.data);
			}
		}
	});
};


/**
 * 刷新列表数据
 */
OrderPage.prototype.refreshOrderPages = function(page,status){
	$(".user-btn").html("正在加载中...");
	var userOrderListObj = $("#userOrderList");
	var str = "";
	var userOrders = page.pageContent;
	
	//如果是第一页
	if(page.pageNo == 1) {
		userOrderListObj.html("");
		if(userOrders.length == 0){
			str += '<div class="line">';
	    	str += 		'<h4 class="nodata">暂无数据</h4>';
	    	str += '</div>';
	       
	    	userOrderListObj.append(str);
	    	str = "";
	    	$(".user-btn").hide();
	    };
	} 
    if(userOrders != null && userOrders.length > 0){
    	//记录数据显示
    	for(var i = 0 ; i < userOrders.length; i++){
    		var userOrder = userOrders[i];
    		var lotteryIdStr = userOrder.lotteryId;
    		//订单状态特殊处理
    		var prostateStatusShow = userOrder.prostateStatusDes;
    		var prostateStatus = userOrder.prostateStatus;
    		if(prostateStatus == "WINNING"){
    			str += " <a href=\'"+contextPath+"\\gameBet\\orderDetail.html?id="+ userOrder.id +"\'><div class='line open'>";
    		}else{
    			str += " <a href=\'"+contextPath+"\\gameBet\\orderDetail.html?id="+ userOrder.id +"\'><div class='line'>";
    		}
    		str += "	<div class='periods'>";
    		str += "		<p class='period'>"+ userOrder.expect +"</p>";
    		str += "		<p class='time'>"+ userOrder.createdDateStr +"</p>";
    		str += "	</div>";
    		str += "	<div class='content'>";
    		str += "		<h2 class='title'>"+ userOrder.lotteryTypeDes +"</h2>";
    		str += "		<div class='info'>";
    		str += "		<span class='money'>"+ userOrder.payMoney.toFixed(2) +"</span>";
    		str += "		</div>";
    		str += "	</div>";
    		
			if(prostateStatus == "DEALING"){
				str += "	<div class='status font-yellow'>"+prostateStatusShow+"</div>";
			}else if(prostateStatus == "WINNING"){
				str += "	<div class='status font-red'>"+prostateStatusShow+"</div>";
				str += "	<div class='status2'>¥ "+ userOrder.winCost +"</div>";
				
			}else if(prostateStatus == "NOT_WINNING"){
				str += "	<div class='status font-green'>"+prostateStatusShow+"</div>";
			}else if(prostateStatus == "ORDER_CANCELED"){
				str += "	<div class='status font-gray'>"+prostateStatusShow+"</div>";
			}
    		
    		str += " </div></a>";
    		userOrderListObj.append(str);
    		str = "";
    	}
    	$(".user-btn").hide();
    	//如果当前页数小于总页数，显示加载更多
    	if(page.pageNo < page.totalPageNum) {
    		var a =$("#userOrderList").find("a");
    		if(a.length > 0 ){
    			$(".user-btn").html("加载更多");
    			$(".user-btn").show();
    			//添加加载更多事件
        		$(".user-btn").unbind("click").click(function(){
        			userOrderPage.pageParam.pageNo++;
        			userOrderPage.findUserOrderByQueryParam(status);
        		});
    		}else{
    			$(".user-btn").hide();
				str += '<div class="line">';
		    	str += 		'<h4 class="nodata">暂无数据</h4>';
		    	str += '</div>';
		    	userOrderListObj.html(str);
		    	str = "";
    		}
    		
    	};
    }else{
    	$(".user-btn").hide();
    };
  
	
};

/**
 * 按页面条件查询数据
 */
OrderPage.prototype.findUserOrderByQueryParam = function(status){
	userOrderPage.pageParam.queryMethodParam = 'findUserOrderByQueryParam';
	
	userOrderPage.queryParam.lotteryType = $("#lotteryType").val();
	userOrderPage.queryParam.userName = null;
	userOrderPage.queryParam.lotteryId = null;
	userOrderPage.queryParam.queryScope = 1;
	userOrderPage.queryParam.lotteryKind = null;
	userOrderPage.queryParam.lotteryModel = null;
	userOrderPage.queryParam.prostateStatus = status==""?null:status;
	userOrderPage.queryConditionUserOrders(userOrderPage.queryParam,userOrderPage.pageParam.pageNo,status);
};


/**
 * 根据条件查找对应页
 */
OrderPage.prototype.pageQuery = function(pageNo){
	if(pageNo < 1){
		userOrderPage.pageParam.pageNo = 1;
	}else{
		userOrderPage.pageParam.pageNo = pageNo;
	}
	//如果页码小于等于0，则不进行分页查找
	if(userOrderPage.pageParam.pageNo <= 0){
		return;
	}
	
	userOrderPage.findUserOrderByQueryParam(status);
	
};

