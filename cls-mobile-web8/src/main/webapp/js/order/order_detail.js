function OrderDetailPage() {
}
var orderDetailPage = new OrderDetailPage();

/**
 * 投注信息参数
 */
orderDetailPage.param = {
	lotteryId : null,
	lotteryCodes : new Array()
};

orderDetailPage.kindKeyValue={
		summationDescriptionMap:new JS_OBJECT_MAP()
};
$(document).ready(function() {

	// animate_add(".user_order_detail .main .head","fadeInDown");
	// 	animate_add(".user_order_detail .main .content","zoomIn");
	// 	animate_add(".user_order_detail .main .foot","fadeInUp");
	
	 orderDetailPage.param.lotteryId = id;
	 
	 orderDetailPage.initShengXiaoNumber();
	 
		if(orderDetailPage.param.lotteryId != null){
			orderDetailPage.getLotteryMsg();
		}else{
			showTip("投注参数传递错误.");
		}
		
		//进入页面清空开奖号码内容
		$("#opencodes").html("");
		
		//撤单按钮事件
		$("#stopOrderButton").unbind("click").click(function(){
			if(comfirm("温馨提示！","确认撤销该投注订单吗?","取消",orderDetailPage.cancleFun,"确定",orderDetailPage.stopOrder)){
				orderDetailPage.stopOrder();
			}
		});
		
		//撤单取消
		$(".rightck_5").unbind("click").click(function() {
			$(".rightck").hide();
		});
		//撤单
		$(".rightck_7").unbind("click").click(function() {
			$(".rightck").hide();
			orderDetailPage.stopOrder();
		});
});

OrderDetailPage.prototype.cancleFun = function() {
	showTip("取消成功！");
};

/**
 * 获取投注信息
 */
OrderDetailPage.prototype.getLotteryMsg = function() {
	var jsonData = {
			"orderId" : orderDetailPage.param.lotteryId,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/order/getOrderById",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if(result.code == "ok"){
				orderDetailPage.showLotteryMsg(result.data);
			}else if(result.code == "error"){
				showTip(result.data);
			}
		}
	});
};

/**
 * 展示投注信息
 */
OrderDetailPage.prototype.showLotteryMsg = function(order) {
	if(order != null){
		var expect = order.expect;
		$("#lotteryTypeDes").text(order.lotteryTypeDes);
		//根据彩种换log图片
		$(".logo img").attr("src",orderDetailPage.getPicsrcByLotteryKind(order.lotteryType));
		//福彩3D期号特殊处理
		if(order.lotteryType == 'FCSDDPC' || order.lotteryType == 'BJPK10' || order.lotteryType == 'JSPK10' 
			|| order.lotteryType == 'BJKS' || order.lotteryType == 'HGFFC' || order.lotteryType == 'XJPLFC' || order.lotteryType == 'XYEB' || order.lotteryType == 'LHC'){
			$("#lotteryExpect").text("期号："+ expect);
		}else{
			$("#lotteryExpect").text("期号："+ expect.substring(0,8) + "-" + expect.substring(8,expect.length));
		}
		
		$("#lotteryId").text(order.lotteryId);
		$("#lotteryCreateDate").text(order.createdDateStr);
		$("#lotteryPayMoney").text(parseFloat(order.payMoney).toFixed(2));
		$("#lotteryWinCost").text(parseFloat(order.winCost).toFixed(2));
		//如果是追号记录的话
		if(order.afterNumber == 1){
			$("#toShowAfterNumberOrder").show();
			
			$("#toShowAfterNumberOrder").attr("href",contextPath + "/gameBet/lotteryafternumbermsg/"+order.lotteryId+"/lotteryafternumbermsg.html");
		}
		//是否展示撤单按钮
		if(order.status == "GOING"){
			$("#stopOrderButton").show();
		}else{
			$("#stopOrderButton").hide();
		}
		
		if(order.openCode != null){
			var lotteryCodeDiv=orderDetailPage.showLotteryCode(order.openCode);
			var lotteryLhcCodeDiv=orderDetailPage.showLotteryLhcCode(order.openCode);
			if(order.lotteryType.indexOf("KS") > 0){
				var str ="";
				str += "			<img src='images/dice_pic/gray/1.png' alt='' class='dice' id='lotteryNumber1'>";
				str += "			<img src='images/dice_pic/gray/3.png' alt='' class='dice' id='lotteryNumber2'>";
				str += "			<img src='images/dice_pic/gray/6.png' alt='' class='dice' id='lotteryNumber3'>";
				$("#opencodes").html(str);
				//快三开奖图片显示
				var openCode =order.openCode.split(",");
				var lotteryCode = new Object();
				lotteryCode.numInfo1 = openCode[0];
				lotteryCode.numInfo2 = openCode[1];
				lotteryCode.numInfo3 = openCode[2];
				orderDetailPage.showLastLotteryPictureCode(lotteryCode,null);
			}else{
				if(order.lotteryType == "LHC"){
					$("#opencodes").html(lotteryLhcCodeDiv);
				}else if(order.lotteryType == "XYEB"){
					var str=order.openCode.split(",");
					var sum=Number(str[0])+Number(str[1])+Number(str[2]);
					lotteryCodeDiv+="<div class='mun "+orderDetailPage.changeColorByxyebCode(sum)+"'>"+ sum +"</div>";
					$("#opencodes").html(lotteryCodeDiv);
					
				}else {
					$("#opencodes").html(lotteryCodeDiv);
				}
			}
			
			// 幸运28显示加减
			if(order.lotteryType == "XYEB"){
				$('.symbol').show();
			}
		}
		if(order.prostate == "WINNING"){
			 $("#statusId").addClass("font-red");
			 $("#statusId").html(order.prostateStatusStr +"<br>￥"+ order.winCost);
		}else if(order.prostate == "NOT_WINNING"){
			 $("#statusId").addClass("font-green");
			 $("#statusId").html(order.prostateStatusStr);
		}else{
			$("#statusId").html(order.prostateStatusStr);
		}
		 
		$("#codeList").html("");
		var codeListObj = $("#codeList");
		var orderCodesMap = order.codeDetailList;//后台将数据封装在列表对象中
		var codeCount = 0;
		var str = "";
		var alertOpencodes = new Array();
		for(var i = 0; i < orderCodesMap.length; i++){
			var orderCodesM = orderCodesMap[i];
			var code = orderCodesM.code;
			alertOpencodes.push(code);
			var key = orderCodesM.key;
			var cathecticCount =  orderCodesM.cathecticCount;
			var codeStr =  "<div class='child child2'>";
			var codeArray = code.split(",");
			for(k=0;k<codeArray.length;k++){
				if(k<codeArray.length-1){
					codeStr += "<span>"+codeArray[k]+"</span>,";
				}else {
					codeStr += "<span>"+codeArray[k]+"</span></div>";
				}
			}
			orderDetailPage.param.lotteryCodes.push(code);
			codeCount++;
			str += "	<div class='line'>";
			if (code.length > 13) {
				str += "		<div class='line-title font-blue'><p>" + code.substring(0,14)+"..."
						+ " <a class='font-gray' onclick=\"element_toggle(true,['.alert'])\">更多</a></p></div>";
			} else {
				str += "		<div class='line-title font-blue'><p>" + codeStr + " </p></div>";
			}
			str += "		<div class='line-content'><p>" + key + "</p></div>";
			str += "	</div>";
			codeListObj.append(str);
			str = "";
		}
		$("#alertOpencodes").text(orderCodesM.code);
	}else{
		showTip("投注订单为空.");
	}
};

/**
 *刷新快三最近三期开奖号码的图片
 */
OrderDetailPage.prototype.showLastLotteryPictureCode = function(lotteryCode,i){
	if(lotteryCode == null || typeof(lotteryCode)=="undefined" || typeof(i)=="undefined"){
		return;
	}

	var id_a ="";
	var id_b ="";
	var id_c ="";
	if(i == null){
		id_a = "lotteryNumber1";
		id_b = "lotteryNumber2";
		id_c = "lotteryNumber3";
	}else{
		id_a = "lotteryNumber1_"+i;
		id_b = "lotteryNumber2_"+i;
		id_c = "lotteryNumber3_"+i;
	}
	
	
	//先移除开奖号码样式（更新开奖号码）
	$('#'+id_a).attr("src","");
	if(lotteryCode.numInfo1 == "1"){
		$('#'+id_a).attr("src",""+contextPath+"/images/dice_pic/gray/1.png");
	}else if(lotteryCode.numInfo1 == "2"){
		$('#'+id_a).attr("src",""+contextPath+"/images/dice_pic/gray/2.png");
	}else if(lotteryCode.numInfo1 == "3"){
		$('#'+id_a).attr("src",""+contextPath+"/images/dice_pic/gray/3.png");
	}else if(lotteryCode.numInfo1 == "4"){
		$('#'+id_a).attr("src",""+contextPath+"/images/dice_pic/gray/4.png");
	}else if(lotteryCode.numInfo1 == "5"){
		$('#'+id_a).attr("src",""+contextPath+"/images/dice_pic/gray/5.png");
	}else if(lotteryCode.numInfo1 == "6"){
		$('#'+id_a).attr("src",""+contextPath+"/images/dice_pic/gray/6.png");
	}
	
	//先移除开奖号码样式（更新开奖号码）
	$('#'+id_b).attr("src","");
	if(lotteryCode.numInfo2 == "1"){
		$('#'+id_b).attr("src",""+contextPath+"/images/dice_pic/gray/1.png");
	}else if(lotteryCode.numInfo2 == "2"){
		$('#'+id_b).attr("src",""+contextPath+"/images/dice_pic/gray/2.png");
	}else if(lotteryCode.numInfo2 == "3"){
		$('#'+id_b).attr("src",""+contextPath+"/images/dice_pic/gray/3.png");
	}else if(lotteryCode.numInfo2 == "4"){
		$('#'+id_b).attr("src",""+contextPath+"/images/dice_pic/gray/4.png");
	}else if(lotteryCode.numInfo2 == "5"){
		$('#'+id_b).attr("src",""+contextPath+"/images/dice_pic/gray/5.png");
	}else if(lotteryCode.numInfo2 == "6"){
		$('#'+id_b).attr("src",""+contextPath+"/images/dice_pic/gray/6.png");
	}
	
	//先移除开奖号码样式（更新开奖号码）
	$('#'+id_c).attr("src","");
	if(lotteryCode.numInfo3 == "1"){
		$('#'+id_c).attr("src",""+contextPath+"/images/dice_pic/gray/1.png");
	}else if(lotteryCode.numInfo3 == "2"){
		$('#'+id_c).attr("src",""+contextPath+"/images/dice_pic/gray/2.png");
	}else if(lotteryCode.numInfo3 == "3"){
		$('#'+id_c).attr("src",""+contextPath+"/images/dice_pic/gray/3.png");
	}else if(lotteryCode.numInfo3 == "4"){
		$('#'+id_c).attr("src",""+contextPath+"/images/dice_pic/gray/4.png");
	}else if(lotteryCode.numInfo3 == "5"){
		$('#'+id_c).attr("src",""+contextPath+"/images/dice_pic/gray/5.png");
	}else if(lotteryCode.numInfo3 == "6"){
		$('#'+id_c).attr("src",""+contextPath+"/images/dice_pic/gray/6.png");
	}
};




/**
 * 撤单事件
 */
OrderDetailPage.prototype.stopOrder = function() {
	var jsonData = {
			"orderId" : orderDetailPage.param.lotteryId,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/order/cancelOrderById",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if(result.code == "ok"){
				showTip("恭喜您,撤单成功.");
				setTimeout("window.location.reload()", 2000);
			}else if(result.code == "error"){
				showTip(result.data);
			}
		}
	});
};

//计算颜色
OrderDetailPage.prototype.changeColorByxyebCode = function(k){
		var colors="";
		if(k==3||k==6||k==9||k==12||k==15||k==18||k==21||k==24){
			colors="color-red";
		}else if(k==1||k==4||k==7||k==10||k==16||k==19||k==22||k==25){
			colors="color-green";
		}else if(k==2||k==5||k==8||k==11||k==17||k==20||k==23||k==26){
			colors="color-blue";
		}else if(k== 0||k==13||k==14||k==27){
			colors="color-gray";
		}
		return colors;
}

/**
 * 处理开奖号码样式
 * @param code
 * @returns {String}
 */
OrderDetailPage.prototype.showLotteryCode = function(code) {
	var cadeArr=code.split(",");
	var str = "";
	for(var i = 0;i< cadeArr.length;i++){
		
		if(i == 0){
			str += "<div class='mun color-red'>"+ cadeArr[0] +"</div><span class='symbol' style='display:none;'>+</span>";
		}else if(i == 1){
			str += "<div class='mun color-red'>"+ cadeArr[1] +"</div><span class='symbol' style='display:none;'>+</span>";
		}else if(i == 2){
			str += "<div class='mun color-red'>"+ cadeArr[2] +"</div><span class='symbol' style='display:none;'>=</span>";
		}else if(i == 3){
			str += "<div class='mun color-red'>"+ cadeArr[3] +"</div>";
		}else if(i == 4){
			str += "<div class='mun color-red'>"+ cadeArr[4] +"</div>";
		}else if(i == 5){
			str += "<div class='mun color-red'>"+ cadeArr[5] +"</div>";
		}else if(i == 6){
			str += "<div class='mun color-red'>"+ cadeArr[6] +"</div>";
		}else if(i == 7){
			str += "<div class='mun color-red'>"+ cadeArr[7] +"</div>";
		}else if(i == 8){
			str += "<div class='mun color-red'>"+ cadeArr[8] +"</div>";
		}else if(i == 9){
			str += "<div class='mun color-red'>"+ cadeArr[9] +"</div>";
		}
	}
	
	return str;
};

/**
 * 六合彩处理开奖号码样式
 * @param code
 * @returns {String}
 */
OrderDetailPage.prototype.showLotteryLhcCode = function(code) {
	var cadeArr=code.split(",");
	var str = "";
	//颜色
	var lotteryCode = new Object();
	lotteryCode.numInfo1 = cadeArr[0];
	lotteryCode.numInfo2 = cadeArr[1];
	lotteryCode.numInfo3 = cadeArr[2];
	lotteryCode.numInfo4 = cadeArr[3];
	lotteryCode.numInfo5 = cadeArr[4];
	lotteryCode.numInfo6 = cadeArr[5];
	lotteryCode.numInfo7 = cadeArr[6];
	var  colors = orderDetailPage.changeColorByCode(lotteryCode);
	for(var i = 0;i< cadeArr.length;i++){
		
		if(i == 0){
			str += "<div class='lhc-item'><div class='mun lhc-"+colors[0]+"'>"+ cadeArr[0] +"</div><p class='lhc-shengxiao lhc-"+colors[0]+"'>"+orderDetailPage.returnAninal(cadeArr[0])+"</p></div>";
		}else if(i == 1){
			str += "<div class='lhc-item'><div class='mun lhc-"+colors[1]+"'>"+ cadeArr[1] +"</div><p class='lhc-shengxiao lhc-"+colors[1]+"'>"+orderDetailPage.returnAninal(cadeArr[1])+"</p></div>";
		}else if(i == 2){
			str += "<div class='lhc-item'><div class='mun lhc-"+colors[2]+"'>"+ cadeArr[2] +"</div><p class='lhc-shengxiao lhc-"+colors[2]+"'>"+orderDetailPage.returnAninal(cadeArr[2])+"</p></div>";
		}else if(i == 3){
			str += "<div class='lhc-item'><div class='mun lhc-"+colors[3]+"'>"+ cadeArr[3] +"</div><p class='lhc-shengxiao lhc-"+colors[3]+"'>"+orderDetailPage.returnAninal(cadeArr[3])+"</p></div>";
		}else if(i == 4){
			str += "<div class='lhc-item'><div class='mun lhc-"+colors[4]+"'>"+ cadeArr[4] +"</div><p class='lhc-shengxiao lhc-"+colors[4]+"'>"+orderDetailPage.returnAninal(cadeArr[4])+"</p></div>";
		}else if(i == 5){
			str += "<div class='lhc-item'><div class='mun lhc-"+colors[5]+"'>"+ cadeArr[5] +"</div> + <p class='lhc-shengxiao lhc-"+colors[5]+"'>"+orderDetailPage.returnAninal(cadeArr[5])+"</p></div>";
		}else if(i == 6){
			str += "<div class='lhc-item'><div class='mun lhc-"+colors[6]+"'>"+ cadeArr[6] +"</div><p class='lhc-shengxiao lhc-"+colors[6]+"'>"+orderDetailPage.returnAninal(cadeArr[6])+"</p></div>";
		}
	}
	
	return str;
};


/**
 * //根据彩获取对应的log图片
 */
OrderDetailPage.prototype.getPicsrcByLotteryKind = function(currentLotteryKind){
	var picSrc = contextPath+"\\images\\lottery_logo\\index\\";
	if("JSKS" == currentLotteryKind || "HBKS" == currentLotteryKind || "GXKS" == currentLotteryKind
			|| "AHKS" == currentLotteryKind || "JLKS" == currentLotteryKind || "GSKS" == currentLotteryKind
			|| "BJKS" == currentLotteryKind || "JYKS" == currentLotteryKind || "SHKS" == currentLotteryKind
			|| "SFKS" == currentLotteryKind || "WFKS" == currentLotteryKind) {
		picSrc += "logo_kuaisan.png";
	} else if("CQSSC" == currentLotteryKind || "TJSSC" == currentLotteryKind
			|| "XJSSC" == currentLotteryKind || "JXSSC" == currentLotteryKind
			|| "SFSSC" == currentLotteryKind || "WFSSC" == currentLotteryKind) {
		picSrc += "logo_shishicai.png";
	} else if("FJSYXW" == currentLotteryKind || "SDSYXW" == currentLotteryKind
			|| "GDSYXW" == currentLotteryKind || "JXSYXW" == currentLotteryKind) {
		picSrc += "logo_shiyixuanwu.png";
	} else if("JLFFC" == currentLotteryKind) {
		picSrc += "logo_fenfencai.png";
	} else if("BJPK10" == currentLotteryKind) {
		picSrc += "logo_pk10.png";
	} else if("JSPK10" == currentLotteryKind) {
		picSrc += "logo_jspk10.png";
	} else if("XJPLFC" == currentLotteryKind) {
		picSrc += "logo_erfencai.png";
	}  else if("TWWFC" == currentLotteryKind) {
		picSrc += "logo_wufencai.png";
	}   else if("HGFFC" == currentLotteryKind) {
		picSrc += "logo_yidianwufencai.png";
	}else if("FCSDDPC" == currentLotteryKind){
		picSrc += "logo_dipincai.png";
	}else if( currentLotteryKind.search("LHC") != -1){
		picSrc += "logo_liuhecai.png";
	}else if("XYEB" == currentLotteryKind){
		picSrc += "logo_xyeb.png";
	}else {
		//使用默认的添加图片
		picSrc = "";
	}
	return picSrc;
};

//六合彩参数
orderDetailPage.shengxiao = {
		shengxiaoKey:new Array('鼠','牛','虎','兔','龙','蛇','马','羊','猴','鸡','狗','猪'),
	    shengxiaoArr: []
}
//波色
orderDetailPage.boshe = {
		hongbo:[1,2,7,8,12,13,18,19,23,24,29,30,34,35,40,45,46],
		lanbo:[3,4,9,10,14,15,20,25,26,31,36,37,41,42,47,48],
		lvbo:[5,6,11,16,17,21,22,27,28,32,33,38,39,43,44,49]
}


OrderDetailPage.prototype.returnAninal = function(opencode){
	var shengxiaoArr =orderDetailPage.shengxiao.shengxiaoArr;
	for(var i=0;i<shengxiaoArr.length;i++){		
		for(var j=0;j<shengxiaoArr[i].length;j++){
			if(Number(opencode) == Number(shengxiaoArr[i][j])){
				return orderDetailPage.shengxiao.shengxiaoKey[i];
			}
		}
	}
	return "-";
}

/**
 * 初始化生肖号码描述
 */
OrderDetailPage.prototype.initShengXiaoNumber = function (){
	$.ajax({
		type : "POST",
		url : contextPath + "/code/getShengXiaoNumber",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if(result.code == "ok"){
				orderDetailPage.shengxiao.shengxiaoArr = result.data;
			}else if(result.code == "error"){
				frontCommonPage.showKindlyReminder(result.data);
				$("#editUserButton").removeAttr("disabled");
			}
		}
	});
}

//计算颜色
OrderDetailPage.prototype.changeColorByCode = function(lotteryCode){
	var colors = new Array();
	var hong = orderDetailPage.boshe.hongbo;
	var lan = orderDetailPage.boshe.lanbo;
	var lv  = orderDetailPage.boshe.lvbo;
	
	//判断是否是红波
	for(var i=0;i<hong.length;i++){
		if(Number(hong[i])==Number(lotteryCode.numInfo1)){
			colors[0] = 'red';
		}
		if(Number(hong[i])==Number(lotteryCode.numInfo2)){
			colors[1] = 'red';
		}
		if(Number(hong[i])==Number(lotteryCode.numInfo3)){
			colors[2] = 'red';
		}
		if(Number(hong[i])==Number(lotteryCode.numInfo4)){
			colors[3] = 'red';
		}
		if(Number(hong[i])==Number(lotteryCode.numInfo5)){
			colors[4] = 'red';
		}
		if(Number(hong[i])==Number(lotteryCode.numInfo6)){
			colors[5] = 'red';
		}
		if(Number(hong[i])==Number(lotteryCode.numInfo7)){
			colors[6] = 'red';
		}
	}
	
	//判断是否是蓝波
	for(var i=0;i<lan.length;i++){
		if(Number(lan[i])==Number(lotteryCode.numInfo1)){
			colors[0] = 'blue';
		}
		if(Number(lan[i])==Number(lotteryCode.numInfo2)){
			colors[1] = 'blue';
		}
		if(Number(lan[i])==Number(lotteryCode.numInfo3)){
			colors[2] = 'blue';
		}
		if(Number(lan[i])==Number(lotteryCode.numInfo4)){
			colors[3] = 'blue';
		}
		if(Number(lan[i])==Number(lotteryCode.numInfo5)){
			colors[4] = 'blue';
		}
		if(Number(lan[i])==Number(lotteryCode.numInfo6)){
			colors[5] = 'blue';
		}
		if(Number(lan[i])==Number(lotteryCode.numInfo7)){
			colors[6] = 'blue';
		}
	}
	
	//判断是否是绿波
	for(var i=0;i<lv.length;i++){
		if(Number(lv[i])==Number(lotteryCode.numInfo1)){
			colors[0] = 'green';
		}
		if(Number(lv[i])==Number(lotteryCode.numInfo2)){
			colors[1] = 'green';
		}
		if(Number(lv[i])==Number(lotteryCode.numInfo3)){
			colors[2] = 'green';
		}
		if(Number(lv[i])==Number(lotteryCode.numInfo4)){
			colors[3] = 'green';
		}
		if(Number(lv[i])==Number(lotteryCode.numInfo5)){
			colors[4] = 'green';
		}
		if(Number(lv[i])==Number(lotteryCode.numInfo6)){
			colors[5] = 'green';
		}
		if(Number(lv[i])==Number(lotteryCode.numInfo7)){
			colors[6] = 'green';
		}
	}
	
	return colors;
}
