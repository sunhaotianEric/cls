function RechargeOrderPage(){}
var rechargeOrderPage = new RechargeOrderPage();

rechargeOrderPage.param = {
	queryType : "RECHARGE"
};

/**
 * 查询参数
 */
rechargeOrderPage.queryParam = {
		statuss : null,
		payType : null,
		createdDateStart : null,
		createdDateEnd : null,
		dateRange :1,
		
};

//分页参数
rechargeOrderPage.pageParam = {
        queryMethodParam : null,
        pageNo : 1,
        pageMaxNo : 1,  //最大的页数
        skipHtmlStr : ""
};

$(document).ready(function() {

	animate_add(".user_recharge_orderCtrl .main","fadeInUp");
	var strDate = new Date();
	rechargeOrderPage.queryParam.createdDateStart=strDate.setHours(3,-1,60,999);
	rechargeOrderPage.queryParam.createdDateEnd=strDate.AddDays(1).setHours(2,59,-1,999);
	//点击切换查询
    $(".move-content").find("p").click(function(){
         var value = $(this).attr("data-value");
         var todate = new Date();
 			  
 		if(value=="today"){
 			rechargeOrderPage.queryParam.reportType = 1;
 			rechargeOrderPage.queryParam.dateRange = 1;
 			/*rechargeOrderPage.queryParam.createdDateStart=todate.setHours(3,-1,60,999);
 			rechargeOrderPage.queryParam.createdDateEnd=todate.AddDays(1).setHours(2,59,-1,999);*/
 			$(".header-right").find("span").html("今天");
 		}else if(value=="yesterDay"){
 			rechargeOrderPage.queryParam.reportType=0;
 			rechargeOrderPage.queryParam.dateRange = -1;
 			/*rechargeOrderPage.queryParam.createdDateEnd=todate.setHours(2,59,59,999);
 			rechargeOrderPage.queryParam.createdDateStart=todate.AddDays(-1).setHours(3,-1,60,999);*/
 			$(".header-right").find("span").html("昨天");
 		}else if(value=="month"){
 			rechargeOrderPage.queryParam.reportType=0;
 			rechargeOrderPage.queryParam.dateRange = 30;
 			/*var defalutDate=new Date();
 			defalutDate.setDate(1);
 			defalutDate.setHours(3,0,0,0);
 			rechargeOrderPage.queryParam.createdDateStart=defalutDate;
 			rechargeOrderPage.queryParam.createdDateEnd=todate.setHours(2,59,-1,999);*/
 			$(".header-right").find("span").html("本月");
 		}else if(value=="lastmonth"){
 			rechargeOrderPage.queryParam.reportType=0;
 			rechargeOrderPage.queryParam.dateRange = -30;
			/* var defalutDate=new Date();
			 defalutDate.setDate(1);
			 defalutDate.setHours(3,0,0,0);
			 todate.setMonth(todate.getMonth()-1);
			 todate.setDate(1);
			 todate.setHours(3,0,0,0);
			 rechargeOrderPage.queryParam.createdDateStart=todate;
			 rechargeOrderPage.queryParam.createdDateEnd=defalutDate;*/
			 $(".header-right").find("span").html("上个月");
		}
 		     
 		var parent=$(this).closest("div.lottery-classify1");
 		parent.hide();
 		$(".alert-bg").stop(false,true).fadeToggle(500);
 		rechargeOrderPage.findUserRechargeOrderByQueryParam();
    })
	
	//进入页面查询充值明细
	rechargeOrderPage.findUserRechargeOrderByQueryParam();
});



/**
 * 加载所有的提现记录
 */
RechargeOrderPage.prototype.getWithDrawOrderDetails = function(){
	rechargeOrderPage.pageParam.queryMethodParam = 'getWithDrawOrderDetails';
	rechargeOrderPage.queryParam = {};
	rechargeOrderPage.queryConditionUserWithDrawOrders(userWithDrawOrderPage.queryParam,userWithDrawOrderPage.pageParam.pageNo);
};

/**
 * 按页面条件查询充值数据
 */
RechargeOrderPage.prototype.findUserRechargeOrderByQueryParam = function(){
	rechargeOrderPage.pageParam.queryMethodParam = 'findUserRechargeOrderByQueryParam';
	rechargeOrderPage.queryConditionUserRechargeOrders(rechargeOrderPage.queryParam,rechargeOrderPage.pageParam.pageNo);
};

/**
 * 刷新列表数据
 */
RechargeOrderPage.prototype.refreshUserWithdrawOrderPages = function(page){
	$(".span_more").remove();
	var userWithDrawOrderListObj = $("#userWithDrawOrderList");
	
	var str = "";
	var userWithDrawOrders = page.pageContent;
	
	//如果是第一页
	if(page.pageNo == 1) {
		userWithDrawOrderListObj.html("");
		if(userWithDrawOrders.length == 0){
	    	
			str += '<div class="line">';
	    	str += 		'<h4 class="nodata">暂无数据</h4>';
	    	str += '</div>';
	    	
	    	userWithDrawOrderListObj.append(str);
	    }
	} 
    if(userWithDrawOrders != null && userWithDrawOrders.length > 0){
    	//记录数据显示
    	for(var i = 0 ; i < userWithDrawOrders.length; i++){
    		
    		var userWithDrawOrder = userWithDrawOrders[i];
    		var serialNumberStr = userWithDrawOrder.serialNumber;
    		
    		str += '<div class="line">';
    		str += 		'<div class="child child1"><p>'+  serialNumberStr.substring(serialNumberStr.lastIndexOf('_')+1,serialNumberStr.length) +'</p></div>';
    		str += 		'<div class="child child2"><p>'+ (userWithDrawOrder.applyValue).toFixed(frontCommonPage.param.fixVaue)+'</p></div>';
    		/*str += 		'<div class="child child3"><p>'+ (userWithDrawOrder.accountValue + userWithDrawOrder.feeValue).toFixed(frontCommonPage.param.fixVaue)+'</p></div>';*/

      		var payWayName = userWithDrawOrder.bankType;
    		var payType = userWithDrawOrder.payType;
    		var payTypeDes ="未知";
    		if(payType == "ALIPAY"){
    			payTypeDes ="支付宝";
    		}else if(payType == "WEIXIN"){
    			payTypeDes ="微信";
    		}else if(payType == "BANK_TRANSFER"){
    			payTypeDes ="网银";
    		}else if(payType == "QUICK_PAY"){
    			payTypeDes ="快捷支付";
    		}else if(payType == "ALIPAYWAP"){
    			payTypeDes ="支付宝WAP";
    		}else if(payType == "WEIXINWAP"){
    			payTypeDes ="微信WAP";
    		}else if(payType == "QQPAY"){
    			payTypeDes ="QQ钱包";
    		}else if(payType == "QQPAYWAP"){
    			payTypeDes ="QQ钱包WAP";
    		}else if(payType == "UNIONPAY"){
    			payTypeDes ="银联扫码";
    		}else if(payType == "JDPAY"){
    			payTypeDes ="京东扫码";
    		}else if(payType == "WEIXINBARCODE"){
    			payTypeDes ="微信条形码";
    		}
    		str += 		'<div class="child child4"><p>'+ payTypeDes +'</p></div>';
    		str += 		'<div class="child child5"><p>'+ userWithDrawOrder.createdDateStr +'</p></div>';
    		
    		if(userWithDrawOrder.statusStr == "支付成功"){
    			str += 		'<div class="child child6 font-green">';
    		}else if(userWithDrawOrder.statusStr == "支付失败"){
    			str += 	'<div class="child child6 font-red">';
    		}else if(userWithDrawOrder.statusStr == "支付中"){
    			str += 	'<div class="child child6 font-yellow">';
    		}else{
    			str += 	'<div class="child child6 font-blue">';
    		}
    		str += '<p>'+ userWithDrawOrder.statusStr +'</p></div>';
    		str += '</div>';
    		
    		userWithDrawOrderListObj.append(str);
    		str = "";
    	}
    	//如果当前页数小于总页数，显示加载更多
    	if(page.pageNo < page.totalPageNum) {
    		$(".user-btn").show();
    		//添加加载更多事件
    		$(".user-btn").unbind("click").click(function(){
    			rechargeOrderPage.pageParam.pageNo++;
    			if(rechargeOrderPage.pageParam.queryMethodParam == "findUserRechargeOrderByQueryParam") {
    				rechargeOrderPage.findUserRechargeOrderByQueryParam();
    			} else {
    				rechargeOrderPage.findUserWithDrawOrderByQueryParam();
    			}
    		});
    	}
    	
    	//绑定点击事件
    	$(".list_item").unbind("click").click(function(){
    		var dataVal = $(this).attr("data-val");
    		//window.location.href = contextPath+"/gameBet/mobile/rechargewithdraw/rechargewithdraw_detail.html?param1="+dataVal+"&param2="+rechargeOrderPage.param.queryType;
    	});
    }
	
};


/**
 * 条件查询充值记录
 */
RechargeOrderPage.prototype.queryConditionUserRechargeOrders = function(queryParam,pageNo){
	var param={}
	if(queryParam.statuss!=null && queryParam.statuss!=""){
		param.statuss=queryParam.statuss;
	}
    if(queryParam.payType!=null && queryParam.payType!=""){
    	param.payType=queryParam.payType;
	}
    param.dateRange = queryParam.dateRange;
   /* param.createdDateStart=new Date(queryParam.createdDateStart).Format("yyyy-MM-ddThh:mm:ss.000Z");
    param.createdDateEnd=new Date(queryParam.createdDateEnd).Format("yyyy-MM-ddThh:mm:ss.000Z");*/
    var queryStr={"query":param,"pageNo":pageNo};
	$.ajax({
		type: "POST",
        url: contextPath +"/recharge/rechargeOrderQuery",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(queryStr), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
            var data=result.data;
        	if(result.code == "ok"){
        		rechargeOrderPage.refreshUserWithdrawOrderPages(data);
        	}else if(result.code == "error"){
        		var err = result.data;
    			showTip(err);
        	}else{
        		showTip("查询充值记录数据失败.");
        	}
        }
	});
};

Date.prototype.Format = function (fmt) {
	    var o = {
	        "M+": this.getMonth() + 1, //月份 
	        "d+": this.getDate(), //日 
	        "h+": this.getHours(), //小时 
	        "m+": this.getMinutes(), //分 
	        "s+": this.getSeconds(), //秒 
	        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
	        "S": this.getMilliseconds() //毫秒 
	    };
	    if (/(y+)/.test(fmt))fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
	    for (var k in o){
	    if (new RegExp("(" + k + ")").test(fmt)) {fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));}
	    }
	    return fmt;
	}
