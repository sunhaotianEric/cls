function RechargeListPage(){}
var rechargeListPage = new RechargeListPage();

rechargeListPage.param = {
	requestProtocol:null
};

$(document).ready(function() {
	rechargeListPage.getAllCanUseRechargeConfig()
  	
	
});

RechargeListPage.prototype.getAllCanUseRechargeConfig = function(){
	var param={};
	param.requestProtocol=rechargeListPage.param.requestProtocol;
	//暂时都只传递http
	param.requestProtocol = "http";
	$.ajax({
		type: "POST",
        url: contextPath +"/recharge/getRechargeWays",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(param), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		var rechargeConfigList = result.data;
        		console.log(rechargeConfigList);
        		if(rechargeConfigList!=null){
        			var content=$("div.content");
        			var divStr='';
        			for(var i=0;i<rechargeConfigList.length;i++){
        				var list=rechargeConfigList[i];
        				var str='';
        				var imgStr=""
        				if(list.payType=="BANK_TRANSFER" || list.payType=="QUICK_PAY"){
        					imgStr=contextPath+"/images/bankcard_logo/yinlian.png";
        			    }else if(list.payType=="ALIPAY" || list.payType=="ALIPAYWAP"){
        			        imgStr=contextPath+"/images/bankcard_logo/zhifubao.png";
        			     
        			    }else if(list.payType=="WEIXIN" || list.payType=="WEIXINWAP" || list.payType=="WEIXINBARCODE"){
        			    	imgStr=contextPath+"/images/bankcard_logo/weixin.png";
        			    	
        			    }else if(list.payType=="QQPAY" || list.payType=="QQPAYWAP"){
        			    	imgStr=contextPath+"/images/bankcard_logo/qqpay.png";
        			    	
        			    }else if(list.payType=="UNIONPAY"){
        			    	imgStr=contextPath+"/images/bankcard_logo/unionpay.png";
        			    	
        			    }else if(list.payType=="JDPAY"){
        			    	imgStr=contextPath+"/images/bankcard_logo/jdpay.png";
        			    	
        			    }
        				 str+='<ul class="user-list"><a href="'+contextPath+'/gameBet/recharge.html?id='+list.id+'"><li class="user-line">'+
			             '<div class="container"><div class="line-title"><img src='+imgStr+' alt=""></div><div class="line-content"><h2>'+list.showName+'</h2>'+
			             '<p>单笔最低'+list.lowestValue+'元，最高'+list.highestValue+'元</p></div>'+
			             '<div class="right"><img class="icon" src="'+contextPath+'/images/icon/icon-pointer.png" /></div></div></li></a></ul>';
        				 
        				 content.after(str);
        			}
        		}
        		
        		
        	}
        }
	});
}
