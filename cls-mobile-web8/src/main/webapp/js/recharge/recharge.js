function RechargePage(){}
var rechargePage = new RechargePage();

rechargePage.param = {
	payBankType: null,
	payQuickBankType: null,
	payBankLowestValue : null,
	payBankHighestValue : null,
	payValue : null,
	payId : null,
	bankUrl : null,
	canclePayId : null,
	payNickName : null,
	costWay : "",
	content:"",
	bankId : null,
	rechargeConfigId : null,
	payBankList:null,
	payType:null,
	refType:null,
	rechargeMoney:null,
	nickName:null,
	
	
};

rechargePage.limitParam = {
	quickRechargeLowestMoney : null,
	quickRechargeHighestMoney : null,
	shanfuQuickRechargeLowestMoney : null,
	shanfuQuickRechargeHighestMoney : null
};

//页面表单进行循环操作

/**
 * 给快捷表单的input文本框赋值银行的码值的回调方法
 */
RechargePage.prototype.getFormToCanRechargerPanks = function(query,content){
	
	var formcontent=content;
	$.ajax({
		type: "POST",
        url: contextPath +"/recharge/getCurrentUserCanRechargerPanks",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(query), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		var data = result.data;
        		rechargePage.showFormToCanRechargerPanks(data,content);
        	}else if(result.code == "error"){
        		var err = result.data;
    			showTip(err);
        	}else{
        		showTip("加载系统可支付银行的请求失败.");
        	}
        }
	});
};

/**
 * 给快捷表单的input文本框赋值银行的码值
 */
RechargePage.prototype.showFormToCanRechargerPanks = function(banks,content){
	if(banks != null && banks.length > 0){
		for(var i = 0; i < banks.length; i++){
			var rechargeBank = banks[i];
			$(content).find("input[name='payId']").val(rechargeBank.payId);
		}
	}
};

/**
 * 获取用户配置信息
 */
RechargePage.prototype.getAllRechargeConfig = function(){
	var param={};
	param.id=rechargePage.param.id;
	$.ajax({
		type: "POST",
        url: contextPath +"/recharge/getRechargeInfo",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(param), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		var data = result.data;
        		var data2 = result.data2;
        		rechargePage.param.payBankList=data2;
    			rechargePage.refreshRecharge(data);
    			
    			/*headerPostion();
    			headerChange("#headerId");*/
    			$('div.header-title').trigger("click");
    			$("div.user-btn.color-blue").each(function(i){
    		  		if($(this).attr("data-pay-type")=="WEIXIN" ||$(this).attr("data-pay-type")=="ALIPAY"||$(this).attr("data-pay-type")=="QQPAY"){
    		  			var query={};
    		  			query.payType=$(this).attr("data-pay-type");
    		  			query.refType=$(this).attr("data-ref-type");
    		  			query.id=$(this).attr("data-id");
    		  			var context="#"+$(this).parents(".content").attr("id");
    		  			rechargePage.getFormToCanRechargerPanks(query,context);
    		  	   }
    		  	})
        	}else if(result.code == "error"){
        		var err = result.data;
        		showTip(err);
        	}else{
        		showTip("获取用户配置信息的请求失败.");
        	}
        }
	});
};
/**
 * 动态渲染页面元素
 * data-pay-type="${list.payType}"  data-ref-type="${list.refType}" onBlur="rechargePage.rechargeMoneyRandom(this)"
 */
RechargePage.prototype.refreshRecharge = function(recharge){
	var rebateIdObj	= $("#rebateId");
	var head= $("#showHeader");
	var content= $("#fundContent");
	var divStr="";
    var str = "";
		if(recharge!=null){
				var rechargeObj=recharge;
				$("#headerId").text(rechargeObj.showName);
				$("#highestValue").text(rechargeObj.highestValue);
				$("#lowestValue").text(rechargeObj.lowestValue);
				rechargePage.param.rechargeConfigId=rechargeObj.id;
				rechargePage.param.payType=rechargeObj.payType;
				rechargePage.param.refType=rechargeObj.refType;
				rechargePage.param.bankType=rechargeObj.bankType;
			   /* str='<h2 class="header-title" id="headerId" onclick="headerChange(this)" data-div="content'+1+'" data-payType="'+rechargeObj.payType+'" data-id="'+rechargeObj.id+'" data-refIdList="'+rechargeObj.refIdList+'" data-refType="'+rechargeObj.refType+'">'+rechargeObj.showName+'</h2>'
				head.after(str);
				var str='<div class="content on" id="content'+1+'">';*/
				$("#payDiv").prev("li").remove();
				$("#selectLineBank").remove();
				if(rechargeObj.payType=="BANK_TRANSFER"){
					
					str='<li class="user-line select bankname" id="selectLineBank" onclick="rechargePage.showRechargeTypes();modal_toggle(\'.bank-modal\')">'+
		               '<div class="container select-container"><div class="line-title"><img src=\"'+contextPath+'\\images\\bankcard_logo\\yinlian.png"' +'alt=""></div><div class="line-title line-banklogo"><img src="" alt=""></div>'+
		               '<div class="line-content"><input type="hidden" name="bankId" value=""><input type="hidden" name="max" value=""><input type="hidden" name="min" value=""><input type="hidden" name="chargePayId" value=""><input type="hidden" name="payId" value="">'+
		               '<h2 class="title"></h2></div><div class="right"><img class="icon" src=\"'+contextPath+'\\images\\icon\\icon-pointer.png" /></div></div></li>';
					$("#selectLine").before(str);
					$("#selectLine").hide();
				}else if(rechargeObj.payType=="WEIXIN" || rechargeObj.payType=="WEIXINWAP" || rechargeObj.payType=="WEIXINBARCODE"){
					$("#selectLine").show();
					/*$("#selectLineBank").remove();*/
					$("#selectLine img").attr("src",contextPath+"\\images\\bankcard_logo\\weixin.png");
					$("#selectLine h2.title").text("微信支付");
					if(rechargeObj.refType=="TRANSFER"){
						var weixinStr='<li class="user-line"><div class="container"><div class="line-title"><p>微信昵称</p></div><div class="line-content"><input name="paynameinput" class="inputText" type="text" placeholder="请输入微信昵称"></div></div></li>'
							$("#payDiv").before(weixinStr);
					}
					
					
				}else if(rechargeObj.payType=="ALIPAY" || rechargeObj.payType=="ALIPAYWAP"){
					$("#selectLine").show();
					/*$("#selectLineBank").remove();*/
					$("#selectLine img").attr("src",contextPath+"\\images\\bankcard_logo\\zhifubao.png");
					$("#selectLine h2.title").text("支付宝");
					/*$("#payDiv").prev("li").remove();*/
					if(rechargeObj.refType=="TRANSFER"){
						var weixinStr='<li class="user-line"><div class="container"><div class="line-title"><p>支付宝姓名</p></div><div class="line-content"><input name="paynameinput" class="inputText" type="text" placeholder="请输入支付宝姓名"></div></div></li>'
							$("#payDiv").before(weixinStr);
					}
					
				}else if(rechargeObj.payType=="QQPAY" || rechargeObj.payType=="QQPAYWAP"){
					$("#selectLine").show();
					/*$("#selectLineBank").remove();*/
					$("#selectLine img").attr("src",contextPath+"\\images\\bankcard_logo\\qqpay.png");
					$("#selectLine h2.title").text("QQ钱包");
					/*$("#payDiv").prev("li").remove();*/
					if(rechargeObj.refType=="TRANSFER"){
						var weixinStr='<li class="user-line"><div class="container"><div class="line-title"><p>QQ钱包姓名</p></div><div class="line-content"><input name="paynameinput" class="inputText" type="text" placeholder="请输入QQ钱包姓名"></div></div></li>'
							$("#payDiv").before(weixinStr);
					}
					
				}else if(rechargeObj.payType=="UNIONPAY"){
					$("#selectLine").show();
					/*$("#selectLineBank").remove();*/
					$("#selectLine img").attr("src",contextPath+"\\images\\bankcard_logo\\unionpay.png");
					$("#selectLine h2.title").text("银联扫码");
					/*$("#payDiv").prev("li").remove();*/
					if(rechargeObj.refType=="TRANSFER"){
						var weixinStr='<li class="user-line"><div class="container"><div class="line-title"><p>姓名</p></div><div class="line-content"><input name="paynameinput" class="inputText" type="text" placeholder="请输入姓名"></div></div></li>'
							$("#payDiv").before(weixinStr);
					}
					
				}else if(rechargeObj.payType=="JDPAY"){
					$("#selectLine").show();
					/*$("#selectLineBank").remove();*/
					$("#selectLine img").attr("src",contextPath+"\\images\\bankcard_logo\\jdpay.png");
					$("#selectLine h2.title").text("京东扫码");
					/*$("#payDiv").prev("li").remove();*/
					if(rechargeObj.refType=="TRANSFER"){
						var weixinStr='<li class="user-line"><div class="container"><div class="line-title"><p>姓名</p></div><div class="line-content"><input name="paynameinput" class="inputText" type="text" placeholder="请输入姓名"></div></div></li>'
							$("#payDiv").before(weixinStr);
					}
					
				}
				
			}
}
		
/**
 * 查找用户可支付的银行
 */
RechargePage.prototype.getCurrentUserCanRechargerPanks = function(payType,id,refType,refIdList){
	/*var str=type.split(",")*/
	var query={};
	query.payType=payType;
	query.id=id;
    query.refType=refType;
    query.refIdList=refIdList;
    $.ajax({
		type: "POST",
        url: contextPath +"/recharge/getCurrentUserCanRechargerPanks",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(query), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		var data = result.data;
        		rechargePage.showRechargeTypes(data);
        	}else if(result.code == "error"){
        		var err = result.data;
    			showTip(r[1]);
        	}else{
        		showTip("加载系统可支付银行的请求失败.");
        	}
        }
	});
};

/**
 * 展示可充值的银行卡
 */
RechargePage.prototype.showRechargeTypes = function(banks){
	/*if(rechargePage.param.costWay != "zhifubao"){*/
	var banks=rechargePage.param.payBankList;
	if(banks != null && banks.length > 0){
			var rechargeBanksObj = $("#banklistinfo");
			rechargeBanksObj.html("");
			var str = "";
			for(var i = 0; i < banks.length; i++){
				var rechargeBank = banks[i];
				
					str += "<div class='container' onclick=\"rechargePage.selectBank('"+ rechargePage.getBankImgLogo(rechargeBank.bankType) +"','"+
					rechargeBank.bankName+"', '"+rechargeBank.id+"', '"+rechargeBank.highestValue+"', '"+rechargeBank.lowestValue+"', '"+rechargeBank.pId+"', '"+rechargeBank.payId+"')\">";
					str += "	<div class='line-title'><img src='"+ rechargePage.getBankImgLogo(rechargeBank.bankType) +"' alt=''></div>";
					str += "	<div class='line-content'>";
					str += "   		<input type='hidden' data-id='"+rechargeBank.id+"' bankName='"+rechargeBank.bankName+"' data-value='"+rechargeBank.bankType+"' max='"+rechargeBank.highestValue+"' min='"+rechargeBank.lowestValue+"'/>";
					str += "		<h2 class='title' value='"+rechargeBank.bankName+"'>"+rechargeBank.bankName+"</h2>";
					str += "	</div>";
					str += "</div>";
				if(i == 0) {
					var contentId="#content1";
					
					$(contentId+" .bankname .line-banklogo img").attr("src",rechargePage.getBankImgLogo(rechargeBank.bankType));
					$(contentId+" .bankname .title").html(rechargeBank.bankName);
					$(contentId+" .bankname .value").val(rechargeBank.bankName);
					$(contentId+" .bankname .line-content [name='max']").val(rechargeBank.highestValue);
					$(contentId+" .bankname .line-content [name='min']").val(rechargeBank.lowestValue);
					$(contentId+" .bankname .line-content [name='bankId']").val(rechargeBank.id);
					if(rechargeBank.id==null){
						$(contentId+" .bankname .line-content [name='chargePayId']").val(rechargeBank.pId);
						$(contentId+" .bankname .line-content [name='payId']").val(rechargeBank.payId);
					}
					/*$(contentId+" [name='bankLowestValue']").text(rechargeBank.lowestValue);
					$(contentId+" [name='bankHighestValue']").text(rechargeBank.highestValue);*/
					
					//网银充值最大值和最小值
					rechargePage.param.bankId = rechargeBank.id;
					rechargePage.param.payBankLowestValue = parseFloat(rechargeBank.lowestValue);
					rechargePage.param.payBankHighestValue = parseFloat(rechargeBank.highestValue);
				/*	var contentId="#"+rechargePage.param.content;
					
					if($(contentId+" .bankname .value").val()=="")
						{
						rechargePage.selectBank(rechargePage.getBankImgLogo(rechargeBank.bankType),
								rechargeBank.bankName,rechargeBank.id,rechargeBank.highestValue,rechargeBank.lowestValue,rechargeBank.pId,rechargeBank.payId);
						}*/
					
					/*if($(".pane[nav-view='active'] .select-container .line-banklogo").length > 0) {
						//$(".select-container .value").val(value);
						$(".select-container .title").html(rechargeBank.bankname);
						$(".select-container #wangyin_max").val(rechargeBank.highestValue);
						$(".select-container #wangyin_min").val(rechargeBank.lowestValue);
						$("#bankHighestValue").text(rechargeBank.highestValue);
						$("#bankLowestValue").text(rechargeBank.lowestValue);
						$(".select-container #bankId").val(rechargeBank.id);
						$(".select-container .line-banklogo img").attr("src",rechargePage.getBankImgLogo(rechargeBank.bankType));
					}*/
				}
				
				rechargeBanksObj.append(str);
				str = "";
			}
		}
	//}
	
	//绑定下拉选择事件
	select();
	$(".select-list .container").unbind();
	$(".select-list .container").click(function(){
		var value=$(this).find(".title").attr("value");
		var html=$(this).find(".title").html();

		var max_value=$(this).find("input").attr("max");
		var min_value=$(this).find("input").attr("min");
		var payId = $(this).find("input").attr("data-id");

		var logo=$(this).find(".line-title img").attr("src");
		var parent=$(this).closest(".select");
		parent.find(".select-container .value").val(value);
		parent.find(".select-container .title").html(html);
		parent.find(".select-container #wangyin_max").val(max_value);
		parent.find(".select-container #wangyin_min").val(min_value);
		parent.find(".select-container #payId").val(payId);
		parent.find(".select-container .line-banklogo img").attr("src",logo);
		
		rechargePage.showBankHighestAndLowest(this);
	});
	
	//$(".select-list .container:eq(0)").trigger("click");
	
};

/**
 * 获取银行logo图片地址
 */
RechargePage.prototype.getBankImgLogo = function(bankType) {
	var imgLogo = "";
	if(bankType == "ICBC") {
		imgLogo = contextPath+"/images/bankcard_logo/gongshang.png";
	} else if(bankType == "CCB") {
		imgLogo = contextPath+"/images/bankcard_logo/jianshe.png";
	} else if(bankType == "ABC") {
		imgLogo = contextPath+"/images/bankcard_logo/nongye.png";
	} else if(bankType == "PSBC") {
		imgLogo = contextPath+"/images/bankcard_logo/youzheng.png";
	} else if(bankType == "CMBC") {
		imgLogo = contextPath+"/images/bankcard_logo/minsheng.png";
	} else if(bankType == "CIB") {
		imgLogo = contextPath+"/images/bankcard_logo/xingye.png";
	} else if(bankType == "COMM") {
		imgLogo = contextPath+"/images/bankcard_logo/jiaotong.png";
	} else if(bankType == "CEB") {
		imgLogo = contextPath+"/images/bankcard_logo/gaungda.png";
	} else if(bankType == "BOC") {
		imgLogo = contextPath+"/images/bankcard_logo/zhongguo.png";
	} else if(bankType == "SPABANK") {
		imgLogo = contextPath+"/images/bankcard_logo/pingan.png";
	} else if(bankType == "GDB") {
		imgLogo = contextPath+"/images/bankcard_logo/guangfa.png";
	} else if(bankType == "CITIC") {
		imgLogo = contextPath+"/images/bankcard_logo/zhongxin.png";
	} else if(bankType == "HXBANK") {
		imgLogo = contextPath+"/images/bankcard_logo/huaxia.png";
	} else if(bankType == "CMB") {
		imgLogo = contextPath+"/images/bankcard_logo/zhaoshang.png";
	} else if(bankType == "QQPAY") {
		imgLogo = contextPath+"/images/bankcard_logo/qqpay.png";
	}else if(bankType == "UNIONPAY") {
		imgLogo = contextPath+"/images/bankcard_logo/unionpay.png";
	}else if(bankType == "JDPAY") {
		imgLogo = contextPath+"/images/bankcard_logo/jdpay.png";
	}
	return imgLogo;
};

/**
 * 显示网银充值最高金额跟最低金额
 */
RechargePage.prototype.showBankHighestAndLowest = function(obj){
	var inputObj = $(obj).find("input");
	var min = inputObj.attr("min");
	var max = inputObj.attr("max");
	
	/*$("#bankLowestValue").text(min);
	$("#bankHighestValue").text(max);*/
	
	//网银充值最大值和最小值
	/*rechargePage.param.payId = inputObj.attr("data-id");*/
	rechargePage.param.payBankLowestValue = parseFloat(inputObj.attr("min"));
	rechargePage.param.payBankHighestValue = parseFloat(inputObj.attr("max"));
};

//选择银行卡
RechargePage.prototype.selectBank=function(logo,bankname, bankId, max_value, min_value,chargePayId,payId){
	
	var contentId="#content1";
	
	$(contentId+" .bankname .line-banklogo img").attr("src",logo);
	$(contentId+" .bankname .title").html(bankname);
	$(contentId+" .bankname .value").val(bankname);
	$(contentId+" .bankname .line-content [name='max']").val(max_value);
	$(contentId+" .bankname .line-content [name='min']").val(min_value);
	$(contentId+" .bankname .line-content [name='bankId']").val(bankId);
	if(bankId=="null"){
		
		$(contentId+" .bankname .line-content [name='chargePayId']").val(chargePayId);
		$(contentId+" .bankname .line-content [name='payId']").val(payId);
	}
	/*$(contentId+" [name='bankLowestValue']").text(min_value);
	$(contentId+" [name='bankHighestValue']").text(max_value);*/
	
	//网银充值最大值和最小值
	rechargePage.param.bankId = bankId;
	rechargePage.param.payBankLowestValue = parseFloat(min_value);
	rechargePage.param.payBankHighestValue = parseFloat(max_value);
	
	modal_toggle('.bank-modal');
};

/**
 * 充值校验
 */
RechargePage.prototype.rechargeStepValidate = function(obj){
	if(currentUser.isTourist==1){
		showTip("游客无操作权限，请先注册为本系统用户");
		return;
	}
	//元素上一级图层
	var content=$("#content1");
	//关联方式类型
	var refType=rechargePage.param.refType;
	/*//支付类型
	var chargeType=$(obj).attr("data-charge-type");*/
	//充值类型
	var payType=rechargePage.param.payType;
	//配置ID
	var rechargeConfigId=rechargePage.param.rechargeConfigId;
	//配置ID
	var bankType=rechargePage.param.bankType;
	$(".pane[nav-view='active'] #fuyuanId").html("附言(编号)");
	$(".gotoRechargeButton").unbind();
	$(".gotoRechargeButton").bind("click").click(function(){
		if(payType == "BANK_TRANSFER"){
			showTip("亲 ，请前网银充值！");
		}else if(payType == "ALIPAY"){
			showTip("亲 ，请前往支付宝充值！");
		}else if(payType == "WEIXIN"){
			showTip("亲 ，请前往微信充值！");
		}else if(payType == "QQPAY"){
			showTip("亲 ，请前往QQ钱包充值！");
		}else if(payType == "UNIONPAY"){
			showTip("亲 ，请前往银联扫码充值！");
		}else if(payType == "JDPAY"){
			showTip("亲 ，请前往京东扫码充值！");
		}else{
			showTip("亲 ，请前往相应的充值方式充值！");
		}
	});
	/*rechargeMoney:null,
	nickName:null*/
	if(content.find("input[name='rechargeininput']").val() == ""){
		showTip("对不起,您还没填写您的充值金额.");
		return;
	}
	rechargePage.param.rechargeMoney = parseFloat(content.find("input[name='rechargeininput']").val());
	if (typeof(content.find("input[name='paynameinput']").val()) != "undefined"){
		rechargePage.param.nickName=content.find("input[name='paynameinput']").val();
	}
	if(payType == "BANK_TRANSFER"){
		$(".alipayTip").hide();
		$(".bankpayTip").show();
		$(".alipayAccount").hide();
		$(".weixinAccount").hide();
		$(".qqpayAccount").hide();
		
		//防止重复提交
		if($("#wangyin").attr("data-submit") == "true") {
			return;
		}
		$("#wangyin").attr("data-submit", "true");
		$("#wangyin").text("提交中");
	}else if(payType == "ALIPAY" || payType == "ALIPAYWAP"){
		if(refType=="TRANSFER"){  
			if(bankType=="ALIPAY"){
				$(".alipayTip").hide();
				$(".bankpayTip").hide();
				$(".alipayAccount").show();
				$(".weixinAccount").hide();
				$(".qqpayAccount").hide();
		    }else{
				$(".alipayTip").show();
				$(".bankpayTip").hide();
				$(".alipayAccount").hide();
				$(".weixinAccount").hide();
				$(".qqpayAccount").hide();
			}
	   }
	}else if(payType == "WEIXIN" || payType == "WEIXINWAP" || payType == "WEIXINBARCODE"){
		if(refType=="TRANSFER"){
			$(".alipayTip").hide();
			$(".bankpayTip").hide();
			$(".alipayAccount").hide();
			$(".qqpayAccount").hide();
			$(".weixinAccount").show();
			
		}
	}else if(payType == "QQPAY" || payType == "QQPAYWAP"){
		if(refType=="TRANSFER"){
			$(".alipayTip").hide();
			$(".bankpayTip").hide();
			$(".alipayAccount").hide();
			$(".qqpayAccount").show();
			$(".weixinAccount").hide();
		}
	}else if(payType == "UNIONPAY"){
		if(refType=="TRANSFER"){
			$(".alipayTip").hide();
			$(".bankpayTip").hide();
			$(".alipayAccount").hide();
			$(".qqpayAccount").show();
			$(".weixinAccount").hide();
		}
	}else if(payType == "JDPAY"){
		if(refType=="TRANSFER"){
			$(".alipayTip").hide();
			$(".bankpayTip").hide();
			$(".alipayAccount").hide();
			$(".jdpayAccount").show();
			$(".weixinAccount").hide();
		}
    }else{
	  showTip("未找到此种支付方式.");
	  return;
    }
	
	var maxPay = parseFloat($("#highestValue").text());
	var minPay = parseFloat($("#lowestValue").text());
	if(rechargePage.param.rechargeMoney < minPay || rechargePage.param.rechargeMoney > maxPay){
		showTip("充值金额不正确,充值金额最小为:" + minPay + ",充值最大金额为:" + maxPay);
		return;
	}
	rechargePage.doRecharge();
}
/**
 * 根据相关的第三方类型跳转相应的页面
 */
RechargePage.prototype.skipPage = function(chargeType) {
	var strUrl = "";
	if (chargeType == "BAOFU") {
		strUrl = contextPath + "/gameBet/pay/baofu/post.html";
	} else if (chargeType == "SHANFU") {
		strUrl = contextPath + "/gameBet/pay/shanfu/post.html";
	} else if (chargeType == "KDPAY") {
		strUrl = contextPath + "/gameBet/pay/kdpay/post.html";
	} else if (chargeType == "X6PAY") {
		strUrl = contextPath + "/gameBet/pay/xhpay/post.html";
	} else if (chargeType == "YBSW") {
		strUrl = contextPath + "/gameBet/pay/yinbao/post.html";
	} else if (chargeType == "OKPAY") {
		strUrl = contextPath + "/gameBet/pay/okpay/post.html";
	} else if (chargeType == "DBPAY") {
		strUrl = contextPath + "/gameBet/pay/duobao/post.html";
	} else if (chargeType == "RXPAY") {
		strUrl = contextPath + "/gameBet/pay/renxin/post.html";
	} else if (chargeType == "YTBPAY") {
		strUrl = contextPath + "/gameBet/pay/yingtongbao/post.html";
	} else if (chargeType == "ZBPAY") {
		strUrl = contextPath + "/gameBet/pay/zhongbao/post.html";
	} else if (chargeType == "GTPAY") {
		strUrl = contextPath + "/gameBet/pay/gaotong/post.html";
	} else if (chargeType == "YBPAY") {
		strUrl = contextPath + "/gameBet/pay/yinbang/post.html";
	} else if (chargeType == "HTPAY") {
		strUrl = contextPath + "/gameBet/pay/huitong/post.html";
	} else if (chargeType == "YIDAO") {
		strUrl = contextPath + "/gameBet/pay/yidao/post.html";
	} else if (chargeType == "JINYANG") {
		strUrl = contextPath + "/gameBet/pay/jinyang/post.html";
	} else if (chargeType == "XINMA") {
		strUrl = contextPath + "/gameBet/pay/xinma/post.html";
	} else if (chargeType == "SHUNFU") {
		strUrl = contextPath + "/gameBet/pay/shunfu/post.html";
	} else if (chargeType == "JINHAO") {
		strUrl = contextPath + "/gameBet/pay/jinhao/post.html";
	} else if (chargeType == "RUJINBAO") {
		strUrl = contextPath + "/gameBet/pay/rujinbao/post.html";
	} else if (chargeType == "XINYU") {
		strUrl = contextPath + "/gameBet/pay/xinyu/post.html";
	} else if (chargeType == "JURONG") {
		strUrl = contextPath + "/gameBet/pay/jurong/post.html";
	} else if (chargeType == "JIANZHENGBAO") {
		strUrl = contextPath + "/gameBet/pay/jianzhengbao/post.html";
	} else if (chargeType == "XINGFU") {
		strUrl = contextPath + "/gameBet/pay/xingfu/post.html";
	} else if (chargeType == "RONGCAN") {
		strUrl = contextPath + "/gameBet/pay/rongcan/post.html";
	} else if (chargeType == "BAIQIANFU") {
		strUrl = contextPath + "/gameBet/pay/baiqianfu/post.html";
	} else if (chargeType == "PUJIN") {
		strUrl = contextPath + "/gameBet/pay/pujin/post.html";
	} else if (chargeType == "HUIBAO") {
		strUrl = contextPath + "/gameBet/pay/huibao/post.html";
	} else if (chargeType == "GZPAY") {
		strUrl = contextPath + "/gameBet/pay/gzpay/post.html";
	} else if (chargeType == "BDPAY") {
		strUrl = contextPath + "/gameBet/pay/badapay/post.html";
	} else if (chargeType == "MSPAY") {
		strUrl = contextPath + "/gameBet/pay/mashanpay/post.html";
	} else if (chargeType == "QUEPAY") {
		strUrl = contextPath + "/gameBet/pay/quepay/post.html";
	} else if (chargeType == "DUDUPAY") {
		strUrl = contextPath + "/gameBet/pay/dudupay/post.html";
	}else if (chargeType == "SIXEIGHTSIXUPAY") {
		strUrl = contextPath + "/gameBet/pay/sespay/post.html";
	}else if (chargeType == "BEIFUBAOPAY") {
		strUrl = contextPath + "/gameBet/pay/beifubao/post.html";
	}else if (chargeType == "LIANYINGPAY") {
		strUrl = contextPath + "/gameBet/pay/lianyingpay/post.html";
	}else if (chargeType == "FEILONGPAY") {
		strUrl = contextPath + "/gameBet/pay/feilong/post.html";
	}else if (chargeType == "TIANTIANKUAIPAY") {
		strUrl = contextPath + "/gameBet/pay/tiantiankuaipay/post.html";
	}else if (chargeType == "KUAIKUAIRUPAY") {
		strUrl = contextPath + "/gameBet/pay/kuaikuairu/post.html";
	}else if (chargeType == "CRYPTPAY") {
		strUrl = contextPath + "/front/pay/cryptpay/post.jsp";
	}else if (chargeType == "FANKEPAY") {
		strUrl = contextPath + "/front/pay/fankepay/post.jsp";
	}else if(chargeType=="YINHEPAY"){
		strUrl = contextPath + "/front/pay/yinhe/post.jsp";
	}else if(chargeType=="HEXIEPAY"){
		strUrl = contextPath + "/front/pay/hexiepay/post.jsp";
	}else if(chargeType=="XIUFUPAY"){
		strUrl = contextPath + "/front/pay/xiufupay/post.jsp";
	}else if(chargeType=="WEIFUTONG"){
		strUrl = contextPath + "/front/pay/weifutong/post.jsp";
	}else if(chargeType=="YIFU"){
		strUrl = contextPath + "/front/pay/yifu/post.jsp";
	}else if(chargeType=="WANNIAN"){
		strUrl = contextPath + "/front/pay/wannian/post.jsp";
	}else if(chargeType=="SHANDEPAY"){
		strUrl = contextPath + "/front/pay/shandepay/post.jsp";
	}else if(chargeType=="JINDOUYUNPAY"){
		strUrl = contextPath + "/front/pay/jindouyunpay/post.jsp";
	}else if(chargeType=="MUMINGPAY"){
		strUrl = contextPath + "/front/pay/mumingpay/post.jsp";
	}else if(chargeType=="HUJINGPAY"){
		strUrl = contextPath + "/front/pay/hujingpay/post.jsp";
	}
	return strUrl;
}

/**
 * 充值金额加上随机数
 * 
 */
RechargePage.prototype.rechargeMoneyRandom = function(obj){
		var content=$("#content1").find("input[name='rechargeininput']");
		var refType=content.attr("data-ref-type");
		/*var payType=content.attr("data-pay-type");*/
		//元素上一级图层
		/*var content=$(obj);*/
		if(content.val() == ""){
			alert("对不起,您还没填写您的充值金额.");
			return;
		}
		var mumberRandom=(Math.ceil(Math.random()*5))/100;
		var money=parseFloat(content.val())+mumberRandom;
			rechargePage.param.content=content;
			rechargePage.param.money=money;
			if(refType=="THIRDPAY"){
				confirm("为了更准确核对你的金额系统已经将充值金额调整为"+money, function(){
					var content=rechargePage.param.content;
					var money=rechargePage.param.money;
					content.val(money);
					rechargePage.param.inputChange=true;
				});
			}
	
};


/**
 * 充值支付下一步
 */
RechargePage.prototype.doRecharge = function(){
	var param={};
	param.rechargeMoney=rechargePage.param.rechargeMoney;
	param.rechargeConfigId=rechargePage.param.rechargeConfigId;
	param.nickName=rechargePage.param.nickName;
	$.ajax({
		type: "POST",
        url: contextPath +"/recharge/doRecharge",
        	   contentType :"application/json;charset=UTF-8",
               data:JSON.stringify(param), 
               dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	$("#content1").find("div.submitBoolean").attr("data-submit", "false");
        	if(result.code == "ok"){
        		var data = result.data;
        		var data2 = result.data2;
        		var data3 = result.data3;
        		var data4 = result.data4;
        		var data5 = result.data5;
        		var data6 = result.data6;
        		var data7 = result.data7;
        		if (data == "UnFinshedOrder") {
        			$(".cancelRechargeButton").show();
        			$(".gotoRechargeButton").hide();
        			$("#rechargeTip").text("当前系统已存在充值订单");
        			modal_toggle('.recharge-modal');
        			rechargePage.showRechargeOrderMsg(data2);
        		}else if(data == "NewOrder"){
        			$(".cancelRechargeButton").hide();
        			$(".gotoRechargeButton").show();
        			$("#rechargeTip").text("订单已提交");
        			modal_toggle('.recharge-modal');
        			rechargePage.showRechargeOrderMsg(data2);
        		}else{
        			//普金支付充值金额特殊校验
        			if(data5=="PUJIN"){
        				//小于等于100必须为整数
        				if(data <= 100) {
        					var r = /^\+?[1-9][0-9]*$/;
        					if(!r.test(data)) {
        						showTip("充值金额不正确,必须为整数值");
        						return;
        					}
        				} else {
        					var r = /^\+?[1-9][0-9]*[0]$/;
        					if(!r.test(data)) {
        						showTip("充值金额不正确,充值金额大于100的最后一位必须是0，如110、120");
        						return;
        					}
        				}
        			}
        			
        			//密付直接跳转
        			if(data5=="CRYPTPAY"){
        				var postUrl = contextPath + "/front/pay/cryptpay/post.jsp?OrderMoney=" + data + "&chargePayId=" +
        				data3 + "&PayId=" + data2 + "&payType=" + data4+"&serialNumber=" + data6;
        				window.location.href =  postUrl;
        				return;
        			}
        			/*var strUrl=rechargePage.skipPage(data5);*/
        			var strUrl=data7;
        			var postForm = $(window.frames["submitFrame"].document).find("#form2");
        			postForm.attr("action", strUrl);
        			postForm.find("input[name='OrderMoney']").val(data);
        			postForm.find("input[name='chargePayId']").val(data3);
        			postForm.find("input[name='PayId']").val(data2);
        			postForm.find("input[name='payType']").val(data4);
        			//postForm.find("input[name='rechargeConfigId']").val(data6);
        			postForm.find("input[name='serialNumber']").val(data6);
        			postForm.submit();
        			modal_toggle('.weixin-recharge-modal');
        			showTip("订单正在提交中，请稍等……");
        			$(".showTip-msg2").stop(false,true).delay(200).fadeIn(500);
        			$(".showTip-msg-bg").stop(false,true).fadeIn(500);
        		}
        	}else if(result.code == "error"){
        		var err = result.data;
    			showTip(err);
        	}else{
        		showTip("充值下一步动作请求失败.");
        		$("#wangyin").text("下一步");
        	}
        }
	});
};



/**
 * 获取支付记录信息
 */
RechargePage.prototype.getPayBankMsg = function(){
	$.ajax({
		type: "POST",
        url: contextPath +"/recharge/getPayBankMsg",
        contentType :"application/json;charset=UTF-8",
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		var data = result.data;
        		rechargePage.showRechargeOrderMsg(data);
        	}else if(result.code == "error"){
        		var err = result.data;
        		if( err== "CHARGEORDERSEND"){
                    alert("您当前该充值记录状态已经改变,请重新进行充值.");
                    return;
        		}else{
        			showTip(err);
        		}
        	}else{
        		showTip("加载系统充值记录的请求失败.");
        	}
        }
	});
};

/**
 * 展示可充值的银行卡
 */
RechargePage.prototype.showRechargeOrderMsg = function(order){
	$("div.recharge-modal").find("input").val("");
	if(order != null){
		rechargePage.param.canclePayId = order.id;
		rechargePage.param.bankUrl = order.bankUrl;
		
        $("#alertBankLogo").addClass(order.banklogo);
        
        $(".gotoRechargeButton").unbind();
    	$(".gotoRechargeButton").bind("click").click(function(){
    		   
            var payWayName = order.bankType;
            var payType = order.payType;
            var bankNane = order.bankName;
    		
    		if(payType == "BANK_TRANSFER" && payWayName == "ALIPAY"){
    			showTip("亲 ，请前往支付宝转账！");
    			/*$("#fuyuanId").html("支付宝姓名：");*/
    		}else if(payWayName == "WEIXIN"){
    			showTip("亲 ，请前往微信充值！");
    		}else if(payType == "BANK_TRANSFER" && payWayName != "ALIPAY"){
    			showTip("亲，请前往"+bankNane+"转账！");
    		}else if(payWayName == "QQPAY"){
    			showTip("亲 ，请前往QQ钱包充值！");
    		}else if(payType == "BANK_TRANSFER" && payWayName != "QQPAY"){
    			showTip("亲，请前往QQ钱包转账！");
    		}else if(payWayName == "UNIONPAY"){
    			showTip("亲 ，请前往银联扫码充值！");
    		}else if(payWayName == "JDPAY"){
    			showTip("亲 ，请前往京东扫码充值！");
    		}else{
    			showTip("亲 ，请前往相应的充值方式充值！");
    		}
    	});
        //支付宝收款银行 用支行字段显示
        if(order.payType == "ALIPAY") {
        	if(order.payType.bankType=="ALIPAY"){
        		$("#alertBankAmount").html(order.applyValue + "元");
              	 $("#alipayAccountBankName").val(order.bankAccountName);
              	 $("#alipayAccountBankId").val(order.bankCardNum);
              	 $("#alipayAccountBankscript").val(order.postscript);
              	 //隐藏开户行地址
              	 $("#bankBranchNameLi").hide();
              	 $(".gotoRechargeButton").html("去支付宝转账");
              	 $(".alipayTip").show();
              	 $("#alipayAccountfuyuanId").html("支付宝姓名：");
              	 $(".alipayTip").hide();
           		$(".bankpayTip").hide();
           		$(".alipayAccount").show();
           		$(".weixinAccount").hide();
           		$(".qqpayAccount").hide();
        		}else{
        		$("#alertBankAmount").html(order.applyValue + "元");
              	 $("#alipayBankName").val(order.bankName);
              	 //隐藏开户行地址
              	 $("#bankBranchNameLi").hide();
              	 $(".gotoRechargeButton").html("去支付宝转账");
              	 $(".alipayTip").show();
              	 $("#alipayfuyuanId").html("支付宝姓名：");
              	 $(".alipayTip").show();
				 if (isNotEmpty(order.barcodeUrl)) {
					$("#imgAlipay").show();
					$("#barcodeImgAlipay").attr("src", order.barcodeUrl);
				} else {
					$("#imgAlipay").hide();
				}
              	
           		$(".bankpayTip").hide();
           		$(".alipayAccount").hide();
           		$(".weixinAccount").hide();
           		$(".qqpayAccount").hide();
                $("#alipayBankUserName").val(order.bankAccountName);
                $("#alipayBankId").val(order.bankCardNum);
                $("#alipayBankBranchName").val(order.subbranchName);
                $("#alipayBankscript").val(order.postscript);
        		}
        	 
        }else if(order.payType=="WEIXIN"){
        	$(".alipayTip").hide();
     		$(".bankpayTip").hide();
     		$(".alipayAccount").hide();
     		$(".qqpayAccount").hide();
     		$(".weixinAccount").show();
     		if(isNotEmpty(order.barcodeUrl)){
     			$("#imgWeixin").show();
     			$("#barcodeImgWeixin").attr("src", order.barcodeUrl);
     		}else{
     			$("#imgWeixin").hide();
     		}
     		
     		$("#weixinalertBankscript").val(order.postscript);
            $("#weixinalertBankUserName").val(order.bankAccountName);
            $("#weixinalertBankId").val(order.bankCardNum);
            $("#weixinfuyuanId").html("微信昵称：");
     		/*$("#alertBankName").val(order.bankName);*/
     		
     		$(".gotoRechargeButton").html("去微信转账");
        }else if(order.payType=="BANK_TRANSFER"){
        	$("#alertBankName").val(order.bankName);
        	$("#alertBankscript").val(order.postscript);
            $("#alertBankUserName").text(order.bankAccountName);
            $("#alertBankId").val(order.bankCardNum);
            $("#alertBankBranchName").val(order.subbranchName);
        	$("#bankBranchNameLi").show();
        	$(".gotoRechargeButton").html("去银行转账");
        	$(".alipayTip").hide();
        	$("#fuyuanId").html("附言(编号)");
        	$(".alipayTip").hide();
    		$(".bankpayTip").show();
    		$(".alipayAccount").hide();
     		$(".weixinAccount").hide();
     		$(".qqpayAccount").hide();
        }else if(order.payType=="QQPAY"){
        	$(".alipayTip").hide();
     		$(".bankpayTip").hide();
     		$(".alipayAccount").hide();
     		$(".qqpayAccount").hide();
     		$(".weixinAccount").show();
     		
     		$("#qqpayalertBankscript").val(order.postscript);
            $("#qqpayalertBankUserName").val(order.bankAccountName);
            $("#qqpayalertBankId").val(order.bankCardNum);
            $("#qqpayfuyuanId").html("QQ钱包呢称：");
     		/*$("#alertBankName").val(order.bankName);*/
     		
     		$(".gotoRechargeButton").html("去QQ钱包转账");
        }
       
        
        $("#alertBankAmount").html(order.applyValue + "元");
        $("#alertBankAmount").val(order.applyValue + "元");
        
        
        //如果有二维码图片
        if(isNotEmpty(order.barcodeUrl)) {
        	$("#barcodeImg").attr("src", order.barcodeUrl);
        	$("#rechargeOrderDialog1").css("width", "730px");
        	$("#barcodeDiv").css("display", "inline-block");
        	$("#rechargeInfoDiv").attr("style", "display: inline-block;width: 300px;float: left;");
        	
        } else {
        	$("#rechargeInfoDiv").attr("style", "display: block; width: 400px;");
        	$("#rechargeOrderDialog1").css("width", "500px");
        	$("#barcodeDiv").css("display", "none");
        }
        
        if(order.bankType == "WEIXIN") {
        	$("#scriptTipInfo").hide();
        } else {
        	$("#scriptTipInfo").show();
        }
        
	}
};

/**
 * 充值撤单
 */
RechargePage.prototype.cancelRechargeOrder = function(){
	var param={};
	param.rechargeOrderId=rechargePage.param.canclePayId;
	$.ajax({
		type: "POST",
        url: contextPath +"/recharge/cancelRechargeOrder",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(param), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if(result.code == "ok"){
        		showTip("充值订单取消成功.");
    			modal_toggle('.recharge-modal');
    			$(".gotoRechargeButton").hide();
    			$(".cancelRechargeButton").hide();
        	}else if(result.code == "error"){
        		var err = result.data;
        		showTip(err);
        	}else{
        		showTip("撤销订单的请求失败.");
        	}
        }
	});
};

/**
 * 获取支付方式开关
 */
RechargePage.prototype.getModeofPaymentSwitch = function(){
	
	if(shanfuWeixinRecharge != 1){
		$(".rechargewithdraw_fund .header-title a:eq(0)").hide();
	}
	if(alipayRecharge != 1){
		$(".rechargewithdraw_fund .header-title a:eq(1)").hide();
	}
	if(onlineBanking != 1){
		$(".rechargewithdraw_fund .header-title a:eq(2)").hide();
	}
};


$(document).ready(function() {
	
	//撤单
	$(".cancelRechargeButton").unbind("click").click(function() {
        if(rechargePage.param.canclePayId != null){
    		rechargePage.cancelRechargeOrder();
        }else{
        	$(".cancelRechargeButton").removeAttr("disabled");
        }
	});
	//获取支付方式开关
	//setTimeout("rechargePage.getModeofPaymentSwitch()", 600);
	//设置闪付充值的最低金额与最高金额
	/*rechargePage.limitParam.shanfuQuickRechargeLowestMoney = shanfuQuickRechargeLowestMoney;
	rechargePage.limitParam.shanfuQuickRechargeHighestMoney =  shanfuQuickRechargeHighestMoney;
	$("#shanfuQuickRechargeLowestMoney").text(rechargePage.limitParam.shanfuQuickRechargeLowestMoney);
	$("#shanfuQuickRechargeHighestMoney").text(rechargePage.limitParam.shanfuQuickRechargeHighestMoney);*/
	
  	rechargePage.getAllRechargeConfig();
  
  	// 复制插件clipboard.js
  	var clipboard = new Clipboard('.copy');
  	clipboard.on("success",function(e){
        showTip("已复制");
    });
});

/**
 * 头部样式调整
 */
function headerPostion(){
	var headerWidth=0;
	$(".rechargewithdraw_fund .header-title-child").each(function(i,n){
		headerWidth+=parseInt($(this).css("width"));
	});
	$(".rechargewithdraw_fund .header-title-content").css("width",headerWidth+200);
}

function headerChange(e){
	var index=$(e).index(".rechargewithdraw_fund .header-title-child");
	/*rechargePage.param.content=$(e).attr("data-div");*/
	/*var payType=$(e).attr("data-payType");
	var id=$(e).attr("data-id");
	var refType=$(e).attr("data-refType");
	var refIdList=$(e).attr("data-refIdList");
	//document.getElementById('header-title').scrollLeft=index*228;
	$(".rechargewithdraw_fund .header-title-child").removeClass('on')
	$(e).addClass('on');
	$(".rechargewithdraw_fund .content").removeClass('on')
	$(".rechargewithdraw_fund .content:eq("+index+")").addClass('on')
	var str=payType+","+id+","+refType+","+refIdList;*/
	/*rechargePage.getCurrentUserCanRechargerPanks(payType,id,refType,refIdList);*/
}

function inputSelect(e){
	var parent=$(e).closest('.container');
	var text=parent.find('.line-content .font-black');
	setTimeout(function(text){
//		$(text).select();
		$(text)[0].focus();
		$(text)[0].setSelectionRange(0, 9999);
	},100,text);
}
function select(){
	$(".select").unbind();
	$(".select-list .container").unbind();
	$(".select").click(function(){
		$(this).find(".select-list").toggleClass('on');
	});
	$(".select-list .container").click(function(){
		var value=$(this).find(".title").attr("value");
		var html=$(this).find(".title").html();
		var parent=$(this).closest(".select");
		parent.find(".select-container .value").val(value);
		parent.find(".select-container .title").html(html);
	});
	
}
