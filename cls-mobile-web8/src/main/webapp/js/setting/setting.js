function SettingPage() {
};
var settingPage = new SettingPage();

$(document).ready(function() {
	$(".i-person").parent().addClass("on");
	settingPage.turnToUpdateSavePassword();
	settingPage.turnToUpdateSafetyPassword();
	settingPage.turnToUpdatePhoneEmail();

	$(".logout").unbind("click").click(function() {
		settingPage.userLogout();
	});

	// 加载当前用户投注模式设置
	settingPage.loadUserModelSet();

	// 绑定元角模式切换事件
	$("#modelSetToggle").unbind("click");
	$("#modelSetToggle").click(function() {
		var ele = $(this);
		var jsonData = {
			"model" : $("#lotteryModelSet").val(),
		}
		
		$.ajax({
			type : "POST",
			url : contextPath + "/lottery/saveUserLotteryModelSet",
			data : JSON.stringify(jsonData),//将对象序列化成JSON字符串;
			dataType : "json",//设置请求参数类型
			contentType : 'application/json;charset=utf-8',//设置请求头信息;
			success : function(result) {
				commonHandlerResult(result, this.url);
				if (result.code == "ok") {
					var value, html, on_status, on_class;
					on_class = ele.attr('data-onclass');
					on_status = ele.hasClass('on') ? 0 : 1;
					value = ele.find(".value").eq(on_status).attr("value");
					html = ele.find(".value").eq(on_status).html();
					if (ele.hasClass('on')) {
						ele.removeClass('on').removeClass("color-" + on_class);
						ele.find(".ball").removeClass("font-" + on_class);
					} else {
						ele.addClass('on').addClass("color-" + on_class);
						ele.find(".ball").addClass("font-" + on_class);
					}
					ele.find(".ball").html(html);
					ele.find(".toggle-value").val(value);
				} else if (result.code == "error") {
					showTip("切换元角模式失败，请重试");
				}
			}
		});
	});

	$("#noticeToggle").unbind("click");
	$("#noticeToggle").click(function() {
		var ele = $(this);
		on_class = ele.attr('data-onclass');
		on_status = ele.hasClass('on') ? 0 : 1;
		value = ele.find(".value").eq(on_status).attr("value");
		html = ele.find(".value").eq(on_status).html();
		if (ele.hasClass('on')) {
			ele.removeClass('on').removeClass("color-" + on_class);
			ele.find(".ball").removeClass("font-" + on_class);
		} else {
			$(this).addClass('on').addClass("color-" + on_class);
			$(this).find(".ball").addClass("font-" + on_class);
		}
	});

	$("#versionUpdateId").unbind("click");
	$("#versionUpdateId").click(function() {
		showTip("亲，你现在已经最新版本了");
	});

});

/**
 * 判断是否修改安全密码
 */
SettingPage.prototype.turnToUpdateSavePassword = function() {

	$.ajax({
		type : "POST",
		url : contextPath + "/usersafe/checkIsSetSafePassword",
		contentType : 'application/json;charset=utf-8',//设置请求头信息;
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				$("#safetyPasswordTitle").html("修改安全密码");
				$("#safetyPassword").attr("href", contextPath + "/gameBet/safeCenter/resetSafePwd.html");				
			} else if (result.code == "error") {
				$("#safetyPasswordTitle").html("设置安全密码");
				$("#safetyPassword").attr("href", contextPath + "/gameBet/safeCenter/setSafePwd.html");
			}
		}
	});
};

/**
 * 判断是否修改安全问题
 */
SettingPage.prototype.turnToUpdateSafetyPassword = function() {
	$.ajax({
		type : "POST",
		url : contextPath + "/safeq/loadUserSafeQuestions",
		contentType : 'application/json;charset=utf-8',//设置请求头信息;
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var callbackStr = result.data;
				if (callbackStr.length == 0) {
					$("#safetyQuestionTitle").html("设置安全问题");
					$("#safetyQuestion").attr("href", contextPath + "/gameBet/safeCenter/setSafeQuestion.html");
				} else {
					$("#safetyQuestion").attr("href", contextPath + "/gameBet/safeCenter/verifySafeQuestion.html");
					$("#safetyQuestionTitle").html("修改安全问题");
				}
			} else if (result.code == "error") {
				showTip("操作失败，请重试");
			}
		}
	});

};

/**
 * 判断是否修改安全手机及安全邮箱（1是代表修改 0代表绑定）
 */
SettingPage.prototype.turnToUpdatePhoneEmail = function() {
	if (currentUser.phone != null && currentUser.phone != "") {
		$("#safetyPhoneTitle").html("修改安全手机");
		$("#bindmobile").attr("href", contextPath + "/gameBet/safeCenter/bindPhoneChange.html");
	} else {
		$("#safetyPhoneTitle").html("绑定安全手机");
		$("#bindmobile").attr("href", contextPath + "/gameBet/safeCenter/bindPhone.html");
	}
	if (currentUser.email != null && currentUser.email != "") {
		$("#safetyEmailTitle").html("修改安全邮箱");
		$("#bindemail").attr("href", contextPath + "/gameBet/safeCenter/bindEmailChange.html");
	} else {
		$("#safetyEmailTitle").html("绑定安全邮箱");
		$("#bindemail").attr("href", contextPath + "/gameBet/safeCenter/bindEmail.html");
	}
};

/**
 * 用户退出
 */
SettingPage.prototype.userLogout = function() {
	$.ajax({
		type : "POST",
		url : contextPath + "/user/userLogout",
		contentType : 'application/json;charset=UTF-8', 
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				window.location.href= contextPath + "/gameBet/login.html";
			} else if (result.code == "error") {
				showTip(result.data);
			}
		}
	});
};

/**
 * 查询用户当前的投注模式
 */
SettingPage.prototype.loadUserModelSet = function() {

	$.ajax({
		type : "POST",
		url : contextPath + "/lottery/loadUserModelSet",
		contentType : 'application/json;charset=utf-8',//设置请求头信息;
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var model = result.data.mobileLotteryModel;
				if (model == "JIAO") {
					var ele = $("#modelSetToggle");
					var value, html, on_status, on_class;
					on_class = ele.attr('data-onclass');
					on_status = ele.hasClass('on') ? 0 : 1;
					value = ele.find(".value").eq(on_status).attr("value");
					html = ele.find(".value").eq(on_status).html();
					ele.addClass('on').addClass("color-" + on_class);
					ele.find(".ball").addClass("font-" + on_class);
					ele.find(".ball").html(html);
					ele.find(".toggle-value").val(value);
				}
			} else if (result.code == "error") {
				showTip("加载用户投注模式设置失败，请重试");
			}
		}
	});
}
