function ResetSafeQuestionPage() {
	
}

var resetSafeQuestionPage= new ResetSafeQuestionPage();

resetSafeQuestionPage.param = {
	passwordIsNull : false
};

$(document).ready(function() {
	$(".i-person").parent().addClass("on");
	// 当前链接位置标注选中样式
	$("#user_info_save").addClass("selected");

	resetSafeQuestionPage.initPage();

	$("#dna_ques_one").blur(function() {
		var dna_ques_one = $("#dna_ques_one").val();
		if (dna_ques_one == null || dna_ques_one == "") {
			showTip("请选择安全问题一");
		}
	});

	$("#dna_ques_two").blur(function() {
		var dna_ques_two = $("#dna_ques_two").val();
		if (dna_ques_two == null || dna_ques_two == "") {
			showTip("请选择安全问题二");
		}
	});
	
	$("#dna_ques_three").blur(function() {
		var dna_ques_three = $("#dna_ques_three").val();
		if (dna_ques_three == null || dna_ques_three == "") {
			showTip("请选择安全问题三");
		}
	});
	

	$("#answerOne").blur(function() {
		var answerOne = $("#answerOne").val();
		if (answerOne == null || answerOne == "") {
			showTip("问题一答案不能为空");
			$("#answerOneTip").html("<i></i>");
		} else if (answerOne.length < 1 || answerOne.length > 30) {
			showTip("问题一答案必须为1-30个字符");
		}
	});

	$("#answerTwo").blur(function() {
		var answerTwo = $("#answerTwo").val();
		if (answerTwo == null || answerTwo == "") {
			showTip("问题二答案不能为空");
			$("#sQuestionBtn").text("保存");
			return;
		} else if (answerTwo.length < 1 || answerTwo.length > 30) {
			showTip("问题二答案必须为1-30个字符");
		}
	});
	
	$("#answerThree").blur(function() {
		var answerThree = $("#answerThree").val();
		if (answerThree == null || answerThree == "") {
			showTip("问题三答案不能为空");
			$("#sQuestionBtn").text("保存");
			return;
		} else if (answerThree.length < 1 || answerThree.length > 30) {
			showTip("问题三答案必须为1-30个字符");
		}
	});

	$("#sQuestionBtn").click(function() {
		resetSafeQuestionPage.saveInfoQuestion();
	});
});

/**
 * 保存安全密码数据
 */
ResetSafeQuestionPage.prototype.saveInfoQuestion = function() {

	$("#sQuestionBtn").text("提交中.....");

	var dna_ques_one = $("#dna_ques_one").val();
	var answerOne = $("#answerOne").val();
	var dna_ques_two = $("#dna_ques_two").val();
	var answerTwo = $("#answerTwo").val();
	var dna_ques_three = $("#dna_ques_three").val();
	var answerThree = $("#answerThree").val();

	if (dna_ques_one == null || dna_ques_one == "") {
		showTip("请选择安全问题一");
		$("#sQuestionBtn").text("保存");
		return;
	}

	if (answerOne == null || answerOne == "") {
		showTip("问题一答案不能为空");
		$("#sQuestionBtn").text("保存");
		return;
	} else {
		if (answerOne.length < 1 || answerOne.length > 30) {
			showTip("问题一答案必须为1-30个字符");
			$("#sQuestionBtn").text("保存");
			return;
		}
	}

	if (dna_ques_two == null || dna_ques_two == "") {
		showTip("请选择安全问题二");
		$("#sQuestionBtn").text("保存");
		return;
	}

	if (answerTwo == null || answerTwo == "") {
		showTip("问题二答案不能为空");
		$("#sQuestionBtn").text("保存");
		return;
	} else {
		if (answerTwo.length < 1 || answerTwo.length > 30) {
			showTip("问题二答案必须为1-30个字符");
			$("#sQuestionBtn").text("保存");
			return;
		}
	}
	
	if (answerThree == null || answerThree == "") {
		showTip("问题三答案不能为空");
		$("#sQuestionBtn").text("保存");
		return;
	} else {
		if (answerThree.length < 1 || answerThree.length > 30) {
			showTip("问题二答案必须为1-30个字符");
			$("#sQuestionBtn").text("保存");
			return;
		}
	}
	
	$("#sQuestionBtn").unbind("click");
	var userQuestionList = new Array();
	var password = $("#password").val();
	var userQuestion = {};
	var userQuestion2 = {};
	var userQuestion3 = {};
	
	userQuestion.type = dna_ques_one;
	userQuestion.answer = answerOne;
	
	userQuestion2.type = dna_ques_two;
	userQuestion2.answer = answerTwo;
	
	userQuestion3.type = dna_ques_three;
	userQuestion3.answer = answerThree;
	
	userQuestionList.push(userQuestion);
	
	userQuestionList.push(userQuestion2);
	
	userQuestionList.push(userQuestion3);
	
	var jsonData = {
			"questionList" : userQuestionList,
			"safePassword" : password,
	}
	$.ajax({
		type : "POST",// 请求方式
		url : contextPath + "/safeq/resetSafeQuestions",// 请求路径
		data : JSON.stringify(jsonData),// 将对象序列化成JSON字符串;
		dataType : "json",// 设置请求参数类型
		contentType : 'application/json;charset=utf-8',// 设置请求头信息;
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				showTip("设置成功,三秒后离开此页面...");
				$("#sQuestionBtn").text("保存");
				setTimeout("window.location.href= contextPath + '/gameBet/setting.html'", 3000);
			} else if (result.code == "error") {
				showTip(result.data);
				// 刷新按钮事件
				$("#sQuestionBtn").click(function() {
					resetSafeQuestionPage.saveInfoQuestion();
				});
				$("#sQuestionBtn").text("保存");
			}
		}
	});
};

/**
 * 初始化页面
 */
ResetSafeQuestionPage.prototype.initPage = function() {
	$.ajax({
		type : "POST",
		url : contextPath + "/safeq/loadUserSafeQuestions",
		contentType : 'application/json;charset=UTF-8', 
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var callbackStr = result.data;
				if (callbackStr.length != 0) {
					$("#setNewInfo").hide();
					$("#updateInfo").css({
						display : "block"
					});
					return;
				}
			} else if (result.code == "error") {
				showTip(jQuery(document.body), "操作失败，请重试");
			}
		}
	});
	// 设置问题,获取所有的问题
	$.ajax({
		type : "POST",
		url : contextPath + "/safeq/getSafeQuestionInfos",
		contentType : 'application/json;charset=UTF-8', 
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code != null && result.code == "ok") {
				var dna_ques_oneObj = $("#dna_ques_one");
				var dna_ques_twoObj = $("#dna_ques_two");
				var dna_ques_threeObj = $("#dna_ques_three");

				dna_ques_oneObj.empty();
				dna_ques_oneObj.append("<option value=''>请选择密保问题</option>");
				dna_ques_twoObj.empty();
				dna_ques_twoObj.append("<option value=''>请选择密保问题</option>");
				dna_ques_threeObj.empty();
				dna_ques_threeObj.append("<option value=''>请选择密保问题</option>");
				// 获取后台传输的List参数!
				var eSaveQuestionInfos = result.data;
				for (var i = 0; i < eSaveQuestionInfos.length; i++) {
					var eSaveQuestionInfo = eSaveQuestionInfos[i];
					dna_ques_oneObj.append("<option value='" + eSaveQuestionInfo.code + "'>" + eSaveQuestionInfo.description + "</option>");
					dna_ques_twoObj.append("<option value='" + eSaveQuestionInfo.code + "'>" + eSaveQuestionInfo.description + "</option>");
					dna_ques_threeObj.append("<option value='" + eSaveQuestionInfo.code + "'>" + eSaveQuestionInfo.description + "</option>");
				}
			} else if (result.code != null && result.code == "error") {
				showErrorDlg(jQuery(document.body), "操作失败，请重试");
			} else {
				showErrorDlg(jQuery(document.body), "操作失败，请重试");
			}
		}
	});
};