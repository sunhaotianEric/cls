function SetSafeQuestionPage() {
}
var setSafeQuestionPage = new SetSafeQuestionPage();

setSafeQuestionPage.param = {
	
};

$(document).ready(function() {
	$(".i-person").parent().addClass("on");
	// 当前链接位置标注选中样式
	$("#user_info_save").addClass("selected");

	setSafeQuestionPage.initPage();

	$("#dna_ques_one").blur(function() {
		var dna_ques_one = $("#dna_ques_one").val();
		if (dna_ques_one == null || dna_ques_one == "") {
			showTip("请选择安全问题一");
		}
	});

	$("#dna_ques_two").blur(function() {
		var dna_ques_two = $("#dna_ques_two").val();
		if (dna_ques_two == null || dna_ques_two == "") {
			showTip("请选择安全问题二");
		}
	});
	
	$("#dna_ques_three").blur(function() {
		var dna_ques_two = $("#dna_ques_three").val();
		if (dna_ques_three == null || dna_ques_three == "") {
			showTip("请选择安全问题三");
		}
	});

	$("#answerOne").blur(function() {
		var answerOne = $("#answerOne").val();
		if (answerOne == null || answerOne == "") {
			showTip("问题一答案不能为空");
			$("#answerOneTip").html("<i></i>");
		} else if (answerOne.length < 1 || answerOne.length > 30) {
			showTip("问题一答案必须为1-30个字符");
		}
	});

	$("#answerTwo").blur(function() {
		var answerTwo = $("#answerTwo").val();
		if (answerTwo == null || answerTwo == "") {
			showTip("问题二答案不能为空");
			$("#sQuestionBtn").text("保存");
			return;
		} else if (answerTwo.length < 1 || answerTwo.length > 30) {
			showTip("问题二答案必须为1-30个字符");
		}
	});
	
	$("#answerThree").blur(function() {
		var answerTwo = $("#answerThree").val();
		if (answerThree == null || answerThree == "") {
			showTip("问题三答案不能为空");
			$("#sQuestionBtn").text("保存");
			return;
		} else if (answerThree.length < 1 || answerThree.length > 30) {
			showTip("问题三答案必须为1-30个字符");
		}
	});
	
	$("#password").blur(function() {
		var password = $("#password").val();
		if (password == null || password == "" || password.length < 6 || password.length > 15) {
			showTip("密码格式不正确");
		}

	});
	$("#sQuestionBtn").click(function() {
		setSafeQuestionPage.saveInfoQuestion();
	});

});

/**
 * 保存安全密码数据
 */
SetSafeQuestionPage.prototype.saveInfoQuestion = function() {
	
	//安全问题1以及答案.
	var dna_ques_one = $("#dna_ques_one").val();
	var answerOne = $("#answerOne").val();
	//安全问题2以及答案.
	var dna_ques_two = $("#dna_ques_two").val();
	var answerTwo = $("#answerTwo").val();
	//安全问题3以及答案.
	var dna_ques_three = $("#dna_ques_three").val();
	var answerThree = $("#answerThree").val();
	var password = $("#password").val();

	if (dna_ques_one == null || dna_ques_one == "") {
		showTip("请选择安全问题一");
		return;
	}

	if (answerOne == null || answerOne == "") {
		showTip("问题一答案不能为空");
		return;
	} else {
		if (answerOne.length < 1 || answerOne.length > 30) {
			showTip("问题一答案必须为1-30个字符");
			return;
		}
	}

	if (dna_ques_two == null || dna_ques_two == "") {
		showTip("请选择安全问题二");
		return;
	}

	if (answerTwo == null || answerTwo == "") {
		showTip("问题二答案不能为空");
		return;
	} else {
		if (answerTwo.length < 1 || answerTwo.length > 30) {
			showTip("问题二答案必须为1-30个字符");
			return;
		}
	}

	if (password == null || password == "" || password.length < 6 || password.length > 15) {
		showTip("密码格式不正确");
		return;
	}

	// 密码校验通过,就设置新的安全问题;
	// 新建List对象,用户存储问题以及答案
	var userQuestionList = new Array();
	// 新建问题1
	var userQuestion = {};
	// 新建问题2
	var userQuestion2 = {};
	// 新建问题3
	var userQuestion3 = {};
	
	userQuestion.type = dna_ques_one;
	userQuestion.answer = answerOne;
	
	userQuestion2.type = dna_ques_two;
	userQuestion2.answer = answerTwo;
	
	userQuestion3.type = dna_ques_three;
	userQuestion3.answer = answerThree;
	
	userQuestionList.push(userQuestion);
	userQuestionList.push(userQuestion2);
	userQuestionList.push(userQuestion3);
	
	var jsonData = {
		"questionList" : userQuestionList,
		"safePassword" : password,
	}
	
	$("#sQuestionBtn").text("提交中.....");
	$("#sQuestionBtn").unbind("click");

	$.ajax({
		type : "POST",
		url : contextPath + "/safeq/setSafeQuestions",// 请求路径
		data : JSON.stringify(jsonData),// 将对象序列化成JSON字符串.请求参数
		dataType : "json",// 设置请求参数类型
		contentType : 'application/json;charset=utf-8',// 设置请求头信息;
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				showTip("设置成功...3秒后跳转页面");
				$("#sQuestionBtn").text("保存");
				// 修改成功跳转页面到设置页面
				setTimeout("window.location.href= contextPath + '/gameBet/setting.html'", 3000);
			} else if (result.code == "error") {
				showTip(result.data);
				$("#sQuestionBtn").text("保存");
				//重新绑定点击事件
				$("#sQuestionBtn").click(function() {
					setSafeQuestionPage.saveInfoQuestion();
				});
			}
		}
	});
};

/**
 * 初始化页面
 */
SetSafeQuestionPage.prototype.initPage = function() {
	$.ajax({
		type : "POST",
		url : contextPath + "/usersafe/checkIsSetSafePassword",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				return;
			} else if (result.code == "error") {
				showTip("请先设置安全密码...三秒后离开此页面");
				setTimeout("window.location.href= contextPath + '/gameBet/setting.html'", 3000);
			}
		}
	});
	// 设置安全问题下拉框的值
	$.ajax({
		type : "POST",
		url : contextPath + "/safeq/getSafeQuestionInfos",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var dna_ques_oneObj = $("#dna_ques_one");
				var dna_ques_twoObj = $("#dna_ques_two");
				var dna_ques_threeObj = $("#dna_ques_three");

				dna_ques_oneObj.empty();
				dna_ques_oneObj.append("<option value=''>请选择密保问题</option>");
				dna_ques_twoObj.empty();
				dna_ques_twoObj.append("<option value=''>请选择密保问题</option>");
				dna_ques_threeObj.empty();
				dna_ques_threeObj.append("<option value=''>请选择密保问题</option>");

				var eSaveQuestionInfos = result.data;
				for (var i = 0; i < eSaveQuestionInfos.length; i++) {
					var eSaveQuestionInfo = eSaveQuestionInfos[i];
					dna_ques_oneObj.append("<option value='" + eSaveQuestionInfo.code + "'>" + eSaveQuestionInfo.description + "</option>");
					dna_ques_twoObj.append("<option value='" + eSaveQuestionInfo.code + "'>" + eSaveQuestionInfo.description + "</option>");
					dna_ques_threeObj.append("<option value='" + eSaveQuestionInfo.code + "'>" + eSaveQuestionInfo.description + "</option>");
				}
			} else if (result.code == "error") {
				showErrorDlg(jQuery(document.body), "操作失败，请重试");
			}
		}
	});
};
