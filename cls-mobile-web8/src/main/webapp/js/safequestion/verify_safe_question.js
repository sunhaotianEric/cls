function VerifySafeQuestionPage() {
	
}

var verifySafeQuestionPage = new VerifySafeQuestionPage();

verifySafeQuestionPage.param = {

};

/**
 * 初始化页面
 */
$(document).ready(function() {
	$(".i-person").parent().addClass("on");
	// 当前链接位置标注选中样式
	$("#user_info_save").addClass("selected");

	verifySafeQuestionPage.initPage();

	$("#answer1").blur(function() {
		var answer1 = $("#answer1").val();
		if (answer1 == null || answer1 == "") {
			showTip("问题一答案不能为空");
		} else if (answer1.length < 1 || answer1.length > 30) {
			showTip("问题一答案必须为1-30个字符");
		}
	});

	$("#answer2").blur(function() {
		var answer2 = $("#answer2").val();
		if (answer2 == null || answer2 == "") {
			showTip("问题二答案不能为空");
			$("#sQuestionBtn").text("保存");
			return;
		} else if (answer2.length < 1 || answer2.length > 30) {
			showTip("问题二答案必须为1-30个字符");
		}
	});
	$("#nextQuestionBtn").click(function() {
		verifySafeQuestionPage.checkInfo();
	});
});

/**
 * 检验安全问题数据
 */
VerifySafeQuestionPage.prototype.checkInfo = function() {
	var dna_ques_1 = $("#dna_ques_1").val();
	var answer1 = $("#answer1").val();
	var dna_ques_2 = $("#dna_ques_2").val();
	var answer2 = $("#answer2").val();

	if (dna_ques_1 == null || dna_ques_1 == "") {
		showTip("请选择安全问题一");
		return;
	}

	if (answer1 == null || answer1 == "") {
		showTip("问题一答案不能为空");
		return;
	} else {
		if (answer1.length < 1 || answer1.length > 30) {
			showTip("问题一答案必须为1-30个字符");
			return;
		}
	}

	if (dna_ques_2 == null || dna_ques_2 == "") {
		showTip("请选择安全问题二");
		return;
	}

	if (answer2 == null || answer2 == "") {
		showTip("问题二答案不能为空");
		return;
	} else {
		if (answer2.length < 1 || answer2.length > 30) {
			showTip("问题二答案必须为1-30个字符");
			return;
		}
	}
	$("#nextQuestionBtn").unbind("click");
	var userQuestionList = new Array();
	var userQuestion = {};
	var userQuestion2 = {};
	userQuestion.type = dna_ques_1;
	userQuestion.answer = answer1;
	userQuestion2.type = dna_ques_2;
	userQuestion2.answer = answer2;
	userQuestionList.push(userQuestion);
	userQuestionList.push(userQuestion2);

	$.ajax({
		type : "POST",
		url : contextPath + "/safeq/verifySafeQuestions",
		data : JSON.stringify(userQuestionList),// 将对象序列化成JSON字符串;
		dataType : "json",// 设置请求参数类型
		contentType : 'application/json;charset=utf-8',// 设置请求头信息;
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				window.location.href = contextPath + "/gameBet/safeCenter/resetSafeQuestion.html";
			} else if (result.code == "error") {
				showTip(result.data);
				$("#nextQuestionBtn").click(function() {
					verifySafeQuestionPage.checkInfo();
				});
			}
		}
	});
};

/**
 * 初始化页面,判断用户是否绑定了安全问题.
 */
VerifySafeQuestionPage.prototype.initPage = function() {

	$.ajax({
		type : "POST",
		url : contextPath + "/safeq/loadUserSafeQuestions",
		contentType : 'application/json;charset=UTF-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var dna_ques_1Obj = $("#dna_ques_1");
				var dna_ques_2Obj = $("#dna_ques_2");

				dna_ques_1Obj.empty();
				dna_ques_2Obj.empty();

				var questionInfos = result.data;
				if (questionInfos.length > 0) {
					dna_ques_1Obj.append("<option value='" + questionInfos[0].type + "'>" + questionInfos[0].question + "</option>");
					dna_ques_2Obj.append("<option value='" + questionInfos[1].type + "'>" + questionInfos[1].question + "</option>");
				}

			} else if (result.code == "error") {
				showTip("操作失败，请重试");
			}
		}
	});
};
