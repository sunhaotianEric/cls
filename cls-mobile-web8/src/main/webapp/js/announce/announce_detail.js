function AnnounceDetailPage() {
}
var announceDetailPage = new AnnounceDetailPage();

announceDetailPage.param = {

};

$(document).ready(function() {
	// 当前链接位置标注选中样式
	var announceId = announceDetailPage.param.id; // 获取查询的公告ID
	if (announceId != null) {
		announceDetailPage.showAnnounce(announceId);
	}
});

/**
 * 赋值网站公告信息
 */
AnnounceDetailPage.prototype.showAnnounce = function(id) {
	var jsonData = {
		"id" : id
	};
	$.ajax({
		// 请求方式.
		type : "POST",
		// 请求对应的Controller方法.
		url : contextPath + "/announces/getAnnounceById",
		data : JSON.stringify(jsonData),//将请求参数(对象)序列化成JSON字符串;
		dataType : "json",//设置请求参数类型
		contentType : 'application/json;charset=utf-8',//设置请求头信息;
		// 请求成功返回结果处理.
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				$("#title").html(result.data.title);
				$("#time").html("发布时间:" + result.data.publishTimeStr);
				$("#content").html(result.data.content);
			} else if (result.code == "error") {
				showTip(result.data);
			} 
		}
	});
};