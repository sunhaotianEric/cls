function AnnouncePage(){}
var announcePage = new AnnouncePage();

announcePage.param = {
   	
};

/**
 * 查询参数
 */
announcePage.queryParam = {
		
};

//分页参数
announcePage.pageParam = {
        queryMethodParam : null,
        pageNo : 1,
        pageMaxNo : 1,  //最大的页数
        skipHtmlStr : ""
};


$(document).ready(function() {
	//当前链接位置标注选中样式
	$(".index").removeClass("active");
	announcePage.getAllUserAnnounces(); //查询网站公告数据
});

/**
 * 加载网站公告数据
 */
AnnouncePage.prototype.getAllUserAnnounces = function(){
	announcePage.pageParam.queryMethodParam = 'getAllUserAnnounces';
	announcePage.queryParam = {};
	announcePage.queryConditionUserAnnounces(announcePage.queryParam,announcePage.pageParam.pageNo);
};

/**
 * 刷新列表数据
 */
AnnouncePage.prototype.refreshUserAnnouncePages = function(page){
	var userAnnounceListObj = $("#userAnnounceList");
	
	userAnnounceListObj.html("");
	var str = "";
    var userAnnounces = page.pageContent;
    
    if(userAnnounces.length == 0){
    	str += '<div class="line">';
    	str += 		'<h4 class="nodata">暂无数据</h4>';
    	str += '</div>';
    	str += "";
    	userAnnounceListObj.append(str);
    }else{
    	//记录数据显示 <a href="#"><li><p class="title">手机下载通知手机下载通知手机下载通知手机下载通知</p><p class="time">2016/02/06</p></li></a>
    	for(var i = 0 ; i < userAnnounces.length; i++){
    		var userAnnounce = userAnnounces[i];
    		
    		str += "<a href='"+contextPath+"/gameBet/announceDetail.html?id="+ userAnnounce.id +"'><li class='main-child'><div class='container'>";
    		str += 	"<div class='child-content'>";
    		str += 		"<h2 class='title font-yellow'>"+userAnnounce.title+"</h2>";
    		str += 		"<div class='info'>";
    		str += 			"<span class='time font-gray'>"+ userAnnounce.publishTimeStr +"</span>";
    		str += 		"</div>";
    		str += 	"</div>";
    		str += "<img class='icon pointer' src= '"+contextPath+"/images/icon/icon-pointer.png'>";
    		str += "</div></li></a>";
    		userAnnounceListObj.append(str);
    		str = "";
    	}
    }
};



/**
 * 条件查询投注记录
 */
AnnouncePage.prototype.queryConditionUserAnnounces = function(queryParam,pageNo){
	var jsonData = {"pageNo" : pageNo};
	$.ajax({
		type : "POST",
		url : contextPath + "/announces/announceQuery",
		data : JSON.stringify(jsonData),//将对象序列化成JSON字符串;
		dataType : "json",//设置请求参数类型
		contentType : 'application/json;charset=utf-8',//设置请求头信息;
		success : function(result) {
			commonHandlerResult(result, this.url);
			if(result.code == "ok"){
				announcePage.refreshUserAnnouncePages(result.data);
			}else if(result.code == "error"){
				showTip(result.data);
			}
		}
	});
};


/**
 * 根据条件查找对应页
 */
AnnouncePage.prototype.pageQuery = function(pageNo){
	if(pageNo < 1){
		announcePage.pageParam.pageNo = 1;
	}else if(pageNo > announcePage.pageParam.pageMaxNo){
		announcePage.pageParam.pageNo = announcePage.pageParam.pageMaxNo;
	}else{
		announcePage.pageParam.pageNo = pageNo;
	}
	//如果页码小于等于0，则不进行分页查找
	if(announcePage.pageParam.pageNo <= 0){
		return;
	}
	
	if(announcePage.pageParam.queryMethodParam == 'getAllUserAnnounces'){
		announcePage.getAllUserAnnounces();
	}else{
		alert("类型对应的查找方法未找到.");
	}
};
