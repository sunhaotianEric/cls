function WithdrawPage(){}
var withdrawPage = new WithdrawPage();

withdrawPage.param = {
   //提现银行卡id
   bankId : null,
   //提现金额
   withDrawValue : null,
   //安全密码
   password : null,
   //提现是否包含手续费
   isWithDrawFee : 0
};

withdrawPage.limitParam = {
	//单笔提现最小金额
	withDrawOneStrokeLowestMoney : null,
	//单笔提现最高金额
	withDrawOneStrokeHighestMoney : null
};

$(document).ready(function() {
	
	//加载提现信息
	withdrawPage.loadWithdrawInfo();
	
	withdrawPage.getAppInfo();
	
	//提现点击事件绑定
	$("#withDrawButton").unbind("click").click(withDrawButtonClickEvent);
});

/**
 * 提现按钮点击事件
 */
var withDrawButtonClickEvent = function(){
	var withDrawValue = $("#withDrawValue").val();
	var password = $("#withDrawPassword").val();
	
	if(withDrawValue == null || withDrawValue == ""){
		showTip("请输入提现金额");
		return;
	}
	if(withdrawPage.limitParam.withDrawOneStrokeLowestMoney != null && parseFloat(withDrawValue) < withdrawPage.limitParam.withDrawOneStrokeLowestMoney){
		showTip("提现金额小于设定的最小提现金额");
		return false;
	}
	if(withdrawPage.limitParam.withDrawOneStrokeHighestMoney != null && parseFloat(withDrawValue) > withdrawPage.limitParam.withDrawOneStrokeHighestMoney){
		showTip("提现金额超过设定的最大提现金额");
		return false;
	}
	
	//取现银行卡
	if(withdrawPage.param.bankId == null){
		showTip("对不起,您还没选择好要取现的银行卡.");
		return false;
	}
	//安全密码
	if(password == null || password == "" || password.length < 6 || password.length > 15){
		showTip("安全密码格式不正确.");
		return;
	}
	
	withdrawPage.param.withDrawValue = withDrawValue;
	withdrawPage.param.password = password;
	
	comfirm("温馨提示", "确定要进行提现吗?", "取消", function(){}, "确定", function(){
		//调用提现事件
		withdrawPage.withDrawApplySure();
	});
}

/**
 * 确认取现
 */
WithdrawPage.prototype.withDrawApplySure = function(){
	var param={};
	param.bankId=withdrawPage.param.bankId;
	param.withDrawValue=withdrawPage.param.withDrawValue;
	param.password=withdrawPage.param.password;
	param.isWithDrawFee=withdrawPage.param.isWithDrawFee;
	$.ajax({
		type: "POST",
        url: contextPath +"/withdraw/withDrawApply",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(param), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if (result.code == "ok") {
        		$("#withDrawValue").val("");
    			$("#withDrawPassword").val("");
    			showTip("提现申请成功");
    			var str="window.location.href='"+contextPath+"/front/withdraw/withdraw_order.jsp'";
    			setTimeout(str, 1000);
    			
    		}else if(result.code == "error") {
    			var data = result.data;
    			if(data == "FeeTip"){
    				var data2 = result.data2;
    				var data3 = result.data3;
             		comfirm("温馨提示", "您当前申请的提现金额超过您的可提现额度,超出部分:"+data2+"元将会收取"+data3+"%手续费,是否继续操作?", "取消", function(){}, "确定", function(){
             			//调用提现事件
                 		withdrawPage.param.isWithDrawFee=1;
             			withdrawPage.withDrawApplySure();
         			});
             	} else {
             		showTip(data);
             	}
    		}
        }
	});
};


/**
 * 加载当前App用户信息
 */
WithdrawPage.prototype.getAppInfo = function(){
	$.ajax({
		type: "POST",
        url: contextPath +"/withdraw/getWithdrawConfig",
        contentType :"application/json;charset=UTF-8",
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
            var data = result.data;
            var data2 = result.data2;
            var data3 = result.data3;
        	if(result.code == "ok"){
        		withdrawPage.limitParam.withDrawOneStrokeLowestMoney = data;
    			withdrawPage.limitParam.withDrawOneStrokeHighestMoney = data2;
    			$("#withDrawEveryDayNumber").text(data3);
    			$("#withDrawOneStrokeLowestMoney").text(data);
    			$("#withDrawOneStrokeHighestMoney").text(data2);
        	}else if(result.code == "error"){
        		var err = result.data;
    			showTip(err);
        	}else{
        		showTip("刷新用户的请求失败.");
        	}
        }
	});
};
/**
 * 加载当前用户信息 用户名余额 可去取现的银行卡
 */
WithdrawPage.prototype.loadWithdrawInfo = function(){
	$.ajax({
		type: "POST",
        url: contextPath +"/withdraw/loadWithdrawInfo",
        contentType :"application/json;charset=UTF-8",
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
        	if (result.code == "ok") {
        		var data = result.data;
            	var data2 = result.data2;
            	var data3 = result.data3;
        		$("#withdrawUserName").text(currentUser.userName);
    			$("#withdrowMoney").text(data2.toFixed(2));
    			$("#withDrawValue").attr("placeholder", "当前可提现金额" + data3.toFixed(2) +"");
    			withdrawPage.showRechargeTypes(data);
    		} else if(result.code == "error"){
    			if(result.data == "SafePasswordIsNull") {
    				//没有设置安全密码的情况
    				showTip("您当前没有设置安全密码，请立即前往绑定");
    				var str="window.location.href='"+contextPath+"/front/setting/setting.jsp'";
    				setTimeout(str, 1000);
    			} else if(result.data == "BankCardIsNull") {
    				//没有绑定银行卡的情况
    				showTip("您当前没有绑定银行卡，请立即前往绑定");
    				var str="window.location.href='"+contextPath+"/front/setting/setting.jsp'";
    				setTimeout(str, 1000);
    			}
    		}
        }
	});
};

/**
 * 展示可充值的银行卡
 */
WithdrawPage.prototype.showRechargeTypes = function(banks){
	if(banks != null && banks.length > 0){
		var rechargeBanksObj = $("#withdrawbanklistinfo");
		rechargeBanksObj.html("");
	    var str = "";
		for(var i = 0; i < banks.length; i++){
			var rechargeBank = banks[i];
			var startPosition = rechargeBank.bankCardNum.length-4;
			var endPosition = rechargeBank.bankCardNum.length;
			var bankNumPostion = rechargeBank.bankCardNum;
			var paybankNumTmp = bankNumPostion.substring(startPosition,endPosition);
			var payBankNum = "＊＊＊＊＊＊＊＊＊＊" + paybankNumTmp;
			
			str += "<div class='container'>";
			str += "	<div class='line-title'><img src='"+ withdrawPage.getBankImgLogo(rechargeBank.bankType) +"' alt=''></div>";
			str += "	<div class='line-content'>";
			str += "   		<input type='hidden' data-id='"+rechargeBank.id+"' bankname='"+rechargeBank.bankName+"' data-value='"+rechargeBank.bankType+"'/>";
			str += "		<h2 class='title' value='"+rechargeBank.bankName+"'>"+rechargeBank.bankName+"</h2>";
			str += "		<p class='mun' value='"+ payBankNum +"'>"+ paybankNumTmp +"</p>";
			str += "	</div>";
			str += "</div>";
			
			//默认选择第一条
			if(i == 0) {
				var currentWithdrawEle = $(".select-container");
				currentWithdrawEle.find(".value").val(rechargeBank.bankBank);
				currentWithdrawEle.find(".mun_value").val(rechargeBank.bankBank);
				currentWithdrawEle.find(".title").html(rechargeBank.bankBank);
				currentWithdrawEle.find(".mun").html(payBankNum);
				currentWithdrawEle.find(".line-title img").attr("src", withdrawPage.getBankImgLogo(rechargeBank.bankType));
				withdrawPage.param.bankId = rechargeBank.id;
			}
			//str += "<li onclick='withdrawPage.chooseWithDrawBank(this)' data-id='"+rechargeBank.id+"'><p><span name='rechargeBankChooseName'>"+ rechargeBank.bankBank +"  &nbsp;&nbsp;&nbsp;尾号:"+ paybankNumTmp +"</span></p></li>";
		};
		rechargeBanksObj.html(str);
		
		//绑定下拉选择事件
		select();
		$(".select-list .container").unbind();
		$(".select-list .container").click(function(){
			var value=$(this).find(".title").attr("value");
			var html=$(this).find(".title").html();

			var mun_value=$(this).find(".mun").attr("value");
			var mun_html=$(this).find(".mun").html();

			var logo=$(this).find(".line-title img").attr("src");
			var parent=$(this).closest(".select");
			parent.find(".select-container .value").val(value);
			parent.find(".select-container .title").html(html);
			parent.find(".select-container .mun_value").val(mun_value);
			parent.find(".select-container .mun").html(mun_html);
			parent.find(".select-container .line-title img").attr("src",logo);
			
			//withdrawPage.chooseWithDrawBank(this);
			withdrawPage.param.bankId = $(this).find("input").attr("data-id");
		});
	}
	
};

/**
 * 获取银行logo图片地址
 */
WithdrawPage.prototype.getBankImgLogo = function(bankType) {
	var imgLogo = "";
	if(bankType == "ICBC") {
		imgLogo = contextPath+"/images/bankcard_logo/gongshang.png";
	} else if(bankType == "CCB") {
		imgLogo = contextPath+"/images/bankcard_logo/jianshe.png";
	} else if(bankType == "ABC") {
		imgLogo = contextPath+"/images/bankcard_logo/nongye.png";
	} else if(bankType == "PSBC") {
		imgLogo = contextPath+"/images/bankcard_logo/youzheng.png";
	} else if(bankType == "CMBC") {
		imgLogo = contextPath+"/images/bankcard_logo/minsheng.png";
	} else if(bankType == "CIB") {
		imgLogo = contextPath+"/images/bankcard_logo/xingye.png";
	} else if(bankType == "COMM") {
		imgLogo = contextPath+"/images/bankcard_logo/jiaotong.png";
	} else if(bankType == "CEB") {
		imgLogo = contextPath+"/images/bankcard_logo/gaungda.png";
	} else if(bankType == "BOC") {
		imgLogo = contextPath+"/images/bankcard_logo/zhongguo.png";
	} else if(bankType == "SPABANK") {
		imgLogo = contextPath+"/images/bankcard_logo/pingan.png";
	} else if(bankType == "GDB") {
		imgLogo = contextPath+"/images/bankcard_logo/guangfa.png";
	} else if(bankType == "CITIC") {
		imgLogo = contextPath+"/images/bankcard_logo/zhongxin.png";
	} else if(bankType == "HXBANK") {
		imgLogo = contextPath+"/images/bankcard_logo/huaxia.png";
	} else if(bankType == "CMB") {
		imgLogo = contextPath+"/images/bankcard_logo/zhaoshang.png";
	}
	return imgLogo;
};




