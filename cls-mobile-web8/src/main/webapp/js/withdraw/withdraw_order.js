function WithdrawOrderPage(){}
var withdrawOrderPage = new WithdrawOrderPage();

withdrawOrderPage.param = {
	queryType : "RECHARGE"
};

/**
 * 查询参数
 */
withdrawOrderPage.queryParam = {
		statuss : null,
		payType : null,
		createdDateStart : null,
		createdDateEnd : null,
		queryScope:1,
		dateRange :1,
};

//分页参数
withdrawOrderPage.pageParam = {
        pageNo : 1
};

$(document).ready(function() {
	animate_add(".user_withdraw_orderCtrl .main","fadeInUp");
	withdrawOrderPage.pageParam.pageNo = 1;
	var strDate = new Date();
	withdrawOrderPage.queryParam.createdDateStart=strDate.setHours(3,-1,60,999);
	withdrawOrderPage.queryParam.createdDateEnd=strDate.AddDays(1).setHours(2,59,-1,999);
	//点击切换查询
    $(".move-content").find("p").click(function(){
         var value = $(this).attr("data-value");
         var todate = new Date();
 			  
 		if(value=="today"){
 			withdrawOrderPage.queryParam.reportType = 1;
 			withdrawOrderPage.queryParam.dateRange = 1;
 			/*withdrawOrderPage.queryParam.createdDateStart=todate.setHours(3,-1,60,999);
 			withdrawOrderPage.queryParam.createdDateEnd=todate.AddDays(1).setHours(2,59,-1,999);*/
 			$(".header-right").find("span").html("今天");
 		}else if(value=="yesterDay"){
 			withdrawOrderPage.queryParam.reportType=0;
 			withdrawOrderPage.queryParam.dateRange = -1;
 			/*withdrawOrderPage.queryParam.createdDateEnd=todate.setHours(2,59,59,999);
 			withdrawOrderPage.queryParam.createdDateStart=todate.AddDays(-1).setHours(3,-1,60,999);*/
 			$(".header-right").find("span").html("昨天");
 		}else if(value=="month"){
 			withdrawOrderPage.queryParam.reportType=0;
 			withdrawOrderPage.queryParam.dateRange = 30;
 			/*var defalutDate=new Date();
 			defalutDate.setDate(1);
 			defalutDate.setHours(3,0,0,0);
 			withdrawOrderPage.queryParam.createdDateStart=defalutDate;
 			withdrawOrderPage.queryParam.createdDateEnd=todate.setHours(2,59,-1,999);*/
 			$(".header-right").find("span").html("本月");
 		}else if(value=="lastmonth"){
 			withdrawOrderPage.queryParam.reportType=0;
 			withdrawOrderPage.queryParam.dateRange = -30;
			 /*var defalutDate=new Date();
			 defalutDate.setDate(1);
			 defalutDate.setHours(3,0,0,0);
			 todate.setMonth(todate.getMonth()-1);
			 todate.setDate(1);
			 todate.setHours(3,0,0,0);
			 withdrawOrderPage.queryParam.createdDateStart=todate;
			 withdrawOrderPage.queryParam.createdDateEnd=defalutDate;*/
			 $(".header-right").find("span").html("上个月");
		}
 		     
 		var parent=$(this).closest("div.lottery-classify1");
 		parent.hide();
 		$(".alert-bg").stop(false,true).fadeToggle(500);
 		withdrawOrderPage.findUserWithDrawOrderByQueryParam();
    })
	//进入页面查询取现明细
	withdrawOrderPage.findUserWithDrawOrderByQueryParam();
});



/**
 * 按页面条件查询提现数据
 */
WithdrawOrderPage.prototype.findUserWithDrawOrderByQueryParam = function(){
	withdrawOrderPage.queryConditionUserWithDrawOrders(withdrawOrderPage.queryParam,withdrawOrderPage.pageParam.pageNo);
};


/**
 * 条件查询取现记录
 */
WithdrawOrderPage.prototype.queryConditionUserWithDrawOrders = function(queryParam,pageNo){
	var param={}
	if(queryParam.statuss!=null && queryParam.statuss!=""){
		param.statuss=queryParam.statuss;
	}
    if(queryParam.payType!=null && queryParam.payType!=""){
    	param.payType=queryParam.payType;
	}
    if(queryParam.queryScope!=null && queryParam.queryScope!=""){
		param.queryScope=queryParam.queryScope;
	}
    
    param.dateRange = queryParam.dateRange;
    /*param.createdDateStart=new Date(queryParam.createdDateStart).Format("yyyy-MM-ddThh:mm:ss.000Z");
    param.createdDateEnd=new Date(queryParam.createdDateEnd).Format("yyyy-MM-ddThh:mm:ss.000Z");*/
    /*param.pageNo=pageNo;*/
    var query={
    		"query":param,
    		"pageNo":pageNo
    }
	$.ajax({
		type: "POST",
        url: contextPath +"/withdraw/withDrawOrderQuery",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(query), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
            var data=result.data;
        	if(result.code == "ok"){
        		withdrawOrderPage.refreshUserWithdrawOrderPages(data);
        	}else if(result.code == "error"){
    			showTip(result.data);
        	}
        }
	});
};

/**
 * 刷新列表数据
 */
WithdrawOrderPage.prototype.refreshUserWithdrawOrderPages = function(page){
	$(".span_more").remove();
	var userWithDrawOrderListObj = $("#userWithdrawOrderList");
	
	var str = "";
	var userWithDrawOrders = page.pageContent;
	
	//如果是第一页
	if(page.pageNo == 1) {
		userWithDrawOrderListObj.html("");
		if(userWithDrawOrders.length == 0){
			str += '<div class="line">';
	    	str += 		'<h4 class="nodata">暂无数据</h4>';
	    	str += '</div>';
	       
	    	userWithDrawOrderListObj.append(str);
	    }
	} 
    if(userWithDrawOrders != null && userWithDrawOrders.length > 0){
    	//记录数据显示
    	for(var i = 0 ; i < userWithDrawOrders.length; i++){
    		
    		var userWithDrawOrder = userWithDrawOrders[i];
    		var serialNumberStr = userWithDrawOrder.serialNumber;
    		
    		str += '<div class="line">';
    		str += 		'<div class="child child1"><p>'+  serialNumberStr.substring(serialNumberStr.lastIndexOf('_')+1,serialNumberStr.length) +'</p></div>';
    		str += 		'<div class="child child2"><p>'+ userWithDrawOrder.applyValue +'</p></div>';
    		str += 		'<div class="child child4" ><p>'+ userWithDrawOrder.createdDateStr +'</p></div>';
    		
    		if(userWithDrawOrder.statusStr == "提现成功"){
    			str += 		'<div class="child child5 font-green">';
    		}else if(userWithDrawOrder.statusStr == "提现失败"){
    			str += 	'<div class="child child5 font-red">';
    		}else if(userWithDrawOrder.statusStr == "支付中"){
    			str += 	'<div class="child child5 font-yellow">';
    		}else{
    			str += 	'<div class="child child5 font-blue">';
    		}
    		
    		str += '<p>'+ userWithDrawOrder.statusStr +'</p></div>';
    		str += '</div>';
    		
    		userWithDrawOrderListObj.append(str);
    		str = "";
    	}
    	//如果当前页数小于总页数，显示加载更多
    	if(page.pageNo < page.totalPageNum) {
    		$(".user-btn").show();
    		
    		//添加加载更多事件
    		$(".user-btn").unbind("click").click(function(){
    			withdrawOrderPage.pageParam.pageNo++;
    			
    			withdrawOrderPage.findUserWithDrawOrderByQueryParam();
    		});
    	}
    }
	
};

Date.prototype.Format = function (fmt) {
	    var o = {
	        "M+": this.getMonth() + 1, //月份 
	        "d+": this.getDate(), //日 
	        "h+": this.getHours(), //小时 
	        "m+": this.getMinutes(), //分 
	        "s+": this.getSeconds(), //秒 
	        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
	        "S": this.getMilliseconds() //毫秒 
	    };
	    if (/(y+)/.test(fmt))fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
	    for (var k in o){
	    if (new RegExp("(" + k + ")").test(fmt)) {fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));}
	    }
	    return fmt;
	}

