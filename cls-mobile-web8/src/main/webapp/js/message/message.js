function MessagePage() {
	
}

var messagePage= new MessagePage();

messagePage.param = {
	pitchArray : new Array()
};

/**
 * 查询参数
 */
messagePage.queryParam = {
	createdDateStart : null,
	createdDateEnd : null,
	type : null,
	status : null,
	pageNo : 1,
};

$(document).ready(function() {
	$(".i-person").parent().addClass("on");
	// 查询我的收件箱
	messagePage.queryConditionUserNoteList();
});

/**
 * 条件查询投注记录
 */
MessagePage.prototype.queryConditionUserNoteList = function() {
	var query = {
		"createdDateStart" : messagePage.queryParam.createdDateStart,
		"createdDateEnd" : messagePage.queryParam.createdDateEnd,
		"type" : messagePage.queryParam.type,
		"status" : messagePage.queryParam.status,
	};
	var jsonData = {
		"query" : query,
		"pageNo" : messagePage.queryParam.pageNo
	}
	$.ajax({
		type : "POST",
		url : contextPath + "/note/getToMeNotes",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result,pageNo) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				messagePage.refreshMessagePages(result.data);
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 刷新列表数据
 */
MessagePage.prototype.refreshMessagePages = function(page) {
	var noteListObj = $("#noteList");
	var userNoteLists = page.pageContent;

	noteListObj.html("");
	var str = "";
	if (userNoteLists.length == 0) {
		str += '<div class="line">';
		str += '<h4 class="nodata">暂无数据</h4>';
		str += '</div>';
		noteListObj.append(str);
		str += "";
	} else {
		for (var i = 0; i < userNoteLists.length; i++) {
			var userNote = userNoteLists[i];

			str += "<a href='" + contextPath + "/gameBet/messageDetail.html?id=" + userNote.id + "'><li class='main-child'><div class='container'>";
			str += "<div class='child-content'>";
			str += "<h2 class='title font-yellow'>" + userNote.sub + "</h2>";
			str += "<div class='info'>";
			str += "<span class='time font-gray'>" + userNote.createDateStr + "</span>";
			str += "</div>";
			str += "</div>";
			str += "<img class='icon pointer' src= '" + contextPath + "/images/icon/icon-pointer.png'>";
			str += "</div></li></a>";

		}
		noteListObj.append(str);
	}

};
