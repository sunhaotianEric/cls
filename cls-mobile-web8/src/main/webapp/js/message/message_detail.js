function MessageDetailPage() {

};

var messageDetailPage = new MessageDetailPage();

messageDetailPage.parem = {

};

$(document).ready(function() {
	$(".i-person").parent().addClass("on");
	var noteId = messageDetailPage.parem.id;
	messageDetailPage.getNoteDetailById(noteId);
});

/**
 * 获取消息详情
 * 
 * @param noteId
 */
MessageDetailPage.prototype.getNoteDetailById = function(noteId) {
	var jsonData = {
		"noteId" : noteId,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/note/getNoteDetailList",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				messageDetailPage.refreshMessageDetailPages(result.data, noteId);
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});

};

MessageDetailPage.prototype.refreshMessageDetailPages = function(notes, noteId) {
	var noteDetailListObj = $("#noteDetailId");
	noteDetailListObj.html("");

	if (notes.length > 0) {
		// 主题显示
		var str = "";
		str += "<div class='child-content'><h2 class='title'>主题:" + notes[0].sub + "</h2>";
		str += "<div class='info'>";
		// if(note.fromUserName == currentUser.username){
		// str += "<p >发件人:自己</p>";
		// str += "<p class='time font-gray'>"+ note.createDateStr +"</p>";
		// }else{
		 if (notes[0].fromUserName != "") {
			str += "<p >发件人:"+notes[0].fromUserName+"</p>";
			str += "<p class='time font-gray'>" + notes[0].createDateStr + "</p>";
		} else {
			showTip("该消息类型未配置.");
		}
		;
		// }
		str += "</div>";
		str += "</div>";
		str += "<img class='icon pointer' src='" + contextPath + "/images/icon/icon-pointer.png'>";
		noteDetailListObj.append(str);

		// 内容显示
		var replyStr = '';
		for (var i = 0; i < notes.length; i++) {
			var note = notes[i];
			replyStr += '<div class="reply-block">';
			if (note.type == 'UP_DOWN') {
				replyStr += '		<h2>上级</h2><span class="time">' + note.createDateStr + '</span>';
				replyStr += '		<p>' + note.body + '</p>';
			} else if (note.type == "SYSTEM") {
				replyStr += '		<div style="display:none"><h2>上级</h2><span class="time">' + note.createDateStr + '</span></div>';
				replyStr += '		<p style="padding-left:0;">' + note.body + '</p>';
			} else {
				replyStr += '		<h2>' + note.fromUserName + '</h2><span class="time">' + note.createDateStr + '</span>';
				replyStr += '		<p>' + note.body + '</p>';
			}
			replyStr += '</div>';
		}
		;
		$("#bodyId").append(replyStr);
		messageDetailPage.signNoteYetRead(noteId); // 将消息记录标志位已读
	} else {
		noteDetailListObj.append(str);
		$("#bodyId").html("<p>暂时没有消息</p>");
	}
};

/**
 * 打开收件箱，标志该消息为已读
 */
MessageDetailPage.prototype.signNoteYetRead = function(noteId) {
	var jsonData = {
		"parentId" : noteId,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/note/signNoteYetRead",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {

			} else if (result.code == "error") {
				showTip(result.data);
			}
		}
	})

};