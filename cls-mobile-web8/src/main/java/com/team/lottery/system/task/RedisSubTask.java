package com.team.lottery.system.task;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.enums.ERedisChannel;
import com.team.lottery.redis.mq.RedisThread;
import com.team.lottery.redis.mq.Subscriber;
import com.team.lottery.system.SystemListener;
/**
 * redis重新消息订阅任务
 * @author Administrator
 *
 */
public class RedisSubTask extends TimerTask{
	private static Logger logger = LoggerFactory.getLogger(RedisSubTask.class);
   public void run() {
		String str = "当前系统时间"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+"订阅状态是有订阅"+SystemListener.clsRedisPubSubListener.isSubscribed()+":"+SystemListener.clsRedisPubSubListener.getSubscribedChannels()+",已经重新订阅!";
		logger.info(str);
		Subscriber.setClsRedisPubSubListener(SystemListener.clsRedisPubSubListener);
		if(SystemListener.clsRedisPubSubListener.isSubscribed()){
			Subscriber.isSubscribed = 0;
			SystemListener.clsRedisPubSubListener.unsubscribe(ERedisChannel.CLSCACHE.name());
		}		
		new Thread(new RedisThread(ERedisChannel.CLSCACHE.name(),SystemListener.clsRedisPubSubListener)).start();
		
   }
}
