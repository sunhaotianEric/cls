package com.team.lottery.system;


import java.lang.reflect.InvocationTargetException;
import java.util.Timer;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.cache.ChatCacheManager;
import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.redis.mq.ClsRedisPubSubListener;
import com.team.lottery.system.cache.LotteryIssueCache;
import com.team.lottery.system.message.MessageQueueThread;
import com.team.lottery.system.task.RedisSubTask;

public class SystemListener implements ServletContextListener{
	
	private static Logger logger = LoggerFactory.getLogger(SystemListener.class);
	public static ClsRedisPubSubListener clsRedisPubSubListener=new ClsRedisPubSubListener();
	private Timer timer;
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		
		
		//初始化业务系统缓存
		ClsCacheManager.initCache();
		
		
		//缓存数据初始化
		SystemCache.loadAllCacheData();
		//初始化聊天缓存
//		ChatCacheManager.initCache();
		//初始化当前系统常量配置
		SystemPropeties.loginType = "MOBILE";
		SystemPropeties.serverNum = "3";
		
		
		//初始最新开奖号码缓存 此缓存弃用，改用redis实现
		//LotteryCodeCache.initCache();
		//初始化开奖时间缓存
		LotteryIssueCache.initCache();
		
		//初始化业务系统参数配置
		try {
			BizSystemConfigManager.initAllBizSystemConfig();
		} catch (IllegalAccessException e) {
			logger.debug("初始化业务系统资源配额参数配置错误："+e.getMessage());
		} catch (InvocationTargetException e) {
			logger.debug("初始化业务系统资源配额参数配置错误："+e.getMessage());
		}
		
		//启动消息接收处理线程
		MessageQueueThread messageQueueThread = new MessageQueueThread();
		messageQueueThread.start();
		//reids订阅  线程监听 系统的
		timer = new Timer();
		timer.schedule(new RedisSubTask(),30 * 1000,60 * 60 * 1000);
		
		logger.info("cls-mobile-web8 start success");
		
//		final List<Thread> threads = new ArrayList<Thread>();
//		Thread thread  = new Thread(new RedisThread(ERedisChannel.CLSCACHE.name(),clsRedisPubSubListener));
//		threads.add(thread);
//		final ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
//		Thread startThread = new Thread(new Runnable() {
//			
//			@Override
//			public void run() {
//				//采用线程池启动线程集合  每隔4秒启动一个线程
//				for (int i = 0; i < threads.size(); i++) {
//					try {
//						Thread.sleep(1800 * 1000);
//					} catch (InterruptedException e) {
//						e.printStackTrace();
//					}
//					cachedThreadPool.execute(threads.get(i));
//				}
//			}
//		});
//		startThread.start();
		
	}
	


}
