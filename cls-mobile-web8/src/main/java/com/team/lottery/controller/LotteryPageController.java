package com.team.lottery.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 投注页面相关路径
 * 
 * @author luocheng
 *
 */
@Controller
public class LotteryPageController {

	/**
	 * 三分快三
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/sfks/sfks.html", method = RequestMethod.GET)
	public String toSfks() {
		return "front/lottery/sfks/sfks";
	}
	
	/**
	 * 五分快三
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/wfks/wfks.html", method = RequestMethod.GET)
	public String toWfks() {
		return "front/lottery/wfks/wfks";
	}
	
	/**
	 * 三分时时彩
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/sfssc/sfssc.html", method = RequestMethod.GET)
	public String toSfssc() {
		return "front/lottery/sfssc/sfssc";
	}
	
	/**
	 * 五分时时彩
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/wfssc/wfssc.html", method = RequestMethod.GET)
	public String toWfssc() {
		return "front/lottery/wfssc/wfssc";
	}
	
	/**
	 * 重庆时时彩
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/cqssc/cqssc.html", method = RequestMethod.GET)
	public String toCqssc() {
		return "front/lottery/cqssc/cqssc";
	}

	/**
	 * 安徽快三
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/ahks/ahks.html", method = RequestMethod.GET)
	public String toAhks() {
		return "front/lottery/ahks/ahks";
	}

	/**
	 * 北京快三
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/bjks/bjks.html", method = RequestMethod.GET)
	public String toBjks() {
		return "front/lottery/bjks/bjks";
	}

	/**
	 * 北京PK10
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/bjpks/bjpk10.html", method = RequestMethod.GET)
	public String toBjpks() {
		return "front/lottery/bjpks/bjpk10";
	}

	/**
	 * 福彩3D
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/fcsddpc/fcsddpc.html", method = RequestMethod.GET)
	public String toFcsddpc() {
		return "front/lottery/fcsddpc/fcsddpc";
	}

	/**
	 * 福建十一选五
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/fjsyxw/fjsyxw.html", method = RequestMethod.GET)
	public String toFjsyxw() {
		return "front/lottery/fjsyxw/fjsyxw";
	}

	/**
	 * 广东十一选五
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/gdsyxw/gdsyxw.html", method = RequestMethod.GET)
	public String toGdsyxw() {
		return "front/lottery/gdsyxw/gdsyxw";
	}
	
	/**
	 * 五分十一选五
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/wfsyxw/wfsyxw.html", method = RequestMethod.GET)
	public String toWfsyxw() {
		return "front/lottery/wfsyxw/wfsyxw";
	}
	
	/**
	 * 三分十一选五
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/sfsyxw/sfsyxw.html", method = RequestMethod.GET)
	public String toSfsyxw() {
		return "front/lottery/sfsyxw/sfsyxw";
	}

	/**
	 * 甘肃快三
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/gsks/gsks.html", method = RequestMethod.GET)
	public String toGsks() {
		return "front/lottery/gsks/gsks";
	}

	/**
	 * 广西快三
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/gxks/gxks.html", method = RequestMethod.GET)
	public String toGxks() {
		return "front/lottery/gxks/gxks";
	}

	/**
	 * 湖北快三
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/hbks/hbks.html", method = RequestMethod.GET)
	public String toHbks() {
		return "front/lottery/hbks/hbks";
	}

	/**
	 * 吉林快三
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/jlks/jlks.html", method = RequestMethod.GET)
	public String toJlks() {
		return "front/lottery/jlks/jlks";
	}

	/**
	 * 江苏快三
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/jsks/jsks.html", method = RequestMethod.GET)
	public String toJsks() {
		return "front/lottery/jsks/jsks";
	}

	/**
	 * 极速PK10
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/jspks/jspk10.html", method = RequestMethod.GET)
	public String toJspk10() {
		return "front/lottery/jspks/jspk10";
	}

	/**
	 * 江西11选5
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/jxsyxw/jxsyxw.html", method = RequestMethod.GET)
	public String toJxsyxw() {
		return "front/lottery/jxsyxw/jxsyxw";
	}

	/**
	 * 幸运快三
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/jyks/jyks.html", method = RequestMethod.GET)
	public String toJyks() {
		return "front/lottery/jyks/jyks";
	}

	/**
	 * 六合彩
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/lhc/lhc.html", method = RequestMethod.GET)
	public String toLhc() {
		return "front/lottery/lhc/lhc";
	}

	/**
	 * 山东十一选五
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/sdsyxw/sdsyxw.html", method = RequestMethod.GET)
	public String toSdsyxw() {
		return "front/lottery/sdsyxw/sdsyxw";
	}

	/**
	 * 上海快三
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/shks/shks.html", method = RequestMethod.GET)
	public String toShks() {
		return "front/lottery/shks/shks";
	}

	/**
	 * 天津时时彩
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/tjssc/tjssc.html", method = RequestMethod.GET)
	public String toTjssc() {
		return "front/lottery/tjssc/tjssc";
	}

	/**
	 * 台湾五分彩
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/twwfc/twwfc.html", method = RequestMethod.GET)
	public String toTwwfc() {
		return "front/lottery/twwfc/twwfc";
	}

	/**
	 * 新疆时时彩
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/xjssc/xjssc.html", method = RequestMethod.GET)
	public String toXjssc() {
		return "front/lottery/xjssc/xjssc";
	}

	/**
	 * 幸运28
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/xyeb/xy28.html", method = RequestMethod.GET)
	public String toXyeb() {
		return "front/lottery/xyeb/xy28";
	}

	/**
	 * 幸运分分彩
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/xyffc/xyffc.html", method = RequestMethod.GET)
	public String toXyffc() {
		return "front/lottery/xyffc/xyffc";
	}

	/**
	 * 幸运六合彩
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/xylhc/xylhc.html", method = RequestMethod.GET)
	public String toXylhc() {
		return "front/lottery/xylhc/xylhc";
	}

}
