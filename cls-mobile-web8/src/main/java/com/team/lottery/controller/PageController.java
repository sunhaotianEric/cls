package com.team.lottery.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 手机端页面控制器
 * 
 * @author luocheng 
 *
 */
@Controller
public class PageController {
	/**
	 * 跳转主页
	 */
	@RequestMapping(value = "/gameBet/index.html", method = RequestMethod.GET)
	public String toIndexPage() {
		return "front/index/index";
	}
	
	/**
	 * 跳转登录页
	 */
	@RequestMapping(value = "/gameBet/login.html", method = RequestMethod.GET)
	public String toLoginPage() {
		return "front/login/login";
	}
	
	/**
	 * 跳转彩票大厅
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/hall.html", method = RequestMethod.GET)
	public String toLotteryHallPage() {
		return "front/hall/hall";
	}
	
	/**
	 * 跳转活动页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/activity.html", method = RequestMethod.GET)
	public String toActivityPage() {
		return "front/activity/activity";
	}
	
	/**
	 * 获取活动详情
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/activityDetail.html", method = RequestMethod.GET)
	public String toGetActivityDetail() {
		return "front/activity/activity_detail";
	}
	
	/**
	 * 新手大礼包!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/newUserGift.html", method = RequestMethod.GET)
	public String toActivityNewUserGift() {
		return "front/activity/new_user_gift";
	}
	
	/**
	 * 发现(中奖信息.)!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/wininfo.html", method = RequestMethod.GET)
	public String toAwardPage() {
		return "front/wininfo/win_info";
	}
	
	/**
	 * 发现(开奖号码.)!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lotterycode.html", method = RequestMethod.GET)
	public String toAwardPage1() {
		return "front/lotterycode/lottery_code";
	}
	
	/**
	 * 发现(开奖号码详情.)!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lotterycodeDetail.html", method = RequestMethod.GET)
	public String toLotteryCodeDetail() {
		return "front/lotterycode/lottery_code_detail";
	}
	
	/**
	 * App下载!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/appdownload.html", method = RequestMethod.GET)
	public String toAppdownload() {
		return "front/appdownload/appdownload";
	}
	
	/**
	 * 跳转注册.
	 */
	@RequestMapping(value = "/gameBet/reg.html", method = RequestMethod.GET)
	public String toReg() {
		return "front/reg/reg";
	}
	
	/**
	 * 忘记密码!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/forgetPwd.html", method = RequestMethod.GET)
	public String toForgetpassword() {
		return "front/forgetpwd/forget_pwd";
	}
	
	/**
	 * 忘记密码(选择找回密码方式.)!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/forgetPwd/chooseWay.html", method = RequestMethod.GET)
	public String toRetrieveMode() {
		return "front/forgetpwd/choose_way";
	}
	
	/**
	 * 忘记密码(找回密码验证. )!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/forgetPwd/verifyWay.html", method = RequestMethod.GET)
	public String toGetRetrieveModeMq() {
		return "front/forgetpwd/verify_way";
	}
	
	/**
	 * 忘记密码(重置登录密码.)!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/forgetPwd/resetPwd.html", method = RequestMethod.GET)
	public String toRetrieveResetPwd() {
		return "front/forgetpwd/reset_pwd";
	}
	
	/**
	 * 跳转维护页面.
	 */
	@RequestMapping(value = "/gameBet/maintain.html", method = RequestMethod.GET)
	public String toMatain() {
		return "front/maintain/matain";
	}
	
	/**
	 * 跳转404页面.	
	 */
	@RequestMapping(value = "/404.html", method = RequestMethod.GET)
	public String toMatain404() {
		return "front/maintain/404";
	}
	
	
	
	
	
	//--------------------------以下页面需要登录才能访问-----------------------------//
	
	
	/**
	 * 我的(跳转个人主页).
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/my.html", method = RequestMethod.GET)
	public String toUserInfoSave() {
		return "front/my/my";
	}
	
	/**
	 * 个人中心设置!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/setting.html", method = RequestMethod.GET)
	public String toUserSetting() {
		return "front/setting/setting";
	}
	
	/**
	 * 成长详情!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/userLevel.html", method = RequestMethod.GET)
	public String toUserLevel() {
		return "front/my/user_level";
	}
	
	/**
	 * 设置-奖金详情!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/awardDetail.html", method = RequestMethod.GET)
	public String toAwardDetail() {
		return "front/my/award_detail";
	}
	
	/**
	 * 监控查看!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/monitor.html", method = RequestMethod.GET)
	public String toMonitor() {
		return "front/monitor/monitor";
	}
	
	/**
	 * 个人投注记录!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/order.html", method = RequestMethod.GET)
	public String toOrder() {
		return "front/order/order";
	}
	
	/**
	 * 个人投注记录明细!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/orderDetail.html", method = RequestMethod.GET)
	public String toOrderDetail() {
		return "front/order/order_detail";
	}
	
	/**
	 * 充值列表!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/rechargeList.html", method = RequestMethod.GET)
	public String toRechargeList() {
		return "front/recharge/recharge_list";
	}
	
	/**
	 * 充值!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/recharge.html", method = RequestMethod.GET)
	public String toRecharge() {
		return "front/recharge/recharge";
	}
	
	/**
	 * 充值记录!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/rechargeOrder.html", method = RequestMethod.GET)
	public String toRechargeOrder() {
		return "front/recharge/recharge_order";
	}
	
	/**
	 * 提现!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/withdraw.html", method = RequestMethod.GET)
	public String toWithdraw() {
		return "front/withdraw/withdraw";
	}
	
	/**
	 * 提现记录!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/withdrawOrder.html", method = RequestMethod.GET)
	public String toWithdrawOrder() {
		return "front/withdraw/withdraw_order";
	}
	
	/**
	 * 资金明细!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/moneyDetail.html", method = RequestMethod.GET)
	public String toMoneyDetail() {
		return "front/moneydetail/money_detail";
	}
	
	/**
	 * 站内信息(跳转到消息页面).
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/message.html", method = RequestMethod.GET)
	public String toGetMessage() {
		return "front/message/message";
	}
	
	/**
	 * 站内信息详情(获取信息详情)!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/messageDetail.html", method = RequestMethod.GET)
	public String toGetMessageDetail() {
		return "front/message/message_detail";
	}
	
	/**
	 * 公告!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/announce.html", method = RequestMethod.GET)
	public String toGetAnnounce() {
		return "front/announce/announce";
	}
	
	/**
	 * 公告详情!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/announceDetail.html", method = RequestMethod.GET)
	public String toGetAnnounceDetail() {
		return "front/announce/announce_detail";
	}
	
	/**
	 * 修改登录密码!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/safeCenter/resetPwd.html", method = RequestMethod.GET)
	public String toChangeUserLoginPassword() {
		return "front/safecenter/resetpwd";
	}
	
	/**
	 * 设置用户安全密码!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/safeCenter/setSafePwd.html", method = RequestMethod.GET)
	public String toSetUserSafePassword() {
		return "front/safecenter/set_safe_pwd";
	}
	
	/**
	 * 修改用户安全密码!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/safeCenter/resetSafePwd.html", method = RequestMethod.GET)
	public String toChangeUserSafePassword() {
		return "front/safecenter/reset_safe_pwd";
	}
	
	/**
	 * 绑定邮箱!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/safeCenter/bindEmail.html", method = RequestMethod.GET)
	public String toBandEmail() {
		return "front/safecenter/bind_email";
	}
	
	/**
	 * 修改绑定邮箱!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/safeCenter/bindEmailChange.html", method = RequestMethod.GET)
	public String toBandEmailChange() {
		return "front/safecenter/bind_email_change";
	}
	
	/**
	 * 绑定手机号!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/safeCenter/bindPhone.html", method = RequestMethod.GET)
	public String toChangeUserBandPhone() {
		return "front/safecenter/bind_phone";
	}
	
	/**
	 * 修改用户的手机号码!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/safeCenter/bindPhoneChange.html", method = RequestMethod.GET)
	public String toChangeUserBandPhoneChange() {
		return "front/safecenter/bind_phone_change";
	}
	
	/**
	 * 设置安全问题(如果用户没有设置安全问题,就设置安全问题).
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/safeCenter/setSafeQuestion.html", method = RequestMethod.GET)
	public String toSetUserInfoSafetyQuestion() {
		return "front/safequestion/set_safe_question";
	}
	
	/**
	 * 校验安全问题(回答用户安全问题)!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/safeCenter/verifySafeQuestion.html", method = RequestMethod.GET)
	public String toCheckUserInfoQuestion() {
		return "front/safequestion/verify_safe_question";
	}
	
	/**
	 * 重新设置安全问题(回答用户安全问题成功后的下一个页面)!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/safeCenter/resetSafeQuestion.html", method = RequestMethod.GET)
	public String toCheckUserInfoQuestionNextPage() {
		return "front/safequestion/reset_safe_question";
	}
	
	/**
	 * 银行卡列表(银行卡管理)!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/bankCard/bankCardList.html", method = RequestMethod.GET)
	public String toUserBank() {
		return "front/bankcard/bank_card_list";
	}
	
	/**
	 * 新增银行卡!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/bankCard/addBankCard.html", method = RequestMethod.GET)
	public String toAddUserBank() {
		return "front/bankcard/add_bank_card";
	}
	
	/**
	 * 下级开户
	 * @return
	 */
	@RequestMapping(value = "/gameBet/agentCenter/addUser.html", method = RequestMethod.GET)
	public String toAddUser() {
		return "front/user/add_user";
	}
	
	/**
	 * 代理中心.
	 * @return
	 */
	@RequestMapping(value = "/gameBet/agentCenter.html", method = RequestMethod.GET)
	public String toAgentCenter() {
		return "front/agentcenter/agent_center";
	}
	
	
	/**
	 * 团队列表.
	 * @return
	 */
	@RequestMapping(value = "/gameBet/agentCenter/userList.html", method = RequestMethod.GET)
	public String toUserList() {
		return "front/user/user_list";
	}
	
	/**
	 * 生成推广.
	 * @return
	 */
	@RequestMapping(value = "/gameBet/agentCenter/addExtend.html", method = RequestMethod.GET)
	public String toAddExtend() {
		return "front/user/add_extend";
	}
	
	/**
	 * 推广管理.
	 * @return
	 */
	@RequestMapping(value = "/gameBet/agentCenter/extendMgr.html", method = RequestMethod.GET)
	public String toExtendMgr() {
		return "front/user/extend_mgr";
	}
	
	/**
	 * 下级投注记录!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/agentCenter/order.html", method = RequestMethod.GET)
	public String toAgentOrder() {
		return "front/order/agent_order";
	}
	
	/**
	 * 下级账变!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/agentCenter/moneyDetail.html", method = RequestMethod.GET)
	public String toAgentMoneyDetail() {
		return "front/moneydetail/agent_money_detail";
	}
	
	/**
	 * 下级充提记录!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/report/rechargeWithdrawOrder.html", method = RequestMethod.GET)
	public String toRechargeWithdrawOrder() {
		return "front/report/recharge_withdraw_order";
	}
	
	/**
	 * 盈亏报表!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/report/consumeReport.html", method = RequestMethod.GET)
	public String toConsumeReport() {
		return "front/report/consume_report";
	}
	
	/**
	 * 代理报表!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/report/agentReport.html", method = RequestMethod.GET)
	public String toAgentReport() {
		return "front/report/agent_report";
	}
	
	/**
	 * 充值跳转支付第三方post页面!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/pay/{payType}/post.html", method = {RequestMethod.POST,RequestMethod.GET})
	public String toSkipPay(@PathVariable String payType) {
		String returnPath = "/front/pay/" + payType + "/post";
		return returnPath;
	}
}
