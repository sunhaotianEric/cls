package com.team.lottery.filter;

import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.service.UserCookieLogService;
import com.team.lottery.service.UserService;
import com.team.lottery.system.BizSystemConfigManager;
import com.team.lottery.system.BizSystemConfigVO;
import com.team.lottery.system.OnlineUserManager;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.system.message.MessageQueueCenter;
import com.team.lottery.system.message.messagetype.LoginMessage;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.util.BaseDwrUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.vo.BizSystem;
import com.team.lottery.vo.BizSystemDomain;
import com.team.lottery.vo.Login;
import com.team.lottery.vo.LoginLog;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserCookieLog;

public class SessionUserFilter implements Filter {

	private static Logger log = LoggerFactory.getLogger(SessionUserFilter.class);
	// 不需要拦截的url放在set当中用set数据防止重复
	private static Set<String> allowUrl = new HashSet<String>();

	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		HttpSession session = req.getSession();

		String url = req.getServletPath();
		String scheme = request.getScheme();
		String serverName = req.getServerName();
		int port = req.getServerPort();
		String basePath = req.getContextPath();
		String fullUrl = scheme + "://" + serverName + ":" + port + req.getContextPath() + url;
		log.debug("filter url:{}" + fullUrl);

		BizSystemDomain domainInfo = ClsCacheManager.getDomainInfoByUrl(serverName);

		// 强制跳转https的判断
		if (domainInfo != null && domainInfo.getIsHttps() == 1 && !"https".equals(scheme)) {
			basePath = "https://" + serverName + basePath;

			// 进行跳转
			if (req.getQueryString() != null && !"".equals(req.getQueryString())) {
				res.sendRedirect(basePath + url + "?" + req.getQueryString());
			} else {
				res.sendRedirect(basePath + url);
			}
			return;
		}

		/*
		 * log.info("url:" + url); log.info("basePath1:" + basePath1);
		 * log.info("basePath:" + basePath); log.info("reqPort:" + reqPort);
		 */

		// 判断是否在限制的ip里面
		/*
		 * if(SystemFrontConfigInfo.denyIps.size() > 0) { Iterator<String>
		 * iterator = SystemFrontConfigInfo.denyIps.iterator();
		 * while(iterator.hasNext()){ String denyIp = iterator.next();
		 * if(serverName.equals(denyIp)) {
		 * log.info("使用禁止的ip输入["+denyIp+"],禁止访问"); return; } } }
		 * 
		 * //判断只能允许的域名访问 if(SystemFrontConfigInfo.allowDomains.size() > 0) {
		 * Iterator<String> iterator =
		 * SystemFrontConfigInfo.allowDomains.iterator(); boolean isAllow =
		 * false; while(iterator.hasNext()){ String denyIp = iterator.next();
		 * if(serverName.equals(denyIp)) { isAllow = true; } } if(!isAllow) {
		 * log.info("使用不被允许的域名["+serverName+"],禁止访问"); return; } }
		 */

		String bizSystem = ClsCacheManager.getBizSystemByDomainUrl(serverName);
		Boolean isTo404Page = false;
		if (StringUtils.isNotBlank(bizSystem)) {
			BizSystem bizSystemVo = ClsCacheManager.getBizSystemByCode(bizSystem);
			// 如果系统停止使用
			if (bizSystemVo.getEnable() == 0) {
				isTo404Page = true;
			}
		} else {
			// 获取到的字符串为空
			isTo404Page = true;
		}

		// 判断能允许访问的域名
		if (StringUtils.isNotBlank(SystemConfigConstant.allowUrls)) {
			String[] allowUrlStrs = SystemConfigConstant.allowUrls.split(",");
			for (String allowStrs : allowUrlStrs) {
				if (allowStrs.equals(serverName)) {
					isTo404Page = false;
				}
			}
		}

		if (isTo404Page) {
			// 若识别不到业务系统，则跳转404页面
			if (!url.contains("404.html")) {
				res.sendRedirect(basePath + "/404.html");
				return;
			}
		} else {
			// 网站维护判断
			BizSystemConfigVO bizSystemConfig = BizSystemConfigManager.getBizSystemConfig(bizSystem);
			// 系统维护中
			if (!url.contains("maintain.html") && bizSystemConfig != null && bizSystemConfig.getMaintainSwich() != null && bizSystemConfig.getMaintainSwich() == 1) {
				res.sendRedirect(basePath + "/gameBet/maintain.html");
				return;
			}
		}

		// 邀请码处理
		String invitationCode = request.getParameter("code");
		if (StringUtils.isNotBlank(invitationCode)) {
			String sessionInvitationCode = (String) session.getAttribute(ConstantUtil.INVITAION_CODE);
			// 之前session中没有设置过，则设置新的
			if (StringUtils.isBlank(sessionInvitationCode)) {
				session.setAttribute(ConstantUtil.INVITAION_CODE, invitationCode);
			} else {
				// 之前session有设置过,比较邀请码是不是新的，不一致则设置新的
				if (!invitationCode.equals(sessionInvitationCode)) {
					session.setAttribute(ConstantUtil.INVITAION_CODE, invitationCode);
				}
			}
		}

		// 手机端前台登录页面
		String indexPageUrl = basePath + "/gameBet/index.html";

		if (!isInAllowUrl(url)) {
			// 只要用户未登录 直接到首页
			if (session.getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER) == null) {
				res.sendRedirect(indexPageUrl);
				return;
			}
		} else {
			/**
			// 允许放过的页面，首页，登录页面，如果当前用户未登录，判断是否进行cookie自动登录
			if (session.getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER) == null && (url.contains("index.html") || url.contains("login.html"))) {
				Cookie[] cookies = req.getCookies(); // 获取cookie数组
				String token = "";
				String userName = "";
				String password = "";
				if (cookies != null) {
					for (Cookie cookie : cookies) {// 遍历cookie数组
						if ("userName".equals(cookie.getName())) {
							userName = cookie.getValue();
						} else if ("password".equals(cookie.getName())) {
							password = cookie.getValue();
						} else if ("token".equals(cookie.getName())) {
							token = cookie.getValue();
						}
					}
				}
				if (StringUtils.isNotBlank(token) && StringUtils.isNotBlank(userName) && StringUtils.isNotBlank(password)) {
					String domainUrl = req.getScheme() + "://" + serverName + ":" + req.getServerPort();
					String userIp = BaseDwrUtil.getRequestIp(req);
					UserCookieLogService userCookieLogService = ApplicationContextUtil.getBean(UserCookieLogService.class);
					UserCookieLog userCookieLog = userCookieLogService.queryByToken(token);
					// 启用，未失效，域名对应的上才进行处理  暂时去掉ip对应关系
					if (userCookieLog != null && userCookieLog.getEnable() == 1 && userCookieLog.getExpiredDate().after(new Date())
							//&& userCookieLog.getIp().equals(userIp)
							&& userCookieLog.getDomainUrl().equals(domainUrl) ) {
						UserService userService = ApplicationContextUtil.getBean(UserService.class);
						User loginUser = userService.selectByPrimaryKey(userCookieLog.getUserId());
						if (loginUser != null && loginUser.getLoginLock() != 1) {
							log.info("业务系统[{}],用户名[{}],登录域名[{}]进行cookie登录处理...", loginUser.getBizSystem(), loginUser.getUserName(), domainUrl);

							// 更新使用次数
							userCookieLog.setUseCount(userCookieLog.getUseCount() + 1);
							userCookieLog.setUpdateDate(new Date());
							userCookieLogService.updateByPrimaryKeySelective(userCookieLog);

							StringBuffer requestURL = req.getRequestURL();
							String tempContextUrl = requestURL.delete(requestURL.length() - req.getRequestURI().length(), requestURL.length()).append("/").toString();
							String loginInfo = tempContextUrl + "-----" + req.getHeaders("User-Agent").nextElement().toString();

							// 根据域名获取登录类型
							String loginType = ClsCacheManager.getLoginType(serverName);

							LoginLog loginLog = new LoginLog();
							loginLog.setEnabled(loginUser.getIsTourist() == 1 ? 0 : 1);
							loginLog.setUserName(loginUser.getUserName());
							loginLog.setUserId(loginUser.getId());
							loginLog.setBizSystem(loginUser.getBizSystem());
							loginLog.setLoginIp(userIp);
							loginLog.setLoginDomain(serverName);
							loginLog.setLoginPort(req.getServerPort());
							loginLog.setBrowser(loginInfo);
							loginLog.setLogType(loginType == null ? Login.LOGTYPE_MOBILE : loginType);
							loginUser.setLoginType(loginLog.getLogType());
							userService.loginDeal(loginUser, loginLog);

							// 登录成功设置session
							session.setAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER, loginUser);
							session.setAttribute(ConstantUtil.CURRENT_SYSTEM, loginUser.getBizSystem());
							// 设置1个小时失效！
							//session.setMaxInactiveInterval(3600);

							// 放置登录消息队列,异步获取登陆地区
							LoginMessage loginMessage = new LoginMessage();
							loginMessage.setIp(loginLog.getLoginIp());
							loginMessage.setLoginId(loginLog.getId());
							loginMessage.setUserName(loginUser.getUserName());
							loginMessage.setBizSystem(loginUser.getBizSystem());
							loginMessage.setLoginType(loginLog.getLogType());
							loginMessage.setSessionId(session.getId());
							MessageQueueCenter.putMessage(loginMessage);
							
							// 增加在线用户人数
							OnlineUserManager.addOnlineUser(loginLog.getLogType(), loginLog.getBizSystem(), loginLog.getUserName(), session.getId());
						}
					}
				}
			}
			**/
		}
		chain.doFilter(request, response);
	}

	/**
	 * url判断方法：foreach遍历set元素与传入的url进行判断如果有包含的话返回true; 如果遍历完了没有包含的话返回false!
	 * 
	 * @param action
	 * @return
	 */
	public boolean isInAllowUrl(String url) {
		for (String s : allowUrl) {
			if (url.contains(s)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 在初始化方法中,向set当中添加不需要拦截的url
	 *
	 *
	 */
	@Override
	public void init(FilterConfig arg0) throws ServletException {
		allowUrl.add("index.html");
		allowUrl.add("404.html");
		allowUrl.add("login.html");
		allowUrl.add("maintain.html");
		allowUrl.add("hall.html");
		allowUrl.add("activity.html");
		allowUrl.add("activityDetail.html");
		allowUrl.add("wininfo.html");
		allowUrl.add("lotterycode.html");
		allowUrl.add("lotterycodeDetail.html");
		allowUrl.add("reg.html");
		allowUrl.add("award_message.html");
		allowUrl.add("appdownload.html");
		allowUrl.add("lotterynumber.html");
		allowUrl.add("activity_detail.html");
		allowUrl.add("monitor.html");
		allowUrl.add("new_user_gift.html");
		allowUrl.add("message.html");
		allowUrl.add("message_detail.html");
		allowUrl.add("announce.html");
		allowUrl.add("announce_detail.html");
		allowUrl.add("forgetPwd.html");
		allowUrl.add("chooseWay.html");
		allowUrl.add("verifyWay.html");
		allowUrl.add("resetPwd.html");
		allowUrl.add("newUserGift.html");
		
		//支付通知
		allowUrl.add("returnurl.jsp");
		allowUrl.add("pageurl.jsp");
	}

}
