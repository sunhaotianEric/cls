package com.team.lottery.filter;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.team.lottery.util.ConstantUtil;

public class LoginInterceptor implements HandlerInterceptor {
	
	private static Logger logger = LoggerFactory.getLogger(LoginInterceptor.class);

	private static Set<String> allowUrl = new HashSet<String>();
	// 不需要拦截的url放在set当中用set数据防止重复
	static {
		
		allowUrl.add("/system/refreshCache");
		allowUrl.add("/system/reSubscribe");
		// 帮助页.
//		allowUrl.add("/socket/sendToAllUsers");
		allowUrl.add("/help/helpQuery");
		allowUrl.add("/system/getAppDownloadInfo");
		// 获取二维码.
		allowUrl.add("/tools/getQrCodePic");
		allowUrl.add("/tools/getRandomCodePic");
		// 用户登录页相关请求.
		allowUrl.add("/user/userMobileLogin");
		allowUrl.add("/user/userLogin");
		allowUrl.add("/user/userLogout");
		allowUrl.add("/user/guestUserPCLogin");

		// -------------------首页请求---------------//
		allowUrl.add("/announces/getIndexAnnounce");
		allowUrl.add("/activity/isShowNewUserGift");
		allowUrl.add("/lottery/getYestodayWin");
		allowUrl.add("/system/getBizSystemInfo");
		allowUrl.add("/code/getLastLotteryCode");
		allowUrl.add("/system/getUserCustomLotteryKind");
		allowUrl.add("/index/getHomeImage");
		allowUrl.add("/user/getCurrentUser");
		

		// -----------------登录页请求----------------//
		allowUrl.add("/user/getIsShowCheckCode");
		allowUrl.add("/system/getAppInfo");
		allowUrl.add("/system/getSiteConfig");

		// -----------------跳转彩票大厅请求--------------//
		allowUrl.add("/lotteryinfo/getAllLotteryKindExpectAndDiffTime");
		allowUrl.add("/lotteryinfo/getExpectAndDiffTime");
		allowUrl.add("/code/getAllLastLotteryCode");
		allowUrl.add("/code/getLastLotteryCode");

		// -----------------活动页面请求------------------//
		allowUrl.add("/activity/getAllActivity");

		// -----------------活动详情请求-----------------//
		allowUrl.add("/activity/getActivityById");
		allowUrl.add("/activity/dayConsumeActityApply");
		allowUrl.add("/activity/signinApply");

		// -----------------新手大礼包请求---------------//
		allowUrl.add("/activity/newUserGiftApply");
		allowUrl.add("/activity/getNewUserGiftStatus");

		// --------------发现-中奖信息页面请求------------//
		allowUrl.add("/index/getNearestWinLottery");

		// --------------发现-开奖号码页面请求------------//
		allowUrl.add("/code/getLotteryCodeList");
		allowUrl.add("/code/getShengXiaoNumber");

		// ------------发现-开奖号码详情页面请求-----------//
		allowUrl.add("/code/getNearestLotteryCode");

		// ----------------注册页面请求------------------//
		allowUrl.add("/user/userDuplicate");
		allowUrl.add("/user/sendSmsVerifycode");
		allowUrl.add("/usersafe/sendPhoneCode");
		allowUrl.add("/user/userRegister");
		allowUrl.add("/user/userMobileLogin");

		// --------------忘记密码页面相关请求-------------//
		allowUrl.add("/forgetpwd/forgetPwdNext");
		allowUrl.add("/forgetpwd/loadfindPwdWay");
		allowUrl.add("/forgetpwd/loadQuestions");
		allowUrl.add("/forgetpwd/verifyQuestion");
		allowUrl.add("/forgetpwd/verifyPhone");
		allowUrl.add("/forgetpwd/checkEmailCode");
		allowUrl.add("/forgetpwd/sendSmsVerifycode");
		allowUrl.add("/forgetpwd/sendEmailCode");
		allowUrl.add("/forgetpwd/resetPwd");

		allowUrl.add("/system/loadSystemRebate");
		
		//-----------------注册页面相关------------------//
		allowUrl.add("/register/regConfig");
		
		//-----------------彩种开关按钮------------------//
		allowUrl.add("/lotteryinfo/getLotteryStates");
		
		//------------------------pay-----------------------------//
		allowUrl.add("/deposit");
		
		//------------------------user-----------------------------//
		allowUrl.add("/user/guestUserMobileLogin");
		
		//-------------------------index----------------------------//
		allowUrl.add("/index/getYesterdayProfitRank");
		//------------------------index-----------------------------//
		allowUrl.add("/index/getYesterdayProfit");
		
		//------------------公告内容(公告接口,公告详情接口)-------------------//
		allowUrl.add("/announces/getAnnounceById");
		//------------------------获取最新的公告内容-----------------------------//
		allowUrl.add("/announces/getNewestAnnounce");
		
	}

	/**
	 * url判断方法：foreach遍历set元素与传入的url进行判断如果有包含的话返回true; 如果遍历完了没有包含的话返回false!
	 * 
	 * @param action
	 * @return
	 */
	public boolean isInAllowUrl(String url, Set<String> allowUrl) {
		for (String s : allowUrl) {
			if (url.contains(s)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 返回未登录的结果
	 * 
	 * @param objs
	 * @return
	 */
	protected Map<String, Object> createNoLoginRes() {
		Map<String, Object> resMap = new HashMap<String, Object>();
		resMap.put("code", "nologin");
		resMap.put("data", "未登录");
		return resMap;
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		if (request.getMethod().equals("GET") || request.getMethod().equals("OPTIONS")) {
			return true;
		} else if (request.getMethod().equals("POST")) {
			// 可以返回值的路由：获取二维码，帮助页面信息等
			String url = request.getServletPath();
			if(logger.isDebugEnabled()) {
				logger.debug("Method:[{}],请求url:{} ", request.getMethod(), url);
			}
			if (isInAllowUrl(url, allowUrl)) {
				return true;
			}
			// 处理controller用户未登录
			HttpSession session = request.getSession();
			if (session.getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER) == null) {
				response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin")); //解决跨域访问报错   
				response.setHeader("Access-Control-Allow-Credentials", "true"); //解决跨域访问报错 
				response.setHeader("Access-Control-Allow-Methods", "POST");
				response.setHeader("Access-Control-Allow-Headers", "x-requested-with,content-type");
				//响应的有效时间为 86400 秒，也就是 24 小时。在有效时间内，浏览器无须为同一请求再次发起预检请求
				response.setHeader("Access-Control-Max-Age", "86400");
				response.setHeader("Vary", "Origin");
				response.setHeader("Content-Type", "application/json;charset=UTF-8"); //返回没有Content-Type  
				PrintWriter out = response.getWriter();
				Map<String, Object> map = createNoLoginRes();
				out.print(JSONObject.fromObject(map));
				return false;
			}
		}
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView)
			throws Exception {

	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

	}
}
