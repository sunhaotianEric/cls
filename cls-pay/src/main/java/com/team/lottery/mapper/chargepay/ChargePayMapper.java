package com.team.lottery.mapper.chargepay;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.ChargePayVo;
import com.team.lottery.vo.ChargePay;
import com.team.lottery.vo.ThirdPayConfig;

public interface ChargePayMapper {
	int deleteByPrimaryKey(Long id);

	int insert(ChargePay record);

	int insertSelective(ChargePay record);

	ChargePay selectByPrimaryKey(Long id);

	int updateByPrimaryKeySelective(ChargePay record);

	int updateByPrimaryKey(ChargePay record);

	/**
	 * 查找对应的支付，并且锁定
	 * @param id
	 * @return
	 */
	public ChargePay selectByPrimaryKeyForUpdate(Long id);

	/**
	 * 查找对应的快捷支付可支付的
	 * @param bankType
	*/
	public List<ChargePay> getCanPayQuicksByType(@Param("chargeType") String chargeType);

	/**
	 * 将同类的快捷支付置为可用
	 * @param bankType
	 */
	public void updatePayQuicksUseByType(@Param("chargeType") String chargeType);

	/**
	 * 查找可快捷充值的支付平台
	 * @return
	 */
	public List<ChargePay> selectCanQuickRecharges();

	/**
	 * 查找所有的支付信息
	 * @return
	 */
	public List<ChargePay> getAllChargePays(@Param("query") ChargePayVo query);

	/**
	 * 根据第三方设置信息修改相关的快捷支付信息
	 * @return
	 */
	int updateByChargeType(ThirdPayConfig thirdPayConfig);

	/**
	 * 查找所有的支付信息
	 * @return
	 */
	public List<ChargePay> selectAllChargePays(@Param("query") ChargePayVo query);

	/**
	 * 根据chargtype查询支付信息
	 * @return
	 */
	public List<ChargePay> selectByChargeType(@Param("chargeType") String chargeType);

	/**
	 * 根据商户号和业务系统获取充值信息.
	 * @param merchNo
	 * @param bizSystem
	 * @return
	 */
	List<ChargePay> getChargePaysByMemberId(@Param("merchNo") String merchNo, @Param("bizSystem") String bizSystem);

}