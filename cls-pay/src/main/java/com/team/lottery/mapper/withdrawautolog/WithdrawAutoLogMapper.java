package com.team.lottery.mapper.withdrawautolog;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.team.lottery.vo.WithdrawAutoLog;

public interface WithdrawAutoLogMapper {
	int deleteByPrimaryKey(Long id);

	int insert(WithdrawAutoLog record);

	int insertSelective(WithdrawAutoLog record);

	WithdrawAutoLog selectByPrimaryKey(Long id);

	int updateByPrimaryKeySelective(WithdrawAutoLog record);

	int updateByPrimaryKey(WithdrawAutoLog record);

	/**
	 * 分页条件
	 * @param query
	 * @param startNum
	 * @param pageNum
	 * @return
	 */
	List<WithdrawAutoLog> getAllWithdrawAutoLog(@Param("query") WithdrawAutoLog query, @Param("startNum") Integer startNum,
			@Param("pageNum") Integer pageNum);

	/**
	 * 查询所有的
	 * @param query
	 * @return
	 */
	public int getWithdrawAutoLogCount(@Param("query") WithdrawAutoLog query);

	/**
	* 条件查询充值记录
	* @return
	*/
	public List<WithdrawAutoLog> getWithdrawAutoLogs(@Param("query") WithdrawAutoLog query);

	/**
	 * 查询今天未处理自动充值订单.
	 * @return
	 */
	List<WithdrawAutoLog> selectWithDrawAutoLogByDealStatus();
}