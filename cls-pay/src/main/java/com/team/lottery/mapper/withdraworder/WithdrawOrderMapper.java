package com.team.lottery.mapper.withdraworder;

import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.RechargeWithDrawOrderQuery;
import com.team.lottery.vo.WithdrawOrder;
import com.team.lottery.vo.WithdrawOrderCount;

public interface WithdrawOrderMapper {
	int deleteByPrimaryKey(Long id);

	int insert(WithdrawOrder record);

	int insertSelective(WithdrawOrder record);

	WithdrawOrder selectByPrimaryKey(Long id);

	int updateByPrimaryKeySelective(WithdrawOrder record);

	int updateByPrimaryKey(WithdrawOrder record);

	/**
	 * 查询符合条件的记录条数
	 * 
	 * @param query
	 * @return
	 */
	public Integer getWithDrawOrdersByQueryPageCount(@Param("query") RechargeWithDrawOrderQuery query);

	/**
	 * 分页条件查询下注记录
	 * 
	 * @return
	 */
	List<WithdrawOrder> getWithDrawOrdersByQueryPage(@Param("query") RechargeWithDrawOrderQuery query,
			@Param("startNum") Integer startNum, @Param("pageNum") Integer pageNum);

	/**
	 * 条件查询充值记录
	 * 
	 * @return
	 */
	public List<WithdrawOrder> getWithDrawOrders(@Param("query") RechargeWithDrawOrderQuery query);

	/**
	 * 总共的查询订单总值
	 * 
	 * @param query
	 * @return
	 */
	public WithdrawOrder getWithDrawOrdersByTotal(@Param("query") RechargeWithDrawOrderQuery query);

	/**
	 * 取现的申请总额，手续费总额
	 * 
	 * @return
	 */
	List<WithdrawOrder> getTotalValueSuccessOrderByType(@Param("query") RechargeWithDrawOrderQuery query);

	/**
	 * 按类型统计各充值类型、取现的人数
	 * 
	 * @param query
	 * @return
	 */
	List<WithdrawOrder> getUserCountSuccessOrderByType(@Param("query") RechargeWithDrawOrderQuery query);

	/**
	 * 统计充值、取现的人数
	 * 
	 * @param query
	 * @return
	 */
	List<WithdrawOrder> getUserCountSuccessOrderByOperateType(@Param("query") RechargeWithDrawOrderQuery query);

	/**
	 * 条件查询充值记录
	 * 
	 * @return
	 */
	public List<WithdrawOrder> getRechargeWithDrawOrders(@Param("query") RechargeWithDrawOrderQuery query);

	/**
	 * 查询最新的充值订单.
	 * 
	 * @param query
	 * @return
	 */
	public String getUptodateSerialNumber(RechargeWithDrawOrderQuery query);

	/**
	 * 查询用户当天的取现总额.
	 * 
	 * @param query
	 * @return
	 */
	public WithdrawOrderCount getUserApplyValueCountTodayWithDrawOrder(@Param("query")RechargeWithDrawOrderQuery query);
	
	/**
	 * 查询当前用户当天的提现次数.
	 * 
	 * @param query
	 * @return
	 */
	public Integer countUserWithdrawByDay(@Param("query")RechargeWithDrawOrderQuery query);
}