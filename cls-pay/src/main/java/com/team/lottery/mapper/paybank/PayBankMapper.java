package com.team.lottery.mapper.paybank;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.PayBankVo;
import com.team.lottery.extvo.RechargeConfigVo;
import com.team.lottery.vo.PayBank;

public interface PayBankMapper {
    int deleteByPrimaryKey(Long id);

    int insert(PayBank record);

    int insertSelective(PayBank record);

    PayBank selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(PayBank record);

    int updateByPrimaryKey(PayBank record);
    
    /**
     * 查找所有的收款银行数据
     * @return
     */
    public List<PayBank> getAllPayBanks(@Param("query")PayBankVo query);
    
    /**
     * 查找对应的银行可转账的银行
     * @param bankType
    */
    public List<PayBank> getCanPayBanksByType(String bankType);
    
    /**
     * 将同类的银行卡置为可用
     * @param bankType
     */
    public void updatePayBanksUseByType(String bankType);
  
    /**
     * 查找用户可使用的银行种类
     * @return
     */
    public List<PayBank> selectCanRechargeBanks(RechargeConfigVo query);
    
    /**
     * 随机获取一张可取现的银行卡
     * @return
     */
    public PayBank getPayBankForRandom();
    
    /**
     * 根据银行名称、姓名和卡号查找银行卡信息
     * @return
     */
    public PayBank getPayBankByTypeNameAndId(@Param("bankType")String bankType,@Param("bankUsername")String bankUsername, @Param("bandId")String bandId);
}