package com.team.lottery.mapper.quickbanktype;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.QuickBankType;

public interface QuickBankTypeMapper {
    int deleteByPrimaryKey(Long id);

    int insert(QuickBankType record);

    int insertSelective(QuickBankType record);

    QuickBankType selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(QuickBankType record);

    int updateByPrimaryKey(QuickBankType record);
    
    /**
     * 查询快捷充值的可支付银行
     * @return
     */
    public List<QuickBankType> getAllCanPayBanksByQuickRecharge();
    /**
     * 查询指定的银行代码记录
     * @return
     */
    QuickBankType selectByPrimaryKeyForUpdate(Long id);
    /**
     * 查询符合条件的记录条数
     * @param query
     * @return
     */
    public Integer getAllQuickBankTypePageCount(@Param("query")QuickBankType query);
    /**
     * 分页条件查询银行代码记录
     * @return
     */
    List<QuickBankType> getAllQuickBankTypePage(@Param("query")QuickBankType query,@Param("startNum")int startIndex, @Param("pageNum")Integer pageSize);

    /**
     * 查询符合条件的记录条数
     * @param query
     * @return
     */
    public List<QuickBankType> getQuickBankTypeByQuery(@Param("query")QuickBankType query);
}