package com.team.lottery.mapper.paynotifylog;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.PayNotifyLog;

public interface PayNotifyLogMapper {
    int deleteByPrimaryKey(Long id);

    int insert(PayNotifyLog record);

    int insertSelective(PayNotifyLog record);

    PayNotifyLog selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(PayNotifyLog record);

    int updateByPrimaryKey(PayNotifyLog record);
    
    /**
     * 查询符合条件的记录条数
     * @param query
     * @return
     */
    public Integer getAllPayNotifyLogByQueryPageCount(@Param("query")PayNotifyLog query);
        
    /**
     * 分页条件
     * @return
     */
    public  List<PayNotifyLog> getAllPayNotifyLogByQueryPage(@Param("query")PayNotifyLog query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
    
}