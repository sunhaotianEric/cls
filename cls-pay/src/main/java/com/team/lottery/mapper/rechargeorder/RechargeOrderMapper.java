package com.team.lottery.mapper.rechargeorder;

import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.RechargeWithDrawOrderQuery;
import com.team.lottery.vo.RechargeOrder;

public interface RechargeOrderMapper {
	int deleteByPrimaryKey(Long id);

	int insert(RechargeOrder record);

	int insertSelective(RechargeOrder record);

	RechargeOrder selectByPrimaryKey(Long id);

	int updateByPrimaryKeySelective(RechargeOrder record);

	int updateByPrimaryKey(RechargeOrder record);

	/**
	 * 分页查询下级查询条件总数.
	 * 
	 * @param query
	 *            查询参数.
	 * @return
	 */
	Integer getRechargeOrdersByQueryPageCount(@Param("query") RechargeWithDrawOrderQuery query);

	/**
	 * 根据条件查询充值订单集合.
	 * 
	 * @param query
	 *            查询参数.
	 * @param startIndex
	 *            开始页.
	 * @param pageSize
	 *            页面大小.
	 * @return
	 */
	List<RechargeOrder> getRechargeOrdersByQueryPage(@Param("query") RechargeWithDrawOrderQuery query, @Param("startNum") Integer startIndex, @Param("pageNum") Integer pageSize);

	/**
	 * 根据条件查询充值订单表.
	 * 
	 * @param query
	 *            查询参数.
	 * @return
	 */
	List<RechargeOrder> getRechargeOrders(@Param("query") RechargeWithDrawOrderQuery query);

	/**
	 * 查询订单总额.
	 * 
	 * @param query
	 * @return
	 */
	RechargeOrder getRechargeOrdersByTotal(@Param("query") RechargeWithDrawOrderQuery query);

	/**
	 * 根据充值订单查找充值记录.
	 * 
	 * @param serialNumber
	 * @return
	 */
	public List<RechargeOrder> getRechargeOrderBySerialNumber(String serialNumber);

	/**
	 * 查询最新的充值订单.
	 * 
	 * @param query
	 * @return
	 */
	public String getUptodateSerialNumber(RechargeWithDrawOrderQuery query);

	/**
	 * 查找业务系统某个用户的某段时间总冲值金额
	 * 
	 * @return
	 */
	public BigDecimal getBizSystemUserSumMoney(@Param("query") RechargeWithDrawOrderQuery query);

	/**
	 * 查询符合条件的记录条数
	 * 
	 * @param query
	 * @return
	 */
	public Integer getRechargeOrdersPageCount(@Param("query") RechargeWithDrawOrderQuery query);
	
	/**
	 * 按类型统计各充值类型申请总额，手续费总额
	 * @return
	 */
	List<RechargeOrder> getTotalValueSuccessOrderByType(@Param("query")RechargeWithDrawOrderQuery query);
	
	/**
	 * 按类型统计各充值类型、取现的人数
	 * @param query
	 * @return
	 */
	List<RechargeOrder> getUserCountSuccessOrderByType(@Param("query")RechargeWithDrawOrderQuery query);
	
	/**
	 * 按类型统计各充值类型、取现的人数
	 * @param query
	 * @return
	 */
	List<RechargeOrder> getUserCountSuccessOrderByTypeSpecial(@Param("query")RechargeWithDrawOrderQuery query);
	
	/**
	 * 统计充值、取现的人数
	 * @param query
	 * @return
	 */
	List<RechargeOrder> getUserCountSuccessOrderByOperateType(@Param("query")RechargeWithDrawOrderQuery query);
	
	/**
	 * 查询五分钟内的充值订单.
	 * @param rechargeOrderQuery
	 * @return
	 */
	List<RechargeOrder> getReChargeOrderInTenMinute(@Param("query")RechargeWithDrawOrderQuery query);
	
	/**
	 * 根据充值订单查找唯一的充值记录.
	 * 
	 * @param serialNumber
	 * @return
	 */
	public RechargeOrder getRechargeOrderByOderNumber(String serialNumber);


}