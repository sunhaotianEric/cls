package com.team.lottery.mapper.withdrawautoconfig;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.WithdrawAutoConfigQuery;
import com.team.lottery.vo.WithdrawAutoConfig;

public interface WithdrawAutoConfigMapper {
	int deleteByPrimaryKey(Integer id);

	int insert(WithdrawAutoConfig record);

	int insertSelective(WithdrawAutoConfig record);

	WithdrawAutoConfig selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(WithdrawAutoConfig record);

	int updateByPrimaryKey(WithdrawAutoConfig record);

	/**
	 * 查询总条数.
	 * 
	 * @param query
	 * @return
	 */
	Integer getWithdrawAutoConfigByQueryPageCount(@Param("query") WithdrawAutoConfigQuery query);

	/**
	 * 查询总的记录.
	 * 
	 * @param query
	 * @param startNum
	 * @param pageNum
	 * @return
	 */
	List<WithdrawAutoConfig> getWithdrawAutoConfigByQueryPage(@Param("query") WithdrawAutoConfigQuery query, @Param("startNum") Integer startNum, @Param("pageNum") Integer pageNum);


	/**
	 * 通过ID查询出对应的充值配置对象.
	 * 
	 * @param id
	 * @return
	 */
	WithdrawAutoConfig getWithdrawAutoConfigById(@Param("id") Integer id);


	/**
	 * 根据业务系统查询相关自动出款配置
	 * 
	 * @param query
	 * @param page
	 * @return
	 */
	WithdrawAutoConfig selectWithdrawAutoConfig(@Param("bizSystem") String bizSystem);
	
	/**
	 * 根据bizSystem查询出对应的充值配置对象
	 * @param bizSystem
	 * @return
	 */
	WithdrawAutoConfig getWithdrawAutoConfig(@Param("bizSystem") String bizSystem);

	/**
	 * 根据业务系统查询出对应的所有自动出款配置对象.并且状态为开启的对象.
	 * @param bizSystem
	 * @return
	 */
	WithdrawAutoConfig getWithdrawAutoConfigByBizSystem(String bizSystem);
}