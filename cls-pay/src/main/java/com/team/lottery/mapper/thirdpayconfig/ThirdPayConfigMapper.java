package com.team.lottery.mapper.thirdpayconfig;

import java.util.List;

import com.team.lottery.vo.ThirdPayConfig;

public interface ThirdPayConfigMapper {
    int insert(ThirdPayConfig record);

    int insertSelective(ThirdPayConfig record);
    
    int deleteByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ThirdPayConfig record);

    int updateByPrimaryKey(ThirdPayConfig record);
   
    public List<ThirdPayConfig>  getThirdPayConfig();
    
    public ThirdPayConfig selectByPrimaryKey(Long id);
    
    public List<ThirdPayConfig> queryThirdPayConfig(ThirdPayConfig record);

	ThirdPayConfig thirdPayConfigByChargeType(String chargeType);
}