package com.team.lottery.mapper.rechargeconfig;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.RechargeConfigVo;
import com.team.lottery.vo.RechargeConfig;

public interface RechargeConfigMapper {
    int deleteByPrimaryKey(Long id);

    int insert(RechargeConfig record);

    int insertSelective(RechargeConfig record);

    RechargeConfig selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(RechargeConfig record);

    int updateByPrimaryKey(RechargeConfig record);
    
    RechargeConfig selectByPrimaryKeyForUpdate(Long id);
    
    /**
     * 查找所有的配置信息
     * @return
     */
    public List<RechargeConfig> getAllRechargeConfig(RechargeConfigVo query);
    
    public Integer getAllRechargeConfigPageCount(@Param("query")RechargeConfig query);

	List<RechargeConfig> getAllRechargeConfigPage(@Param("query")RechargeConfig query,@Param("startNum")int startIndex, @Param("pageNum")Integer pageSize);
	
	/**
     * 查找所有的配置信息
     * @return
     */
    public List<RechargeConfig> getAllPayIdByChargePay(RechargeConfigVo query);
    
    /**
     * 查找相关的配置信息
     * @return
     */
    public List<RechargeConfig> selectRechargeConfig(RechargeConfigVo query);
    
    /**
     * 查找相关快捷方式的配置信息
     * @return
     */
    public List<RechargeConfig> selectRechargeConfigQuickPay(RechargeConfigVo query);

}