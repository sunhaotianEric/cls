package com.team.lottery.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.enums.EAlarmInfo;
import com.team.lottery.enums.EBankInfo;
import com.team.lottery.enums.EFundPayType;
import com.team.lottery.enums.EFundRechargeStatus;
import com.team.lottery.enums.EFundRefType;
import com.team.lottery.enums.EMoneyDetailType;
import com.team.lottery.enums.ENoteStatus;
import com.team.lottery.enums.ENoteType;
import com.team.lottery.enums.EUserBettingDemandStatus;
import com.team.lottery.exception.NormalBusinessException;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.RechargeWithDrawOrderQuery;
import com.team.lottery.extvo.UserTotalMoneyByTypeVo;
import com.team.lottery.mapper.rechargeorder.RechargeOrderMapper;
import com.team.lottery.system.BizSystemConfigManager;
import com.team.lottery.system.BizSystemConfigVO;
import com.team.lottery.system.message.MessageQueueCenter;
import com.team.lottery.system.message.messagetype.AlarmInfoMessage;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.MakeOrderNum;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.AlarmInfo;
import com.team.lottery.vo.LevelSystem;
import com.team.lottery.vo.MoneyDetail;
import com.team.lottery.vo.Notes;
import com.team.lottery.vo.PayBank;
import com.team.lottery.vo.RechargeConfig;
import com.team.lottery.vo.RechargeOrder;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserBettingDemand;
import com.team.lottery.vo.UserMoneyTotal;

/**
 * 后台充值订相模块Service相关方法.
 * 
 * @author Jamine
 *
 */
@Service("rechargeOrderService")
public class RechargeOrderService {
	private static Logger logger = LoggerFactory.getLogger(RechargeOrderService.class);
	@Autowired
	private RechargeOrderMapper rechargeOrderMapper;
	@Autowired
	private MoneyDetailService moneyDetailService;
	@Autowired
	private UserService userService;
	@Autowired
	private NotesService notesService;
	@Autowired
	private UserMoneyTotalService userMoneyTotalService;
	@Autowired
	private LevelSytemService levelSytemService;
	@Autowired
	private UserBettingDemandService userBettingDemandService;
	@Autowired
	private RechargeConfigService rechargeConfigService;
	@Autowired
	private PromotionRecordService promotionRecordService;

	/**
	 * 通过id查询订单信息.
	 * 
	 * @param id
	 *            订单的id号
	 * @return
	 */
	public RechargeOrder selectByPrimaryKey(Long id) {
		return rechargeOrderMapper.selectByPrimaryKey(id);
	}

	/**
	 * 更新订单信息.
	 * 
	 * @param record
	 * @return
	 */
	public int updateByPrimaryKeySelective(RechargeOrder record) {
		setMoneyScale(record);
		return rechargeOrderMapper.updateByPrimaryKeySelective(record);
	}

	/**
	 * 控制所有金额的精度
	 * 
	 * @param record
	 */
	public void setMoneyScale(RechargeOrder record) {
		if (record.getAccountValue() != null) {
			record.setAccountValue(
					record.getAccountValue().setScale(ConstantUtil.BIGDECIMAL_SCAL, BigDecimal.ROUND_HALF_UP));
		}
		if (record.getApplyValue() != null) {
			record.setApplyValue(
					record.getApplyValue().setScale(ConstantUtil.BIGDECIMAL_SCAL, BigDecimal.ROUND_HALF_UP));
		}
	}

	/**
	 * 根据条件查询所有的充值记录.
	 * 
	 * @param query
	 *            查询参数
	 * @param page
	 *            分页参数
	 * @return
	 */
	public Page getRechargeOrdersByQueryPage(RechargeWithDrawOrderQuery query, Page page) {

		page.setTotalRecNum(rechargeOrderMapper.getRechargeOrdersByQueryPageCount(query));
		page.setPageContent(
				rechargeOrderMapper.getRechargeOrdersByQueryPage(query, page.getStartIndex(), page.getPageSize()));
		return page;
	}

	/**
	 * 导出数据时用此方法.获取满足条件的充值订单.
	 * 
	 * @param query
	 * @return
	 */
	public List<RechargeOrder> getRechargeOrders(RechargeWithDrawOrderQuery query) {
		return rechargeOrderMapper.getRechargeOrders(query);

	}

	/**
	 * 查询对应的充值报表.
	 * 
	 * @param query
	 * @return
	 */
	public RechargeOrder getRechargeOrdersByTotal(RechargeWithDrawOrderQuery query) {
		return rechargeOrderMapper.getRechargeOrdersByTotal(query);
	}

	/**
	 * 
	 * 充值成功处理.
	 * 
	 * @param rechargeId
	 * @param rechargePresent
	 * @param bettingDemand
	 * @param rechargeGiftScale
	 * @return
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public RechargeOrder setRechargerOrdersPaySuccess(RechargeOrder rechargeOrder, String rechargePresent, String bettingDemand,
			String rechargeGiftScale,User orderUser,Admin admin) throws UnEqualVersionException, Exception {

		//RechargeOrder rechargeOrder = this.selectByPrimaryKey(rechargeId);
		/*if (rechargeOrder == null) {
			throw new Exception("该订单不存在.");
		}
		if (rechargeOrder.getDealStatus().equals(EFundRechargeStatus.PAYMENT_SUCCESS.name())) {
			throw new Exception("该订单已经支付成功,不能重新确认收账.");
		}*/
		/*
		 * if(!StringUtils.isEmpty(rechargeOrder.getThirdPayType())){ throw new
		 * Exception("订单不是转账类型的订单,不支持确认到账."); }
		 */
		rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT_SUCCESS.name());
//		String vipLevel = orderUser.getVipLevel();
		logger.info("业务系统[" + orderUser.getBizSystem() + "]用户名[" + orderUser.getUserName() + "],充值:"
				+ rechargeOrder.getApplyValue() + "元充值成功!充值订单流水号[" + rechargeOrder.getSerialNumber() + "]");
		// 操作人
		String operateUserName = "";
		if (admin != null) {
			operateUserName = admin.getUsername();
		}
		// 资金明细
		MoneyDetail moneyDetail = new MoneyDetail();
		moneyDetail.setUserId(orderUser.getId()); // 用户id
		moneyDetail.setOrderId(rechargeOrder.getId()); // 订单id
		moneyDetail.setOperateUserName(operateUserName); // 操作人员
		moneyDetail.setUserName(orderUser.getUserName()); // 用户名
		moneyDetail.setLotteryId(rechargeOrder.getSerialNumber()); // 订单单号
		moneyDetail.setExpect(""); // 哪个期号
		moneyDetail.setLotteryType(""); // 彩种
		moneyDetail.setLotteryTypeDes(""); // 彩种名称
		moneyDetail.setDetailType(EMoneyDetailType.RECHARGE.name()); // 资金类型为投注
		moneyDetail.setIncome(rechargeOrder.getApplyValue()); // 收入为0
		moneyDetail.setPay(new BigDecimal(0)); // 支出金额
		moneyDetail.setBalanceBefore(orderUser.getMoney()); // 支出前金额
		moneyDetail.setBalanceAfter(orderUser.getMoney().add(rechargeOrder.getApplyValue())); // 支出后的金额
		moneyDetail.setWinLevel(null);
		moneyDetail.setWinCost(null);
		moneyDetail.setWinCostRule(null);
		moneyDetail.setDescription("充值金额:" + rechargeOrder.getApplyValue());
		moneyDetail.setBizSystem(orderUser.getBizSystem());
		moneyDetailService.insertSelective(moneyDetail);

		List<UserTotalMoneyByTypeVo> userTotalMoneyByTypeVos = new ArrayList<UserTotalMoneyByTypeVo>();
		// 是否充值赠送
		BigDecimal presentValue = new BigDecimal(rechargePresent);
		MoneyDetail zsmoneyDetail = null;
		if (presentValue.compareTo(new BigDecimal("0")) > 0) {
			zsmoneyDetail = new MoneyDetail();
			zsmoneyDetail.setUserId(orderUser.getId()); // 用户id
			zsmoneyDetail.setOrderId(rechargeOrder.getId()); // 订单id
			zsmoneyDetail.setOperateUserName(operateUserName); // 操作人员
			zsmoneyDetail.setUserName(orderUser.getUserName()); // 用户名
			zsmoneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.ZS)); // 订单单号
			zsmoneyDetail.setExpect(""); // 哪个期号
			zsmoneyDetail.setLotteryType(""); // 彩种
			zsmoneyDetail.setLotteryTypeDes(""); // 彩种名称
			zsmoneyDetail.setDetailType(EMoneyDetailType.RECHARGE_PRESENT.name()); // 资金类型为投注
			zsmoneyDetail.setIncome(presentValue); // 收入为0
			zsmoneyDetail.setPay(new BigDecimal(0)); // 支出金额
			zsmoneyDetail.setBalanceBefore(orderUser.getMoney()); // 支出前金额
			zsmoneyDetail.setBalanceAfter(orderUser.getMoney().add(presentValue)); // 支出后的金额
			zsmoneyDetail.setWinLevel(null);
			zsmoneyDetail.setWinCost(null);
			zsmoneyDetail.setWinCostRule(null);
			zsmoneyDetail.setDescription("充值赠送金额:" + presentValue);
			zsmoneyDetail.setBizSystem(orderUser.getBizSystem());
			moneyDetailService.insertSelective(zsmoneyDetail);
			// 充值赠送增加用户余额
			orderUser.addMoney(presentValue, false);
			// 针对盈亏报表多线程不会事物回滚处理方式
			UserTotalMoneyByTypeVo userTotalMoneyByTypeVo = new UserTotalMoneyByTypeVo(orderUser,
					EMoneyDetailType.RECHARGE_PRESENT, presentValue);
			userTotalMoneyByTypeVos.add(userTotalMoneyByTypeVo);
			// userMoneyTotalService.addUserTotalMoneyByType(orderUser,
			// EMoneyDetailType.RECHARGE_PRESENT, presentValue);
		}

		// 打码要求处理
		BigDecimal addBettingMoney = new BigDecimal(bettingDemand);
		if (addBettingMoney.compareTo(BigDecimal.ZERO) > 0) {
			UserBettingDemand userBettingDemand = userBettingDemandService.getUserGoingBettingDemand(orderUser.getId());
			// 有没有完成的打码，则继续增加打码要求
			if (userBettingDemand != null) {
				BigDecimal newRequiredBettingMoney = userBettingDemand.getRequiredBettingMoney().add(addBettingMoney);
				logger.info("发现业务系统[{}]用户[{}]还有未完成的打码要求,当前打码要求[{}],累加打码量[{}]，累加后的要求打码量[{}]", orderUser.getBizSystem(),
						orderUser.getUserName(), userBettingDemand.getRequiredBettingMoney(), bettingDemand,
						newRequiredBettingMoney);
				userBettingDemand.setRequiredBettingMoney(newRequiredBettingMoney);
				userBettingDemand.setUpdateDate(new Date());
				userBettingDemandService.updateByPrimaryKeySelective(userBettingDemand);
			} else {
				logger.info("新增业务系统[{}]用户[{}]，打码要求[{}]", orderUser.getBizSystem(), orderUser.getUserName(),
						addBettingMoney);
				userBettingDemand = new UserBettingDemand();
				userBettingDemand.setBizSystem(orderUser.getBizSystem());
				userBettingDemand.setUserId(orderUser.getId());
				userBettingDemand.setUserName(orderUser.getUserName());
				userBettingDemand.setRequiredBettingMoney(addBettingMoney);
				userBettingDemand.setDoneStatus(EUserBettingDemandStatus.GOING.getCode());
				userBettingDemand.setCreateDate(new Date());
				userBettingDemandService.insertSelective(userBettingDemand);
			}
		}

		// 成功网银汇款充值系统消息
		Notes notes = new Notes();
		notes.setParentId(0l);
		notes.setEnabled(1);
		notes.setFromUserName(ConstantUtil.SYSTEM_OPERATE_NAME);
		notes.setToUserName(orderUser.getUserName());
		notes.setSub(rechargeOrder.getOperateDes());
		if (zsmoneyDetail != null) {
			notes.setBody("您成功充值" + rechargeOrder.getApplyValue().setScale(2, BigDecimal.ROUND_HALF_UP) + ","
					+ zsmoneyDetail.getDescription() + " ,充值方式:" + rechargeOrder.getOperateDes() + "。");
		} else {
			notes.setBody("您成功充值" + rechargeOrder.getApplyValue().setScale(2, BigDecimal.ROUND_HALF_UP) + " ,充值方式:"
					+ rechargeOrder.getOperateDes() + "。");
		}
		notes.setType(ENoteType.SYSTEM.name());
		notes.setStatus(ENoteStatus.NO_READ.name());
		notes.setCreatedDate(new Date());
		notes.setToUserId(orderUser.getId());
		notes.setBizSystem(orderUser.getBizSystem());
		notesService.insertSelective(notes);

		// 增加用户充值总额
		UserMoneyTotal userMoneyTotal = userMoneyTotalService.selectByUserId(orderUser.getId());
		EFundPayType payType = EFundPayType.valueOf(rechargeOrder.getPayType());
		if (EFundPayType.BANK_TRANSFER.equals(payType)) {
			userMoneyTotal.setTotalBankTransferRecharge(
					userMoneyTotal.getTotalBankTransferRecharge().add(rechargeOrder.getApplyValue()));
		} else if (EFundPayType.QUICK_PAY.equals(payType)) {
			userMoneyTotal.setTotalQuickPayRecharge(
					userMoneyTotal.getTotalQuickPayRecharge().add(rechargeOrder.getApplyValue()));
		} else if (EFundPayType.ALIPAY.equals(payType)) {
			userMoneyTotal
					.setTotalAlipayRecharge(userMoneyTotal.getTotalAlipayRecharge().add(rechargeOrder.getApplyValue()));
		} else if (EFundPayType.WEIXIN.equals(payType)) {
			userMoneyTotal
					.setTotalWeixinRecharge(userMoneyTotal.getTotalWeixinRecharge().add(rechargeOrder.getApplyValue()));
		}
		userMoneyTotal.setTotalRecharge(userMoneyTotal.getTotalRecharge().add(rechargeOrder.getApplyValue()));
		userMoneyTotalService.updateByPrimaryKeyWithVersionSelective(userMoneyTotal);

		// 网银转账充值增加用户余额
		orderUser.addMoney(rechargeOrder.getApplyValue(), false);
		if (orderUser.getFirstRechargeTime() == null) {
			orderUser.setFirstRechargeTime(new Date());
			orderUser.setFirstRechargeOrderId(rechargeOrder.getId());
		}
		BizSystemConfigVO systemConfigVO = BizSystemConfigManager.getBizSystemConfig(orderUser.getBizSystem());
		if (systemConfigVO.getIsOpenLevelAward() == 1) {
			// 用户增加积分!增加赠送金额!修改用户星!修改用户头衔
			BigDecimal addPoint = new BigDecimal(rechargeOrder.getApplyValue().intValue());
			BigDecimal point = orderUser.getPoint().add(addPoint);
			
			List<LevelSystem> levelSystemList = levelSytemService.selectDifferenceLevel(orderUser.getPoint().intValue(), point.intValue(), orderUser.getBizSystem());
			if (levelSystemList != null && levelSystemList.size() > 0) {

//				// 当用户当前等级与用户积分最高等级相等不执行
//				if (levelSystem.getLevel() != Integer.valueOf(orderUser.getVipLevel())) {
//					addPromotionAward = levelSytemService.selectLevelBetween(
//							Integer.valueOf(orderUser.getVipLevel()) + 1, levelSystem.getLevel(),
//							orderUser.getBizSystem());
//					for (LevelSystem levelSystems : levelSystemList) {
//						addPromotionAward = addPromotionAward.add(levelSystems.getPromotionAward());
//					}
					
//					if (addPromotionAward == null) {
//						addPromotionAward = BigDecimal.ZERO;
//					}
//				}
				// 晋级记录
				promotionRecordService.openPromotionRecord(userTotalMoneyByTypeVos, levelSystemList, systemConfigVO, orderUser);
			}
			orderUser.setPoint(point);
		}
		userService.updateByPrimaryKeyWithVersionSelective(orderUser);

		if (presentValue.compareTo(new BigDecimal("0")) > 0) {
			rechargeOrder.setAccountValue(rechargeOrder.getApplyValue().add(presentValue));
		} else {
			rechargeOrder.setAccountValue(rechargeOrder.getApplyValue());
		}
		//Admin admin = BaseDwrUtil.getCurrentAdmin();
		rechargeOrder.setUpdateAdmin(admin.getUsername());
		rechargeOrder.setUpdateDate(new Date());

		BigDecimal rechargeGiftScaleValue = new BigDecimal(rechargeGiftScale);
		if (rechargeOrder.getRechargeGiftScale() != null) {
			if (rechargeOrder.getRechargeGiftScale().compareTo(rechargeGiftScaleValue) != 0) {
				rechargeOrder.setRechargeGiftScale(rechargeGiftScaleValue);
			}
		}

		if (rechargeOrder.getRechargeGiftValue().compareTo(presentValue) != 0) {
			rechargeOrder.setRechargeGiftValue(presentValue);
		}
		this.updateByPrimaryKeySelective(rechargeOrder);
		// 循环遍历插入报表总额和盈亏报表
		for (UserTotalMoneyByTypeVo userTotalMoneyByTypeVo : userTotalMoneyByTypeVos) {
			userMoneyTotalService.addUserTotalMoneyByType(userTotalMoneyByTypeVo.getUser(),
					userTotalMoneyByTypeVo.getDetailType(), userTotalMoneyByTypeVo.getOperationValue());
		}
		// 统计实时团队和自身盈亏报表
		userMoneyTotalService.excuteUserDayConsume(orderUser, EMoneyDetailType.RECHARGE.name(),
				rechargeOrder.getApplyValue());
		userMoneyTotalService.excuteUserSelfDayConsume(orderUser, EMoneyDetailType.RECHARGE.name(),
				rechargeOrder.getApplyValue());
		return rechargeOrder;
	}

	/**
	 * 管理员取消订单.
	 * 
	 * @param rechargeId
	 *            需要取消订单的ID.
	 * @return
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public RechargeOrder setRechargerOrdersPayClose(RechargeOrder rechargeOrder,Admin admin) throws Exception {
		rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT_CLOSE.name());
		//Admin admin = BaseDwrUtil.getCurrentAdmin();
		rechargeOrder.setUpdateAdmin(admin.getUsername());
		rechargeOrder.setUpdateDate(new Date());
		logger.info("绠＄悊鍛榌" + admin.getUsername() + "]鍏抽棴璁㈠崟娴佹按鍙穂" + rechargeOrder.getSerialNumber() + "]鐨勫厖鍊艰鍗�");
		this.updateByPrimaryKeySelective(rechargeOrder);
		return rechargeOrder;
	}

	/**
	 * 根据充值订单号进行打码要求
	 * 
	 * @param rechargeId
	 *            对应的订单号.
	 * @param bettingDemand
	 *            打码金额.
	 */
	public void setUserBettingDemand(Long rechargeId, String bettingDemand) throws Exception {
		RechargeOrder rechargeOrder = this.selectByPrimaryKey(rechargeId);
		if (rechargeOrder == null) {
			throw new Exception("该订单不存在.");
		}
		User orderUser = userService.selectByPrimaryKey(rechargeOrder.getUserId());
		if (orderUser == null) {
			throw new Exception("当前用户不存在.");
		}
		BigDecimal addBettingMoney = new BigDecimal(bettingDemand);
		if (addBettingMoney.compareTo(BigDecimal.ZERO) > 0) {
			UserBettingDemand userBettingDemand = userBettingDemandService.getUserGoingBettingDemand(orderUser.getId());
			// 有没有完成的打码，则继续增加打码要求
			if (userBettingDemand != null) {
				BigDecimal newRequiredBettingMoney = userBettingDemand.getRequiredBettingMoney().add(addBettingMoney);
				logger.info("发现业务系统[{}]用户[{}]还有未完成的打码要求,当前打码要求[{}],累加打码量[{}]，累加后的要求打码量[{}]", orderUser.getBizSystem(),
						orderUser.getUserName(), userBettingDemand.getRequiredBettingMoney(), bettingDemand,
						newRequiredBettingMoney);
				userBettingDemand.setRequiredBettingMoney(newRequiredBettingMoney);
				userBettingDemand.setUpdateDate(new Date());
				userBettingDemandService.updateByPrimaryKeySelective(userBettingDemand);
			} else {
				logger.info("新增业务系统[{}]用户[{}]，打码要求[{}]", orderUser.getBizSystem(), orderUser.getUserName(),
						addBettingMoney);
				userBettingDemand = new UserBettingDemand();
				userBettingDemand.setBizSystem(orderUser.getBizSystem());
				userBettingDemand.setUserId(orderUser.getId());
				userBettingDemand.setUserName(orderUser.getUserName());
				userBettingDemand.setRequiredBettingMoney(addBettingMoney);
				userBettingDemand.setDoneStatus(EUserBettingDemandStatus.GOING.getCode());
				userBettingDemand.setCreateDate(new Date());
				userBettingDemandService.insertSelective(userBettingDemand);
			}
		}

	}

	/**
	 * 生成充值订单
	 * 
	 * @throws Exception
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public Map<String, Object> generateRechargeOrder(BigDecimal rechargeValue, String rechargeConfigId, PayBank payBank,
			String payNickName, User currentUser) throws Exception {
		Map<String, Object> resMap = new HashMap<String, Object>();
		Boolean isExistOrder = false;
		RechargeConfig rechargeConfig = rechargeConfigService.selectByPrimaryKey(Long.valueOf(rechargeConfigId));

		/* User currentUser = BaseDwrUtil.getCurrentUser(); */
		if (!currentUser.getBizSystem().equals(rechargeConfig.getBizSystem())) {
			throw new NormalBusinessException("当前登陆用户所属系统与当前的充值设置的所属系统不相同!不能生成订单.");
		}
		// 查询当前用户的5分钟内的,网银转账类型,支付宝转银行卡,微信转账,支付宝转支付宝,如果大于3条数据时候给用户提示提交订单台频繁,请稍后再试.
		// 判断当前充值的类型是什么,如果当前的充值类型是转账类型,就去查询五分钟内的转账类型的订单.
		String refType = rechargeConfig.getRefType();
		if (refType.equals(EFundRefType.TRANSFER.name())) {
			// 查询5分钟内的订单.
			// 新建订单查询对象.
			RechargeWithDrawOrderQuery rechargeOrderQuery = new RechargeWithDrawOrderQuery();
			// 设置用户ID.
			rechargeOrderQuery.setUserId(currentUser.getId());
			// 设置转账类型.
			rechargeOrderQuery.setRefType(EFundRefType.TRANSFER.name());
			// 设置支付状态为支付中的状态.
			rechargeOrderQuery.setStatuss(EFundRechargeStatus.PAYMENT.name());
			// 设置10分钟之内的查询时间.
			rechargeOrderQuery.setCreatedDateStart(new Date(new Date().getTime() - 10 * 60 * 1000));
			rechargeOrderQuery.setCreatedDateEnd(new Date());
			// 得到10分钟内的充值订单,并且是支付中的.
			List<RechargeOrder> orderListInTenMin = rechargeOrderMapper.getReChargeOrderInTenMinute(rechargeOrderQuery);
			if (orderListInTenMin.size() >= 3) {
				logger.info("系统[{}]中的[{}]用户,十分钟内充值未处理的订单超过3个", currentUser.getBizSystem(), currentUser.getUserName());
				throw new NormalBusinessException("订单提交操作太频繁,请稍后再试.");
			}
		}

		String serialNumber = MakeOrderNum.makeOrderNum(MakeOrderNum.CZ);
		RechargeOrder rechargeOrderNew = new RechargeOrder();
		rechargeOrderNew.setDealStatus(EFundRechargeStatus.PAYMENT.name());
		rechargeOrderNew.setBankName(payBank.getBankName());
		rechargeOrderNew.setSubbranchName(payBank.getSubbranchName());
		rechargeOrderNew.setBankAccountName(payBank.getBankUserName());
		rechargeOrderNew.setBankCardNum(payBank.getBankId());
		rechargeOrderNew.setBankType(payBank.getBankType());
		rechargeOrderNew.setFromType(currentUser.getLoginType());
		rechargeOrderNew.setRechargeConfigId(Long.valueOf(rechargeConfigId));
		rechargeOrderNew.setRechargeWayName(rechargeConfig.getShowName());
		rechargeOrderNew.setSerialNumber(serialNumber);
		// rechargeOrderNew.setPayType(rechargeOrder.getPayType());
		// rechargeOrderNew.setOperateDes(rechargeOrder.getOperateDes());
		rechargeOrderNew.setUserId(currentUser.getId());
		rechargeOrderNew.setUserName(currentUser.getUserName());
		rechargeOrderNew.setBizSystem(currentUser.getBizSystem());
		rechargeOrderNew.setChargePayId(null);
		rechargeOrderNew.setApplyValue(rechargeValue);
		rechargeOrderNew.setCreatedDate(new Date());
		rechargeOrderNew.setRefType(EFundRefType.TRANSFER.name());
		rechargeOrderNew.setAccountValue(BigDecimal.ZERO);
		// rechargeOrderNew.setThirdPayType(rechargeOrder.getThirdPayType());
		rechargeOrderNew.setPayBankId(payBank.getId());
		if (rechargeConfig.getPayType().equals(EFundPayType.ALIPAY.getCode())) {
			rechargeOrderNew.setPayType(rechargeConfig.getPayType());
			rechargeOrderNew.setOperateDes(
					EFundPayType.BANK_TRANSFER.getDescription() + "[" + EFundPayType.ALIPAY.getDescription() + "]");
		} else if (rechargeConfig.getPayType().equals(EFundPayType.WEIXIN.getCode())) {
			rechargeOrderNew.setPayType(rechargeConfig.getPayType());
			rechargeOrderNew.setOperateDes(
					EFundPayType.BANK_TRANSFER.getDescription() + "[" + EFundPayType.WEIXIN.getDescription() + "]");
		} else {
			rechargeOrderNew.setPayType(EFundPayType.BANK_TRANSFER.name());
			rechargeOrderNew
					.setOperateDes(EFundPayType.BANK_TRANSFER.getDescription() + "[" + payBank.getBankName() + "]");
		}
		// 微信充值特殊处理
		if (EBankInfo.WEIXIN.getCode().equals(rechargeConfig.getPayType())) {
			if (StringUtils.isEmpty(payNickName)) {
				throw new NormalBusinessException("请填写支付微信昵称");
			}
			if (payNickName.trim().length() > 30) {
				throw new NormalBusinessException("微信昵称长度非法！请输入少于30个字符");
			}
			rechargeOrderNew.setPostscript(payNickName.trim());
		} else if (EBankInfo.ALIPAY.getCode().equals(rechargeConfig.getPayType())) {
			if (StringUtils.isEmpty(payNickName)) {
				throw new NormalBusinessException("请填写支付宝姓名");
			}
			if (payNickName.trim().length() > 30) {
				throw new NormalBusinessException("支付宝姓名长度非法！请输入少于30个字符");
			}
			rechargeOrderNew.setPostscript(payNickName.trim());
		} else {
			if (StringUtils.isEmpty(payNickName)) {
				throw new NormalBusinessException("请填写转账人姓名");
			}
			if (payNickName.trim().length() > 30) {
				throw new NormalBusinessException("转账人姓名长度非法！请输入少于30个字符");
			}
			rechargeOrderNew.setPostscript(payNickName.trim());
		}

		// 当充值增送开关开启,并且当前充值金额大于充值增送最低金额,当前增送充值金额=当前充值金额*充值增送比例
		if (rechargeConfig.getRechargeGiftEnabled() == 1) {
			if (rechargeConfig.getRechargeGiftLowestValue() != null
					&& rechargeValue.compareTo(rechargeConfig.getRechargeGiftLowestValue()) >= 0) {
				if (rechargeConfig.getRechargeGiftScale() != null) {
					rechargeOrderNew.setRechargeGiftScale(rechargeConfig.getRechargeGiftScale());
					rechargeOrderNew
							.setRechargeGiftValue(rechargeValue.multiply(rechargeConfig.getRechargeGiftScale()));
				}
			}
		}
		rechargeOrderNew.setThirdPayTypeDesc(rechargeConfig.getChargeDes());
		// 插入充值表
		rechargeOrderMapper.insertSelective(rechargeOrderNew);

		/*
		 * payBankResult.setYetUse(1);
		 * payBankService.updateByPrimaryKeySelective(payBankResult);
		 */

		/* rechargeWithDrawOrder = rechargeOrder; */
		resMap.put("isExistOrder", isExistOrder);
		resMap.put("rechargeWithDrawOrder", rechargeOrderNew);
		return resMap;
	}

	public int insertSelective(RechargeOrder record) {
		setMoneyScale(record);
		return rechargeOrderMapper.insertSelective(record);
	}

	/**
	 * 根据充值订单查找充值记录
	 * 
	 * @deprecated
	 * @param serialNumber
	 * @return
	 */
	public List<RechargeOrder> getRechargeOrderBySerialNumber(String serialNumber) {
		return rechargeOrderMapper.getRechargeOrderBySerialNumber(serialNumber);
	}

	/**
	 * 第三方充值成功处理
	 * 
	 * @throws Exception
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public synchronized boolean rechargeOrderSuccessDeal(String serialNumber, BigDecimal money)
			throws UnEqualVersionException, Exception {
		logger.info("充值订单号[" + serialNumber + "],实际支付金额[" + money + "],进行充值成功处理...");
		List<UserTotalMoneyByTypeVo> userTotalMoneyByTypeVos = new ArrayList<UserTotalMoneyByTypeVo>();
		List<RechargeOrder> rechargeWithDrawOrders = this.getRechargeOrderBySerialNumber(serialNumber);
		if (rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1) {
			logger.error("根据订单号[" + serialNumber + "]查找充值订单记录为空或者有两笔相同订单号订单");
			throw new Exception("处理订单[" + serialNumber + "]发生错误");
		}
		RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
		if (rechargeWithDrawOrder == null) {
			logger.error("根据订单号[" + serialNumber + "]查找充值订单记录为空");
			throw new Exception("处理订单[" + serialNumber + "]发生错误");
		}
		// 第三方入账处理比较金额，若通知入账金额大于订单金额，则入账失败
		if (rechargeWithDrawOrder.getApplyValue().compareTo(money) < 0) {
			logger.error(
					"发现充值订单的金额[" + rechargeWithDrawOrder.getApplyValue() + "]与第三方通知入账金额[" + money + "]不一致,不进行入账处理");
			return false;
		}
		if (rechargeWithDrawOrder.getDealStatus().equals(EFundRechargeStatus.PAYMENT.name())) { // 只有出于支付中的订单,才能给用户充值
			rechargeWithDrawOrder.setDealStatus(EFundRechargeStatus.PAYMENT_SUCCESS.name());
			// 到账金额取实际支付金额
			rechargeWithDrawOrder.setAccountValue(money);
			rechargeWithDrawOrder.setUpdateDate(new Date());
			this.updateByPrimaryKeySelective(rechargeWithDrawOrder);
			logger.info("当前订单状态变为已支付，订单号[" + rechargeWithDrawOrder.getSerialNumber() + "]，用户名称[ "
					+ rechargeWithDrawOrder.getUserName() + "],用户ID号[" + rechargeWithDrawOrder.getUserId() + "],业务系统["
					+ rechargeWithDrawOrder.getBizSystem() + "],进行用户金额充值处理...");

			Integer userId = rechargeWithDrawOrder.getUserId();
			User orderUser = userService.selectByPrimaryKey(userId);
//			String vipLevel = orderUser.getVipLevel();
			MoneyDetail orderUserMoneyDetail = new MoneyDetail();
			orderUserMoneyDetail.setUserId(orderUser.getId()); // 用户id
			orderUserMoneyDetail.setOrderId(null); // 订单id
			orderUserMoneyDetail.setUserName(orderUser.getUserName()); // 用户名
			orderUserMoneyDetail.setOperateUserName(ConstantUtil.SYSTEM_OPERATE_NAME); // 操作人员
			orderUserMoneyDetail.setLotteryId(rechargeWithDrawOrder.getSerialNumber()); // 取冲值订单单号
			orderUserMoneyDetail.setWinLevel(null);
			orderUserMoneyDetail.setWinCost(null);
			orderUserMoneyDetail.setWinCostRule(null);
			orderUserMoneyDetail.setDetailType(EMoneyDetailType.RECHARGE.name());
			orderUserMoneyDetail.setIncome(rechargeWithDrawOrder.getAccountValue());
			orderUserMoneyDetail.setPay(new BigDecimal(0));
			orderUserMoneyDetail.setDescription("在线充值:" + rechargeWithDrawOrder.getAccountValue());
			orderUserMoneyDetail.setBalanceBefore(orderUser.getMoney()); // 支出前金额
			orderUserMoneyDetail.setBalanceAfter(orderUser.getMoney().add(rechargeWithDrawOrder.getAccountValue())); // 支出后的金额
			orderUserMoneyDetail.setBizSystem(orderUser.getBizSystem());
			moneyDetailService.insertSelective(orderUserMoneyDetail); // 保存资金明细

			// 充值成功增加用户余额
			orderUser.addMoney(rechargeWithDrawOrder.getAccountValue(), false);
			if (orderUser.getFirstRechargeTime() == null) {
				orderUser.setFirstRechargeTime(new Date());
				orderUser.setFirstRechargeOrderId(rechargeWithDrawOrder.getId());
			}
			BizSystemConfigVO systemConfigVO = BizSystemConfigManager.getBizSystemConfig(orderUser.getBizSystem());
			
			// 打码要求处理, 按参数配置的打码量倍数进行设置打码量
			if(systemConfigVO.getThirdPayOrderBettingMultiple() != null) {
				BigDecimal addBettingMoney = rechargeWithDrawOrder.getApplyValue().multiply(new BigDecimal(systemConfigVO.getThirdPayOrderBettingMultiple()));
				if (addBettingMoney.compareTo(BigDecimal.ZERO) > 0) {
					UserBettingDemand userBettingDemand = userBettingDemandService.getUserGoingBettingDemand(orderUser.getId());
					// 有没有完成的打码，则继续增加打码要求
					if (userBettingDemand != null) {
						BigDecimal newRequiredBettingMoney = userBettingDemand.getRequiredBettingMoney().add(addBettingMoney);
						logger.info("发现业务系统[{}]用户[{}]还有未完成的打码要求,当前打码要求[{}],累加打码量[{}]，累加后的要求打码量[{}]", orderUser.getBizSystem(),
								orderUser.getUserName(), userBettingDemand.getRequiredBettingMoney(), addBettingMoney,
								newRequiredBettingMoney);
						userBettingDemand.setRequiredBettingMoney(newRequiredBettingMoney);
						userBettingDemand.setUpdateDate(new Date());
						userBettingDemandService.updateByPrimaryKeySelective(userBettingDemand);
					} else {
						logger.info("新增业务系统[{}]用户[{}]，打码要求[{}]", orderUser.getBizSystem(), orderUser.getUserName(),
								addBettingMoney);
						userBettingDemand = new UserBettingDemand();
						userBettingDemand.setBizSystem(orderUser.getBizSystem());
						userBettingDemand.setUserId(orderUser.getId());
						userBettingDemand.setUserName(orderUser.getUserName());
						userBettingDemand.setRequiredBettingMoney(addBettingMoney);
						userBettingDemand.setDoneStatus(EUserBettingDemandStatus.GOING.getCode());
						userBettingDemand.setCreateDate(new Date());
						userBettingDemandService.insertSelective(userBettingDemand);
					}
				}
			}
			
			if (systemConfigVO.getIsOpenLevelAward() == 1) {
				// 用户增加积分!增加赠送金额!修改用户星!修改用户头衔
				BigDecimal addPoint = new BigDecimal(rechargeWithDrawOrder.getApplyValue().intValue());
				BigDecimal point = orderUser.getPoint().add(addPoint);
				
				List<LevelSystem> levelSystemList = levelSytemService.selectDifferenceLevel(orderUser.getPoint().intValue(),point.intValue(), orderUser.getBizSystem());
				if (levelSystemList != null && levelSystemList.size() > 0) {
//					LevelSystem levelSystem = levelSystemList.get(0);
//					orderUser.setVipLevel(String.valueOf(levelSystem.getLevel()));
//					orderUser.setLevelName(levelSystem.getLevelName());
					// 当用户当前等级与用户积分最高等级相等不执行
//					if (levelSystem.getLevel() != Integer.valueOf(orderUser.getVipLevel())) {
//							addPromotionAward = levelSytemService.selectLevelBetween(
//									Integer.valueOf(orderUser.getVipLevel()) + 1, levelSystem.getLevel(),
//									orderUser.getBizSystem());
//						for (LevelSystem levelSystems : levelSystemList) {
//							addPromotionAward = addPromotionAward.add(levelSystems.getPromotionAward());
//						}
//						if (addPromotionAward == null) {
//							addPromotionAward = BigDecimal.ZERO;
//						}
//					}

					// 晋级记录
					promotionRecordService.openPromotionRecord(userTotalMoneyByTypeVos, levelSystemList, systemConfigVO, orderUser);

				}
				orderUser.setPoint(point);
			}
			userService.updateByPrimaryKeyWithVersionSelective(orderUser);

			// 增加用户充值总额
			UserMoneyTotal userMoneyTotal = userMoneyTotalService.selectByUserId(orderUser.getId());
			EFundPayType payType = EFundPayType.valueOf(rechargeWithDrawOrder.getPayType());
			if (EFundPayType.BANK_TRANSFER.equals(payType)) {
				userMoneyTotal.setTotalBankTransferRecharge(
						userMoneyTotal.getTotalBankTransferRecharge().add(rechargeWithDrawOrder.getApplyValue()));
			} else if (EFundPayType.QUICK_PAY.equals(payType)) {
				userMoneyTotal.setTotalQuickPayRecharge(
						userMoneyTotal.getTotalQuickPayRecharge().add(rechargeWithDrawOrder.getApplyValue()));
			} else if (EFundPayType.ALIPAY.equals(payType)) {
				userMoneyTotal.setTotalAlipayRecharge(
						userMoneyTotal.getTotalAlipayRecharge().add(rechargeWithDrawOrder.getApplyValue()));
			} else if (EFundPayType.WEIXIN.equals(payType)) {
				userMoneyTotal.setTotalWeixinRecharge(
						userMoneyTotal.getTotalWeixinRecharge().add(rechargeWithDrawOrder.getApplyValue()));
			}
			userMoneyTotal
					.setTotalRecharge(userMoneyTotal.getTotalRecharge().add(rechargeWithDrawOrder.getApplyValue()));
			userMoneyTotalService.updateByPrimaryKeyWithVersionSelective(userMoneyTotal);

			// 快捷充值发送系统消息
			Notes notes = new Notes();
			notes.setParentId(0l);
			notes.setEnabled(1);
			notes.setFromUserName(ConstantUtil.SYSTEM_OPERATE_NAME);
			notes.setToUserName(orderUser.getUserName());
			notes.setToUserId(orderUser.getId());
			notes.setSub("[快捷充值]");
			notes.setBody("恭喜您,您在在快捷充值中,成功充值 " + rechargeWithDrawOrder.getAccountValue() + "。");
			notes.setType(ENoteType.SYSTEM.name());
			notes.setStatus(ENoteStatus.NO_READ.name());
			notes.setCreatedDate(new Date());
			notes.setBizSystem(orderUser.getBizSystem());
			notesService.insertSelective(notes);

			// 统计实时团队和自身盈亏报表
			userMoneyTotalService.excuteUserDayConsume(orderUser, EMoneyDetailType.RECHARGE.name(),
					rechargeWithDrawOrder.getApplyValue());
			userMoneyTotalService.excuteUserSelfDayConsume(orderUser, EMoneyDetailType.RECHARGE.name(),
					rechargeWithDrawOrder.getApplyValue());
			// 循环遍历插入报表总额和盈亏报表
			for (UserTotalMoneyByTypeVo userTotalMoneyByTypeVo : userTotalMoneyByTypeVos) {
				userMoneyTotalService.addUserTotalMoneyByType(userTotalMoneyByTypeVo.getUser(),
						userTotalMoneyByTypeVo.getDetailType(), userTotalMoneyByTypeVo.getOperationValue());
			}

			/**
			 * 监控金额操作
			 */
			BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager
					.getBizSystemConfig(orderUser.getBizSystem());
			if (bizSystemConfigVO.getRechargeAlarmOpen() != null && bizSystemConfigVO.getRechargeAlarmOpen() == 1) {
				if (rechargeWithDrawOrder.getAccountValue()
						.compareTo(bizSystemConfigVO.getRechargeAlarmValue()) >= 0) {
					AlarmInfo alarmInfo = new AlarmInfo();
					alarmInfo.setUserName(orderUser.getUserName());
					alarmInfo.setMoney(rechargeWithDrawOrder.getAccountValue());
					alarmInfo.setAlarmType(EAlarmInfo.CZ.getCode());
					alarmInfo.setBizSystem(orderUser.getBizSystem());
					if (bizSystemConfigVO.getAlarmEmailOpen() != null
							&& bizSystemConfigVO.getAlarmEmailOpen() == 1) {
						alarmInfo.setEmailSendOpen(1);
						alarmInfo.setAlarmEmail(bizSystemConfigVO.getAlarmEmail());
					} else {
						alarmInfo.setEmailSendOpen(0);
					}
					if (bizSystemConfigVO.getAlarmPhoneOpen() != null
							&& bizSystemConfigVO.getAlarmPhoneOpen() == 1) {
						alarmInfo.setSmsSendOpen(1);
						alarmInfo.setAlarmPhone(bizSystemConfigVO.getAlarmPhone());
					} else {
						alarmInfo.setSmsSendOpen(0);
					}
					alarmInfo.setCreateDate(new Date());
					AlarmInfoMessage alarmInfoMessage = new AlarmInfoMessage();
					alarmInfoMessage.setAlarmInfo(alarmInfo);
					MessageQueueCenter.putMessage(alarmInfoMessage);
				}
			}

			return true;
		} else {
			logger.info("当前订单状态已改变，订单号[" + rechargeWithDrawOrder.getSerialNumber() + "]，不进行金额充值处理...");
			return false;
		}
	}

	public Page getRechargeByQueryPage(RechargeWithDrawOrderQuery query, Page page) {
		page.setTotalRecNum(rechargeOrderMapper.getRechargeOrdersByQueryPageCount(query));
		page.setPageContent(
				rechargeOrderMapper.getRechargeOrdersByQueryPage(query, page.getStartIndex(), page.getPageSize()));
		return page;
	}

	/**
	 * 取消充值订单
	 * 
	 * @return
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void cancelRechargeOrder(Long rechargeOrderId) throws Exception {
		RechargeOrder rechargeOrder = this.selectByPrimaryKey(rechargeOrderId);
		if (rechargeOrder == null) {
			throw new Exception("未找到你的充值记录.");
		} else if (rechargeOrder.getDealStatus().equals(EFundRechargeStatus.PAYMENT.name())) { // 此处的请求必然是已经超过15分钟的充值订单,
																								// 如果这时候还处于支付中的状态,如果超过15分钟，则关闭该支付订单
			rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT_CLOSE.name());
			this.updateByPrimaryKeySelective(rechargeOrder);
		} else if (rechargeOrder.getDealStatus().equals(EFundRechargeStatus.PAYMENT_CLOSE.name())) {
			throw new Exception("该订单已经关闭了,请刷新页面.");
		} else if (rechargeOrder.getDealStatus().equals(EFundRechargeStatus.PAYMENT_SUCCESS.name())) {
			throw new Exception("该订单已经支付成功,不能撤销了.");
		} else {
			throw new Exception("未知的充值订单状态类型");
		}
	}

	/**
	 * 获取最新的充值期号.
	 * 
	 * @param query
	 * @return
	 */
	public String getUptodateSerialNumber(RechargeWithDrawOrderQuery query) {
		return rechargeOrderMapper.getUptodateSerialNumber(query);
	}

	/**
	 * 充值订单结果
	 * 
	 * @return
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void dealPayBankRechargeResult(Long rechargeOrderId) throws Exception {
		RechargeOrder rechargeOrder = this.selectByPrimaryKey(rechargeOrderId);
		if (rechargeOrder == null) {
			throw new Exception("未找到你的充值记录.");
		} else if (rechargeOrder.getDealStatus().equals(EFundRechargeStatus.PAYMENT.name())) { // 此处的请求必然是已经超过15分钟的充值订单,
																								// 如果这时候还处于支付中的状态,如果超过15分钟，则关闭该支付订单
			rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT_CLOSE.name());
			this.updateByPrimaryKeySelective(rechargeOrder);
		} else if (rechargeOrder.getDealStatus().equals(EFundRechargeStatus.PAYMENT_CLOSE.name())) {
			throw new Exception("PAYMENT_CLOSE");
		} else if (rechargeOrder.getDealStatus().equals(EFundRechargeStatus.PAYMENT_SUCCESS.name())) {
			throw new Exception("PAYMENT_SUCCESS");
		} else {
			throw new Exception("未知的充值订单状态类型");
		}
	}

	/**
	 * 查找业务系统某个用户的某段时间总冲值金额
	 * 
	 * @return
	 */
	public BigDecimal getBizSystemUserSumMoney(RechargeWithDrawOrderQuery query) {

		return rechargeOrderMapper.getBizSystemUserSumMoney(query);
	}

	/**
	 * 查询符合条件的数据总条数
	 * 
	 * @param rechargeWithDrawOrderQueryVo
	 */
	public Integer getRechargeOrdersPageCount(RechargeWithDrawOrderQuery rechargeWithDrawOrderQueryVo) {
		return rechargeOrderMapper.getRechargeOrdersPageCount(rechargeWithDrawOrderQueryVo);

	}

	/**
	 * 按类型统计各充值类型、取现的申请总额，手续费总额
	 */
	public List<RechargeOrder> getTotalValueSuccessOrderByType(RechargeWithDrawOrderQuery query) {
		return rechargeOrderMapper.getTotalValueSuccessOrderByType(query);
	}

	/**
	 * 按类型统计各充值类型、取现的人数
	 * 
	 * @param query
	 * @return
	 */
	public List<RechargeOrder> getUserCountSuccessOrderByType(RechargeWithDrawOrderQuery query) {
		return rechargeOrderMapper.getUserCountSuccessOrderByType(query);
	}

	/**
	 * 按类型统计各充值类型、取现的人数
	 * 
	 * @param query
	 * @return
	 */
	public List<RechargeOrder> getUserCountSuccessOrderByTypeSpecial(RechargeWithDrawOrderQuery query) {
		return rechargeOrderMapper.getUserCountSuccessOrderByTypeSpecial(query);
	}

	/**
	 * 统计充值、取现的人数
	 * 
	 * @param query
	 * @return
	 */
	public List<RechargeOrder> getUserCountSuccessOrderByOperateType(RechargeWithDrawOrderQuery query) {
		return rechargeOrderMapper.getUserCountSuccessOrderByOperateType(query);
	}

	/**
	 * 第三方充值方式生成充值订单
	 * 
	 * @param currentUser
	 *            当前登录用户
	 * @param money
	 *            充值金额
	 * @param rechargeConfig
	 *            充值配置
	 * @param serialNumber
	 *            充值订单号
	 * @return
	 */
	public void thirdPayInsertRechargeOrder(User currentUser, BigDecimal money, RechargeConfig rechargeConfig,
			String serialNumber) {
		// 构建订单
		RechargeOrder rechargeOrder = new RechargeOrder();
		// 用户ID
		rechargeOrder.setUserId(currentUser.getId());
		// 快捷ID
		rechargeOrder.setChargePayId(rechargeConfig.getRefId());
		// 用户名称
		rechargeOrder.setUserName(currentUser.getUserName());
		// 申请金额
		rechargeOrder.setApplyValue(money);
		// 订单创建时间
		rechargeOrder.setCreatedDate(new Date());
		// 充值类型
		rechargeOrder.setPayType(rechargeConfig.getPayType());
		// 第三方充值类型
		rechargeOrder.setThirdPayType(rechargeConfig.getChargeType());
		// 操作描述
		rechargeOrder.setOperateDes(EFundPayType.valueOf(rechargeConfig.getPayType()).getDescription());
		// 流水号
		rechargeOrder.setSerialNumber(serialNumber);
		rechargeOrder.setPostscript("");
		rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT.name());
		rechargeOrder.setBankName("");
		rechargeOrder.setSubbranchName("");
		rechargeOrder.setBankAccountName("");
		rechargeOrder.setBankCardNum("");
		rechargeOrder.setBankType("");
		rechargeOrder.setRefType(EFundRefType.THIRDPAY.name());
		rechargeOrder.setBizSystem(currentUser.getBizSystem());
		rechargeOrder.setFromType(currentUser.getLoginType());
		// 设置支付配置表ID.
		rechargeOrder.setRechargeConfigId(rechargeConfig.getId());
		// 设置支付配置的名称.
		rechargeOrder.setRechargeWayName(rechargeConfig.getShowName());

		// 当充值增送开关开启,并且当前充值金额大于充值增送最低金额,当前增送充值金额=当前充值金额*充值增送比例
		if (rechargeConfig.getRechargeGiftEnabled() == 1) {
			if (rechargeConfig.getRechargeGiftLowestValue() != null
					&& money.compareTo(rechargeConfig.getRechargeGiftLowestValue()) >= 0) {
				if (rechargeConfig.getRechargeGiftScale() != null) {
					rechargeOrder.setRechargeGiftScale(rechargeConfig.getRechargeGiftScale());
					rechargeOrder.setRechargeGiftValue(money.multiply(rechargeConfig.getRechargeGiftScale()));
				}
			}
		}
		rechargeOrder.setThirdPayTypeDesc(rechargeConfig.getChargeDes());
		this.insertSelective(rechargeOrder);
	}

	/**
	 * 根据充值订单号查询出充值对象.新方法.
	 * 
	 * @param orderid
	 *            充值订单号.
	 * @return
	 */
	public RechargeOrder getRechargeOrderByOderNumber(String orderid) {
		return rechargeOrderMapper.getRechargeOrderByOderNumber(orderid);
	}

}
