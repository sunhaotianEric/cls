package com.team.lottery.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.team.lottery.enums.EFundPayType;
import com.team.lottery.extvo.PayBankVo;
import com.team.lottery.extvo.RechargeConfigVo;
import com.team.lottery.mapper.paybank.PayBankMapper;
import com.team.lottery.vo.PayBank;



@Service("payBankService")
public class PayBankService {

	@Autowired
	private PayBankMapper payBankMapper;
	
    public int deleteByPrimaryKey(Long id){
    	return payBankMapper.deleteByPrimaryKey(id);
    }

    public int insert(PayBank record){
    	return payBankMapper.insert(record);
    }

    public int insertSelective(PayBank record){
    	if(record.getBankType().equals("ALIPAY"))
    	{
    		record.setBankName(EFundPayType.valueOf(record.getBankType()).getDescription());
    		record.setSubbranchName(EFundPayType.valueOf(record.getBankType()).getDescription());
    		
    	}
    	else if (record.getBankType().equals("WEIXIN"))
    	{
    		record.setBankName(EFundPayType.valueOf(record.getBankType()).getDescription());
    		record.setSubbranchName(EFundPayType.valueOf(record.getBankType()).getDescription());
    	}
    	return payBankMapper.insertSelective(record);
    }

    public PayBank selectByPrimaryKey(Long id){
    	return payBankMapper.selectByPrimaryKey(id);
    }

    public int updateByPrimaryKeySelective(PayBank record){
    	return payBankMapper.updateByPrimaryKeySelective(record);
    }

    public int updateByPrimaryKey(PayBank record){
    	return payBankMapper.updateByPrimaryKey(record);
    }
	
    /**
     * 查找所有的收款银行数据
     * @return
     */
    public List<PayBank> getAllPayBanks(PayBankVo query){
    	return payBankMapper.getAllPayBanks(query);
    }
	
    /**
     * 查找对应的银行可转账的银行
     * @param bankType
    */
    public List<PayBank> getCanPayBanksByType(String bankType){
    	return payBankMapper.getCanPayBanksByType(bankType);
    }
    
    /**
     * 将同类的银行卡置为可用
     * @param bankType
     */
    public void updatePayBanksUseByType(String bankType){
    	payBankMapper.updatePayBanksUseByType(bankType);
    }
    
    /**
     * 查找用户可使用的银行种类
     * @return
     */
    public List<PayBank> selectCanRechargeBanks(RechargeConfigVo query){
    	return payBankMapper.selectCanRechargeBanks(query);
    }
    
    /**
     * 随机获取一张可取现的银行卡
     * @return
     */
    public PayBank getPayBankForRandom(){
    	return payBankMapper.getPayBankForRandom();
    }
    
    /**
     * 根据银行名称、姓名和卡号查找银行卡信息
     * @return
     */
    public PayBank getPayBankByTypeNameAndId(String bankType,String bankUsername, String bandId){
    	return payBankMapper.getPayBankByTypeNameAndId(bankType, bankUsername, bandId);
    }
}
