package com.team.lottery.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.WithdrawAutoConfigQuery;
import com.team.lottery.mapper.withdrawautoconfig.WithdrawAutoConfigMapper;
import com.team.lottery.vo.WithdrawAutoConfig;

@Service("withdrawAutoConfigService")
public class WithdrawAutoConfigService {
	@Autowired
	private WithdrawAutoConfigMapper withdrawAutoConfigMapper;

	public int deleteByPrimaryKey(Integer id) {
		return withdrawAutoConfigMapper.deleteByPrimaryKey(id);
	}

	public int insertSelective(WithdrawAutoConfig record) {
		return withdrawAutoConfigMapper.insertSelective(record);
	}

	public WithdrawAutoConfig selectByPrimaryKey(Integer id) {
		return withdrawAutoConfigMapper.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(WithdrawAutoConfig record) {
		return withdrawAutoConfigMapper.updateByPrimaryKeySelective(record);
	}

	/**
	 * 分页查询自动出款对象.
	 * 
	 * @param query
	 * @param page
	 * @return
	 */
	public Page getWithdrawAutoConfigByQueryPage(WithdrawAutoConfigQuery query, Page page) {
		page.setTotalRecNum(withdrawAutoConfigMapper.getWithdrawAutoConfigByQueryPageCount(query));
		page.setPageContent(withdrawAutoConfigMapper.getWithdrawAutoConfigByQueryPage(query, page.getStartIndex(), page.getPageSize()));
		return page;
	}

	/**
	 * 根据业务系统查询相关自动出款配置
	 * 
	 * @param query
	 * @return
	 */
	public WithdrawAutoConfig selectWithdrawAutoConfig(String bizSystem) {
		return withdrawAutoConfigMapper.selectWithdrawAutoConfig(bizSystem);
	}

	/**
	 * 根据bizSystem查询出对应的充值配置对象
	 * @param bizSystem
	 * @return
	 */
	public WithdrawAutoConfig getWithdrawAutoConfig(String bizSystem){
		return withdrawAutoConfigMapper.getWithdrawAutoConfig(bizSystem);
		
	}

	/**
	 * 根据业务系统查询出所有的自动出款配置对象.
	 * @param bizSystem 系统.
	 * @return
	 */
	public WithdrawAutoConfig getWithdrawAutoConfigByBizSystem(String bizSystem) {
		return withdrawAutoConfigMapper.getWithdrawAutoConfigByBizSystem(bizSystem);
	}
}
