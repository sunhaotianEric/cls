package com.team.lottery.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.RechargeConfigVo;
import com.team.lottery.mapper.rechargeconfig.RechargeConfigMapper;
import com.team.lottery.util.UserNameUtil;
import com.team.lottery.vo.RechargeConfig;
import com.team.lottery.vo.User;

@Service("rechargeConfigService")
public class RechargeConfigService {

	@Autowired
	private RechargeConfigMapper rechargeConfigMapper;

	public int deleteByPrimaryKey(Long id) {
		return rechargeConfigMapper.deleteByPrimaryKey(id);
	}

	public int insert(RechargeConfig record) {
		return rechargeConfigMapper.insert(record);
	}

	public int insertSelective(RechargeConfig record) {
		return rechargeConfigMapper.insertSelective(record);
	}

	public RechargeConfig selectByPrimaryKey(Long id) {
		return rechargeConfigMapper.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(RechargeConfig record) {
		return rechargeConfigMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(RechargeConfig record) {
		return rechargeConfigMapper.updateByPrimaryKey(record);
	}

	/**
	 * 查找对应的支付，并且锁定
	 * 
	 * @param id
	 * @return
	 */
	public RechargeConfig selectByPrimaryKeyForUpdate(Long id) {
		return rechargeConfigMapper.selectByPrimaryKeyForUpdate(id);
	}

	/**
	 * 设置
	 * 
	 * @param id
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void updateRechargeConfigEnabled(Long id, Integer enabled) {
		RechargeConfig rechargeConfig = this.selectByPrimaryKeyForUpdate(id);
		rechargeConfig.setEnabled(enabled);
		this.updateByPrimaryKeySelective(rechargeConfig);
	}

	/**
	 * 查找所有的充值设置信息
	 * 
	 * @return
	 */
	public List<RechargeConfig> getAllRechargeConfig(RechargeConfigVo query) {
		return rechargeConfigMapper.getAllRechargeConfig(query);
	}

	/**
	 * 分页查找充值设置
	 * 
	 * @param query
	 * @param page
	 * @return
	 */
	public Page getAllRechargeConfigQueryPage(RechargeConfig query, Page page) {
		page.setTotalRecNum(rechargeConfigMapper.getAllRechargeConfigPageCount(query));
		page.setPageContent(rechargeConfigMapper.getAllRechargeConfigPage(query, page.getStartIndex(), page.getPageSize()));
		return page;
	}

	/**
	 * 查找所有的充值设置信息
	 * 
	 * @return
	 */
	public List<RechargeConfig> getAllPayIdByChargePay(RechargeConfigVo query) {
		return rechargeConfigMapper.getAllPayIdByChargePay(query);
	}

	/**
	 * 查找相关的配置信息
	 * 
	 * @return
	 */
	public List<RechargeConfig> selectRechargeConfig(RechargeConfigVo query) {
		return rechargeConfigMapper.selectRechargeConfig(query);
	}

	/**
	 * 查找相关快捷的配置信息
	 * 
	 * @return
	 */
	public List<RechargeConfig> selectRechargeConfigQuickPay(RechargeConfigVo query) {
		
		return rechargeConfigMapper.selectRechargeConfigQuickPay(query);
	}

	/**
	 * 根据用户等级去查询合格的RechargeConfig的list集合
	 * 
	 * @param user
	 * @return
	 */
	public List<RechargeConfig> getAllCanUseRechargeConfig(User user, RechargeConfigVo query,String requestProtocol) {
		// 注释代码!勿删,该功能后面使用!
		/*if (requestProtocol.equals("https")) {
			query.setSupportHttps(1);
		}*/
		
		List<RechargeConfig> allRechargeConfig = rechargeConfigMapper.getAllRechargeConfig(query);
		List<RechargeConfig> listParent = new ArrayList<RechargeConfig>();
		String regfrom = user.getRegfrom();
		if (regfrom != null && !"".equals(regfrom)) {
			for (RechargeConfig rechargeConfig : allRechargeConfig) {
				// 获取当前的等级组
				String showUserVipLevels = rechargeConfig.getShowUserVipLevels();			
				// 判断当前的RechargeConfig是否是为空字符串,如果是空的话根据VIP等级去查看权限
				if (StringUtils.isBlank(showUserVipLevels)) {
					if (user.getStarLevel().compareTo(rechargeConfig.getShowUserStarLevel()) >= 0 && Integer.valueOf(user.getVipLevel()).compareTo(rechargeConfig.getShowUserVipLevel()) >= 0 ) {
						listParent.add(rechargeConfig);
						continue;
					}
				} else {
					// 在根据逗号将字符串分割开得到String类型的数组showUserVipLevelsArr
					String[] showUserVipLevelsArr = showUserVipLevels.split(",");
					// 通过Arraylist中的asList将String数组转化为List数组然后在通过Contain方法判断数组中是否含有user.getVipLevel()返回Boolean类型的值;
					boolean isHaveViplevel = Arrays.asList(showUserVipLevelsArr).contains(String.valueOf(user.getVipLevel()));
					// 如果RechargeConfig不为空的话就通过当前用户的VIP等级去RechargeConfig中的showVipLevels去查看当前用户等级是否存在
					if (user.getStarLevel().compareTo(rechargeConfig.getShowUserStarLevel()) >= 0 && isHaveViplevel) {
						listParent.add(rechargeConfig);
						continue;
					}
				}
				if (rechargeConfig.getShowUserNameLine() != null) {
					String[] showUserNameLine = rechargeConfig.getShowUserNameLine().split(",");

					for (String line : showUserNameLine) {

						if (user.getUserName().equals(line)) {
							listParent.add(rechargeConfig);
							break;
						} else {
							if (UserNameUtil.isInRegfrom(regfrom, line)) {
								listParent.add(rechargeConfig);
								break;
							}
						}
					}
				}
			}
			// rechargeConfigVo.setShowUserNameLine(str[str.length-1]);
		}

		return listParent;

	}
	
	
}
