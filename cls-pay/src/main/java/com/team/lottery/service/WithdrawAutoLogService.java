package com.team.lottery.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.withdrawautolog.WithdrawAutoLogMapper;
import com.team.lottery.vo.WithdrawAutoLog;

@Service
public class WithdrawAutoLogService {

	@Autowired
	private WithdrawAutoLogMapper withdrawAutoLogMapper;

	public int deleteByPrimaryKey(Long id) {
		return withdrawAutoLogMapper.deleteByPrimaryKey(id);

	}

	public int insertSelective(WithdrawAutoLog record) {
		return withdrawAutoLogMapper.insertSelective(record);

	}

	public WithdrawAutoLog selectByPrimaryKey(Long id) {
		return withdrawAutoLogMapper.selectByPrimaryKey(id);

	}

	public int updateByPrimaryKeySelective(WithdrawAutoLog record) {
		return withdrawAutoLogMapper.updateByPrimaryKeySelective(record);

	}

	/**
	 * 分页查询
	 * @param query
	 * @param page
	 * @return
	 */
	public Page getAllWithdrawAutoLog(WithdrawAutoLog query, Page page) {
		page.setTotalRecNum(withdrawAutoLogMapper.getWithdrawAutoLogCount(query));
		page.setPageContent(withdrawAutoLogMapper.getAllWithdrawAutoLog(query, page.getStartIndex(), page.getPageSize()));
		return page;
	}

	public List<WithdrawAutoLog> getWithdrawAutoLogs(WithdrawAutoLog record) {
		return withdrawAutoLogMapper.getWithdrawAutoLogs(record);

	}

	/**
	 * 查询自动出款日志中所有未处理的订单.
	 * @return
	 */
	public List<WithdrawAutoLog> selectWithDrawAutoLogByDealStatus() {
		return withdrawAutoLogMapper.selectWithDrawAutoLogByDealStatus();
	}

}
