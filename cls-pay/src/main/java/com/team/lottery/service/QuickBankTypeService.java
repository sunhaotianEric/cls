package com.team.lottery.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.quickbanktype.QuickBankTypeMapper;
import com.team.lottery.vo.QuickBankType;




@Service("quickBankTypeService")
public class QuickBankTypeService {

	@Autowired
	private QuickBankTypeMapper quickBankTypeMapper;
	
	public int deleteByPrimaryKey(Long id){ 
    	return quickBankTypeMapper.deleteByPrimaryKey(id);
    }

    public int insert(QuickBankType record){
    	return quickBankTypeMapper.insert(record);
    }

    public int insertSelective(QuickBankType record){
    	return quickBankTypeMapper.insertSelective(record);
    }

    public QuickBankType selectByPrimaryKey(Long id){
    	return quickBankTypeMapper.selectByPrimaryKey(id);
    }

    public int updateByPrimaryKeySelective(QuickBankType record){
    	return quickBankTypeMapper.updateByPrimaryKeySelective(record);
    }

    public int updateByPrimaryKey(QuickBankType record){
    	return quickBankTypeMapper.updateByPrimaryKey(record);
    }
	
    /**
     * 查询快捷充值的可支付银行
     * @return
     */
    public List<QuickBankType> getAllCanPayBanksByQuickRecharge(){
    	return quickBankTypeMapper.getAllCanPayBanksByQuickRecharge();
    }
	
    /**
     * 查找对应的第三方支付银行代码,并且锁定
     * @param id
     * @return
     */
    public QuickBankType selectByPrimaryKeyForUpdate(Long id){
    	return quickBankTypeMapper.selectByPrimaryKeyForUpdate(id);
    }
    
    /**
     * 设置
     * @param id
     */
	@Transactional(readOnly=false,rollbackFor=Exception.class)
    public void updateQuickBankTypeEnabled(Long id,Integer enabled){
		QuickBankType quickBankType = this.selectByPrimaryKeyForUpdate(id);
		quickBankType.setEnabled(enabled);
    	this.updateByPrimaryKeySelective(quickBankType);
    }
    
	
    /**
     * 分页查找第三方支付银行代码
     * @param query
     * @param page
     * @return
     */
    public Page getAllQuickBankTypeQueryPage(QuickBankType query,Page page) {
		page.setTotalRecNum(quickBankTypeMapper.getAllQuickBankTypePageCount(query));
		page.setPageContent(quickBankTypeMapper.getAllQuickBankTypePage(query,page.getStartIndex(),page.getPageSize()));
    	return page;
	}
    
    public List<QuickBankType> getQuickBankTypeQuery(QuickBankType query) {
     	return quickBankTypeMapper.getQuickBankTypeByQuery(query);
 	}
}
