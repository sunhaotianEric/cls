package com.team.lottery.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.mapper.chargepay.ChargePayMapper;
import com.team.lottery.mapper.thirdpayconfig.ThirdPayConfigMapper;
import com.team.lottery.vo.ThirdPayConfig;

@Service("thirdPayConfigService")
public class ThirdPayConfigService {

	@Autowired
	private ThirdPayConfigMapper thirdPayConfigMapper;
	@Autowired
	private ChargePayMapper chargePayMapper;

	public int deleteByPrimaryKey(Long id) {
		return thirdPayConfigMapper.deleteByPrimaryKey(id);
	}

	public int insertSelective(ThirdPayConfig record) {
		return thirdPayConfigMapper.insertSelective(record);
	}

	public int updateByPrimaryKeySelective(ThirdPayConfig record) {
		return thirdPayConfigMapper.updateByPrimaryKeySelective(record);
	}

	/**
	 * 查找所有的第三方设置信息
	 * 
	 * @return
	 */
	public List<ThirdPayConfig> getThirdPayConfig() {
		return thirdPayConfigMapper.getThirdPayConfig();
	}

	public ThirdPayConfig selectByPrimaryKey(Long id) {
		return thirdPayConfigMapper.selectByPrimaryKey(id);
	}

	/**
	 * 根据查询条件查找所有的第三方设置信息
	 * 
	 * @return
	 */
	public List<ThirdPayConfig> queryThirdPayConfig(ThirdPayConfig record) {
		return thirdPayConfigMapper.queryThirdPayConfig(record);
	}

	/**
	 * 设置启用状态
	 * 
	 * @param id
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void updateThirdPayConfigEnabled(Long id, Integer enabled) {
		ThirdPayConfig thirdPayConfig = this.selectByPrimaryKey(id);
		thirdPayConfig.setEnabled(enabled);
		this.updateByPrimaryKeySelective(thirdPayConfig);
	}

	/**
	 * 修改第三方支付并且加入事务
	 * 
	 * @param thirdPayConfig 第三方支付相关参数
	 *
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void updateByPrimaryKeySelectiveAndChargeType(ThirdPayConfig thirdPayConfig) {
		thirdPayConfigMapper.updateByPrimaryKeySelective(thirdPayConfig);
		chargePayMapper.updateByChargeType(thirdPayConfig);
	}
	
	/**
	 * 根据查询条件查找所有的第三方设置信息
	 * 
	 * @return
	 */
	public ThirdPayConfig thirdPayConfigByChargeType(String chargeType) {
		return thirdPayConfigMapper.thirdPayConfigByChargeType(chargeType);
	}


}
