package com.team.lottery.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.extvo.ChargePayVo;
import com.team.lottery.mapper.chargepay.ChargePayMapper;
import com.team.lottery.vo.ChargePay;
import com.team.lottery.vo.ThirdPayConfig;

@Service("chargePayService")
public class ChargePayService {

	@Autowired
	private ChargePayMapper chargePayMapper;

	public int deleteByPrimaryKey(Long id) {
		return chargePayMapper.deleteByPrimaryKey(id);
	}

	public int insert(ChargePay record) {
		return chargePayMapper.insert(record);
	}

	public int insertSelective(ChargePay record) {
		return chargePayMapper.insertSelective(record);
	}

	public ChargePay selectByPrimaryKey(Long id) {
		return chargePayMapper.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(ChargePay record) {
		return chargePayMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(ChargePay record) {
		return chargePayMapper.updateByPrimaryKey(record);
	}

	/**
	 * 查找对应的支付，并且锁定
	 * @param id
	 * @return
	 */
	public ChargePay selectByPrimaryKeyForUpdate(Long id) {
		return chargePayMapper.selectByPrimaryKeyForUpdate(id);
	}

	/**
	 * 设置
	 * @param id
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void updateChargePayEnabled(Long id, Integer enabled) {
		ChargePay chargePay = this.selectByPrimaryKeyForUpdate(id);
		chargePay.setEnabled(enabled);
		this.updateByPrimaryKeySelective(chargePay);
	}

	/**
	 * 获取可支付的商户信息
	 * @return
	 * @throws Exception 
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public ChargePay getPayMemmberMsg(Long id) throws Exception {
		/*
		 * List<ChargePay> chargePays = this.getCanPayQuicksByType(chargeType);
		 * ChargePay chargePayResult = null; ChargePay payTemp = null; if(chargePays !=
		 * null && chargePays.size() > 0){ chargePayResult = chargePays.get(0); }else{
		 * chargePayMapper.updatePayQuicksUseByType(chargeType); chargePays =
		 * chargePayMapper.getCanPayQuicksByType(chargeType); if(chargePays != null &&
		 * chargePays.size() > 0){ chargePayResult = chargePays.get(0); }else{ throw new
		 * Exception("当前系统未找到对应的可用快捷支付."); } }
		 * 
		 * //chargePayResult.setYetUse(1);
		 * chargePayMapper.updateByPrimaryKeySelective(chargePayResult); return
		 * chargePayResult;
		 */
		ChargePay chargePayResult = chargePayMapper.selectByPrimaryKey(id);
		return chargePayResult;

	}

	/**
	 * 查找对应的快捷支付可支付的
	 * @param bankType
	*/
	public List<ChargePay> getCanPayQuicksByType(String chargeType) {
		return chargePayMapper.getCanPayQuicksByType(chargeType);
	}

	/**
	 * 将同类的快捷支付置为可用
	 * @param bankType
	 */
	public void updatePayQuicksUseByType(String chargeType) {
		chargePayMapper.updatePayQuicksUseByType(chargeType);
	}

	/**
	 * 查找可快捷充值的支付平台
	 * @return
	 */
	public List<ChargePay> selectCanQuickRecharges() {
		return chargePayMapper.selectCanQuickRecharges();
	}

	/**
	 * 查找所有的支付信息
	 * @return
	 */
	public List<ChargePay> getAllChargePays(ChargePayVo query) {
		return chargePayMapper.getAllChargePays(query);
	}

	/**
	 * 查找所有的支付信息
	 * @return
	 */
	public List<ChargePay> selectAllChargePays(ChargePayVo query) {
		return chargePayMapper.selectAllChargePays(query);
	}

	/**
	 * 根据第三方设置信息修改相关的快捷支付信息
	 * @return
	 */
	public void updateByChargeType(ThirdPayConfig thirdPayConfig) {
		chargePayMapper.updateByChargeType(thirdPayConfig);
	}

	/**
	 * 根据当前删除对象的唯一chargeType去查询是否还存在其他快捷支付
	 * @param chargeType
	 * @return
	 */
	public List<ChargePay> selectByChargeType(String chargeType) {
		return chargePayMapper.selectByChargeType(chargeType);
	}

	/**
	 * 根据商户号和业务系统获取商户号配置,
	 * @param merchNo
	 * @param bizSystem
	 * @return
	 */
	public List<ChargePay> getChargePaysByMemberId(String merchNo, String bizSystem) {
		return chargePayMapper.getChargePaysByMemberId(merchNo, bizSystem);
	}
}
