package com.team.lottery.service;

import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.paynotifylog.PayNotifyLogMapper;
import com.team.lottery.vo.PayNotifyLog;

@Service("payNotifyLogService")
public class PayNotifyLogService {
	private static Logger logger = LoggerFactory.getLogger(PayNotifyLogService.class);
	@Autowired
	private PayNotifyLogMapper payNotifyLogMapper;

	public int deleteByPrimaryKey(Long id) {
		return payNotifyLogMapper.deleteByPrimaryKey(id);
	}

	public int insert(PayNotifyLog record) {
		return payNotifyLogMapper.insert(record);
	}

	public int insertSelective(PayNotifyLog record) {
		return payNotifyLogMapper.insertSelective(record);
	}

	public PayNotifyLog selectByPrimaryKey(Long id) {
		return payNotifyLogMapper.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(PayNotifyLog record) {
		return payNotifyLogMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(PayNotifyLog record) {
		return payNotifyLogMapper.updateByPrimaryKeySelective(record);
	}

	/**
	 * 查找所有的
	 * 
	 * @return
	 */
	public Page getAllPayNotifyLogByQueryPage(PayNotifyLog query, Page page) {
		page.setTotalRecNum(payNotifyLogMapper.getAllPayNotifyLogByQueryPageCount(query));
		page.setPageContent(payNotifyLogMapper.getAllPayNotifyLogByQueryPage(query, page.getStartIndex(), page.getPageSize()));
		return page;
	}

	/**
	 * 增加第三方支付操作日志
	 * 
	 * @return PayNotifyLog
	 */
	public PayNotifyLog addPayNotifyLog(PayNotifyLog payNotifyLog, String thirdType, String thirdTypeDesc, String messageContent) {
		logger.info("第三方操作记录日志打印:" + "requestIp= " + payNotifyLog.getRequestIp() + "thirdType= " + thirdType + "thirdTypeDesc= " + thirdTypeDesc
				+ "messageContent= " + messageContent);
		payNotifyLog.setBizSystem(null);
		payNotifyLog.setThirdType(thirdType);
		payNotifyLog.setThirdTypeDesc(thirdTypeDesc);
		payNotifyLog.setCreatedDate(new Date());
		payNotifyLog.setMessageContent(messageContent);
		payNotifyLogMapper.insert(payNotifyLog);
		return payNotifyLog;
	}

}
