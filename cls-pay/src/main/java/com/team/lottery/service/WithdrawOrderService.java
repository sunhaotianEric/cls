package com.team.lottery.service;

import java.io.*;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.*;

import com.team.lottery.pay.util.RSA;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.enums.EFundAutoWithdrawDealStatus;
import com.team.lottery.enums.EFundOperateType;
import com.team.lottery.enums.EFundWithDrawStatus;
import com.team.lottery.enums.EMoneyDetailType;
import com.team.lottery.enums.ENoteStatus;
import com.team.lottery.enums.ENoteType;
import com.team.lottery.enums.EWithdrawAutoStatus;
import com.team.lottery.enums.EWithdrawAutoType;
import com.team.lottery.exception.NormalBusinessException;
import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.RechargeWithDrawOrderQuery;
import com.team.lottery.extvo.UserTotalMoneyByTypeVo;
import com.team.lottery.mapper.withdraworder.WithdrawOrderMapper;
import com.team.lottery.system.BizSystemConfigManager;
import com.team.lottery.system.BizSystemConfigVO;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.HttpClientUtil;
import com.team.lottery.util.MakeOrderNum;
import com.team.lottery.util.ToolKit;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.ChargePay;
import com.team.lottery.vo.MoneyDetail;
import com.team.lottery.vo.Notes;
import com.team.lottery.vo.QuickBankType;
import com.team.lottery.vo.ThirdPayConfig;
import com.team.lottery.vo.User;
import com.team.lottery.vo.UserBank;
import com.team.lottery.vo.WithdrawAutoConfig;
import com.team.lottery.vo.WithdrawAutoLog;
import com.team.lottery.vo.WithdrawOrder;
import com.team.lottery.vo.WithdrawOrderCount;

import net.sf.json.JSONObject;

import javax.crypto.Cipher;

@Service("withdrawOrderService")
public class WithdrawOrderService {

	private static Logger logger = LoggerFactory.getLogger(WithdrawOrderService.class);

	@Autowired
	private MoneyDetailService moneyDetailService;
	@Autowired
	private UserService userService;
	@Autowired
	private UserBankService userBankService;
	@Autowired
	private NotesService notesService;
	@Autowired
	private UserMoneyTotalService userMoneyTotalService;
	@Autowired
	private WithdrawOrderMapper withdrawOrderMapper;
	@Autowired
	private WithdrawAutoLogService withdrawAutoLogService;
	@Autowired
	private QuickBankTypeService quickBankTypeService;
	@Autowired
	private ThirdPayConfigService thirdPayConfigService;

	public int deleteByPrimaryKey(Long id) {
		return withdrawOrderMapper.deleteByPrimaryKey(id);
	}

	public int insert(WithdrawOrder record) {
		return withdrawOrderMapper.insert(record);
	}

	public int insertSelective(WithdrawOrder record) {
		return withdrawOrderMapper.insertSelective(record);
	}

	public WithdrawOrder selectByPrimaryKey(Long id) {
		return withdrawOrderMapper.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(WithdrawOrder record) {
		return withdrawOrderMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(WithdrawOrder record) {
		return withdrawOrderMapper.updateByPrimaryKey(record);
	}

	/**
	 * 查找所有的充值记录
	 *
	 * @return
	 */
	public Page getWithDrawOrdersByQueryPage(RechargeWithDrawOrderQuery query, Page page) {
		page.setTotalRecNum(withdrawOrderMapper.getWithDrawOrdersByQueryPageCount(query));
		page.setPageContent(withdrawOrderMapper.getWithDrawOrdersByQueryPage(query, page.getStartIndex(), page.getPageSize()));
		return page;
	}

	/**
	 * 取现申请锁定
	 *
	 * @param rechargeId
	 * @throws Exception
	 */
	public void setWithDrawOrdersLocked(WithdrawOrder rechargeOrder, Admin admin) throws Exception {
		/* logger.info("rechargeId["+rechargeId+"],取现申请成功... "); */
		// WithdrawOrder rechargeOrder = this.selectByPrimaryKey(rechargeId);

		// Admin admin = BaseDwrUtil.getCurrentAdmin();
		logger.info("管理员[" + admin.getUsername() + "]锁定业务系统[" + rechargeOrder.getBizSystem() + "]订单流水号[" + rechargeOrder.getSerialNumber()
				+ "]取现订单");
		rechargeOrder.setDealStatus(EFundWithDrawStatus.LOCKED.name());
		rechargeOrder.setLockStatus(WithdrawOrder.LOCKED);
		rechargeOrder.setLockAdmin(admin.getUsername());
		rechargeOrder.setUpdateAdmin(admin.getUsername());
		rechargeOrder.setUpdateDate(new Date());
		this.updateByPrimaryKeySelective(rechargeOrder);

	}

	/**
	 * 取现申请成功
	 *
	 * @param rechargeId
	 * @throws Exception
	 */
	public void setWithDrawOrdersPaySuccess(WithdrawOrder rechargeOrder, Admin admin) throws UnEqualVersionException, Exception {
		/* logger.info("rechargeId["+rechargeId+"],取现申请成功... "); */
		// WithdrawOrder rechargeOrder = this.selectByPrimaryKey(rechargeId);

		// Admin admin = BaseDwrUtil.getCurrentAdmin();

		logger.info("管理员[" + admin.getUsername() + "]允许业务系统[" + rechargeOrder.getBizSystem() + "]中用户[" + rechargeOrder.getUserName()
				+ "]的订单流水号[" + rechargeOrder.getSerialNumber() + "]取现申请");
		rechargeOrder.setDealStatus(EFundWithDrawStatus.WITHDRAW_SUCCESS.name());
		rechargeOrder.setAccountValue(rechargeOrder.getApplyValue());
		rechargeOrder.setUpdateAdmin(admin.getUsername());
		rechargeOrder.setUpdateDate(new Date());
		this.updateByPrimaryKeySelective(rechargeOrder);
		User currentUser = userService.selectByPrimaryKey(rechargeOrder.getUserId());
		if (currentUser.getWithdrawLock() == 1) {
			throw new Exception("用户[" + currentUser.getUserName() + "]账户被提现锁定,您无法进行确认取现操作");
		}
		Notes notes = new Notes();
		notes.setParentId(0l);
		notes.setEnabled(1);
		notes.setFromUserName(ConstantUtil.SYSTEM_OPERATE_NAME);
		notes.setToUserName(rechargeOrder.getUserName());
		notes.setSub("[取现申请]");
		notes.setBody("恭喜您,您的取现申请已经成功提交,申请取现的资金是:" + rechargeOrder.getApplyValue().setScale(2, BigDecimal.ROUND_HALF_UP));
		notes.setType(ENoteType.SYSTEM.name());
		notes.setStatus(ENoteStatus.NO_READ.name());
		notes.setCreatedDate(new Date());
		notes.setBizSystem(currentUser.getBizSystem());
		notes.setToUserId(currentUser.getId());
		notesService.insertSelective(notes);
	}

	/**
	 * 取现申请订单关闭
	 *
	 * @param rechargeId
	 * @throws Exception
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void setWithDrawOrdersPayClose(WithdrawOrder rechargeOrder, String analyseValue, Admin admin) throws UnEqualVersionException,
			Exception {
		/* logger.info("rechargeId["+rechargeId+"],取现申请订单关闭... "); */
		// WithdrawOrder rechargeOrder = this.selectByPrimaryKey(rechargeId);

		// Admin admin = BaseDwrUtil.getCurrentAdmin();

		logger.info("管理员[" + admin.getUsername() + "将订单流水号[" + rechargeOrder.getSerialNumber() + "]取现申请关闭");
		rechargeOrder.setDealStatus(EFundWithDrawStatus.WITHDRAW_FAIL.name());
		rechargeOrder.setUpdateAdmin(admin.getUsername());
		rechargeOrder.setUpdateDate(new Date());
		this.updateByPrimaryKeySelective(rechargeOrder);

		User currentUser = userService.selectByPrimaryKey(rechargeOrder.getUserId());
		List<UserTotalMoneyByTypeVo> userTotalMoneyByTypeVos = new ArrayList<UserTotalMoneyByTypeVo>();
		if (currentUser != null) {
			// 将钱退回去
			MoneyDetail moneyDetail = new MoneyDetail();
			moneyDetail.setUserId(currentUser.getId());
			moneyDetail.setOrderId(rechargeOrder.getId());
			moneyDetail.setOperateUserName(admin.getUsername()); // 操作人员
			moneyDetail.setUserName(currentUser.getUserName()); // 用户名
			moneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.TX)); // 订单单号
			moneyDetail.setExpect(""); // 哪个期号
			moneyDetail.setLotteryType(""); // 彩种
			moneyDetail.setLotteryTypeDes(""); // 彩种名称
			moneyDetail.setDetailType(EMoneyDetailType.WITHDRAW.name()); // 资金类型为投注
			moneyDetail.setIncome(rechargeOrder.getApplyValue()); // 收入为0
			moneyDetail.setPay(new BigDecimal(0)); // 支出金额
			moneyDetail.setBalanceBefore(currentUser.getMoney()); // 支出前金额
			moneyDetail.setBalanceAfter(currentUser.getMoney().add(rechargeOrder.getApplyValue())); // 支出后的金额
			moneyDetail.setWinLevel(null);
			moneyDetail.setWinCost(null);
			moneyDetail.setWinCostRule(null);
			moneyDetail.setDescription("取现订单关闭:" + rechargeOrder.getApplyValue());
			moneyDetail.setBizSystem(currentUser.getBizSystem());
			moneyDetailService.insertSelective(moneyDetail);

			// 提现关闭归还增加用户余额
			currentUser.addMoney(rechargeOrder.getApplyValue(), false);
			// 提现关闭归还当时使用的可提现额度
			currentUser.addCanWithdrawMoney(rechargeOrder.getCurrentCanWithdrawMoney());
			// 归还扣减总提现金额，负值
			BigDecimal reduceApplyValue = rechargeOrder.getApplyValue().multiply(new BigDecimal("-1"));
			// 针对盈亏报表多线程不会事物回滚处理方式
			UserTotalMoneyByTypeVo userTotalMoneyByTypeVo = new UserTotalMoneyByTypeVo(currentUser, EMoneyDetailType.WITHDRAW,
					reduceApplyValue);
			userTotalMoneyByTypeVos.add(userTotalMoneyByTypeVo);
			// userMoneyTotalService.addUserTotalMoneyByType(currentUser,
			// EMoneyDetailType.WITHDRAW, reduceApplyValue);

			// 手续费记录
			if (rechargeOrder.getFeeValue() != null && rechargeOrder.getFeeValue().compareTo(new BigDecimal(0)) > 0) {
				// 手续费资金明细
				MoneyDetail moneyDetailFee = new MoneyDetail();
				moneyDetailFee.setUserId(currentUser.getId());
				moneyDetailFee.setOrderId(rechargeOrder.getId());
				moneyDetailFee.setOperateUserName(admin.getUsername()); // 操作人员
				moneyDetailFee.setUserName(currentUser.getUserName()); // 用户名
				moneyDetailFee.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.TF)); // 订单单号
				moneyDetailFee.setExpect(""); // 哪个期号
				moneyDetailFee.setLotteryType(""); // 彩种
				moneyDetailFee.setLotteryTypeDes(""); // 彩种名称
				moneyDetailFee.setDetailType(EMoneyDetailType.WITHDRAW_FEE.name()); // 资金类型为投注
				moneyDetailFee.setIncome(rechargeOrder.getFeeValue()); // 收入为0
				moneyDetailFee.setPay(new BigDecimal(0)); // 支出金额
				moneyDetailFee.setBalanceBefore(currentUser.getMoney()); // 支出前金额
				moneyDetailFee.setBalanceAfter(currentUser.getMoney().add(rechargeOrder.getFeeValue())); // 支出后的金额
				moneyDetailFee.setWinLevel(null);
				moneyDetailFee.setWinCost(null);
				moneyDetailFee.setWinCostRule(null);
				moneyDetailFee.setDescription("返还取现手续费:" + rechargeOrder.getFeeValue());
				moneyDetailFee.setBizSystem(currentUser.getBizSystem());
				moneyDetailService.insertSelective(moneyDetailFee);

				// 归还手续费
				currentUser.addMoney(rechargeOrder.getFeeValue(), false);
				// 归还扣减总提现手续费，负值
				BigDecimal reduceFeeValue = rechargeOrder.getFeeValue().multiply(new BigDecimal("-1"));
				// 针对盈亏报表多线程不会事物回滚处理方式
				UserTotalMoneyByTypeVo userTotalMoneyByTypeVo1 = new UserTotalMoneyByTypeVo(currentUser, EMoneyDetailType.WITHDRAW_FEE,
						reduceFeeValue);
				userTotalMoneyByTypeVos.add(userTotalMoneyByTypeVo1);
			}

			// 取现申请成功系统消息
			Notes notes = new Notes();
			notes.setParentId(0l);
			notes.setEnabled(1);
			notes.setFromUserName(ConstantUtil.SYSTEM_OPERATE_NAME);
			notes.setToUserName(rechargeOrder.getUserName());
			notes.setSub("[取现申请]");
			notes.setBody("抱歉,您的取现申请订单号  " + rechargeOrder.getSerialNumber() + "  已经关闭。如果您有疑问,请联系我们的客服.");
			notes.setType(ENoteType.SYSTEM.name());
			notes.setStatus(ENoteStatus.NO_READ.name());
			notes.setCreatedDate(new Date());
			notes.setBizSystem(currentUser.getBizSystem());
			notes.setToUserId(currentUser.getId());
			notesService.insertSelective(notes);
			userService.updateByPrimaryKeyWithVersionSelective(currentUser);
		}
		// 循环遍历插入报表总额和盈亏报表
		for (UserTotalMoneyByTypeVo userTotalMoneyByTypeVo : userTotalMoneyByTypeVos) {
			userMoneyTotalService.addUserTotalMoneyByType(userTotalMoneyByTypeVo.getUser(), userTotalMoneyByTypeVo.getDetailType(),
					userTotalMoneyByTypeVo.getOperationValue());
		}
	}

	/**
	 * 用户提现申请
	 *
	 * @param withDrawId
	 * @param withDrawValue
	 * @throws Exception
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public WithdrawOrder withDrawApply(Long userBankId, String withDrawValueStr, User currentUser) throws UnEqualVersionException,
			Exception {
		logger.info("业务系统[{}],用户名[{}]申请提现,申请提现金额[{}],银行卡ID[{}]", currentUser.getBizSystem(), currentUser.getUserName(), withDrawValueStr,
				userBankId);
		BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(currentUser.getBizSystem());
		BigDecimal withDrawValue = new BigDecimal(withDrawValueStr);
		if (bizSystemConfigVO.getWithDrawOneStrokeLowestMoney() != null) {
			if (withDrawValue.compareTo(bizSystemConfigVO.getWithDrawOneStrokeLowestMoney()) < 0) {
				throw new NormalBusinessException("单笔取现金额不满足要求.");
			}
		}

		if (bizSystemConfigVO.getWithDrawOneStrokeHighestMoney() != null) {
			if (withDrawValue.compareTo(bizSystemConfigVO.getWithDrawOneStrokeHighestMoney()) > 0) {
				throw new NormalBusinessException("单笔取现金额不满足要求.");
			}
		}

		String fromType = currentUser.getLoginType();;
		currentUser = userService.selectByPrimaryKey(currentUser.getId());

		if (currentUser.getMoney().compareTo(withDrawValue) < 0) {
			throw new NormalBusinessException("当前提现余额不足.");
		}

		UserBank userBank = userBankService.getUserBankByIdAndUserId(userBankId, currentUser.getId());
		if (userBank == null) {
			throw new NormalBusinessException("您当前该银行卡记录不正常,请刷新页面.");
		}

		WithdrawOrder withdrawOrderNew = new WithdrawOrder();
		// 订单编号
		String serialNumber = MakeOrderNum.makeOrderNum(MakeOrderNum.TX);
		if (currentUser.getCanWithdrawMoney().compareTo(withDrawValue) >= 0 || bizSystemConfigVO.getExceedWithdrawExpenses() == null) {
			withdrawOrderNew.setFeeValue(BigDecimal.ZERO);
			withdrawOrderNew.setApplyValue(withDrawValue);
			withdrawOrderNew.setCurrentCanWithdrawMoney(withDrawValue);
		} else {
			if (bizSystemConfigVO.getExceedWithdrawExpenses() != null) {
				BigDecimal feedValue = withDrawValue.subtract(currentUser.getCanWithdrawMoney())
						.multiply(bizSystemConfigVO.getExceedWithdrawExpenses()).multiply(new BigDecimal(0.01))
						.setScale(2, BigDecimal.ROUND_HALF_UP);
				withdrawOrderNew.setFeeValue(feedValue);
				withdrawOrderNew.setApplyValue(withDrawValue.subtract(withdrawOrderNew.getFeeValue()));
				withdrawOrderNew.setCurrentCanWithdrawMoney(currentUser.getCanWithdrawMoney());
			}
		}

		// 资金明细
		MoneyDetail moneyDetail = new MoneyDetail();
		moneyDetail.setUserId(currentUser.getId());
		moneyDetail.setOrderId(withdrawOrderNew.getId());
		moneyDetail.setOperateUserName(currentUser.getUserName()); // 操作人员
		moneyDetail.setUserName(currentUser.getUserName()); // 用户名
		// 取提现订单的编号
		moneyDetail.setLotteryId(serialNumber);
		moneyDetail.setExpect(""); // 哪个期号
		moneyDetail.setLotteryType(""); // 彩种
		moneyDetail.setLotteryTypeDes(""); // 彩种名称
		moneyDetail.setDetailType(EMoneyDetailType.WITHDRAW.name()); // 资金类型为投注
		moneyDetail.setIncome(new BigDecimal(0)); // 收入为0
		moneyDetail.setPay(withdrawOrderNew.getApplyValue()); // 支出金额
		moneyDetail.setBalanceBefore(currentUser.getMoney()); // 支出前金额
		moneyDetail.setBalanceAfter(currentUser.getMoney().subtract(withdrawOrderNew.getApplyValue())); // 支出后的金额
		moneyDetail.setWinLevel(null);
		moneyDetail.setWinCost(null);
		moneyDetail.setWinCostRule(null);
		moneyDetail.setDescription("取现金额:" + withdrawOrderNew.getApplyValue());
		moneyDetail.setBizSystem(currentUser.getBizSystem());
		moneyDetailService.insertSelective(moneyDetail);
		// 提现申请扣除用户余额
		currentUser.reduceMoney(withdrawOrderNew.getApplyValue(), true);
		List<UserTotalMoneyByTypeVo> userTotalMoneyByTypeVos = new ArrayList<UserTotalMoneyByTypeVo>();
		// 针对盈亏报表多线程不会事物回滚处理方式
		UserTotalMoneyByTypeVo userTotalMoneyByTypeVo = new UserTotalMoneyByTypeVo(currentUser, EMoneyDetailType.WITHDRAW,
				withdrawOrderNew.getApplyValue());
		userTotalMoneyByTypeVos.add(userTotalMoneyByTypeVo);

		// 手续费记录
		if (withdrawOrderNew.getFeeValue() != null && withdrawOrderNew.getFeeValue().compareTo(new BigDecimal(0)) > 0) {
			// 手续费资金明细
			MoneyDetail moneyDetailFee = new MoneyDetail();
			moneyDetailFee.setUserId(currentUser.getId());
			moneyDetailFee.setOrderId(withdrawOrderNew.getId());
			moneyDetailFee.setOperateUserName(currentUser.getUserName()); // 操作人员
			moneyDetailFee.setUserName(currentUser.getUserName()); // 用户名
			moneyDetailFee.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.TF)); // 订单单号
			moneyDetailFee.setExpect(""); // 哪个期号
			moneyDetailFee.setLotteryType(""); // 彩种
			moneyDetailFee.setLotteryTypeDes(""); // 彩种名称
			moneyDetailFee.setDetailType(EMoneyDetailType.WITHDRAW_FEE.name()); // 资金类型为投注
			moneyDetailFee.setIncome(new BigDecimal(0)); // 收入为0
			moneyDetailFee.setPay(withdrawOrderNew.getFeeValue()); // 支出金额
			moneyDetailFee.setBalanceBefore(currentUser.getMoney()); // 支出前金额
			moneyDetailFee.setBalanceAfter(currentUser.getMoney().subtract(withdrawOrderNew.getFeeValue())); // 支出后的金额
			moneyDetailFee.setWinLevel(null);
			moneyDetailFee.setWinCost(null);
			moneyDetailFee.setWinCostRule(null);
			moneyDetailFee.setBizSystem(currentUser.getBizSystem());
			moneyDetailFee.setDescription("取现手续费金额:" + withdrawOrderNew.getApplyValue());
			moneyDetailService.insertSelective(moneyDetailFee);

			// 扣除手续费
			currentUser.reduceMoney(withdrawOrderNew.getFeeValue(), true);
			// 针对盈亏报表多线程不会事物回滚处理方式
			UserTotalMoneyByTypeVo userTotalMoneyByTypeVo1 = new UserTotalMoneyByTypeVo(currentUser, EMoneyDetailType.WITHDRAW_FEE,
					withdrawOrderNew.getFeeValue());
			userTotalMoneyByTypeVos.add(userTotalMoneyByTypeVo1);
		}

		withdrawOrderNew.setBankAccountName(userBank.getBankAccountName());
		withdrawOrderNew.setBankCardNum(userBank.getBankCardNum());
		withdrawOrderNew.setBankName(userBank.getBankName());
		withdrawOrderNew.setBankType(userBank.getBankType());
		withdrawOrderNew.setBizSystem(currentUser.getBizSystem());
		withdrawOrderNew.setCreatedDate(new Date());
		withdrawOrderNew.setDealStatus(EFundWithDrawStatus.DEALING.name());
		withdrawOrderNew.setOperateDes(EFundOperateType.WITHDRAW.getDescription() + "[" + userBank.getBankName() + "]");
		/* withdrawOrderNew.setRemark(rechargeOrder.getAnalyse()); */
		withdrawOrderNew.setSerialNumber(serialNumber);
		withdrawOrderNew.setSubbranchName(userBank.getSubbranchName());
		withdrawOrderNew.setUserId(currentUser.getId());
		/*withdrawOrderNew.setBankCity(userBank.getCity());
		withdrawOrderNew.setBankProvince(userBank.getProvince());
*/
		withdrawOrderNew.setUserName(currentUser.getUserName());
		logger.info("用户登录[{}]类型[{}]", currentUser.getUserName(), fromType);
		withdrawOrderNew.setFromType(fromType);
		withdrawOrderMapper.insertSelective(withdrawOrderNew);

		logger.info("业务系统[" + withdrawOrderNew.getBizSystem() + "],用户名[" + withdrawOrderNew.getUserName() + "],用户ID号["
				+ withdrawOrderNew.getUserId() + "],取现金额[" + withdrawOrderNew.getApplyValue() + "],订单流水号["
				+ withdrawOrderNew.getSerialNumber() + "]");
		userBank.setLocked(1);
		logger.info("业务系统[" + withdrawOrderNew.getBizSystem() + "],用户名[" + withdrawOrderNew.getUserName() + "],用户ID号["
				+ withdrawOrderNew.getUserId() + "],锁定银行卡[" + userBank.getBankCardNum() + "]");
		userBankService.updateByPrimaryKeySelective(userBank);
		userService.updateByPrimaryKeyWithVersionSelective(currentUser);
		// 循环遍历插入报表总额和盈亏报表
		for (UserTotalMoneyByTypeVo uVo : userTotalMoneyByTypeVos) {
			userMoneyTotalService.addUserTotalMoneyByType(uVo.getUser(), uVo.getDetailType(), uVo.getOperationValue());
		}
		return withdrawOrderNew;
	}

	public List<WithdrawOrder> getWithDrawOrders(RechargeWithDrawOrderQuery query) {
		return withdrawOrderMapper.getWithDrawOrders(query);
	}

	/**
	 * 总共的查询订单总值
	 *
	 * @param query
	 * @return
	 */
	public WithdrawOrder getWithDrawOrdersByTotal(RechargeWithDrawOrderQuery query) {
		return withdrawOrderMapper.getWithDrawOrdersByTotal(query);
	}

	/**
	 * 分页查找充值提现记录
	 *
	 * @return
	 */
	public Page getWithDrawsByQueryPage(RechargeWithDrawOrderQuery query, Page page) {
		page.setTotalRecNum(withdrawOrderMapper.getWithDrawOrdersByQueryPageCount(query));
		page.setPageContent(withdrawOrderMapper.getWithDrawOrdersByQueryPage(query, page.getStartIndex(), page.getPageSize()));
		return page;
	}

	/**
	 * 按类型统计各充值类型、取现的申请总额，手续费总额
	 */
	public List<WithdrawOrder> getTotalValueSuccessOrderByType(RechargeWithDrawOrderQuery query) {
		return withdrawOrderMapper.getTotalValueSuccessOrderByType(query);
	}

	/**
	 * 按类型统计各充值类型、取现的人数
	 *
	 * @param query
	 * @return
	 */
	public List<WithdrawOrder> getUserCountSuccessOrderByType(RechargeWithDrawOrderQuery query) {
		return withdrawOrderMapper.getUserCountSuccessOrderByType(query);
	}

	/**
	 * 统计充值、取现的人数
	 *
	 * @param query
	 * @return
	 */
	public List<WithdrawOrder> getUserCountSuccessOrderByOperateType(RechargeWithDrawOrderQuery query) {
		return withdrawOrderMapper.getUserCountSuccessOrderByOperateType(query);
	}

	public List<WithdrawOrder> getRechargeWithDrawOrders(RechargeWithDrawOrderQuery query) {
		return withdrawOrderMapper.getRechargeWithDrawOrders(query);
	}

	/**
	 * 自动出款成功
	 *
	 * @param rechargeId
	 * @throws Exception
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public synchronized boolean setWithDrawOrdersAutoPaySuccess(Long rechargeId, String orderNum) throws UnEqualVersionException, Exception {
		WithdrawOrder rechargeOrder = this.selectByPrimaryKey(rechargeId);
		if (rechargeOrder == null) {
			throw new Exception("该订单不存在.");
		}
		if (!EFundWithDrawStatus.LOCKED.name().equals(rechargeOrder.getDealStatus())) {
			throw new Exception("该订单还未被锁定，请先进行锁定操作");
		}
		if (rechargeOrder.getDealStatus().equals(EFundWithDrawStatus.WITHDRAW_SUCCESS.name())) {
			throw new Exception("该订单已经取现成功了,无法操作");
		}
		if (rechargeOrder.getDealStatus().equals(EFundWithDrawStatus.WITHDRAW_FAIL.name())) {
			throw new Exception("该订单已经取现关闭了,无法操作");
		}

		logger.info("管理员[" + rechargeOrder.getLockAdmin() + "]允许业务系统[" + rechargeOrder.getBizSystem() + "]中的用户["
				+ rechargeOrder.getUserName() + "]的订单流水号[" + rechargeOrder.getSerialNumber() + "]取现申请");
		rechargeOrder.setDealStatus(EFundWithDrawStatus.WITHDRAW_SUCCESS.name());
		rechargeOrder.setAutoWithdrawDealStatus(EFundAutoWithdrawDealStatus.SUCCESS.name());
		rechargeOrder.setAccountValue(rechargeOrder.getApplyValue());
		rechargeOrder.setUpdateAdmin(rechargeOrder.getLockAdmin());
		rechargeOrder.setUpdateDate(new Date());
		this.updateByPrimaryKeySelective(rechargeOrder);
		WithdrawAutoLog query = new WithdrawAutoLog();
		query.setSerialNumber(orderNum);
		List<WithdrawAutoLog> withdrawAutoLog = withdrawAutoLogService.getWithdrawAutoLogs(query);
		if (withdrawAutoLog != null && withdrawAutoLog.size() > 0) {
			WithdrawAutoLog autoLog = withdrawAutoLog.get(0);
			autoLog.setDealStatus(EWithdrawAutoStatus.SUCCESS.name());
			autoLog.setUpdatedDate(new Date());
			withdrawAutoLogService.updateByPrimaryKeySelective(autoLog);
		}
		Notes notes = new Notes();
		notes.setParentId(0l);
		notes.setEnabled(1);
		notes.setFromUserName(ConstantUtil.SYSTEM_OPERATE_NAME);
		notes.setToUserName(rechargeOrder.getUserName());
		notes.setSub("[取现申请]");
		notes.setBody("恭喜您,您的取现申请已经成功提交,申请取现的资金是:" + rechargeOrder.getApplyValue().setScale(2, BigDecimal.ROUND_HALF_UP));
		notes.setType(ENoteType.SYSTEM.name());
		notes.setStatus(ENoteStatus.NO_READ.name());
		notes.setCreatedDate(new Date());
		notes.setBizSystem(rechargeOrder.getBizSystem());
		notes.setToUserId(rechargeOrder.getUserId());
		notesService.insertSelective(notes);
		return true;
	}

	/**
	 * 获取最近的提现订单号.
	 *
	 * @param query
	 * @return
	 */
	public String getUptodateSerialNumber(RechargeWithDrawOrderQuery query) {
		return withdrawOrderMapper.getUptodateSerialNumber(query);
	}

	/**
	 * 星支付自动出款，不需要审核
	 * @param withdrawOrder
	 * @param chargePay
	 * @throws Exception
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void setWithDrawAutoOrdersPaySuccessByStart(WithdrawOrder withdrawOrder, ChargePay chargePay) throws Exception {
		try {
			// 商户公共密钥
			String merNo = chargePay.getMemberId();
			Map<String, String> metaSignMap = new HashMap<>();

			metaSignMap.put("amount", withdrawOrder.getApplyValue().toString());
			metaSignMap.put("bankbranchname", withdrawOrder.getSubbranchName());
			metaSignMap.put("bankcode", withdrawOrder.getBankType());
			metaSignMap.put("bankname", withdrawOrder.getBankName());

			metaSignMap.put("cardname", withdrawOrder.getBankAccountName());
			metaSignMap.put("cardno",withdrawOrder.getBankCardNum());
			metaSignMap.put("cardtype", "1");
			metaSignMap.put("channel", "bank");
			metaSignMap.put("currency", "CNY");
			metaSignMap.put("notifyurl",chargePay.getNotifyUrl());
			metaSignMap.put("merchantid",withdrawOrder.getSerialNumber());
			metaSignMap.put("mid", chargePay.getMemberId());
			metaSignMap.put("service", "Withdraw");




			/*metaSignMap.put("service",    "Withdraw");
			metaSignMap.put("mid",        chargePay.getMemberId());
			metaSignMap.put("merchantid",withdrawOrder.getSerialNumber());
			metaSignMap.put("amount",     withdrawOrder.getApplyValue().toString());
			metaSignMap.put("currency",   "CNY");
			metaSignMap.put("channel",    "bank");
			metaSignMap.put("cardno",     "6217923552672809");
			metaSignMap.put("cardname",   "李世民");
			metaSignMap.put("cardtype",   "1");
			metaSignMap.put("bankcode",   "ICBC");
			metaSignMap.put("bankname",    "中国工商银行");
			metaSignMap.put("bankbranchname", "");
			metaSignMap.put("bankprovince", "河北省");
			metaSignMap.put("bankcity",     "石家庄市");
			metaSignMap.put("notifyurl",    "http://xxxx/callback");
*/





			SortedSet<String> parameter = new TreeSet<>(metaSignMap.keySet());
			String signstring = "";
			for (String key : parameter) {
				signstring += key + "=" + metaSignMap.get(key) + "&";
			}
			signstring +=chargePay.getSign();

			String sign = sign(signstring);

			logger.info("rsaStr:" + sign);
			metaSignMap.put("sign",sign);

			String resultJsonStr = HttpClientUtil.doPost(chargePay.getAddress(), metaSignMap);

			logger.info("星支付自动出款返回信息:" + resultJsonStr);
			// 检查状态
			com.alibaba.fastjson.JSONObject jsonObject = com.alibaba.fastjson.JSONObject.parseObject(resultJsonStr);
			String result = jsonObject.get("code").toString();
			if (!result.equals("0000")) {
				WithdrawAutoLog withdrawAutoLog = new WithdrawAutoLog();
				withdrawAutoLog.setAdminName(withdrawOrder.getLockAdmin());
				withdrawAutoLog.setBizSystem(withdrawOrder.getBizSystem());
				withdrawAutoLog.setBankAccountName(withdrawOrder.getBankAccountName());
				withdrawAutoLog.setBankCardNum(withdrawOrder.getBankCardNum());
				withdrawAutoLog.setBankType(withdrawOrder.getBankType());
				withdrawAutoLog.setBankName(withdrawOrder.getBankName());
				ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("CHENGXINTONG");
				withdrawAutoLog.setChargeDes(thirdPayConfig.getChargeDes());
				withdrawAutoLog.setChargeType(thirdPayConfig.getChargeType());
				withdrawAutoLog.setCreatedDate(new Date());
				withdrawAutoLog.setDealStatus(EWithdrawAutoStatus.SENDING.name());
				withdrawAutoLog.setMemberId(merNo);
				withdrawAutoLog.setSerialNumber(withdrawOrder.getSerialNumber());
				withdrawAutoLog.setType(EWithdrawAutoType.AUTO.name());
				withdrawAutoLog.setUserName(withdrawOrder.getUserName());
				withdrawAutoLog.setUserId(withdrawOrder.getUserId());
				withdrawAutoLog.setWithdrowOrderId(withdrawOrder.getId());
				withdrawAutoLog.setMoney(withdrawOrder.getApplyValue());
				withdrawAutoLogService.insertSelective(withdrawAutoLog);
				withdrawOrder.setAutoWithdrawDealStatus(EFundAutoWithdrawDealStatus.DEALING.name());
				this.updateByPrimaryKey(withdrawOrder);
			} else {
				logger.info("自动出款返回错误信息:", jsonObject.toString());
			}
		} catch (Exception e) {
			logger.error("自动出款错误:", e);
			throw new Exception("自动出款错误");

		}

	}

	public static void main(String[] args) throws Exception {
		String sign = sign("amount=100&bankbranchname=&bankcity=石家庄市&bankcode=ICBC&bankname=中国工商银行&bankprovince=河北省&cardname=李世民&cardno=6217923552672809&cardtype=1&channel=bank&currency=CNY&merchantid=TX2008132121577831000&mid=1014&notifyurl=http://xxxx/callback&service=Withdraw&3544FD7F9459FFEFBAF6DA79C4CBEFE8");
		System.out.println(sign);

	}

	public static String encrypt( String str, String privateKeyContent ) throws Exception{
		//base64编码的公钥
		privateKeyContent = privateKeyContent.replaceAll("\n", "").replace("-----BEGIN PRIVATE KEY-----", "").replace("-----END PRIVATE KEY-----", "");
		KeyFactory kf = KeyFactory.getInstance("RSA");
		PKCS8EncodedKeySpec keySpecPKCS8 = new PKCS8EncodedKeySpec(java.util.Base64.getDecoder().decode(privateKeyContent));
		PrivateKey privKey = kf.generatePrivate(keySpecPKCS8);

		Signature rsa = Signature.getInstance("SHA1withRSA");
		rsa.initSign(privKey);
		rsa.update(str.getBytes("UTF-8"));
		return java.util.Base64.getEncoder().encodeToString(rsa.sign());


	}

	public static String sign(String data) throws Exception {

		//String signstring ="amount=100&bankbranchname=&bankcity=石家庄市&bankcode=ICBC&bankname=中国工商银行&bankprovince=河北省&cardname=李世民&cardno=6217923552672809&cardtype=1&channel=bank&currency=CNY&merchantid=TX2008132121577831000&mid=1014&notifyurl=http://xxxx/callback&service=Withdraw&3544FD7F9459FFEFBAF6DA79C4CBEFE8";
		File f = new File("/root/gmstone_rsa_pkcs8.pem");
		FileInputStream fis = new FileInputStream(f);
		DataInputStream dis = new DataInputStream(fis);
		byte[] keyBytes = new byte[(int) f.length()];
		dis.readFully(keyBytes);
		dis.close();


		String temp = new String(keyBytes);
		String privKeyPEM = temp.replace("-----BEGIN PRIVATE KEY-----\n", "");
		privKeyPEM = privKeyPEM.replace("-----END PRIVATE KEY-----", "");


		/*Base64 b64 = new Base64();
		byte [] decoded = b64.decode(privKeyPEM);

		PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(decoded);
		KeyFactory kf = KeyFactory.getInstance("RSA");


		Signature rsa = Signature.getInstance("SHA1withRSA");
		rsa.initSign(kf.generatePrivate(spec));
		rsa.update(data.getBytes());
		byte[] signByte = rsa.sign();
*/
		String sign=encrypt(data,privKeyPEM);//java.util.Base64.getEncoder().encodeToString(signByte);
		//String sign = RSA.sign(data, privKeyPEM);
		return sign;
	}


	/**
	 * 自动出款(不用后台审核!直接提交第三方)
	 *
	 * @param
	 * @param withdrawOrder
	 * @return
	 * @throws Exception
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void setWithDrawAutoOrdersPaySuccess(WithdrawOrder withdrawOrder, ChargePay chargePay) throws Exception {
		try {
			// MD5加密串
			String key = chargePay.getSign();
			// 商户公共密钥
			String publicKey = chargePay.getPublickey();
			String merNo = chargePay.getMemberId();
			String callBackUrl = chargePay.getReturnUrl();
			Map<String, String> metaSignMap = new TreeMap<String, String>();
			metaSignMap.put("orderNum", withdrawOrder.getSerialNumber());
			metaSignMap.put("version", "V3.1.0.0");
			metaSignMap.put("charset", "UTF-8");
			// 银行代码、商户号、银行账户开户名、卡号、金额（分）、支付结果通知地址
			String bankCode = "";
			QuickBankType queryBankType = new QuickBankType();
			queryBankType.setChargeType(chargePay.getChargeType());
			queryBankType.setBankType(withdrawOrder.getBankType());
			List<QuickBankType> QuickBankTypeList = quickBankTypeService.getQuickBankTypeQuery(queryBankType);
			if (QuickBankTypeList != null && QuickBankTypeList.size() == 0) {
				throw new Exception("该银行类型不支持自动出款请联系客服!");
			} else {
				bankCode = QuickBankTypeList.get(0).getPayId();
			}
			metaSignMap.put("bankCode", bankCode);
			metaSignMap.put("merNo", merNo);
			metaSignMap.put("bankAccountName", withdrawOrder.getBankAccountName());
			metaSignMap.put("bankAccountNo", withdrawOrder.getBankCardNum());
			// 使用分进行提交
			String OrderMoney = String.valueOf(withdrawOrder.getApplyValue().multiply(new BigDecimal(100)).intValue());
			metaSignMap.put("amount", OrderMoney);
			metaSignMap.put("callBackUrl", callBackUrl);

			// 生成参数加入参数列表
			String metaSignJsonStr = ToolKit.mapToJson(metaSignMap);
			String sign = ToolKit.MD5(metaSignJsonStr + key, metaSignMap.get("charset"));
			metaSignMap.put("sign", sign);
			// ClassLoader classLoader =
			// Thread.currentThread().getContextClassLoader();
			// InputStream inputStream =
			// classLoader.getResourceAsStream("config.properties");
			// 加载属性列表
			// 数据公钥加密
			logger.info("publicKey:" + publicKey);
			logger.info("metaSignMap:" + metaSignMap);
			byte[] dataStr = ToolKit.encryptByPublicKey(ToolKit.mapToJson(metaSignMap).getBytes("UTF-8"), publicKey);
			logger.info("dataStr:" + dataStr);
			String param = Base64.encodeBase64String(dataStr);
			logger.info("param:" + param);
			String data = URLEncoder.encode(param, "UTF-8");
			String url = chargePay.getAddress();
			// 发起代付请求
			String resultJsonStr = ToolKit.request(url,
					"data=" + data + "&merchNo=" + metaSignMap.get("merNo") + "&version=" + metaSignMap.get("version"));
			// 检查状态
			JSONObject resultJsonObj = JSONObject.fromObject(resultJsonStr);
			String stateCode = resultJsonObj.getString("stateCode");
			if (stateCode.equals("00")) {
				WithdrawAutoLog withdrawAutoLog = new WithdrawAutoLog();
				withdrawAutoLog.setAdminName(withdrawOrder.getLockAdmin());
				withdrawAutoLog.setBizSystem(withdrawOrder.getBizSystem());
				withdrawAutoLog.setBankAccountName(withdrawOrder.getBankAccountName());
				withdrawAutoLog.setBankCardNum(withdrawOrder.getBankCardNum());
				withdrawAutoLog.setBankType(withdrawOrder.getBankType());
				withdrawAutoLog.setBankName(withdrawOrder.getBankName());
				// chargeType与chargeDes两属性暂时先写死以后字段扩展之后在修改
				/*
				 * withdrawAutoLog.setChargeDes(EFundThirdPayType.JIUTONG.
				 * getDescription());
				 * withdrawAutoLog.setChargeType(EFundThirdPayType
				 * .JIUTONG.name());
				 */
				ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("JIUTONG");
				withdrawAutoLog.setChargeDes(thirdPayConfig.getChargeDes());
				withdrawAutoLog.setChargeType(thirdPayConfig.getChargeType());
				withdrawAutoLog.setCreatedDate(new Date());
				withdrawAutoLog.setDealStatus(EWithdrawAutoStatus.SENDING.name());
				withdrawAutoLog.setMemberId(merNo);
				withdrawAutoLog.setSerialNumber(withdrawOrder.getSerialNumber());
				withdrawAutoLog.setType(EWithdrawAutoType.AUTO.name());
				withdrawAutoLog.setUserName(withdrawOrder.getUserName());
				withdrawAutoLog.setUserId(withdrawOrder.getUserId());
				withdrawAutoLog.setWithdrowOrderId(withdrawOrder.getId());
				withdrawAutoLog.setMoney(withdrawOrder.getApplyValue());
				withdrawAutoLogService.insertSelective(withdrawAutoLog);
				withdrawOrder.setAutoWithdrawDealStatus(EFundAutoWithdrawDealStatus.DEALING.name());
				this.updateByPrimaryKey(withdrawOrder);
			} else {
				logger.info("自动出款返回错误信息:", resultJsonObj.getString("msg"));
			}
		} catch (Exception e) {
			logger.error("自动出款错误:", e);
			throw new Exception("自动出款错误");

		}

	}


	/**
	 * 自动出款(不用后台审核!直接提交第三方)
	 *
	 * @param
	 * @param withdrawOrder
	 * @return
	 * @throws Exception
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void setWithDrawAutoOrdersPaySuccessByChengXinTong(WithdrawOrder withdrawOrder, ChargePay chargePay) throws Exception {
		try {
			// MD5加密串
			String key = chargePay.getSign();
			// 商户公共密钥
			String publicKey = chargePay.getPublickey();
			String merNo = chargePay.getMemberId();
			String callBackUrl = chargePay.getReturnUrl();
			Map<String, String> metaSignMap = new TreeMap<String, String>();
			metaSignMap.put("version", "1.0");
			metaSignMap.put("partner", merNo);
			metaSignMap.put("bankType", "BANK");
			metaSignMap.put("paymoney", withdrawOrder.getApplyValue().toString());
			metaSignMap.put("bankAccount", withdrawOrder.getBankCardNum());
			metaSignMap.put("bankName", withdrawOrder.getBankName());
			metaSignMap.put("bankUserName", withdrawOrder.getBankAccountName());
			metaSignMap.put("tradeCode", withdrawOrder.getSerialNumber());
			metaSignMap.put("timestamp", String.valueOf(System.currentTimeMillis()));
			metaSignMap.put("callbackurl", callBackUrl);
			String Md5key = chargePay.getSign();
			String md5Str = "version=" + metaSignMap.get("version") + "&partner=" + metaSignMap.get("partner") + "&bankType="
					+ metaSignMap.get("bankType") + "&paymoney=" + metaSignMap.get("paymoney") + "&bankAccount="
					+ metaSignMap.get("bankAccount") + "&bankName=" + metaSignMap.get("bankName") + "&bankUserName="
					+ metaSignMap.get("bankUserName") + "&tradeCode=" + metaSignMap.get("tradeCode") + "&timestamp="
					+ metaSignMap.get("timestamp") + "&callbackurl=" + metaSignMap.get("callbackurl");
			// String md5 =new String();//MD5签名格式
			logger.info("md5Str:" + md5Str + Md5key);
			String Signature = DigestUtils.md5Hex(md5Str + Md5key);
			metaSignMap.put("sign", Signature);
			md5Str = md5Str + "&sign=" + metaSignMap.get("sign");

			String sBuilder = "";
			Collection<String> keysetFrom = metaSignMap.keySet();
			List listForm = new ArrayList<String>(keysetFrom);
			for (int i = 0; i < listForm.size(); i++) {
				if (i == 0) {
					sBuilder += listForm.get(i) + "=" + metaSignMap.get(listForm.get(i)).toString();
				} else {
					String str = "&" + listForm.get(i) + "=" + metaSignMap.get(listForm.get(i)).toString();
					sBuilder += str;

				}
			}

			String resultJsonStr = HttpClientUtil.doPost(chargePay.getAddress(), metaSignMap);

			logger.info("诚信通自动出款返回信息:" + resultJsonStr);
			// 检查状态
			JSONObject resultJsonObj = JSONObject.fromObject(resultJsonStr);
			String success = resultJsonObj.getString("success");
			if (success.equals("00")) {
				WithdrawAutoLog withdrawAutoLog = new WithdrawAutoLog();
				withdrawAutoLog.setAdminName(withdrawOrder.getLockAdmin());
				withdrawAutoLog.setBizSystem(withdrawOrder.getBizSystem());
				withdrawAutoLog.setBankAccountName(withdrawOrder.getBankAccountName());
				withdrawAutoLog.setBankCardNum(withdrawOrder.getBankCardNum());
				withdrawAutoLog.setBankType(withdrawOrder.getBankType());
				withdrawAutoLog.setBankName(withdrawOrder.getBankName());
				ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("CHENGXINTONG");
				withdrawAutoLog.setChargeDes(thirdPayConfig.getChargeDes());
				withdrawAutoLog.setChargeType(thirdPayConfig.getChargeType());
				withdrawAutoLog.setCreatedDate(new Date());
				withdrawAutoLog.setDealStatus(EWithdrawAutoStatus.SENDING.name());
				withdrawAutoLog.setMemberId(merNo);
				withdrawAutoLog.setSerialNumber(withdrawOrder.getSerialNumber());
				withdrawAutoLog.setType(EWithdrawAutoType.AUTO.name());
				withdrawAutoLog.setUserName(withdrawOrder.getUserName());
				withdrawAutoLog.setUserId(withdrawOrder.getUserId());
				withdrawAutoLog.setWithdrowOrderId(withdrawOrder.getId());
				withdrawAutoLog.setMoney(withdrawOrder.getApplyValue());
				withdrawAutoLogService.insertSelective(withdrawAutoLog);
				withdrawOrder.setAutoWithdrawDealStatus(EFundAutoWithdrawDealStatus.DEALING.name());
				this.updateByPrimaryKey(withdrawOrder);
			} else {
				logger.info("自动出款返回错误信息:", resultJsonObj.getString("msg"));
			}
		} catch (Exception e) {
			logger.error("自动出款错误:", e);
			throw new Exception("自动出款错误");

		}

	}

	/**
	 * 自动取现申请锁定
	 *
	 * @param rechargeId
	 * @throws Exception
	 */
	public void setAutoWithDrawOrdersLocked(WithdrawOrder rechargeOrder, WithdrawAutoConfig config) throws Exception {
		if (rechargeOrder == null) {
			throw new Exception("该订单不存在.");
		}
		if (rechargeOrder.getDealStatus().equals(EFundWithDrawStatus.WITHDRAW_SUCCESS.name())) {
			throw new Exception("该订单已经取现成功了,无法锁定.");
		}
		if (rechargeOrder.getDealStatus().equals(EFundWithDrawStatus.WITHDRAW_FAIL.name())) {
			throw new Exception("该订单已经取现关闭了,无法锁定.");
		}
		logger.info("管理员[" + config.getDealAdminUsername() + "]锁定订单流水号[" + rechargeOrder.getSerialNumber() + "]取现订单");
		rechargeOrder.setDealStatus(EFundWithDrawStatus.LOCKED.name());
		rechargeOrder.setLockStatus(WithdrawOrder.LOCKED);
		rechargeOrder.setLockAdmin(config.getDealAdminUsername());
		rechargeOrder.setUpdateAdmin(config.getDealAdminUsername());
		rechargeOrder.setUpdateDate(new Date());
		this.updateByPrimaryKeySelective(rechargeOrder);

	}

	/**
	 * 取现申请订单自动关闭
	 *
	 * @param rechargeId
	 * @throws Exception
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void setAutoWithDrawOrdersPayClose(WithdrawOrder rechargeOrder, WithdrawAutoConfig config) throws UnEqualVersionException,
			Exception {
		if (rechargeOrder == null) {
			throw new Exception("该订单不存在.");
		}

		if (rechargeOrder.getDealStatus().equals(EFundWithDrawStatus.WITHDRAW_SUCCESS.name())) {
			throw new Exception("该订单已经取现成功了,无法操作");
		}
		if (rechargeOrder.getDealStatus().equals(EFundWithDrawStatus.WITHDRAW_FAIL.name())) {
			throw new Exception("该订单已经取现关闭了,无法操作");
		}

		logger.info("管理员[" + config.getDealAdminUsername() + "将订单流水号[" + rechargeOrder.getSerialNumber() + "]取现申请关闭");
		rechargeOrder.setDealStatus(EFundWithDrawStatus.WITHDRAW_FAIL.name());
		rechargeOrder.setAutoWithdrawDealStatus(EFundAutoWithdrawDealStatus.FAIL.name());
		rechargeOrder.setUpdateAdmin(config.getDealAdminUsername());
		rechargeOrder.setUpdateDate(new Date());
		this.updateByPrimaryKeySelective(rechargeOrder);

		User currentUser = userService.selectByPrimaryKey(rechargeOrder.getUserId());
		List<UserTotalMoneyByTypeVo> userTotalMoneyByTypeVos = new ArrayList<UserTotalMoneyByTypeVo>();
		if (currentUser != null) {
			// 将钱退回去
			MoneyDetail moneyDetail = new MoneyDetail();
			moneyDetail.setUserId(currentUser.getId());
			moneyDetail.setOrderId(rechargeOrder.getId());
			moneyDetail.setOperateUserName(config.getDealAdminUsername()); // 操作人员
			moneyDetail.setUserName(currentUser.getUserName()); // 用户名
			moneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.TX)); // 订单单号
			moneyDetail.setExpect(""); // 哪个期号
			moneyDetail.setLotteryType(""); // 彩种
			moneyDetail.setLotteryTypeDes(""); // 彩种名称
			moneyDetail.setDetailType(EMoneyDetailType.WITHDRAW.name()); // 资金类型为投注
			moneyDetail.setIncome(rechargeOrder.getApplyValue()); // 收入为0
			moneyDetail.setPay(new BigDecimal(0)); // 支出金额
			moneyDetail.setBalanceBefore(currentUser.getMoney()); // 支出前金额
			moneyDetail.setBalanceAfter(currentUser.getMoney().add(rechargeOrder.getApplyValue())); // 支出后的金额
			moneyDetail.setWinLevel(null);
			moneyDetail.setWinCost(null);
			moneyDetail.setWinCostRule(null);
			moneyDetail.setDescription("取现订单关闭:" + rechargeOrder.getApplyValue());
			moneyDetail.setBizSystem(currentUser.getBizSystem());
			moneyDetailService.insertSelective(moneyDetail);

			// 提现关闭归还增加用户余额
			currentUser.addMoney(rechargeOrder.getApplyValue(), false);
			// 提现关闭归还当时使用的可提现额度
			currentUser.addCanWithdrawMoney(rechargeOrder.getCurrentCanWithdrawMoney());
			// 归还扣减总提现金额，负值
			BigDecimal reduceApplyValue = rechargeOrder.getApplyValue().multiply(new BigDecimal("-1"));
			// 针对盈亏报表多线程不会事物回滚处理方式
			UserTotalMoneyByTypeVo userTotalMoneyByTypeVo = new UserTotalMoneyByTypeVo(currentUser, EMoneyDetailType.WITHDRAW,
					reduceApplyValue);
			userTotalMoneyByTypeVos.add(userTotalMoneyByTypeVo);

			// 手续费记录
			if (rechargeOrder.getFeeValue() != null && rechargeOrder.getFeeValue().compareTo(new BigDecimal(0)) > 0) {
				// 手续费资金明细
				MoneyDetail moneyDetailFee = new MoneyDetail();
				moneyDetailFee.setUserId(currentUser.getId());
				moneyDetailFee.setOrderId(rechargeOrder.getId());
				moneyDetailFee.setOperateUserName(config.getDealAdminUsername()); // 操作人员
				moneyDetailFee.setUserName(currentUser.getUserName()); // 用户名
				moneyDetailFee.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.TF)); // 订单单号
				moneyDetailFee.setExpect(""); // 哪个期号
				moneyDetailFee.setLotteryType(""); // 彩种
				moneyDetailFee.setLotteryTypeDes(""); // 彩种名称
				moneyDetailFee.setDetailType(EMoneyDetailType.WITHDRAW_FEE.name()); // 资金类型为投注
				moneyDetailFee.setIncome(rechargeOrder.getFeeValue()); // 收入为0
				moneyDetailFee.setPay(new BigDecimal(0)); // 支出金额
				moneyDetailFee.setBalanceBefore(currentUser.getMoney()); // 支出前金额
				moneyDetailFee.setBalanceAfter(currentUser.getMoney().add(rechargeOrder.getFeeValue())); // 支出后的金额
				moneyDetailFee.setWinLevel(null);
				moneyDetailFee.setWinCost(null);
				moneyDetailFee.setWinCostRule(null);
				moneyDetailFee.setDescription("返还取现手续费:" + rechargeOrder.getFeeValue());
				moneyDetailFee.setBizSystem(currentUser.getBizSystem());
				moneyDetailService.insertSelective(moneyDetailFee);

				// 归还手续费
				currentUser.addMoney(rechargeOrder.getFeeValue(), false);
				// 归还扣减总提现手续费，负值
				BigDecimal reduceFeeValue = rechargeOrder.getFeeValue().multiply(new BigDecimal("-1"));
				// 针对盈亏报表多线程不会事物回滚处理方式
				UserTotalMoneyByTypeVo userTotalMoneyByTypeVo1 = new UserTotalMoneyByTypeVo(currentUser, EMoneyDetailType.WITHDRAW_FEE,
						reduceFeeValue);
				userTotalMoneyByTypeVos.add(userTotalMoneyByTypeVo1);
			}

			// 取现申请成功系统消息
			Notes notes = new Notes();
			notes.setParentId(0l);
			notes.setEnabled(1);
			notes.setFromUserName(ConstantUtil.SYSTEM_OPERATE_NAME);
			notes.setToUserName(rechargeOrder.getUserName());
			notes.setSub("[取现申请]");
			notes.setBody("抱歉,您的取现申请订单号  " + rechargeOrder.getSerialNumber() + "  已经关闭。如果您有疑问,请联系我们的客服.");
			notes.setType(ENoteType.SYSTEM.name());
			notes.setStatus(ENoteStatus.NO_READ.name());
			notes.setCreatedDate(new Date());
			notes.setBizSystem(currentUser.getBizSystem());
			notes.setToUserId(currentUser.getId());
			notesService.insertSelective(notes);
			userService.updateByPrimaryKeyWithVersionSelective(currentUser);
		}
		// 循环遍历插入报表总额和盈亏报表
		for (UserTotalMoneyByTypeVo userTotalMoneyByTypeVo : userTotalMoneyByTypeVos) {
			userMoneyTotalService.addUserTotalMoneyByType(userTotalMoneyByTypeVo.getUser(), userTotalMoneyByTypeVo.getDetailType(),
					userTotalMoneyByTypeVo.getOperationValue());
		}
	}

	/**
	 * 查询当前用户的今天的提现总额.
	 *
	 * @param query
	 * @return
	 */
	public WithdrawOrderCount getUserApplyValueCountTodayWithDrawOrder(RechargeWithDrawOrderQuery query) {
		return withdrawOrderMapper.getUserApplyValueCountTodayWithDrawOrder(query);
	}

	/**
	 * 查询当前用户当天的提现次数
	 *
	 * @param query
	 * @return
	 */
	public Integer countUserWithdrawByDay(RechargeWithDrawOrderQuery query) {
		return withdrawOrderMapper.countUserWithdrawByDay(query);
	}

}
