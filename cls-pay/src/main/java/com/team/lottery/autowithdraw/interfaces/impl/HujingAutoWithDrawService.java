
package com.team.lottery.autowithdraw.interfaces.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.map.HashedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.autowithdraw.interfaces.IAutoWithDrawService;
import com.team.lottery.autowithdraw.result.ResultForAutoWithDraw;
import com.team.lottery.enums.EFundAutoWithdrawDealStatus;
import com.team.lottery.enums.EWithdrawAutoStatus;
import com.team.lottery.enums.EWithdrawAutoType;
import com.team.lottery.pay.model.hujingpay.RSAUtil;
import com.team.lottery.service.QuickBankTypeService;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.service.UserBankService;
import com.team.lottery.service.WithdrawAutoLogService;
import com.team.lottery.service.WithdrawOrderService;
import com.team.lottery.util.HttpClientUtil;
import com.team.lottery.util.JSONUtils;
import com.team.lottery.vo.ChargePay;
import com.team.lottery.vo.ThirdPayConfig;
import com.team.lottery.vo.UserBank;
import com.team.lottery.vo.WithdrawAutoLog;
import com.team.lottery.vo.WithdrawOrder;

import net.sf.json.JSONObject;

/** 
 * @Description: 虎鲸自动出款实对象.
 * @Author: Jamine.
 * @CreateDate：2019-05-18 03:25:54.
 */
@Service("HujingAutoWithDrawService")
public class HujingAutoWithDrawService implements IAutoWithDrawService {
	private static Logger logger = LoggerFactory.getLogger(HujingAutoWithDrawService.class);
	@Autowired
	private ThirdPayConfigService thirdPayConfigService;
	@Autowired
	private WithdrawAutoLogService withdrawAutoLogService;
	@Autowired
	private WithdrawOrderService withdrawOrderService;
	@Autowired
	private QuickBankTypeService quickBankTypeService;
	@Autowired
	private UserBankService userBankService;

	/**
	 * 虎鲸自动出款实现.
	 * @param withdrawOrder
	 * @param chargePay
	 * @throws Exception
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	@Override
	public ResultForAutoWithDraw autoWithDraw(WithdrawOrder withdrawOrder, ChargePay chargePay) {
		ResultForAutoWithDraw result = null;
		try {

			
			String merchantId = chargePay.getMemberId();
			// 代付的通知入账回调地址
			String notifyUrl = chargePay.getReturnUrl();
			// 代付类型,写死，只有一个
			String payType = "alipay";
			// 订单号
			String merchantOrderId = withdrawOrder.getSerialNumber();
			String merchantUid = String.valueOf(withdrawOrder.getUserId());
			// 商家订单时间戳
			String timestamp = String.valueOf(System.currentTimeMillis());
			// 代付订单金额
			String money = String.valueOf(withdrawOrder.getApplyValue());
			// 备注信息
			String mark = "";
			// 银行卡账号（持卡人）
			String accountName = withdrawOrder.getBankAccountName();
			// 银行卡卡号
			String cardNo = withdrawOrder.getBankCardNum();
			
			
			String bankCity = "";
            String province = "";
			// 查询用户银行卡表获取省份城市
			List<UserBank> userBanks = userBankService.getAllUserBanksByUserId(withdrawOrder.getUserId());
			if(CollectionUtils.isNotEmpty(userBanks)) {
			    for(UserBank userBank : userBanks) {
			        // 银行卡卡号相等
			        if(userBank.getBankCardNum().equals(withdrawOrder.getBankCardNum())) {
			            province = userBank.getProvince();
			            bankCity = userBank.getCity();
			        }
			    }
			}
			
			
			// 银行名称
            String bankName = withdrawOrder.getBankName();
//			QuickBankType queryBankType = new QuickBankType();
//			queryBankType.setChargeType(chargePay.getChargeType());
//			queryBankType.setBankType(withdrawOrder.getBankType());
//			List<QuickBankType> QuickBankTypeList = quickBankTypeService.getQuickBankTypeQuery(queryBankType);
//			if (QuickBankTypeList != null && QuickBankTypeList.size() == 0) {
//				result = new ResultForAutoWithDraw();
//				result.setStatus(false);
//				result.setMessage("该银行类型不支持自动出款请联系客服!");
//				return result;
//			} else {
//			    bankName = QuickBankTypeList.get(0).getPayId();
//			}
			
			
			// 获取秘钥.
			String key = chargePay.getSign();
			String markChar = "&";
			// MD5加密.
			String rsaStr = "merchantId=" + merchantId + markChar + 
							"merchantOrderId=" + merchantOrderId + markChar + 
							"merchantUid=" + merchantUid + markChar + 
							"money=" + money + markChar + 
							"notifyUrl=" + notifyUrl + markChar + 
							"payType=" + payType + markChar + 
							"timestamp=" + timestamp;
			
			String sign = "";
			try {
				logger.info("私钥是[{}]", chargePay.getPrivatekey());
				sign = RSAUtil.rsaSign(chargePay.getPrivatekey(), rsaStr);
			} catch (Exception e) {
				logger.error("签名失败", e);
			}
			
			logger.info("虎鲸自动出款RSA加密前字符串为:" + rsaStr + "加密后签名为:" + sign);

			// json提交post表单.
			Map<String, String> dataMap = new HashMap<String, String>();
			dataMap.put("merchantId", merchantId);
			dataMap.put("notifyUrl", notifyUrl);
			dataMap.put("sign", sign);
			dataMap.put("payType", payType);
			dataMap.put("merchantOrderId", merchantOrderId);
			dataMap.put("merchantUid", merchantUid);
			dataMap.put("timestamp", timestamp);
			dataMap.put("money", money);
			dataMap.put("mark", mark);
			dataMap.put("accountName", accountName);
			dataMap.put("cardNo", cardNo);
			dataMap.put("bankCity", bankCity);
			dataMap.put("province", province);
			dataMap.put("bankName", bankName);
			
			// 代付用的网关地址
			String url = chargePay.getAddress();
			String doJsonForPost = HttpClientUtil.doPost(url, dataMap);
			logger.info("虎鲸自动出款返回信息:" + doJsonForPost);
			Boolean isJson = JSONUtils.isJson(doJsonForPost);

			if (isJson) {
				JSONObject jsonObject = JSONObject.fromObject(doJsonForPost);
				String status = jsonObject.getString("status");
				String message = jsonObject.getString("message");
				// 下单状态 200成功 5000失败
				if ("200".equals(status)) {
					WithdrawAutoLog withdrawAutoLog = new WithdrawAutoLog();
					withdrawAutoLog.setAdminName(withdrawOrder.getLockAdmin());
					withdrawAutoLog.setBizSystem(withdrawOrder.getBizSystem());
					withdrawAutoLog.setBankAccountName(withdrawOrder.getBankAccountName());
					withdrawAutoLog.setBankCardNum(withdrawOrder.getBankCardNum());
					withdrawAutoLog.setBankType(withdrawOrder.getBankType());
					withdrawAutoLog.setBankName(withdrawOrder.getBankName());
					ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("HUJINDPAY");
					withdrawAutoLog.setChargeDes(thirdPayConfig.getChargeDes());
					withdrawAutoLog.setChargeType(thirdPayConfig.getChargeType());
					withdrawAutoLog.setCreatedDate(new Date());
					withdrawAutoLog.setDealStatus(EWithdrawAutoStatus.SENDING.name());
					withdrawAutoLog.setMemberId(chargePay.getMemberId());
					withdrawAutoLog.setSerialNumber(withdrawOrder.getSerialNumber());
					withdrawAutoLog.setType(EWithdrawAutoType.AUTO.name());
					withdrawAutoLog.setUserName(withdrawOrder.getUserName());
					withdrawAutoLog.setUserId(withdrawOrder.getUserId());
					withdrawAutoLog.setWithdrowOrderId(withdrawOrder.getId());
					withdrawAutoLog.setMoney(withdrawOrder.getApplyValue());
					withdrawAutoLogService.insertSelective(withdrawAutoLog);
					withdrawOrder.setAutoWithdrawDealStatus(EFundAutoWithdrawDealStatus.DEALING.name());
					withdrawOrderService.updateByPrimaryKey(withdrawOrder);
					result = new ResultForAutoWithDraw();
					result.setStatus(true);
					result.setMessage("自动出款成功");
					return result;
				} else {
					logger.info("虎鲸自动出款失败status为:" + status);
					result = new ResultForAutoWithDraw();
					result.setStatus(false);
					result.setMessage("自动出款失败" + message);
					return result;
				}

			} else {
				logger.info("虎鲸自动出款返回对象不是JSON格式:" + doJsonForPost);
				result = new ResultForAutoWithDraw();
				result.setStatus(false);
				result.setMessage("虎鲸自动出款失败");
				return result;
			}
		} catch (Exception e) {
			logger.error("自动出款错误:", e);
			result = new ResultForAutoWithDraw();
			result.setStatus(false);
			result.setMessage("虎鲸自动出款出现异常");
			return result;
		}
	}
}
