
package com.team.lottery.autowithdraw.interfaces.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.autowithdraw.interfaces.IAutoWithDrawService;
import com.team.lottery.autowithdraw.result.ResultForAutoWithDraw;
import com.team.lottery.enums.EFundAutoWithdrawDealStatus;
import com.team.lottery.enums.EWithdrawAutoStatus;
import com.team.lottery.enums.EWithdrawAutoType;
import com.team.lottery.pay.util.WuyouAESUtil;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.service.WithdrawAutoLogService;
import com.team.lottery.service.WithdrawOrderService;
import com.team.lottery.util.HttpClientUtil;
import com.team.lottery.util.JSONUtils;
import com.team.lottery.vo.ChargePay;
import com.team.lottery.vo.ThirdPayConfig;
import com.team.lottery.vo.WithdrawAutoLog;
import com.team.lottery.vo.WithdrawOrder;

import net.sf.json.JSONObject;

/** 
 * @Description: 无忧自动出款实对象.
 * @Author: Jamine.
 * @CreateDate：2019-05-18 03:25:54.
 */
@Service("WuyouAutoWithDrawService")
public class WuyouAutoWithDrawService implements IAutoWithDrawService {
	private static Logger logger = LoggerFactory.getLogger(WuyouAutoWithDrawService.class);
	@Autowired
	private ThirdPayConfigService thirdPayConfigService;
	@Autowired
	private WithdrawAutoLogService withdrawAutoLogService;
	@Autowired
	private WithdrawOrderService withdrawOrderService;
//	@Autowired
//	private QuickBankTypeService quickBankTypeService;
//	@Autowired
//	private UserBankService userBankService;

	/**
	 * 无忧自动出款实现.
	 * @param withdrawOrder
	 * @param chargePay
	 * @throws Exception
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	@Override
	public ResultForAutoWithDraw autoWithDraw(WithdrawOrder withdrawOrder, ChargePay chargePay) {
		ResultForAutoWithDraw result = null;
		try {

			
			String firmNumber = chargePay.getMemberId();
			// 订单号
			String orderNumber = withdrawOrder.getSerialNumber();
			// 代付订单金额
			String money = String.valueOf(withdrawOrder.getApplyValue());
			// 银行卡账号（持卡人）
			String bankAcc = withdrawOrder.getBankAccountName();
			// 银行卡卡号
			String bankNumber = withdrawOrder.getBankCardNum();
			
			
//			String bankCity = "";
//            String province = "";
//			// 查询用户银行卡表获取省份城市
//			List<UserBank> userBanks = userBankService.getAllUserBanksByUserId(withdrawOrder.getUserId());
//			if(CollectionUtils.isNotEmpty(userBanks)) {
//			    for(UserBank userBank : userBanks) {
//			        // 银行卡卡号相等
//			        if(userBank.getBankCardNum().equals(withdrawOrder.getBankCardNum())) {
//			            province = userBank.getProvince();
//			            bankCity = userBank.getCity();
//			        }
//			    }
//			}
			
			// 银行名称
//            String bankName = withdrawOrder.getBankName();
//			QuickBankType queryBankType = new QuickBankType();
//			queryBankType.setChargeType(chargePay.getChargeType());
//			queryBankType.setBankType(withdrawOrder.getBankType());
//			List<QuickBankType> QuickBankTypeList = quickBankTypeService.getQuickBankTypeQuery(queryBankType);
//			if (QuickBankTypeList != null && QuickBankTypeList.size() == 0) {
//				result = new ResultForAutoWithDraw();
//				result.setStatus(false);
//				result.setMessage("该银行类型不支持自动出款请联系客服!");
//				return result;
//			} else {
//			    bankName = QuickBankTypeList.get(0).getPayId();
//			}
			
			
			// 获取秘钥.
			String aesKey = chargePay.getSign();
			String markChar = ",";
			// AES加密.
			String content = "{\"OrderNumber\":\"" + orderNumber + "\"" + markChar + 
							"\"Money\":\"" + money +"\"" + markChar + 
							"\"BankNumber\":\"" + bankNumber + "\"" + markChar + 
							"\"BankAcc\":\"" + bankAcc + "\"" +
							"}";
			
			String signature = "";
			try {
				logger.info("AES私钥是[{}]", chargePay.getSign());
				signature = WuyouAESUtil.encrypt(content, aesKey);
			} catch (Exception e) {
				logger.error("签名失败", e);
			}
			
			logger.info("无忧自动出款AES加密前字符串为:" + content + "加密后签名为:" + signature);

			// json提交post表单.
			Map<String, String> dataMap = new HashMap<String, String>();
			dataMap.put("FirmNumber", firmNumber);
			dataMap.put("Signature", signature);
			
			// 先插入日志防止重复发送
			WithdrawAutoLog withdrawAutoLog = new WithdrawAutoLog();
			withdrawAutoLog.setAdminName(withdrawOrder.getLockAdmin());
			withdrawAutoLog.setBizSystem(withdrawOrder.getBizSystem());
			withdrawAutoLog.setBankAccountName(withdrawOrder.getBankAccountName());
			withdrawAutoLog.setBankCardNum(withdrawOrder.getBankCardNum());
			withdrawAutoLog.setBankType(withdrawOrder.getBankType());
			withdrawAutoLog.setBankName(withdrawOrder.getBankName());
			ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("WUYOU");
			withdrawAutoLog.setChargeDes(thirdPayConfig.getChargeDes());
			withdrawAutoLog.setChargeType(thirdPayConfig.getChargeType());
			withdrawAutoLog.setCreatedDate(new Date());
			withdrawAutoLog.setDealStatus(EWithdrawAutoStatus.SENDED.name());
			withdrawAutoLog.setMemberId(chargePay.getMemberId());
			withdrawAutoLog.setSerialNumber(withdrawOrder.getSerialNumber());
			withdrawAutoLog.setType(EWithdrawAutoType.AUTO.name());
			withdrawAutoLog.setUserName(withdrawOrder.getUserName());
			withdrawAutoLog.setUserId(withdrawOrder.getUserId());
			withdrawAutoLog.setWithdrowOrderId(withdrawOrder.getId());
			withdrawAutoLog.setMoney(withdrawOrder.getApplyValue());
			
			try {
				withdrawAutoLogService.insertSelective(withdrawAutoLog);
			} catch (Exception e) {
				result = new ResultForAutoWithDraw();
				result.setStatus(false);
				result.setMessage("自动出款失败，代付申请已发送，请勿重复点击");
				return result;
			}
			
			// 代付用的网关地址
			String url = chargePay.getAddress();
			String doJsonForPost = HttpClientUtil.doPost(url, dataMap);
			logger.info("无忧自动出款返回信息:" + doJsonForPost);
			Boolean isJson = JSONUtils.isJson(doJsonForPost);

			if (isJson) {
				JSONObject jsonObject = JSONObject.fromObject(doJsonForPost);
				String msg = jsonObject.getString("msg");
				String message = jsonObject.getString("PlatformTicketNumber");
				// 下单状态 200成功 5000失败
				if ("代付成功".equals(msg)) {
					
					// 没有回调日志，直接更新出款订单为出款成功
					withdrawOrderService.setWithDrawOrdersAutoPaySuccess(withdrawOrder.getId(), String.valueOf(withdrawOrder.getApplyValue()));
					
					// 更新为成功
					withdrawAutoLog.setDealStatus(EWithdrawAutoStatus.SUCCESS.name());
					withdrawAutoLogService.updateByPrimaryKeySelective(withdrawAutoLog);
					
					
					result = new ResultForAutoWithDraw();
					result.setStatus(true);
					result.setMessage("自动出款成功");
					return result;
				} else {
					logger.info("无忧自动出款失败status为:" + msg);
					
					// 更新为失败
					withdrawAutoLog.setDealStatus(EWithdrawAutoStatus.FAIL.name());
					withdrawAutoLogService.updateByPrimaryKeySelective(withdrawAutoLog);
					
					result = new ResultForAutoWithDraw();
					result.setStatus(false);
					result.setMessage("自动出款失败" + msg);
					return result;
				}

			} else {
				logger.info("无忧自动出款返回对象不是JSON格式:" + doJsonForPost);
				result = new ResultForAutoWithDraw();
				result.setStatus(false);
				result.setMessage("无忧自动出款失败");
				return result;
			}
		} catch (Exception e) {
			logger.error("自动出款错误:", e);
			result = new ResultForAutoWithDraw();
			result.setStatus(false);
			result.setMessage("无忧自动出款出现异常");
			return result;
		}
	}
}
