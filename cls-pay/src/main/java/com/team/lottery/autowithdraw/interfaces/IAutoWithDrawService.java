
package com.team.lottery.autowithdraw.interfaces;

import com.team.lottery.autowithdraw.result.ResultForAutoWithDraw;
import com.team.lottery.vo.ChargePay;
import com.team.lottery.vo.WithdrawOrder;

/** 
 * @Description: 自动出款接口.
 * @Author: Jamine.
 * @CreateDate：2019-05-18 08:23:15.
 */
public interface IAutoWithDrawService {
	ResultForAutoWithDraw autoWithDraw(WithdrawOrder withdrawOrder, ChargePay chargePay);
}
