
package com.team.lottery.autowithdraw.interfaces.impl;

import com.team.lottery.autowithdraw.interfaces.IAutoWithDrawService;
import com.team.lottery.autowithdraw.result.ResultForAutoWithDraw;
import com.team.lottery.enums.EFundAutoWithdrawDealStatus;
import com.team.lottery.enums.EWithdrawAutoStatus;
import com.team.lottery.enums.EWithdrawAutoType;
import com.team.lottery.pay.util.WuyouAESUtil;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.service.WithdrawAutoLogService;
import com.team.lottery.service.WithdrawOrderService;
import com.team.lottery.util.HttpClientUtil;
import com.team.lottery.util.JSONUtils;
import com.team.lottery.vo.ChargePay;
import com.team.lottery.vo.ThirdPayConfig;
import com.team.lottery.vo.WithdrawAutoLog;
import com.team.lottery.vo.WithdrawOrder;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.security.KeyFactory;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/** 
 * @Description: 星支付自动出款实对象.
 * @Author: Eric.
 * @CreateDate：2019-05-18 03:25:54.
 */
@Service("StartAutoWithDrawService")
public class StartAutoWithDrawService implements IAutoWithDrawService {
	private static Logger logger = LoggerFactory.getLogger(StartAutoWithDrawService.class);
	@Autowired
	private ThirdPayConfigService thirdPayConfigService;
	@Autowired
	private WithdrawAutoLogService withdrawAutoLogService;
	@Autowired
	private WithdrawOrderService withdrawOrderService;
//	@Autowired
//	private QuickBankTypeService quickBankTypeService;
//	@Autowired
//	private UserBankService userBankService;

	/**
	 * 星支付自动出款实现.
	 * @param withdrawOrder
	 * @param chargePay
	 * @throws Exception
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	@Override
	public ResultForAutoWithDraw autoWithDraw(WithdrawOrder withdrawOrder, ChargePay chargePay) {
		ResultForAutoWithDraw result = null;
		try {


			// 商户公共密钥
			String merNo = chargePay.getMemberId();
			Map<String, String> metaSignMap = new TreeMap<String, String>();

			metaSignMap.put("amount", withdrawOrder.getApplyValue().toString());
			metaSignMap.put("bankbranchname", withdrawOrder.getSubbranchName());
			//metaSignMap.put("bankcity", withdrawOrder.getBankCity());
			metaSignMap.put("bankcode", withdrawOrder.getBankType());
			metaSignMap.put("bankname", withdrawOrder.getBankName());
			//metaSignMap.put("bankprovince", withdrawOrder.getBankProvince());
			metaSignMap.put("cardname", withdrawOrder.getBankAccountName());
			metaSignMap.put("cardno",withdrawOrder.getBankCardNum());
			metaSignMap.put("cardtype", "1");
			metaSignMap.put("channel", "alipay2");
			metaSignMap.put("currency", "CNY");
			metaSignMap.put("notifyurl",chargePay.getNotifyUrl());
			metaSignMap.put("merchantid",withdrawOrder.getSerialNumber());
			metaSignMap.put("mid", chargePay.getMemberId());
			metaSignMap.put("service", "Withdraw");

			String signstring = "";
			for (String key : metaSignMap.keySet()) {
				signstring += key + "=" + metaSignMap.get(key) + "&";
			}
			signstring +=chargePay.getSign();

			File f = new File("/root/gmstone_rsa_pkcs8.pem");
			FileInputStream fis = new FileInputStream(f);
			DataInputStream dis = new DataInputStream(fis);
			byte[] keyBytes = new byte[(int) f.length()];
			dis.readFully(keyBytes);
			dis.close();

			String temp = new String(keyBytes);
			String privKeyPEM = temp.replace("-----BEGIN PRIVATE KEY-----\n", "");
			privKeyPEM = privKeyPEM.replace("-----END PRIVATE KEY-----", "");


			Base64 b64 = new Base64();
			byte [] decoded = b64.decode(privKeyPEM);

			PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(decoded);
			KeyFactory kf = KeyFactory.getInstance("RSA");


			Signature rsa = Signature.getInstance("SHA1withRSA");
			rsa.initSign(kf.generatePrivate(spec));
			rsa.update(signstring.getBytes("UTF-8"));
			byte[] signByte = rsa.sign();

			String sign=java.util.Base64.getEncoder().encodeToString(signByte);

			metaSignMap.put("sign",sign);

			logger.info("星支付自动出款AES加密前字符串为:" + signstring + "加密后签名为:" + sign);

			// json提交post表单.

			// 先插入日志防止重复发送
			WithdrawAutoLog withdrawAutoLog = new WithdrawAutoLog();
			withdrawAutoLog.setAdminName(withdrawOrder.getLockAdmin());
			withdrawAutoLog.setBizSystem(withdrawOrder.getBizSystem());
			withdrawAutoLog.setBankAccountName(withdrawOrder.getBankAccountName());
			withdrawAutoLog.setBankCardNum(withdrawOrder.getBankCardNum());
			withdrawAutoLog.setBankType(withdrawOrder.getBankType());
			withdrawAutoLog.setBankName(withdrawOrder.getBankName());
			ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("STARTOUT");
			withdrawAutoLog.setChargeDes(thirdPayConfig.getChargeDes());
			withdrawAutoLog.setChargeType(thirdPayConfig.getChargeType());
			withdrawAutoLog.setCreatedDate(new Date());
			withdrawAutoLog.setDealStatus(EWithdrawAutoStatus.SENDED.name());
			withdrawAutoLog.setMemberId(chargePay.getMemberId());
			withdrawAutoLog.setSerialNumber(withdrawOrder.getSerialNumber());
			withdrawAutoLog.setType(EWithdrawAutoType.AUTO.name());
			withdrawAutoLog.setUserName(withdrawOrder.getUserName());
			withdrawAutoLog.setUserId(withdrawOrder.getUserId());
			withdrawAutoLog.setWithdrowOrderId(withdrawOrder.getId());
			withdrawAutoLog.setMoney(withdrawOrder.getApplyValue());

			try {
				withdrawAutoLogService.insertSelective(withdrawAutoLog);
			} catch (Exception e) {
				result = new ResultForAutoWithDraw();
				result.setStatus(false);
				result.setMessage("自动出款失败，代付申请已发送，请勿重复点击");
				return result;
			}

			// 代付用的网关地址
			String url = chargePay.getAddress();
			String doJsonForPost = HttpClientUtil.doPost(url, metaSignMap);
			logger.info("星支付自动出款返回信息:" + doJsonForPost);
			Boolean isJson = JSONUtils.isJson(doJsonForPost);

			if (isJson) {
				JSONObject jsonObject = JSONObject.fromObject(doJsonForPost);
				String code = jsonObject.getString("code");
				String msg = jsonObject.getString("message");
				// 下单状态 200成功 5000失败
				if ("1000".equals(code)) {

					// 没有回调日志，直接更新出款订单为出款成功
					withdrawOrderService.setWithDrawOrdersAutoPaySuccess(withdrawOrder.getId(), String.valueOf(withdrawOrder.getApplyValue()));

					// 更新为成功
					withdrawAutoLog.setDealStatus(EWithdrawAutoStatus.SUCCESS.name());
					withdrawAutoLogService.updateByPrimaryKeySelective(withdrawAutoLog);


					result = new ResultForAutoWithDraw();
					result.setStatus(true);
					result.setMessage("自动出款成功");
					return result;
				}else if("0000".equals(code)){
					// 改变自动出款状态为处理中
					withdrawAutoLog.setDealStatus(EWithdrawAutoStatus.SENDED.name());
					withdrawAutoLogService.updateByPrimaryKeySelective(withdrawAutoLog);
					withdrawOrder.setAutoWithdrawDealStatus(EFundAutoWithdrawDealStatus.DEALING.name());
					withdrawOrder.setPayType("STARTOUT");
					withdrawOrder.setMemberId(chargePay.getMemberId());
					// : TODO 此种转换需要修改.
					withdrawOrder.setAutoConfigId(Integer.valueOf(chargePay.getId().toString()));
					withdrawOrderService.updateByPrimaryKey(withdrawOrder);
					result = new ResultForAutoWithDraw();
					result.setStatus(true);
					result.setMessage("正在等待第三方自动出款");
					return result;

				} else {
					logger.info("星支付自动出款失败status为:" + code);
					
					// 更新为失败
					withdrawAutoLog.setDealStatus(EWithdrawAutoStatus.FAIL.name());
					withdrawAutoLogService.updateByPrimaryKeySelective(withdrawAutoLog);
					
					result = new ResultForAutoWithDraw();
					result.setStatus(false);
					result.setMessage("自动出款失败" + msg);
					return result;
				}

			} else {
				logger.info("星支付出款返回对象不是JSON格式:" + doJsonForPost);
				result = new ResultForAutoWithDraw();
				result.setStatus(false);
				result.setMessage("星支付自动出款失败");
				return result;
			}
		} catch (Exception e) {
			logger.error("自动出款错误:", e);
			result = new ResultForAutoWithDraw();
			result.setStatus(false);
			result.setMessage("星支付出款出现异常");
			return result;
		}
	}
}
