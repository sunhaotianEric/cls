
package com.team.lottery.autowithdraw.model.pp;

/** 
 * @Description: PP自动出款发起对象..
 * @Author: Jamine.
 * @CreateDate：2019-07-02 10:03:07.
 */
public class PPAutoWithDraw {
	// 商户ID;
	private String Username;
	// 商户自定义订单号;
	private String AimsOrderNumber;
	// 目标卡号
	private String AimsCardNumber;
	// 目标卡主姓名
	private String AimsCardName;
	// 目标卡银行名称
	private String AimsCardBank;
	// 目标金额数
	private String AimsMoney;
	// md5签名.
	private String sign;

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getUsername() {
		return Username;
	}

	public void setUsername(String username) {
		Username = username;
	}

	public String getAimsOrderNumber() {
		return AimsOrderNumber;
	}

	public void setAimsOrderNumber(String aimsOrderNumber) {
		AimsOrderNumber = aimsOrderNumber;
	}

	public String getAimsCardNumber() {
		return AimsCardNumber;
	}

	public void setAimsCardNumber(String aimsCardNumber) {
		AimsCardNumber = aimsCardNumber;
	}

	public String getAimsCardName() {
		return AimsCardName;
	}

	public void setAimsCardName(String aimsCardName) {
		AimsCardName = aimsCardName;
	}

	public String getAimsCardBank() {
		return AimsCardBank;
	}

	public void setAimsCardBank(String aimsCardBank) {
		AimsCardBank = aimsCardBank;
	}

	public String getAimsMoney() {
		return AimsMoney;
	}

	public void setAimsMoney(String aimsMoney) {
		AimsMoney = aimsMoney;
	}

}
