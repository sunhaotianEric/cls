
package com.team.lottery.autowithdraw.interfaces.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.autowithdraw.interfaces.IAutoWithDrawService;
import com.team.lottery.autowithdraw.model.kuaihuitong.KuaiHuiTongAutoWithDraw;
import com.team.lottery.autowithdraw.result.ResultForAutoWithDraw;
import com.team.lottery.enums.EFundAutoWithdrawDealStatus;
import com.team.lottery.enums.EWithdrawAutoStatus;
import com.team.lottery.enums.EWithdrawAutoType;
import com.team.lottery.pay.util.Md5Util;
import com.team.lottery.service.QuickBankTypeService;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.service.WithdrawAutoLogService;
import com.team.lottery.service.WithdrawOrderService;
import com.team.lottery.util.HttpClientUtil;
import com.team.lottery.util.JSONUtils;
import com.team.lottery.vo.ChargePay;
import com.team.lottery.vo.QuickBankType;
import com.team.lottery.vo.ThirdPayConfig;
import com.team.lottery.vo.WithdrawAutoLog;
import com.team.lottery.vo.WithdrawOrder;

import net.sf.json.JSONObject;

/** 
 * @Description: 快汇通自动出款实对象.
 * @Author: Jamine.
 * @CreateDate：2019-05-18 03:25:54.
 */
@Service("kuaiHuiTongAutoWithDrawService")
public class KuaiHuiTongAutoWithDrawService implements IAutoWithDrawService {
	private static Logger logger = LoggerFactory.getLogger(KuaiHuiTongAutoWithDrawService.class);
	@Autowired
	private ThirdPayConfigService thirdPayConfigService;
	@Autowired
	private WithdrawAutoLogService withdrawAutoLogService;
	@Autowired
	private WithdrawOrderService withdrawOrderService;
	@Autowired
	private QuickBankTypeService quickBankTypeService;

	/**
	 * 快汇通自动出款实现.
	 * @param withdrawOrder
	 * @param chargePay
	 * @throws Exception
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	@Override
	public ResultForAutoWithDraw autoWithDraw(WithdrawOrder withdrawOrder, ChargePay chargePay) {
		ResultForAutoWithDraw result = null;
		try {

			KuaiHuiTongAutoWithDraw kuaiHuiTong = new KuaiHuiTongAutoWithDraw();
			// 入账银行卡卡号.
			kuaiHuiTong.setAccountId(withdrawOrder.getBankCardNum());
			// 入账名称.
			kuaiHuiTong.setAccountName(withdrawOrder.getBankAccountName());
			// 商户名称.
			kuaiHuiTong.setCertId(chargePay.getMemberId());
			// 证件名称(第三方要求固定为0)
			kuaiHuiTong.setCertType(String.valueOf("0"));
			// 机构号.
			kuaiHuiTong.setInstCode("1069");
			String bankCode = "";
			// 回调地址.
			kuaiHuiTong.setBackUrl(chargePay.getReturnUrl());
			QuickBankType queryBankType = new QuickBankType();
			queryBankType.setChargeType(chargePay.getChargeType());
			queryBankType.setBankType(withdrawOrder.getBankType());
			List<QuickBankType> QuickBankTypeList = quickBankTypeService.getQuickBankTypeQuery(queryBankType);
			if (QuickBankTypeList != null && QuickBankTypeList.size() == 0) {
				result = new ResultForAutoWithDraw();
				result.setStatus(false);
				result.setMessage("该银行类型不支持自动出款请联系客服!");
				return result;
			} else {
				bankCode = QuickBankTypeList.get(0).getPayId();
			}
			kuaiHuiTong.setBankCode(bankCode);
			// 订单号.
			kuaiHuiTong.setOrderNo(withdrawOrder.getSerialNumber());
			// 金额(分为单位).
			String OrderMoney = String.valueOf(withdrawOrder.getApplyValue().multiply(new BigDecimal(100)).intValue());
			kuaiHuiTong.setTransAmt(OrderMoney);
			// 交易日期(yyyyMMdd).
			Date currTime = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd", Locale.CHINA);
			String TradeDate = new String(formatter.format(currTime));
			kuaiHuiTong.setTransDate(TradeDate);
			// 交易类型.
			kuaiHuiTong.setTransType("1009");

			// 获取秘钥.
			String key = chargePay.getSign();
			String mark = "&";
			// MD5加密.
			String md5Str = "accountId=" + kuaiHuiTong.getAccountId() + mark + 
							"accountName=" + kuaiHuiTong.getAccountName() + mark + 
							"backUrl=" + kuaiHuiTong.getBackUrl() + mark + 
							"bankCode=" + kuaiHuiTong.getBankCode() + mark + 
							"certId=" + kuaiHuiTong.getCertId() + mark + 
							"certType=" + kuaiHuiTong.getCertType() + mark + 
							"instCode=" + kuaiHuiTong.getInstCode() + mark + 
							"orderNo=" + kuaiHuiTong.getOrderNo() + mark + 
							"transAmt=" + kuaiHuiTong.getTransAmt() + mark + 
							"transDate=" + kuaiHuiTong.getTransDate() + mark + 
							"transType=" + kuaiHuiTong.getTransType() + mark + "key=" + key;

			String md5Sign = Md5Util.getMD5ofStr(md5Str,"UTF-8");
			logger.info("快汇通自动出款MD5加密前字符串为:" + md5Str + "加密后为:" + md5Sign);
			kuaiHuiTong.setSign(md5Sign);
			logger.info(kuaiHuiTong.toString());

			// json提交post表单.
			JSONObject jsonMap = new JSONObject();
			jsonMap.put("accountId", kuaiHuiTong.getAccountId());
			jsonMap.put("accountName", kuaiHuiTong.getAccountName());
			jsonMap.put("certId", kuaiHuiTong.getCertId());
			jsonMap.put("bankCode", kuaiHuiTong.getBankCode());
			jsonMap.put("certType", kuaiHuiTong.getCertType());
			jsonMap.put("instCode", kuaiHuiTong.getInstCode());
			jsonMap.put("orderNo", kuaiHuiTong.getOrderNo());
			jsonMap.put("transAmt", kuaiHuiTong.getTransAmt());
			jsonMap.put("transDate", kuaiHuiTong.getTransDate());
			jsonMap.put("backUrl", kuaiHuiTong.getBackUrl());
			jsonMap.put("transType", kuaiHuiTong.getTransType());
			jsonMap.put("sign", kuaiHuiTong.getSign());
			String url = chargePay.getAddress();
			String doJsonForPost = HttpClientUtil.doJsonForPost(url, jsonMap);
			logger.info("快汇通自动出款返回信息:" + doJsonForPost);
			Boolean isJson = JSONUtils.isJson(doJsonForPost);

			if (isJson) {
				JSONObject jsonObject = JSONObject.fromObject(doJsonForPost);
				String ret_code = jsonObject.getString("ret_code");
				String ret_msg = jsonObject.getString("ret_msg");
				if ("0000".equals(ret_code)) {
					WithdrawAutoLog withdrawAutoLog = new WithdrawAutoLog();
					withdrawAutoLog.setAdminName(withdrawOrder.getLockAdmin());
					withdrawAutoLog.setBizSystem(withdrawOrder.getBizSystem());
					withdrawAutoLog.setBankAccountName(withdrawOrder.getBankAccountName());
					withdrawAutoLog.setBankCardNum(withdrawOrder.getBankCardNum());
					withdrawAutoLog.setBankType(withdrawOrder.getBankType());
					withdrawAutoLog.setBankName(withdrawOrder.getBankName());
					ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("KUAIHUITONGPAY");
					withdrawAutoLog.setChargeDes(thirdPayConfig.getChargeDes());
					withdrawAutoLog.setChargeType(thirdPayConfig.getChargeType());
					withdrawAutoLog.setCreatedDate(new Date());
					withdrawAutoLog.setDealStatus(EWithdrawAutoStatus.SENDING.name());
					withdrawAutoLog.setMemberId(chargePay.getMemberId());
					withdrawAutoLog.setSerialNumber(withdrawOrder.getSerialNumber());
					withdrawAutoLog.setType(EWithdrawAutoType.AUTO.name());
					withdrawAutoLog.setUserName(withdrawOrder.getUserName());
					withdrawAutoLog.setUserId(withdrawOrder.getUserId());
					withdrawAutoLog.setWithdrowOrderId(withdrawOrder.getId());
					withdrawAutoLog.setMoney(withdrawOrder.getApplyValue());
					withdrawAutoLogService.insertSelective(withdrawAutoLog);
					withdrawOrder.setAutoWithdrawDealStatus(EFundAutoWithdrawDealStatus.DEALING.name());
					withdrawOrderService.updateByPrimaryKey(withdrawOrder);
					result = new ResultForAutoWithDraw();
					result.setStatus(true);
					result.setMessage("自动出款成功");
					return result;
				} else {
					logger.info("快汇通自动出款失败ret_code为:" + ret_code);
					result = new ResultForAutoWithDraw();
					result.setStatus(false);
					result.setMessage("自动出款失败" + ret_msg);
					return result;
				}

			} else {
				logger.info("快汇通自动出款返回对象不是JSON格式:" + doJsonForPost);
				result = new ResultForAutoWithDraw();
				result.setStatus(false);
				result.setMessage("快汇通自动出款失败");
				return result;
			}
		} catch (Exception e) {
			logger.error("自动出款错误:", e);
			result = new ResultForAutoWithDraw();
			result.setStatus(false);
			result.setMessage("快汇通自动出款出现异常");
			return result;
		}
	}
}
