
package com.team.lottery.autowithdraw.interfaces.impl;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.autowithdraw.interfaces.IAutoWithDrawService;
import com.team.lottery.autowithdraw.model.pp.PPAutoWithDraw;
import com.team.lottery.autowithdraw.result.ResultForAutoWithDraw;
import com.team.lottery.enums.EFundAutoWithdrawDealStatus;
import com.team.lottery.enums.EWithdrawAutoStatus;
import com.team.lottery.enums.EWithdrawAutoType;
import com.team.lottery.pay.util.Md5Util;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.service.WithdrawAutoLogService;
import com.team.lottery.service.WithdrawOrderService;
import com.team.lottery.util.HttpClientUtil;
import com.team.lottery.util.JSONUtils;
import com.team.lottery.vo.ChargePay;
import com.team.lottery.vo.ThirdPayConfig;
import com.team.lottery.vo.WithdrawAutoLog;
import com.team.lottery.vo.WithdrawOrder;

import net.sf.json.JSONObject;

/** 
 * @Description: PP 自动出款实现.
 * @Author: Jamine.
 * @CreateDate：2019-07-02 10:25:13.
 */
@Service("pPAutoWithDrawService")
public class PPAutoWithDrawService implements IAutoWithDrawService {

	@Autowired
	private ThirdPayConfigService thirdPayConfigService;
	@Autowired
	private WithdrawAutoLogService withdrawAutoLogService;
	@Autowired
	private WithdrawOrderService withdrawOrderService;

	private static Logger logger = LoggerFactory.getLogger(PPAutoWithDrawService.class);

	@Override
	public ResultForAutoWithDraw autoWithDraw(WithdrawOrder withdrawOrder, ChargePay chargePay) {
		ResultForAutoWithDraw result = null;
		try {
			PPAutoWithDraw pPAutoWithDraw = new PPAutoWithDraw();
			pPAutoWithDraw.setAimsCardBank(withdrawOrder.getBankName());
			pPAutoWithDraw.setAimsCardName(withdrawOrder.getBankAccountName());
			pPAutoWithDraw.setAimsCardNumber(withdrawOrder.getBankCardNum());
			pPAutoWithDraw.setAimsMoney(String.valueOf(withdrawOrder.getApplyValue()));
			pPAutoWithDraw.setAimsOrderNumber(withdrawOrder.getSerialNumber());
			pPAutoWithDraw.setUsername(chargePay.getMemberId());
			// 获取当前unix时间
			String unixTime = Long.toString(new Date().getTime() / 1000);
			// 参数加密,直接拼接加密.
			String mD5Str = pPAutoWithDraw.getUsername() + chargePay.getSign() + unixTime + pPAutoWithDraw.getAimsOrderNumber()
					+ pPAutoWithDraw.getAimsCardNumber() + pPAutoWithDraw.getAimsCardName() + pPAutoWithDraw.getAimsCardBank()
					+ pPAutoWithDraw.getAimsMoney() + "9rehECh9zes#QacHebrePlg53r43Ab";
			String md5Sign = Md5Util.getMD5ofStr(mD5Str, "UTF-8");
			logger.info("PP自动出款MD5加密前字符串为:" + mD5Str + "加密后为:" + md5Sign);
			pPAutoWithDraw.setSign(md5Sign);
			logger.info(pPAutoWithDraw.toString());

			// json提交post表单.
			JSONObject jsonMap = new JSONObject();

			jsonMap.put("Username", pPAutoWithDraw.getUsername());
			jsonMap.put("AimsOrderNumber", pPAutoWithDraw.getAimsOrderNumber());
			jsonMap.put("AimsCardNumber", pPAutoWithDraw.getAimsCardNumber());
			jsonMap.put("AimsCardName", pPAutoWithDraw.getAimsCardName());
			jsonMap.put("AimsCardBank", pPAutoWithDraw.getAimsCardBank());
			jsonMap.put("AimsMoney", pPAutoWithDraw.getAimsMoney());

			// 新增自动出款配置数据.
			WithdrawAutoLog withdrawAutoLog = new WithdrawAutoLog();
			withdrawAutoLog.setAdminName(withdrawOrder.getLockAdmin());
			withdrawAutoLog.setBizSystem(withdrawOrder.getBizSystem());
			withdrawAutoLog.setBankAccountName(withdrawOrder.getBankAccountName());
			withdrawAutoLog.setBankCardNum(withdrawOrder.getBankCardNum());
			withdrawAutoLog.setBankType(withdrawOrder.getBankType());
			withdrawAutoLog.setBankName(withdrawOrder.getBankName());
			ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("PPPAY");
			withdrawAutoLog.setChargeDes(thirdPayConfig.getChargeDes());
			withdrawAutoLog.setChargeType(thirdPayConfig.getChargeType());
			withdrawAutoLog.setCreatedDate(new Date());
			withdrawAutoLog.setDealStatus(EWithdrawAutoStatus.SENDING.name());
			withdrawAutoLog.setMemberId(chargePay.getMemberId());
			withdrawAutoLog.setSerialNumber(withdrawOrder.getSerialNumber());
			withdrawAutoLog.setType(EWithdrawAutoType.AUTO.name());
			withdrawAutoLog.setUserName(withdrawOrder.getUserName());
			withdrawAutoLog.setUserId(withdrawOrder.getUserId());
			withdrawAutoLog.setWithdrowOrderId(withdrawOrder.getId());
			withdrawAutoLog.setMoney(withdrawOrder.getApplyValue());
			withdrawAutoLogService.insertSelective(withdrawAutoLog);
			// 获取发起地址,
			String url = chargePay.getAddress();
			url = url + "?timeunix=" + unixTime + "&signature=" + pPAutoWithDraw.getSign();
			logger.info("参数timeunix为[{}]参数signature[{}]", unixTime, pPAutoWithDraw.getSign());
			String doJsonForPost = HttpClientUtil.doJsonForPost(url, jsonMap);
			logger.info("PP通自动出款返回信息:" + doJsonForPost);
			Boolean isJson = JSONUtils.isJson(doJsonForPost);
			if (isJson) {
				JSONObject jsonObject = JSONObject.fromObject(doJsonForPost);
				String statusReplyNumbering = jsonObject.getString("StatusReplyNumbering");
				if (statusReplyNumbering.equals("LX1020")) {
					// 改变自动出款状态为处理中
					withdrawAutoLog.setDealStatus(EWithdrawAutoStatus.SENDED.name());
					withdrawAutoLogService.updateByPrimaryKeySelective(withdrawAutoLog);
					withdrawOrder.setAutoWithdrawDealStatus(EFundAutoWithdrawDealStatus.DEALING.name());
					withdrawOrder.setPayType("PPPAY");
					withdrawOrder.setMemberId(chargePay.getMemberId());
					// : TODO 此种转换需要修改.
					withdrawOrder.setAutoConfigId(Integer.valueOf(chargePay.getId().toString()));
					withdrawOrderService.updateByPrimaryKey(withdrawOrder);
					result = new ResultForAutoWithDraw();
					result.setStatus(true);
					result.setMessage("正在等待第三方自动出款");
					return result;
				} else {
					logger.info("PP自动出款失败StatusReplyNumbering为:" + statusReplyNumbering);
					result = new ResultForAutoWithDraw();
					result.setStatus(false);
					result.setMessage("自动出款失败" + jsonObject.getString("StatusReply"));
					return result;
				}
			} else {
				logger.info("PP自动出款返回对象不是JSON格式:" + doJsonForPost);
				result = new ResultForAutoWithDraw();
				result.setStatus(false);
				result.setMessage("PP自动出款失败");
				return result;
			}
		} catch (Exception e) {
			logger.error("PP自动出款异常:", e);
			result = new ResultForAutoWithDraw();
			result.setStatus(false);
			result.setMessage("PP自动出款出现异常");
			return result;
		}
	}
}
