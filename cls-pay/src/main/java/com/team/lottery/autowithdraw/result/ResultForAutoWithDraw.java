
package com.team.lottery.autowithdraw.result;

/** 
 * @Description: 代付返回对象.
 * @Author: Jamine.
 * @CreateDate：2019-05-18 08:43:59.
 */
public class ResultForAutoWithDraw {
	
	// 状态(ture成功,false失败).
	private boolean status;
	private String message;

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
