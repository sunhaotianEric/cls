
package com.team.lottery.autowithdraw.interfaces.impl;

import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.autowithdraw.interfaces.IAutoWithDrawService;
import com.team.lottery.autowithdraw.result.ResultForAutoWithDraw;
import com.team.lottery.enums.EFundAutoWithdrawDealStatus;
import com.team.lottery.enums.EWithdrawAutoStatus;
import com.team.lottery.enums.EWithdrawAutoType;
import com.team.lottery.pay.util.hipay3721.RSADemo;
import com.team.lottery.service.QuickBankTypeService;
import com.team.lottery.service.ThirdPayConfigService;
import com.team.lottery.service.WithdrawAutoLogService;
import com.team.lottery.service.WithdrawOrderService;
import com.team.lottery.util.HttpClientUtil;
import com.team.lottery.util.JSONUtils;
import com.team.lottery.util.Md5Util;
import com.team.lottery.vo.ChargePay;
import com.team.lottery.vo.ThirdPayConfig;
import com.team.lottery.vo.WithdrawAutoLog;
import com.team.lottery.vo.WithdrawOrder;

import net.sf.json.JSONObject;

/** 
 * @Description: 3721自动出款实对象.
 * @Author: luocheng
 * @CreateDate：2019-05-18 03:25:54.
 */
@Service("Hipay3721AutoWithDrawService")
public class Hipay3721AutoWithDrawService implements IAutoWithDrawService {
	private static Logger logger = LoggerFactory.getLogger(Hipay3721AutoWithDrawService.class);
	@Autowired
	private ThirdPayConfigService thirdPayConfigService;
	@Autowired
	private WithdrawAutoLogService withdrawAutoLogService;
	@Autowired
	private WithdrawOrderService withdrawOrderService;
	@Autowired
	private QuickBankTypeService quickBankTypeService;

	/**
	 * 3721自动出款实现.
	 * @param withdrawOrder
	 * @param chargePay
	 * @throws Exception
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	@Override
	public ResultForAutoWithDraw autoWithDraw(WithdrawOrder withdrawOrder, ChargePay chargePay) {
		ResultForAutoWithDraw result = null;
		try {

			// 商户号
			String merchantNo = chargePay.getMemberId();
			String merchantOrderNo = withdrawOrder.getSerialNumber();
			// 出款类型，固定为1 银行卡
			String type = "1";
			// 银行卡号
			String outNo = withdrawOrder.getBankCardNum();
			// 银行卡姓名
			String outName = withdrawOrder.getBankAccountName();
			String bankName = withdrawOrder.getBankName();
			// 出款金额，只能为整数
			String amount = String.valueOf(withdrawOrder.getApplyValue().intValue());
			// 商户密钥,这里配置在私钥这个字段（注意下）
			String key = chargePay.getPrivatekey();
			logger.info("key:[{}]", key);
			
			// 组合Pre明文参数
			String mark = "&";
			String preString = "merchantNo=" + merchantNo + mark + 
					"merchantOrderNo=" + merchantOrderNo + mark + 
					"outNo=" + outNo + mark + 
					"outName=" + outName + mark + 
					"bankName=" + bankName + mark + 
					"amount=" + amount + mark + 
					"key=" + key + mark + 
					"type=" + type;
			
			// 签名加密
			String publicKey = chargePay.getPublickey();
			byte[] decoded = Base64.decodeBase64(publicKey);
		    byte[] encryptByPublicKey = RSADemo.encryptByPublicKey(preString.getBytes(), decoded);
		    String encodeToString = Base64.encodeBase64String(encryptByPublicKey);
		    String pre = URLEncoder.encode(encodeToString, "UTF-8");
		    
			logger.info("3721自动出款加密前字符串为:" + preString + "加密后为:" + pre);
			
			// 计算验签
			String sign = Md5Util.getMD5ofStr(preString);

			
			// 发送报文前插入日志
			WithdrawAutoLog withdrawAutoLog = new WithdrawAutoLog();
			withdrawAutoLog.setAdminName(withdrawOrder.getLockAdmin());
			withdrawAutoLog.setBizSystem(withdrawOrder.getBizSystem());
			withdrawAutoLog.setBankAccountName(withdrawOrder.getBankAccountName());
			withdrawAutoLog.setBankCardNum(withdrawOrder.getBankCardNum());
			withdrawAutoLog.setBankType(withdrawOrder.getBankType());
			withdrawAutoLog.setBankName(withdrawOrder.getBankName());
			ThirdPayConfig thirdPayConfig = thirdPayConfigService.thirdPayConfigByChargeType("HIPAY3721");
			withdrawAutoLog.setChargeDes(thirdPayConfig.getChargeDes());
			withdrawAutoLog.setChargeType(thirdPayConfig.getChargeType());
			withdrawAutoLog.setCreatedDate(new Date());
			withdrawAutoLog.setDealStatus(EWithdrawAutoStatus.SENDING.name());
			withdrawAutoLog.setMemberId(chargePay.getMemberId());
			withdrawAutoLog.setSerialNumber(withdrawOrder.getSerialNumber());
			withdrawAutoLog.setType(EWithdrawAutoType.AUTO.name());
			withdrawAutoLog.setUserName(withdrawOrder.getUserName());
			withdrawAutoLog.setUserId(withdrawOrder.getUserId());
			withdrawAutoLog.setWithdrowOrderId(withdrawOrder.getId());
			withdrawAutoLog.setMoney(withdrawOrder.getApplyValue());
			withdrawAutoLogService.insertSelective(withdrawAutoLog);
			
			// 发送报文
			Map<String, String> params = new HashMap<String, String>();
			params.put("pr", pre);
			params.put("sign", sign);
			
//			String url = chargePay.getAddress();
			// 这里先写死，防止充值跟代付使用相同的通知地址
			String url = "https://hipay3721.com/payweb/outPay";
			
			String postResult = HttpClientUtil.doPost(url, params);
			logger.info("3721自动出款返回信息:" + postResult);
			Boolean isJson = JSONUtils.isJson(postResult);
			
			// 更新报文发送日志为已发送
			withdrawAutoLog.setDealStatus(EWithdrawAutoStatus.SENDED.name());
			withdrawAutoLogService.updateByPrimaryKeySelective(withdrawAutoLog);

			if (isJson) {
				JSONObject jsonObject = JSONObject.fromObject(postResult);
				String ret_code = jsonObject.getString("result");
				String ret_msg = jsonObject.getString("msg");
				if ("true".equals(ret_code)) {
					
					// 没有回调日志，直接更新出款订单为出款成功
					withdrawOrderService.setWithDrawOrdersAutoPaySuccess(withdrawOrder.getId(), String.valueOf(withdrawOrder.getApplyValue()));
					// 更新报文发送日志为成功 
		            withdrawAutoLog.setDealStatus(EWithdrawAutoStatus.SUCCESS.name());
		            withdrawAutoLogService.updateByPrimaryKeySelective(withdrawAutoLog);
		            
					result = new ResultForAutoWithDraw();
					result.setStatus(true);
					result.setMessage("自动出款成功");
					return result;
				} else {
					logger.info("3721自动出款失败result为:" + ret_code);
					result = new ResultForAutoWithDraw();
					result.setStatus(false);
					result.setMessage("自动出款失败" + ret_msg);
					return result;
				}

			} else {
				logger.info("3721自动出款返回对象不是JSON格式:" + postResult);
				result = new ResultForAutoWithDraw();
				result.setStatus(false);
				result.setMessage("3721自动出款失败");
				return result;
			}
		} catch (Exception e) {
			logger.error("自动出款错误:", e);
			result = new ResultForAutoWithDraw();
			result.setStatus(false);
			result.setMessage("3721自动出款出现异常");
			return result;
		}
	}
}
