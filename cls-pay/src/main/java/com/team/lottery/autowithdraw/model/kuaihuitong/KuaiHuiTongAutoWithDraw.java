
package com.team.lottery.autowithdraw.model.kuaihuitong;

/** 
 * @Description: 快汇通自动出款发起对象.
 * @Author: Jamine.
 * @CreateDate：2019-05-18 11:17:30.
 */
public class KuaiHuiTongAutoWithDraw {
		// 交易类型.
		private String transType;
		// 机构号.
		private String instCode;
		// 证件类型.
		private String certType;
		// 证件号码.
		private String certId;
		// 金额(以分为单位.)
		private String transAmt;
		// 交易日期.
		private String transDate;
		// 订单到.
		private String orderNo;
		// 结算账号.
		private String accountId;
		// 结算户名.
		private String accountName;
		// 订单状态.
		private String orderStatus;
		// 结算银行编号
		private String bankCode;
		// 后台通知地址.
		private String backUrl;
		// 通讯应答码.
		private String ret_code;
		// 通讯描述.
		private String ret_msg;
		// 签名
		private String sign;
		
		public String getTransType() {
			return transType;
		}
		public void setTransType(String transType) {
			this.transType = transType;
		}
		public String getInstCode() {
			return instCode;
		}
		public void setInstCode(String instCode) {
			this.instCode = instCode;
		}
		public String getCertType() {
			return certType;
		}
		public void setCertType(String certType) {
			this.certType = certType;
		}
		public String getCertId() {
			return certId;
		}
		public void setCertId(String certId) {
			this.certId = certId;
		}
		public String getTransAmt() {
			return transAmt;
		}
		public void setTransAmt(String transAmt) {
			this.transAmt = transAmt;
		}
		public String getTransDate() {
			return transDate;
		}
		public void setTransDate(String transDate) {
			this.transDate = transDate;
		}
		public String getOrderNo() {
			return orderNo;
		}
		public void setOrderNo(String orderNo) {
			this.orderNo = orderNo;
		}
		public String getAccountId() {
			return accountId;
		}
		public void setAccountId(String accountId) {
			this.accountId = accountId;
		}
		public String getAccountName() {
			return accountName;
		}
		public void setAccountName(String accountName) {
			this.accountName = accountName;
		}
		public String getOrderStatus() {
			return orderStatus;
		}
		public void setOrderStatus(String orderStatus) {
			this.orderStatus = orderStatus;
		}
		public String getBankCode() {
			return bankCode;
		}
		public void setBankCode(String bankCode) {
			this.bankCode = bankCode;
		}
		public String getBackUrl() {
			return backUrl;
		}
		public void setBackUrl(String backUrl) {
			this.backUrl = backUrl;
		}
		public String getRet_code() {
			return ret_code;
		}
		public void setRet_code(String ret_code) {
			this.ret_code = ret_code;
		}
		public String getRet_msg() {
			return ret_msg;
		}
		public void setRet_msg(String ret_msg) {
			this.ret_msg = ret_msg;
		}
		public String getSign() {
			return sign;
		}
		public void setSign(String sign) {
			this.sign = sign;
		}
		
		@Override
		public String toString() {
			return "快汇通代付发起信息处理对象: [transType=" + transType + ", instCode=" + instCode + ", certType=" + certType + ", certId=" + certId
					+ ", transAmt=" + transAmt + ", transDate=" + transDate + ", orderNo=" + orderNo + ", accountId=" + accountId
					+ ", accountName=" + accountName + ", orderStatus=" + orderStatus + ", bankCode=" + bankCode + ", backUrl=" + backUrl
					+ ", ret_code=" + ret_code + ", ret_msg=" + ret_msg + "]";
		}
		
		
		
}
