package com.team.lottery.system;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.exception.UnEqualVersionException;
import com.team.lottery.service.RechargeOrderService;
import com.team.lottery.util.ApplicationContextUtil;

public class UserVersionExceptionPayDealThread extends Thread {

	private static Logger logger = LoggerFactory.getLogger(UserVersionExceptionPayDealThread.class);
	// 在线充值更新版本号不一致
	public static final Integer RECHARGE_ORDER_EXCEPTION = 1;
	// 自动入款更新版本号不一致
	public static final Integer AUTO_RECHARGE_INCOME_EXCEPTION = 4;

	// 重试间隔时间（毫秒）
	public static final Integer RETRY_SLEEP_TIME = 1000;
	// 重试总次数
	public static final Integer RETRY_TOTAL_COUNT = 10;

	private Integer exceptionId;
	// 重试次数
	private Integer retryConut = 0;

	private String serialNumber;
	private BigDecimal money;
	// 自动入款使用变量
	private Long rechargeId;

	public UserVersionExceptionPayDealThread(Integer exceptionId, String serialNumber, BigDecimal money, String succTime) {
		this.exceptionId = exceptionId;
		this.serialNumber = serialNumber;
		this.money = money;
	}

	public UserVersionExceptionPayDealThread(Integer exceptionId, Long rechargeId) {
		this.exceptionId = exceptionId;
		this.rechargeId = rechargeId;
	}

	@Override
	public void run() {
		// 重试在线充值成功处理
		if (RECHARGE_ORDER_EXCEPTION == this.exceptionId) {
			dealRechargeOrderSuccess();
			// 重试自动入款处理
		} else if (AUTO_RECHARGE_INCOME_EXCEPTION == this.exceptionId) {
			//dealAutoRechargeIncome();
		}
	}

	/**
	 * 单笔订单处理,当重复调用到设定次数还没成功，则退出线程
	 * 
	 * @param rechargeWithDrawOrderService
	 */
	private void dealRechargeOrderSuccess() {
		RechargeOrderService rechargeOrderService = ApplicationContextUtil.getBean("rechargeOrderService");
		if (retryConut < RETRY_TOTAL_COUNT) {
			try {
				// 当前线程休眠
				Thread.sleep(RETRY_SLEEP_TIME);
				retryConut++;
				rechargeOrderService.rechargeOrderSuccessDeal(serialNumber, money);
			} catch (UnEqualVersionException e) {
				logger.info("发现版本号不一致，第[" + retryConut + "]次重试，方法[dealRechargeOrderSuccess]");
				dealRechargeOrderSuccess();
			} catch (Exception e) {
				// 业务异常
				logger.error(e.getMessage(), e);
			}
		} else {
			logger.error("发现版本号不一致，方法[dealRechargeOrderSuccess],重试次数超出" + RETRY_TOTAL_COUNT + "次，终止重试进程");
			return;
		}
	}

	/**
	 * 处理自动入款
	 * 
	 * @deprecated
	 * @param userName
	 * @param outContrlMoney
	 */
	/*private void dealAutoRechargeIncome() {
		RechargeOrderService rechargeOrderService = ApplicationContextUtil.getBean("rechargeOrderService");
		if (retryConut < RETRY_TOTAL_COUNT) {
			try {
				// 当前线程休眠
				Thread.sleep(RETRY_SLEEP_TIME);
				retryConut++;
				//rechargeOrderService.setRechargerOrdersPaySuccess(rechargeId, "0", "0", "0",null,null);
				;
			} catch (UnEqualVersionException e) {
				logger.info("发现版本号不一致，第[" + retryConut + "]次重试，方法[dealAutoRechargeIncome]");
				dealAutoRechargeIncome();
			} catch (Exception e) {
				// 业务异常
				logger.error(e.getMessage(), e);
			}
		} else {
			logger.error("发现版本号不一致，方法[dealAutoRechargeIncome],重试次数超出" + RETRY_TOTAL_COUNT + "次，终止重试进程");
			return;
		}
	}*/
}
