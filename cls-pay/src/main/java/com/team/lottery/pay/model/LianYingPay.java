package com.team.lottery.pay.model;
/**
 * 联赢第三方支付
 * @author Jamine
 *
 */
public class LianYingPay {
	//商户号.
	private String merchantId;
	//交易金额(分为单位).
	private String amount;
	//时间戳.
	private String time;
	//订单类型(alipay/weChat).
	private String paymentType;
	//加密方式.
	private String signType;
	//通知地址.
	private String notifyUrl;
	//商户订单号.
	private String poId;
	//签名.
	private String sign;
	
	
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getSignType() {
		return signType;
	}
	public void setSignType(String signType) {
		this.signType = signType;
	}
	public String getNotifyUrl() {
		return notifyUrl;
	}
	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}
	public String getPoId() {
		return poId;
	}
	public void setPoId(String poId) {
		this.poId = poId;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	
}
