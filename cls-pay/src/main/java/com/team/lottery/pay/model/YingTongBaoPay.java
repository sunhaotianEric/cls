package com.team.lottery.pay.model;
public class YingTongBaoPay {
	//商户号
	public String merchno;
	//交易金额
	public String amount;
	//商户流水号
	public String traceno;
	//支付方式
	public String payType;
	//商品名称
	public String goodsName;
	//通知地址
	public String notifyUrl;
	//数据签名
	public String signature;
	//备注
	public String remark;
	//结算方式
	public String settleType;
	
	public String getMerchno() {
		return merchno;
	}



	public void setMerchno(String merchno) {
		this.merchno = merchno;
	}



	public String getAmount() {
		return amount;
	}



	public void setAmount(String amount) {
		this.amount = amount;
	}



	public String getTraceno() {
		return traceno;
	}



	public void setTraceno(String traceno) {
		this.traceno = traceno;
	}



	public String getPayType() {
		return payType;
	}



	public void setPayType(String payType) {
		this.payType = payType;
	}



	public String getGoodsName() {
		return goodsName;
	}



	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}



	public String getNotifyUrl() {
		return notifyUrl;
	}



	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	public String getSignature() {
		return signature;
	}



	public void setSignature(String signature) {
		this.signature = signature;
	}
    
	


	public String getRemark() {
		return remark;
	}



	public void setRemark(String remark) {
		this.remark = remark;
	}
    
	
	
	public String getSettleType() {
		return settleType;
	}



	public void setSettleType(String settleType) {
		this.settleType = settleType;
	}



	public String toString(){
		return " 商户 号:"+merchno + " 交易金额:" + amount+
			   " 商户流水号:"+traceno + " 支付方式:" + payType+
			   " 商品名称:"+goodsName + " 通知地址:" + notifyUrl+
			   " 备注:"+remark +" 数据签名:" + signature+" 结算方式:" + settleType;
	}
	
	public static void main(String[] args) {
		
		System.out.println(System.currentTimeMillis()/1000);
	}
}
