package com.team.lottery.pay.model.shandepay;


/**
 * 杉德支付对象(快捷支付).
 * 
 * @author Jamine
 *
 */
public class ShanDePay {
	
	// 用户ID
	private String userId;
	// 商户上传唯一ID号码
	private String orderCode; 
	// 商户支付时间
	private String orderTime;
	// 订单金额
	private String totalAmount;
	// 订单标题
	private String subject;
	// 订单描述
	private String body;
	// 币种
	private String currencyCode;
	// 异步通知地址.
	private String notifyUrl;
	// 支付成功跳转地址.
	private String frontUrl;
	// 清算模式.(默认0)
	private String clearCycle;
	
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getOrderCode() {
		return orderCode;
	}
	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}
	public String getOrderTime() {
		return orderTime;
	}
	public void setOrderTime(String orderTime) {
		this.orderTime = orderTime;
	}
	public String getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getNotifyUrl() {
		return notifyUrl;
	}
	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}
	public String getFrontUrl() {
		return frontUrl;
	}
	public void setFrontUrl(String frontUrl) {
		this.frontUrl = frontUrl;
	}
	public String getClearCycle() {
		return clearCycle;
	}
	public void setClearCycle(String clearCycle) {
		this.clearCycle = clearCycle;
	}
	
	
}
