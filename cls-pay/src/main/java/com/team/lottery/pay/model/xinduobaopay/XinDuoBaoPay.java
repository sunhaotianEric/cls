
package com.team.lottery.pay.model.xinduobaopay;

/** 
 * @Description: 鑫多宝支付发起支付对象
 * @Author: Jamine.
 * @CreateDate：2019-05-13 02:01:51.
 */
public class XinDuoBaoPay {
	
	// 用户编号.
	private String usercode;
	// 商户订单号.
	private String customno;
	// 产品名称(不能用中文).
	private String productname;
	// 支付金额(请输入个位不为零的整数或两位小数).
	private String money;
	// 码表.
	private String scantype;
	// 支付发起时间(yyyyMMddHHmmss).
	private String sendtime;
	// 回调地址.
	private String notifyurl;
	// 用户发起IP地址.
	private String buyerip;
	// 签名.
	private String sign;

	public String getUsercode() {
		return usercode;
	}

	public void setUsercode(String usercode) {
		this.usercode = usercode;
	}

	public String getCustomno() {
		return customno;
	}

	public void setCustomno(String customno) {
		this.customno = customno;
	}

	public String getProductname() {
		return productname;
	}

	public void setProductname(String productname) {
		this.productname = productname;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	public String getScantype() {
		return scantype;
	}

	public void setScantype(String scantype) {
		this.scantype = scantype;
	}

	public String getSendtime() {
		return sendtime;
	}

	public void setSendtime(String sendtime) {
		this.sendtime = sendtime;
	}

	public String getNotifyurl() {
		return notifyurl;
	}

	public void setNotifyurl(String notifyurl) {
		this.notifyurl = notifyurl;
	}

	public String getBuyerip() {
		return buyerip;
	}

	public void setBuyerip(String buyerip) {
		this.buyerip = buyerip;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	@Override
	public String toString() {
		return "鑫多宝支付发起对象 [商户号=" + usercode + ", 订单号=" + customno + ",产品名称=" + productname + ", 支付金额=" + money
				+ ", 码表=" + scantype + ", 发起支付时间=" + sendtime + ", 回调地址=" + notifyurl + ", 用户ip=" + buyerip + ", 签名="
				+ sign + "]";
	}
	
	
	
}
