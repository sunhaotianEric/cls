package com.team.lottery.pay.model.shengshipay;

/**
 * 第三方支付,盛世支付发起对象.(SHENGSHIPAY)
 * @author jamine
 *
 */
public class ShengShiPay {
	
	// 商户号.
	private String merchant_no;
	// 平台唯一订单ID.
	private String merchant_order_no;
	// 码表(1支付宝H5,支付宝扫码).
	private String pay_type;
	// 回调地址.
	private String notify_url;
	// 支付成功跳转地址.
	private String return_url;
	// 支付金额(元为单位)
	private String trade_amount;
	// 签名.
	private String sign;
	public String getMerchant_no() {
		return merchant_no;
	}
	public void setMerchant_no(String merchant_no) {
		this.merchant_no = merchant_no;
	}
	public String getMerchant_order_no() {
		return merchant_order_no;
	}
	public void setMerchant_order_no(String merchant_order_no) {
		this.merchant_order_no = merchant_order_no;
	}
	public String getPay_type() {
		return pay_type;
	}
	public void setPay_type(String pay_type) {
		this.pay_type = pay_type;
	}
	public String getNotify_url() {
		return notify_url;
	}
	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}
	public String getReturn_url() {
		return return_url;
	}
	public void setReturn_url(String return_url) {
		this.return_url = return_url;
	}
	public String getTrade_amount() {
		return trade_amount;
	}
	public void setTrade_amount(String trade_amount) {
		this.trade_amount = trade_amount;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	
	@Override
	public String toString() {
		return "盛世发起支付对象 [商户号=" + merchant_no + ", 订单号=" + merchant_order_no + ", 支付方式="
				+ pay_type + ", 回调地址=" + notify_url + ", 成功跳转地址=" + return_url + ", 支付金额="
				+ trade_amount + "元, 签名=" + sign + "]";
	}
	
	

}
