package com.team.lottery.pay.model;
public class BaofuPay {
	//商户 ID
	public String MemberID;
	//终端 ID
	public String TerminalID;
	//接口版本
	public String InterfaceVersio;
	//加密类型
	public String KeyType;
	//银行通道编号
	public String PayID;
	//订单日期
	public String TradeDate;
	//订单号
	public String TransID;
	//订单金额
	public String OrderMoney;
	//商品名称
	public String ProductName;
	//商品数量
	public String Amount;
	//用户名称
	public String Username;
	//附加字段
	public String AdditionalInfo;
	//通知类型
	public String NoticeType;
	//页面返回地址
	public String PageUrl;
	//交易通知地址
	public String ReturnUrl;
	//交易签名
	public String Md5Sign;
	public String getMemberID() {
		return MemberID;
	}
	public void setMemberID(String memberID) {
		MemberID = memberID;
	}
	public String getTerminalID() {
		return TerminalID;
	}
	public void setTerminalID(String terminalID) {
		TerminalID = terminalID;
	}
	public String getInterfaceVersio() {
		return InterfaceVersio;
	}
	public void setInterfaceVersio(String interfaceVersio) {
		InterfaceVersio = interfaceVersio;
	}
	public String getKeyType() {
		return KeyType;
	}
	public void setKeyType(String keyType) {
		KeyType = keyType;
	}
	public String getPayID() {
		return PayID;
	}
	public void setPayID(String payID) {
		PayID = payID;
	}
	public String getTradeDate() {
		return TradeDate;
	}
	public void setTradeDate(String tradeDate) {
		TradeDate = tradeDate;
	}
	public String getTransID() {
		return TransID;
	}
	public void setTransID(String transID) {
		TransID = transID;
	}
	public String getOrderMoney() {
		return OrderMoney;
	}
	public void setOrderMoney(String orderMoney) {
		OrderMoney = orderMoney;
	}
	public String getProductName() {
		return ProductName;
	}
	public void setProductName(String productName) {
		ProductName = productName;
	}
	public String getAmount() {
		return Amount;
	}
	public void setAmount(String amount) {
		Amount = amount;
	}
	public String getUsername() {
		return Username;
	}
	public void setUsername(String username) {
		Username = username;
	}
	public String getAdditionalInfo() {
		return AdditionalInfo;
	}
	public void setAdditionalInfo(String additionalInfo) {
		AdditionalInfo = additionalInfo;
	}
	public String getNoticeType() {
		return NoticeType;
	}
	public void setNoticeType(String noticeType) {
		NoticeType = noticeType;
	}
	public String getPageUrl() {
		return PageUrl;
	}
	public void setPageUrl(String pageUrl) {
		PageUrl = pageUrl;
	}
	public String getReturnUrl() {
		return ReturnUrl;
	}
	public void setReturnUrl(String returnUrl) {
		ReturnUrl = returnUrl;
	}
	public String getMd5Sign() {
		return Md5Sign;
	}
	public void setMd5Sign(String md5Sign) {
		Md5Sign = md5Sign;
	}
	
	public String toString(){
		return " 商户 ID:"+MemberID + " 终端 ID:" + TerminalID+
			   " 接口版本:"+InterfaceVersio + " 加密类型:" + KeyType+
			   " 银行通道编号:"+PayID + " 订单日期:" + TradeDate+
		       " 订单号:"+TransID + " 订单金额:" + OrderMoney+
		       " 商品名称:"+ProductName + " 商品数量:" + Amount+
		       " 用户名称:"+Username + " 附加字段:" + AdditionalInfo+
		       " 通知类型:"+NoticeType + " 页面返回地址:" + PageUrl+
		       " 交易通知地址:"+ReturnUrl + " 交易签名:" + Md5Sign;
	}
	
	
}
