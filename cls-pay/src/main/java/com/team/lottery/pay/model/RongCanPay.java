package com.team.lottery.pay.model;
public class RongCanPay {
//编码方式
public String charset;
//商户号
public String merchantCode;
//订单号
public String orderNo;
//订单金额
public String amount;
//支付通道
public String channel;
//到收银台的支付方式
public String bankCode;
//订单描述
public String remark;
//异步通知地址
public String notifyUrl;
//页面通知地址
public String returnUrl;
//公用回传参数
public String extraReturnParam;
//签名方式
public String signType;
//数据签名
public String sign;



public String getCharset() {
	return charset;
}



public void setCharset(String charset) {
	this.charset = charset;
}



public String getMerchantCode() {
	return merchantCode;
}



public void setMerchantCode(String merchantCode) {
	this.merchantCode = merchantCode;
}



public String getOrderNo() {
	return orderNo;
}



public void setOrderNo(String orderNo) {
	this.orderNo = orderNo;
}



public String getAmount() {
	return amount;
}



public void setAmount(String amount) {
	this.amount = amount;
}



public String getChannel() {
	return channel;
}



public void setChannel(String channel) {
	this.channel = channel;
}



public String getBankCode() {
	return bankCode;
}



public void setBankCode(String bankCode) {
	this.bankCode = bankCode;
}



public String getRemark() {
	return remark;
}



public void setRemark(String remark) {
	this.remark = remark;
}



public String getNotifyUrl() {
	return notifyUrl;
}



public void setNotifyUrl(String notifyUrl) {
	this.notifyUrl = notifyUrl;
}



public String getReturnUrl() {
	return returnUrl;
}



public void setReturnUrl(String returnUrl) {
	this.returnUrl = returnUrl;
}



public String getExtraReturnParam() {
	return extraReturnParam;
}



public void setExtraReturnParam(String extraReturnParam) {
	this.extraReturnParam = extraReturnParam;
}



public String getSignType() {
	return signType;
}



public void setSignType(String signType) {
	this.signType = signType;
}



public String getSign() {
	return sign;
}



public void setSign(String sign) {
	this.sign = sign;
}



	public String toString(){
		return " 编码方式:"+charset + " 商户号:" + merchantCode+
			   " 订单号:"+orderNo + " 订单金额:" + amount+
			   " 支付通道:"+channel + " 到收银台的支付方式:" + bankCode+
		       " 订单描述:"+remark + " 异步通知地址:" + notifyUrl+
		       " 页面通知地址:"+returnUrl + " 公用回传参数:" + extraReturnParam+
		       " 签名方式:"+signType + " 数据签名:" + sign;
	}
	
	
}
