package com.team.lottery.pay.model.fankepay;

/**
 * 凡客支付发起对象
 * 
 * @author Jamine
 *
 */
public class FanKePay {
	// 商户号.
	private String merchant_no;
	// 系统中唯一订单号.
	private String merchant_order_no;
	// 支付方式.
	private String pay_type;
	// 商户通知地址.
	private String notify_url;
	// 支付成功跳转地址.
	private String return_url;
	// 支付金额.
	private String trade_amount;
	// 备注.
	private String attach;
	// 签名.
	private String sign;

	public String getMerchant_no() {
		return merchant_no;
	}

	public void setMerchant_no(String merchant_no) {
		this.merchant_no = merchant_no;
	}

	public String getMerchant_order_no() {
		return merchant_order_no;
	}

	public void setMerchant_order_no(String merchant_order_no) {
		this.merchant_order_no = merchant_order_no;
	}

	public String getPay_type() {
		return pay_type;
	}

	public void setPay_type(String pay_type) {
		this.pay_type = pay_type;
	}

	public String getNotify_url() {
		return notify_url;
	}

	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}

	public String getReturn_url() {
		return return_url;
	}

	public void setReturn_url(String return_url) {
		this.return_url = return_url;
	}

	public String getTrade_amount() {
		return trade_amount;
	}

	public void setTrade_amount(String trade_amount) {
		this.trade_amount = trade_amount;
	}

	public String getAttach() {
		return attach;
	}

	public void setAttach(String attach) {
		this.attach = attach;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

}
