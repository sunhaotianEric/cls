package com.team.lottery.pay.model;
public class JinYangPay {
	//商户ID
	public String p1_mchtid;
	//支付方式
	public String p2_paytype;
	//支付金额
	public String p3_paymoney;
	//商户平台唯一订单号
	public String p4_orderno;
	//商户异步回调通知地址
	public String p5_callbackurl;
	//商户同步通知地址
	public String p6_notifyurl;
	//版本号
	public String p7_version;
	//签名加密方式
	public String p8_signtype;
	//备注信息
	public String p9_attach;
	//分成标识
	public String p10_appname;
	//是否显示收银台
	public String p11_isshow;
	//商户的用户下单IP
	public String p12_orderip;
	//签名
	public String sign;
    
	public String getP1_mchtid() {
		return p1_mchtid;
	}




	public void setP1_mchtid(String p1_mchtid) {
		this.p1_mchtid = p1_mchtid;
	}




	public String getP2_paytype() {
		return p2_paytype;
	}




	public void setP2_paytype(String p2_paytype) {
		this.p2_paytype = p2_paytype;
	}




	public String getP3_paymoney() {
		return p3_paymoney;
	}




	public void setP3_paymoney(String p3_paymoney) {
		this.p3_paymoney = p3_paymoney;
	}




	public String getP4_orderno() {
		return p4_orderno;
	}




	public void setP4_orderno(String p4_orderno) {
		this.p4_orderno = p4_orderno;
	}




	public String getP5_callbackurl() {
		return p5_callbackurl;
	}




	public void setP5_callbackurl(String p5_callbackurl) {
		this.p5_callbackurl = p5_callbackurl;
	}




	public String getP6_notifyurl() {
		return p6_notifyurl;
	}




	public void setP6_notifyurl(String p6_notifyurl) {
		this.p6_notifyurl = p6_notifyurl;
	}




	public String getP7_version() {
		return p7_version;
	}




	public void setP7_version(String p7_version) {
		this.p7_version = p7_version;
	}




	public String getP8_signtype() {
		return p8_signtype;
	}




	public void setP8_signtype(String p8_signtype) {
		this.p8_signtype = p8_signtype;
	}




	public String getP9_attach() {
		return p9_attach;
	}




	public void setP9_attach(String p9_attach) {
		this.p9_attach = p9_attach;
	}




	public String getP10_appname() {
		return p10_appname;
	}




	public void setP10_appname(String p10_appname) {
		this.p10_appname = p10_appname;
	}




	public String getP11_isshow() {
		return p11_isshow;
	}




	public void setP11_isshow(String p11_isshow) {
		this.p11_isshow = p11_isshow;
	}




	public String getP12_orderip() {
		return p12_orderip;
	}




	public void setP12_orderip(String p12_orderip) {
		this.p12_orderip = p12_orderip;
	}




	public String getSign() {
		return sign;
	}




	public void setSign(String sign) {
		this.sign = sign;
	}




	public String toString(){
		return " 商户ID:"+p1_mchtid + " 支付方式:" + p2_paytype+
			   " 支付金额:"+p3_paymoney + " 商户平台唯一订单号:" + p4_orderno+
			   " 商户异步回调通知地址:"+p5_callbackurl + " 商户同步通知地址:" + p6_notifyurl+
			   " 版本号:"+p7_version + " 签名加密方式:" + p8_signtype+
			   " 备注信息:"+p9_attach + " 分成标识:" + p10_appname+
			   " 是否显示收银台:"+p11_isshow + " 商户的用户下单IP:" + p12_orderip+" 签名:" + sign;
	}
	
	
}
