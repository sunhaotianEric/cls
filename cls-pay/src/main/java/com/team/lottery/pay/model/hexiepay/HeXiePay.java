package com.team.lottery.pay.model.hexiepay;

/**
 * 和谐支付第三方
 * 
 * @author Jamine
 *
 */
public class HeXiePay {
	// 商户号.
	private String pay_memberid;
	// 订单号.
	private String pay_orderid;
	// 提交时间.
	private String pay_applydate;
	// 银行编码.
	private String pay_bankcode;
	// 服务返回地址.
	private String pay_notifyurl;
	// 页面跳转地址.
	private String pay_callbackurl;
	// 支付金额.
	private String pay_amount;
	// md5签名.
	private String pay_md5sign;
	// 商品名称.
	private String pay_productname;

	public String getPay_memberid() {
		return pay_memberid;
	}

	public void setPay_memberid(String pay_memberid) {
		this.pay_memberid = pay_memberid;
	}

	public String getPay_orderid() {
		return pay_orderid;
	}

	public void setPay_orderid(String pay_orderid) {
		this.pay_orderid = pay_orderid;
	}

	public String getPay_applydate() {
		return pay_applydate;
	}

	public void setPay_applydate(String pay_applydate) {
		this.pay_applydate = pay_applydate;
	}

	public String getPay_bankcode() {
		return pay_bankcode;
	}

	public void setPay_bankcode(String pay_bankcode) {
		this.pay_bankcode = pay_bankcode;
	}

	public String getPay_notifyurl() {
		return pay_notifyurl;
	}

	public void setPay_notifyurl(String pay_notifyurl) {
		this.pay_notifyurl = pay_notifyurl;
	}

	public String getPay_callbackurl() {
		return pay_callbackurl;
	}

	public void setPay_callbackurl(String pay_callbackurl) {
		this.pay_callbackurl = pay_callbackurl;
	}

	public String getPay_amount() {
		return pay_amount;
	}

	public void setPay_amount(String pay_amount) {
		this.pay_amount = pay_amount;
	}

	public String getPay_md5sign() {
		return pay_md5sign;
	}

	public void setPay_md5sign(String pay_md5sign) {
		this.pay_md5sign = pay_md5sign;
	}

	public String getPay_productname() {
		return pay_productname;
	}

	public void setPay_productname(String pay_productname) {
		this.pay_productname = pay_productname;
	}

}
