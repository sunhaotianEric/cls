package com.team.lottery.pay.model;
public class RuJinBaoPay {
	//商户ID
	public String merchant_no;
	//银行类型
	public String version;
	//金额
	public String out_trade_no;
	//商户订单号
	public String payment_type;
	//下行异步通知地址
	public String payment_bank;
	//下行同步通知地址
	public String notify_url;
	//备注信息
	public String page_url;
	//MD5签名
	public String total_fee;
	//获取二维码地址
	public String trade_time;
	
	public String user_account;
	
	public String body;
	
	public String channel;
	
	public String sign;
	
	
	

	public String getMerchant_no() {
		return merchant_no;
	}




	public void setMerchant_no(String merchant_no) {
		this.merchant_no = merchant_no;
	}




	public String getVersion() {
		return version;
	}




	public void setVersion(String version) {
		this.version = version;
	}




	public String getOut_trade_no() {
		return out_trade_no;
	}




	public void setOut_trade_no(String out_trade_no) {
		this.out_trade_no = out_trade_no;
	}




	public String getPayment_type() {
		return payment_type;
	}




	public void setPayment_type(String payment_type) {
		this.payment_type = payment_type;
	}




	public String getPayment_bank() {
		return payment_bank;
	}




	public void setPayment_bank(String payment_bank) {
		this.payment_bank = payment_bank;
	}




	public String getNotify_url() {
		return notify_url;
	}




	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}




	public String getPage_url() {
		return page_url;
	}




	public void setPage_url(String page_url) {
		this.page_url = page_url;
	}




	public String getTotal_fee() {
		return total_fee;
	}




	public void setTotal_fee(String total_fee) {
		this.total_fee = total_fee;
	}




	public String getTrade_time() {
		return trade_time;
	}




	public void setTrade_time(String trade_time) {
		this.trade_time = trade_time;
	}




	public String getUser_account() {
		return user_account;
	}




	public void setUser_account(String user_account) {
		this.user_account = user_account;
	}




	public String getBody() {
		return body;
	}




	public void setBody(String body) {
		this.body = body;
	}




	public String getChannel() {
		return channel;
	}




	public void setChannel(String channel) {
		this.channel = channel;
	}




	public String getSign() {
		return sign;
	}




	public void setSign(String sign) {
		this.sign = sign;
	}


	

	public String toString(){
		return " 商户号:"+merchant_no+ " 版本号:" +version+
			   " 商户订单号:"+out_trade_no +" 支付类型:" + payment_type+" 支付银行:"+payment_bank+
			   " 服务器异步通知地址:"+notify_url + " 页面同步跳转地址:" + page_url+
		       " 交易金额:"+total_fee + " 交易时间:" + trade_time+" 用户帐号:"+user_account+
		       " 商品描述:"+body+" 渠道信息:"+channel+" MD5签名结果:"+sign;
	}
	
	
}
