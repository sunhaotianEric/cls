package com.team.lottery.pay.model.hengxingpay;

import com.team.lottery.pay.util.Md5Util;

import java.util.LinkedHashMap;

/**
 * 下单请求参数
 */
public class HengXingPay {
    // 商户号
    private String mcnNum;
    // 订单号
    private String orderId;
    private int payType;

    // payType为21-银联网关时有效
    private String bankCode;
    // 支付金额( 单位:分 )
    private int amount;
    // 通知Url
    private String backUrl;
    // 跳转URL( 此字段非必填不参与签名 ),微信公众号才有用
    private String returnUrl;
    // 签名串
    private String sign;
    // IP
    private String ip;

    public String getMcnNum() {
        return mcnNum;
    }

    public void setMcnNum(String mcnNum) {
        this.mcnNum = mcnNum;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public int getPayType() {
        return payType;
    }

    public void setPayType(int payType) {
        this.payType = payType;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getBackUrl() {
        return backUrl;
    }

    public void setBackUrl(String backUrl) {
        this.backUrl = backUrl;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * 生成请求签名
     * @param secreyKey
     * @return
     */
    public String genReqSign(String secreyKey){
        String reqSign="";
        LinkedHashMap<String, String> mapParam = new LinkedHashMap<String, String>();
        mapParam.put("mcnNum", this.getMcnNum());
        mapParam.put("orderId", this.getOrderId());
        mapParam.put("backUrl", this.getBackUrl());
        mapParam.put("payType", String.valueOf(this.getPayType()));
        mapParam.put("amount", String.valueOf(this.getAmount()));
        mapParam.put("secreyKey", secreyKey);
        reqSign = Md5Util.signMd5(mapParam, "");
        return reqSign;
    }
}
