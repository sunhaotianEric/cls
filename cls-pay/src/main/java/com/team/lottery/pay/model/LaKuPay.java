package com.team.lottery.pay.model;

import java.math.BigDecimal;

public class LaKuPay {

	public String version;
	public int customerid;
	public String sdorderno;
	public BigDecimal total_fee;
	public String paytype;
	public String bankcode;
	public String notifyurl;
	public String returnurl;
	public String remark;
	public String sign;

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public int getCustomerid() {
		return customerid;
	}

	public void setCustomerid(int customerid) {
		this.customerid = customerid;
	}

	public String getSdorderno() {
		return sdorderno;
	}

	public void setSdorderno(String sdorderno) {
		this.sdorderno = sdorderno;
	}

	public BigDecimal getTotal_fee() {
		return total_fee;
	}

	public void setTotal_fee(BigDecimal total_fee) {
		this.total_fee = total_fee;
	}

	public String getPaytype() {
		return paytype;
	}

	public void setPaytype(String paytype) {
		this.paytype = paytype;
	}

	public String getBankcode() {
		return bankcode;
	}

	public void setBankcode(String bankcode) {
		this.bankcode = bankcode;
	}

	public String getNotifyurl() {
		return notifyurl;
	}

	public void setNotifyurl(String notifyurl) {
		this.notifyurl = notifyurl;
	}

	public String getReturnurl() {
		return returnurl;
	}

	public void setReturnurl(String returnurl) {
		this.returnurl = returnurl;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}
	
	public String toString() {
		return " version:" + version + " customerid:" + customerid + " sdorderno:" + sdorderno + " total_fee:" + total_fee + " paytype:" + paytype
				+ " bankcode:" + bankcode + " notifyurl:" + notifyurl + " returnurl:" + returnurl + " remark:" + remark + " sign:"+ sign;
	}

}
