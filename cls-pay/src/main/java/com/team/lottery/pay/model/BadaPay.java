package com.team.lottery.pay.model;
//八达付支付对象
public class BadaPay {
	//当前八达付版本号,接口版本号,目前固定值为3.0
	private String version;
	//接口名称:pay
	private String method;
	//商户ID
	private String partner;
	//请求类型
	private String requesttype;
	//银行类型,default为ALIPAY，现在只支付ALIPAY！
	private String banktype;
	//金额,!!仅支持整数
	private String paymoney;
	//商户订单号
	private String ordernumber;
	//下行异步通知地址
	private String callbackurl;
	//请求时间戳
	private Long timestamp;
	//下行同步通知地址
	private String hrefbackurl;
	//商品名称
	private String goodsname;
	//会员号
	private String memberId;
	//备注信息
	private String attach;
	//MD5签名
	private String sign;
	
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getPartner() {
		return partner;
	}
	public void setPartner(String partner) {
		this.partner = partner;
	}
	public String getRequesttype() {
		return requesttype;
	}
	public void setRequesttype(String requesttype) {
		this.requesttype = requesttype;
	}
	public String getBanktype() {
		return banktype;
	}
	public void setBanktype(String banktype) {
		this.banktype = banktype;
	}
	public String getPaymoney() {
		return paymoney;
	}
	public void setPaymoney(String paymoney) {
		this.paymoney = paymoney;
	}
	public String getOrdernumber() {
		return ordernumber;
	}
	public void setOrdernumber(String ordernumber) {
		this.ordernumber = ordernumber;
	}
	public String getCallbackurl() {
		return callbackurl;
	}
	public void setCallbackurl(String callbackurl) {
		this.callbackurl = callbackurl;
	}

	public Long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}
	public String getHrefbackurl() {
		return hrefbackurl;
	}
	public void setHrefbackurl(String hrefbackurl) {
		this.hrefbackurl = hrefbackurl;
	}
	public String getGoodsname() {
		return goodsname;
	}
	public void setGoodsname(String goodsname) {
		this.goodsname = goodsname;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getAttach() {
		return attach;
	}
	public void setAttach(String attach) {
		this.attach = attach;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	@Override
	public String toString() {
		return "当前 版本: " + version + 
			   "当前 接口方法: " + method + 
			   "商户 ID: " + partner +
			   "请求 类型: " + requesttype +
			   "银行 类型: " + banktype + 
			   "支付 金额: " + paymoney+
			   "订单 号码: " + ordernumber + 
			   "回调 地址: " + callbackurl + 
			   "时间 戳: " + timestamp + 
			   "返回 地址: " + hrefbackurl +
			   "商品 类型: " + goodsname+ 
			   "会员 ID: " + memberId + 
			   "备注 信息: " + attach +
			   "签 名: " + sign;
	}
	
}
