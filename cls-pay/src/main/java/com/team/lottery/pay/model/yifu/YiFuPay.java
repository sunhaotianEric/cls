package com.team.lottery.pay.model.yifu;
/**
 * 易付支付宝
 * @author Owner
 *
 */
public class YiFuPay {
	//当前易付版本号,接口版本号,目前固定值为1.0
	private String version;
	//商户编号
	private String customerid;
	//商户订单号
	private String sdorderno;
	//支付类型
	private String paytype;
	//订单金额
	private String total_fee;
	//异步通知URL
	private String notifyurl;
	//同步跳转URL
	private String returnurl;
	//订单备注说明
	private String remark;
	//md5签名串
	private String sign;
	
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getCustomerid() {
		return customerid;
	}

	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}

	public String getSdorderno() {
		return sdorderno;
	}

	public void setSdorderno(String sdorderno) {
		this.sdorderno = sdorderno;
	}

	public String getPaytype() {
		return paytype;
	}

	public void setPaytype(String paytype) {
		this.paytype = paytype;
	}

	public String getTotal_fee() {
		return total_fee;
	}

	public void setTotal_fee(String total_fee) {
		this.total_fee = total_fee;
	}

	public String getNotifyurl() {
		return notifyurl;
	}

	public void setNotifyurl(String notifyurl) {
		this.notifyurl = notifyurl;
	}

	public String getReturnurl() {
		return returnurl;
	}

	public void setReturnurl(String returnurl) {
		this.returnurl = returnurl;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	@Override
	public String toString() {
		return "YiFuPay [version=" + version + ", customerid=" + customerid + ", sdorderno=" + sdorderno + ", paytype="
				+ paytype + ", total_fee=" + total_fee + ", notifyurl=" + notifyurl + ", returnurl=" + returnurl
				+ ", remark=" + remark + ", sign=" + sign + "]";
	}
	
	
}
