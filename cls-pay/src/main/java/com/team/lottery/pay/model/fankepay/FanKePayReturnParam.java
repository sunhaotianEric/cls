package com.team.lottery.pay.model.fankepay;

/**
 * 凡客支付对象发起,返回报文
 * 
 * @author Jamine
 *
 */
public class FanKePayReturnParam {
	
	// 返回Json数据.
	private String data;
	// 描述.
	private String info;
	// 请求状态码: 1成功，-1失败.
	private String status;
	
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
