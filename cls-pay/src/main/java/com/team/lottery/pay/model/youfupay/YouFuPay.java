
package com.team.lottery.pay.model.youfupay;

/** 
 * @Description: 优付支付,
 * @Author: Jamine.
 * @CreateDate：2019-10-14 01:17:23.
 */
public class YouFuPay {
	// 商户号.
	private String brandNo;
	// 客户端IP.
	private String clientIP;
	// 订单号.
	private String orderNo;
	// 支付金额.
	private String price;
	// 服务类型.
	private String serviceType;
	// 用户名.
	private String userName;
	// 签名值.
	private String signature;
	// 加密方式.
	private String signType;
	// 回调地址.
	private String callbackUrl;
	// 同步跳转地址.
	private String frontUrl;

	public String getBrandNo() {
		return brandNo;
	}

	public void setBrandNo(String brandNo) {
		this.brandNo = brandNo;
	}

	public String getClientIP() {
		return clientIP;
	}

	public void setClientIP(String clientIP) {
		this.clientIP = clientIP;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getSignType() {
		return signType;
	}

	public void setSignType(String signType) {
		this.signType = signType;
	}

	public String getCallbackUrl() {
		return callbackUrl;
	}

	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}

	public String getFrontUrl() {
		return frontUrl;
	}

	public void setFrontUrl(String frontUrl) {
		this.frontUrl = frontUrl;
	}

}
