package com.team.lottery.pay.model.changjiangpay;

/**
 * 长江支付发起对象.
 * 
 * @author jamine
 *
 */
public class ChangJiangPay {
	// 商户号.
	private String merchant_no;
	// 随机字符串.
	private String nonce_str;
	// 签名.
	private String sign;
	// 请求订单号.
	private String request_no;
	// 支付金额(元).
	private String amount;
	// 支付通道.
	private String pay_channel;
	// 时间戳.
	private String request_time;
	// 回调地址.
	private String notify_url;
	// 商品名称.
	private String goods_name;
	// 用户IP地址.
	private String ip_addr;

	public String getMerchant_no() {
		return merchant_no;
	}

	public void setMerchant_no(String merchant_no) {
		this.merchant_no = merchant_no;
	}

	public String getNonce_str() {
		return nonce_str;
	}

	public void setNonce_str(String nonce_str) {
		this.nonce_str = nonce_str;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getRequest_no() {
		return request_no;
	}

	public void setRequest_no(String request_no) {
		this.request_no = request_no;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getPay_channel() {
		return pay_channel;
	}

	public void setPay_channel(String pay_channel) {
		this.pay_channel = pay_channel;
	}

	public String getRequest_time() {
		return request_time;
	}

	public void setRequest_time(String request_time) {
		this.request_time = request_time;
	}

	public String getNotify_url() {
		return notify_url;
	}

	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}

	public String getGoods_name() {
		return goods_name;
	}

	public void setGoods_name(String goods_name) {
		this.goods_name = goods_name;
	}

	public String getIp_addr() {
		return ip_addr;
	}

	public void setIp_addr(String ip_addr) {
		this.ip_addr = ip_addr;
	}

	@Override
	public String toString() {
		return "长江支付发起对象 [商户号=" + merchant_no + ", 随机字符串=" + nonce_str + ", 平台订单号=" + request_no + ", 支付金额=" + amount + "元, 支付通道=" + pay_channel
				+ ", 时间戳=" + request_time + ", 回调地址=" + notify_url + ", 商品名称=" + goods_name
				+ ", ip地址=" + ip_addr +", 签名=" + sign
				+  "]" ;
	}
	
	
}
