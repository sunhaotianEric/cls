
package com.team.lottery.pay.model.xinfa;

/** 
 * @Description: 鑫发支付发起对象.
 * @Author: Jamine.
 * @CreateDate：2019-08-10 02:06:31.
 */
public class XinFaPay {
	// 商户号.
	private String merchNo;
	// 版本号.固定值V3.3.0.0
	private String version;
	// 支付方式.
	private String payType;
	// 8位随机数
	private String randomNum;
	// 订单号.
	private String orderNo;
	// 支付金额.
	private String amount; // 分
	// 商品名称.
	private String goodsName;
	// 异步回调金额地址.
	private String notifyUrl;
	// 支付成功跳转地址
	private String notifyViewUrl;
	// 客户端系统编码格式(UTF-8)
	private String charsetCode;
	// 签名.
	private String sign;

	public String getMerchNo() {
		return merchNo;
	}

	public void setMerchNo(String merchNo) {
		this.merchNo = merchNo;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getRandomNum() {
		return randomNum;
	}

	public void setRandomNum(String randomNum) {
		this.randomNum = randomNum;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getNotifyUrl() {
		return notifyUrl;
	}

	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	public String getNotifyViewUrl() {
		return notifyViewUrl;
	}

	public void setNotifyViewUrl(String notifyViewUrl) {
		this.notifyViewUrl = notifyViewUrl;
	}

	public String getCharsetCode() {
		return charsetCode;
	}

	public void setCharsetCode(String charsetCode) {
		this.charsetCode = charsetCode;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	@Override
	public String toString() {
		return "鑫发支付发起对象 商户号=" + merchNo + ", 版本号=" + version + ", 支付方式=" + payType + ", 随机数=" + randomNum + ", 订单号="
				+ orderNo + ", 支付金额=" + amount + "分, 商品名称=" + goodsName + ", 回调地址=" + notifyUrl + ", 跳转地址=" + notifyViewUrl + ", 客户端编码="
				+ charsetCode + ", 签名=" + sign + "]";
	}

}
