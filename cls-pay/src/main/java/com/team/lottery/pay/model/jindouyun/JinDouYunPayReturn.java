package com.team.lottery.pay.model.jindouyun;

/**
 * 筋斗云支付回调参数对象(用于封装参数).
 * 
 * @author Jamine
 *
 */
public class JinDouYunPayReturn {
	// 响应码.
	private String retCode;
	// 响应信息.
	private String retMsg;
	// 商户号.
	private String merchantCode;
	// 商户订单号.
	private String merchantOrderNo;
	// 平台订单号.
	private String platOrderNo;
	// 订单交易完成时间,不准确.
	private String orderCompleteTime;
	// 订单金额.
	private String orderAmount;
	// 产品名称.
	private String productCode;
	// 支付状态.
	private String payState;
	// 签名.
	private String sign;
	public String getRetCode() {
		return retCode;
	}
	public void setRetCode(String retCode) {
		this.retCode = retCode;
	}
	public String getRetMsg() {
		return retMsg;
	}
	public void setRetMsg(String retMsg) {
		this.retMsg = retMsg;
	}
	public String getMerchantCode() {
		return merchantCode;
	}
	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}
	public String getMerchantOrderNo() {
		return merchantOrderNo;
	}
	public void setMerchantOrderNo(String merchantOrderNo) {
		this.merchantOrderNo = merchantOrderNo;
	}
	public String getPlatOrderNo() {
		return platOrderNo;
	}
	public void setPlatOrderNo(String platOrderNo) {
		this.platOrderNo = platOrderNo;
	}
	public String getOrderCompleteTime() {
		return orderCompleteTime;
	}
	public void setOrderCompleteTime(String orderCompleteTime) {
		this.orderCompleteTime = orderCompleteTime;
	}
	public String getOrderAmount() {
		return orderAmount;
	}
	public void setOrderAmount(String orderAmount) {
		this.orderAmount = orderAmount;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getPayState() {
		return payState;
	}
	public void setPayState(String payState) {
		this.payState = payState;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	
	@Override
	public String toString() {
		return "筋斗云回调参数打印 [响应码=" + retCode + ", 响应信息=" + retMsg + ", 商户号=" + merchantCode + ", 商户订单号=" + merchantOrderNo + ", 平台订单号="
				+ platOrderNo + ", 交易完成时间=" + orderCompleteTime + ", 支付金额=" + orderAmount + ", 产品名称=" + productCode + ", 支付状态=" + payState + ", 签名="
				+ sign + "]";
	}
	
	
}
