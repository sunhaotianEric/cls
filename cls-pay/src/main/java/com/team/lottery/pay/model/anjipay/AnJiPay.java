
package com.team.lottery.pay.model.anjipay;

/** 
 * @Description: 安吉支付发起对象.
 * @Author: Jamine.
 * @CreateDate：2019-05-14 05:24:25.
 */
public class AnJiPay {
	// 商户ID号.
	private String Merchant_ID;
	// 支付类型.
	private String Type;
	// 银行编号.
	private String Bankcode;
	// 商户订单号.
	private String Merchant_Order;
	// 回调地址.
	private String Notice_Url;
	// 订单号金额(单位:分).
	private String Amount;
	// 创建时间(yyyyMMddHHmmss).
	private String Create_Time;
	// 签名类型(固定值: MD5).
	private String Sign_Type;
	// 签名.
	private String Sign;

	public String getMerchant_ID() {
		return Merchant_ID;
	}

	public void setMerchant_ID(String merchant_ID) {
		Merchant_ID = merchant_ID;
	}

	public String getType() {
		return Type;
	}

	public void setType(String type) {
		Type = type;
	}

	public String getBankcode() {
		return Bankcode;
	}

	public void setBankcode(String bankcode) {
		Bankcode = bankcode;
	}

	public String getMerchant_Order() {
		return Merchant_Order;
	}

	public void setMerchant_Order(String merchant_Order) {
		Merchant_Order = merchant_Order;
	}

	public String getNotice_Url() {
		return Notice_Url;
	}

	public void setNotice_Url(String notice_Url) {
		Notice_Url = notice_Url;
	}

	public String getAmount() {
		return Amount;
	}

	public void setAmount(String amount) {
		Amount = amount;
	}

	public String getCreate_Time() {
		return Create_Time;
	}

	public void setCreate_Time(String create_Time) {
		Create_Time = create_Time;
	}

	public String getSign_Type() {
		return Sign_Type;
	}

	public void setSign_Type(String sign_Type) {
		Sign_Type = sign_Type;
	}

	public String getSign() {
		return Sign;
	}

	public void setSign(String sign) {
		Sign = sign;
	}

	@Override
	public String toString() {
		return "安吉支付发起对象 [商户号=" + Merchant_ID + ", 支付类型=" + Type + ", 银行编号=" + Bankcode + ", 平台订单号=" + Merchant_Order + ", 回调地址="
				+ Notice_Url + ", 支付金额=" + Amount + "分, 创建时间=" + Create_Time + ", 加密方式=" + Sign_Type + ", 签名=" + Sign + "]";
	}

}
