package com.team.lottery.pay.model.sixpay;

/**
 * 第六支付发起对象.
 * 
 * @author jamine
 *
 */
public class SixPay {
	
	// 商户ID.
	private String mchNo;
	// 支付类型(目前支持alipayapp).
	private String paytype;
	// 支付金额(元).
	private String money;
	// 平台订单号.
	private String tradeno;
	// 回调地址.
	private String notify_url;
	// 支付成功,同步跳转地址.
	private String returnurl;
	// 支付时间(10位时间戳).
	private String time;
	// 签名.
	private String sign;
	public String getMchNo() {
		return mchNo;
	}
	public void setMchNo(String mchNo) {
		this.mchNo = mchNo;
	}
	public String getPaytype() {
		return paytype;
	}
	public void setPaytype(String paytype) {
		this.paytype = paytype;
	}
	public String getMoney() {
		return money;
	}
	public void setMoney(String money) {
		this.money = money;
	}
	public String getTradeno() {
		return tradeno;
	}
	public void setTradeno(String tradeno) {
		this.tradeno = tradeno;
	}
	public String getNotify_url() {
		return notify_url;
	}
	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}
	public String getReturnurl() {
		return returnurl;
	}
	public void setReturnurl(String returnurl) {
		this.returnurl = returnurl;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	@Override
	public String toString() {
		return "第六支付支付发起参数 [商户号=" + mchNo + ", 支付码表=" + paytype + ", 支付金额=" + money + "元, 订单号=" + tradeno
				+ ", 回调地址=" + notify_url + ", 同步跳转地址=" + returnurl + ", 时间戳=" + time + ", 签名=" + sign + "]";
	}
		
	
}
