package com.team.lottery.pay.model.zhaoqianpay;

import com.team.lottery.util.Md5Util;

/**
 * 兆乾支付回调对象.
 * 
 * @author jamine
 *
 */
public class ZhaoQianPayReturn {
	/*public static void main(String[] args) {
		String md5ofStr = Md5Util.getMD5ofStr("amount=1.00&channelOrderno=02190404100000000923&merchName=北京旺辰日盛&merchno=002110150740001&orderno=02190404100000000923&payType=1&status=1&traceno=CZ1904041606254161000&transDate=2019-04-04&transTime=16:06:26&415A82882D4E9508802E52B1DA72B311", "GBK").toUpperCase();
		System.out.println(md5ofStr);
	}*/
	// 订单状态.
	private String returncode;
	// 商户编号.
	private String memberid;
	// 平台订单号.
	private String orderid;
	// 支付流水号.
	private String transaction_id;
	// 交易金额.
	private String amount;
    // 订单时间.
	private String datetime;
	// md5验证签名串.
	private String sign;
	
	
	
	public String getReturncode() {
		return returncode;
	}



	public void setReturncode(String returncode) {
		this.returncode = returncode;
	}



	public String getMemberid() {
		return memberid;
	}



	public void setMemberid(String memberid) {
		this.memberid = memberid;
	}



	public String getOrderid() {
		return orderid;
	}



	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}



	public String getTransaction_id() {
		return transaction_id;
	}



	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}



	public String getAmount() {
		return amount;
	}



	public void setAmount(String amount) {
		this.amount = amount;
	}



	public String getDatetime() {
		return datetime;
	}



	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}



	public String getSign() {
		return sign;
	}



	public void setSign(String sign) {
		this.sign = sign;
	}



	@Override
	public String toString() {
		return "兆乾支付回调对象 [订单状态=" + returncode + ", 商户编号=" + memberid + ", 平台订单号=" + orderid
				+ ", 支付流水号=" + transaction_id + ", 交易金额=" + amount + ", 订单时间=" + datetime +  ", md5验证签名串=" + sign + "]";
	}
}
