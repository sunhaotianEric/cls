package com.team.lottery.pay.util;

import java.security.MessageDigest;
import java.util.*;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class Md5Util {
	private static final Log log = LogFactory.getLog(Md5Util.class);
	
	public static String getMD5ofStr(String str){
		return getMD5ofStr(str,"utf-8");
	}
	
	public static String getMD5ofStr(String str, String encode) {
		try {
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			md5.update(str.getBytes(encode));
			byte[] digest = md5.digest();

			StringBuffer hexString = new StringBuffer();
			String strTemp;
			for (int i = 0; i < digest.length; i++) {
				// byteVar &
				// 0x000000FF的作用是，如果digest[i]是负数，则会清除前面24个零，正的byte整型不受影响。
				// (...) | 0xFFFFFF00的作用是，如果digest[i]是正数，则置前24位为一，
				// 这样toHexString输出一个小于等于15的byte整型的十六进制时，倒数第二位为零且不会被丢弃，这样可以通过substring方法进行截取最后两位即可。
				strTemp = Integer.toHexString((digest[i] & 0x000000FF) | 0xFFFFFF00).substring(6);
				hexString.append(strTemp);
			}
			return hexString.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}

	}
	
	/**
	 * 获取参数签名
	 * 
	 * @param paramMap
	 *            签名参数
	 * @param paySecret
	 *            签名密钥
	 * @return
	 */
	public static String getSign(Map<String, Object> paramMap,String keyName, String paySecret) {
		SortedMap<String, Object> smap = new TreeMap<String, Object>(paramMap);
		StringBuffer stringBuffer = new StringBuffer();
		for (Map.Entry<String, Object> m : smap.entrySet()) {
			Object value = m.getValue();
			if (value != null && StringUtils.isNotBlank(String.valueOf(value))) {
				stringBuffer.append(m.getKey()).append("=").append(m.getValue()).append("&");
			}
		}
		stringBuffer.delete(stringBuffer.length() - 1, stringBuffer.length());
		/*"&paySecret="*/
		String argPreSign = stringBuffer.append(keyName).append(paySecret).toString();
		//System.out.println(argPreSign);
		log.info("MD5原文:" + argPreSign);
	    Md5Util md5Util=new Md5Util();
		String signStr = md5Util.getMD5ofStr(argPreSign).toUpperCase();
		//String signStr = JinHaoMD5Util.encode(argPreSign).toUpperCase();
		log.info("MD5值：" + signStr);
		return signStr;
	}

	public static void main(String[] args) {
		String infoStr = "version=3.0&method=Rx.online.pay&partner=16960&banktype=ALIPAY&paymoney=2.0.0&ordernumber=20161110161947421&callbackurl=http://www.youdomain.com/callback.aspx270bbae38500459a90b7b2f49a9aa6ba";
		String singStr = getMD5ofStr(infoStr);
		System.out.println(singStr);
	}

	/**
	 * 签名算法
	 * @param mapParam
	 * @param extend
	 * @return
	 */
	public static String signMd5(LinkedHashMap<String, String> mapParam, String extend){
		String signData="", key="", value="";
		for (final Map.Entry<String, String> entry : mapParam.entrySet()) {
			key = entry.getKey();
			value = entry.getValue();
			signData += key+"="+value+"&";
		}
		if(!StringUtils.isEmpty(signData)){
			// 去除最后一个&
			signData = signData.substring(0, signData.length()-1);
		}
		if(!StringUtils.isEmpty(extend)){
			signData += extend;
		}
		return Md5Util.Md5(signData).toUpperCase();
	}

	public static String Md5(String s){
		return  Md5Util.byte2string(Md5Util.Md5(s.getBytes()));
	}

	/**
	 * byte 转字符串
	 * @param array
	 * @return
	 */
	public static String byte2string(final byte[] array) {
		if (array == null) {
			return null;
		}
		final StringBuffer sb = new StringBuffer();
		for (int i = 0; i < array.length; ++i) {
			sb.append(String.format("%02X", array[i]));
		}
		return sb.toString().toLowerCase(Locale.US);
	}

	/**
	 * Md5 算法
	 * @param array
	 * @return
	 */
	public static byte[] Md5(final byte[] array) {
		try {
			final MessageDigest instance = MessageDigest.getInstance("MD5");
			instance.reset();
			instance.update(array);
			return instance.digest();
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	//获取参数签名    参数名ASCII码从小到大排序
	public static String createSign(String signName,SortedMap<Object,Object> parameters,String key){
		StringBuffer sb = new StringBuffer();
		StringBuffer sbkey = new StringBuffer();
		Set es = parameters.entrySet();  //所有参与传参的参数按照accsii排序（升序）
		Iterator it = es.iterator();
		while(it.hasNext()) {
			Map.Entry entry = (Map.Entry)it.next();
			String k = (String)entry.getKey();
			Object v = entry.getValue();
			//空值不传递，不参与签名组串
			if(null != v && !"".equals(v)) {
				sb.append(k + "=" + v + "&");
				sbkey.append(k + "=" + v + "&");
			}
		}
		//System.out.println("字符串:"+sb.toString());
		sbkey=sbkey.append("key="+key);
		log.info("原文:"+sbkey.toString());
		//MD5加密,结果转换为大写字符
		Md5Util md5Util=new Md5Util();
		String sign = md5Util.getMD5ofStr(sbkey.toString()).toUpperCase();
		log.info("密文:"+sign);
		return sign;
	}
}
