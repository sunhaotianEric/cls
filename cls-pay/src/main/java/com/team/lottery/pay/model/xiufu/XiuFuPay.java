package com.team.lottery.pay.model.xiufu;

/**
 * 秀付支付第三方支付.
 * 
 * @author Jamine
 *
 */
public class XiuFuPay {
	// 商户号.
	private String uid;
	// 价格.
	private String price;
	// 支付方式.
	private String istype;
	// 回调地址.
	private String notify_url;
	// 商户订单号.
	private String orderid;
	// 自定义信息.
	private String sp_extdata;
	// 签名.
	private String sign;

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getIstype() {
		return istype;
	}

	public void setIstype(String istype) {
		this.istype = istype;
	}

	public String getNotify_url() {
		return notify_url;
	}

	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}

	public String getOrderid() {
		return orderid;
	}

	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}

	public String getSp_extdata() {
		return sp_extdata;
	}

	public void setSp_extdata(String sp_extdata) {
		this.sp_extdata = sp_extdata;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	@Override
	public String toString() {
		return "XiuFuPay [商户号=" + uid + ", 支付金额=" + price + ", 支付方式=" + istype + ", 回调地址=" + notify_url + ", 订单号=" + orderid + ", 备注信息=" + sp_extdata
				+ ",加密签名=" + sign + "]";
	}
	
	

}
