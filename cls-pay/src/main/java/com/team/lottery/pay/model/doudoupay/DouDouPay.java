
package com.team.lottery.pay.model.doudoupay;

/** 
 * @Description: 豆豆支付,发起对象.
 */
public class DouDouPay {
	// 商户号.
	private String pid;
	// 支付金额
	private String money;
	// 订单号
	private String out_trade_no;
	// 商品名称.
	private String notify_url;
	// out_trade_no.
	private String return_url;
	// 商品名称
	private String name;
	// 支付方式
	private String type;
	// 数据签名
	private String sign;

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	public String getOut_trade_no() {
		return out_trade_no;
	}

	public void setOut_trade_no(String out_trade_no) {
		this.out_trade_no = out_trade_no;
	}

	public String getNotify_url() {
		return notify_url;
	}

	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}

	public String getReturn_url() {
		return return_url;
	}

	public void setReturn_url(String return_url) {
		this.return_url = return_url;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	@Override
	public String toString() {
		return "豆豆支付发起对象 [ 商户号=" + pid + ", 商户订单号=" + out_trade_no
				+ ", 商品名称=" + name + ", 支付金额=" + money + ", 支付方式=" + type + "分, 异步地址=" + notify_url + ", 回调地址=" + notify_url
				+ ", 签名=" + sign + "]";
	}
	
	
}
