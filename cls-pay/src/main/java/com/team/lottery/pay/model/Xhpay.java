package com.team.lottery.pay.model;
public class Xhpay {
	//商户号
	public String merchno;
	//交易金额
	public String amount;
	//商户流水号
	public String traceno;
	//支付方式
	public String payType;
	//商品名称
	public String goodsName;
	//页面通知地址
	public String notifyUrl;
	//交易返回地址
	public String returnUrl;
	//数据签名
	public String signature;
	//
	public String remark;
	//
	public String settleType;
	//
	public String certno;
	//
	public String mobile;
	//
	public String accountno;
	//
	public String accountName;
	//
	public String fee;
	
	
	
	
	public String getMerchno() {
		return merchno;
	}



	public void setMerchno(String merchno) {
		this.merchno = merchno;
	}



	public String getAmount() {
		return amount;
	}



	public void setAmount(String amount) {
		this.amount = amount;
	}



	public String getTraceno() {
		return traceno;
	}



	public void setTraceno(String traceno) {
		this.traceno = traceno;
	}



	public String getPayType() {
		return payType;
	}



	public void setPayType(String payType) {
		this.payType = payType;
	}



	public String getGoodsName() {
		return goodsName;
	}



	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}



	public String getNotifyUrl() {
		return notifyUrl;
	}



	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}



	public String getReturnUrl() {
		return returnUrl;
	}



	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}



	public String getSignature() {
		return signature;
	}



	public void setSignature(String signature) {
		this.signature = signature;
	}
    
	


	public String getRemark() {
		return remark;
	}



	public void setRemark(String remark) {
		this.remark = remark;
	}



	public String getSettleType() {
		return settleType;
	}



	public void setSettleType(String settleType) {
		this.settleType = settleType;
	}



	public String getCertno() {
		return certno;
	}



	public void setCertno(String certno) {
		this.certno = certno;
	}



	public String getMobile() {
		return mobile;
	}



	public void setMobile(String mobile) {
		this.mobile = mobile;
	}



	public String getAccountno() {
		return accountno;
	}



	public void setAccountno(String accountno) {
		this.accountno = accountno;
	}



	public String getAccountName() {
		return accountName;
	}



	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}



	public String getFee() {
		return fee;
	}



	public void setFee(String fee) {
		this.fee = fee;
	}
	
	public String toString(){
		return " 商户 号:"+merchno + " 交易金额:" + amount+
			   " 商户流水号:"+traceno + " 支付方式:" + payType+
			   " 商品名称:"+goodsName + " 页面通知地址:" + notifyUrl+
			   " 备注:"+remark + " 结算方式:" + settleType+
			   " 收款人身份证号:"+certno + " 收款人手机号码:" + mobile+
			   " 收款人账号:"+accountno + " 收款人姓名:" + accountName+" 交易手续费:" + fee+
		       " 交易返回地址:"+returnUrl + " 数据签名:" + signature;
	}
	
	
}
