package com.team.lottery.pay.model;
public class RenXinPay {
	
	//版本号
	public String version;
	//接口名称
	public String method;
	//商户ID
	public String partner;
	//银行类型
	public String banktype;
	//金额
	public String paymoney;
	//商户订单号
	public String ordernumber;
	//下行异步通知地址
	public String callbackurl;
	//下行同步通知地址
	public String hrefbackurl;
	//商品名称
    public String goodsname;
	//备注信息
	public String attach;
	//是否显示收银台
	public String isshow;
	//MD5签名
	public String sign;
	
	
    
	

	public String getVersion() {
		return version;
	}





	public void setVersion(String version) {
		this.version = version;
	}





	public String getMethod() {
		return method;
	}





	public void setMethod(String method) {
		this.method = method;
	}




	public String getPartner() {
		return partner;
	}





	public void setPartner(String partner) {
		this.partner = partner;
	}





	public String getBanktype() {
		return banktype;
	}





	public void setBanktype(String banktype) {
		this.banktype = banktype;
	}





	public String getPaymoney() {
		return paymoney;
	}





	public void setPaymoney(String paymoney) {
		this.paymoney = paymoney;
	}





	public String getOrdernumber() {
		return ordernumber;
	}





	public void setOrdernumber(String ordernumber) {
		this.ordernumber = ordernumber;
	}





	public String getCallbackurl() {
		return callbackurl;
	}





	public void setCallbackurl(String callbackurl) {
		this.callbackurl = callbackurl;
	}





	public String getHrefbackurl() {
		return hrefbackurl;
	}





	public void setHrefbackurl(String hrefbackurl) {
		this.hrefbackurl = hrefbackurl;
	}





	public String getGoodsname() {
		return goodsname;
	}





	public void setGoodsname(String goodsname) {
		this.goodsname = goodsname;
	}





	public String getAttach() {
		return attach;
	}





	public void setAttach(String attach) {
		this.attach = attach;
	}





	public String getIsshow() {
		return isshow;
	}





	public void setIsshow(String isshow) {
		this.isshow = isshow;
	}





	public String getSign() {
		return sign;
	}





	public void setSign(String sign) {
		this.sign = sign;
	}





	public String toString(){
		return " 商户 ID:"+partner+ " 版本号:" +version+" 银行类型:" +banktype+
			   " 金额:"+paymoney +" 商户订单号:" + ordernumber+" 接口名称:"+method+
			   " 下行异步通知地址:"+callbackurl + " 下行同步通知地址:" + hrefbackurl+
		       " 备注信息:"+attach + " MD5签名:" + sign+ " 是否显示收银台:" + isshow;
	}
	
	
}
