package com.team.lottery.pay.model;
public class ZhongBaoPay {
	//商户ID
	public String customer;
	//银行类型 
	public String banktype;
	//交易金额
	public String amount;
	//商户流水号
	public String orderid;
	//异步通知地址
	public String asynbackurl;
	//请求时间
	public String request_time;
	//同步通知地址
	public String synbackurl;
	//获取二维码地址
	public String onlyqr;
	//备注消息 
	public String attach;
	//备注消息 
	public String sign;




	public String getCustomer() {
		return customer;
	}




	public void setCustomer(String customer) {
		this.customer = customer;
	}




	public String getBanktype() {
		return banktype;
	}




	public void setBanktype(String banktype) {
		this.banktype = banktype;
	}




	public String getAmount() {
		return amount;
	}




	public void setAmount(String amount) {
		this.amount = amount;
	}




	public String getOrderid() {
		return orderid;
	}




	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}




	public String getAsynbackurl() {
		return asynbackurl;
	}




	public void setAsynbackurl(String asynbackurl) {
		this.asynbackurl = asynbackurl;
	}




	public String getRequest_time() {
		return request_time;
	}




	public void setRequest_time(String request_time) {
		this.request_time = request_time;
	}




	public String getSynbackurl() {
		return synbackurl;
	}




	public void setSynbackurl(String synbackurl) {
		this.synbackurl = synbackurl;
	}




	public String getOnlyqr() {
		return onlyqr;
	}




	public void setOnlyqr(String onlyqr) {
		this.onlyqr = onlyqr;
	}




	public String getAttach() {
		return attach;
	}




	public void setAttach(String attach) {
		this.attach = attach;
	}




	public String getSign() {
		return sign;
	}




	public void setSign(String sign) {
		this.sign = sign;
	}




	public String toString(){
		return " 商户 号:"+customer + " 交易金额:" + amount+
			   " 商户流水号:"+orderid + " 支付方式:" + banktype+
			   " 通知地址:" + synbackurl+
			   " 备注:"+attach +" 数据签名:" + sign +"" ;
	}
	
	
}
