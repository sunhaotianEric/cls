package com.team.lottery.pay.model;
public class KuaiKuaiRuPay {
	//商户uid
	public String uid;
	//价格
	public String price;
	//支付渠道
	public String istype;
	//通知回调网址	
	public String notify_url;
	//跳转网址
	public String return_url;
	//商户自定义订单号
	public String orderid;
	//商户自定义客户号	
	public String orderuid;
	//商品名称
	public String goodsname;
	//附加内容
	public String attach;
	//秘钥
	public String key;
	
	
	
	public String getUid() {
		return uid;
	}



	public void setUid(String uid) {
		this.uid = uid;
	}



	public String getPrice() {
		return price;
	}



	public void setPrice(String price) {
		this.price = price;
	}



	public String getIstype() {
		return istype;
	}



	public void setIstype(String istype) {
		this.istype = istype;
	}



	public String getNotify_url() {
		return notify_url;
	}



	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}



	public String getReturn_url() {
		return return_url;
	}



	public void setReturn_url(String return_url) {
		this.return_url = return_url;
	}



	public String getOrderid() {
		return orderid;
	}



	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}



	public String getOrderuid() {
		return orderuid;
	}



	public void setOrderuid(String orderuid) {
		this.orderuid = orderuid;
	}



	public String getGoodsname() {
		return goodsname;
	}



	public void setGoodsname(String goodsname) {
		this.goodsname = goodsname;
	}



	public String getAttach() {
		return attach;
	}



	public void setAttach(String attach) {
		this.attach = attach;
	}



	public String getKey() {
		return key;
	}



	public void setKey(String key) {
		this.key = key;
	}



	public String toString(){
		return " 商户uid:"+uid + " 价格:" + price+
			   " 支付渠道:"+istype + " 通知回调网址:" + notify_url+
			   " 跳转网址:"+return_url + " 商户自定义订单号:" + orderid+
		       " 商户自定义客户号:"+orderuid +" 商品名称:"+goodsname +
		       " 附加内容:" + attach+ " 秘钥:" + key;
	}
	
	
}
