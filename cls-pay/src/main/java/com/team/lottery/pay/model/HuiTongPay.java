package com.team.lottery.pay.model;
/**
 * 汇通支付
 *
 */
public class HuiTongPay {
	/**
	 * 服务器异步通知地址
	 */
	private String notify_url;
	
	/**
	 * 页面同步跳转通知地址
	 */
	private String return_url;
	
	/**
	 * 支付方式
	 */
	private String pay_type;
	
	/**
	 * 银行编码
	 */
	private String bank_code;
	
	/**
	 * 商户号
	 */
	private String merchant_code;
	
	/**
	 * 商户订单号
	 */
	private String order_no;
	
	/**
	 * 订单金额
	 */
	private String order_amount;
	
	/**
	 * 订单时间
	 */
	private String order_time;
	
	
	/**
	 * 消费者id
	 */
	private String customer_ip;
	
	
	/**
	 * 来路域名
	 */
	private String req_referer;
	
	/**
	 * 回传参数
	 */
	private String return_params;
	
	/**
	 * 签名
	 */
	private String sign;

	public String getNotify_url() {
		return notify_url;
	}

	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}

	public String getReturn_url() {
		return return_url;
	}

	public void setReturn_url(String return_url) {
		this.return_url = return_url;
	}

	public String getPay_type() {
		return pay_type;
	}

	public void setPay_type(String pay_type) {
		this.pay_type = pay_type;
	}

	public String getBank_code() {
		return bank_code;
	}

	public void setBank_code(String bank_code) {
		this.bank_code = bank_code;
	}

	public String getMerchant_code() {
		return merchant_code;
	}

	public void setMerchant_code(String merchant_code) {
		this.merchant_code = merchant_code;
	}

	public String getOrder_no() {
		return order_no;
	}

	public void setOrder_no(String order_no) {
		this.order_no = order_no;
	}

	public String getOrder_amount() {
		return order_amount;
	}

	public void setOrder_amount(String order_amount) {
		this.order_amount = order_amount;
	}

	public String getOrder_time() {
		return order_time;
	}

	public void setOrder_time(String order_time) {
		this.order_time = order_time;
	}

	public String getCustomer_ip() {
		return customer_ip;
	}

	public void setCustomer_ip(String customer_ip) {
		this.customer_ip = customer_ip;
	}

	public String getReq_referer() {
		return req_referer;
	}

	public void setReq_referer(String req_referer) {
		this.req_referer = req_referer;
	}

	public String getReturn_params() {
		return return_params;
	}

	public void setReturn_params(String return_params) {
		this.return_params = return_params;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}
}
