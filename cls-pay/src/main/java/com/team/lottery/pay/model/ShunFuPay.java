package com.team.lottery.pay.model;
public class ShunFuPay {
	//商户号
	public String merNo;
	//支付网关
	public String payNetway;
	//随机数
	public String random;
	//订单号
	public String orderNo;
	//金额
	public String amount;
	//商品信息
	public String goodsInfo;
	//支付结果通知地址
	public String callBackUrl;
	//回显地址
	public String callBackViewUrl;
	//客户IP地址
	public String clientIP;
	//签名
	public String sign;
   
    

	public String getMerNo() {
		return merNo;
	}



	public void setMerNo(String merNo) {
		this.merNo = merNo;
	}



	public String getPayNetway() {
		return payNetway;
	}



	public void setPayNetway(String payNetway) {
		this.payNetway = payNetway;
	}



	public String getRandom() {
		return random;
	}



	public void setRandom(String random) {
		this.random = random;
	}



	public String getOrderNo() {
		return orderNo;
	}



	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}



	public String getAmount() {
		return amount;
	}



	public void setAmount(String amount) {
		this.amount = amount;
	}



	public String getGoodsInfo() {
		return goodsInfo;
	}



	public void setGoodsInfo(String goodsInfo) {
		this.goodsInfo = goodsInfo;
	}



	public String getCallBackUrl() {
		return callBackUrl;
	}



	public void setCallBackUrl(String callBackUrl) {
		this.callBackUrl = callBackUrl;
	}



	public String getCallBackViewUrl() {
		return callBackViewUrl;
	}



	public void setCallBackViewUrl(String callBackViewUrl) {
		this.callBackViewUrl = callBackViewUrl;
	}



	public String getClientIP() {
		return clientIP;
	}



	public void setClientIP(String clientIP) {
		this.clientIP = clientIP;
	}



	public String getSign() {
		return sign;
	}



	public void setSign(String sign) {
		this.sign = sign;
	}



	public String toString(){
		return " 商户号:"+merNo + " 支付网关:" + payNetway+
			   " 随机数:"+random + " 订单号:" + orderNo+
			   " 金额:"+amount + " 商品信息:" + goodsInfo+
			   " 支付结果通知地址:"+callBackUrl + " 回显地址:" + callBackViewUrl+
			   " 客户IP地址:"+clientIP + " 签名:" + sign;
	}
	
	
}
