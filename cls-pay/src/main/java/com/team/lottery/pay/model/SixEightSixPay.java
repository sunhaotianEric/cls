package com.team.lottery.pay.model;

public class SixEightSixPay {
	//下行异步通知的地址
	public String notifyUrl;
	//MD5签名
	public String sign;
	//外部订单号
	public String outOrderNo;
	//商品名称
	public String goodsClauses;
	//交易金额
	public String tradeAmount;
	//商户code
	public String code;
	//支付类型
	public String payCode;
	
	public String getNotifyUrl() {
		return notifyUrl;
	}

	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getOutOrderNo() {
		return outOrderNo;
	}

	public void setOutOrderNo(String outOrderNo) {
		this.outOrderNo = outOrderNo;
	}

	public String getGoodsClauses() {
		return goodsClauses;
	}

	public void setGoodsClauses(String goodsClauses) {
		this.goodsClauses = goodsClauses;
	}

	public String getTradeAmount() {
		return tradeAmount;
	}

	public void setTradeAmount(String tradeAmount) {
		this.tradeAmount = tradeAmount;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getPayCode() {
		return payCode;
	}

	public void setPayCode(String payCode) {
		this.payCode = payCode;
	}

		public String toString(){
			return " 下行异步通知的地址:"+notifyUrl + " MD5签名:" + sign+
				   " 外部订单号:"+outOrderNo + " 商品名称:" + goodsClauses+
				   " 交易金额:"+tradeAmount + " 商户code:" + code+
			       " 支付类型:"+payCode;
		}

}
