package com.team.lottery.pay.model.hujingpay;

/**
 * 虎鲸支付,参数对象封装.
 * 
 * @author Jamine
 *
 */
public class HuJingPayReturn {
	// 商家号.
	private String merchantId;
	// 商户系统中的用户ID.
	private String merchantUid;
	// 商户订单号.
	private String merchantOrderId;
	// 平台订单号.
	private String orderId;
	// 商家订单金额.
	private String money;
	// 实际支付金额.
	private String payAmount;
	// 支付方式.
	private String payType;
	// 支付时间.
	private String payTime;
	// 签名.
	private String sign;
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getMerchantUid() {
		return merchantUid;
	}
	public void setMerchantUid(String merchantUid) {
		this.merchantUid = merchantUid;
	}
	public String getMerchantOrderId() {
		return merchantOrderId;
	}
	public void setMerchantOrderId(String merchantOrderId) {
		this.merchantOrderId = merchantOrderId;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getMoney() {
		return money;
	}
	public void setMoney(String money) {
		this.money = money;
	}
	public String getPayAmount() {
		return payAmount;
	}
	public void setPayAmount(String payAmount) {
		this.payAmount = payAmount;
	}
	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}
	public String getPayTime() {
		return payTime;
	}
	public void setPayTime(String payTime) {
		this.payTime = payTime;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	@Override
	public String toString() {
		return "虎鲸回调参数打印 [商户号=" + merchantId + ", 商户平台唯一用户ID=" + merchantUid + ", 商户产生的唯一订单号=" + merchantOrderId + ", 虎鲸平台订单号 =" + orderId + ", 商家订单金额=" + money
				+ ", 实际支付金额=" + payAmount + ", 支付方式=" + payType + ", 支付时间=" + payTime + ", 签名=" + sign + "]";
	}
	

}
