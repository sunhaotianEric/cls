package com.team.lottery.pay.model;

public class LianYingPayReturn {
	//结果
	private String ret;
	//支付路径
	private String msg;
	//金额:单位分
	private String amount;
	//平台订单号
	private String orderId;
	//时间戳
	private String time;
	//图片
	private String qrImg;
	//支付方式
	private String paymentType;
	public String getRet() {
		return ret;
	}
	public void setRet(String ret) {
		this.ret = ret;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getQrImg() {
		return qrImg;
	}
	public void setQrImg(String qrImg) {
		this.qrImg = qrImg;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	
	
	
}
