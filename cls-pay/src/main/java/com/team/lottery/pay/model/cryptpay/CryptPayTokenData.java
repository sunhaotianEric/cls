package com.team.lottery.pay.model.cryptpay;

public class CryptPayTokenData {

	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	
}
