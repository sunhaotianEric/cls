package com.team.lottery.pay.model.shandepay;

/**
 * 杉德二维码支付.
 * 
 * @author Jamine
 *
 */
public class ShanDePayQr {

	// 支付方式,0401支付宝扫码,0402微信扫码,0403银联扫码.
	private String payTool;
	// 商户订单号.
	private String orderCode;
	// 订单金额.
	private String totalAmount;
	// 订单标题.
	private String subject;

	// 异步通知地址.
	private String notifyUrl;

	public String getPayTool() {
		return payTool;
	}

	public void setPayTool(String payTool) {
		this.payTool = payTool;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getNotifyUrl() {
		return notifyUrl;
	}

	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

}
