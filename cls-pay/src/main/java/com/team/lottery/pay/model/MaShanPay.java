package com.team.lottery.pay.model;
/**
 * 码闪付支付对象
 * @author hahah
 *
 */
public class MaShanPay {
	//商户号码
	private String merNo;
	//充值的金额单位为分
	private Integer amount;
	//支付类型QQ,ZFB，ZFBWAP,QQH5
	private String channelCode;
	//商品名称
	private String goodsName;
	//订单号码
	private String orderNum;
	//商户号码
	private String organizationCode;
	//回显地址
	private String payViewUrl;
	//备注
	private String remark;
	//加密报文
	private String data;
	//签名
	private String sign;
	public String getMerNo() {
		return merNo;
	}
	public void setMerNo(String merNo) {
		this.merNo = merNo;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	public String getChannelCode() {
		return channelCode;
	}
	public void setChannelCode(String channelCode) {
		this.channelCode = channelCode;
	}
	public String getGoodsName() {
		return goodsName;
	}
	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}
	public String getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}
	public String getOrganizationCode() {
		return organizationCode;
	}
	public void setOrganizationCode(String organizationCode) {
		this.organizationCode = organizationCode;
	}
	public String getPayViewUrl() {
		return payViewUrl;
	}
	public void setPayViewUrl(String payViewUrl) {
		this.payViewUrl = payViewUrl;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	@Override
	public String toString() {
		return "码闪付对象信息 [商户号=" + merNo + ", 充值金额=" + amount/100 + ", 充值方式=" + channelCode + ", 商品名称=" + goodsName + ", 订单号码=" + orderNum + ", 商户号="
				+ organizationCode + ", 回调地址=" + payViewUrl + ", 备注信息=" + remark + ", 签名=" + sign + "]";
	}
	
}
