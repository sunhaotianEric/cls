package com.team.lottery.pay.util.shandeutil;


import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

public class FastPayServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * 签名报文数据并且返回签名信息
     *
     * @param req
     * @param resp
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {

        // 获取支付报文参数
        @SuppressWarnings("unchecked")
		Map<String, String[]> params = req.getParameterMap();
        Map<String, String> map = new HashMap<String, String>();
        for (String key : params.keySet()) {
            String[] values = params.get(key);
            if (values.length > 0) {
                map.put(key, values[0]);
            }
        }

        // 组后台报文
        JSONObject head = new JSONObject();
        head.put("version", map.get("version"));
        head.put("method", map.get("method"));
        head.put("productId", map.get("productId"));
        head.put("accessType", map.get("accessType"));
        head.put("mid", map.get("mid"));
        head.put("plMid", map.get("plMid"));
        head.put("channelType", map.get("channelType"));
        head.put("reqTime", map.get("reqTime"));

        JSONObject body = new JSONObject();
        body.put("userId", map.get("userId"));
        body.put("clearCycle", map.get("clearCycle"));
        body.put("currencyCode", map.get("currencyCode"));
        body.put("frontUrl", map.get("frontUrl"));
        body.put("notifyUrl", map.get("notifyUrl"));
        body.put("orderCode", map.get("orderCode"));
        body.put("orderTime", map.get("orderTime"));
        body.put("totalAmount", map.get("totalAmount"));
        body.put("body", map.get("body"));
        body.put("subject", map.get("subject"));
        body.put("extend", map.get("extend"));

        JSONObject data = new JSONObject();
        data.put("head", head);
        data.put("body", body);
        try {
            // 签名
            String reqSign = URLEncoder.encode(new String(
                    Base64.encodeBase64(CryptoUtil.digitalSign(JSON.toJSONString(data).getBytes("UTF-8"),
                            CertUtil.getPrivateKey(), "SHA1WithRSA"))), "UTF-8");
            JSONObject r = new JSONObject();
            r.put("data", JSON.toJSONString(data));
            r.put("sign", reqSign);// 签名串
            resp.getOutputStream().print(r.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
