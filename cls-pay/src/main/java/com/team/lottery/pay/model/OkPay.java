package com.team.lottery.pay.model;
public class OkPay {
    //当前接口版本号
	public String version;
	//用户ID
	public String partner;
	//商户订单号
	public String orderid;
	//订单总金额
	public String payamount;
	//用户所在客户端的真实ip
	public String payip;
	//支付后返回的商户处理页面
	public String notifyurl;
	//支付后返回的商户显示页面
	public String returnurl;
	//支付类型
	public String paytype;
	//商家自定义数据包
	public String remark;
	//MD5签名
	public String sign;
	
	
	public String getVersion() {
		return version;
	}


	public void setVersion(String version) {
		this.version = version;
	}


	public String getPartner() {
		return partner;
	}


	public void setPartner(String partner) {
		this.partner = partner;
	}


	public String getOrderid() {
		return orderid;
	}


	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}


	public String getPayamount() {
		return payamount;
	}


	public void setPayamount(String payamount) {
		this.payamount = payamount;
	}


	public String getPayip() {
		return payip;
	}


	public void setPayip(String payip) {
		this.payip = payip;
	}


	public String getNotifyurl() {
		return notifyurl;
	}


	public void setNotifyurl(String notifyurl) {
		this.notifyurl = notifyurl;
	}


	public String getReturnurl() {
		return returnurl;
	}


	public void setReturnurl(String returnurl) {
		this.returnurl = returnurl;
	}


	public String getPaytype() {
		return paytype;
	}


	public void setPaytype(String paytype) {
		this.paytype = paytype;
	}


	public String getRemark() {
		return remark;
	}


	public void setRemark(String remark) {
		this.remark = remark;
	}


	public String getSign() {
		return sign;
	}


	public void setSign(String sign) {
		this.sign = sign;
	}


	public String toString(){
		return " 当前接口版本号:"+version + " 用户ID:" + partner+
			   " 接口版本:"+orderid + " 订单总金额:" + payamount+
			   " 用户所在客户端的真实ip:"+payip + " 支付后返回的商户处理页面:" + notifyurl+
		       " 支付后返回的商户显示页面:"+returnurl + " 支付类型:" + paytype+
		       " 商家自定义数据包:"+remark + " MD5签名:" + sign;
	}
	
	
}
