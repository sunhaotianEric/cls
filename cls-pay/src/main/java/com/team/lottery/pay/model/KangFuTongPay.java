package com.team.lottery.pay.model;
public class KangFuTongPay {
	//商家号
	public String merchant_code;
	//服务类型
	public String service_type;
	//服务器异步通知地址
	public String notify_url;
	//接口版本
	public String interface_version;
	//参数编码字符集
	public String input_charset;
	//签名方式
	public String sign_type;
	//签名数据
	public String sign;
	//页面跳转同步通知地址
	public String return_url;
	//支付类型
	public String pay_type;
	//客户端IP
	public String client_ip;
	//客户端IP是否校验标识
	public String client_ip_check;
	//商家订单号
	public String order_no;
	//商家订单时间 
	public String order_time;
	//商家订单金额
	public String order_amount;
	//网银直连银行代码
	public String bank_code;
	//是否允许重复订单
	public String redo_flag;
	//商品名称
	public String product_name;
	//商品编号
	public String product_code;
	//商品数量
	public String product_num;
	//商品描述
	public String product_desc;
	//回传参数
	public String extra_return_param;
	//业务扩展参数
	public String extend_param;
	//商品展示URL
	public String show_url;
	//储存子订单的相关信息
	public String orders_info;

	
	


	

	public String getMerchant_code() {
		return merchant_code;
	}







	public void setMerchant_code(String merchant_code) {
		this.merchant_code = merchant_code;
	}







	public String getService_type() {
		return service_type;
	}







	public void setService_type(String service_type) {
		this.service_type = service_type;
	}







	public String getNotify_url() {
		return notify_url;
	}







	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}







	public String getInterface_version() {
		return interface_version;
	}







	public void setInterface_version(String interface_version) {
		this.interface_version = interface_version;
	}







	public String getInput_charset() {
		return input_charset;
	}







	public void setInput_charset(String input_charset) {
		this.input_charset = input_charset;
	}







	public String getSign_type() {
		return sign_type;
	}







	public void setSign_type(String sign_type) {
		this.sign_type = sign_type;
	}







	public String getSign() {
		return sign;
	}







	public void setSign(String sign) {
		this.sign = sign;
	}







	public String getReturn_url() {
		return return_url;
	}







	public void setReturn_url(String return_url) {
		this.return_url = return_url;
	}







	public String getPay_type() {
		return pay_type;
	}







	public void setPay_type(String pay_type) {
		this.pay_type = pay_type;
	}







	public String getClient_ip() {
		return client_ip;
	}







	public void setClient_ip(String client_ip) {
		this.client_ip = client_ip;
	}







	public String getClient_ip_check() {
		return client_ip_check;
	}







	public void setClient_ip_check(String client_ip_check) {
		this.client_ip_check = client_ip_check;
	}







	public String getOrder_no() {
		return order_no;
	}







	public void setOrder_no(String order_no) {
		this.order_no = order_no;
	}







	public String getOrder_time() {
		return order_time;
	}







	public void setOrder_time(String order_time) {
		this.order_time = order_time;
	}







	public String getOrder_amount() {
		return order_amount;
	}







	public void setOrder_amount(String order_amount) {
		this.order_amount = order_amount;
	}







	public String getBank_code() {
		return bank_code;
	}







	public void setBank_code(String bank_code) {
		this.bank_code = bank_code;
	}







	public String getRedo_flag() {
		return redo_flag;
	}







	public void setRedo_flag(String redo_flag) {
		this.redo_flag = redo_flag;
	}







	public String getProduct_name() {
		return product_name;
	}







	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}







	public String getProduct_code() {
		return product_code;
	}







	public void setProduct_code(String product_code) {
		this.product_code = product_code;
	}







	public String getProduct_num() {
		return product_num;
	}







	public void setProduct_num(String product_num) {
		this.product_num = product_num;
	}







	public String getProduct_desc() {
		return product_desc;
	}







	public void setProduct_desc(String product_desc) {
		this.product_desc = product_desc;
	}







	public String getExtra_return_param() {
		return extra_return_param;
	}







	public void setExtra_return_param(String extra_return_param) {
		this.extra_return_param = extra_return_param;
	}







	public String getExtend_param() {
		return extend_param;
	}







	public void setExtend_param(String extend_param) {
		this.extend_param = extend_param;
	}







	public String getShow_url() {
		return show_url;
	}







	public void setShow_url(String show_url) {
		this.show_url = show_url;
	}







	public String getOrders_info() {
		return orders_info;
	}







	public void setOrders_info(String orders_info) {
		this.orders_info = orders_info;
	}
		

	public String toString(){
		return " 商家号:"+merchant_code + " 服务类型:" + service_type+
			   " 服务器异步通知地址:"+notify_url + " 接口版本:" + interface_version+
			   " 参数编码字符集:"+input_charset + " 签名方式:" + sign_type+
			   " 签名数据:"+sign + " 页面跳转同步通知地址:" + return_url+
			   " 支付类型:"+pay_type + " 客户端IP:" + client_ip+
			   " 客户端IP是否校验标识:"+client_ip_check + " 商家订单号:" + order_no+
			   " 商家订单时间:"+order_time + " 商家订单金额:" + order_amount+
			   " 网银直连银行代码:"+bank_code + " 是否允许重复订单:" + redo_flag+
			   " 商品名称:"+product_name + " 商品编号:" + product_code+
			   " 商品数量:"+product_num + " 商品描述:" + product_desc+
			   " 回传参数:"+extra_return_param + " 业务扩展参数:" + extend_param+
			   " 商品展示URL:"+show_url + " 储存子订单的相关信息:" + orders_info;
			 
	}
	
	
}
