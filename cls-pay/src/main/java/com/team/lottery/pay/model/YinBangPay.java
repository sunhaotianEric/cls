package com.team.lottery.pay.model;

import com.team.lottery.util.GsonUtil;

/**
 * 银邦支付
 * @author Administrator
 *
 */
public class YinBangPay {	
	public static final String serverPublicKey= "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCabJDz/66tGW6J0SBHI3zTqz+vB7lkBwEcSnnaNJ6mAZ64Garc4Ax9lcFV9aUI3/v/w7LRnhPRnMCHc9HeBFS66jPixlvk3cB/TYsVoxuQInTE/VmQDv+9cRlKYpemULGr6VoeOzAoEHz68g/YUZCjFBxbhTyOKutBoCorsAmQeQIDAQAB";
	public static final String privateKey= "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBANhDOTbBWaXDylC9"
											+"mko+0J42fcJGFlVTaWPSwppqgPfkXApPxg0dxKBgpJDYCBtEmu7Ewh+J6p1AtVW7"
											+"GRwrERKcFD7zoAVLq0+NuVmFYi3wk2HtCUBL12Q+ypFqGn3VNKVbSpv5MA9naRRC"
											+"JmNDMk3Ip9w+f/3LPaC3LwsZ+J3DAgMBAAECgYEAqmk+WlE9jQudDsZ5H+wjg6g4"
											+"rK+Bxba2ozxesEMgYTYG+PEnhQph+GZvBtvUd3g2BdPxi9ynjUaCHY4TGUkuZ2LM"
											+"MI3v6RPB0C1Qa7A6FfC7Q5gbFqvZND/eRaRkjIMgibothtf0xNc13n4IUc1jEdiH"
											+"7ToBCHxMGPd0VglavUECQQDvArfGtm/nNDhn+TxqSQSNYpNKWSySWg3BvdeYTXDu"
											+"DIJt0lSHcOadGTCBYbvYOxOOkwBMDGGYCJV7nKkEBxsRAkEA56KPKnOMJ2NU0cW9"
											+"d95ZOvjKBLzpkFhtDur4P04ArhqjgQolDcWPYnwei8L/xEAm2J+7RP9pLIMYATL/"
											+"xqTjkwJAIXBe59VEaLN8pMdQ9YnLskA1XTSGsN8ah52Y2T0UbKmhYd4IZ36bNSV+"
											+"DMvwrPru7IaShxFAYjXpPAcZ+m9IEQJAKwcpbQtVx6NOCEGhNnkZPjkk3chx4pxq"
											+"ddD0GF5XBgBwyvGlfG9seTDxQ6kPddcs4CSb1u9TDC+179MXbEEI/QJAUYxnOU1B"
											+"82rGOPRd3ajLZh5fVQ5e/mwmfKGQufMq1jGJBzdU2aRvh3aA16/Zg5z9edNbDhBe"
											+"aOXQc4sZX/yNGQ==";
	/**
	 * 签名
	 */
	private String sign;
	
	/**
	 * 商务号
	 */
	private String merId;

	/**
	 * 金额
	 */
	private String version;
	
	/**
	 * 商户号（终端号）
	 */
	private String terId;
	
	/**
	 * 商务订单号
	 */
	private String businessOrdid;
	
	/**
	 * 订单名称
	 */
	private String orderName;
	
	/**
	 * 订单金额
	 */
	private String tradeMoney;
	
	/**
	 * 自定义信息
	 */
	private String selfParam;
	
	/**
	 * 支付方式
	 */
	private String payType;
	
	
	/**
	 * 应用场景,获取二维码不用这字段
	 */
	private String appSence;
	
	/**
	 * 下行同步通知地址,获取二维码不用这字段
	 */
	private String syncURL;
	/**
	 * 下行异步通知地址
	 */
	private String asynURL;


	public String getSign() {
		return sign;
	}


	public void setSign(String sign) {
		this.sign = sign;
	}


	public String getMerId() {
		return merId;
	}


	public void setMerId(String merId) {
		this.merId = merId;
	}


	public String getVersion() {
		return version;
	}


	public void setVersion(String version) {
		this.version = version;
	}


	public String getTerId() {
		return terId;
	}


	public void setTerId(String terId) {
		this.terId = terId;
	}


	public String getBusinessOrdid() {
		return businessOrdid;
	}


	public void setBusinessOrdid(String businessOrdid) {
		this.businessOrdid = businessOrdid;
	}


	public String getOrderName() {
		return orderName;
	}


	public void setOrderName(String orderName) {
		this.orderName = orderName;
	}


	public String getTradeMoney() {
		return tradeMoney;
	}


	public void setTradeMoney(String tradeMoney) {
		this.tradeMoney = tradeMoney;
	}


	public String getSelfParam() {
		return selfParam;
	}


	public void setSelfParam(String selfParam) {
		this.selfParam = selfParam;
	}


	public String getPayType() {
		return payType;
	}


	public void setPayType(String payType) {
		this.payType = payType;
	}


	public String getAppSence() {
		return appSence;
	}


	public void setAppSence(String appSence) {
		this.appSence = appSence;
	}


	public String getSyncURL() {
		return syncURL;
	}


	public void setSyncURL(String syncURL) {
		this.syncURL = syncURL;
	}


	public String getAsynURL() {
		return asynURL;
	}


	public void setAsynURL(String asynURL) {
		this.asynURL = asynURL;
	}


	public String toString(){
		return " 商户 号:"+terId + " 交易金额:" + tradeMoney+
			   " 商户流水号:"+businessOrdid + " 支付方式:" + payType+
			   " 通知地址:" + asynURL+
			   " 备注:"+selfParam +" 数据签名:" + sign +"" ;
	}
	
	public static void main(String[] args) {
		YinBangPay yinBangPay = new YinBangPay();
		yinBangPay.setAppSence("45");
		yinBangPay.setMerId("");
		yinBangPay.setBusinessOrdid("");
		String json = GsonUtil.toJson(yinBangPay);
		System.out.println(json);
	}
}
