package com.team.lottery.pay.model;
public class JinHaoPay {
	
public String payKey;

public String orderPrice;

public String outTradeNo;

public String productType;

public String orderTime;

public String productName;

public String orderIp;

public String returnUrl;

public String notifyUrl;

public String subPayKey;

public String remark;

public String sign;

//B2C附加属性

public String bankCode;

public String bankAccountType;

public String mobile;

public String getPayKey() {
	return payKey;
}

public void setPayKey(String payKey) {
	this.payKey = payKey;
}

public String getOrderPrice() {
	return orderPrice;
}

public void setOrderPrice(String orderPrice) {
	this.orderPrice = orderPrice;
}

public String getOutTradeNo() {
	return outTradeNo;
}

public void setOutTradeNo(String outTradeNo) {
	this.outTradeNo = outTradeNo;
}

public String getProductType() {
	return productType;
}

public void setProductType(String productType) {
	this.productType = productType;
}

public String getOrderTime() {
	return orderTime;
}

public void setOrderTime(String orderTime) {
	this.orderTime = orderTime;
}

public String getProductName() {
	return productName;
}

public void setProductName(String productName) {
	this.productName = productName;
}

public String getOrderIp() {
	return orderIp;
}

public void setOrderIp(String orderIp) {
	this.orderIp = orderIp;
}

public String getReturnUrl() {
	return returnUrl;
}

public void setReturnUrl(String returnUrl) {
	this.returnUrl = returnUrl;
}

public String getNotifyUrl() {
	return notifyUrl;
}

public void setNotifyUrl(String notifyUrl) {
	this.notifyUrl = notifyUrl;
}

public String getSubPayKey() {
	return subPayKey;
}

public void setSubPayKey(String subPayKey) {
	this.subPayKey = subPayKey;
}

public String getRemark() {
	return remark;
}

public void setRemark(String remark) {
	this.remark = remark;
}

public String getSign() {
	return sign;
}

public void setSign(String sign) {
	this.sign = sign;
}

public String getBankCode() {
	return bankCode;
}

public void setBankCode(String bankCode) {
	this.bankCode = bankCode;
}

public String getBankAccountType() {
	return bankAccountType;
}

public void setBankAccountType(String bankAccountType) {
	this.bankAccountType = bankAccountType;
}

public String getMobile() {
	return mobile;
}

public void setMobile(String mobile) {
	this.mobile = mobile;
}



}
