package com.team.lottery.pay.model;
/**
 * 嘟嘟第三方支付对象
 * @author Jamine
 *
 */
public class DuDuPay {
	//嘟嘟支付的商户号.
	private Integer merchant_id;
	//商户订单号.
	private String order_id;
	//支付金额0.01~5000的随机数.
	private String amount;
	//服务器异步通知地址.
	private String notify_url;
	//用户支付结果地址.
	private String return_url;
	//支付方式编号.
	private Integer pay_method;
	//商户传递的加密参数.
	private String sign;
	public Integer getMerchant_id() {
		return merchant_id;
	}
	public void setMerchant_id(Integer merchant_id) {
		this.merchant_id = merchant_id;
	}
	public String getOrder_id() {
		return order_id;
	}
	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getNotify_url() {
		return notify_url;
	}
	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}
	public String getReturn_url() {
		return return_url;
	}
	public void setReturn_url(String return_url) {
		this.return_url = return_url;
	}
	public Integer getPay_method() {
		return pay_method;
	}
	public void setPay_method(Integer pay_method) {
		this.pay_method = pay_method;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	@Override
	public String toString() {
		return "商户号: " + merchant_id + 
			   "商户订单: " + order_id + 
			   "支付金额: " + amount +
			   "服务器异步通知地址: " + notify_url +
			   "支付结果返回地址: " + return_url + 
			   "支付方式: " + pay_method+
			   "交易签名: " + sign ;
	}
}
