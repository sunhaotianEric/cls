package com.team.lottery.pay.model;
public class JianZhengBaoPay {
	//商户ID
	public String merchant_order_no;
	//银行类型 
	public String merchant_no;
	//交易金额
	public String callback_url;
	//商户流水号
	public String order_smt_time;
	//异步通知地址
	public String order_type;
	//请求时间
	public String trade_amount;
	//同步通知地址
	public String goods_name;
	//获取二维码地址
	public String goods_type;
	//备注消息 
	public String goods_desc;
	//备注消息 
	public String trade_desc;
	//备注消息 
	public String wx_pay_result_url;
	//备注消息 
	public String biz_type;
	//备注消息 
	public String sign;
	//备注消息 
	public String sign_type;

	

	public String getMerchant_order_no() {
		return merchant_order_no;
	}



	public void setMerchant_order_no(String merchant_order_no) {
		this.merchant_order_no = merchant_order_no;
	}



	public String getMerchant_no() {
		return merchant_no;
	}



	public void setMerchant_no(String merchant_no) {
		this.merchant_no = merchant_no;
	}



	public String getCallback_url() {
		return callback_url;
	}



	public void setCallback_url(String callback_url) {
		this.callback_url = callback_url;
	}



	public String getOrder_smt_time() {
		return order_smt_time;
	}



	public void setOrder_smt_time(String order_smt_time) {
		this.order_smt_time = order_smt_time;
	}



	public String getOrder_type() {
		return order_type;
	}



	public void setOrder_type(String order_type) {
		this.order_type = order_type;
	}



	public String getTrade_amount() {
		return trade_amount;
	}



	public void setTrade_amount(String trade_amount) {
		this.trade_amount = trade_amount;
	}



	public String getGoods_name() {
		return goods_name;
	}



	public void setGoods_name(String goods_name) {
		this.goods_name = goods_name;
	}



	public String getGoods_type() {
		return goods_type;
	}



	public void setGoods_type(String goods_type) {
		this.goods_type = goods_type;
	}



	public String getGoods_desc() {
		return goods_desc;
	}



	public void setGoods_desc(String goods_desc) {
		this.goods_desc = goods_desc;
	}



	public String getTrade_desc() {
		return trade_desc;
	}



	public void setTrade_desc(String trade_desc) {
		this.trade_desc = trade_desc;
	}



	public String getWx_pay_result_url() {
		return wx_pay_result_url;
	}



	public void setWx_pay_result_ur(String wx_pay_result_url) {
		this.wx_pay_result_url = wx_pay_result_url;
	}



	public String getBiz_type() {
		return biz_type;
	}



	public void setBiz_type(String biz_type) {
		this.biz_type = biz_type;
	}



	public String getSign() {
		return sign;
	}



	public void setSign(String sign) {
		this.sign = sign;
	}



	public String getSign_type() {
		return sign_type;
	}



	public void setSign_type(String sign_type) {
		this.sign_type = sign_type;
	}



	public String toString(){
		return " 商户订单号:"+merchant_order_no + " 商户号:" + merchant_no+
			   " 商户通知地址:"+callback_url + " 商户提交订单时间:" + order_smt_time+
			   " 订单类型:" + order_type+
			   " 订单金额:"+trade_amount +" 商品名称:" + goods_name +
			   " 商品类型:" + goods_type+
			   " 商品描述:"+goods_desc +" 交易描述:" + trade_desc +
			   " 微信支付结果页面地址:" + wx_pay_result_url+" 支付参数:"+biz_type +
			   " 数据签名:" + sign +" 加密类型:"+sign_type;
	}
	
	
}
