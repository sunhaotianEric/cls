package com.team.lottery.pay.model.pujin;
public class PuJinPay {
	//商户号
	public String appId;
	//交易金额
	public String money;
	//支付方式
	public String payType;
	//订单号
	public String orderNumber;
	//异步地址
	public String notifyUrl;
	//同步地址
	public String returnUrl;
	//数据签名
	public String signature;
   
	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getNotifyUrl() {
		return notifyUrl;
	}

	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	public String getReturnUrl() {
		return returnUrl;
	}

	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String toString(){
		return " 商户号:"+appId + " 交易金额:" + money+
			   " 支付方式:"+payType + " 订单号:" + orderNumber+
			   " 异步地址:"+notifyUrl + " 同步地址:" + returnUrl+
			   " 签名:" + signature;
	}
	
	
}
