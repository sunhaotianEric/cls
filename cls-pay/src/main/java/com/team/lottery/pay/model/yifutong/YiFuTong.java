package com.team.lottery.pay.model.yifutong;

/**
 * 第三方支付易付通支付(必填字段.)!
 * 
 * @author Jamine
 *
 */
public class YiFuTong {
	// 商户号.
	private String pay_memberid;
	// 上送唯一订单号.
	private String pay_orderid;
	// 交易时间(格式2016-12-26 18:18:18).
	private String pay_applydate;
	// 银行编码.
	private String pay_bankcode;
	// 异步通知地址,回调地址.
	private String pay_notifyurl;
	// 支付成功跳转地址.
	private String pay_callbackurl;
	// 支付金额.
	private String pay_amount;
	// 签名.
	private String pay_md5sign;
	// 商品名称.
	private String pay_productname;

	
	public String getPay_memberid() {
		return pay_memberid;
	}

	public void setPay_memberid(String pay_memberid) {
		this.pay_memberid = pay_memberid;
	}

	public String getPay_orderid() {
		return pay_orderid;
	}

	public void setPay_orderid(String pay_orderid) {
		this.pay_orderid = pay_orderid;
	}

	public String getPay_applydate() {
		return pay_applydate;
	}

	public void setPay_applydate(String pay_applydate) {
		this.pay_applydate = pay_applydate;
	}

	public String getPay_bankcode() {
		return pay_bankcode;
	}

	public void setPay_bankcode(String pay_bankcode) {
		this.pay_bankcode = pay_bankcode;
	}

	public String getPay_notifyurl() {
		return pay_notifyurl;
	}

	public void setPay_notifyurl(String pay_notifyurl) {
		this.pay_notifyurl = pay_notifyurl;
	}

	public String getPay_callbackurl() {
		return pay_callbackurl;
	}

	public void setPay_callbackurl(String pay_callbackurl) {
		this.pay_callbackurl = pay_callbackurl;
	}

	public String getPay_amount() {
		return pay_amount;
	}

	public void setPay_amount(String pay_amount) {
		this.pay_amount = pay_amount;
	}

	public String getPay_md5sign() {
		return pay_md5sign;
	}

	public void setPay_md5sign(String pay_md5sign) {
		this.pay_md5sign = pay_md5sign;
	}

	public String getPay_productname() {
		return pay_productname;
	}

	public void setPay_productname(String pay_productname) {
		this.pay_productname = pay_productname;
	}

	@Override
	public String toString() {
		return "YiFuTong [易付通商户号=" + pay_memberid + ", 订单号=" + pay_orderid + ", 支付时间=" + pay_applydate + ", 支付银行编码=" + pay_bankcode
				+ ", 异步通知地址=" + pay_notifyurl + ", 同步地址=" + pay_callbackurl + ", 支付金额=" + pay_amount + ", 签名=" + pay_md5sign
				+ ", 产品名称=" + pay_productname + "]";
	}
	
}
