package com.team.lottery.pay.model;
public class JuRongPay {
	
public String mch_no;

public String pay_type;

public String amount;

public String time;

public String order_sn;

public String return_url;

public String notify_url;

public String card_type;

public String yl_pay_type;

public String bank_name;

public String sign;

public String extra;

public String client_ip;

public String getMch_no() {
	return mch_no;
}

public void setMch_no(String mch_no) {
	this.mch_no = mch_no;
}

public String getPay_type() {
	return pay_type;
}

public void setPay_type(String pay_type) {
	this.pay_type = pay_type;
}

public String getAmount() {
	return amount;
}

public void setAmount(String amount) {
	this.amount = amount;
}

public String getTime() {
	return time;
}

public void setTime(String time) {
	this.time = time;
}

public String getOrder_sn() {
	return order_sn;
}

public void setOrder_sn(String order_sn) {
	this.order_sn = order_sn;
}

public String getReturn_url() {
	return return_url;
}

public void setReturn_url(String return_url) {
	this.return_url = return_url;
}

public String getNotify_url() {
	return notify_url;
}

public void setNotify_url(String notify_url) {
	this.notify_url = notify_url;
}

public String getCard_type() {
	return card_type;
}

public void setCard_type(String card_type) {
	this.card_type = card_type;
}

public String getYl_pay_type() {
	return yl_pay_type;
}

public void setYl_pay_type(String yl_pay_type) {
	this.yl_pay_type = yl_pay_type;
}

public String getBank_name() {
	return bank_name;
}

public void setBank_name(String bank_name) {
	this.bank_name = bank_name;
}

public String getSign() {
	return sign;
}

public void setSign(String sign) {
	this.sign = sign;
}

public String getExtra() {
	return extra;
}

public void setExtra(String extra) {
	this.extra = extra;
}

public String getClient_ip() {
	return client_ip;
}

public void setClient_ip(String client_ip) {
	this.client_ip = client_ip;
}



}
