package com.team.lottery.pay.model;

import java.util.Date;

import com.team.lottery.util.DateUtil;

/**
 * 雀付支付对象
 * @author Jamine
 *
 */
public class QuePay {
	//商户编号
	private String pay_memberid;
	//订单编号
	private String pay_orderid;
	//支付金额
	private String pay_amount;
	//支付时间
	private Date pay_applydate;
	//通道编号
	private String pay_bankcode;
	//服务端返回地址
	private String pay_notifyurl;
	//页面返回地址
	private String pay_callbackurl;
	//用户Id
	private Integer pay_userid;
	//商品名称
	private String pay_productname;
	//MD5签名字段
	private String pay_md5sign;
	
	public String getPay_productname() {
		return pay_productname;
	}
	public void setPay_productname(String pay_productname) {
		this.pay_productname = pay_productname;
	}
	public String getPay_memberid() {
		return pay_memberid;
	}
	public void setPay_memberid(String pay_memberid) {
		this.pay_memberid = pay_memberid;
	}
	public String getPay_orderid() {
		return pay_orderid;
	}
	public void setPay_orderid(String pay_orderid) {
		this.pay_orderid = pay_orderid;
	}
	
	public String getPay_amount() {
		return pay_amount;
	}
	public void setPay_amount(String pay_amount) {
		this.pay_amount = pay_amount;
	}
	public Date getPay_applydate() {
		return pay_applydate;
	}
	public void setPay_applydate(Date pay_applydate) {
		this.pay_applydate = pay_applydate;
	}
	public String getPay_bankcode() {
		return pay_bankcode;
	}
	public void setPay_bankcode(String pay_bankcode) {
		this.pay_bankcode = pay_bankcode;
	}
	public String getPay_notifyurl() {
		return pay_notifyurl;
	}
	public void setPay_notifyurl(String pay_notifyurl) {
		this.pay_notifyurl = pay_notifyurl;
	}
	public String getPay_callbackurl() {
		return pay_callbackurl;
	}
	public void setPay_callbackurl(String pay_callbackurl) {
		this.pay_callbackurl = pay_callbackurl;
	}
	public Integer getPay_userid() {
		return pay_userid;
	}
	public void setPay_userid(Integer pay_userid) {
		this.pay_userid = pay_userid;
	}
	public String getPay_md5sign() {
		return pay_md5sign;
	}
	public void setPay_md5sign(String pay_md5sign) {
		this.pay_md5sign = pay_md5sign;
	}
	 public String getPay_applydateDateStr() {
	    	String dateStr = "";
	    	if(this.pay_applydate != null) {
	    		dateStr = DateUtil.parseDate(pay_applydate, "yyyy-MM-dd HH:mm:ss");
	    	}
	    	return dateStr;
	    }

	
}
