package com.team.lottery.pay.model;

public class QuanYinRetrunPay {

	public String payKey;

	public String productName;

	public String orderNo;

	public String orderPrice;

	public String payWayCode;

	public String payPayCode;

	public String orderDate;

	public String orderTime;

	public String remark;

	public String trxNo;

	public String tradeStatus;

	public String sign;

	public String getPayKey() {
		return payKey;
	}

	public void setPayKey(String payKey) {
		this.payKey = payKey;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getOrderPrice() {
		return orderPrice;
	}

	public void setOrderPrice(String orderPrice) {
		this.orderPrice = orderPrice;
	}

	public String getPayWayCode() {
		return payWayCode;
	}

	public void setPayWayCode(String payWayCode) {
		this.payWayCode = payWayCode;
	}

	public String getPayPayCode() {
		return payPayCode;
	}

	public void setPayPayCode(String payPayCode) {
		this.payPayCode = payPayCode;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public String getOrderTime() {
		return orderTime;
	}

	public void setOrderTime(String orderTime) {
		this.orderTime = orderTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getTrxNo() {
		return trxNo;
	}

	public void setTrxNo(String trxNo) {
		this.trxNo = trxNo;
	}

	public String getTradeStatus() {
		return tradeStatus;
	}

	public void setTradeStatus(String tradeStatus) {
		this.tradeStatus = tradeStatus;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String toString() {
		return " payKey:" + payKey + " productName:" + productName + " orderNo:" + orderNo + " orderPrice:" + orderPrice + " payWayCode:"
				+ payWayCode + " payPayCode:" + payPayCode  + " orderDate:" + orderDate + " orderTime:"
				+ orderTime + " remark:" + remark + " tradeStatus:" + tradeStatus + " trxNo:" + trxNo  
				+ " sign:" + sign;
	}

}
