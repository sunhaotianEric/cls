package com.team.lottery.pay.model;
public class YinBaoPay {
	//商户ID
	public String partner;
	//银行类型
	public String banktype;
	//金额
	public String paymoney;
	//商户订单号
	public String ordernumber;
	//下行异步通知地址
	public String callbackurl;
	//下行同步通知地址
	public String hrefbackurl;
	//备注信息
	public String attach;
	//MD5签名
	public String sign;
	
	
	
	public String getPartner() {
		return partner;
	}



	public void setPartner(String partner) {
		this.partner = partner;
	}



	public String getBanktype() {
		return banktype;
	}



	public void setBanktype(String banktype) {
		this.banktype = banktype;
	}



	public String getPaymoney() {
		return paymoney;
	}



	public void setPaymoney(String paymoney) {
		this.paymoney = paymoney;
	}



	public String getOrdernumber() {
		return ordernumber;
	}



	public void setOrdernumber(String ordernumber) {
		this.ordernumber = ordernumber;
	}



	public String getCallbackurl() {
		return callbackurl;
	}



	public void setCallbackurl(String callbackurl) {
		this.callbackurl = callbackurl;
	}



	public String getHrefbackurl() {
		return hrefbackurl;
	}



	public void setHrefbackurl(String hrefbackurl) {
		this.hrefbackurl = hrefbackurl;
	}



	public String getAttach() {
		return attach;
	}



	public void setAttach(String attach) {
		this.attach = attach;
	}



	public String getSign() {
		return sign;
	}



	public void setSign(String sign) {
		this.sign = sign;
	}



	public String toString(){
		return " 商户 ID:"+partner + " 银行类型:" + banktype+
			   " 金额:"+paymoney + " 商户订单号:" + ordernumber+
			   " 下行异步通知地址:"+callbackurl + " 下行同步通知地址:" + hrefbackurl+
		       " 备注信息:"+attach + " MD5签名:" + sign;
	}
	
	
}
