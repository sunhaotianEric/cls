package com.team.lottery.pay.model.gt;

/**
 * GT支付支付发起对象.
 * 
 * @author jamine
 *
 */
public class Gtpay {
	// 支付金额.
	private String amount;
	// 回掉地址.
	private String backUrl;
	// 商户号.
	private String mcnNum;
	// 商户订单号.
	private String orderId;
	// 支付方式(码表,现在仅支持2和12这两种方式)
	private String payType;
	// 发起请求IP地址.
	private String ip;
	// 签名.
	private String sign;
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getBackUrl() {
		return backUrl;
	}
	public void setBackUrl(String backUrl) {
		this.backUrl = backUrl;
	}
	public String getMcnNum() {
		return mcnNum;
	}
	public void setMcnNum(String mcnNum) {
		this.mcnNum = mcnNum;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	
	@Override
	public String toString() {
		return "GT支付发起对象 [支付金额=" + amount + "分, 回调地址=" + backUrl + ", 商户号=" + mcnNum + ", 订单号=" + orderId
				+ ", 支付码表=" + payType + ", IP地址=" + ip + ", 签名=" + sign + "]";
	}
	
	
}
