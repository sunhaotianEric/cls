package com.team.lottery.pay.model;

public class FeiLongPay {
	// 商户ID
	public String companyId;
	// 支付方式
	public String payType;
	// 订单号
	public String userOrderId;
	// 商品名
	public String item;
	// 价格
	public String fee;
	// 超时时间
	public String expire;
	// 前端回调地址
	public String callbackUrl;
	// 异步通知地址
	public String syncUrl;
	// 签名
	public String sign;
	// IP
	public String ip;
	// 手机号
	public String mobile;
	// 持卡人姓名
	public String name;
	// 持卡人身份证号码
	public String idCardNo;

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getUserOrderId() {
		return userOrderId;
	}

	public void setUserOrderId(String userOrderId) {
		this.userOrderId = userOrderId;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public String getFee() {
		return fee;
	}

	public void setFee(String fee) {
		this.fee = fee;
	}

	public String getExpire() {
		return expire;
	}

	public void setExpire(String expire) {
		this.expire = expire;
	}

	public String getCallbackUrl() {
		return callbackUrl;
	}

	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}

	public String getSyncUrl() {
		return syncUrl;
	}

	public void setSyncUrl(String syncUrl) {
		this.syncUrl = syncUrl;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdCardNo() {
		return idCardNo;
	}

	public void setIdCardNo(String idCardNo) {
		this.idCardNo = idCardNo;
	}


	public String toString(){
		return " 商户ID:"+companyId + " 订单号:" + userOrderId+
			   " 支付方式:"+payType + " 商品名:" + item+
			   " 价格:"+fee + " 超时时间:" + expire+
			   " 前端回调地址:"+callbackUrl + " 异步通知地址:" + syncUrl+
			   " 签名:"+sign + " IP:" + ip+
			   " 手机号:"+mobile + " 持卡人姓名:" + name+" 持卡人身份证号码:" + idCardNo;
	}
	
	
}
