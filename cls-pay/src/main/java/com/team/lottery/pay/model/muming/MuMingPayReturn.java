package com.team.lottery.pay.model.muming;

/**
 * 慕名支付,回调参数封装.
 * 
 * @author Jamine
 *
 */
public class MuMingPayReturn {

	
	// 商户编号
	public String memberid;
	// 订单号
	public String orderid;
	// 订单金额
	public String amount;
	// 交易流水号
	public String transaction_id;
	// 交易时间
	public String datetime;
	// 交易状态
	public String returncode;
	// 扩展返回
	public String attach;
	// 签名
	public String sign;
	
	public String getMemberid() {
		return memberid;
	}



	public void setMemberid(String memberid) {
		this.memberid = memberid;
	}



	public String getOrderid() {
		return orderid;
	}



	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}



	public String getAmount() {
		return amount;
	}



	public void setAmount(String amount) {
		this.amount = amount;
	}



	public String getTransaction_id() {
		return transaction_id;
	}



	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}



	public String getDatetime() {
		return datetime;
	}



	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}



	public String getReturncode() {
		return returncode;
	}



	public void setReturncode(String returncode) {
		this.returncode = returncode;
	}



	public String getAttach() {
		return attach;
	}



	public void setAttach(String attach) {
		this.attach = attach;
	}



	public String getSign() {
		return sign;
	}



	public void setSign(String sign) {
		this.sign = sign;
	}



	@Override
	public String toString() {
		return "慕名支付成功返回对象 [状态=" + returncode + ", 商户订单号=" + orderid + ", 支付时间=" + datetime + ", 支付金额=" + amount
				+ ", 商户号=" + memberid + ", 签名=" + sign + "]";
	}
	

}
