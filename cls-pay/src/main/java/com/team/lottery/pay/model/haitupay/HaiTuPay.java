
package com.team.lottery.pay.model.haitupay;

/** 
 * @Description: 海图支付,发起对象.
 * @Author: Jamine.
 * @CreateDate：2019-05-25 03:12:03.
 */
public class HaiTuPay {
	// 接口名称
	private String service;
	// 商户号.
	private String mch_id;
	// 随机字符串.
	private String nonce_str;
	// 商户上送唯一订单号.
	private String out_trade_no;
	// 商品描述.
	private String body;
	// 总金额(分)
	private String total_fee;
	// 请求IP地址.
	private String spbill_create_ip;
	// 回调地址.
	private String notify_url;
	// 签名.
	private String sign;
	
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public String getMch_id() {
		return mch_id;
	}
	public void setMch_id(String mch_id) {
		this.mch_id = mch_id;
	}
	public String getNonce_str() {
		return nonce_str;
	}
	public void setNonce_str(String nonce_str) {
		this.nonce_str = nonce_str;
	}
	public String getOut_trade_no() {
		return out_trade_no;
	}
	public void setOut_trade_no(String out_trade_no) {
		this.out_trade_no = out_trade_no;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getTotal_fee() {
		return total_fee;
	}
	public void setTotal_fee(String total_fee) {
		this.total_fee = total_fee;
	}
	public String getSpbill_create_ip() {
		return spbill_create_ip;
	}
	public void setSpbill_create_ip(String spbill_create_ip) {
		this.spbill_create_ip = spbill_create_ip;
	}
	public String getNotify_url() {
		return notify_url;
	}
	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}
	@Override
	public String toString() {
		return "海图支付发起对象 [接口名称=" + service + ", 商户号=" + mch_id + ", 随机字符串=" + nonce_str + ", 商户订单号=" + out_trade_no
				+ ", 商品描述=" + body + ", 支付金额=" + total_fee + "分, IP发起地址=" + spbill_create_ip + ", 回调地址=" + notify_url
				+ ", 签名=" + sign + "]";
	}
	
	
}
