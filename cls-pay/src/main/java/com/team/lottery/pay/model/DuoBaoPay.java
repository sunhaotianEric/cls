package com.team.lottery.pay.model;
public class DuoBaoPay {
	//商户ID
	public String parter;
	//银行类型
	public String type;
	//金额
	public String value;
	//商户订单号
	public String orderid;
	//下行异步通知地址
	public String callbackurl;
	//下行同步通知地址
	public String hrefbackurl;
	//备注信息
	public String attach;
	//MD5签名
	public String sign;
	//获取二维码地址
	public String onlyqr;
	
	
    
	
	public String getParter() {
		return parter;
	}



	public void setParter(String parter) {
		this.parter = parter;
	}



	public String getType() {
		return type;
	}



	public void setType(String type) {
		this.type = type;
	}



	public String getValue() {
		return value;
	}



	public void setValue(String value) {
		this.value = value;
	}



	public String getOrderid() {
		return orderid;
	}



	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}



	public String getOnlyqr() {
		return onlyqr;
	}



	public void setOnlyqr(String onlyqr) {
		this.onlyqr = onlyqr;
	}



	public String getCallbackurl() {
		return callbackurl;
	}



	public void setCallbackurl(String callbackurl) {
		this.callbackurl = callbackurl;
	}



	public String getHrefbackurl() {
		return hrefbackurl;
	}



	public void setHrefbackurl(String hrefbackurl) {
		this.hrefbackurl = hrefbackurl;
	}



	public String getAttach() {
		return attach;
	}



	public void setAttach(String attach) {
		this.attach = attach;
	}



	public String getSign() {
		return sign;
	}



	public void setSign(String sign) {
		this.sign = sign;
	}



	public String toString(){
		return " 商户 ID:"+parter+ " 银行类型:" +type+
			   " 金额:"+value +" 商户订单号:" + orderid+" 获取二维码地址:"+onlyqr+
			   " 下行异步通知地址:"+callbackurl + " 下行同步通知地址:" + hrefbackurl+
		       " 备注信息:"+attach + " MD5签名:" + sign;
	}
	
	
}
