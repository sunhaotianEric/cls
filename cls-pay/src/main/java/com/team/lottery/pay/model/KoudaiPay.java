package com.team.lottery.pay.model;
public class KoudaiPay {
	//商户 ID
	public String P_UserId;
	//商户订单号
	public String P_OrderId;
	//卡类交易时的卡号
	public String P_CardId;
	//卡类交易时的卡密
	public String P_CardPass;
	//面值/金额（支付金额）
	public String P_FaceValue;
	//充值类型 ID
	public String P_ChannelId;
	//产品名称
	public String P_Subject;
	//产品价格
	public String P_Price;
	//产品数量
	public String P_Quantity;
	//银行 ID
	public String P_Description;
	//用户附加信息
	public String P_Notic;
	//异步通知地址
	public String P_Result_URL;
	//同步跳转地址
	public String P_Notify_URL;
	//签名认证串
	public String P_PostKey;
	//WAP 版快捷支付判断
	public String P_IsSmart;
	
	
	public String getP_UserId() {
		return P_UserId;
	}


	public void setP_UserId(String p_UserId) {
		P_UserId = p_UserId;
	}


	public String getP_OrderId() {
		return P_OrderId;
	}


	public void setP_OrderId(String p_OrderId) {
		P_OrderId = p_OrderId;
	}


	public String getP_CardId() {
		return P_CardId;
	}


	public void setP_CardId(String p_CardId) {
		P_CardId = p_CardId;
	}


	public String getP_CardPass() {
		return P_CardPass;
	}


	public void setP_CardPass(String p_CardPass) {
		P_CardPass = p_CardPass;
	}


	public String getP_FaceValue() {
		return P_FaceValue;
	}


	public void setP_FaceValue(String p_FaceValue) {
		P_FaceValue = p_FaceValue;
	}


	public String getP_ChannelId() {
		return P_ChannelId;
	}


	public void setP_ChannelId(String p_ChannelId) {
		P_ChannelId = p_ChannelId;
	}


	public String getP_Subject() {
		return P_Subject;
	}


	public void setP_Subject(String p_Subject) {
		P_Subject = p_Subject;
	}


	public String getP_Price() {
		return P_Price;
	}


	public void setP_Price(String p_Price) {
		P_Price = p_Price;
	}


	public String getP_Quantity() {
		return P_Quantity;
	}


	public void setP_Quantity(String p_Quantity) {
		P_Quantity = p_Quantity;
	}


	public String getP_Description() {
		return P_Description;
	}


	public void setP_Description(String p_Description) {
		P_Description = p_Description;
	}


	public String getP_Notic() {
		return P_Notic;
	}


	public void setP_Notic(String p_Notic) {
		P_Notic = p_Notic;
	}


	public String getP_Result_URL() {
		return P_Result_URL;
	}


	public void setP_Result_URL(String p_Result_URL) {
		P_Result_URL = p_Result_URL;
	}


	public String getP_Notify_URL() {
		return P_Notify_URL;
	}


	public void setP_Notify_URL(String p_Notify_URL) {
		P_Notify_URL = p_Notify_URL;
	}


	public String getP_PostKey() {
		return P_PostKey;
	}


	public void setP_PostKey(String p_PostKey) {
		P_PostKey = p_PostKey;
	}


	public String getP_IsSmart() {
		return P_IsSmart;
	}


	public void setP_IsSmart(String p_IsSmart) {
		P_IsSmart = p_IsSmart;
	}


	public String toString(){
		return " 商户 ID:"+P_UserId + " 商户订单号:" + P_OrderId+
			   " 卡类交易时的卡号:"+P_CardId + " 卡类交易时的卡密:" + P_CardPass+
			   " 面值/金额（支付金额）:"+P_FaceValue + " 充值类型 ID:" + P_ChannelId+
		       " 产品名称:"+P_Subject + " 产品价格:" + P_Price+
		       " 产品数量:"+P_Quantity + " 银行 ID:" + P_Description+
		       " 用户附加信息:"+P_Notic + " 异步通知地址:" + P_Result_URL+
		       " 同步跳转地址:"+P_Notify_URL + " 签名认证串:" + P_PostKey+
		       " WAP 版快捷支付判断:"+P_IsSmart;
	}
	
	
}
