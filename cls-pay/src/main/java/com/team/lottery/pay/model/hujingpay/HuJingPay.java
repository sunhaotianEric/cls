package com.team.lottery.pay.model.hujingpay;

/**
 * 虎鲸发起支付对象.
 * 
 * @author Jamine
 *
 */
public class HuJingPay {
	// 商户号.
	private String merchantId;
	// 异步回调地址.
	private String notifyUrl;
	// 签名.
	private String sign;
	// 同步跳转地址.
	private String returnUrl;
	// 支付类型(码表).
	private String payType;
	// 用户IP地址.
	private String clientIp;
	// 返回数据格式.
	private String resultFormat;
	// 商户订单号.
	private String merchantOrderId;
	// 商户系统中的唯一用户ID.
	private String merchantUid;
	// 商家订单时间戳.
	private String timestamp;
	// 订单金额.
	private String money;
	// 商品名称.
	private String goodsName;
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getNotifyUrl() {
		return notifyUrl;
	}
	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public String getReturnUrl() {
		return returnUrl;
	}
	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}
	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}
	public String getClientIp() {
		return clientIp;
	}
	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}
	public String getResultFormat() {
		return resultFormat;
	}
	public void setResultFormat(String resultFormat) {
		this.resultFormat = resultFormat;
	}
	public String getMerchantOrderId() {
		return merchantOrderId;
	}
	public void setMerchantOrderId(String merchantOrderId) {
		this.merchantOrderId = merchantOrderId;
	}
	public String getMerchantUid() {
		return merchantUid;
	}
	public void setMerchantUid(String merchantUid) {
		this.merchantUid = merchantUid;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getMoney() {
		return money;
	}
	public void setMoney(String money) {
		this.money = money;
	}
	public String getGoodsName() {
		return goodsName;
	}
	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}
	@Override
	public String toString() {
		return "虎鲸发起支付对象 [商家号=" + merchantId + ", 回调地址=" + notifyUrl + ", 签名=" + sign + ", 同步跳转地址=" + returnUrl + ", 支付方式=" + payType + ", 用户IP="
				+ clientIp + ", 返回方式=" + resultFormat + ", 订单号=" + merchantOrderId + ", 商户中用户ID=" + merchantUid + ", 时间戳=" + timestamp + ", 金额="
				+ money + ", 商品名称=" + goodsName + "]";
	}
	
	
	
}
