package com.team.lottery.pay.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "xml")
@XmlAccessorType(XmlAccessType.FIELD)
public class XinYuPay {
	//版本号
	@XmlElement(name="version")
	public String version;
	//字符集
	@XmlElement(name="charset")
	public String charset;
	//签名方式
	@XmlElement(name="sign_type")
	public String sign_type;
	//商户号
	@XmlElement(name="mch_id")
	public String mch_id;
	//商户订单号
	@XmlElement(name="out_trade_no")
	public String out_trade_no;
	//设备号
	@XmlElement(name="device_info")
	public String device_info;
	//商品描述
	@XmlElement(name="body")
	public String body;
	//附加信息
	@XmlElement(name="attach")
	public String attach;
	//总金额
	@XmlElement(name="total_fee")
	public String total_fee;
	//终端IP
	@XmlElement(name="mch_create_ip")
	public String mch_create_ip;
	//通知地址
	@XmlElement(name="notify_url")
	public String notify_url;
	/*//订单生成时间
	@XmlElement(name="time_start")
	public String time_start;
	//订单超时时间
	@XmlElement(name="time_expire")
	public String time_expire;
	//操作员
	@XmlElement(name="op_user_id")
	public String op_user_id;
	//商品标记
	@XmlElement(name="goods_tag")
	public String goods_tag;
	//商品ID
	@XmlElement(name="product_id")
    public String product_id;*/
    //随机字符串
	@XmlElement(name="nonce_str")
  	public String nonce_str;
    /*//是否限制信用卡
	@XmlElement(name="limit_credit_pay")
  	public String limit_credit_pay;*/
    //签名
	@XmlElement(name="sign")
  	public String sign;
  
  	
	public String getVersion() {
		return version;
	}


	public void setVersion(String version) {
		this.version = version;
	}


	public String getCharset() {
		return charset;
	}


	public void setCharset(String charset) {
		this.charset = charset;
	}


	public String getSign_type() {
		return sign_type;
	}


	public void setSign_type(String sign_type) {
		this.sign_type = sign_type;
	}


	public String getMch_id() {
		return mch_id;
	}


	public void setMch_id(String mch_id) {
		this.mch_id = mch_id;
	}


	public String getOut_trade_no() {
		return out_trade_no;
	}


	public void setOut_trade_no(String out_trade_no) {
		this.out_trade_no = out_trade_no;
	}


	public String getDevice_info() {
		return device_info;
	}


	public void setDevice_info(String device_info) {
		this.device_info = device_info;
	}


	public String getBody() {
		return body;
	}


	public void setBody(String body) {
		this.body = body;
	}


	public String getAttach() {
		return attach;
	}


	public void setAttach(String attach) {
		this.attach = attach;
	}


	public String getTotal_fee() {
		return total_fee;
	}


	public void setTotal_fee(String total_fee) {
		this.total_fee = total_fee;
	}


	public String getMch_create_ip() {
		return mch_create_ip;
	}


	public void setMch_create_ip(String mch_create_ip) {
		this.mch_create_ip = mch_create_ip;
	}


	public String getNotify_url() {
		return notify_url;
	}


	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}


	/*public String getTime_start() {
		return time_start;
	}


	public void setTime_start(String time_start) {
		this.time_start = time_start;
	}


	public String getTime_expire() {
		return time_expire;
	}


	public void setTime_expire(String time_expire) {
		this.time_expire = time_expire;
	}


	public String getOp_user_id() {
		return op_user_id;
	}


	public void setOp_user_id(String op_user_id) {
		this.op_user_id = op_user_id;
	}


	public String getGoods_tag() {
		return goods_tag;
	}


	public void setGoods_tag(String goods_tag) {
		this.goods_tag = goods_tag;
	}


	public String getProduct_id() {
		return product_id;
	}


	public void setProduct_id(String product_id) {
		this.product_id = product_id;
	}*/


	public String getNonce_str() {
		return nonce_str;
	}


	public void setNonce_str(String nonce_str) {
		this.nonce_str = nonce_str;
	}


	/*public String getLimit_credit_pay() {
		return limit_credit_pay;
	}


	public void setLimit_credit_pay(String limit_credit_pay) {
		this.limit_credit_pay = limit_credit_pay;
	}*/


	public String getSign() {
		return sign;
	}


	public void setSign(String sign) {
		this.sign = sign;
	}


	/*public String toString(){
		return " 版本号:"+version + " 字符集:" + charset+
			   " 签名方式:"+sign_type + " 商户号:" + mch_id+
			   " 商户订单号:"+out_trade_no + " 设备号:" + device_info+
			   " 商品描述:"+body + " 附加信息:" + attach+
			   " 总金额:"+total_fee + " 终端IP:" + mch_create_ip+
			   " 通知地址:"+notify_url + " 订单生成时间:" + time_start+" 订单超时时间:" + time_expire+
			   " 操作员:"+op_user_id + " 商品标记:" + goods_tag+" 商品ID:" + product_id+
			   " 随机字符串:"+nonce_str + " 是否限制信用卡:" + limit_credit_pay+" 签名:" + sign;
	}*/
	
	
}
