
package com.team.lottery.pay.model.guangsupay;

/** 
 * @Description: 光速支付
 * @Author: Jamine.
 * @CreateDate：2019-10-07 01:03:06.
 */
public class GuangSuPay {
	// 商户号.
	private String mchno;
	// 当前系统秒级时间戳.
	private String timestamp;
	// 会员账号.
	private String username;
	// 商户系统订单号.
	private String order_no;
	// 订单类型(0-查看订单；1-买入；2-卖出)
	private String order_type;
	// 币类型
	private String vcoin_code;
	// 法币类型
	private String currency_type;
	// 支付方式.
	private String pay_methods;
	// 签名
	private String signature;
	// 法币金额
	private String currency_money;
	// 是否测试.
	private String fortest;

	public String getMchno() {
		return mchno;
	}

	public void setMchno(String mchno) {
		this.mchno = mchno;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getOrder_no() {
		return order_no;
	}

	public void setOrder_no(String order_no) {
		this.order_no = order_no;
	}

	public String getOrder_type() {
		return order_type;
	}

	public void setOrder_type(String order_type) {
		this.order_type = order_type;
	}

	public String getVcoin_code() {
		return vcoin_code;
	}

	public void setVcoin_code(String vcoin_code) {
		this.vcoin_code = vcoin_code;
	}

	public String getCurrency_type() {
		return currency_type;
	}

	public void setCurrency_type(String currency_type) {
		this.currency_type = currency_type;
	}

	public String getPay_methods() {
		return pay_methods;
	}

	public void setPay_methods(String pay_methods) {
		this.pay_methods = pay_methods;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getCurrency_money() {
		return currency_money;
	}

	public void setCurrency_money(String currency_money) {		
		this.currency_money = currency_money;
	}

	public String getFortest() {
		return fortest;
	}

	public void setFortest(String fortest) {
		this.fortest = fortest;
	}
	
	
}
