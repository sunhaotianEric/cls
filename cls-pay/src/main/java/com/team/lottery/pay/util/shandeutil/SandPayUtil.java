package com.team.lottery.pay.util.shandeutil;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.alibaba.fastjson.JSONObject;

/**
 * 抽象发送报文类
 */
public class SandPayUtil {

	protected static final Log logger = LogFactory.getLog(SandPayUtil.class);

	/**
	 * 获取请求参数的拼接字符串.
	 * 
	 * @param o1
	 * @param o2
	 * @return
	 */
	public static JSONObject getSandData(Object o1, Object o2) {
		JSONObject data = new JSONObject();
		try {
			// head
			JSONObject head = new JSONObject();
			Method[] ms1 = o1.getClass().getMethods();
			for (int i = 0; i < ms1.length; i++) {
				if (ms1[i].getName().startsWith("get") && !ms1[i].getName().startsWith("getClass")) {
					String paramName = ms1[i].getName().substring(3);
					head.put(paramName.substring(0, 1).toLowerCase() + paramName.substring(1), ms1[i].invoke(o1, new Object[] {}));
				}
			}
			data.put("head", head);

			// body
			JSONObject body = new JSONObject();
			Method[] ms2 = o2.getClass().getMethods();
			for (int i = 0; i < ms2.length; i++) {
				if (ms2[i].getName().startsWith("get") && !ms2[i].getName().startsWith("getClass")) {
					String paramName = ms2[i].getName().substring(3);
					body.put(paramName.substring(0, 1).toLowerCase() + paramName.substring(1), ms2[i].invoke(o2, new Object[] {}));
				}
			}
			data.put("body", body);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		logger.info("杉德支付生成Data字符串为:" + data.toJSONString());
		return data;
	}

	/**
	 * 获取签名.
	 * 
	 * @param data
	 * @return
	 */
	public static String getSandSign(JSONObject data) {
		String sign = "";
		try {
			sign = URLEncoder.encode(new String(Base64.encodeBase64(CryptoUtil.digitalSign(data.toJSONString().getBytes("UTF-8"), CertUtil.getPrivateKey(), "SHA1WithRSA"))),
					"UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		logger.info("杉德支付生成Sign字符串为:" + sign);
		return sign;
	}

	/**
	 * 获取杉德支付需要的金额格式.
	 * 
	 * @param money
	 * @return
	 */
	public static String getSandAmountMoney(String money) {
		// 获取字符串长度.
		int length = money.length();
		// 最终要返回的结果初始化.
		String sandAmountMoney = "";
		if (length < 12) {
			int i = 12 - length;
			String str = "0";
			for (int j = 0; j < i - 1; j++) {
				str += "0";
			}
			sandAmountMoney = str + money;
		}
		logger.info("杉德支付生成金额为字符串为:[" + sandAmountMoney + "]分");
		return sandAmountMoney;
	}

	public static JSONObject requestServer(JSONObject header, JSONObject body, String reqAddr) {

		Map<String, String> reqMap = new HashMap<String, String>();
		JSONObject reqJson = new JSONObject();
		reqJson.put("head", header);
		reqJson.put("body", body);
		String reqStr = reqJson.toJSONString();
		String reqSign;
		// 签名
		try {
			reqSign = new String(Base64.encodeBase64(CryptoUtil.digitalSign(reqStr.getBytes("UTF-8"), CertUtil.getPrivateKey(), "SHA1WithRSA")));
		} catch (Exception e) {
			logger.error(e.getMessage());
			return null;
		}
		// 整体报文格式
		reqMap.put("charset", "UTF-8");
		reqMap.put("data", reqStr);
		reqMap.put("signType", "01");
		reqMap.put("sign", reqSign);
		reqMap.put("extend", "");

		String result;
		try {
			logger.info("请求报文：\n" + JSONObject.toJSONString(reqJson, true));
			result = HttpClient.doPost(reqAddr, reqMap, 300000, 600000);
			result = URLDecoder.decode(result, "UTF-8");
		} catch (IOException e) {
			logger.error(e.getMessage());
			return null;
		}

		Map<String, String> respMap = SDKUtil.convertResultStringToMap(result);
		String respData = respMap.get("data");
		String respSign = respMap.get("sign");

		// 验证签名
		boolean valid;
		try {
			valid = CryptoUtil.verifyDigitalSign(respData.getBytes("UTF-8"), Base64.decodeBase64(respSign), CertUtil.getPublicKey(), "SHA1WithRSA");
			if (!valid) {
				logger.error("verify sign fail.");
				return null;
			}
			logger.info("verify sign success");
			JSONObject respJson = JSONObject.parseObject(respData);
			if (respJson != null) {
				logger.info("响应码：[" + respJson.getJSONObject("head").getString("respCode") + "]");
				logger.info("响应描述：[" + respJson.getJSONObject("head").getString("respMsg") + "]");
				logger.info("响应报文：\n" + JSONObject.toJSONString(respJson, true));
			} else {
				logger.error("服务器请求异常！！！");
			}
			return respJson;

		} catch (Exception e) {
			logger.error(e.getMessage());
			return null;
		}
	}

}
