package com.team.lottery.pay.model.cryptpay;

public class CryptPayAddress {

	private String type;
	private String data;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	
	
}
