package com.team.lottery.pay.model.wannian;

/**
 * 万年支付
 * @author Owner
 *
 */
public class WanNianPay {

	//商户 ID
	public String cpId;
	//支付类型
	public String channel;
	//支付金额（单位分）
	public String money;
	//商品名
	public String subject;
	//商品说明
	public String description;
	//商户订单号
	public String orderIdCp;
	//异步通知地址
	public String notifyUrl;
	//当前时间戳（13 位）
	public String timestamp;
	//用户 ip
	public String ip;
	//版本号（固定 1）
	public String version;
	//通道方式（固定applyqr）
	public String command;
	//签名
	public String sign;
	
	public String getCpId() {
		return cpId;
	}
	public void setCpId(String cpId) {
		this.cpId = cpId;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public String getMoney() {
		return money;
	}
	public void setMoney(String money) {
		this.money = money;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getOrderIdCp() {
		return orderIdCp;
	}
	public void setOrderIdCp(String orderIdCp) {
		this.orderIdCp = orderIdCp;
	}
	public String getNotifyUrl() {
		return notifyUrl;
	}
	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getCommand() {
		return command;
	}
	public void setCommand(String command) {
		this.command = command;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	@Override
	public String toString() {
		return "WanNianPay [cpId=" + cpId + ", channel=" + channel + ", money=" + money + ", subject=" + subject
				+ ", description=" + description + ", orderIdCp=" + orderIdCp + ", notifyUrl=" + notifyUrl
				+ ", timestamp=" + timestamp + ", ip=" + ip + ", version=" + version + ", command=" + command
				+ ", sign=" + sign + "]";
	}
	
	
}
