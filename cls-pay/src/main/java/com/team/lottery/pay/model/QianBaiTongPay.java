package com.team.lottery.pay.model;
/**
 * 钱佰通支付
 * @author Administrator
 *
 */
public class QianBaiTongPay {
	
	/**
	 * 该笔订单总金额(元,精确到小数两位).
	 */
	private String X1_Amount;
	
	/**
	 * 订单号,格式：数字，字母，下划线，竖划线，中 划线
	 */
	private String X2_BillNo;

	/**
	 * 商户ID.
	 */
	private String X3_MerNo;
	
	/**
	 * 服务端后台支付状态通知
	 */
	private String X4_ReturnURL;
	
	/**
	 * 参数加密串
	 */
	private String X6_MD5info;
	
	/**
	 * 商户前台页面跳转通知URL
	 */
	private String X5_NotifyURL;
	
	/**
	 * 支付方式.
	 */
	private String X7_PaymentType;
	
	/**
	 * 商户备注信息
	 */
	private String X8_MerRemark;
	
	/**
	 * 客户端ip，微信h5支付必传。
	 */
	private String X9_ClientIp;
	
	/**
	 * 银行卡号 快捷支付时必传
	 */
	private String X10_AccNo;

	/**
	 * APP支付 传空即可（已废弃）
	 */
	private String isApp;
	
	

	public String getX1_Amount() {
		return X1_Amount;
	}



	public void setX1_Amount(String x1_Amount) {
		X1_Amount = x1_Amount;
	}



	public String getX2_BillNo() {
		return X2_BillNo;
	}



	public void setX2_BillNo(String x2_BillNo) {
		X2_BillNo = x2_BillNo;
	}



	public String getX3_MerNo() {
		return X3_MerNo;
	}



	public void setX3_MerNo(String x3_MerNo) {
		X3_MerNo = x3_MerNo;
	}



	public String getX4_ReturnURL() {
		return X4_ReturnURL;
	}



	public void setX4_ReturnURL(String x4_ReturnURL) {
		X4_ReturnURL = x4_ReturnURL;
	}



	public String getX6_MD5info() {
		return X6_MD5info;
	}



	public void setX6_MD5info(String x6_MD5info) {
		X6_MD5info = x6_MD5info;
	}



	public String getX5_NotifyURL() {
		return X5_NotifyURL;
	}



	public void setX5_NotifyURL(String x5_NotifyURL) {
		X5_NotifyURL = x5_NotifyURL;
	}



	public String getX7_PaymentType() {
		return X7_PaymentType;
	}



	public void setX7_PaymentType(String x7_PaymentType) {
		X7_PaymentType = x7_PaymentType;
	}



	public String getX8_MerRemark() {
		return X8_MerRemark;
	}



	public void setX8_MerRemark(String x8_MerRemark) {
		X8_MerRemark = x8_MerRemark;
	}



	public String getX9_ClientIp() {
		return X9_ClientIp;
	}



	public void setX9_ClientIp(String x9_ClientIp) {
		X9_ClientIp = x9_ClientIp;
	}



	public String getX10_AccNo() {
		return X10_AccNo;
	}



	public void setX10_AccNo(String x10_AccNo) {
		X10_AccNo = x10_AccNo;
	}



	public String getIsApp() {
		return isApp;
	}



	public void setIsApp(String isApp) {
		this.isApp = isApp;
	}

	public String toString(){
		return " 商户 号:"+X3_MerNo + " 交易金额:" + X1_Amount+
			   " 商户流水号:"+X2_BillNo + " 支付方式:" + X7_PaymentType+
			   " 通知地址:" + X4_ReturnURL+
			   " 备注:"+X8_MerRemark +" 数据签名:" + X6_MD5info +"" ;
	}
	
	
}
