package com.team.lottery.pay.util;



/**
 * 加密的工具类
 */
public class RsaUtilss {
    private static  int enSegmentSize=117;//加密长度
    private static int deSegmentSize=128;//解密长度

    public static String dencipher(String mer_private_key, String data){
        RsaHelper rsa = new RsaHelper();
        String deTxt = rsa.decipher(data, mer_private_key,deSegmentSize);
        return deTxt;
    }

    public static String merencipher(String mer_public_key,String data,int type){
        RsaHelper rsa = new RsaHelper();
        String ciphertext = rsa.encipher(data, mer_public_key,enSegmentSize,type);
        return  ciphertext;
    }
}
