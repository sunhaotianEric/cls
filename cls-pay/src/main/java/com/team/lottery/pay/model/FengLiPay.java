package com.team.lottery.pay.model;

public class FengLiPay {

	private String custid;
	private String out_trade_no;
	private String body;
	private String attach;
	private String total_fee;
	private String notify_url;
	private String return_url;
	private String sign;
	private String service;
	private String charset;
	private String sign_type;

	public String getCustid() {
		return custid;
	}

	public void setCustid(String custid) {
		this.custid = custid;
	}

	public String getOut_trade_no() {
		return out_trade_no;
	}

	public void setOut_trade_no(String out_trade_no) {
		this.out_trade_no = out_trade_no;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getAttach() {
		return attach;
	}

	public void setAttach(String attach) {
		this.attach = attach;
	}

	public String getTotal_fee() {
		return total_fee;
	}

	public void setTotal_fee(String total_fee) {
		this.total_fee = total_fee;
	}

	public String getNotify_url() {
		return notify_url;
	}

	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}

	public String getReturn_url() {
		return return_url;
	}

	public void setReturn_url(String return_url) {
		this.return_url = return_url;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}
	
	public String getCharset() {
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}

	public String getSign_type() {
		return sign_type;
	}

	public void setSign_type(String sign_type) {
		this.sign_type = sign_type;
	}

	public String toString() {
		return " custid:" + custid + " out_trade_no:" + out_trade_no + " body:" + body + " attach:" + attach + " total_fee:" + total_fee
				+ " notify_url:" + notify_url + " return_url:" + return_url + " sign:" + sign + " service:" + service+" charset:" + charset+" sign_type:" + sign_type;
	}

}
