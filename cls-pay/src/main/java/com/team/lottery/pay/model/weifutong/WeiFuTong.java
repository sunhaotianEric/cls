package com.team.lottery.pay.model.weifutong;

/**
 * 第三方支付微付通(澳门威尼斯人.)
 * 
 * @author Jamine
 *
 */
public class WeiFuTong {
	
	// 商户ID.
	private String shop_id;
	// 自己品台用户ID.
	private String user_id;
	// 支付金额.
	private String money;
	// 支付方式.(wechat,alipay)
	private String type;
	// 签名.
	private String sign;
	// 商家订单号.
	private String shop_no;
	// 异步通知地址(做入账处理).
	private String notify_url;
	// 支付成功跳转地址.
	private String return_url;

	public String getShop_id() {
		return shop_id;
	}

	public void setShop_id(String shop_id) {
		this.shop_id = shop_id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getShop_no() {
		return shop_no;
	}

	public void setShop_no(String shop_no) {
		this.shop_no = shop_no;
	}

	public String getNotify_url() {
		return notify_url;
	}

	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}

	public String getReturn_url() {
		return return_url;
	}

	public void setReturn_url(String return_url) {
		this.return_url = return_url;
	}

}
