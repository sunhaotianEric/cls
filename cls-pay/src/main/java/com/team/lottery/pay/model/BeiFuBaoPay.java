package com.team.lottery.pay.model;
public class BeiFuBaoPay {
	private String memberNumber;
	
	private String charset;
	
	private String method;
	
	private String sign;
	
	private String signType;
	
	private String version;
	
	private String requestTime;
	
	private String defrayalType;
	
	private String memberOrderNumber;
	
	private String tradeCheckCycle;
	
	private String orderTime;
	
	private String currenciesType;
	
	private String tradeAmount;
	
	private String commodityBody;
	
	private String commodityDetail;
	
	private String commodityRemark;
	
	private String notifyUrl;
	
	private String returnUrl;
	
	private String terminalIP;
	
	private String terminalId;
	
	private String userId;
	
	private String attach;
	
	private String remark;
	
	
	

	public String getMemberNumber() {
		return memberNumber;
	}




	public void setMemberNumber(String memberNumber) {
		this.memberNumber = memberNumber;
	}




	public String getCharset() {
		return charset;
	}




	public void setCharset(String charset) {
		this.charset = charset;
	}




	public String getMethod() {
		return method;
	}




	public void setMethod(String method) {
		this.method = method;
	}




	public String getSign() {
		return sign;
	}




	public void setSign(String sign) {
		this.sign = sign;
	}




	public String getSignType() {
		return signType;
	}




	public void setSignType(String signType) {
		this.signType = signType;
	}




	public String getVersion() {
		return version;
	}




	public void setVersion(String version) {
		this.version = version;
	}




	public String getRequestTime() {
		return requestTime;
	}




	public void setRequestTime(String requestTime) {
		this.requestTime = requestTime;
	}




	public String getDefrayalType() {
		return defrayalType;
	}




	public void setDefrayalType(String defrayalType) {
		this.defrayalType = defrayalType;
	}




	public String getMemberOrderNumber() {
		return memberOrderNumber;
	}




	public void setMemberOrderNumber(String memberOrderNumber) {
		this.memberOrderNumber = memberOrderNumber;
	}




	public String getTradeCheckCycle() {
		return tradeCheckCycle;
	}




	public void setTradeCheckCycle(String tradeCheckCycle) {
		this.tradeCheckCycle = tradeCheckCycle;
	}




	public String getOrderTime() {
		return orderTime;
	}




	public void setOrderTime(String orderTime) {
		this.orderTime = orderTime;
	}




	public String getCurrenciesType() {
		return currenciesType;
	}




	public void setCurrenciesType(String currenciesType) {
		this.currenciesType = currenciesType;
	}




	public String getTradeAmount() {
		return tradeAmount;
	}




	public void setTradeAmount(String tradeAmount) {
		this.tradeAmount = tradeAmount;
	}




	public String getCommodityBody() {
		return commodityBody;
	}




	public void setCommodityBody(String commodityBody) {
		this.commodityBody = commodityBody;
	}




	public String getCommodityDetail() {
		return commodityDetail;
	}




	public void setCommodityDetail(String commodityDetail) {
		this.commodityDetail = commodityDetail;
	}




	public String getCommodityRemark() {
		return commodityRemark;
	}




	public void setCommodityRemark(String commodityRemark) {
		this.commodityRemark = commodityRemark;
	}




	public String getNotifyUrl() {
		return notifyUrl;
	}




	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}




	public String getReturnUrl() {
		return returnUrl;
	}




	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}




	public String getTerminalIP() {
		return terminalIP;
	}




	public void setTerminalIP(String terminalIP) {
		this.terminalIP = terminalIP;
	}




	public String getTerminalId() {
		return terminalId;
	}




	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}




	public String getUserId() {
		return userId;
	}




	public void setUserId(String userId) {
		this.userId = userId;
	}




	public String getAttach() {
		return attach;
	}




	public void setAttach(String attach) {
		this.attach = attach;
	}




	public String getRemark() {
		return remark;
	}




	public void setRemark(String remark) {
		this.remark = remark;
	}

	
	public String toString(){
		return " memberNumber:"+memberNumber + " charset:" + charset+
			   " method:"+method + " sign:" + sign+
			   " signType:" + signType+
			   " version:"+version +" requestTime:" + requestTime +
			   " defrayalType:" + defrayalType+
			   " memberOrderNumber:"+memberOrderNumber +" tradeCheckCycle:" + tradeCheckCycle +
			   " orderTime:" + orderTime+" currenciesType:"+currenciesType +
			   " tradeAmount:" + tradeAmount +" commodityBody:"+commodityBody +
			   " commodityDetail:" + commodityDetail +" commodityRemark:"+commodityRemark +
			   " notifyUrl:" + notifyUrl +" returnUrl:"+returnUrl+
			   " terminalIP:" + terminalIP +" terminalId:"+terminalId+
			   " userId:" + userId +" attach:"+attach+" remark:" + remark ;
	}
	
	
}
