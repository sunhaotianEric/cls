package com.team.lottery.pay.model.zhaoqianpay;

/**
 * 兆乾支付发起对象.
 * 
 * @author jamine
 *
 */
public class ZhaoQianPay {
	// 商户编号.
	private String pay_memberid;
	// 商户订单号.
	private String pay_orderid;
	// 订单金额.
	private String pay_amount;
	// 订单日期.
	private String pay_applydate;
	// 银行编号.
	private String pay_bankcode;
	// 异步通知URL.
	private String pay_notifyurl;
	// 同步跳转URL.
	private String pay_callbackurl;
	// 订单备注说明.
	private String pay_attach;
	// md5签名串.
	private String pay_md5sign;
	// 商品名称.
	private String pay_productname;
    
	
	public String getPay_memberid() {
		return pay_memberid;
	}


	public void setPay_memberid(String pay_memberid) {
		this.pay_memberid = pay_memberid;
	}


	public String getPay_orderid() {
		return pay_orderid;
	}


	public void setPay_orderid(String pay_orderid) {
		this.pay_orderid = pay_orderid;
	}


	public String getPay_amount() {
		return pay_amount;
	}


	public void setPay_amount(String pay_amount) {
		this.pay_amount = pay_amount;
	}


	public String getPay_applydate() {
		return pay_applydate;
	}


	public void setPay_applydate(String pay_applydate) {
		this.pay_applydate = pay_applydate;
	}


	public String getPay_bankcode() {
		return pay_bankcode;
	}


	public void setPay_bankcode(String pay_bankcode) {
		this.pay_bankcode = pay_bankcode;
	}


	public String getPay_notifyurl() {
		return pay_notifyurl;
	}


	public void setPay_notifyurl(String pay_notifyurl) {
		this.pay_notifyurl = pay_notifyurl;
	}


	public String getPay_callbackurl() {
		return pay_callbackurl;
	}


	public void setPay_callbackurl(String pay_callbackurl) {
		this.pay_callbackurl = pay_callbackurl;
	}


	public String getPay_attach() {
		return pay_attach;
	}


	public void setPay_attach(String pay_attach) {
		this.pay_attach = pay_attach;
	}


	public String getPay_md5sign() {
		return pay_md5sign;
	}


	public void setPay_md5sign(String pay_md5sign) {
		this.pay_md5sign = pay_md5sign;
	}


	public String getPay_productname() {
		return pay_productname;
	}


	public void setPay_productname(String pay_productname) {
		this.pay_productname = pay_productname;
	}


	@Override
	public String toString() {
		return "兆乾支付发起对象 [商户编号=" + pay_memberid + ", 商户订单号=" + pay_orderid + "订单金额=" + pay_amount + "元, 订单日期="
				+ pay_applydate + ", 银行编号=" + pay_bankcode + ", 异步通知URL=" + pay_notifyurl + ", 同步跳转URL=" + pay_callbackurl + ", 订单备注说明="
				+ pay_attach + ", md5签名串=" + pay_md5sign + ", 商品名称=" + pay_productname +"]";
	}
	
}
