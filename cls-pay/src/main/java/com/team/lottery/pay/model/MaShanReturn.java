package com.team.lottery.pay.model;
/**
 * 码闪支付返回对象
 * @author hahah
 *
 */
public class MaShanReturn {
	//返回的数据跳转地址;
	private String data;
	//返回的签名;
	private String sign;
	//返回的消息;
	private String message;
	//返回的状态码;
	private String status;
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
