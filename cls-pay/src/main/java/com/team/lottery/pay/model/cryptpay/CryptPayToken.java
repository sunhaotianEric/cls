package com.team.lottery.pay.model.cryptpay;

public class CryptPayToken {

	private String type;
	private CryptPayTokenData data;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public CryptPayTokenData getData() {
		return data;
	}
	public void setData(CryptPayTokenData data) {
		this.data = data;
	}
	
	
}
