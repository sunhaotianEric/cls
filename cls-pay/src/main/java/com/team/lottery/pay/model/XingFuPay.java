package com.team.lottery.pay.model;
public class XingFuPay {
	//接口名字
	public String service;
	//接口版本
	public String version;
	//商户账号
	public String merId;
	//类型ID
	public String typeId;
	//商户订单号
	public String tradeNo;
	//交易日期
	public String tradeDate;
	//订单金额
	public String amount;
	//支付结果通知地址
	public String notifyUrl;
	//商户参数
	public String extra;
	//交易摘要
	public String summary;
	//超时时间
	public String expireTime;
	//客户端IP 
	public String clientIp;
	//签名
	public String sign;
	//银行代码
	public String bankId;

	
	
	public String getService() {
		return service;
	}



	public void setService(String service) {
		this.service = service;
	}



	public String getVersion() {
		return version;
	}



	public void setVersion(String version) {
		this.version = version;
	}



	public String getMerId() {
		return merId;
	}



	public void setMerId(String merId) {
		this.merId = merId;
	}



	public String getTypeId() {
		return typeId;
	}



	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}



	public String getTradeNo() {
		return tradeNo;
	}



	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}



	public String getTradeDate() {
		return tradeDate;
	}



	public void setTradeDate(String tradeDate) {
		this.tradeDate = tradeDate;
	}



	public String getAmount() {
		return amount;
	}



	public void setAmount(String amount) {
		this.amount = amount;
	}



	public String getNotifyUrl() {
		return notifyUrl;
	}



	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}



	public String getExtra() {
		return extra;
	}



	public void setExtra(String extra) {
		this.extra = extra;
	}



	public String getSummary() {
		return summary;
	}



	public void setSummary(String summary) {
		this.summary = summary;
	}



	public String getExpireTime() {
		return expireTime;
	}



	public void setExpireTime(String expireTime) {
		this.expireTime = expireTime;
	}



	public String getClientIp() {
		return clientIp;
	}



	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}



	public String getSign() {
		return sign;
	}



	public void setSign(String sign) {
		this.sign = sign;
	}

    

	public String getBankId() {
		return bankId;
	}



	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	

	public String toString(){
		return " 接口名字:"+service + " 接口版本:" + version+
			   " 商户账号:"+merId + " 类型ID:" + typeId+
			   " 商户订单号:"+tradeNo + " 交易日期:" + tradeDate+
			   " 订单金额:"+amount + " 支付结果通知地址:" + notifyUrl+
			   " 商户参数:"+extra + " 交易摘要:" + summary+
			   " 超时时间:" + expireTime+
			   " 客户端IP:"+clientIp +" 数据签名:" + sign +" 银行代码:"+ bankId;
	}
	
	
}
