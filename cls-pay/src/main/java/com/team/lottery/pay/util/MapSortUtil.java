package com.team.lottery.pay.util;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MapSortUtil {
	 Logger log = LoggerFactory.getLogger(this.getClass());
	public  String sign(Map<String, String> paramMap, String md5key) {
		if (paramMap == null || paramMap.isEmpty() || StringUtils.isBlank(md5key)) {
			return null;
		}
		Set<String> keyset = new TreeSet<String>();
		keyset.addAll(paramMap.keySet());
		StringBuilder paramBuilder = new StringBuilder();
		Iterator<String> iterator = keyset.iterator();
		while (iterator.hasNext()) {
			String key = iterator.next();
			if (StringUtils.isNotBlank(paramMap.get(key)) && !StringUtils.equals(key, "sign")) {
				paramBuilder.append("&").append(key).append("=").append(paramMap.get(key));
			}
		}
		String md5Str = paramBuilder.append("&key=").append(md5key).substring(1).toString();
		log.info("加密前的MD5: " + md5Str);
		return	Md5Util.getMD5ofStr(md5Str,"UTF-8").toUpperCase();
	}
}
