package com.team.lottery.pay.util;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 * 第三方支付获取文件数据!
 * 
 * @author Jamine
 *
 */
public class RequestGetParamUtils {

	public static Map<String, String> getParams(HttpServletRequest request) {
		DiskFileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		upload.setHeaderEncoding("UTF-8");
		List<FileItem> items = null;
		Map<String, String> param = null;
		try {
			try {
				items = upload.parseRequest(request);
			} catch (FileUploadException e) {
				e.printStackTrace();
			}
			param = new HashMap<String, String>();
			for (FileItem fileItem : items) {
				if (fileItem.isFormField()) {
					param.put(fileItem.getFieldName(), fileItem.getString("utf-8"));
				}
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return param;
	}
}
