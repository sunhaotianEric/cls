package com.team.lottery.pay.model;
public class XinMaPay {
	//商户ID
	public String messageid;
	//支付方式
	public String out_trade_no;
	//支付金额
	public String branch_id;
	//商户平台唯一订单号
	public String pay_type;
	//商户异步回调通知地址
	public String total_fee;
	//商户同步通知地址
	public String prod_name;
	//版本号
	public String prod_desc;
	//签名加密方式
	public String back_notify_url;
	//备注信息
	public String nonce_str;
	//分成标识
	public String attach_content;
	//签名
	public String sign;
    //银行简码
    public String bank_code;
    //银行类型
    public String bank_flag;
    //前端通知url
    public String front_notify_url;
    //支付发起方IP
    public String client_ip;


	public String getMessageid() {
		return messageid;
	}

	public void setMessageid(String messageid) {
		this.messageid = messageid;
	}

	public String getOut_trade_no() {
		return out_trade_no;
	}

	public void setOut_trade_no(String out_trade_no) {
		this.out_trade_no = out_trade_no;
	}

	public String getBranch_id() {
		return branch_id;
	}

	public void setBranch_id(String branch_id) {
		this.branch_id = branch_id;
	}

	public String getPay_type() {
		return pay_type;
	}

	public void setPay_type(String pay_type) {
		this.pay_type = pay_type;
	}

	public String getTotal_fee() {
		return total_fee;
	}

	public void setTotal_fee(String total_fee) {
		this.total_fee = total_fee;
	}

	public String getProd_name() {
		return prod_name;
	}

	public void setProd_name(String prod_name) {
		this.prod_name = prod_name;
	}

	public String getProd_desc() {
		return prod_desc;
	}

	public void setProd_desc(String prod_desc) {
		this.prod_desc = prod_desc;
	}

	public String getBack_notify_url() {
		return back_notify_url;
	}

	public void setBack_notify_url(String back_notify_url) {
		this.back_notify_url = back_notify_url;
	}

	public String getNonce_str() {
		return nonce_str;
	}

	public void setNonce_str(String nonce_str) {
		this.nonce_str = nonce_str;
	}

	public String getAttach_content() {
		return attach_content;
	}

	public void setAttach_content(String attach_content) {
		this.attach_content = attach_content;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}
    
	
	public String getBank_code() {
		return bank_code;
	}

	public void setBank_code(String bank_code) {
		this.bank_code = bank_code;
	}

	public String getBank_flag() {
		return bank_flag;
	}

	public void setBank_flag(String bank_flag) {
		this.bank_flag = bank_flag;
	}
	

	public String getFront_notify_url() {
		return front_notify_url;
	}

	public void setFront_notify_url(String front_notify_url) {
		this.front_notify_url = front_notify_url;
	}
	
	public String getClient_ip() {
		return client_ip;
	}

	public void setClient_ip(String client_ip) {
		this.client_ip = client_ip;
	}

	public String toString(){
		return " messageid:"+messageid + " 订单号:" + out_trade_no+
			   " 商户号:"+branch_id + " 支付编号:" + pay_type+
			   " 订单总金额:"+total_fee + " 订单标题:" + prod_name+
			   " 产品描述:"+prod_desc + " 后台通知url:" + back_notify_url+
			   " 随机字符串:"+nonce_str + " 附加信息:" + attach_content+
			   " 签名:" + sign +" 银行简码:" + bank_code +" 银行类型:" + bank_flag+" 前端通知url:" + front_notify_url+" 支付发起方IP:" + client_ip;
	}
	
	
}
