package com.team.lottery.pay.model;

public class LiuFuTongPay {

	public String mch_id;

	public String service;

	public String out_trade_no;

	public String trade_time;

	public String subject;

	public String body;

	public String attach;

	public String total_fee;

	public String spbill_create_ip;

	public String notify_url;

	public String return_url;

	public String url_type;

	public String clear_cycle;

	public String sign_type;

	public String sign;

	public String trade_type;

	public String buyer_id;

	public String buyer_logon_id;

	public String getMch_id() {
		return mch_id;
	}

	public void setMch_id(String mch_id) {
		this.mch_id = mch_id;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getOut_trade_no() {
		return out_trade_no;
	}

	public void setOut_trade_no(String out_trade_no) {
		this.out_trade_no = out_trade_no;
	}

	public String getTrade_time() {
		return trade_time;
	}

	public void setTrade_time(String trade_time) {
		this.trade_time = trade_time;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getAttach() {
		return attach;
	}

	public void setAttach(String attach) {
		this.attach = attach;
	}

	public String getTotal_fee() {
		return total_fee;
	}

	public void setTotal_fee(String total_fee) {
		this.total_fee = total_fee;
	}

	public String getSpbill_create_ip() {
		return spbill_create_ip;
	}

	public void setSpbill_create_ip(String spbill_create_ip) {
		this.spbill_create_ip = spbill_create_ip;
	}

	public String getNotify_url() {
		return notify_url;
	}

	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}

	public String getReturn_url() {
		return return_url;
	}

	public void setReturn_url(String return_url) {
		this.return_url = return_url;
	}

	public String getUrl_type() {
		return url_type;
	}

	public void setUrl_type(String url_type) {
		this.url_type = url_type;
	}

	public String getClear_cycle() {
		return clear_cycle;
	}

	public void setClear_cycle(String clear_cycle) {
		this.clear_cycle = clear_cycle;
	}

	public String getSign_type() {
		return sign_type;
	}

	public void setSign_type(String sign_type) {
		this.sign_type = sign_type;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getTrade_type() {
		return trade_type;
	}

	public void setTrade_type(String trade_type) {
		this.trade_type = trade_type;
	}

	public String getBuyer_id() {
		return buyer_id;
	}

	public void setBuyer_id(String buyer_id) {
		this.buyer_id = buyer_id;
	}

	public String getBuyer_logon_id() {
		return buyer_logon_id;
	}

	public void setBuyer_logon_id(String buyer_logon_id) {
		this.buyer_logon_id = buyer_logon_id;
	}

	public String toString() {
		return " mch_id:" + mch_id + " service:" + service + " out_trade_no:" + out_trade_no + " trade_time:" + trade_time + " subject:"
				+ subject + " body:" + body + " attach:" + attach + " total_fee:" + total_fee + " spbill_create_ip:" + spbill_create_ip
				+ " notify_url:" + notify_url + " return_url:" + return_url + " url_type:" + url_type + " clear_cycle:" + clear_cycle
				+ " sign_type:" + sign_type + " sign:" + sign + " trade_type:" + trade_type + " buyer_id:" + buyer_id + " buyer_logon_id:"
				+ buyer_logon_id;
	}

}
