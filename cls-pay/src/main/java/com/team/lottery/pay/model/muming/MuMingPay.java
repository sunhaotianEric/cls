package com.team.lottery.pay.model.muming;

/**
 * 慕名第三方支付支付请求参数对象.
 * 
 * @author Jamine
 *
 */
public class MuMingPay {
	// 商户号.
	public String pay_memberid;
	// 订单号
	public String pay_orderid;
	// 提交时间
	public String pay_applydate;
	// 银行编码
	public String pay_bankcode;
	// 服务端通知
	public String pay_notifyurl;
	// 页面跳转通知
	public String pay_callbackurl;
	// 订单金额
	public String pay_amount;
	// MD5签名
	public String pay_md5sign;
	// 附加字段
	public String pay_attach;
	// 商品名称
	public String pay_productname;
	// 商户品数量
	public String pay_productnum;
	// 商品描述
	public String pay_productdesc;
	// 商户链接地址
	public String pay_producturl;

	public String getPay_memberid() {
		return pay_memberid;
	}

	public void setPay_memberid(String pay_memberid) {
		this.pay_memberid = pay_memberid;
	}

	public String getPay_orderid() {
		return pay_orderid;
	}

	public void setPay_orderid(String pay_orderid) {
		this.pay_orderid = pay_orderid;
	}

	public String getPay_applydate() {
		return pay_applydate;
	}

	public void setPay_applydate(String pay_applydate) {
		this.pay_applydate = pay_applydate;
	}

	public String getPay_bankcode() {
		return pay_bankcode;
	}

	public void setPay_bankcode(String pay_bankcode) {
		this.pay_bankcode = pay_bankcode;
	}

	public String getPay_notifyurl() {
		return pay_notifyurl;
	}

	public void setPay_notifyurl(String pay_notifyurl) {
		this.pay_notifyurl = pay_notifyurl;
	}

	public String getPay_callbackurl() {
		return pay_callbackurl;
	}

	public void setPay_callbackurl(String pay_callbackurl) {
		this.pay_callbackurl = pay_callbackurl;
	}

	public String getPay_amount() {
		return pay_amount;
	}

	public void setPay_amount(String pay_amount) {
		this.pay_amount = pay_amount;
	}

	public String getPay_md5sign() {
		return pay_md5sign;
	}

	public void setPay_md5sign(String pay_md5sign) {
		this.pay_md5sign = pay_md5sign;
	}

	public String getPay_attach() {
		return pay_attach;
	}

	public void setPay_attach(String pay_attach) {
		this.pay_attach = pay_attach;
	}

	public String getPay_productname() {
		return pay_productname;
	}

	public void setPay_productname(String pay_productname) {
		this.pay_productname = pay_productname;
	}

	public String getPay_productnum() {
		return pay_productnum;
	}

	public void setPay_productnum(String pay_productnum) {
		this.pay_productnum = pay_productnum;
	}

	public String getPay_productdesc() {
		return pay_productdesc;
	}

	public void setPay_productdesc(String pay_productdesc) {
		this.pay_productdesc = pay_productdesc;
	}

	public String getPay_producturl() {
		return pay_producturl;
	}

	public void setPay_producturl(String pay_producturl) {
		this.pay_producturl = pay_producturl;
	}

	@Override
	public String toString() {
		return "慕名支付对象 [商户号=" + pay_memberid + ", 商户订单号=" + pay_orderid + ", 银行编码=" + pay_bankcode + ", 服务端通知=" + pay_notifyurl + ", 页面跳转通知="
				+ pay_callbackurl + ", 订单金额=" + pay_amount + ", 签名=" + pay_md5sign + "]";
	}

}
