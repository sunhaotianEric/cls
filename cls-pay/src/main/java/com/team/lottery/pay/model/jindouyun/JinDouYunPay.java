package com.team.lottery.pay.model.jindouyun;

/**
 * 筋斗云支付请求对象!
 * 
 * @author Jamine
 *
 */
public class JinDouYunPay {
	// 商户编号.
	private String merchantCode;
	// 商户订单号.
	private String merchantOrderNo;
	// 支付金额.
	private String orderAmount;
	// 产品代码固定ALIPAY_PERSON.
	private String productCode;
	// 订单标题.
	private String subject;
	// 随机数.
	private String nonceStr;
	// 异步通知地址.
	private String notifyUrl;
	// 签名.
	private String sign;

	public String getMerchantCode() {
		return merchantCode;
	}

	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}

	public String getMerchantOrderNo() {
		return merchantOrderNo;
	}

	public void setMerchantOrderNo(String merchantOrderNo) {
		this.merchantOrderNo = merchantOrderNo;
	}

	public String getOrderAmount() {
		return orderAmount;
	}

	public void setOrderAmount(String orderAmount) {
		this.orderAmount = orderAmount;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getNonceStr() {
		return nonceStr;
	}

	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}

	public String getNotifyUrl() {
		return notifyUrl;
	}

	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	@Override
	public String toString() {
		return "JinDouYunPay [商户号=" + merchantCode + ", 商户订单号=" + merchantOrderNo + ", 订单金额=" + orderAmount + ", 产品代码=" + productCode + ", 订单标题=" + subject + ", 随机数=" + nonceStr
				+ ", 异步通知地址=" + notifyUrl + ", 签名=" + sign + "]";
	}

}
