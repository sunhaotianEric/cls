package com.team.lottery.pay.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.team.lottery.pay.model.XinMaPay;
import com.team.lottery.util.JSONUtils;


/*import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;*/
import net.sf.json.JSONObject;

public class SendMessageUtil {
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static String FormatBizQueryParaMap(Map<String, String> paraMap,
			boolean urlencode) throws Exception {
		String buff = "";
		try {
			List infoIds = new ArrayList(paraMap.entrySet());

			Collections.sort(infoIds,
					new Comparator<Map.Entry<String, String>>() {
						public int compare(Map.Entry<String, String> o1,
								Map.Entry<String, String> o2) {
							return ((String) o1.getKey()).toString().compareTo(
									(String) o2.getKey());
						}
					});
			for (int i = 0; i < infoIds.size(); i++) {
				Map.Entry item = (Map.Entry) infoIds.get(i);

				if (item.getKey() != "") {
					String key = String.valueOf(item.getKey());
					String val = String.valueOf(item.getValue());
					if (urlencode) {
						val = URLEncoder.encode(val, "utf-8");
					}
					if ("".equals(val)) {
						continue;
					}
					buff = buff + key + "=" + val + "&";
					// buff = buff + key.toLowerCase() + "=" + val + "&";
				}
			}
			if (!buff.isEmpty())
				buff = buff.substring(0, buff.length() - 1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return buff;
	}

	@SuppressWarnings("unchecked")
	public static JSONObject sign(JSONObject req, String signKey) {
		try {
			if ("".equals(signKey) || null == signKey) {
				signKey = "";
			}
			Map<String, String> map = new HashMap<String, String>();
			XinMaPay xinMaPay=JSONUtils.toBean(req,XinMaPay.class);
			map = JSONUtils.toHashMap(xinMaPay);
			String noSignStr = FormatBizQueryParaMap(map, false);// 未签名的字符串
			String sign = MD5s.Sign(noSignStr, signKey);// 签名
			req.put("sign", sign);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return req;
	}

	public static String CreateNoncestr(int length) {
		String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		String res = "";
		for (int i = 0; i < length; i++) {
			Random rd = new Random();
			res = res + chars.charAt(rd.nextInt(chars.length() - 1));
		}
		return res;
	}

	public static byte[] sendMessage(XinMaPay xinMaPay,String address,String key) throws UnsupportedEncodingException {
		String AppKey = key;//注：这个需要换成实际的商户密钥
		String url = address;
		JSONObject reqData = new JSONObject();
		reqData.put("messageid",xinMaPay.getMessageid());
		reqData.put("out_trade_no", xinMaPay.getOut_trade_no());  //商户方的流水号，在同个商户号下必须唯一
		reqData.put("back_notify_url",xinMaPay.getBack_notify_url());   //商户的回调地址
		reqData.put("branch_id", xinMaPay.getBranch_id());   //注：这个需要换成实际的商户号
		reqData.put("prod_name", "测试支付");
		reqData.put("prod_desc", "测试支付描述");
		reqData.put("pay_type", xinMaPay.getPay_type());
		reqData.put("total_fee", xinMaPay.getTotal_fee());
		reqData.put("nonce_str", CreateNoncestr(32));
		reqData.put("client_ip", xinMaPay.getClient_ip());
		reqData.put("front_notify_url",xinMaPay.getFront_notify_url());
		reqData.put("bank_code",xinMaPay.getBank_code());
		reqData.put("bank_flag",xinMaPay.getBank_flag());
		
		reqData = sign(reqData, AppKey);
		System.out.println("发送报文" +JSONUtils.toJSONString(reqData));
		byte[] resByte = HttpsUtil.httpsPost(url, JSONUtils.toJSONString(reqData));
		if (null == resByte) {
			System.out.println("返回报文为空");
		} else {
			System.out.println("返回数据:" + new String(resByte, "UTF-8"));
		}
		return resByte;
	}
}
