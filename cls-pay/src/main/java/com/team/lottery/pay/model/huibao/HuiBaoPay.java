package com.team.lottery.pay.model.huibao;

/**
 * 惠宝支付请求对象
 * @author luocheng
 *
 */
public class HuiBaoPay {

	private String version;
	private String customerid;
	private String sdorderno;
	private String total_fee;
	private String paytype;
	private String bankcode;
	private String notifyurl;
	private String returnurl;
	private String remark;
	private String get_code;
	private String sign;
	
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getCustomerid() {
		return customerid;
	}
	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}
	public String getSdorderno() {
		return sdorderno;
	}
	public void setSdorderno(String sdorderno) {
		this.sdorderno = sdorderno;
	}
	public String getTotal_fee() {
		return total_fee;
	}
	public void setTotal_fee(String total_fee) {
		this.total_fee = total_fee;
	}
	public String getPaytype() {
		return paytype;
	}
	public void setPaytype(String paytype) {
		this.paytype = paytype;
	}
	public String getBankcode() {
		return bankcode;
	}
	public void setBankcode(String bankcode) {
		this.bankcode = bankcode;
	}
	public String getNotifyurl() {
		return notifyurl;
	}
	public void setNotifyurl(String notifyurl) {
		this.notifyurl = notifyurl;
	}
	public String getReturnurl() {
		return returnurl;
	}
	public void setReturnurl(String returnurl) {
		this.returnurl = returnurl;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getGet_code() {
		return get_code;
	}
	public void setGet_code(String get_code) {
		this.get_code = get_code;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	
	public String toString(){
		return " 版本号:"+ version + " 商户编号:" + customerid+
			   " 商户订单号:"+sdorderno + " 订单金额:" + total_fee+
			   " 支付编号:"+ paytype + " 银行编号:" + bankcode+
			   " 异步通知URL:" + notifyurl + " 同步跳转URL:" +  returnurl +
			   " 订单备注说明:"+ remark + " 获取微信二维码:" + get_code +
			   " md5签名串:" + sign;
	}
}
