package com.team.lottery.activemq;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.team.lottery.extvo.UserKickOutMessageVo;
import com.team.lottery.system.OnlineUserManager;
import com.team.lottery.vo.Login;

/**
 * 从ActiveMQ队列中接收从后台发起的踢出用户的消息，并进行踢出用户处理
 * @author luocheng
 *
 */
@Component("userKickOutQueueReceiver")
public class UserKickOutQueueReceiver implements MessageListener{
	
	
	private static Logger logger = LoggerFactory.getLogger(UserKickOutQueueReceiver.class);
	

	@Override
	public void onMessage(final Message message) {
		try {
			ObjectMessage objMsg = (ObjectMessage) message; 
			UserKickOutMessageVo userKickOutMessage = (UserKickOutMessageVo)objMsg.getObject();
			logger.info("接收到消息, 消息内容：踢出的业务系统[{}], 踢出的用户名[{}]", 
					userKickOutMessage.getBizSystem(), userKickOutMessage.getUserName());
			
			OnlineUserManager.delOnlineUser(Login.LOGTYPE_PC, userKickOutMessage.getBizSystem(), userKickOutMessage.getUserName());
		} catch (JMSException e) {
			logger.error("userKickOutQueueReceiver接收消息处理出现异常", e);
		}
	}
	
}
