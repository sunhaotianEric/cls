package com.team.lottery.filter;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.vo.BizSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.team.lottery.redis.JedisUtils;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.User;

import net.sf.json.JSONObject;
import redis.clients.jedis.Jedis;

public class LoginInterceptor implements HandlerInterceptor {

	private static Logger logger = LoggerFactory.getLogger(LoginInterceptor.class);

	private static Boolean loginBizsystemUsername = true; //重复登录业务的开关
	private static Set<String> allowUrlForThirdParty = new HashSet<String>();

	private static Set<String> allowUrl = new HashSet<String>();
	// 不需要拦截的url放在set当中用set数据防止重复
	static {
		// 帮助页.
		allowUrl.add("/info/helpCenter");
		allowUrl.add("/user/getRsaPublicKey");
		allowUrl.add("/system/refreshCache");
		allowUrl.add("/system/reSubscribe");
		// 获取二维码.
		allowUrl.add("/tools/getQrCodePic");
		allowUrl.add("/tools/getRandomCodePic");
		// 用户登录页相关请求.
		// allowUrl.add("/user/userMobileLogin");
		allowUrlForThirdParty.add("/user/userSimpleLogin");
		allowUrl.add("/user/guestUserPCLogin");
		// -------------------首页请求---------------//
		allowUrl.add("/announces/getIndexAnnounce");
		allowUrl.add("/activity/isShowNewUserGift");
		allowUrl.add("/lottery/getYestodayWin");
		allowUrl.add("/system/getBizSystemInfo");
		allowUrl.add("/code/getLastLotteryCode");
		allowUrl.add("/system/getUserCustomLotteryKind");
		allowUrl.add("/index/getHomeImage");
		allowUrl.add("/user/getCurrentUser");
		allowUrl.add("/code/getExpectAndDiffTime");
		allowUrl.add("/code/getBizSystemInfo");
		allowUrl.add("/announces/getNewestAnnounce");
		allowUrl.add("/announces/announceQuery");
		allowUrl.add("/index/getNearestWinLottery");
		allowUrl.add("/code/getShengXiaoNumber");
		allowUrl.add("/withdraw/withDrawApply");
		// -----------------登录页请求----------------//
		allowUrl.add("/user/getIsShowCheckCode");
		allowUrl.add("/user/userLogin");
		allowUrl.add("/system/getAppInfo");
		allowUrl.add("/user/userLogout");
		allowUrl.add("/register/regConfig");
		allowUrl.add("/lottery/userLottery");
		// -----------------跳转彩票大厅请求--------------//
		/*allowUrl.add("/lotteryinfo/getAllLotteryKindExpectAndDiffTime");*/
		allowUrl.add("/lotteryinfo/getLotterySwitchs");
		allowUrl.add("/code/getAllLotteryKindExpectAndDiffTime");
		allowUrl.add("/lotteryinfo/getAllLotteryKindExpectAndDiffTime");
		allowUrl.add("/code/getExpectAndDiffTime");
		allowUrl.add("/code/getAllLastLotteryCode");
		allowUrl.add("/code/getLastLotteryCode");

		//-----------------活动页面请求------------------//
		allowUrl.add("/activity/getAllActivity");
		allowUrl.add("/activity/getAllActivitys");
		allowUrl.add("/activity/dayConsumeActityApply");
		allowUrl.add("/activity/signinApply");
		allowUrl.add("/activity/newUserGiftApply");
		allowUrl.add("/activity/getNewUserGiftStatus");

		//-----------------活动详情请求-----------------//
		allowUrl.add("/activity/getActivityById");
		allowUrl.add("/activity/dayConsumeActityApply");
		allowUrl.add("/activity/signinApply");

		//-----------------新手大礼包请求---------------//
		allowUrl.add("/activity/newUserGiftApply");
		allowUrl.add("/activity/getNewUserGiftStatus");

		//--------------发现-中奖信息页面请求------------//
		allowUrl.add("/index/getNearestWinLottery");

		//--------------发现-开奖号码页面请求------------//
		allowUrl.add("/code/getLotteryCodeList");
		allowUrl.add("/code/getShengXiaoNumber");

		//------------发现-开奖号码详情页面请求-----------//
		allowUrl.add("/code/getNearestLotteryCode");

		//----------------注册页面请求------------------//
		allowUrl.add("/user/userDuplicate");
		allowUrl.add("/user/sendSmsVerifycode");
		allowUrl.add("/usersafe/sendPhoneCode");
		allowUrl.add("/user/userRegister");
		allowUrlForThirdParty.add("/user/simpleRegister");
		// allowUrl.add("/user/userMobileLogin");

		//--------------忘记密码页面相关请求-------------//
		allowUrl.add("/forgetpwd/forgetPwdNext");
		allowUrl.add("/forgetpwd/loadfindPwdWay");
		allowUrl.add("/forgetpwd/loadQuestions");
		allowUrl.add("/forgetpwd/verifyQuestion");
		allowUrl.add("/forgetpwd/verifyPhone");
		allowUrl.add("/forgetpwd/checkEmailCode");
		allowUrl.add("/forgetpwd/sendSmsVerifycode");
		allowUrl.add("/forgetpwd/sendEmailCode");
		allowUrl.add("/code/getBizSystemInfo");
		allowUrl.add("/forgetpwd/resetPwd");
		//------------------------pay-----------------------------//
		allowUrl.add("/deposit");
		allowUrlForThirdParty.add("/moneydetail/simpleTransfer");
		allowUrlForThirdParty.add("/moneydetail/simpleGetUserBalance");
		allowUrlForThirdParty.add("/moneydetail/simpleDeleteAllData");
		allowUrlForThirdParty.add("/order/getLotteryBySystem");
		//------------------------index-----------------------------//
		allowUrl.add("/index/getYesterdayProfit");
		//------------------------获取最新的公告内容-----------------------------//
		allowUrl.add("/announces/getNewestAnnounce");
		allowUrl.add("/system/getSiteConfig");
		// 玩法规则		
		allowUrl.add("/lotteryinfo/getRuleByType");
		allowUrl.add("/druid");
	}

	/**
	 * url判断方法：foreach遍历set元素与传入的url进行判断如果有包含的话返回true; 如果遍历完了没有包含的话返回false!
	 *
	 * @param
	 * @return
	 */
	public boolean isInAllowUrl(String url, Set<String> allowUrl) {
		for (String s : allowUrl) {
			if (url.contains(s)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 返回未登录的结果
	 *
	 * @param
	 * @return
	 */
	protected Map<String, Object> createNoLoginRes() {
		Map<String, Object> resMap = new HashMap<String, Object>();
		resMap.put("code", "nologin");
		resMap.put("data", "未登录,请先登陆!");
		return resMap;
	}

	/**
	 * 验证只能允许同一个用户在同一时间登陆一次有效返回提示
	 * @param
	 * @return
	 */
	protected Map<String, Object> createRepeatLoginError() {
		Map<String, Object> resMap = new HashMap<String, Object>();
		resMap.put("code", "repeat_login_error");
		resMap.put("data", "您已经退出或在另一个地点登陆,请重新登陆;若不是您本人操作,请及时修改密码!");
		return resMap;
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		String serverName= request.getHeader("REMOTE-HOST");//request.getServerName();
		if(StringUtils.isEmpty(serverName)) serverName =request.getServerName();
		String bizSystemCode= ClsCacheManager.getBizSystemByDomainUrl(serverName);
		BizSystem bizSystem=ClsCacheManager.getBizSystemByCode(bizSystemCode);
		//logger.info("业务系统名称>>>>>>:[{}] serverName[{}]  request.getRemoteHost()[{}]  ", bizSystemCode, serverName,request.getRemoteHost(),bizSystem);
		if (request.getMethod().equals("GET") || request.getMethod().equals("OPTIONS")) {
			return true;
		} else if (request.getMethod().equals("POST") ) {
			// 可以返回值的路由：获取二维码，帮助页面信息等
			String url = request.getServletPath();
			if(logger.isDebugEnabled()) {
				logger.debug("Method:[{}],请求url:{} ", request.getMethod(), url);
			}
			//第三方接入的接口逻辑判断
			if(isInAllowUrl(url,allowUrlForThirdParty)){
				if(url.equals("/moneydetail/simpleDeleteAllData")) return true;
				Map<String, Object> parma = new HashMap<>();
				String secretCode = request.getHeader("secretCode");
				//秘钥参数校验
				if(StringUtils.isEmpty(secretCode)||secretCode==null) {
					parma.put("code","-2");
					parma.put("msg","参数错误");
					setResponseHeader(response,request);
					PrintWriter out = response.getWriter();
					out.print(JSONObject.fromObject(parma));
					return false;
				}
				//商户秘钥校验
				logger.info("业务系统[{}] 业务系统密钥值为[{}]  收到的密钥值为：[{}]  ", bizSystemCode,bizSystem.getSecretCode(), secretCode);
				if(secretCode.equals(bizSystem.getSecretCode())) return true;
				//校验失败走下面
				setResponseHeader(response,request);
				PrintWriter out = response.getWriter();
				parma.put("code","-1");
				parma.put("msg","您无此权限");
				out.print(JSONObject.fromObject(parma));
				return false;
			}
			if (isInAllowUrl(url, allowUrl))    return true;
			// 处理controller用户未登录
			HttpSession session = request.getSession();
			if (session.getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER) == null) {
				response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin")); //解决跨域访问报错   
				response.setHeader("Access-Control-Allow-Credentials", "true"); //解决跨域访问报错 
				response.setHeader("Access-Control-Allow-Methods", "POST");
				response.setHeader("Access-Control-Allow-Headers", "x-requested-with,content-type");
				//响应的有效时间为 86400 秒，也就是 24 小时。在有效时间内，浏览器无须为同一请求再次发起预检请求
				response.setHeader("Access-Control-Max-Age", "86400");
				response.setHeader("Vary", "Origin");
				response.setHeader("Content-Type", "application/json;charset=UTF-8"); //返回没有Content-Type

				PrintWriter out = response.getWriter();
				Map<String, Object> map = createNoLoginRes();
				out.print(JSONObject.fromObject(map));
				return false;
			}
			if (loginBizsystemUsername) {
				//验证只能允许同一个用户在同一时间登陆一次有效，本次登陆将上次登陆剔除
				Long timestamp = (Long) session.getAttribute(ConstantUtil.RE_LOGIN_TIMESTAMP);//用户当前会话的session保存的时间戳
				User loginUser = (User)session.getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
				String key = "login:"+loginUser.getBizSystem()+":"+loginUser.getUserName();
				String timsFromRedis = JedisUtils.get(key);//最新登陆的时间戳
				if (StringUtils.isNotBlank(timsFromRedis) && timestamp!=null) {//用户正常登录后保存有时间戳，timsFromRedis==null表示第一次登陆需要放过
					if (!(timestamp+"").equals(timsFromRedis)) {//相等说明用户没有进行第二次正常登陆
						//主动失效被踢出去的用户session
						session.invalidate();
						setResponseHeader(response,request);
						PrintWriter out = response.getWriter();
						Map<String, Object> map = createRepeatLoginError();
						out.print(JSONObject.fromObject(map));
						return false;
					}
				}
			}
		}
		return true;
	}

	public void setResponseHeader (HttpServletResponse response,HttpServletRequest request){
		//重置response
//						response.reset();
		//设置编码格式
		response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin")); //解决跨域访问报错
		response.setHeader("Access-Control-Allow-Credentials", "true"); //解决跨域访问报错
		response.setHeader("Access-Control-Allow-Methods", "POST");
		response.setHeader("Access-Control-Allow-Headers", "x-requested-with,content-type");
		//响应的有效时间为 86400 秒，也就是 24 小时。在有效时间内，浏览器无须为同一请求再次发起预检请求
		response.setHeader("Access-Control-Max-Age", "86400");
		response.setHeader("Vary", "Origin");
		response.setHeader("Content-Type", "application/json;charset=UTF-8"); //返回没有Content-Type
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json;charset=UTF-8");
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

	}
}
