package com.team.lottery.system;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.enums.ERedisChannel;
import com.team.lottery.redis.mq.RedisThread;
import com.team.lottery.system.cache.LotteryIssueCache;
import com.team.lottery.system.message.MessageQueueThread;


public class SystemListener implements ServletContextListener{

	private static Logger logger = LoggerFactory.getLogger(SystemListener.class);
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		WebApplicationContext app = WebApplicationContextUtils.getWebApplicationContext(arg0.getServletContext());
		
		
		//缓存数据初始化
		SystemCache.loadAllCacheData();
		
		//初始化当前系统常量配置
		SystemPropeties.serverNum = "1";
		SystemPropeties.loginType = "PC";

		//初始化业务系统缓存
		ClsCacheManager.initCache();
		//初始化聊天缓存
//		ChatCacheManager.initCache();
				
		
		//初始化开奖时间缓存
		LotteryIssueCache.initCache();
		
		//初始化业务系统参数配置
		BizSystemConfigManager.initAllBizSystemConfig();
		
		//启动消息接收处理线程
		MessageQueueThread messageQueueThread = new MessageQueueThread();
		messageQueueThread.start();
		
		//启动订阅线程
		new Thread(new RedisThread(ERedisChannel.CLSCACHE.name())).start();
		
		logger.info("cls-web9 start success");
		
//		final List<Thread> threads = new ArrayList<Thread>();
//		Thread thread  = new Thread(new RedisThread(ERedisChannel.CLSCACHE.name(),clsRedisPubSubListener));
//		threads.add(thread);
//		final ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
//		Thread startThread = new Thread(new Runnable() {
//			
//			@Override
//			public void run() {
//				//采用线程池启动线程集合  每隔4秒启动一个线程
//				for (int i = 0; i < threads.size(); i++) {
//					try {
//						Thread.sleep(1500 * 1000);
//					} catch (InterruptedException e) {
//						e.printStackTrace();
//					}
//					cachedThreadPool.execute(threads.get(i));
//				}
//			}
//		});
//		startThread.start();
		
	}

}
