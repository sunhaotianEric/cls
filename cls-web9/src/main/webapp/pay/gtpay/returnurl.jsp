<%@page import="com.team.lottery.pay.model.muming.MuMingPayReturn"%>
<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="com.team.lottery.pay.model.jindouyun.JinDouYunPayReturn"%>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@page import="java.io.*"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="net.sf.json.JSONObject"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.extvo.returnModel.DuDuPayReturnParam"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
    BufferedReader  streamReader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));  
    StringBuilder responseStrBuilder = new StringBuilder();  
    String inputStr;  
    while ((inputStr = streamReader.readLine()) != null) {
    	responseStrBuilder.append(inputStr);   
    }
    log.info("接收GT支付回调通知报文: "+responseStrBuilder.toString());
    JSONObject jsonObject = JSONUtils.toJSONObject(responseStrBuilder.toString());
    // 支付时间戳.
    String payTime = jsonObject.getString("payTime");
    // 平台订单号.
    String orderId = jsonObject.getString("orderId");
    // 支付状态(1是成功,2是失败).
    String payStatus = jsonObject.getString("payStatus");
    // 支付金额(以分为单位).
    String amount = jsonObject.getString("amount");
    // 签名.
    String sign = jsonObject.getString("sign");
   
    // 日志打印.
   	log.info("GT支付,回调返回参数支付时间戳=" + payTime + "订单号=" + orderId + "支付状态 = " + payStatus + "金额 = " + amount + "分,签名=" + sign );

	RechargeOrderService rechargeOrderService = (RechargeOrderService) ApplicationContextUtil.getBean("rechargeOrderService"); 
	RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(orderId);
	if (rechargeOrder == null ) {
		log.error("根据订单号[" + orderId + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知慕名云支付报文接收成功
		out.print("SUCCESS");
	}
	//获取秘钥.
	String Md5key = rechargeOrder.getSign();
	String MARK = "&";
	//拼接加密字符串.
	String mD5Str = "orderId=" + orderId + MARK +
					"payTime=" + payTime + MARK +
					"payStatus=" + payStatus + MARK + 
					"secreyKey=" + Md5key;
	
	log.info("当前MD5源码:" + mD5Str);
	//Md5加密串并且大写
	String WaitSign = Md5Util.getMD5ofStr(mD5Str).toUpperCase();
	log.info("加密后的MD5: " + WaitSign);

	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if (WaitSign.compareTo(sign) == 0) {
		boolean dealResult = false;
		if (!StringUtils.isEmpty(payStatus) && payStatus.equals("1")) {
			// 将金额从分转换为元.
			BigDecimal factMoneyValue = new BigDecimal(Integer.parseInt(amount)).divide(new BigDecimal(100));
			try {
				dealResult = rechargeOrderService.rechargeOrderSuccessDeal(orderId, factMoneyValue);
			} catch (UnEqualVersionException e) {
				log.error("GT支付支付充值时发现版本号不一致,订单号[" + orderId + "]");
				//启动新进程进行多次重试
				UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, orderId,
						factMoneyValue, "");
				thread.start();
			} catch (Exception e) {
				//异常不进行重试
				log.error(e.getMessage());
			}
		} else {
			if (!payStatus.equals("1")) {
				log.info("GT支付处理错误支付结果payState为：" +  payStatus);
				
			}
		}
		log.info("GT支付订单处理入账结果:{}", dealResult);
		//通知第三方报文接收成功
		out.println("SUCCESS");
		return;
	} else {
		log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容[" + WaitSign + "]");
		out.println("SUCCESS");
		return;
	}
%>