<%@page import="com.team.lottery.pay.model.muming.MuMingPay"%>
<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.team.lottery.service.QuickBankTypeService"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.team.lottery.vo.QuickBankType"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="java.net.URLEncoder"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.team.lottery.service.RechargeConfigService"%>
<%@page import="com.team.lottery.vo.RechargeConfig"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
	String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	String serialNumber = request.getParameter("serialNumber"); // 获取配置ID;
	String orderMoneyYuan = OrderMoney;

	User currentUser = (User) request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	if (currentUser == null) {
		response.sendRedirect(path + "/gameBet/login.html");
	}
	Date currTime = new Date();
    //时间以yyyy-MM-dd HH:mm:ss的方式表示
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.CHINA);
    //时间以yyyyMMDDHHmmss的方式表示 
   /*  SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss",Locale.CHINA);
    String webdate=new String(formatter1.format(currTime));
    String TradeDate=new String(formatter2.format(currTime));
 */
	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));
	
	log.info("业务系统[{}],用户名[{}],发起了慕名支付，生成订单号[{}],支付金额[{}],PayId[{}],payType[{}],chargePayId[{}]",
  			currentUser.getBizSystem(), currentUser.getUserName(), serialNumber, OrderMoney, payId, payType, chargePayId);
	String payUrl = chargePay.getPayUrl();
	String Address = chargePay.getAddress();
	log.info("payUrl: "+payUrl+" Address: "+Address);
	
	/* //同步跳转地址
	String notifyUrl = "";
	if (chargePay.getNotifyUrl().startsWith("http://") || chargePay.getNotifyUrl().startsWith("https://")) {
		notifyUrl = chargePay.getNotifyUrl();
	} else {
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
		notifyUrl = basePath + chargePay.getNotifyUrl();
	}
	 */
	// 慕名支付对象.
	MuMingPay muMingPay = new MuMingPay();
	muMingPay.setPay_memberid(chargePay.getMemberId());
	muMingPay.setPay_amount(OrderMoney);
	muMingPay.setPay_attach("sj");
	muMingPay.setPay_applydate(formatter1.format(currTime));
	muMingPay.setPay_bankcode(payId);
	muMingPay.setPay_callbackurl(chargePay.getNotifyUrl());
	muMingPay.setPay_notifyurl(chargePay.getReturnUrl());
	muMingPay.setPay_productname("cz");
	muMingPay.setPay_productdesc("rmb");
	muMingPay.setPay_productnum("1");
	muMingPay.setPay_producturl("localhost");
	muMingPay.setPay_orderid(serialNumber);
	
	//sign加密;
	String mark = "&";
	//加密原始串.
	String md5SignStr =  "pay_amount=" + muMingPay.getPay_amount() + mark + 
						 "pay_applydate=" + muMingPay.getPay_applydate() + mark + 
						 "pay_bankcode=" + muMingPay.getPay_bankcode() + mark + 
						 "pay_callbackurl=" + muMingPay.getPay_callbackurl() + mark + 
						 "pay_memberid=" + muMingPay.getPay_memberid() + mark + 
						 "pay_notifyurl=" + muMingPay.getPay_notifyurl() + mark + 
						 "pay_orderid=" + muMingPay.getPay_orderid()+ mark + 
						 "key=" + chargePay.getSign();

	//进行MD5小写加密
	String md5Sign = Md5Util.getMD5ofStr(md5SignStr).toUpperCase();
	//打印日志
	log.info("md5原始串: " + md5SignStr + "生成后的交易签名:" + md5Sign);
	//设置签名
	muMingPay.setPay_md5sign(md5Sign);


%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>
<body onload="pay.submit()">
<form method="post" name="pay" id="pay" action="<%=Address%>">
<TABLE>
<TR>
	<TD>
	<input name='pay_memberid' type='hidden' value= "<%=muMingPay.pay_memberid%>"/>
	<input name='pay_orderid' type='hidden' value= "<%=muMingPay.pay_orderid%>"/>
	<input name='pay_applydate' type='hidden' value= "<%=muMingPay.pay_applydate%>"/>
	<input name='pay_bankcode' type='hidden' value= "<%=muMingPay.pay_bankcode%>"/>
	<input name='pay_notifyurl' type='hidden' value= "<%=muMingPay.pay_notifyurl%>"/>
	<input name='pay_callbackurl' type='hidden' value= "<%=muMingPay.pay_callbackurl%>"/>		
	<input name='pay_amount' type='hidden' value= "<%=muMingPay.pay_amount%>" />
	<input name='pay_md5sign' type='hidden' value= "<%=muMingPay.pay_md5sign%>" />
	<input name='pay_attach' type='hidden' value= "<%=muMingPay.pay_attach%>"/>
	<input name='pay_productname' type='hidden' value= "<%=muMingPay.pay_productname%>"/>
	<input name='pay_productnum' type='hidden' value= "<%=muMingPay.pay_productnum%>"/>
	<input name='pay_productdesc' type='hidden' value= "<%=muMingPay.pay_productdesc%>"/>
	<input name='pay_producturl' type='hidden' value= "<%=muMingPay.pay_producturl%>"/>
	</TD>
</TR>
</TABLE>
	
</form>	

</body>
</html>
 
<%-- <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
<style>
body {
	text-align: center;
}

#weixinImg {
	margin-top: 150px;
	width: 280px;
}

p {
	font-size: 18px;
}
</style>
</head>

<c:if test="${basePath!=''}">
	<body>
		<img id="weixinImg" src='<%=basePath%>'></img>
		<img src="http://qr.topscan.com/api.php?bg=f3f3f3&fg=ff0000&gc=222222&el=l&w=684&m=30&text=<%=code%>"/>
		<p>
			请使用<span id="pay-way"><c:if test="${payType == 'ALIPAY' }">支付宝</c:if>
				<c:if test="${payType == 'WEIXIN' }">微信</c:if> <c:if
					test="${payType == 'QQPAY' }">QQ钱包</c:if> <c:if
					test="${payType == 'JDPAY' }">京东扫码</c:if> <c:if
					test="${payType == 'UNIONPAY' }">银联扫码</c:if></span>扫描二维码以完成支付
		</p>
	</body>
</c:if>
</html>
 --%>