<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@ page import="com.team.lottery.pay.util.RsaUtilss"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@ page import="com.team.lottery.pay.util.Base64Utils"%>
<%@ page import="com.team.lottery.pay.util.KeyGenUtil"%>
<%@ page import="com.team.lottery.pay.util.RSAUtils"%>
<%@ page import="com.team.lottery.pay.util.SignUtils"%>
<%@page import="com.team.lottery.vo.ChargePay,com.team.lottery.service.ChargePayService"%>
<%@page import="net.sf.json.JSONObject"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%  
    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String orderNum = request.getParameter("orderNo");//订单号
	String data = request.getParameter("data");//加密报文
	String merNo = request.getParameter("merNo");//商户号
	String sign = request.getParameter("sign");//签名
	log.info("请求返回报文: 订单号:"+orderNum+",加密报文:"+data+",商户号:"+merNo+",签名:"+sign);
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(orderNum);
	if(rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1){
		log.error("根据订单号[" + orderNum + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知码闪付接收报文成功
	    out.println("0");
		return;
	}
	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
	if(rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + orderNum + "]查找充值订单记录为空");
		//通知码闪接收报文成功
	    out.println("0");
		return;
	}
	
	//签名
	Long chargePayId = rechargeWithDrawOrder.getChargePayId();
	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	ChargePay chargePay = chargePayService.getPayMemmberMsg(Long.valueOf(chargePayId));
	//对data数据进行私钥解密
    String privateKey=chargePay.getPrivatekey();
  	//解密后得到的字符串格式为:{"":"","":"","":"",...}
	String data1=RsaUtilss.dencipher(privateKey, data);
	log.info("解密后的data:"+data1);
	//获取到JSON中的支付状态码
	JSONObject jsonObj=JSONObject.fromObject(data1);
	String amount=jsonObj.getString("amount");//金额
	String merNo1=jsonObj.getString("merNo");//商户号
	String orderNum1=jsonObj.getString("orderNum");//订单号
	String payStateCode=jsonObj.getString("payStateCode");//支付状态码
	String goodsName=jsonObj.getString("goodsName");//商品名称
	String notifyDate=jsonObj.getString("notifyDate");//通知时间
	//转为大写
	String WaitSign=Md5Util.getMD5ofStr(data1+chargePay.getSign(),"UTF-8").toUpperCase();
	log.info("原始串:"+data1+chargePay.getSign()+"生成的Sign串"+WaitSign+"返回的Sign串:"+sign);
	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if(WaitSign.compareTo(sign)==0){
		boolean dealResult = false;
		if(!StringUtils.isEmpty(payStateCode) && payStateCode.equals("10")){
			//将实际成功金额除以100
			BigDecimal factMoneyValue = new BigDecimal(amount).divide(new BigDecimal("100"));
			try {
				dealResult = rechargeWithDrawOrderService.rechargeOrderSuccessDeal(orderNum1,factMoneyValue);
			}catch(UnEqualVersionException e) {
		   		log.error("码闪付在线充值时发现版本号不一致,订单号["+orderNum1+"]");
		   		//启动新进程进行多次重试
		   		UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION
		   				, orderNum1,factMoneyValue,"");
		   		thread.start();
			} catch(Exception e){
				//异常不进行重试
				log.error(e.getMessage());
			}
		} else if(!StringUtils.isEmpty(payStateCode) && payStateCode.equals("2")) {
			if(!payStateCode.equals("10")){
				log.info("码闪付支付处理错误支付结果："+payStateCode);
			}
		}
		log.info("码闪付订单处理入账结果:{}", dealResult);
		//通知码闪接收报文成功	
	    out.println("0");
		return;
	}else{
		log.info("MD5校验失败,报文返回MD5签名内容["+ sign +"],计算MD5签名内容[" + WaitSign + "]");
		//让第三方继续补发
		out.println("1");
		return;
	}
%>