<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="com.team.lottery.pay.model.jindouyun.JinDouYunPayReturn"%>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@page import="java.io.*"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="net.sf.json.JSONObject"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.extvo.returnModel.DuDuPayReturnParam"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
 	// 商户号.
 	String memberid = request.getParameter("memberid");
    // 订单号.
 	String orderid = request.getParameter("orderid");
    // 订单金额.
 	String amount = request.getParameter("amount");
    // 交易流水号.
 	String transaction_id = request.getParameter("transaction_id");
    // 交易时间.
 	String datetime = request.getParameter("datetime");
    // 交易状态(00为成功).
 	String returncode = request.getParameter("returncode");
    // 签名
 	String sign = request.getParameter("sign");
   
    // 日志打印.
   	log.info("融腾支付,回调返回参数商户号=" + memberid + "商户订单号=" + orderid + "支付金额 = " + amount + "交易流水号 = " + transaction_id + "交易时间=" + datetime + ",状态=" + returncode + ",签名=" + sign);

	RechargeOrderService rechargeOrderService = (RechargeOrderService) ApplicationContextUtil.getBean("rechargeOrderService"); 
	RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(orderid);
	if (rechargeOrder == null ) {
		log.error("根据订单号[" + orderid + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知支付报文接收成功
		out.print("OK");
	}
	//获取秘钥.
	String Md5key = rechargeOrder.getSign();
	String MARK = "&";
	String mD5Str = "amount=" + amount + MARK +
					"datetime=" + datetime + MARK +
					"memberid=" + memberid + MARK + 
					"orderid=" + orderid + MARK + 
					"returncode=" + returncode + MARK + 
					"transaction_id=" + transaction_id + MARK + 
					"key=" + Md5key;
	
	log.info("当前MD5源码:" + mD5Str);
	//Md5加密小写
	String WaitSign = Md5Util.getMD5ofStr(mD5Str).toUpperCase();
	log.info("加密后的MD5: " + WaitSign);

	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if (WaitSign.compareTo(sign) == 0) {
		boolean dealResult = false;
		if (!StringUtils.isEmpty(returncode) && returncode.equals("00")) {
			// 将金额从分转换为元.
			BigDecimal factMoneyValue = new BigDecimal(amount);
			try {
				dealResult = rechargeOrderService.rechargeOrderSuccessDeal(orderid, factMoneyValue);
			} catch (UnEqualVersionException e) {
				log.error("盛世支付支付充值时发现版本号不一致,订单号[" + orderid + "]");
				//启动新进程进行多次重试
				UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, orderid,
						factMoneyValue, "");
				thread.start();
			} catch (Exception e) {
				//异常不进行重试
				log.error(e.getMessage());
			}
		} else {
			if (!returncode.equals("00")) {
				log.info("融腾支付处理错误支付结果payState为：" +  returncode);
				
			}
		}
		log.info("融腾支付订单处理入账结果:{}", dealResult);
		//通知第三方报文接收成功
		out.println("OK");
		return;
	} else {
		log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容[" + WaitSign + "]");
		out.println("OK");
		return;
	}
%>