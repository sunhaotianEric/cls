<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="java.math.*"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%  
    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String version = request.getParameter("version");//商户号
	String partner = request.getParameter("partner");//商户终端号
	String orderid = request.getParameter("orderid");//商户订单号
	String payamount = request.getParameter("payamount");//订单金额
	String opstate = request.getParameter("opstate");//订单状态
	String orderno = request.getParameter("orderno");//OK付交易号
	String okfpaytime = request.getParameter("okfpaytime");//支付完成时间
	String message = request.getParameter("message");//订单交易结果说明	
	String paytype = request.getParameter("paytype");//支付类型
	String remark = request.getParameter("remark");
	String Md5Sign = request.getParameter("sign");//MD5签名
	log.info("ok支付成功通知平台处理信息如下: 商户号: "+partner+" 订单号: "+orderid+" 支付结果: "+opstate+" 支付结果描述: "+message+" 实际成功金额: "+
			payamount+" 支付完成时间: "+okfpaytime+" 第三方返回MD5签名: "+Md5Sign);
  
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(orderid);
	if(rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1){
		log.error("根据订单号[" + orderid + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知ok付报文接收成功
	    out.println("success");
		return;
	}
	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
	if(rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + orderid + "]查找充值订单记录为空");
		//通知ok付报文接收成功
	    out.println("success");
		return;
	}
	
	//签名
	String Md5key = rechargeWithDrawOrder.getSign();
	String MARK = "&";
	String md5 = "version="+version+MARK+"partner="+partner+MARK+"orderid="+orderid+MARK+"payamount="+payamount+MARK+"opstate="+opstate+MARK+"orderno="+orderno+MARK
			     +"okfpaytime="+okfpaytime+MARK+"message="+message+MARK+"paytype="+paytype+MARK+"remark="+remark+MARK+"key="+Md5key;
	//Md5加密串
	String WaitSign = md5Util.getMD5ofStr(md5);

	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if(WaitSign.compareTo(Md5Sign)==0){
		boolean dealResult = false;
		if(!StringUtils.isEmpty(opstate) && opstate.equals("2")){
			//实际成功金额
			BigDecimal factMoneyValue = new BigDecimal(payamount);
			try {
				dealResult = rechargeWithDrawOrderService.rechargeOrderSuccessDeal(orderid,factMoneyValue);
			}catch(UnEqualVersionException e) {
		   		log.error("ok付在线充值时发现版本号不一致,订单号["+orderid+"]");
		   		//启动新进程进行多次重试
		   		UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION
		   				, orderid,factMoneyValue,"");
		   		thread.start();
			} catch(Exception e){
				//异常不进行重试
				log.error(e.getMessage());
			}	
		} else {
			if(!opstate.equals("2")){
				log.info("ok支付处理错误支付结果："+opstate+" 支付结果描述："+message);
			}
		}
		log.info("ok付订单处理入账结果:{}", dealResult);
		//通知ok付报文接收成功
	    out.println("success");
		return;
	}else{
		log.info("MD5校验失败,报文返回MD5签名内容["+ Md5Sign +"],计算MD5签名内容[" + WaitSign + "]");
		//让第三方继续补发
	    out.println("fail");
		return;
	}
%>