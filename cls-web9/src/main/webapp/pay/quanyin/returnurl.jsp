<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage=""%>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page
	import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
	String path = request.getContextPath();
	String payKey = request.getParameter("payKey");//
	String productName = request.getParameter("productName");//
	String orderNo = request.getParameter("orderNo");//订单号
	String orderPrice = request.getParameter("orderPrice");//
	String payWayCode = request.getParameter("payWayCode");//
	String payPayCode = request.getParameter("payPayCode");//
	String orderDate = request.getParameter("orderDate");//
	String orderTime = request.getParameter("orderTime");//
	String remark = request.getParameter("remark");//
	String trxNo = request.getParameter("trxNo");//
	String tradeStatus = request.getParameter("tradeStatus");//
	String sign = request.getParameter("sign");//
	log.info("全银支付成功通知平台处理信息如下: 商户ID: " + payKey + " 商户订单号: " + orderNo + " 订单金额: " + orderPrice + " MD5签名: " + sign);

	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService) ApplicationContextUtil
			.getBean("rechargeOrderService");
	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(orderNo);
	if (rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1) {
		log.error("根据订单号[" + orderNo + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知全银支付报文接收成功
		response.getOutputStream().write("success".getBytes("UTF-8"));
		return;
	}
	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
	if (rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + orderNo + "]查找充值订单记录为空");
		//通知全银支付报文接收成功
		response.getOutputStream().write("success".getBytes("UTF-8"));
		return;
	}

	//计算签名
	// 签名customerid={value}&status={value}&sdpayno={value}&sdorderno={value}&total_fee={value}&paytype={value}&{apikey}
	String Md5key = rechargeWithDrawOrder.getSign();
	String MARK = "&";
	String md5 = new String("orderDate=" + orderDate + MARK + "orderNo=" + orderNo + MARK + "orderPrice=" + orderPrice + MARK
			+ "orderTime=" + orderTime + MARK + "payKey=" + payKey + MARK + "payPayCode=" + payPayCode + MARK + "payWayCode="
			+ payWayCode + MARK + "productName=" + productName + MARK + "remark=" + remark + MARK + "tradeStatus=" + tradeStatus
			+ MARK + "trxNo=" + trxNo + MARK + "paySecret=" + Md5key);//MD5签名格式
	log.info("加密前的MD5: " + md5);
	// Md5加密串
	String WaitSign = Md5Util.getMD5ofStr(md5).toUpperCase();
	log.info("加密后的MD5: " + WaitSign);

	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if (WaitSign.compareTo(sign) == 0) {
		boolean dealResult = false;
		if (!StringUtils.isEmpty(tradeStatus) && tradeStatus.equals("SUCCESS")) {
			BigDecimal factMoneyValue = new BigDecimal(orderPrice);
			try {
				dealResult = rechargeWithDrawOrderService.rechargeOrderSuccessDeal(orderNo, factMoneyValue);
			} catch (UnEqualVersionException e) {
				log.error("全银支付在线充值时发现版本号不一致,订单号[" + orderNo + "]");
				//启动新进程进行多次重试
				UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
						UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, orderNo, factMoneyValue, "");
				thread.start();
			} catch (Exception e) {
				//异常不进行重试
				log.error(e.getMessage());
			}
		} else {
			if (!tradeStatus.equals("1")) {
				log.info("全银支付处理错误支付结果：" + tradeStatus);
			}
		}
		log.info("全银支付订单处理入账结果:{}", dealResult);
		//通知全银支付报文接收成功
		response.getOutputStream().write("success".getBytes("UTF-8"));
		return;
	} else {
		log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容[" + WaitSign + "]");
		//让第三方继续补发
		response.getOutputStream().write("error".getBytes("UTF-8"));
		return;
	}
%>