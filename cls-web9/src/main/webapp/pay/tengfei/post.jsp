<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>s
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.RSA"%>
<%@page import="com.team.lottery.pay.model.JianZhengBaoPay"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
 <%@page errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.InetAddress"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="com.team.lottery.extvo.returnModel.YinHeParameter"%>
<%@page import="com.team.lottery.pay.model.TengFeiPay"%>
<%@page import="java.util.*"%>
<%@page import="net.sf.json.JSONObject"%>
<%@ page import="com.team.lottery.pay.util.HttpClients"%>
<%@ page import="com.team.lottery.pay.util.RC4"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
    String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
    String orderMoneyYuan = OrderMoney;
    String serialNumber = request.getParameter("serialNumber");
 
    
    User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
	
	

	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));
	String  orderId=MakeOrderNum.makeOrderNum("CZ");
	log.info("用户名称: "+currentUser.getUserName()+" 用户 ID号: "+currentUser.getId()+" 生成订单号: "+orderId+" 订单金额: "+OrderMoney);
	
	if (!"".equals(OrderMoney)) 
	{	BigDecimal a;
		a = new BigDecimal(OrderMoney).multiply(BigDecimal.valueOf(100)); //使用分进行提交
		OrderMoney=String.valueOf(a.setScale(0));
	}
	else{
		OrderMoney = ("0");
	}

	String payUrl = chargePay.getPayUrl();//转发银河网关地址
	String Address = chargePay.getAddress(); //最终银河地址
	log.info("payUrl: "+payUrl+" Address: "+Address);
	TengFeiPay tengFeiPay =new TengFeiPay();
	tengFeiPay.setUid(chargePay.getMemberId());
	tengFeiPay.setPrice(OrderMoney);
	//tengFeiPay.setGoodsname(goodsname);
	tengFeiPay.setReturnurl(chargePay.getNotifyUrl());
	tengFeiPay.setNotifyurl(chargePay.getReturnUrl());
	tengFeiPay.setExtra("cz");
	tengFeiPay.setPaytype(payId);
	tengFeiPay.setOrderid(serialNumber);
	tengFeiPay.setOrderuid(String.valueOf(currentUser.getId()));
    
	String Md5key = chargePay.getSign();///////////md5密钥（KEY）
	String MARK = "&";
	String md5 =new String("extra="+tengFeiPay.getExtra()+MARK+"notifyurl="+tengFeiPay.getNotifyurl()+MARK+
			"orderid="+tengFeiPay.getOrderid()+MARK+"orderuid="+tengFeiPay.getOrderuid()+MARK+"paytype="+tengFeiPay.getPaytype()+MARK+"price="+tengFeiPay.getPrice()+
			MARK+"returnurl="+tengFeiPay.getReturnurl()+MARK+"uid="+tengFeiPay.getUid()+Md5key);//MD5签名格式
	String Signature = md5Util.getMD5ofStr(md5);

	System.out.println("Signature:" + Signature);
	tengFeiPay.setKey(Signature);

	JSONObject jsonObject = new JSONObject();
	jsonObject.put("uid", tengFeiPay.getUid());           // 平台分配的商户编号
	jsonObject.put("orderid", tengFeiPay.getOrderid());              // 商户生成的订单号需要保证系统唯一平台分配的商户编号
	jsonObject.put("orderuid", tengFeiPay.getOrderuid());           // 商户的用户ID，显示在商户后台中，方便对账(选填)
	jsonObject.put("price", tengFeiPay.getPrice());                         // 支付金额，单位(分)
	jsonObject.put("paytype", tengFeiPay.getPaytype());                      // 支付通道。 100:支付宝; 200:微信
	jsonObject.put("notifyurl", tengFeiPay.getNotifyurl());   // 商户服务端接收支付结果通知地址
	jsonObject.put("returnurl", tengFeiPay.getReturnurl());                    // 使用平台页面支付完成后会调转到此页面(选填)
//      map.put("goosname", "333");                   // 消费商品的名称，不填平台会默认设置一个商品名称"1.00元",
      jsonObject.put("extra", tengFeiPay.getExtra());
      jsonObject.put("key", tengFeiPay.getKey());
      System.out.println("result:" + jsonObject.toString());
	//
	/* JSONObject jsonObject = new JSONObject();
    jsonObject.put("context", context); */
    log.info("发送的密文context        :" + jsonObject.toString());

    JSONObject returnStr = HttpClients.doPost(Address, jsonObject);
	// 获取结果
	System.out.println("result:" + returnStr);

	JSONObject object = JSONObject.fromObject(returnStr);
	String code = object.getString("code");
	String msg = object.getString("msg");
	String err_msg = object.getString("err_msg");
	String data = object.getString("data");
	JSONObject object2 = JSONObject.fromObject(data);
	String result = object2.getString("result");
	JSONObject object3 = JSONObject.fromObject(result);
	String qrurl=object3.getString("qrurl");
	
	if(code.equals("1")){
		response.sendRedirect(qrurl);
	}else{
		out.print(err_msg);
	}
%>
 
<%-- <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta id="viewport" name="viewport"
	content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
<title>充值接口-提交信息处理</title>
<style>
body {
	text-align: center;
}

img {
	margin-top: 60px;
}

p {
	font-size: 36px;
}
</style>
</head>

<body>
	<img id="weixinImg" src='<%=path%>/tools/getQrCodePic?code=<%=codeUrl%>'></img>
	<p>
		请使用<span id="pay-way">
		    <c:if test="${payType == 'ALIPAY' }">支付宝</c:if>
			<c:if test="${payType == 'WEIXIN' }">微信</c:if>
			<c:if test="${payType == 'JDQB' }">京东钱包</c:if>
			<c:if test="${payType == 'QQPAY' }">QQ钱包</c:if>
			<c:if test="${payType == 'UNIONPAY' }">银联二维码</c:if>
			</span>扫描二维码以完成支付
	</p>

</body>
</html> --%>
