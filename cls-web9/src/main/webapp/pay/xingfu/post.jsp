<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.pay.model.XingFuPay"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
 <%@page errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URL"%>
<%@page import="com.team.lottery.util.JaxbUtil"%>
<%@page import="com.team.lottery.extvo.returnModel.XingFuData"%>
<%@page import="com.team.lottery.extvo.returnModel.XingFuDetailData"%>
<%@page import="java.net.InetAddress"%>
<%@page import="com.team.lottery.util.JaxbUtil"%>
<%@page import="com.team.lottery.extvo.returnModel.XinYuTokenData"%>
<%@page import="sun.misc.BASE64Decoder"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
    String orderMoneyYuan = OrderMoney;
 
    
    User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
	
	Date currTime = new Date();
    //时间以yyyy-MM-dd HH:mm:ss的方式表示
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.CHINA);
    //时间以yyyyMMDDHHmmss的方式表示 
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss",Locale.CHINA);
    String webdate=new String(formatter1.format(currTime));
    String TradeDate=new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));
	String  orderId=MakeOrderNum.makeOrderNum("CZ");
	log.info("用户名称: "+currentUser.getUserName()+" 用户 ID号: "+currentUser.getId()+" 生成订单号: "+orderId+" 订单金额: "+OrderMoney);
	
	
	if(!"".equals(OrderMoney)) {	
		 BigDecimal a;
		a = new BigDecimal(OrderMoney); //使用分进行提交
		OrderMoney=String.valueOf(a);
	}
	else{
		OrderMoney = ("0");
	} 
	String payUrl = chargePay.getPayUrl();//转发支付网关地址
	String Address = chargePay.getAddress(); //最终银宝地址
	log.info("payUrl: "+payUrl+" Address: "+Address);
	
	//信誉支付请求对象
	XingFuPay xingFuPay =new XingFuPay();
	if(payType.equals("BANK_TRANSFER") || payType.equals("QUICK_PAY")){
		xingFuPay.setService("TRADE.B2C");
	}else if(payType.equals("ALIPAY") || payType.equals("WEIXIN") || payType.equals("QQPAY") || payType.equals("UNIONPAY") || payType.equals("JDPAY")){
		xingFuPay.setService("TRADE.SCANPAY");
	}else if(payType.equals("ALIPAYWAP") || payType.equals("WEIXINWAP") || payType.equals("QQPAYWAP")){
		xingFuPay.setService("TRADE.H5PAY");
	}
	xingFuPay.setVersion("1.0.0.0");
	xingFuPay.setMerId(chargePay.getMemberId());
	//xingFuPay.setTypeId(payId);
	if(payType.equals("ALIPAY") || payType.equals("ALIPAYWAP")){
		xingFuPay.setTypeId("1");
	}else if(payType.equals("WEIXIN") || payType.equals("WEIXINWAP")){
		xingFuPay.setTypeId("2");
	}else if(payType.equals("QQPAY") || payType.equals("QQPAYWAP")){
		xingFuPay.setTypeId("3");
	}else if(payType.equals("UNIONPAY")){
		xingFuPay.setTypeId("4");
	}else if(payType.equals("JDPAY")){
		xingFuPay.setTypeId("5");
	}
	/* xingFuPay.setTypeId("1"); */
	xingFuPay.setTradeNo(orderId);
	SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd",Locale.CHINA);
	String tradeDate=new String(formatter.format(new Date()));
	xingFuPay.setTradeDate(tradeDate);
	xingFuPay.setAmount(OrderMoney);
	xingFuPay.setNotifyUrl(chargePay.getReturnUrl());
	InetAddress address = InetAddress.getLocalHost();    
    String ip=address .getHostAddress().toString();    
    xingFuPay.setClientIp(ip);
    xingFuPay.setExtra("pay");
    xingFuPay.setSummary("CZ");
    xingFuPay.setExpireTime("1000");
    if(payType.equals("BANK_TRANSFER")){
    	xingFuPay.setBankId(payId);
    }else if(payType.equals("QUICK_PAY")){
    	xingFuPay.setBankId("KJZF");
    }
	//生成md5密钥签名
    String Md5key = chargePay.getSign();///////////md5密钥（KEY）
	String MARK = "&";
    String strStart="service="+xingFuPay.getService()+MARK+"version="+xingFuPay.getVersion()+MARK+"merId="+xingFuPay.getMerId()+MARK;
    String strEnd="tradeNo="+xingFuPay.getTradeNo()+MARK+
	"tradeDate="+xingFuPay.getTradeDate()+MARK+"amount="+xingFuPay.getAmount()+MARK+"notifyUrl="+xingFuPay.getNotifyUrl()+MARK+
	"extra="+xingFuPay.getExtra()+MARK+"summary="+xingFuPay.getSummary()+MARK+"expireTime="+xingFuPay.getExpireTime()+MARK+
	"clientIp="+xingFuPay.getClientIp();
    String  strMd5="";
     if(payType.equals("BANK_TRANSFER") || payType.equals("QUICK_PAY")){
    	 strMd5=strStart+strEnd+MARK+"bankId="+xingFuPay.getBankId();
     }else{
    	 strMd5=strStart+"typeId="+xingFuPay.getTypeId()+MARK+strEnd;
     }
     System.out.println("strMD5:"+strMd5);
     String md5 =new String(strMd5+chargePay.getSign());//MD5签名格式 
	
	String Signature = md5Util.getMD5ofStr(md5).toUpperCase();//计算MD5值
    //交易签名
    xingFuPay.setSign(Signature);
    log.info("md5原始串: "+md5 + "生成后的交易签名:" + Signature);
    System.out.println(md5);
    System.out.println(Signature);
    //冲值订单
	RechargeOrder rechargeOrder = new RechargeOrder();
	//用户ID
	rechargeOrder.setUserId(currentUser.getId());
	//快捷ID
	rechargeOrder.setChargePayId(chargePay.getId());
	//用户名称
	rechargeOrder.setUserName(currentUser.getUserName());
	//申请金额
	rechargeOrder.setApplyValue(new BigDecimal(orderMoneyYuan));
	//订单创建时间
	rechargeOrder.setCreatedDate(new Date());
	//充值类型
	rechargeOrder.setPayType(payType);
	//第三方充值类型
    rechargeOrder.setThirdPayType("XINGFU");
	//操作描述
	rechargeOrder.setOperateDes(EFundPayType.valueOf(payType).getDescription());
	//流水号
	rechargeOrder.setSerialNumber(orderId);
	rechargeOrder.setPostscript("");
	rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT.name());
	rechargeOrder.setBankName("");
	rechargeOrder.setSubbranchName("");
	rechargeOrder.setBankAccountName("");
	rechargeOrder.setBankCardNum("");
	rechargeOrder.setBankType("");
	rechargeOrder.setRefType(EFundRefType.THIRDPAY.name());
	rechargeOrder.setBizSystem(currentUser.getBizSystem());
	rechargeOrder.setFromType(currentUser.getLoginType());
	rechargeWithDrawOrderService.insertSelective(rechargeOrder);
	log.info("实际星付支付网关地址["+Address +"],交易报文: "+xingFuPay);
	
	String codeUrl="";
	if(payType.equals("ALIPAY") || payType.equals("WEIXIN") || payType.equals("QQPAY") || payType.equals("UNIONPAY")|| payType.equals("JDPAY")){
		 
    String strPara="service="+URLEncoder.encode(xingFuPay.getService(), "utf-8") + "&version="
			+ URLEncoder.encode(xingFuPay.getVersion(), "utf-8") + "&merId="
			+ URLEncoder.encode(xingFuPay.getMerId(), "utf-8") + "&typeId="
			+ URLEncoder.encode(xingFuPay.getTypeId(), "utf-8") + "&tradeNo="
			+ URLEncoder.encode(xingFuPay.getTradeNo(), "utf-8") + "&tradeDate="
			+ URLEncoder.encode(xingFuPay.getTradeDate(), "utf-8") + "&amount="
			+ URLEncoder.encode(xingFuPay.getAmount(), "utf-8") + "&notifyUrl="
			+ URLEncoder.encode(xingFuPay.getNotifyUrl(), "utf-8") + "&extra="
			+ URLEncoder.encode(xingFuPay.getExtra(), "utf-8")+"&summary="
		    + URLEncoder.encode(xingFuPay.getSummary(), "utf-8")+"&expireTime="
			+ URLEncoder.encode(xingFuPay.getExpireTime(), "utf-8")+"&clientIp="
			+ URLEncoder.encode(xingFuPay.getClientIp(), "utf-8")+"&sign="
			+ URLEncoder.encode(xingFuPay.getSign(), "utf-8");
	/* if(payType.equals("ALIPAYWAP")){
		Address="http://wx.eposbank.com/papay";
	} */
	URL url = new URL(Address+"?"+strPara);
    
	//openConnection函数会根据URL的协议返回不同的URLConnection子类的对象
	//这里URL是一个http,因此实际返回的是HttpURLConnection 
	HttpURLConnection httpConn = (HttpURLConnection) url
			.openConnection();
	/* httpConn.setRequestMethod("POST"); */
	//进行连接,实际上request要在下一句的connection.getInputStream()函数中才会真正发到 服务器****待验证
	httpConn.setDoOutput(true);
	httpConn.setDoInput(true);
	httpConn.connect();
	/* PrintWriter outWrite = new PrintWriter(httpConn.getOutputStream());
	
	outWrite.print(strPara);
    // flush输出流的缓冲
    outWrite.flush(); */
	// 取得输入流，并使用Reader读取
	BufferedReader reader = new BufferedReader(new InputStreamReader(
			httpConn.getInputStream(),"utf-8"));

	String lines = "";
	String str="";
	while ((str=reader.readLine()) != null) {
		lines += str; 
	}
	reader.close();
	log.info("星付支付返回报文:" + lines);
	
	 String desc="";
	XingFuData returnParameter = JaxbUtil.transformToObject(XingFuData.class,lines);
	XingFuDetailData xingFuDetailData=returnParameter.getDetail();
	if (xingFuDetailData.getCode().equals("00")) {
		codeUrl=new String((new BASE64Decoder()).decodeBuffer(xingFuDetailData.getQrCode()));
		/* response.sendRedirect(skipUrl);
		return; */
		
	} else {
		out.print(xingFuDetailData.getDesc());
		return;
	}
	
	}
	request.setAttribute("payType", payType);
	request.setAttribute("codeUrl", codeUrl);
%>
 
 <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta id="viewport" name="viewport"
	content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
<title>充值接口-提交信息处理</title>
<style>
body {
	text-align: center;
}

#weixinImg {
	margin-top: 150px;
	width: 280px;
}

p {
	font-size: 18px;
}
</style>
</head>
<body onload="pay.submit()">
<c:if test="${codeUrl!=''}">
<form method="post" name="pay" id="pay" action="<%=payUrl%>">
<TABLE>
<TR>
	<TD>
	<input name='payType' type='hidden' value= "<%=payType%>"/>
	<input name='codeUrl' type='hidden' value= "<%=codeUrl%>"/>
	
	
	</TD>
</TR>
</TABLE>
	
</form>	
</c:if>
<c:if test="${codeUrl==''}">
<form method="post" name="pay" id="pay" action="<%=Address%>">
<TABLE>
<TR>
	<TD>
	<input name='service' type='hidden' value= "<%=xingFuPay.service%>"/>
	<input name='version' type='hidden' value= "<%=xingFuPay.version%>"/>
	<input name='merId' type='hidden' value= "<%=xingFuPay.merId%>"/>
	<input name='typeId' type='hidden' value= "<%=xingFuPay.typeId%>"/>
	<input name='tradeNo' type='hidden' value= "<%=xingFuPay.tradeNo%>"/>
	<input name='tradeDate' type='hidden' value= "<%=xingFuPay.tradeDate%>"/>
	<input name='amount' type='hidden' value= "<%=xingFuPay.amount%>"/>
	<input name='notifyUrl' type='hidden' value= "<%=xingFuPay.notifyUrl%>"/>
	<input name='extra' type='hidden' value= "<%=xingFuPay.extra%>"/>
	<input name='summary' type='hidden' value= "<%=xingFuPay.summary%>"/>
	<input name='expireTime' type='hidden' value= "<%=xingFuPay.expireTime%>"/>
	<input name='clientIp' type='hidden' value= "<%=xingFuPay.clientIp%>"/>
	<input name='sign' type='hidden' value= "<%=xingFuPay.sign%>"/>
	
	
	</TD>
</TR>
</TABLE>
	
</form>	
</c:if>
</body>
<%-- <body>
	<img id="weixinImg" src='<%=codeUrl%>'></img>
	<img src="http://qr.topscan.com/api.php?bg=f3f3f3&fg=ff0000&gc=222222&el=l&w=684&m=30&text=<%=code%>"/>
	<p>
		请使用<span id="pay-way"><c:if test="${payType == 'ALIPAY' }">支付宝</c:if>
			<c:if test="${payType == 'WEIXIN' }">微信</c:if>
			<c:if test="${payType == 'QQPAY' }">QQ钱包</c:if>
			<c:if test="${payType == 'JDPAY' }">京东扫码</c:if>
			<c:if test="${payType == 'UNIONPAY' }">银联扫码</c:if></span>扫描二维码以完成支付
	</p>

</body> --%>
</html>

