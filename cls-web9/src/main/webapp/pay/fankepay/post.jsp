<%@page import="com.team.lottery.pay.model.fankepay.FanKePay"%>
<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.team.lottery.service.QuickBankTypeService"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.team.lottery.vo.QuickBankType"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="java.net.URLEncoder"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.team.lottery.service.RechargeConfigService"%>
<%@page import="com.team.lottery.vo.RechargeConfig"%>
<%
	//打印日志
	Logger log = LoggerFactory.getLogger(this.getClass());
	//获取当前web路径
	String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	String orderMoneyYuan = OrderMoney;
	String serialNumber = request.getParameter("serialNumber");
	//判断当前请求页面用户是否登录,没有的话就跳往登录页面
	User currentUser = (User) request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	if (currentUser == null) {
		response.sendRedirect(path + "/gameBet/login.html");
	}
	if (orderMoneyYuan.indexOf(".") > 0) {
		orderMoneyYuan = orderMoneyYuan.replaceAll("0+?$", "");//去掉多余的0  
		orderMoneyYuan = orderMoneyYuan.replaceAll("[.]$", "");//如最后一位是.则去掉  
	}
	//获取订单提交时间
	Date currTime = new Date();
	QuickBankTypeService quickBankTypeService = (QuickBankTypeService) ApplicationContextUtil.getBean("quickBankTypeService");
	ChargePayService chargePayService = (ChargePayService) ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService) ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.getPayMemmberMsg(Long.valueOf(chargePayId));
	
	//打印日志
	log.info("业务系统[{}],用户名[{}],发起了凡客支付，生成订单号[{}],支付金额[{}],PayId[{}],payType[{}],chargePayId[{}]", currentUser.getBizSystem(), currentUser.getUserName(), serialNumber,
			OrderMoney, payId, payType, chargePayId);
	QuickBankType query = new QuickBankType();
	query.setPayId(payId);
	String bankType = "";
	List<QuickBankType> list = quickBankTypeService.getQuickBankTypeQuery(query);
	if (list.size() > 0) {
		bankType = list.get(0).getBankType();
	}

	//生成需要跳转的支付地址;
	String payUrl = chargePay.getPayUrl();//转发支付网关地址
	String Address = chargePay.getAddress(); //最终地址

	log.info("凡客支付报文转发地址payUrl: " + payUrl + ",实际网关地址address: " + Address);
	//同步跳转地址
	String notifyUrl = "";
	if (chargePay.getNotifyUrl().startsWith("http://") || chargePay.getNotifyUrl().startsWith("https://")) {
		notifyUrl = chargePay.getNotifyUrl();
	} else {
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
		notifyUrl = basePath + chargePay.getNotifyUrl();
	}

	//凡客发起支付对象.
	FanKePay fanKePay = new FanKePay();
	//备注.
	fanKePay.setAttach(currentUser.getUserName());
	//设置商户号.
	fanKePay.setMerchant_no(chargePay.getMemberId());
	//设置订单号.
	fanKePay.setMerchant_order_no(serialNumber);
	//设置通知地址.
	fanKePay.setNotify_url(chargePay.getReturnUrl());
	//设置支付方式.
	fanKePay.setPay_type(payId);
	//设置支付成功跳转地址.
	fanKePay.setReturn_url(notifyUrl);
	//设置支付金额.
	fanKePay.setTrade_amount(orderMoneyYuan);

	//sign加密;
	String mark = "&";
	//加密原始串.
	String Md5Sign = "attach=" + fanKePay.getAttach() + mark + 
					 "merchant_no=" + fanKePay.getMerchant_no() + mark + 
					 "merchant_order_no=" + fanKePay.getMerchant_order_no() + mark + 
					 "notify_url=" + fanKePay.getNotify_url() + mark + 
					 "pay_type=" + fanKePay.getPay_type() + mark + 
					 "return_url=" + fanKePay.getReturn_url() + mark + 
					 "trade_amount=" + fanKePay.getTrade_amount() + mark + 
					 "key=" + chargePay.getSign();

	//进行MD5小写加密
	String Signature = Md5Util.getMD5ofStr(Md5Sign);
	//打印日志
	log.info("md5原始串: " + Md5Sign + "生成后的交易签名:" + Signature);
	//设置签名
	fanKePay.setSign(Signature);

	//通过JSON格式去提交数据,以便获取第三方跳转地址
	Map<String, String> postDataMap = new TreeMap<String, String>();
	postDataMap.put("attach", fanKePay.getAttach());
	postDataMap.put("merchant_no", fanKePay.getMerchant_no());
	postDataMap.put("merchant_order_no", fanKePay.getMerchant_order_no());
	postDataMap.put("notify_url", fanKePay.getNotify_url());
	postDataMap.put("pay_type", fanKePay.getPay_type());
	postDataMap.put("return_url", fanKePay.getReturn_url());
	postDataMap.put("trade_amount", fanKePay.getTrade_amount());
	postDataMap.put("sign", fanKePay.getSign());
	//获取第三方给我们返回的JSON数据做判断跳转
	String result = HttpClientUtil.doPost(Address, postDataMap);
	log.info("凡客支付提交支付报文，返回内容:{}", result);
	Boolean isJson = JSONUtils.isJson(result);
	//最终我们要生成二维码的数据
	String payInfoNew = "";
	String basePath = "";
	if (isJson == true ) {
		JSONObject jsonObject =  JSONUtils.toJSONObject(result);
		if (StringUtils.isBlank(result)) {
			out.print("发起支付失败");
			return;
		}
		//获取返回的状态.1为发起成功,-1为发起失败.
		String status = jsonObject.getString("status");
		if (status.equals("1")) {
			//将json字符串取值用于取到最后的支付地址.
			String data = jsonObject.getString("data");
			JSONObject jsonObjectData = JSONUtils.toJSONObject(data);
			//获取最终的支付地址.
			String truePayUrl = jsonObjectData.getString("pay_url");
			payInfoNew = truePayUrl.replace("\\","");
			basePath =path+"/"+"tools/getQrCodePic?code="+URLEncoder.encode(payInfoNew, "utf-8");
			request.setAttribute("payType", payType);
		}else {
			out.print(result);
			return;
		}
		log.info("交易报文: " + fanKePay); 
	} else {
		out.print(result);
		log.error("不是json格式的数据: " + result); 
		return;
	}
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
<style>
body {
	text-align: center;
}

#weixinImg {
	margin-top: 150px;
	width: 280px;
}

p {
	font-size: 18px;
}
</style>
</head>

<c:if test="${basePath!=''}">
	<body>
		<img id="weixinImg" src='<%=basePath%>'></img>
		<%-- <img src="http://qr.topscan.com/api.php?bg=f3f3f3&fg=ff0000&gc=222222&el=l&w=684&m=30&text=<%=code%>"/> --%>
		<p>
			请使用<span id="pay-way"><c:if test="${payType == 'ALIPAY' }">支付宝</c:if>
				<c:if test="${payType == 'WEIXIN' }">微信</c:if> <c:if
					test="${payType == 'QQPAY' }">QQ钱包</c:if> <c:if
					test="${payType == 'JDPAY' }">京东扫码</c:if> <c:if
					test="${payType == 'UNIONPAY' }">银联扫码</c:if></span>扫描二维码以完成支付
		</p>
	</body>
</c:if>
</html>
