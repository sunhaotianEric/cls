<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.vo.ChargePay,com.team.lottery.service.ChargePayService"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>

<%
	Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
    request.setCharacterEncoding("UTF-8");
	String interface_version = (String) request.getParameter("interface_version");
	String merchant_code = (String) request.getParameter("merchant_code");
	String notify_type = (String) request.getParameter("notify_type");
	String notify_id = (String) request.getParameter("notify_id");
	String sign_type = (String) request.getParameter("sign_type");
	String dinpaySign = (String) request.getParameter("sign");
	String order_no = (String) request.getParameter("order_no");
	String order_time = (String) request.getParameter("order_time");
	String order_amount = (String) request.getParameter("order_amount");
	String extra_return_param = (String) request.getParameter("extra_return_param");
	String trade_no = (String) request.getParameter("trade_no");
	String trade_time = (String) request.getParameter("trade_time");
	String trade_status = (String) request.getParameter("trade_status");
	String bank_seq_no = (String) request.getParameter("bank_seq_no");
	log.info("康付通支付成功通知平台处理信息如下: 商户号: "+merchant_code+" 订单号: "+order_no+" 支付结果: "+trade_status+" 实际成功金额: "+
			order_amount+" 支付完成时间: "+order_time+" 第三方返回MD5签名: "+dinpaySign);
	
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(order_no);
	if(rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1){
		log.error("根据订单号[" + order_no + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知康付通接收报文成功	
	    out.println("SUCCESS");
		return;
	}
	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
	if(rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + order_no + "]查找充值订单记录为空");
		//通知康付通接收报文成功
	    out.println("SUCCESS");
		return;
	}

	//签名
	
	Long chargePayId = rechargeWithDrawOrder.getChargePayId();
	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	ChargePay chargePay = chargePayService.getPayMemmberMsg(Long.valueOf(chargePayId));
	log.info("publicKey:"+chargePay.getPublickey());
	String MARK = "&";
	String md5="";
	if(null != bank_seq_no && !bank_seq_no.equals("")) {
		md5="bank_seq_no="+bank_seq_no+MARK;
 	}
	    md5 = md5+"interface_version=" + interface_version + MARK + "merchant_code=" + merchant_code + MARK + "notify_id=" + notify_id + MARK + "notify_type=" + notify_type + MARK + "order_amount=" + order_amount + MARK
			+ "order_no=" + order_no + MARK + "order_time="+order_time+MARK + "trade_no="+trade_no+MARK + "trade_status="+trade_status+MARK + "trade_time="+trade_time;
    String publicKey=chargePay.getPublickey();
	//Md5加密串
	/* String WaitSign = RSAUtils.publicKeyDecrypt(publicKey, sign);
	boolean signStat = RSAWithSoftware.validateSignByPublicKey(md5, publicKey, dinpaySign);
	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if(signStat){
		boolean dealResult = false;
		if(!StringUtils.isEmpty(trade_status) && trade_status.equals("1")){
			//
			BigDecimal factMoneyValue = new BigDecimal(order_amount);
			try {
				dealResult = rechargeWithDrawOrderService.rechargeOrderSuccessDeal(order_no,factMoneyValue);
			}catch(UnEqualVersionException e) {
		   		log.error("康付通在线充值时发现版本号不一致,订单号["+order_no+"]");
		   		//启动新进程进行多次重试
		   		UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION
		   				, order_no,factMoneyValue,"");
		   		thread.start();
			} catch(Exception e){
				//异常不进行重试
				log.error(e.getMessage());
			}
		} else {
			if(!trade_status.equals("1")){
				log.info("康付通支付处理错误支付结果："+trade_status);
			}
		}
		log.info("康付通订单处理入账结果:{}", dealResult);
		//通知康付通接收报文成功
	    out.println("SUCCESS");
		
	}else{
		log.info("MD5校验失败,报文返回MD5签名内容["+ dinpaySign +"],计算MD5签名内容[" + md5 + "]");
		//不输出OK，让第三方继续补发
		out.println("ERROR");
	} */
%>