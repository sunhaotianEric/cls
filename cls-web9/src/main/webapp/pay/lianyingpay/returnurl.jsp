<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@page import="java.io.*"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="net.sf.json.JSONObject"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.extvo.returnModel.LianYingReturnParam"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
    //获取当前的状态
	BufferedReader streamReader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
	StringBuilder responseStrBuilder = new StringBuilder();
	String inputStr;
	while ((inputStr = streamReader.readLine()) != null) {
		responseStrBuilder.append(inputStr);
	}
	log.info("联赢支付付回调通知报文: " + responseStrBuilder.toString());
	JSONObject jsonObject = JSONUtils.toJSONObject(responseStrBuilder.toString());
	LianYingReturnParam lianYingPayReturnParam = JSONUtils.toBean(jsonObject, LianYingReturnParam.class);
	//获取参数
	//获取支付金额,以分为单位;
	String amount = lianYingPayReturnParam.getAmount();
	String merchantId = lianYingPayReturnParam.getMerchantId();
	String orderId = lianYingPayReturnParam.getOrderId();
	String poId = lianYingPayReturnParam.getPoId();
	String signType = lianYingPayReturnParam.getSignType();
	String status = lianYingPayReturnParam.getStatus();
	String time = lianYingPayReturnParam.getTime();
	
	String sign = lianYingPayReturnParam.getSign();
	

	log.info("联赢支付成功通知平台处理信息如下: 商户ID:" + merchantId + " 订单号:" + poId + "订单金额:" + new BigDecimal(amount).divide(new BigDecimal(100)) + " 元 "+"签名:" + sign);

	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService) ApplicationContextUtil.getBean("rechargeOrderService");
	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(poId);
	if (rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1) {
		log.error("根据订单号[" + poId + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知联赢付支付报文接收成功
		response.getOutputStream().write("SUCCESS".getBytes("UTF-8"));
	}
	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
	if (rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + poId + "]查找充值订单记录为空");
		//通知;联赢支付报文接收成功
		response.getOutputStream().write("SUCCESS".getBytes("UTF-8"));
	}
	//计算签名
	String Md5key = rechargeWithDrawOrder.getSign();
	String MARK = "&";
	//需要加密的字符串
	String MD5Str = null;
	if(signType == null || signType.equals("null")){
		
		MD5Str = "amount="+amount+MARK+
				 "merchantId="+merchantId+MARK+
				 "orderId="+orderId+MARK+
				 "poId="+poId+MARK+
				 "status="+status+MARK+
				 "time="+time+MARK+
				 "KEY=" + rechargeWithDrawOrder.getSign();
	}else{
		
		MD5Str = "amount="+amount+MARK+
				 "merchantId="+merchantId+MARK+
				 "orderId="+orderId+MARK+
				 "poId="+poId+MARK+
				 "signType="+signType+MARK+
				 "status="+status+MARK+
				 "time="+time+ MARK+
				 "KEY=" + rechargeWithDrawOrder.getSign();
	}
	log.info("当前MD5源码:" + MD5Str);
	//Md5加密串并且大写
	String WaitSign = Md5Util.getMD5ofStr(MD5Str).toUpperCase();
	log.info("加密后的MD5: " + WaitSign);
	//将金额转换为元
	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if (WaitSign.compareTo(sign) == 0) {
		boolean dealResult = false;
		//获取当前订单的订单价格
		String money = rechargeWithDrawOrder.getApplyValue().toString();
		//需要入账的资产;
		BigDecimal factMoneyValue = new BigDecimal(amount).divide(new BigDecimal(100));
		if (!StringUtils.isEmpty(status) && status.equals("S")) {
			try {
				dealResult = rechargeWithDrawOrderService.rechargeOrderSuccessDeal(poId, factMoneyValue);
			} catch (UnEqualVersionException e) {
				log.error("联赢支付充值时发现版本号不一致,订单号[" + poId + "]");
				//启动新进程进行多次重试
				UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, poId,
						factMoneyValue, "");
				thread.start();
			} catch (Exception e) {
				//异常不进行重试
				log.error(e.getMessage());
			}
		} else {
			if (!status.equals("S")) {
				log.info("联赢支付处理错误支付结果：" +  status.equals("S"));
			}
		}
		log.info("联赢支付订单处理入账结果:{}", dealResult);
		//通知联赢付报文接收成功
		response.getOutputStream().write("SUCCESS".getBytes("UTF-8"));
		return;
	} else {
		log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容[" + WaitSign + "]");
		response.getOutputStream().write("SUCCESS".getBytes("UTF-8"));
		return;
	}
%>