<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.pay.model.SixEightSixPay"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@page import="com.team.lottery.pay.util.Md5Util"%>
 <%@page errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.InetAddress"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="com.team.lottery.extvo.returnModel.SixEightSixReturnPayUrl"%>
<%@page import="com.team.lottery.extvo.returnModel.JianZhengBaoData"%>
<%@page import="java.util.*"%>
<%@page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%  

    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
    String orderMoneyYuan = OrderMoney;
 
    
    User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
    	response.sendRedirect(path+"/gameBet/login.html");
    }
	
	/* Date currTime = new Date();
    //时间以yyyy-MM-dd HH:mm:ss的方式表示
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.CHINA);
    //时间以yyyyMMDDHHmmss的方式表示 
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss",Locale.CHINA);
    String webdate=new String(formatter1.format(currTime));
    String TradeDate=new String(formatter2.format(currTime)); */

	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
    RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));
	String  orderId=MakeOrderNum.makeOrderNum("CZ");
	log.info("用户名称: "+currentUser.getUserName()+" 用户 ID号: "+currentUser.getId()+" 生成订单号: "+orderId+" 订单金额: "+OrderMoney);
	String[] moneyAllow={"10","20","30","40","50","60","80","90","100","120","140","150","160","180","200","220","240","250","260","270","280","300","500"};
	int i=0;
	for(String str:moneyAllow){
		if(str.equals(orderMoneyYuan)){
			i++;
			break;
		}
	}
	if(i==0){
		out.print("目前只支持以下金额：10,20,30,40,50,60,80,90,100,120,140,150,160,180,200,220,240,250,260,270,280,300,500");
		return;
	}
	if(!"".equals(OrderMoney)) {	
		 BigDecimal a;
		a = new BigDecimal(OrderMoney); //使用分进行提交
		OrderMoney=String.valueOf(a.setScale(2, BigDecimal.ROUND_HALF_UP));
	}
	else{
		OrderMoney = ("0");
	} 
	String payUrl = chargePay.getPayUrl();//转发支付网关地址
	String Address = chargePay.getAddress(); //最终686地址
	log.info("payUrl: "+payUrl+" Address: "+Address);
	
	//686支付请求对象
	SixEightSixPay sixEightSixPay =new SixEightSixPay();
	sixEightSixPay.setCode(chargePay.getMemberId());
	sixEightSixPay.setNotifyUrl(chargePay.getReturnUrl());
	sixEightSixPay.setGoodsClauses("CZ");
	sixEightSixPay.setOutOrderNo(orderId);
	sixEightSixPay.setPayCode(payId);
	sixEightSixPay.setTradeAmount(OrderMoney);
	
    
   
  //生成md5密钥签名
    String Md5key = chargePay.getSign();///////////md5密钥（KEY）
	String MARK = "&";
	/* partner={}&banktype={}&paymoney={}&ordernumber={}&callbackurl={}key */
	String md5 =new String("code="+sixEightSixPay.getCode()+MARK+"goodsClauses="+sixEightSixPay.getGoodsClauses()+MARK+"notifyUrl="+sixEightSixPay.getNotifyUrl()+MARK+"outOrderNo="+sixEightSixPay.getOutOrderNo()+MARK+"payCode="+sixEightSixPay.getPayCode()
	+MARK+"tradeAmount="+sixEightSixPay.getTradeAmount()+MARK+"key="+Md5key);//MD5签名格式
	String Signature = Md5Util.getMD5ofStr(md5);//计算MD5值
    //交易签名
    sixEightSixPay.setSign(Signature);
    log.info("md5原始串: "+md5 + "生成后的交易签名:" + Signature);
    //冲值订单
	RechargeOrder rechargeOrder = new RechargeOrder();
	//用户ID
	rechargeOrder.setUserId(currentUser.getId());
	//快捷ID
	rechargeOrder.setChargePayId(chargePay.getId());
	//用户名称
	rechargeOrder.setUserName(currentUser.getUserName());
	//申请金额
	rechargeOrder.setApplyValue(new BigDecimal(orderMoneyYuan));
	//订单创建时间
	rechargeOrder.setCreatedDate(new Date());
	//充值类型
	rechargeOrder.setPayType(payType);
	//第三方充值类型
    rechargeOrder.setThirdPayType("SIXEIGHTSIXUPAY");
	//操作描述
	rechargeOrder.setOperateDes(EFundPayType.valueOf(payType).getDescription());
	//流水号
	rechargeOrder.setSerialNumber(orderId);
	rechargeOrder.setPostscript("");
	rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT.name());
	rechargeOrder.setBankName("");
	rechargeOrder.setSubbranchName("");
	rechargeOrder.setBankAccountName("");
	rechargeOrder.setBankCardNum("");
	rechargeOrder.setBankType("");
	rechargeOrder.setRefType(EFundRefType.THIRDPAY.name());
	rechargeOrder.setBizSystem(currentUser.getBizSystem());
	rechargeOrder.setFromType(currentUser.getLoginType());
	rechargeWithDrawOrderService.insertSelective(rechargeOrder);
	log.info("实际686支付网关地址["+Address +"],交易报文: "+sixEightSixPay);
	Map<String, String> params=new HashMap<String, String>();
	params.put("code", sixEightSixPay.getCode());
	params.put("goodsClauses", sixEightSixPay.getGoodsClauses());
	params.put("notifyUrl",sixEightSixPay.getNotifyUrl());
	params.put("outOrderNo", sixEightSixPay.getOutOrderNo());
	params.put("payCode",sixEightSixPay.getPayCode());
	params.put("tradeAmount", sixEightSixPay.getTradeAmount());
	params.put("sign", sixEightSixPay.getSign());
	String lines= HttpClientUtil.doPost(Address,params);
	log.info("686支付返回报文:" + lines);
	 String codeUrl="";
	SixEightSixReturnPayUrl returnParameter = JSONUtils.toBean(lines,
			SixEightSixReturnPayUrl.class);
	if(returnParameter.getPayState()!=null){
		if (returnParameter.getPayState().equals("success")) {
			codeUrl=URLEncoder.encode(returnParameter.getUrl(), "utf-8");
			
		} else {
			out.print(returnParameter.getMessage());
			return;
		}
	}
	
	if(returnParameter.getResultStatus()!=null){
		if(returnParameter.getResultStatus().equals("FAIL")){
			out.print(returnParameter.getReMsg());
			return;
		}
	}
	request.setAttribute("payType", payType);
	
 %>
 
 <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta id="viewport" name="viewport"
	content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
<title>充值接口-提交信息处理</title>
<style>
body {
	text-align: center;
}

img {
	margin-top: 60px;
}

p {
	font-size: 36px;
}
</style>
</head>

<body>
	<img id="weixinImg" src='<%=path%>/tools/getQrCodePic?code=<%=codeUrl%>'></img>
	<%-- <img src="http://qr.topscan.com/api.php?bg=f3f3f3&fg=ff0000&gc=222222&el=l&w=684&m=30&text=<%=code%>"/> --%>
	<p>
		请使用<span id="pay-way">
		    <c:if test="${payType == 'ALIPAY' }">支付宝</c:if>
			<c:if test="${payType == 'WEIXIN' }">微信</c:if>
			<%-- <c:if test="${payType == 'JDQB' }">京东钱包</c:if> --%>
			<c:if test="${payType == 'QQPAY' }">QQ钱包</c:if>
			<c:if test="${payType == 'UNIONPAY' }">银联二维码</c:if>
			</span>扫描二维码以完成支付
	</p>

</body>
</html>
 