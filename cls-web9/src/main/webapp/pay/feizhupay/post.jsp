<%@page import="com.team.lottery.pay.model.feizhupay.FeiZhuPay"%>
<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.team.lottery.service.QuickBankTypeService"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.util.IPUtil"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.team.lottery.vo.QuickBankType"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="java.net.URLEncoder"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.team.lottery.service.RechargeConfigService"%>
<%@page import="com.team.lottery.vo.RechargeConfig"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
	String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	String serialNumber = request.getParameter("serialNumber"); // 获取配置ID;
	String orderMoneyYuan = OrderMoney;
	// 如果为微信支付,对支付金额有要求(500元以下为整数.500以上整百要求.)
	if(payId.equals("910")){
		try{
			Integer wechatPayMoney = Integer.valueOf(OrderMoney);
			// 如果金额大于500必须为整百金额.
			if(wechatPayMoney > 499){
				if(wechatPayMoney % 100 != 0){
					out.print("充值金额大于等于500时需要输入整百金额,才能充值(类似500,600,700...)");
					return;
				}
			}
		}catch(NumberFormatException e){
			out.print("请输入整数金额");
			return;
		}
	}
	User currentUser = (User) request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	if (currentUser == null) {
		response.sendRedirect(path + "/gameBet/login.html");
	}
	Date currTime = new Date();
	//时间以yyyy-MM-dd HH:mm:ss的方式表示
	SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);
	//时间以yyyyMMDDHHmmss的方式表示 
	SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss", Locale.CHINA);
	String webdate = new String(formatter1.format(currTime));
	String TradeDate = new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService) ApplicationContextUtil.getBean("chargePayService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));

	log.info("业务系统[{}],用户名[{}],发起了飞猪支付，生成订单号[{}],支付金额[{}]分,PayId[{}],payType[{}],chargePayId[{}]",
	currentUser.getBizSystem(), currentUser.getUserName(), serialNumber, OrderMoney, payId, payType,
	chargePayId);
	String payUrl = chargePay.getPayUrl();
	String Address = chargePay.getAddress();
	log.info("payUrl: " + payUrl + " Address: " + Address);

	//同步跳转地址
	String notifyUrl = "";
	if (chargePay.getNotifyUrl().startsWith("http://") || chargePay.getNotifyUrl().startsWith("https://")) {
		notifyUrl = chargePay.getNotifyUrl();
	} else {
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
		+ path + "/";
		notifyUrl = basePath + chargePay.getNotifyUrl();
	}
	// 新建飞猪支付发起对象.
	FeiZhuPay feiZhuPay = new FeiZhuPay();
	// 支付金额.
	feiZhuPay.setPay_amount(OrderMoney);
	// 支付时间(yyyy-MM-dd HH:mm:ss).
	feiZhuPay.setPay_applydate(webdate);
	// 支付码表.
	feiZhuPay.setPay_bankcode(payId);
	// 同步跳转地址.
	feiZhuPay.setPay_callbackurl(notifyUrl);
	// 商户号.
	feiZhuPay.setPay_memberid(chargePay.getMemberId());
	// 回调地址.
	feiZhuPay.setPay_notifyurl(chargePay.getReturnUrl());
	// 平台上送唯一订单号.
	feiZhuPay.setPay_orderid(serialNumber);
	
	//sign加密;
	String mark = "&";
	String md5SignStr = "pay_amount=" + feiZhuPay.getPay_amount() + mark +
						"pay_applydate=" + feiZhuPay.getPay_applydate() + mark +
						"pay_bankcode=" + feiZhuPay.getPay_bankcode() + mark +
						"pay_callbackurl=" + feiZhuPay.getPay_callbackurl() + mark +
						"pay_memberid=" + feiZhuPay.getPay_memberid() + mark +
						"pay_notifyurl=" + feiZhuPay.getPay_notifyurl() + mark + 
						"pay_orderid=" + feiZhuPay.getPay_orderid() + mark + 
						"key=" + chargePay.getSign();

	//进行MD5大写加密
	String md5Sign = Md5Util.getMD5ofStr(md5SignStr).toUpperCase();
	//打印日志
	log.info("md5原始串: " + md5SignStr + "生成后的交易签名:" + md5Sign);
	//设置签名
	feiZhuPay.setPay_md5sign(md5Sign);
	log.info(feiZhuPay.toString());

	//通过JSON格式去提交数据,以便获取第三方跳转地址
	Map<String,String> dataMap = new HashMap<String,String>();
	dataMap.put("pay_amount", feiZhuPay.getPay_amount());
	dataMap.put("pay_applydate", feiZhuPay.getPay_applydate());
	dataMap.put("pay_bankcode", feiZhuPay.getPay_bankcode());
	dataMap.put("pay_callbackurl", feiZhuPay.getPay_callbackurl());
	dataMap.put("pay_memberid", feiZhuPay.getPay_memberid());
	dataMap.put("pay_notifyurl", feiZhuPay.getPay_notifyurl());
	dataMap.put("pay_orderid", feiZhuPay.getPay_orderid());
	dataMap.put("pay_md5sign", feiZhuPay.getPay_md5sign());
	// 该字段标识第三方返回JSON数据.
	dataMap.put("pay_returntype", "2");
	
	//获取第三方给我们返回的JSON数据做判断跳转
	String result = HttpClientUtil.doPost(Address , dataMap);
	log.info("飞猪支付提交支付报文，返回内容:{}", result);
	Boolean isJson = JSONUtils.isJson(result);
	//最终我们要生成二维码的数据
	String payInfoNew = "";
	String basePath = "";
	if (isJson == true) {
		JSONObject jsonObject = JSONUtils.toJSONObject(result);
		if (StringUtils.isBlank(result)) {
			out.print("发起支付失败");
			return;
		}
		//获取返回的状态.success为发起成功,其他为发起失败.
		String status = jsonObject.getString("status");
		if (status.equals("success")) {
			String data = jsonObject.getString("data");
			// 获取二维码支付路径.
			JSONObject jsonObjectOfData = JSONUtils.toJSONObject(data);
			String pay_url = jsonObjectOfData.getString("pay_url");
			log.info("飞猪支付获取到的支付二维码路径为: " + pay_url);
			response.sendRedirect(pay_url);
		} else {
			out.print(result);
			return;
		}
	} else {
		out.print(result);
		log.error("飞猪支付支付发起失败: " + result);
		return;
	}
%>