<%@page import="com.alibaba.fastjson.JSON"%>
<%@page import="com.team.lottery.pay.util.RSAUtils"%>
<%@page import="com.team.lottery.pay.model.xinfa.XinFaPay"%>
<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.team.lottery.service.QuickBankTypeService"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.util.IPUtil"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.team.lottery.vo.QuickBankType"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="java.net.URLEncoder"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.team.lottery.service.RechargeConfigService"%>
<%@page import="com.team.lottery.vo.RechargeConfig"%>
<%@page import="com.team.lottery.pay.util.RSA"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
	String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	String serialNumber = request.getParameter("serialNumber"); // 获取配置ID;
	String orderMoneyYuan = OrderMoney;
	if (payId.equals("WX_WAP")) {
		try {
			Integer orderMoney = Integer.valueOf(orderMoneyYuan);
			if (orderMoney != 10 || orderMoney != 20 || orderMoney != 30 || orderMoney != 50 || orderMoney != 100) {
				out.print("支付金额固定为10元,20元,30元,50元,100元,请重新发起支付");
				return;
			}
			// 判断
		} catch (NumberFormatException e) {
			out.print("支付金额固定为10元,20元,30元,50元,100元");
			return;
		}
	}

	if (!"".equals(OrderMoney)) {
		BigDecimal a;
		a = new BigDecimal(OrderMoney).multiply(BigDecimal.valueOf(100)); //使用分进行提交
		OrderMoney = String.valueOf(a.setScale(0));
	} else {
		OrderMoney = ("0");
	}
	User currentUser = (User) request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	if (currentUser == null) {
		response.sendRedirect(path + "/gameBet/login.html");
	}
	Date currTime = new Date();
	//时间以yyyy-MM-dd HH:mm:ss的方式表示
	SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);
	//时间以yyyyMMDDHHmmss的方式表示 
	SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss", Locale.CHINA);
	String webdate = new String(formatter1.format(currTime));
	String TradeDate = new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService) ApplicationContextUtil.getBean("chargePayService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));

	log.info("业务系统[{}],用户名[{}],发起了鑫发支付，生成订单号[{}],支付金额[{}]分,PayId[{}],payType[{}],chargePayId[{}]", currentUser.getBizSystem(),
			currentUser.getUserName(), serialNumber, OrderMoney, payId, payType, chargePayId);
	String payUrl = chargePay.getPayUrl();
	String Address = chargePay.getAddress();
	log.info("payUrl: " + payUrl + " Address: " + Address);

	//同步跳转地址
	String notifyUrl = "";
	if (chargePay.getNotifyUrl().startsWith("http://") || chargePay.getNotifyUrl().startsWith("https://")) {
		notifyUrl = chargePay.getNotifyUrl();
	} else {
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
		notifyUrl = basePath + chargePay.getNotifyUrl();
	}
	// 获取当前客户端IP
	// 获取当前用户的客户端IP.
	String userIp = IPUtil.getRequestIp(request);
	// 海图支付对象
	XinFaPay xinFaPay = new XinFaPay();
	xinFaPay.setAmount(OrderMoney);
	xinFaPay.setCharsetCode("UTF-8");
	xinFaPay.setGoodsName(currentUser.getUserName());
	xinFaPay.setMerchNo(chargePay.getMemberId());
	xinFaPay.setNotifyUrl(chargePay.getReturnUrl());
	xinFaPay.setNotifyViewUrl(chargePay.getNotifyUrl());
	xinFaPay.setOrderNo(serialNumber);
	xinFaPay.setPayType(payId);
	StringBuilder randomStr = new StringBuilder();//定义变长字符串
	Random random = new Random();
	//随机生成数字，并添加到字符串
	for (int i = 0; i < 8; i++) {
		randomStr.append(random.nextInt(10));
	}
	xinFaPay.setRandomNum(randomStr.toString());
	xinFaPay.setVersion("V3.3.0.0");

	//MD5加密;
	String beginStr = "{";
	String endStr = "}";
	String mark = ",";
	String key = chargePay.getSign();
	JSONObject jsonObjectJson = new JSONObject();
	jsonObjectJson.accumulate("amount", xinFaPay.getAmount());
	jsonObjectJson.accumulate("charsetCode", xinFaPay.getCharsetCode());
	jsonObjectJson.accumulate("goodsName", xinFaPay.getGoodsName());
	jsonObjectJson.accumulate("merchNo", xinFaPay.getMerchNo());
	jsonObjectJson.accumulate("notifyUrl", xinFaPay.getNotifyUrl());
	jsonObjectJson.accumulate("notifyViewUrl", xinFaPay.getNotifyViewUrl());
	jsonObjectJson.accumulate("orderNo", xinFaPay.getOrderNo());
	jsonObjectJson.accumulate("payType", xinFaPay.getPayType());
	jsonObjectJson.accumulate("randomNum", xinFaPay.getRandomNum());
	jsonObjectJson.accumulate("version", xinFaPay.getVersion());
	String md5SignStr = jsonObjectJson.toString() + key;
	//进行MD5大写加密
	String md5Sign = Md5Util.getMD5ofStr(md5SignStr).toUpperCase();
	//打印日志
	log.info("md5原始串: " + md5SignStr + "加密后生成的交易签名:" + md5Sign);

	//设置签名
	xinFaPay.setSign(md5Sign);
	log.info(xinFaPay.toString());
	chargePay.getPublickey();
	JSONObject jsonObjectDataJson = new JSONObject();
	jsonObjectDataJson.accumulate("amount", xinFaPay.getAmount());
	jsonObjectDataJson.accumulate("charsetCode", xinFaPay.getCharsetCode());
	jsonObjectDataJson.accumulate("goodsName", xinFaPay.getGoodsName());
	jsonObjectDataJson.accumulate("merchNo", xinFaPay.getMerchNo());
	jsonObjectDataJson.accumulate("notifyUrl", xinFaPay.getNotifyUrl());
	jsonObjectDataJson.accumulate("notifyViewUrl", xinFaPay.getNotifyViewUrl());
	jsonObjectDataJson.accumulate("orderNo", xinFaPay.getOrderNo());
	jsonObjectDataJson.accumulate("payType", xinFaPay.getPayType());
	jsonObjectDataJson.accumulate("randomNum", xinFaPay.getRandomNum());
	jsonObjectDataJson.accumulate("sign", xinFaPay.getSign());
	jsonObjectDataJson.accumulate("version", xinFaPay.getVersion());

	log.info("data加密前参数为:" + jsonObjectDataJson.toString());
	log.info("公钥为:" + chargePay.getPublickey());
	String publicKey = StringUtils.replaceBlank(chargePay.getPublickey());
	String rsaData = RSA.encryptByPublicKey(jsonObjectDataJson.toString(), publicKey);
	log.info("加密后得到的数据为:" + rsaData);
	String encodeStr = URLEncoder.encode(rsaData, "UTF-8");
	log.info("encode解码后得到的数据为:" + encodeStr);

	//通过JSON格式去提交数据,以便获取第三方跳转地址
	Map<String, String> jsonDataMap = new HashMap<String, String>();
	jsonDataMap.put("data", encodeStr);
	jsonDataMap.put("merchNo", xinFaPay.getMerchNo());
	log.info("发起请求的商户号为" + xinFaPay.getMerchNo());
	jsonDataMap.put("version", xinFaPay.getVersion());
	//获取第三方给我们返回的JSON数据做判断跳转
	String result = HttpClientUtil.doPostUrl(Address, jsonDataMap);
	log.info("鑫发支付提交支付报文，返回内容:{}", result);
	Boolean isJson = JSONUtils.isJson(result);
	String basePath = "";
	if (isJson == true) {
		JSONObject jsonObject = JSONUtils.toJSONObject(result);
		if (StringUtils.isEmpty(result)) {
			out.print("发起支付失败");
			return;
		}
		//获取返回的状态.00为发起成功,其他为发起失败.
		String code = jsonObject.getString("stateCode");
		if (code.equals("00")) {
			// 获取二维码支付路径.
			String qrcodeUrl = jsonObject.getString("qrcodeUrl");
			log.info("鑫发支付获取到的支付二维码路径为: " + qrcodeUrl);
			if ("WX_WAP".equals(payId) || "WX_H5".equals(payId) || "WX_H5_D1".equals(payId) || "ZFB_WAP".equals(payId)
					|| "ZFB_HB_H5".equals(payId) || "UNION_PAY_H5".equals(payId) || "QQ_WAP".equals(payId)) {
				response.sendRedirect(qrcodeUrl);
			} else {
				basePath =path+"/"+"tools/getQrCodePic?code="+URLEncoder.encode(qrcodeUrl, "utf-8");
				request.setAttribute("payType", payType);
			}

		} else {
			out.print(result);
			return;
		}
	} else {
		out.print(result);
		log.error("海图支付支付发起失败: " + result);
		return;
	}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
<style>
body {
	text-align: center;
}

#weixinImg {
	margin-top: 150px;
	width: 280px;
}

p {
	font-size: 18px;
}
</style>
</head>

<c:if test="${basePath!=''}">
	<body>
		<img id="weixinImg" src='<%=basePath%>'></img>
		<p>
			请使用<span id="pay-way">
		<% if(payType.equals("ALIPAY")){%>
			支付宝扫码
		<% }%>
		<% if(payType.equals("WEIXIN")){%>
			微信扫码
		<% }%>
		<% if(payType.equals("QQPAY")){%>
			QQ扫码
		<% }%>
		<% if(payType.equals("JDPAY")){%>
			京东扫码
		<% }%>
		<% if(payType.equals("UNIONPAY")){%>
			银联扫码
		<% }%>
		</span>扫描二维码以完成支付
	</body>
</c:if>
</html>