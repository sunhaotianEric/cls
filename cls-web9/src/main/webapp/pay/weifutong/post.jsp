<%@page import="com.team.lottery.pay.model.weifutong.WeiFuTong"%>
<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.team.lottery.service.QuickBankTypeService"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.team.lottery.vo.QuickBankType"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="java.net.URLEncoder"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@page import="com.team.lottery.vo.RechargeConfig"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.service.RechargeConfigService"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	//打印日志
	Logger log = LoggerFactory.getLogger(this.getClass());
	//获取当前web路径
	String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	String serialNumber = request.getParameter("serialNumber");//充值类型
	String orderMoneyYuan = OrderMoney;
	//判断当前请求页面用户是否登录,没有的话就跳往登录页面
	User currentUser = (User) request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	if (currentUser == null) {
		response.sendRedirect(path + "/gameBet/login.html");
	}
	if (orderMoneyYuan.indexOf(".") > 0) {
		orderMoneyYuan = orderMoneyYuan.replaceAll("0+?$", "");//去掉多余的0  
		orderMoneyYuan = orderMoneyYuan.replaceAll("[.]$", "");//如最后一位是.则去掉  
	}
	//获取订单提交时间
	Date currTime = new Date();
	QuickBankTypeService quickBankTypeService = (QuickBankTypeService) ApplicationContextUtil.getBean("quickBankTypeService");
	ChargePayService chargePayService = (ChargePayService) ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService) ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.getPayMemmberMsg(Long.valueOf(chargePayId));
	//打印日志
	log.info("业务系统[{}],用户名[{}],发起了微付通支付，生成订单号[{}],支付金额[{}],PayId[{}],payType[{}],chargePayId[{}]", currentUser.getBizSystem(), currentUser.getUserName(), serialNumber,
			OrderMoney, payId, payType, chargePayId);
	QuickBankType query = new QuickBankType();
	query.setPayId(payId);
	String bankType = "";
	List<QuickBankType> list = quickBankTypeService.getQuickBankTypeQuery(query);
	if (list.size() > 0) {
		bankType = list.get(0).getBankType();
	}

	//生成需要跳转的支付地址;
	String payUrl = chargePay.getPayUrl();//转发支付网关地址
	String Address = chargePay.getAddress(); //最终地址

	log.info("微付通支付报文转发地址payUrl: " + payUrl + ",实际网关地址address: " + Address);
	//同步跳转地址
	String notifyUrl = "";
	if (chargePay.getNotifyUrl().startsWith("http://") || chargePay.getNotifyUrl().startsWith("https://")) {
		notifyUrl = chargePay.getNotifyUrl();
	} else {
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
		notifyUrl = basePath + chargePay.getNotifyUrl();
	}

	// 微付通支付对象.
	WeiFuTong weiFuTong = new WeiFuTong();
	// 支付金额.
	weiFuTong.setMoney(OrderMoney);
	// 回调地址.(入账处理)
	weiFuTong.setNotify_url(chargePay.getReturnUrl());
	// 支付成功跳转地址.
	weiFuTong.setReturn_url(notifyUrl);
	// 商户号.
	weiFuTong.setShop_id(chargePay.getMemberId());
	// 商户对应的订单号.
	weiFuTong.setShop_no(serialNumber);
	// 支付方式.wechat,alipay
	weiFuTong.setType(payId);
	// 用户ID.
	weiFuTong.setUser_id(String.valueOf(currentUser.getId()));

	//获取商户秘钥.
	String apiKey = chargePay.getSign();

	//加密原始串.
	String Md5Sign = weiFuTong.getShop_id() + weiFuTong.getUser_id() + weiFuTong.getMoney() + weiFuTong.getType() + apiKey;

	//进行MD5小写加密
	String Signature = Md5Util.getMD5ofStr(Md5Sign);
	//打印日志
	log.info("md5原始串: " + Md5Sign + "生成后的交易签名:" + Signature);
	//设置签名
	weiFuTong.setSign(Signature);
	
	//通过JSON格式去提交数据,以便获取第三方跳转地址
	JSONObject jsonParam = new JSONObject();
	jsonParam.put("shop_id", weiFuTong.getShop_id());
	jsonParam.put("user_id", weiFuTong.getUser_id());
	jsonParam.put("money", weiFuTong.getMoney());
	jsonParam.put("type", weiFuTong.getType());
	jsonParam.put("sign", weiFuTong.getSign());
	jsonParam.put("shop_no", weiFuTong.getShop_no());
	jsonParam.put("notify_url", weiFuTong.getNotify_url());
	jsonParam.put("return_url", weiFuTong.getReturn_url());
	
	log.info("提交的Json参数:"+ jsonParam);
	//获取第三方给我们返回的JSON数据做判断跳转
	String result = HttpClientUtil.doJsonForPost(payUrl, jsonParam);
	log.info("微付通实际支付地址"+payUrl);
	log.info("微付通支付提交支付报文，返回内容:"+ result);
	
	Boolean isJson = JSONUtils.isJson(result);
	String basePath = "";
	if (isJson == true) {
		JSONObject jsonObject = JSONUtils.toJSONObject(result);
		if (StringUtils.isBlank(result)) {
			out.print("微付通发起支付失败,请联系客服");
			return;
		}
		try {
			String qrcode_url = jsonObject.getString("qrcode_url");
			if (!StringUtils.isEmpty(qrcode_url)) {
				response.sendRedirect(qrcode_url);
			} else {
				out.print("微付通支付发起失败,请联系客服!");
				log.error("不是json格式的数据: " + result);
				return;
			}
		} catch (Exception e) {
			out.print("微付通支付发起失败,请联系客服!");
			log.error("微付通发起支付失败!");
			return;
		}

	} else {
		out.print(result);
		log.error("不是json格式的数据: " + result);
		return;
	}
	log.info("交易报文: " + weiFuTong);
%>
