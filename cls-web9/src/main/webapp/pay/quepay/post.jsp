<%@page import="com.team.lottery.pay.model.QuePay"%>
<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.team.lottery.service.QuickBankTypeService"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.vo.QuickBankType"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<jsp:useBean id='md5Util' scope='request'
	class='com.team.lottery.pay.util.Md5Util' />
<%
	//打印日志
	Logger log = LoggerFactory.getLogger(this.getClass());
	//获取当前web路径
	String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	String orderMoneyYuan = OrderMoney;
	//判断当前请求页面用户是否登录,没有的话就跳往登录页面
	User currentUser = (User) request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	if (currentUser == null) {
		response.sendRedirect(path + "/gameBet/login.html");
	}

	//获取订单提交时间
	Date currTime = new Date();
	QuickBankTypeService quickBankTypeService = (QuickBankTypeService) ApplicationContextUtil.getBean("quickBankTypeService");
	ChargePayService chargePayService = (ChargePayService) ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService) ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.getPayMemmberMsg(Long.valueOf(chargePayId));
	//生成订单Id
	String orderId = MakeOrderNum.makeOrderNum("CZ");
	//打印日志
	log.info("业务系统[{}],用户名[{}],发起了雀付支付，生成订单号[{}],支付金额[{}],PayId[{}],payType[{}],chargePayId[{}]",
  			currentUser.getBizSystem(), currentUser.getUserName(), orderId, OrderMoney, payId, payType, chargePayId);
	QuickBankType query = new QuickBankType();
	query.setPayId(payId);
	String bankType = "";
	List<QuickBankType> list = quickBankTypeService.getQuickBankTypeQuery(query);
	if (list.size() > 0) {
		bankType = list.get(0).getBankType();
	}

	//生成需要跳转的支付地址;
	String payUrl = chargePay.getPayUrl();//转发支付网关地址
	String Address = chargePay.getAddress(); //最终地址

	log.info("雀付支付宝报文转发地址payUrl: "+payUrl+",实际网关地址address: "+Address);
	//同步跳转地址
	String returnUrl = "";
	if (chargePay.getNotifyUrl().startsWith("http://") || chargePay.getNotifyUrl().startsWith("https://")) {
		returnUrl = chargePay.getNotifyUrl();
	} else {
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
		returnUrl = basePath + chargePay.getNotifyUrl();
	}
	//设置雀付支付对象
	QuePay quePay=new QuePay();
	//设置商户ID
	quePay.setPay_memberid(chargePay.getMemberId());
	//设置金额
	quePay.setPay_amount(OrderMoney);
	//设置订单号;
	quePay.setPay_orderid(orderId);
	//设置订单生成时间
	quePay.setPay_applydate(currTime);
	//设置通道编号
	quePay.setPay_bankcode(payId);
	//设置服务返回地址
	quePay.setPay_notifyurl(chargePay.getReturnUrl());
	//设置报文返回地址
	quePay.setPay_callbackurl(returnUrl);
	//设置商品名称
	quePay.setPay_productname(currentUser.getUserName());
	
	//用于加密字符串分割;
	String mark = "&";
	//需要加密的字段
	String Md5Sin="pay_amount="+OrderMoney.toString()+mark+"pay_applydate="+quePay.getPay_applydateDateStr()+mark+"pay_bankcode="+payId+mark+"pay_callbackurl="+returnUrl+mark+"pay_memberid="+chargePay.getMemberId()+mark+"pay_notifyurl="+chargePay.getReturnUrl()+mark+"pay_orderid="+orderId+mark+"key="+chargePay.getSign();
	//MD5大写加密的字段;
	String Signature=Md5Util.getMD5ofStr(Md5Sin).toUpperCase();
	log.info("md5原始串: " + Md5Sin + "生成后的交易签名:" + Signature);
	//设置商户签名
	quePay.setPay_md5sign(Signature);
	//冲值订单
	RechargeOrder rechargeOrder = new RechargeOrder();
	//用户ID
	rechargeOrder.setUserId(currentUser.getId());
	//快捷ID
	rechargeOrder.setChargePayId(chargePay.getId());
	//用户名称
	rechargeOrder.setUserName(currentUser.getUserName());
	//申请金额
	rechargeOrder.setApplyValue(new BigDecimal(orderMoneyYuan));
	//订单创建时间
	rechargeOrder.setCreatedDate(new Date());
	//充值类型
	rechargeOrder.setPayType(payType);
	//第三方充值类型
	rechargeOrder.setThirdPayType("QUEPAY");
	//操作描述
	rechargeOrder.setOperateDes(EFundPayType.valueOf(payType).getDescription());
	//流水号
	rechargeOrder.setSerialNumber(orderId);
	rechargeOrder.setPostscript("");
	rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT.name());
	rechargeOrder.setBankName("");
	rechargeOrder.setSubbranchName("");
	rechargeOrder.setBankAccountName("");
	rechargeOrder.setBankCardNum("");
	rechargeOrder.setBankType("");
	rechargeOrder.setBizSystem(currentUser.getBizSystem());
	rechargeOrder.setRefType(EFundRefType.THIRDPAY.name());
	rechargeOrder.setFromType(currentUser.getLoginType());
	//保存新的订单到数据库中
	rechargeWithDrawOrderService.insertSelective(rechargeOrder);
	log.info("交易报文: "+quePay);
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>

<body onload="pay.submit()">
	<form method="post" name="pay" id="pay" action="<%=payUrl%>">
		<TABLE>
			<TR>
				<TD>
				<input name='pay_memberid' type='hidden'value="<%=quePay.getPay_memberid()%>" /> 
				<input name='pay_amount' type='hidden' value="<%=quePay.getPay_amount()%>" /> 
				<input name='pay_applydate' type='hidden' value="<%=quePay.getPay_applydateDateStr() %>" /> 
				<input name='pay_bankcode' type='hidden' value="<%=quePay.getPay_bankcode()%>" />
				<input name='pay_notifyurl' type='hidden' value="<%=quePay.getPay_notifyurl()%>" /> 
				<input name='pay_callbackurl' type='hidden' value="<%=quePay.getPay_callbackurl()%>" />
				<input name='pay_orderid' type='hidden' value="<%=quePay.getPay_orderid()%>" /> 
				<input name='pay_productname' type='hidden' value="<%=quePay.getPay_productname()%>" />
				<!-- <input name='pay_userid' type='hidden' value="ABC0004" /> -->
				<input name='pay_md5sign' type='hidden' value="<%=quePay.getPay_md5sign()%>" />
				</TD>
			</TR>
		</TABLE>

	</form>

</body>
</html>
