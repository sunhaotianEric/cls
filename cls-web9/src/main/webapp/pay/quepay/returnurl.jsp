<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@page import="java.io.*"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="net.sf.json.JSONObject"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
    //获取当前的状态
    //获取商户ID
    String memberid=request.getParameter("memberid");
    //获取订单号
    String orderid=request.getParameter("orderid");
  	//支付流水号
    String transaction_id=request.getParameter("transaction_id");
    //获取订单金额
    String amount=request.getParameter("amount");
    //获取当前时间戳
    String datetime=request.getParameter("datetime");
  	//获取当前交易状态00为成功
    String returncode=request.getParameter("returncode");
  	//获取签名
    String sign=request.getParameter("sign");
    log.info("雀付支付成功通知平台处理信息如下: 商户ID:"+memberid+" 订单号:"+orderid+"订单金额:"+amount+"时间戳:"+datetime+"交易状态:"+returncode+"签名:"+sign);
    
    RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(orderid);
	if(rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1){
		log.error("根据订单号[" + orderid + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知雀付支付报文接收成功
		response.getOutputStream().write("OK".getBytes("UTF-8"));
	}
	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
	if(rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + orderid + "]查找充值订单记录为空");
		//通知雀付支付报文接收成功
		response.getOutputStream().write("OK".getBytes("UTF-8"));
	}		
	//计算签名
	String Md5key = rechargeWithDrawOrder.getSign();
	String MARK = "&";
	//需要加密的字符串
	String hmacstr="amount="+amount+MARK+"datetime="+datetime+MARK+"memberid="+memberid+MARK+"orderid="+orderid+MARK+"returncode="+returncode+MARK+"transaction_id="+transaction_id+MARK+"key="+rechargeWithDrawOrder.getSign();
	log.info("当前MD5源码:"+hmacstr);
	
	//Md5加密串并且大写
	String WaitSign = Md5Util.getMD5ofStr(hmacstr).toUpperCase();
	log.info("加密后的MD5: " + WaitSign);

	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if (WaitSign.compareTo(sign) == 0) {
		boolean dealResult = false;
		if (!StringUtils.isEmpty(returncode) && ("00").equals(returncode)) {
			BigDecimal factMoneyValue = new BigDecimal(amount);
			try {
				dealResult = rechargeWithDrawOrderService.rechargeOrderSuccessDeal(orderid, factMoneyValue);
			} catch (UnEqualVersionException e) {
				log.error("雀付支付充值时发现版本号不一致,订单号[" + orderid + "]");
				//启动新进程进行多次重试
				UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
						UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION,
						orderid, factMoneyValue, "");
				thread.start();
			} catch (Exception e) {
				//异常不进行重试
				log.error(e.getMessage());
			}
		} else {
			if (!("00").equals(returncode)) {
				log.info("雀付支付宝处理错误支付结果：" + returncode);
			}
		}
		log.info("雀付支付宝付订单处理入账结果:{}", dealResult);
		//通知雀付报文接收成功
		out.println("OK");
		return;
	} else {
		log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容["
				+ WaitSign + "]");
		//让第三方继续补发
		out.println("OK");
		return;
	}
%>