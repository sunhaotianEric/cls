<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.RSA"%>
<%@page import="com.team.lottery.pay.model.JianZhengBaoPay"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<jsp:useBean id='md5Util' scope='request'
	class='com.team.lottery.pay.util.Md5Util' />
<%@page errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.InetAddress"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="com.team.lottery.extvo.returnModel.AnShengParameter"%>
<%@page import="com.team.lottery.extvo.returnModel.AnShengData"%>
<%@page import="com.team.lottery.pay.model.BeiFuBaoPay"%>
<%@page import="java.util.*"%>
<%@page import="net.sf.json.JSONObject"%>
<%@ page import="com.team.lottery.pay.util.HttpClients"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@page import="java.security.KeyFactory"%>
<%@page import="java.security.PrivateKey"%>
<%@page import="java.security.Signature"%>
<%@page import="java.security.spec.PKCS8EncodedKeySpec"%>
<%@page import="org.apache.commons.codec.binary.Base64"%>
<%@page import="com.team.lottery.pay.util.HttpClients"%>
<%@page import="com.team.lottery.pay.util.Base64Utils"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
	String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	String orderMoneyYuan = OrderMoney;
	String serialNumber = request.getParameter("serialNumber");

	

	ChargePayService chargePayService = (ChargePayService) ApplicationContextUtil
			.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService) ApplicationContextUtil
			.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long
			.valueOf(chargePayId));
	log.info(" 生成订单号: " + serialNumber + " 订单金额: "
			+ OrderMoney);

	String payUrl = chargePay.getPayUrl();//转发讯银网关地址
	String Address = chargePay.getAddress(); //最终讯银地址
	log.info("payUrl: " + payUrl + " Address: " + Address);
    
    	
    
	String url = Address;
	
	Date currTime = new Date();
	SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.CHINA);
	
	//String sBuilder = "00160003000000000001";
	HashMap<String,Object> mapContent = new HashMap<String,Object>();
	mapContent.put("pay_memberid", chargePay.getMemberId());
	mapContent.put("pay_orderid", serialNumber);
	mapContent.put("pay_applydate", formatter1.format(currTime));
	mapContent.put("pay_bankcode", payId);
	mapContent.put("pay_notifyurl", chargePay.getReturnUrl());
	mapContent.put("pay_callbackurl", chargePay.getNotifyUrl());
	mapContent.put("pay_amount", OrderMoney);
	
	String sBuilder = "";
	
	Collection<String> keyset= mapContent.keySet();
	List list=new ArrayList<String>(keyset);
	Collections.sort(list);
	for(int i=0;i<list.size();i++){
		if(i == 0) {
			sBuilder += list.get(i)+"="+mapContent.get(list.get(i)).toString();
		}
		else {
			sBuilder += "&"+list.get(i)+"="+mapContent.get(list.get(i)).toString();
		}
	}
	String md5 =new String(sBuilder+"&key="+chargePay.getSign());//MD5签名格式
	String Signature = md5Util.getMD5ofStr(md5).toUpperCase();//计算MD5值
	
    //交易签名
    log.info("md5原始串: "+md5 + "生成后的交易签名:" + Signature);
	mapContent.put("pay_productname", "cz");
	InetAddress address = InetAddress.getLocalHost();    
    String ip=address.getHostAddress().toString();   
	mapContent.put("clientip",ip);
	mapContent.put("pay_returnType","json");
	mapContent.put("pay_md5sign", Signature);
	sBuilder = "";
	Collection<String> keysetFrom= mapContent.keySet();
	List listForm=new ArrayList<String>(keysetFrom);
	Collections.sort(listForm);
	for(int i=0;i<listForm.size();i++){
		if(i == 0) {
			sBuilder += listForm.get(i)+"="+mapContent.get(listForm.get(i)).toString();
		}
		else {
			sBuilder += "&"+listForm.get(i)+"="+mapContent.get(listForm.get(i)).toString();
		}
	}
	log.info("发送报文=["+sBuilder+"]");
	String resultString = HttpClients.sendPost(url, sBuilder);
	log.info("返回报文=["+resultString+"]");
	
	//将获得的String对象转为JSON格式
    JSONObject jsonObject = JSONObject.fromObject(resultString);
    String code = jsonObject.getString("status");
    String pay_info=jsonObject.getString("pay_info");
    String msg=jsonObject.getString("msg");
    if(code.equals("0000")){
    	response.sendRedirect(pay_info);
    	return;
    }else{
    	out.print(msg);
    }
   /*  //通过利用JSON键值对的key，来查询value
    String rspMsg = jsonObject.get("msg").toString();
    log.info("msg=" + rspMsg);
    String strSign = jsonObject.get("sign").toString();
    log.info("sign=" + strSign);
	AnShengParameter businessContextReturn = JSONUtils.toBean(
			resultString,
			AnShengParameter.class);
	//String code=businessContextReturn.getCode();
	String urlStr = "";
	if(code.equals("1")){
		AnShengData data = businessContextReturn.getData();
		urlStr=data.getUrl();
		/* response.sendRedirect(urlStr); */
    	/* return; */
	//}
	
	
	//request.setAttribute("payType", payType); */
	//System.out.println("接收的报文               ：" + RSA.decryptByPrivateKey(returnStr.getString("context"), memberPriKeyJava));
%>

<%-- <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta id="viewport" name="viewport"
	content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
<title>充值接口-提交信息处理</title>
<style>
body {
	text-align: center;
}

img {
	margin-top: 60px;
}

p {
	font-size: 36px;
}
</style>
</head>

<body>
	<img id="weixinImg"
		src='<%=path%>/tools/getQrCodePic?code=<%=urlStr%>'></img>
	<p>
		请使用<span id="pay-way"> <c:if test="${payType == 'ALIPAY' }">支付宝</c:if>
			<c:if test="${payType == 'WEIXIN' }">微信</c:if> <c:if test="${payType == 'JDQB' }">京东钱包</c:if>
			<c:if test="${payType == 'QQPAY' }">QQ钱包</c:if> <c:if
				test="${payType == 'UNIONPAY' }">银联二维码</c:if>
		</span>扫描二维码以完成支付
	</p>

</body>
</html>
 --%>