<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%  
    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
    Enumeration enu=request.getParameterNames();
    String logStr="";
    while(enu.hasMoreElements()){
    String paraName=(String)enu.nextElement();
    logStr=logStr+paraName+": "+request.getParameter(paraName)+" ";
    }
    log.info("收到的快快入支付参数："+logStr);
	String result_code = request.getParameter("result_code");
	String time_end = request.getParameter("time_end");
	String platform_trade_no = request.getParameter("platform_trade_no");
	String transaction_id = request.getParameter("transaction_id");
	String orderid = request.getParameter("orderid");
	String price = request.getParameter("price");
	String realprice = request.getParameter("realprice");	
	String orderuid = request.getParameter("orderuid");
	String attach = request.getParameter("attach");
	String key = request.getParameter("key");
  
	log.info("快快入支付成功通知平台处理信息如下: 订单号: "+orderid+" 支付结果: "+result_code+" 实际成功金额: "+
			price+" 支付完成时间: "+time_end+" 第三方返回MD5签名: "+key);
  
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(orderid);
	if(rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1){
		log.error("根据订单号[" + orderid + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知快快入接收报文成功
	    out.println("OK");
		return;
	}
	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
	if(rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + orderid + "]查找充值订单记录为空");
		//通知快快入接收报文成功
	    out.println("OK");
		return;
	}
	
	//签名
	String Md5key = rechargeWithDrawOrder.getSign();
	String MARK = "|";
	String md5 = orderid + MARK + orderuid + MARK + platform_trade_no + MARK + price + MARK + realprice + MARK+ result_code + MARK + Md5key;
	//Md5加密串
	String WaitSign = md5Util.getMD5ofStr(md5);

	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if(WaitSign.compareTo(key)==0){
		boolean dealResult = false;
		if(!StringUtils.isEmpty(result_code) && result_code.equals("SUCCESS")){
			//将实际成功金额除以100
			BigDecimal factMoneyValue = new BigDecimal(price);
			try {
				dealResult = rechargeWithDrawOrderService.rechargeOrderSuccessDeal(orderid,factMoneyValue);
			}catch(UnEqualVersionException e) {
		   		log.error("快快入在线充值时发现版本号不一致,订单号["+orderid+"]");
		   		//启动新进程进行多次重试
		   		UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION
		   				, orderid,factMoneyValue,time_end);
		   		thread.start();
			} catch(Exception e){
				//异常不进行重试
				log.error(e.getMessage());
			}
		} else {
			if(!result_code.equals("SUCCESS")){
				log.info("快快入支付处理错误支付结果："+result_code);
			}
		}
		log.info("快快入订单处理入账结果:{}", dealResult);
		//通知快快入接收报文成功	
	    out.println("OK");
		return;
	}else{
		log.info("MD5校验失败,报文返回MD5签名内容["+ key +"],计算MD5签名内容[" + WaitSign + "]");
		//让第三方继续补发
		out.println("ERROR");
		return;
	}
%>