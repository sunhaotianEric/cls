<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page
	import="com.team.lottery.cache.ClsCacheManager,com.team.lottery.vo.BizSystemInfo"%>
<%
	//获取当前域名
	String serverName = request.getServerName();
	String bizSystem = ClsCacheManager
			.getBizSystemByDomainUrl(serverName);
	BizSystemInfo bizSystemInfo = ClsCacheManager
			.getBizSystemInfo(bizSystem);
	String faviconUrl = "";
	if (bizSystemInfo != null && bizSystemInfo.getFaviconUrl() != null) {
		faviconUrl = bizSystemInfo.getFaviconUrl();
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset=utf-8>
<meta http-equiv=X-UA-Compatible content="IE=edge">
<meta name=apple-mobile-web-app-capable content=yes>
<meta name=viewport
	content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0">
<meta name=robots content=noindex,nofollow>
<meta name=apple-touch-fullscreen content=yes>
<meta name=renderer content=webkit>
<meta name=apple-mobile-web-app-status-bar-style content=black>
<meta name=format-detection content="telphone=no, email=no">
<meta name=HandheldFriendly content=true>
<meta name=MobileOptimized content=320>
<meta name=screen-orientation content=portrait>
<meta name=x5-orientation content=portrait>
<meta name=full-screen content=yes>
<meta name=msapplication-tap-highlight content=no>
<meta name=imagemode content=force>
<meta name=nightmode content=disable>
<meta name=layoutmode content=fitscreen/standard>
<title>首页</title>
<link rel="shortcut icon" href="<%=faviconUrl%>" type="image/x-icon" />
</head>
<body>
	<h1>这是首页</h1>
	<noscript>
		<strong>本页面需要浏览器支持（启用）JavaScript</strong>
	</noscript>
</body>
</html>