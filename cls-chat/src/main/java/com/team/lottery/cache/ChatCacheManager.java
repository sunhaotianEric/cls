package com.team.lottery.cache;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.cache.ehcache.EhCacheCacheManager;

import com.team.lottery.service.ChatBanService;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.vo.ChatBan;

import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;

public class ChatCacheManager {

	private static ChatBanService chatBanService = ApplicationContextUtil.getBean(ChatBanService.class);
	//聊天黑名单缓存name
	public static final String CHAT_BAN_CACHE="chatBanCache";
	
	private static EhCacheCacheManager cacheManager;
	static {
		cacheManager = ApplicationContextUtil.getBean(EhCacheCacheManager.class);
	}
	
	/**
	 * 初始化聊天缓存
	 */
	public static void initCache() {
		//初始化黑名单缓存
		initChatBanCache();
	}
	
	/**
	 * 初始化黑名单缓存
	 */
	public static void initChatBanCache(){
		ChatBan record =new ChatBan();
		record.setLostTime(new Date());
		List<ChatBan> chatBans=chatBanService.getAllChatBan(record);
		Cache cache = cacheManager.getCacheManager().getCache(CHAT_BAN_CACHE);
		if(cache!=null) {
			cache.removeAll();
			if(CollectionUtils.isNotEmpty(chatBans)){
				Map<String,Object> chatMap=new HashMap<String,Object>();
				for(ChatBan chatBan : chatBans) {
					chatMap.put(chatBan.getBizSystem()+chatBan.getRoomId(),(chatMap.get(chatBan.getBizSystem()+chatBan.getRoomId())==null?",":chatMap.get(chatBan.getBizSystem()+chatBan.getRoomId()))+chatBan.getUserName()+","+chatBan.getIp()+",");
				}
				for(Map.Entry<String,Object> entry : chatMap.entrySet()){
					Element element = new Element(entry.getKey(),entry.getValue());
					cache.put(element);
				}
			}
		}
	}
	
	/**
	 * 更新黑名单缓存
	 * @param record
	 * @param operateType
	 */
	public static void updateChatBanCache(ChatBan record,String operateType){
		record.setLostTime(new Date());
		List<ChatBan> chatBans=chatBanService.getAllChatBan(record);
		Cache cache = cacheManager.getCacheManager().getCache(CHAT_BAN_CACHE);
		if(cache!=null){
			if(CollectionUtils.isNotEmpty(chatBans)){
				if("-1".equals(operateType)){
					cache.remove(record.getBizSystem()+record.getRoomId());
					Element element = new Element(record.getBizSystem()+record.getRoomId(),"");
					cache.put(element);
				}else if("0".equals(operateType)){
					Map<String,Object> chatMap=new HashMap<String,Object>();
					for(ChatBan chatBan : chatBans) {
						chatMap.put(chatBan.getBizSystem()+chatBan.getRoomId(), (chatMap.get(chatBan.getBizSystem()+chatBan.getRoomId())==null?",":chatMap.get(chatBan.getBizSystem()+chatBan.getRoomId()))+chatBan.getUserName()+","+chatBan.getIp()+",");
					}
					cache.remove(record.getBizSystem()+record.getRoomId());
					Element element = new Element(record.getBizSystem()+record.getRoomId(),  chatMap.get(record.getBizSystem()+record.getRoomId()));
					cache.put(element);
				}else if("1".equals(operateType)){
					Map<String,Object> chatMap=new HashMap<String,Object>();
					for(ChatBan chatBan : chatBans) {
						chatMap.put(chatBan.getBizSystem()+chatBan.getRoomId(), (chatMap.get(chatBan.getBizSystem()+chatBan.getRoomId())==null?",":chatMap.get(chatBan.getBizSystem()+chatBan.getRoomId()))+chatBan.getUserName()+","+chatBan.getIp()+",");
					}
					Element element = new Element(record.getBizSystem()+record.getRoomId(), chatMap.get(record.getBizSystem()+record.getRoomId()));
					cache.put(element);
				}
			}
		}
	}
	
	/**
	 * 获取指定房间的黑名单缓存
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static String getChatBan(ChatBan record){
		Cache cache = cacheManager.getCacheManager().getCache(CHAT_BAN_CACHE);
		if(cache!=null){
			List<String> keys = (List<String>)cache.getKeys();
			for(String key : keys) {
				if(key.equals(record.getBizSystem()+record.getRoomId())){
					Element element = cache.get(key);
					String ban = (String) element.getObjectValue();
					return ban;
				}
			}
		}
		return null;
	}
}
