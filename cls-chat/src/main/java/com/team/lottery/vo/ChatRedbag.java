package com.team.lottery.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.util.DateUtils;

public class ChatRedbag {
	private Integer id;

	private String bizSystem;

	private Integer roomId;

	private Integer userId;

	private String userName;

	private BigDecimal money;

	private Integer count;

	private String remark;

	private Integer receivedCount;

	private BigDecimal receivedMoney;

	private String type;

	private Integer status;

	private BigDecimal expireMoney;

	private Date expireTime;

	private Date createTime;

	private Date updateTime;
	
	private String randomMoney;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem == null ? null : bizSystem.trim();
	}

	public Integer getRoomId() {
		return roomId;
	}

	public void setRoomId(Integer roomId) {
		this.roomId = roomId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName == null ? null : userName.trim();
	}

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}

	public Integer getReceivedCount() {
		return receivedCount;
	}

	public void setReceivedCount(Integer receivedCount) {
		this.receivedCount = receivedCount;
	}

	public BigDecimal getReceivedMoney() {
		return receivedMoney;
	}

	public void setReceivedMoney(BigDecimal receivedMoney) {
		this.receivedMoney = receivedMoney;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type == null ? null : type.trim();
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public BigDecimal getExpireMoney() {
		return expireMoney;
	}

	public void setExpireMoney(BigDecimal expireMoney) {
		this.expireMoney = expireMoney;
	}

	public Date getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(Date expireTime) {
		this.expireTime = expireTime;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
	public String getRandomMoney() {
        return randomMoney;
    }

    public void setRandomMoney(String randomMoney) {
        this.randomMoney = randomMoney == null ? null : randomMoney.trim();
    }

	// 获取时间字符串
	public String getCreateTimeStr() {
		if (this.getCreateTime() != null) {
			return DateUtils.getDateToStrByFormat(this.getCreateTime(), "yyyy/MM/dd HH:mm:ss");
		}
		return "";
	}

	public String getBizSystemName() {
		return this.bizSystem == null ? "" : ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
	}
}