package com.team.lottery.vo;

import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.util.DateUtils;

public class ChatRoom {
	private Long id;

	private String bizSystem;

	private Integer roomId;

	private String roomName;

	private String agent;

	private Date createDate;

	private Date updateDate;
	
	private Integer vipRemind;

	private String announce;

	private String msgban;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem == null ? null : bizSystem.trim();
	}

	public Integer getRoomId() {
		return roomId;
	}

	public void setRoomId(Integer roomId) {
		this.roomId = roomId;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName == null ? null : roomName.trim();
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent == null ? null : agent.trim();
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Integer getVipRemind() {
        return vipRemind;
    }

    public void setVipRemind(Integer vipRemind) {
        this.vipRemind = vipRemind;
    }
	
	public String getAnnounce() {
		return announce;
	}

	public void setAnnounce(String announce) {
		this.announce = announce == null ? null : announce.trim();
	}

	public String getMsgban() {
		return msgban;
	}

	public void setMsgban(String msgban) {
		this.msgban = msgban == null ? null : msgban.trim();
	}

	public String getBizSystemName() {
		return this.bizSystem == null ? "" : ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
	}

	// 获取时间字符串
	public String getCreateDateStr() {
		if (this.getCreateDate() != null) {
			return DateUtils.getDateToStrByFormat(this.getCreateDate(), "yyyy/MM/dd HH:mm:ss");
		}
		return "";
	}
}