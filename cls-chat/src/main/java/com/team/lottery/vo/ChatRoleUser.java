package com.team.lottery.vo;

import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.util.DateUtils;

public class ChatRoleUser {
	private Integer id;

	private String bizSystem;

	private Integer userId;

	private String userName;

	private String role;

	private Integer roomId;

	private Date createDate;

	private Date updateDate;

	// 用来关联查询的
	private String nick;
	private String vipLevel;
	private String headImg;

	public String getHeadImg() {
		return headImg;
	}

	public void setHeadImg(String headImg) {
		this.headImg = headImg;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getVipLevel() {
		return vipLevel;
	}

	public void setVipLevel(String vipLevel) {
		this.vipLevel = vipLevel;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem == null ? null : bizSystem.trim();
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName == null ? null : userName.trim();
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role == null ? null : role.trim();
	}

	public Integer getRoomId() {
		return roomId;
	}

	public void setRoomId(Integer roomId) {
		this.roomId = roomId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	// 获取时间字符串
	public String getCreateDateStr() {
		if (this.getCreateDate() != null) {
			return DateUtils.getDateToStrByFormat(this.getCreateDate(), "yyyy/MM/dd HH:mm:ss");
		}
		return "";
	}

	// 获取时间字符串
	public String getUpdateDateStr() {
		if (this.getCreateDate() != null) {
			return DateUtils.getDateToStrByFormat(this.getUpdateDate(), "yyyy/MM/dd HH:mm:ss");
		}
		return "";
	}
	
	public String getBizSystemName() {
        return this.bizSystem==null?"":ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
    }
}