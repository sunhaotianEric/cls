package com.team.lottery.vo;

import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.util.DateUtils;

public class ChatLevelAuth {
    private Long id;

    private String bizSystem;

    private Integer level;

    private String levelName;

    private String rules;

    private Date createDate;

    private Date updateDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName == null ? null : levelName.trim();
    }

    public String getRules() {
        return rules;
    }

    public void setRules(String rules) {
        this.rules = rules == null ? null : rules.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
    public String getBizSystemName() {
    	
        return this.bizSystem==null?"":ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
    }
    public String getCreateDateStr(){
    	return DateUtils.getDateToStrByFormat(this.getCreateDate(), "yyyy-MM-dd HH:mm:ss");
    }
}