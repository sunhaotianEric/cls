package com.team.lottery.vo;

import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.util.DateUtils;

public class ChatUserSendredbagAuth {
    private Integer id;

    private String bizSystem;

    private Integer roomid;

    private String userName;

    private Long allowSendMoney;

    private String showName;

    private Date createDate;

    private Date updateDate;
    
    private String updateAdmin;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public Integer getRoomid() {
        return roomid;
    }

    public void setRoomid(Integer roomid) {
        this.roomid = roomid;
    }

    public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getAllowSendMoney() {
        return allowSendMoney;
    }

    public void setAllowSendMoney(Long allowSendMoney) {
        this.allowSendMoney = allowSendMoney;
    }

    public String getShowName() {
        return showName;
    }

    public void setShowName(String showName) {
        this.showName = showName == null ? null : showName.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

	public String getUpdateAdmin() {
		return updateAdmin;
	}

	public void setUpdateAdmin(String updateAdmin) {
		this.updateAdmin = updateAdmin;
	}
    
	  //获取时间字符串
  	public String getCreateDateStr(){
  		if(this.getCreateDate() != null){
  	    	return DateUtils.getDateToStrByFormat(this.getCreateDate(), "yyyy/MM/dd HH:mm:ss");
  		}
  		return "";
  	}
  	
  	public String getBizSystemName() {
        return this.bizSystem==null?"":ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
    }
}