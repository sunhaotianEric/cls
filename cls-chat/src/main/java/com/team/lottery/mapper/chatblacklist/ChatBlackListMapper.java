package com.team.lottery.mapper.chatblacklist;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.ChatBlackList;

public interface ChatBlackListMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ChatBlackList record);

    int insertSelective(ChatBlackList record);

    ChatBlackList selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ChatBlackList record);

    int updateByPrimaryKey(ChatBlackList record);
    
    ChatBlackList getByCondition(@Param("userName") String userName,@Param("roomId") Integer roomId,@Param("bizSystem") String bizSystem);
    List<ChatBlackList> getListByCondition(@Param("roomId") Integer roomId,@Param("bizSystem") String bizSystem);
    
    /**
     * 根据用户名查询
     * @param bizSystem
     * @param userName
     * @return
     */
   	public ChatBlackList selectByUserName(@Param("bizSystem") String bizSystem, @Param("userName") String userName);
   	
   	public int deleteChatBlackList(@Param("Id") List<Integer> Id);
   	
   	List<ChatBlackList> getAllByCondition(@Param("userName") List<String> userName,@Param("roomId") Integer roomId,@Param("bizSystem") String bizSystem);
}