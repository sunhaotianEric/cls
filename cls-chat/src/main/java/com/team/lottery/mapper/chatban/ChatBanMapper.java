package com.team.lottery.mapper.chatban;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.ChatBan;

public interface ChatBanMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ChatBan record);

    int insertSelective(ChatBan record);

    ChatBan selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ChatBan record);

    int updateByPrimaryKey(ChatBan record);
    
    public Integer getAllChatBanQueryPageCount(@Param("query")ChatBan query);
    
   	List<ChatBan> getAllChatBanQueryPage(@Param("query")ChatBan query, @Param("startNum")Integer startNum,
   			@Param("pageNum")Integer pageNum);
   	List<ChatBan> getAllChatBan(@Param("query")ChatBan query);
   	
   	/**
     * 根据用户名查询
     * @param bizSystem
     * @param userName
     * @return
     */
   	public ChatBan selectByUserName(@Param("bizSystem") String bizSystem, @Param("userName") String userName);
   	
   	/**
   	 * 根据业务系统查询所有的
   	 * @param bizSystem
   	 * @return
   	 */
   	public List<ChatBan> getChatBan(@Param("roomId") Integer roomId,@Param("bizSystem") String bizSystem);
   	
   	ChatBan getByOnlyChatBan(@Param("userName") String userName,@Param("roomId") Integer roomId,@Param("bizSystem") String bizSystem);

   	public int deleteChatBan(@Param("Id") List<Integer> Id);
   	
   	List<ChatBan> getByOnlyChatBans(@Param("userName") List<String> userName,@Param("roomId") Integer roomId,@Param("bizSystem") String bizSystem);
}