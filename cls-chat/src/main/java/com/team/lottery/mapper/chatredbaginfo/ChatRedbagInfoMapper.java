package com.team.lottery.mapper.chatredbaginfo;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import com.team.lottery.vo.ChatRedbagInfo;

public interface ChatRedbagInfoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ChatRedbagInfo record);

    int insertSelective(ChatRedbagInfo record);

    ChatRedbagInfo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ChatRedbagInfo record);

    int updateByPrimaryKey(ChatRedbagInfo record);
   
    /**
	 * 查询所有领取记录
	 * @return
	 * @throws ParseException 
	 */
    List<ChatRedbagInfo> getChatRedbagInfoAll(@Param("query")ChatRedbagInfo record);
    
    /**
	 * 查询用户的
	 * @return
	 */
    List<Map<String,Object>> getChatRedBagInfoByMap(@Param("query")ChatRedbagInfo record);
    
    /**
	 * 查询用户已领取的红包
	 * @param record
	 * @return
	 */
    List<ChatRedbagInfo> getOverdueRedEnvelopes(@Param("query")ChatRedbagInfo record);
    
    /**
     * 分页查询
     * @param record
     * @param page
     * @param endTime
     * @return
     */
    List<ChatRedbagInfo> getChatRedbaginfoPage(@Param("query")ChatRedbagInfo record,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
   
    /**
     * 查询符合条件的记录条数
     * @param record
     * @return
     */
    int getChatRedbaginfoPageCount(@Param("query")ChatRedbagInfo record);
    
    /**
     * 根据Rid查询数据
     * @param chatlogId
     * @return
     */
    List<ChatRedbagInfo> getChatRedbaginfoByrid(@Param("chatlogId")Integer chatlogId);
    
    List<ChatRedbagInfo> getChatRedbaginfoByChatRedbagId(@Param("roomId") Integer roomId, @Param("chatRedbagId") Integer chatRedbagId);
}