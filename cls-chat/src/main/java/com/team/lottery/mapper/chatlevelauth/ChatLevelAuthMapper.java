package com.team.lottery.mapper.chatlevelauth;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.ChatLevelAuth;

public interface ChatLevelAuthMapper {
	int deleteByPrimaryKey(Long id);

	int insert(ChatLevelAuth record);

	int insertSelective(ChatLevelAuth record);

	ChatLevelAuth selectByPrimaryKey(Long id);

	int updateByPrimaryKeySelective(ChatLevelAuth record);

	int updateByPrimaryKey(ChatLevelAuth record);

	/**
	 * 查询符合条件的记录条数
	 * @param query
	 * @return
	 */
	public Integer getAllChatLevelAuthQueryPageCount(@Param("query") ChatLevelAuth query);

	/**
	 * 查找所有的数据
	 * 
	 * @param page
	 * @param query
	 * @return
	 */
	List<ChatLevelAuth> getAllChatLevelAuthQueryPage(@Param("query") ChatLevelAuth query,
			@Param("startNum") int startIndex, @Param("pageNum") Integer pageSize);

	/**
	 * 根据业务系统及VIP等级查询发言权限
	 * 
	 * @return
	 */
	ChatLevelAuth getAllChatLevelAuth(@Param("query") ChatLevelAuth query);
}