package com.team.lottery.mapper.chatroom;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.ChatRoom;

public interface ChatRoomMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ChatRoom record);

    int insertSelective(ChatRoom record);

    ChatRoom selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ChatRoom record);

    int updateByPrimaryKeyWithBLOBs(ChatRoom record);

    int updateByPrimaryKey(ChatRoom record);
    
    List<ChatRoom> getChatRoom(@Param("query")ChatRoom chatRoom,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
    int getChatRoomCount(@Param("query")ChatRoom chatRoom);
    List<ChatRoom> getChatRoomByRoomId(@Param("query")ChatRoom chatRoom);
    List<ChatRoom> getChatRoomList(@Param("query")ChatRoom chatRoom);
    ChatRoom getChatRoomOnUser(@Param("query")ChatRoom chatRoom);
    
    /**
     * 根据房间id和系统查询房间信息
     * @param roomId
     * @param bizSystem
     * @return
     */
    public ChatRoom getChatRoomByRoomIds(@Param("roomId") Integer roomId,@Param("bizSystem") String bizSystem);
    
    public List<ChatRoom> getChatRoomByVipRemind(@Param("roomId") Integer roomId,@Param("bizSystem") String bizSystem,@Param("vipRemind") List<Integer> vipRemind);
    
}