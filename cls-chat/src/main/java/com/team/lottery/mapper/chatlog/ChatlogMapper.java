package com.team.lottery.mapper.chatlog;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.Chatlog;
import com.team.lottery.vo.User;

public interface ChatlogMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Chatlog record);

    int insertSelective(Chatlog record);

    Chatlog selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Chatlog record);

    int updateByPrimaryKeyWithBLOBs(Chatlog record);

    int updateByPrimaryKey(Chatlog record);
    
	/**
	 * 根据条件查询
	 * @return
	 */
	public Integer getAllChatLogPageCount(@Param("query")Chatlog query);

	List<Chatlog> getAllChatLogPage(@Param("query")Chatlog query,@Param("startNum")int startIndex, @Param("pageNum")Integer pageSize);
	
	List<Chatlog> getHisChatLogPage(@Param("query")Chatlog query,@Param("startNum")int startIndex, @Param("pageNum")Integer pageSize);
	
	List<Chatlog> getChatLog(@Param("query")Chatlog query,@Param("startNum")int startIndex, @Param("pageNum")Integer pageSize);
	
	Integer getChatLogCount(@Param("query")Chatlog query);
	Integer updateChatLogByUser(@Param("query")User query);
	
	public Chatlog selectByUsername(String username);
}