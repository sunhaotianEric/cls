package com.team.lottery.mapper.chatroomconfig;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.ChatroomConfig;

public interface ChatroomConfigMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ChatroomConfig record);

    int insertSelective(ChatroomConfig record);

    ChatroomConfig selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ChatroomConfig record);

    int updateByPrimaryKey(ChatroomConfig record);
    
    List<ChatroomConfig> getAllChatRoomConfig(@Param("query")ChatroomConfig record,@Param("startNum")int startNum,@Param("pageNum")int pageNum);
    
    int getAllChatRoomConfigCount(@Param("query")ChatroomConfig record);
    
    ChatroomConfig getChatRoomConfig(@Param("query")ChatroomConfig record);
}