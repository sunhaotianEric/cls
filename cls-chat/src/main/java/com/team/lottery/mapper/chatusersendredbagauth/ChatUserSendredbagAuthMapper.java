package com.team.lottery.mapper.chatusersendredbagauth;

import org.apache.ibatis.annotations.Param;
import java.util.List;
import com.team.lottery.vo.ChatUserSendredbagAuth;

public interface ChatUserSendredbagAuthMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ChatUserSendredbagAuth record);

    int insertSelective(ChatUserSendredbagAuth record);

    ChatUserSendredbagAuth selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ChatUserSendredbagAuth record);

    int updateByPrimaryKey(ChatUserSendredbagAuth record);
    
    /**
     * 根据业务系统，房间ID，用户名查询
     * @param record
     * @return
     */
    ChatUserSendredbagAuth getChatUserSendredbagAuth(@Param("query")ChatUserSendredbagAuth record);
    /**
     * 分页查询
     * @param record
     * @param page
     * @return
     */
    List<ChatUserSendredbagAuth> getChatUserSendredbagAuthPage(@Param("query")ChatUserSendredbagAuth record,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
    
    /**
     * 查询符合条件的记录条数
     * @param record
     * @return
     */
    int getChatUserSendredbagAuthPageCount(@Param("query")ChatUserSendredbagAuth record);
    
    /**
     * 根据条件查询记录条数
     * @param record
     * @return
     */
    int getChatUserSendredbagAuthCountForUpdate(@Param("query")ChatUserSendredbagAuth record);
}