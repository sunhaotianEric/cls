package com.team.lottery.mapper.chatroleuser;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.ChatRoleUser;

public interface ChatRoleUserMapper {
	int deleteByPrimaryKey(Integer id);

	int insert(ChatRoleUser record);

	int insertSelective(ChatRoleUser record);

	ChatRoleUser selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(ChatRoleUser record);

	int updateByPrimaryKey(ChatRoleUser record);

	/**
	 * 分页条件
	 * 
	 * @param query
	 * @param startNum
	 * @param pageNum
	 * @return
	 */
	List<ChatRoleUser> getAllChatRoleUser(@Param("query") ChatRoleUser query, @Param("startNum") Integer startNum, @Param("pageNum") Integer pageNum);

	/**
	 * 查询所有的
	 * 
	 * @param query
	 * @return
	 */
	public int getChatRoleUserCount(@Param("query") ChatRoleUser query);

	/**
	 * 根据名字查询
	 * 
	 * @param userName
	 * @param roomId
	 * @param bizSystem
	 * @return
	 */
	ChatRoleUser getChatRoleUserByUserName(@Param("userName") String userName, @Param("roomId") Integer roomId, @Param("bizSystem") String bizSystem);

	/**
	 * 根据用户名查询
	 * 
	 * @param bizSystem
	 * @param userName
	 * @return
	 */
	public ChatRoleUser selectByUserName(@Param("bizSystem") String bizSystem, @Param("userName") String userName);

	/**
	 * 查询所有不包括群主的信息
	 * 
	 * @param userName
	 * @param roomId
	 * @param bizSystem
	 * @return
	 */
	List<ChatRoleUser> selectByRole(@Param("roomId") Integer roomId, @Param("bizSystem") String bizSystem);

	/**
	 * 根据房间号，系统查询所有
	 * 
	 * @param role
	 * @param roomId
	 * @param bizSystem
	 * @return
	 */
	public List<ChatRoleUser> getChatRoleUser(@Param("roomId") Integer roomId, @Param("bizSystem") String bizSystem);

	/**
	 * 根据角色，房间号，系统查询所有
	 * 
	 * @param role
	 * @param roomId
	 * @param bizSystem
	 * @return
	 */
	public List<ChatRoleUser> getChatRoleUserRole(@Param("role") String role, @Param("roomId") Integer roomId, @Param("bizSystem") String bizSystem);

	List<ChatRoleUser> getAllByOnlyChatRoleUser(@Param("userName") List<String> userName, @Param("roomId") Integer roomId, @Param("bizSystem") String bizSystem);

	int insertChatRoleUser(List<ChatRoleUser> record);

	public List<ChatRoleUser> selectAllByUserName(@Param("bizSystem") String bizSystem, @Param("userName") List<String> userName);

	public int deleteChatRoleUser(@Param("Id") List<Integer> Id);
}