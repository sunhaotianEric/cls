package com.team.lottery.mapper.chatredbag;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.ChatRedbag;

public interface ChatRedbagMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ChatRedbag record);

    int insertSelective(ChatRedbag record);

    ChatRedbag selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ChatRedbag record);

    int updateByPrimaryKey(ChatRedbag record);
    
    /**
	 * 统计今天发红包总数
	 * @param record
	 * @return
	 */
    ChatRedbag getChatRedBagBySumMoney(@Param("query")ChatRedbag record);
    
    /**
     * 分页查询
     * @param record
     * @param page
     * @param endTime
     * @return
     */
    List<ChatRedbag> getChatRedbagPage(@Param("query")ChatRedbag record,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
    
    /**
     * 查询符合条件的记录条数
     * @param record
     * @return
     */
    int getChatRedbagPageCount(@Param("query")ChatRedbag record);
    
    /**
	 * 以当前时间为基准查询-1天以内的红包数据
	 * @param record
	 * @return
	 */
    List<ChatRedbag> getChatRedBagBytime(@Param("query")ChatRedbag record);
    
    /**
     * 查询已抢光的红包
     * @param record
     * @return
     */
    List<ChatRedbag> getChatRedBagByCount(@Param("query")ChatRedbag record);
   
    /**
     * 查询已过期红包及以领取金额
     * @param createTime
     * @return
     */
    List<ChatRedbag> getChatRedbagByStatus(@Param("createTime")Date createTime);
    
    /**
     * 根据条件查询
     * @param bizSystem
     * @param userName
     * @return
     */
    ChatRedbag getChatRedBagBySystemAndUserName(@Param("bizSystem")String bizSystem,@Param("userName") String userName,@Param("type") String type);
}