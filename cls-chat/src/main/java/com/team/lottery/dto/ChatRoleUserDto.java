package com.team.lottery.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.team.lottery.enums.EChatRole;
import com.team.lottery.util.DateUtils;
import com.team.lottery.vo.ChatRoleUser;

public class ChatRoleUserDto {

    private String userName;
    // 角色英文简写
    private String role;
    // 角色中文描述
    private String roleres;

    private Integer roomId;

    private Date createDate;

    private Date updateDate;
    
    // 用来关联查询的
    private String nick;
    private String vipLevel;
    private String headImg;

    public String getHeadImg() {
		return headImg;
	}

	public void setHeadImg(String headImg) {
		this.headImg = headImg;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getVipLevel() {
		return vipLevel;
	}

	public void setVipLevel(String vipLevel) {
		this.vipLevel = vipLevel;
	}

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role == null ? null : role.trim();
    }

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
    
    public String getRoleres() {
		return roleres;
	}

	public void setRoleres(String roleres) {
		this.roleres = roleres;
	}

	/**
	 * 将ChatRoleUser对象装换为ChatRoleUserDto对象.
	 * @param ChatRoleUser
	 * @return
	 */
	public static ChatRoleUserDto transToDto(ChatRoleUser chatRoleUser) {
		ChatRoleUserDto chatRoleUserDto = null;
		if(chatRoleUser != null){
			chatRoleUserDto = new ChatRoleUserDto();
			chatRoleUserDto.setCreateDate(chatRoleUser.getCreateDate());
			chatRoleUserDto.setRoomId(chatRoleUser.getRoomId());
			chatRoleUserDto.setUpdateDate(chatRoleUser.getUpdateDate());
			chatRoleUserDto.setUserName(chatRoleUser.getUserName());
			chatRoleUserDto.setNick(chatRoleUser.getNick());
			chatRoleUserDto.setVipLevel(chatRoleUser.getVipLevel());
			chatRoleUserDto.setHeadImg(chatRoleUser.getHeadImg());
			chatRoleUserDto.setRole(chatRoleUser.getRole());
			if (chatRoleUser.getRole().equals(EChatRole.MANAGER.getCode())) {
				chatRoleUserDto.setRoleres(EChatRole.MANAGER.getDescription());
			}
			if (chatRoleUser.getRole().equals(EChatRole.ORDINARYUSERS.getCode())) {
				chatRoleUserDto.setRoleres(EChatRole.ORDINARYUSERS.getDescription());
			}
			if (chatRoleUser.getRole().equals(EChatRole.CUSTOMER.getCode())) {
				chatRoleUserDto.setRoleres(EChatRole.CUSTOMER.getDescription());
			}
			if (chatRoleUser.getRole().equals(EChatRole.TUTOR.getCode())) {
				chatRoleUserDto.setRoleres(EChatRole.TUTOR.getDescription());
			}
		}
		return chatRoleUserDto;
	}
     
	/**
	 * 将ChatRoleUserList对象装换为ChatRoleUserDtoList对象.
	 * @param chatRoleUsers
	 * @return
	 */
	public static List<ChatRoleUserDto> transToDtoList(List<ChatRoleUser> chatRoleUsers) {
		List<ChatRoleUserDto> chatRoleUserDtos = new ArrayList<ChatRoleUserDto>();
		for (ChatRoleUser chatRoleUser : chatRoleUsers) {
			chatRoleUserDtos.add(transToDto(chatRoleUser));
		}
		return chatRoleUserDtos;
	}
	
	/**
     * 获取归属时间字符串
     * @return
     */
    public String getCreateDateStr(){
    	return DateUtils.getDateToStrByFormat(this.getCreateDate(), "yyyy-MM-dd");
    }
    
    /**
     * 获取归属时间字符串
     * @return
     */
    public String getUpdateDateStr(){
    	return DateUtils.getDateToStrByFormat(this.getUpdateDate(), "yyyy-MM-dd");
    }
}
