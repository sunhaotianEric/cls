package com.team.lottery.dto;

import com.team.lottery.vo.ChatRoleUser;
import com.team.lottery.vo.ChatRoom;

public class ChatRoomDto {
	
	private Integer roomId;

	private String roomName;

	private String agent;
	
	private Integer vipRemind;

	private String announce;

	private String msgban;
	
	private Integer userId;

	private String userName;

	private String role;
	
	// 用来关联查询的
	private String nick;
	private String vipLevel;
	private String headImg;
	public Integer getRoomId() {
		return roomId;
	}
	public void setRoomId(Integer roomId) {
		this.roomId = roomId;
	}
	public String getRoomName() {
		return roomName;
	}
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	public String getAgent() {
		return agent;
	}
	public void setAgent(String agent) {
		this.agent = agent;
	}
	public Integer getVipRemind() {
		return vipRemind;
	}
	public void setVipRemind(Integer vipRemind) {
		this.vipRemind = vipRemind;
	}
	public String getAnnounce() {
		return announce;
	}
	public void setAnnounce(String announce) {
		this.announce = announce;
	}
	public String getMsgban() {
		return msgban;
	}
	public void setMsgban(String msgban) {
		this.msgban = msgban;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	public String getVipLevel() {
		return vipLevel;
	}
	public void setVipLevel(String vipLevel) {
		this.vipLevel = vipLevel;
	}
	public String getHeadImg() {
		return headImg;
	}
	public void setHeadImg(String headImg) {
		this.headImg = headImg;
	}
	
	/**
	 * 将ChatRoom,ChatRoleUser对象装换为ChatRoomDto对象.
	 * @param ChatRoleUser
	 * @return
	 */
	public static ChatRoomDto transToDto(ChatRoleUser chatRoleUser, ChatRoom chatRoom) {
		ChatRoomDto chatRoomDto = null;
		if(chatRoleUser != null || chatRoom != null){
			chatRoomDto = new ChatRoomDto();
			chatRoomDto.setAgent(chatRoom.getAgent());
			chatRoomDto.setAnnounce(chatRoom.getAnnounce());
			chatRoomDto.setHeadImg(chatRoleUser.getHeadImg());
			chatRoomDto.setMsgban(chatRoom.getMsgban());
			chatRoomDto.setNick(chatRoleUser.getNick());
			chatRoomDto.setRole(chatRoleUser.getRole());
			chatRoomDto.setRoomId(chatRoom.getRoomId());
			chatRoomDto.setRoomName(chatRoom.getRoomName());
			chatRoomDto.setUserId(chatRoleUser.getUserId());
			chatRoomDto.setUserName(chatRoleUser.getUserName());
			chatRoomDto.setVipLevel(chatRoleUser.getVipLevel());
			chatRoomDto.setVipRemind(chatRoom.getVipRemind());
		}
		return chatRoomDto;
	}
   
}
