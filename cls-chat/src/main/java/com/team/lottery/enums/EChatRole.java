package com.team.lottery.enums;

/**
 * 聊天室角色枚举
 * 群主，管理员，普通用户
 * @author Owner
 *
 */
public enum EChatRole {
	
	GROUPOWNER("GROUPOWNER", "群主"),
	MANAGER("MANAGER", "管理员"),
	ORDINARYUSERS("ORDINARYUSERS", "普通用户"),
	CUSTOMER("CUSTOMER", "客服号"),
	TUTOR("TUTOR", "导师");

	private String code;
	private String description;
	
	private EChatRole(String code, String description) {
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
