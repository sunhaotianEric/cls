package com.team.lottery.enums;

/**
 *红包状态
 * @author Owner
 *
 */
public enum EChatRedBagType {

	RANDOM("RANDOM", "拼手气红"), 
	AVERAGE("AVERAGE", "平均红包");

	private String code;
	private String description;

	private EChatRedBagType(String code, String description) {
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
