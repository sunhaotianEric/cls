package com.team.lottery.system.message.messagetype;

import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.handler.ChatBanMessageHandler;
import com.team.lottery.vo.ChatBan;

public class ChatBanMessage extends BaseMessage {


	/**
	 * 
	 */
	private static final long serialVersionUID = -8208413082642481317L;

	private ChatBan chatBan;
	

	public ChatBan getChatBan() {
		return chatBan;
	}


	public void setChatBan(ChatBan chatBan) {
		this.chatBan = chatBan;
	}


	public ChatBanMessage() {
		this.handler=new ChatBanMessageHandler();
	}

	
}
