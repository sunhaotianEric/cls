package com.team.lottery.system.message.handler;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.cache.ChatCacheManager;
import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.BaseMessageHandler;
import com.team.lottery.system.message.messagetype.ChatBanMessage;

public class ChatBanMessageHandler extends BaseMessageHandler implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6560943204227426245L;
	private static Logger logger = LoggerFactory.getLogger(ChatBanMessageHandler.class);
//	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@Override
	public void dealMessage(BaseMessage message) {
		try {
			logger.info("接受redis订阅消息，开始更新插入聊天室黑名单...");
			ChatBanMessage chatBanMessage=(ChatBanMessage) message;
			ChatCacheManager.updateChatBanCache(chatBanMessage.getChatBan(), chatBanMessage.getType());
			
		} catch (Exception e) {
			logger.error("处理插入聊天室黑名单发生错误...", e);
		}
	}

}
