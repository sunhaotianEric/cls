package com.team.lottery.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import com.team.lottery.cache.ChatCacheManager;
import com.team.lottery.service.ChatLevelAuthService;
import com.team.lottery.service.ChatRedbagInfoService;
import com.team.lottery.service.ChatRedbagService;
import com.team.lottery.service.ChatUserSendredbagAuthService;
import com.team.lottery.vo.ChatBan;
import com.team.lottery.vo.ChatLevelAuth;
import com.team.lottery.vo.ChatRedbag;
import com.team.lottery.vo.ChatRedbagInfo;
import com.team.lottery.vo.ChatUserSendredbagAuth;
import com.team.lottery.vo.User;

import net.sf.json.JSONObject;

public class ChatUtil {
	
	public static SimpleDateFormat sdf = new SimpleDateFormat("HH");

	@Autowired
	private static ChatUserSendredbagAuthService userSendredbagAuthService = ApplicationContextUtil.getBean(ChatUserSendredbagAuthService.class);
	@Autowired
	private static ChatRedbagInfoService chatRedbagInfoService = ApplicationContextUtil.getBean(ChatRedbagInfoService.class);
	@Autowired
	private static ChatRedbagService chatRedbagService = ApplicationContextUtil.getBean(ChatRedbagService.class);
	@Autowired
	private static ChatLevelAuthService chatLevelAuthService = ApplicationContextUtil.getBean(ChatLevelAuthService.class);
	
	/**
	 * 获取访客
	 * @return
	 */
	public static String GetVisitorName(HttpServletRequest request, String bizSystem, String roomId) {
		Map<String, String> map = new HashMap<String, String>();
		User user = (User) request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
		String ipAdress = IPUtil.getIpAddr(request);
		if (user != null) {
			// 判断登陆的会员是不是拥有发红包权限
			ChatUserSendredbagAuth auth = new ChatUserSendredbagAuth();
			auth.setBizSystem(bizSystem);
			auth.setRoomid(Integer.parseInt(roomId));
			auth.setUserName(user.getUserName());
			// 获取每日红包限额
			auth = userSendredbagAuthService.getChatUserSendredbagAuth(auth);
			String Jurisdiction = "0";
			if (auth != null) {
				Jurisdiction = "1";
			}
			// 获取已抢红包chatlogid
			ChatRedbagInfo record = new ChatRedbagInfo();
			record.setBizSystem(bizSystem);
//			record.setRoomId(roomId);
			record.setUserName(user.getUserName());
			List<ChatRedbagInfo> recordlist = chatRedbagInfoService.getOverdueRedEnvelopes(record);
			String redInfoChatid = "";
			for (ChatRedbagInfo chatRedbagInfo : recordlist) {
//				redInfoChatid += "," + chatRedbagInfo.getChatlogId();
			}
			if (redInfoChatid != "") {
				redInfoChatid += ",";
			}
			// 获取未过期红包chatlogid
			ChatRedbag redbag = new ChatRedbag();
			redbag.setBizSystem(bizSystem);
//			redbag.setRoomId(roomId);
			List<ChatRedbag> redbaglist = chatRedbagService.getChatRedBagBytime(redbag);
			String redbagtime = "";
//			for (ChatRedbag chatRedbag : redbaglist) {
//				redbagtime += "," + chatRedbag.getChatlogId();
//			}
			if (redbagtime != "") {
				redbagtime += ",";
			}
			// 获取当前房间内已抢光的红包
			List<ChatRedbag> redbagCountlist = chatRedbagService.getChatRedBagByCount(redbag);
			String redbagCount = "";
//			for (ChatRedbag chatRedbag : redbagCountlist) {
//				redbagCount += "," + chatRedbag.getChatlogId();
//			}
//			if (redbagCount != "") {
//				redbagCount += ",";
//			}
			// 获取用户的发言权限
			String levelAuth = "0";
			String levelAuthRedbag = "0";
			String levelAuthImg = "0";
			ChatLevelAuth query = new ChatLevelAuth();
			query.setBizSystem(bizSystem);
			query.setLevel(Integer.parseInt(user.getVipLevel()));
			query = chatLevelAuthService.getAllChatLevel(query);
			if (query != null) {
				if (query.getRules().indexOf("msg_send") > -1) {
					levelAuth = "1";
				}
				if (query.getRules().indexOf("sendRedBag") > -1) {
					levelAuthRedbag = "1";
				}
				if (query.getRules().indexOf("sendImg") > -1) {
					levelAuthImg = "1";
				}
			}
			// 查询是否被禁言
			String chatban = "0";
			ChatBan ban = new ChatBan();
			ban.setBizSystem(bizSystem);
			ban.setRoomId(Integer.parseInt(roomId));
			if (ChatCacheManager.getChatBan(ban) != null) {
				String judge = ChatCacheManager.getChatBan(ban);
				if (judge.indexOf("," + user.getUserName() + ",") > -1 || judge.indexOf("," + ipAdress + ",") > -1) {
					chatban = "1";
				}
			}

			String headImg = user.getHeadImg();
			if (headImg == null || "".equals(headImg)) {
				headImg = "images/user/default.png";
			} else {
				if (user.getHeadImg().startsWith("../")) {
					headImg = headImg.substring(6);
				}
			}
			map.put("chatid", user.getId() + "");
			map.put("bizSystem", bizSystem);
			map.put("roomid", roomId);
			map.put("nick", user.getNick());
			map.put("userName", user.getUserName());
			map.put("touxiang", headImg);
			map.put("state", "1");// 状态是1 是会员
			map.put("vip", user.getVipLevel());
			map.put("vipName", user.getLevelName());
			map.put("ip", ipAdress);
			map.put("levelAuth", levelAuth); // 发言权限 1为允许
			map.put("chatban", chatban); // 是否禁言 1为禁言
			map.put("redbag", Jurisdiction); // 发红包权限1为有权限 0为无权限视为用户
			map.put("levelAuthRedbag", levelAuthRedbag); // 个人用户发红包权限1为有权限 0为无权限
			map.put("levelAuthImg", levelAuthImg); // 个人用户发图片权限1为有权限 0为无权限
			map.put("redbagtime", redbagtime); // 未过期的红包chatlogid
			map.put("redbagCount", redbagCount); // 已抢光的红包chatlogid
			map.put("redInfoChatid", redInfoChatid); // 领取过的红包chatlogid
		} else {
			String chatid = (String) request.getSession().getAttribute(bizSystem + "chatid");
			// 之前有生成了,不重新生成
			if (chatid == null) {
				Date d = new Date();
				chatid = sdf.format(new Date()) + MD5PasswordUtil.GetMD5Code(d.getTime() + "").substring(0, 6).toUpperCase();
				request.getSession().setAttribute(bizSystem + "chatid", chatid);
			}
			// 获取用户的发言权限
			String levelAuth = "0";
			String levelAuthRedbag = "0";
			String levelAuthImg = "0";
			ChatLevelAuth query = new ChatLevelAuth();
			query.setBizSystem(bizSystem);
			query.setLevel(0);
			query = chatLevelAuthService.getAllChatLevel(query);
			if (query != null) {
				if (query.getRules().indexOf("msg_send") > -1) {
					levelAuth = "1";
				}
				if (query.getRules().indexOf("sendRedBag") > -1) {
					levelAuthRedbag = "1";
				}
				if (query.getRules().indexOf("sendImg") > -1) {
					levelAuthImg = "1";
				}
			}
			// 查询是否被禁言
			String chatban = "0";
			ChatBan ban = new ChatBan();
			ban.setBizSystem(bizSystem);
			ban.setRoomId(Integer.parseInt(roomId));
			if (ChatCacheManager.getChatBan(ban) != null) {
				String judge = ChatCacheManager.getChatBan(ban);
				if (judge.indexOf("," + chatid + ",") > -1 || judge.indexOf("," + ipAdress + ",") > -1) {
					chatban = "1";
				}
			}

			map.put("chatid", chatid);
			map.put("bizSystem", bizSystem);
			map.put("roomid", roomId);
			map.put("nick", "游客" + chatid);
			map.put("userName", chatid);
			map.put("touxiang", "images/smallheadimg/0.png");
			map.put("state", "0");// 状态是1 是会员
			map.put("vip", "0");
			map.put("vipName", "游客");
			map.put("ip", ipAdress);
			map.put("levelAuth", levelAuth);// 发言权限 1为允许
			map.put("chatban", chatban); // 是否禁言 1为禁言
			map.put("levelAuthRedbag", levelAuthRedbag); // 个人用户发红包权限1为有权限 0为无权限
			map.put("levelAuthImg", levelAuthImg); // 个人用户发图片权限1为有权限 0为无权限
			map.put("redbag", "0");
			map.put("redbagtime", "");
			map.put("redbagCount", "");
			map.put("redInfoChatid", "");
		}
		JSONObject obj = JSONObject.fromObject(map);
		return obj.toString();
	}
}
