package com.team.lottery.util;

import java.util.List;

import com.team.lottery.enums.EChatRole;
import com.team.lottery.vo.ChatRoleUser;

/**
 * 用户校验权限工具类
 * @author Owner
 *
 */
public class ChatRoleUserUtil {

	/**
	 * 当前用户权限校验
	 * 
	 * @param currentUsername
	 *            当前用户名
	 * @param username
	 *            被拉黑的用户名
	 * @param roomId
	 * @param bizSystem
	 * @return
	 */
	public static boolean currentUserRoleCheck(ChatRoleUser chatRoleUser) {
		String role = chatRoleUser.getRole();
		// 当前不为群主或者不为管理者无权限
		if (role.equals(EChatRole.GROUPOWNER.getCode()) || role.equals(EChatRole.MANAGER.getCode())) {
			return true;
		}
		return false;
	}

	/**
	 * 被添加用户权限校验
	 * 
	 * @param currentUsername
	 *            当前用户名
	 * @param username
	 *            被拉黑的用户名
	 * @param roomId
	 * @param bizSystem
	 * @return
	 */
	public static boolean addedUserRoleUserCheck(ChatRoleUser userRole) {
		String role = userRole.getRole();
		// 已经是群主，管理员，导师，客服的角色不能添加进入禁言列表
		if (role.equals(EChatRole.GROUPOWNER.getCode()) || !role.equals(EChatRole.MANAGER.getCode())
				|| role.equals(EChatRole.ORDINARYUSERS.getCode()) || role.equals(EChatRole.CUSTOMER.getCode())) {
			return false;
		}
		return true;
	}

	/**
	 * 被添加用户权限批量校验
	 * @param userRole
	 * @return
	 */
	public static boolean addedUserRoleUserChecks(List<ChatRoleUser> userRole) {
		String role = null;
		for (ChatRoleUser chatRoleUser : userRole) {
			role = chatRoleUser.getRole();
			// 已经是群主，管理员，导师，客服的角色不能添加进入禁言列表
			if (role.equals(EChatRole.GROUPOWNER.getCode()) || !role.equals(EChatRole.MANAGER.getCode())
					|| role.equals(EChatRole.ORDINARYUSERS.getCode()) || role.equals(EChatRole.CUSTOMER.getCode())) {
				return false;
			}
		}
		return true;
	}
}
