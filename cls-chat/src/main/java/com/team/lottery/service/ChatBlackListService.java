package com.team.lottery.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.mapper.chatblacklist.ChatBlackListMapper;
import com.team.lottery.vo.ChatBlackList;

@Service("chatBlackListService")
public class ChatBlackListService {
	
	@Autowired
	private ChatBlackListMapper chatBlackListMapper;
	
	/**
	 * 查询唯一黑名单
	 * @param userId
	 * @param roomId
	 * @param bizSystem
	 * @return
	 */
	public ChatBlackList getByCondition(String userName,Integer roomId,String bizSystem){
		return chatBlackListMapper.getByCondition(userName, roomId, bizSystem);
	}
	
	/**
	 * 插入数据
	 * @param record
	 * @return
	 */
	@Transactional(readOnly=false,rollbackFor=Exception.class)
	public int insertSelective(ChatBlackList record){
		return chatBlackListMapper.insertSelective(record);
	}
	
	/**
	 * 查询当前系统的某房间黑名单数据列表
	 * @param roomId
	 * @param bizSystem
	 * @return
	 */
	public List<ChatBlackList> getListByCondition(Integer roomId,String bizSystem){
		return chatBlackListMapper.getListByCondition(roomId, bizSystem);
	}
	
	/**
	 * 根据用户名查询
	 * 
	 * @param bizSystem
	 * @param userName
	 * @return
	 */
	public ChatBlackList selectByUserName(String bizSystem, String userName) {
		return chatBlackListMapper.selectByUserName(bizSystem, userName);
	}
	
	public int deleteByPrimaryKey(Integer id){
		return chatBlackListMapper.deleteByPrimaryKey(id);
	}

	/**
	 * 批量删除
	 * @param Id
	 * @return
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int deleteChatBlackList(List<Integer> Id){
		return chatBlackListMapper.deleteChatBlackList(Id);
	}
	
	/**
	 * 批量查询
	 * @param userName
	 * @param roomId
	 * @param bizSystem
	 * @return
	 */
	public List<ChatBlackList> getAllByCondition(List<String> userName,Integer roomId,String bizSystem){
		return chatBlackListMapper.getAllByCondition(userName, roomId, bizSystem);
	}
}
