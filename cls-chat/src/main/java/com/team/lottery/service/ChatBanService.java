package com.team.lottery.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.chatban.ChatBanMapper;
import com.team.lottery.vo.ChatBan;

@Service("chatBanService")
public class ChatBanService {

	@Autowired
	private ChatBanMapper chatBanMapper;

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int deleteByPrimaryKey(Integer id) {
		return chatBanMapper.deleteByPrimaryKey(id);
	}

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int insertSelective(ChatBan record) {
		return chatBanMapper.insertSelective(record);
	}

	public ChatBan selectByPrimaryKey(Integer id) {
		return chatBanMapper.selectByPrimaryKey(id);
	}

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int updateByPrimaryKeySelective(ChatBan record) {
		return chatBanMapper.updateByPrimaryKeySelective(record);
	}

	/**
	 * 查找所有的数据
	 * 
	 * @param page
	 * @param query
	 * @return
	 */
	public Page getAllChatBanQueryPage(ChatBan query, Page page) {
		page.setTotalRecNum(chatBanMapper.getAllChatBanQueryPageCount(query));
		page.setPageContent(chatBanMapper.getAllChatBanQueryPage(query, page.getStartIndex(), page.getPageSize()));
		return page;
	}

	/**
	 * 查找所有禁言对象
	 * 
	 * @return
	 */
	public List<ChatBan> getAllChatBan(ChatBan query) {
		return chatBanMapper.getAllChatBan(query);
	}
	
	/**
	 * 根据用户名查询
	 * 
	 * @param bizSystem
	 * @param userName
	 * @return
	 */
	public ChatBan selectByUserName(String bizSystem, String userName) {
		return chatBanMapper.selectByUserName(bizSystem, userName);
	}
	
	/**
   	 * 根据业务系统查询所有的
   	 * @param bizSystem
   	 * @return
   	 */
	public List<ChatBan> getChatBan(Integer roomId, String bizSystem){
		return chatBanMapper.getChatBan(roomId, bizSystem);
	}
	
	/**
	 * 查询唯一
	 * @param userName
	 * @param roomId
	 * @param bizSystem
	 * @return
	 */
	public ChatBan getByOnlyChatBan(String userName,Integer roomId,String bizSystem){
		return chatBanMapper.getByOnlyChatBan(userName, roomId, bizSystem);
	}
	
	/**
	 * 批量删除
	 * @param Id
	 * @return
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int deleteChatBan(List<Integer> Id){
		return chatBanMapper.deleteChatBan(Id);
	}

	/**
	 * 批量查询
	 * @param userName
	 * @param roomId
	 * @param bizSystem
	 * @return
	 */
	public List<ChatBan> getByOnlyChatBans(List<String> userName,Integer roomId,String bizSystem){
		return chatBanMapper.getByOnlyChatBans(userName, roomId, bizSystem);
	}
}
