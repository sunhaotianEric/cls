package com.team.lottery.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.chatroom.ChatRoomMapper;
import com.team.lottery.vo.ChatRoom;
import com.team.lottery.vo.User;

@Service("chatRoomService")
public class ChatRoomService {

	@Autowired
	private ChatRoomMapper chatRoomMapper;

	/**
	 * 分页查询聊天室
	 * 
	 * @param chatRoom
	 * @param page
	 * @return
	 */
	public Page getChatRoom(ChatRoom chatRoom, Page page) {
		page.setPageContent(chatRoomMapper.getChatRoom(chatRoom, page.getStartIndex(), page.getPageSize()));
		page.setTotalRecNum(chatRoomMapper.getChatRoomCount(chatRoom));
		return page;
	}

	/**
	 * 查询是否存在相同的roomid
	 * 
	 * @param chatRoom
	 * @return
	 */
	public List<ChatRoom> getChatRoomByRoomId(ChatRoom chatRoom) {
		return chatRoomMapper.getChatRoomByRoomId(chatRoom);
	}

	/**
	 * 根据ID查询单个聊天室信息
	 * 
	 * @param id
	 * @return
	 */
	public ChatRoom getChatRoomById(Long id) {
		return chatRoomMapper.selectByPrimaryKey(id);
	}

	/**
	 * 添加聊天室数据
	 * 
	 * @param record
	 * @return
	 */
	public int saveChatRoom(ChatRoom record) {
		record.setCreateDate(new Date());
		return chatRoomMapper.insertSelective(record);
	}

	/**
	 * 根据ID删除聊天室数据
	 * 
	 * @param id
	 * @return
	 */
	public int delChatRoom(Long id) {
		return chatRoomMapper.deleteByPrimaryKey(id);
	}

	/**
	 * 修改聊天室数据
	 * 
	 * @param record
	 * @return
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int updateChatRoomByid(ChatRoom record) {
		record.setUpdateDate(new Date());
		return chatRoomMapper.updateByPrimaryKeySelective(record);
	}

	/**
	 * 根据条件获取
	 * 
	 * @param chatRoom
	 * @return
	 */
	public List<ChatRoom> getChatRoomList(ChatRoom chatRoom) {
		return chatRoomMapper.getChatRoomList(chatRoom);
	}

	/**
	 * 根据业务系统和
	 * 
	 * @param chatRoom
	 * @return
	 */
	public ChatRoom getChatRoomByBizSystem(ChatRoom chatRoom) {
		List<ChatRoom> list = getChatRoomList(chatRoom);
		if (list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

	/**
	 * 根据业务系统 查询房间
	 * 
	 * @param chatRoom
	 * @param root
	 *            为1时查询上级代理房间 0时查询该业务系统的超级代理聊天室
	 * @param user
	 * @return
	 */
	public ChatRoom getChatRoomOnUser(ChatRoom chatRoom, Integer root, User user) {
		try {
			ChatRoom room = new ChatRoom();
			if (root == 1) {
				if (!user.getDailiLevel().equals("ORDINARYAGENCY") && !user.getDailiLevel().equals("REGULARMEMBERS")) {
					user.setRegfrom(user.getRegfrom() + user.getUserName() + "&");
				}
				String[] str = user.getRegfrom().split("&");
				for (int i = str.length - 1; i >= 0; i--) {
					chatRoom.setAgent(str[i]);
					room = chatRoomMapper.getChatRoomOnUser(chatRoom);
					if (room != null) {
						break;
					}
				}
			} else {
				room = chatRoomMapper.getChatRoomOnUser(chatRoom);
			}
			return room;
		} catch (Exception e) {
			// 防止程序错误时系统崩溃
			return null;
		}
	}
	
	/**
     * 根据房间id和系统查询房间信息
     * @param roomId
     * @param bizSystem
     * @return
     */
	public ChatRoom getChatRoomByRoomIds(Integer roomId, String bizSystem) {
		return chatRoomMapper.getChatRoomByRoomIds(roomId, bizSystem);
	}
	
	/**
	 * 根据会员级别查询
	 * @param roomId
	 * @param bizSystem
	 * @param vipRemind
	 * @return
	 */
	public List<ChatRoom> getChatRoomByVipRemind(Integer roomId, String bizSystem, List<Integer> vipRemind){
		return chatRoomMapper.getChatRoomByVipRemind(roomId, bizSystem, vipRemind);
		
	}
}
