package com.team.lottery.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.enums.EMoneyDetailType;
import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.chatredbaginfo.ChatRedbagInfoMapper;
import com.team.lottery.mapper.user.UserMapper;
import com.team.lottery.util.MakeOrderNum;
import com.team.lottery.vo.ChatRedbagInfo;
import com.team.lottery.vo.MoneyDetail;
import com.team.lottery.vo.User;

@Service("chatRedbagInfoService")
public class ChatRedbagInfoService {

	@Autowired
	public ChatRedbagInfoMapper chatRedbagInfoMapper;
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private UserService userService;
	@Autowired
	private UserMoneyTotalService userMoneyTotalService;
	@Autowired
	private MoneyDetailService moneyDetailService;

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int insertSelective(ChatRedbagInfo record) {
		return chatRedbagInfoMapper.insertSelective(record);

	}

	/**
	 * 根据ID修改
	 * 
	 * @param record
	 * @return
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int updateByPrimaryKeySelective(ChatRedbagInfo record) {
		return chatRedbagInfoMapper.updateByPrimaryKeySelective(record);
	}

	/**
	 * 根据ID查询
	 * 
	 * @param id
	 * @return
	 */
	public ChatRedbagInfo selectByPrimaryKey(int id) {
		return chatRedbagInfoMapper.selectByPrimaryKey(id);
	}

	/**
	 * 新增
	 * 
	 * @param chatroomConfig
	 * @return
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int insertSelective(ChatRedbagInfo record, Integer chatlogid, Integer receivedCount) {
		return chatRedbagInfoMapper.insertSelective(record);
	}

	/**
	 * 查询用户已领取的红包
	 * 
	 * @param record
	 * @return
	 */
	public List<ChatRedbagInfo> getOverdueRedEnvelopes(ChatRedbagInfo record) {
		return chatRedbagInfoMapper.getOverdueRedEnvelopes(record);
	}

	/**
	 * 分页查询
	 * 
	 * @param record
	 * @param page
	 * @param endTime
	 * @return
	 */
	public Page getChatRedbaginfoPage(ChatRedbagInfo record, Page page) {
		page.setPageContent(
				chatRedbagInfoMapper.getChatRedbaginfoPage(record, page.getStartIndex(), page.getPageSize()));
		page.setTotalRecNum(chatRedbagInfoMapper.getChatRedbaginfoPageCount(record));
		return page;
	}

	/**
	 * 查询符合条件的记录条数
	 * 
	 * @param record
	 * @return
	 */
	public int getChatRedbaginfoPageCount(ChatRedbagInfo record) {
		return chatRedbagInfoMapper.getChatRedbaginfoPageCount(record);
	}

	/**
	 * 根据Rid查询数据
	 * 
	 * @param rbid
	 * @return
	 */
	public List<ChatRedbagInfo> getChatRedbaginfoByrid(Integer rbid) {
		return chatRedbagInfoMapper.getChatRedbaginfoByrid(rbid);
	}

	public List<ChatRedbagInfo> getChatRedbaginfoByChatRedbagId(Integer roomId, Integer chatRedbagId) {
		return chatRedbagInfoMapper.getChatRedbaginfoByChatRedbagId(roomId, chatRedbagId);
	}

	/**
	 * 抢红包的资金明细
	 * @param chatRedbagInfo
	 * @param user
	 * @param money
	 * @throws Exception
	 * @throws Exception
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void robChatRedbagMoney(ChatRedbagInfo chatRedbagInfo, BigDecimal money)
			throws Exception, Exception {
		Integer userId = chatRedbagInfo.getUserId();
		User operationUser = userMapper.selectByPrimaryKey(userId);

		if (operationUser == null) {
			return;
		}
		this.insertSelective(chatRedbagInfo);
		// 将用户抢到的红包金额加上用户本身所拥有的金额
		operationUser.addMoney(money, false);
		// 游客 领取就是无效订单
		Integer enabled = operationUser.getIsTourist() == 1 ? 0 : 1;

		MoneyDetail moneyDetail = new MoneyDetail();
		moneyDetail.setEnabled(enabled);
		moneyDetail.setUserId(operationUser.getId()); // 用户id
		moneyDetail.setOrderId(null); // 订单id
		moneyDetail.setUserName(operationUser.getUserName()); // 用户名
		moneyDetail.setBizSystem(operationUser.getBizSystem());
		moneyDetail.setOperateUserName(operationUser.getUserName()); // 操作人员
		// 订单单号
		String newOrderId = MakeOrderNum.makeOrderNum(MakeOrderNum.CJ);
		moneyDetail.setLotteryId(newOrderId);
		moneyDetail.setWinLevel(null);
		moneyDetail.setWinCost(null);
		moneyDetail.setWinCostRule(null);

		// 资金明细
		moneyDetail.setDetailType(EMoneyDetailType.ACTIVITIES_MONEY.name());
		moneyDetail.setIncome(chatRedbagInfo.getMoney());
		moneyDetail.setPay(new BigDecimal("0"));
		moneyDetail.setBalanceBefore(operationUser.getMoney()); // 收入前金额
		// 活动彩金
		operationUser.addMoney(chatRedbagInfo.getMoney(), false);
		moneyDetail.setBalanceAfter(operationUser.getMoney()); // 收入后的金额
		moneyDetail.setDescription(chatRedbagInfo.getUserName() + "抢了红包" + chatRedbagInfo.getMoney());

		userService.updateByPrimaryKeyWithVersionSelective(operationUser);
		moneyDetailService.insertSelective(moneyDetail); // 保存资金明细
		// 最后执行 增加用户总金额 线程问题
		userMoneyTotalService.addUserTotalMoneyByType(operationUser, EMoneyDetailType.ACTIVITIES_MONEY,
				chatRedbagInfo.getMoney());
	}
}
