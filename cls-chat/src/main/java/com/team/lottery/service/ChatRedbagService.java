package com.team.lottery.service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.enums.EMoneyDetailType;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.UserTotalMoneyByTypeVo;
import com.team.lottery.mapper.chatredbag.ChatRedbagMapper;
import com.team.lottery.mapper.chatredbaginfo.ChatRedbagInfoMapper;
import com.team.lottery.mapper.user.UserMapper;
import com.team.lottery.util.MakeOrderNum;
import com.team.lottery.vo.ChatRedbag;
import com.team.lottery.vo.MoneyDetail;
import com.team.lottery.vo.User;

@Service
public class ChatRedbagService {
	
	@Autowired
	public ChatRedbagMapper chatRedbagMapper;
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private UserService userService;
	@Autowired
	private UserMoneyTotalService userMoneyTotalService;
	@Autowired
	private MoneyDetailService moneyDetailService;
	@Autowired
	public ChatRedbagInfoMapper chatRedbagInfoMapper;

	/**
	 * 根据ID
	 * 
	 * @param record
	 * @return
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int updateByPrimaryKeySelective(ChatRedbag record) {
		return chatRedbagMapper.updateByPrimaryKeySelective(record);
	}

	/**
	 * 根据ID查询
	 * 
	 * @param id
	 * @return
	 */
	public ChatRedbag selectByPrimaryKey(int id) {
		return chatRedbagMapper.selectByPrimaryKey(id);
	}

	/**
	 * 新增
	 * 
	 * @param chatroomConfig
	 * @return
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int insertSelective(ChatRedbag record) {
		return chatRedbagMapper.insertSelective(record);
	}

	/**
	 * 统计今天发红包总数
	 * 
	 * @param record
	 * @return
	 */
	public ChatRedbag getChatRedBagBySumMoney(ChatRedbag record) {
		return chatRedbagMapper.getChatRedBagBySumMoney(record);
	}

	/**
	 * 以当前时间为基准查询-1天以内的红包数据
	 * 
	 * @param record
	 * @return
	 */
	public List<ChatRedbag> getChatRedBagBytime(ChatRedbag record) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, -1);
		date = calendar.getTime();
		try {
			record.setCreateTime(sdf.parse(sdf.format(date)));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return chatRedbagMapper.getChatRedBagBytime(record);
	}

	/**
	 * 查询已抢光的红包
	 * 
	 * @param record
	 * @return
	 */
	public List<ChatRedbag> getChatRedBagByCount(ChatRedbag record) {
		return chatRedbagMapper.getChatRedBagByCount(record);
	}

	/**
	 * 分页查询
	 * 
	 * @param record
	 * @param page
	 * @param endTime
	 * @return
	 */
	public Page getChatRedbagPage(ChatRedbag record, Page page) {
		page.setPageContent(chatRedbagMapper.getChatRedbagPage(record, page.getStartIndex(), page.getPageSize()));
		page.setTotalRecNum(chatRedbagMapper.getChatRedbagPageCount(record));
		return page;
	}

	/**
	 * 查询已过期红包及以领取金额
	 * 
	 * @param createTime
	 * @return
	 */
	public List<ChatRedbag> getChatRedbagByStatus(Date createTime) {
		return chatRedbagMapper.getChatRedbagByStatus(createTime);
	}

	/**
	 * 根据条件查询
	 * 
	 * @param bizSystem
	 * @param userName
	 * @return
	 */
	public ChatRedbag getChatRedBag(String currentSystem, String username, String type) {
		return chatRedbagMapper.getChatRedBagBySystemAndUserName(currentSystem, username, type);
	}
	
	/**
	 * 红包过期，退还未被领取的剩余金额
	 * 
	 * @param userName
	 * @param bizSystem
	 * @param transferMoney
	 * @return
	 * @throws Exception
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int saveRedbagReturn(String userName, String bizSystem, BigDecimal transferMoney) throws Exception {
		// TODO
		User transUser = userService.getUserByName(bizSystem, userName);
		if (transUser == null) {
			return 0;
		}
		MoneyDetail transUserMoneyDetail = new MoneyDetail();
		transUserMoneyDetail.setEnabled(1);
		transUserMoneyDetail.setUserId(transUser.getId()); // 用户id
		transUserMoneyDetail.setOrderId(null); // 订单id
		transUserMoneyDetail.setBizSystem(bizSystem);
		transUserMoneyDetail.setUserName(transUser.getUserName()); // 用户名
		transUserMoneyDetail.setOperateUserName("superAdmin"); // 操作人员
		transUserMoneyDetail.setLotteryId(MakeOrderNum.makeOrderNum(MakeOrderNum.TS)); // 订单单号
		transUserMoneyDetail.setWinLevel(null);
		transUserMoneyDetail.setWinCost(null);
		transUserMoneyDetail.setWinCostRule(null);
		transUserMoneyDetail.setDetailType(EMoneyDetailType.INCOME_TRANFER.name());
		transUserMoneyDetail.setIncome(transferMoney);
		transUserMoneyDetail.setPay(new BigDecimal(0));
		transUserMoneyDetail.setBalanceBefore(transUser.getMoney()); // 支出前金额
		transUserMoneyDetail.setBalanceAfter(transUser.getMoney().add(transferMoney)); // 支出后的金额
		transUserMoneyDetail.setDescription("退还过期红包未被领取金额:" + transferMoney);
		moneyDetailService.insertSelective(transUserMoneyDetail); // 保存资金明细
		// 转账转入增加用户余额
		transUser.addMoney(transferMoney, false); // 余额增加
		// 针对盈亏报表多线程不会事物回滚处理方式
		UserTotalMoneyByTypeVo userTotalMoneyByTypeVo = new UserTotalMoneyByTypeVo(transUser, EMoneyDetailType.INCOME_TRANFER, transferMoney);
		userService.updateByPrimaryKeyWithVersionSelective(transUser);
		int i = userMoneyTotalService.addUserTotalMoneyByType(userTotalMoneyByTypeVo.getUser(), userTotalMoneyByTypeVo.getDetailType(), userTotalMoneyByTypeVo.getOperationValue());
		return i;
	}


	/**
	 * 发红包资金明细
	 * @param chatRedbag
	 * @param user
	 * @param redmoney
	 * @throws Exception
	 * @throws Exception
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void giveChatRedbagMoney(ChatRedbag chatRedbag ,BigDecimal redmoney) throws Exception, Exception {
		Integer userId = chatRedbag.getUserId();
		User operationUser = userMapper.selectByPrimaryKey(userId);

		if (operationUser == null) {
			return;
		}
		this.insertSelective(chatRedbag);
		int id = chatRedbag.getId();
		chatRedbag = this.selectByPrimaryKey(id);
		// 将用户本身所拥有的金额减去用户发出的红包金额
		operationUser.reduceMoney(redmoney, false);
		// 游客 领取就是无效订单
		Integer enabled = operationUser.getIsTourist() == 1 ? 0 : 1;

		MoneyDetail moneyDetail = new MoneyDetail();
		moneyDetail.setEnabled(enabled);
		moneyDetail.setUserId(operationUser.getId()); // 用户id
		moneyDetail.setOrderId(null); // 订单id
		moneyDetail.setUserName(operationUser.getUserName()); // 用户名
		moneyDetail.setBizSystem(operationUser.getBizSystem());
		moneyDetail.setOperateUserName(operationUser.getUserName()); // 操作人员
		// 订单单号
		String newOrderId = MakeOrderNum.makeOrderNum(MakeOrderNum.CJ);
		moneyDetail.setLotteryId(newOrderId);
		moneyDetail.setWinLevel(null);
		moneyDetail.setWinCost(null);
		moneyDetail.setWinCostRule(null);

		// 资金明细
		moneyDetail.setDetailType(EMoneyDetailType.ACTIVITIES_MONEY.name());
		moneyDetail.setIncome(chatRedbag.getMoney());
		moneyDetail.setPay(new BigDecimal("0"));
		moneyDetail.setBalanceBefore(operationUser.getMoney()); // 收入前金额
		// 活动彩金
//		operationUser.addMoney(chatRedbag.getMoney(), false);
		moneyDetail.setBalanceAfter(operationUser.getMoney()); // 收入后的金额
		moneyDetail.setDescription(chatRedbag.getUserName() + "发送了红包" + chatRedbag.getMoney());
//		int i = 1/0;
		userService.updateByPrimaryKeyWithVersionSelective(operationUser);
		moneyDetailService.insertSelective(moneyDetail); // 保存资金明细
		// 最后执行 增加用户总金额 线程问题
//		int i = 1/0;
		userMoneyTotalService.addUserTotalMoneyByType(operationUser, EMoneyDetailType.ACTIVITIES_MONEY,
				chatRedbag.getMoney());
	}
}
