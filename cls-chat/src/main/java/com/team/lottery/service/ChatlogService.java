package com.team.lottery.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.chatlog.ChatlogMapper;
import com.team.lottery.vo.Chatlog;
import com.team.lottery.vo.User;

@Service("chatlogService")
public class ChatlogService {

	@Autowired
	private ChatlogMapper chatlogMapper;

	public int deleteByPrimaryKey(Long id) {
		return chatlogMapper.deleteByPrimaryKey(id);
	}

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int insertSelective(Chatlog record) {
		return chatlogMapper.insertSelective(record);
	}

	public Chatlog selectByPrimaryKey(Long id) {
		return chatlogMapper.selectByPrimaryKey(id);
	}

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int updateByPrimaryKeySelective(Chatlog record) {
		return chatlogMapper.updateByPrimaryKeySelective(record);
	}

	/**
	 * 获取历史记录 最近的30条
	 * 
	 * @param query
	 * @param page
	 * @return
	 */
	public List<Chatlog> getHisChatLogPage(Chatlog query, Page page) {
		if (page == null) {
			page = new Page();
			page.setPageSize(30);
		}
		return chatlogMapper.getHisChatLogPage(query, page.getStartIndex(), page.getPageSize());
	}

	/**
	 *
	 * @param query
	 * @param page
	 * @return
	 */
	public Page getAllChatLogPage(Chatlog query, Page page) {
		page.setTotalRecNum(chatlogMapper.getAllChatLogPageCount(query));
		page.setPageContent(chatlogMapper.getAllChatLogPage(query, page.getStartIndex(), page.getPageSize()));
		return page;
	}

	/**
	 * 根据条件分页查询
	 * 
	 * @param query
	 * @param page
	 * @return
	 */
	public Page getChatLogPage(Chatlog query, Page page) {
		page.setTotalRecNum(chatlogMapper.getChatLogCount(query));
		page.setPageContent(chatlogMapper.getChatLog(query, page.getStartIndex(), page.getPageSize()));
		return page;
	}

	/**
	 * 修改记录里用户的昵称
	 * 
	 * @param query
	 * @return
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public Integer updateChatLogByUser(User query) {
		return chatlogMapper.updateChatLogByUser(query);
	}
	
	public Chatlog selectByUsername(String username){
		return chatlogMapper.selectByUsername(username);
		
	}
}
