package com.team.lottery.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.chatroomconfig.ChatroomConfigMapper;
import com.team.lottery.vo.ChatroomConfig;

@Service("chatRoomConfigService")
public class ChatRoomConfigService {

	@Autowired
	public ChatroomConfigMapper chatroomConfigMapper;
	
	/**
	 * 分页查询数据
	 * @param chatroomConfig
	 * @param page
	 * @return
	 */
	public Page getAllChatRoomConfig(ChatroomConfig chatroomConfig,Page page){
		page.setTotalRecNum(chatroomConfigMapper.getAllChatRoomConfigCount(chatroomConfig));
		page.setPageContent(chatroomConfigMapper.getAllChatRoomConfig(chatroomConfig, page.getStartIndex(),page.getPageSize()));
		return page;
	}
	
	/**
	 * 根据ID修改数据库配置
	 * @param record
	 * @return
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int updateByPrimaryKeySelective(ChatroomConfig record){
		return chatroomConfigMapper.updateByPrimaryKeySelective(record);
	}
	
	/**
	 * 根据ID查询单个数据库配置
	 * @param id
	 * @return
	 */
	public ChatroomConfig selectByPrimaryKey(int id){
		return chatroomConfigMapper.selectByPrimaryKey(id);
	}
	
	/**
	 * 根据业务系统查询数据库配置
	 * @param chatroomConfig
	 * @return
	 */
	public ChatroomConfig getChatRoomConfig(ChatroomConfig chatroomConfig){
		return chatroomConfigMapper.getChatRoomConfig(chatroomConfig);
	}
	
	/**
	 * 新增
	 * @param chatroomConfig
	 * @return
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int insertSelective(ChatroomConfig chatroomConfig){
		return chatroomConfigMapper.insertSelective(chatroomConfig);
	}
	
	/**
	 * 根据ID删除数据库配置
	 * @param id
	 * @return
	 */
	public int delChatRoomConfigById(int id){
		return chatroomConfigMapper.deleteByPrimaryKey(id);
	}
}
