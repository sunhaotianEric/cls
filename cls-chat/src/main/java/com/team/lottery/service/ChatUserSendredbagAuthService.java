package com.team.lottery.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.chatusersendredbagauth.ChatUserSendredbagAuthMapper;
import com.team.lottery.vo.ChatUserSendredbagAuth;

@Service("chatUserSendredbagAuthService")
public class ChatUserSendredbagAuthService {
	
	@Autowired
	private ChatUserSendredbagAuthMapper chatUserSendredbagAuthMapper;
	
	
   public int deleteByPrimaryKey(Integer id){
	  return chatUserSendredbagAuthMapper.deleteByPrimaryKey(id);
   }
   
   @Transactional(readOnly = false, rollbackFor = Exception.class)
   public  int insertSelective(ChatUserSendredbagAuth record){
	   return chatUserSendredbagAuthMapper.insertSelective(record);
   }

   public  ChatUserSendredbagAuth selectByPrimaryKey(Integer id){
	   return chatUserSendredbagAuthMapper.selectByPrimaryKey(id);
   }

   @Transactional(readOnly = false, rollbackFor = Exception.class)
   public  int updateByPrimaryKeySelective(ChatUserSendredbagAuth record){
	   return chatUserSendredbagAuthMapper.updateByPrimaryKeySelective(record);
   }
   
   /**
    * 根据业务系统，房间ID，用户名查询
    * @param record
    * @return
    */
   public ChatUserSendredbagAuth getChatUserSendredbagAuth(ChatUserSendredbagAuth record){
	   return chatUserSendredbagAuthMapper.getChatUserSendredbagAuth(record);
   }
   
   /**
    * 分页查询
    * @param record
    * @param page
    * @return
    */
   public  Page getChatUserSendredbagAuthPage(ChatUserSendredbagAuth record,Page page){
		page.setPageContent(chatUserSendredbagAuthMapper.getChatUserSendredbagAuthPage(record, page.getStartIndex(),page.getPageSize()));
    	page.setTotalRecNum(chatUserSendredbagAuthMapper.getChatUserSendredbagAuthPageCount(record));
    	return page;
   }
   
   /**
    * 查询符合条件的记录条数
    * @param record
    * @return
    */
   public int getChatUserSendredbagAuthPageCount(ChatUserSendredbagAuth record){
	   return chatUserSendredbagAuthMapper.getChatUserSendredbagAuthPageCount(record);  
   }
   
   /**
    * 根据条件查询记录条数
    * @param record
    * @return
    */
   public int getChatUserSendredbagAuthCountForUpdate(ChatUserSendredbagAuth record){
	   return chatUserSendredbagAuthMapper.getChatUserSendredbagAuthCountForUpdate(record);  
   }
}
