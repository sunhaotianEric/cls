package com.team.lottery.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.chatroleuser.ChatRoleUserMapper;
import com.team.lottery.vo.ChatRoleUser;

@Service("chatRoleUserService")
public class ChatRoleUserService {

	@Autowired
	private ChatRoleUserMapper chatRoleUserMapper;

	public int deleteByPrimaryKey(Integer id) {
		return chatRoleUserMapper.deleteByPrimaryKey(id);
	}

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int insertSelective(ChatRoleUser record) {
		return chatRoleUserMapper.insertSelective(record);
	}

	public ChatRoleUser selectByPrimaryKey(Integer id) {
		return chatRoleUserMapper.selectByPrimaryKey(id);
	}

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int updateByPrimaryKeySelective(ChatRoleUser record) {
		return chatRoleUserMapper.updateByPrimaryKeySelective(record);
	}

	/**
	 * 分页查询
	 * 
	 * @param query
	 * @param page
	 * @return
	 */
	public Page getAllChatRoleUser(ChatRoleUser query, Page page) {
		page.setTotalRecNum(chatRoleUserMapper.getChatRoleUserCount(query));
		page.setPageContent(chatRoleUserMapper.getAllChatRoleUser(query, page.getStartIndex(), page.getPageSize()));
		return page;
	}

	/**
	 * 根据名字查询
	 * 
	 * @param userName
	 * @param roomId
	 * @param bizSystem
	 * @return
	 */
	public ChatRoleUser getChatRoleUserByUserName(String userName, Integer roomId, String bizSystem) {
		return chatRoleUserMapper.getChatRoleUserByUserName(userName, roomId, bizSystem);
	}

	/**
	 * 根据用户名查询
	 * 
	 * @param bizSystem
	 * @param userName
	 * @return
	 */
	public ChatRoleUser selectByUserName(String bizSystem, String userName) {
		return chatRoleUserMapper.selectByUserName(bizSystem, userName);
	}

	/**
	 * 查询所有不包括群主的信息
	 * 
	 * @param bizSystem
	 * @param userName
	 * @return
	 */
	public List<ChatRoleUser> selectByRole(Integer roomId, String bizSystem) {
		return chatRoleUserMapper.selectByRole(roomId, bizSystem);
	}

	/**
	 * 根据业务系统查询所有的
	 * 
	 * @param bizSystem
	 * @return
	 */
	public List<ChatRoleUser> getChatRoleUser(Integer roomId, String bizSystem) {
		return chatRoleUserMapper.getChatRoleUser(roomId, bizSystem);
	}

	/**
	 * 根据业务系统查询所有的
	 * 
	 * @param bizSystem
	 * @return
	 */
	public List<ChatRoleUser> getChatRoleUserRole(String role, Integer roomId, String bizSystem) {
		return chatRoleUserMapper.getChatRoleUserRole(role, roomId, bizSystem);
	}

	/**
	 * 批量插入
	 * 
	 * @param record
	 * @return
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int insertChatRoleUser(List<ChatRoleUser> record) {
		return chatRoleUserMapper.insertChatRoleUser(record);
	}

	/**
	 * 批量修改
	 * 
	 * @param currentSystem
	 * @param username
	 * @return
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int deleteChatRoleUser(List<Integer> Id) {
		return chatRoleUserMapper.deleteChatRoleUser(Id);
	}

	/**
	 * 根据系统，名字，房间号批量查询
	 * 
	 * @param userName
	 * @param roomId
	 * @param bizSystem
	 * @return
	 */
	public List<ChatRoleUser> getAllByOnlyChatRoleUser(List<String> userName, Integer roomId, String bizSystem) {
		return chatRoleUserMapper.getAllByOnlyChatRoleUser(userName, roomId, bizSystem);
	}

	/**
	 * 根据系统，名字批量查询
	 * 
	 * @param bizSystem
	 * @param userName
	 * @return
	 */
	public List<ChatRoleUser> selectAllByUserName(String bizSystem, List<String> userName) {
		return chatRoleUserMapper.selectAllByUserName(bizSystem, userName);
	}
}
