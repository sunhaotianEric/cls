package com.team.lottery.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.chatlevelauth.ChatLevelAuthMapper;
import com.team.lottery.vo.ChatLevelAuth;

@Service("chatLevelAuthService")
public class ChatLevelAuthService {

	@Autowired
	private ChatLevelAuthMapper chatLevelAuthMapper;

	public int deleteByPrimaryKey(Long id) {
		return chatLevelAuthMapper.deleteByPrimaryKey(id);
	}

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int insertSelective(ChatLevelAuth record) {
		return chatLevelAuthMapper.insertSelective(record);
	}

	public ChatLevelAuth selectByPrimaryKey(Long id) {
		return chatLevelAuthMapper.selectByPrimaryKey(id);
	}

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int updateByPrimaryKeySelective(ChatLevelAuth record) {
		return chatLevelAuthMapper.updateByPrimaryKeySelective(record);
	}

	/**
	 * 查找所有的数据
	 * 
	 * @param page
	 * @param query
	 * @return
	 */
	public Page getAllChatLevelAuthQueryPage(ChatLevelAuth query, Page page) {
		page.setTotalRecNum(chatLevelAuthMapper.getAllChatLevelAuthQueryPageCount(query));
		page.setPageContent(
				chatLevelAuthMapper.getAllChatLevelAuthQueryPage(query, page.getStartIndex(), page.getPageSize()));
		return page;
	}

	/**
	 * 根据业务系统及VIP等级查询发言权限
	 * 
	 * @return
	 */
	public ChatLevelAuth getAllChatLevel(ChatLevelAuth query) {
		return chatLevelAuthMapper.getAllChatLevelAuth(query);
	}

}
