package com.team.lottery.redis.mq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.enums.ERedisChannel;
import com.team.lottery.system.SystemListener;
import com.team.lottery.system.message.messagetype.OpenCodeApiMessage;
import com.team.lottery.util.SerializeUtils;
import com.team.lottery.vo.OpenCodeApi;

public class OpenCodeRedisPubSubListener extends  RedisPubSubListener{
	private static Logger logger = LoggerFactory.getLogger(OpenCodeRedisPubSubListener.class);

	@Override
	public void onMessage(String channel, String message) {
		
		try {
			//刷新系统缓存通知
			if(channel.equals(ERedisChannel.OPENCODECACHE.name())){
				//刷新开奖接口缓存
				OpenCodeApiMessage openCodeApiMessage=(OpenCodeApiMessage) SerializeUtils.unSerialize(message.getBytes("ISO-8859-1"));
		         if(openCodeApiMessage.getList() != null){
		        		for(OpenCodeApi obj:openCodeApiMessage.getList()){
							SystemListener.initThreandParam(obj,1);
						} 
		         }
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}	
		//logger.info("onMessage: channel[" + channel + "], message[" + message + "]");  
	}
}
