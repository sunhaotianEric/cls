package com.team.lottery.system;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.team.lottery.opencode.official.lhc.AutoAMLhcThread;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.enums.EOpenInterfaceType;
import com.team.lottery.enums.ERedisChannel;
import com.team.lottery.opencode.official.dpc.AutoFcsddpcThread;
import com.team.lottery.opencode.official.ks.AutoAhksThread;
import com.team.lottery.opencode.official.ks.AutoBjksThread;
import com.team.lottery.opencode.official.ks.AutoGsksThread;
import com.team.lottery.opencode.official.ks.AutoGxksThread;
import com.team.lottery.opencode.official.ks.AutoHbksThread;
import com.team.lottery.opencode.official.ks.AutoJlksThread;
import com.team.lottery.opencode.official.ks.AutoJsksThread;
import com.team.lottery.opencode.official.ks.AutoShksThread;
import com.team.lottery.opencode.official.lhc.AutoLhcThread;
import com.team.lottery.opencode.official.pk10.AutoBjPk10Thread;
import com.team.lottery.opencode.official.pk10.AutoXyftThread;
import com.team.lottery.opencode.official.ssc.AutoCqsscThread;
import com.team.lottery.opencode.official.ssc.AutoHgffcThread;
import com.team.lottery.opencode.official.ssc.AutoJxsscThread;
import com.team.lottery.opencode.official.ssc.AutoTjsscThread;
import com.team.lottery.opencode.official.ssc.AutoTwwfcThread;
import com.team.lottery.opencode.official.ssc.AutoXjplfcThread;
import com.team.lottery.opencode.official.ssc.AutoXjsscThread;
import com.team.lottery.opencode.official.syxw.AutoCqsyxwThread;
import com.team.lottery.opencode.official.syxw.AutoFjsyxwThread;
import com.team.lottery.opencode.official.syxw.AutoGdsyxwThread;
import com.team.lottery.opencode.official.syxw.AutoJxsyxwThread;
import com.team.lottery.opencode.official.syxw.AutoSdsyxwThread;
import com.team.lottery.opencode.official.xyeb.AutoXyebThread;
import com.team.lottery.redis.mq.OpenCodeRedisPubSubListener;
import com.team.lottery.redis.mq.RedisThread;
import com.team.lottery.service.OpenCodeApiService;
import com.team.lottery.system.cache.LhcWinKindPlayCache;
import com.team.lottery.system.message.MessageQueueThread;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.util.GetApplicationPropertiesValueUtil;
import com.team.lottery.vo.OpenCodeApi;

public class SystemListener implements ServletContextListener {

	private static Logger log = LoggerFactory.getLogger(SystemListener.class);

	private OpenCodeApiService openCodeApiService;

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		@SuppressWarnings("unused")
		WebApplicationContext app = WebApplicationContextUtils.getWebApplicationContext(arg0.getServletContext());

		// 初始化当前系统常量配置
		SystemPropeties.serverNum = "0";

		// 启动消息接收处理线程
		MessageQueueThread messageQueueThread = new MessageQueueThread();
		messageQueueThread.start();

		// reids订阅 线程监听 系统的
		new Thread(new RedisThread(ERedisChannel.CLSCACHE.name())).start();

		OpenCodeRedisPubSubListener openCodeRedisPubSubListener = new OpenCodeRedisPubSubListener();
		new Thread(new RedisThread(ERedisChannel.OPENCODECACHE.name() )).start();

		OpenCodeApi query = new OpenCodeApi();
		openCodeApiService = (OpenCodeApiService) ApplicationContextUtil.getBean("openCodeApiService");
		List<OpenCodeApi> list = openCodeApiService.getOpenCodeApiByLotteryKind(query);

		for (OpenCodeApi obj : list) {
			initThreandParam(obj, 0);
		}

		// 缓存数据初始化
		SystemCache.loadAllCacheData();
		// 初始化六合彩的彩种赔率
		LhcWinKindPlayCache.initLhcWinKindPlay();

		// 开奖线程集合
		final List<Thread> threads = new ArrayList<Thread>();

		// 虚拟幸运彩开奖线程
//		AutoXnXyThread autoXnXy = new AutoXnXyThread();
//		Thread xnXyThread = new Thread(autoXnXy);
//		threads.add(xnXyThread);

//---------------------------------时时彩类 start------------------------------------
		log.info("开始启动线程........");
		// 重庆时时彩
		AutoCqsscThread autoCqSscThread = new AutoCqsscThread();
		Thread cqSscThread = new Thread(autoCqSscThread);
		threads.add(cqSscThread);

		// 天津时时彩
		AutoTjsscThread autoTjSscThread = new AutoTjsscThread();
		Thread tjSscThread = new Thread(autoTjSscThread);
		threads.add(tjSscThread);

		// 新疆时时彩
		AutoXjsscThread autoXjSscThread = new AutoXjsscThread();
		Thread xjSscThread = new Thread(autoXjSscThread);
		threads.add(xjSscThread);

		// 幸运分分彩 改用调度执行
		/*
		 * AutoJlffcThread autoJlffcThread = new AutoJlffcThread(); Thread t = new
		 * Thread(autoJlffcThread); t.start();
		 */

		// 新加坡2分彩
		/*
		 * AutoXjplfcThread autoXjplfcThread = new AutoXjplfcThread(); Thread
		 * xjplfcThread = new Thread(autoXjplfcThread); threads.add(xjplfcThread);
		 */

		// 台湾5分彩
	/*	AutoTwwfcThread autoTwwfcThread = new AutoTwwfcThread();
		Thread twwfcThread = new Thread(autoTwwfcThread);
		threads.add(twwfcThread);*/

		// 韩国1.5分彩
		/*
		 * AutoHgffcThread autoHgffcThread = new AutoHgffcThread(); Thread hgffcThread =
		 * new Thread(autoHgffcThread); threads.add(hgffcThread);
		 */

		// 江西时时彩 次彩种下架
		/*
		 * AutoJxsscThread autoJxSscThread = new AutoJxsscThread(); Thread jxSscThread =
		 * new Thread(autoJxSscThread); threads.add(jxSscThread);
		 */
//---------------------------------时时彩类 end------------------------------------

//---------------------------------11选5类 start------------------------------------
		// 山东11选5
		AutoSdsyxwThread autoSdsyxwThread = new AutoSdsyxwThread();
		Thread sdSyxwThread = new Thread(autoSdsyxwThread);
		threads.add(sdSyxwThread);

		// 广东11选5
		AutoGdsyxwThread autoGdsyxwThread = new AutoGdsyxwThread();
		Thread gdSyxwThread = new Thread(autoGdsyxwThread);
		threads.add(gdSyxwThread);

		// 江西11选5
		AutoJxsyxwThread autoJxsyxwThread = new AutoJxsyxwThread();
		Thread jxSyxwThread = new Thread(autoJxsyxwThread);
		threads.add(jxSyxwThread);

		// 福建11选5
		AutoFjsyxwThread autoFjsyxwThread = new AutoFjsyxwThread();
		Thread fjSyxwThread = new Thread(autoFjsyxwThread);
		//threads.add(fjSyxwThread);
//---------------------------------11选5类 end------------------------------------

//---------------------------------快三类 start------------------------------------
		// 江苏快三
		AutoJsksThread autoJsksThread = new AutoJsksThread();
		Thread jsksThread = new Thread(autoJsksThread);
		threads.add(jsksThread);

		// 安徽快三
		AutoAhksThread autoAhksThread = new AutoAhksThread();
		Thread ahksThread = new Thread(autoAhksThread);
		threads.add(ahksThread);

		// 湖北快三
		AutoHbksThread autoHbksThread = new AutoHbksThread();
		Thread hbksThread = new Thread(autoHbksThread);
		threads.add(hbksThread);

		// 北京快三
		AutoBjksThread autoBjksThread = new AutoBjksThread();
		Thread bjksThread = new Thread(autoBjksThread);
		threads.add(bjksThread);

		// 广西快三
		AutoGxksThread autoGxksThread = new AutoGxksThread();
		Thread gxksThread = new Thread(autoGxksThread);
		threads.add(gxksThread);

		// 上海快三
		AutoShksThread autoShksThread = new AutoShksThread();
		Thread shksThread = new Thread(autoShksThread);
		threads.add(shksThread);

		//甘肃快三
		AutoGsksThread autoGsksThread = new AutoGsksThread();
		Thread gsksThread = new Thread(autoGsksThread);
		threads.add(gsksThread);

		// 吉林快三
		AutoJlksThread autoJlksThread = new AutoJlksThread();
		Thread jlksThread = new Thread(autoJlksThread);
		threads.add(jlksThread);

		// 幸运快三 改用调度执行

//		  AutoJyksThread autoJyksThread = new AutoJyksThread(); Thread t = new
//		  Thread(autoJyksThread); t.start();

//---------------------------------快三类 end------------------------------------

//---------------------------------PK10类 start----------------------------------
		// 北京PK10
		AutoBjPk10Thread autoBjpk10Thread = new AutoBjPk10Thread();
		Thread bjpk10Thread = new Thread(autoBjpk10Thread);
		threads.add(bjpk10Thread);
		//香港六合彩
		AutoLhcThread autoLhcThread = new AutoLhcThread();
		Thread lhcThread = new Thread(autoLhcThread);
		threads.add(lhcThread);

		//澳门六合彩
		AutoAMLhcThread autoAMLhcThread = new AutoAMLhcThread();
		Thread amlhcThread = new Thread(autoAMLhcThread);
		threads.add(amlhcThread);

		// 幸运飞艇
		AutoXyftThread autoXyftThread = new AutoXyftThread();
		Thread xyfThread = new Thread(autoXyftThread);
		threads.add(xyfThread);
//---------------------------------PK10类 end------------------------------------

//---------------------------------彩种暂未分类 start------------------------------------
		// 福彩3D 改用调度方式执行
		/*AutoFcsddpcThread autoFcsddpcThread = new AutoFcsddpcThread();
		Thread fcsddpcThread = new Thread(autoFcsddpcThread);
		threads.add(fcsddpcThread);*/

		// 幸运28
		/*AutoXyebThread autoXyebThread = new AutoXyebThread();
		Thread xyebThread = new Thread(autoXyebThread);
		threads.add(xyebThread);*/
//---------------------------------彩种暂未分类 end------------------------------------

		final ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
		Thread startThread = new Thread(new Runnable() {

			@Override
			public void run() {
				// 采用线程池启动线程集合 每隔4秒启动一个线程
				for (int i = 0; i < threads.size(); i++) {
					try {
						Thread.sleep(4 * 1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					cachedThreadPool.execute(threads.get(i));
				}
			}
		});

		// 定义key值.用于取出value.
		String key = "officialLotteryOpen";
		// 根据key读取配置文件的value值.
		String propertiesValue = GetApplicationPropertiesValueUtil.getPropertiesValue(key);
		// 赋值给静态资源对象.
		GetApplicationPropertiesValueUtil.OFFICIALLOTTERYOPEN = propertiesValue;
		log.info("根据配置文件application.properties中到根据key值[{}]得到value值[{}]",key,GetApplicationPropertiesValueUtil.OFFICIALLOTTERYOPEN);
		// 当开启状态为1的时候才启动.
		if (GetApplicationPropertiesValueUtil.OFFICIALLOTTERYOPEN.equals("1")) {
			log.info("项目启动,开始启动官彩开奖线程...");
			startThread.start();
		} else {
			log.info("项目启动,没有启动官彩开奖线程...");
		}
		// 停止清理线程资源
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

			@Override
			public void run() {
				cachedThreadPool.shutdown();
			}
		}));

	}

	/**
	 * 初始化线程参数 flag =0 初始化参数， =1 切换和刷新
	 */
	public static void initThreandParam(OpenCodeApi obj, int flag) {
		@SuppressWarnings("rawtypes")
		Class cla = null;
		String logStr = "初始化线程";
		if (flag == 1) {
			logStr = "切换接口";
		}
		if (ELotteryKind.AHKS.getCode().equals(obj.getLotteryKind())) {
			AutoAhksThread.openCodeApiId = obj.getId();
			AutoAhksThread.URL = obj.getApiUrl();
			AutoAhksThread.TYPE = EOpenInterfaceType.valueOf(obj.getApiType());
			AutoAhksThread.BACK_URL = obj.getBackApiUrl();
			AutoAhksThread.BACKTYPE = EOpenInterfaceType.valueOf(obj.getBackApiType());
			AutoAhksThread.sleepSecond = obj.getSleepSecond();
			cla = AutoAhksThread.class;
			if (flag == 1) {
				AutoAhksThread.CHANGE_CURRENT_URL = obj.getCurrentApiUrl();
				AutoAhksThread.CHANGE_CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			} else {
				AutoAhksThread.DATE = new Date();
				AutoAhksThread.CURRENT_URL = obj.getCurrentApiUrl();
				AutoAhksThread.CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			}

		} else if (ELotteryKind.BJKS.getCode().equals(obj.getLotteryKind())) {
			AutoBjksThread.openCodeApiId = obj.getId();
			AutoBjksThread.URL = obj.getApiUrl();
			AutoBjksThread.TYPE = EOpenInterfaceType.valueOf(obj.getApiType());
			AutoBjksThread.BACK_URL = obj.getBackApiUrl();
			AutoBjksThread.BACKTYPE = EOpenInterfaceType.valueOf(obj.getBackApiType());
			AutoBjksThread.sleepSecond = obj.getSleepSecond();
			cla = AutoBjksThread.class;
			if (flag == 1) {
				AutoBjksThread.CHANGE_CURRENT_URL = obj.getCurrentApiUrl();
				AutoBjksThread.CHANGE_CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			} else {
				AutoBjksThread.DATE = new Date();
				AutoBjksThread.CURRENT_URL = obj.getCurrentApiUrl();
				AutoBjksThread.CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			}
		} else if (ELotteryKind.BJPK10.getCode().equals(obj.getLotteryKind())) {
			AutoBjPk10Thread.openCodeApiId = obj.getId();
			AutoBjPk10Thread.URL = obj.getApiUrl();
			AutoBjPk10Thread.TYPE = EOpenInterfaceType.valueOf(obj.getApiType());
			AutoBjPk10Thread.BACK_URL = obj.getBackApiUrl();
			AutoBjPk10Thread.BACKTYPE = EOpenInterfaceType.valueOf(obj.getBackApiType());
			AutoBjPk10Thread.sleepSecond = obj.getSleepSecond();
			cla = AutoBjPk10Thread.class;
			if (flag == 1) {
				AutoBjPk10Thread.CHANGE_CURRENT_URL = obj.getCurrentApiUrl();
				AutoBjPk10Thread.CHANGE_CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			} else {
				AutoBjPk10Thread.DATE = new Date();
				AutoBjPk10Thread.CURRENT_URL = obj.getCurrentApiUrl();
				AutoBjPk10Thread.CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			}
		} else if (ELotteryKind.CQSSC.getCode().equals(obj.getLotteryKind())) {
			AutoCqsscThread.openCodeApiId = obj.getId();
			AutoCqsscThread.URL = obj.getApiUrl();
			AutoCqsscThread.TYPE = EOpenInterfaceType.valueOf(obj.getApiType());
			AutoCqsscThread.BACK_URL = obj.getBackApiUrl();
			AutoCqsscThread.BACKTYPE = EOpenInterfaceType.valueOf(obj.getBackApiType());
			AutoCqsscThread.sleepSecond = obj.getSleepSecond();
			cla = AutoCqsscThread.class;
			if (flag == 1) {
				AutoCqsscThread.CHANGE_CURRENT_URL = obj.getCurrentApiUrl();
				AutoCqsscThread.CHANGE_CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			} else {
				AutoCqsscThread.DATE = new Date();
				AutoCqsscThread.CURRENT_URL = obj.getCurrentApiUrl();
				AutoCqsscThread.CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			}
		} else if (ELotteryKind.CQSYXW.getCode().equals(obj.getLotteryKind())) {
			AutoCqsyxwThread.openCodeApiId = obj.getId();
			AutoCqsyxwThread.URL = obj.getApiUrl();
			AutoCqsyxwThread.TYPE = EOpenInterfaceType.valueOf(obj.getApiType());
			AutoCqsyxwThread.BACK_URL = obj.getBackApiUrl();
			AutoCqsyxwThread.BACKTYPE = EOpenInterfaceType.valueOf(obj.getBackApiType());
			AutoCqsyxwThread.sleepSecond = obj.getSleepSecond();
			cla = AutoCqsyxwThread.class;
			if (flag == 1) {
				AutoCqsyxwThread.CHANGE_CURRENT_URL = obj.getCurrentApiUrl();
				AutoCqsyxwThread.CHANGE_CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			} else {
				AutoCqsyxwThread.DATE = new Date();
				AutoCqsyxwThread.CURRENT_URL = obj.getCurrentApiUrl();
				AutoCqsyxwThread.CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			}
		} else if (ELotteryKind.FCSDDPC.getCode().equals(obj.getLotteryKind())) {
			AutoFcsddpcThread.openCodeApiId = obj.getId();
			AutoFcsddpcThread.URL = obj.getApiUrl();
			AutoFcsddpcThread.TYPE = EOpenInterfaceType.valueOf(obj.getApiType());
			AutoFcsddpcThread.BACK_URL = obj.getBackApiUrl();
			AutoFcsddpcThread.BACKTYPE = EOpenInterfaceType.valueOf(obj.getBackApiType());
			AutoFcsddpcThread.sleepSecond = obj.getSleepSecond();
			cla = AutoFcsddpcThread.class;
			if (flag == 1) {
				AutoFcsddpcThread.CHANGE_CURRENT_URL = obj.getCurrentApiUrl();
				AutoFcsddpcThread.CHANGE_CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			} else {
				AutoFcsddpcThread.DATE = new Date();
				AutoFcsddpcThread.CURRENT_URL = obj.getCurrentApiUrl();
				AutoFcsddpcThread.CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			}
		} else if (ELotteryKind.FJSYXW.getCode().equals(obj.getLotteryKind())) {
			AutoFjsyxwThread.openCodeApiId = obj.getId();
			AutoFjsyxwThread.URL = obj.getApiUrl();
			AutoFjsyxwThread.TYPE = EOpenInterfaceType.valueOf(obj.getApiType());
			AutoFjsyxwThread.BACK_URL = obj.getBackApiUrl();
			AutoFjsyxwThread.BACKTYPE = EOpenInterfaceType.valueOf(obj.getBackApiType());
			AutoFjsyxwThread.sleepSecond = obj.getSleepSecond();
			cla = AutoFjsyxwThread.class;
			if (flag == 1) {
				AutoFjsyxwThread.CHANGE_CURRENT_URL = obj.getCurrentApiUrl();
				AutoFjsyxwThread.CHANGE_CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			} else {
				AutoFjsyxwThread.DATE = new Date();
				AutoFjsyxwThread.CURRENT_URL = obj.getCurrentApiUrl();
				AutoFjsyxwThread.CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			}
		} else if (ELotteryKind.GDSYXW.getCode().equals(obj.getLotteryKind())) {
			AutoGdsyxwThread.openCodeApiId = obj.getId();
			AutoGdsyxwThread.URL = obj.getApiUrl();
			AutoGdsyxwThread.TYPE = EOpenInterfaceType.valueOf(obj.getApiType());
			AutoGdsyxwThread.BACK_URL = obj.getBackApiUrl();
			AutoGdsyxwThread.BACKTYPE = EOpenInterfaceType.valueOf(obj.getBackApiType());
			AutoGdsyxwThread.sleepSecond = obj.getSleepSecond();
			cla = AutoGdsyxwThread.class;
			if (flag == 1) {
				AutoGdsyxwThread.CHANGE_CURRENT_URL = obj.getCurrentApiUrl();
				AutoGdsyxwThread.CHANGE_CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			} else {
				AutoGdsyxwThread.DATE = new Date();
				AutoGdsyxwThread.CURRENT_URL = obj.getCurrentApiUrl();
				AutoGdsyxwThread.CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			}
		} else if (ELotteryKind.HBKS.getCode().equals(obj.getLotteryKind())) {
			AutoHbksThread.openCodeApiId = obj.getId();
			AutoHbksThread.URL = obj.getApiUrl();
			AutoHbksThread.TYPE = EOpenInterfaceType.valueOf(obj.getApiType());
			AutoHbksThread.BACK_URL = obj.getBackApiUrl();
			AutoHbksThread.BACKTYPE = EOpenInterfaceType.valueOf(obj.getBackApiType());
			AutoHbksThread.sleepSecond = obj.getSleepSecond();
			cla = AutoHbksThread.class;
			if (flag == 1) {
				AutoHbksThread.CHANGE_CURRENT_URL = obj.getCurrentApiUrl();
				AutoHbksThread.CHANGE_CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			} else {
				AutoHbksThread.DATE = new Date();
				AutoHbksThread.CURRENT_URL = obj.getCurrentApiUrl();
				AutoHbksThread.CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			}
		} else if (ELotteryKind.HGFFC.getCode().equals(obj.getLotteryKind())) {
			AutoHgffcThread.openCodeApiId = obj.getId();
			AutoHgffcThread.URL = obj.getApiUrl();
			AutoHgffcThread.TYPE = EOpenInterfaceType.valueOf(obj.getApiType());
			AutoHgffcThread.BACK_URL = obj.getBackApiUrl();
			AutoHgffcThread.BACKTYPE = EOpenInterfaceType.valueOf(obj.getBackApiType());
			AutoHgffcThread.sleepSecond = obj.getSleepSecond();
			cla = AutoHgffcThread.class;
			if (flag == 1) {
				AutoHgffcThread.CHANGE_CURRENT_URL = obj.getCurrentApiUrl();
				AutoHgffcThread.CHANGE_CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			} else {
				AutoHgffcThread.DATE = new Date();
				AutoHgffcThread.CURRENT_URL = obj.getCurrentApiUrl();
				AutoHgffcThread.CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			}
		} else if (ELotteryKind.JLKS.getCode().equals(obj.getLotteryKind())) {
			AutoJlksThread.openCodeApiId = obj.getId();
			AutoJlksThread.URL = obj.getApiUrl();
			AutoJlksThread.TYPE = EOpenInterfaceType.valueOf(obj.getApiType());
			AutoJlksThread.BACK_URL = obj.getBackApiUrl();
			AutoJlksThread.BACKTYPE = EOpenInterfaceType.valueOf(obj.getBackApiType());
			AutoJlksThread.sleepSecond = obj.getSleepSecond();
			cla = AutoJlksThread.class;
			if (flag == 1) {
				AutoJlksThread.CHANGE_CURRENT_URL = obj.getCurrentApiUrl();
				AutoJlksThread.CHANGE_CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			} else {
				AutoJlksThread.DATE = new Date();
				AutoJlksThread.CURRENT_URL = obj.getCurrentApiUrl();
				AutoJlksThread.CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			}
		} else if (ELotteryKind.JSKS.getCode().equals(obj.getLotteryKind())) {
			AutoJsksThread.openCodeApiId = obj.getId();
			AutoJsksThread.URL = obj.getApiUrl();
			AutoJsksThread.TYPE = EOpenInterfaceType.valueOf(obj.getApiType());
			AutoJsksThread.BACK_URL = obj.getBackApiUrl();
			AutoJsksThread.BACKTYPE = EOpenInterfaceType.valueOf(obj.getBackApiType());
			AutoJsksThread.sleepSecond = obj.getSleepSecond();
			cla = AutoJsksThread.class;
			if (flag == 1) {
				AutoJsksThread.CHANGE_CURRENT_URL = obj.getCurrentApiUrl();
				AutoJsksThread.CHANGE_CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			} else {
				AutoJsksThread.DATE = new Date();
				AutoJsksThread.CURRENT_URL = obj.getCurrentApiUrl();
				AutoJsksThread.CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			}
		} else if (ELotteryKind.GXKS.getCode().equals(obj.getLotteryKind())) {
			AutoGxksThread.openCodeApiId = obj.getId();
			AutoGxksThread.URL = obj.getApiUrl();
			AutoGxksThread.TYPE = EOpenInterfaceType.valueOf(obj.getApiType());
			AutoGxksThread.BACK_URL = obj.getBackApiUrl();
			AutoGxksThread.BACKTYPE = EOpenInterfaceType.valueOf(obj.getBackApiType());
			AutoGxksThread.sleepSecond = obj.getSleepSecond();
			cla = AutoGxksThread.class;
			if (flag == 1) {
				AutoGxksThread.CHANGE_CURRENT_URL = obj.getCurrentApiUrl();
				AutoGxksThread.CHANGE_CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			} else {
				AutoGxksThread.DATE = new Date();
				AutoGxksThread.CURRENT_URL = obj.getCurrentApiUrl();
				AutoGxksThread.CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			}
		} else if (ELotteryKind.JXSSC.getCode().equals(obj.getLotteryKind())) {
			AutoJxsscThread.openCodeApiId = obj.getId();
			AutoJxsscThread.URL = obj.getApiUrl();
			AutoJxsscThread.TYPE = EOpenInterfaceType.valueOf(obj.getApiType());
			AutoJxsscThread.BACK_URL = obj.getBackApiUrl();
			AutoJxsscThread.BACKTYPE = EOpenInterfaceType.valueOf(obj.getBackApiType());
			AutoJxsscThread.sleepSecond = obj.getSleepSecond();
			cla = AutoJxsscThread.class;
			if (flag == 1) {
				AutoJxsscThread.CHANGE_CURRENT_URL = obj.getCurrentApiUrl();
				AutoJxsscThread.CHANGE_CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			} else {
				AutoJxsscThread.DATE = new Date();
				AutoJxsscThread.CURRENT_URL = obj.getCurrentApiUrl();
				AutoJxsscThread.CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			}
		} else if (ELotteryKind.JXSYXW.getCode().equals(obj.getLotteryKind())) {
			AutoJxsyxwThread.openCodeApiId = obj.getId();
			AutoJxsyxwThread.URL = obj.getApiUrl();
			AutoJxsyxwThread.TYPE = EOpenInterfaceType.valueOf(obj.getApiType());
			AutoJxsyxwThread.BACK_URL = obj.getBackApiUrl();
			AutoJxsyxwThread.BACKTYPE = EOpenInterfaceType.valueOf(obj.getBackApiType());
			AutoJxsyxwThread.sleepSecond = obj.getSleepSecond();
			cla = AutoJxsyxwThread.class;
			if (flag == 1) {
				AutoJxsyxwThread.CHANGE_CURRENT_URL = obj.getCurrentApiUrl();
				AutoJxsyxwThread.CHANGE_CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			} else {
				AutoJxsyxwThread.DATE = new Date();
				AutoJxsyxwThread.CURRENT_URL = obj.getCurrentApiUrl();
				AutoJxsyxwThread.CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			}
		} else if (ELotteryKind.LHC.getCode().equals(obj.getLotteryKind())) {
			AutoLhcThread.openCodeApiId = obj.getId();
			AutoLhcThread.URL = obj.getApiUrl();
			AutoLhcThread.TYPE = EOpenInterfaceType.valueOf(obj.getApiType());
			AutoLhcThread.BACK_URL = obj.getBackApiUrl();
			AutoLhcThread.BACKTYPE = EOpenInterfaceType.valueOf(obj.getBackApiType());
			AutoLhcThread.sleepSecond = obj.getSleepSecond();
			cla = AutoLhcThread.class;
			if (flag == 1) {
				AutoLhcThread.CHANGE_CURRENT_URL = obj.getCurrentApiUrl();
				AutoLhcThread.CHANGE_CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			} else {
				AutoLhcThread.DATE = new Date();
				AutoLhcThread.CURRENT_URL = obj.getCurrentApiUrl();
				AutoLhcThread.CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			}
		} else if (ELotteryKind.AMLHC.getCode().equals(obj.getLotteryKind())) {
			AutoAMLhcThread.openCodeApiId = obj.getId();
			AutoAMLhcThread.URL = obj.getApiUrl();
			AutoAMLhcThread.TYPE = EOpenInterfaceType.valueOf(obj.getApiType());
			AutoAMLhcThread.BACK_URL = obj.getBackApiUrl();
			AutoAMLhcThread.BACKTYPE = EOpenInterfaceType.valueOf(obj.getBackApiType());
			AutoAMLhcThread.sleepSecond = obj.getSleepSecond();
			cla = AutoAMLhcThread.class;
			if (flag == 1) {
				AutoAMLhcThread.CHANGE_CURRENT_URL = obj.getCurrentApiUrl();
				AutoAMLhcThread.CHANGE_CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			} else {
				AutoAMLhcThread.DATE = new Date();
				AutoAMLhcThread.CURRENT_URL = obj.getCurrentApiUrl();
				AutoAMLhcThread.CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			}
		}else if (ELotteryKind.SDSYXW.getCode().equals(obj.getLotteryKind())) {
			AutoSdsyxwThread.openCodeApiId = obj.getId();
			AutoSdsyxwThread.URL = obj.getApiUrl();
			AutoSdsyxwThread.TYPE = EOpenInterfaceType.valueOf(obj.getApiType());
			AutoSdsyxwThread.BACK_URL = obj.getBackApiUrl();
			AutoSdsyxwThread.BACKTYPE = EOpenInterfaceType.valueOf(obj.getBackApiType());
			AutoSdsyxwThread.sleepSecond = obj.getSleepSecond();
			cla = AutoSdsyxwThread.class;
			if (flag == 1) {
				AutoSdsyxwThread.CHANGE_CURRENT_URL = obj.getCurrentApiUrl();
				AutoSdsyxwThread.CHANGE_CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			} else {
				AutoSdsyxwThread.DATE = new Date();
				AutoSdsyxwThread.CURRENT_URL = obj.getCurrentApiUrl();
				AutoSdsyxwThread.CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			}
		} else if (ELotteryKind.TJSSC.getCode().equals(obj.getLotteryKind())) {
			AutoTjsscThread.openCodeApiId = obj.getId();
			AutoTjsscThread.URL = obj.getApiUrl();
			AutoTjsscThread.TYPE = EOpenInterfaceType.valueOf(obj.getApiType());
			AutoTjsscThread.BACK_URL = obj.getBackApiUrl();
			AutoTjsscThread.BACKTYPE = EOpenInterfaceType.valueOf(obj.getBackApiType());
			AutoTjsscThread.sleepSecond = obj.getSleepSecond();
			cla = AutoTjsscThread.class;
			if (flag == 1) {
				AutoTjsscThread.CHANGE_CURRENT_URL = obj.getCurrentApiUrl();
				AutoTjsscThread.CHANGE_CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			} else {
				AutoTjsscThread.DATE = new Date();
				AutoTjsscThread.CURRENT_URL = obj.getCurrentApiUrl();
				AutoTjsscThread.CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			}
		} /*else if (ELotteryKind.TWWFC.getCode().equals(obj.getLotteryKind())) {
			AutoTwwfcThread.openCodeApiId = obj.getId();
			AutoTwwfcThread.URL = obj.getApiUrl();
			AutoTwwfcThread.TYPE = EOpenInterfaceType.valueOf(obj.getApiType());
			AutoTwwfcThread.BACK_URL = obj.getBackApiUrl();
			AutoTwwfcThread.BACKTYPE = EOpenInterfaceType.valueOf(obj.getBackApiType());
			AutoTwwfcThread.sleepSecond = obj.getSleepSecond();
			cla = AutoTwwfcThread.class;
			if (flag == 1) {
				AutoTwwfcThread.CHANGE_CURRENT_URL = obj.getCurrentApiUrl();
				AutoTwwfcThread.CHANGE_CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			} else {
				AutoTwwfcThread.DATE = new Date();
				AutoTwwfcThread.CURRENT_URL = obj.getCurrentApiUrl();
				AutoTwwfcThread.CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			}
		}*/ else if (ELotteryKind.XJPLFC.getCode().equals(obj.getLotteryKind())) {
			AutoXjplfcThread.openCodeApiId = obj.getId();
			AutoXjplfcThread.URL = obj.getApiUrl();
			AutoXjplfcThread.TYPE = EOpenInterfaceType.valueOf(obj.getApiType());
			AutoXjplfcThread.BACK_URL = obj.getBackApiUrl();
			AutoXjplfcThread.BACKTYPE = EOpenInterfaceType.valueOf(obj.getBackApiType());
			AutoXjplfcThread.sleepSecond = obj.getSleepSecond();
			cla = AutoXjplfcThread.class;
			if (flag == 1) {
				AutoXjplfcThread.CHANGE_CURRENT_URL = obj.getCurrentApiUrl();
				AutoXjplfcThread.CHANGE_CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			} else {
				AutoXjplfcThread.DATE = new Date();
				AutoXjplfcThread.CURRENT_URL = obj.getCurrentApiUrl();
				AutoXjplfcThread.CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			}
		} else if (ELotteryKind.XJSSC.getCode().equals(obj.getLotteryKind())) {
			AutoXjsscThread.openCodeApiId = obj.getId();
			AutoXjsscThread.URL = obj.getApiUrl();
			AutoXjsscThread.TYPE = EOpenInterfaceType.valueOf(obj.getApiType());
			AutoXjsscThread.BACK_URL = obj.getBackApiUrl();
			AutoXjsscThread.BACKTYPE = EOpenInterfaceType.valueOf(obj.getBackApiType());
			AutoXjsscThread.sleepSecond = obj.getSleepSecond();
			cla = AutoXjsscThread.class;
			if (flag == 1) {
				AutoXjsscThread.CHANGE_CURRENT_URL = obj.getCurrentApiUrl();
				AutoXjsscThread.CHANGE_CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			} else {
				AutoXjsscThread.DATE = new Date();
				AutoXjsscThread.CURRENT_URL = obj.getCurrentApiUrl();
				AutoXjsscThread.CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			}
		} else if (ELotteryKind.XYEB.getCode().equals(obj.getLotteryKind())) {
			AutoXyebThread.openCodeApiId = obj.getId();
			AutoXyebThread.URL = obj.getApiUrl();
			AutoXyebThread.TYPE = EOpenInterfaceType.valueOf(obj.getApiType());
			AutoXyebThread.BACK_URL = obj.getBackApiUrl();
			AutoXyebThread.BACKTYPE = EOpenInterfaceType.valueOf(obj.getBackApiType());
			AutoXyebThread.sleepSecond = obj.getSleepSecond();
			cla = AutoXyebThread.class;
			if (flag == 1) {
				AutoXyebThread.CHANGE_CURRENT_URL = obj.getCurrentApiUrl();
				AutoXyebThread.CHANGE_CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			} else {
				AutoXyebThread.DATE = new Date();
				AutoXyebThread.CURRENT_URL = obj.getCurrentApiUrl();
				AutoXyebThread.CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			}
		} else if (ELotteryKind.SHKS.getCode().equals(obj.getLotteryKind())) {
			AutoShksThread.openCodeApiId = obj.getId();
			AutoShksThread.URL = obj.getApiUrl();
			AutoShksThread.TYPE = EOpenInterfaceType.valueOf(obj.getApiType());
			AutoShksThread.BACK_URL = obj.getBackApiUrl();
			AutoShksThread.BACKTYPE = EOpenInterfaceType.valueOf(obj.getBackApiType());
			AutoShksThread.sleepSecond = obj.getSleepSecond();
			cla = AutoShksThread.class;
			if (flag == 1) {
				AutoShksThread.CHANGE_CURRENT_URL = obj.getCurrentApiUrl();
				AutoShksThread.CHANGE_CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			} else {
				AutoShksThread.DATE = new Date();
				AutoShksThread.CURRENT_URL = obj.getCurrentApiUrl();
				AutoShksThread.CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			}

		} else if (ELotteryKind.GSKS.getCode().equals(obj.getLotteryKind())) {
			AutoGsksThread.openCodeApiId = obj.getId();
			AutoGsksThread.URL = obj.getApiUrl();
			AutoGsksThread.TYPE = EOpenInterfaceType.valueOf(obj.getApiType());
			AutoGsksThread.BACK_URL = obj.getBackApiUrl();
			AutoGsksThread.BACKTYPE = EOpenInterfaceType.valueOf(obj.getBackApiType());
			AutoGsksThread.sleepSecond = obj.getSleepSecond();
			cla = AutoGsksThread.class;
			if (flag == 1) {
				AutoGsksThread.CHANGE_CURRENT_URL = obj.getCurrentApiUrl();
				AutoGsksThread.CHANGE_CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			} else {
				AutoGsksThread.DATE = new Date();
				AutoGsksThread.CURRENT_URL = obj.getApiUrl();
				AutoGsksThread.CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			}
		} else if (ELotteryKind.XYFT.getCode().equals(obj.getLotteryKind())) {
			AutoXyftThread.openCodeApiId = obj.getId();
			AutoXyftThread.URL = obj.getApiUrl();
			AutoXyftThread.TYPE = EOpenInterfaceType.valueOf(obj.getApiType());
			AutoXyftThread.BACK_URL = obj.getBackApiUrl();
			AutoXyftThread.BACKTYPE = EOpenInterfaceType.valueOf(obj.getBackApiType());
			AutoXyftThread.sleepSecond = obj.getSleepSecond();
			cla = AutoXyftThread.class;
			if (flag == 1) {
				AutoXyftThread.CHANGE_CURRENT_URL = obj.getCurrentApiUrl();
				AutoXyftThread.CHANGE_CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			} else {
				AutoXyftThread.DATE = new Date();
				AutoXyftThread.CURRENT_URL = obj.getCurrentApiUrl();
				AutoXyftThread.CURRENT_TYPE = EOpenInterfaceType.valueOf(obj.getCurrentApiType());
			}

		} else {
			log.error(logStr + "参数未知参数：id=" + obj.getId() + ",彩种：" + obj.getLotteryKind() + ",url=" + obj.getApiUrl()
					+ ",type=" + obj.getApiType());
		}
		if (cla != null) {

			log.info(logStr + "" + cla.getName() + "彩种：" + obj.getLotteryKind() + "参数：id=" + obj.getId() + ",url="
					+ obj.getApiUrl() + ",type=" + obj.getApiType() + ",backurl=" + obj.getBackApiUrl() + ",backType="
					+ obj.getBackApiType());
		}
	}
}
