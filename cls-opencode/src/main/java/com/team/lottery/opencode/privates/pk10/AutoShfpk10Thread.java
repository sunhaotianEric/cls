package com.team.lottery.opencode.privates.pk10;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.enums.EOrderStatus;
import com.team.lottery.enums.EPK10Kind;
import com.team.lottery.enums.EProstateStatus;
import com.team.lottery.extvo.OrderQuery;
import com.team.lottery.opencode.BaseOpenCodeHandler;
import com.team.lottery.service.CommonOpenCodeService;
import com.team.lottery.service.DayLotteryGainService;
import com.team.lottery.service.LotteryCodeXyImportService;
import com.team.lottery.service.LotteryCodeXyService;
import com.team.lottery.service.LotteryControlConfiService;
import com.team.lottery.service.LotteryNumberService;
import com.team.lottery.service.OrderService;
import com.team.lottery.service.lotterykindplay.LotteryKindPlayService;
import com.team.lottery.system.MainOpenDealThread;
import com.team.lottery.system.message.LotteryCodeSendMessage;
import com.team.lottery.system.message.MessageQueueCenter;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.util.BeanUtilsExtends;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.DateUtils;
import com.team.lottery.util.IntegerUtil;
import com.team.lottery.util.StringUtils;
import com.team.lottery.vo.BizSystem;
import com.team.lottery.vo.DayLotteryGain;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryCodeXy;
import com.team.lottery.vo.LotteryCodeXyImport;
import com.team.lottery.vo.LotteryControlConfig;
import com.team.lottery.vo.LotteryNumber;
import com.team.lottery.vo.Order;

/**
 * 十分pk10开奖线程
 * @author listgoo
 *
 * @date 2019年3月15日
 */
@Component("autoShfpk10Job")
public class AutoShfpk10Thread extends BaseOpenCodeHandler implements Runnable{

	private static Logger log = LoggerFactory.getLogger(AutoShfpk10Thread.class);
	
	//线程循环执行标志
	public static Boolean runFlag = true;
	//线程休眠时间，单位：秒
	public static int sleepSecond = 10;
	
	//标志是否第一次运行
	private static Boolean isFirstRun = true;
	
	//是否从CodeXyImport表中导入数据处理,true是处理，false是不处理
	private static Boolean importCode = true;
	
	private ELotteryKind lotteryKind = ELotteryKind.SHFPK10;
	
	@Autowired
	private LotteryNumberService lotteryNumberService;
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private LotteryCodeXyService lotteryCodeXyService;
	
	@Autowired
	private LotteryControlConfiService lotteryControlConfiService;
	
	@Autowired
	private DayLotteryGainService dayLotteryGainService;

	@Autowired
	private LotteryCodeXyImportService lotteryCodeXyImportService;
	
	public void run() {
		log.info("启动了" + lotteryKind.getDescription() + "开奖线程...");
		while(runFlag) {
			openDeal();
			try {
				//10秒计算一次
				Thread.sleep(sleepSecond * 1000); 
			} catch (InterruptedException e) {
				log.error(lotteryKind.getDescription() + "开奖接口线程休眠出现异常.", e);
			} 
			log.debug(lotteryKind.getDescription() + "开奖计算成功.");
		}
		log.info("结束" + lotteryKind.getDescription() + "开奖线程，线程退出...");
	}
	
	public void execute() {
		openDeal();
		log.debug(lotteryKind.getDescription() + "开奖计算成功.");
	}
	
	private LotteryNumberService getLotteryNumberService() {
		if(lotteryNumberService == null) {
			lotteryNumberService = ApplicationContextUtil.getBean("lotteryNumberService");
		}
		return lotteryNumberService;
	}
	
	private LotteryCodeXyService getLotteryCodeXyService() {
		if(lotteryCodeXyService == null) {
			lotteryCodeXyService = ApplicationContextUtil.getBean("lotteryCodeXyService");
		}
		return lotteryCodeXyService;
	}
	private OrderService getOrderService() {
		if(orderService == null) {
			orderService = ApplicationContextUtil.getBean("orderService");
		}
		return orderService;
	}
	
	private LotteryCodeXyImportService getLotteryCodeXyImportService(){
		if (lotteryCodeXyImportService == null) {
			lotteryCodeXyImportService = ApplicationContextUtil.getBean("lotteryCodeXyImportService");
		}
		return lotteryCodeXyImportService;
	}

	/**
	 * 自动开奖的计算
	 */
	@Override
	protected void openDeal() {
		//执行批次号
		String excuteNo = DateUtil.getDate(new Date(), "yyyyMMddHHmmssSSS");
		log.info("开始极速pk10开奖处理，当前执行批次号[{}]", excuteNo);
		Long startTime = System.currentTimeMillis();
		//查找当前最新未开奖期号
        String currentSFM = DateUtils.getDateToStrByFormat(new Date(), "HH:mm:ss");  //当前服务器时间时分秒
        String configDateControl =  ELotteryKind.SHFPK10.getDateControlStr();
        Date queryDate = DateUtils.getDateByStrFormat(configDateControl + " " + currentSFM, "yyyy-MM-dd HH:mm:ss");
      //kr_lottery_number第一期开售开始到截止
        Date startDate = DateUtils.getDateByStrFormat(configDateControl + " 00:00:00", "yyyy-MM-dd HH:mm:ss");
        Date endDate = DateUtils.getDateByStrFormat(configDateControl + " 00:10:10", "yyyy-MM-dd HH:mm:ss");
        
        //获取当前时间前60分钟至当前时间段可开奖的期号 
        if(startDate.getTime() <= queryDate.getTime() &&  queryDate.getTime() < endDate.getTime()){
        	queryDate = DateUtil.addDaysToDate(queryDate, 1);
        }
        
        int dealLotteryNumbersCount = -2;
        if(isFirstRun) {
        	dealLotteryNumbersCount = -60;
        	isFirstRun = false;
        }
        List<LotteryNumber> lotteryNumbers = getLotteryNumberService().getAllLotteryNumbersByCondition(ELotteryKind.SHFPK10.name(), DateUtil.addMinutesToDate(queryDate, dealLotteryNumbersCount) , queryDate);
        //尾期
//        Date flashbackDate1 = DateUtils.getDateByStrFormat("2015-01-05 00:00:00", "yyyy-MM-dd HH:mm:ss");
//        Date flashbackDate2 = DateUtils.getDateByStrFormat("2015-01-05 00:00:10", "yyyy-MM-dd HH:mm:ss");
        //头期 kr_lottery_number昨天最后2期的截止时间
        Date flashbackDate3 = DateUtils.getDateByStrFormat("2015-10-15 23:50:10", "yyyy-MM-dd HH:mm:ss");
        Date flashbackDate4 = DateUtils.getDateByStrFormat("2015-10-16 00:00:10", "yyyy-MM-dd HH:mm:ss");
        //kr_lottery_number今天最后2期的截止时间
        Date flashbackDate1439 = DateUtils.getDateByStrFormat("2015-10-16 23:50:10", "yyyy-MM-dd HH:mm:ss");
        Date flashbackDate1440 = DateUtils.getDateByStrFormat("2015-10-17 00:00:10", "yyyy-MM-dd HH:mm:ss");
        
//        Date flashackDate1375_1 = DateUtils.getDateByStrFormat("2015-01-04 22:55:10", "yyyy-MM-dd HH:mm:ss");
//        Date flashackDate1375_2 = DateUtils.getDateByStrFormat("2015-01-05 00:00:10", "yyyy-MM-dd HH:mm:ss");
        
        Date kaijiangDate = null;
        Date currentDate = new Date();
        Date todayEndDate =  DateUtil.getNowStartTimeByEnd(new Date());
        String currentDateStr = DateUtils.getDateToStrByFormat(currentDate, "yyyy-MM-dd");
        //第一期截止到第三期截止时间段
        Date date1440_1 = DateUtils.getDateByStrFormat(currentDateStr + " 00:10:10", "yyyy-MM-dd HH:mm:ss"); 
        Date date1440_2 = DateUtils.getDateByStrFormat(currentDateStr + " 00:30:10", "yyyy-MM-dd HH:mm:ss"); 

        boolean isToday = false;
        if(currentDate.getTime() <= todayEndDate.getTime()){
        	isToday = true;
        }
        //获取要处理的业务系统盈利模式集合
        Map<Integer, List<String>> dbProfitModelBizSystemMap = getProfitModelBizSystemMap();
        
        if(lotteryNumbers != null && lotteryNumbers.size() >0){
        	LotteryNumber lastLotteryNumber = lotteryNumbers.get(0);
        	long currentTime0 = lastLotteryNumber.getBeginTime().getTime();
        	if(currentTime0 == flashbackDate1440.getTime() && (currentDate.getTime() < date1440_2.getTime())){
            	isToday = false;
            	if (importCode) {
					kaijiangDate = DateUtil.addDaysToDate(currentDate, -1);
				}
        	}else if(currentTime0 == flashbackDate1439.getTime() && (currentDate.getTime() < date1440_1.getTime())){
            	isToday = false;
            	if (importCode) {
					kaijiangDate = DateUtil.addDaysToDate(currentDate, -1);
				}
        	}else{
        		isToday = true;
        		if (importCode) {
					kaijiangDate = currentDate;
					if (flashbackDate3.getTime() <= currentTime0&& currentTime0 <= flashbackDate4.getTime()) {
						kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, -1);
					}
				}
        	}
        	String minIssueNo = null;
			String maxIssueNo = null;
        	List<LotteryCodeXyImport> lotteryCodeXyImports = null;
        	List<LotteryCodeXyImport> updateCodeXyImports = null;//保存开奖后需要更新的数据
			if (importCode) {
				//此处处理CodeXyImport数据，有待开奖的期号，开完奖后要处理状态
				minIssueNo = DateUtils.getDateToStrByFormat(kaijiangDate, "yyyyMMdd") + lotteryNumbers.get(lotteryNumbers.size() - 1).getLotteryNum();
				maxIssueNo = DateUtils.getDateToStrByFormat(kaijiangDate, "yyyyMMdd") + lotteryNumbers.get(0).getLotteryNum();
				lotteryCodeXyImports = getLotteryCodeXyImportService().getByLotteryTypeAndNumInterval(ELotteryKind.SHFPK10.getCode(), minIssueNo,maxIssueNo);
				updateCodeXyImports = new ArrayList<LotteryCodeXyImport>();
			}
            for(LotteryNumber lotteryNumber : lotteryNumbers){
				if (isToday) { // 当日
					kaijiangDate = currentDate;
					long currentTime = lotteryNumber.getBeginTime().getTime();
					if (flashbackDate3.getTime() <= currentTime && currentTime <= flashbackDate4.getTime()) {
						kaijiangDate = DateUtil.addDaysToDate(kaijiangDate, -1);
					}
				} else {
					kaijiangDate = DateUtil.addDaysToDate(currentDate, -1);
				}
				
				lotteryNumber.setLotteryNum(DateUtils.getDateToStrByFormat(kaijiangDate, "yyyyMMdd") + lotteryNumber.getLotteryNum());
				List<LotteryCodeXy> lotteryCodes = new ArrayList<LotteryCodeXy>();
				
				//查询当前期号的所有订单
				List<Order> allDealOrders = null;
				OrderQuery queryOrder = new OrderQuery();
				queryOrder.setExpect(lotteryNumber.getLotteryNum()); // 期号
				queryOrder.setLotteryType(ELotteryKind.SHFPK10.name()); // 彩种
				queryOrder.setOrderStatus(EOrderStatus.GOING.name()); // 进行中状态的订单
				queryOrder.setProstateStatus(EProstateStatus.DEALING.name()); // 处理中的中奖状态
				Long startTime1 = System.currentTimeMillis();
				allDealOrders = getOrderService().getOrderByCondition(queryOrder);
				Long endTime1 = System.currentTimeMillis();
				log.info("{}查询当前期[{}]订单,订单耗时"+(endTime1-startTime1) +"ms", lotteryKind.getDescription(), lotteryNumber.getLotteryNum());
				//业务系统和对应的有效订单的集合
				Map<String, List<Order>> effectiveOrdersMap = new HashMap<String, List<Order>>();
				//业务系统和对应所有订单集合
				Map<String, List<Order>> allOrdersMap = new HashMap<String, List<Order>>();
				if(CollectionUtils.isNotEmpty(allDealOrders)) {
					for(Order order : allDealOrders) {
						String bizSystem = order.getBizSystem();
						List<Order> allOrders = allOrdersMap.get(bizSystem);
						List<Order> effectiveOrders = effectiveOrdersMap.get(bizSystem);
						if(allOrders == null) {
							allOrders = new ArrayList<Order>();
						}
						if(effectiveOrders == null) {
							effectiveOrders = new ArrayList<Order>();
						}
						//添加所有订单集合Map
						allOrders.add(order);
						allOrdersMap.put(bizSystem, allOrders);
						
						//添加有效订单集合Map
						if(order.getEnabled() == 1) {
							effectiveOrders.add(order);
							effectiveOrdersMap.put(bizSystem, effectiveOrders);
						}
					}
				}
				
				//根据订单量重新获取Map
				Map<Integer, List<String>> profitModelBizSystemMap = resetProfitModelBizSystemMap(dbProfitModelBizSystemMap, effectiveOrdersMap, lotteryNumber.getLotteryNum());
				
				//开始遍历处理各业务系统的开奖模式
				Iterator<Integer> iterator = profitModelBizSystemMap.keySet().iterator();
				while(iterator.hasNext()) {
					Integer key = iterator.next();
					List<String> bizSystems = profitModelBizSystemMap.get(key);
					//开奖模式就是key
					int shfpk10ProfitModel = key;
					
					if(CollectionUtils.isNotEmpty(bizSystems)) {
						String profitModelDes = "";
						//随机模式
						if (shfpk10ProfitModel == 0) {
							profitModelDes = "随机模式";
							LotteryCode lotteryCode = new LotteryCode();
							lotteryCode.setLotteryNum(lotteryNumber.getLotteryNum()); // 期号
							lotteryCode.setLotteryName(ELotteryKind.SHFPK10.name()); // 类型为极速PK10
							
							List<String> openCodes = getOpenCodeList();
							lotteryCode.setNumInfo1(openCodes.get(0));
							lotteryCode.setNumInfo2(openCodes.get(1));
							lotteryCode.setNumInfo3(openCodes.get(2));
							lotteryCode.setNumInfo4(openCodes.get(3));
							lotteryCode.setNumInfo5(openCodes.get(4));
							lotteryCode.setNumInfo6(openCodes.get(5));
							lotteryCode.setNumInfo7(openCodes.get(6));
							lotteryCode.setNumInfo8(openCodes.get(7));
							lotteryCode.setNumInfo9(openCodes.get(8));
							lotteryCode.setNumInfo10(openCodes.get(9));
							lotteryCode.setKjtime(new Date()); // 当前时间为开奖时间
							lotteryCode.setProstateDeal(0); // 默认设置未还未进行中奖的处理
							for (String bizStr : bizSystems) {
								try {
									List<Order> allOrders = allOrdersMap.get(bizStr);
									//标记是否使用开奖号码导入
									boolean useCodeImp = false;
									LotteryCode lotteryCodeImp = null;
									if (importCode) {
										//将开奖号改为从CodeXyImport表中获取的数据开奖号,
										for (LotteryCodeXyImport lCI : lotteryCodeXyImports) {
											if (lCI.getBizSystem().equals(bizStr)&& lCI.getIssueNo().equals(lotteryNumber.getLotteryNum())) {
												log.info("开奖使用CodeXyImport表数据开奖,业务系统[{}],彩种[{}],期号[{}]",bizStr,ELotteryKind.SHFPK10.getCode(),lotteryNumber.getLotteryNum());
												String[] oCs = lCI.getOpenCode().split(",");
												lotteryCodeImp = new LotteryCode();
												lotteryCodeImp.setLotteryNum(lotteryNumber.getLotteryNum()); // 期号
												lotteryCodeImp.setLotteryName(ELotteryKind.SHFPK10.name());
												lotteryCodeImp.setNumInfo1(oCs[0]);
												lotteryCodeImp.setNumInfo2(oCs[1]);
												lotteryCodeImp.setNumInfo3(oCs[2]);
												lotteryCodeImp.setNumInfo4(oCs[3]);
												lotteryCodeImp.setNumInfo5(oCs[4]);
												lotteryCodeImp.setNumInfo6(oCs[5]);
												lotteryCodeImp.setNumInfo7(oCs[6]);
												lotteryCodeImp.setNumInfo8(oCs[7]);
												lotteryCodeImp.setNumInfo9(oCs[8]);
												lotteryCodeImp.setNumInfo10(oCs[9]);
												lotteryCodeImp.setKjtime(new Date()); // 当前时间为开奖时间
												lotteryCodeImp.setProstateDeal(0); // 默认设置未还未进行中奖的处理
												lCI.setUsedStatus(1);
												updateCodeXyImports.add(lCI);
												//标志使用开奖号码导入
												useCodeImp = true;
												break;
											}
										}
									}
									//若使用了开奖号码导入，则使用导入的开奖号码处理
									if(useCodeImp) {
											boolean isTheLast = this.prostate(lotteryCodeImp, bizStr,allOrders, profitModelDes);
											if (!isTheLast) {
												LotteryCodeXy lotteryCodeXy = new LotteryCodeXy();
												BeanUtilsExtends.copyProperties(lotteryCodeXy,lotteryCodeImp);
												lotteryCodeXy.setBizSystem(bizStr);
												lotteryCodes.add(lotteryCodeXy);
											}
									} else {
											boolean isTheLast = this.prostate(lotteryCode, bizStr,allOrders, profitModelDes);
											if (!isTheLast) {
												LotteryCodeXy lotteryCodeXy = new LotteryCodeXy();
												BeanUtilsExtends.copyProperties(lotteryCodeXy,lotteryCode);
												lotteryCodeXy.setBizSystem(bizStr);
												lotteryCodes.add(lotteryCodeXy);
											}
									}
								} catch (Exception e) {
									log.error("进行业务系统[{}],彩种[{}]开奖处理出现异常", bizStr, lotteryKind.getDescription(), e);
								}
							}
						//随机盈利模式
						} else if (shfpk10ProfitModel == 1) {
							profitModelDes = "随机盈利模式";
							//各系统分别进行开奖处理
							for (String bizStr : bizSystems) {
								try {
									BigDecimal currentExpectProfis = null;
									// 是否找到可以盈利的开奖号码
									boolean isGetProfitCode = false; 
									List<BigDecimal> prifits = new ArrayList<BigDecimal>();
									Map<BigDecimal, LotteryCode> profitsLotteryCodeMap = new HashMap<BigDecimal, LotteryCode>();
									List<Order> dealOrders = effectiveOrdersMap.get(bizStr);
									List<Order> allOrders = allOrdersMap.get(bizStr);
									
									//标记是否使用开奖号码导入
									boolean useCodeImp = false;
									LotteryCode lotteryCodeImp = null;
									if (importCode) {
										//将开奖号改为从CodeXyImport表中获取的数据开奖号,
										for (LotteryCodeXyImport lCI : lotteryCodeXyImports) {
											if (lCI.getBizSystem().equals(bizStr)&& lCI.getIssueNo().equals(lotteryNumber.getLotteryNum())) {
												log.info("开奖使用CodeXyImport表数据开奖,业务系统[{}],彩种[{}],期号[{}]",bizStr,ELotteryKind.SHFPK10.getCode(),lotteryNumber.getLotteryNum());
												String[] oCs = lCI.getOpenCode().split(",");
												lotteryCodeImp = new LotteryCode();
												lotteryCodeImp.setLotteryNum(lotteryNumber.getLotteryNum()); // 期号
												lotteryCodeImp.setLotteryName(ELotteryKind.SHFPK10.name());
												lotteryCodeImp.setNumInfo1(oCs[0]);
												lotteryCodeImp.setNumInfo2(oCs[1]);
												lotteryCodeImp.setNumInfo3(oCs[2]);
												lotteryCodeImp.setNumInfo4(oCs[3]);
												lotteryCodeImp.setNumInfo5(oCs[4]);
												lotteryCodeImp.setNumInfo6(oCs[5]);
												lotteryCodeImp.setNumInfo7(oCs[6]);
												lotteryCodeImp.setNumInfo8(oCs[7]);
												lotteryCodeImp.setNumInfo9(oCs[8]);
												lotteryCodeImp.setNumInfo10(oCs[9]);
												lotteryCodeImp.setKjtime(new Date()); // 当前时间为开奖时间
												lotteryCodeImp.setProstateDeal(0); // 默认设置未还未进行中奖的处理
												lCI.setUsedStatus(1);
												updateCodeXyImports.add(lCI);
												//标志使用开奖号码导入
												useCodeImp = true;
												break;
											}
										}
									}
									//若使用了开奖号码导入，则使用导入的开奖号码处理
									if (useCodeImp) {
											boolean isTheLast = this.prostate(lotteryCodeImp, bizStr,allOrders, profitModelDes);
											if (!isTheLast) {
												LotteryCodeXy lotteryCodeXy = new LotteryCodeXy();
												BeanUtilsExtends.copyProperties(lotteryCodeXy,lotteryCodeImp);
												lotteryCodeXy.setBizSystem(bizStr);
												lotteryCodes.add(lotteryCodeXy);
											}
									} else {
										LotteryCode lotteryCode = new LotteryCode();
										lotteryCode.setLotteryNum(lotteryNumber.getLotteryNum()); // 期号
										lotteryCode.setLotteryName(ELotteryKind.SHFPK10.name()); // 类型为极速PK10
										for (int i = 0; i < 10000; i++) {
											List<String> openCodes = getOpenCodeList();
											lotteryCode.setNumInfo1(openCodes.get(0));
											lotteryCode.setNumInfo2(openCodes.get(1));
											lotteryCode.setNumInfo3(openCodes.get(2));
											lotteryCode.setNumInfo4(openCodes.get(3));
											lotteryCode.setNumInfo5(openCodes.get(4));
											lotteryCode.setNumInfo6(openCodes.get(5));
											lotteryCode.setNumInfo7(openCodes.get(6));
											lotteryCode.setNumInfo8(openCodes.get(7));
											lotteryCode.setNumInfo9(openCodes.get(8));
											lotteryCode.setNumInfo10(openCodes.get(9));
											lotteryCode.setKjtime(new Date()); // 当前时间为开奖时间
											lotteryCode.setProstateDeal(0); // 默认设置未还未进行中奖的处理
											
											// 盈利计算
											currentExpectProfis = getCurrentOpenCodesProfit(dealOrders, lotteryCode);
											// 不计算盈利
											if (currentExpectProfis.compareTo(new BigDecimal("-200")) > 0 || currentExpectProfis.compareTo(BigDecimal.ZERO) > 0 || CollectionUtils.isEmpty(dealOrders)) { // 如果这个开奖号码的盈利大于0
												isGetProfitCode = true;
													boolean isTheLast = this.prostate(lotteryCode, bizStr, allOrders, profitModelDes);
													if(!isTheLast) {
														LotteryCodeXy lotteryCodeXy = new LotteryCodeXy();
														BeanUtilsExtends.copyProperties(lotteryCodeXy, lotteryCode);
														lotteryCodeXy.setBizSystem(bizStr);
														lotteryCodes.add(lotteryCodeXy);
													}
												break;
											} else {
												prifits.add(currentExpectProfis);
												profitsLotteryCodeMap.put(currentExpectProfis, lotteryCode);
											}
										}
										
										// 如果随机获取的10000中,未获取到可以盈利的开奖号码
										if (!isGetProfitCode) {
											if (prifits.size() > 0) {
												Collections.sort(prifits, new Comparator<BigDecimal>() {
													public int compare(BigDecimal p1, BigDecimal p2) {
														return p1.compareTo(p2);
													}
												});
												BigDecimal[] profitsArray = prifits.toArray(new BigDecimal[0]);
												lotteryCode = profitsLotteryCodeMap.get(profitsArray[0]);
													boolean isTheLast = this.prostate(lotteryCode, bizStr, allOrders, profitModelDes);
													if(!isTheLast) {
														LotteryCodeXy lotteryCodeXy = new LotteryCodeXy();
														BeanUtilsExtends.copyProperties(lotteryCodeXy, lotteryCode);
														lotteryCodeXy.setBizSystem(bizStr);
														lotteryCodes.add(lotteryCodeXy);
													}
											} else {
												throw new RuntimeException("未找到可开奖的号码，盈利的排序不正确");
											}
										}
									}
								} catch (Exception e) {
									log.error("进行业务系统[{}],彩种[{}]开奖处理出现异常", bizStr, lotteryKind.getDescription(), e);
								}
							}
						//绝对盈利模式
						} else if (shfpk10ProfitModel == 2) {
							profitModelDes = "绝对盈利模式";
							//各系统分别进行开奖处理
							for (String bizStr : bizSystems) {
								try {
									BigDecimal currentExpectProfis = null;
									// 是否找到可以盈利的开奖号码
									boolean isGetProfitCode = false; 
									List<BigDecimal> prifits = new ArrayList<BigDecimal>();
									Map<BigDecimal, LotteryCode> profitsLotteryCodeMap = new HashMap<BigDecimal, LotteryCode>();
									List<Order> dealOrders = effectiveOrdersMap.get(bizStr);
									List<Order> allOrders = allOrdersMap.get(bizStr);
									
									//标记是否使用开奖号码导入
									boolean useCodeImp = false;
									LotteryCode lotteryCodeImp = null;
									if (importCode) {
										//将开奖号改为从CodeXyImport表中获取的数据开奖号,
										for (LotteryCodeXyImport lCI : lotteryCodeXyImports) {
											if (lCI.getBizSystem().equals(bizStr)&& lCI.getIssueNo().equals(lotteryNumber.getLotteryNum())) {
												log.info("开奖使用CodeXyImport表数据开奖,业务系统[{}],彩种[{}],期号[{}]",bizStr,ELotteryKind.SHFPK10.getCode(),lotteryNumber.getLotteryNum());
												String[] oCs = lCI.getOpenCode().split(",");
												lotteryCodeImp = new LotteryCode();
												lotteryCodeImp.setLotteryNum(lotteryNumber.getLotteryNum()); // 期号
												lotteryCodeImp.setLotteryName(ELotteryKind.SHFPK10.name());
												lotteryCodeImp.setNumInfo1(oCs[0]);
												lotteryCodeImp.setNumInfo2(oCs[1]);
												lotteryCodeImp.setNumInfo3(oCs[2]);
												lotteryCodeImp.setNumInfo4(oCs[3]);
												lotteryCodeImp.setNumInfo5(oCs[4]);
												lotteryCodeImp.setNumInfo6(oCs[5]);
												lotteryCodeImp.setNumInfo7(oCs[6]);
												lotteryCodeImp.setNumInfo8(oCs[7]);
												lotteryCodeImp.setNumInfo9(oCs[8]);
												lotteryCodeImp.setNumInfo10(oCs[9]);
												lotteryCodeImp.setKjtime(new Date()); // 当前时间为开奖时间
												lotteryCodeImp.setProstateDeal(0); // 默认设置未还未进行中奖的处理
												lCI.setUsedStatus(1);
												updateCodeXyImports.add(lCI);
												//标志使用开奖号码导入
												useCodeImp = true;
												break;
											}
										}
									}
									//若使用了开奖号码导入，则使用导入的开奖号码处理
									if (useCodeImp) {
											boolean isTheLast = this.prostate(lotteryCodeImp, bizStr,allOrders, profitModelDes);
											if (!isTheLast) {
												LotteryCodeXy lotteryCodeXy = new LotteryCodeXy();
												BeanUtilsExtends.copyProperties(lotteryCodeXy,lotteryCodeImp);
												lotteryCodeXy.setBizSystem(bizStr);
												lotteryCodes.add(lotteryCodeXy);
											}
									} else {
										LotteryCode lotteryCode = new LotteryCode();
										lotteryCode.setLotteryNum(lotteryNumber.getLotteryNum()); // 期号
										lotteryCode.setLotteryName(ELotteryKind.SHFPK10.name()); // 类型为极速PK10
										for (int i = 0; i < 10000; i++) {
											List<String> openCodes = getOpenCodeList();
											lotteryCode.setNumInfo1(openCodes.get(0));
											lotteryCode.setNumInfo2(openCodes.get(1));
											lotteryCode.setNumInfo3(openCodes.get(2));
											lotteryCode.setNumInfo4(openCodes.get(3));
											lotteryCode.setNumInfo5(openCodes.get(4));
											lotteryCode.setNumInfo6(openCodes.get(5));
											lotteryCode.setNumInfo7(openCodes.get(6));
											lotteryCode.setNumInfo8(openCodes.get(7));
											lotteryCode.setNumInfo9(openCodes.get(8));
											lotteryCode.setNumInfo10(openCodes.get(9));
											lotteryCode.setKjtime(new Date()); // 当前时间为开奖时间
											lotteryCode.setProstateDeal(0); // 默认设置未还未进行中奖的处理
											
											// 盈利计算
											currentExpectProfis = getCurrentOpenCodesProfit(dealOrders, lotteryCode);
											// 不计算盈利
											if (currentExpectProfis.compareTo(BigDecimal.ZERO) > 0 || CollectionUtils.isEmpty(dealOrders)) { // 如果这个开奖号码的盈利大于0
												isGetProfitCode = true;
													boolean isTheLast = this.prostate(lotteryCode, bizStr, allOrders, profitModelDes);
													if(!isTheLast) {
														LotteryCodeXy lotteryCodeXy = new LotteryCodeXy();
														BeanUtilsExtends.copyProperties(lotteryCodeXy, lotteryCode);
														lotteryCodeXy.setBizSystem(bizStr);
														lotteryCodes.add(lotteryCodeXy);
													}
												break;
											} else {
												prifits.add(currentExpectProfis);
												profitsLotteryCodeMap.put(currentExpectProfis, lotteryCode);
											}
										}
										
										// 如果随机获取的10000中,未获取到可以盈利的开奖号码
										if (!isGetProfitCode) {
											if (prifits.size() > 0) {
												Collections.sort(prifits, new Comparator<BigDecimal>() {
													public int compare(BigDecimal p1, BigDecimal p2) {
														return p1.compareTo(p2);
													}
												});
												BigDecimal[] profitsArray = prifits.toArray(new BigDecimal[0]);
												lotteryCode = profitsLotteryCodeMap.get(profitsArray[0]);
													boolean isTheLast = this.prostate(lotteryCode, bizStr, allOrders, profitModelDes);
													if(!isTheLast) {
														LotteryCodeXy lotteryCodeXy = new LotteryCodeXy();
														BeanUtilsExtends.copyProperties(lotteryCodeXy, lotteryCode);
														lotteryCodeXy.setBizSystem(bizStr);
														lotteryCodes.add(lotteryCodeXy);
													}
											} else {
												throw new RuntimeException("未找到可开奖的号码，盈利的排序不正确");
											}
										}
									}
								} catch (Exception e) {
									log.error("进行业务系统[{}],彩种[{}]开奖处理出现异常", bizStr, lotteryKind.getDescription(), e);
								}
							}
						//嬴率控制模式
						}else if(shfpk10ProfitModel == 3){
							profitModelDes = "嬴率控制模式";
							
							for (String bizStr : bizSystems) {
								try {
									//获取有效的订单
									List<Order> bizDealOrders = effectiveOrdersMap.get(bizStr);
									List<Order> allOrders = allOrdersMap.get(bizStr);
									
									//标记是否使用开奖号码导入
									boolean useCodeImp = false;
									LotteryCode lotteryCodeImp = null;
									if (importCode) {
										//将开奖号改为从CodeXyImport表中获取的数据开奖号,
										for (LotteryCodeXyImport lCI : lotteryCodeXyImports) {
											if (lCI.getBizSystem().equals(bizStr)&& lCI.getIssueNo().equals(lotteryNumber.getLotteryNum())) {
												log.info("开奖使用CodeXyImport表数据开奖,业务系统[{}],彩种[{}],期号[{}]",bizStr,ELotteryKind.SHFPK10.getCode(),lotteryNumber.getLotteryNum());
												String[] oCs = lCI.getOpenCode().split(",");
												lotteryCodeImp = new LotteryCode();
												lotteryCodeImp.setLotteryNum(lotteryNumber.getLotteryNum()); // 期号
												lotteryCodeImp.setLotteryName(ELotteryKind.SHFPK10.name());
												lotteryCodeImp.setNumInfo1(oCs[0]);
												lotteryCodeImp.setNumInfo2(oCs[1]);
												lotteryCodeImp.setNumInfo3(oCs[2]);
												lotteryCodeImp.setNumInfo4(oCs[3]);
												lotteryCodeImp.setNumInfo5(oCs[4]);
												lotteryCodeImp.setNumInfo6(oCs[5]);
												lotteryCodeImp.setNumInfo7(oCs[6]);
												lotteryCodeImp.setNumInfo8(oCs[7]);
												lotteryCodeImp.setNumInfo9(oCs[8]);
												lotteryCodeImp.setNumInfo10(oCs[9]);
												lotteryCodeImp.setKjtime(new Date()); // 当前时间为开奖时间
												lotteryCodeImp.setProstateDeal(0); // 默认设置未还未进行中奖的处理
												lCI.setUsedStatus(1);
												updateCodeXyImports.add(lCI);
												//标志使用开奖号码导入
												useCodeImp = true;
												break;
											}
										}
									}
									//若使用了开奖号码导入，则使用导入的开奖号码处理
									if(useCodeImp) {
											boolean isTheLast = this.prostate(lotteryCodeImp, bizStr,allOrders, profitModelDes);
											if (!isTheLast) {
												LotteryCodeXy lotteryCodeXy = new LotteryCodeXy();
												BeanUtilsExtends.copyProperties(lotteryCodeXy,lotteryCodeImp);
												lotteryCodeXy.setBizSystem(bizStr);
												lotteryCodes.add(lotteryCodeXy);
											}
									} else {
										LotteryCode lotteryCode = new LotteryCode();
										lotteryCode.setLotteryNum(lotteryNumber.getLotteryNum()); // 期号
										lotteryCode.setLotteryName(ELotteryKind.SHFPK10.name()); // 类型为极速PK10
										DayLotteryGain dayLotteryGain = null;
										if(bizDealOrders.size()>0){
											DayLotteryGain query = getDayLotteryGainQuery(bizStr, ELotteryKind.SHFPK10.name(), bizDealOrders.get(0).getCreatedDate());	
											//获取当前系统赢率
											dayLotteryGain = dayLotteryGainService.getDayLotteryGainByQuery(query);
										}	
										
										//获取当前系统模式信息
										LotteryControlConfig lotteryControlConfigQuery= new LotteryControlConfig();
										lotteryControlConfigQuery.setBizSystem(bizStr);
										LotteryControlConfig  lcc = lotteryControlConfiService.queryLotteryControlConfigBybizSystem(lotteryControlConfigQuery);
										//介入控制标识  1.介入控制,按百分比概率判断是否进入绝对盈利开奖   2 不介入控制，随机智能模式
										int flag = 2;
										//判断哪种模式，当日盈率判断是小于最低=>绝对盈利[1]  ，大于最低和小于最高=>随机[2]，大于最高=>傻瓜模式[2]
										if(dayLotteryGain != null){
											//当天投注额大于 多少才介入控制,投注额过小介入控制没有意义
											if(dayLotteryGain.getPayMoney().compareTo(new BigDecimal("1000")) > 0) {
												//当前的赢率小于等于设定要达到的最小赢率，则介入控制
												if(dayLotteryGain.getWinGain().compareTo((lcc.getShfpk10LowestWinGain().divide(new BigDecimal("100"))))<=0){
													flag = 1;
												}else if(dayLotteryGain.getWinGain().compareTo((lcc.getShfpk10LargestWinGain().divide(new BigDecimal("100"))))>0){
													//暂时没有3 减少赢率的控制
													flag = 2;
												}else{
													flag = 2;
												}
											} else {
												flag = 2;
											}
										}else{
											flag = 2;
										}
										
										log.info("业务系统[{}]进行{}开奖,当前期号[{}]，使用赢率控制当前模式flag是:{}"+flag, bizStr, ELotteryKind.SHFPK10.getDescription(), lotteryNumber.getLotteryNum(), flag);
										
										//介入控制模式
										if(flag == 1){
											int lowestModel = 1;//0是随机模式，1 是绝对盈利
											//根据系统设置，获取随机还是绝对盈利
											//获取小于最低百分比的投注额和开奖、随机比例！
											List<Map<String, BigDecimal>> mapList = new ArrayList<Map<String,BigDecimal>>();
											//先默认初始化
											Map<String, BigDecimal> map =  new HashMap<String, BigDecimal>();
											map.put("money", new BigDecimal(200));
											map.put("modelPercent", new BigDecimal(60));
											mapList.add(map);
											map =  new HashMap<String, BigDecimal>();
											map.put("money", new BigDecimal(1000));
											map.put("modelPercent", new BigDecimal(70));
											mapList.add(map);
											map =  new HashMap<String, BigDecimal>();
											map.put("money", new BigDecimal(3000));
											map.put("modelPercent", new BigDecimal(80));
											mapList.add(map);
											map =  new HashMap<String, BigDecimal>();
											map.put("money", new BigDecimal(5000));
											map.put("modelPercent", new BigDecimal(90));
											mapList.add(map);
											map =  new HashMap<String, BigDecimal>();
											map.put("money", new BigDecimal(10000));
											map.put("modelPercent", new BigDecimal(100));
											mapList.add(map);
											BigDecimal profiPayMoney  = getTotalPayMoney(bizDealOrders);
											//获取绝对模式百分比
											BigDecimal modelPercent =  getOpenCodePercent(mapList, profiPayMoney);
											if(modelPercent!=null){
												Integer[] numArr = getModelPercentArr(modelPercent);
												lowestModel = numArr[(int)(Math.random() * numArr.length)];
												log.info(bizStr+":"+ELotteryKind.SHFPK10.getDescription()+"投注额是"+profiPayMoney+",百分比="+modelPercent+",随机抽签的模式="+lowestModel);
											}
											String lowestModelStr = (lowestModel == 0) ? "随机模式" : "绝对模式";
											log.info("业务系统[{}]进行{}开奖,当前期号[{}]，赢率控制当前进入介入控制模式,投注金额[{}]随机抽签模式结果[{}],开奖前上期赢率={}", bizStr, ELotteryKind.SHFPK10.getDescription(), 
													lotteryNumber.getLotteryNum(), profiPayMoney, lowestModelStr, dayLotteryGain== null?"空":dayLotteryGain.getWinGain());
											
											//随机模式
											if(lowestModel == 0){
												List<String> openCodes = getOpenCodeList();
												
												lotteryCode.setNumInfo1(openCodes.get(0));
												lotteryCode.setNumInfo2(openCodes.get(1));
												lotteryCode.setNumInfo3(openCodes.get(2));
												lotteryCode.setNumInfo4(openCodes.get(3));
												lotteryCode.setNumInfo5(openCodes.get(4));
												lotteryCode.setNumInfo6(openCodes.get(5));
												lotteryCode.setNumInfo7(openCodes.get(6));
												lotteryCode.setNumInfo8(openCodes.get(7));
												lotteryCode.setNumInfo9(openCodes.get(8));
												lotteryCode.setNumInfo10(openCodes.get(9));
												lotteryCode.setKjtime(new Date()); // 当前时间为开奖时间
												lotteryCode.setProstateDeal(0); // 默认设置未还未进行中奖的处理
													boolean isTheLast = this.prostate(lotteryCode, bizStr, allOrders, profitModelDes);
													if(!isTheLast) {
														LotteryCodeXy lotteryCodeXy = new LotteryCodeXy();
														BeanUtilsExtends.copyProperties(lotteryCodeXy, lotteryCode);
														lotteryCodeXy.setBizSystem(bizStr);
														lotteryCodes.add(lotteryCodeXy);
													}
											}else{
												// 是否找到可以盈利的开奖号码
												boolean isGetProfitCode = false;
												//存储当期的投注、中奖、盈利
												Map<BigDecimal, Map<String,BigDecimal>> profitsMap = new HashMap<BigDecimal, Map<String,BigDecimal>>();
												//赢钱集合
												Map<BigDecimal, LotteryCode> winProfitsLotteryCodeMap = new HashMap<BigDecimal, LotteryCode>();
												List<BigDecimal>  winPrifits = new ArrayList<BigDecimal>();
												//亏钱集合
												Map<BigDecimal, LotteryCode> lossProfitsLotteryCodeMap = new HashMap<BigDecimal, LotteryCode>();
												List<BigDecimal>  lossPrifits = new ArrayList<BigDecimal>();
												for (int i = 0; i < 10000; i++) {
													List<String> openCodes = getOpenCodeList();
													lotteryCode.setNumInfo1(openCodes.get(0));
													lotteryCode.setNumInfo2(openCodes.get(1));
													lotteryCode.setNumInfo3(openCodes.get(2));
													lotteryCode.setNumInfo4(openCodes.get(3));
													lotteryCode.setNumInfo5(openCodes.get(4));
													lotteryCode.setNumInfo6(openCodes.get(5));
													lotteryCode.setNumInfo7(openCodes.get(6));
													lotteryCode.setNumInfo8(openCodes.get(7));
													lotteryCode.setNumInfo9(openCodes.get(8));
													lotteryCode.setNumInfo10(openCodes.get(9));
													lotteryCode.setKjtime(new Date()); // 当前时间为开奖时间
													lotteryCode.setProstateDeal(0); // 默认设置未还未进行中奖的处理
													
													// 盈利计算
													Map<String,BigDecimal> currentExpectProfisMap = getCurrentOpenCodesProfitMap(bizDealOrders, lotteryCode);
													BigDecimal profiGain = currentExpectProfisMap.get("gain");
													
													if (profiGain.compareTo(BigDecimal.ZERO) > 0 || CollectionUtils.isEmpty(bizDealOrders)) { // 如果这个开奖号码的盈利大于0
														isGetProfitCode = true;
														winPrifits.add(profiGain);
														winProfitsLotteryCodeMap.put(profiGain, lotteryCode);
														profitsMap.put(profiGain, currentExpectProfisMap);
														//只要拿10个或者空订单只拿一个，直接break
														if(winPrifits.size()== 10 || bizDealOrders.size() == 0){
															break;
														}
													} else {	
														if(lossPrifits.size() < 10 ){
															lossPrifits.add(profiGain);
															lossProfitsLotteryCodeMap.put(profiGain, lotteryCode);
															profitsMap.put(profiGain, currentExpectProfisMap);
														}	 	
													}
													
													
												}
												
												BigDecimal currentPayMoney = BigDecimal.ZERO;
												BigDecimal currentWinMoney = BigDecimal.ZERO;
												BigDecimal winGain =null;
												
												//假如有盈利的号码集合
												if(isGetProfitCode == true && winPrifits.size() >0){
													//正数，按小到大排序
													Collections.sort(winPrifits, new Comparator<BigDecimal>() {
														public int compare(BigDecimal p1, BigDecimal p2) {
															return p1.compareTo(p2);
														}
													});
													/**
													 * 根据赢率计算出最优的开奖号码
													 */
													//取上一期的投注金额 和 中奖金额
													BigDecimal lastPayMoney = dayLotteryGain==null?BigDecimal.ZERO:dayLotteryGain.getPayMoney();
													BigDecimal lastWinMoney = dayLotteryGain==null?BigDecimal.ZERO:dayLotteryGain.getWinMoney();
													
													//取 当期的投注金额 和中奖金额
													for(BigDecimal winPrifitMoney:winPrifits){
														lotteryCode = winProfitsLotteryCodeMap.get(winPrifitMoney);
														currentPayMoney = profitsMap.get(winPrifitMoney).get("payMoney");
														currentWinMoney = profitsMap.get(winPrifitMoney).get("winMoney");
														currentPayMoney = currentPayMoney.add(lastPayMoney);
														currentWinMoney = currentWinMoney.add(lastWinMoney);
														if(currentPayMoney.compareTo(BigDecimal.ZERO) != 0){
															winGain = (currentPayMoney.subtract(currentWinMoney)).divide(currentPayMoney, 4, BigDecimal.ROUND_DOWN);
															//计算出赢率有在区间内，则退出
															if(winGain.compareTo((lcc.getShfpk10LowestWinGain().divide(new BigDecimal(100)))) >= 0){
																break;
															}
														}
													}
													//进行开奖
														boolean isTheLast = this.prostate(lotteryCode, bizStr, allOrders, profitModelDes);
														if(!isTheLast) {
															LotteryCodeXy lotteryCodeXy = new LotteryCodeXy();
															BeanUtilsExtends.copyProperties(lotteryCodeXy, lotteryCode);
															lotteryCodeXy.setBizSystem(bizStr);
															lotteryCodes.add(lotteryCodeXy);
														}
													//没有盈利号码集合，只能在亏损号码计算最优	
												}else{
													//负数，按大到小排序
													Collections.sort(lossPrifits, new Comparator<BigDecimal>() {
														public int compare(BigDecimal p1, BigDecimal p2) {
															return p2.compareTo(p1);
														}
													});
													/**
													 * 根据赢率计算出最优的开奖号码，亏损只取第一个最小亏损
													 */
													BigDecimal[] profitsArray = lossPrifits.toArray(new BigDecimal[0]);
													lotteryCode = lossProfitsLotteryCodeMap.get(profitsArray[0]);
													//进行开奖
														boolean isTheLast = this.prostate(lotteryCode, bizStr, allOrders, profitModelDes);
														if(!isTheLast) {
															LotteryCodeXy lotteryCodeXy = new LotteryCodeXy();
															BeanUtilsExtends.copyProperties(lotteryCodeXy, lotteryCode);
															lotteryCodeXy.setBizSystem(bizStr);
															lotteryCodes.add(lotteryCodeXy);
														}
												}
											}
											//随机模式
										}else{
											log.info("业务系统[{}]进行{}开奖,当前期号[{}]，赢率控制当前进入随机模式,开奖前上期赢率={}", bizStr, ELotteryKind.SHFPK10.getDescription(), 
													lotteryNumber.getLotteryNum(), dayLotteryGain== null?"空":dayLotteryGain.getWinGain());
											List<String> openCodes = getOpenCodeList();
											
											lotteryCode.setNumInfo1(openCodes.get(0));
											lotteryCode.setNumInfo2(openCodes.get(1));
											lotteryCode.setNumInfo3(openCodes.get(2));
											lotteryCode.setNumInfo4(openCodes.get(3));
											lotteryCode.setNumInfo5(openCodes.get(4));
											lotteryCode.setNumInfo6(openCodes.get(5));
											lotteryCode.setNumInfo7(openCodes.get(6));
											lotteryCode.setNumInfo8(openCodes.get(7));
											lotteryCode.setNumInfo9(openCodes.get(8));
											lotteryCode.setNumInfo10(openCodes.get(9));
											lotteryCode.setKjtime(new Date()); // 当前时间为开奖时间
											lotteryCode.setProstateDeal(0); // 默认设置未还未进行中奖的处理
												boolean isTheLast = this.prostate(lotteryCode, bizStr, allOrders, profitModelDes);
												if(!isTheLast) {
													LotteryCodeXy lotteryCodeXy = new LotteryCodeXy();
													BeanUtilsExtends.copyProperties(lotteryCodeXy, lotteryCode);
													lotteryCodeXy.setBizSystem(bizStr);
													lotteryCodes.add(lotteryCodeXy);
												}
										}
									}
								} catch (Exception e) {
									log.error("进行业务系统[{}],彩种[{}]开奖处理出现异常", bizStr, lotteryKind.getDescription(), e);
								}
							}
						}
					} else {
						continue;
					}
				}
				//开始往activemq发送开奖号码
				if(CollectionUtils.isNotEmpty(lotteryCodes)) {
					LotteryCodeSendMessage message = new LotteryCodeSendMessage();
					message.setLotteryCodeXys(lotteryCodes);
					MessageQueueCenter.putMessage(message);
				}
			}
            if (importCode) {
				//对从CodeXyImport表中拿来开奖的数据更新
				if (updateCodeXyImports.size() > 0) {
					try {
						log.info("CodeXyImport表中拿来开奖的数据更新");
						getLotteryCodeXyImportService().setBatchUsedStatus1(
								updateCodeXyImports);
					} catch (Exception e) {
						log.info("CodeXyImport表中拿来开奖的数据更新失败:" + e);
					}
				}
			}
        }
        
        Long endTime = System.currentTimeMillis();
        log.info("结束极速pk10奖处理，当前执行批次号[{}],总耗时["+(endTime - startTime)+"]ms", excuteNo);
	}
	
	
	/**
	 * 获取绝对盈利百分比
	 * @param mapList
	 * @param payMoney
	 * @return
	 */
	private BigDecimal getOpenCodePercent(List<Map<String,BigDecimal>> mapList,BigDecimal payMoney){
		BigDecimal preMoney = BigDecimal.ZERO;
		for(int i =0;i<mapList.size();i++){
			Map<String, BigDecimal> map = mapList.get(i);
			 BigDecimal m = map.get("money");
			 if(payMoney.compareTo(m) < 0){
				 if(i == 0){
					  //绝对盈利 模式 = 0 ，就是随机了
					 return preMoney;
				 }else{
					 map.put("modelPercent", mapList.get(i-1).get("modelPercent"));
					 return mapList.get(i-1).get("modelPercent"); 
				 }
			 }else if(i == (mapList.size()-1)){
	
				 return mapList.get(i).get("modelPercent");
			 }
		 }
		return null;
	}
	
	/**
	 * 动态获取随机和绝对盈利数组
	 * @param modelPercent
	 * @return
	 */
	private Integer[] getModelPercentArr(BigDecimal modelPercent){
		Integer[] num1Arr = new Integer[]{1,1,1,1,1,1,1,1,1,1};
		Integer[] num0Arr = new Integer[]{0,0,0,0,0,0,0,0,0,0};
		Integer num0 = 10 - (modelPercent.intValue()/10);
		if(num0 <= 5){
			for(int i = 0;i<num0;i++){
				num1Arr[i*2] = 0;
			}
			
			return num1Arr;
		}else{
			for(int i = 0;i < (modelPercent.intValue()/10);i++){
				num0Arr[i*2] = 1;
			}
			
			return num0Arr;
		}
	}
	
	/**
	 * 获取查询赢率的条件，根据订单时间判断是否查询当天赢率还是昨天赢率
	 * @param bizSyStem
	 * @param lotteryType
	 * @param date
	 * @return
	 */
	private DayLotteryGain getDayLotteryGainQuery(String bizSyStem,String lotteryType,Date date){
		DayLotteryGain query = new DayLotteryGain();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 3); 
		calendar.set(Calendar.MINUTE,0); 
		calendar.set(Calendar.SECOND,0); 
		calendar.set(Calendar.MILLISECOND, 0);
		//订单时间大于3点，属于当天
		if(date.after(calendar.getTime())){
			calendar.set(Calendar.HOUR_OF_DAY, 12); 
		}else{ //订单时间不超过3点，属于前一天
			calendar.add(Calendar.DATE, -1);
			calendar.set(Calendar.HOUR_OF_DAY, 12);
		}
		
		query.setBizSystem(bizSyStem);
	    query.setBelongDate(calendar.getTime());
		query.setLotteryType(lotteryType);
		return query;
	}
	
	/**
	 * 获取极速PK10盈利模式需要处理的集合
	 * @return
	 */
	private Map<Integer, List<String>> getProfitModelBizSystemMap(){
		Map<Integer, List<String>> profitModelBizSystemMap = new LinkedHashMap<Integer, List<String>>();
		
		//默认的业务系统配置对象
		LotteryControlConfig defaultSetConfig = null;
		//使用默认配置的业务系统集合
		List<String> defaultSetBizSystem = new ArrayList<String>();
		
		LotteryControlConfig query = new LotteryControlConfig();
		//需要处理的业务系统集合，包含启用状态的业务系统集合和一条默认的业务系统配置
		List<LotteryControlConfig> prostateList = new ArrayList<LotteryControlConfig>();
		List<LotteryControlConfig> list = lotteryControlConfiService.getAllConfigs(query);
		for(LotteryControlConfig lotteryControlConfig : list){
			String bizSystem = lotteryControlConfig.getBizSystem();
			//超级系统的记录为默认的设置值
			if("SUPER_SYSTEM".equals(bizSystem)){
				defaultSetConfig = lotteryControlConfig;
			}else{
				//如果当前业务系统被禁用，则不处理当前系统的订单
				BizSystem bizSystemCache = ClsCacheManager.getBizSystemByCode(bizSystem);
				if(bizSystemCache == null || bizSystemCache.getEnable() == 0) {
					continue;
				}
				//启用状态则添加到处理的业务系统集合去
				if(lotteryControlConfig.getEnabled() == 1){
					prostateList.add(lotteryControlConfig);
				}else{
					defaultSetBizSystem.add(lotteryControlConfig.getBizSystem());
				}
			}
			
		}
		//构建Map
		for(LotteryControlConfig lotteryControlConfig : prostateList) {
			Integer key = lotteryControlConfig.getShfpk10ProfitModel();
			//如果业务系统配置项为null,则改为随机模式
			if(null == key) {
				log.error("业务系统{}当前彩种{}彩种控制配置项值为null", lotteryControlConfig.getBizSystem(), lotteryKind.getDescription());
				key = 2;
			}
			List<String> value = profitModelBizSystemMap.get(key);
			if(value == null) {
				value = new ArrayList<String>();
			}
			value.add(lotteryControlConfig.getBizSystem());
			profitModelBizSystemMap.put(key, value);
		}
		if(defaultSetConfig != null) {
			Integer key = defaultSetConfig.getShfpk10ProfitModel();
			//如果默认项为null,则改为随机模式
			if(null == key) {
				log.error("当前彩种{}默认的彩种控制配置项值为null",lotteryKind.getDescription());
				key = 2;
			}
			List<String> value = profitModelBizSystemMap.get(key);
			if(value == null) {
				value = new ArrayList<String>();
			}
			if(CollectionUtils.isNotEmpty(defaultSetBizSystem)) {
				value.addAll(defaultSetBizSystem);
				profitModelBizSystemMap.put(key, value);
			}
		}
		return profitModelBizSystemMap;
	}
	
	/**
	 * 根据是否有订单与开奖号码是否存在 重新设置开奖模式与业务系统集合Map
	 * @param profitModelBizSystemMap
	 * @param effectiveOrdersMap
	 * @return
	 */
	private Map<Integer, List<String>> resetProfitModelBizSystemMap(Map<Integer, List<String>> profitModelBizSystemMap, Map<String, List<Order>> effectiveOrdersMap, String lotteryNum) {
		Map<Integer, List<String>> profitModelBizSystemMapNew = new LinkedHashMap<Integer, List<String>>();
		Iterator<Integer> iterator = profitModelBizSystemMap.keySet().iterator();
		while(iterator.hasNext()) {
			Integer key = iterator.next();
			List<String> value = profitModelBizSystemMap.get(key);
			if(CollectionUtils.isNotEmpty(value)) {
				for(String bizSystem : value) {
					log.debug("业务系统[{}],{}当前使用盈利模式[{}]", bizSystem, lotteryKind.getDescription(), key);
					
					Integer keyNew = key.intValue();
					List<Order> bizSystemOrders = effectiveOrdersMap.get(bizSystem);
					//如果订单为空，则置为随机开奖模式
					if(CollectionUtils.isEmpty(bizSystemOrders)) {
						keyNew = 0;
						log.debug("查询到业务系统[{}],{}订单数为空，改为使用随机盈利模式", bizSystem, lotteryKind.getDescription());
					}
					//添加新Map数据
					List<String> bizSystems = profitModelBizSystemMapNew.get(keyNew);
					if(CollectionUtils.isEmpty(bizSystems)) {
						bizSystems = new ArrayList<String>();
					}
					bizSystems.add(bizSystem);
					profitModelBizSystemMapNew.put(keyNew, bizSystems);
				}
			}
		}
		return profitModelBizSystemMapNew;
	}
	
	/**
	 * 中奖处理
	 * @param lotteryCode
	 * @throws Exception
	 */
	public boolean prostate(LotteryCode lotteryCode,String bizSystem,List<Order> dealOrders, String profitModelDes) throws Exception{
		lotteryCode.setLotteryType(ELotteryKind.SHFPK10.name());
		boolean isTheLast = false; 
		LotteryCodeXy lotteryCodeXy = new LotteryCodeXy();
		BeanUtilsExtends.copyProperties(lotteryCodeXy, lotteryCode);
		lotteryCodeXy.setBizSystem(bizSystem);
		isTheLast = this.getLotteryCodeXyService().getLotteryCodeXyByKindAndExpectExist(lotteryCodeXy, lotteryCode.getLotteryType());
		//如果没有这样子的期号,此时做期号是否中奖的处理
        if(!isTheLast){
        	//拼接开奖号码
        	String openCodeStr = lotteryCode.getOpencodeStr(); //开奖号码拼接字符串
    		//查询到有订单起新线程处理中奖订单
    		if(CollectionUtils.isNotEmpty(dealOrders)) {
    			lotteryCodeXy.setProstateDeal(0);
    			lotteryCode.setProstateDeal(0);  
    			this.getLotteryCodeXyService().insertSelective(lotteryCodeXy);
        		log.info("业务系统["+lotteryCodeXy.getBizSystem()+"]新增开奖处理,盈利模式[" + profitModelDes + "],彩种["+lotteryCode.getLotteryType()+"],期号["+lotteryCode.getLotteryNum()+"],开奖号码["+openCodeStr+"],订单数["+dealOrders.size()+"]");
    			
    			MainOpenDealThread mainOpenDealThread = new MainOpenDealThread(lotteryCode, dealOrders, openCodeStr);
	    		mainOpenDealThread.start();
    		} else {
    			
    			lotteryCodeXy.setProstateDeal(1);  //标识为已经进行了订单的中奖处理 
    			this.getLotteryCodeXyService().insertSelective(lotteryCodeXy);
        		log.info("业务系统["+lotteryCodeXy.getBizSystem()+"]新增开奖处理,盈利模式[" + profitModelDes + "],彩种["+lotteryCode.getLotteryType()+"],期号["+lotteryCode.getLotteryNum()+"],开奖号码["+openCodeStr+"],订单数[0]");
    		}
    		CommonOpenCodeService.dealCodeXyTrend(lotteryCode.getLotteryName(), openCodeStr, bizSystem, lotteryCode.getLotteryNum());
        }
        return isTheLast;
	}
	
	
	/**
	 * 计算订单在该开奖号码下的盈利金额
	 * @param dealOrders
	 * @param lotteryCode
	 * @return
	 */
	public BigDecimal getCurrentOpenCodesProfit(List<Order> dealOrders,LotteryCode lotteryCode){
		BigDecimal totalWinCost = new BigDecimal(0); //总共中奖金额
		BigDecimal totalPayCost = new BigDecimal(0); //总共投注金额
		
		if(dealOrders != null && dealOrders.size() > 0){
			for(int i = 0; i < dealOrders.size(); i++){
				Order order = dealOrders.get(i);
				//投注号码类型处理,获取中奖金额
				String lotteryCodes = order.getCodes();
				String[] lotteryCodeArrays = lotteryCodes.split(ConstantUtil.SOURCECODE_PROTYPE_SPLIT);
				for(String lotteryCodeProtype : lotteryCodeArrays){
					int startPosition = lotteryCodeProtype.indexOf("[");
					int endPosition = lotteryCodeProtype.indexOf("]");
					String kindPlay = lotteryCodeProtype.substring(startPosition + 1,endPosition);
					
					int beishuStartPosition = lotteryCodeProtype.lastIndexOf("[");
					int beishuEndPosition =lotteryCodeProtype.lastIndexOf("]");
					String beishuStr = lotteryCodeProtype.substring(beishuStartPosition + 1,beishuEndPosition);
					//判断倍数是否为空或者是否是正整数
					if(StringUtils.isEmpty(beishuStr) || !IntegerUtil.isPositiveNumeric(beishuStr)){
						return new BigDecimal(0);
					}
					int multiple = Integer.parseInt(beishuStr);
					String code = lotteryCodeProtype.substring(endPosition + 1, beishuStartPosition);
							
					//调用对应的是玩法中奖处理
					if(order.getLotteryType().equals(ELotteryKind.SHFPK10.name())){  
						EPK10Kind currentPlayKind = EPK10Kind.valueOf(kindPlay);
						if(StringUtils.isEmpty(currentPlayKind.getServiceClassName())){
							throw new RuntimeException("极速PK10，玩法配置不正常.");
						}
						LotteryKindPlayService jlffcService = (LotteryKindPlayService)ApplicationContextUtil.getBean(currentPlayKind.getServiceClassName());
						if(jlffcService != null){
							totalWinCost = totalWinCost.add(jlffcService.prostateDeal(lotteryCode, code,order.getAwardModel(),multiple,order.getMultiple(),order.getLotteryModel()));
						}else{
							log.error("极速PK10的" + currentPlayKind.getServiceClassName() + "未成功创建实例 .");
							throw new RuntimeException("极速PK10的" + currentPlayKind.getServiceClassName() + "未成功创建实例 .");
						}
					}else{
						throw new RuntimeException("该彩种未配置");
					}
				}
				totalPayCost = totalPayCost.add(order.getPayMoney());  //该期订单总共的投注金额
			}
		}else{
			
		}
		return totalPayCost.subtract(totalWinCost);  
	}
	
	/**
	 * 计算订单在该开奖号码下的投注额，中奖金额，盈利金额
	 * @param dealOrders
	 * @param lotteryCode
	 * @return
	 */
	public BigDecimal getTotalPayMoney(List<Order> dealOrders){
		BigDecimal totalPayCost = new BigDecimal(0); //总共投注金额
		if(dealOrders != null && dealOrders.size() > 0){
			for(int i = 0; i < dealOrders.size(); i++){
				Order order = dealOrders.get(i);
				totalPayCost = totalPayCost.add(order.getPayMoney());  //该期订单总共的投注金额
			}
		}		
		return totalPayCost;
	}
	
	/**
	 * 计算订单在该开奖号码下的投注额，中奖金额，盈利金额
	 * @param dealOrders
	 * @param lotteryCode
	 * @return
	 */
	public Map<String,BigDecimal> getCurrentOpenCodesProfitMap(List<Order> dealOrders,LotteryCode lotteryCode){
		BigDecimal totalWinCost = new BigDecimal(0); //总共中奖金额
		BigDecimal totalPayCost = new BigDecimal(0); //总共投注金额
		BigDecimal totalGain = new BigDecimal(0); //盈利金额
		Map<String,BigDecimal> map =  new HashMap<String, BigDecimal>();
		map.put("winMoney", totalWinCost);
		map.put("payMoney", totalPayCost);
		map.put("gain", totalGain);
		if(dealOrders != null && dealOrders.size() > 0){
			for(int i = 0; i < dealOrders.size(); i++){
				Order order = dealOrders.get(i);
				//投注号码类型处理,获取中奖金额
				String lotteryCodes = order.getCodes();
				String[] lotteryCodeArrays = lotteryCodes.split(ConstantUtil.SOURCECODE_PROTYPE_SPLIT);
				for(String lotteryCodeProtype : lotteryCodeArrays){
					int startPosition = lotteryCodeProtype.indexOf("[");
					int endPosition = lotteryCodeProtype.indexOf("]");
					String kindPlay = lotteryCodeProtype.substring(startPosition + 1,endPosition);
					
					int beishuStartPosition = lotteryCodeProtype.lastIndexOf("[");
					int beishuEndPosition =lotteryCodeProtype.lastIndexOf("]");
					String beishuStr = lotteryCodeProtype.substring(beishuStartPosition + 1,beishuEndPosition);
					//判断倍数是否为空或者是否是正整数
					if(StringUtils.isEmpty(beishuStr) || !IntegerUtil.isPositiveNumeric(beishuStr)){
						return map;
					}
					int multiple = Integer.parseInt(beishuStr);
					String code = lotteryCodeProtype.substring(endPosition + 1, beishuStartPosition);
							
					//调用对应的是玩法中奖处理
					if(order.getLotteryType().equals(ELotteryKind.SHFPK10.name())){  
						EPK10Kind currentPlayKind = EPK10Kind.valueOf(kindPlay);
						if(StringUtils.isEmpty(currentPlayKind.getServiceClassName())){
							throw new RuntimeException("极速PK10，玩法配置不正常.");
						}
						LotteryKindPlayService jlffcService = (LotteryKindPlayService)ApplicationContextUtil.getBean(currentPlayKind.getServiceClassName());
						if(jlffcService != null){
							totalWinCost = totalWinCost.add(jlffcService.prostateDeal(lotteryCode, code,order.getAwardModel(),multiple,order.getMultiple(),order.getLotteryModel()));
						}else{
							log.error("极速PK10的" + currentPlayKind.getServiceClassName() + "未成功创建实例 .");
							throw new RuntimeException("极速PK10的" + currentPlayKind.getServiceClassName() + "未成功创建实例 .");
						}
					}else{
						throw new RuntimeException("该彩种未配置");
					}
				}
				totalPayCost = totalPayCost.add(order.getPayMoney());  //该期订单总共的投注金额
			}
		}else{
			
		}
		map.put("winMoney", totalWinCost);
		map.put("payMoney", totalPayCost);
		map.put("gain", totalPayCost.subtract(totalWinCost));
		return map;  
	}
	
	
	/**
	 * 获取开奖号码List,号码从小到达排序
	 * @return
	 */
	public static List<String> getOpenCodeList() {
		List<String> codesSelectList = new ArrayList<String>(Arrays.asList("01","02","03","04","05","06","07","08","09","10"));
		List<String> codesList = new ArrayList<String>();
		int ramInt = (int)(Math.random() * codesSelectList.size());
		codesList.add(codesSelectList.get(ramInt));
		codesSelectList.remove(ramInt);
		ramInt = (int)(Math.random() * codesSelectList.size());
		codesList.add(codesSelectList.get(ramInt));
		codesSelectList.remove(ramInt);
		ramInt = (int)(Math.random() * codesSelectList.size());
		codesList.add(codesSelectList.get(ramInt));
		codesSelectList.remove(ramInt);
		ramInt = (int)(Math.random() * codesSelectList.size());
		codesList.add(codesSelectList.get(ramInt));
		codesSelectList.remove(ramInt);
		ramInt = (int)(Math.random() * codesSelectList.size());
		codesList.add(codesSelectList.get(ramInt));
		codesSelectList.remove(ramInt);
		ramInt = (int)(Math.random() * codesSelectList.size());
		codesList.add(codesSelectList.get(ramInt));
		codesSelectList.remove(ramInt);
		ramInt = (int)(Math.random() * codesSelectList.size());
		codesList.add(codesSelectList.get(ramInt));
		codesSelectList.remove(ramInt);
		ramInt = (int)(Math.random() * codesSelectList.size());
		codesList.add(codesSelectList.get(ramInt));
		codesSelectList.remove(ramInt);
		ramInt = (int)(Math.random() * codesSelectList.size());
		codesList.add(codesSelectList.get(ramInt));
		codesSelectList.remove(ramInt);
		codesList.add(codesSelectList.get(0));
	
		return codesList;
	}
}
