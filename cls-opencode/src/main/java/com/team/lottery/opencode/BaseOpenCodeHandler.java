package com.team.lottery.opencode;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;

import com.team.lottery.enums.EOpenInterfaceType;
import com.team.lottery.exception.OpenCodeException;
import com.team.lottery.extvo.LotteryResultForCpk;
import com.team.lottery.extvo.LotteryResultForCpkVo;
import com.team.lottery.extvo.LotteryResultForKcw;
import com.team.lottery.extvo.LotteryResultForKcwVo;
import com.team.lottery.extvo.LotteryResultForXcw;
import com.team.lottery.extvo.LotteryResultForXcwVo;
import com.team.lottery.extvo.LotteryResultVo;
import com.team.lottery.service.CommonOpenCodeService;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.JaxbUtil;
import com.team.lottery.vo.LotteryCode;

public abstract class BaseOpenCodeHandler {
	
	private static Logger log = LoggerFactory.getLogger(BaseOpenCodeHandler.class);
	
	protected abstract void openDeal();
	
	/**
	 * 根据开奖结果转换开奖对象
	 * @param result
	 * @param type
	 * @return
	 * @throws OpenCodeException
	 */
	public List<LotteryResultVo> transferOpenCodeInterfaceData(String result, EOpenInterfaceType type) throws OpenCodeException {
		if(StringUtils.isEmpty(result)) {
			throw new OpenCodeException("转换开奖接口数据失败，获取接口数据为空");
		}
		List<LotteryResultVo> lotteryResultVos = new ArrayList<LotteryResultVo>();
		//开彩网接口
		if(/*type.getCode().equals(EOpenInterfaceType.KCW.getCode())*/true) {
			LotteryResultForKcw lotteryResultForKcw = null;
			//把字符串转成对象
			try {
				lotteryResultForKcw = JaxbUtil.transformToObject(LotteryResultForKcw.class, result);
			} catch (JAXBException e1) {
	           throw new OpenCodeException("转换开奖接口数据失败，接口XML数据转为Vo失败", e1);
			}
			if(lotteryResultForKcw == null){
				throw new OpenCodeException("转换开奖接口数据失败，接口XML数据转为Vo对象为空");
			}
			List<LotteryResultForKcwVo> lotteryResultForKcwVos = lotteryResultForKcw.getLotteryResultVos();
			if(CollectionUtils.isNotEmpty(lotteryResultForKcwVos)) {
				try {
					for(LotteryResultForKcwVo resultForKcwVo : lotteryResultForKcwVos) {
						LotteryResultVo resultVo = new LotteryResultVo();
						BeanUtils.copyProperties(resultForKcwVo, resultVo);
						lotteryResultVos.add(resultVo);
					}
				} catch (Exception e) {
					throw new OpenCodeException("转换开奖接口数据失败,复制开彩网数据对象失败", e);
				} 
			} else {
				throw new OpenCodeException("转换开奖接口数据列表为空");
			}
		} else if(type.getCode().equals(EOpenInterfaceType.CPK.getCode())) {
			LotteryResultForCpk lotteryResultForCpk = null;
			//把字符串转成对象
			try {
				lotteryResultForCpk = JaxbUtil.transformToObject(LotteryResultForCpk.class, result);
			} catch (JAXBException e1) {
	           throw new OpenCodeException("转换开奖接口数据失败，接口XML数据转为Vo失败", e1);
			}
			if(lotteryResultForCpk == null){
				throw new OpenCodeException("转换开奖接口数据失败，接口XML数据转为Vo对象为空");
			}
			List<LotteryResultForCpkVo> lotteryResultForCpkVos = lotteryResultForCpk.getLotteryResultVos();
			if(CollectionUtils.isNotEmpty(lotteryResultForCpkVos)) {
				try {
					for(LotteryResultForCpkVo resultForCpkVo : lotteryResultForCpkVos) {
						LotteryResultVo resultVo = new LotteryResultVo();
						BeanUtils.copyProperties(resultForCpkVo, resultVo);
						lotteryResultVos.add(resultVo);
					}
				} catch (Exception e) {
					throw new OpenCodeException("转换开奖接口数据失败,复制开彩网数据对象失败", e);
				} 
			} else {
				throw new OpenCodeException("转换开奖接口数据列表为空");
			}
		}else if(type.getCode().equals(EOpenInterfaceType.XCW.getCode())) {
			LotteryResultForXcw lotteryResultForXcw = null;
			//把字符串转成对象
			try {
				lotteryResultForXcw = JaxbUtil.transformToObject(LotteryResultForXcw.class, result);
			} catch (JAXBException e1) {
	           throw new OpenCodeException("转换开奖接口数据失败，接口XML数据转为Vo失败", e1);
			}
			if(lotteryResultForXcw == null){
				throw new OpenCodeException("转换开奖接口数据失败，接口XML数据转为Vo对象为空");
			}
			List<LotteryResultForXcwVo> lotteryResultForXcwVos = lotteryResultForXcw.getLotteryResultVos();
			if(CollectionUtils.isNotEmpty(lotteryResultForXcwVos)) {
				try {
					for(LotteryResultForXcwVo resultForXcwVo : lotteryResultForXcwVos) {
						LotteryResultVo resultVo = new LotteryResultVo();
						BeanUtils.copyProperties(resultForXcwVo, resultVo);
						lotteryResultVos.add(resultVo);
					}
				} catch (Exception e) {
					throw new OpenCodeException("转换开奖接口数据失败,复制开彩网数据对象失败", e);
				} 
			} else {
				throw new OpenCodeException("转换开奖接口数据列表为空");
			}
		}
		if(CollectionUtils.isEmpty(lotteryResultVos)) {
			throw new OpenCodeException("转换开奖接口数据列表为空");
		}
		//将开奖结果进行逆向排序，最新的期数排在后面
		//Collections.reverse(lotteryResultVos);
		return lotteryResultVos;
	}
	

	/**
	 * 处理开奖接口数据
	 * 此模块应当要到LotteryCodeService处理
	 * @param result
	 * @return
	 */
	public void dealLotteryResults(List<LotteryCode> lotteryCodes){
		
		int currentCount = 0;
		if(CollectionUtils.isNotEmpty(lotteryCodes)){
			//先查询当前最新的开奖号码
			/*if(lotteryCodeService == null) {
				lotteryCodeService = (LotteryCodeService)ApplicationContextUtil.getBean("lotteryCodeService");
			}
			LotteryCode theNewLotteryCode = lotteryCodeService.getLastLotteryCode(lotteryKind);*/
			
			for(LotteryCode lotteryCode : lotteryCodes){
				try {
					//如果数据库中已经有此开奖号码，则无需再进行订单处理
					/*if(theNewLotteryCode != null && lotteryCode.getLotteryNum().equals(theNewLotteryCode.getLotteryNum())) {
						break;
					}*/
					//进行订单处理
					CommonOpenCodeService.prostate(lotteryCode);
				} catch (Exception e) {
					log.error("处理开奖彩种类型[{}],期号[{}]出现异常",lotteryCode.getLotteryType(), lotteryCode.getLotteryNum(), e);
				}
				
				currentCount++;
				//如果该期号是已经存在的开奖期号,则退出遍历,开奖期数达到10期
				if(currentCount == ConstantUtil.opcodeCount){   //if(isTheLast)
				   break;	
				}
			}
		}
	}
	
	
}
