package com.team.lottery.opencode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.activemq.ActiveMqConfig;
import com.team.lottery.enums.EOrderStatus;
import com.team.lottery.enums.EProstateStatus;
import com.team.lottery.extvo.OrderQuery;
import com.team.lottery.service.CommonOpenCodeService;
import com.team.lottery.service.LotteryCodeXyService;
import com.team.lottery.service.OrderService;
import com.team.lottery.system.MainOpenDealThread;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.util.BeanUtilsExtends;
import com.team.lottery.util.GetApplicationPropertiesValueUtil;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryCodeXy;
import com.team.lottery.vo.Order;

/**
 * 
 * @author luocheng
 * @deprecated 弃用，改用jms监听器来实现，参考LotteryCodeQueueReceiver类
 */
public class AutoXnXyThread implements Runnable{

	private static Logger logger = LoggerFactory.getLogger(AutoXnXyThread.class);
	
    ConnectionFactory connectionFactory;

    Connection connection;

    Session session;
	
	public static final String QUEUE_NAME = "xycode-queue";
	public static final String TOPIC_NAME = "xycode-topic";
	
	boolean flag = true;
	
	private LotteryCodeXyService lotteryCodeXyService;
	
	private OrderService orderService;
	
	private void init(){
        try {
        	logger.info("开始初始化AutoXnXyThread连接信息");
        	connectionFactory = new ActiveMQConnectionFactory(ActiveMqConfig.USERNAME, 
            		ActiveMqConfig.PASSWORD, ActiveMqConfig.BROKEN_URL);
            connection  = connectionFactory.createConnection();
            connection.start();
            session = connection.createSession(false,Session.AUTO_ACKNOWLEDGE);
            logger.info("初始化AutoXnXyThread连接信息成功");
        } catch (JMSException e) {
        	logger.error("初始化AutoXnXyThread连接信息失败", e);
        }
    }
	
	@Override
	public void run() {
		
		this.init();
		
		try {
            Queue queue = session.createQueue(QUEUE_NAME);
//			Destination destination = session.createTopic(TOPIC_NAME);
            MessageConsumer consumer = session.createConsumer(queue);
            //topic处理方式
//            consumer.setMessageListener(new LotteryCodeListener());
            
            //以下是queue处理方式
            while(flag) {
            	Message message = consumer.receive();
            	if(message instanceof ObjectMessage){  
            		try {
            			ObjectMessage objMsg = (ObjectMessage) message;  
            			List<?> lotteryCodes = (List<?>)objMsg.getObject();;
            			if(CollectionUtils.isNotEmpty(lotteryCodes)) {
            				//取第一个开奖号码查询订单和期号
            				Object firstObj = lotteryCodes.get(0);
            				String expect = "00000000";
            				String lotteryType = "XXX";
            				if(firstObj instanceof LotteryCode) {
            					LotteryCode lotteryCode = (LotteryCode)firstObj;
            					expect = lotteryCode.getLotteryNum();
            					lotteryType = lotteryCode.getLotteryName();
            				} else if(firstObj instanceof LotteryCodeXy) {
            					LotteryCodeXy lotteryCodeXy = (LotteryCodeXy)firstObj;
            					expect = lotteryCodeXy.getLotteryNum();
            					lotteryType = lotteryCodeXy.getLotteryName();
            				}
            				
            				//查询当前期号的所有订单
            				List<Order> allDealOrders = null;
            				OrderQuery queryOrder = new OrderQuery();
            				queryOrder.setExpect(expect); // 期号
            				queryOrder.setLotteryType(lotteryType); // 彩种
            				queryOrder.setOrderStatus(EOrderStatus.GOING.name()); // 进行中状态的订单
            				queryOrder.setProstateStatus(EProstateStatus.DEALING.name()); // 处理中的中奖状态
            				allDealOrders = getOrderService().getOrderByCondition(queryOrder);
            				
            				//业务系统和对应所有订单集合
            				Map<String, List<Order>> allOrdersMap = new HashMap<String, List<Order>>();
            				if(CollectionUtils.isNotEmpty(allDealOrders)) {
            					for(Order order : allDealOrders) {
            						String bizSystem = order.getBizSystem();
            						List<Order> allOrders = allOrdersMap.get(bizSystem);
            						if(allOrders == null) {
            							allOrders = new ArrayList<Order>();
            						}
            						//添加所有订单集合Map
            						allOrders.add(order);
            						allOrdersMap.put(bizSystem, allOrders);
            					}
            				}
            				
            				for(Object obj : lotteryCodes) {
            					if(obj instanceof LotteryCode) {
            						LotteryCode lotteryCode = (LotteryCode)obj;
            						logger.info("读取到彩种[{}],期号[{}],开奖号码[{}]", lotteryCode.getLotteryName(), 
            								lotteryCode.getLotteryNum(), lotteryCode.getOpencodeStr());
            						
            						CommonOpenCodeService.prostate(lotteryCode);
            					} else if(obj instanceof LotteryCodeXy) {
            						LotteryCodeXy lotteryCodeXy = (LotteryCodeXy)obj;
            						String bizStr = lotteryCodeXy.getBizSystem();
            						List<Order> allOrders = allOrdersMap.get(bizStr);
            						
            						logger.info("读取到业务系统[{}],彩种[{}],期号[{}],开奖号码[{}]", lotteryCodeXy.getBizSystem(), 
            								lotteryCodeXy.getLotteryName(), lotteryCodeXy.getLotteryNum(), lotteryCodeXy.getOpencodeStr());
            						LotteryCode lotteryCode = new LotteryCode();
            						BeanUtilsExtends.copyProperties(lotteryCode, lotteryCodeXy);
            						prostate(lotteryCode, bizStr, allOrders, "暂无");
            					}
            				}
            			}
            		} catch (Exception e) {
            			logger.error("读取Objetc消息息异常...", e);
            		}
            	} else {
            		logger.error("读取消息类型错误");
            	}
            }
            
        } catch (JMSException e) {
        	logger.error("AutoXnXyThread接收消息发生异常", e);
        }
	}
	
	//Topic处理方式
	class LotteryCodeListener implements MessageListener{

		@Override
		public void onMessage(Message message) {
			if(message instanceof ObjectMessage){  
	            try {
					ObjectMessage objMsg = (ObjectMessage) message;  
					List<?> lotteryCodes = (List<?>)objMsg.getObject();;
					if(CollectionUtils.isNotEmpty(lotteryCodes)) {
						//取第一个开奖号码查询订单和期号
						Object firstObj = lotteryCodes.get(0);
						String expect = "00000000";
						String lotteryType = "XXX";
						if(firstObj instanceof LotteryCode) {
							LotteryCode lotteryCode = (LotteryCode)firstObj;
							expect = lotteryCode.getLotteryNum();
							lotteryType = lotteryCode.getLotteryName();
						} else if(firstObj instanceof LotteryCodeXy) {
							LotteryCodeXy lotteryCodeXy = (LotteryCodeXy)firstObj;
							expect = lotteryCodeXy.getLotteryNum();
							lotteryType = lotteryCodeXy.getLotteryName();
						}
						//查询当前期号的所有订单
						List<Order> allDealOrders = null;
						OrderQuery queryOrder = new OrderQuery();
						queryOrder.setExpect(expect); // 期号
						queryOrder.setLotteryType(lotteryType); // 彩种
						queryOrder.setOrderStatus(EOrderStatus.GOING.name()); // 进行中状态的订单
						queryOrder.setProstateStatus(EProstateStatus.DEALING.name()); // 处理中的中奖状态
						allDealOrders = getOrderService().getOrderByCondition(queryOrder);
						
						for(Object obj : lotteryCodes) {
							if(obj instanceof LotteryCode) {
								LotteryCode lotteryCode = (LotteryCode)obj;
								logger.info("读取到彩种[{}],期号[{}],开奖号码[{}]", lotteryCode.getLotteryName(), 
										lotteryCode.getLotteryNum(), lotteryCode.getOpencodeStr());
								CommonOpenCodeService.prostate(lotteryCode);
							} else if(obj instanceof LotteryCodeXy) {
								LotteryCodeXy lotteryCodeXy = (LotteryCodeXy)obj;
								logger.info("读取到业务系统[{}],彩种[{}],期号[{}],开奖号码[{}]", lotteryCodeXy.getBizSystem(), 
										lotteryCodeXy.getLotteryName(), lotteryCodeXy.getLotteryNum(), lotteryCodeXy.getOpencodeStr());
								LotteryCode lotteryCode = new LotteryCode();
								BeanUtilsExtends.copyProperties(lotteryCode, lotteryCodeXy);
								prostate(lotteryCode, lotteryCodeXy.getBizSystem(), allDealOrders, "暂无");
							}
						}
					}
				} catch (Exception e) {
					logger.error("读取Objetc消息息异常...", e);
				}
	        } 
		}
		
	}
	
	/**
	 * 中奖处理
	 * @param lotteryCode
	 * @throws Exception
	 */
	public boolean prostate(LotteryCode lotteryCode,String bizSystem,List<Order> dealOrders, String profitModelDes) throws Exception{
		lotteryCode.setLotteryType(lotteryCode.getLotteryName());
		boolean isTheLast = false; 
		LotteryCodeXy lotteryCodeXy = new LotteryCodeXy();
		BeanUtilsExtends.copyProperties(lotteryCodeXy, lotteryCode);
		lotteryCodeXy.setBizSystem(bizSystem);
		isTheLast = this.getLotteryCodeXyService().getLotteryCodeXyByKindAndExpectExist(lotteryCodeXy, lotteryCode.getLotteryType());
		//如果没有这样子的期号,此时做期号是否中奖的处理
        if(!isTheLast){
        	//拼接开奖号码
        	String openCodeStr = lotteryCode.getOpencodeStr(); //开奖号码拼接字符串
    		//查询到有订单起新线程处理中奖订单
    		if(CollectionUtils.isNotEmpty(dealOrders)) {
    			lotteryCodeXy.setProstateDeal(0);
    			lotteryCode.setProstateDeal(0);  
    			this.getLotteryCodeXyService().insertSelective(lotteryCodeXy);
        		logger.info("业务系统["+lotteryCodeXy.getBizSystem()+"]新增开奖处理,盈利模式[" + profitModelDes + "],彩种["+lotteryCode.getLotteryType()+"],期号["+lotteryCode.getLotteryNum()+"],开奖号码["+openCodeStr+"],订单数["+dealOrders.size()+"]");
    			
    			MainOpenDealThread mainOpenDealThread = new MainOpenDealThread(lotteryCode, dealOrders, openCodeStr);
	    		mainOpenDealThread.start();
    		} else {
    			
    			lotteryCodeXy.setProstateDeal(1);  //标识为已经进行了订单的中奖处理 
    			this.getLotteryCodeXyService().insertSelective(lotteryCodeXy);
        		logger.info("业务系统["+lotteryCodeXy.getBizSystem()+"]新增开奖处理,盈利模式[" + profitModelDes + "],彩种["+lotteryCode.getLotteryType()+"],期号["+lotteryCode.getLotteryNum()+"],开奖号码["+openCodeStr+"],订单数[0]");
    		}
        }
        return isTheLast;
	}
	
	private LotteryCodeXyService getLotteryCodeXyService() {
		if(lotteryCodeXyService == null) {
			lotteryCodeXyService = ApplicationContextUtil.getBean("lotteryCodeXyService");
		}
		return lotteryCodeXyService;
	}
	
	private OrderService getOrderService() {
		if(orderService == null) {
			orderService = ApplicationContextUtil.getBean("orderService");
		}
		return orderService;
	}


}
