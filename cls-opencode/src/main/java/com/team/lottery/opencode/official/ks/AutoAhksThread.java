package com.team.lottery.opencode.official.ks;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.enums.EOpenInterfaceType;
import com.team.lottery.exception.OpenCodeException;
import com.team.lottery.extvo.LotteryResultVo;
import com.team.lottery.opencode.BaseOpenCodeHandler;
import com.team.lottery.service.OpenCodeApiService;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.URLConnectUtil;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.OpenCodeApi;

/**
 * 安徽快三线程
 * @author chenhsh
 *
 */
@Component("autoAhksJob")
public class AutoAhksThread extends BaseOpenCodeHandler implements Runnable{

	private static Logger log = LoggerFactory.getLogger(AutoAhksThread.class);

	
	//正式接口访问地址
   public static Integer openCodeApiId;
	//正式接口访问地址
	public static String URL = "";
	//备用接口访问地址
	public static String BACK_URL = "";
	//当前调用接口
	public static String CURRENT_URL = "";
	//切换当前接口url
	public static String CHANGE_CURRENT_URL = "";
	//正式接口访问类型
	public static  EOpenInterfaceType TYPE;
	//备用接口访问类型
	public static EOpenInterfaceType BACKTYPE ;
	
	//当前调用接口类型
	public static  EOpenInterfaceType CURRENT_TYPE;
	//切换当前调用接口类型
	public static  EOpenInterfaceType CHANGE_CURRENT_TYPE;
	//记录接口调用日期
	public static Date DATE;
	
	//线程循环执行标志
	public static Boolean runFlag = true;
	//线程休眠时间，单位：秒
	public static int sleepSecond = 30;
	//正式接口调用失败次数
	private Integer request_fail_count = 0;
	//备用接口调用重试次数
	private Integer bak_request_count = 0;
	private ELotteryKind lotteryKind = ELotteryKind.AHKS;
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	private OpenCodeApiService openCodeApiService;
	//private OpenCodeApiService openCodeApiService = (OpenCodeApiService)ApplicationContextUtil.getBean(OpenCodeApiService.class);
	/**
	 * 线程方式进入入口
	 */
	public void run() {
		log.info("启动了" + lotteryKind.getDescription() + "开奖线程...");
		while (runFlag) {
			this.openDeal();
			try {
				//线程休眠
				Thread.sleep(sleepSecond * 1000);
			} catch (InterruptedException e) {
				log.error(lotteryKind.getDescription() + "开奖接口线程休眠出现异常.", e);
			}  
		}
		log.info("结束" + lotteryKind.getDescription() + "开奖线程，线程退出...");
	}
	
	/**
	 * 任务调度方式进入入口
	 */
	public void execute() {
		this.openDeal();
	}
	/**
	 * 变换正式备用接口
	 * @param currentUrl
	 * @param currentType
	 * @return
	 */
	public String[] returnUrlandType(String currentUrl,String currentType){
		String[] arr =new String[4];
       if(currentUrl.equals(BACK_URL)){
    	   arr[0] = currentUrl;
    	   arr[1] = currentType;
    	   arr[2] = URL;
    	   arr[3] = TYPE.getCode();
       }else{
    	   arr[0] = currentUrl;
    	   arr[1] = currentType;
    	   arr[2] = BACK_URL;
    	   arr[3] = BACKTYPE.getCode();
       }
		return arr;
	} 
	/**
	 * 开奖处理方法
	 * @return
	 */
	@Override
	protected void openDeal() {
		Date nowDate = new Date();
		String nowDateStr = sdf.format(nowDate);
		String dateStr = sdf.format(DATE);
		//对比两个时间，如果是昨天和今天，request_fail_count 清0
		if(!nowDateStr.equals(dateStr)){
			log.info("日期切换，重置彩种[{}]的日期[{}]错误失败调用次数[{}]为0", lotteryKind.getDescription(), dateStr, request_fail_count);
			request_fail_count = 0;
			DATE = nowDate;
		}
		
		//判断是否有切换接口，CHANGE_CURRENT_URL 有值表示有在切换接口的 针对没开奖
		if(CHANGE_CURRENT_URL != null && !"".equals(CHANGE_CURRENT_URL)){
			CURRENT_URL = CHANGE_CURRENT_URL;
			CURRENT_TYPE = CHANGE_CURRENT_TYPE;
			//还原初始值
			CHANGE_CURRENT_URL = "";
			CHANGE_CURRENT_TYPE = null;
		}
		
		String result = "";
		openCodeApiService = (OpenCodeApiService)ApplicationContextUtil.getBean("openCodeApiService");
		String[] arr = returnUrlandType(CURRENT_URL,CURRENT_TYPE.getCode());
		//请求接口的url
		String url = arr[0];
		String backUrl = arr[2];
		EOpenInterfaceType type = EOpenInterfaceType.valueOf(arr[1]);
		EOpenInterfaceType backType = EOpenInterfaceType.valueOf(arr[3]);
		List<LotteryCode> lotteryCodes = new ArrayList<LotteryCode>();
		
		try {
			//如果失败次数大于设定次数，则启用备用接口地址
//			if(request_fail_count.intValue() > ConstantUtil.REQUEST_FAIL_COUNT){
//				log.info(lotteryKind.getDescription() + "接口调用失败次数[{}]达到设定次数[{}]，切换为备用接口地址...", request_fail_count, ConstantUtil.REQUEST_FAIL_COUNT);
//				//切换接口
//				String tempUrl = url;
//				EOpenInterfaceType tempType = type;
//				url = backUrl;
//				type = backType;
//				backUrl = tempUrl;
//				backType = tempType;
//				request_fail_count = 0;

//			}
			//失败次数达到一定次数，进行接口调用失败告警
//			if(request_fail_count.intValue() == ConstantUtil.ALARM_REQUEST_FAIL_COUNT) {
//				log.error(lotteryKind.getDescription() + "接口调用失败次数[{}]达到设定告警次数[{}]，进行告警处理", request_fail_count, ConstantUtil.ALARM_REQUEST_FAIL_COUNT);
//			}


			List<LotteryResultVo> lotteryResultVos = null;
			//标志转换开奖接口是否成功
			boolean transSuccess = false;
			try {
				result = URLConnectUtil.getStringFromURL(url);
				lotteryResultVos = super.transferOpenCodeInterfaceData(result, type);
				transSuccess = true;
				log.debug(lotteryKind.getDescription() + "接口调用成功.");
				log.debug(lotteryKind.getDescription() + "使用接口:"+url+",接口类型："+type);
			} catch (OpenCodeException e) {
				log.error(lotteryKind.getDescription() + "转换开奖接口发生异常，错误调用次数[{}]", request_fail_count, e);
				//正式接口失败累加失败次数
				request_fail_count++;  
			} catch (IOException e) {
				if(e instanceof SocketTimeoutException){
					log.error(lotteryKind.getDescription() + "接口请求超时");
				}else if(e instanceof UnknownHostException){
					log.error(lotteryKind.getDescription() + "接口请求未找到该接口主机.");
				}
				log.error(lotteryKind.getDescription() + "接口请求出现IO异常，错误调用次数[{}]", request_fail_count, e);
				request_fail_count++;  //累加失败次数
			} catch (Exception e) {
				log.error(lotteryKind.getDescription() + "开奖接口线程请求出现异常，错误调用次数[{}]", request_fail_count, e);
				request_fail_count++;  //累加失败次数
			} 
			
			//转换失败直接上去备用接口取数据
			if(!transSuccess) {
				log.info(lotteryKind.getDescription() + "使用备用接口获取开奖数据");
				try{
				
					result = URLConnectUtil.getStringFromURL(backUrl);
					lotteryResultVos = this.transferOpenCodeInterfaceData(result, backType);
					log.debug(lotteryKind.getDescription() + "备用接口调用成功.");
					log.debug(lotteryKind.getDescription() + "使用备用接口:"+backUrl+",接口类型："+backType);
				} catch(OpenCodeException e) {
					log.error(lotteryKind.getDescription() + "转换开奖备用接口发生异常，错误调用次数[{}]", bak_request_count, e);
					bak_request_count++;
				} catch (IOException e) {
					if(e instanceof SocketTimeoutException){
						log.error(lotteryKind.getDescription() + "接口请求超时");
					}else if(e instanceof UnknownHostException){
						log.error(lotteryKind.getDescription() + "接口请求未找到该接口主机.");
					}
					log.error(lotteryKind.getDescription() + "备用接口请求出现IO异常，错误调用次数[{}]", bak_request_count, e);
					bak_request_count++;
				} catch (Exception e) {
					log.error(lotteryKind.getDescription() + "开奖接口线程请求出现异常，错误调用次数[{}]", bak_request_count, e);
					bak_request_count++;
				} 
			}
			
			//更新数据库，记录当前接口
			OpenCodeApi openCodeApi = new OpenCodeApi();
			openCodeApi.setId(openCodeApiId);
			openCodeApi.setCurrentApiUrl(CURRENT_URL);
			openCodeApi.setCurrentApiType(CURRENT_TYPE.getCode());
			openCodeApi.setUpdateTime(new Date());
			openCodeApiService.updateByPrimaryKeySelective(openCodeApi);
			
			//判断是否有切换接口，CHANGE_CURRENT_URL 有值表示有在切换接口的 针对半中间正在开奖的
			if(CHANGE_CURRENT_URL != null && !"".equals(CHANGE_CURRENT_URL)){
				CURRENT_URL = CHANGE_CURRENT_URL;
				CURRENT_TYPE = CHANGE_CURRENT_TYPE;
				//还原初始值
				CHANGE_CURRENT_URL = "";
				CHANGE_CURRENT_TYPE = null;
			}
			
			//转换开奖结果对象为开奖号码对象
			int currentCount = 0;
			if(CollectionUtils.isNotEmpty(lotteryResultVos)){
				for(LotteryResultVo lotteryResultVo : lotteryResultVos){
					try {
						LotteryCode lotteryCode = null;
						if(transSuccess){
							lotteryCode = this.getLotteryCodeFromResultVo(lotteryResultVo, type);
						}else{
							lotteryCode = this.getLotteryCodeFromResultVo(lotteryResultVo, backType);
						}
						lotteryCodes.add(lotteryCode);
					} catch (OpenCodeException e1) {
						log.error(e1.getMessage(), e1);
					} catch (Exception e) {
						log.error(lotteryKind.getDescription() +"处理开奖期号出现异常", e);
					}
					
					currentCount++;
					//如果该期号是已经存在的开奖期号,则退出遍历,开奖期数达到10期
					if(currentCount == ConstantUtil.opcodeCount){   //if(isTheLast)
					   break;	
					}
				}
			} else {
				log.error(lotteryKind.getDescription() +"读取开奖结果为空");
			}
		
			
		} catch (Exception e) {
			log.error(lotteryKind.getDescription() + "开奖接口线程请求出现异常.", e);
			request_fail_count++;  //累加失败次数
		}
		
		//处理开奖接口数据
		this.dealLotteryResults(lotteryCodes);
	}
	
	/**
	 * 根据接口返回对象和接口类型获取开奖数据对象
	 * @param lotteryResultVo
	 * @param type
	 * @return
	 * @throws OpenCodeException 
	 */
	public LotteryCode getLotteryCodeFromResultVo(LotteryResultVo lotteryResultVo, EOpenInterfaceType type) throws OpenCodeException {
		LotteryCode lotteryCode = new LotteryCode();
		String openCode = lotteryResultVo.getOpencode();
		String[] nums = openCode.split(ConstantUtil.OPENCODE_PLIT);

		//号码验证
		if(openCode == null || nums.length != lotteryKind.getCodeCount()){
			throw new OpenCodeException(lotteryKind.getDescription() + "接口数据返回错误，接口出现变动");
		}
		
		lotteryCode.setLotteryNum(lotteryResultVo.getExpect());
		lotteryCode.setKjtime(lotteryResultVo.getOpenDateTime());
		lotteryCode.setAddtime(new Date());
		lotteryCode.setLotteryName(lotteryKind.name());
		lotteryCode.setNumInfo1(nums[0]);
		lotteryCode.setNumInfo2(nums[1]);
		lotteryCode.setNumInfo3(nums[2]);
		//默认设置未还未进行中奖的处理
		lotteryCode.setProstateDeal(0);  
		return lotteryCode;
	}

}
