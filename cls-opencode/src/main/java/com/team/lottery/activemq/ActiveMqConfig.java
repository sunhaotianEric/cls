package com.team.lottery.activemq;

import org.apache.activemq.ActiveMQConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//@Component
/**
 * @deprecated  弃用，直接spring配置
 * @author luocheng
 *
 */
public class ActiveMqConfig {
	
	private static Logger logger = LoggerFactory.getLogger(ActiveMqConfig.class);
	
	//@Value("${activemq.username}")
	private String username;
	
	//@Value("${activemq.password}")
	private String password;
	
	//@Value("${activemq.broken_url}")
	private String broken_url;
	
	//ActiveMq 的默认用户名
    public static String USERNAME = ActiveMQConnection.DEFAULT_USER;
    //ActiveMq 的默认登录密码
    public static String PASSWORD = ActiveMQConnection.DEFAULT_PASSWORD;
    //ActiveMQ 的链接地址
    public static String BROKEN_URL = ActiveMQConnection.DEFAULT_BROKER_URL;
    
    /**
     * redis配置数据初始化
     */
    public void init() {
//    	logger.info("初始化activemq配置数据...");
//    	USERNAME = username;
//    	PASSWORD = password;
//    	BROKEN_URL = broken_url;
    }
}
