package com.team.lottery.activemq;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.team.lottery.enums.ECodePlanProstateStatus;
import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.enums.EOrderStatus;
import com.team.lottery.enums.EProstateStatus;
import com.team.lottery.enums.ESYXWKindCodePlan;
import com.team.lottery.extvo.OrderQuery;
import com.team.lottery.extvo.SubKindCodePlanVO;
import com.team.lottery.service.CodePlanService;
import com.team.lottery.service.CommonOpenCodeService;
import com.team.lottery.service.LotteryCodeXyService;
import com.team.lottery.service.LotteryIssueService;
import com.team.lottery.service.OrderService;
import com.team.lottery.service.lotterykindplay.LotterykindPlayCodeplanService;
import com.team.lottery.system.MainOpenDealThread;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.util.BeanUtilsExtends;
import com.team.lottery.util.EumnUtil;
import com.team.lottery.vo.CodePlan;
import com.team.lottery.vo.LotteryCode;
import com.team.lottery.vo.LotteryCodeXy;
import com.team.lottery.vo.LotteryIssue;
import com.team.lottery.vo.Order;

@Component
public class LotteryCodeQueueReceiver implements MessageListener{
	
	//处理开奖的线程池
	public static ThreadPoolExecutor executor = (ThreadPoolExecutor)Executors.newFixedThreadPool(5); 
	
	private static Logger logger = LoggerFactory.getLogger(LotteryCodeQueueReceiver.class);
	
	@Autowired
	private LotteryCodeXyService lotteryCodeXyService;
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
    private CodePlanService codePlanService;
	
	@Autowired
	private LotteryIssueService lotteryIssueService;

	@Override
	public void onMessage(final Message message) {
		final OrderService orderService = this.orderService;
		//接收到消息之后起新线程处理
		executor.execute(new Runnable() {
			
			@Override
			public void run() {
				if(message instanceof ObjectMessage){  
		    		try {
		    			ObjectMessage objMsg = (ObjectMessage) message;  
		    			List<?> lotteryCodes = (List<?>)objMsg.getObject();;
		    			if(CollectionUtils.isNotEmpty(lotteryCodes)) {
		    				//取第一个开奖号码查询订单和期号
		    				Object firstObj = lotteryCodes.get(0);
		    				String expect = "00000000";
		    				String lotteryType = "XXX";
		    				if(firstObj instanceof LotteryCode) {
		    					LotteryCode lotteryCode = (LotteryCode)firstObj;
		    					expect = lotteryCode.getLotteryNum();
		    					lotteryType = lotteryCode.getLotteryName();
		    				} else if(firstObj instanceof LotteryCodeXy) {
		    					LotteryCodeXy lotteryCodeXy = (LotteryCodeXy)firstObj;
		    					expect = lotteryCodeXy.getLotteryNum();
		    					lotteryType = lotteryCodeXy.getLotteryName();
		    				}
		    				
		    				//查询当前期号的所有订单
		    				List<Order> allDealOrders = null;
		    				OrderQuery queryOrder = new OrderQuery();
		    				queryOrder.setExpect(expect); // 期号
		    				queryOrder.setLotteryType(lotteryType); // 彩种
		    				queryOrder.setOrderStatus(EOrderStatus.GOING.name()); // 进行中状态的订单
		    				queryOrder.setProstateStatus(EProstateStatus.DEALING.name()); // 处理中的中奖状态
		    				allDealOrders = orderService.getOrderByCondition(queryOrder);
		    				
		    				//业务系统和对应所有订单集合
		    				Map<String, List<Order>> allOrdersMap = new HashMap<String, List<Order>>();
		    				if(CollectionUtils.isNotEmpty(allDealOrders)) {
		    					for(Order order : allDealOrders) {
		    						String bizSystem = order.getBizSystem();
		    						List<Order> allOrders = allOrdersMap.get(bizSystem);
		    						if(allOrders == null) {
		    							allOrders = new ArrayList<Order>();
		    						}
		    						//添加所有订单集合Map
		    						allOrders.add(order);
		    						allOrdersMap.put(bizSystem, allOrders);
		    					}
		    				}
		    				for(Object obj : lotteryCodes) {
		    					if(obj instanceof LotteryCode) {
		    						LotteryCode lotteryCode = (LotteryCode)obj;
		    						logger.info("读取到彩种[{}],期号[{}],开奖号码[{}]", lotteryCode.getLotteryName(), 
		    								lotteryCode.getLotteryNum(), lotteryCode.getOpencodeStr());
		    						
		    						CommonOpenCodeService.prostate(lotteryCode);
		    					} else if(obj instanceof LotteryCodeXy) {
		    						LotteryCodeXy lotteryCodeXy = (LotteryCodeXy)obj;
		    						String bizStr = lotteryCodeXy.getBizSystem();
		    						List<Order> allOrders = allOrdersMap.get(bizStr);
		    						
		    						logger.info("读取到业务系统[{}],彩种[{}],期号[{}],开奖号码[{}]", lotteryCodeXy.getBizSystem(), 
		    								lotteryCodeXy.getLotteryName(), lotteryCodeXy.getLotteryNum(), lotteryCodeXy.getOpencodeStr());
		    						LotteryCode lotteryCode = new LotteryCode();
		    						BeanUtilsExtends.copyProperties(lotteryCode, lotteryCodeXy);
		    						prostate(lotteryCode, bizStr, allOrders, "暂无");
		    					}
		    				}
		    			}
		    		} catch (Exception e) {
		    			logger.error("读取Objetc消息息异常...", e);
		    		}
		    	} else {
		    		logger.error("读取消息类型错误");
		    	}
				
			}
		});
		
	}
	
	/**
	 * 中奖处理
	 * @param lotteryCode
	 * @throws Exception
	 */
	public boolean prostate(LotteryCode lotteryCode,String bizSystem,List<Order> dealOrders, String profitModelDes) throws Exception{
		lotteryCode.setLotteryType(lotteryCode.getLotteryName());
		boolean isTheLast = false; 
		LotteryCodeXy lotteryCodeXy = new LotteryCodeXy();
		BeanUtilsExtends.copyProperties(lotteryCodeXy, lotteryCode);
		lotteryCodeXy.setBizSystem(bizSystem);
		isTheLast = this.lotteryCodeXyService.getLotteryCodeXyByKindAndExpectExist(lotteryCodeXy, lotteryCode.getLotteryType());
		//如果没有这样子的期号,此时做期号是否中奖的处理
        if(!isTheLast){
        	//拼接开奖号码
        	String openCodeStr = lotteryCode.getOpencodeStr(); //开奖号码拼接字符串
    		//查询到有订单起新线程处理中奖订单
    		if(CollectionUtils.isNotEmpty(dealOrders)) {
    			lotteryCodeXy.setProstateDeal(0);
    			lotteryCode.setProstateDeal(0);  
    			this.lotteryCodeXyService.insertSelective(lotteryCodeXy);
        		logger.info("业务系统["+lotteryCodeXy.getBizSystem()+"]新增开奖处理,盈利模式[" + profitModelDes + "],彩种["+lotteryCode.getLotteryType()+"],期号["+lotteryCode.getLotteryNum()+"],开奖号码["+openCodeStr+"],订单数["+dealOrders.size()+"]");
    			
    			MainOpenDealThread mainOpenDealThread = new MainOpenDealThread(lotteryCode, dealOrders, openCodeStr);
	    		mainOpenDealThread.start();
    		} else {
    			
    			lotteryCodeXy.setProstateDeal(1);  //标识为已经进行了订单的中奖处理 
    			this.lotteryCodeXyService.insertSelective(lotteryCodeXy);
        		logger.info("业务系统["+lotteryCodeXy.getBizSystem()+"]新增开奖处理,盈利模式[" + profitModelDes + "],彩种["+lotteryCode.getLotteryType()+"],期号["+lotteryCode.getLotteryNum()+"],开奖号码["+openCodeStr+"],订单数[0]");
    		}
    		CommonOpenCodeService.dealCodeXyTrend(lotteryCode.getLotteryName(), openCodeStr, bizSystem, lotteryCode.getLotteryNum());
        }
        return isTheLast;
	}
	
	public void dealCodePlan(String expect,String lotteryType,Object obj){
		String openCode = "";
		if(obj instanceof LotteryCode) {
			LotteryCode lotteryCode = (LotteryCode)obj;
			openCode = lotteryCode.getOpencodeStr();
		} else if(obj instanceof LotteryCodeXy) {
			LotteryCodeXy lotteryCodeXy = (LotteryCodeXy)obj;
			openCode = lotteryCodeXy.getOpencodeStr();
		}
		ELotteryKind kind = getByLotteryType(lotteryType);
		if (kind==null) {
			throw new RuntimeException("计划列表功能彩种 "+lotteryType+" 不存在");
		}else {
			/**  计划列表功能注释**/
			try {
				//根据期号查找订单
				List<CodePlan> cps = codePlanService.getAllByexpect(expect,kind.getCode());//expect开奖的一期
				if (cps!=null && cps.size()>0) {
					//处理特殊开奖字符串，特殊彩种如十一选五、PK10、六合彩，开奖号码是两位的，要在前面补0
					String openC ="";
					if (kind.getCode().contains("SYXW") || kind.getCode().contains("PK10") || kind.getCode().contains("LHC")) {
						//openCode去掉开奖号前面的0
						openC = getOpenCode(openCode);
					}else {
						openC = openCode;
					}
					logger.info("计划列表更新期号["+expect+"]"+",彩种["+kind.getCode()+"] ["+kind.getDescription()+"] 已开始...");
					//开奖后更新计划列表
					for (CodePlan cp : cps) {
						cp.setOpenCode(openCode);
						//根据玩法计算是否中奖
						boolean prostate = false;
						LotterykindPlayCodeplanService lotterykindPlayCodeplanService = (LotterykindPlayCodeplanService)ApplicationContextUtil.getBean(
								EumnUtil.getServiceClassName(EumnUtil.getClassNameByLotteryTypeCodePlan(kind.getCode()), cp.getPlaykind()));
						prostate = lotterykindPlayCodeplanService.getProstate(cp, openC);
						if (prostate) {//中奖后设置中奖期数
							cp.setExpect(expect);
							cp.setExpectShort(expect.substring(expect.length()-3,expect.length()));
							cp.setEnabled(1);
							cp.setProstate(prostate?ECodePlanProstateStatus.WINNING.getCode():ECodePlanProstateStatus.NOT_WINNING.getCode());
							cp.setProstateDesc(prostate?ECodePlanProstateStatus.WINNING.getDescription():ECodePlanProstateStatus.NOT_WINNING.getDescription());
						}else {
							String expectsPlan = cp.getExpectsPlan();
							//如果expect是expectsPlan的最后一期，该expectsPlan3期均未中奖，设置expect值
							if (expectsPlan.endsWith(expect)) {
								cp.setExpect(expect);
								cp.setEnabled(1);
								cp.setExpectShort(expect.substring(expect.length()-3,expect.length()));
								cp.setProstate(prostate?ECodePlanProstateStatus.WINNING.getCode():ECodePlanProstateStatus.NOT_WINNING.getCode());
								cp.setProstateDesc(prostate?ECodePlanProstateStatus.WINNING.getDescription():ECodePlanProstateStatus.NOT_WINNING.getDescription());
							}
						}
						cp.setUpdateDate(new Date());
					}
				}
				//批量更新该期所有玩法的计划数据
				if (cps!=null && cps.size()>0){
					codePlanService.updateBatch(cps);
					logger.info("计划列表更新期号["+expect+"]"+",彩种["+kind.getCode()+"] ["+kind.getDescription()+"] 已结束...");
				}
				//未来3期期号,十一选五：二中二 玩法选择未来4期
				List<LotteryIssue> issues = lotteryIssueService.get3NewExpectAndDiffTimeByLotteryKind(kind);
				
				//利用cps存入新数据
				if (cps!=null && cps.size()>0) {
					logger.info("计划列表存储数据:期号["+expect+"]"+",彩种["+kind.getCode()+"] ["+kind.getDescription()+"] 已开始...");
					//开奖后更新计划列表
					List<CodePlan> codesInsert = new ArrayList<CodePlan>();
					for (CodePlan cp : cps) {
						if (cp.getEnabled().equals(0)) {//未处理的数据不需要重新存入
							continue;
						}
						cp.setEnabled(0);
						cp.setOpenCode(openCode);
						cp.setProstate(ECodePlanProstateStatus.DEALING.getCode());
						LotterykindPlayCodeplanService lotterykindPlayCodeplanService = (LotterykindPlayCodeplanService)ApplicationContextUtil.getBean(
								EumnUtil.getServiceClassName(EumnUtil.getClassNameByLotteryTypeCodePlan(kind.getCode()), cp.getPlaykind()));
						cp.setCodes(lotterykindPlayCodeplanService.generateCodes());
						cp.setCodesDesc(lotterykindPlayCodeplanService.showCode(cp));
						cp.setProstate(ECodePlanProstateStatus.DEALING.getCode());
						cp.setProstateDesc(ECodePlanProstateStatus.DEALING.getDescription());
						cp.setOpenCode("");
						cp.setCreatedDate(new Date());
						cp.setUpdateDate(new Date());
						cp.setExpect("");
						cp.setExpectShort("");
						cp.setExpectsPlan(issues.get(0).getLotteryNum()+"|"+issues.get(1).getLotteryNum()+"|"+issues.get(2).getLotteryNum());
						cp.setExpectsPeriod(issues.get(0).getLotteryNum().substring(issues.get(0).getLotteryNum().length()-3, issues.get(0).getLotteryNum().length())+"-"+
								issues.get(2).getLotteryNum().substring(issues.get(2).getLotteryNum().length()-3, issues.get(2).getLotteryNum().length())+"期");
						if (cp.getPlaykind().equals(ESYXWKindCodePlan.EZE.getCode())) {
							cp.setExpectsPlan(issues.get(0).getLotteryNum()+"|"+issues.get(1).getLotteryNum()+"|"+issues.get(2).getLotteryNum());
							cp.setExpectsPeriod(issues.get(0).getLotteryNum().substring(issues.get(0).getLotteryNum().length()-3, issues.get(0).getLotteryNum().length())+"-"+
									issues.get(2).getLotteryNum().substring(issues.get(2).getLotteryNum().length()-3, issues.get(2).getLotteryNum().length())+"期");
						}
						codesInsert.add(cp);
					}
					if (codesInsert.size()>0) {
						int r = codePlanService.insertBatch(codesInsert);
						if (r>0) {
							logger.info("计划列表存储数据:期号["+expect+"]"+",彩种["+kind.getCode()+"] ["+kind.getDescription()+"] 已结束...");
						}
					}
				}else {
					//第一次（或者查询到计划列表数据为空）存入计划列表数据的处理方式
					//加载彩种所有玩法
					List<SubKindCodePlanVO> subKindVOs = EumnUtil.getSubKindsCodePlan(EumnUtil.getClassNameByLotteryTypeCodePlan(kind.getCode()));
					//插入最新计划数据
					String expectsPlan = "";
					String expectsPeriod = "";
					if (issues!=null && (issues.size()==3 || issues.size()==4)) {
						expectsPlan = issues.get(0).getLotteryNum()+"|"+issues.get(1).getLotteryNum()+"|"+issues.get(2).getLotteryNum();
						expectsPeriod = issues.get(0).getLotteryNum().substring(issues.get(0).getLotteryNum().length()-3, issues.get(0).getLotteryNum().length())+"-"+
								issues.get(2).getLotteryNum().substring(issues.get(2).getLotteryNum().length()-3, issues.get(2).getLotteryNum().length())+"期";
					}
					List<CodePlan> codePlans = new ArrayList<CodePlan>();
					for (SubKindCodePlanVO s : subKindVOs) {
						CodePlan codePlan = new CodePlan();
						codePlan.setEnabled(0);
						if(obj instanceof LotteryCode) {
							LotteryCode lotteryCode = (LotteryCode)obj;
							codePlan.setLotteryType(lotteryCode.getLotteryType());
						} else if(obj instanceof LotteryCodeXy) {
							LotteryCodeXy lotteryCodeXy = (LotteryCodeXy)obj;
							codePlan.setLotteryType(lotteryCodeXy.getLotteryType());
						}
						
						codePlan.setBizSystem("");
						LotterykindPlayCodeplanService lotterykindPlayCodeplanService = (LotterykindPlayCodeplanService)ApplicationContextUtil.getBean(
								EumnUtil.getServiceClassName(EumnUtil.getClassNameByLotteryTypeCodePlan(kind.getCode()), s.getCode()));
						codePlan.setCodes(lotterykindPlayCodeplanService.generateCodes());
						codePlan.setCodesDesc(lotterykindPlayCodeplanService.showCode(codePlan));
						codePlan.setProstate(ECodePlanProstateStatus.DEALING.getCode());
						codePlan.setProstateDesc(ECodePlanProstateStatus.DEALING.getDescription());
						codePlan.setOpenCode("");
						codePlan.setCreatedDate(new Date());
						codePlan.setUpdateDate(new Date());
						codePlan.setExpect("");
						codePlan.setExpectNumber(3);
						codePlan.setExpectShort("");
						if (s.getCode().equals(ESYXWKindCodePlan.EZE.getCode())) {
							codePlan.setExpectNumber(4);
							expectsPlan = issues.get(0).getLotteryNum()+"|"+issues.get(1).getLotteryNum()+"|"+issues.get(2).getLotteryNum() + "|" + issues.get(3).getLotteryNum();
							expectsPeriod = issues.get(0).getLotteryNum().substring(issues.get(0).getLotteryNum().length()-3, issues.get(0).getLotteryNum().length())+"-"+
									issues.get(3).getLotteryNum().substring(issues.get(3).getLotteryNum().length()-3, issues.get(3).getLotteryNum().length())+"期";
						}
						codePlan.setExpectsPlan(expectsPlan);
						codePlan.setExpectsPeriod(expectsPeriod);
						codePlan.setCodesNum(lotterykindPlayCodeplanService.getCodeNum());
						codePlan.setPlaykind(s.getCode());
						codePlan.setPlaykindDes(s.getDescription());
						codePlans.add(codePlan);
					}
					if (codePlans!=null && codePlans.size()>0) {
						int r = codePlanService.insertBatch(codePlans);
						if (r>0) {
							logger.info("计划列表新增期号["+expect+"]"+",彩种["+kind.getCode()+"] ["+kind.getDescription()+"]");
						}
					}
				}
				logger.info("手动开奖处理计划列表结束..");
			} catch (Exception e) {
				logger.error("手动开奖处理计划列表:"+e.getMessage());
			}
		}
	}
	
	/**
	 * 通过code获取枚举类
	 * @param lotteryType
	 * @return
	 */
	public ELotteryKind getByLotteryType(String lotteryType){
		for (ELotteryKind e : ELotteryKind.values()) {
			if (e.getCode().equals(lotteryType)) {
				return e;
			}
		}
		return null;
	}
	
	/**
	 * 如果开奖号码是特殊彩种如十一选五、PK10、六合彩，开奖号码是两位的，要在前面补0的处理
	 * @param openCode
	 * @return
	 */
	private String getOpenCode(String openCode){
		String openCodeStr = "";
		String[] ocs = openCode.split(",");
		for (int i = 0; i < ocs.length; i++) {
			if (i<ocs.length-1) {
				if (ocs[i].startsWith("0")) {
					openCodeStr = openCodeStr + ocs[i].substring(1) + ",";
				} else {
					openCodeStr = openCodeStr + ocs[i] + ",";
				}
			}else {
				if (ocs[i].startsWith("0")) {
					openCodeStr = openCodeStr + ocs[i].substring(1);
				} else {
					openCodeStr = openCodeStr + ocs[i];
				}
			}
		}
		return openCodeStr;
	}
}
