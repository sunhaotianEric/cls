package com.team.lottery.action.web.common;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.enums.EFrontCacheType;
import com.team.lottery.system.SystemCache;

/**
 * Servlet implementation class RefreshCacheServlet
 */
public class RefreshCacheServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private static Logger logger = LoggerFactory.getLogger(RefreshCacheServlet.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RefreshCacheServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String type = request.getParameter("type");
		
		logger.info("收到刷新缓存通知，刷新缓存类型["+type+"]");
		String result = "fail";
		if(StringUtils.isNotBlank(type)) {
			result = "success";
			if(EFrontCacheType.SYS_CONFIG_DATA.getCode().equals(type)) {
				SystemCache.loadSystemConfigCacheData();
			} else if(EFrontCacheType.LOTTERY_CONFIG_DATA.getCode().equals(type)) {
				SystemCache.loadLotteryConfigCacheData();
			} else if(EFrontCacheType.BIZ_SYS_CONFIG_DATA.getCode().equals(type)) {
				SystemCache.loadBizSystemConfigCacheData();
			} else {
				result = "fail";
			}
			EFrontCacheType frontCacheType = EFrontCacheType.valueOf(type);
			String desc = "";
			if(frontCacheType != null) {
				desc = frontCacheType.getDescription();
			}
			logger.info("刷新缓存完毕，刷新的缓存类型["+ desc +"]");
		}else{
			result = "success";
			SystemCache.loadAllCacheData();
			logger.info("全量前端应用刷新缓存成功...");
		}
		
		response.setContentType("text");
		response.getWriter().write(result);
	}

}
