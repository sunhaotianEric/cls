package com.boss.img.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.boss.img.util.FileNameUtils;
import com.boss.img.util.FileUtils;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

@Controller
public class FileUploadController {

	private static  Logger logger = LoggerFactory.getLogger(FileUploadController.class);

	//文件保存路径
	@Value("${filedir}")
	private String fileDir;

	//跳转到上传文件的页面
	@RequestMapping(value="/uploadimg.html", method = RequestMethod.GET)
	public String goUploadImg() {
		//跳转到 templates 目录下的 uploadimg.html
		return "uploadimg";
	}

	/**
	 *
	 * @param file 要上传的文件
	 * @return
	 */
	@RequestMapping("fileUpload")
	@ResponseBody
	public Map<String, Object> upload(@RequestParam("file") MultipartFile file, @RequestParam("path")String path, @RequestParam("fileName")String newFileName){

		Map<String, Object> map = new HashMap<>();
		String result = "";
		String msg = "";

		if(file == null || file.isEmpty() || StringUtils.isEmpty(path) || StringUtils.isEmpty(newFileName)) {
			map.put("result", "error");
			map.put("msg", "参数传递错误");
			return map;
		}

		String fileName = file.getOriginalFilename();
		String fileType = FileNameUtils.getSuffix(fileName);
		double kbFileSize = file.getSize()/1024;
		logger.info("接收图片上传处理，上传图片类型[{}],原文件名称[{}],文件大小[{}]kb,存放路径[{}]", fileType, fileName, kbFileSize, path);

		//判断接受上传图片类型
		boolean isAccept =  false;
		String[] allowFileTypes = new String[]{".png", ".jpg", ".ico",".svga"};
		for(String allowFileType : allowFileTypes) {
			if(allowFileType.equals(fileType)) {
				isAccept = true;
				break;
			}
		}
		if(!isAccept) {
			map.put("result", "error");
			map.put("msg", "不接受文件类型为" + fileType + "的文件");
			return map;
		}

		//判断文件大小
		double mbFileSize = kbFileSize / 1024;
		int allowMaxFileSize = 2;
		if(mbFileSize > allowMaxFileSize) {
			map.put("result", "error");
			map.put("msg", "文件大小超过2M");
			return map;
		}

		String savePath = fileDir + "/" + path;
		String fullFilePath = savePath + "/" + newFileName;
		if (FileUtils.upload(file, savePath, newFileName)){
			// 上传成功，给出页面提示
			result = "ok";
			msg = "上传成功！";
		}else {
			result = "error";
			msg = "上传失败！";
		}


		// 显示图片
		map.put("result", result);
		map.put("msg", msg);
		map.put("fileName", fullFilePath);

		return map;
	}

	@RequestMapping("/imgshow")
	public void imgshow(HttpServletResponse response, @RequestParam("src") String src) {

		File file = new File(fileDir,src);
		writefile(response, file);
	}

	public void writefile(HttpServletResponse response, File file) {
		ServletOutputStream sos = null;
		FileInputStream aa = null;
		try {
			aa = new FileInputStream(file);
			sos = response.getOutputStream();
			// 读取文件问字节码
			byte[] data = new byte[(int) file.length()];
			IOUtils.readFully(aa, data);
			// 将文件流输出到浏览器
			IOUtils.write(data, sos);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				sos.close();
				aa.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}


	}
}
