package com.boss.img.controller;

import java.util.Date;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class FreemakerController {
	
	@RequestMapping(value="/hello.html")
    //注意这儿没有@responseBody
    public String welcome(ModelMap map){
        Date date = new Date();
        //传到template模板中（会把模板中能找到的所有的ftl文件中的名称都替换掉）
        map.addAttribute("vv", date.toString());
        //返回页面hello.ftl
        return "hello";
    }

}
