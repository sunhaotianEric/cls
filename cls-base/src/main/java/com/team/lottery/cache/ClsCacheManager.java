package com.team.lottery.cache;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.ehcache.EhCacheCacheManager;

import com.team.lottery.service.AwardModelConfigService;
import com.team.lottery.service.BizSystemConfigService;
import com.team.lottery.service.BizSystemDomainService;
import com.team.lottery.service.BizSystemInfoService;
import com.team.lottery.service.BizSystemService;
import com.team.lottery.service.EmailSendConfigService;
import com.team.lottery.service.LotterySwitchService;
import com.team.lottery.service.SmsSendConfigService;
import com.team.lottery.service.SystemConfigService;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.vo.AdminDomain;
import com.team.lottery.vo.AwardModelConfig;
import com.team.lottery.vo.BizSystem;
import com.team.lottery.vo.BizSystemDomain;
import com.team.lottery.vo.BizSystemInfo;
import com.team.lottery.vo.EmailSendConfig;
import com.team.lottery.vo.LotterySwitch;
import com.team.lottery.vo.SmsSendConfig;

import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;


public class ClsCacheManager{

	private static Logger logger = LoggerFactory.getLogger(ClsCacheManager.class);
	//业务系统缓存name
	public static final String BIZ_SYSTEM_CACHE = "bizSystem";
	//域名管理缓存name
	public static final String BIZ_SYSTEM_DOMAIN_CACHE = "bizSystemDomain";
	//系统信息管理缓存name
	public static final String BIZ_SYSTEM_INFO_CACHE = "bizSystemInfo";
	//业务系统奖金模式配置缓存name
	public static final String AWARD_MODEL_CONFIG_CACHE = "awardModelConfig";
	//邮件发送配置缓存name
	public static final String EMAIL_SEND_CONFIG_CACHE = "emailSendConfigCache";
	//短信发送配置缓存name
	public static final String SMS_SEND_CONFIG_CACHE = "smsSendConfigCache";
	//后台域名管理缓存name
	public static final String MGR_BIZ_SYSTEM_DOMAIN_CACHE = "mgrBizSystemDomainCache";
	//彩种开关全局配置name
	public static final String LOTTERY_SWITCH_CACHE = "lotterySwitchCache";
	
	private static EhCacheCacheManager cacheManager;
	
	static {
		cacheManager = ApplicationContextUtil.getBean(EhCacheCacheManager.class);
	}
	
	private static BizSystemService bizSystemService = ApplicationContextUtil.getBean(BizSystemService.class);
	private static BizSystemDomainService bizSystemDomainService = ApplicationContextUtil.getBean(BizSystemDomainService.class);
	private static BizSystemInfoService bizSystemInfoService = ApplicationContextUtil.getBean(BizSystemInfoService.class);
	private static EmailSendConfigService emailSendConfigService = ApplicationContextUtil.getBean(EmailSendConfigService.class);
	private static SmsSendConfigService smsSendConfigService = ApplicationContextUtil.getBean(SmsSendConfigService.class);
	private static SystemConfigService systemConfigService = ApplicationContextUtil.getBean(SystemConfigService.class);
	private static BizSystemConfigService bizSystemConfigService = ApplicationContextUtil.getBean(BizSystemConfigService.class);
	private static LotterySwitchService lotterySwitchService = ApplicationContextUtil.getBean(LotterySwitchService.class);
	
	
	/**
	 * 初始化缓存
	 */
	public static void initCache() {
		//初始化业务系统缓存
		initBizSystemCache();
		//初始化域名管理缓存
		initBizSystemDomainCache();
		//初始化信息管理缓存
		initBizSystemInfoCache();
		//初始化业务系统奖金模式配置缓存
		initAwardModelConfigCache();
		//初始化邮件配置缓存
		initEmailSendConfigCache();
		//初始化短信配置缓存
		initSmsSendConfigCache();
		// 初始化彩种开关配置.
		initLotterySwitchCache();
		
	}

	/**
	 * 初始化业务系统缓存
	 */
	public static void initBizSystemCache() {
		
		List<BizSystem> bizSystems = bizSystemService.getAllBizSystem();
		Cache cache = cacheManager.getCacheManager().getCache(BIZ_SYSTEM_CACHE);
		if(cache != null) {
			cache.removeAll();
			if(CollectionUtils.isNotEmpty(bizSystems)) {
				for(BizSystem bizSystem : bizSystems) {
					Element element = new Element(bizSystem.getId(), bizSystem);
					cache.put(element);
				}
			}
		}
		logger.info("初始化业务系统缓存结束...");
	}
	
	/**
	 * 初始化域名管理缓存
	 */
	public static void initBizSystemDomainCache() {
		
		BizSystemDomain query=new BizSystemDomain();
		query.setApplyStatus(1);
		query.setEnable(1);	
		List<BizSystemDomain> bizSystemDomains = bizSystemDomainService.getAllBizSystemDomain(query);
		Cache cache = cacheManager.getCacheManager().getCache(BIZ_SYSTEM_DOMAIN_CACHE);
		if(cache != null) {
			cache.removeAll();
			if(CollectionUtils.isNotEmpty(bizSystemDomains)) {
				for(BizSystemDomain bizSystemDomain : bizSystemDomains) {
					Element element = new Element(bizSystemDomain.getId(), bizSystemDomain);
					cache.put(element);
				}
			}
		}
		logger.info("初始化域名管理缓存结束...");
	}
	
	
	/**
	 * 初始化系统信息管理缓存
	 */
	public static void initBizSystemInfoCache() {
		
		List<BizSystemInfo> bizSystemInfos = bizSystemInfoService.getAllBizSystemInfo();
		Cache cache = cacheManager.getCacheManager().getCache(BIZ_SYSTEM_INFO_CACHE);
		if(cache != null) {
			cache.removeAll();
			if(CollectionUtils.isNotEmpty(bizSystemInfos)) {
				for(BizSystemInfo bizSystemInfo : bizSystemInfos) {
					//重设图片url地址
					bizSystemInfo.resetImageUrl();
					Element element = new Element(bizSystemInfo.getId(), bizSystemInfo);
					cache.put(element);
				}
			}
		}
		logger.info("初始化业务系统缓存结束...");
	}
	
	/**
	 * 初始化业务系统奖金模式配置缓存
	 */
	public static void initAwardModelConfigCache() {
		AwardModelConfigService awardModelConfigService = ApplicationContextUtil.getBean(AwardModelConfigService.class);
		List<AwardModelConfig> awardModelConfigs = awardModelConfigService.getAllAwardModelConfigs();
		Cache cache = cacheManager.getCacheManager().getCache(AWARD_MODEL_CONFIG_CACHE);
		if(cache != null) {
			cache.removeAll();
			if(CollectionUtils.isNotEmpty(awardModelConfigs)) {
				for(AwardModelConfig awardModelConfig : awardModelConfigs) {
					Element element = new Element(awardModelConfig.getId(), awardModelConfig);
					cache.put(element);
				}
			}
		}
		logger.info("初始化业务系统奖金模式配置缓存结束...");
	}
	
	/**
	 * 更新业务系统缓存
	 */
	public static void updateBizSystemCache(BizSystem bizSystem,String operateType) {
		
		Cache cache = cacheManager.getCacheManager().getCache(BIZ_SYSTEM_CACHE);
		if(bizSystem!=null)
		{
			if("-1".equals(operateType)){
				cache.remove(bizSystem.getId());
			}else if("0".equals(operateType)){
				cache.remove(bizSystem.getId());
				bizSystem =bizSystemService.selectByPrimaryKey(bizSystem.getId());
				Element element = new Element(bizSystem.getId(), bizSystem);
				cache.put(element);
			}else if("1".equals(operateType)){
				bizSystem =bizSystemService.selectByPrimaryKey(bizSystem.getId());
				Element element = new Element(bizSystem.getId(), bizSystem);
				cache.put(element);
			}
		}
		logger.info("更新业务系统缓存结束...");
	}
	
	/**
	 * 更新域名缓存
	 */
	public static void updateBizSystemDomainCache(BizSystemDomain bizSystemDomain,String operateType) {
		
		Cache cache = cacheManager.getCacheManager().getCache(BIZ_SYSTEM_DOMAIN_CACHE);
		if(bizSystemDomain!=null)
		{
			if("-1".equals(operateType)){
				cache.remove(bizSystemDomain.getId());
			}else if("0".equals(operateType)){
				cache.remove(bizSystemDomain.getId());
				bizSystemDomain= bizSystemDomainService.selectByPrimaryKey(bizSystemDomain.getId());
				Element element = new Element(bizSystemDomain.getId(), bizSystemDomain);
				cache.put(element);
			}else if("1".equals(operateType)){
				bizSystemDomain= bizSystemDomainService.selectByPrimaryKey(bizSystemDomain.getId());
				Element element = new Element(bizSystemDomain.getId(), bizSystemDomain);
				cache.put(element);
			}
		}
		logger.info("更新域名缓存结束...");
	}
	
	/**
	 * 更新信息管理缓存
	 */
	public static void updateBizSystemInfoCache(BizSystemInfo bizSystemInfo,String operateType) {
		
		Cache cache = cacheManager.getCacheManager().getCache(BIZ_SYSTEM_INFO_CACHE);
		if(bizSystemInfo!=null)
		{
			//新增
			if("-1".equals(operateType)){
				cache.remove(bizSystemInfo.getId());
			//更新
			}else if("0".equals(operateType)){
				cache.remove(bizSystemInfo.getId());
				bizSystemInfo = bizSystemInfoService.selectByPrimaryKey(bizSystemInfo.getId());
				//重设图片url地址
				bizSystemInfo.resetImageUrl();
				Element element = new Element(bizSystemInfo.getId(), bizSystemInfo);
				cache.put(element);
			//新增
			}else if("1".equals(operateType)){
				bizSystemInfo = bizSystemInfoService.getBizSystemInfoByCondition(bizSystemInfo);
				//重设图片url地址
				bizSystemInfo.resetImageUrl();
				Element element = new Element(bizSystemInfo.getId(), bizSystemInfo);
				cache.put(element);
			}
		}
		logger.info("更新信息管理缓存结束...");
	}
	
	/**
	 * 根据业务系统简称获取中文名称
	 * @param bizSystemCode
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static String getBizSystemNameByCode(String bizSystemCode) {
		String bizSystemName = "";
		if(bizSystemCode==null || "".equals(bizSystemCode) || ConstantUtil.SUPER_SYSTEM.equals(bizSystemCode)){
			return "超级系统";
		}
		Cache cache = cacheManager.getCacheManager().getCache(BIZ_SYSTEM_CACHE);
		List<Long> keys = (List<Long>)cache.getKeys();
		for(Long key : keys) {
			Element element = cache.get(key);
			BizSystem bizSystem = (BizSystem) element.getObjectValue();
			if(bizSystemCode.equals(bizSystem.getBizSystem())) {
				bizSystemName = bizSystem.getBizSystemName();
				break;
			}
		}
		return bizSystemName;
	}
	
	/**
	 * 根据业务系统简称获取BizSystem对象
	 * @param bizSystemCode
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static BizSystem getBizSystemByCode(String bizSystemCode) {
		if(bizSystemCode==null||"".equals(bizSystemCode)){
			return null;
		}
		Cache cache = cacheManager.getCacheManager().getCache(BIZ_SYSTEM_CACHE);
		List<Long> keys = (List<Long>)cache.getKeys();
		BizSystem bizSystem=null;
		for(Long key : keys) {
			Element element = cache.get(key);
			 bizSystem = (BizSystem) element.getObjectValue();
			 if(bizSystemCode.equals(bizSystem.getBizSystem())) {
				 return bizSystem;
			 }
		}
		return bizSystem;
	}
	
	
	/**
	 * 获取缓存的List<BizSystem>，不从数据库直接取
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<BizSystem> getAllBizSystem()
	{
		List<BizSystem> list = new ArrayList<BizSystem>();
		Cache cache = cacheManager.getCacheManager().getCache(BIZ_SYSTEM_CACHE);
		List<Long> keys = (List<Long>)cache.getKeys();
		for(Long key : keys) {
			Element element = cache.get(key);
			BizSystem bizSystem = (BizSystem) element.getObjectValue();
			list.add(bizSystem);
		}
		return list;
		
	}
	
	/**
	 * 根据业务系统获取对应的系统信息
	 * @param domain
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static BizSystemInfo getBizSystemInfo(String bizSystem) {
		if(bizSystem==null||"".equals(bizSystem)){
			return null;
		}
		Cache cache = cacheManager.getCacheManager().getCache(BIZ_SYSTEM_INFO_CACHE);
		List<Long> keys = (List<Long>)cache.getKeys();
		for(Long key : keys) {
			Element element = cache.get(key);
			BizSystemInfo bizSystemInfo = (BizSystemInfo) element.getObjectValue();
			if(bizSystemInfo != null && bizSystemInfo.getBizSystem().equals(bizSystem)) {
				return bizSystemInfo;
			}
		}
		return null;
	}
	
	
	/**
	 * 根据域名获取对应的系统信息
	 * @param domain
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static BizSystemInfo getBizSystemInfoByDomainUrl(String domainUrl) {
		if(domainUrl==null||"".equals(domainUrl)){
			return null;
		}
		String bizSystem = getBizSystemByDomainUrl(domainUrl);
		if(bizSystem==null||"".equals(bizSystem)){
			return null;
		}
		Cache cache = cacheManager.getCacheManager().getCache(BIZ_SYSTEM_INFO_CACHE);
		List<Long> keys = (List<Long>)cache.getKeys();
		for(Long key : keys) {
			Element element = cache.get(key);
			BizSystemInfo bizSystemInfo = (BizSystemInfo) element.getObjectValue();
			if(bizSystemInfo != null && bizSystemInfo.getBizSystem().equals(bizSystem)) {
				return bizSystemInfo;
			}
		}
		return null;
	}
	/**
	 * 根据域名地址获取对应的业务系统简称
	 * @param domain
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static String getBizSystemByDomainUrl(String domainUrl) {
		if(domainUrl==null||"".equals(domainUrl)){
			return null;
		}
		Cache cache = cacheManager.getCacheManager().getCache(BIZ_SYSTEM_DOMAIN_CACHE);
		List<Long> keys = (List<Long>)cache.getKeys();
		for(Long key : keys) {
			Element element = cache.get(key);
			BizSystemDomain bizSystem = (BizSystemDomain) element.getObjectValue();
			if(bizSystem != null && bizSystem.getDomainUrl().equals(domainUrl)) {
				return bizSystem.getBizSystem();
			}
		}
		return "";
	}
	
	/**
	 * 根据域名地址获取对应的信息
	 * @param domain
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static BizSystemDomain getDomainInfoByUrl(String domainUrl) {
		if(domainUrl==null||"".equals(domainUrl)){
			return null;
		}
		Cache cache = cacheManager.getCacheManager().getCache(BIZ_SYSTEM_DOMAIN_CACHE);
		List<Long> keys = (List<Long>)cache.getKeys();
		for(Long key : keys) {
			Element element = cache.get(key);
			BizSystemDomain bizSystemDomain = (BizSystemDomain) element.getObjectValue();
			if(bizSystemDomain != null && bizSystemDomain.getDomainUrl().equals(domainUrl)) {
				return bizSystemDomain;
			}
		}
		return null;
	}
	
	/**
	 * 根据域名地址获取对应的域名类型
	 * @param domainUrl
	 * @return
	 */
	public static String getLoginType(String domainUrl){
		if(domainUrl==null||"".equals(domainUrl)){
			return null;
		}
		BizSystemDomain domainInfoByUrl = ClsCacheManager.getDomainInfoByUrl(domainUrl);
		if(domainInfoByUrl != null){
			return domainInfoByUrl.getDomainType();
		}
		
		return null;
	}
	
	/**
	 * 根据域名地址获取对应的首推域名地址
	 * @param domainUrl
	 * @param loginType
	 * @return
	 */
	public static BizSystemDomain getFirstDomain(String domainUrl,String loginType){
		if (domainUrl == null || "".equals(domainUrl)) {
			return null;
		}
		if (loginType == null || "".equals(loginType)) {
			return null;
		}
		BizSystemDomain returnMobileDomain = null;
		Cache cache = cacheManager.getCacheManager().getCache(BIZ_SYSTEM_DOMAIN_CACHE);
		String bizSystem = getBizSystemByDomainUrl(domainUrl);
		BizSystemDomain firstMobileDomain = null;
		@SuppressWarnings("unchecked")
		List<Long> keys = (List<Long>) cache.getKeys();

		returnMobileDomain = getDomainInfoByUrl(domainUrl);
		for (Long mobileKey : keys) {
			Element mobileElement = cache.get(mobileKey);
			BizSystemDomain mobileDomain = (BizSystemDomain) mobileElement.getObjectValue();
			if (mobileDomain != null && bizSystem != null && bizSystem.equals(mobileDomain.getBizSystem())) {
				returnMobileDomain = mobileDomain;
				if (loginType.equals(mobileDomain.getDomainType())) {
					// 找到首推域名
					if (mobileDomain.getFirstFlag() == 1) {
						firstMobileDomain = returnMobileDomain;
						return firstMobileDomain;
					}
				}
			}
		}
		return returnMobileDomain;
	}
	
	/**
	 * 根据域名地址获取对应的手机域名地址
	 * @param domain
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static BizSystemDomain getMobileDomainByDomainUrl(String domainUrl) {
		if (domainUrl == null || "".equals(domainUrl)) {
			return null;
		}
		BizSystemDomain returnMobileDomain = null;
		Cache cache = cacheManager.getCacheManager().getCache(BIZ_SYSTEM_DOMAIN_CACHE);
		String bizSystem = getBizSystemByDomainUrl(domainUrl);
		BizSystemDomain firstMobileDomain = null;
		List<Long> keys = (List<Long>) cache.getKeys();

		returnMobileDomain = getDomainInfoByUrl(domainUrl);
		// 判断是否有跳转手机域名
		if (returnMobileDomain != null) {
			if ((returnMobileDomain.getJumpMobileDomainUrl() != null
					&& !"".equals(returnMobileDomain.getJumpMobileDomainUrl()))) {
				returnMobileDomain = getDomainInfoByUrl(returnMobileDomain.getJumpMobileDomainUrl());

			} else {
				for (Long mobileKey : keys) {
					Element mobileElement = cache.get(mobileKey);
					BizSystemDomain mobileDomain = (BizSystemDomain) mobileElement.getObjectValue();
					if (mobileDomain != null && bizSystem != null && bizSystem.equals(mobileDomain.getBizSystem())) {
						returnMobileDomain = mobileDomain;
						if ("MOBILE".equals(mobileDomain.getDomainType())) {
							// 找到手机首推
							if (mobileDomain.getFirstFlag() == 1) {
								firstMobileDomain = returnMobileDomain;
								return firstMobileDomain;
							}

						}
					}
				}
			}
		} else {
			return null;
		}
		return returnMobileDomain;
	}
	
	/**
	 * 根据业务系统简称获取系统奖金模式配置对象
	 * @param bizSystemCode
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static AwardModelConfig getAwardModelConfigByCode(String bizSystemCode) {
		
		if(bizSystemCode==null||"".equals(bizSystemCode)){
			return null;
		}
		Cache cache = cacheManager.getCacheManager().getCache(AWARD_MODEL_CONFIG_CACHE);
		List<Long> keys = (List<Long>)cache.getKeys();
		AwardModelConfig awardModelConfig = null;
		for(Long key : keys) {
			Element element = cache.get(key);
			awardModelConfig = (AwardModelConfig) element.getObjectValue();
			if(bizSystemCode.equals(awardModelConfig.getBizSystem())) {
				return awardModelConfig;
			}
		}
		return awardModelConfig;
	}
	
	/**
	 * 初始化邮件发送配置缓存
	 */
	public static void initEmailSendConfigCache() {
		logger.info("开始初始化邮件发送配置缓存...");
		List<EmailSendConfig> emailSendConfigs = emailSendConfigService.queryAllEmailSendConfig();
		Cache cache = cacheManager.getCacheManager().getCache(EMAIL_SEND_CONFIG_CACHE);
		if(cache != null) {
			cache.removeAll();
			if(CollectionUtils.isNotEmpty(emailSendConfigs)) {
				for(EmailSendConfig emailSendConfig : emailSendConfigs) {
					Element element = new Element(emailSendConfig.getId(), emailSendConfig);
					cache.put(element);
				}
			}
		}
		logger.info("初始化邮件发送配置缓存结束...");
	}
	
	/**
	 * 初始化短信发送配置缓存
	 */
	public static void initSmsSendConfigCache() {
		logger.info("开始初始化短信发送配置缓存...");
		List<SmsSendConfig> smsSendConfigs = smsSendConfigService.queryAllSmsSendConfig();
		Cache cache = cacheManager.getCacheManager().getCache(SMS_SEND_CONFIG_CACHE);
		if(cache != null) {
			cache.removeAll();
			if(CollectionUtils.isNotEmpty(smsSendConfigs)) {
				for(SmsSendConfig smsSendConfig : smsSendConfigs) {
					Element element = new Element(smsSendConfig.getId(), smsSendConfig);
					cache.put(element);
				}
			}
		}
		logger.info("初始化短信发送配置缓存结束...");
	}
	
	/**
	 * 从配置缓存中获取邮件发送配置
	 * @param type
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static EmailSendConfig getEmailSendConfig(String type) {
		Cache cache = cacheManager.getCacheManager().getCache(EMAIL_SEND_CONFIG_CACHE);
		List<Integer> keys = (List<Integer>)cache.getKeys();
		EmailSendConfig emailSendConfig = null;
		for(Integer key : keys) {
			Element element = cache.get(key);
			EmailSendConfig cacheConfig = (EmailSendConfig) element.getObjectValue();
			//有判断类型的情况
			if(StringUtils.isNotBlank(type) && type.equals(cacheConfig.getType())) {
				//第一次直接添加
				if(emailSendConfig == null) {
					emailSendConfig = cacheConfig;
				} else {
					//比较顺序值,顺序值小的优先使用
					if(cacheConfig.getOrders() < emailSendConfig.getOrders()) {
						emailSendConfig = cacheConfig;
					}
				}
			//没有判断类型的情况	
			} else {
				//第一次直接添加
				if(emailSendConfig == null) {
					emailSendConfig = cacheConfig;
				} else {
					//比较顺序值,顺序值小的优先使用
					if(cacheConfig.getOrders() < emailSendConfig.getOrders()) {
						emailSendConfig = cacheConfig;
					}
				}
			}
		}
		return emailSendConfig;
	}
	
	/**
	 * 获取全局且业务系统已开启的彩种
	 * @return
	 */
	public static  Map<String,String> getOpenLotteryKinds(String bizSystem){
		Map<String,String> map=new HashMap<String, String>();
		Map<String, String> systemConfigMap = systemConfigService.getSystemConfigByLikeValue("is%Open");
    	Map<String,String> bizSystemConfigConfigMap=bizSystemConfigService.getBizSystemConfigByLikeValue("is%Open",bizSystem);
    	
		for (String systemKindName : systemConfigMap.keySet()) {
			String systemKindNameValue = systemConfigMap.get(systemKindName);
			if("1".equals(systemKindNameValue)){
				//全局开启的彩种才存入List
				for(String kindName:bizSystemConfigConfigMap.keySet()){
					String kindNameValue = bizSystemConfigConfigMap.get(kindName);
					if(systemKindName.equals(kindName) && "1".equals(kindNameValue)){
						String k = kindName.substring(2, kindName.length()-4);
						map.put(k, k);
					}
				}
			}
		}
    	return map;
	}
	
	/**
	 * 从配置缓存中获取短信发送配置
	 * @param type
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static SmsSendConfig getSmsSendConfig(String type) {
		Cache cache = cacheManager.getCacheManager().getCache(SMS_SEND_CONFIG_CACHE);
		List<Integer> keys = (List<Integer>)cache.getKeys();
		SmsSendConfig smsSendConfig = null;
		for(Integer key : keys) {
			Element element = cache.get(key);
			SmsSendConfig cacheConfig = (SmsSendConfig) element.getObjectValue();
			//有判断类型的情况
			if(StringUtils.isNotBlank(type) && type.equals(cacheConfig.getType())) {
				//第一次直接添加
				if(smsSendConfig == null) {
					smsSendConfig = cacheConfig;
				} else {
					//比较顺序值,顺序值小的优先使用
					if(cacheConfig.getOrders() < smsSendConfig.getOrders()) {
						smsSendConfig = cacheConfig;
					}
				}
			//没有判断类型的情况	
			} else {
				//第一次直接添加
				if(smsSendConfig == null) {
					smsSendConfig = cacheConfig;
				} else {
					//比较顺序值,顺序值小的优先使用
					if(cacheConfig.getOrders() < smsSendConfig.getOrders()) {
						smsSendConfig = cacheConfig;
					}
				}
			}
		}
		return smsSendConfig;
	}
	
	
	/**
	 * 根据域名,ip判断是否授权,无效就放过
	 * @param domain
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static boolean isAllowIPByDomainIp(String domainUrl,String ip) {
 		if(domainUrl==null||"".equals(domainUrl)||ip==null||"".equals(ip)){
			return false;
		}
		Cache cache = cacheManager.getCacheManager().getCache(MGR_BIZ_SYSTEM_DOMAIN_CACHE);
		List<Integer> keys = (List<Integer>)cache.getKeys();
		
		boolean isFindMatch = false;
		//所有记录数
		int allCount = 0;
		//停用记录数
		int stopCount = 0;
		if(CollectionUtils.isNotEmpty(keys)) {
			allCount = keys.size();
			//域名验证
 			for(Integer key : keys) {
				boolean isDomainAllow = false;
				boolean isIpAllow = false;
				Element element = cache.get(key);
				AdminDomain adminDomain = (AdminDomain) element.getObjectValue();
				if(adminDomain.getEnable() == 0){
					stopCount++;
					continue;
				}else {
					//域名全匹配
					if(adminDomain.getDomains().equals(domainUrl)) {
						isDomainAllow = true;
					}
					//允许ip空记录或者再允许的ip配置里，允许访问
					if(StringUtils.isBlank(adminDomain.getAllowIps())) {
						isIpAllow = true;
					} else {
						String[] allowIps = adminDomain.getAllowIps().split(",");
						for(String allowIp : allowIps) {
							if(allowIp.equals(ip)) {
								isIpAllow = true;
								break;
							}
						}
					}
					if(isDomainAllow && isIpAllow) {
						isFindMatch = true;
						break;
					}
				}
			}
		}
		if(isFindMatch) {
			return true;
		} else {
			//如果没有找到匹配的，所有记录都是停用的，则放过
			if(allCount == stopCount) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	/**
	 * 初始化彩种开关配置
	 */
	public static void initLotterySwitchCache() {
		// 获取系统中所有的彩种开关.
		List<LotterySwitch> allLotterySwitchs = lotterySwitchService.getAllLotterySwitchs();
		Cache cache = cacheManager.getCacheManager().getCache(LOTTERY_SWITCH_CACHE);
		if (cache != null) {
			cache.removeAll();
			if (CollectionUtils.isNotEmpty(allLotterySwitchs)) {
				for (LotterySwitch lotterySwitch : allLotterySwitchs) {
					Element element = new Element(lotterySwitch.getId(), lotterySwitch);
					cache.put(element);
				}
			}
		}
		logger.info("初始化彩种开关缓存结束...");
	}
	
	/**
	 * 根据业务更新彩种开关缓存
	 */
	@SuppressWarnings("unchecked")
	public static void updateLotterySwitchCache(String bizSystem) {
		Cache cache = cacheManager.getCacheManager().getCache(LOTTERY_SWITCH_CACHE);
		// 新建list存储从缓存中查询出来的的LotterySwitch对象.
		List<LotterySwitch> lotterySwitchForCache = new ArrayList<LotterySwitch>();
		List<Long> keys = (List<Long>) cache.getKeys();
		for (Long key : keys) {
			Element element = cache.get(key);
			LotterySwitch lotterySwitch = (LotterySwitch) element.getObjectValue();
			lotterySwitchForCache.add(lotterySwitch);
		}

		for (LotterySwitch lotterySwitch : lotterySwitchForCache) {
			if (lotterySwitch.getBizSystem().equals(bizSystem)) {
				// 根据ID删除.
				cache.remove(lotterySwitch.getId());
				lotterySwitchForCache.remove(lotterySwitch);
				break;
			}
		}
		// 根据业务系统查询出对应的彩种开关对象.
		List<LotterySwitch> allLotterySwitchsByBizSystem = lotterySwitchService.getAllLotterySwitchsByBizSystem(bizSystem);
		if (cache != null) {
			if (CollectionUtils.isNotEmpty(allLotterySwitchsByBizSystem)) {
				for (LotterySwitch lotterySwitch : allLotterySwitchsByBizSystem) {
					Element element = new Element(lotterySwitch.getId(), lotterySwitch);
					cache.put(element);
				}
			}
		}
		logger.info("更新[{}]系统彩种开关统缓存结束...", bizSystem);
	}
	
	/**
	 * 从缓存中根据系统获取彩种开关状态.
	 * @param bizSystem
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<LotterySwitch> getLotterySwitchByBizSystem(String bizSystem) {
		// 判断参数状态是否为空.
		if (StringUtils.isEmpty(bizSystem)) {
			return null;
		}
		// 新建list存储从缓存中查询出来的的LotterySwitch对象.
		List<LotterySwitch> list = new ArrayList<LotterySwitch>();
		// 从缓存中获取参数.
		Cache cache = cacheManager.getCacheManager().getCache(LOTTERY_SWITCH_CACHE);
		List<Long> keys = (List<Long>) cache.getKeys();
		for (Long key : keys) {
			Element element = cache.get(key);
			LotterySwitch lotterySwitch = (LotterySwitch) element.getObjectValue();
			if (lotterySwitch.getBizSystem().equals(bizSystem)) {
				list.add(lotterySwitch);
			}
		}
		return list;
	}
	
	/**
	 * 根据业务系统和彩种返回彩种的状态值.
	 * @param bizSystem
	 * @param lotteryType
	 * @return
	 */
	public static Integer getLotteryStatusByLotteryType(String bizSystem, String lotteryType) {
		// 判断参数状态是否为空.
		if (StringUtils.isEmpty(bizSystem) || StringUtils.isEmpty(lotteryType)) {
			return null;
		}
		// 初始化彩种开关值.
		Integer lotteryStatus = null;
		List<LotterySwitch> lotterySwitchByBizSystem = getLotterySwitchByBizSystem(bizSystem);
		if (CollectionUtils.isNotEmpty(lotterySwitchByBizSystem)) {
			for (LotterySwitch lotterySwitch : lotterySwitchByBizSystem) {
				if (lotterySwitch.getLotteryType().equals(lotteryType)) {
					lotteryStatus = lotterySwitch.getLotteryStatus();
					break;
				}
			}
		}
		// 返回彩种开关状态.
		return lotteryStatus;
	}
	
	/**
	 * 根据业务系统获取对应系统的彩种状态(通过全局开关和业务系统开关获取);
	 * @param bizSystem
	 * @return
	 */
	public static List<LotterySwitch> getLotterySwitchsStatusByBizSystem(String bizSystem){
		// 获取全局系统彩种开关.
		List<LotterySwitch> lotterySwitchsBySystem = getLotterySwitchByBizSystem("SUPER_SYSTEM");
		// 获取业务系统彩种开关.
		List<LotterySwitch> lotterySwitchsByBizSystem = getLotterySwitchByBizSystem(bizSystem);
		// 最终要返回的彩种开关数据.
		List<LotterySwitch> returnLotterySwitchs = new ArrayList<LotterySwitch>();
		// 找出符合条件的数据,
		for (LotterySwitch lotterySwitchSystem : lotterySwitchsBySystem) {
			// 全局彩种开关开启,就以业务彩种开关为准
			if (lotterySwitchSystem.getLotteryStatus() == 1) {
				for (LotterySwitch lotterySwitchBizSystem : lotterySwitchsByBizSystem) {
					if (lotterySwitchSystem.getLotteryType().equals(lotterySwitchBizSystem.getLotteryType())) {
						returnLotterySwitchs.add(lotterySwitchBizSystem);
						continue;
					} 
				}
			} else {
				for (LotterySwitch lotterySwitchBizSystem : lotterySwitchsByBizSystem) {
					if (lotterySwitchSystem.getLotteryType().equals(lotterySwitchBizSystem.getLotteryType())) {
						// 设置业务系统开关状态为全局状态开关
						lotterySwitchBizSystem.setLotteryStatus(lotterySwitchSystem.getLotteryStatus());
						returnLotterySwitchs.add(lotterySwitchBizSystem);
						continue;
					} 
				}
			}
		}
		return returnLotterySwitchs;
	}
	 
}
