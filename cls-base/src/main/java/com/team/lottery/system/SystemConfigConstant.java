package com.team.lottery.system;

import java.math.BigDecimal;

/**
 * 从数据库读取kr_system_config
 * @author zzh
 *
 */
public class SystemConfigConstant {
	
	//系统秘密验证码
	public static String secretCode;
	
	//默认设置子系统超级管理员默认密码
	public static String defaultAdminPassword;
	
	//默认后台系统分页条数
	public static Integer defaultMgrPageSize;
	
	//默认前台系统分页条数
	public static Integer frontDefaultPageSize;
	
	//最大登陆验证次数
	public static Integer maxLoginCount=6;
	
	//单点登陆开关
	public static Integer isStationLogin;

	//后台登陆开关
	public static Integer managerLoginSwich;
	
	//追号开关
	public static Integer allowZhuihao;
	
	
	//ftp远程主机IP
	public static String picHost;
	
    //ftp远程主机端口
	public static Integer picPort = 22;
	
    //ftp远程主机账户
	public static String picUsername;
	
    //ftp远程主机密码
	public static String picPassword;
	
	//上传图片服务器路径
	public static String realImgFiledir;
	
	//图片服务器上传接口url
	public static String imgServerUploadUrl;
	
	//图片服务器访问url
	public static String imgServerUrl;
	
	
	
	//春节开始时间
	public static String springFestivalStartDate;
	//春节结束时间
	public static String springFestivalEndDate;
	
	//幸运分分彩盈利模式
	public static Integer jlffcProfitModel;
	
	//幸运快三盈利模式
	public static Integer jyksProfitModel;
		
    
	//平台维护开关
	public static String maintainSwich;
	
	//平台系统维护说明
	public static String maintainContent;
	
	
	//PK10基准日期
	public static String pk10BaseDate;
	//PK10基准期数
	public static Integer pk10BasetExpect;
	
	//韩国1.5分彩基准日期
	public static String hgffcBaseDate;
	//韩国1.5基准期数
	public static Integer hgffcBasetExpect;
	
	//北京快3基准日期
	public static String bjksBaseDate;
	//北京快3基准期数
	public static Integer bjksBasetExpect;
	
	//新加坡2分彩基准日期
	public static String xjplfcBaseDate;
	//新加坡2分彩基准期数
	public static Integer xjplfcBasetExpect;
	
	//台湾5分彩基准日期
	public static String twwfcBaseDate;
	//台湾5分彩基准期数
	public static Integer twwfcBasetExpect;
   
	//幸运28基准日期
	public static String xyebBaseDate;
	//幸运28分彩基准期数
	public static Integer xyebBasetExpect;

    //时时彩最低奖金模式
	public static BigDecimal SSCLowestAwardModel;
	
	//时时彩最高奖金模式
	public static BigDecimal SSCHighestAwardModel;
	
	//分分彩最低奖金模式
	public static BigDecimal FFCLowestAwardModel;
	
	//分分彩奖最高金模式
	public static BigDecimal FFCHighestAwardModel;
	
	//11选5奖最低金模式
	public static BigDecimal SYXWLowestAwardModel;
	
	//11选5奖最高金模式
	public static BigDecimal SYXWHighestAwardModel;
	
	//快3奖最低金模式
	public static BigDecimal KSLowestAwardModel;
	
	//快3奖最高金模式
	public static BigDecimal KSHighestAwardModel;
	
	//PK10最低奖金模式
	public static BigDecimal PK10LowestAwardModel;
	
	//PK10最高奖金模式
	public static BigDecimal PK10HighestAwardModel;
	
	//低频彩奖最低金模式
	public static BigDecimal DPCLowestAwardModel;
	
	//低频彩奖最高金模式
	public static BigDecimal DPCHighestAwardModel;
	
	//骰宝最低奖金模式
	public static BigDecimal TBLowestAwardModel;
	
	//骰宝最高奖金模式
	public static BigDecimal TBHighestAwardModel;
	
	//六合彩最低奖金模式
	public static BigDecimal LHCLowestAwardModel;
	
	//六合彩最高奖金模式
	public static BigDecimal LHCHighestAwardModel;
	
	//快乐十分奖最低金模式
	public static BigDecimal KLSFLowestAwardModel;
	
	//快乐十分奖最高金模式
	public static BigDecimal KLSFHighestAwardModel;
	
	//允许放过的地址
	public static String allowUrls = "";
	
	//当前生肖属性
	public static String currentZodiac;
	
	//pc站点资源版本号
	public static String pcWebRsVersion;
	//手机站点资源版本号
	public static String mobileWebRsVersion;
	//后台站点资源版本号
	public static String mgrWebRsVersion;
	//聊天服务器IP
	public static String chatRoomIp;
	//聊天服务器端口号
	public static String chatRoomPort;
	
	//彩票计划开关
	public static String codePlanSwitch;

	
	public static String getCodePlanSwitch() {
		return codePlanSwitch;
	}

	public static void setCodePlanSwitch(String codePlanSwitch) {
		SystemConfigConstant.codePlanSwitch = codePlanSwitch;
	}

	public String getChatRoomIp() {
		return chatRoomIp;
	}

	public void setChatRoomIp(String chatRoomIp) {
		SystemConfigConstant.chatRoomIp = chatRoomIp;
	}

	public String getChatRoomPort() {
		return chatRoomPort;
	}

	public void setChatRoomPort(String chatRoomPort) {
		SystemConfigConstant.chatRoomPort = chatRoomPort;
	}

	public String getPcWebRsVersion() {
		return pcWebRsVersion;
	}

	public void setPcWebRsVersion(String pcWebRsVersion) {
		SystemConfigConstant.pcWebRsVersion = pcWebRsVersion;
	}

	public String getMobileWebRsVersion() {
		return mobileWebRsVersion;
	}

	public void setMobileWebRsVersion(String mobileWebRsVersion) {
		SystemConfigConstant.mobileWebRsVersion = mobileWebRsVersion;
	}

	public String getMgrWebRsVersion() {
		return mgrWebRsVersion;
	}

	public void setMgrWebRsVersion(String mgrWebRsVersion) {
		SystemConfigConstant.mgrWebRsVersion = mgrWebRsVersion;
	}

	public  String getXyebBaseDate() {
		return xyebBaseDate;
	}

	public  void setXyebBaseDate(String xyebBaseDate) {
		SystemConfigConstant.xyebBaseDate = xyebBaseDate;
	}

	public  Integer getXyebBasetExpect() {
		return xyebBasetExpect;
	}

	public  void setXyebBasetExpect(Integer xyebBasetExpect) {
		SystemConfigConstant.xyebBasetExpect = xyebBasetExpect;
	}

	public String getSecretCode() {
		return secretCode;
	}

	public void setSecretCode(String secretCode) {
		SystemConfigConstant.secretCode = secretCode;
	}

	public String getDefaultAdminPassword() {
		return defaultAdminPassword;
	}

	public void setDefaultAdminPassword(String defaultAdminPassword) {
		SystemConfigConstant.defaultAdminPassword = defaultAdminPassword;
	}

	public Integer getDefaultMgrPageSize() {
		return defaultMgrPageSize;
	}

	public void setDefaultMgrPageSize(Integer defaultMgrPageSize) {
		SystemConfigConstant.defaultMgrPageSize = defaultMgrPageSize;
	}

	public Integer getFrontDefaultPageSize() {
		return frontDefaultPageSize;
	}

	public void setFrontDefaultPageSize(Integer frontDefaultPageSize) {
		SystemConfigConstant.frontDefaultPageSize = frontDefaultPageSize;
	}

	public Integer getMaxLoginCount() {
		return maxLoginCount;
	}

	public void setMaxLoginCount(Integer maxLoginCount) {
		SystemConfigConstant.maxLoginCount = maxLoginCount;
	}

	public Integer getIsStationLogin() {
		return isStationLogin;
	}

	public void setIsStationLogin(Integer isStationLogin) {
		SystemConfigConstant.isStationLogin = isStationLogin;
	}

	public Integer getManagerLoginSwich() {
		return managerLoginSwich;
	}

	public void setManagerLoginSwich(Integer managerLoginSwich) {
		SystemConfigConstant.managerLoginSwich = managerLoginSwich;
	}

	public Integer getAllowZhuihao() {
		return allowZhuihao;
	}

	public void setAllowZhuihao(Integer allowZhuihao) {
		SystemConfigConstant.allowZhuihao = allowZhuihao;
	}

	public String getPicHost() {
		return picHost;
	}

	public void setPicHost(String picHost) {
		SystemConfigConstant.picHost = picHost;
	}

	public Integer getPicPort() {
		return picPort;
	}

	public void setPicPort(Integer picPort) {
		SystemConfigConstant.picPort = picPort;
	}

	public String getPicUsername() {
		return picUsername;
	}

	public void setPicUsername(String picUsername) {
		SystemConfigConstant.picUsername = picUsername;
	}

	public String getPicPassword() {
		return picPassword;
	}

	public void setPicPassword(String picPassword) {
		SystemConfigConstant.picPassword = picPassword;
	}

	public String getRealImgFiledir() {
		return realImgFiledir;
	}

	public void setRealImgFiledir(String realImgFiledir) {
		SystemConfigConstant.realImgFiledir = realImgFiledir;
	}

	public String getImgServerUrl() {
		return imgServerUrl;
	}

	public void setImgServerUrl(String imgServerUrl) {
		SystemConfigConstant.imgServerUrl = imgServerUrl;
	}
	
	public String getImgServerUploadUrl() {
		return imgServerUploadUrl;
	}

	public void setImgServerUploadUrl(String imgServerUploadUrl) {
		SystemConfigConstant.imgServerUploadUrl = imgServerUploadUrl;
	}

	public String getSpringFestivalStartDate() {
		return springFestivalStartDate;
	}

	public void setSpringFestivalStartDate(String springFestivalStartDate) {
		SystemConfigConstant.springFestivalStartDate = springFestivalStartDate;
	}

	public String getSpringFestivalEndDate() {
		return springFestivalEndDate;
	}

	public void setSpringFestivalEndDate(String springFestivalEndDate) {
		SystemConfigConstant.springFestivalEndDate = springFestivalEndDate;
	}

	public Integer getJlffcProfitModel() {
		return jlffcProfitModel;
	}

	public void setJlffcProfitModel(Integer jlffcProfitModel) {
		SystemConfigConstant.jlffcProfitModel = jlffcProfitModel;
	}
	
	public Integer getJyksProfitModel() {
		return jyksProfitModel;
	}

	public void setJyksProfitModel(Integer jyksProfitModel) {
		SystemConfigConstant.jyksProfitModel = jyksProfitModel;
	}

	public String getMaintainSwich() {
		return maintainSwich;
	}

	public void setMaintainSwich(String maintainSwich) {
		SystemConfigConstant.maintainSwich = maintainSwich;
	}

	public String getMaintainContent() {
		return maintainContent;
	}

	public void setMaintainContent(String maintainContent) {
		SystemConfigConstant.maintainContent = maintainContent;
	}

	public String getPk10BaseDate() {
		return pk10BaseDate;
	}

	public void setPk10BaseDate(String pk10BaseDate) {
		SystemConfigConstant.pk10BaseDate = pk10BaseDate;
	}

	public Integer getPk10BasetExpect() {
		return pk10BasetExpect;
	}

	public void setPk10BasetExpect(Integer pk10BasetExpect) {
		SystemConfigConstant.pk10BasetExpect = pk10BasetExpect;
	}

	public String getHgffcBaseDate() {
		return hgffcBaseDate;
	}

	public void setHgffcBaseDate(String hgffcBaseDate) {
		SystemConfigConstant.hgffcBaseDate = hgffcBaseDate;
	}

	public Integer getHgffcBasetExpect() {
		return hgffcBasetExpect;
	}

	public void setHgffcBasetExpect(Integer hgffcBasetExpect) {
		SystemConfigConstant.hgffcBasetExpect = hgffcBasetExpect;
	}

	public String getBjksBaseDate() {
		return bjksBaseDate;
	}

	public void setBjksBaseDate(String bjksBaseDate) {
		SystemConfigConstant.bjksBaseDate = bjksBaseDate;
	}

	public Integer getBjksBasetExpect() {
		return bjksBasetExpect;
	}

	public void setBjksBasetExpect(Integer bjksBasetExpect) {
		SystemConfigConstant.bjksBasetExpect = bjksBasetExpect;
	}

	public String getXjplfcBaseDate() {
		return xjplfcBaseDate;
	}

	public void setXjplfcBaseDate(String xjplfcBaseDate) {
		SystemConfigConstant.xjplfcBaseDate = xjplfcBaseDate;
	}

	public Integer getXjplfcBasetExpect() {
		return xjplfcBasetExpect;
	}

	public void setXjplfcBasetExpect(Integer xjplfcBasetExpect) {
		SystemConfigConstant.xjplfcBasetExpect = xjplfcBasetExpect;
	}

	public String getTwwfcBaseDate() {
		return twwfcBaseDate;
	}

	public void setTwwfcBaseDate(String twwfcBaseDate) {
		SystemConfigConstant.twwfcBaseDate = twwfcBaseDate;
	}

	public Integer getTwwfcBasetExpect() {
		return twwfcBasetExpect;
	}

	public void setTwwfcBasetExpect(Integer twwfcBasetExpect) {
		SystemConfigConstant.twwfcBasetExpect = twwfcBasetExpect;
	}

	public BigDecimal getSSCLowestAwardModel() {
		return SSCLowestAwardModel;
	}

	public void setSSCLowestAwardModel(BigDecimal sSCLowestAwardModel) {
		SSCLowestAwardModel = sSCLowestAwardModel;
	}

	public BigDecimal getSSCHighestAwardModel() {
		return SSCHighestAwardModel;
	}

	public void setSSCHighestAwardModel(BigDecimal sSCHighestAwardModel) {
		SSCHighestAwardModel = sSCHighestAwardModel;
	}

	public BigDecimal getFFCLowestAwardModel() {
		return FFCLowestAwardModel;
	}

	public void setFFCLowestAwardModel(BigDecimal fFCLowestAwardModel) {
		FFCLowestAwardModel = fFCLowestAwardModel;
	}

	public BigDecimal getFFCHighestAwardModel() {
		return FFCHighestAwardModel;
	}

	public void setFFCHighestAwardModel(BigDecimal fFCHighestAwardModel) {
		FFCHighestAwardModel = fFCHighestAwardModel;
	}

	public BigDecimal getSYXWLowestAwardModel() {
		return SYXWLowestAwardModel;
	}

	public void setSYXWLowestAwardModel(BigDecimal sYXWLowestAwardModel) {
		SYXWLowestAwardModel = sYXWLowestAwardModel;
	}

	public BigDecimal getSYXWHighestAwardModel() {
		return SYXWHighestAwardModel;
	}

	public void setSYXWHighestAwardModel(BigDecimal sYXWHighestAwardModel) {
		SYXWHighestAwardModel = sYXWHighestAwardModel;
	}

	public BigDecimal getKSLowestAwardModel() {
		return KSLowestAwardModel;
	}

	public void setKSLowestAwardModel(BigDecimal kSLowestAwardModel) {
		KSLowestAwardModel = kSLowestAwardModel;
	}

	public BigDecimal getKSHighestAwardModel() {
		return KSHighestAwardModel;
	}

	public void setKSHighestAwardModel(BigDecimal kSHighestAwardModel) {
		KSHighestAwardModel = kSHighestAwardModel;
	}

	public BigDecimal getPK10LowestAwardModel() {
		return PK10LowestAwardModel;
	}

	public void setPK10LowestAwardModel(BigDecimal pK10LowestAwardModel) {
		PK10LowestAwardModel = pK10LowestAwardModel;
	}

	public BigDecimal getPK10HighestAwardModel() {
		return PK10HighestAwardModel;
	}

	public void setPK10HighestAwardModel(BigDecimal pK10HighestAwardModel) {
		PK10HighestAwardModel = pK10HighestAwardModel;
	}

	public BigDecimal getDPCLowestAwardModel() {
		return DPCLowestAwardModel;
	}

	public void setDPCLowestAwardModel(BigDecimal dPCLowestAwardModel) {
		DPCLowestAwardModel = dPCLowestAwardModel;
	}

	public BigDecimal getDPCHighestAwardModel() {
		return DPCHighestAwardModel;
	}

	public void setDPCHighestAwardModel(BigDecimal dPCHighestAwardModel) {
		DPCHighestAwardModel = dPCHighestAwardModel;
	}

	public BigDecimal getTBLowestAwardModel() {
		return TBLowestAwardModel;
	}

	public void setTBLowestAwardModel(BigDecimal tBLowestAwardModel) {
		TBLowestAwardModel = tBLowestAwardModel;
	}

	public BigDecimal getTBHighestAwardModel() {
		return TBHighestAwardModel;
	}

	public void setTBHighestAwardModel(BigDecimal tBHighestAwardModel) {
		TBHighestAwardModel = tBHighestAwardModel;
	}

	public BigDecimal getLHCLowestAwardModel() {
		return LHCLowestAwardModel;
	}

	public void setLHCLowestAwardModel(BigDecimal lHCLowestAwardModel) {
		LHCLowestAwardModel = lHCLowestAwardModel;
	}

	public BigDecimal getLHCHighestAwardModel() {
		return LHCHighestAwardModel;
	}

	public void setLHCHighestAwardModel(BigDecimal lHCHighestAwardModel) {
		LHCHighestAwardModel = lHCHighestAwardModel;
	}

	public BigDecimal getKLSFLowestAwardModel() {
		return KLSFLowestAwardModel;
	}

	public void setKLSFLowestAwardModel(BigDecimal kLSFLowestAwardModel) {
		KLSFLowestAwardModel = kLSFLowestAwardModel;
	}

	public BigDecimal getKLSFHighestAwardModel() {
		return KLSFHighestAwardModel;
	}

	public void setKLSFHighestAwardModel(BigDecimal kLSFHighestAwardModel) {
		KLSFHighestAwardModel = kLSFHighestAwardModel;
	}

	public String getAllowUrls() {
		return allowUrls;
	}

	public void setAllowUrls(String allowUrls) {
		SystemConfigConstant.allowUrls = allowUrls;
	}

	public String getCurrentZodiac() {
		return currentZodiac;
	}

	public void setCurrentZodiac(String currentZodiac) {
		SystemConfigConstant.currentZodiac = currentZodiac;
	}
	
	
	
}
