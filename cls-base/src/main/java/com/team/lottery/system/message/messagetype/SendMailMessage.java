package com.team.lottery.system.message.messagetype;

import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.handler.SendMailMessageHandler;

public class SendMailMessage extends BaseMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6618480827230055185L;
	//邮件发送主题
	private String subject;
	//邮件发送内容
	private String content;
	
	public SendMailMessage() {
		this.handler=new SendMailMessageHandler();
	}
	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	
}
