package com.team.lottery.system.message.messagetype;

import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.handler.LotterySwitchMessageHandler;

public class LotterySwitchMessage extends BaseMessage {
	
	private static final long serialVersionUID = -2661744523907830230L;

	
	// 业务系统.
	private String bizSystem;
	
	//0=修改，1=增加，-1=删除
	private String operateType;


	public LotterySwitchMessage() {
		this.handler=new LotterySwitchMessageHandler();
	}
	
	public String getOperateType() {
		return operateType;
	}

	public void setOperateType(String operateType) {
		this.operateType = operateType;
	}

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}
	
}
