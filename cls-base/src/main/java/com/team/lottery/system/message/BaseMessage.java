package com.team.lottery.system.message;

import java.io.Serializable;

public class BaseMessage implements Serializable{
	
	private static final long serialVersionUID = 8385963407699230831L;

	protected BaseMessageHandler handler;
	
	/**
	 * 类型值
	 */
	private String type;

	/**
	 * 数据，预留
	 */
	private Object data;

	
	/**
	 * 获取对应的消息处理器
	 * @return
	 */
	public BaseMessageHandler getMessageHandler() {
		return handler;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public Object getData() {
		return data;
	}


	public void setData(Object data) {
		this.data = data;
	}
	
	
}
