package com.team.lottery.system.message.messagetype;

import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.handler.RedisPubSubMessageHandler;

public class RedisPubSubMessage extends BaseMessage {
     /**
	 * 
	 */
	private static final long serialVersionUID = -6371167127941441383L;

	/**
      * 消息对象
      */
	private BaseMessage baseMessage;
	
	/**
	 * pub=发布
	 */
	private String operateType;


	/**
	 * 频道
	 */
	private String channel;

	public RedisPubSubMessage() {
		this.handler=new RedisPubSubMessageHandler();
	}
    
	public BaseMessage getBaseMessage() {
		return baseMessage;
	}

	public void setBaseMessage(BaseMessage baseMessage) {
		this.baseMessage = baseMessage;
	}



	public String getOperateType() {
		return operateType;
	}

	public void setOperateType(String operateType) {
		this.operateType = operateType;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}
	
}
