package com.team.lottery.system.message.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.BaseMessageHandler;
import com.team.lottery.system.message.messagetype.BizSystemInfoMessage;

public class BizSystemInfoMessageHandler extends BaseMessageHandler  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7520734537128163439L;
	private static Logger logger = LoggerFactory.getLogger(BizSystemInfoMessageHandler.class);
	
	
	@Override
	public void dealMessage(BaseMessage message) {
		try {
			logger.info("接受redis订阅消息，开始更新BizSystemInfo的ehcache缓存...");
			BizSystemInfoMessage bizSysteinfomMessage=(BizSystemInfoMessage) message;
			if("0".equals(bizSysteinfomMessage.getOperateType())||"1".equals(bizSysteinfomMessage.getOperateType())||"-1".equals(bizSysteinfomMessage.getOperateType())){
				ClsCacheManager.updateBizSystemInfoCache(bizSysteinfomMessage.getBizSystemInfo(), bizSysteinfomMessage.getOperateType());
			}else{
			   ClsCacheManager.initBizSystemInfoCache();
			}
			//同时刷新奖金配置缓存数据
			//ClsCacheManager.initAwardModelConfigCache();
		} catch (Exception e) {
			logger.error("处理BizSystemInfo的ehcache发生错误...", e);
		}
	}

}
