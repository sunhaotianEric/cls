package com.team.lottery.system.message.messagetype;

import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.vo.LotteryCache;

/**
 * 投注缓存消息对象
 * @author luocheng
 *
 */
public class LotteryCacheMessage extends BaseMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5693429837052641861L;
	private LotteryCache lotteryCache;

	public LotteryCacheMessage() {
		this.handler= ApplicationContextUtil.getBean("lotteryCacheMessageHandler");
	}
	
	public LotteryCache getLotteryCache() {
		return lotteryCache;
	}

	public void setLotteryCache(LotteryCache lotteryCache) {
		this.lotteryCache = lotteryCache;
	}
	
	
}
