package com.team.lottery.system.message.messagetype;

import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.handler.BizSystemConfigMessageHandler;
import com.team.lottery.vo.BizSystemConfig;

public class BizSystemConfigMessage extends BaseMessage {


	/**
	 * 
	 */
	private static final long serialVersionUID = -8208413082642481317L;

	/**
      * 业务系统对象
      */
	private BizSystemConfig bizSystemConfig;
	
	/**
	 * 0=修改，1=增加，-1=删除
	 */
	private String operateType;
    
	public BizSystemConfigMessage() {
		this.handler=new BizSystemConfigMessageHandler();
	}


	public BizSystemConfig getBizSystemConfig() {
		return bizSystemConfig;
	}


	public void setBizSystemConfig(BizSystemConfig bizSystemConfig) {
		this.bizSystemConfig = bizSystemConfig;
	}


	public String getOperateType() {
		return operateType;
	}

	public void setOperateType(String operateType) {
		this.operateType = operateType;
	}
	
	
}
