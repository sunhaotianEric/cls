package com.team.lottery.system.message.messagetype;

import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.handler.AlarmInfoMessageHandler;
import com.team.lottery.vo.AlarmInfo;

public class AlarmInfoMessage extends BaseMessage {


	/**
	 * 
	 */
	private static final long serialVersionUID = -8208413082642481317L;

	private AlarmInfo alarmInfo;
	
	public AlarmInfo getAlarmInfo() {
		return alarmInfo;
	}

	public void setAlarmInfo(AlarmInfo alarmInfo) {
		this.alarmInfo = alarmInfo;
	}
    
	public AlarmInfoMessage() {
		this.handler=new AlarmInfoMessageHandler();
	}

	
}
