package com.team.lottery.system.message.messagetype;

import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.handler.BizSystemMessageHandler;
import com.team.lottery.vo.BizSystem;

public class BizSystemMessage extends BaseMessage {
     /**
	 * 
	 */
	private static final long serialVersionUID = -4431362405255633401L;

	/**
      * 业务系统对象
      */
	private BizSystem bizSystem;
	
	/**
	 * 0=修改，1=增加，-1=删除
	 */
	private String operateType;
    
	public BizSystemMessage() {
		this.handler=new BizSystemMessageHandler();
	}
	public BizSystem getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(BizSystem bizSystem) {
		this.bizSystem = bizSystem;
	}

	public String getOperateType() {
		return operateType;
	}

	public void setOperateType(String operateType) {
		this.operateType = operateType;
	}
	
	
}
