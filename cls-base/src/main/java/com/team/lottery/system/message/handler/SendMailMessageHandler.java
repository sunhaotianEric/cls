package com.team.lottery.system.message.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.BaseMessageHandler;
import com.team.lottery.system.message.messagetype.SendMailMessage;
import com.team.lottery.util.mail.MailSendUtil;

public class SendMailMessageHandler extends BaseMessageHandler {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3223187785229599852L;
	private static Logger logger = LoggerFactory.getLogger(SendMailMessageHandler.class);

	
	@Override
	public void dealMessage(BaseMessage message) {
		try {
			SendMailMessage sendMailMessage = (SendMailMessage)message;
			boolean sendSuccess = false;
			//使用所有的邮件发送配置轮询发送邮件
			for(int index : MailSendUtil.mailArrs) {
				//发送成功则不再继续发送
				if(sendSuccess) {
					break;
				}
				try {
					MailSendUtil.sendMailByIndex(sendMailMessage.getSubject(), sendMailMessage.getContent(), index);
					sendSuccess = true;
				} catch(Exception e) {
					sendSuccess = false;
					logger.error(e.getMessage());
				}
			}
		} catch (Exception e) {
			logger.error("处理发送邮件消息发生错误...", e);
		}
	}

}
