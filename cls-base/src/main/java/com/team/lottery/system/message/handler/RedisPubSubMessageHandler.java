package com.team.lottery.system.message.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.Jedis;

import com.team.lottery.redis.JedisUtils;
import com.team.lottery.redis.mq.Publisher;
import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.BaseMessageHandler;
import com.team.lottery.system.message.messagetype.RedisPubSubMessage;

public class RedisPubSubMessageHandler extends BaseMessageHandler {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6298695215656556877L;

	private static Logger logger = LoggerFactory.getLogger(RedisPubSubMessageHandler.class);
	
	@Override
	public  void  dealMessage(BaseMessage message) {
		RedisPubSubMessage redisPubSubMessage = null;
		BaseMessage baseMessage = null;
		Jedis redisClient = JedisUtils.getResource();
		try {
			redisPubSubMessage=(RedisPubSubMessage)message;
			baseMessage=redisPubSubMessage.getBaseMessage();
			logger.debug("开始发布消息...");
			//发布操作
			if("pub".equals(redisPubSubMessage.getOperateType())){
				Publisher.publish(redisClient, redisPubSubMessage.getChannel(), baseMessage);
			}
			logger.debug("发布消息成功...");
		} catch (Exception e) {
			logger.error("RedisPubSubMessageHandler 消息发布发生异常", e);
		} finally {
			logger.debug("回收reids连接...");
			JedisUtils.returnResource(redisClient);
		}
	}


}
