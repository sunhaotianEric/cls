package com.team.lottery.system.message.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.BaseMessageHandler;
import com.team.lottery.system.message.messagetype.BizSystemDomainMessage;

public class BizSystemDomainMessageHandler extends BaseMessageHandler{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6793561688276859864L;
	private static Logger logger = LoggerFactory.getLogger(BizSystemDomainMessageHandler.class);
	
	
	@Override
	public void dealMessage(BaseMessage message) {
		try {
			logger.info("接受redis订阅消息，开始更新BizSystemDomain的ehcache缓存...");
			BizSystemDomainMessage bizSystemDomainMessage=(BizSystemDomainMessage) message;
			if("2".equals(bizSystemDomainMessage.getType())){
				logger.info("开始初始化域名管理缓存...");
				ClsCacheManager.initBizSystemDomainCache();
			}else{
				ClsCacheManager.updateBizSystemDomainCache(bizSystemDomainMessage.getBizSystemDomain(), bizSystemDomainMessage.getOperateType());
			}
		} catch (Exception e) {
			logger.error("处理BizSystemDomain的ehcache缓存发生错误...", e);
		}
	}

}
