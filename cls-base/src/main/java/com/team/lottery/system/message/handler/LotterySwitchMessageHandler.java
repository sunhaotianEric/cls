package com.team.lottery.system.message.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.BaseMessageHandler;
import com.team.lottery.system.message.messagetype.LotterySwitchMessage;

public class LotterySwitchMessageHandler extends BaseMessageHandler{

	private static final long serialVersionUID = -3708647274274158493L;
	private static Logger logger = LoggerFactory.getLogger(LotterySwitchMessageHandler.class);
	
	
	@Override
	public void dealMessage(BaseMessage message) {
		try {
			logger.info("接受redis订阅消息，开始更新LotterySwitch的ehcache缓存...");
			LotterySwitchMessage lotterySwitchMessage=(LotterySwitchMessage) message;
			if("2".equals(lotterySwitchMessage.getType())){
				logger.info("开始初始化彩种开关缓存...");
				ClsCacheManager.initLotterySwitchCache();
			}else{
				ClsCacheManager.updateLotterySwitchCache(lotterySwitchMessage.getBizSystem());
			}
		} catch (Exception e) {
			logger.error("处理LotterySwitch的ehcache缓存发生错误...", e);
		}
	}

}
