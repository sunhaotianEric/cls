package com.team.lottery.system.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.team.lottery.enums.ERedisChannel;
import com.team.lottery.redis.mq.RedisThread;

/**
 * 检查订阅
 * 
 * @author Owner
 *
 */
@Component("checkSubscriptionJob")
public class CheckSubscriptionJob {
	private static Logger log = LoggerFactory.getLogger(CheckSubscriptionJob.class);

	public void execute() {
		log.info("进入检查订阅线程的job.....................");
		if (RedisThread.isSubscribeThreadRun == 1) {
			log.info("当前订阅通道[{}],订阅线程正常,不需要重新订阅!", ERedisChannel.CLSCACHE.name());
		} else {
			log.info("前订阅通道[{}],订阅线程断开，重新启动订阅线程!", ERedisChannel.CLSCACHE.name());
			//启动订阅线程
			new Thread(new RedisThread(ERedisChannel.CLSCACHE.name())).start();
		}
		log.info("检查订阅线程的job结束.....................");
	}
}
