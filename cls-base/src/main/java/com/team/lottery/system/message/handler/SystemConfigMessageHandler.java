package com.team.lottery.system.message.handler;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.service.SystemConfigService;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.BaseMessageHandler;
import com.team.lottery.system.message.messagetype.SystemConfigMessage;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.util.BeanUtilsExtends;
import com.team.lottery.vo.LotterySet;
import com.team.lottery.vo.SystemConfig;

public class SystemConfigMessageHandler extends BaseMessageHandler implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6560943204227426245L;
	private static Logger logger = LoggerFactory.getLogger(SystemConfigMessageHandler.class);
	
	
	@Override
	public void dealMessage(BaseMessage message) {
		try {
			logger.info("接受redis订阅消息，开始更新系统配置参数常量缓存...");
			SystemConfigMessage bizSystemMessage=(SystemConfigMessage) message;
			SystemConfig systemConfig=bizSystemMessage.getSystemConfig();
			Map<String, String> map = new HashMap<String, String>();
			if(systemConfig == null || systemConfig.getConfigKey() == null) {
				SystemConfigService systemConfigService = ApplicationContextUtil.getBean(SystemConfigService.class);
				List<SystemConfig> list = systemConfigService.getAllSystemConfig();
				for (SystemConfig sysConfig : list) {
					map.put(sysConfig.getConfigKey(), sysConfig.getConfigValue());
				}
			} else {
				map.put(systemConfig.getConfigKey(), systemConfig.getConfigValue());
			}
			
			SystemConfigConstant systemConfigConstant=new SystemConfigConstant();
			LotterySet lotterySet=new LotterySet();
			try {
				BeanUtilsExtends.populate(systemConfigConstant, map);
				BeanUtilsExtends.populate(lotterySet, map);
			} catch (IllegalAccessException e) {
				logger.error("加载系统配置参数缓存异常", e);
			} catch (InvocationTargetException e) {
				logger.error("加载系统配置参数缓存异常", e);
			}
			logger.info("更新系统配置参数常量缓存成功...");
		} catch (Exception e) {
			logger.error("处理更新系统配置参数常量缓存发生错误...", e);
		}
	}

}
