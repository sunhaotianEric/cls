package com.team.lottery.system.message.handler;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.BaseMessageHandler;
import com.team.lottery.system.message.messagetype.BizSystemMessage;

public class BizSystemMessageHandler extends BaseMessageHandler implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6560943204227426245L;
	private static Logger logger = LoggerFactory.getLogger(BizSystemMessageHandler.class);
	
	
	@Override
	public void dealMessage(BaseMessage message) {
		try {
			logger.info("接受redis订阅消息，开始更新BizSystem的ehcache缓存...");
			BizSystemMessage bizSystemMessage=(BizSystemMessage) message;
			ClsCacheManager.updateBizSystemCache(bizSystemMessage.getBizSystem(), bizSystemMessage.getOperateType());
			//刷新奖金模式缓存
			ClsCacheManager.initAwardModelConfigCache();
		} catch (Exception e) {
			logger.error("处理BizSystem的ehcache缓存发生错误...", e);
		}
	}

}
