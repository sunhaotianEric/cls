package com.team.lottery.system.message.handler;

import java.io.Serializable;
import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.enums.EAlarmInfo;
import com.team.lottery.service.AlarmInfoService;
import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.BaseMessageHandler;
import com.team.lottery.system.message.messagetype.AlarmInfoMessage;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.util.mail.EmailSender;
import com.team.lottery.util.sms.SmsSender;
import com.team.lottery.vo.AlarmInfo;

public class AlarmInfoMessageHandler extends BaseMessageHandler implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6560943204227426245L;
	private static Logger logger = LoggerFactory.getLogger(AlarmInfoMessageHandler.class);
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@Override
	public void dealMessage(BaseMessage message) {
		try {
			logger.info("接受redis订阅消息，开始更新插入监控信息...");
			AlarmInfoMessage alarmInfoMessage = (AlarmInfoMessage) message;
			AlarmInfo alarmInfo = alarmInfoMessage.getAlarmInfo();
			AlarmInfoService alarmInfoService = ApplicationContextUtil.getBean("alarmInfoService");
			String alarmContent = "用户："+alarmInfo.getUserName()+" "+EAlarmInfo.valueOf(alarmInfo.getAlarmType()).getDescription()+" ￥"+alarmInfo.getMoney() +" "+sdf.format(alarmInfo.getCreateDate());
			alarmInfo.setAlarmContent(alarmContent);
			alarmInfoService.insertSelective(alarmInfo);
			//发送邮件
			if(alarmInfo.getEmailSendOpen() == 1){
				alarmContent = alarmInfo.getUserName() +" "+EAlarmInfo.valueOf(alarmInfo.getAlarmType()).getCode() +" "+alarmInfo.getMoney() + " "+sdf.format(alarmInfo.getCreateDate());
				Boolean result =EmailSender.sendMail("gj", alarmContent, alarmInfo.getAlarmEmail());
				logger.info("邮件"+alarmContent+"发送结果："+result);
			}
			//发送信息
            if(alarmInfo.getSmsSendOpen() == 1){
            	alarmContent = alarmInfo.getUserName() +" "+EAlarmInfo.valueOf(alarmInfo.getAlarmType()).getCode() +" "+alarmInfo.getMoney() + " "+sdf.format(alarmInfo.getCreateDate());
            	Boolean result = SmsSender.sendPhone(alarmContent, alarmInfo.getAlarmPhone());
			    logger.info("短信"+alarmContent+"发送结果："+result);
            }
		} catch (Exception e) {
			logger.error("处理插入监控信息发生错误...", e);
		}
	}

}
