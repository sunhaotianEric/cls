package com.team.lottery.system;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.enums.ERedisChannel;
import com.team.lottery.redis.JedisUtils;
import com.team.lottery.system.message.MessageQueueCenter;
import com.team.lottery.vo.BizSystem;
import com.team.lottery.vo.Login;
import com.team.lottery.vo.User;

import redis.clients.jedis.Jedis;

/**
 * 单点登录监听
 * @author chenhsh
 *
 */
public class SingleLoginForRedis {

	//在线状态
	public static final Integer NO_ONLINE = 0;
	public static final Integer PC_ONLINE = 1;
	public static final Integer MOBILE_ONLINE = 2;
	public static final Integer APP_ONLINE = 2;
		
	private static Logger log = LoggerFactory.getLogger(SingleLoginForRedis.class);
    
    public static void sAddRedisSet(String key,String members){
    	Jedis jedis = null; 
    	  try {
          	jedis = JedisUtils.getResource();
          	jedis.sadd(key, members);
    	  } catch (Exception e) {
          	log.error("向redis写入数据失败", e);
      		JedisUtils.returnBrokenResource(jedis);
      	} finally{
      		JedisUtils.returnResource(jedis);
      	}
    }
    
    public static Boolean isExistRedisSet(String key,String members){
    	Boolean flag=false;
    	Jedis jedis = null; 
    	  try {
          	Set<String>  sets= JedisUtils.getSet(key);
          	if(sets==null){
          		return false;
          	}
          	for(String strSet:sets){
          		if(strSet!=null){
          			if(strSet.contains("&"+members)){
          				flag=true;
          				return flag;
          			}
          		}
          	}

    	  } catch (Exception e) {
          	log.error("向redis操作数据失败", e);
      		JedisUtils.returnBrokenResource(jedis);
      	} finally{
      		JedisUtils.returnResource(jedis);
      	}
    	  
    	  return flag;
    }
    
    public static Long sRemoveRedisSet(String key,String members){
    	Jedis jedis = null; 
    	Long result=0l;
        try {
          	jedis = JedisUtils.getResource();
          	result=jedis.srem(key, members);
    	} catch (Exception e) {
          	log.error("向redis操作数据失败", e);
      		JedisUtils.returnBrokenResource(jedis);
      	} finally{
      		JedisUtils.returnResource(jedis);
      	}
    	  return result;
    }
    
    public static Set<String> getRedisSets(String key){
    	 Set<String> sets=null;
          sets=JedisUtils.getSet(key);
          if(sets==null){
        	  sets =new HashSet<String>();  
          }
         
          return sets;
    }
    
    /**
     * 根据key来获取用户名的列表，这里对获取到的value进行处理，只获取用户名
     * 存储结构 sessionId&用户名
     * @param key
     * @return
     */
    public static Set<String> getRedisSetsOnlyNames(String key){
    	Set<String> sets=null;
    	Set<String> nameSets=new HashSet<String>();
    	sets=JedisUtils.getSet(key);
    	if(sets!=null){
		    for(String strSet:sets){
		    	nameSets.add(strSet.split("&")[1]);
		    }
    	}
    	return nameSets;
   }
    
    
    /** 
     * isOnline-用于判断用户是否在线 
     *  
     * @param session 
     *            HttpSession-登录的用户名称 
     * @return boolean-该用户是否在线的标志 
     */  
    public static boolean isOnline(String key,String sUserName) {
        boolean flag = true; 
        if(key == null || sUserName == null || "".equals(sUserName)){
        	return false ;
        }
        flag=isExistRedisSet(key,sUserName);
        return flag;  
    }  
    

    /**
     * 根据类型计算所有业务系统人数
     * loginType 手机，pc
     * @param list
     */
    public static Integer getAllLoginUsers(List<BizSystem> list,String loginType){
    	Set<String> countSets=new HashSet<String>();
    	
    	if(list!=null){
    		for(BizSystem bizSystem:list){
    			Set<String> sets=getRedisSets(loginType+"."+bizSystem.getBizSystem());
    			for(String setStr:sets){
    				if(setStr!=null){
    					countSets.add(setStr.split("&")[1]);	
    				}
    				
    			}
    		}
    	}
    	return countSets.size();
    }  
    
    
    /**
     * 返回前台登录的用户列表
     * @return
     */
    public static List<String> getFrontLoginUserNams(String key) {
    	List<String> userNames = new ArrayList<String>();
    	Set<String> sets= JedisUtils.getSet(key);
    	if(sets==null){
    		return userNames;
    	}
    	for(String value:sets) {
    		if(value!=null){
    			userNames.add(value.split("&")[1]);	
			}
    		
    	}
    	return userNames;
    }
    
    /**
     * 返回前台登录的手机用户列表
     * @return
     */
    public static List<String> getFrontMobileLoginUserNams(String key) {
    	List<String> userNames = new ArrayList<String>();
    	Set<String>  sets= JedisUtils.getSet(key); 
    	if(sets==null){
    		return userNames;
    	}
    	for(String value:sets) {
    		if(value!=null){
    			userNames.add(value.split("&")[1]);	
			}
    	}
    	return userNames;
    }
    
    /**
	 * 根据用户名获取用户在线状态
	 * @param userName
	 * @return
	 */
	public static Integer getUserOnlineStatuByName(String bizSystem,String userName, String sessionId) {
		Integer onlineStatus = SingleLoginForRedis.NO_ONLINE;
		Boolean flag=false;
		List<String> userNames = SingleLoginForRedis.getFrontLoginUserNams(Login.LOGTYPE_PC+"."+bizSystem);
		for(String onlineUserName : userNames) {
			if(onlineUserName.equals(userName)) {
				flag=true;
			}
		}
		if(!flag){
			userNames = SingleLoginForRedis.getFrontLoginUserNams(Login.LOGTYPE_MOBILE+"."+bizSystem);
			for(String onlineUserName : userNames) {
				if(onlineUserName.equals(userName)) {
					flag=true;
				}
			}
		}
		
		if(!flag){
			userNames = SingleLoginForRedis.getFrontLoginUserNams(Login.LOGTYPE_APP+"."+bizSystem);
			for(String onlineUserName : userNames) {
				if(onlineUserName.equals(userName)) {
					flag=true;
				}
			}
		}
		if(flag){
			if(SystemPropeties.loginType.equals(Login.LOGTYPE_PC)){
				onlineStatus = SingleLoginForRedis.PC_ONLINE;
			}else if(SystemPropeties.loginType.equals(Login.LOGTYPE_MOBILE)){
				onlineStatus = SingleLoginForRedis.MOBILE_ONLINE;
			}
		}
		return onlineStatus;
	}
	
	/**
	 * 根据用户列表设置用户的在线状态
	 * @param users
	 */
	public static void setUsersOnlineStatus(List<User> users) {
		if(CollectionUtils.isNotEmpty(users)) {
		//	List<String> userNames = getFrontLoginUserNames();
			
			for(User user : users) {
				List<String> userNames = SingleLoginForRedis.getFrontLoginUserNams(Login.LOGTYPE_PC+"."+user.getBizSystem());
				for(String onlineUserName : userNames) {
					if(onlineUserName.equals(user.getUserName())) {
						user.setIsOnline(1);
					}
				}
				if(user.getIsOnline()==null){
					userNames = SingleLoginForRedis.getFrontLoginUserNams(Login.LOGTYPE_MOBILE+"."+user.getBizSystem());
					for(String onlineUserName : userNames) {
						if(onlineUserName.equals(user.getUserName())) {
							user.setIsOnline(1);
						}
					}
				}
				if(user.getIsOnline()==null){
					userNames = SingleLoginForRedis.getFrontLoginUserNams(Login.LOGTYPE_APP+"."+user.getBizSystem());
					for(String onlineUserName : userNames) {
						if(onlineUserName.equals(user.getUserName())) {
							user.setIsOnline(1);
						}
					}
				}
			}
		}
	}



}
