package com.team.lottery.system;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.mapper.systemconfig.SystemConfigMapper;
import com.team.lottery.redis.JedisUtils;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.util.BeanUtilsExtends;
import com.team.lottery.vo.LotterySet;
import com.team.lottery.vo.SystemConfig;

public class SystemCache {

	private static Logger logger = LoggerFactory.getLogger(SystemCache.class);
	
	private static SystemConfigMapper systemConfigMapper; 
	
	/**
	 * 加载系统所有缓存配置项
	 * @throws Exception 
	 */
	public static void loadAllCacheData(){
		loadLotteryConfigCacheData(); //加载彩种开关数据
		loadSystemConfigCacheData();//加载网站系统参数配置
		ClsCacheManager.initCache();
	}
	
	/**
	 * 初始化域名管理缓存
	 */
	public static void refreshBizSystemDomainCache() {
		ClsCacheManager.initBizSystemDomainCache();
	}

	/**
	 * 加载彩种开关数据
	 */
	public static void loadLotteryConfigCacheData() {
		Map<String,String> systemMap=new HashMap<String, String>();
    	List<SystemConfig> systemList=getSystemConfigMapper().getSystemConfigByLikeValue("is%Open");
    	for(SystemConfig systemConfig:systemList){
    		systemMap.put(systemConfig.getConfigKey(), systemConfig.getConfigValue());
    	}
    	if(systemMap.size()==0){
     		throw new RuntimeException("当前不存在系统配置信息的记录.");
        }
    	LotterySet lotterySet=new LotterySet();
    	try {
			BeanUtils.populate(lotterySet, systemMap);
			loadBizSystemConfigCacheData();
		} catch (IllegalAccessException e) {
			logger.error("加载彩种开关参数配置缓存异常", e);
		} catch (InvocationTargetException e) {
			logger.error("加载彩种开关参数配置缓存异常", e);
		}
	}
	
	
	/**
	 * 加载所有业务系统参数数据
	 */
	public static void loadBizSystemConfigCacheData() {
		
    	try {
			BizSystemConfigManager.refreshAllBizSystemConfig();
		} catch (IllegalAccessException e) {
			logger.error("加载所有业务系统参数数据缓存异常", e);
		} catch (InvocationTargetException e) {
			logger.error("加载所有业务系统参数数据缓存异常", e);
		}
	}
	
	/**
	 * 加载网站系统参数配置缓存，从数据库读取
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 */
	public static void loadSystemConfigCacheData() {
		List<SystemConfig> list = getSystemConfigMapper().getAllSystemConfig();
		Map<String, String> map = new HashMap<String, String>();
		for (SystemConfig systemConfig : list) {
			map.put(systemConfig.getConfigKey(), systemConfig.getConfigValue());
		}
		SystemConfigConstant systemConfigConstant = new SystemConfigConstant();
		try {
			BeanUtilsExtends.populate(systemConfigConstant, map);
		} catch (IllegalAccessException e) {
			logger.error("加载网站系统参数配置缓存异常", e);
		} catch (InvocationTargetException e) {
			logger.error("加载网站系统参数配置缓存异常", e);
		}
	}

	public static SystemConfigMapper getSystemConfigMapper() {
		if(systemConfigMapper == null) {
			systemConfigMapper = ApplicationContextUtil.getBean(SystemConfigMapper.class);
		}
		return systemConfigMapper;
	}

	
	
}
