package com.team.lottery.system.message;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 消息队列处理线程
 * @author luocheng
 *
 */
public class MessageQueueThread extends Thread {
	
	private static Logger logger = LoggerFactory.getLogger(MessageQueueThread.class);
	private boolean isRun = true;

	@Override
	public void run() {
		logger.info("启动消息队列处理线程...");
		while(isRun) {
			ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
			final BaseMessage message = MessageQueueCenter.takeMessage();
			if(message != null) {
				//获取到消息之后  启动新线程进行处理防止堵塞获取消息线程
				cachedThreadPool.execute(new Runnable() {
					
					@Override
					public void run() {
						
						//根据消息类型获取对应的处理器
						BaseMessageHandler handler = message.getMessageHandler();
						if(handler != null) {
							handler.dealMessage(message);
						}
					}
				});
			}
		}
	}
}
