package com.team.lottery.system.message.messagetype;

import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.handler.BizSystemInfoMessageHandler;
import com.team.lottery.vo.BizSystemInfo;

public class BizSystemInfoMessage extends BaseMessage {
     /**
	 * 
	 */
	private static final long serialVersionUID = -2472978487148655031L;

	/**
      * 业务系统对象
      */
	private BizSystemInfo bizSystemInfo;
	
	/**
	 * 0=修改，1=增加，-1=删除
	 */
	private String operateType;
	
    public BizSystemInfoMessage() {
		this.handler=new BizSystemInfoMessageHandler();
	}

	public BizSystemInfo getBizSystemInfo() {
		return bizSystemInfo;
	}

	public void setBizSystemInfo(BizSystemInfo bizSystemInfo) {
		this.bizSystemInfo = bizSystemInfo;
	}

	public String getOperateType() {
		return operateType;
	}

	public void setOperateType(String operateType) {
		this.operateType = operateType;
	}
	
	
}
