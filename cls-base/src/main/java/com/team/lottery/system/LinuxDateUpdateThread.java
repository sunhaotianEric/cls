package com.team.lottery.system;

import java.io.InputStreamReader;
import java.io.LineNumberReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author luocheng
 * @deprecated 改用运维方式同步时间
 */
public class LinuxDateUpdateThread implements Runnable{

	private static Logger log = LoggerFactory.getLogger(LinuxDateUpdateThread.class);

	public void run() {
		while (true) {
		     try {  
		    	    String[] cmdA = { "/bin/sh", "-c","ntpdate time.nist.gov"};   
		    	    log.info("开始同步服务器时间...");
	                Process process = Runtime.getRuntime().exec(cmdA);  
	                LineNumberReader br = new LineNumberReader(new InputStreamReader(  
	                        process.getInputStream()));  
	                StringBuffer sb = new StringBuffer();  
	                String line;  
	                while ((line = br.readLine()) != null) {  
	                    sb.append(line).append("\n");  
	                }  
	                log.info("命令执行结果:" + sb.toString());
	                Thread.sleep(6 * 60 * 60 * 1000);
	            } catch (Exception e) {  
	                e.printStackTrace();  
	                log.info("更新时间失败");
					try {
						Thread.sleep(3 * 60 * 60 * 1000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}  //休眠10秒钟
	            }  
		}
	}
}
