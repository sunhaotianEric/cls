package com.team.lottery.system;

import java.math.BigDecimal;

/**
 * 
 * @author lxh
 * 业务系统配置VO类
 */
public class BizSystemConfigVO {
	
    /**
     * 直属代理人数配置
     */
	private Integer directagencyNum;
	
	/**
     * 总代理人数配置
     */
	private Integer genralagencyNum;
	
	/**
     * 普通代理人数配置
     */
	private Integer ordinayagencyNum;
	
	 /**
     * 设置最低模式
     */
	private  BigDecimal canlotteryhightModel;
	
	/**
	 * 彩种
	 *
	 */
	//老重庆时时彩是否开启
	private Integer isLCQSSCOpen;
	//江苏时时彩是否开启
	private Integer isJSSSCOpen;
	//北京时时彩是否开启
	private Integer isBJSSCOpen;
	//广东时时彩是否开启
	private Integer isGDSSCOpen;
	//四川时时彩是否开启
	private Integer isSCSSCOpen;
	//上海时时彩是否开启
	private Integer isSHSSCOpen;
	//山东时时彩是否开启
	private Integer isSDSSCOpen;

	//十分时时彩是否开启
	private Integer isSHFSSCOpen;	
	//三分时时彩是否开启
	private Integer isSFSSCOpen;	
	//五分时时彩是否开启
	private Integer isWFSSCOpen;
	//重庆时时彩是否开启
	private Integer isCQSSCOpen;	
	//江西时时彩是否开启
	private Integer isJXSSCOpen;	
	//黑龙江时时彩是否开启
	private Integer isHLJSSCOpen;	
	//新疆时时彩是否开启
	private Integer isXJSSCOpen;	
	//天津时时彩是否开启
	private Integer isTJSSCOpen;	
	
	//吉利分分彩是否开启
	private Integer isJLFFCOpen;
	//韩国1.5分彩是否开启
	private Integer isHGFFCOpen;
	
	//新加坡2分彩是否开启
	private Integer isXJPLFCOpen;
	//台湾5分彩是否开启
	private Integer isTWWFCOpen;
	
	//广东11选5是否开启
	private Integer isGDSYXWOpen;
	//山东11选5是否开启
	private Integer isSDSYXWOpen;
	//江西11选5是否开启
	private Integer isJXSYXWOpen;
	//重庆11选5是否开启
	private Integer isCQSYXWOpen;
	//福建11选5是否开启
	private Integer isFJSYXWOpen;
	//五分11选5是否开启
	private Integer isWFSYXWOpen;
	//三分11选5是否开启
	private Integer isSFSYXWOpen;
	
	//三分快3是否开启
	private Integer isSFKSOpen;
	//五分快3是否开启
	private Integer isWFKSOpen;
	//江苏快3是否开启
	private Integer isJSKSOpen;
	//安徽快3是否开启
	private Integer isAHKSOpen;
	//吉林快3是否开启
	private Integer isJLKSOpen;
	//湖北快3是否开启
	private Integer isHBKSOpen;
	//北京快3是否开启
	private Integer isBJKSOpen;
	//幸运快3是否开启
	private Integer isJYKSOpen;
	//广西快3是否开启
	private Integer isGXKSOpen;
	// 甘肃快3是否开启
	public Integer isGSKSOpen;
	// 上海快3是否开启
	public Integer isSHKSOpen;
	
	//广东快乐十分是否开启
	private Integer isGDKLSFOpen;		
	
    //上海时时乐是否开启
	private Integer isSHSSLDPCOpen;
	//福彩3D是否开启 
	private Integer isFC3DDPCOpen;
	//六合彩是否开启 
	private Integer isLHCOpen;
	//幸运六合彩是否开启 
    private Integer isXYLHCOpen;
	//幸运飞艇是否开启
	private Integer isXYFTOpen;
	//三分K10是否开启
	private Integer isSFPK10Open;
	//五分PK10是否开启
	private Integer isWFPK10Open;
	//十分PK10是否开启
	private Integer isSHFPK10Open;
	//北京PK10是否开启
	private Integer isBJPK10Open;
	//极速PK10是否开启
	private Integer isJSPK10Open;
	
	//江苏骰宝是否开启
	private Integer isJSTBOpen;
	//安徽骰宝是否开启
	private Integer isAHTBOpen;
	//吉林骰宝是否开启
	private Integer isJLTBOpen;
	//湖北骰宝是否开启
	private Integer isHBTBOpen;
	//北京骰宝是否开启
	private Integer isBJTBOpen;
	//幸运28是否开启
	private Integer isXYEBOpen;
	
	//可提现余额不足是否允许提现
	private Integer withDrawIsAllowNotEnough;
	//手续费
	private BigDecimal exceedWithdrawExpenses;
	//单笔最低提现限额
	private BigDecimal withDrawOneStrokeLowestMoney;
	//单笔最高提现限额
	private BigDecimal withDrawOneStrokeHighestMoney;
	//每天取现总金额
	private BigDecimal withDrawEveryDayTotalMoney;
	//取现次数
	private Integer withDrawEveryDayNumber;
	//每日转账限额
	private BigDecimal exceedTransferDayLimitExpenses;
	//是否启用上下级转账开关
	private Integer isTransferSwich;
	//进入可提现余额投注额倍数
	private BigDecimal withDrawLotteryMultiple;
	//第三方充值订单要求打码量倍数
	private Integer thirdPayOrderBettingMultiple;
	
	//契约分红发放策略
	private String releasePolicyBonus;
	
	//平台系统维护开关
	private Integer maintainSwich;
	//平台系统维护说明
	private String maintainContent;
	//日工资发放策略
	private String releasePolicyDaySaraly;
	
	//系统秘密验证码
	private String secretCode;
	//系统默认邀请码
	private String defaultInvitationCode;
	
	//六合是否封盘
	private Integer isClose;
	//六合彩单注最低限额
	private BigDecimal lhcLowerLotteryMoney;
	//六合彩单注最高限额
	private BigDecimal lhcHighestLotteryMoney;
	//六合彩单场最高投注额
	private BigDecimal lhcHighestExpectLotteryMoney;
	//六合彩单注最高返奖限额
	private BigDecimal lhcMaxWinMoney;
	
	//监控接收短信手机号码
	private String alarmPhone;
	//监控接收邮件地址
	private String alarmEmail;
	
	//手机告警开关
	private Integer alarmPhoneOpen;
	//邮件告警开关
	private Integer alarmEmailOpen;
	
	//投注监控开关
	private Integer lotteryAlarmOpen;
	//投注监控金额
	private BigDecimal lotteryAlarmValue;
	
	//中奖监控开关
	private Integer winAlarmOpen;
	//中奖监控金额
	private BigDecimal winAlarmValue;
	
	//充值监控开关
	private Integer rechargeAlarmOpen;
	//充值监控金额
	private BigDecimal rechargeAlarmValue;
	
	//取现监控开关
	private Integer withdrawAlarmOpen;
	//取现监控金额
	private BigDecimal withdrawAlarmValue;

	//注册是否显示手机号码
	private Integer regShowPhone;
	//注册手机号码是否必填
	private Integer regRequiredPhone;
	//是否开启短信验证
	private Integer regShowSms;
	//注册是否显示邮件
	private Integer regShowEmail;
	//注册邮件是否必填
	private Integer regRequiredEmail;
	//注册是否显示QQ
	private Integer regShowQq;
	//注册QQ是否必填
	private Integer regRequiredQq;
	//开放注册
	private Integer regOpen;
	//是否开启试玩功能(默认0)
	private Integer guestModel;
	//是否允许游客登录
	private Integer allowGuestLogin;
	//游客登陆默认金钱
	private BigDecimal guestMoney;
	//等级机制开启关闭（0关闭1开启）
	private Integer isOpenLevelAward;
	//晋级奖励自动领取开启关闭（0关闭1开启）
	private Integer automaticPromotionReceiveSwitch;
	//签到活动开启关闭（0关闭1开启）
	private Integer isOpenSignInActivity;
	//签到要求充值金额
	private BigDecimal signinRechargeMoney;
	//每天签到最后时间
	private String signinLastTime;
	//是否开启手机登陆
	private Integer isOpenPhone;
	//是否开启邮箱登陆
	private Integer isOpenEmail;
	//是否开启银行卡同名绑卡
	private Integer isNoSameNameBindBankCard;
	//是否注册完登录
	private Integer regDirectLogin;
	//彩票计划开关
	private Integer codePlanSwitch;
	
	//新手礼包功能开关
	private Integer isNewUserGiftOpen;
	//下载APP赠送金额
	private BigDecimal appDownGiftMoney;
	//完善个人资料赠送金额(填写真实姓名，并绑定银行卡)
	private BigDecimal completeInfoGiftMoney;
	//完善安全资料赠送金额(完善安全问题、安全手机、安全邮箱中的2个)
	private BigDecimal complateSafeGiftMoney;
	//首笔投注赠送金额
	private BigDecimal lotteryGiftMoney;
	//首笔充值赠送金额
	private BigDecimal rechargeGiftMoney;
	//发展新玩家赠送金额（新玩家必须是完善资料的用户）
	private BigDecimal extendUserGiftMoney;
	// 流水返送领取开始时间
	private String dayConsumeOrderTimeStart;
	// 流水返送领取结束时间
	private String dayConsumeOrderTimeEnd;
	// 注册是否展示真实姓名.
	private Integer regShowTrueName;
	// 注册是否必填真实姓名.
	private Integer regRequiredTrueName;

	public String getDayConsumeOrderTimeStart() {
		return dayConsumeOrderTimeStart;
	}

	public void setDayConsumeOrderTimeStart(String dayConsumeOrderTimeStart) {
		this.dayConsumeOrderTimeStart = dayConsumeOrderTimeStart;
	}

	public String getDayConsumeOrderTimeEnd() {
		return dayConsumeOrderTimeEnd;
	}

	public void setDayConsumeOrderTimeEnd(String dayConsumeOrderTimeEnd) {
		this.dayConsumeOrderTimeEnd = dayConsumeOrderTimeEnd;
	}

	public Integer getAutomaticPromotionReceiveSwitch() {
		return automaticPromotionReceiveSwitch;
	}

	public void setAutomaticPromotionReceiveSwitch(Integer automaticPromotionReceiveSwitch) {
		this.automaticPromotionReceiveSwitch = automaticPromotionReceiveSwitch;
	}

	public String getSecretCode() {
		return secretCode;
	}

	public Integer getRegOpen() {
		return regOpen;
	}

	public void setRegOpen(Integer regOpen) {
		this.regOpen = regOpen;
	}

	public void setSecretCode(String secretCode) {
		this.secretCode = secretCode;
	}

	public String getDefaultInvitationCode() {
		return defaultInvitationCode;
	}

	public Integer getRegShowSms() {
		return regShowSms;
	}

	public void setRegShowSms(Integer regShowSms) {
		this.regShowSms = regShowSms;
	}

	public void setDefaultInvitationCode(String defaultInvitationCode) {
		this.defaultInvitationCode = defaultInvitationCode;
	}

	public Integer getDirectagencyNum() {
		return directagencyNum;
	}

	public void setDirectagencyNum(Integer directagencyNum) {
		this.directagencyNum = directagencyNum;
	}

	public Integer getGenralagencyNum() {
		return genralagencyNum;
	}

	public void setGenralagencyNum(Integer genralagencyNum) {
		this.genralagencyNum = genralagencyNum;
	}

	public Integer getOrdinayagencyNum() {
		return ordinayagencyNum;
	}

	public Integer getIsOpenPhone() {
		return isOpenPhone;
	}

	public void setIsOpenPhone(Integer isOpenPhone) {
		this.isOpenPhone = isOpenPhone;
	}

	public Integer getIsOpenEmail() {
		return isOpenEmail;
	}

	public void setIsOpenEmail(Integer isOpenEmail) {
		this.isOpenEmail = isOpenEmail;
	}

	public void setOrdinayagencyNum(Integer ordinayagencyNum) {
		this.ordinayagencyNum = ordinayagencyNum;
	}

	public BigDecimal getCanlotteryhightModel() {
		return canlotteryhightModel;
	}

	public void setCanlotteryhightModel(BigDecimal canlotteryhightModel) {
		this.canlotteryhightModel = canlotteryhightModel;
	}

	public Integer getIsCQSSCOpen() {
		return isCQSSCOpen;
	}

	public void setIsCQSSCOpen(Integer isCQSSCOpen) {
		this.isCQSSCOpen = isCQSSCOpen;
	}

	public Integer getIsXYEBOpen() {
		return isXYEBOpen;
	}

	public void setIsXYEBOpen(Integer isXYEBOpen) {
		this.isXYEBOpen = isXYEBOpen;
	}

	public Integer getIsJXSSCOpen() {
		return isJXSSCOpen;
	}

	public void setIsJXSSCOpen(Integer isJXSSCOpen) {
		this.isJXSSCOpen = isJXSSCOpen;
	}

	public Integer getIsHLJSSCOpen() {
		return isHLJSSCOpen;
	}

	public void setIsHLJSSCOpen(Integer isHLJSSCOpen) {
		this.isHLJSSCOpen = isHLJSSCOpen;
	}

	public Integer getIsXJSSCOpen() {
		return isXJSSCOpen;
	}

	public void setIsXJSSCOpen(Integer isXJSSCOpen) {
		this.isXJSSCOpen = isXJSSCOpen;
	}

	public Integer getIsTJSSCOpen() {
		return isTJSSCOpen;
	}

	public void setIsTJSSCOpen(Integer isTJSSCOpen) {
		this.isTJSSCOpen = isTJSSCOpen;
	}

	public Integer getIsJLFFCOpen() {
		return isJLFFCOpen;
	}

	public void setIsJLFFCOpen(Integer isJLFFCOpen) {
		this.isJLFFCOpen = isJLFFCOpen;
	}

	public Integer getIsHGFFCOpen() {
		return isHGFFCOpen;
	}

	public void setIsHGFFCOpen(Integer isHGFFCOpen) {
		this.isHGFFCOpen = isHGFFCOpen;
	}

	public Integer getIsLHCOpen() {
		return isLHCOpen;
	}

	public void setIsLHCOpen(Integer isLHCOpen) {
		this.isLHCOpen = isLHCOpen;
	}

	public Integer getIsXYLHCOpen() {
		return isXYLHCOpen;
	}

	public void setIsXYLHCOpen(Integer isXYLHCOpen) {
		this.isXYLHCOpen = isXYLHCOpen;
	}
	
	public Integer getIsGDSYXWOpen() {
		return isGDSYXWOpen;
	}

	public void setIsGDSYXWOpen(Integer isGDSYXWOpen) {
		this.isGDSYXWOpen = isGDSYXWOpen;
	}

	public Integer getIsSDSYXWOpen() {
		return isSDSYXWOpen;
	}

	public void setIsSDSYXWOpen(Integer isSDSYXWOpen) {
		this.isSDSYXWOpen = isSDSYXWOpen;
	}

	public Integer getIsJXSYXWOpen() {
		return isJXSYXWOpen;
	}

	public void setIsJXSYXWOpen(Integer isJXSYXWOpen) {
		this.isJXSYXWOpen = isJXSYXWOpen;
	}

	public Integer getIsCQSYXWOpen() {
		return isCQSYXWOpen;
	}

	public void setIsCQSYXWOpen(Integer isCQSYXWOpen) {
		this.isCQSYXWOpen = isCQSYXWOpen;
	}

	public Integer getIsFJSYXWOpen() {
		return isFJSYXWOpen;
	}

	public void setIsFJSYXWOpen(Integer isFJSYXWOpen) {
		this.isFJSYXWOpen = isFJSYXWOpen;
	}

	public Integer getIsJSKSOpen() {
		return isJSKSOpen;
	}

	public void setIsJSKSOpen(Integer isJSKSOpen) {
		this.isJSKSOpen = isJSKSOpen;
	}

	public Integer getIsAHKSOpen() {
		return isAHKSOpen;
	}

	public void setIsAHKSOpen(Integer isAHKSOpen) {
		this.isAHKSOpen = isAHKSOpen;
	}

	public Integer getIsJLKSOpen() {
		return isJLKSOpen;
	}

	public void setIsJLKSOpen(Integer isJLKSOpen) {
		this.isJLKSOpen = isJLKSOpen;
	}

	public Integer getIsHBKSOpen() {
		return isHBKSOpen;
	}

	public void setIsHBKSOpen(Integer isHBKSOpen) {
		this.isHBKSOpen = isHBKSOpen;
	}
	public Integer getIsJYKSOpen() {
		return isJYKSOpen;
	}

	public void setIsJYKSOpen(Integer isJYKSOpen) {
		this.isJYKSOpen = isJYKSOpen;
	}

	public Integer getIsGXKSOpen() {
		return isGXKSOpen;
	}

	public void setIsGXKSOpen(Integer isGXKSOpen) {
		this.isGXKSOpen = isGXKSOpen;
	}

	public Integer getIsGSKSOpen() {
		return isGSKSOpen;
	}

	

	public void setIsGSKSOpen(Integer isGSKSOpen) {
		this.isGSKSOpen = isGSKSOpen;
	}

	public Integer getIsSHKSOpen() {
		return isSHKSOpen;
	}

	public void setIsSHKSOpen(Integer isSHKSOpen) {
		this.isSHKSOpen = isSHKSOpen;
	}

	public Integer getIsGDKLSFOpen() {
		return isGDKLSFOpen;
	}

	public void setIsGDKLSFOpen(Integer isGDKLSFOpen) {
		this.isGDKLSFOpen = isGDKLSFOpen;
	}

	public Integer getIsSHSSLDPCOpen() {
		return isSHSSLDPCOpen;
	}

	public void setIsSHSSLDPCOpen(Integer isSHSSLDPCOpen) {
		this.isSHSSLDPCOpen = isSHSSLDPCOpen;
	}

	public Integer getIsFC3DDPCOpen() {
		return isFC3DDPCOpen;
	}

	public void setIsFC3DDPCOpen(Integer isFC3DDPCOpen) {
		this.isFC3DDPCOpen = isFC3DDPCOpen;
	}

	public Integer getIsBJPK10Open() {
		return isBJPK10Open;
	}

	public void setIsBJPK10Open(Integer isBJPK10Open) {
		this.isBJPK10Open = isBJPK10Open;
	}

	public Integer getIsJSPK10Open() {
		return isJSPK10Open;
	}

	public void setIsJSPK10Open(Integer isJSPK10Open) {
		this.isJSPK10Open = isJSPK10Open;
	}

	public Integer getIsJSTBOpen() {
		return isJSTBOpen;
	}

	public void setIsJSTBOpen(Integer isJSTBOpen) {
		this.isJSTBOpen = isJSTBOpen;
	}

	public Integer getIsAHTBOpen() {
		return isAHTBOpen;
	}

	public void setIsAHTBOpen(Integer isAHTBOpen) {
		this.isAHTBOpen = isAHTBOpen;
	}

	public Integer getIsJLTBOpen() {
		return isJLTBOpen;
	}

	public void setIsJLTBOpen(Integer isJLTBOpen) {
		this.isJLTBOpen = isJLTBOpen;
	}

	public Integer getIsHBTBOpen() {
		return isHBTBOpen;
	}

	public void setIsHBTBOpen(Integer isHBTBOpen) {
		this.isHBTBOpen = isHBTBOpen;
	}

	public Integer getIsBJKSOpen() {
		return isBJKSOpen;
	}

	public void setIsBJKSOpen(Integer isBJKSOpen) {
		this.isBJKSOpen = isBJKSOpen;
	}

	public Integer getIsBJTBOpen() {
		return isBJTBOpen;
	}

	public void setIsBJTBOpen(Integer isBJTBOpen) {
		this.isBJTBOpen = isBJTBOpen;
	}
	public Integer getIsXJPLFCOpen() {
		return isXJPLFCOpen;
	}

	public void setIsXJPLFCOpen(Integer isXJPLFCOpen) {
		this.isXJPLFCOpen = isXJPLFCOpen;
	}

	public Integer getIsTWWFCOpen() {
		return isTWWFCOpen;
	}

	public void setIsTWWFCOpen(Integer isTWWFCOpen) {
		this.isTWWFCOpen = isTWWFCOpen;
	}
	

	public Integer getWithDrawIsAllowNotEnough() {
		return withDrawIsAllowNotEnough;
	}

	public void setWithDrawIsAllowNotEnough(Integer withDrawIsAllowNotEnough) {
		this.withDrawIsAllowNotEnough = withDrawIsAllowNotEnough;
	}

	public BigDecimal getExceedWithdrawExpenses() {
		return exceedWithdrawExpenses;
	}

	public void setExceedWithdrawExpenses(BigDecimal exceedWithdrawExpenses) {
		this.exceedWithdrawExpenses = exceedWithdrawExpenses;
	}

	public BigDecimal getWithDrawOneStrokeLowestMoney() {
		return withDrawOneStrokeLowestMoney;
	}

	public void setWithDrawOneStrokeLowestMoney(
			BigDecimal withDrawOneStrokeLowestMoney) {
		this.withDrawOneStrokeLowestMoney = withDrawOneStrokeLowestMoney;
	}

	public BigDecimal getWithDrawOneStrokeHighestMoney() {
		return withDrawOneStrokeHighestMoney;
	}

	public void setWithDrawOneStrokeHighestMoney(
			BigDecimal withDrawOneStrokeHighestMoney) {
		this.withDrawOneStrokeHighestMoney = withDrawOneStrokeHighestMoney;
	}

	public BigDecimal getWithDrawEveryDayTotalMoney() {
		return withDrawEveryDayTotalMoney;
	}

	public void setWithDrawEveryDayTotalMoney(BigDecimal withDrawEveryDayTotalMoney) {
		this.withDrawEveryDayTotalMoney = withDrawEveryDayTotalMoney;
	}

	public Integer getWithDrawEveryDayNumber() {
		return withDrawEveryDayNumber;
	}

	public void setWithDrawEveryDayNumber(Integer withDrawEveryDayNumber) {
		this.withDrawEveryDayNumber = withDrawEveryDayNumber;
	}

	public BigDecimal getExceedTransferDayLimitExpenses() {
		return exceedTransferDayLimitExpenses;
	}

	public Integer getIsTransferSwich() {
		return isTransferSwich;
	}

	public void setIsTransferSwich(Integer isTransferSwich) {
		this.isTransferSwich = isTransferSwich;
	}
	
	public BigDecimal getWithDrawLotteryMultiple() {
		return withDrawLotteryMultiple;
	}

	public void setWithDrawLotteryMultiple(BigDecimal withDrawLotteryMultiple) {
		this.withDrawLotteryMultiple = withDrawLotteryMultiple;
	}
	
	public Integer getThirdPayOrderBettingMultiple() {
		return thirdPayOrderBettingMultiple;
	}

	public void setThirdPayOrderBettingMultiple(Integer thirdPayOrderBettingMultiple) {
		this.thirdPayOrderBettingMultiple = thirdPayOrderBettingMultiple;
	}

	public void setExceedTransferDayLimitExpenses(
			BigDecimal exceedTransferDayLimitExpenses) {
		this.exceedTransferDayLimitExpenses = exceedTransferDayLimitExpenses;
	}

	public String getReleasePolicyBonus() {
		return releasePolicyBonus;
	}

	public void setReleasePolicyBonus(String releasePolicyBonus) {
		this.releasePolicyBonus = releasePolicyBonus;
	}

	public Integer getMaintainSwich() {
		return maintainSwich;
	}

	public void setMaintainSwich(Integer maintainSwich) {
		this.maintainSwich = maintainSwich;
	}

	public String getMaintainContent() {
		return maintainContent;
	}

	public void setMaintainContent(String maintainContent) {
		this.maintainContent = maintainContent;
	}

	public String getReleasePolicyDaySaraly() {
		return releasePolicyDaySaraly;
	}

	public void setReleasePolicyDaySaraly(String releasePolicyDaySaraly) {
		this.releasePolicyDaySaraly = releasePolicyDaySaraly;
	}

	public BigDecimal getLhcLowerLotteryMoney() {
		return lhcLowerLotteryMoney;
	}

	public void setLhcLowerLotteryMoney(BigDecimal lhcLowerLotteryMoney) {
		this.lhcLowerLotteryMoney = lhcLowerLotteryMoney;
	}

	public BigDecimal getLhcHighestLotteryMoney() {
		return lhcHighestLotteryMoney;
	}

	public void setLhcHighestLotteryMoney(BigDecimal lhcHighestLotteryMoney) {
		this.lhcHighestLotteryMoney = lhcHighestLotteryMoney;
	}

	public BigDecimal getLhcHighestExpectLotteryMoney() {
		return lhcHighestExpectLotteryMoney;
	}

	public void setLhcHighestExpectLotteryMoney(
			BigDecimal lhcHighestExpectLotteryMoney) {
		this.lhcHighestExpectLotteryMoney = lhcHighestExpectLotteryMoney;
	}

	public BigDecimal getLhcMaxWinMoney() {
		return lhcMaxWinMoney;
	}

	public void setLhcMaxWinMoney(BigDecimal lhcMaxWinMoney) {
		this.lhcMaxWinMoney = lhcMaxWinMoney;
	}

	public Integer getIsClose() {
		return isClose;
	}

	public void setIsClose(Integer isClose) {
		this.isClose = isClose;
	}

	public String getAlarmPhone() {
		return alarmPhone;
	}

	public void setAlarmPhone(String alarmPhone) {
		this.alarmPhone = alarmPhone;
	}

	public String getAlarmEmail() {
		return alarmEmail;
	}

	public void setAlarmEmail(String alarmEmail) {
		this.alarmEmail = alarmEmail;
	}

	public Integer getAlarmPhoneOpen() {
		return alarmPhoneOpen;
	}

	public void setAlarmPhoneOpen(Integer alarmPhoneOpen) {
		this.alarmPhoneOpen = alarmPhoneOpen;
	}

	public Integer getAlarmEmailOpen() {
		return alarmEmailOpen;
	}

	public void setAlarmEmailOpen(Integer alarmEmailOpen) {
		this.alarmEmailOpen = alarmEmailOpen;
	}

	public Integer getLotteryAlarmOpen() {
		return lotteryAlarmOpen;
	}

	public void setLotteryAlarmOpen(Integer lotteryAlarmOpen) {
		this.lotteryAlarmOpen = lotteryAlarmOpen;
	}

	public BigDecimal getLotteryAlarmValue() {
		return lotteryAlarmValue;
	}

	public void setLotteryAlarmValue(BigDecimal lotteryAlarmValue) {
		this.lotteryAlarmValue = lotteryAlarmValue;
	}

	public Integer getWinAlarmOpen() {
		return winAlarmOpen;
	}

	public void setWinAlarmOpen(Integer winAlarmOpen) {
		this.winAlarmOpen = winAlarmOpen;
	}

	public BigDecimal getWinAlarmValue() {
		return winAlarmValue;
	}

	public void setWinAlarmValue(BigDecimal winAlarmValue) {
		this.winAlarmValue = winAlarmValue;
	}

	public Integer getRechargeAlarmOpen() {
		return rechargeAlarmOpen;
	}

	public void setRechargeAlarmOpen(Integer rechargeAlarmOpen) {
		this.rechargeAlarmOpen = rechargeAlarmOpen;
	}

	public BigDecimal getRechargeAlarmValue() {
		return rechargeAlarmValue;
	}

	public void setRechargeAlarmValue(BigDecimal rechargeAlarmValue) {
		this.rechargeAlarmValue = rechargeAlarmValue;
	}

	public Integer getWithdrawAlarmOpen() {
		return withdrawAlarmOpen;
	}

	public void setWithdrawAlarmOpen(Integer withdrawAlarmOpen) {
		this.withdrawAlarmOpen = withdrawAlarmOpen;
	}

	public BigDecimal getWithdrawAlarmValue() {
		return withdrawAlarmValue;
	}

	public void setWithdrawAlarmValue(BigDecimal withdrawAlarmValue) {
		this.withdrawAlarmValue = withdrawAlarmValue;
	}

	public Integer getRegShowPhone() {
		return regShowPhone;
	}

	public void setRegShowPhone(Integer regShowPhone) {
		this.regShowPhone = regShowPhone;
	}

	public Integer getRegRequiredPhone() {
		return regRequiredPhone;
	}

	public void setRegRequiredPhone(Integer regRequiredPhone) {
		this.regRequiredPhone = regRequiredPhone;
	}

	public Integer getRegShowEmail() {
		return regShowEmail;
	}

	public void setRegShowEmail(Integer regShowEmail) {
		this.regShowEmail = regShowEmail;
	}

	public Integer getRegRequiredEmail() {
		return regRequiredEmail;
	}

	public void setRegRequiredEmail(Integer regRequiredEmail) {
		this.regRequiredEmail = regRequiredEmail;
	}

	public Integer getRegShowQq() {
		return regShowQq;
	}

	public void setRegShowQq(Integer regShowQq) {
		this.regShowQq = regShowQq;
	}

	public Integer getRegRequiredQq() {
		return regRequiredQq;
	}

	public void setRegRequiredQq(Integer regRequiredQq) {
		this.regRequiredQq = regRequiredQq;
	}

	public Integer getAllowGuestLogin() {
		return allowGuestLogin;
	}

	public void setAllowGuestLogin(Integer allowGuestLogin) {
		this.allowGuestLogin = allowGuestLogin;
	}

	public Integer getGuestModel() {
		return guestModel;
	}

	public void setGuestModel(Integer guestModel) {
		this.guestModel = guestModel;
	}

	public BigDecimal getGuestMoney() {
		return guestMoney;
	}

	public void setGuestMoney(BigDecimal guestMoney) {
		this.guestMoney = guestMoney;
	}

	public Integer getIsOpenLevelAward() {
		return isOpenLevelAward;
	}

	public void setIsOpenLevelAward(Integer isOpenLevelAward) {
		this.isOpenLevelAward = isOpenLevelAward;
	}

	public Integer getIsOpenSignInActivity() {
		return isOpenSignInActivity;
	}

	public void setIsOpenSignInActivity(Integer isOpenSignInActivity) {
		this.isOpenSignInActivity = isOpenSignInActivity;
	}

	public BigDecimal getSigninRechargeMoney() {
		return signinRechargeMoney;
	}

	public void setSigninRechargeMoney(BigDecimal signinRechargeMoney) {
		this.signinRechargeMoney = signinRechargeMoney;
	}

	public String getSigninLastTime() {
		return signinLastTime;
	}

	public void setSigninLastTime(String signinLastTime) {
		this.signinLastTime = signinLastTime;
	}

	public Integer getRegDirectLogin() {
		return regDirectLogin;
	}

	public void setRegDirectLogin(Integer regDirectLogin) {
		this.regDirectLogin = regDirectLogin;
	}
	
	public Integer getCodePlanSwitch() {
		return codePlanSwitch;
	}

	public void setCodePlanSwitch(Integer codePlanSwitch) {
		this.codePlanSwitch = codePlanSwitch;
	}

	public Integer getIsNewUserGiftOpen() {
		return isNewUserGiftOpen;
	}

	public void setIsNewUserGiftOpen(Integer isNewUserGiftOpen) {
		this.isNewUserGiftOpen = isNewUserGiftOpen;
	}

	public BigDecimal getAppDownGiftMoney() {
		return appDownGiftMoney;
	}

	public void setAppDownGiftMoney(BigDecimal appDownGiftMoney) {
		this.appDownGiftMoney = appDownGiftMoney;
	}

	public BigDecimal getCompleteInfoGiftMoney() {
		return completeInfoGiftMoney;
	}

	public void setCompleteInfoGiftMoney(BigDecimal completeInfoGiftMoney) {
		this.completeInfoGiftMoney = completeInfoGiftMoney;
	}

	public BigDecimal getComplateSafeGiftMoney() {
		return complateSafeGiftMoney;
	}

	public void setComplateSafeGiftMoney(BigDecimal complateSafeGiftMoney) {
		this.complateSafeGiftMoney = complateSafeGiftMoney;
	}

	public BigDecimal getLotteryGiftMoney() {
		return lotteryGiftMoney;
	}

	public void setLotteryGiftMoney(BigDecimal lotteryGiftMoney) {
		this.lotteryGiftMoney = lotteryGiftMoney;
	}

	public BigDecimal getRechargeGiftMoney() {
		return rechargeGiftMoney;
	}

	public void setRechargeGiftMoney(BigDecimal rechargeGiftMoney) {
		this.rechargeGiftMoney = rechargeGiftMoney;
	}

	public BigDecimal getExtendUserGiftMoney() {
		return extendUserGiftMoney;
	}

	public void setExtendUserGiftMoney(BigDecimal extendUserGiftMoney) {
		this.extendUserGiftMoney = extendUserGiftMoney;
	}

	public Integer getIsSFSSCOpen() {
		return isSFSSCOpen;
	}

	public void setIsSFSSCOpen(Integer isSFSSCOpen) {
		this.isSFSSCOpen = isSFSSCOpen;
	}

	public Integer getIsWFSSCOpen() {
		return isWFSSCOpen;
	}

	public void setIsWFSSCOpen(Integer isWFSSCOpen) {
		this.isWFSSCOpen = isWFSSCOpen;
	}

	public Integer getIsSFKSOpen() {
		return isSFKSOpen;
	}

	public void setIsSFKSOpen(Integer isSFKSOpen) {
		this.isSFKSOpen = isSFKSOpen;
	}

	public Integer getIsWFKSOpen() {
		return isWFKSOpen;
	}

	public void setIsWFKSOpen(Integer isWFKSOpen) {
		this.isWFKSOpen = isWFKSOpen;
	}

	public Integer getIsSHFSSCOpen() {
		return isSHFSSCOpen;
	}

	public void setIsSHFSSCOpen(Integer isSHFSSCOpen) {
		this.isSHFSSCOpen = isSHFSSCOpen;
	}

	public Integer getIsWFSYXWOpen() {
		return isWFSYXWOpen;
	}

	public void setIsWFSYXWOpen(Integer isWFSYXWOpen) {
		this.isWFSYXWOpen = isWFSYXWOpen;
	}

	public Integer getIsSFSYXWOpen() {
		return isSFSYXWOpen;
	}

	public void setIsSFSYXWOpen(Integer isSFSYXWOpen) {
		this.isSFSYXWOpen = isSFSYXWOpen;
	}

	public Integer getIsSFPK10Open() {
		return isSFPK10Open;
	}

	public void setIsSFPK10Open(Integer isSFPK10Open) {
		this.isSFPK10Open = isSFPK10Open;
	}

	public Integer getIsWFPK10Open() {
		return isWFPK10Open;
	}

	public void setIsWFPK10Open(Integer isWFPK10Open) {
		this.isWFPK10Open = isWFPK10Open;
	}

	public Integer getIsSHFPK10Open() {
		return isSHFPK10Open;
	}

	public void setIsSHFPK10Open(Integer isSHFPK10Open) {
		this.isSHFPK10Open = isSHFPK10Open;
	}

	public Integer getIsXYFTOpen() {
		return isXYFTOpen;
	}

	public void setIsXYFTOpen(Integer isXYFTOpen) {
		this.isXYFTOpen = isXYFTOpen;
	}

	public Integer getIsLCQSSCOpen() {
		return isLCQSSCOpen;
	}

	public void setIsLCQSSCOpen(Integer isLCQSSCOpen) {
		this.isLCQSSCOpen = isLCQSSCOpen;
	}

	public Integer getIsNoSameNameBindBankCard() {
		return isNoSameNameBindBankCard;
	}

	public void setIsNoSameNameBindBankCard(Integer isNoSameNameBindBankCard) {
		this.isNoSameNameBindBankCard = isNoSameNameBindBankCard;
	}

	public Integer getRegShowTrueName() {
		return regShowTrueName;
	}

	public void setRegShowTrueName(Integer regShowTrueName) {
		this.regShowTrueName = regShowTrueName;
	}

	public Integer getRegRequiredTrueName() {
		return regRequiredTrueName;
	}

	public void setRegRequiredTrueName(Integer regRequiredTrueName) {
		this.regRequiredTrueName = regRequiredTrueName;
	}

	public Integer getIsJSSSCOpen() {
		return isJSSSCOpen;
	}

	public void setIsJSSSCOpen(Integer isJSSSCOpen) {
		this.isJSSSCOpen = isJSSSCOpen;
	}

	public Integer getIsBJSSCOpen() {
		return isBJSSCOpen;
	}

	public void setIsBJSSCOpen(Integer isBJSSCOpen) {
		this.isBJSSCOpen = isBJSSCOpen;
	}

	public Integer getIsGDSSCOpen() {
		return isGDSSCOpen;
	}

	public void setIsGDSSCOpen(Integer isGDSSCOpen) {
		this.isGDSSCOpen = isGDSSCOpen;
	}

	public Integer getIsSCSSCOpen() {
		return isSCSSCOpen;
	}

	public void setIsSCSSCOpen(Integer isSCSSCOpen) {
		this.isSCSSCOpen = isSCSSCOpen;
	}

	public Integer getIsSHSSCOpen() {
		return isSHSSCOpen;
	}

	public void setIsSHSSCOpen(Integer isSHSSCOpen) {
		this.isSHSSCOpen = isSHSSCOpen;
	}

	public Integer getIsSDSSCOpen() {
		return isSDSSCOpen;
	}

	public void setIsSDSSCOpen(Integer isSDSSCOpen) {
		this.isSDSSCOpen = isSDSSCOpen;
	}
}
