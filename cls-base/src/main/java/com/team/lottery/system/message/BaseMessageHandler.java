package com.team.lottery.system.message;

import java.io.Serializable;

public abstract class BaseMessageHandler implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4500748904488040745L;

	public abstract void dealMessage(BaseMessage message);
}
