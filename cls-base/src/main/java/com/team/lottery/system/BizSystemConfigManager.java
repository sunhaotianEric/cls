package com.team.lottery.system;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.service.BizSystemConfigService;
import com.team.lottery.util.ApplicationContextUtil;
import com.team.lottery.vo.BizSystemConfig;

public class BizSystemConfigManager {
	
	private static Logger logger = LoggerFactory.getLogger(BizSystemConfigManager.class);
	
	private static Map<String, BizSystemConfigVO> bizSystemConfigMap = new ConcurrentHashMap<String, BizSystemConfigVO>();
	
    private static BizSystemConfigService bizSystemConfigService = ApplicationContextUtil.getBean(BizSystemConfigService.class);
	
    /**
	 * 初始化业务系统配置项
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 */
	public static void initAllBizSystemConfig() {
		try {
			logger.info("初始化业务系统配置项start");
			List<BizSystemConfig> bizSystemlist = bizSystemConfigService.getAllBizSystems();
			
			bizSystemConfigMap = new ConcurrentHashMap<String, BizSystemConfigVO>();
			for (BizSystemConfig biz : bizSystemlist) {
				List<BizSystemConfig> list = bizSystemConfigService.getBizSystemConfigByQuery(biz.getBizSystem());
				Map<String, String> voMap = new HashMap<String, String>();
				BizSystemConfigVO bizSystemConfigVO = new BizSystemConfigVO();
				for (BizSystemConfig bizSystemConfig : list) {
					voMap.put(bizSystemConfig.getConfigKey(), bizSystemConfig.getConfigValue());
				}
				try {
					logger.debug("bizsytem:{}, map:{}", biz.getBizSystem(), voMap);
					BeanUtils.populate(bizSystemConfigVO, voMap);
				} catch (Exception e) {
					logger.error("转换Map对象出错...", e);
				}
	
				bizSystemConfigMap.put(biz.getBizSystem(), bizSystemConfigVO);
			}
			logger.info("初始化业务系统配置项end bizSystemConfigMapsize=" + bizSystemConfigMap.size());
		} catch (Exception e) {
			logger.error("初始化业务系统配置项错误", e);
		}
	}
	
	/**
	 * 全部刷新业务系统配置
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 */
	public static void refreshAllBizSystemConfig() throws IllegalAccessException, InvocationTargetException {
		initAllBizSystemConfig();
	}
	
	/**
	 * 根据业务系统刷新业务系统配置
	 * @param bizSystem
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 */
	public static void refreshBizSystemConfig(String bizSystem) {
		try {
			logger.info("根据业务系统刷新业务系统配置start");
			List<BizSystemConfig> list = bizSystemConfigService.getBizSystemConfigByQuery(bizSystem);
			Map<String, String> voMap = new HashMap<String, String>();
			BizSystemConfigVO bizSystemConfigVO = new BizSystemConfigVO();
			for (BizSystemConfig bizSystemConfig : list) {
				voMap.put(bizSystemConfig.getConfigKey(), bizSystemConfig.getConfigValue());
			}
			BeanUtils.populate(bizSystemConfigVO, voMap);
			bizSystemConfigMap.put(bizSystem, bizSystemConfigVO);
			logger.info("根据业务系统刷新业务系统配置end bizSystemConfigMapsize=" + bizSystemConfigMap.size());
		} catch (Exception e) {
			logger.error("根据业务系统刷新业务系统配置错误", e);
		}
	}
	
	/**
	 * 根据业务系统获取配置项
	 * @param bizSystem
	 * @return
	 */
	public static BizSystemConfigVO getBizSystemConfig(String bizSystem) {
		BizSystemConfigVO bizSystemConfigVO = bizSystemConfigMap.get(bizSystem);
		return bizSystemConfigVO;
	}
}
