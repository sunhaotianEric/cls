package com.team.lottery.system.message.messagetype;

import java.util.List;

import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.handler.RedisPubSubMessageHandler;
import com.team.lottery.vo.OpenCodeApi;

public class OpenCodeApiMessage extends BaseMessage {




	/**
	 * 
	 */
	private static final long serialVersionUID = -5676886109235007695L;
	
	/**
      * 对象
      */
	private List<OpenCodeApi> list;
	    
	public OpenCodeApiMessage() {
		this.handler = new RedisPubSubMessageHandler();
	}



	public List<OpenCodeApi> getList() {
		return list;
	}



	public void setList(List<OpenCodeApi> list) {
		this.list = list;
	}



	
}
