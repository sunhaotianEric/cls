package com.team.lottery.system.message;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.system.message.messagetype.RedisPubSubMessage;

/**
 * 消息队列中心
 * @author luocheng
 *
 */
public class MessageQueueCenter {
	
	private static Logger logger = LoggerFactory.getLogger(MessageQueueCenter.class);
	private static BlockingQueue<BaseMessage> messageQueue = new LinkedBlockingQueue<BaseMessage>();
	
	/**
	 * 放置消息到本地消息队列中
	 * @param message
	 */
	public static void putMessage(BaseMessage message) {
		try {
			logger.debug("放置消息到本地消息队列中，消息类型[{}]", message.getType());
			boolean putSuccessRes = messageQueue.offer(message);
			if(!putSuccessRes) {
				logger.info("无法放置消息到本地消息队列中，消息类型["+ message +"]");
			}
		} catch (Exception e) {
			logger.info("放置消息到本地消息队列中发生错误，消息类型["+ message +"]", e);
		}
	}
	
	/**
	 * 发布消息到redis中
	 * @param <T>
	 * @param message
	 */
	public static <T> void putRedisMessage(BaseMessage baseMessage,String channel) {
		try {
			logger.info("发布redis消息，消息通道[{}],消息类型[{}]", channel, baseMessage.getType());
	        RedisPubSubMessage redisPubSubMessage=new RedisPubSubMessage();
	        redisPubSubMessage.setChannel(channel);
	       
	        redisPubSubMessage.setBaseMessage(baseMessage);
	        redisPubSubMessage.setOperateType("pub");        
	        putMessage(redisPubSubMessage);
		} catch (Exception e) {
			logger.error("发布消redis发生错误，消息类型["+ baseMessage +"]", e);
		}
	}
	/**
	 * 从消息队列中获取消息，此方法会阻塞当前线程
	 * @return
	 */
	public static BaseMessage takeMessage() {
		BaseMessage message = null;
		try {
			message = messageQueue.take();
		} catch (InterruptedException e) {
			logger.error("消息队列中获取消息发生线程中断异常", e);
		}
		return message;
	}
}
