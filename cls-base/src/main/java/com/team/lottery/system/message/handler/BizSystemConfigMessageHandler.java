package com.team.lottery.system.message.handler;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.system.BizSystemConfigManager;
import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.BaseMessageHandler;
import com.team.lottery.system.message.messagetype.BizSystemConfigMessage;
import com.team.lottery.vo.BizSystemConfig;

public class BizSystemConfigMessageHandler extends BaseMessageHandler implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6560943204227426245L;
	private static Logger logger = LoggerFactory.getLogger(BizSystemConfigMessageHandler.class);
	
	
	@Override
	public void dealMessage(BaseMessage message) {
		try {
			logger.info("接受redis订阅消息，开始更新业务系统参数配置常量缓存...");
			BizSystemConfigMessage bizSystemConfigMessage=(BizSystemConfigMessage) message;
			BizSystemConfig bizSystemConfig=bizSystemConfigMessage.getBizSystemConfig();
			BizSystemConfigManager.refreshBizSystemConfig(bizSystemConfig.getBizSystem());	
		} catch (Exception e) {
			logger.error("处理业务系统参数配置常量缓存发生错误...", e);
		}
	}

}
