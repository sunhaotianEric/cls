package com.team.lottery.system.message.messagetype;

import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.handler.BizSystemDomainMessageHandler;
import com.team.lottery.vo.BizSystemDomain;

public class BizSystemDomainMessage extends BaseMessage {
     /**
	 * 
	 */
	private static final long serialVersionUID = -2087166359655159769L;

	/**
      * 域名管理对象
      */
	private BizSystemDomain bizSystemDomain;
	
	/**
	 * 0=修改，1=增加，-1=删除
	 */
	private String operateType;


	public BizSystemDomainMessage() {
		this.handler=new BizSystemDomainMessageHandler();
	}
	
	public BizSystemDomain getBizSystemDomain() {
		return bizSystemDomain;
	}

	public void setBizSystemDomain(BizSystemDomain bizSystemDomain) {
		this.bizSystemDomain = bizSystemDomain;
	}

	public String getOperateType() {
		return operateType;
	}

	public void setOperateType(String operateType) {
		this.operateType = operateType;
	}
	
	
}
