package com.team.lottery.system.message.messagetype;

import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.handler.SystemConfigMessageHandler;
import com.team.lottery.vo.SystemConfig;

public class SystemConfigMessage extends BaseMessage {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1247548235339862489L;

	/**
      * 业务系统对象
      */
	private SystemConfig systemConfig;
	
	/**
	 * 0=修改，1=增加，-1=删除
	 */
	private String operateType;
    
	public SystemConfigMessage() {
		this.handler=new SystemConfigMessageHandler();
	}

	public SystemConfig getSystemConfig() {
		return systemConfig;
	}

	public void setSystemConfig(SystemConfig systemConfig) {
		this.systemConfig = systemConfig;
	}

	public String getOperateType() {
		return operateType;
	}

	public void setOperateType(String operateType) {
		this.operateType = operateType;
	}
	
}
