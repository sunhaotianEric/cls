package com.team.lottery.dto;

import java.util.Date;

/**
 * 公用查询对象
 * @author luocheng
 *
 */
public class CommonQueryVo {

	private Integer pageNo;    //当前页号。
    private Integer pageSize;  //每页记录条数。
    private Integer timeType;  //查询时间类型。
    private Integer dateRange;
    //起始时间
 	private Date dateStart;
 	//结束时间
 	private Date dateEnd;
 	
	public Integer getPageNo() {
		return pageNo;
	}
	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public Integer getTimeType() {
		return timeType;
	}
	public void setTimeType(Integer timeType) {
		this.timeType = timeType;
	}
	public Date getDateStart() {
		return dateStart;
	}
	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}
	public Date getDateEnd() {
		return dateEnd;
	}
	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}
	public Integer getDateRange() {
		return dateRange;
	}
	public void setDateRange(Integer dateRange) {
		this.dateRange = dateRange;
	}
	
 	
 	
}
