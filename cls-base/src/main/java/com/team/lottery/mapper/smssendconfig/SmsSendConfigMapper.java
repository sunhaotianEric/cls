package com.team.lottery.mapper.smssendconfig;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.SmsSendConfig;

public interface SmsSendConfigMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SmsSendConfig record);

    int insertSelective(SmsSendConfig record);

    SmsSendConfig selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SmsSendConfig record);

    int updateByPrimaryKey(SmsSendConfig record);
    
    /**
     * 查询所有可用的发送配置
     * @return
     */
    List<SmsSendConfig> queryAllSmsSendConfig();
    
    /**
     * 分页条件
     * @param query
     * @param startNum
     * @param pageNum
     * @return
     */
    public List<SmsSendConfig> getAllSmsSendConfig(@Param("query")SmsSendConfig query, @Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
    
    /**
     * 查询所有
     * @param query
     * @return
     */
	public int  getSmsSendConfig(@Param("query")SmsSendConfig query);
}