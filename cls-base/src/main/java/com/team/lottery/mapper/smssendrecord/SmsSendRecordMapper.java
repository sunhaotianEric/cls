package com.team.lottery.mapper.smssendrecord;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.SmsSendRecord;

public interface SmsSendRecordMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SmsSendRecord record);

    int insertSelective(SmsSendRecord record);

    SmsSendRecord selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SmsSendRecord record);

    int updateByPrimaryKey(SmsSendRecord record);
    
    List<SmsSendRecord> queryAllSmsSendRecord(@Param("query")SmsSendRecord query);

    /**
     * 分页条件
     * @param query
     * @param startNum
     * @param pageNum
     * @return
     */
	List<SmsSendRecord> getAllSmsSendRecord(@Param("query")SmsSendRecord query, @Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
	
	/**
	 * 查询所有的
	 * @param query
	 * @return
	 */
	public int  getSmsSendRecord(@Param("query")SmsSendRecord query); 
}