package com.team.lottery.mapper.admin;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.Admin;

public interface AdminMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Admin record);

    int insertSelective(Admin record);

    Admin selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Admin record);

    int updateByPrimaryKey(Admin record);
    
    public List<Admin> getAllAdmins();

    /**
     * 根据用户名查找用户
     * @param admin
     * @return
     */
	List<Admin> getAdminByUsername(@Param("admin")Admin admin);
	
	/**
	 * 根据条件查找管理员用户
	 * @param admin
	 * @return
	 */
	public List<Admin> getAdminByCondition(Admin admin);
	
	 /**
     * 查询符合条件的记录条数
     * @param query
     * @return
     */
    public Integer getAllAdminByQueryPageCount(@Param("query")Admin query);
        
    /**
     * 分页条件
     * @return
     */
    public  List<Admin> getAllAdminByQueryPage(@Param("query")Admin query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
	
    /**
     * 查询该系统的超级管理员个数 
     * @param query
     * @return
     */
    public Integer getSuperManagerCountByBizSystem(@Param("query")Admin query);
    
    /**
     * 根据业务系统和角色查询管理员
     * @param query
     * @return
     */
    public List<Admin> getAllAdminByQuery(@Param("query")Admin query);
    
    /**
     * 根据业务系统查询所有管理员
     * @param query
     * @return
     */
    public List<Admin> getAllAdminByBizSystem(@Param("query")Admin query);
}