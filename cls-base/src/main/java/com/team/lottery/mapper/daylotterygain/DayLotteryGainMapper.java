package com.team.lottery.mapper.daylotterygain;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.DayLotteryGainQuery;
import com.team.lottery.vo.DayLotteryGain;

public interface DayLotteryGainMapper {
    int deleteByPrimaryKey(Long id);

    int insert(DayLotteryGain record);

    int insertSelective(DayLotteryGain record);
    //插入存在就更新
    int insertOrUpdate(DayLotteryGain record);

    DayLotteryGain selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(DayLotteryGain record);

    int updateByPrimaryKey(DayLotteryGain record);
    
    DayLotteryGain getDayLotteryGainByQuery(@Param("query")DayLotteryGain query);
    
    public  List<DayLotteryGain> getAllDayLotteryGainByQueryPage(@Param("query")DayLotteryGainQuery query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
    
    /**
     * 查询符合条件的记录条数
     * @param query
     * @return
     */
    public Integer getAllDayLotteryGainByQueryPageCount(@Param("query")DayLotteryGainQuery query);
    
    public List<DayLotteryGain> getDayLotteryCountByQuery(@Param("query")DayLotteryGainQuery query);
    
}