package com.team.lottery.mapper.bizsystemdomain;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.BizSystemDomain;

public interface BizSystemDomainMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BizSystemDomain record);

    int insertSelective(BizSystemDomain record);

    BizSystemDomain selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BizSystemDomain record);

    int updateByPrimaryKey(BizSystemDomain record);
    
    /**
     * 更新成不是首推域名  
     * @param record
     * @return
     */
    int updateFirstUrl(BizSystemDomain record);
    /**
     * 查询符合条件的记录条数
     * @param query
     * @return
     */
    public Integer getAllBizSystemDomainByQueryPageCount(@Param("query")BizSystemDomain query);
        
    /**
     * 分页条件
     * @return
     */
    public  List<BizSystemDomain> getAllBizSystemDomainByQueryPage(@Param("query")BizSystemDomain query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
    
    /**
     * 查询所有记录
     * @return
     */
    public  List<BizSystemDomain> getAllBizSystemDomain(@Param("query")BizSystemDomain query);
}