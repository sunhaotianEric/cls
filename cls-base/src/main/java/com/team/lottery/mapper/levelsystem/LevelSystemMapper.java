package com.team.lottery.mapper.levelsystem;
import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.LevelSystem;

public interface LevelSystemMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(LevelSystem record);

    int insertSelective(LevelSystem record);

    LevelSystem selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(LevelSystem record);

    int updateByPrimaryKey(LevelSystem record);

    /**
     * 查询两个等级之间的晋级奖励的总和
     * @param startLevel,endLevel,bizSystem
     * @return
     */
    BigDecimal selectLevelBetween(@Param("startLevel")Integer startLevel,@Param("endLevel")Integer endLevel,@Param("bizSystem")String bizSystem);
    /**
     * 查询当前积分对应的最大积分奖励设置
     * @param point,bizSystem
     * @return
     */
    List<LevelSystem> selectMaxLevel(@Param("point")Integer point,@Param("bizSystem")String bizSystem);

    int addEmps(@Param("emps")List<LevelSystem> emps);
    /**
     * 查询同个业务系统下是否存在相同的VIP等级
     * @param levelSystem
     * @return
     */
    int getLevelSystemBylevel(@Param("query")LevelSystem levelSystem);
    
    List<LevelSystem> getLevelSytem(@Param("query")LevelSystem levelSystem,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
    int getLevelSytemCount(@Param("query")LevelSystem levelSystem);
    /**
     * 查询某个业务系统下的等级机制
     * @param levelSystem
     * @return
     */
    List<LevelSystem> getLevelSystemByBizSystem(@Param("query")LevelSystem levelSystem);
    
    List<LevelSystem> selectDifferenceLevel(@Param("currentpoint")Integer currentpoint, @Param("point")Integer point,@Param("bizSystem")String bizSystem);
}