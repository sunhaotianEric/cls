package com.team.lottery.mapper.bizsysteminfo;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.BizSystemInfo;

public interface BizSystemInfoMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BizSystemInfo record);

    int insertSelective(BizSystemInfo record);

    BizSystemInfo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BizSystemInfo record);

    int updateByPrimaryKeyWithBLOBs(BizSystemInfo record);

    int updateByPrimaryKey(BizSystemInfo record);
    
    /**
     * 查询符合条件的记录条数
     * @param query
     * @return
     */
    public Integer getAllBizSystemInfoByQueryPageCount(@Param("query")BizSystemInfo query);
        
    /**
     * 分页条件
     * @return
     */
    public  List<BizSystemInfo> getAllBizSystemInfoByQueryPage(@Param("query")BizSystemInfo query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
    
    /**
     * 根据条件查询
     * @param query
     * @return
     */
    public List<BizSystemInfo> getBizSystemInfoByCondition(@Param("query")BizSystemInfo query);
}