package com.team.lottery.mapper.emailsendconfig;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.EmailSendConfig;

public interface EmailSendConfigMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(EmailSendConfig record);

    int insertSelective(EmailSendConfig record);

    EmailSendConfig selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(EmailSendConfig record);

    int updateByPrimaryKey(EmailSendConfig record);
    
    /**
     * 查询所有可用的邮件发送配置
     * @return
     */
    List<EmailSendConfig> queryAllEmailSendConfig();
    
    /**
     * 分页条件
     * @param query
     * @param startNum
     * @param pageNum
     * @return
     */
    public List<EmailSendConfig> getAllEmailSendConfig(@Param("query")EmailSendConfig query, @Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
    
    /**
     * 查询所有
     * @param query
     * @return
     */
	public int  getEmailSendConfigCount(@Param("query")EmailSendConfig query);
	
	
}