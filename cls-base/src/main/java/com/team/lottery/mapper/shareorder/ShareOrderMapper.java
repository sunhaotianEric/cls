package com.team.lottery.mapper.shareorder;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.ShareOrder;

public interface ShareOrderMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ShareOrder record);

    int insertSelective(ShareOrder record);

    ShareOrder selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ShareOrder record);

    int updateByPrimaryKey(ShareOrder record);
    
    List<ShareOrder> getShareOrderPage(@Param("query")ShareOrder query,@Param("startNum")int startIndex, @Param("pageNum")Integer pageSize);
    int getShareOrderPageCount(@Param("query")ShareOrder query);
}