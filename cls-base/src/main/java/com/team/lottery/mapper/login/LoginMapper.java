package com.team.lottery.mapper.login;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.DayOnlineTypeCountVo;
import com.team.lottery.extvo.LoginLogQuery;
import com.team.lottery.vo.Login;

public interface LoginMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Login record);

    int insertSelective(Login record);

    Login selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Login record);

    int updateByPrimaryKey(Login record);

    /**
     * 查询符合条件的记录条数
     * @param query
     * @return
     */
    public Integer getAllLoginLogsByQueryPageCount(LoginLogQuery query);
    
    /**
     * 分页条件查询登陆日志
     * @return
     */
	List<Login> getAllLoginLogsByQueryPage(@Param("query")LoginLogQuery query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
	
	/**
     * 删除指定时间内的记录条数
     * @return
     */
	public void delLoginLogsByDay(@Param("dayFrom")Date from,@Param("dayTo")Date to);
	
	/**
	 * 查询上次登录的日志(第二次)
	 * @param userId
	 * @return
	 */
	public List<Login> getLastIPAddress(String userName);
	
	/**
	 * 查询每日登录人数(按登录类型统计)
	 * @param query
	 * @return
	 */
	public List<DayOnlineTypeCountVo> getDayLoginTypeCount(@Param("query")LoginLogQuery query);
	
	/**
	 * 查询每日登录人数
	 * @param query
	 * @return
	 */
	public Integer getDayLoginCount(@Param("query")LoginLogQuery query);
	
	
	/**
	 * 查询登录日志
	 * @param query
	 * @return
	 */
	public List<Login> getAllLoginLogs(@Param("query")LoginLogQuery query);
}