package com.team.lottery.mapper.config;

import java.util.Date;

import com.team.lottery.vo.Config;

public interface ConfigMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Config record);

    int insertSelective(Config record);

    Config selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Config record);

    int updateByPrimaryKeyWithBLOBs(Config record);
    
    /**
     * 删除某一天之前的数据
     * @param endDate
     */
    public void deleteAgoDataFromThisTimeForOrder(Date endDate);
    public void deleteAgoDataFromThisTimeForMoneyDetail(Date endDate);
    public void deleteAgoDataFromThisTimeForNotes(Date endDate);
    public void deleteAgoDataFromThisTimeForSystemNotes(Date endDate);
    //public void deleteAgoDataFromThisTimeForDrawOrder(Date endDate);
    public void deleteAgoDataFromThisTimeForLogin(Date endDate);
    public void deleteAgoDataFromThisTimeForLotteryCode(Date endDate);
    public void deleteAgoDataFromThisTimeForLotteryCodeXy(Date endDate);
    public void deleteAgoDataFromThisTimeForLowLotteryCode(Date endDate);
    public void deleteAgoDataFromThisTimeForUseMoneyLog(Date endDate);
    public void deleteAgoDataFromThisTimeForLotteryCache(Date endDate);
    public void deleteAgoDataFromThisTimeForOrderAfterRecord(Date endDate);
    public void deleteAgoDataFromThisTimeForOrderAfterAmount(Date endDate);
   
    public void deleteAgoDataFromThisTimeForQuotaDetail(Date endDate);
    public void deleteAgoDataFromThisTimeForUserDayConsume(Date endDate);
    public void deleteAgoDataFromThisTimeForUserHourConsume(Date endDate);
    public void deleteAgoDataFromThisTimeForUserDayMoney(Date endDate);
    public void deleteAgoDataFromThisTimeForUserSelfDayConsume(Date endDate);
    public void deleteAgoDataFromThisTimeForUserModelUsed(Date endDate);
    public void deleteAgoDataFromThisTimeForDayConsumeActivityOrder(Date endDate);
    public void deleteAgoDataFromThisTimeForRechargeOrder(Date endDate);
    public void deleteAgoDataFromThisTimeForWithdrawOrder(Date endDate);
    
    public void deleteAgoDataFromThisTimeForGuestUserMoneyTotal(Date endDate);
    public void deleteAgoDataFromThisTimeForGuestUser(Date endDate);
    public void deleteAgoDataFromThisTimeForGuestOrder(Date endDate);
    public void deleteAgoDataFromThisTimeForGuestMoneyDetial(Date endDate);
    public void deleteAgoDataFromThisTimeForGuestLoginLog(Date endDate);
    
    public void deleteAgoDataFromThisTimeForUserDayLotteryCount(Date endDate);
    
    public void deleteAgoDateFromThisTimeForChatRedBag(Date endDate);
    public void deleteAgoDateFromThisTimeForChatRedBagInfo(Date endDate);
    public void deleteAgoDateFromThisTimeForChatlog(Date endDate);
    
    public void deleteAgoDateFromThisTimeForUserCookieLog(Date endDate);
    public void deleteAgoDateFromThisTimeForUserBettingDemand(Date endDate);
    
    public void deletePasswordErrorLogDateFromThisTimeForThirtyDaysAgo(Date endDate);
    public void deleteAdminPasswordErrorLogDateFromThisTimeForThirtyDaysAgo(Date endDate);
    
    public void deleteAgoDataFromThisTimeForRiskRecord(Date endDate);
    
    
    //删除业务系统
    public void deleteBizSystem(String bizSystem);
    public void deleteBizSystemBonusConfig(String bizSystem);
    public void deleteBizSystemConfig(String bizSystem);  
    public void deleteBizSystemDomain(String bizSystem);
    public void deleteBizSystemInfo(String bizSystem);
    public void deleteAwardModelConfig(String bizSystem);
    public void deleteLotteryControlConfig(String bizSystem);
    public void deleteDomainInvitationCode(String bizSystem);
    public void deleteDayProfit(String bizSystem);
    public void deleteDayOnlineInfo(String bizSystem);
    public void deleteLotteryDayGain(String bizSystem);
    public void deleteAdmin(String bizSystem);
    public void deleteUser(String bizSystem);
    public void deleteUserMoneyTotal(String bizSystem);
    public void deleteUserBank(String bizSystem);
    public void deleteUserRegister(String bizSystem);
    public void deleteQuotaControl(String bizSystem);
    public void deleteUserQuotaDetail(String bizSystem);
    public void deleteUserQuota(String bizSystem);
    public void deleteLoginLog(String bizSystem);
    public void deleteMoneyDetail(String bizSystem);
    public void deleteNotes(String bizSystem);
    public void deleteUserSafetyQuestion(String bizSystem);
    public void deleteUserHourConsume(String bizSystem);
    public void deleteUserDayConsume(String bizSystem);
    public void deleteUserSelfDayConsume(String bizSystem);
    public void deleteOrder(String bizSystem);
    public void deleteLotteryCache(String bizSystem);
    public void deleteOrderAfterRecord(String bizSystem);
    public void deleteOrderAmount(String bizSystem);
    public void deleteUserDayLotteryCount(String bizSystem);
    public void deleteBonusAuth(String bizSystem);
    public void deleteBonusConfig(String bizSystem);
    public void deleteBonusOrder(String bizSystem);
    public void deleteDaySalaryAuth(String bizSystem);
    public void deleteDaySalaryConfig(String bizSystem);
    public void deleteDaySalaryOrder(String bizSystem);
    public void deleteDayConsumeActivityConfig(String bizSystem);
    public void deleteDayConsumeActivityOrder(String bizSystem);
    //public void deleteRechargeDrawOrder(String bizSystem);
    public void deleteRechargeConfig(String bizSystem);
    public void deletePayBank(String bizSystem);
    public void deleteChargePay(String bizSystem);
    public void deleteActivity(String bizSystem);
    public void deleteHomePic(String bizSystem);
    public void deleteHelp(String bizSystem);
    public void deleteHelps(String bizSystem);
    public void deleteAnnounce(String bizSystem);
    public void deleteLotteryWinLhc(String bizSystem);
    public void deleteLotteryWinXyeb(String bizSystem);
    public void deleteAdminRecvNote(String bizSystem);
    public void deleteAlarmInfo(String bizSystem);
    public void deleteAdminDomain(String bizSystem);
    public void deleteLevelSystem(String bizSystem);
    public void deleteUserCookieLog(String bizSystem);
    public void deleteUserBettingDemand(String bizSystem);
    public void deleteRechargeOrder(String bizSystem);
    public void deleteWithdrawOrder(String bizSystem);
    public void deleteWithdrawAutoLog(String bizSystem);
    public void deleteWithdrawAutoConfig(String bizSystem);

    public void deleteChatBan(String bizSystem);
    public void deleteChatBlackList(String bizSystem);
    public void deleteChatLevelAuth(String bizSystem);
    public void deleteChatRoleUser(String bizSystem);
    public void deleteChatRoom(String bizSystem);
    public void deleteChatUserSendRebagAuth(String bizSystem);
    public void deleteChatLog(String bizSystem);
    public void deleteChatroomConfig(String bizSystem);
    public void deleteChatRedbag(String bizSystem);
    public void deleteChatRedbagInfo(String bizSystem);
    public void deleteFeeBack(String bizSystem);

    
    // 删除业务密码错误日志记录表.
	public void deletePasswordErrorLog(String bizSystem);
	public void deleteAdminPasswordErrorLog(String bizSystem);
	public void deleteLotteryCodeXyTrend(String bizSystem);
	
	//根据业务删系统除开奖相关表
	public void deleteLotteryCodeXy(String bizSystem);
	public void deleteLotteryCodeXyImport(String bizSystem);
	
	// 根据业务系统删除操作日志记录数据.
	public void deleteAdminOperateLog(String bizSystem);
	public void deleteUserOperateLog(String bizSystem);

	// 根据业务系统删除彩种开关数据.
	public void deleteLotterySwitchByBizSystem(String bizSystem);

}