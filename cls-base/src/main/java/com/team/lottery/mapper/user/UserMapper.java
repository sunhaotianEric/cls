package com.team.lottery.mapper.user;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.OrderQuery;
import com.team.lottery.extvo.SendNoteQueryVo;
import com.team.lottery.extvo.TeamUserQuery;
import com.team.lottery.extvo.UserDeleteQuery;
import com.team.lottery.extvo.UserQuery;
import com.team.lottery.vo.User;

public interface UserMapper {

	int deleteByPrimaryKey(Integer id);

	int insert(User record);

	int insertSelective(User record);

	User selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(User record);

	int updateByPrimaryKey(User record);

	/**
	 * 查找用户并且加上锁
	 * 
	 * @param id
	 * @return
	 */
	User selectByPrimaryKeyForUpdate(Integer id);

	/**
	 * 根据用户名,查找用户并且加上锁
	 * 
	 * @param id
	 * @return
	 */
	public User selectByUserNameForUpdate(String memberUserName);

	/**
	 * 查找某个用户的所有注册会员
	 * 
	 * @param userName
	 * @return
	 */
	public List<User> getRegFromByUser(@Param("currentUserName") String currentUserName, @Param("memberUserName") String memberUserName, @Param("isQuerySub") Boolean isQuerySub,
			@Param("currentUsersBizSystem") String currentUsersBizSystem);

	/**
	 * 查询用户最大编码
	 * 
	 * @return
	 */
	public Integer getUserMaxCode(String bizSystem);

	/**
	 * 判断用户安全密码是否为空
	 * 
	 * @param userId
	 * @return
	 */
	public User getUserByPassword1IsNotNull(Integer userId);

	/**
	 * 根据条件查找用户
	 * 
	 * @param user
	 * @return
	 */
	public User getUser(User user);

	/**
	 * 用户名查找对应的用户
	 * 
	 * @param username
	 * @return
	 */
	public User getUserForUnique(User user);

	/**
	 * 根据用户名查找用户
	 * 
	 * @param username
	 * @return
	 */
	public User getUserByName(@Param("bizSystem") String bizSystem, @Param("userName") String userName);

	/**
	 * 根据条件查找用户列表
	 * 
	 * @param query
	 * @return
	 */
	public List<User> getUsersByCondition(UserQuery query);

	/**
	 * 查询多少天没有登录及余额为0的会员
	 * 
	 * @param dayCount
	 * @return
	 */
	public List<User> queryNoMoneyForNumberOfDays(Integer dayCount);

	/**
	 * 查询多少天没有登录的会员
	 * 
	 * @param dayCount
	 * @return
	 */
	public List<User> queryForNumberOfDays(Integer dayCount);

	/**
	 * 查询符合条件的记录条数
	 * 
	 * @param query
	 * @return
	 */
	public Integer getAllUsersByQueryPageCount(@Param("query") UserQuery query);

	/**
	 * 分页条件查询登陆日志
	 * 
	 * @return
	 */
	public List<User> getAllUsersByQueryPage(@Param("query") UserQuery query, @Param("startNum") Integer startNum, @Param("pageNum") Integer pageNum);

	/**
	 * 统计用户的所有余额、分红、盈利等数据
	 * 
	 * @return
	 */
	public User getTotalUsersConsumeReport();

	/**
	 * 分页查询团队的下级会员列表
	 * 
	 * @param query
	 * @param startNum
	 * @param pageNum
	 * @return
	 */
	public List<User> getTeamUserListByQueryPage(@Param("query") TeamUserQuery query, @Param("startNum") Integer startNum, @Param("pageNum") Integer pageNum);

	/**
	 * 查询团队的下级会员列表的记录条数
	 * 
	 * @param query
	 * @return
	 */
	public Integer getTeamUserListByQueryPageCount(@Param("query") TeamUserQuery query);

	/**
	 * 查询团队的下级会员列表
	 * 
	 * @param query
	 * @param startNum
	 * @param pageNum
	 * @return
	 */
	public List<User> getAllTeamUserList(@Param("query") TeamUserQuery query);

	/**
	 * 查询用户被锁数据
	 * 
	 * @param user
	 * @return
	 */
	public User getUserLock(User user);

	/**
	 * 查找所有的用户
	 * 
	 * @return
	 */
	public List<User> getAllUsers();

	/**
	 * 查找无效会员
	 * 
	 * @return
	 */
	public List<User> everyDeleteUsers();

	/**
	 * 查找用户直接下级某个模式的数量 iseqLevel 同模式与不同模式
	 * 
	 * @return
	 */
	public Integer getUserCountBySscRebate(@Param("user") User user, @Param("iseqLevel") Boolean iseqLevel);

	/**
	 * 根据时时彩模式条件查找用户列表
	 * 
	 * @param query
	 * @return
	 */
	public List<User> getUsersByModelCondition(UserQuery query);

	/**
	 * 更新用户 版本号+1
	 * 
	 * @param record
	 * @return
	 */
	public int updateByPrimaryKeyWithVersionSelective(User record);

	/**
	 * 根据要发送站内信条件查找用户列表
	 * 
	 * @param query
	 * @return
	 */
	List<User> getUsersBySendNoteQuery(SendNoteQueryVo query);

	/**
	 * 查询出要删除的会员
	 * 
	 * @param query
	 * @return
	 */
	public List<User> getUsersByDeleteCondition(UserDeleteQuery query);

	/**
	 * 根据代理类型统计用户余额总额
	 * 
	 * @param query
	 * @return
	 */
	public List<User> getTotalMoneyByDailiLevel(@Param("bizSystem") String bizSystem);

	/**
	 * 查询新加入的会员人数
	 * 
	 * @param query
	 * @return
	 */
	public Integer getNewUsersCount(@Param("query") TeamUserQuery query);

	/**
	 * 获取最大模式值
	 * 
	 * @param query
	 * @return
	 */
	public Integer getMaxRebate(@Param("regfromName") String regfromName, @Param("bizSystem") String bizSystem, @Param("rebateType") Integer rebateType);

	/**
	 * 在线会员列表
	 * 
	 * @param query
	 * @param startNum
	 * @param pageNum
	 * @return
	 */
	public List<User> getOnlineUserListByQueryPage(@Param("querys") List<TeamUserQuery> querys, @Param("startNum") Integer startNum, @Param("pageNum") Integer pageNum,
			@Param("orderSort") Integer orderSort);

	/**
	 * 在线会员列表的记录条数
	 * 
	 * @param query
	 * @return
	 */
	public Integer getOnlineUserListByQueryPageCount(@Param("querys") List<TeamUserQuery> querys);

	/**
	 * 查询出有配额的已使用配额数
	 * 
	 * @return
	 */
	public List<User> userquotaCount(@Param("query") User query);

	/**
	 * 查询首充人数
	 * 
	 * @param bizSystem
	 * @param firstRechargeTime
	 * @return
	 */
	public Integer getUserCountByFirstRecharger(@Param("bizSystem") String bizSystem, @Param("startTime") Date startTime, @Param("endTime") Date endTime,
			@Param("userName") String userName);

	/**
	 * 查找当天00:00:00-23:59:59 注册的用户
	 * 
	 * @param addtime
	 * @return
	 */
	public List<User> getUserByAddtime(@Param("bizSystem") String bizSystem, @Param("userName") String userName, @Param("startTime") Date startTime,
			@Param("endTime") Date endTime, @Param("dailiLevel") String dailiLevel);

	/**
	 * 根据业务系统和用户名查询是否存在该用户
	 * 
	 * @param user
	 * @return
	 */
	public User getUserByByzSystem(@Param("query") User user);

	/**
	 * 修改该用户下的所有下级用户的配额信息或星级数
	 * 
	 * @param user
	 * @return
	 */
	public int updateUserAllLowerLevelsByName(User user);

	/**
	 * 删除游客账户
	 * 
	 * @param id
	 * @return
	 */
	public int delTouristUser(@Param("id") Integer id);

	/**
	 * 批量删除游客账户
	 * 
	 * @param user
	 * @return
	 */
	public int delAllTouristUser(User user);

	/**
	 * 查找符合条件的团队注册人数
	 * 
	 * @param query
	 * @return
	 */
	public Integer getTeamUserRegistersByQueryCount(@Param("query") TeamUserQuery query);

	/**
	 * 查找符合条件的团队首冲人数
	 * 
	 * @param query
	 * @return
	 */
	public Integer getTeamUserfirstrechargeTimeByQueryCount(@Param("query") TeamUserQuery query);

	/**
	 * 统计用户的有效
	 * 
	 * @param query
	 * @return
	 */
	public Integer getTeamUserLotteryByQueryCount(@Param("query") OrderQuery query);

	/**
	 * 查询符合条件的记录条数
	 * 
	 * @param query
	 * @return
	 */
	public Integer getAllUsersByQueryFristRechargeTimePageCount(@Param("query") UserQuery query);

	/**
	 * 分页条件查询登陆日志
	 * 
	 * @return
	 */
	public List<User> getAllUsersByQueryFristRechargeTimePage(@Param("query") UserQuery query, @Param("startNum") Integer startNum, @Param("pageNum") Integer pageNum);

	public Integer getAllUserPhoneByPhone(@Param("query") User query);

	/**
	 * 根据条件查找用户列表
	 * 
	 * @param query
	 * @return
	 */
	public List<User> getUsersByQuery(UserQuery query);

	/**
	 * 批量处理用户头像
	 * @param users
	 * @return
	 */
	public int updateUserHeadImg(List<User> users);
	
	public List<User> selectAllByUserNames(@Param("bizSystem") String bizSystem,@Param("userName") List<String> userName);

}