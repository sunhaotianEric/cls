package com.team.lottery.mapper.bizsystemconfig;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.BizSystemConfig;

public interface BizSystemConfigMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BizSystemConfig record);

    int insertSelective(BizSystemConfig record);

    BizSystemConfig selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BizSystemConfig record);

    int updateByPrimaryKey(BizSystemConfig record);
    
    /**
     * 在sql语句中初始化新增系统配置
     * @param query
     */
    void initBizSystemConfigBySql(@Param("query")BizSystemConfig query);
    
    /**
     * 查询符合条件的记录条数
     * @param query
     * @return
     */
    public Integer getAllBizSystemConfigByQueryPageCount(@Param("query")BizSystemConfig query);
    
    /**
     * 查询验证 config_key 唯一
     * @param query
     * @return
     */
    public Integer checkConfigKeyIsExist(@Param("query")BizSystemConfig query);
    
    
    /**
     * 分页条件
     * @return
     */
    public  List<BizSystemConfig> getAllBizSystemConfigByQueryPage(@Param("query")BizSystemConfig query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
	
    /**
     * 根据条件查询
     * @return
     */
    public  List<BizSystemConfig> getAllBizSystemConfig(@Param("bizSystem")String bizSystem);
    
    /**
     * 获取所有的bizsystem
     * @return
     */
    public List<BizSystemConfig> getAllBizSystems();
    
    /**
     * 模糊条件查询
     * @param likeValue
     * @return
     */
    public List<BizSystemConfig> getBizSystemConfigByLikeValue(@Param("likeValue")String likeValue,@Param("bizSystem")String bizSystem);
    
    /**
     * configKey条件查询
     * @param likeValue
     * @return
     */
    public BizSystemConfig getBizSystemConfigByKey(@Param("configKey")String configKey,@Param("bizSystem")String bizSystem);
    
}