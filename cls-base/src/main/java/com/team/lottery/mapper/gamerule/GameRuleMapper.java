package com.team.lottery.mapper.gamerule;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.GameRule;

public interface GameRuleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GameRule record);

    int insertSelective(GameRule record);

    GameRule selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GameRule record);

    int updateByPrimaryKeyWithBLOBs(GameRule record);

    int updateByPrimaryKey(GameRule record);
    
    /**
     * 分页条件
     * @param query
     * @param startNum
     * @param pageNum
     * @return
     */
	List<GameRule> getAllGameRule(@Param("query")GameRule query, @Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
	
	/**
	 * 查询所有的
	 * @param query
	 * @return
	 */
	public int  getGameRuleCount(@Param("query")GameRule query); 
	
	/**
	 * 
	 * @param lotteryType
	 * @return
	 */
	public GameRule getRuleByType(String lotteryType);
	
}