package com.team.lottery.mapper.emailsendrecord;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.EmailSendRecord;

public interface EmailSendRecordMapper {
    int deleteByPrimaryKey(Long id);

    int insert(EmailSendRecord record);

    int insertSelective(EmailSendRecord record);

    EmailSendRecord selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(EmailSendRecord record);

    int updateByPrimaryKey(EmailSendRecord record);

	List<EmailSendRecord> queryAllEmailSendRecord(@Param("query")EmailSendRecord record);
	
	/**
     * 分页条件
     * @param query
     * @param startNum
     * @param pageNum
     * @return
     */
	List<EmailSendRecord> getAllEmailSendRecord(@Param("query")EmailSendRecord query, @Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
	
	/**
	 * 查询所有的
	 * @param query
	 * @return
	 */
	public int  getEmailSendRecordCount(@Param("query")EmailSendRecord query); 
	
	
}