package com.team.lottery.mapper.lotteryswitch;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.LotterySwitch;

public interface LotterySwitchMapper {
	int deleteByPrimaryKey(Long id);

	int insert(LotterySwitch record);

	int insertSelective(LotterySwitch record);

	LotterySwitch selectByPrimaryKey(Long id);

	int updateByPrimaryKeySelective(LotterySwitch record);

	int updateByPrimaryKey(LotterySwitch record);

	/**
	 * 根据业务系统查询对应的彩种开关.
	 * 
	 * @param bizSystem 业务系统.
	 * @return
	 */
	List<LotterySwitch> getAllLotterySwitchsByBizSystem(@Param("bizSystem")String bizSystem);
	
	/**
	 * 获取所有系统的彩种开关.
	 * 
	 * @return
	 */
	List<LotterySwitch> getAllLotterySwitchs();

	/**
	 * 新增业务系统添加对应的业务系统彩种开关.
	 * 
	 * @param bizSystem
	 * @param username
	 */
	void insertBizSystemLotterySwitchBySql(@Param("bizSystem")String bizSystem, @Param("adminName")String adminName);

}