package com.team.lottery.mapper.alarminfo;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.AlarmInfo;

public interface AlarmInfoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(AlarmInfo record);

    int insertSelective(AlarmInfo record);

    AlarmInfo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(AlarmInfo record);

    int updateByPrimaryKeyWithBLOBs(AlarmInfo record);

    int updateByPrimaryKey(AlarmInfo record);
    
    int getAlarminfoPageCount(@Param("query")AlarmInfo record);
    
    List<AlarmInfo> getAlarminfoPage(@Param("query")AlarmInfo record,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);

	List<AlarmInfo> queryAlarmInfoBytime(@Param("query")AlarmInfo record);
}