package com.team.lottery.mapper.awardmodelconfig;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.AwardModelConfig;

public interface AwardModelConfigMapper {
    int deleteByPrimaryKey(Long id);

    int insert(AwardModelConfig record);

    int insertSelective(AwardModelConfig record);

    AwardModelConfig selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(AwardModelConfig record);

    int updateByPrimaryKey(AwardModelConfig record);
    
    /**
     * 查找所有的奖金模式配置数据
     * @return
     */
    List<AwardModelConfig> getAllAwardModelConfigs();
    /**
     * 查找奖金模式配置数据
     * @return
     */
     List<AwardModelConfig> getAwardModelConfigByQuery(@Param("query")AwardModelConfig query);
}