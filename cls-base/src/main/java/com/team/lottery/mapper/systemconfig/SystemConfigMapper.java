package com.team.lottery.mapper.systemconfig;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.SystemConfig;

public interface SystemConfigMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SystemConfig record);

    int insertSelective(SystemConfig record);

    SystemConfig selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SystemConfig record);

    int updateByPrimaryKey(SystemConfig record);
    
    /**
     * 查询符合条件的记录条数
     * @param query
     * @return
     */
    public Integer getAllSystemConfigByQueryPageCount(@Param("query")SystemConfig query);
    
    /**
     * 查询验证 config_key 唯一
     * @param query
     * @return
     */
    public Integer checkConfigKeyIsExist(@Param("query")SystemConfig query);
    
    
    /**
     * 分页条件
     * @return
     */
    public  List<SystemConfig> getAllSystemConfigByQueryPage(@Param("query")SystemConfig query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
	
    /**
     * 查询所有的
     * @return
     */
    public  List<SystemConfig> getAllSystemConfig();
    
    /**
     * 模糊条件查询
     * @param likeValue
     * @return
     */
    public List<SystemConfig> getSystemConfigByLikeValue(String likeValue);
    
    /**
     * configKey条件查询
     * @param likeValue
     * @return
     */
    public SystemConfig getSystemConfigByKey(String configKey);
    
    public List<SystemConfig> getSystemConfig();
    
}