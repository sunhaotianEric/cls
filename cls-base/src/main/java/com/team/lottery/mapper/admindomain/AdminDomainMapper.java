package com.team.lottery.mapper.admindomain;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.AdminDomainQuery;
import com.team.lottery.vo.AdminDomain;

public interface AdminDomainMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(AdminDomain record);

    int insertSelective(AdminDomain record);

    AdminDomain selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(AdminDomain record);

    int updateByPrimaryKeyWithBLOBs(AdminDomain record);

    int updateByPrimaryKey(AdminDomain record);
    
    /**
     * 根据子系统和域名,ip查询
     * @param bizSystem
     * @param domains
     * @return
     */
    AdminDomain getAdminDomainByBizSystem(AdminDomainQuery adminDomainQuery);
    List<AdminDomain> getAllAdminDomain(AdminDomainQuery adminDomainQuery);
    int getAllAdminDomainPageCount(@Param("query")AdminDomainQuery adminDomainQuery);
    
    /**
     * 分页查询域名IP授权管理 
     * @param bizSystem
     * @param domains
     * @param startIndex
     * @param pageSize
     * @return
     */
    List<AdminDomain> getAllAdminDomainPage(@Param("query")AdminDomainQuery adminDomainQuery,@Param("startNum")int startIndex, @Param("pageNum")Integer pageSize);
}