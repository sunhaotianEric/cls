package com.team.lottery.mapper.loginlog;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.DayOnlineTypeCountVo;
import com.team.lottery.extvo.LoginLogQuery;
import com.team.lottery.vo.LoginLog;

public interface LoginLogMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(LoginLog record);

    int insertSelective(LoginLog record);

    LoginLog selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(LoginLog record);

    int updateByPrimaryKey(LoginLog record);
    
    /**
     * 查询符合条件的记录条数
     * @param query
     * @return
     */
    public Integer getAllLoginLogsByQueryPageCount(LoginLogQuery query);
    
    /**
     * 分页条件查询登陆日志
     * @return
     */
	List<LoginLog> getAllLoginLogsByQueryPage(@Param("query")LoginLogQuery query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
	
	/**
     * 删除指定时间内的记录条数
     * @return
     */
	public void delLoginLogsByDay(@Param("bizSystem")String bizSystem,@Param("dayFrom")Date from,@Param("dayTo")Date to);
	
	/**
	 * 查询上次登录的日志(第二次)
	 * @param userId
	 * @return
	 */
	public List<LoginLog> getLastIPAddress(@Param("bizSystem")String bizSytem,@Param("userName")String userName);
	
	/**
	 * 查询每日登录人数(按登录类型统计)
	 * @param query
	 * @return
	 */
	public List<DayOnlineTypeCountVo> getDayLoginTypeCount(@Param("query")LoginLogQuery query);
	
	/**
	 * 查询每日登录人数
	 * @param query
	 * @return
	 */
	public Integer getDayLoginCount(@Param("query")LoginLogQuery query);
	
	
	/**
	 * 查询登录日志
	 * @param query
	 * @return
	 */
	public List<LoginLog> getAllLoginLogs(@Param("query")LoginLogQuery query);
	
	/**
	 * 查询异常登录日志
	 * @param query
	 * @return
	 */
	public List<LoginLog> getLoginLogsByUnusual(@Param("query")LoginLogQuery query);
	
}