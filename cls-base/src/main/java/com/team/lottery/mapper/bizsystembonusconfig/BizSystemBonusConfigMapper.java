package com.team.lottery.mapper.bizsystembonusconfig;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.BizSystemBonusConfig;

public interface BizSystemBonusConfigMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(BizSystemBonusConfig record);

    int insertSelective(BizSystemBonusConfig record);

    BizSystemBonusConfig selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(BizSystemBonusConfig record);

    int updateByPrimaryKey(BizSystemBonusConfig record);
    public List<BizSystemBonusConfig> getBizSystemBonusConfigByCondition(@Param("query")BizSystemBonusConfig query);
    int deleteByPrimaryBizSystem(String bizSystem);
}