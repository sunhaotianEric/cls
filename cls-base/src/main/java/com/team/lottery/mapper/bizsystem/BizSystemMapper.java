package com.team.lottery.mapper.bizsystem;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.vo.BizSystem;

public interface BizSystemMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BizSystem record);

    int insertSelective(BizSystem record);

    BizSystem selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BizSystem record);

    int updateByPrimaryKey(BizSystem record);
    
    /**
     * 查询符合条件的记录条数
     * @param query
     * @return
     */
    public Integer getAllBizSystemByQueryPageCount(@Param("query")BizSystem query);
    /**
     * 统计业务系统的总分红，维护收入
     * @param query
     * @return
     */
    public List<BizSystem> getALLBizSystemIncome(@Param("query")BizSystem query);
    
    /**
     * 分页条件
     * @return
     */
    public List<BizSystem> getAllBizSystemByQueryPage(@Param("query")BizSystem query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
    
    /**
     * 查询所有的业务系统
     * @return
     */
    public List<BizSystem> getAllBizSystem();
    
    /**
     * 查找查询启用中的业务系统 
     * @return
     */
    public  List<BizSystem> getAllEnableBizSystem();
    
    /**
     * 查找bizsystem查出唯一对象 
     * @return
     */
    public  BizSystem  getBizSystemByCondition(@Param("query")BizSystem bizSystem);
}