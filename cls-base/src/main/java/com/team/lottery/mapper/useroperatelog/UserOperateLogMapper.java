package com.team.lottery.mapper.useroperatelog;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.team.lottery.extvo.UserOperateLogQuery;
import com.team.lottery.vo.UserOperateLog;

public interface UserOperateLogMapper {
    int deleteByPrimaryKey(Long id);

    int insert(UserOperateLog record);

    int insertSelective(UserOperateLog record);

    UserOperateLog selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UserOperateLog record);

    int updateByPrimaryKeyWithBLOBs(UserOperateLog record);

    int updateByPrimaryKey(UserOperateLog record);
    
    /**
     * 查询符合条件的记录条数
     * @param query
     * @return
     */
    public Integer getAllUserOperateLogQueryPageCount(UserOperateLogQuery query);
    
    /**
     * 分页条件前台查询登陆日志
     * @return
     */
    public List<UserOperateLog> getAllUserOperateLogQueryPage(@Param("query")UserOperateLogQuery query,@Param("startNum")Integer startNum,@Param("pageNum")Integer pageNum);
}