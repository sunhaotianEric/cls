package com.team.lottery.vo;

import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.util.DateUtil;

public class SmsSendRecord {
	
	//默认类型
	public static final String DEFAULT_TYPE = "DEFAULT_TYPE";
	//忘记密码类型
	public static final String FORGETPWD_TYPE = "FORGETPWD_TYPE";
	//绑定手机类型
	public static final String BIND_PHONE_TYPE = "BIND_PHONE_TYPE";
	//修改绑定手机类型
	public static final String CHANGE_PHONE_TYPE = "CHANGE_PHONE_TYPE";
	//注册类型
	public static final String REG_TYPE = "REG_TYPE";
	
	private Long id;

	private String bizSystem;

	private String mobile;

	private String vcode;

    private Integer userId;

    private String userName;

    private String ip;

    private String type;

    private String sessionId;
    
	private String remark;

	private Date createDate;

	//查询开始时间.
	private Date startDate;

	//查询结束时间.
	private Date endDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem == null ? null : bizSystem.trim();
	}

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip == null ? null : ip.trim();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public String getSessionId() {
        return sessionId;
    }
    
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId == null ? null : sessionId.trim();
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile == null ? null : mobile.trim();
	}

	public String getVcode() {
		return vcode;
	}

	public void setVcode(String vcode) {
		this.vcode = vcode == null ? null : vcode.trim();
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public String getBizSystemName() {
		return this.bizSystem == null ? "" : ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
	}
	
	public String getTypeName(){
		String type="";
		if(this.type.equals(SmsSendRecord.DEFAULT_TYPE)){
			type="默认类型";
		}else if(this.type.equals(SmsSendRecord.FORGETPWD_TYPE)){
			type="忘记密码类型";
		}else if(this.type.equals(SmsSendRecord.BIND_PHONE_TYPE)){
			type="绑定手机类型";
		}else if(this.type.equals(SmsSendRecord.CHANGE_PHONE_TYPE)){
			type="修改绑定手机类型";
		}else if(this.type.equals(SmsSendRecord.REG_TYPE)){
			type="注册类型";
		}
		return type;
	}

	// 获取创建时间时分秒的格式
	public String getCreateDateStr() {
		String dateStr = "";
		if (this.createDate != null) {
			dateStr = DateUtil.parseDate(createDate, "yyyy-MM-dd HH:mm:ss");
		}
		return dateStr;
	}

}