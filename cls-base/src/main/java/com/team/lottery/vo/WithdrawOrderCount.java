package com.team.lottery.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.enums.EFundAutoWithdrawDealStatus;
import com.team.lottery.enums.EFundRechargeStatus;
import com.team.lottery.enums.EFundWithDrawStatus;
import com.team.lottery.util.DateUtils;

public class WithdrawOrderCount {
	
	

	private BigDecimal applyValue;


    private Integer number; //笔数、


	public BigDecimal getApplyValue() {
		return applyValue;
	}


	public void setApplyValue(BigDecimal applyValue) {
		this.applyValue = applyValue;
	}


	public Integer getNumber() {
		return number;
	}


	public void setNumber(Integer number) {
		this.number = number;
	}

	
}