package com.team.lottery.vo;

import java.io.Serializable;
import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.util.DateUtils;

public class Admin implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5051041588018790488L;
	
	//超级管理员
	public static final Integer SUPER_STATE = 0;  
	//管理员
	public static final Integer ADMIN_STATE = 1; 
	//财务管理员
	public static final Integer FINANCE_STATE = 2;
	//客服管理员
	public static final Integer SERVICE_STATE = 3;
	
    private Integer id;
    
    private String bizSystem;

    private String username;

    private String password;
    
    private String operatePassword;
    
    private Integer dataViewSwitch;

    private Integer state; 
    
    private Integer loginLock;

    private Date createDate;
    
    //新加字段 更新
    private Date updateTime;
    
    //新加字段 操作人
    private String updateAdmin;
    
    //扩展字段
    private Integer isOnline; //是否在线
    private String newpassword; //新密码
    private String surepassword; //新密码
    private String checkCode;  //验证码
    private String secretCode; //秘密验证码

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }
    
    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
    
    public String getNewpassword() {
		return newpassword;
	}

	public void setNewpassword(String newpassword) {
		this.newpassword = newpassword;
	}

	public String getCheckCode() {
		return checkCode;
	}

	public void setCheckCode(String checkCode) {
		this.checkCode = checkCode;
	}
	
	public Integer getIsOnline() {
		return isOnline;
	}

	public void setIsOnline(Integer isOnline) {
		this.isOnline = isOnline;
	}
	public String getSecretCode() {
		return secretCode;
	}

	public void setSecretCode(String secretCode) {
		this.secretCode = secretCode;
	}
	
	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getUpdateAdmin() {
		return updateAdmin;
	}

	public void setUpdateAdmin(String updateAdmin) {
		this.updateAdmin = updateAdmin;
	}

	public String getStateStr() {
    	String str = "";
    	if(this.state == 0){
			str = "超级管理员";
    	}else if(this.state == 1){
    		str = "管理员";
    	}else if(this.state == 2){
    		str = "财务管理员";
    	}else if(this.state == 3){
    		str = "客服管理员";
    	}else{
    		throw new RuntimeException("管理员类型少掉了.");
    	}
        return str;
    }

	public String getSurepassword() {
		return surepassword;
	}

	public void setSurepassword(String surepassword) {
		this.surepassword = surepassword;
	}

	public String getOperatePassword() {
		return operatePassword;
	}

	public void setOperatePassword(String operatePassword) {
		this.operatePassword = operatePassword;
	}
	
	public Integer getDataViewSwitch() {
        return dataViewSwitch;
    }

    public void setDataViewSwitch(Integer dataViewSwitch) {
        this.dataViewSwitch = dataViewSwitch;
    }

	public Integer getLoginLock() {
		return loginLock;
	}

	public void setLoginLock(Integer loginLock) {
		this.loginLock = loginLock;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	//创建时间字符串
    public String getCreateDateStr(){
    	return DateUtils.getDateToStrByFormat(this.getCreateDate(), "yyyy/MM/dd HH:mm:ss");
    }
	
    public String getBizSystemName() {
    	
        return this.bizSystem==null?"":ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
    }
}