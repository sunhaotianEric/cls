package com.team.lottery.vo;

import java.util.Date;

public class CodePlan {
    private Long id;

    private String lotteryType;

    private String bizSystem;

    private String codes;
    
    private String codesDesc;

    private String prostate;

    private String expect;

    private String openCode;

    private Date createdDate;

    private Date updateDate;

    private Integer enabled;

    private String playkind;
    
    private String playkindDes;
    
    private int expectNumber;
    
    private String prostateDesc;
    
    private Integer codesNum;
    
    private String expectsPlan;
    
    private String expectsPeriod;
    
    private String expectShort;
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLotteryType() {
		return lotteryType;
	}

	public void setLotteryType(String lotteryType) {
		this.lotteryType = lotteryType;
	}

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}

	public String getCodes() {
		return codes;
	}

	public void setCodes(String codes) {
		this.codes = codes;
	}

	public String getCodesDesc() {
		return codesDesc;
	}

	public void setCodesDesc(String codesDesc) {
		this.codesDesc = codesDesc;
	}

	public String getProstate() {
		return prostate;
	}

	public void setProstate(String prostate) {
		this.prostate = prostate;
	}

	public String getExpect() {
		return expect;
	}

	public void setExpect(String expect) {
		this.expect = expect;
	}

	public String getOpenCode() {
		return openCode;
	}

	public void setOpenCode(String openCode) {
		this.openCode = openCode;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

	public String getPlaykind() {
		return playkind;
	}

	public void setPlaykind(String playkind) {
		this.playkind = playkind;
	}

	public String getPlaykindDes() {
		return playkindDes;
	}

	public void setPlaykindDes(String playkindDes) {
		this.playkindDes = playkindDes;
	}

	public int getExpectNumber() {
		return expectNumber;
	}

	public void setExpectNumber(int expectNumber) {
		this.expectNumber = expectNumber;
	}

	public String getProstateDesc() {
		return prostateDesc;
	}

	public void setProstateDesc(String prostateDesc) {
		this.prostateDesc = prostateDesc;
	}

	public Integer getCodesNum() {
		return codesNum;
	}

	public void setCodesNum(Integer codesNum) {
		this.codesNum = codesNum;
	}

	public String getExpectsPlan() {
		return expectsPlan;
	}

	public void setExpectsPlan(String expectsPlan) {
		this.expectsPlan = expectsPlan;
	}

	public String getExpectsPeriod() {
		return expectsPeriod;
	}

	public void setExpectsPeriod(String expectsPeriod) {
		this.expectsPeriod = expectsPeriod;
	}

	public String getExpectShort() {
		return expectShort;
	}

	public void setExpectShort(String expectShort) {
		this.expectShort = expectShort;
	}
}