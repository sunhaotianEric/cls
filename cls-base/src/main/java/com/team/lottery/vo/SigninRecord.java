package com.team.lottery.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.util.DateUtils;

public class SigninRecord {
    private Integer id;

    private String bizSystem;

    private String userName;

    private Integer userId;

    private Date signinTime;
    
    private Date signinDate;

    private Integer continueSigninDays;

    private BigDecimal money;
    
    //扩展字段
    private String signState;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getSigninTime() {
        return signinTime;
    }

    public void setSigninTime(Date signinTime) {
        this.signinTime = signinTime;
    }
    
    public Date getSigninDate() {
        return signinDate;
    }

    public void setSigninDate(Date signinDate) {
        this.signinDate = signinDate;
    }

    public Integer getContinueSigninDays() {
        return continueSigninDays;
    }

    public void setContinueSigninDays(Integer continueSigninDays) {
        this.continueSigninDays = continueSigninDays;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }
    
    public String getSignState() {
		return signState;
	}

	public void setSignState(String signState) {
		this.signState = signState;
	}

	//获取时间字符串
  	public String getSigninTimeStr(){
  		if(this.getSigninTime() != null){
  	    	return DateUtils.getDateToStrByFormat(this.getSigninTime(), "yyyy/MM/dd HH:mm:ss");
  		}
  		return "";
  	}
    /**
	 * 业务系统
	 * 
	 */
	public String getBizSystemName() {
		return ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
	}
}