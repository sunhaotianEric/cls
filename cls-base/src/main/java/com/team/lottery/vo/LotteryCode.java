package com.team.lottery.vo;

import java.io.Serializable;
import java.util.Date;

import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.DateUtils;
import com.team.lottery.util.StringUtils;

public class LotteryCode implements Serializable{
   
	private static final long serialVersionUID = 1556972216065023622L;

	private Integer id;

    //开奖期号
    private String lotteryNum;

    private String numInfo1;

    private String numInfo2;

    private String numInfo3;

    private String numInfo4;

    private String numInfo5;

    private String numInfo6;

    private String numInfo7;

    private String numInfo8;

    private String numInfo9;

    private String numInfo10;

    private String numInfo11;

    private String numInfo12;

    private String numInfo13;

    private String numInfo14;

    private Date kjtime;

    private Date addtime;

    private String lotteryName;

    private Integer state;
    
    private Integer prostateDeal;

    //扩展属性
    private String codesStr; 
    private String lotteryType;//投注类型
    private Integer rowIndex;  //查询的期号序号
    //彩种类型名称
    private String lotteryTypeDes;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLotteryNum() {
        return lotteryNum;
    }

    public void setLotteryNum(String lotteryNum) {
        this.lotteryNum = lotteryNum == null ? null : lotteryNum.trim();
    }

    public String getNumInfo1() {
        return numInfo1;
    }

    public void setNumInfo1(String numInfo1) {
        this.numInfo1 = numInfo1 == null ? null : numInfo1.trim();
    }

    public String getNumInfo2() {
        return numInfo2;
    }

    public void setNumInfo2(String numInfo2) {
        this.numInfo2 = numInfo2 == null ? null : numInfo2.trim();
    }

    public String getNumInfo3() {
        return numInfo3;
    }

    public void setNumInfo3(String numInfo3) {
        this.numInfo3 = numInfo3 == null ? null : numInfo3.trim();
    }

    public String getNumInfo4() {
        return numInfo4;
    }

    public void setNumInfo4(String numInfo4) {
        this.numInfo4 = numInfo4 == null ? null : numInfo4.trim();
    }

    public String getNumInfo5() {
        return numInfo5;
    }

    public void setNumInfo5(String numInfo5) {
        this.numInfo5 = numInfo5 == null ? null : numInfo5.trim();
    }

    public String getNumInfo6() {
        return numInfo6;
    }

    public void setNumInfo6(String numInfo6) {
        this.numInfo6 = numInfo6 == null ? null : numInfo6.trim();
    }

    public String getNumInfo7() {
        return numInfo7;
    }

    public void setNumInfo7(String numInfo7) {
        this.numInfo7 = numInfo7 == null ? null : numInfo7.trim();
    }

    public String getNumInfo8() {
        return numInfo8;
    }

    public void setNumInfo8(String numInfo8) {
        this.numInfo8 = numInfo8 == null ? null : numInfo8.trim();
    }

    public String getNumInfo9() {
        return numInfo9;
    }

    public void setNumInfo9(String numInfo9) {
        this.numInfo9 = numInfo9 == null ? null : numInfo9.trim();
    }

    public String getNumInfo10() {
        return numInfo10;
    }

    public void setNumInfo10(String numInfo10) {
        this.numInfo10 = numInfo10 == null ? null : numInfo10.trim();
    }

    public String getNumInfo11() {
        return numInfo11;
    }

    public void setNumInfo11(String numInfo11) {
        this.numInfo11 = numInfo11 == null ? null : numInfo11.trim();
    }

    public String getNumInfo12() {
        return numInfo12;
    }

    public void setNumInfo12(String numInfo12) {
        this.numInfo12 = numInfo12 == null ? null : numInfo12.trim();
    }

    public String getNumInfo13() {
        return numInfo13;
    }

    public void setNumInfo13(String numInfo13) {
        this.numInfo13 = numInfo13 == null ? null : numInfo13.trim();
    }

    public String getNumInfo14() {
        return numInfo14;
    }

    public void setNumInfo14(String numInfo14) {
        this.numInfo14 = numInfo14 == null ? null : numInfo14.trim();
    }

    public Date getKjtime() {
        return kjtime;
    }

    public void setKjtime(Date kjtime) {
        this.kjtime = kjtime;
    }

    public Date getAddtime() {
        return addtime;
    }

    public void setAddtime(Date addtime) {
        this.addtime = addtime;
    }

    public String getLotteryName() {
        return lotteryName;
    }

    public void setLotteryName(String lotteryName) {
        this.lotteryName = lotteryName == null ? null : lotteryName.trim();
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

	public Integer getProstateDeal() {
		return prostateDeal;
	}

	public void setProstateDeal(Integer prostateDeal) {
		this.prostateDeal = prostateDeal;
	}

	public String getCodesStr() {
		return codesStr;
	}

	public void setCodesStr(String codesStr) {
		this.codesStr = codesStr;
	}
	
	public String getLotteryType() {
		return lotteryType;
	}

	public void setLotteryType(String lotteryType) {
		this.lotteryType = lotteryType;
	}

	public String getKjtimeStr(){
    	return DateUtils.getDateToStrByFormat(this.getKjtime(), "yyyy/MM/dd HH:mm:ss");
    }
	
	public String getAddtimeStr(){
		return DateUtils.getDateToStrByFormat(this.getAddtime(), "yyyy/MM/dd HH:mm:ss");
	}

	public Integer getRowIndex() {
		return rowIndex;
	}

	public void setRowIndex(Integer rowIndex) {
		this.rowIndex = rowIndex;
	}
	
	//获取彩种类型名称
	public String getLotteryTypeDes() {
		if(lotteryTypeDes != null && !"".equals(lotteryTypeDes)) {
			return lotteryTypeDes;
		}
		if(lotteryName != null && !"".equals(lotteryName)) {
			ELotteryKind kind = ELotteryKind.valueOf(lotteryName);
			if(kind != null) {
				return kind.getDescription();
			}
		}
		return lotteryTypeDes;
	}

	public void setLotteryTypeDes(String lotteryTypeDes) {
		this.lotteryTypeDes = lotteryTypeDes;
	}

	/**
	 * 获取开奖号码的拼接
	 * @return
	 */
	public String getOpencodeStr() {
		StringBuffer sb = new StringBuffer();
		if(!StringUtils.isEmpty(numInfo1)) {
			sb.append(numInfo1.trim()).append(ConstantUtil.OPENCODE_PLIT);
		}
		if(!StringUtils.isEmpty(numInfo2)) {
			sb.append(numInfo2.trim()).append(ConstantUtil.OPENCODE_PLIT);
		}
		if(!StringUtils.isEmpty(numInfo3)) {
			sb.append(numInfo3.trim()).append(ConstantUtil.OPENCODE_PLIT);
		}
		if(!StringUtils.isEmpty(numInfo4)) {
			sb.append(numInfo4.trim()).append(ConstantUtil.OPENCODE_PLIT);
		}
		if(!StringUtils.isEmpty(numInfo5)) {
			sb.append(numInfo5.trim()).append(ConstantUtil.OPENCODE_PLIT);
		}
		if(!StringUtils.isEmpty(numInfo6)) {
			sb.append(numInfo6.trim()).append(ConstantUtil.OPENCODE_PLIT);
		}
		if(!StringUtils.isEmpty(numInfo7)) {
			sb.append(numInfo7.trim()).append(ConstantUtil.OPENCODE_PLIT);
		}
		if(!StringUtils.isEmpty(numInfo8)) {
			sb.append(numInfo8.trim()).append(ConstantUtil.OPENCODE_PLIT);
		}
		if(!StringUtils.isEmpty(numInfo9)) {
			sb.append(numInfo9.trim()).append(ConstantUtil.OPENCODE_PLIT);
		}
		if(!StringUtils.isEmpty(numInfo10)) {
			sb.append(numInfo10.trim()).append(ConstantUtil.OPENCODE_PLIT);
		}
		if(!StringUtils.isEmpty(numInfo11)) {
			sb.append(numInfo11.trim()).append(ConstantUtil.OPENCODE_PLIT);
		}
		if(!StringUtils.isEmpty(numInfo12)) {
			sb.append(numInfo12.trim()).append(ConstantUtil.OPENCODE_PLIT);
		}
		if(!StringUtils.isEmpty(numInfo13)) {
			sb.append(numInfo13.trim()).append(ConstantUtil.OPENCODE_PLIT);
		}
		if(!StringUtils.isEmpty(numInfo14)) {
			sb.append(numInfo14.trim()).append(ConstantUtil.OPENCODE_PLIT);
		}
		
		if(sb.length() != 0){
			sb.deleteCharAt(sb.length() - 1);
		}
		
		//删除最后一个分隔符
		return sb.toString();
	}
	
}