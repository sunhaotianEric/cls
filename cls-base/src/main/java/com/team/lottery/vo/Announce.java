package com.team.lottery.vo;

import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.util.DateUtils;

public class Announce {
    private Integer id;
    
    private String bizSystem;

    private String title;

    private Date addtime;

    private Integer hit;
    
    private Integer yetshow;

    private String content;
    
    private String updateAdmin;
    
    private Integer topping;

    private Integer showDays;

    private Date endShowDate;
    
    public Integer getTopping() {
		return topping;
	}

	public void setTopping(Integer topping) {
		this.topping = topping;
	}

	public Integer getShowDays() {
		return showDays;
	}

	public void setShowDays(Integer showDays) {
		this.showDays = showDays;
	}

	public Date getEndShowDate() {
		return endShowDate;
	}

	public void setEndShowDate(Date endShowDate) {
		this.endShowDate = endShowDate;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public Date getAddtime() {
        return addtime;
    }

    public void setAddtime(Date addtime) {
        this.addtime = addtime;
    }

    public Integer getHit() {
        return hit;
    }

    public void setHit(Integer hit) {
        this.hit = hit;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }
    
    public String getLogtimeStr(){
    	return DateUtils.getDateToStrByFormat(this.getAddtime(), "yyyy/MM/dd HH:mm:ss");
    }
    
    public String getShowtimeStr(){
    	return DateUtils.getDateToStrByFormat(this.getEndShowDate(), "yyyy/MM/dd");
    }

	public Integer getYetshow() {
		return yetshow;
	}

	public void setYetshow(Integer yetshow) {
		this.yetshow = yetshow;
	}


	public String getUpdateAdmin() {
		return updateAdmin;
	}

	public void setUpdateAdmin(String updateAdmin) {
		this.updateAdmin = updateAdmin;
	}
	
	public String getBizSystemName() {
        return ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
    }
	
	public String getLogdateStr(){
    	return DateUtils.getDateToStrByFormat(this.getEndShowDate(), "yyyy/MM/dd HH:mm:ss");
    }
    
    public String getShowdateStr(){
    	return DateUtils.getDateToStrByFormat(this.getEndShowDate(), "yyyy/MM/dd");
    }
	
}