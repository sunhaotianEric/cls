package com.team.lottery.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;

public class LevelSystem {
    private Integer id;

    private String bizSystem;

    private Integer level;

    private String levelName;

    private String levelImgPath;

    private Integer point;

    private BigDecimal promotionAward;

    private BigDecimal skipAward;

    private Date createDate;

    private Date updateTime;

    private String updateAdmin;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName == null ? null : levelName.trim();
    }

    public String getLevelImgPath() {
        return levelImgPath;
    }

    public void setLevelImgPath(String levelImgPath) {
        this.levelImgPath = levelImgPath == null ? null : levelImgPath.trim();
    }

    public Integer getPoint() {
        return point;
    }

    public void setPoint(Integer point) {
        this.point = point;
    }

    public BigDecimal getPromotionAward() {
        return promotionAward;
    }

    public void setPromotionAward(BigDecimal promotionAward) {
        this.promotionAward = promotionAward;
    }

    public BigDecimal getSkipAward() {
        return skipAward;
    }

    public void setSkipAward(BigDecimal skipAward) {
        this.skipAward = skipAward;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

	public String getUpdateAdmin() {
		return updateAdmin;
	}

	public void setUpdateAdmin(String updateAdmin) {
		this.updateAdmin = updateAdmin;
	}
	public String getBizSystemName() {
        return this.bizSystem==null?"":ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
    }

}