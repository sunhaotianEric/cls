package com.team.lottery.vo;

import java.util.Date;

import com.team.lottery.util.DateUtils;

public class AdminSendNote {
    private Long id;

    private Integer fromAdminId;

    private String fromAdminName;

    private String fromBizSystem;

    private String toAdminIds;

    private String toAdminNames;

    private String sub;

    private Integer recvCount;

    private String readedAdminIds;

    private Integer readedCount;

    private Date createdDate;

    private Date updateDate;

    private String content;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getFromAdminId() {
        return fromAdminId;
    }

    public void setFromAdminId(Integer fromAdminId) {
        this.fromAdminId = fromAdminId;
    }

    public String getFromAdminName() {
        return fromAdminName;
    }

    public void setFromAdminName(String fromAdminName) {
        this.fromAdminName = fromAdminName == null ? null : fromAdminName.trim();
    }

    public String getFromBizSystem() {
        return fromBizSystem;
    }

    public void setFromBizSystem(String fromBizSystem) {
        this.fromBizSystem = fromBizSystem == null ? null : fromBizSystem.trim();
    }

    public String getToAdminIds() {
        return toAdminIds;
    }

    public void setToAdminIds(String toAdminIds) {
        this.toAdminIds = toAdminIds == null ? null : toAdminIds.trim();
    }

    public String getToAdminNames() {
        return toAdminNames;
    }

    public void setToAdminNames(String toAdminNames) {
        this.toAdminNames = toAdminNames == null ? null : toAdminNames.trim();
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub == null ? null : sub.trim();
    }

    public Integer getRecvCount() {
        return recvCount;
    }

    public void setRecvCount(Integer recvCount) {
        this.recvCount = recvCount;
    }

    public String getReadedAdminIds() {
        return readedAdminIds;
    }

    public void setReadedAdminIds(String readedAdminIds) {
        this.readedAdminIds = readedAdminIds == null ? null : readedAdminIds.trim();
    }

    public Integer getReadedCount() {
        return readedCount;
    }

    public void setReadedCount(Integer readedCount) {
        this.readedCount = readedCount;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }
    //创建时间字符串
    public String getCreateDateStr(){
    	return DateUtils.getDateToStrByFormat(this.getCreatedDate(), "yyyy年MM月dd日 HH:mm:ss");
    }
    
    //创建时间字符串
    public String getUpdateDateStr(){
    	return DateUtils.getDateToStrByFormat(this.getUpdateDate(), "yyyy年MM月dd日 HH:mm:ss");
    }
}