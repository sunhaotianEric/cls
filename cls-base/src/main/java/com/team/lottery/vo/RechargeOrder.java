package com.team.lottery.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.enums.EFundPayType;
import com.team.lottery.enums.EFundRechargeStatus;
import com.team.lottery.enums.EFundRefType;
import com.team.lottery.enums.EFundWithDrawStatus;
import com.team.lottery.util.DateUtils;

/**
 * 充值订单对象
 * 
 * @author Jamine
 *
 */
public class RechargeOrder {
	
	// 表中对应的id
	private Long id;
	// 订单号.
	private String serialNumber;
	// 业务系统
	private String bizSystem;
	// 用户ID
	private Integer userId;
	// 用户名
	private String userName;
	// 申请金额
	private BigDecimal applyValue;
	// 到账金额
	private BigDecimal accountValue;
	// 充值设置id
	private Long rechargeConfigId;
	// 充值方式名称
    private String rechargeWayName;
	// 支付方式.
	private String payType;
	// 支付类型.
	private String refType;
	// 支付方式描述
	private String operateDes;
	// 第三方支付类型
	private String thirdPayType;
	// 第三方配置id
	private Long chargePayId;
	// 订单来源(PC或者手机)
	private String fromType;
	// 订单状态.
	private String dealStatus;
	// 备注.
	private String remark;
	// 转账payband关联id
	private Long payBankId;
	// 附言.
	private String postscript;
	// 银行名称
	private String bankName;
	// 银行类型.
	private String bankType;
	// 银行支行名称.
	private String subbranchName;
	// 银行账户名称.
	private String bankAccountName;
	// 银行卡号.
	private String bankCardNum;
	// 创建时间.
	private Date createdDate;
	// 操作人.
	private String updateAdmin;
	// 更新时间.
	private Date updateDate;
	
	private BigDecimal rechargeGiftScale;

    private BigDecimal rechargeGiftValue;
    
    private String thirdPayTypeDesc;
	
	 //扩展属性
    private String sign;
    private Long timeDifference; //时间差
    private String barcodeUrl; //二维码图片地址
    private Integer num; //笔数、人数


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber == null ? null : serialNumber.trim();
	}

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem == null ? null : bizSystem.trim();
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName == null ? null : userName.trim();
	}

	public BigDecimal getApplyValue() {
		return applyValue == null ? BigDecimal.ZERO :applyValue;
	}

	public void setApplyValue(BigDecimal applyValue) {
		this.applyValue = applyValue  == null ? BigDecimal.ZERO :applyValue;
	}

	public BigDecimal getAccountValue() {
		return accountValue == null ? BigDecimal.ZERO :accountValue;
	}

	public void setAccountValue(BigDecimal accountValue) {
		this.accountValue = accountValue == null ? BigDecimal.ZERO :accountValue;
	}
	
	public Long getRechargeConfigId() {
        return rechargeConfigId;
    }

    public void setRechargeConfigId(Long rechargeConfigId) {
        this.rechargeConfigId = rechargeConfigId;
    }

    public String getRechargeWayName() {
        return rechargeWayName;
    }

    public void setRechargeWayName(String rechargeWayName) {
        this.rechargeWayName = rechargeWayName == null ? null : rechargeWayName.trim();
    }

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType == null ? null : payType.trim();
	}

	public String getRefType() {
		return refType;
	}

	public void setRefType(String refType) {
		this.refType = refType == null ? null : refType.trim();
	}

	public String getThirdPayType() {
		return thirdPayType;
	}

	public void setThirdPayType(String thirdPayType) {
		this.thirdPayType = thirdPayType == null ? null : thirdPayType.trim();
	}

	public Long getChargePayId() {
		return chargePayId;
	}

	public void setChargePayId(Long chargePayId) {
		this.chargePayId = chargePayId;
	}

	public String getFromType() {
		return fromType;
	}

	public void setFromType(String fromType) {
		this.fromType = fromType == null ? null : fromType.trim();
	}

	public String getOperateDes() {
		return operateDes;
	}

	public void setOperateDes(String operateDes) {
		this.operateDes = operateDes == null ? null : operateDes.trim();
	}

	public String getPostscript() {
		return postscript;
	}

	public void setPostscript(String postscript) {
		this.postscript = postscript == null ? null : postscript.trim();
	}

	public String getDealStatus() {
		return dealStatus;
	}

	public void setDealStatus(String dealStatus) {
		this.dealStatus = dealStatus == null ? null : dealStatus.trim();
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}
	
	public Long getPayBankId() {
        return payBankId;
    }

    public void setPayBankId(Long payBankId) {
        this.payBankId = payBankId;
    }

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName == null ? null : bankName.trim();
	}

	public String getBankType() {
		return bankType;
	}

	public void setBankType(String bankType) {
		this.bankType = bankType == null ? null : bankType.trim();
	}

	public String getSubbranchName() {
		return subbranchName;
	}

	public void setSubbranchName(String subbranchName) {
		this.subbranchName = subbranchName == null ? null : subbranchName.trim();
	}

	public String getBankAccountName() {
		return bankAccountName;
	}

	public void setBankAccountName(String bankAccountName) {
		this.bankAccountName = bankAccountName == null ? null : bankAccountName.trim();
	}

	public String getBankCardNum() {
		return bankCardNum;
	}

	public void setBankCardNum(String bankCardNum) {
		this.bankCardNum = bankCardNum == null ? null : bankCardNum.trim();
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdateAdmin() {
		return updateAdmin;
	}

	public void setUpdateAdmin(String updateAdmin) {
		this.updateAdmin = updateAdmin == null ? null : updateAdmin.trim();
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public Long getTimeDifference() {
		return timeDifference;
	}

	public void setTimeDifference(Long timeDifference) {
		this.timeDifference = timeDifference;
	}

	public String getBarcodeUrl() {
		return barcodeUrl;
	}

	public void setBarcodeUrl(String barcodeUrl) {
		this.barcodeUrl = barcodeUrl;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public BigDecimal getRechargeGiftScale() {
		return rechargeGiftScale;
	}

	public void setRechargeGiftScale(BigDecimal rechargeGiftScale) {
		this.rechargeGiftScale = rechargeGiftScale;
	}

	public BigDecimal getRechargeGiftValue() {
		return rechargeGiftValue;
	}

	public void setRechargeGiftValue(BigDecimal rechargeGiftValue) {
		this.rechargeGiftValue = rechargeGiftValue;
	}
	
	public String getThirdPayTypeDesc() {
		return thirdPayTypeDesc;
	}

	public void setThirdPayTypeDesc(String thirdPayTypeDesc) {
		this.thirdPayTypeDesc = thirdPayTypeDesc;
	}

	// 获取时间字符串
	public String getBizSystemName() {
		return this.bizSystem == null ? "" : ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
	}

	// 获取创建时间字符串
	public String getCreatedDateStr() {
		if (this.getCreatedDate() != null) {
			return DateUtils.getDateToStrByFormat(this.getCreatedDate(), "yyyy/MM/dd HH:mm:ss");
		}
		return "";
	}

	// 获取更新时间字符串
	public String getUpdateDateStr() {
		if (this.getUpdateDate() != null) {
			return DateUtils.getDateToStrByFormat(this.getUpdateDate(), "yyyy/MM/dd HH:mm:ss");
		}
		return "";
	}

	/**
	 * 状态描述
	 * 
	 * @return
	 */
	public String getStatusStr() {
		if (this.getDealStatus() != null) {
			String statuss = this.getDealStatus();
			EFundRechargeStatus[] rechargeOrderStatuss = EFundRechargeStatus.values();
			for (EFundRechargeStatus orderStatus : rechargeOrderStatuss) {
				if (statuss.equals(orderStatus.name())) {
					return orderStatus.getDescription();
				}
			}
			EFundWithDrawStatus[] withDrawStatuss = EFundWithDrawStatus.values();
			for (EFundWithDrawStatus withdrawOrderStatus : withDrawStatuss) {
				if (statuss.equals(withdrawOrderStatus.name())) {
					return withdrawOrderStatus.getDescription();
				}
			}
		}
		return "";
	}

	/**
	 * 订单类型描述
	 * 
	 * @return
	 */
	public String getRefTypeStr() {
		if (this.getRefType() != null) {
			String refType = this.getRefType();
			if (refType.equals("TRANSFER")) {
				return EFundRefType.TRANSFER.getDescription();
			} else {
				/*EFundThirdPayType[] eFundThirdPayType = EFundThirdPayType.values();
				for (EFundThirdPayType type : eFundThirdPayType) {
					if (this.getThirdPayType().equals(type.name())) {
						return type.getDescription();
					}
				}*/
				return this.getThirdPayTypeDesc();
			}

		}
		return "";
	}
	
	/**
     * 冲值类别
     * @return
     */
    public String getPayTypeStr(){
    	if(this.getPayType() != null){
    		String payType = this.getPayType();
    		EFundPayType[] eFundPayType = EFundPayType.values();
    		for(EFundPayType type : eFundPayType){
    			if(payType.equals(type.name())){
    				return type.getDescription();
    			}
    		}
    		
    	}
		return "";
    }
    
    /**
     * 第三方充值类型
     * @return
     */
    public String getThirdPayTypeStr(){
    	if(this.getThirdPayType() != null){
    		/*String thirdPay = this.getThirdPayType();
    		EFundThirdPayType[] eFundThirdPayType = EFundThirdPayType.values();
    		for(EFundThirdPayType thirdPayType : eFundThirdPayType){
    			if(thirdPay.equals(thirdPayType.name())){
    				return thirdPayType.getDescription();
    			}
    		}*/
    		return this.getThirdPayTypeDesc();
    	}
		return "";
    }

}