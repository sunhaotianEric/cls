package com.team.lottery.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;

public class QuotaControl {
    private Long id;

    private String bizSystem;

    private BigDecimal sscRebate;

    private Integer openEqualLevelNum;

    private Integer openLowerLevelNum;

    private Date createTime;

    private String createAdmin;

    private Date updateTime;

    private String updateAdmin;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public BigDecimal getSscRebate() {
        return sscRebate;
    }

    public void setSscRebate(BigDecimal sscRebate) {
        this.sscRebate = sscRebate;
    }

    public Integer getOpenEqualLevelNum() {
        return openEqualLevelNum;
    }

    public void setOpenEqualLevelNum(Integer openEqualLevelNum) {
        this.openEqualLevelNum = openEqualLevelNum;
    }

    public Integer getOpenLowerLevelNum() {
        return openLowerLevelNum;
    }

    public void setOpenLowerLevelNum(Integer openLowerLevelNum) {
        this.openLowerLevelNum = openLowerLevelNum;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateAdmin() {
        return createAdmin;
    }

    public void setCreateAdmin(String createAdmin) {
        this.createAdmin = createAdmin == null ? null : createAdmin.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateAdmin() {
        return updateAdmin;
    }

    public void setUpdateAdmin(String updateAdmin) {
        this.updateAdmin = updateAdmin == null ? null : updateAdmin.trim();
    }
    
    public String getBizSystemName() {
    	
        return this.bizSystem==null?"":ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
    }
}