package com.team.lottery.vo;

import java.util.Date;

import com.team.lottery.util.DateUtils;

public class AdminRecvNote {
    private Long id;

    private Integer sendNoteId;

    private Integer fromAdminId;

    private String fromAdminName;

    private String fromBizSystem;

    private Integer toAdminId;

    private String toAdminName;

    private String toBizSystem;

    private String sub;

    private String readStatus;

    private Date createdDate;

    private Date updateDate;

    private String content;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSendNoteId() {
        return sendNoteId;
    }

    public void setSendNoteId(Integer sendNoteId) {
        this.sendNoteId = sendNoteId;
    }

    public Integer getFromAdminId() {
        return fromAdminId;
    }

    public void setFromAdminId(Integer fromAdminId) {
        this.fromAdminId = fromAdminId;
    }

    public String getFromAdminName() {
        return fromAdminName;
    }

    public void setFromAdminName(String fromAdminName) {
        this.fromAdminName = fromAdminName == null ? null : fromAdminName.trim();
    }

    public String getFromBizSystem() {
        return fromBizSystem;
    }

    public void setFromBizSystem(String fromBizSystem) {
        this.fromBizSystem = fromBizSystem == null ? null : fromBizSystem.trim();
    }

    public Integer getToAdminId() {
        return toAdminId;
    }

    public void setToAdminId(Integer toAdminId) {
        this.toAdminId = toAdminId;
    }

    public String getToAdminName() {
        return toAdminName;
    }

    public void setToAdminName(String toAdminName) {
        this.toAdminName = toAdminName == null ? null : toAdminName.trim();
    }

    public String getToBizSystem() {
        return toBizSystem;
    }

    public void setToBizSystem(String toBizSystem) {
        this.toBizSystem = toBizSystem == null ? null : toBizSystem.trim();
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub == null ? null : sub.trim();
    }

    public String getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(String readStatus) {
        this.readStatus = readStatus == null ? null : readStatus.trim();
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }
    
    //创建时间字符串
    public String getCreateDateStr(){
    	return DateUtils.getDateToStrByFormat(this.getCreatedDate(), "yyyy年MM月dd日 HH:mm:ss");
    }
    
    //创建时间字符串
    public String getUpdateDateStr(){
    	return DateUtils.getDateToStrByFormat(this.getUpdateDate(), "yyyy年MM月dd日 HH:mm:ss");
    }
}