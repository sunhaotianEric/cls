package com.team.lottery.vo;

import java.util.Date;

import com.team.lottery.util.DateUtils;

public class Login {
	
	//登录方式
	public static final String LOGTYPE_PC = "PC";
	public static final String LOGTYPE_MOBILE = "MOBILE";
	public static final String LOGTYPE_APP = "APP";
	public static final String LOGTYPE_ADMIN = "ADMIN";

	
    private Integer id;

    private String username;

    private Date logtime;

    private String logip;

    private String browser;

    private String address;

    private String logtype;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public Date getLogtime() {
        return logtime;
    }

    public void setLogtime(Date logtime) {
        this.logtime = logtime;
    }

    public String getLogip() {
        return logip;
    }

    public void setLogip(String logip) {
        this.logip = logip == null ? null : logip.trim();
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser == null ? null : browser.trim();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getLogtype() {
        return logtype;
    }

    public void setLogtype(String logtype) {
        this.logtype = logtype == null ? null : logtype.trim();
    }

    /**
	 * 获取登录时间字符串
	 * @return
	 */
	public String getLogtimeStr(){
    	return DateUtils.getDateToStrByFormat(this.getLogtime(), "yyyy-MM-dd HH:mm:ss");
    }
}