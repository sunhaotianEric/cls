package com.team.lottery.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class BizSystem implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = -1216790515168716114L;
	private Long id;
    /**
     * 系统英文简称
     */
    private String bizSystem;
    /**
     * 系统名称
     */
    private String bizSystemName;
    /**
     * 系统描述
     */
    private String bizSystemDesc;
    /**
     * 系统类型
     */
    private Integer bizSystemType;
    /**
     * 系统状态   1正常   0欠费   2维护 
     */
    private Integer systemStatus;

    /**
     * 分红比例 
     */
    private BigDecimal bonusScale = new BigDecimal("0.1");
    
    /**
     * 系统最低奖金模式
     */
    private BigDecimal lowestAwardModel;

    /**
     * 系统最高奖金模式
     */
    private BigDecimal highestAwardModel;
    
    /**
     * 续费日期
     */
    private Date lastRenewTime;
    /**
     * 到期日期
     */
    private Date expireTime;
    /**
     *维护费用
     */
    private BigDecimal maintainMoney;
    /**
     *总维护费用
     */
    private BigDecimal totalMaintainMoney;
    /**
     *总分红费用
     */
    private BigDecimal totalBonusMoney;
    /**
     *启用状态
     */
    private Integer enable;
    /**
     *创建时间
     */
    private Date createTime;
    /**
     *更新时间
     */
    private Date updateTime;
    /**
     *操作人
     */
    private String updateAdmin;

    private String secretCode;

    /**
     * 
     */
    private List<BizSystemBonusConfig> bizSystemBonusConfigList;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public String getBizSystemName() {
        return bizSystemName;
    }

    public void setBizSystemName(String bizSystemName) {
        this.bizSystemName = bizSystemName == null ? null : bizSystemName.trim();
    }

    public String getBizSystemDesc() {
        return bizSystemDesc;
    }

    public void setBizSystemDesc(String bizSystemDesc) {
        this.bizSystemDesc = bizSystemDesc == null ? null : bizSystemDesc.trim();
    }

    public Integer getBizSystemType() {
        return bizSystemType;
    }

    public void setBizSystemType(Integer bizSystemType) {
        this.bizSystemType = bizSystemType;
    }

    public Integer getSystemStatus() {
        return systemStatus;
    }

    public void setSystemStatus(Integer systemStatus) {
        this.systemStatus = systemStatus;
    }

    public BigDecimal getBonusScale() {
        return bonusScale;
    }

    public void setBonusScale(BigDecimal bonusScale) {
        this.bonusScale = bonusScale;
    }
    
    public BigDecimal getLowestAwardModel() {
        return lowestAwardModel;
    }

    public void setLowestAwardModel(BigDecimal lowestAwardModel) {
        this.lowestAwardModel = lowestAwardModel;
    }

    public BigDecimal getHighestAwardModel() {
        return highestAwardModel;
    }

    public void setHighestAwardModel(BigDecimal highestAwardModel) {
        this.highestAwardModel = highestAwardModel;
    }

    public Date getLastRenewTime() {
        return lastRenewTime;
    }

    public void setLastRenewTime(Date lastRenewTime) {
        this.lastRenewTime = lastRenewTime;
    }

    public Date getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Date expireTime) {
        this.expireTime = expireTime;
    }

    public BigDecimal getTotalMaintainMoney() {
        return totalMaintainMoney;
    }

    public void setTotalMaintainMoney(BigDecimal totalMaintainMoney) {
        this.totalMaintainMoney = totalMaintainMoney;
    }

    public BigDecimal getTotalBonusMoney() {
        return totalBonusMoney;
    }

    public void setTotalBonusMoney(BigDecimal totalBonusMoney) {
        this.totalBonusMoney = totalBonusMoney;
    }

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateAdmin() {
        return updateAdmin;
    }

    public void setUpdateAdmin(String updateAdmin) {
        this.updateAdmin = updateAdmin == null ? null : updateAdmin.trim();
    }

	public BigDecimal getMaintainMoney() {
		return maintainMoney;
	}

	public void setMaintainMoney(BigDecimal maintainMoney) {
		this.maintainMoney = maintainMoney;
	}

	public List<BizSystemBonusConfig> getBizSystemBonusConfigList() {
		return bizSystemBonusConfigList;
	}

	public void setBizSystemBonusConfigList(List<BizSystemBonusConfig> bizSystemBonusConfigList) {
		this.bizSystemBonusConfigList = bizSystemBonusConfigList;
	}

    public String getSecretCode() {
        return secretCode;
    }

    public void setSecretCode(String secretCode) {
        this.secretCode = secretCode;
    }
}