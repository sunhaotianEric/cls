package com.team.lottery.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;

public class LotteryDayGain {
    private Long id;

    private String bizSystem;

    private String lotteryType;

    private BigDecimal payMoney;

    private BigDecimal winPayMoney;

    private BigDecimal notWinPayMoney;

    private BigDecimal winMoney;

    private BigDecimal gain;

    private BigDecimal winGain;

    private Date belongDate;

    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public String getLotteryType() {
        return lotteryType;
    }

    public void setLotteryType(String lotteryType) {
        this.lotteryType = lotteryType == null ? null : lotteryType.trim();
    }

    public BigDecimal getPayMoney() {
        return payMoney;
    }

    public void setPayMoney(BigDecimal payMoney) {
        this.payMoney = payMoney;
    }

    public BigDecimal getWinPayMoney() {
        return winPayMoney;
    }

    public void setWinPayMoney(BigDecimal winPayMoney) {
        this.winPayMoney = winPayMoney;
    }

    public BigDecimal getNotWinPayMoney() {
        return notWinPayMoney;
    }

    public void setNotWinPayMoney(BigDecimal notWinPayMoney) {
        this.notWinPayMoney = notWinPayMoney;
    }

    public BigDecimal getWinMoney() {
        return winMoney;
    }

    public void setWinMoney(BigDecimal winMoney) {
        this.winMoney = winMoney;
    }

    public BigDecimal getGain() {
        return gain;
    }

    public void setGain(BigDecimal gain) {
        this.gain = gain;
    }

    public BigDecimal getWinGain() {
        return winGain;
    }

    public void setWinGain(BigDecimal winGain) {
        this.winGain = winGain;
    }

    public Date getBelongDate() {
		return belongDate;
	}

	public void setBelongDate(Date belongDate) {
		this.belongDate = belongDate;
	}

	public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    
    public String getBizSystemName() {
    	
        return this.bizSystem==null?"":ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
    }
}