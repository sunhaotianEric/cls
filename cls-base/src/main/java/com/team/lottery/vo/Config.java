package com.team.lottery.vo;

public class Config {
    private Long id;

    private String setting;

    private String lotterySetting;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSetting() {
        return setting;
    }

    public void setSetting(String setting) {
        this.setting = setting == null ? null : setting.trim();
    }

	public String getLotterySetting() {
		return lotterySetting;
	}

	public void setLotterySetting(String lotterySetting) {
		this.lotterySetting = lotterySetting;
	}
}