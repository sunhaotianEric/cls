package com.team.lottery.vo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.team.lottery.cache.ClsCacheManager;

public class LotteryControlConfig {
	
	private Integer id;

    private String bizSystem;

    private Integer lcqsscProfitModel;
    
    private BigDecimal lcqsscLowestWinGain;

    private BigDecimal lcqsscLargestWinGain;

	private Integer bjsscProfitModel;

	private BigDecimal bjsscLowestWinGain;

	private BigDecimal bjsscLargestWinGain;

	private Integer gdsscProfitModel;

	private BigDecimal gdsscLowestWinGain;

	private BigDecimal gdsscLargestWinGain;

	private Integer jssscProfitModel;

	private BigDecimal jssscLowestWinGain;

	private BigDecimal jssscLargestWinGain;

	private Integer scsscProfitModel;

	private BigDecimal scsscLowestWinGain;

	private BigDecimal scsscLargestWinGain;

	private Integer sdsscProfitModel;

	private BigDecimal sdsscLowestWinGain;

	private BigDecimal sdsscLargestWinGain;

	private Integer shsscProfitModel;

	private BigDecimal shsscLowestWinGain;

	private BigDecimal shsscLargestWinGain;
    private Integer sfksProfitModel;
    
    private BigDecimal sfksLowestWinGain;
    
    private BigDecimal sfksLargestWinGain;
    
    private Integer wfksProfitModel;
    
    private BigDecimal wfksLowestWinGain;
    
    private BigDecimal wfksLargestWinGain;
    
    private Integer shfsscProfitModel;
    
    private BigDecimal shfsscLowestWinGain;
    
    private BigDecimal shfsscLargestWinGain;
    
    private Integer sfsscProfitModel;
    
    private BigDecimal sfsscLowestWinGain;
    
    private BigDecimal sfsscLargestWinGain;
    
    private Integer wfsscProfitModel;
    
    private BigDecimal wfsscLowestWinGain;
    
    private BigDecimal wfsscLargestWinGain;
    
    private Integer jlffcProfitModel;
    
    private BigDecimal jlffcLowestWinGain;
    
    private BigDecimal jlffcLargestWinGain;

    private Integer jyksProfitModel;

    private BigDecimal jyksLowestWinGain;

    private BigDecimal jyksLargestWinGain;

    private Integer jspk10ProfitModel;

    private BigDecimal jspk10LowestWinGain;

    private BigDecimal jspk10LargestWinGain;
    
    private Integer sfpk10ProfitModel;
    
    private BigDecimal sfpk10LowestWinGain;
    
    private BigDecimal sfpk10LargestWinGain;
    
    private Integer wfpk10ProfitModel;
    
    private BigDecimal wfpk10LowestWinGain;
    
    private BigDecimal wfpk10LargestWinGain;
    
    private Integer shfpk10ProfitModel;
    
    private BigDecimal shfpk10LowestWinGain;
    
    private BigDecimal shfpk10LargestWinGain;

    private Integer enabled;

    private Date createTime;

    private Date updateTime;

    private String updateAdmin;
    
    private Integer xylhcProfitModel;

    private BigDecimal xylhcLowestWinGain;

    private BigDecimal xylhcLargestWinGain;
    
    private Integer wfsyxwProfitModel;

    private BigDecimal wfsyxwLowestWinGain;

    private BigDecimal wfsyxwLargestWinGain;
    
    private Integer sfsyxwProfitModel;

    private BigDecimal sfsyxwLowestWinGain;

    private BigDecimal sfsyxwLargestWinGain;
    
    //扩展字段
    private List<String> bizSystemList;
    
	public List<String> getBizSystemList() {
		return bizSystemList;
	}

	public void setBizSystemList(List<String> bizSystemList) {
		this.bizSystemList = bizSystemList;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public Integer getXylhcProfitModel() {
		return xylhcProfitModel;
	}

	public void setXylhcProfitModel(Integer xylhcProfitModel) {
		this.xylhcProfitModel = xylhcProfitModel;
	}

	public BigDecimal getXylhcLowestWinGain() {
		return xylhcLowestWinGain;
	}

	public void setXylhcLowestWinGain(BigDecimal xylhcLowestWinGain) {
		this.xylhcLowestWinGain = xylhcLowestWinGain;
	}

	public BigDecimal getXylhcLargestWinGain() {
		return xylhcLargestWinGain;
	}

	public void setXylhcLargestWinGain(BigDecimal xylhcLargestWinGain) {
		this.xylhcLargestWinGain = xylhcLargestWinGain;
	}

	public BigDecimal getJlffcLowestWinGain() {
		return jlffcLowestWinGain;
	}

	public void setJlffcLowestWinGain(BigDecimal jlffcLowestWinGain) {
		this.jlffcLowestWinGain = jlffcLowestWinGain;
	}

	public BigDecimal getJlffcLargestWinGain() {
		return jlffcLargestWinGain;
	}

	public void setJlffcLargestWinGain(BigDecimal jlffcLargestWinGain) {
		this.jlffcLargestWinGain = jlffcLargestWinGain;
	}

	public BigDecimal getJyksLowestWinGain() {
		return jyksLowestWinGain;
	}

	public void setJyksLowestWinGain(BigDecimal jyksLowestWinGain) {
		this.jyksLowestWinGain = jyksLowestWinGain;
	}

	public BigDecimal getJyksLargestWinGain() {
		return jyksLargestWinGain;
	}

	public void setJyksLargestWinGain(BigDecimal jyksLargestWinGain) {
		this.jyksLargestWinGain = jyksLargestWinGain;
	}

	public BigDecimal getJspk10LowestWinGain() {
		return jspk10LowestWinGain;
	}

	public void setJspk10LowestWinGain(BigDecimal jspk10LowestWinGain) {
		this.jspk10LowestWinGain = jspk10LowestWinGain;
	}

	public BigDecimal getJspk10LargestWinGain() {
		return jspk10LargestWinGain;
	}

	public void setJspk10LargestWinGain(BigDecimal jspk10LargestWinGain) {
		this.jspk10LargestWinGain = jspk10LargestWinGain;
	}

	public Integer getJspk10ProfitModel() {
		return jspk10ProfitModel;
	}

	public void setJspk10ProfitModel(Integer jspk10ProfitModel) {
		this.jspk10ProfitModel = jspk10ProfitModel;
	}

	public Integer getJlffcProfitModel() {
        return jlffcProfitModel;
    }

    public void setJlffcProfitModel(Integer jlffcProfitModel) {
        this.jlffcProfitModel = jlffcProfitModel;
    }

    public Integer getJyksProfitModel() {
        return jyksProfitModel;
    }

    public void setJyksProfitModel(Integer jyksProfitModel) {
        this.jyksProfitModel = jyksProfitModel;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateAdmin() {
        return updateAdmin;
    }

    public Integer getSfksProfitModel() {
		return sfksProfitModel;
	}

	public void setSfksProfitModel(Integer sfksProfitModel) {
		this.sfksProfitModel = sfksProfitModel;
	}

	public BigDecimal getSfksLowestWinGain() {
		return sfksLowestWinGain;
	}

	public void setSfksLowestWinGain(BigDecimal sfksLowestWinGain) {
		this.sfksLowestWinGain = sfksLowestWinGain;
	}

	public BigDecimal getSfksLargestWinGain() {
		return sfksLargestWinGain;
	}

	public void setSfksLargestWinGain(BigDecimal sfksLargestWinGain) {
		this.sfksLargestWinGain = sfksLargestWinGain;
	}

	public Integer getWfksProfitModel() {
		return wfksProfitModel;
	}

	public void setWfksProfitModel(Integer wfksProfitModel) {
		this.wfksProfitModel = wfksProfitModel;
	}

	public BigDecimal getWfksLowestWinGain() {
		return wfksLowestWinGain;
	}

	public void setWfksLowestWinGain(BigDecimal wfksLowestWinGain) {
		this.wfksLowestWinGain = wfksLowestWinGain;
	}

	public BigDecimal getWfksLargestWinGain() {
		return wfksLargestWinGain;
	}

	public void setWfksLargestWinGain(BigDecimal wfksLargestWinGain) {
		this.wfksLargestWinGain = wfksLargestWinGain;
	}

	public BigDecimal getSfsscLowestWinGain() {
		return sfsscLowestWinGain;
	}

	public void setSfsscLowestWinGain(BigDecimal sfsscLowestWinGain) {
		this.sfsscLowestWinGain = sfsscLowestWinGain;
	}

	public BigDecimal getSfsscLargestWinGain() {
		return sfsscLargestWinGain;
	}

	public void setSfsscLargestWinGain(BigDecimal sfsscLargestWinGain) {
		this.sfsscLargestWinGain = sfsscLargestWinGain;
	}

	public BigDecimal getWfsscLowestWinGain() {
		return wfsscLowestWinGain;
	}

	public void setWfsscLowestWinGain(BigDecimal wfsscLowestWinGain) {
		this.wfsscLowestWinGain = wfsscLowestWinGain;
	}

	public BigDecimal getWfsscLargestWinGain() {
		return wfsscLargestWinGain;
	}

	public void setWfsscLargestWinGain(BigDecimal wfsscLargestWinGain) {
		this.wfsscLargestWinGain = wfsscLargestWinGain;
	}

	public Integer getSfsscProfitModel() {
		return sfsscProfitModel;
	}

	public void setSfsscProfitModel(Integer sfsscProfitModel) {
		this.sfsscProfitModel = sfsscProfitModel;
	}

	public Integer getWfsscProfitModel() {
		return wfsscProfitModel;
	}

	public void setWfsscProfitModel(Integer wfsscProfitModel) {
		this.wfsscProfitModel = wfsscProfitModel;
	}
	
	public Integer getWfsyxwProfitModel() {
		return wfsyxwProfitModel;
	}

	public void setWfsyxwProfitModel(Integer wfsyxwProfitModel) {
		this.wfsyxwProfitModel = wfsyxwProfitModel;
	}

	public BigDecimal getWfsyxwLowestWinGain() {
		return wfsyxwLowestWinGain;
	}

	public void setWfsyxwLowestWinGain(BigDecimal wfsyxwLowestWinGain) {
		this.wfsyxwLowestWinGain = wfsyxwLowestWinGain;
	}

	public BigDecimal getWfsyxwLargestWinGain() {
		return wfsyxwLargestWinGain;
	}

	public void setWfsyxwLargestWinGain(BigDecimal wfsyxwLargestWinGain) {
		this.wfsyxwLargestWinGain = wfsyxwLargestWinGain;
	}

	public void setUpdateAdmin(String updateAdmin) {
        this.updateAdmin = updateAdmin == null ? null : updateAdmin.trim();
    }
    public String getBizSystemName() {
        return this.bizSystem==null?"":ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
    }

	public Integer getShfsscProfitModel() {
		return shfsscProfitModel;
	}

	public void setShfsscProfitModel(Integer shfsscProfitModel) {
		this.shfsscProfitModel = shfsscProfitModel;
	}

	public BigDecimal getShfsscLowestWinGain() {
		return shfsscLowestWinGain;
	}

	public void setShfsscLowestWinGain(BigDecimal shfsscLowestWinGain) {
		this.shfsscLowestWinGain = shfsscLowestWinGain;
	}

	public BigDecimal getShfsscLargestWinGain() {
		return shfsscLargestWinGain;
	}

	public void setShfsscLargestWinGain(BigDecimal shfsscLargestWinGain) {
		this.shfsscLargestWinGain = shfsscLargestWinGain;
	}

	public Integer getSfsyxwProfitModel() {
		return sfsyxwProfitModel;
	}

	public void setSfsyxwProfitModel(Integer sfsyxwProfitModel) {
		this.sfsyxwProfitModel = sfsyxwProfitModel;
	}

	public BigDecimal getSfsyxwLowestWinGain() {
		return sfsyxwLowestWinGain;
	}

	public void setSfsyxwLowestWinGain(BigDecimal sfsyxwLowestWinGain) {
		this.sfsyxwLowestWinGain = sfsyxwLowestWinGain;
	}

	public BigDecimal getSfsyxwLargestWinGain() {
		return sfsyxwLargestWinGain;
	}

	public void setSfsyxwLargestWinGain(BigDecimal sfsyxwLargestWinGain) {
		this.sfsyxwLargestWinGain = sfsyxwLargestWinGain;
	}

	public Integer getSfpk10ProfitModel() {
		return sfpk10ProfitModel;
	}

	public void setSfpk10ProfitModel(Integer sfpk10ProfitModel) {
		this.sfpk10ProfitModel = sfpk10ProfitModel;
	}

	public BigDecimal getSfpk10LowestWinGain() {
		return sfpk10LowestWinGain;
	}

	public void setSfpk10LowestWinGain(BigDecimal sfpk10LowestWinGain) {
		this.sfpk10LowestWinGain = sfpk10LowestWinGain;
	}

	public BigDecimal getSfpk10LargestWinGain() {
		return sfpk10LargestWinGain;
	}

	public void setSfpk10LargestWinGain(BigDecimal sfpk10LargestWinGain) {
		this.sfpk10LargestWinGain = sfpk10LargestWinGain;
	}

	public Integer getWfpk10ProfitModel() {
		return wfpk10ProfitModel;
	}

	public void setWfpk10ProfitModel(Integer wfpk10ProfitModel) {
		this.wfpk10ProfitModel = wfpk10ProfitModel;
	}

	public BigDecimal getWfpk10LowestWinGain() {
		return wfpk10LowestWinGain;
	}

	public void setWfpk10LowestWinGain(BigDecimal wfpk10LowestWinGain) {
		this.wfpk10LowestWinGain = wfpk10LowestWinGain;
	}

	public BigDecimal getWfpk10LargestWinGain() {
		return wfpk10LargestWinGain;
	}

	public void setWfpk10LargestWinGain(BigDecimal wfpk10LargestWinGain) {
		this.wfpk10LargestWinGain = wfpk10LargestWinGain;
	}

	public Integer getShfpk10ProfitModel() {
		return shfpk10ProfitModel;
	}

	public void setShfpk10ProfitModel(Integer shfpk10ProfitModel) {
		this.shfpk10ProfitModel = shfpk10ProfitModel;
	}

	public BigDecimal getShfpk10LowestWinGain() {
		return shfpk10LowestWinGain;
	}

	public void setShfpk10LowestWinGain(BigDecimal shfpk10LowestWinGain) {
		this.shfpk10LowestWinGain = shfpk10LowestWinGain;
	}

	public BigDecimal getShfpk10LargestWinGain() {
		return shfpk10LargestWinGain;
	}

	public void setShfpk10LargestWinGain(BigDecimal shfpk10LargestWinGain) {
		this.shfpk10LargestWinGain = shfpk10LargestWinGain;
	}

	public Integer getLcqsscProfitModel() {
		return lcqsscProfitModel;
	}

	public void setLcqsscProfitModel(Integer lcqsscProfitModel) {
		this.lcqsscProfitModel = lcqsscProfitModel;
	}

	public BigDecimal getLcqsscLowestWinGain() {
		return lcqsscLowestWinGain;
	}

	public void setLcqsscLowestWinGain(BigDecimal lcqsscLowestWinGain) {
		this.lcqsscLowestWinGain = lcqsscLowestWinGain;
	}

	public BigDecimal getLcqsscLargestWinGain() {
		return lcqsscLargestWinGain;
	}

	public void setLcqsscLargestWinGain(BigDecimal lcqsscLargestWinGain) {
		this.lcqsscLargestWinGain = lcqsscLargestWinGain;
	}

	public Integer getBjsscProfitModel() {
		return bjsscProfitModel;
	}

	public void setBjsscProfitModel(Integer bjsscProfitModel) {
		this.bjsscProfitModel = bjsscProfitModel;
	}

	public BigDecimal getBjsscLowestWinGain() {
		return bjsscLowestWinGain;
	}

	public void setBjsscLowestWinGain(BigDecimal bjsscLowestWinGain) {
		this.bjsscLowestWinGain = bjsscLowestWinGain;
	}

	public BigDecimal getBjsscLargestWinGain() {
		return bjsscLargestWinGain;
	}

	public void setBjsscLargestWinGain(BigDecimal bjsscLargestWinGain) {
		this.bjsscLargestWinGain = bjsscLargestWinGain;
	}

	public Integer getGdsscProfitModel() {
		return gdsscProfitModel;
	}

	public void setGdsscProfitModel(Integer gdsscProfitModel) {
		this.gdsscProfitModel = gdsscProfitModel;
	}

	public BigDecimal getGdsscLowestWinGain() {
		return gdsscLowestWinGain;
	}

	public void setGdsscLowestWinGain(BigDecimal gdsscLowestWinGain) {
		this.gdsscLowestWinGain = gdsscLowestWinGain;
	}

	public BigDecimal getGdsscLargestWinGain() {
		return gdsscLargestWinGain;
	}

	public void setGdsscLargestWinGain(BigDecimal gdsscLargestWinGain) {
		this.gdsscLargestWinGain = gdsscLargestWinGain;
	}

	public Integer getJssscProfitModel() {
		return jssscProfitModel;
	}

	public void setJssscProfitModel(Integer jssscProfitModel) {
		this.jssscProfitModel = jssscProfitModel;
	}

	public BigDecimal getJssscLowestWinGain() {
		return jssscLowestWinGain;
	}

	public void setJssscLowestWinGain(BigDecimal jssscLowestWinGain) {
		this.jssscLowestWinGain = jssscLowestWinGain;
	}

	public BigDecimal getJssscLargestWinGain() {
		return jssscLargestWinGain;
	}

	public void setJssscLargestWinGain(BigDecimal jssscLargestWinGain) {
		this.jssscLargestWinGain = jssscLargestWinGain;
	}

	public Integer getScsscProfitModel() {
		return scsscProfitModel;
	}

	public void setScsscProfitModel(Integer scsscProfitModel) {
		this.scsscProfitModel = scsscProfitModel;
	}

	public BigDecimal getScsscLowestWinGain() {
		return scsscLowestWinGain;
	}

	public void setScsscLowestWinGain(BigDecimal scsscLowestWinGain) {
		this.scsscLowestWinGain = scsscLowestWinGain;
	}

	public BigDecimal getScsscLargestWinGain() {
		return scsscLargestWinGain;
	}

	public void setScsscLargestWinGain(BigDecimal scsscLargestWinGain) {
		this.scsscLargestWinGain = scsscLargestWinGain;
	}

	public Integer getSdsscProfitModel() {
		return sdsscProfitModel;
	}

	public void setSdsscProfitModel(Integer sdsscProfitModel) {
		this.sdsscProfitModel = sdsscProfitModel;
	}

	public BigDecimal getSdsscLowestWinGain() {
		return sdsscLowestWinGain;
	}

	public void setSdsscLowestWinGain(BigDecimal sdsscLowestWinGain) {
		this.sdsscLowestWinGain = sdsscLowestWinGain;
	}

	public BigDecimal getSdsscLargestWinGain() {
		return sdsscLargestWinGain;
	}

	public void setSdsscLargestWinGain(BigDecimal sdsscLargestWinGain) {
		this.sdsscLargestWinGain = sdsscLargestWinGain;
	}

	public Integer getShsscProfitModel() {
		return shsscProfitModel;
	}

	public void setShsscProfitModel(Integer shsscProfitModel) {
		this.shsscProfitModel = shsscProfitModel;
	}

	public BigDecimal getShsscLowestWinGain() {
		return shsscLowestWinGain;
	}

	public void setShsscLowestWinGain(BigDecimal shsscLowestWinGain) {
		this.shsscLowestWinGain = shsscLowestWinGain;
	}

	public BigDecimal getShsscLargestWinGain() {
		return shsscLargestWinGain;
	}

	public void setShsscLargestWinGain(BigDecimal shsscLargestWinGain) {
		this.shsscLargestWinGain = shsscLargestWinGain;
	}
}