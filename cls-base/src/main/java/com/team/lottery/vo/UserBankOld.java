package com.team.lottery.vo;

import java.util.Date;

import com.team.lottery.util.DateUtils;

public class UserBankOld {
    private Long id;

    private Integer userId;

    private String userName;

    private String province;

    private String city;

    private String area;

    private String bankType;

    private String bankBank;

    private String bankAccount;

    private String bankNum;

    private String bankName;

    private Date createDate;

    private Date updateDate;
    
    //扩展字段
    private String confirmBankNum;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area == null ? null : area.trim();
    }

    public String getBankType() {
        return bankType;
    }

    public void setBankType(String bankType) {
        this.bankType = bankType == null ? null : bankType.trim();
    }

    public String getBankBank() {
        return bankBank;
    }

    public void setBankBank(String bankBank) {
        this.bankBank = bankBank == null ? null : bankBank.trim();
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount == null ? null : bankAccount.trim();
    }

    public String getBankNum() {
        return bankNum;
    }

    public void setBankNum(String bankNum) {
        this.bankNum = bankNum == null ? null : bankNum.trim();
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName == null ? null : bankName.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
    
	public String getConfirmBankNum() {
		return confirmBankNum;
	}

	public void setConfirmBankNum(String confirmBankNum) {
		this.confirmBankNum = confirmBankNum;
	}
	
	public String getCreateDateStr(){
    	return DateUtils.getDateToStrByFormat(this.getCreateDate(), "yyyy/MM/dd HH:mm:ss");
    }
}