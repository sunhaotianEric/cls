package com.team.lottery.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class BonusConfig  implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -6481472081863058662L;
	public static final String BONUS_TYPE_CURRENT = "CURRENT";
	public static final String BONUS_TYPE_NEW = "NEW";
	private Long id;

    private String bizSystem;

    
    private Integer userId;

    private String userName;

    /**
     * 配置类型 CURRENT当前配置项   NEW新配置项
     * 用户同意为CURRENT，其他时候为NEW
     */
    private String type;

    /**
     * 比例值 如几万
     */
    private BigDecimal scale;

    /**
     * 百分比，分红比例
     */
    private BigDecimal value;

    /**
     * 状态 0未启用  1启用
     */
    private Integer state;

    /**
     * 描述
     */
    private String description;

    /**
     * 创建人契约人的id
     */
    private Integer createUserId;

    /**
     * 创建人契约人的用户名
     */
    private String createUserName;

    /**
     * 
     */
    private Date createTime;

    /**
     * 
     */
    private Date updateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public BigDecimal getScale() {
        return scale;
    }

    public void setScale(BigDecimal scale) {
        this.scale = scale;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public Integer getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Integer createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName == null ? null : createUserName.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}