package com.team.lottery.vo;

import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.util.DateUtils;

public class HomePic {
    private Long id;

    private String bizSystem;

    private String name;

    private String picType;

    private String homePicUrl;

    private String homePicLinkUrl;

    private Integer orders;

    private Integer isShow;

    private Date createTime;

    private String createAdmin;

    private Date updateTime;

    private String updateAdmin;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getPicType() {
        return picType;
    }

    public void setPicType(String picType) {
        this.picType = picType == null ? null : picType.trim();
    }

    public String getHomePicUrl() {
        return homePicUrl;
    }

    public void setHomePicUrl(String homePicUrl) {
        this.homePicUrl = homePicUrl == null ? null : homePicUrl.trim();
    }

    public String getHomePicLinkUrl() {
        return homePicLinkUrl;
    }

    public void setHomePicLinkUrl(String homePicLinkUrl) {
        this.homePicLinkUrl = homePicLinkUrl == null ? null : homePicLinkUrl.trim();
    }

    public Integer getOrders() {
        return orders;
    }

    public void setOrders(Integer orders) {
        this.orders = orders;
    }

    public Integer getIsShow() {
        return isShow;
    }

    public void setIsShow(Integer isShow) {
        this.isShow = isShow;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateAdmin() {
        return createAdmin;
    }

    public void setCreateAdmin(String createAdmin) {
        this.createAdmin = createAdmin == null ? null : createAdmin.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateAdmin() {
        return updateAdmin;
    }

    public void setUpdateAdmin(String updateAdmin) {
        this.updateAdmin = updateAdmin == null ? null : updateAdmin.trim();
    }
    
	//创建时间字符串
    public String getCreateTimeStr(){
    	return DateUtils.getDateToStrByFormat(this.getCreateTime(), "yyyy/MM/dd HH:mm:ss");
    }
    
    //更新时间字符串
    public String getUpdateTimeStr(){
    	return DateUtils.getDateToStrByFormat(this.getUpdateTime(), "yyyy/MM/dd HH:mm:ss");
    }
    public String getBizSystemName() {
        return this.bizSystem==null?"":ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
    }
}