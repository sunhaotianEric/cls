package com.team.lottery.vo;

import java.util.Date;

public class UserLotteryModelSet {
    private Long id;
    
    private String bizSystem;

    private Integer userId;

    private String userName;

    private String mobileLotteryModel;

    private Date updateTime;

    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMobileLotteryModel() {
        return mobileLotteryModel;
    }

    public void setMobileLotteryModel(String mobileLotteryModel) {
        this.mobileLotteryModel = mobileLotteryModel == null ? null : mobileLotteryModel.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}