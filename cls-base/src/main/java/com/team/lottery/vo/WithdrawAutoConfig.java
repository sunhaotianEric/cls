package com.team.lottery.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;

public class WithdrawAutoConfig {
	private Integer id;

	private String bizSystem;

	private String thirdPayType;

	private String thirdPayTypeDesc;

	private Long chargePayId;

	private String memberId;

	private BigDecimal highestMoney;

	private Integer autoWithdrawSwitch;
	
	private Integer dealAdminId;

	private String dealAdminUsername;

	private Integer enabled;

	private Date createTime;

	private String updateAdmin;

	private Date updateTime;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem == null ? null : bizSystem.trim();
	}

	public String getThirdPayType() {
		return thirdPayType;
	}

	public void setThirdPayType(String thirdPayType) {
		this.thirdPayType = thirdPayType == null ? null : thirdPayType.trim();
	}

	public String getThirdPayTypeDesc() {
		return thirdPayTypeDesc;
	}

	public void setThirdPayTypeDesc(String thirdPayTypeDesc) {
		this.thirdPayTypeDesc = thirdPayTypeDesc == null ? null : thirdPayTypeDesc.trim();
	}

	public Long getChargePayId() {
		return chargePayId;
	}

	public void setChargePayId(Long chargePayId) {
		this.chargePayId = chargePayId;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId == null ? null : memberId.trim();
	}

	public BigDecimal getHighestMoney() {
		return highestMoney;
	}

	public void setHighestMoney(BigDecimal highestMoney) {
		this.highestMoney = highestMoney;
	}

	public Integer getAutoWithdrawSwitch() {
		return autoWithdrawSwitch;
	}

	public void setAutoWithdrawSwitch(Integer autoWithdrawSwitch) {
		this.autoWithdrawSwitch = autoWithdrawSwitch;
	}

	public Integer getDealAdminId() {
		return dealAdminId;
	}

	public void setDealAdminId(Integer dealAdminId) {
		this.dealAdminId = dealAdminId;
	}

	public String getDealAdminUsername() {
		return dealAdminUsername;
	}

	public void setDealAdminUsername(String dealAdminUsername) {
		this.dealAdminUsername = dealAdminUsername;
	}

	public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getUpdateAdmin() {
		return updateAdmin;
	}

	public void setUpdateAdmin(String updateAdmin) {
		this.updateAdmin = updateAdmin == null ? null : updateAdmin.trim();
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	// 获取系统对应的中文名
	public String getBizSystemName() {

		return this.bizSystem == null ? "" : ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
	}
}