package com.team.lottery.vo;

import com.team.lottery.enums.EMobileSkipType;

public class QuickBankType {
    private Long id;

    private String bankType;

    private String chargeType;

    private String bankName;

    private String payId;
    
    private Integer mobileSkipType;

    private Integer enabled;
    
    private String chargeTypeDesc;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBankType() {
		return bankType;
	}

	public void setBankType(String bankType) {
		this.bankType = bankType;
	}

	public String getChargeType() {
		return chargeType;
	}

	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}
	
	public Integer getMobileSkipType() {
        return mobileSkipType;
    }

    public void setMobileSkipType(Integer mobileSkipType) {
        this.mobileSkipType = mobileSkipType;
    }

	public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}
	
	 public String getChargeTypeDesc() {
		return chargeTypeDesc;
	}

	public void setChargeTypeDesc(String chargeTypeDesc) {
		this.chargeTypeDesc = chargeTypeDesc;
	}

	/**
     * 第三方支付类型
     * @return
     */
    public String getChargeTypeStr(){
    	if(this.getChargeType() != null){
    		/*String thirdPay = this.getChargeType();
    		EFundThirdPayType[] eFundThirdPayType = EFundThirdPayType.values();
    		for(EFundThirdPayType thirdPayType : eFundThirdPayType){
    			if(thirdPay.equals(thirdPayType.name())){
    				return thirdPayType.getDescription();
    			}
    		}*/
    		return this.getChargeTypeDesc();
    	}
		return "";
    }
    
    /**
     * 第三方支付类型
     * @return
     */
    public String getMobileSkipTypeStr(){
    	if(this.getMobileSkipType() != null){
    		String mobileSkipType = String.valueOf(this.getMobileSkipType());
    		EMobileSkipType[] eMobileSkipType = EMobileSkipType.values();
    		for(EMobileSkipType type : eMobileSkipType){
    			if(mobileSkipType.equals(type.getCode())){
    				return type.getDescription();
    			}
    		}
    	}
		return "";
    }
}