package com.team.lottery.vo;

import java.math.BigDecimal;
import com.team.lottery.cache.ClsCacheManager;

public class LotteryWinLhc {
    private Integer id;

    private String bizSystem;

    private String lotteryTypeProtype;

    private String lotteryName;

    private String code;

    private String codeDes;

    private BigDecimal winMoney;

    private Integer returnPercent;

    private Integer codeNum;

    private Integer orders;

    //拓展字段
    private String[] codeArr;
    
	public String[] getCodeArr() {
		return codeArr;
	}

	public void setCodeArr(String[] codeArr) {
		this.codeArr = codeArr;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public String getLotteryTypeProtype() {
        return lotteryTypeProtype;
    }

    public void setLotteryTypeProtype(String lotteryTypeProtype) {
        this.lotteryTypeProtype = lotteryTypeProtype == null ? null : lotteryTypeProtype.trim();
    }

    public String getLotteryName() {
        return lotteryName;
    }

    public void setLotteryName(String lotteryName) {
        this.lotteryName = lotteryName == null ? null : lotteryName.trim();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    public String getCodeDes() {
        return codeDes;
    }

    public void setCodeDes(String codeDes) {
        this.codeDes = codeDes == null ? null : codeDes.trim();
    }

    public BigDecimal getWinMoney() {
        return winMoney;
    }

    public void setWinMoney(BigDecimal winMoney) {
        this.winMoney = winMoney;
    }

    public Integer getReturnPercent() {
        return returnPercent;
    }

    public void setReturnPercent(Integer returnPercent) {
        this.returnPercent = returnPercent;
    }

    public Integer getCodeNum() {
        return codeNum;
    }

    public void setCodeNum(Integer codeNum) {
        this.codeNum = codeNum;
    }

    public Integer getOrders() {
        return orders;
    }

    public void setOrders(Integer orders) {
        this.orders = orders;
    }
    public String getBizSystemName() {
        return this.bizSystem==null?"":ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
    }
}