package com.team.lottery.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.enums.EConsumeActivityType;
import com.team.lottery.util.DateUtils;

public class DayConsumeActityConfig {
    private Long id;

    private String bizSystem;
    
    private String activityType;

    private BigDecimal lotteryNum;

    private BigDecimal money;

    private Date createDate;

    private String createAdmin;

    private Date updateDate;

    private String updateAdmin;
    
    private String orderBy;
    
    //扩展字段,领取状态!(0为未领取,1是已领取);
    private String receiveStatus;

	public String getReceiveStatus() {
		return receiveStatus;
	}

	public void setReceiveStatus(String receiveStatus) {
		this.receiveStatus = receiveStatus;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }
    
    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType == null ? null : activityType.trim();
    }

    public BigDecimal getLotteryNum() {
        return lotteryNum;
    }

    public void setLotteryNum(BigDecimal lotteryNum) {
        this.lotteryNum = lotteryNum;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateAdmin() {
        return createAdmin;
    }

    public void setCreateAdmin(String createAdmin) {
        this.createAdmin = createAdmin == null ? null : createAdmin.trim();
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateAdmin() {
        return updateAdmin;
    }

    public void setUpdateAdmin(String updateAdmin) {
        this.updateAdmin = updateAdmin == null ? null : updateAdmin.trim();
    }
    
	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	//创建时间字符串
    public String getCreateDateStr(){
    	return DateUtils.getDateToStrByFormat(this.getCreateDate(), "yyyy/MM/dd HH:mm:ss");
    }
  //创建时间字符串
    public String getUpdateDateStr(){
    	return DateUtils.getDateToStrByFormat(this.getUpdateDate(), "yyyy/MM/dd HH:mm:ss");
    }
    public String getBizSystemName() {
        return this.bizSystem==null?"":ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
    }
    /**
     * 第三方充值类型
     * @return
     */
    public String getActivityTypeStr(){
    	if(this.getActivityType() != null){
    		String activityType = this.getActivityType();
    		EConsumeActivityType[] eConsumeActivityType = EConsumeActivityType.values();
    		for(EConsumeActivityType type : eConsumeActivityType){
    			if(activityType.equals(type.name())){
    				return type.getDescription();
    			}
    		}
    		
    	}
		return "";
    }
    
}