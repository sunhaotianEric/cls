package com.team.lottery.vo;

import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.util.DateUtil;

public class EmailSendRecord {
	// 默认类型.
	public static final String DEFAULT_TYPE = "DEFAULT_TYPE";
	// 忘记密码类型
	public static final String FORGETPWD_TYPE = "FORGETPWD_TYPE";
	// 绑定手机类型
	public static final String BIND_EMAIL_TYPE = "BIND_EMAIL_TYPE";
	// 修改绑定手机类型
	public static final String CHANGE_EMAIL_TYPE = "CHANGE_EMAIL_TYPE";

	private Long id;

	private String bizSystem;

	private Integer userId;

	private String userName;

	private String type;

	private String ip;

	private String sessionId;

	// 邮箱
	private String email;

	// 验证码
	private String vcode;

	// 备注
	private String remark;

	private Date createDate;

	// 查询开始时间.
	private Date startDate;

	// 查询结束时间.
	private Date endDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem == null ? null : bizSystem.trim();
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId == null ? null : sessionId.trim();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email == null ? null : email.trim();
	}

	public String getVcode() {
		return vcode;
	}

	public void setVcode(String vcode) {
		this.vcode = vcode == null ? null : vcode.trim();
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getBizSystemName() {
		return this.bizSystem == null ? "" : ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
	}
	
	public String getTypeName(){
		String type="";
		if(this.type.equals(EmailSendRecord.DEFAULT_TYPE)){
			type="默认类型";
		}else if(this.type.equals(EmailSendRecord.FORGETPWD_TYPE)){
			type="忘记密码类型";
		}else if(this.type.equals(EmailSendRecord.BIND_EMAIL_TYPE)){
			type="绑定手机类型";
		}else if(this.type.equals(EmailSendRecord.CHANGE_EMAIL_TYPE)){
			type="修改绑定手机类型";
		}
		return type;
	}
	
	// 获取创建时间时分秒的格式
	public String getCreateDateStr() {
		String dateStr = "";
		if (this.createDate != null) {
			dateStr = DateUtil.parseDate(createDate, "yyyy-MM-dd HH:mm:ss");
		}
		return dateStr;
	}
}