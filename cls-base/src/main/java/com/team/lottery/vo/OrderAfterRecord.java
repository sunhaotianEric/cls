package com.team.lottery.vo;

import java.math.BigDecimal;
import java.util.Date;

public class OrderAfterRecord {
    private Long id;

    private Long orderId;

    private String lotteryId;

    private String bizSystem;

    private Integer userId;

    private String userName;

    private String lotteryType;

    private String startExpect;

    private String endExpect;

    private String expects;

    private Integer yetAfterNumber;

    private BigDecimal yetAfterCost;

    private Integer afterStatus;

    private Date createDate;

    private Date updateDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getLotteryId() {
        return lotteryId;
    }

    public void setLotteryId(String lotteryId) {
        this.lotteryId = lotteryId == null ? null : lotteryId.trim();
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getLotteryType() {
        return lotteryType;
    }

    public void setLotteryType(String lotteryType) {
        this.lotteryType = lotteryType == null ? null : lotteryType.trim();
    }

    public String getStartExpect() {
        return startExpect;
    }

    public void setStartExpect(String startExpect) {
        this.startExpect = startExpect == null ? null : startExpect.trim();
    }

    public String getEndExpect() {
        return endExpect;
    }

    public void setEndExpect(String endExpect) {
        this.endExpect = endExpect == null ? null : endExpect.trim();
    }

    public String getExpects() {
        return expects;
    }

    public void setExpects(String expects) {
        this.expects = expects == null ? null : expects.trim();
    }

    public Integer getYetAfterNumber() {
        return yetAfterNumber;
    }

    public void setYetAfterNumber(Integer yetAfterNumber) {
        this.yetAfterNumber = yetAfterNumber;
    }

    public BigDecimal getYetAfterCost() {
        return yetAfterCost;
    }

    public void setYetAfterCost(BigDecimal yetAfterCost) {
        this.yetAfterCost = yetAfterCost;
    }

    public Integer getAfterStatus() {
        return afterStatus;
    }

    public void setAfterStatus(Integer afterStatus) {
        this.afterStatus = afterStatus;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}