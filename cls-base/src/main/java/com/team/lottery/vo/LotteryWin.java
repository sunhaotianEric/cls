package com.team.lottery.vo;

import java.math.BigDecimal;

public class LotteryWin {
    private Integer id;
    private Integer winLevel;
    private String winName;
    private BigDecimal winMoney;
    private Integer winNum;
    private String lotteryName;
    private BigDecimal specCode;
    private Integer codeNum;
    private Integer isorder;
    private String lotteryTypeProtype;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getWinLevel() {
        return winLevel;
    }

    public void setWinLevel(Integer winLevel) {
        this.winLevel = winLevel;
    }

    public String getWinName() {
        return winName;
    }

    public void setWinName(String winName) {
        this.winName = winName == null ? null : winName.trim();
    }

    public BigDecimal getWinMoney() {
        return winMoney;
    }

    public void setWinMoney(BigDecimal winMoney) {
        this.winMoney = winMoney;
    }

    public Integer getWinNum() {
        return winNum;
    }

    public void setWinNum(Integer winNum) {
        this.winNum = winNum;
    }

    public String getLotteryName() {
        return lotteryName;
    }

    public void setLotteryName(String lotteryName) {
        this.lotteryName = lotteryName == null ? null : lotteryName.trim();
    }

    public BigDecimal getSpecCode() {
        return specCode;
    }

    public void setSpecCode(BigDecimal specCode) {
        this.specCode = specCode;
    }

    public Integer getCodeNum() {
        return codeNum;
    }

    public void setCodeNum(Integer codeNum) {
        this.codeNum = codeNum;
    }

    public Integer getIsorder() {
        return isorder;
    }

    public void setIsorder(Integer isorder) {
        this.isorder = isorder;
    }

    public String getLotteryTypeProtype() {
        return lotteryTypeProtype;
    }

    public void setLotteryTypeProtype(String lotteryTypeProtype) {
        this.lotteryTypeProtype = lotteryTypeProtype == null ? null : lotteryTypeProtype.trim();
    }
}