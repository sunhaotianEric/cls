package com.team.lottery.vo;

import java.util.Date;

public class UserModelUsed {
	
	//使用过
	public static final Integer USED = 1;
	//未使用过
	public static final Integer NOUSED = 0;
	
    private Long id;

    private Integer userId;

    private String userName;

    private Integer haoUsed;

    private Integer liUsed;

    private Integer fenUsed;

    private Integer jiaoUsed;

    private Integer yuanUsed;

    private Date belongDate;

    private Date createDate;

    private Date updateDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public Integer getHaoUsed() {
        return haoUsed;
    }

    public void setHaoUsed(Integer haoUsed) {
        this.haoUsed = haoUsed;
    }

    public Integer getLiUsed() {
        return liUsed;
    }

    public void setLiUsed(Integer liUsed) {
        this.liUsed = liUsed;
    }

    public Integer getFenUsed() {
        return fenUsed;
    }

    public void setFenUsed(Integer fenUsed) {
        this.fenUsed = fenUsed;
    }

    public Integer getJiaoUsed() {
        return jiaoUsed;
    }

    public void setJiaoUsed(Integer jiaoUsed) {
        this.jiaoUsed = jiaoUsed;
    }

    public Integer getYuanUsed() {
        return yuanUsed;
    }

    public void setYuanUsed(Integer yuanUsed) {
        this.yuanUsed = yuanUsed;
    }

    public Date getBelongDate() {
        return belongDate;
    }

    public void setBelongDate(Date belongDate) {
        this.belongDate = belongDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}