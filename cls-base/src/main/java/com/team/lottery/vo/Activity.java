package com.team.lottery.vo;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.util.DateUtils;

public class Activity {
    private Long id;
    
    private String type;

    private String bizSystem;

    private String title;

    private String des;

    private Date startDate;

    private Date endDate;

    private Integer status;

    private String imagePath;
    
    private String mobileImagePath;

    private Integer showOrder;

    private Date createDate;

    private Date updateDate;

    private String createAdmin;

    private String updateAdmin;
    
    //扩展属性 流水返送活动，签到活动使用
    //普通流水返送配置 状态信息
    private List<DayConsumeActityConfig> dayConsumeActityConfigs;
    //分分彩流水返送配置  状态信息
    private List<DayConsumeActityConfig> dayConsumeActityFfcConfigs;
    //签到配置 状态信息
    private List<SigninConfig> signinConfigs;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }
    
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des == null ? null : des.trim();
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath == null ? null : imagePath.trim();
    }

    public String getMobileImagePath() {
		return mobileImagePath;
	}

	public void setMobileImagePath(String mobileImagePath) {
		this.mobileImagePath = mobileImagePath;
	}

	public Integer getShowOrder() {
        return showOrder;
    }

    public void setShowOrder(Integer showOrder) {
        this.showOrder = showOrder;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getCreateAdmin() {
        return createAdmin;
    }

    public void setCreateAdmin(String createAdmin) {
        this.createAdmin = createAdmin == null ? null : createAdmin.trim();
    }

    public String getUpdateAdmin() {
        return updateAdmin;
    }

    public void setUpdateAdmin(String updateAdmin) {
        this.updateAdmin = updateAdmin == null ? null : updateAdmin.trim();
    }
    //开始时间字符串
    public String getStartDateStr(){
    	return DateUtils.getDateToStrByFormat(this.getStartDate(), "yyyy/MM/dd");
    }
    
    //结束时间字符串
    public String getEndDateStr(){
    	return DateUtils.getDateToStrByFormat(this.getEndDate(), "yyyy/MM/dd");
    }
	//创建时间字符串
    public String getCreateDateStr(){
    	return DateUtils.getDateToStrByFormat(this.getCreateDate(), "yyyy/MM/dd HH:mm:ss");
    }
    
    //更新时间字符串
    public String getUpdateDateStr(){
    	return DateUtils.getDateToStrByFormat(this.getUpdateDate(), "yyyy/MM/dd HH:mm:ss");
    }
    
    public String getBizSystemName() {
        return this.bizSystem==null?"":ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
    }
    
    /**
     * 重新设置图片url地址,添加前面域名信息
     */
    public void resetImageUrl() {
    	if(StringUtils.isNotBlank(this.imagePath)) {
    		this.imagePath = SystemConfigConstant.imgServerUrl + this.imagePath;
    	}
    	if(StringUtils.isNotBlank(this.mobileImagePath)) {
    		this.mobileImagePath = SystemConfigConstant.imgServerUrl + this.mobileImagePath;
    	}
    }

	public List<DayConsumeActityConfig> getDayConsumeActityConfigs() {
		return dayConsumeActityConfigs;
	}

	public void setDayConsumeActityConfigs(List<DayConsumeActityConfig> dayConsumeActityConfigs) {
		this.dayConsumeActityConfigs = dayConsumeActityConfigs;
	}

	public List<DayConsumeActityConfig> getDayConsumeActityFfcConfigs() {
		return dayConsumeActityFfcConfigs;
	}

	public void setDayConsumeActityFfcConfigs(List<DayConsumeActityConfig> dayConsumeActityFfcConfigs) {
		this.dayConsumeActityFfcConfigs = dayConsumeActityFfcConfigs;
	}

	public List<SigninConfig> getSigninConfigs() {
		return signinConfigs;
	}

	public void setSigninConfigs(List<SigninConfig> signinConfigs) {
		this.signinConfigs = signinConfigs;
	}
    
    
}