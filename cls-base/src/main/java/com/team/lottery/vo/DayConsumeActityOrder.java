package com.team.lottery.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.util.DateUtils;

public class DayConsumeActityOrder {
    private Long id;

    private String serialNumber;

    private String bizSystem;

    private Integer userId;

    private String userName;

    private BigDecimal money;

    private BigDecimal lotteryMoney;
    
    private BigDecimal ffcMoney;
    
    private BigDecimal ffcLotteryMoney;

    private Date belongDate;
    
    private String ffcReleaseStatus;

    private String releaseStatus;

    private String remark;
    
    private Date ffcAppliyDate;

    private Date appliyDate;
    
    private Date createdDate;

    private Date updateDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber == null ? null : serialNumber.trim();
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public BigDecimal getLotteryMoney() {
        return lotteryMoney;
    }

    public void setLotteryMoney(BigDecimal lotteryMoney) {
        this.lotteryMoney = lotteryMoney;
    }
    
    public BigDecimal getFfcMoney() {
        return ffcMoney;
    }

    public void setFfcMoney(BigDecimal ffcMoney) {
        this.ffcMoney = ffcMoney;
    }
    
    public BigDecimal getFfcLotteryMoney() {
        return ffcLotteryMoney;
    }

    public void setFfcLotteryMoney(BigDecimal ffcLotteryMoney) {
        this.ffcLotteryMoney = ffcLotteryMoney;
    }

    public Date getBelongDate() {
        return belongDate;
    }

    public void setBelongDate(Date belongDate) {
        this.belongDate = belongDate;
    }
    
    public String getFfcReleaseStatus() {
        return ffcReleaseStatus;
    }

    public void setFfcReleaseStatus(String ffcReleaseStatus) {
        this.ffcReleaseStatus = ffcReleaseStatus == null ? null : ffcReleaseStatus.trim();
    }

    public String getReleaseStatus() {
        return releaseStatus;
    }

    public void setReleaseStatus(String releaseStatus) {
        this.releaseStatus = releaseStatus == null ? null : releaseStatus.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }
    
    public Date getFfcAppliyDate() {
        return ffcAppliyDate;
    }

    public void setFfcAppliyDate(Date ffcAppliyDate) {
        this.ffcAppliyDate = ffcAppliyDate;
    }

    public Date getAppliyDate() {
		return appliyDate;
	}

	public void setAppliyDate(Date appliyDate) {
		this.appliyDate = appliyDate;
	}

	public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
    
	//申请时间字符串
    public String getAppliyDateStr(){
    	return DateUtils.getDateToStrByFormat(this.getAppliyDate(), "yyyy/MM/dd HH:mm:ss");
    }
    public String getBelongDateStr(){
    	return DateUtils.getDateToStrByFormat(this.getBelongDate(), "yyyy/MM/dd HH:mm:ss");
    }
    public String getBizSystemName() {
        return this.bizSystem==null?"":ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
    }
}