package com.team.lottery.vo;

import java.util.Date;

import com.team.lottery.util.DateUtils;

public class LotteryIssue {
    private Integer id;
    private String lotteryName;
    private String lotteryType;
    private String timeType;
    private Date begintime;
    private Date endtime;
    private Integer restday;
    private Integer issale;
    private String lotteryNum;
    private Integer state;
    private Integer isbd;
    
    //扩展字段
    private Integer specialTime = 0; //是否特殊的时长
    private Integer specialExpect = 0; //是否预售旗号,默认不是预售旗号
    private String lotteryNumCount;
    private Long timeDifference;  //时间差
    private String currentDateStr;
    
    //是否封盘，六合彩用到
    private Boolean isClose = false;
    
    public Boolean getIsClose() {
		return isClose;
	}

	public void setIsClose(Boolean isClose) {
		this.isClose = isClose;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLotteryName() {
        return lotteryName;
    }

    public void setLotteryName(String lotteryName) {
        this.lotteryName = lotteryName == null ? null : lotteryName.trim();
    }

    public String getLotteryType() {
        return lotteryType;
    }

    public void setLotteryType(String lotteryType) {
        this.lotteryType = lotteryType == null ? null : lotteryType.trim();
    }

    public String getTimeType() {
        return timeType;
    }

    public void setTimeType(String timeType) {
        this.timeType = timeType == null ? null : timeType.trim();
    }

    public Date getBegintime() {
        return begintime;
    }

    public void setBegintime(Date begintime) {
        this.begintime = begintime;
    }

    public Date getEndtime() {
        return endtime;
    }

    public void setEndtime(Date endtime) {
        this.endtime = endtime;
    }

    public Integer getRestday() {
        return restday;
    }

    public void setRestday(Integer restday) {
        this.restday = restday;
    }

    public Integer getIssale() {
        return issale;
    }

    public void setIssale(Integer issale) {
        this.issale = issale;
    }

    public String getLotteryNum() {
        return lotteryNum;
    }

    public void setLotteryNum(String lotteryNum) {
        this.lotteryNum = lotteryNum == null ? null : lotteryNum.trim();
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getIsbd() {
        return isbd;
    }

    public void setIsbd(Integer isbd) {
        this.isbd = isbd;
    }

	public Long getTimeDifference() {
		return timeDifference;
	}

	public void setTimeDifference(Long timeDifference) {
		this.timeDifference = timeDifference;
	}
	
	public String getCurrentDateStr() {
		return currentDateStr;
	}

	public void setCurrentDateStr(String currentDateStr) {
		this.currentDateStr = currentDateStr;
	}
	
	public String getLotteryNumCount() {
		return lotteryNumCount;
	}

	public void setLotteryNumCount(String lotteryNumCount) {
		this.lotteryNumCount = lotteryNumCount;
	}

	public String getBegintimeStr(){
    	return DateUtils.getDateToStrByFormat(this.getBegintime(), "yyyy/MM/dd HH:mm:ss");
    }
	
	public String getEndtimeStr(){
		return DateUtils.getDateToStrByFormat(this.getEndtime(), "yyyy/MM/dd HH:mm:ss");
	}

	public Integer getSpecialExpect() {
		return specialExpect;
	}

	public void setSpecialExpect(Integer specialExpect) {
		this.specialExpect = specialExpect;
	}

	public Integer getSpecialTime() {
		return specialTime;
	}

	public void setSpecialTime(Integer specialTime) {
		this.specialTime = specialTime;
	}
}