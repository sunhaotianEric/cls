package com.team.lottery.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.team.lottery.enums.EUserDailLiLevel;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.DateUtils;
import com.team.lottery.util.StringUtils;

public class UserOld {
	
    private Integer id;
    private Integer code;
    private String username;
    private String userpass;
    private String userpass1;
    private String truename;
    private Integer teamMember;  //团队人数
    private Integer lowerLevel;  //下级人数
    private Integer haveSafePassword; 
    private Integer haveSafeQuestion; 
    private String phone;
    private String identityid;
    private String birthday;
    private Integer sex;
    private String address;
    private String qq;
    private String province;
    private String city;
    private Date addtime;
    private Integer dataLock;//账号锁定
    private Integer islock; //资料锁定
    private Integer state;
    private String regfrom;
    private String dailiLevel; //代理级别
    private String vipLevel; //会员等级(黄金会员，铂金会员等)
    private Integer starLevel; //星级
    private BigDecimal point;
    private BigDecimal uermoney;
    private BigDecimal totalUerMoney;
    private BigDecimal icemoney;
    private BigDecimal money;
    private String email;
    private String logip;
    private Integer logins;
    private Date lastlogout;
    private Date logtime;
    private BigDecimal sscrebate;//时时彩
    private BigDecimal ffcrebate;//分分彩
    private BigDecimal syxwrebate;//11选5
    private BigDecimal ksrebate;//快3
    private BigDecimal tbrebate; //骰宝
    private BigDecimal klsfrebate;//快乐十分
    private BigDecimal dpcrebate;//低频彩
    private BigDecimal pk10rebate; //PK10
    private BigDecimal lhcrebate; //六合彩
    private BigDecimal bonusScale;  //分红比例
    private BigDecimal totalNetRecharge; //总共的网银汇款
    private BigDecimal totalQuickRecharge; //总共的快捷充值
    private BigDecimal totalRecharge;
    private BigDecimal totalRechargePresent; //充值赠送
    private BigDecimal totalActivitiesMoney; //活动彩金
    private BigDecimal totalWithdraw;
    private BigDecimal totalFeevalue;  //总手续费
    private BigDecimal totalExtend;
    private BigDecimal totalRegister;
    private BigDecimal totalLottery;
    private BigDecimal totalShopPoint;
    private BigDecimal totalWin;
    private BigDecimal totalRebate;
    private BigDecimal totalPercentage;
    private BigDecimal totalPayTranfer; //支出的转账
    private BigDecimal totalIncomeTranfer; //收入的转账
    private BigDecimal totalHalfMonthBonus;  //总的半月分红
    private BigDecimal totalDayBonus;        //总的日分红 
    private BigDecimal totalMonthSalary;	 //总的月工资 
    private BigDecimal totalDaySalary;		 //总的日工资 
    private BigDecimal totalDayCommission;	 //总的日佣金 
    private BigDecimal remainExtend;  //剩余推广金额
    private Integer version; //版本号
    
    private Integer isOnline; //当前是否在线
    
    private String operationType;  //操作类型,增加还是减少
    private String operationValue; //操作值
    private Integer numberOfLower; //会员的下级人数
    private BigDecimal totalGain;  //总共盈利
    
    //扩展字段
    private String checkCode;  //验证码
    private String surePassword; //确认密码
    private String oldPassword; //旧密码
    private String loginType; //登录方式   手机或者pc
    
    public UserOld(){}

    public UserOld(String userName){
    	this.username = userName;
    }
    
    public UserOld(String userName,String userpass){
    	this.username = userName;
    	this.userpass = userpass;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getUserpass() {
        return userpass;
    }

    public void setUserpass(String userpass) {
        this.userpass = userpass == null ? null : userpass.trim();
    }

    public String getUserpass1() {
        return userpass1;
    }

    public void setUserpass1(String userpass1) {
        this.userpass1 = userpass1 == null ? null : userpass1.trim();
    }

    public String getTruename() {
        return truename;
    }

    public void setTruename(String truename) {
        this.truename = truename == null ? null : truename.trim();
    }

    public String getIdentityid() {
        return identityid;
    }

    public void setIdentityid(String identityid) {
        this.identityid = identityid == null ? null : identityid.trim();
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday == null ? null : birthday.trim();
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq == null ? null : qq.trim();
    }

    public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public Date getAddtime() {
        return addtime;
    }

    public void setAddtime(Date addtime) {
        this.addtime = addtime;
    }

    public Integer getDataLock() {
        return dataLock;
    }

    public void setDataLock(Integer dataLock) {
        this.dataLock = dataLock;
    }

    public Integer getIslock() {
        return islock;
    }

    public void setIslock(Integer islock) {
        this.islock = islock;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getRegfrom() {
        return regfrom;
    }

    public void setRegfrom(String regfrom) {
        this.regfrom = regfrom == null ? null : regfrom.trim();
    }

    public String getDailiLevel() {
		return dailiLevel;
	}

	public void setDailiLevel(String dailiLevel) {
		this.dailiLevel = dailiLevel;
	}
	

	public String getVipLevel() {
		return vipLevel;
	}

	public void setVipLevel(String vipLevel) {
		this.vipLevel = vipLevel;
	}

	public Integer getStarLevel() {
		return starLevel;
	}

	public void setStarLevel(Integer starLevel) {
		this.starLevel = starLevel;
	}

	public BigDecimal getBonusScale() {
		return bonusScale;
	}

	public void setBonusScale(BigDecimal bonusScale) {
		this.bonusScale = bonusScale;
	}

	public BigDecimal getPoint() {
        return point;
    }

    public void setPoint(BigDecimal point) {
        this.point = point;
    }

    public BigDecimal getUermoney() {
        return uermoney;
    }

    public void setUermoney(BigDecimal uermoney) {
        this.uermoney = uermoney;
    }

    public BigDecimal getIcemoney() {
        return icemoney;
    }

    public void setIcemoney(BigDecimal icemoney) {
        this.icemoney = icemoney;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public BigDecimal getTotalUerMoney() {
		return totalUerMoney;
	}

	public void setTotalUerMoney(BigDecimal totalUerMoney) {
		this.totalUerMoney = totalUerMoney;
	}

	public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getLogip() {
        return logip;
    }

    public void setLogip(String logip) {
        this.logip = logip == null ? null : logip.trim();
    }

    public Integer getLogins() {
        return logins;
    }

    public void setLogins(Integer logins) {
        this.logins = logins;
    }

    public Date getLastlogout() {
        return lastlogout;
    }

    public void setLastlogout(Date lastlogout) {
        this.lastlogout = lastlogout;
    }

    public Date getLogtime() {
        return logtime;
    }

    public void setLogtime(Date logtime) {
        this.logtime = logtime;
    }
    
    public BigDecimal getSscrebate() {
		return sscrebate;
	}

	public void setSscrebate(BigDecimal sscrebate) {
		this.sscrebate = sscrebate;
	}

	public BigDecimal getFfcrebate() {
		return ffcrebate;
	}

	public void setFfcrebate(BigDecimal ffcrebate) {
		this.ffcrebate = ffcrebate;
	}

	public BigDecimal getSyxwrebate() {
		return syxwrebate;
	}

	public void setSyxwrebate(BigDecimal syxwrebate) {
		this.syxwrebate = syxwrebate;
	}

	public BigDecimal getKsrebate() {
		return ksrebate;
	}

	public void setKsrebate(BigDecimal ksrebate) {
		this.ksrebate = ksrebate;
	}

	public BigDecimal getKlsfrebate() {
		return klsfrebate;
	}

	public void setKlsfrebate(BigDecimal klsfrebate) {
		this.klsfrebate = klsfrebate;
	}

	public BigDecimal getDpcrebate() {
		return dpcrebate;
	}

	public void setDpcrebate(BigDecimal dpcrebate) {
		this.dpcrebate = dpcrebate;
	}
	
	
	public BigDecimal getTbrebate() {
		return tbrebate;
	}

	public void setTbrebate(BigDecimal tbrebate) {
		this.tbrebate = tbrebate;
	}

	public BigDecimal getPk10rebate() {
		return pk10rebate;
	}

	public void setPk10rebate(BigDecimal pk10rebate) {
		this.pk10rebate = pk10rebate;
	}

	public BigDecimal getLhcrebate() {
		return lhcrebate;
	}

	public void setLhcrebate(BigDecimal lhcrebate) {
		this.lhcrebate = lhcrebate;
	}

	public BigDecimal getTotalRecharge() {
		return totalRecharge;
	}

	public void setTotalRecharge(BigDecimal totalRecharge) {
		this.totalRecharge = totalRecharge;
	}
	
	public BigDecimal getTotalExtend() {
		return totalExtend;
	}

	public void setTotalExtend(BigDecimal totalExtend) {
		this.totalExtend = totalExtend;
	}

	public BigDecimal getTotalRegister() {
		return totalRegister;
	}

	public void setTotalRegister(BigDecimal totalRegister) {
		this.totalRegister = totalRegister;
	}

	public BigDecimal getTotalWithdraw() {
		return totalWithdraw;
	}

	public void setTotalWithdraw(BigDecimal totalWithdraw) {
		this.totalWithdraw = totalWithdraw;
	}

	public BigDecimal getTotalLottery() {
		return totalLottery;
	}

	public void setTotalLottery(BigDecimal totalLottery) {
		this.totalLottery = totalLottery;
	}

	public BigDecimal getTotalShopPoint() {
		return totalShopPoint;
	}

	public void setTotalShopPoint(BigDecimal totalShopPoint) {
		this.totalShopPoint = totalShopPoint;
	}

	public BigDecimal getTotalWin() {
		return totalWin;
	}

	public void setTotalWin(BigDecimal totalWin) {
		this.totalWin = totalWin;
	}

	public BigDecimal getTotalRebate() {
		return totalRebate;
	}

	public void setTotalRebate(BigDecimal totalRebate) {
		this.totalRebate = totalRebate;
	}

	public BigDecimal getTotalPercentage() {
		return totalPercentage;
	}

	public void setTotalPercentage(BigDecimal totalPercentage) {
		this.totalPercentage = totalPercentage;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public String getOperationValue() {
		return operationValue;
	}

	public void setOperationValue(String operationValue) {
		this.operationValue = operationValue;
	}

	public String getCheckCode() {
		return checkCode;
	}

	public void setCheckCode(String checkCode) {
		this.checkCode = checkCode;
	}  

	public String getSurePassword() {
		return surePassword;
	}

	public void setSurePassword(String surePassword) {
		this.surePassword = surePassword;
	}
	
	public BigDecimal getTotalPayTranfer() {
		return totalPayTranfer;
	}

	public void setTotalPayTranfer(BigDecimal totalPayTranfer) {
		this.totalPayTranfer = totalPayTranfer;
	}

	public BigDecimal getTotalIncomeTranfer() {
		return totalIncomeTranfer;
	}

	public void setTotalIncomeTranfer(BigDecimal totalIncomeTranfer) {
		this.totalIncomeTranfer = totalIncomeTranfer;
	}
	
	public BigDecimal getRemainExtend() {
		return remainExtend;
	}

	public void setRemainExtend(BigDecimal remainExtend) {
		this.remainExtend = remainExtend;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}
	public BigDecimal getTotalRechargePresent() {
		return totalRechargePresent;
	}

	public void setTotalRechargePresent(BigDecimal totalRechargePresent) {
		this.totalRechargePresent = totalRechargePresent;
	}

	public BigDecimal getTotalActivitiesMoney() {
		return totalActivitiesMoney;
	}

	public void setTotalActivitiesMoney(BigDecimal totalActivitiesMoney) {
		this.totalActivitiesMoney = totalActivitiesMoney;
	}

	public BigDecimal getTotalNetRecharge() {
		return totalNetRecharge;
	}

	public void setTotalNetRecharge(BigDecimal totalNetRecharge) {
		this.totalNetRecharge = totalNetRecharge;
	}

	public BigDecimal getTotalQuickRecharge() {
		return totalQuickRecharge;
	}

	public void setTotalQuickRecharge(BigDecimal totalQuickRecharge) {
		this.totalQuickRecharge = totalQuickRecharge;
	}
	
	public Integer getHaveSafePassword() {
		return haveSafePassword;
	}

	public void setHaveSafePassword(Integer haveSafePassword) {
		this.haveSafePassword = haveSafePassword;
	}

	public Integer getHaveSafeQuestion() {
		return haveSafeQuestion;
	}

	public void setHaveSafeQuestion(Integer haveSafeQuestion) {
		this.haveSafeQuestion = haveSafeQuestion;
	}

	public BigDecimal getTotalFeevalue() {
		return totalFeevalue;
	}

	public void setTotalFeevalue(BigDecimal totalFeevalue) {
		this.totalFeevalue = totalFeevalue;
	}
	
	public Integer getIsOnline() {
		return isOnline;
	}

	public void setIsOnline(Integer isOnline) {
		this.isOnline = isOnline;
	}
	
	public Integer getTeamMember() {
		return teamMember;
	}

	public void setTeamMember(Integer teamMember) {
		this.teamMember = teamMember;
	}

	public Integer getLowerLevel() {
		return lowerLevel;
	}

	public void setLowerLevel(Integer lowerLevel) {
		this.lowerLevel = lowerLevel;
	}
	

	public BigDecimal getTotalHalfMonthBonus() {
		return totalHalfMonthBonus;
	}

	public void setTotalHalfMonthBonus(BigDecimal totalHalfMonthBonus) {
		this.totalHalfMonthBonus = totalHalfMonthBonus;
	}

	public BigDecimal getTotalDayBonus() {
		return totalDayBonus;
	}

	public void setTotalDayBonus(BigDecimal totalDayBonus) {
		this.totalDayBonus = totalDayBonus;
	}

	public BigDecimal getTotalMonthSalary() {
		return totalMonthSalary;
	}

	public void setTotalMonthSalary(BigDecimal totalMonthSalary) {
		this.totalMonthSalary = totalMonthSalary;
	}

	public BigDecimal getTotalDaySalary() {
		return totalDaySalary;
	}

	public void setTotalDaySalary(BigDecimal totalDaySalary) {
		this.totalDaySalary = totalDaySalary;
	}

	public BigDecimal getTotalDayCommission() {
		return totalDayCommission;
	}

	public void setTotalDayCommission(BigDecimal totalDayCommission) {
		this.totalDayCommission = totalDayCommission;
	}
	
	

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}
	
	

	public String getLoginType() {
		return loginType;
	}

	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}

	/**
	 * 用户团队报表拷贝
	 * @param user
	 */
	public void userReportCopy(User user){
		if(user != null){
			this.setMoney(this.getMoney().add(user.getMoney()));
//			this.setTotalNetRecharge(this.getTotalNetRecharge().add(user.getTotalNetRecharge()));
//			this.setTotalQuickRecharge(this.getTotalQuickRecharge().add(user.getTotalQuickRecharge()));
//			this.setTotalRecharge(this.getTotalRecharge().add(user.getTotalRecharge()));
//			this.setTotalRechargePresent(this.getTotalRechargePresent().add(user.getTotalRechargePresent()));
//			this.setTotalActivitiesMoney(this.getTotalActivitiesMoney().add(user.getTotalActivitiesMoney()));
//			this.setTotalWithdraw(this.getTotalWithdraw().add(user.getTotalWithdraw()));
//			this.setTotalFeevalue(this.getTotalFeevalue().add(user.getTotalFeevalue()));
//			this.setTotalExtend(this.getTotalExtend().add(user.getTotalExtend()));
//			this.setTotalRegister(this.getTotalRegister().add(user.getTotalRegister()));
//			this.setTotalLottery(this.getTotalLottery().add(user.getTotalLottery()));
//			this.setTotalShopPoint(this.getTotalShopPoint().add(user.getTotalShopPoint()));
//			this.setTotalWin(this.getTotalWin().add(user.getTotalWin()));
//			this.setTotalRebate(this.getTotalRebate().add(user.getTotalRebate()));
//			this.setTotalPercentage(this.getTotalPercentage().add(user.getTotalPercentage()));
//			this.setTotalPayTranfer(this.getTotalPayTranfer().add(user.getTotalPayTranfer()));
//			this.setTotalIncomeTranfer(this.getTotalIncomeTranfer().add(user.getTotalIncomeTranfer()));
//			this.setTotalHalfMonthBonus(this.getTotalHalfMonthBonus().add(user.getTotalHalfMonthBonus()));
//			this.setTotalDayBonus(this.getTotalDayBonus().add(user.getTotalDayBonus()));
//			this.setTotalDayCommission(this.getTotalDayCommission().add(user.getTotalDayCommission()));
//			this.setTotalDaySalary(this.getTotalDaySalary().add(user.getTotalDaySalary()));
//			this.setTotalMonthSalary(this.getTotalMonthSalary().add(user.getTotalMonthSalary()));
//			this.setRemainExtend(this.getRemainExtend().add(user.getRemainExtend()));
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
//		if (username == null) {
//			if (other.username != null)
//				return false;
//		} else if (!username.equals(other.username))
//			return false;
		return true;
	}

	//计算会员的下级人数
	public Integer getNumberOfLower() {
		String regFrom = this.getRegfrom();
		if(!StringUtils.isEmpty(regFrom)){
			String [] userNames = regFrom.split(ConstantUtil.REG_FORM_SPLIT);
			return userNames.length - 1; //减去管理员这一层
		}
		return 0;
	}

	//获取代理级别的描述
	public String getDailiLevelStr(){
		if(!StringUtils.isEmpty(this.getDailiLevel())){
			return EUserDailLiLevel.valueOf(this.getDailiLevel()).getDescription();
		}
		return "";
	}
	
	/**
	 * 计算总共盈利
	 *  盈利(充值赠送  + 系统续费 + 增加的推广费用 + 注册所得 + 中奖 + 返点 + 提成 + 收入转账 - 投注  - 支出转账)
	 * @return
	 */
	public BigDecimal getTotalGain() {
		return this.getTotalRechargePresent().add(this.getTotalRecharge()).add(this.getRemainExtend())
			   .add(this.getTotalRegister()).add(this.getTotalWin()).add(this.getTotalRebate())
			   .add(this.getTotalPercentage()).add(this.getTotalIncomeTranfer()).add(this.getTotalActivitiesMoney())
			   .add(this.getTotalHalfMonthBonus()).add(this.getTotalDayBonus()).add(this.getTotalDayCommission()).add(this.getTotalDaySalary()).add(this.getTotalMonthSalary())
			   .subtract(this.getTotalLottery()).subtract(this.getTotalShopPoint()).subtract(this.getTotalPayTranfer());
	}

	//注册时间字符串
    public String getAddTimeStr(){
    	return DateUtils.getDateToStrByFormat(this.getAddtime(), "yyyy/MM/dd HH:mm:ss");
    }
    
    //登录时间字符串
    public String getLogTimeStr(){
    	return DateUtils.getDateToStrByFormat(this.getLogtime(), "yyyy/MM/dd HH:mm:ss");
    }
    
    //退出登录时间字符串
    public String getLastlogoutTimeStr(){
    	return DateUtils.getDateToStrByFormat(this.getLastlogout(), "yyyy/MM/dd HH:mm:ss");
    }
    
//	/**
//	 * 获取用户的可提现金额
//	 * win + rebate + percentage + bonus = recharge - withdraw -  extend - lottery - shop_point
//	 *  -  register;
//	 * @return
//	 */
//	public BigDecimal getCanMention(){
//       return   this.getTotalRecharge()
//    		    .subtract(this.getTotalWithdraw())
//    		    .subtract(this.getTotalExtend())
//    		    .subtract(this.getTotalLottery())
//    		    .subtract(this.getTotalShopPoint())
//    		    .subtract(this.getTotalRegister());
//	}
//	
//	/**
//	 * 不可提现金额
//	 * withdraw + extend + lottery + shop_point + register =  recharge - win - rebate - percentage - bonus;
//	 * @return
//	 */
//	public BigDecimal getCanNotMention(){
//	       return   this.getTotalRecharge()
//	    		    .subtract(this.getTotalWin())
//	    		    .subtract(this.getTotalRebate())
//	    		    .subtract(this.getTotalPercentage())
//	    		    .subtract(this.getTotalBonus());
//		}

}