package com.team.lottery.vo;

import java.math.BigDecimal;
import java.sql.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.util.DateUtils;

public class UserMoneyTotal {
	
    private Long id;

    private String bizSystem;

    private Integer userId;
    
    private String userName;
    
    private Date startTime;
    
    private Date endTime;
    /**
     * 总网银转账充值
     */
    private BigDecimal totalBankTransferRecharge;

    /**
     * 总快捷充值
     */
    private BigDecimal totalQuickPayRecharge;

    /**
     * 总支付宝充值
     */
    private BigDecimal totalAlipayRecharge;

    /**
     * 总微信充值
     */
    private BigDecimal totalWeixinRecharge;

    /**
     * 总共的充值数目
     */
    private BigDecimal totalRecharge;
    /**
     * 人工存款
     */
    private BigDecimal totalManualRecharge;

    /**
     * 充值赠送的资金
     */
    private BigDecimal totalRechargePresent;

    /**
     * 活动彩金
     */
    private BigDecimal totalActivitiesMoney;
    
    /**
     * 系统续费
     */
    private BigDecimal totalSystemAddMoney;

    /**
     * 系统扣费
     */
    private BigDecimal totalSystemReduceMoney;
    
    /**
     * 行政提出
     */
    private BigDecimal totalManageReduceMoney;

    /**
     * 总共的取款数目
     */
    private BigDecimal totalWithdraw;

    /**
     * 支付的手续费
     */
    private BigDecimal totalWithdarwFee;

    /**
     * 总共推广金额
     */
    private BigDecimal totalExtend;

    /**
     * 注册所获得的资金
     */
    private BigDecimal totalRegister;

    /**
     * 总投注
     */
    private BigDecimal totalLottery;
    
    /**
     * 总追号撤单
     */
    private BigDecimal totalLotteryStopAfterNumber;

    /**
     * 总投注回退
     */
    private BigDecimal totalLotteryRegression;

    /**
     * 总中奖
     */
    private BigDecimal totalWin;

    /**
     * 总共的返点数目
     */
    private BigDecimal totalRebate;

    /**
     *总共的提成金额数目
     */
    private BigDecimal totalPercentage;

    /**
     * 转账至下级(支出)
     */
    private BigDecimal totalPayTranfer;

    /**
     * 上级转账(收入，转账至我的)
     */
    private BigDecimal totalIncomeTranfer;

    /**
     * 总共的半月分红
     */
    private BigDecimal totalHalfMonthBonus;

    /**
     * 总共的日分红
     */
    private BigDecimal totalDayBonus;

    /**
     * 总共的月工资
     */
    private BigDecimal totalMonthSalary;

    /**
     * 总共的日工资
     */
    private BigDecimal totalDaySalary;

    /**
     * 总共的日佣金
     */
    private BigDecimal totalDayCommission;
    
    /**
     * 总其他金额
     */
    private BigDecimal totalOther;

    /**
     * 版本号
     */
    private Long version;
    
    
    private Integer enabled;

    /**
     * 
     * 扩展字段
     */
    
    /**
     *全部金额
     */
     private BigDecimal money;
     
     /**
      *盈利
      */
      private BigDecimal gain;
    
     //拓展字段
    private String teamLeaderName; //团队领导人名字
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

	public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public BigDecimal getTotalBankTransferRecharge() {
		return totalBankTransferRecharge;
	}

	public void setTotalBankTransferRecharge(BigDecimal totalBankTransferRecharge) {
		this.totalBankTransferRecharge = totalBankTransferRecharge;
	}

	public BigDecimal getTotalQuickPayRecharge() {
		return totalQuickPayRecharge;
	}

	public void setTotalQuickPayRecharge(BigDecimal totalQuickPayRecharge) {
		this.totalQuickPayRecharge = totalQuickPayRecharge;
	}

	public BigDecimal getTotalAlipayRecharge() {
        return totalAlipayRecharge;
    }

    public void setTotalAlipayRecharge(BigDecimal totalAlipayRecharge) {
        this.totalAlipayRecharge = totalAlipayRecharge;
    }

    public BigDecimal getTotalWeixinRecharge() {
        return totalWeixinRecharge;
    }

    public void setTotalWeixinRecharge(BigDecimal totalWeixinRecharge) {
        this.totalWeixinRecharge = totalWeixinRecharge;
    }

    public BigDecimal getTotalManualRecharge() {
		return totalManualRecharge;
	}

	public void setTotalManualRecharge(BigDecimal totalManualRecharge) {
		this.totalManualRecharge = totalManualRecharge;
	}

	public BigDecimal getTotalManageReduceMoney() {
		return totalManageReduceMoney;
	}

	public void setTotalManageReduceMoney(BigDecimal totalManageReduceMoney) {
		this.totalManageReduceMoney = totalManageReduceMoney;
	}

	public BigDecimal getTotalRecharge() {
        return totalRecharge;
    }

    public void setTotalRecharge(BigDecimal totalRecharge) {
        this.totalRecharge = totalRecharge;
    }

    public BigDecimal getTotalRechargePresent() {
        return totalRechargePresent;
    }

    public void setTotalRechargePresent(BigDecimal totalRechargePresent) {
        this.totalRechargePresent = totalRechargePresent;
    }

    public BigDecimal getTotalActivitiesMoney() {
        return totalActivitiesMoney;
    }

    public void setTotalActivitiesMoney(BigDecimal totalActivitiesMoney) {
        this.totalActivitiesMoney = totalActivitiesMoney;
    }

    public BigDecimal getTotalWithdraw() {
        return totalWithdraw;
    }

    public void setTotalWithdraw(BigDecimal totalWithdraw) {
        this.totalWithdraw = totalWithdraw;
    }

    public BigDecimal getTotalWithdarwFee() {
		return totalWithdarwFee;
	}

	public void setTotalWithdarwFee(BigDecimal totalWithdarwFee) {
		this.totalWithdarwFee = totalWithdarwFee;
	}

	public BigDecimal getTotalExtend() {
        return totalExtend;
    }

    public void setTotalExtend(BigDecimal totalExtend) {
        this.totalExtend = totalExtend;
    }

    public BigDecimal getTotalRegister() {
        return totalRegister;
    }

    public void setTotalRegister(BigDecimal totalRegister) {
        this.totalRegister = totalRegister;
    }

    public BigDecimal getTotalLottery() {
        return totalLottery;
    }

    public void setTotalLottery(BigDecimal totalLottery) {
        this.totalLottery = totalLottery;
    }

    public BigDecimal getTotalWin() {
        return totalWin;
    }

    public void setTotalWin(BigDecimal totalWin) {
        this.totalWin = totalWin;
    }

    public BigDecimal getTotalRebate() {
        return totalRebate;
    }

    public void setTotalRebate(BigDecimal totalRebate) {
        this.totalRebate = totalRebate;
    }

    public BigDecimal getTotalPercentage() {
        return totalPercentage;
    }

    public void setTotalPercentage(BigDecimal totalPercentage) {
        this.totalPercentage = totalPercentage;
    }

    public BigDecimal getTotalPayTranfer() {
        return totalPayTranfer;
    }

    public void setTotalPayTranfer(BigDecimal totalPayTranfer) {
        this.totalPayTranfer = totalPayTranfer;
    }

    public BigDecimal getTotalIncomeTranfer() {
        return totalIncomeTranfer;
    }

    public void setTotalIncomeTranfer(BigDecimal totalIncomeTranfer) {
        this.totalIncomeTranfer = totalIncomeTranfer;
    }

    public BigDecimal getTotalHalfMonthBonus() {
        return totalHalfMonthBonus;
    }

    public void setTotalHalfMonthBonus(BigDecimal totalHalfMonthBonus) {
        this.totalHalfMonthBonus = totalHalfMonthBonus;
    }

    public BigDecimal getTotalDayBonus() {
        return totalDayBonus;
    }

    public void setTotalDayBonus(BigDecimal totalDayBonus) {
        this.totalDayBonus = totalDayBonus;
    }

    public BigDecimal getTotalMonthSalary() {
        return totalMonthSalary;
    }

    public void setTotalMonthSalary(BigDecimal totalMonthSalary) {
        this.totalMonthSalary = totalMonthSalary;
    }

    public BigDecimal getTotalDaySalary() {
        return totalDaySalary;
    }

    public void setTotalDaySalary(BigDecimal totalDaySalary) {
        this.totalDaySalary = totalDaySalary;
    }

    public BigDecimal getTotalDayCommission() {
        return totalDayCommission;
    }

    public void setTotalDayCommission(BigDecimal totalDayCommission) {
        this.totalDayCommission = totalDayCommission;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }
    
	public BigDecimal getTotalSystemAddMoney() {
		return totalSystemAddMoney;
	}

	public void setTotalSystemAddMoney(BigDecimal totalSystemAddMoney) {
		this.totalSystemAddMoney = totalSystemAddMoney;
	}

	public BigDecimal getTotalSystemReduceMoney() {
		return totalSystemReduceMoney;
	}

	public void setTotalSystemReduceMoney(BigDecimal totalSystemReduceMoney) {
		this.totalSystemReduceMoney = totalSystemReduceMoney;
	}

	public BigDecimal getTotalLotteryStopAfterNumber() {
		return totalLotteryStopAfterNumber;
	}

	public void setTotalLotteryStopAfterNumber(BigDecimal totalLotteryStopAfterNumber) {
		this.totalLotteryStopAfterNumber = totalLotteryStopAfterNumber;
	}

	public BigDecimal getTotalLotteryRegression() {
		return totalLotteryRegression;
	}

	public void setTotalLotteryRegression(BigDecimal totalLotteryRegression) {
		this.totalLotteryRegression = totalLotteryRegression;
	}

	public BigDecimal getTotalOther() {
		return totalOther;
	}

	public void setTotalOther(BigDecimal totalOther) {
		this.totalOther = totalOther;
	}

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}
	
	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date starTime) {
		this.startTime = starTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	
	

	public BigDecimal getGain() {
		return gain;
	}

	public void setGain(BigDecimal gain) {
		this.gain = gain;
	}

	/**
	 * 获取时间字符串
	 * @return
	 */
	public String getStarDateStr(){
    	return DateUtils.getDateToStrByFormat(this.getStartTime(), "yyyy-MM-dd HH:mm:ss");
    }
	
	public String getEndDateStr(){
    	return DateUtils.getDateToStrByFormat(this.getEndTime(), "yyyy-MM-dd HH:mm:ss");
    }
	
	public String getTeamLeaderName() {
		return teamLeaderName;
	}

	public void setTeamLeaderName(String teamLeaderName) {
		this.teamLeaderName = teamLeaderName;
	}

	public String getBizSystemName() {
        return this.bizSystem==null?"":ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
	}
	 
	 /**
	  * 增加金额，money+|addMoney|
	  * @param money
	  * @param addMoney
	  */
	 public BigDecimal addMoney(BigDecimal money,BigDecimal addMoney){
		 if(addMoney.compareTo(BigDecimal.ZERO)>=0){
			return  money.add(addMoney);
		 }else{
			return  money.subtract(addMoney); 
		 }
	 }
	 
	 
	 /**
	  * 减少金额，money+|addMoney|
	  * @param money
	  * @param addMoney
	  */
	 public BigDecimal subMoney(BigDecimal money,BigDecimal subMoney){
		 if(subMoney.compareTo(BigDecimal.ZERO)<0){
			return  money.add(subMoney);
		 }else{
			return  money.subtract(subMoney); 
		 }
	 }
}