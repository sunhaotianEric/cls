package com.team.lottery.vo;

import java.math.BigDecimal;
import java.util.Date;

public class UserDayMoney {
    private Long id;

    private Integer userId;

    private String userName;

    private String regfrom;

    private BigDecimal teamMoney;

    private BigDecimal money;

    private Date belongDate;

    private Date createDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getRegfrom() {
        return regfrom;
    }

    public void setRegfrom(String regfrom) {
        this.regfrom = regfrom == null ? null : regfrom.trim();
    }

    public BigDecimal getTeamMoney() {
        return teamMoney;
    }

    public void setTeamMoney(BigDecimal teamMoney) {
        this.teamMoney = teamMoney;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public Date getBelongDate() {
        return belongDate;
    }

    public void setBelongDate(Date belongDate) {
        this.belongDate = belongDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}