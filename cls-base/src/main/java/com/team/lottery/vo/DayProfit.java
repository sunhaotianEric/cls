package com.team.lottery.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.util.ConstantUtil;
public class DayProfit {
    private Long id;

    private String bizSystem;

    private BigDecimal recharge = BigDecimal.ZERO;

    private Integer rechargeCount = 0;

    private Integer rechargeUserCount = 0;

    private BigDecimal rechargeBankTransfer = BigDecimal.ZERO;

    private Integer rechargeBankTransferUserCount = 0;

    private BigDecimal rechargeQuickPay = BigDecimal.ZERO;

    private Integer rechargeQuickPayUserCount = 0;

    private BigDecimal rechargeWeixin = BigDecimal.ZERO;

    private Integer rechargeWeixinUserCount = 0;

    private BigDecimal rechargeAlipay = BigDecimal.ZERO;

    private Integer rechargeAlipayUserCount = 0;
    private BigDecimal rechargeQqpay = BigDecimal.ZERO;

    private Integer rechargeQqpayUserCount = 0;

    private BigDecimal rechargeJdpay = BigDecimal.ZERO;

    private Integer rechargeJdpayUserCount = 0;

    private BigDecimal rechargeUnionpay = BigDecimal.ZERO;

    private Integer rechargeUnionpayUserCount = 0;

    private BigDecimal rechargeManual = BigDecimal.ZERO;
    
    private Integer rechargeManualUserCount = 0;
    
    private BigDecimal rechargePresent = BigDecimal.ZERO;

    private Integer rechargePresentUserCount = 0;

    private BigDecimal activitiesMoney = BigDecimal.ZERO;

    private Integer activitiesMoneyUserCount = 0;

    private BigDecimal systemAddMoney = BigDecimal.ZERO;

    private Integer systemAddMoneyUserCount = 0;

    private BigDecimal systemReduceMoney = BigDecimal.ZERO;

    private Integer systemReduceMoneyUserCount = 0;
    
    private BigDecimal manageReduceMoney = BigDecimal.ZERO;
    
    private Integer manageReduceMoneyUserCount = 0;
    
    private BigDecimal withdraw = BigDecimal.ZERO;

    private Integer withdrawCount = 0;

    private Integer withdrawUserCount = 0;

    private BigDecimal withdrawFee = BigDecimal.ZERO;

    private Integer withdrawFeeUserCount = 0;

    private BigDecimal lottery = BigDecimal.ZERO;

    private Integer lotteryNumCount = 0;

    private Integer lotteryUserCount = 0;

    private BigDecimal win = BigDecimal.ZERO;

    private Integer winUserCount = 0;

    private BigDecimal percentage = BigDecimal.ZERO;

    private Integer percentageUserCount = 0;

    private BigDecimal rebate = BigDecimal.ZERO;

    private Integer rebateUserCount = 0;

    private BigDecimal lotteryRegression = BigDecimal.ZERO;

    private Integer lotteryRegressionUserCount = 0;

    private BigDecimal lotteryStopAfterNumber = BigDecimal.ZERO;

    private Integer lotteryStopAfterNumberUserCount = 0;

    private BigDecimal halfMonthBonus = BigDecimal.ZERO;

    private Integer halfMonthBonusUserCount = 0;

    private BigDecimal dayBonus = BigDecimal.ZERO;

    private BigDecimal monthSalary = BigDecimal.ZERO;

    private BigDecimal daySalary = BigDecimal.ZERO;

    private Integer daySalaryUserCount = 0;

    private BigDecimal dayCommission = BigDecimal.ZERO;

    private BigDecimal dayActivity = BigDecimal.ZERO;

    private BigDecimal transfer = BigDecimal.ZERO;

    private BigDecimal regularmembersMoney = BigDecimal.ZERO;

    private BigDecimal agencyMoney = BigDecimal.ZERO;

    private Integer newaddUserCount = 0;
    
    private Integer firstRechargeUserCount = 0;

    private BigDecimal lotteryGain = BigDecimal.ZERO;

    private BigDecimal winGain = BigDecimal.ZERO;

    private BigDecimal moneyGain = BigDecimal.ZERO;

    private BigDecimal gain = BigDecimal.ZERO;

    private Date belongDate;

    private Date createdDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public BigDecimal getRecharge() {
        return recharge;
    }

    public void setRecharge(BigDecimal recharge) {
        this.recharge = recharge;
    }

    public Integer getRechargeCount() {
        return rechargeCount;
    }

    public void setRechargeCount(Integer rechargeCount) {
        this.rechargeCount = rechargeCount;
    }

    public Integer getRechargeUserCount() {
        return rechargeUserCount;
    }

    public Integer getRechargeManualUserCount() {
		return rechargeManualUserCount;
	}

	public void setRechargeManualUserCount(Integer rechargeManualUserCount) {
		this.rechargeManualUserCount = rechargeManualUserCount;
	}

	public Integer getManageReduceMoneyUserCount() {
		return manageReduceMoneyUserCount;
	}

	public void setManageReduceMoneyUserCount(Integer manageReduceMoneyUserCount) {
		this.manageReduceMoneyUserCount = manageReduceMoneyUserCount;
	}

	public void setRechargeUserCount(Integer rechargeUserCount) {
        this.rechargeUserCount = rechargeUserCount;
    }

    public BigDecimal getRechargeManual() {
		return rechargeManual;
	}

	public void setRechargeManual(BigDecimal rechargeManual) {
		this.rechargeManual = rechargeManual;
	}

	public BigDecimal getManageReduceMoney() {
		return manageReduceMoney;
	}

	public void setManageReduceMoney(BigDecimal manageReduceMoney) {
		this.manageReduceMoney = manageReduceMoney;
	}

	public BigDecimal getRechargeBankTransfer() {
        return rechargeBankTransfer;
    }

    public void setRechargeBankTransfer(BigDecimal rechargeBankTransfer) {
        this.rechargeBankTransfer = rechargeBankTransfer;
    }

    public Integer getRechargeBankTransferUserCount() {
        return rechargeBankTransferUserCount;
    }

    public void setRechargeBankTransferUserCount(Integer rechargeBankTransferUserCount) {
        this.rechargeBankTransferUserCount = rechargeBankTransferUserCount;
    }

    public BigDecimal getRechargeQuickPay() {
        return rechargeQuickPay;
    }

    public void setRechargeQuickPay(BigDecimal rechargeQuickPay) {
        this.rechargeQuickPay = rechargeQuickPay;
    }

    public Integer getRechargeQuickPayUserCount() {
        return rechargeQuickPayUserCount;
    }

    public void setRechargeQuickPayUserCount(Integer rechargeQuickPayUserCount) {
        this.rechargeQuickPayUserCount = rechargeQuickPayUserCount;
    }

    public BigDecimal getRechargeWeixin() {
        return rechargeWeixin;
    }

    public void setRechargeWeixin(BigDecimal rechargeWeixin) {
        this.rechargeWeixin = rechargeWeixin;
    }

    public Integer getRechargeWeixinUserCount() {
        return rechargeWeixinUserCount;
    }

    public void setRechargeWeixinUserCount(Integer rechargeWeixinUserCount) {
        this.rechargeWeixinUserCount = rechargeWeixinUserCount;
    }

    public BigDecimal getRechargeAlipay() {
        return rechargeAlipay;
    }

    public void setRechargeAlipay(BigDecimal rechargeAlipay) {
        this.rechargeAlipay = rechargeAlipay;
    }

    public Integer getRechargeAlipayUserCount() {
        return rechargeAlipayUserCount;
    }

    public void setRechargeAlipayUserCount(Integer rechargeAlipayUserCount) {
        this.rechargeAlipayUserCount = rechargeAlipayUserCount;
    }

    public BigDecimal getRechargeQqpay() {
		return rechargeQqpay;
	}

	public void setRechargeQqpay(BigDecimal rechargeQqpay) {
		this.rechargeQqpay = rechargeQqpay;
	}

	public Integer getRechargeQqpayUserCount() {
		return rechargeQqpayUserCount;
	}

	public void setRechargeQqpayUserCount(Integer rechargeQqpayUserCount) {
		this.rechargeQqpayUserCount = rechargeQqpayUserCount;
	}

	public BigDecimal getRechargeJdpay() {
		return rechargeJdpay;
	}

	public void setRechargeJdpay(BigDecimal rechargeJdpay) {
		this.rechargeJdpay = rechargeJdpay;
	}

	public Integer getRechargeJdpayUserCount() {
		return rechargeJdpayUserCount;
	}

	public void setRechargeJdpayUserCount(Integer rechargeJdpayUserCount) {
		this.rechargeJdpayUserCount = rechargeJdpayUserCount;
	}

	public BigDecimal getRechargeUnionpay() {
		return rechargeUnionpay;
	}

	public void setRechargeUnionpay(BigDecimal rechargeUnionpay) {
		this.rechargeUnionpay = rechargeUnionpay;
	}

	public Integer getRechargeUnionpayUserCount() {
		return rechargeUnionpayUserCount;
	}

	public void setRechargeUnionpayUserCount(Integer rechargeUnionpayUserCount) {
		this.rechargeUnionpayUserCount = rechargeUnionpayUserCount;
	}

	public BigDecimal getRechargePresent() {
        return rechargePresent;
    }

    public void setRechargePresent(BigDecimal rechargePresent) {
        this.rechargePresent = rechargePresent;
    }

    public Integer getRechargePresentUserCount() {
        return rechargePresentUserCount;
    }

    public void setRechargePresentUserCount(Integer rechargePresentUserCount) {
        this.rechargePresentUserCount = rechargePresentUserCount;
    }

    public BigDecimal getActivitiesMoney() {
        return activitiesMoney;
    }

    public void setActivitiesMoney(BigDecimal activitiesMoney) {
        this.activitiesMoney = activitiesMoney;
    }

    public Integer getActivitiesMoneyUserCount() {
        return activitiesMoneyUserCount;
    }

    public void setActivitiesMoneyUserCount(Integer activitiesMoneyUserCount) {
        this.activitiesMoneyUserCount = activitiesMoneyUserCount;
    }

    public BigDecimal getSystemAddMoney() {
        return systemAddMoney;
    }

    public void setSystemAddMoney(BigDecimal systemAddMoney) {
        this.systemAddMoney = systemAddMoney;
    }

    public Integer getSystemAddMoneyUserCount() {
        return systemAddMoneyUserCount;
    }

    public void setSystemAddMoneyUserCount(Integer systemAddMoneyUserCount) {
        this.systemAddMoneyUserCount = systemAddMoneyUserCount;
    }

    public BigDecimal getSystemReduceMoney() {
        return systemReduceMoney;
    }

    public void setSystemReduceMoney(BigDecimal systemReduceMoney) {
        this.systemReduceMoney = systemReduceMoney;
    }

    public Integer getSystemReduceMoneyUserCount() {
        return systemReduceMoneyUserCount;
    }

    public void setSystemReduceMoneyUserCount(Integer systemReduceMoneyUserCount) {
        this.systemReduceMoneyUserCount = systemReduceMoneyUserCount;
    }

    public BigDecimal getWithdraw() {
        return withdraw;
    }

    public void setWithdraw(BigDecimal withdraw) {
        this.withdraw = withdraw;
    }

    public Integer getWithdrawCount() {
        return withdrawCount;
    }

    public void setWithdrawCount(Integer withdrawCount) {
        this.withdrawCount = withdrawCount;
    }

    public Integer getWithdrawUserCount() {
        return withdrawUserCount;
    }

    public void setWithdrawUserCount(Integer withdrawUserCount) {
        this.withdrawUserCount = withdrawUserCount;
    }

    public BigDecimal getWithdrawFee() {
        return withdrawFee;
    }

    public void setWithdrawFee(BigDecimal withdrawFee) {
        this.withdrawFee = withdrawFee;
    }

    public Integer getWithdrawFeeUserCount() {
        return withdrawFeeUserCount;
    }

    public void setWithdrawFeeUserCount(Integer withdrawFeeUserCount) {
        this.withdrawFeeUserCount = withdrawFeeUserCount;
    }

    public BigDecimal getLottery() {
        return lottery;
    }

    public void setLottery(BigDecimal lottery) {
        this.lottery = lottery;
    }

    public Integer getLotteryNumCount() {
        return lotteryNumCount;
    }

    public void setLotteryNumCount(Integer lotteryNumCount) {
        this.lotteryNumCount = lotteryNumCount;
    }

    public Integer getLotteryUserCount() {
        return lotteryUserCount;
    }

    public void setLotteryUserCount(Integer lotteryUserCount) {
        this.lotteryUserCount = lotteryUserCount;
    }

    public BigDecimal getWin() {
        return win;
    }

    public void setWin(BigDecimal win) {
        this.win = win;
    }

    public Integer getWinUserCount() {
        return winUserCount;
    }

    public void setWinUserCount(Integer winUserCount) {
        this.winUserCount = winUserCount;
    }

    public BigDecimal getPercentage() {
        return percentage;
    }

    public void setPercentage(BigDecimal percentage) {
        this.percentage = percentage;
    }

    public Integer getPercentageUserCount() {
        return percentageUserCount;
    }

    public void setPercentageUserCount(Integer percentageUserCount) {
        this.percentageUserCount = percentageUserCount;
    }

    public BigDecimal getRebate() {
        return rebate;
    }

    public void setRebate(BigDecimal rebate) {
        this.rebate = rebate;
    }

    public Integer getRebateUserCount() {
        return rebateUserCount;
    }

    public void setRebateUserCount(Integer rebateUserCount) {
        this.rebateUserCount = rebateUserCount;
    }

    public BigDecimal getLotteryRegression() {
        return lotteryRegression;
    }

    public void setLotteryRegression(BigDecimal lotteryRegression) {
        this.lotteryRegression = lotteryRegression;
    }

    public Integer getLotteryRegressionUserCount() {
        return lotteryRegressionUserCount;
    }

    public void setLotteryRegressionUserCount(Integer lotteryRegressionUserCount) {
        this.lotteryRegressionUserCount = lotteryRegressionUserCount;
    }

    public BigDecimal getLotteryStopAfterNumber() {
        return lotteryStopAfterNumber;
    }

    public void setLotteryStopAfterNumber(BigDecimal lotteryStopAfterNumber) {
        this.lotteryStopAfterNumber = lotteryStopAfterNumber;
    }

    public Integer getLotteryStopAfterNumberUserCount() {
        return lotteryStopAfterNumberUserCount;
    }

    public void setLotteryStopAfterNumberUserCount(Integer lotteryStopAfterNumberUserCount) {
        this.lotteryStopAfterNumberUserCount = lotteryStopAfterNumberUserCount;
    }

    public BigDecimal getHalfMonthBonus() {
        return halfMonthBonus;
    }

    public void setHalfMonthBonus(BigDecimal halfMonthBonus) {
        this.halfMonthBonus = halfMonthBonus;
    }

    public Integer getHalfMonthBonusUserCount() {
        return halfMonthBonusUserCount;
    }

    public void setHalfMonthBonusUserCount(Integer halfMonthBonusUserCount) {
        this.halfMonthBonusUserCount = halfMonthBonusUserCount;
    }

    public BigDecimal getDayBonus() {
        return dayBonus;
    }

    public void setDayBonus(BigDecimal dayBonus) {
        this.dayBonus = dayBonus;
    }

    public BigDecimal getMonthSalary() {
        return monthSalary;
    }

    public void setMonthSalary(BigDecimal monthSalary) {
        this.monthSalary = monthSalary;
    }

    public BigDecimal getDaySalary() {
        return daySalary;
    }

    public void setDaySalary(BigDecimal daySalary) {
        this.daySalary = daySalary;
    }

    public Integer getDaySalaryUserCount() {
        return daySalaryUserCount;
    }

    public void setDaySalaryUserCount(Integer daySalaryUserCount) {
        this.daySalaryUserCount = daySalaryUserCount;
    }

    public BigDecimal getDayCommission() {
        return dayCommission;
    }

    public void setDayCommission(BigDecimal dayCommission) {
        this.dayCommission = dayCommission;
    }

    public BigDecimal getDayActivity() {
        return dayActivity;
    }

    public void setDayActivity(BigDecimal dayActivity) {
        this.dayActivity = dayActivity;
    }

    public BigDecimal getTransfer() {
        return transfer;
    }

    public void setTransfer(BigDecimal transfer) {
        this.transfer = transfer;
    }

    public BigDecimal getRegularmembersMoney() {
        return regularmembersMoney;
    }

    public void setRegularmembersMoney(BigDecimal regularmembersMoney) {
        this.regularmembersMoney = regularmembersMoney;
    }

    public BigDecimal getAgencyMoney() {
        return agencyMoney;
    }

    public void setAgencyMoney(BigDecimal agencyMoney) {
        this.agencyMoney = agencyMoney;
    }

    public Integer getNewaddUserCount() {
        return newaddUserCount;
    }

    public void setNewaddUserCount(Integer newaddUserCount) {
        this.newaddUserCount = newaddUserCount;
    }
    
    public Integer getFirstRechargeUserCount() {
        return firstRechargeUserCount;
    }

    public void setFirstRechargeUserCount(Integer firstRechargeUserCount) {
        this.firstRechargeUserCount = firstRechargeUserCount;
    }

    public BigDecimal getLotteryGain() {
        return lotteryGain;
    }

    public void setLotteryGain(BigDecimal lotteryGain) {
        this.lotteryGain = lotteryGain;
    }

    public BigDecimal getWinGain() {
        return winGain;
    }

    public void setWinGain(BigDecimal winGain) {
        this.winGain = winGain;
    }

    public BigDecimal getMoneyGain() {
        return moneyGain;
    }

    public void setMoneyGain(BigDecimal moneyGain) {
        this.moneyGain = moneyGain;
    }

    public BigDecimal getGain() {
        return gain;
    }

    public void setGain(BigDecimal gain) {
        this.gain = gain;
    }

    public Date getBelongDate() {
        return belongDate;
    }

    public void setBelongDate(Date belongDate) {
        this.belongDate = belongDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
    
	public String getBizSystemName() {
    	
        return this.bizSystem==null?"":ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
    }
    
    /**
     * 合并盈亏报表
     */
    public void addDayProfit(DayProfit dayProfit){
    	this.setActivitiesMoney(this.activitiesMoney.add(dayProfit.getActivitiesMoney()==null?BigDecimal.ZERO:dayProfit.getActivitiesMoney()));
    	this.setActivitiesMoneyUserCount(this.activitiesMoneyUserCount+dayProfit.getActivitiesMoneyUserCount());
    	this.setAgencyMoney(this.agencyMoney.add(dayProfit.getAgencyMoney()==null?BigDecimal.ZERO:dayProfit.getAgencyMoney()));
    	this.setDayActivity(this.dayActivity.add(dayProfit.getDayActivity()==null?BigDecimal.ZERO:dayProfit.getDayActivity()));
    	this.setDayBonus(this.dayBonus.add(dayProfit.getDayBonus()==null?BigDecimal.ZERO:dayProfit.getDayBonus()));
    	
    	this.setDayCommission(this.dayCommission.add(dayProfit.getDayCommission()==null?BigDecimal.ZERO:dayProfit.getDayCommission()));
    	this.setDaySalary(this.daySalary.add(dayProfit.getDaySalary()==null?BigDecimal.ZERO:dayProfit.getDaySalary()));
    	this.setDaySalaryUserCount(this.daySalaryUserCount+dayProfit.getDaySalaryUserCount());
    	this.setGain(this.gain.add(dayProfit.getGain()==null?BigDecimal.ZERO:dayProfit.getGain()));
    	this.setHalfMonthBonus(this.halfMonthBonus.add(dayProfit.getHalfMonthBonus()==null?BigDecimal.ZERO:dayProfit.getHalfMonthBonus()));
    	this.setLottery(this.lottery.add(dayProfit.getLottery()==null?BigDecimal.ZERO:dayProfit.getLottery()));
   
    	this.setLotteryGain(this.lotteryGain.add(dayProfit.getLotteryGain()==null?BigDecimal.ZERO:dayProfit.getLotteryGain()));
    	
    	this.setLotteryNumCount(this.lotteryNumCount+dayProfit.getLotteryNumCount());
    	this.setLotteryRegression(this.lotteryRegression.add(dayProfit.getLotteryRegression()==null?BigDecimal.ZERO:dayProfit.getLotteryRegression()));
    	this.setLotteryRegressionUserCount(this.lotteryRegressionUserCount+dayProfit.getLotteryRegressionUserCount());
    	this.setLotteryStopAfterNumber(this.lotteryStopAfterNumber.add(dayProfit.getLotteryStopAfterNumber()==null?BigDecimal.ZERO:dayProfit.getLotteryStopAfterNumber()));
    	this.setLotteryStopAfterNumberUserCount(this.lotteryStopAfterNumberUserCount+dayProfit.getLotteryStopAfterNumberUserCount());
    	
    	this.setLotteryUserCount(this.lotteryUserCount+dayProfit.getLotteryUserCount());
    	this.setMoneyGain(this.moneyGain.add(dayProfit.getMoneyGain()==null?BigDecimal.ZERO:dayProfit.getMoneyGain()));
    	this.setMonthSalary(this.monthSalary.add(dayProfit.getMonthSalary()==null?BigDecimal.ZERO:dayProfit.getMonthSalary()));
    	this.setNewaddUserCount(this.newaddUserCount+dayProfit.getNewaddUserCount());
    	this.setPercentage(this.percentage.add(dayProfit.getPercentage()==null?BigDecimal.ZERO:dayProfit.getPercentage()));
    	this.setPercentageUserCount(this.percentageUserCount+dayProfit.getPercentageUserCount());
    	this.setRebate(this.rebate.add(dayProfit.getRebate()==null?BigDecimal.ZERO:dayProfit.getRebate()));
    	this.setRebateUserCount(this.rebateUserCount+dayProfit.getRebateUserCount());
    	this.setRecharge(this.recharge.add(dayProfit.getRecharge()==null?BigDecimal.ZERO:dayProfit.getRecharge()));
    	this.setRechargeAlipay(this.rechargeAlipay.add(dayProfit.getRechargeAlipay()==null?BigDecimal.ZERO:dayProfit.getRechargeAlipay()));
    	this.setRechargeAlipayUserCount(this.rechargeAlipayUserCount+dayProfit.getRechargeAlipayUserCount());
    	this.setRechargeBankTransfer(this.rechargeBankTransfer.add(dayProfit.getRechargeBankTransfer()==null?BigDecimal.ZERO:dayProfit.getRechargeBankTransfer()));
    	this.setRechargeBankTransferUserCount(this.rechargeBankTransferUserCount+dayProfit.getRechargeBankTransferUserCount());
    	this.setRechargeCount(this.rechargeCount+dayProfit.getRechargeCount());
    	this.setRechargePresent(this.rechargePresent.add(dayProfit.getRechargePresent()==null?BigDecimal.ZERO:dayProfit.getRechargePresent()));
    	this.setRechargePresentUserCount(this.rechargePresentUserCount+dayProfit.getRechargePresentUserCount());
    	this.setRechargeQuickPay(this.rechargeQuickPay.add(dayProfit.getRechargeQuickPay()==null?BigDecimal.ZERO:dayProfit.getRechargeQuickPay()));
    	this.setRechargeQuickPayUserCount(this.rechargeQuickPayUserCount+dayProfit.getRechargeQuickPayUserCount());
    	this.setRechargeUserCount(this.rechargeUserCount+dayProfit.getRechargeUserCount());
    	this.setRechargeWeixin(this.rechargeWeixin.add(dayProfit.getRechargeWeixin()==null?BigDecimal.ZERO:dayProfit.getRechargeWeixin()));
    	this.setRechargeWeixinUserCount(this.rechargeWeixinUserCount+dayProfit.getRechargeWeixinUserCount());
    	this.setRechargeQqpay(this.rechargeQqpay.add(dayProfit.getRechargeQqpay()==null?BigDecimal.ZERO:dayProfit.getRechargeQqpay()));
    	this.setRechargeQqpayUserCount(this.rechargeQqpayUserCount+dayProfit.getRechargeQqpayUserCount());
    	this.setRechargeJdpay(this.rechargeJdpay.add(dayProfit.getRechargeJdpay()==null?BigDecimal.ZERO:dayProfit.getRechargeJdpay()));
    	this.setRechargeJdpayUserCount(this.rechargeJdpayUserCount+dayProfit.getRechargeJdpayUserCount());
    	this.setRechargeUnionpay(this.rechargeUnionpay.add(dayProfit.getRechargeUnionpay()==null?BigDecimal.ZERO:dayProfit.getRechargeUnionpay()));
    	this.setRechargeUnionpayUserCount(this.rechargeUnionpayUserCount+dayProfit.getRechargeUnionpayUserCount());
    	this.setRechargeManual(this.rechargeManual == null?BigDecimal.ZERO:dayProfit.getRechargeManual());
    	this.setRechargeManualUserCount(this.rechargeManualUserCount+dayProfit.getRechargeManualUserCount());
    	this.setRegularmembersMoney(this.regularmembersMoney.add(dayProfit.getRegularmembersMoney()==null?BigDecimal.ZERO:dayProfit.getRegularmembersMoney()));	
    	this.setSystemAddMoney(this.systemAddMoney.add(dayProfit.getSystemAddMoney()==null?BigDecimal.ZERO:dayProfit.getSystemAddMoney()));
    	this.setSystemAddMoneyUserCount(this.systemAddMoneyUserCount+dayProfit.getSystemAddMoneyUserCount());
    	this.setSystemReduceMoney(this.systemReduceMoney.add(dayProfit.getSystemReduceMoney()==null?BigDecimal.ZERO:dayProfit.getSystemReduceMoney()));
    	this.setSystemReduceMoneyUserCount(this.systemReduceMoneyUserCount+dayProfit.getSystemReduceMoneyUserCount());
    	this.setManageReduceMoney(this.manageReduceMoney.add(dayProfit.getManageReduceMoney()==null?BigDecimal.ZERO:dayProfit.getManageReduceMoney()));
    	this.setManageReduceMoneyUserCount(this.manageReduceMoneyUserCount+dayProfit.getManageReduceMoneyUserCount());
    	
    	this.setTransfer(this.transfer.add(dayProfit.getTransfer()==null?BigDecimal.ZERO:dayProfit.getTransfer()));
    	this.setWin(this.win.add(dayProfit.getWin()==null?BigDecimal.ZERO:dayProfit.getWin()));
    	this.setWinGain(this.winGain.add(dayProfit.getWinGain()==null?BigDecimal.ZERO:dayProfit.getWinGain()));
    	this.setWinUserCount(this.winUserCount+dayProfit.getWinUserCount());
    	this.setWithdraw(this.withdraw.add(dayProfit.getWithdraw()==null?BigDecimal.ZERO:dayProfit.getWithdraw()));
    	this.setWithdrawCount(this.withdrawCount+dayProfit.getWithdrawCount());
    	this.setWithdrawFee(this.withdrawFee.add(dayProfit.getWithdrawFee()==null?BigDecimal.ZERO:dayProfit.getWithdrawFee()));
    	this.setWithdrawFeeUserCount(this.withdrawFeeUserCount+dayProfit.getWithdrawFeeUserCount());
    	this.setWithdrawUserCount(this.withdrawUserCount+dayProfit.getWithdrawUserCount());
    	this.setFirstRechargeUserCount(this.firstRechargeUserCount+dayProfit.getFirstRechargeUserCount());
    	
    	
    }
    
    /**
     * 重新计算盈率
     * @param dayProfit
     */
    public static void executeDayProfitForWinGain(DayProfit dayProfit){
    	BigDecimal realLottery = dayProfit.getLottery().subtract(dayProfit.getLotteryRegression()).subtract(dayProfit.getLotteryStopAfterNumber());
    	BigDecimal totalWinMoney = dayProfit.getWin();
    	BigDecimal winGain = BigDecimal.ZERO;
    	if (realLottery.compareTo(BigDecimal.ZERO) != 0) {
			winGain = realLottery.subtract(totalWinMoney).divide(realLottery, ConstantUtil.BIGDECIMAL_SCAL_MONEY, BigDecimal.ROUND_HALF_DOWN);
		}
		dayProfit.setWinGain(winGain.setScale(ConstantUtil.BIGDECIMAL_SCAL_MONEY, BigDecimal.ROUND_HALF_DOWN));
    }
    
    
}