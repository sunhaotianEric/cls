package com.team.lottery.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.enums.EPlatformChargePayStatus;
import com.team.lottery.enums.EPlatformChargeType;
/**
 * 业务系统维护分红订单
 * @author zzh
 *
 */
public class PlatformChargeOrder implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 8624800814248359841L;

	private Long id;

    /**
     * 业务系统
     */
    private String bizSystem;

    /**
     * 收费类型  MAINTAIN维护费  BONUS分红费
     */
    private String chargeType;

    /**
     * 金额
     */
    private BigDecimal money;

    /**
     * 支付状态   0=未支付，1=支付
     */
    private String payStatus;

    /**
     * 盈利金额  
     */
    private BigDecimal gain;

    /**
     * 计算使用的分红比例
     */
    private BigDecimal bonusScale;

    /**
     * 
     */
    private String remark;

    /**
     * 归属时间
     */
    private Date belongDate;

    /**
     * 归属时间描述，年月
     */
    private String belongDateStr;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新
     */
    private Date updateTime;

    /**
     * 操作人
     */
    private String updateAdmin;

    //拓展查询字段
    private Date belongDateStart;
    private Date belongDateEnd;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public String getChargeType() {
        return chargeType;
    }

    public void setChargeType(String chargeType) {
        this.chargeType = chargeType == null ? null : chargeType.trim();
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(String payStatus) {
        this.payStatus = payStatus == null ? null : payStatus.trim();
    }

    public BigDecimal getGain() {
        return gain;
    }

    public void setGain(BigDecimal gain) {
        this.gain = gain;
    }

    public BigDecimal getBonusScale() {
        return bonusScale;
    }

    public void setBonusScale(BigDecimal bonusScale) {
        this.bonusScale = bonusScale;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Date getBelongDate() {
        return belongDate;
    }

    public void setBelongDate(Date belongDate) {
        this.belongDate = belongDate;
    }

    public String getBelongDateStr() {
        return belongDateStr;
    }

    public void setBelongDateStr(String belongDateStr) {
        this.belongDateStr = belongDateStr == null ? null : belongDateStr.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateAdmin() {
        return updateAdmin;
    }

    public void setUpdateAdmin(String updateAdmin) {
        this.updateAdmin = updateAdmin == null ? null : updateAdmin.trim();
    }
    
    
    public Date getBelongDateStart() {
		return belongDateStart;
	}

	public void setBelongDateStart(Date belongDateStart) {
		this.belongDateStart = belongDateStart;
	}

	public Date getBelongDateEnd() {
		return belongDateEnd;
	}

	public void setBelongDateEnd(Date belongDateEnd) {
		this.belongDateEnd = belongDateEnd;
	}

	public String getBizSystemName() {
        return ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
    }
   
	public String getPayStatusName() {
        return EPlatformChargePayStatus.valueOf(this.payStatus).getDescription();
    }
	
	public String getChargeTypeStr() {
        return EPlatformChargeType.valueOf(this.chargeType).getDescription();
    }
    
}