package com.team.lottery.vo;

import java.math.BigDecimal;
import java.util.Date;

public class PlatformMonthCharge {
    private Integer id;

    private Date belongDate;

    private String belongDateStr;

    private BigDecimal maintainMoney;

    private BigDecimal bonusMoney;

    private BigDecimal laborMoney;

    private BigDecimal equipmentMoney;
    
    private BigDecimal gain;

    private Date createTime;

    private Date updateTime;

    private String updateAdmin;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getBelongDate() {
        return belongDate;
    }

    public void setBelongDate(Date belongDate) {
        this.belongDate = belongDate;
    }

    public String getBelongDateStr() {
        return belongDateStr;
    }

    public void setBelongDateStr(String belongDateStr) {
        this.belongDateStr = belongDateStr == null ? null : belongDateStr.trim();
    }

    public BigDecimal getMaintainMoney() {
        return maintainMoney;
    }

    public void setMaintainMoney(BigDecimal maintainMoney) {
        this.maintainMoney = maintainMoney;
    }

    public BigDecimal getBonusMoney() {
        return bonusMoney;
    }

    public void setBonusMoney(BigDecimal bonusMoney) {
        this.bonusMoney = bonusMoney;
    }

    public BigDecimal getLaborMoney() {
        return laborMoney;
    }

    public void setLaborMoney(BigDecimal laborMoney) {
        this.laborMoney = laborMoney;
    }

    public BigDecimal getEquipmentMoney() {
        return equipmentMoney;
    }

    public void setEquipmentMoney(BigDecimal equipmentMoney) {
        this.equipmentMoney = equipmentMoney;
    }
    
    public BigDecimal getGain() {
        return gain;
    }

    public void setGain(BigDecimal gain) {
        this.gain = gain;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateAdmin() {
        return updateAdmin;
    }

    public void setUpdateAdmin(String updateAdmin) {
        this.updateAdmin = updateAdmin == null ? null : updateAdmin.trim();
    }
}