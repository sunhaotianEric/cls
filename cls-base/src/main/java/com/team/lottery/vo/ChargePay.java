package com.team.lottery.vo;

import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;

public class ChargePay {
    private Long id;

    private String bizSystem;

    private String chargeType;

    private String chargeDes;

    private String memberId;
    
//    private BigDecimal lowestValue;

//    private BigDecimal highestValue;

    private String terminalId;

    private String interfaceVersion;

    private String signType;

    private String sign;

    private String isTest;

    private String address;

    private String payUrl;

    private String notifyUrl;

    private String returnUrl;
    
    private Integer supportHttps;

    private Integer applyStatus;

    private Integer enabled;

    private Date createTime;

    private String createAdmin;

    private Date updateTime;

    private String updateAdmin;
    
    private String privatekey;

    private String publickey;
    
    private String postUrl;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public String getChargeType() {
        return chargeType;
    }

    public String getPrivatekey() {
		return privatekey;
	}

	public void setPrivatekey(String privatekey) {
		this.privatekey = privatekey;
	}

	public String getPublickey() {
		return publickey;
	}

	public void setPublickey(String publickey) {
		this.publickey = publickey;
	}

	public void setChargeType(String chargeType) {
        this.chargeType = chargeType == null ? null : chargeType.trim();
    }

    public String getChargeDes() {
        return chargeDes;
    }

    public void setChargeDes(String chargeDes) {
        this.chargeDes = chargeDes == null ? null : chargeDes.trim();
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId == null ? null : memberId.trim();
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId == null ? null : terminalId.trim();
    }
    
    /*public BigDecimal getLowestValue() {
        return lowestValue;
    }

    public void setLowestValue(BigDecimal lowestValue) {
        this.lowestValue = lowestValue;
    }

    public BigDecimal getHighestValue() {
        return highestValue;
    }

    public void setHighestValue(BigDecimal highestValue) {
        this.highestValue = highestValue;
    }*/


    public String getInterfaceVersion() {
        return interfaceVersion;
    }

    public void setInterfaceVersion(String interfaceVersion) {
        this.interfaceVersion = interfaceVersion == null ? null : interfaceVersion.trim();
    }

    public String getSignType() {
        return signType;
    }

    public void setSignType(String signType) {
        this.signType = signType == null ? null : signType.trim();
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign == null ? null : sign.trim();
    }

    public String getIsTest() {
        return isTest;
    }

    public void setIsTest(String isTest) {
        this.isTest = isTest == null ? null : isTest.trim();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getPayUrl() {
        return payUrl;
    }

    public void setPayUrl(String payUrl) {
        this.payUrl = payUrl == null ? null : payUrl.trim();
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl == null ? null : notifyUrl.trim();
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl == null ? null : returnUrl.trim();
    }
    
    public Integer getApplyStatus() {
        return applyStatus;
    }

    public void setApplyStatus(Integer applyStatus) {
        this.applyStatus = applyStatus;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateAdmin() {
        return createAdmin;
    }

    public void setCreateAdmin(String createAdmin) {
        this.createAdmin = createAdmin == null ? null : createAdmin.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateAdmin() {
        return updateAdmin;
    }

    public void setUpdateAdmin(String updateAdmin) {
        this.updateAdmin = updateAdmin == null ? null : updateAdmin.trim();
    }
    public String getBizSystemName() {
		 return this.bizSystem==null?"":ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
  }
    /**
     * 第三方充值类型
     * @return
     */
    public String getChargeTypeStr(){
    	if(this.getChargeType() != null){
    		/*String chargeType = this.getChargeType();
    		EFundThirdPayType[] eFundThirdPayType = EFundThirdPayType.values();
    		for(EFundThirdPayType thirdPayType : eFundThirdPayType){
    			if(chargeType.equals(thirdPayType.name())){
    				return thirdPayType.getDescription();
    			}
    		}*/
    		return this.getChargeDes();
    	}
		return "";
    }

	public Integer getSupportHttps() {
		return supportHttps;
	}

	public void setSupportHttps(Integer supportHttps) {
		this.supportHttps = supportHttps;
	}

	public String getPostUrl() {
		return postUrl;
	}

	public void setPostUrl(String postUrl) {
		this.postUrl = postUrl == null ? null : postUrl.trim();
	}
	
}