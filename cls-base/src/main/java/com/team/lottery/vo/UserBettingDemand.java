package com.team.lottery.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.util.DateUtils;

public class UserBettingDemand {
    private Integer id;

    private String bizSystem;

    private Integer userId;

    private String userName;

    private BigDecimal requiredBettingMoney;

    private BigDecimal doneBettingMoney;

    private String doneStatus;

    private Date createDate;

    private Date endDate;

    private Date updateDate;
    
    //扩展字段
    private BigDecimal addRequiredBettingMoney;
    private BigDecimal addDoneBettingMoney;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public BigDecimal getRequiredBettingMoney() {
        return requiredBettingMoney;
    }

    public void setRequiredBettingMoney(BigDecimal requiredBettingMoney) {
        this.requiredBettingMoney = requiredBettingMoney;
    }

    public BigDecimal getDoneBettingMoney() {
        return doneBettingMoney;
    }

    public void setDoneBettingMoney(BigDecimal doneBettingMoney) {
        this.doneBettingMoney = doneBettingMoney;
    }

    public String getDoneStatus() {
        return doneStatus;
    }

    public void setDoneStatus(String doneStatus) {
        this.doneStatus = doneStatus == null ? null : doneStatus.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
    
    public BigDecimal getAddRequiredBettingMoney() {
		return addRequiredBettingMoney;
	}

	public void setAddRequiredBettingMoney(BigDecimal addRequiredBettingMoney) {
		this.addRequiredBettingMoney = addRequiredBettingMoney;
	}

	public BigDecimal getAddDoneBettingMoney() {
		return addDoneBettingMoney;
	}

	public void setAddDoneBettingMoney(BigDecimal addDoneBettingMoney) {
		this.addDoneBettingMoney = addDoneBettingMoney;
	}

	/**
   	 * 获取创建时间字符串
   	 * @return
   	 */
   	public String getCreateDateStr(){
   		return DateUtils.getDateToStrByFormat(this.getCreateDate(), "yyyy-MM-dd HH:mm:ss");
   	}
   	
   	/**
   	 * 获取结束时间字符串
   	 * @return
   	 */
   	public String getEndDateStr(){
   		return DateUtils.getDateToStrByFormat(this.getEndDate(), "yyyy-MM-dd HH:mm:ss");
   	}
   	
    public String getBizSystemName() {
    	if(this.bizSystem == null || "".equals(this.bizSystem)) {
    		 return "超级系统";	
    	}
        return ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
    }
}