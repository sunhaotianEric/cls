package com.team.lottery.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class LotteryCache implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = -3088709428364311567L;

	private Long id;

    private Long orderId;
    
    private String bizSystem;

    private Integer userId;

    private String userName;

    private String codes;
    
    private Date createdDate;

    private List<Map<String, String>> codesList; //号码和玩法的映射

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }
    
    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getCodes() {
        return codes;
    }

    public void setCodes(String codes) {
        this.codes = codes == null ? null : codes.trim();
    }

	public List<Map<String, String>> getCodesList() {
		return codesList;
	}

	public void setCodesList(List<Map<String, String>> codesList) {
		this.codesList = codesList;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
}