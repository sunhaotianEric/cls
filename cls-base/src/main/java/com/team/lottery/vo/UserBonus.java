package com.team.lottery.vo;

import java.math.BigDecimal;

public class UserBonus {
    private Long id;

    private BigDecimal measurement;

    private BigDecimal scale;

    private String measureType;

    private String dailiLevel;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getMeasurement() {
        return measurement;
    }

    public void setMeasurement(BigDecimal measurement) {
        this.measurement = measurement;
    }

    public BigDecimal getScale() {
        return scale;
    }

    public void setScale(BigDecimal scale) {
        this.scale = scale;
    }

    public String getMeasureType() {
        return measureType;
    }

    public void setMeasureType(String measureType) {
        this.measureType = measureType == null ? null : measureType.trim();
    }

    public String getDailiLevel() {
        return dailiLevel;
    }

    public void setDailiLevel(String dailiLevel) {
        this.dailiLevel = dailiLevel == null ? null : dailiLevel.trim();
    }
}