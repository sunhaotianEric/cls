package com.team.lottery.vo;

import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.util.DateUtils;

/**
 * 每日在线登录投注的统计
 * @author gs
 *
 */
public class DayOnlineInfo {
    private Long id;

    private String bizSystem;

    private Integer loginTotalCount;

    private Integer pcLoginTotalCount;

    private Integer mobileLoginTotalCount;

    private Integer appLoginTotalCount;
    
    private Integer lotteryUserCount;

    private Integer pcLotteryUserCount;

    private Integer mobileLotteryUserCount;
    
    private Integer appLotteryUserCount;
    
    private Integer maxOnlineCount;

    private Integer pcMaxOnlineCount;

    private Integer mobileMaxOnlineCount;

    private Integer appMaxOnlineCount;
    
    private Date belongDate;

    private Date createDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public Integer getLoginTotalCount() {
        return loginTotalCount;
    }

    public void setLoginTotalCount(Integer loginTotalCount) {
        this.loginTotalCount = loginTotalCount;
    }

    public Integer getPcLoginTotalCount() {
        return pcLoginTotalCount;
    }

    public void setPcLoginTotalCount(Integer pcLoginTotalCount) {
        this.pcLoginTotalCount = pcLoginTotalCount;
    }

    public Integer getMobileLoginTotalCount() {
        return mobileLoginTotalCount;
    }

    public void setMobileLoginTotalCount(Integer mobileLoginTotalCount) {
        this.mobileLoginTotalCount = mobileLoginTotalCount;
    }

    public Integer getLotteryUserCount() {
        return lotteryUserCount;
    }

    public void setLotteryUserCount(Integer lotteryUserCount) {
        this.lotteryUserCount = lotteryUserCount;
    }

    public Integer getPcLotteryUserCount() {
        return pcLotteryUserCount;
    }

    public Integer getAppLoginTotalCount() {
		return appLoginTotalCount;
	}

	public void setAppLoginTotalCount(Integer appLoginTotalCount) {
		this.appLoginTotalCount = appLoginTotalCount;
	}

	public Integer getAppLotteryUserCount() {
		return appLotteryUserCount;
	}

	public void setAppLotteryUserCount(Integer appLotteryUserCount) {
		this.appLotteryUserCount = appLotteryUserCount;
	}

	public Integer getAppMaxOnlineCount() {
		return appMaxOnlineCount;
	}

	public void setAppMaxOnlineCount(Integer appMaxOnlineCount) {
		this.appMaxOnlineCount = appMaxOnlineCount;
	}

	public void setPcLotteryUserCount(Integer pcLotteryUserCount) {
        this.pcLotteryUserCount = pcLotteryUserCount;
    }

    public Integer getMobileLotteryUserCount() {
        return mobileLotteryUserCount;
    }

    public void setMobileLotteryUserCount(Integer mobileLotteryUserCount) {
        this.mobileLotteryUserCount = mobileLotteryUserCount;
    }

    public Integer getMaxOnlineCount() {
        return maxOnlineCount;
    }

    public void setMaxOnlineCount(Integer maxOnlineCount) {
        this.maxOnlineCount = maxOnlineCount;
    }

    public Integer getPcMaxOnlineCount() {
        return pcMaxOnlineCount;
    }

    public void setPcMaxOnlineCount(Integer pcMaxOnlineCount) {
        this.pcMaxOnlineCount = pcMaxOnlineCount;
    }

    public Integer getMobileMaxOnlineCount() {
        return mobileMaxOnlineCount;
    }

    public void setMobileMaxOnlineCount(Integer mobileMaxOnlineCount) {
        this.mobileMaxOnlineCount = mobileMaxOnlineCount;
    }

    public Date getBelongDate() {
        return belongDate;
    }

    public void setBelongDate(Date belongDate) {
        this.belongDate = belongDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
    
    /**
     * 获取归属时间字符串
     * @return
     */
    public String getBelongDateStr(){
    	return DateUtils.getDateToStrByFormat(this.getBelongDate(), "yyyy-MM-dd");
    }
    
    public String getBizSystemName() {
    	
        return this.bizSystem==null?"":ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
    }
}