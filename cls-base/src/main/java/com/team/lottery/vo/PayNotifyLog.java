package com.team.lottery.vo;

import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;

public class PayNotifyLog {
	private Long id;

	private String bizSystem;

	private String orderId;

	private String thirdType;

	private String thirdTypeDesc;

	private String requestIp;

	private String requestUrl;

	private String requestMethod;

	private String contentType;

	private Integer contentLength;

	private Date createdDate;

	private String messageContent;

	private String headerNames;

	private String parameterNames;

	private Date createdDateStart;

	private Date createdDateEnd;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem == null ? null : bizSystem.trim();
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId == null ? null : orderId.trim();
	}

	public String getThirdType() {
		return thirdType;
	}

	public void setThirdType(String thirdType) {
		this.thirdType = thirdType == null ? null : thirdType.trim();
	}

	public String getThirdTypeDesc() {
		return thirdTypeDesc;
	}

	public void setThirdTypeDesc(String thirdTypeDesc) {
		this.thirdTypeDesc = thirdTypeDesc == null ? null : thirdTypeDesc
				.trim();
	}

	public String getRequestIp() {
		return requestIp;
	}

	public void setRequestIp(String requestIp) {
		this.requestIp = requestIp == null ? null : requestIp.trim();
	}

	public String getRequestUrl() {
		return requestUrl;
	}

	public void setRequestUrl(String requestUrl) {
		this.requestUrl = requestUrl == null ? null : requestUrl.trim();
	}

	public String getRequestMethod() {
		return requestMethod;
	}

	public void setRequestMethod(String requestMethod) {
		this.requestMethod = requestMethod == null ? null : requestMethod
				.trim();
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType == null ? null : contentType.trim();
	}

	public Integer getContentLength() {
		return contentLength;
	}

	public void setContentLength(Integer contentLength) {
		this.contentLength = contentLength;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getMessageContent() {
		return messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent == null ? null : messageContent
				.trim();
	}

	public String getHeaderNames() {
		return headerNames;
	}

	public void setHeaderNames(String headerNames) {
		this.headerNames = headerNames == null ? null : headerNames.trim();
	}

	public String getParameterNames() {
		return parameterNames;
	}

	public void setParameterNames(String parameterNames) {
		this.parameterNames = parameterNames == null ? null : parameterNames
				.trim();
	}

	public String getBizSystemName() {
		return this.bizSystem == null ? "" : ClsCacheManager
				.getBizSystemNameByCode(this.bizSystem);
	}

	public Date getCreatedDateStart() {
		return createdDateStart;
	}

	public void setCreatedDateStart(Date createdDateStart) {
		this.createdDateStart = createdDateStart;
	}

	public Date getCreatedDateEnd() {
		return createdDateEnd;
	}

	public void setCreatedDateEnd(Date createdDateEnd) {
		this.createdDateEnd = createdDateEnd;
	}

}