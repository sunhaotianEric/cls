package com.team.lottery.vo;

import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.util.DateUtils;

/**
 * 开奖号码导入表.
 * 
 * @author Jamine
 *
 */
public class LotteryCodeXyImport {

	/**
	 * 自增ID.
	 */
	private Integer id;

	/**
	 * 所属系统.
	 */
	private String bizSystem;

	/**
	 * 开奖号码类型.
	 */
	private String lotteryType;

	/**
	 * 开奖期号.
	 */
	private String issueNo;

	/**
	 * 开奖号码.
	 */
	private String openCode;

	/**
	 * 创建时间.
	 */
	private Date createTime;

	/**
	 * 更新时间.
	 */
	private Date updateTime;

	/**
	 * 创建人.
	 */
	private String createAdmin;

	/**
	 * 更新时间.
	 */
	private String updateAdmin;

	/**
	 * 使用状态,0是未使用,1是使用,2是设置为失效.
	 */
	private Integer usedStatus;
	
	// 扩展字段
	// 彩种名称
	private String lotteryTypeDes;

	// 获取系统对应的中文
	public String getBizSystemName() {
        return this.bizSystem==null?"":ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
    }
	
	//创建时间字符串
    public String getCreateTimeStr(){
    	return DateUtils.getDateToStrByFormat(this.getCreateTime(), "yyyy/MM/dd HH:mm:ss");
    }
    
  //获取彩种类型名称
  	public String getLotteryTypeDes() {
  		if(lotteryTypeDes != null && !"".equals(lotteryTypeDes)) {
  			return lotteryTypeDes;
  		}
  		if(lotteryType != null && !"".equals(lotteryType)) {
  			ELotteryKind kind = ELotteryKind.valueOf(lotteryType);
  			if(kind != null) {
  				return kind.getDescription();
  			}
  		}
  		return lotteryTypeDes;
  	}
    
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOpenCode() {
		return openCode;
	}

	public void setOpenCode(String openCode) {
		this.openCode = openCode;
	}

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem == null ? null : bizSystem.trim();
	}

	public String getLotteryType() {
		return lotteryType;
	}

	public void setLotteryType(String lotteryType) {
		this.lotteryType = lotteryType == null ? null : lotteryType.trim();
	}

	public String getIssueNo() {
		return issueNo;
	}

	public void setIssueNo(String issueNo) {
		this.issueNo = issueNo == null ? null : issueNo.trim();
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getCreateAdmin() {
		return createAdmin;
	}

	public void setCreateAdmin(String createAdmin) {
		this.createAdmin = createAdmin == null ? null : createAdmin.trim();
	}

	public Integer getUsedStatus() {
		return usedStatus;
	}

	public void setUsedStatus(Integer usedStatus) {
		this.usedStatus = usedStatus;
	}

	public String getUpdateAdmin() {
		return updateAdmin;
	}

	public void setUpdateAdmin(String updateAdmin) {
		this.updateAdmin = updateAdmin;
	}

}