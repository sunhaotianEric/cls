package com.team.lottery.vo;

import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.util.DateUtils;

public class Help {
    private Long id;
    
    private String bizSystem;

    private String title;

    private Integer yetShow;

    private Date createDate;

    private Integer hit;

    private String type;

    private String typeDes;
    
    private String essentialsDes;

    private String content;
    
    private Integer frequentlyQuestion;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public Integer getYetShow() {
        return yetShow;
    }

    public void setYetShow(Integer yetShow) {
        this.yetShow = yetShow;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Integer getHit() {
        return hit;
    }

    public void setHit(Integer hit) {
        this.hit = hit;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }
    
    public String getTypeDes() {
		return typeDes;
	}

	public void setTypeDes(String typeDes) {
		this.typeDes = typeDes;
	}

	public String getEssentialsDes() {
		return essentialsDes;
	}

	public void setEssentialsDes(String essentialsDes) {
		this.essentialsDes = essentialsDes;
	}
	
	public Integer getFrequentlyQuestion() {
		return frequentlyQuestion;
	}

	public void setFrequentlyQuestion(Integer frequentlyQuestion) {
		this.frequentlyQuestion = frequentlyQuestion;
	}

	public String getLogtimeStr(){
    	return DateUtils.getDateToStrByFormat(this.getCreateDate(), "yyyy/MM/dd HH:mm:ss");
    }
	
	public String getBizSystemName() {
	        return this.bizSystem==null?"":ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
	}
}