package com.team.lottery.vo;

import java.io.Serializable;
import java.util.Date;
/**
 * 网站系统参数配置
 * @author zhuang
 *
 */
public class SystemConfig implements Serializable {
   
	/**
	 * 
	 */
	private static final long serialVersionUID = 4150906263820475937L;
	private Long id;
    /**
     * 配置key,唯一
     */
    private String configKey;
    /**
     * 配置值
     */
    private String configValue;
    /**
     * 配置描述
     */
    private String configDes;
    
    /**
     * 是否启用 ，启用状态 1启用  0不启用
     */
    private Integer enable;

    /**
     * 创建时间
     */
    private Date createDate;
     /**
      * 操作人
      */
    private String updateAdmin;
    /**
     * 最后更新时间
     */
    private Date updateDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getConfigKey() {
        return configKey;
    }

    public void setConfigKey(String configKey) {
        this.configKey = configKey == null ? null : configKey.trim();
    }

    public String getConfigValue() {
        return configValue;
    }

    public void setConfigValue(String configValue) {
        this.configValue = configValue == null ? null : configValue.trim();
    }

    public String getConfigDes() {
        return (configDes==null?"":configDes);
    }

    public void setConfigDes(String configDes) {
        this.configDes = configDes == null ? null : configDes.trim();
    }

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    public Date getCreateDate() {
    	
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUpdateAdmin() {
        return updateAdmin;
    }

    public void setUpdateAdmin(String updateAdmin) {
        this.updateAdmin = updateAdmin == null ? null : updateAdmin.trim();
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}