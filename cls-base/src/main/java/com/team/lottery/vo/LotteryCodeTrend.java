package com.team.lottery.vo;

import java.util.Date;

public class LotteryCodeTrend {
    private Long id;

    private String lotteryName;

    private String lotteryNum;

    private String openCode;

    private String playkindContent;

    private String numPosition;

    private Date kjtime;

    private Date addtime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLotteryName() {
        return lotteryName;
    }

    public void setLotteryName(String lotteryName) {
        this.lotteryName = lotteryName == null ? null : lotteryName.trim();
    }

    public String getLotteryNum() {
        return lotteryNum;
    }

    public void setLotteryNum(String lotteryNum) {
        this.lotteryNum = lotteryNum == null ? null : lotteryNum.trim();
    }

    public String getOpenCode() {
        return openCode;
    }

    public void setOpenCode(String openCode) {
        this.openCode = openCode == null ? null : openCode.trim();
    }

    public String getPlaykindContent() {
        return playkindContent;
    }

    public void setPlaykindContent(String playkindContent) {
        this.playkindContent = playkindContent == null ? null : playkindContent.trim();
    }

    public String getNumPosition() {
        return numPosition;
    }

    public void setNumPosition(String numPosition) {
        this.numPosition = numPosition == null ? null : numPosition.trim();
    }

    public Date getKjtime() {
        return kjtime;
    }

    public void setKjtime(Date kjtime) {
        this.kjtime = kjtime;
    }

    public Date getAddtime() {
        return addtime;
    }

    public void setAddtime(Date addtime) {
        this.addtime = addtime;
    }
}