package com.team.lottery.vo;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.enums.EBonusOrderStatus;
import com.team.lottery.util.DateUtils;

public class BonusOrder {
    private Long id;

    private String serialNumber;

    private String bizSystem;

    private Integer userId;

    private String userName;

    private Integer fromUserId;

    private String fromUserName;

    private BigDecimal money;

    private BigDecimal bonusScale;

    private BigDecimal gain;

    private BigDecimal lotteryMoney;

    private Date belongDate;

    private String releaseStatus;

    private String remark;

    private Date shouldReleaseDate;

    private Date releaseDate;

    private Date createdDate;

    private Date updateDate;

    /**
     * 下面为拓展查询字段
     * @return
     */
    /**
     * 是否查询逾期未补发的
     */
    private Boolean isQueryOuttimeNotRelease;
    
    private Integer scope;
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber == null ? null : serialNumber.trim();
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public Integer getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(Integer fromUserId) {
        this.fromUserId = fromUserId;
    }

    public String getFromUserName() {
        return fromUserName;
    }

    public void setFromUserName(String fromUserName) {
        this.fromUserName = fromUserName == null ? null : fromUserName.trim();
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public BigDecimal getBonusScale() {
        return bonusScale;
    }

    public void setBonusScale(BigDecimal bonusScale) {
        this.bonusScale = bonusScale;
    }

    public BigDecimal getGain() {
        return gain;
    }

    public void setGain(BigDecimal gain) {
        this.gain = gain;
    }

    public BigDecimal getLotteryMoney() {
        return lotteryMoney;
    }

    public void setLotteryMoney(BigDecimal lotteryMoney) {
        this.lotteryMoney = lotteryMoney;
    }

    public Date getBelongDate() {
        return belongDate;
    }

    public void setBelongDate(Date belongDate) {
        this.belongDate = belongDate;
    }

    public String getReleaseStatus() {
        return releaseStatus;
    }

    public void setReleaseStatus(String releaseStatus) {
        this.releaseStatus = releaseStatus == null ? null : releaseStatus.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Date getShouldReleaseDate() {
        return shouldReleaseDate;
    }

    public void setShouldReleaseDate(Date shouldReleaseDate) {
        this.shouldReleaseDate = shouldReleaseDate;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

	public Boolean getIsQueryOuttimeNotRelease() {
		return isQueryOuttimeNotRelease;
	}

	public void setIsQueryOuttimeNotRelease(Boolean isQueryOuttimeNotRelease) {
		this.isQueryOuttimeNotRelease = isQueryOuttimeNotRelease;
	}
	
     public Integer getScope() {
		return scope;
	}

	public void setScope(Integer scope) {
		this.scope = scope;
	}

	/**
     * 
     * 状态值中文
     */
	 public String getReleaseStatusName() {
		 
		    if(releaseStatus!=null){
		      return EBonusOrderStatus.valueOf(releaseStatus).getDescription();
		    }else{
		      return "";	
		    }
	        
	 }
	 
	 /**
	     * 归属时间
	     */
	  public String getBelongDateStr() {
	
		  SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		  if(belongDate!=null){
		     return sdf.format(belongDate);
		  }else{
			  return "";  
		  }
	   }
    
	   /**
	     * 归属开始时间
	     */
	  public String getStartDateStr() {
	
		  SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM");
		  if(belongDate!=null){
		     return sdf.format(belongDate);
		  }else{
			  return "";  
		  }
	   }
	  
	   /**
	     * 
	     * 归属结束时间
	     */
	  public String getEndDateStr() {
		  SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM");
		  if(belongDate!=null){
			 return  new String(sdf.format(belongDate));
		  }else{
			  return "";  
		  }   
	   }
	  
	  /**
		 * 发放时间
		 * 
		 */
		public String getReleaseDateStr() {
			return DateUtils.getDateToStrByFormat(this.releaseDate, "yyyy-MM-dd HH:mm:ss");
		}
	  
	   public String getBizSystemName() {
	    	
	        return this.bizSystem==null?"":ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
	    }
}