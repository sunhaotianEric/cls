package com.team.lottery.vo;

import java.math.BigDecimal;

public class AwardModelConfig {
    private Long id;

    private String bizSystem;

    private BigDecimal sscLowestAwardModel;

    private BigDecimal sscHighestAwardModel;

    private BigDecimal ffcLowestAwardModel;

    private BigDecimal ffcHighestAwardModel;

    private BigDecimal syxwLowestAwardModel;

    private BigDecimal syxwHighestAwardModel;

    private BigDecimal ksLowestAwardModel;

    private BigDecimal ksHighestAwardModel;

    private BigDecimal tbLowestAwardModel;

    private BigDecimal tbHighestAwardModel;

    private BigDecimal dpcLowestAwardModel;

    private BigDecimal dpcHighestAwardModel;

    private BigDecimal pk10LowestAwardModel;

    private BigDecimal pk10HighestAwardModel;

    private BigDecimal klsfLowestAwardModel;

    private BigDecimal klsfHighestAwardModel;

    private BigDecimal lhcLowestAwardModel;

    private BigDecimal lhcHighestAwardModel;
    
    private BigDecimal xyebLowestAwardModel;
    
    private BigDecimal xyebHighestAwardModel;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public BigDecimal getSscLowestAwardModel() {
        return sscLowestAwardModel;
    }

    public void setSscLowestAwardModel(BigDecimal sscLowestAwardModel) {
        this.sscLowestAwardModel = sscLowestAwardModel;
    }

    public BigDecimal getSscHighestAwardModel() {
        return sscHighestAwardModel;
    }

    public void setSscHighestAwardModel(BigDecimal sscHighestAwardModel) {
        this.sscHighestAwardModel = sscHighestAwardModel;
    }

    public BigDecimal getFfcLowestAwardModel() {
        return ffcLowestAwardModel;
    }

    public void setFfcLowestAwardModel(BigDecimal ffcLowestAwardModel) {
        this.ffcLowestAwardModel = ffcLowestAwardModel;
    }

    public BigDecimal getFfcHighestAwardModel() {
        return ffcHighestAwardModel;
    }

    public void setFfcHighestAwardModel(BigDecimal ffcHighestAwardModel) {
        this.ffcHighestAwardModel = ffcHighestAwardModel;
    }

    public BigDecimal getSyxwLowestAwardModel() {
        return syxwLowestAwardModel;
    }

    public void setSyxwLowestAwardModel(BigDecimal syxwLowestAwardModel) {
        this.syxwLowestAwardModel = syxwLowestAwardModel;
    }

    public BigDecimal getSyxwHighestAwardModel() {
        return syxwHighestAwardModel;
    }

    public void setSyxwHighestAwardModel(BigDecimal syxwHighestAwardModel) {
        this.syxwHighestAwardModel = syxwHighestAwardModel;
    }

    public BigDecimal getKsLowestAwardModel() {
        return ksLowestAwardModel;
    }

    public void setKsLowestAwardModel(BigDecimal ksLowestAwardModel) {
        this.ksLowestAwardModel = ksLowestAwardModel;
    }

    public BigDecimal getKsHighestAwardModel() {
        return ksHighestAwardModel;
    }

    public void setKsHighestAwardModel(BigDecimal ksHighestAwardModel) {
        this.ksHighestAwardModel = ksHighestAwardModel;
    }

    public BigDecimal getTbLowestAwardModel() {
        return tbLowestAwardModel;
    }

    public void setTbLowestAwardModel(BigDecimal tbLowestAwardModel) {
        this.tbLowestAwardModel = tbLowestAwardModel;
    }

    public BigDecimal getTbHighestAwardModel() {
        return tbHighestAwardModel;
    }

    public void setTbHighestAwardModel(BigDecimal tbHighestAwardModel) {
        this.tbHighestAwardModel = tbHighestAwardModel;
    }

    public BigDecimal getDpcLowestAwardModel() {
        return dpcLowestAwardModel;
    }

    public void setDpcLowestAwardModel(BigDecimal dpcLowestAwardModel) {
        this.dpcLowestAwardModel = dpcLowestAwardModel;
    }

    public BigDecimal getDpcHighestAwardModel() {
        return dpcHighestAwardModel;
    }

    public void setDpcHighestAwardModel(BigDecimal dpcHighestAwardModel) {
        this.dpcHighestAwardModel = dpcHighestAwardModel;
    }

    public BigDecimal getPk10LowestAwardModel() {
        return pk10LowestAwardModel;
    }

    public void setPk10LowestAwardModel(BigDecimal pk10LowestAwardModel) {
        this.pk10LowestAwardModel = pk10LowestAwardModel;
    }

    public BigDecimal getPk10HighestAwardModel() {
        return pk10HighestAwardModel;
    }

    public void setPk10HighestAwardModel(BigDecimal pk10HighestAwardModel) {
        this.pk10HighestAwardModel = pk10HighestAwardModel;
    }

    public BigDecimal getKlsfLowestAwardModel() {
        return klsfLowestAwardModel;
    }

    public void setKlsfLowestAwardModel(BigDecimal klsfLowestAwardModel) {
        this.klsfLowestAwardModel = klsfLowestAwardModel;
    }

    public BigDecimal getKlsfHighestAwardModel() {
        return klsfHighestAwardModel;
    }

    public void setKlsfHighestAwardModel(BigDecimal klsfHighestAwardModel) {
        this.klsfHighestAwardModel = klsfHighestAwardModel;
    }

    public BigDecimal getLhcLowestAwardModel() {
        return lhcLowestAwardModel;
    }

    public void setLhcLowestAwardModel(BigDecimal lhcLowestAwardModel) {
        this.lhcLowestAwardModel = lhcLowestAwardModel;
    }

    public BigDecimal getLhcHighestAwardModel() {
        return lhcHighestAwardModel;
    }

    public void setLhcHighestAwardModel(BigDecimal lhcHighestAwardModel) {
        this.lhcHighestAwardModel = lhcHighestAwardModel;
    }

	public BigDecimal getXyebLowestAwardModel() {
		return xyebLowestAwardModel;
	}

	public void setXyebLowestAwardModel(BigDecimal xyebLowestAwardModel) {
		this.xyebLowestAwardModel = xyebLowestAwardModel;
	}

	public BigDecimal getXyebHighestAwardModel() {
		return xyebHighestAwardModel;
	}

	public void setXyebHighestAwardModel(BigDecimal xyebHighestAwardModel) {
		this.xyebHighestAwardModel = xyebHighestAwardModel;
	}
    
}