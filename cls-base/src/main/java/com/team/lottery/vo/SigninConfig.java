package com.team.lottery.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.util.DateUtils;

public class SigninConfig {
    private Integer id;

    private String bizSystem;

    private Integer signinDay;

    private BigDecimal money;

    private Date createTime;

    private Date updateTime;

    private String updateAdmin;
    //扩展字段
    private String signState;
    private Date signinDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public Integer getSigninDay() {
        return signinDay;
    }

    public void setSigninDay(Integer signinDay) {
        this.signinDay = signinDay;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateAdmin() {
        return updateAdmin;
    }

    public String getSignState() {
		return signState;
	}

	public void setSignState(String signState) {
		this.signState = signState;
	}

	public void setUpdateAdmin(String updateAdmin) {
        this.updateAdmin = updateAdmin == null ? null : updateAdmin.trim();
    }
	public Date getSigninDate() {
		return signinDate;
	}

	public void setSigninDate(Date signinDate) {
		this.signinDate = signinDate;
	}

	//获取时间字符串
  	public String getCreateTimeStr(){
  		if(this.getCreateTime() != null){
  	    	return DateUtils.getDateToStrByFormat(this.getCreateTime(), "yyyy/MM/dd HH:mm:ss");
  		}
  		return "";
  	}
  	public String getUpdateTimeStr(){
  		if(this.getUpdateTime() != null){
  	    	return DateUtils.getDateToStrByFormat(this.getUpdateTime(), "yyyy/MM/dd HH:mm:ss");
  		}
  		return "";
  	}
  	/**
	 * 业务系统
	 * 
	 */
	public String getBizSystemName() {
		return ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
	}
}