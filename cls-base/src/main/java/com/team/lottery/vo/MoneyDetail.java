package com.team.lottery.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.enums.ELotteryModel;
import com.team.lottery.enums.EMoneyDetailType;
import com.team.lottery.util.DateUtils;
import com.team.lottery.util.StringUtils;

public class MoneyDetail {
	
	//标记是否计算进分红中 0是   1不是
	public static Integer YETBONUS = 0;
	public static Integer NOT_YETBONUS = 1;
	
    private Long id;

    private String bizSystem;
    private Long orderId;

    private String lotteryId;
    
    private Integer userId;

    private String userName;
    
    private String operateUserName;

    private String expect;

    private String lotteryType;

    private String lotteryTypeDes;

    private String lotteryProtype;

    private String lotteryProtypeDes;

    private String detailType;

    private BigDecimal income;

    private BigDecimal pay;

    private BigDecimal balanceBefore;

    private BigDecimal balanceAfter;

    private Integer winLevel;

    private BigDecimal winCost;

    private BigDecimal winCostRule;
    
    private String lotteryModel;
    
    private String detailDes;

    private String description;

    private Integer yetBonus;

    private Date createdDate;
    /**
     * 是否有效记录
     */
    private Integer enabled;
    
    //扩展属性
    private BigDecimal money; //用户余额
    private String regfrom;  //用户推荐人的关系
    private String dailiLevel; //代理类型 
    private Integer userCount; //用户人数
    
    public MoneyDetail(){
    }

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    
    public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getLotteryId() {
        return lotteryId;
    }

    public void setLotteryId(String lotteryId) {
        this.lotteryId = lotteryId == null ? null : lotteryId.trim();
    }

    public String getExpect() {
        return expect;
    }

    public void setExpect(String expect) {
        this.expect = expect == null ? null : expect.trim();
    }

    public String getLotteryType() {
        return lotteryType;
    }

    public void setLotteryType(String lotteryType) {
        this.lotteryType = lotteryType == null ? null : lotteryType.trim();
    }

    public String getLotteryProtype() {
        return lotteryProtype;
    }

    public void setLotteryProtype(String lotteryProtype) {
        this.lotteryProtype = lotteryProtype == null ? null : lotteryProtype.trim();
    }

    public String getLotteryProtypeDes() {
        return lotteryProtypeDes;
    }

    public void setLotteryProtypeDes(String lotteryProtypeDes) {
        this.lotteryProtypeDes = lotteryProtypeDes == null ? null : lotteryProtypeDes.trim();
    }

    public String getDetailType() {
        return detailType;
    }

    public void setDetailType(String detailType) {
        this.detailType = detailType == null ? null : detailType.trim();
    }

    public BigDecimal getIncome() {
        return income;
    }

    public void setIncome(BigDecimal income) {
        this.income = income;
    }

    public BigDecimal getPay() {
        return pay;
    }

    public void setPay(BigDecimal pay) {
        this.pay = pay;
    }

    public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

	public BigDecimal getBalanceBefore() {
        return balanceBefore;
    }

    public void setBalanceBefore(BigDecimal balanceBefore) {
        this.balanceBefore = balanceBefore;
    }

    public BigDecimal getBalanceAfter() {
        return balanceAfter;
    }

    public void setBalanceAfter(BigDecimal balanceAfter) {
        this.balanceAfter = balanceAfter;
    }

    public Integer getWinLevel() {
        return winLevel;
    }

    public void setWinLevel(Integer winLevel) {
        this.winLevel = winLevel;
    }

    public BigDecimal getWinCost() {
        return winCost;
    }

    public void setWinCost(BigDecimal winCost) {
        this.winCost = winCost;
    }

    public BigDecimal getWinCostRule() {
        return winCostRule;
    }

    public void setWinCostRule(BigDecimal winCostRule) {
        this.winCostRule = winCostRule;
    }
    
    public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLotteryTypeDes() {
		return lotteryTypeDes;
	}

	public void setLotteryTypeDes(String lotteryTypeDes) {
		this.lotteryTypeDes = lotteryTypeDes;
	}

	public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
    public String getOperateUserName() {
		return operateUserName;
	}

	public void setOperateUserName(String operateUserName) {
		this.operateUserName = operateUserName;
	}

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public String getRegfrom() {
		return regfrom;
	}

	public void setRegfrom(String regfrom) {
		this.regfrom = regfrom;
	}
	
	public Integer getYetBonus() {
		return yetBonus;
	}

	public void setYetBonus(Integer yetBonus) {
		this.yetBonus = yetBonus;
	}

	public String getDailiLevel() {
		return dailiLevel;
	}

	public void setDailiLevel(String dailiLevel) {
		this.dailiLevel = dailiLevel;
	}
	
	public String getDetailDes() {
		return detailDes;
	}

	public void setDetailDes(String detailDes) {
		this.detailDes = detailDes;
	}

	public String getLotteryModel() {
		return lotteryModel;
	}

	public void setLotteryModel(String lotteryModel) {
		this.lotteryModel = lotteryModel;
	}

	public Integer getUserCount() {
		return userCount;
	}

	public void setUserCount(Integer userCount) {
		this.userCount = userCount;
	}

	/**
	 * 资金明细拷贝
	 * @param moneyDetail
	 */
	public MoneyDetail copy(String toUserName){
		MoneyDetail moneyDetail = new MoneyDetail();
		if(!StringUtils.isEmpty(toUserName)){
			moneyDetail.setBizSystem(this.getBizSystem());
			moneyDetail.setUserName(toUserName);
			moneyDetail.setIncome(this.getIncome());
			moneyDetail.setPay(this.getPay());
			moneyDetail.setDetailType(this.getDetailType());
			moneyDetail.setDescription(this.getDescription());
		}
		return moneyDetail;
	}
	
	public String getCreateDateStr(){
    	return DateUtils.getDateToStrByFormat(this.getCreatedDate(), "yyyy/MM/dd HH:mm:ss");
    }
    
    //返回操作模式
    public String getDetailTypeDes(){
      if(this.getDetailType() != null){    	
    	  return EMoneyDetailType.valueOf(this.getDetailType()).getDescription();
      }
      return "";
    }
    
    /**
	 * 投注模式描述
	 * @return
	 */
	public String getLotteryModelStr(){
		if(!StringUtils.isEmpty(this.getLotteryModel())){
			return ELotteryModel.valueOf(this.getLotteryModel()).getDescription();
		}
		return "";
	}

	/**
	 * 获取业务系统中文名称
	 * @return
	 */
	 public String getBizSystemName() {
	    	
	        return this.bizSystem==null?"":ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
	    }
}