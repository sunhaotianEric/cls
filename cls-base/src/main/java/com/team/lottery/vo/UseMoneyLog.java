package com.team.lottery.vo;

import java.math.BigDecimal;
import java.util.Date;

public class UseMoneyLog {
    private Long id;

    private String lotteryId;

    private BigDecimal useMoney;
    
    private Integer enabled;
    
    private Date createdDate;

    public UseMoneyLog(){
    	
    }
    
    public UseMoneyLog(String lotteryId){
      this.lotteryId = 	lotteryId;
    }   
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLotteryId() {
        return lotteryId;
    }

    public void setLotteryId(String lotteryId) {
        this.lotteryId = lotteryId == null ? null : lotteryId.trim();
    }

    public BigDecimal getUseMoney() {
        return useMoney;
    }

    public void setUseMoney(BigDecimal useMoney) {
        this.useMoney = useMoney;
    }

	public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}
	
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((lotteryId == null) ? 0 : lotteryId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UseMoneyLog other = (UseMoneyLog) obj;
		if (lotteryId == null) {
			if (other.lotteryId != null)
				return false;
		} else if (!lotteryId.equals(other.lotteryId))
			return false;
		return true;
	}
	
}