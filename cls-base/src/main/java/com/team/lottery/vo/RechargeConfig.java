package com.team.lottery.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.enums.EFundPayType;
import com.team.lottery.enums.EFundRefType;

public class RechargeConfig {
	
    private Long id;

    private String bizSystem;
    
    private String showType;

    private String payType;

    private String refType;

    private Long refId;

    private String showName;
    
    private Integer showUserVipLevel;
    
    private String showUserVipLevels;
    
    private Integer showUserStarLevel;

    private BigDecimal showUserSscRebate;
    
    private String showUserNameLine;

    private Integer orderNum;

    private Integer enabled;

    private Date createTime;

    private String createAdmin;

    private Date updateTime;

    private String updateAdmin;
  
    private String chargeType;
    
    private String memberId;
    
    private String bankName;
    
    private String bankUserName;
    
    private BigDecimal highestValue;
    
    private BigDecimal lowestValue;
    
    private String description;
    
    private Integer rechargeGiftEnabled;

    private BigDecimal rechargeGiftScale;

    private BigDecimal rechargeGiftLowestValue;
    
    //扩展字段
    private String payId;

    private String bankType;
    
    private Long pId;
    
    private BigDecimal bhighestValue;
    
    private BigDecimal blowestValue;
    
    private Long bId;
    
    private String subbranchName;
    
    private String refIdList;
    
    private String chargeDes;
    
    private Integer mobileSkipType;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }
    
    public String getShowType() {
        return showType;
    }

    public void setShowType(String showType) {
        this.showType = showType == null ? null : showType.trim();
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType == null ? null : payType.trim();
    }

    public String getRefType() {
        return refType;
    }

    public void setRefType(String refType) {
        this.refType = refType == null ? null : refType.trim();
    }

    public Long getRefId() {
        return refId;
    }

    public void setRefId(Long refId) {
        this.refId = refId;
    }

    public String getShowName() {
        return showName;
    }

    public void setShowName(String showName) {
        this.showName = showName == null ? null : showName.trim();
    }

    public Integer getShowUserVipLevel() {
		return showUserVipLevel;
	}

	public void setShowUserVipLevel(Integer showUserVipLevel) {
		this.showUserVipLevel = showUserVipLevel;
	}
	
	public String getShowUserVipLevels() {
        return showUserVipLevels;
    }

    public void setShowUserVipLevels(String showUserVipLevels) {
        this.showUserVipLevels = showUserVipLevels == null ? null : showUserVipLevels.trim();
    }

	public Integer getShowUserStarLevel() {
        return showUserStarLevel;
    }

    public void setShowUserStarLevel(Integer showUserStarLevel) {
        this.showUserStarLevel = showUserStarLevel;
    }

    public BigDecimal getShowUserSscRebate() {
        return showUserSscRebate;
    }

    public void setShowUserSscRebate(BigDecimal showUserSscRebate) {
        this.showUserSscRebate = showUserSscRebate;
    }
    
    public String getShowUserNameLine() {
        return showUserNameLine;
    }

    public void setShowUserNameLine(String showUserNameLine) {
        this.showUserNameLine = showUserNameLine == null ? null : showUserNameLine.trim();
    }

    public Integer getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateAdmin() {
        return createAdmin;
    }

    public void setCreateAdmin(String createAdmin) {
        this.createAdmin = createAdmin == null ? null : createAdmin.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateAdmin() {
        return updateAdmin;
    }

    public void setUpdateAdmin(String updateAdmin) {
        this.updateAdmin = updateAdmin == null ? null : updateAdmin.trim();
    }
    
    public String getBizSystemName() {
		 return this.bizSystem==null?"":ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
    }
    /**
     * 第三方充值类型
     * @return
     */
    public String getPayTypeStr(){
    	if(this.getPayType() != null){
    		String payType = this.getPayType();
    		EFundPayType[] eFundPayType = EFundPayType.values();
    		for(EFundPayType type : eFundPayType){
    			if(payType.equals(type.name())){
    				return type.getDescription();
    			}
    		}
    		
    	}
		return "";
    }
    /**
     * 第三方充值类型
     * @return
     */
    public String getRefTypeStr(){
    	if(this.getRefType() != null){
    		String refType = this.getRefType();
    		EFundRefType[] eFundRefType = EFundRefType.values();
    		for(EFundRefType type : eFundRefType){
    			if(refType.equals(type.name())){
    				return type.getDescription();
    			}
    		}
    		
    	}
		return "";
    }

	public String getChargeType() {
		return chargeType;
	}

	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankUserName() {
		return bankUserName;
	}

	public void setBankUserName(String bankUserName) {
		this.bankUserName = bankUserName;
	}
	
	
	
	 public BigDecimal getHighestValue() {
		return highestValue;
	}

	public void setHighestValue(BigDecimal highestValue) {
		this.highestValue = highestValue;
	}

	public BigDecimal getLowestValue() {
		return lowestValue;
	}

	public void setLowestValue(BigDecimal lowestValue) {
		this.lowestValue = lowestValue;
	}
	
	public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}
	
	

	public String getBankType() {
		return bankType;
	}

	public void setBankType(String bankType) {
		this.bankType = bankType;
	}

    
	
	public Long getpId() {
		return pId;
	}

	public void setpId(Long pId) {
		this.pId = pId;
	}

	
	
	public BigDecimal getBhighestValue() {
		return bhighestValue;
	}

	public void setBhighestValue(BigDecimal bhighestValue) {
		this.bhighestValue = bhighestValue;
	}

	public BigDecimal getBlowestValue() {
		return blowestValue;
	}

	public void setBlowestValue(BigDecimal blowestValue) {
		this.blowestValue = blowestValue;
	}
	
	
	public Long getbId() {
		return bId;
	}

	public void setbId(Long bId) {
		this.bId = bId;
	}

	
	public String getSubbranchName() {
		return subbranchName;
	}

	public void setSubbranchName(String subbranchName) {
		this.subbranchName = subbranchName;
	}

	
	public String getRefIdList() {
		return refIdList;
	}

	public void setRefIdList(String refIdList) {
		this.refIdList = refIdList;
	}
	

	public Integer getRechargeGiftEnabled() {
		return rechargeGiftEnabled;
	}

	public void setRechargeGiftEnabled(Integer rechargeGiftEnabled) {
		this.rechargeGiftEnabled = rechargeGiftEnabled;
	}

	public BigDecimal getRechargeGiftScale() {
		return rechargeGiftScale;
	}

	public void setRechargeGiftScale(BigDecimal rechargeGiftScale) {
		this.rechargeGiftScale = rechargeGiftScale;
	}

	public BigDecimal getRechargeGiftLowestValue() {
		return rechargeGiftLowestValue;
	}

	public void setRechargeGiftLowestValue(BigDecimal rechargeGiftLowestValue) {
		this.rechargeGiftLowestValue = rechargeGiftLowestValue;
	}

	public String getChargeDes() {
		return chargeDes;
	}

	public void setChargeDes(String chargeDes) {
		this.chargeDes = chargeDes;
	}

	public Integer getMobileSkipType() {
		return mobileSkipType;
	}

	public void setMobileSkipType(Integer mobileSkipType) {
		this.mobileSkipType = mobileSkipType;
	}

	/**
     * 第三方充值类型
     * @return
     */
    public String getChargeTypeStr(){
    	if(this.getChargeType() != null){
    		/*String chargeType = this.getChargeType();
    		EFundThirdPayType[] eFundThirdPayType = EFundThirdPayType.values();
    		for(EFundThirdPayType type : eFundThirdPayType){
    			if(chargeType.equals(type.name())){
    				return type.getDescription();
    			}
    		}*/
    		return this.getChargeDes();
    		
    	}
		return "";
    }
}