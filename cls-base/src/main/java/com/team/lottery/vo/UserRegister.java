package com.team.lottery.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.util.DateUtils;

public class UserRegister {
	private Integer id;

	/**
	 * 业务系统.
	 */
	private String bizSystem;

	/**
	 * 用户id.
	 */
	private Integer userId;

	/**
	 * 用户名称.
	 */
	private String userName;

	/**
	 * 注册链接设置信息.
	 */
	private String linkSets;

	/**
	 * 备注.
	 */
	private String linkDes;

	/**
	 * 设定注册人数 -1表示无限制.
	 */
	private Integer setCount;

	/**
	 * 使用次数.
	 */
	private Integer useCount;

	/**
	 * 失效时间,为null的情况,代表永久有效.
	 */
	private Date loseDate;

	/**
	 * 创建时间.
	 */
	private Date createdDate;
	/**
	 * 更新时间.
	 */
	private Date updatedDate;

	/**
	 * 是否停用.
	 */
	private Integer enabled;
	/**
	 * 邀请码.
	 */
	private String invitationCode;

	/**
	 * 时时彩模式.
	 */
	private BigDecimal sscRebate;

	/**
	 * 分分彩模式.
	 */
	private BigDecimal ffcRebate;

	/**
	 * 十一选五模式.
	 */
	private BigDecimal syxwRebate;

	/**
	 * 快三模式.
	 */
	private BigDecimal ksRebate;

	/**
	 * PK10模式.
	 */
	private BigDecimal pk10Rebate;

	/**
	 * 低频彩模式.
	 */
	private BigDecimal dpcRebate;

	/**
	 * 投宝模式.
	 */
	private BigDecimal tbRebate;

	/**
	 * 六合彩模式.
	 */
	private BigDecimal lhcRebate;

	/**
	 * 快乐十分模式.
	 */
	private BigDecimal klsfRebate;

	/**
	 * 赠送金额.
	 */
	private BigDecimal presentMoney;

	/**
	 * 分红.
	 */
	private BigDecimal bonusScale;

	/**
	 * 代理类型.
	 */
	private String dailiLevel; //

	// 扩展属性 20&-&1958&1958&-&-&-&-&1&2&
	private Double resisterDonateMoney; // 注册赠送资金
	private Integer resisterDonatePoint; // 注册赠送积分
	private Integer continueDay; // 延续天数
	private String linkContent; // 注册链接

	// :TODO 该字段名称错误,后面需要修改掉
	private BigDecimal sscrebate; // 时时彩模式

	// 拓展字段
	private Integer domainEnabled;

	// 加载全部数据
	private Integer maxPageNum;

	public BigDecimal getBonusScale() {
		return bonusScale;
	}

	public void setBonusScale(BigDecimal bonusScale) {
		this.bonusScale = bonusScale;
	}

	public BigDecimal getPresentMoney() {
		return presentMoney;
	}

	public void setPresentMoney(BigDecimal presentMoney) {
		this.presentMoney = presentMoney;
	}

	public BigDecimal getSscrebate() {
		return sscrebate;
	}

	public void setSscrebate(BigDecimal sscrebate) {
		this.sscrebate = sscrebate;
	}

	public Integer getMaxPageNum() {
		return maxPageNum;
	}

	public void setMaxPageNum(Integer maxPageNum) {
		this.maxPageNum = maxPageNum;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBizSystem() {
		return bizSystem;
	}

	public BigDecimal getFfcRebate() {
		return ffcRebate;
	}

	public void setFfcRebate(BigDecimal ffcRebate) {
		this.ffcRebate = ffcRebate;
	}

	public BigDecimal getTbRebate() {
		return tbRebate;
	}

	public void setTbRebate(BigDecimal tbRebate) {
		this.tbRebate = tbRebate;
	}

	public BigDecimal getLhcRebate() {
		return lhcRebate;
	}

	public void setLhcRebate(BigDecimal lhcRebate) {
		this.lhcRebate = lhcRebate;
	}

	public BigDecimal getKlsfRebate() {
		return klsfRebate;
	}

	public void setKlsfRebate(BigDecimal klsfRebate) {
		this.klsfRebate = klsfRebate;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem == null ? null : bizSystem.trim();
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName == null ? null : userName.trim();
	}

	public String getLinkSets() {
		return linkSets;
	}

	public void setLinkSets(String linkSets) {
		this.linkSets = linkSets == null ? null : linkSets.trim();
	}

	public String getLinkContent() {
		return linkContent;
	}

	public void setLinkContent(String linkContent) {
		this.linkContent = linkContent == null ? null : linkContent.trim();
	}

	public String getLinkDes() {
		return linkDes;
	}

	public void setLinkDes(String linkDes) {
		this.linkDes = linkDes == null ? null : linkDes.trim();
	}

	public Integer getSetCount() {
		return setCount;
	}

	public void setSetCount(Integer setCount) {
		this.setCount = setCount;
	}

	public Integer getUseCount() {
		return useCount;
	}

	public void setUseCount(Integer useCount) {
		this.useCount = useCount;
	}

	public Date getLoseDate() {
		return loseDate;
	}

	public void setLoseDate(Date loseDate) {
		this.loseDate = loseDate;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

	public Double getResisterDonateMoney() {
		return resisterDonateMoney;
	}

	public void setResisterDonateMoney(Double resisterDonateMoney) {
		this.resisterDonateMoney = resisterDonateMoney;
	}

	public Integer getResisterDonatePoint() {
		return resisterDonatePoint;
	}

	public void setResisterDonatePoint(Integer resisterDonatePoint) {
		this.resisterDonatePoint = resisterDonatePoint;
	}

	public BigDecimal getSscRebate() {
		return sscRebate;
	}

	public void setSscRebate(BigDecimal sscRebate) {
		this.sscRebate = sscRebate;
	}

	public String getDailiLevel() {
		return dailiLevel;
	}

	public void setDailiLevel(String dailiLevel) {
		this.dailiLevel = dailiLevel;
	}

	public Integer getContinueDay() {
		return continueDay;
	}

	public void setContinueDay(Integer continueDay) {
		this.continueDay = continueDay;
	}

	public Integer getDomainEnabled() {
		return domainEnabled;
	}

	public void setDomainEnabled(Integer domainEnabled) {
		this.domainEnabled = domainEnabled;
	}

	public String getInvitationCode() {
		return invitationCode;
	}

	public void setInvitationCode(String invitationCode) {
		this.invitationCode = invitationCode;
	}

	public BigDecimal getKsRebate() {
		return ksRebate;
	}

	public void setKsRebate(BigDecimal ksRebate) {
		this.ksRebate = ksRebate;
	}

	public BigDecimal getSyxwRebate() {
		return syxwRebate;
	}

	public void setSyxwRebate(BigDecimal syxwRebate) {
		this.syxwRebate = syxwRebate;
	}

	public BigDecimal getPk10Rebate() {
		return pk10Rebate;
	}

	public void setPk10Rebate(BigDecimal pk10Rebate) {
		this.pk10Rebate = pk10Rebate;
	}

	public BigDecimal getDpcRebate() {
		return dpcRebate;
	}

	public void setDpcRebate(BigDecimal dpcRebate) {
		this.dpcRebate = dpcRebate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	// 创建时间的字符串
	public String getCreatedDateStr() {
		if (this.getCreatedDate() != null) {
			return DateUtils.getDateToStrByFormat(this.getCreatedDate(), "yyyy/MM/dd HH:mm:ss");
		}
		return "";
	}

	// 更新时间的字符串
	public String getUpdatedDateStr() {
		if (this.getUpdatedDate() != null) {
			return DateUtils.getDateToStrByFormat(this.getUpdatedDate(), "yyyy/MM/dd HH:mm:ss");
		}
		return "";
	}

	// 失效时间的字符串
	public String getLoseDateStr() {
		if (this.getLoseDate() != null) {
			return DateUtils.getDateToStrByFormat(this.getLoseDate(), "yyyy/MM/dd HH:mm:ss");
		}
		return "";
	}

	public String getBizSystemName() {

		return this.bizSystem == null ? "" : ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
	}
}