package com.team.lottery.vo;

import java.io.Serializable;
import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
/**
 * 域名管理
 * @author zhuang
 *
 */
public class BizSystemDomain implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = -7322682497279293187L;
	
	// PC域名类型
	public static final String DOMAIN_TYPE_PC = "PC";
	// MOBILE域名类型
	public static final String DOMAIN_TYPE_MOBILE = "MOBILE";
	// APP域名类型
	public static final String DOMAIN_TYPE_APP = "APP";
	
	private Long id;
    /**
     * 业务系统
     */
    private String bizSystem;

    /**
     * 域名url地址
     */
    private String domainUrl;
    /**
     * 域名类型 
     */
    private String domainType;

    /**
     * 申请状态  0申请中   1申请通过  2不通过
     */
    private Integer applyStatus;
    
    /**
     * 是否跳转https
     */
    private Integer isHttps;

    /**
     * 跳转手机端域名url(PC类型的域名使用)
     */
    private String jumpMobileDomainUrl;
    
    /**
     * 是否首推域名
     */
    private Integer firstFlag;

    /**
     * 审核备注
     */
    private String auditDesc;
    
    /**
     * 启用，禁用
     */
    private Integer enable;
     /**
      * 创建时间
      */
    private Date createTime;
     /**
      * 更新时间
      */
    private Date updateTime;
     /**
      * 操作人
      */
    private String updateAdmin;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public String getDomainUrl() {
        return domainUrl;
    }

    public void setDomainUrl(String domainUrl) {
        this.domainUrl = domainUrl == null ? null : domainUrl.trim();
    }

    public String getDomainType() {
        return domainType;
    }

    public void setDomainType(String domainType) {
        this.domainType = domainType == null ? null : domainType.trim();
    }

    public Integer getApplyStatus() {
        return applyStatus;
    }

    public void setApplyStatus(Integer applyStatus) {
        this.applyStatus = applyStatus;
    }
    
    public Integer getIsHttps() {
        return isHttps;
    }

    public void setIsHttps(Integer isHttps) {
        this.isHttps = isHttps;
    }

    public String getJumpMobileDomainUrl() {
        return jumpMobileDomainUrl;
    }

    public void setJumpMobileDomainUrl(String jumpMobileDomainUrl) {
        this.jumpMobileDomainUrl = jumpMobileDomainUrl == null ? null : jumpMobileDomainUrl.trim();
    }

    public String getAuditDesc() {
        return auditDesc==null?"":auditDesc;
    }

    public void setAuditDesc(String auditDesc) {
        this.auditDesc = auditDesc == null ? null : auditDesc.trim();
    }

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateAdmin() {
        return updateAdmin;
    }

    public void setUpdateAdmin(String updateAdmin) {
        this.updateAdmin = updateAdmin == null ? null : updateAdmin.trim();
    }
      
    public Integer getFirstFlag() {
		return firstFlag;
	}

	public void setFirstFlag(Integer firstFlag) {
		this.firstFlag = firstFlag;
	}

	public String getBizSystemName() {
    	
        return this.bizSystem==null?"":ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
    }
}