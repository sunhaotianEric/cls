package com.team.lottery.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.team.lottery.enums.ELotteryModel;
import com.team.lottery.enums.EOrderStatus;
import com.team.lottery.enums.EProstateStatus;
import com.team.lottery.util.DateUtils;
import com.team.lottery.util.StringUtils;

public class OrderAmount {
    private Long id;

    private String lotteryId;

    private String expect;

    private Integer multiple;

    private String lotteryType;

    private String lotteryTypeDes;

    private BigDecimal payMoney;

    private BigDecimal payPoint;
    
    private String bizSystem;

    private Integer userId;

    private String userName;

    private BigDecimal awardModel;

    private BigDecimal currentUserModel;

    private String lotteryModel;

    private Integer cathecticCount;

    private BigDecimal cathecticUnitPrice;

    private String detailDes;

    private String openCode;

    private String status;

    private String prostate;

    private String winInfo;

    private Integer winLevel;

    private BigDecimal winCost;

    private Integer afterNumber;

    private Integer stopAfterNumber;

    private Date closeDate;

    private Date createdDate;

    private Date updateDate;

    private Integer enabled;

    private String winCostStr;
    
    //扩展字段  用户推荐人的关系
    private String regfrom;  

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLotteryId() {
        return lotteryId;
    }

    public void setLotteryId(String lotteryId) {
        this.lotteryId = lotteryId == null ? null : lotteryId.trim();
    }

    public String getExpect() {
        return expect;
    }

    public void setExpect(String expect) {
        this.expect = expect == null ? null : expect.trim();
    }

    public Integer getMultiple() {
        return multiple;
    }

    public void setMultiple(Integer multiple) {
        this.multiple = multiple;
    }

    public String getLotteryType() {
        return lotteryType;
    }

    public void setLotteryType(String lotteryType) {
        this.lotteryType = lotteryType == null ? null : lotteryType.trim();
    }

    public String getLotteryTypeDes() {
        return lotteryTypeDes;
    }

    public void setLotteryTypeDes(String lotteryTypeDes) {
        this.lotteryTypeDes = lotteryTypeDes == null ? null : lotteryTypeDes.trim();
    }

    public BigDecimal getPayMoney() {
        return payMoney;
    }

    public void setPayMoney(BigDecimal payMoney) {
        this.payMoney = payMoney;
    }

    public BigDecimal getPayPoint() {
        return payPoint;
    }

    public void setPayPoint(BigDecimal payPoint) {
        this.payPoint = payPoint;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public BigDecimal getAwardModel() {
        return awardModel;
    }

    public void setAwardModel(BigDecimal awardModel) {
        this.awardModel = awardModel;
    }

    public BigDecimal getCurrentUserModel() {
        return currentUserModel;
    }

    public void setCurrentUserModel(BigDecimal currentUserModel) {
        this.currentUserModel = currentUserModel;
    }

    public String getLotteryModel() {
        return lotteryModel;
    }

    public void setLotteryModel(String lotteryModel) {
        this.lotteryModel = lotteryModel == null ? null : lotteryModel.trim();
    }

    public Integer getCathecticCount() {
        return cathecticCount;
    }

    public void setCathecticCount(Integer cathecticCount) {
        this.cathecticCount = cathecticCount;
    }

    public BigDecimal getCathecticUnitPrice() {
        return cathecticUnitPrice;
    }

    public void setCathecticUnitPrice(BigDecimal cathecticUnitPrice) {
        this.cathecticUnitPrice = cathecticUnitPrice;
    }

    public String getDetailDes() {
        return detailDes;
    }

    public void setDetailDes(String detailDes) {
        this.detailDes = detailDes == null ? null : detailDes.trim();
    }

    public String getOpenCode() {
        return openCode;
    }

    public void setOpenCode(String openCode) {
        this.openCode = openCode == null ? null : openCode.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getProstate() {
        return prostate;
    }

    public void setProstate(String prostate) {
        this.prostate = prostate == null ? null : prostate.trim();
    }

    public String getWinInfo() {
        return winInfo;
    }

    public void setWinInfo(String winInfo) {
        this.winInfo = winInfo == null ? null : winInfo.trim();
    }

    public Integer getWinLevel() {
        return winLevel;
    }

    public void setWinLevel(Integer winLevel) {
        this.winLevel = winLevel;
    }

    public BigDecimal getWinCost() {
        return winCost;
    }

    public void setWinCost(BigDecimal winCost) {
        this.winCost = winCost;
    }

    public Integer getAfterNumber() {
        return afterNumber;
    }

    public void setAfterNumber(Integer afterNumber) {
        this.afterNumber = afterNumber;
    }

    public Integer getStopAfterNumber() {
        return stopAfterNumber;
    }

    public void setStopAfterNumber(Integer stopAfterNumber) {
        this.stopAfterNumber = stopAfterNumber;
    }

    public Date getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(Date closeDate) {
        this.closeDate = closeDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public String getWinCostStr() {
        return winCostStr;
    }

    public void setWinCostStr(String winCostStr) {
        this.winCostStr = winCostStr == null ? null : winCostStr.trim();
    }
    
    
    
    public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}

	public String getRegfrom() {
		return regfrom;
	}

	public void setRegfrom(String regfrom) {
		this.regfrom = regfrom;
	}

	/**
     * 从订单去拷贝数据
     */
    public void copyFromOrder(Order order){
    	this.setLotteryId(order.getLotteryId());
    	this.setExpect(order.getExpect());
    	this.setMultiple(order.getMultiple());
    	this.setLotteryType(order.getLotteryType());
    	this.setLotteryTypeDes(order.getLotteryTypeDes());
    	this.setPayMoney(order.getPayMoney());
    	this.setPayPoint(order.getPayPoint());
    	this.setBizSystem(order.getBizSystem());
    	this.setUserId(order.getUserId());
    	this.setUserName(order.getUserName());
        this.setAwardModel(order.getAwardModel());
        this.setCurrentUserModel(order.getCurrentUserModel());
    	this.setLotteryModel(order.getLotteryModel());
    	this.setCathecticCount(order.getCathecticCount());
        this.setCathecticUnitPrice(order.getCathecticUnitPrice());
        this.setDetailDes(order.getDetailDes());
        this.setOpenCode(order.getOpenCode());
        this.setStatus(order.getStatus());
        this.setProstate(order.getProstate());
        this.setWinInfo(order.getWinInfo());
        this.setWinLevel(order.getWinLevel());
        this.setWinCost(order.getWinCost());
        this.setAfterNumber(order.getAfterNumber());
        this.setStopAfterNumber(order.getStopAfterNumber());
        this.setCloseDate(order.getCloseDate());
        this.setCreatedDate(order.getCreatedDate());
        this.setUpdateDate(order.getUpdateDate());
        this.setEnabled(order.getEnabled());
        this.setWinCostStr(order.getWinCostStr());
    }
    
    /**
	 * 投注模式描述
	 * @return
	 */
	public String getLotteryModelStr(){
		if(!StringUtils.isEmpty(this.getLotteryModel())){
			return ELotteryModel.valueOf(this.getLotteryModel()).getDescription();
		}
		return "";
	}
	
	//获取订单状态的描述
	public String getStatusStr(){
	    if(!StringUtils.isEmpty(this.getStatus())){
	    	return EOrderStatus.valueOf(this.getStatus()).getDescription();
	    }
		return "";
	}
	
	//获取中奖状态的描述
	public String getProstateStatusStr(){
		if(!StringUtils.isEmpty(this.getProstate())){
			return EProstateStatus.valueOf(this.getProstate()).getDescription();
		}
		return "";
	}
	
	//获取时间字符串
	public String getUpdateDateStr(){
		if(this.getUpdateDate() != null){
	    	return DateUtils.getDateToStrByFormat(this.getUpdateDate(), "yyyy/MM/dd HH:mm:ss");
		}
		return "";
	}
	
	//获取时间字符串
		public String getCreatedDateStr(){
			if(this.getCreatedDate() != null){
		    	return DateUtils.getDateToStrByFormat(this.getCreatedDate(), "yyyy/MM/dd HH:mm:ss");
			}
			return "";
		}
    
    
}