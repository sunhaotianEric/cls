package com.team.lottery.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.enums.EFundAutoWithdrawDealStatus;
import com.team.lottery.enums.EFundRechargeStatus;
import com.team.lottery.enums.EFundWithDrawStatus;
import com.team.lottery.util.DateUtils;

public class WithdrawOrder {

	// 订单未锁定
	public static final Integer UNLOCKED = 0;
	// 订单锁定
	public static final Integer LOCKED = 1;

	private Long id;

	private String serialNumber;

	private String bizSystem;

	private Integer userId;

	private String userName;

	private Integer lockStatus;

	private String lockAdmin;

	private BigDecimal applyValue;

	private BigDecimal accountValue;

	private BigDecimal feeValue;

	private BigDecimal currentCanWithdrawMoney;

	private String fromType;

	private String operateDes;

	private String dealStatus;

	private String autoWithdrawDealStatus;

	private String remark;

	private String bankName;

	private String bankType;

	/*private String bankCity;

	private String bankProvince;*/

	private String subbranchName;

	private String bankAccountName;

	private String bankCardNum;

	private Date createdDate;

	private String updateAdmin;

	private Date updateDate;

	// 第三方类型.
	private String payType;

	// 对应商户号.
	private String memberId;

	// 对应的自动出款配置ID
	private Integer autoConfigId;

	// 扩展属性
	private String sign;
	private Long timeDifference; // 时间差
	private String barcodeUrl; // 二维码图片地址
	private Integer num; // 笔数、人数


/*
	public String getBankCity() {
		return bankCity;
	}

	public void setBankCity(String bankCity) {
		this.bankCity = bankCity;
	}

	public String getBankProvince() {
		return bankProvince;
	}

	public void setBankProvince(String bankProvince) {
		this.bankProvince = bankProvince;
	}
*/

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public Integer getAutoConfigId() {
		return autoConfigId;
	}

	public void setAutoConfigId(Integer autoConfigId) {
		this.autoConfigId = autoConfigId;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber == null ? null : serialNumber.trim();
	}

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem == null ? null : bizSystem.trim();
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName == null ? null : userName.trim();
	}

	public Integer getLockStatus() {
		return lockStatus;
	}

	public void setLockStatus(Integer lockStatus) {
		this.lockStatus = lockStatus;
	}

	public String getLockAdmin() {
		return lockAdmin;
	}

	public void setLockAdmin(String lockAdmin) {
		this.lockAdmin = lockAdmin == null ? null : lockAdmin.trim();
	}

	public BigDecimal getApplyValue() {
		return applyValue == null ? BigDecimal.ZERO : applyValue;
	}

	public void setApplyValue(BigDecimal applyValue) {
		this.applyValue = applyValue == null ? BigDecimal.ZERO : applyValue;
	}

	public BigDecimal getAccountValue() {
		return accountValue == null ? BigDecimal.ZERO : accountValue;
	}

	public void setAccountValue(BigDecimal accountValue) {
		this.accountValue = accountValue == null ? BigDecimal.ZERO : accountValue;
	}

	public BigDecimal getFeeValue() {
		return feeValue == null ? BigDecimal.ZERO : feeValue;
	}

	public void setFeeValue(BigDecimal feeValue) {
		this.feeValue = feeValue == null ? BigDecimal.ZERO : feeValue;
	}

	public BigDecimal getCurrentCanWithdrawMoney() {
		return currentCanWithdrawMoney;
	}

	public void setCurrentCanWithdrawMoney(BigDecimal currentCanWithdrawMoney) {
		this.currentCanWithdrawMoney = currentCanWithdrawMoney;
	}

	public String getFromType() {
		return fromType;
	}

	public void setFromType(String fromType) {
		this.fromType = fromType == null ? null : fromType.trim();
	}

	public String getOperateDes() {
		return operateDes;
	}

	public void setOperateDes(String operateDes) {
		this.operateDes = operateDes == null ? null : operateDes.trim();
	}

	public String getDealStatus() {
		return dealStatus;
	}

	public void setDealStatus(String dealStatus) {
		this.dealStatus = dealStatus == null ? null : dealStatus.trim();
	}

	public String getAutoWithdrawDealStatus() {
		return autoWithdrawDealStatus;
	}

	public void setAutoWithdrawDealStatus(String autoWithdrawDealStatus) {
		this.autoWithdrawDealStatus = autoWithdrawDealStatus == null ? null : autoWithdrawDealStatus.trim();
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName == null ? null : bankName.trim();
	}

	public String getBankType() {
		return bankType;
	}

	public void setBankType(String bankType) {
		this.bankType = bankType == null ? null : bankType.trim();
	}

	public String getSubbranchName() {
		return subbranchName;
	}

	public void setSubbranchName(String subbranchName) {
		this.subbranchName = subbranchName == null ? null : subbranchName.trim();
	}

	public String getBankAccountName() {
		return bankAccountName;
	}

	public void setBankAccountName(String bankAccountName) {
		this.bankAccountName = bankAccountName;
	}

	public String getBankCardNum() {
		return bankCardNum;
	}

	public void setBankCardNum(String bankCardNum) {
		this.bankCardNum = bankCardNum;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdateAdmin() {
		return updateAdmin;
	}

	public void setUpdateAdmin(String updateAdmin) {
		this.updateAdmin = updateAdmin == null ? null : updateAdmin.trim();
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getBizSystemName() {
		return this.bizSystem == null ? "" : ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public Long getTimeDifference() {
		return timeDifference;
	}

	public void setTimeDifference(Long timeDifference) {
		this.timeDifference = timeDifference;
	}

	public String getBarcodeUrl() {
		return barcodeUrl;
	}

	public void setBarcodeUrl(String barcodeUrl) {
		this.barcodeUrl = barcodeUrl;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	/**
	 * 状态描述
	 * 
	 * @return
	 */
	public String getStatusStr() {
		if (this.getDealStatus() != null) {
			String statuss = this.getDealStatus();
			EFundRechargeStatus[] rechargeOrderStatuss = EFundRechargeStatus.values();
			for (EFundRechargeStatus orderStatus : rechargeOrderStatuss) {
				if (statuss.equals(orderStatus.name())) {
					return orderStatus.getDescription();
				}
			}
			EFundWithDrawStatus[] withDrawStatuss = EFundWithDrawStatus.values();
			for (EFundWithDrawStatus withdrawOrderStatus : withDrawStatuss) {
				if (statuss.equals(withdrawOrderStatus.name())) {
					return withdrawOrderStatus.getDescription();
				}
			}
		}
		return "";
	}

	// 获取时间字符串
	public String getCreatedDateStr() {
		if (this.getCreatedDate() != null) {
			return DateUtils.getDateToStrByFormat(this.getCreatedDate(), "yyyy/MM/dd HH:mm:ss");
		}
		return "";
	}

	// 获取时间字符串
	public String getUpdateDateStr() {
		if (this.getUpdateDate() != null) {
			return DateUtils.getDateToStrByFormat(this.getUpdateDate(), "yyyy/MM/dd HH:mm:ss");
		}
		return "";
	}

	/**
	 * 状态描述
	 * 
	 * @return
	 */
	public String getAutoWithdrawDealStatusStr() {
		if (this.getAutoWithdrawDealStatus() != null) {
			String autoWithdrawDealstatuss = this.getAutoWithdrawDealStatus();
			EFundAutoWithdrawDealStatus[] rechargeOrderStatuss = EFundAutoWithdrawDealStatus.values();
			for (EFundAutoWithdrawDealStatus orderStatus : rechargeOrderStatuss) {
				if (autoWithdrawDealstatuss.equals(orderStatus.name())) {
					return orderStatus.getDescription();
				}
			}
		}
		return "";
	}
}