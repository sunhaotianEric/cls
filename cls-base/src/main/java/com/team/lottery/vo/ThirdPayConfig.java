package com.team.lottery.vo;

import java.util.Date;

public class ThirdPayConfig {
	private Long id;

	private String chargeType;

	private String chargeDes;

	private String interfaceVersion;

	private String signType;

	private String address;

	private String payUrl;

	private String notifyUrl;

	private String returnUrl;

	// 是否开启Https
	private Integer supportHttps;

	private Integer enabled;

	private Date createTime;

	private String createAdmin;

	private Date updateTime;

	private String updateAdmin;

	private String postUrl;
	
	private String authorizedIp;

	private Integer authorizedIpEnabled;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getChargeType() {
		return chargeType;
	}

	public void setChargeType(String chargeType) {
		this.chargeType = chargeType == null ? null : chargeType.trim();
	}

	public String getChargeDes() {
		return chargeDes;
	}

	public void setChargeDes(String chargeDes) {
		this.chargeDes = chargeDes == null ? null : chargeDes.trim();
	}

	public String getInterfaceVersion() {
		return interfaceVersion;
	}

	public void setInterfaceVersion(String interfaceVersion) {
		this.interfaceVersion = interfaceVersion == null ? null : interfaceVersion.trim();
	}

	public String getSignType() {
		return signType;
	}

	public void setSignType(String signType) {
		this.signType = signType == null ? null : signType.trim();
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address == null ? null : address.trim();
	}

	public String getPayUrl() {
		return payUrl;
	}

	public void setPayUrl(String payUrl) {
		this.payUrl = payUrl == null ? null : payUrl.trim();
	}

	public String getNotifyUrl() {
		return notifyUrl;
	}

	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl == null ? null : notifyUrl.trim();
	}

	public String getReturnUrl() {
		return returnUrl;
	}

	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl == null ? null : returnUrl.trim();
	}

	public Integer getSupportHttps() {
		return supportHttps;
	}

	public void setSupportHttps(Integer supportHttps) {
		this.supportHttps = supportHttps;
	}

	public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreateAdmin() {
		return createAdmin;
	}

	public void setCreateAdmin(String createAdmin) {
		this.createAdmin = createAdmin == null ? null : createAdmin.trim();
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getUpdateAdmin() {
		return updateAdmin;
	}

	public void setUpdateAdmin(String updateAdmin) {
		this.updateAdmin = updateAdmin == null ? null : updateAdmin.trim();
	}

	public String getPostUrl() {
		return postUrl;
	}

	public void setPostUrl(String postUrl) {
		this.postUrl = postUrl == null ? null : postUrl.trim();
	}

	public String getAuthorizedIp() {
		return authorizedIp;
	}

	public void setAuthorizedIp(String authorizedIp) {
        this.authorizedIp = authorizedIp == null ? null : authorizedIp.trim();
	}

	public Integer getAuthorizedIpEnabled() {
		return authorizedIpEnabled;
	}

	public void setAuthorizedIpEnabled(Integer authorizedIpEnabled) {
		this.authorizedIpEnabled = authorizedIpEnabled;
	}

	/**
	 * 第三方支付类型
	 * 
	 * @return
	 */
	public String getChargeTypeStr() {
		if (this.getChargeType() != null) {
			/*String thirdPay = this.getChargeType();
			EFundThirdPayType[] eFundThirdPayType = EFundThirdPayType.values();
			for (EFundThirdPayType thirdPayType : eFundThirdPayType) {
				if (thirdPay.equals(thirdPayType.name())) {
					return thirdPayType.getDescription();
				}
			}*/
			return this.getChargeDes();

		}
		return "";
	}

}