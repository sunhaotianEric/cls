package com.team.lottery.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.enums.EDaySalaryOrderStatus;
import com.team.lottery.util.DateUtils;

public class DaySalaryOrder {
	private Long id;

	private String serialNumber;

	private String bizSystem;

	private Integer userId;

	private String userName;

	private Integer fromUserId;

	private String fromUserName;

	private BigDecimal money;

	private BigDecimal lotteryMoney;

	private Date belongDate;

	private String releaseStatus;

	private String remark;

	private Date releaseDate;

	private Date createdDate;

	private Date updateDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber == null ? null : serialNumber.trim();
	}

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem == null ? null : bizSystem.trim();
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName == null ? null : userName.trim();
	}

	public Integer getFromUserId() {
		return fromUserId;
	}

	public void setFromUserId(Integer fromUserId) {
		this.fromUserId = fromUserId;
	}

	public String getFromUserName() {
		return fromUserName;
	}

	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName == null ? null : fromUserName.trim();
	}

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public BigDecimal getLotteryMoney() {
		return lotteryMoney;
	}

	public void setLotteryMoney(BigDecimal lotteryMoney) {
		this.lotteryMoney = lotteryMoney;
	}

	public Date getBelongDate() {
		return belongDate;
	}

	public void setBelongDate(Date belongDate) {
		this.belongDate = belongDate;
	}

	public String getReleaseStatus() {
		return releaseStatus;
	}

	public void setReleaseStatus(String releaseStatus) {
		this.releaseStatus = releaseStatus == null ? null : releaseStatus
				.trim();
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	/**
	 * 工资发放状态
	 * 
	 * @return
	 */
	public String getReleaseStatusStr() {
		if (this.getReleaseStatus() != null) {
			String releaseStatus = this.getReleaseStatus();
			EDaySalaryOrderStatus[] eDaySalaryOrderStatusList = EDaySalaryOrderStatus
					.values();
			for (EDaySalaryOrderStatus eDaySalaryOrderStatus : eDaySalaryOrderStatusList) {
				if (releaseStatus.equals(eDaySalaryOrderStatus.name())) {
					return eDaySalaryOrderStatus.getDescription();
				}
			}

		}
		return "";
	}

	/**
	 * 所属时间
	 * 
	 */
	public String getBelongDateStr() {
		return DateUtils.getDateToStrByFormat(this.belongDate, "yyyy-MM-dd");
	}
	
	/**
	 * 发放时间
	 * 
	 */
	public String getReleaseDateStr() {
		return DateUtils.getDateToStrByFormat(this.releaseDate, "yyyy-MM-dd HH:mm:ss");
	}

	/**
	 * 业务系统
	 * 
	 */
	public String getBizSystemName() {
		return ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
	}
}