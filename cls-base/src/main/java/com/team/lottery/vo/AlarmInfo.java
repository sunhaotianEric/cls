package com.team.lottery.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.util.DateUtils;

public class AlarmInfo {
	
    private Integer id;

    private String bizSystem;

    private String alarmType;

    private Integer smsSendOpen;

    private Integer emailSendOpen;

    private Date createDate;

    private String alarmContent;

    /**
     * 拓展字段
     * @return
     */
     
    private BigDecimal money;//金额
    private String userName;//金额对应的人
    private String alarmPhone;//告警手机号码
    private String alarmEmail;//告警邮箱地址
    
    
    public String getAlarmPhone() {
		return alarmPhone;
	}

	public void setAlarmPhone(String alarmPhone) {
		this.alarmPhone = alarmPhone;
	}

	public String getAlarmEmail() {
		return alarmEmail;
	}

	public void setAlarmEmail(String alarmEmail) {
		this.alarmEmail = alarmEmail;
	}

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public String getAlarmType() {
        return alarmType;
    }

    public void setAlarmType(String alarmType) {
        this.alarmType = alarmType == null ? null : alarmType.trim();
    }

    public Integer getSmsSendOpen() {
        return smsSendOpen;
    }

    public void setSmsSendOpen(Integer smsSendOpen) {
        this.smsSendOpen = smsSendOpen;
    }

    public Integer getEmailSendOpen() {
        return emailSendOpen;
    }

    public void setEmailSendOpen(Integer emailSendOpen) {
        this.emailSendOpen = emailSendOpen;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getAlarmContent() {
        return alarmContent;
    }

    public void setAlarmContent(String alarmContent) {
        this.alarmContent = alarmContent == null ? null : alarmContent.trim();
    }
    //创建时间字符串
    public String getCreateDateStr(){
    	return DateUtils.getDateToStrByFormat(this.getCreateDate(), "yyyy年MM月dd日 HH:mm:ss");
    }
    public String getBizSystemName() {
        return this.bizSystem==null?"":ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
    }
}