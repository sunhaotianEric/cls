package com.team.lottery.vo;

import java.util.Date;

public class UserCustomLotteryKind {
    private Long id;

    private Integer userId;

    private String userName;

    private String setType;

    private String position1LotteryType;

    private String position2LotteryType;

    private String position3LotteryType;

    private String position4LotteryType;

    private String position5LotteryType;

    private String position6LotteryType;

    private String position7LotteryType;

    private String position8LotteryType;

    private String position9LotteryType;

    private Date createdDate;

    private Date updateDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getSetType() {
        return setType;
    }

    public void setSetType(String setType) {
        this.setType = setType == null ? null : setType.trim();
    }

    public String getPosition1LotteryType() {
        return position1LotteryType;
    }

    public void setPosition1LotteryType(String position1LotteryType) {
        this.position1LotteryType = position1LotteryType == null ? null : position1LotteryType.trim();
    }

    public String getPosition2LotteryType() {
        return position2LotteryType;
    }

    public void setPosition2LotteryType(String position2LotteryType) {
        this.position2LotteryType = position2LotteryType == null ? null : position2LotteryType.trim();
    }

    public String getPosition3LotteryType() {
        return position3LotteryType;
    }

    public void setPosition3LotteryType(String position3LotteryType) {
        this.position3LotteryType = position3LotteryType == null ? null : position3LotteryType.trim();
    }

    public String getPosition4LotteryType() {
        return position4LotteryType;
    }

    public void setPosition4LotteryType(String position4LotteryType) {
        this.position4LotteryType = position4LotteryType == null ? null : position4LotteryType.trim();
    }

    public String getPosition5LotteryType() {
        return position5LotteryType;
    }

    public void setPosition5LotteryType(String position5LotteryType) {
        this.position5LotteryType = position5LotteryType == null ? null : position5LotteryType.trim();
    }

    public String getPosition6LotteryType() {
        return position6LotteryType;
    }

    public void setPosition6LotteryType(String position6LotteryType) {
        this.position6LotteryType = position6LotteryType == null ? null : position6LotteryType.trim();
    }

    public String getPosition7LotteryType() {
        return position7LotteryType;
    }

    public void setPosition7LotteryType(String position7LotteryType) {
        this.position7LotteryType = position7LotteryType == null ? null : position7LotteryType.trim();
    }

    public String getPosition8LotteryType() {
        return position8LotteryType;
    }

    public void setPosition8LotteryType(String position8LotteryType) {
        this.position8LotteryType = position8LotteryType == null ? null : position8LotteryType.trim();
    }

    public String getPosition9LotteryType() {
        return position9LotteryType;
    }

    public void setPosition9LotteryType(String position9LotteryType) {
        this.position9LotteryType = position9LotteryType == null ? null : position9LotteryType.trim();
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}