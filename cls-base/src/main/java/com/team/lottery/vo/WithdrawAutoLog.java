package com.team.lottery.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.enums.EWithdrawAutoStatus;
import com.team.lottery.enums.EWithdrawAutoType;
import com.team.lottery.util.DateUtil;

public class WithdrawAutoLog {
    private Long id;

    private String bizSystem;

    private Integer userId;

    private String userName;

    private String adminName;//操作管理员名字

    private String chargeType;//第三方支付名称

    private String chargeDes;

    private String memberId;//商户号

    private String type;//自动出款类型

    private String dealStatus;//处理状态

    private Long withdrowOrderId;

    private String serialNumber;

    private BigDecimal money;//出款金额

    private String bankCardNum;//银行卡号

    private String bankAccountName;//银行卡开户姓名

    private String bankName;//银行名称

    private String bankType;//银行类型(英文

    private Date createdDate;//创建时间

    private Date updatedDate;
    
    
    // 查询开始时间.
 	private Date startDate;

 	// 查询结束时间.
 	private Date endDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName == null ? null : adminName.trim();
    }

    public String getChargeType() {
        return chargeType;
    }

    public void setChargeType(String chargeType) {
        this.chargeType = chargeType == null ? null : chargeType.trim();
    }

    public String getChargeDes() {
        return chargeDes;
    }

    public void setChargeDes(String chargeDes) {
        this.chargeDes = chargeDes == null ? null : chargeDes.trim();
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId == null ? null : memberId.trim();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public String getDealStatus() {
        return dealStatus;
    }

    public void setDealStatus(String dealStatus) {
        this.dealStatus = dealStatus == null ? null : dealStatus.trim();
    }

    public Long getWithdrowOrderId() {
        return withdrowOrderId;
    }

    public void setWithdrowOrderId(Long withdrowOrderId) {
        this.withdrowOrderId = withdrowOrderId;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber == null ? null : serialNumber.trim();
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getBankCardNum() {
        return bankCardNum;
    }

    public void setBankCardNum(String bankCardNum) {
        this.bankCardNum = bankCardNum == null ? null : bankCardNum.trim();
    }

    public String getBankAccountName() {
        return bankAccountName;
    }

    public void setBankAccountName(String bankAccountName) {
        this.bankAccountName = bankAccountName == null ? null : bankAccountName.trim();
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName == null ? null : bankName.trim();
    }

    public String getBankType() {
        return bankType;
    }

    public void setBankType(String bankType) {
        this.bankType = bankType == null ? null : bankType.trim();
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public String getTypeName() {
        return EWithdrawAutoType.valueOf(type).getDescription();
    }
	
	public String getDealStatusName() {
        return EWithdrawAutoStatus.valueOf(dealStatus).getDescription();
    }
    
	// 获取创建时间时分秒的格式
	public String getCreatedDateStr() {
		String dateStr = "";
		if (this.createdDate != null) {
			dateStr = DateUtil.parseDate(createdDate, "yyyy-MM-dd HH:mm:ss");
		}
		return dateStr;
	}
	
	// 获取创建时间时分秒的格式
		public String getUpdatedDateStr() {
			String dateStr = "";
			if (this.updatedDate != null) {
				dateStr = DateUtil.parseDate(updatedDate, "yyyy-MM-dd HH:mm:ss");
			}
			return dateStr;
		}
	
	/**
     * 第三方支付类型
     * @return
     */
    public String getChargeTypeStr(){
    	if(this.getChargeType() != null){
    		/*String thirdPay = this.getChargeType();
    		EFundThirdPayType[] eFundThirdPayType = EFundThirdPayType.values();
    		for(EFundThirdPayType thirdPayType : eFundThirdPayType){
    			if(thirdPay.equals(thirdPayType.name())){
    				return thirdPayType.getDescription();
    			}
    		}*/
    		return this.getChargeDes();
    	}
		return "";
    }
    
    public String getBizSystemName() {
		return this.bizSystem == null ? "" : ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
	}
}