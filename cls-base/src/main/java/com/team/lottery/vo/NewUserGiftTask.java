package com.team.lottery.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.util.DateUtil;

public class NewUserGiftTask {
    private Integer id;

    private String bizSystem;

    private String userName;

    private Integer userId;

    private Integer appDownTaskStatus;

    private BigDecimal appDownTaskMoney;

    private Integer completeInfoTaskStatus;

    private BigDecimal completeInfoTaskMoney;

    private Integer complateSafeTaskStatus;

    private BigDecimal complateSafeTaskMoney;

    private Integer lotteryTaskStatus;

    private BigDecimal lotteryTaskMoney;

    private Integer rechargeTaskStatus;

    private BigDecimal rechargeTaskMoney;

    private Integer extendUserTaskStatus;

    private BigDecimal extendUserTaskMoney;

    private Date createTime;

    private Date updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getAppDownTaskStatus() {
        return appDownTaskStatus;
    }

    public void setAppDownTaskStatus(Integer appDownTaskStatus) {
        this.appDownTaskStatus = appDownTaskStatus;
    }

    public BigDecimal getAppDownTaskMoney() {
        return appDownTaskMoney;
    }

    public void setAppDownTaskMoney(BigDecimal appDownTaskMoney) {
        this.appDownTaskMoney = appDownTaskMoney;
    }

    public Integer getCompleteInfoTaskStatus() {
        return completeInfoTaskStatus;
    }

    public void setCompleteInfoTaskStatus(Integer completeInfoTaskStatus) {
        this.completeInfoTaskStatus = completeInfoTaskStatus;
    }

    public BigDecimal getCompleteInfoTaskMoney() {
        return completeInfoTaskMoney;
    }

    public void setCompleteInfoTaskMoney(BigDecimal completeInfoTaskMoney) {
        this.completeInfoTaskMoney = completeInfoTaskMoney;
    }

    public Integer getComplateSafeTaskStatus() {
        return complateSafeTaskStatus;
    }

    public void setComplateSafeTaskStatus(Integer complateSafeTaskStatus) {
        this.complateSafeTaskStatus = complateSafeTaskStatus;
    }

    public BigDecimal getComplateSafeTaskMoney() {
        return complateSafeTaskMoney;
    }

    public void setComplateSafeTaskMoney(BigDecimal complateSafeTaskMoney) {
        this.complateSafeTaskMoney = complateSafeTaskMoney;
    }

    public Integer getLotteryTaskStatus() {
        return lotteryTaskStatus;
    }

    public void setLotteryTaskStatus(Integer lotteryTaskStatus) {
        this.lotteryTaskStatus = lotteryTaskStatus;
    }

    public BigDecimal getLotteryTaskMoney() {
        return lotteryTaskMoney;
    }

    public void setLotteryTaskMoney(BigDecimal lotteryTaskMoney) {
        this.lotteryTaskMoney = lotteryTaskMoney;
    }

    public Integer getRechargeTaskStatus() {
        return rechargeTaskStatus;
    }

    public void setRechargeTaskStatus(Integer rechargeTaskStatus) {
        this.rechargeTaskStatus = rechargeTaskStatus;
    }

    public BigDecimal getRechargeTaskMoney() {
        return rechargeTaskMoney;
    }

    public void setRechargeTaskMoney(BigDecimal rechargeTaskMoney) {
        this.rechargeTaskMoney = rechargeTaskMoney;
    }

    public Integer getExtendUserTaskStatus() {
        return extendUserTaskStatus;
    }

    public void setExtendUserTaskStatus(Integer extendUserTaskStatus) {
        this.extendUserTaskStatus = extendUserTaskStatus;
    }

    public BigDecimal getExtendUserTaskMoney() {
        return extendUserTaskMoney;
    }

    public void setExtendUserTaskMoney(BigDecimal extendUserTaskMoney) {
        this.extendUserTaskMoney = extendUserTaskMoney;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
    public String getBizSystemName() {
        return this.bizSystem==null?"":ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
    }
    //获取创建时间时分秒的格式
    public String getCreateDateStr() {
    	String dateStr = "";
    	if(this.createTime != null) {
    		dateStr = DateUtil.parseDate(createTime, "yyyy-MM-dd HH:mm:ss");
    	}
    	return dateStr;
    }
}