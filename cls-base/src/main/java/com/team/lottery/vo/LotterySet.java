package com.team.lottery.vo;

/**
 * 系统信息
 * 
 * @author chenhsh
 *
 */
public class LotterySet {

	// 重庆时时彩是否开启
	public static Integer isCQSSCOpen;
	// 江西时时彩是否开启
	public static Integer isJXSSCOpen;
	// 黑龙江时时彩是否开启
	public static Integer isHLJSSCOpen;
	// 新疆时时彩是否开启
	public static Integer isXJSSCOpen;
	// 天津时时彩是否开启
	public static Integer isTJSSCOpen;
	
	// 老重庆时时彩是否开启.
	public static Integer isLCQSSCOpen;

	public static Integer isJSSSCOpen;
	public static Integer isBJSSCOpen;
	public static Integer isGDSSCOpen;
	public static Integer isSCSSCOpen;
	public static Integer isSHSSCOpen;
	public static Integer isSDSSCOpen;

	//十分时时彩是否开启
	public static Integer isSHFSSCOpen;	
	//三分时时彩是否开启
	public static Integer isSFSSCOpen;	
	//五分时时彩是否开启
	public static Integer isWFSSCOpen;
	// 金鹰分分彩是否开启
	public static Integer isJLFFCOpen;
	// 韩国1.5分彩是否开启
	public static Integer isHGFFCOpen;

	// 新加坡2分彩是否开启
	public static Integer isXJPLFCOpen;
	// 台湾5分彩是否开启
	public static Integer isTWWFCOpen;

	// 广东11选5是否开启
	public static Integer isGDSYXWOpen;
	// 山东11选5是否开启
	public static Integer isSDSYXWOpen;
	// 江西11选5是否开启
	public static Integer isJXSYXWOpen;
	// 重庆11选5是否开启
	public static Integer isCQSYXWOpen;
	// 福建11选5是否开启
	public static Integer isFJSYXWOpen;
	// 五分11选5是否开启
	public static Integer isWFSYXWOpen;
	// 三分11选5是否开启
	public static Integer isSFSYXWOpen;

	// 三分快3是否开启
	public static Integer isSFKSOpen;
	// 五分快3是否开启
	public static Integer isWFKSOpen;
	// 江苏快3是否开启
	public static Integer isJSKSOpen;
	// 安徽快3是否开启
	public static Integer isAHKSOpen;
	// 吉林快3是否开启
	public static Integer isJLKSOpen;
	// 湖北快3是否开启
	public static Integer isHBKSOpen;
	// 北京快3是否开启
	public static Integer isBJKSOpen;
	// 幸运快3是否开启
	public static Integer isJYKSOpen;
	// 广西快3是否开启
	public static Integer isGXKSOpen;
	// 甘肃快3是否开启
	public static Integer isGSKSOpen;
	// 上海快3是否开启
	public static Integer isSHKSOpen;

	// 广东快乐十分是否开启
	public static Integer isGDKLSFOpen;

	// 上海时时乐是否开启
	public static Integer isSHSSLDPCOpen;
	// 福彩3D是否开启
	public static Integer isFC3DDPCOpen;

	//香港六合彩是否开启
	public static Integer isLHCOpen;
	//澳门六合彩是否开启
	public static Integer isAMLHCOpen;
	//幸运六合彩是否开启 
   public static Integer isXYLHCOpen;
	// 幸运飞艇是否开启
	public static Integer isXYFTOpen;
	// 三分PK10是否开启
	public static Integer isSFPK10Open;
	// 五分PK10是否开启
	public static Integer isWFPK10Open;
	// 十分PK10是否开启
	public static Integer isSHFPK10Open;
	// 北京PK10是否开启
	public static Integer isBJPK10Open;
	// 极速PK10是否开启
	public static Integer isJSPK10Open;

	// 江苏骰宝是否开启
	public static Integer isJSTBOpen;
	// 安徽骰宝是否开启
	public static Integer isAHTBOpen;
	// 吉林骰宝是否开启
	public static Integer isJLTBOpen;
	// 湖北骰宝是否开启
	public static Integer isHBTBOpen;
	// 北京骰宝是否开启
	public static Integer isBJTBOpen;
	// 北京骰宝是否开启
	public static Integer isXYEBOpen;
	
	
	public  Integer getIsXYEBOpen() {
		return isXYEBOpen;
	}

	public void setIsXYEBOpen(Integer isXYEBOpen) {
		this.isXYEBOpen = isXYEBOpen;
	}

	public Integer getIsXYLHCOpen() {
		return isXYLHCOpen;
	}

	public void setIsXYLHCOpen(Integer isXYLHCOpen) {
		this.isXYLHCOpen = isXYLHCOpen;
	}
	public Integer getIsLHCOpen() {
		return isLHCOpen;
	}

	public void setIsLHCOpen(Integer isLHCOpen) {
		this.isLHCOpen = isLHCOpen;
	}

	public Integer getIsCQSSCOpen() {
		return isCQSSCOpen;
	}

	public void setIsCQSSCOpen(Integer isCQSSCOpen) {
		this.isCQSSCOpen = isCQSSCOpen;
	}

	public Integer getIsJXSSCOpen() {
		return isJXSSCOpen;
	}

	public void setIsJXSSCOpen(Integer isJXSSCOpen) {
		this.isJXSSCOpen = isJXSSCOpen;
	}

	public Integer getIsHLJSSCOpen() {
		return isHLJSSCOpen;
	}

	public void setIsHLJSSCOpen(Integer isHLJSSCOpen) {
		this.isHLJSSCOpen = isHLJSSCOpen;
	}

	public Integer getIsXJSSCOpen() {
		return isXJSSCOpen;
	}

	public void setIsXJSSCOpen(Integer isXJSSCOpen) {
		this.isXJSSCOpen = isXJSSCOpen;
	}

	public Integer getIsTJSSCOpen() {
		return isTJSSCOpen;
	}

	public void setIsTJSSCOpen(Integer isTJSSCOpen) {
		this.isTJSSCOpen = isTJSSCOpen;
	}

	public Integer getIsJLFFCOpen() {
		return isJLFFCOpen;
	}

	public void setIsJLFFCOpen(Integer isJLFFCOpen) {
		this.isJLFFCOpen = isJLFFCOpen;
	}

	public Integer getIsHGFFCOpen() {
		return isHGFFCOpen;
	}

	public void setIsHGFFCOpen(Integer isHGFFCOpen) {
		this.isHGFFCOpen = isHGFFCOpen;
	}

	public Integer getIsXJPLFCOpen() {
		return isXJPLFCOpen;
	}

	public void setIsXJPLFCOpen(Integer isXJPLFCOpen) {
		this.isXJPLFCOpen = isXJPLFCOpen;
	}

	public Integer getIsTWWFCOpen() {
		return isTWWFCOpen;
	}

	public void setIsTWWFCOpen(Integer isTWWFCOpen) {
		this.isTWWFCOpen = isTWWFCOpen;
	}

	public Integer getIsGDSYXWOpen() {
		return isGDSYXWOpen;
	}

	public void setIsGDSYXWOpen(Integer isGDSYXWOpen) {
		this.isGDSYXWOpen = isGDSYXWOpen;
	}

	public Integer getIsSDSYXWOpen() {
		return isSDSYXWOpen;
	}

	public void setIsSDSYXWOpen(Integer isSDSYXWOpen) {
		this.isSDSYXWOpen = isSDSYXWOpen;
	}

	public Integer getIsJXSYXWOpen() {
		return isJXSYXWOpen;
	}

	public void setIsJXSYXWOpen(Integer isJXSYXWOpen) {
		this.isJXSYXWOpen = isJXSYXWOpen;
	}

	public Integer getIsCQSYXWOpen() {
		return isCQSYXWOpen;
	}

	public void setIsCQSYXWOpen(Integer isCQSYXWOpen) {
		this.isCQSYXWOpen = isCQSYXWOpen;
	}

	public Integer getIsFJSYXWOpen() {
		return isFJSYXWOpen;
	}

	public void setIsFJSYXWOpen(Integer isFJSYXWOpen) {
		this.isFJSYXWOpen = isFJSYXWOpen;
	}

	public Integer getIsJSKSOpen() {
		return isJSKSOpen;
	}

	public void setIsJSKSOpen(Integer isJSKSOpen) {
		this.isJSKSOpen = isJSKSOpen;
	}

	public Integer getIsAHKSOpen() {
		return isAHKSOpen;
	}

	public void setIsAHKSOpen(Integer isAHKSOpen) {
		this.isAHKSOpen = isAHKSOpen;
	}

	public Integer getIsJLKSOpen() {
		return isJLKSOpen;
	}

	public void setIsJLKSOpen(Integer isJLKSOpen) {
		this.isJLKSOpen = isJLKSOpen;
	}

	public Integer getIsHBKSOpen() {
		return isHBKSOpen;
	}

	public void setIsHBKSOpen(Integer isHBKSOpen) {
		this.isHBKSOpen = isHBKSOpen;
	}

	public Integer getIsBJKSOpen() {
		return isBJKSOpen;
	}

	public void setIsBJKSOpen(Integer isBJKSOpen) {
		this.isBJKSOpen = isBJKSOpen;
	}

	public Integer getIsJYKSOpen() {
		return isJYKSOpen;
	}

	public void setIsJYKSOpen(Integer isJYKSOpen) {
		this.isJYKSOpen = isJYKSOpen;
	}

	public Integer getIsGXKSOpen() {
		return isGXKSOpen;
	}

	public Integer getIsGSKSOpen() {
		return isGSKSOpen;
	}

	public void setIsGSKSOpen(Integer isGSKSOpen) {
		this.isGSKSOpen = isGSKSOpen;
	}

	public Integer getIsSHKSOpen() {
		return isSHKSOpen;
	}

	public void setIsSHKSOpen(Integer isSHKSOpen) {
		this.isSHKSOpen = isSHKSOpen;
	}

	public void setIsGXKSOpen(Integer isGXKSOpen) {
		this.isGXKSOpen = isGXKSOpen;
	}

	public Integer getIsGDKLSFOpen() {
		return isGDKLSFOpen;
	}

	public void setIsGDKLSFOpen(Integer isGDKLSFOpen) {
		this.isGDKLSFOpen = isGDKLSFOpen;
	}

	public Integer getIsSHSSLDPCOpen() {
		return isSHSSLDPCOpen;
	}

	public void setIsSHSSLDPCOpen(Integer isSHSSLDPCOpen) {
		this.isSHSSLDPCOpen = isSHSSLDPCOpen;
	}
	

	public Integer getIsFC3DDPCOpen() {
		return isFC3DDPCOpen;
	}

	public void setIsFC3DDPCOpen(Integer isFC3DDPCOpen) {
		this.isFC3DDPCOpen = isFC3DDPCOpen;
	}

	public Integer getIsBJPK10Open() {
		return isBJPK10Open;
	}

	public void setIsBJPK10Open(Integer isBJPK10Open) {
		this.isBJPK10Open = isBJPK10Open;
	}

	public Integer getIsJSPK10Open() {
		return isJSPK10Open;
	}

	public void setIsJSPK10Open(Integer isJSPK10Open) {
		this.isJSPK10Open = isJSPK10Open;
	}

	public Integer getIsJSTBOpen() {
		return isJSTBOpen;
	}

	public void setIsJSTBOpen(Integer isJSTBOpen) {
		this.isJSTBOpen = isJSTBOpen;
	}

	public Integer getIsAHTBOpen() {
		return isAHTBOpen;
	}

	public void setIsAHTBOpen(Integer isAHTBOpen) {
		this.isAHTBOpen = isAHTBOpen;
	}

	public Integer getIsJLTBOpen() {
		return isJLTBOpen;
	}

	public void setIsJLTBOpen(Integer isJLTBOpen) {
		this.isJLTBOpen = isJLTBOpen;
	}

	public Integer getIsHBTBOpen() {
		return isHBTBOpen;
	}

	public void setIsHBTBOpen(Integer isHBTBOpen) {
		this.isHBTBOpen = isHBTBOpen;
	}

	public Integer getIsBJTBOpen() {
		return isBJTBOpen;
	}

	public void setIsBJTBOpen(Integer isBJTBOpen) {
		this.isBJTBOpen = isBJTBOpen;
	}

	public Integer getIsSFSSCOpen() {
		return isSFSSCOpen;
	}

	public void setIsSFSSCOpen(Integer isSFSSCOpen) {
		this.isSFSSCOpen = isSFSSCOpen;
	}

	public Integer getIsWFSSCOpen() {
		return isWFSSCOpen;
	}

	public void setIsWFSSCOpen(Integer isWFSSCOpen) {
		this.isWFSSCOpen = isWFSSCOpen;
	}

	public Integer getIsSFKSOpen() {
		return isSFKSOpen;
	}

	public void setIsSFKSOpen(Integer isSFKSOpen) {
		this.isSFKSOpen = isSFKSOpen;
	}

	public Integer getIsWFKSOpen() {
		return isWFKSOpen;
	}

	public void setIsWFKSOpen(Integer isWFKSOpen) {
		this.isWFKSOpen = isWFKSOpen;
	}

	public Integer getIsSHFSSCOpen() {
		return isSHFSSCOpen;
	}

	public void setIsSHFSSCOpen(Integer isSHFSSCOpen) {
		this.isSHFSSCOpen = isSHFSSCOpen;
	}
	
	public Integer getIsWFSYXWOpen() {
		return isWFSYXWOpen;
	}

	public void setIsWFSYXWOpen(Integer isWFSYXWOpen) {
		this.isWFSYXWOpen = isWFSYXWOpen;
	}
	
	public Integer getIsSFSYXWOpen() {
		return isSFSYXWOpen;
	}

	public void setIsSFSYXWOpen(Integer isSFSYXWOpen) {
		this.isSFSYXWOpen = isSFSYXWOpen;
	}

	public Integer getIsSFPK10Open() {
		return isSFPK10Open;
	}

	public void setIsSFPK10Open(Integer isSFPK10Open) {
		LotterySet.isSFPK10Open = isSFPK10Open;
	}

	public Integer getIsWFPK10Open() {
		return isWFPK10Open;
	}

	public void setIsWFPK10Open(Integer isWFPK10Open) {
		LotterySet.isWFPK10Open = isWFPK10Open;
	}

	public Integer getIsSHFPK10Open() {
		return isSHFPK10Open;
	}

	public void setIsSHFPK10Open(Integer isSHFPK10Open) {
		LotterySet.isSHFPK10Open = isSHFPK10Open;
	}

	public Integer getIsXYFTOpen() {
		return isXYFTOpen;
	}

	public void setIsXYFTOpen(Integer isXYFTOpen) {
		LotterySet.isXYFTOpen = isXYFTOpen;
	}

	public Integer getIsLCQSSCOpen() {
		return isLCQSSCOpen;
	}

	public void setIsLCQSSCOpen(Integer isLCQSSCOpen) {
		this.isLCQSSCOpen = isLCQSSCOpen;
	}

	public static Integer getIsJSSSCOpen() {
		return isJSSSCOpen;
	}

	public static void setIsJSSSCOpen(Integer isJSSSCOpen) {
		LotterySet.isJSSSCOpen = isJSSSCOpen;
	}

	public static Integer getIsBJSSCOpen() {
		return isBJSSCOpen;
	}

	public static void setIsBJSSCOpen(Integer isBJSSCOpen) {
		LotterySet.isBJSSCOpen = isBJSSCOpen;
	}

	public static Integer getIsGDSSCOpen() {
		return isGDSSCOpen;
	}

	public static void setIsGDSSCOpen(Integer isGDSSCOpen) {
		LotterySet.isGDSSCOpen = isGDSSCOpen;
	}

	public static Integer getIsSCSSCOpen() {
		return isSCSSCOpen;
	}

	public static void setIsSCSSCOpen(Integer isSCSSCOpen) {
		LotterySet.isSCSSCOpen = isSCSSCOpen;
	}

	public static Integer getIsSHSSCOpen() {
		return isSHSSCOpen;
	}

	public static void setIsSHSSCOpen(Integer isSHSSCOpen) {
		LotterySet.isSHSSCOpen = isSHSSCOpen;
	}

	public static Integer getIsSDSSCOpen() {
		return isSDSSCOpen;
	}

	public static void setIsSDSSCOpen(Integer isSDSSCOpen) {
		LotterySet.isSDSSCOpen = isSDSSCOpen;
	}

	public static Integer getIsAMLHCOpen() {
		return isAMLHCOpen;
	}

	public static void setIsAMLHCOpen(Integer isAMLHCOpen) {
		LotterySet.isAMLHCOpen = isAMLHCOpen;
	}
}
