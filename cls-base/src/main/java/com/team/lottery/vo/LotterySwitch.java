package com.team.lottery.vo;

import java.util.Date;

public class LotterySwitch {
	// 自增ID.
    private Long id;
    // 系统.
    private String bizSystem;
    // 彩种类型.
    private String lotteryType;
    // 彩种类型描述.
    private String lotteryTypeDes;
    // 彩种开启状态.(0关闭 1开启 2彩种维护 3暂停销售).
    private Integer lotteryStatus;
    // 创建日期.
    private Date createDate;
    // 更新人.
    private String updateAdmin;
    // 更新时间.
    private Date updateDate;

    private Integer closingTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public String getLotteryType() {
        return lotteryType;
    }

    public void setLotteryType(String lotteryType) {
        this.lotteryType = lotteryType == null ? null : lotteryType.trim();
    }

    public String getLotteryTypeDes() {
        return lotteryTypeDes;
    }

    public void setLotteryTypeDes(String lotteryTypeDes) {
        this.lotteryTypeDes = lotteryTypeDes == null ? null : lotteryTypeDes.trim();
    }

    public Integer getLotteryStatus() {
        return lotteryStatus;
    }

    public void setLotteryStatus(Integer lotteryStatus) {
        this.lotteryStatus = lotteryStatus;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUpdateAdmin() {
        return updateAdmin;
    }

    public void setUpdateAdmin(String updateAdmin) {
        this.updateAdmin = updateAdmin == null ? null : updateAdmin.trim();
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getClosingTime() {
        return closingTime;
    }

    public void setClosingTime(Integer closingTime) {
        this.closingTime = closingTime;
    }
}