package com.team.lottery.vo;

import java.io.Serializable;
import java.util.Date;

import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.enums.EOpenInterfaceType;

public class OpenCodeApi  implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;

    private String lotteryKind;

    private String apiUrl;

    private String apiType;

    private String backApiUrl;

    private String backApiType;

    private Integer sleepSecond;

    private String currentApiUrl;

    private String currentApiType;

    private Date createTime;

    private Date updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLotteryKind() {
        return lotteryKind;
    }

    public String getLotteryKindName() {
        return ELotteryKind.valueOf(lotteryKind).getDescription();
    }
    
    public void setLotteryKind(String lotteryKind) {
        this.lotteryKind = lotteryKind == null ? null : lotteryKind.trim();
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl == null ? null : apiUrl.trim();
    }

    public String getApiType() {
        return apiType;
    }
    
    public String getApiTypeName() {
        return EOpenInterfaceType.valueOf(apiType).getDescription();
    }

    public void setApiType(String apiType) {
        this.apiType = apiType == null ? null : apiType.trim();
    }

    public String getBackApiUrl() {
        return backApiUrl;
    }

    public void setBackApiUrl(String backApiUrl) {
        this.backApiUrl = backApiUrl == null ? null : backApiUrl.trim();
    }

    public String getBackApiType() {
        return backApiType;
    }
    public String getBackApiTypeName() {
        return EOpenInterfaceType.valueOf(backApiType).getDescription();
    }
    public void setBackApiType(String backApiType) {
        this.backApiType = backApiType == null ? null : backApiType.trim();
    }

    public Integer getSleepSecond() {
        return sleepSecond;
    }

    public void setSleepSecond(Integer sleepSecond) {
        this.sleepSecond = sleepSecond;
    }

    public String getCurrentApiUrl() {
        return currentApiUrl;
    }

    public void setCurrentApiUrl(String currentApiUrl) {
        this.currentApiUrl = currentApiUrl == null ? null : currentApiUrl.trim();
    }

    public String getCurrentApiType() {
        return currentApiType;
    }
    public String getCurrentApiTypeName() {
        return EOpenInterfaceType.valueOf(currentApiType).getDescription();
    }
    public void setCurrentApiType(String currentApiType) {
        this.currentApiType = currentApiType == null ? null : currentApiType.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}