package com.team.lottery.vo;

import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;

import com.team.lottery.enums.EMeasureType;
import com.team.lottery.enums.EUserDailLiLevel;

public class UserBonusConfig {
    private Long id;

    private BigDecimal measurement;

    private BigDecimal scale;

    private BigDecimal scale2;

    private String measureType;  //量值类型  

    private String description;

    private String dailiLevel;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getMeasurement() {
        return measurement;
    }

    public void setMeasurement(BigDecimal measurement) {
        this.measurement = measurement;
    }

    public BigDecimal getScale() {
        return scale;
    }

    public void setScale(BigDecimal scale) {
        this.scale = scale;
    }

    public BigDecimal getScale2() {
        return scale2;
    }

    public void setScale2(BigDecimal scale2) {
        this.scale2 = scale2;
    }

    public String getMeasureType() {
        return measureType;
    }

    public void setMeasureType(String measureType) {
        this.measureType = measureType == null ? null : measureType.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public String getDailiLevel() {
        return dailiLevel;
    }

    public void setDailiLevel(String dailiLevel) {
        this.dailiLevel = dailiLevel == null ? null : dailiLevel.trim();
    }
    
    //获取代理级别的描述
  	public String getDailiLevelStr(){
  		if(!StringUtils.isEmpty(this.getDailiLevel())){
  			if("SUPERDIRECTAGENCY".equals(dailiLevel)) {
  				return "特级直属";
  			} 
  			return EUserDailLiLevel.valueOf(this.getDailiLevel()).getDescription();
  		}
  		return "";
  	}
  	
  	//获取量值类型的描述
  	public String getMeasureTypeStr(){
  		if(!StringUtils.isEmpty(this.getMeasureType())){
  			return EMeasureType.valueOf(this.getMeasureType()).getDescription();
  		}
  		return "";
  	}
}