package com.team.lottery.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.team.lottery.enums.EUserDailLiLevel;
import com.team.lottery.util.DateUtils;
import com.team.lottery.util.StringUtils;

public class UserDel {
    private Integer id;

    private Integer code;

    private String username;

    private String regfrom;

    private Integer teamMember;

    private Integer lowerLevel;

    private String userpass;

    private String userpass1;

    private Integer version;

    private String phone;

    private String truename;

    private Integer haveSafePassword;

    private Integer haveSafeQuestion;

    private String identityid;

    private String birthday;

    private Integer sex;

    private String address;

    private String qq;

    private String province;

    private String city;

    private Date addtime;

    private Integer dataLock;

    private Integer islock;

    private Integer state;

    private String dailiLevel;
    
    private String vipLevel;

    private Integer starLevel;

    private BigDecimal point;

    private BigDecimal icemoney;

    private String email;

    private String logip;

    private Integer logins;

    private Date lastlogout;

    private Date logtime;

    private BigDecimal sscrebate;

    private BigDecimal ffcrebate;

    private BigDecimal syxwrebate;

    private BigDecimal ksrebate;

    private BigDecimal klsfrebate;

    private BigDecimal dpcrebate;

    private BigDecimal bonusScale;

    private BigDecimal money;

    private BigDecimal uermoney;

    private BigDecimal totalUerMoney;

    private BigDecimal totalNetRecharge;

    private BigDecimal totalQuickRecharge;

    private BigDecimal totalRecharge;

    private BigDecimal totalRechargePresent;

    private BigDecimal totalActivitiesMoney;

    private BigDecimal totalWithdraw;

    private BigDecimal totalFeevalue;

    private BigDecimal totalExtend;

    private BigDecimal totalRegister;

    private BigDecimal totalLottery;

    private BigDecimal totalShopPoint;

    private BigDecimal totalWin;

    private BigDecimal totalRebate;

    private BigDecimal totalPercentage;

    private BigDecimal totalPayTranfer;

    private BigDecimal totalIncomeTranfer;

    private BigDecimal totalHalfMonthBonus;

    private BigDecimal totalDayBonus;

    private BigDecimal totalMonthSalary;

    private BigDecimal totalDaySalary;

    private BigDecimal totalDayCommission;

    private BigDecimal remainExtend;

    private Date deleteDate;
    private String deleteDateStr;
    private String deleteCause;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public  String getRegfrom() {
        return regfrom;
    }

    public void setRegfrom(String regfrom) {
        this.regfrom = regfrom == null ? null : regfrom.trim();
    }

    public Integer getTeamMember() {
        return teamMember;
    }

    public void setTeamMember(Integer teamMember) {
        this.teamMember = teamMember;
    }

    public Integer getLowerLevel() {
        return lowerLevel;
    }

    public void setLowerLevel(Integer lowerLevel) {
        this.lowerLevel = lowerLevel;
    }

    public String getUserpass() {
        return userpass;
    }

    public void setUserpass(String userpass) {
        this.userpass = userpass == null ? null : userpass.trim();
    }

    public String getUserpass1() {
        return userpass1;
    }

    public void setUserpass1(String userpass1) {
        this.userpass1 = userpass1 == null ? null : userpass1.trim();
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getTruename() {
        return truename;
    }

    public void setTruename(String truename) {
        this.truename = truename == null ? null : truename.trim();
    }

    public Integer getHaveSafePassword() {
        return haveSafePassword;
    }

    public void setHaveSafePassword(Integer haveSafePassword) {
        this.haveSafePassword = haveSafePassword;
    }

    public Integer getHaveSafeQuestion() {
        return haveSafeQuestion;
    }

    public void setHaveSafeQuestion(Integer haveSafeQuestion) {
        this.haveSafeQuestion = haveSafeQuestion;
    }

    public String getIdentityid() {
        return identityid;
    }

    public void setIdentityid(String identityid) {
        this.identityid = identityid == null ? null : identityid.trim();
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday == null ? null : birthday.trim();
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq == null ? null : qq.trim();
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public Date getAddtime() {
        return addtime;
    }

    public void setAddtime(Date addtime) {
        this.addtime = addtime;
    }

    public Integer getDataLock() {
        return dataLock;
    }

    public void setDataLock(Integer dataLock) {
        this.dataLock = dataLock;
    }

    public Integer getIslock() {
        return islock;
    }

    public void setIslock(Integer islock) {
        this.islock = islock;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getDailiLevel() {
        return dailiLevel;
    }

    public void setDailiLevel(String dailiLevel) {
        this.dailiLevel = dailiLevel == null ? null : dailiLevel.trim();
    }
    
    public String getVipLevel() {
		return vipLevel;
	}

	public void setVipLevel(String vipLevel) {
		this.vipLevel = vipLevel;
	}

	public Integer getStarLevel() {
        return starLevel;
    }

    public void setStarLevel(Integer starLevel) {
        this.starLevel = starLevel;
    }

    public BigDecimal getPoint() {
        return point;
    }

    public void setPoint(BigDecimal point) {
        this.point = point;
    }

    public BigDecimal getIcemoney() {
        return icemoney;
    }

    public void setIcemoney(BigDecimal icemoney) {
        this.icemoney = icemoney;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getLogip() {
        return logip;
    }

    public void setLogip(String logip) {
        this.logip = logip == null ? null : logip.trim();
    }

    public Integer getLogins() {
        return logins;
    }

    public void setLogins(Integer logins) {
        this.logins = logins;
    }

    public Date getLastlogout() {
        return lastlogout;
    }

    public void setLastlogout(Date lastlogout) {
        this.lastlogout = lastlogout;
    }

    public Date getLogtime() {
        return logtime;
    }

    public void setLogtime(Date logtime) {
        this.logtime = logtime;
    }

    public BigDecimal getSscrebate() {
        return sscrebate;
    }

    public void setSscrebate(BigDecimal sscrebate) {
        this.sscrebate = sscrebate;
    }

    public BigDecimal getFfcrebate() {
        return ffcrebate;
    }

    public void setFfcrebate(BigDecimal ffcrebate) {
        this.ffcrebate = ffcrebate;
    }

    public BigDecimal getSyxwrebate() {
        return syxwrebate;
    }

    public void setSyxwrebate(BigDecimal syxwrebate) {
        this.syxwrebate = syxwrebate;
    }

    public BigDecimal getKsrebate() {
        return ksrebate;
    }

    public void setKsrebate(BigDecimal ksrebate) {
        this.ksrebate = ksrebate;
    }

    public BigDecimal getKlsfrebate() {
        return klsfrebate;
    }

    public void setKlsfrebate(BigDecimal klsfrebate) {
        this.klsfrebate = klsfrebate;
    }

    public BigDecimal getDpcrebate() {
        return dpcrebate;
    }

    public void setDpcrebate(BigDecimal dpcrebate) {
        this.dpcrebate = dpcrebate;
    }

    public BigDecimal getBonusScale() {
        return bonusScale;
    }

    public void setBonusScale(BigDecimal bonusScale) {
        this.bonusScale = bonusScale;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public BigDecimal getUermoney() {
        return uermoney;
    }

    public void setUermoney(BigDecimal uermoney) {
        this.uermoney = uermoney;
    }

    public BigDecimal getTotalUerMoney() {
        return totalUerMoney;
    }

    public void setTotalUerMoney(BigDecimal totalUerMoney) {
        this.totalUerMoney = totalUerMoney;
    }

    public BigDecimal getTotalNetRecharge() {
        return totalNetRecharge;
    }

    public void setTotalNetRecharge(BigDecimal totalNetRecharge) {
        this.totalNetRecharge = totalNetRecharge;
    }

    public BigDecimal getTotalQuickRecharge() {
        return totalQuickRecharge;
    }

    public void setTotalQuickRecharge(BigDecimal totalQuickRecharge) {
        this.totalQuickRecharge = totalQuickRecharge;
    }

    public BigDecimal getTotalRecharge() {
        return totalRecharge;
    }

    public void setTotalRecharge(BigDecimal totalRecharge) {
        this.totalRecharge = totalRecharge;
    }

    public BigDecimal getTotalRechargePresent() {
        return totalRechargePresent;
    }

    public void setTotalRechargePresent(BigDecimal totalRechargePresent) {
        this.totalRechargePresent = totalRechargePresent;
    }

    public BigDecimal getTotalActivitiesMoney() {
        return totalActivitiesMoney;
    }

    public void setTotalActivitiesMoney(BigDecimal totalActivitiesMoney) {
        this.totalActivitiesMoney = totalActivitiesMoney;
    }

    public BigDecimal getTotalWithdraw() {
        return totalWithdraw;
    }

    public void setTotalWithdraw(BigDecimal totalWithdraw) {
        this.totalWithdraw = totalWithdraw;
    }

    public BigDecimal getTotalFeevalue() {
        return totalFeevalue;
    }

    public void setTotalFeevalue(BigDecimal totalFeevalue) {
        this.totalFeevalue = totalFeevalue;
    }

    public BigDecimal getTotalExtend() {
        return totalExtend;
    }

    public void setTotalExtend(BigDecimal totalExtend) {
        this.totalExtend = totalExtend;
    }

    public BigDecimal getTotalRegister() {
        return totalRegister;
    }

    public void setTotalRegister(BigDecimal totalRegister) {
        this.totalRegister = totalRegister;
    }

    public BigDecimal getTotalLottery() {
        return totalLottery;
    }

    public void setTotalLottery(BigDecimal totalLottery) {
        this.totalLottery = totalLottery;
    }

    public BigDecimal getTotalShopPoint() {
        return totalShopPoint;
    }

    public void setTotalShopPoint(BigDecimal totalShopPoint) {
        this.totalShopPoint = totalShopPoint;
    }

    public BigDecimal getTotalWin() {
        return totalWin;
    }

    public void setTotalWin(BigDecimal totalWin) {
        this.totalWin = totalWin;
    }

    public BigDecimal getTotalRebate() {
        return totalRebate;
    }

    public void setTotalRebate(BigDecimal totalRebate) {
        this.totalRebate = totalRebate;
    }

    public BigDecimal getTotalPercentage() {
        return totalPercentage;
    }

    public void setTotalPercentage(BigDecimal totalPercentage) {
        this.totalPercentage = totalPercentage;
    }

    public BigDecimal getTotalPayTranfer() {
        return totalPayTranfer;
    }

    public void setTotalPayTranfer(BigDecimal totalPayTranfer) {
        this.totalPayTranfer = totalPayTranfer;
    }

    public BigDecimal getTotalIncomeTranfer() {
        return totalIncomeTranfer;
    }

    public void setTotalIncomeTranfer(BigDecimal totalIncomeTranfer) {
        this.totalIncomeTranfer = totalIncomeTranfer;
    }

    public BigDecimal getTotalHalfMonthBonus() {
        return totalHalfMonthBonus;
    }

    public void setTotalHalfMonthBonus(BigDecimal totalHalfMonthBonus) {
        this.totalHalfMonthBonus = totalHalfMonthBonus;
    }

    public BigDecimal getTotalDayBonus() {
        return totalDayBonus;
    }

    public void setTotalDayBonus(BigDecimal totalDayBonus) {
        this.totalDayBonus = totalDayBonus;
    }

    public BigDecimal getTotalMonthSalary() {
        return totalMonthSalary;
    }

    public void setTotalMonthSalary(BigDecimal totalMonthSalary) {
        this.totalMonthSalary = totalMonthSalary;
    }

    public BigDecimal getTotalDaySalary() {
        return totalDaySalary;
    }

    public void setTotalDaySalary(BigDecimal totalDaySalary) {
        this.totalDaySalary = totalDaySalary;
    }

    public BigDecimal getTotalDayCommission() {
        return totalDayCommission;
    }

    public void setTotalDayCommission(BigDecimal totalDayCommission) {
        this.totalDayCommission = totalDayCommission;
    }

    public BigDecimal getRemainExtend() {
        return remainExtend;
    }

    public void setRemainExtend(BigDecimal remainExtend) {
        this.remainExtend = remainExtend;
    }

    public Date getDeleteDate() {
        return deleteDate;
    }

    public void setDeleteDate(Date deleteDate) {
        this.deleteDate = deleteDate;
    }

    public String getDeleteCause() {
        return deleteCause;
    }

    public void setDeleteCause(String deleteCause) {
        this.deleteCause = deleteCause == null ? null : deleteCause.trim();
    }

	public String getDeleteDateStr() {
		return DateUtils.getDateToStrByFormat(getDeleteDate(), "yyyy/MM/dd HH:mm:ss");
	}

	public void setDeleteDateStr(String deleteDateStr) {
		this.deleteDateStr = deleteDateStr;
	}

	//获取代理级别的描述
	public String getDailiLevelStr(){
		if(!StringUtils.isEmpty(this.getDailiLevel())){
			return EUserDailLiLevel.valueOf(this.getDailiLevel()).getDescription();
		}
		return "";
	}

}