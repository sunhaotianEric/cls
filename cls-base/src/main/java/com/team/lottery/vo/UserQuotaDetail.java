package com.team.lottery.vo;

import java.math.BigDecimal;
import java.util.Date;

public class UserQuotaDetail {
	
	//增加
	public static final Integer ADD_TYPE = 1;
	//减少
	public static final Integer REDUCE_TYPE = 0;
	
    private Long id;

    /**
     * 业务系统
     */
    private String bizSystem;

    /**
     * 配额归属用户id
     */
    private Integer userId;

    /**
     * 配额归属用户名
     */
    private String userName;

    /**
     * 使用配额注册用户id
     */
    private Integer operateUserId;

    /**
     * 使用配额注册用户名
     */
    private String operateUserName;

    /**
     *   
     * 使用配额的模式
     */
    private BigDecimal operateUseSscRebate;

    private Integer updateLevel;
    /**
     * 配额变动类型 0减少  1增加 
     */
    private Integer type;

    /**
     * 改变配额数
     */
    private Integer changeNum;

 
    /**
     * 操作后配额
     */
    private Integer afterRemain;

    /**
     * 操作前配额
     */
    private Integer beforeRemain;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建时间
     */
    private Date createdDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public Integer getOperateUserId() {
        return operateUserId;
    }

    public void setOperateUserId(Integer operateUserId) {
        this.operateUserId = operateUserId;
    }

    public String getOperateUserName() {
        return operateUserName;
    }

    public void setOperateUserName(String operateUserName) {
        this.operateUserName = operateUserName == null ? null : operateUserName.trim();
    }

    public BigDecimal getOperateUseSscRebate() {
        return operateUseSscRebate;
    }

    public void setOperateUseSscRebate(BigDecimal operateUseSscRebate) {
        this.operateUseSscRebate = operateUseSscRebate;
    }

    public Integer getUpdateLevel() {
        return updateLevel;
    }

    public void setUpdateLevel(Integer updateLevel) {
        this.updateLevel = updateLevel;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getChangeNum() {
        return changeNum;
    }

    public void setChangeNum(Integer changeNum) {
        this.changeNum = changeNum;
    }

    public Integer getAfterRemain() {
        return afterRemain;
    }

    public void setAfterRemain(Integer afterRemain) {
        this.afterRemain = afterRemain;
    }

    public Integer getBeforeRemain() {
        return beforeRemain;
    }

    public void setBeforeRemain(Integer beforeRemain) {
        this.beforeRemain = beforeRemain;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}