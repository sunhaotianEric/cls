package com.team.lottery.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.enums.ETemplateType;
import com.team.lottery.system.SystemConfigConstant;

public class BizSystemInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5259133174455436572L;

	private Long id;

	/**
	 * 业务系统
	 */
	private String bizSystem;

	/**
	 * 业务系统名称
	 */
	private String bizSystemName;

	/**
	 * 模板
	 */
	private String templateType;

	/**
	 * 首页彩种
	 */
	private String indexLotteryKind;

	/**
	 * 热门彩种
	 */
	private String hotLotteryKinds;

	/**
	 * 首页默认彩种
	 */
	private String indexDefaultLotteryKinds;

	/**
	 * 手机端首页默认彩种
	 */
	private String indexMobileDefaultLotteryKinds;

	/**
	 * 登陆logo图片地址
	 */
	private String logoUrl;

	/**
	 * 页面顶部logo图片地址
	 */
	private String headLogoUrl;

	/**
	 * 手机端顶部logo图片地址
	 */
	private String mobileHeadLogoUrl;

	/**
	 * 手机端登录页面logo图片地址
	 */
	private String mobileLogoUrl;

	/**
	 * 客服地址
	 */
	private String customUrl;

	/**
	 * 电话号码
	 */
	private String telphoneNum;
	/**
	 * qq
	 */
	private String qq;
	private String andriodIosDownloadUrl;

	private String appDownLogo;

	private String appShowname;
	/**
	 * 二维码地址
	 */
	private String barcodeUrl;

	/**
	 * 手机版二维码访问地址
	 */
	private String mobileBarcodeUrl;

	/**
	 * 是否有app版
	 */
	private Integer hasApp;

	/**
	 * 安卓app端二维码地址
	 */
	private String appBarcodeUrl;

	/**
	 * 安卓app端下载地址
	 */
	private String appAndroidDownloadUrl;
	/**
	 * 安卓苹果端二维码地址
	 */
	private String appIosBarcodeUrl;

	/**
	 * 安卓苹果下载地址
	 */
	private String appStoreIosUrl;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 更新时间
	 */
	private Date updateTime;

	/**
	 * 操作人
	 */
	private String updateAdmin;

	/**
	 * 优势编辑
	 */
	private String advantageDesc;

	/**
	 * 页脚内容
	 */
	private String footer;
	/**
	 * 站点统计脚本
	 */
	private String siteScript;
	/**
	 * 手机站点脚本统计
	 */
	private String mobileSiteScript;
	/**
	 * 网站图标 url
	 */
	private String faviconUrl;
	private BigDecimal rechargeSecond;

	private BigDecimal withdrawSecond;
	// pc快捷彩种
	private String pcQuickLotteryKind;
	// pc热门彩种
	private String pcPopularLotteryKind;
	// pc首页主推彩种
	private String pcIndexPushLotteryKind;
	// 联系我们
    private String contactUs;
	// 关于我们
	private String aboutUs;
	// 加盟代理
	private String franchiseAgent;
	// 法律说明
	private String legalStatement;
	// 隐私说明
    private String privacyStatement;

	public String getContactUs() {
		return contactUs;
	}

	public void setContactUs(String contactUs) {
		this.contactUs = contactUs;
	}

	public String getAboutUs() {
		return aboutUs;
	}

	public void setAboutUs(String aboutUs) {
		this.aboutUs = aboutUs;
	}

	public String getFranchiseAgent() {
		return franchiseAgent;
	}

	public void setFranchiseAgent(String franchiseAgent) {
		this.franchiseAgent = franchiseAgent;
	}

	public String getLegalStatement() {
		return legalStatement;
	}

	public void setLegalStatement(String legalStatement) {
		this.legalStatement = legalStatement;
	}

	public String getPrivacyStatement() {
		return privacyStatement;
	}

	public void setPrivacyStatement(String privacyStatement) {
		this.privacyStatement = privacyStatement;
	}

	public String getPcQuickLotteryKind() {
		return pcQuickLotteryKind;
	}

	public void setPcQuickLotteryKind(String pcQuickLotteryKind) {
		this.pcQuickLotteryKind = pcQuickLotteryKind;
	}

	public String getPcPopularLotteryKind() {
		return pcPopularLotteryKind;
	}

	public void setPcPopularLotteryKind(String pcPopularLotteryKind) {
		this.pcPopularLotteryKind = pcPopularLotteryKind;
	}

	public String getPcIndexPushLotteryKind() {
		return pcIndexPushLotteryKind;
	}

	public void setPcIndexPushLotteryKind(String pcIndexPushLotteryKind) {
		this.pcIndexPushLotteryKind = pcIndexPushLotteryKind;
	}

	public String getFaviconUrl() {
		return faviconUrl;
	}

	public void setFaviconUrl(String faviconUrl) {
		this.faviconUrl = faviconUrl;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}

	public String getBizSystemName() {
		return bizSystemName;
	}

	public void setBizSystemName(String bizSystemName) {
		this.bizSystemName = bizSystemName;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getTemplateType() {
		return templateType;
	}

	public void setTemplateType(String templateType) {
		this.templateType = templateType;
	}

	public String getIndexLotteryKind() {
		return indexLotteryKind;
	}

	public void setIndexLotteryKind(String indexLotteryKind) {
		this.indexLotteryKind = indexLotteryKind;
	}

	public String getIndexDefaultLotteryKinds() {
		return indexDefaultLotteryKinds;
	}

	public void setIndexDefaultLotteryKinds(String indexDefaultLotteryKinds) {
		this.indexDefaultLotteryKinds = indexDefaultLotteryKinds;
	}

	public String getAndriodIosDownloadUrl() {
		return andriodIosDownloadUrl;
	}

	public void setAndriodIosDownloadUrl(String andriodIosDownloadUrl) {
		this.andriodIosDownloadUrl = andriodIosDownloadUrl;
	}

	public String getAppDownLogo() {
		return appDownLogo;
	}

	public void setAppDownLogo(String appDownLogo) {
		this.appDownLogo = appDownLogo;
	}

	public String getAppShowname() {
		return appShowname;
	}

	public void setAppShowname(String appShowname) {
		this.appShowname = appShowname;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl == null ? null : logoUrl.trim();
	}

	public String getCustomUrl() {
		return customUrl;
	}

	public void setCustomUrl(String customUrl) {
		this.customUrl = customUrl;
	}

	public String getTelphoneNum() {
		return telphoneNum;
	}

	public void setTelphoneNum(String telphoneNum) {
		this.telphoneNum = telphoneNum;
	}

	public String getBarcodeUrl() {
		return barcodeUrl;
	}

	public void setBarcodeUrl(String barcodeUrl) {
		this.barcodeUrl = barcodeUrl;
	}

	public String getAppAndroidDownloadUrl() {
		return appAndroidDownloadUrl;
	}

	public void setAppAndroidDownloadUrl(String appAndroidDownloadUrl) {
		this.appAndroidDownloadUrl = appAndroidDownloadUrl;
	}

	public String getAppIosBarcodeUrl() {
		return appIosBarcodeUrl;
	}

	public void setAppIosBarcodeUrl(String appIosBarcodeUrl) {
		this.appIosBarcodeUrl = appIosBarcodeUrl;
	}

	public String getAppStoreIosUrl() {
		return appStoreIosUrl;
	}

	public void setAppStoreIosUrl(String appStoreIosUrl) {
		this.appStoreIosUrl = appStoreIosUrl;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getUpdateAdmin() {
		return updateAdmin;
	}

	public void setUpdateAdmin(String updateAdmin) {
		this.updateAdmin = updateAdmin == null ? null : updateAdmin.trim();
	}

	public String getAdvantageDesc() {
		return advantageDesc;
	}

	public void setAdvantageDesc(String advantageDesc) {
		this.advantageDesc = advantageDesc;
	}

	public String getHotLotteryKinds() {
		return hotLotteryKinds;
	}

	public void setHotLotteryKinds(String hotLotteryKinds) {
		this.hotLotteryKinds = hotLotteryKinds;
	}

	public String getIndexMobileDefaultLotteryKinds() {
		return indexMobileDefaultLotteryKinds;
	}

	public void setIndexMobileDefaultLotteryKinds(String indexMobileDefaultLotteryKinds) {
		this.indexMobileDefaultLotteryKinds = indexMobileDefaultLotteryKinds;
	}

	public String getHeadLogoUrl() {
		return headLogoUrl;
	}

	public void setHeadLogoUrl(String headLogoUrl) {
		this.headLogoUrl = headLogoUrl;
	}

	public String getMobileHeadLogoUrl() {
		return mobileHeadLogoUrl;
	}

	public void setMobileHeadLogoUrl(String mobileHeadLogoUrl) {
		this.mobileHeadLogoUrl = mobileHeadLogoUrl == null ? null : mobileHeadLogoUrl.trim();
	}

	public String getMobileLogoUrl() {
		return mobileLogoUrl;
	}

	public void setMobileLogoUrl(String mobileLogoUrl) {
		this.mobileLogoUrl = mobileLogoUrl;
	}

	public String getAppBarcodeUrl() {
		return appBarcodeUrl;
	}

	public void setAppBarcodeUrl(String appBarcodeUrl) {
		this.appBarcodeUrl = appBarcodeUrl;
	}

	public String getMobileBarcodeUrl() {
		return mobileBarcodeUrl;
	}

	public void setMobileBarcodeUrl(String mobileBarcodeUrl) {
		this.mobileBarcodeUrl = mobileBarcodeUrl;
	}

	public Integer getHasApp() {
		return hasApp;
	}

	public void setHasApp(Integer hasApp) {
		this.hasApp = hasApp;
	}

	public String getFooter() {
		return footer;
	}

	public void setFooter(String footer) {
		this.footer = footer;
	}

	public BigDecimal getRechargeSecond() {
		return rechargeSecond;
	}

	public void setRechargeSecond(BigDecimal rechargeSecond) {
		this.rechargeSecond = rechargeSecond;
	}

	public BigDecimal getWithdrawSecond() {
		return withdrawSecond;
	}

	public void setWithdrawSecond(BigDecimal withdrawSecond) {
		this.withdrawSecond = withdrawSecond;
	}

	public String getSiteScript() {
		return siteScript;
	}

	public void setSiteScript(String siteScript) {
		this.siteScript = siteScript;
	}

	public String getMobileSiteScript() {
		return mobileSiteScript;
	}

	public void setMobileSiteScript(String mobileSiteScript) {
		this.mobileSiteScript = mobileSiteScript;
	}

	/**
	 * 首页彩种名
	 * 
	 * @return
	 */
	public String getIndexLotteryKindName() {
		if (StringUtils.isNoneBlank(this.getIndexLotteryKind())) {
			return ELotteryKind.valueOf(this.getIndexLotteryKind()).getDescription();
		}
		return "";
	}

	/**
	 * 首页默认彩种名
	 * 
	 * @return
	 */
	public String getIndexDefaultLotteryKindsName() {
		if (StringUtils.isNoneBlank(this.getIndexLotteryKind())) {
			String[] lotteryKinds = this.getIndexDefaultLotteryKinds().split(",");
			StringBuffer lotteryKindsName = new StringBuffer();

			for (String lotteryKind : lotteryKinds) {
				lotteryKindsName.append("," + ELotteryKind.valueOf(lotteryKind).getDescription());
			}

			if (lotteryKindsName.length() > 0) {
				return lotteryKindsName.substring(1);
			}
		}
		return "";
	}

	/**
	 * 模板名称
	 * 
	 * @return
	 */
	public String getTemplateName() {
		if (this.templateType != null && !"".equals(this.templateType)) {
			return ETemplateType.valueOf(this.templateType).getDescription();
		}
		return "";
	}

	/**
	 * 重新设置图片url地址,添加前面域名信息
	 */
	public void resetImageUrl() {
		if (StringUtils.isNoneBlank(this.headLogoUrl)) {
			this.headLogoUrl = SystemConfigConstant.imgServerUrl + this.headLogoUrl;
		}
		if (StringUtils.isNoneBlank(this.logoUrl)) {
			this.logoUrl = SystemConfigConstant.imgServerUrl + this.logoUrl;
		}
		if (StringUtils.isNoneBlank(this.mobileLogoUrl)) {
			this.mobileLogoUrl = SystemConfigConstant.imgServerUrl + this.mobileLogoUrl;
		}
		if (StringUtils.isNoneBlank(this.mobileHeadLogoUrl)) {
			this.mobileHeadLogoUrl = SystemConfigConstant.imgServerUrl + this.mobileHeadLogoUrl;
		}
		if (StringUtils.isNoneBlank(this.barcodeUrl)) {
			this.barcodeUrl = SystemConfigConstant.imgServerUrl + this.barcodeUrl;
		}
		if (StringUtils.isNoneBlank(this.mobileBarcodeUrl)) {
			this.mobileBarcodeUrl = SystemConfigConstant.imgServerUrl + this.mobileBarcodeUrl;
		}
		if (StringUtils.isNoneBlank(this.appDownLogo)) {
			this.appDownLogo = SystemConfigConstant.imgServerUrl + this.appDownLogo;
		}
		if (StringUtils.isNoneBlank(this.appBarcodeUrl)) {
			this.appBarcodeUrl = SystemConfigConstant.imgServerUrl + this.appBarcodeUrl;
		}
		if (StringUtils.isNoneBlank(this.appIosBarcodeUrl)) {
			this.appIosBarcodeUrl = SystemConfigConstant.imgServerUrl + this.appIosBarcodeUrl;
		}
		if (StringUtils.isNoneBlank(this.faviconUrl)) {
			this.faviconUrl = SystemConfigConstant.imgServerUrl + this.faviconUrl;
		}
	}
}