package com.team.lottery.vo;

import java.util.Date;

public class LhcIssueConfig {
    private Long id;

    private Date lastEndTime;

    private String lastExpect;

    private Date nextEndTime;

    private String nextExpect;

    private Integer isClose;

    private String lotteryName;

    private Date updateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getLastEndTime() {
        return lastEndTime;
    }

    public void setLastEndTime(Date lastEndTime) {
        this.lastEndTime = lastEndTime;
    }

    public String getLastExpect() {
        return lastExpect;
    }

    public void setLastExpect(String lastExpect) {
        this.lastExpect = lastExpect == null ? null : lastExpect.trim();
    }

    public Date getNextEndTime() {
        return nextEndTime;
    }

    public void setNextEndTime(Date nextEndTime) {
        this.nextEndTime = nextEndTime;
    }

    public String getNextExpect() {
        return nextExpect;
    }

    public void setNextExpect(String nextExpect) {
        this.nextExpect = nextExpect == null ? null : nextExpect.trim();
    }

    public Integer getIsClose() {
        return isClose;
    }

    public void setIsClose(Integer isClose) {
        this.isClose = isClose;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getLotteryName() {
        return lotteryName;
    }

    public void setLotteryName(String lotteryName) {
        this.lotteryName = lotteryName;
    }
}