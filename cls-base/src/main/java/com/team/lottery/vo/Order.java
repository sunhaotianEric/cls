package com.team.lottery.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.enums.ELotteryModel;
import com.team.lottery.enums.EOrderStatus;
import com.team.lottery.enums.EProstateStatus;
import com.team.lottery.util.DateUtils;
import com.team.lottery.util.StringUtils;

public class Order {
    private Long id;
    
    /**
     * 彩票方案
     */
    private String lotteryId;

    /**
     * 期号
     */
    private String expect;
    
    /**
     * 追号期数倍数
     */
    private Integer multiple;

    /**
     * 彩种类型
     */
    private String lotteryType;

    /**
     * 彩种类型描述
     */
    private String lotteryTypeDes;

    /**
     * 支出金额
     */
    private BigDecimal payMoney;

    /**
     * 支出积分
     */
    private BigDecimal payPoint;
    
    /**
     * 业务系统
     */
    private String bizSystem;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 用户名
     */
    private String userName;
    
    /**
     * 用户推荐人的关系
     */
    private String regfrom;  
    
    /**
     * 订单的来源  PC还是MOBILE
     */
    private String fromType;

    /**
     * 奖金模式
     */
    private BigDecimal awardModel;
    
    /**
     * 当时用户投注的模式
     */
    private BigDecimal currentUserModel;

    /**
     * 投注模式 元角分等
     */
    private String lotteryModel;

    /**
     * 投注数目
     */
    private Integer cathecticCount;

    /**
     * 每注价格
     */
    private BigDecimal cathecticUnitPrice;

    /**
     * 投注号码
     */
    private String codes;
    
    /**
     * 玩法描述
     */
    private String detailDes;

    /**
     * 开奖号码
     */
    private String openCode;

    /**
     * 订单状态
     */
    private String status;

    /**
     * 中奖状态
     */
    private String prostate;

    /**
     * 获奖信息
     */
    private String winInfo;

    /**
     * 奖金等级
     */
    private Integer winLevel;

    /**
     * 获奖金额
     */
    private BigDecimal winCost;
    
    /**
     * 
     */
    private String winCostStr;

    /**
     * 是否追号 0未追号 1追号
     */
    private Integer afterNumber;  //是否追号
    
    /**
     * 是否中奖以后停止追号  0中奖以后继续追号  1中奖以后停止追号
     */
    private Integer stopAfterNumber; //是否中奖以后停止追号
    
    /**
     * 备注
     */
    private String remark;
    
    /**
     * 截止时间
     */
    private Date closeDate;

    /**
     * 创建时间
     */
    private Date createdDate;

    /**
     * 更新时间
     */
    private Date updateDate;

    /**
     * 是否有效记录
     */
    private Integer enabled;
    
    
    
    private List<Map<String, String>> codesList; //号码和玩法的映射
    //追号总价格 扩展属性
    private BigDecimal afterNumberPayMoney;
    //追号总期数 扩展属性
    private Integer afterNumberCount;
    //追号中奖金额  扩展属性
    private BigDecimal winCostTotal;
    //已追期数
    private Integer yetAfterNumber;
    //已追金额
    private BigDecimal yetAfterCost;
    //追号状态
    private Integer afterStatus;
    
    //订单详情需要用到的数据（下）
    //数据封装对象
    private List<Map<String, Object>> orderCodesMap = new ArrayList<Map<String,Object>>();
    //订单详情需要用到的数据（上）
    
    /**
	 * 订单拷贝，用于统计用户团队有效投注总额
	 */
	public Order copy(String toUserName){
		Order order = new Order();
		if(!StringUtils.isEmpty(toUserName)){
			order.setUserName(toUserName);
			order.setPayMoney(payMoney);;
			order.setWinCost(winCost);
			order.setStatus(status);
			order.setProstate(prostate);
		}
		return order;
	}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLotteryId() {
        return lotteryId;
    }

    public void setLotteryId(String lotteryId) {
        this.lotteryId = lotteryId == null ? null : lotteryId.trim();
    }

    public String getExpect() {
        return expect;
    }

    public void setExpect(String expect) {
        this.expect = expect == null ? null : expect.trim();
    }

    public String getLotteryType() {
        return lotteryType;
    }

    public void setLotteryType(String lotteryType) {
        this.lotteryType = lotteryType == null ? null : lotteryType.trim();
    }

    public String getLotteryTypeDes() {
        return lotteryTypeDes;
    }

    public void setLotteryTypeDes(String lotteryTypeDes) {
        this.lotteryTypeDes = lotteryTypeDes == null ? null : lotteryTypeDes.trim();
    }

    public BigDecimal getPayMoney() {
        return payMoney;
    }

    public void setPayMoney(BigDecimal payMoney) {
        this.payMoney = payMoney;
    }

    public BigDecimal getPayPoint() {
        return payPoint;
    }

    public void setPayPoint(BigDecimal payPoint) {
        this.payPoint = payPoint;
    }
    
    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public BigDecimal getAwardModel() {
        return awardModel;
    }

    public void setAwardModel(BigDecimal awardModel) {
        this.awardModel = awardModel;
    }

    public String getLotteryModel() {
        return lotteryModel;
    }

    public void setLotteryModel(String lotteryModel) {
        this.lotteryModel = lotteryModel == null ? null : lotteryModel.trim();
    }

    public BigDecimal getCurrentUserModel() {
		return currentUserModel;
	}

	public void setCurrentUserModel(BigDecimal currentUserModel) {
		this.currentUserModel = currentUserModel;
	}

	public Integer getCathecticCount() {
        return cathecticCount;
    }

    public void setCathecticCount(Integer cathecticCount) {
        this.cathecticCount = cathecticCount;
    }

    public BigDecimal getCathecticUnitPrice() {
        return cathecticUnitPrice;
    }

    public void setCathecticUnitPrice(BigDecimal cathecticUnitPrice) {
        this.cathecticUnitPrice = cathecticUnitPrice;
    }

    public String getCodes() {
        return codes;
    }

    public void setCodes(String codes) {
        this.codes = codes == null ? null : codes.trim();
    }
    
    public String getDetailDes() {
		return detailDes;
	}

	public void setDetailDes(String detailDes) {
		this.detailDes = detailDes;
	}

	public String getOpenCode() {
        return openCode;
    }

    public void setOpenCode(String openCode) {
        this.openCode = openCode == null ? null : openCode.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getProstate() {
        return prostate;
    }

    public void setProstate(String prostate) {
        this.prostate = prostate == null ? null : prostate.trim();
    }

    public String getWinInfo() {
        return winInfo;
    }

    public void setWinInfo(String winInfo) {
        this.winInfo = winInfo == null ? null : winInfo.trim();
    }

    public BigDecimal getWinCost() {
        return winCost;
    }

    public void setWinCost(BigDecimal winCost) {
        this.winCost = winCost;
    }

    public Integer getStopAfterNumber() {
		return stopAfterNumber;
	}

	public void setStopAfterNumber(Integer stopAfterNumber) {
		this.stopAfterNumber = stopAfterNumber;
	}

	public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }
    public Integer getWinLevel() {
		return winLevel;
	}

	public void setWinLevel(Integer winLevel) {
		this.winLevel = winLevel;
	}

	public Integer getAfterNumber() {
		return afterNumber;
	}

	public void setAfterNumber(Integer afterNumber) {
		this.afterNumber = afterNumber;
	}
	
	public List<Map<String, String>> getCodesList() {
		return codesList;
	}

	public void setCodesList(List<Map<String, String>> codesList) {
		this.codesList = codesList;
	}

	public String getWinCostStr() {
		return winCostStr;
	}

	public void setWinCostStr(String winCostStr) {
		this.winCostStr = winCostStr;
	}
	
	public Integer getMultiple() {
		return multiple;
	}

	public void setMultiple(Integer multiple) {
		this.multiple = multiple;
	}
	
	public BigDecimal getAfterNumberPayMoney() {
		return afterNumberPayMoney;
	}

	public void setAfterNumberPayMoney(BigDecimal afterNumberPayMoney) {
		this.afterNumberPayMoney = afterNumberPayMoney;
	}
	
	public Integer getAfterNumberCount() {
		return afterNumberCount;
	}

	public void setAfterNumberCount(Integer afterNumberCount) {
		this.afterNumberCount = afterNumberCount;
	}

	public Date getCloseDate() {
		return closeDate;
	}

	public void setCloseDate(Date closeDate) {
		this.closeDate = closeDate;
	}

	public BigDecimal getWinCostTotal() {
		return winCostTotal;
	}

	public void setWinCostTotal(BigDecimal winCostTotal) {
		this.winCostTotal = winCostTotal;
	}

	public Integer getYetAfterNumber() {
		return yetAfterNumber;
	}

	public void setYetAfterNumber(Integer yetAfterNumber) {
		this.yetAfterNumber = yetAfterNumber;
	}

	public BigDecimal getYetAfterCost() {
		return yetAfterCost;
	}

	public void setYetAfterCost(BigDecimal yetAfterCost) {
		this.yetAfterCost = yetAfterCost;
	}

	public Integer getAfterStatus() {
		return afterStatus;
	}

	public void setAfterStatus(Integer afterStatus) {
		this.afterStatus = afterStatus;
	}
	
	public String getRegfrom() {
		return regfrom;
	}

	public void setRegfrom(String regfrom) {
		this.regfrom = regfrom;
	}
	
	public String getFromType() {
		return fromType;
	}

	public void setFromType(String fromType) {
		this.fromType = fromType;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}



	/**
     * 获取业务系统中文名称
     * @return
     */
	  public String getBizSystemName() {
	    	
	        return this.bizSystem==null?"":ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
	   }
	
	/**
	 * 投注模式描述
	 * @return
	 */
	public String getLotteryModelStr(){
		if(!StringUtils.isEmpty(this.getLotteryModel())){
			return ELotteryModel.valueOf(this.getLotteryModel()).getDescription();
		}
		return "";
	}
	
	//获取订单状态的描述
	public String getStatusStr(){
	    if(!StringUtils.isEmpty(this.getStatus())){
	    	return EOrderStatus.valueOf(this.getStatus()).getDescription();
	    }
		return "";
	}
	
	//获取中奖状态的描述
	public String getProstateStatusStr(){
		if(!StringUtils.isEmpty(this.getProstate())){
			return EProstateStatus.valueOf(this.getProstate()).getDescription();
		}
		return "";
	}
	
	//获取时间字符串
	public String getCreatedDateStr(){
		if(this.getCreatedDate() != null){
	    	return DateUtils.getDateToStrByFormat(this.getCreatedDate(), "yyyy/MM/dd HH:mm:ss");
		}
		return "";
	}

	public List<Map<String, Object>> getOrderCodesMap() {
		return orderCodesMap;
	}

	public void setOrderCodesMap(List<Map<String, Object>> orderCodesMap) {
		this.orderCodesMap = orderCodesMap;
	}
}