package com.team.lottery.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.team.lottery.cache.ClsCacheManager;

public class UserSelfDayConsume {
    private Long id;

    private String bizSystem;

    private Integer userId;

    private String userName;

    private String regfrom;

    private BigDecimal money;

    private BigDecimal gain;

    private Date belongDate;

    private Date createDate;

    private Long version;
    
    private BigDecimal recharge;

    private BigDecimal rechargepresent;

    private BigDecimal activitiesmoney;

    private BigDecimal systemaddmoney;

    private BigDecimal systemreducemoney;
    
    private BigDecimal managereducemoney;

	private BigDecimal withdraw;

    private BigDecimal withdrawfee;

    private BigDecimal lottery;
    
    private BigDecimal lotteryEffective;

    private BigDecimal lotteryRegression;

    private BigDecimal lotteryStopAfterNumber;

    private BigDecimal win;

    private BigDecimal rebate;

    private BigDecimal percentage;

    private BigDecimal paytranfer;

    private BigDecimal incometranfer;

    private BigDecimal halfmonthbonus;

    private BigDecimal daybonus;

    private BigDecimal monthsalary;

    private BigDecimal daysalary;

    private BigDecimal daycommission;

    private BigDecimal other;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getRegfrom() {
        return regfrom;
    }

    public void setRegfrom(String regfrom) {
        this.regfrom = regfrom == null ? null : regfrom.trim();
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public BigDecimal getGain() {
        return gain;
    }

    public void setGain(BigDecimal gain) {
        this.gain = gain;
    }
    public BigDecimal getManagereducemoney() {
 		return managereducemoney;
 	}

 	public void setManagereducemoney(BigDecimal managereducemoney) {
 		this.managereducemoney = managereducemoney;
 	}
    public Date getBelongDate() {
        return belongDate;
    }

    public void setBelongDate(Date belongDate) {
        this.belongDate = belongDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public BigDecimal getRecharge() {
        return recharge;
    }

    public void setRecharge(BigDecimal recharge) {
        this.recharge = recharge;
    }

    public BigDecimal getRechargepresent() {
        return rechargepresent;
    }

    public void setRechargepresent(BigDecimal rechargepresent) {
        this.rechargepresent = rechargepresent;
    }

    public BigDecimal getActivitiesmoney() {
        return activitiesmoney;
    }

    public void setActivitiesmoney(BigDecimal activitiesmoney) {
        this.activitiesmoney = activitiesmoney;
    }

    public BigDecimal getSystemaddmoney() {
        return systemaddmoney;
    }

    public void setSystemaddmoney(BigDecimal systemaddmoney) {
        this.systemaddmoney = systemaddmoney;
    }

    public BigDecimal getSystemreducemoney() {
        return systemreducemoney;
    }

    public void setSystemreducemoney(BigDecimal systemreducemoney) {
        this.systemreducemoney = systemreducemoney;
    }

    public BigDecimal getWithdraw() {
        return withdraw;
    }

    public void setWithdraw(BigDecimal withdraw) {
        this.withdraw = withdraw;
    }

    public BigDecimal getWithdrawfee() {
        return withdrawfee;
    }

    public void setWithdrawfee(BigDecimal withdrawfee) {
        this.withdrawfee = withdrawfee;
    }

    public BigDecimal getLottery() {
        return lottery;
    }

    public void setLottery(BigDecimal lottery) {
        this.lottery = lottery;
    }

    public BigDecimal getLotteryEffective() {
		return lotteryEffective;
	}

	public void setLotteryEffective(BigDecimal lotteryEffective) {
		this.lotteryEffective = lotteryEffective;
	}

	public BigDecimal getLotteryRegression() {
		return lotteryRegression;
	}

	public void setLotteryRegression(BigDecimal lotteryRegression) {
		this.lotteryRegression = lotteryRegression;
	}

	public BigDecimal getLotteryStopAfterNumber() {
		return lotteryStopAfterNumber;
	}

	public void setLotteryStopAfterNumber(BigDecimal lotteryStopAfterNumber) {
		this.lotteryStopAfterNumber = lotteryStopAfterNumber;
	}

	public BigDecimal getWin() {
        return win;
    }

    public void setWin(BigDecimal win) {
        this.win = win;
    }

    public BigDecimal getRebate() {
        return rebate;
    }

    public void setRebate(BigDecimal rebate) {
        this.rebate = rebate;
    }

    public BigDecimal getPercentage() {
        return percentage;
    }

    public void setPercentage(BigDecimal percentage) {
        this.percentage = percentage;
    }

    public BigDecimal getPaytranfer() {
        return paytranfer;
    }

    public void setPaytranfer(BigDecimal paytranfer) {
        this.paytranfer = paytranfer;
    }

    public BigDecimal getIncometranfer() {
        return incometranfer;
    }

    public void setIncometranfer(BigDecimal incometranfer) {
        this.incometranfer = incometranfer;
    }

    public BigDecimal getHalfmonthbonus() {
        return halfmonthbonus;
    }

    public void setHalfmonthbonus(BigDecimal halfmonthbonus) {
        this.halfmonthbonus = halfmonthbonus;
    }

    public BigDecimal getDaybonus() {
        return daybonus;
    }

    public void setDaybonus(BigDecimal daybonus) {
        this.daybonus = daybonus;
    }

    public BigDecimal getMonthsalary() {
        return monthsalary;
    }

    public void setMonthsalary(BigDecimal monthsalary) {
        this.monthsalary = monthsalary;
    }

    public BigDecimal getDaysalary() {
        return daysalary;
    }

    public void setDaysalary(BigDecimal daysalary) {
        this.daysalary = daysalary;
    }

    public BigDecimal getDaycommission() {
        return daycommission;
    }

    public void setDaycommission(BigDecimal daycommission) {
        this.daycommission = daycommission;
    }

    public BigDecimal getOther() {
        return other;
    }

    public void setOther(BigDecimal other) {
        this.other = other;
    }
    public String getBizSystemName() {
        return this.bizSystem==null?"":ClsCacheManager.getBizSystemNameByCode(this.bizSystem);
    }
}