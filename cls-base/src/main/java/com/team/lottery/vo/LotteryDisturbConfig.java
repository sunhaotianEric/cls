package com.team.lottery.vo;

import java.math.BigDecimal;

import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.util.StringUtils;

public class LotteryDisturbConfig {
	
	//下注显示干扰
	public static final int DISTURB_SHOW = 1;
	//下注彩种改变
	public static final int DISTURB_CHANGE = 2;
	//下注提示干扰
	public static final int DISTURB_TIP = 3;
	
    private Long id;

    private Integer disturbModel;

    private Integer userId;

    private String userName;

    private BigDecimal money;

    private String lotteryType;

    private String expectsets;

    private String disturbLotteryType;

    private String disturbTip;

    private Integer enable;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getDisturbModel() {
        return disturbModel;
    }

    public void setDisturbModel(Integer disturbModel) {
        this.disturbModel = disturbModel;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getLotteryType() {
        return lotteryType;
    }

    public void setLotteryType(String lotteryType) {
        this.lotteryType = lotteryType == null ? null : lotteryType.trim();
    }

    public String getExpectsets() {
        return expectsets;
    }

    public void setExpectsets(String expectsets) {
        this.expectsets = expectsets == null ? null : expectsets.trim();
    }

    public String getDisturbLotteryType() {
        return disturbLotteryType;
    }

    public void setDisturbLotteryType(String disturbLotteryType) {
        this.disturbLotteryType = disturbLotteryType == null ? null : disturbLotteryType.trim();
    }

    public String getDisturbTip() {
        return disturbTip;
    }

    public void setDisturbTip(String disturbTip) {
        this.disturbTip = disturbTip == null ? null : disturbTip.trim();
    }

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }
    
	public String getLotteryTypeStr(){
		if(!StringUtils.isEmpty(this.getLotteryType())){
			return ELotteryKind.valueOf(this.getLotteryType()).getDescription();
		}
		return "";
	}

	public String getDisturbLotteryTypeStr(){
		if(!StringUtils.isEmpty(this.getDisturbLotteryType())){
			return ELotteryKind.valueOf(this.getDisturbLotteryType()).getDescription();
		}
		return "";
	}
}