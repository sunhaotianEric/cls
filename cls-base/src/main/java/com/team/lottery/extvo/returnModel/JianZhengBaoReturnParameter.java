package com.team.lottery.extvo.returnModel;

public class JianZhengBaoReturnParameter {
	
	private String merchant_order_no;
	
	private String status;
	
	private String msg;
	
	private String amount;
	
	private String prd_ord_no;
	
	private String sign;
	
	private String pay_channel;

	public String getMerchant_order_no() {
		return merchant_order_no;
	}

	public void setMerchant_order_no(String merchant_order_no) {
		this.merchant_order_no = merchant_order_no;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getPrd_ord_no() {
		return prd_ord_no;
	}

	public void setPrd_ord_no(String prd_ord_no) {
		this.prd_ord_no = prd_ord_no;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getPay_channel() {
		return pay_channel;
	}

	public void setPay_channel(String pay_channel) {
		this.pay_channel = pay_channel;
	}
	
	
	
}
