package com.team.lottery.extvo;

public class RechargeWithDrawInfoExportVO {

	private String bizSystem;//增加业务系统字段
	private Boolean serialNumber;
	private Boolean userName;
	private Boolean applyValue;
	private Boolean accountValue;
	private Boolean createdDateStr;
	private Boolean statusStr;
	private Boolean operateDes;
	private Boolean refTypeStr;
	private Boolean fromType;
	private Boolean postscript;
	private Boolean bankUserName;
	private Boolean updateAdmin;
	private Boolean updateDateStr;
	private Boolean feeValue;
	private Boolean bankName;
	private Boolean subbranchName;
	private Boolean bankId;
	private Boolean bankCardNum;
	private Boolean bankAccountName;
	public String getBizSystem() {
		return bizSystem;
	}
	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}
	public Boolean getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(Boolean serialNumber) {
		this.serialNumber = serialNumber;
	}
	public Boolean getUserName() {
		return userName;
	}
	public void setUserName(Boolean userName) {
		this.userName = userName;
	}
	public Boolean getApplyValue() {
		return applyValue;
	}
	public void setApplyValue(Boolean applyValue) {
		this.applyValue = applyValue;
	}
	public Boolean getAccountValue() {
		return accountValue;
	}
	public void setAccountValue(Boolean accountValue) {
		this.accountValue = accountValue;
	}
	public Boolean getCreatedDateStr() {
		return createdDateStr;
	}
	public void setCreatedDateStr(Boolean createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	public Boolean getStatusStr() {
		return statusStr;
	}
	public void setStatusStr(Boolean statusStr) {
		this.statusStr = statusStr;
	}
	public Boolean getOperateDes() {
		return operateDes;
	}
	public void setOperateDes(Boolean operateDes) {
		this.operateDes = operateDes;
	}
	public Boolean getRefTypeStr() {
		return refTypeStr;
	}
	public void setRefTypeStr(Boolean refTypeStr) {
		this.refTypeStr = refTypeStr;
	}
	public Boolean getFromType() {
		return fromType;
	}
	public void setFromType(Boolean fromType) {
		this.fromType = fromType;
	}
	public Boolean getPostscript() {
		return postscript;
	}
	public void setPostscript(Boolean postscript) {
		this.postscript = postscript;
	}
	public Boolean getBankUserName() {
		return bankUserName;
	}
	public void setBankUserName(Boolean bankUserName) {
		this.bankUserName = bankUserName;
	}
	public Boolean getUpdateAdmin() {
		return updateAdmin;
	}
	public void setUpdateAdmin(Boolean updateAdmin) {
		this.updateAdmin = updateAdmin;
	}
	public Boolean getUpdateDateStr() {
		return updateDateStr;
	}
	public void setUpdateDateStr(Boolean updateDateStr) {
		this.updateDateStr = updateDateStr;
	}
	public Boolean getFeeValue() {
		return feeValue;
	}
	public void setFeeValue(Boolean feeValue) {
		this.feeValue = feeValue;
	}
	public Boolean getBankName() {
		return bankName;
	}
	public void setBankName(Boolean bankName) {
		this.bankName = bankName;
	}
	public Boolean getSubbranchName() {
		return subbranchName;
	}
	public void setSubbranchName(Boolean subbranchName) {
		this.subbranchName = subbranchName;
	}
	public Boolean getBankId() {
		return bankId;
	}
	public void setBankId(Boolean bankId) {
		this.bankId = bankId;
	}
	public Boolean getBankCardNum() {
		return bankCardNum;
	}
	public void setBankCardNum(Boolean bankCardNum) {
		this.bankCardNum = bankCardNum;
	}
	public Boolean getBankAccountName() {
		return bankAccountName;
	}
	public void setBankAccountName(Boolean bankAccountName) {
		this.bankAccountName = bankAccountName;
	}
}
