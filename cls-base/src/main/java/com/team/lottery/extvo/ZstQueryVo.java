package com.team.lottery.extvo;

import com.team.lottery.enums.ELotteryKind;

/**
 * 走势图查询vo
 * @author chenhsh
 *
 */
public class ZstQueryVo {

	private ELotteryKind lotteryKind; //彩种
	private Integer nearExpectNums; //近几期数目  
	private Integer extendNums = 0; //扩展期数
	private String bizSystem;

	public ELotteryKind getLotteryKind() {
		return lotteryKind;
	}

	public void setLotteryKind(ELotteryKind lotteryKind) {
		this.lotteryKind = lotteryKind;
	}

	public Integer getNearExpectNums() {
		return nearExpectNums + extendNums;
	}
	
	//返回真实的期号数目
	public Integer getNearExpectNumsReal(){
		return nearExpectNums;
	}

	//多查询10期
	public void setNearExpectNums(Integer nearExpectNums) {
		this.nearExpectNums = nearExpectNums;
	}

	public Integer getExtendNums() {
		return extendNums;
	}

	public void setExtendNums(Integer extendNums) {
		this.extendNums = extendNums;
	}

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}
}
