package com.team.lottery.extvo;

public class AdminUseVO {
	
	private Integer adminId;
	private Boolean used;
	
	public AdminUseVO(Integer adminId, Boolean used) {
		super();
		this.adminId = adminId;
		this.used = used;
	}
	
	public Integer getAdminId() {
		return adminId;
	}
	public void setAdminId(Integer adminId) {
		this.adminId = adminId;
	}
	public Boolean getUsed() {
		return used;
	}
	public void setUsed(Boolean used) {
		this.used = used;
	}

	
}
