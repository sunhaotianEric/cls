package com.team.lottery.extvo;


/**
 * 奖金配置查询vo
 * @author chenhsh
 *
 */
public class LotteryWinQuery {

    private String lotteryType;

	public String getLotteryType() {
		return lotteryType;
	}

	public void setLotteryType(String lotteryType) {
		this.lotteryType = lotteryType;
	}
    
}
