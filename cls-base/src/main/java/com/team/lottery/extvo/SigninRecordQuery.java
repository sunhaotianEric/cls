package com.team.lottery.extvo;

import java.util.Date;

public class SigninRecordQuery {
    private Integer id;

    private String bizSystem;

    private String userName;

    private Integer userId;

    private Date signinTimeStart;
    
    private Date signinTimeEnd;
    
    private Integer continueSigninDay;
    
    // 签到记录时间归属查询字段..
    private String signDateStr;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

	public Date getSigninTimeStart() {
		return signinTimeStart;
	}

	public void setSigninTimeStart(Date signinTimeStart) {
		this.signinTimeStart = signinTimeStart;
	}

	public Date getSigninTimeEnd() {
		return signinTimeEnd;
	}

	public void setSigninTimeEnd(Date signinTimeEnd) {
		this.signinTimeEnd = signinTimeEnd;
	}

	public Integer getContinueSigninDay() {
		return continueSigninDay;
	}

	public void setContinueSigninDay(Integer continueSigninDay) {
		this.continueSigninDay = continueSigninDay;
	}

	public String getSignDateStr() {
		return signDateStr;
	}

	public void setSignDateStr(String signDateStr) {
		this.signDateStr = signDateStr;
	}
	

}