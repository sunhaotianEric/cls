package com.team.lottery.extvo;

import java.util.Date;


/**
 * 奖金期号查询vo
 * @author chenhsh
 *
 */
public class LotteryCodeXyQuery {

	private String bizSystem;
	
    private String lotteryName;
    
    private String lotteryNum;
    
    private Date startime;
    
    private Date endtime;

	public String getLotteryName() {
		return lotteryName;
	}

	public void setLotteryName(String lotteryName) {
		this.lotteryName = lotteryName;
	}

	public String getLotteryNum() {
		return lotteryNum;
	}

	public void setLotteryNum(String lotteryNum) {
		this.lotteryNum = lotteryNum;
	}
	public Date getStartime() {
		return startime;
	}

	public void setStartime(Date startime) {
		this.startime = startime;
	}

	public Date getEndtime() {
		return endtime;
	}

	public void setEndtime(Date endtime) {
		this.endtime = endtime;
	}

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}
}
