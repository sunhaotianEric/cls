package com.team.lottery.extvo;

public class WeixinUserQuery {
	  private Long weixinId;
	  private String openId;  
	  
	  
	public Long getWeixinId() {
		return weixinId;
	}
	public void setWeixinId(Long weixinId) {
		this.weixinId = weixinId;
	}
	public String getOpenId() {
		return openId;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}

	  
	  
}
