package com.team.lottery.extvo;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

public class LhcOrderReportVo {

	//玩法
	private String kindPlay;
	//玩法描述
	private String kindPlayDes;
	private String code;
	private String codeDes;
	private BigDecimal lotteryMoney = BigDecimal.ZERO;
	private BigDecimal winMoney = BigDecimal.ZERO;
	private Set<String> userNameSet = new HashSet<String>();
	private int num;
	
	
	public String getKindPlay() {
		return kindPlay;
	}
	public void setKindPlay(String kindPlay) {
		this.kindPlay = kindPlay;
	}
	public String getKindPlayDes() {
		return kindPlayDes;
	}
	public void setKindPlayDes(String kindPlayDes) {
		this.kindPlayDes = kindPlayDes;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getCodeDes() {
		return codeDes;
	}
	public void setCodeDes(String codeDes) {
		this.codeDes = codeDes;
	}
	public BigDecimal getLotteryMoney() {
		return lotteryMoney;
	}
	public void setLotteryMoney(BigDecimal lotteryMoney) {
		this.lotteryMoney = lotteryMoney;
	}
	public BigDecimal getWinMoney() {
		return winMoney;
	}
	public void setWinMoney(BigDecimal winMoney) {
		this.winMoney = winMoney;
	}
	public int getNum() {
		//从用户集合中获取
		return this.userNameSet.size();
	}
	public void setNum(int num) {
		this.num = num;
	}
	public Set<String> getUserNameSet() {
		return userNameSet;
	}
	public void setUserNameSet(Set<String> userNameSet) {
		this.userNameSet = userNameSet;
	}
	
	public void addUserName(String userName) {
		this.userNameSet.add(userName);
	}
	
	public void addLotteryMoney(BigDecimal money) {
		this.lotteryMoney = this.lotteryMoney.add(money);
	}
	
	public void addWinMoney(BigDecimal money) {
		this.winMoney = this.winMoney.add(money);
	}
}
