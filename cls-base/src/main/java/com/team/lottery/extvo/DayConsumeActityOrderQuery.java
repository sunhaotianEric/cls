package com.team.lottery.extvo;

import java.util.Date;

public class DayConsumeActityOrderQuery {
	
	public Integer userId;
	private String userName;
	private String bizSystem;
	private String serialNumber;
	private String releaseStatus;
	private Date belongDate;
    private Date belongDateStart;
    private Date belongDateEnd;
    
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getBizSystem() {
		return bizSystem;
	}
	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getReleaseStatus() {
		return releaseStatus;
	}
	public void setReleaseStatus(String releaseStatus) {
		this.releaseStatus = releaseStatus;
	}
	public Date getBelongDate() {
		return belongDate;
	}
	public void setBelongDate(Date belongDate) {
		this.belongDate = belongDate;
	}
	public Date getBelongDateStart() {
		return belongDateStart;
	}
	public void setBelongDateStart(Date belongDateStart) {
		this.belongDateStart = belongDateStart;
	}
	public Date getBelongDateEnd() {
		return belongDateEnd;
	}
	public void setBelongDateEnd(Date belongDateEnd) {
		this.belongDateEnd = belongDateEnd;
	}
    
}
