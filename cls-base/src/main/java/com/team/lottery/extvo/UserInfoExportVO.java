package com.team.lottery.extvo;

public class UserInfoExportVO {

	private String bizSystem;//增加业务系统字段
	private String userName;
	private Boolean hasTruename;
	private Boolean hasAddress;
	private Boolean hasPhone;
	private Boolean hasQq;
	private Boolean hasEmail;
	private Boolean hasIdentityid;
	private Boolean hasAddtime;
	
	public String getBizSystem() {
		return bizSystem;
	}
	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Boolean getHasTruename() {
		return hasTruename;
	}
	public void setHasTruename(Boolean hasTruename) {
		this.hasTruename = hasTruename;
	}
	public Boolean getHasAddress() {
		return hasAddress;
	}
	public void setHasAddress(Boolean hasAddress) {
		this.hasAddress = hasAddress;
	}
	public Boolean getHasPhone() {
		return hasPhone;
	}
	public void setHasPhone(Boolean hasPhone) {
		this.hasPhone = hasPhone;
	}
	public Boolean getHasQq() {
		return hasQq;
	}
	public void setHasQq(Boolean hasQq) {
		this.hasQq = hasQq;
	}
	public Boolean getHasEmail() {
		return hasEmail;
	}
	public void setHasEmail(Boolean hasEmail) {
		this.hasEmail = hasEmail;
	}
	public Boolean getHasIdentityid() {
		return hasIdentityid;
	}
	public void setHasIdentityid(Boolean hasIdentityid) {
		this.hasIdentityid = hasIdentityid;
	}
	public Boolean getHasAddtime() {
		return hasAddtime;
	}
	public void setHasAddtime(Boolean hasAddtime) {
		this.hasAddtime = hasAddtime;
	}
	
}
