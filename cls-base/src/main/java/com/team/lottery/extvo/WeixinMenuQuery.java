package com.team.lottery.extvo;

public class WeixinMenuQuery {


    private Long weixinId;
    private String weixinName;
    private Integer menuLevel;
    private String name;
    private String type;
    private Long parentMenuId;
    private String parentMenuName;
    
	public Long getWeixinId() {
		return weixinId;
	}
	public void setWeixinId(Long weixinId) {
		this.weixinId = weixinId;
	}
	public String getWeixinName() {
		return weixinName;
	}
	public void setWeixinName(String weixinName) {
		this.weixinName = weixinName;
	}
	public Integer getMenuLevel() {
		return menuLevel;
	}
	public void setMenuLevel(Integer menuLevel) {
		this.menuLevel = menuLevel;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Long getParentMenuId() {
		return parentMenuId;
	}
	public void setParentMenuId(Long parentMenuId) {
		this.parentMenuId = parentMenuId;
	}
	public String getParentMenuName() {
		return parentMenuName;
	}
	public void setParentMenuName(String parentMenuName) {
		this.parentMenuName = parentMenuName;
	}
    
    
}
