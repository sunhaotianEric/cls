package com.team.lottery.extvo;

import java.util.Date;

import com.team.lottery.util.DateUtil;

public class PointLogQuery {
	//积分类型
    private String detailType;
    //创建时间起始时间
    private Date createdDateStart;
    //结束时间结束时间
    private Date createdDateEnd;
    //方案号码
    private String lotteryId;
    //用户名
    private String userName;
    
	public String getDetailType() {
		return detailType;
	}
	public void setDetailType(String detailType) {
		this.detailType = detailType;
	}
	public Date getCreatedDateStart() {
		if(this.createdDateStart != null){
			return DateUtil.getNowStartTimeByStart(this.createdDateStart);
		}
		return createdDateStart;
	}
	public void setCreatedDateStart(Date createdDateStart) {
		this.createdDateStart = createdDateStart;
	}
	public Date getCreatedDateEnd() {
		if(this.createdDateEnd != null){
			return DateUtil.getNowStartTimeByEnd(this.createdDateEnd);
		}
		return createdDateEnd;
	}
	public void setCreatedDateEnd(Date createdDateEnd) {
		this.createdDateEnd = createdDateEnd;
	}
	public String getLotteryId() {
		return lotteryId;
	}
	public void setLotteryId(String lotteryId) {
		this.lotteryId = lotteryId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
}
