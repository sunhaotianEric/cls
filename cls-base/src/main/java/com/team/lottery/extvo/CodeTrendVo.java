package com.team.lottery.extvo;


/**
 * 走势图页面显示数据
 * @author Administrator
 *
 */
public class CodeTrendVo {
	//序号
	Long id;
	//期号
	String lotteryNum;
	//期号显示
	String issue;
	//开奖号码
	String openCode;
	//开奖号码列表
	String[] openCodeArr;
	//开奖号、玩法等列
	String[] playkindResult;
	//走势数据
	String[] trendData;
	public String getIssue() {
		return issue;
	}
	public void setIssue(String issue) {
		this.issue = issue;
	}
	public String getOpenCode() {
		return openCode;
	}
	public void setOpenCode(String openCode) {
		this.openCode = openCode;
	}
	public String[] getPlaykindResult() {
		return playkindResult;
	}
	public void setPlaykindResult(String[] playkindResult) {
		this.playkindResult = playkindResult;
	}
	public String[] getTrendData() {
		return trendData;
	}
	public void setTrendData(String[] trendData) {
		this.trendData = trendData;
	}
	public String[] getOpenCodeArr() {
		return openCodeArr;
	}
	public void setOpenCodeArr(String[] openCodeArr) {
		this.openCodeArr = openCodeArr;
	}
	public String getLotteryNum() {
		return lotteryNum;
	}
	public void setLotteryNum(String lotteryNum) {
		this.lotteryNum = lotteryNum;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
}
