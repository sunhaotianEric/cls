package com.team.lottery.extvo;

import java.math.BigDecimal;

public class LotteryDisturbConfigQuery {

	private Integer disturbModel;
	private String userName;
	private BigDecimal money;
	private String lotteryType;
	private String disturbLotteryType;
	private String expect;
	private Integer enable;

	public Integer getDisturbModel() {
		return disturbModel;
	}

	public void setDisturbModel(Integer disturbModel) {
		this.disturbModel = disturbModel;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName == null ? null : userName.trim();
	}

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public String getLotteryType() {
		return lotteryType;
	}

	public void setLotteryType(String lotteryType) {
		this.lotteryType = lotteryType == null ? null : lotteryType.trim();
	}

	public String getDisturbLotteryType() {
		return disturbLotteryType;
	}

	public void setDisturbLotteryType(String disturbLotteryType) {
		this.disturbLotteryType = disturbLotteryType == null ? null
				: disturbLotteryType.trim();
	}
	
	public String getExpect() {
		return expect;
	}

	public void setExpect(String expect) {
		this.expect = expect;
	}

	public Integer getEnable() {
		return enable;
	}

	public void setEnable(Integer enable) {
		this.enable = enable;
	}
}