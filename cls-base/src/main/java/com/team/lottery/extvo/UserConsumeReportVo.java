package com.team.lottery.extvo;

import java.math.BigDecimal;

import com.team.lottery.cache.ClsCacheManager;

/**
 * 消费报表vo
 * @author chenhsh
 *
 */
public class UserConsumeReportVo {

    private Integer userId;
    private String userName;
    private String bizSystem;
    private String regfrom;  //用户推荐人的关系
    private String dailiLevel; //用户代理类型
    private BigDecimal money = BigDecimal.ZERO; //用户余额
    private BigDecimal recharge = BigDecimal.ZERO; //存款总额
    private BigDecimal rechargePresent = BigDecimal.ZERO; //充值赠送总额
    private BigDecimal activitiesMoney = BigDecimal.ZERO; //活动彩金赠送总额
   // private BigDecimal netRecharge = BigDecimal.ZERO; //总共的网银汇款
  //  private BigDecimal quickRecharge = BigDecimal.ZERO; //总共的快捷充值
    private BigDecimal withDraw = BigDecimal.ZERO; //取款总额
    private BigDecimal withDrawFee = BigDecimal.ZERO; //取款总额
    private BigDecimal extend = BigDecimal.ZERO; //使用的推广金额
  //  private BigDecimal totalExtend = BigDecimal.ZERO; //总共的推广金额
    private BigDecimal register = BigDecimal.ZERO; //注册总额;
    private BigDecimal lottery = BigDecimal.ZERO;  //总共投注额 -撤单
   // private BigDecimal shopPoint = BigDecimal.ZERO; //购买积分额
    private BigDecimal win = BigDecimal.ZERO; //中奖金额
    private BigDecimal rebate = BigDecimal.ZERO; //返点金额
    private BigDecimal percentage = BigDecimal.ZERO; //提成金额
    private BigDecimal payTranfer = BigDecimal.ZERO; //
    private BigDecimal incomeTranfer = BigDecimal.ZERO; //提成金额
    private BigDecimal halfMonthBonus = BigDecimal.ZERO;  //半月分红
    private BigDecimal dayBonus = BigDecimal.ZERO;  //日分红
    private BigDecimal monthSalary = BigDecimal.ZERO;	 //月工资 
    private BigDecimal daySalary = BigDecimal.ZERO;		 //日工资 
    private BigDecimal dayCommission = BigDecimal.ZERO;	 //日佣金 
    private BigDecimal systemaddmoney = BigDecimal.ZERO;//系统续费
    private BigDecimal systemreducemoney = BigDecimal.ZERO;//系统扣费
    private Integer addUser;
    private Integer addFirstMember;
    private BigDecimal gain = BigDecimal.ZERO; //(中奖+返点+提成+分红(日分红、月分红、日工资、月工资、日佣金)+注册赠送+收入转账  - 投注 - 购买积分 - 推广 - 支出转账)
    private Integer regCount;

    private Integer firstRechargeCount;

    private Integer lotteryCount;
    
    private  String bizSystemName;
    
    public UserConsumeReportVo(){
    }
    
    public UserConsumeReportVo(String userName){
    	this.userName = userName;
    }
    
	public Integer getRegCount() {
		return regCount;
	}

	public void setRegCount(Integer regCount) {
		this.regCount = regCount;
	}

	public Integer getFirstRechargeCount() {
		return firstRechargeCount;
	}

	public void setFirstRechargeCount(Integer firstRechargeCount) {
		this.firstRechargeCount = firstRechargeCount;
	}

	public Integer getLotteryCount() {
		return lotteryCount;
	}

	public void setLotteryCount(Integer lotteryCount) {
		this.lotteryCount = lotteryCount;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRegfrom() {
		return regfrom;
	}

	public void setRegfrom(String regfrom) {
		this.regfrom = regfrom;
	}

	public BigDecimal getMoney() {
		return money;
	}

	public String getDailiLevel() {
		return dailiLevel;
	}

	public void setDailiLevel(String dailiLevel) {
		this.dailiLevel = dailiLevel;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public BigDecimal getRecharge() {
		return recharge;
	}

	public void setRecharge(BigDecimal recharge) {
		this.recharge = recharge;
	}

	public BigDecimal getWithDraw() {
		return withDraw;
	}

	public void setWithDraw(BigDecimal withDraw) {
		this.withDraw = withDraw;
	}
	
	public BigDecimal getWithDrawFee() {
		return withDrawFee;
	}

	public void setWithDrawFee(BigDecimal withDrawFee) {
		this.withDrawFee = withDrawFee;
	}

	public BigDecimal getLottery() {
		return lottery;
	}

	public void setLottery(BigDecimal lottery) {
		this.lottery = lottery;
	}

//	public BigDecimal getShopPoint() {
//		return shopPoint;
//	}
//
//	public void setShopPoint(BigDecimal shopPoint) {
//		this.shopPoint = shopPoint;
//	}

	public BigDecimal getWin() {
		return win;
	}

	public void setWin(BigDecimal win) {
		this.win = win;
	}

	public BigDecimal getRebate() {
		return rebate;
	}

	public void setRebate(BigDecimal rebate) {
		this.rebate = rebate;
	}

	public BigDecimal getPercentage() {
		return percentage;
	}

	public void setPercentage(BigDecimal percentage) {
		this.percentage = percentage;
	}

	

	public BigDecimal getHalfMonthBonus() {
		return halfMonthBonus;
	}

	public void setHalfMonthBonus(BigDecimal halfMonthBonus) {
		this.halfMonthBonus = halfMonthBonus;
	}

	public BigDecimal getDayBonus() {
		return dayBonus;
	}

	public void setDayBonus(BigDecimal dayBonus) {
		this.dayBonus = dayBonus;
	}

	public BigDecimal getMonthSalary() {
		return monthSalary;
	}

	public void setMonthSalary(BigDecimal monthSalary) {
		this.monthSalary = monthSalary;
	}

	public BigDecimal getDaySalary() {
		return daySalary;
	}

	public void setDaySalary(BigDecimal daySalary) {
		this.daySalary = daySalary;
	}

	public BigDecimal getDayCommission() {
		return dayCommission;
	}

	public void setDayCommission(BigDecimal dayCommission) {
		this.dayCommission = dayCommission;
	}

	public BigDecimal getGain() {
		return gain;
	}

	public void setGain(BigDecimal gain) {
		this.gain = gain;
	}

	public BigDecimal getExtend() {
		return extend;
	}

	public void setExtend(BigDecimal extend) {
		this.extend = extend;
	}

	public BigDecimal getRegister() {
		return register;
	}

	public void setRegister(BigDecimal register) {
		this.register = register;
	}

	public BigDecimal getPayTranfer() {
		return payTranfer;
	}

	public void setPayTranfer(BigDecimal payTranfer) {
		this.payTranfer = payTranfer;
	}

	public BigDecimal getIncomeTranfer() {
		return incomeTranfer;
	}

	public void setIncomeTranfer(BigDecimal incomeTranfer) {
		this.incomeTranfer = incomeTranfer;
	}

//	public BigDecimal getTotalExtend() {
//		return totalExtend;
//	}
//
//	public void setTotalExtend(BigDecimal totalExtend) {
//		this.totalExtend = totalExtend;
//	}

	public BigDecimal getRechargePresent() {
		return rechargePresent;
	}

	public void setRechargePresent(BigDecimal rechargePresent) {
		this.rechargePresent = rechargePresent;
	}

//	public BigDecimal getNetRecharge() {
//		return netRecharge;
//	}
//
//	public void setNetRecharge(BigDecimal netRecharge) {
//		this.netRecharge = netRecharge;
//	}
//
//	public BigDecimal getQuickRecharge() {
//		return quickRecharge;
//	}
//
//	public void setQuickRecharge(BigDecimal quickRecharge) {
//		this.quickRecharge = quickRecharge;
//	}

	public BigDecimal getActivitiesMoney() {
		return activitiesMoney;
	}

	public void setActivitiesMoney(BigDecimal activitiesMoney) {
		this.activitiesMoney = activitiesMoney;
	}
	
	public BigDecimal getSystemaddmoney() {
		return systemaddmoney;
	}

	public void setSystemaddmoney(BigDecimal systemaddmoney) {
		this.systemaddmoney = systemaddmoney;
	}

	public BigDecimal getSystemreducemoney() {
		return systemreducemoney;
	}

	public void setSystemreducemoney(BigDecimal systemreducemoney) {
		this.systemreducemoney = systemreducemoney;
	}
   
    public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}

	public void setBizSystemName(String bizSystemName) {
		this.bizSystemName = bizSystemName;
		
	}

	public Integer getAddUser() {
		return addUser;
	}

	public void setAddUser(Integer addUser) {
		this.addUser = addUser;
	}

	public Integer getAddFirstMember() {
		return addFirstMember;
	}

	public void setAddFirstMember(Integer addFirstMember) {
		this.addFirstMember = addFirstMember;
	}

	public String getBizSystemName() {
		bizSystemName=(this.bizSystem==null?"":ClsCacheManager.getBizSystemNameByCode(this.bizSystem));
		return bizSystemName;
	}
	
	/**
	 * 复制消费报表数据
	 */
	public void copyToConsumeReport(UserConsumeReportVo consumeReportVo){
		if(consumeReportVo != null && this.getUserName().equals(consumeReportVo.getUserName())){
	    	this.setRecharge(this.getRecharge().add(consumeReportVo.getRecharge()));
	    	this.setRechargePresent(this.getRechargePresent().add(consumeReportVo.getRechargePresent()));
	    	this.setActivitiesMoney(this.getActivitiesMoney().add(consumeReportVo.getActivitiesMoney()));  //活动彩金
	    	//this.setNetRecharge(this.getNetRecharge().add(consumeReportVo.getNetRecharge()));
	    	//this.setQuickRecharge(this.getQuickRecharge().add(consumeReportVo.getQuickRecharge()));
	    	this.setWithDraw(this.getWithDraw().add(consumeReportVo.getWithDraw()));
	    	this.setWithDrawFee(this.getWithDrawFee().add(consumeReportVo.getWithDrawFee()));
	    	this.setExtend(this.getExtend().add(consumeReportVo.getExtend()));
	    	this.setRegister(this.getRegister().add(consumeReportVo.getRegister()));
	    	this.setLottery(this.getLottery().add(consumeReportVo.getLottery()));
	    	this.setGain(this.getGain().add(consumeReportVo.getGain()));
	    	this.setWin(this.getWin().add(consumeReportVo.getWin()));
	    	this.setRebate(this.getRebate().add(consumeReportVo.getRebate()));
	    	//this.setShopPoint(this.getShopPoint().add(consumeReportVo.getShopPoint()));
	    	this.setPercentage(this.getPercentage().add(consumeReportVo.getPercentage()));
	    	this.setHalfMonthBonus(this.getHalfMonthBonus().add(consumeReportVo.getHalfMonthBonus()));
	    	this.setDayBonus(this.getDayBonus().add(consumeReportVo.getDayBonus()));
	    	this.setDayCommission(this.getDayCommission().add(consumeReportVo.getDayCommission()));
	    	this.setDaySalary(this.getDaySalary().add(consumeReportVo.getDaySalary()));
	    	this.setMonthSalary(this.getMonthSalary().add(consumeReportVo.getMonthSalary()));
	    	this.setPayTranfer(this.getPayTranfer().add(consumeReportVo.getPayTranfer()));
	    	this.setIncomeTranfer(this.getIncomeTranfer().add(consumeReportVo.getIncomeTranfer()));
	    	this.setSystemaddmoney(this.getSystemaddmoney().add(consumeReportVo.getSystemaddmoney()));
	    	this.setSystemreducemoney(this.getSystemreducemoney().add(consumeReportVo.getSystemreducemoney()));
		}
	}
	
}
