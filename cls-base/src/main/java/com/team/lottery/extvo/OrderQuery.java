package com.team.lottery.extvo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class OrderQuery {
	
	//查询自己
	public static final Integer SCOPE_OWN = 1;
	//查询直接下级
	public static final Integer SCOPE_SUB = 2;
	//查询所有下级
	public static final Integer SCOPE_ALL_SUB = 3;

	//彩种
    private String lotteryType;
    //投注时间起始时间
    private Date createdDateStart;
    //投注时间结束时间
    private Date createdDateEnd;
    //方案id
    private String lotteryId;
    //用户id
    private Integer userId;
    //用户名
    private String userName;
    //业务系统
    private String bizSystem;
    private List<String> bizSystemList;
    //期号
    private String expect;
    //订单排序类型
    private Integer orderSort;
    //中奖状态类型
    private String prostateStatus;
    //订单状态
    private String orderStatus;
    //是否追号
    private Integer afterNumber;  
    //是否中奖以后停止追号
    private Integer stopAfterNumber; 
    //中奖的金额    
    private BigDecimal winCost;
    //玩法  
    private String lotteryKind;
    //模式
    private String lotteryModel;
    //团队领导人名字
  	private String teamLeaderName; //包含推荐人的拼接 
  	private String realLeaderName; //真是的领导人名字
  	//订单来源
  	private String fromType; 
	//有效订单
  	private Integer enabled;
  	
    //扩展属性,指定是否查找code字段
    private Integer queryCode = 1; 
    
	private Boolean isQuerySub = false;  //是否同时查询下级用户
	private Integer queryScope = SCOPE_OWN;  //账户明细查询的范围
	//追号状态
    private Integer afterStatus;
	
	//彩种列表
    private List<String> lotteryTypeList;
    //彩种列表
    private String status;

	private Date updateDateStart;

	public Date getUpdateDateStart() {
		return updateDateStart;
	}

	public void setUpdateDateStart(Date updateDateStart) {
		this.updateDateStart = updateDateStart;
	}

	public Date getUpdateDateEnd() {
		return updateDateEnd;
	}

	public void setUpdateDateEnd(Date updateDateEnd) {
		this.updateDateEnd = updateDateEnd;
	}

	private Date updateDateEnd;
    
    private List<String> prostateList;
    
    private Integer dateRange; // 查询时间代号.(1表示今天,-1表示昨天,-7表示最近7天,30表示本月,-30表示上个月.)

	public List<String> getBizSystemList() {
		return bizSystemList;
	}
	public void setBizSystemList(List<String> bizSystemList) {
		this.bizSystemList = bizSystemList;
	}
	
	public Integer getEnabled() {
		return enabled;
	}
	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}
	public String getLotteryType() {
		return lotteryType;
	}
	public void setLotteryType(String lotteryType) {
		this.lotteryType = lotteryType;
	}
	public Date getCreatedDateStart() {
		/*if(this.createdDateStart != null){
			return DateUtil.getNowStartTimeByStart(this.createdDateStart);
		}*/
		return createdDateStart;
	}
	public void setCreatedDateStart(Date createdDateStart) {
		this.createdDateStart = createdDateStart;
	}
	public Date getCreatedDateEnd() {
		/*if(this.createdDateEnd != null){
			return DateUtil.getNowStartTimeByEnd(this.createdDateEnd);
		}*/
		return createdDateEnd;
	}
	public void setCreatedDateEnd(Date createdDateEnd) {
		this.createdDateEnd = createdDateEnd;
	}
	public String getLotteryId() {
		return lotteryId;
	}
	public void setLotteryId(String lotteryId) {
		this.lotteryId = lotteryId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getExpect() {
		return expect;
	}
	public void setExpect(String expect) {
		this.expect = expect;
	}
	public Integer getOrderSort() {
		return orderSort;
	}
	public void setOrderSort(Integer orderSort) {
		this.orderSort = orderSort;
	}
	public String getProstateStatus() {
		return prostateStatus;
	}
	public void setProstateStatus(String prostateStatus) {
		this.prostateStatus = prostateStatus;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public Integer getAfterNumber() {
		return afterNumber;
	}
	public void setAfterNumber(Integer afterNumber) {
		this.afterNumber = afterNumber;
	}
	public Integer getStopAfterNumber() {
		return stopAfterNumber;
	}
	public void setStopAfterNumber(Integer stopAfterNumber) {
		this.stopAfterNumber = stopAfterNumber;
	}
	public BigDecimal getWinCost() {
		return winCost;
	}
	public void setWinCost(BigDecimal winCost) {
		this.winCost = winCost;
	}
	public Integer getQueryCode() {
		return queryCode;
	}
	public void setQueryCode(Integer queryCode) {
		this.queryCode = queryCode;
	}
	public String getLotteryKind() {
		return lotteryKind;
	}
	public void setLotteryKind(String lotteryKind) {
		this.lotteryKind = lotteryKind;
	}
	public String getLotteryModel() {
		return lotteryModel;
	}
	public void setLotteryModel(String lotteryModel) {
		this.lotteryModel = lotteryModel;
	}
	public String getTeamLeaderName() {
		return teamLeaderName;
	}
	public void setTeamLeaderName(String teamLeaderName) {
		this.teamLeaderName = teamLeaderName;
	}
	public Boolean getIsQuerySub() {
		return isQuerySub;
	}
	public void setIsQuerySub(Boolean isQuerySub) {
		this.isQuerySub = isQuerySub;
	}
	public Integer getQueryScope() {
		return queryScope;
	}
	public void setQueryScope(Integer queryScope) {
		this.queryScope = queryScope;
	}
	public String getRealLeaderName() {
		return realLeaderName;
	}
	public void setRealLeaderName(String realLeaderName) {
		this.realLeaderName = realLeaderName;
	}
	public String getFromType() {
		return fromType;
	}
	public void setFromType(String fromType) {
		this.fromType = fromType;
	}
	public String getBizSystem() {
		return bizSystem;
	}
	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}
	public List<String> getLotteryTypeList() {
		return lotteryTypeList;
	}
	public void setLotteryTypeList(List<String> lotteryTypeList) {
		this.lotteryTypeList = lotteryTypeList;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<String> getProstateList() {
		return prostateList;
	}
	public void setProstateList(List<String> prostateList) {
		this.prostateList = prostateList;
	}
	public Integer getAfterStatus() {
		return afterStatus;
	}
	public void setAfterStatus(Integer afterStatus) {
		this.afterStatus = afterStatus;
	}
	public Integer getDateRange() {
		return dateRange;
	}
	public void setDateRange(Integer dageRange) {
		this.dateRange = dageRange;
	}
}
