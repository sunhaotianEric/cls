package com.team.lottery.extvo;

import com.team.lottery.enums.EAfterNumberType;
import com.team.lottery.enums.ELotteryKind;

/**
 * 追号信息
 * @author chenhsh
 *
 */
public class LotteryOrderExpect {
	
	private EAfterNumberType afterNumberType; //追号类型
	private String expect;  //期号
	private Integer multiple; //每一期的倍数
	private ELotteryKind lotteryKind;  //投注类型
	
	//最新期号
	private String lastExpect;
	
	public String getExpect() {
		return expect;
	}
	public void setExpect(String expect) {
		this.expect = expect;
	}
	public Integer getMultiple() {
		return multiple;
	}
	public void setMultiple(Integer multiple) {
		this.multiple = multiple;
	}
	public EAfterNumberType getAfterNumberType() {
		return afterNumberType;
	}
	public void setAfterNumberType(EAfterNumberType afterNumberType) {
		this.afterNumberType = afterNumberType;
	}
	
	public ELotteryKind getLotteryKind() {
		return lotteryKind;
	}
	public void setLotteryKind(ELotteryKind lotteryKind) {
		this.lotteryKind = lotteryKind;
	}
	
	public String getLastExpect() {
		return lastExpect;
	}
	public void setLastExpect(String lastExpect) {
		this.lastExpect = lastExpect;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((expect == null) ? 0 : expect.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LotteryOrderExpect other = (LotteryOrderExpect) obj;
		if (expect == null) {
			if (other.expect != null)
				return false;
		} else if (!expect.equals(other.expect))
			return false;
		return true;
	}
}
