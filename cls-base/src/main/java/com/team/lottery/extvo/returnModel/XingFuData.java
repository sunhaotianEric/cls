package com.team.lottery.extvo.returnModel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "message")
@XmlAccessorType(XmlAccessType.FIELD)
public class XingFuData {
    //版本号
	 @XmlElement(name="detail")
	private XingFuDetailData detail;
	//字符集
	 @XmlElement(name="sign")
	private String sign;
	 
	public XingFuDetailData getDetail() {
		return detail;
	}
	public void setDetail(XingFuDetailData detail) {
		this.detail = detail;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	
	 
}
