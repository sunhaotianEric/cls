package com.team.lottery.extvo;

import java.util.Date;

import com.team.lottery.util.DateUtil;

/**
 * 充值取现记录查询vo
 * @author chenhsh
 *
 */
public class RechargeWithDrawQuery {

    private String serialNumber;
    private Integer adminId;
    private String payType;
    private String operateType;
	private String userName;
	private Integer userId;
	private String statuss;
    private Date createdDateStart;
    private Date createdDateEnd;
	private String thirdPayType;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getStatuss() {
		return statuss;
	}
	public void setStatuss(String statuss) {
		this.statuss = statuss;
	}
	public Date getCreatedDateStart() {
		if(this.createdDateStart != null){
			return DateUtil.getNowStartTimeByStart(this.createdDateStart);
		}
		return createdDateStart;
	}
	public void setCreatedDateStart(Date createdDateStart) {
		this.createdDateStart = createdDateStart;
	}
	public Date getCreatedDateEnd() {
		if(this.createdDateEnd != null){
			return DateUtil.getNowStartTimeByEnd(this.createdDateEnd);
		}
		return createdDateEnd;
	}
	public void setCreatedDateEnd(Date createdDateEnd) {
		this.createdDateEnd = createdDateEnd;
	}
	public Integer getAdminId() {
		return adminId;
	}
	public void setAdminId(Integer adminId) {
		this.adminId = adminId;
	}
	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}
	public String getOperateType() {
		return operateType;
	}
	public void setOperateType(String operateType) {
		this.operateType = operateType;
	}
	public String getThirdPayType() {
		return thirdPayType;
	}
	public void setThirdPayType(String thirdPayType) {
		this.thirdPayType = thirdPayType;
	}
	
}
