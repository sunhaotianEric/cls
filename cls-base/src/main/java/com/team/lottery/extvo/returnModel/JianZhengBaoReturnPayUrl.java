package com.team.lottery.extvo.returnModel;

public class JianZhengBaoReturnPayUrl {
	private String code;
	
	private String message;
	
	private JianZhengBaoData body;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public JianZhengBaoData getBody() {
		return body;
	}

	public void setBody(JianZhengBaoData body) {
		this.body = body;
	}
	
	
	
}
