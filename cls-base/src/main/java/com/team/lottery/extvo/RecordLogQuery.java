package com.team.lottery.extvo;

import java.util.Date;

public class RecordLogQuery {
	
	private String sourceName;
    private String targetName;
    private String modelName;
    private Date logtimeStart;
    private Date logtimeEnd;
    private String logType;
    private String bizSystem;
    
    
    
    public String getSourceName() {
		return sourceName;
	}
	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}
	public String getTargetName() {
		return targetName;
	}
	public void setTargetName(String targetName) {
		this.targetName = targetName;
	}
	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	
	public Date getLogtimeStart() {
		return logtimeStart;
	}
	public void setLogtimeStart(Date logtimeStart) {
		this.logtimeStart = logtimeStart;
	}
	public Date getLogtimeEnd() {
		return logtimeEnd;
	}
	public void setLogtimeEnd(Date logtimeEnd) {
		this.logtimeEnd = logtimeEnd;
	}
	public String getLogType() {
		return logType;
	}
	public void setLogType(String logType) {
		this.logType = logType;
	}
	public String getBizSystem() {
		return bizSystem;
	}
	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}
    
}
