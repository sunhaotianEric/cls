package com.team.lottery.extvo;

import java.util.Date;

/**
 * 新手礼包查询对象
 * 
 * @author QaQ
 *
 */
public class NewUserGiftTaskQuery {
	//系统名称
	private String bizSystem;
	//用户名
	private String userName;
	//开始时间
	private Date starTime;
	//结束时间
	private Date endTime;
	
	public String getBizSystem() {
		return bizSystem;
	}
	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Date getStarTime() {
		return starTime;
	}
	public void setStarTime(Date starTime) {
		this.starTime = starTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	

}
