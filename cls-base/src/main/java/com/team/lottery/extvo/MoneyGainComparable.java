package com.team.lottery.extvo;

import java.util.Comparator;

import com.team.lottery.vo.DayProfit;
/**
 * 
 * 盈亏报表，资金盈利大小比较
 *
 */
public class MoneyGainComparable  implements Comparator<DayProfit>{

	@Override
	public int compare(DayProfit d1, DayProfit d2) {		
		return d1.getMoneyGain().compareTo(d2.getMoneyGain());
	}

}
