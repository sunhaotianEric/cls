package com.team.lottery.extvo;

import java.util.Date;

import com.team.lottery.enums.ENoteType;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.StringUtils;


/**
 * 站内信息查询vo
 * @author jxxu
 *
 */
public class NotesQuery {
    
    //业务系统
    private String bizSystem;
    //起始时间
    private Date createdDateStart;
    //结束时间
    private Date createdDateEnd;
    //信息类型
    private String type;
    //信息状态
    private String status;
	//我发出的
    private String fromUserName;
    //我发出的UserId
    private Integer fromUserId;
    //发给我的
    private String toUserName;
    //发给我的UserId
    private Integer toUserId;
    //父亲id
    private Long parentId;
    
    private Long noteId;
    
    //是否限制条数
    private Integer isLimit;
    
    

	public Integer getToUserId() {
		return toUserId;
	}
	public void setToUserId(Integer toUserId) {
		this.toUserId = toUserId;
	}
	public Integer getFromUserId() {
		return fromUserId;
	}
	public void setFromUserId(Integer fromUserId) {
		this.fromUserId = fromUserId;
	}
	public String getBizSystem() {
		return bizSystem;
	}
	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}
	public String getFromUserName() {
		return fromUserName;
	}
	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}
	public String getToUserName() {
		return toUserName;
	}
	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
	}
	public Date getCreatedDateStart() {
		if(this.createdDateStart != null){
			return DateUtil.getNowStartTimeByStart(this.createdDateStart);
		}
		return createdDateStart;
	}
	public void setCreatedDateStart(Date createdDateStart) {
		this.createdDateStart = createdDateStart;
	}
	public Date getCreatedDateEnd() {
		if(this.createdDateEnd != null){
			return DateUtil.getNowStartTimeByEnd(this.createdDateEnd);
		}
		return createdDateEnd;
	}
	public void setCreatedDateEnd(Date createdDateEnd) {
		this.createdDateEnd = createdDateEnd;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	public Integer getIsLimit() {
		return isLimit;
	}
	public void setIsLimit(Integer isLimit) {
		this.isLimit = isLimit;
	}
	
	public Long getNoteId() {
		return noteId;
	}
	public void setNoteId(Long noteId) {
		this.noteId = noteId;
	}
	/**
	 * 获得消息类型的描述
	 */
	public String getTypeDes(){
		if(!StringUtils.isEmpty(this.type)){
			return ENoteType.valueOf(this.type).getDescription();
		}
		return "";
	}
	
}
