package com.team.lottery.extvo;

import java.util.HashMap;

import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;

public class LotteryCodeKsZstVo {


    //开奖期号
    private String lotteryNum;

    private String numInfo1;

    private String numInfo2;

    private String numInfo3;
    /**
     * 号码分布
     */
    private HashMap<String, Integer> numMateCurrent;
    
    /**
     * 和值
     */
    private HashMap<String, Integer> hzNumMateCurrent ;
    
    /**
     * 和值形态
     */
    private HashMap<String, Integer> hzxtNumMateCurrent;
    
    /**
     * 号码形态
     */
    private HashMap<String, Integer> hmxtNumMateCurrent;
    
    
    public String getLotteryNum() {
        return lotteryNum;
    }

    public void setLotteryNum(String lotteryNum) {
        this.lotteryNum = lotteryNum == null ? null : lotteryNum.trim();
    }

    public String getNumInfo1() {
        return numInfo1;
    }

    public void setNumInfo1(String numInfo1) {
        this.numInfo1 = numInfo1 == null ? null : numInfo1.trim();
    }

    public String getNumInfo2() {
        return numInfo2;
    }

    public void setNumInfo2(String numInfo2) {
        this.numInfo2 = numInfo2 == null ? null : numInfo2.trim();
    }

    public String getNumInfo3() {
        return numInfo3;
    }

    public void setNumInfo3(String numInfo3) {
        this.numInfo3 = numInfo3 == null ? null : numInfo3.trim();
    }

	
	public HashMap<String, Integer> getNumMateCurrent() {
		return numMateCurrent;
	}

	public void setNumMateCurrent(HashMap<String, Integer> numMateCurrent) {
		this.numMateCurrent = numMateCurrent;
	}

	public HashMap<String, Integer> getHzNumMateCurrent() {
		return hzNumMateCurrent;
	}

	public void setHzNumMateCurrent(HashMap<String, Integer> hzNumMateCurrent) {
		this.hzNumMateCurrent = hzNumMateCurrent;
	}

	public HashMap<String, Integer> getHzxtNumMateCurrent() {
		return hzxtNumMateCurrent;
	}

	public void setHzxtNumMateCurrent(HashMap<String, Integer> hzxtNumMateCurrent) {
		this.hzxtNumMateCurrent = hzxtNumMateCurrent;
	}

	public HashMap<String, Integer> getHmxtNumMateCurrent() {
		return hmxtNumMateCurrent;
	}

	public void setHmxtNumMateCurrent(HashMap<String, Integer> hmxtNumMateCurrent) {
		this.hmxtNumMateCurrent = hmxtNumMateCurrent;
	}

	/**
	 * 获取开奖号码的拼接
	 * @return
	 */
	public String getOpencodeStr() {
		StringBuffer sb = new StringBuffer();
		if(!StringUtils.isEmpty(numInfo1)) {
			sb.append(numInfo1.trim()).append(ConstantUtil.OPENCODE_PLIT);
		}
		if(!StringUtils.isEmpty(numInfo2)) {
			sb.append(numInfo2.trim()).append(ConstantUtil.OPENCODE_PLIT);
		}
		if(!StringUtils.isEmpty(numInfo3)) {
			sb.append(numInfo3.trim()).append(ConstantUtil.OPENCODE_PLIT);
		}
		
		if(sb.length() != 0){
			sb.deleteCharAt(sb.length() - 1);
		}
		
		//删除最后一个分隔符
		return sb.toString();
	}
	
	
}