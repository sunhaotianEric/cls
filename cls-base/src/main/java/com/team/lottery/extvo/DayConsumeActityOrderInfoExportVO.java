package com.team.lottery.extvo;

public class DayConsumeActityOrderInfoExportVO {

	private String bizSystem;//增加业务系统字段
	private Boolean serialNumber;
	private Boolean userName;
	private Boolean money;
	private Boolean lotteryMoney;
	private Boolean ffcLotteryMoney;
	private Boolean ffcMoney;
	private Boolean belongDateStr;
	private Boolean releaseStatus;
	private Boolean ffcReleaseStatus;
	private Boolean appliyDateStr;
	public String getBizSystem() {
		return bizSystem;
	}
	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}
	public Boolean getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(Boolean serialNumber) {
		this.serialNumber = serialNumber;
	}
	public Boolean getUserName() {
		return userName;
	}
	public void setUserName(Boolean userName) {
		this.userName = userName;
	}
	public Boolean getMoney() {
		return money;
	}
	public void setMoney(Boolean money) {
		this.money = money;
	}
	public Boolean getLotteryMoney() {
		return lotteryMoney;
	}
	public void setLotteryMoney(Boolean lotteryMoney) {
		this.lotteryMoney = lotteryMoney;
	}
	public Boolean getFfcLotteryMoney() {
		return ffcLotteryMoney;
	}
	public void setFfcLotteryMoney(Boolean ffcLotteryMoney) {
		this.ffcLotteryMoney = ffcLotteryMoney;
	}
	public Boolean getFfcMoney() {
		return ffcMoney;
	}
	public void setFfcMoney(Boolean ffcMoney) {
		this.ffcMoney = ffcMoney;
	}
	public Boolean getBelongDateStr() {
		return belongDateStr;
	}
	public void setBelongDateStr(Boolean belongDateStr) {
		this.belongDateStr = belongDateStr;
	}
	public Boolean getReleaseStatus() {
		return releaseStatus;
	}
	public void setReleaseStatus(Boolean releaseStatus) {
		this.releaseStatus = releaseStatus;
	}
	public Boolean getFfcReleaseStatus() {
		return ffcReleaseStatus;
	}
	public void setFfcReleaseStatus(Boolean ffcReleaseStatus) {
		this.ffcReleaseStatus = ffcReleaseStatus;
	}
	public Boolean getAppliyDateStr() {
		return appliyDateStr;
	}
	public void setAppliyDateStr(Boolean appliyDateStr) {
		this.appliyDateStr = appliyDateStr;
	}
	
	
	
}
