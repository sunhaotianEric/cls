package com.team.lottery.extvo;


public class AdminDomainQuery {

    private String bizSystem;

    private String domains;

    private String allowIps;

    
    public String getBizSystem() {
		return bizSystem;
	}


	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}


	public String getDomains() {
		return domains;
	}


	public void setDomains(String domains) {
		this.domains = domains;
	}


	public String getAllowIps() {
		return allowIps;
	}


	public void setAllowIps(String allowIps) {
		this.allowIps = allowIps;
	}
}