package com.team.lottery.extvo.returnModel;

/**
 * 金阳支付返回参数
 * 
 * @author
 *
 */
public class JinYangReturnParam {
	private String partner;

	private String ordernumber;

	private String orderstatus;

	private String paymoney;

	private String sysnumber;

	private String attach;

	private String sign;

	public String getPartner() {
		return partner;
	}

	public void setPartner(String partner) {
		this.partner = partner;
	}

	public String getOrdernumber() {
		return ordernumber;
	}

	public void setOrdernumber(String ordernumber) {
		this.ordernumber = ordernumber;
	}

	public String getOrderstatus() {
		return orderstatus;
	}

	public void setOrderstatus(String orderstatus) {
		this.orderstatus = orderstatus;
	}

	public String getPaymoney() {
		return paymoney;
	}

	public void setPaymoney(String paymoney) {
		this.paymoney = paymoney;
	}

	public String getSysnumber() {
		return sysnumber;
	}

	public void setSysnumber(String sysnumber) {
		this.sysnumber = sysnumber;
	}

	public String getAttach() {
		return attach;
	}

	public void setAttach(String attach) {
		this.attach = attach;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}
	
	@Override
	public String toString() {
		return "金阳支付回调参数打印 [partner=" + partner + ", ordernumber=" + ordernumber + ", orderstatus=" + orderstatus + ", paymoney=" + paymoney + ", sysnumber="
				+ sysnumber + ", attach=" + attach + ", sign="
				+ sign + "]";
	}

}
