package com.team.lottery.extvo;

import java.util.Comparator;

import com.team.lottery.vo.DayProfit;
/**
 * 
 * 盈亏报表，赢率大小比较
 *
 */
public class WinGainComparable  implements Comparator<DayProfit>{

	@Override
	public int compare(DayProfit d1, DayProfit d2) {		
		return d1.getWinGain().compareTo(d2.getWinGain());
	}

}
