package com.team.lottery.extvo.returnModel;
/**
 * 嘟嘟嘟支付返回对象
 * @author Jamine
 *
 */
public class DuDuPayReturnParam {
	//商户ID
	private Integer merchant_id;
	//订单
	private String  order_id;
	//支付金额
	private String amount;
	//支付方式
	private Integer pay_method;
	//交易签名
	private String sign;
	public Integer getMerchant_id() {
		return merchant_id;
	}
	public void setMerchant_id(Integer merchant_id) {
		this.merchant_id = merchant_id;
	}
	public String getOrder_id() {
		return order_id;
	}
	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public Integer getPay_method() {
		return pay_method;
	}
	public void setPay_method(Integer pay_method) {
		this.pay_method = pay_method;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	
}
