package com.team.lottery.extvo;

/**
 * 快捷支付信息查询vo
 * @author chengf
 *
 */
public class ChargePayVo {
    
	
	private String bizSystem;
    
    private String chargeType;
    
    private String memberId;
    
    public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}

	public String getChargeType() {
		return chargeType;
	}

	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
    
	
}
