package com.team.lottery.extvo.returnModel;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "xml")
public class XinYuTokenData {
    //token
	private String token;
	//有效期
	private String token_expir_second;
	
	@XmlElement
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	@XmlElement
	public String getToken_expir_second() {
		return token_expir_second;
	}
	public void setToken_expir_second(String token_expir_second) {
		this.token_expir_second = token_expir_second;
	}
	
	
}
