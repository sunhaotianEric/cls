package com.team.lottery.extvo.returnModel;
/**
 * 凤梨回调返回对象
 * @author Jamine
 *
 */
public class FengLiPayReturnParam {
	
    private String charset;
	
	private String sign_type;
	
	private String status;
	
	private String custid;
	
	private String sign;
	
	private String transaction_id;
	
	private String out_trade_no;
	
	private Float total_fee;
	
	private String time_end;

	public String getCharset() {
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}

	public String getSign_type() {
		return sign_type;
	}

	public void setSign_type(String sign_type) {
		this.sign_type = sign_type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCustid() {
		return custid;
	}

	public void setCustid(String custid) {
		this.custid = custid;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getTransaction_id() {
		return transaction_id;
	}

	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}

	public String getOut_trade_no() {
		return out_trade_no;
	}

	public void setOut_trade_no(String out_trade_no) {
		this.out_trade_no = out_trade_no;
	}

	public Float getTotal_fee() {
		return total_fee;
	}

	public void setTotal_fee(Float total_fee) {
		this.total_fee = total_fee;
	}

	public String getTime_end() {
		return time_end;
	}

	public void setTime_end(String time_end) {
		this.time_end = time_end;
	}
	
	
}
