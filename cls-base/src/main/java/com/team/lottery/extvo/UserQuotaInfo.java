package com.team.lottery.extvo;

/**
 * 用户下级配额信息
 * @author gs
 *
 */
public class UserQuotaInfo {

	//同级模式
	private Integer equlaLevelNum;
	//下级模式
	private Integer lowerLevelNum;
	public Integer getEqulaLevelNum() {
		return equlaLevelNum;
	}
	public void setEqulaLevelNum(Integer equlaLevelNum) {
		this.equlaLevelNum = equlaLevelNum;
	}
	public Integer getLowerLevelNum() {
		return lowerLevelNum;
	}
	public void setLowerLevelNum(Integer lowerLevelNum) {
		this.lowerLevelNum = lowerLevelNum;
	}
	
	
}
