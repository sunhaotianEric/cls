package com.team.lottery.extvo.returnModel;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "detail")
public class XingFuDetailData {
	
 private String code;
 
 private String desc;
 
 private String qrCode;

 @XmlElement
public String getCode() {
	return code;
}

public void setCode(String code) {
	this.code = code;
}

@XmlElement
public String getDesc() {
	return desc;
}

public void setDesc(String desc) {
	this.desc = desc;
}

@XmlElement
public String getQrCode() {
	return qrCode;
}

public void setQrCode(String qrCode) {
	this.qrCode = qrCode;
}
 
 
 
	 
}
