package com.team.lottery.extvo;

public class AbstractOrderQuery {

	public static final String DESC = "desc";  //倒序排列
	public static final String ASC = "asc";   //正序排列
	
	private String orderyBy;  //排序字段
	private String order = DESC;  //排序大小
	private Boolean reversedOrder = false; //是否正序
	
	public String getOrderyBy() {
		return orderyBy;
	}
	public void setOrderyBy(String orderyBy) {
		this.orderyBy = orderyBy;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public Boolean getReversedOrder() {
		return reversedOrder;
	}
	public void setReversedOrder(Boolean reversedOrder) {
		this.reversedOrder = reversedOrder;
		if(reversedOrder) {
			order = ASC;
		}
	}
	
	
}
