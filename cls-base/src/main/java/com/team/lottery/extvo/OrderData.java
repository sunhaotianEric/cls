package com.team.lottery.extvo;

import java.math.BigDecimal;

/**
 * 订单数据
 * @author chenhsh
 *
 */
public class OrderData {
	
	//各种玩法对应的金额
	private BigDecimal currentNeedPayMoney;
	//总共的投注数目
	private Integer totalCathecticCount;
	//投注号码的拼接
	private StringBuffer codesSb;
	//玩法的拼接
	private StringBuffer detailDesSb;
	//可支付的金额
	private BigDecimal canPayMoney;
	
	public BigDecimal getCurrentNeedPayMoney() {
		return currentNeedPayMoney;
	}
	public void setCurrentNeedPayMoney(BigDecimal currentNeedPayMoney) {
		this.currentNeedPayMoney = currentNeedPayMoney;
	}
	public Integer getTotalCathecticCount() {
		return totalCathecticCount;
	}
	public void setTotalCathecticCount(Integer totalCathecticCount) {
		this.totalCathecticCount = totalCathecticCount;
	}
	public StringBuffer getCodesSb() {
		return codesSb;
	}
	public void setCodesSb(StringBuffer codesSb) {
		this.codesSb = codesSb;
	}
	public StringBuffer getDetailDesSb() {
		return detailDesSb;
	}
	public void setDetailDesSb(StringBuffer detailDesSb) {
		this.detailDesSb = detailDesSb;
	}
	public BigDecimal getCanPayMoney() {
		return canPayMoney;
	}
	public void setCanPayMoney(BigDecimal canPayMoney) {
		this.canPayMoney = canPayMoney;
	}
	
}
