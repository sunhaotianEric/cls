package com.team.lottery.extvo.returnModel;

/**
 * 交易成功通知处理通知.
 * 
 * @author Jamine
 *
 */
public class TianTianKuaiPayReturnParam {
	// 版本号.
	private String version;
	// 字符集.
	private String charset;
	// 签名方式.
	private String sign_type;
	// 返回状态码.
	private String status;
	// 返回信息.
	private String message;
	// 业务结果.
	private String result_code;
	// 商户号.
	private String merchant_id;
	// 随机字符串.
	private String nonce_str;
	// 签名.
	private String sign;

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getCharset() {
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}

	public String getSign_type() {
		return sign_type;
	}

	public void setSign_type(String sign_type) {
		this.sign_type = sign_type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getResult_code() {
		return result_code;
	}

	public void setResult_code(String result_code) {
		this.result_code = result_code;
	}

	public String getMerchant_id() {
		return merchant_id;
	}

	public void setMerchant_id(String merchant_id) {
		this.merchant_id = merchant_id;
	}

	public String getNonce_str() {
		return nonce_str;
	}

	public void setNonce_str(String nonce_str) {
		this.nonce_str = nonce_str;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

}
