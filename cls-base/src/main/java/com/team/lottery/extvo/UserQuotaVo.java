package com.team.lottery.extvo;

import java.util.List;

import com.team.lottery.vo.UserQuota;

public class UserQuotaVo {
    private Long id;
    
    /**
     * 业务系统
     */
    private String bizSystem;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 用户名
     */
    private String userName;

    private List<UserQuota> userQuotaList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

	public List<UserQuota> getUserQuotaList() {
		return userQuotaList;
	}

	public void setUserQuotaList(List<UserQuota> userQuotaList) {
		this.userQuotaList = userQuotaList;
	}


}