package com.team.lottery.extvo;

import java.math.BigDecimal;
import java.util.List;

/**
 * 发送站内信对象
 * @author gs
 *
 */
public class SendNoteQueryVo {

	private Integer sendType;
	private Integer starLevel;
	private String dailiLevel;
	//真正数据库要查询代理级别集合
	private List<String> dailiLevels;
	private BigDecimal sscrebate;
	private String userName;
	private Integer filterCondition;
	private String sub;
	private String body;
	private String bizSystem;
	
	
	public String getBizSystem() {
		return bizSystem;
	}
	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}
	public Integer getSendType() {
		return sendType;
	}
	public void setSendType(Integer sendType) {
		this.sendType = sendType;
	}
	public Integer getStarLevel() {
		return starLevel;
	}
	public void setStarLevel(Integer starLevel) {
		this.starLevel = starLevel;
	}
	public String getDailiLevel() {
		return dailiLevel;
	}
	public void setDailiLevel(String dailiLevel) {
		this.dailiLevel = dailiLevel;
	}
	
	public List<String> getDailiLevels() {
		return dailiLevels;
	}
	public void setDailiLevels(List<String> dailiLevels) {
		this.dailiLevels = dailiLevels;
	}
	public BigDecimal getSscrebate() {
		return sscrebate;
	}
	public void setSscrebate(BigDecimal sscrebate) {
		this.sscrebate = sscrebate;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Integer getFilterCondition() {
		return filterCondition;
	}
	public void setFilterCondition(Integer filterCondition) {
		this.filterCondition = filterCondition;
	}
	public String getSub() {
		return sub;
	}
	public void setSub(String sub) {
		this.sub = sub;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	
	
}
