package com.team.lottery.extvo;

import java.util.Comparator;

import com.team.lottery.vo.DayProfit;
/**
 * 
 * 盈亏报表，投注盈利大小比较
 *
 */
public class LotteryGainComparable  implements Comparator<DayProfit>{

	@Override
	public int compare(DayProfit d1, DayProfit d2) {		
		return d1.getLotteryGain().compareTo(d2.getLotteryGain());
	}

}
