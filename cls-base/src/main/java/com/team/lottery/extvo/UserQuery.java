package com.team.lottery.extvo;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 用户查询
 * @author chenhsh
 *
 */
public class UserQuery {
	
	public static final String EQUAL = "equal";                //等于
	public static final String NOT_EQUAL = "not_equal";        //不等于 
	public static final String GREATER_THAN = "greater_than";  //大于
	public static final String LESS_THAN = "less_than";        //小于
	public static final String GREATER_THAN_EQUAL = "greater_than_equal";  //大于等于
	public static final String LESS_THAN_EQUAL = "less_than_qual";  //小于等于
	/**
	 * 新增业务系统字段，20161123
	 */
	private String bizSystem;
    private String username;
    private String dailiLevel; //代理级别
	private String teamLeaderName; //团队领导人名字
	private String realLeaderName; //真实领导人名称
	private Integer state = 1; //默认查找有效的会员
	private Integer hasAdminUser = 1;  //是否查询加载lotterychenhsh用户
	
	private BigDecimal sscrebate; //模式值 
	private String compare = EQUAL; //模式比较    

    private Integer queryByUserName;
    private Integer queryByMoney;
    private Integer queryBySscRebate;
    private Integer queryByAgent;//按会员排列
    private Integer queryByRegisterDate;  //按注册时间排列
    private Integer queryByLogtime;
    private Integer queryByLogins; //按登录次数
    private Integer queryByLock; //按锁定汇源
    private Integer queryByStarLevel; //按用户星级
    private Integer queryByWithdrawLock;//按提现锁定
    private Integer queryByVipLevel;//按VIP等级
    
    private Boolean isQueryHasBonus;//查询是否含有契约！
    private Boolean isQueryHasSalary;//查询是否含有工资！
    
    private String orderField; //排序字段
    private String orderType; //排序方式
    private BigDecimal startMoney; //金额上限字段
    private BigDecimal endMoney; //金额下限字段
    private String trueName;
    private String phone;
    private String qq;
    private String email;
    /**
     * 是否游客 0否  1是
     */
    private Integer isTourist;
    
    private Boolean isQueryFirstTime;//是否进行首冲时间查询
    
    private Date createdDateStart; //查询开始时间
    
    private Date createdDateEnd;  //查询结束时间
    
    private String regfrom; //查询上线
    
    
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Integer getIsTourist() {
		return isTourist;
	}
	public void setIsTourist(Integer isTourist) {
		this.isTourist = isTourist;
	}
	public String getBizSystem() {
		return bizSystem;
	}
	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getDailiLevel() {
		return dailiLevel;
	}
	public void setDailiLevel(String dailiLevel) {
		this.dailiLevel = dailiLevel;
	}
	public Integer getQueryByUserName() {
		return queryByUserName;
	}
	public void setQueryByUserName(Integer queryByUserName) {
		this.queryByUserName = queryByUserName;
	}
	public Integer getQueryByMoney() {
		return queryByMoney;
	}
	public void setQueryByMoney(Integer queryByMoney) {
		this.queryByMoney = queryByMoney;
	}
	public Integer getQueryBySscRebate() {
		return queryBySscRebate;
	}
	public void setQueryBySscRebate(Integer queryBySscRebate) {
		this.queryBySscRebate = queryBySscRebate;
	}
	public Integer getQueryByAgent() {
		return queryByAgent;
	}
	public void setQueryByAgent(Integer queryByAgent) {
		this.queryByAgent = queryByAgent;
	}
	public Integer getQueryByRegisterDate() {
		return queryByRegisterDate;
	}
	public void setQueryByRegisterDate(Integer queryByRegisterDate) {
		this.queryByRegisterDate = queryByRegisterDate;
	}
	public Integer getQueryByLogtime() {
		return queryByLogtime;
	}
	public void setQueryByLogtime(Integer queryByLogtime) {
		this.queryByLogtime = queryByLogtime;
	}
	public Integer getQueryByLogins() {
		return queryByLogins;
	}
	public void setQueryByLogins(Integer queryByLogins) {
		this.queryByLogins = queryByLogins;
	}
	public Integer getQueryByLock() {
		return queryByLock;
	}
	public void setQueryByLock(Integer queryByLock) {
		this.queryByLock = queryByLock;
	}
	public String getTeamLeaderName() {
		return teamLeaderName;
	}
	public void setTeamLeaderName(String teamLeaderName) {
		this.teamLeaderName = teamLeaderName;
	}
	public String getRealLeaderName() {
		return realLeaderName;
	}
	public void setRealLeaderName(String realLeaderName) {
		this.realLeaderName = realLeaderName;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public BigDecimal getSscrebate() {
		return sscrebate;
	}
	public void setSscrebate(BigDecimal sscrebate) {
		this.sscrebate = sscrebate;
	}
	public String getCompare() {
		return compare;
	}
	public void setCompare(String compare) {
		this.compare = compare;
	}
	public Integer getHasAdminUser() {
		return hasAdminUser;
	}
	public void setHasAdminUser(Integer hasAdminUser) {
		this.hasAdminUser = hasAdminUser;
	}
	public Integer getQueryByStarLevel() {
		return queryByStarLevel;
	}
	public void setQueryByStarLevel(Integer queryByStarLevel) {
		this.queryByStarLevel = queryByStarLevel;
	}
	public Boolean getIsQueryHasBonus() {
		return isQueryHasBonus;
	}
	public void setIsQueryHasBonus(Boolean isQueryHasBonus) {
		this.isQueryHasBonus = isQueryHasBonus;
	}
	public Boolean getIsQueryHasSalary() {
		return isQueryHasSalary;
	}
	public void setIsQueryHasSalary(Boolean isQueryHasSalary) {
		this.isQueryHasSalary = isQueryHasSalary;
	}
	public Integer getQueryByWithdrawLock() {
		return queryByWithdrawLock;
	}
	public void setQueryByWithdrawLock(Integer queryByWithdrawLock) {
		this.queryByWithdrawLock = queryByWithdrawLock;
	}
	public String getOrderField() {
		return orderField;
	}
	public void setOrderField(String orderField) {
		this.orderField = orderField;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public BigDecimal getStartMoney() {
		return startMoney;
	}
	public void setStartMoney(BigDecimal startMoney) {
		this.startMoney = startMoney;
	}
	public BigDecimal getEndMoney() {
		return endMoney;
	}
	public void setEndMoney(BigDecimal endMoney) {
		this.endMoney = endMoney;
	}
	public String getTrueName() {
		return trueName;
	}
	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getQq() {
		return qq;
	}
	public void setQq(String qq) {
		this.qq = qq;
	}
	public Boolean getIsQueryFirstTime() {
		return isQueryFirstTime;
	}
	public void setIsQueryFirstTime(Boolean isQueryFirstTime) {
		this.isQueryFirstTime = isQueryFirstTime;
	}
	public Date getCreatedDateStart() {
		return createdDateStart;
	}
	public void setCreatedDateStart(Date createdDateStart) {
		this.createdDateStart = createdDateStart;
	}
	public Date getCreatedDateEnd() {
		return createdDateEnd;
	}
	public void setCreatedDateEnd(Date createdDateEnd) {
		this.createdDateEnd = createdDateEnd;
	}
	public String getRegfrom() {
		return regfrom;
	}
	public void setRegfrom(String regfrom) {
		this.regfrom = regfrom;
	}
	public Integer getQueryByVipLevel() {
		return queryByVipLevel;
	}
	public void setQueryByVipLevel(Integer queryByVipLevel) {
		this.queryByVipLevel = queryByVipLevel;
	}
    
	
}
