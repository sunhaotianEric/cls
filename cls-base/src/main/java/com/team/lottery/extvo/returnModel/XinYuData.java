package com.team.lottery.extvo.returnModel;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "xml")
public class XinYuData {
    //版本号
	private String version;
	//字符集
	private String charset;
	//签名方式
	private String sign_type;
	//返回状态码
	private String status;
	//返回信息
	private String message;
	//业务结果
	private String result_code;
	//签名
	private String sign;
    //二维码链接
	private String code_url;
	//二维码图片
	private String code_img_url;
	//支付地址
	private String pay_info;
	
	@XmlElement
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	@XmlElement
	public String getCharset() {
		return charset;
	}
	public void setCharset(String charset) {
		this.charset = charset;
	}
	@XmlElement
	public String getSign_type() {
		return sign_type;
	}
	public void setSign_type(String sign_type) {
		this.sign_type = sign_type;
	}
	@XmlElement
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@XmlElement
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@XmlElement
	public String getResult_code() {
		return result_code;
	}
	public void setResult_code(String result_code) {
		this.result_code = result_code;
	}
	@XmlElement
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	@XmlElement
	public String getCode_url() {
		return code_url;
	}
	public void setCode_url(String code_url) {
		this.code_url = code_url;
	}
	@XmlElement
	public String getCode_img_url() {
		return code_img_url;
	}
	public void setCode_img_url(String code_img_url) {
		this.code_img_url = code_img_url;
	}
	@XmlElement
	public String getPay_info() {
		return pay_info;
	}
	public void setPay_info(String pay_info) {
		this.pay_info = pay_info;
	}
	
	
}
