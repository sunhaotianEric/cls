package com.team.lottery.extvo;

import java.util.Date;

/**
 * 根据创建时间查询平台盈利数据
 * @author Administrator
 *
 */
public class DayProfitQuery {
	private String bizSystem;
	private Integer gainModel;
	private Date belongDateStart;
	private Date belongDateEnd;
   
   
	public String getBizSystem() {
		return bizSystem;
	}
	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}
	public Integer getGainModel() {
		return gainModel;
	}
	public void setGainModel(Integer gainModel) {
		this.gainModel = gainModel;
	}
	public Date getBelongDateStart() {
		return belongDateStart;
	}
	public void setBelongDateStart(Date belongDateStart) {
		this.belongDateStart = belongDateStart;
	}
	public Date getBelongDateEnd() {
		return belongDateEnd;
	}
	public void setBelongDateEnd(Date belongDateEnd) {
		this.belongDateEnd = belongDateEnd;
	}
    
    
}
