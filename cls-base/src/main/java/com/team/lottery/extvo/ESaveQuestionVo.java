package com.team.lottery.extvo;

/**
 * 安全问题扩展vo
 * @author
 *
 */
public class ESaveQuestionVo {

	private String code;
	private String description;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

}
