package com.team.lottery.extvo;

import java.util.Date;

public class TreatmentApplyQuery {

	private String userName; 
	private String treatmentType;  //待遇类型
	private Date belongDate;  //待遇归属日期(精准查询)
	private Integer applyStatus;  //申请状态
	private Date belongDateStart;  //查询待遇归属日期起始时间
	private Date belongDateEnd;  //查询待遇归属日期结束时间
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getTreatmentType() {
		return treatmentType;
	}
	public void setTreatmentType(String treatmentType) {
		this.treatmentType = treatmentType;
	}
	public Date getBelongDate() {
		return belongDate;
	}
	public void setBelongDate(Date belongDate) {
		this.belongDate = belongDate;
	}
	public Integer getApplyStatus() {
		return applyStatus;
	}
	public void setApplyStatus(Integer applyStatus) {
		this.applyStatus = applyStatus;
	}
	public Date getBelongDateStart() {
		return belongDateStart;
	}
	public void setBelongDateStart(Date belongDateStart) {
		this.belongDateStart = belongDateStart;
	}
	public Date getBelongDateEnd() {
		return belongDateEnd;
	}
	public void setBelongDateEnd(Date belongDateEnd) {
		this.belongDateEnd = belongDateEnd;
	}
	
}
