package com.team.lottery.extvo;

import java.util.HashMap;

import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;

public class LotteryCodeBjpk10ZstVo {
    private Integer id;

    //开奖期号
    private String lotteryNum;

    private String numInfo1;

    private String numInfo2;

    private String numInfo3;

    private String numInfo4;

    private String numInfo5;

    private String numInfo6;

    private String numInfo7;

    private String numInfo8;

    private String numInfo9;

    private String numInfo10;
    /**
     * 第1名
     */
    private HashMap<String, Integer> oneNumMateCurrent;
    /**
     * 第2名
     */
    private HashMap<String, Integer> secondNumMateCurrent;
    /**
     * 第3名
     */
    private HashMap<String, Integer> threeNumMateCurrent;
    /**
     * 第4名
     */
    private HashMap<String, Integer> fourNumMateCurrent;
    /**
     * 第5名
     */
    private HashMap<String, Integer> fiveNumMateCurrent;
    /**
     * 第6名
     */
    private HashMap<String, Integer> sixNumMateCurrent;
    /**
     * 第7名
     */
    private HashMap<String, Integer> sevenNumMateCurrent;
    /**
     * 第8名
     */
    private HashMap<String, Integer> eightNumMateCurrent;
    /**
     * 第9名
     */
    private HashMap<String, Integer> nineNumMateCurrent;
    /**
     * 第10名
     */
    private HashMap<String, Integer> tenNumMateCurrent;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLotteryNum() {
        return lotteryNum;
    }

    public void setLotteryNum(String lotteryNum) {
        this.lotteryNum = lotteryNum == null ? null : lotteryNum.trim();
    }

    public String getNumInfo1() {
        return numInfo1;
    }

    public void setNumInfo1(String numInfo1) {
        this.numInfo1 = numInfo1 == null ? null : numInfo1.trim();
    }

    public String getNumInfo2() {
        return numInfo2;
    }

    public void setNumInfo2(String numInfo2) {
        this.numInfo2 = numInfo2 == null ? null : numInfo2.trim();
    }

    public String getNumInfo3() {
        return numInfo3;
    }

    public void setNumInfo3(String numInfo3) {
        this.numInfo3 = numInfo3 == null ? null : numInfo3.trim();
    }

    public String getNumInfo4() {
        return numInfo4;
    }

    public void setNumInfo4(String numInfo4) {
        this.numInfo4 = numInfo4 == null ? null : numInfo4.trim();
    }

    public String getNumInfo5() {
        return numInfo5;
    }

    public void setNumInfo5(String numInfo5) {
        this.numInfo5 = numInfo5 == null ? null : numInfo5.trim();
    }

    public String getNumInfo6() {
        return numInfo6;
    }

    public void setNumInfo6(String numInfo6) {
        this.numInfo6 = numInfo6 == null ? null : numInfo6.trim();
    }

    public String getNumInfo7() {
        return numInfo7;
    }

    public void setNumInfo7(String numInfo7) {
        this.numInfo7 = numInfo7 == null ? null : numInfo7.trim();
    }

    public String getNumInfo8() {
        return numInfo8;
    }

    public void setNumInfo8(String numInfo8) {
        this.numInfo8 = numInfo8 == null ? null : numInfo8.trim();
    }

    public String getNumInfo9() {
        return numInfo9;
    }

    public void setNumInfo9(String numInfo9) {
        this.numInfo9 = numInfo9 == null ? null : numInfo9.trim();
    }

    public String getNumInfo10() {
        return numInfo10;
    }

    public void setNumInfo10(String numInfo10) {
        this.numInfo10 = numInfo10 == null ? null : numInfo10.trim();
    }

   
	
	public HashMap<String, Integer> getOneNumMateCurrent() {
		return oneNumMateCurrent;
	}

	public void setOneNumMateCurrent(HashMap<String, Integer> oneNumMateCurrent) {
		this.oneNumMateCurrent = oneNumMateCurrent;
	}

	public HashMap<String, Integer> getSecondNumMateCurrent() {
		return secondNumMateCurrent;
	}

	public void setSecondNumMateCurrent(HashMap<String, Integer> secondNumMateCurrent) {
		this.secondNumMateCurrent = secondNumMateCurrent;
	}

	public HashMap<String, Integer> getThreeNumMateCurrent() {
		return threeNumMateCurrent;
	}

	public void setThreeNumMateCurrent(HashMap<String, Integer> threeNumMateCurrent) {
		this.threeNumMateCurrent = threeNumMateCurrent;
	}

	public HashMap<String, Integer> getFourNumMateCurrent() {
		return fourNumMateCurrent;
	}

	public void setFourNumMateCurrent(HashMap<String, Integer> fourNumMateCurrent) {
		this.fourNumMateCurrent = fourNumMateCurrent;
	}

	public HashMap<String, Integer> getFiveNumMateCurrent() {
		return fiveNumMateCurrent;
	}

	public void setFiveNumMateCurrent(HashMap<String, Integer> fiveNumMateCurrent) {
		this.fiveNumMateCurrent = fiveNumMateCurrent;
	}

	public HashMap<String, Integer> getSixNumMateCurrent() {
		return sixNumMateCurrent;
	}

	public void setSixNumMateCurrent(HashMap<String, Integer> sixNumMateCurrent) {
		this.sixNumMateCurrent = sixNumMateCurrent;
	}

	public HashMap<String, Integer> getSevenNumMateCurrent() {
		return sevenNumMateCurrent;
	}

	public void setSevenNumMateCurrent(HashMap<String, Integer> sevenNumMateCurrent) {
		this.sevenNumMateCurrent = sevenNumMateCurrent;
	}

	public HashMap<String, Integer> getEightNumMateCurrent() {
		return eightNumMateCurrent;
	}

	public void setEightNumMateCurrent(HashMap<String, Integer> eightNumMateCurrent) {
		this.eightNumMateCurrent = eightNumMateCurrent;
	}

	public HashMap<String, Integer> getNineNumMateCurrent() {
		return nineNumMateCurrent;
	}

	public void setNineNumMateCurrent(HashMap<String, Integer> nineNumMateCurrent) {
		this.nineNumMateCurrent = nineNumMateCurrent;
	}

	public HashMap<String, Integer> getTenNumMateCurrent() {
		return tenNumMateCurrent;
	}

	public void setTenNumMateCurrent(HashMap<String, Integer> tenNumMateCurrent) {
		this.tenNumMateCurrent = tenNumMateCurrent;
	}

	/**
	 * 获取开奖号码的拼接
	 * @return
	 */
	public String getOpencodeStr() {
		StringBuffer sb = new StringBuffer();
		if(!StringUtils.isEmpty(numInfo1)) {
			sb.append(numInfo1.trim()).append(ConstantUtil.OPENCODE_PLIT);
		}
		if(!StringUtils.isEmpty(numInfo2)) {
			sb.append(numInfo2.trim()).append(ConstantUtil.OPENCODE_PLIT);
		}
		if(!StringUtils.isEmpty(numInfo3)) {
			sb.append(numInfo3.trim()).append(ConstantUtil.OPENCODE_PLIT);
		}
		if(!StringUtils.isEmpty(numInfo4)) {
			sb.append(numInfo4.trim()).append(ConstantUtil.OPENCODE_PLIT);
		}
		if(!StringUtils.isEmpty(numInfo5)) {
			sb.append(numInfo5.trim()).append(ConstantUtil.OPENCODE_PLIT);
		}
		if(!StringUtils.isEmpty(numInfo6)) {
			sb.append(numInfo6.trim()).append(ConstantUtil.OPENCODE_PLIT);
		}
		if(!StringUtils.isEmpty(numInfo7)) {
			sb.append(numInfo7.trim()).append(ConstantUtil.OPENCODE_PLIT);
		}
		if(!StringUtils.isEmpty(numInfo8)) {
			sb.append(numInfo8.trim()).append(ConstantUtil.OPENCODE_PLIT);
		}
		if(!StringUtils.isEmpty(numInfo9)) {
			sb.append(numInfo9.trim()).append(ConstantUtil.OPENCODE_PLIT);
		}
		if(!StringUtils.isEmpty(numInfo10)) {
			sb.append(numInfo10.trim()).append(ConstantUtil.OPENCODE_PLIT);
		}
		
		if(sb.length() != 0){
			sb.deleteCharAt(sb.length() - 1);
		}
		
		//删除最后一个分隔符
		return sb.toString();
	}
	
}