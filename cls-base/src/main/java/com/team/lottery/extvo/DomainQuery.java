package com.team.lottery.extvo;

public class DomainQuery {
	   private Integer enabled;
	   private Integer indexIsShow;
	   private Integer extendIsShow;
	   
	public Integer getEnabled() {
		return enabled;
	}
	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}
	public Integer getIndexIsShow() {
		return indexIsShow;
	}
	public void setIndexIsShow(Integer indexIsShow) {
		this.indexIsShow = indexIsShow;
	}
	public Integer getExtendIsShow() {
		return extendIsShow;
	}
	public void setExtendIsShow(Integer extendIsShow) {
		this.extendIsShow = extendIsShow;
	}
	   
	   
}