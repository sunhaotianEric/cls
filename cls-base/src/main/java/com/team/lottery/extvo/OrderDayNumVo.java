package com.team.lottery.extvo;

import java.math.BigDecimal;

/**
 * 日量查询结果
 * @author gs
 *
 */
public class OrderDayNumVo {

	private String dayTime;   //日期
	private Integer activeUserNum;  //活跃人数
	private BigDecimal payMoney;  //投注量，即总金额
	private Integer orderNum;  //投注注数
	
	
	public String getDayTime() {
		return dayTime;
	}
	public void setDayTime(String dayTime) {
		this.dayTime = dayTime;
	}
	public Integer getActiveUserNum() {
		return activeUserNum;
	}
	public void setActiveUserNum(Integer activeUserNum) {
		this.activeUserNum = activeUserNum;
	}
	public BigDecimal getPayMoney() {
		return payMoney;
	}
	public void setPayMoney(BigDecimal payMoney) {
		this.payMoney = payMoney;
	}
	public Integer getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}
	
	
	
}
