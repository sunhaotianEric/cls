package com.team.lottery.extvo;

import java.math.BigDecimal;

import com.team.lottery.enums.EMoneyDetailType;
import com.team.lottery.vo.User;

public class UserTotalMoneyByTypeVo {
  
	private User user;
	private EMoneyDetailType detailType;
	private BigDecimal operationValue;
	public UserTotalMoneyByTypeVo() {
		
	}
    public UserTotalMoneyByTypeVo(User user,EMoneyDetailType detailType,BigDecimal operationValue) {
		this.user = user;
		this.detailType = detailType;
		this.operationValue = operationValue;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public EMoneyDetailType getDetailType() {
		return detailType;
	}
	public void setDetailType(EMoneyDetailType detailType) {
		this.detailType = detailType;
	}
	public BigDecimal getOperationValue() {
		return operationValue;
	}
	public void setOperationValue(BigDecimal operationValue) {
		this.operationValue = operationValue;
	}
	
	
}
