package com.team.lottery.extvo;

/**
 * 彩种下对应玩法VO
 * @author gs
 *
 */
public class SubKindVO {

	private String code;
	private String description;
	
	
	
	public SubKindVO() {
	}

	public SubKindVO(String code, String description) {
		this.code = code;
		this.description = description;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
