package com.team.lottery.extvo.returnModel;
/**
 * 北付宝支付返回对象
 * @author Jamine
 *
 */
public class BeiFuBaoPayReturnParam {
	private String memberOrderNumber;
	
	private String lxbOrderNumber;
	
	private String orderStatus;
	
	private String defrayalType;
	
	private String currenciesType;
	
	private String tradeAmount;
	
	private String tradeFee;
	
	private String paymentTime;
	
	private String payBank;
	
	private String attach;
	
	private String checkSimpleDate;

	public String getMemberOrderNumber() {
		return memberOrderNumber;
	}

	public void setMemberOrderNumber(String memberOrderNumber) {
		this.memberOrderNumber = memberOrderNumber;
	}

	public String getLxbOrderNumber() {
		return lxbOrderNumber;
	}

	public void setLxbOrderNumber(String lxbOrderNumber) {
		this.lxbOrderNumber = lxbOrderNumber;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getDefrayalType() {
		return defrayalType;
	}

	public void setDefrayalType(String defrayalType) {
		this.defrayalType = defrayalType;
	}

	public String getCurrenciesType() {
		return currenciesType;
	}

	public void setCurrenciesType(String currenciesType) {
		this.currenciesType = currenciesType;
	}

	public String getTradeAmount() {
		return tradeAmount;
	}

	public void setTradeAmount(String tradeAmount) {
		this.tradeAmount = tradeAmount;
	}

	public String getTradeFee() {
		return tradeFee;
	}

	public void setTradeFee(String tradeFee) {
		this.tradeFee = tradeFee;
	}

	public String getPaymentTime() {
		return paymentTime;
	}

	public void setPaymentTime(String paymentTime) {
		this.paymentTime = paymentTime;
	}

	public String getPayBank() {
		return payBank;
	}

	public void setPayBank(String payBank) {
		this.payBank = payBank;
	}

	public String getAttach() {
		return attach;
	}

	public void setAttach(String attach) {
		this.attach = attach;
	}

	public String getCheckSimpleDate() {
		return checkSimpleDate;
	}

	public void setCheckSimpleDate(String checkSimpleDate) {
		this.checkSimpleDate = checkSimpleDate;
	}
	
	
	
	
}
