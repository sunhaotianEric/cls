package com.team.lottery.extvo;

public class LotteryOrderDetail {

	private String playKindStr;
	private String playKindCodes;
	private Integer lotteryMultiple;
	
	public String getPlayKindStr() {
		return playKindStr;
	}
	public void setPlayKindStr(String playKindStr) {
		this.playKindStr = playKindStr;
	}
	public String getPlayKindCodes() {
		return playKindCodes;
	}
	public void setPlayKindCodes(String playKindCodes) {
		this.playKindCodes = playKindCodes;
	}
	public Integer getLotteryMultiple() {
		return lotteryMultiple;
	}
	public void setLotteryMultiple(Integer lotteryMultiple) {
		this.lotteryMultiple = lotteryMultiple;
	}
	
	
}
