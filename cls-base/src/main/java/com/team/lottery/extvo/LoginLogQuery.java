package com.team.lottery.extvo;

import java.util.Date;

/**
 * 登录日志查询vo
 * @author chenhsh
 *
 */
public class LoginLogQuery {

    private String username;
    private String logip;
    private String loginDomain;
    private Integer loginPort;
    
    private Date logtimeStart;
    private Date logtimeEnd;
   
    private Integer queryByLoginIp;
    private Integer queryByLoginTime;
    private Integer queryByUserName;
    private String bizSystem;
    private String logType;
    private String address;
    private Integer enabled;
    
    private Integer userId;
    
	public Integer getEnabled() {
		return enabled;
	}
	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}
	public String getLogType() {
		return logType;
	}
	public void setLogType(String logType) {
		this.logType = logType;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getLogip() {
		return logip;
	}
	public void setLogip(String logip) {
		this.logip = logip;
	}
	public String getLoginDomain() {
		return loginDomain;
	}
	public void setLoginDomain(String loginDomain) {
		this.loginDomain = loginDomain;
	}
	public Integer getLoginPort() {
		return loginPort;
	}
	public void setLoginPort(Integer loginPort) {
		this.loginPort = loginPort;
	}
	public Date getLogtimeStart() {
		return logtimeStart;
	}
	public void setLogtimeStart(Date logtimeStart) {
		this.logtimeStart = logtimeStart;
	}
	public Date getLogtimeEnd() {
		return logtimeEnd;
	}
	public void setLogtimeEnd(Date logtimeEnd) {
		this.logtimeEnd = logtimeEnd;
	}
	public Integer getQueryByLoginIp() {
		return queryByLoginIp;
	}
	public void setQueryByLoginIp(Integer queryByLoginIp) {
		this.queryByLoginIp = queryByLoginIp;
	}
	public Integer getQueryByLoginTime() {
		return queryByLoginTime;
	}
	public void setQueryByLoginTime(Integer queryByLoginTime) {
		this.queryByLoginTime = queryByLoginTime;
	}
	public Integer getQueryByUserName() {
		return queryByUserName;
	}
	public void setQueryByUserName(Integer queryByUserName) {
		this.queryByUserName = queryByUserName;
	}
	public String getBizSystem() {
		return bizSystem;
	}
	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	
	
}
