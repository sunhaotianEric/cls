package com.team.lottery.extvo.returnModel;

public class AnShengParameter {
	
	private String code;
	
	private String msg;
	
	private AnShengData data;
	
	private String sign;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public AnShengData getData() {
		return data;
	}

	public void setData(AnShengData data) {
		this.data = data;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}


	
}
	
	
	
	

	