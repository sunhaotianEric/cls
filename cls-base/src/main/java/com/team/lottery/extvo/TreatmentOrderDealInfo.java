package com.team.lottery.extvo;

public class TreatmentOrderDealInfo {
	

	//一键处理代理福利按钮是否展现
	private Integer dealHalfMonthBonusBtnOpen = 0;  //0不展现  1展现
	private Integer dealDayBonusBtnOpen = 0;
	private Integer dealDaySalaryBtnOpen = 0;
	private Integer dealMonthSalaryBtnOpen = 0;
	private Integer dealDayCommissionBtnOpen = 0;
	private Integer dealDayActivityBtnOpen = 0;
	
	//一键处理代理福利按钮是否可用
	private Integer dealHalfMonthBonusOpen = 0;  //0 不可以点击  1可以点击
	private Integer dealDayBonusOpen = 0;
	private Integer dealDaySalaryOpen = 0;
	private Integer dealMonthSalaryOpen = 0;
	private Integer dealDayCommissionOpen = 0;
	private Integer dealDayActivityOpen = 0;
	
	
	public Integer getDealDayActivityBtnOpen() {
		return dealDayActivityBtnOpen;
	}
	public void setDealDayActivityBtnOpen(Integer dealDayActivityBtnOpen) {
		this.dealDayActivityBtnOpen = dealDayActivityBtnOpen;
	}
	public Integer getDealDayActivityOpen() {
		return dealDayActivityOpen;
	}
	public void setDealDayActivityOpen(Integer dealDayActivityOpen) {
		this.dealDayActivityOpen = dealDayActivityOpen;
	}
	public Integer getDealHalfMonthBonusBtnOpen() {
		return dealHalfMonthBonusBtnOpen;
	}
	public void setDealHalfMonthBonusBtnOpen(Integer dealHalfMonthBonusBtnOpen) {
		this.dealHalfMonthBonusBtnOpen = dealHalfMonthBonusBtnOpen;
	}
	public Integer getDealDayBonusBtnOpen() {
		return dealDayBonusBtnOpen;
	}
	public void setDealDayBonusBtnOpen(Integer dealDayBonusBtnOpen) {
		this.dealDayBonusBtnOpen = dealDayBonusBtnOpen;
	}
	public Integer getDealDaySalaryBtnOpen() {
		return dealDaySalaryBtnOpen;
	}
	public void setDealDaySalaryBtnOpen(Integer dealDaySalaryBtnOpen) {
		this.dealDaySalaryBtnOpen = dealDaySalaryBtnOpen;
	}
	public Integer getDealMonthSalaryBtnOpen() {
		return dealMonthSalaryBtnOpen;
	}
	public void setDealMonthSalaryBtnOpen(Integer dealMonthSalaryBtnOpen) {
		this.dealMonthSalaryBtnOpen = dealMonthSalaryBtnOpen;
	}
	public Integer getDealDayCommissionBtnOpen() {
		return dealDayCommissionBtnOpen;
	}
	public void setDealDayCommissionBtnOpen(Integer dealDayCommissionBtnOpen) {
		this.dealDayCommissionBtnOpen = dealDayCommissionBtnOpen;
	}
	public Integer getDealHalfMonthBonusOpen() {
		return dealHalfMonthBonusOpen;
	}
	public void setDealHalfMonthBonusOpen(Integer dealHalfMonthBonusOpen) {
		this.dealHalfMonthBonusOpen = dealHalfMonthBonusOpen;
	}
	public Integer getDealDayBonusOpen() {
		return dealDayBonusOpen;
	}
	public void setDealDayBonusOpen(Integer dealDayBonusOpen) {
		this.dealDayBonusOpen = dealDayBonusOpen;
	}
	public Integer getDealDaySalaryOpen() {
		return dealDaySalaryOpen;
	}
	public void setDealDaySalaryOpen(Integer dealDaySalaryOpen) {
		this.dealDaySalaryOpen = dealDaySalaryOpen;
	}
	public Integer getDealMonthSalaryOpen() {
		return dealMonthSalaryOpen;
	}
	public void setDealMonthSalaryOpen(Integer dealMonthSalaryOpen) {
		this.dealMonthSalaryOpen = dealMonthSalaryOpen;
	}
	public Integer getDealDayCommissionOpen() {
		return dealDayCommissionOpen;
	}
	public void setDealDayCommissionOpen(Integer dealDayCommissionOpen) {
		this.dealDayCommissionOpen = dealDayCommissionOpen;
	}
	
	
}
