package com.team.lottery.extvo;

import java.util.Comparator;

import com.team.lottery.vo.DayProfit;
/**
 * 
 * 盈亏报表，盈利大小比较
 *
 */
public class GainComparable  implements Comparator<DayProfit>{

	@Override
	public int compare(DayProfit d1, DayProfit d2) {		
		return d1.getGain().compareTo(d2.getGain());
	}

}
