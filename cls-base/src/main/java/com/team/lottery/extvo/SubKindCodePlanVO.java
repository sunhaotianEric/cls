package com.team.lottery.extvo;

/**
 * 用于计划列表
 * @author Administrator
 *
 */
public class SubKindCodePlanVO {
	private String code;
	private String description;
	private String serviceClassName; //对应的实现逻辑
	
	
	
	public SubKindCodePlanVO() {
	}

	public SubKindCodePlanVO(String code, String description, String serviceClassName) {
		this.code = code;
		this.description = description;
		this.serviceClassName = serviceClassName;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public String getServiceClassName() {
		return serviceClassName;
	}

	public void setServiceClassName(String serviceClassName) {
		this.serviceClassName = serviceClassName;
	}
}
