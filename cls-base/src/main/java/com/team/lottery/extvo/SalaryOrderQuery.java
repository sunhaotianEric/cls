package com.team.lottery.extvo;

import java.math.BigDecimal;
import java.util.Date;

public class SalaryOrderQuery {
    //开始时间
	private Date createdDateStart;
	//结束时间
	private Date createdDateEnd;
	//上级用户ID
	private Integer fromUserId;
	//上级用户姓名
	private String fromUserName;
	//金额
	private BigDecimal money;
	//投注金额
	private BigDecimal lotteryMoney;
	//当前用户ID
	private Integer userId;
	//当前用户姓名
	private String userName;
	//当前业务系统
	private String bizSystem;
	//所属时间
	private Date belongDate;
    //发放状态
	private String releaseStatus;
	//查询范围
	private Integer scope;
	
	public Date getCreatedDateStart() {
		return createdDateStart;
	}

	public void setCreatedDateStart(Date createdDateStart) {
		this.createdDateStart = createdDateStart;
	}

	public Date getCreatedDateEnd() {
		return createdDateEnd;
	}

	public void setCreatedDateEnd(Date createdDateEnd) {
		this.createdDateEnd = createdDateEnd;
	}

	public Integer getFromUserId() {
		return fromUserId;
	}

	public void setFromUserId(Integer fromUserId) {
		this.fromUserId = fromUserId;
	}

	public String getFromUserName() {
		return fromUserName;
	}

	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public BigDecimal getLotteryMoney() {
		return lotteryMoney;
	}

	public void setLotteryMoney(BigDecimal lotteryMoney) {
		this.lotteryMoney = lotteryMoney;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}

	public Date getBelongDate() {
		return belongDate;
	}

	public void setBelongDate(Date belongDate) {
		this.belongDate = belongDate;
	}

	public String getReleaseStatus() {
		return releaseStatus;
	}

	public void setReleaseStatus(String releaseStatus) {
		this.releaseStatus = releaseStatus;
	}

	public Integer getScope() {
		return scope;
	}

	public void setScope(Integer scope) {
		this.scope = scope;
	}

	
}
