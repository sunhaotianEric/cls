package com.team.lottery.extvo;

public class OrderInfoExportVO {

	private String bizSystem;//增加业务系统字段
	private Boolean lotteryId;
	private Boolean lotteryTypeDes;
	private Boolean expect;
	private Boolean userName;
	private Boolean payMoney;
	private Boolean multiple;
	private Boolean afterNumber;
	private Boolean statusStr;
	private Boolean prostateStatusStr;
	private Boolean winCost;
	private Boolean lottery;
	private Boolean createdDateStr;
	private Boolean fromType;
	private Boolean operate;
	public String getBizSystem() {
		return bizSystem;
	}
	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}
	public Boolean getLotteryId() {
		return lotteryId;
	}
	public void setLotteryId(Boolean lotteryId) {
		this.lotteryId = lotteryId;
	}
	public Boolean getLotteryTypeDes() {
		return lotteryTypeDes;
	}
	public void setLotteryTypeDes(Boolean lotteryTypeDes) {
		this.lotteryTypeDes = lotteryTypeDes;
	}
	public Boolean getExpect() {
		return expect;
	}
	public void setExpect(Boolean expect) {
		this.expect = expect;
	}
	public Boolean getUserName() {
		return userName;
	}
	public void setUserName(Boolean userName) {
		this.userName = userName;
	}
	public Boolean getPayMoney() {
		return payMoney;
	}
	public void setPayMoney(Boolean payMoney) {
		this.payMoney = payMoney;
	}
	public Boolean getMultiple() {
		return multiple;
	}
	public void setMultiple(Boolean multiple) {
		this.multiple = multiple;
	}
	public Boolean getAfterNumber() {
		return afterNumber;
	}
	public void setAfterNumber(Boolean afterNumber) {
		this.afterNumber = afterNumber;
	}
	public Boolean getStatusStr() {
		return statusStr;
	}
	public void setStatusStr(Boolean statusStr) {
		this.statusStr = statusStr;
	}
	public Boolean getProstateStatusStr() {
		return prostateStatusStr;
	}
	public void setProstateStatusStr(Boolean prostateStatusStr) {
		this.prostateStatusStr = prostateStatusStr;
	}
	public Boolean getWinCost() {
		return winCost;
	}
	public void setWinCost(Boolean winCost) {
		this.winCost = winCost;
	}
	public Boolean getLottery() {
		return lottery;
	}
	public void setLottery(Boolean lottery) {
		this.lottery = lottery;
	}
	public Boolean getCreatedDateStr() {
		return createdDateStr;
	}
	public void setCreatedDateStr(Boolean createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	public Boolean getFromType() {
		return fromType;
	}
	public void setFromType(Boolean fromType) {
		this.fromType = fromType;
	}
	public Boolean getOperate() {
		return operate;
	}
	public void setOperate(Boolean operate) {
		this.operate = operate;
	}
	
	
	
	
}
