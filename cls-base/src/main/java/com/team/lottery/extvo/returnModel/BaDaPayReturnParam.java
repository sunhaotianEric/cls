package com.team.lottery.extvo.returnModel;
/**
 * 八达付支付返回参数
 * @author hahah
 *
 */
public class BaDaPayReturnParam {
	//商户id
	private String partner;
	//订单号
	private String ordernumber;
	//支付状态 1:支付成功，0为支付失败 ,-1 用户取消支付，－9用户末支付失败
	private String orderstatus;
	//订单金额
	private String paymoney;
	//时间戳
	private String timestamp;
	//订单号
	private String sysnumber;
	//备注信息
	private String attach;
	//MD5签名
	private String sign;
	
	//提供Getter/Setter方法
	public String getPartner() {
		return partner;
	}
	public void setPartner(String partner) {
		this.partner = partner;
	}
	public String getOrdernumber() {
		return ordernumber;
	}
	public void setOrdernumber(String ordernumber) {
		this.ordernumber = ordernumber;
	}
	public String getOrderstatus() {
		return orderstatus;
	}
	public void setOrderstatus(String orderstatus) {
		this.orderstatus = orderstatus;
	}
	public String getPaymoney() {
		return paymoney;
	}
	public void setPaymoney(String paymoney) {
		this.paymoney = paymoney;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getSysnumber() {
		return sysnumber;
	}
	public void setSysnumber(String sysnumber) {
		this.sysnumber = sysnumber;
	}
	public String getAttach() {
		return attach;
	}
	public void setAttach(String attach) {
		this.attach = attach;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	
}
