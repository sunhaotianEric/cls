package com.team.lottery.extvo;

/**
 * 图片上传接口返回信息
 * @author luocheng
 *
 */
public class ImageUploadResult {

	private String result;
	private String msg;
	
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	
}
