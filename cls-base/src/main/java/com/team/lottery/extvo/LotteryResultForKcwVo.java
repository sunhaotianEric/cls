package com.team.lottery.extvo;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import com.team.lottery.util.JaxbUtil;

/**
 * 开彩网接口每行返回数据
 * @author chenhsh
 *
 */
@XmlRootElement(name = "item")
public class LotteryResultForKcwVo{
	
	//期号
	private String expect;
	//开奖号码
	private String opencode;
	//接口返回的开奖时间
	private String opentime;
	
	@XmlAttribute(name = "expect")
	public String getExpect() {
		return expect;
	}
	
	@XmlAttribute(name = "opentime")
	public String getOpentime() {
		return opentime;
	}
	
	@XmlAttribute(name = "opencode")
	public String getOpencode() {
		return opencode;
	}
	
	public void setExpect(String expect) {
		this.expect = expect;
	}
	
	public void setOpencode(String opencode) {
		this.opencode = opencode;
	}
	
	public void setOpentime(String opentime) {
		this.opentime = opentime;
	}
	
	public static void main(String[] args) {

		LotteryResultForKcwVo item1 = new LotteryResultForKcwVo();
		item1.setExpect("2018034");
		item1.setOpencode("41,49,31,46,21,29,36");
		item1.setOpentime("2018-04-03 21:35:08");
		
		LotteryResultForKcwVo item2 = new LotteryResultForKcwVo();
		item2.setExpect("2018033");
		item2.setOpencode("16,13,19,18,09,34,21");
		item2.setOpentime("2018-03-31 21:35:31");

		LotteryResultForKcw resultRoot = new LotteryResultForKcw();
		resultRoot.setLotteryResultVos(new ArrayList<LotteryResultForKcwVo>());
		resultRoot.getLotteryResultVos().add(item1);
		resultRoot.getLotteryResultVos().add(item2);

		String result = JaxbUtil.transformToXml(LotteryResultForKcw.class, resultRoot);
		System.out.println(result);
	}

}
