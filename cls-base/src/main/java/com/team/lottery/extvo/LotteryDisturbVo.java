package com.team.lottery.extvo;

import java.math.BigDecimal;

public class LotteryDisturbVo {
 
	private boolean isDisturb;
	private int model;
	//投注干扰提示
	private String tip;
	//投注干扰的彩种
	private String lotteryType;
	private String lotteryTypeDes;
	//投注干扰彩种对应的期号
	private String lotteryExpect;
	//投注干扰需达到的金额
	private BigDecimal money;
	
	public boolean isDisturb() {
		return isDisturb;
	}
	public void setDisturb(boolean isDisturb) {
		this.isDisturb = isDisturb;
	}
	public int getModel() {
		return model;
	}
	public void setModel(int model) {
		this.model = model;
	}
	public String getTip() {
		return tip;
	}
	public void setTip(String tip) {
		this.tip = tip;
	}
	public String getLotteryType() {
		return lotteryType;
	}
	public void setLotteryType(String lotteryType) {
		this.lotteryType = lotteryType;
	}
	public String getLotteryTypeDes() {
		return lotteryTypeDes;
	}
	public void setLotteryTypeDes(String lotteryTypeDes) {
		this.lotteryTypeDes = lotteryTypeDes;
	}
	public String getLotteryExpect() {
		return lotteryExpect;
	}
	public void setLotteryExpect(String lotteryExpect) {
		this.lotteryExpect = lotteryExpect;
	}
	public BigDecimal getMoney() {
		return money;
	}
	public void setMoney(BigDecimal money) {
		this.money = money;
	}
	
}
