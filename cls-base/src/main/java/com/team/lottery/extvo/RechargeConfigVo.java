package com.team.lottery.extvo;

import java.math.BigDecimal;

/**
 * 充值配置信息查询vo
 * @author chengf
 *
 */
public class RechargeConfigVo {
    
	
	private String bizSystem;
    private String payType;
    private String refType;
    private Integer showUserStarLevel;
    private BigDecimal showUserSscRebate;
    private Integer enabled;
    private Long id;
    private String refIdList;
    private Long refId;
    private String showType;
    //第三方支付是否开启
    private Integer supportHttps;
    
    public Integer getSupportHttps() {
		return supportHttps;
	}

	public void setSupportHttps(Integer supportHttps) {
		this.supportHttps = supportHttps;
	}

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getRefType() {
		return refType;
	}

	public void setRefType(String refType) {
		this.refType = refType;
	}

	public Integer getShowUserStarLevel() {
		return showUserStarLevel;
	}

	public void setShowUserStarLevel(Integer showUserStarLevel) {
		this.showUserStarLevel = showUserStarLevel;
	}

	public BigDecimal getShowUserSscRebate() {
		return showUserSscRebate;
	}

	public void setShowUserSscRebate(BigDecimal showUserSscRebate) {
		this.showUserSscRebate = showUserSscRebate;
	}

	public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRefIdList() {
		return refIdList;
	}

	public void setRefIdList(String refIdList) {
		this.refIdList = refIdList;
	}

	public Long getRefId() {
		return refId;
	}

	public void setRefId(Long refId) {
		this.refId = refId;
	}

	public String getShowType() {
		return showType;
	}

	public void setShowType(String showType) {
		this.showType = showType;
	}
    
}
