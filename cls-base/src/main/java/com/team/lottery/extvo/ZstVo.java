package com.team.lottery.extvo;

import java.util.List;
import java.util.Map;

/**
 * 走势图vo
 * @author chenhsh
 *
 */
public class ZstVo {

	private String expect; //期号
	private String openCodes; //开奖号码
	private List<Map<String, Integer>> omitMap; //当前期号的各个位数的遗漏映射
	
	public String getExpect() {
		return expect;
	}
	public void setExpect(String expect) {
		this.expect = expect;
	}
	public String getOpenCodes() {
		return openCodes;
	}
	public void setOpenCodes(String openCodes) {
		this.openCodes = openCodes;
	}
	public List<Map<String, Integer>> getOmitMap() {
		return omitMap;
	}
	public void setOmitMap(List<Map<String, Integer>> omitMap) {
		this.omitMap = omitMap;
	}
}
