package com.team.lottery.extvo.returnModel;
/**
 * 连赢支付返回对象
 * @author Jamine
 *
 */
public class LianYingPayReturnParam {
	//返回状态码
	private String ret;
	//返回消息
	private String msg;
	//订单编号
	private String orderId;
	//时间戳
	private String time;
	//签名
	private String sign;
	public String getRet() {
		return ret;
	}
	public void setRet(String ret) {
		this.ret = ret;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	
}
