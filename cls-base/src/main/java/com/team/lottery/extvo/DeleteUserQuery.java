package com.team.lottery.extvo;

import java.math.BigDecimal;

public class DeleteUserQuery {
	private Integer id;
	private String	username;
	private String	truename;
	private String dailiLevel; //代理级别
	private BigDecimal sscrebate;//时时彩
	private Integer code;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getTruename() {
		return truename;
	}
	public void setTruename(String truename) {
		this.truename = truename;
	}
	public String getDailiLevel() {
		return dailiLevel;
	}
	public void setDailiLevel(String dailiLevel) {
		this.dailiLevel = dailiLevel;
	}
	public BigDecimal getSscrebate() {
		return sscrebate;
	}
	public void setSscrebate(BigDecimal sscrebate) {
		this.sscrebate = sscrebate;
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	
	
	
}
