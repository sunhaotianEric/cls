package com.team.lottery.extvo;

import java.util.Date;


/**
 * 奖金期号查询vo
 * @author chenhsh
 *
 */
public class LotteryIssueQuery {

    private String lotteryType;
    
    private String lotteryNum;
    
    private Date startime;
    
    private Date endtime;
    
    private Date startime2;
    
    private Date endtime2;


	public String getLotteryType() {
		return lotteryType;
	}

	public void setLotteryType(String lotteryType) {
		this.lotteryType = lotteryType;
	}

	public String getLotteryNum() {
		return lotteryNum;
	}

	public void setLotteryNum(String lotteryNum) {
		this.lotteryNum = lotteryNum;
	}

	public Date getStartime() {
		return startime;
	}

	public void setStartime(Date startime) {
		this.startime = startime;
	}

	public Date getEndtime() {
		return endtime;
	}

	public void setEndtime(Date endtime) {
		this.endtime = endtime;
	}

	public Date getStartime2() {
		return startime2;
	}

	public void setStartime2(Date startime2) {
		this.startime2 = startime2;
	}

	public Date getEndtime2() {
		return endtime2;
	}

	public void setEndtime2(Date endtime2) {
		this.endtime2 = endtime2;
	}
}
