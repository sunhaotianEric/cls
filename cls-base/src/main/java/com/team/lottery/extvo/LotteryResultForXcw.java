package com.team.lottery.extvo;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 星彩网返回结果类
 * @author chenhsh
 *
 */
@XmlRootElement(name = "xml") 
@XmlAccessorType(XmlAccessType.FIELD)
public class LotteryResultForXcw {
	
	@XmlElement(name="row")
	private List<LotteryResultForXcwVo> LotteryResultVos;

	public List<LotteryResultForXcwVo> getLotteryResultVos() {
		return LotteryResultVos;
	}

	public void setLotteryResultVos(List<LotteryResultForXcwVo> lotteryResultVos) {
		LotteryResultVos = lotteryResultVos;
	}
	
}
