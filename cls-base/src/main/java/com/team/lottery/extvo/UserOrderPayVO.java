package com.team.lottery.extvo;

import java.math.BigDecimal;

/**
 * 用户团队有效投注量VO
 * @author gs
 *
 */
public class UserOrderPayVO {

	private Integer userId;
    private String userName;
    private String regfrom;  //用户推荐人的关系
    private String dailiLevel; //用户代理类型
    private BigDecimal sscrebate; //时时彩模式
    private BigDecimal payMoney = BigDecimal.ZERO; //支出金额
    private Integer activeNum;  //达到活跃人数
    
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getRegfrom() {
		return regfrom;
	}
	public void setRegfrom(String regfrom) {
		this.regfrom = regfrom;
	}
	public String getDailiLevel() {
		return dailiLevel;
	}
	public void setDailiLevel(String dailiLevel) {
		this.dailiLevel = dailiLevel;
	}
	public BigDecimal getSscrebate() {
		return sscrebate;
	}
	public void setSscrebate(BigDecimal sscrebate) {
		this.sscrebate = sscrebate;
	}
	public BigDecimal getPayMoney() {
		return payMoney;
	}
	public void setPayMoney(BigDecimal payMoney) {
		this.payMoney = payMoney;
	}
	public Integer getActiveNum() {
		return activeNum;
	}
	public void setActiveNum(Integer activeNum) {
		this.activeNum = activeNum;
	}
	
    
    
}
