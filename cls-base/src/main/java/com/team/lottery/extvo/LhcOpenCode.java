package com.team.lottery.extvo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.util.DateUtil;
import com.team.lottery.util.DateUtils;
import com.team.lottery.util.ShengXiaoNumberUtil;

public class LhcOpenCode {

	//开奖期号
    private String lotteryNum;
    private Date kjtime;
	//开奖号码
	private Integer code1;
	private Integer code2;
	private Integer code3;
	private Integer code4;
	private Integer code5;
	private Integer code6;
	private Integer code7;
		
	public Integer getCode1() {
		return code1;
	}
	public void setCode1(Integer code1) {
		this.code1 = code1;
	}
	public Integer getCode2() {
		return code2;
	}
	public void setCode2(Integer code2) {
		this.code2 = code2;
	}
	public Integer getCode3() {
		return code3;
	}
	public void setCode3(Integer code3) {
		this.code3 = code3;
	}
	public Integer getCode4() {
		return code4;
	}
	public void setCode4(Integer code4) {
		this.code4 = code4;
	}
	public Integer getCode5() {
		return code5;
	}
	public void setCode5(Integer code5) {
		this.code5 = code5;
	}
	public Integer getCode6() {
		return code6;
	}
	public void setCode6(Integer code6) {
		this.code6 = code6;
	}
	public Integer getCode7() {
		return code7;
	}
	public void setCode7(Integer code7) {
		this.code7 = code7;
	}
	
	public String getLotteryNum() {
		return lotteryNum;
	}
	public void setLotteryNum(String lotteryNum) {
		this.lotteryNum = lotteryNum;
	}
	public Date getKjtime() {
		return kjtime;
	}
	public void setKjtime(Date kjtime) {
		this.kjtime = kjtime;
	}
	
	public String getKjtimeStr(){
    	return DateUtils.getDateToStrByFormat(this.getKjtime(), "yyyy/MM/dd HH:mm:ss");
    }
	public Integer getAnimal1() throws ParseException {
		if(code1 != null && kjtime != null){
			 Date festivalStartDate = new SimpleDateFormat("yyyy-MM-dd").parse(SystemConfigConstant.springFestivalStartDate);
			 Date chuyiDate = DateUtil.addDaysToDate(festivalStartDate, 1);
			if(kjtime.before(chuyiDate) && (new Date().compareTo(festivalStartDate) > 0)){
				return ShengXiaoNumberUtil.retuenShengXiaoCode(code1.toString(),1);	
			}else{
				return ShengXiaoNumberUtil.retuenShengXiaoCode(code1.toString(),0);		
			}
			
		}else{
			return 0;
		}
		
	}

	public Integer getAnimal2() throws ParseException {
		if(code2 != null && kjtime != null){
			 Date festivalStartDate = new SimpleDateFormat("yyyy-MM-dd").parse(SystemConfigConstant.springFestivalStartDate);
			 Date chuyiDate = DateUtil.addDaysToDate(festivalStartDate, 1);
			if(kjtime.before(chuyiDate) && (new Date().compareTo(festivalStartDate) > 0)){
				return ShengXiaoNumberUtil.retuenShengXiaoCode(code2.toString(),1);	
			}else{
				return ShengXiaoNumberUtil.retuenShengXiaoCode(code2.toString(),0);		
			}	
		}else{
			return 0;
		}
	}

	public Integer getAnimal3() throws ParseException {
		if(code3 != null && kjtime != null){
			 Date festivalStartDate = new SimpleDateFormat("yyyy-MM-dd").parse(SystemConfigConstant.springFestivalStartDate);
			 Date chuyiDate = DateUtil.addDaysToDate(festivalStartDate, 1);
			if(kjtime.before(chuyiDate) && (new Date().compareTo(festivalStartDate) > 0)){
				return ShengXiaoNumberUtil.retuenShengXiaoCode(code3.toString(),1);	
			}else{
				return ShengXiaoNumberUtil.retuenShengXiaoCode(code3.toString(),0);		
			}	
		}else{
			return 0;
		}
	}

	public Integer getAnimal4() throws ParseException {
		if(code4 != null && kjtime != null){
			 Date festivalStartDate = new SimpleDateFormat("yyyy-MM-dd").parse(SystemConfigConstant.springFestivalStartDate);
			 Date chuyiDate = DateUtil.addDaysToDate(festivalStartDate, 1);
			if(kjtime.before(chuyiDate) && (new Date().compareTo(festivalStartDate) > 0)){
				return ShengXiaoNumberUtil.retuenShengXiaoCode(code4.toString(),1);	
			}else{
				return ShengXiaoNumberUtil.retuenShengXiaoCode(code4.toString(),0);		
			}		
		}else{
			return 0;
		}
	}

	public Integer getAnimal5() throws ParseException {
		if(code5 != null && kjtime != null){
			 Date festivalStartDate = new SimpleDateFormat("yyyy-MM-dd").parse(SystemConfigConstant.springFestivalStartDate);
			 Date chuyiDate = DateUtil.addDaysToDate(festivalStartDate, 1);
			if(kjtime.before(chuyiDate) && (new Date().compareTo(festivalStartDate) > 0)){
				return ShengXiaoNumberUtil.retuenShengXiaoCode(code5.toString(),1);	
			}else{
				return ShengXiaoNumberUtil.retuenShengXiaoCode(code5.toString(),0);		
			}		
		}else{
			return 0;
		}
	}

	public Integer getAnimal6() throws ParseException {
		if(code6 != null && kjtime != null){
			 Date festivalStartDate = new SimpleDateFormat("yyyy-MM-dd").parse(SystemConfigConstant.springFestivalStartDate);
			 Date chuyiDate = DateUtil.addDaysToDate(festivalStartDate, 1);
			if(kjtime.before(chuyiDate) && (new Date().compareTo(festivalStartDate) > 0)){
				return ShengXiaoNumberUtil.retuenShengXiaoCode(code6.toString(),1);	
			}else{
				return ShengXiaoNumberUtil.retuenShengXiaoCode(code6.toString(),0);		
			}		
		}else{
			return 0;
		}
	}

	public Integer getAnimal7() throws ParseException {
		if(code7 != null && kjtime != null){
			 Date festivalStartDate = new SimpleDateFormat("yyyy-MM-dd").parse(SystemConfigConstant.springFestivalStartDate);
			 Date chuyiDate = DateUtil.addDaysToDate(festivalStartDate, 1);
			if(kjtime.before(chuyiDate) && (new Date().compareTo(festivalStartDate) > 0)){
				return ShengXiaoNumberUtil.retuenShengXiaoCode(code7.toString(),1);	
			}else{
				return ShengXiaoNumberUtil.retuenShengXiaoCode(code7.toString(),0);		
			}		
		}else{
			return 0;
		}
	}
	
	//特码大小 49为和
	public String getCode7DaXiao() {
		if(code7 != null && kjtime != null){
			if(code7==49){
				return "和";	
			}
			return ShengXiaoNumberUtil.decideBigOrSmall(code7.toString())==true?"大":"小";	
		}else{
			return "";
		}
	}
	
	//特码单双 49为和
	public String getCode7DanShuang() {
		
		if(code7 != null && kjtime != null){
			if(code7==49){
				return "和";	
			}
			return ShengXiaoNumberUtil.decideSingleOrdouble(code7.toString())==true?"单":"双";	
		}else{
			return "";
		}
	}
	
	//特码家禽 野兽 49为和
	public String getCode7JiaQinOrYeShou() throws ParseException {
		
		if(code7 != null && kjtime != null){
			if(code7==49){
				return "和";	
			}
			 Date festivalStartDate = new SimpleDateFormat("yyyy-MM-dd").parse(SystemConfigConstant.springFestivalStartDate);
			 Date chuyiDate = DateUtil.addDaysToDate(festivalStartDate, 1);
			if(kjtime.before(chuyiDate) && (new Date().compareTo(festivalStartDate) > 0)){
				return ShengXiaoNumberUtil.decideJiaQinOrYeShou(code7.toString(),1)==true?"家禽":"野兽";
			}else{
				return ShengXiaoNumberUtil.decideJiaQinOrYeShou(code7.toString(),0)==true?"家禽":"野兽";
			}
				
		}else{
			return "";
		}
	}
	
	//特码合大小 49为和
	public String getCode7HeDaOrHeXiao() {
		
		if(code7 != null){
			if(code7==49){
				return "和";	
			}
			return ShengXiaoNumberUtil.decideHeDaOrHeXiao(code7.toString())==true?"合大":"合小";	
		}else{
			return "";
		}
	}
	
	//特码合单双 49为和
	public String getCode7HeDanOrHeShuang() {	
		if(code7 != null){
			if(code7==49){
				return "和";	
			}
			return ShengXiaoNumberUtil.decideHeDanOrHeShuang(code7.toString())==true?"合单":"合双";	
		}else{
			return "";
		}
	}
	
	//7个号码总和
	public Integer getCodeZongHe(){
		return code1+code2+code3+code4+code5+code6+code7;
	}
	
	//7个号码总和大小
	public String getCodeZongHeDaXiao() throws Exception{
		String[] codes =new String[7];
		codes[0] = code1.toString();
		codes[1] = code2.toString();
		codes[2] = code3.toString();
		codes[3] = code4.toString();
		codes[4] = code5.toString();
		codes[5] = code6.toString();
		codes[6] = code7.toString();
		return ShengXiaoNumberUtil.decideZongHeDa_Xiao(codes) == true?"总大":"总小";
	}
	
	//7个号码总和单双
	public String getCodeZongHeDanShuang() throws Exception{
		String[] codes =new String[7];
		codes[0] = code1.toString();
		codes[1] = code2.toString();
		codes[2] = code3.toString();
		codes[3] = code4.toString();
		codes[4] = code5.toString();
		codes[5] = code6.toString();
		codes[6] = code7.toString();
		return ShengXiaoNumberUtil.decideZongHeDan_Shuang(codes) == true?"总单":"总双";
	}
}
