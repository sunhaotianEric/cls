package com.team.lottery.extvo;

import java.math.BigDecimal;

public class UserRegisterVo {

	private String userName;
	private String bizSystem;
	private String userPassword;
	private String userPassword2; //密码确认
	private String checkCode; //验证码
	private String mobile; //手机号码
	private String qq; //qq
	private String email; //邮箱
	private String registerUUID;  //注册的推广链接
	private String mobileVcode; //手机短信验证码
	private String regfrom = "mr"; //注册来源，聊天系统 = lt 
	private String sessionId;
	private String headImg; // 用户头像.
	
	/**
     * 邀请码
     */
    private String invitationCode;
    //用于游客设置初始金钱
    private BigDecimal money;
    //是否是游客
    private Integer isTourist;
    // 真实姓名.
    private String trueName; // 用户头像.
    
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	
	public String getMobileVcode() {
		return mobileVcode;
	}
	public void setMobileVcode(String mobileVcode) {
		this.mobileVcode = mobileVcode;
	}
	public String getUserPassword2() {
		return userPassword2;
	}
	public void setUserPassword2(String userPassword2) {
		this.userPassword2 = userPassword2;
	}
	public String getRegisterUUID() {
		return registerUUID;
	}
	public void setRegisterUUID(String registerUUID) {
		this.registerUUID = registerUUID;
	}
	public String getCheckCode() {
		return checkCode;
	}
	public void setCheckCode(String checkCode) {
		this.checkCode = checkCode;
	}
	public String getQq() {
		return qq;
	}
	public void setQq(String qq) {
		this.qq = qq;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getBizSystem() {
		return bizSystem;
	}
	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}
	public String getInvitationCode() {
		return invitationCode;
	}
	public void setInvitationCode(String invitationCode) {
		this.invitationCode = invitationCode;
	}
	public String getRegfrom() {
		return regfrom;
	}
	public void setRegfrom(String regfrom) {
		this.regfrom = regfrom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public BigDecimal getMoney() {
		return money;
	}
	public void setMoney(BigDecimal money) {
		this.money = money;
	}
	public Integer getIsTourist() {
		return isTourist;
	}
	public void setIsTourist(Integer isTourist) {
		this.isTourist = isTourist;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getHeadImg() {
		return headImg;
	}
	public void setHeadImg(String headImg) {
		this.headImg = headImg;
	}
	public String getTrueName() {
		return trueName;
	}
	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}
	
}
