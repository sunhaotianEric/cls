package com.team.lottery.extvo;

import java.util.List;

/**
 * 接口返回的开奖数据对象
 * @author chenhsh
 *
 */
public class LotteryResult {
	
	protected List<LotteryResultVo> LotteryResultVos;

	public List<LotteryResultVo> getLotteryResultVos() {
		return LotteryResultVos;
	}

	public void setLotteryResultVos(List<LotteryResultVo> lotteryResultVos) {
		LotteryResultVos = lotteryResultVos;
	}
	
}
