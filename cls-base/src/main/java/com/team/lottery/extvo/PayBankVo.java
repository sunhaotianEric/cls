package com.team.lottery.extvo;

/**
 * 银行卡信息查询vo
 * @author chengf
 *
 */
public class PayBankVo {
    
	
	private String bizSystem;
    
    private String bankName;
    
    private String bankUserName;
    
    private String adminName;
    
    private String payType;
    
	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankUserName() {
		return bankUserName;
	}

	public void setBankUserName(String bankUserName) {
		this.bankUserName = bankUserName;
	}

	public String getAdminName() {
		return adminName;
	}

	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}
    
    

}
