package com.team.lottery.extvo.returnModel;

public class SixEightSixReturnParameter {
	
	private String outOrderNo;
	
	private String goodsClauses;
	
	private String tradeAmount;
	
	private String shopCode;
	
	private String code;

	private String sign;
	
	private String nonStr;
	
	private String msg;

	public String getOutOrderNo() {
		return outOrderNo;
	}

	public void setOutOrderNo(String outOrderNo) {
		this.outOrderNo = outOrderNo;
	}

	public String getGoodsClauses() {
		return goodsClauses;
	}

	public void setGoodsClauses(String goodsClauses) {
		this.goodsClauses = goodsClauses;
	}

	public String getTradeAmount() {
		return tradeAmount;
	}

	public void setTradeAmount(String tradeAmount) {
		this.tradeAmount = tradeAmount;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getNonStr() {
		return nonStr;
	}

	public void setNonStr(String nonStr) {
		this.nonStr = nonStr;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
}
