package com.team.lottery.extvo.returnModel;

public class YinHeParameter {
	
	private String payurl;
	
	private String mark;
	
	private String price;
	
	private String real_price;
	
	private String is_type;
	
	private String order_id;
	
	private String paysapi_id;
	
	private String url;
	
	private YinHeMessage messages;

	

	public String getPayurl() {
		return payurl;
	}

	public void setPayurl(String payurl) {
		this.payurl = payurl;
	}

	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getReal_price() {
		return real_price;
	}

	public void setReal_price(String real_price) {
		this.real_price = real_price;
	}

	public String getIs_type() {
		return is_type;
	}

	public void setIs_type(String is_type) {
		this.is_type = is_type;
	}

	public String getOrder_id() {
		return order_id;
	}

	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}

	public String getPaysapi_id() {
		return paysapi_id;
	}

	public void setPaysapi_id(String paysapi_id) {
		this.paysapi_id = paysapi_id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public YinHeMessage getMessages() {
		return messages;
	}

	public void setMessages(YinHeMessage messages) {
		this.messages = messages;
	}

	
	
}
	
	
	
	

	