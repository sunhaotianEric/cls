package com.team.lottery.extvo;

import java.util.Date;

import com.team.lottery.util.DateUtil;

public class OperateLogQuery {

	//类型
    private String logType;
    //日志的字符串标识值 
    private String logValurStr;
    //起始时间
    private Date createdDateStart;
    //结束时间
    private Date createdDateEnd;
    //用户名
    private String userName;
    //用户id
    private Integer userId;
    //业务系统
    private String bizSystem;
    
	public Date getCreatedDateStart() {
		if(this.createdDateStart != null){
			return DateUtil.getNowStartTimeByStart(this.createdDateStart);
		}
		return createdDateStart;
	}
	
	public void setCreatedDateStart(Date createdDateStart) {
		this.createdDateStart = createdDateStart;
	}
	
	public Date getCreatedDateEnd() {
		if(this.createdDateEnd != null){
			return DateUtil.getNowStartTimeByEnd(this.createdDateEnd);
		}
		return createdDateEnd;
	}
	
	public void setCreatedDateEnd(Date createdDateEnd) {
		this.createdDateEnd = createdDateEnd;
	}
	
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getLogType() {
		return logType;
	}
	
	public void setLogType(String logType) {
		this.logType = logType;
	}
	
	public String getLogValurStr() {
		return logValurStr;
	}
	
	public void setLogValurStr(String logValurStr) {
		this.logValurStr = logValurStr;
	}
	
	public String getBizSystem() {
		return bizSystem;
	}
	
	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}
	
	
}
