package com.team.lottery.extvo;

public class UserRebate {
	//彩票返点
	private String lotteryRebate;
	//真人返水
	private String realityPersonRebate;
	//电子返水
	private String electronicsRebate;
	//斗鸡返水
	private String gamecockRebate;
	//体育返水
	private String sportRebate;
	//棋牌返水
	private String chessRebate;
	
	
	public String getLotteryRebate() {
		return lotteryRebate;
	}
	public void setLotteryRebate(String lotteryRebate) {
		this.lotteryRebate = lotteryRebate;
	}
	public String getRealityPersonRebate() {
		return realityPersonRebate;
	}
	public void setRealityPersonRebate(String realityPersonRebate) {
		this.realityPersonRebate = realityPersonRebate;
	}
	public String getElectronicsRebate() {
		return electronicsRebate;
	}
	public void setElectronicsRebate(String electronicsRebate) {
		this.electronicsRebate = electronicsRebate;
	}
	public String getGamecockRebate() {
		return gamecockRebate;
	}
	public void setGamecockRebate(String gamecockRebate) {
		this.gamecockRebate = gamecockRebate;
	}
	public String getSportRebate() {
		return sportRebate;
	}
	public void setSportRebate(String sportRebate) {
		this.sportRebate = sportRebate;
	}
	public String getChessRebate() {
		return chessRebate;
	}
	public void setChessRebate(String chessRebate) {
		this.chessRebate = chessRebate;
	}
}
