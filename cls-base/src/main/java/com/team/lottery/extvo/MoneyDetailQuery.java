package com.team.lottery.extvo;

import java.util.Date;

public class MoneyDetailQuery {

	// 查询自己
	public static final Integer SCOPE_OWN = 1;
	// 查询直接下级
	public static final Integer SCOPE_SUB = 2;
	// 查询所有下级
	public static final Integer SCOPE_ALL_SUB = 3;

	// 报表类型 历史报表
	public static final Integer REPORT_TYPE_HISTORY = 0;
	// 报表类型 实时报表
	public static final Integer REPORT_TYPE_TODAY = 1;

	private String bizSystem;

	// 资金明细类型
	private String detailType;
	// 订单号
	private Long orderId;
	// 方案ID
	private String lotteryId;
	// 投注彩种类型
	private String lotteryType;
	// 期号
	private String expect;
	// 资金起始时间
	private Date createdDateStart;
	// 资金结束时间
	private Date createdDateEnd;
	// 用户名
	private String userName;
	// 是否已经分红
	private Integer yetBonus;

	// 玩法 用于查询detail_des
	private String lotteryKind;
	// 模式
	private String lotteryModel;

	// 报表类型
	private Integer reportType;
	// 是否有效
	private Integer enabled;
	// 团队领导人名字
	private String teamLeaderName; // 包含推荐人的拼接
	private String realLeaderName; // 真是的领导人名字

	private Boolean isQuerySub = false; // 是否同时查询下级用户
	private Integer queryScope = SCOPE_OWN; // 账户明细查询的范围

	private String dailiLevel;

	private Integer dateRange; // 查询时间代号.(1表示今天,-1表示昨天,-7表示最近7天,30表示本月,-30表示上个月.)

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}

	public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

	public String getDetailType() {
		return detailType;
	}

	public void setDetailType(String detailType) {
		this.detailType = detailType;
	}

	public String getLotteryId() {
		return lotteryId;
	}

	public void setLotteryId(String lotteryId) {
		this.lotteryId = lotteryId;
	}

	public String getExpect() {
		return expect;
	}

	public void setExpect(String expect) {
		this.expect = expect;
	}

	public Date getCreatedDateStart() {
		return createdDateStart;
	}

	public void setCreatedDateStart(Date createdDateStart) {
		this.createdDateStart = createdDateStart;
	}

	public Date getCreatedDateEnd() {
		return createdDateEnd;
	}

	public void setCreatedDateEnd(Date createdDateEnd) {
		this.createdDateEnd = createdDateEnd;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getYetBonus() {
		return yetBonus;
	}

	public void setYetBonus(Integer yetBonus) {
		this.yetBonus = yetBonus;
	}

	public String getTeamLeaderName() {
		return teamLeaderName;
	}

	public void setTeamLeaderName(String teamLeaderName) {
		this.teamLeaderName = teamLeaderName;
	}

	public String getRealLeaderName() {
		return realLeaderName;
	}

	public void setRealLeaderName(String realLeaderName) {
		this.realLeaderName = realLeaderName;
	}

	public String getLotteryType() {
		return lotteryType;
	}

	public void setLotteryType(String lotteryType) {
		this.lotteryType = lotteryType;
	}

	public Boolean getIsQuerySub() {
		return isQuerySub;
	}

	public void setIsQuerySub(Boolean isQuerySub) {
		this.isQuerySub = isQuerySub;
	}

	public Integer getQueryScope() {
		return queryScope;
	}

	public void setQueryScope(Integer queryScope) {
		this.queryScope = queryScope;
	}

	public String getLotteryKind() {
		return lotteryKind;
	}

	public void setLotteryKind(String lotteryKind) {
		this.lotteryKind = lotteryKind;
	}

	public String getLotteryModel() {
		return lotteryModel;
	}

	public void setLotteryModel(String lotteryModel) {
		this.lotteryModel = lotteryModel;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Integer getReportType() {
		return reportType;
	}

	public void setReportType(Integer reportType) {
		this.reportType = reportType;
	}

	public String getDailiLevel() {
		return dailiLevel;
	}

	public void setDailiLevel(String dailiLevel) {
		this.dailiLevel = dailiLevel;
	}

	public Integer getDateRange() {
		return dateRange;
	}

	public void setDageRange(Integer dageRange) {
		this.dateRange = dageRange;
	}

}
