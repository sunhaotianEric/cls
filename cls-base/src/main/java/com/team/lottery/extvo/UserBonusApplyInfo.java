package com.team.lottery.extvo;

import java.math.BigDecimal;

public class UserBonusApplyInfo {
	
	//对应页面的按钮的申请状态
	//可申请
	public static final Integer CAN_APPLY = 1;
	//不可申请
	public static final Integer CAN_NOT_APPLY = 2;
	//已经申请
	public static final Integer YET_APPLY = 3;

	//日佣金可申请金额
	private BigDecimal dayCommissionApplyMoney = BigDecimal.ZERO;
	//日工资可申请金额
	private BigDecimal daySalaryApplyMoney = BigDecimal.ZERO;
	//日分红可申请金额
	private BigDecimal dayBonusApplyMoney = BigDecimal.ZERO;
	//上半月分红可申请金额
	private BigDecimal upHalfMonthBonusApplyMoney = BigDecimal.ZERO;
	//下半月分红可申请金额
	private BigDecimal downHalfMonthBonusApplyMoney = BigDecimal.ZERO;
	//月工资可申请金额
	private BigDecimal monthSalaryApplyMoney = BigDecimal.ZERO;
	
	//每日活动可申请金额
	private BigDecimal dayActivityApplyMoney = BigDecimal.ZERO;
	
	private Integer dayCommissionApplyStatus = CAN_NOT_APPLY;
	private Integer daySalaryApplyStatus = CAN_NOT_APPLY;
	private Integer dayBonusApplyStatus = CAN_NOT_APPLY;
	private Integer upHalfMonthBonusStatus = CAN_NOT_APPLY;
	private Integer downHalfMonthBonusApplyStatus = CAN_NOT_APPLY;
	private Integer monthSalaryApplyStatus = CAN_NOT_APPLY;
	private Integer dayActivityApplyStatus = CAN_NOT_APPLY;
	
	public Integer getDayActivityApplyStatus() {
		return dayActivityApplyStatus;
	}
	public void setDayActivityApplyStatus(Integer dayActivityApplyStatus) {
		this.dayActivityApplyStatus = dayActivityApplyStatus;
	}
	public BigDecimal getDayActivityApplyMoney() {
		return dayActivityApplyMoney;
	}
	public void setDayActivityApplyMoney(BigDecimal dayActivityApplyMoney) {
		this.dayActivityApplyMoney = dayActivityApplyMoney;
	}
	public BigDecimal getDayCommissionApplyMoney() {
		return dayCommissionApplyMoney;
	}
	public void setDayCommissionApplyMoney(BigDecimal dayCommissionApplyMoney) {
		this.dayCommissionApplyMoney = dayCommissionApplyMoney;
	}
	public BigDecimal getDaySalaryApplyMoney() {
		return daySalaryApplyMoney;
	}
	public void setDaySalaryApplyMoney(BigDecimal daySalaryApplyMoney) {
		this.daySalaryApplyMoney = daySalaryApplyMoney;
	}
	public BigDecimal getDayBonusApplyMoney() {
		return dayBonusApplyMoney;
	}
	public void setDayBonusApplyMoney(BigDecimal dayBonusApplyMoney) {
		this.dayBonusApplyMoney = dayBonusApplyMoney;
	}
	public BigDecimal getUpHalfMonthBonusApplyMoney() {
		return upHalfMonthBonusApplyMoney;
	}
	public void setUpHalfMonthBonusApplyMoney(BigDecimal upHalfMonthBonusApplyMoney) {
		this.upHalfMonthBonusApplyMoney = upHalfMonthBonusApplyMoney;
	}
	public BigDecimal getDownHalfMonthBonusApplyMoney() {
		return downHalfMonthBonusApplyMoney;
	}
	public void setDownHalfMonthBonusApplyMoney(
			BigDecimal downHalfMonthBonusApplyMoney) {
		this.downHalfMonthBonusApplyMoney = downHalfMonthBonusApplyMoney;
	}
	public BigDecimal getMonthSalaryApplyMoney() {
		return monthSalaryApplyMoney;
	}
	public void setMonthSalaryApplyMoney(BigDecimal monthSalaryApplyMoney) {
		this.monthSalaryApplyMoney = monthSalaryApplyMoney;
	}
	public Integer getDayCommissionApplyStatus() {
		return dayCommissionApplyStatus;
	}
	public void setDayCommissionApplyStatus(Integer dayCommissionApplyStatus) {
		this.dayCommissionApplyStatus = dayCommissionApplyStatus;
	}
	public Integer getDaySalaryApplyStatus() {
		return daySalaryApplyStatus;
	}
	public void setDaySalaryApplyStatus(Integer daySalaryApplyStatus) {
		this.daySalaryApplyStatus = daySalaryApplyStatus;
	}
	public Integer getDayBonusApplyStatus() {
		return dayBonusApplyStatus;
	}
	public void setDayBonusApplyStatus(Integer dayBonusApplyStatus) {
		this.dayBonusApplyStatus = dayBonusApplyStatus;
	}
	public Integer getUpHalfMonthBonusStatus() {
		return upHalfMonthBonusStatus;
	}
	public void setUpHalfMonthBonusStatus(Integer upHalfMonthBonusStatus) {
		this.upHalfMonthBonusStatus = upHalfMonthBonusStatus;
	}
	public Integer getDownHalfMonthBonusApplyStatus() {
		return downHalfMonthBonusApplyStatus;
	}
	public void setDownHalfMonthBonusApplyStatus(
			Integer downHalfMonthBonusApplyStatus) {
		this.downHalfMonthBonusApplyStatus = downHalfMonthBonusApplyStatus;
	}
	public Integer getMonthSalaryApplyStatus() {
		return monthSalaryApplyStatus;
	}
	public void setMonthSalaryApplyStatus(Integer monthSalaryApplyStatus) {
		this.monthSalaryApplyStatus = monthSalaryApplyStatus;
	}
	
	
}
