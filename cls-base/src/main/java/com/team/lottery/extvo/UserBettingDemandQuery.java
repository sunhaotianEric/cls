package com.team.lottery.extvo;

import java.util.Date;

public class UserBettingDemandQuery {

	private String bizSystem;

    private Integer userId;

    private String userName;
    
    private String doneStatus;

    private Date createDateStart;
    
    private Date createDateEnd;

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getDoneStatus() {
		return doneStatus;
	}

	public void setDoneStatus(String doneStatus) {
		this.doneStatus = doneStatus;
	}

	public Date getCreateDateStart() {
		return createDateStart;
	}

	public void setCreateDateStart(Date createDateStart) {
		this.createDateStart = createDateStart;
	}

	public Date getCreateDateEnd() {
		return createDateEnd;
	}

	public void setCreateDateEnd(Date createDateEnd) {
		this.createDateEnd = createDateEnd;
	}
    
    
}
