package com.team.lottery.extvo.returnModel;

public class BeiFuBaoReturnParameter {
	
	private String context;
	
	private String success;
	
	private String message;

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
}
	
	
	

	