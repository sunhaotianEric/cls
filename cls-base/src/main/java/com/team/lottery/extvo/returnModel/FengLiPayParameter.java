package com.team.lottery.extvo.returnModel;

public class FengLiPayParameter {
	
	private String ret_code;
	
	private String ret_msg;
	
	private String custid;
	
	private String out_trade_no;
	
	private String body;
	
	private Float total_fee;
	
	private String status;
	
	private String code_img_url;
	
	private String transaction_id;
	
	private String code_qr_url;
	

	public String getRet_code() {
		return ret_code;
	}

	public void setRet_code(String ret_code) {
		this.ret_code = ret_code;
	}

	public String getRet_msg() {
		return ret_msg;
	}

	public void setRet_msg(String ret_msg) {
		this.ret_msg = ret_msg;
	}

	public String getCustid() {
		return custid;
	}

	public void setCustid(String custid) {
		this.custid = custid;
	}

	public String getOut_trade_no() {
		return out_trade_no;
	}

	public void setOut_trade_no(String out_trade_no) {
		this.out_trade_no = out_trade_no;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Float getTotal_fee() {
		return total_fee;
	}

	public void setTotal_fee(Float total_fee) {
		this.total_fee = total_fee;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCode_img_url() {
		return code_img_url;
	}

	public void setCode_img_url(String code_img_url) {
		this.code_img_url = code_img_url;
	}

	public String getTransaction_id() {
		return transaction_id;
	}

	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}

	public String getCode_qr_url() {
		return code_qr_url;
	}

	public void setCode_qr_url(String code_qr_url) {
		this.code_qr_url = code_qr_url;
	}
	
}
	
	
	
	

	