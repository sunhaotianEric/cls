package com.team.lottery.extvo;

/**
 * 幸运开奖号码导入查询对象.
 * 
 * @author Jamine
 *
 */
public class LotteryCodeXyImportQuery {

	// 系统.
	private String bizSystem;
	// 彩种类型.
	private String lotteryType;
	// 期号.
	private String issueNo;
	// 使用状态.(0是未使用,1是已使用,2是已失效).
	private Integer usedStatus;

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}

	public String getLotteryType() {
		return lotteryType;
	}

	public void setLotteryType(String lotteryType) {
		this.lotteryType = lotteryType;
	}

	public String getIssueNo() {
		return issueNo;
	}

	public void setIssueNo(String issueNo) {
		this.issueNo = issueNo;
	}

	public Integer getUsedStatus() {
		return usedStatus;
	}

	public void setUsedStatus(Integer usedStatus) {
		this.usedStatus = usedStatus;
	}

}
