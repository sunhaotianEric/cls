package com.team.lottery.extvo;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import com.team.lottery.util.JaxbUtil;

/**
 * 星彩网接口每行返回数据
 * @author chenhsh
 *
 */
@XmlRootElement(name = "item")
public class LotteryResultForXcwVo{
	
	//期号
	private String expect;
	//开奖号码
	private String opencode;
	//接口返回的开奖时间
	private String opentime;
	
	@XmlAttribute(name = "expect")
	public String getExpect() {
		return expect;
	}
	
	@XmlAttribute(name = "opentime")
	public String getOpentime() {
		return opentime;
	}
	
	@XmlAttribute(name = "opencode")
	public String getOpencode() {
		return opencode;
	}
	
	public void setExpect(String expect) {
		this.expect = expect;
	}
	
	public void setOpencode(String opencode) {
		this.opencode = opencode;
	}
	
	public void setOpentime(String opentime) {
		this.opentime = opentime;
	}
	
	public static void main(String[] args) {

		LotteryResultForXcwVo item1 = new LotteryResultForXcwVo();
		item1.setExpect("2016090612");
		item1.setOpencode("4,9,5,2,2");
		item1.setOpentime("2016-09-06 10:55:56");
		
		LotteryResultForXcwVo item2 = new LotteryResultForXcwVo();
		item2.setExpect("2016090613");
		item2.setOpencode("3,8,5,1,1");
		item2.setOpentime("2016-09-06 11:05:56");

		LotteryResultForXcw resultRoot = new LotteryResultForXcw();
		resultRoot.setLotteryResultVos(new ArrayList<LotteryResultForXcwVo>());
		resultRoot.getLotteryResultVos().add(item1);
		resultRoot.getLotteryResultVos().add(item2);

		String result = JaxbUtil.transformToXml(LotteryResultForKcw.class, resultRoot);
		System.out.println(result);
	}

}
