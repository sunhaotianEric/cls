package com.team.lottery.extvo;

import java.util.Date;

public class UserOperateLogQuery {
	
	private String bizSystem;

    private Integer userId;

    private String userName;

    private String modelName;
    
    private String operateDesc;
    
    private Date logtimeStart;
    
    private Date logtimeEnd;

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getOperateDesc() {
		return operateDesc;
	}

	public void setOperateDesc(String operateDesc) {
		this.operateDesc = operateDesc;
	}

	public Date getLogtimeStart() {
		return logtimeStart;
	}

	public void setLogtimeStart(Date logtimeStart) {
		this.logtimeStart = logtimeStart;
	}

	public Date getLogtimeEnd() {
		return logtimeEnd;
	}

	public void setLogtimeEnd(Date logtimeEnd) {
		this.logtimeEnd = logtimeEnd;
	}

}
