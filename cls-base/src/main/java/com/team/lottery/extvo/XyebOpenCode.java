package com.team.lottery.extvo;

import java.util.Date;

import com.team.lottery.util.DateUtils;
import com.team.lottery.util.XY28NumberUtil;

public class XyebOpenCode {

	//开奖期号
    private String lotteryNum;
    private Date kjtime;
	//开奖号码
	private Integer code1;
	private Integer code2;
	private Integer code3;
	
	public Integer getCode1() {
		return code1;
	}
	public void setCode1(Integer code1) {
		this.code1 = code1;
	}
	public Integer getCode2() {
		return code2;
	}
	public void setCode2(Integer code2) {
		this.code2 = code2;
	}
	public Integer getCode3() {
		return code3;
	}
	public void setCode3(Integer code3) {
		this.code3 = code3;
	}
	
	public String getLotteryNum() {
		return lotteryNum;
	}
	public void setLotteryNum(String lotteryNum) {
		this.lotteryNum = lotteryNum;
	}
	public Date getKjtime() {
		return kjtime;
	}
	public void setKjtime(Date kjtime) {
		this.kjtime = kjtime;
	}
	
	public String getKjtimeStr(){
    	return DateUtils.getDateToStrByFormat(this.getKjtime(), "yyyy/MM/dd HH:mm:ss");
    }
	/**
	 * 总和
	 * @return
	 */
	public Integer getZonghe(){
		return code1+code2+code3;
	}
	
	/**
	 * 大小
	 * @return
	 */
	public String  getDaxiao(){
		 Integer sumCode = getZonghe();
		 Boolean flag = XY28NumberUtil.decideBigOrSmall(sumCode+"");
		 if(flag){
			 return "大";
		 }else{
			 return "小"; 
		 }
	}
	
	/**
	 *  大小 颜色
	 * @return
	 */
	public String getDaxiaoColor(){
		 Integer sumCode = getZonghe();
		 Boolean flag = XY28NumberUtil.decideBigOrSmall(sumCode+"");
		 if(flag){
			 return "red";
		 }else {
			 return "blue"; 
		 }
	}
	
	/**
	 * 单双
	 * @return
	 */
	public String getDanshuang(){
		 Integer sumCode = getZonghe();
		 Boolean flag = XY28NumberUtil.decideSingleOrdouble(sumCode+"");
		 if(flag){
			 return "单";
		 }else{
			 return "双"; 
		 }
	}
	
	/**
	 *  单双 颜色
	 * @return
	 */
	public String getDanshuangColor(){
		 Integer sumCode = getZonghe();
		 Boolean flag = XY28NumberUtil.decideSingleOrdouble(sumCode+"");
		 if(flag){
			 return "red";
		 }else {
			 return "blue"; 
		 }
	}
	
	/**
	 *  极值
	 * @return
	 */
	public String getJizhi(){
		 Integer sumCode = getZonghe();
		 Integer flag = XY28NumberUtil.decideJDOrJX(sumCode+"");
		 if(flag == 1){
			 return "极大";
		 }else if(flag == 2){
			 return "极小"; 
		 }else{
			 return "--";  
		 }
	}
	
	
	/**
	 *  极值 颜色
	 * @return
	 */
	public String getJizhiColor(){
		 Integer sumCode = getZonghe();
		 Integer flag = XY28NumberUtil.decideJDOrJX(sumCode+"");
		 if(flag == 1){
			 return "red";
		 }else if(flag == 2){
			 return "blue"; 
		 }else{
			 return "";  
		 }
	}
	
	/**
	 *  波色
	 * @return
	 */
	public String getBose(){
		 Integer sumCode = getZonghe();
		 Integer flag = XY28NumberUtil.decidePoShe(sumCode+"");
		 if(flag == 1){
			 return "红波";
		 }else if(flag == 2){
			 return "蓝波"; 
		 }else if(flag == 3){
			 return "绿波"; 
		 }else{
			 return "--";  
		 }
	}
	
	/**
	 *  波色 颜色
	 * @return
	 */
	public String getBoseColor(){
		 Integer sumCode = getZonghe();
		 Integer flag = XY28NumberUtil.decidePoShe(sumCode+"");
		 if(flag == 1){
			 return "red";
		 }else if(flag == 2){
			 return "blue"; 
		 }else if(flag == 3){
			 return "green"; 
		 }else{
			 return "";  
		 }
	}
	
	
}
