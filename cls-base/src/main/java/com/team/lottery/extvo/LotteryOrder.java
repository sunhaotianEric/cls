package com.team.lottery.extvo;

import java.util.List;

import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.enums.ELotteryModel;

/**
 * 用户投注信息的扩展vo
 * @author chenhsh
 *
 */
public class LotteryOrder {

	private ELotteryKind lotteryKind; //投注彩种
	
	private String expect;  //投注期号
	
	//投注玩法和号码(dwr框架通过此字段注入内容)
	private String[][] lotteryPlayKindMsg; 
	//投注玩法和号码(spring mvc框架通过此字段注入内容)
	private List<LotteryOrderDetail> playKindMsgs;
	
	private ELotteryModel model; //投注模式,元\角\分
	
    private Integer awardModel; //奖金模式
    
	private Integer isLotteryByPoint; //是否使用积分付款(无用)
	
	private Integer stopAfterNumber;  //是否中奖以后停止追号
	
	private String lotteryId; //投注方案号
	
    private Integer multiple; //投注倍数 
	
	private String bizSystem;
	
	private Long orderId ; //投注订单id
	
	private String remark ; //备注 跟头单 = gt
	
	public String getBizSystem() {
		return bizSystem;
	}
	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}
	
	
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	public ELotteryKind getLotteryKind() {
		return lotteryKind;
	}
	public void setLotteryKind(ELotteryKind lotteryKind) {
		this.lotteryKind = lotteryKind;
	}
	public String[][] getLotteryPlayKindMsg() {
		return lotteryPlayKindMsg;
	}
	public void setLotteryPlayKindMsg(String[][] lotteryPlayKindMsg) {
		this.lotteryPlayKindMsg = lotteryPlayKindMsg;
	}
	public Integer getAwardModel() {
		return awardModel;
	}
	public void setAwardModel(Integer awardModel) {
		this.awardModel = awardModel;
	}
	public ELotteryModel getModel() {
		return model;
	}
	public void setModel(ELotteryModel model) {
		this.model = model;
	}
	public Integer getIsLotteryByPoint() {
		return isLotteryByPoint;
	}
	public void setIsLotteryByPoint(Integer isLotteryByPoint) {
		this.isLotteryByPoint = isLotteryByPoint;
	}
	public String getExpect() {
		return expect;
	}
	public void setExpect(String expect) {
		this.expect = expect;
	}
	public String getLotteryId() {
		return lotteryId;
	}
	public void setLotteryId(String lotteryId) {
		this.lotteryId = lotteryId;
	}
	public Integer getStopAfterNumber() {
		return stopAfterNumber;
	}
	public void setStopAfterNumber(Integer stopAfterNumber) {
		this.stopAfterNumber = stopAfterNumber;
	}
	public Integer getMultiple() {
		return multiple;
	}
	public void setMultiple(Integer multiple) {
		this.multiple = multiple;
	}
	public List<LotteryOrderDetail> getPlayKindMsgs() {
		return playKindMsgs;
	}
	public void setPlayKindMsgs(List<LotteryOrderDetail> playKindMsgs) {
		this.playKindMsgs = playKindMsgs;
	}
}
