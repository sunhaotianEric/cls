package com.team.lottery.extvo;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 开彩网返回结果类
 * @author chenhsh
 *
 */
@XmlRootElement(name = "xml") 
@XmlAccessorType(XmlAccessType.FIELD)
public class LotteryResultForKcw {
	
	@XmlElement(name="row")
	private List<LotteryResultForKcwVo> LotteryResultVos;

	public List<LotteryResultForKcwVo> getLotteryResultVos() {
		return LotteryResultVos;
	}

	public void setLotteryResultVos(List<LotteryResultForKcwVo> lotteryResultVos) {
		LotteryResultVos = lotteryResultVos;
	}
	
}
