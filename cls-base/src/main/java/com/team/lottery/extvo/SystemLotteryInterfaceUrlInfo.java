package com.team.lottery.extvo;

import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.enums.ELotteryKind;

/**
 * 开奖接口的URL地址
 * @author gs
 *
 */
public class SystemLotteryInterfaceUrlInfo {
	
	private static Logger logger = LoggerFactory.getLogger(SystemLotteryInterfaceUrlInfo.class);
	
	//接口类型  正式的
	public static final Integer INTERFACE_TYPE_FORMAL = 0;
	//接口类型 备用的
	public static final Integer INTERFACE_TYPE_BACK = 1;

	public static String CQSSC;
	public static String CQSSC_BACKUP;
	
	public static String JXSSC;
	public static String JXSSC_BACKUP;

	public static String TJSSC;
	public static String TJSSC_BACKUP;
	
	public static String XJSSC;
	public static String XJSSC_BACKUP;
	
	public static String SDSYXW;
	public static String SDSYXW_BACKUP;
	
	public static String JXSYXW;
	public static String JXSYXW_BACKUP;
	
	public static String FJSYXW;
	public static String FJSYXW_BACKUP;
	
	public static String GDSYXW;
	public static String GDSYXW_BACKUP;
	
	public static String FCSDDPC;
	public static String FCSDDPC_BACKUP;
	
	public static String JSKS;
	public static String JSKS_BACKUP;
	
	public static String BJPK10;
	public static String BJPK10_BACKUP;
	
	public static String HGFFC;
	public static String HGFFC_BACKUP;
	
	public static String AHKS;
	public static String AHKS_BACKUP;
	
	public static String HBKS;
	public static String HBKS_BACKUP;
	
	public static String JLKS;
	public static String JLKS_BACKUP;
	
	public static String BJKS;
	public static String BJKS_BACKUP;
	
	public static String XJPLFC;
	public static String XJPLFC_BACKUP;
	
	public static String TWWFC;
	public static String TWWFC_BACKUP;
	
	public static String LHC;
	public static String LHC_BACKUP;
	public static String AMLHC;
	public static String AMLHC_BACKUP;

	//从配置文件加载开奖接口url信息
	public static void init() {
		Properties pro = new Properties();   
		try {  
			logger.info("开始加载开奖接口url信息...");
			InputStream in = SystemLotteryInterfaceUrlInfo.class.getResourceAsStream("/kaijiang.properties");   
            pro.load(in);  
            
			CQSSC = (String) pro.get("CQSSC");
			CQSSC_BACKUP = (String) pro.get("CQSSC_BACKUP");
			
			JXSSC = (String) pro.get("JXSSC");
			JXSSC_BACKUP = (String) pro.get("JXSSC_BACKUP");
	
			TJSSC = (String) pro.get("TJSSC");
			TJSSC_BACKUP = (String) pro.get("TJSSC_BACKUP");
			
			XJSSC = (String) pro.get("XJSSC");
			XJSSC_BACKUP = (String) pro.get("XJSSC_BACKUP");
			
			SDSYXW = (String) pro.get("SDSYXW");
			SDSYXW_BACKUP = (String) pro.get("SDSYXW_BACKUP");
			
			JXSYXW = (String) pro.get("JXSYXW");
			JXSYXW_BACKUP = (String) pro.get("JXSYXW_BACKUP");
			
			FJSYXW = (String) pro.get("FJSYXW");
			FJSYXW_BACKUP = (String) pro.get("FJSYXW_BACKUP");
			
			GDSYXW = (String) pro.get("GDSYXW");
			GDSYXW_BACKUP = (String) pro.get("GDSYXW_BACKUP");
			
			FCSDDPC = (String) pro.get("FCSDDPC");
			FCSDDPC_BACKUP = (String) pro.get("FCSDDPC_BACKUP");
			
			BJPK10 = (String) pro.get("BJPK10");
			BJPK10_BACKUP = (String) pro.get("BJPK10_BACKUP");
			
			HGFFC = (String) pro.get("HGFFC");
			HGFFC_BACKUP = (String) pro.get("HGFFC_BACKUP");
			
			XJPLFC = (String) pro.get("XJPLFC");
			XJPLFC_BACKUP = (String) pro.get("XJPLFC_BACKUP");
			
			TWWFC = (String) pro.get("TWWFC");
			TWWFC_BACKUP = (String) pro.get("TWWFC_BACKUP");
			
			JSKS = (String) pro.get("JSKS");
			JSKS_BACKUP = (String) pro.get("JSKS_BACKUP");
			
			AHKS = (String) pro.get("AHKS");
			AHKS_BACKUP = (String) pro.get("AHKS_BACKUP");
			
			HBKS = (String) pro.get("HBKS");
			HBKS_BACKUP = (String) pro.get("HBKS_BACKUP");
			
			JLKS = (String) pro.get("JLKS");
			JLKS_BACKUP = (String) pro.get("JLKS_BACKUP");
			
			BJKS = (String) pro.get("BJKS");
			BJKS_BACKUP = (String) pro.get("BJKS_BACKUP");
			
			LHC = (String) pro.get("LHC");
			LHC_BACKUP = (String) pro.get("LHC_BACKUP");

			AMLHC = (String) pro.get("AMLHC");
			AMLHC_BACKUP = (String) pro.get("AMLHC_BACKUP");
			
			logger.info("加载开奖接口url信息结束...");
		}catch (Exception e) {   
        	logger.error("加载开奖接口url信息出错", e);   
        }  
	}
	
	/**
	 * 根据彩种类型获取接口的url地址
	 * @param lotteryKind
	 * @param interfaceType  0正式  1备用
	 * @return
	 */
	public static String getUrlByLotteryKind(ELotteryKind lotteryKind, Integer interfaceType) {
		String res = "";
		if(lotteryKind != null) {
			if(ELotteryKind.CQSSC.getCode().equals(lotteryKind.getCode())) {
				if(interfaceType == null || interfaceType == INTERFACE_TYPE_FORMAL) {
					res = CQSSC;
				} else {
					res = CQSSC_BACKUP;
				}
			} else if(ELotteryKind.JXSSC.getCode().equals(lotteryKind.getCode())) {
				if(interfaceType == null || interfaceType == INTERFACE_TYPE_FORMAL) {
					res = JXSSC;
				} else {
					res = JXSSC_BACKUP;
				}
			} else if(ELotteryKind.TJSSC.getCode().equals(lotteryKind.getCode())) {
				if(interfaceType == null || interfaceType == INTERFACE_TYPE_FORMAL) {
					res = TJSSC;
				} else {
					res = TJSSC_BACKUP;
				}
			} else if(ELotteryKind.XJSSC.getCode().equals(lotteryKind.getCode())) {
				if(interfaceType == null || interfaceType == INTERFACE_TYPE_FORMAL) {
					res = XJSSC;
				} else {
					res = XJSSC_BACKUP;
				}
			} else if(ELotteryKind.SDSYXW.getCode().equals(lotteryKind.getCode())) {
				if(interfaceType == null || interfaceType == INTERFACE_TYPE_FORMAL) {
					res = SDSYXW;
				} else {
					res = SDSYXW_BACKUP;
				}
			} else if(ELotteryKind.JXSYXW.getCode().equals(lotteryKind.getCode())) {
				if(interfaceType == null || interfaceType == INTERFACE_TYPE_FORMAL) {
					res = JXSYXW;
				} else {
					res = JXSYXW_BACKUP;
				}
			} else if(ELotteryKind.FJSYXW.getCode().equals(lotteryKind.getCode())) {
				if(interfaceType == null || interfaceType == INTERFACE_TYPE_FORMAL) {
					res = FJSYXW;
				} else {
					res = FJSYXW_BACKUP;
				}
			} else if(ELotteryKind.GDSYXW.getCode().equals(lotteryKind.getCode())) {
				if(interfaceType == null || interfaceType == INTERFACE_TYPE_FORMAL) {
					res = GDSYXW;
				} else {
					res = GDSYXW_BACKUP;
				}
			} else if(ELotteryKind.FCSDDPC.getCode().equals(lotteryKind.getCode())) {
				if(interfaceType == null || interfaceType == INTERFACE_TYPE_FORMAL) {
					res = FCSDDPC;
				} else {
					res = FCSDDPC_BACKUP;
				}
			} else if(ELotteryKind.BJPK10.getCode().equals(lotteryKind.getCode())) {
				if(interfaceType == null || interfaceType == INTERFACE_TYPE_FORMAL) {
					res = BJPK10;
				} else {
					res = BJPK10_BACKUP;
				}
			} else if(ELotteryKind.HGFFC.getCode().equals(lotteryKind.getCode())) {
				if(interfaceType == null || interfaceType == INTERFACE_TYPE_FORMAL) {
					res = HGFFC;
				} else {
					res = HGFFC_BACKUP;
				}
			} else if(ELotteryKind.TWWFC.getCode().equals(lotteryKind.getCode())) {
				if(interfaceType == null || interfaceType == INTERFACE_TYPE_FORMAL) {
					res = TWWFC;
				} else {
					res = TWWFC_BACKUP;
				}
			} else if(ELotteryKind.XJPLFC.getCode().equals(lotteryKind.getCode())) {
				if(interfaceType == null || interfaceType == INTERFACE_TYPE_FORMAL) {
					res = XJPLFC;
				} else {
					res = XJPLFC_BACKUP;
				}
			} else if(ELotteryKind.JSKS.getCode().equals(lotteryKind.getCode())) {
				if(interfaceType == null || interfaceType == INTERFACE_TYPE_FORMAL) {
					res = JSKS;
				} else {
					res = JSKS_BACKUP;
				}
			} else if(ELotteryKind.AHKS.getCode().equals(lotteryKind.getCode())) {
				if(interfaceType == null || interfaceType == INTERFACE_TYPE_FORMAL) {
					res = AHKS;
				} else {
					res = AHKS_BACKUP;
				}
			} else if(ELotteryKind.BJKS.getCode().equals(lotteryKind.getCode())) {
				if(interfaceType == null || interfaceType == INTERFACE_TYPE_FORMAL) {
					res = BJKS;
				} else {
					res = BJKS_BACKUP;
				}
			} else if(ELotteryKind.HBKS.getCode().equals(lotteryKind.getCode())) {
				if(interfaceType == null || interfaceType == INTERFACE_TYPE_FORMAL) {
					res = HBKS;
				} else {
					res = HBKS_BACKUP;
				}
			} else if(ELotteryKind.JLKS.getCode().equals(lotteryKind.getCode())) {
				if(interfaceType == null || interfaceType == INTERFACE_TYPE_FORMAL) {
					res = JLKS;
				} else {
					res = JLKS_BACKUP;
				}
			}else if(ELotteryKind.LHC.getCode().equals(lotteryKind.getCode())) {
				if(interfaceType == null || interfaceType == INTERFACE_TYPE_FORMAL) {
					res = LHC;
				} else {
					res = LHC_BACKUP;
				}
			}else if(ELotteryKind.AMLHC.getCode().equals(lotteryKind.getCode())) {
				if(interfaceType == null || interfaceType == INTERFACE_TYPE_FORMAL) {
					res = AMLHC;
				} else {
					res = AMLHC_BACKUP;
				}
			}
		}
		return res;
	}

}
