package com.team.lottery.extvo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 团队查询vo
 * @author chenhsh
 *
 */
public class TeamUserQuery {

	private String teamLeaderName; //团队领导人名字
	private String userName; //用户名
	private String bizSystem;//业务系统
	private Date registerDateStart; //注册时间
	private Date registerDateEnd; //注册时间
	private BigDecimal balanceStart; //余额
	private BigDecimal balanceEnd; //余额
	private Boolean isQuerySub = false;  //是否同时查询下级用户
	private String dailiLevel; //代理级别
	private Boolean isOnline; //是否在线
	private List<String> onlineUserNames;  //在线会员名称
	private List<String> dailiLevels; //多个代理级别查询
	private String onlineType; //在线方式
	
	private Integer orderSort;  //排序顺序

	private Boolean isQueryLike = false; //用户名查询是否使用模糊查询
	/**
     * 是否游客 0否  1是
     */
    private Integer isTourist;
	private Integer hasBonus;//有签契约的最小状态
	public String getTeamLeaderName() {
		return teamLeaderName;
	}
	public void setTeamLeaderName(String teamLeaderName) {
		this.teamLeaderName = teamLeaderName;
	}
	public Integer getIsTourist() {
		return isTourist;
	}
	public void setIsTourist(Integer isTourist) {
		this.isTourist = isTourist;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Date getRegisterDateStart() {
		return this.registerDateStart;
	}
	public void setRegisterDateStart(Date registerDateStart) {
		this.registerDateStart = registerDateStart;
	}
	public Date getRegisterDateEnd() {
		return registerDateEnd;
	}
	public void setRegisterDateEnd(Date registerDateEnd) {
		this.registerDateEnd = registerDateEnd;
	}
	public BigDecimal getBalanceStart() {
		return balanceStart;
	}
	public void setBalanceStart(BigDecimal balanceStart) {
		this.balanceStart = balanceStart;
	}
	public BigDecimal getBalanceEnd() {
		return balanceEnd;
	}
	public void setBalanceEnd(BigDecimal balanceEnd) {
		this.balanceEnd = balanceEnd;
	}
	public String getDailiLevel() {
		return dailiLevel;
	}
	public void setDailiLevel(String dailiLevel) {
		this.dailiLevel = dailiLevel;
	}
	public Boolean getIsOnline() {
		return isOnline;
	}
	public void setIsOnline(Boolean isOnline) {
		this.isOnline = isOnline;
	}
	public List<String> getOnlineUserNames() {
		return onlineUserNames;
	}
	public void setOnlineUserNames(List<String> onlineUserNames) {
		this.onlineUserNames = onlineUserNames;
	}
	public Integer getOrderSort() {
		return orderSort;
	}
	public void setOrderSort(Integer orderSort) {
		this.orderSort = orderSort;
	}
	public Boolean getIsQuerySub() {
		return isQuerySub;
	}
	public void setIsQuerySub(Boolean isQuerySub) {
		this.isQuerySub = isQuerySub;
	}
	public Boolean getIsQueryLike() {
		return isQueryLike;
	}
	public void setIsQueryLike(Boolean isQueryLike) {
		this.isQueryLike = isQueryLike;
	}
	public List<String> getDailiLevels() {
		return dailiLevels;
	}
	public void setDailiLevels(List<String> dailiLevels) {
		this.dailiLevels = dailiLevels;
	}
	public String getOnlineType() {
		return onlineType;
	}
	public void setOnlineType(String onlineType) {
		this.onlineType = onlineType;
	}
	public String getBizSystem() {
		return bizSystem;
	}
	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}
	public Integer getHasBonus() {
		return hasBonus;
	}
	public void setHasBonus(Integer hasBonus) {
		this.hasBonus = hasBonus;
	}
	
	
}
