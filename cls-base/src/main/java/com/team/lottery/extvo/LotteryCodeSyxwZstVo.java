package com.team.lottery.extvo;

import java.util.HashMap;

import com.team.lottery.util.ConstantUtil;
import com.team.lottery.util.StringUtils;

public class LotteryCodeSyxwZstVo {

    //开奖期号
    private String lotteryNum;

    private String numInfo1;

    private String numInfo2;

    private String numInfo3;
    
    private String numInfo4;
    
    private String numInfo5;
	
    /**
	 * 万位
	 */
    private HashMap<String, Integer> wanNumMateCurrent;
    
    /**
	 * 千位
	 */
    private HashMap<String, Integer> qianNumMateCurrent;
    
    /**
	 * 百位
	 */
    private HashMap<String, Integer> baiNumMateCurrent;
    
    /**
	 * 十位
	 */
    private HashMap<String, Integer> shiNumMateCurrent;
    
    /**
	 * 个位
	 */
    private HashMap<String, Integer> geNumMateCurrent;
    
    
    public String getLotteryNum() {
        return lotteryNum;
    }

    public void setLotteryNum(String lotteryNum) {
        this.lotteryNum = lotteryNum == null ? null : lotteryNum.trim();
    }

    public String getNumInfo1() {
        return numInfo1;
    }

    public void setNumInfo1(String numInfo1) {
        this.numInfo1 = numInfo1 == null ? null : numInfo1.trim();
    }

    public String getNumInfo2() {
        return numInfo2;
    }

    public void setNumInfo2(String numInfo2) {
        this.numInfo2 = numInfo2 == null ? null : numInfo2.trim();
    }

    public String getNumInfo3() {
        return numInfo3;
    }

    public void setNumInfo3(String numInfo3) {
        this.numInfo3 = numInfo3 == null ? null : numInfo3.trim();
    }

	public String getNumInfo4() {
		return numInfo4;
	}

	public void setNumInfo4(String numInfo4) {
		this.numInfo4 = numInfo4;
	}

	public String getNumInfo5() {
		return numInfo5;
	}

	public void setNumInfo5(String numInfo5) {
		this.numInfo5 = numInfo5;
	}

	
	
	public HashMap<String, Integer> getWanNumMateCurrent() {
		return wanNumMateCurrent;
	}

	public void setWanNumMateCurrent(HashMap<String, Integer> wanNumMateCurrent) {
		this.wanNumMateCurrent = wanNumMateCurrent;
	}

	public HashMap<String, Integer> getQianNumMateCurrent() {
		return qianNumMateCurrent;
	}

	public void setQianNumMateCurrent(HashMap<String, Integer> qianNumMateCurrent) {
		this.qianNumMateCurrent = qianNumMateCurrent;
	}

	public HashMap<String, Integer> getBaiNumMateCurrent() {
		return baiNumMateCurrent;
	}

	public void setBaiNumMateCurrent(HashMap<String, Integer> baiNumMateCurrent) {
		this.baiNumMateCurrent = baiNumMateCurrent;
	}

	public HashMap<String, Integer> getShiNumMateCurrent() {
		return shiNumMateCurrent;
	}

	public void setShiNumMateCurrent(HashMap<String, Integer> shiNumMateCurrent) {
		this.shiNumMateCurrent = shiNumMateCurrent;
	}

	public HashMap<String, Integer> getGeNumMateCurrent() {
		return geNumMateCurrent;
	}

	public void setGeNumMateCurrent(HashMap<String, Integer> geNumMateCurrent) {
		this.geNumMateCurrent = geNumMateCurrent;
	}

	/**
	 * 获取开奖号码的拼接
	 * @return
	 */
	public String getOpencodeStr() {
		StringBuffer sb = new StringBuffer();
		if(!StringUtils.isEmpty(numInfo1)) {
			sb.append(numInfo1.trim()).append(ConstantUtil.OPENCODE_PLIT);
		}
		if(!StringUtils.isEmpty(numInfo2)) {
			sb.append(numInfo2.trim()).append(ConstantUtil.OPENCODE_PLIT);
		}
		if(!StringUtils.isEmpty(numInfo3)) {
			sb.append(numInfo3.trim()).append(ConstantUtil.OPENCODE_PLIT);
		}
		if(!StringUtils.isEmpty(numInfo4)) {
			sb.append(numInfo4.trim()).append(ConstantUtil.OPENCODE_PLIT);
		}
		if(!StringUtils.isEmpty(numInfo5)) {
			sb.append(numInfo5.trim()).append(ConstantUtil.OPENCODE_PLIT);
		}
		
		if(sb.length() != 0){
			sb.deleteCharAt(sb.length() - 1);
		}
		
		//删除最后一个分隔符
		return sb.toString();
	}
}