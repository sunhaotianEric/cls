package com.team.lottery.extvo;

import java.util.List;

/**
 * 发送消息vo
 * @author chenhsh
 *
 */
public class NoteVo {

    private String sub; //主题
    private String body;//内容
    private List<String> toUserNames; //需要发送群

    private String sendType; //to_up,to_down

	public String getSub() {
		return sub;
	}

	public void setSub(String sub) {
		this.sub = sub;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getSendType() {
		return sendType;
	}

	public void setSendType(String sendType) {
		this.sendType = sendType;
	}

	public List<String> getToUserNames() {
		return toUserNames;
	}

	public void setToUserNames(List<String> toUserNames) {
		this.toUserNames = toUserNames;
	}

}
