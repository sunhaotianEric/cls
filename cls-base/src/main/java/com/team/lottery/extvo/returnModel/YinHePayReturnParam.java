package com.team.lottery.extvo.returnModel;
/**
 * 北付宝支付返回对象
 * @author Jamine
 *
 */
public class YinHePayReturnParam {
	private String order_id;
	
	private String paysapi_id;
	
	private String price;
	
	private String real_price;
	
	private String mark;
	
	private String sign;
	
	private String code;
	
	private String is_type;

	public String getOrder_id() {
		return order_id;
	}

	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}

	public String getPaysapi_id() {
		return paysapi_id;
	}

	public void setPaysapi_id(String paysapi_id) {
		this.paysapi_id = paysapi_id;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getReal_price() {
		return real_price;
	}

	public void setReal_price(String real_price) {
		this.real_price = real_price;
	}

	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getIs_type() {
		return is_type;
	}

	public void setIs_type(String is_type) {
		this.is_type = is_type;
	}
	
	
	
	
	
}
