package com.team.lottery.extvo.returnModel;

public class ShunFuReturnPayUrl {
	private String merNo;
	
	private String resultCode;
	
	private String resultMsg;
	
	private String orderNo;
	
	private String qrcodeInfo;
	
	private String sign;

	public String getMerNo() {
		return merNo;
	}

	public void setMerNo(String merNo) {
		this.merNo = merNo;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getResultMsg() {
		return resultMsg;
	}

	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getQrcodeInfo() {
		return qrcodeInfo;
	}

	public void setQrcodeInfo(String qrcodeInfo) {
		this.qrcodeInfo = qrcodeInfo;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}
	
	
	
}
