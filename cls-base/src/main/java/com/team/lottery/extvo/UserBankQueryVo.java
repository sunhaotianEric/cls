package com.team.lottery.extvo;

import java.util.Date;

public class UserBankQueryVo {
    private String bankAccountName;
    private String bankCardNum;
	private String userName;
	private String bizSystem;
	private Integer withDrawId;
    private Date belongDateStart;
    private Integer userId;
	
	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getBankAccountName() {
		return bankAccountName;
	}

	public void setBankAccountName(String bankAccountName) {
		this.bankAccountName = bankAccountName;
	}

	public String getBankCardNum() {
		return bankCardNum;
	}

	public void setBankCardNum(String bankCardNum) {
		this.bankCardNum = bankCardNum;
	}

	public Integer getWithDrawId() {
		return withDrawId;
	}

	public void setWithDrawId(Integer withDrawId) {
		this.withDrawId = withDrawId;
	}

	public Date getBelongDateStart() {
		return belongDateStart;
	}

	public void setBelongDateStart(Date belongDateStart) {
		this.belongDateStart = belongDateStart;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	

	
    

	
}
