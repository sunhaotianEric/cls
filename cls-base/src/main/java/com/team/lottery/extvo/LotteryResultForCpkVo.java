package com.team.lottery.extvo;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

import com.team.lottery.util.JaxbUtil;

/**
 * 彩票控接口每行返回数据
 * 
 * @author gs
 *
 */
@XmlRootElement(name = "item")
public class LotteryResultForCpkVo {

	// 期号
	private String expect;
	// 开奖号码
	private String opencode;
	// 接口返回的开奖时间
	private String opentime;

	@XmlAttribute(name = "id")
	public String getExpect() {
		return this.expect;
	}

	@XmlAttribute(name = "dateline")
	public String getOpentime() {
		return this.opentime;
	}

	@XmlValue
	public String getOpencode() {
		return this.opencode;
	}

	public void setExpect(String expect) {
		this.expect = expect;
	}

	public void setOpencode(String opencode) {
		this.opencode = opencode;
	}

	public void setOpentime(String opentime) {
		this.opentime = opentime;
	}

	public static void main(String[] args) {

		LotteryResultForCpkVo item1 = new LotteryResultForCpkVo();
		item1.setExpect("2016090612");
		item1.setOpencode("4,9,5,2,2");
		item1.setOpentime("2016-09-06 10:55:56");
		
		LotteryResultForCpkVo item2 = new LotteryResultForCpkVo();
		item2.setExpect("2016090613");
		item2.setOpencode("3,8,5,1,1");
		item2.setOpentime("2016-09-06 11:05:56");

		LotteryResultForCpk resultRoot = new LotteryResultForCpk();
		resultRoot.setLotteryResultVos(new ArrayList<LotteryResultForCpkVo>());
		resultRoot.getLotteryResultVos().add(item1);
		resultRoot.getLotteryResultVos().add(item2);

		String result = JaxbUtil.transformToXml(LotteryResultForCpk.class, resultRoot);
		System.out.println(result);
	}
}
