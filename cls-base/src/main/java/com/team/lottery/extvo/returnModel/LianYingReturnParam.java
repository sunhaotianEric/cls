package com.team.lottery.extvo.returnModel;
/**
 * 联赢支付返回对象
 * @author Jamine
 *
 */
public class LianYingReturnParam {
	//平台订单号
	private String orderId;
	//状态(S成功,F失败)
	private String  status;
	//时间戳
	private String time;
	//加密方式
	private String signType;
	//商户号
	private String merchantId;
	//金额,单位分!
	private String amount;
	//商户订单号
	private String poId;
	//签名
	private String sign;
	
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getSignType() {
		return signType;
	}
	public void setSignType(String signType) {
		this.signType = signType;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getPoId() {
		return poId;
	}
	public void setPoId(String poId) {
		this.poId = poId;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	
}
