package com.team.lottery.extvo;

/**
 * 调整倒计时时间
 * 
 *
 */
public class LotteryIssueAdjustVo {

    private String lotteryType;
    
    private Integer adjustmenTime;
    //0 延迟， 1 提前
    private Integer adjustMode;


	public String getLotteryType() {
		return lotteryType;
	}

	public void setLotteryType(String lotteryType) {
		this.lotteryType = lotteryType;
	}
	public Integer getAdjustmenTime() {
		return adjustmenTime;
	}

	public void setAdjustmenTime(Integer adjustmenTime) {
		this.adjustmenTime = adjustmenTime;
	}

	public Integer getAdjustMode() {
		return adjustMode;
	}

	public void setAdjustMode(Integer adjustMode) {
		this.adjustMode = adjustMode;
	}

}
