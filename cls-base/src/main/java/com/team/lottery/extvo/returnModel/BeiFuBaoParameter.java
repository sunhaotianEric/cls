package com.team.lottery.extvo.returnModel;

public class BeiFuBaoParameter {
	
	private String orderStatus;
	
	private String content;
	
	private String lxbOrderNumber;

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getLxbOrderNumber() {
		return lxbOrderNumber;
	}

	public void setLxbOrderNumber(String lxbOrderNumber) {
		this.lxbOrderNumber = lxbOrderNumber;
	}
	
	
}
	
	
	
	

	