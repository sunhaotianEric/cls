package com.team.lottery.extvo;

import java.util.Date;

public class UserHourConsumeQuery {

	//用户名
	private String userName;  
	private String bizSystem;
	//团队领导人名字
	private String teamLeaderName; //包含推荐人的拼接 
	private String realLeaderName; //真是的领导人名字
	
	//资金起始时间
    private Date createdDateStart;
    //资金结束时间
    private Date createdDateEnd;
    //是否在包含=号
    private Boolean isInclude;
    
    
	public String getTeamLeaderName() {
		return teamLeaderName;
	}
	public void setTeamLeaderName(String teamLeaderName) {
		this.teamLeaderName = teamLeaderName;
	}
	public String getRealLeaderName() {
		return realLeaderName;
	}
	public void setRealLeaderName(String realLeaderName) {
		this.realLeaderName = realLeaderName;
	}
	public Date getCreatedDateStart() {
		return createdDateStart;
	}
	public void setCreatedDateStart(Date createdDateStart) {
		this.createdDateStart = createdDateStart;
	}
	public Date getCreatedDateEnd() {
		return createdDateEnd;
	}
	public void setCreatedDateEnd(Date createdDateEnd) {
		this.createdDateEnd = createdDateEnd;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getBizSystem() {
		return bizSystem;
	}
	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}
	public Boolean getIsInclude() {
		return isInclude;
	}
	public void setIsInclude(Boolean isInclude) {
		this.isInclude = isInclude;
	}
    
    
}
