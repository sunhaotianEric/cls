package com.team.lottery.extvo;

import java.util.Date;

public class UserDayMoneyQuery {

	//团队领导人名字
	private String teamLeaderName; //包含推荐人的拼接 
	private String realLeaderName; //真是的领导人名字
	
	//资金起始时间
    private Date createdDateStart;
    //资金结束时间
    private Date createdDateEnd;
    
	public String getTeamLeaderName() {
		return teamLeaderName;
	}
	public void setTeamLeaderName(String teamLeaderName) {
		this.teamLeaderName = teamLeaderName;
	}
	public String getRealLeaderName() {
		return realLeaderName;
	}
	public void setRealLeaderName(String realLeaderName) {
		this.realLeaderName = realLeaderName;
	}
	public Date getCreatedDateStart() {
		return createdDateStart;
	}
	public void setCreatedDateStart(Date createdDateStart) {
		this.createdDateStart = createdDateStart;
	}
	public Date getCreatedDateEnd() {
		return createdDateEnd;
	}
	public void setCreatedDateEnd(Date createdDateEnd) {
		this.createdDateEnd = createdDateEnd;
	}
    
    
}
