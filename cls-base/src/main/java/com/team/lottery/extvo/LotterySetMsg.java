package com.team.lottery.extvo;

import com.team.lottery.util.ConstantUtil;

/**
 * 彩种是否开启的扩展vo
 * @author chenhsh
 *
 */
public class LotterySetMsg {
	
	//重庆时时彩是否开启
	private Integer isCQSSCOpen;	
	//江西时时彩是否开启
	private Integer isJXSSCOpen;	
	//黑龙江时时彩是否开启
	private Integer isHLJSSCOpen;	
	//新疆时时彩是否开启
	private Integer isXJSSCOpen;	
	//天津时时彩是否开启
	private Integer isTJSSCOpen;	
	
	
	//老重庆时时彩是否开启
	private Integer isLCQSSCOpen;
	//十分时时彩是否开启
	private Integer isSHFSSCOpen;	
	//三分时时彩是否开启
	private Integer isSFSSCOpen;	
	//五分时时彩是否开启
	private Integer isWFSSCOpen;	
	//吉利分分彩是否开启
	private Integer isJLFFCOpen;
	//韩国1.5分彩是否开启
	private Integer isHGFFCOpen;
	
	//新加坡2分彩是否开启
	private Integer isXJPLFCOpen;
	//台湾5分彩是否开启
	private Integer isTWWFCOpen;
	
	//广东11选5是否开启
	private Integer isGDSYXWOpen;
	//山东11选5是否开启
	private Integer isSDSYXWOpen;
	//江西11选5是否开启
	private Integer isJXSYXWOpen;
	//重庆11选5是否开启
	private Integer isCQSYXWOpen;
	//福建11选5是否开启
	private Integer isFJSYXWOpen;
	//五分11选5是否开启
	private Integer isWFSYXWOpen;
	//三分11选5是否开启
	private Integer isSFSYXWOpen;
	
	//三分快3是否开启
	private Integer isSFKSOpen;
	//五分快3是否开启
	private Integer isWFKSOpen;
	//江苏快3是否开启
	private Integer isJSKSOpen;
	//安徽快3是否开启
	private Integer isAHKSOpen;
	//吉林快3是否开启
	private Integer isJLKSOpen;
	//湖北快3是否开启
	private Integer isHBKSOpen;
	//北京快3是否开启
	private Integer isBJKSOpen;
	//幸运快3是否开启
	private Integer isJYKSOpen;
	//广西快3是否开启
	private Integer isGXKSOpen;
	// 甘肃快3是否开启
	private Integer isGSKSOpen;
	// 上海快3是否开启
	private Integer isSHKSOpen;
	
	//广东快乐十分是否开启
	private Integer isGDKLSFOpen;		
	
    //上海时时乐是否开启
	public Integer isSHSSLDPCOpen;
	//福彩3D是否开启 
	public Integer isFC3DDPCOpen;
	
	//六合彩是否开启 
	public Integer isLHCOpen;
	
	//幸运飞艇是否开启
	public Integer isXYFTOpen;
	//三分PK10是否开启
	public Integer isSFPK10Open;
	//五分PK10是否开启
	public Integer isWFPK10Open;
	//十分PK10是否开启
	public Integer isSHFPK10Open;
	//北京PK10是否开启
	public Integer isBJPK10Open;
	//极速PK10是否开启
	public Integer isJSPK10Open;
	
	//江苏骰宝是否开启
	private Integer isJSTBOpen;
	//安徽骰宝是否开启
	private Integer isAHTBOpen;
	//吉林骰宝是否开启
	private Integer isJLTBOpen;
	//湖北骰宝是否开启
	private Integer isHBTBOpen;
	//北京骰宝是否开启
	private Integer isBJTBOpen;
	//北京骰宝是否开启
	private Integer isXYEBOpen;
	//幸运六合彩是否开启 
	public  Integer isXYLHCOpen;
	/**
	 * 解析彩种开关的字符串
	 * @param setsStr
	 */
	public void parseLotterySet(String setsStr) throws Exception {
		String[] setts = setsStr.split(ConstantUtil.SYSTEM_SETTING_JOIN);

		if (setts.length != 30) {
			throw new Exception("系统信息数据配置不正确，长度已经改变.");
		}
		this.setIsCQSSCOpen(Integer.parseInt(setts[0])); // 重庆时时彩
		this.setIsJXSSCOpen(Integer.parseInt(setts[1])); // 江西时时彩
		this.setIsHLJSSCOpen(Integer.parseInt(setts[2])); // 黑龙江时时彩
		this.setIsXJSSCOpen(Integer.parseInt(setts[3])); // 新疆时时彩

		this.setIsJLFFCOpen(Integer.parseInt(setts[4])); // 吉利分分彩

		this.setIsJSKSOpen(Integer.parseInt(setts[5])); // 江苏快3

		this.setIsGDKLSFOpen(Integer.parseInt(setts[6])); // 广东快乐十分

		this.setIsGDSYXWOpen(Integer.parseInt(setts[7])); // 广东11选5

		this.setIsSHSSLDPCOpen(Integer.parseInt(setts[8])); // 上海时时乐
		this.setIsFC3DDPCOpen(Integer.parseInt(setts[9])); // 福彩3D

		this.setIsSDSYXWOpen(Integer.parseInt(setts[10])); // 山东11选5
		this.setIsJXSYXWOpen(Integer.parseInt(setts[11])); // 江西11选5
		this.setIsCQSYXWOpen(Integer.parseInt(setts[12])); // 重庆11选5

		this.setIsTJSSCOpen(Integer.parseInt(setts[13])); // 天津时时彩
		this.setIsFJSYXWOpen(Integer.parseInt(setts[14])); // 福建11选5
		
		this.setIsBJPK10Open(Integer.parseInt(setts[15])); // 北京pk10
		
		this.setIsHGFFCOpen(Integer.parseInt(setts[16])); // 韩国1.5分彩
		
		this.setIsAHKSOpen(Integer.parseInt(setts[17])); // 安徽快3
		this.setIsJLKSOpen(Integer.parseInt(setts[18])); // 吉林快3
		this.setIsHBKSOpen(Integer.parseInt(setts[19])); // 湖北快3
		
		this.setIsJSTBOpen(Integer.parseInt(setts[20])); // 江苏骰宝
		this.setIsAHTBOpen(Integer.parseInt(setts[21])); // 安徽骰宝
		this.setIsJLTBOpen(Integer.parseInt(setts[22])); //	吉林骰宝
		this.setIsHBTBOpen(Integer.parseInt(setts[23])); // 湖北骰宝
		
		this.setIsJLTBOpen(Integer.parseInt(setts[24])); //	北京快三
		this.setIsHBTBOpen(Integer.parseInt(setts[25])); // 北京骰宝
		
		this.setIsXJPLFCOpen(Integer.parseInt(setts[26])); // 新加坡2分彩
		this.setIsTWWFCOpen(Integer.parseInt(setts[27])); // 台湾5分彩
		this.setIsLHCOpen(Integer.parseInt(setts[28]));//六合彩
		this.setIsXYEBOpen(Integer.parseInt(setts[29]));//幸运28
		this.setIsGXKSOpen(Integer.parseInt(setts[30]));//广西快三
		this.setIsGSKSOpen(Integer.parseInt(setts[31]));
		this.setIsSHKSOpen(Integer.parseInt(setts[32]));
		this.setIsXYLHCOpen(Integer.parseInt(setts[33]));
	}
	
	

	public Integer getIsLHCOpen() {
		return isLHCOpen;
	}
	public void setIsLHCOpen(Integer isLHCOpen) {
		this.isLHCOpen = isLHCOpen;
	}
	public Integer getIsXYEBOpen() {
		return isXYEBOpen;
	}
	public void setIsXYEBOpen(Integer isXYEBOpen) {
		this.isXYEBOpen = isXYEBOpen;
	}
	public Integer getIsCQSSCOpen() {
		return isCQSSCOpen;
	}
	public void setIsCQSSCOpen(Integer isCQSSCOpen) {
		this.isCQSSCOpen = isCQSSCOpen;
	}

	public Integer getIsJXSSCOpen() {
		return isJXSSCOpen;
	}

	public void setIsJXSSCOpen(Integer isJXSSCOpen) {
		this.isJXSSCOpen = isJXSSCOpen;
	}

	public Integer getIsHLJSSCOpen() {
		return isHLJSSCOpen;
	}

	public void setIsHLJSSCOpen(Integer isHLJSSCOpen) {
		this.isHLJSSCOpen = isHLJSSCOpen;
	}

	public Integer getIsXJSSCOpen() {
		return isXJSSCOpen;
	}

	public void setIsXJSSCOpen(Integer isXJSSCOpen) {
		this.isXJSSCOpen = isXJSSCOpen;
	}

	public Integer getIsTJSSCOpen() {
		return isTJSSCOpen;
	}

	public void setIsTJSSCOpen(Integer isTJSSCOpen) {
		this.isTJSSCOpen = isTJSSCOpen;
	}

	public Integer getIsJLFFCOpen() {
		return isJLFFCOpen;
	}

	public void setIsJLFFCOpen(Integer isJLFFCOpen) {
		this.isJLFFCOpen = isJLFFCOpen;
	}

	public Integer getIsHGFFCOpen() {
		return isHGFFCOpen;
	}

	public void setIsHGFFCOpen(Integer isHGFFCOpen) {
		this.isHGFFCOpen = isHGFFCOpen;
	}

	public Integer getIsGDSYXWOpen() {
		return isGDSYXWOpen;
	}

	public void setIsGDSYXWOpen(Integer isGDSYXWOpen) {
		this.isGDSYXWOpen = isGDSYXWOpen;
	}

	public Integer getIsSDSYXWOpen() {
		return isSDSYXWOpen;
	}

	public void setIsSDSYXWOpen(Integer isSDSYXWOpen) {
		this.isSDSYXWOpen = isSDSYXWOpen;
	}

	public Integer getIsJXSYXWOpen() {
		return isJXSYXWOpen;
	}

	public void setIsJXSYXWOpen(Integer isJXSYXWOpen) {
		this.isJXSYXWOpen = isJXSYXWOpen;
	}

	public Integer getIsCQSYXWOpen() {
		return isCQSYXWOpen;
	}

	public void setIsCQSYXWOpen(Integer isCQSYXWOpen) {
		this.isCQSYXWOpen = isCQSYXWOpen;
	}

	public Integer getIsFJSYXWOpen() {
		return isFJSYXWOpen;
	}

	public void setIsFJSYXWOpen(Integer isFJSYXWOpen) {
		this.isFJSYXWOpen = isFJSYXWOpen;
	}

	public Integer getIsJSKSOpen() {
		return isJSKSOpen;
	}

	public void setIsJSKSOpen(Integer isJSKSOpen) {
		this.isJSKSOpen = isJSKSOpen;
	}

	public Integer getIsAHKSOpen() {
		return isAHKSOpen;
	}

	public void setIsAHKSOpen(Integer isAHKSOpen) {
		this.isAHKSOpen = isAHKSOpen;
	}

	public Integer getIsJLKSOpen() {
		return isJLKSOpen;
	}

	public void setIsJLKSOpen(Integer isJLKSOpen) {
		this.isJLKSOpen = isJLKSOpen;
	}

	public Integer getIsHBKSOpen() {
		return isHBKSOpen;
	}

	public void setIsHBKSOpen(Integer isHBKSOpen) {
		this.isHBKSOpen = isHBKSOpen;
	}

	public Integer getIsJYKSOpen() {
		return isJYKSOpen;
	}

	public void setIsJYKSOpen(Integer isJYKSOpen) {
		this.isJYKSOpen = isJYKSOpen;
	}

	public Integer getIsGSKSOpen() {
		return isGSKSOpen;
	}
	public void setIsGSKSOpen(Integer isGSKSOpen) {
		this.isGSKSOpen = isGSKSOpen;
	}
	public Integer getIsSHKSOpen() {
		return isSHKSOpen;
	}
	public void setIsSHKSOpen(Integer isSHKSOpen) {
		this.isSHKSOpen = isSHKSOpen;
	}
	public Integer getIsGXKSOpen() {
		return isGXKSOpen;
	}
	public void setIsGXKSOpen(Integer isGXKSOpen) {
		this.isGXKSOpen = isGXKSOpen;
	}
	public Integer getIsGDKLSFOpen() {
		return isGDKLSFOpen;
	}

	public void setIsGDKLSFOpen(Integer isGDKLSFOpen) {
		this.isGDKLSFOpen = isGDKLSFOpen;
	}

	public Integer getIsSHSSLDPCOpen() {
		return isSHSSLDPCOpen;
	}

	public void setIsSHSSLDPCOpen(Integer isSHSSLDPCOpen) {
		this.isSHSSLDPCOpen = isSHSSLDPCOpen;
	}

	public Integer getIsFC3DDPCOpen() {
		return isFC3DDPCOpen;
	}

	public void setIsFC3DDPCOpen(Integer isFC3DDPCOpen) {
		this.isFC3DDPCOpen = isFC3DDPCOpen;
	}

	public Integer getIsBJPK10Open() {
		return isBJPK10Open;
	}

	public void setIsBJPK10Open(Integer isBJPK10Open) {
		this.isBJPK10Open = isBJPK10Open;
	}
	public Integer getIsJSPK10Open() {
		return isJSPK10Open;
	}
	public void setIsJSPK10Open(Integer isJSPK10Open) {
		this.isJSPK10Open = isJSPK10Open;
	}
	public Integer getIsJSTBOpen() {
		return isJSTBOpen;
	}

	public void setIsJSTBOpen(Integer isJSTBOpen) {
		this.isJSTBOpen = isJSTBOpen;
	}

	public Integer getIsAHTBOpen() {
		return isAHTBOpen;
	}

	public void setIsAHTBOpen(Integer isAHTBOpen) {
		this.isAHTBOpen = isAHTBOpen;
	}

	public Integer getIsJLTBOpen() {
		return isJLTBOpen;
	}

	public void setIsJLTBOpen(Integer isJLTBOpen) {
		this.isJLTBOpen = isJLTBOpen;
	}

	public Integer getIsHBTBOpen() {
		return isHBTBOpen;
	}

	public void setIsHBTBOpen(Integer isHBTBOpen) {
		this.isHBTBOpen = isHBTBOpen;
	}

	public Integer getIsBJKSOpen() {
		return isBJKSOpen;
	}

	public void setIsBJKSOpen(Integer isBJKSOpen) {
		this.isBJKSOpen = isBJKSOpen;
	}

	public Integer getIsBJTBOpen() {
		return isBJTBOpen;
	}

	public void setIsBJTBOpen(Integer isBJTBOpen) {
		this.isBJTBOpen = isBJTBOpen;
	}
	public Integer getIsXJPLFCOpen() {
		return isXJPLFCOpen;
	}

	public void setIsXJPLFCOpen(Integer isXJPLFCOpen) {
		this.isXJPLFCOpen = isXJPLFCOpen;
	}

	public Integer getIsTWWFCOpen() {
		return isTWWFCOpen;
	}

	public void setIsTWWFCOpen(Integer isTWWFCOpen) {
		this.isTWWFCOpen = isTWWFCOpen;
	}



	public  Integer getIsXYLHCOpen() {
		return isXYLHCOpen;
	}



	public  void setIsXYLHCOpen(Integer isXYLHCOpen) {
		this.isXYLHCOpen = isXYLHCOpen;
	}



	public Integer getIsSFSSCOpen() {
		return isSFSSCOpen;
	}



	public void setIsSFSSCOpen(Integer isSFSSCOpen) {
		this.isSFSSCOpen = isSFSSCOpen;
	}



	public Integer getIsWFSSCOpen() {
		return isWFSSCOpen;
	}



	public void setIsWFSSCOpen(Integer isWFSSCOpen) {
		this.isWFSSCOpen = isWFSSCOpen;
	}



	public Integer getIsSFKSOpen() {
		return isSFKSOpen;
	}



	public void setIsSFKSOpen(Integer isSFKSOpen) {
		this.isSFKSOpen = isSFKSOpen;
	}



	public Integer getIsWFKSOpen() {
		return isWFKSOpen;
	}


	public void setIsWFKSOpen(Integer isWFKSOpen) {
		this.isWFKSOpen = isWFKSOpen;
	}

	public Integer getIsSHFSSCOpen() {
		return isSHFSSCOpen;
	}

	public void setIsSHFSSCOpen(Integer isSHFSSCOpen) {
		this.isSHFSSCOpen = isSHFSSCOpen;
	}

	public Integer getIsWFSYXWOpen() {
		return isWFSYXWOpen;
	}

	public void setIsWFSYXWOpen(Integer isWFSYXWOpen) {
		this.isWFSYXWOpen = isWFSYXWOpen;
	}

	public Integer getIsSFSYXWOpen() {
		return isSFSYXWOpen;
	}

	public void setIsSFSYXWOpen(Integer isSFSYXWOpen) {
		this.isSFSYXWOpen = isSFSYXWOpen;
	}



	public Integer getIsSFPK10Open() {
		return isSFPK10Open;
	}



	public void setIsSFPK10Open(Integer isSFPK10Open) {
		this.isSFPK10Open = isSFPK10Open;
	}



	public Integer getIsWFPK10Open() {
		return isWFPK10Open;
	}



	public void setIsWFPK10Open(Integer isWFPK10Open) {
		this.isWFPK10Open = isWFPK10Open;
	}



	public Integer getIsSHFPK10Open() {
		return isSHFPK10Open;
	}



	public void setIsSHFPK10Open(Integer isSHFPK10Open) {
		this.isSHFPK10Open = isSHFPK10Open;
	}



	public Integer getIsXYFTOpen() {
		return isXYFTOpen;
	}


	public void setIsXYFTOpen(Integer isXYFTOpen) {
		this.isXYFTOpen = isXYFTOpen;
	}



	public Integer getIsLCQSSCOpen() {
		return isLCQSSCOpen;
	}



	public void setIsLCQSSCOpen(Integer isLCQSSCOpen) {
		this.isLCQSSCOpen = isLCQSSCOpen;
	}
	
	
	
}
