package com.team.lottery.extvo;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * 彩票控返回结果类
 * @author gs
 *
 */
@XmlRootElement(name="root")
@XmlAccessorType(XmlAccessType.FIELD)
public class LotteryResultForCpk
{
  @XmlElement(name="item")
  private List<LotteryResultForCpkVo> LotteryResultVos;
  
  public List<LotteryResultForCpkVo> getLotteryResultVos()
  {
    return this.LotteryResultVos;
  }
  
  public void setLotteryResultVos(List<LotteryResultForCpkVo> lotteryResultVos)
  {
    this.LotteryResultVos = lotteryResultVos;
  }
}