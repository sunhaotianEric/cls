package com.team.lottery.extvo;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

import com.team.lottery.util.DateUtils;


/**
 * 接口返回的每行开奖数据对象
 * @author chenhsh
 *
 */

public class LotteryResultVo {
	
	//期号
	private String expect;
	//开奖号码
	private String opencode;
	//接口返回的开奖时间
	private String opentime;
	
	public String getExpect() {
		return expect;
	}
	public void setExpect(String expect) {
		this.expect = expect;
	}
	public String getOpencode() {
		return opencode;
	}
	public void setOpencode(String opencode) {
		this.opencode = opencode;
	}
	public String getOpentime() {
		return opentime;
	}
	public void setOpentime(String opentime) {
		this.opentime = opentime;
	}
	
	/**
	 * 获取日期类型的开奖时间
	 * @return
	 */
	public Date getOpenDateTime() {
		if(StringUtils.isNotEmpty(opentime)){
			return DateUtils.getDateByStrFormat(opentime, "yyyy-MM-dd HH:mm:ss");
		}
		return null;
	}
	
}
