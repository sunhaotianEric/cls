package com.team.lottery.extvo;

/**
 * 每日登录或投注按类型统计对象
 * @author gs
 *
 */
public class DayOnlineTypeCountVo {
	
	//在线类型  手机或电脑
	private String onlineType;
	//数量
	private Integer countNum;
	
	public String getOnlineType() {
		return onlineType;
	}
	public void setOnlineType(String onlineType) {
		this.onlineType = onlineType;
	}
	public Integer getCountNum() {
		return countNum;
	}
	public void setCountNum(Integer countNum) {
		this.countNum = countNum;
	}
	
}
