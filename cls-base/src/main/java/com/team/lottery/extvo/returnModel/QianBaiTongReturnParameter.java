package com.team.lottery.extvo.returnModel;

public class QianBaiTongReturnParameter {

	private String MerNo;
	private String Amount;
	private String BillNo;
	private String Succeed;
	private String MD5info;
	private String Result;
	private String MerRemark;
	
	public String getMerNo() {
		return MerNo;
	}
	public void setMerNo(String merNo) {
		MerNo = merNo;
	}
	public String getAmount() {
		return Amount;
	}
	public void setAmount(String amount) {
		Amount = amount;
	}
	public String getBillNo() {
		return BillNo;
	}
	public void setBillNo(String billNo) {
		BillNo = billNo;
	}
	public String getSucceed() {
		return Succeed;
	}
	public void setSucceed(String succeed) {
		Succeed = succeed;
	}
	public String getMD5info() {
		return MD5info;
	}
	public void setMD5info(String mD5info) {
		MD5info = mD5info;
	}
	public String getResult() {
		return Result;
	}
	public void setResult(String result) {
		Result = result;
	}
	public String getMerRemark() {
		return MerRemark;
	}
	public void setMerRemark(String merRemark) {
		MerRemark = merRemark;
	}
	
	
}
