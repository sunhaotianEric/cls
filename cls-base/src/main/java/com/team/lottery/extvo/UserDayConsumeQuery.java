package com.team.lottery.extvo;

import java.util.Date;

import com.team.lottery.dto.CommonQueryVo;

public class UserDayConsumeQuery extends CommonQueryVo{

	//用户名
	private String userName;  
	private String bizSystem;
	//团队领导人名字
	private String teamLeaderName; //包含推荐人的拼接 
	private String realLeaderName; //真是的领导人名字
	private String regfrom;
	//资金起始时间
    private Date createdDateStart;
    //资金结束时间
    private Date createdDateEnd;
    //归属日期
    private Date belongDate;
    
    //归属开始日期
    private Date belongDateStart;
    //归属结束日期
    private Date belongDateEnd;
    
    //代理报表排序标识
    private Integer sortMunber;
    
	public String getTeamLeaderName() {
		return teamLeaderName;
	}
	public void setTeamLeaderName(String teamLeaderName) {
		this.teamLeaderName = teamLeaderName;
	}
	public String getRealLeaderName() {
		return realLeaderName;
	}
	public void setRealLeaderName(String realLeaderName) {
		this.realLeaderName = realLeaderName;
	}
	public Date getCreatedDateStart() {
		return createdDateStart;
	}
	public void setCreatedDateStart(Date createdDateStart) {
		this.createdDateStart = createdDateStart;
	}
	public Date getCreatedDateEnd() {
		return createdDateEnd;
	}
	public void setCreatedDateEnd(Date createdDateEnd) {
		this.createdDateEnd = createdDateEnd;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getBizSystem() {
		return bizSystem;
	}
	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}
	public Date getBelongDate() {
		return belongDate;
	}
	public void setBelongDate(Date belongDate) {
		this.belongDate = belongDate;
	}
	public Date getBelongDateStart() {
		return belongDateStart;
	}
	public void setBelongDateStart(Date belongDateStart) {
		this.belongDateStart = belongDateStart;
	}
	public Date getBelongDateEnd() {
		return belongDateEnd;
	}
	public void setBelongDateEnd(Date belongDateEnd) {
		this.belongDateEnd = belongDateEnd;
	}
	public Integer getSortMunber() {
		return sortMunber;
	}
	public void setSortMunber(Integer sortMunber) {
		this.sortMunber = sortMunber;
	}
	public String getRegfrom() {
		return regfrom;
	}
	public void setRegfrom(String regfrom) {
		this.regfrom = regfrom;
	}
	
    
    
}
