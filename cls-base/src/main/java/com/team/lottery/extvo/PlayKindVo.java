package com.team.lottery.extvo;

public class PlayKindVo {
	private String code;
	private String serviceClassName; //对应的实现逻辑
	
	public PlayKindVo(String code,String serviceClassName) {
		this.code = code;
		this.serviceClassName = serviceClassName;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getServiceClassName() {
		return serviceClassName;
	}
	public void setServiceClassName(String serviceClassName) {
		this.serviceClassName = serviceClassName;
	}
}
