package com.team.lottery.extvo;

import java.math.BigDecimal;
import java.util.Date;

public class DayLotteryGainQuery {
    private Long id;

    private String bizSystem;

    private String lotteryType;

    private BigDecimal payMoney;

    private BigDecimal winMoney;

    private BigDecimal gain;

    private BigDecimal winGain;

    private Date belongDate;

    private Date updateTime;

    private Date createTime;
    
	private Integer gainModel;
	
	private Date belongDateStart;
	
	private Date belongDateEnd;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBizSystem() {
        return bizSystem;
    }

    public void setBizSystem(String bizSystem) {
        this.bizSystem = bizSystem == null ? null : bizSystem.trim();
    }

    public String getLotteryType() {
        return lotteryType;
    }

    public void setLotteryType(String lotteryType) {
        this.lotteryType = lotteryType == null ? null : lotteryType.trim();
    }

    public BigDecimal getPayMoney() {
        return payMoney;
    }

    public void setPayMoney(BigDecimal payMoney) {
        this.payMoney = payMoney;
    }

    public BigDecimal getWinMoney() {
        return winMoney;
    }

    public void setWinMoney(BigDecimal winMoney) {
        this.winMoney = winMoney;
    }

    public BigDecimal getGain() {
        return gain;
    }

    public void setGain(BigDecimal gain) {
        this.gain = gain;
    }

    public BigDecimal getWinGain() {
        return winGain;
    }

    public void setWinGain(BigDecimal winGain) {
        this.winGain = winGain;
    }

    public Date getBelongDate() {
        return belongDate;
    }

    public void setBelongDate(Date belongDate) {
        this.belongDate = belongDate;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

	public Integer getGainModel() {
		return gainModel;
	}

	public void setGainModel(Integer gainModel) {
		this.gainModel = gainModel;
	}

	public Date getBelongDateStart() {
		return belongDateStart;
	}

	public void setBelongDateStart(Date belongDateStart) {
		this.belongDateStart = belongDateStart;
	}

	public Date getBelongDateEnd() {
		return belongDateEnd;
	}

	public void setBelongDateEnd(Date belongDateEnd) {
		this.belongDateEnd = belongDateEnd;
	}
    
    
}