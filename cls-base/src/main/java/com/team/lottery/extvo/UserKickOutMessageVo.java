package com.team.lottery.extvo;

import java.io.Serializable;

/**
 * 后台踢出前台用户的消息对象
 * @author luocheng
 *
 */
public class UserKickOutMessageVo implements Serializable {

	private static final long serialVersionUID = 6618987032194516414L;
	
	// 业务系统
	private String bizSystem;
	// 用户名
	private String userName;
	
	public UserKickOutMessageVo(String bizSystem, String userName) {
		super();
		this.bizSystem = bizSystem;
		this.userName = userName;
	}
	
	public String getBizSystem() {
		return bizSystem;
	}
	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
}
