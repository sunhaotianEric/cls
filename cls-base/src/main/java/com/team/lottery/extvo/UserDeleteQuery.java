package com.team.lottery.extvo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.team.lottery.enums.EUserDailLiLevel;

public class UserDeleteQuery {
	public static final String EQUAL = "equal";//等于
	public static final String LESS_THAN = "less_than";//小于
	private String compare = EQUAL; //模式比较   
	
	private List<EUserDailLiLevel> dailiLevels; //真正数据库要查询代理级别集合
	private BigDecimal sscreBate; //时时彩sscRebate
	private BigDecimal Money; //余额
	private Date queryTime; //前台传入的查询时间
	private Date  lastLogout; //扣除30天后数据库真正要查询的时间
	private Integer teamMember;  //团队人数
    private Integer lowerLevel;  //下级人数

	public String getCompare() {
		return compare;
	}
	public void setCompare(String compare) {
		this.compare = compare;
	}
	public List<EUserDailLiLevel> getDailiLevels() {
		return dailiLevels;
	}
	public void setDailiLevels(List<EUserDailLiLevel> dailiLevels) {
		this.dailiLevels = dailiLevels;
	}
	public BigDecimal getSscreBate() {
		return sscreBate;
	}
	public void setSscreBate(BigDecimal sscreBate) {
		this.sscreBate = sscreBate;
	}
	public BigDecimal getMoney() {
		return Money;
	}
	public void setMoney(BigDecimal quota0Monery) {
		Money = quota0Monery;
	}
	public Date getQueryTime() {
		return queryTime;
	}
	public void setQueryTime(Date queryTime) {
		this.queryTime = queryTime;
	}
	public Date getLastLogout() {
		return lastLogout;
	}
	public void setLastLogout(Date lastLogout) {
		this.lastLogout = lastLogout;
	}
	public Integer getTeamMember() {
		return teamMember;
	}
	public void setTeamMember(Integer teamMember) {
		this.teamMember = teamMember;
	}
	public Integer getLowerLevel() {
		return lowerLevel;
	}
	public void setLowerLevel(Integer lowerLevel) {
		this.lowerLevel = lowerLevel;
	}
	
}
