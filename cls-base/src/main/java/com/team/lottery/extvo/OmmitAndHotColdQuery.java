package com.team.lottery.extvo;

import com.team.lottery.enums.ELotteryKind;

/**
 * 冷热遗漏的查询对象
 * @author gs
 *
 */
public class OmmitAndHotColdQuery {
	
	public static final Integer CURRENT_OMMIT_TYPE = 0;
	public static final Integer HOTCOLD_TYPE = 1;
	public static final Integer MAX_OMMIT_TYPE = 2;
	public static final Integer DEFAULT_HOTCOLD_EXPECT = 30;

	//彩种
	private ELotteryKind lotteryKind;
	//玩法
	private String kindKey;
	//类型    遗漏或冷热
	private Integer type = CURRENT_OMMIT_TYPE;
	//期数
	private Integer expectNum = DEFAULT_HOTCOLD_EXPECT;
	
	public ELotteryKind getLotteryKind() {
		return lotteryKind;
	}
	public void setLotteryKind(ELotteryKind lotteryKind) {
		this.lotteryKind = lotteryKind;
	}
	public String getKindKey() {
		return kindKey;
	}
	public void setKindKey(String kindKey) {
		this.kindKey = kindKey;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getExpectNum() {
		return expectNum;
	}
	public void setExpectNum(Integer expectNum) {
		this.expectNum = expectNum;
	}
	
	
}
