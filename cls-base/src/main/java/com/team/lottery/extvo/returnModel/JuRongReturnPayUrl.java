package com.team.lottery.extvo.returnModel;

public class JuRongReturnPayUrl {
	private String net;
	
	private String ok;
	
	private String code;
	
	private String msg;
	
	private JuRongReturnData data;
	
	/*private String img;*/

	public String getNet() {
		return net;
	}

	public void setNet(String net) {
		this.net = net;
	}

	public String getOk() {
		return ok;
	}

	public void setOk(String ok) {
		this.ok = ok;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public JuRongReturnData getData() {
		return data;
	}

	public void setData(JuRongReturnData data) {
		this.data = data;
	}


	/*public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}*/

	
	
}
