package com.team.lottery.extvo;

import com.team.lottery.dto.CommonQueryVo;

/**
 * 自动出款查询对象.
 * 
 * @author Jamine
 *
 */
public class WithdrawAutoConfigQuery extends CommonQueryVo {
	// 系统简写.
	private String bizSystem;
	// 状态开关.
	private Integer enabled;

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}

	public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

}
