package com.team.lottery.extvo;

public class TreatmentConfigMsg {
	
	private String applyHalfMonthBonusSwitch;
	private String halfMonthBonus1956;
	private String halfMonthBonus1954;
	private String applyUpHalfMonthBonusTime;
	private String applyDownHalfMonthBonusTime;
	private String orderAmountJudgeSwitch;
	private String downDealSwitch;
	private String reach1958DayNum;
	private String reach1956DayNum;
	private String userReach1956DayNum;
	private String halfMonthAllowDiff;
	private String halfMonthDayNumPercent;
	
	private String applyDayBonusSwitch;
	private String dayBonus;
	private String applyDayBonusTime;
	
	private String applyDayCommissionSwitch;
	private String applyDayCommissionTime;
	
	private String applyMonthSalarySwitch;
	private String applyMonthSalaryTime;
	
	private String applyDaySalarySwitch;
	private String applyDaySalaryTime;
	
	private String applyDayActivitySwitch;
	private String applyDayActivityTime;
	
	//日分红每月发放日期
	private String dayBonusOpenTime;
	
	//处理对应待遇的管理员Id集合
	private String dealHalfMonthBonusAdmins;
	private String dealDayBonusAdmins;
	private String dealDayCommissionAdmins;
	private String dealMonthSalaryAdmins;
	private String dealDaySalaryAdmins;
	private String dealDayActivityAdmins;
	
	//处理对应待遇的管理员username集合
	private String dealHalfMonthBonusAdminNames;
	private String dealDayBonusAdminNames;
	private String dealDayCommissionAdminNames;
	private String dealMonthSalaryAdminNames;
	private String dealDaySalaryAdminNames;
	private String dealDayActivityAdminNames;
	
	public String getApplyDayActivitySwitch() {
		return applyDayActivitySwitch;
	}
	public void setApplyDayActivitySwitch(String applyDayActivitySwitch) {
		this.applyDayActivitySwitch = applyDayActivitySwitch;
	}
	public String getApplyDayActivityTime() {
		return applyDayActivityTime;
	}
	public void setApplyDayActivityTime(String applyDayActivityTime) {
		this.applyDayActivityTime = applyDayActivityTime;
	}
	public String getApplyHalfMonthBonusSwitch() {
		return applyHalfMonthBonusSwitch;
	}
	public void setApplyHalfMonthBonusSwitch(String applyHalfMonthBonusSwitch) {
		this.applyHalfMonthBonusSwitch = applyHalfMonthBonusSwitch;
	}
	public String getHalfMonthBonus1956() {
		return halfMonthBonus1956;
	}
	public void setHalfMonthBonus1956(String halfMonthBonus1956) {
		this.halfMonthBonus1956 = halfMonthBonus1956;
	}
	public String getHalfMonthBonus1954() {
		return halfMonthBonus1954;
	}
	public void setHalfMonthBonus1954(String halfMonthBonus1954) {
		this.halfMonthBonus1954 = halfMonthBonus1954;
	}
	public String getApplyUpHalfMonthBonusTime() {
		return applyUpHalfMonthBonusTime;
	}
	public void setApplyUpHalfMonthBonusTime(String applyUpHalfMonthBonusTime) {
		this.applyUpHalfMonthBonusTime = applyUpHalfMonthBonusTime;
	}
	public String getApplyDownHalfMonthBonusTime() {
		return applyDownHalfMonthBonusTime;
	}
	public void setApplyDownHalfMonthBonusTime(String applyDownHalfMonthBonusTime) {
		this.applyDownHalfMonthBonusTime = applyDownHalfMonthBonusTime;
	}
	public String getApplyDayBonusSwitch() {
		return applyDayBonusSwitch;
	}
	public void setApplyDayBonusSwitch(String applyDayBonusSwitch) {
		this.applyDayBonusSwitch = applyDayBonusSwitch;
	}
	public String getDayBonus() {
		return dayBonus;
	}
	public void setDayBonus(String dayBonus) {
		this.dayBonus = dayBonus;
	}
	public String getApplyDayBonusTime() {
		return applyDayBonusTime;
	}
	public void setApplyDayBonusTime(String applyDayBonusTime) {
		this.applyDayBonusTime = applyDayBonusTime;
	}
	public String getApplyDayCommissionSwitch() {
		return applyDayCommissionSwitch;
	}
	public void setApplyDayCommissionSwitch(String applyDayCommissionSwitch) {
		this.applyDayCommissionSwitch = applyDayCommissionSwitch;
	}
	public String getApplyDayCommissionTime() {
		return applyDayCommissionTime;
	}
	public void setApplyDayCommissionTime(String applyDayCommissionTime) {
		this.applyDayCommissionTime = applyDayCommissionTime;
	}
	public String getApplyMonthSalarySwitch() {
		return applyMonthSalarySwitch;
	}
	public void setApplyMonthSalarySwitch(String applyMonthSalarySwitch) {
		this.applyMonthSalarySwitch = applyMonthSalarySwitch;
	}
	public String getApplyMonthSalaryTime() {
		return applyMonthSalaryTime;
	}
	public void setApplyMonthSalaryTime(String applyMonthSalaryTime) {
		this.applyMonthSalaryTime = applyMonthSalaryTime;
	}
	public String getApplyDaySalarySwitch() {
		return applyDaySalarySwitch;
	}
	public void setApplyDaySalarySwitch(String applyDaySalarySwitch) {
		this.applyDaySalarySwitch = applyDaySalarySwitch;
	}
	public String getApplyDaySalaryTime() {
		return applyDaySalaryTime;
	}
	public void setApplyDaySalaryTime(String applyDaySalaryTime) {
		this.applyDaySalaryTime = applyDaySalaryTime;
	}
	public String getDealHalfMonthBonusAdmins() {
		return dealHalfMonthBonusAdmins;
	}
	public void setDealHalfMonthBonusAdmins(String dealHalfMonthBonusAdmins) {
		this.dealHalfMonthBonusAdmins = dealHalfMonthBonusAdmins;
	}
	public String getDealDayBonusAdmins() {
		return dealDayBonusAdmins;
	}
	public void setDealDayBonusAdmins(String dealDayBonusAdmins) {
		this.dealDayBonusAdmins = dealDayBonusAdmins;
	}
	public String getDealDayCommissionAdmins() {
		return dealDayCommissionAdmins;
	}
	public void setDealDayCommissionAdmins(String dealDayCommissionAdmins) {
		this.dealDayCommissionAdmins = dealDayCommissionAdmins;
	}
	public String getDealMonthSalaryAdmins() {
		return dealMonthSalaryAdmins;
	}
	public void setDealMonthSalaryAdmins(String dealMonthSalaryAdmins) {
		this.dealMonthSalaryAdmins = dealMonthSalaryAdmins;
	}
	public String getDealDaySalaryAdmins() {
		return dealDaySalaryAdmins;
	}
	public void setDealDaySalaryAdmins(String dealDaySalaryAdmins) {
		this.dealDaySalaryAdmins = dealDaySalaryAdmins;
	}
	public String getDealHalfMonthBonusAdminNames() {
		return dealHalfMonthBonusAdminNames;
	}
	public void setDealHalfMonthBonusAdminNames(String dealHalfMonthBonusAdminNames) {
		this.dealHalfMonthBonusAdminNames = dealHalfMonthBonusAdminNames;
	}
	public String getDealDayBonusAdminNames() {
		return dealDayBonusAdminNames;
	}
	public void setDealDayBonusAdminNames(String dealDayBonusAdminNames) {
		this.dealDayBonusAdminNames = dealDayBonusAdminNames;
	}
	public String getDealDayCommissionAdminNames() {
		return dealDayCommissionAdminNames;
	}
	public void setDealDayCommissionAdminNames(String dealDayCommissionAdminNames) {
		this.dealDayCommissionAdminNames = dealDayCommissionAdminNames;
	}
	public String getDealMonthSalaryAdminNames() {
		return dealMonthSalaryAdminNames;
	}
	public void setDealMonthSalaryAdminNames(String dealMonthSalaryAdminNames) {
		this.dealMonthSalaryAdminNames = dealMonthSalaryAdminNames;
	}
	public String getDealDaySalaryAdminNames() {
		return dealDaySalaryAdminNames;
	}
	public void setDealDaySalaryAdminNames(String dealDaySalaryAdminNames) {
		this.dealDaySalaryAdminNames = dealDaySalaryAdminNames;
	}
	public String getDayBonusOpenTime() {
		return dayBonusOpenTime;
	}
	public void setDayBonusOpenTime(String dayBonusOpenTime) {
		this.dayBonusOpenTime = dayBonusOpenTime;
	}
	public String getOrderAmountJudgeSwitch() {
		return orderAmountJudgeSwitch;
	}
	public void setOrderAmountJudgeSwitch(String orderAmountJudgeSwitch) {
		this.orderAmountJudgeSwitch = orderAmountJudgeSwitch;
	}
	public String getDownDealSwitch() {
		return downDealSwitch;
	}
	public void setDownDealSwitch(String downDealSwitch) {
		this.downDealSwitch = downDealSwitch;
	}
	public String getReach1958DayNum() {
		return reach1958DayNum;
	}
	public void setReach1958DayNum(String reach1958DayNum) {
		this.reach1958DayNum = reach1958DayNum;
	}
	public String getReach1956DayNum() {
		return reach1956DayNum;
	}
	public void setReach1956DayNum(String reach1956DayNum) {
		this.reach1956DayNum = reach1956DayNum;
	}
	public String getUserReach1956DayNum() {
		return userReach1956DayNum;
	}
	public void setUserReach1956DayNum(String userReach1956DayNum) {
		this.userReach1956DayNum = userReach1956DayNum;
	}
	public String getHalfMonthAllowDiff() {
		return halfMonthAllowDiff;
	}
	public void setHalfMonthAllowDiff(String halfMonthAllowDiff) {
		this.halfMonthAllowDiff = halfMonthAllowDiff;
	}
	public String getHalfMonthDayNumPercent() {
		return halfMonthDayNumPercent;
	}
	public void setHalfMonthDayNumPercent(String halfMonthDayNumPercent) {
		this.halfMonthDayNumPercent = halfMonthDayNumPercent;
	}
	public String getDealDayActivityAdmins() {
		return dealDayActivityAdmins;
	}
	public void setDealDayActivityAdmins(String dealDayActivityAdmins) {
		this.dealDayActivityAdmins = dealDayActivityAdmins;
	}
	public String getDealDayActivityAdminNames() {
		return dealDayActivityAdminNames;
	}
	public void setDealDayActivityAdminNames(String dealDayActivityAdminNames) {
		this.dealDayActivityAdminNames = dealDayActivityAdminNames;
	}
	
}
