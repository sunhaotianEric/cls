package com.team.lottery.extvo;

import com.team.lottery.vo.Order;

public class OrderUnEqualVersionVo {
	
	private Order order;
	private String winNotesSb;
	
	
	public OrderUnEqualVersionVo(Order order, String winNotesSb) {
		super();
		this.order = order;
		this.winNotesSb = winNotesSb;
	}
	
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	public String getWinNotesSb() {
		return winNotesSb;
	}
	public void setWinNotesSb(String winNotesSb) {
		this.winNotesSb = winNotesSb;
	}
	
	

}
