package com.team.lottery.extvo;

import java.math.BigDecimal;

public class UserVipInfo {
	//可申请
	public static final Integer CAN_APPLY = 1;
	//不可申请
	public static final Integer CAN_NOT_APPLY = 2;
	//已经申请
	public static final Integer YET_APPLY = 3;
	
    private Long id;

    private String vipLevelName;

    private String vipLevelDesc;

    private BigDecimal monthRecharegeMoney;

    private BigDecimal monthLotteryAmount;

    private BigDecimal keepRechargeMoney;

    private Integer promotedApplyStates;

    private BigDecimal promotedMoney;

    private Integer freeCodeApplyStates;

    private BigDecimal freeCodeMoney;

    private Integer rechargeWelfareSwitch;

    private BigDecimal rechargeWelfareValue;

    private Integer monthLossReturnSwitch;

    private BigDecimal monthLossReturnValue;

    private Integer dayWithdrawSwtich;

    private BigDecimal dayWithdrawValue;

    private Integer counterServiceSwitch;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVipLevelName() {
        return vipLevelName;
    }

    public void setVipLevelName(String vipLevelName) {
        this.vipLevelName = vipLevelName == null ? null : vipLevelName.trim();
    }

    public String getVipLevelDesc() {
        return vipLevelDesc;
    }

    public void setVipLevelDesc(String vipLevelDesc) {
        this.vipLevelDesc = vipLevelDesc == null ? null : vipLevelDesc.trim();
    }

    public BigDecimal getMonthRecharegeMoney() {
        return monthRecharegeMoney;
    }

    public void setMonthRecharegeMoney(BigDecimal monthRecharegeMoney) {
        this.monthRecharegeMoney = monthRecharegeMoney;
    }

    public BigDecimal getMonthLotteryAmount() {
        return monthLotteryAmount;
    }

    public void setMonthLotteryAmount(BigDecimal monthLotteryAmount) {
        this.monthLotteryAmount = monthLotteryAmount;
    }

    public BigDecimal getKeepRechargeMoney() {
        return keepRechargeMoney;
    }

    public void setKeepRechargeMoney(BigDecimal keepRechargeMoney) {
        this.keepRechargeMoney = keepRechargeMoney;
    }
	public Integer getPromotedApplyStates() {
		return promotedApplyStates;
	}

	public void setPromotedApplyStates(Integer promotedApplyStates) {
		this.promotedApplyStates = promotedApplyStates;
	}

	public BigDecimal getPromotedMoney() {
		return promotedMoney;
	}

	public void setPromotedMoney(BigDecimal promotedMoney) {
		this.promotedMoney = promotedMoney;
	}

	public Integer getFreeCodeApplyStates() {
		return freeCodeApplyStates;
	}

	public void setFreeCodeApplyStates(Integer freeCodeApplyStates) {
		this.freeCodeApplyStates = freeCodeApplyStates;
	}

	public BigDecimal getFreeCodeMoney() {
        return freeCodeMoney;
    }

    public void setFreeCodeMoney(BigDecimal freeCodeMoney) {
        this.freeCodeMoney = freeCodeMoney;
    }

    public Integer getRechargeWelfareSwitch() {
        return rechargeWelfareSwitch;
    }

    public void setRechargeWelfareSwitch(Integer rechargeWelfareSwitch) {
        this.rechargeWelfareSwitch = rechargeWelfareSwitch;
    }

    public BigDecimal getRechargeWelfareValue() {
        return rechargeWelfareValue;
    }

    public void setRechargeWelfareValue(BigDecimal rechargeWelfareValue) {
        this.rechargeWelfareValue = rechargeWelfareValue;
    }

    public Integer getMonthLossReturnSwitch() {
        return monthLossReturnSwitch;
    }

    public void setMonthLossReturnSwitch(Integer monthLossReturnSwitch) {
        this.monthLossReturnSwitch = monthLossReturnSwitch;
    }

    public BigDecimal getMonthLossReturnValue() {
        return monthLossReturnValue;
    }

    public void setMonthLossReturnValue(BigDecimal monthLossReturnValue) {
        this.monthLossReturnValue = monthLossReturnValue;
    }

    public Integer getDayWithdrawSwtich() {
        return dayWithdrawSwtich;
    }

    public void setDayWithdrawSwtich(Integer dayWithdrawSwtich) {
        this.dayWithdrawSwtich = dayWithdrawSwtich;
    }

    public BigDecimal getDayWithdrawValue() {
        return dayWithdrawValue;
    }

    public void setDayWithdrawValue(BigDecimal dayWithdrawValue) {
        this.dayWithdrawValue = dayWithdrawValue;
    }

    public Integer getCounterServiceSwitch() {
        return counterServiceSwitch;
    }

    public void setCounterServiceSwitch(Integer counterServiceSwitch) {
        this.counterServiceSwitch = counterServiceSwitch;
    }
}