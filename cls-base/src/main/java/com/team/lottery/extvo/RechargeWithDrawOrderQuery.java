package com.team.lottery.extvo;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 充值订单查询vo
 * @author chenhsh
 *
 */
public class RechargeWithDrawOrderQuery{
    
	// 查询自己
	public static final Integer SCOPE_OWN = 1;
	// 查询直接下级
	public static final Integer SCOPE_SUB = 2;
	// 查询所有下级
	public static final Integer SCOPE_ALL_SUB = 3;
    private String serialNumber;
    private Integer adminId;
    private String payType;
    private String userName; //用户名
    private BigDecimal applyValue;  //申请金额
	private String statuss; //状态
    private String operateType; //操作类型
    private String postscript; //附言
    private String bankType; //银行
    private Date createdDateStart;
    private Date createdDateEnd;
    private String bizSystem;//业务系统
    private String thirdPayType;//第三方冲值类型
    private Integer userId;//用户Id号
    private String refType;//充值类型
    private String regfrom;
    private String fromType;
    private Integer queryScope = SCOPE_OWN;
    private Integer dateRange; //(1表示今天,-1表示昨天,-7表示最近7天,30表示本月,-30表示上个月.)
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getStatuss() {
		return statuss;
	}
	public void setStatuss(String statuss) {
		this.statuss = statuss;
	}
	public String getPostscript() {
		return postscript;
	}
	public void setPostscript(String postscript) {
		this.postscript = postscript;
	}
	public void setCreatedDateStart(Date createdDateStart) {
		this.createdDateStart = createdDateStart;
	}
	public void setCreatedDateEnd(Date createdDateEnd) {
		this.createdDateEnd = createdDateEnd;
	}
	public String getOperateType() {
		return operateType;
	}
	public void setOperateType(String operateType) {
		this.operateType = operateType;
	}
	public Integer getAdminId() {
		return adminId;
	}
	public void setAdminId(Integer adminId) {
		this.adminId = adminId;
	}
	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getBankType() {
		return bankType;
	}
	public void setBankType(String bankType) {
		this.bankType = bankType;
	}

	public Date getCreatedDateStart() {
		return createdDateStart;
	}
	public Date getCreatedDateEnd() {
		return createdDateEnd;
	}
	public BigDecimal getApplyValue() {
		return applyValue;
	}
	public void setApplyValue(BigDecimal applyValue) {
		this.applyValue = applyValue;
	}
	public String getBizSystem() {
		return bizSystem;
	}
	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}
	public String getThirdPayType() {
		return thirdPayType;
	}
	public void setThirdPayType(String thirdPayType) {
		this.thirdPayType = thirdPayType;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getRefType() {
		return refType;
	}
	public void setRefType(String refType) {
		this.refType = refType;
	}
	public String getRegfrom() {
		return regfrom;
	}
	public void setRegfrom(String regfrom) {
		this.regfrom = regfrom;
	}
	public String getFromType() {
		return fromType;
	}
	public void setFromType(String fromType) {
		this.fromType = fromType;
	}
	public Integer getQueryScope() {
		return queryScope;
	}
	public void setQueryScope(Integer queryScope) {
		this.queryScope = queryScope;
	}
	public Integer getDateRange() {
		return dateRange;
	}
	public void setDageRange(Integer dageRange) {
		this.dateRange = dageRange;
	}
	
}
