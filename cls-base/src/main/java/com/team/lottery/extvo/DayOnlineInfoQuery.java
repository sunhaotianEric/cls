package com.team.lottery.extvo;

import java.util.Date;

public class DayOnlineInfoQuery {

	private Date dayOnlineInfoDateStart;
	private Date dayOnlineInfoDateEnd;
	private String bizSystem;
	public Date getDayOnlineInfoDateStart() {
		return dayOnlineInfoDateStart;
	}
	public void setDayOnlineInfoDateStart(Date dayOnlineInfoDateStart) {
		this.dayOnlineInfoDateStart = dayOnlineInfoDateStart;
	}
	public Date getDayOnlineInfoDateEnd() {
		return dayOnlineInfoDateEnd;
	}
	public void setDayOnlineInfoDateEnd(Date dayOnlineInfoDateEnd) {
		this.dayOnlineInfoDateEnd = dayOnlineInfoDateEnd;
	}
	public String getBizSystem() {
		return bizSystem;
	}
	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}
	
	
}
