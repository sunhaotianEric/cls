package com.team.lottery.extvo;

public class LhcOrderReportQuery {

	private String bizSystem;

	//玩法
	private String lotteryTypeProtype;
	
	//投注期号
	private String expect;

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}

	public String getLotteryTypeProtype() {
		return lotteryTypeProtype;
	}

	public void setLotteryTypeProtype(String lotteryTypeProtype) {
		this.lotteryTypeProtype = lotteryTypeProtype;
	}

	public String getExpect() {
		return expect;
	}

	public void setExpect(String expect) {
		this.expect = expect;
	}  
	
	
}
