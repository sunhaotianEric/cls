package com.team.lottery.extvo;

import java.util.Date;

public class SigninRecrodQuery {
	
	private String bizSystem;
	private String userName;
	private Integer userId;
	private Date createdDateStart; //查询开始时间
    private Date createdDateEnd;  //查询结束时间
    
    
	public String getBizSystem() {
		return bizSystem;
	}
	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Date getCreatedDateStart() {
		return createdDateStart;
	}
	public void setCreatedDateStart(Date createdDateStart) {
		this.createdDateStart = createdDateStart;
	}
	public Date getCreatedDateEnd() {
		return createdDateEnd;
	}
	public void setCreatedDateEnd(Date createdDateEnd) {
		this.createdDateEnd = createdDateEnd;
	}
    
    

}
