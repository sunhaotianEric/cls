package com.team.lottery.extvo;

/**
 * 第三方银行码表查询vo
 * @author chengf
 *
 */
public class QuickBankTypeVo {
    
	
	private String chargeType;
    
    private String bankType;

	public String getChargeType() {
		return chargeType;
	}

	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	public String getBankType() {
		return bankType;
	}

	public void setBankType(String bankType) {
		this.bankType = bankType;
	}
    
    
}
