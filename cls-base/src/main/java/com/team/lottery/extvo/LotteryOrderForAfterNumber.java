package com.team.lottery.extvo;

import java.util.List;

import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.enums.ELotteryModel;

public class LotteryOrderForAfterNumber {

	private ELotteryKind lotteryKind; //投注彩种
	
	//投注玩法和号码(dwr框架通过此字段注入内容)
	private String[][] lotteryPlayKindMsg; 
	//投注玩法和号码(spring mvc框架通过此字段注入内容)
	private List<LotteryOrderDetail> playKindMsgs;
	
	private ELotteryModel model; //投注模式,元\角\分
	
	private Integer awardModel; //奖金模式
	
	private Integer stopAfterNumber;  //是否中奖以后停止追号
	
	private List<LotteryOrderExpect> orderExpects;//追号期数倍数内容
	
	private Integer isLotteryByPoint; //是否使用积分付款
	
	private String lotteryId; //投注方案号


	public String[][] getLotteryPlayKindMsg() {
		return lotteryPlayKindMsg;
	}

	public void setLotteryPlayKindMsg(String[][] lotteryPlayKindMsg) {
		this.lotteryPlayKindMsg = lotteryPlayKindMsg;
	}

	public ELotteryModel getModel() {
		return model;
	}

	public void setModel(ELotteryModel model) {
		this.model = model;
	}

	public Integer getIsLotteryByPoint() {
		return isLotteryByPoint;
	}

	public void setIsLotteryByPoint(Integer isLotteryByPoint) {
		this.isLotteryByPoint = isLotteryByPoint;
	}

	public List<LotteryOrderExpect> getOrderExpects() {
		return orderExpects;
	}

	public void setOrderExpects(List<LotteryOrderExpect> orderExpects) {
		this.orderExpects = orderExpects;
	}

	public ELotteryKind getLotteryKind() {
		return lotteryKind;
	}

	public void setLotteryKind(ELotteryKind lotteryKind) {
		this.lotteryKind = lotteryKind;
	}

	public Integer getAwardModel() {
		return awardModel;
	}

	public void setAwardModel(Integer awardModel) {
		this.awardModel = awardModel;
	}

	public String getLotteryId() {
		return lotteryId;
	}

	public void setLotteryId(String lotteryId) {
		this.lotteryId = lotteryId;
	}

	public Integer getStopAfterNumber() {
		return stopAfterNumber;
	}

	public void setStopAfterNumber(Integer stopAfterNumber) {
		this.stopAfterNumber = stopAfterNumber;
	}

	public List<LotteryOrderDetail> getPlayKindMsgs() {
		return playKindMsgs;
	}

	public void setPlayKindMsgs(List<LotteryOrderDetail> playKindMsgs) {
		this.playKindMsgs = playKindMsgs;
	}
	
}
