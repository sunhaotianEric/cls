package com.team.lottery.extvo;

import java.math.BigDecimal;
import java.util.Date;

public class DayProfitRankQuery {
	
	private String bizSystem;

    private Integer userId;

    private String userName;
    
    private String nickName;

    private BigDecimal bonus;

    private Integer ranking;

	private Date profittimeStart;
    
    private Date profittimeEnd;

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public BigDecimal getBonus() {
		return bonus;
	}

	public void setBonus(BigDecimal bonus) {
		this.bonus = bonus;
	}

	public Integer getRanking() {
		return ranking;
	}

	public void setRanking(Integer ranking) {
		this.ranking = ranking;
	}

	public Date getProfittimeStart() {
		return profittimeStart;
	}

	public void setProfittimeStart(Date profittimeStart) {
		this.profittimeStart = profittimeStart;
	}

	public Date getProfittimeEnd() {
		return profittimeEnd;
	}

	public void setProfittimeEnd(Date profittimeEnd) {
		this.profittimeEnd = profittimeEnd;
	}
    
    
}
