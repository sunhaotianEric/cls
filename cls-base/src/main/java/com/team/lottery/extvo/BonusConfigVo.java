package com.team.lottery.extvo;

import java.io.Serializable;
import java.util.List;

import com.team.lottery.vo.BonusConfig;

public class BonusConfigVo  implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -6481472081863058662L;

	 private Integer userId;
	 private String userName;
	 private String bizSystem;
     /**
      *分红契约签订状态
     */
	 private Integer bonusState;
	 
	 private List<BonusConfig> bonusConfigList;

	 private List<BonusConfig> newBonusConfigList;
	 
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getBizSystem() {
		return bizSystem;
	}

	public void setBizSystem(String bizSystem) {
		this.bizSystem = bizSystem;
	}

	public Integer getBonusState() {
		return bonusState;
	}

	public void setBonusState(Integer bonusState) {
		this.bonusState = bonusState;
	}

	public List<BonusConfig> getBonusConfigList() {
		return bonusConfigList;
	}

	public void setBonusConfigList(List<BonusConfig> bonusConfigList) {
		this.bonusConfigList = bonusConfigList;
	}
	
	public List<BonusConfig> getNewBonusConfigList() {
		return newBonusConfigList;
	}

	public void setNewBonusConfigList(List<BonusConfig> newBonusConfigList) {
		this.newBonusConfigList = newBonusConfigList;
	}

	public String getBonusStateName() {
		if(bonusState!=null){
			if(bonusState==0){
				return "未签订契约";
			}
			if(bonusState==1){
				return "契约确认中";			
			}
			if(bonusState==2){
				return "已签订契约";
			}
			if(bonusState==3){
				return "契约确认中";
			}
			if(bonusState==4){
				return "契约被拒绝,请重新签订";
			}
		}
		return "";
	}
	 
}