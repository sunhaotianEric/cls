package com.team.lottery.extvo;

import java.math.BigDecimal;

/**
 * 控制数据删除表对象
 * @author gs
 *
 */
public class DeleteAgoDataInfo {
	
	private Integer isDeleteOrder;
	private Integer isDeleteMoneyDetail;
	private Integer isDeleteNotes;
	private Integer isDeleteDrawOrder;
	private Integer isDeleteLogin;
	private Integer isDeleteLotteryCode;
	private Integer isDeleteOperateLog;
	private Integer isDeleteUseMoneyLog;
	private Integer isDeleteLotteryCache;
	private Integer isDeleteRecordLog;
	private Integer isDeleteTreatmentApply;
	private Integer isDeleteTreatmentOrder;
	private Integer isDeleteQuotaDetail;
	private Integer isDeleteSafetyQuestions;
	private Integer isDeleteUserBank;
	private BigDecimal sscRebate; //模式
	
	public Integer getIsDeleteOrder() {
		return isDeleteOrder;
	}
	public void setIsDeleteOrder(Integer isDeleteOrder) {
		this.isDeleteOrder = isDeleteOrder;
	}
	public Integer getIsDeleteMoneyDetail() {
		return isDeleteMoneyDetail;
	}
	public void setIsDeleteMoneyDetail(Integer isDeleteMoneyDetail) {
		this.isDeleteMoneyDetail = isDeleteMoneyDetail;
	}
	public Integer getIsDeleteNotes() {
		return isDeleteNotes;
	}
	public void setIsDeleteNotes(Integer isDeleteNotes) {
		this.isDeleteNotes = isDeleteNotes;
	}
	public Integer getIsDeleteDrawOrder() {
		return isDeleteDrawOrder;
	}
	public void setIsDeleteDrawOrder(Integer isDeleteDrawOrder) {
		this.isDeleteDrawOrder = isDeleteDrawOrder;
	}
	public Integer getIsDeleteLogin() {
		return isDeleteLogin;
	}
	public void setIsDeleteLogin(Integer isDeleteLogin) {
		this.isDeleteLogin = isDeleteLogin;
	}
	public Integer getIsDeleteLotteryCode() {
		return isDeleteLotteryCode;
	}
	public void setIsDeleteLotteryCode(Integer isDeleteLotteryCode) {
		this.isDeleteLotteryCode = isDeleteLotteryCode;
	}
	public Integer getIsDeleteOperateLog() {
		return isDeleteOperateLog;
	}
	public void setIsDeleteOperateLog(Integer isDeleteOperateLog) {
		this.isDeleteOperateLog = isDeleteOperateLog;
	}
	public Integer getIsDeleteUseMoneyLog() {
		return isDeleteUseMoneyLog;
	}
	public void setIsDeleteUseMoneyLog(Integer isDeleteUseMoneyLog) {
		this.isDeleteUseMoneyLog = isDeleteUseMoneyLog;
	}
	public Integer getIsDeleteLotteryCache() {
		return isDeleteLotteryCache;
	}
	public void setIsDeleteLotteryCache(Integer isDeleteLotteryCache) {
		this.isDeleteLotteryCache = isDeleteLotteryCache;
	}
	public Integer getIsDeleteRecordLog() {
		return isDeleteRecordLog;
	}
	public void setIsDeleteRecordLog(Integer isDeleteRecordLog) {
		this.isDeleteRecordLog = isDeleteRecordLog;
	}
	public Integer getIsDeleteTreatmentApply() {
		return isDeleteTreatmentApply;
	}
	public void setIsDeleteTreatmentApply(Integer isDeleteTreatmentApply) {
		this.isDeleteTreatmentApply = isDeleteTreatmentApply;
	}
	public Integer getIsDeleteTreatmentOrder() {
		return isDeleteTreatmentOrder;
	}
	public void setIsDeleteTreatmentOrder(Integer isDeleteTreatmentOrder) {
		this.isDeleteTreatmentOrder = isDeleteTreatmentOrder;
	}
	public Integer getIsDeleteQuotaDetail() {
		return isDeleteQuotaDetail;
	}
	public void setIsDeleteQuotaDetail(Integer isDeleteQuotaDetail) {
		this.isDeleteQuotaDetail = isDeleteQuotaDetail;
	}
	public Integer getIsDeleteSafetyQuestions() {
		return isDeleteSafetyQuestions;
	}
	public void setIsDeleteSafetyQuestions(Integer isDeleteSafetyQuestions) {
		this.isDeleteSafetyQuestions = isDeleteSafetyQuestions;
	}
	public Integer getIsDeleteUserBank() {
		return isDeleteUserBank;
	}
	public void setIsDeleteUserBank(Integer isDeleteUserBank) {
		this.isDeleteUserBank = isDeleteUserBank;
	}
	public BigDecimal getSscRebate() {
		return sscRebate;
	}
	public void setSscRebate(BigDecimal sscRebate) {
		this.sscRebate = sscRebate;
	}

	

}
