package com.team.lottery.extvo;

import java.math.BigDecimal;

public class SystemSetMsg {
	
	//网站名称
	private String websiteName;
	//网站标题
	private String websiteTitle;
	//网站地址
	private String websiteAddress;
	//网站LOGO图标
	private String websiteLogo;
	//网站meta关键字
	private String websiteKey;
	//网站meta描述
	private String websiteDes;
	//网站站长
	private String websiteStation;
	//是否关闭网站
	private Integer isWebsiteClose;
	//关闭网站信息
	public String websiteCloseInfo;
	//元模式
	private Integer yuanModel;
	//角模式
	private Integer jiaoModel;
	//分模式
	private Integer fenModel;
	//厘模式
	private Integer liModel;
	//毫模式
	private Integer haoModel;
	//时时彩最低奖金模式
	private Integer SSCLowestAwardModel;
	//分分彩最低奖金模式
	private Integer FFCLowestAwardModel;
	//11选5奖最低金模式
	private Integer SYXWLowestAwardModel;
	//快3奖最低金模式
	private Integer KSLowestAwardModel;
	//骰宝最低奖金模式
	public Integer TBLowestAwardModel;
	//快乐十分奖最低金模式
	private Integer KLSFLowestAwardModel;
	//低频彩奖最低金模式
	private Integer DPCLowestAwardModel;
	//PK10最低奖金模式
	public Integer PK10LowestAwardModel;
	//六合彩最低奖金模式
	public Integer LHCLowestAwardModel;
	//是否启用全站登陆模式
	private Integer isStationLogin;
	//前台投注列表翻页显示条数
	private Integer frontCathecticPageSize;
	//后台翻页显示条数
	private Integer managerPageSize;
	//最高奖金限制设置
	private Double highestAward;	
	//禁止登陆IP
	private String forbidIps;
	//会员中心翻页显示条数
	private Integer frontMemberPageSize;	
	//最少提现金额
	private Double lowestWithdrawMoney;	
	//至少充值金额
	private Double lowestRechargeMoney;	
	//每天取款次数
	private Integer highestWithdrawCount;		
	//存款说明
	private String rechargeDes;
	//取款说明
	private String withdrawDes;	
	//取款时间限制开始点 
	private Integer withdrawTimeStart;		
	//取款时间限制结束点
	private Integer withdrawTimeEnd;		
	//充值赠送积分
	private Integer rechargeDonatePoint;	
	//公司名称
	private String companyName;		
	//联系人
	private String contactsName;		
	//客服热线
	private String customServicePhone;	
	//客服邮箱
	private String customServiceMail;		
	//客服QQ号码
	private String customServiceQQ;	
	//ICP备案
	private String ICPRecord;	
	//是否开启邮件发送功能
	private Integer isMailSend;	
	//发送邮箱
	private String mailName;	
	//邮箱服务器地址
	private String mailService;	
	//邮箱登录用户名
	private String mailLoginName;	
	//邮箱登录密码
	private String mailLoginPassword;		
	//发送时显示的姓名
	private String mailSendShowName;		
	//是否开启推荐注册
	private Integer isRegisterRecommend;	
	//是否启用积分投注
	private Integer isPointCathectic;	
	//注册赠送资金
	private Double resisterDonateMoney;	
	//注册赠送积分
	private Integer resisterDonatePoint;	
	//会员登陆赠送积分
	private Integer loginDonatePoint;	
	//会员购彩赠送积分
	private Integer cathecticDonatePoint;	
	//会员中奖赠送积分
	private Integer drawDonatePoint;
	//1块钱多少积分
	private Integer pointScale;	
	//是否允许代理锁定下级会员
	private Integer isAllowLockByAgent;	
	//是否允许代理给下级会员充值
	private Integer isRechargeByAgent;	
	//是否启用代理会员返点
	private Integer isAgentGetRebate;	
	//是否启用上下级转账开关
	private Integer isTransferSwich;
	//是否开启线上环境
	private Integer isOnline;
	//开奖ip地址
	private String openCodeSystemHosts;
	//其他前端ip地址
	private String frontSystemHosts;
	//会员注册协议
	private String registerProtocol;
	//直属代理和总代理的累加分红比例
	private BigDecimal dailiHighestBonusScale;
	//分红周期
	private Integer bonusPeriod;
	//是否允许追号
	private Integer allowZhuihao;
	//金鹰分分彩盈利模式
	private Integer jlffcProfitModel;
	//超过取现金额的手续费百分比
	public BigDecimal exceedWithdrawExpenses;
	
	//金额相关
	//每天取现次数
	private Integer withDrawEveryDayNumber;
	//取现单笔最低金额
	public BigDecimal withDrawOneStrokeLowestMoney;
	//取现单笔最高金额
	public BigDecimal withDrawOneStrokeHighestMoney;
	//每天取现总金额
	private BigDecimal withDrawEveryDayTotalMoney;
	//快捷充值最少金额
	private BigDecimal quickRechargeLowestMoney;
	//快捷充值最多金额
	private BigDecimal quickRechargeHighestMoney;
	//闪付快捷充值最少金额
	private BigDecimal shanfuQuickRechargeLowestMoney;
	//闪付快捷充值最多金额
	private BigDecimal shanfuQuickRechargeHighestMoney;
	//每日转账限额
	private BigDecimal exceedTransferDayLimitExpenses;
	
	//3D起售时间
	private String d3StopStartDate;
	//3D起售时间
	private String d3StopEndDate;
	//是否允许后台登陆的设置
	private Integer managerLoginSet;
	//管理员登陆key
	private String managerLoginKey;
	
	//1960注册1958配额数
	public Integer quota1960to1958;
	//1960注册1956配额数
	public Integer quota1960to1956;
	//1960注册1954配额数
	public Integer quota1960to1954;
	//1960注册1952配额数
	public Integer quota1960to1952;
	//1958注册1956配额数
	private Integer quota1958to1956;
	//1958注册1954配额数
	private Integer quota1958to1954;
	//1958注册1952配额数
	private Integer quota1958to1952;
	//1956注册1954配额数
	private Integer quota1956to1954;
	//1956注册1952配额数
	private Integer quota1956to1952;
	//1954注册1952配额数
	private Integer quota1954to1952;
	
	//毫模式返奖控制总额
	private BigDecimal haoReturnControl;
	//厘模式返奖控制总额
	private BigDecimal liReturnControl;
	//分模式返奖控制总额
	private BigDecimal fenReturnControl;
	//角模式返奖控制总额
	private BigDecimal jiaoReturnControl;
	//元模式返奖控制总额
	private BigDecimal yuanReturnControl;
	
	//支付方式开关
	private Integer onlineBankingPayment;
	private Integer quickRecharge;
	private Integer alipayRecharge;
	//微信转账支付开关
	private Integer weiXinRecharge;
	//闪付微信支付开关
	private Integer shanfuWeixinRecharge;
	//闪付快捷开关
	private Integer shanfuPayRecharge;
	
	//PK10基准日期
	private String pk10BaseDate;
	//PK10基准期数
	private Integer pk10BasetExpect;
	
	//韩国1.5分彩基准日期
	private String hgffcBaseDate;
	//韩国1.5分彩基准期数
	private Integer hgffcBaseExpect;
	
	//北京快3基准日期
	private String bjksBaseDate;
	//北京快3基准期数
	private Integer bjksBaseExpect;
	
	//新加坡2分彩基准日期
	private String xjplfcBaseDate;
	//新加坡2分彩基准期数
	private Integer xjplfcBaseExpect;
	
	//台湾5分彩基准日期
	private String twwfcBaseDate;
	//台湾5分彩基准期数
	private Integer twwfcBaseExpect;
	
	
	public String getOpenCodeSystemHosts() {
		return openCodeSystemHosts;
	}
	public void setOpenCodeSystemHosts(String openCodeSystemHosts) {
		this.openCodeSystemHosts = openCodeSystemHosts;
	}
	public String getFrontSystemHosts() {
		return frontSystemHosts;
	}
	public void setFrontSystemHosts(String frontSystemHosts) {
		this.frontSystemHosts = frontSystemHosts;
	}
	public Integer getIsOnline() {
		return isOnline;
	}
	public void setIsOnline(Integer isOnline) {
		this.isOnline = isOnline;
	}

	public Integer getIsTransferSwich() {
		return isTransferSwich;
	}
	public void setIsTransferSwich(Integer isTransferSwich) {
		this.isTransferSwich = isTransferSwich;
	}
	public BigDecimal getExceedTransferDayLimitExpenses() {
		return exceedTransferDayLimitExpenses;
	}
	public void setExceedTransferDayLimitExpenses(
			BigDecimal exceedTransferDayLimitExpenses) {
		this.exceedTransferDayLimitExpenses = exceedTransferDayLimitExpenses;
	}
	public String getWebsiteName() {
		return websiteName;
	}
	public void setWebsiteName(String websiteName) {
		this.websiteName = websiteName;
	}
	public String getWebsiteTitle() {
		return websiteTitle;
	}
	public void setWebsiteTitle(String websiteTitle) {
		this.websiteTitle = websiteTitle;
	}
	public String getWebsiteAddress() {
		return websiteAddress;
	}
	public void setWebsiteAddress(String websiteAddress) {
		this.websiteAddress = websiteAddress;
	}
	public String getWebsiteLogo() {
		return websiteLogo;
	}
	public void setWebsiteLogo(String websiteLogo) {
		this.websiteLogo = websiteLogo;
	}
	public String getWebsiteKey() {
		return websiteKey;
	}
	public void setWebsiteKey(String websiteKey) {
		this.websiteKey = websiteKey;
	}
	public String getWebsiteDes() {
		return websiteDes;
	}
	public void setWebsiteDes(String websiteDes) {
		this.websiteDes = websiteDes;
	}
	public String getWebsiteStation() {
		return websiteStation;
	}
	public void setWebsiteStation(String websiteStation) {
		this.websiteStation = websiteStation;
	}
	public Integer getIsWebsiteClose() {
		return isWebsiteClose;
	}
	public void setIsWebsiteClose(Integer isWebsiteClose) {
		this.isWebsiteClose = isWebsiteClose;
	}
	public Integer getYuanModel() {
		return yuanModel;
	}
	public void setYuanModel(Integer yuanModel) {
		this.yuanModel = yuanModel;
	}
	public Integer getJiaoModel() {
		return jiaoModel;
	}
	public void setJiaoModel(Integer jiaoModel) {
		this.jiaoModel = jiaoModel;
	}
	public Integer getFenModel() {
		return fenModel;
	}
	public void setFenModel(Integer fenModel) {
		this.fenModel = fenModel;
	}
	public Integer getLiModel() {
		return liModel;
	}
	public void setLiModel(Integer liModel) {
		this.liModel = liModel;
	}
	public Integer getSSCLowestAwardModel() {
		return SSCLowestAwardModel;
	}
	public void setSSCLowestAwardModel(Integer sSCLowestAwardModel) {
		SSCLowestAwardModel = sSCLowestAwardModel;
	}
	public Integer getFFCLowestAwardModel() {
		return FFCLowestAwardModel;
	}
	public void setFFCLowestAwardModel(Integer fFCLowestAwardModel) {
		FFCLowestAwardModel = fFCLowestAwardModel;
	}
	public Integer getSYXWLowestAwardModel() {
		return SYXWLowestAwardModel;
	}
	public void setSYXWLowestAwardModel(Integer sYXWLowestAwardModel) {
		SYXWLowestAwardModel = sYXWLowestAwardModel;
	}
	public Integer getKSLowestAwardModel() {
		return KSLowestAwardModel;
	}
	public void setKSLowestAwardModel(Integer kSLowestAwardModel) {
		KSLowestAwardModel = kSLowestAwardModel;
	}
	public Integer getKLSFLowestAwardModel() {
		return KLSFLowestAwardModel;
	}
	public void setKLSFLowestAwardModel(Integer kLSFLowestAwardModel) {
		KLSFLowestAwardModel = kLSFLowestAwardModel;
	}
	public Integer getDPCLowestAwardModel() {
		return DPCLowestAwardModel;
	}
	public void setDPCLowestAwardModel(Integer dPCLowestAwardModel) {
		DPCLowestAwardModel = dPCLowestAwardModel;
	}
	public Integer getIsStationLogin() {
		return isStationLogin;
	}
	public void setIsStationLogin(Integer isStationLogin) {
		this.isStationLogin = isStationLogin;
	}
	public Integer getFrontCathecticPageSize() {
		return frontCathecticPageSize;
	}
	public void setFrontCathecticPageSize(Integer frontCathecticPageSize) {
		this.frontCathecticPageSize = frontCathecticPageSize;
	}
	public Integer getManagerPageSize() {
		return managerPageSize;
	}
	public void setManagerPageSize(Integer managerPageSize) {
		this.managerPageSize = managerPageSize;
	}
	public Double getHighestAward() {
		return highestAward;
	}
	public void setHighestAward(Double highestAward) {
		this.highestAward = highestAward;
	}
	public String getForbidIps() {
		return forbidIps;
	}
	public void setForbidIps(String forbidIps) {
		this.forbidIps = forbidIps;
	}
	public Integer getFrontMemberPageSize() {
		return frontMemberPageSize;
	}
	public void setFrontMemberPageSize(Integer frontMemberPageSize) {
		this.frontMemberPageSize = frontMemberPageSize;
	}
	public Double getLowestWithdrawMoney() {
		return lowestWithdrawMoney;
	}
	public void setLowestWithdrawMoney(Double lowestWithdrawMoney) {
		this.lowestWithdrawMoney = lowestWithdrawMoney;
	}
	public Double getLowestRechargeMoney() {
		return lowestRechargeMoney;
	}
	public void setLowestRechargeMoney(Double lowestRechargeMoney) {
		this.lowestRechargeMoney = lowestRechargeMoney;
	}
	public Integer getHighestWithdrawCount() {
		return highestWithdrawCount;
	}
	public void setHighestWithdrawCount(Integer highestWithdrawCount) {
		this.highestWithdrawCount = highestWithdrawCount;
	}
	public String getRechargeDes() {
		return rechargeDes;
	}
	public void setRechargeDes(String rechargeDes) {
		this.rechargeDes = rechargeDes;
	}
	public String getWithdrawDes() {
		return withdrawDes;
	}
	public void setWithdrawDes(String withdrawDes) {
		this.withdrawDes = withdrawDes;
	}
	public Integer getWithdrawTimeStart() {
		return withdrawTimeStart;
	}
	public void setWithdrawTimeStart(Integer withdrawTimeStart) {
		this.withdrawTimeStart = withdrawTimeStart;
	}
	public Integer getWithdrawTimeEnd() {
		return withdrawTimeEnd;
	}
	public void setWithdrawTimeEnd(Integer withdrawTimeEnd) {
		this.withdrawTimeEnd = withdrawTimeEnd;
	}
	public Integer getRechargeDonatePoint() {
		return rechargeDonatePoint;
	}
	public void setRechargeDonatePoint(Integer rechargeDonatePoint) {
		this.rechargeDonatePoint = rechargeDonatePoint;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getContactsName() {
		return contactsName;
	}
	public void setContactsName(String contactsName) {
		this.contactsName = contactsName;
	}
	public String getCustomServicePhone() {
		return customServicePhone;
	}
	public void setCustomServicePhone(String customServicePhone) {
		this.customServicePhone = customServicePhone;
	}
	public String getCustomServiceMail() {
		return customServiceMail;
	}
	public void setCustomServiceMail(String customServiceMail) {
		this.customServiceMail = customServiceMail;
	}
	public String getCustomServiceQQ() {
		return customServiceQQ;
	}
	public void setCustomServiceQQ(String customServiceQQ) {
		this.customServiceQQ = customServiceQQ;
	}
	public String getICPRecord() {
		return ICPRecord;
	}
	public void setICPRecord(String iCPRecord) {
		ICPRecord = iCPRecord;
	}
	public Integer getIsMailSend() {
		return isMailSend;
	}
	public void setIsMailSend(Integer isMailSend) {
		this.isMailSend = isMailSend;
	}
	public String getMailName() {
		return mailName;
	}
	public void setMailName(String mailName) {
		this.mailName = mailName;
	}
	public String getMailService() {
		return mailService;
	}
	public void setMailService(String mailService) {
		this.mailService = mailService;
	}
	public String getMailLoginName() {
		return mailLoginName;
	}
	public void setMailLoginName(String mailLoginName) {
		this.mailLoginName = mailLoginName;
	}
	public String getMailLoginPassword() {
		return mailLoginPassword;
	}
	public void setMailLoginPassword(String mailLoginPassword) {
		this.mailLoginPassword = mailLoginPassword;
	}
	public String getMailSendShowName() {
		return mailSendShowName;
	}
	public void setMailSendShowName(String mailSendShowName) {
		this.mailSendShowName = mailSendShowName;
	}
	public Integer getIsRegisterRecommend() {
		return isRegisterRecommend;
	}
	public void setIsRegisterRecommend(Integer isRegisterRecommend) {
		this.isRegisterRecommend = isRegisterRecommend;
	}
	public Integer getIsPointCathectic() {
		return isPointCathectic;
	}
	public void setIsPointCathectic(Integer isPointCathectic) {
		this.isPointCathectic = isPointCathectic;
	}
	public Double getResisterDonateMoney() {
		return resisterDonateMoney;
	}
	public void setResisterDonateMoney(Double resisterDonateMoney) {
		this.resisterDonateMoney = resisterDonateMoney;
	}
	public Integer getResisterDonatePoint() {
		return resisterDonatePoint;
	}
	public void setResisterDonatePoint(Integer resisterDonatePoint) {
		this.resisterDonatePoint = resisterDonatePoint;
	}
	public Integer getLoginDonatePoint() {
		return loginDonatePoint;
	}
	public void setLoginDonatePoint(Integer loginDonatePoint) {
		this.loginDonatePoint = loginDonatePoint;
	}
	public Integer getCathecticDonatePoint() {
		return cathecticDonatePoint;
	}
	public void setCathecticDonatePoint(Integer cathecticDonatePoint) {
		this.cathecticDonatePoint = cathecticDonatePoint;
	}
	public Integer getDrawDonatePoint() {
		return drawDonatePoint;
	}
	public void setDrawDonatePoint(Integer drawDonatePoint) {
		this.drawDonatePoint = drawDonatePoint;
	}
	public Integer getPointScale() {
		return pointScale;
	}
	public void setPointScale(Integer pointScale) {
		this.pointScale = pointScale;
	}
	public Integer getIsAllowLockByAgent() {
		return isAllowLockByAgent;
	}
	public void setIsAllowLockByAgent(Integer isAllowLockByAgent) {
		this.isAllowLockByAgent = isAllowLockByAgent;
	}
	public Integer getIsRechargeByAgent() {
		return isRechargeByAgent;
	}
	public void setIsRechargeByAgent(Integer isRechargeByAgent) {
		this.isRechargeByAgent = isRechargeByAgent;
	}
	public Integer getIsAgentGetRebate() {
		return isAgentGetRebate;
	}
	public void setIsAgentGetRebate(Integer isAgentGetRebate) {
		this.isAgentGetRebate = isAgentGetRebate;
	}
	public String getRegisterProtocol() {
		return registerProtocol;
	}
	public void setRegisterProtocol(String registerProtocol) {
		this.registerProtocol = registerProtocol;
	}
	public BigDecimal getDailiHighestBonusScale() {
		return dailiHighestBonusScale;
	}
	public void setDailiHighestBonusScale(BigDecimal dailiHighestBonusScale) {
		this.dailiHighestBonusScale = dailiHighestBonusScale;
	}
	public Integer getBonusPeriod() {
		return bonusPeriod;
	}
	public void setBonusPeriod(Integer bonusPeriod) {
		this.bonusPeriod = bonusPeriod;
	}
	public Integer getAllowZhuihao() {
		return allowZhuihao;
	}
	public void setAllowZhuihao(Integer allowZhuihao) {
		this.allowZhuihao = allowZhuihao;
	}
	public Integer getWithDrawEveryDayNumber() {
		return withDrawEveryDayNumber;
	}
	public void setWithDrawEveryDayNumber(Integer withDrawEveryDayNumber) {
		this.withDrawEveryDayNumber = withDrawEveryDayNumber;
	}
	public BigDecimal getWithDrawOneStrokeLowestMoney() {
		return withDrawOneStrokeLowestMoney;
	}
	public void setWithDrawOneStrokeLowestMoney(
			BigDecimal withDrawOneStrokeLowestMoney) {
		this.withDrawOneStrokeLowestMoney = withDrawOneStrokeLowestMoney;
	}
	public BigDecimal getWithDrawOneStrokeHighestMoney() {
		return withDrawOneStrokeHighestMoney;
	}
	public void setWithDrawOneStrokeHighestMoney(
			BigDecimal withDrawOneStrokeHighestMoney) {
		this.withDrawOneStrokeHighestMoney = withDrawOneStrokeHighestMoney;
	}
	public BigDecimal getWithDrawEveryDayTotalMoney() {
		return withDrawEveryDayTotalMoney;
	}
	public void setWithDrawEveryDayTotalMoney(BigDecimal withDrawEveryDayTotalMoney) {
		this.withDrawEveryDayTotalMoney = withDrawEveryDayTotalMoney;
	}
	public BigDecimal getQuickRechargeLowestMoney() {
		return quickRechargeLowestMoney;
	}
	public void setQuickRechargeLowestMoney(BigDecimal quickRechargeLowestMoney) {
		this.quickRechargeLowestMoney = quickRechargeLowestMoney;
	}
	public BigDecimal getQuickRechargeHighestMoney() {
		return quickRechargeHighestMoney;
	}
	public void setQuickRechargeHighestMoney(BigDecimal quickRechargeHighestMoney) {
		this.quickRechargeHighestMoney = quickRechargeHighestMoney;
	}
	public Integer getJlffcProfitModel() {
		return jlffcProfitModel;
	}
	public void setJlffcProfitModel(Integer jlffcProfitModel) {
		this.jlffcProfitModel = jlffcProfitModel;
	}
	public BigDecimal getExceedWithdrawExpenses() {
		return exceedWithdrawExpenses;
	}
	public void setExceedWithdrawExpenses(BigDecimal exceedWithdrawExpenses) {
		this.exceedWithdrawExpenses = exceedWithdrawExpenses;
	}
	public String getD3StopStartDate() {
		return d3StopStartDate;
	}
	public void setD3StopStartDate(String d3StopStartDate) {
		this.d3StopStartDate = d3StopStartDate;
	}
	public String getD3StopEndDate() {
		return d3StopEndDate;
	}
	public void setD3StopEndDate(String d3StopEndDate) {
		this.d3StopEndDate = d3StopEndDate;
	}
	public Integer getManagerLoginSet() {
		return managerLoginSet;
	}
	public void setManagerLoginSet(Integer managerLoginSet) {
		this.managerLoginSet = managerLoginSet;
	}
	public String getManagerLoginKey() {
		return managerLoginKey;
	}
	public void setManagerLoginKey(String managerLoginKey) {
		this.managerLoginKey = managerLoginKey;
	}
	public Integer getHaoModel() {
		return haoModel;
	}
	public void setHaoModel(Integer haoModel) {
		this.haoModel = haoModel;
	}
	public Integer getQuota1960to1958() {
		return quota1960to1958;
	}
	public void setQuota1960to1958(Integer quota1960to1958) {
		this.quota1960to1958 = quota1960to1958;
	}
	public Integer getQuota1960to1956() {
		return quota1960to1956;
	}
	public void setQuota1960to1956(Integer quota1960to1956) {
		this.quota1960to1956 = quota1960to1956;
	}
	public Integer getQuota1960to1954() {
		return quota1960to1954;
	}
	public void setQuota1960to1954(Integer quota1960to1954) {
		this.quota1960to1954 = quota1960to1954;
	}
	public Integer getQuota1960to1952() {
		return quota1960to1952;
	}
	public void setQuota1960to1952(Integer quota1960to1952) {
		this.quota1960to1952 = quota1960to1952;
	}
	public Integer getQuota1958to1956() {
		return quota1958to1956;
	}
	public void setQuota1958to1956(Integer quota1958to1956) {
		this.quota1958to1956 = quota1958to1956;
	}
	public Integer getQuota1958to1954() {
		return quota1958to1954;
	}
	public void setQuota1958to1954(Integer quota1958to1954) {
		this.quota1958to1954 = quota1958to1954;
	}
	public Integer getQuota1956to1954() {
		return quota1956to1954;
	}
	public void setQuota1956to1954(Integer quota1956to1954) {
		this.quota1956to1954 = quota1956to1954;
	}
	public Integer getQuota1956to1952() {
		return quota1956to1952;
	}
	public void setQuota1956to1952(Integer quota1956to1952) {
		this.quota1956to1952 = quota1956to1952;
	}
	public Integer getQuota1954to1952() {
		return quota1954to1952;
	}
	public void setQuota1954to1952(Integer quota1954to1952) {
		this.quota1954to1952 = quota1954to1952;
	}
	public Integer getQuota1958to1952() {
		return quota1958to1952;
	}
	public void setQuota1958to1952(Integer quota1958to1952) {
		this.quota1958to1952 = quota1958to1952;
	}
	public BigDecimal getHaoReturnControl() {
		return haoReturnControl;
	}
	public void setHaoReturnControl(BigDecimal haoReturnControl) {
		this.haoReturnControl = haoReturnControl;
	}
	public BigDecimal getLiReturnControl() {
		return liReturnControl;
	}
	public void setLiReturnControl(BigDecimal liReturnControl) {
		this.liReturnControl = liReturnControl;
	}
	public BigDecimal getFenReturnControl() {
		return fenReturnControl;
	}
	public void setFenReturnControl(BigDecimal fenReturnControl) {
		this.fenReturnControl = fenReturnControl;
	}
	public BigDecimal getJiaoReturnControl() {
		return jiaoReturnControl;
	}
	public void setJiaoReturnControl(BigDecimal jiaoReturnControl) {
		this.jiaoReturnControl = jiaoReturnControl;
	}
	public BigDecimal getYuanReturnControl() {
		return yuanReturnControl;
	}
	public void setYuanReturnControl(BigDecimal yuanReturnControl) {
		this.yuanReturnControl = yuanReturnControl;
	}
	public String getWebsiteCloseInfo() {
		return websiteCloseInfo;
	}
	public void setWebsiteCloseInfo(String websiteCloseInfo) {
		this.websiteCloseInfo = websiteCloseInfo;
	}
	public Integer getOnlineBankingPayment() {
		return onlineBankingPayment;
	}
	public void setOnlineBankingPayment(Integer onlineBankingPayment) {
		this.onlineBankingPayment = onlineBankingPayment;
	}
	public Integer getQuickRecharge() {
		return quickRecharge;
	}
	public void setQuickRecharge(Integer quickRecharge) {
		this.quickRecharge = quickRecharge;
	}
	public Integer getAlipayRecharge() {
		return alipayRecharge;
	}
	public void setAlipayRecharge(Integer alipayRecharge) {
		this.alipayRecharge = alipayRecharge;
	}
	public Integer getWeiXinRecharge() {
		return weiXinRecharge;
	}
	public void setWeiXinRecharge(Integer weiXinRecharge) {
		this.weiXinRecharge = weiXinRecharge;
	}
	public Integer getTBLowestAwardModel() {
		return TBLowestAwardModel;
	}
	public void setTBLowestAwardModel(Integer tBLowestAwardModel) {
		TBLowestAwardModel = tBLowestAwardModel;
	}
	public Integer getPK10LowestAwardModel() {
		return PK10LowestAwardModel;
	}
	public void setPK10LowestAwardModel(Integer pK10LowestAwardModel) {
		PK10LowestAwardModel = pK10LowestAwardModel;
	}
	public Integer getLHCLowestAwardModel() {
		return LHCLowestAwardModel;
	}
	public void setLHCLowestAwardModel(Integer lHCLowestAwardModel) {
		LHCLowestAwardModel = lHCLowestAwardModel;
	}
	public String getPk10BaseDate() {
		return pk10BaseDate;
	}
	public void setPk10BaseDate(String pk10BaseDate) {
		this.pk10BaseDate = pk10BaseDate;
	}
	public Integer getPk10BasetExpect() {
		return pk10BasetExpect;
	}
	public void setPk10BasetExpect(Integer pk10BasetExpect) {
		this.pk10BasetExpect = pk10BasetExpect;
	}
	public BigDecimal getShanfuQuickRechargeLowestMoney() {
		return shanfuQuickRechargeLowestMoney;
	}
	public void setShanfuQuickRechargeLowestMoney(
			BigDecimal shanfuQuickRechargeLowestMoney) {
		this.shanfuQuickRechargeLowestMoney = shanfuQuickRechargeLowestMoney;
	}
	public BigDecimal getShanfuQuickRechargeHighestMoney() {
		return shanfuQuickRechargeHighestMoney;
	}
	public void setShanfuQuickRechargeHighestMoney(
			BigDecimal shanfuQuickRechargeHighestMoney) {
		this.shanfuQuickRechargeHighestMoney = shanfuQuickRechargeHighestMoney;
	}
	public Integer getShanfuWeixinRecharge() {
		return shanfuWeixinRecharge;
	}
	public void setShanfuWeixinRecharge(Integer shanfuWeixinRecharge) {
		this.shanfuWeixinRecharge = shanfuWeixinRecharge;
	}
	public Integer getShanfuPayRecharge() {
		return shanfuPayRecharge;
	}
	public void setShanfuPayRecharge(Integer shanfuPayRecharge) {
		this.shanfuPayRecharge = shanfuPayRecharge;
	}
	public String getHgffcBaseDate() {
		return hgffcBaseDate;
	}
	public void setHgffcBaseDate(String hgffcBaseDate) {
		this.hgffcBaseDate = hgffcBaseDate;
	}
	public Integer getHgffcBaseExpect() {
		return hgffcBaseExpect;
	}
	public void setHgffcBaseExpect(Integer hgffcBaseExpect) {
		this.hgffcBaseExpect = hgffcBaseExpect;
	}
	public String getBjksBaseDate() {
		return bjksBaseDate;
	}
	public void setBjksBaseDate(String bjksBaseDate) {
		this.bjksBaseDate = bjksBaseDate;
	}
	public Integer getBjksBaseExpect() {
		return bjksBaseExpect;
	}
	public void setBjksBaseExpect(Integer bjksBaseExpect) {
		this.bjksBaseExpect = bjksBaseExpect;
	}
	public String getXjplfcBaseDate() {
		return xjplfcBaseDate;
	}
	public void setXjplfcBaseDate(String xjplfcBaseDate) {
		this.xjplfcBaseDate = xjplfcBaseDate;
	}
	public Integer getXjplfcBaseExpect() {
		return xjplfcBaseExpect;
	}
	public void setXjplfcBaseExpect(Integer xjplfcBaseExpect) {
		this.xjplfcBaseExpect = xjplfcBaseExpect;
	}
	public String getTwwfcBaseDate() {
		return twwfcBaseDate;
	}
	public void setTwwfcBaseDate(String twwfcBaseDate) {
		this.twwfcBaseDate = twwfcBaseDate;
	}
	public Integer getTwwfcBaseExpect() {
		return twwfcBaseExpect;
	}
	public void setTwwfcBaseExpect(Integer twwfcBaseExpect) {
		this.twwfcBaseExpect = twwfcBaseExpect;
	}
	
}
