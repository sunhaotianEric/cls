package com.team.lottery.redis.mq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.redis.JedisUtils;

import redis.clients.jedis.Jedis;

/**
 * redis启动消息订阅线程
 * @author luocheng
 *
 */
public class RedisThread implements Runnable {
	
	private static Logger logger = LoggerFactory.getLogger(RedisThread.class);
	
	//监听通道名称
	private String channel;
	
	//标志监听线程是否在跑
	public static int isSubscribeThreadRun = 0;
	
	public RedisThread(String channel) {
		this.channel=channel;
	}
	
	@Override
	public void run() {
		logger.info("开启redis消息订阅监听程序线程,监听通道[{}]", channel);
		Jedis redisClient = null;
		try {
			redisClient = JedisUtils.getResource();
			//先设置为已执行状态，因为订阅线程会卡住无法继续往下执行
			isSubscribeThreadRun = 1;
			redisClient.subscribe(new ClsRedisPubSubListener(), channel);
		} catch (Exception e) {
			//出现异常，尝试重新再次订阅
			logger.error("订阅通道[" + channel + "]出现异常，尝试再次订阅", e);
			redisClient = JedisUtils.getResource();
			redisClient.subscribe(new ClsRedisPubSubListener(), channel);
		} finally {
			if(redisClient != null) {
				redisClient.close();
			}
		}
		isSubscribeThreadRun = 0;
		logger.info("退出redis消息订阅监听程序线程，监听通道[{}]", channel);
	}

}
