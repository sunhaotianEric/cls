package com.team.lottery.redis.mq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.enums.ERedisChannel;
import com.team.lottery.system.message.BaseMessage;
import com.team.lottery.system.message.BaseMessageHandler;
import com.team.lottery.util.SerializeUtils;

public class ClsRedisPubSubListener extends  RedisPubSubListener{
	private static Logger logger = LoggerFactory.getLogger(ClsRedisPubSubListener.class);
	
	@Override
	public void onMessage(String channel, String message) {
		try {
			//刷新系统缓存通知
		if(channel.equals(ERedisChannel.CLSCACHE.name())){
				BaseMessage baseMessage=(BaseMessage) SerializeUtils.unSerialize(message.getBytes("ISO-8859-1"));
				BaseMessageHandler handler = baseMessage.getMessageHandler();
				if(handler != null) {
					handler.dealMessage(baseMessage);
				}
			}
			
		} catch (Exception e) {
			logger.error("处理redis订阅消息发生错误", e);
		}	
		logger.debug("onMessage: channel[" + channel + "], message[" + message + "]");  
	}
}
