package com.team.lottery.redis.mq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.Jedis;

import com.team.lottery.redis.JedisUtils;
import com.team.lottery.util.SerializeUtils;

public class Publisher {
	
	private static Logger logger = LoggerFactory.getLogger(Publisher.class);
	
	 /**
	 * 
	 * @param redisClient
	 * @param channel 频道
	 * @param message 泛型消息对象
	 */
	 public static <T> void publish(Jedis redisClient,String channel, T message) { 
		 
		 String msg=SerializeUtils.serialize2(message);
		 try {
			 redisClient.publish(channel, msg);
		} catch (Exception e) {
			logger.error("Publisher 异常,redis 资源归还重新获取... ");
			JedisUtils.returnResource(redisClient);
			
			redisClient = JedisUtils.getResource();
			try{  
				redisClient.publish(channel, msg);
			}catch (Exception e1) {
				logger.error("Publisher 重新获取 ，还是失败"+e1.getMessage());
			}finally{
				//归还资源
				JedisUtils.returnResource(redisClient);
			}
		}
		
	 }
	 
}
