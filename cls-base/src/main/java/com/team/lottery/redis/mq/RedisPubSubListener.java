package com.team.lottery.redis.mq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.JedisPubSub;

public class RedisPubSubListener extends JedisPubSub{
	
	private static Logger logger = LoggerFactory.getLogger(RedisPubSubListener.class);

	// 取得订阅的消息后的处理  
    @Override  
    public void onMessage(String channel, String message) { 
        logger.debug("onMessage: channel[" + channel + "], message[" + message + "]");  
    }  
  
    // 取得按表达式的方式订阅的消息后的处理    
    @Override  
    public void onPMessage(String pattern, String channel, String message) {  
    	logger.debug("onPMessage: pattern[" + pattern + "],channel[" + channel + "], message[" + message + "]");  
    }  
  
    // 初始化按表达式的方式订阅时候的处理   
    @Override  
    public void onPSubscribe(String channel, int subscribedChannels) {  
    	logger.debug("onSubscribe: channel[" + channel + "]," + "subscribedChannels[" + subscribedChannels + "]");  
    }  
//  
//    // 取消按表达式的方式订阅时候的处理   
//    @Override  
//    public void onPUnsubscribe(String channel, int subscribedChannels) {  
//        System.out.println("onUnsubscribe: channel[" + channel + "], " + "subscribedChannels[" + subscribedChannels + "]");  
//    }  
//  
//    // 初始化订阅时候的处理  
//    @Override  
//    public void onSubscribe(String pattern, int subscribedChannels) {  
//        System.out.println("onPUnsubscribe: pattern[" + pattern + "]," +  
//        "subscribedChannels[" + subscribedChannels + "]");  
//    }  
//  
//    // 取消订阅时候的处理  
//    @Override  
//    public void onUnsubscribe(String pattern, int subscribedChannels) {  
//        System.out.println("onPSubscribe: pattern[" + pattern + "], " +  
//        "subscribedChannels[" + subscribedChannels + "]");  
//    } 
//    
// 
    
}
