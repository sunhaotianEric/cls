package com.team.lottery.service;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.DayLotteryGainQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.daylotterygain.DayLotteryGainMapper;
import com.team.lottery.vo.DayLotteryGain;

@Service
public class DayLotteryGainService {
	@Autowired
	public DayLotteryGainMapper dayLotteryGainMapper;
	private static Logger logger = LoggerFactory.getLogger(DayLotteryGainService.class);
	public int deleteByPrimaryKey(Long id){
    	return dayLotteryGainMapper.deleteByPrimaryKey(id);
    }

	/**
	 * 插入数据索引存在即更新
	 */
    public int insertOrUpdate(DayLotteryGain record){
    	return dayLotteryGainMapper.insertOrUpdate(record);
    }
    
    public int insertSelective(DayLotteryGain record){
    	return dayLotteryGainMapper.insertSelective(record);
    }

    public DayLotteryGain selectByPrimaryKey(Long id){
    	return dayLotteryGainMapper.selectByPrimaryKey(id);
    }

    public int updateByPrimaryKeySelective(DayLotteryGain record){
    	return dayLotteryGainMapper.updateByPrimaryKeySelective(record);
    }

    public DayLotteryGain getDayLotteryGainByQuery(DayLotteryGain record){
    	return dayLotteryGainMapper.getDayLotteryGainByQuery(record);
    }

    /**
     * 查找所有的
     * @return
     */
    public Page getAllDayLotteryGainByQueryPage(DayLotteryGainQuery query,Page page){
		page.setTotalRecNum(dayLotteryGainMapper.getAllDayLotteryGainByQueryPageCount(query));
		page.setPageContent(dayLotteryGainMapper.getAllDayLotteryGainByQueryPage(query,page.getStartIndex(),page.getPageSize()));
    	return page;
    }

    
    public int saveDayLotteryGains(Set<String> bizSystems,Map<String,Map<String,BigDecimal>> bizMoneyMap,Date orderDate,String lotteryType){
    	for(String bizSystem:bizSystems){
    		Map<String,BigDecimal> moneyMap = bizMoneyMap.get(bizSystem);
    		//投注金额
			BigDecimal payMoney = moneyMap.get("payMoney");
			//中奖金额
			BigDecimal winMoney = moneyMap.get("winMoney");
			try {
				if(payMoney.compareTo(BigDecimal.ZERO)>0){
					//处理今日实时赢率
					
					 Calendar calendar = Calendar.getInstance();
					 calendar.setTime(orderDate);
					 calendar.set(Calendar.HOUR_OF_DAY, 3); 
					 calendar.set(Calendar.MINUTE,0); 
					 calendar.set(Calendar.SECOND,0); 
					 calendar.set(Calendar.MILLISECOND, 0);
					 //订单时间大于3点，属于当天
					 if(orderDate.after(calendar.getTime())){
						 calendar.set(Calendar.HOUR_OF_DAY, 12); 
					 }else{ //订单时间不超过3点，属于前一天
						 calendar.add(Calendar.DATE, -1);
						 calendar.set(Calendar.HOUR_OF_DAY, 12);
					 }
					//先查询，有没记录，没有在另外插入
					 DayLotteryGain query = new DayLotteryGain();
					 query.setBizSystem(bizSystem);
					 query.setBelongDate(calendar.getTime());
					 query.setLotteryType(lotteryType);
					//插入存在就更新
					 query.setCreateTime(new Date());
					 query.setUpdateTime(new Date());
					 query.setPayMoney(payMoney);
					 query.setWinMoney(winMoney);
					 query.setWinGain((payMoney.subtract(winMoney)).divide(payMoney, 4, BigDecimal.ROUND_DOWN));
					 query.setGain(payMoney.subtract(winMoney));
					 insertOrUpdate(query);
					 
					 /*DayLotteryGain dayLotteryGain = getDayLotteryGainByQuery(query);
					 if(dayLotteryGain != null){
						 dayLotteryGain.setUpdateTime(new Date());
						 dayLotteryGain.setPayMoney(dayLotteryGain.getPayMoney().add(payMoney));
						 dayLotteryGain.setWinMoney(dayLotteryGain.getWinMoney().add(winMoney));
						 BigDecimal winGain = (dayLotteryGain.getPayMoney().subtract(dayLotteryGain.getWinMoney())).divide(dayLotteryGain.getPayMoney(), 4, BigDecimal.ROUND_DOWN);
						 dayLotteryGain.setWinGain(winGain);
						 dayLotteryGain.setGain(dayLotteryGain.getPayMoney().subtract(dayLotteryGain.getWinMoney()));
						 updateByPrimaryKeySelective(dayLotteryGain);
					 }else{
						 query.setCreateTime(new Date());
						 query.setPayMoney(payMoney);
						 query.setWinMoney(winMoney);
						 query.setWinGain((payMoney.subtract(winMoney)).divide(payMoney, 4, BigDecimal.ROUND_DOWN));
						 query.setGain(payMoney.subtract(winMoney));
						  insertSelective(query);
					 }*/
				}
			} catch (Exception e) {
				logger.error("计算["+bizSystem+"]彩种["+lotteryType+"]在["+orderDate+"]实时赢率异常：",e);
			}
    	}
    	return 0;
    }

    /**
     * 根据条件统计投注人数
     * @param query
     * @return
     */
    public List<DayLotteryGain> getDayLotteryCountByQuery(DayLotteryGainQuery query){
    	return dayLotteryGainMapper.getDayLotteryCountByQuery(query);
    }

}
