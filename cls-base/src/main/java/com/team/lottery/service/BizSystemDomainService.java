package com.team.lottery.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.bizsystemdomain.BizSystemDomainMapper;
import com.team.lottery.vo.BizSystemDomain;

@Service("bizSystemDomainService")
public class BizSystemDomainService {

	@Autowired
	private BizSystemDomainMapper bizSystemDomainMapper ;
	
    public int deleteByPrimaryKey(Long id){ 
    	return bizSystemDomainMapper.deleteByPrimaryKey(id);
    }
    @Transactional(readOnly=false,rollbackFor=Exception.class)
    public int  updateByPrimaryKeySelective(BizSystemDomain bizSystemDomain) throws Exception{
    	//是首推域名，要把其他的更新不是
    	 if(bizSystemDomain.getFirstFlag()==1){
    		 bizSystemDomainMapper.updateFirstUrl(bizSystemDomain);
    	 }
    	 bizSystemDomainMapper.updateByPrimaryKeySelective(bizSystemDomain);
    	
		BizSystemDomain query=new BizSystemDomain();
   		query.setBizSystem(bizSystemDomain.getBizSystem());
   		query.setFirstFlag(1);
   		query.setDomainType(bizSystemDomain.getDomainType());
   		int result=bizSystemDomainMapper.getAllBizSystemDomainByQueryPageCount(query);
   		if(result<1){
   			throw new Exception(bizSystemDomain.getBizSystemName()+"系统"+bizSystemDomain.getDomainType()+"类型至少要有一个首推域名！");
   		}
   		
   		return 1;
    }
    public int insert(BizSystemDomain bizSystemDomain){
    
    	return bizSystemDomainMapper.insert(bizSystemDomain);
    }
    @Transactional(readOnly=false,rollbackFor=Exception.class)
    public int insertSelective(BizSystemDomain bizSystemDomain){
    	//是首推域名，要把其他的更新不是
	   	 if(bizSystemDomain.getFirstFlag()==1){
	   		 bizSystemDomainMapper.updateFirstUrl(bizSystemDomain);
	   	 }else{
	   		BizSystemDomain query=new BizSystemDomain();
	   		query.setBizSystem(bizSystemDomain.getBizSystem());
	   		query.setFirstFlag(1);
	   		query.setDomainType(bizSystemDomain.getDomainType());
	   		int result=bizSystemDomainMapper.getAllBizSystemDomainByQueryPageCount(query);
	   		if(result<1){
	   			bizSystemDomain.setFirstFlag(1);
	   		}
	   	 }
    	  bizSystemDomain.setCreateTime(new Date());
    	 
    	
    	 return bizSystemDomainMapper.insertSelective(bizSystemDomain);
    }

    public BizSystemDomain selectByPrimaryKey(Long id){
    	return bizSystemDomainMapper.selectByPrimaryKey(id);
    }
	
    /**
     * 查找所有的
     * @return
     */
    public Page getAllBizSystemByQueryPage(BizSystemDomain query,Page page){
		page.setTotalRecNum(bizSystemDomainMapper.getAllBizSystemDomainByQueryPageCount(query));
		page.setPageContent(bizSystemDomainMapper.getAllBizSystemDomainByQueryPage(query,page.getStartIndex(),page.getPageSize()));
    	return page;
    }
    
    /**
     * 查询所有记录
     * @return
     */
    public  List<BizSystemDomain> getAllBizSystemDomain(BizSystemDomain query){
    	return bizSystemDomainMapper.getAllBizSystemDomain(query);
    }
}
