package com.team.lottery.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.emailsendconfig.EmailSendConfigMapper;
import com.team.lottery.vo.EmailSendConfig;

@Service
public class EmailSendConfigService {

	@Autowired
	private EmailSendConfigMapper emailSendConfigMapper;
	
	public int deleteByPrimaryKey(Integer id) {
		return emailSendConfigMapper.deleteByPrimaryKey(id);
	}

    public int insertSelective(EmailSendConfig record) {
    	return emailSendConfigMapper.insertSelective(record);
    }

    public EmailSendConfig selectByPrimaryKey(Integer id) {
    	return emailSendConfigMapper.selectByPrimaryKey(id);
    }

    public int updateByPrimaryKeySelective(EmailSendConfig record) {
    	return emailSendConfigMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * 查询所有可用的邮件发送配置
     * @return
     */
    public List<EmailSendConfig> queryAllEmailSendConfig() {
    	return emailSendConfigMapper.queryAllEmailSendConfig();
    }
    
    /**
     * 分页查询
     * @param query
     * @param page
     * @return
     */
    public Page getAllEmailSendConfig(EmailSendConfig query,Page page) {
		page.setTotalRecNum(emailSendConfigMapper.getEmailSendConfigCount(query));
		page.setPageContent(emailSendConfigMapper.getAllEmailSendConfig(query, page.getStartIndex(),page.getPageSize()));
		return page ;
	}
    
}
