package com.team.lottery.service;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.enums.EUserOperateLogModel;
import com.team.lottery.extvo.Page;
import com.team.lottery.extvo.UserOperateLogQuery;
import com.team.lottery.mapper.useroperatelog.UserOperateLogMapper;
import com.team.lottery.vo.UserOperateLog;

@Service("userOperateLogService")
public class UserOperateLogService {

	private static Logger log = LoggerFactory.getLogger(UserOperateLogService.class);

	@Autowired
	private UserOperateLogMapper userOperateLogMapper;
	
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public int deleteByPrimaryKey(Long id){
    	return userOperateLogMapper.deleteByPrimaryKey(id);
    }

	@Transactional(readOnly = false, rollbackFor = Exception.class)
    public int insert(UserOperateLog userOperateLog){
    	return userOperateLogMapper.insert(userOperateLog);
    }

	@Transactional(readOnly = false, rollbackFor = Exception.class)
    public int insertSelective(UserOperateLog userOperateLog){
    	return userOperateLogMapper.insertSelective(userOperateLog);
    }

    public UserOperateLog selectByPrimaryKey(Long id){
    	return userOperateLogMapper.selectByPrimaryKey(id);
    }
    
    /**
     * 保存前台操作日志处理
     * @param bizSystem
     * @param userName
     * @param userId
     * @param model
     * @param operateDesc
     */
    public void saveUserOperateLog(String bizSystem, String operatorIp, String userName, Integer userId, EUserOperateLogModel model, String operateDesc){
    	try {
    		UserOperateLog userOperateLog = new UserOperateLog();
    		userOperateLog.setBizSystem(bizSystem);
    		userOperateLog.setUserName(userName);
    		userOperateLog.setModelName(model.getName());
    		userOperateLog.setOperateDesc(operateDesc);
    		userOperateLog.setUserId(userId);
    		userOperateLog.setOperatorIp(operatorIp);
    		userOperateLog.setCreateDate(new Date());
	        log.info("业务系统[{}],操作人[{}],操作模块[{}],操作内容[{}]", bizSystem, userName, model.getDescription(), operateDesc);
	        userOperateLogMapper.insertSelective(userOperateLog);
    	} catch (Exception e) {
			//保存日志出现异常 不进行处理
			log.error("保存添加操作日志出错,targetName:{},operateDesc:{}", userName, operateDesc, e);
		}
    }
	
    /**
     * 查找所有的登陆后台日志数据
     * @return
     */
    public Page getAllUserOperateLogByQueryPage(UserOperateLogQuery query,Page page){
		page.setTotalRecNum(userOperateLogMapper.getAllUserOperateLogQueryPageCount(query));
		List<UserOperateLog> userOperateLogQueryPage = userOperateLogMapper.getAllUserOperateLogQueryPage(query,page.getStartIndex(),page.getPageSize());
		for(UserOperateLog userOperateLog : userOperateLogQueryPage){
			String modelName = userOperateLog.getModelName();
			EUserOperateLogModel eRecordLogModel = EUserOperateLogModel.valueOf(modelName);
			userOperateLog.setModelName(eRecordLogModel.getDescription());
		}
		page.setPageContent(userOperateLogQueryPage);
		
    	return page;
    }
}
