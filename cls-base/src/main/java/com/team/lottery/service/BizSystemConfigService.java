package com.team.lottery.service;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.extvo.LotterySetMsg;
import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.bizsystemconfig.BizSystemConfigMapper;
import com.team.lottery.vo.BizSystemConfig;

@Service("bizSystemConfigService")
public class BizSystemConfigService {

	@Autowired
	private BizSystemConfigMapper bizSystemConfigMapper;

	public int deleteByPrimaryKey(Long id) {
		return bizSystemConfigMapper.deleteByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(BizSystemConfig bizSystemConfig) {
		/*
		 * if(!this.checkConfigKeyIsExist(bizSystemConfig)) { return -2; }
		 */
		int result = bizSystemConfigMapper
				.updateByPrimaryKeySelective(bizSystemConfig);
		// try {
		// BizSystemConfigManager.refreshBizSystemConfig(bizSystemConfig.getBizSystem());
		// } catch (IllegalAccessException e) {
		// } catch (InvocationTargetException e) {
		// }
		return result;
	}

	public int insert(BizSystemConfig bizSystemConfig) {
		if (!this.checkConfigKeyIsExist(bizSystemConfig)) {
			return -2;
		}
		int result = bizSystemConfigMapper.insert(bizSystemConfig);
		// try {
		// BizSystemConfigManager.refreshBizSystemConfig(bizSystemConfig.getBizSystem());
		// } catch (IllegalAccessException e) {
		// } catch (InvocationTargetException e) {
		// }
		return result;
	}

	public int insertSelective(BizSystemConfig bizSystemConfig) {
		if (!this.checkConfigKeyIsExist(bizSystemConfig)) {
			return -2;
		}
		bizSystemConfig.setCreateDate(new Date());
		int result = bizSystemConfigMapper.insertSelective(bizSystemConfig);
		// try {
		// BizSystemConfigManager.refreshBizSystemConfig(bizSystemConfig.getBizSystem());
		// } catch (IllegalAccessException e) {
		// } catch (InvocationTargetException e) {
		// }
		return result;
	}

	public BizSystemConfig selectByPrimaryKey(Long id) {
		return bizSystemConfigMapper.selectByPrimaryKey(id);
	}

	/**
	 * configKey条件查询
	 * 
	 * @param likeValue
	 * @return
	 */
	public BizSystemConfig getBizSystemConfigByKey(String configKey,
			String bizSystem) {
		return bizSystemConfigMapper.getBizSystemConfigByKey(configKey,
				bizSystem);
	}

	/**
	 * 查找所有的登陆日志数据
	 * 
	 * @return
	 */
	public Page getAllBizSystemConfigByQueryPage(BizSystemConfig query,
			Page page) {
		page.setTotalRecNum(bizSystemConfigMapper
				.getAllBizSystemConfigByQueryPageCount(query));
		page.setPageContent(bizSystemConfigMapper
				.getAllBizSystemConfigByQueryPage(query, page.getStartIndex(),
						page.getPageSize()));
		return page;
	}

	/**
	 * 查询验证configkey是否已经已存在
	 * 
	 * @return
	 */
	@SuppressWarnings("unused")
	private boolean checkConfigKeyIsExist(BizSystemConfig query) {
		Integer count = bizSystemConfigMapper.checkConfigKeyIsExist(query);
		if (count > 0) {
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * 以 SUPER_SYSTEM系统的模板初始化新增的业务系统
	 * @param bizSystem
	 * @param updateAdmin
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void initBizSystemConfigBySql(String bizSystem, String updateAdmin){
		BizSystemConfig query = new BizSystemConfig();
		query.setBizSystem(bizSystem);
		Integer num = bizSystemConfigMapper.getAllBizSystemConfigByQueryPageCount(query);
		if (num>0) {
			throw new RuntimeException("业务系统[{}]"+bizSystem+"已存在");
		}else {
			query.setUpdateAdmin(updateAdmin);
			bizSystemConfigMapper.initBizSystemConfigBySql(query);
		}
	}

	/**
	 * 已无用的方法，被initBizSystemConfigBySql代替
	 * @param bizSystem
	 * @param updateAdmin
	 */
	@Deprecated
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void initBizSystemConfig(String bizSystem, String updateAdmin) {
		List<BizSystemConfig> list = new ArrayList<BizSystemConfig>();
		BizSystemConfig bizSystemConfig = null;
		/**
		 * 初始化配额数
		 */
		bizSystemConfig = new BizSystemConfig(bizSystem, "directagencyNum",
				"5", "直属代理人数配置", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "genralagencyNum",
				"10", "总代理人数配置", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "ordinayagencyNum",
				"15", "普通代理人数配置", updateAdmin);
		list.add(bizSystemConfig);

		/**
		 * 初始化彩种开关
		 */
		bizSystemConfig = new BizSystemConfig(bizSystem, "isCQSSCOpen", "1",
				"重庆时时彩是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isJSSSCOpen", "1",
				"江苏时时彩是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isBJSSCOpen", "1",
				"北京时时彩是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isGDSSCOpen", "1",
				"广东时时彩是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isSJSSCOpen", "1",
				"四川时时彩是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isSHSSCOpen", "1",
				"上海时时彩是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isSDSSCOpen", "1",
				"山东时时彩是否开启", updateAdmin);
		list.add(bizSystemConfig);


		bizSystemConfig = new BizSystemConfig(bizSystem, "isJXSSCOpen", "1",
				"江西时时彩是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isHLJSSCOpen", "1",
				"黑龙江时时彩是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isXJSSCOpen", "1",
				"新疆时时彩是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isTJSSCOpen", "1",
				"天津时时彩是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isJLFFCOpen", "1",
				"吉利分分彩是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isHGFFCOpen", "1",
				"韩国1.5分彩是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isXJPLFCOpen", "1",
				"新加坡2分彩是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isTWWFCOpen", "1",
				"台湾5分彩是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isGDSYXWOpen", "1",
				"广东11选5是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isSDSYXWOpen", "1",
				"山东11选5是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isJXSYXWOpen", "1",
				"江西11选5是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isCQSYXWOpen", "1",
				"重庆11选5是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isFJSYXWOpen", "1",
				"福建11选5是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isJSKSOpen", "1",
				"江苏快3是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isAHKSOpen", "1",
				"安徽快3是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isJLKSOpen", "1",
				"吉林快3是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isHBKSOpen", "1",
				"湖北快3是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isBJKSOpen", "1",
				"北京快3是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isJYKSOpen", "1",
				"幸运快3是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isGXKSOpen", "1",
				"广西快3是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isGDKLSFOpen", "1",
				"广东快乐十分是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isSHSSLDPCOpen", "1",
				"上海时时乐是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isFC3DDPCOpen", "1",
				"福彩3D是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isBJPK10Open", "1",
				"北京PK10是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isJSPK10Open", "1",
				"极速PK10是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isJSTBOpen", "1",
				"江苏骰宝是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isAHTBOpen", "1",
				"安徽骰宝是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isJLTBOpen", "1",
				"吉林骰宝是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isHBTBOpen", "1",
				"湖北骰宝是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isBJTBOpen", "1",
				"北京骰宝是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isLHCOpen", "1",
				"六合彩是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isXYEBOpen", "1",
				"幸运28是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isGSKSOpen", "1",
				"甘肃快3是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isSHKSOpen", "1",
				"上海快3是否开启", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isXYLHCOpen", "1",
				"幸运六合彩是否开启", updateAdmin);
		list.add(bizSystemConfig);
		/**
		 * 初始化元，角，分模式
		 */
		bizSystemConfig = new BizSystemConfig(bizSystem, "yuanModel", "1",
				"元模式", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "jiaoModel", "1",
				"角模式", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "fenModel", "1",
				"分模式", updateAdmin);
		list.add(bizSystemConfig);
		/**
		 * 初始化转账，取现,手续费等，是否启用上下级转账开关
		 */
		bizSystemConfig = new BizSystemConfig(bizSystem,
				"exceedWithdrawExpenses", "1.5", "手续费", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem,
				"withDrawOneStrokeLowestMoney", "100", "单笔最低提现限额", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem,
				"withDrawOneStrokeHighestMoney", "5000", "单笔最高提现限额",
				updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem,
				"withDrawEveryDayTotalMoney", "50000", "每天取现总金额", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem,
				"withDrawEveryDayNumber", "10", "取现次数", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem,
				"exceedTransferDayLimitExpenses", "1000", "每日转账限额", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isTransferSwich",
				"1", "是否启用上下级转账开关", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem,
				"withDrawLotteryMultiple", "1", "进入可提现余额投注额倍数", updateAdmin);
		list.add(bizSystemConfig);

		/**
		 * 初始化契约分红，工资发放策略
		 */
		bizSystemConfig = new BizSystemConfig(bizSystem, "releasePolicyBonus",
				"MANANUAL_RELEASE", "契约分红发放策略", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem,
				"releasePolicyDaySaraly", "MANANUAL_RELEASE", "日工资发放策略",
				updateAdmin);
		list.add(bizSystemConfig);

		/**
		 * 平台系统维护开关 1开启维护 0关闭维护
		 */
		bizSystemConfig = new BizSystemConfig(bizSystem, "maintainSwich", "0",
				"平台系统维护开关 1开启维护  0关闭维护", updateAdmin);
		list.add(bizSystemConfig);
		/**
		 * 维护平台系统说明
		 */
		bizSystemConfig = new BizSystemConfig(bizSystem, "maintainContent",
				"平台系统正在维护中", "维护平台系统说明", updateAdmin);
		list.add(bizSystemConfig);

		/**
		 * 秘密验证码
		 */
		bizSystemConfig = new BizSystemConfig(bizSystem, "secretCode",
				"123456", "秘密验证码", updateAdmin);
		list.add(bizSystemConfig);

		/**
		 * 默认邀请码
		 */
		bizSystemConfig = new BizSystemConfig(bizSystem,
				"defaultInvitationCode", "", "默认邀请码", updateAdmin);
		list.add(bizSystemConfig);

		/**
		 * 系统允许投注最高模式 默认1960
		 */
		bizSystemConfig = new BizSystemConfig(bizSystem,
				"canlotteryhightModel", "1960", "系统允许投注最高投注模式", updateAdmin);
		list.add(bizSystemConfig);

		/**
		 * 注册显示填写的资料默认不填
		 */
		bizSystemConfig = new BizSystemConfig(bizSystem, "regShowPhone", "0",
				"注册是否显示手机号码", updateAdmin);
		list.add(bizSystemConfig);

		bizSystemConfig = new BizSystemConfig(bizSystem, "regRequiredPhone",
				"0", "注册手机号码是否必填", updateAdmin);
		list.add(bizSystemConfig);

		bizSystemConfig = new BizSystemConfig(bizSystem, "regShowEmail", "0",
				"注册是否显示邮件", updateAdmin);
		list.add(bizSystemConfig);

		bizSystemConfig = new BizSystemConfig(bizSystem, "regRequiredEmail",
				"0", "注册邮件是否必填", updateAdmin);
		list.add(bizSystemConfig);

		bizSystemConfig = new BizSystemConfig(bizSystem, "regShowQq", "0",
				"注册是否显示QQ", updateAdmin);
		list.add(bizSystemConfig);

		bizSystemConfig = new BizSystemConfig(bizSystem, "regRequiredQq", "0",
				"注册QQ是否必填", updateAdmin);
		list.add(bizSystemConfig);

		bizSystemConfig = new BizSystemConfig(bizSystem, "regShowSms", "0",
				"是否进行手机验证码", updateAdmin);
		list.add(bizSystemConfig);

		bizSystemConfig = new BizSystemConfig(bizSystem, "regDirectLogin", "1",
				"注册完是否登录", updateAdmin);
		list.add(bizSystemConfig);
		/**
		 * 可提现不足不允许提现 默认可以
		 */
		bizSystemConfig = new BizSystemConfig(bizSystem,
				"withDrawIsAllowNotEnough", "1", "可提现不足不允许提现", updateAdmin);
		list.add(bizSystemConfig);

		/**
		 * 系统六合彩参数设置
		 */
		bizSystemConfig = new BizSystemConfig(bizSystem, "isClose", "0",
				"六合是否封盘", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem,
				"lhcLowerLotteryMoney", "2", "六合彩单注最低限额", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem,
				"lhcHighestLotteryMoney", "5000", "六合彩单注最高限额", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem,
				"lhcHighestExpectLotteryMoney", "100000", "六合彩单场最高投注额",
				updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "lhcMaxWinMoney",
				"250000", "六合彩单注最高返奖限额", updateAdmin);
		list.add(bizSystemConfig);

		/**
		 * 监控金额参数设置
		 */
		bizSystemConfig = new BizSystemConfig(bizSystem, "alarmPhone", "",
				"监控接收短信手机号码", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "alarmEmail", "",
				"监控接收邮件地址", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "alarmPhoneOpen", "0",
				"手机告警开关", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "alarmEmailOpen", "0",
				"监控接收邮件地址", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "lotteryAlarmOpen",
				"0", "投注监控开关", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "lotteryAlarmValue",
				"1000", "投注监控金额", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "winAlarmOpen", "0",
				"中奖监控开关", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "winAlarmValue",
				"5000", "中奖监控金额", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "rechargeAlarmOpen",
				"0", "充值监控开关", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "rechargeAlarmValue",
				"5000", "充值监控金额", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "withdrawAlarmOpen",
				"0", "取现监控开关", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "withdrawAlarmValue",
				"5000", "取现监控金额", updateAdmin);
		list.add(bizSystemConfig);

		/**
		 * 试玩功能设置
		 */
		bizSystemConfig = new BizSystemConfig(bizSystem, "guestModel", "0",
				"是否默认游客模式模式否-0", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "allowGuestLogin",
				"0", "是否允许游客登陆", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "guestMoney", "5000",
				"游客登陆默认金钱", updateAdmin);
		list.add(bizSystemConfig);

		/**
		 * 等级制度奖励
		 */
		bizSystemConfig = new BizSystemConfig(bizSystem, "isOpenLevelAward",
				"0", "等级制度奖励", updateAdmin);
		list.add(bizSystemConfig);
		
		/**
		 * 晋级奖励自动领取开启
		 */
		bizSystemConfig = new BizSystemConfig(bizSystem, "automaticPromotionReceiveSwitch",
				"0", "晋级奖励自动领取", updateAdmin);
		list.add(bizSystemConfig);

		/**
		 * 签到活动
		 */
		bizSystemConfig = new BizSystemConfig(bizSystem,
				"isOpenSignInActivity", "0", "签到活动开启关闭", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "signinRechargeMoney",
				"10", "签到要求充值金额", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "signinLastTime",
				"23:59:59", "每天签到最后时间", updateAdmin);
		list.add(bizSystemConfig);
		/**
		 * 新手注册活动
		 */
		bizSystemConfig = new BizSystemConfig(bizSystem, "isNewUserGiftOpen",
				"0", "新手礼包功能开启关闭", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "appDownGiftMoney",
				"0", "下载App赠送金额", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem,
				"completeInfoGiftMoney", "0", "完善个人资料赠送金额", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem,
				"complateSafeGiftMoney", "0", "完善安全资料赠送金额", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "lotteryGiftMoney",
				"0", "首笔投注赠送金额", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "rechargeGiftMoney",
				"0", "首笔充值赠送金额", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "extendUserGiftMoney",
				"0", "发展新玩家赠送金额", updateAdmin);
		list.add(bizSystemConfig);

		/**
		 * 登陆设置
		 */
		bizSystemConfig = new BizSystemConfig(bizSystem, "isOpenPhone", "0",
				"是否开启手机登陆", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "isOpenEmail", "0",
				"是否开启邮箱登陆", updateAdmin);
		list.add(bizSystemConfig);
		
		bizSystemConfig = new BizSystemConfig(bizSystem, "dayConsumeOrderTimeStart",
				"05:00:00", "流水返送领取开始时间", updateAdmin);
		list.add(bizSystemConfig);
		bizSystemConfig = new BizSystemConfig(bizSystem, "dayConsumeOrderTimeEnd",
				"22:00:00", "流水返送领取结束时间", updateAdmin);
		list.add(bizSystemConfig);

		for (BizSystemConfig bizsyConfig : list) {
			this.insertSelective(bizsyConfig);
		}

		// try {
		// BizSystemConfigManager.refreshBizSystemConfig(bizSystemConfig.getBizSystem());
		// } catch (IllegalAccessException e) {
		// } catch (InvocationTargetException e) {
		// }
	}

	/**
	 * 根据bizsystem查询
	 * 
	 * @param bizSystem
	 * @return
	 */
	public List<BizSystemConfig> getBizSystemConfigByQuery(String bizSystem) {
		List<BizSystemConfig> list = bizSystemConfigMapper
				.getAllBizSystemConfig(bizSystem);
		return list;
	}

	/**
	 * 获取所有的bizsystem
	 * 
	 * @return
	 */
	public List<BizSystemConfig> getAllBizSystems() {
		return bizSystemConfigMapper.getAllBizSystems();
	}

	/**
	 * 模糊条件查询
	 * 
	 * @param likeValue
	 * @return
	 */
	public Map<String, String> getBizSystemConfigByLikeValue(String likeValue,
			String bizSystem) {
		Map<String, String> map = new HashMap<String, String>();
		List<BizSystemConfig> list = bizSystemConfigMapper
				.getBizSystemConfigByLikeValue(likeValue, bizSystem);
		for (BizSystemConfig bizSystemConfig : list) {
			map.put(bizSystemConfig.getConfigKey(),
					bizSystemConfig.getConfigValue());
		}
		return map;
	}

	/**
	 * 更新设置彩种信息
	 * 
	 * @param lotterySetMsg
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void saveOrUpdateLotterySetConfig(LotterySetMsg lotterySetMsg,
			String bizSystem) {
		Field fields[] = lotterySetMsg.getClass().getDeclaredFields();
		try {
			for (Field f : fields) {
				f.setAccessible(true);
				String key = f.getName();
				String value = f.get(lotterySetMsg).toString();
				BizSystemConfig bizSystemConfig = bizSystemConfigMapper
						.getBizSystemConfigByKey(key, bizSystem);
				if (bizSystemConfig != null) {

					bizSystemConfig.setConfigValue(value);
					int result = bizSystemConfigMapper
							.updateByPrimaryKeySelective(bizSystemConfig);
					System.out.println("update " + key + " is " + result);
				} else {
					bizSystemConfig = new BizSystemConfig();
					bizSystemConfig.setBizSystem(bizSystem);
					bizSystemConfig.setConfigKey(key);
					bizSystemConfig.setConfigValue(value);
					bizSystemConfigMapper.insertSelective(bizSystemConfig);
				}
			}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}
}
