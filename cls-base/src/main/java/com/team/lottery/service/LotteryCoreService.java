package com.team.lottery.service;

import java.math.BigDecimal;

import com.team.lottery.enums.ELotteryModel;

public class LotteryCoreService {

	/**
	 * 根据投注的模式,获取对应的金额
	 * @param money
	 * @param model
	 * @return
	 */
	public static BigDecimal getMoneyByLotteryModel(BigDecimal money,String model){
		BigDecimal result = null;
    	if(model.equals(ELotteryModel.YUAN.name())){
    		result = money;
    	}else if(model.equals(ELotteryModel.JIAO.name())){
    		result = money.multiply(new BigDecimal("0.1"));  //转换成角以后的金额
    	}else if(model.equals(ELotteryModel.FEN.name())){
    		result = money.multiply(new BigDecimal("0.01"));  //转换成分以后的金额
    	}else if(model.equals(ELotteryModel.LI.name())){
    		result = money.multiply(new BigDecimal("0.001"));  //转换成分以后的金额
    	}else if(model.equals(ELotteryModel.HAO.name())){
    		result = money.multiply(new BigDecimal("0.0001"));  //转换成分以后的金额
    	}else{
    		throw new RuntimeException("投注模式参数不正确.");
    	}
    	return result;
	}
	

	
	
	
	
	/**
	 * 用户返点值
	 * @param rebateMoneyValue
	 * @param order
	 * @param orderUser
	 */
	/*public static BigDecimal userRebate(Order order,User orderUser,User regFormUser){
		BigDecimal rebateMoneyValue = new BigDecimal(0); //返点值
		BigDecimal intervalRebate = null;
        if(order.getLotteryType().toUpperCase().contains(ELotteryTopKind.SSC.name())){  //时时彩返点
    		if(regFormUser != null){
    			intervalRebate = calculateIntervalRebateByAwardModel(orderUser.getSscRebate(),regFormUser.getSscRebate(),ELotteryTopKind.SSC);
    		}else{
    			if(orderUser.getSscRebate().compareTo(ConstantUtil.SSC_COMPUTE_VALUE) > 0){
        			intervalRebate = calculateIntervalRebateByAwardModel(order.getAwardModel(),ConstantUtil.SSC_COMPUTE_VALUE,ELotteryTopKind.SSC);
    			}else{
    				intervalRebate = calculateIntervalRebateByAwardModel(order.getAwardModel(),orderUser.getSscRebate(),ELotteryTopKind.SSC);
    			}
    		}
        }else if(order.getLotteryType().toUpperCase().contains(ELotteryTopKind.FFC.name()) || 
        		order.getLotteryType().toUpperCase().contains("LFC") ||
        		order.getLotteryType().toUpperCase().contains("WFC")){ //分分彩返点
    		if(regFormUser != null){
    			intervalRebate = calculateIntervalRebateByAwardModel(orderUser.getFfcRebate(),regFormUser.getFfcRebate(),ELotteryTopKind.FFC);
    		}else{
    			if(orderUser.getFfcRebate().compareTo(ConstantUtil.FFC_COMPUTE_VALUE) > 0){
    				intervalRebate = calculateIntervalRebateByAwardModel(order.getAwardModel(),ConstantUtil.FFC_COMPUTE_VALUE,ELotteryTopKind.FFC);
    			}else{
    				intervalRebate = calculateIntervalRebateByAwardModel(order.getAwardModel(),orderUser.getFfcRebate(),ELotteryTopKind.FFC);
    			}
    		}
        }else if(order.getLotteryType().toUpperCase().contains(ELotteryTopKind.SYXW.name())){ //11选5返点
    		if(regFormUser != null){
    			intervalRebate = calculateIntervalRebateByAwardModel(orderUser.getSyxwRebate(),regFormUser.getSyxwRebate(),ELotteryTopKind.SYXW);
    		}else{
    			if(orderUser.getSyxwRebate().compareTo(ConstantUtil.SYXW_COMPUTE_VALUE) > 0){
        			intervalRebate = calculateIntervalRebateByAwardModel(order.getAwardModel(),ConstantUtil.SYXW_COMPUTE_VALUE,ELotteryTopKind.SYXW);
    			}else{
        			intervalRebate = calculateIntervalRebateByAwardModel(order.getAwardModel(),orderUser.getSyxwRebate(),ELotteryTopKind.SYXW);
    			}
    		}
        }else if(order.getLotteryType().toUpperCase().contains(ELotteryTopKind.DPC.name())){ //低频彩返点
    		if(regFormUser != null){
    			intervalRebate = calculateIntervalRebateByAwardModel(orderUser.getDpcRebate(),regFormUser.getDpcRebate(),ELotteryTopKind.DPC);
    		}else{
    			if(orderUser.getDpcRebate().compareTo(ConstantUtil.DPC_COMPUTE_VALUE) > 0){
        			intervalRebate = calculateIntervalRebateByAwardModel(order.getAwardModel(),ConstantUtil.DPC_COMPUTE_VALUE,ELotteryTopKind.DPC);
    			}else{
        			intervalRebate = calculateIntervalRebateByAwardModel(order.getAwardModel(),orderUser.getDpcRebate(),ELotteryTopKind.DPC);
    			}
    		}
        }else if(order.getLotteryType().toUpperCase().contains(ELotteryTopKind.KS.name())){ //快三返点
    		if(regFormUser != null){
    			intervalRebate = calculateIntervalRebateByAwardModel(orderUser.getKsRebate(),regFormUser.getKsRebate(),ELotteryTopKind.KS);
    		}else{
    			if(orderUser.getKsRebate().compareTo(ConstantUtil.KS_COMPUTE_VALUE) > 0){
        			intervalRebate = calculateIntervalRebateByAwardModel(order.getAwardModel(),ConstantUtil.KS_COMPUTE_VALUE,ELotteryTopKind.KS);
    			}else{
        			intervalRebate = calculateIntervalRebateByAwardModel(order.getAwardModel(),orderUser.getKsRebate(),ELotteryTopKind.KS);
    			}
    		}
        }else if(order.getLotteryType().toUpperCase().contains(ELotteryTopKind.PK10.name())){ //北京pk10返点
    		if(regFormUser != null){
    			intervalRebate = calculateIntervalRebateByAwardModel(orderUser.getPk10Rebate(),regFormUser.getPk10Rebate(),ELotteryTopKind.PK10);
    		}else{
    			if(orderUser.getPk10Rebate().compareTo(ConstantUtil.PK10_COMPUTE_VALUE) > 0){
        			intervalRebate = calculateIntervalRebateByAwardModel(order.getAwardModel(),ConstantUtil.PK10_COMPUTE_VALUE,ELotteryTopKind.PK10);
    			}else{
        			intervalRebate = calculateIntervalRebateByAwardModel(order.getAwardModel(),orderUser.getPk10Rebate(),ELotteryTopKind.PK10);
    			}
    		}
        }else{
        	throw new RuntimeException("未实现该彩种的返点");
        }
		rebateMoneyValue = order.getPayMoney().multiply(intervalRebate);
        return rebateMoneyValue;
	}*/
	
	
	
	/**
	 * 
	 * @param currentRebate 计算的奖金模式
	 * @param highRebate 相对的奖金模式 
	 * @param topKind  对应的大彩种 
	 * @return
	 */
	/*public static BigDecimal calculateIntervalRebateByAwardModel(BigDecimal currentRebate,BigDecimal highRebate,ELotteryTopKind topKind){
		BigDecimal intervalRebate = null;
		if(highRebate.compareTo(currentRebate) > 0){
			if(topKind.equals(ELotteryTopKind.SSC)){
				intervalRebate = highRebate.subtract(currentRebate).divide(SystemSet.SSC_INTERVAL);
			}else if(topKind.equals(ELotteryTopKind.FFC)){
				intervalRebate = highRebate.subtract(currentRebate).divide(SystemSet.FFC_INTERVAL);
			}else if(topKind.equals(ELotteryTopKind.SYXW)){
				intervalRebate = highRebate.subtract(currentRebate).divide(SystemSet.SYXW_INTERVAL);
			}else if(topKind.equals(ELotteryTopKind.DPC)){
				intervalRebate = highRebate.subtract(currentRebate).divide(SystemSet.DPC_INTERVAL);
			}else if(topKind.equals(ELotteryTopKind.KS)){
				intervalRebate = highRebate.subtract(currentRebate).divide(SystemSet.KS_INTERVAL);
			}else if(topKind.equals(ELotteryTopKind.PK10)){
				intervalRebate = highRebate.subtract(currentRebate).divide(SystemSet.PK10_INTERVAL);
			}else{
	        	throw new RuntimeException("不存在该彩种的类型");
			}
		}else{
			intervalRebate = new BigDecimal(0);
		}
		return intervalRebate.multiply(SystemSet.intervalPercent);
	}*/
}
