package com.team.lottery.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.smssendrecord.SmsSendRecordMapper;
import com.team.lottery.vo.SmsSendRecord;

@Service("smsSendRecordService")
public class SmsSendRecordService {
	@Autowired
	private SmsSendRecordMapper smsSendRecordMapper;
    public int insertSelective(SmsSendRecord record) {
    	return smsSendRecordMapper.insertSelective(record);
    }
	/**
	 * 根据sessionid 查询最近的验证码
	 * @param bizSystem
	 * @param sessionId
	 * @return
	 */
    public List<SmsSendRecord> queryAllSmsSendRecord(SmsSendRecord query){   
		   return smsSendRecordMapper.queryAllSmsSendRecord(query);
    }
    
    /**
	 * 分页查询
	 * @param query
	 * @param page
	 * @return
	 */
	public Page getAllSmsSendRecord(SmsSendRecord query,Page page) {
		page.setTotalRecNum(smsSendRecordMapper.getSmsSendRecord(query));
		page.setPageContent(smsSendRecordMapper.getAllSmsSendRecord(query, page.getStartIndex(),page.getPageSize()));
		return page ;
	}
	
	public SmsSendRecord selectByPrimaryKey(Long id){
		return smsSendRecordMapper.selectByPrimaryKey(id);
		
	}

	public int updateByPrimaryKeySelective(SmsSendRecord record){
		return smsSendRecordMapper.updateByPrimaryKeySelective(record);
		
	}
	
	public int deleteByPrimaryKey(Long id){
		return smsSendRecordMapper.deleteByPrimaryKey(id);
		
	}
    

}
