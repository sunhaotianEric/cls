package com.team.lottery.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.levelsystem.LevelSystemMapper;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.LevelSystem;


@Service
public class LevelSytemService {

	@Autowired
	private LevelSystemMapper levelSystemMapper;


    /**
     * 查询两个等级之间的晋级奖励的总和
     * @param startLevel,endLevel,bizSystem
     * @return
     */
	public BigDecimal selectLevelBetween(Integer startLevel,Integer endLevel,String bizSystem){
    	return levelSystemMapper.selectLevelBetween(startLevel, endLevel,bizSystem);
    }
	/**
     * 查询当前积分对应的最大积分奖励设置
     * @param point,bizSystem
     * @return
     */
	public List<LevelSystem> selectMaxLevel(Integer point,String bizSystem){
    	return levelSystemMapper.selectMaxLevel(point,bizSystem);
    }
	
	/**
	 * 分页查询会员等级机制会员等级机制
	 * @param query
	 * @return
	 */
	public Page getLevelSytem(LevelSystem query,Page page){
		page.setTotalRecNum(levelSystemMapper.getLevelSytemCount(query));
		page.setPageContent(levelSystemMapper.getLevelSytem(query, page.getStartIndex(),page.getPageSize()));
		return page;
	}
	
	/**
	 * 根据ID删除会员等级机制
	 * @param id
	 * @return
	 */
	public int deleteByPrimaryKey(int id){
		return levelSystemMapper.deleteByPrimaryKey(id);
	}
	
	/**
	 * 添加会员等级机制
	 * @param record
	 * @return
	 */
	public int addLevelSystem(LevelSystem record){
		record.setCreateDate(new Date());
		return levelSystemMapper.insertSelective(record);
	}
	
	/**
	 * 根据ID修改会员等级机制
	 * @param record
	 * @return
	 */
	public int updateLevelSystem(LevelSystem record, Admin currentAdmin){
		record.setUpdateAdmin(currentAdmin.getUsername());
		record.setUpdateTime(new Date());
		return levelSystemMapper.updateByPrimaryKeySelective(record);
	}
	
	/**
	 * 根据ID查询单个会员等级机制
	 * @param id
	 * @return
	 */
	public LevelSystem selectByPrimaryKey(int id){
		return levelSystemMapper.selectByPrimaryKey(id);
	}
	
	/**
	 * 检测同个业务系统下是否存在相同的vip等级
	 * @param record
	 * @return
	 */
	public int getLevelSystemBylevel(LevelSystem record){
		return levelSystemMapper.getLevelSystemBylevel(record);
	}
	
	/**
	 * 查询某个业务系统下的等级机制
	 * @param record
	 * @return
	 */
	public List<LevelSystem> getLevelSystemByBizSysgtem(LevelSystem record){
		return levelSystemMapper.getLevelSystemByBizSystem(record);
	}
	
	/**
	 * 查询当前积分到充值可以晋升的积分
	 * @param currentpoint
	 * @param point
	 * @param bizSystem
	 * @return
	 */
	public List<LevelSystem> selectDifferenceLevel(Integer currentpoint, Integer point,String bizSystem){
    	return levelSystemMapper.selectDifferenceLevel(currentpoint, point, bizSystem);
    }
}
