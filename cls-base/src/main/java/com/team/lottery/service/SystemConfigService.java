package com.team.lottery.service;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.extvo.LotterySetMsg;
import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.systemconfig.SystemConfigMapper;
import com.team.lottery.vo.SystemConfig;

@Service("systemConfigService")
public class SystemConfigService {

	@Autowired
	private SystemConfigMapper systemConfigMapper;
	
    public int deleteByPrimaryKey(Long id){ 
    	return systemConfigMapper.deleteByPrimaryKey(id);
    }
    public int  updateByPrimaryKeySelective(SystemConfig systemConfig){
    	if(!this.checkConfigKeyIsExist(systemConfig))
    	{
    		return -2;	
    	}
    	return systemConfigMapper.updateByPrimaryKeySelective(systemConfig);
    }
    public int insert(SystemConfig systemConfig){
    	if(!this.checkConfigKeyIsExist(systemConfig))
    	{
    		return -2;	
    	}
    	return systemConfigMapper.insert(systemConfig);
    }

    public int insertSelective(SystemConfig systemConfig){
    	if(!this.checkConfigKeyIsExist(systemConfig))
    	{
    		return -2;	
    	}
    	systemConfig.setCreateDate(new Date());
    	return systemConfigMapper.insertSelective(systemConfig);
    }

    public SystemConfig selectByPrimaryKey(Long id){
    	return systemConfigMapper.selectByPrimaryKey(id);
    }
    
    public SystemConfig getSystemConfigByKey(String configKey){
    	return systemConfigMapper.getSystemConfigByKey(configKey);
    }
	
    /**
     * 查找所有
     * @return
     */
    public Page getAllSystemConfigByQueryPage(SystemConfig query,Page page){
		page.setTotalRecNum(systemConfigMapper.getAllSystemConfigByQueryPageCount(query));
		page.setPageContent(systemConfigMapper.getAllSystemConfigByQueryPage(query,page.getStartIndex(),page.getPageSize()));
    	return page;
    }
    
    /**
     * 查找所有
     * @return
     */
    public List<SystemConfig> getAllSystemConfig(){
    	return systemConfigMapper.getAllSystemConfig();
    }
    
    /**
     * 查询验证configkey是否已经已存在
     * @return
     */
    @SuppressWarnings("unused")
	private boolean checkConfigKeyIsExist(SystemConfig query){
    	Integer count=systemConfigMapper.checkConfigKeyIsExist(query);
    	if(count>0)
    	{
    		return false;
    	}else{
    		return true;
    	}
    }
    
	
    /**
     * 模糊条件查询
     * @param likeValue
     * @return
     */
    public Map<String,String> getSystemConfigByLikeValue(String likeValue){
    	Map<String,String> map=new HashMap<String, String>();
    	List<SystemConfig> list=systemConfigMapper.getSystemConfigByLikeValue(likeValue);
    	for(SystemConfig systemConfig:list){
    		map.put(systemConfig.getConfigKey(), systemConfig.getConfigValue());
    	}
    	 return map;
    }
    
    /**
     * 更新设置彩种信息
     * @param lotterySetMsg 
     */
    @Transactional(readOnly=false,rollbackFor=Exception.class)
    public void saveOrUpdateLotterySetConfig(LotterySetMsg lotterySetMsg){
    	Field fields[]=lotterySetMsg.getClass().getDeclaredFields();
    	try {
    	  for(Field f:fields){
    		  f.setAccessible(true);
    		  String key=f.getName();
    		  String value=f.get(lotterySetMsg)==null?"0":f.get(lotterySetMsg).toString();
    		  SystemConfig systemConfig=systemConfigMapper.getSystemConfigByKey(key);
    		  if(systemConfig!=null){
    			  
    		    systemConfig.setConfigValue(value);
    		    int result=systemConfigMapper.updateByPrimaryKeySelective(systemConfig); 

    		  }else{
    			  systemConfig=new SystemConfig();
    			  systemConfig.setConfigKey(key);
    			  systemConfig.setConfigValue(value);
    			  systemConfigMapper.insertSelective(systemConfig);
    		  }
    	  }
    	} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
    }
    
    public List<SystemConfig> getSystemConfig(){
		return systemConfigMapper.getSystemConfig();
    	
    }
}
