package com.team.lottery.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.alarminfo.AlarmInfoMapper;
import com.team.lottery.vo.AlarmInfo;

@Service
public class AlarmInfoService {

	@Autowired
	private AlarmInfoMapper alarmInfoMapper;
	
	public int deleteByPrimaryKey(Integer id) {
		return alarmInfoMapper.deleteByPrimaryKey(id);
	}

    public int insertSelective(AlarmInfo record) {
    	return alarmInfoMapper.insertSelective(record);
    }

    public AlarmInfo selectByPrimaryKey(Integer id) {
    	return alarmInfoMapper.selectByPrimaryKey(id);
    }

    public int updateByPrimaryKeySelective(AlarmInfo record) {
    	return alarmInfoMapper.updateByPrimaryKeySelective(record);
    }
    
    public Page getAlarminfoPage(AlarmInfo record,Page page){
    	page.setTotalRecNum(alarmInfoMapper.getAlarminfoPageCount(record));
    	page.setPageContent(alarmInfoMapper.getAlarminfoPage(record, page.getStartIndex(),page.getPageSize()));
    	return page;
    }
    
    public List<AlarmInfo> queryAlarmInfoBytime(AlarmInfo record){
    	return alarmInfoMapper.queryAlarmInfoBytime(record);
    }

}
