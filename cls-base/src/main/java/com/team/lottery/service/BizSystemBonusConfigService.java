package com.team.lottery.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.mapper.bizsystembonusconfig.BizSystemBonusConfigMapper;
import com.team.lottery.vo.BizSystemBonusConfig;

@Service
public class BizSystemBonusConfigService {
	@Autowired
	private BizSystemBonusConfigMapper bizSystemBonusConfigMapper;
	
	public int deleteByPrimaryKey(Integer id){
		return bizSystemBonusConfigMapper.deleteByPrimaryKey(id);
		 
	 }
	 public int insertSelective(BizSystemBonusConfig record){
    	  return bizSystemBonusConfigMapper.insertSelective(record);
      }

	 public  BizSystemBonusConfig selectByPrimaryKey(Integer id){
    	  return bizSystemBonusConfigMapper.selectByPrimaryKey(id);
      }

	 public int updateByPrimaryKeySelective(BizSystemBonusConfig record){
    	  return bizSystemBonusConfigMapper.updateByPrimaryKeySelective(record);
      }
     public List<BizSystemBonusConfig> getBizSystemBonusConfigByCondition(BizSystemBonusConfig query){
    	 return bizSystemBonusConfigMapper.getBizSystemBonusConfigByCondition(query);
     }
     
     public int bathBizSystemBonusConfigs(String deleteBizSystem,List<BizSystemBonusConfig> records){
    	 
    	 bizSystemBonusConfigMapper.deleteByPrimaryBizSystem(deleteBizSystem);
    	 for(BizSystemBonusConfig bizSystemBonusConfig:records){
    		 bizSystemBonusConfig.setBizSystem(deleteBizSystem);
    		 bizSystemBonusConfig.setCreateTime(new Date());
    		 int result = this.insertSelective(bizSystemBonusConfig);
    		 if(result < 1){
    			 return 0;
    		 }
    	 }
    	 return 1;
     }
     
}
