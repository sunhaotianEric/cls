package com.team.lottery.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.shareorder.ShareOrderMapper;
import com.team.lottery.vo.ShareOrder;

@Service
public class ShareOrderService {

	@Autowired
	private ShareOrderMapper shareOrderMapper;
	
	public int insertSelective(ShareOrder record){
		return shareOrderMapper.insertSelective(record);
	}
	
	/**
	 * 分页查询分享投注记录
	 * @param record
	 * @param page
	 * @return
	 */
	public Page getShareOrderPage(ShareOrder record,Page page){
		page.setPageContent(shareOrderMapper.getShareOrderPage(record, page.getStartIndex(), page.getPageSize()));
		page.setTotalRecNum(shareOrderMapper.getShareOrderPageCount(record));
		return page;
	}
}
