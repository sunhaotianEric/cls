package com.team.lottery.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.bizsysteminfo.BizSystemInfoMapper;
import com.team.lottery.vo.BizSystemInfo;

@Service("BizSystemInfoService")
public class BizSystemInfoService {

	@Autowired
	private BizSystemInfoMapper bizSystemInfoMapper;
	
    public int deleteByPrimaryKey(Long id){ 
    	return bizSystemInfoMapper.deleteByPrimaryKey(id);
    }
    
    @Transactional(readOnly=false,rollbackFor=Exception.class)
    public int  updateByPrimaryKeySelective(BizSystemInfo bizSystemInfo){
    	return bizSystemInfoMapper.updateByPrimaryKeySelective(bizSystemInfo);
    }
    
    public int insert(BizSystemInfo bizSystemInfo){
    	return bizSystemInfoMapper.insert(bizSystemInfo);
    }

    public int insertSelective(BizSystemInfo bizSystemInfo){
    	bizSystemInfo.setCreateTime(new Date());
    	return bizSystemInfoMapper.insertSelective(bizSystemInfo);
    }

    public BizSystemInfo selectByPrimaryKey(Long id){
    	return bizSystemInfoMapper.selectByPrimaryKey(id);
    }
	
    /**
     * 查找所有的
     * @return
     */
    public Page getAllBizSystemInfoByQueryPage(BizSystemInfo query,Page page){
		page.setTotalRecNum(bizSystemInfoMapper.getAllBizSystemInfoByQueryPageCount(query));
		page.setPageContent(bizSystemInfoMapper.getAllBizSystemInfoByQueryPage(query,page.getStartIndex(),page.getPageSize()));
    	return page;
    }
    
    /**
     * 根据条件查询,单个对象
     * @param query
     * @return
     */
    public BizSystemInfo getBizSystemInfoByCondition(BizSystemInfo query){
    	List<BizSystemInfo> list=bizSystemInfoMapper.getBizSystemInfoByCondition(query);
    	if(list.size()>0) {
    		return list.get(0);	
    	}else{
    		return null;	
    	}
    }
    
    /**
     * 查询所有的
     * @param query
     * @return
     */
    public List<BizSystemInfo> getAllBizSystemInfo(){
    	BizSystemInfo query=new BizSystemInfo();
    	List<BizSystemInfo> list=bizSystemInfoMapper.getBizSystemInfoByCondition(query);
    	return list;
    }
}
