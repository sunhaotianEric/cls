package com.team.lottery.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.bizsystem.BizSystemMapper;
import com.team.lottery.mapper.bizsysteminfo.BizSystemInfoMapper;
import com.team.lottery.mapper.config.ConfigMapper;
import com.team.lottery.mapper.levelsystem.LevelSystemMapper;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.util.AwardModelUtil;
import com.team.lottery.vo.Admin;
import com.team.lottery.vo.AwardModelConfig;
import com.team.lottery.vo.BizSystem;
import com.team.lottery.vo.BizSystemInfo;
import com.team.lottery.vo.LevelSystem;

@Service("bizSystemService")
public class BizSystemService {
	private static Logger logger = LoggerFactory.getLogger(BizSystemService.class);
	@Autowired
	private BizSystemMapper bizSystemMapper;
	
	@Autowired
	private BizSystemInfoMapper bizSystemInfoMapper ;
	
	@Autowired
	private BizSystemConfigService bizSystemConfigService;
	
	@Autowired
	private AwardModelConfigService awardModelConfigService;
	
	@Autowired
	private BizSystemInfoService bizSystemInfoService;
	
	@Autowired
	private BizSystemBonusConfigService bizSystemBonusConfigService;
	
	@Autowired
	private LevelSystemMapper levelSystemMapper;
	
	@Autowired
	private ConfigMapper configMapper;
	
	@Autowired
	private LotterySwitchService lotterySwitchService;
	
	
    public int deleteByPrimaryKey(Long id){ 
    	return bizSystemMapper.deleteByPrimaryKey(id);
    }
    
    @Transactional(readOnly=false,rollbackFor=Exception.class)
    public int updateByPrimaryKeySelective(BizSystem bizSystem){
    	BizSystem oldBizSystem = bizSystemMapper.selectByPrimaryKey(bizSystem.getId());
    	int result = bizSystemMapper.updateByPrimaryKeySelective(bizSystem);
    	if(result > 0){
    		//业务系统名称修改，同步刷新bizSystemInfo，同时刷新缓存！
			if(!oldBizSystem.getBizSystemName().equals(bizSystem.getBizSystemName())){
				BizSystemInfo	bizSystemInfo = new BizSystemInfo();
				bizSystemInfo.setBizSystem(bizSystem.getBizSystem());
				bizSystemInfo = bizSystemInfoService.getBizSystemInfoByCondition(bizSystemInfo);
				BizSystemInfo updateBizSystemInfo = new BizSystemInfo();
				updateBizSystemInfo.setId(bizSystemInfo.getId());
				updateBizSystemInfo.setBizSystem(bizSystemInfo.getBizSystem());
				updateBizSystemInfo.setBizSystemName(bizSystem.getBizSystemName());
				bizSystemInfoService.updateByPrimaryKeySelective(updateBizSystemInfo);
			}
			//getBizSystemBonusConfigList
			if(bizSystem.getBizSystemBonusConfigList() != null){
				//插入分红比例表
				bizSystemBonusConfigService.bathBizSystemBonusConfigs(bizSystem.getBizSystem(),bizSystem.getBizSystemBonusConfigList());	
			}
    	}
		return result;
    }
    
    //@CachePut(value="bizSystem")
    public int insert(BizSystem bizSystem){
    	return bizSystemMapper.insert(bizSystem);
    }

    @Transactional(readOnly=false,rollbackFor=Exception.class)
    public int insertSelective(BizSystem bizSystem){
    	bizSystem.setCreateTime(new Date());
    	int result=bizSystemMapper.insertSelective(bizSystem);
    	return result;
    }

    public BizSystem selectByPrimaryKey(Long id){
    	return bizSystemMapper.selectByPrimaryKey(id);
    }
    
    /**
     * 保存业务系统逻辑
     * @param bizSystem
     * @return
     */
    @Transactional(readOnly=false,rollbackFor=Exception.class)
    public int saveBizSystem(BizSystem bizSystem, Admin currentAdmin){
    	
    	int result = this.insertSelective(bizSystem);
    	
   	 	//插入成功，系统信息管理同步插入一条数据
    	BizSystemInfo bizSystemInfo=new BizSystemInfo();
    	bizSystemInfo.setBizSystem(bizSystem.getBizSystem());
    	bizSystemInfo.setBizSystemName(bizSystem.getBizSystemName());
    	bizSystemInfo.setIndexLotteryKind("CQSSC");
    	bizSystemInfo.setHotLotteryKinds("CQSSC,JSKS,BJPK10,TJSSC,SDXYXW,TWFFC,FC3DDPC");
    	bizSystemInfo.setIndexDefaultLotteryKinds("CQSSC,JSKS,BJPK10");
    	bizSystemInfo.setIndexMobileDefaultLotteryKinds("CQSSC,JSKS,BJPK10");
    	bizSystemInfo.setHeadLogoUrl("http://"+SystemConfigConstant.picHost+"/images/logo/defaultHeadLogo.png");
    	bizSystemInfo.setLogoUrl("http://"+SystemConfigConstant.picHost+"/images/logo/defaultLogo.png");
    	bizSystemInfo.setMobileLogoUrl("http://"+SystemConfigConstant.picHost+"/images/logo/defaultMobileLogo.png");
    
    	bizSystemInfo.setRechargeSecond(new BigDecimal("6"));
    	bizSystemInfo.setWithdrawSecond(new BigDecimal("9"));
    	bizSystemInfo.setCustomUrl("http://www.53kf.com/");
    	bizSystemInfo.setTelphoneNum("12345678901");
    	bizSystemInfo.setHasApp(0);
    	bizSystemInfo.setBarcodeUrl("http://"+SystemConfigConstant.picHost+"/images/barcode/barcode.png");
    	bizSystemInfo.setMobileBarcodeUrl("http://"+SystemConfigConstant.picHost+"/images/barcode/mobileBarcode.png");
    	bizSystemInfo.setAppBarcodeUrl("http://"+SystemConfigConstant.picHost+"/images/barcode/appBarcode.png");
    	bizSystemInfo.setFooter("COPYRIGHT @ 2016 LOTTERY CARPARINTCN.AII RTGHIS RESCRVCD.");
    	bizSystemInfo.setRechargeSecond(new BigDecimal("6"));
    	bizSystemInfo.setWithdrawSecond(new BigDecimal("9"));
    	bizSystemInfo.setCreateTime(new Date());
    	bizSystemInfo.setUpdateAdmin(currentAdmin.getUsername());
    	bizSystemInfo.setUpdateTime(new Date());
    	bizSystemInfoMapper.insertSelective(bizSystemInfo);
    	
    	//插入分红比例表
		bizSystemBonusConfigService.bathBizSystemBonusConfigs(bizSystem.getBizSystem(),bizSystem.getBizSystemBonusConfigList());
       
    	//插入成功后，一个业务系统配额参数配置
    	bizSystemConfigService.initBizSystemConfigBySql(bizSystem.getBizSystem(),currentAdmin.getUsername());
    	
    	// 系统插入成功过后,添加业务系统彩种开关设置.
    	lotterySwitchService.insertBizSystemLotterySwitchBySql(bizSystem.getBizSystem(),currentAdmin.getUsername());
    	
    	//插入业务系统奖金模式配置表
    	AwardModelConfig awardModelConfig = new AwardModelConfig();
    	awardModelConfig.setBizSystem(bizSystem.getBizSystem());
    	
    	BigDecimal sscLowestAwardModel = bizSystem.getLowestAwardModel();
    	BigDecimal sscHighestAwardModel = bizSystem.getHighestAwardModel();
    	BigDecimal ffcLowestAwardModel = AwardModelUtil.getModelBySsc(sscLowestAwardModel, ELotteryTopKind.FFC);
    	BigDecimal ffcHighestAwardModel = AwardModelUtil.getModelBySsc(sscHighestAwardModel, ELotteryTopKind.FFC);
    	BigDecimal syxwLowestAwardModel = AwardModelUtil.getModelBySsc(sscLowestAwardModel, ELotteryTopKind.SYXW);
    	BigDecimal syxwHighestAwardModel = AwardModelUtil.getModelBySsc(sscHighestAwardModel, ELotteryTopKind.SYXW);
    	BigDecimal ksLowestAwardModel = AwardModelUtil.getModelBySsc(sscLowestAwardModel, ELotteryTopKind.KS);
    	BigDecimal ksHighestAwardModel = AwardModelUtil.getModelBySsc(sscHighestAwardModel, ELotteryTopKind.KS);
    	BigDecimal tbLowestAwardModel = AwardModelUtil.getModelBySsc(sscLowestAwardModel, ELotteryTopKind.TB);
    	BigDecimal tbHighestAwardModel = AwardModelUtil.getModelBySsc(sscHighestAwardModel, ELotteryTopKind.TB);
    	BigDecimal dpcLowestAwardModel = AwardModelUtil.getModelBySsc(sscLowestAwardModel, ELotteryTopKind.DPC);
    	BigDecimal dpcHighestAwardModel = AwardModelUtil.getModelBySsc(sscHighestAwardModel, ELotteryTopKind.DPC);
    	BigDecimal pk10LowestAwardModel = AwardModelUtil.getModelBySsc(sscLowestAwardModel, ELotteryTopKind.PK10);
    	BigDecimal pk10HighestAwardModel = AwardModelUtil.getModelBySsc(sscHighestAwardModel, ELotteryTopKind.PK10);
    	BigDecimal klsfLowestAwardModel = AwardModelUtil.getModelBySsc(sscLowestAwardModel, ELotteryTopKind.KLSF);
    	BigDecimal klsfHighestAwardModel = AwardModelUtil.getModelBySsc(sscHighestAwardModel, ELotteryTopKind.KLSF);
    	BigDecimal lhcLowestAwardModel = AwardModelUtil.getModelBySsc(sscLowestAwardModel, ELotteryTopKind.LHC);
    	BigDecimal lhcHighestAwardModel = AwardModelUtil.getModelBySsc(sscHighestAwardModel, ELotteryTopKind.LHC);
    	
    	
    	awardModelConfig.setSscLowestAwardModel(sscLowestAwardModel);
    	awardModelConfig.setSscHighestAwardModel(sscHighestAwardModel);
    	awardModelConfig.setFfcLowestAwardModel(ffcLowestAwardModel);
    	awardModelConfig.setFfcHighestAwardModel(ffcHighestAwardModel);
    	awardModelConfig.setSyxwLowestAwardModel(syxwLowestAwardModel);
    	awardModelConfig.setSyxwHighestAwardModel(syxwHighestAwardModel);
    	awardModelConfig.setKsLowestAwardModel(ksLowestAwardModel);
    	awardModelConfig.setKsHighestAwardModel(ksHighestAwardModel);
    	awardModelConfig.setTbLowestAwardModel(tbLowestAwardModel);
    	awardModelConfig.setTbHighestAwardModel(tbHighestAwardModel);
    	awardModelConfig.setDpcLowestAwardModel(dpcLowestAwardModel);
    	awardModelConfig.setDpcHighestAwardModel(dpcHighestAwardModel);
    	awardModelConfig.setPk10LowestAwardModel(pk10LowestAwardModel);
    	awardModelConfig.setPk10HighestAwardModel(pk10HighestAwardModel);
    	awardModelConfig.setKlsfLowestAwardModel(klsfLowestAwardModel);
    	awardModelConfig.setKlsfHighestAwardModel(klsfHighestAwardModel);
    	awardModelConfig.setLhcLowestAwardModel(lhcLowestAwardModel);
    	awardModelConfig.setLhcHighestAwardModel(lhcHighestAwardModel);
    	awardModelConfigService.insertSelective(awardModelConfig);
    	
    	//等级机制默认
    	List<LevelSystem> levelSystemlist=new ArrayList<LevelSystem>();
    	String[] list={"农民","地主","知县","通判","知府","总督","巡抚","丞相","帝王"};
    	String[] pointlist={"0","10","200","1000","10000","50000","250000","1000000","5000000"};
    	String[] promotionlist={"0","1","5","10","58","318","1688","6888","38888"};
    	String[] skiplist={"0","1","6","16","74","392","2080","8968","47856"};
    	for(int i=1;i<10;i++){
    		LevelSystem levelSystem=new LevelSystem();
    		levelSystem.setBizSystem(bizSystem.getBizSystem());
    		levelSystem.setLevel(i);
    		levelSystem.setLevelName(list[i-1]);
    		levelSystem.setPoint(Integer.parseInt(pointlist[i-1]));
    		levelSystem.setPromotionAward(new BigDecimal(promotionlist[i-1]));
    		levelSystem.setSkipAward(new BigDecimal(skiplist[i-1]));
    		levelSystem.setCreateDate(new Date());
    		levelSystemlist.add(levelSystem);
    	}
    	levelSystemMapper.addEmps(levelSystemlist);
    	return result;
    }
	
    /**
     * 分页查找所有的业务系统
     * @return
     */
    public Page getAllBizSystemByQueryPage(BizSystem query,Page page){
		page.setTotalRecNum(bizSystemMapper.getAllBizSystemByQueryPageCount(query));
		page.setPageContent(bizSystemMapper.getAllBizSystemByQueryPage(query,page.getStartIndex(),page.getPageSize()));
    	return page;
    }
    
    /**
     * 查询所有的业务系统
     * @return
     */
    //@Cacheable(value="bizSystem")
    public List<BizSystem> getAllBizSystem() {
    	return bizSystemMapper.getAllBizSystem();
    }
    
    /**
     * 查找查询启用中的业务系统 
     * @return
     */
    public  List<BizSystem> getAllEnableBizSystem(){
    	return bizSystemMapper.getAllEnableBizSystem();
    }
    
    /**
     * 查找bizsystem查出唯一对象 
     * @return
     */
    public  BizSystem  getBizSystemByCondition(BizSystem bizSystem){
    	return bizSystemMapper.getBizSystemByCondition(bizSystem);
    }
    
    /**
     * 统计业务系统的总分红，维护收入
     * @param query
     * @return
     */
    public List<BizSystem> getALLBizSystemIncome(BizSystem query){
    	return bizSystemMapper.getALLBizSystemIncome(query);
    }
    
    /**
     * 根据业务系统删除业务系统
     * @param bizSystem
     */
    @Transactional(readOnly=false,rollbackFor=Exception.class)
    public void delBizSystemByAllBizSystem(String bizSystem){
    	logger.info("开始删除数据库表数据,业务系统[{}]....", bizSystem);
    	//业务系统数据相关表
    	configMapper.deleteBizSystem(bizSystem);
    	logger.info("删除【"+bizSystem+"】业务系统kr_biz_system表数据完成。。。。");
    	configMapper.deleteBizSystemBonusConfig(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_biz_system_bonus_config表数据完成。。。。");
        configMapper.deleteBizSystemConfig(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_biz_system_config表数据完成。。。。");
        configMapper.deleteBizSystemDomain(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_biz_system_domain表数据完成。。。。");
        configMapper.deleteAwardModelConfig(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_award_model_config表数据完成。。。。");
        configMapper.deleteDomainInvitationCode(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_domain_invitation_code表数据完成。。。。");
        configMapper.deleteLevelSystem(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_level_system表数据完成。。。。");
        
        configMapper.deleteAdmin(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_admin表数据完成。。。。");
        configMapper.deleteAdminRecvNote(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_admin_recv_note表数据完成。。。。");
        configMapper.deleteAlarmInfo(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_alarm_info表数据完成。。。。");
        //kr_admin_domain
        configMapper.deleteAdminDomain(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_admin_domain表数据完成。。。。");
        
        // 根据业务系统删除操作日志记录数据.
        configMapper.deleteAdminOperateLog(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_admin_operate_log表数据完成。。。。");
        configMapper.deleteUserOperateLog(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_user_operate_log表数据完成。。。。");
        
        //configMapper.deleteDayProfit(bizSystem);
        //logger.info("删除【"+bizSystem+"】业务系统kr_day_profit表数据完成。。。。");
        configMapper.deleteDayOnlineInfo(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_day_online_info表数据完成。。。。");
        configMapper.deleteLotteryDayGain(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_lottery_day_gain表数据完成。。。。");
        
        //业务系统设置数据相关
        configMapper.deleteBizSystemInfo(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_biz_system_info表数据完成。。。。");
        configMapper.deleteActivity(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_activity表数据完成。。。。");
        configMapper.deleteHomePic(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_home_pic表数据完成。。。。");
        configMapper.deleteHelp(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_help表数据完成。。。。");
        configMapper.deleteHelps(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_helps表数据完成。。。。");
        configMapper.deleteAnnounce(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_announce表数据完成。。。。");
        
        //开奖相关  kr_lottery_code_xy_trend  kr_lottery_code_xy_import  kr_lottery_code_xy
        configMapper.deleteLotteryCodeXy(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_lottery_code_xy表数据完成。。。。");
        configMapper.deleteLotteryCodeXyImport(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_lottery_code_xy_import表数据完成。。。。");
        configMapper.deleteLotteryControlConfig(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_lottery_control_config表数据完成。。。。");
        
        //赔率数据
        configMapper.deleteLotteryWinLhc(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_biz_lottery_win_lhc表数据完成。。。。");
        configMapper.deleteLotteryWinXyeb(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_lottery_win_xyeb表数据完成。。。。");
        
        //用户相关       
        configMapper.deleteUser(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_user表数据完成。。。。");
        configMapper.deleteUserMoneyTotal(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_user_money_total表数据完成。。。。");
        configMapper.deleteUserBank(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_user_bank表数据完成。。。。");
        configMapper.deleteUserRegister(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_user_register表数据完成。。。。");
        configMapper.deleteQuotaControl(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_quota_control表数据完成。。。。");
        configMapper.deleteUserQuotaDetail(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_user_quota_detail表数据完成。。。。");
        configMapper.deleteUserQuota(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_user_quota表数据完成。。。。");
        configMapper.deleteLoginLog(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_login_log表数据完成。。。。");
        configMapper.deleteMoneyDetail(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_money_detail表数据完成。。。。");
        configMapper.deleteNotes(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_notes表数据完成。。。。");
        configMapper.deleteUserSafetyQuestion(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_user_safety_question表数据完成。。。。");
        configMapper.deleteUserCookieLog(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_user_cookie_log表数据完成。。。。");
        
        //用户报表相关
        configMapper.deleteUserHourConsume(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_user_hour_consume表数据完成。。。。");
        configMapper.deleteUserDayConsume(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_user_day_consume表数据完成。。。。");
        configMapper.deleteUserSelfDayConsume(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_user_self_day_consume表数据完成。。。。");
        configMapper.deleteOrder(bizSystem);
        
        //用户订单相关
        logger.info("删除【"+bizSystem+"】业务系统kr_order表数据完成。。。。");
        configMapper.deleteLotteryCache(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_lottery_cache表数据完成。。。。");
        configMapper.deleteOrderAfterRecord(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_order_after_record表数据完成。。。。");
        configMapper.deleteOrderAmount(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_order_adount表数据完成。。。。");
        configMapper.deleteUserDayLotteryCount(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_user_day_lottery_count表数据完成。。。。");
        configMapper.deleteBonusAuth(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_bonus_auth表数据完成。。。。");
        configMapper.deleteBonusConfig(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_bonus_config表数据完成。。。。");
        configMapper.deleteBonusOrder(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_bonus_order表数据完成。。。。");
        configMapper.deleteDaySalaryAuth(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_day_salary_auth表数据完成。。。。");
        configMapper.deleteDaySalaryConfig(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_day_salary_config表数据完成。。。。");
        configMapper.deleteDaySalaryOrder(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_day_salary_order表数据完成。。。。");
        configMapper.deleteDayConsumeActivityConfig(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_day_consume_activity_config表数据完成。。。。");
        configMapper.deleteDayConsumeActivityOrder(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_day_consume_activity_order表数据完成。。。。");
        
        //充值提现相关表
        configMapper.deleteRechargeOrder(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_recharge_order表数据完成。。。。");
        configMapper.deleteWithdrawOrder(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_withdraw_order表数据完成。。。。");
        configMapper.deleteWithdrawAutoLog(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_withdraw_auto_log表数据完成。。。。");
        configMapper.deleteWithdrawAutoConfig(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_withdraw_auto_config表数据完成。。。。");
        /*logger.info("删除【"+bizSystem+"】业务系统kr_recharge_draw_order表数据完成。。。。");*/
        configMapper.deleteRechargeConfig(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_recharge_config表数据完成。。。。");
        configMapper.deletePayBank(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_pay_bank表数据完成。。。。");
        configMapper.deleteChargePay(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_charge_pay表数据完成。。。。");
        configMapper.deleteUserBettingDemand(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_user_betting_demand表数据完成。。。。");
        
        // 聊天室相关表
        configMapper.deleteChatroomConfig(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_chatroom_config表数据完成。。。。");
        configMapper.deleteChatRedbag(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_chat_redbag表数据完成。。。。");
        configMapper.deleteChatRedbagInfo(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_chat_redbag_info表数据完成。。。。");
        configMapper.deleteChatBan(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_chat_ban表数据完成。。。。");
        configMapper.deleteChatBlackList(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_chat_blacklist表数据完成。。。。");
        configMapper.deleteChatLevelAuth(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_chat_level_auth表数据完成。。。。");
        configMapper.deleteChatRoleUser(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_chat_role_user表数据完成。。。。");
        configMapper.deleteChatRoom(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_chat_room表数据完成。。。。");
        configMapper.deleteChatUserSendRebagAuth(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_chat_user_sendredbag_auth表数据完成。。。。");
        configMapper.deleteChatLog(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_chatlog表数据完成。。。。");
        
        configMapper.deleteFeeBack(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_feedback表数据完成。。。。");
        
        // 根据业务系统删除密码错误日志表数据.
        configMapper.deletePasswordErrorLog(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_password_error_log表数据完成。。。。");
        configMapper.deleteAdminPasswordErrorLog(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_admin_password_error_log表数据完成。。。。");
        configMapper.deleteLotteryCodeXyTrend(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_lottery_code_xy_trend表数据完成。。。。");
        
        // 根据业务系统删除彩种开关数据.
        configMapper.deleteLotterySwitchByBizSystem(bizSystem);
        logger.info("删除【"+bizSystem+"】业务系统kr_lottery_switch表数据完成。。。。");
        
        
    }
}
