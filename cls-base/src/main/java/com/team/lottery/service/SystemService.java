package com.team.lottery.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.extvo.DeleteAgoDataInfo;
import com.team.lottery.mapper.config.ConfigMapper;
import com.team.lottery.util.DateUtil;
import com.team.lottery.vo.Config;

@Service("systemService")
public class SystemService {

	@Autowired
	private ConfigMapper configMapper;
	
    public int deleteByPrimaryKey(Long id) {
		return configMapper.deleteByPrimaryKey(id);
	}

    public int insert(Config record) {
    	return configMapper.insert(record);
	}

    public int insertSelective(Config record) {
    	return configMapper.insertSelective(record);
	}

    public Config selectByPrimaryKey(Long id) {
    	return configMapper.selectByPrimaryKey(id);
	}

    public int updateByPrimaryKeySelective(Config record) {
    	return configMapper.updateByPrimaryKeySelective(record);
	}

    public int updateByPrimaryKeyWithBLOBs(Config record) {
    	return configMapper.updateByPrimaryKeyWithBLOBs(record);
	}
	
	@Transactional(readOnly=false,rollbackFor=Exception.class)
    public void deleteAgoDataFromThisTime(Date date) throws Exception{
		Date endDate = DateUtil.getNowStartTimeByEnd(date);
    	
        configMapper.deleteAgoDataFromThisTimeForOrder(endDate);
        configMapper.deleteAgoDataFromThisTimeForMoneyDetail(endDate);
        configMapper.deleteAgoDataFromThisTimeForNotes(endDate);
        //configMapper.deleteAgoDataFromThisTimeForDrawOrder(endDate);
        configMapper.deleteAgoDataFromThisTimeForLogin(endDate);
        configMapper.deleteAgoDataFromThisTimeForLotteryCode(endDate);
        configMapper.deleteAgoDataFromThisTimeForUseMoneyLog(endDate);
        configMapper.deleteAgoDataFromThisTimeForLotteryCache(endDate);
    }
	
	/**
	 * 删除几天前的数据 并控制删除那些表
	 * @param deleteDataInfo
	 * @param date
	 * @throws Exception
	 */
	@Transactional(readOnly=false,rollbackFor=Exception.class)
    public void deleteAgoDataFromThisTime(DeleteAgoDataInfo deleteDataInfo, Date date) throws Exception{
		Date endDate = DateUtil.getNowStartTimeByEnd(date);
    	
		if(deleteDataInfo.getIsDeleteOrder() != null && deleteDataInfo.getIsDeleteOrder() == 1) {
			configMapper.deleteAgoDataFromThisTimeForOrder(endDate);
			configMapper.deleteAgoDataFromThisTimeForOrderAfterRecord(endDate);
			configMapper.deleteAgoDataFromThisTimeForOrderAfterAmount(endDate);
		}
		if(deleteDataInfo.getIsDeleteMoneyDetail() != null && deleteDataInfo.getIsDeleteMoneyDetail() == 1) {
			configMapper.deleteAgoDataFromThisTimeForMoneyDetail(endDate);
		}
		if(deleteDataInfo.getIsDeleteNotes() != null && deleteDataInfo.getIsDeleteNotes() == 1) {
			configMapper.deleteAgoDataFromThisTimeForNotes(endDate);
		}
		if(deleteDataInfo.getIsDeleteDrawOrder() != null && deleteDataInfo.getIsDeleteDrawOrder() == 1) {
			//configMapper.deleteAgoDataFromThisTimeForDrawOrder(endDate);
			configMapper.deleteAgoDataFromThisTimeForRechargeOrder(endDate);
			configMapper.deleteAgoDataFromThisTimeForWithdrawOrder(endDate);
		}
		if(deleteDataInfo.getIsDeleteLogin() != null && deleteDataInfo.getIsDeleteLogin() == 1) {
			configMapper.deleteAgoDataFromThisTimeForLogin(endDate);
		}
		if(deleteDataInfo.getIsDeleteLotteryCode() != null && deleteDataInfo.getIsDeleteLotteryCode() == 1) {
			configMapper.deleteAgoDataFromThisTimeForLotteryCode(endDate);
		}
		if(deleteDataInfo.getIsDeleteUseMoneyLog() != null && deleteDataInfo.getIsDeleteUseMoneyLog() == 1) {
			configMapper.deleteAgoDataFromThisTimeForUseMoneyLog(endDate);
		}
		if(deleteDataInfo.getIsDeleteLotteryCache() != null && deleteDataInfo.getIsDeleteLotteryCache() == 1) {
			configMapper.deleteAgoDataFromThisTimeForLotteryCache(endDate);
		}
		if(deleteDataInfo.getIsDeleteQuotaDetail() != null && deleteDataInfo.getIsDeleteQuotaDetail() == 1) {
			configMapper.deleteAgoDataFromThisTimeForQuotaDetail(endDate);
		}
		
    }
    
}
