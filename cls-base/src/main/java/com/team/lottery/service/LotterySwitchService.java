package com.team.lottery.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.team.lottery.mapper.lotteryswitch.LotterySwitchMapper;
import com.team.lottery.vo.LotterySwitch;

/**
 * 彩种开关相关Service类.
 * 
 * @author jamine
 *
 */
@Service("lotterySwitchService")
public class LotterySwitchService {
	@Autowired
	private LotterySwitchMapper lotterySwitchMapper;

	public Integer deleteByPrimaryKey(Long id) {
		return lotterySwitchMapper.deleteByPrimaryKey(id);
	}

	public Integer insert(LotterySwitch record) {
		return lotterySwitchMapper.insert(record);
	}

	public Integer insertSelective(LotterySwitch record) {
		return lotterySwitchMapper.insertSelective(record);
	}

	public LotterySwitch selectByPrimaryKey(Long id) {
		return lotterySwitchMapper.selectByPrimaryKey(id);
	}

	public Integer updateByPrimaryKeySelective(LotterySwitch record) {
		return lotterySwitchMapper.updateByPrimaryKeySelective(record);
	}

	public Integer updateByPrimaryKey(LotterySwitch record) {
		return lotterySwitchMapper.updateByPrimaryKey(record);
	}

	/**
	 * 根据业务系统获取对应的彩种开关.
	 * 
	 * @param bizSystem 系统.
	 * @return
	 */
	public List<LotterySwitch> getAllLotterySwitchsByBizSystem(String bizSystem) {
		return lotterySwitchMapper.getAllLotterySwitchsByBizSystem(bizSystem);
	}

	/**
	 * 修改修改或者批量新增彩种开关.增加事务操作.
	 * 
	 * @param allLotterySwitchsByBizSystem 对应的彩种开关.
	 * @param newLotterySwitchs
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void updateLotterySwitchsByBizSystem(List<LotterySwitch> updateLotterySwitchs, List<LotterySwitch> newLotterySwitchs) {
		// 通过For循环批量插入彩种开关,或者新增数据.
		if (updateLotterySwitchs.size() > 0) {
			for (LotterySwitch updateLotterySwitch : updateLotterySwitchs) {
				lotterySwitchMapper.updateByPrimaryKey(updateLotterySwitch);
			}
		}
		if (newLotterySwitchs.size() > 0) {
			for (LotterySwitch newLotterySwitch : newLotterySwitchs) {
				lotterySwitchMapper.insertSelective(newLotterySwitch);
			}
		}
		

	}
	
	/**
	 * 获取所有系统的彩种开关.
	 * @return
	 */
	public List<LotterySwitch> getAllLotterySwitchs() {
		return lotterySwitchMapper.getAllLotterySwitchs();
	}
	
	/**
	 * 新增业务系统时候,添加业务系统彩种开关.
	 * 
	 * @param bizSystem 业务系统.
	 * @param username 操作人.
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void insertBizSystemLotterySwitchBySql(String bizSystem, String adminName) {
		lotterySwitchMapper.insertBizSystemLotterySwitchBySql(bizSystem, adminName);
	}
	
}
