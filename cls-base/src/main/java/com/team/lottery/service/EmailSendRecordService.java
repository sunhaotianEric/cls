package com.team.lottery.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.emailsendrecord.EmailSendRecordMapper;
import com.team.lottery.vo.EmailSendRecord;

@Service("emailSendRecordService")
public class EmailSendRecordService {

	@Autowired
	private EmailSendRecordMapper emailSendRecordMapper;
	
	public int insertEmailSendRecord(EmailSendRecord record){
		return emailSendRecordMapper.insertSelective(record);
	}
	
	public List<EmailSendRecord> queryAllEmailSendRecord(EmailSendRecord query) {
		return emailSendRecordMapper.queryAllEmailSendRecord(query);
	}

	/**
	 * 分页查询
	 * @param query
	 * @param page
	 * @return
	 */
	public Page getAllEmailSendRecord(EmailSendRecord query,Page page) {
		page.setTotalRecNum(emailSendRecordMapper.getEmailSendRecordCount(query));
		page.setPageContent(emailSendRecordMapper.getAllEmailSendRecord(query, page.getStartIndex(),page.getPageSize()));
		return page ;
	}
	
	public EmailSendRecord selectByPrimaryKey(Long id){
		return emailSendRecordMapper.selectByPrimaryKey(id);
	}
	
	public int deleteByPrimaryKey(Long id){
		return emailSendRecordMapper.deleteByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(EmailSendRecord record){
		return emailSendRecordMapper.updateByPrimaryKeySelective(record);
	}
	
	
}
