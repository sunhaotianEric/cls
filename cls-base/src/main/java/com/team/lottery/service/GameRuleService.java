package com.team.lottery.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.gamerule.GameRuleMapper;
import com.team.lottery.vo.GameRule;

@Service
public class GameRuleService {

	@Autowired
	private GameRuleMapper gameRuleMapper;
	
	public int deleteByPrimaryKey(Integer id){
		return gameRuleMapper.deleteByPrimaryKey(id);
		
	}

/*    public int insert(GameRule record){
		return gameRuleMapper.insert(record);
    	
    }
    */
    public int insertSelective(GameRule record){
    	return gameRuleMapper.insertSelective(record);
    }

    public GameRule selectByPrimaryKey(Integer id){
		return gameRuleMapper.selectByPrimaryKey(id);
    	
    }

    public int updateByPrimaryKeySelective(GameRule record){
		return gameRuleMapper.updateByPrimaryKeySelective(record);
    	
    }

    public int updateByPrimaryKeyWithBLOBs(GameRule record){
		return gameRuleMapper.updateByPrimaryKeyWithBLOBs(record);
    	
    }

    /**
     * 分页查询
     * @param query
     * @param page
     * @return
     */
    public Page getAllGameRule(GameRule query,Page page) {
		page.setTotalRecNum(gameRuleMapper.getGameRuleCount(query));
		page.setPageContent(gameRuleMapper.getAllGameRule(query, page.getStartIndex(),page.getPageSize()));
		return page ;
	}
    
    /**
     * 根据彩种类型获取玩法规则描述
     * @param lotteryType
     * @return
     */
    public GameRule getRuleByType(String lotteryType){
		return gameRuleMapper.getRuleByType(lotteryType);
    	
    }
    
      
}
