package com.team.lottery.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.DayOnlineTypeCountVo;
import com.team.lottery.extvo.LoginLogQuery;
import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.loginlog.LoginLogMapper;
import com.team.lottery.vo.LoginLog;

@Service("loginLogService")
public class LoginLogService {

	// @Autowired
	// private LoginMapper loginMapper;

	@Autowired
	private LoginLogMapper loginLogMapper;

	public int deleteByPrimaryKey(Integer id) {
		return loginLogMapper.deleteByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(LoginLog record) {
		return loginLogMapper.updateByPrimaryKeySelective(record);
	}

	public int insert(LoginLog record) {
		return loginLogMapper.insert(record);
	}

	public int insertSelective(LoginLog record) {
		record.setLoginTime(new Date());
		return loginLogMapper.insertSelective(record);
	}

	public LoginLog selectByPrimaryKey(Integer id) {
		return loginLogMapper.selectByPrimaryKey(id);
	}

	/**
	 * 查找所有的登陆日志数据
	 * 
	 * @return
	 */
	public Page getAllLoginLogsByQueryPage(LoginLogQuery query, Page page) {
		page.setTotalRecNum(loginLogMapper.getAllLoginLogsByQueryPageCount(query));
		page.setPageContent(loginLogMapper.getAllLoginLogsByQueryPage(query, page.getStartIndex(), page.getPageSize()));
		return page;
	}

	/**
	 * 删除时间段内的登陆日志数据
	 * 
	 * @return
	 */
	public void delLoginLogsByDay(String bizSystem, Date from, Date to) {
		loginLogMapper.delLoginLogsByDay(bizSystem, from, to);
	}

	/**
	 * 查询上次登录的日志(第二次)
	 * 
	 * @param userId
	 * @return
	 */
	public List<LoginLog> getLastIPAddress(String bizSytem, String userName) {
		return loginLogMapper.getLastIPAddress(bizSytem, userName);
	}

	/**
	 * 查询每日登录人数(按登录类型统计)
	 * 
	 * @param query
	 * @return
	 */
	public List<DayOnlineTypeCountVo> getDayLoginTypeCount(LoginLogQuery query) {
		return loginLogMapper.getDayLoginTypeCount(query);
	}

	/**
	 * 查询每日登录人数
	 * 
	 * @param query
	 * @return
	 */
	public Integer getDayLoginCount(LoginLogQuery query) {
		return loginLogMapper.getDayLoginCount(query);
	}

	/**
	 * 查询登录日志
	 * 
	 * @param query
	 * @return
	 */
	public List<LoginLog> getLoginLogs(LoginLogQuery query) {
		return loginLogMapper.getAllLoginLogs(query);
	}

	/**
	 * 查询异常登录日志
	 * 
	 * @param query
	 * @return
	 */
	public List<LoginLog> getLoginLogsByUnusual(LoginLogQuery query) {
		return loginLogMapper.getLoginLogsByUnusual(query);
	}
}
