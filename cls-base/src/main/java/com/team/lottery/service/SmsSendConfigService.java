package com.team.lottery.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.extvo.Page;
import com.team.lottery.mapper.smssendconfig.SmsSendConfigMapper;
import com.team.lottery.vo.SmsSendConfig;

@Service
public class SmsSendConfigService {

	@Autowired
	private SmsSendConfigMapper smsSendConfigMapper;
	
	public int deleteByPrimaryKey(Integer id) {
		return smsSendConfigMapper.deleteByPrimaryKey(id);
	}

    public int insertSelective(SmsSendConfig record) {
    	return smsSendConfigMapper.insertSelective(record);
    }

    public SmsSendConfig selectByPrimaryKey(Integer id) {
    	return smsSendConfigMapper.selectByPrimaryKey(id);
    }

    public int updateByPrimaryKeySelective(SmsSendConfig record) {
    	return smsSendConfigMapper.updateByPrimaryKeySelective(record);
    }
    
    /**
     * 查询所有可用的发送配置
     * @return
     */
    public List<SmsSendConfig> queryAllSmsSendConfig() {
    	return smsSendConfigMapper.queryAllSmsSendConfig();
    }
    
    /**
     * 分页查询
     * @param query
     * @param page
     * @return
     */
    public Page getAllSmsSendConfig(SmsSendConfig query,Page page) {
		page.setTotalRecNum(smsSendConfigMapper.getSmsSendConfig(query));
		page.setPageContent(smsSendConfigMapper.getAllSmsSendConfig(query, page.getStartIndex(),page.getPageSize()));
		return page ;
	}

}
