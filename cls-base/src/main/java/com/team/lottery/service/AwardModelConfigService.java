package com.team.lottery.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team.lottery.mapper.awardmodelconfig.AwardModelConfigMapper;
import com.team.lottery.vo.AwardModelConfig;

@Service
public class AwardModelConfigService {

	@Autowired
	private AwardModelConfigMapper awardModelConfigMapper;
	
	public int deleteByPrimaryKey(Long id) {
		return awardModelConfigMapper.deleteByPrimaryKey(id);
	}

    public int insertSelective(AwardModelConfig record) {
    	return awardModelConfigMapper.insertSelective(record);
    }

    public AwardModelConfig selectByPrimaryKey(Long id) {
    	return awardModelConfigMapper.selectByPrimaryKey(id);
    }

    public int updateByPrimaryKeySelective(AwardModelConfig record) {
    	return awardModelConfigMapper.updateByPrimaryKeySelective(record);
    }
    
    /**
     * 查找所有的奖金模式配置数据
     * @return
     */
    public List<AwardModelConfig> getAllAwardModelConfigs() {
    	return awardModelConfigMapper.getAllAwardModelConfigs();
    }
    
    /**
     * 查找所有的奖金模式配置数据
     * @return
     */
    public List<AwardModelConfig> getAwardModelConfigByQuery(AwardModelConfig query) {
    	return awardModelConfigMapper.getAwardModelConfigByQuery(query);
    }

}
