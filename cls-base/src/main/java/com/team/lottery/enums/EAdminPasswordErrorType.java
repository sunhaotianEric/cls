package com.team.lottery.enums;

public enum EAdminPasswordErrorType {

	// 后台管理员登陆密码错误类型.
	PASSWORD_ERROR("PASSWORD_ERROR", "管理员登陆时密码错误"),
	// 后台管理员操作时密码错误(非登陆时).
	OPERATE_PASSWORD_ERROR("OPERATE_PASSWORD_ERROR", "管理员操作管理员时密码错误");

	// 大写英文描述.
	private String code;
	// 中文描述.
	private String description;

	private EAdminPasswordErrorType(String code, String description) {
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
