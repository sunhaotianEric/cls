package com.team.lottery.enums;

/**
 * 计划列表使用
 * @author Administrator
 * ----时时彩类----
 */
public enum ESSCKindCodePlan {
	WXDWD("WXDWD","五星定位胆","ssc_codeplan_wxdwdServiceImpl"),
	HSZS("HSZS","后三组三","ssc_codeplan_hszsServiceImpl"),
	HSZL("HSZL","后三组六","ssc_codeplan_hszlServiceImpl"),
	ZHDX("ZHDX","总和大小","ssc_codeplan_zhdxServiceImpl"),
	ZHDS("ZHDS","总和单双","ssc_codeplan_zhdsServiceImpl"),
	
	WWDW("WWDW","万位定位","ssc_codeplan_wwdwServiceImpl"),
	WWDX("WWDX","万位大小","ssc_codeplan_wwdxServiceImpl"),
	WWDS("GJDS","万位单双","ssc_codeplan_wwdsServiceImpl"),
	
	QWDW("QWDW","千位定位","ssc_codeplan_qwdwServiceImpl"),
	QWDX("QWDX","千位大小","ssc_codeplan_qwdxServiceImpl"),
	QWDS("QWDS","千位单双","ssc_codeplan_qwdsServiceImpl"),
	
	BWDW("BWDW","百位定位","ssc_codeplan_bwdwServiceImpl"),
	BWDX("BWDX","百位大小","ssc_codeplan_bwdxServiceImpl"),
	BWDS("BWDS","百位单双","ssc_codeplan_bwdsServiceImpl"),
	
	SWDW("SWDW","十位定位","ssc_codeplan_swdwServiceImpl"),
	SWDX("SWDX","十位大小","ssc_codeplan_swdxServiceImpl"),
	SWDS("SWDS","十位单双","ssc_codeplan_swdsServiceImpl"),
	
	GWDW("GWDW","个位定位","ssc_codeplan_gwdwServiceImpl"),
	GWDX("GWDX","个位大小","ssc_codeplan_gwdxServiceImpl"),
	GWDS("GWDS","个位单双","ssc_codeplan_gwdsServiceImpl");
	
	private String code;
	private String description;
	private String serviceClassName; //对应的实现逻辑
	
	ESSCKindCodePlan(String code,String description,String serviceClassName){
		this.code = code;
		this.description = description;
		this.serviceClassName = serviceClassName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getServiceClassName() {
		return serviceClassName;
	}

	public void setServiceClassName(String serviceClassName) {
		this.serviceClassName = serviceClassName;
	}
}
