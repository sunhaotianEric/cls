package com.team.lottery.enums;

/**
 * 积分明细类型
 * @author chenhsh
 *
 */
public enum EPointDetailType {

//	LOTTERY("LOTTERY","投注"),
//	WIN("WIN","中奖"),
//	LOTTERY_STOP_AFTER_NUMBER("LOTTERY_STOP_AFTER_NUMBER","撤单"),
	REGISTER("REGISTER","注册赠送",1),
	RECHARGE("RECHARGE","充值赠送",1),
	SIGN_IN("SIGN_IN","签到赠送",1),
	
	SYSTEM_ADD_POINT("SYSTEM_ADD_POINT","系统奖励",1),
	SYSTEM_REDUCE_POINT("SYSTEM_REDUCE_POINT","系统扣除",1),
	ADD_EXTEND("ADD_EXTEND","增加推广积分",0),
	REDUCE_EXTEND("REDUCE_EXTEND","扣除推广积分",0);
	
	private String code;
	private String description;
	private Integer isShow;

	private EPointDetailType(String code,String description,Integer isShow){
		this.code = code;
		this.description = description;
		this.isShow = isShow;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getIsShow() {
		return isShow;
	}

	public void setIsShow(Integer isShow) {
		this.isShow = isShow;
	}
}
