package com.team.lottery.enums;

/**
 * 配额帐变类型
 * @author gs
 *
 */
public enum EQuotaType {

	ADDUSER_DECREASE("ADDUSER_DECREASE","开户扣减"),
	SYSTEM_DECREASE("SYSTEM_DECREASE","系统扣减"),
	SYSTEM_INCREASE("SYSTEM_INCREASE","系统返还"),
	ADDREBATE_DECREASE("ADDREBATE_DECREASE", "升点扣减"),
	ADDREBATE_INCREASE("ADDREBATE_INCREASE", "升点增加");
	
	private String code;
	private String description;
	
	private EQuotaType(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
