package com.team.lottery.enums;

/**
 * 前端缓存类型
 * @author gs
 *
 */
public enum EFrontCacheType {

	SYS_CONFIG_DATA("SYS_CONFIG_DATA", "系统参数设置"),
	LOTTERY_CONFIG_DATA("LOTTERY_CONFIG_DATA", "彩种开关设置"),
	BIZ_SYSTEM_DOMAIN_CACHE("BIZ_SYSTEM_DOMAIN_CACHE", "域名管理缓存"),
	BIZ_SYS_CONFIG_DATA("BIZ_SYS_CONFIG_DATA", "业务系统参数设置"),
	HOME_PICTUS_CACHE_INFO("HOME_PICTUS_CACHE_INFO","首页图片缓存"),
	REFRESH_LOTTERYISSUE_CACHE("REFRESH_LOTTERYISSUE_CACHE","刷新彩种开奖时间缓存"),
	MGR_BIZ_SYSTEM_DOMAIN_CACHE("MGR_BIZ_SYSTEM_DOMAIN_CACHE","刷新后台域名IP授权缓存");
	
	
	private String code;
	private String description;
	
	private EFrontCacheType(String code, String description) {
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
