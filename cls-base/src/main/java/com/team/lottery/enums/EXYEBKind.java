package com.team.lottery.enums;


/**
 * 六合彩
 * @author Administrator
 *
 */
public enum EXYEBKind {
	TM("TM","特码","xyeb_tmServiceImpl"),  //特码
	TMSB("TMSB","特码包三","xyeb_tmsbServiceImpl"),  //特码三包
	
	HH("HH","混合","xyeb_hhServiceImpl"),  //混合
	BS("BS","波色","xyeb_bsServiceImpl"),  //波色
	
	BZ("BZ","豹子","xyeb_bzServiceImpl");  //豹子
	
	private String code;
	private String description;
	private String serviceClassName; //对应的实现逻辑
	
	private EXYEBKind(String code,String description,String serviceClassName){
		this.code = code;
		this.description = description;
		this.serviceClassName = serviceClassName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getServiceClassName() {
		return serviceClassName;
	}

	public void setServiceClassName(String serviceClassName) {
		this.serviceClassName = serviceClassName;
	}
}
