package com.team.lottery.enums;

/**
 * 操作记录模块枚举
 * @author gs
 *
 */
public enum ERecordLogModel {
	
	ADMIN("ADMIN", "管理员管理"),
	DOMAIN("DOMAIN", "域名管理"),
	ADMIN_DOMAIN("ADMIN_DOMAIN", "后台域名授权管理"),
	
	USER("USER", "会员管理"),
	USER_XUFEI("USER_XUFEI", "续费"),
	USER_BANK("USER_BANK", "用户银行卡"),
	
	CHARGE_PAY("CHARGE_PAY", "快捷支付"),
	PAYBANK("PAYBANK", "收款银行设置"),
	RECHARGECONFIG_MANAGE("RECHARGECONFIG_MANAGE", "充值配置"),
	
	
	HAND_UPDATE_DEAL("HAND_UPDATE_DEAL", "彩票开奖处理"),
	LOTTERY_ISSUE_MANAGE("LOTTERY_ISSUE_MANAGE", "开奖时间管理"),
	
	SYSTEM_CONFIG("SYSTEM_CONFIG", "系统参数设置"),
	BIZ_SYSTEM("BIZ_SYSTEM", "业务系统管理"),
	PLATFORM_CHARGE("PLATFORM_CHARGE", "系统收费管理"),
	BIZ_SYSTEM_CONFIG("BIZ_SYSTEM_CONFIG", "业务系统参数设置"),
	
	BONUS_CONFIG("BONUS_CONFIG", "契约分红"),
	QUOTA_MANAGE("QUOTA_MANAGE", "配额管理"),
	AGENCY_MODEL_ADD("AGENCY_MODEL_ADD", "代理升点"),
	DAY_SALARY_CONFIG("DAY_SALARY_CONFIG", "日工资"),
	
	BAKDATA_MANAGE("BAKDATA_MANAGE", "清除数据"),
	DOMAININVITATIONCODE("DOMAININVITATIONCODE", "邀请码域名设置"),
	HOMEPIC_MANAGE("HOMEPIC_MANAGE","首页图片设置"),
	AUTOCONFIG_MANAGE("AUTOCONFIG_MANAGE","自动出款设置");
	
	
	
	private String name;
	private String description;
	
	private ERecordLogModel(String name, String description) {
		this.name = name;
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getCode(){
		return name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	

	
}
