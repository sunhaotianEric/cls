package com.team.lottery.enums;

/**
 * 低频彩玩法
 * @author chenhsh
 *
 */
public enum EDPCKind {

	SXFS("SXFS","三星复式","dpc_sxfsServiceImpl"),
	SXDS("SXDS","三星单式","dpc_sxdsServiceImpl"),
	SXZXHZ("SXZXHZ","三星直选和值","dpc_sxzxhzServiceImpl"),
	SXZXHZ_G("SXZXHZ_G","三星组选和值","dpc_sxzxhz_GServiceImpl"),
	SXZS("SXZS","三星组三","dpc_sxzsServiceImpl"),
	SXZL("SXZL","三星组六","dpc_sxzlServiceImpl"),
	SXYMBDW("SXYMBDW","三星一码不定位","dpc_sxymbdwServiceImpl"),
	SXEMBDW("SXEMBDW","三星二码不定位","dpc_sxembdwServiceImpl"),
	
	QEZXFS("QEZXFS","前二直选复式","dpc_qezxfsServiceImpl"),
	QEZXDS("QEZXDS","前二直选单式","dpc_qezxdsServiceImpl"),
	QEZXHZ("QEZXHZ","前二直选和值","dpc_qezxhzServiceImpl"),
	QEZXFS_G("QEZXFS_G","前二组选复式","dpc_qezxfs_GServiceImpl"),
	QEZXDS_G("QEZXDS_G","前二组选单式","dpc_qezxds_GServiceImpl"),
	QEZXHZ_G("QEZXHZ_G","前二组选和值","dpc_qezxhz_GServiceImpl"),
	
	HEZXFS("HEZXFS","后二直选复式","dpc_hezxfsServiceImpl"),
	HEZXDS("HEZXDS","后二直选单式","dpc_hezxdsServiceImpl"),
	HEZXHZ("HEZXHZ","后二直选和值","dpc_hezxhzServiceImpl"),
	HEZXFS_G("HEZXFS_G","后二组选复式","dpc_hezxfs_GServiceImpl"),
	HEZXDS_G("HEZXDS_G","后二组选单式","dpc_hezxds_GServiceImpl"),
	HEZXHZ_G("HEZXHZ_G","后二组选和值","dpc_hezxhz_GServiceImpl"),
	
	SXDWD("SXDWD","三星定位胆","dpc_sxdwdServiceImpl"), 
	
	QEDXDS("QEDXDS","前二大小单双复式","dpc_qedxdsServiceImpl"), 
	HEDXDS("HEDXDS","后二大小单双复式","dpc_hedxdsServiceImpl"), 
	
	RXE("RXE","任选二","dpc_rxeServiceImpl"),
	RXEZXDS("RXEZXDS","任选二直选单式","dpc_rxezxdsServiceImpl");  //任选二直选单式

	private String code;
	private String description;
	private String serviceClassName; //对应的实现逻辑
	
	private EDPCKind(String code,String description,String serviceClassName){
		this.code = code;
		this.description = description;
		this.serviceClassName = serviceClassName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getServiceClassName() {
		return serviceClassName;
	}

	public void setServiceClassName(String serviceClassName) {
		this.serviceClassName = serviceClassName;
	}
	public static void main(String[] args) {
		System.out.println(EDPCKind.values().length);
	}
}
