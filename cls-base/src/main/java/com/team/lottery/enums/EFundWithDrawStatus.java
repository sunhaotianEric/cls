package com.team.lottery.enums;

/**
 * 取现状态 
 * @author chenhsh
 *
 */
public enum EFundWithDrawStatus {

	DEALING("DEALING","处理中"),
	LOCKED("LOCKED","锁定中"),
	WITHDRAW_SUCCESS("WITHDRAW_SUCCESS","提现成功"),
	WITHDRAW_FAIL("WITHDRAW_FAIL","提现失败");
	
	private String code;
	private String description;
	
	private EFundWithDrawStatus(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
