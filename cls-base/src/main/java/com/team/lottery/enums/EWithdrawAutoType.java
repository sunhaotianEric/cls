package com.team.lottery.enums;

/**
 * 自动出款类型
 * 枚举
 * @author Owner
 *
 */
public enum EWithdrawAutoType {

	AUTO("AUTO","自动"),
	MANUAL("MANUAL","手动");
	
	private String code;
	private String description;
	
	private EWithdrawAutoType(String code, String description) {
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
