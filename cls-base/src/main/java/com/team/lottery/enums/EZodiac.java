package com.team.lottery.enums;

/**
 * 十二生肖
 * @author
 *
 */
public enum EZodiac {

	/**
	 * poultry ==家禽
	 * beast  == 野兽
	 */
	SHU("SHU","鼠","beast"),
	NIU("NIU","牛","poultry"),
	HU("HU","虎","beast"),
	TU("TU","兔","beast"),
	LONG("LONG","龙","beast"),
	SHE("SHE","蛇","beast"),
	MA("MA","马","poultry"),
	YANG("YANG","羊","poultry"),
	HOU("HOU","猴","beast"),
	JI("JI","鸡","poultry"),
	GOU("GOU","狗","poultry"),
	ZHU("ZHU","猪","poultry");
	
	private String code;
	private String description;
	private String animalType;//家禽，野兽
	private EZodiac(String code,String description,String animalType){
		this.code = code;
		this.description = description;
		this.animalType = animalType;
	}

	public String getAnimalType() {
		return animalType;
	}

	public void setAnimalType(String animalType) {
		this.animalType = animalType;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
