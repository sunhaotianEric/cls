package com.team.lottery.enums;


/**
 * 彩种枚举
 * @author chenhsh
 *
 */
public enum ELotteryKind {

	
	SFSSC("SFSSC","三分时时彩","2019-12-01",5,2.00,"sSCServiceImpl",1,false),
	WFSSC("WFSSC","五分时时彩","2018-12-01",5,2.00,"sSCServiceImpl",1,false),
	SHFSSC("SHFSSC","十分时时彩","2018-02-01",5,2.00,"sSCServiceImpl",1,false),
	LCQSSC("LCQSSC","老重庆时时彩","2018-02-02",5,2.00,"sSCServiceImpl",1,false),
	
	CQSSC("CQSSC","重庆时时彩","2009-12-01",5,2.00,"sSCServiceImpl",1,true),
	JXSSC("JXSSC","江西时时彩","2015-01-01",5,2.00,"sSCServiceImpl",0,true),
	TJSSC("TJSSC","天津时时彩","2015-01-13",5,2.00,"sSCServiceImpl",1,true),
	XJSSC("XJSSC","新疆时时彩","2015-01-03",5,2.00,"sSCServiceImpl",1,true),
	HLJSSC("HLJSSC","黑龙江时时彩","2015-01-02",5,2.00,"sSCServiceImpl",0,true),
	//新增时时彩彩种
	JSSSC("JSSSC","江苏时时彩","2018-02-02",5,2.00,"sSCServiceImpl",1,false),
	BJSSC("BJSSC","北京时时彩","2018-02-02",5,2.00,"sSCServiceImpl",1,false),
	GDSSC("GDSSC","广东时时彩","2018-02-02",5,2.00,"sSCServiceImpl",1,false),
	SCSSC("SCSSC","四川时时彩","2018-02-02",5,2.00,"sSCServiceImpl",1,false),
	SHSSC("SHSSC","上海时时彩","2018-02-02",5,2.00,"sSCServiceImpl",1,false),
	SDSSC("SDSSC","山东时时彩","2018-02-02",5,2.00,"sSCServiceImpl",1,false),


	JLFFC("JLFFC","幸运分分彩","2015-01-04",5,2.00,"sSCServiceImpl",1,false),
	HGFFC("HGFFC","韩国1.5分彩","2015-01-17",5,2.00,"sSCServiceImpl",0,true),
	XJPLFC("XJPLFC","新加坡2分彩","2015-01-22",5,2.00,"sSCServiceImpl",0,true),
	TWWFC("TWWFC","台湾五分彩","2015-01-23",5,2.00,"sSCServiceImpl",1,true),
	
	// 新增彩种(三分11选5,和五分11选5).
	SFSYXW("SFSYXW","三分11选5","2019-01-01",5,2.00,"sYXWServiceImpl",1,false),
	WFSYXW("WFSYXW","五分11选5","2018-01-01",5,2.00,"sYXWServiceImpl",1,false),
	
	GDSYXW("GDSYXW","广东11选5","2015-01-05",5,2.00,"sYXWServiceImpl",1,true),
	SDSYXW("SDSYXW","山东11选5","2015-01-06",5,2.00,"sYXWServiceImpl",1,true),
	JXSYXW("JXSYXW","江西11选5","2015-01-07",5,2.00,"sYXWServiceImpl",1,true),
	CQSYXW("CQSYXW","重庆11选5","2015-01-08",5,2.00,"sYXWServiceImpl",0,true),
	FJSYXW("FJSYXW","福建11选5","2015-01-09",5,2.00,"sYXWServiceImpl",1,true),
	
	SFKS("SFKS","三分快3","2019-01-01",3,1.00,"kSServiceImpl",1,false),
	WFKS("WFKS","五分快3","2018-01-01",3,1.00,"kSServiceImpl",1,false),
	
	JSKS("JSKS","江苏快3","2015-01-14",3,1.00,"kSServiceImpl",1,true),
	FJKS("FJKS","福建快3","2015-01-15",3,1.00,"kSServiceImpl",0,true),
	AHKS("AHKS","安徽快3","2015-01-18",3,1.00,"kSServiceImpl",1,true),
	HBKS("HBKS","湖北快3","2015-01-19",3,1.00,"kSServiceImpl",1,true),
	JLKS("JLKS","吉林快3","2015-01-20",3,1.00,"kSServiceImpl",1,true),
	BJKS("BJKS","北京快3","2015-01-21",3,1.00,"kSServiceImpl",1,true),
	GXKS("GXKS","广西快3","2015-01-26",3,1.00,"kSServiceImpl",1,true),
	SHKS("SHKS","上海快3","2015-01-27",3,1.00,"kSServiceImpl",1,true),
	GSKS("GSKS","甘肃快3","2015-01-28",3,1.00,"kSServiceImpl",1,true),
	JYKS("JYKS","幸运快3","2015-01-24",3,1.00,"kSServiceImpl",1,false),
	
	GDKLSF("GDKLSF","广东快乐十分","2015-01-10",0,0.00,"",0,true),
	
	//低频彩
	SHSSLDPC("SHSSLDPC","上海时时乐","2015-01-11",3,2.00,"dpcServiceImpl",0,true),
	FCSDDPC("FCSDDPC","福彩3D","2015-01-12",3,2.00,"dpcServiceImpl",1,true),
	
	//新增pk10彩种
	XYFT("XYFT", "幸运飞艇", "2015-02-16", 10, 2.00, "pK10ServiceImpl", 1,true),
	SFPK10("SFPK10", "三分PK10", "2015-03-16", 10, 2.00, "pK10ServiceImpl", 1,false),
	WFPK10("WFPK10", "五分PK10", "2015-05-16", 10, 2.00, "pK10ServiceImpl", 1,false),
	SHFPK10("SHFPK10", "十分PK10", "2015-10-16", 10, 2.00, "pK10ServiceImpl", 1,false),
	
	BJPK10("BJPK10", "北京PK10", "2015-01-16", 10, 2.00, "pK10ServiceImpl", 1,true),
	JSPK10("JSPK10", "极速PK10", "2015-01-29", 10, 2.00, "pK10ServiceImpl", 1,false),
	
	XYEB("XYEB","幸运28","2015-01-23",3,1.00,"xYEBServiceImpl",1,true),
	
	LHC("LHC","六合彩","2015-01-25",7,1.00,"lHCServiceImpl",1,true),
	AMLHC("AMLHC","澳门六合彩","2015-01-25",7,1.00,"lHCServiceImpl",1,true),
	XYLHC("XYLHC","幸运六合彩","2015-01-29",7,1.00,"lHCServiceImpl",1,false);
	
	private String code;
	private String description;
	private String dateControlStr; //时间控制的字符串
	private Integer codeCount; //奖号数目
	private Double cathecticUnitPrice; //单注价格
	private String serviceClassName; //对应的实现逻辑
	private Integer isShow; //是否展示
	private boolean isOfficial; //是否是官彩
	
	private ELotteryKind(String code,String description,String dateControlStr,Integer codeCount,Double cathecticUnitPrice
			,String serviceClassName,Integer isShow,boolean isOfficial){
		this.code = code;
		this.description = description;
		this.dateControlStr = dateControlStr;
		this.codeCount = codeCount;
		this.cathecticUnitPrice = cathecticUnitPrice;
		this.serviceClassName = serviceClassName;
		this.isShow = isShow;
		this.isOfficial = isOfficial;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDateControlStr() {
		return dateControlStr;
	}

	public void setDateControlStr(String dateControlStr) {
		this.dateControlStr = dateControlStr;
	}

	public Integer getCodeCount() {
		return codeCount;
	}

	public void setCodeCount(Integer codeCount) {
		this.codeCount = codeCount;
	}

	public Double getCathecticUnitPrice() {
		return cathecticUnitPrice;
	}

	public void setCathecticUnitPrice(Double cathecticUnitPrice) {
		this.cathecticUnitPrice = cathecticUnitPrice;
	}

	public String getServiceClassName() {
		return serviceClassName;
	}

	public void setServiceClassName(String serviceClassName) {
		this.serviceClassName = serviceClassName;
	}

	public Integer getIsShow() {
		return isShow;
	}

	public void setIsShow(Integer isShow) {
		this.isShow = isShow;
	}

	public boolean isOfficial() {
		return isOfficial;
	}

	public void setOfficial(boolean isOfficial) {
		this.isOfficial = isOfficial;
	}
	
}
