package com.team.lottery.enums;

/**
 * 充值类型
 * @author chenhsh
 *
 */
public enum EFundPayType {

	BANK_TRANSFER("BANK_TRANSFER", "网银转账"),
	QUICK_PAY("QUICK_PAY", "快捷支付"),
	ALIPAY("ALIPAY", "支付宝"),
	ALIPAYWAP("ALIPAYWAP", "支付宝WAP"),
	WEIXIN("WEIXIN", "微信"),
	WEIXINWAP("WEIXINWAP", "微信WAP"),
	QQPAY("QQPAY", "QQ钱包"),
	QQPAYWAP("QQPAYWAP", "QQ钱包WAP"),
	UNIONPAY("UNIONPAY", "银联扫码"),
	JDPAY("JDPAY","京东扫码"),
	WEIXINBARCODE("WEIXINBARCODE","微信条形码");
	
	/*弃用
	NET_SILVER("NET_SILVER","网银汇款"),
	BAOFU("BAOFU","快捷支付"),
	SHANFU("SHANFU","微信支付");*/
	
	private String code;
	private String description;
	
	private EFundPayType(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
