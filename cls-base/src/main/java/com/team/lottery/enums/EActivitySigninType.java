package com.team.lottery.enums;

/**
 * 签到活动,状态!
 * 
 * @author Jamine
 *
 */
public enum EActivitySigninType {
	APPLIED("APPLIED", "已签到"),
	DISAPPLIED("DISAPPLIED", "未签到"),
	SIGNIN("SIGNIN", "签到");

	private String code;
	private String description;

	private EActivitySigninType(String code, String description) {
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
