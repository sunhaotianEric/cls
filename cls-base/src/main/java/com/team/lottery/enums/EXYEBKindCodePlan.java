package com.team.lottery.enums;

/**
 * 六合彩计划列表
 * @author Administrator
 *
 */
public enum EXYEBKindCodePlan {
	TM("TM","特码","xyeb_codeplan_tmServiceImpl"),
	HHDX("HHDX","混合大小","xyeb_codeplan_hhdxServiceImpl"),
	HHDS("HHDS","混合单双","xyeb_codeplan_hhdsServiceImpl");
	
	private String code;
	private String description;
	private String serviceClassName; //对应的实现逻辑
	
	private EXYEBKindCodePlan(String code,String description,String serviceClassName){
		this.code = code;
		this.description = description;
		this.serviceClassName = serviceClassName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getServiceClassName() {
		return serviceClassName;
	}

	public void setServiceClassName(String serviceClassName) {
		this.serviceClassName = serviceClassName;
	}

}
