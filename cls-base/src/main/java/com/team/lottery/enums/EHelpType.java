package com.team.lottery.enums;

/**
 * 帮助中心类型
 * @author
 *
 */
public enum EHelpType {

	XSRM("XSRM","新手入门"),
	CZWT("CZWT","充值问题"),
	GCWT("GCWT","购彩问题"),
	TXWT("TXWT","提现问题"),
	ZHAQ("ZHAQ","账户安全"),
	CZZS("CZZS","彩种知识");
	
	private String code;
	private String description;
	
	private EHelpType(String code,String description){
		this.code = code;
		this.description = description;
		
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
