package com.team.lottery.enums;
/**
 * 聊天类型
 * @author Administrator
 *
 */
public enum EChatType {
	
	ALL("ALL", "群聊"),
	PERSONAL("PERSONAL", "私聊"),
	SENDREDBAG("SENDREDBAG", "发红包");
	private String code;
	private String description;
	
	private EChatType(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
