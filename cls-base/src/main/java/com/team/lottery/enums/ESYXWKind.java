package com.team.lottery.enums;

/**
 * 11选5类玩法
 * @author chenhsh
 *
 */
public enum ESYXWKind {

	XYQSYMBDW("XYQSYMBDW","选一前三一码不定位","syxw_xyqsymbdwserviceimpl"),
	XYRXYZYFS("XYRXYZYFS","任选一中一复式","syxw_xyrxyzydsserviceimpl"),
	XYRXYZYDS("XYRXYZYDS","任选一中一单式","syxw_xyrxyzyfsserviceimpl"),

	XEQEZXFS("XEQEZXFS","选二前二直选复式","syxw_xeqezxfsserviceimpl"),
	XEQEZXDS("XEQEZXDS","选二前二直选单式","syxw_xeqezxdsserviceimpl"),
	XEQEZXFS_G("XEQEZXFS_G","选二前二组选复式","syxw_xeqezxfs_gserviceimpl"),
	XEQEZXDS_G("XEQEZXDS_G","选二前二组选单式","syxw_xeqezxds_gserviceimpl"),
	XERXEZEFS("XERXEZEFS","选二任选二中二复式","syxw_xerxezefsserviceimpl"),
	XERXEZEDS("XERXEZEDS","选二任选二中二单式","syxw_xerxezedsserviceimpl"),
	
	XSQSZXFS("XSQSZXFS","选三前三直选复式","syxw_xsqszxfsserviceimpl"),
	XSQSZXDS("XSQSZXDS","选三前三直选单式","syxw_xsqszxdsserviceimpl"),
	XSQSZXFS_G("XSQSZXFS_G","选三前三组选复式","syxw_xsqszxfs_gserviceimpl"),
	XSQSZXDS_G("XSQSZXDS_G","选三前三组选单式","syxw_xsqszxds_gserviceimpl"),
	XSRXSZSFS("XSRXSZSFS","选三任选三中三复式","syxw_xsrxszsfsserviceimpl"),
	XSRXSZSDS("XSRXSZSDS","选三任选三中三单式","syxw_xsrxszsdsserviceimpl"),
	
	XSHRXSZSFS("XSHRXSZSFS","选四任选四中四复式","syxw_xshrxszsfsserviceimpl"),
	XSHRXSZSDS("XSHRXSZSDS","选四任选四中四单式","syxw_xshrxszsdsserviceimpl"),
	
	XWRXWZWFS("XWRXWZWFS","选五任选五中五复式","syxw_xwrxwzwfsserviceimpl"),
	XWRXWZWDS("XWRXWZWDS","选五任选五中五单式","syxw_xwrxwzwdsserviceimpl"),
	
	XLRXLZWFS("XLRXLZWFS","选六任选六中五复式","syxw_xlrxlzwfsserviceimpl"),
	XLRXLZWDS("XLRXLZWDS","选六任选六中五单式","syxw_xlrxlzwdsserviceimpl"),
	
	XQRXQZWFS("XQRXQZWFS","选七任选七中五复式","syxw_xqrxqzwfsServiceImpl"),
	XQRXQZWDS("XQRXQZWDS","选七任选七中五单式","syxw_xqrxqzwdsserviceimpl"),
	
	XBRXBZWFS("XBRXBZWFS","选八任选八中五复式","syxw_xbrxbzwfsserviceimpl"),
	XBRXBZWDS("XBRXBZWDS","选八任选八中五单式","syxw_xbrxbzwdsserviceimpl"),
	
	QWDDS("QWDDS","趣味定单双","syxw_qwddsserviceimpl"),
	QWCZW("QWCZW","趣味猜中位","syxw_qwczwserviceimpl");
	
	
	private String code;
	private String description;
	private String serviceClassName; //对应的实现逻辑
	
	private ESYXWKind(String code,String description,String serviceClassName){
		this.code = code;
		this.description = description;
		this.serviceClassName = serviceClassName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getServiceClassName() {
		return serviceClassName;
	}

	public void setServiceClassName(String serviceClassName) {
		this.serviceClassName = serviceClassName;
	}
	public static void main(String[] args) {
		System.out.println(ESYXWKind.values().length);
	}
}
