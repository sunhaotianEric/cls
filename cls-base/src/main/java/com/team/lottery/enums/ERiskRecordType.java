package com.team.lottery.enums;

/**
 * 风控记录类型
 * @author
 *
 */
public enum ERiskRecordType {

	USER_LOGIN_IP_UNUSUAL("USER_LOGIN_IP_UNUSUAL","用户登录IP异常"),
	BANK_CARD_SAME_NAME("BANK_CARD_SAME_NAME","银行卡姓名同名");
	
	private String code;
	private String description;
	
	private ERiskRecordType(String code,String description){
		this.code = code;
		this.description = description;
		
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
