package com.team.lottery.enums;

/**
 * 处理状态
 * 枚举
 * @author Owner
 *
 */
public enum EWithdrawAutoStatus {

	SENDING("SENDING","发送中"),
	SENDED("SENDED","已发送"),
	SUCCESS("SUCCESS","成功"),
	FAIL("FAIL","失败");
	
	private String code;
	private String description;

	private EWithdrawAutoStatus(String code, String description) {
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
