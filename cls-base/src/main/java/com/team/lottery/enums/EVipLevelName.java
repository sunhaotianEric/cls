package com.team.lottery.enums;

public enum EVipLevelName {
	
	NORMAL_VIP("NORMAL_VIP", "普通会员"),
	GOLDEN_VIP("GOLDEN_VIP", "黄金会员"),
	PLATINUM_VIP("PLATINUM_VIP", "铂金会员"),
	DIAMONDS_VIP("DIAMONDS_VIP", "钻石会员"),
	EXTREME_VIP("EXTREME_VIP", "至尊会员");

	private String code;
	private String description;
	
	private EVipLevelName(String code,String description){
		this.code = code;
		this.description = description;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
