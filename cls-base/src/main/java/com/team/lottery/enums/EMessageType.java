package com.team.lottery.enums;

/**
 * 消息类型
 * @author luocheng
 *
 */
public enum EMessageType {
	
	LOGIN_TYPE("LOGIN_TYPE", "登录消息"),
	LOTTERY_CACHE_TYPE("LOTTERY_CACHE_TYPE", "投注缓存消息"),
	SEND_MAIL_TYPE("SEND_MAIL_TYPE", "发送邮件消息"),
	BIZSSYTEM_CACHE_TYPE("BIZSSYTEM_CACHE_TYPE", "业务系统对象消息"),
	BIZSSYTEMDOMAIN_CACHE_TYPE("BIZSSYTEMDOMAIN_CACHE_TYPE", "域名管理对象消息"),
	BIZSSYTEMINFO_CACHE_TYPE("BIZSSYTEMINFO_CACHE_TYPE", "系统信息管理对象消息"),
	REDIS_PUB_YPE("REDIS_PUB_YPE", "redis发布对象消息");
	private String code;
	private String description;
	
	private EMessageType(String code,String description) {
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
