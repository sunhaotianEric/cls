package com.team.lottery.enums;

/**
 * 后台用户错误密码记录日志枚举类.
 * 
 * @author jamine
 *
 */
public enum EAdminPasswordErrorLog {
	// 后台管理员登陆密码错误类型.
	ADMIN_LOGIN_PWD_ERROR("ADMIN_LOGIN_PWD_ERROR", "管理员登陆时密码错误"),
	
	// 后台管理员操作时密码错误(非登陆时).
	ADMIN_OPERATION_ADMIN_PWD_ERROR("ADMIN_OPERATION_ADMIN_PWD_ERROR", "管理员操作管理员时密码错误"),
	ADMIN_OPERATION_USER_PWD_ERROR("ADMIN_OPERATION_USER_PWD_ERROR", "管理员操作用户时密码错误");

	// 大写英文描述.
	private String code;
	// 中文描述.
	private String description;

	private EAdminPasswordErrorLog(String code, String description) {
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
