package com.team.lottery.enums;

/**
 * 操作类型日志
 * @author chenhsh
 *
 */
public enum EOperateLogType {

	WITH_DRAW_SECURITY_PASSWORD("WITH_DRAW_SECURITY_PASSWORD","取款安全密码"),
	BANK_SET_SECURITY_PASSWORD("BANK_SET_SECURITY_PASSWORD","银行设置安全密码"),
	BANK_BIND_SECURITY_PASSWORD("BANK_BIND_SECURITY_PASSWORD","银行卡绑定安全密码"),
	UPDATE_SECURITY_PASSWORD("UPDATE_SECURITY_PASSWORD","更新安全密码"),
	UPDATE_USERINFO_PASSWORD("UPDATE_USERINFO_PASSWORD","更新个人资料安全密码"),
	TRANSFER_PASSWORD("TRANSFER_PASSWORD","转账安全密码"),
	CHECK_SAVE_PASSWORD("CHECK_SAVE_PASSWORD","验证安全密码"),
	CHECK_SAVE_QUESTION("CHECK_SAVE_QUESTION","验证安全问题"),
	CHECK_SAVE_RETRIEVE("CHECK_SAVE_RETRIEVE","验证找回密码"),
	LOGIN_ERROR_LOG("LOGIN_ERROR_LOG","登陆错误日志"),
	ADMIN_LOGIN_ERROR_LOG("ADMIN_LOGIN_ERROR_LOG","管理员登陆错误日志"),
	WITH_DRAW_LOG("WITH_DRAW_LOG","取现日志记录"),
	MAC_LIMIT_FOR_REGISTER("MAC_LIMIT_FOR_REGISTER","注册次数限制");
	
	private String code;
	private String description;
	
	private EOperateLogType(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
