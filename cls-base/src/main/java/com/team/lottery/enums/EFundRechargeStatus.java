package com.team.lottery.enums;

/**
 * 充值状态
 * @author chenhsh
 *
 */
public enum EFundRechargeStatus {

	PAYMENT("PAYMENT","支付中"),
	PAYMENT_SUCCESS("PAYMENT_SUCCESS","支付成功"),
	PAYMENT_CLOSE("PAYMENT_CLOSE","订单关闭");
	
	private String code;
	private String description;
	
	private EFundRechargeStatus(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
