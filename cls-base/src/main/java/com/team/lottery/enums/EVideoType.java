package com.team.lottery.enums;

public enum EVideoType {
	
	ZHI_BO("ZHI_BO", "直播"),
	RIHAN_AV("RIHAN_AV", "日韩"),
	ZIPAI_TOUPAI("ZIPAI_TOUPAI", "自拍偷拍"),
	JINGSON_FILM("JINGSON_FILM", "惊悚电影"),
	MEINU_ZHUBO("MEINU_ZHUBO", "美女主播"),
	GUOCHAN_SANJI("GUOCHAN_SANJI", "国产三级"),
	DIANYING_DAPIAN("DIANYING_DAPIAN", "电影大片"),
	SEQING_JUQING("SEQING_JUQING", "色情剧情");
	
	private String code;
	private String description;
	
	private EVideoType(String code, String description) {
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
