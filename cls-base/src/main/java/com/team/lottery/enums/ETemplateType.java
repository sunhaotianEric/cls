package com.team.lottery.enums;

public enum ETemplateType {
	
	BLUE("BLUE", "蓝色"),
	RED("RED", "红色");

	private String code;
	private String description;
	
	private ETemplateType(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
