package com.team.lottery.enums;

/**
 * 契约分红，日工资发放策略
 * @author lxh
 *
 */
public enum EReleasePolicy {
	
	MANANUAL_RELEASE("MANANUAL_RELEASE","手动发放"),
	AUTO_DOWN_RELEASE("AUTO_DOWN_RELEASE","自动逐级发放"),
	AUTO_ACCOUNT_FIRST_RELEASE("AUTO_ACCOUNT_FIRST_RELEASE","自动优先发放");
	
	private String code;
	private String description;
	
	private EReleasePolicy(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
