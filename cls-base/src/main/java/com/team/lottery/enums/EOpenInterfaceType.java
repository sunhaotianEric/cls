package com.team.lottery.enums;

/**
 * 开奖接口类型
 * @author Administrator
 *
 */
public enum EOpenInterfaceType {
	
	CPK("CPK", "彩票控"),
	KCW("KCW", "开彩网"),
	XCW("XCW", "星彩网");
	private String code;
	private String description;
	
	private EOpenInterfaceType(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
