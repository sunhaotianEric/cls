package com.team.lottery.enums;

/**
 * 时时彩类玩法
 * @author chenhsh
 *
 */
public enum ESSCKind {

	WXFS("WXFS","五星复式","ssc_wxfsServiceImpl"),
	WXDS("WXDS","五星单式","ssc_wxdsServiceImpl"),
	WXZX120("WXZX120","五星组选120","ssc_wxzx120ServiceImpl"),
	WXZX60("WXZX60","五星组选60","ssc_wxzx60ServiceImpl"),
	WXZX30("WXZX30","五星组选30","ssc_wxzx30ServiceImpl"),
	WXZX20("WXZX20","五星组选20","ssc_wxzx20ServiceImpl"),
	WXZX10("WXZX10","五星组选10","ssc_wxzx10ServiceImpl"),
	WXZX5("WXZX5","五星组选5","ssc_wxzx5ServiceImpl"),
	WXYMBDW("WXYMBDW","五星一码不定位","ssc_wxymbdwServiceImpl"),
	WXEMBDW("WXEMBDW","五星二码不定位","ssc_wxembdwServiceImpl"),
	WXSMBDW("WXSMBDW","五星三码不定位","ssc_wxsmbdwServiceImpl"),
	
	SXFS("SXFS","四星复式","ssc_sxfsServiceImpl"),
	SXDS("SXDS","四星单式","ssc_sxdsServiceImpl"),
	SXZX24("SXZX24","四星组选24","ssc_sxzx24ServiceImpl"),
	SXZX12("SXZX12","四星组选12","ssc_sxzx12ServiceImpl"),
	SXZX6("SXZX6","四星组选6","ssc_sxzx6ServiceImpl"),
	SXZX4("SXZX4","四星组选4","ssc_sxzx4ServiceImpl"),
	SXYMBDW("SXYMBDW","四星一码不定位","ssc_sxymbdwServiceImpl"),
	SXEMBDW("SXEMBDW","四星二码不定位","ssc_sxembdwServiceImpl"),
	
	QSZXFS("QSZXFS","前三直选复式","ssc_qszxfsServiceImpl"),
	QSZXDS("QSZXDS","前三直选单式","ssc_qszxdsServiceImpl"),
	QSZXHZ("QSZXHZ","前三直选和值","ssc_qszxhzServiceImpl"),
	QSZXHZ_G("QSZXHZ_G","前三组选和值","ssc_qszxhz_GServiceImpl"),
	QSZS("QSZS","前三组三","ssc_qszsServiceImpl"),
	QSZL("QSZL","前三组六","ssc_qszlServiceImpl"),
	QSYMBDW("QSYMBDW","前三一码不定位","ssc_qsymbdwServiceImpl"),
	QSEMBDW("QSEMBDW","前三二码不定位","ssc_qsembdwServiceImpl"),
	
	HSZXFS("HSZXFS","后三直选复式","ssc_hszxfsServiceImpl"),
	HSZXDS("HSZXDS","后三直选单式","ssc_hszxdsServiceImpl"),
	HSZXHZ("HSZXHZ","后三直选和值","ssc_hszxhzServiceImpl"),
	HSZXHZ_G("HSZXHZ_G","后三组选和值","ssc_hszxhz_GServiceImpl"),
	HSZS("HSZS","后三组三","ssc_hszsServiceImpl"),
	HSZL("HSZL","后三组六","ssc_hszlServiceImpl"),
	HSYMBDW("HSYMBDW","后三一码不定位","ssc_hsymbdwServiceImpl"),
	HSEMBDW("HSEMBDW","后三二码不定位","ssc_hsembdwServiceImpl"),
	
	QEZXFS("QEZXFS","前二直选复式","ssc_qezxfsServiceImpl"),
	QEZXDS("QEZXDS","前二直选单式","ssc_qezxdsServiceImpl"),
	QEZXHZ("QEZXHZ","前二直选和值","ssc_qezxhzServiceImpl"),
	QEZXFS_G("QEZXFS_G","前二组选复式","ssc_qezxfs_GServiceImpl"),
	QEZXDS_G("QEZXDS_G","前二组选单式","ssc_qezxds_GServiceImpl"),
	QEZXHZ_G("QEZXHZ_G","前二组选和值","ssc_qezxhz_GServiceImpl"),
	
	HEZXFS("HEZXFS","后二直选复式","ssc_hezxfsServiceImpl"),
	HEZXDS("HEZXDS","后二直选单式","ssc_hezxdsServiceImpl"),
	HEZXHZ("HEZXHZ","后二直选和值","ssc_hezxhzServiceImpl"),
	HEZXFS_G("HEZXFS_G","后二组选复式","ssc_hezxfs_GServiceImpl"),
	HEZXDS_G("HEZXDS_G","后二组选单式","ssc_hezxds_GServiceImpl"),
	HEZXHZ_G("HEZXHZ_G","后二组选和值","ssc_hezxhz_GServiceImpl"),
	
	WXDWD("WXDWD","五星定位胆","ssc_wxdwdServiceImpl"),
	YXQY("YXQY","一星前一","ssc_yxqyServiceImpl"),
	YXHY("YXHY","一星后一","ssc_yxhyServiceImpl"),
	
	QEDXDS("QEDXDS","前二大小单双复式","ssc_qedxdsServiceImpl"),
	HEDXDS("HEDXDS","后二大小单双复式","ssc_hedxdsServiceImpl"),
	
	//RXY("RXY","任选一","ssc_rxyServiceImpl"),  //任选一
	RXE("RXE","任选二","ssc_rxeServiceImpl"),  //任选二
	RXS("RXS","任选三","ssc_rxsServiceImpl"),  //任选三
	RXSI("RXSI","任选四","ssc_rxsiServiceImpl"),  //任选四
	RXSIZXDS("RXSIZXDS","任选四直选单式","ssc_rxsizxdsServiceImpl"),  //任选四直选单式
	RXEZXDS("RXEZXDS","任选二直选单式","ssc_rxezxdsServiceImpl"),  //任选二直选单式
	RXSZXDS("RXSZXDS","任选三直选单式","ssc_rxszxdsServiceImpl"),  //任选三直选单式
	//新增玩法
	ZHDN("ZHDN","斗牛","ssc_zhdnServiceImpl"),  //总和斗牛
	ZHQS("ZHQS","前三总和","ssc_zhqsServiceImpl"),  //总和前三
	ZHZS("ZHZS","中三总和","ssc_zhzsServiceImpl"),  //总和中三
	ZHHS("ZHHS","后三总和","ssc_zhhsServiceImpl"),  //总和后三
	ZHWQLHSH("ZHWQLHSH","五球/龙虎/梭哈","ssc_zhwqlhshServiceImpl");  //总和五球龙虎梭哈
	
	private String code;
	private String description;
	private String serviceClassName; //对应的实现逻辑
	
	private ESSCKind(String code,String description,String serviceClassName){
		this.code = code;
		this.description = description;
		this.serviceClassName = serviceClassName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getServiceClassName() {
		return serviceClassName;
	}

	public void setServiceClassName(String serviceClassName) {
		this.serviceClassName = serviceClassName;
	}
	public static void main(String[] args) {
		System.out.println(ESSCKind.values().length);
	}
}
