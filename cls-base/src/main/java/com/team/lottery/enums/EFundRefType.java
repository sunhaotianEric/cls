package com.team.lottery.enums;

/**
 * 充值设置关联类型
 * @author chenhsh
 *
 */
public enum EFundRefType {

	TRANSFER("TRANSFER", "转账方式"),
	THIRDPAY("THIRDPAY", "第三方支付方式");
	
	private String code;
	private String description;
	
	private EFundRefType(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
