package com.team.lottery.enums;


/**
 * 追号时间类型..
 * @author chenhsh
 *
 */
public enum EAfterNumberType {

	TOMORROW("TOMORROW","明天"),
	TODAY("TODAY","今天");
	
	private String code;
	private String description;
	
	private EAfterNumberType(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
