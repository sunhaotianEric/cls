package com.team.lottery.enums;


/**
 * 六合彩类玩法枚举
 * @author Administrator
 *
 */
public enum ELHCKind {
	TMA("TMA","特码A","lhc_tmaServiceImpl"),  //特码a
	TMB("TMB","特码B","lhc_tmbServiceImpl"),  //特码b
	
	ZMA("ZMA","正码A","lhc_zmaServiceImpl"),  //正码a
	ZMB("ZMB","正码B","lhc_zmbServiceImpl"),  //正码b
	
	ZMT1("ZMT1","正一特","lhc_zmt1ServiceImpl"),  //正码特1
	ZMT2("ZMT2","正二特","lhc_zmt2ServiceImpl"),  //正码特2
	ZMT3("ZMT3","正三特","lhc_zmt3ServiceImpl"),  //正码特3
	ZMT4("ZMT4","正四特","lhc_zmt4ServiceImpl"),  //正码特4
	ZMT5("ZMT5","正五特","lhc_zmt5ServiceImpl"),  //正码特5
	ZMT6("ZMT6","正六特","lhc_zmt6ServiceImpl"),  //正码特6
	
	ZM16("ZM16","正码1-6","lhc_zm16ServiceImpl"),  //正码1-6
	
	LMSQZ("LMSQZ","三全中","lhc_lmsqzServiceImpl"),  //三全中
	LMSZ22("LMSZ22","三中二","lhc_lmsz22ServiceImpl"),  //三中二之中二
	LMSZ23("LMSZ23","三中二","lhc_lmsz23ServiceImpl"),  //三中二之中三
	LMEQZ("LMEQZ","二全中","lhc_lmeqzServiceImpl"),  //二全中
	LMEZT("LMEZT","二中特","lhc_lmeztServiceImpl"),  //二中特中之特
	LMEZ2("LMEZ2","二中特","lhc_lmez2ServiceImpl"),  //二中特中之二	
	LMTC("LMTC","特串","lhc_lmtcServiceImpl"),  //特串
	LMSZ1("LMSZ1","四中一","lhc_lmsz1ServiceImpl"),  //四中一
	
    BB("BB","半波","lhc_bbServiceImpl"),  //半波
    
    YXWS("YXWS","一肖/尾数","lhc_yxwsServiceImpl"),  //一肖/尾数
        
    TMSX("TMSX","特码生肖","lhc_tmsxServiceImpl"),  //特码生肖
	
    HX2("HX2","二肖","lhc_hx2ServiceImpl"),  //合肖-二肖
    HX3("HX3","三肖","lhc_hx3ServiceImpl"),  //合肖-三肖
    HX4("HX4","四肖","lhc_hx4ServiceImpl"),  //合肖-四肖
    HX5("HX5","五肖","lhc_hx5ServiceImpl"),  //合肖-五肖
    HX6("HX6","六肖","lhc_hx6ServiceImpl"),  //合肖-六肖
    HX7("HX7","七肖","lhc_hx7ServiceImpl"),  //合肖-七肖
    HX8("HX8","八肖","lhc_hx8ServiceImpl"),  //合肖-八肖
    HX9("HX9","九肖","lhc_hx9ServiceImpl"),  //合肖-九肖
    HX10("HX10","十肖","lhc_hx10ServiceImpl"),  //合肖-十肖
    HX11("HX11","十一肖","lhc_hx11ServiceImpl"),  //合肖-十一肖
    
    LXELZ("LXELZ","二肖连中","lhc_lxelzServiceImpl"),  //连肖-二肖连中
    LXSLZ("LXSLZ","三肖连中","lhc_lxslzServiceImpl"),  //连肖-三肖连中
    LXSILZ("LXSILZ","四肖连中","lhc_lxsilzServiceImpl"),  //连肖-四肖连中
    LXWLZ("LXWLZ","五肖连中","lhc_lxwlzServiceImpl"),  //连肖-五肖连中
    LXELBZ("LXELBZ","二肖连不中","lhc_lxelbzServiceImpl"),  //连肖-二肖连不中
    LXSLBZ("LXSLBZ","三肖连不中","lhc_lxslbzServiceImpl"),  //连肖-三肖连不中
    LXSILBZ("LXSILBZ","四肖连不中","lhc_lxsilbzServiceImpl"),  //连肖-四肖连不中
    
    WSLELZ("WSLELZ","二尾连中","lhc_wslelzServiceImpl"),  //尾数连-二尾连中
    WSLSLZ("WSLSLZ","三尾连中","lhc_wslslzServiceImpl"),  //尾数连-三尾连中
    WSLSILZ("WSLSILZ","四尾连中","lhc_wslsilzServiceImpl"),  //尾数连-四尾连中
    WSLELBZ("WSLELBZ","二尾连不中","lhc_wslelbzServiceImpl"),  //尾数连-二尾连不中
    WSLSLBZ("WSLSLBZ","三尾连不中","lhc_wslslbzServiceImpl"),  //尾数连-三尾连不中
    WSLSILBZ("WSLSILBZ","四尾连不中","lhc_wslsilbzServiceImpl"),  //尾数连-四尾连不中
        
    QBZ5("QBZ5","五不中","lhc_qbz5ServiceImpl"),  //全不中-五不中
    QBZ6("QBZ6","六不中","lhc_qbz6ServiceImpl"),  //全不中-六不中
    QBZ7("QBZ7","七不中","lhc_qbz7ServiceImpl"),  //全不中-七不中
    QBZ8("QBZ8","八不中","lhc_qbz8ServiceImpl"),  //全不中-八不中
    QBZ9("QBZ9","九不中","lhc_qbz9ServiceImpl"),  //全不中-九不中
    QBZ10("QBZ10","十不中","lhc_qbz10ServiceImpl"),  //全不中-十不中
    QBZ11("QBZ11","十一不中","lhc_qbz11ServiceImpl"),  //全不中-十一不中
    QBZ12("QBZ12","十二不中","lhc_qbz12ServiceImpl");  //全不中-十二不中
	
	private String code;
	private String description;
	private String serviceClassName; //对应的实现逻辑
	
	private ELHCKind(String code,String description,String serviceClassName){
		this.code = code;
		this.description = description;
		this.serviceClassName = serviceClassName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getServiceClassName() {
		return serviceClassName;
	}

	public void setServiceClassName(String serviceClassName) {
		this.serviceClassName = serviceClassName;
	}
}
