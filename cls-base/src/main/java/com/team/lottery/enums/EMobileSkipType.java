package com.team.lottery.enums;

/**
 * 手机端跳转方式
 * @author chenhsh
 *
 */
public enum EMobileSkipType {

	IFRAME_SKIP("1","IFRAME跳转"),
	NEW_OPEN("2","新在窗口打开"),
	Direct_SKIP("3","直接跳转");
	
	private String code;
	private String description;
	
	private EMobileSkipType(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
