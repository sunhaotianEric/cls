package com.team.lottery.enums;

/**
 * 计划列表使用
 * @author Administrator
 * ----十一选五-----
 */
public enum ESYXWKindCodePlan {
	DYQDW("DYQDW","第一球定位","syxw_codeplan_dyqdwServiceImpl"),
	DEQDW("DEQDW","第二球定位","syxw_codeplan_deqdwServiceImpl"),
	DSQDW("DSQDW","第三球定位","syxw_codeplan_dsqdwServiceImpl"),
	DSIQDW("DSIQDW","第四球定位","syxw_codeplan_dsiqdwServiceImpl"),
	DWQDW("DWQDW","第五球定位","syxw_codeplan_dwqdwServiceImpl"),
	
	YZY("YZY","一中一","syxw_codeplan_yzyServiceImpl"),
	EZE("EZE","二中二","syxw_codeplan_ezeServiceImpl");
	
	private String code;
	private String description;
	private String serviceClassName; //对应的实现逻辑
	
	ESYXWKindCodePlan(String code,String description,String serviceClassName){
		this.code = code;
		this.description = description;
		this.serviceClassName = serviceClassName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getServiceClassName() {
		return serviceClassName;
	}

	public void setServiceClassName(String serviceClassName) {
		this.serviceClassName = serviceClassName;
	}
	public static void main(String[] args) {
		System.out.println(EPK10Kind.values().length);
	}

}
