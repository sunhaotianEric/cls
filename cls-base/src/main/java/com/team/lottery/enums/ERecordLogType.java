package com.team.lottery.enums;

/**
 * 日志类型
 * @author chenhsh
 *
 */
public enum ERecordLogType {

	FRONT_SYSTEM("FRONT_SYSTEM","前台系统"),
	BACK_SYSTEM("BACK_SYSTEM","后台系统");
	
	private String code;
	private String description;
	
	private ERecordLogType(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
