package com.team.lottery.enums;

public enum EPasswordErrorType {
	// 登陆密码错误.
	PASSWORD_REEOR("PASSWORD_REEOR", "登陆密码错误"),
	// 安全密码错误.
	SAFE_PASSWORD_REEOR("SAFE_PASSWORD_REEOR", "安全密码错误");

	// 大写英文描述.
	private String code;
	// 中文描述.
	private String description;

	private EPasswordErrorType(String code, String description) {
			this.code = code;
			this.description = description;
		}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
