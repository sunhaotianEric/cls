package com.team.lottery.enums;

/**
 * 计划列表使用
 * @author Administrator
 * ----快三----
 */
public enum EKSKindCodePlan {
	HZDX("HZDX","和值大小","ks_codeplan_hzdxServiceImpl"),
	HZDS("HZDS","和值单双","ks_codeplan_hzdsServiceImpl"),
	
	WGHZ("WGHZ","五个和值","ks_codeplan_wghzServiceImpl"),
	SIGHZ("SIGHZ","四个和值","ks_codeplan_sighzServiceImpl"),
	BGHZ("BGHZ","八个和值","ks_codeplan_bghzServiceImpl"),
	
	DYQDW("DYQDW","第一球定位","ks_codeplan_dyqdwServiceImpl"),
	DEQDW("DEQDW","第二球定位","ks_codeplan_deqdwerviceImpl"),
	DSQDW("DSQDW","第三球定位","ks_codeplan_dsqdwServiceImpl");
	
	private String code;
	private String description;
	private String serviceClassName; //对应的实现逻辑
	
	EKSKindCodePlan(String code,String description,String serviceClassName){
		this.code = code;
		this.description = description;
		this.serviceClassName = serviceClassName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getServiceClassName() {
		return serviceClassName;
	}

	public void setServiceClassName(String serviceClassName) {
		this.serviceClassName = serviceClassName;
	}

}
