package com.team.lottery.enums;

/**
 * 银行类型
 * @author chenhsh
 *
 */
public enum EBankInfo {

	BOC("BOC","中国银行",1),
//	BJBANK("BJBANK","北京银行",1),
	ICBC("ICBC","中国工商银行",1),
	ABC("ABC","中国农业银行",1),
	CCB("CCB","中国建设银行",1),
	COMM("COMM","交通银行",1),
	CMB("CMB","招商银行",1),
	PSBC("PSBC","中国邮政储蓄银行",1),
	CEB("CEB","中国光大银行",1),
	SPABANK("SPABANK","平安银行",1),
	CITIC("CITIC","中信银行",1),
	CMBC("CMBC","中国民生银行",1),
	CIB("CIB","兴业银行",1),
	GDB("GDB","广发银行",1),
	HXBANK("HXBANK","华夏银行",1),
	SPDB("SPDB","浦发银行",1),
	CBHB("CBHB","渤海银行", 1),
	ALIPAY("ALIPAY","支付宝",0),
	ALIPAYWAP("ALIPAYWAP","支付宝WAP",0),
	WEIXIN("WEIXIN","微信",0),
	WEIXINWAP("WEIXINWAP","微信WAP",0),
	QQPAY("QQPAY","QQ钱包",0),
	QQPAYWAP("QQPAYWAP","QQ钱包WAP",0),
	UNIONPAY("UNIONPAY","银联扫码",0),
	JDPAY("JDPAY","京东扫码",0),
	WEIXINBARCODE("WEIXINBARCODE","微信条形码",0);
//	EGBANK("EGBANK","恒丰银行"),
//	SPDB("SPDB","浦发银行"),
//	BSB("BSB","包商银行"),
//	FJHXBC("FJHXBC","福建海峡银行"),
//	HZCB("HZCB","杭州银行"),
//	JHBANK("JHBANK","金华银行"),
//	JSBANK("JSBANK","江苏银行"),
//	JXBANK("JXBANK","嘉兴银行"),
//	NBBANK("NBBANK","宁波银行"),
//	NBYZ("NBYZ","鄞州银行"),
//	SDB("SDB","深圳发展银行"),
//	SHBANK("SHBANK","上海银行"),
//	SXCB("SXCB","绍兴银行"),
//	WZCB("WZCB","温州银行");
	
	private String code;
	private String description;
	private Integer isShow;
	
	private EBankInfo(String code,String description,Integer isShow){
		this.code = code;
		this.description = description;
		this.isShow = isShow;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getIsShow() {
		return isShow;
	}

	public void setIsShow(Integer isShow) {
		this.isShow = isShow;
	}
}
