package com.team.lottery.enums;

/**
 * 平台收费订单状态
 * @author chenhsh
 *
 */
public enum EPlatformChargePayStatus {

	PAYMENT("PAYMENT","未支付"),
	PAYMENT_NEEDNOT("PAYMENT_NEEDNOT", "无需支付"),
	PAYMENT_SUCCESS("PAYMENT_SUCCESS","支付成功"),
	PAYMENT_CLOSE("PAYMENT_CLOSE","不收取");
	
	private String code;
	private String description;
	
	private EPlatformChargePayStatus(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
