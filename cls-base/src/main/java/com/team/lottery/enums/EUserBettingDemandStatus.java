package com.team.lottery.enums;

/**
 * 打码要求状态
 * @author luocheng
 *
 */
public enum EUserBettingDemandStatus {
	
	GOING("GOING", "未完成"),
	END("END", "已完成"),
	FORCE_END("FORCE_END", "强制完成");

	private String code;
	private String description;
	
	private EUserBettingDemandStatus(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
