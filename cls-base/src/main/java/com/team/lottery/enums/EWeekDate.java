package com.team.lottery.enums;


/**
 * 星期日期
 * @author chenhsh
 *
 */
public enum EWeekDate {

	Sun("1","星期日"),
	Mon("2","星期一"),
	Tue("3","星期二"),
	Wed("4","星期三"),
	Thu("5","星期四"),
	Fri("6","星期五"),
	Sat("7","星期六");
	
	private String code;
	private String description;
	
	private EWeekDate(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
