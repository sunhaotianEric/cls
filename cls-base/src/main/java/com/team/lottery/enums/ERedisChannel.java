package com.team.lottery.enums;

public enum ERedisChannel {

	FRONTCACHE("FRONTCACHE","redis前台缓存刷新"),
	CLSCACHE("CLSCACHE","系统缓存刷新"),
	OPENCODECACHE("OPENCODECACHE","redis开奖缓存刷新");
	
	private String code;
	private String description;
	
	private ERedisChannel(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


}
