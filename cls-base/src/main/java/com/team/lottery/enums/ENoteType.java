package com.team.lottery.enums;

/**
 * 信息类型
 * @author chenhsh
 *
 */
public enum ENoteType {

	SYSTEM("SYSTEM","系统消息"),
	UP_DOWN("UP_DOWN","上级联系下级"),
	DOWN_UP("DOWN_UP","下级联系上级");
	
	private String code;
	private String description;
	
	private ENoteType(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
