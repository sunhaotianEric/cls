package com.team.lottery.enums;

public enum EAlarmInfo {
	
	TZ("TZ", "投注"),
	ZJ("ZJ", "中奖"),
	CZ("CZ", "充值"),
	TX("TX", "提现");

	private String code;
	private String description;
	
	private EAlarmInfo(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
