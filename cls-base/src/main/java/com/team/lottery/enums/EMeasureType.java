package com.team.lottery.enums;

public enum EMeasureType {
	
	HALF_MONTH_BONUS_MEASURE("HALF_MONTH_BONUS_MEASURE", "半月分红"),
	DAY_SALARY_MEASURE("DAY_SALARY_MEASURE", "日工资"),
	MONTH_SALARY_MEASURE("MONTH_SALARY_MEASURE", "月工资"),
	DAY_ACTIVITY_MEASURE("DAY_ACTIVITY_MEASURE","日活动"),
	DAY_COMMISSION_MEASURE("DAY_COMMISSION_MEASURE", "日佣金");

	private String code;
	private String description;
	
	private EMeasureType(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
