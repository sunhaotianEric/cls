package com.team.lottery.enums;

/**
 * 投注模式,元\角\分
 * @author chenhsh
 *
 */
public enum ELotteryModel {

	YUAN("YUAN","元"),
	JIAO("JIAO","角"),
	FEN("FEN","分"),
	LI("LI","厘"),
	HAO("HAO", "毫");
	
	private String code;
	private String description;
	
	private ELotteryModel(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
