package com.team.lottery.enums;

/**
 * 平台收费类型
 * @author luocheng
 *
 */
public enum EPlatformChargeType {
	
	BONUS("BONUS", "分红费"),
	MAINTAIN("MAINTAIN", "维护费"),
	LABOR("LABOR", "工资支出"),
	EQUIPMENT("EQUIPMENT", "服务器支出");

	private String code;
	private String description;
	
	private EPlatformChargeType(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
