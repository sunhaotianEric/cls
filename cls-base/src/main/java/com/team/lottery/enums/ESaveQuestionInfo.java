package com.team.lottery.enums;

/**
 * 安全问题类型
 * @author 
 *
 */
public enum ESaveQuestionInfo {

	Q1("Q1","您母亲的姓名是？"),
	Q2("Q2","您配偶的生日是？"),
	Q3("Q3","您的学号或工号是？"),
	Q4("Q4","您母亲的生日是？"),
	Q5("Q5","您高中班主任的名字是？"),
	Q6("Q6","您父亲的姓名是？"),
	Q7("Q7","您小学班主任的名字是？"),
	Q8("Q8","您父亲的生日是？"),
	Q9("Q9","您配偶的姓名是？"),
	Q10("Q10","您初中班主任的名字是？"),
	Q11("Q11","您最熟悉的好友名字是？"),
	Q12("Q12","您最熟悉的舍友名字是？");
	
	private String code;
	private String description;
	
	private ESaveQuestionInfo(String code,String description){
		this.code = code;
		this.description = description;
		
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
