package com.team.lottery.enums;

/**
 * 自动出款状态 
 * @author chenhsh
 *
 */
public enum EFundAutoWithdrawDealStatus {

	NOT_HANDLE("NOT_HANDLE","未处理自动出款"),
	DEALING("DEALING","自动出款处理中"),
	SUCCESS("SUCCESS","自动出款成功"),
	FAIL("FAIL","自动出款失败");
	
	private String code;
	private String description;
	
	private EFundAutoWithdrawDealStatus(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
