package com.team.lottery.enums;

public enum EUserOperateLogModel {
	
	SAFE_QUESTION("SAFE_QUESTION", "安全问题"),
	PASSWORD("PASSWORD", "密码"),
	SAFE_PASSWORD("SAFE_PASSWORD", "安全密码"),

	USER("USER", "会员管理"),
	USER_BANK("USER_BANK", "用户银行卡"),
	
	BONUS_CONFIG("BONUS_CONFIG", "契约分红"),
	QUOTA_MANAGE("QUOTA_MANAGE", "配额管理"),
	AGENCY_MODEL_ADD("AGENCY_MODEL_ADD", "代理升点"),
	DAY_SALARY_CONFIG("DAY_SALARY_CONFIG", "日工资");
	
	private String name;
	private String description;
	
	private EUserOperateLogModel(String name, String description) {
		this.name = name;
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getCode(){
		return name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
