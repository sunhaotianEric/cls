package com.team.lottery.enums;

/**
 * 短信配置类型区分(分为国际短信,和默认短信).
 * 
 * @author Jamine
 *
 */
public enum ESmsSendType {
	
	DEFAULT("DEFAULT", "默认短信接口"), 
	INTERNATIONNAL("INTERNATIONNAL", "国际短信接口");

	private String code;
	private String description;

	private ESmsSendType(String code, String description) {
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
