package com.team.lottery.enums;
/**
 * 活动类型
 * @author Administrator
 *
 */
public enum EConsumeActivityType {
	
	NORMAL("NORMAL", "普通"),
	FFC("FFC", "分分彩");
	
	private String code;
	private String description;
	
	private EConsumeActivityType(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
