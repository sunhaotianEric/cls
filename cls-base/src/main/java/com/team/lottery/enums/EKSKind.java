package com.team.lottery.enums;

/**
 * 快三类玩法实现
 * @author gs
 *
 */
public enum EKSKind {

	HZ("HZ","和值","ks_hzServiceImpl"),
	DXDS("DXDS","大小单双","ks_dxdsServiceImpl"),
	STHTX("STHTX","三同号通选","ks_sthtxServiceImpl"),
	STHDX("STHDX","三同号单选","ks_sthdxServiceImpl"),
	SBTHBZ("SBTHBZ","三不同号标准","ks_sbthbzServiceImpl"),
	SBTHDT("SBTHDT","三不同号胆拖","ks_sbthdtServiceImpl"),
	SLHTX("SLHTX","三连号通选","ks_slhtxServiceImpl"),
	ETHFX("ETHFX","二同号复选","ks_ethfxServiceImpl"),
	ETHDX("ETHDX","二同号单选","ks_ethdxServiceImpl"),
	EBTHBZ("EBTHBZ","二不同号标准","ks_ebthbzServiceImpl"),
	EBTHDT("EBTHDT","二不同号胆拖","ks_ebthdtServiceImpl"),
	CYG("CYG","猜一个就中奖","ks_cygServiceImpl");  

	private String code;
	private String description;
	private String serviceClassName; //对应的实现逻辑
	
	private EKSKind(String code,String description,String serviceClassName){
		this.code = code;
		this.description = description;
		this.serviceClassName = serviceClassName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getServiceClassName() {
		return serviceClassName;
	}

	public void setServiceClassName(String serviceClassName) {
		this.serviceClassName = serviceClassName;
	}
	public static void main(String[] args) {
		System.out.println(EKSKind.values().length);
	}
}
