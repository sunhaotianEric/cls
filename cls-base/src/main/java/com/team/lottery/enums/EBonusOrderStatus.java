package com.team.lottery.enums;

/**
 * 分红申请订单处理状态
 * @author gs
 *
 */
public enum EBonusOrderStatus {

	NOT_RELEASE("NOT_RELEASE","尚未发放"),
	RELEASED("RELEASED","发放完毕"),
	NEEDNOT_RELEASE("NEEDNOT_RELEASE","不需分红"),
	OUTTIME_NOT_RELEASE("OUTTIME_NOT_RELEASE","逾期未发放"),
	OUTTIME_RELEASED("OUTTIME_RELEASED","逾期补发完毕"),
	FORCE_RELEASED("FORCE_RELEASED","强制发放完毕");
	
	private String code;
	private String description;
	
	private EBonusOrderStatus(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
