package com.team.lottery.enums;


/**
 * 彩种大种类
 * @author chenhsh
 *
 */
public enum ELotteryTopKind {

	SSC("SSC","时时彩"),
	FFC("FFC","分分彩"),
	SYXW("SYXW","11选5"),
	KS("KS","快3"),
	PK10("PK10", "PK10"),
	DPC("DPC","低频彩"),
	TB("TB", "骰宝"),
	LHC("LHC", "六合彩"),
	XYEB("XYEB", "幸运28"),
	KLSF("KLSF","快乐十分");
	
	private String code;
	private String description;
	
	private ELotteryTopKind(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
