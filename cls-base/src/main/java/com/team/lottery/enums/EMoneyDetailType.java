package com.team.lottery.enums;

/**
 * 资金明细类型
 * @author chenhsh
 *
 */
public enum EMoneyDetailType {

	LOTTERY("LOTTERY","投注扣款",1,1),
	WIN("WIN","奖金派送",1,1),
	LOTTERY_STOP_AFTER_NUMBER("LOTTERY_STOP_AFTER_NUMBER","追号撤单",1,1), //订单中奖停止追号
	LOTTERY_REGRESSION("LOTTERY_REGRESSION","投注回退",1,1),
	
	PERCENTAGE("PERCENTAGE","推广返点",1,1),
	REBATE("REBATE","投注返点",1,1),
	
	
	RECHARGE("RECHARGE","充值",1,1),
	RECHARGE_PRESENT("RECHARGE_PRESENT","充值赠送",1,1),
	RECHARGE_MANUAL("RECHARGE_MANUAL", "人工存款", 1, 1),
    /*WAGES_PRESENT("WAGES_PRESENT","工资派送",1,1),*/
	ACTIVITIES_MONEY("ACTIVITIES_MONEY","活动彩金",1,1),
	SYSTEM_ADD_MONEY("SYSTEM_ADD_MONEY","人工存入",1,1),
	SYSTEM_REDUCE_MONEY("SYSTEM_REDUCE_MONEY","误存提出",1,1),
	MANAGE_REDUCE_MONEY("MANAGE_REDUCE_MONEY","行政提出",1,1),
	
	WITHDRAW("WITHDRAW","提现",1,1),
	WITHDRAW_FEE("WITHDRAW_FEE","提现手续费",1,1),
	
	PAY_TRANFER("PAY_TRANFER","转账转出",1,1),
	INCOME_TRANFER("INCOME_TRANFER","转账转入",1,1),
	
	HALF_MONTH_BOUNS("HALF_MONTH_BOUNS","契约分红",1,1),

	DAY_SALARY("DAY_SALARY","日工资",1,1),
	//下方几个资金明细类型暂时无用
	DAY_BOUNS("DAY_BOUNS","日分红",1,1),
	MONTH_SALARY("MONTH_SALARY","月工资",1,1),
	DAY_COMMISSION("DAY_COMMISSION","日佣金",1,1);
	
	/*SYSTEM_REDUCE_MONEY("SYSTEM_REDUCE_MONEY","系统扣费",1,1),
	REGISTER("REGISTER","注册赠送",1,1),
	EXTEND("EXTEND","推广消耗",0,1),
	ADD_EXTEND("ADD_EXTEND","推广增加",0,1),
	REDUCE_EXTEND("REDUCE_EXTEND","推广扣减",0,1),  
	
	OUT_WIN_CONTROL("OUT_WIN_CONTROL","超额扣款",1,1);*/
	
	private String code;
	private String description;
	private Integer isShow;
	private Integer isDirectagencyShow; //是否直属展示
	
	private EMoneyDetailType(String code,String description,Integer isShow,Integer isDirectagencyShow){
		this.code = code;
		this.description = description;
		this.isShow = isShow;
		this.isDirectagencyShow = isDirectagencyShow;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getIsShow() {
		return isShow;
	}

	public void setIsShow(Integer isShow) {
		this.isShow = isShow;
	}

	public Integer getIsDirectagencyShow() {
		return isDirectagencyShow;
	}

	public void setIsDirectagencyShow(Integer isDirectagencyShow) {
		this.isDirectagencyShow = isDirectagencyShow;
	}
}
