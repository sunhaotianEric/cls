package com.team.lottery.enums;

/**
 * pk10玩法
 * @author Administrator
 *
 */
public enum EPK10Kind {
	QYZXFS("QYZXFS","前一直选复式","pk10_qyzxfsServiceImpl"),
	QEZXFS("QEZXFS","前二直选复式","pk10_qezxfsServiceImpl"),
	QEZXDS("QEZXDS","前二直选单式","pk10_qezxdsServiceImpl"),
	QSZXFS("QSZXFS","前三直选复式","pk10_qszxfsServiceImpl"),
	QSZXDS("QSZXDS","前三直选单式","pk10_qszxdsServiceImpl"),
	QSIZXFS("QSIZXFS","前四直选复式","pk10_qsizxfsServiceImpl"),
	QSIZXDS("QSIZXDS","前四直选单式","pk10_qsizxdsServiceImpl"),
	QWZXFS("QWZXFS","前五直选复式","pk10_qwzxfsServiceImpl"),
	QWZXDS("QWZXDS","前五直选单式","pk10_qwzxdsServiceImpl"),
	DWD("DWD","定位胆","pk10_dwdServiceImpl"),
	LHD1VS10("LHD1VS10","龙虎斗1VS10","pk10_lhd1vs10ServiceImpl"),
	LHD2VS9("LHD2VS9","龙虎斗2VS9","pk10_lhd2vs9ServiceImpl"),
	LHD3VS8("LHD3VS8","龙虎斗3VS8","pk10_lhd3vs8ServiceImpl"),
	LHD4VS7("LHD4VS7","龙虎斗4VS7","pk10_lhd4vs7ServiceImpl"),
	LHD5VS6("LHD5VS6","龙虎斗5VS6","pk10_lhd5vs6ServiceImpl"),
	DXGJ("DXGJ","大小_冠军","pk10_dxgjServiceImpl"),
	DXYJ("DXYJ","大小_亚军","pk10_dxyjServiceImpl"),
	DXJJ("DXJJ","大小_季军","pk10_dxjjServiceImpl"),
	DXDSM("DXDSM","大小_第四名","pk10_dxdsmServiceImpl"),
	DXDWM("DXDWM","大小_第五名","pk10_dxdwmServiceImpl"),
	DSGJ("DSGJ","单双_冠军","pk10_dsgjServiceImpl"),
	DSYJ("DSYJ","单双_亚军","pk10_dsyjServiceImpl"),
	DSJJ("DSJJ","单双_季军","pk10_dsjjServiceImpl"),
	DSDSM("DSDSM","单双_第四名","pk10_dsdsmServiceImpl"),
	DSDWM("DSDWM","单双_第五名","pk10_dsdwmServiceImpl"),
	HZGYJ("HZGYJ","冠亚和值","pk10_hzgyjServiceImpl");
	
	private String code;
	private String description;
	private String serviceClassName; //对应的实现逻辑
	
	private EPK10Kind(String code,String description,String serviceClassName){
		this.code = code;
		this.description = description;
		this.serviceClassName = serviceClassName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getServiceClassName() {
		return serviceClassName;
	}

	public void setServiceClassName(String serviceClassName) {
		this.serviceClassName = serviceClassName;
	}
	public static void main(String[] args) {
		System.out.println(EPK10Kind.values().length);
	}
}
