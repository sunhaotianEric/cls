package com.team.lottery.enums;

/**
 * 订单状态
 * @author chenhsh
 *
 */
public enum EOrderStatus {

//	END_STOP_AFTER_NUMBER("END_STOP_AFTER_NUMBER","停止追单"),
	GOING("GOING","未开奖"),
	END("END","结束");
//	REGRESSION("REGRESSION","投注回退"); //用户未开奖前的主动撤单和订单异常管理员的回退订单
	
	private String code;
	private String description;
	
	private EOrderStatus(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
