package com.team.lottery.enums;

/**
 * 计划列表中奖状态
 * @author Administrator
 *
 */
public enum ECodePlanProstateStatus {
	
	DEALING("DEALING","等待开奖"),
	NOT_WINNING("NOT_WINNING","错"),
	WINNING("WINNING","中");
	
	private String code;
	private String description;
	
	private ECodePlanProstateStatus(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
