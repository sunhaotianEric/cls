package com.team.lottery.enums;

/**
 * 用户代理类型
 * @author chenhsh
 *
 */
public enum EUserDailLiLevel {

	SUPERAGENCY("SUPERAGENCY", "超级代理"),
	DIRECTAGENCY("DIRECTAGENCY","直属代理"),
	GENERALAGENCY("GENERALAGENCY","总代理"),
	ORDINARYAGENCY("ORDINARYAGENCY","普通代理"),
	REGULARMEMBERS("REGULARMEMBERS","普通会员");
	
	private String code;
	private String description;
	
	private EUserDailLiLevel(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
