package com.team.lottery.enums;

public enum EActivityType {
	
	NORMAL("NORMAL", "普通活动"),
	DAY_CONSUME_ACTIVITY("DAY_CONSUME_ACTIVITY", "流水返送活动"),
	SIGNIN_ACTIVITY("SIGNIN_ACTIVITY", "签到活动"),
	PROMOTON_ACTIVITY("PROMOTON_ACTIVITY", "晋级活动");
	
	private String code;
	private String description;
	
	private EActivityType(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
