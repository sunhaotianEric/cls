package com.team.lottery.enums;

/**
 * 中奖状态
 * @author chenhsh
 *
 */
public enum EProstateStatus {

	DEALING("DEALING","未开奖"),
	NOT_WINNING("NOT_WINNING","未中奖"),
	WINNING("WINNING","已中奖"),
	REGRESSION("REGRESSION","投注回退"), //用户未开奖前的主动撤单和订单异常管理员的回退订单
	END_STOP_AFTER_NUMBER("END_STOP_AFTER_NUMBER","停止追单"),
	NO_LOTTERY_BACKSPACE("NO_LOTTERY_BACKSPACE","未开奖回退"),
	ORDER_CANCELED("ORDER_CANCELED","已撤单"),
	STOP_DEALING("STOP_DEALING","停止交易"),
	UNUSUAL_BACKSPACE("UNUSUAL_BACKSPACE","强制回退");
	
	private String code;
	private String description;
	
	private EProstateStatus(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
