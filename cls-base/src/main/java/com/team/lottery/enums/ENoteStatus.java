package com.team.lottery.enums;

/**
 * 信息状态
 * @author chenhsh
 *
 */
public enum ENoteStatus {

	NO_READ("NO_READ","未读"),
	YET_READ("YET_READ","已读");
	
	private String code;
	private String description;
	
	private ENoteStatus(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
