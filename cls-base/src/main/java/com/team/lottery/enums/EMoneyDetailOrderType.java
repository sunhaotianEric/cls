package com.team.lottery.enums;

/**
 * 充值类型
 * @author chenhsh
 *
 */
public enum EMoneyDetailOrderType {

	_ZHI_("_ZHI_","转账收入"),
	_ZHP_("_ZHP_","转账支出"),
	_XF_("_XF_","管理员续费"),
	_EZS_("_EZS_","推广消耗"),
	_RZS_("_RZS_","注册赠送"),
	_WCZ_("_WCZ_","网银汇款"),
	_XCZ_("_XCZ_","在线充值"),
	_WZS_("_WZS_","网银充值赠送"),
	_QX_("_QX_","取现"),
	_TDPAY_("_TDPAY_","退单(支付)"),
	_TDWIN_("_TDWIN_","退单(中奖)"),
	_FH_("_FH_","分红");
	
	private String code;
	private String description;
	
	private EMoneyDetailOrderType(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
