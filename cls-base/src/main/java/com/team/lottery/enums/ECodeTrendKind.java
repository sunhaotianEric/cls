package com.team.lottery.enums;

/**
 * 开奖走势图彩种枚举
 * @author Administrator
 *
 */
public enum ECodeTrendKind {
	
	SSC("SSC","时时彩","sSCCodeTrendServiceImpl"),
	FC("FC","分分彩","fFCodeTrendServiceImpl"),
	SYXW("SYXW","11选5","sYXWCodeTrendServiceImpl"),
	KS("KS","快3","kSCodeTrenderviceImpl"),
	PK10("PK10", "PK10","pK10CodeTrendServiceImpl"),
	LHC("LHC","六合彩","lHCCodeTrendServiceImpl"),
	EB("EB","28","xYEBCodeTrendServiceImpl"),
	DPC("DPC","低频彩","dpcCodeTrendServiceImpl"),
	KLSF("KLSF","快乐十分","klsfCodeTrendServiceImpl");
	private String code;
	private String description;
	private String serviceClassName; //对应的实现逻辑
	
	private ECodeTrendKind(String code,String description,String serviceClassName) {
		this.code=code;
		this.description=description;
		this.serviceClassName=serviceClassName;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getServiceClassName() {
		return serviceClassName;
	}
	public void setServiceClassName(String serviceClassName) {
		this.serviceClassName = serviceClassName;
	}
}
