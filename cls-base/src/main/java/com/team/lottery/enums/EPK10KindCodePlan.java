package com.team.lottery.enums;

/**
 * 计划列表使用
 * @author Administrator
 * ----PK10类----
 */
public enum EPK10KindCodePlan {
	GYDS("GYDS","冠亚单双","pk10_gydsServiceImpl"),
	GYDX("GYDX","冠亚大小","pk10_gydxServiceImpl"),
	GJDW("GJDW","冠军定位","pk10_gjdwServiceImpl"),
	GJDX("GJDX","冠军大小","pk10_gjdxServiceImpl"),
	GJDS("GJDS","冠军单双","pk10_gjdsServiceImpl"),
	YJDW("YJDW","亚军定位","pk10_yjdwServiceImpl"),
	YJDX("YJDX","亚军大小","pk10_yjdxServiceImpl"),
	YJDS("YJDS","亚军单双","pk10_yjdsServiceImpl"),
	DSMDW("DSMDW","第三名定位","pk10_dsmdwServiceImpl"),
	DSMDX("DSMDX","第三名大小","pk10_dsmdxServiceImpl"),
	DSMDS("DSMDS","第三名单双","pk10_dsmdsServiceImpl"),
	DSIMDW("DSIMDW","第四名定位","pk10_dsimdwServiceImpl"),
	DSIMDX("DSIMDX","第四名大小","pk10_dsimdxServiceImpl"),
	DSIMDS("DSIMDS","第四名单双","pk10_dsimdsServiceImpl"),
	DWMDW("DWMDW","第五名定位","pk10_dwmdwServiceImpl"),
	DWMDX("DWMDX","第五名大小","pk10_dwmdxServiceImpl"),
	DWMDS("DWMDS","第五名单双","pk10_dwmdsServiceImpl");
	
	private String code;
	private String description;
	private String serviceClassName; //对应的实现逻辑
	
	EPK10KindCodePlan(String code,String description,String serviceClassName){
		this.code = code;
		this.description = description;
		this.serviceClassName = serviceClassName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getServiceClassName() {
		return serviceClassName;
	}

	public void setServiceClassName(String serviceClassName) {
		this.serviceClassName = serviceClassName;
	}
	public static void main(String[] args) {
		System.out.println(EPK10Kind.values().length);
	}

}
