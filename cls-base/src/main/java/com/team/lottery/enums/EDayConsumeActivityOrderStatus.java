package com.team.lottery.enums;

/**
 * 流水返送订单处理状态
 * @author gs
 *
 */
public enum EDayConsumeActivityOrderStatus {
	
	NOT_APPLY("NOT_APPLY","未申请"),
	APPLIED("APPLIED","已申请"),
	RELEASED("RELEASED","已发放"),
	NOTALLOWED_APPLY("NOTALLOWED_APPLY","不可申请");
	
	private String code;
	private String description;
	
	private EDayConsumeActivityOrderStatus(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
