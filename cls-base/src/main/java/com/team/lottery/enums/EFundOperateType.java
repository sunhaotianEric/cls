package com.team.lottery.enums;

/**
 * 充值和取现 
 * @author chenhsh
 *
 */
public enum EFundOperateType {

	RECHARGE("RECHARGE","充值"),
	WITHDRAW("WITHDRAW","取现");
	
	private String code;
	private String description;
	
	private EFundOperateType(String code,String description){
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
