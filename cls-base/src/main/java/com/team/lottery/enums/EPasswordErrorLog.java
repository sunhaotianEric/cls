package com.team.lottery.enums;

/**
 * 前台用户密码错误登陆日志记录类型(安全密码和登陆密码错误).
 * 
 * @author jamine
 *
 */
public enum EPasswordErrorLog {
	// 前台用户安全密码错误类型.
	WITHDRAW_SAFEPWD_ERROR("WITHDRAW_SAFEPWD_ERROR", "提现时安全密码错误"),
	BAND_EMAIL_SAFEPWD_ERROR("BAND_EMAIL_SAFEPWD_ERROR", "绑定邮箱时安全密码错误"),
	CHANGE_EMAIL_SAFEPWD_ERROR("CHANGE_EMAIL_SAFEPWD_ERROR", "修改邮箱时安全密码错误"),
	BAND_PHONE_SAFEPWD_ERROR("BAND_PHONE_SAFEPWD_ERROR", "绑定手机时安全密码错误"),
	CHANGE_PHONE_SAFEPWD_ERROR("CHANGE_PHONE_SAFEPWD_ERROR", "修改手机时安全密码错误"),
	ADD_BANKCARD_SAFEPWD_ERROR("ADD_BANKCARD_SAFEPWD_ERROR", "添加银行卡时安全密码错误"),
	EDIT_BANKCARD_SAFEPWD_ERROR("EDIT_BANKCARD_SAFEPWD_ERROR", "编辑银行卡时安全密码错误"),
	CHANGE_SAFEPWD_SAFEPWD_ERROR("CHANGE_SAFEPWD_SAFEPWD_ERROR", "修改安全密码时安全密码错误"),

	// 前台用户登陆密码错误类型.
	PC_LOGIN_PWD_ERROR("PC_LOGIN_PWD_ERROR", "电脑登陆时密码错误"),
	MOBILE_LOGIN_PWD_ERROR("MOBILE_LOGIN_PWD_ERROR", "手机登陆时密码错误"),
	CHANGE_PWD_PWD_ERROR("CHANGE_PWD_PWD_ERROR", "修改登陆密码时密码错误");
	
	// 大写英文描述.
	private String code;
	// 中文描述.
	private String description;

	private EPasswordErrorLog(String code, String description) {
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
