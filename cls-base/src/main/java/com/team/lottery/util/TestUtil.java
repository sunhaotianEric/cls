package com.team.lottery.util;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class TestUtil {

	public static void main(String[] args) throws IOException {
		// String str = "快彩网|网上买彩票_网上购彩_网上投注_本站仅供测试并非运营网站|http://localhost:84|";
		// System.out.println(str.split("\\|"));
		//
		// String str_1 = "ABcd";
		// System.out.println(str_1.toLowerCase().contains("ab"));

		// 5,6|4,7,3,-
		// String matchStr = "^\\d{1}$";
		// String codeStr = "0";
		// Pattern pattern = Pattern.compile(matchStr);
		// System.out.println(pattern.matcher(codeStr).matches());

		// String str = "-,-,-,-,-";
		// str = str.replaceAll(ConstantUtil.CODE_REPLACE, ""); //先将空余为数置空
		// String [] ss = str.split(ConstantUtil.SOURCECODE_SPLIT);
		// for(String s : ss){
		// System.out.println("");
		// }

		// String lotteryCodes = "[csq]5,6|4,7,3,-$[csq]4,6|4,7,3,-";
		// String[] lotteryCodeArrays =
		// lotteryCodes.split(ConstantUtil.SOURCECODE_PROTYPE_SPLIT);
		// for(String lotteryCodeProtype : lotteryCodeArrays){
		// int startPosition = lotteryCodeProtype.indexOf("[");
		// int endPosition = lotteryCodeProtype.indexOf("]");
		// String kind = lotteryCodeProtype.substring(startPosition,endPosition
		// + 1);
		// String code = lotteryCodeProtype.substring(endPosition + 1,
		// lotteryCodeProtype.length());
		// System.out.println(kind);
		// System.out.println(code);
		// }

		// BigDecimal b1 = new BigDecimal(12);
		// b1 = b1.add(new BigDecimal(12));
		// System.out.println(b1);

		// String str =
		// "http://localhost:84#http://localhost:84#http://localhost:84";
		// String[] s = str.split("#");
		// for(String ss : s){
		// System.out.println(ss);
		// }

		// Date nowStartDate = DateUtil.getNowStartTime();
		// System.out.println(DateUtils.getDateToStrByFormat(nowStartDate,
		// "yyyy/MM/dd HH:mm:ss"));

		// List<Integer> test = new ArrayList<Integer>();
		// for (int i = 0; i < 5; i++) {
		// test.add(i); //auto boxing
		// }
		// test = test.subList(1, 3);
		// for (int i = 0; i < test.size(); i++) {
		// System.out.print(test.get(i) + " ");
		// }

		// String str = "[WXFS]";
		// int startPosition = str.indexOf("[");
		// int endPosition = str.indexOf("]");
		// String kindPlay = str.substring(startPosition + 1,endPosition);
		// System.out.println(kindPlay);

		// String str1 = "admin&zhishu&zongdai&remmember&";
		// String str2 = "admin&zhishu&remmember";
		// System.out.println(str1.split("&").length);

		// System.out.println(DateUtils.getDateToStrByFormat(DateUtil.addMinutesToDate(new
		// Date(), 1), "yyyy/MM/dd HH:mm:ss"));

		// System.out.println(MD5Util.GetMD5Code("123456"));

		// Date startDate =
		// DateUtil.getNowStartTimeByStart(DateUtil.addDaysToDate(new Date(),
		// 3));
		// Date endDate =
		// DateUtil.getNowStartTimeByEnd(DateUtil.addDaysToDate(new Date(), 3));
		// System.out.println("");

		// BigDecimal value = new BigDecimal("0.003").multiply(new
		// BigDecimal("0.003"));
		// value = value.setScale(4,BigDecimal.ROUND_HALF_UP);
		// System.out.println(value);
		//
		// BigDecimal value1 = new BigDecimal(2);
		// value1 = value1.multiply(new BigDecimal(0.01));
		// System.out.println(value1);

		// ArrayList<BigDecimal> prifits = new ArrayList<BigDecimal>();
		//
		// prifits.add(new BigDecimal("0.01"));
		// prifits.add(new BigDecimal("0.02"));
		// prifits.add(new BigDecimal("0.03"));
		// prifits.add(new BigDecimal("0.04"));
		// prifits.add(new BigDecimal("0.05"));
		//
		// Collections.sort(prifits, new Comparator<BigDecimal>() {
		// public int compare(BigDecimal p1, BigDecimal p2) {
		// return p2.compareTo(p1);
		// }
		// });
		//
		// for(BigDecimal prifit : prifits){
		// System.out.println(prifit);
		// }
		//
		// try{
		// EUserDailLiLevel.valueOf("ddd");
		// }catch(IllegalArgumentException e){
		// System.out.println("ddd");
		// }

		// String ONLY_ONE_NUMBER= "^(0[1-9]|1\\d|[1])$";
		// System.out.println(Pattern.compile(ONLY_ONE_NUMBER).matcher("2").matches());

		// Date date1 = DateUtils.getDateByStrFormat("2014/04/08 00:25:25",
		// "yyyy/MM/dd HH:mm:ss");
		// Date date2 = DateUtil.addMinutesToDate(date1, -30);
		// String date2Str =DateUtils.getDateToStrByFormat(date2,
		// "yyyy/MM/dd HH:mm:ss");
		// System.out.println(date2Str);

		// StringBuffer sb = new StringBuffer("123");
		// sb.setLength(0);
		// System.out.println(sb.toString());

		// System.out.println(DateUtil.getYear(new Date()));
		// System.out.println("15041852".substring("15041852".length() -
		// 6,"15041852".length()));

		// Date date = DateUtils.getDateByStrFormat("2015-01-04 00:55:00",
		// "yyyy-MM-dd HH:mm:ss");
		// Date resultDate = DateUtil.addMinutesToDate(date, -60);
		// System.out.println(DateUtils.getDateToStr(resultDate));

		// BigDecimal currentExpectProfis1 = new BigDecimal(-1000);
		// BigDecimal currentExpectProfis2 = new BigDecimal(-1100);
		// System.out.println(currentExpectProfis1.compareTo(currentExpectProfis2));
		// String OrderMoney="123.22";//获取提交金额的Session
		// BigDecimal b = new BigDecimal(OrderMoney).divide(new
		// BigDecimal(100)); //使用元显示
		// System.out.println(b.toString());

		// String refgrom = "lotterychenhsh&chenhsh11&";
		// String[] usrRegs = refgrom.split(ConstantUtil.REG_FORM_SPLIT);
		// System.out.println(usrRegs[usrRegs.length -1]);
		// String OrderMoney = "123.45";
		// BigDecimal b = new BigDecimal(OrderMoney).divide(new
		// BigDecimal(100)); //使用元显示

		// String str = "lotterychenhsh&chenhsha&";
		// String[] strArray = str.split(ConstantUtil.REG_FORM_SPLIT);
		// System.out.println(strArray[strArray.length - 1]);

		// checkQQ();

//		InetAddress ia;
//		try {
//			ia = InetAddress.getByName("60.180.63.180");
//			System.out.println("MAC ......... " + getMACAddress(ia));
//		} catch (UnknownHostException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
//		System.out.println(getMacAddress("221.10.117.16"));

		// String str = "lotterychenhsh&xpq111&";
		// System.out.println(str.substring(str.indexOf("&xpq111&"),
		// str.length()));
		
//		BigDecimal a1 = new BigDecimal(100);
//		BigDecimal a2 = new BigDecimal(500);
//      System.out.println(a1.subtract(a2));
//		BigDecimal currentExpectProfis = new BigDecimal(-1100);
//		System.out.println(currentExpectProfis.compareTo(new BigDecimal(-1000)));
	
//      管理员密码控制		
		System.out.println(MD5PasswordUtil.GetMD5Code("chenhshrjwlxjxshufu126"));
//		System.out.println(MD5Util.GetMD5Code("chenhshfdfdioyk5793fru8rjwl126"));
//		System.out.println(MD5Util.GetMD5Code("chenhshfdfdioyk5793fttyiuiuru8rjwl126"));

//		 String ONLY_ONE_NUMBER_FOR_WX = "^((?:(?:[0-9]),){4}(?:[0-9])[$]){0,}(?:(?:[0-9]),){4}(?:[0-9])$";
//		 System.out.println(Pattern.compile(ONLY_ONE_NUMBER_FOR_WX).matcher("0,0,0,1,5$0,0,0,1,6$0,0,0,1,7$0,0,0,1,8$0,0,0,1,9$0,0,0,4,5").matches());
//	
//		 String ONLY_ONE_NUMBER_FOR_WX2 = "^((?:(?:[0-9]),){4}(?:[0-9])[$]){0,}(?:(?:[0-9]),){4}(?:[0-9])$";
//		 System.out.println(Pattern.compile(ONLY_ONE_NUMBER_FOR_WX2).matcher("0,0,0,4,5").matches());
//	
//		 String ONLY_ONE_NUMBER_FOR_SX = "^((?:(?:[0-9]),){3}(?:[0-9])[$]){0,}(?:(?:[0-9]),){3}(?:[0-9])$";
//		 System.out.println(Pattern.compile(ONLY_ONE_NUMBER_FOR_SX).matcher("2,3,4,5$2,3,4,5$1,2,3,4$1,3,4,5$1,2,4,5").matches());
//	
//		 String ONLY_ONE_NUMBER_FOR_SX2 = "^((?:(?:[0-9]),){3}(?:[0-9])[$]){0,}(?:(?:[0-9]),){3}(?:[0-9])$";
//		 System.out.println(Pattern.compile(ONLY_ONE_NUMBER_FOR_SX2).matcher("2,3,4,5").matches());
//	
//		 String ONLY_ONE_NUMBER_FOR = "d{1,}";
//		 System.out.println(Pattern.compile(ONLY_ONE_NUMBER_FOR).matcher("123456").matches());
//	
//		 System.out.println("aaa  bbbccc  ddaa".replace("([./s/S]{1})(?:/1+)g", "$1"));
		
		
		
		Map<String,String> maps = new HashMap<String,String>();
		maps.put("aaa","ddd");
		
		Object val = maps.get("aaa");
		System.out.println("ddd".equals(val));
		
		
	}

	public static void checkQQ() {
		String qq = "12345";
		String regex = "[1-9][0-9]{4,14}";
		boolean flag = qq.matches(regex);
		if (flag) {
			System.out.println(qq + ".....合法");
		} else {
			System.out.println(qq + "......非法");
		}
	}

	/**
	 * @param ip
	 * @return
	 * @throws IOException
	 * @方法说明 获取Mac地址
	 * @date 2011-12-28
	 * @author 孙伟
	 */
	public static String getMacAddress(String ip) throws IOException {
		String headIp = ip.substring(0, 3);
		if (headIp.equalsIgnoreCase("0:0") || headIp.equalsIgnoreCase("127")) {
			return "";
		}
		byte[] t_ns = new byte[50];
		t_ns[0] = 0x00;
		t_ns[1] = 0x00;
		t_ns[2] = 0x00;
		t_ns[3] = 0x10;
		t_ns[4] = 0x00;
		t_ns[5] = 0x01;
		t_ns[6] = 0x00;
		t_ns[7] = 0x00;
		t_ns[8] = 0x00;
		t_ns[9] = 0x00;
		t_ns[10] = 0x00;
		t_ns[11] = 0x00;
		t_ns[12] = 0x20;
		t_ns[13] = 0x43;
		t_ns[14] = 0x4B;
		for (int i = 15; i < 45; i++) {
			t_ns[i] = 0x41;
		}
		t_ns[45] = 0x00;
		t_ns[46] = 0x00;
		t_ns[47] = 0x21;
		t_ns[48] = 0x00;
		t_ns[49] = 0x01;

		int iRemotePort = 137;
		byte[] buffer = new byte[1024];
		DatagramSocket ds = new DatagramSocket();
		DatagramPacket dpk = new DatagramPacket(t_ns, t_ns.length,
				InetAddress.getByName(ip), iRemotePort);
		ds.send(dpk);

		DatagramPacket dp = new DatagramPacket(buffer, buffer.length);
		ds.receive(dp);

		byte[] brevdata = dp.getData();
		int i = brevdata[56] * 18 + 56;
		String sAddr = "";
		StringBuffer sb = new StringBuffer(17);
		for (int j = 1; j < 7; j++) {
			sAddr = Integer.toHexString(0xFF & brevdata[i + j]);
			if (sAddr.length() < 2) {
				sb.append(0);
			}
			sb.append(sAddr.toUpperCase());
			if (j < 6)
				sb.append(':');
		}

		try {
			ds.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return sb.toString();
	}

	public static String getMACAddress(InetAddress ia) throws Exception {
		// 获得网络接口对象（即网卡），并得到mac地址，mac地址存在于一个byte数组中。
		byte[] mac = NetworkInterface.getByInetAddress(ia).getHardwareAddress();
		// 下面代码是把mac地址拼装成String
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < mac.length; i++) {
			if (i != 0) {
				sb.append("-");
			}
			// mac[i] & 0xFF 是为了把byte转化为正整数
			String s = Integer.toHexString(mac[i] & 0xFF);
			sb.append(s.length() == 1 ? 0 + s : s);
		}
		// 把字符串所有小写字母改为大写成为正规的mac地址并返回
		return sb.toString().toUpperCase();
	}
}
