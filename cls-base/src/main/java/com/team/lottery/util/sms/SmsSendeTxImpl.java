package com.team.lottery.util.sms;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.util.JaxbUtil;

public class SmsSendeTxImpl {
	
	private static Logger logger = LoggerFactory.getLogger(SmsSendeTxImpl.class);
    /**
     * 
     * @param api_url
     * @param userName
     * @param password
     * @param content
     * @param phones
     * @return 返回不为0 的taskid
     */
	public static String sendSms(String api_url,String userName, String password, String content, String phones) {

		try {
		//	String params = "uid="+ userName +"&password="+ password +"&to="+ phones +"&content=" + URLEncoder.encode(content, "gb2312") + "&time=";
			String params = "action=send&userid=122&account="+userName+"&password="+password+"&mobile="+phones+"&content="+URLEncoder.encode(content, "UTF-8")+"&sendTime=&extno=122";
			//System.out.println(params);
			SmsObject smsObject= post(api_url, params);
			if("Success".equals(smsObject.getReturnstatus())){
				return smsObject.getTaskID();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return "0";
	}
	
	public static SmsObject post(String path, String params) throws Exception {
		SmsObject smsObject = null;
		BufferedReader in = null;
		PrintWriter out = null;
		HttpURLConnection httpConn = null;
		try {
			URL url = new URL(path);
			httpConn = (HttpURLConnection) url.openConnection();
			httpConn.setRequestMethod("POST");
			httpConn.setDoInput(true);
			httpConn.setDoOutput(true);

			out = new PrintWriter(httpConn.getOutputStream());
			out.println(params);
			out.flush();

			if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
				StringBuffer content = new StringBuffer();
				String tempStr = "";
				in = new BufferedReader(new InputStreamReader(
						httpConn.getInputStream()));
				while ((tempStr = in.readLine()) != null) {
					content.append(tempStr);
				}
				 smsObject = JaxbUtil.transformToObject(SmsObject.class, content.toString());
				return smsObject;
			} else {
				throw new Exception("请求出现了问题!");
			}
		} catch (IOException e) {
			logger.error("调用发送短信接口出现IO错误", e);
		} finally {
			in.close();
			out.close();
			httpConn.disconnect();
		}
		return null;
	}

	public static void main(String[] args) throws Exception {
//		String xml = "<?xml version='1.0' encoding='utf-8' ?><returnsms> <returnstatus>Success</returnstatus> <message>ok</message> <remainpoint>88</remainpoint> <taskID>14110</taskID> <successCounts>1</successCounts></returnsms>";
//		SmsObject smsObject = JaxbUtil.transformToObject(SmsObject.class, xml);
//		System.out.println(smsObject.getReturnstatus());
		//SmsSendeTxImpl smsSenderDwyImpl = new SmsSendeTxImpl();
		//smsSenderDwyImpl.sendSms("http://123.207.46.192:8088/sms.aspx", "joney", "tx123456", "【起点科技】此次验证码为：2383", "18750596079");
		//System.out.println(resMessage);
	}
}
