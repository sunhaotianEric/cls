package com.team.lottery.util;

import java.util.Arrays;

public class HexStringUtil {

	/**
	 * 数组转成十六进制字符串
	 * 
	 * @param byte[]
	 * @return HexString
	 */
	public static String toHexString(byte[] b) {
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < b.length; ++i) {
			buffer.append(toHexString(b[i]));
		}
		return buffer.toString();
	}

	public static String toHexString(byte b) {
		String s = Integer.toHexString(b & 0xFF);
		if (s.length() == 1) {
			return "0" + s;
		} else {
			return s;
		}
	}

	/**
	 * 把16进制字符串转换成字节数组
	 * 
	 * @param hexString
	 * @return byte[]
	 */
	public static byte[] hexStringToByte(String hex) {
		int len = (hex.length() / 2);
		byte[] result = new byte[len];
		char[] achar = hex.toCharArray();
		for (int i = 0; i < len; i++) {
			int pos = i * 2;
			result[i] = (byte) (toByte(achar[pos]) << 4 | toByte(achar[pos + 1]));
		}
		return result;
	}

	private static int toByte(char c) {
		byte b = (byte) "0123456789ABCDEF".indexOf(c);
		return b;
	}
	
	public static void main(String[] args) {
		String tempStr = "6966842a7258b667f844df6a0460f8e9";
		byte[] tempByte = hexStringToByte(tempStr);
		String res = Arrays.toString(tempByte);
		System.out.println("字节数组：" + res);
		String res2 = toHexString(tempByte);
		System.out.println("源十六进制:" + res2);
	}
}
