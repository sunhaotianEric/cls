package com.team.lottery.util;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class OrderDetailUtil {
	
	private static Logger log = LoggerFactory.getLogger(OrderDetailUtil.class);
	
	/**
	 * 五球/龙虎/梭哈
	 * @return
	 */
	public static Map<String, String> zHWQLHSHMap(){
		Map<String, String> zHWQLHSHMap = new HashMap<String, String>();
		zHWQLHSHMap.put("1", "大");
		zHWQLHSHMap.put("2", "小");
		zHWQLHSHMap.put("3", "单");
		zHWQLHSHMap.put("4", "双");
		zHWQLHSHMap.put("5", "五条");
		zHWQLHSHMap.put("6", "四条");
		zHWQLHSHMap.put("7", "葫芦");
		zHWQLHSHMap.put("8", "顺子");
		zHWQLHSHMap.put("9", "三条");
		zHWQLHSHMap.put("10", "两对");
		zHWQLHSHMap.put("11", "一对");
		zHWQLHSHMap.put("12", "散号");
		zHWQLHSHMap.put("13", "龙");
		zHWQLHSHMap.put("14", "虎");
		zHWQLHSHMap.put("15", "和");
		return zHWQLHSHMap;
	}
	
	/**
	 * 前三总和、中三总和、后三总和
	 * @return
	 */
	public static Map<String, String> zHQSMap(){
		Map<String, String> zHQSMap = new HashMap<String, String>();
		zHQSMap.put("0", "0");
		zHQSMap.put("1", "1");
		zHQSMap.put("2", "2");
		zHQSMap.put("3", "3");
		zHQSMap.put("4", "4");
		zHQSMap.put("5", "5");
		zHQSMap.put("6", "6");
		zHQSMap.put("7", "7");
		zHQSMap.put("8", "8");
		zHQSMap.put("9", "9");
		zHQSMap.put("10", "10");
		zHQSMap.put("11", "11");
		zHQSMap.put("12", "12");
		zHQSMap.put("13", "13");
		zHQSMap.put("14", "14");
		zHQSMap.put("15", "15");
		zHQSMap.put("16", "16");
		zHQSMap.put("17", "17");
		zHQSMap.put("18", "18");
		zHQSMap.put("19", "19");
		zHQSMap.put("20", "20");
		zHQSMap.put("21", "21");
		zHQSMap.put("22", "22");
		zHQSMap.put("23", "23");
		zHQSMap.put("24", "24");
		zHQSMap.put("25", "25");
		zHQSMap.put("26", "26");
		zHQSMap.put("27", "27");
		zHQSMap.put("28", "小");
		zHQSMap.put("29", "大");
		zHQSMap.put("30", "单");
		zHQSMap.put("31", "双");
		zHQSMap.put("32", "0-3");
		zHQSMap.put("33", "4-7");
		zHQSMap.put("34", "8-11");
		zHQSMap.put("35", "12-15");
		zHQSMap.put("36", "16-19");
		zHQSMap.put("37", "20-23");
		zHQSMap.put("38", "24-27");
		zHQSMap.put("39", "豹子");
		zHQSMap.put("40", "顺子");
		zHQSMap.put("41", "对子");
		zHQSMap.put("42", "半顺");
		zHQSMap.put("43", "杂六");
		return zHQSMap;
	}
	
	/**
	 * 斗牛
	 * @return
	 */
	public static Map<String, String> zHDNMap(){
		Map<String, String> zHDNMap = new HashMap<String, String>();
		zHDNMap.put("1", "牛1");
		zHDNMap.put("2", "牛2");
		zHDNMap.put("3", "牛3");
		zHDNMap.put("4", "牛4");
		zHDNMap.put("5", "牛5");
		zHDNMap.put("6", "牛6");
		zHDNMap.put("7", "牛7");
		zHDNMap.put("8", "牛8");
		zHDNMap.put("9", "牛9");
		zHDNMap.put("10", "牛牛");
		zHDNMap.put("11", "牛大");
		zHDNMap.put("12", "牛小");
		zHDNMap.put("13", "牛单");
		zHDNMap.put("14", "牛双");
		zHDNMap.put("15", "无牛");
		return zHDNMap;
	}
	
	public static Map<String, String> getCodesDesByKindReplace(String kindPlay){
		Map<String, String> map = null;
		if (kindPlay.equals("ZHWQLHSH")) {
			map = OrderDetailUtil.zHWQLHSHMap();
		}else if (kindPlay.equals("ZHQS") || kindPlay.equals("ZHZS") || kindPlay.equals("ZHHS")) {
			map = OrderDetailUtil.zHQSMap();
		}else if (kindPlay.equals("ZHDN")) {
			map = OrderDetailUtil.zHDNMap();
		}
		return map;
	}
	
	/**
	 * 单式投注位数
	 */
	public static String getLotteryPosition(String lotteryType, String[] positionArray){
		String positionDes = "(";
		if(lotteryType.equals("FCSDDPC")){
			if(positionArray[0].equals("1")){
				positionDes += "百位  ";
			}
			if(positionArray[1].equals("1")){
				positionDes += "十位  ";
			}
			if(positionArray[2].equals("1")){
				positionDes += "个位  ";
			}
		}else{
			if(positionArray[0].equals("1")){
				positionDes += "万位  ";
			}
			if(positionArray[1].equals("1")){
				positionDes += "千位  ";
			}
			if(positionArray[2].equals("1")){
				positionDes += "百位  ";
			}
			if(positionArray[3].equals("1")){
				positionDes += "十位  ";
			}
			if(positionArray[4].equals("1")){
				positionDes += "个位  ";
			}
		}
		positionDes += ")";
		return positionDes;
	}
	
	/**
	 * 判断是否包含开奖号码
	 */
	public static boolean isContainOpenCode(String lotteryType, int[] openCodeArrays, String code, String key, int index){
		if(key.equals("前二大小单双复式")){
			boolean oneWeiBig = false;
			if(openCodeArrays[0] >= 5){
				oneWeiBig = true;
			}else{
				oneWeiBig = false;
			}
			boolean oneWeiDan = false;
			if(openCodeArrays[0] % 2 != 0){
				oneWeiDan = true;
			}else{
				oneWeiDan = false;
			}
			
			boolean twoWeiBig = false;
			if(openCodeArrays[1] >= 5){
				twoWeiBig = true;
			}else{
				twoWeiBig = false;
			}
			boolean twoWeiDan = false;
			if(openCodeArrays[1] % 2 != 0){
				twoWeiDan = true;
			}else{
				twoWeiDan = false;
			}
			
			if(index == 0){
				if(code.equals("大")){
	                if(oneWeiBig){
	                	return true;
	                }else{
	                	return false;
	                }
				}
				if(code.equals("小")){
	                if(oneWeiBig){
	                	return false;
	                }else{
	                	return true;
	                }
				}
				if(code.equals("单")){
	                if(oneWeiDan){
	                	return true;
	                }else{
	                	return false;
	                }
				}
				if(code.equals("双")){
	                if(oneWeiDan){
	                	return false;
	                }else{
	                	return true;
	                }
				}
			}else if(index == 1){
				if(code.equals("大")){
	                if(twoWeiBig){
	                	return true;
	                }else{
	                	return false;
	                }
				}
				if(code.equals("小")){
	                if(twoWeiBig){
	                	return false;
	                }else{
	                	return true;
	                }
				}
				if(code.equals("单")){
	                if(twoWeiDan){
	                	return true;
	                }else{
	                	return false;
	                }
				}
				if(code.equals("双")){
	                if(twoWeiDan){
	                	return false;
	                }else{
	                	return true;
	                }
				}
			}
		}else if(key.equals("后二大小单双复式")){
			boolean siWeiBig = false;
			int weiValueOne = 0; 
			int weiValueTwo = 0;
			
			if(lotteryType.equals("FCSDDPC")){
				weiValueOne = openCodeArrays[1];
				weiValueTwo = openCodeArrays[2];
			}else{
				weiValueOne = openCodeArrays[3];
				weiValueTwo = openCodeArrays[4];
			}
			
			if(weiValueOne >= 5){
				siWeiBig = true;
			}else{
				siWeiBig = false;
			}
			boolean siWeiDan = false;
			if(weiValueOne % 2 != 0){
				siWeiDan = true;
			}else{
				siWeiDan = false;
			}
			
			boolean wuWeiBig = false;
			if(weiValueTwo >= 5){
				wuWeiBig = true;
			}else{
				wuWeiBig = false;
			}
			boolean wuWeiDan = false;
			if(weiValueTwo % 2 != 0){
				wuWeiDan = true;
			}else{
				wuWeiDan = false;
			}
			
			if(index == 0){
				if(code.equals("大")){
	                if(siWeiBig){
	                	return true;
	                }else{
	                	return false;
	                }
				}
				if(code.equals("小")){
	                if(siWeiBig){
	                	return false;
	                }else{
	                	return true;
	                }
				}
				if(code.equals("单")){
	                if(siWeiDan){
	                	return true;
	                }else{
	                	return false;
	                }
				}
				if(code.equals("双")){
	                if(siWeiDan){
	                	return false;
	                }else{
	                	return true;
	                }
				}
			}else if(index == 1){
				if(code.equals("大")){
	                if(wuWeiBig){
	                	return true;
	                }else{
	                	return false;
	                }
				}
				if(code.equals("小")){
	                if(wuWeiBig){
	                	return false;
	                }else{
	                	return true;
	                }
				}
				if(code.equals("单")){
	                if(wuWeiDan){
	                	return true;
	                }else{
	                	return false;
	                }
				}
				if(code.equals("双")){
	                if(wuWeiDan){
	                	return false;
	                }else{
	                	return true;
	                }
				}
			}
		}else if(key.equals("前三直选和值") || key.equals("前三组选和值")){
			int qsHz = openCodeArrays[0] + openCodeArrays[1] + openCodeArrays[2];
			if(qsHz == Integer.valueOf(code).intValue()){
				return true;
			}else{
				return false;
			}
		}else if(key.equals("后三直选和值") || key.equals("后三组选和值")){
			int hsHz = openCodeArrays[2] + openCodeArrays[3] + openCodeArrays[4];
			if(hsHz == Integer.valueOf(code).intValue()){
				return true;
			}else{
				return false;
			}
		}else if(key.equals("前二直选和值") || key.equals("前二组选和值")){
			int qeHz = openCodeArrays[0] + openCodeArrays[1];
			if(qeHz == Integer.valueOf(code).intValue()){
				return true;
			}else{
				return false;
			}
		}else if(key.equals("后二直选和值") || key.equals("后二组选和值")){
			int heHz = openCodeArrays[3] + openCodeArrays[4];
			if(heHz == Integer.valueOf(code).intValue()){
				return true;
			}else{
				return false;
			}
		}else if(key.equals("趣味定单双")){
			int openCode1 = openCodeArrays[0];
			int openCode2 = openCodeArrays[1];
			int openCode3 = openCodeArrays[2];
			int openCode4 = openCodeArrays[3];
			int openCode5 = openCodeArrays[4];
			
			int unevenCount = 0; //奇数个数
			int evenCount = 0;  //偶数个数
			//计算开奖号码单双个数
	        if(openCode1 % 2 == 0){
	        	evenCount++;
	        }else{
	        	unevenCount++;
	        }
	        if(openCode2 % 2 == 0){
	        	evenCount++;
	        }else{
	        	unevenCount++;
	        }
	        if(openCode3 % 2 == 0){
	        	evenCount++;
	        }else{
	        	unevenCount++;
	        }
	        if(openCode4 % 2 == 0){
	        	evenCount++;
	        }else{
	        	unevenCount++;
	        }		
	        if(openCode5 % 2 == 0){
	        	evenCount++;
	        }else{
	        	unevenCount++;
	        }			
	        boolean isAward = false;
	        String everyPositionCode = code;
			if(everyPositionCode.equals("5单0双")){
				if(unevenCount == 5 && evenCount == 0){  //5个单0个双
					isAward = true;
				}
			}else if(everyPositionCode.equals("4单1双")){
				if(unevenCount == 4 && evenCount == 1){  //4个单1个双
					isAward = true;
				}
			}else if(everyPositionCode.equals("3单2双")){
				if(unevenCount == 3 && evenCount == 2){  //3个单2个双
					isAward = true;
				}
			}else if(everyPositionCode.equals("2单3双")){
				if(unevenCount == 2 && evenCount == 3){  //2个单3个双
					isAward = true;
				}
			}else if(everyPositionCode.equals("1单4双")){
				if(unevenCount == 1 && evenCount == 4){  //1个单4个双
					isAward = true;
				}
			}else if(everyPositionCode.equals("0单5双")){
				if(unevenCount == 0 && evenCount == 5){  //0个单5个双
					isAward = true;
				}
			}
			return isAward;
		}else{
			for(int i = 0; i < openCodeArrays.length; i++){
				try {
					//在投注的时候，某个位置上的号码可能是多个选择如"0|2|4|6|8"等，需要处理
					if (code.contains("|")) {
						String[] codes = code.split("\\|");
						for (int j = 0; j < codes.length; j++) {
							if(openCodeArrays[i] == Integer.valueOf(codes[j]).intValue()){//一些彩种的code会是汉字：XYEB
								return true;
							}
						}
					}else if(openCodeArrays[i] == Integer.valueOf(code).intValue()){//一些彩种的code会是汉字：XYEB
						return true;
					}
				} catch (NumberFormatException e) {
					log.debug("解析code[{}]非数字,", code);
					return false;
				}
			}
		}
		return false;
	}

}
