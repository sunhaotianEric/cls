package com.team.lottery.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.team.lottery.system.SystemConfigConstant;


/**
 * @version $Revision$ $Date$
 */
public class DateUtil {
    private static final Log log = LogFactory.getLog(DateUtil.class);

    public static Date getDate(int year, int month, int day) {
        Calendar cal = Calendar.getInstance(); 
        cal.set(year, month - 1, day, 0, 0, 0);
        return cal.getTime();
    }

    /**
     * Get a new date time from a existed date and time
     *
     * @param date
     * @param time
     * @return
     */
    public static Date getDateTime(Date date, Date time) {
        if (null == date && null == time) {
            return null;
        }
        if (null == date) {
            date = DateUtil.getSystemDate();
        }
        if (null != date && null == time) {
            time = DateUtil.getSystemTimestamp();
        }
        Calendar cal = Calendar.getInstance();
        int year = Integer.parseInt(DateUtil.parseDate(date, "yyyy"));
        int month = Integer.parseInt(DateUtil.parseDate(date, "M"));
        int day = Integer.parseInt(DateUtil.parseDate(date, "dd"));
        int hour = Integer.parseInt(DateUtil.parseDate(time, "h"));
        int minute = Integer.parseInt(DateUtil.parseDate(time, "m"));
        cal.set(year, month, day, hour, minute);
        return cal.getTime();
    }

    public static boolean isDateEqual(Date date1, Date date2) {
        return !((date1 == null) || (date2 == null)) && resetTime(date1).compareTo(resetTime(date2)) == 0;
    }

    public static boolean isEndOfTheMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        return cal.get(Calendar.DATE) == maxDay;
    }

    public static boolean isEndOfTheYear(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return (cal.get(Calendar.MONTH) == 11) && (cal.get(Calendar.DATE) == 31);
    }

    public static int getLastDayOfTheMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    public static Date getNextWorkingDay() {
        Date nextWorkingDay = DateUtil.addDaysToDate(DateUtil.getSystemDate(), 1);
        Calendar c = Calendar.getInstance();
        c.setTime(nextWorkingDay);
        int day = c.get(Calendar.DAY_OF_WEEK);
        if (day == Calendar.SUNDAY) {
            nextWorkingDay = DateUtil.addDaysToDate(nextWorkingDay, 1);
        }
        return nextWorkingDay;
    }

    public static boolean isStartBeforeEndDate(Date startDate, Date endDate) {
        return !((startDate == null) || (endDate == null)) && resetTime(startDate).compareTo(resetTime(endDate)) < 0;
    }

    public static boolean isStartBeforeEndTime(Date startTime, Date endTime) {
        if ((startTime == null) || (endTime == null)) {
            return true;
        }
        return startTime.getTime() <= endTime.getTime();
    }

    public static boolean isStartOfTheMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.DATE) == 1;
    }

    public static int getMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.MONTH);
    }

    public static int getYear(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.YEAR);
    }
    public static int getHour(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.HOUR);
    }
    /**
     * 获取24小时制的时间
     * @param date
     * @return
     */
    public static int getHourOfDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.HOUR_OF_DAY);
    }
    public static int getMis(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.MINUTE);
    }

    public static int getSec(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.SECOND);
    }

    public static long getTimeByHourMisSec(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        long time = 0;;
        time = time + cal.get(Calendar.HOUR_OF_DAY) * 60 * 60 * 1000;
        time = time + cal.get(Calendar.MINUTE) * 60 * 1000;
        time = time + cal.get(Calendar.SECOND) * 1000;
        return time;
    }
    
    public static boolean isStartOfTheYear(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return (cal.get(Calendar.MONTH) == 1) && (cal.get(Calendar.DATE) == 1);
    }

    public static java.sql.Date getSystemDate() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return new java.sql.Date(cal.getTime().getTime());
    }

    public static Timestamp getSystemTimestamp() {
        return new Timestamp(System.currentTimeMillis());
    }

    public static boolean isValidYearFormat(String s) {
        if (s == null) {
            return false;
        } else if (s.trim().length() == 4) {
            return true;
        }
        return false;
    }

    public static String getDate(Date date, String strFormat) {
        return DateUtil.parseDate(date, strFormat);
    }

    public static Date addDate(int type, Date date, int num) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(type, num);
        return new Date(cal.getTime().getTime());
    }

    public static Date addDaysToDate(Date date, int numDays) {
        if (date == null) {
            return null;
        }
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, numDays);
        return c.getTime();
    }

    public static Date addHoursToDate(Date date, int numHours) {
        if (date == null) {
            return null;
        }
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.HOUR_OF_DAY, numHours);

        return c.getTime();
    }

    public static Date addMinutesToDate(Date date, int numMins) {
        if (date == null) {
            return null;
        }
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MINUTE, numMins);
        return c.getTime();
    }

    public static Date addSecondsToDate(Date date, int numSecs) {
        if (date == null) {
            return null;
        }
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.SECOND, numSecs);
        return c.getTime();
    }

    public static Date addMonthsToDate(Date date, int numMonths) {
        if (date == null) {
            return null;
        }
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MONTH, numMonths);
        return c.getTime();
    }

    public static Date addYearsToDate(Date date, int numYears) {
        if (date == null) {
            return null;
        }
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.YEAR, numYears);
        return c.getTime();
    }

    public static String parseDate(Date date, String formatStr) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(formatStr);
        if (date == null) {
            return null;
        } else {
            return dateFormat.format(date);
        }
    }

    public static Date resetTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Date toDate(String strDateTime, String dateTimeFormat) {
        if ((strDateTime == null) || (strDateTime.length() == 0)
                || (dateTimeFormat == null) || (dateTimeFormat.length() == 0)) {
            return null;
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat(dateTimeFormat);
        Date date = dateFormat.parse(strDateTime, new ParsePosition(0));
        if (date == null) {
            return null;
        }
        String dateStr = parseDate(date, dateTimeFormat);
        if (!strDateTime.equals(dateStr)) {
            return null;
        }
        return date;
    }

    public static Timestamp toTimestamp(String dateTimeStr, String dateTimeFormat) {
        return toTimestamp(toDate(dateTimeStr, dateTimeFormat));
    }

    public static Timestamp toTimestamp(Date date) {
        if (date == null) {
            return null;
        }
        return new Timestamp(date.getTime());
    }

    public static Date toDate(Timestamp timeStamp) {
        if (timeStamp == null) {
            return null;
        }

        return new Date(timeStamp.getTime());
    }

    public static String toTwoDigits(String number) {
        return StringUtils.leftPad(number, 2, "0");
    }

    public static Date firstDayOfWeek(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.setFirstDayOfWeek(0);
        int year = DateUtil.getYear(date);
        int month = DateUtil.getMonth(date) + 1;
        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK) - 2;
        int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
        if (dayOfWeek < 0) {
            dayOfWeek += 7;
        }
        return DateUtil.getDate(year, month, dayOfMonth - dayOfWeek);
    }

    public static Date lastDayOfWeek(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.setFirstDayOfWeek(0);
        int year = DateUtil.getYear(date);
        int month = DateUtil.getMonth(date) + 1;
        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK) - 2;
        int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
        return DateUtil.getDate(year, month, dayOfMonth - dayOfWeek + 6);
    }

    public static int getFirstDayOfWeekInMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK) - 2;
        int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
        return dayOfMonth - dayOfWeek;
    }


   


    public static int getDateDiffSec(Date startDate, Date endDate) {
        int sec = 0;
        Calendar ca1 = Calendar.getInstance();
        Calendar ca2 = Calendar.getInstance();
        ca1.setTime(endDate);
        ca2.setTime(startDate);
        double timeLong = ca1.getTimeInMillis() - ca2.getTimeInMillis();
        sec = (int) timeLong / 1000;
        return sec;

    }


    public static Date getDateAddInt(Date inDate, int AddDateInt) {
        Calendar theCalendar = new GregorianCalendar();
        Date returnDate = new Date();

        if (inDate != null) {
            String DateStr = parseDate(inDate, "yyyy-MM-dd HH:mm:ss");
            theCalendar.set(Integer.parseInt(DateStr.substring(0, 4)), Integer
                    .parseInt(DateStr.substring(5, 7)) - 1, Integer
                    .parseInt(DateStr.substring(8, 10))
                    + AddDateInt, Integer.parseInt(DateStr.substring(11, 13)),
                    Integer.parseInt(DateStr.substring(14, 16)), Integer
                            .parseInt(DateStr.substring(17, 19)));
            returnDate = new Date();
            returnDate = theCalendar.getTime();
        }

        return returnDate;
    }

    public static Date toTime(String dateStr,String formatStr){
        Date startTime = null;
        if(dateStr==null && formatStr==null) return null;
        try{
            java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("HH:mm:ss");
            startTime = sdf.parse(dateStr);
        }catch(Exception e){
            e.printStackTrace();
        }
        return  startTime;
    }

    
    public static boolean isBetweenTimes(String startTimeStr,String endTimeStr,String timeStr,String formatStr){
        boolean flag = false;
        Date startTime = null;
        Date endTime = null;
        Date time = null;
        try{
            java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(formatStr);
            startTime = sdf.parse(startTimeStr);
            endTime = sdf.parse(endTimeStr);
            time = sdf.parse(timeStr);
            if(startTime.before(time) && endTime.after(time)){
                flag = true;
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return flag;
    }


    //同当前时间比较时间差
    public static boolean timeDiff(String timestamp, Integer diffMinute) {
        boolean flag = false;
        Date date = DateUtil.toDate(timestamp, "yyyyMMddHHmmss");
        if (date != null) {
            java.util.Date now = new Date();
            long l = now.getTime() - date.getTime();
            long day = l / (24 * 60 * 60 * 1000);
            long hour = (l / (60 * 60 * 1000) - day * 24);
            long min = ((l / (60 * 1000)) - day * 24 * 60 - hour * 60);
            long s = (l / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
            if (day != 0 || hour != 0 || min < 0 || min > diffMinute)
                flag = true;
        } else {
            flag = true;
        }
        return flag;
    }
    
    /**
     * 获取当前开始时间
     * @return
     */
    public static Date getNowStartTime(){  
    	Calendar currentDate = new GregorianCalendar();   
    	currentDate.set(Calendar.HOUR_OF_DAY, 0);  
    	currentDate.set(Calendar.MINUTE, 0);  
    	currentDate.set(Calendar.SECOND, 0);  
        return currentDate.getTime();  
    }  
    
    /**
     * 获取当前的开始时间 00:00:00
     * @return
     */
    public static Date getNowStartTimeByStart(Date startDate){  
    	Calendar currentDate = new GregorianCalendar();   
    	currentDate.setTime(startDate);
    	currentDate.set(Calendar.HOUR_OF_DAY, 0);  
    	currentDate.set(Calendar.MINUTE, 0);  
    	currentDate.set(Calendar.SECOND, 0);  
    	currentDate.set(Calendar.MILLISECOND, 0);  
        return currentDate.getTime();  
    }  
    
    /**
     * 获取当前的结束时间 23:59:59
     * @return
     */
    public static Date getNowStartTimeByEnd(Date endDate){
    	Calendar currentDate = new GregorianCalendar();   
    	currentDate.setTime(endDate);
    	currentDate.set(Calendar.HOUR_OF_DAY, 23);  
    	currentDate.set(Calendar.MINUTE, 59);  
    	currentDate.set(Calendar.SECOND, 59);  
    	currentDate.set(Calendar.MILLISECOND, 999);
        return currentDate.getTime();  
    }

    /**
     * 取当月的最后天日期
     * @param currDate
     * @return
     */
    public static Date getLastDayOfMonth(Date currDate)   {
        Calendar cDay1 = Calendar.getInstance();
        cDay1.setTime(currDate);
        final int lastDay = cDay1.getActualMaximum(Calendar.DAY_OF_MONTH);
        Date lastDate = cDay1.getTime();
        lastDate.setDate(lastDay);
        return lastDate;
    }

    /**
     * 取当月的第一天日期
     * @param currDate
     * @return
     */
    public static Date getFirstDayOfMonth(Date currDate)   {
        Calendar cDay1 = Calendar.getInstance();
        cDay1.setTime(currDate);
        final int lastDay = cDay1.getActualMinimum(Calendar.DAY_OF_MONTH);
        Date lastDate = cDay1.getTime();
        lastDate.setDate(lastDay);
        return lastDate;
    }

    /**
     * 获取当前日期是星期几
     *
     * @param dt
     * @return 当前日期是星期几
     */
    public static String getWeekOfDate(Date dt) {
        String[] weekDays = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0)
            w = 0;
        return weekDays[w];
    }
    
    /**  
     * 计算两个日期之间相差的天数  
     * @param smdate 较小的时间 
     * @param bdate  较大的时间 
     * @return 相差天数 
     * @throws ParseException  
     */    
    public static int daysBetween(Date smdate,Date bdate) throws ParseException {    
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");  
        smdate=sdf.parse(sdf.format(smdate));  
        bdate=sdf.parse(sdf.format(bdate));  
        Calendar cal = Calendar.getInstance();    
        cal.setTime(smdate);    
        long time1 = cal.getTimeInMillis();                 
        cal.setTime(bdate);    
        long time2 = cal.getTimeInMillis();         
        long between_days=(time2-time1)/(1000*3600*24);  
        return Integer.parseInt(String.valueOf(between_days));           
    } 

    /**
     * 判断是否为闰年
     * @param year
     * @return
     */
    public static boolean isRunYear(int year){
    	if(year%4 == 0 && year%100!=0 || year%400 == 0){//是闰年
    		return true;
	    }else{
	    	return false;
	    }
    }
    
    /**
     * 判断是否在春节期间
     * @param date
     * @return
     */
    public static boolean isInSpringFestival(Date date) {
    	//春节开始时间
    	Date springStartDate = DateUtil.getNowStartTimeByStart(DateUtils.getDateByStrFormat(SystemConfigConstant.springFestivalStartDate, "yyyy-MM-dd"));
    	//春节结束时间
    	Date springEndDate = DateUtil.getNowStartTimeByEnd(DateUtils.getDateByStrFormat(SystemConfigConstant.springFestivalEndDate, "yyyy-MM-dd"));
    	//春节期间
    	if(springStartDate.getTime() <= date.getTime() && date.getTime() <= springEndDate.getTime()){
    		return true;
    	} else {
    		return false;
    	}
    }
    
    /**
     * 判断是否在春节之后时间
     * @param date
     * @return
     */
    public static boolean isAfterSpringFestival(Date date) {
    	//春节结束时间
    	Date springEndDate = DateUtil.getNowStartTimeByEnd(DateUtils.getDateByStrFormat(SystemConfigConstant.springFestivalEndDate, "yyyy-MM-dd"));
    	//春节期间
    	if(date.getTime() > springEndDate.getTime()){
    		return true;
    	} else {
    		return false;
    	}
    }
    
    public static void main(String[] args) throws Exception{
//        String timerDate = "2012-11-20 15:00:00";
//        Date dt = DateUtil.toDate(timerDate, "yyyy-MM-dd HH:mm:ss");
//        String sendTime = DateUtil.parseDate(dt, "yyyyMMddHHmmss");
//        System.out.println ("dt1 = " + sendTime);
    	
    	System.out.println(daysBetween(DateUtils.getDateByStrFormat(DateUtil.getYear(new Date()) + "/01/01","yyyy/MM/dd"),new Date()));
    	
    }
    
    @SuppressWarnings("deprecation")
	public  static String getTimeState(){
    	Date nowTime = new Date();
    	int hours = nowTime.getHours();
    	String stateOfTime = "";
    	
    	if(5 < hours && hours < 12){
    		stateOfTime ="上午好！";
    	}else if(12 <=  hours && hours <= 14){
    		stateOfTime ="中午好！";
    	}else if(14 <  hours && hours <= 18){
    		stateOfTime ="下午好！";
    	}else if((18 <  hours && hours <= 23) || (0 <= hours && hours <= 5)){
    		stateOfTime ="晚上好！";
    	}
    	return stateOfTime;
    }
    
    //计算2个日期之间相差天数，小时，分钟
    public static String getDatePoor(Date startDate, Date endDate) {
    	 
        long nd = 1000 * 24 * 60 * 60;
        long nh = 1000 * 60 * 60;
        long nm = 1000 * 60;
        // long ns = 1000;
        // 获得两个时间的毫秒时间差异
        long diff = endDate.getTime() - startDate.getTime();
        // 计算差多少天
        long day = diff / nd;
        // 计算差多少小时
        long hour = diff % nd / nh;
        // 计算差多少分钟
        long min = diff % nd % nh / nm;
        // 计算差多少秒//输出结果
        // long sec = diff % nd % nh % nm / ns;
        return day + "," + hour;
    }
    
    /**
	 * 判断是否超过24小时
	 * @param date1
	 * @param date2
	 * @return
	 * @throws Exception
	 */
	public static boolean getDate(String date1, String date2) throws Exception { 
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
        java.util.Date start = sdf.parse(date1); 
        java.util.Date end = sdf.parse(date2); 
        long cha = end.getTime() - start.getTime(); 
        double result = cha * 1.0 / (1000 * 60 * 60); 
        if(result<=24){ 
             //System.out.println("可用");   
             return true; 
        }else{ 
             //System.out.println("已过期");  
             return false; 
        } 
    } 
}