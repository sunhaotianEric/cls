package com.team.lottery.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.team.lottery.vo.User;

/**
 * 系统用户名称处理工具类
 * 
 * @author gs
 *
 */
public class UserNameUtil {

public static SimpleDateFormat sdf = new SimpleDateFormat("HH");

	/**
	 * 获得上级会员的用户名
	 * 
	 * @param regfrom
	 * @return
	 */
	public static String getUpLineUserName(String regfrom) {
		String upLineUserName = "";
		if (StringUtils.isNotBlank(regfrom)) {
			String[] regFromUserNames = regfrom.split(ConstantUtil.REG_FORM_SPLIT);
			if (regFromUserNames.length > 0) {
				upLineUserName = regFromUserNames[regFromUserNames.length - 1];
			}
		}
		return upLineUserName;
	}

	/**
	 * 获得上上级会员的用户名
	 * 
	 * @param regfrom
	 * @return
	 */
	public static String getUpUpLineUserName(String regfrom) {
		String upLineUserName = "";
		if (StringUtils.isNotBlank(regfrom)) {
			String[] regFromUserNames = regfrom.split(ConstantUtil.REG_FORM_SPLIT);
			if (regFromUserNames.length > 1) {
				upLineUserName = regFromUserNames[regFromUserNames.length - 2];
			}
		}
		return upLineUserName;
	}

	/**
	 * 获得直属会员的用户名
	 * 
	 * @param regfrom
	 * @return
	 */
	public static String getZhiShuUserName(String regfrom) {
		String upLineUserName = "";
		if (StringUtils.isNotBlank(regfrom)) {
			String[] regFromUserNames = regfrom.split(ConstantUtil.REG_FORM_SPLIT);
			if (regFromUserNames.length > 1) {
				upLineUserName = regFromUserNames[1];
			}
		}
		return upLineUserName;
	}

	/**
	 * 获得总代会员的用户名
	 * 
	 * @param regfrom
	 * @return
	 */
	public static String getZongDaiUserName(String regfrom) {
		String upLineUserName = "";
		if (StringUtils.isNotBlank(regfrom)) {
			String[] regFromUserNames = regfrom.split(ConstantUtil.REG_FORM_SPLIT);
			if (regFromUserNames.length > 2) {
				upLineUserName = regFromUserNames[2];
			}
		}
		return upLineUserName;
	}

	/**
	 * 根据业务系统获取超级前台用户
	 * 
	 * @param bizSystem
	 * @return
	 */
	public static String getSuperUserNameByBizSystem(String bizSystem) {
		StringBuffer sb = new StringBuffer();
		sb.append(bizSystem);
		sb.append("Admin");
		return sb.toString();
	}

	/**
	 * 判断用户名称是否在用户列表里
	 * 
	 * @param userName
	 * @param users
	 * @return
	 */
	public static Boolean isInUsers(String userName, List<User> users) {
		Boolean res = false;
		for (User user : users) {
			if (userName.equals(user.getUserName())) {
				res = true;
				break;
			}
		}
		return res;
	}

	/**
	 * 根据regfrom校验是否是用户的下级
	 * @param regfrom
	 * @param userName
	 * @return
	 */
	public static Boolean isInRegfrom(String regfrom, String userName) {
		Boolean res = false;
		if (StringUtils.isNotBlank(regfrom)) {
			String[] regFromUserNames = regfrom.split(ConstantUtil.REG_FORM_SPLIT);
			for (int i = 0; i < regFromUserNames.length; i++) {
				if (userName.equals(regFromUserNames[i])) {
					res = true;
					break;
				}
			}
		}
		return res;
	}

	/**
	 * 判断手机,还是邮箱,或者用户名
	 */
	public static String getUserNameType(String userName) {
		if (RegxUtil.isEmailNumber(userName)) {
			return "email";
		} else if (RegxUtil.isPhoneNumber(userName)) {
			return "phone";
		} else {
			return "username";
		}
	}

	


	/**
	 * 随机生成4到8位的用户名!
	 * @return
	 */
	public static String getRandomUsername() {
		// 大写字母
		String[] upcaseLetters = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
		// 小写字母
		String[] lowercaseLetters = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
		// 数字
		String[] numbers = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
		// 获取大写字母随机的字符位置.
		Integer minLength = 2;
		Integer maxLength = 3;
		Random random = new Random();
		Integer length = random.nextInt(maxLength) % (maxLength - minLength + 1) + minLength;
		String randomUsername = "";
		for (int i = 0; i < length; i++) {
			int upcaseLettersIndex = (int) (Math.random() * upcaseLetters.length);
			randomUsername += upcaseLetters[upcaseLettersIndex];
		}
		for (int i = 0; i < length; i++) {
			// 获取小写字母的随机位置.
			int lowercaseLettersIndex = (int) (Math.random() * lowercaseLetters.length);
			randomUsername += lowercaseLetters[lowercaseLettersIndex];
		}
		
		// 数字位置的随机字符位置.
		Integer minNumberLength = 0;
		Integer maxNumberLength = 2;
		Integer numberLength = random.nextInt(maxNumberLength) % (maxNumberLength - minNumberLength + 1) + minNumberLength;
		
		for (int i = 0; i < numberLength; i++) {
			// 获取数字随机字符的位置.
			int numbersIndex = (int) (Math.random() * numbers.length);
			randomUsername += numbers[numbersIndex];
		}
		return randomUsername;
	}

	public static void main(String[] args) {
		String userNameType = getUserNameType("deng5029");
		System.out.println(userNameType);
	}

}
