package com.team.lottery.util;

import org.apache.commons.lang.StringUtils;

public class XSSFilterUtil {
	
	
	
	//private static final String FLT = "script|<|>";
	
	private static final String FLT = "script|javascript|function|xss|xss.|embed";
	private static final String MGRFLT = "script|";
	
	/**
	 * 前台过滤
	 * @param content
	 * @return
	 */
	public static String getSqlFilterStr(String content) {
		if(!StringUtils.isEmpty(content)){
			String filter[] = FLT.split("\\|");
	        for(int i=0;i < filter.length; i++){
	        	while(StringUtils.containsIgnoreCase(content, filter[i])){
	        	 content = content.replaceAll("(?i)"+filter[i], ""); 
	        	}
	        }
			return content;
		}else{
			return content;
		}
		
	}
	/**
	 * 后台过滤
	 * @param content
	 * @return
	 */
	public static String getSqlMgrFilterStr(String content) {
//		if(!StringUtils.isEmpty(content)){
//			String filter[] = MGRFLT.split("\\|");
//	        for(int i=0;i < filter.length; i++){
//	         	while(StringUtils.containsIgnoreCase(content, filter[i])){
//		        	 content = content.replaceAll("(?i)"+filter[i], "");
//		        	}
//	        }
//			return content;
//		}else{
//			return content;
//		}
		return content;
	}
}
