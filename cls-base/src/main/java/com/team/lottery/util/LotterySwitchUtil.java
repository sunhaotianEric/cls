package com.team.lottery.util;

import java.util.ArrayList;
import java.util.List;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.vo.BizSystemDomain;
import com.team.lottery.vo.BizSystemInfo;
import com.team.lottery.vo.LotterySwitch;

/**
 * 彩种开关工具类.
 * 
 * @author jamine
 *
 */
public class LotterySwitchUtil {
	
	/**
	 * 根据业务系统获取热门彩种开关.
	 * @param bizSystem
	 * @return
	 */
	public static List<LotterySwitch> getHotLotterySwitchByBizSystem(String bizSystem){
		// 该集合用于存储热门彩种开关集合.
		List<LotterySwitch> hotLotterySwitchs = new ArrayList<LotterySwitch>();
		// 获取业务系统信息对象.
		BizSystemInfo bizSystemInfo = ClsCacheManager.getBizSystemInfo(bizSystem);
		if (bizSystemInfo == null) {
			return hotLotterySwitchs;
		}
		// 获取到热门彩种字符串.
		String hotLotteryKinds = bizSystemInfo.getHotLotteryKinds();
		// 用逗号分隔开
		String[] hotLotteryKindsStr = hotLotteryKinds.split(",");
		// 根据业务系统获取对应的彩种开关状态.(需要判断全局开关和系统开关.)
		List<LotterySwitch> lotterySwitchsStatusByBizSystem = ClsCacheManager.getLotterySwitchsStatusByBizSystem(bizSystem);
		
		// 从状态为开启的彩种中获取热门彩种.并且添加到集合中.
		for (int i = 0; i < hotLotteryKindsStr.length; i++) {
			for (LotterySwitch lotterySwitch : lotterySwitchsStatusByBizSystem) {
				if (lotterySwitch.getLotteryStatus() == 1) {
					if (lotterySwitch.getLotteryType().equals(hotLotteryKindsStr[i])) {
						hotLotterySwitchs.add(lotterySwitch);
						continue;
					}
				}
			}
		}
		return hotLotterySwitchs;
	}
	
	/**
	 * 根据业务系统和域名类型返回首页彩种开关(包括状态.)
	 * @param bizSystem 业务系统.
	 * @param domainType 域名类型.
	 * @return
	 */
	public static List<LotterySwitch> getIndexLotterySwitchByBizSystem(String bizSystem , String domainType){
		// 该集合用于存储首页彩种开关集合.
		List<LotterySwitch> indexLotterySwitchs = new ArrayList<LotterySwitch>();
		// 获取业务系统信息对象.
		BizSystemInfo bizSystemInfo = ClsCacheManager.getBizSystemInfo(bizSystem);
		String indexDefaultLotteryKinds = "";
		// 根据是电脑端还是手机端返回首页彩种开关.(通过全局彩种开关和业务彩种开关处理过后的彩种开关).
		if (domainType.equals(BizSystemDomain.DOMAIN_TYPE_APP) || domainType.equals(BizSystemDomain.DOMAIN_TYPE_MOBILE)) {
			indexDefaultLotteryKinds = bizSystemInfo.getIndexMobileDefaultLotteryKinds();
		} else {
			indexDefaultLotteryKinds = bizSystemInfo.getIndexDefaultLotteryKinds();
		}
		String[] indexLotterySwitchStrs = indexDefaultLotteryKinds.split(",");
		// 根据业务系统获取对应的彩种开关状态.(需要判断全局开关和系统开关.)
		List<LotterySwitch> lotterySwitchsStatusByBizSystem = ClsCacheManager.getLotterySwitchsStatusByBizSystem(bizSystem);
		// 从状态为开启的彩种中获取热门彩种.并且添加到集合中.
		for (int i = 0; i < indexLotterySwitchStrs.length; i++) {
			for (LotterySwitch lotterySwitch : lotterySwitchsStatusByBizSystem) {
				if (lotterySwitch.getLotteryType().equals(indexLotterySwitchStrs[i])) {
					indexLotterySwitchs.add(lotterySwitch);
					continue;
				}
			}
		}
		return indexLotterySwitchs;
	}
	
}
