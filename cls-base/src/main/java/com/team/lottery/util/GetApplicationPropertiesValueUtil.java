package com.team.lottery.util;

import java.io.InputStream;
import java.util.Properties;

/**
 * 根据字段获取配置文件的值!
 * 
 * @author jamine
 *
 */
public class GetApplicationPropertiesValueUtil {
	
	// 静态资源配置文件.(开奖线程是否调用)
	public static String OFFICIALLOTTERYOPEN;
	
	/**
	 * 根据字段获取对应的properties中的字段的值.
	 * 
	 * @param field
	 * @return
	 */
	public static String getPropertiesValue(String field) {
		
		Properties props = new Properties();
		InputStream in;
		// 是否开启开奖线程.(0是关闭,1是开启,默认为1)
		String fieldValue = "";
		try {
			// 获取application.properties配置文件中的值
			in = GetApplicationPropertiesValueUtil.class.getClassLoader().getResourceAsStream("application.properties");
			props.load(in);
			fieldValue = props.getProperty(field);
		} catch (Exception e) {
			return fieldValue;
		}
		return fieldValue;

	}
}
