package com.team.lottery.util.sms;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.enums.ESmsSendType;
import com.team.lottery.util.RegxUtil;
import com.team.lottery.vo.SmsSendConfig;

public class SmsSender {

	private static Logger logger = LoggerFactory.getLogger(SmsSender.class);

	/**
	 * 发送短信验证码指定通道类型
	 * 
	 * @param content
	 * @param phones
	 * @param type
	 * @return
	 */
	public static String sendPhoneByType(String content, String phones, String type) {
		SmsSendConfig sendConfig = ClsCacheManager.getSmsSendConfig(type);
		if (sendConfig == null) {
			logger.error("缓存配置获取到的短信发送配置为空");
			return "0";
		}
		if (StringUtils.isBlank(content) || StringUtils.isBlank(phones)) {
			return "0";
		}
		if (SmsSendConfig.TX_API_TYPE.equals(sendConfig.getApiType())) {
			return SmsSendeTxImpl.sendSms(sendConfig.getApiUrl(), sendConfig.getUserName(), sendConfig.getPassword(), content, phones);
		} else if (SmsSendConfig.HX_API_TYPE.equals(sendConfig.getApiType())) {
			return SmsSendHxImpl.sendSms(sendConfig.getApiUrl(), sendConfig.getUserName(), sendConfig.getPassword(), content, phones);
		} else {
			logger.error("找不到对应的短信接口实现");
			return "0";
		}

	}

	/**
	 * 发送短信验证码，使用默认通道类型
	 * 
	 * @param content 发送内容,
	 * @param phones 发送的手机号,可以是多个,也可以是一个!
	 * @return true表示发送成功,false表示发送失败!
	 */
	public static boolean sendPhone(String content, String phones) {
		if (StringUtils.isBlank(content) || StringUtils.isBlank(phones)) {
			logger.error("传入的短信类容或者手机号为空.");
			return false;
		}
		// 将传入的phones号码进行拆分.然后进行判断,是国际号码还是国际号码.然后封装在两个list里面,最后调用对应的短信发送接口进行发送!
		// 新建集合,用于存放中国手机号码集合.
		List<String> chinaNumber = new ArrayList<String>();
		// 新建集合用于存放国际号码集合.
		List<String> internationalNumber = new ArrayList<String>();
		String[] split = phones.split(",");
		for (int i = 0; i < split.length; i++) {
			String number = split[i];
			// 先正则校验国内的手机号,(如果校验结果为true说明当前号码是国内手机号码,为false的时候,就把号码归类为国际号码).
			boolean phoneNumber = RegxUtil.isPhoneNumber(number);
			if (phoneNumber) {
				// 国内手机号.
				chinaNumber.add(number);
			} else {
				// 国际手机号.
				internationalNumber.add(number);
			}
		}
		// 查看国际号码集合,是否含有国际号码,如果有的话,就查询出国际短信发送接口对象.
		if (internationalNumber.size() > 0) {
			String internationalNumberStr = "";
			if (internationalNumber.size() == 1) {
				internationalNumberStr = internationalNumber.get(0);
			} else {
				// 取出第一个号码,并且跟后面的号码进行拼接,来处理批量发送的功能!
				internationalNumberStr = internationalNumber.get(0);
				for (int i = 1; i < internationalNumber.size(); i++) {
					internationalNumberStr += "," + internationalNumber.get(i);
				}

			}

			// 查询可以发送国际短信的接口欧
			SmsSendConfig internationalSendConfig = ClsCacheManager.getSmsSendConfig(ESmsSendType.INTERNATIONNAL.getCode());
			if (internationalSendConfig == null) {
				logger.error("系统查询出:" + ESmsSendType.INTERNATIONNAL.getDescription() + "短信配置为空,请联系管理员.");
				return false;
			}
			// 查询出对应的短信接口.并且进行匹配!
			if (SmsSendConfig.HX_API_TYPE.equals(internationalSendConfig.getApiType())) {
				String sendSms = SmsSendHxImpl.sendSms(internationalSendConfig.getApiUrl(), internationalSendConfig.getUserName(), internationalSendConfig.getPassword(), content,
						internationalNumberStr);
				if (sendSms != null && sendSms.equals("1")) {
					return true;
				}
			}
		}
		// 判断当前的国内手机发送集合是否为空.(如果不为空,就调用默认的接口发送对象.)
		if (chinaNumber.size() > 0) {
			// 新建一个字符串用于处理国内段手机号码拼接.
			String chinaNumberStr = "";
			// 如果只有一个号码就不用进行拼接.
			if (chinaNumber.size() == 1) {
				chinaNumberStr = chinaNumber.get(0);
			} else {
				// 拼接号码.
				chinaNumberStr = chinaNumber.get(0);
				for (int i = 1; i < chinaNumber.size(); i++) {
					chinaNumberStr += "," + chinaNumber.get(i);
				}
			}
			// 查询默认配置短信配置对象.
			SmsSendConfig defaultSendConfig = ClsCacheManager.getSmsSendConfig(ESmsSendType.DEFAULT.getCode());
			if (defaultSendConfig == null) {
				logger.error("系统查询出:" + ESmsSendType.DEFAULT.getDescription() + "短信配置为空,请联系管理员.");
				return false;
			}
			// 匹配海信短信配置对象,并且调用对应的实现.
			if (SmsSendConfig.HX_API_TYPE.equals(defaultSendConfig.getApiType())) {
				// 调用发送接口
				String sendSms = SmsSendHxImpl.sendSms(defaultSendConfig.getApiUrl(), defaultSendConfig.getUserName(), defaultSendConfig.getPassword(), content, chinaNumberStr);
				// 返回结果为1的时候表示调用海信发送短信成功,返回0表示失败.
				if (sendSms != null && sendSms.equals("1")) {
					return true;
				}
			} else if (SmsSendConfig.TX_API_TYPE.equals(defaultSendConfig.getApiType())) {
				// 匹配图信的短信配置对象,并且调用对应的实现.
				String result = SmsSendeTxImpl.sendSms(defaultSendConfig.getApiUrl(), defaultSendConfig.getUserName(), defaultSendConfig.getPassword(), content, phones);
				if (result != null && "0".equals(result)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Main方法用于测试短信发送.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		List<String> list = new ArrayList<String>();
		list.add("639158783038");
		list.add("639158783037");
		list.add("639158783036");
		list.add("639158783035");
		list.add("639158783034");
		list.add("639158783033");
		list.add("639158783032");
		String str = list.get(0);
		for (int i = 1; i < list.size(); i++) {
			str += "," + list.get(i);
		}
		System.out.println(str);
		String str1 = "13922474647";
		String[] split = str1.split(",");
		System.out.println(split[0]);
		/*
		 * String sendPhone2 = sendPhoneByType("测试国际短信发送", "639158783038",
		 * "DEFAULT"); System.err.println(sendPhone2);
		 */

		boolean sendPhone = sendPhone("你好", "13922474647");
		System.err.println(sendPhone);
	}
}
