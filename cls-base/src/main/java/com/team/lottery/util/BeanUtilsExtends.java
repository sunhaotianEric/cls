package com.team.lottery.util;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.ConvertUtils;
/**
 * 重写BeanUtils.populate
 * @author zzh
 *
 */
public class BeanUtilsExtends extends BeanUtils{
	 static {
	       ConvertUtils.register(new DateConvert(), java.util.Date.class);
	       ConvertUtils.register(new DateConvert(), java.sql.Date.class);
	   }
	
	  public static void populate(Object bean, Map<String, ? extends Object> properties)
		        throws IllegalAccessException, InvocationTargetException {
          BeanUtilsBean.getInstance().populate(bean, properties);
	   }
	  
	  public static void copyProperties(Object dest, Object orig) {
	       try {
	           BeanUtils.copyProperties(dest, orig);
	       } catch (IllegalAccessException ex) {
	           ex.printStackTrace();
	       } catch (InvocationTargetException ex) {
	           ex.printStackTrace();
	       }
	   }
}
