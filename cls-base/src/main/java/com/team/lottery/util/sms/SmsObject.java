package com.team.lottery.util.sms;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "returnsms") 
@XmlAccessorType(XmlAccessType.FIELD)
public class SmsObject {
	@XmlElement(name = "returnstatus")
	private String returnstatus;
	@XmlElement(name = "message")
	private String message;
	@XmlElement(name = "taskID")
	private String taskID;
	public String getReturnstatus() {
		return returnstatus;
	}
	public void setReturnstatus(String returnstatus) {
		this.returnstatus = returnstatus;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getTaskID() {
		return taskID;
	}
	public void setTaskID(String taskID) {
		this.taskID = taskID;
	}
	
	
	
}
