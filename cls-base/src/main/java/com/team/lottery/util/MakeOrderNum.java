package com.team.lottery.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.team.lottery.system.SystemPropeties;

/**
 * @ClassName: MakeOrderNum
 * @CreateTime 2015年9月13日 下午4:51:02
 * @author : mayi
 * @Description: 订单号生成工具，生成非重复订单号，理论上限1毫秒1000个，可扩展
 *
 */
public class MakeOrderNum {
	/**
	 * 充值
	 */
	public static final String CZ = "CZ";
	/**
	 * 提现
	 */
	public static final String TX = "TX";
	/**
	 * 提现手续费
	 */
	public static final String TF = "TF";
	/**
	 * 充值赠送
	 */
	public static final String ZS = "ZS";
	/**
	 * 转账
	 */
	public static final String TS = "TS";
	/**
	 * 活动彩金
	 */
	public static final String CJ = "CJ";
	
	/**
	 * 中奖
	 */
	public static final String ZJ = "ZJ";
	
	
	/**
	 * 人工存入
	 */
	public static final String XF = "XF";
	
	/**
	 * 人工存款
	 */
	public static final String CK = "CK";
	

	/**
	 * 误存提出
	 */
	public static final String KF = "KF";
	
	/**
	 * 行政提出
	 */
	public static final String XZ = "XZ";
	
	/**
	 * 返点
	 */
	public static final String FS = "FS";
	
	/**
	 * 提成
	 */
	public static final String TC = "TC";
	
	/**
	 * 普通投注
	 */
	public static final String PT = "PT";
	
	/**
	 * 追号投注
	 */
	public static final String ZT = "ZT";
	
	/**
	 * 追号 停止投注，撤单
	 */
	public static final String LS = "LS";
	
	/**
	 * 未开奖,撤单
	 */
	public static final String LR = "LR";
	
	/**
	 * 工资
	 */
	public static final String GZ = "GZ";
	
	/**
	 * 分红
	 */
	public static final String FH = "FH";
	
	/**
	 * 锁对象，可以为任意对象
	 */
	private static Object lockObj = "lockerOrder";
	/**
	 * 订单号生成计数器
	 */
	private static long orderNumCount = 0L;
	/**
	 * 每毫秒生成订单号数量最大值
	 */
	private static int maxPerMSECSize=1000;
	/**
	 * 生成非重复订单号，理论上限1毫秒1000个，可扩展
	 * @param tname 测试用
	 */
	
	/**
	 * 考虑分布式机子，可以加分布式机子的编号，在本地文件配置
	 */
	public static String serverNum="0";
	
	
	/**
	 * 订单号生成规则：
	 * yyyyMMddHHmmssSSS+分布式服务器编号（01）+xxx 
	 * 如第一台服务器：2016112517055788001001
	 *   第二台服务器：2016112517055788002001
	 * 分布式服务器编号： 到时可以配置到服务器本地文件
	 * xxx ：是一个0到999有顺序的编号，线程锁锁住，递增。
	 * @param tname
	 */
	public static String makeOrderNum(String tname) {
		serverNum=SystemPropeties.serverNum;
		
		// 最终生成的订单号
		String finOrderNum = "";
		try {
			synchronized (lockObj) {
				// 取系统当前时间作为订单号变量前半部分，精确到毫秒
				long nowLong = Long.parseLong(new SimpleDateFormat("yyMMddHHmmssSSS").format(new Date()));
				// 计数器到最大值归零，可扩展更大，目前1毫秒处理峰值1000个，1秒100万
				if (orderNumCount > maxPerMSECSize) {
					orderNumCount = 0L;
				}
				//组装订单号
				String countStr=maxPerMSECSize +orderNumCount+"";
				finOrderNum = tname+nowLong+serverNum+countStr.substring(1);
				orderNumCount++;
			
				// Thread.sleep(1000);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return finOrderNum;
	}

	/**
	 * 撤单订单号生成规则：
	 * 投注订单号后4位（考虑分分彩）+期号后三位相加的最后三位
	 * @param tname
	 */
	public static String makeLROrderNum(String tname,String lotteryId,String expect) {
		lotteryId = lotteryId.substring(2);
		
		// 最终生成的订单号
		String finOrderNum = "";
		Integer lotteryIdInt = Integer.parseInt(lotteryId.substring(lotteryId.length()-4));
		Integer expectInt = Integer.parseInt(expect.substring(expect.length()-3));
		finOrderNum = tname+lotteryId.substring(0, lotteryId.length()-4)+String.format("%04d", (lotteryIdInt+expectInt));
		return finOrderNum;
	}
	public static void main(String[] args) {
//		long nowLongstart = Long.parseLong(new SimpleDateFormat("yyMMddHHmmssSSS").format(new Date()));
//		
//	
//		MakeOrderNum makeOrder = new MakeOrderNum();
//		makeOrder.makeOrderNum("XS");
//
//		long nowLongend = Long.parseLong(new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()));
//		System.out.println(nowLongend);
		String lotteryId = "ZT1801191124391610001";
		Integer lotteryIdInt = Integer.parseInt(lotteryId.substring(lotteryId.length()-5));
		System.out.println(lotteryId.substring(2));
	}

}
