/**
 * 
 */
package com.team.lottery.util;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Spring上下文工具类。
 * 
 * @author wuzhb
 *
 */
public class ApplicationContextUtil implements ApplicationContextAware {
    
    private static ApplicationContext context; // 声明一个静态变量保存
                                               
    public void setApplicationContext(final ApplicationContext contex) {
    	synchronized (ApplicationContextUtil.class) { 
    		ApplicationContextUtil.context = contex;
    	} 
    }
    
    public static ApplicationContext getContext() {
        return ApplicationContextUtil.context;
    }
    
    @SuppressWarnings("unchecked")
	public static synchronized <T> T getBean(final String beanId) {
    	return (T) ApplicationContextUtil.context.getBean(beanId);
    }
    
    @SuppressWarnings("unchecked")
	public static <T> T getBean(final String beanId, Object... args) {
        return (T) ApplicationContextUtil.context.getBean(beanId, args);
    }
    
    /**
	 * 从静态变量applicationContext中取得Bean, 自动转型为所赋值对象的类型.
	 */
	public static <T> T getBean(Class<T> requiredType) {
		return ApplicationContextUtil.context.getBean(requiredType);
	}

}
