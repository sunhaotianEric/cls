package com.team.lottery.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpClientUtil {

	private static Logger log = LoggerFactory.getLogger(HttpClientUtil.class);
	// private static final CloseableHttpClient httpClient;
	// 默认字符集
	public static final String CHARSET = "UTF-8";
	// 响应超时时间,毫秒.
	public static final int DEFAULT_SOCKET_TIMEOUT = 20000;
	// 建立链接超时时间,毫秒
	public static final int DEFAULT_CONNECT_TIMEOUT = 20000;

	// static {
	// RequestConfig config =
	// RequestConfig.custom().setConnectTimeout(60000).setSocketTimeout(15000).build();
	// httpClient =
	// HttpClientBuilder.create().setDefaultRequestConfig(config).build();
	// }

	public static CloseableHttpClient getCloseableHttpClient() {
		CloseableHttpClient httpClient = null;
		RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(DEFAULT_SOCKET_TIMEOUT).setConnectTimeout(DEFAULT_CONNECT_TIMEOUT).build();
		httpClient = HttpClientBuilder.create().setDefaultRequestConfig(requestConfig).build();
		return httpClient;
	}

	public static String doGet(String url, Map<String, String> params) {
		return doGet(url, params, CHARSET);
	}

	public static String doPost(String url, Map<String, String> params) {
		return doPost(url, params, CHARSET);
	}

	public static String doJsonForPost(String urls, JSONObject jsonParam) {
		HttpPost httpPost = null;
		CloseableHttpClient client = null;
		HttpResponse resp = null;
		HttpEntity he = null;
		String respContent = null;
		try {

			httpPost = new HttpPost(urls);
			client = HttpClients.createDefault();

			StringEntity entity = new StringEntity(jsonParam.toString(), "utf-8");// 解决中文乱码问题
			entity.setContentEncoding("UTF-8");
			entity.setContentType("application/json");
			httpPost.setEntity(entity);
			resp = client.execute(httpPost);
			if (resp.getStatusLine().getStatusCode() == 200) {
				he = resp.getEntity();
				respContent = EntityUtils.toString(he, "UTF-8");
			}
			return respContent;
		} catch (Exception e) {
			log.error("HttpClientUtil doJsonForPost发生异常", e);
		}
		return respContent;
	}

	public static String doGet(String url, Map<String, String> params, String charset) {
		if (StringUtils.isBlank(url)) {
			return "";
		}
		String result = "";
		CloseableHttpResponse response = null;
		CloseableHttpClient httpClient = null;
		try {
			if (url.startsWith("https")) {
				// 获取https连接客户端
				httpClient = createSSLInsecureClient();
			} else {
				// 获取http连接客户端
				httpClient = getCloseableHttpClient();
			}

			if (params != null && !params.isEmpty()) {
				List<BasicNameValuePair> pairs = new ArrayList<BasicNameValuePair>();
				for (Map.Entry<String, String> entry : params.entrySet()) {
					String value = entry.getValue();
					if (value != null) {
						pairs.add(new BasicNameValuePair(entry.getKey(), value));
					}
				}
				url += "?" + EntityUtils.toString(new UrlEncodedFormEntity(pairs, charset));
			}
			HttpGet httpGet = new HttpGet(url);
			response = httpClient.execute(httpGet);
			int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode != HttpStatus.SC_OK) {
				httpGet.abort();
				throw new RuntimeException("HttpClient,error status code :" + statusCode);
			}
			HttpEntity entity = response.getEntity();

			if (entity != null) {
				result = EntityUtils.toString(entity, charset);
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			log.error("HttpClientUtil doGet发生异常", e);
		} finally {
			try {
				if (response != null) {
					response.close();
				}
			} catch (IOException e) {
				log.error("HttpClientUtil doGet response.close()发生IO异常", e);
			}
			try {
				if (httpClient != null) {
					httpClient.close();
				}
			} catch (IOException e) {
				log.error("HttpClientUtil doGet httpClient.close()发生IO异常", e);
			}
		}
		return result;
	}

	public static String doPost(String url, Map<String, String> params, String charset) {
		if (StringUtils.isBlank(url)) {
			return "";
		}
		String result = "";
		CloseableHttpResponse response = null;
		CloseableHttpClient httpClient = null;
		try {
			if (url.startsWith("https")) {
				// 获取https连接客户端
				httpClient = createSSLInsecureClient();
			} else {
				// 获取http连接客户端
				httpClient = getCloseableHttpClient();
			}

			List<BasicNameValuePair> pairs = new ArrayList<BasicNameValuePair>();
			// List pairs = null;
			if (params != null && !params.isEmpty()) {
				pairs = new ArrayList<BasicNameValuePair>(params.size());
				for (Map.Entry<String, String> entry : params.entrySet()) {
					String value = entry.getValue();
					if (value != null) {
						pairs.add(new BasicNameValuePair(entry.getKey(), value));
					}
				}
			}
			HttpPost httpPost = new HttpPost(url);
			if (pairs != null && pairs.size() > 0) {
				httpPost.setEntity(new UrlEncodedFormEntity(pairs, CHARSET));
			}
			response = httpClient.execute(httpPost);
			int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode != HttpStatus.SC_OK) {
				httpPost.abort();
				log.error("HttpClient doPost error status code :" + statusCode);
			}
			HttpEntity entity = response.getEntity();

			if (entity != null) {
				result = EntityUtils.toString(entity, charset);
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			log.error("HttpClientUtil doPost发生异常", e);
		} finally {
			try {
				if (response != null) {
					response.close();
				}
			} catch (IOException e) {
				log.error("HttpClientUtil doPost response.close()发生IO异常", e);
			}
			try {
				if (httpClient != null) {
					httpClient.close();
				}
			} catch (IOException e) {
				log.error("HttpClientUtil doPost httpClient.close()发生IO异常", e);
			}
		}
		return result;
	}

	public static String doPostSSL(String apiUrl, JSONObject json) {

		CloseableHttpClient httpClient = null;
		HttpPost httpPost = new HttpPost(apiUrl);
		CloseableHttpResponse response = null;
		String httpStr = null;
		try {

			if (apiUrl.startsWith("https")) {
				// 获取https连接客户端
				httpClient = createSSLInsecureClient();
			} else {
				// 获取http连接客户端
				httpClient = getCloseableHttpClient();
			}

			StringEntity stringEntity = new StringEntity(json.toString(),"UTF-8");//解决中文乱码问题
			stringEntity.setContentEncoding("UTF-8");
			stringEntity.setContentType("application/json");
			httpPost.setEntity(stringEntity);
			httpPost.setHeader("Content-Type", "application/json");
			response = httpClient.execute(httpPost);
			int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode != HttpStatus.SC_OK) {
				return null;
			}
			HttpEntity entity = response.getEntity();
			if (entity == null) {
				return null;
			}
			httpStr = EntityUtils.toString(entity, "utf-8");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (response != null) {
				try {
					EntityUtils.consume(response.getEntity());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return httpStr;
	}

	/**
	 * 上传图片
	 * 
	 * @param url
	 * @param file
	 * @param params
	 * @return
	 */
	public static String doPostFileUpload(String url, File file, Map<String, String> params) {
		return doPostFileUpload(url, file, params, CHARSET);
	}

	/**
	 * 上传图片
	 * 
	 * @param url
	 * @param file
	 * @param params
	 * @param charset
	 * @return
	 */
	public static String doPostFileUpload(String url, File file, Map<String, String> params, String charset) {
		if (StringUtils.isBlank(url)) {
			return "";
		}
		String result = "";
		CloseableHttpResponse response = null;
		CloseableHttpClient httpClient = null;
		try {
			if (url.startsWith("https")) {
				// 获取https连接客户端
				httpClient = createSSLInsecureClient();
			} else {
				// 获取http连接客户端
				httpClient = getCloseableHttpClient();
			}

			MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create();
			multipartEntityBuilder.addBinaryBody("file", file);
			for (Map.Entry<String, String> entry : params.entrySet()) {
				String value = entry.getValue();
				if (value != null) {
					multipartEntityBuilder.addTextBody(entry.getKey(), value);
				}
			}
			HttpEntity httpEntity = multipartEntityBuilder.build();

			HttpPost httpPost = new HttpPost(url);
			httpPost.setEntity(httpEntity);
			response = httpClient.execute(httpPost);
			int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode != HttpStatus.SC_OK) {
				httpPost.abort();
				log.error("HttpClient doPost error status code :" + statusCode);
			}
			HttpEntity entity = response.getEntity();

			if (entity != null) {
				result = EntityUtils.toString(entity, charset);
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			log.error("HttpClientUtil doPost发生异常", e);
		} finally {
			try {
				if (response != null) {
					response.close();
				}
			} catch (IOException e) {
				log.error("HttpClientUtil doPost response.close()发生IO异常", e);
			}
			try {
				if (httpClient != null) {
					httpClient.close();
				}
			} catch (IOException e) {
				log.error("HttpClientUtil doPost httpClient.close()发生IO异常", e);
			}
		}
		return result;
	}

	/**
	 * 创建 SSL连接
	 * 
	 * @return
	 * @throws GeneralSecurityException
	 */
	private static CloseableHttpClient createSSLInsecureClient() throws GeneralSecurityException {
		try {
			SSLContext ctx = SSLContext.getInstance("TLSv1.2");
			X509TrustManager tm = new X509TrustManager() {
				@Override
				public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
				}

				@Override
				public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
				}

				@Override
				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}
			};
			ctx.init(null, new TrustManager[] { tm }, null);
			SSLConnectionSocketFactory ssf = new SSLConnectionSocketFactory(ctx, NoopHostnameVerifier.INSTANCE);
			RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(DEFAULT_SOCKET_TIMEOUT).setConnectTimeout(DEFAULT_CONNECT_TIMEOUT).build();
			CloseableHttpClient httpclient = HttpClients.custom().setSSLSocketFactory(ssf).setDefaultRequestConfig(requestConfig).build();
			return httpclient;

		} catch (GeneralSecurityException e) {
			throw e;
		}
	}
	
	public static String doPostUrl(String url, Map<String, String> params) {  
        URL u = null;  
        HttpURLConnection con = null;  
        // 构建请求参数  
        StringBuffer sb = new StringBuffer();  
        if (params != null) {  
            for (Entry<String, String> e : params.entrySet()) {  
                sb.append(e.getKey());  
                sb.append("=");  
                sb.append(e.getValue());  
                sb.append("&");  
            }  
            sb.substring(0, sb.length() - 1);  
        }  
        System.out.println("send_url:" + url);  
        System.out.println("send_data:" + sb.toString());  
        // 尝试发送请求  
        try {  
            u = new URL(url);  
            con = (HttpURLConnection) u.openConnection();  
            //// POST 只能为大写，严格限制，post会不识别  
            con.setRequestMethod("POST");  
            con.setDoOutput(true);  
            con.setDoInput(true);  
            con.setUseCaches(false);  
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");  
            OutputStreamWriter osw = new OutputStreamWriter(con.getOutputStream(), "UTF-8");  
            osw.write(sb.toString());  
            osw.flush();  
            osw.close();  
        } catch (Exception e) {  
            e.printStackTrace();  
        } finally {  
            if (con != null) {  
                con.disconnect();  
            }  
        }  
  
        // 读取返回内容  
        StringBuffer buffer = new StringBuffer();  
        try {  
            //一定要有返回值，否则无法把请求发送给server端。  
            BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));  
            String temp;  
            while ((temp = br.readLine()) != null) {  
                buffer.append(temp);  
                buffer.append("\n");  
            }  
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
  
        return buffer.toString();  
    }
	
	public static void main(String[] args) {
		try {
			// 支持https和http
			Map<String, String> map = new HashMap<String, String>();
			 //ap.put("name", "111");
			 //map.put("page", "222");
			String str = doPost("https://www.yn63.com/api/shopApi/order/createorder", map, "UTF-8");
			System.out.println(str);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
