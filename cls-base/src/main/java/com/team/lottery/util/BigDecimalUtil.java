package com.team.lottery.util;

import java.math.BigDecimal;

public class BigDecimalUtil {

	/**
	 * 数据是否大于0或者不等于null
	 * @param bigData
	 * @return
	 */
	public static boolean isEmpty(BigDecimal bigData){
		if(bigData == null || bigData.compareTo(new BigDecimal(0)) == 0){
			return true;
		}
		return false;
	}
}
