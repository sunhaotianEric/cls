package com.team.lottery.util;

import java.security.MessageDigest;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class Md5Util {
	private static final Log log = LogFactory.getLog(Md5Util.class);
	
	public static String getMD5ofStr(String str){
		return getMD5ofStr(str,"utf-8");
	}
	
	public static String getMD5ofStr(String str, String encode) {
		try {
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			md5.update(str.getBytes(encode));
			byte[] digest = md5.digest();

			StringBuffer hexString = new StringBuffer();
			String strTemp;
			for (int i = 0; i < digest.length; i++) {
				// byteVar &
				// 0x000000FF的作用是，如果digest[i]是负数，则会清除前面24个零，正的byte整型不受影响。
				// (...) | 0xFFFFFF00的作用是，如果digest[i]是正数，则置前24位为一，
				// 这样toHexString输出一个小于等于15的byte整型的十六进制时，倒数第二位为零且不会被丢弃，这样可以通过substring方法进行截取最后两位即可。
				strTemp = Integer.toHexString((digest[i] & 0x000000FF) | 0xFFFFFF00).substring(6);
				hexString.append(strTemp);
			}
			return hexString.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}

	}
	
	/**
	 * 获取参数签名
	 * 
	 * @param paramMap
	 *            签名参数
	 * @param paySecret
	 *            签名密钥
	 * @return
	 */
	public static String getSign(Map<String, Object> paramMap,String keyName, String paySecret) {
		SortedMap<String, Object> smap = new TreeMap<String, Object>(paramMap);
		StringBuffer stringBuffer = new StringBuffer();
		for (Map.Entry<String, Object> m : smap.entrySet()) {
			Object value = m.getValue();
			if (value != null && StringUtils.isNotBlank(String.valueOf(value))) {
				stringBuffer.append(m.getKey()).append("=").append(m.getValue()).append("&");
			}
		}
		stringBuffer.delete(stringBuffer.length() - 1, stringBuffer.length());
		/*"&paySecret="*/
		String argPreSign = stringBuffer.append(keyName).append(paySecret).toString();
		//System.out.println(argPreSign);
		log.info("MD5原文:" + argPreSign);
	    Md5Util md5Util=new Md5Util();
		String signStr = md5Util.getMD5ofStr(argPreSign).toUpperCase();
		//String signStr = JinHaoMD5Util.encode(argPreSign).toUpperCase();
		log.info("MD5值：" + signStr);
		return signStr;
	}

	public static void main(String[] args) {
		String infoStr = "version=3.0&method=Rx.online.pay&partner=16960&banktype=ALIPAY&paymoney=2.0.0&ordernumber=20161110161947421&callbackurl=http://www.youdomain.com/callback.aspx270bbae38500459a90b7b2f49a9aa6ba";
		String singStr = getMD5ofStr(infoStr);
		System.out.println(singStr);
	}
}
