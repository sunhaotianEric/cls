package com.team.lottery.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.team.lottery.enums.EZodiac;
import com.team.lottery.system.SystemConfigConstant;

public class ShengXiaoNumberUtil {
	
	private final static String[] animalYear = new String[]{"SHU","NIU","HU","TU","LONG","SHE","MA","YANG","HOU","JI","GOU","ZHU"};
	
	private final static int startYear = 2008;//定义起始年,鼠年
	//红波
	private final static String hongboCodes = "1,2,7,8,12,13,18,19,23,24,29,30,34,35,40,45,46";
	//绿波
	private final static String lvboCodes =   "5,6,11,16,17,21,22,27,28,32,33,38,39,43,44,49";
	//蓝波
	private final static String lanboCodes = "3,4,9,10,14,15,20,25,26,31,36,37,41,42,47,48";
    
	public final static int HONGBO = 1;
	public final static int LVBO = 3;
	public final static int LANBO = 2;
	
	/**
	 * 根据当年的年份春节，生成12生肖号码
	 * @param springFestivalStartDate 当年的春节开始时间
	 * @return
	 */
	public static Map<String, String> convertShengxiao(Date springFestivalStartDate) {
		Map<String, String> shengxiaoMap = new HashMap<String, String>();
		int nowYear = 0;
		Date nowDate = new Date();
		int nowDateYear = DateUtil.getYear(nowDate);
		int nextDateYear = DateUtil.getYear(springFestivalStartDate);
		// 只有在当前时间和设定的春节时间在同一年，且当前时间小于设定的春节时间才变更年份
		// 在进入新的一年后，年份会增加一年，这时候应当要年份扣掉一年以防止生肖变动，只有过了春节时间之后，才使用新的一年的时间
		if (nowDate.compareTo(springFestivalStartDate) < 0 && nowDateYear == nextDateYear) {
			nowYear = nowDateYear - 1;
		} else {
			nowYear = nowDateYear;
		}
		int pos = (nowYear - startYear + 1) % 12;
		for (String shengxiao : animalYear) {
			int code = pos;
			if (pos <= 0) {
				code = code + 12;
			}
			shengxiaoMap.put(shengxiao, returnCodes(code));

			pos--;
		}
		return shengxiaoMap;
	}
	
	public static String returnCodes(int code){
		String codeStr ="";
		while(code<50){
			codeStr += ","+code;
			code +=12;
		}
		return codeStr.substring(1);
	}
	
	/**
	 * 大==true
	 * 小==false
	 * 49为和
	 * @param code
	 * @return
	 */
	public static Boolean  decideBigOrSmall(String code){
		int codeInt = Integer.parseInt(code);
		
		if(codeInt>24){
			return true;	
		}else{
			return false;
		}
	}
	
	/**
	 * 单==true
	 * 双==false
	 * 49为和
	 * @param code
	 * @return
	 */
	public static Boolean  decideSingleOrdouble(String code){
	   int codeInt = Integer.parseInt(code);
		if(codeInt%2==1){
			return true;	
		}else{
			return false;
		}	
	}
	
	/**
	 * 合大==true
	 * 合小==false
	 * @param code
	 * @return
	 */
	public static Boolean  decideHeDaOrHeXiao(String code){
		
		int codeInt = Integer.parseInt(code);
		if(codeInt>=10){
			codeInt =(codeInt/10)+(codeInt%10);
		}
		
		if(codeInt>=7){
			return true;	
		}else{
			return false;
		}	
	}
	/**
	 * 合单==true
	 * 合双==false
	 * @param code
	 * @return
	 */
	public static Boolean  decideHeDanOrHeShuang(String code){
		
		int codeInt = Integer.parseInt(code);
		if(codeInt>=10){
			codeInt =(codeInt/10)+(codeInt%10);
		}
		
		if(codeInt%2==1){
			return true;	
		}else{
			return false;
		}	
	}
	
	
	/**
	 * 尾大==true
	 * 尾小==false
	 * @param code
	 * @return
	 */
	public static Boolean  decideWeiDaOrWeiXiao(String code){
		int codeInt = Integer.parseInt(code);
		if(codeInt%10>=5){
			return true;	
		}else{
			return false;
		}
		
	}
	
	/**
	 * 家禽==true
	 * 野兽==false
	 * @param code
	 * @return
	 */
	public static Boolean  decideJiaQinOrYeShou(String code){
		Map<String, String> shengxiaoMap = null;
		try {
			shengxiaoMap = convertShengxiao(new SimpleDateFormat("yyyy-MM-dd").parse(SystemConfigConstant.springFestivalStartDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		int codeInt = Integer.parseInt(code);
		for (Map.Entry<String, String> entry : shengxiaoMap.entrySet()) {  
			  String codes = entry.getValue();
			  String shengxiao = entry.getKey();
			  if(codes.startsWith(codeInt+",") || codes.contains(","+codeInt+",") || codes.endsWith(","+codeInt)){
				  EZodiac eZodiac=EZodiac.valueOf(shengxiao);
				  if("poultry".equals(eZodiac.getAnimalType())){
					  return true;
				  }
			  }
		}
		
		return false;	
	}
	
	/**
	 * 家禽==true
	 * 野兽==false
	 * @param code
	 * @return
	 */
	public static Boolean  decideJiaQinOrYeShou(String code,int yearInt){
		Map<String, String> shengxiaoMap =  new HashMap<String, String>();
		Map<String, String> resultShengxiaoMap = new HashMap<String, String>();
		try {
			resultShengxiaoMap = convertShengxiao(new SimpleDateFormat("yyyy-MM-dd").parse(SystemConfigConstant.springFestivalStartDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		//用今年生肖转去年的
		if(yearInt > 0){
			for(int i=0;i<animalYear.length;i++){
				if(i==11){
					shengxiaoMap.put(animalYear[i], resultShengxiaoMap.get(animalYear[0]));
				}else{
					shengxiaoMap.put(animalYear[i], resultShengxiaoMap.get(animalYear[i+1]));	
				}
				
			}
		}else{
			shengxiaoMap = resultShengxiaoMap;
		}
		int codeInt = Integer.parseInt(code);
		for (Map.Entry<String, String> entry : shengxiaoMap.entrySet()) {  
			  String codes = entry.getValue();
			  String shengxiao = entry.getKey();
			  if(codes.startsWith(codeInt+",") || codes.contains(","+codeInt+",")|| codes.endsWith(","+codeInt)){
				  EZodiac eZodiac=EZodiac.valueOf(shengxiao);
				  if("poultry".equals(eZodiac.getAnimalType())){
					  return true;
				  }
			  }
		}
		
		return false;	
	}
	/**
	 * 生肖号码 1-12
	 * @return
	 */
	public static int  retuenShengXiaoCode(String code){
		Map<String, String> shengxiaoMap = null;
		try {
			shengxiaoMap = convertShengxiao(new SimpleDateFormat("yyyy-MM-dd").parse(SystemConfigConstant.springFestivalStartDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		int codeInt = Integer.parseInt(code);
		for(int i=0;i<animalYear.length;i++){
			String[] codes=shengxiaoMap.get(animalYear[i]).split(",");
			for(String c:codes){
				int cInt= Integer.parseInt(c);
				if(cInt==codeInt){
					return i+1;
				}
			}
			
		}
		
		return 0;	
	}
	
	
	/**
	 * 生肖号码 1-12， 
	 * yearInt 1
	 * @return
	 */
	public static int  retuenShengXiaoCode(String code,int yearInt){
		Map<String, String> shengxiaoMap =  new HashMap<String, String>();
		Map<String, String> resultShengxiaoMap = new HashMap<String, String>();
		try {
			resultShengxiaoMap = convertShengxiao(new SimpleDateFormat("yyyy-MM-dd").parse(SystemConfigConstant.springFestivalStartDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		//用今年生肖转去年的
		if(yearInt > 0){
			for(int i=0;i<animalYear.length;i++){
				if(i==11){
					shengxiaoMap.put(animalYear[i], resultShengxiaoMap.get(animalYear[0]));
				}else{
					shengxiaoMap.put(animalYear[i], resultShengxiaoMap.get(animalYear[i+1]));	
				}
				
			}
		}else{
			shengxiaoMap = resultShengxiaoMap;
		}
		
		int codeInt = Integer.parseInt(code);
		for(int i=0;i<animalYear.length;i++){
			String[] codes=shengxiaoMap.get(animalYear[i]).split(",");
			for(String c:codes){
				int cInt= Integer.parseInt(c);
				if(cInt==codeInt){
					return i+1;
				}
			}
			
		}
		
		return 0;	
	}
	
	
	/**
	 * 大单==1
	 * 小单==2
 	 * 大双==3
	 * 小双==4
	 * @param code
	 * @return
	 */
	public static int  decideDXDS(String code){
		  int codeInt = Integer.parseInt(code);
		  if(codeInt%2==1 && codeInt>24){
				return 1;	
		   }else if(codeInt%2==1 && codeInt<=24){
				return 2;
		   }else if(codeInt%2==0 && codeInt>24){
				return 3;
		   }else{
			    return 4;
		   }		
	}
	
	
	
	/**
	 * 红波==1
	 * 铝箔==2
	 * 蓝波==3
	 * @param code
	 * @return
	 */
	public static int  decidePoShe(String code){
		 String codeStr = Integer.parseInt(code)+"";
		 String[] hongboStr = hongboCodes.split(",");
		 String[] lanboStr = lanboCodes.split(",");
		 String[] lvboStr = lvboCodes.split(",");
		 
		 for(String c:hongboStr){
			 if(c.equals(codeStr)){
				 return 1; 
			 }
		 }
		 for(String c:lanboStr){
			 if(c.equals(codeStr)){
				 return 2; 
			 }
		 }
		 
		 for(String c:lvboStr){
			 if(c.equals(codeStr)){
				 return 3; 
			 }
		 }
		return 0;
	}
	
	
	/**
	 * 总单 == true，总双 ==false
	 *  所有七个开奖号码的分数总和是单数叫(总分单)，如分数总和是115、183；
	 *  分数总和是双数叫(总 分双)，如分数总和是108、162。假如投注组合符合中奖结果，视为中奖，其馀情形视为不中奖 。
	 * @param code
	 * @return
	 * @throws Exception 
	 */
	public static Boolean  decideZongHeDan_Shuang(String[] code) throws Exception{
		if(code.length!=7){
			throw new Exception("开奖号码传递参数错误！");
		}
		int code1 = Integer.parseInt(code[0]);
		int code2 = Integer.parseInt(code[1]);
		int code3 = Integer.parseInt(code[2]);
		int code4 = Integer.parseInt(code[3]);
		int code5 = Integer.parseInt(code[4]);
		int code6 = Integer.parseInt(code[5]);
		int code7 = Integer.parseInt(code[6]);
		int result = code1 + code2 + code3 + code4 + code5 + code6 + code7;
		if(result%2==1){
			return true;	
		}else{
			return false;
		}
	}
	
	/**
	 * 总小==fasle，总大==true
	 * 总和大小 ： 所有七个开奖号码的分数总和大于或等于175为总分大；
	 * 分数总和小于或等于174为总分小。 如开奖号码为02、08、17、28、39、46、25，分数总和是165，
	 * 则总分小。假如投注组合符合 中奖结果，视为中奖，其馀情形视为不中奖。
	 * @param code
	 * @return
	 * @throws Exception 
	 */
	public static Boolean  decideZongHeDa_Xiao(String[] code) throws Exception{
		if(code.length!=7){
			throw new Exception("开奖号码传递参数错误！");
		}
		int code1 = Integer.parseInt(code[0]);
		int code2 = Integer.parseInt(code[1]);
		int code3 = Integer.parseInt(code[2]);
		int code4 = Integer.parseInt(code[3]);
		int code5 = Integer.parseInt(code[4]);
		int code6 = Integer.parseInt(code[5]);
		int code7 = Integer.parseInt(code[6]);
		int result = code1 + code2 + code3 + code4 + code5 + code6 + code7;
		if(result >=175){
			return true;	
		}else{
			return false;
		}
	}
	
	
	/**
	 * 红单 绿单 蓝单
	 * @param code 判断号码
	 * @param poshe 要判断波色
	 * @return
	 */
	public static Boolean  decideBoSeDan(String code,int poshe){
		int result = decidePoShe(code);
		//先判断波色
		if(poshe == result){
			return decideSingleOrdouble(code);
		}
		
		return false;
	}
	
	/**
	 * 红双 绿双 蓝双
	 * @param code 判断号码
	 * @param poshe 要判断波色
	 * @return
	 */
	public static Boolean  decideBoSeShuang(String code,int poshe){
		int result = decidePoShe(code);
		//先判断波色
		if(poshe == result){
			return !decideSingleOrdouble(code);
		}
		
		return false;
	}
	
	
	/**
	 * 红大 绿大 蓝大
	 * @param code 判断号码
	 * @param poshe 要判断波色
	 * @return
	 */
	public static Boolean  decideBoSeDa(String code,int poshe){
		int result = decidePoShe(code);
		//先判断波色
		if(poshe == result){
			return decideBigOrSmall(code);
		}
		
		return false;
	}	
	
	/**
	 * 红小 绿小 蓝小
	 * @param code 判断号码
	 * @param poshe 要判断波色
	 * @return
	 */
	public static Boolean  decideBoSeXiao(String code,int poshe){
		int result = decidePoShe(code);
		//先判断波色
		if(poshe == result){
			return !decideBigOrSmall(code);
		}
		
		return false;
	}	
	
	/**
	 * 红单 绿单 蓝单
	 * @param code 判断号码
	 * @param poshe 要判断波色
	 * @return
	 */
	public static Boolean  decideBoSeHeDan(String code,int poshe){
		int result = decidePoShe(code);
		//先判断波色
		if(poshe == result){
			return decideHeDanOrHeShuang(code);
		}
		
		return false;
	}	
	
	/**
	 * 红双 绿双 蓝双
	 * @param code 判断号码
	 * @param poshe 要判断波色
	 * @return
	 */
	public static Boolean  decideBoSeHeShuang(String code,int poshe){
		int result = decidePoShe(code);
		//先判断波色
		if(poshe == result){
			return !decideHeDanOrHeShuang(code);
		}
		
		return false;
	}
	public static void main(String[] args) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date nextYearDate = sdf.parse("2020-01-25 00:00:00");
		System.out.println(ShengXiaoNumberUtil.convertShengxiao(nextYearDate));	
	
		//decideJiaQinOrYeShou("1");
	}
}
