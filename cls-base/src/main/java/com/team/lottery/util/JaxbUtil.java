package com.team.lottery.util;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class JaxbUtil {

	/**
	 * 将POJP对象转换为XML格式字符串。
	 * 
	 * @param clazz POJO类型
	 * @param object POJP对象
	 * @return XML格式字符串
	 */
	public static String transformToXml(Class<?> clazz, Object object) {
		try {
			StringWriter writer = new StringWriter();
			Marshaller marshaller = JAXBContext.newInstance(clazz).createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.marshal(object, writer);
			return writer.toString();
		} catch (JAXBException e) {
			throw new RuntimeException("[POJO]转换为XML异常...", e);
		}
	}

	/**
	 * 将XML格式字符串转换为POJP对象。
	 * 
	 * @param clazz POJO类型
	 * @param xml XML格式字符串
	 * @return POJO对象
	 * @throws JAXBException 
	 */
	@SuppressWarnings("unchecked")
	public static <T> T transformToObject(Class<T> clazz, String xml) throws JAXBException {
		try {
			StringReader reader = new StringReader(xml);
			Unmarshaller unmarshaller = JAXBContext.newInstance(clazz).createUnmarshaller();
			return (T) unmarshaller.unmarshal(reader);
		} catch (JAXBException e) {
			throw e;
		}
	}

}
