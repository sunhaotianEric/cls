package com.team.lottery.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 获取用户随机头像.
 * 
 * @author Jamine
 *
 */
public class UserHeadImgUtil {

	// 默认头像.
	private static String defaultHeadImg = "man_01.7cefd80a.jpg";

	/**
	 * 存储所有的图片头像集合.
	 * 
	 * @return
	 */
	public static List<String> getUserHeadImgList() {
		// 新建list用于存储图片名称.
		List<String> headImgs = new ArrayList<String>();

		// 男人头像.
		headImgs.add("man_01.7cefd80a.jpg");
		headImgs.add("man_02.03db2fa6.jpg");
		headImgs.add("man_03.0837f309.jpg");
		headImgs.add("man_04.d9473481.jpg");
		headImgs.add("man_05.11de2a3e.jpg");
		headImgs.add("man_06.4dcd3c94.jpg");
		headImgs.add("man_07.a17ceb91.jpg");
		headImgs.add("man_08.b2f80521.jpg");
		headImgs.add("man_09.a30c42b3.jpg");
		headImgs.add("man_10.5dd22979.jpg");

		// 女人头像.
		headImgs.add("woman_01.5b6b493e.jpg");
		headImgs.add("woman_02.d7f61d0d.jpg");
		headImgs.add("woman_03.e9791804.jpg");
		headImgs.add("woman_04.749dbf34.jpg");
		headImgs.add("woman_05.35d674ab.jpg");
		headImgs.add("woman_06.39d47575.jpg");
		headImgs.add("woman_07.ff99e930.jpg");
		headImgs.add("woman_08.38aa8958.jpg");
		headImgs.add("woman_09.8ef9b135.jpg");
		headImgs.add("woman_10.0166e2cf.jpg");

		// NBA篮球明星头像.
		headImgs.add("nba_01.e0a1d99a.jpg");
		headImgs.add("nba_02.bef5e603.jpg");
		headImgs.add("nba_03.26d68656.jpg");
		headImgs.add("nba_04.61ee81fe.jpg");
		headImgs.add("nba_05.a4a8b3a3.jpg");
		headImgs.add("nba_06.8c3d03f3.jpg");
		headImgs.add("nba_07.889f754a.jpg");
		headImgs.add("nba_08.07b58815.jpg");
		headImgs.add("nba_09.0c7909ab.jpg");
		headImgs.add("nba_10.16ba7182.jpg");

		return headImgs;
	}

	/**
	 * 随机从上面List中获取一个头像.
	 * 
	 * @return
	 */
	public static String getUserHeadImg() {
		List<String> userHeadImgList = UserHeadImgUtil.getUserHeadImgList();
		if (userHeadImgList.size() > 0) {
			Random random = new Random();
			int i = random.nextInt(userHeadImgList.size());
			return userHeadImgList.get(i);
		} else {
			return UserHeadImgUtil.defaultHeadImg;
		}
	}
}
