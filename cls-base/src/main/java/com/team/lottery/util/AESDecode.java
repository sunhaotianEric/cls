package com.team.lottery.util;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * AES解密工具类
 * 
 * @author Owner
 *
 */
public class AESDecode {
	private static Logger log = LoggerFactory.getLogger(AESDecode.class);

	// 加密
	public static String encrypt(String sSrc, String sKey) throws Exception {
		if (sKey == null) {
			log.info("Key为空null");
			return null;
		}
		// 判断Key是否为16位
		if (sKey.length() != 16) {
			log.info("Key长度不是16位");
			return null;
		}
		byte[] raw = sKey.getBytes("utf-8");
		SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");// "算法/模式/补码方式"
		cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
		byte[] encrypted = cipher.doFinal(sSrc.getBytes("utf-8"));

		return new Base64().encodeToString(encrypted);// 此处使用BASE64做转码功能，同时能起到2次加密的作用。
	}

	// 解密
	public static String decrypt(String sSrc, String sKey) throws Exception {
		try {
			// 判断Key是否正确
			if (sKey == null) {
				log.info("Key为空null");
				return null;
			}
			// 判断Key是否为16位
			if (sKey.length() != 16) {
				log.info("Key长度不是16位");
				return null;
			}
			byte[] raw = sKey.getBytes("utf-8");
			SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, skeySpec);
			byte[] encrypted1 = new Base64().decode(sSrc);// 先用base64解密
			try {
				byte[] original = cipher.doFinal(encrypted1);
				String originalString = new String(original, "utf-8");
				return originalString;
			} catch (Exception e) {
				log.info(e.toString());
				return null;
			}
		} catch (Exception ex) {
			log.info(ex.toString());
			return null;
		}
	}

	public static void main(String[] args) {
		// long date = new Date().getTime();
		// String aString = String.valueOf(date);// 时间戳
		// System.out.println("时间戳:" + aString);
		String content = "a1234567";
		// 加密
		String enString = null;
		try {
			enString = AESDecode.encrypt(content, "4671601341744826");
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		System.out.println("加密后的字串是：" + enString);

		// 解密
		String DeString = null;
		try {
			DeString = AESDecode.decrypt(enString, "4671601341744826");
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("解密后的字串是：" + DeString);

		// "t36oKkzuinTQByoH4N/UYQ==", "4671601341744826"
		// "5330775289389333", "zSY3FBLTSExmyrZio7oyXg=="

	}

}
