package com.team.lottery.util;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.BeanUtils;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.extvo.LotterySetMsg;
import com.team.lottery.system.BizSystemConfigManager;
import com.team.lottery.system.BizSystemConfigVO;
import com.team.lottery.vo.BizSystemInfo;
import com.team.lottery.vo.LotterySet;

public class LotterySetUntil {

	/**
	 * 根据 域名得到系统彩种设置，业务系统配置信息设置，业务系统信息设置得到最终结果
	 * serverName 域名
	 * @return
	 */
	public  LotterySetMsg compareLotterySets(String  serverName){
		String bizSystem=ClsCacheManager.getBizSystemByDomainUrl(serverName);
		BizSystemConfigVO bizSystemConfigVO=BizSystemConfigManager.getBizSystemConfig(bizSystem);//业务系统彩种设置
	    BizSystemInfo bizSystemInfo=ClsCacheManager.getBizSystemInfo(bizSystem);//热门彩种设置，首页彩种设置
	    LotterySet lotterySet=new LotterySet();
	    LotterySetMsg lotterySetMsg =null;
	    if(bizSystemInfo==null&&bizSystemConfigVO!=null&&lotterySet!=null){
	    	lotterySetMsg=compareLotterySetAndBizSystemConfigVO(lotterySet,bizSystemConfigVO);
	    }else if(bizSystemInfo!=null&&bizSystemConfigVO!=null&&lotterySet!=null){
	    	lotterySetMsg=compareLotterySetALL(lotterySet,bizSystemConfigVO,bizSystemInfo);
	    }
		return lotterySetMsg;
		
	}
	
	public  LotterySetMsg compareLotterySetAndBizSystemConfigVO(LotterySet lotterySet,BizSystemConfigVO bizSystemConfigVO){
		LotterySetMsg lotterySetMsg=new LotterySetMsg();
		if(bizSystemConfigVO==null){
			try {
				BeanUtils.copyProperties(lotterySetMsg, lotterySet);
				
			} catch (IllegalAccessException e) {
				
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
			
			return lotterySetMsg;
		}
		
		
		if(lotterySet.getIsSFKSOpen()!=null&&bizSystemConfigVO.getIsSFKSOpen()!=null
		   &&lotterySet.getIsSFKSOpen()==1&&bizSystemConfigVO.getIsSFKSOpen()==1){
			lotterySetMsg.setIsSFKSOpen(1);
		}
		
		if(lotterySet.getIsWFKSOpen()!=null&&bizSystemConfigVO.getIsWFKSOpen()!=null
				&&lotterySet.getIsWFKSOpen()==1&&bizSystemConfigVO.getIsWFKSOpen()==1){
			lotterySetMsg.setIsWFKSOpen(1);
		}
		
		if(lotterySet.getIsSHFSSCOpen()!=null&&bizSystemConfigVO.getIsSHFSSCOpen()!=null
				&&lotterySet.getIsSHFSSCOpen()==1&&bizSystemConfigVO.getIsSHFSSCOpen()==1){
			lotterySetMsg.setIsSHFSSCOpen(1);
		}
		
		if(lotterySet.getIsSFSSCOpen()!=null&&bizSystemConfigVO.getIsSFSSCOpen()!=null
				&&lotterySet.getIsSFSSCOpen()==1&&bizSystemConfigVO.getIsSFSSCOpen()==1){
			lotterySetMsg.setIsSFSSCOpen(1);
		}
		
		if(lotterySet.getIsWFSSCOpen()!=null&&bizSystemConfigVO.getIsWFSSCOpen()!=null
				&&lotterySet.getIsWFSSCOpen()==1&&bizSystemConfigVO.getIsWFSSCOpen()==1){
			lotterySetMsg.setIsWFSSCOpen(1);
		}
		
		if(lotterySet.getIsLCQSSCOpen()!=null&&bizSystemConfigVO.getIsLCQSSCOpen()!=null
				&&lotterySet.getIsLCQSSCOpen()==1&&bizSystemConfigVO.getIsLCQSSCOpen()==1){
			lotterySetMsg.setIsLCQSSCOpen(1);
		}
		
		if(lotterySet.getIsAHKSOpen()!=null&&bizSystemConfigVO.getIsAHKSOpen()!=null
				&&lotterySet.getIsAHKSOpen()==1&&bizSystemConfigVO.getIsAHKSOpen()==1){
			lotterySetMsg.setIsAHKSOpen(1);
		}
		
		if(lotterySet.getIsAHTBOpen()!=null&&bizSystemConfigVO.getIsAHTBOpen()!=null
				   &&lotterySet.getIsAHTBOpen()==1&&bizSystemConfigVO.getIsAHTBOpen()==1){
			 lotterySetMsg.setIsAHTBOpen(1);
		}
		
		if(lotterySet.getIsBJKSOpen()!=null&&bizSystemConfigVO.getIsBJKSOpen()!=null
				   &&lotterySet.getIsBJKSOpen()==1&&bizSystemConfigVO.getIsBJKSOpen()==1){
			 lotterySetMsg.setIsBJKSOpen(1);
		}
		
		if(lotterySet.getIsJYKSOpen()!=null&&bizSystemConfigVO.getIsJYKSOpen()!=null
				   &&lotterySet.getIsJYKSOpen()==1&&bizSystemConfigVO.getIsJYKSOpen()==1){
			 lotterySetMsg.setIsJYKSOpen(1);
		}
		
		if(lotterySet.getIsXYFTOpen()!=null&&bizSystemConfigVO.getIsXYFTOpen()!=null
				   &&lotterySet.getIsXYFTOpen()==1&&bizSystemConfigVO.getIsXYFTOpen()==1){
			 lotterySetMsg.setIsXYFTOpen(1);
		}
		
		if(lotterySet.getIsSFPK10Open()!=null&&bizSystemConfigVO.getIsSFPK10Open()!=null
				&&lotterySet.getIsSFPK10Open()==1&&bizSystemConfigVO.getIsSFPK10Open()==1){
			lotterySetMsg.setIsSFPK10Open(1);
		}
		
		if(lotterySet.getIsWFPK10Open()!=null&&bizSystemConfigVO.getIsWFPK10Open()!=null
				&&lotterySet.getIsWFPK10Open()==1&&bizSystemConfigVO.getIsWFPK10Open()==1){
			lotterySetMsg.setIsWFPK10Open(1);
		}
		
		if(lotterySet.getIsSHFPK10Open()!=null&&bizSystemConfigVO.getIsSHFPK10Open()!=null
				&&lotterySet.getIsSHFPK10Open()==1&&bizSystemConfigVO.getIsSHFPK10Open()==1){
			lotterySetMsg.setIsSHFPK10Open(1);
		}
		
		if(lotterySet.getIsBJPK10Open()!=null&&bizSystemConfigVO.getIsBJPK10Open()!=null
				&&lotterySet.getIsBJPK10Open()==1&&bizSystemConfigVO.getIsBJPK10Open()==1){
			lotterySetMsg.setIsBJPK10Open(1);
		}

		if(lotterySet.getIsJSPK10Open()!=null&&bizSystemConfigVO.getIsJSPK10Open()!=null
				   &&lotterySet.getIsJSPK10Open()==1&&bizSystemConfigVO.getIsJSPK10Open()==1){
			 lotterySetMsg.setIsJSPK10Open(1);
		}
		
		if(lotterySet.getIsBJTBOpen()!=null&&bizSystemConfigVO.getIsBJTBOpen()!=null
				   &&lotterySet.getIsBJTBOpen()==1&&bizSystemConfigVO.getIsBJTBOpen()==1){
			 lotterySetMsg.setIsBJTBOpen(1);
		}
		
		if(lotterySet.getIsCQSSCOpen()!=null&&bizSystemConfigVO.getIsCQSSCOpen()!=null
				   &&lotterySet.getIsCQSSCOpen()==1&&bizSystemConfigVO.getIsCQSSCOpen()==1){
			 lotterySetMsg.setIsCQSSCOpen(1);
		}
		
		if(lotterySet.getIsCQSYXWOpen()!=null&&bizSystemConfigVO.getIsCQSYXWOpen()!=null
				   &&lotterySet.getIsCQSYXWOpen()==1&&bizSystemConfigVO.getIsCQSYXWOpen()==1){
			 lotterySetMsg.setIsCQSYXWOpen(1);
		}
		
		if(lotterySet.getIsFC3DDPCOpen()!=null&&bizSystemConfigVO.getIsFC3DDPCOpen()!=null
				   &&lotterySet.getIsFC3DDPCOpen()==1&&bizSystemConfigVO.getIsFC3DDPCOpen()==1){
			lotterySetMsg.setIsFC3DDPCOpen(1);
		}
		
		if(lotterySet.getIsFJSYXWOpen()!=null&&bizSystemConfigVO.getIsFJSYXWOpen()!=null
				   &&lotterySet.getIsFJSYXWOpen()==1&&bizSystemConfigVO.getIsFJSYXWOpen()==1){
			 lotterySetMsg.setIsFJSYXWOpen(1);
		}
		
		if(lotterySet.getIsGDKLSFOpen()!=null&&bizSystemConfigVO.getIsGDKLSFOpen()!=null
				   &&lotterySet.getIsGDKLSFOpen()==1&&bizSystemConfigVO.getIsGDKLSFOpen()==1){
			 lotterySetMsg.setIsGDKLSFOpen(1);
		}
		if(lotterySet.getIsWFSYXWOpen()!=null&&bizSystemConfigVO.getIsWFSYXWOpen()!=null
				   &&lotterySet.getIsWFSYXWOpen()==1&&bizSystemConfigVO.getIsWFSYXWOpen()==1){
			 lotterySetMsg.setIsWFSYXWOpen(1);
		}
		if(lotterySet.getIsSFSYXWOpen()!=null&&bizSystemConfigVO.getIsSFSYXWOpen()!=null
				   &&lotterySet.getIsSFSYXWOpen()==1&&bizSystemConfigVO.getIsSFSYXWOpen()==1){
			 lotterySetMsg.setIsSFSYXWOpen(1);
		}
		if(lotterySet.getIsGDSYXWOpen()!=null&&bizSystemConfigVO.getIsGDSYXWOpen()!=null
				   &&lotterySet.getIsGDSYXWOpen()==1&&bizSystemConfigVO.getIsGDSYXWOpen()==1){
			 lotterySetMsg.setIsGDSYXWOpen(1);
		}

		if(lotterySet.getIsHBKSOpen()!=null&&bizSystemConfigVO.getIsHBKSOpen()!=null
				   &&lotterySet.getIsHBKSOpen()==1&&bizSystemConfigVO.getIsHBKSOpen()==1){
			 lotterySetMsg.setIsHBKSOpen(1);
		}
		
		if(lotterySet.getIsHBTBOpen()!=null&&bizSystemConfigVO.getIsHBTBOpen()!=null
				   &&lotterySet.getIsHBTBOpen()==1&&bizSystemConfigVO.getIsHBTBOpen()==1){
			 lotterySetMsg.setIsHBTBOpen(1);
		}
		
		if(lotterySet.getIsHGFFCOpen()!=null&&bizSystemConfigVO.getIsHGFFCOpen()!=null
				   &&lotterySet.getIsHGFFCOpen()==1&&bizSystemConfigVO.getIsHGFFCOpen()==1){
			 lotterySetMsg.setIsHGFFCOpen(1);
		}
		
		
		if(lotterySet.getIsHLJSSCOpen()!=null&&bizSystemConfigVO.getIsHLJSSCOpen()!=null
				   &&lotterySet.getIsHLJSSCOpen()==1&&bizSystemConfigVO.getIsHLJSSCOpen()==1){
			 lotterySetMsg.setIsHLJSSCOpen(1);
		}	
		
		if(lotterySet.getIsJLFFCOpen()!=null&&bizSystemConfigVO.getIsJLFFCOpen()!=null
				   &&lotterySet.getIsJLFFCOpen()==1&&bizSystemConfigVO.getIsJLFFCOpen()==1){
			 lotterySetMsg.setIsJLFFCOpen(1);
		}	
		
		if(lotterySet.getIsJLKSOpen()!=null&&bizSystemConfigVO.getIsJLKSOpen()!=null
				   &&lotterySet.getIsJLKSOpen()==1&&bizSystemConfigVO.getIsJLKSOpen()==1){
			 lotterySetMsg.setIsJLKSOpen(1);
		}
		
		if(lotterySet.getIsGSKSOpen()!=null&&bizSystemConfigVO.getIsGSKSOpen()!=null
				   &&lotterySet.getIsGSKSOpen()==1&&bizSystemConfigVO.getIsGSKSOpen()==1){
			 lotterySetMsg.setIsGSKSOpen(1);
		}
		
		if(lotterySet.getIsSHKSOpen()!=null&&bizSystemConfigVO.getIsSHKSOpen()!=null
				   &&lotterySet.getIsSHKSOpen()==1&&bizSystemConfigVO.getIsSHKSOpen()==1){
			 lotterySetMsg.setIsSHKSOpen(1);
		}
		
		if(lotterySet.getIsJLTBOpen()!=null&&bizSystemConfigVO.getIsJLTBOpen()!=null
				   &&lotterySet.getIsJLTBOpen()==1&&bizSystemConfigVO.getIsJLTBOpen()==1){
			 lotterySetMsg.setIsJLTBOpen(1);
		}	
		
		if(lotterySet.getIsJSKSOpen()!=null&&bizSystemConfigVO.getIsJSKSOpen()!=null
				   &&lotterySet.getIsJSKSOpen()==1&&bizSystemConfigVO.getIsJSKSOpen()==1){
			 lotterySetMsg.setIsJSKSOpen(1);
		}	
		if(lotterySet.getIsGXKSOpen()!=null&&bizSystemConfigVO.getIsGXKSOpen()!=null
				   &&lotterySet.getIsGXKSOpen()==1&&bizSystemConfigVO.getIsGXKSOpen()==1){
			 lotterySetMsg.setIsGXKSOpen(1);
		}
		
		if(lotterySet.getIsJSTBOpen()!=null&&bizSystemConfigVO.getIsJSTBOpen()!=null
				   &&lotterySet.getIsJSTBOpen()==1&&bizSystemConfigVO.getIsJSTBOpen()==1){
			 lotterySetMsg.setIsJSTBOpen(1);
		}	
		
		if(lotterySet.getIsJXSSCOpen()!=null&&bizSystemConfigVO.getIsJXSSCOpen()!=null
				   &&lotterySet.getIsJXSSCOpen()==1&&bizSystemConfigVO.getIsJXSSCOpen()==1){
			 lotterySetMsg.setIsJXSSCOpen(1);
		}	
		
		if(lotterySet.getIsJXSYXWOpen()!=null&&bizSystemConfigVO.getIsJXSYXWOpen()!=null
				   &&lotterySet.getIsJXSYXWOpen()==1&&bizSystemConfigVO.getIsJXSYXWOpen()==1){
			 lotterySetMsg.setIsJXSYXWOpen(1);
		}	
		
		if(lotterySet.getIsSDSYXWOpen()!=null&&bizSystemConfigVO.getIsSDSYXWOpen()!=null
				   &&lotterySet.getIsSDSYXWOpen()==1&&bizSystemConfigVO.getIsSDSYXWOpen()==1){
			 lotterySetMsg.setIsSDSYXWOpen(1);
		}
		
		if(lotterySet.getIsSHSSLDPCOpen()!=null&&bizSystemConfigVO.getIsSHSSLDPCOpen()!=null
				   &&lotterySet.getIsSHSSLDPCOpen()==1&&bizSystemConfigVO.getIsSHSSLDPCOpen()==1){
			 lotterySetMsg.setIsSHSSLDPCOpen(1);
		}
		
		if(lotterySet.getIsTJSSCOpen()!=null&&bizSystemConfigVO.getIsTJSSCOpen()!=null
				   &&lotterySet.getIsTJSSCOpen()==1&&bizSystemConfigVO.getIsTJSSCOpen()==1){
			 lotterySetMsg.setIsTJSSCOpen(1);
		}
		
		if(lotterySet.getIsTWWFCOpen()!=null&&bizSystemConfigVO.getIsTWWFCOpen()!=null
				   &&lotterySet.getIsTWWFCOpen()==1&&bizSystemConfigVO.getIsTWWFCOpen()==1){
			 lotterySetMsg.setIsTWWFCOpen(1);
		}
		
		if(lotterySet.getIsXJPLFCOpen()!=null&&bizSystemConfigVO.getIsXJPLFCOpen()!=null
				   &&lotterySet.getIsXJPLFCOpen()==1&&bizSystemConfigVO.getIsXJPLFCOpen()==1){
			 lotterySetMsg.setIsXJPLFCOpen(1);
		}
		
		if(lotterySet.getIsXJSSCOpen()!=null&&bizSystemConfigVO.getIsXJSSCOpen()!=null
				   &&lotterySet.getIsXJSSCOpen()==1&&bizSystemConfigVO.getIsXJSSCOpen()==1){
			 lotterySetMsg.setIsXJSSCOpen(1);
		}
		
		if(lotterySet.getIsLHCOpen()!=null&&bizSystemConfigVO.getIsLHCOpen()!=null
				   &&lotterySet.getIsLHCOpen()==1&&bizSystemConfigVO.getIsLHCOpen()==1){
			 lotterySetMsg.setIsLHCOpen(1);
		}
		if(lotterySet.getIsXYEBOpen()!=null&&bizSystemConfigVO.getIsXYEBOpen()!=null
				   &&lotterySet.getIsXYEBOpen()==1&&bizSystemConfigVO.getIsXYEBOpen()==1){
			 lotterySetMsg.setIsXYEBOpen(1);
		}
		if(lotterySet.getIsXYLHCOpen()!=null&&bizSystemConfigVO.getIsXYLHCOpen()!=null
				   &&lotterySet.getIsXYLHCOpen()==1&&bizSystemConfigVO.getIsXYLHCOpen()==1){
			 lotterySetMsg.setIsXYLHCOpen(1);
		}
		return lotterySetMsg;
		
	}
	
	public  LotterySetMsg compareLotterySetALL(LotterySet lotterySet,BizSystemConfigVO bizSystemConfigVO,BizSystemInfo bizSystemInfo){
		LotterySetMsg lotterySetMsgOld=compareLotterySetAndBizSystemConfigVO(lotterySet,bizSystemConfigVO);
		LotterySetMsg lotterySetMsg=new LotterySetMsg();
		
		if(bizSystemInfo==null){
			return lotterySetMsgOld;
		}
		String kindsStr=bizSystemInfo.getHotLotteryKinds();	
		if(kindsStr==null||"".equals(kindsStr)){
			return lotterySetMsgOld;
		}
		
		if(lotterySetMsgOld.getIsSHFSSCOpen()!=null&&kindsStr.contains("SHFSSC")
		   &&lotterySetMsgOld.getIsSHFSSCOpen()==1){
			lotterySetMsg.setIsSHFSSCOpen(1);
		}
		
		if(lotterySetMsgOld.getIsSFSSCOpen()!=null&&kindsStr.contains("SFSSC")
				&&lotterySetMsgOld.getIsSFSSCOpen()==1){
			lotterySetMsg.setIsSFSSCOpen(1);
		}
		if(lotterySetMsgOld.getIsWFSSCOpen()!=null&&kindsStr.contains("WFSSC")
				&&lotterySetMsgOld.getIsWFSSCOpen()==1){
			lotterySetMsg.setIsWFSSCOpen(1);
		}
		if(lotterySetMsgOld.getIsLCQSSCOpen()!=null&&kindsStr.contains("LCQSSC")
				&&lotterySetMsgOld.getIsLCQSSCOpen()==1){
			lotterySetMsg.setIsLCQSSCOpen(1);
		}
		if(lotterySetMsgOld.getIsSFKSOpen()!=null&&kindsStr.contains("SFKS")
				&&lotterySetMsgOld.getIsSFKSOpen()==1){
			lotterySetMsg.setIsSFKSOpen(1);
		}
		if(lotterySetMsgOld.getIsWFKSOpen()!=null&&kindsStr.contains("WFKS")
				&&lotterySetMsgOld.getIsWFKSOpen()==1){
			lotterySetMsg.setIsWFKSOpen(1);
		}
		if(lotterySetMsgOld.getIsAHKSOpen()!=null&&kindsStr.contains("AHKS")
				&&lotterySetMsgOld.getIsAHKSOpen()==1){
			lotterySetMsg.setIsAHKSOpen(1);
		}
		if(lotterySetMsgOld.getIsGXKSOpen()!=null&&kindsStr.contains("GXKS")
				   &&lotterySetMsgOld.getIsGXKSOpen()==1){
					lotterySetMsg.setIsGXKSOpen(1);
		}
		if(lotterySetMsgOld.getIsAHTBOpen()!=null&&kindsStr.contains("AHTB")
				   &&lotterySetMsgOld.getIsAHTBOpen()==1){
			 lotterySetMsg.setIsAHTBOpen(1);
		}
		
		if(lotterySetMsgOld.getIsBJKSOpen()!=null&&kindsStr.contains("BJKS")
				   &&lotterySetMsgOld.getIsBJKSOpen()==1){
			 lotterySetMsg.setIsBJKSOpen(1);
		}
		
		if(lotterySetMsgOld.getIsBJKSOpen()!=null&&kindsStr.contains("BJKS")
				   &&lotterySetMsgOld.getIsBJKSOpen()==1){
			 lotterySetMsg.setIsBJKSOpen(1);
		}
		
		if(lotterySetMsgOld.getIsJYKSOpen()!=null&&kindsStr.contains("JYKS")
				   &&lotterySetMsgOld.getIsJYKSOpen()==1){
			 lotterySetMsg.setIsJYKSOpen(1);
		}
		
		if(lotterySetMsgOld.getIsGSKSOpen()!=null&&kindsStr.contains("GSKS")
				   &&lotterySetMsgOld.getIsGSKSOpen()==1){
			 lotterySetMsg.setIsGSKSOpen(1);
		}
		
		if(lotterySetMsgOld.getIsSHKSOpen()!=null&&kindsStr.contains("SHKS")
				   &&lotterySetMsgOld.getIsSHKSOpen()==1){
			 lotterySetMsg.setIsSHKSOpen(1);
		}
		
		if(lotterySetMsgOld.getIsXYFTOpen()!=null&&kindsStr.contains("XYFT")
				   &&lotterySetMsgOld.getIsXYFTOpen()==1){
			 lotterySetMsg.setIsXYFTOpen(1);
		}
		
		if(lotterySetMsgOld.getIsSFPK10Open()!=null&&kindsStr.contains("SFPK10")
				&&lotterySetMsgOld.getIsSFPK10Open()==1){
			lotterySetMsg.setIsSFPK10Open(1);
		}
		
		if(lotterySetMsgOld.getIsWFPK10Open()!=null&&kindsStr.contains("WFPK10")
				&&lotterySetMsgOld.getIsWFPK10Open()==1){
			lotterySetMsg.setIsWFPK10Open(1);
		}
		
		if(lotterySetMsgOld.getIsSHFPK10Open()!=null&&kindsStr.contains("SHFPK10")
				&&lotterySetMsgOld.getIsSHFPK10Open()==1){
			lotterySetMsg.setIsSHFPK10Open(1);
		}
		
		if(lotterySetMsgOld.getIsBJPK10Open()!=null&&kindsStr.contains("BJPK10")
				&&lotterySetMsgOld.getIsBJPK10Open()==1){
			lotterySetMsg.setIsBJPK10Open(1);
		}

		if(lotterySetMsgOld.getIsJSPK10Open()!=null&&kindsStr.contains("JSPK10")
				   &&lotterySetMsgOld.getIsJSPK10Open()==1){
			 lotterySetMsg.setIsJSPK10Open(1);
		}
		
		if(lotterySetMsgOld.getIsBJTBOpen()!=null&&kindsStr.contains("BJTB")
				   &&lotterySetMsgOld.getIsBJTBOpen()==1){
			 lotterySetMsg.setIsBJTBOpen(1);
		}
		
		if(lotterySetMsgOld.getIsCQSSCOpen()!=null&&kindsStr.contains("CQSSC")
				   &&lotterySetMsgOld.getIsCQSSCOpen()==1){
			 lotterySetMsg.setIsCQSSCOpen(1);
		}
		
		if(lotterySetMsgOld.getIsCQSYXWOpen()!=null&&kindsStr.contains("CQSYXW")
				   &&lotterySetMsgOld.getIsCQSYXWOpen()==1){
			 lotterySetMsg.setIsCQSYXWOpen(1);
		}
		
		if(lotterySetMsgOld.getIsFC3DDPCOpen()!=null&&kindsStr.contains("FCSDDPC")
				   &&lotterySetMsgOld.getIsFC3DDPCOpen()==1){
			lotterySetMsg.setIsFC3DDPCOpen(1);
		}
		if(lotterySetMsgOld.getIsWFSYXWOpen()!=null&&kindsStr.contains("WFSYXW")
				   &&lotterySetMsgOld.getIsWFSYXWOpen()==1){
			 lotterySetMsg.setIsWFSYXWOpen(1);
		}
		if(lotterySetMsgOld.getIsSFSYXWOpen()!=null&&kindsStr.contains("SFSYXW")
				   &&lotterySetMsgOld.getIsSFSYXWOpen()==1){
			 lotterySetMsg.setIsSFSYXWOpen(1);
		}
		if(lotterySetMsgOld.getIsFJSYXWOpen()!=null&&kindsStr.contains("FJSYXW")
				   &&lotterySetMsgOld.getIsFJSYXWOpen()==1){
			 lotterySetMsg.setIsFJSYXWOpen(1);
		}
		
		if(lotterySetMsgOld.getIsGDKLSFOpen()!=null&&kindsStr.contains("GDKLSF")
				   &&lotterySetMsgOld.getIsGDKLSFOpen()==1){
			 lotterySetMsg.setIsGDKLSFOpen(1);
		}
		
		if(lotterySetMsgOld.getIsGDSYXWOpen()!=null&&kindsStr.contains("GDSYXW")
				   &&lotterySetMsgOld.getIsGDSYXWOpen()==1){
			 lotterySetMsg.setIsGDSYXWOpen(1);
		}

		if(lotterySetMsgOld.getIsHBKSOpen()!=null&&kindsStr.contains("HBKS")
				   &&lotterySetMsgOld.getIsHBKSOpen()==1){
			 lotterySetMsg.setIsHBKSOpen(1);
		}
		
		if(lotterySetMsgOld.getIsHBTBOpen()!=null&&kindsStr.contains("HBTB")
				   &&lotterySetMsgOld.getIsHBTBOpen()==1){
			 lotterySetMsg.setIsHBTBOpen(1);
		}
		
		if(lotterySetMsgOld.getIsHGFFCOpen()!=null&&kindsStr.contains("HGFFC")
				   &&lotterySetMsgOld.getIsHGFFCOpen()==1){
			 lotterySetMsg.setIsHGFFCOpen(1);
		}
		
		
		if(lotterySetMsgOld.getIsHLJSSCOpen()!=null&&kindsStr.contains("HLJSSC")
				   &&lotterySetMsgOld.getIsHLJSSCOpen()==1){
			 lotterySetMsg.setIsHLJSSCOpen(1);
		}	
		
		if(lotterySetMsgOld.getIsJLFFCOpen()!=null&&kindsStr.contains("JLFFC")
				   &&lotterySetMsgOld.getIsJLFFCOpen()==1){
			 lotterySetMsg.setIsJLFFCOpen(1);
		}	
		
		if(lotterySetMsgOld.getIsJLKSOpen()!=null&&kindsStr.contains("JLKS")
				   &&lotterySetMsgOld.getIsJLKSOpen()==1){
			 lotterySetMsg.setIsJLKSOpen(1);
		}	
		if(lotterySetMsgOld.getIsJLTBOpen()!=null&&kindsStr.contains("JLTB")
				   &&lotterySetMsgOld.getIsJLTBOpen()==1){
			 lotterySetMsg.setIsJLTBOpen(1);
		}	
		
		if(lotterySetMsgOld.getIsJSKSOpen()!=null&&kindsStr.contains("JSKS")
				   &&lotterySetMsgOld.getIsJSKSOpen()==1){
			 lotterySetMsg.setIsJSKSOpen(1);
		}	
	
		if(lotterySetMsgOld.getIsJSTBOpen()!=null&&kindsStr.contains("JSTB")
				   &&lotterySetMsgOld.getIsJSTBOpen()==1){
			 lotterySetMsg.setIsJSTBOpen(1);
		}	
		
		if(lotterySetMsgOld.getIsJXSSCOpen()!=null&&kindsStr.contains("JXSSC")
				   &&lotterySetMsgOld.getIsJXSSCOpen()==1){
			 lotterySetMsg.setIsJXSSCOpen(1);
		}	
		
		if(lotterySetMsgOld.getIsJXSYXWOpen()!=null&&kindsStr.contains("JXSYXW")
				   &&lotterySetMsgOld.getIsJXSYXWOpen()==1){
			 lotterySetMsg.setIsJXSYXWOpen(1);
		}	
		
		if(lotterySetMsgOld.getIsSDSYXWOpen()!=null&&kindsStr.contains("SDSYXW")
				   &&lotterySetMsgOld.getIsSDSYXWOpen()==1){
			 lotterySetMsg.setIsSDSYXWOpen(1);
		}
		
		if(lotterySetMsgOld.getIsSHSSLDPCOpen()!=null&&kindsStr.contains("SHSSLDPC")
				   &&lotterySetMsgOld.getIsSHSSLDPCOpen()==1){
			 lotterySetMsg.setIsSHSSLDPCOpen(1);
		}
		
		if(lotterySetMsgOld.getIsTJSSCOpen()!=null&&kindsStr.contains("TJSSC")
				   &&lotterySetMsgOld.getIsTJSSCOpen()==1&&bizSystemConfigVO.getIsTJSSCOpen()==1){
			 lotterySetMsg.setIsTJSSCOpen(1);
		}
		
		if(lotterySetMsgOld.getIsTWWFCOpen()!=null&&kindsStr.contains("TWWFC")
				   &&lotterySetMsgOld.getIsTWWFCOpen()==1){
			 lotterySetMsg.setIsTWWFCOpen(1);
		}
		
		if(lotterySetMsgOld.getIsXJPLFCOpen()!=null&&kindsStr.contains("XJPLFC")
				   &&lotterySetMsgOld.getIsXJPLFCOpen()==1){
			 lotterySetMsg.setIsXJPLFCOpen(1);
		}
		
		if(lotterySetMsgOld.getIsXJSSCOpen()!=null&&kindsStr.contains("XJSSC")
				   &&lotterySetMsgOld.getIsXJSSCOpen()==1){
			 lotterySetMsg.setIsXJSSCOpen(1);
		}
		if(lotterySetMsgOld.getIsLHCOpen()!=null&&kindsStr.contains("LHC")
				   &&lotterySetMsgOld.getIsLHCOpen()==1){
			 lotterySetMsg.setIsLHCOpen(1);
		}
		
		if(lotterySetMsgOld.getIsXYLHCOpen() !=null&&kindsStr.contains("XYLHC")
				   &&lotterySetMsgOld.getIsXYLHCOpen()==1){
			 lotterySetMsg.setIsXYLHCOpen(1);
		}
		
		if(lotterySetMsgOld.getIsXYEBOpen()!=null&&kindsStr.contains("XYEB")
				   &&lotterySetMsgOld.getIsXYEBOpen()==1){
			 lotterySetMsg.setIsXYEBOpen(1);
		}
		return lotterySetMsg;
		
	}
	
}
