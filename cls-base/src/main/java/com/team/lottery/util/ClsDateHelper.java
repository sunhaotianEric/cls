package com.team.lottery.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 计算时间工具类
 *
 */
public class ClsDateHelper {

	private static Logger logger = LoggerFactory.getLogger(ClsDateHelper.class);

	/**
	 * 获取正常的开始时间.
	 * 
	 * @param dayNumber
	 * @return
	 */
	public static Date getRangeStartTime(int dayNumber) {
		Calendar currentDate = new GregorianCalendar();
		Date currentDateTime = currentDate.getTime();

		if (dayNumber == 1) {
			// 获取今天的开始时间,今天日期00时00分00秒
			currentDate.set(Calendar.HOUR_OF_DAY, 0);
			currentDate.set(Calendar.MINUTE, 0);
			currentDate.set(Calendar.SECOND, 0);
			currentDate.set(Calendar.MILLISECOND, 0);
			return currentDate.getTime();
		} else if (dayNumber == -1) {
			// 昨天开始时间.00时00分00秒开始
			currentDate.add(Calendar.DATE, -1);
			currentDate.set(Calendar.HOUR_OF_DAY, 0);
			currentDate.set(Calendar.MINUTE, 0);
			currentDate.set(Calendar.SECOND, 0);
			currentDate.set(Calendar.MILLISECOND, 0);
			return currentDate.getTime();
		} else if (dayNumber == -7) {
			// 今天减去6天,然后加上00时00分00秒
			currentDate.add(Calendar.DATE, -6);
			currentDate.set(Calendar.HOUR_OF_DAY, 0);
			currentDate.set(Calendar.MINUTE, 0);
			currentDate.set(Calendar.SECOND, 0);
			currentDate.set(Calendar.MILLISECOND, 0);
			return currentDate.getTime();
		} else if (dayNumber == 30) {
			// 获取本月正常时间的1号,然后加上00时00分00秒
			currentDate.set(Calendar.DAY_OF_MONTH, 1);
			currentDate.set(Calendar.HOUR_OF_DAY, 0);
			currentDate.set(Calendar.MINUTE, 0);
			currentDate.set(Calendar.SECOND, 0);
			currentDate.set(Calendar.MILLISECOND, 0);
			return currentDate.getTime();
		} else if (dayNumber == -30) {
			// 获取上个月1号,加上00时00分00秒
			currentDate.add(Calendar.MONTH, -1);
			currentDate.set(Calendar.DAY_OF_MONTH, 1);
			currentDate.set(Calendar.HOUR_OF_DAY, 0);
			currentDate.set(Calendar.MINUTE, 0);
			currentDate.set(Calendar.SECOND, 0);
			currentDate.set(Calendar.MILLISECOND, 0);
			return currentDate.getTime();
		} else {
			logger.error("获取报表开始时间错误,当前传递值:{}", dayNumber);
			return currentDateTime;
		}
	}

	/**
	 * 获取正常的结束时间.
	 * 
	 * @param dayNumber
	 * @return
	 */
	public static Date getRangeEndTime(int dayNumber) {
		Calendar currentDate = new GregorianCalendar();
		Date currentDateTime = currentDate.getTime();
		if (dayNumber == 1) {
			// 获取今天的结束时间23:59:59
			currentDate.set(Calendar.HOUR_OF_DAY, 23);
			currentDate.set(Calendar.MINUTE, 59);
			currentDate.set(Calendar.SECOND, 59);
			currentDate.set(Calendar.MILLISECOND, 999);
			return currentDate.getTime();
		} else if (dayNumber == -1) {
			// 获取昨天的结束时间23:59:59
			currentDate.add(Calendar.DATE, -1);
			currentDate.set(Calendar.HOUR_OF_DAY, 23);
			currentDate.set(Calendar.MINUTE, 59);
			currentDate.set(Calendar.SECOND, 59);
			currentDate.set(Calendar.MILLISECOND, 999);
			return currentDate.getTime();
		} else if (dayNumber == -7) {
			// 获取今天的结束时间23:59:59
			currentDate.set(Calendar.HOUR_OF_DAY, 23);
			currentDate.set(Calendar.MINUTE, 59);
			currentDate.set(Calendar.SECOND, 59);
			currentDate.set(Calendar.MILLISECOND, 999);
			return currentDate.getTime();
		} else if (dayNumber == 30) {
			// 设置本月底时间.
			currentDate.add(Calendar.MONTH, 1);
			currentDate.set(Calendar.DAY_OF_MONTH, 1);
			currentDate.add(Calendar.DATE, -1);
			currentDate.set(Calendar.HOUR_OF_DAY, 23);
			currentDate.set(Calendar.MINUTE, 59);
			currentDate.set(Calendar.SECOND, 59);
			currentDate.set(Calendar.MILLISECOND, 999);
			return currentDate.getTime();
		} else if (dayNumber == -30) {
			// 设置上月底时间.
			currentDate.set(Calendar.DAY_OF_MONTH, 1);
			currentDate.add(Calendar.DATE, -1);
			currentDate.set(Calendar.HOUR_OF_DAY, 23);
			currentDate.set(Calendar.MINUTE, 59);
			currentDate.set(Calendar.SECOND, 59);
			currentDate.set(Calendar.MILLISECOND, 999);
			return currentDate.getTime();
		} else {
			logger.error("获取报表结束时间错误,当前传递值:{}", dayNumber);
			return currentDateTime;
		}

	}

	/**
	 * 获取报表开始时间
	 * 
	 * @param dayNumber
	 *            1 今天 -1 昨天 7 最近7天 30 本月 -30 上月
	 * @return
	 */
	public static Date getClsRangeStartTime(int dayNumber) {
		Calendar currentDate = new GregorianCalendar();
		Date currentDateTime = currentDate.getTime();
		int hour = currentDate.get(Calendar.HOUR_OF_DAY);
		Date starteDate = getClsStartTime(currentDateTime);
		// 今天
		if (dayNumber == 1) {
			if (hour > 2) {
				// 3点之后的时间取当前时间
				return starteDate;
			} else {
				// 0-3点之间的起始时间取昨天的
				starteDate = DateUtil.addDaysToDate(starteDate, -1);
				return starteDate;
			}
			// 昨天
		} else if (dayNumber == -1) {
			if (hour > 2) {
				// 3点之后的时间取当前时间
				starteDate = DateUtil.addDaysToDate(starteDate, -1);
				return starteDate;
			} else {
				// 0-3点之间的起始时间取昨天的
				starteDate = DateUtil.addDaysToDate(starteDate, -2);
				return starteDate;
			}
			// 最近7天
		} else if (dayNumber == -7) {
			if (hour > 2) {
				// 3点之后的时间取当前时间
				starteDate = DateUtil.addDaysToDate(starteDate, -6);
				return starteDate;
			} else {
				// 0-3点之间的起始时间取昨天的
				starteDate = DateUtil.addDaysToDate(starteDate, -7);
				return starteDate;
			}
			// 本月
		} else if (dayNumber == 30) {
			// 取本月1号 03:00:00
			currentDate.set(Calendar.DAY_OF_MONTH, 1);
			currentDate.set(Calendar.HOUR_OF_DAY, 3);
			currentDate.set(Calendar.MINUTE, 0);
			currentDate.set(Calendar.SECOND, 0);
			currentDate.set(Calendar.MILLISECOND, 0);
			return currentDate.getTime();
			// 上月
		} else if (dayNumber == -30) {
			// 取上个月1号 03:00:00
			currentDate.add(Calendar.MONTH, -1);
			currentDate.set(Calendar.DAY_OF_MONTH, 1);
			currentDate.set(Calendar.HOUR_OF_DAY, 3);
			currentDate.set(Calendar.MINUTE, 0);
			currentDate.set(Calendar.SECOND, 0);
			currentDate.set(Calendar.MILLISECOND, 0);
			return currentDate.getTime();
		} else {
			logger.error("获取报表开始时间错误,当前传递值:{}", dayNumber);
			return currentDateTime;
		}
	}

	/**
	 * 获取报表结束时间
	 * 
	 * @param dayNumber
	 *            1 今天 -1 昨天 -7 最近7天 30 本月 -30 上月
	 * @return
	 */
	public static Date getClsRangeEndTime(int dayNumber) {
		Calendar currentDate = new GregorianCalendar();
		Date currentDateTime = currentDate.getTime();
		int hour = currentDate.get(Calendar.HOUR_OF_DAY);
		Date endDate = getClsEndTime(currentDateTime);
		// 今天
		if (dayNumber == 1) {
			if (hour > 2) {
				// 3点之后的时间取当前时间
				return endDate;
			} else {
				// 0-3点之间的起始时间取昨天的
				endDate = DateUtil.addDaysToDate(endDate, -1);
				return endDate;
			}
			// 昨天
		} else if (dayNumber == -1) {
			if (hour > 2) {
				// 3点之后的时间取当前时间
				endDate = DateUtil.addDaysToDate(endDate, -1);
				return endDate;
			} else {
				// 0-3点之间的起始时间取昨天的
				endDate = DateUtil.addDaysToDate(endDate, -2);
				return endDate;
			}
			// 最近7天
		} else if (dayNumber == -7) {
			if (hour > 2) {
				// 3点之后的时间取当前时间
				return endDate;
			} else {
				// 0-3点之间的起始时间取昨天的
				endDate = DateUtil.addDaysToDate(endDate, -1);
				return endDate;
			}
			// 本月
		} else if (dayNumber == 30) {
			// 取下个月1号 02:59:59
			currentDate.add(Calendar.MONTH, 1);
			currentDate.set(Calendar.DAY_OF_MONTH, 1);
			currentDate.set(Calendar.HOUR_OF_DAY, 2);
			currentDate.set(Calendar.MINUTE, 59);
			currentDate.set(Calendar.SECOND, 59);
			currentDate.set(Calendar.MILLISECOND, 999);
			return currentDate.getTime();
			// 上月
		} else if (dayNumber == -30) {
			// 取本月1号 02:59：59
			currentDate.set(Calendar.DAY_OF_MONTH, 1);
			currentDate.set(Calendar.HOUR_OF_DAY, 2);
			currentDate.set(Calendar.MINUTE, 59);
			currentDate.set(Calendar.SECOND, 59);
			currentDate.set(Calendar.MILLISECOND, 999);
			return currentDate.getTime();
		} else {
			logger.error("获取报表结束时间错误,当前传递值:{}", dayNumber);
			return currentDateTime;
		}
	}

	/**
	 * 获取当前日期的 03:00:00
	 * 
	 * @param date
	 * @return
	 */
	public static Date getClsStartTime(Date date) {
		Calendar currentDate = new GregorianCalendar();
		currentDate.setTime(date);
		currentDate.set(Calendar.HOUR_OF_DAY, 3);
		currentDate.set(Calendar.MINUTE, 0);
		currentDate.set(Calendar.SECOND, 0);
		currentDate.set(Calendar.MILLISECOND, 0);
		return currentDate.getTime();
	}

	/**
	 * 获取当前日期第二天的 02:59:59
	 * 
	 * @param date
	 * @return
	 */
	public static Date getClsEndTime(Date date) {
		Calendar currentDate = new GregorianCalendar();
		currentDate.setTime(date);
		currentDate.add(Calendar.DATE, 1);
		currentDate.set(Calendar.HOUR_OF_DAY, 2);
		currentDate.set(Calendar.MINUTE, 59);
		currentDate.set(Calendar.SECOND, 59);
		currentDate.set(Calendar.MILLISECOND, 999);
		return currentDate.getTime();
	}

	/**
	 * 获取当前日期的 12:00:00
	 * 
	 * @param date
	 * @return
	 */
	public static Date getClsMiddleTime(Date date) {
		Calendar currentDate = new GregorianCalendar();
		currentDate.setTime(date);
		currentDate.set(Calendar.HOUR_OF_DAY, 12);
		currentDate.set(Calendar.MINUTE, 0);
		currentDate.set(Calendar.SECOND, 0);
		currentDate.set(Calendar.MILLISECOND, 0);
		return currentDate.getTime();
	}

	/**
	 * 获取报表最晚出来的时间 05:00:00
	 * 
	 * @param date
	 * @return
	 */
	public static Date getClsReportLastTime(Date date) {
		Calendar currentDate = new GregorianCalendar();
		currentDate.setTime(date);
		currentDate.set(Calendar.HOUR_OF_DAY, 5);
		currentDate.set(Calendar.MINUTE, 0);
		currentDate.set(Calendar.SECOND, 0);
		currentDate.set(Calendar.MILLISECOND, 0);
		return currentDate.getTime();
	}

	public static void main(String[] args) {
		// 正常
		// 今天
		Date rangeStartTimeToday = ClsDateHelper.getRangeStartTime(1);
		Date rangeEndTimeToday = ClsDateHelper.getRangeEndTime(1);

		// 昨天
		Date rangeStartTimeYesterday = ClsDateHelper.getRangeStartTime(-1);
		Date rangeEndTimeYesterday = ClsDateHelper.getRangeEndTime(-1);

		// 最近七天
		Date rangeStartTimeWeek = ClsDateHelper.getRangeStartTime(-7);
		Date rangeEndTimeWeek = ClsDateHelper.getRangeEndTime(-7);

		// 本月
		Date rangeStartTimeMonth = ClsDateHelper.getRangeStartTime(30);
		Date rangeEndTimeMonth = ClsDateHelper.getRangeEndTime(30);

		// 上月
		Date rangeStartTimeLastMonth = ClsDateHelper.getRangeStartTime(-30);
		Date rangeEndTimeLastMonth = ClsDateHelper.getRangeEndTime(-30);

		// 时间格式化
		SimpleDateFormat myFmt = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");

		System.out.println("-----------------------今天----------------------------");
		System.out.println("天开始时间:" + myFmt.format(rangeStartTimeToday));
		System.out.println("天结束时间:" + myFmt.format(rangeEndTimeToday));
		System.out.println("-----------------------昨天----------------------------");
		System.out.println("昨天开始时间:" + myFmt.format(rangeStartTimeYesterday));
		System.out.println("昨天结束时间:" + myFmt.format(rangeEndTimeYesterday));
		System.out.println("-----------------------最近7 天-------------------------");
		System.out.println("最近七天开始时间:" + myFmt.format(rangeStartTimeWeek));
		System.out.println("最近七天结束时间:" + myFmt.format(rangeEndTimeWeek));
		System.out.println("-----------------------本月----------------------------");
		System.out.println("本月开始时间:" + myFmt.format(rangeStartTimeMonth));
		System.out.println("本月结束时间:" + myFmt.format(rangeEndTimeMonth));
		System.out.println("-----------------------上月----------------------------");
		System.out.println("上月开始时间:" + myFmt.format(rangeStartTimeLastMonth));
		System.out.println("上月结束时间:" + myFmt.format(rangeEndTimeLastMonth));

		// 非今天
		Date clsRangeStartTimeToday = ClsDateHelper.getClsRangeStartTime(1);
		Date clsRangeEndTimeToday = ClsDateHelper.getClsRangeEndTime(1);

		// 昨天
		Date clsRangeStartTimeYesterday = ClsDateHelper.getClsRangeStartTime(-1);
		Date clsRangeEndTimeYesterday = ClsDateHelper.getClsRangeEndTime(-1);

		// 最近七天
		Date clsRangeStartTimeWeek = ClsDateHelper.getClsRangeStartTime(-7);
		Date clsRangeEndTimeWeek = ClsDateHelper.getClsRangeEndTime(-7);

		// 本月
		Date clsRangeStartTimeMonth = ClsDateHelper.getClsRangeStartTime(30);
		Date clsRangeEndTimeMonth = ClsDateHelper.getClsRangeEndTime(30);

		// 上月
		Date clsRangeStartTimeLastMonth = ClsDateHelper.getClsRangeStartTime(-30);
		Date clsRangeEndTimeLastMonth = ClsDateHelper.getClsRangeEndTime(-30);
		

		System.out.println("------------------------------------------------------------");
		System.out.println("------------------------------------------------------------");
		System.out.println("------------------------------------------------------------");
		System.out.println("------------------------------------------------------------");
		System.out.println("-----------------------非正常时间----------------------------");
		System.out.println("--------------------------今天-------------------------------");
		System.out.println("天开始时间:" + myFmt.format(clsRangeStartTimeToday));
		System.out.println("天结束时间:" + myFmt.format(clsRangeEndTimeToday));
		System.out.println("-----------------------昨天----------------------------");
		System.out.println("昨天开始时间:" + myFmt.format(clsRangeStartTimeYesterday));
		System.out.println("昨天结束时间:" + myFmt.format(clsRangeEndTimeYesterday));
		System.out.println("-----------------------最近7 天-------------------------");
		System.out.println("最近七天开始时间:" + myFmt.format(clsRangeStartTimeWeek));
		System.out.println("最近七天结束时间:" + myFmt.format(clsRangeEndTimeWeek));
		System.out.println("-----------------------本月----------------------------");
		System.out.println("本月开始时间:" + myFmt.format(clsRangeStartTimeMonth));
		System.out.println("本月结束时间:" + myFmt.format(clsRangeEndTimeMonth));
		System.out.println("-----------------------上月----------------------------");
		System.out.println("上月开始时间:" + myFmt.format(clsRangeStartTimeLastMonth));
		System.out.println("上月结束时间:" + myFmt.format(clsRangeEndTimeLastMonth));
	}

}
