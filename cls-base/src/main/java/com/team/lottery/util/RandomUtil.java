package com.team.lottery.util;

import java.util.Random;

public class RandomUtil {
	
	/**
	 * 获取几个位数的随机字符串
	 * @param count
	 * @return
	 */
	public static String getRandomStrByCount(int count){
		String reslut = "";
	    for(int i=0;i<count;i++) {
	    	reslut += (int)(10*(Math.random()));
	    }
	    return reslut;
	}
	
	/**
	 * 生成几位数的随机字母
	 * @param length
	 * @return
	 */
	public static String getRandomString(int length) { //length表示生成字符串的长度  
	    String base = "abcdefghijklmnopqrstuvwxyz";     
	    Random random = new Random();     
	    StringBuffer sb = new StringBuffer();     
	    for (int i = 0; i < length; i++) {     
	        int number = random.nextInt(base.length());     
	        sb.append(base.charAt(number));     
	    }     
	    return sb.toString();     
	 } 
	
	/**
	 * 生成几位数的随机字母和数字
	 * @param length
	 * @return
	 */
	public static String getRandom2String(int length) { //length表示生成字符串的长度  
	    String base = "abcdefghijklmnopqrstuvwxyz01234567890";     
	    Random random = new Random();     
	    StringBuffer sb = new StringBuffer();     
	    for (int i = 0; i < length; i++) {     
	        int number = random.nextInt(base.length());     
	        sb.append(base.charAt(number));     
	    }     
	    return sb.toString();     
	 } 
	
	/**
	 * 生成某个范围内的随机数
	 * @param min
	 * @param max
	 * @return
	 */
	public static int getRandom(int min, int max){
	    Random random = new Random();
	    int s = random.nextInt(max) % (max - min + 1) + min;
	    return s;

	}
	
	public static void main(String[] args) {
		;
		System.out.println(getRandom2String(6));
		System.out.println(getRandom2String(6));
		System.out.println(getRandom2String(6));
		System.out.println(getRandom2String(6));
		System.out.println(getRandom2String(6));
		System.out.println(getRandom2String(6));
		System.out.println(getRandom2String(6));
		System.out.println(getRandom2String(6));
		
	}
	
}
