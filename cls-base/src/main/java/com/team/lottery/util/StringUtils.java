package com.team.lottery.util;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * 字符串处理工具类。
 * 
 * @author huangchao
 * 
 */
public final class StringUtils {
	
	private static final String CHARSET_NAME = "UTF-8";

	/**
	 * 判断字符串是否为空。
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(CharSequence str) {
		return str == null || str.length() == 0;
	}

	/**
	 * 如果是空串，则返回null
	 * @param str
	 * @return
	 */
	public static String getNullForEmpty(String str) {
		if(isEmpty(str)){
			return null;
		}
		return str;
	}
	
	public static boolean isNotBlank(final CharSequence cs) {
        int strLen;
        if (cs == null || (strLen = cs.length()) == 0) {
            return false;
        }
        for (int i = 0; i < strLen; i++) {
            if (Character.isWhitespace(cs.charAt(i)) == false) {
                return true;
            }
        }
        return false;
    }
	
	/**
	 * 若对象为空返回<code>""</code>，否则调用{@link Object#toString()}方法。
	 * 
	 * @param obj
	 * @return
	 */
	public static String toString(Object obj) {
		return obj == null ? "" : obj.toString();
	}

	private StringUtils() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * 判断是否为数字
	 * @param str
	 * @return
	 */
	public static boolean isNumeric(String str){ 
	   Pattern pattern = Pattern.compile("[0-9]*"); 
	   Matcher isNum = pattern.matcher(str);
	   if( !isNum.matches() ){
	       return false; 
	   } 
	   return true; 
	}

	/**
	 * 从数字和字母种随机选取字符
	 * @param length
	 * @return
	 */
    public static String getRandomString(int length) {   
        StringBuffer buffer = new StringBuffer("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");   
        StringBuffer sb = new StringBuffer();   
        Random random = new Random();   
        int range = buffer.length();   
        for (int i = 0; i < length; i ++) {   
            sb.append(buffer.charAt(random.nextInt(range)));   
        }   
        return sb.toString();   
    } 
    
	/**
	 * 从数字选取字符
	 * @param length
	 * @return
	 */
    public static String getRandomInt(int length) {   
        StringBuffer buffer = new StringBuffer("0123456789");   
        StringBuffer sb = new StringBuffer();   
        Random random = new Random();   
        int range = buffer.length();   
        for (int i = 0; i < length; i ++) {   
            sb.append(buffer.charAt(random.nextInt(range)));   
        }   
        return sb.toString();   
    } 
    
	/**
	 * 从数字
	 * @param length
	 * @return
	 */
    public static String getRandomStringForEasy(int length) {   
        StringBuffer buffer = new StringBuffer("0123456789abcdefghijklmnopqrstuvwxyz");   
        StringBuffer sb = new StringBuffer();   
        Random random = new Random();   
        int range = buffer.length();   
        for (int i = 0; i < length; i ++) {   
            sb.append(buffer.charAt(random.nextInt(range)));   
        }   
        return sb.toString();   
    } 
    
    /**
     * 校验数组是否包含某个值
     * @param arrayParam
     * @param value
     * @return
     */
    public static boolean isArrayContain(String [] arrayParam, String value){
    	for(String param : arrayParam){
    		if(param.equals(value)){
    			return true;
    		}
    	}
    	return false;
    }
    
    /**
     * 获取数组包含某个数值的次数
     * @param arrayParam
     * @param value
     * @return
     */
    public static int containCount(String [] arrayParam, String value){
    	int result = 0;
    	for(String param : arrayParam){
    		if(param.equals(value)){
    			result++;
    		}
    	}
    	return result;
    }
    
    /**
     * 字符串包含子字符串的数目
     * @param sStr
     * @param qStr
     * @return
     */
    public static int stringNumbers(String sStr,String qStr){
    	int count = 0;
        if (sStr.indexOf(qStr)==-1){
            return 0;
        }else if(sStr.indexOf(qStr) != -1){
        	count++;
        	count = count + StringUtils.stringNumbers(sStr.substring(sStr.indexOf(qStr) + qStr.length()),qStr);
            return count;
        }
        return 0;
    }
    
    /**
	 * 返回以字符分隔的字符串
	 * @param list
	 * @param separator
	 * @return
	 */
	public static String listToString(List<String> list, char separator) {    
		StringBuilder sb = new StringBuilder();    
		for (int i = 0; i < list.size(); i++) {        
			sb.append(list.get(i)).append(separator);    
		}  
		if(sb.toString().length() > 0) {
			return sb.toString().substring(0,sb.toString().length()-1);
		} else {
			return sb.toString();
		}
	}
	
	/**
     * 转换为字节数组
     * @param str
     * @return
     */
    public static byte[] getBytes(String str){
    	if (str != null){
    		try {
				return str.getBytes(CHARSET_NAME);
			} catch (UnsupportedEncodingException e) {
				return null;
			}
    	}else{
    		return null;
    	}
    }
	
	/**
     * 转换为字节数组
     * @param str
     * @return
     */
    public static String toString(byte[] bytes){
    	try {
			return new String(bytes, CHARSET_NAME);
		} catch (UnsupportedEncodingException e) {
			return "";
		}
    }
	
	//表情符拦截
    public static String filterEmoji(String source,String type) { 
    	if(!StringUtils.isEmpty(source)){  
            return EmojiFilter.filterEmoji(source);
    	}else{
    	    return source; 
    	}
    } 
    
    /**
     * 去除空格换行!
     * @param str
     * @return
     */
    public static String replaceBlank(String str) {
        String dest = "";
        if (str!=null) {
            Pattern p = Pattern.compile("\\s*|\t|\r|\n");
            Matcher m = p.matcher(str);
            dest = m.replaceAll("");
        }
        return dest;
    }
    
    public static void main(String[] args) {
		System.out.println(stringNumbers("aabbaabdsabsaabaabaab787ana","aa"));
		List<String> aa = new ArrayList<String>();
		System.out.println(listToString(aa, ','));
	}
}
