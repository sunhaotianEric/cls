package com.team.lottery.util;

/**
 * sql注入攻击字符串拦截
 * @author Administrator
 *
 */
public class SqlFilteUtil {
	
	private static final String FLT = "'|and|exec|insert|select|delete|update|count|*|%|chr|mid|master|truncate|char|declare|;|or|+";

	public static String getSqlFilterStr(String content) {
		if(!StringUtils.isEmpty(content)){
			String filter[] = FLT.split("\\|");
	        for(int i=0;i < filter.length; i++){
	        	content = content.replace(filter[i], "");
	        }
			return content;
		}else{
			return content;
		}
	}
	
}
