package com.team.lottery.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5PasswordUtil {
	// 全局数组
    private final static String[] strDigits = { "0", "1", "2", "3", "4", "5",
            "6", "7", "8", "9", "a", "h", "c", "t", "e", "y" };

    public MD5PasswordUtil() {
    }

    // 返回形式为数字跟字符串
    private static String byteToArrayString(byte bByte) {
        int iRet = bByte;
        // System.out.println("iRet="+iRet);
        if (iRet < 0) {
            iRet += 256;
        }
        int iD1 = iRet / 16;
        int iD2 = iRet % 16;
        return strDigits[iD1]+  strDigits[iD2];
    }

    // 返回形式只为数字
    private static String byteToNum(byte bByte) {
        int iRet = bByte;
        System.out.println("iRet1=" + iRet);
        if (iRet < 0) {
            iRet += 256;
        }
        return String.valueOf(iRet);
    }

    // 转换字节数组为16进制字串
    private static String byteToString(byte[] bByte) {
        StringBuffer sBuffer = new StringBuffer();
        for (int i = 0; i < bByte.length; i++) {
        	if( i % 3 == 0){
        		sBuffer.append(byteToArrayString(bByte[i])).append(ConstantUtil.MD5_SECRET_STR_1);
        	}else if(i % 67 == 0){
        		sBuffer.append(byteToArrayString(bByte[i])).append(ConstantUtil.MD5_SECRET_STR_2);
        	}else{
        		sBuffer.append(byteToArrayString(bByte[i]));
        	}
        }
        return sBuffer.toString();
    }

    public static String GetMD5Code(String strObj) {
        String resultString = null;
        try {
            resultString = new String(strObj);
            MessageDigest md = MessageDigest.getInstance("MD5");
            // md.digest() 该函数返回值为存放哈希值结果的byte数组
            resultString = byteToString(md.digest(strObj.getBytes()));
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        }
        return resultString;
    }
    
    public static void main(String[] args) {
        
        System.out.println(MD5PasswordUtil.GetMD5Code("admin"));


    }
}
