package com.team.lottery.util;

import java.io.UnsupportedEncodingException;

import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Base64解密工具类
 * 
 * @author Owner
 *
 */
public class Base64Util {

	private static Logger log = LoggerFactory.getLogger(Base64Util.class);
	
	/***
	 * encode by Base64
	 */
/*	public static String encodeBase64(byte[] input) throws Exception {
		Class<?> clazz = Class.forName("com.sun.org.apache.xerces.internal.impl.dv.util.Base64");
		Method mainMethod = clazz.getMethod("encode", byte[].class);
		mainMethod.setAccessible(true);
		Object retObj = mainMethod.invoke(null, new Object[] { input });
		return (String) retObj;
	}*/

	/**
	 * 加密
	 * @param s
	 * @return
	 */
	public static String encodeStr(String s) {
		if (StringUtils.isEmpty(s)) {
			return null;
		}
		Base64 base64 = new Base64();
		String str = null;
		try {
			str = new String((byte[]) base64.encode(s), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			log.info(e.toString());
			return null;
		} catch (EncoderException e) {
			log.info(e.toString());
			return null;
		}

		return str;
	}

	/**
	 * 对BASE64进行解码
	 * 
	 * @param encode
	 * @return
	 */
	public static String getBASE64Code(String encode) {
		if (StringUtils.isEmpty(encode)) {
			return null;
		}
		Base64 base64 = new Base64();
		String decode = null;
		try {
			decode = new String(base64.decode(encode), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			log.info(e.toString());
			return null;
		}
		return decode;
	}
}
