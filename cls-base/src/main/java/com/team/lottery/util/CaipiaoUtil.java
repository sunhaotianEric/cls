package com.team.lottery.util;

public class CaipiaoUtil {

	/**
	 * 获取需要的html文本内容
	 * @param viewContent
	 * @return
	 */
	public static String getNeedBodyText(String viewContent){
		if(StringUtils.isEmpty(viewContent)){
			return null;
		}
		String htmlContent = viewContent.substring(viewContent.indexOf("<head>"), viewContent.indexOf("</html>"));
		String text = htmlContent.substring(htmlContent.indexOf("<body>") + "<body>".length(), htmlContent.lastIndexOf("</body>"));
		text = text.replaceAll("ssc","cqssc");
		text = text.replaceAll("width=\"997\"","width=\"1000\"");
		return text;
	}
	
	/**
	 * 获取需要的header文本内容
	 * @param viewContent
	 * @return
	 */
	public static String getNeedHeaderText(String viewContent){
		if(StringUtils.isEmpty(viewContent)){
			return null;
		}
		String htmlContent = viewContent.substring(viewContent.indexOf("<head>"), viewContent.indexOf("</html>"));
		String header = htmlContent.substring(htmlContent.indexOf("<head>") + "<head>".length(), htmlContent.lastIndexOf("</head>"));
		header = header.replaceFirst(header.substring(header.indexOf("<title>"), header.lastIndexOf("</title>") + "</title>".length()),"");
		header = header.replaceAll("javascript/", "js/");
		return header;
	}
}
