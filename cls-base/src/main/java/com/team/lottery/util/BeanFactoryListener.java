/**
 * 
 */
package com.team.lottery.util;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;


/**
 * BeanFactory初始化的监听器。主要用于注册Service和Dao。
 * 
 * @author wuzhb
 *
 */
public class BeanFactoryListener implements ServletContextListener {

	/* (non-Javadoc)
	 * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.ServletContextEvent)
	 */
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
	}

	/* (non-Javadoc)
	 * @see javax.servlet.ServletContextListener#contextInitialized(javax.servlet.ServletContextEvent)
	 */
	@Override
	public void contextInitialized(ServletContextEvent arg0) {

//		/************商品信息***********************/
//        BeanFactory.registerBean(GoodsDaoImpl.class);
//        BeanFactory.registerBean(GoodsServiceImpl.class);
//		
//		/************用户相关***********************/
//        BeanFactory.registerBean(UserMapper.class);
//        BeanFactory.registerBean(UserServiceImpl.class);
//        
//		/************活动相关***********************/
//        BeanFactory.registerBean(ActivityDaoImpl.class);
//        BeanFactory.registerBean(ActivityServiceImpl.class);
//        
//		/************员工相关***********************/
//        BeanFactory.registerBean(StaffDaoImpl.class);
//        BeanFactory.registerBean(StaffServiceImpl.class);
        

	}
}
