package com.team.lottery.util;

import java.math.BigDecimal;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.vo.AwardModelConfig;
import com.team.lottery.vo.Order;
import com.team.lottery.vo.User;

/**
 * 奖金模式,返点提成计算工具类
 * @author lxh
 *
 */
public class AwardModelUtil {
	
	/**
	 * 根据时时彩的模式控制其他彩种的模式
	 * @param sscRebate
	 * @param lotteryTopKind
	 */
	public static BigDecimal getModelBySsc(BigDecimal sscRebate, ELotteryTopKind lotteryTopKind){
		BigDecimal result = sscRebate;
		if(lotteryTopKind.equals(ELotteryTopKind.SSC)){
		}else if(lotteryTopKind.equals(ELotteryTopKind.FFC)){
			result = sscRebate.multiply((SystemConfigConstant.FFCHighestAwardModel)).divide(SystemConfigConstant.SSCHighestAwardModel,2).setScale(2);
		}else if(lotteryTopKind.equals(ELotteryTopKind.SYXW)){
			result = sscRebate.multiply(SystemConfigConstant.SYXWHighestAwardModel).divide(SystemConfigConstant.SSCHighestAwardModel,2).setScale(2);
		}else if(lotteryTopKind.equals(ELotteryTopKind.KS)){
			result = sscRebate.multiply(SystemConfigConstant.KSHighestAwardModel).divide(SystemConfigConstant.SSCHighestAwardModel,2).setScale(2);
		}else if(lotteryTopKind.equals(ELotteryTopKind.KLSF)){
			result = sscRebate.multiply(SystemConfigConstant.KLSFHighestAwardModel).divide(SystemConfigConstant.SSCHighestAwardModel,2).setScale(2);
		}else if(lotteryTopKind.equals(ELotteryTopKind.DPC)){
			result = sscRebate.multiply(SystemConfigConstant.DPCHighestAwardModel).divide(SystemConfigConstant.SSCHighestAwardModel,2).setScale(2);
		}else if(lotteryTopKind.equals(ELotteryTopKind.TB)){
			result = sscRebate.multiply(SystemConfigConstant.TBHighestAwardModel).divide(SystemConfigConstant.SSCHighestAwardModel,2).setScale(2);
		}else if(lotteryTopKind.equals(ELotteryTopKind.PK10)){
			result = sscRebate.multiply(SystemConfigConstant.PK10HighestAwardModel).divide(SystemConfigConstant.SSCHighestAwardModel,2).setScale(2);
		}else if(lotteryTopKind.equals(ELotteryTopKind.LHC)){
			result = sscRebate.multiply(SystemConfigConstant.LHCHighestAwardModel).divide(SystemConfigConstant.SSCHighestAwardModel,2).setScale(2);
		}else{
			throw new RuntimeException("该彩种的时时彩映射模式未配置");
		}
		/*
		 * 模式允许有小数点存在
		Integer intValue = result.intValue();
		if(intValue % 2 == 0){
		}else{
			intValue = intValue - 1;	
		}
		result = new BigDecimal(intValue);
		*/
		return result;
	}
	
	/**
	 * 根据时时彩模式设定用户其他彩种的对应的模式
	 * @param user
	 * @param sscrebate  时时彩模式
	 */
	public static void setUserRebateInfo(User user, BigDecimal sscrebate) {
		if(user == null){
			return;
		}
		//更新需要更新的用户字段
    	if(sscrebate != null){
    		user.setSscRebate(sscrebate);
    	}else{
    		user.setSscRebate(SystemConfigConstant.SSCLowestAwardModel);
    	}
    	
		//根据时时彩模式   自定义其他彩种的模式
		user.setFfcRebate(AwardModelUtil.getModelBySsc(sscrebate, ELotteryTopKind.FFC));
		user.setSyxwRebate(AwardModelUtil.getModelBySsc(sscrebate, ELotteryTopKind.SYXW));
		user.setKsRebate(AwardModelUtil.getModelBySsc(sscrebate, ELotteryTopKind.KS));
		user.setPk10Rebate(AwardModelUtil.getModelBySsc(sscrebate, ELotteryTopKind.PK10));
		user.setDpcRebate(AwardModelUtil.getModelBySsc(sscrebate, ELotteryTopKind.DPC));
		user.setTbRebate(AwardModelUtil.getModelBySsc(sscrebate, ELotteryTopKind.TB));
		user.setLhcRebate(AwardModelUtil.getModelBySsc(sscrebate, ELotteryTopKind.LHC));
		user.setKlsfRebate(AwardModelUtil.getModelBySsc(sscrebate, ELotteryTopKind.KLSF));
	}
	
	
	
	/**
	 * 用户选择奖金模式校验 
	 * @param lotteryKind
	 * @param toCheckAwardModel
	 * @return
	 */
	public static Object[] userAwardModelCheck(User newUser, User currentUser) {
		Object[] result = new Object[2];
		AwardModelConfig awardModelConfig = ClsCacheManager.getAwardModelConfigByCode(currentUser.getBizSystem());
		//时时彩校验
		if (newUser.getSscRebate().compareTo(awardModelConfig.getSscHighestAwardModel()) > 0) {
			result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
			result[1] = "超过系统设置的时时彩投注最高模式,当前模式不允许,请咨询客服.";
			return result;
		}
		if (newUser.getSscRebate().compareTo(awardModelConfig.getSscLowestAwardModel()) < 0
				|| newUser.getSscRebate().compareTo(currentUser.getSscRebate()) > 0) {
			result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
			result[1] = "时时彩奖金模式传递值越界,当前模式不允许,请咨询客服.";
			return result;
		}
		//分分彩校验
		if (newUser.getFfcRebate().compareTo(awardModelConfig.getFfcHighestAwardModel()) > 0) {
			result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
			result[1] = "超过系统设置的分分彩投注最高模式,当前模式不允许,请咨询客服.";
			return result;
		}
		if (newUser.getFfcRebate().compareTo(awardModelConfig.getFfcLowestAwardModel()) < 0
				|| newUser.getFfcRebate().compareTo(currentUser.getFfcRebate()) > 0) {
			result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
			result[1] = "分分彩奖金模式传递值越界,当前模式不允许,请咨询客服.";
			return result;
		}
		//十一选五校验
		if (newUser.getSyxwRebate().compareTo(awardModelConfig.getSyxwHighestAwardModel()) > 0) {
			result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
			result[1] = "超过系统设置的11选5彩投注最高模式,当前模式不允许,请咨询客服.";
			return result;
		}
		if (newUser.getSyxwRebate().compareTo(awardModelConfig.getSyxwLowestAwardModel()) < 0
				|| newUser.getSyxwRebate().compareTo(currentUser.getSyxwRebate()) > 0) {
			result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
			result[1] = "11选5奖金模式传递值越界,当前模式不允许,请咨询客服.";
			return result;
		}
		//快三校验
		if (newUser.getKsRebate().compareTo(awardModelConfig.getKsHighestAwardModel()) > 0) {
			result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
			result[1] = "超过系统设置的快3彩投注最高模式,当前模式不允许,请咨询客服.";
			return result;
		}
		if (newUser.getKsRebate().compareTo(awardModelConfig.getKsLowestAwardModel()) < 0
				|| newUser.getKsRebate().compareTo(currentUser.getKsRebate()) > 0) {
			result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
			result[1] = "快3奖金模式传递值越界,当前模式不允许,请咨询客服.";
			return result;
		}
		//PK10校验
		if (newUser.getPk10Rebate().compareTo(awardModelConfig.getPk10HighestAwardModel()) > 0) {
			result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
			result[1] = "超过系统设置的pk10模式彩投注最高模式,当前模式不允许,请咨询客服.";
			return result;
		}
		if (newUser.getPk10Rebate().compareTo(awardModelConfig.getPk10LowestAwardModel()) < 0
				|| newUser.getPk10Rebate().compareTo(currentUser.getPk10Rebate()) > 0) {
			result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
			result[1] = "pk10模式传递值越界,当前模式不允许,请咨询客服.";
			return result;
		}
		//低频彩校验
		if (newUser.getDpcRebate().compareTo(awardModelConfig.getDpcHighestAwardModel()) > 0) {
			result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
			result[1] = "超过系统设置的低频彩模式彩投注最高模式,当前模式不允许,请咨询客服.";
			return result;
		}
		if (newUser.getDpcRebate().compareTo(awardModelConfig.getDpcLowestAwardModel()) < 0
				|| newUser.getDpcRebate().compareTo(currentUser.getDpcRebate()) > 0) {
			result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
			result[1] = "低频彩模式传递值越界,当前模式不允许,请咨询客服.";
			return result;
		}
		//骰宝校验
		if (newUser.getTbRebate().compareTo(awardModelConfig.getTbHighestAwardModel()) > 0) {
			result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
			result[1] = "超过系统设置的骰宝投注最高模式,当前模式不允许,请咨询客服.";
			return result;
		}
		if (newUser.getTbRebate().compareTo(awardModelConfig.getTbLowestAwardModel()) < 0
				|| newUser.getTbRebate().compareTo(currentUser.getTbRebate()) > 0) {
			result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
			result[1] = "骰宝模式传递值越界,当前模式不允许,请咨询客服.";
			return result;
		}
		//六合彩校验
		if (newUser.getLhcRebate().compareTo(awardModelConfig.getLhcHighestAwardModel()) > 0) {
			result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
			result[1] = "超过系统设置的六合彩投注最高模式,当前模式不允许,请咨询客服.";
			return result;
		}
		if (newUser.getLhcRebate().compareTo(awardModelConfig.getLhcLowestAwardModel()) < 0
				|| newUser.getLhcRebate().compareTo(currentUser.getLhcRebate()) > 0) {
			result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
			result[1] = "六合彩模式传递值越界,当前模式不允许,请咨询客服.";
			return result;
		}
		//快乐十分校验
		if (newUser.getKlsfRebate().compareTo(awardModelConfig.getKlsfHighestAwardModel()) > 0) {
			result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
			result[1] = "超过系统设置的快乐十分模式彩投注最高模式,当前模式不允许,请咨询客服.";
			return result;
		}
		if (newUser.getKlsfRebate().compareTo(awardModelConfig.getKlsfLowestAwardModel()) < 0
				|| newUser.getKlsfRebate().compareTo(currentUser.getKlsfRebate()) > 0) {
			result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
			result[1] = "快乐十分模式传递值越界,当前模式不允许,请咨询客服.";
			return result;
		}
		
		
		result[0] = ConstantUtil.DWR_AJAX_RESULT_OK;
		return result;
	}
	
	/**
	 * 当前用户投注使用奖金模式校验 
	 * @param lotteryKind 彩种
	 * @param toCheckAwardModel 要校验使用的模式
	 * @return
	 */
	public static Object[] lotteryAwardModelCheck(User user, ELotteryKind lotteryKind, BigDecimal toCheckAwardModel){
		Object [] result = new Object[2];
		//根据彩种类型获取彩种大类
		ELotteryTopKind lotteryTopKind = AwardModelUtil.getLotteryTopKindByLotteryKind(lotteryKind);
		if(lotteryTopKind == null) {
			result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
			result[1] = "获取当前彩种大类失败，模式校验不通过";
    		return result; 
		}
		AwardModelConfig awardModelConfig = ClsCacheManager.getAwardModelConfigByCode(user.getBizSystem());
		
		if(lotteryTopKind.name().equals(ELotteryTopKind.SSC.name())){
		  if(toCheckAwardModel.compareTo(awardModelConfig.getSscHighestAwardModel()) > 0){
				result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
				result[1] = "超过系统设置的时时彩投注最高模式,当前模式不允许,请咨询客服.";
	    		return result; 
		  }
		  if(toCheckAwardModel.compareTo(awardModelConfig.getSscLowestAwardModel()) < 0  ||
					toCheckAwardModel.compareTo(user.getSscRebate()) > 0){
				result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
				result[1] = "时时彩奖金模式传递值越界,当前模式不允许,请咨询客服.";
	    		return result; 
			}
		}else if(lotteryTopKind.name().equals(ELotteryTopKind.FFC.name())){
			if(toCheckAwardModel.compareTo(awardModelConfig.getFfcHighestAwardModel()) > 0){
				result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
				result[1] = "超过系统设置的分分彩投注最高模式,当前模式不允许,请咨询客服.";
	    		return result; 
			}
			if(toCheckAwardModel.compareTo(awardModelConfig.getFfcLowestAwardModel()) < 0  ||
					toCheckAwardModel.compareTo(user.getFfcRebate()) > 0){
				result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
				result[1] = "分分彩奖金模式传递值越界,当前模式不允许,请咨询客服.";
	    		return result; 
			}
		}else if(lotteryTopKind.name().equals(ELotteryTopKind.SYXW.name())){
			if(toCheckAwardModel.compareTo(awardModelConfig.getSyxwHighestAwardModel()) > 0){
				result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
				result[1] = "超过系统设置的11选5投注最高模式,当前模式不允许,请咨询客服.";
	    		return result; 
			}
			if(toCheckAwardModel.compareTo(awardModelConfig.getSyxwLowestAwardModel()) < 0 ||
					toCheckAwardModel.compareTo(user.getSyxwRebate()) > 0){
				result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
				result[1] = "11选5奖金模式传递值越界,当前模式不允许,请咨询客服.";
	    		return result; 
			}
		}else if(lotteryTopKind.name().equals(ELotteryTopKind.KS.name())){
			if(toCheckAwardModel.compareTo(awardModelConfig.getKsHighestAwardModel()) > 0){
				result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
				result[1] = "超过系统设置的快3投注最高模式,当前模式不允许,请咨询客服.";
	    		return result; 
			}
			if(toCheckAwardModel.compareTo(awardModelConfig.getKsLowestAwardModel()) < 0 ||
					toCheckAwardModel.compareTo(user.getKsRebate()) > 0){
				result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
				result[1] = "快3奖金模式传递值越界,当前模式不允许,请咨询客服.";
	    		return result; 
			}
		}else if(lotteryTopKind.name().equals(ELotteryTopKind.PK10.name())){
			if(toCheckAwardModel.compareTo(awardModelConfig.getPk10HighestAwardModel()) > 0){
				result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
				result[1] = "超过系统设置的pk10投注最高模式,当前模式不允许,请咨询客服.";
	    		return result; 
			}
			if(toCheckAwardModel.compareTo(awardModelConfig.getPk10LowestAwardModel()) < 0 ||
					toCheckAwardModel.compareTo(user.getPk10Rebate()) > 0){
				result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
				result[1] = "pk10的模式传递值越界,当前模式不允许,请咨询客服.";
	    		return result; 
			}
		}else if(lotteryTopKind.name().equals(ELotteryTopKind.DPC.name())){
			if(toCheckAwardModel.compareTo(awardModelConfig.getDpcHighestAwardModel()) > 0){
				result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
				result[1] = "超过系统设置的低频彩最高模式,当前模式不允许,请咨询客服.";
	    		return result; 
			}
			if(toCheckAwardModel.compareTo(awardModelConfig.getDpcLowestAwardModel()) < 0 ||
					toCheckAwardModel.compareTo(user.getDpcRebate()) > 0){
				result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
				result[1] = "低频彩模式传递值越界,当前模式不允许,请咨询客服.";
	    		return result; 
			}
		}else if(lotteryTopKind.name().equals(ELotteryTopKind.TB.name())){
			if(toCheckAwardModel.compareTo(awardModelConfig.getTbHighestAwardModel()) > 0){
				result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
				result[1] = "超过系统设置的骰宝最高模式,当前模式不允许,请咨询客服.";
	    		return result; 
			}
			if(toCheckAwardModel.compareTo(awardModelConfig.getTbLowestAwardModel()) < 0 ||
					toCheckAwardModel.compareTo(user.getTbRebate()) > 0){
				result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
				result[1] = "骰宝传递值越界,当前模式不允许,请咨询客服.";
	    		return result; 
			}
		}else if(lotteryTopKind.name().equals(ELotteryTopKind.LHC.name())){
			if(toCheckAwardModel.compareTo(awardModelConfig.getTbHighestAwardModel()) > 0){
				result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
				result[1] = "超过系统设置的六合彩最高模式,当前模式不允许,请咨询客服.";
	    		return result; 
			}
			if(toCheckAwardModel.compareTo(awardModelConfig.getLhcLowestAwardModel()) < 0 ||
					toCheckAwardModel.compareTo(user.getLhcRebate()) > 0){
				result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
				result[1] = "六合彩模式传递值越界,当前模式不允许,请咨询客服.";
	    		return result; 
			}
		}else if(lotteryTopKind.name().equals(ELotteryTopKind.KLSF.name())){
			if(toCheckAwardModel.compareTo(awardModelConfig.getKlsfHighestAwardModel()) > 0){
				result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
				result[1] = "超过系统设置的快乐十分最高模式,当前模式不允许,请咨询客服.";
	    		return result; 
			}
			if(toCheckAwardModel.compareTo(awardModelConfig.getKlsfLowestAwardModel()) < 0 ||
					toCheckAwardModel.compareTo(user.getKlsfRebate()) > 0){
				result[0] = ConstantUtil.DWR_AJAX_RESULT_ERROR;
				result[1] = "快乐十分模式传递值越界,当前模式不允许,请咨询客服.";
	    		return result; 
			}
		}else if(lotteryTopKind.name().equals(ELotteryTopKind.XYEB.name())){
			
		}
    	result[0] = ConstantUtil.DWR_AJAX_RESULT_OK;
		return result; 
	}
	
	
	/**
	 * 计算订单返点或提成金额
	 * 如果regFromUser为空，则计算自身返点
	 * 如果regFromUser有值，则计算提成金额
	 * @param rebateMoneyValue
	 * @param order
	 * @param orderUser
	 */
	public static BigDecimal calculateOrderRebateMoney(Order order,User orderUser,User regFromUser){
		//返点金额
		BigDecimal rebateMoneyValue = BigDecimal.ZERO; 
		BigDecimal intervalRebate = null;
		//根据彩种类型获取彩种大类
		ELotteryKind lotteryKind = ELotteryKind.valueOf(order.getLotteryType());
		ELotteryTopKind lotteryTopKind = AwardModelUtil.getLotteryTopKindByLotteryKind(lotteryKind);
		if(lotteryKind == null) {
			throw new RuntimeException("未实现该彩种的返点");
		}
		BigDecimal lowRebate = BigDecimal.ZERO;
		BigDecimal highRebate = BigDecimal.ZERO;
		if(regFromUser != null){
			if(lotteryTopKind.equals(ELotteryTopKind.SSC)){
				lowRebate = orderUser.getSscRebate();
				highRebate = regFromUser.getSscRebate();
			}else if(lotteryTopKind.equals(ELotteryTopKind.FFC)){
				lowRebate = orderUser.getFfcRebate();
				highRebate = regFromUser.getFfcRebate();
			}else if(lotteryTopKind.equals(ELotteryTopKind.SYXW)){
				lowRebate = orderUser.getSyxwRebate();
				highRebate = regFromUser.getSyxwRebate();
			}else if(lotteryTopKind.equals(ELotteryTopKind.KS)){
				lowRebate = orderUser.getKsRebate();
				highRebate = regFromUser.getKsRebate();
			}else if(lotteryTopKind.equals(ELotteryTopKind.PK10)){
				lowRebate = orderUser.getPk10Rebate();
				highRebate = regFromUser.getPk10Rebate();
			}else if(lotteryTopKind.equals(ELotteryTopKind.DPC)){
				lowRebate = orderUser.getDpcRebate();
				highRebate = regFromUser.getDpcRebate();
			}else if(lotteryTopKind.equals(ELotteryTopKind.TB)){
				lowRebate = orderUser.getTbRebate();
				highRebate = regFromUser.getTbRebate();
			}else if(lotteryTopKind.equals(ELotteryTopKind.LHC)){
				lowRebate = orderUser.getLhcRebate();
				highRebate = regFromUser.getLhcRebate();
			}else if(lotteryTopKind.equals(ELotteryTopKind.KLSF)){
				lowRebate = orderUser.getKlsfRebate();
				highRebate = regFromUser.getKlsfRebate();
			}else if(lotteryTopKind.equals(ELotteryTopKind.XYEB)){
				lowRebate = BigDecimal.ZERO;
				highRebate = BigDecimal.ZERO;
			}else {
				throw new RuntimeException("未实现该彩种的返点");
			}
			//如果regFromUser有值，则计算提成金额
			intervalRebate = calculateRebateByAwardModelInterval(lowRebate, highRebate, lotteryTopKind);
		}else{
			lowRebate = order.getAwardModel();
			if(lotteryTopKind.equals(ELotteryTopKind.SSC)){
				highRebate = orderUser.getSscRebate();
			}else if(lotteryTopKind.equals(ELotteryTopKind.FFC)){
				highRebate = orderUser.getFfcRebate();
			}else if(lotteryTopKind.equals(ELotteryTopKind.SYXW)){
				highRebate = orderUser.getSyxwRebate();
			}else if(lotteryTopKind.equals(ELotteryTopKind.KS)){
				highRebate = orderUser.getKsRebate();
			}else if(lotteryTopKind.equals(ELotteryTopKind.PK10)){
				highRebate = orderUser.getPk10Rebate();
			}else if(lotteryTopKind.equals(ELotteryTopKind.DPC)){
				highRebate = orderUser.getDpcRebate();
			}else if(lotteryTopKind.equals(ELotteryTopKind.TB)){
				highRebate = orderUser.getTbRebate();
			}else if(lotteryTopKind.equals(ELotteryTopKind.LHC)){
				highRebate = orderUser.getLhcRebate();
			}else if(lotteryTopKind.equals(ELotteryTopKind.KLSF)){
				highRebate = orderUser.getKlsfRebate();
			}else if(lotteryTopKind.equals(ELotteryTopKind.XYEB)){
				lowRebate = BigDecimal.ZERO;
				highRebate = BigDecimal.ZERO;
			}else {
				throw new RuntimeException("未实现该彩种的返点");
			}
			//如果regFromUser为空，则计算自身返点
			intervalRebate = calculateRebateByAwardModelInterval(lowRebate, highRebate, lotteryTopKind);
		}
		
		rebateMoneyValue = order.getPayMoney().multiply(intervalRebate);
        return rebateMoneyValue;
	}
	
	
	
	
	/**
	 * 根据奖金模式差值和彩种大类来计算返点比例
	 * 返点比例值精度 控制三个小数点，即最低 0.1%
	 * @param lowRebate 高的奖金模式
	 * @param highRebate 低的奖金模式 
	 * @param topKind  对应的大彩种 
	 * @return
	 */
	public static BigDecimal calculateRebateByAwardModelInterval(BigDecimal lowRebate,BigDecimal highRebate,ELotteryTopKind topKind){
		BigDecimal intervalRebate = null;
		if(highRebate.compareTo(lowRebate) > 0){
			if(topKind.equals(ELotteryTopKind.SSC)){
				intervalRebate = highRebate.subtract(lowRebate).divide(SystemConfigConstant.SSCHighestAwardModel, ConstantUtil.BIGDECIMAL_SCAL, BigDecimal.ROUND_HALF_UP);
			}else if(topKind.equals(ELotteryTopKind.FFC)){
				intervalRebate = highRebate.subtract(lowRebate).divide(SystemConfigConstant.FFCHighestAwardModel, ConstantUtil.BIGDECIMAL_SCAL, BigDecimal.ROUND_HALF_UP);
			}else if(topKind.equals(ELotteryTopKind.SYXW)){
				intervalRebate = highRebate.subtract(lowRebate).divide(SystemConfigConstant.SYXWHighestAwardModel, ConstantUtil.BIGDECIMAL_SCAL, BigDecimal.ROUND_HALF_UP);
			}else if(topKind.equals(ELotteryTopKind.KS)){
				intervalRebate = highRebate.subtract(lowRebate).divide(SystemConfigConstant.KSHighestAwardModel, ConstantUtil.BIGDECIMAL_SCAL, BigDecimal.ROUND_HALF_UP);
			}else if(topKind.equals(ELotteryTopKind.PK10)){
				intervalRebate = highRebate.subtract(lowRebate).divide(SystemConfigConstant.PK10HighestAwardModel, ConstantUtil.BIGDECIMAL_SCAL, BigDecimal.ROUND_HALF_UP);
			}else if(topKind.equals(ELotteryTopKind.DPC)){
				intervalRebate = highRebate.subtract(lowRebate).divide(SystemConfigConstant.DPCHighestAwardModel, ConstantUtil.BIGDECIMAL_SCAL, BigDecimal.ROUND_HALF_UP);
			}else if(topKind.equals(ELotteryTopKind.TB)){
				intervalRebate = highRebate.subtract(lowRebate).divide(SystemConfigConstant.TBHighestAwardModel, ConstantUtil.BIGDECIMAL_SCAL, BigDecimal.ROUND_HALF_UP);
			}else if(topKind.equals(ELotteryTopKind.LHC)){
				intervalRebate = highRebate.subtract(lowRebate).divide(SystemConfigConstant.LHCHighestAwardModel, ConstantUtil.BIGDECIMAL_SCAL, BigDecimal.ROUND_HALF_UP);
			}else if(topKind.equals(ELotteryTopKind.KLSF)){
				intervalRebate = highRebate.subtract(lowRebate).divide(SystemConfigConstant.KLSFHighestAwardModel, ConstantUtil.BIGDECIMAL_SCAL, BigDecimal.ROUND_HALF_UP);
			}else if(topKind.equals(ELotteryTopKind.XYEB)){
				intervalRebate = BigDecimal.ZERO;
			}else{
	        	throw new RuntimeException("不存在该彩种的类型");
			}
		}else{
			intervalRebate = BigDecimal.ZERO;
		}
		return intervalRebate;
	}
	
	/**
	 * 根据彩种类型获取彩种大类
	 * @param lotteryKind
	 * @return
	 */
	public static ELotteryTopKind getLotteryTopKindByLotteryKind(ELotteryKind lotteryKind) {
		//时时彩
		if(lotteryKind.getCode().contains(ELotteryTopKind.SSC.name())){  
			return ELotteryTopKind.SSC;
		//分分彩
        }else if(lotteryKind.getCode().contains(ELotteryTopKind.FFC.name()) || 
        		lotteryKind.getCode().contains("LFC") || 
        		lotteryKind.getCode().contains("WFC")){ 
        	return ELotteryTopKind.FFC;
    	//11选5
        }else if(lotteryKind.getCode().contains(ELotteryTopKind.SYXW.name())){ 
        	return ELotteryTopKind.SYXW;
    	//快三
        }else if(lotteryKind.getCode().contains(ELotteryTopKind.KS.name())){ 
        	return ELotteryTopKind.KS;
    	//pk10
        }else if(lotteryKind.getCode().contains(ELotteryTopKind.PK10.name()) || lotteryKind.getCode().equals(ELotteryKind.XYFT.getCode())){ 
        	return ELotteryTopKind.PK10;
    	//低频彩
        }else if(lotteryKind.getCode().contains(ELotteryTopKind.DPC.name())){ 
        	return ELotteryTopKind.DPC;
        //骰宝
        }else if(lotteryKind.getCode().contains(ELotteryTopKind.TB.name())){ 
        	return ELotteryTopKind.TB;
        //六合彩
        }else if(lotteryKind.getCode().contains(ELotteryTopKind.LHC.name())){ 
        	return ELotteryTopKind.LHC;
        //快乐十分
        }else if(lotteryKind.getCode().contains(ELotteryTopKind.KLSF.name())){ 
        	return ELotteryTopKind.KLSF;
        //幸运28
        }else if(lotteryKind.getCode().contains(ELotteryTopKind.XYEB.name())){
        	return ELotteryTopKind.XYEB;
        }else{
        	return null;
        }
	}
	
	public static void main(String[] args) {
		String[] lotteryCodeArrays = "[TMA]1[55]{55}(1)$[TMA]17[66]{66}(1)$".split(ConstantUtil.SOURCECODE_PROTYPE_SPLIT);
		
		for(String lotteryCodeProtype : lotteryCodeArrays){
			int startPosition = lotteryCodeProtype.indexOf("[");
			int endPosition = lotteryCodeProtype.indexOf("]");
			String kindPlay = lotteryCodeProtype.substring(startPosition + 1,endPosition);
			
			int moneyStartPosition = lotteryCodeProtype.lastIndexOf("{");
			int moneyEndPosition =lotteryCodeProtype.lastIndexOf("}");
			String beishuStr = lotteryCodeProtype.substring(moneyStartPosition + 1,moneyEndPosition);
			String code = lotteryCodeProtype.substring(endPosition + 1, lotteryCodeProtype.lastIndexOf("["));
		   System.out.println(code);
		}
	
	}
}
