package com.team.lottery.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SSCNumberUtil {
	
	
	/**
	 * ***************************************斗牛方法集合 end*************************************************************
	 */
	/**
	 * 根据开奖号码集合得出斗牛号码集合
	 * @param openCode
	 * @return
	 * @throws Exception 
	 */
	public static List<Integer> convertDNOpenCode(String[] openCodes){
		List<Integer> convertCodeList = new ArrayList<Integer>();
        //先判断有牛和无牛
		boolean flag = isDevide10(openCodes);
		if(flag){//有牛，
			//判断是牛几
			int codeInt = (Integer.parseInt(openCodes[0]) + Integer.parseInt(openCodes[1]) + Integer.parseInt(openCodes[2])+ Integer.parseInt(openCodes[3])+ Integer.parseInt(openCodes[4]))%10; 
			//是否牛牛
			if(codeInt == 0){
				convertCodeList.add(10);
			}else {
				convertCodeList.add(codeInt);
			}
			//判断牛大，牛小
			if(codeInt == 0 || codeInt > 5){
				convertCodeList.add(11);
			}else{
				convertCodeList.add(12);
			}
			
			//判断牛单，牛双
			if(codeInt % 2 == 1){
				convertCodeList.add(13);
			}else{
				convertCodeList.add(14);
			}
		}else{
			convertCodeList.add(15);
		}
		
		return convertCodeList;
	}
		
	
	/**
	 * 判断拿其中三个数是否能被10整除
	 * @return
	 */
	private static boolean isDevide10(String [] openCodes){
		 int len = openCodes.length;  
	        for(int i =0; i < len; i++){ //依次遍历数组找出3个符合条件的数  
	            for(int j=i+1; j < len; j++){  
	                for(int k=j+1; k< len; k++){ 
	                	int temp = (Integer.parseInt(openCodes[i]) + Integer.parseInt(openCodes[j]) + Integer.parseInt(openCodes[k]))%10; 	
	                    if(temp == 0){
	                    	return true;
	                    }
	                }
	              }
	           }
	        return false;
	}
	/**
	 * ***************************************斗牛方法集合 end*************************************************************
	 */
	
	
	/**
	 * ***************************************前三，中三，后三方法集合 start*************************************************************
	 */
	
	/**
	 * 根据开奖号码集合得出三球号码集合
	 * @param openCode
	 * @return
	 * @throws Exception 
	 */
	public static List<Integer> convertSQOpenCode(String[] openCodes){
		List<Integer> convertCodeList = new ArrayList<Integer>();
		Integer opencodeInt = Integer.parseInt(openCodes[0]) + Integer.parseInt(openCodes[1]) +Integer.parseInt(openCodes[2]);
		//0-27号码
		convertCodeList.add(opencodeInt);
		
		//小(0-13)、大(14-27)
		if(opencodeInt >=0 && opencodeInt <=13){
			convertCodeList.add(28);
		}else{
			convertCodeList.add(29);
		}
		
		//单、双
		if(opencodeInt % 2 == 1){
			convertCodeList.add(30);
		}else{
			convertCodeList.add(31);
		}
       
		//0-3
		if(opencodeInt >=0 && opencodeInt <=3){
			convertCodeList.add(32);
		}
		//4-7
		if(opencodeInt >=4 && opencodeInt <=7){
			convertCodeList.add(33);
		}
		//8-11
		if(opencodeInt >=8 && opencodeInt <=11){
			convertCodeList.add(34);
		}
		//12-15
		if(opencodeInt >=12 && opencodeInt <=15){
			convertCodeList.add(35);
		}
		//16-19
		if(opencodeInt >=16 && opencodeInt <=19){
			convertCodeList.add(36);
		}
		//20-23
		if(opencodeInt >=20 && opencodeInt <=23){
			convertCodeList.add(37);
		}
		//24-27
		if(opencodeInt >=24 && opencodeInt <=27){
			convertCodeList.add(38);
		}
		//豹子
		if(isBaoZi(openCodes)){
			convertCodeList.add(39);
		}
		//顺子
		if(isShunZi(openCodes)){
			convertCodeList.add(40);
		}
		//对子
		if(isDuiZi(openCodes)){
			convertCodeList.add(41);
		}
		//半顺
		if(isBanShun(openCodes)){
			convertCodeList.add(42);
		}
		//杂六
		if(isZaLiu(openCodes)){
			convertCodeList.add(43);
		}
		return convertCodeList;
	}
	
	
	/**
	 * 判断在范围{0-9} 三个数是否豹子
	 * @return
	 */
	public static boolean isBaoZi(String [] openCodes){
		  int openCodeInt0 = Integer.parseInt(openCodes[0]);
		  int openCodeInt1 = Integer.parseInt(openCodes[1]);
		  int openCodeInt2 = Integer.parseInt(openCodes[2]);
		  if(openCodeInt0 == openCodeInt1 && openCodeInt0 == openCodeInt2){
			  return true ;
		  }
	        return false;
	}
	/**
	 * 判断在范围{0-9} 三个数是否顺子
	 * @return
	 */
	public static boolean isShunZi(String [] openCodes){
		  int openCodeInt0 = Integer.parseInt(openCodes[0]);
		  int openCodeInt1 = Integer.parseInt(openCodes[1]);
		  int openCodeInt2 = Integer.parseInt(openCodes[2]);
		  
		  int a[] = new int[]{openCodeInt0,openCodeInt1,openCodeInt2};
	       Arrays.sort(a);//从小到大
	       // 089,019
		  if((a[0] == 0 && a[2] == 9) && (a[1] == 8 || a[1] == 1)){
			  return true ;
		  }
		   openCodeInt0 = a[0];
		   openCodeInt1 = a[1];
		   openCodeInt2 = a[2];
		  if((Math.abs(openCodeInt0-openCodeInt1) == 1) && (Math.abs(openCodeInt1-openCodeInt2) == 1)){
			  return true ;
		  }
	        return false;
	}
	
	/**
	 * 判断在范围{0-9} 三个数是否对子(排除豹子)
	 * @return
	 */
	public static boolean isDuiZi(String [] openCodes){
		  int openCodeInt0 = Integer.parseInt(openCodes[0]);
		  int openCodeInt1 = Integer.parseInt(openCodes[1]);
		  int openCodeInt2 = Integer.parseInt(openCodes[2]);
		  //判断是否豹子
		  if(isBaoZi(openCodes)){
			  return false;  
		  }
		  //就三个可以一一列举判断
		  if((openCodeInt0 == openCodeInt1 && openCodeInt0 != openCodeInt2) || (openCodeInt0 == openCodeInt2 && openCodeInt0 != openCodeInt1) || (openCodeInt1 == openCodeInt2 && openCodeInt0 != openCodeInt1)){
			  return true ;
		  }
	        return false;
	}
	
	/**
	 * 判断在范围{0-9} 三个数是否半顺(排除豹子，顺子，对子)
	 * @return
	 */
	public static boolean isBanShun(String [] openCodes){
		  int openCodeInt0 = Integer.parseInt(openCodes[0]);
		  int openCodeInt1 = Integer.parseInt(openCodes[1]);
		  int openCodeInt2 = Integer.parseInt(openCodes[2]);
		  //判断是否豹子
		  if(isBaoZi(openCodes) || isDuiZi(openCodes) || isShunZi(openCodes)){
			  return false;  
		  }
		  int a[] = new int[]{openCodeInt0,openCodeInt1,openCodeInt2};
	       Arrays.sort(a);//从小到大
	       // 不等于 089,019
		  if((a[0] == 0 && a[2] == 9) && (a[1] != 8 && a[1] != 1)){
			  return true ;
		  }
		   openCodeInt0 = a[0];
		   openCodeInt1 = a[1];
		   openCodeInt2 = a[2];
		  if((Math.abs(openCodeInt0-openCodeInt1) == 1 ) || (Math.abs(openCodeInt1-openCodeInt2) == 1 )){
			  return true ;
		  }
	        return false;
	}
	
	/**
	 * 判断在范围{0-9} 三个数是否杂六(排除豹子，顺子，对子，半顺)
	 * @return
	 */
	public static boolean isZaLiu(String[] openCodes){
		  int openCodeInt0 = Integer.parseInt(openCodes[0]);
		  int openCodeInt1 = Integer.parseInt(openCodes[1]);
		  int openCodeInt2 = Integer.parseInt(openCodes[2]);
		  //判断是否豹子
		  if(isBaoZi(openCodes) || isDuiZi(openCodes) || isShunZi(openCodes) || isBanShun(openCodes)){
			  return false;  
		  }
		  
		  if((Math.abs(openCodeInt0-openCodeInt1) != 1 && Math.abs(openCodeInt0-openCodeInt1) != 9) || (Math.abs(openCodeInt1-openCodeInt2) != 1 || Math.abs(openCodeInt1-openCodeInt2) != 9)){
			  return true ;
		  }
	        return false;
	}
	
	
	/**
	 * ***************************************前三，中三，后三方法集合 end*************************************************************
	 */
	
	/**
	 * ***************************************五球，龙虎，梭哈方法集合 start*************************************************************
	 */
	
	/**
	 * 根据开奖号码集合得出五球，龙虎，梭哈号码集合
	 * @param openCode
	 * @return
	 * @throws Exception 
	 */
	public static List<Integer> convertWQOpenCode(String[] openCodes){
		List<Integer> convertCodeList = new ArrayList<Integer>();
		Integer opencodeInt = Integer.parseInt(openCodes[0]) + Integer.parseInt(openCodes[1]) +Integer.parseInt(openCodes[2]) +Integer.parseInt(openCodes[3]) +Integer.parseInt(openCodes[4]);
		//大小
		if(opencodeInt >=23 && opencodeInt <= 45){
			convertCodeList.add(1);
		}else{
			convertCodeList.add(2);
		}
		//单双
		if(opencodeInt % 2 ==1){
			convertCodeList.add(3);
		}else{
			convertCodeList.add(4);
		}
		//梭哈号码
		Integer convertCode = shuoHa(openCodes);
		if(convertCode != 100){
			convertCodeList.add(convertCode);
		}
		//龙虎和
		convertCode = longHu(openCodes);
		convertCodeList.add(convertCode);
		return convertCodeList;
		
	}
	
	
	
	
	/**
	 * 梭哈统计
	 * @param openCodes
	 * @return
	 */
	public static Integer shuoHa(String[] openCodes){
		  //统计 key = 号码值 ， value = 出现个数 
		  Map<String,Integer> openCodeMap =  new HashMap<String, Integer>();
		  List<Integer> openCodeList = new ArrayList<Integer>();
		  for(String openCode:openCodes){
			  if(openCodeMap.get(openCode) != null){
				  Integer openCodeCount = openCodeMap.get(openCode) + 1;
				  openCodeMap.put(openCode, openCodeCount);
			  }else{
				  Integer openCodeCount = 1;
				  openCodeMap.put(openCode, openCodeCount);
			  }
			  openCodeList.add(Integer.parseInt(openCode));
		  }
		  

		  //五条
		  if(openCodeMap.size() == 1){
			  return 5;  
		  }
		  Integer[] openCodeCountArr = new Integer[openCodeMap.size()];
		  int k = 0;
		  for(Map.Entry<String, Integer> entry : openCodeMap.entrySet()){
			  openCodeCountArr[k] = entry.getValue();
			  k++;
		  }
		  if(openCodeMap.size() == 2){
			  //四条
			  if((openCodeCountArr[0] ==4 && openCodeCountArr[1] ==1) || (openCodeCountArr[0] ==1 && openCodeCountArr[1] ==4) ){
				  return 6; 
			  }
			  //葫芦
			  if((openCodeCountArr[0] ==3 && openCodeCountArr[1] ==2) || (openCodeCountArr[0] ==2 && openCodeCountArr[1] ==3) ){
				  return 7; 
			  }  
		  }
		  
		  if(openCodeMap.size() == 3){
			  //三条
			  if((openCodeCountArr[0] ==3 && openCodeCountArr[1] ==1 && openCodeCountArr[2] ==1) || (openCodeCountArr[1] ==3 && openCodeCountArr[0] ==1 && openCodeCountArr[2] ==1) || (openCodeCountArr[2] ==3 && openCodeCountArr[0] ==1 && openCodeCountArr[1] ==1)){
				  return 9; 
			  }
			  //两对
			  if((openCodeCountArr[0] ==2 && openCodeCountArr[1] ==2 && openCodeCountArr[2] ==1) || (openCodeCountArr[1] ==2 && openCodeCountArr[2] ==2 && openCodeCountArr[0] ==1) || (openCodeCountArr[2] ==2 && openCodeCountArr[0] ==2 && openCodeCountArr[1] ==1)){
				  return 10; 
			  }  
		  }
		  
		  //一对
		  if(openCodeMap.size() == 4){
			  return 11;  
		  }
		  
		  
		  if(openCodeMap.size() == 5){
			  //排序 从小到大
			  Collections.sort(openCodeList, new Comparator<Integer>() {
					public int compare(Integer p1, Integer p2) {
						return p1.compareTo(p2);
					}
				});
			  //0,9特殊情况处理
			  if(openCodeList.get(0) == 0 && openCodeList.get(4) == 9){
				  int openCodeInt0 = openCodeList.get(0)+10;
				  int openCodeInt1 = openCodeList.get(1);
				  int openCodeInt2 = openCodeList.get(2);
				  int openCodeInt3 = openCodeList.get(3);
				  int openCodeInt4 = openCodeList.get(4);
				  openCodeList = new ArrayList<Integer>();
				  if(openCodeInt1<=3){
					  openCodeInt1 = openCodeInt1+10; 
				  }
				  if(openCodeInt2<=3){
					  openCodeInt2 = openCodeInt2+10;
				  }
				  if(openCodeInt3<=3){
					  openCodeInt3 = openCodeInt3+10;
				  }
				  openCodeList.add(openCodeInt0);
				  openCodeList.add(openCodeInt1);
				  openCodeList.add(openCodeInt2);
				  openCodeList.add(openCodeInt3);
				  openCodeList.add(openCodeInt4);
				  //重新排序 从小到大
				  Collections.sort(openCodeList, new Comparator<Integer>() {
						public int compare(Integer p1, Integer p2) {
							return p1.compareTo(p2);
						}
					});
			  }
			  
			  int count = 0 ;
			  Integer openTempInt = openCodeList.get(0);
			  //顺子
			 for(int i=1;i<openCodeList.size();i++){
				 if(Math.abs(openTempInt - openCodeList.get(i)) == 1){
					 openTempInt = openCodeList.get(i);
				 }else{
					 count++; 
				 } 
			 }
			 //count=0,顺子，否则散号
			 if(count == 0){
				 return 8;
			 }else{
				 return 12;
			 }	 
		  }
		   //返回100错误
		  return 100;	  
	}
	/**
	 * 龙虎统计
	 * @param openCodes
	 * @return
	 */
	public static Integer longHu(String[] openCodes){
		  int openCodeInt0 = Integer.parseInt(openCodes[0]);//万位
		  int openCodeInt4 = Integer.parseInt(openCodes[4]);//个位
		  if(openCodeInt0 > openCodeInt4){
			  return 13;
		  }else if(openCodeInt0 < openCodeInt4){
			  return 14;
		  }else{
			  return 15; 
		  }
	}
	/**
	 * ***************************************五球，龙虎，梭哈方法集合 end*************************************************************
	 */
//	/**
//	 * 斗牛出现概率计算
//	 * @param args
//	 */
//	public static void main(String[] args) {
//		Map<Integer,Integer> countMap= new HashMap<Integer,Integer>();
//		int kk =0;
//		countMap.put(1, 0);
//		countMap.put(2, 0);
//		countMap.put(3, 0);
//		countMap.put(4, 0);
//		countMap.put(5, 0);
//		countMap.put(6, 0);
//		countMap.put(7, 0);
//		countMap.put(8, 0);
//		countMap.put(9, 0);
//		countMap.put(10, 0);
//		countMap.put(11, 0);
//		countMap.put(12, 0);
//		countMap.put(13, 0);
//		countMap.put(14, 0);
//		countMap.put(15, 0);
//		for(int i=0;i<10;i++){
//			for(int j=0;j<10;j++){
//				for(int k=0;k<10;k++){
//					for(int l=0;l<10;l++){
//						for(int n=0;n<10;n++){
//							String[] openCodes = {""+i,""+j,""+k,""+l,""+n};
//							List<Integer> list = convertDNOpenCode(openCodes);
//							for(Integer ii:list){
//								countMap.put(ii, countMap.get(ii)+1);
//							}
//							System.out.println("kk="+kk++);
//						}	
//					}
//				}
//			}
//		}
//		
//		for (Map.Entry<Integer, Integer> entry : countMap.entrySet()) {  
//			  
//		    System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());  
//		  
//		} 
//	}
	
	
	/**
	 * 三球出现概率计算
	 * @param args
	 */
	public static void main(String[] args) {
		Map<Integer,Integer> countMap= new HashMap<Integer,Integer>();
		int kk =0;
		countMap.put(0, 0);
		countMap.put(1, 0);
		countMap.put(2, 0);
		countMap.put(3, 0);
		countMap.put(4, 0);
		countMap.put(5, 0);
		countMap.put(6, 0);
		countMap.put(7, 0);
		countMap.put(8, 0);
		countMap.put(9, 0);
		countMap.put(10, 0);
		countMap.put(11, 0);
		countMap.put(12, 0);
		countMap.put(13, 0);
		countMap.put(14, 0);
		countMap.put(15, 0);
		countMap.put(16, 0);
		countMap.put(17, 0);
		countMap.put(18, 0);
		countMap.put(19, 0);
		countMap.put(20, 0);
		countMap.put(21, 0);
		countMap.put(22, 0);
		countMap.put(23, 0);
		countMap.put(24, 0);
		countMap.put(25, 0);
		countMap.put(26, 0);
		countMap.put(27, 0);
		countMap.put(28, 0);
		countMap.put(29, 0);
		countMap.put(30, 0);
		countMap.put(31, 0);
		countMap.put(32, 0);
		countMap.put(33, 0);
		countMap.put(34, 0);
		countMap.put(35, 0);
		countMap.put(36, 0);
		countMap.put(37, 0);
		countMap.put(38, 0);
		countMap.put(39, 0);
		countMap.put(40, 0);
		countMap.put(41, 0);
		countMap.put(42, 0);
		countMap.put(43, 0);
		
		for(int i=0;i<10;i++){
			for(int j=0;j<10;j++){
				for(int k=0;k<10;k++){
							String[] openCodes = {""+i,""+j,""+k};
							List<Integer> list = convertSQOpenCode(openCodes);
							for(Integer ii:list){
								countMap.put(ii, countMap.get(ii)+1);
							}
							System.out.println("kk="+kk++);
				}
			}
		}
		
		for (Map.Entry<Integer, Integer> entry : countMap.entrySet()) {  
			  
		    System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());  
		  
		} 
	}
}
