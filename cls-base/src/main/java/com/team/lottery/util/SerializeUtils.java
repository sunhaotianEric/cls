package com.team.lottery.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SerializeUtils {
	private static Logger logger = LoggerFactory.getLogger(SerializeUtils.class);
	
	/**
	 * 序列化 返回字符串
	 * @param object
	 * @return
	 */
	public static String serialize2(Object object) {
	    ObjectOutputStream oos = null;
	    ByteArrayOutputStream baos = null;
	    try {
	        baos = new ByteArrayOutputStream();
	        oos = new ObjectOutputStream(baos);
	        oos.writeObject(object);
	      //  return baos.toString("ISO-8859-1");
	        return baos.toString("ISO-8859-1");
	    } catch (Exception e) {
	        logger.info("context", e);
	    }
	    return null;
	}

	/**
	 * 反序列化
	 * @param bytes
	 * @return
	 */
	public static Object unSerialize(byte[] bytes) {
	    ByteArrayInputStream bais = null;
	    try {
	        bais = new ByteArrayInputStream(bytes);
	        ObjectInputStream ois =new ObjectInputStream(bais);
	        return ois.readObject();
	    } catch (Exception e) {
	        logger.info("context", e);
	    }
	    return null;
	}

	
	public static void main(String[] args) throws UnsupportedEncodingException {
		String msg=serialize2("户口卡看");
		System.out.println("kkk="+msg);
		
		System.out.println("unSerialize"+unSerialize(msg.getBytes("ISO-8859-1")));
		
	}
}
