package com.team.lottery.util;

import java.math.BigDecimal;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;

public class AdminOperateUtil {
	private static final String fileName = "adminOperateLog.properties";
	private static Properties pro;
	
	static{
		String filePath = AdminOperateUtil.class.getClassLoader().getResource(fileName).getFile();
		pro = PropertiesUtil.getProperties(filePath); 
	}
	
	public static void main(String[] args) {	
		BigDecimal big1 = new BigDecimal("2");
		BigDecimal big2 = new BigDecimal("2.0");
        System.out.println(big1 == big2);
        
	}
	
	/**
	 * 根据key获得消息模型
	 * @param key
	 * @return
	 */
	public static String getModelMessageByKey(String key) {
		return pro.getProperty(key);
	}
	
	/**
	 * 根据key来构建消息
	 * @param modelMessage
	 * @param args
	 * @return
	 */
	public static String constructMessage(String key, String... args) {
		String modelMessage = "";
		if(pro != null) {
			modelMessage = pro.getProperty(key);
		}
		StringBuffer sb = new StringBuffer();
		if(StringUtils.isNotBlank(modelMessage)) {
			int index = modelMessage.indexOf("{");
			int i = 0;
			while(index != -1) {
				sb.append(modelMessage.substring(0, index));
				if(args != null && args.length > i) {
					if(StringUtils.isNotBlank(args[i])) {
						sb.append(args[i]);
					}
				}
				//截取后面的字符串继续处理
				modelMessage = modelMessage.substring(index + 2);
				index = modelMessage.indexOf("{");
				i++;
			}
			//加上剩余的字符串
			sb.append(modelMessage);
		}
		return sb.toString();
	}
}
