package com.team.lottery.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 日期工具类。
 * 
 * @author huangchao
 * 
 */
public final class DateUtils {

	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	
	private static SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	private static SimpleDateFormat sdf_date = new SimpleDateFormat("yyyy-MM-dd");
	
	private static String sdf2_str = "yyyy-MM-dd HH:mm:ss";

	/**
	* ISO标准的日期格式。
	* 
	*/
	private static SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");


	public static Long getOneDayMilli(){
		return 1000l*60*60*24;
	}

	/**
	 * 根据传入的时间参数加或者减时间
	 * 小时为单位
	 * @param time
	 * @return
	 */
	public static Date getTimeAddByParma(Date time,Integer hour){
		Calendar cal=Calendar.getInstance();
		cal.setTime(time);
		cal.add(Calendar.HOUR_OF_DAY,hour);
		return cal.getTime();
	}

	/**
	 * 根据日期字符串返回昨天的日期字符串
	 * @author listgoo
	 * @Description: 
	 * @date: 2019年3月31日
	 * @param dateStr
	 * @return
	 */
	public static String getYesDateStrByDateStr(String dateStr){
		 Date date1 = getDateByDateStr(dateStr);
		 Date date2 = getYesDateTime(date1);
		 String date2Str  = getDateToStrBySdfDate(date2);
	     String date2DateStr = date2Str.split(" ")[0];
	     return date2DateStr;
	}
	
	/**
	 * 字符串转成默认时间的Date
	 * @return
	 */
	public synchronized static Date getDateByDateStr(String dateStr){
		if(dateStr != null && !StringUtils.isEmpty(dateStr)){
			try {
				return sdf_date.parse(dateStr);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}
	
	/**
	 * 字符串转成默认时间
	 * @return
	 */
	public static Date getDateByStr(String dateStr){
		if(dateStr != null && !StringUtils.isEmpty(dateStr)){
			try {
				return sdf.parse(dateStr);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}
	
	/**
	 * 获取昨天的当前时间
	 * @date 2019年3月20日
	 * @param date
	 * @return
	 */
	public static Date getYesDateTime(Date date){
		if (date==null) {
			date = new Date();
		}
		Calendar cal=Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE,-1);
		return cal.getTime();
	}
	
	/**
	 * 字符串转成默认时间
	 * @return
	 */
	public static Date getDateByStr2(String dateStr){
		if(dateStr != null && !StringUtils.isEmpty(dateStr)){
			try {
				return sdf2.parse(dateStr);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}
	
	/**
	 * 字符串转成制定时间
	 * @return
	 */
	public static Date getDateByStrFormat(String dateStr,String format){
		if(dateStr != null && !StringUtils.isEmpty(dateStr)){
			try {
				SimpleDateFormat sdf = new SimpleDateFormat(format);
				return sdf.parse(dateStr);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}
	
	
	public static Date getTime(int hour, int minute, int second) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, hour);
		calendar.set(Calendar.MINUTE, minute);
		calendar.set(Calendar.SECOND, second);
		return calendar.getTime();
	}

	public static String getDateToStr(Date date) {
		if(date == null){
			return "";
		}
		return sdf.format(date);
	}
	
	/**
	 * 根据格式 获取日期的字符串 
	 * @return
	 */
	public static String getDateToStrByFormat(Date date,String format){
		if(date == null){
			return "";
		}
		SimpleDateFormat sdfFormat = new SimpleDateFormat(format);
		return sdfFormat.format(date);
	}
	
	
	/**
	 * 根据格式 获取日期的字符串 年月日-时分秒
	 * @return
	 */
	public static String getDateToStrBySdfDate(Date date){
		if(date == null){
			return "";
		}
		SimpleDateFormat sdfFormat = new SimpleDateFormat(sdf2_str);
		return sdfFormat.format(date);
	}
	

	/**
	 * 将日期格式化为ISO标准格式，以字符串输出。
	 * 
	 * @param sourceDate 原始日期
	 * @return 日期字符串
	 */
	public static String formatToISO(Date sourceDate) {
		return isoFormat.format(sourceDate);
	}

	/*
	 * public static void main(String[] args) { System.out.println(getTime(0, 0,
	 * 0)); System.out.println(getTime(23, 59, 59)); }
	 */
	private DateUtils() {
		// TODO Auto-generated constructor stub
	}

}
