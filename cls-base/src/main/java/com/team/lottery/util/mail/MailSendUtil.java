package com.team.lottery.util.mail;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.util.StringUtils;

public class MailSendUtil {

	public static Properties prop= null;
	private static Logger logger = LoggerFactory.getLogger(MailSendUtil.class);
	public static int [] mailArrs = {1,2,3,4,5,6};
	
	static {   
		prop = new Properties();   
        InputStream in = MailSendUtil.class.getResourceAsStream("/mail.properties");   
        try {   
            prop.load(in);   
        } catch (IOException e) {   
        	logger.error("读取mail.properties配置文件出现异常", e);     
        }   
    }   
	
	/**
	 * 随机获取下标
	 * @return
	 */
	private static int getRandIndex() {
		int index=(int)(Math.random()*mailArrs.length);
		int rand = mailArrs[index];	
		return rand;
	}
	
	/**
	 * 发送邮件,随机选取一个邮件发送者
	 */
	public static void sendMailByRand(String subject, String content) throws Exception{
		int rand = getRandIndex();
		sendMailByIndex(subject, content, rand);
	}
	
	/**
	 * 发送邮件,根据邮件配置下标作为邮件发送者
	 */
	public static void sendMailByIndex(String subject, String content, int rand) throws Exception{
		
		String sourceMailName = prop.getProperty("SOURCE_MAIL_"+rand+"_NAME");
		String sourceMailPwd = prop.getProperty("SOURCE_MAIL_"+rand+"_PWD");
		String sourceMailHost = prop.getProperty("SOURCE_MAIL_"+rand+"_HOST");
		//获取邮件接收者
		String targetMailName = prop.getProperty("TARGET_ALARM_MAIL_NAME");
		if(!StringUtils.isEmpty(targetMailName) && !StringUtils.isEmpty(sourceMailName) 
				&& !StringUtils.isEmpty(sourceMailPwd) && !StringUtils.isEmpty(sourceMailHost)) {
			MailSenderInfo mailInfo = new MailSenderInfo();    
			mailInfo.setMailServerHost(sourceMailHost);    
			mailInfo.setMailServerPort("25");    
			mailInfo.setValidate(true);    
			mailInfo.setUserName(sourceMailName);    
			mailInfo.setPassword(sourceMailPwd);//您的邮箱密码    
			mailInfo.setFromAddress(sourceMailName);    
			mailInfo.setToAddress(targetMailName);  
			
			mailInfo.setSubject(subject);    
			mailInfo.setContent(content);   
			
			//这个类主要来发送邮件   
			SimpleMailSender sms = new SimpleMailSender(); 
			boolean res = sms.sendTextMail(mailInfo);//发送文体格式    
			if(res) {
				logger.info("邮件发送成功,邮件发送者[{}]",sourceMailName);
			} else {
				logger.error("邮件发送失败,邮件发送者[{}],邮件发送密码[{}]",sourceMailName, sourceMailPwd);
				throw new Exception("邮件发送失败");
			}
		} else {
			throw new Exception("邮件发送失败,邮件配置信息不完整");
		}
		
	}
	
	
	public static void main(String[] args) throws Exception {
    	String result = StringUtils.getRandomStringForEasy(6);
    	MailSendUtil.sendMailByRand(result, result);
	}
}
