package com.team.lottery.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.beanutils.Converter;
/**
* 重写日期转换
*
* @author zzh
*/
public class DateConvert implements Converter{

	@Override
	public Object convert(Class type, Object value) {
		if(value instanceof java.util.Date){
			 return value;
		}
	     String p = (String) value;
	       if (p == null || p.trim().length() == 0) {
	           return null;
	       }
	       try {
	           SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	           return df.parse(p.trim());
	       } catch (Exception e) {
	           try {
	               SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	               return df.parse(p.trim());
	           } catch (ParseException ex) {
	               return null;
	           }
	       }
	}

}
