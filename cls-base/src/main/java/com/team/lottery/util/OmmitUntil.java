package com.team.lottery.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.team.lottery.extvo.LotteryCodeBjpk10ZstVo;
import com.team.lottery.extvo.LotteryCodeDpcZstVo;
import com.team.lottery.extvo.LotteryCodeFfcZstVo;
import com.team.lottery.extvo.LotteryCodeKsZstVo;
import com.team.lottery.extvo.LotteryCodeSscZstVo;
import com.team.lottery.extvo.LotteryCodeSyxwZstVo;
import com.team.lottery.vo.LotteryCode;
/**
 * 走势遗漏
 * @author Administrator
 *
 */
public class OmmitUntil {
	
	
	/**
	 * 初始化快三遗漏值
	 * @param lotteryCodes
	 * @return
	 */
	public static List<LotteryCodeKsZstVo> getKsOmmit(List<LotteryCode> lotteryCodes) {
		if(lotteryCodes == null){
			lotteryCodes = new ArrayList<LotteryCode>();
		}
		List<LotteryCodeKsZstVo> list =new ArrayList<LotteryCodeKsZstVo>();	
    	String [] numsCurrent = new String[]{"1","2","3","4","5","6"};
    	String [] hznumsCurrent = new String[]{"3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18"};
    	String [] hzxtnumsCurrent = new String[]{"1","2","3","4"};
    	String [] hmxtnumsCurrent = new String[]{"1","2","3","4","5","6"};
    	
    	
    	//当前遗漏
    	HashMap<String, Integer> numMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> hzNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> hzxtNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> hmxtNumMateCurrent = new HashMap<String, Integer>();
    	
    	for(String num : numsCurrent){
    		numMateCurrent.put(num, 0);
    		hmxtNumMateCurrent.put(num, 0);
    	}
    	
    	for(String num : hznumsCurrent){
    		hzNumMateCurrent.put(num, 0);
    	}
    	
    	for(String num : hzxtnumsCurrent){
    		hzxtNumMateCurrent.put(num, 0);
    	}
    	    	
    	for(int i = 0; i < lotteryCodes.size(); i++){
    		LotteryCode lotteryCode = lotteryCodes.get(i);
    		//初始化vo类和值
    		LotteryCodeKsZstVo lotteryCodeKsZstVo = new LotteryCodeKsZstVo();
    		lotteryCodeKsZstVo.setNumInfo1(lotteryCode.getNumInfo1());
    		lotteryCodeKsZstVo.setNumInfo2(lotteryCode.getNumInfo2());
    		lotteryCodeKsZstVo.setNumInfo3(lotteryCode.getNumInfo3());
    		lotteryCodeKsZstVo.setLotteryNum(lotteryCode.getLotteryNum());
    		int numInfo1 = Integer.parseInt(lotteryCodeKsZstVo.getNumInfo1());
    		int numInfo2 = Integer.parseInt(lotteryCodeKsZstVo.getNumInfo2());
    		int numInfo3 = Integer.parseInt(lotteryCodeKsZstVo.getNumInfo3());
    		
    		List<String> numInfoArray = new ArrayList<String>();
    		numInfoArray.add(lotteryCodeKsZstVo.getNumInfo1());
    		numInfoArray.add(lotteryCodeKsZstVo.getNumInfo2());
    		numInfoArray.add(lotteryCodeKsZstVo.getNumInfo3());
    		numsOmmit(numsCurrent, numMateCurrent, numInfoArray);

    		//和值
    		int hzNum = numInfo1 + numInfo2 + numInfo3;
    		numsOmmit(hznumsCurrent,hzNumMateCurrent, String.valueOf(hzNum));
    		//和值形态
    		String hzxt = "";
    		if(hzNum < 11) {
    			if(hzNum % 2 == 0) {
    				hzxt = "2";
    			} else {
    				hzxt = "1";
    			}
    		} else {
    			if(hzNum % 2 == 0) {
    				hzxt = "4";
    			} else {
    				hzxt = "3";
    			}
    		}
    		if(!StringUtils.isEmpty(hzxt)) {
    			numsOmmit(hzxtnumsCurrent, hzxtNumMateCurrent, hzxt);
    		}
    		
    		//号码形态
    		List<String> hmxtArray = new ArrayList<String>();
    		if(numInfo1 == numInfo2 && numInfo2 == numInfo3){
            	hmxtArray.add("1");
            } else if((numInfo1 != numInfo2) && (numInfo2 != numInfo3) && (numInfo1 != numInfo3)){
            	hmxtArray.add("2");
            	if(numInfo1 + 1 == numInfo2 && numInfo2 + 1 == numInfo3) {
            		hmxtArray.add("3");
            	} 
            } else if(numInfo1 == numInfo2 || numInfo2 == numInfo3 || numInfo1 == numInfo2) {
            	hmxtArray.add("4");
            	if(numInfo1 != numInfo2 || numInfo2 != numInfo3 || numInfo1 != numInfo3) {
            		hmxtArray.add("5");
            	}
            }
            if(numInfo1 != numInfo2 || numInfo2 != numInfo3 || numInfo1 != numInfo3) {
        		hmxtArray.add("6");
        	}  
            numsOmmit(hmxtnumsCurrent, hmxtNumMateCurrent, hmxtArray);
            
            //需要重新拿一个临时变量赋值，不然最后map都是一个值
        	HashMap<String, Integer> numMateCurrentTemp = new HashMap<String, Integer>();
        	HashMap<String, Integer> hzNumMateCurrentTemp = new HashMap<String, Integer>();
        	HashMap<String, Integer> hzxtNumMateCurrentTemp = new HashMap<String, Integer>();
        	HashMap<String, Integer> hmxtNumMateCurrentTemp = new HashMap<String, Integer>();
        	for (Map.Entry<String, Integer> entry : numMateCurrent.entrySet()) {  
        		numMateCurrentTemp.put(entry.getKey(), entry.getValue());
        	}  
        	for (Map.Entry<String, Integer> entry : hzNumMateCurrent.entrySet()) {  
        		hzNumMateCurrentTemp.put(entry.getKey(), entry.getValue());
        	}  
        	for (Map.Entry<String, Integer> entry : hzxtNumMateCurrent.entrySet()) {  
        		hzxtNumMateCurrentTemp.put(entry.getKey(), entry.getValue());
        	}  
        	for (Map.Entry<String, Integer> entry : hmxtNumMateCurrent.entrySet()) {  
        		hmxtNumMateCurrentTemp.put(entry.getKey(), entry.getValue());
        	}  
            //插入当前值
        	lotteryCodeKsZstVo.setNumMateCurrent(numMateCurrentTemp);
    		lotteryCodeKsZstVo.setHzNumMateCurrent(hzNumMateCurrentTemp);
    		lotteryCodeKsZstVo.setHzxtNumMateCurrent(hzxtNumMateCurrentTemp);
    		lotteryCodeKsZstVo.setHmxtNumMateCurrent(hmxtNumMateCurrentTemp);
    		list.add(lotteryCodeKsZstVo);
    	}
    	
    	return list;
    }
	
	
	

	/**
	 * 初始化时时彩遗漏值
	 * @param lotteryCodes
	 * @return
	 */
	public static List<LotteryCodeSscZstVo> getSscOmmit(List<LotteryCode> lotteryCodes) {
		if(lotteryCodes == null){
			lotteryCodes = new ArrayList<LotteryCode>();
		}

		List<LotteryCodeSscZstVo> list =new ArrayList<LotteryCodeSscZstVo>();	
    	String [] wannumsCurrent = new String[]{"0","1","2","3","4","5","6","7","8","9"};
    	String [] qiannumsCurrent = new String[]{"0","1","2","3","4","5","6","7","8","9"};
    	String [] bainumsCurrent = new String[]{"0","1","2","3","4","5","6","7","8","9"};
    	String [] shinumsCurrent = new String[]{"0","1","2","3","4","5","6","7","8","9"};
    	String [] gennumsCurrent = new String[]{"0","1","2","3","4","5","6","7","8","9"};

    	HashMap<String, Integer> wanNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> qianNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> baiNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> shiNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> geNumMateCurrent = new HashMap<String, Integer>();
    	
    	for(String num : wannumsCurrent){	
    		wanNumMateCurrent.put(num, 0);
    		qianNumMateCurrent.put(num, 0);
    		baiNumMateCurrent.put(num, 0);
    		shiNumMateCurrent.put(num, 0);
    		geNumMateCurrent.put(num, 0);
    	}
    	for(int i = 0; i < lotteryCodes.size(); i++){
    		LotteryCode lotteryCode = lotteryCodes.get(i);
    		if(lotteryCode.getNumInfo1() != null){ 
    			numsOmmit(wannumsCurrent, wanNumMateCurrent, lotteryCode.getNumInfo1());
    		}
    		//千
    		if(lotteryCode.getNumInfo2() != null){  
    			numsOmmit(qiannumsCurrent, qianNumMateCurrent, lotteryCode.getNumInfo2());
    		}  	
    		//百
    		if(lotteryCode.getNumInfo3() != null){  
    			numsOmmit(bainumsCurrent, baiNumMateCurrent, lotteryCode.getNumInfo3());
    		}    	
    		//十
    		if(lotteryCode.getNumInfo4() != null){  
    			numsOmmit(shinumsCurrent, shiNumMateCurrent, lotteryCode.getNumInfo4());
    		}   
    		
    		//个
    		if(lotteryCode.getNumInfo5() != null){  
    			numsOmmit(gennumsCurrent, geNumMateCurrent, lotteryCode.getNumInfo5());
    		}   
 
    		//初始化vo类和值
			LotteryCodeSscZstVo lotteryCodeSscZstVo = new LotteryCodeSscZstVo();
			lotteryCodeSscZstVo.setNumInfo1(lotteryCode.getNumInfo1());
			lotteryCodeSscZstVo.setNumInfo2(lotteryCode.getNumInfo2());
			lotteryCodeSscZstVo.setNumInfo3(lotteryCode.getNumInfo3());
			lotteryCodeSscZstVo.setNumInfo4(lotteryCode.getNumInfo4());
			lotteryCodeSscZstVo.setNumInfo5(lotteryCode.getNumInfo5());
			lotteryCodeSscZstVo.setLotteryNum(lotteryCode.getLotteryNum());
    		
			  //需要重新拿一个临时变量赋值，不然最后map都是一个值
        	HashMap<String, Integer> wannumsCurrentTemp = new HashMap<String, Integer>();
        	HashMap<String, Integer> qiannumsCurrentTemp = new HashMap<String, Integer>();
        	HashMap<String, Integer> bainumsCurrentTemp = new HashMap<String, Integer>();
        	HashMap<String, Integer> shinumsCurrentTemp = new HashMap<String, Integer>();
        	HashMap<String, Integer> genumsCurrentTemp = new HashMap<String, Integer>();
        	for (Map.Entry<String, Integer> entry : wanNumMateCurrent.entrySet()) {  
        		wannumsCurrentTemp.put(entry.getKey(), entry.getValue());
        	}  
        	for (Map.Entry<String, Integer> entry : qianNumMateCurrent.entrySet()) {  
        		qiannumsCurrentTemp.put(entry.getKey(), entry.getValue());
        	}  
        	for (Map.Entry<String, Integer> entry : baiNumMateCurrent.entrySet()) {  
        		bainumsCurrentTemp.put(entry.getKey(), entry.getValue());
        	}  
        	for (Map.Entry<String, Integer> entry : shiNumMateCurrent.entrySet()) {  
        		shinumsCurrentTemp.put(entry.getKey(), entry.getValue());
        	}  
        	for (Map.Entry<String, Integer> entry : geNumMateCurrent.entrySet()) {  
        		genumsCurrentTemp.put(entry.getKey(), entry.getValue());
        	}  
            //插入当前值
        	lotteryCodeSscZstVo.setWanNumMateCurrent(wannumsCurrentTemp);
        	lotteryCodeSscZstVo.setQianNumMateCurrent(qiannumsCurrentTemp);
        	lotteryCodeSscZstVo.setBaiNumMateCurrent(bainumsCurrentTemp);
        	lotteryCodeSscZstVo.setShiNumMateCurrent(shinumsCurrentTemp);
        	lotteryCodeSscZstVo.setGeNumMateCurrent(genumsCurrentTemp);
    		list.add(lotteryCodeSscZstVo);
    		
    	}
    	

    	return list;
    }
	
	
	/**
	 * 初始化十一选五遗漏值
	 * @param lotteryCodes
	 * @return
	 */
	public static List<LotteryCodeSyxwZstVo> getSyxwOmmit(List<LotteryCode> lotteryCodes) {
		if(lotteryCodes == null){
			lotteryCodes = new ArrayList<LotteryCode>();
		}

		List<LotteryCodeSyxwZstVo> list =new ArrayList<LotteryCodeSyxwZstVo>();	
    	String [] wannumsCurrent = new String[]{"01","02","03","04","05","06","07","08","09","10","11"};
    	String [] qiannumsCurrent = new String[]{"01","02","03","04","05","06","07","08","09","10","11"};
    	String [] bainumsCurrent = new String[]{"01","02","03","04","05","06","07","08","09","10","11"};
    	String [] shinumsCurrent = new String[]{"01","02","03","04","05","06","07","08","09","10","11"};
    	String [] gennumsCurrent = new String[]{"01","02","03","04","05","06","07","08","09","10","11"};

    	HashMap<String, Integer> wanNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> qianNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> baiNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> shiNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> geNumMateCurrent = new HashMap<String, Integer>();
    	
    	for(String num : wannumsCurrent){	
    		wanNumMateCurrent.put(num, 0);
    		qianNumMateCurrent.put(num, 0);
    		baiNumMateCurrent.put(num, 0);
    		shiNumMateCurrent.put(num, 0);
    		geNumMateCurrent.put(num, 0);
    	}
    	for(int i = 0; i < lotteryCodes.size(); i++){
    		LotteryCode lotteryCode = lotteryCodes.get(i);
    		if(lotteryCode.getNumInfo1() != null){ 
    			numsOmmit(wannumsCurrent, wanNumMateCurrent, lotteryCode.getNumInfo1());
    		}
    		//千
    		if(lotteryCode.getNumInfo2() != null){  
    			numsOmmit(qiannumsCurrent, qianNumMateCurrent, lotteryCode.getNumInfo2());
    		}  	
    		//百
    		if(lotteryCode.getNumInfo3() != null){  
    			numsOmmit(bainumsCurrent, baiNumMateCurrent, lotteryCode.getNumInfo3());
    		}    	
    		//十
    		if(lotteryCode.getNumInfo4() != null){  
    			numsOmmit(shinumsCurrent, shiNumMateCurrent, lotteryCode.getNumInfo4());
    		}   
    		
    		//个
    		if(lotteryCode.getNumInfo5() != null){  
    			numsOmmit(gennumsCurrent, geNumMateCurrent, lotteryCode.getNumInfo5());
    		}   
 
    		//初始化vo类和值
    		LotteryCodeSyxwZstVo lotteryCodeSyxwZstVo = new LotteryCodeSyxwZstVo();
    		lotteryCodeSyxwZstVo.setNumInfo1(lotteryCode.getNumInfo1());
    		lotteryCodeSyxwZstVo.setNumInfo2(lotteryCode.getNumInfo2());
    		lotteryCodeSyxwZstVo.setNumInfo3(lotteryCode.getNumInfo3());
    		lotteryCodeSyxwZstVo.setNumInfo4(lotteryCode.getNumInfo4());
    		lotteryCodeSyxwZstVo.setNumInfo5(lotteryCode.getNumInfo5());
    		lotteryCodeSyxwZstVo.setLotteryNum(lotteryCode.getLotteryNum());
    		
			  //需要重新拿一个临时变量赋值，不然最后map都是一个值
        	HashMap<String, Integer> wannumsCurrentTemp = new HashMap<String, Integer>();
        	HashMap<String, Integer> qiannumsCurrentTemp = new HashMap<String, Integer>();
        	HashMap<String, Integer> bainumsCurrentTemp = new HashMap<String, Integer>();
        	HashMap<String, Integer> shinumsCurrentTemp = new HashMap<String, Integer>();
        	HashMap<String, Integer> genumsCurrentTemp = new HashMap<String, Integer>();
        	for (Map.Entry<String, Integer> entry : wanNumMateCurrent.entrySet()) {  
        		wannumsCurrentTemp.put(entry.getKey(), entry.getValue());
        	}  
        	for (Map.Entry<String, Integer> entry : qianNumMateCurrent.entrySet()) {  
        		qiannumsCurrentTemp.put(entry.getKey(), entry.getValue());
        	}  
        	for (Map.Entry<String, Integer> entry : baiNumMateCurrent.entrySet()) {  
        		bainumsCurrentTemp.put(entry.getKey(), entry.getValue());
        	}  
        	for (Map.Entry<String, Integer> entry : shiNumMateCurrent.entrySet()) {  
        		shinumsCurrentTemp.put(entry.getKey(), entry.getValue());
        	}  
        	for (Map.Entry<String, Integer> entry : geNumMateCurrent.entrySet()) {  
        		genumsCurrentTemp.put(entry.getKey(), entry.getValue());
        	}  
            //插入当前值
        	lotteryCodeSyxwZstVo.setWanNumMateCurrent(wannumsCurrentTemp);
        	lotteryCodeSyxwZstVo.setQianNumMateCurrent(qiannumsCurrentTemp);
        	lotteryCodeSyxwZstVo.setBaiNumMateCurrent(bainumsCurrentTemp);
        	lotteryCodeSyxwZstVo.setShiNumMateCurrent(shinumsCurrentTemp);
        	lotteryCodeSyxwZstVo.setGeNumMateCurrent(genumsCurrentTemp);
    		list.add(lotteryCodeSyxwZstVo);
    		
    	}
    	

    	return list;
    }
	
	/**
	 * 初始化PK10遗漏值
	 * @param lotteryCodes
	 * @return
	 */
	public static List<LotteryCodeBjpk10ZstVo> getPK10Ommit(List<LotteryCode> lotteryCodes) {
		if(lotteryCodes == null){
			lotteryCodes = new ArrayList<LotteryCode>();
		}

		List<LotteryCodeBjpk10ZstVo> list =new ArrayList<LotteryCodeBjpk10ZstVo>();	
    	String [] onenumsCurrent = new String[]{"01","02","03","04","05","06","07","08","09","10"};
    	String [] secondnumsCurrent = new String[]{"01","02","03","04","05","06","07","08","09","10"};
    	String [] threenumsCurrent = new String[]{"01","02","03","04","05","06","07","08","09","10"};
    	String [] fournumsCurrent = new String[]{"01","02","03","04","05","06","07","08","09","10"};
    	String [] fivenumsCurrent = new String[]{"01","02","03","04","05","06","07","08","09","10"};
    	String [] sixnumsCurrent = new String[]{"01","02","03","04","05","06","07","08","09","10"};
    	String [] sevennumsCurrent = new String[]{"01","02","03","04","05","06","07","08","09","10"};
    	String [] eightnumsCurrent = new String[]{"01","02","03","04","05","06","07","08","09","10"};
    	String [] ninenumsCurrent = new String[]{"01","02","03","04","05","06","07","08","09","10"};
    	String [] tennumsCurrent = new String[]{"01","02","03","04","05","06","07","08","09","10"};
    	
    	//当前遗漏
    	HashMap<String, Integer> oneNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> secondNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> threeNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> fourNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> fiveNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> sixNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> sevenNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> eightNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> nineNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> tenNumMateCurrent = new HashMap<String, Integer>();
    	
    	
    	for(String num : onenumsCurrent){
    		oneNumMateCurrent.put(num, 0);
    		secondNumMateCurrent.put(num, 0);
    		threeNumMateCurrent.put(num, 0);
    		fourNumMateCurrent.put(num, 0);
    		fiveNumMateCurrent.put(num, 0);
    		sixNumMateCurrent.put(num, 0);
    		sevenNumMateCurrent.put(num, 0);
    		eightNumMateCurrent.put(num, 0);
    		nineNumMateCurrent.put(num, 0);
    		tenNumMateCurrent.put(num, 0);
    	}
    	for(int i = 0; i < lotteryCodes.size(); i++){
    		LotteryCode lotteryCode = lotteryCodes.get(i);
    		 //第一名
    		if(lotteryCode.getNumInfo1() != null){ 
    			numsOmmit(onenumsCurrent, oneNumMateCurrent, lotteryCode.getNumInfo1());
    		}
    		//第二名
    		if(lotteryCode.getNumInfo2() != null){  
    			numsOmmit(secondnumsCurrent, secondNumMateCurrent, lotteryCode.getNumInfo2());
    		}  	
    		//第三名
    		if(lotteryCode.getNumInfo3() != null){  
    			numsOmmit(threenumsCurrent, threeNumMateCurrent, lotteryCode.getNumInfo3());
    		}    	
    		//第四名
    		if(lotteryCode.getNumInfo4() != null){  
    			numsOmmit(fournumsCurrent, fourNumMateCurrent, lotteryCode.getNumInfo4());
    		}   
    		
    		//第五名
    		if(lotteryCode.getNumInfo5() != null){  
    			numsOmmit(fivenumsCurrent, fiveNumMateCurrent, lotteryCode.getNumInfo5());
    		}  
    		
    		//第六名
    		if(lotteryCode.getNumInfo6() != null){ 
    			numsOmmit(sixnumsCurrent, sixNumMateCurrent, lotteryCode.getNumInfo6());
    		}
    		//第七名
    		if(lotteryCode.getNumInfo7() != null){  
    			numsOmmit(sevennumsCurrent, sevenNumMateCurrent, lotteryCode.getNumInfo7());
    		}  	
    		//第八名
    		if(lotteryCode.getNumInfo8() != null){  
    			numsOmmit(eightnumsCurrent, eightNumMateCurrent, lotteryCode.getNumInfo8());
    		}    	
    		//第九名
    		if(lotteryCode.getNumInfo9() != null){  
    			numsOmmit(ninenumsCurrent, nineNumMateCurrent, lotteryCode.getNumInfo9());
    		}   
    		
    		//第十名
    		if(lotteryCode.getNumInfo10() != null){  
    			numsOmmit(tennumsCurrent, tenNumMateCurrent, lotteryCode.getNumInfo10());
    		}   
 
    		//初始化vo类和值
    		LotteryCodeBjpk10ZstVo lotteryCodeBjpk10ZstVo = new LotteryCodeBjpk10ZstVo();
    		lotteryCodeBjpk10ZstVo.setNumInfo1(lotteryCode.getNumInfo1());
    		lotteryCodeBjpk10ZstVo.setNumInfo2(lotteryCode.getNumInfo2());
    		lotteryCodeBjpk10ZstVo.setNumInfo3(lotteryCode.getNumInfo3());
    		lotteryCodeBjpk10ZstVo.setNumInfo4(lotteryCode.getNumInfo4());
    		lotteryCodeBjpk10ZstVo.setNumInfo5(lotteryCode.getNumInfo5());
    		lotteryCodeBjpk10ZstVo.setNumInfo6(lotteryCode.getNumInfo6());
    		lotteryCodeBjpk10ZstVo.setNumInfo7(lotteryCode.getNumInfo7());
    		lotteryCodeBjpk10ZstVo.setNumInfo8(lotteryCode.getNumInfo8());
    		lotteryCodeBjpk10ZstVo.setNumInfo9(lotteryCode.getNumInfo9());
    		lotteryCodeBjpk10ZstVo.setNumInfo10(lotteryCode.getNumInfo10());
    		lotteryCodeBjpk10ZstVo.setLotteryNum(lotteryCode.getLotteryNum());
    		
			  //需要重新拿一个临时变量赋值，不然最后map都是一个值
    		HashMap<String, Integer> oneNumMateCurrentTemp = new HashMap<String, Integer>();
        	HashMap<String, Integer> secondNumMateCurrentTemp = new HashMap<String, Integer>();
        	HashMap<String, Integer> threeNumMateCurrentTemp = new HashMap<String, Integer>();
        	HashMap<String, Integer> fourNumMateCurrentTemp = new HashMap<String, Integer>();
        	HashMap<String, Integer> fiveNumMateCurrentTemp = new HashMap<String, Integer>();
        	HashMap<String, Integer> sixNumMateCurrentTemp = new HashMap<String, Integer>();
        	HashMap<String, Integer> sevenNumMateCurrentTemp = new HashMap<String, Integer>();
        	HashMap<String, Integer> eightNumMateCurrentTemp = new HashMap<String, Integer>();
        	HashMap<String, Integer> nineNumMateCurrentTemp = new HashMap<String, Integer>();
        	HashMap<String, Integer> tenNumMateCurrentTemp = new HashMap<String, Integer>();
        	for (Map.Entry<String, Integer> entry : oneNumMateCurrent.entrySet()) {  
        		oneNumMateCurrentTemp.put(entry.getKey(), entry.getValue());
        	}  
        	for (Map.Entry<String, Integer> entry : secondNumMateCurrent.entrySet()) {  
        		secondNumMateCurrentTemp.put(entry.getKey(), entry.getValue());
        	}  
        	for (Map.Entry<String, Integer> entry : threeNumMateCurrent.entrySet()) {  
        		threeNumMateCurrentTemp.put(entry.getKey(), entry.getValue());
        	}  
        	for (Map.Entry<String, Integer> entry : fourNumMateCurrent.entrySet()) {  
        		fourNumMateCurrentTemp.put(entry.getKey(), entry.getValue());
        	}  
        	for (Map.Entry<String, Integer> entry : fiveNumMateCurrent.entrySet()) {  
        		fiveNumMateCurrentTemp.put(entry.getKey(), entry.getValue());
        	}  
         	for (Map.Entry<String, Integer> entry : sixNumMateCurrent.entrySet()) {  
         		sixNumMateCurrentTemp.put(entry.getKey(), entry.getValue());
        	}  
         	for (Map.Entry<String, Integer> entry : sevenNumMateCurrent.entrySet()) {  
         		sevenNumMateCurrentTemp.put(entry.getKey(), entry.getValue());
        	}  
         	for (Map.Entry<String, Integer> entry : eightNumMateCurrent.entrySet()) {  
         		eightNumMateCurrentTemp.put(entry.getKey(), entry.getValue());
        	}  
         	for (Map.Entry<String, Integer> entry : nineNumMateCurrent.entrySet()) {  
         		nineNumMateCurrentTemp.put(entry.getKey(), entry.getValue());
        	}  
         	for (Map.Entry<String, Integer> entry : tenNumMateCurrent.entrySet()) {  
         		tenNumMateCurrentTemp.put(entry.getKey(), entry.getValue());
        	}  
            //插入当前值
         	lotteryCodeBjpk10ZstVo.setOneNumMateCurrent(oneNumMateCurrentTemp);
         	lotteryCodeBjpk10ZstVo.setSecondNumMateCurrent(secondNumMateCurrentTemp);
         	lotteryCodeBjpk10ZstVo.setThreeNumMateCurrent(threeNumMateCurrentTemp);
         	lotteryCodeBjpk10ZstVo.setFourNumMateCurrent(fourNumMateCurrentTemp);
         	lotteryCodeBjpk10ZstVo.setFiveNumMateCurrent(fiveNumMateCurrentTemp);
         	lotteryCodeBjpk10ZstVo.setSixNumMateCurrent(sixNumMateCurrentTemp);
         	lotteryCodeBjpk10ZstVo.setSevenNumMateCurrent(sevenNumMateCurrentTemp);
         	lotteryCodeBjpk10ZstVo.setEightNumMateCurrent(eightNumMateCurrentTemp);
         	lotteryCodeBjpk10ZstVo.setNineNumMateCurrent(nineNumMateCurrentTemp);
         	lotteryCodeBjpk10ZstVo.setTenNumMateCurrent(tenNumMateCurrentTemp);
    		list.add(lotteryCodeBjpk10ZstVo);
    		
    	}
    	

    	return list;
    }
	
	
	/**
	 * 初始化低频彩遗漏值
	 * @param lotteryCodes
	 * @return
	 */
	public static List<LotteryCodeDpcZstVo> getDpcOmmit(List<LotteryCode> lotteryCodes) {
		if(lotteryCodes == null){
			lotteryCodes = new ArrayList<LotteryCode>();
		}

		List<LotteryCodeDpcZstVo> list =new ArrayList<LotteryCodeDpcZstVo>();	
    	String [] bainumsCurrent = new String[]{"0","1","2","3","4","5","6","7","8","9"};
    	String [] shinumsCurrent = new String[]{"0","1","2","3","4","5","6","7","8","9"};
    	String [] gennumsCurrent = new String[]{"0","1","2","3","4","5","6","7","8","9"};

    
    	HashMap<String, Integer> baiNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> shiNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> geNumMateCurrent = new HashMap<String, Integer>();
    	
    	for(String num : bainumsCurrent){	
    
    		baiNumMateCurrent.put(num, 0);
    		shiNumMateCurrent.put(num, 0);
    		geNumMateCurrent.put(num, 0);
    	}
    	Map<String, Integer> maps = null;
    	for(int i = 0; i < lotteryCodes.size(); i++){
    		LotteryCode lotteryCode = lotteryCodes.get(i);
    		//百
    		if(lotteryCode.getNumInfo3() != null){  
    			numsOmmit(bainumsCurrent, baiNumMateCurrent, lotteryCode.getNumInfo3());
    		}    	
    		//十
    		if(lotteryCode.getNumInfo4() != null){  
    			numsOmmit(shinumsCurrent, shiNumMateCurrent, lotteryCode.getNumInfo4());
    		}   
    		
    		//个
    		if(lotteryCode.getNumInfo5() != null){  
    			numsOmmit(gennumsCurrent, geNumMateCurrent, lotteryCode.getNumInfo5());
    		}   
 
    		//初始化vo类和值
    		LotteryCodeDpcZstVo lotteryCodeDpcZstVo = new LotteryCodeDpcZstVo();
    		lotteryCodeDpcZstVo.setNumInfo1(lotteryCode.getNumInfo1());
    		lotteryCodeDpcZstVo.setNumInfo2(lotteryCode.getNumInfo2());
    		lotteryCodeDpcZstVo.setNumInfo3(lotteryCode.getNumInfo3());
    		lotteryCodeDpcZstVo.setLotteryNum(lotteryCode.getLotteryNum());
    		
			  //需要重新拿一个临时变量赋值，不然最后map都是一个值

        	HashMap<String, Integer> bainumsCurrentTemp = new HashMap<String, Integer>();
        	HashMap<String, Integer> shinumsCurrentTemp = new HashMap<String, Integer>();
        	HashMap<String, Integer> genumsCurrentTemp = new HashMap<String, Integer>();
        
        	for (Map.Entry<String, Integer> entry : baiNumMateCurrent.entrySet()) {  
        		bainumsCurrentTemp.put(entry.getKey(), entry.getValue());
        	}  
        	for (Map.Entry<String, Integer> entry : shiNumMateCurrent.entrySet()) {  
        		shinumsCurrentTemp.put(entry.getKey(), entry.getValue());
        	}  
        	for (Map.Entry<String, Integer> entry : geNumMateCurrent.entrySet()) {  
        		genumsCurrentTemp.put(entry.getKey(), entry.getValue());
        	}  
            //插入当前值
        	lotteryCodeDpcZstVo.setBaiNumMateCurrent(bainumsCurrentTemp);
        	lotteryCodeDpcZstVo.setShiNumMateCurrent(shinumsCurrentTemp);
        	lotteryCodeDpcZstVo.setGeNumMateCurrent(genumsCurrentTemp);
    		list.add(lotteryCodeDpcZstVo);
    		
    	}
    	

    	return list;
    }
	
	/**
	 * 初始化分分彩遗漏值
	 * @param lotteryCodes
	 * @return
	 */
	public static List<LotteryCodeFfcZstVo> getFfcOmmit(List<LotteryCode> lotteryCodes) {
		if(lotteryCodes == null){
			lotteryCodes = new ArrayList<LotteryCode>();
		}

		List<LotteryCodeFfcZstVo> list =new ArrayList<LotteryCodeFfcZstVo>();	
    	String [] wannumsCurrent = new String[]{"0","1","2","3","4","5","6","7","8","9"};
    	String [] qiannumsCurrent = new String[]{"0","1","2","3","4","5","6","7","8","9"};
    	String [] bainumsCurrent = new String[]{"0","1","2","3","4","5","6","7","8","9"};
    	String [] shinumsCurrent = new String[]{"0","1","2","3","4","5","6","7","8","9"};
    	String [] gennumsCurrent = new String[]{"0","1","2","3","4","5","6","7","8","9"};

    	HashMap<String, Integer> wanNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> qianNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> baiNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> shiNumMateCurrent = new HashMap<String, Integer>();
    	HashMap<String, Integer> geNumMateCurrent = new HashMap<String, Integer>();
    	
    	for(String num : wannumsCurrent){	
    		wanNumMateCurrent.put(num, 0);
    		qianNumMateCurrent.put(num, 0);
    		baiNumMateCurrent.put(num, 0);
    		shiNumMateCurrent.put(num, 0);
    		geNumMateCurrent.put(num, 0);
    	}
    	for(int i = 0; i < lotteryCodes.size(); i++){
    		LotteryCode lotteryCode = lotteryCodes.get(i);
    		if(lotteryCode.getNumInfo1() != null){ 
    			numsOmmit(wannumsCurrent, wanNumMateCurrent, lotteryCode.getNumInfo1());
    		}
    		//千
    		if(lotteryCode.getNumInfo2() != null){  
    			numsOmmit(qiannumsCurrent, qianNumMateCurrent, lotteryCode.getNumInfo2());
    		}  	
    		//百
    		if(lotteryCode.getNumInfo3() != null){  
    			numsOmmit(bainumsCurrent, baiNumMateCurrent, lotteryCode.getNumInfo3());
    		}    	
    		//十
    		if(lotteryCode.getNumInfo4() != null){  
    			numsOmmit(shinumsCurrent, shiNumMateCurrent, lotteryCode.getNumInfo4());
    		}   
    		
    		//个
    		if(lotteryCode.getNumInfo5() != null){  
    			numsOmmit(gennumsCurrent, geNumMateCurrent, lotteryCode.getNumInfo5());
    		}   
 
    		//初始化vo类和值
    		LotteryCodeFfcZstVo lotteryCodeFfcZstVo = new LotteryCodeFfcZstVo();
    		lotteryCodeFfcZstVo.setNumInfo1(lotteryCode.getNumInfo1());
    		lotteryCodeFfcZstVo.setNumInfo2(lotteryCode.getNumInfo2());
    		lotteryCodeFfcZstVo.setNumInfo3(lotteryCode.getNumInfo3());
    		lotteryCodeFfcZstVo.setNumInfo4(lotteryCode.getNumInfo4());
    		lotteryCodeFfcZstVo.setNumInfo5(lotteryCode.getNumInfo5());
    		lotteryCodeFfcZstVo.setLotteryNum(lotteryCode.getLotteryNum());
    		
			  //需要重新拿一个临时变量赋值，不然最后map都是一个值
        	HashMap<String, Integer> wannumsCurrentTemp = new HashMap<String, Integer>();
        	HashMap<String, Integer> qiannumsCurrentTemp = new HashMap<String, Integer>();
        	HashMap<String, Integer> bainumsCurrentTemp = new HashMap<String, Integer>();
        	HashMap<String, Integer> shinumsCurrentTemp = new HashMap<String, Integer>();
        	HashMap<String, Integer> genumsCurrentTemp = new HashMap<String, Integer>();
        	for (Map.Entry<String, Integer> entry : wanNumMateCurrent.entrySet()) {  
        		wannumsCurrentTemp.put(entry.getKey(), entry.getValue());
        	}  
        	for (Map.Entry<String, Integer> entry : qianNumMateCurrent.entrySet()) {  
        		qiannumsCurrentTemp.put(entry.getKey(), entry.getValue());
        	}  
        	for (Map.Entry<String, Integer> entry : baiNumMateCurrent.entrySet()) {  
        		bainumsCurrentTemp.put(entry.getKey(), entry.getValue());
        	}  
        	for (Map.Entry<String, Integer> entry : shiNumMateCurrent.entrySet()) {  
        		shinumsCurrentTemp.put(entry.getKey(), entry.getValue());
        	}  
        	for (Map.Entry<String, Integer> entry : geNumMateCurrent.entrySet()) {  
        		genumsCurrentTemp.put(entry.getKey(), entry.getValue());
        	}  
            //插入当前值
        	lotteryCodeFfcZstVo.setWanNumMateCurrent(wannumsCurrentTemp);
        	lotteryCodeFfcZstVo.setQianNumMateCurrent(qiannumsCurrentTemp);
        	lotteryCodeFfcZstVo.setBaiNumMateCurrent(bainumsCurrentTemp);
        	lotteryCodeFfcZstVo.setShiNumMateCurrent(shinumsCurrentTemp);
        	lotteryCodeFfcZstVo.setGeNumMateCurrent(genumsCurrentTemp);
    		list.add(lotteryCodeFfcZstVo);
    		
    	}
   
    	return list;
    }
	
	
	
	/**
	 * 位数对应的遗漏值(当前遗漏和最大遗漏)
	 * @param nums
	 * @param maps
	 * @param numValue
	 * @param position
	 */
    public static void numsOmmit(String [] nums,Map<String, Integer> maps,String numValue){
		for(int j = 0; j < nums.length; j++){ 
			String num = nums[j];
			if(numValue.equals(num)){
				maps.put(num, 0); 
				continue;
			}else{
				if(num != null){
					maps.put(num, maps.get(num) + 1); 
				}
			}
		}
	}
    
    /**
	 * 位数对应的遗漏值(当前遗漏和最大遗漏)
	 * @param nums
	 * @param maps
	 * @param numValue
	 * @param position
	 */
    public static void numsOmmit(String [] nums,Map<String, Integer> maps,List<String> numValues){
		for(int i = 0; i < nums.length; i++){ 
			String num = nums[i];
			//值是否在数组里面
			boolean isNumIn = false;
			for(int j = 0; j < numValues.size(); j++) {
				if(numValues.get(j).equals(num)){
					isNumIn = true;
					maps.put(num, 0); 
					continue;
				}
			}
			if(!isNumIn) {
				if(num != null){
					maps.put(num, maps.get(num) + 1); 
				}
			}
		}
	}
    
    

}
