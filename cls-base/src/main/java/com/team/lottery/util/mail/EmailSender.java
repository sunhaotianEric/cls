package com.team.lottery.util.mail;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.vo.EmailSendConfig;

public class EmailSender {
	
	private static Logger logger = LoggerFactory.getLogger(EmailSender.class);

	/**
	 * 发送邮件，使用默认的邮件发送配置类型
	 * @param subject
	 * @param content
	 * @param toEmails
	 * @return
	 */
	public static boolean sendMail(String subject, String content, String toEmails) {
		if(StringUtils.isBlank(subject) || StringUtils.isBlank(content) || StringUtils.isBlank(toEmails)) {
			return false;
		}
		EmailSendConfig sendConfig = ClsCacheManager.getEmailSendConfig(EmailSendConfig.DEFAULT_TYPE);
		if(sendConfig == null) {
			logger.error("缓存配置获取到的邮件发送配置为空");
			return false;
		}
		MailSenderInfo mailInfo = new MailSenderInfo();    
		mailInfo.setMailServerHost(sendConfig.getMailHost());    
		mailInfo.setMailServerPort(sendConfig.getMailPort());    
		mailInfo.setValidate(true);    
		mailInfo.setUserName(sendConfig.getUserName()); 
		mailInfo.setPassword(sendConfig.getPassword());//您的邮箱密码    
		mailInfo.setFromAddress(sendConfig.getUserName()); 
		//处理邮件接收者,如果有超过两个以上的邮件接收者，则其他处理为密送
		String[] mails = toEmails.split(",");
		if(mails.length > 1) {
			int index = toEmails.indexOf(",");
			String bccMails = toEmails.substring(index + 1);
			mailInfo.setToAddress(mails[0]);
			mailInfo.setBccAddress(bccMails);
		} else {
			mailInfo.setToAddress(toEmails);
		}
		
		mailInfo.setSubject(subject);    
		mailInfo.setContent(content);   
		
		//这个类主要来发送邮件   
		SimpleMailSender sms = new SimpleMailSender(); 
		boolean res = false;
		if(EmailSendConfig.NORMAL_SEND_TYPE.equals(sendConfig.getSendType())) {
			//发送文体格式    
			res = sms.sendTextMail(mailInfo);
		} else {
			//发送文体格式 ssl方式    
			res = sms.sendTextMailBySsl(mailInfo);
		}
		return res;
	}
	
	/**
	 * 发送邮件，使用对应的邮件发送配置类型
	 * @param subject
	 * @param content
	 * @param toEmails
	 * @return
	 */
	public static boolean sendMail(String subject, String content, String toEmails, String type) {
		if(StringUtils.isBlank(subject) || StringUtils.isBlank(content) || StringUtils.isBlank(toEmails)) {
			return false;
		}
		EmailSendConfig sendConfig = ClsCacheManager.getEmailSendConfig(type);
		MailSenderInfo mailInfo = new MailSenderInfo();    
		mailInfo.setMailServerHost(sendConfig.getMailHost());    
		mailInfo.setMailServerPort(sendConfig.getMailPort());    
		mailInfo.setValidate(true);    
		mailInfo.setUserName(sendConfig.getUserName());    
		mailInfo.setPassword(sendConfig.getPassword());//您的邮箱密码    
		mailInfo.setFromAddress(sendConfig.getUserName()); 
		//处理邮件接收者,如果有超过两个以上的邮件接收者，则其他处理为密送
		String[] mails = toEmails.split(",");
		if(mails.length > 1) {
			int index = toEmails.indexOf(",");
			String bccMails = toEmails.substring(index + 1);
			mailInfo.setToAddress(mails[0]);
			mailInfo.setBccAddress(bccMails);
		} else {
			mailInfo.setToAddress(toEmails);
		}
		
		mailInfo.setSubject(subject);    
		mailInfo.setContent(content);   
		
		//这个类主要来发送邮件   
		SimpleMailSender sms = new SimpleMailSender(); 
		boolean res = sms.sendTextMail(mailInfo);//发送文体格式    
		return res;
	}
}
