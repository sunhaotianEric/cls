package com.team.lottery.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 正则校验
 * 
 * @author Jamine
 * 
 *         原文作者:王立平的博客 代码来源:CSDN
 *         来源路径:https://blog.csdn.net/qq_37131111/article/details/77525056
 * 
 *         代码来源:CSDN
 *         原文地址:https://blog.csdn.net/m18860232520/article/details/79396889
 *         原文作者:yuongxi
 */

public class RegxUtil {
	/**
	 * 校验邮箱是否合法.
	 * 
	 * @param email
	 * @return
	 */
	public static boolean isEmailNumber(String email) {

		boolean flag = false;
		try {
			// 正则表达式
			String check = "^([a-z0-9A-Z]+[-|_|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
			Pattern regex = Pattern.compile(check);
			Matcher matcher = regex.matcher(email);
			flag = matcher.matches();
		} catch (Exception e) {
			flag = false;
		}
		return flag;
	}

	/**
	 * 校验手机是否合法
	 * 
	 * @param phoneNumber
	 * @return
	 */
	public static boolean isPhoneNumber(String phoneNumber) {
		// 正则表达式.
		String regex = "^((13[0-9])|(14[5,7,9])|(15([0-3]|[5-9]))|(166)|(17[0,1,3,5,6,7,8])|(18[0-9])|(19[8|9]))\\d{8}$";
		if (phoneNumber.length() != 11) {
			return false;
		} else {
			Pattern p = Pattern.compile(regex);
			Matcher m = p.matcher(phoneNumber);
			boolean isMatch = m.matches();
			if (!isMatch) {
				return false;
			}
			return true;
		}
	}

	/**
	 * 校验是否是6位纯数字的安全密码.
	 * 
	 * @param safePassword
	 * @return
	 */
	public static boolean isSixNumber(String safePassword) {
		// 对应的正则表达式.
		String regex = "\\d{6}";
		// 首先判断密码的长度不为6就直接返回false.
		if (safePassword.length() != 6) {
			return false;
		} else {
			Pattern p = Pattern.compile(regex);
			Matcher m = p.matcher(safePassword);
			boolean isMatch = m.matches();
			if (!isMatch) {
				return false;
			}
			return true;
		}
	}

	/**
	 * 校验用户名长度是否正确
	 * 
	 * @param safePassword
	 * @return
	 */
	public static boolean checkUserNameLength(String userName) {
		// 判断用户名是否是2位到十位之间.
		if (userName.length() < 4 || userName.length() > 16) {
			// 用户名长度不正确.
			return false;
		} else {
			return true;
		}

	}

	/**
	 * 校验用户名格式是否正确
	 * 
	 * @param userName
	 * @return
	 */
	public static boolean checkUserName(String userName) {

		// 对应的正则表达式.
		String regex = "([a-z]|[A-Z]|[0-9])+";
		// 校验用户名格式是否正确.
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(userName);
		boolean isMatch = m.matches();
		if (!isMatch) {
			return false;
		}
		return true;
	}
	
	/**
	 * 校验昵称是否是8位以下的汉字.
	 * @param nickName
	 * @return
	 */
	public static boolean checkNickNameIsRight(String nickName) {

		// 对应的正则表达式.
		String regex = "[\u4e00-\u9fa5]";
		// 初始化字符校验次数正确,默认为0
		int a = 0;
		// 校验用户名格式是否正确.
		for (int i = 0; i < nickName.length(); i++) {
			// 取出对应索引的字符.
			String codePointAt = nickName.charAt(i)+"";
			Pattern p = Pattern.compile(regex);
			Matcher m = p.matcher(codePointAt);
			// 先判断是否为汉字.如果是a就进行++操作.
			boolean isMatch = m.matches();
			if (!isMatch) {
				return false;
			} else {
				a++;
			}
		}
		// 判断a的长度,如果小于等于8就返回ture
		if (a <= 8) {
			return true;
		}else {
			return false;
		}
	}
	
	/**
	 * 校验中文汉字是否是15位以下.
	 * 
	 * @param trueName
	 * @return
	 */
	public static boolean isChineseName(String trueName) {

		// 对应的正则表达式.
		String regex = "[\u4e00-\u9fa5]";
		// 初始化字符校验次数正确,默认为0
		int a = 0;
		// 校验用户名格式是否正确.
		for (int i = 0; i < trueName.length(); i++) {
			// 取出对应索引的字符.
			String codePointAt = trueName.charAt(i) + "";
			Pattern p = Pattern.compile(regex);
			Matcher m = p.matcher(codePointAt);
			// 先判断是否为汉字.如果是a就进行++操作.
			boolean isMatch = m.matches();
			if (!isMatch) {
				return false;
			} else {
				a++;
			}
		}
		// 判断a的长度,如果小于等于15就返回ture
		if (a <= 15) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * main方法测试.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println(checkNickNameIsRight("aaa"));
		System.out.println(checkNickNameIsRight("a一二三四五六"));
		System.out.println(checkNickNameIsRight("一二三四五六七"));
		System.out.println(checkNickNameIsRight("一二三四五六七八"));
		System.out.println(checkNickNameIsRight("一二三四五六七八九"));
		System.out.println(checkNickNameIsRight("一二三四五六七八九十"));
		
	}
}
