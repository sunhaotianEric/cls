package com.team.lottery.util;


public class XY28NumberUtil {

	//红波
	private final static String hongboCodes = "3,6,9,12,15,18,21,24";
	//绿波
	private final static String lvboCodes =   "1,4,7,10,16,19,22,25";
	//蓝波
	private final static String lanboCodes = "2,5,8,11,17,20,23,26";
	//无波
	private final static String wuboCodes = "0,13,14,27";
	public final static int HONGBO = 1;
	public final static int LVBO = 3;
	public final static int LANBO = 2;
	
	

	/**
	 * 大==true
	 * 小==false
	 * @param code
	 * @return
	 */
	public static Boolean  decideBigOrSmall(String code){
		int codeInt = Integer.parseInt(code);
		
		if(codeInt>13){
			return true;	
		}else{
			return false;
		}
	}
	
	/**
	 * 单==true
	 * 双==false
	 * 49为和
	 * @param code
	 * @return
	 */
	public static Boolean  decideSingleOrdouble(String code){
	   int codeInt = Integer.parseInt(code);
		if(codeInt%2==1){
			return true;	
		}else{
			return false;
		}	
	}
	

	
	/**
	 * 大单==1
	 * 小单==2
 	 * 大双==3
	 * 小双==4
	 * @param code
	 * @return
	 */
	public static int  decideDXDS(String code){
		  int codeInt = Integer.parseInt(code);
		  if(codeInt%2==1 && codeInt>14){
				return 1;	
		   }else if(codeInt%2==1 && codeInt<14){
				return 2;
		   }else if(codeInt%2==0 && codeInt>=14){
				return 3;
		   }else{
			    return 4;
		   }		
	}
	
	/**
	 * 极大==1
	 * 极小==2
	 * 不是极值  = 0 
	 * @param code
	 * @return
	 */
	public static Integer  decideJDOrJX(String code){
	   int codeInt = Integer.parseInt(code);
		if(codeInt >= 23){
			return 1;	
		}else if(codeInt <= 4){
			return 2;
		}
		return 0;
	}
	
	/**
	 * 红波==1
	 * 绿波==3
	 * 蓝波==2
	 * @param code
	 * @return
	 */
	public static int  decidePoShe(String code){
		 String codeStr = Integer.parseInt(code)+"";
		 String[] hongboStr = hongboCodes.split(",");
		 String[] lanboStr = lanboCodes.split(",");
		 String[] lvboStr = lvboCodes.split(",");
		 
		 for(String c:hongboStr){
			 if(c.equals(codeStr)){
				 return 1; 
			 }
		 }
		 for(String c:lanboStr){
			 if(c.equals(codeStr)){
				 return 2; 
			 }
		 }
		 
		 for(String c:lvboStr){
			 if(c.equals(codeStr)){
				 return 3; 
			 }
		 }
		return 0;
	}
}
