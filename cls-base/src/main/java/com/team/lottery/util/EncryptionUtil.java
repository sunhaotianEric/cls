package com.team.lottery.util;

import com.team.lottery.util.StringUtils;

/**
 * 加密工具类
 * 
 * @author Owner
 *
 */
public class EncryptionUtil {

	/**
	 * 加密银行卡信息显示后四位
	 * 
	 * @param bankCardNum
	 * @return
	 */
	public static String EncryptionCard(String bankCardNum) {
		if (!StringUtils.isEmpty(bankCardNum)) {
			bankCardNum = "****************" + bankCardNum.substring(bankCardNum.length() - 4, bankCardNum.length());
		}
		return bankCardNum;
	}

	/**
	 * 昵称做加密处理.显示第一个字符
	 * 
	 * @param nickName
	 * @return
	 */
	public static String EncryptionNickName(String nickName) {
		if (!StringUtils.isEmpty(nickName)) {
			nickName = nickName.substring(0, 1) + "**";
		}
		return nickName;
	}

	/**
	 * 用户名做加密处理.显示前两位与最后一位
	 * 
	 * @param UserName
	 * @return
	 */
	public static String EncryptionUserName(String userName) {
		if (!StringUtils.isEmpty(userName)) {
			if (userName.length() == 1 || userName.length() == 2) {
				userName = userName.substring(0, 1) + "**";
			}
			userName = userName.substring(0, 2) + "**" + userName.substring(userName.length() - 1, userName.length());
		}
		return userName;
	}

	/**
	 * 手机号加密处理.
	 * 
	 * @param phoneNumber
	 * @return
	 */
	public static String getEncryptedPhoneNumber(String phoneNumber) {
		// 传入的手机号不为空才进行加密处理.
		if (!StringUtils.isEmpty(phoneNumber)) {
			StringBuffer replace = new StringBuffer(phoneNumber).replace(3, 7, "****");
			return replace.toString();
		}
		return "";
	}

	/**
	 * 获取加密后的邮箱号码.
	 * 
	 * @param phoneNumber
	 * @return
	 */
	public static String getEncryptedEmailNumber(String emailNumber) {
		if (StringUtils.isEmpty(emailNumber)) {
			return "";
		}
		// 截取指定字符串后面的字符.
		String substring = emailNumber.substring(emailNumber.indexOf("@") + 1);
		String[] split = emailNumber.split("@");
		String emailNumberBegin = split[0];
		// 获取当前邮箱前缀长度.
		int length = emailNumberBegin.length();
		String a = "*";
		String b = "";
		if (length > 3) {
			for (int i = 0; i < length - 2; i++) {
				b += a;
			}
		}
		String encryptedEmailNumber = emailNumber.substring(0, 2) + b + "@" + substring;
		return encryptedEmailNumber;
	}

	public static void main(String[] args) {
		/*
		 * String s = "l"; String ss = "lc"; String sss = "lclclc";
		 * 
		 * System.out.println("一位：" + EncryptionUserName(s));// l****
		 * System.out.println("二位：" + EncryptionUserName(ss));// l****
		 * System.out.println("多位：" + EncryptionUserName(sss));// lc**c
		 * System.out.println("null：" + EncryptionUserName(null));
		 * System.out.println("==============================================");
		 * System.out.println("一位：" + EncryptionNickName(s));// l**
		 * System.out.println("二位：" + EncryptionNickName(ss));// l**
		 * System.out.println("多位：" + EncryptionNickName(sss));// l**
		 * System.out.println("null：" + EncryptionNickName(null));
		 */

		System.out.println(getEncryptedPhoneNumber("00000000000"));
		System.out.println(getEncryptedPhoneNumber("00000000000"));
		System.out.println(getEncryptedEmailNumber("00000000000@qq.com"));

	}
}
