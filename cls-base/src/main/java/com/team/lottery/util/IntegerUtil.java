package com.team.lottery.util;

import java.util.regex.Pattern;

public class IntegerUtil {

    /**
     * 判断是否为整数
     * @param str
     * @return
     */
	public static boolean isNumeric(String str){  
	    Pattern pattern = Pattern.compile("[0-9]*");  
	    return pattern.matcher(str).matches();     
	}  
	
    /**
     * 判断是否为正整数
     * @param str
     * @return
     */
	public static boolean isPositiveNumeric(String str){  
	    Pattern pattern = Pattern.compile("[0-9]*");  
	    return pattern.matcher(str).matches();     
	}  
	
	
	  /**
     * 判断是否为正数
     * @param str
     * @return
     */
	public static boolean isPositiveNum(String str){  
	    Pattern pattern = Pattern.compile("(0|([1-9]\\d*))(\\.\\d+)?");  
	    return pattern.matcher(str).matches();     
	}  
	
	public static void main(String[] args) {
		//System.out.println(isPositiveNumeric1("19999999"));
	}
}
