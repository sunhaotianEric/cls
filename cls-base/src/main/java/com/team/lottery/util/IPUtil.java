package com.team.lottery.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IPUtil {

	private static Logger logger = LoggerFactory.getLogger(IPUtil.class);

	// 访问百度接口IP
	public static final String BAIDU_HTTP_URL = "http://apis.baidu.com/apistore/iplookupservice/iplookup";
	public static final String BAIDU_API_KEY = "fd99825964658ac972f6f624245216d6";

	/**
	 * 获取ip的地域信息
	 * 
	 * @param ip
	 * @return
	 */
	// public static String getAddressByIp(String ip) {
	// StringBuffer result = new StringBuffer();
	// try {
	// String strIP = ip;
	// URL url = new URL("http://ip.qq.com/cgi-bin/searchip?searchip1="
	// + strIP);
	// URLConnection conn = url.openConnection();
	// BufferedReader reader = new BufferedReader(new InputStreamReader(
	// conn.getInputStream(), "GBK"));
	// String line = null;
	// while ((line = reader.readLine()) != null) {
	// result.append(line);
	// }
	// reader.close();
	// strIP = result.substring(result.indexOf("该IP所在地为："));
	// strIP = strIP.substring(strIP.indexOf("：") + 1);
	// String province = strIP.substring(6, strIP.indexOf("省"));
	// String city = strIP.substring(strIP.indexOf("省") + 1,
	// strIP.indexOf("市"));
	// result.append(province).append(" - ").append(city);
	// } catch (IOException e) {
	// System.out.println("");
	// }
	// return result.toString();
	// }

	/**
	 * 
	 * @param content
	 *            请求的参数 格式为：name=xxx&pwd=xxx
	 * @param encoding
	 *            服务器端请求编码。如GBK,UTF-8等
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String getAddresses(String content, String encodingString) throws UnsupportedEncodingException {
		// 这里调用pconline的接口
		String urlStr = "http://ip.taobao.com/service/getIpInfo.php";
		// 从http://whois.pconline.com.cn取得IP所在的省市区信息
		String returnStr = getResult(urlStr, content, encodingString);
		StringBuffer result = new StringBuffer();
		if (returnStr != null) {
			// 处理返回的省市区信息
			System.out.println(returnStr);
			String[] temp = returnStr.split(",");
			if (temp.length < 3) {
				return "0";// 无效IP，局域网测试
			}
			String region = (temp[5].split(":"))[1].replaceAll("\"", "");
			region = decodeUnicode(region);// 省份

			String country = "";
			String area = "";
			// String region = "";
			String city = "";
			String county = "";
			String isp = "";
			for (int i = 0; i < temp.length; i++) {
				switch (i) {
				case 1:
					country = (temp[i].split(":"))[2].replaceAll("\"", "");
					country = decodeUnicode(country);// 国家
					break;
				case 3:
					area = (temp[i].split(":"))[1].replaceAll("\"", "");
					area = decodeUnicode(area);// 地区
					break;
				case 5:
					region = (temp[i].split(":"))[1].replaceAll("\"", "");
					region = decodeUnicode(region);// 省份
					break;
				case 7:
					city = (temp[i].split(":"))[1].replaceAll("\"", "");
					city = decodeUnicode(city);// 市区
					break;
				case 9:
					county = (temp[i].split(":"))[1].replaceAll("\"", "");
					county = decodeUnicode(county);// 地区
					break;
				case 11:
					isp = (temp[i].split(":"))[1].replaceAll("\"", "");
					isp = decodeUnicode(isp); // ISP公司
					break;
				}
			}
			// System.out.println(country + "=" + area + "=" + region + "=" +
			// city + "=" + county + "=" + isp);
			return result.append(region).append(" ").append(city).append(" ").append(isp).toString();
		}
		return result.toString();
	}

	/**
	 * 
	 * @param content
	 *            请求的参数 格式为：name=xxx&pwd=xxx
	 * @param encoding
	 *            服务器端请求编码。如GBK,UTF-8等
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String getAddressesSina(String ipStr) {
		// 这里调用pconline的接口
		String urlStr = "http://int.dpool.sina.com.cn/iplookup/iplookup.php";
		String encodingString = "GBK";

		String returnStr = getResult(urlStr, ipStr, encodingString);
		StringBuffer result = new StringBuffer();
		if (returnStr != null) {
			// 处理返回的省市区信息
			// System.out.println(returnStr);
			String[] temp = returnStr.split("\t");
			if (temp.length < 5) {
				return "0";// 无效IP，局域网测试
			}

			// String country = temp[3];
			String province = temp[4];
			String city = temp[5];
			String isp = "";

			// System.out.println("country=" + country + " province=" + province
			// + " city=" + city);
			return result.append(province).append(" ").append(city).append(" ").append(isp).toString();
		}
		return result.toString();
	}

	/**
	 * @param urlStr
	 *            请求的地址
	 * @param content
	 *            请求的参数 格式为：name=xxx&pwd=xxx
	 * @param encoding
	 *            服务器端请求编码。如GBK,UTF-8等
	 * @return
	 */
	private static String getResult(String urlStr, String content, String encoding) {
		URL url = null;
		HttpURLConnection connection = null;
		try {
			url = new URL(urlStr);
			connection = (HttpURLConnection) url.openConnection();// 新建连接实例
			connection.setConnectTimeout(2000);// 设置连接超时时间，单位毫秒
			connection.setReadTimeout(2000);// 设置读取数据超时时间，单位毫秒
			connection.setDoOutput(true);// 是否打开输出流 true|false
			connection.setDoInput(true);// 是否打开输入流true|false
			connection.setRequestMethod("POST");// 提交方法POST|GET
			connection.setUseCaches(false);// 是否缓存true|false
			connection.connect();// 打开连接端口
			DataOutputStream out = new DataOutputStream(connection.getOutputStream());// 打开输出流往对端服务器写数据
			out.writeBytes(content);// 写数据,也就是提交你的表单 name=xxx&pwd=xxx
			out.flush();// 刷新
			out.close();// 关闭输出流
			BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), encoding));// 往对端写完数据对端服务器返回数据
			// ,以BufferedReader流来读取
			StringBuffer buffer = new StringBuffer();
			String line = "";
			while ((line = reader.readLine()) != null) {
				buffer.append(line);
			}
			reader.close();
			return buffer.toString();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				connection.disconnect();// 关闭连接
			}
		}
		return null;
	}

	/**
	 * unicode 转换成 中文
	 * 
	 * @author fanhui 2007-3-15
	 * @param theString
	 * @return
	 */
	public static String decodeUnicode(String theString) {
		char aChar;
		int len = theString.length();
		StringBuffer outBuffer = new StringBuffer(len);
		for (int x = 0; x < len;) {
			aChar = theString.charAt(x++);
			if (aChar == '\\') {
				aChar = theString.charAt(x++);
				if (aChar == 'u') {
					int value = 0;
					for (int i = 0; i < 4; i++) {
						aChar = theString.charAt(x++);
						switch (aChar) {
						case '0':
						case '1':
						case '2':
						case '3':
						case '4':
						case '5':
						case '6':
						case '7':
						case '8':
						case '9':
							value = (value << 4) + aChar - '0';
							break;
						case 'a':
						case 'b':
						case 'c':
						case 'd':
						case 'e':
						case 'f':
							value = (value << 4) + 10 + aChar - 'a';
							break;
						case 'A':
						case 'B':
						case 'C':
						case 'D':
						case 'E':
						case 'F':
							value = (value << 4) + 10 + aChar - 'A';
							break;
						default:
							throw new IllegalArgumentException("Malformed      encoding.");
						}
					}
					outBuffer.append((char) value);
				} else {
					if (aChar == 't') {
						aChar = '\t';
					} else if (aChar == 'r') {
						aChar = '\r';
					} else if (aChar == 'n') {
						aChar = '\n';
					} else if (aChar == 'f') {
						aChar = '\f';
					}
					outBuffer.append(aChar);
				}
			} else {
				outBuffer.append(aChar);
			}
		}
		return outBuffer.toString();
	}

	/**
	 * @param urlAll
	 *            :请求接口
	 * @param httpArg
	 *            :参数
	 * @return 返回结果
	 */
	public static String getAddressesBaidu(String ipStr) {
		BufferedReader reader = null;
		String result = null;
		StringBuffer sbf = new StringBuffer();
		String httpUrl = BAIDU_HTTP_URL + "?ip=" + ipStr;

		try {
			URL url = new URL(httpUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setConnectTimeout(2000);// 设置连接超时时间，单位毫秒
			connection.setReadTimeout(2000);// 设置读取数据超时时间，单位毫秒
			connection.setUseCaches(false);// 是否缓存true|false
			// 填入apikey到HTTP header
			connection.setRequestProperty("apikey", BAIDU_API_KEY);
			connection.connect();
			InputStream is = connection.getInputStream();
			reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			String strRead = null;
			while ((strRead = reader.readLine()) != null) {
				sbf.append(strRead);
				sbf.append("\r\n");
			}
			reader.close();
			result = sbf.toString();
		} catch (Exception e) {
			logger.error("连接到百度ip接口获取信息出错...", e);
		}

		// 开始处理返回信息
		StringBuffer addrDes = new StringBuffer();
		if (StringUtils.isNotBlank(result)) {
			try {
				JSONObject jsonObj = JSONUtils.toJSONObject(result);
				if (jsonObj != null) {
					String errMsg = jsonObj.get("errMsg").toString();
					// System.out.println("errMsg : " + errMsg);
					Object retDataObj = jsonObj.get("retData");
					JSONObject retDataJsonObj = null;
					if (retDataObj instanceof JSONObject) {
						retDataJsonObj = (JSONObject) retDataObj;
					}
					if ("success".equals(errMsg) && retDataJsonObj != null) {
						String province = retDataJsonObj.get("province").toString();
						String city = retDataJsonObj.get("city").toString();
						String district = retDataJsonObj.get("district").toString();
						String carrier = retDataJsonObj.get("carrier").toString();
						addrDes.append(province + " ");
						addrDes.append(city + " ");
						if (StringUtils.isNotBlank(district)) {
							addrDes.append(district + " ");
						}
						addrDes.append(carrier);
					} else {
						addrDes.append(retDataObj.toString());
					}
				}
			} catch (Exception e) {
				logger.error("处理百度ip接口返回结果出错...", e);
			}
		}
		return addrDes.toString();
	}

	/**
	 * @param urlAll
	 *            :请求接口
	 * @param httpArg
	 *            :参数
	 * @return 返回结果
	 */
	public static String getAddressesPcOnline(String ipStr) {
		BufferedReader reader = null;
		String result = null;
		StringBuffer sbf = new StringBuffer();
		String httpUrl = "http://whois.pconline.com.cn/ip.jsp" + "?ip=" + ipStr;

		try {
			URL url = new URL(httpUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setConnectTimeout(4000);// 设置连接超时时间，单位毫秒
			connection.setReadTimeout(4000);// 设置读取数据超时时间，单位毫秒
			connection.setUseCaches(false);// 是否缓存true|false
			connection.connect();
			InputStream is = connection.getInputStream();
			reader = new BufferedReader(new InputStreamReader(is, "GBK"));
			String strRead = null;
			while ((strRead = reader.readLine()) != null) {
				sbf.append(strRead);
				sbf.append("\r\n");
			}
			reader.close();
			result = sbf.toString();
		} catch (Exception e) {
			logger.error("连接到太平洋ip接口获取信息出错...", e);
		}

		// 开始处理返回信息
		StringBuffer addrDes = new StringBuffer();
		if (StringUtils.isNotBlank(result)) {
			try {
				/*
				 * JSONObject jsonObj = JSONUtils.toJSONObject(result);
				 * if(jsonObj != null) { String errMsg =
				 * jsonObj.get("errMsg").toString();
				 * //System.out.println("errMsg : " + errMsg); Object retDataObj
				 * = jsonObj.get("retData"); JSONObject retDataJsonObj = null;
				 * if(retDataObj instanceof JSONObject) { retDataJsonObj =
				 * (JSONObject)retDataObj; } if("success".equals(errMsg) &&
				 * retDataJsonObj != null) { String province =
				 * retDataJsonObj.get("province").toString(); String city =
				 * retDataJsonObj.get("city").toString(); String district =
				 * retDataJsonObj.get("district").toString(); String carrier =
				 * retDataJsonObj.get("carrier").toString();
				 * addrDes.append(province + " "); addrDes.append(city + " ");
				 * if(StringUtils.isNotBlank(district)) {
				 * addrDes.append(district + " "); } addrDes.append(carrier); }
				 * else { addrDes.append(retDataObj.toString()); } }
				 */
				addrDes.append(result);
			} catch (Exception e) {
				logger.error("处理太平洋ip接口返回结果出错...", e);
			}
		}
		return addrDes.toString();
	}

	public static String getIpAddr(HttpServletRequest request) {
		String ipAddress = null;
		// ipAddress = this.getRequest().getRemoteAddr();
		ipAddress = request.getHeader("x-forwarded-for");
		if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
			ipAddress = request.getHeader("Proxy-Client-IP");
		}
		if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
			ipAddress = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
			ipAddress = request.getRemoteAddr();
			if (ipAddress.equals("127.0.0.1")) {
				// 根据网卡取本机配置的IP
				InetAddress inet = null;
				try {
					inet = InetAddress.getLocalHost();
				} catch (UnknownHostException e) {
					e.printStackTrace();
				}
				ipAddress = inet.getHostAddress();
			}

		}

		// 对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
		if (ipAddress != null && ipAddress.length() > 15) { // "***.***.***.***".length()
															// = 15
			if (ipAddress.indexOf(",") > 0) {
				ipAddress = ipAddress.substring(0, ipAddress.indexOf(","));
			}
		}
		return ipAddress;
	}

	/**
	 * 获取客户端IP.
	 * @param request
	 * @return
	 */
	public static String getRequestIp(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		// 多级反向代理会有多个ip值，此时取第一个ip值
		if (ip != null && ip.length() > 0) {
			String[] ips = ip.split(",");
			// 默认取第一个ip
			ip = ips[0].trim();
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}

	// 测试
	// public static void main(String[] args) {
	// IPUtil addressUtils = new IPUtil();
	// // 测试ip 219.136.134.157 中国=华南=广东省=广州市=越秀区=电信
	// String ip = "59.57.220.74";
	// String address = "";
	// try {
	// address = addressUtils.getAddresses("ip=" + ip, "utf-8");
	// } catch (UnsupportedEncodingException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// System.out.println(address);
	// // 输出结果为：广东省,广州市,越秀区
	// }

	public static void main(String[] args) throws UnsupportedEncodingException {
		// getAddressByIp("192.168.0.107");
		// getAddressesSina("192.168.0.107");
		String res = getAddressesBaidu("115.214.234.16");
		String res2 = getAddressesPcOnline("115.214.234.16");
		System.out.println(res);
		System.out.println(res2);
	}
}
