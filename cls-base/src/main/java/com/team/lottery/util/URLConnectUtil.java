package com.team.lottery.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;

/**
 * 请求URL数据工具类
 * @author chenhsh
 *
 */
public class URLConnectUtil {
	
	
	//调用其他接口超时时间
	public static Integer CONNECTION_TIMEOUT = 20000;
	//调用其他接口socket超时时间
	public static Integer SO_TIMEOUT = 20000;
	
	/**
	 * 原来的方式，通过URLConnection来获取数据
	 * @deprecated 改用httpclinet
	 * @param urlStr
	 * @return
	 * @throws IOException
	 */
	public static String getStringFromURLOld(String urlStr) throws IOException{
		if(StringUtils.isEmpty(urlStr)){
			return null;
		}
		URL url = new URL(urlStr);  
		URLConnection rulConnection = url.openConnection();
		rulConnection.setConnectTimeout(20000);  //设置连接主机超时（单位：毫秒） 
		rulConnection.setReadTimeout(20000);  //设置从主机读取数据超时（单位：毫秒）
		BufferedReader in = new BufferedReader(new InputStreamReader(rulConnection.getInputStream()));
		StringBuffer sb = new StringBuffer();
		String line = "";
		String NL = System.getProperty("line.separator");
		while ((line = in.readLine()) != null) {
		sb.append(line + NL);
		}
		in.close();
		return sb.toString();
	}
	
	/**
	 * 使用httpClient来进行接口请求
	 * @param urlStr
	 * @return
	 * @throws Exception
	 */
	public static String getStringFromURL(String urlStr) throws Exception{
		if(StringUtils.isEmpty(urlStr)){
			return null;
		}
		HttpClient httpclient = new HttpClient();
		//链接超时
		httpclient.getHttpConnectionManager().getParams().setConnectionTimeout(CONNECTION_TIMEOUT);  
		//读取超时
		httpclient.getHttpConnectionManager().getParams().setSoTimeout(SO_TIMEOUT);
		
		GetMethod getMethod = new GetMethod(urlStr);
        // 执行getMethod
        int statusCode = httpclient.executeMethod(getMethod);
        StringBuffer stringBuffer = new StringBuffer();  
        if (statusCode == HttpStatus.SC_OK) {
            //returnStr = getMethod.getResponseBodyAsString();
            InputStream inputStream = getMethod.getResponseBodyAsStream();  
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream)); 
            String str= "";  
            String separator = System.getProperty("line.separator");
            while((str = br.readLine()) != null){  
            	stringBuffer.append(str).append(separator);  
            }  
        } else {
        	String res = "调用接口"+ urlStr +"失败,返回错误码:" + statusCode;
        	throw new Exception(res);
        }
        return new String(stringBuffer.toString().getBytes(),"utf-8");
	}
}
