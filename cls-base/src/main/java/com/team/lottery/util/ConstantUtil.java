package com.team.lottery.util;

import java.math.BigDecimal;

import com.team.lottery.system.SystemConfigConstant;

public class ConstantUtil {
	
	//超级系统类型
	public static final String SUPER_SYSTEM = "SUPER_SYSTEM";
	//超级管理员的账号用户名
	/*public static final String ADMIN_USER_NAME = "lotterychenhsh";*/
	//超级管理员的ID
	public static final Integer ADMIN_USER_ID = 1;
	public static final String FRONT_ADMIN_USER_NAME ="lotterychenhsh";
	//游客的最上级
	public static final String GUEST_ADMIN = "GUEST_ADMIN";
	//游客默认邀请码
    public static final String GUEST_CODE = "00000000";
	//邮箱登陆次数
	public static final String MAIL_USER_COUNT = "MAIL_USER_COUNT";
	//邮箱验证的标识
	public static final String MAIL_USER_SIGN = "MAIL_USER_SIGN";
	//前台登陆session标识
	public static final String USER_LOGIN_MARK_FOR_USER = "USER_LOGIN_MARK_FOR_USER";
	//后台登陆session标识
	public static final String USER_LOGIN_MARK_FOR_ADMIN = "USER_LOGIN_MARK_FOR_ADMIN";
	//前台是否被踢出标识
	public static final String KICK_OUT_STATUS = "kickOutStatus";
	//最新公告的session标识
	public static final String NEWEST_ANNOUNCE = "NEWEST_ANNOUNCE";
	//新手礼包弹框标识
	public static final String NEW_USER_GIFT_TASK = "NEW_USER_GIFT_TASK";
	//邀请码的session标识
	public static final String INVITAION_CODE = "INVITAION_CODE";
	//前台系统标识
	public static final String CURRENT_SYSTEM = "CURRENT_SYSTEM";
	//是否退出的标识
	public static final String USER_LOGIN_OUT_SIGN = "FLOW_LOGIN_OUT_USER";
    //验证码session key 
	public static final String CHECK_CODE_STR = "RANDOMVALIDATECODEKEY_LOTTERY_KEY";
	//用户快捷充值的订单标识
	public static final String USER_QUICK_ORDER_SIGN = "USER_QUICK_ORDER_SIGN";
	//订单通知
    public static final String ORDER_NOTIFY_SCRIPT_SESSION = "ORDER_NOTIFY_SCRIPT_SESSION";
    //最新开奖号码通知
    public static final String LOTTERY_CODE_NOTIFY_SCRIPT_SESSION = "LOTTERY_CODE_NOTIFY_SCRIPT_SESSION";
	//开奖期号的分隔符
    //public static final String EXPECT_PLIT = "-";
	//订单分隔符 
	public static final String LOTTERY_ORDER_SPLIT = "-";
	//期号分隔符
	public static final String LOTTERY_EXPECT_SPLIT = "-";
	//彩种模式类型的分隔符 
	public static final String LOTTERY_AND_PROTYPE_PLIT = "_";
	//开奖号码的分隔符
	public static final String OPENCODE_PLIT = ",";
	//投注号码的分隔符
	public static final String SOURCECODE_SPLIT = ",";
	//投注号码的分隔符
	public static final String SOURCECODE_SPLIT_KS = "&";
	//投注号码的胆拖分隔符
	public static final String SOURCECODE_SPLIT_DT = "#";
	//投注不同玩法的分隔符
	public static final String SOURCECODE_PROTYPE_JOIN = "$";
	//投注不同玩法的分隔符
	public static final String SOURCECODE_PROTYPE_SPLIT = "\\$";
	//空余位数的代替字符
	public static final String CODE_REPLACE = "-";
	//投注号码相邻排列的分隔符
	public static final String SOURCECODE_ARRANGE_SPLIT = "\\|";
	//系统设置信息的拼接
	public static final String SYSTEM_SETTING_SPLIT = "|";
	//系统设置信息分隔符
	public static final String SYSTEM_SETTING_JOIN = "\\|";
	//系统域名分隔符
	public static final String SYSTEM_DOMAIN_SPLIT = "#";
    //代理推广链接信息的拼接字符串
	public static final String REGISTER_SPLIT = "&";
    //代理推广链接信息的拼接字符串
	public static final String DS_CODE_SPLIT = "&";
	//任选 单式投注号码分隔符 
	public static final String LOTTERY_DS_RX_PROTYPE_PLIT = "_";
    //代理推广链接信息的null的代替
	public static final String REGISTER_NULL_REPLACE = "-";
	
	//系统操作名称
	public static final String SYSTEM_OPERATE_NAME = "SYSTEM";
    //用户注册代理人之间的分隔符
	public static final String REG_FORM_SPLIT = "&";
	//dwr的ajax处理，争取与否的标识	
	public static final String DWR_AJAX_RESULT_OK = "ok"; 
	public static final String DWR_AJAX_RESULT_ERROR = "error"; 
	//加密涉及字符串一
	public static final String MD5_SECRET_STR_1 = "h";
	//加密涉及字符串二
	public static final String MD5_SECRET_STR_2 = "c";
    //用户名只能是字母和数字
	public static final String USER_NAME_CHAR_FILTER = "([a-z]|[A-Z]|[0-9])+";
	//充值间距时间
	public static final Integer RECHARGE_INTEVERAL = 15;
	//奖金精度
	public static final Integer BIGDECIMAL_SCAL = 3;
	//金额精度
	public static final Integer BIGDECIMAL_SCAL_MONEY = 2;
	//中奖金额分隔符
	public static final String EVERYWINCOST_SPLT = "&";
	//开奖的期数
	public static final Integer opcodeCount = 10;
    //请求失败次数控制启用备用接口地址
	public static final Integer REQUEST_FAIL_COUNT = 4;
	//请求备用接口地址到达多少次切回正式接口地址
	public static final Integer BACK_REQUEST_COUNT = 20;
	//请求失败次数达到多少次进行接口告警
	public static final Integer ALARM_REQUEST_FAIL_COUNT = 8;
	//时时彩计算的最高模式
	public static final BigDecimal SSC_COMPUTE_VALUE = new BigDecimal("1960");
	//分分彩计算的最高模式
	public static final BigDecimal FFC_COMPUTE_VALUE = new BigDecimal("1960");
	//11选5计算的最高模式
	public static final BigDecimal SYXW_COMPUTE_VALUE = new BigDecimal("1930");
	//低频彩计算的最高模式
	public static final BigDecimal DPC_COMPUTE_VALUE = new BigDecimal("1930");
	//快三计算的最高模式
	public static final BigDecimal KS_COMPUTE_VALUE = new BigDecimal("1960");
	//骰宝计算的最高模式
	public static final BigDecimal TB_COMPUTE_VALUE = new BigDecimal("1960");
	//PK10计算的最高模式
	public static final BigDecimal PK10_COMPUTE_VALUE = new BigDecimal("1960");
	//六合彩计算的最高模式
	public static final BigDecimal LHC_COMPUTE_VALUE = new BigDecimal("1960");
		
	//代理对应的时时彩最低模式
	public static final BigDecimal SSC_LOEWST_REBATE_FOR_DAILI = new BigDecimal("1850"); //1910
	//删除用户最低模式
	public static final BigDecimal DEL_LOEWST_REBATE_FOR_DAILI = new BigDecimal("1950"); //1950
	
	//特级直属配额达到的模式
	public static final BigDecimal QUOTA_1960_REBATE = new BigDecimal("1960");
	//直属配额达到的模式
	public static final BigDecimal QUOTA_1958_REBATE = new BigDecimal("1958");
	//总代配额达到的模式
	public static final BigDecimal QUOTA_1956_REBATE = new BigDecimal("1956");
	//代理配额达到的模式
	public static final BigDecimal QUOTA_1954_REBATE = new BigDecimal("1954");
	//代理下级配额达到的模式
	public static final BigDecimal QUOTA_1952_REBATE = new BigDecimal("1952");
	
	//查询选择彩种动态加载期数的条数
	public static Integer QUERY_LOTTERY_CODE_SIZE = 1000;
	
	//待遇配置的分隔符
	public static final String TREATMENT_SPLIT = ",";
	
	//删除试玩用户的相关数据数据保留天数
	public static final Integer DELETE_AGO_DATA_GUEST = 3;
	//删除常规数据  必须删除几天之前的数据
	public static final Integer DELETE_AGO_DATA_LOWER = 7;
	//删除订单数据数据保留天数
	public static final Integer DELETE_AGO_DATA_ORDER = 20;
	//删除资金明细数据保留天数
	public static final Integer DELETE_AGO_DATA_MONEY_DETAIL = 30;
	//删除登录日志数据保留天数
	public static final Integer DELETE_AGO_DATA_LOGIN_LOG = 30;
	//删除充值提现订单数据保留天数
	public static final Integer DELETE_AGO_DATA_RECHARGE_WITHDRAW_ORDER = 65;
	//删除红包聊天室数据保留天数
	public static final Integer DELETE_AGO_DATA_CHAT_RED_BAG = 30;
	//删除常规数据  必须删除几天之前的数据
	public static final Integer DELETE_AGO_DATA_NORMAL = 65;
	//删除非常规数据  必须删除几天之前的数据
	public static final Integer DELETE_AGO_DATA_NUNORMAL = 90;
	
	//删除低频彩 180
    public static final Integer DELETE_AGO_DATA_LOWERCODEDATE= 180;
	//彻底删除会员  必须删除几天之前的数据
	public static final Integer DELETE_AGO_DATA_EVERY = 30;
	
	
	//特级直属的模式
	public static final BigDecimal SUPER_AGENCY_1960_REBATE = new BigDecimal("1960");
	//特级直属代理级别
	public static final String SUPERDIRECTAGENCY = "SUPERDIRECTAGENCY";
	
	//每日盈亏计算终止小时时间
	public static final Integer DAY_COMSUME_END_HOUR = 3;
	//每日盈亏计算终止分钟时间
	public static final Integer DAY_COMSUME_END_MINUTE = 0;
	
	//日志下载文件路径
	public static final	 String FILE_PATH="/usr/local/tomcat/apache-tomcat-6.0.43/logs";
	
	//页面显示条数
	public static final Integer DOMAIN_COUNT=5;
	
	
	public static final String DOMAIN_HEAD = "http://";

	//有效投注额 
	public static final String LOTTERY_EFFECTIVE = "LOTTERY_EFFECTIVE";
	
	//存储redis最大开奖号码最大记录数
	public static final int REDIS_CODE_MAX_RECORD = 600;
	
	/**
	 * 图片上传处理资源路径 
	 * 配合图片上传URL SystemConfigConstant.imgServerUploadUrl+ConstantUtil.FILE_UPLOAD_RESOURCE
	 */
	public static final String FILE_UPLOAD_RESOURCE = "/fileUpload";
	
	/**
	 * 图片上传富文本处理资源路径
	 * 配合图片上传URL SystemConfigConstant.imgServerUploadUrl+ConstantUtil.FILE_UPLOAD_RICHTEXT_RESOURCE
	 */
	public static final String FILE_UPLOAD_RICHTEXT_RESOURCE = "/upload_json.jsp";
	
	/**
	 * session的时间戳
	 */
	public static final String RE_LOGIN_TIMESTAMP = "";

	public static final String INITIALIZATION_PASSWORD = "a123456";
	
}
