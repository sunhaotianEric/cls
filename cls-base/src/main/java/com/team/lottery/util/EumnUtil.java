package com.team.lottery.util;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.reflect.MethodUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.enums.ECodeTrendKind;
import com.team.lottery.enums.ELotteryKind;
import com.team.lottery.enums.ELotteryTopKind;
import com.team.lottery.extvo.SubKindCodePlanVO;
import com.team.lottery.extvo.SubKindVO;
/**
 * 
 * <strong>功能:</strong>枚举使用工具
 * <strong>作者：</strong>Gary Huang
 * <strong>日期:</strong> 2014-3-5 
 * <strong>版权：<strong>版权所有(C) 2014，QQ 834865081
 */
public class EumnUtil {

	private static Logger logger = LoggerFactory.getLogger(EumnUtil.class);

	public static String getDescription(Class<?> ref , String code){
		return parseEumn(ref).get(code); 
	}
	
	public static <T> Map<String, String> parseEumn(Class<T> ref){
		Map<String, String> map = new LinkedHashMap<String, String>();
		if(ref.isEnum()){
			T[] ts = ref.getEnumConstants() ; 
			for(T t : ts){
				String text = getInvokeValue(t, "getDescription"); 
				Enum<?> tempEnum = (Enum<?>) t ;
				if(text == null){
					text = tempEnum.name() ;
				}
				String code = getInvokeValue(t, "getCode"); 
				if(code != null){
					map.put(code , text); 
				}
			}
		}
		return map ;
	}
	
	public static <T> List<SubKindCodePlanVO> parseEumnCodePlan(Class<T> ref){
		List<SubKindCodePlanVO> s = new ArrayList<SubKindCodePlanVO>();
		if(ref.isEnum()){
			T[] ts = ref.getEnumConstants() ; 
			for(T t : ts){
				String description = getInvokeValue(t, "getDescription"); 
				Enum<?> tempEnum = (Enum<?>) t ;
				if(description == null){
					description = tempEnum.name() ;
				}
				String code = getInvokeValue(t, "getCode"); 
				String serviceClassName = getInvokeValue(t, "getServiceClassName"); 
				if(StringUtils.isNotBlank(code) && StringUtils.isNotBlank(serviceClassName)){
					SubKindCodePlanVO subKindCodePlanVO = new SubKindCodePlanVO(code, description, serviceClassName);
					s.add(subKindCodePlanVO);
				}
			}
		}
		return s;
	}
	
	public static <T> String parseEumnCodePlan(Class<T> ref,String playkind){
		String serviceClassName = "";
		if(ref.isEnum()){
			T[] ts = ref.getEnumConstants() ; 
			for(T t : ts){
				String code = getInvokeValue(t, "getCode"); 
				if (code.equals(playkind)) {
					serviceClassName = getInvokeValue(t, "getServiceClassName"); 
					return serviceClassName;
				}else {
					continue;
				}
			}
		}
		return serviceClassName;
	}
	
	static <T> String getInvokeValue(T t , String methodName){
		try {
			Method method = MethodUtils.getAccessibleMethod(t.getClass() , methodName); 
			String text = (String)method.invoke(t) ; 
			return text ;
		} catch (Exception e) {
			return null ;
		}
	}
	
	/**
	 * 获取枚举类名下对应code的描述description
	 * @param enumClassName
	 * @param code
	 * @return
	 */
	public static String getDescription(String enumClassName , String code){
		try {
			Class clazz = Class.forName("com.team.lottery.enums." + enumClassName);
			return getDescription(clazz,code); 
		} catch (ClassNotFoundException e) {
			logger.error("查找枚举类：" + enumClassName +"出错", e);
		}
		return "";
	}
	
	/**
	 * 获取枚举类名下的所有描述列表
	 * @param enumClassName
	 * @return
	 */
	public static List<String> getDescriptions(String enumClassName){
		List<String> descs = new ArrayList<String>();
		try {
			Class clazz = Class.forName("com.team.lottery.enums." + enumClassName);
			Map<String, String> map = parseEumn(clazz);
			if(map != null) {
				Iterator<String> iter = map.values().iterator();
				while(iter.hasNext()) {
					descs.add(iter.next());
				}
			}
		} catch (ClassNotFoundException e) {
			logger.error("查找枚举类：" + enumClassName +"出错", e);
		}
		return descs;
	}
	
	/**
	 * 计划列表中根据彩种获取对应的实现类
	 * @param lotteryType
	 * @return
	 */
	public static String getClassNameByLotteryType(String lotteryType){
		String className = "";
		if (lotteryType != null) {
			if (lotteryType.contains("PK10") || lotteryType.equals(ELotteryKind.XYFT.getCode())) {
				className= "E" + "PK10" + "Kind";
			}else if (lotteryType.contains("DPC")) {
				className= "E" + "DPC" + "Kind";
			}else if (lotteryType.contains("KS")) {
				className= "E" + "KS" + "Kind";
			}else if (lotteryType.contains("LHC")) {
				className= "E" + "LHC" + "Kind";
			}else if (lotteryType.contains("SSC")) {
				className= "E" + "SSC" + "Kind";
			}else if (lotteryType.contains("SYXW")) {
				className= "E" + "SYXW" + "Kind";
			}else if (lotteryType.contains("XYEB")) {
				className= "E" + "XYEB" + "Kind";
			}else {
				className = "E" + ELotteryKind.valueOf(lotteryType).getCode() + "Kind";
			}
		}
		return className;
	}
	
	/**
	 * 计划列表中根据彩种获取对应的实现类
	 * @param lotteryType
	 * @return
	 */
	public static String getClassNameByLotteryTypeCodePlan(String lotteryType){
		String className = "";
		if (lotteryType != null) {
			if (lotteryType.contains(ELotteryTopKind.PK10.getCode())
					|| lotteryType.equals(ELotteryKind.XYFT.getCode())) {
				className= "E" + ELotteryTopKind.PK10.getCode() + "KindCodePlan";
			}else if (lotteryType.contains(ELotteryTopKind.DPC.getCode())) {
				className= "E" + ELotteryTopKind.DPC.getCode() + "KindCodePlan";
			}else if (lotteryType.contains(ELotteryTopKind.KS.getCode())) {
				className= "E" + ELotteryTopKind.KS.getCode() + "KindCodePlan";
			}else if (lotteryType.contains(ELotteryTopKind.LHC.getCode())) {
				className= "E" + ELotteryTopKind.LHC.getCode() + "KindCodePlan";
			}else if (lotteryType.contains(ELotteryTopKind.SSC.getCode())) {
				className= "E" + ELotteryTopKind.SSC.getCode() + "KindCodePlan";
			}else if (lotteryType.contains(ELotteryTopKind.SYXW.getCode())) {
				className= "E" + ELotteryTopKind.SYXW.getCode() + "KindCodePlan";
			}else if (lotteryType.contains(ELotteryTopKind.XYEB.getCode())) {
				className= "E" + ELotteryTopKind.XYEB.getCode() + "KindCodePlan";
			}else {
				className = "E" + ELotteryKind.valueOf(lotteryType).getCode() + "KindCodePlan";
			}
		}
		return className;
	}
	
	/**
	 * 获取枚举类名下的所有描述和Code
	 * @param enumClassName
	 * @return
	 */
	public static List<SubKindVO> getSubKinds(String enumClassName){
		List<SubKindVO> subKinds = new ArrayList<SubKindVO>();
		try {
			Class clazz = Class.forName("com.team.lottery.enums." + enumClassName);
			Map<String, String> map = parseEumn(clazz);
			if(map != null) {
				Iterator<Map.Entry<String, String>> iter = map.entrySet().iterator();
				while(iter.hasNext()) {
					Map.Entry<String, String> entry = iter.next();
					SubKindVO subKind = new SubKindVO(entry.getKey(), entry.getValue());
					subKinds.add(subKind);
				}
			}
		} catch (ClassNotFoundException e) {
			logger.error("查找枚举类：" + enumClassName +"出错", e);
		}
		return subKinds;
	}
	
	/**
	 * 获取枚举类名下的所有描述和Code
	 * @param enumClassName
	 * @return
	 */
	public static List<SubKindCodePlanVO> getSubKindsCodePlan(String enumClassName){
		List<SubKindCodePlanVO> s = new ArrayList<SubKindCodePlanVO>();
		try {
			Class clazz = Class.forName("com.team.lottery.enums." + enumClassName);
			s = parseEumnCodePlan(clazz);
		} catch (ClassNotFoundException e) {
			logger.error("查找枚举类：" + enumClassName +"出错", e);
		}
		return s;
	}
	
	
	/**
	 * 获取枚举类名下的所有描述和Code
	 * @param enumClassName
	 * @return
	 */
	public static String getServiceClassName(String enumClassName,String playkind){
		String s = "";
		try {
			Class clazz = Class.forName("com.team.lottery.enums." + enumClassName);
			s = parseEumnCodePlan(clazz,playkind);
		} catch (ClassNotFoundException e) {
			logger.error("查找枚举类：" + enumClassName +"出错", e);
		}
		return s;
	}
	
	public static String getServiceClassNameCodeTrend(String lotteryName){
		for (ECodeTrendKind e : ECodeTrendKind.values()) {
			if (lotteryName.equals(ELotteryKind.XYFT.getCode())) {
				return ECodeTrendKind.PK10.getServiceClassName();
			}
			if (lotteryName.endsWith(e.getCode())) {
				return e.getServiceClassName();
			}
		}
		return "";
	}
	
}

