package com.team.lottery.util.sms;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URLDecoder;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.SimpleHttpConnectionManager;
import org.apache.commons.httpclient.URI;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.apache.commons.httpclient.params.HttpMethodParams;

/**
 * 海客信使短信发送实现类.
 * 
 * @author Jamine
 *
 */
public class SmsSendHxImpl {
	
	// 静态常量.(该条标识可以批量发送短信).
	public static final String COMMAND = "MULTI_MT_REQUEST";

	/**
	 * 发送接口
	 * 
	 * @param apiUrl
	 *            接口API发送地址.
	 * @param userName
	 *            用户Api名称(需要进入海客信使平台-API账号设置中查看:https://api.heysky.com/).
	 * @param password
	 *            用户Api密码(需要进入海客信使平台-API账号设置中查看:https://api.heysky.com/).
	 * @param content
	 *            需要发送的内容(国际接口无模板要求).
	 * @param phones
	 *            需要发送的手机号码(可以批量发送,使用使用","隔开).
	 * @return 返回值为1的时候表示发送成功,返回值为0的时候表示发送失败.
	 */
	public static String sendSms(String apiUrl, String userName, String password, String content, String phones) {

		// 用于接收post请求过后返回回来的值.
		String sendPost = "";
		// 用于做切割赋值.
		String result = "";
		try {
			// 海信发送POST请求
			sendPost = sendPost(apiUrl, SmsSendHxImpl.COMMAND, userName, password, phones, content);
			// 截取返回结果的最后三位.如果等于000就表示发送成功.
			if (sendPost.length() > 3) {
				result = sendPost.substring(sendPost.length() - 3, sendPost.length());
				if (result.equals("000")) {
					return "1";
				} else {
					return "0";
				}
			} else {
				return "0";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 海信POST发送接口实现.
	 * 
	 * @param url
	 *            短信发送接口地址
	 * @param command
	 *            操作类型
	 * @param cpid
	 *            账号
	 * @param cppwd
	 *            密码
	 * @param da
	 *            要发送的手机号
	 * @param sm
	 *            要发送的类容
	 * @return
	 * @throws Exception
	 */
	public static String sendPost(String url, String command, String cpid, String cppwd, String da, String sm) throws Exception {

		HttpClient client = new HttpClient(new HttpClientParams(), new SimpleHttpConnectionManager());
		PostMethod method = new PostMethod();
		try {
			URI base = new URI(url, false);
			method.setURI(new URI(base, "", false));
			method.setRequestBody(new NameValuePair[] { new NameValuePair("command", command), new NameValuePair("cpid", cpid), new NameValuePair("cppwd", cppwd),
					new NameValuePair("da", da), new NameValuePair("sm", sm) });
			HttpMethodParams params = new HttpMethodParams();
			params.setContentCharset("UTF-8");
			method.setParams(params);
			int result = client.executeMethod(method);
			if (result == HttpStatus.SC_OK) {
				InputStream in = method.getResponseBodyAsStream();
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				byte[] buffer = new byte[1024];
				int len = 0;
				while ((len = in.read(buffer)) != -1) {
					baos.write(buffer, 0, len);
				}
				in.close();// 1
				baos.close();// 2
				return URLDecoder.decode(baos.toString(), "UTF-8");
			} else {
				throw new Exception("HTTP ERROR Status: " + method.getStatusCode() + ":" + method.getStatusText());
			}
		} finally {
			method.releaseConnection();
		}
	}

	/**
	 * 测试发送短信.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		String sendSms = sendSms("http://api2.santo.cc/submit", "ct4qbp", "wSBtZJzZ", "最新通知: 红红火火出现异常,请及时处理", "8613099864648,639158783038");
		System.out.println(sendSms);
	}

}
