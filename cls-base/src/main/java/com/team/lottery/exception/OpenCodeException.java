package com.team.lottery.exception;

public class OpenCodeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public OpenCodeException(String msg) {
		super(msg);
	}

	public OpenCodeException(String msg, Throwable e) {
		super(msg, e);
	}
}
