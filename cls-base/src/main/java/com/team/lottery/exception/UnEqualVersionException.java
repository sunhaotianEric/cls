package com.team.lottery.exception;

public class UnEqualVersionException extends Exception {

	public UnEqualVersionException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
