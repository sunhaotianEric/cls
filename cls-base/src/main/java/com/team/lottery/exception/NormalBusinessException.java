package com.team.lottery.exception;

/**
 * 业务正常的异常抛出
 *
 */
public class NormalBusinessException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 10L;
	
	public NormalBusinessException(String msg) {
		super(msg);
	}
	
	public NormalBusinessException(String msg, Throwable e) {
		super(msg, e);
	}

}
