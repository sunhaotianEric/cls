package com.team.lottery.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 投注页面相关路径
 * 
 * @author luocheng
 *
 */
@Controller
public class LotteryPageController {
	// ---------------------投注界面------以及对应的走势图------------------- //
	/**
	 * 重庆时时彩.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/cqssc/cqssc.html", method = RequestMethod.GET)
	public String toCqssc() {
		return "front/lottery/cqssc/cqssc";
	}

	/**
	 * 重庆时时彩走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/cqssc/cqssc_zst.html", method = RequestMethod.GET)
	public String toCqsscZst() {
		return "front/lottery/cqssc/cqssc_zst";
	}

	/**
	 * 重庆时时彩规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/cqssc/playrule_cqssc.html", method = RequestMethod.GET)
	public String toplayRuleCqssc() {
		return "front/lottery/cqssc/playrule_cqssc";
	}

	/**
	 * 安徽快三.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/ahks/ahks.html", method = RequestMethod.GET)
	public String toAhks() {
		return "front/lottery/ahks/ahks";
	}

	/**
	 * 安徽快三走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/ahks/ahks_zst.html", method = RequestMethod.GET)
	public String toAhksZst() {
		return "front/lottery/ahks/ahks_zst";
	}

	/**
	 * 安徽快三规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/ahks/playrule_ahks.html", method = RequestMethod.GET)
	public String toPlayRuleAhks() {
		return "front/lottery/ahks/playrule_ahks";
	}

	/**
	 * 北京快三.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/bjks/bjks.html", method = RequestMethod.GET)
	public String toBjks() {
		return "front/lottery/bjks/bjks";
	}

	/**
	 * 北京快三走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/bjks/bjks_zst.html", method = RequestMethod.GET)
	public String toBjksZxt() {
		return "front/lottery/bjks/bjks_zst";
	}

	/**
	 * 北京快三规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/bjks/playrule_bjks.html", method = RequestMethod.GET)
	public String toPlayRuleBjks() {
		return "front/lottery/bjks/playrule_bjks";
	}

	/**
	 * 北京PK10.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/bjpks/bjpk10.html", method = RequestMethod.GET)
	public String toBjpks() {
		return "front/lottery/bjpks/bjpk10";
	}

	/**
	 * 北京PK10走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/bjpks/bjpk10_zst.html", method = RequestMethod.GET)
	public String toBjpksZst() {
		return "front/lottery/bjpks/bjpk10_zst";
	}

	/**
	 * 北京PK10规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/bjpks/playrule_bjpk10.html", method = RequestMethod.GET)
	public String toPlayRuleBjpks() {
		return "front/lottery/bjpks/playrule_bjpk10";
	}
	
	/**
	 * 幸运飞艇.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/xyft/xyft.html", method = RequestMethod.GET)
	public String toXyft() {
		return "front/lottery/xyft/xyft";
	}
	
	/**
	 * 幸运飞艇走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/xyft/xyft_zst.html", method = RequestMethod.GET)
	public String toXyftZst() {
		return "front/lottery/xyft/xyft_zst";
	}
	
	/**
	 * 幸运飞艇规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/xyft/playrule_xyft.html", method = RequestMethod.GET)
	public String toPlayRuleXyft() {
		return "front/lottery/xyft/playrule_xyft";
	}

	/**
	 * 福彩3D.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/fcsddpc/fcsddpc.html", method = RequestMethod.GET)
	public String toFcsddpc() {
		return "front/lottery/fcsddpc/fcsddpc";
	}

	/**
	 * 福彩3D走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/fcsddpc/fcsddpc_zst.html", method = RequestMethod.GET)
	public String toFcsddpcZst() {
		return "front/lottery/fcsddpc/fcsddpc_zst";
	}

	/**
	 * 福彩3D规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/fcsddpc/playrule_fc3d.html", method = RequestMethod.GET)
	public String toPlayRuleFcsddpc() {
		return "front/lottery/fcsddpc/playrule_fc3d";
	}

	/**
	 * 福建十一选五.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/fjsyxw/fjsyxw.html", method = RequestMethod.GET)
	public String toFjsyxw() {
		return "front/lottery/fjsyxw/fjsyxw";
	}

	/**
	 * 福建十一选五走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/fjsyxw/fjsyxw_zst.html", method = RequestMethod.GET)
	public String toFjsyxwZst() {
		return "front/lottery/fjsyxw/fjsyxw_zst";
	}

	/**
	 * 福建十一选五规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/fjsyxw/playrule_fjsyxw.html", method = RequestMethod.GET)
	public String toPlayRuleFjsyxw() {
		return "front/lottery/fjsyxw/playrule_fjsyxw";
	}

	/**
	 * 广东十一选五.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/gdsyxw/gdsyxw.html", method = RequestMethod.GET)
	public String toGdsyxw() {
		return "front/lottery/gdsyxw/gdsyxw";
	}

	/**
	 * 广东十一选五走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/gdsyxw/gdsyxw_zst.html", method = RequestMethod.GET)
	public String toGdsyxwZst() {
		return "front/lottery/gdsyxw/gdsyxw_zst";
	}

	/**
	 * 广东十一选五规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/gdsyxw/playrule_gdsyxw.html", method = RequestMethod.GET)
	public String toPlayRuleGdsyxw() {
		return "front/lottery/gdsyxw/playrule_gdsyxw";
	}
	
	/**
	 * 五分十一选五.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/wfsyxw/wfsyxw.html", method = RequestMethod.GET)
	public String toWfsyxw() {
		return "front/lottery/wfsyxw/wfsyxw";
	}

	/**
	 * 五分十一选五走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/wfsyxw/wfsyxw_zst.html", method = RequestMethod.GET)
	public String toWfsyxwZst() {
		return "front/lottery/wfsyxw/wfsyxw_zst";
	}

	/**
	 * 五分十一选五规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/wfsyxw/playrule_wfsyxw.html", method = RequestMethod.GET)
	public String toPlayRuleWfsyxw() {
		return "front/lottery/wfsyxw/playrule_wfsyxw";
	}
	
	/**
	 * 三分十一选五.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/sfsyxw/sfsyxw.html", method = RequestMethod.GET)
	public String toSfsyxw() {
		return "front/lottery/sfsyxw/sfsyxw";
	}

	/**
	 * 三分十一选五走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/sfsyxw/sfsyxw_zst.html", method = RequestMethod.GET)
	public String toSfsyxwZst() {
		return "front/lottery/sfsyxw/sfsyxw_zst";
	}

	/**
	 * 三分十一选五规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/sfsyxw/playrule_sfsyxw.html", method = RequestMethod.GET)
	public String toPlayRuleSfsyxw() {
		return "front/lottery/sfsyxw/playrule_sfsyxw";
	}

	/**
	 * 甘肃快三.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/gsks/gsks.html", method = RequestMethod.GET)
	public String toGsks() {
		return "front/lottery/gsks/gsks";
	}

	/**
	 * 甘肃快三走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/gsks/gsks_zst.html", method = RequestMethod.GET)
	public String toGsksZst() {
		return "front/lottery/gsks/gsks_zst";
	}

	/**
	 * 甘肃快三规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/gsks/playrule_gsks.html", method = RequestMethod.GET)
	public String toPlayRuleGsks() {
		return "front/lottery/gsks/playrule_gsks";
	}

	/**
	 * 广西快三.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/gxks/gxks.html", method = RequestMethod.GET)
	public String toGxks() {
		return "front/lottery/gxks/gxks";
	}

	/**
	 * 广西快三走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/gxks/gxks_zst.html", method = RequestMethod.GET)
	public String toGxksZst() {
		return "front/lottery/gxks/gxks_zst";
	}

	/**
	 * 广西快三规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/gxks/playrule_gxks.html", method = RequestMethod.GET)
	public String toPlayRuleGxks() {
		return "front/lottery/gxks/playrule_gxks";
	}

	/**
	 * 湖北快三.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/hbks/hbks.html", method = RequestMethod.GET)
	public String toHbks() {
		return "front/lottery/hbks/hbks";
	}

	/**
	 * 湖北快三走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/hbks/hbks_zst.html", method = RequestMethod.GET)
	public String toHbksZst() {
		return "front/lottery/hbks/hbks_zst";
	}

	/**
	 * 湖北快三规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/hbks/playrule_hbks.html", method = RequestMethod.GET)
	public String toPlayRuleHbks() {
		return "front/lottery/hbks/playrule_hbks";
	}

	/**
	 * 吉林快三.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/jlks/jlks.html", method = RequestMethod.GET)
	public String toJlks() {
		return "front/lottery/jlks/jlks";
	}

	/**
	 * 吉林快三走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/jlks/jlks_zst.html", method = RequestMethod.GET)
	public String toJlksZst() {
		return "front/lottery/jlks/jlks_zst";
	}

	/**
	 * 吉林快三规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/jlks/playrule_jlks.html", method = RequestMethod.GET)
	public String toPlayRuleJlks() {
		return "front/lottery/jlks/playrule_jlks";
	}

	/**
	 * 江苏快三.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/jsks/jsks.html", method = RequestMethod.GET)
	public String toJsks() {
		return "front/lottery/jsks/jsks";
	}

	/**
	 * 江苏快三走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/jsks/jsks_zst.html", method = RequestMethod.GET)
	public String toJsksZst() {
		return "front/lottery/jsks/jsks_zst";
	}

	/**
	 * 江苏快三规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/jsks/playrule_jsks.html", method = RequestMethod.GET)
	public String toPlayRuleJsks() {
		return "front/lottery/jsks/playrule_jsks";
	}

	/**
	 * 极速PK10.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/jspks/jspk10.html", method = RequestMethod.GET)
	public String toJspk10() {
		return "front/lottery/jspks/jspk10";
	}

	/**
	 * 极速PK10走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/jspks/jspk10_zst.html", method = RequestMethod.GET)
	public String toJspk10Zst() {
		return "front/lottery/jspks/jspk10_zst";
	}

	/**
	 * 极速PK10规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/jspks/playrule_jspk10.html", method = RequestMethod.GET)
	public String toPlayRuleJspk10() {
		return "front/lottery/jspks/playrule_jspk10";
	}
	
	/**
	 * 三分PK10.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/sfpk10/sfpk10.html", method = RequestMethod.GET)
	public String toSfpk10() {
		return "front/lottery/sfpk10/sfpk10";
	}
	
	/**
	 * 三分PK10走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/sfpk10/sfpk10_zst.html", method = RequestMethod.GET)
	public String toSfpk10Zst() {
		return "front/lottery/sfpk10/sfpk10_zst";
	}
	
	/**
	 * 三分PK10规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/sfpk10/playrule_sfpk10.html", method = RequestMethod.GET)
	public String toPlayRuleSfpk10() {
		return "front/lottery/sfpk10/playrule_sfpk10";
	}
	
	/**
	 * 五分PK10.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/wfpk10/wfpk10.html", method = RequestMethod.GET)
	public String toWfpk10() {
		return "front/lottery/wfpk10/wfpk10";
	}
	
	/**
	 * 五分PK10走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/wfpk10/wfpk10_zst.html", method = RequestMethod.GET)
	public String toWfpk10Zst() {
		return "front/lottery/wfpk10/wfpk10_zst";
	}
	
	/**
	 * 五分PK10规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/wfpk10/playrule_wfpk10.html", method = RequestMethod.GET)
	public String toPlayRuleWfpk10() {
		return "front/lottery/wfpk10/playrule_wfpk10";
	}
	
	/**
	 * 十分PK10.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/shfpk10/shfpk10.html", method = RequestMethod.GET)
	public String toShfpk10() {
		return "front/lottery/shfpk10/shfpk10";
	}
	
	/**
	 * 十分PK10走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/shfpk10/shfpk10_zst.html", method = RequestMethod.GET)
	public String toShfpk10Zst() {
		return "front/lottery/shfpk10/shfpk10_zst";
	}
	
	/**
	 * 十分PK10规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/shfpk10/playrule_shfpk10.html", method = RequestMethod.GET)
	public String toPlayRuleShfpk10() {
		return "front/lottery/shfpk10/playrule_shfpk10";
	}

	/**
	 * 江西11选5.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/jxsyxw/jxsyxw.html", method = RequestMethod.GET)
	public String toJxsyxw() {
		return "front/lottery/jxsyxw/jxsyxw";
	}

	/**
	 * 江西11选5走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/jxsyxw/jxsyxw_zst.html", method = RequestMethod.GET)
	public String toJxsyxwZst() {
		return "front/lottery/jxsyxw/jxsyxw_zst";
	}

	/**
	 * 江西11选5规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/jxsyxw/playrule_jxsyxw.html", method = RequestMethod.GET)
	public String toPlayRuleJxsyxw() {
		return "front/lottery/jxsyxw/playrule_jxsyxw";
	}

	/**
	 * 幸运快三.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/jyks/jyks.html", method = RequestMethod.GET)
	public String toJyks() {
		return "front/lottery/jyks/jyks";
	}

	/**
	 * 幸运快三走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/jyks/jyks_zst.html", method = RequestMethod.GET)
	public String toJyksZst() {
		return "front/lottery/jyks/jyks_zst";
	}

	/**
	 * 幸运快三规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/jyks/playrule_jyks.html", method = RequestMethod.GET)
	public String toPlayRuleJyks() {
		return "front/lottery/jyks/playrule_jyks";
	}

	/**
	 * 六合彩.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/lhc/lhc.html", method = RequestMethod.GET)
	public String toLhc() {
		return "front/lottery/lhc/lhc";
	}

	/**
	 * 六合彩走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/lhc/zst_liuhecai.html", method = RequestMethod.GET)
	public String toLhcZst() {
		return "front/lottery/lhc/zst_liuhecai";
	}
	
	/**
	 * 六合彩玩法规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/lhc/playrule_lhc.html", method = RequestMethod.GET)
	public String toPlayRuleLhc() {
		return "front/lottery/lhc/playrule_lhc";
	}

	/**
	 * 山东十一选五.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/sdsyxw/sdsyxw.html", method = RequestMethod.GET)
	public String toSdsyxw() {
		return "front/lottery/sdsyxw/sdsyxw";
	}

	/**
	 * 山东十一选五走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/sdsyxw/sdsyxw_zst.html", method = RequestMethod.GET)
	public String toSdsyxwZst() {
		return "front/lottery/sdsyxw/sdsyxw_zst";
	}

	/**
	 * 山东十一选五规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/sdsyxw/playrule_sdsyxw.html", method = RequestMethod.GET)
	public String toPlayRuleSdsyxw() {
		return "front/lottery/sdsyxw/playrule_sdsyxw";
	}

	/**
	 * 上海快三.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/shks/shks.html", method = RequestMethod.GET)
	public String toShks() {
		return "front/lottery/shks/shks";
	}

	/**
	 * 上海快三走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/shks/shks_zst.html", method = RequestMethod.GET)
	public String toShksZst() {
		return "front/lottery/shks/shks_zst";
	}

	/**
	 * 上海快三规则图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/shks/playrule_shks.html", method = RequestMethod.GET)
	public String toPlayRuleShks() {
		return "front/lottery/shks/playrule_shks";
	}

	/**
	 * 天津时时彩.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/tjssc/tjssc.html", method = RequestMethod.GET)
	public String toTjssc() {
		return "front/lottery/tjssc/tjssc";
	}

	/**
	 * 天津时时彩走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/tjssc/tjssc_zst.html", method = RequestMethod.GET)
	public String toTjsscZst() {
		return "front/lottery/tjssc/tjssc_zst";
	}

	/**
	 * 天津时时彩规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/tjssc/playrule_tjssc.html", method = RequestMethod.GET)
	public String toPlayRuleTjssc() {
		return "front/lottery/tjssc/playrule_tjssc";
	}

	/**
	 * 台湾五分彩.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/twwfc/twwfc.html", method = RequestMethod.GET)
	public String toTwwfc() {
		return "front/lottery/twwfc/twwfc";
	}

	/**
	 * 台湾五分彩走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/twwfc/twwfc_zst.html", method = RequestMethod.GET)
	public String toTwwfcZst() {
		return "front/lottery/twwfc/twwfc_zst";
	}

	/**
	 * 台湾五分彩规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/twwfc/playrule_twwfc.html", method = RequestMethod.GET)
	public String toPlayRuleTwwfc() {
		return "front/lottery/twwfc/playrule_twwfc";
	}

	/**
	 * 新疆时时彩.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/xjssc/xjssc.html", method = RequestMethod.GET)
	public String toXjssc() {
		return "front/lottery/xjssc/xjssc";
	}

	/**
	 * 新疆时时彩走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/xjssc/xjssc_zst.html", method = RequestMethod.GET)
	public String toXjsscZst() {
		return "front/lottery/xjssc/xjssc_zst";
	}

	/**
	 * 新疆时时彩规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/xjssc/playrule_xjssc.html", method = RequestMethod.GET)
	public String toPlayRuleXjssc() {
		return "front/lottery/xjssc/playrule_xjssc";
	}

	/**
	 * 幸运28.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/xyeb/xy28.html", method = RequestMethod.GET)
	public String toXyeb() {
		return "front/lottery/xyeb/xy28";
	}

	/**
	 * 幸运28走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/xyeb/xyeb_zst.html", method = RequestMethod.GET)
	public String toXyebZst() {
		return "front/lottery/xyeb/xyeb_zst";
	}

	/**
	 * 幸运28规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/xyeb/playrule_xyeb.html", method = RequestMethod.GET)
	public String toPlayRuleXyeb() {
		return "front/lottery/xyeb/playrule_xyeb";
	}

	/**
	 * 幸运分分彩.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/jyffc/jyffc.html", method = RequestMethod.GET)
	public String toXyffc() {
		return "front/lottery/jyffc/jyffc";
	}
	
	/**
	 * 十分时时彩.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/shfssc/shfssc.html", method = RequestMethod.GET)
	public String toShfssc() {
		return "front/lottery/shfssc/shfssc";
	}
	
	/**
	 * 十分时时彩走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/shfssc/shfssc_zst.html", method = RequestMethod.GET)
	public String toShfsscZst() {
		return "front/lottery/shfssc/shfssc_zst";
	}

	/**
	 * 十分时时彩规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/shfssc/playrule_shfssc.html", method = RequestMethod.GET)
	public String toPlayRuleShfssc() {
		return "front/lottery/shfssc/playrule_shfssc";
	}
	
	/**
	 * 三分时时彩.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/sfssc/sfssc.html", method = RequestMethod.GET)
	public String toSfssc() {
		return "front/lottery/sfssc/sfssc";
	}
	
	/**
	 * 三分时时彩走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/sfssc/sfssc_zst.html", method = RequestMethod.GET)
	public String toSfsscZst() {
		return "front/lottery/sfssc/sfssc_zst";
	}
	
	/**
	 * 三分时时彩规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/sfssc/playrule_sfssc.html", method = RequestMethod.GET)
	public String toPlayRuleSfssc() {
		return "front/lottery/sfssc/playrule_sfssc";
	}
	
	/**
	 * 老重庆时时彩.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/lcqssc/lcqssc.html", method = RequestMethod.GET)
	public String toLcqssc() {
		return "front/lottery/lcqssc/lcqssc";
	}
	
	/**
	 * 老重庆时时彩走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/lcqssc/lcqssc_zst.html", method = RequestMethod.GET)
	public String toLcqsscZst() {
		return "front/lottery/lcqssc/lcqssc_zst";
	}
	
	/**
	 * 老重庆时时彩规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/lcqssc/playrule_lcqssc.html", method = RequestMethod.GET)
	public String toPlayRuleLcqssc() {
		return "front/lottery/lcqssc/playrule_lcqssc";
	}

	/**
	 * 江苏时时彩.
	 *
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/jsssc/jsssc.html", method = RequestMethod.GET)
	public String toJsssc() {
		return "front/lottery/jsssc/jsssc";
	}

	/**
	 * 江苏时时彩走势图.
	 *
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/jsssc/jsssc_zst.html", method = RequestMethod.GET)
	public String toJssscZst() {
		return "front/lottery/jsssc/jsssc_zst";
	}

	/**
	 * 江苏时时彩规则.
	 *
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/jsssc/playrule_jsssc.html", method = RequestMethod.GET)
	public String toPlayRuleJsssc() {
		return "front/lottery/jsssc/playrule_jsssc";
	}

	/**
	 * 北京时时彩.
	 *
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/bjssc/bjssc.html", method = RequestMethod.GET)
	public String toBjssc() {
		return "front/lottery/bjssc/bjssc";
	}

	/**
	 * 北京时时彩走势图.
	 *
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/bjssc/bjssc_zst.html", method = RequestMethod.GET)
	public String toBjsscZst() {
		return "front/lottery/bjssc/bjssc_zst";
	}

	/**
	 * 北京时时彩规则.
	 *
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/bjssc/playrule_bjssc.html", method = RequestMethod.GET)
	public String toPlayRuleBjssc() {
		return "front/lottery/bjssc/playrule_bjssc";
	}

	/**
	 * 广东时时彩.
	 *
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/gdssc/gdssc.html", method = RequestMethod.GET)
	public String toGdssc() {
		return "front/lottery/gdssc/gdssc";
	}

	/**
	 * 广东时时彩走势图.
	 *
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/gdssc/gdssc_zst.html", method = RequestMethod.GET)
	public String toGdsscZst() {
		return "front/lottery/gdssc/gdssc_zst";
	}

	/**
	 * 广东时时彩规则.
	 *
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/gdssc/playrule_gdssc.html", method = RequestMethod.GET)
	public String toPlayRuleGdssc() {
		return "front/lottery/gdssc/playrule_gdssc";
	}

	/**
	 * 四川时时彩.
	 *
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/scssc/scssc.html", method = RequestMethod.GET)
	public String toScssc() {
		return "front/lottery/scssc/scssc";
	}

	/**
	 * 四川时时彩走势图.
	 *
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/scssc/scssc_zst.html", method = RequestMethod.GET)
	public String toScsscZst() {
		return "front/lottery/scssc/scssc_zst";
	}

	/**
	 * 四川时时彩规则.
	 *
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/scssc/playrule_scssc.html", method = RequestMethod.GET)
	public String toPlayRuleScssc() {
		return "front/lottery/scssc/playrule_scssc";
	}

	/**
	 * 上海时时彩.
	 *
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/shssc/shssc.html", method = RequestMethod.GET)
	public String toShssc() {
		return "front/lottery/shssc/shssc";
	}

	/**
	 * 上海时时彩走势图.
	 *
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/shssc/shssc_zst.html", method = RequestMethod.GET)
	public String toShsscZst() {
		return "front/lottery/shssc/shssc_zst";
	}

	/**
	 * 上海时时彩规则.
	 *
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/shssc/playrule_shssc.html", method = RequestMethod.GET)
	public String toPlayRuleShssc() {
		return "front/lottery/shssc/playrule_shssc";
	}

	/**
	 * 山东时时彩.
	 *
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/sdssc/sdssc.html", method = RequestMethod.GET)
	public String toSdssc() {
		return "front/lottery/sdssc/sdssc";
	}

	/**
	 * 山东时时彩走势图.
	 *
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/sdssc/sdssc_zst.html", method = RequestMethod.GET)
	public String toSdsscZst() {
		return "front/lottery/sdssc/sdssc_zst";
	}

	/**
	 * 山东时时彩规则.
	 *
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/sdssc/playrule_sdssc.html", method = RequestMethod.GET)
	public String toPlayRuleSdssc() {
		return "front/lottery/sdssc/playrule_sdssc";
	}

	/**
	 * 五分时时彩.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/wfssc/wfssc.html", method = RequestMethod.GET)
	public String toWfssc() {
		return "front/lottery/wfssc/wfssc";
	}
	
	/**
	 * 五分时时彩走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/wfssc/wfssc_zst.html", method = RequestMethod.GET)
	public String toWfsscZst() {
		return "front/lottery/wfssc/wfssc_zst";
	}

	/**
	 * 五分时时彩规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/wfssc/playrule_wfssc.html", method = RequestMethod.GET)
	public String toPlayRuleWfssc() {
		return "front/lottery/wfssc/playrule_wfssc";
	}
	
	/**
	 * 三分快3.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/sfks/sfks.html", method = RequestMethod.GET)
	public String toSfks() {
		return "front/lottery/sfks/sfks";
	}
	
	/**
	 * 三分快3走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/sfks/sfks_zst.html", method = RequestMethod.GET)
	public String toSfksZst() {
		return "front/lottery/sfks/sfks_zst";
	}
	
	/**
	 * 三分快3规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/sfks/playrule_sfks.html", method = RequestMethod.GET)
	public String toPlayRuleSfks() {
		return "front/lottery/sfks/playrule_sfks";
	}
	
	/**
	 * 五分时时彩.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/wfks/wfks.html", method = RequestMethod.GET)
	public String toWfks() {
		return "front/lottery/wfks/wfks";
	}
	
	/**
	 * 五分时时彩走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/wfks/wfks_zst.html", method = RequestMethod.GET)
	public String toWfksZst() {
		return "front/lottery/wfks/wfks_zst";
	}
	
	/**
	 * 五分时时彩规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/wfks/playrule_wfks.html", method = RequestMethod.GET)
	public String toPlayRuleWfks() {
		return "front/lottery/wfks/playrule_wfks";
	}

	/**
	 * 幸运分分彩走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/jyffc/jyffc_zst.html", method = RequestMethod.GET)
	public String toXyffcZst() {
		return "front/lottery/jyffc/jyffc_zst";
	}

	/**
	 * 幸运分分彩规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/jyffc/playrule_jyffc.html", method = RequestMethod.GET)
	public String toPlayRuleXyffc() {
		return "front/lottery/jyffc/playrule_jyffc";
	}

	/**
	 * 幸运六合彩.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/xylhc/xylhc.html", method = RequestMethod.GET)
	public String toXylhc() {
		return "front/lottery/xylhc/xylhc";
	}

	/**
	 * 幸运六合彩走势图.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/xylhc/zst_liuhecai.html", method = RequestMethod.GET)
	public String toXylhcZst() {
		return "front/lottery/xylhc/zst_liuhecai";
	}

	/**
	 * 幸运六合彩玩法规则.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/lottery/xylhc/playrule_lhc.html", method = RequestMethod.GET)
	public String toPlayRuleXylhc() {
		return "front/lottery/xylhc/playrule_lhc";
	}

}
