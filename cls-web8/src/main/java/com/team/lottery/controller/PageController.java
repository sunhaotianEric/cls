package com.team.lottery.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 手机端页面控制器
 * 
 * @author luocheng
 *
 */
@Controller
public class PageController {

	/**
	 * 跳转首页
	 */
	@RequestMapping(value = "/gameBet/index.html", method = RequestMethod.GET)
	public String toIndexPage() {
		return "front/index";
	}

	/**
	 * 跳转登录页
	 */
	@RequestMapping(value = "/gameBet/login.html", method = RequestMethod.GET)
	public String toLoginPage() {
		return "front/login/login";
	}

	/**
	 * 彩票大厅.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/hall.html", method = RequestMethod.GET)
	public String toLotteryhome() {
		return "front/hall/hall";

	}

	/**
	 * 跳转活动页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/activity.html", method = RequestMethod.GET)
	public String toActivityPage() {
		return "front/activity/activity";
	}

	/**
	 * 新手礼包.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/newUserGift.html", method = RequestMethod.GET)
	public String toNewUserGift() {
		return "front/activity/new_user_gift";

	}

	/**
	 * 帮助.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/help.html", method = RequestMethod.GET)
	public String toHelp() {
		return "front/help/help";

	}

	/**
	 * 手机购彩.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/wap.html", method = RequestMethod.GET)
	public String toWap() {
		return "front/wap/wap";

	}

	/**
	 * 跳转注册
	 */
	@RequestMapping(value = "/gameBet/reg.html", method = RequestMethod.GET)
	public String toRegPage() {
		return "front/reg/reg";
	}

	/**
	 * 忘记密码.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/forgetPwd.html", method = RequestMethod.GET)
	public String toForgetPassword() {
		return "front/forgetpwd/forgetpwd";

	}

	/**
	 * 选择找回方式!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/forgetPwd/chooseWay.html", method = RequestMethod.GET)
	public String toRetrieveMode() {
		return "front/forgetpwd/choose_way";
	}

	/**
	 * 校验找回方式!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/forgetPwd/verifyWay.html", method = RequestMethod.GET)
	public String toGetRetrieveModeMq() {
		return "front/forgetpwd/verify_way";
	}

	/**
	 * 重置密码!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/forgetPwd/resetPwd.html", method = RequestMethod.GET)
	public String toRetrieveResetPwd() {
		return "front/forgetpwd/resetpwd";
	}

	/**
	 * 维护页面.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/maintain.html", method = RequestMethod.GET)
	public String toMatain() {
		return "front/maintain/matain";

	}

	/**
	 * 维护页面.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/404.html", method = RequestMethod.GET)
	public String toMatain404() {
		return "front/maintain/404";

	}

	/**
	 * 监控.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/monitor.html", method = RequestMethod.GET)
	public String toMonitor() {
		return "front/monitor/monitor";

	}

	/**
	 * 跳转个人主页
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/accountCenter.html", method = RequestMethod.GET)
	public String toUserInfoSave() {
		return "front/my/account_center";
	}

	/**
	 * 个人资料
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/myInfo.html", method = RequestMethod.GET)
	public String toMyInfo() {
		return "front/my/my_info";
	}

	/**
	 * PC修改登录密码!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/safeCenter/resetPwd.html", method = RequestMethod.GET)
	public String toChangeLoginPassword() {
		return "front/safecenter/resetpwd";

	}

	/**
	 * PC设置安全密码!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/safeCenter/setSafePwd.html", method = RequestMethod.GET)
	public String toSetSafePassWord() {
		return "front/safecenter/set_safe_pwd";

	}

	/**
	 * 修改安全密码!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/safeCenter/resetSafePwd.html", method = RequestMethod.GET)
	public String toSafePassWordModify() {
		return "front/safecenter/reset_safe_pwd";

	}

	/**
	 * 绑定安全邮箱.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/safeCenter/bindEmail.html", method = RequestMethod.GET)
	public String toBandSafeEmail() {
		return "front/safecenter/bind_email";

	}

	/**
	 * 修改安全邮箱.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/safeCenter/bindEmailChange.html", method = RequestMethod.GET)
	public String toBandSafeEmailChange() {
		return "front/safecenter/bind_email_change";

	}

	/**
	 * 安全手机绑定.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/safeCenter/bindPhone.html", method = RequestMethod.GET)
	public String toChangeSafeMobile() {
		return "front/safecenter/bind_phone";

	}

	/**
	 * 修改绑定手机.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/safeCenter/bindPhoneChange.html", method = RequestMethod.GET)
	public String toChangeSafeMobileChange() {
		return "front/safecenter/bind_phone_change";

	}

	/**
	 * 设置安全问题!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/safeCenter/setSafeQuestion.html", method = RequestMethod.GET)
	public String toSetSafeQuestion() {
		return "front/safequestion/set_safe_question";

	}

	/**
	 * 修改安全问题!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/safeCenter/verifySafeQuestion.html", method = RequestMethod.GET)
	public String toChangeSafeQuestion() {
		return "front/safequestion/verify_safe_question";

	}

	/**
	 * 重新设置安全问题!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/safeCenter/resetSafeQuestion.html", method = RequestMethod.GET)
	public String toResetSafeQuestion() {
		return "front/safequestion/reset_safe_question";

	}

	/**
	 * 充值!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/recharge.html", method = RequestMethod.GET)
	public String toRechargewithdraw() {
		return "front/recharge/recharge";
	}

	/**
	 * 充值列表
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/rechargeOrder.html", method = RequestMethod.GET)
	public String toUserRechargeOrderr() {
		return "/front/recharge/recharge_order";
	}

	/**
	 * 提现!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/withdraw.html", method = RequestMethod.GET)
	public String toTakeCash() {
		return "front/withdraw/withdraw";
	}

	/**
	 * 提现列表
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/withdrawOrder.html", method = RequestMethod.GET)
	public String toUserWithdrawOrder() {
		return "/front/withdraw/withdraw_order";
	}

	/**
	 * 银行卡列表.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/bankCard/bankCardList.html", method = RequestMethod.GET)
	public String toBankCardList() {
		return "front/bankcard/bank_card_list";

	}

	/**
	 * 跳转盈亏报表
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/report/consumeReport.html", method = RequestMethod.GET)
	public String toConsumeReport() {
		return "front/report/consume_report";
	}

	/**
	 * 跳转订单报表，默认代理报表
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/report/agentReport.html", method = RequestMethod.GET)
	public String toAgentReport() {
		return "front/report/agent_report";
	}

	/**
	 * 跳转账变明细
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/moneyDetail.html", method = RequestMethod.GET)
	public String toMoneyDetail() {
		return "front/moneydetail/money_detail";
	}

	/**
	 * 跳转投注明细
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/order.html", method = RequestMethod.GET)
	public String toUserOrder() {
		return "front/order/order";
	}

	/**
	 * 跳转追号记录
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/followOrder.html", method = RequestMethod.GET)
	public String toUserOrderAfternum() {
		return "front/order/follow_order";
	}

	/**
	 * 投注详情.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/orderDetail.html", method = RequestMethod.GET)
	public String toOrderDetail() {
		return "front/order/order_detail";

	}

	/**
	 * 追号详情.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/followOrderDetail.html", method = RequestMethod.GET)
	public String toFollowOrderDetail() {
		return "front/order/follow_order_detail";

	}

	/**
	 * 跳转团队列表
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/agentCenter/userList.html", method = RequestMethod.GET)
	public String toUserTeamListUsers() {
		return "front/user/user_list";
	}

	/**
	 * 会员转账
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/agentCenter/userTransfer.html", method = RequestMethod.GET)
	public String toUserTransfer() {
		return "front/user/user_transfer";

	}

	/**
	 * 会员升点
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/agentCenter/userEdit.html", method = RequestMethod.GET)
	public String toUserEdit2() {
		return "front/user/user_edit";
	}

	/**
	 * 下级开户
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/agentCenter/addUser.html", method = RequestMethod.GET)
	public String toUserAdd() {
		return "front/user/add_user";
	}

	/**
	 * 跳转生成推广
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/agentCenter/addExtend.html", method = RequestMethod.GET)
	public String toAddExtend() {
		return "front/user/add_extend";
	}

	/**
	 * 跳转推广管理,生成点击跳转
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/agentCenter/extendMgr.html", method = RequestMethod.GET)
	public String toExtendMagr() {
		return "front/user/extend_mgr";
	}

	/**
	 * 跳转生成推广的详情页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/agentCenter/extendDetail.html", method = RequestMethod.GET)
	public String toExtendDetail() {
		return "front/user/extend_detail";
	}

	/**
	 * 跳转推广管理的模式调整=推广编辑
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/agentCenter/extendEdit.html", method = RequestMethod.GET)
	public String toExtendEdit() {
		return "front/user/extend_edit";
	}

	/**
	 * 工资记录
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/daysalary/daysalaryOrder.html", method = RequestMethod.GET)
	public String toDaysalaryOrder() {
		return "front/daysalary/daysalary_order";

	}

	/**
	 * 我的日工资
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/daysalary/myDaysalary.html", method = RequestMethod.GET)
	public String toMyDaysalary() {
		return "front/daysalary/my_daysalary";

	}

	/**
	 * 分红记录
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/bonus/bonusOrder.html", method = RequestMethod.GET)
	public String toBonusOrder() {
		return "front/bonus/bonus_order";
	}

	/**
	 * 我的契约
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/bonus/myBonus.html", method = RequestMethod.GET)
	public String toMyBonus() {
		return "front/bonus/my_bonus";
	}

	/**
	 * 分红契约规则
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/bonus/bonusRule.html", method = RequestMethod.GET)
	public String toBonusRule() {
		return "front/bonus/bonus_rule";
	}

	/**
	 * 未读消息!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/message/inbox.html", method = RequestMethod.GET)
	public String toGetNoteList() {
		return "front/message/inbox";
	}

	/**
	 * 发件箱!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/message/outbox.html", method = RequestMethod.GET)
	public String toMySendList() {
		return "front/message/outbox";
	}

	/**
	 * 编写消息!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/message/sendUp.html", method = RequestMethod.GET)
	public String toMyNoteSend() {
		return "front/message/send_up";
	}

	/**
	 * 给下级发消息!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/message/sendDown.html", method = RequestMethod.GET)
	public String toSendToDown() {
		return "front/message/send_down";

	}

	/**
	 * 消息详情!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/message/messageDetail.html", method = RequestMethod.GET)
	public String toGetNoteDetail() {
		return "front/message/message_detail";

	}

	/**
	 * 系统消息详情!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/message/messageSystemDetail.html", method = RequestMethod.GET)
	public String toMessageSystemDetail() {
		return "front/message/message_system_detail";
	}

	/**
	 * 获取公告!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/announce.html", method = RequestMethod.GET)
	public String toGetAnnounce() {
		return "front/announce/announce";

	}

	/**
	 * 获取公告详情!
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/announceDetail.html", method = RequestMethod.GET)
	public String toGetAnnounceDetail() {
		return "front/announce/announce_detail";

	}

	/**
	 * 充值跳转支付第三方post页面
	 * 
	 * @return
	 */
	@RequestMapping(value = "/gameBet/pay/{payType}/post.html", method = { RequestMethod.POST, RequestMethod.GET })
	public String toSkipPay(@PathVariable String payType) {
		String returnPath = "/front/pay/" + payType + "/post";
		return returnPath;
	}

}
