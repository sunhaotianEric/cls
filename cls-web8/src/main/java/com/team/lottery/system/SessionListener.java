package com.team.lottery.system;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.redis.JedisUtils;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.vo.User;

public class SessionListener implements HttpSessionListener{
	
	private static Logger logger = LoggerFactory.getLogger(SessionListener.class);

	@Override
	public void sessionCreated(HttpSessionEvent se) {
		String sessionId = se.getSession().getId();
		if (logger.isDebugEnabled()) {
			logger.debug("sessionCreated sessionId:{}", sessionId);
		}
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		HttpSession session = se.getSession();
		String sessionId = se.getSession().getId();
		if (logger.isDebugEnabled()) {
			logger.debug("sessionDestroyed sessionId:{}", sessionId);
		}
		User loginUser = (User) session.getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
		if (loginUser!=null) {
			String userName = loginUser.getUserName();
			String loginType = loginUser.getLoginType();
			String bizSystem = loginUser.getBizSystem();
			OnlineUserManager.delOnlineUser(loginType, bizSystem, userName, sessionId);
		}
		session.invalidate();
	}

}
