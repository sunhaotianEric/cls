package com.team.lottery.filter;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.team.lottery.cache.ClsCacheManager;
import com.team.lottery.system.BizSystemConfigManager;
import com.team.lottery.system.BizSystemConfigVO;
import com.team.lottery.system.SystemConfigConstant;
import com.team.lottery.util.ConstantUtil;
import com.team.lottery.vo.BizSystem;
import com.team.lottery.vo.BizSystemDomain;


public class SessionUserFilter implements Filter {

	private static Logger log = LoggerFactory.getLogger(SessionUserFilter.class);
	
	//不需要拦截的url放在set当中用set数据防止重复
	private static Set<String> allowUrl=new HashSet<String>();

	@Override
	public void destroy() {
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;   
	    HttpServletResponse res = (HttpServletResponse) response;   
	    HttpSession session = req.getSession();
	    
	    String url = req.getServletPath();
	    String scheme = request.getScheme();
	    String serverName = req.getServerName();
	    int port = req.getServerPort();
	    String basePath = req.getContextPath();
	    String fullUrl = scheme + "://" + serverName + ":" + port + req.getContextPath() + url;
	    log.debug("filter url:{}" + fullUrl);
	    
	    BizSystemDomain domainInfo = ClsCacheManager.getDomainInfoByUrl(serverName);
		
		if(domainInfo != null && domainInfo.getIsHttps() == 1 && !"https".equals(scheme)){
			basePath = "https://" + serverName + basePath;
			
			//进行跳转
			if(req.getQueryString() != null && !"".equals(req.getQueryString())){
				res.sendRedirect(basePath + url + "?" + req.getQueryString());	
			}else{
				res.sendRedirect(basePath+url);
			}
	        return;
		
		}
        //int reqPort = req.getServerPort();
		
		/*log.info("serverName:" + serverName);
		log.info("url:" + url);
		log.info("basePath1:" + basePath1);
		log.info("basePath:" + basePath);
		log.info("reqPort:" + reqPort);*/
		
		//判断是否在限制的ip里面
		/*if(SystemFrontConfigInfo.denyIps.size() > 0) {
			Iterator<String> iterator = SystemFrontConfigInfo.denyIps.iterator();
			while(iterator.hasNext()){
				String denyIp = iterator.next();
				if(serverName.equals(denyIp)) {
					log.info("使用禁止的ip输入["+denyIp+"],禁止访问");
					return;
				}
			}
		}
		
		//判断只能允许的域名访问
		if(SystemFrontConfigInfo.allowDomains.size() > 0) {
			Iterator<String> iterator = SystemFrontConfigInfo.allowDomains.iterator();
			boolean isAllow = false;
			while(iterator.hasNext()){
				String denyIp = iterator.next();
				if(serverName.equals(denyIp)) {
					isAllow = true;
				}
			}
			if(!isAllow) {
				log.info("使用不被允许的域名["+serverName+"],禁止访问");
				return;
			}
		}*/
		
		String bizSystem = ClsCacheManager.getBizSystemByDomainUrl(serverName);
		Boolean isTo404Page = false;
		if(StringUtils.isNotBlank(bizSystem)) {
			BizSystem bizSystemVo = ClsCacheManager.getBizSystemByCode(bizSystem);
			//如果系统停止使用  
			if(bizSystemVo.getEnable() == 0) {
				isTo404Page = true;
			}
		} else {
			//获取到的字符串为空
			isTo404Page = true;
		}
		
		//判断能允许访问的域名
		if(StringUtils.isNotBlank(SystemConfigConstant.allowUrls)) {
			String[] allowUrlStrs = SystemConfigConstant.allowUrls.split(",");
			for(String allowStrs : allowUrlStrs) {
				if(allowStrs.equals(serverName)) {
					isTo404Page = false;
				}
			}
		}
		
		if(isTo404Page) {
			//若识别不到业务系统，则跳转404页面
			if(!url.contains("404.html")) {
				res.sendRedirect(basePath+"/404.html");
				return;
			}
		} else {
			//网站维护判断
			String maintainSwich = SystemConfigConstant.maintainSwich;
			if(!url.contains("maintain.html") && "1".equals(maintainSwich)){//开启全局维护
				res.sendRedirect(basePath+"/gameBet/maintain.html");
				return;
			}else{
				BizSystemConfigVO bizSystemConfig = BizSystemConfigManager.getBizSystemConfig(bizSystem);
				//系统维护中
				if(!url.contains("maintain.html") && bizSystemConfig != null && 
						bizSystemConfig.getMaintainSwich() != null && bizSystemConfig.getMaintainSwich() == 1) {
					res.sendRedirect(basePath+"/gameBet/maintain.html");
					return;
				}
			}
		}
		
		//邀请码处理
		String invitationCode = request.getParameter("code");
		if(StringUtils.isNotBlank(invitationCode)) {
			String sessionInvitationCode = (String)session.getAttribute(ConstantUtil.INVITAION_CODE);
			//之前session中没有设置过，则设置新的
			if(StringUtils.isBlank(sessionInvitationCode)) {
				session.setAttribute(ConstantUtil.INVITAION_CODE, invitationCode);
			} else {
				//之前session有设置过,比较邀请码是不是新的，不一致则设置新的
				if(!invitationCode.equals(sessionInvitationCode)) {
					session.setAttribute(ConstantUtil.INVITAION_CODE, invitationCode);
				}
			}
		}
		
		//网站端登录页面
		String indexPageUrl = basePath+"/gameBet/index.html";
		
		//如果直接访问jsp文件，则直接拦截到登陆页面 
		if(url.contains(".jsp") && !url.contains("returnurl.jsp") && !url.contains("pageurl.jsp") && !url.contains("index.jsp")){
		    res.sendRedirect(indexPageUrl);
		    return;
		}
		
		if(!isInAllowUrl(url)) {
			//如果用户没有登录;则返回登录页面!
			if(session.getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER) == null){
		    	res.sendRedirect(indexPageUrl);
		        return;
		    }
		}
	    chain.doFilter(request, response); 
	    
	}
	
	/**
	 * url判断方法：foreach遍历set元素与传入的url进行判断如果有包含的话返回true;
	 * 如果遍历完了没有包含的话返回false!
	 * @param action
	 * @return
	 */
	public boolean isInAllowUrl(String url) {
		for (String s : allowUrl) {
			if (url.contains(s)) {
				return true;
			}
		}
		return false;
	}
   
   /**
	 * 在初始化方法中,向set当中添加不需要拦截的url
	 *
	 *
	 */
	@Override
	public void init(FilterConfig arg0) throws ServletException {
		allowUrl.add("404.html");
		allowUrl.add("login.html");
		allowUrl.add("reg.html");
		allowUrl.add("index.html");
		allowUrl.add("maintain.html");
		allowUrl.add("hall.html");
		allowUrl.add("wap.html");
		allowUrl.add("help.html");
		allowUrl.add("activity.html");
		allowUrl.add("announce.html");
		//忘记密码
		allowUrl.add("forgetPwd.html");
		allowUrl.add("chooseWay.html");
		allowUrl.add("verifyWay.html");
		allowUrl.add("resetPwd.html");	
		allowUrl.add("monitor.html");
		allowUrl.add("newUserGift.html");
		
		//支付通知
		allowUrl.add("returnurl.jsp");
		allowUrl.add("pageurl.jsp");
	}

}
