/*
 * 追号记录
 */
function FollowOrderPage(){}
var followOrderPage = new FollowOrderPage();

followOrderPage.param = {
	lotteryId : null
};


/**
 * 查询参数
 */
followOrderPage.queryParam = {
		lotteryType : null,
		createdDateStart : null,
		createdDateEnd : null,
	    lotteryId : null,
	    userName:null,
		lotteryId:null,
		lotteryKind:null,
		lotteryModel:null,
		expect:null,
		queryScope:1
};

//分页参数
followOrderPage.pageParam = {
        queryMethodParam : null,
        pageNo : 1,
        pageMaxNo : 1,  //最大的页数
        skipHtmlStr : ""
};


$(document).ready(function() {
	
	$("div.lists").find("div.child:eq(2)").addClass("on");
	$("#dateStar").val(new Date().setDayStartTime().format("yyyy-MM-dd hh:mm:ss"));
	$("#dateEnd").val(new Date().setDayEndTime().format("yyyy-MM-dd hh:mm:ss"));
	
	//添加单击事件
	addSelectEvent();

	$("#lotteryTypeSel li").unbind("click").click("unbind",function(){
		var parent_select=$(this).closest(".select-list");
		parent_select.find("li").removeClass("on");
		$(this).addClass("on");
		var parent=$(this).closest(".select-content");
		var val=$(this).html();
		var html=$(val).html();
		var valLen=$(val).html().length;
		if(!isNaN(parseInt(html)))valLen=valLen/2;
		valLen=valLen*16+32;
		parent.css("width",valLen+"px")
		var realVal = $(this).find("p").attr("data-value");
		parent.find(".select-save").val(realVal);
		parent.find(".select-title").html(val);
		
		//清除玩法第二个之后的下拉框选项
		$("#lotteryKind li").each(function(index){
			if(index != 0) {
				$(this).remove();
			}
		});
		//清除期号第二个之后的下拉框选项
		$("#expectUl li").each(function(index){
			if(index != 0) {
				$(this).remove();
			}
		});
		$("#expectUl").css("height", "20px");
		$("#expect").val("");
		
		//重新加载玩法
		followOrderPage.reloadSubKinds();
		//重新加载期号
		followOrderPage.reloadExpects();
	});
	
	
	//期数 div select初始化
	$("#expectUl").hide();
    $("#selectExpect").click(function(){
		var ul = $("#expectUl");
		if(ul.css("display")=="none"){
			ul.slideDown("fast");
		}else{
			ul.slideUp("fast");
		}
	});
    //单击期数元素之外隐藏
    $(document).bind("click",function(e){ 
    	var target = $(e.target); 
    	if(target.closest("#expectUl").length == 0){ 
    		if(target.closest("#divselect").length == 0) {
    			$("#expectUl").hide(); 
    		}
    	} 
	});
	
	//当前链接位置标注选中样式
	$("#user_order").addClass("selected");
	followOrderPage.findUserOrderAfterNumByQueryParam(); //查询所有的投注记录
	//followOrderPage.getAllUserOrderAfterNums(); //查询所有的投注记录
	
	$("#lotteryOrdera").click(function(){
		followOrderPage.getUserOrderPage();
	});
});


/**
 * 跳转到投注明细
 */
FollowOrderPage.prototype.getUserOrderPage = function(){
	window.location.href= contextPath + "/gameBet/order.html";
};

/**
 * 加载所有的投注记录
 */
FollowOrderPage.prototype.getAllUserOrderAfterNums = function(){
	followOrderPage.pageParam.queryMethodParam = 'getAllUserOrderAfterNums';
	followOrderPage.queryParam = {};
	followOrderPage.queryConditionUserOrders(followOrderPage.queryParam,followOrderPage.pageParam.pageNo);
};

/**
 * 按页面条件查询数据
 */
FollowOrderPage.prototype.findUserOrderAfterNumByQueryParam = function(){
	followOrderPage.pageParam.queryMethodParam = 'findUserOrderAfterNumByQueryParam';
	
	followOrderPage.queryParam.lotteryType = $("#lotteryType").val();
	followOrderPage.queryParam.lotteryId = $("#lotteryId").val();
	followOrderPage.queryParam.expect = $("#expect").val();
	followOrderPage.queryParam.userName = getSearchVal("userName");
	followOrderPage.queryParam.queryScope = getSearchVal("queryScope");
	followOrderPage.queryParam.lotteryKind = getSearchVal("lotteryKind");
	followOrderPage.queryParam.lotteryModel = getSearchVal("lotteryModel");
	
	var startimeStr = $("#dateStar").val();
	var endtimeStr = $("#dateEnd").val();
	if(startimeStr != ""){
		followOrderPage.queryParam.createdDateStart = startimeStr.stringToDate();
	}
	if(endtimeStr != ""){
		followOrderPage.queryParam.createdDateEnd = endtimeStr.stringToDate();
	}
	
	if(followOrderPage.queryParam.lotteryType!=null && $.trim(followOrderPage.queryParam.lotteryType)==""){
		followOrderPage.queryParam.lotteryType = null;
	}
	if(followOrderPage.queryParam.expect != null && $.trim(followOrderPage.queryParam.expect)==""){
		followOrderPage.queryParam.expect = null;
	}
	if(followOrderPage.queryParam.lotteryId != null && $.trim(followOrderPage.queryParam.lotteryId)==""){
		followOrderPage.queryParam.lotteryId = null;
	}
	if(followOrderPage.queryParam.createdDateStart!=null && $.trim($("#dateStar").val())==""){
		followOrderPage.queryParam.createdDateStart = null;
	}
	if(followOrderPage.queryParam.createdDateEnd!=null && $.trim($("#dateEnd").val())==""){
		followOrderPage.queryParam.createdDateEnd = null;
	}
	
	$("#userOrderAfterNumList").html("<div class='siftings-line'><div colspan='13'>正在加载...</div></div>");
	followOrderPage.queryConditionUserOrders(followOrderPage.queryParam,followOrderPage.pageParam.pageNo);
};


var afterNumberLotteryWindow;
/**
 * 刷新列表数据
 */
FollowOrderPage.prototype.refreshFollowOrderPages = function(page){
	var userOrderAfterNumListObj = $("#userOrderAfterNumList");
	
	userOrderAfterNumListObj.html("");
	var str = "";
    var userOrders = page.pageContent;
    
    if(userOrders.length == 0){
    	str += "<div class='siftings-line'>";
    	str += "  <div colspan='13'>没有符合条件的记录！</div>";
    	str += "</div>";
    	userOrderAfterNumListObj.append(str);
    }else{
    	//记录数据显示
    	for(var i = 0 ; i < userOrders.length; i++){
    		var userOrder = userOrders[i];
    		var lotteryIdStr = userOrder.lotteryId;
    		
    		str += "<div class='siftings-line'>";
    		str += "  <div class='child child1'>"+ lotteryIdStr.substring(lotteryIdStr.lastIndexOf('-')+1,lotteryIdStr.length) +"</div>";
    		str += "  <div class='child child2'>"+ userOrder.userName +"</div>";
    		str += "  <div class='child child3'>"+ userOrder.createdDateStr +"</div>";
    		str += "  <div class='child child4'>"+ userOrder.lotteryTypeDes +"</div>";
    		str += "  <div class='child child5'>"+ userOrder.expect +"</div>";
    		str += "  <div class='child child6'>"+ userOrder.afterNumberCount +"</div>";
    		str += "  <div class='child child7'>"+ userOrder.yetAfterNumber +"</div>";
    		str += "  <div class='child child8'>"+ userOrder.lotteryModelStr +"</div>";
    		str += "  <div class='child child9'>"+ userOrder.afterNumberPayMoney.toFixed(frontCommonPage.param.fixVaue) +"</div>";
    		str += "  <div class='child child10'>"+ userOrder.yetAfterCost.toFixed(frontCommonPage.param.fixVaue) +"</div>";
    		str += "  <div class='child child11'>"+ userOrder.winCostTotal.toFixed(frontCommonPage.param.fixVaue) +"</div>";
    		str += "  <div class='child child12'>"+ userOrder.afterStatusStr +"</div>";
			str += "  <div class='color-blue' ><a href='javascript:void(0);' name='lotteryMsgRecord' data-value='"+userOrder.lotteryId+"'>查看</a></div>";
    		str += "</div>";
    		userOrderAfterNumListObj.append(str);
    		str = "";
    	}
    }
	
	//查看投注详情
	$("a[name='lotteryMsgRecord']").unbind("click").click(function(event){
        var orderId = $(this).attr("data-value");
        var width = 800;
        var height = 400;
        var left = parseInt((screen.availWidth/2) - (width/2));//屏幕居中
        var top = parseInt((screen.availHeight/2) - (height/2));
        var windowFeatures = "width=" + width + ",height=" + height + ",status,resizable,scrollbars=yes,left=" + left + ",top=" + top;
    	var lotteryhrefStr = contextPath + "/gameBet/followOrderDetail.html?orderId="+orderId;
        afterNumberLotteryWindow = window.open(lotteryhrefStr, "subWind", windowFeatures);
        afterNumberLotteryWindow.focus();
	});
	
	//显示分页栏
    $("#followOrderPageMsg").pagination({
        items: page.totalRecNum,
        itemsOnPage: page.pageSize,
        currentPage: page.pageNo,
		onPageClick:function(pageNumber) {
			followOrderPage.pageQuery(pageNumber);
		}
    }); 
    
};


/**
 * 条件查询追号记录
 */
FollowOrderPage.prototype.queryConditionUserOrders = function(queryParam,pageNo){
	$.ajax({
        type: "POST",
        url: contextPath +"/order/followOrderQuery",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify({'query':queryParam,'pageNo':pageNo}), 
        dataType:"json",
        success: function(result){
            if (result.code == "ok") {
            	followOrderPage.refreshFollowOrderPages(result.data);
            }else if(result.code != null && result.code == "error"){
                alert(result.data);
            }else{
                showErrorDlg(jQuery(document.body), "查询追号记录数据失败.");
            }
        }
    });
};

/**
 * 根据条件查找对应页
 */
FollowOrderPage.prototype.pageQuery = function(pageNo){
	if(pageNo < 1){
		followOrderPage.pageParam.pageNo = 1;
	} else{
		followOrderPage.pageParam.pageNo = pageNo;
	}
	//如果页码小于等于0，则不进行分页查找
	if(followOrderPage.pageParam.pageNo <= 0){
		return;
	}
	
	if(followOrderPage.pageParam.queryMethodParam == 'getAllUserOrderAfterNums'){
		followOrderPage.getAllUserOrderAfterNums();
	}else if(followOrderPage.pageParam.queryMethodParam == 'findUserOrderAfterNumByQueryParam'){
		followOrderPage.findUserOrderAfterNumByQueryParam();
	}else{
		alert("类型对应的查找方法未找到.");
	}
};

/**
 * 查看方案详情
 */
FollowOrderPage.prototype.showInfo = function(id){
	window.location.href= contextPath + "/gameBet/user/param1="+ id +"/user_order_detail.html";
};

/**
 * 重新加载玩法
 */
FollowOrderPage.prototype.reloadSubKinds = function(){
	var lotteryType = $("#lotteryType").val();
	if(isNotEmpty(lotteryType)) {
		var param={};
		param.lotteryType=lotteryType;
		$.ajax({
			type : "POST",
			url : contextPath + "/moneydetail/getSubKindsByLotteryKind",
			contentType : 'application/json;charset=utf-8',
			data : JSON.stringify(param),
			success : function(result) {
				commonHandlerResult(result, this.url);
				if (result.code == "ok") {
					var subKinds =result.data;
					//先清空原来的数据
                	var lotteryKindsObj = $("#lotteryKinds");
                	
                	lotteryKindsObj.html("");
                	$("#lotteryKinds").append("<li><p data-value=''>选择玩法</p></li>")
					//重新添加下拉框选项
					for(var i = 0 ; i < subKinds.length; i++){
						$("#lotteryKinds").append("<li><p  data-value='"+subKinds[i].code+"'>"+subKinds[i].description+"</p></li>")
					}
				} else if (result.code == "error") {
					frontCommonPage.showKindlyReminder(result.data);
				}else{
					showErrorDlg(jQuery(document.body), "加载彩种下的玩法数据失败.");
				}
			}
		});
	}
};


/**
 * 重新加载期号
 */
FollowOrderPage.prototype.reloadExpects = function(){
	var lotteryType = $("#lotteryType").val();
	if(isNotEmpty(lotteryType)) {
		var param={};
		param.lotteryType=lotteryType;
		$.ajax({
			type : "POST",
			url : contextPath + "/moneydetail/getExpectsByLotteryKind",
			contentType : 'application/json;charset=utf-8',
			data : JSON.stringify(param),
			success : function(result) {
				commonHandlerResult(result, this.url);
				if (result.code == "ok") {
					var expects =result.data;
					var selHeight = 160;
					if(expects.length <= 8) {
						selHeight = 20 * (expects.length + 1);
					}
					$("#expectUl").css("height", selHeight +"px");
					//重新添加下拉框选项
					for(var i = 0 ; i < expects.length; i++){
						$("#expectUl").append("<li><p href='javascript:void(0);' data-value='"+expects[i].lotteryNum+"'>"+expects[i].lotteryNum+"</p></li>");
					}
					if(expects.length == 1000) {
						$("#expectUl").append("<li><p href='javascript:void(0);' id='loadMoreLi' data-value='"+expects[expects.length - 1].id+"'>加载更多</p></li>");
						$("#loadMoreLi").unbind("click").click(function(event){
							followOrderPage.loadMoreExpects(event);
						});
					}
					//添加自定义下拉框选中事件
					var myList = $(".myList");
					addSelectEvent(true,myList);
				} else if (result.code == "error") {
					frontCommonPage.showKindlyReminder(result.data);
				}else{
					showErrorDlg(jQuery(document.body), "加载彩种下的期号数据失败.");
				}
			}
		});
	}
};

/**
 * 重新加载更多期号
 */
FollowOrderPage.prototype.loadMoreExpects= function(event) {
	//移除加载的的选项
	var lotteryType = $("#lotteryType").val();
	var codeId = $("#expectUl li").last().find("p").attr("selectid");
	var param={};
	param.lotteryType=lotteryType;
	param.id=codeId;
	$("#expectUl li").last().remove();
	$.ajax({
		type : "POST",
		url : contextPath + "/moneydetail/getExpectsByLotteryKind",
		contentType : 'application/json;charset=utf-8',
		data : JSON.stringify(param),
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var expects =result.data;
				//重新添加下拉框选项
				for(var i = 0 ; i < expects.length; i++){
					$("#expectUl").append("<li><p href='javascript:void(0);' selectid='"+expects[i].lotteryNum+"'>"+expects[i].lotteryNum+"</p></li>");
				}
				if(expects.length == 1000) {
					$("#expectUl").append("<li><p href='javascript:void(0);'  id='loadMoreLi' selectid='"+expects[expects.length - 1].id+"'>加载更多</p></li>");
					$("#loadMoreLi").unbind("click").click(function(event){
						followOrderPage.loadMoreExpects(event);
					});
				}
				//添加自定义下拉框选中事件
				var myList = $(".myList");
				addSelectEvent(true,myList);
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}else{
				showErrorDlg(jQuery(document.body), "加载彩种下的期号数据失败.");
			}
		}
	});
	event.stopPropagation(); 
}