function ForgetpwdPage() {
	
}

var forgetpwdPage = new ForgetpwdPage();

forgetpwdPage.param = {
		
};

/**
 * 初始化页面
 */
$(document).ready(function() {
    $(".quc-service").unbind("mouseover").mouseover(function() {
        $(this).addClass("quc-service-open")
    });
    $(".quc-service").unbind("mouseout").mouseout(function() {
        $(this).removeClass("quc-service-open")
    });
    $("#username").unbind("focus").focus(function() {
        $("#username_field").removeClass("red");
        $("#name_line").removeClass("error");
        $("#username_tip").hide()
    });
    $("#username").unbind("blur").blur(function() {
        var username = $("#username").val();
        if (username == null || $.trim(username) == "") {
            $("#username_field").addClass("red");
            $("#name_line").addClass("error");
            $("#username_tip").show();
            $("#username_tip").text("请输入用户名")
        } 
    });
	
    $("#verifyCode").unbind("focus").focus(function() {
    	$("#verifycode_line").removeClass("error");
        $("#verifyCode_field").removeClass("red");
        $("#verifyCode_tip").hide()
    });
    $("#verifyCode").unbind("blur").blur(function() {
        var verifyCode = $("#verifyCode").val();
        if (verifyCode == null || $.trim(verifyCode) == "") {
            $("#verifyCode_field").addClass("red");
            $("#verifycode_line").addClass("error");
            checkCodeRefresh();
            $("#verifyCode_tip").show()
        }
    });
    $("#nextButton").click(function() {
    	forgetpwdPage.nextPage()
    });
    $("body").keydown(function(event) {
        var curKey = event.which;
        if (curKey == "13") {
        	forgetpwdPage.nextPage()
        }
    })
});

/**
 * 找回密码信息输入判断
 */
ForgetpwdPage.prototype.nextPage = function() {
    $("#nextButton").unbind("click");
    $("#username_field").removeClass("red");
    $("#verifyCode_field").removeClass("red");
    var userName = $("#username").val();
    var verifyCode = $("#verifyCode").val();
    if (userName == null || userName == "") {
        $("#username_field").addClass("red");
        $("#username_tip").text("请输入用户名");
        $("#username_tip").show();
        forgetpwdPage.resetNextButton();
        return
    }
    if (verifyCode == null || verifyCode == "") {
        $("#verifyCode_field").addClass("red");
        $("#verifyCode_tip").show();
        forgetpwdPage.resetNextButton();
        return
    }
    
    var jsonData = {
    		"userName" : userName,
    		"checkCode" : verifyCode,
    };
    $.ajax({
		type : "POST",
		url : contextPath + "/forgetpwd/forgetPwdNext",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if( result.code == "ok"){
				 window.location.href = contextPath + "/gameBet/forgetPwd/verifyWay.html"
			}else if(result.code == "error"){
				 frontCommonPage.showKindlyReminder(result.data);
				 forgetpwdPage.resetNextButton();
			}else {
				 frontCommonPage.showKindlyReminder("数据请求失败，请重试");
				 forgetpwdPage.resetNextButton();
            }
		}
	});
};

/**
 * 进入下一个页面
 */
ForgetpwdPage.prototype.resetNextButton = function() {
    $("#nextButton").click(function() {
    	forgetpwdPage.nextPage()
    })
};