function AnnouncePage() {
}
var announcePage = new AnnouncePage();

announcePage.param = {

};

/**
 * 查询参数
 */
announcePage.queryParam = {

};

// 分页参数
announcePage.pageParam = {
	queryMethodParam : null,
	pageNo : 1,
	pageMaxNo : 1, // 最大的页数
	skipHtmlStr : ""
};

$(document).ready(function() {
	// 当前链接位置标注选中样式
	$(".index").removeClass("active");
	announcePage.getAllUserAnnounces(); // 查询网站公告数据
});

/**
 * 加载网站公告数据
 */
AnnouncePage.prototype.getAllUserAnnounces = function() {
	announcePage.pageParam.queryMethodParam = 'getAllUserAnnounces';
	announcePage.queryParam = {};
	announcePage.queryConditionUserAnnounces(announcePage.pageParam.pageNo);
};

/**
 * 刷新列表数据
 */
AnnouncePage.prototype.refreshUserAnnouncePages = function(page) {
	var userAnnounceListObj = $("#userAnnounceList");

	userAnnounceListObj.html("");
	var str = "";
	var userAnnounces = page.pageContent;

	if (userAnnounces.length == 0) {
		str += "<li>";
		str += "  <span class='time'></span>";
		str += "暂无相关信息";
		str += "</li>";
		userAnnounceListObj.append(str);
	} else {
		// 记录数据显示 <a href="#"><li><p
		// class="title">手机下载通知手机下载通知手机下载通知手机下载通知</p><p
		// class="time">2016/02/06</p></li></a>
		for (var i = 0; i < userAnnounces.length; i++) {
			var userAnnounce = userAnnounces[i];
			str += "<a href='javascript:void(0)'; onclick='announcePage.showAnnounceDetail(" + userAnnounce.id + ")'><li>";
			str += "<p class='title' >" + userAnnounce.title + "</p>";
			str += "<p class='time'>" + userAnnounce.publishTimeStr + "</p>";
			str += "</li></a>";
			userAnnounceListObj.append(str);
			str = "";
		}
	}

	// 记录统计
	var pageMsg = $("#pageMsg");
	var pageStr = "";
	pageStr += "<p class='msg'>当前第" + page.pageNo + "页/共" + page.totalPageNum + "页&nbsp;&nbsp;共" + page.totalRecNum
			+ "条记录</p><div class='navs'><a href='javascript:void(0)'  onclick='announcePage.pageQuery(1)'>首页</a>";
	pageStr += "<a href='javascript:void(0)' onclick='announcePage.pageParam.pageNo--;announcePage.pageQuery(announcePage.pageParam.pageNo)'>上一页</a>";
	pageStr += "<a href='javascript:void(0)' onclick='announcePage.pageParam.pageNo++;announcePage.pageQuery(announcePage.pageParam.pageNo)'>下一页</a>";
	pageStr += "<a href='javascript:void(0)' onclick='announcePage.pageQuery(" + page.totalPageNum + ")'>尾页</a>";

	var skipHtmlStr = "";
	if ($("#pageSkip").html() != null) {
		$("#pageSkip").remove();
	}
	skipHtmlStr += "<span id='pageSkip'>";
	skipHtmlStr += "&nbsp;&nbsp;跳转至&nbsp;&nbsp;";
	skipHtmlStr += "<select id='currentPageChoose' class='select' onchange='announcePage.pageParam.pageNo = this.value;announcePage.pageQuery(announcePage.pageParam.pageNo)'  style='width:auto;'>";
	for (var i = 1; i <= page.totalPageNum; i++) {
		skipHtmlStr += "<option value='" + i + "'>" + i + "</option>";
	}
	skipHtmlStr += "</select>";
	skipHtmlStr += "</div></span>";
	pageStr += skipHtmlStr;
	pageStr += "</th>";
	pageMsg.html(pageStr);

	$("#currentPageChoose").find("option[value='" + page.pageNo + "']").attr("selected", true); // 显示当前页码
	announcePage.pageParam.pageMaxNo = page.totalPageNum; // 标识最大的页数
};

/**
 * 公告记录查询
 */
AnnouncePage.prototype.queryConditionUserAnnounces = function(pageNo) {
	//封装参数
	var jsonData = {
		"pageNo" : pageNo,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/announces/announceQuery",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				announcePage.refreshUserAnnouncePages(result.data);
			} else if (result.code == "error") {
				setTimeout("window.location.href= contextPath + '/gameBet/login.html'", 0);
			}
		}
	});
};

/**
 * 查看公告内容
 */
AnnouncePage.prototype.showAnnounceDetail = function(id) {
	window.location.href = contextPath + "/gameBet/announceDetail.html?parm1=" + id;
};

/**
 * 根据条件查找对应页
 */
AnnouncePage.prototype.pageQuery = function(pageNo) {
	if (pageNo < 1) {
		announcePage.pageParam.pageNo = 1;
	} else if (pageNo > announcePage.pageParam.pageMaxNo) {
		announcePage.pageParam.pageNo = announcePage.pageParam.pageMaxNo;
	} else {
		announcePage.pageParam.pageNo = pageNo;
	}
	// 如果页码小于等于0，则不进行分页查找
	if (announcePage.pageParam.pageNo <= 0) {
		return;
	}

	if (announcePage.pageParam.queryMethodParam == 'getAllUserAnnounces') {
		announcePage.getAllUserAnnounces();
	} else {
		alert("类型对应的查找方法未找到.");
	}
};
