function Helppage() {}
var helppage = new Helppage();

//参数
helppage.param = {
};

/**
 * 查询参数
 */
helppage.queryParam = {
	type : "CJWT"
};

//分页参数
helppage.pageParam = {
    queryMethodParam : null,
    pageNo : 1,
    pageMaxNo : 1,  //最大的页数
    skipHtmlStr : ""
};

function main_child_click(e){
	var val=$(e).html();
	var parent=$(e).closest(".main-child");
	if(val=="查看"){
		$(e).html("收起");
		parent.find(".child-content").stop(false,true).slideDown(500);
	}else{
		$(e).html("查看");
		parent.find(".child-content").stop(false,true).slideUp(500);
	}
}
function main_child_hide(e){
	var val=$(e).html();
	var parent=$(e).closest(".main-child");
	$(".ck").html("查看");
	parent.find(".child-content").stop(false,true).slideUp(500);
}

$(document).ready(function() {
	//控制菜单栏底色
	$(".index").removeClass("active");
	$("#helpMenu").addClass("active");
	
	// 顶部导航链接选中样式
	$('.header .foot .nav .child4').addClass('on');
	
	var functionParam = helppage.param.functionParam;
	if(functionParam == "CJWT"){
		helppage.clickChang($("#CJWT"));
		helppage.getAllCJWTHelps();
	}else if(functionParam == "XSRM"){
		helppage.clickChang($("#XSRM"));
		helppage.getAllXSRMHelps();
	}else if(functionParam == "CZWT"){
		helppage.clickChang($("#CZWT"));
		helppage.getAllCZWTHelps();
	}else if(functionParam == "GCWT"){
		helppage.clickChang($("#GCWT"));
		helppage.getAllGCWTHelps();
	}else if(functionParam == "TXWT"){
		helppage.clickChang($("#TXWT"));
		helppage.getAllTXWTHelps();
	}else if(functionParam == "ZHAQ"){
		helppage.clickChang($("#ZHAQ"));
		helppage.getAllZHAQHelps();
	}else if(functionParam == "CZZS"){
		helppage.clickChang($("#CZZS"));
		helppage.getAllCZZSHelps();
	}else{
		helppage.getAllCJWTHelps();
	}
	
	//常见问题信息展示
	$("#CJWT").click(function() {
		//统一处理
		helppage.clickChang($(this));
		helppage.getAllCJWTHelps();
	});
	//新手入门信息展示
	$("#XSRM").click(function() {
		//统一处理
		helppage.clickChang($(this));
		helppage.getAllXSRMHelps();
	});
	//充值问题信息展示
	$("#CZWT").click(function() {
		//统一处理
		helppage.clickChang($(this));
		helppage.getAllCZWTHelps();
	});
	//购彩问题信息展示
	$("#GCWT").click(function() {
		//统一处理
		helppage.clickChang($(this));
		helppage.getAllGCWTHelps();
	});
	//提现问题信息展示
	$("#TXWT").click(function() {
		//统一处理
		helppage.clickChang($(this));
		helppage.getAllTXWTHelps();
	});
	//账户安全信息展示
	$("#ZHAQ").click(function() {
		//统一处理
		helppage.clickChang($(this));
		helppage.getAllZHAQHelps();
	});
	//彩种知识信息展示
	$("#CZZS").click(function() {
		//统一处理
		helppage.clickChang($(this));
		helppage.getAllCZZSHelps();
	});
});

/**
 * 加载常见问题信息
 */
Helppage.prototype.getAllCJWTHelps = function(){
	helppage.pageParam.queryMethodParam = 'getAllCJWTHelps';
	helppage.queryParam.type='CJWT';
	helppage.queryConditionHelps(helppage.queryParam.type,helppage.pageParam.pageNo);
};
/**
 * 加载新手入门信息
 */
Helppage.prototype.getAllXSRMHelps = function(){
	helppage.pageParam.queryMethodParam = 'getAllXSRMHelps';
	helppage.queryParam.type='XSRM';
	helppage.queryConditionHelps(helppage.queryParam.type,helppage.pageParam.pageNo);
};
/**
 * 加载充值问题信息
 */
Helppage.prototype.getAllCZWTHelps = function(){
	helppage.pageParam.queryMethodParam = 'getAllCZWTHelps';
	helppage.queryParam.type='CZWT';
	helppage.queryConditionHelps(helppage.queryParam.type,helppage.pageParam.pageNo);
};
/**
 * 加载购彩问题信息
 */
Helppage.prototype.getAllGCWTHelps = function(){
	helppage.pageParam.queryMethodParam = 'getAllGCWTHelps';
	helppage.queryParam.type='GCWT';
	helppage.queryConditionHelps(helppage.queryParam.type,helppage.pageParam.pageNo);
};
/**
 * 加载提现问题信息
 */
Helppage.prototype.getAllTXWTHelps = function(){
	helppage.pageParam.queryMethodParam = 'getAllTXWTHelps';
	helppage.queryParam.type='TXWT';
	helppage.queryConditionHelps(helppage.queryParam.type,helppage.pageParam.pageNo);
};
/**
 * 加载账户安全信息
 */
Helppage.prototype.getAllZHAQHelps = function(){
	helppage.pageParam.queryMethodParam = 'getAllZHAQHelps';
	helppage.queryParam.type='ZHAQ';
	helppage.queryConditionHelps(helppage.queryParam.type,helppage.pageParam.pageNo);
};
/**
 * 加载彩种知识信息
 */
Helppage.prototype.getAllCZZSHelps = function(){
	helppage.pageParam.queryMethodParam = 'getAllCZZSHelps';
	helppage.queryParam.type='CZZS';
	helppage.queryConditionHelps(helppage.queryParam.type,helppage.pageParam.pageNo);
};

/**
 * 条件查询记录
 */
Helppage.prototype.queryConditionHelps = function(type,pageNo){
	$.ajax({
        type: "POST",
        url: contextPath +"/help/helpQuery",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify({'type':type,'pageNo':pageNo}), 
        dataType:"json",
        success: function(result){
            if (result.code != null && result.code == "ok") {
            	helppage.refreshHelpPages(result.data);
            }else if(result.code != null && result.code == "error"){
                alert(result.data);
            }else{
                showErrorDlg(jQuery(document.body), "查询记录数据失败.");
            }
        }
    });
};

/**
 * 刷新列表数据
 */
Helppage.prototype.refreshHelpPages = function(page){
	var helpListObj = $("#helpList");
	
	helpListObj.html("");
	var str = "";
    var helps = page.pageContent;
    
    if(helps.length == 0){
    	str += "<li>";
    	str += "<p class='help-list-name'></p>";
    	str += "<p class='help-list-text'>暂无相关信息</p>";
    	str += "</li>";
    	helpListObj.append(str);
    }else{
    	//记录数据显示
    	for(var i = 0 ; i < helps.length; i++){
    		var help = helps[i];
    		str += "<li class='main-child'>";
			str += "<div class='child-head'><div class='center'>";
			if(help.type == "XSRM"){
				str += "<div class='child-icon'><img src='"+contextPath+"/front/images/help/i2.png' /></div>";
			}else if(help.type == "CZWT"){
				str += "<div class='child-icon'><img src='"+contextPath+"/front/images/help/i3.png' /></div>";
			}else{
				str += "<div class='child-icon'><img src='"+contextPath+"/front/images/help/i1.png' /></div>";
			}
			str +=" <div class='child-title'>";
        	str += "<p class='title'><span class='help-list-num'>"+(i+1)+". </span>"+ help.title +"</p>";
        	str +=" <div class='info'>";
			str += "<p>"+ help.essentialsDes +"</p>";
        	str +=" </div>";
			str +=" </div>";
			str +=" <div class='btn ck' onClick='main_child_click(this)'>查看</div>";
			str +=" </div></div>";
			str +=" <div class='child-content'><div class='center'><p style='font-size:16px;color:#000000;'>"+help.content+"</p><p></br></p><p></br></p>";
			str +="  <div class='btn' onClick='main_child_hide(this)'><div class='title'>关闭收起</div><div class='bg'></div></div>";
			str +=" </div></div>";
			str += "</li>";
    		helpListObj.append(str);
    		str = "";
    	}
    
    }
    
	//记录统计
	var pageMsg = $("#pageMsg");
	var pageStr = "";
	pageStr += "<span class='page-text'>当前第" + page.pageNo + "页/共" + page.totalPageNum +"页&nbsp;&nbsp;共" + page.totalRecNum +"条记录</span>&nbsp;&nbsp;<div class='page-list'><a href='javascript:void(0)'  onclick='helppage.pageQuery(1)'>首页</a>&nbsp;";
	pageStr += "<a href='javascript:void(0)' onclick='helppage.pageParam.pageNo--;helppage.pageQuery(helppage.pageParam.pageNo)'>上一页</a>&nbsp;";
	pageStr += "<a href='javascript:void(0)' onclick='helppage.pageParam.pageNo++;helppage.pageQuery(helppage.pageParam.pageNo)'>下一页</a>&nbsp;";
	pageStr += "<a href='javascript:void(0)' onclick='helppage.pageQuery("+ page.totalPageNum +")'>尾页</a>";
	
	var skipHtmlStr = "";
	if($("#pageSkip").html() != null){
		$("#pageSkip").remove();
	}
    skipHtmlStr += "<span id='pageSkip'>";
    skipHtmlStr += "&nbsp;&nbsp;跳转至&nbsp;&nbsp;";
	skipHtmlStr += "<select id='currentPageChoose' class='select' onchange='helppage.pageParam.pageNo = this.value;helppage.pageQuery(helppage.pageParam.pageNo)'  style='width:auto;'>";
	for(var i = 1; i <= page.totalPageNum; i++){
		skipHtmlStr += "<option value='"+ i +"'>"+ i +"</option>";
	}
	skipHtmlStr += "</select>";
	skipHtmlStr += "</div></span>";	
	pageStr += skipHtmlStr;
	pageMsg.html(pageStr);
	
	$("#currentPageChoose").find("option[value='"+ page.pageNo +"']").attr("selected",true);  //显示当前页码
	helppage.pageParam.pageMaxNo = page.totalPageNum;  //标识最大的页数
};

/**
 * 根据条件查找对应页
 */
Helppage.prototype.pageQuery = function(pageNo){
	if(pageNo < 1){
		helppage.pageParam.pageNo = 1;
	}else if(pageNo > helppage.pageParam.pageMaxNo){
		helppage.pageParam.pageNo = helppage.pageParam.pageMaxNo;
	}else{
		helppage.pageParam.pageNo = pageNo;
	}
	//如果页码小于等于0，则不进行分页查找
	if(helppage.pageParam.pageNo <= 0){
		return;
	}
	
	if(helppage.pageParam.queryMethodParam == 'getAllCJWTHelps'){
		helppage.getAllCJWTHelps();
	}else if(helppage.pageParam.queryMethodParam == 'getAllXSRMHelps'){
		helppage.getAllXSRMHelps();
	}else if(helppage.pageParam.queryMethodParam == 'getAllCZWTHelps'){
		helppage.getAllCZWTHelps();
	}else if(helppage.pageParam.queryMethodParam == 'getAllGCWTHelps'){
		helppage.getAllGCWTHelps();
	}else if(helppage.pageParam.queryMethodParam == 'getAllTXWTHelps'){
		helppage.getAllTXWTHelps();
	}else if(helppage.pageParam.queryMethodParam == 'getAllZHAQHelps'){
		helppage.getAllZHAQHelps();
	}else if(helppage.pageParam.queryMethodParam == 'getAllCZZSHelps'){
		helppage.getAllCZZSHelps();
	}else{
		alert("类型对应的查找方法未找到.");
	}
};

/**
 * 查看公告内容
 */
Helppage.prototype.showHelpDetail = function(id){
	window.location.href= contextPath + "/gameBet/help/help_detail.html?parm1=" + id;
};

/**
 * 选中样式处理
 */
Helppage.prototype.clickChang = function(obj){
	$("#CJWT").removeClass("on");
	$("#XSRM").removeClass("on");
	$("#CZWT").removeClass("on");
	$("#GCWT").removeClass("on");
	$("#TXWT").removeClass("on");
	$("#ZHAQ").removeClass("on");
	$("#CZZS").removeClass("on");
	obj.addClass("on");
	
	helppage.pageParam.pageNo = 1;
};