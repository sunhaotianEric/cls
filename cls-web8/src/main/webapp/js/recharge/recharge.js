function RechargePage() {
}
var rechargePage = new RechargePage();

rechargePage.param = {
	payBankType : null,
	payQuickBankType : null,
	payBankLowestValue : null,
	payBankHighestValue : null,
	payValue : null,
	bankId : null,
	bankUrl : null,
	canclePayId : null,
	payNickName : null,
	payId : null,
	rechargeConfigId : null,
	money : null
};

rechargePage.limitParam = {
	quickRechargeLowestMoney : null,
	quickRechargeHighestMoney : null,
	shanfuQuickRechargeLowestMoney : null,
	shanfuQuickRechargeHighestMoney : null
};

var isBankListContentOver = false;
$(document).ready(
		function() {
			// 充值方式列表，左右滑动按钮
			var width = 148 * $('.model-nav .child').length;
			if (width > 1044) {
				$('.model-nav').width(width);
				$('.slide .screen').css('bottom', '-1px');
				$('.slide .left-btn, .slide .right-btn').show();
			}
			$('.left-btn').on('click', function() {
				var scrollLeft = $('.slide-container').scrollLeft();
				$('.slide-container').scrollLeft(scrollLeft - 296);
			})
			$('.right-btn').on('click', function() {
				var scrollLeft = $('.slide-container').scrollLeft();
				$('.slide-container').scrollLeft(scrollLeft + 296);
			})

			// 选择银行的事件
			$("#gotoRechargeButton").unbind("click").click(function() {
				if (rechargePage.param.bankUrl != null) {
					window.open(rechargePage.param.bankUrl);
				}
			});
			// 继续充值按钮
			$("#cancelOrderSuccessDialogRechargeButton").unbind("click").click(function() {
				window.location.href = contextPath + "/gameBet/recharge.html";
			});
			// 充值记录
			$("#cancelOrderSuccessDialogRechargeRecordButton").unbind("click").click(function() {
				window.location.href = contextPath + "/gameBet/rechargeOrder.html";
			});
			$("#rechargeOrderDialogRechargeButton").unbind("click").click(function() {
				window.location.href = contextPath + "/gameBet/rechargeOrder.html";
			});

			// 前往支付历史记录
			$("#toQuickPayDialogHistoryButton").unbind("click").click(function() {
				window.location.href = contextPath + '/gameBet/rechargeOrder.html';
			});
			$("#toQuickPayDialogContinueButton").unbind("click").click(function() {
				window.location.href = contextPath + "/gameBet/recharge.html";
			});

			// 撤单
			$("#cancelRechargeButton").unbind("click").click(function() {
				if (rechargePage.param.canclePayId != null) {
					rechargePage.cancelRechargeOrder();
				} else {
					$("#cancelRechargeButton").removeAttr("disabled");
				}
			});

			// 图层关闭事件申明
			$("#rechargeOrderDialogClose").unbind("click").click(function(event) {
				$("#gotoRechargeButton").hide();
				$("#cancelRechargeButton").hide();

				$(".alert-msg").stop(false, true).delay(200).fadeOut(500);
				$(".alert-msg-bg").stop(false, true).fadeOut(500);
			});
			$("#rechargeOrderDialog2Close").unbind("click").click(function(event) {
				$(".alert-msg").stop(false, true).delay(200).fadeOut(500);
				$(".alert-msg-bg").stop(false, true).fadeOut(500);
				$(".alert-msg-bg").removeClass('in');
			});

			// 点击金额赋值事件
			$(".line-money .line-money-child .child").click(function() {
				var parent = $(this).closest(".line-money");
				var val = $(this).attr("data-val");
				parent.find(".inputText").val(val);
			});

			// clipboard.js
			var clipboard = new Clipboard('.copy', {
				text : function(e) {
					return $(e).prev().find('.copytext').text();
				}
			});
			clipboard.on('success', function(e) {
				$(e.trigger).css("color", "#f8a525");
				$(e.trigger).text("已复制");
				setTimeout(function() {
					$(e.trigger).css('color', '#ff4444');
					$(e.trigger).text('复制')
				}, 3000);
			});
			clipboard.on('error', function(e) {
				alert("请重新复制！");
			});

			// 充值方式选择事件
			$(".main .model-nav .child").click(function() {
				rechargePage.clearRadioCheck();
				if ($(this).hasClass("on"))
					return;
				$(".main .model-nav .child").removeClass("on");
				$(this).addClass("on");
				var index = $(".main .model-nav .child").index($(this));
				$(".main .content").not($(".main .content:eq(" + index + ")")).stop(false, true).hide();
				$(".main .content:eq(" + index + ")").stop(false, true).show();
				let payType = $(".main .content").eq(index);
				let type = payType.attr("data-ref-type");
				let payId = payType.attr("data-id");
				if (type == "TRANSFER") {
					rechargePage.getRechargeInfo(payId);
				}
			});
			
			//rechargePage.clearRadioCheck();
			if ($(this).hasClass("on"))
				return;
			$(".main .model-nav .child").removeClass("on");
			$(this).addClass("on");
			var index = $(".main .model-nav .child").index($(this));
			$(".main .content").not($(".main .content:eq(" + index + ")")).stop(false, true).hide();
			$(".main .content:eq(" + index + ")").stop(false, true).show();
			let payType = $(".main .content").eq(index);
			let type = payType.attr("data-ref-type");
			let payId = payType.attr("data-id");
			if (type == "TRANSFER") {
				rechargePage.getRechargeInfo(payId);
			}
			
			// 初始化页面时候当页面只有一个的时候而且为转账的时候处理付值的问题.
			
			
			// 页面图层进行操作
			$("div.content").each(function(i) {
				// 支付类型
				var type = $(this).attr("data-pay-type");
				// 充值配置ID
				var id = $(this).attr("data-id");
				// 关联方式类型
				var refType = $(this).attr("data-ref-type");
				var refIdList = $(this).attr("data-refIdList");
				var query = {};
				query.payType = type;
				query.id = id;
				query.refType = refType;
				query.refIdList = refIdList;
				var str = "div";
				var div = $(this).attr('class');

				var t = div.split(" ");

				for (var k = 0; k < t.length; k++) {
					str = str + "." + t[k];
				}

				if (type != undefined) {
					// 加载系统可支付的银行
					setTimeout(rechargePage.getCurrentUserCanRechargerPanks(query, str), 1000);
				}

			})
			// 页面表单进行循环操作
			$("form").each(
					function(i) {
						if ($(this).attr("data-pay-type") == "WEIXIN" || $(this).attr("data-pay-type") == "ALIPAY" || $(this).attr("data-pay-type") == "QQPAY"
								|| $(this).attr("data-pay-type") == "UNIONPAY" || $(this).attr("data-pay-type") == "JDPAY") {
							var query = {};
							query.payType = $(this).attr("data-pay-type");
							query.refType = $(this).attr("data-ref-type");
							query.id = $(this).attr("data-id");
							var context = "#" + $(this).attr("id");
							rechargePage.getFormToCanRechargerPanks(query, context);

						}
					});

			// 弹出框遮挡特殊处理
			if (window.innerHeight <= 650) {
				$("#rechargeOrderDialog1").css({
					"margin-top" : 0,
					"top" : "112px"
				});
			}

		});


/**
 * 根据ID获取银行卡信息!!
 */
RechargePage.prototype.getRechargeInfo = function(payId) {
	$(".bankId").val('');
	$(".bankUserName").val('');
	$(".barcodeUrl").hide();
	$(".barcodeUrl").children('img').attr('src', '');
	var jsonData = {
		"id" : payId,
	}
	$.ajax({
		type : "POST",
		url : contextPath + "/recharge/getRechargeInfo",
		contentType : "application/json;charset=UTF-8",
		data : JSON.stringify(jsonData),
		dataType : "json",
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var resultData2 = result.data2[0];
				// 获取转账银行卡信息.
				$(".bankId").val(resultData2.bankId);
				// 获取账户信息,并且设置值.
				$(".bankUserName").val(resultData2.bankUserName);
				// 获取二维码.
				var barcodeUrl = resultData2.barcodeUrl;
				if (barcodeUrl != null) {
					$(".barcodeUrl").show()
					$(".barcodeUrl").children('img').attr('src', barcodeUrl);
				}

			} else if (result.code == "error") {
				var err = result.data;
				frontCommonPage.showKindlyReminder(err);
			} else {
				showErrorDlg(jQuery(document.body), "加载系统可支付银行的请求失败.");
			}
		}
	});
};

/**
 * 给快捷表单的input文本框赋值银行的码表值的回调方法
 */
RechargePage.prototype.getFormToCanRechargerPanks = function(query, content) {
	var formcontent = content;
	$.ajax({
		type : "POST",
		url : contextPath + "/recharge/getCurrentUserCanRechargerPanks",
		contentType : "application/json;charset=UTF-8",
		data : JSON.stringify(query),
		dataType : "json",
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var banks = result.data;
				if (banks != null && banks.length > 0) {
					for (var i = 0; i < banks.length; i++) {
						var rechargeBank = banks[i];
						$(content).find("input[name='PayId']").val(rechargeBank.payId);
					}
				}
			} else if (result.code == "error") {
				var err = result.data;
				frontCommonPage.showKindlyReminder(err);
			} else {
				showErrorDlg(jQuery(document.body), "加载系统可支付银行的请求失败.");
			}
		}
	});
};

// 复制.
var clipboard = new Clipboard('.copy', {
	text : function(e) {
		return $(e).prev().val()
	}
});
clipboard.on('success', function(e) {
	$(e.trigger).css("color", "#f8a525");
	$(e.trigger).text("已复制");
	setTimeout(function() {
		$(e.trigger).css('color', '#ff4444');
		$(e.trigger).text('复制')
	}, 6000);
});
clipboard.on('error', function(e) {
	alert("请重新复制！");
});

/**
 * 查找用户可支付的银行
 */
RechargePage.prototype.getCurrentUserCanRechargerPanks = function(query, content) {
	// 充值类型
	var type = query.payType;
	// div元素字段将来用这个字符串用JQ操作相关元素
	var divcontent = content;
	// 关联方式类型
	var refType = query.refType;
	$.ajax({
		type : "POST",
		url : contextPath + "/recharge/getCurrentUserCanRechargerPanks",
		contentType : "application/json;charset=UTF-8",
		data : JSON.stringify(query),
		dataType : "json",
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var data = result.data;
				rechargePage.showRechargeTypes(data, type, divcontent, refType);
			} else if (result.code == "error") {
				var err = result.data;
				frontCommonPage.showKindlyReminder(err);
			} else {
				showErrorDlg("加载系统可支付银行的请求失败.");
			}
		}
	});
};

/**
 * 充值记录撤单
 */
RechargePage.prototype.cancelRechargeOrder = function() {
	var param = {};
	param.rechargeOrderId = rechargePage.param.canclePayId;
	$.ajax({
		type : "POST",
		url : contextPath + "/recharge/cancelRechargeOrder",
		contentType : "application/json;charset=UTF-8",
		data : JSON.stringify(param),
		dataType : "json",
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				frontCommonPage.showKindlyReminder("充值订单取消成功.");
				$("#gotoRechargeButton").hide();
				$("#cancelRechargeButton").hide();
				$("#rechargeOrderDialog1").stop(false, true).delay(200).fadeOut(500);
				$(".alert-msg-bg").stop(false, true).fadeOut(500);
			} else if (result.code == "error") {
				var err = result.data;
				frontCommonPage.showKindlyReminder(err);
			} else {
				showErrorDlg("撤销订单的请求失败.");
			}
		}
	});
};

/**
 * 充值校验
 */
RechargePage.prototype.rechargeStepValidate = function(obj) {
	if (currentUser.isTourist == 1) {
		alert("游客无操作权限，请先注册为本系统用户");
		return;
	}
	// 元素名称
	var payWayName = $(obj).attr("name");
	// 元素上一级图层
	var content = $(obj).parents(".content");
	// 关联方式类型
	var refType = $(obj).attr("data-ref-type");
	// 支付类型
	var chargeType = $(obj).attr("data-charge-type");
	// 关联充值id号
	var rechargeConfigId = $(obj).attr("data-id");

	// 名称为wangyin
	if (payWayName == "wangyin") {
		// 寻找金额输入框
		var wangyininputValue = content.find("input[name='rechargeininput']").val();

		var inputObj = content.find("div[name='wangyinRadioCheck'][class='radio check']").find("input");
		var transferNickName = content.find("input[name='transferNickName']").val();
		rechargePage.param.bankId = parseInt(inputObj.attr("data-id"));
		rechargePage.param.payValue = parseFloat(wangyininputValue);
		if (rechargePage.param.bankId == null) {
			alert("您还没有选择对应的银行");
			return;
		}
		if (wangyininputValue == "") {
			alert("对不起,您还没填写您的充值金额.");
			return;
		}

		rechargePage.param.payNickName = transferNickName;
		if (refType == "TRANSFER") {
			if (transferNickName == null || transferNickName == "") {
				alert("对不起,您还没填写转账姓名");
				return;
			}
		}

		var maxPay = parseFloat(inputObj.attr("max"));
		var minPay = parseFloat(inputObj.attr("min"));
		if (rechargePage.param.payValue < minPay || rechargePage.param.payValue > maxPay) {
			alert("充值金额不正确,该银行支付金额最小" + minPay + ",支付最大金额" + maxPay);
			return;
		}

		rechargePage.param.rechargeConfigId = rechargeConfigId;

		rechargePage.rechargeStep();
		// 名称为kuaijie
	} else if (payWayName == "kuaijie") {
		// 寻找金额输入框
		var kuaijieinputValue = content.find("input[name='rechargeininput']").val();
		// 寻找快捷radio
		var inputObj = $("div[name='kuaijieRadioCheck'][class='radio check']").find("input");
		// 荣灿支付可以不选择银行
		if (chargeType != "RONGCAN" && chargeType != "SHANDEPAY" && chargeType != "FENGLIPAY" && chargeType != "LEKU") {
			if (rechargePage.param.payId == null || rechargePage.param.payId == "") {
				alert("请选择银行！");
				return;
			}
		}
		content.find("form input[name='PayId']").val(rechargePage.param.payId);
		if (kuaijieinputValue == "") {
			alert("对不起,您还没填写您的充值金额.");
			return;
		}
		var maxPay = parseFloat(inputObj.attr("max"));
		var minPay = parseFloat(inputObj.attr("min"));
		if (chargeType == "RONGCAN") {
			maxPay = parseFloat(content.find("input[name='rechargeininput']").attr("max"));
			minPay = parseFloat(content.find("input[name='rechargeininput']").attr("min"));
		}
		if (parseFloat(kuaijieinputValue) < minPay || parseFloat(kuaijieinputValue) > maxPay) {
			alert("充值金额不正确,快捷充值支付金额最小" + minPay + ",支付最大金额" + maxPay);
			return;
		}
		// 第三方充值金额动态调整
		rechargePage.rechargeMoneyRandom(obj, chargeType, content);

		// 名称为zhifubao
	} else if (payWayName == "zhifubao") {
		// 寻找金额输入框
		var zhifubaoinputObj = content.find("input[name='rechargeininput']");
		// var zhifubaoinputValue = zhifubaoinputObj.val();
		var zhifubaoinputValue = zhifubaoinputObj.val();
		rechargePage.param.bankId = parseInt(zhifubaoinputObj.attr("data-value"));
		rechargePage.param.payValue = parseFloat(zhifubaoinputValue);
		var alipayNickName = content.find("input[name='alipayNickName']").val();
		if (rechargePage.param.bankId == null) {
			alert("您还没有选择对应的银行");
			return;
		}
		if (zhifubaoinputValue == "") {
			alert("对不起,您还没填写您的充值金额.");
			return;
		}

		var maxPay = parseFloat(zhifubaoinputObj.attr("max"));
		var minPay = parseFloat(zhifubaoinputObj.attr("min"));
		rechargePage.param.payNickName = alipayNickName;
		if (refType == "TRANSFER") {
			if (alipayNickName == null || alipayNickName == "") {
				alert("对不起,您还没填写支付宝姓名");
				return;
			}
		}
		if (rechargePage.param.payValue < minPay || rechargePage.param.payValue > maxPay) {
			alert("充值金额不正确,支付宝支付金额最小" + minPay + ",支付最大金额" + maxPay);
			return;
		}
		// 普金支付充值金额特殊校验
		if (chargeType == "PUJIN") {
			// 小于等于100必须为整数
			if (rechargePage.param.payValue <= 100) {
				var r = /^\+?[1-9][0-9]*$/;
				if (!r.test(rechargePage.param.payValue)) {
					alert("充值金额不正确,必须为整数值");
					return;
				}
			} else {
				var r = /^\+?[1-9][0-9]*[0]$/;
				if (!r.test(rechargePage.param.payValue)) {
					alert("充值金额不正确,充值金额大于100的最后一位必须是0，如110、120");
					return;
				}
			}

		}
		if (refType == "TRANSFER") {
			rechargePage.param.rechargeConfigId = rechargeConfigId;
			rechargePage.rechargeStep();
		} else {
			// 第三方充值金额动态调整
			rechargePage.rechargeMoneyRandom(obj, chargeType, content);
		}
		// 名称为weixin
	} else if (payWayName == "weixin") {
		// 寻找金额输入框
		var weixininputObj = content.find("input[name='rechargeininput']");
		var weixininputValue = weixininputObj.val();
		var weixinNickName = content.find("input[name='weixinNickName']").val();
		rechargePage.param.bankId = parseInt(weixininputObj.attr("data-value"));
		rechargePage.param.payValue = parseFloat(weixininputValue);
		if (rechargePage.param.bankId == null) {
			alert("您还没有选择对应的银行");
			return;
		}
		if (weixininputValue == "") {
			alert("对不起,您还没填写您的充值金额.");
			return;
		}
		var maxPay = parseFloat(weixininputObj.attr("max"));
		var minPay = parseFloat(weixininputObj.attr("min"));
		if (rechargePage.param.payValue < minPay || rechargePage.param.payValue > maxPay) {
			alert("充值金额不正确,微信支付金额最小" + minPay + ",支付最大金额" + maxPay);
			return;
		}

		// 普金支付充值金额特殊校验
		if (chargeType == "PUJIN") {
			// 小于等于100必须为整数
			if (rechargePage.param.payValue <= 100) {
				var r = /^\+?[1-9][0-9]*$/;
				if (!r.test(rechargePage.param.payValue)) {
					alert("充值金额不正确,必须为整数值");
					return;
				}
			} else {
				var r = /^\+?[1-9][0-9]*[0]$/;
				if (!r.test(rechargePage.param.payValue)) {
					alert("充值金额不正确,充值金额大于100的最后一位必须是0，如110、120");
					return;
				}
			}
		}
		rechargePage.param.payNickName = weixinNickName;
		if (refType == "TRANSFER") {
			if (weixinNickName == null || weixinNickName == "") {
				alert("对不起,您还没填写支付微信昵称");
				return;
			}
			rechargePage.param.rechargeConfigId = rechargeConfigId;
			rechargePage.rechargeStep();
		} else {
			// 第三方充值金额动态调整
			rechargePage.rechargeMoneyRandom(obj, chargeType, content);
		}

	} else if (payWayName == "qqpay") {
		// 寻找金额输入框
		var qqpayinputObj = content.find("input[name='rechargeininput']");
		var qqpayinputValue = qqpayinputObj.val();
		var qqpayNickName = content.find("input[name='qqpayNickName']").val();
		rechargePage.param.bankId = parseInt(qqpayinputObj.attr("data-value"));
		rechargePage.param.payValue = parseFloat(qqpayinputValue);
		if (rechargePage.param.bankId == null) {
			alert("您还没有选择对应的银行");
			return;
		}
		if (qqpayinputValue == "") {
			alert("对不起,您还没填写您的充值金额.");
			return;
		}

		var maxPay = parseFloat(qqpayinputObj.attr("max"));
		var minPay = parseFloat(qqpayinputObj.attr("min"));
		if (rechargePage.param.payValue < minPay || rechargePage.param.payValue > maxPay) {
			alert("充值金额不正确,QQ钱包支付金额最小" + minPay + ",支付最大金额" + maxPay);
			return;
		}

		rechargePage.param.payNickName = qqpayNickName;
		if (refType == "TRANSFER") {
			if (qqpayNickName == null || qqpayNickName == "") {
				alert("对不起,您还没填写支付QQ钱包昵称");
				return;
			}
		}
		if (refType == "TRANSFER") {
			rechargePage.param.rechargeConfigId = rechargeConfigId;
			rechargePage.rechargeStep();
		} else {
			// 第三方充值金额动态调整
			rechargePage.rechargeMoneyRandom(obj, chargeType, content);
		}

	} else if (payWayName == "unionpay") {
		// 寻找金额输入框
		var unionpayinputObj = content.find("input[name='rechargeininput']");
		var unionpayinputValue = unionpayinputObj.val();
		var unionpayNickName = content.find("input[name='unionpayNickName']").val();
		rechargePage.param.bankId = parseInt(unionpayinputObj.attr("data-value"));
		rechargePage.param.payValue = parseFloat(unionpayinputValue);
		if (rechargePage.param.bankId == null) {
			alert("您还没有选择对应的银行");
			return;
		}
		if (qqpayinputValue == "") {
			alert("对不起,您还没填写您的充值金额.");
			return;
		}

		var maxPay = parseFloat(unionpayinputObj.attr("max"));
		var minPay = parseFloat(unionpayinputObj.attr("min"));
		if (rechargePage.param.payValue < minPay || rechargePage.param.payValue > maxPay) {
			alert("充值金额不正确,支付金额最小" + minPay + ",支付最大金额" + maxPay);
			return;
		}

		rechargePage.param.payNickName = qqpayNickName;
		if (refType == "TRANSFER") {
			if (unionpayNickName == null || unionpayNickName == "") {
				alert("对不起,您还没填写支付昵称");
				return;
			}
		}
		if (refType == "TRANSFER") {
			rechargePage.param.rechargeConfigId = rechargeConfigId;
			rechargePage.rechargeStep();
		} else {
			// 第三方充值金额动态调整
			rechargePage.rechargeMoneyRandom(obj, chargeType, content);
		}
		// 京东支付
	} else if (payWayName == "jdpay") {
		// 寻找金额输入框
		var jdpayinputObj = content.find("input[name='rechargeininput']");
		var jdpayinputValue = jdpayinputObj.val();
		var jdpayNickName = content.find("input[name='jdpayNickName']").val();
		rechargePage.param.bankId = parseInt(jdpayinputObj.attr("data-value"));
		rechargePage.param.payValue = parseFloat(jdpayinputValue);
		if (rechargePage.param.bankId == null) {
			alert("您还没有选择对应的银行");
			return;
		}
		if (qqpayinputValue == "") {
			alert("对不起,您还没填写您的充值金额.");
			return;
		}

		var maxPay = parseFloat(jdpayinputObj.attr("max"));
		var minPay = parseFloat(jdpayinputObj.attr("min"));
		if (rechargePage.param.payValue < minPay || rechargePage.param.payValue > maxPay) {
			alert("充值金额不正确,支付金额最小" + minPay + ",支付最大金额" + maxPay);
			return;
		}

		rechargePage.param.payNickName = qqpayNickName;
		if (refType == "TRANSFER") {
			if (jdpayNickName == null || jdpayNickName == "") {
				alert("对不起,您还没填写支付昵称");
				return;
			}
		}
		if (refType == "TRANSFER") {
			rechargePage.param.rechargeConfigId = rechargeConfigId;
			rechargePage.rechargeStep();
		} else {
			// 第三方充值金额动态调整
			rechargePage.rechargeMoneyRandom(obj, chargeType, content);
		}
		// 微信条形码
	} else if (payWayName == "weixinbarcode") {
		// 寻找金额输入框
		var weixininputObj = content.find("input[name='rechargeininput']");
		var weixininputValue = weixininputObj.val();
		rechargePage.param.bankId = parseInt(weixininputObj.attr("data-value"));
		rechargePage.param.payValue = parseFloat(weixininputValue);
		if (rechargePage.param.bankId == null) {
			alert("您还没有选择对应的银行");
			return;
		}
		if (weixininputValue == "") {
			alert("对不起,您还没填写您的充值金额.");
			return;
		}

		var maxPay = parseFloat(weixininputObj.attr("max"));
		var minPay = parseFloat(weixininputObj.attr("min"));
		if (rechargePage.param.payValue < minPay || rechargePage.param.payValue > maxPay) {
			alert("充值金额不正确,微信支付金额最小" + minPay + ",支付最大金额" + maxPay);
			return;
		}
		// 第三方充值金额动态调整
		rechargePage.rechargeMoneyRandom(obj, chargeType, content);
	} else {
		alert("未找到此种支付方式.");
	}
}

/**
 * 充值金额加上随机数
 * 
 */
RechargePage.prototype.rechargeMoneyRandom = function(obj, chargeType, content) {
	var rechargeininput = $(obj).parents(".content").find("input[name='rechargeininput']");
	var refType = rechargeininput.attr("data-ref-type");
	var payType = rechargeininput.attr("data-pay-type");
	var inputMoney = rechargeininput.val();
	if (inputMoney == "") {
		alert("对不起,您还没填写充值金额.");
		return;
	}
	rechargePage.param.money = inputMoney;
	// 小数不用进行随机数处理,直接跳转
	// 普金支付特殊处理,金额要求整数，直接跳转 荣汕支付支付宝要求整数
	if (parseInt(inputMoney) != inputMoney || chargeType == "PUJIN" || chargeType == "RONGCAN" || chargeType == "BDPAY" || chargeType == "MSPAY" || chargeType == "QUEPAY"
			|| chargeType == "DUDUPAY" || chargeType == "SIXEIGHTSIXUPAY" || chargeType == "LIANYINGPAY" || chargeType == "FEILONGPAY" || chargeType == "TIANTIANKUAIPAY"
			|| chargeType == "FANKEPAY" || chargeType == "HEXIEPAY" || chargeType == "XIUFUPAY" || chargeType == "CRYPTPAY" || chargeType == "WEIFUTONG" || chargeType == "WANNIAN"
			|| chargeType == "YIFUTONG" || chargeType == "JINDOUYUNPAY" || chargeType == "MUMINGPAY" || chargeType == "FENGLIPAY" || chargeType == "GTTPAY" || chargeType == "SHENGSHIPAY" || chargeType == "RONGTENGPAY" || chargeType == "HUJINGPAY" || chargeType == "ZHAOQIANPAY" || chargeType == "FEIZHUPAY" || chargeType == "CHANGJIANGPAY" || chargeType == "DBPAY" || chargeType == "HEMA" || chargeType == "JINYANG" || chargeType == "HIPAY3721" || chargeType == "XINDUOBAOPAY" || chargeType == "ANJIPAY" || chargeType == "XINFUTONG" || chargeType == "XINDACATPAY"
			|| chargeType == "HAITUPAY" || chargeType == "XUNYIN" || chargeType == "LONGTENGPAY" || chargeType == "DONGFANGLONG" || chargeType == "XINFAPAY"  || chargeType == "YOUFUPAY") {
		content.find("form input[name='OrderMoney']").val(rechargePage.param.money);
		$(".alert-msg2").stop(false, true).delay(200).fadeIn(500);
		$(".alert-msg-bg").stop(false, true).fadeIn(500);
		// 根据支付类型跳转相应的表单
		var rechargeConfigId = content.find("form input[name='rechargeConfigId']").val();
		var param = {};
		param.rechargeMoney = rechargePage.param.money;
		param.rechargeConfigId = rechargeConfigId;
		$.ajax({
			type : "POST",
			url : contextPath + "/recharge/thirdPayInsertRechargeOrder",
			contentType : "application/json;charset=UTF-8",
			data : JSON.stringify(param),
			dataType : "json",
			success : function(result) {
				commonHandlerResult(result, this.url);
				if (result.code == "ok") {
					var data = result.data;
					var data2 = result.data2;
					content.find("form input[name='serialNumber']").val(data);
					content.find("form").attr("action", data2);
					content.find("form").submit();
				} else if (result.code == "error") {
					var err = result.data;
					frontCommonPage.showKindlyReminder(err);
				}
			}
		});
		return;
	}

	var mumberRandom = (Math.ceil(Math.random() * 5)) / 100;
	var money = parseFloat(inputMoney) + mumberRandom;
	confirm("为了更准确核对你的金额系统已经将充值金额调整为" + money, function() {
		// 点击确定跳转第三方支付处理
		rechargePage.param.money = money;
		rechargeininput.val(money);
		// 跳转页面
		content.find("form input[name='OrderMoney']").val(rechargePage.param.money);
		$(".alert-msg2").stop(false, true).delay(200).fadeIn(500);
		$(".alert-msg-bg").stop(false, true).fadeIn(500);
		// 根据支付类型跳转相应的表单
		var rechargeConfigId = content.find("form input[name='rechargeConfigId']").val();
		var param = {};
		param.rechargeMoney = rechargePage.param.money;
		param.rechargeConfigId = rechargeConfigId;
		$.ajax({
			type : "POST",
			url : contextPath + "/recharge/thirdPayInsertRechargeOrder",
			contentType : "application/json;charset=UTF-8",
			data : JSON.stringify(param),
			dataType : "json",
			success : function(result) {
				commonHandlerResult(result, this.url);
				if (result.code == "ok") {
					var data = result.data;
					var data2 = result.data2;
					content.find("form input[name='serialNumber']").val(data);
					/* rechargePage.skipPage(chargeType,content); */
					content.find("form").attr("action", data2);
					content.find("form").submit();
				} else if (result.code == "error") {
					var err = result.data;
					frontCommonPage.showKindlyReminder(err);
				}
			}
		});

	});
};

/**
 * 充值支付下一步
 */
RechargePage.prototype.rechargeStep = function() {
	var param = {};
	param.rechargeId = rechargePage.param.bankId;
	param.rechargeConfigId = rechargePage.param.rechargeConfigId;
	param.rechargeMoney = rechargePage.param.payValue;
	if (rechargePage.param.payNickName == null || rechargePage.param.payNickName == "") {
		param.nickName = "";
	} else {
		param.nickName = rechargePage.param.payNickName;
	}

	$.ajax({
		type : "POST",
		url : contextPath + "/recharge/doRecharge",
		contentType : "application/json;charset=UTF-8",
		data : JSON.stringify(param),
		dataType : "json",
		success : function(result) {
			commonHandlerResult(result, this.url);
			$("#" + rechargePage.param.content).find("div.submitBoolean").attr("data-submit", "false");
			if (result.code == "ok") {
				var data = result.data;
				frontCommonPage.showKindlyReminder("订单提交成功,请前往转账!!!请仔细核对转账账号.");
			} else if (result.code == "error") {
				var err = result.data;
				frontCommonPage.showKindlyReminder(err);
			} else {
				alert("充值下一步动作请求失败.");
				$("#wangyin").removeAttr("disabled");
				$("#wangyin").text("下一步");
			}
		}
	});
};

/**
 * 展示可充值的银行卡
 */
RechargePage.prototype.showRechargeTypes = function(banks, payType, divcontent, refType) {
	// 充值类型是网银
	// alert(divcontent);
	if (payType == "BANK_TRANSFER") {
		if (banks != null && banks.length > 0) {
			// 用JQ寻找相应的radio元素
			var rechargeBanksObj = $(divcontent).find("div.line-content.radio-content");
			rechargeBanksObj.html("");
			var str = "";
			for (var i = 0; i < banks.length; i++) {
				var rechargeBank = banks[i];
				if (i == 0) {
					// 默认第一个radio是选中的;其他不选中!并且将最低金额,最高金额并赋值给显示最低金额与最高金额的标签!
					str += "<div class='radio check' name='wangyinRadioCheck' onclick='rechargePage.showBankHighestAndLowest(this,\"" + divcontent + "\")'>";
				} else {
					str += "<div class='radio' name='wangyinRadioCheck' onclick='rechargePage.showBankHighestAndLowest(this,\"" + divcontent + "\")'>";
				}
				// 加载银行类型,银行码值,银行名称,最低金额,最高金额并赋值给radio;并显示相应的银行图片
				str += "   <input type='hidden' class='radio' data-id='" + rechargeBank.id + "' bankname='" + rechargeBank.bankName + "' data-value='" + rechargeBank.bankType
						+ "' max='" + rechargeBank.highestValue + "' min='" + rechargeBank.lowestValue + "'/>";
				str += "   <div class='radio-pointer'><div class='pointer'></div></div>";
				str += "   <div class='radio-bank'><img src='" + rechargePage.getBankImgLogo(rechargeBank.bankType) + "' /></div>";
				str += "</div>";
				rechargeBanksObj.append(str);
				str = "";
			}
		}
		$($("div[name='wangyinRadioCheck']")[0]).trigger("click");
		// //充值类型是快捷
	} else if (payType == "QUICK_PAY") {
		if (banks != null && banks.length > 0) {
			// 用JQ寻找相应的radio元素
			var rechargeBanksObj = $(divcontent).find("div.line-content.radio-content");
			rechargeBanksObj.html("");
			var str = "";
			for (var i = 0; i < banks.length; i++) {
				var rechargeBank = banks[i];
				if (i == 0) {
					// 默认第一个radio是选中的;其他不选中!并且将最低金额,最高金额并赋值给显示最低金额与最高金额的标签!
					str += "<div class='radio check' name='kuaijieRadioCheck' onclick='rechargePage.showBankHighestAndLowest(this,\"" + divcontent + "\")'>";
				} else {
					str += "<div class='radio' name='kuaijieRadioCheck' onclick='rechargePage.showBankHighestAndLowest(this,\"" + divcontent + "\")'>";
				}
				// 加载银行类型,银行码值,银行名称,最低金额,最高金额并赋值给radio;并显示相应的银行图片
				str += "   <input type='hidden' class='radio' data-type='" + rechargeBank.payType + "' data-charge-type='" + rechargeBank.chargeType + "' bankname='"
						+ rechargeBank.bankName + "' data-payId='" + rechargeBank.payId + "' max='" + rechargeBank.highestValue + "' min='" + rechargeBank.lowestValue + "'/>";
				str += "   <div class='radio-pointer'><div class='pointer'></div></div>";
				str += "   <div class='radio-bank'><img src='" + rechargePage.getBankImgLogo(rechargeBank.bankType) + "' /></div>";
				str += "</div>";
				rechargeBanksObj.append(str);
				str = "";
			}
		}
		$($(divcontent).find("div[name='kuaijieRadioCheck']")[0]).trigger("click");
	} else if (payType == "ALIPAY") {
		if (refType == "TRANSFER") {

		} else if (refType == "THIRDPAY") {

		}

	} else if (payType == "WEIXIN") {
		if (refType == "TRANSFER") {

		} else if (refType == "THIRDPAY") {

		}
	} else if (payType == "QQPAY") {
		if (refType == "TRANSFER") {

		} else if (refType == "THIRDPAY") {

		}
	}

	// 银行单选框
	$(".radio").click(function() {
		var parent = $(this).closest(".radio-content");
		parent.find(".radio").removeClass("check");
		$(this).addClass("check");
		var val = $(this).html();
		parent.find(".radio-save").html(val);
	});
};

/**
 * 获取银行logo图片地址
 */
RechargePage.prototype.getBankImgLogo = function(bankType) {
	var imgLogo = "";
	if (bankType == "ICBC") {
		imgLogo = contextPath + "/front/images/user/bank1.png";
	} else if (bankType == "CCB") {
		imgLogo = contextPath + "/front/images/user/bank2.png";
	} else if (bankType == "ABC") {
		imgLogo = contextPath + "/front/images/user/bank4.png";
	} else if (bankType == "PSBC") {
		imgLogo = contextPath + "/front/images/user/bank12.png";
	} else if (bankType == "CMBC") {
		imgLogo = contextPath + "/front/images/user/bank6.png";
	} else if (bankType == "CIB") {
		imgLogo = contextPath + "/front/images/user/bank8.png";
	} else if (bankType == "COMM") {
		imgLogo = contextPath + "/front/images/user/bank7.png";
	} else if (bankType == "CEB") {
		imgLogo = contextPath + "/front/images/user/bank13.png";
	} else if (bankType == "BOC") {
		imgLogo = contextPath + "/front/images/user/bank3.png";
	} else if (bankType == "SPABANK") {
		imgLogo = contextPath + "/front/images/user/bank14.png";
	} else if (bankType == "GDB") {
		imgLogo = contextPath + "/front/images/user/bank15.png";
	} else if (bankType == "CITIC") {
		imgLogo = contextPath + "/front/images/user/bank16.png";
	} else if (bankType == "HXBANK") {
		imgLogo = contextPath + "/front/images/user/bank17.png";
	} else if (bankType == "CMB") {
		imgLogo = contextPath + "/front/images/user/bank5.png";
	} else if (bankType == "QQPAY") {
		imgLogo = contextPath + "/front/images/user/bank18.png";
	} else if (bankType == "UNIONPAY") {
		imgLogo = contextPath + "/front/images/user/bank19.png";
	} else if (bankType == "JDPAY") {
		imgLogo = contextPath + "/front/images/user/bank20.png";
	} else if (bankType == "SPDB") {
		imgLogo = contextPath + "/front/images/user/bank21.png";
	}
	return imgLogo;
}

/**
 * 银行种类选择
 */
RechargePage.prototype.showBankHighestAndLowest = function(obj, divcontent) {
	var inputObj = $(obj).find("input");
	var min = inputObj.attr("min");
	var max = inputObj.attr("max");
	// alert(divcontent);
	$(divcontent).find("span[name='bankLowestValue']").text(min);
	$(divcontent).find("span[name='bankHighestValue']").text(max);

	// 网银充值最大值和最小值
	rechargePage.param.bankId = inputObj.attr("data-id");
	rechargePage.param.payId = inputObj.attr("data-payId");
	rechargePage.param.payBankLowestValue = parseFloat(inputObj.attr("min"));
	rechargePage.param.payBankHighestValue = parseFloat(inputObj.attr("max"));
};

/**
 * 获取支付记录信息
 */
RechargePage.prototype.getPayBankMsg = function() {
	$.ajax({
		type : "POST",
		url : contextPath + "/recharge/getPayBankMsg",
		contentType : "application/json;charset=UTF-8",
		dataType : "json",
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var data = result.data;
				rechargePage.showRechargeOrderMsg(data);
			} else if (result.code == "error") {
				var err = result.data;
				if (err == "CHARGEORDERSEND") {
					frontCommonPage.showKindlyReminder("您当前该充值记录状态已经改变,请重新进行充值.");
					return;
				} else {
					frontCommonPage.showKindlyReminder(err);
				}
			} else {
				showErrorDlg("加载系统充值记录的请求失败.");
			}
		}
	});
};

/**
 * 展示可充值的银行卡
 */
RechargePage.prototype.showRechargeOrderMsg = function(order) {
	if (order != null) {
		rechargePage.param.canclePayId = order.id;
		rechargePage.param.bankUrl = order.bankUrl;

		// 支付宝收款银行 用支行字段显示
		if (order.payType == "ALIPAY") {
			$("#rechargeInfoDiv").hide();
			$("#rechargeInfoWeixinDiv").hide();
			if (order.bankType != "ALIPAY") {
				$("#rechargeInfoAlipayDivTip").show();
				$("#rechargeInfoAlipayDivAccount").hide();
				$("#alertBankAmountTip").text(order.applyValue + "元");
				$("#alipayBankUserName").text(order.bankAccountName);
				$("#alipayAccountfuyuanId").text(order.postscript);
				$("#alipayBankName").text(order.bankName);
				// 隐藏开户行地址
				/* $("#bankBranchNameLi").hide(); */
				$("#gotoRechargeButton").val("去支付宝转账");
				$("#alipayBankId").text(order.bankCardNum);
			} else {
				$("#rechargeInfoAlipayDivTip").hide();
				$("#rechargeInfoAlipayDivAccount").show();
				$("#alertBankAmountAccount").text(order.applyValue + "元");
				// 隐藏开户行地址
				$("#gotoRechargeButton").val("去支付宝转账");
				$("#alipayfuyuanId").text("支付宝姓名：");
				if (isNotEmpty(order.barcodeUrl)) {
					$("#imgAlipay").show();
					$("#barcodeImgAlipay").attr("src", order.barcodeUrl);
				} else {
					$("#imgAlipay").hide();
				}
				$("#alipayAccountBankName").text(order.bankAccountName);
				$("#alipayAccountfuyuanIdAccount").text(order.postscript);
				$("#alipayAccountBankId").text(order.bankCardNum);
				$("#alipayAccountfuyuanIdAccount").text(order.postscript);
			}
		} else if (order.payType == "WEIXIN") {
			$("#rechargeInfoDiv").hide();
			$("#rechargeInfoAlipayDivTip").hide();
			$("#rechargeInfoAlipayDivAccount").hide();
			$("#rechargeInfoWeixinDiv").show();
			$("#alertBankAmountWeixin").text(order.applyValue + "元");
			$("#alertBankscriptWeixin").text(order.postscript);
			$("#alertBankUserNameWeixin").text(order.bankAccountName);
			$("#alertBankIdWeixin").text(order.bankCardNum);
			$("#gotoRechargeButton").val("去微信转账");
		} else if (order.payType == "BANK_TRANSFER") {
			$("#rechargeInfoAlipayDivTip").hide();
			$("#rechargeInfoAlipayDivAccount").hide();
			$("#rechargeInfoWeixinDiv").hide();
			$("#rechargeInfoDiv").show();
			$("#alertBankLogo").addClass(order.banklogo);
			$("#alertBankName").text(order.bankName);
			$("#alertBankscript").text(order.postscript);
			$("#alertBankUserName").text(order.bankAccountName);
			$("#alertBankAmount").text(order.applyValue + "元");
			$("#alertBankId").text(order.bankCardNum);
			$("#alertBankBranchName").text(order.subbranchName);
			$(".alipayTip").hide();
			$(".bankpayTip").show();
			$("#gotoRechargeButton").val("去银行充值");

		} else if (order.payType == "QQPAY") {
			$(".alipayTip").hide();
			$(".bankpayTip").hide();
			$(".alipayAccount").hide();
			$(".qqpayAccount").hide();
			$(".weixinAccount").show();

			$("#qqpayalertBankscript").val(order.postscript);
			$("#qqpayalertBankUserName").val(order.bankAccountName);
			$("#qqpayalertBankId").val(order.bankCardNum);
			$("#qqpayfuyuanId").html("QQ钱包呢称：");

			$("#gotoRechargeButton").val("去QQ钱包转账");
		}

		// 如果有二维码图片
		if (isNotEmpty(order.barcodeUrl)) {
			$("#barcodeImg").attr("src", order.barcodeUrl);
			$("#rechargeOrderDialog1").css("width", "730px");
			$("#barcodeDiv").css("display", "inline-block");
		} else {
			$("#rechargeOrderDialog1").css("width", "500px");
			$("#barcodeDiv").css("display", "none");
		}

		if (order.bankType == "WEIXIN") {
			$("#scriptTipInfo").hide();
		} else {
			$("#scriptTipInfo").show();
		}

		$("#rechargeOrderDialog1").stop(false, true).delay(200).fadeIn(500);
		$(".alert-msg-bg").stop(false, true).fadeIn(500);
	}
};
/**
 * 清空redio默认选择
 */
RechargePage.prototype.clearRadioCheck = function() {
	$(".radio.check").removeClass("check");
	rechargePage.param.payId = null;
}