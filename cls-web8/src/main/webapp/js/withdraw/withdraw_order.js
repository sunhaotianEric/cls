function WithDrawOrderPage(){}
var withDrawOrderPage = new WithDrawOrderPage();

/**
 * 查询参数
 */
withDrawOrderPage.queryParam = {
		statuss : null,
		createdDateStart : null,
		createdDateEnd : null,
		queryScope:1
};

//分页参数
withDrawOrderPage.pageParam = {
        pageNo : 1
};


$(document).ready(function() {
	
	$("div.lists").find("div.child:eq(1)").addClass("on");
	$("#dateStar").val(new Date().setDayStartTime().format("yyyy-MM-dd hh:mm:ss"));
	$("#dateEnd").val(new Date().setDayEndTime().format("yyyy-MM-dd hh:mm:ss"));
	
	//当前链接位置标注选中样式
	$("#user_withdraw_order").addClass("selected");
	withDrawOrderPage.findUserWithDrawOrderByQueryParam(); //查询所有的提现记录
	
	$("#accountDetaila").click(function(){
		withDrawOrderPage.getUserMoneyDetails();
	});
	
	$("#rechargeDetaila").click(function(){
		withDrawOrderPage.getRechargeOrderDetails();
	});
});


/**
 * 加载账户明细
 */
WithDrawOrderPage.prototype.getUserMoneyDetails = function(){
	window.location.href= contextPath + "/gameBet/user/user_money_detail.html";
};

/**
 * 加载充值明细
 */
WithDrawOrderPage.prototype.getRechargeOrderDetails = function(){
	window.location.href= contextPath + "/gameBet/rechargeOrder.html";
};

/**
 * 按页面条件查询数据
 */
WithDrawOrderPage.prototype.findUserWithDrawOrderByQueryParam = function(){
	withDrawOrderPage.queryParam.statuss = $("#status").val();
	withDrawOrderPage.queryParam.queryScope = $("#rangen").val();
//	添加自定义下拉框选中事件
	var myList =$(".myList");
	addSelectEvent(false, myList);
	var startimeStr = $("#dateStar").val();
	var endtimeStr = $("#dateEnd").val();
	if(startimeStr != ""){
		withDrawOrderPage.queryParam.createdDateStart = startimeStr.stringToDate();
	}
	if(endtimeStr != ""){
		withDrawOrderPage.queryParam.createdDateEnd = endtimeStr.stringToDate();
	}
	
	if(withDrawOrderPage.queryParam.statuss!=null && $.trim(withDrawOrderPage.queryParam.statuss)==""){
		withDrawOrderPage.queryParam.statuss = null;
	}
	if(withDrawOrderPage.queryParam.createdDateStart!=null && $.trim($("#dateStar").val())==""){
		withDrawOrderPage.queryParam.createdDateStart = null;
	}
	if(withDrawOrderPage.queryParam.createdDateEnd!=null && $.trim($("#dateEnd").val())==""){
		withDrawOrderPage.queryParam.createdDateEnd = null;
	}
	withDrawOrderPage.queryConditionUserWithDrawOrders(withDrawOrderPage.queryParam,withDrawOrderPage.pageParam.pageNo);
};

/**
 * 刷新列表数据
 */
WithDrawOrderPage.prototype.refreshUserWithdrawOrderPages = function(page){
	var userWithDrawOrderListObj = $("#userWithDrawOrderList");
	
	userWithDrawOrderListObj.html("");
	var str = "";
    var userWithDrawOrders = page.pageContent;
    
    if(parseInt(withDrawOrderPage.queryParam.queryScope) != 1) {
    	$("#userNameCol").show();
    	$("#userNameCol").parent().addClass("siftings-titles2");
    	$("#userWithDrawOrderList").addClass("siftings-content2");
    } else {
    	$("#userNameCol").hide();
    	$("#userNameCol").parent().removeClass("siftings-titles2");
    	$("#userWithDrawOrderList").removeClass("siftings-content2");
    }
    
    if(userWithDrawOrders.length == 0){
    	if(parseInt(withDrawOrderPage.queryParam.queryScope) != 1) {
    		str += "  <p colspan='6'>没有符合条件的记录！</p>";
    	} else {
    		str += "  <p colspan='5'>没有符合条件的记录！</p>";
    	}
    	userWithDrawOrderListObj.append(str);
    }else{
    	//记录数据显示
    	for(var i = 0 ; i < userWithDrawOrders.length; i++){
    		var userWithDrawOrder = userWithDrawOrders[i];
    		var serialNumberStr = userWithDrawOrder.serialNumber;
    		
    		str +="<div class='siftings-line'>";
    		if(parseInt(withDrawOrderPage.queryParam.queryScope) != 1) {
    			str += " <div class='child'>"+ userWithDrawOrder.userName +"</div>";
    		}
    		str += " <div class='child'>"+ serialNumberStr.substring(serialNumberStr.lastIndexOf('_')+1,serialNumberStr.length) +"</div>";
    		str += " <div class='child'>"+ (userWithDrawOrder.applyValue + userWithDrawOrder.feeValue).toFixed(frontCommonPage.param.fixVaue) +"</div>";
    		str += " <div class='child'>"+ userWithDrawOrder.accountValue.toFixed(frontCommonPage.param.fixVaue) +"</div>";
    		str += " <div class='child'>"+ userWithDrawOrder.createdDateStr +"</div>";
    		if(userWithDrawOrder.statusStr == "提现成功"){
    			str += "  <div class='child' style='color:blue'>"+ userWithDrawOrder.statusStr +"</div>";
    		}else if(userWithDrawOrder.statusStr == "提现失败"){
    			str += "  <div class='status_prompt'>";
    			str += "      <div class='exampleButton child child11' style='color:red'>"+ userWithDrawOrder.statusStr +"</div>";
    			str += "      <div class='exampleTip' style='right: 30px;display: none;'>原因分析："+ userWithDrawOrder.analyse +"</div>";
    			str += "  </div>";
    		}else{
    			str += "  <div class='child child11' >"+ userWithDrawOrder.statusStr +"</div>";
    		}
    		

    		str += " </div>";
    		str += "</div>";
    		userWithDrawOrderListObj.append(str);
    		str = "";

    	}
    }
    
    //处理失败原因
    $(".exampleButton").unbind("mouseover").mouseover(function(){
		$(this).next().show();
	});
	$(".exampleButton").unbind("mouseout").mouseout(function(){
        $(this).next().hide();
	});	
	
	 //显示分页栏
    $("#pageMsg").pagination({
        items: page.totalRecNum,
        itemsOnPage: page.pageSize,
        currentPage: page.pageNo,
		onPageClick:function(pageNumber) {
			withDrawOrderPage.pageQuery(pageNumber);
		}
    }); 
    
};



/**
 * 条件查询提现记录
 */
WithDrawOrderPage.prototype.queryConditionUserWithDrawOrders = function(queryParam,pageNo){
	var param={}
	if(queryParam.statuss!=null && queryParam.statuss!=""){
		param.statuss=queryParam.statuss;
	}
    if(queryParam.payType!=null && queryParam.payType!=""){
    	param.payType=queryParam.payType;
	}
    if(queryParam.queryScope!=null && queryParam.queryScope!=""){
		param.queryScope=queryParam.queryScope;
	}
    param.createdDateStart=queryParam.createdDateStart;
    param.createdDateEnd=queryParam.createdDateEnd;
    /*param.pageNo=pageNo;*/
    var query={
    		"query":param,
    		"pageNo":pageNo
    }
	$.ajax({
		type: "POST",
        url: contextPath +"/withdraw/withDrawOrderQuery",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(query), 
        dataType:"json",
        success: function(result){
        	commonHandlerResult(result, this.url);
            var data=result.data;
        	if(result.code == "ok"){
        		withDrawOrderPage.refreshUserWithdrawOrderPages(data);
        	}else if(result.code == "error"){
        		var err = result.data;
        		frontCommonPage.showKindlyReminder(err);
        	}
        }
	});
};

/**
 * 根据条件查找对应页
 */
WithDrawOrderPage.prototype.pageQuery = function(pageNo){
	if(pageNo < 1){
		withDrawOrderPage.pageParam.pageNo = 1;
	} else{
		withDrawOrderPage.pageParam.pageNo = pageNo;
	}
	//如果页码小于等于0，则不进行分页查找
	if(withDrawOrderPage.pageParam.pageNo <= 0){
		return;
	}
	
	withDrawOrderPage.findUserWithDrawOrderByQueryParam();
};