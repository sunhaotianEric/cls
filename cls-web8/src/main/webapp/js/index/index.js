//选中图片的数组集合
var selectedArray = new Array();
// 首页轮播
var banner = new change();

var oldOpenCodeStr;

function set_click() {
	if (currentUser.id == undefined || currentUser.id == null) {
		alert("sorry!请先登陆才能进行设置！");
		return;
	}
	$(".classify-alert").stop(false, true).fadeToggle(500);
}
function set_submit() {
	selectedArray.length = 0;
	$("#classifys li.on").each(function() {
		selectedArray.push($(this).attr("data-name"));

	});
	indexPage.updateTopSelected();
	set_click();

}

var notice = setInterval(function() {
	$(".main .lottery .notice .notice-container li:last").hide();
	var html = $(".main .lottery .notice .notice-container li:last");
	$(".main .lottery .notice .notice-container ul").prepend(html);
	$(".main .lottery .notice .notice-container li:first").slideDown(500);

}, 2000);

/**
 * 获取最新的开奖号码
 */
Indexpage.prototype.getLastLotteryCode = function() {
	var jsonData = {
		"lotteryKind" : indexPage.param.kindName,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/code/getLastLotteryCode",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var str = "";
				$("#lotteryLineText").empty();
				var resultData = result.data;
				if (resultData != null) {
					if (indexPage.param.kindName == 'BJPK10' || indexPage.param.kindName == 'JSPK10'
						|| indexPage.param.kindName == 'XYFT' || indexPage.param.kindName == 'SFPK10'
							|| indexPage.param.kindName == 'WFPK10' || indexPage.param.kindName == 'SHFPK10') {
						$("#lotteryLineText").attr("style", "padding-top:7px;");
						// $("#lotteryLineText").addClass("muns-pk10");
						str += "<div class='mun'style='width:40px;height:40px;line-height:40px;'>" + resultData.numInfo1 + "</div>";
						str += "<div class='mun'style='width:40px;height:40px;line-height:40px;'>" + resultData.numInfo2 + "</div>";
						str += "<div class='mun'style='width:40px;height:40px;line-height:40px;'>" + resultData.numInfo3 + "</div>";
						str += "<div class='mun'style='width:40px;height:40px;line-height:40px;'>" + resultData.numInfo4 + "</div>";
						str += "<div class='mun'style='width:40px;height:40px;line-height:40px;'>" + resultData.numInfo5 + "</div><br>";
						str += "<div class='mun'style='width:40px;height:40px;line-height:40px;'>" + resultData.numInfo6 + "</div>";
						str += "<div class='mun'style='width:40px;height:40px;line-height:40px;'>" + resultData.numInfo7 + "</div>";
						str += "<div class='mun'style='width:40px;height:40px;line-height:40px;'>" + resultData.numInfo8 + "</div>";
						str += "<div class='mun'style='width:40px;height:40px;line-height:40px;'>" + resultData.numInfo9 + "</div>";
						str += "<div class='mun'style='width:40px;height:40px;line-height:40px;'>" + resultData.numInfo10 + "</div>";
					} else if (indexPage.param.kindName.lastIndexOf('KS') > -1 || indexPage.param.kindName.lastIndexOf('DPC') > -1) {

						str += "<div class='mun'>" + resultData.numInfo1 + "</div>";
						str += "<div class='mun'>" + resultData.numInfo2 + "</div>";
						str += "<div class='mun'>" + resultData.numInfo3 + "</div>";
					} else if (indexPage.param.kindName == 'LHC' || indexPage.param.kindName == 'XYLHC') {
						$('.main .lottery-bar .lottery-left .content').addClass('small');
						var colors = indexPage.changeColorByCode(resultData);
						str += "<div class='mun " + colors[0] + "'>" + resultData.numInfo1 + "<br><span class='shengxiao " + colors[0] + "'>"
								+ indexPage.returnAninal(resultData.numInfo1) + "</span></div>";
						str += "<div class='mun " + colors[1] + "'>" + resultData.numInfo2 + "<br><span class='shengxiao " + colors[1] + "'>"
								+ indexPage.returnAninal(resultData.numInfo2) + "</span></div>";
						str += "<div class='mun " + colors[2] + "'>" + resultData.numInfo3 + "<br><span class='shengxiao " + colors[2] + "'>"
								+ indexPage.returnAninal(resultData.numInfo3) + "</span></div>";
						str += "<div class='mun " + colors[3] + "'>" + resultData.numInfo4 + "<br><span class='shengxiao " + colors[3] + "'>"
								+ indexPage.returnAninal(resultData.numInfo4) + "</span></div>";
						str += "<div class='mun " + colors[4] + "'>" + resultData.numInfo5 + "<br><span class='shengxiao " + colors[4] + "'>"
								+ indexPage.returnAninal(resultData.numInfo5) + "</span></div>";
						str += "<div class='mun " + colors[5] + "'>" + resultData.numInfo6 + "<br><span class='shengxiao " + colors[5] + "'>"
								+ indexPage.returnAninal(resultData.numInfo6) + "</span></div>";
						str += "<div class='and'>+</div>";
						str += "<div class='mun " + colors[6] + "'>" + resultData.numInfo7 + "<br><span class='shengxiao " + colors[6] + "'>"
								+ indexPage.returnAninal(resultData.numInfo7) + "</span></div>";
					} else if (indexPage.param.kindName == 'XYEB') {
						$('.main .lottery-bar .lottery-left .content').addClass('small').addClass('pt30');
						var sum = Number(resultData.numInfo1) + Number(resultData.numInfo2) + Number(r[1].numInfo3);
						str += "<div class='mun btn btn-fadered'>" + resultData.numInfo1 + "</div>+";
						str += "<div class='mun btn btn-fadered'>" + resultData.numInfo2 + "</div>+";
						str += "<div class='mun btn btn-fadered'>" + resultData.numInfo3 + "</div>=";
						str += "<div class='mun " + indexPage.changeColorByNumber(sum) + "'>" + sum + "</div>";
					} else {
						str += "<div class='mun'>" + resultData.numInfo1 + "</div>";
						str += "<div class='mun'>" + resultData.numInfo2 + "</div>";
						str += "<div class='mun'>" + resultData.numInfo3 + "</div>";
						str += "<div class='mun'>" + resultData.numInfo4 + "</div>";
						str += "<div class='mun'>" + resultData.numInfo5 + "</div>";
					}

					// 展示最新开奖号码的期号
					indexPage.showCurrentLotteryCode(resultData);
					$("#lotteryLineText").append(str);
					indexPage.showLotteryUrlBykindName(indexPage.param.kindName);

					oldOpenCodeStr = indexPage.getOpenCodeStr(resultData.lotteryNum);
					$('#oldNumber').text(oldOpenCodeStr);
				} else {
					$("#lotteryLineText").append("<p styssle='size:13px;color:red;'>预售中...</p>");
				}
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 显示
 */
Indexpage.prototype.showLotteryUrlBykindName = function(currentLotteryKind) {
	$("#btnlj").html("");
	if ("CQSSC" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/cqssc/cqssc.html\",\"\")'>"
						+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("TJSSC" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/tjssc/tjssc.html\",\"\")'>"
						+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("XJSSC" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/xjssc/xjssc.html\",\"\")'>"
						+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("JLFFC" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/jyffc/jyffc.html\",\"\")'>"
						+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("HGFFC" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/hgffc/hgffc.html\",\"\")'>"
						+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("XJPLFC" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/xjplfc/xjplfc.html\",\"\")'>"
						+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("TWWFC" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/twwfc/twwfc.html\",\"\")'>"
						+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("BJPK10" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/bjpks/bjpk10.html\",\"\")'>"
						+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("JSPK10" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/jspks/jspk10.html\",\"\")'>"
						+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("XYFT" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/xyft/xyft.html\",\"\")'>"
				+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("SFPK10" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/sfpk10/sfpk10.html\",\"\")'>"
				+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("WFPK10" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/wfpk10/wfpk10.html\",\"\")'>"
				+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("SHFPK10" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/shfpk10/shfpk10.html\",\"\")'>"
				+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("SDSYXW" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/sdsyxw/sdsyxw.html\",\"\")'>"
						+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("GDSYXW" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/gdsyxw/gdsyxw.html\",\"\")'>"
						+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("FJSYXW" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/fjsyxw/fjsyxw.html\",\"\")'>"
						+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("JXSYXW" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/jxsyxw/jxsyxw.html\",\"\")'>"
						+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("WFSYXW" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/wfsyxw/wfsyxw.html\",\"\")'>"
				+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("SFSYXW" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/sfsyxw/sfsyxw.html\",\"\")'>"
				+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("JSKS" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/jsks/jsks.html\",\"\")'>"
						+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("AHKS" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/ahks/ahks.html\",\"\")'>"
						+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("JLKS" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.showKindlyReminder(\"开发中，敬请期待\")'>" + "<span style='color: #fff;'>立即投注</span><img src='" + contextPath
						+ "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("HBKS" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/hbks/hbks.html\",\"\")'>"
						+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("BJKS" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/bjks/bjks.html\",\"\")'>"
						+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("JYKS" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/jyks/jyks.html\",\"\")'>"
						+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("GXKS" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/gxks/gxks.html\",\"\")'>"
						+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("GSKS" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/gsks/gsks.html\",\"\")'>"
						+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("SHKS" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/shks/shks.html\",\"\")'>"
						+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("HBTB" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.showKindlyReminder(\"开发中，敬请期待\")'>" + "<span style='color: #fff;'>立即投注</span><img src='" + contextPath
						+ "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("JSTB" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.showKindlyReminder(\"开发中，敬请期待\")'>" + "<span style='color: #fff;'>立即投注</span><img src='" + contextPath
						+ "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("AHTB" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.showKindlyReminder(\"开发中，敬请期待\")'>" + "<span style='color: #fff;'>立即投注</span><img src='" + contextPath
						+ "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("JLTB" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.showKindlyReminder(\"开发中，敬请期待\")'>" + "<span style='color: #fff;'>立即投注</span><img src='" + contextPath
						+ "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("FCSDDPC" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/fcsddpc/fcsddpc.html\",\"\")'>"
						+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("LHC" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/lhc/lhc.html\",\"\")'>"
						+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("XYLHC" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/xylhc/xylhc.html\",\"\")'>"
						+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("XYEB" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/xyeb/xy28.html\",\"\")'>"
						+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("SHFSSC" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/shfssc/shfssc.html\",\"\")'>"
				+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("SFSSC" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/sfssc/sfssc.html\",\"\")'>"
				+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("LCQSSC" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/lcqssc/lcqssc.html\",\"\")'>"
				+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("WFSSC" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/wfssc/wfssc.html\",\"\")'>"
				+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("SFKS" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/sfks/sfks.html\",\"\")'>"
				+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else if ("WFKS" == currentLotteryKind) {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/wfks/wfks.html\",\"\")'>"
				+ "<span style='color: #fff;'>立即投注</span><img src='" + contextPath + "/front/images/index/icon-shop.png' alt=''></a>");
	} else {
		$("#btnlj").append(
				"<a href='javascript:frontCommonPage.showKindlyReminder(\"开发中，敬请期待\")'>" + "<span style='color: #fff;'>立即投注</span><img src='" + contextPath
						+ "/front/images/index/icon-shop.png' alt=''></a>");
	}

};

/**
 * 显示
 */
Indexpage.prototype.showLotteryCodeByResult = function(kindName, rs) {
	alert(rs[1].numInfo1);
	var str = "";

};

/**
 * 获取开奖号码前台展现的字符串
 */
Indexpage.prototype.getOpenCodeStr = function(openCodeNum) {
	var openCodeStr = "";
	if (isNotEmpty(openCodeNum)) {
		var expectDay = openCodeNum.substring(0, openCodeNum.length - 3);
		var expectNum = openCodeNum.substring(openCodeNum.length - 3, openCodeNum.length);
		openCodeStr = expectDay + "-" + expectNum;
	}
	return openCodeStr;
};

/* scroll */
var imgBool = [], imgBool1 = [], main_scroll = new keepScroll(), timer;
$(".banner .content img").each(function(index, element) {
	var img = new Image();
	img.src = $(element).attr("src");
	imgBool[index] = false;
	imgBool1[index] = true;
	base.imgLoad(img, function() {
		imgBool[index] = true;
	});
});

function update_muns(time, mun_arr, stage) {
	/*
	 * time(倒数时间 {"m":02,"s":30}) 上一期开奖号码数组（[0,1,2,3,4,5]） stage(上一次期数
	 * 20160214-056)
	 */
	$(".main .lottery .timely .muns").addClass("on");
	setTimeout(function() {
		$(".main .lottery .timely .muns").removeClass("on");
	}, 2000);
	// $(".main .lottery .timely .time1 span").html(stage);
	// $.each(mun_arr,function(i,n){
	// $(".main .lottery .timely .muns .child"+i+" .child-title").html(n);
	// });
	$(".main .lottery .timely .time .m").html(time["m"]);
	$(".main .lottery .timely .time .s").html(time["s"]);
	update_interval();
}
function update_interval() {
	var interval = setInterval(function() {
		var m = parseInt($(".main .lottery .timely .time .m").html()), s = parseInt($(".main .lottery .timely .time .s").html());
		if (s == 0 && m != 0) {
			s = 59;
			m--;
		} else if ((s != 0 && m != 0) || (m == 0 && s != 0)) {
			s--;
		} else { // 倒计时结束
			clearInterval(interval);
			return 0;
		}
		m = m < 10 ? "0" + m : m;
		s = s < 10 ? "0" + s : s;
		$(".main .lottery .timely .time .m").html(m);
		$(".main .lottery .timely .time .s").html(s);
	}, 1000);
}
timer = setInterval(function() {
	if (imgBool1.toString() == imgBool.toString()) {
		$(".loading").hide();
		$(".header").addClass("loading-in");
		$(".banner").addClass("loading-in");
		$(".lottery").addClass("loading-in");

		setTimeout(function() {

			$(".header").removeClass("loading-in");
			$(".banner").removeClass("loading-in");
			$(".lottery").removeClass("loading-in");

			// update_muns({"m":00,"s":30},[Opencode.numInfo1,Opencode.numInfo2,Opencode.numInfo3,Opencode.numInfo4,Opencode.numInfo5],oldOpenCodeStr);
			update_muns({
				"m" : 00,
				"s" : 30
			}, [], oldOpenCodeStr);
			setInterval(function() {
				update_muns({
					"m" : 01,
					"s" : 30
				}, [], oldOpenCodeStr);
			}, 90000);

			main_scroll.init({
				element_name : [ '.footer .head', '.footer .foot' ]
			});
		}, 2000);
		clearInterval(timer);
	}
}, 50);
/*
 * timer = setInterval(function() { if (imgBool1.toString()==imgBool.toString()) {
 * $(".loading").hide(); $(".header").addClass("loading-in");
 * $(".banner").addClass("loading-in"); $(".lottery").addClass("loading-in");
 * 
 * setTimeout(function(){
 * 
 * $(".header").removeClass("loading-in");
 * $(".banner").removeClass("loading-in");
 * $(".lottery").removeClass("loading-in"); main_scroll.init({
 * element_name:['.footer .head','.footer .foot'] }); },2000);
 * clearInterval(timer); } }, 50);
 */

function Indexpage() {
}
var indexPage = new Indexpage();

// 参数
indexPage.param = {
	kindName : "CQSSC", // 首页显示倒计时的彩种
	diffTime : 0, // 时间倒计时剩余时间
	currentExpect : null, // 当前期号
	intervalKey : null, // 倒计时周期key
	progressTime : 2000, // 分数进度时间
	isCanActiveExpect : true,
	position1LotteryType : null,
	position2LotteryType : null,
	position3LotteryType : null,
	position4LotteryType : null,
	position5LotteryType : null,
	position6LotteryType : null,
	position7LotteryType : null,
	bizSystemConfigVO : null,

};
/*
 * //分页参数 indexPage.param = { queryMethodParam : null, pageNo : 1, pageMaxNo :
 * 1, //最大的页数 skipHtmlStr : "" };
 */
// 轮播参数
indexPage.param.isClick = false;

$(document).ready(function() {

	// 顶部导航选中处理
	$("#navChoose .nav-child").removeClass("on");
	$("#navChoose .nav-child:eq(0)").addClass("on");

	// 展示首页轮播图片
	indexPage.getHomeActivityImage();

	// 显示当前余额和用户名
	$("#showAllBall").parent().hide();
	$("#hiddBall").show();
	if (currentUser != "{}") {
		if (currentUser.money == null) {
			$("#userName").text("---");
			$("#spanBall").text("00.000");
		} else {
			var money = parseFloat(currentUser.money).toFixed(headerPage.param.toFixedValue);
			$("#spanBall").text(money);
			$("#userName").text(currentUser.username);
		}
	}

	// 提示框关闭
	$(".close-reveal-modal").unbind("click").click(function(event) {
		$(this).parent().parent().parent().hide();
		$("#shadeFloor").hide();
	});

	// 跳往去维护用户信息的页面
	$("#userMsgPerfectDialogSureButton").unbind("click").click(function() {
		window.open(contextPath + "/gameBet/accountCenter.html");
		$("#shadeFloor").hide();
		$("#userMsgPerfectDialog").hide();
	});
	$("#userMsgPerfectDialogCancelButton").unbind("click").click(function() {
		$("#shadeFloor").hide();
		$("#userMsgPerfectDialog").hide();
	});

	// 判断当前登录用户是否为空
	if (!$.isEmptyObject(currentUser)) {
		// 展示公告
		// indexPage.getNewestAnnounce();
		// 展示公告列表
		indexPage.getAnnounceList();
	}
	// 首页首推彩种
	indexPage.ShowIndexLotteryKind();

	// 首页中奖排行列表
	indexPage.getNearestWinLottery();

	// 系统安全检测安全信息
	if (currentUser.haveSafePassword != 1 || currentUser.haveSafeQuestion <= 0) {
		$("#shadeFloor").show();
		$("#userMsgPerfectDialog").show();
	}

	// 获取当前用户的自定义彩种信息
	indexPage.loadCurrentUserLotteryKind();

	window.setInterval("indexPage.getLastLotteryCode()", 10000); // 读取最新的开奖号码

	/* 设置常用彩票 */
	$("#classifys .checkbox").click(function() {
		var parent = $(this).closest('li');
		var bool = parent.hasClass("on");
		if (bool) {
			parent.removeClass("on");
		} else {
			var len = $("#classifys li.on").length;
			if (len >= 7) {
				alert("您选择的个数已经超过7个");
				$(this).removeAttr('checked');
			} else {
				parent.addClass("on");
			}
		}
	});

	// $(".popup .close").click(function(){
	// $(".popup").fadeOut(500);
	// });

	// 首页顶部导航链接选中样式
	$('.header .foot .nav .child1').addClass('on');

	/* 公告 */
	$(".main .lottery .notice .notice-title p").mouseover(function() {
		var index = $(".main .lottery .notice .notice-title p").index($(this));
		$(".main .lottery .notice .notice-title p").removeClass("on");
		$(this).addClass("on");
		$(".main .lottery .notice .notice-container").removeClass("on");
		$(".main .lottery .notice .notice-container:eq(" + index + ")").addClass("on");
	});

	// 新手大礼包事件处理
	$('.receive-btn').click(function() {
		window.open(contextPath + "/gameBet/newUserGift.html");
	});
	$('.close-btn').click(function() {
		$('.pop').hide();
		$('.pop-btn').show();
	});
	$('.pop-btn').click(function() {
		$('.pop').show();
		$('.pop-btn').hide();
	});
	// 是否弹出新手礼包活动弹框
	indexPage.showNewUserGift();

	// 足球外链处理
	if (indexPage.param.bizSystemConfigVO.isFootballOpen == 1) {
		$('.footballgame').show();
		$('.footballgame').attr("href", indexPage.param.bizSystemConfigVO.footballUrl);
		$('.notice').addClass('footballgame-on');
	} else {
		$('.footballgame').hide();
		$('.notice').removeClass('footballgame-on');
	}

});

/**
 * 获取当前用户的自定义彩种信息
 */
Indexpage.prototype.loadCurrentUserLotteryKind = function() {

	$.ajax({
		type : "POST",
		url : contextPath + "/system/getUserCustomLotteryKind",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				selectedArray = new Array();
				var resultData = result.data;
				if (resultData != null) {
					if (resultData.position1LotteryType != null && resultData.position1LotteryType != "") {
						selectedArray.push(resultData.position1LotteryType);
					}
					if (resultData.position2LotteryType != null && resultData.position2LotteryType != "") {
						selectedArray.push(resultData.position2LotteryType);
					}
					if (resultData.position3LotteryType != null && resultData.position3LotteryType != "") {
						selectedArray.push(resultData.position3LotteryType);
					}
					if (resultData.position4LotteryType != null && resultData.position4LotteryType != "") {
						selectedArray.push(resultData.position4LotteryType);
					}
					if (resultData.position5LotteryType != null && resultData.position5LotteryType != "") {
						selectedArray.push(resultData.position5LotteryType);
					}
					if (resultData.position6LotteryType != null && resultData.position6LotteryType != "") {
						selectedArray.push(resultData.position6LotteryType);
					}
					if (resultData.position7LotteryType != null && resultData.position7LotteryType != "") {
						selectedArray.push(resultData.position7LotteryType);
					}
					if (resultData.position8LotteryType != null && resultData.position8LotteryType != "") {
						selectedArray.push(resultData.position8LotteryType);
					}
					if (resultData.position9LotteryType != null && resultData.position9LotteryType != "") {
						selectedArray.push(resultData.position9LotteryType);
					}
				}
				// 更新图片
				indexPage.updateTopSelected();
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 根据数组更新首页默认的彩种
 */
Indexpage.prototype.updateTopSelected = function() {
	// 先删除所有首页收藏的彩种
	$(".classify").children("a").remove();

	if (selectedArray.length > 0) {
		var picSrc = "";
		// 循环获取出首页彩种开关数据.该数组是从index.jsp中传递过来.
		for (var i = 0; i < indexLotterySwitchs.length; i++){
			// 获取单个彩种开关数据.
			var lotterySwitch = indexLotterySwitchs[i];
			// 获取彩种类型.
			var currentLotteryKind = lotterySwitch.lotteryType;
			if ("CQSSC" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/cqssc/cqssc.html\",\"\")'>" + "<div class='child'>"
						+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/icon-shishicai.png' /></div>"
						+ "<p class='child-title'>重庆时时彩</p></div></a>";
			} else if ("TJSSC" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/tjssc/tjssc.html\",\"\")'>" + "<div class='child'>"
						+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/icon-shishicai.png' />"
						+ "</div><p class='child-title'>天津时时彩</p></div></a>";
			} else if ("XJSSC" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/xjssc/xjssc.html\",\"\")'>" + "<div class='child'>"
						+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/icon-shishicai.png' />"
						+ "</div><p class='child-title'>新疆时时彩</p></div></a>";
			} else if ("JLFFC" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/jyffc/jyffc.html\",\"\")'>" + "<div class='child'>"
						+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/icon-fenfencai.png' />"
						+ "</div><p class='child-title'>幸运分分彩</p></div></a>";
			} else if ("HGFFC" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/hgffc/hgffc.html\",\"\")'>" + "<div class='child'>"
						+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/logo_1.5fencai.png' />"
						+ "</div><p class='child-title'>韩国1.5分彩</p></div></a>";
			} else if ("XJPLFC" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/xjplfc/xjplfc.html\",\"\")'>" + "<div class='child'>"
						+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/logo_erfencai.png' />"
						+ "</div><p class='child-title'>新加坡二分彩</p></div></a>";
			} else if ("TWWFC" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/twwfc/twwfc.html\",\"\")'>" + "<div class='child'>"
						+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/logo_wufencai.png' />"
						+ "</div><p class='child-title'>台湾五分彩</p></div></a>";
			} else if ("BJPK10" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/bjpks/bjpk10.html\",\"\")'>" + "<div class='child'>"
						+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/icon-pk10.png' />" + "</div><p class='child-title'>北京PK10</p></div></a>";
			} else if ("JSPK10" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/jspks/jspk10.html\",\"\")'>" + "<div class='child'>"
						+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/icon-PK10-95.png' />"
						+ "</div><p class='child-title'>极速PK10</p></div></a>";
			} else if ("XYFT" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/xyft/xyft.html\",\"\")'>" + "<div class='child'>"
				+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/icon-xyft.png' />"
				+ "</div><p class='child-title'>幸运飞艇</p></div></a>";
			} else if ("SFPK10" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/sfpk10/sfpk10.html\",\"\")'>" + "<div class='child'>"
				+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/icon-PK10-common.png' />"
				+ "</div><p class='child-title'>三分PK10</p></div></a>";
			} else if ("WFPK10" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/wfpk10/wfpk10.html\",\"\")'>" + "<div class='child'>"
				+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/icon-PK10-common.png' />"
				+ "</div><p class='child-title'>五分PK10</p></div></a>";
			} else if ("SHFPK10" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/shfpk10/shfpk10.html\",\"\")'>" + "<div class='child'>"
				+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/icon-PK10-common.png' />"
				+ "</div><p class='child-title'>十分PK10</p></div></a>";
			} else if ("SDSYXW" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/sdsyxw/sdsyxw.html\",\"\")'>" + "<div class='child'>"
						+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/icon-11xuan5.png' />"
						+ "</div><p class='child-title'>山东11选5</p></div></a>";
			} else if ("GDSYXW" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/gdsyxw/gdsyxw.html\",\"\")'>" + "<div class='child'>"
						+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/icon-11xuan5.png' />"
						+ "</div><p class='child-title'>广东11选5</p></div></a>";
			} else if ("FJSYXW" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/fjsyxw/fjsyxw.html\",\"\")'>" + "<div class='child'>"
						+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/icon-11xuan5.png' />"
						+ "</div><p class='child-title'>福建11选5</p></div></a>";
			} else if ("JXSYXW" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/jxsyxw/jxsyxw.html\",\"\")'>" + "<div class='child'>"
						+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/icon-11xuan5.png' />"
						+ "</div><p class='child-title'>江西11选5</p></div></a>";
			} else if ("WFSYXW" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/wfsyxw/wfsyxw.html\",\"\")'>" + "<div class='child'>"
				+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/icon-11xuan5.png' />" + "</div><p class='child-title'>五分11选5</p></div></a>";
			} else if ("SFSYXW" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/sfsyxw/sfsyxw.html\",\"\")'>" + "<div class='child'>"
				+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/icon-11xuan5.png' />" + "</div><p class='child-title'>三分11选5</p></div></a>";
			} else if ("JSKS" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/jsks/jsks.html\",\"\")'>" + "<div class='child'>"
						+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/icon-kuai3.png' />" + "</div><p class='child-title'>江苏快3</p></div></a>";
			} else if ("AHKS" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/ahks/ahks.html\",\"\")'>" + "<div class='child'>"
						+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/icon-kuai3.png' />" + "</div><p class='child-title'>安徽快3</p></div></a>";
			} else if ("JLKS" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.showKindlyReminder(\"开发中，敬请期待\")'>" + "<div class='child'>" + "<div class='child-icon'><img src='" + contextPath
						+ "/front/images/lottery/logo/icon-kuai3.png' />" + "</div><p class='child-title'>吉林快3</p></div></a>";
			} else if ("HBKS" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/hbks/hbks.html\",\"\")'>" + "<div class='child'>"
						+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/icon-kuai3.png' />" + "</div><p class='child-title'>湖北快3</p></div></a>";
			} else if ("BJKS" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/bjks/bjks.html\",\"\")'>" + "<div class='child'>"
						+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/icon-kuai3.png' />" + "</div><p class='child-title'>北京快3</p></div></a>";
			} else if ("JYKS" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/jyks/jyks.html\",\"\")'>" + "<div class='child'>"
						+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/icon-kuai3.png' />" + "</div><p class='child-title'>幸运快3</p></div></a>";
			} else if ("GXKS" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/gxks/gxks.html\",\"\")'>" + "<div class='child'>"
						+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/icon-kuai3.png' />" + "</div><p class='child-title'>广西快3</p></div></a>";
			} else if ("GSKS" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/gsks/gsks.html\",\"\")'>" + "<div class='child'>"
						+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/icon-kuai3.png' />" + "</div><p class='child-title'>甘肃快3</p></div></a>";
			} else if ("SHKS" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/shks/shks.html\",\"\")'>" + "<div class='child'>"
						+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/icon-kuai3.png' />" + "</div><p class='child-title'>上海快3</p></div></a>";
			} else if ("HBTB" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.showKindlyReminder(\"开发中，敬请期待\")'>" + "<div class='child'>" + "<div class='child-icon'><img src='" + contextPath
						+ "/front/images/lottery/logo/toubao-hubei.png' />" + "</div><p class='child-title'>湖北</p></div></a>";
			} else if ("JSTB" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.showKindlyReminder(\"开发中，敬请期待\")'>" + "<div class='child'>" + "<div class='child-icon'><img src='" + contextPath
						+ "/front/images/lottery/logo/toubao-jiangsu.png' />" + "</div><p class='child-title'>江苏</p></div></a>";
			} else if ("AHTB" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.showKindlyReminder(\"开发中，敬请期待\")'>" + "<div class='child'>" + "<div class='child-icon'><img src='" + contextPath
						+ "/front/images/lottery/logo/toubao-anhui.png' />" + "</div><p class='child-title'>安徽</p></div></a>";
			} else if ("JLTB" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.showKindlyReminder(\"开发中，敬请期待\")'>" + "<div class='child'>" + "<div class='child-icon'><img src='" + contextPath
						+ "/front/images/lottery/logo/toubao-jilin.png' />" + "</div><p class='child-title'>吉林</p></div></a>";
			} else if ("FCSDDPC" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/fcsddpc/fcsddpc.html\",\"\")'>"
						+ "<div class='child'>" + "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/icon-3d.png' />"
						+ "</div><p class='child-title'>福彩3D</p></div></a>";
			} else if ("LHC" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/lhc/lhc.html\",\"\")'>" + "<div class='child'>"
						+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/icon-liuhecai.png' />"
						+ "</div><p class='child-title'>六合彩</p></div></a>";
			} else if ("XYLHC" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/xylhc/xylhc.html\",\"\")'>" + "<div class='child'>"
						+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/icon-xyliuhecai.png' />"
						+ "</div><p class='child-title'>幸运六合彩</p></div></a>";
			} else if ("XYEB" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/xyeb/xy28.html\",\"\")'>" + "<div class='child'>"
						+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/icon-xy28.png' />" + "</div><p class='child-title'>幸运28</p></div></a>";
			} else if ("SHFSSC" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/shfssc/shfssc.html\",\"\")'>" + "<div class='child'>"
				+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/icon-shishicai.png' />" + "</div><p class='child-title'>十分时时彩</p></div></a>";
			} else if ("SFSSC" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/sfssc/sfssc.html\",\"\")'>" + "<div class='child'>"
				+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/icon-shishicai.png' />" + "</div><p class='child-title'>三分时时彩</p></div></a>";
			} else if ("LCQSSC" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/lcqssc/lcqssc.html\",\"\")'>" + "<div class='child'>"
				+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/icon-shishicai.png' />" + "</div><p class='child-title'>老重庆时时彩</p></div></a>";
			} else if ("WFSSC" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/wfssc/wfssc.html\",\"\")'>" + "<div class='child'>"
				+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/icon-shishicai.png' />" + "</div><p class='child-title'>五分时时彩</p></div></a>";
			} else if ("SFKS" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/sfks/sfks.html\",\"\")'>" + "<div class='child'>"
				+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/icon-kuai3.png' />" + "</div><p class='child-title'>三分快三</p></div></a>";
			} else if ("WFKS" == currentLotteryKind) {
				picSrc += "<a href='javascript:frontCommonPage.judgeCurrentUserToLogin(\"" + contextPath + "/gameBet/lottery/wfks/wfks.html\",\"\")'>" + "<div class='child'>"
				+ "<div class='child-icon'><img src='" + contextPath + "/front/images/lottery/logo/icon-kuai3.png' />" + "</div><p class='child-title'>五分快三</p></div></a>";
			} 
			$("#classifys li").each(function() {
				if (selectedArray.length != 0) {
					for (var i = 0; i < selectedArray.length; i++) {
						var lotteryKind = selectedArray[i];
						var attr = $(this).attr("data-name");
						if (lotteryKind == attr) {
							$(this).addClass("on");
							$(this).children().children().attr("checked", "checked");
						}
					}
				}
			});
		}
		$(".classify").append(picSrc);
		var alen = $(".classify").children("a").length;

		if (alen <= 9) {
			var count = 10 - alen;
			for (var i = 0; i < count; i++) {
				var tr = "<a><div class='child'></div></a>";
				if (i == count - 1) {
					tr = "<a href=" + contextPath + "/gameBet/hall.html>" + "<div class='child'>" + "<div class='child-icon'><img src='" + contextPath
							+ "/front/images/lottery/logo/icon-more.png' />" + "</div><p class='child-title'>更多</p></div></a>";
				}
				$(".classify").append(tr);

			}
		}

		indexPage.highlightDisplayTopSelected();
	}
}

/**
 * 收藏玩法图片加亮
 */
Indexpage.prototype.highlightDisplayTopSelected = function() {
	/* 通告 */
	$(".main .lottery .classify .child").mouseleave(function() {
		var imageSrc = $(this).find(".child-img").find("img").attr("src");
		if (typeof (imageSrc) != "undefined") {
			if (imageSrc.indexOf("_on.png") > 0) {
			} else {
				imageSrc = imageSrc.replace(".png", "_on.png");
			}
		}
		$(this).find(".child-img").find("img").attr("src", imageSrc);
	}).mouseover(function() {
		var imageSrc = $(this).find(".child-img").find("img").attr("src");
		if (typeof (imageSrc) != "undefined") {
			if (imageSrc.indexOf("_on.png") > 0) {
				imageSrc = imageSrc.replace("_on.png", ".png");
			} else {
			}
		}
		$(this).find(".child-img").find("img").attr("src", imageSrc);
	});
}

/**
 * 获取系统当前正在投注的期号
 */
Indexpage.prototype.getActiveExpect = function() {
	// 判断是否可以进行期号的请求
	if (indexPage.param.isCanActiveExpect) {
		// 封装参数
		var jsonData = {
			"lotteryKind" : indexPage.param.kindName,
		};
		$.ajax({
			type : "POST",
			url : contextPath + "/code/getExpectAndDiffTime",
			data : JSON.stringify(jsonData),
			dataType : "json",
			contentType : 'application/json;charset=utf-8',
			success : function(result) {
				commonHandlerResult(result, this.url);
				if (result.code == "ok") {
					indexPage.param.isCanActiveExpect = false;
					var issue = result.data;
					if (issue != null) {
						if (issue.specialExpect == 2) {
							// 显示暂停销售
							$("#diffTime").html("<h2>暂停销售</h2>");
						} else {
							// 记录当前期号
							indexPage.param.currentExpect = issue.lotteryNum;
							// 当前期号剩余时间倒计时
							indexPage.param.diffTime = issue.timeDifference;
							indexPage.calculateTime();
							window.clearInterval(indexPage.param.intervalKey); // 终止周期性倒计时
							indexPage.param.intervalKey = setInterval("indexPage.calculateTime()", 1000);
							// setInterval("indexPage.getLastLotteryCode()",1000);
						}
					} else {
						$("#lotteryLineText").append("<p style='size:13px;color:red;'>预售中...</p>");
					}

					if (indexPage.param.diffTime > 5000) { // 如果剩余时间超过5秒的
						setTimeout("indexPage.controlIsCanActive()", 2000);
					} else {
						indexPage.param.isCanActiveExpect = true;
					}
				} else if (result.code == "error") {
					frontCommonPage.showKindlyReminder(result.data);
				}
			}
		});
	}
};

/**
 * 图片轮播,点击事件
 */
Indexpage.prototype.carouselImage = function() {
	var len = $("#indexSliderBox ul li").length; // 获取焦点图个数
	var index = 0;
	var picTimer;

	var btn = "<div class='sliderRound'>";
	for (var i = 0; i < len; i++) {
		btn += "<a href='javascript:void(0);'></a>";
	}
	btn += "</div>";

	$("#indexSliderBox").append(btn);

	$("#indexSliderBox .sliderRound a").mouseover(function() {
		index = $("#indexSliderBox .sliderRound a").index(this);
		showPics(index);
	}).eq(0).trigger("mouseover");

	function showPics(index) { // 普通切换
		$("#indexSliderBox ul li").eq(index).stop(true, false).fadeIn(500).siblings("li").fadeOut(500);
		$("#indexSliderBox .sliderRound a").removeClass("current").eq(index).addClass("current");
	}

	$("#indexSliderBox").hover(function() {
		if (picTimer) {
			clearInterval(picTimer);
		}
	}, function() {
		picTimer = setInterval(function() {
			showPics(index);
			index++;
			if (index == $("#indexSliderBox ul li").length) {
				index = 0;
			}
		}, 3000);
	});
	// 自动开始
	var picTimer = setInterval(function() {
		showPics(index);
		index++;
		if (index == $("#indexSliderBox ul li").length) {
			index = 0;
		}
	}, 3000);
};

/**
 * 分分彩滚动事件
 */
$.fn.ScrollProstate = function(options) {
	var d = {
		time : 2000,
		s : 'current',
		num : 1
	};
	var o = $.extend(d, options);

	this.children('ul').addClass('line');
	var _con = $('.line').eq(0);
	var _conH = _con.height();
	var _conChildH = _con.children().eq(0).height();
	var _temp = _conChildH;
	var _time = d.time;
	var _s = d.s;

	_con.clone().insertAfter(_con);

	var num = d.num;
	var _p = this.find('li');
	var allNum = _p.length;

	_p.eq(num).addClass(_s);

	var timeID = setInterval(Up, _time);
	this.hover(function() {
		clearInterval(timeID)
	}, function() {
		timeID = setInterval(Up, _time);
	});

	function Up() {
		_con.animate({
			marginTop : '-' + _conChildH
		});
		_p.removeClass(_s);
		num += 1;
		_p.eq(num).addClass(_s);

		if (_conH == _conChildH) {
			_con.animate({
				marginTop : '-' + _conChildH
			}, "normal", over);
		} else {
			_conChildH += _temp;
		}
	}
	function over() {
		_con.attr("style", 'margin-top:0');
		_conChildH = _temp;
		num = 1;
		_p.removeClass(_s);
		_p.eq(num).addClass(_s);
	}
};

/**
 * 展示首页图片
 */
Indexpage.prototype.getHomeActivityImage = function() {
	$.ajax({
		type : "POST",
		url : contextPath + "/index/getHomeImage",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var homeActivityImageObj = $("#homeActivityImage");
				var navIdObj = $("#navId");
				homeActivityImageObj.html("");
				navIdObj.html("");
				var homePicConfigs = result.data;
				var str = "";
				for (var i = 0; i < homePicConfigs.length; i++) {
					var homePicConfig = homePicConfigs[i];
					if (typeof (homePicConfig.picLinkUrl) != "undifined" && homePicConfig.picLinkUrl != null) {
						var reg = /^(http|https):\/\//;
						var picUrl = homePicConfig.picLinkUrl;
						if (!reg.test(picUrl)) {
							str += '<a href="' + contextPath + '/' + homePicConfig.picLinkUrl + '" target="_blank">';
						} else {
							str += '<a href="' + homePicConfig.picLinkUrl + '" target="_blank">';
						}
					} else {
						str += '<a href="javascript:void(0)">';
					}
					str += '	<div class="content load-image" style="background-image:url(' + homePicConfig.picUrl + ');"></div>';
					str += '</a>';
					homeActivityImageObj.append(str);
					str = "";

					$("#navId").append('<div class="child" onclick="banner.todo({\'index\':' + i + '})"></div>');

				}

				if (!$("#navId .child").onclick) {
					$("#navId .child").remove();
				}
				// 首页轮播
				indexPage.homeImageBannerShow();
				// 图片轮播,点击事件
				indexPage.carouselImage();
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 首页轮播
 */
Indexpage.prototype.homeImageBannerShow = function() {
	/* banner */
	banner.position = function() {
		change().position.call(this);

		if (this.data["position_style"] == "banner") {
			$(this.data["element"]).css({
				"left" : -this.data["position_width"] + "px"
			});
			$(this.data["element"] + ":eq(" + this.data["index"] + ")").css({
				"left" : "0px"
			});
		}
	}
	banner.todo = function(data) {

		var bool = change().todo.call(this, data);
		if (!bool)
			return false;

		if (this.data["position_style"] == "banner") {
			var direc = -1;
			if (data["direc"] == "-")
				direc = 1;
			$(this.data["element"] + ":eq(" + this.data["lastindex"] + ")").stop(false, true).animate({
				"left" : direc * this.data["position_width"] + "px"
			}, {
				easing : 'jswing',
				duration : 1000
			}).removeClass("on");
			$(this.data["element"] + ":eq(" + this.data["index"] + ")").stop(false, true).css({
				"left" : -direc * this.data["position_width"] + "px"
			}).animate({
				"left" : "0px"
			}, {
				easing : 'jswing',
				duration : 1000,
				complete : function() {
					banner.data["todo_bool"] = false;
				}
			}).addClass("on");
		}
	}
	/*
	 * banner.init({ "element_move":".banner", "element":".banner .content",
	 * "position_style":"banner", "autoplay_time":8000, "autoplay":true });
	 */

	banner.init({
		"element_move" : ".banner",
		"element" : ".banner .content",
		"position_style" : "banner",
		"btn_parent" : ".banner .nav",
		"btn_function" : "banner",
		"autoplay_time" : 8000,
		"autoplay" : true
	});
}

/**
 * 查看公告内容
 */
Indexpage.prototype.showAnnounceDetail = function(id) {
	window.location.href = contextPath + "/gameBet/announceDetail.html?parm1=" + id;
};

/**
 * 展示当前正在投注的期号,当前属于时时彩的期号管理
 */
Indexpage.prototype.showCurrentLotteryCode = function(lotteryIssue) {
	var lotteryNum = lotteryIssue.lotteryNum;
	expectDay = lotteryNum.substring(0, lotteryNum.length - 3);
	expectNum = lotteryNum.substring(lotteryNum.length - 3, lotteryNum.length);
	if ((lotteryIssue.lotteryName.indexOf("PK10") > -1 || lotteryIssue.lotteryName=="XYFT") || lotteryIssue.lotteryName.indexOf("XYEB") > -1) {
		$('#nowNumber').text("第" + expectDay + expectNum + "期开奖号码");
	} else {
		$('#nowNumber').text("第" + expectDay + "-" + expectNum + "期开奖号码");
	}
};

/**
 * 获取最近的20条中奖记录
 */
Indexpage.prototype.getNearestWinLottery = function() {

	$.ajax({
		type : "POST",
		url : contextPath + "/index/getNearestWinLottery",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var winList = result.data;
				$("#winnerList").html("");
				var str = "";
				for (var i = 0; i < winList.length; i++) {
					var win = winList[i];
					str += "<li><p><span>[" + win.userName + "]</span>&nbsp;&nbsp;在[" + win.lotteryTypeDes + "]中奖" + win.bonus + "元</p></li>";
					$("#winnerList").append(str);
					str = "";
				}
				// 不到20条的中奖记录
				if (winList.length < 20) {
					for (var j = 0; j < (20 - winList.length); j++) {
						var random1 = Math.round(Math.random() * 25) + 0;
						var random2 = Math.round(Math.random() * 25) + 0;
						var random3 = Math.round(Math.random() * 25) + 0;
						var bonus = GetRndNum(1000, 30000) + "." + GetRndNum(0, 20);
						str += "<li><p><span>[" + letters[random1] + letters[random2] + letters[random3] + "****]</span>&nbsp;&nbsp;在[重庆时时彩]中奖" + bonus + "元</p></li>";
						$("#winnerList").append(str);
						str = "";
					}
				}

				// 首页中奖信息滚动显示
				$('#scrollProstateInfo').ScrollProstate({
					time : 2000,
					s : 'current',
					num : 1
				});
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 服务器时间倒计时
 */
Indexpage.prototype.calculateTime = function() {
	var SurplusSecond = indexPage.param.diffTime;
	var diffTimeStr = "";
	if (SurplusSecond >= 3600 && indexPage.param.kindName != 'LHC' && indexPage.param.kindName != 'FCSDDPC') {
		$("#lotteryLineText").html("<p styssle='size:13px;color:red;'>预售中...</p>");
		$("#lotteryLineText").removeClass("muns");
		$(".time-title").hide();
		$("#diffTime").hide();
		$("#btnlj").hide();

		window.clearInterval(indexPage.param.intervalKey); // 终止周期性倒计时
	} else {
		// 福彩3d和六合彩，倒计时显示三位数
		if (indexPage.param.kindName == 'FCSDDPC' || indexPage.param.kindName == 'LHC') {
			var h = Math.floor(SurplusSecond / 3600);
			if (h < 10) {
				h = "0" + h;
			}
			// 计算剩余的分钟
			SurplusSecond = SurplusSecond - (h * 3600);
			var m = Math.floor(SurplusSecond / 60);
			if (m < 10) {
				m = "0" + m;
			}
			var s = SurplusSecond % 60;
			if (s < 10) {
				s = "0" + s;
			}

			h = h.toString();
			m = m.toString();
			s = s.toString();
			var hArray = h.split("");
			var mArray = m.split("");
			var sArray = s.split("");

			diffTimeStr += "<h2>" + hArray[0] + hArray[1] + ":";
			diffTimeStr += +mArray[0] + mArray[1] + ":";
			diffTimeStr += sArray[0] + sArray[1] + "</h2><p>开始倒计时</p>";

			$("#diffTime").html(diffTimeStr);
		} else {
			var m = Math.floor(SurplusSecond / 60);
			if (m < 10) {
				m = "0" + m;
			}
			var s = SurplusSecond % 60;
			if (s < 10) {
				s = "0" + s;
			}

			m = m.toString();
			s = s.toString();
			var mArray = m.split("");
			var sArray = s.split("");

			diffTimeStr += "<h2>" + mArray[0] + mArray[1] + ":";
			diffTimeStr += sArray[0] + sArray[1] + "</h2><p>开始倒计时</p>";

			$("#diffTime").html(diffTimeStr);
		}
	}
	indexPage.param.diffTime--;
	if (indexPage.param.diffTime < 0) {
		window.clearInterval(indexPage.param.intervalKey); // 终止周期性倒计时
		indexPage.getActiveExpect(); // 倒计时结束重新请求最新期号
		// 获取最新的开奖号码
		indexPage.getLastLotteryCode();
	}
	;
};

/**
 * 控制是否能发起期号的请求
 */
Indexpage.prototype.controlIsCanActive = function() {
	indexPage.param.isCanActiveExpect = true;
};

/**
 * 返回主页
 */
Indexpage.prototype.backToIndex = function() {
	window.location.href = contextPath + "/gameBet/index.html";
};

/**
 * 显示默认首页彩种
 */
Indexpage.prototype.ShowIndexLotteryKind = function() {
	// indexPage.param.kindName='GDSYXW';

	$.ajax({
		type : "POST",
		url : contextPath + "/code/getBizSystemInfo",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var resultData = result.data;
				if (resultData.indexLotteryKind != null && resultData.indexLotteryKind != "") {
					indexPage.param.kindName = resultData.indexLotteryKind;
					$("#timelyImg").attr('src', contextPath + '/front/images/lottery/logo/icon-' + indexPage.Difference(indexPage.param.kindName) + '.png');
					$("#timely_address").text(resultData.indexLotteryKindName);
					customServiceUrl = resultData.customUrl;
				}
				;

				// 获取最新的开奖号码
				if (indexPage.param.kindName == 'XYLHC' || indexPage.param.kindName == 'LHC') {
					indexPage.initShengXiaoNumber();
				} else {
					indexPage.getLastLotteryCode();
				}

				// 获取当前彩种的最新期号
				indexPage.getActiveExpect();
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 设置热门彩种图标
 */
Indexpage.prototype.Difference = function(lotteryName) {
	if (lotteryName.indexOf("SSC") > -1) {
		return "shishicai";
	} else if (lotteryName.indexOf("SYXW") > -1) {
		return "11xuan5";
	} else if (lotteryName.indexOf("SDDPC") > -1) {
		return "3d";
	} else if (lotteryName.indexOf("XYLHC") > -1) {
		return "xyliuhecai";
	} else if (lotteryName.indexOf("LHC") > -1) {
		return "liuhecai";
	} else if (lotteryName.indexOf("KS") > -1) {
		return "kuai3";
	} else if (lotteryName.indexOf("PK10") > -1 || lotteryName=="XYFT") {
		return "pk10";
	} else if (lotteryName.indexOf("FFC") > -1) {
		return "fenfencai";
	} else if (lotteryName.indexOf("WFC") > -1) {
		return "wufencai";
	} else if (lotteryName.indexOf("HGFFC") > -1) {
		return "1.5fencai";
	} else if (lotteryName.indexOf("XJPLFC") > -1) {
		return "erfencai";
	} else if (lotteryName.indexOf("XYEB") > -1) {
		return "xy28";
	}

}

/**
 * 展示公告
 */
Indexpage.prototype.getNewestAnnounce = function() {
	
	$.ajax({
		type : "POST",
		url : contextPath + "/announces/getNewestAnnounce",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if( result.code == "ok"){
				var userAnnounces = result.data;
				if (userAnnounces != null) {
					frontCommonPage.showKindlyReminder("" + userAnnounces.title + "", "" + userAnnounces.content + "", userAnnounces.id, true, false);
				}
			}else if(result.code == "error"){
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 展示公告列表
 */
Indexpage.prototype.getAnnounceList = function() {
	var jsonData = {
		"pageNo" : 1,
	};

	$.ajax({
		type : "POST",
		url : contextPath + "/announces/announceQuery",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var userAnnounceListObj = $("#noticeList");
				userAnnounceListObj.html("");
				var str = "";
				var userAnnounces = result.data.pageContent;

				if (userAnnounces.length == 0) {
					str += "<li>";
					str += "  <span class='time'></span>";
					str += "暂无相关信息";
					str += "</li>";
					userAnnounceListObj.append(str);
				} else {
					for (var i = 0; i < userAnnounces.length; i++) {
						var userAnnounce = userAnnounces[i];
						str += '<a href="javascript:void(0)"; onclick="indexPage.showAnnounceDetail(' + userAnnounce.id + ')"><em></em>' + userAnnounce.title + '</a>';
						userAnnounceListObj.append(str);
						str = "";
					}
				}
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 显示新手大礼包 根据Ajax请求返回的值,来确定是否弹框,true是弹出新手礼包.false是不弹出;
 */
Indexpage.prototype.showNewUserGift = function() {

	$.ajax({
		type : "POST",
		url : contextPath + "/activity/isShowNewUserGift",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				if (result.data == true && result.data4 != false) {
					$(".pop").show();
				} else {
					$(".pop").hide();
				}
				// 设置右侧栏中的新手礼包显示功能
				if (result.data2 == true && result.data4 != false) {
					$(".pop-btn").show();
				} else {
					$(".pop-btn").hide();
				}
				// 设置新手礼包弹框时候的总价格为(r[2])
				if (result.data3 != null) {
					$("#newUserGiftTaskAlert").text(result.data3 + "元新手大礼包!")
				} else {
					$("#newUserGiftTaskAlert").text("领取新手大礼包!")
				}
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

// 六合彩参数
indexPage.shengxiao = {
	shengxiaoKey : new Array('鼠', '牛', '虎', '兔', '龙', '蛇', '马', '羊', '猴', '鸡', '狗', '猪'),
	shengxiaoArr : []
}
// 波色
indexPage.boshe = {
	hongbo : [ 1, 2, 7, 8, 12, 13, 18, 19, 23, 24, 29, 30, 34, 35, 40, 45, 46 ],
	lanbo : [ 3, 4, 9, 10, 14, 15, 20, 25, 26, 31, 36, 37, 41, 42, 47, 48 ],
	lvbo : [ 5, 6, 11, 16, 17, 21, 22, 27, 28, 32, 33, 38, 39, 43, 44, 49 ]
}

Indexpage.prototype.returnAninal = function(opencode) {
	var shengxiaoArr = indexPage.shengxiao.shengxiaoArr;
	for (var i = 0; i < shengxiaoArr.length; i++) {
		for (var j = 0; j < shengxiaoArr[i].length; j++) {
			if (Number(opencode) == Number(shengxiaoArr[i][j])) {
				return indexPage.shengxiao.shengxiaoKey[i];
			}
		}
	}
	return "-";
}

/**
 * 初始化生肖号码描述
 */
Indexpage.prototype.initShengXiaoNumber = function() {

	$.ajax({
		type : "POST",
		url : contextPath + "/code/getShengXiaoNumber",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				indexPage.shengxiao.shengxiaoArr = result.data;
				// 获取最新的开奖号码
				indexPage.getLastLotteryCode();
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
}

// 计算颜色
Indexpage.prototype.changeColorByCode = function(lotteryCode) {
	var colors = new Array();
	var hong = indexPage.boshe.hongbo;
	var lan = indexPage.boshe.lanbo;
	var lv = indexPage.boshe.lvbo;

	// 判断是否是红波
	for (var i = 0; i < hong.length; i++) {
		if (Number(hong[i]) == Number(lotteryCode.numInfo1)) {
			colors[0] = 'btn btn-red';
		}
		if (Number(hong[i]) == Number(lotteryCode.numInfo2)) {
			colors[1] = 'btn btn-red';
		}
		if (Number(hong[i]) == Number(lotteryCode.numInfo3)) {
			colors[2] = 'btn btn-red';
		}
		if (Number(hong[i]) == Number(lotteryCode.numInfo4)) {
			colors[3] = 'btn btn-red';
		}
		if (Number(hong[i]) == Number(lotteryCode.numInfo5)) {
			colors[4] = 'btn btn-red';
		}
		if (Number(hong[i]) == Number(lotteryCode.numInfo6)) {
			colors[5] = 'btn btn-red';
		}
		if (Number(hong[i]) == Number(lotteryCode.numInfo7)) {
			colors[6] = 'btn btn-red';
		}
	}

	// 判断是否是蓝波
	for (var i = 0; i < lan.length; i++) {
		if (Number(lan[i]) == Number(lotteryCode.numInfo1)) {
			colors[0] = 'btn btn-blue';
		}
		if (Number(lan[i]) == Number(lotteryCode.numInfo2)) {
			colors[1] = 'btn btn-blue';
		}
		if (Number(lan[i]) == Number(lotteryCode.numInfo3)) {
			colors[2] = 'btn btn-blue';
		}
		if (Number(lan[i]) == Number(lotteryCode.numInfo4)) {
			colors[3] = 'btn btn-blue';
		}
		if (Number(lan[i]) == Number(lotteryCode.numInfo5)) {
			colors[4] = 'btn btn-blue';
		}
		if (Number(lan[i]) == Number(lotteryCode.numInfo6)) {
			colors[5] = 'btn btn-blue';
		}
		if (Number(lan[i]) == Number(lotteryCode.numInfo7)) {
			colors[6] = 'btn btn-blue';
		}
	}

	// 判断是否是绿波
	for (var i = 0; i < lv.length; i++) {
		if (Number(lv[i]) == Number(lotteryCode.numInfo1)) {
			colors[0] = 'btn btn-green';
		}
		if (Number(lv[i]) == Number(lotteryCode.numInfo2)) {
			colors[1] = 'btn btn-green';
		}
		if (Number(lv[i]) == Number(lotteryCode.numInfo3)) {
			colors[2] = 'btn btn-green';
		}
		if (Number(lv[i]) == Number(lotteryCode.numInfo4)) {
			colors[3] = 'btn btn-green';
		}
		if (Number(lv[i]) == Number(lotteryCode.numInfo5)) {
			colors[4] = 'btn btn-green';
		}
		if (Number(lv[i]) == Number(lotteryCode.numInfo6)) {
			colors[5] = 'btn btn-green';
		}
		if (Number(lv[i]) == Number(lotteryCode.numInfo7)) {
			colors[6] = 'btn btn-green';
		}
	}

	return colors;
}
Indexpage.prototype.changeColorByNumber = function(i) {
	var colors = "";
	// 红波
	if (i == 3 || i == 6 || i == 9 || i == 12 || i == 15 || i == 18 || i == 21 || i == 24) {
		colors = "btn btn-red";
		// 绿波
	} else if (i == 1 || i == 4 || i == 7 || i == 10 || i == 16 || i == 19 || i == 22 || i == 25) {
		colors = "btn btn-green";
		// 蓝波
	} else if (i == 2 || i == 5 || i == 8 || i == 11 || i == 17 || i == 20 || i == 23 || i == 26) {
		colors = "btn btn-blue";
		// 灰色
	} else if (i == 0 || i == 13 || i == 14 || i == 27) {
		colors = "btn btn-gray";
	}
	return colors;
}

// 开启关闭按钮
// showTip("下午好,感谢您的到来~","Good afternoon and welcome",true); //开启关闭按钮
// showTip("下午好,感谢您的到来~","Good afternoon and welcome",false); //关闭关闭按钮
// showTip("下午好,感谢您的到来~","Good afternoon and welcome",false,5000); //延时5秒自动关闭
// showTip("下午好,感谢您的到来~","Good afternoon and welcome",true,false); //不关闭
