function BindPhonePage() {

}

var bindPhonePage = new BindPhonePage();

// 初始化页面.
$(document).ready(function() {
	$('#send-verifycode').on('click', function() {
		bindPhonePage.sendTextMessages();
	})
});

/**
 * 发送验证码.
 */
BindPhonePage.prototype.sendTextMessages = function() {
	// 获取用户输入的手机号!并且校验是否为空.
	var mobile = $("#mobile").val();
	var type = "BIND_PHONE_TYPE";
	if (mobile == null || mobile == "") {
		frontCommonPage.showKindlyReminder("手机号不能空");
		return;
	}

	// 封装参数.
	var jsonData = {
		"phone" : mobile,
		"type" : type,
	};

	// 发送验证码.
	$.ajax({
		type : "POST",
		url : contextPath + "/usersafe/sendPhoneCode",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				frontCommonPage.showKindlyReminder("短信已发送，请注意查收");
				$("#send-verifycode").hide();
				$('.mobile-verify-time').css('display', 'inline-block');

				// 手机验证码倒计时60秒
				var i = 60;
				var interval = setInterval(function() {
					i = i - 1;
					$('.mobile-verify-time .countdown').text(i);
					if (i == 0) {
						clearInterval(interval);
						$('.mobile-verify-time').hide();
						$('#send-verifycode').show();
					}
				}, 1000)
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
				return;
			}
		}
	});
}

/**
 * 绑定新手机(校验手机号是否被使用过,校验安全密码是否正确,校验手机和验证码是否匹配);
 */
BindPhonePage.prototype.bindPhone = function() {

	// 获取参数并且校验是否为空.
	var mobile = $("#mobile").val();
	if (mobile == null || mobile == "") {
		frontCommonPage.showKindlyReminder("手机号不能为空.");
		return;
	}

	var vcode = $("#mobile_verifyCode").val();
	if (vcode == null || vcode == "") {
		frontCommonPage.showKindlyReminder("验证码不能为空.");
		return;
	}

	var safepasw = $("#safepasw").val();
	if (safepasw == null || safepasw == "") {
		frontCommonPage.showKindlyReminder("安全密码不能为空.");
		return;
	}

	// 封装参数
	var jsonData = {
		"vcode" : vcode,
		"phone" : mobile,
		"safePassword" : safepasw,
	};

	// 绑定新手机.
	$.ajax({
		type : "POST",
		url : contextPath + "/usersafe/bindPhone",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				frontCommonPage.showKindlyReminder("绑定成功，自动跳转到账号总览页面...");
				setTimeout(function() {
					window.location.href = contextPath + "/gameBet/accountCenter.html";
				}, 1500);
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
}
