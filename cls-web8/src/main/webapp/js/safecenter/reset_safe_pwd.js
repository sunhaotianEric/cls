function SafePasswordModifyPage() {
}
var safePasswordModifyPage = new SafePasswordModifyPage();

safePasswordModifyPage.param = {

};

$(document).ready(function() {

	$("#oldPassword").focus(function() {
		$("#passTip3").css({
			display : "none"
		});
	});
	$("#oldPassword").blur(function() {
		var userPasswordOld = $("#oldPassword").val();
		if (userPasswordOld == null || userPasswordOld == "" || userPasswordOld.length < 6 || userPasswordOld.length > 15) {
			$("#passTip3").css({
				display : "inline"
			});
			$("#passTip3").html("<i></i>当前密码格式不正确.");
			$("#passTip3").css({
				color : "red"
			});
		}
	});
	$("#newPassword").focus(function() {
		$("#passTip").html("<i></i>6-15个字符，建议使用字母、数字组合，混合大小写");
		$("#passTip").removeClass("red");
		$("#passTip").css({
			display : "inline"
		});
	});

	$("#newPassword").blur(function() {
		var userPassword = $("#newPassword").val();
		if (userPassword == null || userPassword == "" || userPassword.length < 6 || userPassword.length > 15) {
			$("#passTip").css({
				display : "inline"
			});
			$("#passTip").addClass("red");
			$("#passTip").html("<i></i>密码填写不合法.");
		}
	});

	$("#confirmNewPassword").focus(function() {
		$("#passTip2").removeClass("red");
		$("#passTip2").css({
			display : "none"
		});
	});

	$("#confirmNewPassword").blur(function() {
		var userPassword = $("#newPassword").val();
		var userPassword2 = $("#confirmNewPassword").val();
		if (userPassword2 == null || userPassword2 == "" || userPassword2.length < 6 || userPassword2.length > 15) {
			$("#passTip2").css({
				display : "inline"
			});
			$("#passTip2").addClass("red");
			$("#passTip2").html("<i></i>确认密码不能为空.");
		} else if (userPassword != userPassword2) {
			$("#passTip").css({
				display : "inline"
			});
			$("#passTip2").css({
				display : "inline"
			});
			$("#passTip").html("<i></i>两次密码输入不一致.");
			$("#passTip").addClass("red");
			$("#passTip2").html("<i></i>两次密码输入不一致.");
			$("#passTip2").addClass("red");
		}
	});

	safePasswordModifyPage.trunToUpdateSavePassword();

	$("#updateLoginPass").click(function() {
		safePasswordModifyPage.trunToUpdateLoginPassword();
	});

	$("#updateSavePass").click(function() {
		safePasswordModifyPage.trunToUpdateSavePassword();
	});

	$("#changePasswordBtn").click(function() {
		safePasswordModifyPage.saveInfoPassword();
	});

	$("#changeSavePasswordBtn").click(function() {
		safePasswordModifyPage.saveInfoSavePassword();
	});
});

/**
 * 公共样式处理
 */
SafePasswordModifyPage.prototype.commonCss = function() {
	$("#passTip2").css({
		display : "none"
	});
	$("#passTip3").css({
		display : "none"
	});
	$("#errmsg").css({
		display : "none"
	});
	$("#oldPassword").val("");
	$("#newPassword").val("");
	$("#confirmNewPassword").val("");
};

/**
 * 转到修改登录密码
 */
SafePasswordModifyPage.prototype.trunToUpdateLoginPassword = function() {
	$("#updateSavePass").removeClass("on");
	$("#updateLoginPass").addClass("on");
	$("#setNewInfo").hide();
	$("#updateInfo").css({
		display : "block"
	});
	safePasswordModifyPage.commonCss();
	$("#updateSavePass").removeClass("current");
	$("#updateLoginPass").addClass("current");
	$("#pssmsg1").text("当前登录密码：");
	$("#pssmsg2").text("新登录密码：");
	$("#changeSavePasswordBtn").css({
		display : "none"
	});
	$("#changePasswordBtn").css({
		display : "inline-block"
	});
};

/**
 * 转到修改安全密码
 */
SafePasswordModifyPage.prototype.trunToUpdateSavePassword = function() {
	$("#updateSavePass").addClass("on");
	$("#updateLoginPass").removeClass("on");
	$("#updateLoginPass").removeClass("current");
	$("#updateSavePass").addClass("current");

	$.ajax({
		type : "POST",
		url : contextPath + "/usersafe/checkIsSetSafePassword",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				$("#updateInfo").hide();
				$("#setNewInfo").css({
					display : "block"
				});
			} else if (result.code == "error") {
				safePasswordModifyPage.commonCss();
				$("#pssmsg1").html("当前安全密码：");
				$("#pssmsg2").html("<i></i>新安全密码：");
				$("#changePasswordBtn").css({
					display : "none"
				});
				$("#changeSavePasswordBtn").css({
					display : "inline-block"
				});
				$("#changeSavePasswordBtn").removeAttr("disabled");
			}
		}
	});
};

/**
 * 绑定事件
 */
SafePasswordModifyPage.prototype.bindClick = function() {
	$("#changePasswordBtn").unbind("click").click(function() {
		safePasswordModifyPage.saveInfoPassword();
	});
};

/**
 * 绑定事件
 */
SafePasswordModifyPage.prototype.bindSaveClick = function() {
	$("#changeSavePasswordBtn").unbind("click").click(function() {
		safePasswordModifyPage.saveInfoSavePassword();
	});
};

/**
 * 保存安全密码数据
 */
SafePasswordModifyPage.prototype.saveInfoSavePassword = function() {

	var userPasswordOld = $("#oldPassword").val();
	var userPassword = $("#newPassword").val();
	var userPassword2 = $("#confirmNewPassword").val();

	if (userPasswordOld == null || userPasswordOld == "") {
		$("#passTip3").css({
			display : "inline"
		});
		$("#passTip3").text("当前密码不能为空.");
		$("#passTip3").css({
			color : "red"
		});
		safePasswordModifyPage.bindSaveClick();
		return;
	}

	if (userPassword == null || userPassword == "" || userPassword.length < 6 || userPassword.length > 15) {
		$("#passTip").css({
			display : "inline"
		});
		$("#passTip").addClass("red");
		$("#passTip").html("<i></i>密码填写不合法.");
		safePasswordModifyPage.bindSaveClick();
		return;
	}

	if (userPassword2 == null || userPassword2 == "") {
		$("#passTip2").css({
			display : "inline"
		});
		$("#passTip2").html("<i></i>确认密码不能为空.");
		$("#passTip2").addClass("red");
		safePasswordModifyPage.bindSaveClick();
		return;
	}

	if (userPassword != userPassword2) {
		$("#passTip").css({
			display : "inline"
		});
		$("#passTip2").css({
			display : "inline"
		});
		$("#passTip").html("<i></i>两次密码输入不一致.");
		$("#passTip").addClass("red");
		$("#passTip2").html("<i></i>两次密码输入不一致.");
		$("#passTip2").addClass("red");
		safePasswordModifyPage.bindSaveClick();
		return;
	}

	$("#changeSavePasswordBtn").unbind("click");
	$("#changeSavePasswordBtn").text("提交中.....");

	// 16位随机数
	function Number() {
      let rnd = ""
      for (let i = 0; i < 16; i++) {
        rnd += Math.floor(Math.random() * 10)
      }
      return rnd
    }
	
	var time = Number();
	var base = new Base64();
	var Base64key = base.encode(time);
	
	var oldSafePasswordRandom = encrypt(userPasswordOld,time)
	var safePasswordRandom = encrypt(userPassword,time)
	var sureSafePasswordRandom = encrypt(userPassword2,time)
	
	/**
	 * 加密（需要先加载lib/aes/aes.min.js文件）
	 * @param word
	 * @returns {*}
	 */
	function encrypt(word,t){
	    var key = CryptoJS.enc.Utf8.parse(t);
	    var srcs = CryptoJS.enc.Utf8.parse(word);
	    var encrypted = CryptoJS.AES.encrypt(srcs, key, {mode:CryptoJS.mode.ECB,padding: CryptoJS.pad.Pkcs7});
	    return encrypted.toString();
	}
	
	// 封装参数.
	var user = {
		"random" : Base64key,	
		"oldSafePasswordRandom" : oldSafePasswordRandom,
		"safePasswordRandom" : safePasswordRandom,
		"sureSafePasswordRandom" : sureSafePasswordRandom,
	};
	var jsonData = {
		"user" : user,
	};
	// 修改安全密码.
	$.ajax({
		type : "POST",
		url : contextPath + "/usersafe/resetSafePwd",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				frontCommonPage.showKindlyReminder("修改成功...3秒后自动跳转到安全中心...");
				$("#errmsg").hide();
				$("#changeSavePasswordBtn").text("确定");
				setTimeout("window.location.href= contextPath + '/gameBet/accountCenter.html'", 3000);
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
				safePasswordModifyPage.bindSaveClick();
				$("#changeSavePasswordBtn").text("确定");
			}
		}
	});
};

/**
 * 跳转到设置页面
 */
SafePasswordModifyPage.prototype.trunToSetPage = function() {
	window.location.href = contextPath + "/gameBet/safeCenter/setSafePwd.html";
};