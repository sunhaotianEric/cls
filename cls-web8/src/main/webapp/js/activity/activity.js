function Activitypage() {
}
var activitypage = new Activitypage();


$(document).ready(function() {
	// 顶部导航选中处理
	$("#navChoose .nav-child").removeClass("on");
	$("#navChoose .nav-child:eq(5)").addClass("on");

	// 控制菜单栏底色
	$(".index").removeClass("active");
	$("#activityMenu").addClass("active");

	activitypage.getAllActivitys();
	base.animate_add(".main .main-child", "fadeInUp");

	// 顶部导航链接选中样式
	$('.header .foot .nav .child5').addClass('on');

});

/**
 * 加载所有的活动中心
 */
Activitypage.prototype.getAllActivitys = function() {

	$.ajax({
		type : "POST",
		url : contextPath + "/activity/getAllActivitys",
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				activitypage.refreshActivityPages(result.data);
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 刷新列表数据
 */
Activitypage.prototype.refreshActivityPages = function(activitys) {

	var activityListObj = $("#activityList");
	activityListObj.html("");

	var str = "";
	var count = 0;
	var signinDateList = new Array();
	for (var i = 0; i < activitys.length; i++) {

		var activity = activitys[i];

		var activit = activity.des.split("|");

		var longTermBoolean = false;

		str += '	<div class="main-child">';
		str += '		<div class="head">';
		str += '			<div class="img" style="background-image:url(' + activity.imagePath + ');"></div>';
		str += '			<div class="infos">';
		str += '				<h2 class="title">' + activity.title + '</h2>';
		str += '				<p class="time">发布时间：' + activity.startDateStr + '</p>';
		var currentDate = new Date().getTime();
		var startDate = new Date(activity.startDate).getTime();
		var endDate = new Date(activity.endDate).getTime();

		if (parseInt(endDate - startDate) / 1000 / 60 / 60 / 24 > 365) {
			str += "     <div class='status red'>长期有效</div>";
			str += '				<p class="ac-time">活动日期：' + activity.startDateStr + ' 至 ' + activity.endDateStr + '</p>'
			str += '				<div class="btn red-btn">活动进行中</div>';
			longTermBoolean = true;
		}
		if (currentDate < endDate && ! longTermBoolean) {
			str += '				<p class="ac-time">活动日期：' + activity.startDateStr + ' 至 ' + activity.endDateStr + '</p>'
			str += '				<div class="btn red-btn">活动进行中</div>';
		}
		if (currentDate > endDate && ! longTermBoolean) {
			str += '	<div class="btn gray-btn">已结束</div>';
		}
		str += '			</div>';
		str += '			<div class="pointer"><img src="' + contextPath + '/front/images/activity/pointer.png" alt=""></div>';
		str += '		</div>';
		str += '		<div class="content">';
		str += '			<h2 class="font-red">' + activity.title + '</h2>';
		str += "   <p> 活动介绍：<br/>" + activity.des + "</p>";
		/* str += " <div class='flags' >"; */
		if (activity.type == "DAY_CONSUME_ACTIVITY") {
			str += "	<div class='flags' id='activityDay'>";
		} else {
			str += "	<div class='flags' >";
		}
		if (activity.type == "DAY_CONSUME_ACTIVITY") {
			var dayConsumeActityConfigs = activity.dayConsumeActityConfigs;

			for (var j = 0; j < dayConsumeActityConfigs.length; j++) {
				var dayConsumeActityConfig = dayConsumeActityConfigs[j];
				var receiveStatus = dayConsumeActityConfig.receiveStatus;
				var receiveState = null;
				if (receiveStatus == "NOT_APPLY") {
					receiveState = "立即领取";
				} else if (receiveStatus == "APPLIED") {
					receiveState = "已申请";
				} else if (receiveStatus == "RELEASED") {
					receiveState = "已发放"
				} else if (receiveStatus == "NOTALLOWED_APPLY") {
					receiveState = "未达到";
				}

				str += "	<div class='flag' >";
				str += "		<div class='flag-head'><p>每日活动</p></div>";
				str += " 		<div class='flag-content'>";
				str += "			<p>会员当天投注量达到<span>" + dayConsumeActityConfig.lotteryNum + "</span></p>";
				str += "  			<p>可领取 <span>" + dayConsumeActityConfig.money + "</span> 元</p>";
				str += " 		 	<div class='btn no'>";
				if (receiveStatus == "NOT_APPLY") {
					str += "              <div class='title activity ' data-value='" + dayConsumeActityConfig.money + "' data-type='" + dayConsumeActityConfig.activityType + "'>"
							+ receiveState + "</div>";
					str += "             <div class='bg'></div></div>";
				}else if(receiveStatus == "APPLIED"){
					str += 				"<div class='title activity'>" + receiveState + "</div>";
					str += 				"<div class='bg' style='background: url(&quot;../../front/images/activity/btn.png&quot;) center top no-repeat;'></div></div>";
				}else if (receiveStatus == "RELEASED") {
					str += 				"<div class='title activity'>" + receiveState + "</div>";
					str += 				"<div class='bg' style='background: url(&quot;../../front/images/activity/btn3.png&quot;) center top no-repeat;'></div></div>";
				}
				else {
					str += "              <div class='title activity'>" + receiveState + "</div>";
					str += "             <div class='bg'></div></div>";
				}
				
				str += "  		</div>";
				str += " 	</div>";
			}
			var dayConsumeActityConfigsFfcs = activity.dayConsumeActityFfcConfigs;
			for (var j = 0; j < dayConsumeActityConfigsFfcs.length; j++) {
				var dayConsumeActityConfigsFfc = dayConsumeActityConfigsFfcs[j];
				var receiveStatus = dayConsumeActityConfigsFfc.receiveStatus;
				var ffcReceiveState = null;
				if (receiveStatus == "NOT_APPLY") {
					ffcReceiveState = "立即领取";
				} else if (receiveStatus == "APPLIED") {
					ffcReceiveState = "已申请";
				} else if (receiveStatus == "RELEASED") {
					ffcReceiveState = "已发放"
				} else if (receiveStatus == "NOTALLOWED_APPLY") {
					ffcReceiveState = "未达到";
				}
				str += "	<div class='flag' >";
				str += "		<div class='flag-head'><p>分分彩活动</p></div>";
				str += " 		<div class='flag-content'>";
				str += "			<p>会员当天投注量达到<span>" + dayConsumeActityConfigsFfc.lotteryNum + "</span></p>";
				str += "  			<p>可领取 <span>" + dayConsumeActityConfigsFfc.money + "</span> 元</p>";
				str += " 		 	<div class='btn no'>";
				if (receiveStatus == "NOT_APPLY") {
					str += "<div class='title activity' data-value='" + dayConsumeActityConfigsFfc.money + "' data-type='" + dayConsumeActityConfigsFfc.activityType + "'>" + ffcReceiveState + "</div>";
					str += "<div class='bg'></div></div>";
				} else if (receiveStatus == "APPLIED"){
					str += 				"<div class='title activity'>" + ffcReceiveState + "</div>";
					str += 				"<div class='bg' style='background: url(&quot;../../front/images/activity/btn.png&quot;) center top no-repeat;'></div></div>";
				} else if (receiveStatus == "RELEASED"){
					str += 				"<div class='title activity'>" + ffcReceiveState + "</div>";
					str += 				"<div class='bg' style='background: url(&quot;../../front/images/activity/btn3.png&quot;) center top no-repeat;'></div></div>";
				}
				else {
					str += 				"<div class='title activity'>" + ffcReceiveState + "</div>";
					str += 			"<div class='bg'></div></div>";
				}
				
				str += "  		</div>";
				str += " 	</div>";
			}
		}
		if (activity.type == "SIGNIN_ACTIVITY") {
			// 活动内容
			str += '		</div>';
			str += '<div class="activity-cont"> ';
			str += '	<p><span class="bigtitle">活动内容</span></p>';
			str += "	<div class='flags' >";
			var signinConfigs = activity.signinConfigs;
			for (var j = 0; j < signinConfigs.length; j++) {
				var signinConfig = signinConfigs[j];
				str += "	<div class='flag' >";
				str += "		<div class='flag-head'><p>第" + (j + 1) + "天</p></div>";
				str += " 		<div class='flag-content'>";
				if (signinConfig.signState == "APPLIED") {
					str += "			<p class='por'><img src='" + contextPath + "/front/images/activity/signed.png' alt=''><span class='money'>" + signinConfig.money + "元</span></p>";// 签到是signed.png
					str += " 		 	<div class='btn no'><div class='title signEvent signed' data-value='" + signinConfig.signinDay + "'>已签到</div></div>";// signin点击签到
					count++;
					signinDateList.push(signinConfig.signinDate);
				} else {
					str += "			<p class='por'><img src='" + contextPath + "/front/images/activity/sign.png' alt=''><span class='money'>" + signinConfig.money + "元</span></p>";// 签到是signed.png
					str += " 		 	<div class='btn no'><div class='title signEvent sign' data-value='" + signinConfig.signinDay + "'>未签到</div></div>";// signin点击签到
				}
				str += "  		</div>";
				str += " 	</div>";
			}
			str += '		</div>';
			str += '</div>';
		}
		str += '		</div>';
		str += '		</div>';
		str += '	</div>';

		activityListObj.append(str);

		str = "";
		// 每日活动自动显示出来
		if (activity.type == "DAY_CONSUME_ACTIVITY") {
			$(".child-content").show();
		}
	}
	activitypage.initApplyBtn();
	// 给查看活动详情绑定单击事件
	activitypage.addEnevt();
	if (!$.isEmptyObject(currentUser)) {
		// 签到初始化
		activitypage.initSigninBtn(count, signinDateList);
	}
};

// 初始化申请领取按钮
Activitypage.prototype.initApplyBtn = function() {
	$(".activity").each(function(i) {
		var money = $(this).attr("data-value");
		money = parseInt(money);
		var type = $(this).attr("data-type");
		if (type == "NORMAL") {
			$(this).click(function() {
				$(this).unbind("click");
				// 进行每日活动赠送金申请,领取
				activitypage.applyDayActivityJobByType("DAY_ACTIVITY", "NORMAL" ,$(this));
			});
		} else if (type == "FFC") {
			$(this).click(function() {
				$(this).unbind("click");
				// 进行每日活动赠送金申请,领取
				activitypage.applyDayActivityJobByType("DAY_ACTIVITY", "FFC" ,$(this));
			});
		}
	});

}

/**
 * 进行每日活动赠送金申请,领取
 */
Activitypage.prototype.applyDayActivityJobByType = function(treatmentByType, type , thisBtn) {
	var jsonData = {
		"type" : type,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/activity/dayConsumeActityApply",
		data : JSON.stringify(jsonData),// 将对象序列化成JSON字符串;
		dataType : "json",// 设置请求参数类型
		contentType : 'application/json;charset=utf-8',// 设置请求头信息;
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				frontCommonPage.showKindlyReminder(result.data);
				thisBtn.html("已申请");
				thisBtn.parent().find('.bg').css('background','url(../../front/images/activity/btn.png) no-repeat center top');
				// 刷新页面#FFCA13
				//activitypage.refreshActivityStatus();
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};
// 初始化签到领取按钮
Activitypage.prototype.initSigninBtn = function(count, signinDateList) {
	var mathDay = new Date(Math.max.apply(null, signinDateList)).format("yyyy-MM-dd");
	var now = new Date().format("yyyy-MM-dd");

	$(".signEvent").each(function(i) {
		var signinDay = $(this).attr("data-value");
		signinDay = parseInt(signinDay);

		var signState = $(this).html();
		$(this).unbind("click");
		if ((count == 0 || count == 7) && Date.parse(mathDay) != Date.parse(now)) {
			if (i == 0) {
				$(this).html("点击签到");
				$(this).addClass("signin");
				$(this).removeClass("signed");
				$(this).unbind("click").click(function() {
					// 进行签到活动赠送金申请,领取
					activitypage.applySigninReceive(signinDay, $(this));
				});
			} else {
				$(this).html("未签到");
				$(this).removeClass("sign");
				$(this).removeClass("signed");
				$(this).parent().prev().find("img").attr("src", contextPath + "/front/images/activity/sign.png");
			}
		} else if ((count + 1) == signinDay && signState == "未签到" && Date.parse(mathDay) != Date.parse(now)) {
			$(this).html("可点击签到");
			$(this).addClass("signin");
			$(this).unbind("click").click(function() {
				// 进行签到活动赠送金申请,领取
				activitypage.applySigninReceive(signinDay, $(this));
			});
		}
	});
}

/**
 * 进行签到活动赠送金申请,领取
 */
Activitypage.prototype.applySigninReceive = function(signinDay, thisBtn) {
	var jsonData = {
		"continueDay" : signinDay,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/activity/signinApply",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if( result.code == "ok"){
				frontCommonPage.showKindlyReminder(result.data);
				thisBtn.unbind("click");
				thisBtn.html("已签到");
				thisBtn.removeClass("signin");
				thisBtn.addClass("signed");
				thisBtn.parent().parent().find('.por img').attr('src', contextPath + '/front/images/activity/signed.png');
			}else if(result.code == "error"){
				frontCommonPage.showKindlyReminder(result.data)
				thisBtn.unbind("click");
				thisBtn.click(function() {
					// 进行签到活动赠送金申请,领取
					activitypage.applySigninReceive(signinDay, thisBtn);
				});
			}
		}
	});
};

/**
 * 给查看活动详情绑定单击事件
 */
Activitypage.prototype.addEnevt = function() {

	$(".main .main-child .pointer").click(function() {
		$(this).toggleClass("on");
		var parent = $(this).closest(".main-child");
		parent.find(".content").stop(false, true).slideToggle(500);
	});

}

/**
 * 图片轮播,点击事件
 */
Activitypage.prototype.carouselImage = function() {
	var len = $("#indexSliderBox ul li").length; // 获取焦点图个数
	var index = 0;
	var picTimer;

	var btn = "<div class='activity-slider-dot'>";
	for (var i = 0; i < len; i++) {
		btn += "<a href='javascript:void(0);'></a>";
	}
	btn += "</div>";

	$("#indexSliderBox").append(btn);

	$("#indexSliderBox .activity-slider-dot a").mouseover(function() {
		index = $("#indexSliderBox .activity-slider-dot a").index(this);
		showPics(index);
	}).eq(0).trigger("mouseover");

	function showPics(index) { // 普通切换
		$("#indexSliderBox ul li").eq(index).stop(true, false).fadeIn(500).siblings("li").fadeOut(500);
		$("#indexSliderBox .activity-slider-dot a").removeClass("on").eq(index).addClass("on");
	}

	$("#indexSliderBox").hover(function() {
		if (picTimer) {
			clearInterval(picTimer);
		}
	}, function() {
		picTimer = setInterval(function() {
			showPics(index);
			index++;
			if (index == $("#indexSliderBox ul li").length) {
				index = 0;
			}
		}, 3000);
	});
	// 自动开始
	var picTimer = setInterval(function() {
		showPics(index);
		index++;
		if (index == $("#indexSliderBox ul li").length) {
			index = 0;
		}
	}, 3000);
};