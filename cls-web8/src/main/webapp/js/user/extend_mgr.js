/*
 * 推广管理
 */
function ExtendMgrPage() {
}
var extendMgrPage = new ExtendMgrPage();

extendMgrPage.param = {
	linkList : null,
	deleteLinkId : null
// 当前用户的注册列表
};

extendMgrPage.sscParam = {
	    sscInterval : null,
	    sscOpenUserHighest : null,
	    sscOpenUserLowest : null,
	    sscOpenUserHighestRebateValue : null,
	    sscOpenUserLowestRebateValue : null
};

/**
 * 查询参数
 */
extendMgrPage.queryParam = {
	userId : null
};

//分页参数
extendMgrPage.pageParam = {
        queryMethodParam : null,
        pageNo : 1,
        pageMaxNo : 1,  //最大的页数
        skipHtmlStr : ""
};

$(document).ready(function() {

	$("div.lists").find("div.child:eq(3)").addClass("on");
	$("#userExtendEditDialog").click(function(){
		$("#userExtendEditDialog").hide();
		$("#shadeFloor").hide();
	});
	
	
	// 前往查找推广链接
	extendMgrPage.getAllUserRegisterLinks();
	

});



/**
 * 加载用户的所有注册地址
 */
ExtendMgrPage.prototype.getAllUserRegisterLinks = function(queryParam,pageNo) {
	extendMgrPage.queryParam.userId=currentUser.id;
	extendMgrPage.getUserRegisterLinks(extendMgrPage.queryParam,extendMgrPage.pageParam.pageNo);
};


/**
 * 请求加载加载用户的所有注册地址
 */
ExtendMgrPage.prototype.getUserRegisterLinks = function(queryParam,pageNo) {
	queryParam.pageNo = pageNo;
	var jsonData = {
		"query" : queryParam,
		"pageNo" : pageNo,
	};
	$.ajax({
        type: "POST",
        url: contextPath +"/register/userExtendQuery",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(jsonData), 
        dataType:"json",
        success: function(result){
            if (result.code == "ok") {
            	extendMgrPage.showUserRegisterLinks(result.data);
            }else if(result.code != null && result.code == "error"){
            	frontCommonPage.showKindlyReminder(result.data);
            }else{
                showErrorDlg(jQuery(document.body), "用户发送消息记录失败.");
            }
//            footerBool();
        }
    });
};


/**
 * 展示用户的所有可注册地址
 */
ExtendMgrPage.prototype.showUserRegisterLinks = function(page) {
	var extendLinkListObj = $("#extendLinkList");
	 var registerList = page.pageContent;
	var str = "";
	extendLinkListObj.html("");
	if (registerList.length > 0) {

		extendMgrPage.param.linkList = registerList;

		for (var i = 0; i < registerList.length; i++) {
			var register = registerList[i];
			str += " <div class='links-line'>";
//			str += " <div class='child child1'>"+register.orders+"</div>";
			str += " <div class='child child1Extend'>"+(register.invitationCode==null?"":register.invitationCode)+"</div>";
			str += " <div class='child child2Extend'>" + register.createdDateStr + "</div>";
			str += " <div class='child child4'>" + register.useCount + "</div>";	
		/*		if (register.domainEnabled != null && register.domainEnabled == 1) {
					str += "  <div class='child child5'>链接有效</div>";
				} else {
					str += "  <div class='child child5'>链接无效</div>";
				}*/
				
				// 注册代理类型
		//		if(register.dailiLevel == "DIRECTAGENCY"){
		//			str += "  <td><span>直属代理</span></td>";
		//		}else if(register.dailiLevel == "GENERALAGENCY"){
		//			str += "  <td><span>总代理</span></td>";
		//		}else if(register.dailiLevel == "ORDINARYAGENCY"){
		//			str += "  <td><span>代理</span></td>";
		//		}else if(register.dailiLevel == "REGULARMEMBERS"){
		//			str += "  <td><span>普通会员</span></td>";
		//		}else{
		//			alert("未知的代理类型");
		//		}
				
				str += "  <div class='child child6'> <a href='javascript:void(0)' onclick='extendMgrPage.editExtend("+ register.id + ")'>模式调整</a>" +
					 "&nbsp;&nbsp;&nbsp;<a href='javascript:void(0)' onclick='extendMgrPage.showRegisterDetail("
						+ register.id
						+ ")'>详情</a>&nbsp;&nbsp;&nbsp;<a href='javascript:void(0)' onclick='extendMgrPage.deleteRegisterLinkEvent("
						+ register.id + ")'>删除</a></div>";
				str += "</div>";
				extendLinkListObj.append(str);
				str = "";
		}
	} else {
		str += "<div class='links-line'>";
		str += "  <div><p>您还没有可注册的地址.</p></div>";
		str += "</div>";
		extendLinkListObj.append(str);
	}
	
	  //显示分页栏
    $("#extendLinkListPageMsg").pagination({
        items: page.totalRecNum,
        itemsOnPage: page.pageSize,
        currentPage: page.pageNo,
		onPageClick:function(pageNumber) {
			extendMgrPage.pageQuery(pageNumber);
		}
    }); 
};



/**
 * 根据条件查找对应页
 */
ExtendMgrPage.prototype.pageQuery = function(pageNo){
	if(pageNo < 1){
		extendMgrPage.pageParam.pageNo = 1;
	} else{
		extendMgrPage.pageParam.pageNo = pageNo;
	}
	//如果页码小于等于0，则不进行分页查找
	if(extendMgrPage.pageParam.pageNo <= 0){
		return;
	}
	
	extendMgrPage.getUserRegisterLinks(extendMgrPage.queryParam,extendMgrPage.pageParam.pageNo);
	
};

/**
 *模式调整
 * 
 * @param index
 */
ExtendMgrPage.prototype.editExtend = function(linkId) {
	window.location.href = contextPath + "/gameBet/agentCenter/extendEdit.html?linkid="+linkId;
};

/**
 * 删除用户注册链接按钮事件
 * 
 * @param index
 */
ExtendMgrPage.prototype.deleteRegisterLinkEvent = function(linkId) {
	extendMgrPage.param.deleteLinkId = linkId;
	if(confirm("确认删除该推广链接吗?",extendMgrPage.deleteRegisterLink)){
	}
};

/**
 * 删除用户注册链接
 * 
 * @param index
 */
ExtendMgrPage.prototype.deleteRegisterLink = function() {
	$.ajax({
        type: "POST",
        url: contextPath +"/register/deleteUserExtend",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify({'linkId':extendMgrPage.param.deleteLinkId}), 
        dataType:"json",
        success: function(result){
            if (result.code == "ok") {
            	extendMgrPage.getUserRegisterLinks(extendMgrPage.queryParam,extendMgrPage.pageParam.pageNo); // 重新加载用户的注册链接地址
            }else if(result.code != null && result.code == "error"){
            	frontCommonPage.showKindlyReminder(result.data);
            }else{
                showErrorDlg(jQuery(document.body), "删除注册链接请求失败.");
            }
        }
    });
};

/**
 * 展示注册详情的信息
 */
ExtendMgrPage.prototype.showRegisterDetail = function(linkid) {
	window.location.href = contextPath + "/gameBet/agentCenter/extendDetail.html?linkid="+linkid;
};

