/*
 * 推广编辑
 */
function ExtendEditPage() {
}
var extendEditPage = new ExtendEditPage();

extendEditPage.param = {
		linkId : null
};

/**
 * 添加参数
 */
extendEditPage.editParam = {
	id : null,
	sscrebate : null,
	ffcRebate : null,
	syxwRebate : null,
	ksRebate : null,
	klsfRebate : null,
	dpcRebate : null,
	pk10Rebate:null,
	ksRebate:null
};

$(document).ready(function() {
	extendEditPage.param.linkId = location.search.split('=')[1];
	$("div.lists").find("div.child:eq(3)").addClass("on");
    //加载系统最低配额
	extendEditPage.loadUserRebate();
	if(extendEditPage.param.linkId !=null){
		extendEditPage.getExtendLink();
	}else{
		alert("参数传递错误.");
	}

	
	$("#sscModeAdd").unbind("click").click(function() {
		var sscrebateValue = Number($("#sscrebateValue").val());
		if(sscrebateValue%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		extendEditPage.editParam.sscrebate = sscrebateValue;
		if(extendEditPage.editParam.sscrebate > sscOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于时时彩模式["+ sscOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(extendEditPage.editParam.sscrebate < sscOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于时时彩模式["+ sscOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		if(extendEditPage.editParam.sscrebate == sscOpenUserHighest){
			frontCommonPage.showKindlyReminder("当前时时彩模式已经是最高值了.");
			return;
		}
		
		extendEditPage.editParam.sscrebate += sscInterval; 
		if(extendEditPage.editParam.sscrebate > sscOpenUserHighest){
			extendEditPage.editParam.sscrebate = sscOpenUserHighest;
		}
		$("#sscrebateValue").val(extendEditPage.editParam.sscrebate);
	
		$("#sscTip").html('');
		$("#sscTip").append("返点"+extendEditPage.calculateRebateByAwardModelInterval(sscOpenUserHighest, extendEditPage.editParam.sscrebate, sscHighestAwardModel)+"%");
	});
	
	$("#sscModeReduction").unbind("click").click(function() {
		var sscrebateValue = Number($("#sscrebateValue").val());
		if(sscrebateValue%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		extendEditPage.editParam.sscrebate = sscrebateValue;
		if(extendEditPage.editParam.sscrebate > sscOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于时时彩模式最高值了.请重新输入！");
			return;
		}
		if(extendEditPage.editParam.sscrebate < sscOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于时时彩模式最低值了.请重新输入！");
			return;
		}
		if(extendEditPage.editParam.sscrebate == sscOpenUserLowest){
			frontCommonPage.showKindlyReminder("当前时时彩模式已经是最低值了.");
			return;
		}
		
		extendEditPage.editParam.sscrebate -= sscInterval;
		if(extendEditPage.editParam.sscrebate < sscOpenUserLowest){
			extendEditPage.editParam.sscrebate = sscOpenUserLowest;
		}
		$("#sscrebateValue").val(extendEditPage.editParam.sscrebate);
		$("#sscTip").html('');
		$("#sscTip").append("返点"+extendEditPage.calculateRebateByAwardModelInterval(sscOpenUserHighest, extendEditPage.editParam.sscrebate, sscHighestAwardModel)+"%");
	});
	
	$("#sscrebateValue").change(function(){
		var val=$(this).val();
		if(val%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		extendEditPage.editParam.sscrebate = val;
		if(extendEditPage.editParam.sscrebate > sscOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于时时彩模式最高值了.请重新输入！");
			return;
		}
		if(extendEditPage.editParam.sscrebate < sscOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于时时彩模式最低值了.请重新输入！");
			return;
		}
		if(extendEditPage.editParam.sscrebate == sscOpenUserLowest){
			frontCommonPage.showKindlyReminder("当前时时彩模式已经是最低值了.");
			return;
		}
		$("#sscTip").html('');
		$("#sscTip").append("返点"+extendEditPage.calculateRebateByAwardModelInterval(sscOpenUserHighest, extendEditPage.editParam.sscrebate, sscHighestAwardModel)+"%");
	});
	
	//快三模式事件
	$("#ksModeAdd").unbind("click").click(function() {
		var ksrebateValue = Number($("#ksrebateValue").val());
		if(ksrebateValue%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		extendEditPage.editParam.ksRebate = ksrebateValue;
		if(extendEditPage.editParam.ksRebate > ksOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于快三模式["+ ksOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(extendEditPage.editParam.ksRebate < ksOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于快三模式["+ ksOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		if(extendEditPage.editParam.ksRebate == ksOpenUserHighest){
			frontCommonPage.showKindlyReminder("当前快三模式已经是最高值了.");
			return;
		}
		
		extendEditPage.editParam.ksRebate += ksInterval;
		if(extendEditPage.editParam.ksRebate > ksOpenUserHighest){
			extendEditPage.editParam.ksRebate = ksOpenUserHighest;
		}
		$("#ksrebateValue").val(extendEditPage.editParam.ksRebate);
		$("#ksTip").html('');
		$("#ksTip").append("返点"+extendEditPage.calculateRebateByAwardModelInterval(ksOpenUserHighest, extendEditPage.editParam.ksRebate, ksHighestAwardModel)+"%");
	});
	
	$("#ksModeReduction").unbind("click").click(function() {
		var ksrebateValue = Number($("#ksrebateValue").val());
		if(ksrebateValue%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		extendEditPage.editParam.ksRebate = ksrebateValue;
		if(extendEditPage.editParam.ksRebate > ksOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于快三模式最高值了.请重新输入！");
			return;
		}
		if(extendEditPage.editParam.ksRebate < ksOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于快三模式最低值了.请重新输入！");
			return;
		}
		
		
		extendEditPage.editParam.ksRebate -= ksInterval;
		if(extendEditPage.editParam.ksRebate < ksOpenUserLowest){
			extendEditPage.editParam.ksRebate = ksOpenUserLowest;
		}
		$("#ksrebateValue").val(extendEditPage.editParam.ksRebate);
		$("#ksTip").html('');
		$("#ksTip").append("返点"+extendEditPage.calculateRebateByAwardModelInterval(ksOpenUserHighest, extendEditPage.editParam.ksRebate, ksHighestAwardModel)+"%");
	});
	
	$("#ksrebateValue").change(function(){
		var val=$(this).val();
		if(val%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		extendEditPage.editParam.ksRebate = val;
		if(extendEditPage.editParam.ksRebate > ksOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于快三模式["+ ksOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(extendEditPage.editParam.ksRebate < ksOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于快三模式["+ ksOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		$("#ksTip").html('');
		$("#ksTip").append("返点"+extendEditPage.calculateRebateByAwardModelInterval(ksOpenUserHighest, extendEditPage.editParam.ksRebate, ksHighestAwardModel)+"%");
	});
	
	//十一选五模式事件
	$("#syxwModeAdd").unbind("click").click(function() {
		var syxwrebateValue = Number($("#syxwrebateValue").val());
		/*if(syxwrebateValue%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}*/
		extendEditPage.editParam.syxwRebate = syxwrebateValue;
		if(extendEditPage.editParam.syxwRebate > syxwOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于十一选五模式["+ syxwOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(extendEditPage.editParam.syxwRebate < syxwOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于十一选五模式["+ syxwOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		if(extendEditPage.editParam.syxwRebate == syxwOpenUserHighest){
			frontCommonPage.showKindlyReminder("当前十一选五模式已经是最高值了.");
			return;
		}
		
		extendEditPage.editParam.syxwRebate += syxwInterval; 
		if(extendEditPage.editParam.syxwRebate > syxwOpenUserHighest){
			extendEditPage.editParam.syxwRebate = syxwOpenUserHighest;
		}
		$("#syxwrebateValue").val(extendEditPage.editParam.syxwRebate);
		$("#syxwTip").html('');
		$("#syxwTip").append("返点"+extendEditPage.calculateRebateByAwardModelInterval(syxwOpenUserHighest, extendEditPage.editParam.syxwRebate, syxwHighestAwardModel)+"%");
	});
	
	$("#syxwModeReduction").unbind("click").click(function() {
		var syxwrebateValue = Number($("#syxwrebateValue").val());
		/*if(syxwrebateValue%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}*/
		extendEditPage.editParam.syxwRebate = syxwrebateValue;
		if(extendEditPage.editParam.syxwRebate > syxwOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于十一选五模式最高值了.请重新输入！");
			return;
		}
		if(extendEditPage.editParam.syxwRebate < syxwOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于十一选五模式最低值了.请重新输入！");
			return;
		}
		if(extendEditPage.editParam.syxwRebate == syxwOpenUserLowest){
			frontCommonPage.showKindlyReminder("当前十一选五模式已经是最低值了.");
			return;
		}
		
		extendEditPage.editParam.syxwRebate -= syxwInterval;
		if(extendEditPage.editParam.syxwRebate < syxwOpenUserLowest){
			extendEditPage.editParam.syxwRebate = syxwOpenUserLowest;
		}
		$("#syxwrebateValue").val(extendEditPage.editParam.syxwRebate);
		$("#syxwTip").html('');
		$("#syxwTip").append("返点"+extendEditPage.calculateRebateByAwardModelInterval(syxwOpenUserHighest, extendEditPage.editParam.syxwRebate, syxwHighestAwardModel)+"%");
	});
	
	$("#syxwrebateValue").change(function(){
		var val=$(this).val();
		/*if(val%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}*/
		extendEditPage.editParam.syxwRebate = val;
		if(extendEditPage.editParam.syxwRebate > syxwOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于十一选五模式["+ syxwOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(extendEditPage.editParam.syxwRebate < syxwOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于十一选五模式["+ syxwOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		$("#syxwTip").html('');
		$("#syxwTip").append("返点"+extendEditPage.calculateRebateByAwardModelInterval(syxwOpenUserHighest, extendEditPage.editParam.syxwRebate, syxwHighestAwardModel)+"%");
	});
	
	//pk10模式事件
	$("#pk10ModeAdd").unbind("click").click(function() {
		var pk10rebateValue = Number($("#pk10rebateValue").val());
		if(pk10rebateValue%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		extendEditPage.editParam.pk10Rebate = pk10rebateValue;
		if(extendEditPage.editParam.pk10Rebate > pk10OpenUserHighest){
			frontCommonPage.showKindlyReminder("大于PK10模式["+ pk10OpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(extendEditPage.editParam.pk10Rebate < pk10OpenUserLowest){
			frontCommonPage.showKindlyReminder("小于PK10模式["+ pk10OpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		if(extendEditPage.editParam.pk10Rebate == pk10OpenUserHighest){
			frontCommonPage.showKindlyReminder("当前PK10模式已经是最高值了.");
			return;
		}
		
		extendEditPage.editParam.pk10Rebate += pk10Interval; 
		if(extendEditPage.editParam.pk10Rebate > pk10OpenUserHighest){
			extendEditPage.editParam.pk10Rebate = pk10OpenUserHighest;
		}
		$("#pk10rebateValue").val(extendEditPage.editParam.pk10Rebate);
		$("#pk10Tip").html('');
		$("#pk10Tip").append("返点"+extendEditPage.calculateRebateByAwardModelInterval(pk10OpenUserHighest, extendEditPage.editParam.pk10Rebate, pk10HighestAwardModel)+"%");
	});
	
	$("#pk10ModeReduction").unbind("click").click(function() {
		var pk10rebateValue = Number($("#pk10rebateValue").val());
		if(pk10rebateValue%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		extendEditPage.editParam.pk10Rebate = pk10rebateValue;
		if(extendEditPage.editParam.pk10Rebate > pk10OpenUserHighest){
			frontCommonPage.showKindlyReminder("大于PK10模式最高值了.请重新输入！");
			return;
		}
		if(extendEditPage.editParam.pk10Rebate < pk10OpenUserLowest){
			frontCommonPage.showKindlyReminder("小于PK10模式最低值了.请重新输入！");
			return;
		}
		if(extendEditPage.editParam.pk10Rebate == pk10OpenUserLowest){
			frontCommonPage.showKindlyReminder("当前PK10模式已经是最低值了.");
			return;
		}
		
		extendEditPage.editParam.pk10Rebate -= pk10Interval;
		if(extendEditPage.editParam.pk10Rebate < pk10OpenUserLowest){
			extendEditPage.editParam.pk10Rebate = pk10OpenUserLowest;
		}
		$("#pk10rebateValue").val(extendEditPage.editParam.pk10Rebate);
		$("#pk10Tip").html('');
		$("#pk10Tip").append("返点"+extendEditPage.calculateRebateByAwardModelInterval(pk10OpenUserHighest, extendEditPage.editParam.pk10Rebate, pk10HighestAwardModel)+"%");
	});
	
	$("#pk10rebateValue").change(function(){
		var val=$(this).val();
		if(val%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		extendEditPage.editParam.pk10Rebate = val;
		if(extendEditPage.editParam.pk10Rebate > pk10OpenUserHighest){
			frontCommonPage.showKindlyReminder("大于PK10模式["+ pk10OpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(extendEditPage.editParam.pk10Rebate < pk10OpenUserLowest){
			frontCommonPage.showKindlyReminder("小于PK10模式["+ pk10OpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		$("#pk10Tip").html('');
		$("#pk10Tip").append("返点"+extendEditPage.calculateRebateByAwardModelInterval(pk10OpenUserHighest, extendEditPage.editParam.pk10Rebate, pk10HighestAwardModel)+"%");
	});
	
	//低频彩模式事件
	$("#dpcModeAdd").unbind("click").click(function() {
		var dpcrebateValue = Number($("#dpcrebateValue").val());
		if(dpcrebateValue%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		extendEditPage.editParam.dpcRebate = dpcrebateValue;
		if(extendEditPage.editParam.dpcRebate > dpcOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于低频彩模式["+ dpcOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(extendEditPage.editParam.dpcRebate < dpcOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于低频彩模式["+ dpcOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		if(extendEditPage.editParam.dpcRebate == dpcOpenUserHighest){
			frontCommonPage.showKindlyReminder("当前低频彩模式已经是最高值了.");
			return;
		}
		
		extendEditPage.editParam.dpcRebate += dpcInterval; 
		if(extendEditPage.editParam.dpcRebate > dpcOpenUserHighest){
			extendEditPage.editParam.dpcRebate = dpcOpenUserHighest ;
		}
		$("#dpcrebateValue").val(extendEditPage.editParam.dpcRebate);
		$("#dpcTip").html('');
		$("#dpcTip").append("返点"+extendEditPage.calculateRebateByAwardModelInterval(dpcOpenUserHighest,extendEditPage.editParam.dpcRebate, dpcHighestAwardModel)+"%");
	});
	
	//
	$("#dpcModeReduction").unbind("click").click(function() {
		var dpcrebateValue = Number($("#dpcrebateValue").val());
		if(dpcrebateValue%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		extendEditPage.editParam.dpcRebate = dpcrebateValue;
		if(extendEditPage.editParam.dpcRebate > dpcOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于低频彩模式最高值了.请重新输入！");
			return;
		}
		if(extendEditPage.editParam.dpcRebate < dpcOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于低频彩模式最低值了.请重新输入！");
			return;
		}
		if(extendEditPage.editParam.dpcRebate == dpcOpenUserLowest){
			frontCommonPage.showKindlyReminder("当前低频彩模式已经是最低值了.");
			return;
		}
		
		extendEditPage.editParam.dpcRebate -= dpcInterval;
		$("#dpcrebateValue").val(extendEditPage.editParam.dpcRebate);
		$("#dpcTip").html('');
		$("#dpcTip").append("返点"+extendEditPage.calculateRebateByAwardModelInterval(dpcOpenUserHighest,extendEditPage.editParam.dpcRebate, dpcHighestAwardModel)+"%");
	});
	
	$("#dpcrebateValue").change(function(){
		var val=$(this).val();
		if(val%2!=0){
			frontCommonPage.showKindlyReminder("sorry!模式值需要是偶数,请输入偶数,谢谢！");
			return;
		}
		extendEditPage.editParam.dpcRebate = val;
		if(extendEditPage.editParam.dpcRebate > dpcOpenUserHighest){
			frontCommonPage.showKindlyReminder("大于低频彩模式["+ dpcOpenUserHighest +"]最高值了.请重新输入！");
			return;
		}
		if(extendEditPage.editParam.dpcRebate < dpcOpenUserLowest){
			frontCommonPage.showKindlyReminder("小于低频彩模式["+ dpcOpenUserLowest +"]最低值了.请重新输入！");
			return;
		}
		$("#dpcTip").html('');
		$("#dpcTip").append("返点"+extendEditPage.calculateRebateByAwardModelInterval(dpcOpenUserHighest,extendEditPage.editParam.dpcRebate, dpcHighestAwardModel)+"%");
	});
	
});



/**
 * 获取本系统的最高最低模式
 */
ExtendEditPage.prototype.loadUserRebate = function(){
	$.ajax({
        type: "POST",
        url: contextPath +"/system/loadSystemRebate",
        contentType :"application/json;charset=UTF-8",
//        data:JSON.stringify(), 
        dataType:"json",
        success: function(result){
            if (result.code == "ok") {
            	sscOpenUserLowest=result.data.sscLowestAwardModel;
    			syxwOpenUserLowest=result.data.syxwLowestAwardModel;
    			dpcOpenUserLowest=result.data.dpcLowestAwardModel;
    			pk10OpenUserLowest=result.data.pk10LowestAwardModel;
    			ksOpenUserLowest=result.data.ksLowestAwardModel;
            }else if(result.code != null && result.code == "error"){
            	//加载为空不处理
            }else{
                showErrorDlg(jQuery(document.body), "加载用户配额信息请求失败.");
            }
        }
    });
}


/**
 * 获取推广链接 
 */
ExtendEditPage.prototype.getExtendLink = function(){
	$.ajax({
        type: "POST",
        url: contextPath +"/register/getUserExtendById",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify({'linkId':extendEditPage.param.linkId}), 
        dataType:"json",
        success: function(result){
            if (result.code == "ok") {
            	extendEditPage.showRegisterDetail(result.data);
            }else if(result.code != null && result.code == "error"){
            	frontCommonPage.showKindlyReminder(result.data);
            }else{
                showErrorDlg(jQuery(document.body), "获取系统可用域名请求失败.");
            }
        }
    });
};



/**
 * 获取推广链接 
 */
ExtendEditPage.prototype.updateExtendLink = function(){
	extendEditPage.editParam.id = extendEditPage.param.linkId;
	extendEditPage.editParam.sscrebate = $("#sscrebateValue").val();
	extendEditPage.editParam.syxwRebate = $("#syxwrebateValue").val();
	extendEditPage.editParam.ksRebate = $("#ksrebateValue").val();
	extendEditPage.editParam.pk10Rebate = $("#pk10rebateValue").val();
	extendEditPage.editParam.dpcRebate = $("#dpcrebateValue").val();
	$.ajax({
        type: "POST",
        url: contextPath +"/register/updateUserExtend",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(extendEditPage.editParam), 
        dataType:"json",
        success: function(result){
            if (result.code == "ok") {
            	frontCommonPage.showKindlyReminder("保存成功！");
            }else if(result.code != null && result.code == "error"){
            	frontCommonPage.showKindlyReminder(result.data);
            }else{
                showErrorDlg(jQuery(document.body), "获取系统可用域名请求失败.");
            }
        }
    });
};


/**
 * 展示注册详情的信息
 */
ExtendEditPage.prototype.showRegisterDetail = function(userLink) {
		var extendLink = userLink;
	
		// 时时彩
		extendEditPage.editParam.sscRebate = extendLink.sscRebate;
		$("#sscrebateValue").val(extendLink.sscRebate);
		$("#sscTip").html('');
		$("#sscTip").append("返点"+extendLink.sscRebateValue+"%");

		// 十一选五
		extendEditPage.editParam.syxwRebate = extendLink.syxwRebate;
		$("#syxwTip").html('');
		$("#syxwTip").append("返点"+extendLink.syxwRebateValue+"%");
		$("#syxwrebateValue").val(extendLink.syxwRebate);
	
		// 快三模式
		extendEditPage.editParam.ksRebate = extendLink.ksRebate;
		$("#ksTip").html('');
		$("#ksTip").append("返点"+extendLink.ksRebateValue+"%");
		$("#ksrebateValue").val(extendLink.ksRebate);
	
		// PK10模式
		extendEditPage.editParam.pk10Rebate = extendLink.pk10Rebate;
		$("#pk10Tip").html('');
		$("#pk10Tip").append("返点"+extendLink.pk10RebateValue+"%");
		$("#pk10rebateValue").val(extendLink.pk10Rebate);
	
		// 低频彩模式
		extendEditPage.editParam.dpcRebate = extendLink.dpcRebate;
		$("#dpcTip").html('');
		$("#dpcTip").append("返点"+extendLink.dpcRebateValue+"%");
		$("#dpcrebateValue").val(extendLink.dpcRebate);

};


/**
 * 计算用户自身保留返点
 */
ExtendEditPage.prototype.calculateRebateByAwardModelInterval = function(highRebate, lowRebate, highestAwardModel) {
	var rebate = (highRebate - lowRebate)/highestAwardModel;
	rebate = (rebate * 100).toFixed(2);
	return rebate;
}
