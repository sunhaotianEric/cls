function UserMyContract(){}
var userMyContract = new UserMyContract();



$(document).ready(function() {
	
	$("div.lists").find("div.child:eq(3)").addClass("on");
	userMyContract.showdividend();
});




/**
 * 契约设定
 */
UserMyContract.prototype.showdividend = function(){
	$.ajax({
		type: "POST",
        url: contextPath +"/bonus/getMyContract",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(), 
        dataType:"json",
        success: function(result){
        	if(result.code != null && result.code == "ok"){
        		userMyContract.dividendHtml(result.data);
        	}else if(result.code != null && result.code == "error"){
        		frontCommonPage.showKindlyReminder(result.code);
        	}else{
    			showErrorDlg(jQuery(document.body), "查询用户的团队会员数据失败.");
    		}
        }
	});
}

UserMyContract.prototype.dividendHtml = function(bonusConfigs){
	
	
	$('#div_myContract').html("");
	var str='<div class="msgs content-line even">';
	str+='<p class="msg-title">说明： </p>';
	str+='<p>1. 周期特指每月：1号到每月最后一天 </p>';
	str+='<p>2. 设置契约分红时，请遵循由低至高的规律填写 </p>';
	str+='<p>3. 契约分红记录请到订单报表—分红记录查看</p>';
	str+='</div>';
	str+='<div class="content-line odd" >';
	str+='<p class="c-title">分红契约</p>';
	str+='<div class="info">';
	if(bonusConfigs == null){
		str+='<p>当前没有可用的契约！</p>';
	}else{
		var bonusConfigList=bonusConfigs.bonusConfigList;
		for(var i=0;i<bonusConfigList.length;i++){
			if(i==0){
				str+='<p>保底分红'+bonusConfigList[i].value+'%</p>';	
			}else{
				str+='<p>方案'+i+':周期投注额'+bonusConfigList[i].scale+'万元，可获得 '+bonusConfigList[i].value+'%分紅</p>';		
			}		
		}
			
		if(bonusConfigList.length==0){
			str+='<p>当前没有可用的契约！</p>';
		}
	}
	str+=' </div>';
	str+=' </div>';
	$('#div_myContract').append(str);
	//userMyContract.showBounsButton(bonusConfigs.bonusState);
}

UserMyContract.prototype.showBounsButton = function(bonusState){
     
	if(bonusState==1||bonusState==3){
		$("#button_myContract").show();
	}else{
		$("#button_myContract").hide();
	}

}



/**
 * 同意契约
 */
//UserMyContract.prototype.agreeBonusConfigs = function(){
//	$.ajax({
//		type: "POST",
//        url: contextPath +"/bonus/agreeBonusConfigs",
//        contentType :"application/json;charset=UTF-8",
//        data:JSON.stringify(), 
//        dataType:"json",
//        success: function(result){
//        	if(result.code != null && result.code == "ok"){
//        		userMyContract.showdividend();
//        		frontCommonPage.showKindlyReminder("契约同意成功！");
//        	}else if(result.code != null && result.code == "error"){
//        		frontCommonPage.showKindlyReminder(result.code);
//        	}else{
//    			showErrorDlg(jQuery(document.body), "查询数据失败.");
//    		}
//        }
//	});
//}

/**
 * 拒接契约
 */
UserMyContract.prototype.refuseBonusConfigs = function(){
	$.ajax({
		type: "POST",
        url: contextPath +"/bonus/refuseBonusConfigs",
        contentType :"application/json;charset=UTF-8",
        data:JSON.stringify(), 
        dataType:"json",
        success: function(result){
        	if(result.code != null && result.code == "ok"){
        		userMyContract.showdividend();
        		frontCommonPage.showKindlyReminder("契约拒绝成功！");
        	}else if(result.code != null && result.code == "error"){
        		frontCommonPage.showKindlyReminder(result.code);
        	}else{
    			showErrorDlg(jQuery(document.body), "查询数据失败.");
    		}
        }
	});
}