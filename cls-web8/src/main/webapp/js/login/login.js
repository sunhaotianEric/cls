function LoginPage() {
	
}
var loginPage = new LoginPage();
var isLogined = 1;
loginPage.param = {
	showCheckCode: false
};

var mainbg=new change();
var password = ''
$(document).ready(function() {
	
	mainbg.init({
		"element_move":".mainbg",
		"element":".mainbg .content",
		"position_style":"no",
		"autoplay_time":10000,
		"autoplay":true
	});
	
	$("#quc_password").val("");
	$("#quc_account").unbind("blur").blur(function() {
		var account = $("#quc_account").val();
		if (account == null || $.trim(account) == "") {
			$("#quc_account_tip").css("display", "block");
		}
	});
	$("#quc_password").unbind("focus").focus(function() {
		$.ajax({
			type: "POST",
			url: contextPath +"/user/getIsShowCheckCode",
			contentType : 'application/json;charset=UTF-8',
	        success: function(result){
	        	commonHandlerResult(result, this.url);
	        	if(result.code == "ok") {
	        	} else if(result.code == "error"){
	        		var r = result.data;
	        		var count = r[1];
					if (count >= 3) {
						loginPage.param.showCheckCode = true;
						$("#quc_phrase_tip").css("display", "block");
					}
	        	} else {
	        		frontCommonPage.showKindlyReminder(result.data);
//	        		alert("获取验证码显示数据出现异常,请联系管理员");
	        	}
	        }
	    });
	});
	$("#quc_password").unbind("blur").blur(function() {
		 password = $("#quc_password").val();
		if (password == null || $.trim(password) == "") {
			$("#quc_password_tip").css("display", "block");
		}
	});
	$("#quc_phrase").unbind("focus").focus(function() {
		/*$("#quc_phrase_tip").css("display", "none");*/
	});
	$("#quc_phrase").unbind("blur").blur(function() {
		var phrase = $("#quc_phrase").val();
		if (phrase == null || $.trim(phrase) == "") {
			$("#quc_phrase_tip").css("display", "block");
		}
	});
	$("body").keydown(function(event) {
		var curKey = event.which;
		if (curKey == "13") {
			loginPage.loginValidate();
		}
	});
	
	$("#userAgreeButton").click(function(){
		//协议隐藏
		$("#userMsgPerfectDialog").hide();
		$(".alert-msg-bg").hide();
		
		loginPage.login();
	});
	
	//验证银行卡姓名提交验证登陆
	$("#cardNameValidateBtn").click(function(){
		loginPage.login();
	});
	//验证银行卡姓名取消验证登陆
	$("#cardNameValidateCancelBtn").click(function(){
		$("#trueNameSuccessTip").hide();
		$("#trueNameTip").hide();
		$("#trueName").val("");
		$(".alert-msg-bg").stop(false,true).fadeOut(500);
		$(".alert-msg").stop(false,true).fadeOut(500);
	});
	//登陆姓名校验输入隐藏提示
	$("#trueName").unbind("focus").focus(function() {
		$("#trueNameTip").hide();
	});
	
	
	//协议隐藏
	$("#userDisAgreeButton").click(function(){
		$("#userMsgPerfectDialog").hide();
		$(".alert-msg-bg").hide();
	});
	//协议关闭
	$(".close-reveal-modal").click(function(){
		$("#userMsgPerfectDialog").hide();
		$(".alert-msg-bg").hide();
	});
	
	$("#loginButton").click(function() {
		$("#quc_account_tip").removeClass("error");
		$("#quc_password_tip").removeClass("error")
		$("#quc_phrase_tip").removeClass("error")
		if(isLogined==1){
			isLogined = 0;
			loginPage.loginValidate();
		}
	});
	
});

LoginPage.prototype.initPage = function() {
	$("#reveal_modal_loading").show();
	loginLoadPage.loadPage();
	$("#reveal_modal_loading").hide();
	var b_name = navigator.appName;
	var b_version = navigator.appVersion;
	var version = b_version.split(";");
	if (b_name == "Microsoft Internet Explorer") {
		var trim_version = version[1].replace(/[ ]/g, "");
		if (trim_version == "MSIE7.0" || trim_version == "MSIE6.0") {
			$("#reveal_lowIE_message").show();
			$("#reveal_modal_bg").show();
			loginPage.param.isIeLow = true
		}
	}
};

LoginPage.prototype.checkCodeRefresh = function() {
	$('#quc_phrase').attr('src', contextPath + "/tools/getRandomCodePic?time="+Math.random());
}

LoginPage.prototype.loginValidate = function() {
	var userName = $("#quc_account").val();
	var userPassword = $("#quc_password").val();
	password = userPassword;
	var code = $("#quc_phrase").val();
	if (userName == null || $.trim(userName) == "") {
		$("#quc_account_tip").addClass("error");
		/*loginPage.resetLoginButton();*/
		return;
	}
	if (userPassword == null || $.trim(userPassword) == "") {
		/*$("#quc_password_tip").css("display", "block");*/
		/*loginPage.resetLoginButton();*/
		$("#quc_password_tip").addClass("error");
		return;
	}
	if (loginPage.param.showCheckCode == true) {
		if (code == null || $.trim(code) == "") {
			/*$("#quc_phrase_tip").show();*/
			/*loginPage.resetLoginButton();*/
			$("#quc_phrase_tip").addClass("error");
			return;
		}
	}
	loginPage.login();
	
};

LoginPage.prototype.login = function() {
	//$("#loginButton").unbind("click");
	$("#loginTip").text("登录中...");
	$("#errMsg").css("display", "none");
	var userName = $("#quc_account").val();
	var userPassword = $("#quc_password").val();
	var code = $("#quc_phrase").val();
	var trueName = $("#trueName").val();
	if (userName == null || $.trim(userName) == "") {
		$("#quc_account_tip").addClass("error");
		//loginPage.resetLoginButton();
		return
	}
	if (userPassword == null || $.trim(userPassword) == "") {
		/*$("#quc_password_tip").css("display", "block");*/
		/*loginPage.resetLoginButton();*/
		$("#quc_password_tip").addClass("error");
		return
	}
	if (loginPage.param.showCheckCode == true) {
		if (code == null || $.trim(code) == "") {
			/*$("#quc_phrase_tip").show();*/
			/*loginPage.resetLoginButton();*/
			$("#quc_phrase_tip").addClass("error");
			return
		}
	}
	// 16位随机数
	function Number() {
      let rnd = ""
      for (let i = 0; i < 16; i++) {
        rnd += Math.floor(Math.random() * 10)
      }
      return rnd
    }
	
	var time = Number();
	var base = new Base64();
	var Base64key = base.encode(time);
	var encryptData = encrypt(password,time)
//alert("加密前："+encryptData);
	
	/**
	 * 加密（需要先加载lib/aes/aes.min.js文件）
	 * @param word
	 * @returns {*}
	 */
	function encrypt(word,t){
	    var key = CryptoJS.enc.Utf8.parse(t);
	    var srcs = CryptoJS.enc.Utf8.parse(word);
	    var encrypted = CryptoJS.AES.encrypt(srcs, key, {mode:CryptoJS.mode.ECB,padding: CryptoJS.pad.Pkcs7});
	    return encrypted.toString();
	}

	/**
	 * 解密
	 * @param word
	 * @returns {*}
	 */
//	function decrypt(word){
//	    var key = CryptoJS.enc.Utf8.parse(time);
//	    var decrypt = CryptoJS.AES.decrypt(word, key, {mode:CryptoJS.mode.ECB,padding: CryptoJS.pad.Pkcs7});
//	    return CryptoJS.enc.Utf8.stringify(decrypt).toString();
//	}
	
	var loginParam = {};
	loginParam.userName = userName;
//	loginParam.password = userPassword;
	loginParam.checkCode = code;
	loginParam.trueName = trueName;
	loginParam.random = Base64key;
	loginParam.passwordRandom = encryptData;

	$.ajax({
		type: "POST",
		url: contextPath +"/user/userLogin",
		contentType : 'application/json;charset=UTF-8',
        data: JSON.stringify(loginParam),
        success: function(result){
        	isLogined=1;
        	if(result.code == "ok") {
        		$("#truNameSuccessTip").show();
				$(".alert-msg-bg").stop(false,true).fadeOut(500);
				$(".alert-msg").stop(false,true).fadeOut(500);
				setTimeout("window.location.href= contextPath + '/gameBet/index.html'", 1000);
        	} else if(result.code == "error") {
        		$("#errMsg").show();
        		$("#errMsg").css("opacity",'1');
        		$("#errMsg").html(result.data);
				//刷新验证码
				loginCheckCodeRefresh();
				var count = result.data2;
				if (count >= 3) {
					loginPage.param.showCheckCode = true;
					$("#quc_phrase_tip").css("display", "block");
				}
				/*loginPage.resetLoginButton();*/
			}
        }
    });
};

/*LoginPage.prototype.resetLoginButton = function() {
	$("#loginButton").click(function() {
		$("#quc_account_tip").removeClass("error");
		loginPage.loginValidate();
	});
	$("#loginTip").text("登录");
};*/

