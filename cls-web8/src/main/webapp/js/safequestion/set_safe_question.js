function SafequestionSetPage() {
}
var safequestionSetPage = new SafequestionSetPage();

safequestionSetPage.param = {
	passwordIsNull : false
};

$(document).ready(function() {
	// 当前链接位置标注选中样式
	$("#user_info_save").addClass("selected");

	safequestionSetPage.initPage();

	$("#dna_que_1").focus(function() {
		$("#dna_que_1Tip").css({
			display : "none"
		});
		$("#dna_que_1Tip").removeClass("red");
	});
	$("#dna_que_1").blur(function() {
		var dna_que_1 = $("#dna_ques_1").val();
		if (dna_que_1 == null || dna_ques_1 == "") {
			$("#dna_que_1Tip").css({
				display : "inline"
			});
			$("#dna_que_1Tip").addClass("red");
			$("#dna_que_1Tip").html("<i></i>请选择安全问题一");
		}
	});

	$("#dna_que_2").focus(function() {
		$("#dna_que_2Tip").css({
			display : "none"
		});
		$("#dna_que_2Tip").removeClass("red");
	});
	$("#dna_que_2").blur(function() {
		var dna_que_2 = $("#dna_que_2").val();
		if (dna_que_2 == null || dna_ques_2 == "") {
			$("#dna_que_2Tip").css({
				display : "inline"
			});
			$("#dna_que_2Tip").addClass("red");
			$("#dna_que_2Tip").html("<i></i>请选择安全问题二");
		}
	});
	
	
	$("#dna_que_3").focus(function() {
		$("#dna_que_3Tip").css({
			display : "none"
		});
		$("#dna_que_3Tip").removeClass("red");
	});
	$("#dna_que_3").blur(function() {
		var dna_que_3 = $("#dna_que_3").val();
		if (dna_que_3 == null || dna_ques_3 == "") {
			$("#dna_que_3Tip").css({
				display : "inline"
			});
			$("#dna_que_3Tip").addClass("red");
			$("#dna_que_3Tip").html("<i></i>请选择安全问题三");
		}
	});

	$("#answer1").focus(function() {
		$("#answer1Tip").text("1-30个字符");
		$("#answer1Tip").css({
			display : "inline"
		});
		$("#answer1Tip").removeClass("red");
	});
	$("#answer1").blur(function() {
		var answer1 = $("#answer1").val();
		if (answer1 == null || answer1 == "") {
			$("#answer1Tip").css({
				display : "inline"
			});
			$("#answer1Tip").addClass("red");
			$("#answer1Tip").html("<i></i>问题一答案不能为空");
		} else if (answer1.length < 1 || answer1.length > 30) {
			$("#answer1Tip").css({
				display : "inline"
			});
			$("#answer1Tip").addClass("red");
			$("#answer1Tip").html("<i></i>问题一答案必须为1-30个字符");
		}
	});

	$("#answer2").focus(function() {
		$("#answer2Tip").text("1-30个字符");
		$("#answer2Tip").css({
			display : "inline"
		});
		$("#answer2Tip").removeClass("red");
	});

	$("#answer2").blur(function() {
		var answer2 = $("#answer2").val();
		if (answer2 == null || answer2 == "") {
			$("#answer2Tip").css({
				display : "inline"
			});
			$("#answer2Tip").addClass("red");
			$("#answer2Tip").html("<i></i>问题二答案不能为空");
			safequestionSetPage.bindClick();
			$("#sQuestionBtn").val("保存");
			return;
		} else if (answer2.length < 1 || answer2.length > 30) {
			$("#answer2Tip").css({
				display : "inline"
			});
			$("#answer2Tip").addClass("red");
			$("#answer2Tip").html("<i></i>问题二答案必须为1-30个字符");
		}
	});
	
	$("#answer2").focus(function() {
		$("#answer2Tip").text("1-30个字符");
		$("#answer2Tip").css({
			display : "inline"
		});
		$("#answer2Tip").removeClass("red");
	});

	$("#answer3").blur(function() {
		var answer3 = $("#answer3").val();
		if (answer3 == null || answer3 == "") {
			$("#answer3Tip").css({
				display : "inline"
			});
			$("#answer3Tip").addClass("red");
			$("#answer3Tip").html("<i></i>问题三答案不能为空");
			safequestionSetPage.bindClick();
			$("#sQuestionBtn").val("保存");
			return;
		} else if (answer3.length < 1 || answer3.length > 30) {
			$("#answer3Tip").css({
				display : "inline"
			});
			$("#answer3Tip").addClass("red");
			$("#answer3Tip").html("<i></i>问题二答案必须为1-30个字符");
		}
	});

	$("#password").blur(function() {
		if (safequestionSetPage.param.passwordIsNull == false) {
			var password = $("#password").val();
			if (password == null || password == "" || password.length < 6 || password.length > 15) {
				$("#passwordTip").css({
					display : "inline"
				});
				$("#passwordTip").addClass("red");
				$("#passwordTip").html("<i></i>密码格式不正确");
			}
		}

	});
	$("#sQuestionBtn").click(function() {
		safequestionSetPage.saveInfoQuestion();
	});

});

/**
 * 初始化页面
 */
SafequestionSetPage.prototype.initPage = function() {

	// 设置安全问题选项.
	$.ajax({
		type : "POST",
		url : contextPath + "/safeq/getSafeQuestionInfos",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var dna_ques_1Obj = $("#dna_ques_1");
				var dna_ques_2Obj = $("#dna_ques_2");
				var dna_ques_3Obj = $("#dna_ques_3");

				dna_ques_1Obj.empty();
				dna_ques_2Obj.empty();
				dna_ques_3Obj.empty();

				var eSaveQuestionInfos = result.data;
				for (var i = 0; i < eSaveQuestionInfos.length; i++) {
					var eSaveQuestionInfo = eSaveQuestionInfos[i];
					dna_ques_1Obj.append("<li><p data-value='" + eSaveQuestionInfo.code + "'>" + eSaveQuestionInfo.description + "</p></li>");
					dna_ques_2Obj.append("<li><p data-value='" + eSaveQuestionInfo.code + "'>" + eSaveQuestionInfo.description + "</p></li>");
					dna_ques_3Obj.append("<li><p data-value='" + eSaveQuestionInfo.code + "'>" + eSaveQuestionInfo.description + "</p></li>");
					// 添加自定义下拉框选中事件
					addSelectEvent();
				}
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(jQuery(document.body), "操作失败，请重试");
			}
		}
	});
};


/**
 * 下拉列表联动
 */
SafequestionSetPage.prototype.selectChange = function(obj) {
	var dna_ques_1Obj = $("#dna_que_1");
	var dna_ques_2Obj = $("#dna_que_2");
	var dna_ques_3Obj = $("#dna_que_3");

	var selct1 = dna_ques_1Obj.val();
	var selct2 = dna_ques_2Obj.val();
	var selct3 = dna_ques_3Obj.val();

	var text1 = $("#dna_ques_1").find(".on").children().text();
	var text2 = $("#dna_ques_2").find(".on").children().text();
	var text3 = $("#dna_ques_3").find(".on").children().text();

	if (safequestionSetPage.param.selct1 != null && $.trim(safequestionSetPage.param.selct1) != "") {
		if (safequestionSetPage.param.selct1 != selct1) {
			dna_ques_2Obj.append("<li><p data-value='" + safequestionSetPage.param.selct1 + "'>" + safequestionSetPage.param.text1 + "</p></li>");
		}
	}
	if (safequestionSetPage.param.selct2 != null && $.trim(safequestionSetPage.param.selct2) != "") {
		if (safequestionSetPage.param.selct2 != selct2) {
			dna_ques_1Obj.append("<li><p data-value='" + safequestionSetPage.param.selct2 + "'>" + safequestionSetPage.param.text2 + "</p></li>");
		}
	}
	if (safequestionSetPage.param.selct3 != null && $.trim(safequestionSetPage.param.selct3) != "") {
		if (safequestionSetPage.param.selct3 != selct3) {
			dna_ques_1Obj.append("<li><p data-value='" + safequestionSetPage.param.selct3 + "'>" + safequestionSetPage.param.text3 + "</p></li>");
		}
	}

	safequestionSetPage.param.selct1 = selct1;
	safequestionSetPage.param.selct2 = selct2;
	safequestionSetPage.param.selct3 = selct3;
	
	safequestionSetPage.param.text1 = text1;
	safequestionSetPage.param.text2 = text2;
	safequestionSetPage.param.text3 = text3;

	if (selct1 != null && $.trim(selct1) != "") {
		$("#dna_ques_2").children().each(function(i) {
			if ($(this).find("p").attr("data-value") == selct1) {
				$(this).remove();
			}

		});
	}
	if (selct2 != null && $.trim(selct2) != "") {
		$("#dna_ques_1").children().each(function(i) {
			if ($(this).find("p").attr("data-value") == selct2) {
				$(this).remove();
			}

		});
	}
	if (selct3 != null && $.trim(selct3) != "") {
		$("#dna_ques_1").children().each(function(i) {
			if ($(this).find("p").attr("data-value") == selct3) {
				$(this).remove();
			}

		});
	}
};

/**
 * 绑定事件
 */
SafequestionSetPage.prototype.bindClick = function() {
	$("#sQuestionBtn").unbind("click").click(function() {
		safequestionSetPage.saveInfoQuestion();
	});
};

/**
 * 保存安全密码数据
 */
SafequestionSetPage.prototype.saveInfoQuestion = function() {
	$("#sQuestionBtn").unbind("click");
	$("#sQuestionBtn").val("提交中.....");

	var dna_ques_1 = $("#dna_que_1").val();
	var answer1 = $("#answer1").val();
	
	var dna_ques_2 = $("#dna_que_2").val();
	var answer2 = $("#answer2").val();
	
	var dna_ques_3 = $("#dna_que_3").val();
	var answer3 = $("#answer3").val();

	if (dna_ques_1 == null || dna_ques_1 == "") {
		$("#dna_ques_1Tip").css({
			display : "inline"
		});
		$("#dna_ques_1Tip").addClass("red");
		$("#dna_ques_1Tip").html("<i></i>请选择安全问题一");
		safequestionSetPage.bindClick();
		$("#sQuestionBtn").val("保存");
		return;
	}

	if (answer1 == null || answer1 == "") {
		$("#answer1Tip").css({
			display : "inline"
		});
		$("#answer1Tip").addClass("red");
		$("#answer1Tip").html("<i></i>问题一答案不能为空");
		safequestionSetPage.bindClick();
		$("#sQuestionBtn").val("保存");
		return;
	} else {
		if (answer1.length < 1 || answer1.length > 30) {
			$("#answer1Tip").css({
				display : "inline"
			});
			$("#answer1Tip").addClass("red");
			$("#answer1Tip").html("<i></i>问题一答案必须为1-30个字符");
			safequestionSetPage.bindClick();
			$("#sQuestionBtn").val("保存");
			return;
		}
	}

	if (dna_ques_2 == null || dna_ques_2 == "") {
		$("#dna_ques_2Tip").css({
			display : "inline"
		});
		$("#dna_ques_2Tip").addClass("red");
		$("#dna_ques_2Tip").html("<i></i>请选择安全问题二");
		safequestionSetPage.bindClick();
		$("#sQuestionBtn").val("保存");
		return;
	}

	if (answer2 == null || answer2 == "") {
		$("#answer2Tip").css({
			display : "inline"
		});
		$("#answer2Tip").addClass("red");
		$("#answer2Tip").html("<i></i>问题二答案不能为空");
		safequestionSetPage.bindClick();
		$("#sQuestionBtn").val("保存");
		return;
	} else {
		if (answer2.length < 1 || answer2.length > 30) {
			$("#answer2Tip").css({
				display : "inline"
			});
			$("#answer2Tip").addClass("red");
			$("#answer2Tip").html("<i></i>问题二答案必须为1-30个字符");
			safequestionSetPage.bindClick();
			$("#sQuestionBtn").val("保存");
			return;
		}
	}
	
	if (dna_ques_2 == null || dna_ques_2 == "") {
		$("#dna_ques_2Tip").css({
			display : "inline"
		});
		$("#dna_ques_2Tip").addClass("red");
		$("#dna_ques_2Tip").html("<i></i>请选择安全问题二");
		safequestionSetPage.bindClick();
		$("#sQuestionBtn").val("保存");
		return;
	}

	if (answer3 == null || answer3 == "") {
		$("#answer3Tip").css({
			display : "inline"
		});
		$("#answer3Tip").addClass("red");
		$("#answer3Tip").html("<i></i>问题二答案不能为空");
		safequestionSetPage.bindClick();
		$("#sQuestionBtn").val("保存");
		return;
	} else {
		if (answer3.length < 1 || answer3.length > 30) {
			$("#answer3Tip").css({
				display : "inline"
			});
			$("#answer3Tip").addClass("red");
			$("#answer3Tip").html("<i></i>问题三答案必须为1-30个字符");
			safequestionSetPage.bindClick();
			$("#sQuestionBtn").val("保存");
			return;
		}
	}

	var userQuestionList = new Array();

	var userQuestion = {};
	
	var userQuestion2 = {};
	
	var userQuestion3 = {};
	
	userQuestion.type = dna_ques_1;
	userQuestion.answer = answer1;
	
	userQuestion2.type = dna_ques_2;
	userQuestion2.answer = answer2;
	
	userQuestion3.type = dna_ques_3;
	userQuestion3.answer = answer3;
	
	userQuestionList.push(userQuestion);
	userQuestionList.push(userQuestion2);
	userQuestionList.push(userQuestion3);
	var jsonData = {
			"questionList" : userQuestionList,
	}
	$.ajax({
		type : "POST",
		url : contextPath + "/safeq/setSafeQuestions",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if( result.code == "ok"){
				frontCommonPage.showKindlyReminder("设置成功...3秒后自动跳转到安全中心...");
				$("#errmsg").hide();
				$("#sQuestionBtn").val("保存");
				setTimeout("window.location.href= contextPath + '/gameBet/accountCenter.html'", 3000);
			}else if(result.code == "error"){
				$("#errmsg").show();
				$("#errmsg").addClass("red")
				$("#errmsg").text(result.data);
				safequestionSetPage.bindClick();
				$("#sQuestionBtn").val("保存");
			}
		}
	});
};

/**
 * 跳转到设置页面
 */
SafequestionSetPage.prototype.trunToSetPage = function() {
	window.location.href = contextPath + "/gameBet/safeCenter/setSafePwd.html";
};

/**
 * 跳转到修改页面
 */
SafequestionSetPage.prototype.trunToUpdatePage = function() {
	window.location.href = contextPath + "/gameBet/safequestion/user_info_question_check.html";
};