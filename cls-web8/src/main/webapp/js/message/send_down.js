function UserNoteSendPage() {
}
var userNoteSendPage = new UserNoteSendPage();

userNoteSendPage.param = {

};

/**
 * 添加参数
 */
userNoteSendPage.addParam = {
	sub : null,
	body : null,
	toUserNames : new Array(),
	sendType : "to_up" // 默认是发给上级
};

$(document).ready(
		function() {

			$("div.lists").find("div.child:eq(4)").addClass("on");
			// 模式对话框关闭事件
			$(".close-reveal-modal").unbind("click").click(function(event) {
				$(this).parent().parent().parent().hide();
				$("#shadeFloor").hide();
			});

			// 分页切换
			$(".head li").unbind("click").click(function() {

				$(".head li").each(function(i) {
					$(this).removeClass("on");
				});
				$(this).addClass("on");
				$(".siftings").hide();

				var dataRole = $(this).attr("data-role");
				var dataValue = $(this).attr("data-value");
				$("#" + dataRole).show();
				userNoteSendPage.addParam.sendType = dataValue;
			});

			// 消息发送
			$("#toUpSend").unbind("click").click(function() {
				userNoteSendPage.sendNote();
			});
			// 消息发送
			$("#toDownSend").unbind("click").click(function() {
				userNoteSendPage.addParam.sendType = "to_down";
				userNoteSendPage.sendNote();
			});

			// 打开添加用户对话框
			$("#toShowAddUserDialog").unbind("click").click(function() {
				$("#addToUserDialog").stop(false, true).delay(200).fadeIn(500);
				$(".alert-msg-bg").stop(false, true).fadeIn(500);
				$("#addToUserDialog").show();
				$("#shadeFloor").show();
				var userListObj = $("#userList");
				userListObj.html("");
				var str = "";
				str += "<span  class='listUserSpan'>暂无记录</span>";

				userListObj.append(str);

				// 查找对应的用户列表
				userNoteSendPage.queryUserDownMembers();
			});

			// 搜索按钮
			$("#searchUserButton").unbind("click").click(function() {
				// var searchUserName = $("#searchUserName").val();
				// if(searchUserName == null || searchUserName == ""){
				// frontCommonPage.showKindlyReminder("对不起,请先填写要查找的用户名.");
				// return;
				// }
				// 查找对应的用户列表
				userNoteSendPage.queryUserDownMembers();
			});

			// 添加用户的图层关闭事件申明
			$(".closeBtn").unbind("click").click(function() {
				$(this).parent().parent().hide();
				$("#shadeFloor").hide();
			});

			// 全选或者取消全选的事件
			$("#userNameSearchRadioName1")[0].checked = false;
			$("#userNameSearchRadioName2")[0].checked = false;
			$("input[name='userNameSearchRadioName1']").unbind("click").click(function() {
				$("#userNameSearchRadioName2")[0].checked = false;
				$("input[name='sendUserName']").each(function(i) {
					if (this.checked == false) {
						this.checked = true;
					}
				});
			});
			$("input[name='userNameSearchRadioName2']").unbind("click").click(function() {
				$("#userNameSearchRadioName1")[0].checked = false;
				$("input[name='sendUserName']").each(function(i) {
					if (this.checked == true) {
						this.checked = false;
					}
				});
			});

			$("#select-all").click(function() {
				$(".alert-msg .line .line-content.line-list input").prop("checked", function() {
					return true;
				});
			});
			$("#select-inverse").click(function() {
				$(".alert-msg .line .line-content.line-list input").each(function(i, n) {
					$(n).prop("checked", function() {
						if ($(this).is(":checked"))
							return false;
						else
							return true;
					});
				});
			});

			// 添加用户事件
			$("#addUserButton").unbind("click").click(
					function() {
						userNoteSendPage.addParam.toUserNames = new Array();
						var toSendUserNameObj = $("#toSendUserName");
						var isHaveChooseUser = false;
						var str = "";
						$("input[name='sendUserName']").each(
								function(i) {
									if (this.checked == true) {
										var sendUserName = $(this).attr("data-value");
										userNoteSendPage.addParam.toUserNames.push(sendUserName);
										var length = userNoteSendPage.addParam.toUserNames.length;
										if (length < 8) {
											str += "<span><span class='deleteUserName' onclick='userNoteSendPage.deleteTheSendUserName(this,\"" + sendUserName
													+ "\")' title='删除该收件人'></span>" + sendUserName + "</span><span>&nbsp;</span>";
										} else {
											if (i == 8) {
												str += "....."
											}
										}
										isHaveChooseUser = true;
									}
								});

						// 是否有选择用户
						if (isHaveChooseUser) {
							$(".alert-msg").stop(false, true).delay(200).fadeOut(500);
							$(".alert-msg-bg").stop(false, true).fadeOut(500);

							$("#toSendUserName").html(str);
							// $("#addToUserDialog").hide();
							// $("#shadeFloor").hide();
						} else {
							frontCommonPage.showKindlyReminder("对不起,您还没选择对应的收件人.");
						}
					});

			// 焦点事件(向上级发送)
			$("#toUpSub").focus(function() {
				$(this).next().show();
				$(this).next().next().hide();
			});
			$("#toUpSub").blur(function() {
				var value = $(this).val();
				if (value == null || value == "" || value.length > 60) {
					$(this).next().hide();
					$(this).next().next().show();
					return;
				}
			});
			$("#toUpBody").focus(function() {
				$(this).next().show();
				$(this).next().next().hide();
			});
			$("#toUpBody").blur(function() {
				var value = $(this).val();
				if (value == null || value == "" || value.length > 500) {
					$(this).next().hide();
					$(this).next().next().show();
					return;
				}
			});

			// 焦点事件(向下级发送)
			$("#toDownSub").focus(function() {
				$(this).next().show();
				$(this).next().next().hide();
			});
			$("#toDownSub").blur(function() {
				var value = $(this).val();
				if (value == null || value == "" || value.length > 60) {
					$(this).next().hide();
					$(this).next().next().show();
					return;
				}
			});
			$("#toDownBody").focus(function() {
				$(this).next().show();
				$(this).next().next().hide();
			});
			$("#toDownBody").blur(function() {
				var value = $(this).val();
				if (value == null || value == "" || value.length > 500) {
					$(this).next().hide();
					$(this).next().next().show();
					return;
				}
			});

		});

/**
 * 用户发送消息
 */
UserNoteSendPage.prototype.sendNote = function() {

	if (userNoteSendPage.addParam.sendType != null && userNoteSendPage.addParam.sendType == "to_up") {
		userNoteSendPage.addParam.sub = $("#toUpSub").val();
		userNoteSendPage.addParam.body = $("#toUpBody").val();
		if (userNoteSendPage.addParam.sub == null || userNoteSendPage.addParam.sub == "" || userNoteSendPage.addParam.sub.length > 60) {
			$("#toUpMsg").css({
				color : 'red'
			});
			return;
		}
		if (userNoteSendPage.addParam.body == null || userNoteSendPage.addParam.body == "" || userNoteSendPage.addParam.body.length > 500) {
			$("#toUpMsgBody").css({
				color : 'red'
			});
			return;
		}
	} else if (userNoteSendPage.addParam.sendType != null && userNoteSendPage.addParam.sendType == "to_down") {
		userNoteSendPage.addParam.sub = $("#toDownSub").val();
		userNoteSendPage.addParam.body = $("#toDownBody").val();
		if (userNoteSendPage.addParam.sub == null || userNoteSendPage.addParam.sub == "" || userNoteSendPage.addParam.sub.length > 60) {
			$("#toDownMsg").css({
				color : 'red'
			});
			return;
		}
		if (userNoteSendPage.addParam.body == null || userNoteSendPage.addParam.body == "" || userNoteSendPage.addParam.body.length > 500) {
			$("#toDownMsgBody").css({
				color : 'red'
			});
			return;
		}
		if (userNoteSendPage.addParam.toUserNames.length == 0) {
			frontCommonPage.showKindlyReminder("对不起,请先选择收件人.");
			return;
		}
		userNoteSendPage.addParam.sub = $("#toDownSub").val();
		userNoteSendPage.addParam.body = $("#toDownBody").val();
	} else {
		alert("消息类型不正确");
	}

	// 封装参数.
	var jsonData = {
		"noteVo" : userNoteSendPage.addParam,
	};
	// 发送站内信息.
	$.ajax({
		type : "POST",
		url : contextPath + "/note/sendNote",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				frontCommonPage.showKindlyReminder("恭喜您,消息发送成功.");
				window.location.href = contextPath + "/gameBet/message/outbox.html";
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 查询用户下的注册人员
 */
UserNoteSendPage.prototype.queryUserDownMembers = function() {
	var searchUserName = $("#searchUserName").val();
	if (searchUserName == null || searchUserName == "") {
		searchUserName == null;
	}
	// 封装参数
	var jsonData = {
		"memberUserName" : searchUserName,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/user/queryUserDownMembers",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				userNoteSendPage.showUserDownMembers(result.data);
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 删除要发送的收件人
 */
UserNoteSendPage.prototype.deleteTheSendUserName = function(obj, sendUserName) {
	$(obj).next().remove();
	$(obj).remove();
	userNoteSendPage.addParam.toUserNames.remove(sendUserName);
	if (userNoteSendPage.addParam.toUserNames.length == 0) {
		$("#toSendUserName").append("<span>您还未选择收件人</span>");
	}
};

/**
 * 刷新用户列表数据
 */
UserNoteSendPage.prototype.showUserDownMembers = function(userList) {

	var userListObj = $("#userList");
	userListObj.html("");
	var str = "";

	if (userList.length > 0) {
		for (var i = 0; i < userList.length; i++) {
			var user = userList[i];
			str += "<li>";
			str += "<input type='checkbox' name='sendUserName' id='s" + i + "' data-value='" + user.userName + "'/> ";
			str += "<label for='s" + i + "' style='vertical-align:middle'>" + user.userName + "</label>";
			str += "</li>";
			userListObj.append(str);
			str = "";
		}
	} else {
		str += "<span  class='listUserSpan'>未找到符合条件的记录.</span>";
		userListObj.append(str);
	}
};
