function UserNoteDetailPage() {
}
var userNoteDetailPage = new UserNoteDetailPage();

userNoteDetailPage.param = {
	noteId : null,
	noteParent : null
};

// 回复消息参数
userNoteDetailPage.replyParam = {
	parentId : null,
	body : null
};

$(document).ready(function() {

	if (userNoteDetailPage.param.noteId != null || userNoteDetailPage.param.noteId != "") {
		userNoteDetailPage.signNoteYetRead(); // 将消息记录标志位已读
		userNoteDetailPage.queryNoteDetailList(); // 查询消息记录列表
	}

	// 消息回复按钮事件
	$("#replyNoteButton").unbind("click").click(function() {
		$(this).attr('disabled', 'disabled');
		$(this).text("消息发送中...");
		var replyContent = $("#replyContent").val();
		if (replyContent == null || replyContent == "") {
			frontCommonPage.showKindlyReminder("回复内容不能为空.");
			$(this).removeAttr("disabled");
			$(this).text("回复");
			return;
		}
		if (replyContent.length > 500) {
			frontCommonPage.showKindlyReminder("回复的内容太长,请重新回复.");
			$(this).removeAttr("disabled");
			$(this).text("回复");
			return;
		}

		if (userNoteDetailPage.param.noteId != null) {
			userNoteDetailPage.replyNote(); // 回复消息
		} else {
			frontCommonPage.showKindlyReminder("系统参数传递错误.");
			$(this).removeAttr("disabled");
			$(this).text("回复");
		}
	});

	// 删除按钮事件
	$("#deleteNoteButton").unbind("click").click(function() {
		if (confirm("删除以后,不能恢复.确认要删除该会话吗?", userNoteDetailPage.updateToUserNameForDelete)) {

		}
	});

});

/**
 * 消息回复
 */
UserNoteDetailPage.prototype.replyNote = function() {
	userNoteDetailPage.replyParam.parentId = userNoteDetailPage.param.noteId;
	userNoteDetailPage.replyParam.body = $("#replyContent").val();
	var jsonData = {
		"note" : userNoteDetailPage.replyParam,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/note/replyNote",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var note = result.data;
				if (note != null) {
					var noteDetailListObj = $("#noteDetailList");
					var str = "";
					str += "<dl class='dia-list-left'>";
					str += "  <dt><strong>我</strong>" + note.createDateStr + "</dt>";
					str += "  <dd>" + note.body + "</dd>";
					str += "</dl>";
					noteDetailListObj.append(str);
				}
				$("#replyContent").val("");
				$("#replyNoteButton").removeAttr("disabled");
				$("#replyNoteButton").text("回复");
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
				$("#replyNoteButton").removeAttr("disabled");
				$("#replyNoteButton").text("回复");
			}
		}
	});
};

/**
 * 查询消息详情!
 */
UserNoteDetailPage.prototype.queryNoteDetailList = function() {
	var jsonData = {
		"noteId" : userNoteDetailPage.param.noteId,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/note/getNoteDetailList",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				userNoteDetailPage.refreshNoteDetailListPages(result.data);
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 打开收件箱，标志该消息为已读
 */
UserNoteDetailPage.prototype.signNoteYetRead = function() {
	// 封装参数
	var jsonData = {
		"parentId" : userNoteDetailPage.param.noteId,
	};
	// 查看自己发送的邮件!
	$.ajax({
		type : "POST",
		url : contextPath + "/note/signNoteYetRead",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				headerPage.getHeadNoReadMsg(); // 加载头部消息数据
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 删除会话
 */
UserNoteDetailPage.prototype.updateToUserNameForDelete = function() {
	// 封装参数.
	var jsonData = {
		"noteId" : userNoteDetailPage.param.noteId,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/note/updateToUserNameForDelete",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				window.location.href = contextPath + "/gameBet/message/inbox.html";
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 渲染消息记录数据
 */
UserNoteDetailPage.prototype.refreshNoteDetailListPages = function(list) {
	var noteDetailListObj = $("#noteDetailList");
	noteDetailListObj.html("");

	if (list.length > 0) {
		userNoteDetailPage.param.noteParent = list[0];

		var str = "";
		for (var i = 0; i < list.length; i++) {
			var note = list[i];
			if (note.fromUserName == currentUser.userName) {
				str += "<dl class='dia-list-left'>";
				str += "  <dt><strong>我</strong>&nbsp;&nbsp;" + note.createDateStr + "</dt>";
				str += "  <dd>" + note.body + "</dd>";
				str += "</dl>";
			} else {
				str += "<dl class='dia-list-right'>";
				if (note.type != "") {
					str += "  <dt><strong>" + note.fromUserName + "</strong>" + note.createDateStr + "</dt>";
				} else {
					alert("该消息类型未配置.");
				}
				str += "  <dd>" + note.body + "</dd>";
				str += "</dl>";
			}
			noteDetailListObj.append(str);
			str = "";
		}
	} else {

	}

	// 开始的信息的主题
	if (userNoteDetailPage.param.noteParent != null) {

		$("#noteParentSub").text(userNoteDetailPage.param.noteParent.sub);

		if (userNoteDetailPage.param.noteParent.fromUserName == currentUser.userName) {
			$("#sender").text("我");
		} else {
			if (userNoteDetailPage.param.noteParent.type == 'UP_DOWN') {
				$("#sender").text("上级");
			} else if (userNoteDetailPage.param.noteParent.type == 'DOWN_UP') {
				$("#sender").text(userNoteDetailPage.param.noteParent.fromUserName);
			} else {
				alert("该消息类型未配置.");
			}
		}
		$("#sendTime").text(userNoteDetailPage.param.noteParent.createDateStr);
		if (userNoteDetailPage.param.noteParent.toUserName == currentUser.username) {
			$("#reciver").text("我");
		} else {
			if (userNoteDetailPage.param.noteParent.type == 'UP_DOWN') {
				$("#reciver").text(userNoteDetailPage.param.noteParent.toUserName);
			} else if (userNoteDetailPage.param.noteParent.type == 'DOWN_UP') {
				$("#reciver").text("上级");
			} else {
				alert("该消息类型未配置.");
			}
		}

		// 删除按钮显示条件
		if (userNoteDetailPage.param.noteParent.fromUserName == currentUser.userName) {
			$("#deleteNoteButton").show();
		} else {
			if ("DOWN_UP" == userNoteDetailPage.param.noteParent.type) {
				$("#deleteNoteButton").show();
			}
		}

	}
};