function UserNoteMySendListPage() {
}
var userNoteMySendListPage = new UserNoteMySendListPage();

userNoteMySendListPage.param = {
	pitchArray : new Array()
};

// 分页参数
userNoteMySendListPage.pageParam = {
	queryMethodParam : null,
	pageNo : 1,
	pageMaxNo : 1, // 最大的页数
	skipHtmlStr : ""
};

$(document).ready(function() {

	/* $("div.lists").find("div.child:eq(4)").addClass("on"); */
	$("#noteChild").addClass("on");
	// 全选操作
	$("#checkAll").unbind("click").click(function() {
		var valueType = $(this).attr("data-value");
		if (valueType == "all") {
			$("input[name='noteChkArr']").each(function(i) {
				if (this.checked == false) {
					this.checked = true;
				}
			});
			$(this).attr("data-value", "cancal_all");
			$(this).attr("value", "反选");
			$(this).attr("value", "反选");
		} else if (valueType == "cancal_all") {
			$("input[name='noteChkArr']").each(function(i) {
				if (this.checked == true) {
					this.checked = false;
				}
			});
			$(this).attr("data-value", "all");
			$(this).text("全选");
		} else {
			alert("未知的值类型");
		}
	});

	// 删除记录
	$("#deleteSystemNoteButton").unbind("click").click(function() {
		userNoteMySendListPage.param.pitchArray = new Array();

		$("input[name='noteChkArr']").each(function(i) {
			if (this.checked == true) {
				userNoteMySendListPage.param.pitchArray.push($(this).attr("data-value"));
			}
		});
		// 判断是否有选
		if (userNoteMySendListPage.param.pitchArray.length == 0) {
			frontCommonPage.showKindlyReminder("你还没选择要标志的记录.");
			return;
		}

		if (confirm("确认删除?", userNoteMySendListPage.delNotes)) {
		}
	});

	// 查询我的系统收件箱
	userNoteMySendListPage.findUserMySendNotesByQueryParam();
});

/**
 * 页面
 */
UserNoteMySendListPage.prototype.findUserMySendNotesByQueryParam = function() {
	userNoteMySendListPage.pageParam.queryMethodParam = 'findUserMySendNotesByQueryParam';
	userNoteMySendListPage.queryUserSystemNotesByCondition(userNoteMySendListPage.pageParam.pageNo);
};

/**
 * 查询我的发件箱消息
 */
UserNoteMySendListPage.prototype.queryUserSystemNotesByCondition = function(pageNo) {
	var jsonData = {
		"pageNo" : pageNo,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/note/queryMySendNotesByPage",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				userNoteMySendListPage.refreshuserNoteMySendListPages(result.data);
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 刷新列表数据
 */
UserNoteMySendListPage.prototype.refreshuserNoteMySendListPages = function(page) {
	var noteListObj = $("#noteList");

	noteListObj.html("");
	var str = "";
	var userNoteLists = page.pageContent;

	if (userNoteLists.length == 0) {
		str += "<div class='siftings-line on'>";
		str += "  <div  class='child' style='margin-left:485px'>没有符合条件的记录！</div>";
		str += "</div>";
		noteListObj.append(str);
	} else {
		// 记录数据显示
		for (var i = 0; i < userNoteLists.length; i++) {
			var userNoteList = userNoteLists[i];
			str += " <div class='siftings-line'>";
			str += " <div class='child child1'><input  type='checkbox' class='checkbox' data-value=' " + userNoteList.id + "'  name='noteChkArr'></div>";
			if (userNoteList.status == "YET_READ") {
				str += " <div class='child child3'><a href='" + contextPath + "/gameBet/message/messageDetail.html?id=" + userNoteList.id + " '>" + userNoteList.sub
						+ "</a>&nbsp;<i class='fa fa-envelope-open-o'></i></div>";
			} else {
				str += " <div class='child child3'><a href='" + contextPath + "/gameBet/message/messageDetail.html?id=" + userNoteList.id + " '>" + userNoteList.sub
						+ "</a>&nbsp;<i class='fa fa-envelope-o'></i></div>";
			}

			if (userNoteList.type == "UP_DOWN") {
				str += " <div class='child child2'>" + userNoteList.toUserName + "</div>";
			} else if (userNoteList.type == "DOWN_UP") {
				str += " <div class='child child2'>上级</div>";
			}
			str += "  <div class='child child4'>" + userNoteList.createDateStr + "</div>";
			str += "  <div class='child child5'>" + "	<a href='" + contextPath + "/gameBet/message/messageDetail.html?id=" + userNoteList.id + "'>查看</a> "
					+ "	<a href='javascript:void(0);' onClick='userNoteMySendListPage.delNoteById(" + userNoteList.id + ")'>删除</a> <div>";
			str += "</div>";
			noteListObj.append(str);
			str = "";
		}
	}

	// 显示分页栏
	$("#pageMsg").pagination({
		items : page.totalRecNum,
		itemsOnPage : page.pageSize,
		currentPage : page.pageNo,
		onPageClick : function(pageNumber) {
			userNoteMySendListPage.pageQuery(pageNumber);
		}
	});
};

/**
 * 批量删除选中消息
 */
UserNoteMySendListPage.prototype.delNotes = function() {
	// 封装参数.
	var jsonData = {
		"signVal" : userNoteMySendListPage.param.pitchArray,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/note/delNotes",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				window.location.href = contextPath + "/gameBet/message/outbox.html";
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 删除单条记录
 */
UserNoteMySendListPage.prototype.delNoteById = function(id) {
	idArray = new Array();
	idArray.push(id);

	// 封装参数.
	var jsonData = {
		"signVal" : idArray,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/note/delNotes",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				window.location.href = contextPath + "/gameBet/message/outbox.html";
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
}

/**
 * 根据条件查找对应页
 */
UserNoteMySendListPage.prototype.pageQuery = function(pageNo) {
	if (pageNo < 1) {
		userNoteMySendListPage.pageParam.pageNo = 1;
	} else if (pageNo > userNoteMySendListPage.pageParam.pageMaxNo) {
		userNoteMySendListPage.pageParam.pageNo = userNoteMySendListPage.pageParam.pageMaxNo;
	} else {
		userNoteMySendListPage.pageParam.pageNo = pageNo;
	}
	// 如果页码小于等于0，则不进行分页查找
	if (userNoteMySendListPage.pageParam.pageNo <= 0) {
		return;
	}

	if (userNoteMySendListPage.pageParam.queryMethodParam == 'findUserMySendNotesByQueryParam') {
		userNoteMySendListPage.findUserMySendNotesByQueryParam();
	} else {
		alert("类型对应的查找方法未找到.");
	}
};