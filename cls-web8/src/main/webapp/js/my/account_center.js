function UserInfoSavePage() {
}
var userInfoSavePage = new UserInfoSavePage();

userInfoSavePage.param = {
	toFixedValue : 3,
	isToGetHeaderMsg : true
// 默认马上去加载头部数据
};

$(document).ready(function() {
	// 当前链接位置标注选中样式
	$("#user_info_save").addClass("selected");
	$("div.lists").find("div.child:eq(0)").addClass("on");
	userInfoSavePage.initPage();

	$("#editLoginPass").click(function() {
		userInfoSavePage.trunToUpdatePage();
	});
	$("#editLoginPass2").click(function() {
		userInfoSavePage.trunToUpdatePasswordPage();
	});
	$("#setSavePass").click(function() {
		userInfoSavePage.trunToSetSavePassPage();
	});
	$("#editSavePass").click(function() {
		userInfoSavePage.trunToUpdateSecuritypasswordPage();
	});
	$("#setSaveQuestion").click(function() {
		userInfoSavePage.trunToSetSaveQuestionPage();
	});
	$("#editSaveQuestion").click(function() {
		userInfoSavePage.trunToUpdateSaveQuestionPage();
	});
	// can = document.getElementById("fraction-canvas").getContext("2d");
});

/**
 * 初始化页面
 */
UserInfoSavePage.prototype.initPage = function() {
	if (currentUser != "{}") {
		if (currentUser.haveSafePassword > 0) {
			$("#setSavePass").hide();
			$("#editSavePass").show();
			$("#editSavePass").addClass("blue1");
			$("#editSavePass").text("修改安全密码");
		} else {
			$("#editSavePass").hide();
			$("#setSavePass").show();
			$("#setSavePass").addClass("red");
		}

		if (currentUser.haveSafeQuestion > 0) {
			$("#editSaveQuestion").show();
			$("#setSaveQuestion").hide();
		}

		if (currentUser.haveSafePassword == 1 && (currentUser.haveSafeQuestion == 0 || currentUser.haveSafeQuestion == null)) {
			// setQUAN(0.5);
			$("#safeLevel").html("<font class='cp-yellow'>温馨提示：您的账号安全级别为中，请完善安全信息。</font>");
		} else if ((currentUser.haveSafePassword == 0 || currentUser.haveSafePassword == null) && currentUser.haveSafeQuestion > 0) { // 无安全密码
			// 有安全问题
			$("#safeLevel").html("<font class='cp-yellow'>温馨提示：您的账号安全级别为中，请完善安全信息。</font>");
		} else if (currentUser.haveSafePassword == 1 && currentUser.haveSafeQuestion > 0) { // 有安全密码
			// 有安全问题
			$("#safeLevel").html("<font class='cp-green'>温馨提示：您的账号安全级别为高。</font>");
		} else {
			$("#safeLevel").html("<font class='cp-red'>温馨提示：您的账号安全级别为低，请完善安全信息。</font>");
		}

	}

	// 获取用户登录IP等相关信息.
	$.ajax({
		type : "POST",
		url : contextPath + "/user/getLastIPAddress",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var lastLogins = result.data;
				if (lastLogins != null && lastLogins.length == 1) { // 第一次登录
					$("#lastLoginTime").text("第一次登录: " + lastLogins[0].logtimeStr + "   " + lastLogins[0].address);
				} else if (lastLogins != null && lastLogins.length == 2) {
					$("#lastLoginTime").text("上次登录: " + lastLogins[1].logtimeStr + "   " + lastLogins[1].address);
				} else {
					frontCommonPage.showKindlyReminder("登录日志记录不正常.");
				}
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});

	// 获取用户返点.
	$.ajax({
		type : "POST",
		url : contextPath + "/user/getUserDefection",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var resultData = result.data;
				$("#lotteryRebate").text(resultData.lotteryRebate);
				$("#realityPersonRebate").text(resultData.realityPersonRebate);
				$("#electronicsRebate").text(resultData.electronicsRebate);
				$("#gamecockRebate").text(resultData.gamecockRebate);
				$("#sportRebate").text(resultData.sportRebate);
				$("#chessRebate").text(resultData.chessRebate);
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});

	/**
	 * 刷新用户余额
	 */

	$.ajax({
		type : "POST",
		url : contextPath + "/user/getCurrentUser",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var resultData = result.data;
				$("#currentUserMoney").text(resultData.money.toFixed(userInfoSavePage.param.toFixedValue));
				$("#canMention").text(resultData.canWithdrawMoney.toFixed(userInfoSavePage.param.toFixedValue))
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});

	// 统计用户的所有余额、分红、盈利等数据
	$.ajax({
		type : "POST",
		url : contextPath + "/user/getTotalUsersConsumeReport",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				var resultData = result.data;
				$("#userTotalRechargeMoney").text(resultData.totalRecharge.toFixed(userInfoSavePage.param.toFixedValue));
				$("#userTotalWithdrawMoney").text(resultData.totalWithdraw.toFixed(userInfoSavePage.param.toFixedValue))
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

function setQUAN(mun) {
	var aa = mun * 360;
	bb = 0;
	var interval = setInterval(function() {
		if (bb >= aa) {
			bb = aa;
			clearInterval(interval)
		}
		can.sector(90, 90, 90, 0, bb).fill();
		bb += 2
	}, 20)
}
/**
 * 跳转到修改密码页面
 */
UserInfoSavePage.prototype.trunToUpdatePasswordPage = function() {
	window.location.href = contextPath + "/gameBet/safeCenter/resetPwd.html";
};
/**
 * 跳转到设置安全密码页面
 */
UserInfoSavePage.prototype.trunToSetSavePassPage = function() {
	window.location.href = contextPath + "/gameBet/safeCenter/setSafePwd.html";
};

/**
 * 跳转到修改安全密码页面
 */
UserInfoSavePage.prototype.trunToUpdateSecuritypasswordPage = function() {
	window.location.href = contextPath + "/gameBet/safeCenter/resetSafePwd.html";
};

/**
 * 跳转到设置安全问题页面
 */
UserInfoSavePage.prototype.trunToSetSaveQuestionPage = function() {
	window.location.href = contextPath + "/gameBet/safeCenter/setSafeQuestion.html";
};
/**
 * 跳转到修改安全问题页面
 */
UserInfoSavePage.prototype.trunToUpdateSaveQuestionPage = function() {
	window.location.href = contextPath + "/gameBet/safeCenter/verifySafeQuestion.html";
};