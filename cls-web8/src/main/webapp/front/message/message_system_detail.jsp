<%@ page language="java" contentType="text/html; charset=UTF-8"
    import="com.team.lottery.enums.ENoteType"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
	String	id = request.getParameter("id");
%>    
<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>站内信</title>
<script src="<%=path%>/front/assets/jquery/jquery.js"></script>
<jsp:include page="/front/include/include.jsp"></jsp:include
>
<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script src="<%=path%>/front/js/user_base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<!-- CSS -->
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_note.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/js/message/message_system_detail.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<!-- 业务js -->
<%
 String noteId = request.getParameter("param1");  //获取查询的商品ID
%>
<script type="text/javascript">
userNoteDetailPage.param.noteId = <%=id %>;
/* headerPage.param.isToGetHeaderMsg = false; */
</script>

</head>
<body>

<jsp:include page="/front/include/header_user.jsp"></jsp:include>

<!-- m-banner -->
<jsp:include page="/front/include/header_nav.jsp"></jsp:include>

<!-- main -->
<div class="main main2"><div class="container">
	<div class="head">
    	<ul class="nav">
        	<a href="<%=path%>/gameBet/message/inbox.html"><li class="on">收件箱</li></a>
            <a href="<%=path%>/gameBet/message/outbox.html"><li>发件箱</li></a>
            <a href="<%=path%>/gameBet/announce.html"><li>公告</li></a>
        </ul>
        <a href="sendUp.html"><div class="nav-btn">编写消息</div></a>
    </div>
    <div class="siftings">
    	<div class="siftings-titles">
        	<p class="title" >主题：<span class="text" id='systemNoteSub'>***</span></p>
            <div class="infos">
            	<p class="send">发件人：<span>系统管理员</span></p>
                <p class="time" id='systemNoteDate'>时间：</p>
               <!--  <div class="btns">
                	<a href="user_msg2.html">回复</a><a href="#">删除</a>
                </div> -->
            </div>
        </div>
        <div class="siftings-content" >
        	<p id='systemNoteBody'>*************</p>
        </div>
    </div>

</div></div>
    
    <div class="reveal-modal-bg" id='shadeFloor' style="display:none;"></div>
    
<!-- online -->
<jsp:include page="/front/include/chat_common.jsp"></jsp:include>
<!-- fixed over -->
    <div class="reveal-modal-bg" id='shadeFloor' style="display:none;"></div>
</div>
<!--footer-->
<jsp:include page="/front/include/footer.jsp"></jsp:include>
</body>
</html>