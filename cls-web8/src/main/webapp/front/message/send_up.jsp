<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>站内信</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<script src="<%=path%>/front/assets/jquery/jquery.js"></script>
<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script src="<%=path%>/front/js/user_base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<!-- css -->
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_note.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />

<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/js/message/send_up.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
</head>
<body>

<jsp:include page="/front/include/header_user.jsp"></jsp:include>

<!-- m-banner -->
<jsp:include page="/front/include/header_nav.jsp"></jsp:include>

<!-- main 给上级发信-->
<div class="main main3">
<div class="container">
	<div class="head" id='commonTab'>
    	<ul class="nav">
        	<a href="javascript:void(0)"><li class="on" data-role='sendToUp' data-value='to_up'>给上级发信</li></a>
            <a href="sendDown.html"><li data-role='sendToDown' data-value='to_down'>给下级发信</li></a>
        </ul>
        <a href="inbox.html"><div class="nav-btn">返回收件箱</div></a>
    </div>
    <div class="siftings">
    	<div class="siftings-titles">
        	<div class="line line1">
                <div class="line-title">收件人：</div>上级
                <!-- <div class="line-content">
                
                    <div class="line-text"><input type="text" class="inputText" /></div>
                </div> -->
            </div>
            <div class="line line2">
                <div class="line-title">主题：</div>
                <div class="line-content">
                    <div class="line-text">
                    	<input type="text" class="inputText" id='toUpSub' value="" name="tel" maxlength="50"/>
                    </div>
                </div>
                <div class="line-msg" id='toUpMsg'><p>必须填写,最多60字符</p></div>
            </div>
        </div>
        
        <div class="siftings-send">
        	<p class="title">正文:</p>
            <div class="send-text"><textarea class="textArea" id="toUpBody" ></textarea></div>
            <div class="form-tip" id='toUpMsgBody'><p>必须填写,最多500字符</p></div>
        </div>
        <div class="siftings-btns">
        	<!-- <input type="button" class="btn gray clear-btn" value="清除" />  -->
            <input type="button" class="btn blue submit-btn" id='toUpSend' value="发送" /> 
        </div>
    </div>
</div></div>
<!-- main over -->
<!--footer-->
<jsp:include page="/front/include/footer.jsp"></jsp:include>
<div id="addToUserDialog" class="reveal-modal reveal-modal-c3" style="opacity:1; margin-top:-140px; visibility: visible; display:none;z-index:150;">
    <div class="reveal-modal-wapper cp-current-mode clearfix">
        <div class="reveal-modal-header">
            <div class="reveal-modal-header-title">添加收件人</div>
            <a class="close-reveal-modal" title="关闭">关闭</a>
        </div>
        <div class="current-mode-slider ">
            <div class="general-form note-send clearfix">
                <div class="form-bd">
                    <div class="form-item">
                        <label class="form-item-hd">搜索用户：</label>
                        <div class="form-item-bd">
                            <input type="text" class="input w" id="searchUserName" maxlength="50">
                            <button type="button" class="a-btn general-btn" id='searchUserButton'>搜索</button>
                        </div>
                    </div>
                    <div class="form-item">
                        <label class="form-item-hd">用户列表：</label>
                        <div class="form-item-bd" style="width: 300px;">
	                        <div class="panel-select">
		                        <ul class="userlist" id='userList'>
		                            <!-- <li>
		                                <input name="" id="s1" type="checkbox" value="">
		                                <label for="s1" style="vertical-align:middle">nolay0</label>
		                            </li> -->
		                        </ul>
		                    </div>
		                </div>
                    </div>
                    <div class="form-item">
                        <label class="form-item-hd">选择：</label>
                        <div class="form-item-bd">
                            <input type="radio" id='userNameSearchRadioName1' name='userNameSearchRadioName1' data-type='choose-all'>
                            <label for="userNameSearchRadioName1" style="vertical-align:middle">全选</label>
                            &nbsp;&nbsp;
                            <input type="radio" id='userNameSearchRadioName2' name='userNameSearchRadioName2' data-type='cancel-choose-all'>
                            <label for="userNameSearchRadioName2" style="vertical-align:middle">反选</label>
                        </div>
                    </div>
                    <div class="form-item">
                        <label class="form-item-hd"></label>
                        <div class="form-item-bd">
                            <a href="javascript:void(0);" class="a-btn general-btn" id='addUserButton'>添加</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="reveal-modal-bg" id='shadeFloor' style="display:none;"></div>
<!-- online -->
<jsp:include page="/front/include/chat_common.jsp"></jsp:include>
<!-- fixed over -->
    <div class="reveal-modal-bg" id='shadeFloor' style="display:none;"></div>
</body>
</html>
	