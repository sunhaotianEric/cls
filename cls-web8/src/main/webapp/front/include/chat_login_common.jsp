<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
%>

<!-- fixed-custom -->
<div class="fixed-custom" onClick="frontCommonPage.showCustomService()"><img src="<%=path%>/front/images/login/custom_icon.png" /></div>
<!-- fixed-custom over -->

<!-- online -->
<div class="online-bg"></div>
<div class="online">
	<div class="online-element">
    	<div class="head"><img src="<%=path%>/front/images/online/t1.png" /></div>
        <div class="content">
        	<ul>
            	<li>
                	<div class="headimg"><img src="<%=path%>/front/images/online/custom1.png" /></div>
                    <p class="title">在线客服</p><img class="pointer" src="<%=path%>/front/images/online/custom_pointer.png" />
                <div class="sub"></div></li>
                <li>
                	<div class="headimg"><img src="<%=path%>/front/images/online/custom2.png" /></div>
                    <p class="title">上级</p><img class="pointer" src="<%=path%>/front/images/online/custom_pointer.png" />
                <div class="sub"></div></li>
                <li class="no">
                	<div class="headimg"><img src="<%=path%>/front/images/online/custom2.png" /></div>
                    <p class="title">下级</p><img class="pointer" src="<%=path%>/front/images/online/custom_pointer.png" />
                <div class="sub"></div></li>
            </ul>
        </div>
    </div>
    <div class="online-container">
    	<div class="head">
        	<div class="headimg"><img src="<%=path%>/front/images/online/custom1.png" /></div>
            <p class="title">在线客服<span>(在线)</span></p>
            <img class="o_btn sub_btn" src="<%=path%>/front/images/online/custom_sub.png" onClick="online_hide()" />
            <img class="o_btn close_btn" src="<%=path%>/front/images/online/custom_close.png" onClick="online_hide()" />
        </div>
        <div class="online-window">
        	<ul>
            	<!-- 我的记录 -->
            	<li class="right">
                	<div class="headimg"><img src="<%=path%>/front/images/online/custom3.png" /></div>
                    <div class="li-content">
                    	<p class="title">我 (iceart2016)</p>
                        <div class="info">
                        	<p>你好，我有问题需要解决～</p>
                        </div>
                    </div>
                </li>
                <!-- 我的记录 -->
                <!-- 客服 -->
                <li class="left">
                	<div class="headimg"><img src="<%=path%>/front/images/online/custom1.png" /></div>
                    <div class="li-content">
                    	<p class="title">在线客服 </p>
                        <div class="info">
                        	<p>你好，有什么可以帮你的？</p>
                        </div>
                    </div>
                </li>
                <li class="left">
                	<div class="headimg"><img src="<%=path%>/front/images/online/custom1.png" /></div>
                    <div class="li-content">
                    	<p class="title">在线客服 </p>
                        <div class="info">
                        	<p>你好，还想要帮助吗？韩冰云！～</p>
                        </div>
                    </div>
                </li>
                <!-- 客服 -->
            </ul>
        </div>
        <div class="foot">
        	<div class="send1">
            	<img src="<%=path%>/front/images/online/i.png" /><span>聊天记录</span>
            </div>
            <div class="send">
            	<div class="send-editor"><textarea></textarea></div>
                <div class="send-btn">发送</div>
            </div>
        </div>
    </div>
</div>
<!-- online over --> 
<script type="text/javascript">
  $(".online .online-container .online-window").mCustomScrollbar();
</script>
