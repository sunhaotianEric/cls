<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();

%>
<div class="alert-msg-bg"></div>

<div id="J_androidPopup" class="popup-android popup" style="z-index: 99999; position: fixed; top:  50%;margin:0 auto;margin-top:-236px; left: 0;right:0px;">
    <!-- <div class="clearfix"> -->
        <div class="box">
            <p class="title">手机扫码安装</p>
            <p class="tips">推荐使用UC浏览器或其他二维码软件扫码</p>
            <p class="qrc"><img id="appBarCode" src="<%=path%>/front/app/qrcode/qrcode.png"></p>
            <p class="tips"><span style="color: red;">温馨提醒</span>：使用微信扫一扫，可能无法正常打开，</p>
            <p class="tips">请使用其他方式扫码</p>
        </div>
       <!--  <div class="box">
            <p class="title"><strong>方法2</strong>手机扫码安装</p>
            <p>请选择以下下载地址：</p>
            <p><a href="http://www.bangbbs.com/download.aspx?id=40199&amp;filetype=apk" target="_blank">下载地址1</a></p>
            <p>手机连接电脑，打开同步软件安装</p>
            <p>（下载<a href="#" target="_blank">91助手</a>, <a href="#" target="_blank">PP助手</a>）</p>
        </div> -->
    <!-- </div> -->
 <!--    <div class="help">
        <p>安装有问题？<a href="#" target="_blank">更多帮助 &gt;</a></p>
    </div> -->
    <i hook="close" class="close"></i>
</div>


<div id="J_iphonePopup" class="popup-iphone popup" style="z-index: 99999; position: fixed; top: 50%;margin:0 auto;margin-top:-236px; left: 0;right:0px;">
    <div class="box">
        <p class="title">手机扫码安装</p>
        <p class="tips">使用微信进行扫码安装</p>
        <p class="qrc"><img id="appLeBarCode" src="<%=path%>/front/app/qrcode/qrcode.png"></p>
        <p class="tips">点击右上角在safari中打开，在新页面点击安装</p>
    </div>
    <div class="help">
        <p><em>温馨提示：</em>IOS 安装后运行会出现“未受信任的企业级开发者”，解决办法“设置——通用——描述文件与设备管理——选择企业级应用下的证书——信任”。</p>
        <!-- <p><a href="#" target="_blank">点击查看图文解说</a></p> -->
    </div>
    <i hook="close" class="close"></i>
</div>


      <!-- other -->
    <div class="other">

        <div class="child recharge" style="width:230px">
            <h2 class="child-title">技术支持</h2>
            <div class="child-content">
               <a href="http://www.boss166.com" target="_blank"> <img src="<%=path%>/front/images/index/jishuzhichi.png"/></a>
            </div>
        </div>
        <!-- consumtime -->
        <div class="child consumtime" style="width:255px;margin:0 17px;">
            <h2 class="child-title">服务体验</h2>
            <div class="child-content">
                <div class="process" id="czdz_process">
                    <h3 class="process-title">昨日充值到账平均时间</h3>
                    <div class="process-content" style="width:215px">
                        <div class="process-ing process-yellow" data-process="60" style="width: 51%;"></div>
                    </div>
                    <h3 class="process-second" style="left:225px" ><span>10</span>秒</h3>
                </div>
                <div class="process" id="txdz_process">
                    <h3 class="process-title">昨日提现到账平均时间</h3>
                    <div class="process-content" style="width:215px">
                        <div class="process-ing process-blue" data-process="80" style="width: 61%;"></div>
                    </div>
                    <h3 class="process-second" style="left:225px"><span>5</span>秒</h3>
                </div>
            </div>
        </div>
        <!-- consumtime over -->
        
        <!-- recharge -->
        <div class="child recharge" style="width:225px">
            <h2 class="child-title">充值方式</h2>
            <div class="child-content">
                <a href="#"><div class="recharge-child">
                    <div class="icon"><img src="<%=path%>/front/images/index/recharge_wechat.png" alt=""></div>
                    <h3 class="title">微信支付</h3>
                </div></a>
                <a href="#"><div class="recharge-child">
                    <div class="icon"><img src="<%=path%>/front/images/index/recharge_alipay.png" alt=""></div>
                    <h3 class="title">支付宝</h3>
                </div></a>
                <a href="#"><div class="recharge-child">
                    <div class="icon"><img src="<%=path%>/front/images/index/recharge_yinlian.png" alt=""></div>
                    <h3 class="title">银联</h3>
                </div></a>
            </div>
        </div>
        <!-- recharge over -->

        <!-- download -->
        <div class="child download" id="phonecodediv" style="width:300px">
            <h2 class="child-title">手机下载</h2>
            <div class="child-content">
                <div class="download-child download-child-android" style="width:150px" >
                    <div class="download-child-img" ><img id="downloadAndroid" onclick="appLoadShow('android')" src="<%=path%>/front/images/index/download-android.png" alt=""></div>
                    <h3 class="download-child-title">支持Android2.2或以上</h3>
                </div>
                <div class="download-child download-child-ios" style="width:150px">
                    <div class="download-child-img"><img id="downloadIos" onclick="appLoadShow('iphone')" src="<%=path%>/front/images/index/download-ios.png" alt=""></div>
                    <h3 class="download-child-title">支持IOS4.3或以上</h3>
                </div>
            </div>
        </div>
        <!-- download over -->
        
        <!-- wapcode -->
        <div class="child wapcode" id="wapcodediv" style="display: none;">
            <h2 class="child-title">移动版二维码</h2>
            <div class="child-content">
                <div class="btn code-btn">
                    <p class="btn-title">扫描二维码</p>
                    <div class="code">
                        <img id="wapcode" class="img" src="<%=path%>/front/images/wap/code.jpg" alt="">
                        <p class="title">扫码打开手机版</p>
                        <img class="pointer" src="<%=path%>/front/images/wap/pointer.png" alt="">
                    </div>
                </div>
               
            </div>
            <!-- <div class="child-content">
                <img src="images/wap/code.jpg" alt="">
            </div> -->
        </div>
        <!-- wapcode over -->



    </div>
    <!-- other over -->
<script type="text/javascript">
 /**
 * 服务体验效果JS
 * @param id ‘#id’
 * @param process_second 时间 
 * @param process_maxSecond 最大值 ，一般设为15秒 ,
 */
function showProcess(id,process_second,process_maxSecond)
{
		var maxSecond=parseInt(process_maxSecond),
   		    proc=parseInt(process_second),
			dirct=1,
			procing=0,
		interval=setInterval(function(e,proc){
			percent = procing/maxSecond*100; 
			percent = percent.toFixed(0);
			$(id).find(".process-ing").css("width",percent+"%");
			$(id).find(".process-second span").html(procing-1);
			if(procing>=maxSecond){
				dirct=-1;
			}
			procing+=dirct;
			if(procing<=proc&&dirct==-1)clearInterval(interval)
		},500/process_maxSecond,$(this),proc);
}
/**
 * 二维码显示
 */
 function howIndexLotteryKind(){
	 $.ajax({
			type: "POST",
	        url: contextPath +"/system/getBizSystemInfo",
	        contentType :"application/json;charset=UTF-8",
	        success: function(result){
	        	commonHandlerResult(result, this.url);
	        	var data = result.data;
	        	if (result.code == "ok") {
	    			if(data.indexLotteryKind!=null&&data.indexLotteryKind!=""){
	    				
	    				if(data.hasApp==1){
	    					$("#phonecodediv").show();
	    					$("#wapcodediv").hide();
	    					//没有安卓二维码，直接下载
	    					if(data.appBarCode !=null &&  data.appBarCode != ""){
	    						$("#downloadAndroid").attr('onclick',"appLoadShow('android')");
	    						$("#appBarCode").attr('src',data.appBarCode);
	    					}else{
	    						$("#downloadAndroid").attr('onclick',"javascript:window.open('"+data.appAndroidDownloadUrl+"')");
	    					}
	    					//没有IOS二维码，直接下载
	    					if(data.appIosBarcode !=null &&  data.appIosBarcode != ""){
	    						$("#downloadIos").attr('onclick',"appLoadShow('iphone')");
	    						$("#appLeBarCode").attr('src',data.appIosBarcode);
	    					}else{
	    						var appStoreIosUrl = data.appStoreIosUrl;
	    						if(appStoreIosUrl.indexOf("itms-apps")!=-1){
	    							appStoreIosUrl = appStoreIosUrl.replace("itms-apps","https");
	    						}
	    						$("#downloadIos").attr('onclick',"javascript:window.open('"+appStoreIosUrl+"')");
	    					}
	    				}else{
	    					$("#phonecodediv").hide();
	    					$("#wapcodediv").show();
	    					$("#wapcode").attr('src',data.mobileBarcode);
	    				}
	    				
	    			};
	    	
	    			
	    		}else if(result.code == "error"){
	    			frontCommonPage.showKindlyReminder(data);
	    		}else{
	    			showErrorDlg(jQuery(document.body), "获取最新二维码请求失败.");
	    		}
	        }
		});
	/* frontLotteryCodeAction.getBizSystemInfo(function(r){
		if (r[0] != null && r[0] == "ok") {
			if(r[1].indexLotteryKind!=null&&r[1].indexLotteryKind!=""){
				
				if(r[1].hasApp==1){
					$("#phonecodediv").show();
					$("#wapcodediv").hide();
					//没有安卓二维码，直接下载
					if(r[1].appBarCode !=null &&  r[1].appBarCode != ""){
						$("#downloadAndroid").attr('onclick',"appLoadShow('android')");
						$("#appBarCode").attr('src',r[1].appBarCode);
					}else{
						$("#downloadAndroid").attr('onclick',"javascript:window.open('"+r[1].appAndroidDownloadUrl+"')");
					}
					//没有IOS二维码，直接下载
					if(r[1].appIosBarcode !=null &&  r[1].appIosBarcode != ""){
						$("#downloadIos").attr('onclick',"appLoadShow('iphone')");
						$("#appLeBarCode").attr('src',r[1].appIosBarcode);
					}else{
						var appStoreIosUrl = r[1].appStoreIosUrl;
						if(appStoreIosUrl.indexOf("itms-apps")!=-1){
							appStoreIosUrl = appStoreIosUrl.replace("itms-apps","https");
						}
						$("#downloadIos").attr('onclick',"javascript:window.open('"+appStoreIosUrl+"')");
					}
				}else{
					$("#phonecodediv").hide();
					$("#wapcodediv").show();
					$("#wapcode").attr('src',r[1].mobileBarcode);
				}
				
			};
	
			
		}else if(r[0] != null && r[0] == "error"){
			frontCommonPage.showKindlyReminder(r[1]);
		}else{
			showErrorDlg(jQuery(document.body), "获取最新二维码请求失败.");
		}
    });
 */
}; 

function appLoadShow(type){
	if(type == 'android'){
		$('.popup-android').show();
		$('.popup-iphone').hide();	
		$(".alert-msg-bg").stop(false,true).fadeIn(500);
	}else{
		$('.popup-iphone').show();
		$('.popup-android').hide();	
		$(".alert-msg-bg").stop(false,true).fadeIn(500);
	}
}

$(function(){
	$(".popup .close").click(function(){
		$(".popup").fadeOut(500);
		$(".alert-msg-bg").stop(false,true).fadeOut(500);
	});
	howIndexLotteryKind();
})

//服务体验,recharge_second,withdraw_second在head_user.jsp那边
showProcess("#czdz_process",recharge_second,15);
showProcess("#txdz_process",withdraw_second,15);
//ShowIndexLotteryKind();
</script>   
    