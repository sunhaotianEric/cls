<%@ page language="java" contentType="text/html; charset=UTF-8" 
  import="com.team.lottery.system.BizSystemConfigVO,com.team.lottery.system.BizSystemConfigManager,java.util.*,
         com.team.lottery.vo.LotterySet,com.team.lottery.cache.ClsCacheManager,
         com.team.lottery.system.BizSystemConfigManager,com.team.lottery.vo.BizSystemInfo,
         com.team.lottery.util.LotterySetUntil,com.team.lottery.extvo.LotterySetMsg,
         com.team.lottery.util.LotterySwitchUtil,
         java.util.List,com.team.lottery.vo.LotterySwitch,
         net.sf.json.JSONArray"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
    String serverName=  request.getServerName();//获取当前域名
    String bizSystem=ClsCacheManager.getBizSystemByDomainUrl(serverName);
    BizSystemConfigVO bizSystemConfigVO=BizSystemConfigManager.getBizSystemConfig(bizSystem);//业务系统彩种设置
    BizSystemInfo bizSystemInfo=ClsCacheManager.getBizSystemInfo(bizSystem);//热门彩种设置，首页彩种设置
    LotterySet lotterySet=new LotterySet();
    LotterySetUntil lotterySetUntil=new LotterySetUntil();
    //热门彩种
    LotterySetMsg lotterySetMsgHot=lotterySetUntil.compareLotterySetALL(lotterySet, bizSystemConfigVO, bizSystemInfo);
    //全部彩种的
    LotterySetMsg lotterySetMsgAll=lotterySetUntil.compareLotterySetAndBizSystemConfigVO(lotterySet, bizSystemConfigVO);
    /* request.setAttribute("lotterySetMsgHot", lotterySetMsgHot);
    request.setAttribute("lotterySetMsgAll", lotterySetMsgAll); */
    if(bizSystemInfo!=null&&bizSystemInfo.getHotLotteryKinds()!=null){
     request.setAttribute("kinds", bizSystemInfo.getHotLotteryKinds().split(","));
    }else{
    	request.setAttribute("kinds", "");
    }
 	// 获取处理过后的热门彩种.
    List<LotterySwitch> hotLotterySwitchList = LotterySwitchUtil.getHotLotterySwitchByBizSystem(bizSystem);
    List<LotterySwitch> lotterySwitchListBySystem = ClsCacheManager.getLotterySwitchsStatusByBizSystem(bizSystem);
 	// 将数据转换成JSON格式的数据.
 	JSONArray hotLotterySwitchs = JSONArray.fromObject(hotLotterySwitchList);
 	String hotLotterySwitchsJson = hotLotterySwitchs.toString();
 	// 获取当前系统的所有彩种开关状态.
 	request.setAttribute("hotLotterySwitchList", hotLotterySwitchList);
 	request.setAttribute("lotterySwitchListBySystem", lotterySwitchListBySystem);
    String hotLotteryKinds="";
    
    Map<String,String> lotterykindsMap = ClsCacheManager.getOpenLotteryKinds(bizSystem);
    //业务系统热门彩种
    for(int i=0;i<bizSystemInfo.getHotLotteryKinds().split(",").length;i++){
    	String hotKind = bizSystemInfo.getHotLotteryKinds().split(",")[i];
    	String bizSystemlotterykindConfig = lotterykindsMap.get(hotKind);
    	if(bizSystemlotterykindConfig != null){
	    	hotLotteryKinds += bizSystemlotterykindConfig+",";
    		
    	}
    }
 %>
<script>
	var hotLotterySwitchs = <%=hotLotterySwitchsJson%>;
	var xs='<%=hotLotteryKinds%>';
</script>
<div class="main" style="margin-left:0">
    <!-- main-content -->
    <div class="main-nav mCustomScrollbar _mCS_1 animated" style="animation-name: fadeInLeft;animation-delay: 0s;">
        <div class="child child-change">
            <div class="icon icon-tool"></div>
            <h2>购彩大厅</h2>
            <!-- <div class="after" onclick="transformNav()"></div> -->
        </div>
        <div class="nav">

            <div class="child hasNav child-title">
                <div class="icon icon-hot"></div>
                <h2>热门彩票</h2>
                <div class="pointer icon-pointer"></div>
            </div>
            <ul class="nav-child">
           		<c:forEach var="hotLotterySwitch" items="${hotLotterySwitchList}">
            	<c:if test="${hotLotterySwitch.lotteryType == 'CQSSC'}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/cqssc/cqssc.html','')">
                    <div class="icon icon-shishicai-small"></div>
                    <h2>重庆时时彩</h2>
                </div>
                </c:if>
                
                <c:if test="${hotLotterySwitch.lotteryType == 'TJSSC'}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/tjssc/tjssc.html','')">
                    <div class="icon icon-shishicai-small"></div>
                    <h2>天津时时彩</h2>
                </div>
                </c:if>
                
                <c:if test="${hotLotterySwitch.lotteryType == 'JXSSC'}">
                <div class="child" onclick="javascript:void(0);">
                    <div class="icon icon-shishicai-small"></div>
                    <h2>江西时时彩</h2>
                </div>
                </c:if>
                
                <c:if test="${hotLotterySwitch.lotteryType == 'HLJSSC'}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/hljssc/hljssc.html','')">
                    <div class="icon icon-shishicai-small"></div>
                    <h2>黑龙江时时彩</h2>
                </div>
                </c:if>
                
                <c:if test="${hotLotterySwitch.lotteryType == 'XJSSC'}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/xjssc/xjssc.html','')">
                    <div class="icon icon-shishicai-small"></div>
                    <h2>新疆时时彩</h2>
                </div>
                </c:if>

                <c:if test="${hotLotterySwitch.lotteryType == 'LCQSSC'}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/lcqssc/lcqssc.html','')">
                    <div class="icon icon-shishicai-small"></div>
                    <h2>老重庆时时彩</h2>
                </div>
                </c:if>

                <c:if test="${hotLotterySwitch.lotteryType == 'JSSSC'}">
                    <div class="child"
                         onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jsssc/jsssc.html','')">
                        <div class="icon icon-shishicai-small"></div>
                        <h2>江苏时时彩</h2>
                    </div>
                </c:if>

                <c:if test="${hotLotterySwitch.lotteryType == 'BJSSC'}">
                    <div class="child"
                         onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/bjssc/bjssc.html','')">
                        <div class="icon icon-shishicai-small"></div>
                        <h2>北京时时彩</h2>
                    </div>
                </c:if>

                <c:if test="${hotLotterySwitch.lotteryType == 'GDSSC'}">
                    <div class="child"
                         onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/gdssc/gdssc.html','')">
                        <div class="icon icon-shishicai-small"></div>
                        <h2>广东时时彩</h2>
                    </div>
                </c:if>

                <c:if test="${hotLotterySwitch.lotteryType == 'SCSSC'}">
                    <div class="child"
                         onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/scssc/scssc.html','')">
                        <div class="icon icon-shishicai-small"></div>
                        <h2>四川时时彩</h2>
                    </div>
                </c:if>

                <c:if test="${hotLotterySwitch.lotteryType == 'SHSSC'}">
                    <div class="child"
                         onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/shssc/shssc.html','')">
                        <div class="icon icon-shishicai-small"></div>
                        <h2>上海时时彩</h2>
                    </div>
                </c:if>

                <c:if test="${hotLotterySwitch.lotteryType == 'SDSSC'}">
                    <div class="child"
                         onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/sdssc/sdssc.html','')">
                        <div class="icon icon-shishicai-small"></div>
                        <h2>山东时时彩</h2>
                    </div>
                </c:if>
                
                <c:if test="${hotLotterySwitch.lotteryType == 'SDSYXW'}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/sdsyxw/sdsyxw.html','')">
                    <div class="icon icon-11xuan5-small"></div>
                    <h2>山东11选5</h2>
                </div>
                </c:if>
                
                <c:if test="${hotLotterySwitch.lotteryType == 'GDSYXW'}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/gdsyxw/gdsyxw.html','')">
                    <div class="icon icon-11xuan5-small"></div>
                    <h2>广东11选5</h2>
                </div>
                </c:if>
                
                <c:if test="${hotLotterySwitch.lotteryType == 'JXSYXW'}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jxsyxw/jxsyxw.html','')">
                    <div class="icon icon-11xuan5-small"></div>
                    <h2>江西11选5</h2>
                </div>
                </c:if>
                
                <c:if test="${hotLotterySwitch.lotteryType == 'CQSYXW'}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/cqsyxw/cqsyxw.html','')">
                    <div class="icon icon-11xuan5-small"></div>
                    <h2>重庆11选5</h2>
                </div>
                </c:if>
                
                <c:if test="${hotLotterySwitch.lotteryType == 'WFSYXW'}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/wfsyxw/wfsyxw.html','')">
                    <div class="icon icon-11xuan5-small"></div>
                    <h2>五分11选5</h2>
                </div>
                </c:if>
                
                 <c:if test="${hotLotterySwitch.lotteryType == 'SFSYXW'}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/sfsyxw/sfsyxw.html','')">
                    <div class="icon icon-11xuan5-small"></div>
                    <h2>三分11选5</h2>
                </div>
                </c:if>
                
                <c:if test="${hotLotterySwitch.lotteryType == 'FJSYXW'}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/fjsyxw/fjsyxw.html','')">
                    <div class="icon icon-11xuan5-small"></div>
                    <h2>福建11选5</h2>
                </div>
                </c:if>
                
                <c:if test="${hotLotterySwitch.lotteryType == 'FCSDDPC'}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/fcsddpc/fcsddpc.html','')">
                    <div class="icon icon-3d-small"></div>
                    <h2>福彩3D</h2>
                </div>
                </c:if>
                <c:if test="${hotLotterySwitch.lotteryType == 'LHC'}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/lhc/lhc.html','')">
                    <div class="icon icon-liuhecai-small"></div>
                    <h2>六合彩</h2>
                </div>
                </c:if>
                <c:if test="${hotLotterySwitch.lotteryType == 'XYLHC'}">
                <div class="child hot" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/xylhc/xylhc.html','')">
                    <div class="icon icon-xyliuhecai-small"></div>
                    <h2>幸运六合彩</h2>
                </div>
                </c:if>
                <c:if test="${hotLotterySwitch.lotteryType == 'JSKS'}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jsks/jsks.html','')">
                    <div class="icon icon-kuai3-small"></div>
                    <h2>江苏快三</h2>
                </div>
                </c:if>
                <c:if test="${hotLotterySwitch.lotteryType == 'AHKS'}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/ahks/ahks.html','')">
                    <div class="icon icon-kuai3-small"></div>
                    <h2>安徽快三</h2>
                </div>
                </c:if>
                
                <c:if test="${hotLotterySwitch.lotteryType == 'JLKS'}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jlks/jlks.html','')">
                    <div class="icon icon-kuai3-small"></div>
                    <h2>吉林快三</h2>
                </div>
                </c:if>
                
                <c:if test="${hotLotterySwitch.lotteryType == 'HBKS'}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/hbks/hbks.html','')">
                    <div class="icon icon-kuai3-small"></div>
                    <h2>湖北快三</h2>
                </div>
                </c:if>
                
                <c:if test="${hotLotterySwitch.lotteryType == 'BJKS'}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/bjks/bjks.html','')">
                    <div class="icon icon-kuai3-small"></div>
                    <h2>北京快三</h2>
                </div>
                </c:if>
                
                <c:if test="${hotLotterySwitch.lotteryType == 'JYKS'}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jyks/jyks.html','')">
                    <div class="icon icon-kuai3-small"></div>
                    <h2>幸运快三</h2>
                </div>
                </c:if>
                <c:if test="${hotLotterySwitch.lotteryType == 'GSKS'}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/gsks/gsks.html','')">
                    <div class="icon icon-kuai3-small"></div>
                    <h2>甘肃快三</h2>
                </div>
                </c:if>
                <c:if test="${hotLotterySwitch.lotteryType == 'SHKS'}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/shks/shks.html','')">
                    <div class="icon icon-kuai3-small"></div>
                    <h2>上海快三</h2>
                </div>
                </c:if>
                <c:if test="${hotLotterySwitch.lotteryType == 'BJPK10'}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/bjpks/bjpk10.html','')">
                    <div class="icon icon-pk10-small"></div>
                    <h2>北京PK10</h2>
                </div>
                </c:if>
                <c:if test="${hotLotterySwitch.lotteryType == 'JSPK10'}">
                <div class="child hot" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jspks/jspk10.html','')">
                    <div class="icon icon-jspk10-small"></div>
                    <h2>极速PK10</h2>
                </div>
                </c:if>
                <c:if test="${hotLotterySwitch.lotteryType == 'XYFT'}">
                <div class="child hot" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/xyft/xyft.html','')">
                    <div class="icon icon-xyft-small"></div>
                    <h2>幸运飞艇</h2>
                </div>
                </c:if>
                <c:if test="${hotLotterySwitch.lotteryType == 'SFPK10'}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/sfpk10/sfpk10.html','')">
                    <div class="icon icon-PK10-common-small"></div>
                    <h2>三分PK10</h2>
                </div>
                </c:if>
                <c:if test="${hotLotterySwitch.lotteryType == 'WFPK10'}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/wfpk10/wfpk10.html','')">
                    <div class="icon icon-PK10-common-small"></div>
                    <h2>五分PK10</h2>
                </div>
                </c:if>
                <c:if test="${hotLotterySwitch.lotteryType == 'SHFPK10'}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/shfpk10/shfpk10.html','')">
                    <div class="icon icon-PK10-common-small"></div>
                    <h2>十分PK10</h2>
                </div>
                </c:if>
                 <c:if test="${hotLotterySwitch.lotteryType == 'GXKS'}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/gxks/gxks.html','')">
                    <div class="icon icon-kuai3-small"></div>
                    <h2>广西快三</h2>
                </div>
                </c:if>
                <c:if test="${hotLotterySwitch.lotteryType == 'XYEB'}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/xyeb/xy28.html','')">
                    <div class="icon icon-xy28-small"></div>
                    <h2>幸运28</h2>
                </div>
                </c:if>
                
                <c:if test="${hotLotterySwitch.lotteryType == 'JLFFC'}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jyffc/jyffc.html','')">
                    <div class="icon icon-fenfencai-small"></div>
                    <h2>幸运分分彩</h2>
                </div>
                </c:if>
                
                <c:if test="${hotLotterySwitch.lotteryType == 'TWWFC'}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/twwfc/twwfc.html','')">
                    <div class="icon icon-wufencai-small"></div>
                    <h2>台湾五分彩</h2>
                </div>
                </c:if>
                
                <c:if test="${hotLotterySwitch.lotteryType == 'HGFFC'}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/hgffc/hgffc.html','')">
                    <div class="icon icon-1_5fencai-small"></div>
                    <h2>韩国1.5分彩</h2>
                </div>
                </c:if>
                
                <c:if test="${hotLotterySwitch.lotteryType == 'XJPLFC'}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/xjplfc/xjplfc.html','')">
                    <div class="icon icon-erfencai-small"></div>
                    <h2>新加坡2分彩</h2>
                </div>
                </c:if>
                
                <c:if test="${hotLotterySwitch.lotteryType == 'SHFSSC'}">
                  <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/shfssc/shfssc.html','')">
                      <div class="icon icon-erfencai-small"></div>
                      <h2>十分时时彩</h2>
                  </div>
                 </c:if>
                <c:if test="${hotLotterySwitch.lotteryType == 'SFSSC'}">
                  <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/sfssc/sfssc.html','')">
                      <div class="icon icon-erfencai-small"></div>
                      <h2>三分时时彩</h2>
                  </div>
                 </c:if>
                 <c:if test="${hotLotterySwitch.lotteryType == 'WFSSC'}">
                  <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/wfssc/wfssc.html','')">
                      <div class="icon icon-erfencai-small"></div>
                      <h2>五分时时彩</h2>
                  </div>
                 </c:if>
                 <c:if test="${hotLotterySwitch.lotteryType == 'SFKS'}">
	                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/sfks/sfks.html','')">
	                    <div class="icon icon-kuai3-small"></div>
	                    <h2>三分快三</h2>
	                </div>
	                </c:if>
	                <c:if test="${hotLotterySwitch.lotteryType == 'WFKS'}">
	                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/wfks/wfks.html','')">
	                    <div class="icon icon-kuai3-small"></div>
	                    <h2>五分快三</h2>
	                </div>
	                </c:if>
                </c:forEach>
            </ul>


            <div class="child hasNav child-title hide">
                <div class="icon icon-gaopinlottery"></div>
                <h2>高频彩</h2>
                <div class="pointer icon-pointer"></div>
            </div>
            <ul class="nav-child">
            	
                <div class="child hasNav hide">
                    <div class="icon icon-shishicai-small"></div>
                    <h2>时时彩</h2>
                </div>
                <ul class="nav-child">
                	<c:forEach var="lotterySwitchBySystem" items="${lotterySwitchListBySystem}">
                	<c:choose>
                	<c:when test="${lotterySwitchBySystem.lotteryType == 'CQSSC' && lotterySwitchBySystem.lotteryStatus==1}">
	                    <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/cqssc/cqssc.html','')">
	                        <div class="icon icon-shishicai-small"></div>
	                        <h2>重庆时时彩</h2>
	                    </div>
                    </c:when>
                    <c:when test="${lotterySwitchBySystem.lotteryType == 'XJSSC' && lotterySwitchBySystem.lotteryStatus==1}">
	                    <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/xjssc/xjssc.html','')">
		                    <div class="icon icon-shishicai-small"></div>
		                    <h2>新疆时时彩</h2>
		                </div>
                    </c:when>
                    <c:when test="${lotterySwitchBySystem.lotteryType == 'TJSSC' && lotterySwitchBySystem.lotteryStatus==1}">
	                    <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/tjssc/tjssc.html','')">
		                    <div class="icon icon-shishicai-small"></div>
		                    <h2>天津时时彩</h2>
		                </div>
                    </c:when>
                    <c:when test="${lotterySwitchBySystem.lotteryType == 'LCQSSC' && lotterySwitchBySystem.lotteryStatus==1}">
	                    <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/lcqssc/lcqssc.html','')">
		                    <div class="icon icon-shishicai-small"></div>
		                    <h2>老重庆时时彩</h2>
		                </div>
                    </c:when>

                    <c:when test="${lotterySwitchBySystem.lotteryType == 'JSSSC' && lotterySwitchBySystem.lotteryStatus==1}">
                        <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jsssc/jsssc.html','')">
                            <div class="icon icon-shishicai-small"></div>
                            <h2>江苏时时彩</h2>
                        </div>
                    </c:when>

                    <c:when test="${lotterySwitchBySystem.lotteryType == 'BJSSC' && lotterySwitchBySystem.lotteryStatus==1}">
                        <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/bjssc/bjssc.html','')">
                            <div class="icon icon-shishicai-small"></div>
                            <h2>北京时时彩</h2>
                        </div>
                    </c:when>

                    <c:when test="${lotterySwitchBySystem.lotteryType == 'GDSSC' && lotterySwitchBySystem.lotteryStatus==1}">
                        <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/gdssc/gdssc.html','')">
                            <div class="icon icon-shishicai-small"></div>
                            <h2>广东时时彩</h2>
                        </div>
                    </c:when>

                    <c:when test="${lotterySwitchBySystem.lotteryType == 'SCSSC' && lotterySwitchBySystem.lotteryStatus==1}">
                        <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/scssc/scssc.html','')">
                            <div class="icon icon-shishicai-small"></div>
                            <h2>四川时时彩</h2>
                        </div>
                    </c:when>

                    <c:when test="${lotterySwitchBySystem.lotteryType == 'SHSSC' && lotterySwitchBySystem.lotteryStatus==1}">
                        <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/shssc/shssc.html','')">
                            <div class="icon icon-shishicai-small"></div>
                            <h2>上海时时彩</h2>
                        </div>
                    </c:when>

                    <c:when test="${lotterySwitchBySystem.lotteryType == 'SDSSC' && lotterySwitchBySystem.lotteryStatus==1}">
                        <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/sdssc/sdssc.html','')">
                            <div class="icon icon-shishicai-small"></div>
                            <h2>山东时时彩</h2>
                        </div>
                    </c:when>


                    <c:when test="${lotterySwitchBySystem.lotteryType == 'JXSSC' && lotterySwitchBySystem.lotteryStatus==1}">
	                    <div class="child" onclick="javascript:void(0);">
		                    <div class="icon icon-shishicai-small"></div>
		                    <h2>江西时时彩</h2>
		                </div>
                    </c:when>
                    </c:choose>
                    </c:forEach>
                </ul>
                <div class="child hasNav hide">
                    <div class="icon icon-fenfencai-small"></div>
                    <h2>分分彩</h2>
                </div>
                <ul class="nav-child">
                	<c:forEach var="lotterySwitchBySystem" items="${lotterySwitchListBySystem}">
                	<c:choose>
                	<c:when test="${lotterySwitchBySystem.lotteryType == 'JLFFC' && lotterySwitchBySystem.lotteryStatus==1}">
	                    <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jyffc/jyffc.html','')">
	                        <div class="icon icon-fenfencai-small"></div>
	                        <h2>幸运分分彩</h2>
	                    </div>
                    </c:when>
                    <c:when test="${lotterySwitchBySystem.lotteryType == 'TWWFC' && lotterySwitchBySystem.lotteryStatus==1}">
	                    <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/twwfc/twwfc.html','')">
	                        <div class="icon icon-wufencai-small"></div>
	                        <h2>台湾5分彩</h2>
	                    </div>
                    </c:when>
                    <c:when test="${lotterySwitchBySystem.lotteryType == 'HGFFC' && lotterySwitchBySystem.lotteryStatus==1}">
	                    <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/hgffc/hgffc.html','')">
	                        <div class="icon icon-1_5fencai-small"></div>
	                        <h2>韩国1.5分彩</h2>
	                    </div>
                    </c:when>
                    <c:when test="${lotterySwitchBySystem.lotteryType == 'XJPLFC' && lotterySwitchBySystem.lotteryStatus==1}">
	                    <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/xjplfc/xjplfc.html','')">
	                        <div class="icon icon-erfencai-small"></div>
	                        <h2>新加坡2分彩</h2>
	                    </div>
                    </c:when>
                    <c:when test="${lotterySwitchBySystem.lotteryType == 'SHFSSC' && lotterySwitchBySystem.lotteryStatus==1}">
	                    <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/shfssc/shfssc.html','')">
	                        <div class="icon icon-erfencai-small"></div>
	                        <h2>十分时时彩</h2>
	                    </div>
                    </c:when>
                    <c:when test="${lotterySwitchBySystem.lotteryType == 'SFSSC' && lotterySwitchBySystem.lotteryStatus==1}">
	                    <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/sfssc/sfssc.html','')">
	                        <div class="icon icon-erfencai-small"></div>
	                        <h2>三分时时彩</h2>
	                    </div>
                    </c:when>
                    <c:when test="${lotterySwitchBySystem.lotteryType == 'WFSSC' && lotterySwitchBySystem.lotteryStatus==1}">
	                    <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/wfssc/wfssc.html','')">
	                        <div class="icon icon-erfencai-small"></div>
	                        <h2>五分时时彩</h2>
	                    </div>
                    </c:when>
                    </c:choose>
                    </c:forEach>
                </ul>
                <div class="child hasNav hide">
                    <div class="icon icon-kuai3-small"></div>
                    <h2>快三</h2>
                </div>
                <ul class="nav-child">
                	<c:forEach var="lotterySwitchBySystem" items="${lotterySwitchListBySystem}">
                	<c:choose>
                	<c:when test="${lotterySwitchBySystem.lotteryType == 'JYKS' && lotterySwitchBySystem.lotteryStatus==1}">
	                    <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jyks/jyks.html','')">
	                        <div class="icon icon-kuai3-small"></div>
	                        <h2>幸运快三</h2>
	                    </div>
                    </c:when>
                    <c:when test="${lotterySwitchBySystem.lotteryType == 'AHKS' && lotterySwitchBySystem.lotteryStatus==1}">
	                    <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/ahks/ahks.html','')">
	                        <div class="icon icon-kuai3-small"></div>
	                        <h2>安徽快三</h2>
	                    </div>
                    </c:when>
                    <c:when test="${lotterySwitchBySystem.lotteryType == 'JSKS' && lotterySwitchBySystem.lotteryStatus==1}">
	                    <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jsks/jsks.html','')">
	                        <div class="icon icon-kuai3-small"></div>
	                        <h2>江苏快三</h2>
	                    </div>
                    </c:when>
                    <c:when test="${lotterySwitchBySystem.lotteryType == 'HBKS' && lotterySwitchBySystem.lotteryStatus==1}">
	                    <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/hbks/hbks.html','')">
	                        <div class="icon icon-kuai3-small"></div>
	                        <h2>湖北快三</h2>
	                    </div>
                    </c:when>
                    <c:when test="${lotterySwitchBySystem.lotteryType == 'BJKS' && lotterySwitchBySystem.lotteryStatus==1}">
	                    <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/bjks/bjks.html','')">
	                        <div class="icon icon-kuai3-small"></div>
	                        <h2>北京快三</h2>
	                    </div>
                    </c:when>
                    <c:when test="${lotterySwitchBySystem.lotteryType == 'JLKS' && lotterySwitchBySystem.lotteryStatus==1}">
	                    <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jlks/jlks.html','')">
	                        <div class="icon icon-kuai3-small"></div>
	                        <h2>吉林快三</h2>
	                    </div>
                    </c:when>
                    <c:when test="${lotterySwitchBySystem.lotteryType == 'GXKS' && lotterySwitchBySystem.lotteryStatus==1}">
	                    <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/gxks/gxks.html','')">
	                        <div class="icon icon-kuai3-small"></div>
	                        <h2>广西快三</h2>
	                    </div>
                    </c:when>
                    <c:when test="${lotterySwitchBySystem.lotteryType == 'GSKS' && lotterySwitchBySystem.lotteryStatus==1}">
                    <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/gsks/gsks.html','')">
	                    <div class="icon icon-kuai3-small"></div>
	                    <h2>甘肃快三</h2>
	                </div>
	                </c:when>
	                <c:when test="${lotterySwitchBySystem.lotteryType == 'SHKS' && lotterySwitchBySystem.lotteryStatus==1}">
	                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/shks/shks.html','')">
	                    <div class="icon icon-kuai3-small"></div>
	                    <h2>上海快三</h2>
	                </div>
	                </c:when>
	                <c:when test="${lotterySwitchBySystem.lotteryType == 'SFKS' && lotterySwitchBySystem.lotteryStatus==1}">
	                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/sfks/sfks.html','')">
	                    <div class="icon icon-kuai3-small"></div>
	                    <h2>三分快三</h2>
	                </div>
	                </c:when>
	                <c:when test="${lotterySwitchBySystem.lotteryType == 'WFKS' && lotterySwitchBySystem.lotteryStatus==1}">
	                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/wfks/wfks.html','')">
	                    <div class="icon icon-kuai3-small"></div>
	                    <h2>五分快三</h2>
	                </div>
	                </c:when>
	                </c:choose>
	                </c:forEach>
                </ul>
                <div class="child hasNav hide">
                    <div class="icon icon-11xuan5-small"></div>
                    <h2>11选5</h2>
                </div>
                <ul class="nav-child">
                	<c:forEach var="lotterySwitchBySystem" items="${lotterySwitchListBySystem}">
                	<c:choose>
                	<c:when test="${lotterySwitchBySystem.lotteryType == 'SDSYXW' && lotterySwitchBySystem.lotteryStatus==1}">
	                    <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/sdsyxw/sdsyxw.html','')">
	                        <div class="icon icon-11xuan5-small"></div>
	                        <h2>山东11选5</h2>
	                    </div>
                    </c:when>
                    <c:when test="${lotterySwitchBySystem.lotteryType == 'CQSYXW' && lotterySwitchBySystem.lotteryStatus==1}">
	                    <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/cqsyxw/cqsyxw.html','')">
	                        <div class="icon icon-11xuan5-small"></div>
	                        <h2>重庆11选5</h2>
	                    </div>
                    </c:when>
                    <c:when test="${lotterySwitchBySystem.lotteryType == 'JXSYXW' && lotterySwitchBySystem.lotteryStatus==1}">
	                    <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jxsyxw/jxsyxw.html','')">
	                        <div class="icon icon-11xuan5-small"></div>
	                        <h2>江西11选5</h2>
	                    </div>
                    </c:when>
                    <c:when test="${lotterySwitchBySystem.lotteryType == 'FJSYXW' && lotterySwitchBySystem.lotteryStatus==1}">
	                    <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/fjsyxw/fjsyxw.html','')">
	                        <div class="icon icon-11xuan5-small"></div>
	                        <h2>福建11选5</h2>
	                    </div>
                    </c:when>
                    <c:when test="${lotterySwitchBySystem.lotteryType == 'GDSYXW' && lotterySwitchBySystem.lotteryStatus==1}">
	                    <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/gdsyxw/gdsyxw.html','')">
	                        <div class="icon icon-11xuan5-small"></div>
	                        <h2>广东11选5</h2>
	                    </div>
                    </c:when>
                   <c:when test="${lotterySwitchBySystem.lotteryType == 'WFSYXW' && lotterySwitchBySystem.lotteryStatus==1}">
	                    <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/wfsyxw/wfsyxw.html','')">
	                        <div class="icon icon-11xuan5-small"></div>
	                        <h2>五分11选5</h2>
	                    </div>
                    </c:when>
                    <c:when test="${lotterySwitchBySystem.lotteryType == 'SFSYXW' && lotterySwitchBySystem.lotteryStatus==1}">
	                    <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/sfsyxw/sfsyxw.html','')">
	                        <div class="icon icon-11xuan5-small"></div>
	                        <h2>三分11选5</h2>
	                    </div>
                    </c:when>
                    </c:choose>
	                </c:forEach>
                </ul>
                
                <div class="child hasNav hide">
                    <div class="icon icon-PK10-common-small"></div>
                    <h2>PK10</h2>
                </div>
                <ul class="nav-child">
                	<c:forEach var="lotterySwitchBySystem" items="${lotterySwitchListBySystem}">
                	<c:choose>
                	<c:when test="${lotterySwitchBySystem.lotteryType == 'BJPK10' && lotterySwitchBySystem.lotteryStatus==1}">
                		<div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/bjpks/bjpk10.html','')">
	                    	<div class="icon icon-pk10-small"></div>
	                    	<h2>北京PK10</h2>
                		</div>
                	</c:when>
                	<c:when test="${lotterySwitchBySystem.lotteryType == 'JSPK10' && lotterySwitchBySystem.lotteryStatus==1}">
		                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jspks/jspk10.html','')">
		                    <div class="icon icon-jspk10-small"></div>
		                    <h2>极速PK10</h2>
		                </div>
	                </c:when>
	                <c:when test="${lotterySwitchBySystem.lotteryType == 'SFPK10' && lotterySwitchBySystem.lotteryStatus==1}">
		                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/sfpk10/sfpk10.html','')">
		                    <div class="icon icon-PK10-common-small"></div>
		                    <h2>三分PK10</h2>
		                </div>
	                </c:when>
	                <c:when test="${lotterySwitchBySystem.lotteryType == 'WFPK10' && lotterySwitchBySystem.lotteryStatus==1}">
		                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/wfpk10/wfpk10.html','')">
		                    <div class="icon icon-PK10-common-small"></div>
		                    <h2>五分PK10</h2>
		                </div>
	                </c:when>
	                <c:when test="${lotterySwitchBySystem.lotteryType == 'SHFPK10' && lotterySwitchBySystem.lotteryStatus==1}">
		                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/shfpk10/shfpk10.html','')">
		                    <div class="icon icon-PK10-common-small"></div>
		                    <h2>十分PK10</h2>
		                </div>
	                </c:when>
	                
	                </c:choose>
	                </c:forEach>
                </ul>
                <c:forEach var="lotterySwitchBySystem" items="${lotterySwitchListBySystem}">
                <c:choose>
                <c:when test="${lotterySwitchBySystem.lotteryType == 'XYEB' && lotterySwitchBySystem.lotteryStatus==1}">
			        <div class="child hot hide" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/xyeb/xy28.html','')">
				        <div class="icon icon-xy28-small"></div>
				        <h2>幸运28</h2>
			        </div>
	            </c:when>
	            <c:when test="${lotterySwitchBySystem.lotteryType == 'XYFT' && lotterySwitchBySystem.lotteryStatus==1}">
		                <div class="child hot hide" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/xyft/xyft.html','')">
		                    <div class="icon icon-xyft-small"></div>
		                    <h2>幸运飞艇</h2>
		                </div>
	            </c:when>
                <c:when test="${lotterySwitchBySystem.lotteryType == 'XYLHC' && lotterySwitchBySystem.lotteryStatus==1}">
		            <div class="child hot hide" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/xylhc/xylhc.html','')">
		                <div class="icon icon-xyliuhecai-small"></div>
		                <h2>幸运六合彩</h2>
		            </div>
	            </c:when>
	            </c:choose>
	            </c:forEach>
            </ul>


            <div class="child hasNav child-title hide">
                <div class="icon icon-dipinlottery"></div>
                <h2>低频彩</h2>
                <div class="pointer icon-pointer"></div>
            </div>
            <ul class="nav-child">
            	<c:forEach var="lotterySwitchBySystem" items="${lotterySwitchListBySystem}">
                <c:choose>
                <c:when test="${lotterySwitchBySystem.lotteryType == 'LHC' && lotterySwitchBySystem.lotteryStatus==1}">
	                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/lhc/lhc.html','')">
	                    <div class="icon icon-liuhecai-small"></div>
	                    <h2>香港六合彩</h2>
	                </div>
                </c:when>
                <c:when test="${lotterySwitchBySystem.lotteryType == 'FCSDDPC' && lotterySwitchBySystem.lotteryStatus==1}">
	                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/fcsddpc/fcsddpc.html','')">
	                    <div class="icon icon-3d-small"></div>
	                    <h2>福彩3D</h2>
	                </div>
				</c:when>
				</c:choose>
	            </c:forEach>
            </ul>

        </div>
        <div class="nav-control"><i class="icon"></i></div>
    </div>
	
    <div class="main-crumb">
    	<div class="logo"><img src="<%=path%>/front/images/lottery_home/icon-caiting.png" alt=""></div>
    	<div class="change" onclick="transformNav()"></div>
    	<div class="nav">
            <div class="icon icon-shishicai-small"></div>
            <ul class="nav-child">
            <c:forEach var="lotterySwitchBySystem" items="${lotterySwitchListBySystem}">
                <c:choose>
            	<c:when test="${lotterySwitchBySystem.lotteryType == 'CQSSC' && lotterySwitchBySystem.lotteryStatus==1}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/cqssc/cqssc.html','')"><p>重庆时时彩</p></div>
                </c:when>
                <c:when test="${lotterySwitchBySystem.lotteryType == 'XJSSC' && lotterySwitchBySystem.lotteryStatus==1}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/xjssc/xjssc.html','')"><p>新疆时时彩</p></div>
                </c:when>
                <c:when test="${lotterySwitchBySystem.lotteryType == 'TJSSC' && lotterySwitchBySystem.lotteryStatus==1}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/tjssc/tjssc.html','')"><p>天津时时彩</p></div>
               	</c:when>
               	<c:when test="${lotterySwitchBySystem.lotteryType == 'HLJSSC' && lotterySwitchBySystem.lotteryStatus==1}">
	            <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/hljssc/hljssc.html','')"><p>黑龙江时时彩</p></div>
                </c:when>
                </c:choose>
	        </c:forEach>
            </ul>
        </div>
        <div class="nav">
            <div class="icon icon-pk10-small"></div>
            <ul class="nav-child">
            <c:forEach var="lotterySwitchBySystem" items="${lotterySwitchListBySystem}">
                <c:choose>
            	<c:when test="${lotterySwitchBySystem.lotteryType == 'BJPK10' && lotterySwitchBySystem.lotteryStatus==1}">
                	<div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/bjpks/bjpk10.html','')"><p>北京PK拾</p></div>
                </c:when>
                <c:when test="${lotterySwitchBySystem.lotteryType == 'JSPK10' && lotterySwitchBySystem.lotteryStatus==1}">
                	<div class="child hot" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jspks/jspk10.html','')"><p>极速PK拾</p></div>
                </c:when>
                <c:when test="${lotterySwitchBySystem.lotteryType == 'SFPK10' && lotterySwitchBySystem.lotteryStatus==1}">
                	<div class="child hot" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/sfpk10/sfpk10.html','')"><p>三分K拾</p></div>
                </c:when>
                <c:when test="${lotterySwitchBySystem.lotteryType == 'WFPK10' && lotterySwitchBySystem.lotteryStatus==1}">
                	<div class="child hot" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/wfpk10/wfpk10.html','')"><p>五分PK拾</p></div>
                </c:when>
                <c:when test="${lotterySwitchBySystem.lotteryType == 'SHFPK10' && lotterySwitchBySystem.lotteryStatus==1}">
                	<div class="child hot" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/shfpk10/shfpk10.html','')"><p>十分PK拾</p></div>
                </c:when>
                </c:choose>
	        </c:forEach>
            </ul>
        </div>
        <div class="nav">
            <div class="icon icon-fenfencai-small"></div>
            <ul class="nav-child">
            <c:forEach var="lotterySwitchBySystem" items="${lotterySwitchListBySystem}">
                <c:choose>
                <c:when test="${lotterySwitchBySystem.lotteryType == 'JLFFC' && lotterySwitchBySystem.lotteryStatus==1}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jyffc/jyffc.html','')"><p>幸运分分彩</p></div>
                </c:when>
                <c:when test="${lotterySwitchBySystem.lotteryType == 'TWWFC' && lotterySwitchBySystem.lotteryStatus==1}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/twwfc/twwfc.html','')"><p>台湾五分彩</p></div>
                </c:when>
                <c:when test="${lotterySwitchBySystem.lotteryType == 'HGFFC' && lotterySwitchBySystem.lotteryStatus==1}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/hgffc/hgffc.html','')"><p>韩国1.5分彩</p></div>
                </c:when>
                <c:when test="${lotterySwitchBySystem.lotteryType == 'XJPLFC' && lotterySwitchBySystem.lotteryStatus==1}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/xjplfc/xjplfc.html','')"><p>新加坡2分彩</p></div>
                </c:when>
                </c:choose>
	        </c:forEach>
            </ul>
        </div>
        <div class="nav">
            <div class="icon icon-kuai3-small"></div>
            <ul class="nav-child">
            <c:forEach var="lotterySwitchBySystem" items="${lotterySwitchListBySystem}">
                <c:choose>
                <c:when test="${lotterySwitchBySystem.lotteryType == 'JYKS' && lotterySwitchBySystem.lotteryStatus==1}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jyks/jyks.html','')"><p>幸运快3</p></div>
                </c:when>
                <c:when test="${lotterySwitchBySystem.lotteryType == 'JSKS' && lotterySwitchBySystem.lotteryStatus==1}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jsks/jsks.html','')"><p>江苏快3</p></div>
                </c:when>
                <c:when test="${lotterySwitchBySystem.lotteryType == 'AHKS' && lotterySwitchBySystem.lotteryStatus==1}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/ahks/ahks.html','')"><p>安徽快3</p></div>
                </c:when>
                <c:when test="${lotterySwitchBySystem.lotteryType == 'JLKS' && lotterySwitchBySystem.lotteryStatus==1}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jlks/jlks.html','')"><p>吉林快3</p></div>
                </c:when>
                <c:when test="${lotterySwitchBySystem.lotteryType == 'HBKS' && lotterySwitchBySystem.lotteryStatus==1}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/hbks/hbks.html','')"><p>湖北快3</p></div>
                </c:when>
                <c:when test="${lotterySwitchBySystem.lotteryType == 'BJKS' && lotterySwitchBySystem.lotteryStatus==1}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/bjks/bjks.html','')"><p>北京快3</p></div>
                </c:when>
                <c:when test="${lotterySwitchBySystem.lotteryType == 'GXKS' && lotterySwitchBySystem.lotteryStatus==1}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/gxks/gxks.html','')"><p>广西快3</p></div>
                </c:when>
                </c:choose>
	        </c:forEach>
            </ul>
        </div>
        <div class="nav">
            <div class="icon icon-11xuan5-small"></div>
            <ul class="nav-child">
            <c:forEach var="lotterySwitchBySystem" items="${lotterySwitchListBySystem}">
                <c:choose>
                <c:when test="${lotterySwitchBySystem.lotteryType == 'SDSYXW' && lotterySwitchBySystem.lotteryStatus==1}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/sdsyxw/sdsyxw.html','')"><p>山东11选5</p></div>
                </c:when>
                <c:when test="${lotterySwitchBySystem.lotteryType == 'GDSYXW' && lotterySwitchBySystem.lotteryStatus==1}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/gdsyxw/gdsyxw.html','')"><p>广东11选5</p></div>
                </c:when>
                <c:when test="${lotterySwitchBySystem.lotteryType == 'JXSYXW' && lotterySwitchBySystem.lotteryStatus==1}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/jxsyxw/jxsyxw.html','')"><p>江西11选5</p></div>
                </c:when>
                <c:when test="${lotterySwitchBySystem.lotteryType == 'CQSYXW' && lotterySwitchBySystem.lotteryStatus==1}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/cqsyxw/cqsyxw.html','')"><p>重庆11选5</p></div>
                </c:when>
                <c:when test="${lotterySwitchBySystem.lotteryType == 'FJSYXW' && lotterySwitchBySystem.lotteryStatus==1}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/fjsyxw/fjsyxw.html','')"><p>福建11选5</p></div>
                </c:when>
                <c:when test="${lotterySwitchBySystem.lotteryType == 'WFSYXW' && lotterySwitchBySystem.lotteryStatus==1}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/wfsyxw/wfsyxw.html','')"><p>五分11选5</p></div>
                </c:when>
                <c:when test="${lotterySwitchBySystem.lotteryType == 'SFSYXW' && lotterySwitchBySystem.lotteryStatus==1}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/sfsyxw/sfsyxw.html','')"><p>五分11选5</p></div>
                </c:when>
                </c:choose>
	        </c:forEach>
            </ul>
        </div>
         <div class="nav">
            <div class="icon icon-xyliuhecai-small"></div>
            <ul class="nav-child">
            <c:forEach var="lotterySwitchBySystem" items="${lotterySwitchListBySystem}">
                <c:choose>
                <c:when test="${lotterySwitchBySystem.lotteryType == 'XYLHC' && lotterySwitchBySystem.lotteryStatus==1}">
                <div class="child hot" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/xylhc/xylhc.html','')"><p>幸运六合彩</p></div>
                </c:when>
                </c:choose>
	        </c:forEach>
            </ul>
        </div>
        <div class="nav">
            <div class="icon icon-liuhecai-small"></div>
            <ul class="nav-child">
            <c:forEach var="lotterySwitchBySystem" items="${lotterySwitchListBySystem}">
                <c:choose>
                <c:when test="${lotterySwitchBySystem.lotteryType == 'LHC' && lotterySwitchBySystem.lotteryStatus==1}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/lhc/lhc.html','')"><p>香港六合彩</p></div>
                </c:when>
                </c:choose>
	        </c:forEach>
            </ul>
        </div>
        <div class="nav">
            <div class="icon icon-3d-small"></div>
            <ul class="nav-child">
            	<c:forEach var="lotterySwitchBySystem" items="${lotterySwitchListBySystem}">
                <c:choose>
                <c:when test="${lotterySwitchBySystem.lotteryType == 'FCSDDPC' && lotterySwitchBySystem.lotteryStatus==1}">
                <div class="child" onclick="frontCommonPage.judgeCurrentUserToLogin('<%=path%>/gameBet/lottery/fcsddpc/fcsddpc.html','')"><p>福彩3D</p></div>
                </c:when>
                </c:choose>
	        </c:forEach>
            </ul>
        </div>
    </div>
    <div class="main-content" style="width:calc(100% - 215px);" id="lotteryhome">

    </div>

    <!-- main-content over -->
</div>

<script>
	$(function(){
		/**
		 * 导航切换
		 * @type {[type]}
		 */
		$(".main-nav .child.hasNav").click(function(){
				var $el = $(this),
						$nav = $el.next(".nav-child");
				$nav.stop(false,true).slideToggle(300);
				$el.toggleClass("hide");
		});
		transformMain();
		$(window).scroll(transformMain);
	
		base.animate_add(".main-content .child","fadeInRight",0.1);
		base.animate_add(".main-nav","fadeInLeft");
		
		/* 侧栏展开关闭切换*/
		$('.nav-control').on('click',function(){
			if($(this).hasClass('nav-open')){
				$('.main-ml0').css('margin-left','238px');
				$(this).removeClass('nav-open');
				$('.main-nav').removeClass('on');
			}else{
				$('.main-ml0').css('margin-left','auto');
				$('.main-nav').addClass('on');
				$(this).addClass('nav-open');
			}
		})
	});
	
	function transformMain(){
		var winHeight = window.innerHeight,
				headerHeight = $(".header").height();
		var mainHeight = winHeight - headerHeight;
		$(".main").css({
				"height": mainHeight+"px"
		});
	}
	
	function transformColor(classname){
		var $el = $(".main"),
				classname = classname || "main-black";
		if(!$el.hasClass("main-black")) {$el.addClass(classname);$('body, html').css('background-color','#2a303e')}
		else{ $el.attr("class","main");$('body, html').css('background-color','#efefef')}
	}
	
	function transformNav(){
		$(".main-nav").removeClass("animated").attr("style","").toggleClass("maincrumb");
		$(".main-crumb").removeClass("animated").attr("style","").toggleClass("maincrumb");
		$(".main-content").removeClass("animated").attr("style","").toggleClass("maincrumb");
		$(".main-ml0").removeClass("animated").attr("style","").toggleClass("ml0");
	}
</script>