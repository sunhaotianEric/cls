<%@ page language="java" contentType="text/html; charset=UTF-8"
    import="com.team.lottery.vo.LotterySet,com.team.lottery.util.ConstantUtil,com.team.lottery.vo.User,
    com.team.lottery.system.BizSystemConfigVO,com.team.lottery.system.BizSystemConfigManager,
    com.team.lottery.cache.ClsCacheManager,com.team.lottery.vo.BizSystemInfo,com.team.lottery.util.DateUtil,com.team.lottery.system.SystemConfigConstant"
	pageEncoding="UTF-8"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
    String serverName=request.getServerName();
    
    String bizSystem=ClsCacheManager.getBizSystemByDomainUrl(serverName);
    User user = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	BizSystemInfo bizSystemInfo=ClsCacheManager.getBizSystemInfo(bizSystem);
	LotterySet lotterySet=new LotterySet();
    if(bizSystemInfo==null){
    	bizSystemInfo = new BizSystemInfo();
    }
    BizSystemConfigVO bizSystemConfigVO=BizSystemConfigManager.getBizSystemConfig(bizSystem);
	request.setAttribute("bizSystemInfo", bizSystemInfo);
	request.setAttribute("bizSystemConfigVO", bizSystemConfigVO);
%>
<!-- header -->
<div class="header">
	<!-- head -->
	<div class="head">
	<div class="container">
    	<p class="gmt">
    		<img src="<%=path%>/front/images/GMT.png" />
    		<span id="gmtTime"></span>
    	</p>
    	<div class="change-color" id="changecolor-show" onclick="transformColor()">
             <span>换色</span>
             <img src="<%=path%>/front/images/lottery_home/icon-bluepointer.png" alt="">
             <img src="<%=path%>/front/images/lottery_home/icon-redpointer.png" alt="">
             <img src="<%=path%>/front/images/lottery_home/icon-yellowpointer.png" alt="">
         </div>
    	<% if(user!=null){%>
    	 <ul class="nav">
        	<!-- <a href="javascript:void(0);"><li><p id="username">你好，---</p></li></a> -->
        	
        	<a href="<%=path %>/gameBet/recharge.html"><li><p>充值</p></li></a>
            <a href="<%=path %>/gameBet/withdraw.html"><li><p>提现</p></li></a>
            
            <li id="msgbox">
            	<a href="<%=path %>/gameBet/message/inbox.html"><div class="mun" id="msgcount">0</div></a>
            	<div class="list mun-list" >
	                <div class="m-title">我的未读消息(<span id="msgcount2" style="color: #0090ff">0</span>) <a href="<%=path %>/gameBet/message/inbox.html">更多 >></a></div>
	                <div class="center" id="msgbd">
	                	<!-- <p class="m-msg">暂未收到新消息</p>
	                	<a href="user_msg2.html"><div class="list-child"><p>信息1</p></div></a>
	                    <a href="user_msg2.html"><div class="list-child"><p>信息2</p></div></a> -->
	                </div>
                </div>
            </li>
            
            <li>	
            	<p id="hiddBall" style="display: none;">
            		<span>余额：</span><span id="spanBall">0000.000</span> <img style="cursor:pointer;" src="<%=path%>/front/images/refreshBall.png" id='refreshBall'>
            	</p>
            	<p id="hiddenBall"  style="display: block;">
            		余额已隐藏 <a href="javascript:void(0)" id="showAllBall">显示</a>
            	</p>
            </li>
           
            <li><p>我的账户<img src="<%=path%>/front/images/nav-pointer.png" /></p>
            	<div class="list"><div class="center">
                    <a href="<%=path %>/gameBet/accountCenter.html"><div class="list-child">账户总览</div></a>
                	<%-- <a href="<%=path %>/gameBet/user_center.html"><div class="list-child">会员中心</div></a> --%>
                    <a href="<%=path %>/gameBet/order.html"><div class="list-child">投注历史</div></a>
                    <a href="<%=path %>/gameBet/moneyDetail.html"><div class="list-child">账户明细</div></a>
                    <%
					  if(!user.getDailiLevel().equals("REGULARMEMBERS")){  //如果是代理会员,才能显示
					%>
                    <a href="<%=path %>/gameBet/agentCenter/userList.html"><div class="list-child">代理中心</div></a>
					<%
					  }
					%>
					<a href="javascript:void(0)" class="exit"><div class="list-child">退出登录</div></a>
                </div></div>
            </li>
        </ul>
    	<%}else{ %>
    	<div class="nav">
            <p><span class="color-red"><span><%=DateUtil.getTimeState() %></span></span>您还未登录哦~</p>
        </div>
        <%if(bizSystemConfigVO.getGuestModel()==1){ %>
        	<a href="javascript:void(0)" class="btn-tryplay">点击试玩</a>
        <%} %>
        <%}%>
    </div>
    </div>
    <!-- head over -->
    <!-- foot -->
    <div class="foot">
    <div class="container">
	   	<a href="<%=path%>/gameBet/index.html"><div class="logo"><img id="logoImg" src="<%=bizSystemInfo==null?"":bizSystemInfo.getHeadLogoUrl()%>"></div></a>
    	<%-- <a href="<%=path %>/gameBet/index.html"><div class="logo"><img src="<%=path%>/front/images/logo-icon.png" /></div></a>
        <a href="<%=path %>/gameBet/index.html"><div class="logo logo-title"><p>国际娱乐</p></div></a> --%>
        <ul class="nav" id="navChoose">
        	<a href="<%=path %>/gameBet/index.html"><li class="child child1">
        		<div class="child-icon"></div>
        		<p class="child-title">首页</p></li>
        	</a>
        	<a href="<%=path%>/gameBet/hall.html"><li class="child child2">
        		<div class="child-icon"></div>
        		<p class="child-title">彩票大厅</p>
        	</a>
        	<a href="<%=path%>/gameBet/wap.html"><li class="child child3">
        		<div class="child-icon"></div>
        		<p class="child-title">手机购彩</p></li>
        	</a>
            <a href="<%=path%>/gameBet/help.html?parm1=CJWT"><li class="child child4">
            	<div class="child-icon"></div>
            	<p class="child-title">帮助中心</p></li>
            </a>
            <a href="<%=path%>/gameBet/activity.html"><li class="child child5">
            	<div class="child-icon"></div>
            	<p class="child-title">活动专区</p></li>
            </a>
            <a href="<%=path%>/gameBet/activity.html" class="footballgame" target="_blank"><li class="child child6">
            	<div class="child-icon"></div>
            	<p class="child-title">足球竞彩</p></li>
            </a>
        </ul>
        
        <% if(user!=null){%>
        	<div class="infos">
	            <div class="info">
	                <p class="title" id="username">下午好,XXX</p>
	                <p class="logout"><a href="javascript:headerPage.userLogout();">退出登录</a></p>
	            </div>
	            <div class="icon"><a href="<%=path %>/gameBet/accountCenter.html"><img src="<%=path%>/front/images/user/headimg.png" alt=""></a></div>
	        </div>
        <%}else{ %>
        	<div class="status">
	            <a href="<%=path%>/gameBet/reg.html"><div class="btn btn-black">注册</div></a>
	            <a href="<%=path%>/gameBet/login.html"><div class="btn btn-red">登录</div></a>
	        </div>
        <%}%>
        
    </div>
    </div>
    <!-- foot over -->
</div>
<!-- header over -->

<!-- alert-msg -->
<div class="tourist-alert-msg-bg" style="z-index: 4;"></div>
<div id="touristDialog" class="alert-msg" >
    <div class="msg-head">
        <p>温馨提示<span style="font-size: 14px;font-weight: bold;color: rgb(255, 0, 0);margin-left: 15px;display: none;">当天用过毫模式的，当天每期最高返奖总额为2万元</span></p>
        <img  class="close" src="<%=path%>/front/images/user/close.png" />
    </div> 
    <div class="msg-content pop-tryPlay">
    	 <div class="enroll-line" id="touristVerifycode_line">
            <div class="line-title">验证码：</div>
            <div class="line-content"><input type="text" class="inputText" id='touristVerifycode'><img class="verifyImg" style="width: 220px; height: 100px;" id="checkCode" onclick="checkCodeRefresh()" src="" alt=""></div>
            <p class="line-msg">验证码不正确</p>
        </div>
    	<div class="line-block no-border">
    		<div class="cont">试玩功能仅提供投注体验使用，为了更好的为您提供服务，请点击注册按钮注册真实用户，完善个人信息。</div>
    	</div>
        <div class="btn"><input id="tryPlayDialogSureButton" onclick="touRist()"  type="button" class="inputBtn" value="确定" /></div>
    </div>
</div>
<!-- alert-msg over -->

<script type="text/javascript">
//设置首页平均充值时间，首页平均提现时间
var recharge_second= '<%=bizSystemInfo.getRechargeSecond()!=null?bizSystemInfo.getRechargeSecond():5%>';
var withdraw_second= '<%=bizSystemInfo.getWithdrawSecond()!=null?bizSystemInfo.getWithdrawSecond():7%>';
//首页登陆默认打开，其他页不展示
 var url=window.location.href;
 if(url.indexOf("index.html")>0){
		var headH = $(".header .head").height(),
		footH = $(".header .foot").height();
		if($(".shortcut-logo").hasClass("on")){
				$(".shortcut-logo").removeClass("on");
				window.shortcut = setTimeout(function() {
						var h = headH + footH;
						$(".header").css({ height: h + "px" });
				},500);
		}else{
				$(".shortcut-logo").addClass("on");
				$(".header").css({ height: "auto" });
				clearTimeout(window.shortcut);
		}

	 
 }else{
		var headH = $(".header .head").height(),
		footH = $(".header .foot").height();
		if($(".shortcut-logo").hasClass("on")){
				$(".shortcut-logo").removeClass("on");
				window.shortcut = setTimeout(function() {
						var h = headH + footH;
						$(".header").css({ height: h + "px" });
				},500);
		}else{
				$(".shortcut-logo").addClass("on");
				$(".header").css({ height: "auto" });
				clearTimeout(window.shortcut);
		} 

 }
</script>
<script type="text/javascript">   
	var contextPath = '<%=path %>';
	function checkCodeRefresh() {
		$('#touristCheckcode').attr('src', contextPath + "/tools/getRandomCodePic?time="+Math.random());
	}
</script>
<script>
	$(document).ready(function(){
		$(".header .head .nav li img").click(function(){
			var parent=$(this).closest('.nav');
			$(this).addClass("on");
			parent.find("#spanBall").html("加载中...");
			setTimeout(function(parent){
				parent.find("li img").removeClass('on');
			},1000,parent);
		});
		
		//确认-点击试玩事件
		$(".btn-tryplay").unbind("click").click(function(){
			$("#touristDialog").stop(false,true).delay(10).fadeIn(500);
			$(".tourist-alert-msg-bg").stop(false,true).fadeIn(500);
			$('#checkCode').attr('src', contextPath + "/tools/getRandomCodePic?time="+Math.random());
		});
		//关闭点击试玩事件
	    $(".close").unbind("click").click(function(){
	    	$("#touristDialog").stop(false,true).delay(200).fadeOut(500);
	    	$(".tourist-alert-msg-bg").stop(false,true).fadeOut(500);
		});
	})
	//确认试玩处理方法
	function touRist(){
	    	var code=$("#touristVerifycode").val();
	    	if(code==""||code==null){
	    		frontCommonPage.showKindlyReminder("请输入验证码");
	    		return;
	    	}
	    	$.ajax({
	            type: "POST",
	            url: contextPath +"/user/guestUserPCLogin",
	            contentType :"application/json;charset=UTF-8",
	            data :JSON.stringify({"code":code}), 
	            dataType:"json",
	            success: function(result){
	                if (result.code == "ok") {
	                	$(".close").click();
		    			frontCommonPage.showKindlyReminder("祝您试玩愉快，2秒后自动刷新页面！",function(){
		    				location.reload();
		    			});
		    			setTimeout("location.reload()",2000);
	                }else{
	                	frontCommonPage.showKindlyReminder(result.data);
	                }
	                touristCheckcode();
	            }
	        });
	    	/* frontUserAction.guestUserPCLogin(code,function(r){
	    		if(r[0]=="ok"){
	    			$(".close").click();
	    			frontCommonPage.showKindlyReminder("祝您试玩愉快，2秒后自动刷新页面！",function(){
	    				location.reload();
	    			});
	    			setTimeout("location.reload()",2000);
	    		}else{
	    			frontCommonPage.showKindlyReminder(r[1]);
	    		}
	    		touristCheckcode();
	    	}); */
	}
</script>
