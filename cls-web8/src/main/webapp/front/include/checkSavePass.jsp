<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
%>    
<!-- <div id="saveCheckDialog" class="miniwindow" style="z-index: 700; position: fixed; left: 687px; top: 338px; display: none;">
    <div class="hd">
        <i class="close closeBtn" id="closeBtn"></i>
        <span class="title">为了保障您的账户安全，需要先验证您的安全密码</span>
    </div>
    <div class="bd">
        <div class="form-info clearfix">
            <table>
                <tbody>
                    <tr>
                        <th>安全密码:</th>
                        <td class="line30">
                            <input type="password" class="input w" id="savePassword" name="password" maxlength="50">
                            <span id="passwordTip" class="form-tip cross" style="display: none;"><i></i>安全密码错误！</span>
                        </td>
                   </tr>
               </tbody>
           </table>
           <input type="button" class="setting-submit-btn a-btn" id="saveChkBtn" value="提交">    
           <span class="save-ok" id="errmsg" style="display: none;">安全密码连续三次错误</span>
        </div>
    </div>
</div> -->


<div id="saveCheckDialog"  class="reveal-modal reveal-modal-c3" style="opacity:1; width:600px; margin-top:-140px;margin-left:-310px; visibility: visible; display:none;">
    <div class="reveal-modal-wapper cp-current-mode clearfix">
        <div class="reveal-modal-header">
            <div class="reveal-modal-header-title">操作提示</div>
            <a class="close-reveal-modal" id="closeBtn" title="关闭">关闭</a>
        </div>
        <div class="current-mode-slider ">
             <div class="pop-success">
				<i class="ico-success"></i>
				<h4 class="pop-text cp-yellow">为了保障您的账户安全，需要先验证您的安全密码</h4>
			</div>
            <div class="general-form bank-cash clearfix">
                <div class="form-bd">
                    <div class="form-item">
                        <label class="form-item-hd">安全密码：</label>
                        <div class="form-item-bd">
                            <input type="password" class="input w" id="savePassword" name="password" maxlength="50">
                            <span id="passwordTip" class="form-tip cross" style="display: none;"><i></i>安全密码错误！</span>
                        </div>
                    </div>
                    <div class="form-item">
                        <label class="form-item-hd"></label>
                        <div class="form-item-bd">
                            <a href="javascript:void(0);" class="a-btn general-btn" id="saveChkBtn">提交</a>
            				<span class="form-tip cross" id="errmsg" style="display: none;"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>






<div class="reveal-modal-bg" id="reveal-modal-bg" style="display:none;"></div>
