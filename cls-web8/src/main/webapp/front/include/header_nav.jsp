<%@page import="com.team.lottery.system.SystemConfigConstant"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.util.ConstantUtil,com.team.lottery.vo.User,com.team.lottery.util.DateUtil" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	User user = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
%>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<!-- 需要加载的图片 -->
<div class="stance"></div>
<!-- m-banner -->
<div class="m-banner" style="background-image:url(<%=path%>/front/images/login/banner.jpg);">
    <div class="banner-content container">
        <div class="first">
         	<div class="bg"></div>
            <div class="head-img"><img src="<%=path%>/front/images/user/headimg.png" /></div>
            <div class="head-content">
                <p class="title"><span><%=DateUtil.getTimeState() %></span><%=user.getUserName() %></p>
               <%--  <p class="info">当前模式：<%=user.getSscrebate() %></p> --%>
               <div class="grade" id="star">
               		头衔: <span class="orange" id="levelName">&nbsp;<%=user.getLevelName() %></span><i class="icon icon-level<%=user.getVipLevel() %>"></i>
               		<a href="javascript:;" onClick='showUserMsg()' class="btn-check">成长详情 &gt;&gt;</a>
               </div>
               <div class="level"  id="starLevel">
               		<span class="level1">VIP<%=user.getVipLevel() %></span>
               		<div class="progressbar" id="progressbar"><div class="progress"></div>0/0</div>
               </div>
                <div class="btns">
                    <a href="<%=path%>/gameBet/myInfo.html">修改资料</a>
                    <a href="<%=path%>/gameBet/bankCard/bankCardList.html">绑定银行卡</a>
                </div>
            </div>
        </div>
        <div class="lists">
        	<!-- 代理菜单 -->
        	<c:if test="${user.dailiLevel!='REGULARMEMBERS'}">
        		<a href="<%=path %>/gameBet/accountCenter.html">
	        		<div class="child child1">
	        			<div class="bg"></div>
	                	<div class="child-image"></div>
	                	<p class="child-title">账户总览</p>
	            	</div>
	             </a>
	             <a href="<%=path%>/gameBet/rechargeOrder.html">
	             	<div class="child child2">
	             		<div class="bg"></div>
                		<div class="child-image"></div>
                		<p class="child-title">财务中心</p>
           			</div>
            	 </a>
            	 <a href="<%=path%>/gameBet/report/agentReport.html">
            	 	<div class="child child3" >
	            	 	<div class="bg"></div>
	                	<div class="child-image"></div>
	                	<p class="child-title">订单报表</p>
            		</div>
            	</a>
            	<a href="<%=path%>/gameBet/agentCenter/userList.html">
           			 <div class="child child4">
            			<div class="bg"></div>
               			<div class="child-image"></div>
               			<p class="child-title">代理中心</p>
            		</div>
            	</a>
            	<a href="<%=path%>/gameBet/message/inbox.html">
            		<div class="child child5" id="noteChild">
            			<div class="bg"></div>
                		<div class="child-image"></div>
                		<p class="child-title">消息中心</p>
            		</div>
            	</a>
            	<a href="javascript:void(0)" id="exit" class="exit">
            		<div class="child child6 no">
            			<div class="bg"></div>
                		<div class="child-image"></div>
                		<p class="child-title">退出</p>
            		</div>
            	</a>
        	</c:if>
        	<!-- 普通会员菜单 -->
        	<c:if test="${user.dailiLevel=='REGULARMEMBERS'}">
        		<a href="<%=path %>/gameBet/accountCenter.html">
	        		<div class="child child1" style="width:142px;">
	        			<div class="bg"></div>
	                	<div class="child-image"></div>
	                	<p class="child-title">账户总览</p>
	            	</div>
	             </a>
	             <a href="<%=path%>/gameBet/rechargeOrder.html">
	             	<div class="child child2" style="width:142px;">
	             		<div class="bg"></div>
                		<div class="child-image"></div>
                		<p class="child-title">财务中心</p>
           			</div>
            	 </a>
            	 <a href="<%=path%>/gameBet/order.html">
            	 	<div class="child child3" style="width:142px;">
	            	 	<div class="bg"></div>
	                	<div class="child-image"></div>
	                	<p class="child-title">订单报表</p>
            		</div>
            	</a>
            	<a href="<%=path%>/gameBet/message/inbox.html">
            		<div class="child child5" id="noteChild" style="width:142px;">
            			<div class="bg"></div>
                		<div class="child-image"></div>
                		<p class="child-title">消息中心</p>
            		</div>
            	</a>
            	<a href="javascript:void(0)" id="exit" class="exit" style="width:142px;">
            		<div class="child child6 no">
            			<div class="bg"></div>
                		<div class="child-image"></div>
                		<p class="child-title">退出</p>
            		</div>
            	</a>
        	</c:if>
        </div>
    </div>
</div>
<!-- m-banner over -->

<!-- alert-msg -->
<div class="alert-msg-bg"></div>
<div class="alert-msg alert-userinfo-msg" id="userinfo_msg">
	<div class="msg-head">
		<p>等级头衔</p>
		<img class="close" src="<%=path%>/front/images/user/close.png" onClick='(function msg_show(){$(".alert-userinfo-msg").stop(false,true).delay(200).fadeOut(500);$(".alert-msg-bg").stop(false,true).fadeOut(500);})()' />
	</div>
	<div class="msg-content">
        <div class="rule">
        	<h5>每充值1元得到1个成长积分</h5>
        	<table id="levelSystemTable">
				<thead>
					<th>等级</th>
					<th>头衔</th>
					<th>成长积分</th>
					<th>晋级奖励（元）</th>
					<th>跳级奖励（元）</th>
				</thead>
				<tbody>
					<tr>
						<td>VIP1</td>
						<td>农民<i class="icon icon-level1"></i></td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
					</tr>
					<tr>
						<td>VIP2</td>
						<td>农民<i class="icon icon-level2"></i></td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
					</tr>
					<tr>
						<td>VIP3</td>
						<td>农民<i class="icon icon-level3"></i></td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
					</tr>
					<tr>
						<td>VIP4</td>
						<td>农民<i class="icon icon-level4"></i></td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
					</tr>
					<tr>
						<td>VIP5</td>
						<td>农民<i class="icon icon-level5"></i></td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
					</tr>
					<tr>
						<td>VIP6</td>
						<td>农民<i class="icon icon-level6"></i></td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
					</tr>
					<tr>
						<td>VIP7</td>
						<td>农民<i class="icon icon-level7"></i></td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
					</tr>
					<tr>
						<td>VIP8</td>
						<td>农民<i class="icon icon-level8"></i></td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
					</tr>
					<tr>
						<td>VIP9</td>
						<td>农民<i class="icon icon-level9"></i></td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
					</tr>
				</tbody>        	
        	</table>
        </div>
	</div>
</div>
<script>
	function showUserMsg(){
		$("#userinfo_msg").stop(false,true).delay(200).fadeIn(500);
	    $(".alert-msg-bg").stop(false,true).fadeIn(500);
	}
</script>
<script type="text/javascript" src="<%=path%>/front/js/header_nav.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>