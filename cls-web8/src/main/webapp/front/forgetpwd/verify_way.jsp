<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>找回密码方式</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/index-enroll.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/index-retrieve-mode.css?v=<%=SystemConfigConstant.pcWebRsVersion%>"/>
<script src="<%=path%>/front/assets/jquery/jquery.js"></script>
<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<jsp:include page="/front/include/header_user.jsp"></jsp:include>
	
<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/js/forgetpwd/verify_way.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script type="text/javascript" src="<%=path%>/front/js/common.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script type="text/javascript" src="<%=path%>/js/forgetpwd/other.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>

<%
    String userName =  (String)request.getSession().getAttribute("retrieveUsername");
    if(userName == null || userName.equals("")){
		response.sendRedirect(path + "/gameBet/forgetpwd.html");
    	return;
    }
    
%>
<script type="text/javascript">
		verifyWayPage.param.id = '<%=userName %>';
</script>

</head>
<body>
<div class="stance"></div>
<!-- m-banner -->
<div class="m-banner" style="background-image:url(<%=path%>/front/images/login/banner.jpg);">
	<div class="content" ><img src="<%=path%>/front/images/enroll/banner-title2.png" /></div>
</div>
<!-- m-banner over -->

<!-- main -->
<div class="main"><div class="container">

    <div class="main-content">

      <div class="login">
          <div class="login-container">
            <p class="title1">您正在找回密码的帐号为 <span><%=userName %></span>，<a href="<%=path%>/gameBet/forgetpassword/index_retrieve.html">换一个帐号</a></p>
              <div class="l">
                  <div class="l-child on">
                      <div class="sub">
                          <div class="pointer"></div>
                      </div>
                      <p class="l-title">1.选择找回方式</p>
                  </div>
                  <div class="l-child">
                      <div class="sub">
                          <div class="pointer"></div>
                      </div>
                      <p class="l-title">2.进行安全验证</p>
                  </div>
                  <div class="l-child">
                      <div class="sub">
                          <div class="pointer"></div>
                      </div>
                      <p class="l-title">3.设置新密码</p>
                  </div>
                  <div class="l-sub"></div>
              </div>
          </div>
          <p class="title2">请选择找回密码的方式：</p>
          <div class="select-style">
              <!-- <p class="msg">您还未绑定任何安全信息，请与客服人员联系！<a href="#">点击联系客服</a></p> -->
              <p class="msg"  id="kefu"><!-- 您还未绑定任何安全信息，请与客服人员联系！<a href="#">点击联系客服</a> --></p>
              <ul id="find_way_list">
                  <!--       <li><input type="radio" ><span>选择密保问题来找回密码</span></li> -->
              </ul>
          </div>
          <div class="button button-red" id="nextButton">下一步</div>
      </div>

    </div>

    <!-- other -->
    <div class="other">

        <!-- consumtime -->
        <div class="child consumtime">
            <h2 class="child-title">服务体验</h2>
            <div class="child-content">
                <div class="process" id="czdz_process">
                    <h3 class="process-title">昨日充值到账平均时间</h3>
                    <div class="process-content" >
                        <div class="process-ing process-yellow" data-process="60" style="width: 51%;"></div>
                    </div>
                    <h3 class="process-second"><span>10</span>秒</h3>
                </div>
                <div class="process" id="txdz_process">
                    <h3 class="process-title">昨日提现到账平均时间</h3>
                    <div class="process-content" >
                        <div class="process-ing process-blue" data-process="80" style="width: 61%;"></div>
                    </div>
                    <h3 class="process-second"><span>5</span>秒</h3>
                </div>
            </div>
        </div>
        <!-- consumtime over -->

        <!-- recharge -->
        <div class="child recharge">
            <h2 class="child-title">充值方式</h2>
            <div class="child-content">
                <a href="#"><div class="recharge-child">
                    <div class="icon"><img src="<%=path%>/front/images/index/recharge_wechat.png" alt=""></div>
                    <h3 class="title">微信支付</h3>
                </div></a>
                <a href="#"><div class="recharge-child">
                    <div class="icon"><img src="<%=path%>/front/images/index/recharge_alipay.png" alt=""></div>
                    <h3 class="title">支付宝</h3>
                </div></a>
                <a href="#"><div class="recharge-child">
                    <div class="icon"><img src="<%=path%>/front/images/index/recharge_yinlian.png" alt=""></div>
                    <h3 class="title">银联</h3>
                </div></a>
            </div>
        </div>
        <!-- recharge over -->

        <!-- download -->
        <!-- 
        <div class="child download" id="phonecodediv">
            <h2 class="child-title">手机下载</h2>
            <div class="child-content">
                <div class="download-child download-child-android" onclick="javascript:$('.popup-android').show();$('.popup-iphone').hide()">
                    <div class="download-child-img" ><img src="<%=path%>/front/images/index/download-android.png" alt=""></div>
                    <h3 class="download-child-title">支持Android2.2或以上</h3>
                </div>
                <div class="download-child download-child-ios" onclick="javascript:$('.popup-iphone').show();$('.popup-android').hide()">
                    <div class="download-child-img"><img src="<%=path%>/front/images/index/download-ios.png" alt=""></div>
                    <h3 class="download-child-title">支持IOS4.3或以上</h3>
                </div>
            </div>
        </div>
        -->
        <!-- download over -->

        <!-- wapcode -->
        <div class="child wapcode" id="wapcodediv" style="display: none;">
            <h2 class="child-title">移动版二维码</h2>
            <div class="child-content">
                <div class="btn code-btn">
                    <p class="btn-title">扫描二维码</p>
                    <div class="code">
                        <img id="wapcode" class="img" src="<%=path%>/front/images/wap/code.jpg" alt="">
                        <p class="title">扫码打开手机版</p>
                        <img class="pointer" src="<%=path%>/front/images/wap/pointer.png" alt="">
                    </div>
                </div>
               
            </div>
            <!-- <div class="child-content">
                <img src="images/wap/code.jpg" alt="">
            </div> -->
        </div>
        <!-- wapcode over -->

    </div>
    <!-- other over -->



</div></div>
<!-- main over -->
</body>
</html>