<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>安全验证</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/index-enroll.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/index-retrieve-mode.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />

<script src="<%=path%>/front/assets/jquery/jquery.js"></script>
<jsp:include page="/front/include/header_user.jsp"></jsp:include>
	
<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/js/forgetpwd/choose_way.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script type="text/javascript" src="<%=path%>/front/js/common.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script type="text/javascript" src="<%=path%>/js/forgetpwd/other.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>

<%
    String userName =  (String)request.getSession().getAttribute("retrieveUsername");
    if(userName == null || userName.equals("")){
		response.sendRedirect(path + "/gameBet/forgetpwd.html");
    	return;
    }
    String type=request.getParameter("type");
    request.setAttribute("type",type);
%>
<script type="text/javascript">
	chooseWayPage.param.id = '<%=userName %>';
	chooseWayPage.param.type = '<%=type %>';
</script>

<script type="text/javascript">
	function checkCodeRefresh() {
		$('#checkCodeRefresh').attr('src', contextPath + "/tools/getRandomCodePic?time=" + Math.random());
	}
</script>
</head>
<body>
<div class="stance"></div>
<!-- m-banner -->
<div class="m-banner" style="background-image:url(<%=path%>/front/images/login/banner.jpg);">
	<div class="content" ><img src="<%=path%>/front/images/enroll/banner-title2.png" /></div>
</div>
<!-- m-banner over -->

<!-- main -->
<div class="main"><div class="container">

    <div class="main-content">

      <div class="login">
          <div class="login-container">
            <p class="title1">您正在找回密码的帐号为 <span><%=userName %></span>，<a href="<%=path%>/gameBet/forgetpassword/index_retrieve.html">换一个帐号</a></p>
              <div class="l">
                  <div class="l-child on">
                      <div class="sub">
                          <div class="pointer"></div>
                      </div>
                      <p class="l-title">1.选择找回方式</p>
                  </div>
                  <div class="l-child on">
                      <div class="sub">
                          <div class="pointer"></div>
                      </div>
                      <p class="l-title">2.进行安全验证</p>
                  </div>
                  <div class="l-child">
                      <div class="sub">
                          <div class="pointer"></div>
                      </div>
                      <p class="l-title">3.设置新密码</p>
                  </div>
                  <div class="l-sub"></div>
              </div>



          </div>
          <c:if test="${type=='question' }">
          <div class="line">
              <img src="<%=path%>/front/images/login/i1.png" />
              <input type="text" class="inputText" value="" readonly="true"  id="question"/>
              <input type="hidden" id="qType"/>
          </div>
          <div class="line" id="answer_field">
              <img src="<%=path%>/front/images/login/i2.png" />
              <input type="text" class="inputText" placeholder="答案" id="answer"/>
          </div>
          <p class="msg red" style="display: none;" id="answer_tip">请输入答案！</p>
          
          <div class="line">
              <img src="<%=path%>/front/images/login/i1.png" />
              <input type="text" class="inputText" value="" readonly="true"  id="question1"/>
              <input type="hidden" id="qType1"/>
          </div>
          <div class="line" id="answer1_field">
              <img src="<%=path%>/front/images/login/i2.png" />
              <input type="text" class="inputText" placeholder="答案" id="answer1"/>
          </div>
          <p class="msg red" style="display: none;" id="answer1_tip">请输入答案！</p>
          <div class="line" id="verifyCode_field">
              <img src="<%=path%>/front/images/login/i3.png" />
              <input type="text" class="inputText" placeholder="验证码" id="verifyCode"/>
               <img class="code" alt="验证码" id="checkCodeRefresh" title="点击更换" onclick="checkCodeRefresh()" src="<%=path%>/tools/getRandomCodePic">
          </div>
          <p class="msg red" style="display: none;" id="verifyCode_tip">请输入验证码！</p>
          </c:if>
          <c:if test="${type=='phone' }">
          <!-- 手机短信验证 -->
	          <div class="mobile-vertify">
	          		<div class="line" id="mobile_field">
	              		<img src="<%=path%>/front/images/login/i4.png" />
	              		<input type="text" class="inputText" placeholder="手机号码" id="mobile"/>
	          			<button type="submit" class="btn-sendnum" id="btn_send">发送</button>
	          			<span class="mobile-vertify-time">发送验证码(<em class="countdown">60</em> s)</span>
	          		</div>
		          	<p class="msg red" style="display: none;" id="mobile_tip">请输入手机号码！</p>
		          	<div class="line" id="mobile_verifyCode_field">
		              	<img src="<%=path%>/front/images/login/i3.png" />
		              	<input type="text" class="inputText" placeholder="验证码" id="mobile_verifyCode"/>
		          	</div>
		          	<p class="msg red" style="display: none;" id="mobile_verifyCode_tip">请输入短信验证码！</p>
	          </div>
          <!-- 手机短信验证end -->
		  </c:if>
		  <c:if test="${type=='email' }">
		  	<!-- 邮箱验证 -->
		  	 <div class="mobile-vertify">
	          		<div class="line" id="mobile_field">
	              		<img src="<%=path%>/front/images/login/i4.png" />
	              		<input type="text" class="inputText" placeholder="邮箱号码" id="email"/>
	          			<button type="submit" class="btn-sendnum">发送</button>
	          			<span class="mobile-vertify-time">重发(<em class="countdown">60</em> s)</span>
	          		</div>
		          	<p class="msg red" style="display: none;" id="mobile_tip">请输入邮箱！</p>
		          	<div class="line" id="mobile_verifyCode_field">
		              	<img src="<%=path%>/front/images/login/i3.png" />
		              	<input type="text" class="inputText" placeholder="验证码" id="email_verifyCode"/>
		          	</div>
		          	<p class="msg red" style="display: none;" id="mobile_verifyCode_tip">请输入邮箱验证码！</p>
	         </div>
		  	
		  </c:if>
          <div class="button button-red"  id="nextButton">下一步</div>
      </div>

    </div>

    <!-- other -->
    <div class="other">

        <!-- consumtime -->
        <div class="child consumtime">
            <h2 class="child-title">服务体验</h2>
            <div class="child-content">
                <div class="process" id="czdz_process">
                    <h3 class="process-title">昨日充值到账平均时间</h3>
                    <div class="process-content" >
                        <div class="process-ing process-yellow" data-process="60" style="width: 51%;"></div>
                    </div>
                    <h3 class="process-second"><span>10</span>秒</h3>
                </div>
                <div class="process" id="txdz_process">
                    <h3 class="process-title">昨日提现到账平均时间</h3>
                    <div class="process-content" >
                        <div class="process-ing process-blue" data-process="80" style="width: 61%;"></div>
                    </div>
                    <h3 class="process-second"><span>5</span>秒</h3>
                </div>
            </div>
        </div>
        <!-- consumtime over -->

        <!-- recharge -->
        <div class="child recharge">
            <h2 class="child-title">充值方式</h2>
            <div class="child-content">
                <a href="#"><div class="recharge-child">
                    <div class="icon"><img src="<%=path%>/front/images/index/recharge_wechat.png" alt=""></div>
                    <h3 class="title">微信支付</h3>
                </div></a>
                <a href="#"><div class="recharge-child">
                    <div class="icon"><img src="<%=path%>/front/images/index/recharge_alipay.png" alt=""></div>
                    <h3 class="title">支付宝</h3>
                </div></a>
                <a href="#"><div class="recharge-child">
                    <div class="icon"><img src="<%=path%>/front/images/index/recharge_yinlian.png" alt=""></div>
                    <h3 class="title">银联</h3>
                </div></a>
            </div>
        </div>
        <!-- recharge over -->

        <!-- download -->
        <!-- 
        <div class="child download" id="phonecodediv">
            <h2 class="child-title">手机下载</h2>
            <div class="child-content">
                <div class="download-child download-child-android" onclick="javascript:$('.popup-android').show();$('.popup-iphone').hide()">
                    <div class="download-child-img" ><img src="<%=path%>/front/images/index/download-android.png" alt=""></div>
                    <h3 class="download-child-title">支持Android2.2或以上</h3>
                </div>
                <div class="download-child download-child-ios" onclick="javascript:$('.popup-iphone').show();$('.popup-android').hide()">
                    <div class="download-child-img"><img src="<%=path%>/front/images/index/download-ios.png" alt=""></div>
                    <h3 class="download-child-title">支持IOS4.3或以上</h3>
                </div>
            </div>
        </div>
        -->
        <!-- download over -->

        <!-- wapcode -->
        <div class="child wapcode" id="wapcodediv" style="display: none;">
            <h2 class="child-title">移动版二维码</h2>
            <div class="child-content">
                <div class="btn code-btn">
                    <p class="btn-title">扫描二维码</p>
                    <div class="code">
                        <img id="wapcode" class="img" src="<%=path%>/front/images/wap/code.jpg" alt="">
                        <p class="title">扫码打开手机版</p>
                        <img class="pointer" src="<%=path%>/front/images/wap/pointer.png" alt="">
                    </div>
                </div>
               
            </div>
            <!-- <div class="child-content">
                <img src="images/wap/code.jpg" alt="">
            </div> -->
        </div>
        <!-- wapcode over -->

    </div>
    <!-- other over -->


</div></div>
<!-- main over -->

<!-- footer over -->
<jsp:include page="/front/include/footer.jsp"></jsp:include>
</body>
</html>