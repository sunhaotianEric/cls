<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%  
    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String partner = request.getParameter("partner");//商户ID
	String ordernumber = request.getParameter("ordernumber");//商户订单号
	String orderstatus = request.getParameter("orderstatus");//订单结果
	String paymoney = request.getParameter("paymoney");//订单金额
	String sysnumber = request.getParameter("sysnumber");//银宝商务订单号
	String attach = request.getParameter("attach");//备注信息
	String sign = request.getParameter("sign");//MD5签名
	log.info("银宝支付成功通知平台处理信息如下: 商户ID: "+partner+" 商户订单号: "+ordernumber+" 订单结果: "+orderstatus+" 订单金额: "+paymoney+" 银宝商务订单号: "+sysnumber+" 备注信息: "+
			attach+"MD5签名: "+sign);
    
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(ordernumber);
	if(rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1){
		log.error("根据订单号[" + ordernumber + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知银宝报文接收成功
	    out.println("ok");
		return;
	}
	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
	if(rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + ordernumber + "]查找充值订单记录为空");
		//通知银宝报文接收成功
	    out.println("ok");
		return;
	}
	
	//签名
	String Md5key = rechargeWithDrawOrder.getSign();
	String MARK = "&";
	String md5 = "partner=" + partner + MARK + "ordernumber=" + ordernumber + MARK + "orderstatus=" + orderstatus + MARK + "paymoney=" + paymoney + Md5key;
	//Md5加密串
	String WaitSign = md5Util.getMD5ofStr(md5);

	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if(WaitSign.compareTo(sign)==0){
		boolean dealResult = false;
		if(!StringUtils.isEmpty(orderstatus) && orderstatus.equals("1")){
			BigDecimal factMoneyValue = new BigDecimal(paymoney);
			try {
				dealResult = rechargeWithDrawOrderService.rechargeOrderSuccessDeal(ordernumber,factMoneyValue);
			}catch(UnEqualVersionException e) {
		   		log.error("银宝在线充值时发现版本号不一致,订单号["+ordernumber+"]");
		   		//启动新进程进行多次重试
		   		UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION
		   				, ordernumber,factMoneyValue,"");
		   		thread.start();
			} catch(Exception e){
				//异常不进行重试
				log.error(e.getMessage());
			}	
		} else {
			if(!orderstatus.equals("1")){
				log.info("银宝支付处理错误支付结果："+orderstatus);
			}
		}
		log.info("银宝订单处理入账结果:{}", dealResult);
		//通知银宝报文接收成功
	    out.println("ok");
		return;
	}else{
		log.info("MD5校验失败,报文返回MD5签名内容["+ sign +"],计算MD5签名内容[" + WaitSign + "]");
		//让第三方继续补发
	    out.println("error");
		return;
	}
%>