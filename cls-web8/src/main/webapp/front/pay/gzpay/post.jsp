<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String orderMoney = request.getParameter("OrderMoney");//订单金额 
	String payId = request.getParameter("PayId");//支付的银行接口
	String payType = request.getParameter("payType");//充值类型
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 

    
	//用户登录校验
	User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
	//充值金额校验
    BigDecimal money = new BigDecimal(orderMoney);
	money = money.setScale(2, BigDecimal.ROUND_DOWN);
    
    //生成订单号
    String orderId=MakeOrderNum.makeOrderNum("CZ");
	log.info("业务系统[{}],用户名[{}],发起了公众号支付，生成订单号[{}],支付金额[{}],PayId[{}],payType[{}],chargePayId[{}]",
			currentUser.getBizSystem(), currentUser.getUserName(), orderId, orderMoney, payId, payType, chargePayId);
	
	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));
    
	//特殊处理-从终端号取出作为图片二维码地址
	String imgUrl = chargePay.getTerminalId();
	
	
    //构建订单
	RechargeOrder rechargeOrder = new RechargeOrder();
	//用户ID
	rechargeOrder.setUserId(currentUser.getId());
	//快捷ID
	rechargeOrder.setChargePayId(chargePay.getId());
	//用户名称
	rechargeOrder.setUserName(currentUser.getUserName());
	//申请金额
	rechargeOrder.setApplyValue(money);
	//订单创建时间
	rechargeOrder.setCreatedDate(new Date());
	//充值类型
	rechargeOrder.setPayType(payType);
	//第三方充值类型
    rechargeOrder.setThirdPayType("GZPAY");
	//操作描述
	rechargeOrder.setOperateDes(EFundPayType.valueOf(payType).getDescription());
	//流水号
	rechargeOrder.setSerialNumber(orderId);
	rechargeOrder.setPostscript("");
	rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT.name());
	rechargeOrder.setBankName("");
	rechargeOrder.setSubbranchName("");
	rechargeOrder.setBankAccountName("");
	rechargeOrder.setBankCardNum("");
	rechargeOrder.setBankType("");
	rechargeOrder.setRefType(EFundRefType.THIRDPAY.name());
	rechargeOrder.setBizSystem(currentUser.getBizSystem());
	rechargeOrder.setFromType(currentUser.getLoginType());
	rechargeWithDrawOrderService.insertSelective(rechargeOrder);
	  
%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
<style>
body {
	text-align: center;
}
img {
	margin-top: 60px;
	width:400px;
	height:auto;
}
p {
	font-size: 36px;
}
</style>
</head>

<body>
	<img id="weixinImg" src='<%=imgUrl%>'></img>
	<p>
		请使用您的微信扫描二维码,并填写您的会员名称和提交订单的金额进行下一步支付
	</p>
	<p>
		手机端用户可以进行截图后，使用扫一扫选择相册中的图片进行操作
	</p>
</body>
</body>
</html>
