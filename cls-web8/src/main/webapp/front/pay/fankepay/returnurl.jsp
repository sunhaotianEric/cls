<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@page import="java.io.*"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="net.sf.json.JSONObject"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.extvo.returnModel.DuDuPayReturnParam"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
    
    //订单状态: 1待支付，2成功，3冻结，-1失败,
	String status = request.getParameter("status");
    //商户订单号.
	String merchant_order_no = request.getParameter("merchant_order_no");
    //支付时间.
	String payment_time = request.getParameter("payment_time");
    //支付金额.
	String trade_amount = request.getParameter("trade_amount");
    //商户号.
	String merchant_no = request.getParameter("merchant_no");
    //备注信息.
	String attach = request.getParameter("attach");
    //签名
	String sign = request.getParameter("sign");
    
	log.info("凡客支付成功通知平台处理信息如下: 商户ID:" + merchant_no + " 订单号:" + merchant_order_no + "订单金额:" + trade_amount + " 元," + "签名:" + sign);

	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService) ApplicationContextUtil.getBean("rechargeOrderService");
	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(merchant_order_no);
	if (rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1) {
		log.error("根据订单号[" + merchant_order_no + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知凡客支付报文接收成功
		response.getOutputStream().write("success".getBytes("UTF-8"));
	}
	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
	if (rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + merchant_order_no + "]查找充值订单记录为空");
		//通知凡客支付报文接收成功
		response.getOutputStream().write("success".getBytes("UTF-8"));
	}
	//获取秘钥.
	String Md5key = rechargeWithDrawOrder.getSign();
	String MARK = "&";
	//拼接加密字符串.
	String mD5Str = "attach=" + attach + MARK +
					"merchant_no=" + merchant_no + MARK +
					"merchant_order_no=" + merchant_order_no + MARK + 
					"payment_time=" + payment_time + MARK + 
					"status=" + status + MARK + 
					"trade_amount=" + trade_amount + MARK +
					"key=" + Md5key;
	
	log.info("当前MD5源码:" + mD5Str);
	//Md5加密串并且大写
	String WaitSign = Md5Util.getMD5ofStr(mD5Str);
	log.info("加密后的MD5: " + WaitSign);

	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if (WaitSign.compareTo(sign) == 0) {
		boolean dealResult = false;
		if (!StringUtils.isEmpty(status) && status.equals("2")) {
			BigDecimal factMoneyValue = new BigDecimal(trade_amount);
			try {
				dealResult = rechargeWithDrawOrderService.rechargeOrderSuccessDeal(merchant_order_no, factMoneyValue);
			} catch (UnEqualVersionException e) {
				log.error("凡客支付充值时发现版本号不一致,订单号[" + merchant_order_no + "]");
				//启动新进程进行多次重试
				UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, merchant_order_no,
						factMoneyValue, "");
				thread.start();
			} catch (Exception e) {
				//异常不进行重试
				log.error(e.getMessage());
			}
		} else {
			if (!status.equals("2")) {
				log.info("凡客支付处理错误支付结果：" +  status.equals("2"));
			}
		}
		log.info("凡客支付付订单处理入账结果:{}", dealResult);
		//通知凡客付报文接收成功
		out.println("success");
		return;
	} else {
		log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容[" + WaitSign + "]");
		out.println("success");
		return;
	}
%>