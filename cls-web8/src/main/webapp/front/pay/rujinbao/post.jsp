<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.pay.model.RuJinBaoPay"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
<%  

    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
    String orderMoneyYuan = OrderMoney;
 
    
    User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
	
	/* Date currTime = new Date();
    //时间以yyyy-MM-dd HH:mm:ss的方式表示
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.CHINA);
    //时间以yyyyMMDDHHmmss的方式表示 
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss",Locale.CHINA);
    String webdate=new String(formatter1.format(currTime));
    String TradeDate=new String(formatter2.format(currTime)); */

	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
    RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.getPayMemmberMsg(Long.valueOf(chargePayId));
	String  orderId = MakeOrderNum.makeOrderNum("CZ");
	log.info("用户名称: "+currentUser.getUserName()+" 用户 ID号: "+currentUser.getId()+" 生成订单号: "+orderId+" 订单金额: "+OrderMoney);
	
	
	 if (!"".equals(OrderMoney)) 
	{	BigDecimal a;
		a = new BigDecimal(OrderMoney); //使用分进行提交
		OrderMoney=String.valueOf(a.setScale(2));
	}
	else{
		OrderMoney = ("0");
	}
	 
	 
	String payUrl = chargePay.getPayUrl();//转发支付网关地址
	String Address = chargePay.getAddress(); //最终入金宝地址
	log.info("payUrl: "+payUrl+" Address: "+Address);
	
	//入金宝请求对象
	RuJinBaoPay ruJinBaoPay =new RuJinBaoPay();
    //商户ID
    ruJinBaoPay.setMerchant_no(chargePay.getMemberId());
    //银行类型
    ruJinBaoPay.setVersion("1.0.3");
    //商户订单号
    ruJinBaoPay.setOut_trade_no(orderId);
    //金额
    ruJinBaoPay.setTotal_fee(OrderMoney);
    //下行异步通知地址
    //chargePay.getReturnUrl()
    ruJinBaoPay.setNotify_url(chargePay.getReturnUrl());
    //下行同步通知地址
    //chargePay.getNotifyUrl()
    ruJinBaoPay.setPage_url(chargePay.getNotifyUrl());
    //备注信息
    ruJinBaoPay.setBody("money");
    
    ruJinBaoPay.setChannel("default");
    
    ruJinBaoPay.setPayment_type(payId);
    
    ruJinBaoPay.setPayment_bank(payId);
    Date currTime = new Date();
    SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddHHmmss",Locale.CHINA);
	String trade_time = sf.format(currTime);
	ruJinBaoPay.setTrade_time(trade_time);
	
	ruJinBaoPay.setUser_account("123");
	
	//生成md5密钥签名
    String Md5key = chargePay.getSign();///////////md5密钥（KEY）
	String MARK = "&";
	/* partner={}&banktype={}&paymoney={}&ordernumber={}&callbackurl={}key
			body=&channel=default&merchant_no=1514990594078637&notify_url=http://myhost.com/paya
pi/callback.htm&out_trade_no=2017070902237&page_url=http://myhost.com/payapi/page.htm&
payment_bank=&payment_type=wxpay&total_fee=100&trade_time=&user_account=&version=1.
0.3dbyk1eimdokaysmzrowhzi5c7fchuesz
			*/
	String md5 =new String("body="+ruJinBaoPay.getBody()+MARK+"channel="+ruJinBaoPay.getChannel()+MARK+"merchant_no="+ruJinBaoPay.getMerchant_no()
	+MARK+"notify_url="+ruJinBaoPay.getNotify_url()+MARK+"out_trade_no="+ruJinBaoPay.getOut_trade_no()+MARK+"page_url="+ruJinBaoPay.getPage_url()
	+MARK+"payment_bank="+ruJinBaoPay.getPayment_type()+MARK+"payment_type="+ruJinBaoPay.getPayment_type()+MARK+"total_fee="+ruJinBaoPay.getTotal_fee()+MARK+
	"trade_time="+ruJinBaoPay.getTrade_time()+MARK+"user_account="+ruJinBaoPay.getUser_account()+MARK+"version="+ruJinBaoPay.getVersion()+chargePay.getSign());//MD5签名格式
	
	String Signature = md5Util.getMD5ofStr(md5);//计算MD5值
	System.out.println(md5);
	System.out.println(Signature);
    //交易签名
    ruJinBaoPay.setSign(Signature);
    log.info("md5原始串: "+md5 + "生成后的交易签名:" + Signature);
    //冲值订单
	RechargeOrder rechargeOrder = new RechargeOrder();
	//用户ID
	rechargeOrder.setUserId(currentUser.getId());
	//快捷ID
	rechargeOrder.setChargePayId(chargePay.getId());
	//用户名称
	rechargeOrder.setUserName(currentUser.getUserName());
	//申请金额
	rechargeOrder.setApplyValue(new BigDecimal(orderMoneyYuan));
	//订单创建时间
	rechargeOrder.setCreatedDate(new Date());
	//充值类型
	rechargeOrder.setPayType(payType);
	//第三方充值类型
	rechargeOrder.setThirdPayType("RUJINBAO");
	//操作描述
	rechargeOrder.setOperateDes(EFundPayType.valueOf(payType).getDescription());
	//流水号
	rechargeOrder.setSerialNumber(orderId);
	rechargeOrder.setPostscript("");
	rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT.name());
	rechargeOrder.setBankName("");
	rechargeOrder.setSubbranchName("");
	rechargeOrder.setBankAccountName("");
	rechargeOrder.setBankCardNum("");
	rechargeOrder.setBankType("");
	rechargeOrder.setBizSystem(currentUser.getBizSystem());
	rechargeOrder.setRefType(EFundRefType.THIRDPAY.name());
	rechargeOrder.setFromType(currentUser.getLoginType());
	rechargeWithDrawOrderService.insertSelective(rechargeOrder);
	log.info("提交中转支付网关地址["+payUrl+"],实际入金宝支付网关地址["+Address +"],交易报文: "+ruJinBaoPay);
 %>
 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>

<body onload="pay.submit()">
<form method="get" name="pay" id="pay" action="<%=Address%>">
<TABLE>
<TR>
	<TD>
	<input name="merchant_no" type="hidden" value="<%=ruJinBaoPay.merchant_no%>"></input>
	<input name="version" type="hidden" value="<%=ruJinBaoPay.version%>"></input>
	<input name="out_trade_no" type="hidden" value="<%=ruJinBaoPay.out_trade_no%>"></input>
	<input name="payment_type" type="hidden" value="<%=ruJinBaoPay.payment_type%>"></input>
	<input name="payment_bank" type="hidden" value="<%=ruJinBaoPay.payment_bank%>"></input>
	<input name="notify_url" type="hidden" value="<%=ruJinBaoPay.notify_url%>"></input>
	<input name="page_url" type="hidden" value="<%=ruJinBaoPay.page_url%>"></input>
	<input name="total_fee" type="hidden" value="<%=ruJinBaoPay.total_fee%>"></input>
	<input name="trade_time" type="hidden" value="<%=ruJinBaoPay.trade_time%>"></input>
	<input name="user_account" type="hidden" value="<%=ruJinBaoPay.user_account%>"></input>
	<input name="body" type="hidden" value="<%=ruJinBaoPay.body%>"></input>
	<input name="channel" type="hidden" value="<%=ruJinBaoPay.channel%>"></input>
	<input name="sign" type="hidden" value="<%=ruJinBaoPay.sign%>"></input>
	
	</TD>
</TR>
</TABLE>
	
</form>	

</body>
</html>
