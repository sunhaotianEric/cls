<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.pay.model.JianZhengBaoPay"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
 <%@page errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.InetAddress"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="com.team.lottery.extvo.returnModel.JianZhengBaoReturnPayUrl"%>
<%@page import="com.team.lottery.extvo.returnModel.JianZhengBaoData"%>
<%@page import="java.util.*"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%  

    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
    String orderMoneyYuan = OrderMoney;
 
    
    User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
	
	/* Date currTime = new Date();
    //时间以yyyy-MM-dd HH:mm:ss的方式表示
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.CHINA);
    //时间以yyyyMMDDHHmmss的方式表示 
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss",Locale.CHINA);
    String webdate=new String(formatter1.format(currTime));
    String TradeDate=new String(formatter2.format(currTime)); */

	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
    RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));
	String  orderId=MakeOrderNum.makeOrderNum("CZ");
	log.info("用户名称: "+currentUser.getUserName()+" 用户 ID号: "+currentUser.getId()+" 生成订单号: "+orderId+" 订单金额: "+OrderMoney);
	
	
	if(!"".equals(OrderMoney)) {	
		 BigDecimal a;
		a = new BigDecimal(OrderMoney); //使用分进行提交
		OrderMoney=String.valueOf(a.multiply(new BigDecimal(100)).intValue());
	}
	else{
		OrderMoney = ("0");
	} 
	String payUrl = chargePay.getPayUrl();//转发支付网关地址
	String Address = chargePay.getAddress(); //最终见证宝地址
	log.info("payUrl: "+payUrl+" Address: "+Address);
	
	//见证宝支付请求对象
	JianZhengBaoPay jianZhengBaoPay =new JianZhengBaoPay();
	jianZhengBaoPay.setMerchant_no(chargePay.getMemberId());
	jianZhengBaoPay.setMerchant_order_no(orderId);
	jianZhengBaoPay.setCallback_url(chargePay.getReturnUrl());
	jianZhengBaoPay.setOrder_type("02");
	Date currTime = new Date();
    //时间以yyyy-MM-dd HH:mm:ss的方式表示
    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss",Locale.CHINA);
	jianZhengBaoPay.setOrder_smt_time(formatter.format(new Date()));
	jianZhengBaoPay.setTrade_amount(OrderMoney);
	jianZhengBaoPay.setGoods_name("cz");
	jianZhengBaoPay.setTrade_desc("xj");
	jianZhengBaoPay.setSign_type("01");
	jianZhengBaoPay.setGoods_type("02");
	
    Map param = new TreeMap();
    param.put("callback_url", jianZhengBaoPay.getCallback_url());
    param.put("goods_name", jianZhengBaoPay.getGoods_name());
    param.put("goods_type",jianZhengBaoPay.getGoods_type());
    param.put("merchant_no", jianZhengBaoPay.getMerchant_no());
    param.put("merchant_order_no", jianZhengBaoPay.getMerchant_order_no());
    param.put("order_smt_time", jianZhengBaoPay.getOrder_smt_time());
    param.put("order_type",jianZhengBaoPay.getOrder_type());
    param.put("trade_amount", jianZhengBaoPay.getTrade_amount());
    param.put("trade_desc", jianZhengBaoPay.getTrade_desc());
    param.put("sign_type", jianZhengBaoPay.getSign_type());
    System.out.println("map1:" + param);
    String jSON = JSONUtils.toJSONString(param).substring(1, JSONUtils.toJSONString(param).length()-1);
    System.out.println("jSON: " + jSON);
  //生成md5密钥签名
    String Md5key = chargePay.getSign();///////////md5密钥（KEY）
	String md5 =new String(Md5key+jSON+Md5key);//MD5签名格式
	String Signature = md5Util.getMD5ofStr(md5);//计算MD5值
    //交易签名
    jianZhengBaoPay.setSign(Signature);
    log.info("md5原始串: "+md5 + "生成后的交易签名:" + Signature);
    //冲值订单
	RechargeOrder rechargeOrder = new RechargeOrder();
	//用户ID
	rechargeOrder.setUserId(currentUser.getId());
	//快捷ID
	rechargeOrder.setChargePayId(chargePay.getId());
	//用户名称
	rechargeOrder.setUserName(currentUser.getUserName());
	//申请金额
	rechargeOrder.setApplyValue(new BigDecimal(orderMoneyYuan));
	//订单创建时间
	rechargeOrder.setCreatedDate(new Date());
	//充值类型
	rechargeOrder.setPayType(payType);
	//第三方充值类型
    rechargeOrder.setThirdPayType("JIANZHENGBAO");
	//操作描述
	rechargeOrder.setOperateDes(EFundPayType.valueOf(payType).getDescription());
	//流水号
	rechargeOrder.setSerialNumber(orderId);
	rechargeOrder.setPostscript("");
	rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT.name());
	rechargeOrder.setBankName("");
	rechargeOrder.setSubbranchName("");
	rechargeOrder.setBankAccountName("");
	rechargeOrder.setBankCardNum("");
	rechargeOrder.setBankType("");
	rechargeOrder.setRefType(EFundRefType.THIRDPAY.name());
	rechargeOrder.setBizSystem(currentUser.getBizSystem());
	rechargeOrder.setFromType(currentUser.getLoginType());
	rechargeWithDrawOrderService.insertSelective(rechargeOrder);
	log.info("实际见证宝支付网关地址["+Address +"],交易报文: "+jianZhengBaoPay);
	String strPara="merchant_no="+URLEncoder.encode(jianZhengBaoPay.getMerchant_no(), "utf-8") + "&callback_url="
			+ URLEncoder.encode(jianZhengBaoPay.getCallback_url(), "utf-8") + "&order_type="
			+ URLEncoder.encode(jianZhengBaoPay.getOrder_type(), "utf-8") + "&merchant_order_no="
			+ URLEncoder.encode(jianZhengBaoPay.getMerchant_order_no(), "utf-8") + "&order_smt_time="
			+ URLEncoder.encode(jianZhengBaoPay.getOrder_smt_time(), "utf-8") + "&trade_desc="
			+ URLEncoder.encode(jianZhengBaoPay.getTrade_desc(), "utf-8") + "&goods_name="
			+ URLEncoder.encode(jianZhengBaoPay.getGoods_name(), "utf-8") + "&goods_type="
			+ URLEncoder.encode(jianZhengBaoPay.getGoods_type(), "utf-8") + "&trade_amount="
			+ URLEncoder.encode(jianZhengBaoPay.getTrade_amount(), "utf-8")+"&sign_type="
			+ URLEncoder.encode(jianZhengBaoPay.getSign_type(), "utf-8")+"&sign="
			+ URLEncoder.encode(jianZhengBaoPay.getSign(), "utf-8");
	/* if(payType.equals("ALIPAYWAP")){
		Address="http://wx.eposbank.com/papay";
	} */
	URL url = new URL(Address);
    
	//openConnection函数会根据URL的协议返回不同的URLConnection子类的对象
	//这里URL是一个http,因此实际返回的是HttpURLConnection 
	HttpURLConnection httpConn = (HttpURLConnection) url
			.openConnection();
	httpConn.setRequestMethod("POST");
	//进行连接,实际上request要在下一句的connection.getInputStream()函数中才会真正发到 服务器****待验证
	httpConn.setDoOutput(true);
	httpConn.setDoInput(true);
	httpConn.connect();
	PrintWriter outWrite = new PrintWriter(httpConn.getOutputStream());
	
	outWrite.print(strPara);
    // flush输出流的缓冲
    outWrite.flush();
	// 取得输入流，并使用Reader读取
	BufferedReader reader = new BufferedReader(new InputStreamReader(
			httpConn.getInputStream(),"utf-8"));

	String lines = "";
	String str="";
	while ((str=reader.readLine()) != null) {
		lines += str; 
	}
	reader.close();
	log.info("见证宝支付返回报文:" + lines);
	/* String codeUrl=""; */
	JianZhengBaoReturnPayUrl returnParameter = JSONUtils.toBean(lines,
			JianZhengBaoReturnPayUrl.class);
	if (returnParameter.getCode().equals("608")) {
		JianZhengBaoData jianZhengBaoData=returnParameter.getBody();
		String skipUrl="";
		if(jianZhengBaoData.getParams()!=null){
			skipUrl=jianZhengBaoData.getParams();
		}
		response.sendRedirect(skipUrl);
		return;
		
	} else {
		out.print(returnParameter.getMessage());
		return;
	}
	
 %>