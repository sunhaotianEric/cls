<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.extvo.returnModel.XinMaSendParameter"%>
<%@page import="com.team.lottery.extvo.returnModel.JinHaoReturnParameter"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="java.io.*"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import=" com.team.lottery.pay.util.MerchantApiUtil"%>
<%@page import="java.util.*"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%  
    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
    String merchant_order_no=request.getParameter("merchant_order_no");
    String status=request.getParameter("status");
    String amount=request.getParameter("amount");
    String msg=request.getParameter("msg");
    String prd_ord_no=request.getParameter("prd_ord_no");
    String trade_no=request.getParameter("trade_no");
    String sign=request.getParameter("sign");
    String pay_channel=request.getParameter("pay_channel");
    String payment_time=request.getParameter("payment_time");
    String risk=request.getParameter("risk");
    
	log.info("见证宝支付成功通知平台处理信息如下:商户订单号: "+merchant_order_no+" 订单结果: "+status+" 订单金额: "+amount+"MD5签名: "+sign);
    
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(merchant_order_no);
	if(rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1){
		log.error("根据订单号[" + merchant_order_no + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知见证宝支付报文接收成功
	    out.println("success");
		return;
	}
	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
	if(rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + merchant_order_no + "]查找充值订单记录为空");
		//通知见证宝支付报文接收成功
	    out.println("success");
		return;
	}
	
    //签名
    Map param = new TreeMap();
    param.put("amount", amount);
    param.put("merchant_order_no", merchant_order_no);
    param.put("msg",msg);
    param.put("pay_channel", pay_channel);
    param.put("payment_time", payment_time);
    param.put("prd_ord_no", prd_ord_no);
    param.put("risk",risk);
    param.put("status",status);
    param.put("trade_no", trade_no);
    log.info(param.toString());
    String jSON = JSONUtils.toJSONString(param).substring(1, JSONUtils.toJSONString(param).length()-1);
    System.out.println("jSON: " + jSON);
	String Md5key = rechargeWithDrawOrder.getSign();
	//String MARK = "&";
	String md5 =  Md5key + jSON +Md5key;
	//Md5加密串
	String WaitSign = md5Util.getMD5ofStr(md5);
	/* //Md5加密串
	String WaitSign = md5Util.Sign(md5,Md5key); */

	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if(WaitSign.compareTo(sign)==0){
		boolean dealResult = false;
		if(!StringUtils.isEmpty(status) && status.equals("Success")){
			BigDecimal factMoneyValue = new BigDecimal(amount).divide(new BigDecimal(100));
			try {
				dealResult = rechargeWithDrawOrderService.rechargeOrderSuccessDeal(merchant_order_no,factMoneyValue);
			}catch(UnEqualVersionException e) {
		   		log.error("见证宝支付在线充值时发现版本号不一致,订单号["+merchant_order_no+"]");
		   		//启动新进程进行多次重试
		   		UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION
		   				, merchant_order_no,factMoneyValue,"");
		   		thread.start();
			} catch(Exception e){
				//异常不进行重试
				log.error(e.getMessage());
			}	
		} else {
			if(!status.equals("1")){
				log.info("见证宝支付处理错误支付结果："+status);
			}
		}
		log.info("见证宝支付订单处理入账结果:{}", dealResult);
		//通知见证宝支付报文接收成功
	    out.println("success");
		return;
	}else{
		log.info("MD5校验失败,报文返回MD5签名内容["+ sign +"],计算MD5签名内容[" + WaitSign + "]");
		//让第三方继续补发
	    out.println("FAIL");
		return;
	}
%>