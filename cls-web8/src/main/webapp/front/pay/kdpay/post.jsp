<%@page import="com.team.lottery.enums.EBankInfo"%>
<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@ page import="org.apache.log4j.*,org.apache.commons.logging.*"%>
<%@page import="com.team.lottery.pay.model.KoudaiPay"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
<%  
    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	//String chargePayId = request.getParameter("chargePayId");//支付的银行接口
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	if (!"".equals(OrderMoney)) 
	{	BigDecimal a;
		a = new BigDecimal(OrderMoney).setScale(2, BigDecimal.ROUND_DOWN); 
		OrderMoney=String.valueOf(a);
	} else{
		OrderMoney = "0.00";
	}
	
	  User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	    if(currentUser == null){
	        response.sendRedirect(path+"/gameBet/login.html");
	    }
	
	Date currTime = new Date();

	ChargePayService  chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService   rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.getPayMemmberMsg(Long.valueOf(chargePayId));
	String orderId = MakeOrderNum.makeOrderNum("CZ");
	log.info("用户名称: "+currentUser.getUserName()+" 用户 ID号: "+currentUser.getId()+" 生成订单号: "+orderId+" 订单金额: "+OrderMoney);
	
	//充值类型 ID默认值是1;代表在线银行
    String p_ChannelId="1";
	//根据传入的银行码值进行相应的判断!如果码值为以下值时:充值类型 ID等于传入的银行码值!并且提交表单当中的银行 ID字段设为空字符串!如果充值类型 ID默认值是1;则提交表单当中的银行 ID字段设为传入的银行码值
    if(payId.equals("2")||payId.equals("3")||payId.equals("21")||payId.equals("31")||payId.equals("33")||payId.equals("36"))
    {
    	p_ChannelId=payId;
    }
    //口袋请求对象
    KoudaiPay koudaiPay=new KoudaiPay();
    //卡类交易时的卡号
    koudaiPay.setP_CardId("");
    //卡类交易时的卡密
    koudaiPay.setP_CardPass("");
    //充值类型 ID
    koudaiPay.setP_ChannelId(p_ChannelId);
    if(p_ChannelId.equals("1"))
    {
    	//银行 ID
    	koudaiPay.setP_Description(payId);
    }
    else
    {
    	//银行 ID
    	koudaiPay.setP_Description("");
    }
    //面值/金额（支付金额）
    koudaiPay.setP_FaceValue(OrderMoney);
    //WAP 版快捷支付判断
    koudaiPay.setP_IsSmart("");
    //用户附加信息
    koudaiPay.setP_Notic(currentUser.getBizSystem());
    //同步跳转地址
    koudaiPay.setP_Notify_URL(chargePay.getNotifyUrl());
    //商户订单号
    koudaiPay.setP_OrderId(orderId);
    //产品价格
    koudaiPay.setP_Price("1.00");
    //产品数量
    koudaiPay.setP_Quantity("1");
    //异步通知地址
    koudaiPay.setP_Result_URL(chargePay.getReturnUrl());
    //产品名称
    koudaiPay.setP_Subject("");
    //商户 ID
    koudaiPay.setP_UserId(chargePay.getMemberId());
	
    //生成md5密钥签名
	String SalfStr = chargePay.getSign();///////////md5密钥（KEY）
	String plain = koudaiPay.getP_UserId() + "|" + koudaiPay.getP_OrderId() + "|" + koudaiPay.getP_CardId() + "|" + koudaiPay.getP_CardPass() + "|" + koudaiPay.getP_FaceValue() + "|" + koudaiPay.getP_ChannelId() + "|" + SalfStr;
	String P_PostKey = md5Util.getMD5ofStr(plain, "gb2312").toLowerCase();//计算MD5值
	//String P_IsSmart = "";
	
	//签名认证串
	koudaiPay.setP_PostKey(P_PostKey);
	log.info("md5原始串: "+plain + "生成后的交易签名:" + P_PostKey);

	String payUrl = chargePay.getPayUrl();//转发支付网关地址
	String Address = chargePay.getAddress(); //最终口袋地址
	log.info("payUrl: "+payUrl+" Address: "+Address);
	
	//冲值订单
	RechargeOrder rechargeOrder = new RechargeOrder();
	//用户ID
	rechargeOrder.setUserId(currentUser.getId());
	//快捷ID
	rechargeOrder.setChargePayId(chargePay.getId());
	//用户名称
	rechargeOrder.setUserName(currentUser.getUserName());
	//申请金额
	rechargeOrder.setApplyValue(new BigDecimal(OrderMoney));
	//订单创建时间
	rechargeOrder.setCreatedDate(new Date());
	//流水号
	rechargeOrder.setSerialNumber(orderId);
	rechargeOrder.setPostscript("");
	rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT.name());
	rechargeOrder.setBankName("");
	rechargeOrder.setSubbranchName("");
	rechargeOrder.setBankAccountName("");
	rechargeOrder.setBankCardNum("");
	rechargeOrder.setPayType(payType);
	//第三方充值类型
	rechargeOrder.setThirdPayType("KDPAY");
	rechargeOrder.setOperateDes(EFundPayType.valueOf(payType).getDescription());
	//默认使用微信
	if(StringUtils.isEmpty(chargePayId) || "21".equals(chargePayId)){
		rechargeOrder.setBankType(EBankInfo.WEIXIN.getCode());
    }else{
    	rechargeOrder.setBankType("");
    }
	rechargeOrder.setBizSystem(currentUser.getBizSystem());
	rechargeOrder.setRefType(EFundRefType.THIRDPAY.name());
	rechargeOrder.setFromType(currentUser.getLoginType());
	rechargeWithDrawOrderService.insertSelective(rechargeOrder);
	log.info("提交中转支付网关地址["+payUrl+"],实际闪付支付网关地址["+Address +"],交易报文: "+koudaiPay);
 %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>

<body onload="pay.submit()">
	<form method="post" name="pay" id="pay" action="<%=payUrl%>">
		<table>
			<tr>
				<td><input name='Address' type='hidden' value="<%=Address%>" />
					<input name='P_UserId' type='hidden' value="<%=koudaiPay.getP_UserId()%>" /> 
					<input name='P_OrderId' type='hidden' value="<%=koudaiPay.getP_OrderId()%>" /> 
			        <input name='P_CardId' type='hidden' value= "<%=koudaiPay.getP_CardId()%>" />
			        <input name='P_CardPass' type='hidden' value= "<%=koudaiPay.getP_CardPass()%>" /> 
			        <input name='P_FaceValue' type='hidden' value="<%=koudaiPay.getP_FaceValue()%>" />
					<input name='P_ChannelId' type='hidden' value="<%=koudaiPay.getP_ChannelId()%>" />
					<input name='P_Subject' type='hidden' value="<%=koudaiPay.getP_Subject()%>" /> 
					<input name='P_Price' type='hidden' value="<%=koudaiPay.getP_Price()%>" /> 
					<input name='P_Quantity' type='hidden' value="<%=koudaiPay.getP_Quantity()%>" /> 
					<input name='P_Description' type='hidden' value="<%=koudaiPay.getP_Description()%>" /> 
					<input name='P_Notic' type='hidden' value="<%=koudaiPay.getP_Notic()%>" /> 
					<input name='P_Result_URL' type='hidden' value="<%=koudaiPay.getP_Result_URL()%>" /> 
					<input name='P_Notify_URL' type='hidden' value="<%=koudaiPay.getP_Notify_URL()%>" /> 
					<input name='P_PostKey' type='hidden' value="<%=koudaiPay.getP_PostKey()%>" />
					<input name='P_IsSmart' type='hidden' value="<%=koudaiPay.getP_IsSmart()%>" />
			  </td>
			</tr>
		</table>
	</form>

</body>
</html>
