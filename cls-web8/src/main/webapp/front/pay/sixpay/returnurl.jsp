<%@page import="com.team.lottery.pay.model.muming.MuMingPayReturn"%>
<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="com.team.lottery.pay.model.jindouyun.JinDouYunPayReturn"%>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@page import="java.io.*"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="net.sf.json.JSONObject"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.extvo.returnModel.DuDuPayReturnParam"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
 	// 商户号.
  	String mchNo = request.getParameter("mchNo");
 	// 平台订单号.
  	String tradeno = request.getParameter("tradeno");
 	// 支付金额.
  	String money = request.getParameter("money");
 	// 第六支付内部订单号.
  	String orderid = request.getParameter("orderid");
 	// 支付方式.
  	String paytype = request.getParameter("paytype");
 	// 支付状态.
  	String status = request.getParameter("status");
 	// 备注.
  	String attach = request.getParameter("attach");
 	// 签名.
  	String sign = request.getParameter("sign");
    // 日志打印.
   	log.info("第六支付支付,回调返回参数商户号=" + mchNo + "平台订单号=" + tradeno + "支付金额 = " + money + "第六支付内部订单号 = " + orderid + "支付方式=" + paytype + "支付状态=" + status + "备注=" + attach + "签名=" + sign);

	RechargeOrderService rechargeOrderService = (RechargeOrderService) ApplicationContextUtil.getBean("rechargeOrderService"); 
	RechargeOrder rechargeOrder = rechargeOrderService.getRechargeOrderByOderNumber(tradeno);
	if (rechargeOrder == null ) {
		log.error("根据订单号[" + tradeno + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知第六支付报文接收成功
		out.print("success");
	}
	//获取秘钥.
	String Md5key = rechargeOrder.getSign();
	String MARK = "&";
	//拼接加密字符串.
	String mD5Str = "attach=" + attach + MARK +
					"mchNo=" + mchNo + MARK +
					"money=" + money + MARK + 
					"orderid=" + orderid + MARK + 
					"paytype=" + paytype + MARK + 
					"status=" + status + MARK + 
					"tradeno=" + tradeno + MARK + 
					"key=" + Md5key;
	
	log.info("当前MD5源码:" + mD5Str);
	//Md5加密小写
	String WaitSign = Md5Util.getMD5ofStr(mD5Str);
	log.info("加密后的MD5: " + WaitSign);

	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if (WaitSign.compareTo(sign) == 0) {
		boolean dealResult = false;
		if (!StringUtils.isEmpty(status) && status.equals("success")) {
			// 支付金额.
			BigDecimal factMoneyValue = new BigDecimal(money);
			try {
				dealResult = rechargeOrderService.rechargeOrderSuccessDeal(tradeno, factMoneyValue);
			} catch (UnEqualVersionException e) {
				log.error("GT支付支付充值时发现版本号不一致,订单号[" + tradeno + "]");
				//启动新进程进行多次重试
				UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, tradeno,
						factMoneyValue, "");
				thread.start();
			} catch (Exception e) {
				//异常不进行重试
				log.error(e.getMessage());
			}
		} else {
			if (!status.equals("success")) {
				log.info("第六支付处理错误支付结果payState为：" +  status);
				
			}
		}
		log.info("第六支付订单处理入账结果:{}", dealResult);
		//通知第三方报文接收成功
		out.println("success");
		return;
	} else {
		log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容[" + WaitSign + "]");
		out.println("success");
		return;
	}
%>