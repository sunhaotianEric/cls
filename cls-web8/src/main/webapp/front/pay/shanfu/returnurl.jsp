<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String MemberID = request.getParameter("MemberID");//商户号
	String TerminalID = request.getParameter("TerminalID");//商户终端号
	String TransID = request.getParameter("TransID");//商户流水号
	String Result = request.getParameter("Result");//支付结果
	String ResultDesc = request.getParameter("ResultDesc");//支付结果描述
	String FactMoney = request.getParameter("FactMoney");//实际成功金额
	String AdditionalInfo = request.getParameter("AdditionalInfo");//订单附加消息	
	String SuccTime = request.getParameter("SuccTime");//支付完成时间
	String Md5Sign = request.getParameter("Md5Sign");//MD5签名
	log.info("闪付支付成功通知平台处理信息如下: 商户号: "+MemberID+" 商户终端号: "+TerminalID+" 订单号: "+TransID+" 支付结果: "+Result+" 支付结果描述: "+ResultDesc+" 实际成功金额: "+
	         FactMoney+" 支付完成时间: "+SuccTime+" 第三方返回MD5签名: "+Md5Sign);
	
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(TransID);
	if(rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1){
		log.error("根据订单号[" + TransID + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知闪付接收报文成功	
	    out.println("OK");
		return;
	}
	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
	if(rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + TransID + "]查找充值订单记录为空");
		//通知闪付接收报文成功
	    out.println("OK");
		return;
	}

	//签名
	String Md5key = rechargeWithDrawOrder.getSign();
	String MARK = "~|~";
	String md5 = "MemberID=" + MemberID + MARK + "TerminalID=" + TerminalID + MARK + "TransID=" + TransID + MARK + "Result=" + Result + MARK + "ResultDesc=" + ResultDesc + MARK
	+ "FactMoney=" + FactMoney + MARK + "AdditionalInfo=" + AdditionalInfo + MARK + "SuccTime=" + SuccTime
	+ MARK + "Md5Sign=" + Md5key;
	//Md5加密串
	String WaitSign = md5Util.getMD5ofStr(md5);

	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if(WaitSign.compareTo(Md5Sign)==0){
		boolean dealResult = false;
		if(!StringUtils.isEmpty(Result) && Result.equals("1")){
			//将实际成功金额除以100
			BigDecimal factMoneyValue = new BigDecimal(FactMoney).divide(new BigDecimal("100"));
			try {
				dealResult = rechargeWithDrawOrderService.rechargeOrderSuccessDeal(TransID,factMoneyValue);
			}catch(UnEqualVersionException e) {
		   		log.error("闪付在线充值时发现版本号不一致,订单号["+TransID+"]");
		   		//启动新进程进行多次重试
		   		UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION
		   				, TransID,factMoneyValue,SuccTime);
		   		thread.start();
			} catch(Exception e){
				//异常不进行重试
				log.error(e.getMessage());
			}
		} else {
			if(!Result.equals("1")){
				log.info("闪付支付处理错误支付结果："+Result+" 支付结果描述："+ResultDesc);
			}
		}
		log.info("闪付订单处理入账结果:{}", dealResult);
		//通知闪付接收报文成功
	    out.println("OK");
		
	}else{
		log.info("MD5校验失败,报文返回MD5签名内容["+ Md5Sign +"],计算MD5签名内容[" + WaitSign + "]");
		//不输出OK，让第三方继续补发
		out.println("ERROR");
	}
%>