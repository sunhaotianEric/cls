<%@page import="com.team.lottery.pay.model.muming.MuMingPayReturn"%>
<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="com.team.lottery.pay.model.jindouyun.JinDouYunPayReturn"%>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@page import="java.io.*"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="net.sf.json.JSONObject"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.extvo.returnModel.DuDuPayReturnParam"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
    
    //获取当前的状态
   	BufferedReader streamReader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
   	StringBuilder responseStrBuilder = new StringBuilder();
   	String inputStr;
   	while ((inputStr = streamReader.readLine()) != null) {
   		responseStrBuilder.append(inputStr);
   	}
   	String resonseStr = responseStrBuilder.toString();
   	log.info("慕名支付回调通知报文: " + resonseStr);
   	JSONObject jsonObject = JSONUtils.toJSONObject(resonseStr);
    
 // 保存http request相关头部请求参数
 		Map<String, String> map = new HashMap<String, String>();
 		Enumeration<?> enum1 = request.getHeaderNames();
 		while (enum1.hasMoreElements()) {
 			String key = (String) enum1.nextElement();
 			String value = request.getHeader(key);
 			map.put(key, value);
 		}

 		log.info("request相关头部请求参数:"+map.toString());
 		map.clear();

 		// 保存http request相关请求参数
 		Enumeration paramNames = request.getParameterNames();
 		while (paramNames.hasMoreElements()) {
 			String paramName = (String) paramNames.nextElement();

 			String[] paramValues = request.getParameterValues(paramName);
 			if (paramValues.length > 0) {
 				String paramValue = paramValues[0];
 				if (paramValue.length() != 0) {
 					map.put(paramName, paramValue);
 				}
 			}
 		}
 		
 	 log.info("request相关请求参数:"+map.toString());
 		
    //商户编号
	String memberid = jsonObject.getString("memberid");
    //订单号
	String orderid = jsonObject.getString("orderid");
    //订单金额
	String amount = jsonObject.getString("amount");
    //交易流水号
	String transaction_id = jsonObject.getString("transaction_id");
    //交易时间
	String datetime = jsonObject.getString("datetime");
    //交易状态
	String returncode = jsonObject.getString("returncode");
	 //扩展返回
	String attach = jsonObject.getString("attach");
	//签名
	String sign = jsonObject.getString("sign");
	 //扩展返回
	String out_trade_no = jsonObject.getString("out_trade_no");
    // 新建慕名回调对象.
    MuMingPayReturn muMingPayReturn = new MuMingPayReturn();
    muMingPayReturn.setAmount(amount);
    muMingPayReturn.setAttach(attach);
    muMingPayReturn.setDatetime(datetime);
    muMingPayReturn.setMemberid(memberid);
    muMingPayReturn.setOrderid(orderid);
    muMingPayReturn.setReturncode(returncode);
    muMingPayReturn.setSign(sign);
    muMingPayReturn.setTransaction_id(transaction_id);
    // 打印日志.通知成功.
	log.info(muMingPayReturn.toString());

	RechargeOrderService rechargeOrderService = (RechargeOrderService) ApplicationContextUtil.getBean("rechargeOrderService"); 
	List<RechargeOrder> rechargeOrders = rechargeOrderService.getRechargeOrderBySerialNumber(orderid);
	if (rechargeOrders == null || rechargeOrders.size() != 1) {
		log.error("根据订单号[" + orderid + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知慕名云支付报文接收成功
		out.print("OK");
	}
	RechargeOrder rechargeWithDrawOrder = rechargeOrders.get(0);
	if (rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + orderid + "]查找充值订单记录为空");
		//通知慕名云支付报文接收成功
		out.print("OK");
	}
	//获取秘钥.
	//String Md5key = rechargeWithDrawOrder.getSign();
	//String MARK = "&";
	//拼接加密字符串.
	/* String mD5Str = "amount=" + amount + MARK +
					"datetime=" + datetime + MARK +
					"memberid=" + memberid + MARK + 
					"orderid=" + orderid + MARK + 
					"returncode=" + returncode + MARK + 
					"transaction_id=" + transaction_id + MARK + 
					"key=" + Md5key; */
	
	//log.info("当前MD5源码:" + mD5Str);
	//Md5加密串并且大写
	//String WaitSign = Md5Util.getMD5ofStr(mD5Str);
	//log.info("加密后的MD5: " + WaitSign);

	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	/* if (WaitSign.compareTo(sign) == 0) { */
		boolean dealResult = false;
		if (!StringUtils.isEmpty(returncode) && returncode.equals("00")) {
			BigDecimal factMoneyValue = new BigDecimal(amount);
			try {
				dealResult = rechargeOrderService.rechargeOrderSuccessDeal(orderid, factMoneyValue);
			} catch (UnEqualVersionException e) {
				log.error("慕名支付充值时发现版本号不一致,订单号[" + orderid + "]");
				//启动新进程进行多次重试
				UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, orderid,
						factMoneyValue, "");
				thread.start();
			} catch (Exception e) {
				//异常不进行重试
				log.error(e.getMessage());
			}
		} else {
			if (!returncode.equals("00")) {
				log.info("慕名支付处理错误支付结果returncode为：" +  returncode);
			}
		}
		log.info("慕名支付订单处理入账结果:{}", dealResult);
		//通知凡客付报文接收成功
		out.println("OK");
		return;
	/* }else {
		log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容[" + WaitSign + "]");
		out.println("OK");
		return;
	} */
%>