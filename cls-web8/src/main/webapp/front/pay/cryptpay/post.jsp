<%@page import="com.team.lottery.pay.util.Base64Utils"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.pay.model.cryptpay.CryptPayToken"%>
<%@page import="com.team.lottery.pay.model.cryptpay.CryptPayTokenData"%>
<%@page import="com.team.lottery.pay.model.cryptpay.CryptPayAddress"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@page import="com.team.lottery.vo.RechargeConfig"%>
<%@page import="com.team.lottery.service.RechargeConfigService"%>
<%
    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String orderMoney = request.getParameter("OrderMoney");//订单金额 
	String payId = request.getParameter("PayId");//支付的银行接口
	String payType = request.getParameter("payType");//充值类型
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String serialNumber = request.getParameter("serialNumber");
	if(StringUtils.isBlank(orderMoney) || StringUtils.isBlank(chargePayId)) {
		out.print("参数传递错误");
		return;
	}
    
	//用户登录校验
	User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
    
	log.info("业务系统[{}],用户名[{}],发起了密付支付，生成订单号[{}],支付金额[{}],PayId[{}],payType[{}],chargePayId[{}]",
			currentUser.getBizSystem(), currentUser.getUserName(), serialNumber, orderMoney, payId, payType, chargePayId);
	
	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));
    
	//充值金额校验
    BigDecimal money = new BigDecimal(orderMoney);
	
	String payUrl = chargePay.getPayUrl();//转发支付网关地址
	String address = chargePay.getAddress(); //实际密付地址
	log.info("密付支付报文转发地址payUrl: "+payUrl+",实际网关地址address: "+address);

	// Get Token /:appId/token/:userId
	
	String getTokenUrl = address + "/" + chargePay.getMemberId() + "/token/" + currentUser.getId() 
							+ "?appsecret=" + chargePay.getSign();
	log.info("密付支付获取token url地址:" + getTokenUrl);
	String result = HttpClientUtil.doGet(getTokenUrl, null);
	log.info("密付支付提交支付报文，返回内容:{}", result);
	if(StringUtils.isBlank(result)) {
		out.print("发起获取token失败");
		return;
	} 
	CryptPayToken payToken = JSONUtils.toBean(result, CryptPayToken.class);
	if(payToken != null){
		if("0".equals(payToken.getType())) {
			String token = payToken.getData().getToken();
			log.info("获取到 token:{}", token);
			// 跳转加密货币购买OTC平台
			String getOtcAddressUrl = address + "/" + chargePay.getMemberId() + "/otcurl/" + currentUser.getId()
									+ "?token=" + token + "&userName=" + currentUser.getUserName();
			log.info("密付支付获取otc平台支付地址:" + getOtcAddressUrl);
			String otcResult = HttpClientUtil.doGet(getOtcAddressUrl, null);
			log.info("密付支付otcResult:" + otcResult);
			if(StringUtils.isBlank(result)) {
				out.print("发起获取otc支付地址失败");
				return;
			} 
			CryptPayAddress payAddress = JSONUtils.toBean(otcResult, CryptPayAddress.class);
			log.info("密付支付payAddress:" + payAddress);
			if(payAddress != null) {
				if("0".equals(payAddress.getType())) {
					String otcAddress = payAddress.getData();
					log.info("获取到otc支付地址:{}", otcAddress);
					
					
					//同步跳转地址
					String backopenurl = "";
					if(chargePay.getNotifyUrl().startsWith("http://") || chargePay.getNotifyUrl().startsWith("https://")) {
						backopenurl = chargePay.getNotifyUrl();
					} else {
						String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
						backopenurl = basePath + chargePay.getNotifyUrl();
					}
					
					log.info("backopenurl base64 before:{}", backopenurl);
					backopenurl = Base64Utils.encode(backopenurl.getBytes());
					log.info("backopenurl base64 after:{}", backopenurl);
					
					//支付方式先默认是微信
					String payways = "[2]";
					if(payId.equals("1")){
						payways="[1]";
					}else if(payId.equals("2")){
						payways="[2]";
					}
					
					//添加支付方式，金额，跳回地址
					otcAddress += "&matchone=" + orderMoney + "&matchtype=ccn&backopenurl=" + backopenurl +"&orderid=" + serialNumber  +"&payways=" + payways;
					log.info("实际请求otc支付地址:{}", otcAddress);
					response.sendRedirect(otcAddress);
				} else {
					out.print("请求otc支付地址失败，返回type:" + payAddress.getType());
					return;
				}
			} else {
				out.print("发起支付失败,解析otc支付地址出错");
				return;
			}
		} else {
			out.print("请求token失败，返回type:" + payToken.getType());
			return;
		}
	}else{
		out.print("发起支付失败,解析token出错");
		return;
	}
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>

<body onload="">
<form method="post" name="pay" id="pay">
<table>
<tr>
	<td>
	</td>
</tr>
</table>
	
</form>	

</body>
</html>
