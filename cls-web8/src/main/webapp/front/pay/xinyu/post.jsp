<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.pay.model.XinYuPay"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
 <%@page errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URL"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="com.team.lottery.extvo.returnModel.XinYuReturnParameter"%>
<%@page import="com.team.lottery.extvo.returnModel.XinYuData"%>
<%@page import="java.net.InetAddress"%>
<%@page import="com.team.lottery.util.JaxbUtil"%>
<%@page import="com.team.lottery.extvo.returnModel.XinYuTokenData"%>
<%@page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
    String orderMoneyYuan = OrderMoney;
 
    
    User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
	
	Date currTime = new Date();
    //时间以yyyy-MM-dd HH:mm:ss的方式表示
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.CHINA);
    //时间以yyyyMMDDHHmmss的方式表示 
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss",Locale.CHINA);
    String webdate=new String(formatter1.format(currTime));
    String TradeDate=new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));
	String  orderId=MakeOrderNum.makeOrderNum("CZ");
	log.info("用户名称: "+currentUser.getUserName()+" 用户 ID号: "+currentUser.getId()+" 生成订单号: "+orderId+" 订单金额: "+OrderMoney);
	
	
	if(!"".equals(OrderMoney)) {	
		 BigDecimal a;
		a = new BigDecimal(OrderMoney); //使用分进行提交
		OrderMoney=String.valueOf(a.multiply(new BigDecimal(100)).intValue());
	}
	else{
		OrderMoney = ("0");
	} 
	String payUrl = chargePay.getPayUrl();//转发支付网关地址
	String Address = chargePay.getAddress(); //最终银宝地址
	log.info("payUrl: "+payUrl+" Address: "+Address);
	
	//信誉支付请求对象
	XinYuPay xinYuPay =new XinYuPay();
	xinYuPay.setVersion("1.0");
	xinYuPay.setCharset("UTF-8");
	xinYuPay.setSign_type("MD5");
	xinYuPay.setMch_id(chargePay.getMemberId());
	xinYuPay.setOut_trade_no(orderId);
    xinYuPay.setDevice_info("123");
	xinYuPay.setBody("cz");
	xinYuPay.setAttach("ss");
	xinYuPay.setTotal_fee(OrderMoney);
	InetAddress address = InetAddress.getLocalHost();    
    String ip=address .getHostAddress().toString();    
	xinYuPay.setMch_create_ip(ip);
	xinYuPay.setNotify_url(chargePay.getReturnUrl());
	/* xinYuPay.setTime_start("");
	xinYuPay.setTime_expire("");
	xinYuPay.setOp_user_id("");
	xinYuPay.setGoods_tag("");
	xinYuPay.setProduct_id(""); */
	xinYuPay.setNonce_str("AAAAAA");
	//xinYuPay.setLimit_credit_pay("");
	//生成md5密钥签名
    String Md5key = chargePay.getSign();///////////md5密钥（KEY）
	String MARK = "&";
	String md5 =new String("attach="+xinYuPay.getAttach()+MARK+"body="+xinYuPay.getBody()+MARK+"charset="+xinYuPay.getCharset()+MARK+"device_info="+xinYuPay.getDevice_info()+MARK+"mch_create_ip="+xinYuPay.getMch_create_ip()+MARK+
	"mch_id="+xinYuPay.getMch_id()+MARK+"nonce_str="+xinYuPay.getNonce_str()+MARK+"notify_url="+xinYuPay.getNotify_url()+MARK+
	"out_trade_no="+xinYuPay.getOut_trade_no()+MARK+"sign_type="+xinYuPay.getSign_type()+MARK+"total_fee="+xinYuPay.getTotal_fee()+MARK+
	"version="+xinYuPay.getVersion()+MARK+"key="+chargePay.getSign());//MD5签名格式
	String Signature = md5Util.getMD5ofStr(md5).toUpperCase();//计算MD5值
    //交易签名
    xinYuPay.setSign(Signature);
    log.info("md5原始串: "+md5 + "生成后的交易签名:" + Signature);
    System.out.println(md5);
    System.out.println(Signature);
    //冲值订单
	RechargeOrder rechargeOrder = new RechargeOrder();
	//用户ID
	rechargeOrder.setUserId(currentUser.getId());
	//快捷ID
	rechargeOrder.setChargePayId(chargePay.getId());
	//用户名称
	rechargeOrder.setUserName(currentUser.getUserName());
	//申请金额
	rechargeOrder.setApplyValue(new BigDecimal(orderMoneyYuan));
	//订单创建时间
	rechargeOrder.setCreatedDate(new Date());
	//充值类型
	rechargeOrder.setPayType(payType);
	//第三方充值类型
    rechargeOrder.setThirdPayType("XINYU");
	//操作描述
	rechargeOrder.setOperateDes(EFundPayType.valueOf(payType).getDescription());
	//流水号
	rechargeOrder.setSerialNumber(orderId);
	rechargeOrder.setPostscript("");
	rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT.name());
	rechargeOrder.setBankName("");
	rechargeOrder.setSubbranchName("");
	rechargeOrder.setBankAccountName("");
	rechargeOrder.setBankCardNum("");
	rechargeOrder.setBankType("");
	rechargeOrder.setRefType(EFundRefType.THIRDPAY.name());
	rechargeOrder.setBizSystem(currentUser.getBizSystem());
	rechargeOrder.setFromType(currentUser.getLoginType());
	rechargeWithDrawOrderService.insertSelective(rechargeOrder);
	log.info("实际信誉支付网关地址["+Address +"],交易报文: "+xinYuPay);
	boolean flag=true;
	String codeUrl="";
	int i=0;
	while (i<5) {
		i++;
		//当循环次数大于5次强制退出循环
		if(i==4){
			flag=false;
			
		}
		//获取token值;首先取数据库的数据,如果为空就向远程发起请求获取新的token值,如果为非空则取数据库的数据
		String tokenStr = "";
		if (chargePay.getIsTest() == null) {
			String token = "http://api.xinyuzhifu.com/auth/access-token";
			token = token + "?appid=" + chargePay.getMemberId()
					+ "&secretid=" + chargePay.getSign();
			URL urlToken = new URL(token);
			HttpURLConnection httpConnToken = (HttpURLConnection) urlToken
					.openConnection();
			httpConnToken.setDoOutput(true);
			httpConnToken.setDoInput(true);
			httpConnToken.connect();

			// 取得输入流，并使用Reader读取
			BufferedReader readerToken = new BufferedReader(
					new InputStreamReader(
							httpConnToken.getInputStream(), "utf-8"));

			String linesToken = "";
			String strToken = "";
			while ((strToken = readerToken.readLine()) != null) {
				linesToken += strToken;
			}
			readerToken.close();
			XinYuTokenData returnTokenParameter = JaxbUtil
					.transformToObject(XinYuTokenData.class, linesToken);
			tokenStr = returnTokenParameter.getToken();
			chargePay.setIsTest(tokenStr);
			chargePayService.updateByPrimaryKey(chargePay);
		} else {
			tokenStr = chargePay.getIsTest();
		}

		/*  String strPara= JaxbUtil.transformToXml(XinYuPay.class, xinYuPay); */
        //传输数据的map
		Map<String, String> params = new HashMap<String, String>();

		params.put("version", xinYuPay.getVersion());
		params.put("charset", xinYuPay.getCharset());
		params.put("sign_type", xinYuPay.getSign_type());
		params.put("mch_id", xinYuPay.getMch_id());
		params.put("out_trade_no", xinYuPay.getOut_trade_no());
		params.put("device_info", xinYuPay.getDevice_info());
		params.put("body", xinYuPay.getBody());
		params.put("attach", xinYuPay.getAttach());
		params.put("total_fee", xinYuPay.getTotal_fee());
		params.put("attach", xinYuPay.getAttach());
		params.put("mch_create_ip", xinYuPay.getMch_create_ip());
		params.put("notify_url", xinYuPay.getNotify_url());
		params.put("nonce_str", xinYuPay.getNonce_str());
		params.put("sign", xinYuPay.getSign());
		
		//根据不同的支付类型获取相应的支付url地址
		String url = "";
		if (payType.equals("WEIXIN")) {
			url = "http://api.xinyuzhifu.com/sig/v1/wx/native?token="
					+ tokenStr;
		} else if (payType.equals("ALIPAY")) {
			url = "http://api.xinyuzhifu.com/sig/v1/alipay/native?token="
					+ tokenStr;
		} else if (payType.equals("QQPAY")) {
			url = "http://api.xinyuzhifu.com/sig/v1/qq/native?token="
					+ tokenStr;
		} else if (payType.equals("UNIONPAY")) {
			url = "http://api.xinyuzhifu.com/sig/v1/union/native?token="
					+ tokenStr;
		} else if (payType.equals("JDPAY")) {
			url = "http://api.xinyuzhifu.com/sig/v1/jd/native?token="
					+ tokenStr;
		} else if (payType.equals("QQPAYWAP")) {
			url = "http://api.xinyuzhifu.com/sig/v1/qq/wap?token="
					+ tokenStr;
		} else if (payType.equals("ALIPAYWAP")) {
			url = "http://api.xinyuzhifu.com/sig/v1/alipay/wap?token="
					+ tokenStr;
		} else if (payType.equals("WEIXINWAP")) {
			url = "http://api.xinyuzhifu.com/sig/v1/wx/wappay?token="
					+ tokenStr;
		} else {
			url = "http://api.xinyuzhifu.com/sig/v1/qq/native?token="
					+ tokenStr;
		}
		
        //向相应的支付地址发起请求并且获取相应的返回参数
		String lines = HttpClientUtil.doPost(url, params);
		log.info("信誉支付返回报文:" + lines);
		/* String codeUrl="";*/
		XinYuData returnParameter = JaxbUtil.transformToObject(
				XinYuData.class, lines);
		if (returnParameter.getStatus().equals("0")) {
			if (returnParameter.getResult_code().equals("0")) {
				if (payType.equals("QQPAYWAP")
						|| payType.equals("ALIPAYWAP")
						|| payType.equals("WEIXINWAP")) {
					response.sendRedirect(returnParameter.getPay_info());
					flag = false;
					break;
				}
				codeUrl = returnParameter.getCode_url();
				break;
			} else {
				out.print(returnParameter.getMessage());
				flag = false;
				break;
			}
		} else {
			if (returnParameter.getResult_code() == null
					&& returnParameter.getStatus().equals("0")) {
				continue;
			}else if(returnParameter.getResult_code() == null && returnParameter.getStatus().equals("用户未登录")){
				chargePay.setIsTest(null);
				chargePayService.updateByPrimaryKey(chargePay);
				continue;
			}
			out.print(returnParameter.getMessage());
			flag = false;
			break;
		}
	}
	
	if (!flag){
		return;
	}
		
	log.info("接收信誉支付转发成功,商户号[" + xinYuPay.getMch_id() + "],充值金额["
			+ xinYuPay.getTotal_fee() + "],充值类型[" + payType
			+ "],提交信誉支付处理");

	request.setAttribute("payType", payType);
%>
 
 <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta id="viewport" name="viewport"
	content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
<title>充值接口-提交信息处理</title>
<style>
body {
	text-align: center;
}

#weixinImg {
	margin-top: 150px;
	width: 280px;
}

p {
	font-size: 18px;
}
</style>
</head>
<body onload="pay.submit()">
<form method="post" name="pay" id="pay" action="<%=payUrl%>">
<TABLE>
<TR>
	<TD>
	<input name='payType' type='hidden' value= "<%=payType%>"/>
	<input name='codeUrl' type='hidden' value= "<%=codeUrl%>"/>
	
	
	</TD>
</TR>
</TABLE>
	
</form>	

</body>
<%-- <body>
	<img id="weixinImg" src='<%=codeUrl%>'></img>
	<img src="http://qr.topscan.com/api.php?bg=f3f3f3&fg=ff0000&gc=222222&el=l&w=684&m=30&text=<%=code%>"/>
	<p>
		请使用<span id="pay-way"><c:if test="${payType == 'ALIPAY' }">支付宝</c:if>
			<c:if test="${payType == 'WEIXIN' }">微信</c:if>
			<c:if test="${payType == 'QQPAY' }">QQ钱包</c:if>
			<c:if test="${payType == 'JDPAY' }">京东扫码</c:if>
			<c:if test="${payType == 'UNIONPAY' }">银联扫码</c:if></span>扫描二维码以完成支付
	</p>

</body> --%>
</html>

