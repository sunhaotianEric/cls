<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.pay.model.DuoBaoPay"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
<%  

    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
    String orderMoneyYuan = OrderMoney;
    String serialNumber = request.getParameter("serialNumber");
    
    User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
	
	Date currTime = new Date();
    //时间以yyyy-MM-dd HH:mm:ss的方式表示
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.CHINA);
    //时间以yyyyMMDDHHmmss的方式表示 
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss",Locale.CHINA);
    String webdate=new String(formatter1.format(currTime));
    String TradeDate=new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	ChargePay chargePay = chargePayService.getPayMemmberMsg(Long.valueOf(chargePayId));
	log.info("用户名称: "+currentUser.getUserName()+" 用户 ID号: "+currentUser.getId()+" 订单金额: "+OrderMoney);
	
	
	 if (!"".equals(OrderMoney)) 
	{	BigDecimal a;
		a = new BigDecimal(OrderMoney); //使用分进行提交
		OrderMoney=String.valueOf(a.setScale(2));
	}
	else{
		OrderMoney = ("0");
	}
	 
	 
	String payUrl = chargePay.getPayUrl();//转发支付网关地址
	String Address = chargePay.getAddress(); //最终银宝地址
	log.info("payUrl: "+payUrl+" Address: "+Address);
	
	//银宝请求对象
	DuoBaoPay duoBaoPay =new DuoBaoPay();
    //商户ID
    duoBaoPay.setParter(chargePay.getMemberId());
    //银行类型
    duoBaoPay.setType(payId);
    //商户订单号
    duoBaoPay.setOrderid(serialNumber);
    //金额
    duoBaoPay.setValue(OrderMoney);
    //下行异步通知地址
    //chargePay.getReturnUrl()
    duoBaoPay.setCallbackurl(chargePay.getReturnUrl());
    //下行同步通知地址
    //chargePay.getNotifyUrl()
    duoBaoPay.setHrefbackurl(chargePay.getNotifyUrl());
    //备注信息
    duoBaoPay.setAttach("");
	//生成md5密钥签名
    String Md5key = chargePay.getSign();///////////md5密钥（KEY）
	String MARK = "&";
	/* partner={}&banktype={}&paymoney={}&ordernumber={}&callbackurl={}key */
	String md5 =new String("parter="+duoBaoPay.getParter()+MARK+"type="+duoBaoPay.getType()+MARK+"value="+duoBaoPay.getValue()
	+MARK+"orderid="+duoBaoPay.getOrderid()+MARK+"callbackurl="+duoBaoPay.getCallbackurl()+chargePay.getSign());//MD5签名格式
	String Signature = md5Util.getMD5ofStr(md5);//计算MD5值
    //交易签名
    duoBaoPay.setSign(Signature);
    log.info("md5原始串: "+md5 + "生成后的交易签名:" + Signature);
    //冲值订单
	log.info("提交中转支付网关地址["+payUrl+"],实际多宝支付网关地址["+Address +"],交易报文: "+duoBaoPay);
 %>
 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>

<body onload="pay.submit()">
<form method="get" name="pay" id="pay" action="<%=payUrl%>">
<TABLE>
<TR>
	<TD>
	<input name='Address' type='hidden' value= "<%=Address%>"/>
	<input name='parter' type='hidden' value= "<%=duoBaoPay.parter%>"/>
	<input name='type' type='hidden' value= "<%=duoBaoPay.type%>"/>
	<input name='value' type='hidden' value= "<%=duoBaoPay.value%>"/>
	<input name='orderid' type='hidden' value= "<%=duoBaoPay.orderid%>"/>
	<input name='callbackurl' type='hidden' value= "<%=duoBaoPay.callbackurl%>"/>		
	<input name='hrefbackurl' type='hidden' value= "<%=duoBaoPay.hrefbackurl%>" />
	<input name='attach' type='hidden' value= "<%=duoBaoPay.attach%>" />
	<input name='sign' type='hidden' value= "<%=duoBaoPay.sign%>"/>
	
	</TD>
</TR>
</TABLE>
	
</form>	

</body>
</html>
