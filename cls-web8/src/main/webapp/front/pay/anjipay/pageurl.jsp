<%@page import="java.math.BigDecimal"%>
<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>  
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>充值结果</title>
<script type="text/javascript" src="<%=path%>/resources/jquery/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%=path%>/front/static/login/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<style type="text/css">
.wrap-box{
	margin:125px auto 0;
	width:320px;
	background:#fff;
	border:5px solid #000;
}
.bd-tisp{
	padding:15px 0;
	text-align:center;
}
.bd-tisp h2{
	font-size:18px;
	margin-bottom:5px;
}
</style>
</head>

<body class="bg-content">
<div class="wrap-box">
    <div class="bd-tisp">
        <h2 class="green">恭喜支付成功! </h2>
        <span id='closePrompt'></span>
    </div>
</div>
</body>
<script language="javascript">
//设置超时时间为10秒钟
var timeout = 3;
function show() {
    var showbox = $("#closePrompt");
    showbox.html("窗口在" + timeout + "秒后关闭");
    timeout--;
    if (timeout == 0) {
    	closeWindows();
    }
    else {
        setTimeout("show()", 1000);
    }
}

show();
</script>
<script type="text/javascript">
function closeWindows() {
    var browserName = navigator.appName;
    var browserVer = parseInt(navigator.appVersion);

    if(browserName == "Microsoft Internet Explorer"){
        var ie7 = (document.all && !window.opera && window.XMLHttpRequest) ? true : false;  
        if (ie7){  
          window.open('','_parent','');
          window.close();
        }
       else{
          this.focus();
          self.opener = this;
          self.close();
        }
   }else{  
       //For NON-IE Browsers except Firefox which doesnt support Auto Close
       try{
           this.focus();
           self.opener = this;
           self.close();
       }
       catch(e){

       }

       try{
           window.open('','_self','');
           window.close();
       }
       catch(e){

       }
   }
}
</script>
</html>
