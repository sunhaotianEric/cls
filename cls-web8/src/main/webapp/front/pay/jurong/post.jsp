<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.pay.model.JuRongPay"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
 <%@page errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.InetAddress"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="com.team.lottery.extvo.returnModel.JuRongReturnPayUrl"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%  

    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
    String orderMoneyYuan = OrderMoney;
 
    
    User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
	
	/* Date currTime = new Date();
    //时间以yyyy-MM-dd HH:mm:ss的方式表示
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.CHINA);
    //时间以yyyyMMDDHHmmss的方式表示 
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss",Locale.CHINA);
    String webdate=new String(formatter1.format(currTime));
    String TradeDate=new String(formatter2.format(currTime)); */

	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
    RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));
	String  orderId=MakeOrderNum.makeOrderNum("CZ");
	log.info("用户名称: "+currentUser.getUserName()+" 用户 ID号: "+currentUser.getId()+" 生成订单号: "+orderId+" 订单金额: "+OrderMoney);
	
	if(!"".equals(OrderMoney)) {	
		 BigDecimal a;
		a = new BigDecimal(OrderMoney); //使用分进行提交
		OrderMoney=String.valueOf(a.multiply(new BigDecimal(100)).intValue());
	}
	else{
		OrderMoney = ("0");
	} 
	String payUrl = chargePay.getPayUrl();//转发支付网关地址
	String Address = chargePay.getAddress(); //最终聚融地址
	log.info("payUrl: "+payUrl+" Address: "+Address);
	
	//聚融支付请求对象
	JuRongPay juRongPay =new JuRongPay();
	juRongPay.setMch_no(chargePay.getMemberId());
	juRongPay.setPay_type(payId);
	Date d=new Date(); 
    Long epoch=d.getTime()/1000; 
	juRongPay.setTime(String.valueOf(epoch));
	juRongPay.setOrder_sn(orderId);
	juRongPay.setReturn_url(path+chargePay.getNotifyUrl());
	juRongPay.setNotify_url(chargePay.getReturnUrl());
	/* juRongPay.setCard_type("")
	juRongPay.setYl_pay_type("dd") */
	//juRongPay.setBank_name("dd");
	juRongPay.setExtra("czpay");
	juRongPay.setAmount(OrderMoney);
	InetAddress address = InetAddress.getLocalHost();    
    String ip=address .getHostAddress().toString();   
	juRongPay.setClient_ip(ip);
	//生成md5密钥签名
    String Md5key = chargePay.getSign();///////////md5密钥（KEY）
	String md5 =new String(juRongPay.getOrder_sn()+juRongPay.getAmount()+juRongPay.getPay_type()
			+juRongPay.getTime()+juRongPay.getMch_no()+md5Util.getMD5ofStr(Md5key));//MD5签名格式
	String Signature = md5Util.getMD5ofStr(md5);//计算MD5值
    //交易签名
    juRongPay.setSign(Signature);
    log.info("md5原始串: "+md5 + "生成后的交易签名:" + Signature);
    //冲值订单
	RechargeOrder rechargeOrder = new RechargeOrder();
	//用户ID
	rechargeOrder.setUserId(currentUser.getId());
	//快捷ID
	rechargeOrder.setChargePayId(chargePay.getId());
	//用户名称
	rechargeOrder.setUserName(currentUser.getUserName());
	//申请金额
	rechargeOrder.setApplyValue(new BigDecimal(orderMoneyYuan));
	//订单创建时间
	rechargeOrder.setCreatedDate(new Date());
	//充值类型
	rechargeOrder.setPayType(payType);
	//第三方充值类型
    rechargeOrder.setThirdPayType("JURONG");
	//操作描述
	rechargeOrder.setOperateDes(EFundPayType.valueOf(payType).getDescription());
	//流水号
	rechargeOrder.setSerialNumber(orderId);
	rechargeOrder.setPostscript("");
	rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT.name());
	rechargeOrder.setBankName("");
	rechargeOrder.setSubbranchName("");
	rechargeOrder.setBankAccountName("");
	rechargeOrder.setBankCardNum("");
	rechargeOrder.setBankType("");
	rechargeOrder.setRefType(EFundRefType.THIRDPAY.name());
	rechargeOrder.setBizSystem(currentUser.getBizSystem());
	rechargeOrder.setFromType(currentUser.getLoginType());
	rechargeWithDrawOrderService.insertSelective(rechargeOrder);
	log.info("实际聚融支付网关地址["+Address +"],交易报文: "+juRongPay);
	
	String strPara="mch_no="+URLEncoder.encode(juRongPay.getMch_no(), "utf-8") + "&pay_type="
			+ URLEncoder.encode(juRongPay.getPay_type(), "utf-8") + "&amount="
			+ URLEncoder.encode(juRongPay.getAmount(), "utf-8") + "&time="
			+ URLEncoder.encode(juRongPay.getTime(), "utf-8") + "&order_sn="
			+ URLEncoder.encode(juRongPay.getOrder_sn(), "utf-8") + "&return_url="
			+ URLEncoder.encode(juRongPay.getReturn_url(), "utf-8") + "&notify_url="
			+ URLEncoder.encode(juRongPay.getNotify_url(), "utf-8") + "&sign="
			+ URLEncoder.encode(juRongPay.getSign(), "utf-8") + "&extra="
			+ URLEncoder.encode(juRongPay.getExtra(), "utf-8")+"&client_ip="
			+ URLEncoder.encode(juRongPay.getClient_ip(), "utf-8");
	URL url = new URL(Address+"?"+strPara);
    
	//openConnection函数会根据URL的协议返回不同的URLConnection子类的对象
	//这里URL是一个http,因此实际返回的是HttpURLConnection 
	HttpURLConnection httpConn = (HttpURLConnection) url
			.openConnection();

	//进行连接,实际上request要在下一句的connection.getInputStream()函数中才会真正发到 服务器****待验证
	httpConn.setDoOutput(true);
	httpConn.setDoInput(true);
	httpConn.connect();
	/* PrintWriter outWrite = new PrintWriter(httpConn.getOutputStream());
	
	outWrite.print(strPara);
    // flush输出流的缓冲
    outWrite.flush(); */
	// 取得输入流，并使用Reader读取
	BufferedReader reader = new BufferedReader(new InputStreamReader(
			httpConn.getInputStream(),"utf-8"));

	String lines = "";
	String str="";
	while ((str=reader.readLine()) != null) {
		lines += str; 
	}
	reader.close();
	log.info("聚融支付返回报文:" + lines);
	String codeUrl="";
	JuRongReturnPayUrl returnParameter = JSONUtils.toBean(lines,
			JuRongReturnPayUrl.class);
	if (returnParameter.getOk().equals("true")) {
		/* if(returnParameter.getImg()==null){
			response.sendRedirect(returnParameter.getData());
		}else{
			 codeUrl = returnParameter.getImg();
		} */
		/* response.sendRedirect(returnParameter.getBarCode());
		return; */
		codeUrl=returnParameter.getData().getImg();
	} else {
		out.print(returnParameter.getMsg());
		return;
	}
	log.info("接收聚融支付转发成功,商户号[" + juRongPay.getMch_no() + "],充值金额[" + juRongPay.getAmount()
			+ "],银行码表[" + juRongPay.getPay_type() + "],提交聚融支付处理");

	request.setAttribute("payType", payType);
 %>
 
 <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta id="viewport" name="viewport"
	content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
<title>充值接口-提交信息处理</title>
<style>
body {
	text-align: center;
}

#weixinImg {
	margin-top: 150px;
	width: 280px;
}

p {
	font-size: 18px;
}
</style>
</head>

<body>
	<img id="weixinImg" src='<%=codeUrl%>'></img>
	<%-- <img src="http://qr.topscan.com/api.php?bg=f3f3f3&fg=ff0000&gc=222222&el=l&w=684&m=30&text=<%=code%>"/> --%>
	<p>
		请使用<span id="pay-way"><c:if test="${payType == 'ALIPAY' }">支付宝</c:if>
			<c:if test="${payType == 'WEIXIN' }">微信</c:if>
			<c:if test="${payType == 'QQPAY' }">QQ钱包</c:if>
			<c:if test="${payType == 'JDPAY' }">京东扫码</c:if>
			<c:if test="${payType == 'UNIONPAY' }">银联扫码</c:if></span>扫描二维码以完成支付
	</p>

</body>
</html>

<%-- <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>

<body onload="pay.submit()">
<form method="post" name="pay" id="pay" action="<%=payUrl%>">
<TABLE>
<TR>
	<TD>
	<input name='Address' type='hidden' value= "<%=Address%>"/>
	<input name='p1_mchtid' type='hidden' value= "<%=jinYangPay.p1_mchtid%>"/>
	<input name='p2_paytype' type='hidden' value= "<%=jinYangPay.p2_paytype%>"/>
	<input name='p3_paymoney' type='hidden' value= "<%=jinYangPay.p3_paymoney%>"/>
	<input name='p4_orderno' type='hidden' value= "<%=jinYangPay.p4_orderno%>"/>
	<input name='p5_callbackurl' type='hidden' value= "<%=jinYangPay.p5_callbackurl%>"/>		
	<input name='p6_notifyurl' type='hidden' value= "<%=jinYangPay.p6_notifyurl%>" />
	<input name='p7_version' type='hidden' value= "<%=jinYangPay.p7_version%>" />
	<input name='p8_signtype' type='hidden' value= "<%=jinYangPay.p8_signtype%>" />
	<input name='p9_attach' type='hidden' value= "<%=jinYangPay.p9_attach%>" />
	<input name='p10_appname' type='hidden' value= "<%=jinYangPay.p10_appname%>" />
	<input name='p11_isshow' type='hidden' value= "<%=jinYangPay.p11_isshow%>" />
	<input name='p12_orderip' type='hidden' value= "<%=jinYangPay.p12_orderip%>" />
	<input name='sign' type='hidden' value= "<%=jinYangPay.sign%>"/>
	
	</TD>
</TR>
</TABLE>
	
</form>	

</body>
</html> --%>
