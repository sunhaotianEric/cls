<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
	//request.setCharacterEncoding("GB2312");
    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
    String orderid  = request.getParameter("orderid");//商户订单号 
    String result = request.getParameter("result");//订单结果 
	String amount = request.getParameter("amount");//订单金额 
	String zborderid = request.getParameter("zborderid");//订单号
	String completetime = request.getParameter("completetime");//订单时间 
	String notifytime = request.getParameter("notifytime");//通知时间戳
	String sign  = request.getParameter("sign");//MD5签名
	String attach  = request.getParameter("attach");//备注信息 
	String msg = request.getParameter("msg");//订单结果说明
	
	log.info("众宝支付成功通知平台处理信息如下:商户订单号: "+orderid+" 订单结果: "+result+" 订单金额: "+amount+" 众宝商务订单号: "+zborderid+"MD5签名: "+sign+" 订单结果说明："+msg);
    
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(orderid);
	if(rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1){
		log.error("根据订单号[" + orderid + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知赢通宝报文接收成功
	    out.println("opstate=0");
		return;
	}
	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
	if(rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + orderid + "]查找充值订单记录为空");
		//通知赢通宝报文接收成功
	    out.println("opstate=0");
		return;
	}
	
	//签名
	String Md5key = rechargeWithDrawOrder.getSign();
	String MARK = "&";
	String md5 ="";
	log.info("orderid="+orderid+"&result="+result+"&amount="+amount+"&zborderid="+zborderid+"&completetime="+completetime+"&key="+Md5key);

	md5 = "orderid="+orderid+"&result="+result+"&amount="+amount+"&zborderid="+zborderid+"&completetime="+completetime+"&key="+Md5key;
	
	//Md5加密串
	String WaitSign = md5Util.getMD5ofStr(md5, "GB2312").toLowerCase();
	log.info("WaitSign " + WaitSign);

	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if (WaitSign.compareTo(sign) == 0) {
		boolean dealResult = false;
		if (!StringUtils.isEmpty(result) && ("0").equals(result)) {
			BigDecimal factMoneyValue = new BigDecimal(amount);
			try {
				dealResult = rechargeWithDrawOrderService
						.rechargeOrderSuccessDeal(orderid,
								factMoneyValue);
			} catch (UnEqualVersionException e) {
				log.error("众宝支付充值时发现版本号不一致,订单号[" + orderid + "]");
				//启动新进程进行多次重试
				UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
						UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION,
						orderid, factMoneyValue, "");
				thread.start();
			} catch (Exception e) {
				//异常不进行重试
				log.error(e.getMessage());
			}
		} else {
			if (!("0").equals(result)) {
				log.info("众宝支付处理错误支付结果：" + result +" 订单结果说明:"+msg);
			}
		}
		log.info("众宝订单处理入账结果:{}", dealResult);
		//通知赢通宝报文接收成功
		out.println("opstate=0");
		return;
	} else {
		log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容["
				+ WaitSign + "]");
		//让第三方继续补发
		out.println("opstate=1");
		return;
	}
%>