<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.RSA"%>
<%@page import="com.team.lottery.pay.model.JianZhengBaoPay"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
 <%@page errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.InetAddress"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="com.team.lottery.extvo.returnModel.BeiFuBaoParameter"%>
<%@page import="com.team.lottery.extvo.returnModel.JianZhengBaoData"%>
<%@page import="com.team.lottery.pay.model.BeiFuBaoPay"%>
<%@page import="java.util.*"%>
<%@page import="net.sf.json.JSONObject"%>
<%@ page import="com.team.lottery.pay.util.HttpClients"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%  

    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
    String orderMoneyYuan = OrderMoney;
 
    
    User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
	
	

	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));
	String  orderId=MakeOrderNum.makeOrderNum("CZ");
	log.info("用户名称: "+currentUser.getUserName()+" 用户 ID号: "+currentUser.getId()+" 生成订单号: "+orderId+" 订单金额: "+OrderMoney);
	
	
	if(!"".equals(OrderMoney)) {	
		 BigDecimal a;
		a = new BigDecimal(OrderMoney); //使用分进行提交
		OrderMoney=String.valueOf(a.multiply(new BigDecimal(100)).intValue());
	}
	else{
		OrderMoney = ("0");
	} 
	String payUrl = chargePay.getPayUrl();//转发北付宝网关地址
	String Address = chargePay.getAddress(); //最终北付宝地址
	log.info("payUrl: "+payUrl+" Address: "+Address);
	
	
    //时间以yyyy-MM-dd HH:mm:ss的方式表示

    //交易签名
    /* jianZhengBaoPay.setSign(Signature); */
    //log.info("md5原始串: "+md5 + "生成后的交易签名:" + Signature); */
    //冲值订单
	RechargeOrder rechargeOrder = new RechargeOrder();
	//用户ID
	rechargeOrder.setUserId(currentUser.getId());
	//快捷ID
	rechargeOrder.setChargePayId(chargePay.getId());
	//用户名称
	rechargeOrder.setUserName(currentUser.getUserName());
	//申请金额
	rechargeOrder.setApplyValue(new BigDecimal(orderMoneyYuan));
	//订单创建时间
	rechargeOrder.setCreatedDate(new Date());
	//充值类型
	rechargeOrder.setPayType(payType);
	//第三方充值类型
    rechargeOrder.setThirdPayType("BEIFUBAOPAY");
	//操作描述
	rechargeOrder.setOperateDes(EFundPayType.valueOf(payType).getDescription());
	//流水号
	rechargeOrder.setSerialNumber(orderId);
	rechargeOrder.setPostscript("");
	rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT.name());
	rechargeOrder.setBankName("");
	rechargeOrder.setSubbranchName("");
	rechargeOrder.setBankAccountName("");
	rechargeOrder.setBankCardNum("");
	rechargeOrder.setBankType("");
	rechargeOrder.setRefType(EFundRefType.THIRDPAY.name());
	rechargeOrder.setBizSystem(currentUser.getBizSystem());
	rechargeOrder.setFromType(currentUser.getLoginType());
	rechargeWithDrawOrderService.insertSelective(rechargeOrder);
	//log.info("实际北付宝支付网关地址["+Address +"],交易报文: "+jianZhengBaoPay);
	//北付宝支付请求对象
	BeiFuBaoPay beiFuBaoPay =new BeiFuBaoPay();
	beiFuBaoPay.setMemberNumber(chargePay.getMemberId());
	Date currTime = new Date();
	Map<String, String> mapHead = new HashMap<String, String>();
	String url = Address;
    mapHead.put("memberNumber", beiFuBaoPay.getMemberNumber());
    mapHead.put("charset", "utf-8");
    mapHead.put("method", "UNIFIED_PAYMENT");
    mapHead.put("sign", "");
    mapHead.put("signType", "RSA");
    mapHead.put("version",chargePay.getInterfaceVersion());
    mapHead.put("requestTime", currTime.toGMTString());
    log.info(mapHead.toString());

    Map<String, String> mapContent = new HashMap<String, String>();
    mapContent.put("defrayalType", payId);
    mapContent.put("memberOrderNumber",orderId);
    mapContent.put("tradeCheckCycle", "T1");
    mapContent.put("orderTime", currTime.toGMTString());
    mapContent.put("currenciesType", "CNY");
    mapContent.put("tradeAmount", OrderMoney);
    mapContent.put("commodityBody", "cz");
    mapContent.put("commodityDetail", "T1");
    mapContent.put("commodityRemark", "rmb");
    mapContent.put("notifyUrl",chargePay.getReturnUrl());
    mapContent.put("returnUrl", chargePay.getNotifyUrl());
    mapContent.put("terminalIP", "127.0.0.0");
    mapContent.put("terminalId", "10001");
    mapContent.put("userId", beiFuBaoPay.getMemberNumber());
    mapContent.put("attach", "zx");
    mapContent.put("remark", "cz");
    log.info(mapContent.toString());

    JSONObject businessContext = JSONObject.fromObject(mapContent);
    JSONObject businessHead = JSONObject.fromObject(mapHead);

    log.info("发送的报文businessContext:" + businessContext.toString());
    log.info("发送的报文businessHead   :" + businessHead.toString());
    String memberPriKeyJava=chargePay.getPrivatekey();
    String bfbPublicKey=chargePay.getPublickey();
    String context = RSA.verifyAndEncryptionToString(businessContext, businessHead, memberPriKeyJava, bfbPublicKey);
    JSONObject jsonObject = new JSONObject();
    jsonObject.put("context", context);
    log.info("发送的密文context        :" + jsonObject.toString());

    JSONObject returnStr = HttpClients.doPost(url, jsonObject);
    log.info("接收的密文               ：" + returnStr);
   
    String  str=RSA.decryptByPrivateKey(returnStr.getString("context"),memberPriKeyJava);
    BeiFuBaoParameter businessContextReturn=JSONUtils.toBean(JSONObject.fromObject(str).getString("businessContext"),BeiFuBaoParameter.class);
    log.info("接收的报文               ：" +businessContextReturn.toString());
    String codeUrl="";
    if(businessContextReturn.getOrderStatus().equals("WAIT")){
    	codeUrl=businessContextReturn.getContent();
    }
    request.setAttribute("payType", payType);
    //System.out.println("接收的报文               ：" + RSA.decryptByPrivateKey(returnStr.getString("context"), memberPriKeyJava));
	
 %>
 
 <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta id="viewport" name="viewport"
	content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
<title>充值接口-提交信息处理</title>
<style>
body {
	text-align: center;
}

img {
	margin-top: 60px;
}

p {
	font-size: 36px;
}
</style>
</head>

<body>
	<img id="weixinImg" src='<%=path%>/tools/getQrCodePic?code=<%=codeUrl%>'></img>
	<%-- <img src="http://qr.topscan.com/api.php?bg=f3f3f3&fg=ff0000&gc=222222&el=l&w=684&m=30&text=<%=code%>"/> --%>
	<p>
		请使用<span id="pay-way">
		    <c:if test="${payType == 'ALIPAY' }">支付宝</c:if>
			<c:if test="${payType == 'WEIXIN' }">微信</c:if>
			<%-- <c:if test="${payType == 'JDQB' }">京东钱包</c:if> --%>
			<c:if test="${payType == 'QQPAY' }">QQ钱包</c:if>
			<c:if test="${payType == 'UNIONPAY' }">银联二维码</c:if>
			</span>扫描二维码以完成支付
	</p>

</body>
</html>
 