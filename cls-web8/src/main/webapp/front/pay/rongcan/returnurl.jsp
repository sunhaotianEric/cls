<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@ page import="com.team.lottery.pay.util.Base64Utils"%>
<%@ page import="com.team.lottery.pay.util.KeyGenUtil"%>
<%@ page import="com.team.lottery.pay.util.RSAUtils"%>
<%@ page import="com.team.lottery.pay.util.SignUtils"%>
<%@page import="com.team.lottery.vo.ChargePay,com.team.lottery.service.ChargePayService"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%  
    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String merchantCode = request.getParameter("merchantCode");//商户号
	String orderNo = request.getParameter("orderNo");//商户终端号
	String amount = request.getParameter("amount");//商户流水号
	String successAmt = request.getParameter("successAmt");//支付结果
	String payOrderNo = request.getParameter("payOrderNo");//支付结果描述
	String orderStatus = request.getParameter("orderStatus");//实际成功金额
	String extraReturnParam = request.getParameter("extraReturnParam");//订单附加消息	
	String signType = request.getParameter("signType");//支付完成时间
	String sign = request.getParameter("sign");//MD5签名
  
	log.info("融灿支付成功通知平台处理信息如下: 商户号: "+merchantCode+" 订单号: "+orderNo+" 支付结果: "+orderStatus+" 实际成功金额: "+
			successAmt+" 第三方返回MD5签名: "+sign);
  
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(orderNo);
	if(rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1){
		log.error("根据订单号[" + orderNo + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知融灿接收报文成功
	    out.println("SUCCESS");
		return;
	}
	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
	if(rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + orderNo + "]查找充值订单记录为空");
		//通知融灿接收报文成功
	    out.println("SUCCESS");
		return;
	}
	
	//签名
	Long chargePayId = rechargeWithDrawOrder.getChargePayId();
	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	ChargePay chargePay = chargePayService.getPayMemmberMsg(Long.valueOf(chargePayId));
	String MARK = "&";
	String md5 = "merchantCode=" + merchantCode + MARK + "orderNo=" + orderNo + MARK + "amount=" + amount + MARK + "successAmt=" + successAmt + MARK + "payOrderNo=" + payOrderNo + MARK
			+ "orderStatus=" + orderStatus + MARK + "extraReturnParam="+extraReturnParam;
    String publicKey=chargePay.getPublickey();
	//Md5加密串
	/* String WaitSign = RSAUtils.publicKeyDecrypt(publicKey, sign); */
	boolean signStat = SignUtils.validataSign(md5, sign, publicKey);
	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if(signStat){
		boolean dealResult = false;
		if(!StringUtils.isEmpty(orderStatus) && orderStatus.equals("Success")){
			//将实际成功金额除以100
			BigDecimal factMoneyValue = new BigDecimal(successAmt).divide(new BigDecimal("100"));
			try {
				dealResult = rechargeWithDrawOrderService.rechargeOrderSuccessDeal(orderNo,factMoneyValue);
			}catch(UnEqualVersionException e) {
		   		log.error("融灿在线充值时发现版本号不一致,订单号["+orderNo+"]");
		   		//启动新进程进行多次重试
		   		UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION
		   				, orderNo,factMoneyValue,"");
		   		thread.start();
			} catch(Exception e){
				//异常不进行重试
				log.error(e.getMessage());
			}
		} else {
			if(!orderStatus.equals("Success")){
				log.info("融灿支付处理错误支付结果："+orderStatus);
			}
		}
		log.info("融灿订单处理入账结果:{}", dealResult);
		//通知融灿接收报文成功	
	    out.println("SUCCESS");
		return;
	}else{
		log.info("MD5校验失败,报文返回MD5签名内容["+ sign +"],计算MD5签名内容[" + md5 + "]");
		//让第三方继续补发
		out.println("ERROR");
		return;
	}
%>