<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@page import="com.team.lottery.service.QuickBankTypeService"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.pay.model.GaoTongPay"%>
<%@page import="com.team.lottery.vo.QuickBankType"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
<%  

    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
    String orderMoneyYuan = OrderMoney;
    
    User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
	
	Date currTime = new Date();
    //时间以yyyy-MM-dd HH:mm:ss的方式表示
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.CHINA);
    //时间以yyyyMMDDHHmmss的方式表示 
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss",Locale.CHINA);
    String webdate=new String(formatter1.format(currTime));
    String TradeDate=new String(formatter2.format(currTime));
    QuickBankTypeService quickBankTypeService = (QuickBankTypeService)ApplicationContextUtil.getBean("quickBankTypeService");
	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.getPayMemmberMsg(Long.valueOf(chargePayId));
	String  orderId = MakeOrderNum.makeOrderNum("CZ");
	log.info("用户名称: "+currentUser.getUserName()+" 用户 ID号: "+currentUser.getId()+" 生成订单号: "+orderId+" 订单金额: "+OrderMoney);
	QuickBankType query = new QuickBankType();
	query.setPayId(payId);
	String bankType = "";
	List<QuickBankType>  list = quickBankTypeService.getQuickBankTypeQuery(query);
	if(list.size() > 0){
		bankType = list.get(0).getBankType();
	}
	
	String payUrl = chargePay.getPayUrl();//借贷混合
	String Address = chargePay.getAddress(); //最终地址
	log.info("payUrl: "+payUrl+" Address: "+Address);
	//高通请求对象
	GaoTongPay gaoTongPay = new GaoTongPay();
	gaoTongPay.setPartner(chargePay.getMemberId());
	gaoTongPay.setBanktype(payId);
	gaoTongPay.setPaymoney(OrderMoney);
	gaoTongPay.setOrdernumber(orderId);
	gaoTongPay.setCallbackurl(chargePay.getReturnUrl());
	gaoTongPay.setHrefbackurl(chargePay.getNotifyUrl());
	gaoTongPay.setAttach("");


	String Md5key = chargePay.getSign();///////////md5密钥（KEY）
	String md5 =new String("partner="+gaoTongPay.getPartner()+"&banktype="+gaoTongPay.getBanktype()+"&paymoney="+gaoTongPay.getPaymoney()+"&ordernumber="+gaoTongPay.getOrdernumber()+"&callbackurl="+gaoTongPay.getCallbackurl()+Md5key);//MD5签名格式
	String Signature = md5Util.getMD5ofStr(md5, "GB2312").toLowerCase();//计算MD5值
	log.info("md5原始串: "+md5);
	log.info("交易签名: "+Signature);
	gaoTongPay.setSign(Signature);
	log.info("交易报文: "+gaoTongPay);

    //冲值订单
	RechargeOrder rechargeOrder = new RechargeOrder();
	//用户ID
	rechargeOrder.setUserId(currentUser.getId());
	//快捷ID
	rechargeOrder.setChargePayId(chargePay.getId());
	//用户名称
	rechargeOrder.setUserName(currentUser.getUserName());
	//申请金额
	rechargeOrder.setApplyValue(new BigDecimal(orderMoneyYuan));
	//订单创建时间
	rechargeOrder.setCreatedDate(new Date());
	//充值类型
	rechargeOrder.setPayType(payType);
	//第三方充值类型
	rechargeOrder.setThirdPayType("GTPAY");
	//操作描述
	rechargeOrder.setOperateDes(EFundPayType.valueOf(payType).getDescription());
	//流水号
	rechargeOrder.setSerialNumber(orderId);
	rechargeOrder.setPostscript("");
	rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT.name());
	rechargeOrder.setBankName("");
	rechargeOrder.setSubbranchName("");
	rechargeOrder.setBankAccountName("");
	rechargeOrder.setBankCardNum("");
	rechargeOrder.setBankType(bankType);
	rechargeOrder.setBizSystem(currentUser.getBizSystem());
	rechargeOrder.setRefType(EFundRefType.THIRDPAY.name());
	rechargeOrder.setFromType(currentUser.getLoginType());
	rechargeWithDrawOrderService.insertSelective(rechargeOrder);
 %>
 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>

<body onload="pay.submit()">
<form method="get" name="pay" id="pay" action="<%=payUrl%>">
<TABLE>
<TR>
	<TD>
	<input name='Address' type='hidden' value= "<%=Address%>"/>
	<input name='partner' type='hidden' value= "<%=gaoTongPay.getPartner()%>"/>
	<input name='banktype' type='hidden' value= "<%=gaoTongPay.getBanktype()%>"/>
	<input name='paymoney' type='hidden' value= "<%=gaoTongPay.getPaymoney()%>"/>
	<input name='ordernumber' type='hidden' value= "<%=gaoTongPay.getOrdernumber()%>"/>
	<input name='callbackurl' type='hidden' value= "<%=gaoTongPay.getCallbackurl()%>"/>		
	<input name='hrefbackurl' type='hidden' value= "<%=gaoTongPay.getHrefbackurl()%>"/>
	<input name='attach' type='hidden' value= "<%=gaoTongPay.getAttach()%>"/>
	<input name='sign' type='hidden' value= "<%=gaoTongPay.getSign()%>"/>			
	</TD>
</TR>
</TABLE>
	
</form>	

</body>
</html>
