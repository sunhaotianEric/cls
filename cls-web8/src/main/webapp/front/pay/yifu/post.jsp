<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.team.lottery.service.QuickBankTypeService"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.pay.model.yifu.YiFuPay"%>
<%@page import="com.team.lottery.vo.QuickBankType"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<jsp:useBean id='md5Util' scope='request'
	class='com.team.lottery.pay.util.Md5Util' />
<%
	//打印日志
	Logger log = LoggerFactory.getLogger(this.getClass());
	//获取当前web路径
	String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	String orderMoneyYuan = OrderMoney;
	//判断当前请求页面用户是否登录,没有的话就跳往登录页面
	User currentUser = (User) request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	if (currentUser == null) {
		response.sendRedirect(path + "/gameBet/login.html");
	}

	//获取订单提交时间
	Date currTime = new Date();
	QuickBankTypeService quickBankTypeService = (QuickBankTypeService) ApplicationContextUtil.getBean("quickBankTypeService");
	ChargePayService chargePayService = (ChargePayService) ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService) ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.getPayMemmberMsg(Long.valueOf(chargePayId));
	//生成订单Id
	String orderId = MakeOrderNum.makeOrderNum("CZ");
	//打印日志
	log.info("业务系统[{}],用户名[{}],发起了易付支付，生成订单号[{}],支付金额[{}],PayId[{}],payType[{}],chargePayId[{}]",
  			currentUser.getBizSystem(), currentUser.getUserName(), orderId, OrderMoney, payId, payType, chargePayId);
	QuickBankType query = new QuickBankType();
	query.setPayId(payId);
	String bankType = "";
	List<QuickBankType> list = quickBankTypeService.getQuickBankTypeQuery(query);
	if (list.size() > 0) {
		bankType = list.get(0).getBankType();
	}

	//生成需要跳转的支付地址;
	String payUrl = chargePay.getPayUrl();//转发支付网关地址
	String Address = chargePay.getAddress(); //最终地址

	log.info("易付支付报文转发地址payUrl: "+payUrl+",实际网关地址address: "+Address);
	//同步跳转地址
	String returnUrl = "";
	if (chargePay.getNotifyUrl().startsWith("http://") || chargePay.getNotifyUrl().startsWith("https://")) {
		returnUrl = chargePay.getNotifyUrl();
	} else {
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
		returnUrl = basePath + chargePay.getNotifyUrl();
	}
	
	
	//设置易付支付请求对象
	YiFuPay yiFuPay = new YiFuPay();
	//设置版本号,默认版本是1.0;
	yiFuPay.setVersion(chargePay.getInterfaceVersion());
	//设置商户编号
	yiFuPay.setCustomerid(chargePay.getMemberId());

	yiFuPay.setSdorderno(orderId);
	//设置支付类型;
	yiFuPay.setPaytype(payId);
	//设置支付金额
	yiFuPay.setTotal_fee(OrderMoney);
	//设置下行异步通知地址,返回地址
	yiFuPay.setNotifyurl(chargePay.getReturnUrl());
	//设置下行同步通知地址,返回地址
	yiFuPay.setReturnurl(returnUrl);
	//设置订单备注说明
	yiFuPay.setRemark(currentUser.getUserName());
	//获取商户秘钥
	String MD5Key = chargePay.getSign();
	String MARK = "&";
	//需要加密的字段;
	String yiFuPayMD5 = "version=" + yiFuPay.getVersion() + MARK + 
						"customerid=" + yiFuPay.getCustomerid() + MARK + 
						"total_fee=" + OrderMoney + MARK +
						"sdorderno=" + yiFuPay.getSdorderno() + MARK + 
						"notifyurl=" + yiFuPay.getNotifyurl() + MARK + 
						"returnurl=" + yiFuPay.getReturnurl() + MARK + 
						MD5Key;
	String Signature = Md5Util.getMD5ofStr(yiFuPayMD5);//获取MD5值
	log.info("md5原始串: " + yiFuPayMD5 + "生成后的交易签名:" + Signature);
	//设置交易签名
	yiFuPay.setSign(Signature);
	//冲值订单
	RechargeOrder rechargeOrder = new RechargeOrder();
	//用户ID
	rechargeOrder.setUserId(currentUser.getId());
	//快捷ID
	rechargeOrder.setChargePayId(chargePay.getId());
	//用户名称
	rechargeOrder.setUserName(currentUser.getUserName());
	//申请金额
	rechargeOrder.setApplyValue(new BigDecimal(orderMoneyYuan));
	//订单创建时间
	rechargeOrder.setCreatedDate(new Date());
	//充值类型
	rechargeOrder.setPayType(payType);
	//第三方充值类型
	rechargeOrder.setThirdPayType("BDPAY");
	//操作描述
	rechargeOrder.setOperateDes(EFundPayType.valueOf(payType).getDescription());
	//流水号
	rechargeOrder.setSerialNumber(orderId);
	rechargeOrder.setPostscript("");
	rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT.name());
	rechargeOrder.setBankName("");
	rechargeOrder.setSubbranchName("");
	rechargeOrder.setBankAccountName("");
	rechargeOrder.setBankCardNum("");
	rechargeOrder.setBankType("");
	rechargeOrder.setBizSystem(currentUser.getBizSystem());
	rechargeOrder.setRefType(EFundRefType.THIRDPAY.name());
	rechargeOrder.setFromType(currentUser.getLoginType());
	//保存新的订单到数据库中
	rechargeWithDrawOrderService.insertSelective(rechargeOrder);
	log.info("交易报文: "+yiFuPay);
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>

<body onload="pay.submit()">
	<form method="post" name="pay" id="pay" action="<%=payUrl%>">
		<TABLE>
			<TR>
				<TD>
				<input name='customerid' type='hidden'value="<%=yiFuPay.getCustomerid()%>" /> 
				<input name='version' type='hidden' value="<%=yiFuPay.getVersion()%>" /> 
				<input name='sdorderno' type='hidden' value="<%=yiFuPay.getSdorderno()%>" /> 
				<input name='paytype' type='hidden' value="<%=yiFuPay.getPaytype()%>" /> 
				<input name='total_fee' type='hidden' value="<%=yiFuPay.getTotal_fee()%>" />
				<input name='notifyurl' type='hidden' value="<%=yiFuPay.getNotifyurl()%>" /> 
				<input name='returnurl' type='hidden' value="<%=yiFuPay.getReturnurl()%>" /> 
				<input name='sign' type='hidden' value="<%=yiFuPay.getSign()%>" />
				</TD>
			</TR>
		</TABLE>

	</form>

</body>
</html>
