<%@page import="com.team.lottery.pay.model.hujingpay.RSAUtil"%>
<%@page import="org.apache.http.client.utils.URLEncodedUtils"%>
<%@page import="com.team.lottery.pay.model.youfupay.YouFuPay"%>
<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.team.lottery.service.QuickBankTypeService"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.util.IPUtil"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.team.lottery.vo.QuickBankType"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="com.team.lottery.util.RSAUtils"%>
<%@page import="com.team.lottery.pay.util.Base64Utils"%>
<%@page import="java.net.URLEncoder"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.team.lottery.service.RechargeConfigService"%>
<%@page import="com.team.lottery.vo.RechargeConfig"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
	String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	String serialNumber = request.getParameter("serialNumber"); // 获取配置ID;
	String orderMoneyYuan = OrderMoney;

	Date currTime = new Date();
	//时间以yyyy-MM-dd HH:mm:ss的方式表示
	SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);
	//时间以yyyyMMDDHHmmss的方式表示 
	SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss", Locale.CHINA);
	String webdate = new String(formatter1.format(currTime));
	String TradeDate = new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService) ApplicationContextUtil.getBean("chargePayService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));
	
	String[] moneys = OrderMoney.split("\\.");
	OrderMoney = moneys[0] + ".00";
	log.info("发起了安吉支付，生成订单号[{}],支付金额[{}]元,PayId[{}],payType[{}],chargePayId[{}]", serialNumber, OrderMoney, payId, payType,
			chargePayId);
	String payUrl = chargePay.getPayUrl();
	String Address = chargePay.getAddress();
	log.info("payUrl: " + payUrl + " Address: " + Address);

	//同步跳转地址
	String notifyUrl = "";
	if (chargePay.getNotifyUrl().startsWith("http://") || chargePay.getNotifyUrl().startsWith("https://")) {
		notifyUrl = chargePay.getNotifyUrl();
	} else {
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
		notifyUrl = basePath + chargePay.getNotifyUrl();
	}
	String userIp = IPUtil.getRequestIp(request);
	// 优付发起对象,
	YouFuPay youFuPay = new YouFuPay();
	youFuPay.setBrandNo(chargePay.getMemberId());
	youFuPay.setCallbackUrl(chargePay.getReturnUrl());
	youFuPay.setClientIP(userIp);
	youFuPay.setFrontUrl(notifyUrl);
	youFuPay.setOrderNo(serialNumber);
	youFuPay.setPrice(OrderMoney);
	youFuPay.setServiceType(payId);
	youFuPay.setSignType("RSA-S");
	youFuPay.setUserName(TradeDate);

	String privateKey = chargePay.getPrivatekey();
	// 加密参数.(MD5大写)
	String mark = "&";
	String md5SignStr = "brandNo=" + youFuPay.getBrandNo() + mark + "clientIP=" + youFuPay.getClientIP() + mark + "orderNo="
			+ youFuPay.getOrderNo() + mark + "price=" + youFuPay.getPrice() + mark + "serviceType=" + youFuPay.getServiceType()
			+ mark + "userName=" + youFuPay.getUserName();
	String resultSign = "";
	try {
		byte[] sign  = RSAUtils.signMd5ByPriKey(md5SignStr, privateKey);
		resultSign = Base64Utils.encode(sign);
		youFuPay.setSignature(resultSign);
	} catch (Exception e) {
		log.info(e.toString());
		response.getWriter().append("加密失败.");
		return;
	}
	log.info("md5原始串: " + md5SignStr + "生成后的交易签名:" + resultSign);
	log.info("优付支付发起对象: " + youFuPay.toString());
	JSONObject jsonParam = new JSONObject();
	jsonParam.put("brandNo", youFuPay.getBrandNo());
	jsonParam.put("callbackUrl", youFuPay.getCallbackUrl());
	jsonParam.put("frontUrl", youFuPay.getFrontUrl());
	jsonParam.put("orderNo", youFuPay.getOrderNo());
	jsonParam.put("price", youFuPay.getPrice());
	jsonParam.put("serviceType", youFuPay.getServiceType());
	jsonParam.put("signature", youFuPay.getSignature());
	jsonParam.put("signType", youFuPay.getSignType());
	jsonParam.put("userName", youFuPay.getUserName());
	jsonParam.put("clientIP", youFuPay.getClientIP());
	log.info("提交的Json参数:" + jsonParam);
	//获取第三方给我们返回的JSON数据做判断跳转
	String result = HttpClientUtil.doJsonForPost(payUrl, jsonParam);
	log.info("优付支付,发起返回第三方信息为:" + result);
	Boolean isJson = JSONUtils.isJson(result);
	String basePath = "";
	if (isJson == true) {
		JSONObject jsonObject = JSONUtils.toJSONObject(result);
		if (StringUtils.isBlank(result)) {
			out.print("发起支付失败");
			return;
		}
		//获取返回的状态.true为发起成功,其他为发起失败.
		String code = jsonObject.getString("code");
		
		if (code.equals("0")) {
			String isSuccess = jsonObject.getString("isSuccess");
			if("true".equals(isSuccess)){
				// 获取支付路径路径.
				String data = jsonObject.getString("data");
				JSONObject jsonData = JSONUtils.toJSONObject(data);
				String payUrlYoufu = jsonData.getString("payUrl");
				log.info("优付支付获取到的支付路径为: " + payUrlYoufu);
				response.sendRedirect(payUrlYoufu);
			}
			
		} else {
			out.print(result);
			return;
		}
	} else {
		out.print(result);
		log.error("优付支付支付发起失败: " + result);
		return;
	}
%>
