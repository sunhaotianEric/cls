<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.extvo.returnModel.SixEightSixReturnParameter"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="java.io.*"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
   /*  BufferedReader  streamReader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));  
    StringBuilder responseStrBuilder = new StringBuilder();  
    String inputStr;  
    while ((inputStr = streamReader.readLine()) != null) {
    	responseStrBuilder.append(inputStr);   
    } */
    String jsonStr=""; 
    Enumeration<String> paraNames = request.getParameterNames();
    while (paraNames.hasMoreElements()) {
		String paraKey = paraNames.nextElement();
		jsonStr += paraKey;
	}
	log.info("接收686回调通知报文: " + jsonStr);
	JSONObject jsonObject = JSONUtils.toJSONObject(jsonStr);
	SixEightSixReturnParameter sixEightSixReturnParameter = JSONUtils
			.toBean(jsonObject, SixEightSixReturnParameter.class);
	String sign = sixEightSixReturnParameter.getSign();
	String outOrderNo = sixEightSixReturnParameter.getOutOrderNo();
	String goodsClauses = sixEightSixReturnParameter.getGoodsClauses();
	String tradeAmount = sixEightSixReturnParameter.getTradeAmount();
	String shopCode = sixEightSixReturnParameter.getShopCode();
	String code = sixEightSixReturnParameter.getCode();
	String nonStr = sixEightSixReturnParameter.getNonStr();
	String msg = sixEightSixReturnParameter.getMsg();

	log.info("686支付成功通知平台处理信息如下: 商户ID: " + shopCode + " 商户订单号: "
			+ outOrderNo + " 订单结果: " + msg + " 订单金额: " + tradeAmount
			+ " MD5签名: " + sign);

	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService) ApplicationContextUtil
			.getBean("rechargeWithDrawOrderService");
	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService
			.getRechargeOrderBySerialNumber(outOrderNo);
	if (rechargeWithDrawOrders == null
			|| rechargeWithDrawOrders.size() != 1) {
		log.error("根据订单号[" + outOrderNo + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//

		out.println("SUCCESS");
		return;
	}
	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders
			.get(0);
	if (rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + outOrderNo + "]查找充值订单记录为空");
		out.println("SUCCESS");
		return;
	}

	//签名
	String Md5key = rechargeWithDrawOrder.getSign();
	String MARK = "&";
	String md5 = "code=" + code + MARK + "goodsClauses=" + goodsClauses
			+ MARK + "msg=" + msg + MARK + "nonStr=" + nonStr+MARK
			+ "outOrderNo=" + outOrderNo + MARK + "shopCode="
			+ shopCode + MARK + "tradeAmount=" + tradeAmount+MARK+"key="+Md5key;
	//Md5加密串
	String WaitSign = Md5Util.getMD5ofStr(md5);

	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if (WaitSign.compareTo(sign) == 0) {
		boolean dealResult = false;
		if (!StringUtils.isEmpty(code) && code.equals("0")) {
			BigDecimal factMoneyValue = new BigDecimal(tradeAmount);
			try {
				dealResult = rechargeWithDrawOrderService
						.rechargeOrderSuccessDeal(outOrderNo,
								factMoneyValue);
			} catch (UnEqualVersionException e) {
				log.error("686支付在线充值时发现版本号不一致,订单号[" + outOrderNo + "]");
				//启动新进程进行多次重试
				UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(
						UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION,
						outOrderNo, factMoneyValue, "");
				thread.start();
			} catch (Exception e) {
				//异常不进行重试
				log.error(e.getMessage());
			}
		} else {
			if (!code.equals("0")) {
				log.info("686支付处理错误支付结果：" + code);
			}
		}
		log.info("686支付订单处理入账结果:{}", dealResult);
		//通知686支付报文接收成功

		out.println("SUCCESS");
		return;
	} else {
		log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容["
				+ WaitSign + "]");
		//让第三方继续补发
		out.println("error");
		return;
	}
%>