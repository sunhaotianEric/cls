<%@page import="com.team.lottery.pay.model.shengshipay.ShengShiPay"%>
<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.team.lottery.service.QuickBankTypeService"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.util.IPUtil"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.team.lottery.vo.QuickBankType"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="java.net.URLEncoder"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.team.lottery.service.RechargeConfigService"%>
<%@page import="com.team.lottery.vo.RechargeConfig"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
	String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	String serialNumber = request.getParameter("serialNumber"); // 获取配置ID;
	String orderMoneyYuan = OrderMoney;
	
	User currentUser = (User) request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	if (currentUser == null) {
		response.sendRedirect(path + "/gameBet/login.html");
	}
	Date currTime = new Date();
	//时间以yyyy-MM-dd HH:mm:ss的方式表示
	SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);
	//时间以yyyyMMDDHHmmss的方式表示 
	SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss", Locale.CHINA);
	String webdate = new String(formatter1.format(currTime));
	String TradeDate = new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService) ApplicationContextUtil.getBean("chargePayService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));

	log.info("业务系统[{}],用户名[{}],发起了盛世支付，生成订单号[{}],支付金额[{}]分,PayId[{}],payType[{}],chargePayId[{}]",
	currentUser.getBizSystem(), currentUser.getUserName(), serialNumber, OrderMoney, payId, payType,
	chargePayId);
	String payUrl = chargePay.getPayUrl();
	String Address = chargePay.getAddress();
	log.info("payUrl: " + payUrl + " Address: " + Address);

	//同步跳转地址
	String notifyUrl = "";
	if (chargePay.getNotifyUrl().startsWith("http://") || chargePay.getNotifyUrl().startsWith("https://")) {
		notifyUrl = chargePay.getNotifyUrl();
	} else {
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
		+ path + "/";
		notifyUrl = basePath + chargePay.getNotifyUrl();
	}
	// 获取当前客户端IP
	// 获取当前用户的客户端IP.
	String userIp = IPUtil.getRequestIp(request);
	// 新建GtPay支付对象.
	ShengShiPay shengShiPay = new ShengShiPay();
	// 商户号
	shengShiPay.setMerchant_no(chargePay.getMemberId());
	// 订单号
	shengShiPay.setMerchant_order_no(serialNumber);
	// 回调地址
	shengShiPay.setNotify_url(chargePay.getReturnUrl());
	// 支付码表.
	shengShiPay.setPay_type(payId);
	// 同步跳转地址.
	shengShiPay.setReturn_url(notifyUrl);
	// 支付金额.
	shengShiPay.setTrade_amount(OrderMoney);
	//sign加密;
	String mark = "&";
	
	String md5SignStr = "merchant_no=" + shengShiPay.getMerchant_no() + mark +
						"merchant_order_no=" + shengShiPay.getMerchant_order_no() + mark +
						"notify_url=" + shengShiPay.getNotify_url() + mark +
						"pay_type=" + shengShiPay.getPay_type() + mark +
						"return_url=" + shengShiPay.getReturn_url() + mark +
						"trade_amount=" + shengShiPay.getTrade_amount() + mark + 
						"key=" + chargePay.getSign();

	//进行MD5小写加密
	String md5Sign = Md5Util.getMD5ofStr(md5SignStr);
	//打印日志
	log.info("md5原始串: " + md5SignStr + "生成后的交易签名:" + md5Sign);
	//设置签名
	shengShiPay.setSign(md5Sign);
	log.info(shengShiPay.toString());

	//通过JSON格式去提交数据,以便获取第三方跳转地址
	Map<String,String> dataMap = new HashMap<String,String>();
	dataMap.put("merchant_no", shengShiPay.getMerchant_no());
	dataMap.put("merchant_order_no", shengShiPay.getMerchant_order_no());
	dataMap.put("notify_url", shengShiPay.getNotify_url());
	dataMap.put("pay_type", shengShiPay.getPay_type());
	dataMap.put("return_url", shengShiPay.getReturn_url());
	dataMap.put("trade_amount", shengShiPay.getTrade_amount());
	dataMap.put("sign", shengShiPay.getSign());
	
	//获取第三方给我们返回的JSON数据做判断跳转
	String result = HttpClientUtil.doPost(Address , dataMap);
	log.info("盛世支付提交支付报文，返回内容:{}", result);
	Boolean isJson = JSONUtils.isJson(result);
	//最终我们要生成二维码的数据
	String payInfoNew = "";
	String basePath = "";
	if (isJson == true) {
		JSONObject jsonObject = JSONUtils.toJSONObject(result);
		if (StringUtils.isBlank(result)) {
			out.print("发起支付失败");
			return;
		}
		//获取返回的状态.0为发起成功,其他为发起失败.
		String status = jsonObject.getString("status");
		if (status.equals("1")) {
			String data = jsonObject.getString("data");
			// 获取二维码支付路径.
			JSONObject jsonObjectOfData = JSONUtils.toJSONObject(data);
			String pay_url = jsonObjectOfData.getString("pay_url");
			log.info("盛世支付获取到的支付二维码路径为: " + pay_url);
			response.sendRedirect(pay_url);
		} else {
			out.print(result);
			return;
		}
	} else {
		out.print(result);
		log.error("盛世支付支付发起失败: " + result);
		return;
	}
%>