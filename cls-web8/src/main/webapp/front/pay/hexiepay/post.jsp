<%@page import="com.team.lottery.pay.model.hexiepay.HeXiePay"%>
<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.team.lottery.service.QuickBankTypeService"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.team.lottery.vo.QuickBankType"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="java.net.URLEncoder"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
	//打印日志
	Logger log = LoggerFactory.getLogger(this.getClass());
	//获取当前web路径
	String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	String orderMoneyYuan = OrderMoney;
	//判断当前请求页面用户是否登录,没有的话就跳往登录页面
	User currentUser = (User) request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	if (currentUser == null) {
		response.sendRedirect(path + "/gameBet/login.html");
	}
	if (orderMoneyYuan.indexOf(".") > 0) {
		orderMoneyYuan = orderMoneyYuan.replaceAll("0+?$", "");//去掉多余的0  
		orderMoneyYuan = orderMoneyYuan.replaceAll("[.]$", "");//如最后一位是.则去掉  
	}
	//获取订单提交时间
	Date currTime = new Date();
	QuickBankTypeService quickBankTypeService = (QuickBankTypeService) ApplicationContextUtil.getBean("quickBankTypeService");
	ChargePayService chargePayService = (ChargePayService) ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService) ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.getPayMemmberMsg(Long.valueOf(chargePayId));
	//生成订单Id
	String orderId = MakeOrderNum.makeOrderNum("CZ");
	//打印日志
	log.info("业务系统[{}],用户名[{}],发起了和谐支付，生成订单号[{}],支付金额[{}],PayId[{}],payType[{}],chargePayId[{}]", currentUser.getBizSystem(), currentUser.getUserName(), orderId,
			OrderMoney, payId, payType, chargePayId);
	QuickBankType query = new QuickBankType();
	query.setPayId(payId);
	String bankType = "";
	List<QuickBankType> list = quickBankTypeService.getQuickBankTypeQuery(query);
	if (list.size() > 0) {
		bankType = list.get(0).getBankType();
	}

	//生成需要跳转的支付地址;
	String payUrl = chargePay.getPayUrl();//转发支付网关地址
	String Address = chargePay.getAddress(); //最终地址

	log.info("凡客支付报文转发地址payUrl: " + payUrl + ",实际网关地址address: " + Address);
	//同步跳转地址
	String notifyUrl = "";
	if (chargePay.getNotifyUrl().startsWith("http://") || chargePay.getNotifyUrl().startsWith("https://")) {
		notifyUrl = chargePay.getNotifyUrl();
	} else {
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
		notifyUrl = basePath + chargePay.getNotifyUrl();
	}
	
	//格式化时间.
	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	String strdate = format.format(currTime);
	
	//和谐支付.
	HeXiePay heXiePay = new HeXiePay();
	//支付金额.
	heXiePay.setPay_amount(OrderMoney);
	//支付时间.
	heXiePay.setPay_applydate(strdate);
	//支付方式.
	heXiePay.setPay_bankcode(payId);
	//跳转地址.
	heXiePay.setPay_callbackurl(notifyUrl);
	//商户号.
	heXiePay.setPay_memberid(chargePay.getMemberId());
	//通知地址.
	heXiePay.setPay_notifyurl(chargePay.getReturnUrl());
	//订单号.
	heXiePay.setPay_orderid(orderId);
	//商品名称.
	heXiePay.setPay_productname(currentUser.getUserName());
	
	//获取商户秘钥.
	String apiKey = chargePay.getSign();

	//sign加密;
	String mark = "&";
	//加密原始串.
	String Md5Sign = "pay_amount=" + heXiePay.getPay_amount() + mark + 
					 "pay_applydate=" + heXiePay.getPay_applydate() + mark +
					 "pay_bankcode=" + heXiePay.getPay_bankcode() + mark +
					 "pay_callbackurl=" + heXiePay.getPay_callbackurl() + mark +
					 "pay_memberid=" + heXiePay.getPay_memberid() + mark +
					 "pay_notifyurl=" + heXiePay.getPay_notifyurl() + mark +
					 "pay_orderid=" + heXiePay.getPay_orderid() + mark +
					 "key=" + apiKey;

	//进行MD5大写加密
	String Signature = Md5Util.getMD5ofStr(Md5Sign).toUpperCase();
	//打印日志
	log.info("md5原始串: " + Md5Sign + "生成后的交易签名:" + Signature);
	//设置签名
	heXiePay.setPay_md5sign(Signature);

	//冲值订单
	RechargeOrder rechargeOrder = new RechargeOrder();
	//用户ID
	rechargeOrder.setUserId(currentUser.getId());
	//快捷ID
	rechargeOrder.setChargePayId(chargePay.getId());
	//用户名称
	rechargeOrder.setUserName(currentUser.getUserName());
	//申请金额
	rechargeOrder.setApplyValue(new BigDecimal(orderMoneyYuan));
	//订单创建时间
	rechargeOrder.setCreatedDate(new Date());
	//充值类型
	rechargeOrder.setPayType(payType);
	//第三方充值类型
	rechargeOrder.setThirdPayType("HEXIEPAY");
	//操作描述
	rechargeOrder.setOperateDes(EFundPayType.valueOf(payType).getDescription());
	//流水号
	rechargeOrder.setSerialNumber(orderId);
	rechargeOrder.setPostscript("");
	rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT.name());
	rechargeOrder.setBankName("");
	rechargeOrder.setSubbranchName("");
	rechargeOrder.setBankAccountName("");
	rechargeOrder.setBankCardNum("");
	rechargeOrder.setBankType("");
	rechargeOrder.setBizSystem(currentUser.getBizSystem());
	rechargeOrder.setRefType(EFundRefType.THIRDPAY.name());
	rechargeOrder.setFromType(currentUser.getLoginType());
	//保存新的订单到数据库中
	rechargeWithDrawOrderService.insertSelective(rechargeOrder);
	//通过JSON格式去提交数据,以便获取第三方跳转地址
	
	log.info("交易报文: " + heXiePay); 
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>

<body onload="pay.submit()">
<form method="post" name="pay" id="pay" action="<%=payUrl%>">
<TABLE>
<TR>
	<TD>
	<input name='pay_amount' type='hidden' value= "<%=heXiePay.getPay_amount()%>"/>
	<input name='pay_applydate' type='hidden' value= "<%=heXiePay.getPay_applydate()%>"/>
	<input name='pay_bankcode' type='hidden' value= "<%=heXiePay.getPay_bankcode()%>"/>
	<input name='pay_callbackurl' type='hidden' value= "<%=heXiePay.getPay_callbackurl()%>"/>
	<input name='pay_memberid' type='hidden' value= "<%=heXiePay.getPay_memberid()%>"/>		
	<input name='pay_notifyurl' type='hidden' value= "<%=heXiePay.getPay_notifyurl()%>"/>
	<input name='pay_orderid' type='hidden' value= "<%= heXiePay.getPay_orderid()%>"/>
	<input name='pay_productname' type='hidden' value= "<%= heXiePay.getPay_productname()%>"/>	
	<input name="pay_md5sign" type="hidden"   value= "<%= heXiePay.getPay_md5sign()%>">	
	</TD>
</TR>
</TABLE>
	
</form>	

</body>
</html>
