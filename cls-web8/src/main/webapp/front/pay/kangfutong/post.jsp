<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.pay.model.KangFuTongPay"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
 <%@page errorPage="error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URL"%>
<%@page import="com.team.lottery.util.JaxbUtil"%>
<%@page import="com.team.lottery.extvo.returnModel.XingFuData"%>
<%@page import="com.team.lottery.extvo.returnModel.XingFuDetailData"%>
<%@page import="java.net.InetAddress"%>
<%@page import="com.team.lottery.util.JaxbUtil"%>
<%@page import="com.team.lottery.extvo.returnModel.XinYuTokenData"%>
<%@page import="sun.misc.BASE64Decoder"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
    String orderMoneyYuan = OrderMoney;
 
    
    User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
	
	Date currTime = new Date();
    //时间以yyyy-MM-dd HH:mm:ss的方式表示
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.CHINA);
    //时间以yyyyMMDDHHmmss的方式表示 
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss",Locale.CHINA);
    String webdate=new String(formatter1.format(currTime));
    String TradeDate=new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));
	String  orderId=MakeOrderNum.makeOrderNum("CZ");
	log.info("用户名称: "+currentUser.getUserName()+" 用户 ID号: "+currentUser.getId()+" 生成订单号: "+orderId+" 订单金额: "+OrderMoney);
	
	
	if(!"".equals(OrderMoney)) {	
		 BigDecimal a;
		a = new BigDecimal(OrderMoney); //使用分进行提交
		OrderMoney=String.valueOf(a);
	}
	else{
		OrderMoney = ("0");
	} 
	String payUrl = chargePay.getPayUrl();//转发支付网关地址
	String Address = chargePay.getAddress(); //最终康付通地址
	log.info("payUrl: "+payUrl+" Address: "+Address);
	//康付通支付请求对象
	KangFuTongPay kangFuTongPay =new KangFuTongPay();
	kangFuTongPay.setMerchant_code(chargePay.getMemberId());
	kangFuTongPay.setService_type("direct_pay");
	kangFuTongPay.setNotify_url(chargePay.getReturnUrl());
	kangFuTongPay.setInterface_version("V3.0");
	kangFuTongPay.setInput_charset("UTF-8");
	kangFuTongPay.setSign_type("RSA-S");
	kangFuTongPay.setReturn_url(chargePay.getNotifyUrl());
	if(payType.equals("QUICK_PAY")){
		kangFuTongPay.setPay_type("b2c");
	}else if(payType.equals("WEIXIN")){
		kangFuTongPay.setPay_type("weixin");
	}else if(payType.equals("ALIPAY")){
		kangFuTongPay.setPay_type("alipay_scan");
	}else if(payType.equals("QQPAY")){
		kangFuTongPay.setPay_type("tenpay_scan");
	}else if(payType.equals("UNIONPAY")){
		kangFuTongPay.setPay_type("yl_scan");
	}
	//kangFuTongPay.setPay_type(payType);
	kangFuTongPay.setOrder_no(orderId);
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.CHINA);
	String tradeDate=new String(formatter.format(new Date()));
	kangFuTongPay.setOrder_time(tradeDate);
	kangFuTongPay.setOrder_amount(OrderMoney);
	kangFuTongPay.setBank_code(payId);
	kangFuTongPay.setRedo_flag("1");
	kangFuTongPay.setProduct_name("cz");
	
	//生成md5密钥签名
    String MARK = "&";
	String md5 =new String("bank_code="+kangFuTongPay.getBank_code()+MARK+"input_charset="+kangFuTongPay.getInput_charset()+MARK+"interface_version="+kangFuTongPay.getInterface_version()+MARK+"merchant_code="+kangFuTongPay.getMerchant_code()+
			MARK+"notify_url="+kangFuTongPay.getNotify_url()+MARK+"order_amount="+kangFuTongPay.getOrder_amount()+MARK+"order_no="+kangFuTongPay.getOrder_no()+MARK+"order_time="+kangFuTongPay.getOrder_time()+MARK+"pay_type="+kangFuTongPay.getPay_type()+MARK+
			"product_name="+kangFuTongPay.getProduct_name()+MARK+"redo_flag="+kangFuTongPay.getRedo_flag()+MARK+"return_url="+kangFuTongPay.getReturn_url()+MARK+"service_type="+kangFuTongPay.getService_type());//MD5签名格式
    System.out.println(md5);
	
    String privateKey=chargePay.getPrivatekey();
	/* String Signature =RSAWithSoftware.signByPrivateKey(md5, privateKey);//计算MD5值
    System.out.println("Signature:"+Signature);

    //交易签名
    kangFuTongPay.setSign(Signature);
    log.info("md5原始串: "+md5 + "生成后的交易签名:" + Signature);
    System.out.println(md5);
    System.out.println(Signature); */
    //冲值订单
	RechargeOrder rechargeOrder = new RechargeOrder();
	//用户ID
	rechargeOrder.setUserId(currentUser.getId());
	//快捷ID
	rechargeOrder.setChargePayId(chargePay.getId());
	//用户名称
	rechargeOrder.setUserName(currentUser.getUserName());
	//申请金额
	rechargeOrder.setApplyValue(new BigDecimal(orderMoneyYuan));
	//订单创建时间
	rechargeOrder.setCreatedDate(new Date());
	//充值类型
	rechargeOrder.setPayType(payType);
	//第三方充值类型
    rechargeOrder.setThirdPayType("KANGFUTONG");
	//操作描述
	rechargeOrder.setOperateDes(EFundPayType.valueOf(payType).getDescription());
	//流水号
	rechargeOrder.setSerialNumber(orderId);
	rechargeOrder.setPostscript("");
	rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT.name());
	rechargeOrder.setBankName("");
	rechargeOrder.setSubbranchName("");
	rechargeOrder.setBankAccountName("");
	rechargeOrder.setBankCardNum("");
	rechargeOrder.setBankType("");
	rechargeOrder.setRefType(EFundRefType.THIRDPAY.name());
	rechargeOrder.setBizSystem(currentUser.getBizSystem());
	rechargeOrder.setFromType(currentUser.getLoginType());
	rechargeWithDrawOrderService.insertSelective(rechargeOrder);
	log.info("实际康付通支付网关地址["+Address +"],交易报文: "+kangFuTongPay);
%>
 
 <!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta id="viewport" name="viewport"
	content="initial-scale=0.3, maximum-scale=1, user-scalable=no, width=device-width">
<title>充值接口-提交信息处理</title>
<style>
body {
	text-align: center;
}

#weixinImg {
	margin-top: 150px;
	width: 280px;
}

p {
	font-size: 18px;
}
</style>
</head>
<body onload="pay.submit()">
<form method="post" name="pay" id="pay" action="<%=Address%>">
<TABLE>
<TR>
	<TD>
	<input type="hidden" name="sign" value="<%=kangFuTongPay.sign%>" />
	<input type="hidden" name="merchant_code" value="<%=kangFuTongPay.merchant_code%>" />
	<input type="hidden" name="service_type" value="<%=kangFuTongPay.service_type%>" />	
	<input type="hidden" name="pay_type" value="<%=kangFuTongPay.pay_type%>" />	
	<input type="hidden" name="interface_version" value="<%=kangFuTongPay.interface_version%>" />			
	<input type="hidden" name="input_charset" value="<%=kangFuTongPay.input_charset%>" />	
	<input type="hidden" name="notify_url" value="<%=kangFuTongPay.notify_url%>"/>
	<input type="hidden" name="sign_type" value="<%=kangFuTongPay.sign_type%>" />		
	<input type="hidden" name="order_no" value="<%=kangFuTongPay.order_no%>"/>
	<input type="hidden" name="order_time" value="<%=kangFuTongPay.order_time%>" />	
	<input type="hidden" name="order_amount" value="<%=kangFuTongPay.order_amount%>"/>
	<input type="hidden" name="product_name" value="<%=kangFuTongPay.product_name%>" />	
	<input type="hidden" name="return_url" value="<%=kangFuTongPay.return_url%>"/>	
	<input type="hidden" name="bank_code" value="<%=kangFuTongPay.bank_code%>" />	
	<input type="hidden" name="redo_flag" value="<%=kangFuTongPay.redo_flag%>"/>  
	
	
	</TD>
</TR>
</TABLE>
	
</form>	
</body>
<%-- <body>
	<img id="weixinImg" src='<%=codeUrl%>'></img>
	<img src="http://qr.topscan.com/api.php?bg=f3f3f3&fg=ff0000&gc=222222&el=l&w=684&m=30&text=<%=code%>"/>
	<p>
		请使用<span id="pay-way"><c:if test="${payType == 'ALIPAY' }">支付宝</c:if>
			<c:if test="${payType == 'WEIXIN' }">微信</c:if>
			<c:if test="${payType == 'QQPAY' }">QQ钱包</c:if>
			<c:if test="${payType == 'JDPAY' }">京东扫码</c:if>
			<c:if test="${payType == 'UNIONPAY' }">银联扫码</c:if></span>扫描二维码以完成支付
	</p>

</body> --%>
</html>

