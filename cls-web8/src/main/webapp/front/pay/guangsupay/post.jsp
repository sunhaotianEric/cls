<%@page import="com.team.lottery.util.StringUtils"%>
<%@page import="com.alibaba.fastjson.JSON"%>
<%@page import="com.team.lottery.pay.util.RSAUtils"%>
<%@page import="com.team.lottery.pay.model.guangsupay.GuangSuPay"%>
<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.team.lottery.service.QuickBankTypeService"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.util.IPUtil"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.team.lottery.vo.QuickBankType"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="java.net.URLEncoder"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.team.lottery.service.RechargeConfigService"%>
<%@page import="com.team.lottery.vo.RechargeConfig"%>
<%@page import="com.team.lottery.pay.util.RSA"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
	String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	String serialNumber = request.getParameter("serialNumber"); // 获取配置ID;
	String orderMoneyYuan = OrderMoney;

	Date currTime = new Date();
	//时间以yyyy-MM-dd HH:mm:ss的方式表示
	SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);
	//时间以yyyyMMDDHHmmss的方式表示 
	SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss", Locale.CHINA);
	String webdate = new String(formatter1.format(currTime));
	String TradeDate = new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService) ApplicationContextUtil.getBean("chargePayService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));

	log.info("业务系统[{}],发起了光速支付，生成订单号[{}],支付金额[{}]元,PayId[{}],payType[{}],chargePayId[{}]", chargePay.getBizSystem(), serialNumber, OrderMoney, payId, payType, chargePayId);
	String payUrl = chargePay.getPayUrl();
	String Address = chargePay.getAddress();
	log.info("payUrl: " + payUrl + " Address: " + Address);

	//同步跳转地址
	String notifyUrl = "";
	if (chargePay.getNotifyUrl().startsWith("http://") || chargePay.getNotifyUrl().startsWith("https://")) {
		notifyUrl = chargePay.getNotifyUrl();
	} else {
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
		notifyUrl = basePath + chargePay.getNotifyUrl();
	}
	// 获取秒级时间戳
	String timestamp = String.valueOf(currTime.getTime()/1000);
	// 获取当前客户端IP
	// 获取当前用户的客户端IP.
	String userIp = IPUtil.getRequestIp(request);
	// 光速支付.
	GuangSuPay guangSuPay = new GuangSuPay();
	guangSuPay.setCurrency_money(OrderMoney);
	guangSuPay.setCurrency_type("CNY");
	guangSuPay.setFortest("true");
	guangSuPay.setMchno(chargePay.getMemberId());
	guangSuPay.setOrder_no(serialNumber);
	guangSuPay.setOrder_type("1");
	guangSuPay.setPay_methods(payId);
	guangSuPay.setTimestamp(timestamp);
	guangSuPay.setUsername(TradeDate);
	guangSuPay.setVcoin_code("FCNY");
	
	

	//MD5加密;
	String key = chargePay.getSign();
	String md5SignStr = key + 
						"currency_money" + guangSuPay.getCurrency_money() +
						"currency_type" + guangSuPay.getCurrency_type() +
						"mchno" + guangSuPay.getMchno() +
						"order_no" + guangSuPay.getOrder_no() +
						"order_type" + guangSuPay.getOrder_type() +
						"pay_methods" + guangSuPay.getPay_methods() +
						"timestamp" + guangSuPay.getTimestamp() +
						"username" + guangSuPay.getUsername() +
						"vcoin_code" + guangSuPay.getVcoin_code() + key ;
	//进行MD5小写加密
	String md5Sign = Md5Util.getMD5ofStr(md5SignStr);
	//打印日志
	log.info("md5原始串: " + md5SignStr + "加密后生成的交易签名:" + md5Sign);

	//设置签名
	guangSuPay.setSignature(md5Sign);
	log.info(guangSuPay.toString());
	chargePay.getPublickey();
	Map<String, String>  jsonObjectDataJson = new HashMap();
	jsonObjectDataJson.put("username", guangSuPay.getUsername());
	jsonObjectDataJson.put("order_no", guangSuPay.getOrder_no());
	jsonObjectDataJson.put("order_type", guangSuPay.getOrder_type());
	jsonObjectDataJson.put("vcoin_code", guangSuPay.getVcoin_code());
	jsonObjectDataJson.put("currency_type", guangSuPay.getCurrency_type());
	jsonObjectDataJson.put("currency_money", guangSuPay.getCurrency_money());
	//jsonObjectDataJson.put("fortest", "true");
	jsonObjectDataJson.put("pay_methods", guangSuPay.getPay_methods());
	jsonObjectDataJson.put("mchno", guangSuPay.getMchno());
	jsonObjectDataJson.put("timestamp", guangSuPay.getTimestamp());
	jsonObjectDataJson.put("signature", guangSuPay.getSignature());
    log.info(jsonObjectDataJson.toString());
	//获取第三方给我们返回的JSON数据做判断跳转
	String result = HttpClientUtil.doPost(Address, jsonObjectDataJson);
	log.info("光速支付提交支付报文，返回内容:{}", result);
	Boolean isJson = JSONUtils.isJson(result);
	String basePath = "";
	if (isJson == true) {
		JSONObject jsonObject = JSONUtils.toJSONObject(result);
		if (StringUtils.isEmpty(result)) {
			out.print("发起支付失败");
			return;
		}
		//获取返回的状态.00为发起成功,其他为发起失败.
		String code = jsonObject.getString("errcode");
		if (code.equals("0")) {
			// 获取二维码支付路径.
			String data = jsonObject.getString("data");
			log.info("光速支付获取到的支付data数据为: " + data);
			response.sendRedirect(data);
		} else {
			out.print(result);
			return;
		}
	} else {
		out.print(result);
		log.error("光速支付支付发起失败: " + result);
		return;
	}
%>

<%-- <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
<style>
body {
	text-align: center;
}

#weixinImg {
	margin-top: 150px;
	width: 280px;
}

p {
	font-size: 18px;
}
</style>
</head>

<c:if test="${basePath!=''}">
	<body>
		<img id="weixinImg" src='<%=basePath%>'></img>
		<p>
			请使用<span id="pay-way"><c:if test="${payType == 'ALIPAY' }">支付宝</c:if>
				<c:if test="${payType == 'WEIXIN' }">微信</c:if> <c:if
					test="${payType == 'QQPAY' }">QQ钱包</c:if> <c:if
					test="${payType == 'JDPAY' }">京东扫码</c:if> <c:if
					test="${payType == 'UNIONPAY' }">银联扫码</c:if></span>扫描二维码以完成支付
		</p>
	</body>
</c:if>
</html> --%>