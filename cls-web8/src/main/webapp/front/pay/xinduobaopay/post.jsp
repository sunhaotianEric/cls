<%@page import="com.team.lottery.pay.model.xinduobaopay.XinDuoBaoPay"%>
<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.team.lottery.service.QuickBankTypeService"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.util.IPUtil"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import="com.team.lottery.vo.QuickBankType"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@page import="com.team.lottery.util.HttpClientUtil"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="java.net.URLEncoder"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.team.lottery.service.RechargeConfigService"%>
<%@page import="com.team.lottery.vo.RechargeConfig"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
	String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
	String serialNumber = request.getParameter("serialNumber"); // 获取配置ID;
	String orderMoneyYuan = OrderMoney;
	Date currTime = new Date();
	//时间以yyyy-MM-dd HH:mm:ss的方式表示
	SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);
	//时间以yyyyMMDDHHmmss的方式表示 
	SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss", Locale.CHINA);
	String webdate = new String(formatter1.format(currTime));
	String TradeDate = new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService) ApplicationContextUtil.getBean("chargePayService");
	ChargePay chargePay = chargePayService.selectByPrimaryKey(Long.valueOf(chargePayId));

	log.info("发起了鑫多宝支付，生成订单号[{}],支付金额[{}]元,PayId[{}],payType[{}],chargePayId[{}]",
	serialNumber, OrderMoney, payId, payType,
	chargePayId);
	String payUrl = chargePay.getPayUrl();
	String Address = chargePay.getAddress();
	log.info("payUrl: " + payUrl + " Address: " + Address);

	//同步跳转地址
	String notifyUrl = "";
	if (chargePay.getNotifyUrl().startsWith("http://") || chargePay.getNotifyUrl().startsWith("https://")) {
		notifyUrl = chargePay.getNotifyUrl();
	} else {
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
		+ path + "/";
		notifyUrl = basePath + chargePay.getNotifyUrl();
	}
	// 获取当前客户端IP
	// 获取当前用户的客户端IP.
	String userIp = IPUtil.getRequestIp(request);
	XinDuoBaoPay  xinDuoBaoPay = new XinDuoBaoPay();
	xinDuoBaoPay.setBuyerip(userIp);
	xinDuoBaoPay.setCustomno(serialNumber);
	xinDuoBaoPay.setMoney(OrderMoney);
	xinDuoBaoPay.setNotifyurl(chargePay.getReturnUrl());
	xinDuoBaoPay.setProductname(chargePay.getBizSystem());
	xinDuoBaoPay.setScantype(payId);
	xinDuoBaoPay.setSendtime(TradeDate);
	xinDuoBaoPay.setUsercode(chargePay.getMemberId());
	String mark = "|";
	String md5SignStr = xinDuoBaoPay.getUsercode() + mark +
						xinDuoBaoPay.getCustomno() + mark + 
						xinDuoBaoPay.getScantype() + mark +
						xinDuoBaoPay.getNotifyurl() + mark +
						xinDuoBaoPay.getMoney() + mark +
						xinDuoBaoPay.getSendtime() + mark +
						xinDuoBaoPay.getBuyerip() + mark +
						chargePay.getSign();
	//进行MD5小写加密
	String md5Sign = Md5Util.getMD5ofStr(md5SignStr);
	//打印日志
	log.info("md5原始串: " + md5SignStr + "生成后的交易签名:" + md5Sign);
	//设置签名
	xinDuoBaoPay.setSign(md5Sign);
	log.info("鑫多宝支付发起: " + xinDuoBaoPay.toString());
	//通过JSON格式去提交数据,以便获取第三方跳转地址
	Map<String,String> jsonDataMap = new HashMap<String,String>();
	jsonDataMap.put("usercode", xinDuoBaoPay.getUsercode());
	jsonDataMap.put("customno", xinDuoBaoPay.getCustomno());
	jsonDataMap.put("productname", xinDuoBaoPay.getProductname());
	jsonDataMap.put("scantype", xinDuoBaoPay.getScantype());
	jsonDataMap.put("notifyurl", xinDuoBaoPay.getNotifyurl());
	jsonDataMap.put("money", xinDuoBaoPay.getMoney());
	jsonDataMap.put("sendtime", xinDuoBaoPay.getSendtime());
	jsonDataMap.put("buyerip", xinDuoBaoPay.getBuyerip());
	jsonDataMap.put("sign", xinDuoBaoPay.getSign());
	String result = HttpClientUtil.doPost(Address, jsonDataMap);
	log.info("鑫多宝支付提交支付报文，返回内容:{}", result);
	Boolean isJson = JSONUtils.isJson(result);
	String basePath = "";
	if (isJson == true) {
		JSONObject jsonObject = JSONUtils.toJSONObject(result);
		if (StringUtils.isBlank(result)) {
			out.print("发起支付失败");
			return;
		}
		//获取返回的状态.true为发起成功,其他为发起失败.
		String successStatus = jsonObject.getString("success");
		
		if (successStatus.equals("true")) {
			// 获取支付路径路径.
			String data = jsonObject.getString("data");
			JSONObject jsonData = JSONUtils.toJSONObject(data);
			String payInfoNew = jsonData.getString("scanurl");
			log.info("鑫多宝支付获取到的支付路径为: " + payInfoNew);
			if("YL".equals(payId)){
				basePath = path+"/"+"tools/getQrCodePic?code="+URLEncoder.encode(payInfoNew, "utf-8");
			}else {
				response.sendRedirect(payInfoNew);
			}
		} else {
			out.print(result);
			return;
		}
	} else {
		out.print(result);
		log.error("鑫多宝支付支付发起失败: " + result);
		return;
	}
	request.setAttribute("payType", payType);
%>
<c:if test="${basePath!=''}">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
<style>
body {
	text-align: center;
}

#weixinImg {
	margin-top: 150px;
	width: 280px;
}

p {
	font-size: 18px;
}
</style>
</head>
	<body>
		<img id="weixinImg" src='<%=basePath%>'></img>
		<p>
			请使用<span id="pay-way">银联扫码</span>扫描二维码以完成支付
		</p>
	</body>
</html>
</c:if>