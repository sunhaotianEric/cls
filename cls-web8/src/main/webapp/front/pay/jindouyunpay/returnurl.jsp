<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="com.team.lottery.pay.model.jindouyun.JinDouYunPayReturn"%>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@page import="java.io.*"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="net.sf.json.JSONObject"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.extvo.returnModel.DuDuPayReturnParam"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
    
    // 支付成功回调响应码.
	String retCode = request.getParameter("retCode");
    // 支付成功回调响信息.
	String retMsg = request.getParameter("retMsg");
    // 商户号.
	String merchantCode = request.getParameter("merchantCode");
    // 商户订单号.
	String merchantOrderNo = request.getParameter("merchantOrderNo");
    // 平台订单号.
	String platOrderNo = request.getParameter("platOrderNo");
    // 订单交易完成时间.
	String orderCompleteTime = request.getParameter("orderCompleteTime");
    // 订单金额 单位:元.
	String orderAmount = request.getParameter("orderAmount");
    // 产品编码.
	String productCode = request.getParameter("productCode");
    // 支付状态.只有TRADE_SUCCESS才是支付成功,其他都是支付失败.
	String payState = request.getParameter("payState");
    // 签名
	String sign = request.getParameter("sign");
    
    // 新建筋斗云支付回调对象
    JinDouYunPayReturn jinDouYunPayReturn = new JinDouYunPayReturn();
    // 商户号.
    jinDouYunPayReturn.setMerchantCode(merchantCode);
    // 商户订单号.
    jinDouYunPayReturn.setMerchantOrderNo(merchantOrderNo);
    // 支付金额.
    jinDouYunPayReturn.setOrderAmount(orderAmount);
    // 完成时间.
    jinDouYunPayReturn.setOrderCompleteTime(orderCompleteTime);
    // 支付状态.
    jinDouYunPayReturn.setPayState(payState);
    // 平台订单号.
    jinDouYunPayReturn.setPlatOrderNo(platOrderNo);
    // 产品信息.
    jinDouYunPayReturn.setProductCode(productCode);
    // 相印码.
    jinDouYunPayReturn.setRetCode(retCode);
    // 响应信息.
    jinDouYunPayReturn.setRetMsg(retMsg);
    // 签名
    jinDouYunPayReturn.setSign(sign);
    // 打印日志.通知成功.
	log.info(jinDouYunPayReturn.toString());

	RechargeOrderService rechargeOrderService = (RechargeOrderService) ApplicationContextUtil.getBean("rechargeOrderService"); 
	List<RechargeOrder> rechargeOrders = rechargeOrderService.getRechargeOrderBySerialNumber(merchantOrderNo);
	if (rechargeOrders == null || rechargeOrders.size() != 1) {
		log.error("根据订单号[" + merchantOrderNo + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知筋斗云支付报文接收成功
		out.print("SUCCESS");
	}
	RechargeOrder rechargeWithDrawOrder = rechargeOrders.get(0);
	if (rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + merchantOrderNo + "]查找充值订单记录为空");
		//通知筋斗云支付报文接收成功
		out.print("SUCCESS");
	}
	//获取秘钥.
	String Md5key = rechargeWithDrawOrder.getSign();
	String MARK = "&";
	//拼接加密字符串.
	String mD5Str = "merchantCode=" + merchantCode + MARK +
					"merchantOrderNo=" + merchantOrderNo + MARK +
					"orderAmount=" + orderAmount + MARK + 
					"orderCompleteTime=" + orderCompleteTime + MARK + 
					"payState=" + payState + MARK + 
					"platOrderNo=" + platOrderNo + MARK +
					"productCode=" + productCode + MARK +
					"retCode=" + retCode + MARK +
					"retMsg=" + retMsg + MARK +
					"key=" + Md5key;
	
	log.info("当前MD5源码:" + mD5Str);
	//Md5加密串并且大写
	String WaitSign = Md5Util.getMD5ofStr(mD5Str).toUpperCase();
	log.info("加密后的MD5: " + WaitSign);

	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if (WaitSign.compareTo(sign) == 0) {
		boolean dealResult = false;
		if (!StringUtils.isEmpty(payState) && payState.equals("TRADE_SUCCESS")) {
			BigDecimal factMoneyValue = new BigDecimal(orderAmount);
			try {
				dealResult = rechargeOrderService.rechargeOrderSuccessDeal(merchantOrderNo, factMoneyValue);
			} catch (UnEqualVersionException e) {
				log.error("筋斗云支付充值时发现版本号不一致,订单号[" + merchantOrderNo + "]");
				//启动新进程进行多次重试
				UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, merchantOrderNo,
						factMoneyValue, "");
				thread.start();
			} catch (Exception e) {
				//异常不进行重试
				log.error(e.getMessage());
			}
		} else {
			if (!payState.equals("TRADE_SUCCESS")) {
				log.info("筋斗云支付处理错误支付结果payState为：" +  payState);
			}
		}
		log.info("筋斗云支付订单处理入账结果:{}", dealResult);
		//通知凡客付报文接收成功
		out.println("SUCCESS");
		return;
	} else {
		log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容[" + WaitSign + "]");
		out.println("SUCCESS");
		return;
	}
%>