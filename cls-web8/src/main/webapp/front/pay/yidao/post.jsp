<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.DigestUtil"%>
<%@page import="com.team.lottery.pay.model.YiDaoPay"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
<%  

    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
    String orderMoneyYuan = OrderMoney;
 
    
    User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
	
	Date currTime = new Date();
    //时间以yyyy-MM-dd HH:mm:ss的方式表示
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.CHINA);
    //时间以yyyyMMDDHHmmss的方式表示 
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss",Locale.CHINA);
    String webdate=new String(formatter1.format(currTime));
    String TradeDate=new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.getPayMemmberMsg(Long.valueOf(chargePayId));
	String  orderId = MakeOrderNum.makeOrderNum("CZ");
	log.info("用户名称: "+currentUser.getUserName()+" 用户 ID号: "+currentUser.getId()+" 生成订单号: "+orderId+" 订单金额: "+OrderMoney);
	
	if (!"".equals(OrderMoney)) 
	{	BigDecimal a;
		a = new BigDecimal(OrderMoney).multiply(BigDecimal.valueOf(100)); //使用分进行提交
		OrderMoney=String.valueOf(a.setScale(0));
	}else{
		OrderMoney = ("0");
	}
	 
	String payUrl = chargePay.getPayUrl();//转发支付网关地址
	String Address = chargePay.getAddress(); //最终易到支付地址
	log.info("payUrl: "+payUrl+" Address: "+Address);
	
	//易到支付对象
	YiDaoPay yiDaoPay =new YiDaoPay();
	//业务类型
	yiDaoPay.setP0_Cmd("Buy");
	//商户编号
	yiDaoPay.setP1_MerId(chargePay.getMemberId());
	//商户订单号
	yiDaoPay.setP2_Order(orderId);
	//支付金额
	yiDaoPay.setP3_Amt(orderMoneyYuan);
	//交易币种
	yiDaoPay.setP4_Cur("CNY");
	//商品名称
	yiDaoPay.setP5_Pid("productName");
	//商品种类
	yiDaoPay.setP6_Pcat("productType");
	//商品描述
	yiDaoPay.setP7_Pdesc("productDescn");
	//后台通知地址
	yiDaoPay.setP8_Url(chargePay.getNotifyUrl());
	//前台返回地址
	yiDaoPay.setP9_SAF(chargePay.getReturnUrl());
	//商户扩展信息
	yiDaoPay.setPa_MP("merchantExtraInfo");
	//通道编码
	yiDaoPay.setPd_FrpId(payId);
	//应答机制
	yiDaoPay.setPr_NeedResponse("1");
	//支付IP
	yiDaoPay.setPayerIp("");
    
	/* partner={}&banktype={}&paymoney={}&ordernumber={}&callbackurl={}key */
	String hmacStr =new String(yiDaoPay.getP0_Cmd()+yiDaoPay.getP1_MerId()+yiDaoPay.getP2_Order()+yiDaoPay.getP3_Amt()+
			               yiDaoPay.getP4_Cur()+yiDaoPay.getP5_Pid()+yiDaoPay.getP6_Pcat()+yiDaoPay.getP7_Pdesc()+
	                       yiDaoPay.getP8_Url()+yiDaoPay.getP9_SAF()+yiDaoPay.getPa_MP()+yiDaoPay.getPd_FrpId()+yiDaoPay.getPr_NeedResponse());//签名字符串
	String verifyHmac = DigestUtil.hmacSign(hmacStr, chargePay.getSign());
    //交易签名
    yiDaoPay.setHmac(verifyHmac);
    log.info("md5原始串: "+hmacStr + "生成后的交易签名:" + verifyHmac);
    //冲值订单
	RechargeOrder rechargeOrder = new RechargeOrder();
	//用户ID
	rechargeOrder.setUserId(currentUser.getId());
	//快捷ID
	rechargeOrder.setChargePayId(chargePay.getId());
	//用户名称
	rechargeOrder.setUserName(currentUser.getUserName());
	//申请金额
	rechargeOrder.setApplyValue(new BigDecimal(orderMoneyYuan));
	//订单创建时间
	rechargeOrder.setCreatedDate(new Date());
	//充值类型
	rechargeOrder.setPayType(payType);
	//第三方充值类型
	rechargeOrder.setThirdPayType("YIDAO");
	//操作描述
	rechargeOrder.setOperateDes(EFundPayType.valueOf(payType).getDescription());
	//流水号
	rechargeOrder.setSerialNumber(orderId);
	rechargeOrder.setPostscript("");
	rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT.name());
	rechargeOrder.setBankName("");
	rechargeOrder.setSubbranchName("");
	rechargeOrder.setBankAccountName("");
	rechargeOrder.setBankCardNum("");
	rechargeOrder.setBankType("");
	rechargeOrder.setBizSystem(currentUser.getBizSystem());
	rechargeOrder.setRefType(EFundRefType.THIRDPAY.name());
	rechargeOrder.setFromType(currentUser.getLoginType());
	rechargeWithDrawOrderService.insertSelective(rechargeOrder);
	log.info("提交中转支付网关地址["+payUrl+"],实际易到支付网关地址["+Address +"],交易报文: "+yiDaoPay);
 %>
 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>

<body onload="pay.submit()">
<form method="get" name="pay" id="pay" action="<%=payUrl%>">
<TABLE>
<TR>
	<TD>
	        <input name='Address' type='hidden' value= "<%=Address%>"/>
            <input type='hidden' name='p0_Cmd'   value="<%=yiDaoPay.p0_Cmd%>"/>
  			<input type='hidden' name='p1_MerId' value="<%=yiDaoPay.p1_MerId%>"/>
  		    <input type='hidden' name='p2_Order' value="<%=yiDaoPay.p2_Order%>"/>
  	        <input type='hidden' name='p3_Amt'   value="<%=yiDaoPay.p3_Amt%>"/>
  	        <input type='hidden' name='p4_Cur'   value="<%=yiDaoPay.p4_Cur%>"/>
  		    <input type='hidden' name='p5_Pid'   value="<%=yiDaoPay.p5_Pid%>"/>
  		    <input type='hidden' name='p6_Pcat'  value="<%=yiDaoPay.p6_Pcat%>"/>
  		    <input type='hidden' name='p7_Pdesc' value="<%=yiDaoPay.p7_Pdesc%>"/>
  	        <input type='hidden' name='p8_Url'   value="<%=yiDaoPay.p8_Url%>"/>
  		    <input type='hidden' name='p9_SAF'   value="<%=yiDaoPay.p9_SAF%>"/>
  	        <input type='hidden' name='pa_MP'    value="<%=yiDaoPay.pa_MP%>"/>
  		    <input type='hidden' name='pd_FrpId' value="<%=yiDaoPay.pd_FrpId%>"/>
  		    <input type='hidden' name='pr_NeedResponse' value="<%=yiDaoPay.pr_NeedResponse%>"/>
  		    <input type='hidden' name='hmac'    value="<%=yiDaoPay.hmac%>"/>
	</TD>
</TR>
</TABLE>
	
</form>	

</body>
</html>
