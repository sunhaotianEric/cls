<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.pay.model.Xhpay"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@ page import="org.apache.log4j.*,org.apache.commons.logging.*"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<jsp:useBean id='oMD5' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
<%  

     
    String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
    String orderMoneyYuan = OrderMoney;
    Log log=LogFactory.getLog(path + "/gameBet/pay/x6pay/post.jsp");
    
    User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
	
	Date currTime = new Date();
    //时间以yyyy-MM-dd HH:mm:ss的方式表示
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.CHINA);
    //时间以yyyyMMDDHHmmss的方式表示 
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss",Locale.CHINA);
    String webdate=new String(formatter1.format(currTime));
    String TradeDate=new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.getPayMemmberMsg(Long.valueOf(chargePayId));
	String  orderId = MakeOrderNum.makeOrderNum("CZ");
	log.info("用户名称: "+currentUser.getUserName()+" 用户 ID号: "+currentUser.getId()+" 生成订单号: "+orderId+" 订单金额: "+OrderMoney);
	
	
	/* if (!"".equals(OrderMoney)) 
	{	BigDecimal a;
		a = new BigDecimal(OrderMoney); //使用分进行提交
		OrderMoney=String.valueOf(a.setScale(2));
	}
	else{
		OrderMoney = ("0");
	}*/
	String payUrl = chargePay.getPayUrl();//借贷混合
	String Address = chargePay.getAddress(); //最终迅汇宝地址
	log.info("payUrl: "+payUrl+" Address: "+Address);
	//迅汇宝请求对象
	Xhpay x6Pay =new Xhpay();
	x6Pay.setMerchno(chargePay.getMemberId());
	x6Pay.setAmount(OrderMoney);
	x6Pay.setGoodsName("ads");
	x6Pay.setPayType(payId);
	x6Pay.setNotifyUrl(chargePay.getReturnUrl());
	/* x6Pay.setReturnUrl(chargePay.getReturnUrl()); */
	x6Pay.setTraceno(orderId);
	x6Pay.setSettleType("1");
// 	x6Pay.setSettleType("1");
	/* x6Pay.setCertno("123");
	x6Pay.setMobile("15365789188");
	x6Pay.setAccountno("7700350501");
	x6Pay.setAccountName("15365789188");
	x6Pay.setFee("0"); */
	//生成md5密钥签名
    String Md5key = chargePay.getSign();///////////md5密钥（KEY）
	String MARK = "&";
	String md5 =new String("amount="+x6Pay.getAmount()+MARK+"goodsName="+x6Pay.getGoodsName()+MARK+"merchno="+x6Pay.getMerchno()+MARK+"notifyUrl="+x6Pay.getNotifyUrl()+MARK+
			"payType="+x6Pay.getPayType()+MARK+"settleType="+x6Pay.getSettleType()+MARK+"traceno="+x6Pay.getTraceno()+MARK+Md5key);//MD5签名格式
	String Signature = oMD5.getMD5ofStr(md5);//计算MD5值
	log.info("md5原始串: "+md5);
	log.info("交易签名: "+Signature);
    //交易签名
    x6Pay.setSignature(Signature);
	log.info("交易报文: "+x6Pay);
    //冲值订单
	RechargeOrder rechargeOrder = new RechargeOrder();
	//用户ID
	rechargeOrder.setUserId(currentUser.getId());
	//快捷ID
	rechargeOrder.setChargePayId(chargePay.getId());
	//用户名称
	rechargeOrder.setUserName(currentUser.getUserName());
	//申请金额
	rechargeOrder.setApplyValue(new BigDecimal(orderMoneyYuan));
	//订单创建时间
	rechargeOrder.setCreatedDate(new Date());
	//充值类型
	rechargeOrder.setPayType(payType);
	//第三方充值类型
	rechargeOrder.setThirdPayType("X6PAY");
	//操作描述
	rechargeOrder.setOperateDes(EFundPayType.valueOf(payType).getDescription());
	//流水号
	rechargeOrder.setSerialNumber(orderId);
	rechargeOrder.setPostscript("");
	rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT.name());
	rechargeOrder.setBankName("");
	rechargeOrder.setSubbranchName("");
	rechargeOrder.setBankAccountName("");
	rechargeOrder.setBankCardNum("");
	rechargeOrder.setBankType("");
	rechargeOrder.setBizSystem(currentUser.getBizSystem());
	rechargeOrder.setRefType(EFundRefType.THIRDPAY.name());
	rechargeOrder.setFromType(currentUser.getLoginType());
	rechargeWithDrawOrderService.insertSelective(rechargeOrder);
 %>
 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>

<body onload="pay.submit()">
<form method="post" name="pay" id="pay" action="<%=payUrl%>">
<TABLE>
<TR>
	<TD>
	<input name='Address' type='hidden' value= "<%=Address%>"/>
	<input name='merchno' type='hidden' value= "<%=x6Pay.merchno%>"/>
	<input name='amount' type='hidden' value= "<%=x6Pay.amount%>"/>
	<input name='traceno' type='hidden' value= "<%=x6Pay.traceno%>"/>
	<input name='payType' type='hidden' value= "<%=x6Pay.payType%>"/>
	<input name='goodsName' type='hidden' value= "<%=x6Pay.goodsName%>"/>		
	<input name='notifyUrl' type='hidden' value= "<%=x6Pay.notifyUrl%>"/>
	<input name='remark' type='hidden' value= "<%=x6Pay.remark%>"/>	
	<input name='settleType' type='hidden' value= "<%=x6Pay.settleType%>"/>	
	<input name='certno' type='hidden' value= "<%=x6Pay.certno%>"/>	
	<input name='mobile' type='hidden' value= "<%=x6Pay.mobile%>"/>	
	<input name='accountno' type='hidden' value= "<%=x6Pay.accountno%>"/>	
	<input name='accountName' type='hidden' value= "<%=x6Pay.accountName%>"/>
	<input name='fee' type='hidden' value= "<%=x6Pay.fee%>"/>			
	<input name='signature' type='hidden' value= "<%=x6Pay.signature%>" />

	</TD>
</TR>
</TABLE>
	
</form>	

</body>
</html>
