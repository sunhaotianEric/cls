<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage=""%>
<%@page
	import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.vo.ChargePay"%>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page
	import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@page import="java.io.*"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="net.sf.json.JSONObject"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.extvo.returnModel.DuDuPayReturnParam"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
	String path = request.getContextPath();
	BufferedReader streamReader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
	StringBuilder responseStrBuilder = new StringBuilder();
	String inputStr;
	while ((inputStr = streamReader.readLine()) != null) {
		responseStrBuilder.append(inputStr);
	}
	log.info("接收万年支付回调通知报文: " + responseStrBuilder.toString());
	JSONObject jsonObject = JSONUtils.toJSONObject(responseStrBuilder.toString());

	// 状态码（0 支付成功）
	String status = jsonObject.getString("status");
	// 订单号
	String orderId = jsonObject.getString("orderId");
	// 商户订单号
	String orderIdCp = jsonObject.getString("orderIdCp");
	// 官方流水号
	String linkId = jsonObject.getString("linkId");
	// 支付金额（单位分）
	String money = jsonObject.getString("money");
	// 支付时间戳（13 位）
	String payTime = jsonObject.getString("payTime");
	// 商户透传
	String cpParam = jsonObject.getString("cpParam");
	// 版本号
	String version = jsonObject.getString("version");
	// 签名.
	String sign = jsonObject.getString("sign");

	log.info("万年支付成功通知平台处理信息如下: 状态码" + status + " 订单号:" + orderId + "商户订单号:" + orderIdCp  + "官方流水号:" + linkId + "支付金额:" + money + "支付时间戳:" + payTime + "商户透传:" + cpParam + "版本号:" + version + "签名:" + sign);

	/* RechargeWithDrawOrderService rechargeWithDrawOrderService = (RechargeWithDrawOrderService) ApplicationContextUtil.getBean("rechargeWithDrawOrderService"); */
	RechargeOrderService rechargeOrderService = (RechargeOrderService) ApplicationContextUtil.getBean("rechargeOrderService"); 
	List<RechargeOrder> rechargeWithDrawOrders = rechargeOrderService.getRechargeOrderBySerialNumber(orderIdCp);
	if (rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1) {
		log.error("根据订单号[" + orderIdCp + "]查找充值订单记录为空或者有两笔相同订单号订单");
		// 通知微付通接收报文成功.
		response.getOutputStream().write("success".getBytes("UTF-8"));
	}
	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
	if (rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + orderIdCp + "]查找充值订单记录为空");
		// 通知微付通接收报文成功.
		response.getOutputStream().write("success".getBytes("UTF-8"));
		return;
	}

	// 查询第三方支付配置信息
	Long chargePayId = rechargeWithDrawOrder.getChargePayId();
	ChargePayService chargePayService = (ChargePayService) ApplicationContextUtil.getBean("chargePayService");
	ChargePay chargePay = chargePayService.getPayMemmberMsg(Long.valueOf(chargePayId));

	//获取秘钥.
	String Md5key = rechargeWithDrawOrder.getSign();

	//拼接加密字符串.
	String mD5Str = "status=" + status + "&orderId=" + orderId + "&orderIdCp=" + orderIdCp + "&linkId=" + linkId + "&money=" + money + "&payTime="+payTime + "&cpParam=" + cpParam + "&version="+version
			+ "&" + Md5key;

	log.info("当前MD5源码:" + mD5Str);
	//Md5加密串小写
	String WaitSign = Md5Util.getMD5ofStr(mD5Str);
	log.info("加密后的MD5: " + WaitSign);

	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if (WaitSign.compareTo(sign) == 0) {
		boolean dealResult = false;
		if (!StringUtils.isEmpty(status) && status.equals("0")) {
			BigDecimal factMoneyValue = new BigDecimal(money);
			try {
				dealResult = rechargeOrderService.rechargeOrderSuccessDeal(orderIdCp, factMoneyValue);
			} catch (UnEqualVersionException e) {
				log.error("万年支付充值时发现版本号不一致,订单号[" + orderIdCp + "]");
				//启动新进程进行多次重试
				UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, orderIdCp,
						factMoneyValue, "");
				thread.start();
			} catch (Exception e) {
				//异常不进行重试
				log.error(e.getMessage());
			}
		} else {
			if (!status.equals("0")) {
				log.info("万年支付处理错误支付结果：" + status.equals("0"));
			}
		}
		log.info("万年支付付订单处理入账结果:{}", dealResult);
		//通知凡客付报文接收成功
		out.println("success");
		return;
	} else {
		log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容[" + WaitSign + "]");
		out.println("success");
		return;
	}
%>