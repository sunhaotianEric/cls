<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage=""%>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page
	import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@page import="java.io.*"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="net.sf.json.JSONObject"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page
	import="com.team.lottery.extvo.returnModel.TianTianKuaiPayReturnParam"%>
<%@page import="com.team.lottery.pay.util.MapSortUtil"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%
	Logger log = LoggerFactory.getLogger(this.getClass());
	String path = request.getContextPath();
	//获取请求参数.
	Map<String, String> parameters = new HashMap<String, String>();

	Enumeration<String> parameterNames = request.getParameterNames();

	while (parameterNames.hasMoreElements()) {

		String parameterName = parameterNames.nextElement();

		parameters.put(parameterName, request.getParameter(parameterName));
	}
	String sign = request.getParameter("sign");
	if (StringUtils.isEmpty(sign)) {
		log.info("天天快付数据出错.");
		//通知赢通宝报文接收成功
		out.println("SUCCESS");
		return;
	}
	//获取商户订单号.
	String poId = request.getParameter("out_trade_no");
	//获取实际支付金额
	String realAmount = request.getParameter("total_amount");
	//获取商户号.
	String merchantId = request.getParameter("merchant_id");
	String resultCode = request.getParameter("result_code");
	log.info("天天快付成功通知平台处理信息如下: 商户ID:" + merchantId + " 订单号:" + poId + "订单金额:" + realAmount + "签名:" + sign);
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService) ApplicationContextUtil.getBean("rechargeOrderService");
	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(poId);
	if (rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1) {
		log.error("根据订单号[" + poId + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知八达付支付报文接收成功
		response.getOutputStream().write("SUCCESS".getBytes("UTF-8"));
	}
	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
	if (rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + poId + "]查找充值订单记录为空");
		//通知八达付支付报文接收成功
		response.getOutputStream().write("SUCCESS".getBytes("UTF-8"));
	}
	//获取加密后的Md5
	MapSortUtil mapSortUtil = new MapSortUtil();
	String WaitSign = mapSortUtil.sign(parameters, rechargeWithDrawOrder.getSign());
	log.info("加密后的MD5: " + WaitSign);
	if (WaitSign.compareTo(sign) == 0) {
		boolean dealResult = false;
		//获取当前订单的订单价格
		String money = rechargeWithDrawOrder.getApplyValue().toString();
		//需要入账的资产;
		BigDecimal factMoneyValue = new BigDecimal(realAmount);
		if (!StringUtils.isEmpty(resultCode) && resultCode.equals("0")) {
			try {
				dealResult = rechargeWithDrawOrderService.rechargeOrderSuccessDeal(poId, factMoneyValue);
			} catch (UnEqualVersionException e) {
				log.error("天天快付充值时发现版本号不一致,订单号[" + poId + "]");
				//启动新进程进行多次重试
				UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION, poId,
						factMoneyValue, "");
				thread.start();
			} catch (Exception e) {
				//异常不进行重试
				log.error(e.getMessage());
			}
		} else {
			if (!resultCode.equals("0")) {
				log.info("天天快付处理错误支付结果：" + resultCode.equals("0"));
			}
		}
		log.info("天天快付支付订单处理入账结果:{}", dealResult);
		//通知联赢付报文接收成功
		response.getOutputStream().write("SUCCESS".getBytes("UTF-8"));
		return;
	} else {
		log.info("MD5校验失败,报文返回MD5签名内容[" + sign + "],计算MD5签名内容[" + WaitSign + "]");
		response.getOutputStream().write("SUCCESS".getBytes("UTF-8"));
		return;
	}
%>