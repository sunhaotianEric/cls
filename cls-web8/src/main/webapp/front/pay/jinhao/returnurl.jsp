<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<%@page import="java.util.*"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.text.*"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.util.StringUtils"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@ page import="com.team.lottery.exception.UnEqualVersionException"%>
<%@ page import="com.team.lottery.system.UserVersionExceptionPayDealThread"%>
<%@page import="java.math.*"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.extvo.returnModel.XinMaSendParameter"%>
<%@page import="com.team.lottery.extvo.returnModel.JinHaoReturnParameter"%>
<%@page import="com.team.lottery.util.JSONUtils"%>
<%@page import="java.io.*"%>
<%@page import="net.sf.json.JSONObject"%>
<%@page import=" com.team.lottery.pay.util.MerchantApiUtil"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<%  
    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
    String payKey=request.getParameter("payKey");
    String productName = request.getParameter("productName")==null?"": new String(request.getParameter("productName").getBytes("iso-8859-1"), "utf-8");
    String outTradeNo=request.getParameter("outTradeNo");
    String orderPrice=request.getParameter("orderPrice");
    String productType=request.getParameter("productType");
    String tradeStatus=request.getParameter("tradeStatus");
    String successTime=request.getParameter("successTime");
    String orderTime=request.getParameter("orderTime");
    String trxNo=request.getParameter("trxNo");
    String remark = request.getParameter("remark")==null?"":new String(request.getParameter("remark").getBytes("iso-8859-1"), "utf-8");
    String sign=request.getParameter("sign");
    
	log.info("金好支付成功通知平台处理信息如下: 商户ID: "+payKey+" 商户订单号: "+outTradeNo+" 订单结果: "+tradeStatus+" 订单金额: "+orderPrice+"MD5签名: "+sign);
    
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	List<RechargeOrder> rechargeWithDrawOrders = rechargeWithDrawOrderService.getRechargeOrderBySerialNumber(outTradeNo);
	if(rechargeWithDrawOrders == null || rechargeWithDrawOrders.size() != 1){
		log.error("根据订单号[" + outTradeNo + "]查找充值订单记录为空或者有两笔相同订单号订单");
		//通知金好支付报文接收成功
	    out.println("ERROR");
		return;
	}
	RechargeOrder rechargeWithDrawOrder = rechargeWithDrawOrders.get(0);
	if(rechargeWithDrawOrder == null) {
		log.error("根据订单号[" + outTradeNo + "]查找充值订单记录为空");
		//通知新码支付报文接收成功
	    out.println("ERROR");
		return;
	}
	
	//签名
    Map<String, Object> paramMap = new HashMap<String, Object>();
    paramMap.put("payKey", payKey);// 商户支付Key
	paramMap.put("productName", productName);// 商品名称
	paramMap.put("outTradeNo", outTradeNo);// 订单编号
	paramMap.put("orderPrice", orderPrice); //支付金额
	paramMap.put("productType",productType);//B2C T0支付
	paramMap.put("tradeStatus", tradeStatus); //订单状态
	paramMap.put("successTime", successTime);// 成功时间
	paramMap.put("orderTime", orderTime);// 订单时间
	paramMap.put("trxNo", trxNo);// 页面通知返回url
	if(!remark.equals("")){
	paramMap.put("remark", remark);// 后台消息通知Url
	}
	//Md5加密串
	String WaitSign = md5Util.getSign(paramMap,"&paySecret=", rechargeWithDrawOrder.getSign());
	/* //Md5加密串
	String WaitSign = md5Util.Sign(md5,Md5key); */

	//从第三方返回的MD5加密签名串与后台Md5加密串进行校验
	if(WaitSign.compareTo(sign)==0){
		boolean dealResult = false;
		if(!StringUtils.isEmpty(tradeStatus) && tradeStatus.equals("SUCCESS")){
			BigDecimal factMoneyValue = new BigDecimal(orderPrice).divide(new BigDecimal(100));
			try {
				dealResult = rechargeWithDrawOrderService.rechargeOrderSuccessDeal(outTradeNo,factMoneyValue);
			}catch(UnEqualVersionException e) {
		   		log.error("金好支付在线充值时发现版本号不一致,订单号["+outTradeNo+"]");
		   		//启动新进程进行多次重试
		   		UserVersionExceptionPayDealThread thread = new UserVersionExceptionPayDealThread(UserVersionExceptionPayDealThread.RECHARGE_ORDER_EXCEPTION
		   				, outTradeNo,factMoneyValue,"");
		   		thread.start();
			} catch(Exception e){
				//异常不进行重试
				log.error(e.getMessage());
			}	
		} else {
			if(!tradeStatus.equals("SUCCESS")){
				log.info("金好支付处理错误支付结果："+tradeStatus);
			}
		}
		log.info("金好支付订单处理入账结果:{}", dealResult);
		//通知金好支付报文接收成功
	    out.println("SUCCESS");
		return;
	}else{
		log.info("MD5校验失败,报文返回MD5签名内容["+ sign +"],计算MD5签名内容[" + WaitSign + "]");
		//让第三方继续补发
	    out.println("ERROR");
		return;
	}
%>