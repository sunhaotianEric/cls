<%@page import="java.math.BigDecimal,com.team.lottery.service.ChargePayService,com.team.lottery.util.ApplicationContextUtil,com.team.lottery.util.StringUtils,com.team.lottery.vo.ChargePay"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@page import="com.team.lottery.util.ConstantUtil"%>
<%@page import="com.team.lottery.vo.User"%>
<%@page import="com.team.lottery.enums.EFundRechargeStatus"%>
<%@page import="com.team.lottery.enums.EFundPayType"%>
<%@page import="com.team.lottery.enums.EFundOperateType"%>
<%@page import="com.team.lottery.enums.EFundRefType"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@ page import="com.team.lottery.pay.util.Md5Util"%>
<%@page import="com.team.lottery.pay.model.YingTongBaoPay"%>
<%@page import="com.team.lottery.util.MakeOrderNum"%>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory"%>
<%@page import="com.team.lottery.service.RechargeOrderService"%>
<%@page import="com.team.lottery.vo.RechargeOrder"%>
<jsp:useBean id='md5Util' scope='request' class='com.team.lottery.pay.util.Md5Util'/>
<%  

    Logger log = LoggerFactory.getLogger(this.getClass());
    String path = request.getContextPath();
	String OrderMoney = request.getParameter("OrderMoney");//订单金额
	String payId = request.getParameter("PayId");//支付的银行接口
	String chargePayId = request.getParameter("chargePayId");//第三方ID号 
	String payType = request.getParameter("payType");//充值类型
    String orderMoneyYuan = OrderMoney;
    
    User currentUser = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
    if(currentUser == null){
        response.sendRedirect(path+"/gameBet/login.html");
    }
	
	Date currTime = new Date();
    //时间以yyyy-MM-dd HH:mm:ss的方式表示
    SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.CHINA);
    //时间以yyyyMMDDHHmmss的方式表示 
    SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss",Locale.CHINA);
    String webdate=new String(formatter1.format(currTime));
    String TradeDate=new String(formatter2.format(currTime));

	ChargePayService chargePayService = (ChargePayService)ApplicationContextUtil.getBean("chargePayService");
	RechargeOrderService rechargeWithDrawOrderService = (RechargeOrderService)ApplicationContextUtil.getBean("rechargeOrderService");
	ChargePay chargePay = chargePayService.getPayMemmberMsg(Long.valueOf(chargePayId));
	String  orderId = MakeOrderNum.makeOrderNum("CZ");
	log.info("用户名称: "+currentUser.getUserName()+" 用户 ID号: "+currentUser.getId()+" 生成订单号: "+orderId+" 订单金额: "+OrderMoney);
	
	if(payType.equals("ALIPAY")){
		 payId="1";
	}else if(payType.equals("WEIXIN")){
		 payId="2";
	}else if(payType.equals("QQPAY")){
		 payId="8";
	}
	
	/* if (!"".equals(OrderMoney)) 
	{	BigDecimal a;
		a = new BigDecimal(OrderMoney); //使用分进行提交
		OrderMoney=String.valueOf(a.setScale(2));
	}
	else{
		OrderMoney = ("0");
	} */
	String payUrl = chargePay.getPayUrl();//借贷混合
	String Address = chargePay.getAddress(); //最终迅汇宝地址
	log.info("payUrl: "+payUrl+" Address: "+Address);
	//赢通宝请求对象
	YingTongBaoPay yingTongBaoPay =new YingTongBaoPay();
	yingTongBaoPay.setMerchno(chargePay.getMemberId());
	yingTongBaoPay.setAmount(OrderMoney);
	yingTongBaoPay.setGoodsName("money");
	yingTongBaoPay.setPayType(payId);
	yingTongBaoPay.setNotifyUrl(chargePay.getReturnUrl());
	yingTongBaoPay.setSettleType("1");
	/* yingTongBaoPay.setReturnUrl(chargePay.getReturnUrl()); */
	yingTongBaoPay.setTraceno(orderId);
	//生成md5密钥签名
    String Md5key = chargePay.getSign();///////////md5密钥（KEY）
	String MARK = "&";
	String md5 =new String("amount="+yingTongBaoPay.getAmount()+MARK+"goodsName="+yingTongBaoPay.getGoodsName()+MARK+"merchno="+yingTongBaoPay.getMerchno()+MARK+"notifyUrl="+yingTongBaoPay.getNotifyUrl()+MARK+"payType="+yingTongBaoPay.getPayType()+MARK+"settleType="+yingTongBaoPay.getSettleType()+MARK+"traceno="+yingTongBaoPay.getTraceno()+MARK+Md5key);//MD5签名格式
	String Signature = md5Util.getMD5ofStr(md5);//计算MD5值
	log.info("md5原始串: "+md5);
	log.info("交易签名: "+Signature);
    //交易签名
    yingTongBaoPay.setSignature(Signature);
	log.info("交易报文: "+yingTongBaoPay);
    //冲值订单
	RechargeOrder rechargeOrder = new RechargeOrder();
	//用户ID
	rechargeOrder.setUserId(currentUser.getId());
	//快捷ID
	rechargeOrder.setChargePayId(chargePay.getId());
	//用户名称
	rechargeOrder.setUserName(currentUser.getUserName());
	//申请金额
	rechargeOrder.setApplyValue(new BigDecimal(orderMoneyYuan));
	//订单创建时间
	rechargeOrder.setCreatedDate(new Date());
	//充值类型
	rechargeOrder.setPayType(payType);
	//第三方充值类型
	rechargeOrder.setThirdPayType("YTBPAY");
	//操作描述
	rechargeOrder.setOperateDes(EFundPayType.valueOf(payType).getDescription());
	//流水号
	rechargeOrder.setSerialNumber(orderId);
	rechargeOrder.setPostscript("");
	rechargeOrder.setDealStatus(EFundRechargeStatus.PAYMENT.name());
	rechargeOrder.setBankName("");
	rechargeOrder.setSubbranchName("");
	rechargeOrder.setBankAccountName("");
	rechargeOrder.setBankCardNum("");
	rechargeOrder.setBankType("");
	rechargeOrder.setBizSystem(currentUser.getBizSystem());
	rechargeOrder.setRefType(EFundRefType.THIRDPAY.name());
	rechargeOrder.setFromType(currentUser.getLoginType());
	rechargeWithDrawOrderService.insertSelective(rechargeOrder);
 %>
 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值接口-提交信息处理</title>
</head>

<body onload="pay.submit()">
<form method="post" name="pay" id="pay" action="<%=payUrl%>">
<TABLE>
<TR>
	<TD>
	<input name='Address' type='hidden' value= "<%=Address%>"/>
	<input name='merchno' type='hidden' value= "<%=yingTongBaoPay.merchno%>"/>
	<input name='amount' type='hidden' value= "<%=yingTongBaoPay.amount%>"/>
	<input name='traceno' type='hidden' value= "<%=yingTongBaoPay.traceno%>"/>
	<input name='payType' type='hidden' value= "<%=yingTongBaoPay.payType%>"/>
	<input name='goodsName' type='hidden' value= "<%=yingTongBaoPay.goodsName%>"/>		
	<input name='notifyUrl' type='hidden' value= "<%=yingTongBaoPay.notifyUrl%>"/>
	<input name='remark' type='hidden' value= "<%=yingTongBaoPay.remark%>"/>
	<input name='settleType' type='hidden' value= "<%=yingTongBaoPay.settleType%>"/>			
	<input name='signature' type='hidden' value= "<%=yingTongBaoPay.signature%>" />

	</TD>
</TR>
</TABLE>
	
</form>	

</body>
</html>
