<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>    
<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>帮助中心</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<!-- css -->
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/help.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />

<script src="<%=path%>/front/assets/jquery/jquery.js"></script>
<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<!-- header js -->
<script type="text/javascript" src="<%=path%>/front/js/header.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/js/help/help.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>

<%
 String functionParam = request.getParameter("parm1");  //获取查询的商品ID
%>
<script type="text/javascript">
	helppage.param.functionParam = '<%=functionParam%>';
</script>
</head>
<body>
<jsp:include page="/front/include/header_user.jsp"></jsp:include>

<!-- 需要加载的图片 -->
 <div class="stance"></div>
<!-- m-banner -->
<div class="m-banner" style="background-image:url(<%=path%>/front/images/login/banner.jpg);">
    <div class="content" ><img src="<%=path%>/front/images/help/banner-title.png" /></div>
</div>
<!-- m-banner over -->

<!-- m-nav -->
<div class="m-nav container">
<div class="container">
	<ul class="nav">
    	<li  class="on" id="CJWT"><p class="title">常见问题</p><div class="bg"></div></li>
        <li id="XSRM"><p class="title">新手入门</p><div class="bg"></div></li>
        <li id="CZZS"><p class="title">彩种知识</p><div class="bg"></div></li>
        <li id="GCWT"><p class="title">购彩问题</p><div class="bg"></div></li>
        <li id="CZWT"><p class="title">充值问题</p><div class="bg"></div></li>
        <li id="TXWT"><p class="title">提现问题</p><div class="bg"></div></li>
        <li id="ZHAQ"><p class="title">账户安全</p><div class="bg"></div></li>
    </ul>
     <div class="btn" onclick="frontCommonPage.showCustomService()"><img src="<%=path%>/front/images/help/icon-custom.png" alt=""></div>
</div>
</div>
<!-- m-nav -->
<!-- main -->
<div class="main">
<div class="container">
	<!-- <p class="main-title">问题列表</p> -->
    
    <ul id="helpList">
    	<!-- main-child -->
    	<li class="main-child">
        	<div class="child-head"><div class="center">
            	<div class="child-icon"><img src="<%=path%>/front/images/help/i1.png" /></div>
                <div class="child-title">
                	<p class="title">目前平台可以投注哪些彩种？</p>
                    <div class="info">
                    	<p>重庆时时彩、江西时时彩、新疆时时彩、天津时时彩.[详情]...</p>
                    </div>
                </div>
                <div class="btn" onClick="main_child_click(this)">查看</div>
            </div></div>
            <div class="child-content"><div class="center">
            	<p><img src="<%=path%>/front/images/help/1.jpg" /></p>
                <p><br /></p>
                <p style="font-size:16px;color:#000000;">第一步：点击首页“充值”按钮，选择"工行"输入金额，点击“下一步”</p>
                 <p><br /></p>
                <p style="font-size:16px;color:#000000;">第二步：获取充值信息</p>
                 <p><br /></p>
                 <p><br /></p>
                 <div class="btn" onClick="main_child_hide(this)"><div class="title">关闭收起</div><div class="bg"></div></div>
            </div></div>
        </li>
        <!-- main-child over -->
        
        <!-- main-child -->
<%--     	<li class="main-child">
        	<div class="child-head"><div class="center">
            	<div class="child-icon"><img src="<%=path%>/front/images/help/i1.png" /></div>
                <div class="child-title">
                	<p class="title">投注后撤单，算不算有效投注？</p>
                    <div class="info">
                    	<p>提款额度是根据有效投注额进行计算的，因此撤单的投注金额不会列入累计</p>
                    </div>
                </div>
                <div class="btn" onClick="main_child_click(this)">查看</div>
            </div></div>
            <div class="child-content"><div class="center">
            	<p><img src="<%=path%>/front/images/help/1.jpg" /></p>
                <p><br /></p>
                <p style="font-size:16px;color:#000000;">第一步：点击首页“充值”按钮，选择"工行"输入金额，点击“下一步”</p>
                 <p><br /></p>
                <p style="font-size:16px;color:#000000;">第二步：获取充值信息</p>
                 <p><br /></p>
                 <p><br /></p>
                 <div class="btn" onClick="main_child_hide(this)"><div class="title">关闭收起</div><div class="bg"></div></div>
            </div></div>
        </li>
        <!-- main-child over -->
        
        <!-- main-child -->
    	<li class="main-child">
        	<div class="child-head"><div class="center">
            	<div class="child-icon"><img src="<%=path%>/front/images/help/i2.png" /></div>
                <div class="child-title">
                	<p class="title">新手入门问题图标？</p>
                    <div class="info">
                    	<p>重庆时时彩、江西时时彩、新疆时时彩、天津时时彩.[详情]...</p>
                    </div>
                </div>
                <div class="btn" onClick="main_child_click(this)">查看</div>
            </div></div>
            <div class="child-content"><div class="center">
            	<p><img src="<%=path%>/front/images/help/1.jpg" /></p>
                <p><br /></p>
                <p style="font-size:16px;color:#000000;">第一步：点击首页“充值”按钮，选择"工行"输入金额，点击“下一步”</p>
                 <p><br /></p>
                <p style="font-size:16px;color:#000000;">第二步：获取充值信息</p>
                 <p><br /></p>
                 <p><br /></p>
                 <div class="btn" onClick="main_child_hide(this)"><div class="title">关闭收起</div><div class="bg"></div></div>
            </div></div>
        </li>
        <!-- main-child over -->
        
        <!-- main-child -->
    	<li class="main-child">
        	<div class="child-head"><div class="center">
            	<div class="child-icon"><img src="<%=path%>/front/images/help/i3.png" /></div>
                <div class="child-title">
                	<p class="title">充值类问题标志？</p>
                    <div class="info">
                    	<p>提款额度是根据有效投注额进行计算的，因此撤单的投注金额不会列入累计.</p>
                    </div>
                </div>
                <div class="btn" onClick="main_child_click(this)">查看</div>
            </div></div>
            <div class="child-content"><div class="center">
            	<p><img src="<%=path%>/front/images/help/1.jpg" /></p>
                <p><br /></p>
                <p style="font-size:16px;color:#000000;">第一步：点击首页“充值”按钮，选择"工行"输入金额，点击“下一步”</p>
                 <p><br /></p>
                <p style="font-size:16px;color:#000000;">第二步：获取充值信息</p>
                 <p><br /></p>
                 <p><br /></p>
                 <div class="btn" onClick="main_child_hide(this)"><div class="title">关闭收起</div><div class="bg"></div></div>
            </div></div>
        </li>
        <!-- main-child over -->
        
        <!-- main-child -->
    	<li class="main-child">
        	<div class="child-head"><div class="center">
            	<div class="child-icon"><img src="<%=path%>/front/images/help/i3.png" /></div>
                <div class="child-title">
                	<p class="title">充值类问题标志？</p>
                    <div class="info">
                    	<p>提款额度是根据有效投注额进行计算的，因此撤单的投注金额不会列入累计.</p>
                    </div>
                </div>
                <div class="btn" onClick="main_child_click(this)">查看</div>
            </div></div>
            <div class="child-content"><div class="center">
            	<p><img src="<%=path%>/front/images/help/1.jpg" /></p>
                <p><br /></p>
                <p style="font-size:16px;color:#000000;">第一步：点击首页“充值”按钮，选择"工行"输入金额，点击“下一步”</p>
                 <p><br /></p>
                <p style="font-size:16px;color:#000000;">第二步：获取充值信息</p>
                 <p><br /></p>
                 <p><br /></p>
                 <div class="btn" onClick="main_child_hide(this)"><div class="title">关闭收起</div><div class="bg"></div></div>
            </div></div>
        </li>
        <!-- main-child over -->
        <!-- main-child -->
    	<li class="main-child">
        	<div class="child-head"><div class="center">
            	<div class="child-icon"><img src="<%=path%>/front/images/help/i1.png" /></div>
                <div class="child-title">
                	<p class="title">目前平台可以投注哪些彩种？</p>
                    <div class="info">
                    	<p>重庆时时彩、江西时时彩、新疆时时彩、天津时时彩.[详情]...</p>
                    </div>
                </div>
               <div class="btn" onClick="main_child_click(this)">查看</div>
            </div></div>
            <div class="child-content"><div class="center">
            	<p><img src="<%=path%>/front/images/help/1.jpg" /></p>
                <p><br /></p>
                <p style="font-size:16px;color:#000000;">第一步：点击首页“充值”按钮，选择"工行"输入金额，点击“下一步”</p>
                 <p><br /></p>
                <p style="font-size:16px;color:#000000;">第二步：获取充值信息</p>
                 <p><br /></p>
                 <p><br /></p>
                 <div class="btn" onClick="main_child_hide(this)"><div class="title">关闭收起</div><div class="bg"></div></div>
            </div></div>
        </li>
        <!-- main-child over -->
        
        <!-- main-child -->
    	<li class="main-child">
        	<div class="child-head"><div class="center">
            	<div class="child-icon"><img src="<%=path%>/front/images/help/i1.png" /></div>
                <div class="child-title">
                	<p class="title">投注后撤单，算不算有效投注？</p>
                    <div class="info">
                    	<p>提款额度是根据有效投注额进行计算的，因此撤单的投注金额不会列入累计</p>
                    </div>
                </div>
                <div class="btn" onClick="main_child_click(this)">查看</div>
            </div></div>
            <div class="child-content"><div class="center">
            	<p><img src="<%=path%>/front/images/help/1.jpg" /></p>
                <p><br /></p>
                <p style="font-size:16px;color:#000000;">第一步：点击首页“充值”按钮，选择"工行"输入金额，点击“下一步”</p>
                 <p><br /></p>
                <p style="font-size:16px;color:#000000;">第二步：获取充值信息</p>
                 <p><br /></p>
                 <p><br /></p>
                 <div class="btn" onClick="main_child_hide(this)"><div class="title" >关闭收起</div><div class="bg"></div></div>
            </div></div>
        </li>
        <!-- main-child over -->
        
        <!-- main-child -->
    	<li class="main-child">
        	<div class="child-head"><div class="center">
            	<div class="child-icon"><img src="<%=path%>/front/images/help/i2.png" /></div>
                <div class="child-title">
                	<p class="title">新手入门问题图标？</p>
                    <div class="info">
                    	<p>重庆时时彩、江西时时彩、新疆时时彩、天津时时彩.[详情]...</p>
                    </div>
                </div>
                <div class="btn" onClick="main_child_click(this)">查看</div>
            </div></div>
            <div class="child-content"><div class="center">
            	<p><img src="<%=path%>/front/images/help/1.jpg" /></p>
                <p><br /></p>
                <p style="font-size:16px;color:#000000;">第一步：点击首页“充值”按钮，选择"工行"输入金额，点击“下一步”</p>
                 <p><br /></p>
                <p style="font-size:16px;color:#000000;">第二步：获取充值信息</p>
                 <p><br /></p>
                 <p><br /></p>
                 <div class="btn" onClick="main_child_hide(this)"><div class="title" >关闭收起</div><div class="bg"></div></div>
            </div></div>
        </li> --%>
        <!-- main-child over -->
    </ul>
</div>
</div>
<!-- main over -->

<jsp:include page="/front/include/chat_common.jsp"></jsp:include>
<!--footer-->
<jsp:include page="/front/include/footer.jsp"></jsp:include>
</body>
</html>