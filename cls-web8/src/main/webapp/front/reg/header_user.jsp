<%@ page language="java" contentType="text/html; charset=UTF-8"
    import="com.team.lottery.vo.LotterySet,com.team.lottery.util.ConstantUtil,com.team.lottery.vo.User,
    com.team.lottery.system.BizSystemConfigVO,com.team.lottery.system.BizSystemConfigManager,
    com.team.lottery.cache.ClsCacheManager,com.team.lottery.vo.BizSystemInfo"
	pageEncoding="UTF-8"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
    String serverName=request.getServerName();
    
    String bizSystem=ClsCacheManager.getBizSystemByDomainUrl(serverName);
    User user = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	BizSystemInfo bizSystemInfo=ClsCacheManager.getBizSystemInfo(bizSystem);
	LotterySet lotterySet=new LotterySet();
	 if(bizSystemInfo==null){
	    	bizSystemInfo = new BizSystemInfo();
	    }
%>

<!-- header -->
<div class="header">
	<!-- head -->
	<div class="head">
	<div class="container">
    	<p class="gmt"><img src="<%=path%>/front/images/GMT.png" /><span></span></p>
    	<% if(user!=null){%>
    	 <ul class="nav">
        	<a href="javascript:void(0);"><li><p id="username">你好，---</p></li></a>
        	
        	<a href="<%=path %>/gameBet/recharge.html"><li><p>充值</p></li></a>
            <a href="<%=path %>/gameBet/withdraw.html"><li><p>提现</p></li></a>
            
            <li id="msgbox">
            	<a href="<%=path %>/gameBet/message/inbox.html"><div class="mun" id="msgcount">0</div></a>
            	<div class="list mun-list" >
                <div class="m-title">我的未读消息(<span id="msgcount2">0</span>) <a href="<%=path %>/gameBet/message/inbox.html">更多 >></a></div>
                <div class="center" id="msgbd">
                	<!-- <p class="m-msg">暂未收到新消息</p>
                	<a href="user_msg2.html"><div class="list-child"><p>信息1</p></div></a>
                    <a href="user_msg2.html"><div class="list-child"><p>信息2</p></div></a> -->
                </div></div>
            </li>
            
            <li>	
            	<p id="hiddBall" style="display: none;">
            		<span>余额：</span><span id="spanBall">0000.000</span> <img src="<%=path%>/front/images/refreshBall.png" id='refreshBall'>
            	</p>
            	<p id="hiddenBall"  style="display: block;">
            		余额已隐藏 <a href="javascript:void(0)" id="showAllBall">显示</a>
            	</p>
            </li>
           
            <li><p>我的账户<img src="<%=path%>/front/images/nav-pointer.png" /></p>
            	<div class="list"><div class="center">
                    <a href="<%=path %>/gameBet/accountCenter.html"><div class="list-child">账户总览</div></a>
                	<a href="<%=path %>/gameBet/user_center.html"><div class="list-child">会员中心</div></a>
                    <a href="<%=path %>/gameBet/order.html"><div class="list-child">投注历史</div></a>
                    <a href="<%=path %>/gameBet/moneyDetail.html"><div class="list-child">账户明细</div></a>
                    <%
					  if(!user.getDailiLevel().equals("REGULARMEMBERS")){  //如果是代理会员,才能显示
					%>
                    <a href="<%=path %>/gameBet/agentCenter/userList.html"><div class="list-child">代理中心</div></a>
					<%
					  }
					%>
					<a href="javascript:void(0)" class="exit"><div class="list-child">退出登录</div></a>
                </div></div>
            </li>
        </ul>
    	<%}else{ %>
    	 <ul class="nav">
        	<a href="<%=path%>/gameBet/index.html"><li><p>登录</p></li></a>
            <span>|</span>
            <a href="#"><li><p>免费开户</p></li></a>
        </ul>
       <%}%>
    </div>
    </div>
    <!-- head over -->
    <!-- foot -->
    <div class="foot">
    <div class="container">
	   	<a href="<%=path%>/gameBet/index.html"><div class="logo"><img id="logoImg" src="<%=bizSystemInfo.getHeadLogoUrl()%>"></div></a>
    	<%-- <a href="<%=path %>/gameBet/index.html"><div class="logo"><img src="<%=path%>/front/images/logo-icon.png" /></div></a>
        <a href="<%=path %>/gameBet/index.html"><div class="logo logo-title"><p>国际娱乐</p></div></a> --%>
        <ul class="nav" id="navChoose">
        	<a href="<%=path %>/gameBet/index.html"><li class="nav-child on"><p class="title">首页</p></li></a>
        	<li class="nav-child"><a href="<%=path%>/gameBet/hall.html"><div class="title">彩票大厅</div></a>
     
            <a href="<%=path%>/gameBet/wap.html"><li class="nav-child"><p class="title">手机购彩</p></li></a>
            <a href="<%=path%>/gameBet/help.html?parm1=CJWT"><li class="nav-child"><p class="title">帮助中心</p></li></a>
            <a href="<%=path%>/gameBet/activity.html"><li class="nav-child"><p class="title">活动专区</p></li></a>
        </ul>
    </div>
    </div>
    <!-- foot over -->
    <!-- shortcut-logo over -->
</div>
<!-- header over -->
<script type="text/javascript">
//设置首页平均充值时间，首页平均提现时间
var recharge_second= '<%=bizSystemInfo.getRechargeSecond()!=null?bizSystemInfo.getRechargeSecond():5%>';
var withdraw_second= '<%=bizSystemInfo.getWithdrawSecond()!=null?bizSystemInfo.getWithdrawSecond():7%>';

</script>
