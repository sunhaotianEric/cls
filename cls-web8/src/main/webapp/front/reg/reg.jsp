<%@page import="com.team.lottery.util.ApplicationContextUtil"%>
<%@page import="com.team.lottery.vo.DomainInvitationCode"%>
<%@page import="com.team.lottery.service.DomainInvitationCodeService"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    import="java.math.BigDecimal,java.io.UnsupportedEncodingException,com.team.lottery.cache.ClsCacheManager,org.apache.commons.lang.StringUtils,com.team.lottery.vo.BizSystem,com.team.lottery.system.BizSystemConfigVO,net.sf.json.JSONObject"
    import="com.team.lottery.cache.ClsCacheManager,com.team.lottery.service.UserRegisterService,com.team.lottery.extvo.UserRegisterVo,com.team.lottery.util.ConstantUtil,com.team.lottery.util.ApplicationContextUtil"
    import="com.team.lottery.system.BizSystemConfigManager,com.team.lottery.system.BizSystemConfigVO,com.team.lottery.vo.BizSystemDomain"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String serverName=request.getServerName();
	String bizSystem=ClsCacheManager.getBizSystemByDomainUrl(serverName);
	BizSystem bizSystemConfig=ClsCacheManager.getBizSystemByCode(bizSystem);
	if(bizSystemConfig==null){
		bizSystemConfig = new BizSystem();
	}
	request.setAttribute("bizSystem", bizSystemConfig);


%>        
<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<c:if test="${bizSystem.bizSystemName != null}">
  <title>${bizSystem.bizSystemName}用户注册</title>
</c:if>
<c:if test="${bizSystem.bizSystemName == null}">
 <title>用户注册</title>
</c:if>
<meta name="keywords" content=""/>
<meta name="description" content=""/>

<!-- css -->
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>"/>
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/index.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/indexAn.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/index-enroll.css?v=<%=SystemConfigConstant.pcWebRsVersion%>"/>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<script type="text/javascript" src="<%=path%>/resources/jquery/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>"/>
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script type="text/javascript" src="<%=path%>/front/js/base64.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script type="text/javascript" src="<%=path%>/front/js/aes.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script type="text/javascript" src="<%=path%>/front/js/crypto-js.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/js/reg/reg.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script type="text/javascript">
  var contextPath = '<%=path %>';
</script>
<%
	//获取request中的邀请码
	String registerCode ="";
	boolean isShowRegisterCode = true;
	String context = "请输入邀请码";
	String requestRegisterCode=request.getParameter("code");
	if (bizSystem == null) {
		response.sendRedirect(path + "/gameBet/reg_error.html");
		return;
	}
	BizSystemConfigVO bizSystemConfigVO = BizSystemConfigManager.getBizSystemConfig(bizSystem);
	JSONObject obj = JSONObject.fromObject(bizSystemConfigVO);
	String strJson = obj.toString();
	// 获取当前是否开启了开放注册!
	Integer regOpen = bizSystemConfigVO.getRegOpen();
	DomainInvitationCode domainInvitationCode = new DomainInvitationCode();
	domainInvitationCode.setBizSystem(bizSystem);
	domainInvitationCode.setDomainUrl(serverName);
	domainInvitationCode.setEnable(1);
	//查询相关的域名邀请码
	DomainInvitationCodeService domainInvitationCodeService = ApplicationContextUtil.getBean("domainInvitationCodeService");
	DomainInvitationCode code=domainInvitationCodeService.selectDomainInvitationCode(domainInvitationCode);
	
	// 是否开放注册.
	if(regOpen == 1){
		// 首先设置默认邀请码.
		registerCode = "";
		isShowRegisterCode = true;
		context = "请输入邀请码(非必填)";
		// 然后设置域名邀请码.
		if(code != null){
			registerCode = code.getInvitationCode();
			isShowRegisterCode = false;
			context = "请输入邀请码";
		}
		// 然后判断是否有Code邀请码.
		if(StringUtils.isNotBlank(requestRegisterCode)){
			registerCode = requestRegisterCode;
			isShowRegisterCode = true;
			context = "请输入邀请码";
		}
	
	} else {
		// 没有开放注册的话,就先查看是否有域名邀请码.
		if(code != null){
			registerCode = code.getInvitationCode();
			isShowRegisterCode = false;
			context = "请输入邀请码";
		}
		// 查看域名邀请码.
		if(StringUtils.isNotBlank(requestRegisterCode)){
			registerCode = requestRegisterCode;
			isShowRegisterCode = true;
			context = "请输入邀请码";
		}
	}
	Integer regShowPhone = bizSystemConfigVO.getRegShowPhone() == null ? 0 : bizSystemConfigVO.getRegShowPhone();
	Integer regRequiredPhone = bizSystemConfigVO.getRegRequiredPhone() == null ? 0 : bizSystemConfigVO.getRegRequiredPhone();
	Integer regShowEmail = bizSystemConfigVO.getRegShowEmail() == null ? 0 : bizSystemConfigVO.getRegShowEmail();
	Integer regRequiredEmail = bizSystemConfigVO.getRegRequiredEmail() == null ? 0 : bizSystemConfigVO.getRegRequiredEmail();
	Integer regShowQq = bizSystemConfigVO.getRegShowQq() == null ? 0 : bizSystemConfigVO.getRegShowQq();
	Integer regRequiredQq = bizSystemConfigVO.getRegRequiredQq() == null ? 0 : bizSystemConfigVO.getRegRequiredQq();
	Integer regShowTrueName = bizSystemConfigVO.getRegShowTrueName() == null ? 0 : bizSystemConfigVO.getRegShowTrueName();
	Integer regShowSms = bizSystemConfigVO.getRegShowSms() == null ? 0 : bizSystemConfigVO.getRegShowSms();
	Integer regRequiredTrueName = bizSystemConfigVO.getRegRequiredTrueName() == null ? 0 : bizSystemConfigVO.getRegRequiredTrueName();
%>
<script type="text/javascript">
regPage.param.invitationCode  = '<%=registerCode %>';
regPage.param.regShowPhone='<%=regShowPhone %>';
regPage.param.regRequiredPhone='<%=regRequiredPhone %>';
regPage.param.regShowEmail='<%=regShowEmail %>';
regPage.param.regRequiredEmail='<%=regRequiredEmail %>';
regPage.param.regShowQq='<%=regShowQq %>';
regPage.param.regRequiredQq='<%=regRequiredQq %>';
regPage.param.regShowSms='<%=regShowSms %>';
regPage.param.bizSystemConfigVO=<%=strJson%>;
</script>
</head>
<body>
<jsp:include page="/front/include/header_user.jsp"></jsp:include>
<!-- header over -->
<div class="stance"></div>

<!-- m-banner -->
<div class="m-banner" style="background-image:url(<%=path%>/front/images/login/banner.jpg);">
	<div class="content" >
		<img src="<%=path%>/front/images/enroll/banner-title.png" />
    <a href="<%=path%>/gameBet/login.html">已有账号?前去登录</a>
		<img class="banner-pointer" src="<%=path%>/front/images/login/banner-pointer.png" alt="">
	</div>
</div>
<!-- m-banner over -->
<!-- main -->
<div class="main"><div class="container">
	
    <div class="main-content">
        <img class="bg" style="right:12px;top:98px;width: 500px;" src="<%=path%>/front/images/enroll/img.jpg" alt="">
		 <!-- 如果request邀请码和session邀请码不为空的时候就弹出邀请码的框 -->         
         <div class="enroll-line"  id="invitationCode_line"  
         	<% if(isShowRegisterCode){ %> 
         		style="display: block;"
         	<% } else {%> 
         		style="display: none;"
         	 <%} %>
         	>
            <div class="line-title">邀请码：</div>
            <div class="line-content">
	        <% if(StringUtils.isNotBlank(registerCode)){ %>
	            <input type="text" class="inputText" id="invitationCode" value="<%=registerCode%>" placeholder="<%=context%>">
	        <% }else{ %>
	        	<input type="text" class="inputText" id="invitationCode" placeholder="<%=context%>">
	        <%} %>
	        </div>
            <!-- <p class="line-msg">邀请码不正确</p> -->
        </div>
        <div class="enroll-line" id="username_line">
            <div class="line-title">账号：</div>
            <div class="line-content"><input type="text" class="inputText" id="username">
            <input type="hidden" class="inputText" id="bizSystem" value="<%=bizSystem%>">
            </div>
            <p class="line-msg">账号由4~10位字母或数字，首位为字母</p>
        </div>
        <div class="enroll-line" id="password_line">
            <div class="line-title">设置密码：</div>
            <div class="line-content"><input type="password" class="inputText" id="password"></div>
            <p class="line-msg">密码由6-15位字符组成，区分大小写</p>
        </div>
        <div class="enroll-line" id="password2_line">
            <div class="line-title">确认密码：</div>
            <div class="line-content"><input type="password" class="inputText" id="password2"></div>
            <p class="line-msg">请再重新输入一遍密码</p>
        </div>
        <% if(regShowPhone==1){ %>
        <div class="enroll-line" id="mobile_line">
            <div class="line-title">手机号码：</div>
            <div class="line-content"><input type="text" class="inputText" id="mobile">
            </div>
            <% if(regShowSms == 1){ %>
            <div class="line-mobile-verifycode">
            	<input type="submit" class="line-mobile-verifycode-submit" value="发送" />
            	<span class="mobile-verify-time">发送验证码(<em class="countdown">60</em> s)</span>
            </div>
            <%} %>
            <p class="line-msg">手机号码必须为11位长度的数字</p>
        </div>
        <% } %>
        <% if(regShowPhone==1 && regShowSms == 1){ %>
        <div class="enroll-line" id="mobile_verifycode_line">
            <div class="line-title">短信验证码：</div>
            <div class="line-content w100"><input type="text" class="inputText" id="mobile-verifycode">
            </div>
            <p class="line-msg">短信验证码不为空!</p>
        </div>
        <%} %>
        <% if(regShowQq==1){ %>
        <div class="enroll-line" id="qq_line">
            <div class="line-title">联系QQ：</div>
            <div class="line-content"><input type="text" class="inputText" id="qq">
            </div>
            <p class="line-msg">QQ号码位数为5-11位数字</p>
        </div>
        <% } %>
        <% if(regShowEmail==1){ %>
        <div class="enroll-line" id="email_line">
            <div class="line-title">Email：</div>
            <div class="line-content"><input type="text" class="inputText" id="email">
            </div>
            <p class="line-msg">邮箱格式为xxx.@xxx.xxx</p>
        </div>
        <% } %>
        <% if(regShowTrueName==1){ %>
        <div class="enroll-line" id="trueName_line">
            <div class="line-title">真实姓名:</div>
            <div class="line-content"><input type="text" class="inputText" id="trueName">
            </div>
            <p class="line-msg">请填写您的真实姓名</p>
        </div>
        <% } %>

        <div class="enroll-line" id="verifycode_line">
            <div class="line-title">验证码：</div>
            <div class="line-content">
            	<input type="text" class="inputText" id='verifycode'>
            	<img class="code" style="width: 90px;height: 52px;" alt="验证码" title="点击更换" id="CheckCode" onclick="checkCodeRefresh()" src="<%=path %>/tools/getRandomCodePic">
            </div>
            <p class="line-msg">验证码不正确</p>
        </div>
         <%-- <div class="enroll-line" id="verifycode_line">
            <div class="line-title">验证码：</div>
            <div class="line-content"><input type="text" class="inputText" style="height:44px" placeholder="请输入验证码" id="verifyCode"/>
            <img class="code" alt="验证码" id="checkCodeRefresh" title="点击更换" style="height:33px" onclick="checkCodeRefresh()" src="<%=path%>/tools/getRandomCodePic">
            </div>
        </div> --%>
        
        <div class="enroll-line" id="age18_line">
            <div class="line-title"></div>
            <div class="line-content no"><input type="checkbox" class="inputCheck"><span>我已经年满18岁</span></div>
             <p class="line-msg" id="age18_tip">是否年满18周岁！</p>
        </div>
        <div class="enroll-line" id="regErrorMsg_line">
            <p class="msg" id="regErrorMsg" style="color:red;position:static;margin-top: 12px;"></p>
        </div>
        <div class="enroll-line">
            <div class="line-title"></div>
            <div class="line-content no"><input type="button" class="inputButton" id='regButton' onclick="regPage.register();" value="注册" /></div>
        </div>
    </div>


<jsp:include page="/front/include/advantageShow.jsp"></jsp:include> 


</div></div>
<!-- main over -->
<!-- fixed -->
<div class="fixed">
	<div class="child-top" onClick="base.urlhref({'element':'body'})"><div class="center"><img src="<%=path%>/front/images/f1.png" /></div></div>
    <div class="child-custom" onClick="online_show()"><div class="center"><img src="<%=path%>/front/images/f2.png" /></div></div>
    <div class="child" onClick="online_show()"><div class="center"><img src="<%=path%>/front/images/f3.png" /></div></div>
    <div class="child" onClick="online_show()"><div class="center"><img src="<%=path%>/front/images/f4.png" /></div></div>
    <div class="child"><div class="center"><img src="<%=path%>/front/images/f5.png" /></div><div class="msg msg1"><p>客服电话：00-0877-58889</p><img class="pointer" src="<%=path%>/front/images/fix_pointer.png" /></div></div>
    <div class="child no"><div class="center"><img src="<%=path%>/front/images/f6.png" /></div><div class="msg msg2"><p><img src="<%=path%>/front/images/code.jpg" /></p><img class="pointer" src="<%=path%>/front/images/fix_pointer.png" /></div></div>
</div>
<!-- fixed over -->

<%-- <!-- footer -->
<div class="footer">
    
    <div class="foot">
    <div class="container">
    	<div class="copyright"><span>Copyright & 2015 www.jinying.com   Carparintcn.AII Rtghis Rescrvcd.</span></div>
        
        <ul class="nav">
        	<a href="#"><li><img src="<%=path%>/front/images/foot_pointer.png"><span>玩法介绍</span></li></a>
            <a href="#"><li><img src="<%=path%>/front/images/foot_pointer.png"><span>如何充值</span></li></a>
            <a href="#"><li><img src="<%=path%>/front/images/foot_pointer.png"><span>提现须知</span></li></a>
            <a href="#"><li><img src="<%=path%>/front/images/foot_pointer.png"><span>添加收藏</span></li></a>
        </ul>
    </div>
    </div>
</div> --%>
<jsp:include page="/front/include/footer.jsp"></jsp:include>
  <%--   <div class="enroll">
    	<div class="enroll-top"><img src="<%=path%>/front/images/index/nav-top.png" /></div>
        <div class="enroll-container">
        	<div class="logo"><img src="<%=path%>/front/images/enroll/logo.png" /></div>
            <div class="line">
            	<img src="<%=path%>/front/images/login/i1.png" />
                <input type="text" class="inputText" id="username" placeholder="账号" />
            </div>
                <p class="msg" id="username_tip" style="display: none">账号由4~10位字母或数字，首位为字母</p>
            <div class="line">
            	<img src="<%=path%>/front/images/login/i2.png" />
                <input type="password" class="inputText" id="password" maxlength="15" placeholder="密码" />
            </div>
            <p class="msg" id="password_tip" style="display: none">密码由6-15位字符组成，区分大小写</p>
            <div class="line">
            	<img src="<%=path%>/front/images/login/i2.png" />
                <input type="password" class="inputText" id="password2" maxlength="15"  placeholder="确认密码" />
            </div>
            <p class="msg" id="password2_tip" style="display: none">请再重新输入一遍密码</p>
            <div class="line">
                <img src="<%=path%>/front/images/login/i1.png" />
                <input type="text" class="inputText" id="mobile"  maxlength="16" placeholder="手机号码" />
            </div>
            <p class="msg" id="mobile_tip" style="display: none">手机号码由</p>
            <div class="line">
                <img src="<%=path%>/front/images/login/i1.png" />
                <input type="text" class="inputText"  id="qq" maxlength="16"  placeholder="QQ" />
            </div>
             <p class="msg" id="qq_tip" style="display: none">QQ号码由</p>
            <div class="line">
            	<img src="<%=path%>/front/images/login/i3.png" />
                <input type="text" class="inputText" id='verifycode'  maxlength="4" placeholder="验证码" />
                <img class="code" alt="验证码" title="点击更换" id="CheckCode" onclick="checkCodeRefresh()" src="<%=path %>/tools/getRandomCodePic">
            </div>
            <p class="msg" id="verifycode_tip" style="display: none">请输入正确的验证码</p>
            
             <div class="button button-blue" id='regButton'>注册</div>
           
            <p class="msg" id="regErrorMsg" style="color:red;position:static;margin-top: 12px;"></p>
            
            <p class="t1">已有账号？<a href="<%=path %>/gameBet/login.html">立即登录</a></p>
        </div>
    </div> --%>
<%--     <img class="shadow" src="<%=path%>/front/images/enroll/shadow.png" />
</div>
<p class="main-copyright">COPYRIGHT & 2015 <a href="jinying.com">JINYING.COM</a>   CARPARINTCN.AII RTGHIS RESCRVCD.</p>
<!-- main over -->
<!--footer-->

<jsp:include page="/front/include/chat_login_common.jsp"></jsp:include>
<div class="reveal-modal-message" id="reveal_modal_message" style="display:none">
	<div class='field-result'>
		<span class='result-success'><strong>注册成功</strong>正在进入登陆页面...</span>
	</div>
</div>
<div class="reveal-modal-bg" id="reveal_modal_bg" style="display:none"></div> --%>
<script type="text/javascript">
		function checkCodeRefresh() {
			$('#CheckCode').attr('src', contextPath + "/tools/getRandomCodePic?time=" + Math.random());
		} 
</script>
<script type="text/javascript">
/* $(".process").each(function(i,n){
	var second=parseInt($(this).find(".process-second span").html()),
		proc=parseInt($(this).find(".process-ing").attr("data-process")),
		dirct=1,
		procing=0,
		seconding=0.00,
	interval=setInterval(function(e,second,proc){
		seconding=second*procing*0.01;
		seconding=seconding.toFixed(2);
		seconding=seconding.replace(".","'");
		e.find(".process-ing").css("width",procing+"%");
		e.find(".process-second span").html(seconding);
		if(procing>=100){
			dirct=-1;
		}
		procing+=dirct;
		if(procing<=proc&&dirct==-1)clearInterval(interval)
	},14,$(this),second,proc);
}); */



/**
 * 服务体验效果JS
 * @param id ‘#id’
 * @param process_second 时间 
 * @param process_maxSecond 最大值 ，一般设为15秒 ,
 */
function showProcess(id,process_second,process_maxSecond)
{
//	$(".process").each(function(i,n){
//		var second=parseInt($(this).find(".process-second span").html()),
//			proc=parseInt($(this).find(".process-ing").attr("data-process")),
	   
	
		var maxSecond=parseInt(process_maxSecond),
   		    proc=parseInt(process_second),
			dirct=1,
			procing=0,
		interval=setInterval(function(e,proc){
			percent = procing/maxSecond*100; 
			percent = percent.toFixed(0);
			$(id).find(".process-ing").css("width",percent+"%");
			$(id).find(".process-second span").html(procing-1);
			if(procing>=maxSecond){
				dirct=-1;
			}
			procing+=dirct;
			if(procing<=proc&&dirct==-1)clearInterval(interval)
		},500/process_maxSecond,$(this),proc);
	//});	
}

//服务体验,recharge_second,withdraw_second在head_user.jsp那边
showProcess("#czdz_process",recharge_second,15);
showProcess("#txdz_process",withdraw_second,15);
</script>
</body>  
</html>