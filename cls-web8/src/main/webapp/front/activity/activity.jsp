<%@ page language="java" contentType="text/html; charset=UTF-8"
    import="com.team.lottery.system.BizSystemConfigManager,
    com.team.lottery.cache.ClsCacheManager,com.team.lottery.vo.BizSystem"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
    String serverName=request.getServerName();
    String bizSystemCode=ClsCacheManager.getBizSystemByDomainUrl(serverName);
%>    
<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<title>优惠活动</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>

<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<!-- css -->
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.an.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/activity.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script type="text/javascript">
var  bizSystemCode = '<%=bizSystemCode%>';
</script>
<!-- header js -->
<script type="text/javascript" src="<%=path%>/front/js/header.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/js/activity/activity.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>

</head>
<body>
<jsp:include page="/front/include/header_user.jsp"></jsp:include>

<div class="stance"></div>
<!-- m-banner -->
<div class="m-banner" style="background-image:url(<%=path%>/front/images/login/banner.jpg);">
	<div class="content" ><img src="<%=path%>/front/images/activity/banner-title.png" /></div>
</div>
<!-- m-banner over -->

<!-- main-->
<div class="main">
	<div class="container" id="activityList">
	<!-- main-child -->
<%--<div class="main-child">
	      <div class="head">
	          <div class="img" style="background-image:url('images/activity/1.jpg');"></div>
            <div class="infos">
                <h2 class="title">手机客户端隆重上线</h2>
                <p class="time">发布时间：2017-05-12  12:58</p>

                <p class="ac-time">活动日期：2016-01-01 至 2016-03-31</p>
                <div class="btn red-btn">活动进行中</div>
            </div>
            <div class="pointer"><img src="images/activity/pointer.png" alt=""></div>
	      </div>
        <div class="content">
            <h2 class="font-red">诚信为本</h2>
            <p>作为专业的彩票娱乐运营商，我们承诺，为每一位客户提供最安全、最公平的购彩娱乐，以及全方位的服务。彩票33所有线上彩种共同的优点 :提供手机APP下载、结合PC端及WAP端，突破空间与时间的束缚，全天24小时体验购彩的乐趣，游戏结果公平、公正、公开！全球彩民都赞不绝口！</p>
            <h2>多样化彩种</h2>
            <p>作为专业的彩票娱乐运营商，我们承诺，为每一位客户提供最安全、最公平的购彩娱乐，以及全方位的服务。彩票33所有线上彩种共同的优点 :提供手机APP下载、结合PC端及WAP端，突破空间与时间的束缚，全天24小时体验购彩的乐趣，游戏结果公平、公正、公开！全球彩民都赞不绝口！</p>
            <p>我们为客户提供最专业的娱乐服务：</p>
            <p>1、365天×24小时专业的客户服务，全天候处理会员出入款的相关事宜，严格训练的客服团队，以专业、亲切的态度解，让每位玩家有宾至如归的感觉!</p>
        </div>

	  </div>
        <div class="child-content">
        	<div class="center" >
        	<!-- flags -->
          <div class="flags">
            	<div class="flag">
                	<div class="flag-head"><p>星期一</p></div>
                    <div class="flag-content">
                    	<p>消费满 <span>1888</span> 元</p>
                        <p>签到送钱 <span>8</span> 元</p>
                        <div class="btn"><div class="title">签到</div><div class="bg"></div></div>
                    </div>
                </div>
          <div class="flag">
                	<div class="flag-head"><p>星期二</p></div>
                    <div class="flag-content">
                    	<p>消费满 <span>1888</span> 元</p>
                        <p>签到送钱 <span>8</span> 元</p>
                        <div class="btn"><div class="title">签到</div><div class="bg"></div></div>
                    </div>
                </div>
                <div class="flag">
                	<div class="flag-head"><p>星期三</p></div>
                    <div class="flag-content">
                    	<p>消费满 <span>1888</span> 元</p>
                        <p>签到送钱 <span>8</span> 元</p>
                        <div class="btn"><div class="title">签到</div><div class="bg"></div></div>
                    </div>
                </div>
                <div class="flag">
                	<div class="flag-head"><p>星期四</p></div>
                    <div class="flag-content">
                    	<p>消费满 <span>1888</span> 元</p>
                        <p>签到送钱 <span>8</span> 元</p>
                        <div class="btn"><div class="title">签到</div><div class="bg"></div></div>
                    </div>
                </div>
                <div class="flag">
                	<div class="flag-head"><p>星期五</p></div>
                    <div class="flag-content">
                    	<p>消费满 <span>1888</span> 元</p>
                        <p>签到送钱 <span>8</span> 元</p>
                        <div class="btn"><div class="title">签到</div><div class="bg"></div></div>
                    </div>
                </div>
                <div class="flag">
                	<div class="flag-head"><p>星期六</p></div>
                    <div class="flag-content">
                    	<p>消费满 <span>1888</span> 元</p>
                        <p>签到送钱 <span>8</span> 元</p>
                        <div class="btn"><div class="title">签到</div><div class="bg"></div></div>
                    </div>
                </div>
                <div class="flag">
                	<div class="flag-head"><p>星期日</p></div>
                    <div class="flag-content">
                    	<p>消费满 <span>1888</span> 元</p>
                        <p>签到送钱 <span>8</span> 元</p>
                        <div class="btn"><div class="title">签到</div><div class="bg"></div></div>
                    </div>
                </div>
                <div class="flag">
                	<div class="flag-head"><p>星期日</p></div>
                    <div class="flag-content">
                    	<p>消费满 <span>1888</span> 元</p>
                        <p>签到送钱 <span>8</span> 元</p>
                        <div class="btn no"><div class="title">未达到</div><div class="bg"></div></div>
                    </div>
                </div> 
            </div>
            <!-- flags over -->
            <div class="msg" >
            	<p>活动介绍：金猴开运，金鹰娱乐平台于2016.1.01隆重推出手机客户端支持ios和Android系统！下载手机客户端送16元体验金，不受时间地点限制，随时购彩！ 活动声明：</p>
                <p><br /></p>
                <p>1.通过首页活动图片的“二维码”扫一扫即可下载手机端，和首页横幅“手机版”里面亦可下载；</p>
                <p>2.成功下载《金鹰娱乐》手机端的老用户即可向在线客服申请16元体验金；</p>
                <p>3.成功下载《金鹰娱乐》手机端的新用户充值100元起即可向在线客服申请16元体验金；</p>
            </div>
            
        </div></div> --%>
    </div>
    <!-- main-child over -->
</div></div>
<!-- main over -->
<jsp:include page="/front/include/chat_common.jsp"></jsp:include>
<!--footer-->
<jsp:include page="/front/include/footer.jsp"></jsp:include>
</body>
</html>