 <%@ page language="java" contentType="text/html; charset=UTF-8"
	import = "com.team.lottery.enums.EFundWithDrawStatus"
	import = "com.team.lottery.enums.EFundPayType"
    pageEncoding="UTF-8"%>
<%@ page import="com.team.lottery.system.SystemConfigConstant" %>
<%
	String path = request.getContextPath();
%>    
<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>提现记录</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script src="<%=path%>/front/js/user_base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<!-- css -->
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/user_withdraw_order.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/js/withdraw/withdraw_order.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
</head>
<body>

<jsp:include page="/front/include/header_user.jsp"></jsp:include>
<jsp:include page="/front/include/header_nav.jsp"></jsp:include>
<!-- main -->
<div class="main"><div class="container">
	<div class="head">
    	<ul class="nav">
        	<a href="<%=path%>/gameBet/myInfo.html"><li>个人资料</li></a>
            <a href="<%=path%>/gameBet/bankCard/bankCardList.html"><li>绑定卡号</li></a>
            <a href="<%=path%>/gameBet/rechargeOrder.html"><li>充值记录</li></a>
            <a href="<%=path%>/gameBet/withdrawOrder.html"><li  class="on">提现记录</li></a>
        </ul>
        <div class="t"><p class="title">温馨提示</p>
        	<div class="t-msg"><img class="bg" src="<%=path%>/front/images/user/msg_bg.png" /><p>为了保障您的账户安全，请如实填写以下信息！<br />当您账户信息被盗取时，如实填写信息可帮助您找回账号！</p></div>
        </div>
    </div>
    <div class="siftings">
    	<div class="siftings-setting">
            <div class="child">
                <div class="child-title">状态：</div>
                <div class="line-content select-content sort" onclick="event.cancelBubble = true;">
                    <input type="hidden" class="select-save" value="" id="status">
                    <p class="select-title">全部</p><img class="select-pointer" src="<%=path%>/front/images/user/p2.png">
                    <ul class="select-list myList" style="display: none;">
                      <!--   <li class="on">状态1</li>
                        <li>状态2</li>
                        <li>状态3</li>
                        <li>状态4</li>
                        <li>状态5</li> -->
                           <li><p data-value="">全部</p></li>
                        <%
                              EFundWithDrawStatus [] eFundWithDrawStatuses = EFundWithDrawStatus.values();
                           for(EFundWithDrawStatus eFundWithDrawStatus : eFundWithDrawStatuses){
                        	  
                        %>
                           <li><p data-value="<%=eFundWithDrawStatus.getCode() %>"><%=eFundWithDrawStatus.getDescription()%></p></li>
                        <%
                          }
                        %>
                    </ul>
                </div>
            </div>
           <div class="child">
                <div class="child-title">范围：</div>
                <div class="line-content select-content sort" onclick="event.cancelBubble = true;">
                    <input type="hidden" class="select-save"  name="rangen" value="" id="rangen">
                    <p class="select-title">自己</p><img class="select-pointer" src="<%=path%>/front/images/user/p2.png">
                    <ul class="select-list myList" style="display: none;">
                        <!-- <li class="on">充值方式1</li>
                        <li>充值方式2</li>
                        <li>充值方式3</li>
                        <li>充值方式4</li>
                        <li>充值方式5 <li><p data-value="NOT_RELEASE">尚未发放</p></li></li> -->
                        <!-- <li><p data-value="">全部</p></li> -->
                        <li><p data-value="1">自己</p></li>
                        <li><p data-value="2">直接下级</p></li>
                        <li><p data-value="3">所有下级</p></li>
                    </ul>
                </div>
            </div>
            <div class="child">
            	<div class="child-title">起始日期：</div>
                	<input type="text" class="child-content long" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="readonly" type="text" name="dateStar" id="dateStar"/>
                <div class="child-title">-</div>
                	<input type="text" class="child-content long" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="readonly" type="text" name="dateEnd" id="dateEnd"/>
            </div>
            <div class="child"><input type="button" class="btn"  onclick="withDrawOrderPage.pageParam.pageNo = 1;withDrawOrderPage.findUserWithDrawOrderByQueryParam()" value="查询" /></div>
        </div>
        <div class="siftings-titles">
        	<div class="child" style="display: none;" id="userNameCol">用户名</div>
        	<div class="child">交易流水号</div>
            <div class="child">申请金额</div>
            <div class="child">到账金额</div>
            <div class="child">交易时间</div>
            <div class="child no">状态</div>
        </div>


        <div class="siftings-content" id="userWithDrawOrderList">
            <p>正在加载中.....</p>
        </div>
        <div class="siftings-foot" id='pageMsg'>
            <p class="msg">记录总数： 0 页数： 1/1</p>
            <div class="navs"><a href="#">首页</a><a href="#">上一页</a><a href="#" class="on">1</a><a href="#">下一页</a><a href="#">尾页</a></div>
        </div>
    </div>
    

</div></div>
<!-- main over -->
<jsp:include page="/front/include/chat_common.jsp"></jsp:include>
<!--footer-->
<jsp:include page="/front/include/footer.jsp"></jsp:include>
</body>
</html>