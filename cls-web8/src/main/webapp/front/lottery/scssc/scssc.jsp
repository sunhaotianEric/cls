<%@ page language="java" contentType="text/html; charset=UTF-8"
    import="com.team.lottery.vo.LotterySet,com.team.lottery.vo.BizSystem,java.math.BigDecimal"
    import="com.team.lottery.util.ApplicationContextUtil,com.team.lottery.enums.ELotteryTopKind"
    import="com.team.lottery.util.ConstantUtil,com.team.lottery.service.UserService,com.team.lottery.vo.User,
       com.team.lottery.system.BizSystemConfigVO,com.team.lottery.system.BizSystemConfigManager,
       com.team.lottery.vo.AwardModelConfig,com.team.lottery.system.SystemConfigConstant,com.team.lottery.cache.ClsCacheManager"
    import="com.team.lottery.extvo.ZipConfig,com.team.lottery.system.cache.LotteryIssueCache,java.util.Map,com.team.lottery.enums.ELotteryKind"
    pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	User user = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	Integer systemLotterySwitch = ClsCacheManager.getLotteryStatusByLotteryType("SUPER_SYSTEM", ELotteryKind.SCSSC.getCode());
	// 业务系统彩种开关状态.
	Integer bizSystemLotterySwitch = ClsCacheManager.getLotteryStatusByLotteryType(user.getBizSystem(), ELotteryKind.SCSSC.getCode());
	// 彩种开关状态不为1的时候就跳往首页.
	if((systemLotterySwitch != null && systemLotterySwitch != 1)||(bizSystemLotterySwitch != null && bizSystemLotterySwitch != 1)){
		response.sendRedirect(path+"/gameBet/index.html");
	}
	Integer maxLotteryNum = LotteryIssueCache.getInstance().lotteryIssueMaxNumMap.get(ELotteryKind.SCSSC.getCode());
%>        


<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>四川时时彩</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/lottery_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/lottery1.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/jquery/jquery-ui.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script src="<%=path%>/front/assets/jquery/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>

<style type="text/css">
.game-order-current{
background: #E6E6E6;
}
.cp-yellow{color:#f8a525}
.cp-red{color:red}
.cp-green{color:green}
</style>

<%
AwardModelConfig clsCacheManager = ClsCacheManager.getAwardModelConfigByCode(user.getBizSystem());

BigDecimal currentSystemLowestAwardModel = clsCacheManager.getSscLowestAwardModel(); //当前系统使用的分分彩最低模式
BigDecimal sscLowestAwardModel = SystemConfigConstant.SSCLowestAwardModel; //分分彩最低奖金模式
BigDecimal sscHighestAwardModel = SystemConfigConstant.SSCHighestAwardModel; //分分彩最高奖金模式
%>

</head>

<body style="background-image:url(<%=path%>/front/images/lottery/bg2.png);">

<jsp:include page="/front/include/header_user.jsp"></jsp:include>

<!-- 需要加载的图片 -->
<div class="stance"></div>

<jsp:include page="/front/include/lottery_common.jsp"></jsp:include>

<div class="main-ml0">
<!-- m-banner -->
<div class="m-banner" >
	<div class="banner-content container">
    	<div class="model model1">
            <div class="icon"><img src="<%=path%>/front/images/lottery/logo/icon-shishicai.png" alt=""></div>
            <!-- <div class="info">
                <h2 class="title">六合彩</h2>
                <p class="p1">总共: <span class="font-red">420</span> 期</p>
                <p class="p2">已开64期,还有23期</p>
            </div> -->
            <div class="info">
                <h2 class="title" style="text-align:center;">四川时时彩</h2>
                <p class="p1" style="text-align:center;">总共: <span class="font-red" id="allLotteryNum">--</span> 期</p>
                <p class="p2" style="text-align:center;" id="lotteryNumDetail">已开--期,还有--期</p>
            </div>
        </div>
        <div class="model model2">
            <p class="title">第 <span class="font-red" id="nowNumber">000000-000</span> 期</p>
            <p class="time" id="timer">00<span>:</span>00</p>
            <p class="over">离投注截止还有</p>
            <div id="presellTip" style="display: none">
           		<p style="font-size: 24px;color: #FFB700">预售中...</p>
           	</div>
        </div>

        <div class="model model3">
            <p class="title">第 <span class="font-red" id="J-lottery-info-lastnumber">000000-000</span> 期开奖号码</p>
            <div class="btns">
            	<div id="lotteryNumber1" class="btn">-</div>
                <div id="lotteryNumber2" class="btn">-</div>
                <div id="lotteryNumber3" class="btn">-</div>
                <div id="lotteryNumber4" class="btn">-</div>
                <div id="lotteryNumber5" class="btn">-</div>
            </div>
            <div class="btns2">
            	<span class="btn btn-Music">关闭音乐</span>
            	<audio id="bgMusic" src=""></audio>
                <a href="<%=path%>/gameBet/lottery/scssc/scssc_zst.html" target="_blank" class="btn">号码走势</a>
            </div>
        </div>
        
        <div class="model model4">
          <div class="ctitle">
            	<p class="title-child child1">今日开奖</p>
                <p class="title-child child2">最新投注</p>
                <p class="title-child child3">开奖动态</p>
            </div>
            <div class="c-content">
            	<ul class="c-title" id="nearestTenLotteryCode">
                	<li class="child child1">期号</li>
                    <li class="child child2">开奖号</li>
                    <li class="child child3">后三组态</li>
                </ul>
                <!-- <ul class="c-list">
                	<li class="child child1">20150815-049</li>
                    <li class="child child2">6,5,4,9,2</li>
                    <li class="child child3">组六</li>
                </ul>
                <ul class="c-list">
                	<li class="child child1">20150815-049</li>
                    <li class="child child2">6,5,4,9,2</li>
                    <li class="child child3">组六</li>
                </ul> -->
            </div>
            <div id="showAfterLotteryCodeBtn" class="more more1"><img src="<%=path%>/front/images/lottery/head-pointer.png" /></div>
            <div id="showPreLotteryCodeBtn" class="more more2"><img src="<%=path%>/front/images/lottery/head-pointer.png" /></div>
            <div id="preLotteryCode" style="display: none"></div>
            <div id="afterLotteryCode" style="display: none"></div>
        </div>
        <div class="change">
            <div class="child on"><p>近一期</p></div>
            <div class="child"><p>近五期</p></div>
        </div>
    </div>
</div>
<!-- m-banner over -->

<!-- main -->
<div class="main">
	<div class="container">
	<ul class="classify">
    	
    	<li class="on"><p class="title">五星</p>
        	<div class="classify-list" id="WX"><img class="top" src="<%=path%>/front/images/lottery/list-top.png" />
            </div>
        </li>
    	<li><p class="title">四星</p>
        	<div class="classify-list" id="SX"><img class="top" src="<%=path%>/front/images/lottery/list-top.png" />
            </div>
        </li>
        <li><p class="title">前三</p>
        	<div class="classify-list" id="QS"><img class="top" src="<%=path%>/front/images/lottery/list-top.png" />
            </div>
        </li>
        <li><p class="title">后三</p>
        	<div class="classify-list" id="HS"><img class="top" src="<%=path%>/front/images/lottery/list-top.png" />
            </div>
        </li>
        <li><p class="title">前二</p>
        	<div class="classify-list" id="QE"><img class="top" src="<%=path%>/front/images/lottery/list-top.png" />
            </div>
        </li>
        <li><p class="title">后二</p>
        	<div class="classify-list" id="HE"><img class="top" src="<%=path%>/front/images/lottery/list-top.png" />
            </div>
        </li>
        <li><p class="title">一星</p>
        	<div class="classify-list" id="YX"><img class="top" src="<%=path%>/front/images/lottery/list-top.png" />
            </div>
        </li>
        
        <li><p class="title">任选</p>
        	<div class="classify-list" id="RX"><img class="top" src="<%=path%>/front/images/lottery/list-top.png" />
            </div>
        </li>
        <li><p class="title">大小单双</p>
        	<div class="classify-list" id="DXDS"><img class="top" src="<%=path%>/front/images/lottery/list-top.png" />
            </div>
        </li>
        <li><p class="title">总和</p>
        	<i class="icon-new"></i>
        	<div class="classify-list" id="ZH"><img class="top" src="<%=path%>/front/images/lottery/list-top.png" />
            </div>
        </li>
        
        
        <div class="right">
        	<div class="r-child">
        		<a href="javascript:;" class="r-title btn-rule">游戏说明</a>
            </div>
       </div>
    </ul>
    <div class="main-content">
    	<div class="main-title">
            
        	<p class="title" id="currentKindName">直选：复式</p>
            <!-- <div class="right" id="awardModel">
            	<span class="on" data-value="YUAN">元</span>
            	<span data-value="JIAO">角</span>
            	<span data-value="FEN">分</span>
            </div> -->
        </div>
        <div class="demo-title demo-title1" id="playDes">
            <!-- <div class="title">每位至少选择一个号码，竞猜开奖号码的全部五位，号码和位置都对应即中奖，最高奖金"191000.0000"元 
	            <div class="selct-demo"><span>选号示例</span>
	                <div class="selct-demo-center">
	                    <p>投注：123456</p>
	                    <p>开奖：123456</p>
	                    <p>奖金：1234567894</p>
	                </div>
	            </div>
            </div> -->
        </div>
        <div id="ommitHotColdBtn" class="main-title demo-title4" >
            <div class="classify-childs">
                <div id="currentOmmit" class="classify-child classify-child1 yellow">当前遗漏</div>
                <div id="currentHotCold" class="classify-child classify-child2 blue">冷热</div>
            </div>
           <!--  <div class="cl-btn on">30期</div>
            <div class="cl-btn">60期</div>
            <div class="cl-btn">100期</div> -->
        </div>
        <div id="importDsBtn" class="demo-title demo-title2">
            <div class="title">
            <div class="inputFile">导入注单<input id="lotteryFile" type="file" accept=".txt" class="inputFile" /></div>
            	查看标准格式样本 
            <div class="selct-demo"><span>选号示例</span>
                <div id="dsModelSample" class="selct-demo-center">
                    <!-- <p>01,00,10,00,00</p>
                    <p>01,00,10,00,00</p>
                    <p>01,00,10,00,00</p>
                    <p>01,00,10,00,00</p>
                    <p>01,00,10,00,00</p> -->
                </div>
            </div>
            </div>
        </div>
        <div id="rxWeiCheck" class="demo-title demo-title3">
            <div class="title">
                <input id="position-1" onclick="lottertyDataDeal.dsPositionCheckFunc(this);" name="ds_position" data-position="1" type="checkbox"  checked="checked">万位 
                <input id="position-2" onclick="lottertyDataDeal.dsPositionCheckFunc(this);" name="ds_position" data-position="2" type="checkbox"  checked="checked">千位 
                <input id="position-3" onclick="lottertyDataDeal.dsPositionCheckFunc(this);" name="ds_position" data-position="3" type="checkbox"  checked="checked">百位 
                <input id="position-4" onclick="lottertyDataDeal.dsPositionCheckFunc(this);" name="ds_position" data-position="4" type="checkbox"  checked="checked">十位 
                <input id="position-5" onclick="lottertyDataDeal.dsPositionCheckFunc(this);" name="ds_position" data-position="5" type="checkbox"  checked="checked">个位 
	            <div style="font-size: 12px;margin-left: 10px;display: inline-block;">
	            	温馨提示：您选择了<span id="pnum" style="color: #F39718">5</span>个位置，系统自动根据位置合成<span id="fnum" style="color: #F39718">5</span>个方案
	            </div>
            </div>
        </div>
        <!-- lists -->
        <ul class="lists" id="ballSection">
        	<!-- <li class="odd">
            	<div class="l-title"><p>万位</p><p class="l-msg">当前遗漏</p></div>
                <div class="l-mun">
                	<div class="mun" onclick="$(this).toggleClass('on')">0<p class="l-msg">0</p></div>
                    <div class="mun" onclick="$(this).toggleClass('on')">1<p class="l-msg red">13</p></div>
                    <div class="mun" onclick="$(this).toggleClass('on')">2<p class="l-msg">12</p></div>
                    <div class="mun" onclick="$(this).toggleClass('on')">3<p class="l-msg">13</p></div>
                    <div class="mun" onclick="$(this).toggleClass('on')">4<p class="l-msg red">14</p></div>
                    <div class="mun" onclick="$(this).toggleClass('on')">5<p class="l-msg red">13</p></div>
                    <div class="mun" onclick="$(this).toggleClass('on')">6<p class="l-msg">7</p></div>
                    <div class="mun" onclick="$(this).toggleClass('on')">7<p class="l-msg">0</p></div>
                    <div class="mun" onclick="$(this).toggleClass('on')">8<p class="l-msg">13</p></div>
                    <div class="mun" onclick="$(this).toggleClass('on')">9<p class="l-msg">8</p></div>
                    <div class="mun" onclick="$(this).toggleClass('on')">10<p class="l-msg">13</p></div>
                    <div class="mun" onclick="$(this).toggleClass('on')">11<p class="l-msg">2</p></div>
                </div>
                <div class="l-mun2">
                	<div class="mun" onclick="select_all(this)">全</div>
					<div class="mun" onclick="select_big(this)">大</div>
					<div class="mun" onclick="select_small(this)">小</div>
					<div class="mun" onclick="select_odd(this)">奇</div>
					<div class="mun" onclick="select_even(this)">偶</div>
					<div class="mun" onclick="select_clear(this)">清</div>
                </div>
            </li>
            <li class="even">
            	<div class="l-title"><p>千位</p><p class="l-msg">当前遗漏</p></div>
                <div class="l-mun">
                	<div class="mun" onclick="$(this).toggleClass('on')">0<p class="l-msg">0</p></div>
                    <div class="mun" onclick="$(this).toggleClass('on')">1<p class="l-msg red">13</p></div>
                    <div class="mun" onclick="$(this).toggleClass('on')">2<p class="l-msg">12</p></div>
                    <div class="mun" onclick="$(this).toggleClass('on')">3<p class="l-msg">13</p></div>
                    <div class="mun" onclick="$(this).toggleClass('on')">4<p class="l-msg red">14</p></div>
                    <div class="mun" onclick="$(this).toggleClass('on')">5<p class="l-msg red">13</p></div>
                    <div class="mun" onclick="$(this).toggleClass('on')">6<p class="l-msg">7</p></div>
                    <div class="mun" onclick="$(this).toggleClass('on')">7<p class="l-msg">0</p></div>
                    <div class="mun" onclick="$(this).toggleClass('on')">8<p class="l-msg">13</p></div>
                    <div class="mun" onclick="$(this).toggleClass('on')">9<p class="l-msg">8</p></div>
                    <div class="mun" onclick="$(this).toggleClass('on')">10<p class="l-msg">13</p></div>
                    <div class="mun" onclick="$(this).toggleClass('on')">11<p class="l-msg">2</p></div>
                </div>
                <div class="l-mun2">
                	<div class="mun" onclick="select_all(this)">全</div>
					<div class="mun" onclick="select_big(this)">大</div>
					<div class="mun" onclick="select_small(this)">小</div>
					<div class="mun" onclick="select_odd(this)">奇</div>
					<div class="mun" onclick="select_even(this)">偶</div>
					<div class="mun" onclick="select_clear(this)">清</div>
                </div>
            </li> -->
            
            <!--<div class="l-textarea"><textarea class="textArea"></textarea></div>-->
        </ul>
        <!-- lists over -->
        <!-- choose -->
        <div class="choose">
            <div class="choose-title">
                <span>您选择了 </span>
                <span id="J-balls-statistics-lotteryNum" class="multiple">0</span>
                <span>注，倍数：</span>
                
                <div id="beishuEdit" class="multiple-select">
                    <img id="beishuSub" class="multiple-btn sub" src="<%=path%>/front/images/lottery/sub-btn.png" />
                    <input id="beishu" type="text" class="inputText" value="1" />
                    <img id="beishuAdd" class="multiple-btn add" src="<%=path%>/front/images/lottery/add-btn.png" />
                </div>
                <div id="beishuStatic" class="multiple-select" style="display: none;">
                	<span>1</span>
                </div>
                <span>共计：</span>
                <span id="J-balls-statistics-amount" class="all-multiple">0.0000</span>
                <span>元 &nbsp;&nbsp;&nbsp;</span>
                
                <div class="btnss">
	                <div id="J-add-order" class="btn yellow">
						<span>添加</span>
					</div>
	                <div id="randomOneBtn" class="btn blue"><span>机选一注</span></div>
	                <!-- <div id="randomFiveBtn" class="btn blue"><span>机选五注</span></div> -->
	                <div id="oneKeyLotteryBtn" class="btn blue"><span>一键投注</span></div>
	                <div id="clearAllCodeBtn" class="btn gray"><span>清空</span></div>
                </div>
            </div>
            <div class="choose-content choose-content1">
            
                <ul class="clists" id="J-balls-order-container">
                    <%-- <li>
                        <div class="li-child li-title">[五星_复式]<span>8,6,8,6,6</span></div>
                        <div class="li-child li-money">¥2.0000元</div>
                        <div class="li-child li-strip">1注</div>
                        <div class="li-child li-multiple">1倍</div>
                        <div class="li-child li-addmoney">¥191000.0000元</div>
                        <div class="li-child li-close" onClick="remove_li(this)"><img src="<%=path%>/front/images/lottery/close.png" /></div>
                    </li>
                    --%>
                </ul>
                <div class="btns">
                    <div id="J-submit-order" class="btn red">
                        <div class="btn-image"><img src="<%=path%>/front/images/lottery/i1.png" /></div>
                        <p class="btn-title">确认投注</p>
                    </div>
                    <div id="zhuiCodeButton" class="btn yellow">
                        <div class="btn-image"><img src="<%=path%>/front/images/lottery/i2.png" /></div>
                        <p id="zhuiCodeButtonTip" class="btn-title">我要追加</p>
                    </div>
                </div>
                
            </div>
            
            <div class="choose-foot">
                <span>当前奖金模式：</span>
                <span class="money" id="currentModelValue">****</span>
                <span>返奖调节</span>
                
                <div id="slider"></div>
                <!-- <span class="slider-m slider-mun">0</span> -->
                <span id="returnPercent" class="slider-m slider-pct">0.00%</span>
                
                <div class="choost-fonts">
	                <div class="select" id="awardModel">
	                    <input type="hidden" id="awardModelValue" class="select-title" value="YUAN">
	                    <p class="select-title" id="awardModelShow">元</p><img class="select-pointer" src="<%=path%>/front/images/user/p.png">
	                    <ul class="select-list">
				            	<li class="on" data-value="YUAN">元</li>
				            	<li data-value="JIAO">角</li>
				            	<li data-value="FEN">分</li>
				            	<!-- <li data-value="LI">厘</li>
				            	<li data-value="HAO">毫</li> -->
	                    </ul>
	                </div>
                
	                <span class="big">方案注数</span>
	                <span id="J-gameOrder-lotterys-num" class="strip big">0</span>
	                <span>注， 金额：</span>
	                <span id="J-gameOrder-amount" class="allmoney big">¥0.0000</span>
	                <span class="big">元</span>
	            </div>
            </div>

            
            <div id="J-trace-panel" class="choose-content choose-content2" style="display:none;">
                
                <ul class="clists">
                    <div class="titles">
                        <div id="zhuiCodeForToday" class="child on">今天</div>
                        <div id="zhuiCodeForTomorrow" class="child">明天</div>
                        <p class="more"><input id="J-trace-iswintimesstop" type="checkbox" /><span>中奖后停止追号</span></p>
                    </div>

                    <div id="todayList" class="li-contents" data-type="TODAY">
	                    <li class="li-titles">
	                        <div class="li-child li-no">序号（今日）</div>
	                        <div class="li-child li-title"><input type="checkbox" /><span>期数</span><input type="text" class="inputText" /></div>
	                        <div class="li-child li-multiple"><input type="text" class="inputText" /><span>倍</div>
	                        <div class="li-child li-money">投入金额</div>
	                        <div class="li-child li-time">投注截止时间</div>
	                    </li>
	                    
	                    <!-- <li>
	                        <div class="li-child li-no">1</div>
	                        <div class="li-child li-title"><input type="checkbox" /><span>20160526-1053(当前期号)</span></div>
	                        <div class="li-child li-multiple"><input type="text" class="inputText" /><span>倍</div>
	                        <div class="li-child li-money">¥2.0000元</div>
	                        <div class="li-child li-time">2016/05/26 17:49:00</div>
	                    </li> -->
                    </div>

                    <div id="tomorrowList" class="li-contents" style="display:none;" data-type="TOMORROW">
	                    <li class="li-titles">
	                        <div class="li-child li-no">序号（明日）</div>
	                        <div class="li-child li-title"><input type="checkbox" /><span>期数</span><input type="text" class="inputText" /></div>
	                        <div class="li-child li-multiple"><input type="text" class="inputText" /><span>倍</div>
	                        <div class="li-child li-money">投入金额</div>
	                        <div class="li-child li-time">投注截止时间</div>
	                    </li>
	                    <!-- <li>
	                        <div class="li-child li-no">1</div>
	                        <div class="li-child li-title"><input type="checkbox" /><span>20160526-1053(当前期号)</span></div>
	                        <div class="li-child li-multiple"><input type="text" class="inputText" /><span>倍</div>
	                        <div class="li-child li-money">¥2.0000元</div>
	                        <div class="li-child li-time">2016/05/26 17:49:00</div>
	                    </li> -->
                    </div>
                </ul>
            </div>
            
        </div>
        <!-- choose over -->
    </div>
     <!-- main-msg -->
    <div class="main-msg">
        <div class="titles">
            <div id="userTodayLottery" class="child on">投注记录</div>
            <div id="userTodayLotteryAfter" class="child">追号记录</div>

            <a href="<%=path%>/gameBet/order.html">查看更多</a>
        </div>
        <div class="contents" id="userTodayLotteryList" >
            <div class="line line-titles">
                <div class="child child1">订单编号</div>
                <div class="child child2">彩种</div>
                <div class="child child3">期号</div>
                <div class="child child4">投注金额</div>
                <div class="child child5">奖金</div>
                <div class="child child6">模式</div>
                <div class="child child7">下单时间</div>
                <div class="child child8">状态</div>
                <div class="child child9 no">内容</div>
            </div>
            
        </div>
        <div class="contents" id="userTodayLotteryAfterList" style="display:none;">
            <div class="line line-titles">
                <div class="child child11">订单编号</div>
                <div class="child child12">彩种</div>
                <div class="child child13">开始期数</div>
                <div class="child child14">追号期数</div>
                <div class="child child15">完成期数</div>
                <div class="child child16">总金额</div>
                <div class="child child17">完成金额</div>
                <div class="child child18">中奖金额</div>
                <div class="child child19">模式</div>
                <div class="child child20">下单时间</div>
                <div class="child child21">状态</div>
                <div class="child child22 no">内容</div>
            </div>
            
        </div>
    </div>
    <!-- main-msg over -->
</div>
</div>
<!-- main over -->
</div>

<!-- alert-msg -->
<div class="alert-msg-bg"></div>
<div id="lotteryOrderDialog" class="alert-msg" >
    <div class="msg-head">
        <p>温馨提示<span id="haoTip" style="font-size: 14px;font-weight: bold;color: rgb(255, 0, 0);margin-left: 15px;display: none;">当天用过毫模式的，当天每期最高返奖总额为2万元</span></p>
        <img id="lotteryOrderDialogCancelButton" class="close" src="<%=path%>/front/images/user/close.png" />
    </div> 
    <div class="msg-content">
        <div class="line no-border">
            <div class="line-title">彩种：</div>
            <div class="line-content">
                <div class="line-text"><p id="lotteryKindForOrder">****</p></div>
            </div>
        </div>
        <div class="line no-border">
            <div class="line-title">期号：</div>
            <div class="line-content">
                <div class="line-text"><p id="lotteryExpectDesForOrder" style="width:350px">第 ****** 期</p></div>
            </div>
        </div>
        <div class="line line-textarea">
            <div class="line-title">投注详细：</div>
            <div class="line-content ">
                <div class="line-text"><textarea id="lotteryDetailForOrder" class="textArea" readonly="readonly"></textarea></div>
            </div>
        </div>
        <div class="line no-border">
            <div class="line-title">需支付金额：</div>
            <div class="line-content">
                <div class="line-text"><p id="lotteryTotalPriceForOrder" class="yellow">*.***</p></div>
            </div>
        </div>
        <div class="line no-border">
            <div class="line-title">付款账号：</div>
            <div class="line-content">
                <div class="line-text"><p id="lotteryPayAccountForOrder">***</p></div>
            </div>
        </div>
        
        <div class="btn"><input id="lotteryOrderDialogSureButton" type="button" class="inputBtn" value="确定投注" /></div>
    </div>
</div>

<div id="dsContentShowDialog" class="alert-msg alert-msg2" >
    <div class="msg-head">
        <p>温馨提示</p><img class="close" src="<%=path%>/front/images/user/close.png" onClick='(function msg_show(){$(".alert-msg").stop(false,true).delay(200).fadeOut(500);$(".alert-msg-bg").stop(false,true).fadeOut(500);})()' />
    </div>
    <div class="msg-content">
        <div class="line no-border">
            <div class="line-title">玩法：</div>
            <div class="line-content">
                <div class="line-text"><p id="kindPlayDes">****</p></div>
            </div>
            <div class="line-content">
                <br />
                <div class="line-msg yellow"><p id="kindPlayPositionDes">****</p></div>
            </div>
        </div>
        <div class="line no-border">
            <div class="line-title">模式：</div>
            <div class="line-content">
                <div class="line-text"><p id="kindPlayModel">****</p></div>
            </div>
        </div>
        <div class="line no-border">
            <div class="line-title">包含：</div>
            <div class="line-content">
                <div class="line-text"><p id="kindPlayPriceMsg">****</p></div>
            </div>
        </div>
        <div class="line line-textarea">
            <div class="line-title">投注号码：</div>
            <div class="line-content ">
                <div class="line-text"><textarea id="kindPlayCodeDes" class="textArea" disabled="disabled">*******</textarea></div>
            </div>
        </div>
    
        <div class="btn"><input id="dsContentShowDialogSureButton" type="button" class="inputBtn" value="确定"  /></div>
    </div>
</div>

<!-- alert-msg over -->

	<jsp:include page="/front/include/chat_common.jsp"></jsp:include>

	<jsp:include page="/front/include/footer.jsp"></jsp:include>
	<!-- common js -->
	<script type="text/javascript" src="<%=path%>/front/lottery/common/lottery_common.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
	<!-- 业务js -->
	<script type="text/javascript" src="<%=path%>/front/lottery/scssc/js/data.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
	<script type="text/javascript" src="<%=path%>/front/lottery/scssc/js/scssc.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
	<script type="text/javascript" src="<%=path%>/resources/jszip/jszip.min.js"></script>
	<script type="text/javascript">
		var isZipOpen = <%=ZipConfig.IS_ZIP_OPEN %>;
		var currentSystemLowestAwardModel = <%=currentSystemLowestAwardModel.doubleValue() %>;
		var lowestAwardModel = <%=sscLowestAwardModel.doubleValue() %>;
		var highestAwardModel = <%=sscHighestAwardModel.doubleValue() %>;
		var currentUserHighestModel = currentUser.sscRebate;
		var maxLotteryNum =  <%=maxLotteryNum %>;
		lotteryCommonPage.lotteryParam.modelValue = currentUserHighestModel;
	</script>
</body>
</html>
