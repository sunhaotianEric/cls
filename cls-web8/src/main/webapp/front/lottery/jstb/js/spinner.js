$(function(){
	$(".payinput .reduce").css("cursor","not-allowed");
	$(".payinput .add").click(function(){
			var num=$(this).siblings("input").val();
			num=parseInt(num);
			if(num<10){
				num+=1;
				$(this).siblings("input").val(num);
				$(this).siblings(".reduce").css("cursor","pointer");
				if(num==10){
					$(this).css("cursor","not-allowed");
				}
			}	
	});

	$(".payinput .reduce").click(function(){
			var num=$(this).siblings("input").val();
			num=parseInt(num);	
			if(num>1){
			num-=1;
			$(this).siblings("input").val(num);
			$(this).siblings(".add").css("cursor","pointer");
			if(num==1){
				$(this).css("cursor","not-allowed");

			}
		}			
	});
});