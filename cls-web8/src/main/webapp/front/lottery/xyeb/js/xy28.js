function XyebPage(){}
var xyebPage = new XyebPage();
lotteryCommonPage.currentLotteryKindInstance = xyebPage; //将这个投注实例传至common js当中

//基本参数
xyebPage.param = {
	unitPrice : 1,  //每注价格
	kindDes : "幸运28",
	kindName : 'XYEB',  //彩种
	kindNameType : 'XYEB', //大彩种拼写
	openMusic : true, //开奖音乐开关
	oldLotteryNum : null,
	ommitData : null,  //遗漏数据
	hotColdData : null //冷热数据
};

//投注参数
xyebPage.lotteryParam = {
	lotteryKindMap : new JS_OBJECT_MAP(),
    currentKindKey : 'XYEB',    //当前所选择的玩法模式,默认是特码B
	currentLotteryCount : 0,       //当前投注数目
	currentLotteryPrice : 0,       //当前投注价格,按元来计算
	currentLotteryTotalCount : 0,  //当前总投注数目
	codes : null,                  //投注号码
	codesStastic : new Array()
};
$(document).ready(function() {
	/*//顶部导航选中处理
	$("#navChoose .nav-child").removeClass("on");
	$("#navChoose .nav-child:eq(1)").addClass("on");
	
	//选择地区事件
	$(".m-banner .b-child1 .btn").click(function(){
		$(".b-child-list").stop(false,true).slideToggle(500);
	});
	
	//切换背景事件
	$(".change-bg img").click(function(){
		var url = $(this).attr("data-val");
		$("body").css({"background-image":"url("+url+")"});
	});*/
	
	$('.btn-submit').on('click',function(){
		/*$('.alert-msg-bg, .alert-xyeb-msg').show();*/
		$(".alert-xyeb-msg").stop(false,true).delay(200).fadeIn(500);
		$(".alert-msg-bg").stop(false,true).fadeIn(500);
	})
	
	// 点击查看金额
	$(".quick-money img").click(function(){
		var value = $(this).data("value");
		quickmoney = Number(quickmoney) + Number(value);
		$(".shortcut-money").val(quickmoney);
		lotteryDataDeal.colsChildSet();
	});
	
	// 近一期、近五期tab切换
	$(".m-banner .change .child").click(function(){
		$(".m-banner .change .child").removeClass("on");
		$(this).addClass("on");
		var index = $(".m-banner .change .child").index($(this));
		if(index == 0){
				$(".m-banner .model4").hide();
				$(".m-banner .model3").show();
		}else if(index == 1){
				$(".m-banner .model4").show();
				$(".m-banner .model3").hide();
		}
	});
	
	/*// 顶部导航链接选中样式
	$('.header .foot .nav .child2').addClass('on');
	
	// 显示顶部换色
	$('#changecolor-show').show();*/
	
	$(".btn-rule").click(function() {
		var tempWindow=window.open('','','top=100,left=100,width=1200,height=800');
		tempWindow.location.href = 'playrule_xyeb.html';  //对新打开的页面进行重定向
    });
});

/**
 * 展示当前正在投注的期号
 */
XyebPage.prototype.showCurrentLotteryCode = function(lotteryIssue){
	if(lotteryIssue != null){
		var openCodeStr = xyebPage.getOpenCodeStr(lotteryIssue.lotteryNum);
		$('#nowNumber').text(openCodeStr);
	}
};

/**
 * 获取开奖号码前台展现的字符串
 */
XyebPage.prototype.getOpenCodeStr = function(openCodeNum){
	var openCodeStr = "";
	if(isNotEmpty(openCodeNum)){
//		var expectDay = openCodeNum.substring(0,openCodeNum.length - 3);
//		var expectNum = openCodeNum.substring(openCodeNum.length - 3,openCodeNum.length);
//		openCodeStr = expectDay + "-" + expectNum;
		openCodeStr = openCodeNum;
	}
	return openCodeStr;
};

/**
 * 展示最新的开奖号码
 */
XyebPage.prototype.showLastLotteryCode = function(lotteryCode){
	if(lotteryCode != null){
		var lotteryNum = lotteryCode.lotteryNum;
		var openCodeStr = xyebPage.getOpenCodeStr(lotteryCode.lotteryNum);
		$('#J-lottery-info-lastnumber').text(openCodeStr);
		
		$(".btns3").empty();
		var sum=Number(lotteryCode.numInfo1)+Number(lotteryCode.numInfo2)+Number(lotteryCode.numInfo3);
		var html='<div class="btn btn-fadered" id="lotteryNumber1" >'+lotteryCode.numInfo1+'</div>'+
				'<div class="btn btn-add" ></div>'+
		        '<div class="btn btn-fadered" id="lotteryNumber2" >'+lotteryCode.numInfo2+'</div>'+
		        '<div class="btn btn-add" ></div>'+
		        '<div class="btn btn-fadered" id="lotteryNumber3">'+lotteryCode.numInfo3+'</div>'+
		        '<div class="btn btn-equal" ></div>'+
		        '<div class="'+xyebPage.changeColorByCode(sum)+'" id="lotteryNumber4">'+sum+'</div>';
		$(".btns3").append(html);
		
		//播放(继续播放)
		if(lotteryCommonPage.param.isFirstOpenMusic){
			if(xyebPage.param.openMusic && xyebPage.param.oldLotteryNum != lotteryNum && xyebPage.param.oldLotteryNum != null){
				document.getElementById("bgMusic").play();
			}
			xyebPage.param.oldLotteryNum = lotteryNum;
		}
		lotteryCommonPage.param.isFirstOpenMusic =true;
		
	}
};

//计算颜色
XyebPage.prototype.changeColorByCode = function(k){
		var colors="";
		if(k==3||k==6||k==9||k==12||k==15||k==18||k==21||k==24){
			colors="btn btn-red";
		}else if(k==1||k==4||k==7||k==10||k==16||k==19||k==22||k==25){
			colors="btn btn-green";
		}else if(k==2||k==5||k==8||k==11||k==17||k==20||k==23||k==26){
			colors="btn btn-blue";
		}else if(k== 0||k==13||k==14||k==27){
			colors="btn btn-gray";
		}
		return colors;
}

XyebPage.prototype.returnAninal = function(opencode){
	var shengxiaoArr =lotteryDataDeal.shengxiao.shengxiaoArr;
	for(var i=0;i<shengxiaoArr.length;i++){		
		for(var j=0;j<shengxiaoArr[i].length;j++){
			if(Number(opencode) == Number(shengxiaoArr[i][j])){
				return lotteryDataDeal.shengxiao.shengxiaoKey[i];
			}
		}
	}
	return "-";
}

//服务器时间倒计时
XyebPage.prototype.calculateTime = function(isClose){
	var SurplusSecond = lotteryCommonPage.param.diffTime;
	
	lotteryCommonPage.param.isClose = isClose;
	if(lotteryCommonPage.param.isClose){ 
		//显示预售中
		$("#timer").hide();
		$("#presellTip").show();
		window.clearInterval(lotteryCommonPage.param.intervalKey); //终止周期性倒计时
	}else{
		var h = Math.floor(SurplusSecond/3600);
		if (h<10){
			h = "0"+h;
		}
		//计算剩余的分钟
		SurplusSecond = SurplusSecond - (h * 3600);
		var m = Math.floor(SurplusSecond/60);
		if (m<10){
			m = "0"+m;
		}
		var s = SurplusSecond%60;
		if(s<10){
			s = "0"+s;
		}

		h = h.toString();
		m = m.toString();
		s = s.toString();
//		$(".times .hours").html(h);
//		$(".times .minute").html(m);
//		$(".times .second").html(s);
		
		$("#timer").html(h+"<span>:</span>"+m+"<span>:</span>"+s);
        $("#presellTip").hide();
		$("#timer").show();
	}
	lotteryCommonPage.param.diffTime--;
	if(lotteryCommonPage.param.diffTime < 0){
		isClose=true;
		//弹出窗口控制
		$("#lotteryOrderDialogCancelButton").trigger("click");
		frontCommonPage.closeKindlyReminder();
		var str = "";
		str += "第";
		str +=  lotteryCommonPage.param.currentExpect + "期已截止,<br/>"
		str += "投注时请注意期号!";
		frontCommonPage.showKindlyReminder(str);	
		
		window.clearInterval(lotteryCommonPage.param.intervalKey); //终止周期性倒计时
		lotteryCommonPage.getActiveExpect();  //倒计时结束重新请求最新期号
	};
};