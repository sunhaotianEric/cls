function LotteryDataDeal(){}
var lotteryDataDeal = new LotteryDataDeal();

lotteryDataDeal.param = {
	bizSystem:null
};
//
lotteryDataDeal.modelMap = new JS_OBJECT_MAP();
lotteryDataDeal.playNumDescMap = new JS_OBJECT_MAP();

/**
 * 加载某种玩法赔率
 */
LotteryDataDeal.prototype.loadLotteryKindPL = function() {
	
	$.ajax({
		type : "POST",
		url : contextPath + "/lottery/getLotteryWinXyebByKind",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if( result.code == "ok"){
				var resultData = result.data;
				//需要验证赔率数据加载完全不完全
				for(var i=1;i<=resultData.length;i++){
					$("#pl"+resultData[i-1].code).text(resultData[i-1].winMoney);
				}
			}else if(result.code == "error"){
				frontCommonPage.showKindlyReminder(result.data);
				$("#editUserButton").removeAttr("disabled");
			}
		}
	});
}



/**
 * 初始化1-49html,涉及玩法有正码，特码，正码特，连码，全不中
 * startpos:1,11,21,31,41
 */
LotteryDataDeal.prototype.initPlay = function(startpos) {
	
	 var main_lists_html = '';
	 var inputType ='text';
	 var inputClass ='inputText';
	 var cols_no = '';
	  
		inputType ='text';
		inputClass ='inputText';
		cols_no = 'cols-no'; 
		if(startpos==0){
			main_lists_html='<div class="lists-title">'+
			'<span class="cols">特码</span>'+
			'</div>';
		}
		main_lists_html+='<ul class="lists-xyeb cols cols-25">';
	
	if(startpos == 0 || startpos ==7 || startpos ==14 || startpos ==21){
		var jx=0;
		for(var i=startpos;i<=(startpos+6);i++){
			var odd = (jx%2==0?"odd":"");
			//红波
			if(i==3||i==6||i==9||i==12||i==15||i==18||i==21||i==24){
				
				main_lists_html += '<li class="cols-child '+odd+' cols-child-red cols-mun-'+i+'">'
			                    + '<div class="cols cols-33"><div class="btn btn-red">'+i+'</div></div>'
			                    + '<div class="cols cols-33"><span class="font-red" id="pl'+i+'">加载中</span></div>'
			                    + '<div class="cols '+cols_no+' cols-34"><input type="'+inputType+'" class="'+inputClass+'" onkeyup="checkNum(this)" id="money'+i+'" /></div>'
			                    + '</li>';
			//绿波
			}else if(i==1||i==4||i==7||i==10||i==16||i==19||i==22||i==25){
				main_lists_html   += '<li class="cols-child '+odd+' cols-child-red cols-mun-'+i+'">'
			                      + '<div class="cols cols-33"><div class="btn btn-green">'+i+'</div></div>'
			                      + '<div class="cols cols-33"><span class="font-red" id="pl'+i+'">加载中</span></div>'
			                      + '<div class="cols '+cols_no+' cols-34"><input type="'+inputType+'" class="'+inputClass+'" id="money'+i+'" onkeyup="checkNum(this)" /></div>'
			                      + '</li>';
				
			//蓝波
			}else if(i==2||i==5||i==8||i==11||i==17||i==20||i==23||i==26){
			
				main_lists_html += '<li class="cols-child '+odd+' cols-child-red cols-mun-'+i+'">'
			                    + '<div class="cols cols-33"><div class="btn btn-blue">'+i+'</div></div>'
			                    + '<div class="cols cols-33"><span class="font-red" id="pl'+i+'">加载中</span></div>'
			                    + '<div class="cols '+cols_no+' cols-34"><input type="'+inputType+'" class="'+inputClass+'" id="money'+i+'" onkeyup="checkNum(this)" /></div>'
			                    + '</li>';
			//灰色
			}else if( i == 0||i==13||i==14||i==27){
				main_lists_html += '<li class="cols-child '+odd+' cols-child-red cols-mun-'+i+'">'
					            + '<div class="cols cols-33"><div class="btn btn-gray">'+i+'</div></div>'
					            + '<div class="cols cols-33"><span class="font-red" id="pl'+i+'">加载中</span></div>'
					            + '<div class="cols '+cols_no+' cols-34"><input type="'+inputType+'" class="'+inputClass+'" id="money'+i+'" onkeyup="checkNum(this)" /></div>'
					            + '</li>';
			}
			jx++;
		}
		main_lists_html += '</ul>';
		if(startpos==21){
			main_lists_html +='<ul class="lists-xyeb lists-xyeb-bottom cols">'+
	            '<li class="cols-child cols-child-blue cols-mun-31">'+
	                '<div class="cols cols-8"><div class="tmbs">特码包三</div></div>'+
	                '<div class="cols cols-8"><span class="font-red" id="pl28">加载中</span></div>'+
	                '<div class="cols cols-83 cols-no clear">'+
	                    '<div class="select" data-value="0">'+
						       ' <span class="text"><i class="btn btn-gray">0</i></span>'+
						       '<span class="arrow">▼</span>'+
						       ' <ul class="options">';
			   main_lists_html += lotteryDataDeal.addhtmlli();
			   main_lists_html += '</ul>'+
						 '</div>'+
						 '<div class="select" data-value="1">'+
						       '<span class="text"><i class="btn btn-green">1</i></span>'+
						       '<span class="arrow">▼</span>'+
						       '<ul class="options">';
			   main_lists_html += lotteryDataDeal.addhtmlli();
			   main_lists_html += '</ul>'+
						 '</div>'+
						 '<div class="select" data-value="2">'+
						       '<span class="text"><i class="btn btn-blue">2</i></span>'+
						       '<span class="arrow">▼</span>'+
						       '<ul class="options">';
			   main_lists_html += lotteryDataDeal.addhtmlli();
			   main_lists_html += '</ul>'+
						 '</div>'+
	                  '<input type="text" class="inputText w100" id="money'+i+'" />'+
	                '</div>'+
	           '</li>'+
	       '</ul>';
		}
   }
	return main_lists_html;
}
LotteryDataDeal.prototype.addhtmlli=function(){
	var htmls='';
	for(var i=0;i<28;i++){
		if(i==3||i==6||i==9||i==12||i==15||i==18||i==21||i==24){
			htmls+='<li data-value="'+i+'"><i class="btn btn-red">'+i+'</i></li>';
		}else if(i==1||i==4||i==7||i==10||i==16||i==19||i==22||i==25){
			htmls+='<li data-value="'+i+'"><i class="btn btn-green">'+i+'</i></li>';
		}else if(i==2||i==5||i==8||i==11||i==17||i==20||i==23||i==26){
			htmls+='<li data-value="'+i+'"><i class="btn btn-blue">'+i+'</i></li>';
		}else if(i == 0||i==13||i==14||i==27){
			htmls+='<li data-value="'+i+'"><i class="btn btn-gray">'+i+'</i></li>';
		}
	}
	return htmls;
}
LotteryDataDeal.prototype.initPlay2=function(){
		var list=['大','单','大单','大双','极大','小','双','小单','小双','极小','红波','绿波','蓝波','豹子'];
		var main_lists_html2 ='<div class="lists-title">'+
						    		'<span class="cols cols-50">混合</span>'+
						    		'<span class="cols cols-25">波色</span>'+
						    		'<span class="cols cols-25">豹子</span>'+
    						  '</div>';
		var k=0;
		var jx=0;
	   	for(var i=29;i<=42;i++){
	   		if(i==31||i==32||i==36||i==37){
	   			jx++;
	   			k++;
	   			continue;
	   		}
	   		var odd = (jx%2==0?"odd":"");
	   		if(i==29||i==34||i==39||i==42){
	   			main_lists_html2 +='<ul class="lists-xyeb cols cols-25">';
	   		}
	   		main_lists_html2 += '<li class="cols-child '+odd+'" >'
				   			 + '<div class="cols cols-33"><span>'+list[k]+'</span></div>'
				   			 + '<div class="cols cols-33"><span class="font-red" id="pl'+i+'">加载中</span></div>'
				   			 + '<div class="cols cols-no cols-34"><input type="text" class="inputText" id="money'+i+'" onkeyup="checkNum(this)"/></div>'
				   			 + '</li>';
	   		if(i==42){
	   			for(var ks=1;ks<3;ks++){
	   				odd = (ks%2==0?"odd":"");
	   				main_lists_html2 += '<li class="cols-child '+odd+'">'
							   			 + '<div class="cols cols-33"><span></span></div>'
							   			 + '<div class="cols cols-33"><span class="font-red"></span></div>'
							   			 + '<div class="cols cols-no cols-34"></div>'
							   			 + '</li>';

	   			}
	   		}
	   		jx++;
	   		if(i==33||i==38||i==41||i==42){
	   			main_lists_html2 +='</ul>';
	   			jx=0;
	   		}
	   		k++;
	   	}
		return main_lists_html2;
}
LotteryDataDeal.prototype.colsChildClick = function (){
	$(".cols-child").unbind("click").click(function(){

		//如果到正码1-6，然后标题和左下方的不能点击变色
		if($(this).hasClass('cols-title') && $(this).index()==0){
			$(this).removeClass("on");
		}else {
			$(this).toggleClass("on");
		}
		
		
		var has = $(this).hasClass("on");
		var hascheckbox = $(this).find("input").hasClass("inputCheckbox");
		var hasinputText =$(this).find("input").hasClass("inputText");
		if(hasinputText && has){
			lotteryDataDeal.colsChildSet();
		}else if(hasinputText){
			$(this).find('input.inputText').val("");

		}    
		    if(hascheckbox && has){
		    	$(this).find("input.inputCheckbox")[0].checked = true;
		    }else if(hascheckbox){
		    	$(this).find("input.inputCheckbox")[0].checked = false;
		    }
	
		});
//		$(".cols-child .inputCheckbox").click(function(e){
//		   e.stopPropagation();
//		});
		$(".cols-no,.cols-no .inputText").click(function(e){
		   e.stopPropagation();
		});
}
//colsChildClick();
LotteryDataDeal.prototype.colsChildSet = function (){
	var shortcutMoney = $(".shortcut-money").val();
	//$(".cols-child .cols-no .inputText").val("");
	$(".cols-child.on .cols-no .inputText").val(shortcutMoney);
	
}

LotteryDataDeal.prototype.select_type = function(e){
	var $el = $(e),
			type = $el.attr("data-type");
			$(e).toggleClass("on");
	var key = {},
			$el = $(e) || {},
			bool = true,
			$btns =  $(".on[data-type]");

	for(var i = 1;i < 50; i++) key[i] = false;

	if(type == "reset"){
			$btns.removeClass("on");
			$(".cols-child.on").removeClass("on");
			$(".cols-child .inputText").val("");
			$(".cols-child .inputCheckbox").attr("checked",false);
	}

	for(var i in key){
		if(key[i]==true){
			$(".cols-mun-"+i).addClass("on");
			//选中号码，加载快捷金额
			lotteryDataDeal.colsChildSet();
		}else if(key[i]==false){
			$(".cols-mun-"+i).removeClass("on");
			$(".cols-mun-"+i+" .cols-no .inputText").val("");
		}
	}
	
}

/**
 * 玩法选择
 */
LotteryDataDeal.prototype.lotteryKindPlayChoose = function(){
	$("#lhcReset").click();
	var main_lists_html="";
	var main_lists_html2="";
	
	main_lists_html = lotteryDataDeal.initPlay(0)+lotteryDataDeal.initPlay(7)+lotteryDataDeal.initPlay(14)+lotteryDataDeal.initPlay(21);
	main_lists_html2=lotteryDataDeal.initPlay2;
	$(".main .lists.clear").html(main_lists_html);
	$(".main .lists.mb15").html(main_lists_html2);
	
	lotteryDataDeal.colsChildClick();
	//加载赔率
	lotteryDataDeal.loadLotteryKindPL();
	//存储当前切换的玩法模式
	//lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey = model;
	
}