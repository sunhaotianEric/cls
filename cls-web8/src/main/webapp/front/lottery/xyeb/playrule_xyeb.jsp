<%@ page language="java" contentType="text/html; charset=UTF-8"
    import="com.team.lottery.vo.LotterySet,com.team.lottery.vo.BizSystem,java.math.BigDecimal"
    import="com.team.lottery.util.ApplicationContextUtil,com.team.lottery.enums.ELotteryTopKind"
    import="com.team.lottery.util.ConstantUtil,com.team.lottery.service.UserService,com.team.lottery.vo.User,
       com.team.lottery.system.BizSystemConfigVO,com.team.lottery.system.BizSystemConfigManager,
       com.team.lottery.vo.AwardModelConfig,com.team.lottery.system.SystemConfigConstant,com.team.lottery.cache.ClsCacheManager"
    import="com.team.lottery.extvo.ZipConfig"
    pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
%>        
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>玩法介绍-幸运28</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/play_rule.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
</head>

<body>

<%-- <jsp:include page="/front/include/header_user.jsp"></jsp:include>

<!-- 需要加载的图片 -->
<div class="stance"></div> --%>

<div class="main-ml0">
<!-- m-banner -->
<div class="m-banner" style="background-image:url(<%=path%>/front/images/login/banner.jpg);">
    <div class="content" ><img src="<%=path%>/front/images/play_rule.png" /><span>幸运28</span></div>
</div>
<!-- m-banner over -->

<!-- m-nav -->
<div class="m-nav container">
<div class="container">
    <div class="block">
        <h2>一般规则说明</h2>
        <ul>
            <li>1. 客户一经在本公司开户或投注，即被视为已接受这些规则。</li>
            <li>2. 如果客户怀疑自己的资料被盗用，应立即通知本公司，并更改个人详细资料，且之前所使用的使用者名称及密码将全部无效。</li>
            <li>3. 客户有责任确保自己的帐户及登入资料的保密性。在本网站上以个人的使用者名称及密码进行的任何网上投注将被视为有效。</li>
            <li>4. 为了避免出现争议，请务必在下注之后检查"下注状况"。</li>
            <li>5. 不论任何原因，开奖之后所接受的投注，将一律被视为"无效"。</li>
            <li>6. 任何的投诉必须在开奖之前提出，本公司将不会受理任何开奖之后的投诉。</li>
            <li>7. 公布的投注时间、项目及赔率出现任何打字错误或非故意人为失误，本公司将保留改正错误和按正确搅珠时间、赔率结算注单的权力。</li>
            <li>8. 公布之所有赔率为浮动赔率，派彩时的赔率将以本公司确认投注时之赔率为准。</li>
            <li>9. 如本公司察觉客户投注状况异常时，有权即时中止客户投注并删除不正常注单；在本公司中止下注前，客户之所有正常投注仍属有效，不得要求取消、延迟交收及不得有任何异议。</li>
            <li>10. 如因在本网站投注触犯当地法律，本公司概不负责。</li>
        </ul>
    </div>
    <div class="block">
        <h2>游戏规则说明</h2>
        <ul>
        	<li class="red">以下所有投注皆含本金。</li>
		    <li>「PC蛋蛋（幸运28）」按北京快乐8的开奖结果为基础</li>
		    <li>北京快乐8开奖结果，每日179期，每天9:00 - 23:55，每5分钟公布一次北京快乐8开奖结果。</li>
		    <li>排序后,分别按号码第 1~6 , 7~12 , 13~18 分为【1】【2】【3】三个区；</li>
		    <li>「把每个区的数字分别相加：三个数值相加求余即为幸运28最终的开奖结果。</li>
		    <li>例如北京快乐8开奖结果为：01,02,04,08,15,16, 25,31,40,41,43,47, 55,56,59,66,69,74, 78,79</li>
		    <li>开奖结果即为：6,7,9;</li>
		    <li>每个号码详细计算步骤：</li>
		    <li>第一个号码：1+2+4+8+15+16 = 46%10 = 6</li>
		    <li>第二个号码：25+31+40+41+43+47 = 227%10 = 7</li>
		    <li>第三个号码： 55+56+59+66+69+74 = 379%10 = 9</li>
		    <li class="red">特码号：6+7+9=22</li>
		    <li class="strong">具体游戏规则如下：</li>
		    <li class="red">大：特码号大于等于14,15,16,17,18,19,20,21,22,23,24,25,26,27为大。</li>
		    <li class="red">小：特码号小于等于00,01,02,03,04,05,06,07,08,09,10,11,12,13为小。</li>
		    <li class="red">单：特码号为单就为单。</li>
		    <li class="red">双：特码号为双就为双。</li>
		    <li class="red">大单：特码号为15,17,19,21,23,25,27。</li>
		    <li class="red">小单：特码号为01,03,05,07,09,11,13。</li>
		    <li class="red">大双：特码号为14,16,18,20,22,24,26。</li>
		    <li class="red">小双：特码号为00,02,04,06,08,10,12。</li>
		    <li class="red">极大：特码号为23,24,25,26,27。</li>
		    <li class="red">极小：特码号为00,01,02,03,04。</li>
		    <li class="red">红波：特码号为03,06,09,12,15,18,21,24。</li>
		    <li class="red">绿波：特码号为01,04,07,10,16,19,22,25。</li>
		    <li class="red">蓝波：特码号为02,05,08,11,17,20,23,26。</li>
		    <li class="red">注:当开奖结果为：0,13,14,27(所有买的波色皆输)视为全部不中奖！</li>
		    <li class="red">豹子：当期开出三个数字相同即为豹子</li>
		    <li class="red">特码：投注的特码号码与当期特码号一致视为中奖。</li>
		    <li class="red">特码包三：选取3个特码号码为一注,若包含当期开出的特码号则视为中奖。</li>
		    <li class="red">例如：特码包三选取特码号为：05,06,15。当期开出的特码号为06，则视为中奖。</li>
        </ul>
    </div>
</div>
</div>
<!-- m-nav over -->
</div>

<!-- alert-msg over -->

	<%-- <jsp:include page="/front/include/footer.jsp"></jsp:include> --%>
</body>
</html>
