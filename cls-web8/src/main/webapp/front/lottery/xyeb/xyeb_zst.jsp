<%@ page language="java" contentType="text/html; charset=UTF-8"
    import="com.team.lottery.vo.LotterySet,com.team.lottery.vo.BizSystem,java.math.BigDecimal"
    import="com.team.lottery.util.ApplicationContextUtil,com.team.lottery.enums.ELotteryTopKind"
    import="com.team.lottery.util.ConstantUtil,com.team.lottery.service.UserService,com.team.lottery.vo.User,
       com.team.lottery.system.BizSystemConfigVO,com.team.lottery.system.BizSystemConfigManager,
       com.team.lottery.vo.AwardModelConfig,com.team.lottery.system.SystemConfigConstant,com.team.lottery.cache.ClsCacheManager,com.team.lottery.enums.ELotteryKind"
    import="com.team.lottery.extvo.ZipConfig"
    pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	User user = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	Integer systemLotterySwitch = ClsCacheManager.getLotteryStatusByLotteryType("SUPER_SYSTEM", ELotteryKind.XYEB.getCode());
	// 业务系统彩种开关状态.
	Integer bizSystemLotterySwitch = ClsCacheManager.getLotteryStatusByLotteryType(user.getBizSystem(), ELotteryKind.XYEB.getCode());
	// 彩种开关状态不为1的时候就跳往首页.
	if((systemLotterySwitch != null && systemLotterySwitch != 1)||(bizSystemLotterySwitch != null && bizSystemLotterySwitch != 1)){
		response.sendRedirect(path+"/gameBet/index.html");
	}
%>  
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

<title>幸运28开奖历史</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/zst_liuhecai.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<!-- 业务js -->
<script type="text/javascript" src="<%=path%>/front/lottery/xyeb/js/xyeb_zst.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
</head>

<body>

<%--<div class="loading-bg"></div>
 <div class="loading">
    <img class="logo" src="<%=path%>/front/images/logo_loading.png" />
    <p class="title"><img src="<%=path%>/front/images/loading.gif" /></p> 
</div>--%>


<div class="header">
	<p class="title">彩种：<span>幸运28</span></p>
</div>
<div class="classify">
    <div class="child"><a id='nearest30Expect' name='nearestExpect'>近30期</a></div>
    <div class="child"><a id='nearest50Expect' name='nearestExpect'>近50期</a></div>
    <div class="child"><a id='nearest100Expect' name='nearestExpect'>近100期</a></div>
</div>

<div class="main" id='expectList'>
</div>

<a href="<%=path%>/gameBet/lottery/xyeb/xy28.html"><div class="btn2">返回幸运28</div></a>
</body>
</html>
