//通用的号码数组
var common_code_array = new Array(1, 2, 3, 4, 5, 6);
//和值
var hz_code_arrays = new Array(3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18);
//三同号数组
var sth_code_array = new Array('111', '222', '333', '444', '555', '666');
//二同号数组
var eth_code_array = new Array('11', '22', '33', '44', '55', '66');
//三同号通选
var sthtx_code = "111&222&333&444&555&666";
//三连号通选
var slhtx_code = "123&234&345&456";
//大小单双
var dxds_code_array = new Array(1, 2, 3, 4);

var editorStr = "说明：\n";
editorStr += "1、请参照标准格式样本格式录入或上传方案。\n";
editorStr += "2、每一注号码之间的间隔符支持 回车 空格[ ] 逗号[,] 分号[;] 冒号[:] 竖线 [|]。\n";
editorStr += "3、文件格式必须是.txt格式。\n";
editorStr += "4、文件较大时会导致上传时间较长，请耐心等待！\n";
editorStr += "5、将文件拖入文本框即可快速实现文件上传功能。\n";
editorStr += "6、导入文本内容后将覆盖文本框中现有的内容。";

//////////////////玩法//////////////////// 
//和值玩法
var HZ_PLAY = "<div class='classify-list-li' data-role-id='HZ'><p>和值</p></div>";

//大小单双玩法
var DXDS_PLAY = "<div class='classify-list-li' data-role-id='DXDS'><p>大小单双</p></div>";

//三同号通选玩法
var STHTX_PLAY = "<div class='classify-list-li' data-role-id='STHTX'><p>三同号通选</p></div>";

//三同号单选玩法
var STHDX_PLAY = "<div class='classify-list-li' data-role-id='STHDX'><p>三同号单选</p></div>";

//三不同号玩法
var SBTH_PLAY = "";
SBTH_PLAY += "<div class='classify-list-li' data-role-id='SBTHBZ'><p>三不同号：标准</p></div>";
SBTH_PLAY += "<div class='classify-list-li' data-role-id='SBTHDT'><p>三不同号：胆拖</p></div>";

//三连号通选玩法
var SLHTX_PLAY = "<div class='classify-list-li' data-role-id='SLHTX'><p>三连号通选</p></div>";

//二同号复选玩法
var ETHFX_PLAY = "<div class='classify-list-li' data-role-id='ETHFX'><p>二同号复选</p></div>";

//二同号单选玩法
var ETHDX_PLAY = "<div class='classify-list-li' data-role-id='ETHDX'><p>二同号单选</p></div>";

//二不同号玩法
var EBTH_PLAY = "";
EBTH_PLAY += "<div class='classify-list-li' data-role-id='EBTHBZ'><p>二不同号：标准</p></div>";
EBTH_PLAY += "<div class='classify-list-li' data-role-id='EBTHDT'><p>二不同号：胆拖</p></div>";


//猜一个就中奖玩法
var CYG_PLAY = "<div class='classify-list-li' data-role-id='CYG'><p>猜一个</p></div>";

//////////////////玩法描述////////////////////
//和值
var HZ_DES = "请稍后........";

//大小单双
var DXDS_DES = "请稍后........";

//三同号通选,三同号单选玩法
var STHTX_DES, STHDX_DES = "请稍后........";

//三不同号标准玩法, 三不同号胆拖玩法, 三连号通选玩法
var SBTHBZ_DES, SBTHDT_DES, SLHTX_DES = "请稍后........";

//二同号复选玩法, 二同号单选玩法
var ETHFX_DES, ETHDX_DES = "请稍后........";

//二不同号标准玩法, 二不同号胆拖玩法
var EBTHBZ_DES, EBTHDT_DES = "请稍后........";

//猜一个就中奖
var CYG_DES = "请稍后........";


function LotteryDataDeal() {
}

var lottertyDataDeal = new LotteryDataDeal();

lottertyDataDeal.param = {
  isRxDsAllow: true,  //是否允许任选单式,由位数来进行控制
  dsAllowCount: 0,
};


/**
 * 加载所有的玩法
 */
LotteryDataDeal.prototype.loadLotteryKindPlay = function () {
  /*	$("#HZ").append(HZ_PLAY);
    $("#DXDS").append(DXDS_PLAY);
    $("#STHTX").append(STHTX_PLAY);
    $("#STHDX").append(STHDX_PLAY);
    $("#SLHTX").append(SLHTX_PLAY);
    $("#ETHFX").append(ETHFX_PLAY);
    $("#ETHDX").append(ETHDX_PLAY);
    $("#CYG").append(CYG_PLAY);*/
  $("#EBTH").append(EBTH_PLAY);
  $("#SBTH").append(SBTH_PLAY);
}

/**
 * 玩法选择
 * @param roleId
 */
LotteryDataDeal.prototype.showPlayDes = function (roleId) {

  lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryCount = 0;
  lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codes = null;

  if (roleId == 'HZ') {
    $("#playDes").html(HZ_DES);
  } else if (roleId == 'DXDS') {
    $("#playDes").html(DXDS_DES);
  } else if (roleId == 'STHTX') {
    $("#playDes").html(STHTX_DES);
  } else if (roleId == 'STHDX') {
    $("#playDes").html(STHDX_DES);
  } else if (roleId == 'SBTHBZ') {
    $("#playDes").html(SBTHBZ_DES);
  } else if (roleId == 'SBTHDT') {
    $("#playDes").html(SBTHDT_DES);
  } else if (roleId == 'SLHTX') {
    $("#playDes").html(SLHTX_DES);
  } else if (roleId == 'ETHFX') {
    $("#playDes").html(ETHFX_DES);
  } else if (roleId == 'ETHDX') {
    $("#playDes").html(ETHDX_DES);
  } else if (roleId == 'EBTHBZ') {
    $("#playDes").html(EBTHBZ_DES);
  } else if (roleId == 'EBTHDT') {
    $("#playDes").html(EBTHDT_DES);
  } else if (roleId == 'CYG') {
    $("#playDes").html(CYG_DES);
  } else {
    alert("不可知的彩种玩法描述.");
  }
};

/**
 * 玩法选择
 *
 * @param roleId
 */
LotteryDataDeal.prototype.lotteryKindPlayChoose = function (roleId) {

  // 存储当前切换的玩法模式
  lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey = roleId;

  var desArray = new Array("选号区");
  var emptyDesArray = new Array("");
  if (roleId == 'HZ') {
    lottertyDataDeal.ballSectionCommon(0, 0, desArray, roleId, false, true);
    //获取赔率
    lottertyDataDeal.updateLotteryWinBySpecialKind();
  } else if (roleId == 'DXDS') {
    lottertyDataDeal.ballSectionCommon(0, 1, desArray, roleId, false, true);
  } else if (roleId == 'STHTX') {
    lottertyDataDeal.ballSectionCommon(0, -1, desArray, roleId, false, true);
  } else if (roleId == 'STHDX') {
    lottertyDataDeal.ballSectionCommon(0, -1, desArray, roleId, true);
  } else if (roleId == 'SBTHBZ') {
    lottertyDataDeal.ballSectionCommon(0, 0, desArray, null, true);
  } else if (roleId == 'SBTHDT') {
    var desArray = new Array("胆码", "拖码");
    lottertyDataDeal.ballSectionCommon(0, 1, desArray, roleId, true);
  } else if (roleId == 'SLHTX') {
    lottertyDataDeal.ballSectionCommon(0, -1, desArray, roleId, false, true);
  } else if (roleId == 'ETHFX') {
    lottertyDataDeal.ballSectionCommon(0, -1, desArray, roleId, true);
  } else if (roleId == 'ETHDX') {
    var desArray = new Array("同号", "不同号");
    lottertyDataDeal.ballSectionCommon(0, 1, desArray, roleId, true);
  } else if (roleId == 'EBTHBZ') {
    lottertyDataDeal.ballSectionCommon(0, 0, desArray, null, true);
  } else if (roleId == 'EBTHDT') {
    var desArray = new Array("胆码", "拖码");
    lottertyDataDeal.ballSectionCommon(0, 1, desArray, roleId, true);
  } else if (roleId == 'CYG') {
    lotteryCommonPage.recoverOmmitHotColdShow(); // 遗漏的显示
    lottertyDataDeal.ballSectionCommon(0, 0, desArray, null, true);
  } else {
    alert("不可知的彩种玩法.");
  }


  // 显示玩法名称
  var currentKindName = $(".classify-list-li[data-role-id='" + roleId + "']").find("p").text();
  if (isEmpty(currentKindName)) {
    currentKindName = $(".classify li[data-role-id='" + roleId + "']").find("p").text();
  }
  $("#currentKindName").text(currentKindName);

  // 显示玩法说明
  lottertyDataDeal.showPlayDes(roleId);

  // 星种的选中处理
  lotteryCommonPage.currentLotteryKindInstance.showCurrentPlayKindPage(roleId);

  lotteryCommonPage.dealForEvent(); // 拼接完，进行事件的重新申明
};


/**
 * 复式玩法的号码拼接
 * desArray 描述的数组
 * specialKind 特殊玩法
 * isOmitColdDataDeal 是否遗漏和冷热数据的处理
 */
LotteryDataDeal.prototype.ballSectionCommon = function (numCountStart, numCountEnd, desArray, specialKind, isOmitColdDataDeal, isShowLotteryWin) {
  var content = "";

  if (specialKind == null) {

    for (var i = numCountStart; i <= numCountEnd; i++) {
      if (i % 2 == 0) {
        content += "<li class='odd'>";
      } else {
        content += "<li class='even'>";
      }
      var numDes = desArray[i];
      if (isNotEmpty(numDes)) {
        content += "  <div class='l-title'><p>" + numDes + "</p>";
        //遗漏冷热元素处理
        if (isOmitColdDataDeal) {
          content += "<p class='l-msg'>当前遗漏</p>";
        }
        content += "</div>";
      }
      content += "<div class='l-mun'>";

      //0 - 9的号码
      for (var j = 0; j < common_code_array.length; j++) {
        content += "<div class='mun' data-realvalue='" + common_code_array[j] + "' name='ballNum_" + i + "_match'>" + common_code_array[j] + "";
        //遗漏冷热元素处理
        if (isOmitColdDataDeal) {
          content += "<p class='l-msg'>0</p>";
        }

        content += "</div>";
      }
      content += "</div>";
    }
  } else {

    var code_array = null;
    if (specialKind == "HZ") {
      //和值
      code_array = hz_code_arrays;
    } else if (specialKind == "ETHFX") {
      //二同号复选
      code_array = eth_code_array;
    } else if (specialKind == "STHDX") {
      //三同号单选
      code_array = sth_code_array;
    }

    if (code_array != null) {
      content += "<li class='odd'>";
      var numDes = desArray[0];
      if (isNotEmpty(numDes)) {
        content += "  <div class='l-title'><p>" + numDes + "</p>";
        //遗漏冷热元素处理
        if (isOmitColdDataDeal) {
          content += "<p class='l-msg'>当前遗漏</p>";
        }
        content += "</div>";
      }
      content += "<div class='l-mun'>";
      for (var j = 0; j < code_array.length; j++) {
        if (specialKind == "ETHFX" || specialKind == "STHDX") {
          if (j == 0) {
            content += "<div class='l-mun'>";
          }
          content += "<div class='mun long-mun' style='width:100px;background-size:100% 43px;'  data-realvalue='" + code_array[j] + "' name='ballNum_" + j + "_match'>" + code_array[j] + "";
        } else {
          if (j == 0) {
            content += "<div class='l-mun' style='width:420px'>";
          }
          content += "<div class='mun'  data-realvalue='" + code_array[j] + "' name='ballNum_0_match'>" + code_array[j] + "";
        }
        //遗漏冷热元素处理
        if (isOmitColdDataDeal) {
          content += "<p class='l-msg'>0</p>";
        }

        //赔率元素处理
        if (isShowLotteryWin) {
          content += "<p class='l-msg'>赔率：0.00</p>";
        }
        content += "</div>";
      }
      content += "</div>";
    } else if (specialKind == "EBTHDT" || specialKind == "SBTHDT" || specialKind == "ETHDX") {
      //胆拖 ，二同号单选

      for (var i = numCountStart; i <= numCountEnd; i++) {
        if (i % 2 == 0) {
          content += "<li class='odd'>";
        } else {
          content += "<li class='even'>";
        }
        var numDes = desArray[i];
        if (isNotEmpty(numDes)) {
          content += "  <div class='l-title'><p>" + numDes + "</p>";
          //遗漏冷热元素处理
          if (isOmitColdDataDeal) {
            content += "<p class='l-msg'>当前遗漏</p>";
          }
          content += "</div>";
        }
        content += "<div class='l-mun'>";
        if (specialKind == "ETHDX" && i != 1) {
          for (var j = 0; j < eth_code_array.length; j++) {
            //二同号单选--同号
            content += "<div class='mun' data-realvalue='" + eth_code_array[j] + "' name='ballNum_0_match'>" + eth_code_array[j] + "";
            //遗漏冷热元素处理
            if (isOmitColdDataDeal) {
              content += "<p class='l-msg'>0</p>";
            }
            content += "</div>";
          }
        }
        if (specialKind == "ETHDX" && i != 0 || specialKind != "ETHDX") {
          if (i == 0 && specialKind != "ETHDX") {
            for (var j = 0; j < common_code_array.length; j++) {
              content += "<div class='mun' data-realvalue='" + common_code_array[j] + "' name='ballNum_0_match'>" + common_code_array[j] + "";
              //遗漏冷热元素处理
              if (isOmitColdDataDeal) {
                content += "<p class='l-msg'>0</p>";
              }
              content += "</div>";
            }

          }
          if (i == 1 && specialKind != "ETHDX" || specialKind == "ETHDX") {
            for (var j = 0; j < common_code_array.length; j++) {
              content += "<div class='mun' data-realvalue='" + common_code_array[j] + "' name='ballNum_1_match'>" + common_code_array[j] + "";
              //遗漏冷热元素处理
              if (isOmitColdDataDeal) {
                content += "<p class='l-msg'>0</p>";
              }
              content += "</div>";
            }
          }

        }
        content += "</div>";
      }
    } else if (specialKind == "STHTX" || specialKind == "SLHTX") {
      //通选

      for (var i = 0; i < desArray.length; i++) {
        if (i % 2 == 0) {
          content += "<li class='odd'>";
        } else {
          content += "<li class='even'>";
        }
        var numDes = desArray[i];
        if (isNotEmpty(numDes)) {
          content += "  <div class='l-title'><p>" + numDes + "</p>";
          //遗漏冷热元素处理
          if (isOmitColdDataDeal) {
            content += "<p class='l-msg'>当前遗漏</p>";
          }

          content += "</div>";
        }
        content += "<div class='l-mun'>";
        if (specialKind == "STHTX") {
          if (isShowLotteryWin) {
            var STHTX_WIN = lottertyDataDeal.getKindPlayLotteryWin("STHTX");
          } else {
            STHTX_WIN = 0.00;
          }
          content += "<div class='mun long-mun' style='width:120px;background-size:100% 43px;' data-realvalue='1' name='ballNum_0_match'>三同号通选<p class='l-msg'>赔率：" + STHTX_WIN + "</p></div>";

        } else if (specialKind == "SLHTX") {
          if (isShowLotteryWin) {
            var SLHTX_WIN = lottertyDataDeal.getKindPlayLotteryWin("SLHTX");
          } else {
            STHTX_WIN = 0.00;
          }
          content += "<div class='mun long-mun' style='width:120px;background-size:100% 43px;' data-realvalue='1' name='ballNum_0_match'>三连号通选<p class='l-msg'>赔率：" + SLHTX_WIN + "</p></div>";

        }

        content += "</div>";
        content += "</div>";
      }
    } else if (specialKind == "DXDS") {
      for (var i = 0; i < desArray.length; i++) {
        if (i % 2 == 0) {
          content += "<li class='odd'>";
        } else {
          content += "<li class='even'>";
        }
        var numDes = desArray[i];
        if (isNotEmpty(numDes)) {
          content += "  <div class='l-title'><p>" + numDes + "</p>";
          //遗漏冷热元素处理
          if (isOmitColdDataDeal) {
            content += "<p class='l-msg'>当前遗漏</p>";
          }
          content += "</div>";
        }
        content += "<div class='l-mun'>";

        //1 - 4的号码
        for (var j = 0; j < dxds_code_array.length; j++) {
          content += "<div class='mun' data-realvalue='" + dxds_code_array[j] + "' name='ballNum_" + i + "_match'>";
          if (dxds_code_array[j] == 1) {
            content += "大";
          } else if (dxds_code_array[j] == 2) {
            content += "小";
          } else if (dxds_code_array[j] == 3) {
            content += "单";
          } else if (dxds_code_array[j] == 4) {
            content += "双";
          }
          //遗漏冷热元素处理
          if (isOmitColdDataDeal) {
            content += "<p class='l-msg'>0</p>";
          }
          //赔率元素处理
          if (isShowLotteryWin) {
            //大小单双
            var DXDS_WIN = lottertyDataDeal.getKindPlayLotteryWin("DXDS");
            content += "<p class='l-msg'>赔率：" + DXDS_WIN + "</p>";
          }
          content += "</div>";
        }
        content += "</div>";
      }
    } else {
      alert("未找到该玩法的号码.");
    }
  }
  $("#ballSection").html(content);
  //任选框隐藏
  $("#rxWeiCheck").hide();
  //隐藏单式的导入框
  $("#importDsBtn").hide();

  //进行遗漏冷热的数值处理
  if (isOmitColdDataDeal) {
    $("#ommitHotColdBtn").show();
    lotteryCommonPage.currentLotteryKindInstance.showOmmitData();  //默认显示遗漏数据
  } else {
    $("#ommitHotColdBtn").hide();
  }
};
/**
 * 计算当前用户最高模式下的奖金
 */
LotteryDataDeal.prototype.calucateCurrentUserHighestWin = function (kindKey) {
  if (lotteryCommonPage.lotteryParam.currentLotteryWins.size() == 0) {
    return "奖金未完全获取,请再次操作或者刷新页面";
  }
  var currentLotteryWin;
  currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(kindKey);
  if (currentLotteryWin == null) {
    return "未配置该玩法的奖金.";
  }
  var result = (currentLotteryWin.winMoney + (currentUserHighestModel - lowestAwardModel) * currentLotteryWin.specCode) * 1;
  return result;
};

/**
 * 获取当前用户最高模式下的奖金
 */
LotteryDataDeal.prototype.getKindPlayLotteryWin = function (kindKey) {
  if (lotteryCommonPage.lotteryParam.currentLotteryWins.size() == 0) {
    return "0.00";
  }
  var currentLotteryWin;
  currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get("KS_" + kindKey);
  if (currentLotteryWin == null) {
    return "未配置该玩法的奖金.";
  }
  var result = (currentLotteryWin.winMoney + (currentUserHighestModel - lowestAwardModel) * currentLotteryWin.specCode) * 1;
  return result.toFixed(lotteryCommonPage.param.toFixedValue);
};

/**
 * 获得赔率
 */
LotteryDataDeal.prototype.updateLotteryWinBySpecialKind = function () {
  $("#ballSection .odd .l-mun .mun").each(function () {
    code = $(this).attr("data-realvalue");
    lottertyDataDeal.getShowLotteryWin(code, $(this));
  });

}
/**
 * 显示赔率
 */
LotteryDataDeal.prototype.getShowLotteryWin = function (code, t) {
  //和值
  if (code == 3 || code == 18) {
    var HZ_WIN = lottertyDataDeal.getKindPlayLotteryWin("HZ");
    t.children().text("赔率：" + HZ_WIN);
  } else if (code == 4 || code == 17) {
    var HZ_WIN2 = lottertyDataDeal.getKindPlayLotteryWin("HZ_2");
    t.children().text("赔率：" + HZ_WIN2);
  } else if (code == 5 || code == 16) {
    var HZ_WIN3 = lottertyDataDeal.getKindPlayLotteryWin("HZ_3");
    t.children().text("赔率：" + HZ_WIN3);
  } else if (code == 6 || code == 15) {
    var HZ_WIN4 = lottertyDataDeal.getKindPlayLotteryWin("HZ_4");
    t.children().text("赔率：" + HZ_WIN4);
  } else if (code == 7 || code == 14) {
    var HZ_WIN5 = lottertyDataDeal.getKindPlayLotteryWin("HZ_5");
    t.children().text("赔率：" + HZ_WIN5);
  } else if (code == 8 || code == 13) {
    var HZ_WIN6 = lottertyDataDeal.getKindPlayLotteryWin("HZ_6");
    t.children().text("赔率：" + HZ_WIN6);
  } else if (code == 9 || code == 12) {
    var HZ_WIN7 = lottertyDataDeal.getKindPlayLotteryWin("HZ_7");
    t.children().text("赔率：" + HZ_WIN7);
  } else if (code == 10 || code == 11) {
    var HZ_WIN8 = lottertyDataDeal.getKindPlayLotteryWin("HZ_8");
    t.children().text("赔率：" + HZ_WIN8);
  }
}
/**
 * 获取当前用户最高模式下的奖金
 */
LotteryDataDeal.prototype.setKindPlayDes = function () {
  //获取赔率
  lottertyDataDeal.updateLotteryWinBySpecialKind();
  //和值
  var HZ_WIN = lottertyDataDeal.getKindPlayLotteryWin("HZ");
  var HZ_WIN2 = lottertyDataDeal.getKindPlayLotteryWin("HZ_2");
  var HZ_WIN3 = lottertyDataDeal.getKindPlayLotteryWin("HZ_3");
  var HZ_WIN4 = lottertyDataDeal.getKindPlayLotteryWin("HZ_4");
  var HZ_WIN5 = lottertyDataDeal.getKindPlayLotteryWin("HZ_5");
  var HZ_WIN6 = lottertyDataDeal.getKindPlayLotteryWin("HZ_6");
  var HZ_WIN7 = lottertyDataDeal.getKindPlayLotteryWin("HZ_7");
  var HZ_WIN8 = lottertyDataDeal.getKindPlayLotteryWin("HZ_8");
  HZ_DES = "<div class='title'>至少选择1个和值（3个号码之和）进行投注，所选和值与开奖的3个号码的和值相同即中奖，最高可中\"" + HZ_WIN + "\"元";
  HZ_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
  HZ_DES += "<div class='selct-demo-center' style='width: 190px';><p>投注：3</p><p>开奖：111(和值为3)</p><p>奖金：\"" + HZ_WIN + "\"元</p>" +
    "奖金对照表：<p>(1)和值：3,18  &nbsp;&nbsp;中奖：\"" + HZ_WIN + "\"元<br>" +
    "<p>(2)和值：4,17  &nbsp;&nbsp;中奖：\"" + HZ_WIN2 + "\"元</p>" +
    "<p>(3)和值：5,16  &nbsp;&nbsp;中奖：\"" + HZ_WIN3 + "\"元</p>" +
    "<p>(4)和值：6,15  &nbsp;&nbsp;中奖：\"" + HZ_WIN4 + "\"元</p>" +
    "<p>(5)和值：7,14  &nbsp;&nbsp;中奖：\"" + HZ_WIN5 + "\"元</p>" +
    "<p>(6)和值：8,13  &nbsp;&nbsp;中奖：\"" + HZ_WIN6 + "\"元</p>" +
    "<p>(7)和值：9,12  &nbsp;&nbsp;中奖：\"" + HZ_WIN7 + "\"元</p>" +
    "<p>(8)和值：10,11 &nbsp;&nbsp;中奖：\"" + HZ_WIN8 + "\"元</p>" +
    "</div></div>";

  //大小单双
  var DXDS_WIN = lottertyDataDeal.getKindPlayLotteryWin("DXDS");
  DXDS_DES = "<div class='title'>对开奖出来三位号码的和值,3-10为小,11-18为大,奇数为单,偶数为双，任意号码开出即中奖，单注奖金\"" + DXDS_WIN + "\"元";
  DXDS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
  DXDS_DES += "<div class='selct-demo-center' style='width: 130px'><p>投注：大</p><p>开奖：445(和值13为大)</p><p>奖金：\"" + DXDS_WIN + "\"元</p></div></div>";

  //三同号通选
  var STHTX_WIN = lottertyDataDeal.getKindPlayLotteryWin("STHTX");
  STHTX_DES = "<div class='title'>对所有相同的三个号码（111、222、333、444、555、666）进行投注，任意号码开出即中奖，单注奖金\"" + STHTX_WIN + "\"元";
  STHTX_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
  STHTX_DES += "<div class='selct-demo-center'><p>投注：三同号通选</p><p>开奖：111</p><p>奖金：\"" + STHTX_WIN + "\"元</p></div></div>";

  //三同号单选
  var STHDX_WIN = lottertyDataDeal.getKindPlayLotteryWin("STHDX");
  STHDX_DES = "<div class='title'>对相同的三个号码（111、222、333、444、555、666）中的任意一个进行投注，所选号码开出即中奖，单注奖金\"" + STHDX_WIN + "\"元";
  STHDX_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
  STHDX_DES += "<div class='selct-demo-center'><p>投注：111</p><p>开奖：111(不限顺序)</p><p>奖金：\"" + STHDX_WIN + "\"元</p></div></div>";

  //三不同号标准
  var SBTHBZ_WIN = lottertyDataDeal.getKindPlayLotteryWin("SBTHBZ");
  SBTHBZ_DES = "<div class='title'>从1～6中任选3个或多个号码，所选号码与开奖号码的3个号码相同即中奖，单注奖金\"" + SBTHBZ_WIN + "\"元";
  SBTHBZ_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
  SBTHBZ_DES += "<div class='selct-demo-center'><p>投注：123</p><p>开奖：123(不限顺序)</p><p>奖金：\"" + SBTHBZ_WIN + "\"元</p></div></div>";

  //三不同号胆拖
  var SBTHDT_WIN = lottertyDataDeal.getKindPlayLotteryWin("SBTHDT");
  SBTHDT_DES = "<div class='title'> 选1～2个胆码，选1～5个拖码，胆码加拖码不少于3个；选号与奖号相同即中奖，单注奖金\"" + SBTHDT_WIN + "\"元";
  SBTHDT_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
  SBTHDT_DES += "<div class='selct-demo-center'><p>投注：胆码(1)拖码(2,3)</p><p>开奖：123</p><p>奖金：\"" + SBTHDT_WIN + "\"元</p></div></div>";

  //三连号通选
  var SLHTX_WIN = lottertyDataDeal.getKindPlayLotteryWin("SLHTX");
  SLHTX_DES = "<div class='title'>对所有3个相连的号码（123、234、345、456）进行投注，任意号码开出即中奖，单注奖金\"" + SLHTX_WIN + "\"元";
  SLHTX_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
  SLHTX_DES += "<div class='selct-demo-center' style='width:110px;'><p>投注：三连号通选</p><p>开奖：123,234,345,456</p><p>(不限顺序)</p><p>奖金：\"" + SLHTX_WIN + "\"元</p></div></div>";

  //二同号复选
  var ETHFX_WIN = lottertyDataDeal.getKindPlayLotteryWin("ETHFX");
  ETHFX_DES = "<div class='title'>从11～66中任选1个或多个号码，选号与奖号（包含11～66，不限顺序）相同，即中奖\"" + ETHFX_WIN + "\"元";
  ETHFX_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
  ETHFX_DES += "<div class='selct-demo-center' style='width:110px;'><p>投注：11*</p><p>开奖：112,113,114,115,116</p>(不限顺序)</p><p>奖金：\"" + ETHFX_WIN + "\"元</p></div></div>";

  //二同号单选
  var ETHDX_WIN = lottertyDataDeal.getKindPlayLotteryWin("ETHDX");
  ETHDX_DES = "<div class='title'>选择1对相同号码（11,22,33,44,55,66）和1个不同号码(1,2,3,4,5,6)投注，选号与奖号相同（顺序不限），即中奖\"" + ETHDX_WIN + "\"元";
  ETHDX_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
  ETHDX_DES += "<div class='selct-demo-center'><p>投注：112</p><p>开奖：112(不限顺序)</p><p>奖金：\"" + ETHDX_WIN + "\"元</p></div></div>";

  //二不同号标准
  var EBTHBZ_WIN = lottertyDataDeal.getKindPlayLotteryWin("EBTHBZ");
  EBTHBZ_DES = "<div class='title'>从1～6中任选2个或多个号码，所选号码与开奖号码任意2个号码相同，即中奖\"" + EBTHBZ_WIN + "\"元";
  EBTHBZ_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
  EBTHBZ_DES += "<div class='selct-demo-center'><p>投注：12</p><p>开奖：12*(不限顺序)</p><p>奖金：\"" + EBTHBZ_WIN + "\"元</p></div></div>";

  //二不同号胆拖
  var EBTHDT_WIN = lottertyDataDeal.getKindPlayLotteryWin("EBTHDT");
  EBTHDT_DES = "<div class='title'>选1个胆码，选1～5个拖码，胆码加拖码不少于2个；选号与奖号任意2号相同即中奖，单注奖金\"" + EBTHDT_WIN + "\"元";
  EBTHDT_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
  EBTHDT_DES += "<div class='selct-demo-center'><p>投注：胆码:1拖码:2,3</p><p>开奖：123</p><p>奖金：\"" + EBTHDT_WIN + "\"元</p></div></div>";

  //猜一个就中奖
  var CYG_WIN = lottertyDataDeal.getKindPlayLotteryWin("CYG");
  CYG_DES = "<div class='title'>选择1个您认为一定会开出的号码，所选号码在开奖号码中出现，即中奖金\"" + CYG_WIN + "\"元";
  CYG_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
  CYG_DES += "<div class='selct-demo-center' style='width:80px;'><p>投注：1</p><p>开奖：111,166,215</p><p>(开奖号码有1)</p><p>奖金：\"" + CYG_WIN + "\"元</p></div></div>";


  //展示当前玩法的描述
  lottertyDataDeal.showPlayDes(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey);

  //选号示例
  $(".exampleButton").unbind("mouseover").mouseover(function () {
    $(this).next().show();
  });
  $(".exampleButton").unbind("mouseout").mouseout(function () {
    $(this).next().hide();
  });
};

/**
 * 单式方案控制
 */
LotteryDataDeal.prototype.dsPositionCheckFunc = function (obj) {
  var positionCount = 0;
  $("input[name='ds_position']").each(function () {
    if (this.checked) {
      positionCount++;
    }
  });

  if (lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey == 'RXSIZXDS') {
    //判断当前是否符合该单式的处理方式
    if (positionCount == 4) {  //选择4位的时候,有一个方案
      lottertyDataDeal.param.isRxDsAllow = true;
      lottertyDataDeal.param.dsAllowCount = 1;
      $("#pnum").text(positionCount);
      $("#fnum").text(1);
    } else if (positionCount == 5) {
      lottertyDataDeal.param.isRxDsAllow = true;
      lottertyDataDeal.param.dsAllowCount = 5;
      $("#pnum").text(positionCount);
      $("#fnum").text(5);
    } else {
      lottertyDataDeal.param.isRxDsAllow = false;
      lottertyDataDeal.param.dsAllowCount = 0;
      $("#pnum").text(positionCount);
      $("#fnum").text(0);
    }
  } else if (lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey == 'RXEZXDS') {
    //判断当前是否符合该单式的处理方式
    if (positionCount == 2) {  //选择2位的时候,有一个方案
      lottertyDataDeal.param.isRxDsAllow = true;
      lottertyDataDeal.param.dsAllowCount = 1;
      $("#pnum").text(positionCount);
      $("#fnum").text(1);
    } else if (positionCount == 3) {
      lottertyDataDeal.param.isRxDsAllow = true;
      lottertyDataDeal.param.dsAllowCount = 3;
      $("#pnum").text(positionCount);
      $("#fnum").text(3);
    } else if (positionCount == 4) {
      lottertyDataDeal.param.isRxDsAllow = true;
      lottertyDataDeal.param.dsAllowCount = 6;
      $("#pnum").text(positionCount);
      $("#fnum").text(6);
    } else if (positionCount == 5) {
      lottertyDataDeal.param.isRxDsAllow = true;
      lottertyDataDeal.param.dsAllowCount = 10;
      $("#pnum").text(positionCount);
      $("#fnum").text(10);
    } else {
      lottertyDataDeal.param.isRxDsAllow = false;
      lottertyDataDeal.param.dsAllowCount = 0;
      $("#pnum").text(positionCount);
      $("#fnum").text(0);
    }
  } else if (lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey == 'RXSZXDS') {
    //判断当前是否符合该单式的处理方式
    if (positionCount == 3) {  //选择2位的时候,有一个方案
      lottertyDataDeal.param.isRxDsAllow = true;
      lottertyDataDeal.param.dsAllowCount = 1;
      $("#pnum").text(positionCount);
      $("#fnum").text(1);
    } else if (positionCount == 4) {
      lottertyDataDeal.param.isRxDsAllow = true;
      lottertyDataDeal.param.dsAllowCount = 4;
      $("#pnum").text(positionCount);
      $("#fnum").text(4);
    } else if (positionCount == 5) {
      lottertyDataDeal.param.isRxDsAllow = true;
      lottertyDataDeal.param.dsAllowCount = 10;
      $("#pnum").text(positionCount);
      $("#fnum").text(10);
    } else {
      lottertyDataDeal.param.isRxDsAllow = false;
      lottertyDataDeal.param.dsAllowCount = 0;
      $("#pnum").text(positionCount);
      $("#fnum").text(0);
    }
  } else {
    alert("该直选单式未配置");
  }

  //格式化,统计该单式数目
  lotteryCommonPage.dealForDsFormat("mouseout");
};



