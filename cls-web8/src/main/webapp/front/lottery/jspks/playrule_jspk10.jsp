<%@ page language="java" contentType="text/html; charset=UTF-8"
    import="com.team.lottery.vo.LotterySet,com.team.lottery.vo.BizSystem,java.math.BigDecimal"
    import="com.team.lottery.util.ApplicationContextUtil,com.team.lottery.enums.ELotteryTopKind"
    import="com.team.lottery.util.ConstantUtil,com.team.lottery.service.UserService,com.team.lottery.vo.User,
       com.team.lottery.system.BizSystemConfigVO,com.team.lottery.system.BizSystemConfigManager,
       com.team.lottery.vo.AwardModelConfig,com.team.lottery.system.SystemConfigConstant,com.team.lottery.cache.ClsCacheManager"
    import="com.team.lottery.extvo.ZipConfig"
    pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
%>        
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>玩法介绍-极速PK10</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/play_rule.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
</head>

<body>

<%-- <jsp:include page="/front/include/header_user.jsp"></jsp:include>

<!-- 需要加载的图片 -->
<div class="stance"></div> --%>

<div class="main-ml0">
<!-- m-banner -->
<div class="m-banner" style="background-image:url(<%=path%>/front/images/login/banner.jpg);">
    <div class="content" ><img src="<%=path%>/front/images/play_rule.png" /><span>极速PK10</span></div>
</div>
<!-- m-banner over -->

<!-- m-nav -->
<div class="m-nav container">
<div class="container">
    <div class="block">
        <h2>一般规则说明</h2>
        <ul>
            <li>1. 客户一经在本公司开户或投注，即被视为已接受这些规则。</li>
            <li>2. 如果客户怀疑自己的资料被盗用，应立即通知本公司，并更改个人详细资料，且之前所使用的使用者名称及密码将全部无效。</li>
            <li>3. 客户有责任确保自己的帐户及登入资料的保密性。在本网站上以个人的使用者名称及密码进行的任何网上投注将被视为有效。</li>
            <li>4. 为了避免出现争议，请务必在下注之后检查"下注状况"。</li>
            <li>5. 不论任何原因，开奖之后所接受的投注，将一律被视为"无效"。</li>
            <li>6. 任何的投诉必须在开奖之前提出，本公司将不会受理任何开奖之后的投诉。</li>
            <li>7. 公布的投注时间、项目及赔率出现任何打字错误或非故意人为失误，本公司将保留改正错误和按正确搅珠时间、赔率结算注单的权力。</li>
            <li>8. 公布之所有赔率为浮动赔率，派彩时的赔率将以本公司确认投注时之赔率为准。</li>
            <li>9. 如本公司察觉客户投注状况异常时，有权即时中止客户投注并删除不正常注单；在本公司中止下注前，客户之所有正常投注仍属有效，不得要求取消、延迟交收及不得有任何异议。</li>
            <li>10. 如因在本网站投注触犯当地法律，本公司概不负责。</li>
        </ul>
    </div>
    <div class="block">
        <h2>游戏规则说明</h2>
        <ul>
        	<li class="red">以下所有投注皆含本金。</li>
        	<li class="strong">简介</li>
		    <li>极速PK10：简称“PK拾”，由中国福利彩票发行管理中心统一发行， 由北京市福利彩票发行中心承销。</li>
		    <li>开奖时间：每5分钟1期；每天早上9点00分至晚上23点55分；</li>
		    <li>开奖号码：开奖结果为10个号码。</li>
		    <li class="strong">玩法</li>
		    <li><img src="<%=path%>/front/images/lottery_play/jspk10.png" /></li>
        </ul>
    </div>
</div>
</div>
<!-- m-nav over -->
</div>

<!-- alert-msg over -->

	<%-- <jsp:include page="/front/include/footer.jsp"></jsp:include> --%>
</body>
</html>
