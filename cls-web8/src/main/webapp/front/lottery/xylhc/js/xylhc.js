function LhcPage() {
}
var lhcPage = new LhcPage();
lotteryCommonPage.currentLotteryKindInstance = lhcPage; // 将这个投注实例传至common js当中

// 基本参数
lhcPage.param = {
	unitPrice : 1, // 每注价格
	kindDes : "六合彩",
	kindName : 'XYLHC', // 彩种
	kindNameType : 'XYLHC', // 大彩种拼写
	openMusic : true, // 开奖音乐开关
	oldLotteryNum : null,
	ommitData : null, // 遗漏数据
	hotColdData : null
// 冷热数据
};

// 投注参数
lhcPage.lotteryParam = {
	lotteryKindMap : new JS_OBJECT_MAP(),
	currentKindKey : 'TMB', // 当前所选择的玩法模式,默认是特码B
	currentLotteryCount : 0, // 当前投注数目
	currentLotteryPrice : 0, // 当前投注价格,按元来计算
	currentLotteryTotalCount : 0, // 当前总投注数目
	codes : null, // 投注号码
	codesStastic : new Array()
};

// 数字滚动效果 jQuery插件 参数
lhcPage.numScrollParam = {
	from : [],
	width : 36,
	height : 36,
	to : [],
	scrolled : false,
	colors : []
};

$(document).ready(function() {

	// 初始化玩法的投注号码数
	lhcPage.initModelNun();
	// 初始化号码描述缓存
	lhcPage.initPlayCodeDesc();
	// 初始化生肖号码
	lhcPage.initShengXiaoNumber();
	// $(".m-banner .b-child4 .more1").unbind("click").click(function(){
	// var html='<ul class="c-list">'+
	// '<li class="child child1" >20150815-049</li>'+
	// '<li class="child child2"
	// style="width:162px;">06,05,04,09,02,01+01</li>'+
	// '</ul>';
	// $(".m-banner .b-child4 .c-content
	// .c-list:eq(0)").slideUp(500,function(){$(this).remove();$(".m-banner
	// .b-child4 .c-content").append(html);});
	// });
	// $(".m-banner .b-child4 .more2").unbind("click").click(function(){
	// var html='<ul class="c-list" style="display:none;">'+
	// '<li class="child child1" >20150815-049</li>'+
	// '<li class="child child2"
	// style="width:162px;">06,05,04,09,02,01+01</li>'+
	// '</ul>';
	// $(".m-banner .b-child4 .c-content .c-title").after(html);
	// $(".m-banner .b-child4 .c-content .c-list:eq(0)").slideDown(500);
	// $(".m-banner .b-child4 .c-content .c-list:last").remove();;
	// });

	// $(".m-banner .b-child1 .btn").click(function(){
	// $(".b-child-list").stop(false,true).slideToggle(500);
	// });
	// $(".main .main-content .main-title .right span").click(function(){
	// $(".main .main-content .main-title .right span").removeClass("on");
	// $(this).addClass("on");
	// });

	/* select */
	// $(".choose .choose-foot .select").click(function(){
	// var element=$(".select").not($(this));
	// element.find(".select-list").hide();
	// $(this).find(".select-list").toggle();
	// });
	// $(".choose .choose-foot .select .select-list li").click(function(){
	// var parent_select=$(this).closest(".select-list");
	// parent_select.find("li").removeClass("on");
	// $(this).addClass("on");
	// var parent=$(this).closest(".select");
	// var val=$(this).html();
	// parent.find(".select-title").val(val);
	// parent.find(".select-title").html(val);
	// });
	// //增减倍数
	// $(".choose .choose-title .multiple-select
	// .multiple-btn.sub").click(function(){
	// var val=parseInt($(".choose .choose-title .multiple-select
	// .inputText").val());
	// var pic=parseInt($(".choose .choose-title .multiple-select
	// .inputText").attr("data-price"));
	// if(val==0){return;}else{val--;}pic=pic*val;
	// $(".choose .choose-title .multiple-select .inputText").val(val);
	// $(".choose .choose-title .all-multiple").html(pic.toFixed(2));
	// });
	// $(".choose .choose-title .multiple-select
	// .multiple-btn.add").click(function(){
	// var val=parseInt($(".choose .choose-title .multiple-select
	// .inputText").val());
	// var pic=parseInt($(".choose .choose-title .multiple-select
	// .inputText").attr("data-price"));
	// val++;pic=pic*val;
	// $(".choose .choose-title .multiple-select .inputText").val(val);
	// $(".choose .choose-title .all-multiple").html(pic.toFixed(2));
	// });

	$(".main .classify li").mouseover(function() {
		var w = 0;
		$(this).find(".classify-list .classify-list-li p").each(function(ii, nn) {
			var _w = $(nn).width();
			w = _w > w ? _w : w;
		});
		var ww = w + 24;
		$(this).find(".classify-list").css({
			"width" : ww + "px",
			"margin-left" : -ww / 2 + "px"
		})
	});

	// 开奖效果
	setInterval(lhcPage.lottory_open, 60000);
	// 选择默认的玩法，默认是五星复式
	lotteryDataDeal.lotteryKindPlayChoose(lhcPage.lotteryParam.currentKindKey);

	// 点击查看金额
	$(".quick-money img").click(function() {
		var value = $(this).data("value");
		quickmoney = Number(quickmoney) + Number(value);
		$(".shortcut-money").val(quickmoney);
		lotteryDataDeal.colsChildSet();
	});

	$(".btn-rule").click(function() {
		// 弹窗
		// layer.open({
		// type: 2,
		// title: '玩法说明',
		// maxmin: false,
		// shadeClose: true,
		// 点击遮罩关闭层
		// area: ['1200px', '800px'],
		// content: 'playrule_lhc.html'
		// })
		var tempWindow = window.open('', '', 'top=100,left=100,width=1200,height=800');
		tempWindow.location.href = 'playrule_lhc.html'; // 对新打开的页面进行重定向
	});
});

/**
 * 初始化号码描述
 */
LhcPage.prototype.initPlayCodeDesc = function() {

	$.ajax({
		type : "POST",
		url : contextPath + "/lottery/initLotteryWinLhcDesc",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				lotteryDataDeal.playNumDescMap = result.data;
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
				$("#editUserButton").removeAttr("disabled");
			}
		}
	});
}

/**
 * 初始化生肖号码描述
 */
LhcPage.prototype.initShengXiaoNumber = function() {

	$.ajax({
		type : "POST",
		url : contextPath + "/code/getShengXiaoNumber",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				lotteryDataDeal.shengxiao.shengxiaoArr = result.data;
				lotteryDataDeal.shengxiao.jiqin = result.data2;
				lotteryDataDeal.shengxiao.yeshou = result.data3;

				// 获取最新的开奖号码
				lotteryCommonPage.getLastLotteryCode();
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
				$("#editUserButton").removeAttr("disabled");
			}
		}
	});
}
/**
 * 初始化号码玩法
 */
LhcPage.prototype.initModelNun = function() {

	// maxNum:每个玩法的最大号码，lotteryCodeNum：每注需要的号码数
	// 特码
	lotteryDataDeal.modelMap.put("TMA", {
		maxNum : 66,
		lotteryCodeNum : 1,
		playDesc : "特码-特码A"
	});
	lotteryDataDeal.modelMap.put("TMB", {
		maxNum : 66,
		lotteryCodeNum : 1,
		playDesc : "特码-特码B"
	});
	// 正码
	lotteryDataDeal.modelMap.put("ZMA", {
		maxNum : 53,
		lotteryCodeNum : 1,
		playDesc : "正码-正码A"
	});
	lotteryDataDeal.modelMap.put("ZMB", {
		maxNum : 53,
		lotteryCodeNum : 1,
		playDesc : "正码-正码B"
	});
	// 正码特
	lotteryDataDeal.modelMap.put("ZMT1", {
		maxNum : 58,
		lotteryCodeNum : 1,
		playDesc : "正码特-正一特"
	});
	lotteryDataDeal.modelMap.put("ZMT2", {
		maxNum : 58,
		lotteryCodeNum : 1,
		playDesc : "正码特-正二特"
	});
	lotteryDataDeal.modelMap.put("ZMT3", {
		maxNum : 58,
		lotteryCodeNum : 1,
		playDesc : "正码特-正三特"
	});
	lotteryDataDeal.modelMap.put("ZMT4", {
		maxNum : 58,
		lotteryCodeNum : 1,
		playDesc : "正码特-正四特"
	});
	lotteryDataDeal.modelMap.put("ZMT5", {
		maxNum : 58,
		lotteryCodeNum : 1,
		playDesc : "正码特-正五特"
	});
	lotteryDataDeal.modelMap.put("ZMT6", {
		maxNum : 58,
		lotteryCodeNum : 1,
		playDesc : "正码特-正六特"
	});
	// 正码1-6
	lotteryDataDeal.modelMap.put("ZM16", {
		maxNum : 78,
		lotteryCodeNum : 1,
		playDesc : "正码1-6"
	});
	// 连码
	lotteryDataDeal.modelMap.put("LMSQZ", {
		maxNum : 49,
		lotteryCodeNum : 3,
		playDesc : "连码-三全中"
	});
	lotteryDataDeal.modelMap.put("LMSZ22", {
		maxNum : 49,
		lotteryCodeNum : 3,
		playDesc : "连码-三中二之中二"
	});
	lotteryDataDeal.modelMap.put("LMSZ23", {
		maxNum : 49,
		lotteryCodeNum : 3,
		playDesc : "连码-三中二之中三"
	});
	lotteryDataDeal.modelMap.put("LMEQZ", {
		maxNum : 49,
		lotteryCodeNum : 2,
		playDesc : "连码-二全中"
	});
	lotteryDataDeal.modelMap.put("LMEZT", {
		maxNum : 49,
		lotteryCodeNum : 2,
		playDesc : "连码-二中特中之特"
	});
	lotteryDataDeal.modelMap.put("LMEZ2", {
		maxNum : 49,
		lotteryCodeNum : 2,
		playDesc : "连码-二中特中之二"
	});
	lotteryDataDeal.modelMap.put("LMTC", {
		maxNum : 49,
		lotteryCodeNum : 2,
		playDesc : "连码-特串"
	});
	lotteryDataDeal.modelMap.put("LMSZ1", {
		maxNum : 49,
		lotteryCodeNum : 4,
		playDesc : "连码-四中一"
	});
	// 半波
	lotteryDataDeal.modelMap.put("BB", {
		maxNum : 18,
		lotteryCodeNum : 1,
		playDesc : "半波"
	});
	// 一肖尾数
	lotteryDataDeal.modelMap.put("YXWS", {
		maxNum : 22,
		lotteryCodeNum : 1,
		playDesc : "一肖/尾数"
	});
	// 特码生肖
	lotteryDataDeal.modelMap.put("TMSX", {
		maxNum : 12,
		lotteryCodeNum : 1,
		playDesc : "特码生肖"
	});
	// 合肖
	lotteryDataDeal.modelMap.put("HX2", {
		maxNum : 12,
		lotteryCodeNum : 2,
		playDesc : "合肖-二肖"
	});
	lotteryDataDeal.modelMap.put("HX3", {
		maxNum : 12,
		lotteryCodeNum : 3,
		playDesc : "合肖-三肖"
	});
	lotteryDataDeal.modelMap.put("HX4", {
		maxNum : 12,
		lotteryCodeNum : 4,
		playDesc : "合肖-四肖"
	});
	lotteryDataDeal.modelMap.put("HX5", {
		maxNum : 12,
		lotteryCodeNum : 5,
		playDesc : "合肖-五肖"
	});
	lotteryDataDeal.modelMap.put("HX6", {
		maxNum : 12,
		lotteryCodeNum : 6,
		playDesc : "合肖-六肖"
	});
	lotteryDataDeal.modelMap.put("HX7", {
		maxNum : 12,
		lotteryCodeNum : 7,
		playDesc : "合肖-七肖"
	});
	lotteryDataDeal.modelMap.put("HX8", {
		maxNum : 12,
		lotteryCodeNum : 8,
		playDesc : "合肖-八肖"
	});
	lotteryDataDeal.modelMap.put("HX9", {
		maxNum : 12,
		lotteryCodeNum : 9,
		playDesc : "合肖-九肖"
	});
	lotteryDataDeal.modelMap.put("HX10", {
		maxNum : 12,
		lotteryCodeNum : 10,
		playDesc : "合肖-十肖"
	});
	lotteryDataDeal.modelMap.put("HX11", {
		maxNum : 12,
		lotteryCodeNum : 11,
		playDesc : "合肖-十一肖"
	});
	// 连肖
	lotteryDataDeal.modelMap.put("LXELZ", {
		maxNum : 12,
		lotteryCodeNum : 2,
		playDesc : "连肖-二肖连中"
	});
	lotteryDataDeal.modelMap.put("LXSLZ", {
		maxNum : 12,
		lotteryCodeNum : 3,
		playDesc : "连肖-三肖连中"
	});
	lotteryDataDeal.modelMap.put("LXSILZ", {
		maxNum : 12,
		lotteryCodeNum : 4,
		playDesc : "连肖-四肖连中"
	});
	lotteryDataDeal.modelMap.put("LXWLZ", {
		maxNum : 12,
		lotteryCodeNum : 5,
		playDesc : "连肖-五肖连中"
	});
	lotteryDataDeal.modelMap.put("LXELBZ", {
		maxNum : 12,
		lotteryCodeNum : 2,
		playDesc : "连肖-二肖连不中"
	});
	lotteryDataDeal.modelMap.put("LXSLBZ", {
		maxNum : 12,
		lotteryCodeNum : 3,
		playDesc : "连肖-三肖连不中"
	});
	lotteryDataDeal.modelMap.put("LXSILBZ", {
		maxNum : 12,
		lotteryCodeNum : 4,
		playDesc : "连肖-四肖连不中"
	});

	// 尾数连
	lotteryDataDeal.modelMap.put("WSLELZ", {
		maxNum : 10,
		lotteryCodeNum : 2,
		playDesc : "尾数连-二尾连中"
	});
	lotteryDataDeal.modelMap.put("WSLSLZ", {
		maxNum : 10,
		lotteryCodeNum : 3,
		playDesc : "尾数连-三尾连中"
	});
	lotteryDataDeal.modelMap.put("WSLSILZ", {
		maxNum : 10,
		lotteryCodeNum : 4,
		playDesc : "尾数连-四尾连中"
	});
	lotteryDataDeal.modelMap.put("WSLELBZ", {
		maxNum : 10,
		lotteryCodeNum : 2,
		playDesc : "尾数连-二尾连不中"
	});
	lotteryDataDeal.modelMap.put("WSLSLBZ", {
		maxNum : 10,
		lotteryCodeNum : 3,
		playDesc : "尾数连-三尾连不中"
	});
	lotteryDataDeal.modelMap.put("WSLSILBZ", {
		maxNum : 10,
		lotteryCodeNum : 4,
		playDesc : "尾数连-四尾连不中"
	});
	// 全不中
	lotteryDataDeal.modelMap.put("QBZ5", {
		maxNum : 49,
		lotteryCodeNum : 5,
		playDesc : "全不中-五不中"
	});
	lotteryDataDeal.modelMap.put("QBZ6", {
		maxNum : 49,
		lotteryCodeNum : 6,
		playDesc : "全不中-六不中"
	});
	lotteryDataDeal.modelMap.put("QBZ7", {
		maxNum : 49,
		lotteryCodeNum : 7,
		playDesc : "全不中-七不中"
	});
	lotteryDataDeal.modelMap.put("QBZ8", {
		maxNum : 49,
		lotteryCodeNum : 8,
		playDesc : "全不中-八不中"
	});
	lotteryDataDeal.modelMap.put("QBZ9", {
		maxNum : 49,
		lotteryCodeNum : 9,
		playDesc : "全不中-九不中"
	});
	lotteryDataDeal.modelMap.put("QBZ10", {
		maxNum : 49,
		lotteryCodeNum : 10,
		playDesc : "全不中-十不中"
	});
	lotteryDataDeal.modelMap.put("QBZ11", {
		maxNum : 49,
		lotteryCodeNum : 11,
		playDesc : "全不中-十一不中"
	});
	lotteryDataDeal.modelMap.put("QBZ12", {
		maxNum : 49,
		lotteryCodeNum : 12,
		playDesc : "全不中-十二不中"
	});
}

/**
 * 展示当前正在投注的期号
 */
LhcPage.prototype.showCurrentLotteryCode = function(lotteryIssue) {
	if (lotteryIssue != null) {
		var openCodeStr = lhcPage.getOpenCodeStr(lotteryIssue.lotteryNum);
		$('#nowNumber').text(openCodeStr);
	}
};

/**
 * 获取开奖号码前台展现的字符串
 */
LhcPage.prototype.getOpenCodeStr = function(openCodeNum) {
	var openCodeStr = "";
	if (isNotEmpty(openCodeNum)) {
		var expectDay = openCodeNum.substring(0, openCodeNum.length - 3);
		var expectNum = openCodeNum.substring(openCodeNum.length - 3, openCodeNum.length);
		openCodeStr = expectDay + "-" + expectNum;
	}
	return openCodeStr;
};

/**
 * 展示最新的开奖号码
 */
LhcPage.prototype.showLastLotteryCode = function(lotteryCode) {
	if (lotteryCode != null) {
		var lotteryNum = lotteryCode.lotteryNum;
		var openCodeStr = lhcPage.getOpenCodeStr(lotteryCode.lotteryNum);
		var colors = lhcPage.changeColorByCode(lotteryCode);
		$('#J-lottery-info-lastnumber').text(openCodeStr);
		// $("#lotteryNumber1").attr("class",colors[0]);
		// $("#lotteryNumber2").attr("class",colors[1]);
		// $("#lotteryNumber3").attr("class",colors[2]);
		// $("#lotteryNumber4").attr("class",colors[3]);
		// $("#lotteryNumber5").attr("class",colors[4]);
		// $("#lotteryNumber6").attr("class",colors[5]);
		// $("#lotteryNumber7").attr("class",colors[6]);

		// $('#lotteryNumber1').text(lotteryCode.numInfo1);
		// $('#lotteryNumber2').text(lotteryCode.numInfo2);
		// $('#lotteryNumber3').text(lotteryCode.numInfo3);
		// $('#lotteryNumber4').text(lotteryCode.numInfo4);
		// $('#lotteryNumber5').text(lotteryCode.numInfo5);
		// $('#lotteryNumber6').text(lotteryCode.numInfo6);
		// $('#lotteryNumber7').text(lotteryCode.numInfo7);

		// code滚动
		var startCodes = [ 8, 16, 48, 4, 19, 32, 27 ];
		var resultCodes = lotteryCode.opencodeStr.split(',');// ["03", "24",
																// "33", "26",
																// "11", "12",
																// "13"]
		var colors = lhcPage.getColorsFrom1_49();
		if (lhcPage.param.oldLotteryNum != lotteryNum) {
			lhcPage.numScrollParam.scrolled = false
		}

		if (lhcPage.numScrollParam.scrolled == false) {
			lhcPage.scroll(startCodes, resultCodes, colors);
			lhcPage.numScrollParam.scrolled = true;
		}

		$('#lotteryNumber1').text(lhcPage.returnAninal(lotteryCode.numInfo1));
		$('#lotteryNumber2').text(lhcPage.returnAninal(lotteryCode.numInfo2));
		$('#lotteryNumber3').text(lhcPage.returnAninal(lotteryCode.numInfo3));
		$('#lotteryNumber4').text(lhcPage.returnAninal(lotteryCode.numInfo4));
		$('#lotteryNumber5').text(lhcPage.returnAninal(lotteryCode.numInfo5));
		$('#lotteryNumber6').text(lhcPage.returnAninal(lotteryCode.numInfo6));
		$('#lotteryNumber7').text(lhcPage.returnAninal(lotteryCode.numInfo7));

		// 播放(继续播放)
		if (lotteryCommonPage.param.isFirstOpenMusic) {
			if (lhcPage.param.openMusic && lhcPage.param.oldLotteryNum != lotteryNum && lhcPage.param.oldLotteryNum != null) {
				document.getElementById("bgMusic").play();
			}
			lhcPage.param.oldLotteryNum = lotteryNum;
		}
		lotteryCommonPage.param.isFirstOpenMusic = true;

	}
};

LhcPage.prototype.returnAninal = function(opencode) {
	var shengxiaoArr = lotteryDataDeal.shengxiao.shengxiaoArr;
	for (var i = 0; i < shengxiaoArr.length; i++) {
		for (var j = 0; j < shengxiaoArr[i].length; j++) {
			if (Number(opencode) == Number(shengxiaoArr[i][j])) {
				return lotteryDataDeal.shengxiao.shengxiaoKey[i];
			}
		}
	}
	return "-";
}

// 计算颜色
LhcPage.prototype.changeColorByCode = function(lotteryCode) {
	var colors = new Array();
	var hong = lotteryDataDeal.boshe.hongbo;
	var lan = lotteryDataDeal.boshe.lanbo;
	var lv = lotteryDataDeal.boshe.lvbo;

	// 判断是否是红波
	for (var i = 0; i < hong.length; i++) {
		if (Number(hong[i]) == Number(lotteryCode.numInfo1)) {
			colors[0] = 'btn btn-red';
		}
		if (Number(hong[i]) == Number(lotteryCode.numInfo2)) {
			colors[1] = 'btn btn-red';
		}
		if (Number(hong[i]) == Number(lotteryCode.numInfo3)) {
			colors[2] = 'btn btn-red';
		}
		if (Number(hong[i]) == Number(lotteryCode.numInfo4)) {
			colors[3] = 'btn btn-red';
		}
		if (Number(hong[i]) == Number(lotteryCode.numInfo5)) {
			colors[4] = 'btn btn-red';
		}
		if (Number(hong[i]) == Number(lotteryCode.numInfo6)) {
			colors[5] = 'btn btn-red';
		}
		if (Number(hong[i]) == Number(lotteryCode.numInfo7)) {
			colors[6] = 'btn btn-red';
		}
	}

	// 判断是否是蓝波
	for (var i = 0; i < lan.length; i++) {
		if (Number(lan[i]) == Number(lotteryCode.numInfo1)) {
			colors[0] = 'btn btn-blue';
		}
		if (Number(lan[i]) == Number(lotteryCode.numInfo2)) {
			colors[1] = 'btn btn-blue';
		}
		if (Number(lan[i]) == Number(lotteryCode.numInfo3)) {
			colors[2] = 'btn btn-blue';
		}
		if (Number(lan[i]) == Number(lotteryCode.numInfo4)) {
			colors[3] = 'btn btn-blue';
		}
		if (Number(lan[i]) == Number(lotteryCode.numInfo5)) {
			colors[4] = 'btn btn-blue';
		}
		if (Number(lan[i]) == Number(lotteryCode.numInfo6)) {
			colors[5] = 'btn btn-blue';
		}
		if (Number(lan[i]) == Number(lotteryCode.numInfo7)) {
			colors[6] = 'btn btn-blue';
		}
	}

	// 判断是否是绿波
	for (var i = 0; i < lv.length; i++) {
		if (Number(lv[i]) == Number(lotteryCode.numInfo1)) {
			colors[0] = 'btn btn-green';
		}
		if (Number(lv[i]) == Number(lotteryCode.numInfo2)) {
			colors[1] = 'btn btn-green';
		}
		if (Number(lv[i]) == Number(lotteryCode.numInfo3)) {
			colors[2] = 'btn btn-green';
		}
		if (Number(lv[i]) == Number(lotteryCode.numInfo4)) {
			colors[3] = 'btn btn-green';
		}
		if (Number(lv[i]) == Number(lotteryCode.numInfo5)) {
			colors[4] = 'btn btn-green';
		}
		if (Number(lv[i]) == Number(lotteryCode.numInfo6)) {
			colors[5] = 'btn btn-green';
		}
		if (Number(lv[i]) == Number(lotteryCode.numInfo7)) {
			colors[6] = 'btn btn-green';
		}
	}

	return colors;
}

// 服务器时间倒计时
LhcPage.prototype.calculateTime = function(isClose) {
	var SurplusSecond = lotteryCommonPage.param.diffTime;

	lotteryCommonPage.param.isClose = isClose;
	if (lotteryCommonPage.param.isClose) {
		// 显示预售中
		$("#timer").hide();
		$("#presellTip").show();
		window.clearInterval(lotteryCommonPage.param.intervalKey); // 终止周期性倒计时
	} else {
		// var h = Math.floor(SurplusSecond/3600);
		// if (h<10){
		// h = "0"+h;
		// }
		// 计算剩余的分钟
		// SurplusSecond = SurplusSecond - (h * 3600);
		var m = Math.floor(SurplusSecond / 60);
		if (m < 10) {
			m = "0" + m;
		}
		var s = SurplusSecond % 60;
		if (s < 10) {
			s = "0" + s;
		}

		// h = h.toString();
		m = m.toString();
		s = s.toString();
		// $(".times .hours").html(h);
		// $(".times .minute").html(m);
		// $(".times .second").html(s);

		$("#timer").html(m + "<span>:</span>" + s);
		$("#presellTip").hide();
		$("#timer").show();
	}
	lotteryCommonPage.param.diffTime--;
	if (lotteryCommonPage.param.diffTime < 0) {
		isClose = true;
		// 弹出窗口控制
		$("#lotteryOrderDialogCancelButton").trigger("click");
		frontCommonPage.closeKindlyReminder();
		var str = "";
		str += "第";
		str += lotteryCommonPage.param.currentExpect + "期已截止,<br/>"
		str += "投注时请注意期号!";
		frontCommonPage.showKindlyReminder(str);
		setTimeout("frontCommonPage.closeKindlyReminder()", 2000);
		window.clearInterval(lotteryCommonPage.param.intervalKey); // 终止周期性倒计时
		lotteryCommonPage.getActiveExpect(); // 倒计时结束重新请求最新期号
	}
	;
};

LhcPage.prototype.change_bg = function(url) {
	$("body").css({
		"background-image" : "url(" + url + ")"
	});
}

// 删除注
LhcPage.prototype.remove_li = function(e) {
	$(e).closest("li").remove();
}
// 清空
LhcPage.prototype.clear_li = function() {
	$(".choose .choose-content1 .clists").html("");
}
// 注数增加
LhcPage.prototype.add_li = function(m1, m2, multiple1, multiple2, loop) {
	var html = "";
	for (var i = 0; i < loop; i++) {
		html += '<li>' + '<div class="li-child li-title">[五星_复式]<span>8,6,8,6,6</span></div>' + '<div class="li-child li-money">¥' + parseInt(m2).toFixed(2) + '元</div>'
				+ '<div class="li-child li-strip">' + multiple1 + '注</div>' + '<div class="li-child li-multiple">' + multiple2 + '倍</div>' + '<div class="li-child li-addmoney">¥'
				+ parseInt(m2).toFixed(2) + '元</div>' + '<div class="li-child li-close" onclick="remove_li(this)"><img src="images/lottery/close.png"></div>' + '</li>';
	}
	$(".choose .choose-content1 .clists").append(html);
}
LhcPage.prototype.add_more = function(index) {
	$(".choose-content2 .titles .child").not($(".choose-content2 .titles .child:eq(" + index + ")")).removeClass("on");
	$(".choose-content2 .titles .child:eq(" + index + ")").addClass("on");
	$(".choose-content2 .li-contents").not($(".choose-content2 .li-contents:eq(" + index + ")")).hide();
	$(".choose-content2 .li-contents:eq(" + index + ")").show();
}
LhcPage.prototype.msg_more = function(index) {
	$(".main .main-msg .titles .child").not($(".main .main-msg .titles .child:eq(" + index + ")")).removeClass("on");
	$(".main .main-msg .titles .child:eq(" + index + ")").addClass("on");
	$(".main .main-msg .contents").not($(".main .main-msg .contents:eq(" + index + ")")).hide();
	$(".main .main-msg .contents:eq(" + index + ")").show();
}
// 选择功能
LhcPage.prototype.select_all = function(e) {
	var parent = select_clear(e);
	parent.find(".l-mun .mun").addClass("on");
}
LhcPage.prototype.select_big = function(e) {
	var parent = select_clear(e);
	var len = parseInt(parent.find(".l-mun .mun").length / 2) - 1;
	parent.find(".l-mun .mun").each(function(index, element) {
		if (index > len) {
			$(element).addClass("on");
		}
	});
}
LhcPage.prototype.select_small = function(e) {
	var parent = select_clear(e);
	var len = parseInt(parent.find(".l-mun .mun").length / 2) - 1;
	parent.find(".l-mun .mun").each(function(index, element) {
		if (index <= len) {
			$(element).addClass("on");
		}
	});
}
LhcPage.prototype.select_odd = function(e) {
	var parent = select_clear(e);
	parent.find(".l-mun .mun").each(function(index, element) {
		if (index % 2 == 1) {
			$(element).addClass("on");
		}
	});
}
LhcPage.prototype.select_even = function(e) {
	var parent = select_clear(e);
	parent.find(".l-mun .mun").each(function(index, element) {
		if (index % 2 == 0) {
			$(element).addClass("on");
		}
	});
}
LhcPage.prototype.select_clear = function(e) {
	var parent = $(e).closest(".main .lists li");
	parent.find(".l-mun2 .mun").removeClass("on");
	parent.find(".l-mun .mun").removeClass("on");
	$(e).addClass("on");
	return parent;
}

var danshi = "";
danshi += "说明：\r\n" + "1、请参考标准格式样本格式录入或上传方案。\r\n" + "2、每一注号码之间的间隔符支持 回车 空格[ ] 逗号[,] 分号[;] 冒号[:] 竖线[|]。\r\n" + "3、文件格式必须是.txt格式。\r\n" + "4、文件较大时会导致上传时间较长，请耐心等待!\r\n"
		+ "5、将文件拖入文本框 即可快速实现文件上传功能。\r\n" + "6、导入文本内容将覆盖文本框中现有的内容。\r\n";
LhcPage.prototype.add_danshi = function() {
	$(".main .lists .l-textarea .textArea").val(danshi);
}
LhcPage.prototype.remove_danshi = function() {
	$(".main .lists .l-textarea .textArea").val("");
}

LhcPage.prototype.lottory_open = function() {
	var dtime;
	$(".m-banner .b-child2 .btns .btn").each(function(i, n) {
		dtime = 0.2 * i;
		base.anClasAdd(".m-banner .b-child2 .btns .btn:eq(" + i + ")", "scale", "1s", dtime + "s");
	});
	var dtime1 = (dtime + 1) * 1000;
	setTimeout(function() {
		$(".m-banner .b-child2 .btns .btn").attr("style", "");
	}, dtime1);
}

// 计算颜色
LhcPage.prototype.getColorsFrom1_49 = function() {
	var colors = new Array();
	for (var i = 1; i <= 49; ++i) {
		if (lotteryDataDeal.boshe.hongbo.indexOf(i) != -1)
			colors[i] = 'btn-red';
		else if (lotteryDataDeal.boshe.lanbo.indexOf(i) != -1)
			colors[i] = 'btn-blue';
		else if (lotteryDataDeal.boshe.lvbo.indexOf(i) != -1)
			colors[i] = 'btn-green';
	}

	return colors;
};

LhcPage.prototype.initScrollNumList = function() {

	$('#digital-scroll').remove();
	var html = '<div id="digital-scroll" class="digital-scroll">'
	var k = 2; // 第一个球转1轮后开始停止，往后每一轮停一个球
	var leftPx = 0;
	for (var i = 0; i < this.numScrollParam.from.length; ++i) {
		html += '<li style="left:' + leftPx + 'px;top:-' + (this.numScrollParam.from[i] - 1) * this.numScrollParam.height + 'px" class="loading">'
		liStr = ''
		for (var j = 1; j < 50; ++j) {
			liStr += '<div class="' + this.numScrollParam.colors[j] + '">' + (j > 9 ? j : ('0' + j)) + '</div>'
		}
		m = k++
		while (m--) {
			html += liStr
		}
		html += '</li>'
		if (i == 5) { // 第六个后面需要加上一个 + 号的距离
			html += '<span style="left:' + (leftPx + 38) + 'px;width: 20px;font-size: 20px;">+</span>'
			leftPx += this.numScrollParam.width + 23
		} else {
			leftPx += this.numScrollParam.width + 3
		}
	}
	html += '</div>'
	$('.btns3').append(html);
	// 转动
	// 第二轮转动后开始一个个停止
	k = 1;
	for (var i = 0; i < 7; ++i) {
		var count = (k++) * 49 + parseInt(this.numScrollParam.to[i]) - 1
		$('#digital-scroll').find("li").eq(i).animate({
			top : -count * this.numScrollParam.height
		}, count * 25)
	}
};

// 数字滚动效果 jQuery插件
LhcPage.prototype.scroll = function(from, to, colors) {
	lhcPage.numScrollParam.from = from;
	lhcPage.numScrollParam.to = to;
	lhcPage.numScrollParam.colors = colors;
	lhcPage.initScrollNumList();
	setTimeout(function() {
		$('.shengxiao').show();
	}, 9000);
}
