function JlffcZstPage(){}
var jlffcZstPage = new JlffcZstPage();

jlffcZstPage.param = {
   lotteryKind : "SHSSC",
   nearExpectNums : 30, //默认是近30期的数据
   numTye : "WX"  //默认显示五星的走势
}
var lineArr={};
var drawBool=false;

$(document).ready(function() {
	jlffcZstPage.getZstData();
	
	//是否显示遗漏值
	$("#Missing").unbind("click").click(function(){
		if(this.checked){ //如果选中的话
		   $("#expectList").removeClass("lost");
		}else{
			$("#expectList").addClass("lost");
		}
	});
	
	//走势图单击事件
	$(".tendency").click(function(){
		if(!drawBool) {
			jlffcZstPage.show_tendency();
		}
		if($(this).is(':checked')) {
			$("canvas").show();
		}else {
			$("canvas").hide();
		}
	});
	
	//30期数据
	$("#nearest30Expect").unbind("click").click(function(){
		$("a[name='nearestExpect']").each(function(){
			$(this).css("color","#fff");
		});
		$(this).css("color","#f8a525");
		jlffcZstPage.param.nearExpectNums = 30;
		jlffcZstPage.getZstData();
	});
	
	//50期数据
	$("#nearest50Expect").unbind("click").click(function(){
		$("a[name='nearestExpect']").each(function(){
			$(this).css("color","#fff");
		});
		$(this).css("color","#f8a525");
		jlffcZstPage.param.nearExpectNums = 50;
		jlffcZstPage.getZstData();
	});
	
	//今日数据
	$("#nearestTodayExpect").unbind("click").click(function(){
		$("a[name='nearestExpect']").each(function(){
			$(this).css("color","#fff");
		});
		$(this).css("color","#f8a525");
		jlffcZstPage.param.nearExpectNums = 100;
		jlffcZstPage.getZstData();
	});
	
	//近2天
	$("#nearestTwoDayExpect").unbind("click").click(function(){
		$("a[name='nearestExpect']").each(function(){
			$(this).css("color","#fff");
		});
		$(this).css("color","#f8a525");
		jlffcZstPage.param.nearExpectNums = 200;
		jlffcZstPage.getZstData();
	});
	
	//近5天
	$("#nearestFiveDayExpect").unbind("click").click(function(){
		$("a[name='nearestExpect']").each(function(){
			$(this).css("color","#fff");
		});
		$(this).css("color","#f8a525");
		jlffcZstPage.param.nearExpectNums = 120 * 5;
		jlffcZstPage.getZstData();
	});

	//星位选择
	$("a[name='xingwei']").unbind("click").click(function(){
        var dataType = $(this).attr("data-type");
        jlffcZstPage.param.numTye = dataType;
        
        //选中控制
		$("a[name='xingwei']").each(function(){
			$(this).css("color","#888");
		});
		$(this).css("color","#FFF");
		
		if(jlffcZstPage.param.numTye == 'SX'){
			$("th[name='ballnumber_for_wan']").hide();
		}else if(jlffcZstPage.param.numTye == 'QS'){
			$("th[name='ballnumber_for_shi']").hide();
			$("th[name='ballnumber_for_ge']").hide();
		}else if(jlffcZstPage.param.numTye == 'HS'){
			$("th[name='ballnumber_for_wan']").hide();
			$("th[name='ballnumber_for_qian']").hide();
		}else if(jlffcZstPage.param.numTye == 'QE'){
			$("th[name='ballnumber_for_bai']").hide();
			$("th[name='ballnumber_for_shi']").hide();
			$("th[name='ballnumber_for_ge']").hide();
		}else if(jlffcZstPage.param.numTye == 'HE'){
			$("th[name='ballnumber_for_wan']").hide();
			$("th[name='ballnumber_for_qian']").hide();
			$("th[name='ballnumber_for_bai']").hide();
		}
		
		$("#expectList").html("");
		jlffcZstPage.getZstData();
	});
	
});

/**
 * 查询当对应的走势图数据
 */
JlffcZstPage.prototype.getZstData = function(){
	//加载时候清除数据
	$("#expectList").html("");
	//加载中的样式
	$(".loading").show();
	$(".loading-bg").show();

	var queryParam = {};
	queryParam.lotteryKind = jlffcZstPage.param.lotteryKind;
	queryParam.nearExpectNums = jlffcZstPage.param.nearExpectNums;
	

	var jsonData = {
		"zstQueryVo" : queryParam,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/code/getZSTByCondition",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if( result.code == "ok"){
				jlffcZstPage.showZstData(result.data);  //展示走势图数据
			}else if(result.code == "error"){
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 展示走势图数据
 */
JlffcZstPage.prototype.showZstData = function(zstDatas){
	if(zstDatas != null && zstDatas.length > 0){
		var expectList = $("#expectList");
		var str = "";

		//出现总次数
		var wanTotalCount = [0,0,0,0,0,0,0,0,0,0];
		var qianTotalCount = [0,0,0,0,0,0,0,0,0,0];
		var baiTotalCount = [0,0,0,0,0,0,0,0,0,0];
		var shiTotalCount = [0,0,0,0,0,0,0,0,0,0];
		var geTotalCount = [0,0,0,0,0,0,0,0,0,0];
		
		//总遗漏值
		var wanSumCount = [0,0,0,0,0,0,0,0,0,0];
		var qianSumCount = [0,0,0,0,0,0,0,0,0,0];
		var baiSumCount = [0,0,0,0,0,0,0,0,0,0];
		var shiSumCount = [0,0,0,0,0,0,0,0,0,0];
		var geSumCount = [0,0,0,0,0,0,0,0,0,0];
		//平均遗漏值
		var wanAvgCount = [0,0,0,0,0,0,0,0,0,0];
		var qianAvgCount = [0,0,0,0,0,0,0,0,0,0];
		var baiAvgCount = [0,0,0,0,0,0,0,0,0,0];
		var shiAvgCount = [0,0,0,0,0,0,0,0,0,0];
		var geAvgCount = [0,0,0,0,0,0,0,0,0,0];
		
		//最大遗漏值
		var wanMaxCount = [0,0,0,0,0,0,0,0,0,0];
		var qianMaxCount = [0,0,0,0,0,0,0,0,0,0];
		var baiMaxCount = [0,0,0,0,0,0,0,0,0,0];
		var shiMaxCount = [0,0,0,0,0,0,0,0,0,0];
		var geMaxCount = [0,0,0,0,0,0,0,0,0,0];
		
		//最大连出记录 		
		var wanSerialCount = [0,0,0,0,0,0,0,0,0,0];
		var qianSerialCount = [0,0,0,0,0,0,0,0,0,0];
		var baiSerialCount = [0,0,0,0,0,0,0,0,0,0];
		var shiSerialCount = [0,0,0,0,0,0,0,0,0,0];
		var geSerialCount = [0,0,0,0,0,0,0,0,0,0];
		
		var wanMaxSerialCount = [0,0,0,0,0,0,0,0,0,0];
		var qianMaxSerialCount = [0,0,0,0,0,0,0,0,0,0];
		var baiMaxSerialCount = [0,0,0,0,0,0,0,0,0,0];
		var shiMaxSerialCount = [0,0,0,0,0,0,0,0,0,0];
		var geMaxSerialCount = [0,0,0,0,0,0,0,0,0,0];
		//最大连出记录  记录号码第几条数据		
		var wanSerialRecord = [-2,-2,-2,-2,-2,-2,-2,-2,-2,-2];
		var qianSerialRecord = [-2,-2,-2,-2,-2,-2,-2,-2,-2,-2];
		var baiSerialRecord = [-2,-2,-2,-2,-2,-2,-2,-2,-2,-2];
		var shiSerialRecord = [-2,-2,-2,-2,-2,-2,-2,-2,-2,-2];
		var geSerialRecord = [-2,-2,-2,-2,-2,-2,-2,-2,-2,-2];
		var  length = zstDatas.length;
	    for(var j = 0; j < zstDatas.length; j++){
	    	var zstData = zstDatas[j];
	    	str += "<ul class='line line-canvas' >";
	    	str += "  <li class='line-child line-child1'>";
	    	str += "  	<p  class='title'>"+zstData.lotteryNum+"</p>";
	    	str += "  </li>";
	    	str += "   <li class='line-child line-child2'>";
	    	str += "  <p class='title'>"+zstData.opencodeStr+"</p></li>";
	    	//万，千，百，十，个
	    	var wanOmmitMap = new JS_OBJECT_MAP();
	    	var qianOmmitMap = new JS_OBJECT_MAP();
	    	var baiOmmitMap = new JS_OBJECT_MAP();
	    	var shiOmmitMap = new JS_OBJECT_MAP();
	    	var geOmmitMap = new JS_OBJECT_MAP();
	    	wanOmmitMap=zstData.wanNumMateCurrent;
	    	qianOmmitMap=zstData.qianNumMateCurrent;
	    	baiOmmitMap=zstData.baiNumMateCurrent;
	    	shiOmmitMap=zstData.shiNumMateCurrent;
	    	geOmmitMap=zstData.geNumMateCurrent;
	  
	    	
	    	/**
	    	 * 万位
	    	 */
	    	var i= 0; var obj = wanOmmitMap; var opencode =  zstData.numInfo1;
	    	
	      	str += "<li class='line-child line-child-canvas' data-line-color='#FF2F33'>";
	    	str += "	<div class='muns'>";
	    	for(var k =0 ;k<10;k++){
	    		var kstr= k;
	    		if(k==Number(opencode)){
	    			str += " <span class='red on'><i class='ball-noraml' name='ball-noraml_"+j+"_"+i+"_"+k+"'>"+opencode+"</i> </span>";		
	    			wanTotalCount[k] = wanTotalCount[k]+1;
	    			//判断是否连出
	    			if( 0 == wanSerialCount[k] || (j-1) == wanSerialRecord[k]){
	    				wanSerialCount[k] = wanSerialCount[k] + 1;
	    				if(wanMaxSerialCount[k] < wanSerialCount[k]){
	    					wanMaxSerialCount[k] = wanSerialCount[k];
	    				}
	    			}else{	    				
	    				if(wanMaxSerialCount[k] < wanSerialCount[k]){
	    					wanMaxSerialCount[k] = wanSerialCount[k];
	    				}
	    				wanSerialCount[k] = 1;
	    			}
	    			wanSerialRecord[k] = j;
	    		}else{
	    			str += " <span ><i class='ball-noraml' name='ball-noraml_"+j+"_"+i+"_"+k+"'>"+obj[kstr]+"</i> </span>";		
	    		   
	    			//对比遗漏值，赋值最大遗漏值
	    			if(obj[kstr] > wanMaxCount[k]){
	    				wanMaxCount[k] = obj[kstr];
	    			}
	    			
	    			//总遗漏值
	    			wanSumCount[k] = wanSumCount[k]+obj[kstr];
	    		}
	    		
	    	}
	    	str += "<canvas width='270' height='44'>";
	    	str +="</div>";
	    	str +="</li>";
	    	
	    	/**
	    	 * 千位
	    	 */
	    	 i= 0; obj = qianOmmitMap; opencode =  zstData.numInfo2;
	      	str += "<li class='line-child line-child-canvas' data-line-color='#FF2F33'>";
	    	str += "	<div class='muns'>";
	    	for(var k =0 ;k<10;k++){
	    		var kstr= k;
	    		if(k==Number(opencode)){
	    			str += " <span class='red on'><i class='ball-noraml' name='ball-noraml_"+j+"_"+i+"_"+k+"'>"+opencode+"</i> </span>";		
	    			qianTotalCount[k] = qianTotalCount[k]+1;
	    			//判断是否连出
	    			if( 0 == qianSerialCount[k] || (j-1) == qianSerialRecord[k]){
	    				qianSerialCount[k] = qianSerialCount[k] + 1;
	    				if(qianMaxSerialCount[k] < qianSerialCount[k]){
	    					qianMaxSerialCount[k] = qianSerialCount[k];
	    				}
	    			}else{	    				
	    				if(qianMaxSerialCount[k] < qianSerialCount[k]){
	    					qianMaxSerialCount[k] = qianSerialCount[k];
	    				}
	    				qianSerialCount[k] = 1;
	    			}
	    			qianSerialRecord[k] = j;
	    		}else{
	    			str += " <span ><i class='ball-noraml' name='ball-noraml_"+j+"_"+i+"_"+k+"'>"+obj[kstr]+"</i> </span>";		
	    			//对比遗漏值，赋值最大遗漏值
	    			if(obj[kstr] > qianMaxCount[k]){
	    				qianMaxCount[k] = obj[kstr];
	    			}
	    			//总遗漏值
	    			qianSumCount[k] = qianSumCount[k]+obj[kstr];
	    		}
	    		
	    	}
	    	str += "<canvas width='270' height='44'>";
	    	str +="</div>";
	    	str +="</li>";
	    	
	    	/**
	    	 * 百位
	    	 */
	    	 i= 0; obj = baiOmmitMap; opencode =  zstData.numInfo3;
	      	str += "<li class='line-child line-child-canvas' data-line-color='#FF2F33'>";
	    	str += "	<div class='muns'>";
	    	for(var k =0 ;k<10;k++){
	    		var kstr= k;
	    		if(k==Number(opencode)){
	    			str += " <span class='red on'><i class='ball-noraml' name='ball-noraml_"+j+"_"+i+"_"+k+"'>"+opencode+"</i> </span>";		
	    			baiTotalCount[k] = baiTotalCount[k]+1;
	    			//判断是否连出
	    			if( 0 == baiSerialCount[k] || (j-1) == baiSerialRecord[k]){
	    				baiSerialCount[k] = baiSerialCount[k] + 1;
	    				if(baiMaxSerialCount[k] < baiSerialCount[k]){
	    					baiMaxSerialCount[k] = baiSerialCount[k];
	    				}
	    			}else{	    				
	    				if(baiMaxSerialCount[k] < baiSerialCount[k]){
	    					baiMaxSerialCount[k] = baiSerialCount[k];
	    				}
	    				baiSerialCount[k] = 1;
	    			}
	    			baiSerialRecord[k] = j;
	    		}else{
	    			str += " <span ><i class='ball-noraml' name='ball-noraml_"+j+"_"+i+"_"+k+"'>"+obj[kstr]+"</i> </span>";		
	    			//对比遗漏值，赋值最大遗漏值
	    			if(obj[kstr] > baiMaxCount[k]){
	    				baiMaxCount[k] = obj[kstr];
	    			}
	    			//总遗漏值
	    			baiSumCount[k] = baiSumCount[k]+obj[kstr];
	    		}
	    		
	    	}
	    	str += "<canvas width='270' height='44'>";
	    	str +="</div>";
	    	str +="</li>";
	    	
	    	/**
	    	 * 十位
	    	 */
	    	 i= 0; obj = shiOmmitMap; opencode =  zstData.numInfo4;
	      	str += "<li class='line-child line-child-canvas' data-line-color='#FF2F33'>";
	    	str += "	<div class='muns'>";
	    	for(var k =0 ;k<10;k++){
	    		var kstr= k;
	    		if(k==Number(opencode)){
	    			str += " <span class='red on'><i class='ball-noraml' name='ball-noraml_"+j+"_"+i+"_"+k+"'>"+opencode+"</i> </span>";		
	    			shiTotalCount[k] = shiTotalCount[k]+1;
	    			//判断是否连出
	    			if( 0 == shiSerialCount[k] || (j-1) == shiSerialRecord[k]){
	    				shiSerialCount[k] = shiSerialCount[k] + 1;
	    				if(shiMaxSerialCount[k] < shiSerialCount[k]){
	    					shiMaxSerialCount[k] = shiSerialCount[k];
	    				}
	    			}else{	    				
	    				if(shiMaxSerialCount[k] < shiSerialCount[k]){
	    					shiMaxSerialCount[k] = shiSerialCount[k];
	    				}
	    				shiSerialCount[k] = 1;
	    			}
	    			shiSerialRecord[k] = j;
	    		}else{
	    			str += " <span ><i class='ball-noraml' name='ball-noraml_"+j+"_"+i+"_"+k+"'>"+obj[kstr]+"</i> </span>";		
	    			//对比遗漏值，赋值最大遗漏值
	    			if(obj[kstr] > shiMaxCount[k]){
	    				shiMaxCount[k] = obj[kstr];
	    			}
	    			//总遗漏值
	    			shiSumCount[k] = shiSumCount[k]+obj[kstr];
	    		}
	    		
	    	}
	    	str += "<canvas width='270' height='44'>";
	    	str +="</div>";
	    	str +="</li>";
	    	
	    	/**
	    	 * 个位
	    	 */
	    	 i= 0; obj = geOmmitMap; opencode =  zstData.numInfo5;
	      	str += "<li class='line-child line-child-canvas' data-line-color='#FF2F33'>";
	    	str += "	<div class='muns'>";
	    	for(var k =0 ;k<10;k++){
	    		var kstr= k;
	    		if(k==Number(opencode)){
	    			str += " <span class='red on'><i class='ball-noraml' name='ball-noraml_"+j+"_"+i+"_"+k+"'>"+opencode+"</i> </span>";		
	    			geTotalCount[k] = geTotalCount[k]+1;
	    			//判断是否连出
	    			if( 0 == geSerialCount[k] ||(j-1) == geSerialRecord[k] ){
	    				geSerialCount[k] = geSerialCount[k] + 1;
	    				if(geMaxSerialCount[k] < geSerialCount[k]){
	    					geMaxSerialCount[k] = geSerialCount[k];
	    				}
	    			}else{	    				
	    				if(geMaxSerialCount[k] < geSerialCount[k]){
	    					geMaxSerialCount[k] = geSerialCount[k];
	    				}
	    				geSerialCount[k] = 1;
	    			}
	    			geSerialRecord[k] = j;
	    		}else{
	    			str += " <span ><i class='ball-noraml' name='ball-noraml_"+j+"_"+i+"_"+k+"'>"+obj[kstr]+"</i> </span>";		
	    			//对比遗漏值，赋值最大遗漏值
	    			if(obj[kstr] > geMaxCount[k]){
	    				geMaxCount[k] = obj[kstr];
	    			}
	    			//总遗漏值
	    			geSumCount[k] = geSumCount[k]+obj[kstr];
	    		}
	    		
	    	}
	    	str += "<canvas width='270' height='44'>";
	    	str +="</div>";
	    	str +="</li>";
	    	str += "</ul>";
	    	
	    }		
	    expectList.html(str);
	    str = "";
	    //填充统计值
	   
    	
		 for(var i=0;i<10;i++){	
			//总出现次数
		   $("i[name='ball-noraml_total_count_0_"+i+"']").text(wanTotalCount[i]);
		   $("i[name='ball-noraml_total_count_1_"+i+"']").text(qianTotalCount[i]);
		   $("i[name='ball-noraml_total_count_2_"+i+"']").text(baiTotalCount[i]);
		   $("i[name='ball-noraml_total_count_3_"+i+"']").text(shiTotalCount[i]);
		   $("i[name='ball-noraml_total_count_4_"+i+"']").text(geTotalCount[i]);
		  
		   //最大遗漏值
		   $("i[name='ball-noraml_max_count_0_"+i+"']").text(wanMaxCount[i]);
		   $("i[name='ball-noraml_max_count_1_"+i+"']").text(qianMaxCount[i]);
		   $("i[name='ball-noraml_max_count_2_"+i+"']").text(baiMaxCount[i]);
		   $("i[name='ball-noraml_max_count_3_"+i+"']").text(shiMaxCount[i]);
		   $("i[name='ball-noraml_max_count_4_"+i+"']").text(geMaxCount[i]);
		   
		   //平均遗漏值
		   $("i[name='ball-noraml_average_count_0_"+i+"']").text(parseInt(wanSumCount[i]/length));
		   $("i[name='ball-noraml_average_count_1_"+i+"']").text(parseInt(qianSumCount[i]/length));
		   $("i[name='ball-noraml_average_count_2_"+i+"']").text(parseInt(baiSumCount[i]/length));
		   $("i[name='ball-noraml_average_count_3_"+i+"']").text(parseInt(shiSumCount[i]/length));
		   $("i[name='ball-noraml_average_count_4_"+i+"']").text(parseInt(geSumCount[i]/length));
		   
		   //最大连出值
		   $("i[name='ball-noraml_serial_count_0_"+i+"']").text(wanMaxSerialCount[i]);
		   $("i[name='ball-noraml_serial_count_1_"+i+"']").text(qianMaxSerialCount[i]);
		   $("i[name='ball-noraml_serial_count_2_"+i+"']").text(baiMaxSerialCount[i]);
		   $("i[name='ball-noraml_serial_count_3_"+i+"']").text(shiMaxSerialCount[i]);
		   $("i[name='ball-noraml_serial_count_4_"+i+"']").text(geMaxSerialCount[i]);
		 }
   		
		// 选中近xx期，画布根据是否选中显示折线，来显示或隐藏
		if($(".tendency").is(':checked')) {
			jlffcZstPage.show_tendency();
		}else {
			drawBool=false;
			$("canvas").hide();
		}
	}
	
	setTimeout(function(){
		$(".loading").fadeOut(500);
		$(".loading-bg").fadeOut(500);
	},0);
	$("#modalLoading").hide();
};

/**
 * 拼接cavans
 */
JlffcZstPage.prototype.show_tendency = function(){
	//走势图
	$(".line-canvas").each(function(i,n){
		$(n).find(".line-child-canvas").each(function(ii,nn){
			if(!lineArr[ii])lineArr[ii]={};
			if(!lineArr[ii]["color"])lineArr[ii]["color"]=$(nn).attr("data-line-color");
			lineArr[ii][i]=$(nn).find(".on").position().left;
		});
	});
	$.each(lineArr,function(i,n){
		$.each(n,function(ii,nn){
			var w=$(".line-canvas:eq("+ii+") .line-child-canvas:eq("+i+") canvas").closest('.muns').width();
			var ele=$(".line-canvas:eq("+ii+") .line-child-canvas:eq("+i+") canvas");
			ele.attr("width",w);
			ele.css("width",w+"px");
		});
	});
	drawBool=true;
	$.each(lineArr,function(i,n){
		$.each(n,function(ii,nn){
			var ele=$(".line-canvas:eq("+ii+") .line-child-canvas:eq("+i+") canvas");
			if(ele[0]) {
				jlffcZstPage.drawline(ele,nn,n[parseInt(ii)+1],n["color"]);
			}
		});
	});

}
JlffcZstPage.prototype.drawline = function(ele,top_point,bottom_point,color){
	/*var ele=document.getElementsByTagName("canvas");*/

	var context=ele[0].getContext("2d");
	context.strokeStyle=color;
	context.lineWidth="2";
	context.lineCap="round"; 
	
	context.moveTo(top_point+10,0);
	context.lineTo(bottom_point+10,44);

	context.stroke();
}
