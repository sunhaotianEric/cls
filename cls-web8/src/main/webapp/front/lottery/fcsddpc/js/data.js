//通用的号码数组
var common_code_array = new Array(0,1,2,3,4,5,6,7,8,9);
//三星直选和值
var qszxhz_code_array = new Array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27);
//三星组选和值
var qszxhz_g_code_array = new Array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26);
//前二或者后二直选和值
var qezxhz_code_array = new Array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18);
//前二或者后二组选和值
var qezxhz_g_code_array = new Array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17);
//大小单双
var dxds_code_array = new Array(1,2,3,4);

var editorStr = "说明：\n";
editorStr += "1、请参照标准格式样本格式录入或上传方案。\n";
editorStr += "2、每一注号码之间的间隔符支持 回车 空格[ ] 逗号[,] 分号[;] 冒号[:] 竖线 [|]。\n";
editorStr += "3、文件格式必须是.txt格式。\n";
editorStr += "4、文件较大时会导致上传时间较长，请耐心等待！\n";
editorStr += "5、将文件拖入文本框即可快速实现文件上传功能。\n";
editorStr += "6、导入文本内容后将覆盖文本框中现有的内容。";

//////////////////玩法//////////////////// 
//三星玩法
var SX_PLAY = "";
SX_PLAY += "<div class='classify-list-li' data-role-id='SXFS'><p>直选：复式</p></div>";
SX_PLAY += "<div class='classify-list-li' data-role-id='SXDS'><p>直选：单式</p></div>";
SX_PLAY += "<div class='classify-list-li' data-role-id='SXZXHZ'><p>直选：和值</p></div>";
SX_PLAY += "<div class='classify-list-li' data-role-id='SXZXHZ_G'><p>组选：和值</p></div>";
SX_PLAY += "<div class='classify-list-li' data-role-id='SXZS'><p>组选：三星组三</p></div>";
SX_PLAY += "<div class='classify-list-li' data-role-id='SXZL'><p>组选：三星组六</p></div>";
SX_PLAY += "<div class='classify-list-li' data-role-id='SXYMBDW'><p>直选：一码不定位</p></div>";
SX_PLAY += "<div class='classify-list-li' data-role-id='SXEMBDW'><p>直选：二码不定位</p></div>";

//前二玩法
var QE_PLAY = "";
QE_PLAY += "<div class='classify-list-li' data-role-id='QEZXFS'><p>直选：复式</p></div>";
QE_PLAY += "<div class='classify-list-li' data-role-id='QEZXDS'><p>直选：单式</p></div>";
QE_PLAY += "<div class='classify-list-li' data-role-id='QEZXHZ'><p>直选：和值</p></div>";
QE_PLAY += "<div class='classify-list-li' data-role-id='QEZXFS_G'><p>组选：复式</p></div>";
QE_PLAY += "<div class='classify-list-li' data-role-id='QEZXDS_G'><p>组选：单式</p></div>";
QE_PLAY += "<div class='classify-list-li' data-role-id='QEZXHZ_G'><p>组选：和值</p></div>";

//后二玩法
var HE_PLAY = "";
HE_PLAY += "<div class='classify-list-li' data-role-id='HEZXFS'><p>直选：复式</p></div>";
HE_PLAY += "<div class='classify-list-li' data-role-id='HEZXDS'><p>直选：单式</p></div>";
HE_PLAY += "<div class='classify-list-li' data-role-id='HEZXHZ'><p>直选：和值</p></div>";
HE_PLAY += "<div class='classify-list-li' data-role-id='HEZXFS_G'><p>组选：复式</p></div>";
HE_PLAY += "<div class='classify-list-li' data-role-id='HEZXDS_G'><p>组选：单式</p></div>";
HE_PLAY += "<div class='classify-list-li' data-role-id='HEZXHZ_G'><p>组选：和值</p></div>";

//一星玩法
var YX_PLAY = "";
YX_PLAY += "<div class='classify-list-li' data-role-id='SXDWD'><p>定位胆</p></div>";

//任选
var RX_PLAY = "";
RX_PLAY += "<div class='classify-list-li' data-role-id='RXE'><p>任选二：复式</p></div>";
RX_PLAY += "<div class='classify-list-li' data-role-id='RXEZXDS'><p>任选二：单式</p></div>";

//大小单双
var DXDS_PLAY = "";
DXDS_PLAY += "<div class='classify-list-li' data-role-id='QEDXDS'><p>直选：前二大小单双</p></div>";
DXDS_PLAY += "<div class='classify-list-li' data-role-id='HEDXDS'><p>直选：后二大小单双</p></div>";

//////////////////玩法描述////////////////////
//三星
var SXFS_DES, SXDS_DES, SXZXHZ_DES, SXZXHZ_G_DES, SXZS_DES, SXZL_DES, SXYMBDW_DES, SXEMBDW_DES = "请稍后........";
//三星单式投注样例
var SXDS_SAMPLE = "<p>345</p><p>353</p><p>688</p>";


//前二
var QEZXFS_DES, QEZXDS_DES, QEZXHZ_DES, QEZXFS_G_DES, QEZXDS_G_DES, QEZXHZ_G_DES = "请稍后........";
//前二直选单式投注样例
var QEZXDS_SAMPLE = "<p>65</p><p>33</p><p>97</p>";
//前二组选单式投注样例
var QEZXDS_G_SAMPLE = "<p>12</p><p>23</p><p>56</p>";

//后二
var HEZXFS_DES, HEZXDS_DES, HEZXHZ_DES, HEZXFS_G_DES, HEZXDS_G_DES, HEZXHZ_G_DES = "请稍后........";
//后二直选单式投注样例
var HEZXDS_SAMPLE = "<p>11</p><p>23</p><p>78</p>";
//后二组选单式投注样例
var HEZXDS_G_SAMPLE = "<p>56</p><p>31</p><p>98</p>";

//前二大小单双 后二大小单双
var QEDXDS_DES, HEDXDS_DES = "请稍后........";

//一星 定位胆
var SXDWD_DES = "请稍后........";

//任选
var RXY_DES, RXE_DES, RXEZXDS_DES  = "请稍后........";
//任选三直选单式样例
var RXEZXDS_SAMPLE = "<p>26</p><p>18</p><p>45</p>";

function LotteryDataDeal(){}
var lottertyDataDeal = new LotteryDataDeal();

lottertyDataDeal.param = {
	isRxDsAllow : true,  //是否允许任选单式,由位数来进行控制
    dsAllowCount : 0,
};

/**
 * 加载所有的玩法
 */
LotteryDataDeal.prototype.loadLotteryKindPlay = function() {
	$("#SX").append(SX_PLAY);
	$("#QE").append(QE_PLAY);
	$("#HE").append(HE_PLAY);
	$("#YX").append(YX_PLAY);
	$("#RX").append(RX_PLAY);
	$("#DXDS").append(DXDS_PLAY);
}


/**
 * 玩法选择
 * @param roleId
 */
LotteryDataDeal.prototype.showPlayDes = function(roleId){
	  
	  if(roleId == 'SXFS'){
		  $("#playDes").html(SXFS_DES);
	  }else if(roleId == 'SXDS'){
		  $("#playDes").html(SXDS_DES);
		  $("#dsModelSample").html(SXDS_SAMPLE);
	  }else if(roleId == 'SXZXHZ'){
		  $("#playDes").html(SXZXHZ_DES);
	  }else if(roleId == 'SXZXHZ_G'){
		  $("#playDes").html(SXZXHZ_G_DES);
	  }else if(roleId == 'SXZS'){
		  $("#playDes").html(SXZS_DES);
	  }else if(roleId == 'SXZL'){
		  $("#playDes").html(SXZL_DES);
	  }else if(roleId == 'SXYMBDW'){
		  $("#playDes").html(SXYMBDW_DES);
	  }else if(roleId == 'SXEMBDW'){
		  $("#playDes").html(SXEMBDW_DES);
	  }else if(roleId == 'QEZXFS'){
		  $("#playDes").html(QEZXFS_DES);
	  }else if(roleId == 'QEZXDS'){
		  $("#playDes").html(QEZXDS_DES);
		  $("#dsModelSample").html(QEZXDS_SAMPLE);
	  }else if(roleId == 'QEZXHZ'){
		  $("#playDes").html(QEZXHZ_DES);
	  }else if(roleId == 'QEZXFS_G'){
		  $("#playDes").html(QEZXFS_G_DES);
	  }else if(roleId == 'QEZXDS_G'){  
		  $("#playDes").html(QEZXDS_G_DES);
		  $("#dsModelSample").html(QEZXDS_G_SAMPLE);
	  }else if(roleId == 'QEZXHZ_G'){
		  $("#playDes").html(QEZXHZ_G_DES);
	  }else if(roleId == 'HEZXFS'){
		  $("#playDes").html(HEZXFS_DES);
	  }else if(roleId == 'HEZXDS'){
		  $("#playDes").html(HEZXDS_DES);
		  $("#dsModelSample").html(HEZXDS_SAMPLE);
	  }else if(roleId == 'HEZXHZ'){
		  $("#playDes").html(HEZXHZ_DES);
	  }else if(roleId == 'HEZXFS_G'){
		  $("#playDes").html(HEZXFS_G_DES);
	  }else if(roleId == 'HEZXDS_G'){
		  $("#playDes").html(HEZXDS_G_DES);
		  $("#dsModelSample").html(HEZXDS_G_SAMPLE);
	  }else if(roleId == 'HEZXHZ_G'){
		  $("#playDes").html(HEZXHZ_G_DES);
	  }else if(roleId == 'QEDXDS'){
		  $("#playDes").html(QEDXDS_DES);
	  }else if(roleId == 'HEDXDS'){
		  $("#playDes").html(HEDXDS_DES);
	  }else if(roleId == 'SXDWD'){
		  $("#playDes").html(SXDWD_DES);
	  }else if(roleId == 'RXE'){
		  $("#playDes").html(RXE_DES);
	  }else if(roleId == 'RXEZXDS'){
		  $("#playDes").html(RXEZXDS_DES);
		  $("#dsModelSample").html(RXEZXDS_SAMPLE);
	  }else{
		  alert("不可知的彩种玩法描述.");
	  }  
};

/**
 * 玩法选择
 * @param roleId
 */
LotteryDataDeal.prototype.lotteryKindPlayChoose = function(roleId) {
	var desArray = new Array("万位", "千位", "百位", "十位", "个位");
	var emptyDesArray = new Array("");
	if (roleId == 'SXFS') {
		lottertyDataDeal.ballSectionCommon(2, 4, desArray, null, true);
	} else if (roleId == 'SXDS') {
		lottertyDataDeal.dsBallSectionCommon(roleId);
	} else if (roleId == 'SXZXHZ') {
		lottertyDataDeal.ballSectionCommon(0, 0, emptyDesArray, roleId, false);
	} else if (roleId == 'SXZXHZ_G') {
		lottertyDataDeal.ballSectionCommon(0, 0, emptyDesArray, roleId, false);
	} else if (roleId == 'SXZS') {
		lottertyDataDeal.ballSectionCommon(0, 0, emptyDesArray, null, false);
	} else if (roleId == 'SXZL') {
		lottertyDataDeal.ballSectionCommon(0, 0, emptyDesArray, null, false);
	} else if (roleId == 'SXYMBDW') {
		lottertyDataDeal.ballSectionCommon(0, 0, emptyDesArray, null, false);
	} else if (roleId == 'SXEMBDW') {
		lottertyDataDeal.ballSectionCommon(0, 0, emptyDesArray, null, false);
	} else if (roleId == 'QEZXFS') {
		lottertyDataDeal.ballSectionCommon(2, 3, desArray, null, true);
	} else if (roleId == 'QEZXDS') {
		lottertyDataDeal.dsBallSectionCommon(roleId);
	} else if (roleId == 'QEZXHZ') {
		lottertyDataDeal.ballSectionCommon(0, 0, emptyDesArray, roleId, false);
	} else if (roleId == 'QEZXFS_G') {
		lottertyDataDeal.ballSectionCommon(0, 0, emptyDesArray, null, false);
	} else if (roleId == 'QEZXDS_G') {
		lottertyDataDeal.dsBallSectionCommon(roleId);
	} else if (roleId == 'QEZXHZ_G') {
		lottertyDataDeal.ballSectionCommon(0, 0, emptyDesArray, roleId, false);
	} else if (roleId == 'HEZXFS') {
		lottertyDataDeal.ballSectionCommon(3, 4, desArray, null, true);
	} else if (roleId == 'HEZXDS') {
		lottertyDataDeal.dsBallSectionCommon(roleId);
	} else if (roleId == 'HEZXHZ') {
		lottertyDataDeal.ballSectionCommon(0, 0, emptyDesArray, roleId, false);
	} else if (roleId == 'HEZXFS_G') {
		lottertyDataDeal.ballSectionCommon(0, 0, emptyDesArray, null, false);
	} else if (roleId == 'HEZXDS_G') {
		lottertyDataDeal.dsBallSectionCommon(roleId);
	} else if (roleId == 'HEZXHZ_G') {
		lottertyDataDeal.ballSectionCommon(0, 0, emptyDesArray, roleId, false);
	} else if (roleId == 'QEDXDS') {
		desArray = new Array("百位", "十位");
		lottertyDataDeal.ballSectionCommon(0, -1, desArray, roleId, false);
	} else if (roleId == 'HEDXDS') {
		desArray = new Array("十位", "个位");
		lottertyDataDeal.ballSectionCommon(0, -1, desArray, roleId, false);
	} else if (roleId == 'SXDWD') {
		lottertyDataDeal.ballSectionCommon(2, 4, desArray, null, true);
	} else if (roleId == 'RXE') {
		lottertyDataDeal.ballSectionCommon(2, 4, desArray, null, false);
	} else if (roleId == 'RXEZXDS') { // 任选二直选单式
		lottertyDataDeal.dsBallSectionCommon(roleId);
	} else {
		alert("不可知的彩种玩法.");
	}
	
	//存储当前切换的玩法模式
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey = roleId;
	
	//显示玩法名称
    var currentKindName = $(".classify-list-li[data-role-id='"+ roleId +"']").find("p").text();
    $("#currentKindName").text(currentKindName);

	//显示玩法说明
	lottertyDataDeal.showPlayDes(roleId);
	
	//星种的选中处理
	lotteryCommonPage.currentLotteryKindInstance.showCurrentPlayKindPage(roleId);

	//拼接完，进行事件的重新申明
	lotteryCommonPage.dealForEvent(); 
};

/**
 * 复式玩法的号码拼接
 * desArray 描述的数组
 * specialKind 特殊玩法
 * isOmitColdDataDeal 是否遗漏和冷热数据的处理
 */
LotteryDataDeal.prototype.ballSectionCommon = function(numCountStart,numCountEnd,desArray,specialKind,isOmitColdDataDeal){
	
	var content = "";
	if(specialKind == null){
		//大小奇偶按钮字符串
		var dxjoStr = '<div class="l-mun2">'
					+'	<div class="mun" data-role-type="all">全</div>'
					+'	<div class="mun" data-role-type="big">大</div>'
					+'	<div class="mun" data-role-type="small">小</div>'
					+'	<div class="mun" data-role-type="odd">奇</div>'
					+'	<div class="mun" data-role-type="even">偶</div>'
					+'	<div class="mun" data-role-type="empty">清</div>'
					+'</div>';
		
		for(var i = numCountStart;i <= numCountEnd; i++){
			if(i % 2 == 0) {
				content += "<li class='odd'>";
			} else {
				content += "<li class='even'>";
			}
			var numDes = desArray[i];
			if(isNotEmpty(numDes)) {
				content += "  <div class='l-title'><p>" + numDes + "</p>";
				//遗漏冷热元素处理
				if(isOmitColdDataDeal) {
					content += "<p class='l-msg'>当前遗漏</p>";
				}
				content += "</div>";
			}
			content += "<div class='l-mun'>";	
			
			//0 - 9的号码
			for(var j = 0; j < common_code_array.length; j++){
				content += "<div class='mun' data-realvalue='"+ common_code_array[j] +"' name='ballNum_"+i+"_match'>"+ common_code_array[j] +"";
				//遗漏冷热元素处理
				if(isOmitColdDataDeal) {
					content += "<p class='l-msg'>0</p>";
				}
				content += "</div>";
			}
			content += "</div>";
			content += dxjoStr;
		}
	} else{ 
		var hz_code_array = null;
		if(specialKind == "SXZXHZ"){
			//0 - 27的号码
			hz_code_array = qszxhz_code_array;
		}else if(specialKind == "SXZXHZ_G"){
			//1 - 26的号码
			hz_code_array = qszxhz_g_code_array;
		}else if(specialKind == "QEZXHZ" || specialKind == "HEZXHZ"){
			//0 - 18的号码
			hz_code_array = qezxhz_code_array;
		}else if(specialKind == "QEZXHZ_G" || specialKind == "HEZXHZ_G"){
			//1 - 17的号码
			hz_code_array = qezxhz_g_code_array
		}
		
		if(hz_code_array != null) {
			content += "<li class='odd'>";
			var numDes = desArray[i];
			if(isNotEmpty(numDes)) {
				content += "  <div class='l-title'><p>" + numDes + "</p>";
				//遗漏冷热元素处理
				if(isOmitColdDataDeal) {
					content += "<p class='l-msg'>当前遗漏</p>";
				}
				content += "</div>";
			}
			content += "<div class='l-mun'>";
			for(var j = 0; j < hz_code_array.length; j++){
				content += "<div class='mun' data-realvalue='"+ hz_code_array[j] +"' name='ballNum_"+i+"_match'>"+ hz_code_array[j] +"";
				//遗漏冷热元素处理
				if(isOmitColdDataDeal) {
					content += "<p class='l-msg'>0</p>";
				}
				content += "</div>";
			}
			content += "</div>";
		} else if(specialKind == "QEDXDS" || specialKind == "HEDXDS"){
			for(var i = 0; i < desArray.length; i++){
				if(i % 2 == 0) {
					content += "<li class='odd'>";
				} else {
					content += "<li class='even'>";
				}
				var numDes = desArray[i];
				if(isNotEmpty(numDes)) {
					content += "  <div class='l-title'><p>" + numDes + "</p>";
					//遗漏冷热元素处理
					if(isOmitColdDataDeal) {
						content += "<p class='l-msg'>当前遗漏</p>";
					}
					content += "</div>";
				}
				content += "<div class='l-mun'>";	
				
				//1 - 4的号码
				for(var j = 0; j < dxds_code_array.length; j++){
					content += "<div class='mun' data-realvalue='"+ dxds_code_array[j] +"' name='ballNum_"+i+"_match'>";
					if(dxds_code_array[j] == 1){
						content += "大";
					}else if(dxds_code_array[j] == 2){
						content += "小";
					}else if(dxds_code_array[j] == 3){
						content += "单";
					}else if(dxds_code_array[j] == 4){
						content += "双";
					}
					//遗漏冷热元素处理
					if(isOmitColdDataDeal) {
						content += "<p class='l-msg'>0</p>";
					}
					content += "</div>";
				}
				content += "</div>";
			}
		}else{
			alert("未找到该玩法的号码.");
		}
	} 
	
	$("#ballSection").html(content);
    //任选框隐藏
    $("#rxWeiCheck").hide();
    //隐藏单式的导入框
    $("#importDsBtn").hide();
    
    //进行遗漏冷热的数值处理
    if(isOmitColdDataDeal){
    	$("#ommitHotColdBtn").show();
		lotteryCommonPage.currentLotteryKindInstance.showOmmitData();  //默认显示遗漏数据
    } else {
    	$("#ommitHotColdBtn").hide();
    }
	
};


/**
 * 单式玩法的号码拼接
 * specialKind 特殊玩法
 */
LotteryDataDeal.prototype.dsBallSectionCommon = function(specialKind){
	
	var content = '<div class="l-textarea"><textarea id="lr_editor" class="textArea">'+editorStr+'</textarea></div>';
	content += '<div class="l-btns"><div id="dsRemoveSame" class="l-btn">删除重复项</div>';
	content += '<div id="dsRemoveAll" class="l-btn">清空文本框</div></div>';
    $("#ballSection").html(content);
    if(specialKind == "RXEZXDS") {
    	$("#rxWeiCheck").show();
    	
    	lottertyDataDeal.param.isRxDsAllow = true;
    	//全选
    	$("input[name='ds_position']").each(function(){
    		this.checked = true;
    	});
    	lottertyDataDeal.param.dsAllowCount = 3;
    	$("#fnum").text(3);
    } else {
    	//任选框隐藏
    	$("#rxWeiCheck").hide();
    }
    //遗漏冷热隐藏
    $("#ommitHotColdBtn").hide();
    //显示单式的导入框
    $("#importDsBtn").show();
    
    //添加单式的事件处理
    lotteryCommonPage.addDsEvent();
}

/**
 * 计算当前用户最高模式下的奖金
 */
LotteryDataDeal.prototype.calucateCurrentUserHighestWin = function(kindKey){
	if(lotteryCommonPage.lotteryParam.currentLotteryWins.size() == 0){
		return "奖金未完全获取,请再次操作或者刷新页面";
	}
	var currentLotteryWin;
	currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get(kindKey);
	if(currentLotteryWin == null){
		return "未配置该玩法的奖金.";
	}
	var result = (currentLotteryWin.winMoney + (currentUserHighestModel - lowestAwardModel) * currentLotteryWin.specCode) * 1;	
    return result;
};

/**
 * 获取当前用户最高模式下的奖金
 */
LotteryDataDeal.prototype.getKindPlayLotteryWin = function(kindKey){
	if(lotteryCommonPage.lotteryParam.currentLotteryWins.size() == 0){
		return "奖金未完全获取,请稍作等待或者再次操作.";
	}
	var currentLotteryWin;
	currentLotteryWin = lotteryCommonPage.lotteryParam.currentLotteryWins.get("DPC_"+kindKey);
	if(currentLotteryWin == null){
		return "未配置该玩法的奖金.";
	}
	var result = (currentLotteryWin.winMoney + (currentUserHighestModel - lowestAwardModel) * currentLotteryWin.specCode) * 1;	
    return result.toFixed(lotteryCommonPage.param.toFixedValue);
};

/**
 * 设置当前用户最高模式下的奖金和玩法描述
 */
LotteryDataDeal.prototype.setKindPlayDes = function(){
	//三星复式
	var SXFS_WIN = lottertyDataDeal.getKindPlayLotteryWin("SXFS");
	SXFS_DES = "<div class='title'>每位至少选择一个号码，号码和位置都对应即中奖，最高奖金\""+ SXFS_WIN +"\"元";
	SXFS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	SXFS_DES += "<div class='selct-demo-center'><p>投注：678</p><p>开奖：678</p><p>奖金：\""+ SXFS_WIN +"\"元</p></div></div>";

	//三星单式
	var SXDS_WIN = lottertyDataDeal.getKindPlayLotteryWin("SXDS");
	SXDS_DES = "<div class='title'>每位至少选择一个号码，号码和位置都对应即中奖，最高奖金\""+ SXDS_WIN +"\"元";
	SXDS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	SXDS_DES += "<div class='selct-demo-center'><p>投注：345</p><p>开奖：345</p><p>奖金：\""+ SXDS_WIN +"\"元</p></div></div>";

	//三星直选和值
	var SXZXHZ_WIN = lottertyDataDeal.getKindPlayLotteryWin("SXZXHZ");
	SXZXHZ_DES = "<div class='title'>至少选择一个和值，竞猜开奖号码三位数字之和，最高奖金\""+ SXZXHZ_WIN +"\"元";
	SXZXHZ_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	SXZXHZ_DES += "<div class='selct-demo-center'><p>投注：和值1</p><p>开奖：**1,*1*,1**</p><p>奖金：\""+ SXZXHZ_WIN +"\"元</p></div></div>";

	//三星组选和值
	var SXZXHZ_G_WIN = lottertyDataDeal.getKindPlayLotteryWin("SXZXHZ_G");
	var SXZXHZ_G_2_WIN = lottertyDataDeal.getKindPlayLotteryWin("SXZXHZ_G_2");
	SXZXHZ_G_DES = "<div class='title'>至少选择一个和值，竞猜开奖号码三位数字之和(不含豹子号)，最高奖金\""+ SXZXHZ_G_WIN +"\"元";
	SXZXHZ_G_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	SXZXHZ_G_DES += "<div class='selct-demo-center'><p>投注：和值6</p><p>开奖：(1)015(不限顺序),奖金：\""+ SXZXHZ_G_2_WIN +"\"元<p></p>(2)033(不限顺序),奖金：\""+ SXZXHZ_G_WIN +"\"元</p></div></div>";

	//三星组三
	var SXZS_WIN = lottertyDataDeal.getKindPlayLotteryWin("SXZS");
	SXZS_DES = "<div class='title'>从0-9中选择2个数字组成两注，所选号码与开奖号码的三位相同，顺序不限，最高奖金\""+ SXZS_WIN +"\"元";
	SXZS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	SXZS_DES += "<div class='selct-demo-center'><p>投注：113</p><p>开奖：113(不限顺序)</p><p>奖金：\""+ SXZS_WIN +"\"元</p></div></div>";

	//前三组六
	var SXZL_WIN = lottertyDataDeal.getKindPlayLotteryWin("SXZL");
	SXZL_DES = "<div class='title'>从0-9中任意选择3个号码组成一注，所选号码与开奖号码的三位相同，顺序不限，最高奖金\""+ SXZL_WIN +"\"元";
	SXZL_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	SXZL_DES += "<div class='selct-demo-center'><p>投注：123</p><p>开奖：123(不限顺序)</p><p>奖金：\""+ SXZL_WIN +"\"元</p></div></div>";

	//三星一码不定位
	var SXYMBDW_WIN = lottertyDataDeal.getKindPlayLotteryWin("SXYMBDW");
	SXYMBDW_DES = "<div class='title'> 从0-9中至少选择1个号码投注，竞猜开奖号码三位中包含这个号码，包含即中奖，最高奖金\""+ SXYMBDW_WIN +"\"元";
	SXYMBDW_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	SXYMBDW_DES += "<div class='selct-demo-center'><p>投注：1</p><p>开奖：1xx(不限顺序)</p><p>奖金：\""+ SXYMBDW_WIN +"\"元</p></div></div>";

	//三星二码不定位
	var SXEMBDW_WIN = lottertyDataDeal.getKindPlayLotteryWin("SXEMBDW");
	SXEMBDW_DES = "<div class='title'> 从0-9中至少选择2个号码投注，竞猜开奖号码三位中包含这2个号码，包含即中奖，最高奖金\""+ SXEMBDW_WIN +"\"元";
	SXEMBDW_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	SXEMBDW_DES += "<div class='selct-demo-center'><p>投注：1,2</p><p>开奖：12x(不限顺序)</p><p>奖金：\""+ SXEMBDW_WIN +"\"元</p></div></div>";

	//前二直选复式
	var QEZXFS_WIN = lottertyDataDeal.getKindPlayLotteryWin("QEZXFS");	
	QEZXFS_DES = "<div class='title'> 每位至少选择一个号码，竞猜开奖号码的前二位，号码和位置都对应即中奖，最高奖金\""+ QEZXFS_WIN +"\"元";
	QEZXFS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	QEZXFS_DES += "<div class='selct-demo-center'><p>投注：56*</p><p>开奖：56*</p><p>奖金：\""+ QEZXFS_WIN +"\"元</p></div></div>";

	//前二直选单式
	var QEZXDS_WIN = lottertyDataDeal.getKindPlayLotteryWin("QEZXDS");
	QEZXDS_DES = "<div class='title'> 每位至少选择一个号码，竞猜开奖号码的前二位，号码和位置都对应即中奖，最高奖金\""+ QEZXDS_WIN +"\"元";
	QEZXDS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	QEZXDS_DES += "<div class='selct-demo-center'><p>投注：12*</p><p>开奖：12*</p><p>奖金：\""+ QEZXDS_WIN +"\"元</p></div></div>";

	//前二直选和值
	var QEZXHZ_WIN = lottertyDataDeal.getKindPlayLotteryWin("QEZXHZ");
	QEZXHZ_DES = "<div class='title'> 至少选择一个和值，竞猜开奖号码前二位数字之和，最高奖金\""+ QEZXHZ_WIN +"\"元";
	QEZXHZ_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	QEZXHZ_DES += "<div class='selct-demo-center'><p>投注：和值1</p><p>开奖：01*,10*</p><p>奖金：\""+ QEZXHZ_WIN +"\"元</p></div></div>";

	//前二组选复式
	var QEZXFS_G_WIN = lottertyDataDeal.getKindPlayLotteryWin("QEZXFS_G");
	QEZXFS_G_DES = "<div class='title'> 从0-9中选择2个数字组成一注，所选号码与开奖号码的前二位相同，顺序不限，最高奖金\""+ QEZXFS_G_WIN +"\"元";
	QEZXFS_G_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	QEZXFS_G_DES += "<div class='selct-demo-center'><p>投注：1,2</p><p>开奖：12*(不限顺序)</p><p>奖金：\""+ QEZXFS_G_WIN +"\"元</p></div></div>";

	//前二组选单式
	var QEZXDS_G_WIN = lottertyDataDeal.getKindPlayLotteryWin("QEZXDS_G");
	QEZXDS_G_DES = "<div class='title'> 从0-9中选择2个数字组成一注，所选号码与开奖号码的前二位相同，顺序不限，最高奖金\""+ QEZXDS_G_WIN +"\"元";
	QEZXDS_G_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	QEZXDS_G_DES += "<div class='selct-demo-center'><p>投注：5,6</p><p>开奖：56*(不限顺序)</p><p>奖金：\""+ QEZXDS_G_WIN +"\"元</p></div></div>";

	//前二组选和值
	var QEZXHZ_G_WIN = lottertyDataDeal.getKindPlayLotteryWin("QEZXHZ_G");
	QEZXHZ_G_DES = "<div class='title'> 所选数值等于开奖号码的前二位数字相加之和（不含对子），最高奖金\""+ QEZXHZ_G_WIN +"\"元";
	QEZXHZ_G_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	QEZXHZ_G_DES += "<div class='selct-demo-center'><p>投注：和值1</p><p>开奖：10*(不限顺序)</p><p>奖金：\""+ QEZXHZ_G_WIN +"\"元</p></div></div>";

	//后二直选复式
	var HEZXFS_WIN = lottertyDataDeal.getKindPlayLotteryWin("HEZXFS");
	HEZXFS_DES = "<div class='title'> 每位至少选择一个号码，竞猜开奖号码的后二位，号码和位置都对应即中奖，最高奖金\""+ HEZXFS_WIN +"\"元";
	HEZXFS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	HEZXFS_DES += "<div class='selct-demo-center'><p>投注：*56</p><p>开奖：*56</p><p>奖金：\""+ HEZXFS_WIN +"\"元</p></div></div>";

	//后二直选单式
	var HEZXDS_WIN = lottertyDataDeal.getKindPlayLotteryWin("HEZXDS");
	HEZXDS_DES = "<div class='title'> 每位至少选择一个号码，竞猜开奖号码的后二位，号码和位置都对应即中奖，最高奖金\""+ HEZXDS_WIN +"\"元";
	HEZXDS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	HEZXDS_DES += "<div class='selct-demo-center'><p>投注：*12</p><p>开奖：*12</p><p>奖金：\""+ HEZXDS_WIN +"\"元</p></div></div>";

	//后二直选和值
	var HEZXHZ_WIN = lottertyDataDeal.getKindPlayLotteryWin("HEZXHZ");
	HEZXHZ_DES = "<div class='title'> 至少选择一个和值，竞猜开奖号码后二位数字之和，最高奖金\""+ HEZXHZ_WIN +"\"元";
	HEZXHZ_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	HEZXHZ_DES += "<div class='selct-demo-center'><p>投注：和值1</p><p>开奖：*01,*10</p><p>奖金：\""+ HEZXHZ_WIN +"\"元</p></div></div>";

	//后二组选复式
	var HEZXFS_G_WIN = lottertyDataDeal.getKindPlayLotteryWin("HEZXFS_G");
	HEZXFS_G_DES = "<div class='title'> 从0-9中选择2个数字组成一注，所选号码与开奖号码的后二位相同，顺序不限，最高奖金\""+ HEZXFS_G_WIN +"\"元";
	HEZXFS_G_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	HEZXFS_G_DES += "<div class='selct-demo-center'><p>投注：1,2</p><p>开奖：*12(不限顺序)</p><p>奖金：\""+ HEZXFS_G_WIN +"\"元</p></div></div>";

	//后二组选单式
	var HEZXDS_G_WIN = lottertyDataDeal.getKindPlayLotteryWin("HEZXDS_G");
	HEZXDS_G_DES = "<div class='title'> 从0-9中选择2个数字组成一注，所选号码与开奖号码的后二位相同，顺序不限，最高奖金\""+ HEZXDS_G_WIN +"\"元";
	HEZXDS_G_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	HEZXDS_G_DES += "<div class='selct-demo-center'><p>投注：5,6</p><p>开奖：*56(不限顺序)</p><p>奖金：\""+ HEZXDS_G_WIN +"\"元</p></div></div>";

	//后二组选和值
	var HEZXHZ_G_WIN = lottertyDataDeal.getKindPlayLotteryWin("HEZXHZ_G");
	HEZXHZ_G_DES = "<div class='title'> 所选数值等于开奖号码的后二位数字相加之和（不含对子），最高奖金\""+ HEZXHZ_G_WIN +"\"元";
	HEZXHZ_G_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	HEZXHZ_G_DES += "<div class='selct-demo-center'><p>投注：和值1</p><p>开奖：*10(不限顺序)</p><p>奖金：\""+ HEZXHZ_G_WIN +"\"元</p></div></div>";

	//前二大小单双
	var QEDXDS_WIN = lottertyDataDeal.getKindPlayLotteryWin("QEDXDS");
	QEDXDS_DES = "<div class='title'> 对应百位以及十位选择大小单双，0、2、4 、6、8 为双；1、3、5、7、9 为单；0、1、2、3、4 为小；5、6、7、8、9 为大。最高奖金\""+ QEDXDS_WIN +"\"元";
	QEDXDS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	QEDXDS_DES += "<div class='selct-demo-center'><p>投注：大大、大双、单大、单双</p><p>开奖：98*</p><p>奖金：\""+ QEDXDS_WIN +"\"元</p></div></div>";

	//后二大小单双
	var HEDXDS_WIN = lottertyDataDeal.getKindPlayLotteryWin("HEDXDS");
	HEDXDS_DES = "<div class='title'> 对应个位以及十位选择大小单双，0、2、4 、6、8 为双；1、3、5、7、9 为单；0、1、2、3、4 为小；5、6、7、8、9 为大。最高奖金\""+ HEDXDS_WIN +"\"元";
	HEDXDS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	HEDXDS_DES += "<div class='selct-demo-center'><p>投注：大大、大双、单大、单双</p><p>开奖：*98</p><p>奖金：\""+ HEDXDS_WIN +"\"元</p></div></div>";

	//三星定位胆
	var SXDWD_WIN = lottertyDataDeal.getKindPlayLotteryWin("SXDWD");
	SXDWD_DES = "<div class='title'> 从百位、十位、个位任意位置上至少选择1个号码，选号与相同位置上的开奖号码一致，最高奖金\""+ SXDWD_WIN +"\"元";
	SXDWD_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	SXDWD_DES += "<div class='selct-demo-center'><p>投注：1**</p><p>开奖：1**</p><p>奖金：\""+ SXDWD_WIN +"\"元</p></div></div>";
	
	//任选二
	var RXE_WIN = lottertyDataDeal.getKindPlayLotteryWin("RXE");
	RXE_DES = "<div class='title'> 从百位、十位、个位中至少两位上各选1个号码，选号与相同位置上的开奖号码都一致，最高奖金\""+ RXE_WIN +"\"元";
	RXE_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	RXE_DES += "<div class='selct-demo-center'><p>投注：1*3</p><p>开奖：1*3</p><p>奖金：\""+ RXE_WIN +"\"元</p></div></div>";
	
	//任选二直选单式
	var RXEZXDS_WIN = lottertyDataDeal.getKindPlayLotteryWin("RXEZXDS");
	RXEZXDS_DES = "<div class='title'> 从百位、十位、个位中至少四位上各选择1个号码，选号与相同位置上的开奖号码一致，最高奖金\""+ RXEZXDS_WIN +"\"元";
	RXEZXDS_DES += "<div class='selct-demo' href='javascript:void(0)'><span>选号示例</span>";
	RXEZXDS_DES += "<div class='selct-demo-center'><p>投注：1*2</p><p>开奖：1*2</p><p>奖金：\""+ RXEZXDS_WIN +"\"元</p></div></div>";
	
	//展示当前玩法的描述
	lottertyDataDeal.showPlayDes(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey);
};

/**
 * 单式方案控制
 */
LotteryDataDeal.prototype.dsPositionCheckFunc = function(obj){
	var positionCount = 0;
	$("input[name='ds_position']").each(function(){
		if(this.checked){
			positionCount++;
		}
	});
	
    if(lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey == 'RXEZXDS'){
		//判断当前是否符合该单式的处理方式
		if(positionCount == 2){  //选择2位的时候,有一个方案
			lottertyDataDeal.param.isRxDsAllow = true;
			lottertyDataDeal.param.dsAllowCount = 1;
			$("#pnum").text(positionCount);
			$("#fnum").text(1);  
		}else if(positionCount == 3){
			lottertyDataDeal.param.isRxDsAllow = true;
			lottertyDataDeal.param.dsAllowCount = 3;
			$("#pnum").text(positionCount);
			$("#fnum").text(3);  
		}else if(positionCount == 4){
			lottertyDataDeal.param.isRxDsAllow = true;
			lottertyDataDeal.param.dsAllowCount = 6;
			$("#pnum").text(positionCount);
			$("#fnum").text(6);  
		}else if(positionCount == 5){
			lottertyDataDeal.param.isRxDsAllow = true;
			lottertyDataDeal.param.dsAllowCount = 10;
			$("#pnum").text(positionCount);
			$("#fnum").text(10);  
		}else{
			lottertyDataDeal.param.isRxDsAllow = false;
			lottertyDataDeal.param.dsAllowCount = 0;
			$("#pnum").text(positionCount);
			$("#fnum").text(0);  
		}
    }else{
    	alert("该直选单式未配置");
    }
    
    //格式化,统计该单式数目
    lotteryCommonPage.dealForDsFormat("mouseout");
};


/**
 * 三星直选和值,注数
 */
LotteryDataDeal.prototype.getLotteryCountByShanZhiHeZhi = function(everyPositionCodesInt){
	var cathecticCount = 0;
	if(everyPositionCodesInt == 0){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 1){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 2){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 3){
		cathecticCount += 10;
	}else if(everyPositionCodesInt == 4){
		cathecticCount += 15;
	}else if(everyPositionCodesInt == 5){
		cathecticCount += 21;
	}else if(everyPositionCodesInt == 6){
		cathecticCount += 28;
	}else if(everyPositionCodesInt == 7){
		cathecticCount += 36;
	}else if(everyPositionCodesInt == 8){
		cathecticCount += 45;
	}else if(everyPositionCodesInt == 9){
		cathecticCount += 55;
	}else if(everyPositionCodesInt == 10){
		cathecticCount += 63;
	}else if(everyPositionCodesInt == 11){
		cathecticCount += 69;
	}else if(everyPositionCodesInt == 12){
		cathecticCount += 73;
	}else if(everyPositionCodesInt == 13){
		cathecticCount += 75;
	}else if(everyPositionCodesInt == 14){
		cathecticCount += 75;
	}else if(everyPositionCodesInt == 15){
		cathecticCount += 73;
	}else if(everyPositionCodesInt == 16){
		cathecticCount += 69;
	}else if(everyPositionCodesInt == 17){
		cathecticCount += 63;
	}else if(everyPositionCodesInt == 18){
		cathecticCount += 55;
	}else if(everyPositionCodesInt == 19){
		cathecticCount += 45;
	}else if(everyPositionCodesInt == 20){
		cathecticCount += 36;
	}else if(everyPositionCodesInt == 21){
		cathecticCount += 28;
	}else if(everyPositionCodesInt == 22){
		cathecticCount += 21;
	}else if(everyPositionCodesInt == 23){
		cathecticCount += 15;
	}else if(everyPositionCodesInt == 24){
		cathecticCount += 10;
	}else if(everyPositionCodesInt == 25){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 26){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 27){
		cathecticCount += 1;
	}else{
		alert("无该和值的号码");
	}
	return cathecticCount;	
};

/**
 * 三星组选和值,注数
 */
LotteryDataDeal.prototype.getLotteryCountByShanZuHeZhi = function(everyPositionCodesInt){
	var cathecticCount = 0;
    if(everyPositionCodesInt == 1){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 2){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 3){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 4){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 5){
		cathecticCount += 5;
	}else if(everyPositionCodesInt == 6){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 7){
		cathecticCount += 8;
	}else if(everyPositionCodesInt == 8){
		cathecticCount += 10;
	}else if(everyPositionCodesInt == 9){
		cathecticCount += 11;
	}else if(everyPositionCodesInt == 10){
		cathecticCount += 13;
	}else if(everyPositionCodesInt == 11){
		cathecticCount += 14;
	}else if(everyPositionCodesInt == 12){
		cathecticCount += 14;
	}else if(everyPositionCodesInt == 13){
		cathecticCount += 15;
	}else if(everyPositionCodesInt == 14){
		cathecticCount += 15;
	}else if(everyPositionCodesInt == 15){
		cathecticCount += 14;
	}else if(everyPositionCodesInt == 16){
		cathecticCount += 14;
	}else if(everyPositionCodesInt == 17){
		cathecticCount += 13;
	}else if(everyPositionCodesInt == 18){
		cathecticCount += 11;
	}else if(everyPositionCodesInt == 19){
		cathecticCount += 10;
	}else if(everyPositionCodesInt == 20){
		cathecticCount += 8;
	}else if(everyPositionCodesInt == 21){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 22){
		cathecticCount += 5;
	}else if(everyPositionCodesInt == 23){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 24){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 25){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 26){
		cathecticCount += 1;
	}else{
		alert("无该和值的号码");
	}
	return cathecticCount;	
};

/**
 * 前二或者后二直选和值,注数
 */
LotteryDataDeal.prototype.getLotteryCountByErZhiHeZhi = function(everyPositionCodesInt){
	var cathecticCount = 0;
	if(everyPositionCodesInt == 0){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 1){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 2){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 3){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 4){
		cathecticCount += 5;
	}else if(everyPositionCodesInt == 5){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 6){
		cathecticCount += 7;
	}else if(everyPositionCodesInt == 7){
		cathecticCount += 8;
	}else if(everyPositionCodesInt == 8){
		cathecticCount += 9;
	}else if(everyPositionCodesInt == 9){
		cathecticCount += 10;
	}else if(everyPositionCodesInt == 10){
		cathecticCount += 9;
	}else if(everyPositionCodesInt == 11){
		cathecticCount += 8;
	}else if(everyPositionCodesInt == 12){
		cathecticCount += 7;
	}else if(everyPositionCodesInt == 13){
		cathecticCount += 6;
	}else if(everyPositionCodesInt == 14){
		cathecticCount += 5;
	}else if(everyPositionCodesInt == 15){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 16){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 17){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 18){
		cathecticCount += 1;
	}else{
		alert("无该和值的号码");
	}
	return cathecticCount;
};

/**
 * 前二或者后二组选和值,注数
 */
LotteryDataDeal.prototype.getLotteryCountByErZuHeZhi = function(everyPositionCodesInt){
	var cathecticCount = 0;
	if(everyPositionCodesInt == 1){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 2){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 3){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 4){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 5){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 6){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 7){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 8){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 9){
		cathecticCount += 5;
	}else if(everyPositionCodesInt == 10){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 11){
		cathecticCount += 4;
	}else if(everyPositionCodesInt == 12){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 13){
		cathecticCount += 3;
	}else if(everyPositionCodesInt == 14){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 15){
		cathecticCount += 2;
	}else if(everyPositionCodesInt == 16){
		cathecticCount += 1;
	}else if(everyPositionCodesInt == 17){
		cathecticCount += 1;
	}else{
		alert("无该和值的号码");
	}
	return cathecticCount;
};
