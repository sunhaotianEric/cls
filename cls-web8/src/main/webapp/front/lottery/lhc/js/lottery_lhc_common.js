function LotteryCommonPage() {
}
var lotteryCommonPage = new LotteryCommonPage();

lotteryCommonPage.currentLotteryKindInstance = null;
lotteryCommonPage.param = {
	diffTime : 0, // 时间倒计时剩余时间
	currentExpect : null, // 当前投注期号
	currentLastOpenExpect : null, // 当前最新的开奖期号
	intervalKey : null, // 倒计时周期key
	toFixedValue : 4, // 小数点保留4为数字
	isFirstOpenMusic : false, // 开奖音乐首次进入页面关
	isCanActiveExpect : true,
	isClose : false
// 是否封盘了
};
// 投注订单
lotteryCommonPage.lotteryOrder = {};
/**
 * 投注参数
 */
lotteryCommonPage.lotteryParam = {
	SOURCECODE_SPLIT : ",",
	SOURCECODE_SPLIT_KS : "&",
	CODE_REPLACE : "-",
	SOURCECODE_ARRANGE_SPLIT : "|",
	currentTotalLotteryCount : 0,
	currentTotalLotteryPrice : 0.0000,
	currentZhuiCodeTotalLotteryPrice : 0.0000, // 追号的总共价格
	currentZhuiCodeTotalLotteryCount : 0,
	currentIsUpdateCodeStastic : false,
	updateCodeStasticIndex : null,
	todayTraceList : new Array(),
	tomorrowTraceList : new Array(),
	isZhuiCode : false, // 是否追号
	currentLotteryWins : new JS_OBJECT_MAP(), // 奖金映射
	lotteryMap : new JS_OBJECT_MAP(), // 投注映射
	lotteryMultipleMap : new JS_OBJECT_MAP(), // 投注倍数映射
	modelValue : null,
	awardModel : "YUAN",
	beishu : 1
// 默认是1倍
};

lotteryCommonPage.otherParam = {
	disturb : false,
	oriKindName : null,
	model : null,
	tip : null,
	lotteryType : null,
	lotteryTypeDes : null,
	lotteryExpect : null,
	money : null
};

$(document).ready(function() {

	// 顶部导航选中处理
	$("#navChoose .nav-child").removeClass("on");
	$("#navChoose .nav-child:eq(1)").addClass("on");

	// 先加载所有的玩法
	lotteryDataDeal.loadLotteryKindPlay();
	// 设置原有彩种类型
	lotteryCommonPage.otherParam.oriKindName = lotteryCommonPage.currentLotteryKindInstance.param.kindName;

	// 选择地区事件
	$(".m-banner .b-child1 .btn").click(function() {
		$(".b-child-list").stop(false, true).slideToggle(500);
	});
	// 切换背景事件
	$(".change-bg img").click(function() {
		var url = $(this).attr("data-val");
		$("body").css({
			"background-image" : "url(" + url + ")"
		});
	});

	// //当前遗漏或者冷热值
	// lotteryCommonPage.omitClodEvent();

	// 下方最近投注记录和追号记录的切换
	$("#userTodayLottery").unbind("click").click(function() {
		if ($(this).hasClass("on")) {
			return;
		}
		$("#userTodayLotteryAfterList").hide();
		$("#userTodayLotteryList").show();
		$(this).addClass("on");
		$("#userTodayLotteryAfter").removeClass("on");
	});
	$("#userTodayLotteryAfter").unbind("click").click(function() {
		if ($(this).hasClass("on")) {
			return;
		}
		// 显示查询中
		$("#userTodayLotteryAfterList").html("<div class='line'><p class='no-msg'>正在查询中...</p></div>");
		lotteryCommonPage.getTodayLotteryAfterByUser();

		$("#userTodayLotteryList").hide();
		$("#userTodayLotteryAfterList").show();
		$(this).addClass("on");
		$("#userTodayLottery").removeClass("on");
	});

	//	

	// //获取当前彩种的最新期号和投注倒计时
	lotteryCommonPage.getActiveExpect();
	// 近10期的开奖记录
	lotteryCommonPage.getNearestTenLotteryCode();
	// 获取用户今日投注记录
	lotteryCommonPage.getTodayLotteryByUser();
	window.setInterval("lotteryCommonPage.getLastLotteryCode()", 60000); // 1分钟重新读取
	window.setInterval("lotteryCommonPage.getActiveExpect()", 10000); // 1分钟重新读取服务器

	// 近一期、近五期tab切换
	$(".m-banner .change .child").click(function() {
		$(".m-banner .change .child").removeClass("on");
		$(this).addClass("on");
		var index = $(".m-banner .change .child").index($(this));
		if (index == 0) {
			$(".m-banner .model4").hide();
			$(".m-banner .model3").show();
		} else if (index == 1) {
			$(".m-banner .model4").show();
			$(".m-banner .model3").hide();
		}
	});

	// 顶部导航链接选中样式
	$('.header .foot .nav .child2').addClass('on');

	// 显示顶部换色
	$('#changecolor-show').show();

	// 开奖音乐
	$("#bgMusic").attr("src", contextPath + "/resources/music/kj.mp3");

	$(".btn-Music").click(function() {
		if ($(".btn-Music").html() == "关闭音乐") {
			$(".btn-Music").html("开启音乐");
			lotteryCommonPage.currentLotteryKindInstance.param.openMusic = false;
		} else {
			$(".btn-Music").html("关闭音乐");
			lotteryCommonPage.currentLotteryKindInstance.param.openMusic = true;
		}
	});
});

/**
 * 系统投注逻辑处理
 */
LotteryCommonPage.prototype.userLottery = function(lotteryOrder) {
	if (lotteryCommonPage.param.isClose) {

		frontCommonPage.showKindlyReminder("封盘中,禁止投注！");
		return;
	}
	$("#lotteryOrderDialogSureButton").val("提交中.....");
	$("#lotteryOrderDialogSureButton").attr('disabled', 'disabled');

	$.ajax({
		type : "POST",
		url : contextPath + "/lottery/userLottery",
		data : JSON.stringify(lotteryOrder),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			lotteryCommonPage.hideLotteryMsg();
			if (result.code == "ok") {
				lotteryCommonPage.lotteryEnd(); // 投注结束动作
				frontCommonPage.showKindlyReminder("投注下单成功.");
				lotteryCommonPage.getTodayLotteryByUser(); // 下单成功里面加载用户的投注记录
			} else if (result.code == "error") {
				if (r[1].indexOf("投注期号不正确.") != -1) {
					frontCommonPage.showKindlyReminder(result.data);
				} else {
					frontCommonPage.showKindlyReminder(result.data);
				}
			}
		}
	});
};

/**
 * 获取系统当前正在投注的期号
 */
LotteryCommonPage.prototype.getActiveExpect = function() {
	if (lotteryCommonPage.param.isCanActiveExpect) { // 判断是否可以进行期号的请求

		var jsonData = {
			"lotteryKind" : lotteryCommonPage.currentLotteryKindInstance.param.kindName,
		};
		$.ajax({
			type : "POST",
			url : contextPath + "/code/getExpectAndDiffTime",
			data : JSON.stringify(jsonData),
			dataType : "json",
			contentType : 'application/json;charset=utf-8',
			success : function(result) {
				commonHandlerResult(result, this.url);
				if (result.code == "ok") {
					lotteryCommonPage.param.isCanActiveExpect = false;
					var issue = result.data;
					if (issue != null) {
						// 展示当前期号
						lotteryCommonPage.currentLotteryKindInstance.showCurrentLotteryCode(issue);
						// 判断当前期号是否发生变化
						if (lotteryCommonPage.param.currentExpect == null || lotteryCommonPage.param.currentExpect != issue.lotteryNum) {
							// 记录当前期号
							lotteryCommonPage.param.currentExpect = issue.lotteryNum;
						}
						// 当前期号剩余时间倒计时
						lotteryCommonPage.param.diffTime = issue.timeDifference;

						window.clearInterval(lotteryCommonPage.param.intervalKey); // 终止周期性倒计时

						// 如果是特殊的时间段
						if (issue.specialTime == 1) { // 特殊时长
							// lotteryCommonPage.currentLotteryKindInstance.calculateTime(issue.isClose);
							lotteryCommonPage.param.intervalKey = setInterval("lotteryCommonPage.currentLotteryKindInstance.calculateTime(" + issue.isClose + ")", 1000);
						} else {
							lotteryCommonPage.calculateTime(); // 支持最多一个小时
							lotteryCommonPage.param.intervalKey = setInterval("lotteryCommonPage.calculateTime()", 1000);
						}

					} else {
						// 显示预售中
						$("#timer").hide();
						$("#presellTip").show();
					}

					if (lotteryCommonPage.param.diffTime > 5000) { // 如果剩余时间超过5秒的
						setTimeout("lotteryCommonPage.controlIsCanActive()", 2000);
					} else {
						lotteryCommonPage.param.isCanActiveExpect = true;
					}
				} else if (result.code == "error") {
					frontCommonPage.showKindlyReminder(result.data);
				}
			}
		});
	}
};

/**
 * 控制是否能发起期号的请求
 */
LotteryCommonPage.prototype.controlIsCanActive = function() {
	lotteryCommonPage.param.isCanActiveExpect = true;
};

;

/**
 * 获取最新的开奖号码
 */
LotteryCommonPage.prototype.getLastLotteryCode = function() {
	
	var jsonData = {
		"lotteryKind" : lotteryCommonPage.currentLotteryKindInstance.param.kindName,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/code/getLastLotteryCode",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if( result.code == "ok"){
				var lotteryCode = result.data;
				lotteryCommonPage.currentLotteryKindInstance.showLastLotteryCode(lotteryCode);

				var lotteryNum = lotteryCode.lotteryNum;
				// 当开奖号码发生变化
				if (lotteryCommonPage.param.currentLastOpenExpect == null || lotteryCommonPage.param.currentLastOpenExpect != lotteryNum) {
					// 动画展示开奖号码
					lotteryCommonPage.getLotteryAnimation();
					// 重新获取近10期的开奖记录
					lotteryCommonPage.getNearestTenLotteryCode();

					// 获取用户今日投注记录，刷新下方的中奖状态
					lotteryCommonPage.getTodayLotteryByUser();

					// 刷新冷热遗漏数据
					// lotteryCommonPage.getOmmitAndHotCold();
				}
				lotteryCommonPage.param.currentLastOpenExpect = lotteryNum; // 存储当前期号
			}else if(result.code == "error"){
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 开奖结果动画设置
 */
LotteryCommonPage.prototype.getLotteryAnimation = function() {
	var dtime;
	$(".m-banner .b-child2 .btns .btn").each(function(i, n) {
		dtime = 0.2 * i;
		base.anClasAdd(".m-banner .b-child2 .btns .btn:eq(" + i + ")", "scale", "1s", dtime + "s");
	});
	var dtime1 = (dtime + 1) * 1000;
	setTimeout(function() {
		$(".m-banner .b-child2 .btns .btn").attr("style", "");
	}, dtime1);
}

/**
 * 获取最近十期的开奖号码
 */
LotteryCommonPage.prototype.getNearestTenLotteryCode = function() {

	var jsonData = {
		"lotteryKind" : lotteryCommonPage.currentLotteryKindInstance.param.kindName,
	};
	
	$.ajax({
		type : "POST",
		url : contextPath + "/code/getNearestTenLotteryCode",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if( result.code == "ok"){
				var lotteryCodeList = result.data;
				lotteryCommonPage.showNearestTenLotteryCode(lotteryCodeList);
			}else if(result.code == "error"){
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
}

/**
 * 刷新最近十期的开奖号码的显示
 */
LotteryCommonPage.prototype.showNearestTenLotteryCode = function(lotteryCodeList) {
	if (lotteryCodeList != null && lotteryCodeList.length > 0) {
		$("#nearestTenLotteryCode").html("");
		var str = "";
		var afterStr = "";
		str += "<ul class='c-title'>";
		str += "	<li class='child child1'>期号</li>";
		str += "	<li class='child child2' style='width:162px;'>开奖号</li>";
		str += "</ul>";

		for (var i = 0; i < lotteryCodeList.length; i++) {
			var lotteryCode = lotteryCodeList[i];
			var openCodeStr = lotteryCommonPage.currentLotteryKindInstance.getOpenCodeStr(lotteryCode.lotteryNum);
			var temStr = "";
			temStr += "<ul class='c-list'>";
			temStr += "<li class='child child1'>" + openCodeStr + "</li>";
			temStr += "<li class='child child2' style='width:162px;'>" + lotteryCode.codesStr + "</li>";
			temStr += "</ul>";
			if (i < 4) {
				str += temStr;
			} else {
				afterStr += temStr;
			}
		}
		$("#nearestTenLotteryCode").html(str);
		$("#afterLotteryCode").html(afterStr);
	} else {
		$("#nearestTenLotteryCode").html("<ul class='c-list'><span>未加载到开奖记录</span></ul>");
	}

	// 绑定显示后面开奖号码事件
	$("#showAfterLotteryCodeBtn").unbind("click").click(function() {
		if ($("#afterLotteryCode ul").length <= 0) {
			return;
		}
		if ($(this).attr("click-flag") == "true") {
			return;
		}
		$(this).attr("click-flag", "true");
		$("#nearestTenLotteryCode ul:eq(1)").slideUp(500, function() {
			$(this).appendTo($("#preLotteryCode"));
			$("#afterLotteryCode ul:eq(0)").show();
			$("#afterLotteryCode ul:eq(0)").appendTo($("#nearestTenLotteryCode"));
			$("#showAfterLotteryCodeBtn").attr("click-flag", "false");
		});
	});

	// 绑定显示前面开奖号码事件
	$("#showPreLotteryCodeBtn").unbind("click").click(function() {
		if ($("#preLotteryCode ul").length <= 0) {
			return;
		}
		if ($(this).attr("click-flag") == "true") {
			return;
		}
		$(this).attr("click-flag", "true");
		$("#preLotteryCode ul:last").insertAfter($("#nearestTenLotteryCode ul:eq(0)"));
		$("#nearestTenLotteryCode ul:eq(1)").slideDown(500);
		$("#nearestTenLotteryCode ul:last").prependTo($("#afterLotteryCode"));
		$("#showPreLotteryCodeBtn").attr("click-flag", "false");
	});

}

/**
 * 展示投注信息
 */
LotteryCommonPage.prototype.showLotteryMsg = function() {
	if (lotteryCommonPage.param.isClose) {
		frontCommonPage.showKindlyReminder("封盘中,禁止投注！");
		return;
	}
	var model = lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentKindKey;
	var lotteryMsgHtml = '';
	lotteryMsgHtml += '<div class="msg-head">';
	lotteryMsgHtml += '<p>注单确认</p>';
	lotteryMsgHtml += '<img class="close" src="'
			+ contextPath
			+ '/front/images/user/close.png" onClick=\'(function msg_show(){$(".alert-liuhecai-msg").stop(false,true).delay(200).fadeOut(500);$(".alert-msg-bg").stop(false,true).fadeOut(500);})()\' />';
	lotteryMsgHtml += '</div>';
	lotteryMsgHtml += '<div class="msg-content">';
	lotteryMsgHtml += '<div class="msg-list"><ul>';
	lotteryMsgHtml += '<li class="msg-list-li msg-list-t">';
	lotteryMsgHtml += '<div class="child child1">序号</div>';
	lotteryMsgHtml += '<div class="child child2">内容</div>';
	lotteryMsgHtml += '<div class="child child3">赔率</div>';
	lotteryMsgHtml += '<div class="child child4">下注金额</div></li>';

	var modelObject = lotteryDataDeal.modelMap.get(model);
	lotteryCommonPage.lotteryOrder = {};
	var resultArray = new Array();
	var totalMoney = 0, lotteryNum = 0;
	var codePlArr = new Array();
	// 复选框遍历,连码，合肖，连肖，尾数连，全不中
	if (model == 'LMSQZ' || model == 'LMSZ22' || model == 'LMSZ23' || model == 'LMEQZ' || model == 'LMEZT' || model == 'LMEZ2' || model == 'LMTC' || model == 'LMSZ1'
			|| model == 'HX2' || model == 'HX3' || model == 'HX4' || model == 'HX5' || model == 'HX6' || model == 'HX7' || model == 'HX8' || model == 'HX9' || model == 'HX10'
			|| model == 'HX11' || model == 'WSLELZ' || model == 'WSLSLZ' || model == 'WSLSILZ' || model == 'WSLELBZ' || model == 'WSLSLBZ' || model == 'WSLSILBZ'
			|| model == 'LXELZ' || model == 'LXSLZ' || model == 'LXSILZ' || model == 'LXWLZ' || model == 'LXELBZ' || model == 'LXSLBZ' || model == 'LXSILBZ' || model == 'QBZ5'
			|| model == 'QBZ6' || model == 'QBZ7' || model == 'QBZ8' || model == 'QBZ9' || model == 'QBZ10' || model == 'QBZ11' || model == 'QBZ12') {
		var j = 0;
		var codes = '';
		var codeDesc = '';// 内容
		var codeNun = 0;
		var shortcutMoney = $(".shortcut-money").val();
		if (shortcutMoney == "") {
			alert("请输入下注金额！");
			return;
		}
		var k = 0;
		for (var i = 1; i <= modelObject.maxNum; i++) {

			if ($('#money' + i).is(':checked') && shortcutMoney != "") {
				codeNun++;
				codes += i + ",";
				codeDesc += lotteryDataDeal.playNumDescMap[model + i] + ",";
				var codePl = $("#pl" + i).text();
				codePlArr[k] = codePl;
				k++;
			}
		}

		if (codeNun >= modelObject.lotteryCodeNum) {
			// 统计注数，金钱
			lotteryNum = lotteryCommonPage.countLotteryNum(codeNun, modelObject.lotteryCodeNum);
			totalMoney = Number(shortcutMoney) * Number(lotteryNum);
			var resultArrayDetail = {};
   			resultArrayDetail.playKindStr = model;//内容
   			resultArrayDetail.playKindCodes = codes.substring(0, codes.length - 1);//号码
   			resultArrayDetail.lotteryMultiple = shortcutMoney;//投注金额
   			resultArray[j] = resultArrayDetail;
			// lotteryDataDeal.playNumDescMap
			lotteryMsgHtml += '<li class="msg-list-li">';
			lotteryMsgHtml += '<div class="child child1">' + (j + 1) + '</div>';
			lotteryMsgHtml += '<div class="child child2">' + codeDesc.substring(0, codeDesc.length - 1) + '</div>';
			lotteryMsgHtml += '<div class="child child3">' + lotteryCommonPage.returnMinPl(codePlArr) + '</div>';
			lotteryMsgHtml += '<div class="child child4">' + shortcutMoney + '</div></li>';
		} else {
			alert("至少要选择" + modelObject.lotteryCodeNum + "个号码！");

			return;
		}
		// 文本框遍历
	} else {
		var j = 0;
		for (var i = 1; i <= modelObject.maxNum; i++) {
			var money = $("#money" + i).val();
			if (money != '' && !isNaN(money)) {
				lotteryNum++;
				var resultArrayDetail = {};
	   			resultArrayDetail.playKindStr = model;//内容
	   			resultArrayDetail.playKindCodes = i;// 号码
	   			resultArrayDetail.lotteryMultiple = money;// 投注金额
	   			resultArray[j] = resultArrayDetail;

				lotteryMsgHtml += '<li class="msg-list-li">';
				lotteryMsgHtml += '<div class="child child1">' + (j + 1) + '</div>';
				lotteryMsgHtml += '<div class="child child2">' + lotteryDataDeal.playNumDescMap[model + i] + '</div>';
				lotteryMsgHtml += '<div class="child child3">' + $('#pl' + i).text() + '</div>';
				lotteryMsgHtml += '<div class="child child4">' + money + '</div></li>';
				totalMoney += Number(money);
				j++;
			}
		}

	}
	if (totalMoney == 0) {
		frontCommonPage.showKindlyReminder("请选择投注号码及金额！");
		return;
	}
	lotteryMsgHtml += '</ul></div>';
	lotteryMsgHtml += '<div class="line no-border">';
	lotteryMsgHtml += '<div class="line-title">期数：</div>';
	lotteryMsgHtml += '<div class="line-content">';
	lotteryMsgHtml += '<div class="line-text"><p >' + lotteryCommonPage.param.currentExpect + '</p></div>';
	lotteryMsgHtml += '</div></div>';
	lotteryMsgHtml += '<div class="line no-border">';
	lotteryMsgHtml += '<div class="line-title">玩法：</div>';
	lotteryMsgHtml += '<div class="line-content">';
	lotteryMsgHtml += '<div class="line-text"><p class="blue">' + modelObject.playDesc + '</p></div>';
	lotteryMsgHtml += '</div></div>';
	lotteryMsgHtml += '<div class="line no-border">';
	lotteryMsgHtml += '<div class="line-title">总注数：</div>';
	lotteryMsgHtml += '<div class="line-content">';
	lotteryMsgHtml += '<div class="line-text"><p class="blue">' + lotteryNum + '</p></div>';
	lotteryMsgHtml += '</div></div>';
	lotteryMsgHtml += '<div class="line no-border">';
	lotteryMsgHtml += '<div class="line-title">总下注额：</div>';
	lotteryMsgHtml += '<div class="line-content">';
	lotteryMsgHtml += '<div class="line-text"><p class="blue">' + totalMoney + '</p></div>';
	lotteryMsgHtml += '</div></div>';
	lotteryMsgHtml += '<div class="btn">';
	lotteryMsgHtml += '<input type="button" class="inputBtn" value="确定" id="lotteryOrderDialogSureButton" />';
	lotteryMsgHtml += '<input type="button" class="inputBtn" value="取消" onClick=\'(function msg_show(){$(".alert-liuhecai-msg").stop(false,true).delay(200).fadeOut(500);$(".alert-msg-bg").stop(false,true).fadeOut(500);})()\' />';
	lotteryMsgHtml += '</div></div>';

	var lotteryOrder = {};
	
	lotteryOrder.lotteryKind = lotteryCommonPage.currentLotteryKindInstance.param.kindName; // 投注彩种
	lotteryOrder.model = lotteryCommonPage.lotteryParam.awardModel;
	lotteryOrder.awardModel = currentUser.sscRebate;
	lotteryOrder.isLotteryByPoint = 0;
	lotteryOrder.playKindMsgs = resultArray;
	lotteryOrder.expect = lotteryCommonPage.param.currentExpect;
	$("#liuhecai_msg").html(lotteryMsgHtml);
	$("#liuhecai_msg").stop(false, true).delay(200).fadeIn(500);
	$(".alert-msg-bg").stop(false, true).fadeIn(500);

	// 确认投注-提交后台投注事件
	$("#lotteryOrderDialogSureButton").unbind("click").click(function() {
		lotteryCommonPage.userLottery(lotteryOrder);
	});

	return true;
}
/**
 * codeNun =选择的号码个数 needNum = 玩法需要的号码个数
 */
LotteryCommonPage.prototype.countLotteryNum = function(codeNun, needNum) {
	codeNun = Number(codeNun);
	needNum = Number(needNum);
	var sum = 1;
	for (var i = 1; i <= needNum; i++) {
		sum = sum * codeNun / i;
		codeNun--;
	}
	return sum;
}

/**
 * 计算出最低赔率！
 */
LotteryCommonPage.prototype.returnMinPl = function(plArr) {
	var pl = Number(plArr[0]);
	for (var i = 1; i < plArr.length; i++) {
		var pltemp = Number(plArr[i]);
		if (pl > pltemp) {
			pl = pltemp;
		}
	}
	return pl;
};

/**
 * 关闭投注信息
 */
LotteryCommonPage.prototype.hideLotteryMsg = function() {
	$(".alert-liuhecai-msg").stop(false, true).delay(200).fadeOut(500);
	$(".alert-msg-bg").stop(false, true).fadeOut(500);
	$("#lotteryOrderDialogSureButton").removeAttr("disabled");
	$("#lotteryOrderDialogSureButton").val("确认投注");
}
/**
 * 投注结束动作
 */
LotteryCommonPage.prototype.lotteryEnd = function() {
	$("#lhcReset").click();
	lotteryCommonPage.lotteryParam.currentTotalLotteryCount = 0;
	lotteryCommonPage.lotteryParam.currentTotalLotteryPrice = 0.000;
	lotteryCommonPage.lotteryParam.currentZhuiCodeTotalLotteryPrice = 0.000;
	lotteryCommonPage.lotteryParam.currentZhuiCodeTotalLotteryCount = 0;
	lotteryCommonPage.lotteryParam.currentIsUpdateCodeStastic = false;
	lotteryCommonPage.lotteryParam.updateCodeStasticIndex = null;
	lotteryCommonPage.lotteryParam.isZhuiCode = false;
	lotteryCommonPage.lotteryParam.lotteryMap = new JS_OBJECT_MAP();
	lotteryCommonPage.lotteryParam.lotteryMultipleMap = new JS_OBJECT_MAP();
	lotteryCommonPage.lotteryParam.beishu = 1;
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryCount = 0;
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryPrice = 0;
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.currentLotteryTotalCount = 0;
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codes = null;
	lotteryCommonPage.currentLotteryKindInstance.lotteryParam.codesStastic = new Array();
};

/**
 * 获取今日投注数据
 */
LotteryCommonPage.prototype.getTodayLotteryByUser = function() {
	
	$.ajax({
		type : "POST",
		url : contextPath + "/lottery/getTodayLotteryByUser",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if( result.code == "ok"){
				var todayLotteryList = result.data;
				if (todayLotteryList.length > 0) {

					// 彩种游戏下方的游戏记录
					var userOrderListObj = $("#userTodayLotteryList");
					userOrderListObj.html("");
					str = "";
					str += "<div class='line line-titles'>";
					str += "	<div class='child child1' style='width:170px'>订单编号</div>";
					str += "	<div class='child child2'>彩种</div>";
					str += "	<div class='child child3'>期号</div>";
					str += "	<div class='child child4'>投注金额</div>";
					str += "	<div class='child child5'>奖金</div>";
					str += "	<div class='child child6'>模式</div>";
					str += "	<div class='child child7'>下单时间</div>";
					str += "	<div class='child child8'>状态</div>";
					str += "	<div class='child child9 no'>内容</div>";
					str += "</div>";
					for (var i = 0; i < todayLotteryList.length; i++) {
						// 显示5条最近投注记录
						if (i > 4) {
							break;
						}
						var userOrder = todayLotteryList[i];
						var lotteryIdStr = userOrder.lotteryId;

						// 订单状态特殊处理
						var prostateStatus = userOrder.prostateStatusStr;
						if (userOrder.prostate == "REGRESSION" || userOrder.prostate == "END_STOP_AFTER_NUMBER" || userOrder.prostate == "NO_LOTTERY_BACKSPACE"
								|| userOrder.prostate == "UNUSUAL_BACKSPACE" || userOrder.prostate == "STOP_DEALING") {
							prostateStatus = "已撤单";
						}
						str += "<div class='line'>";
						str += "  <div class='child child1' style='width:170px'>" + lotteryIdStr.substring(lotteryIdStr.lastIndexOf('_') + 1, lotteryIdStr.length) + "</div>";
						str += "  <div class='child child2'>" + userOrder.lotteryTypeDes + "</div>";
						str += "  <div class='child child3'>" + userOrder.expect + "</div>";
						str += "  <div class='child child4'>" + userOrder.payMoney.toFixed(frontCommonPage.param.fixVaue) + "</div>";
						str += "  <div class='child child5'>" + userOrder.winCost.toFixed(frontCommonPage.param.fixVaue) + "</div>";
						str += "  <div class='child child6'>" + userOrder.lotteryModelStr + "</div>";
						/*
						 * if(userOrder.afterNumber == 1){ str += " <span
						 * style='width:5%;'><font class='cp-yellow'>是</font></span>";
						 * }else{ str += " <span style='width:5%;'><font
						 * class='cp-green'>否</font></span>"; }
						 */
						str += "  <div class='child child7'>" + userOrder.createdDateStr + "</div>";

						if (userOrder.prostate == 'DEALING') {
							str += "  <div class='child child8'><font class='cp-yellow'>" + prostateStatus + "</font></div>";
						} else if (userOrder.prostate == 'NOT_WINNING') {
							str += "  <div class='child child8'><font class='cp-red'>" + prostateStatus + "</font></div>";
						} else {
							str += "  <div class='child child8'><font class='cp-green'>" + prostateStatus + "</font></div>";
						}
						str += "  <div class='child child9'><a href='javascript:void(0);' name='lotteryMsgRecord' data-lottery='" + userOrder.lotteryId + "'  data-type='"
								+ userOrder.stopAfterNumber + "' data-value='" + userOrder.id + "'>查看</a></div>";
						str += "</div>";
						userOrderListObj.append(str);
						str = "";
					}

					var afterNumberLotteryWindow;

				} else {
					$("#userTodayLotteryList").html("<div class='line'><p class='no-msg'>还没有投注记录哟~</p></div>");
				}

				// 查看投注详情
				$("a[name='lotteryMsgRecord']").unbind("click").click(function(event) {
					var orderId = $(this).attr("data-value");
					var dataType = $(this).attr("data-type");
					var width = 800;
					var height = 400;
					var left = parseInt((screen.availWidth / 2) - (width / 2));// 屏幕居中
					var top = parseInt((screen.availHeight / 2) - (height / 2));
					var windowFeatures = "width=" + width + ",height=" + height + ",status,resizable,scrollbars=yes,left=" + left + ",top=" + top;

					if (dataType == 0) {
						var lotteryhrefStr = contextPath + "/gameBet/orderDetail.html?orderId="+orderId;
					} else {
						var dataLottery = $(this).attr("data-lottery");
						var lotteryhrefStr = contextPath + "/gameBet/followOrderDetail.html?orderId="+dataLottery;
					}
					afterNumberLotteryWindow = window.open(lotteryhrefStr, "subWind", windowFeatures);
					afterNumberLotteryWindow.focus();
				});
			}else if(result.code == "error"){
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
}

/**
 * 获取用户的今日追号投注记录
 */
LotteryCommonPage.prototype.getTodayLotteryAfterByUser = function() {
	
	$.ajax({
		type : "POST",
		url : contextPath + "/lottery/getTodayLotteryAfterByUser",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if( result.code == "ok"){
				var todayLotteryList = result.data;
				if (todayLotteryList.length > 0) {

					// 彩种游戏下方的游戏记录
					var userOrderListObj = $("#userTodayLotteryAfterList");
					userOrderListObj.html("");
					str = "";
					str += "<div class='line line-titles'>";
					str += "	<div class='child child1' style='width:170px'>订单编号</div>";
					str += "	<div class='child child2'>彩种</div>";
					str += "	<div class='child child3'>开始期数</div>";
					str += "	<div class='child child4'>追号期数</div>";
					str += "	<div class='child child5'>完成期数</div>";
					str += "	<div class='child child6' style='width:80px'>总金额</div>";
					str += "	<div class='child child7' style='width:90px'>完成金额</div>";
					str += "	<div class='child child8'>中奖金额</div>";
					str += "	<div class='child child8'>模式</div>";
					str += "	<div class='child child10' style='width:130px'>下单时间</div>";
					str += "	<div class='child child11' style='width:50px'>状态</div>";
					str += "	<div class='child child12 no' style='text-align: center;'>内容</div>";
					str += "</div>";
					for (var i = 0; i < todayLotteryList.length; i++) {
						if (i > 4) {
							break;
						}
						var userOrder = todayLotteryList[i];
						var lotteryIdStr = userOrder.lotteryId;

						str += "<div class='line line-titles'>";
						str += "  <div class='child child1' style='width:170px'>" + lotteryIdStr.substring(lotteryIdStr.lastIndexOf('-') + 1, lotteryIdStr.length) + "</div>";
						str += "  <div class='child child2'>" + userOrder.lotteryTypeDes + "</div>";
						str += "  <div class='child child3'>" + userOrder.expect + "</div>";
						str += "  <div class='child child4'>" + userOrder.afterNumberCount + "</div>";
						str += "  <div class='child child5'>" + userOrder.yetAfterNumber + "</div>";
						str += "  <div class='child child6' style='width:80px'>" + userOrder.afterNumberPayMoney.toFixed(frontCommonPage.param.fixVaue) + "</div>";
						str += "  <div class='child child7' style='width:90px'>" + userOrder.yetAfterCost.toFixed(frontCommonPage.param.fixVaue) + "</div>";
						str += "  <div class='child child8'>" + userOrder.winCostTotal.toFixed(frontCommonPage.param.fixVaue) + "</div>";
						str += "  <div class='child child8'>" + userOrder.lotteryModelStr + "</div>";
						str += "  <div class='child child10' style='width:130px'>" + userOrder.createdDateStr + "</div>";
						var statusStr = "&nbsp;";
						if (userOrder.afterStatus == 1) {
							statusStr = "未开始";
						} else if (userOrder.afterStatus == 2) {
							statusStr = "进行中";
						} else if (userOrder.afterStatus == 3) {
							statusStr = "已完成";
						}
						str += "  <div class='child child11'style='width:50px'>" + statusStr + "</div>";
						str += "  <div class='child child12 no' style='text-align: center;'><a href='javascript:void(0);' name='lotteryAfterMsgRecord' data-lottery='"
								+ userOrder.lotteryId + "'  data-type='" + userOrder.stopAfterNumber + "' data-value='" + userOrder.id + "'>查看</a></div>";
						str += "</div>";

						userOrderListObj.append(str);
						str = "";
					}

					var afterNumberLotteryWindow;

				} else {
					$("#userTodayLotteryAfterList").html("<div class='line'><p class='no-msg'>还没有投注记录哟~</p></div>");
				}

				// 查看投注详情
				$("a[name='lotteryAfterMsgRecord']").unbind("click").click(function(event) {
					var orderId = $(this).attr("data-value");
					var dataType = $(this).attr("data-type");
					var width = 800;
					var height = 400;
					var left = parseInt((screen.availWidth / 2) - (width / 2));// 屏幕居中
					var top = parseInt((screen.availHeight / 2) - (height / 2));
					var windowFeatures = "width=" + width + ",height=" + height + ",status,resizable,scrollbars=yes,left=" + left + ",top=" + top;

					if (dataType == 0) {
						var lotteryhrefStr = contextPath + "/gameBet/orderDetail.html?orderId="+orderId;
					} else {
						var dataLottery = $(this).attr("data-lottery");
						var lotteryhrefStr = contextPath + "/gameBet/followOrderDetail.html?orderId="+dataLottery;
					}
					afterNumberLotteryWindow = window.open(lotteryhrefStr, "subWind", windowFeatures);
					afterNumberLotteryWindow.focus();
				});
			}else if(result.code == "error"){
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
}
