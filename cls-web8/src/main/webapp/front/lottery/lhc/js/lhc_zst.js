function LhcZstPage() {
}
var lhcZstPage = new LhcZstPage();

lhcZstPage.param = {
	lotteryKind : "LHC",
	nearExpectNums : 30
// 默认是近30期的数据
}
lhcZstPage.animalArr = [ "鼠", "牛", "虎", "兔", "龙", "蛇", "马", "羊", "猴", "鸡", "狗", "猪" ];
lhcZstPage.boshe = {
	hongbo : [ 1, 2, 7, 8, 12, 13, 18, 19, 23, 24, 29, 30, 34, 35, 40, 45, 46 ],
	lanbo : [ 3, 4, 9, 10, 14, 15, 20, 25, 26, 31, 36, 37, 41, 42, 47, 48 ],
	lvbo : [ 5, 6, 11, 16, 17, 21, 22, 27, 28, 32, 33, 38, 39, 43, 44, 49 ]
};
$(document).ready(function() {
	lhcZstPage.getZstData();
	$("#nearest30Expect").css("color", "#f8a525");
	// 30期数据
	$("#nearest30Expect").unbind("click").click(function() {
		$("a[name='nearestExpect']").each(function() {
			$(this).css("color", "#3B45A7");
		});
		$(this).css("color", "#f8a525");
		lhcZstPage.param.nearExpectNums = 30;
		lhcZstPage.getZstData();
	});

	// 50期数据
	$("#nearest50Expect").unbind("click").click(function() {
		$("a[name='nearestExpect']").each(function() {
			$(this).css("color", "#3B45A7");
		});
		$(this).css("color", "#f8a525");
		lhcZstPage.param.nearExpectNums = 50;
		lhcZstPage.getZstData();
	});

	// 100期数据
	$("#nearest100Expect").unbind("click").click(function() {
		$("a[name='nearestExpect']").each(function() {
			$(this).css("color", "#3B45A7");
		});
		$(this).css("color", "#f8a525");
		lhcZstPage.param.nearExpectNums = 100;
		lhcZstPage.getZstData();
	});

});

/**
 * 查询当对应的走势图数据
 */
LhcZstPage.prototype.getZstData = function() {
	// 加载前清空
	$("#expectList").html("");
	// 加载中的样式
	$(".loading").show();
	$(".loading-bg").show();

	var queryParam = {};
	queryParam.lotteryKind = lhcZstPage.param.lotteryKind;
	queryParam.nearExpectNums = lhcZstPage.param.nearExpectNums;

	var jsonData = {
		"zstQueryVo" : queryParam,
	};
	$.ajax({
		type : "POST",
		url : contextPath + "/code/getZSTByCondition",
		data : JSON.stringify(jsonData),
		dataType : "json",
		contentType : 'application/json;charset=utf-8',
		success : function(result) {
			commonHandlerResult(result, this.url);
			if (result.code == "ok") {
				lhcZstPage.showZstData(result.data); // 展示走势图数据
			} else if (result.code == "error") {
				frontCommonPage.showKindlyReminder(result.data);
			}
		}
	});
};

/**
 * 展示走势图数据
 */
LhcZstPage.prototype.showZstData = function(zstDatas) {
	var str1 = '<ul class="lists-liuhecai cols cols-50">';
	str1 += '<li class="cols-child cols-title odd">';
	str1 += '<div class="cols"><span>香港六合彩</span></div>';
	str1 += '</li>';
	str1 += '<li class="cols-child cols-title odd">';
	str1 += '<div class="cols cols-10"><span>期号</span></div>';
	str1 += '<div class="cols cols-30"><span>开奖日期</span></div>';
	str1 += '<div class="cols cols-60"><span>开奖号码</span></div>';
	str1 += '</li>';
	var str2 = '<ul class="lists-liuhecai cols cols-50">';
	str2 += '<li class="cols-child cols-title odd">';
	str2 += '<div class="cols cols-20"><span>正码</span></div>';
	str2 += '<div class="cols cols-40"><span>特码</span></div>';
	str2 += '<div class="cols cols-10"><span>特合</span></div>';
	str2 += '<div class="cols cols-30"><span>总数</span></div>';
	str2 += '</li>';
	str2 += '<li class="cols-child cols-title odd">';
	str2 += '<div class="cols cols-20"><span>正码生肖</span></div>';
	str2 += '<div class="cols cols-10"><span>生肖</span></div>';
	str2 += '<div class="cols cols-10"><span>大小/单双</span></div>';
	str2 += '<div class="cols cols-10"><span>家禽野兽</span></div>';
	str2 += '<div class="cols cols-10"><span>尾数</span></div>';
	str2 += '<div class="cols cols-10"><span>大小/单双</span></div>';
	str2 += '<div class="cols cols-10"><span>总合</span></div>';
	str2 += '<div class="cols cols-10"><span>单双</span></div>';
	str2 += '<div class="cols cols-10"><span>大小</span></div>';
	str2 += '</li>';
	if (zstDatas != null && zstDatas.length > 0) {
		var expectList = $("#expectList");
		expectList.html("");

		for (var i = 0; i < zstDatas.length; i++) {
			var zstVo = zstDatas[i];
			str1 += '<li class="cols-child cols-mun-1">';
			str1 += '<div class="cols cols-10"><span class="">' + zstVo.lotteryNum + '</span></div>';
			str1 += '<div class="cols cols-30"><span class="">' + zstVo.kjtimeStr + '</span></div>';
			str1 += '<div class="cols cols-60">';
			str1 += '<div class="btn btn-' + lhcZstPage.changeColorByCode(zstVo.code1) + '">' + zstVo.code1 + '</div>';
			str1 += '<div class="btn btn-' + lhcZstPage.changeColorByCode(zstVo.code2) + '">' + zstVo.code2 + '</div>';
			str1 += '<div class="btn btn-' + lhcZstPage.changeColorByCode(zstVo.code3) + '">' + zstVo.code3 + '</div>';
			str1 += '<div class="btn btn-' + lhcZstPage.changeColorByCode(zstVo.code4) + '">' + zstVo.code4 + '</div>';
			str1 += '<div class="btn btn-' + lhcZstPage.changeColorByCode(zstVo.code5) + '">' + zstVo.code5 + '</div>';
			str1 += '<div class="btn btn-' + lhcZstPage.changeColorByCode(zstVo.code6) + '">' + zstVo.code6 + '</div>';
			str1 += '<span class="">+</span>';
			str1 += '<div class="btn btn-' + lhcZstPage.changeColorByCode(zstVo.code7) + '">' + zstVo.code7 + '</div>';
			// str1 += '<div class="green">'+zstVo.code1+'</div>';
			// str1 += '<div class="red">'+zstVo.code2+'</div>';
			// str1 += '<div class="blue">'+zstVo.code3+'</div>';
			// str1 += '<div class="green">'+zstVo.code4+'</div>';
			// str1 += '<div class="green">'+zstVo.code5+'</div>';
			// str1 += '<div class="green">'+zstVo.code6+'</div>';
			// str1 += '<span class="">+</span>';
			// str1 += '<div class="green">'+zstVo.code7+'</div>';
			str1 += '</div></li>';

			str2 += '<li class="cols-child">';
			str2 += '<div class="cols cols-20"><span>' + lhcZstPage.animalArr[zstVo.animal1 - 1] + lhcZstPage.animalArr[zstVo.animal2 - 1]
					+ lhcZstPage.animalArr[zstVo.animal3 - 1] + lhcZstPage.animalArr[zstVo.animal4 - 1] + lhcZstPage.animalArr[zstVo.animal5 - 1]
					+ lhcZstPage.animalArr[zstVo.animal6 - 1] + '</span></div>';
			str2 += '<div class="cols cols-10"><span>' + lhcZstPage.animalArr[zstVo.animal7 - 1] + '</span></div>';
			str2 += '<div class="cols cols-10"><span>' + zstVo.code7DaXiao + "/" + zstVo.code7DanShuang + '</span></div>';
			str2 += '<div class="cols cols-10"><span>' + zstVo.code7JiaQinOrYeShou + '</span></div>';
			str2 += '<div class="cols cols-10"><span>' + (zstVo.code7 % 10) + '</span></div>';
			str2 += '<div class="cols cols-10"><span>' + zstVo.code7HeDaOrHeXiao + '/' + zstVo.code7HeDanOrHeShuang + '</span></div>';
			str2 += '<div class="cols cols-10"><span>' + zstVo.codeZongHe + '</span></div>';
			str2 += '<div class="cols cols-10"><span>' + zstVo.codeZongHeDanShuang + '</span></div>';
			str2 += '<div class="cols cols-10"><span>' + zstVo.codeZongHeDaXiao + '</span></div>';
			str2 += '</li>';
		}
		str1 += "</ul>";
		str2 += "</ul>";

	}
	expectList.html(str1 + str2);
};
// 计算颜色
LhcZstPage.prototype.changeColorByCode = function(lotteryCode) {
	var colors = "";
	var hong = lhcZstPage.boshe.hongbo;
	var lan = lhcZstPage.boshe.lanbo;
	var lv = lhcZstPage.boshe.lvbo;

	// 判断是否是红波
	for (var i = 0; i < hong.length; i++) {
		if (Number(hong[i]) == Number(lotteryCode)) {
			colors = 'red';
			return colors;
		}
	}

	// 判断是否是蓝波
	for (var i = 0; i < lan.length; i++) {
		if (Number(lan[i]) == Number(lotteryCode)) {
			colors = 'blue';
			return colors;
		}

	}

	// 判断是否是绿波
	for (var i = 0; i < lv.length; i++) {
		if (Number(lv[i]) == Number(lotteryCode)) {
			colors = 'green';
			return colors;
		}

	}

	return colors;
}