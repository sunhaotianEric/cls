<%@ page language="java" contentType="text/html; charset=UTF-8"
    import="com.team.lottery.vo.LotterySet,com.team.lottery.vo.BizSystem,java.math.BigDecimal"
    import="com.team.lottery.util.ApplicationContextUtil,com.team.lottery.enums.ELotteryTopKind"
    import="com.team.lottery.util.ConstantUtil,com.team.lottery.service.UserService,com.team.lottery.vo.User,
       com.team.lottery.system.BizSystemConfigVO,com.team.lottery.system.BizSystemConfigManager,
       com.team.lottery.vo.AwardModelConfig,com.team.lottery.system.SystemConfigConstant,com.team.lottery.cache.ClsCacheManager,com.team.lottery.enums.ELotteryKind"
    import="com.team.lottery.extvo.ZipConfig"
    pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	User user = (User)request.getSession().getAttribute(ConstantUtil.USER_LOGIN_MARK_FOR_USER);
	Integer systemLotterySwitch = ClsCacheManager.getLotteryStatusByLotteryType("SUPER_SYSTEM", ELotteryKind.LHC.getCode());
	// 业务系统彩种开关状态.
	Integer bizSystemLotterySwitch = ClsCacheManager.getLotteryStatusByLotteryType(user.getBizSystem(), ELotteryKind.LHC.getCode());
	// 彩种开关状态不为1的时候就跳往首页.
	if((systemLotterySwitch != null && systemLotterySwitch != 1)||(bizSystemLotterySwitch != null && bizSystemLotterySwitch != 1)){
		response.sendRedirect(path+"/gameBet/index.html");
	}
%>        
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>香港六合彩</title>
<jsp:include page="/front/include/include.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/lottery_base.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<link rel="stylesheet" type="text/css" href="<%=path%>/front/css/lottery1.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />

<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/jquery/jquery-ui.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script src="<%=path%>/front/assets/jquery/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.min.css?v=<%=SystemConfigConstant.pcWebRsVersion%>" />
<script src="<%=path%>/front/assets/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<%=path%>/front/js/base.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>

<!-- common js -->
<script type="text/javascript" src="<%=path%>/front/lottery/lhc/js/lottery_lhc_common.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<!-- 业务js -->
<!-- layer弹窗 -->
<%-- <script type="text/javascript" src="<%=path%>/front/lottery/lhc/js/layer.min.js"></script> --%>
<script type="text/javascript" src="<%=path%>/front/lottery/lhc/js/data.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script type="text/javascript" src="<%=path%>/front/lottery/lhc/js/lhc.js?v=<%=SystemConfigConstant.pcWebRsVersion%>"></script>
<script type="text/javascript" src="<%=path%>/resources/jszip/jszip.min.js"></script>
<%
AwardModelConfig clsCacheManager = ClsCacheManager.getAwardModelConfigByCode(user.getBizSystem());

BigDecimal currentSystemLowestAwardModel = clsCacheManager.getSscLowestAwardModel();  //当前系统使用的时时彩最低模式
BigDecimal sscLowestAwardModel = SystemConfigConstant.SSCLowestAwardModel;   //时时彩最低奖金模式
BigDecimal sscHighestAwardModel = SystemConfigConstant.SSCHighestAwardModel; //时时彩最高奖金模式

%>
<script type="text/javascript">
var isZipOpen = <%=ZipConfig.IS_ZIP_OPEN %>;
var currentSystemLowestAwardModel = <%=currentSystemLowestAwardModel.doubleValue() %>;
var lowestAwardModel = <%=sscLowestAwardModel.doubleValue() %>;
var highestAwardModel = <%=sscLowestAwardModel.doubleValue() %>;
var currentUserHighestModel = currentUser.sscRebate;

lotteryCommonPage.lotteryParam.modelValue = currentUserHighestModel;
</script>
<style type="text/css">
.game-order-current{
background: #E6E6E6;
}
.cp-yellow{color:#f8a525}
.cp-red{color:red}
.cp-green{color:green}
.header{z-index:4;}
.fixed{z-index:3;}
</style>
</head>

<body style="background-image:url(<%=path%>/front/images/lottery/bg2.png);">

<jsp:include page="/front/include/header_user.jsp"></jsp:include>

<!-- 需要加载的图片 -->
<div class="stance"></div>

<jsp:include page="/front/include/lottery_common.jsp"></jsp:include>

<div class="main-ml0">
<!-- m-banner -->
<div class="m-banner" >
	<div class="banner-content container">
    	
		 <div class="alert-msg-bg"></div>
		 <div class="alert-msg alert-liuhecai-msg" id="liuhecai_msg">
			 <div class="msg-head">
			       <p>注单确认</p>
			       <img class="close" src="<%=path%>/front/images/user/close.png" onClick='(function msg_show(){$(".alert-liuhecai-msg").stop(false,true).delay(200).fadeOut(500);$(".alert-msg-bg").stop(false,true).fadeOut(500);})()' />
			 </div>
		    <div class="msg-content">
		        <div class="msg-list">
		            <ul>
		                <li class="msg-list-li msg-list-t">
		                    <div class="child child1">序号</div>
		                    <div class="child child2">内容</div>
		                    <div class="child child3">赔率</div>
		                    <div class="child child4">下注金额</div>
		                </li>
		
		            </ul>
		        </div>
		
		        <div class="line no-border">
		            <div class="line-title">期数：</div>
		            <div class="line-content">
		                <div class="line-text"><p >25</p></div>
		            </div>
		        </div>
		        <div class="line no-border">
		            <div class="line-title">盘口：</div>
		            <div class="line-content">
		                <div class="line-text"><p class="blue">A</p></div>
		            </div>
		        </div>
		        <div class="line no-border">
		            <div class="line-title">总下注额：</div>
		            <div class="line-content">
		                <div class="line-text"><p class="blue">10</p></div>
		            </div>
		        </div>
		
		        <div class="btn">
		          <input type="button" class="inputBtn" value="确定"  />
		          <input type="button" class="inputBtn" value="取消" onClick='(function msg_show(){$(".alert-liuhecai-msg").stop(false,true).delay(200).fadeOut(500);$(".alert-msg-bg").stop(false,true).fadeOut(500);})()' />
		        </div>
		    </div>
		 </div>        
        
        <div class="model model1">
            <div class="icon"><img src="<%=path%>/front/images/lottery/logo/icon-liuhecai.png" alt=""></div>
            <!-- <div class="info">
                <h2 class="title">六合彩</h2>
                <p class="p1">总共: <span class="font-red">420</span> 期</p>
                <p class="p2">已开64期,还有23期</p>
            </div> -->
            <div class="info info-single">
                <h2 class="title" style="text-align:center;">香港六合彩</h2>
                <!-- <p class="p1" style="text-align:center;">总共: <span class="font-red">420</span> 期</p>
                <p class="p2" style="text-align:center;">已开64期,还有23期</p> -->
            </div>
        </div>
        <div class="model model2">
            <p class="title">第 <span class="font-red" id="nowNumber">000000-000</span> 期</p>
            <p class="over">离投注截止还有</p>
             <p class="time" id="timer">00<span>:</span>00<span>:</span>00</p>
            <div id="presellTip" style="display: none">
           		<p style="font-size: 24px;color: #FFB700">封盘中...</p>
           	</div>
        </div>

        <div class="model model3">
            <p class="title">第 <span class="font-red" id="J-lottery-info-lastnumber">000000-000</span> 期开奖号码</p>
            <div class="btns btns3">
            	<div class="shengxiao">
					<span id="lotteryNumber1"></span> 
					<span id="lotteryNumber2"></span> 
					<span id="lotteryNumber3"></span> 
					<span id="lotteryNumber4"></span> 
					<span id="lotteryNumber5"></span> 
					<span id="lotteryNumber6"></span> 
					<span style="width:12px;">&nbsp;</span>
					<span id="lotteryNumber7"></span>            	
            	</div>
            	<!-- <div class="btn btn-blue" id="lotteryNumber1">0</div>
                <div class="btn btn-green" id="lotteryNumber2" >0</div>
                <div class="btn btn-green" id="lotteryNumber3">0</div>
                <div class="btn btn-blue" id="lotteryNumber4">0</div>
                <div class="btn btn-blue" id="lotteryNumber5">0</div>
                <div class="btn btn-red" id="lotteryNumber6">0</div>
                <div class="btn btn-add" ></div>
                <div class="btn btn-yellow" id="lotteryNumber7">0</div> -->
            </div>
            <div class="btns2">
            	<span class="btn btn-Music">关闭音乐</span>
            	<audio id="bgMusic" src=""></audio>
                <a href="<%=path%>/gameBet/lottery/lhc/zst_liuhecai.html" target="_blank" class="btn">号码走势</a>
            </div>
        </div>
        
        <div class="model model4">
          <div class="ctitle">
            	<p class="title-child child1">今日开奖</p>
                <p class="title-child child2" style="width:166px;">最新投注</p>
            </div>
            <div class="c-content">
            	<ul class="c-title" id="nearestTenLotteryCode">
                	<li class="child child1">期号</li>
                    <li class="child child2">开奖号</li>
                </ul>
                <!-- <ul class="c-list">
                	<li class="child child1">20150815-049</li>
                    <li class="child child2">6,5,4,9,2</li>
                    <li class="child child3">组六</li>
                </ul>
                <ul class="c-list">
                	<li class="child child1">20150815-049</li>
                    <li class="child child2">6,5,4,9,2</li>
                    <li class="child child3">组六</li>
                </ul> -->
            </div>
            <div id="showAfterLotteryCodeBtn" class="more more1"><img src="<%=path%>/front/images/lottery/head-pointer.png" /></div>
            <div id="showPreLotteryCodeBtn" class="more more2"><img src="<%=path%>/front/images/lottery/head-pointer.png" /></div>
            <div id="preLotteryCode" style="display: none"></div>
            <div id="afterLotteryCode" style="display: none"></div>
        </div>
        <div class="change">
            <div class="child on"><p>近一期</p></div>
            <div class="child"><p>近五期</p></div>
        </div>
    </div>
</div>
<!-- m-banner over -->

<!-- main -->
<div class="main">
	<div class="container">
	<ul class="classify p0-14">
    	
    	<li class="on"><p class="title">特码</p>
        	<div class="classify-list" id="TM"><img class="top" src="<%=path%>/front/images/lottery/list-top.png" />
            </div>
        </li>
    	<li><p class="title">正码</p>
        	<div class="classify-list" id="ZM"><img class="top" src="<%=path%>/front/images/lottery/list-top.png" />
            </div>
        </li>
        <li><p class="title">正码特</p>
        	<div class="classify-list" id="ZTM"><img class="top" src="<%=path%>/front/images/lottery/list-top.png" />
            </div>
        </li>
        <li onClick="lotteryDataDeal.lotteryKindPlayChoose('ZM16',this)"><p class="title">正码1-6</p></li>
        <li><p class="title">连码</p>
       	<div class="classify-list" id="LM"><img class="top" src="<%=path%>/front/images/lottery/list-top.png" />
            </div>
        </li>
        <li onClick="lotteryDataDeal.lotteryKindPlayChoose('BB',this)"><p class="title">半波</p></li>
        <li onClick="lotteryDataDeal.lotteryKindPlayChoose('YXWS',this)"><p class="title">一肖/尾数</p></li> 
        <li onClick="lotteryDataDeal.lotteryKindPlayChoose('TMSX',this)"><p class="title">特码生肖</p></li>
        <li><p class="title">合肖</p>
        	<div class="classify-list" id="HX"><img class="top" src="<%=path%>/front/images/lottery/list-top.png" />
            </div>
        </li>
        <li><p class="title">连肖</p>
        	<div class="classify-list" id="LX"><img class="top" src="<%=path%>/front/images/lottery/list-top.png" />
            </div>
        </li>
        <li><p class="title">尾数连</p>
        	<div class="classify-list" id="WSL"><img class="top" src="<%=path%>/front/images/lottery/list-top.png" />
            </div>
        </li>
        
        <li><p class="title">全不中</p>
        	<div class="classify-list" id="QUZ"><img class="top" src="<%=path%>/front/images/lottery/list-top.png" />
            </div>
        </li>   
      <%--   <div class="right">
         	<div class="r-child" onClick="lotteryDataDeal.lotteryKindPlayChoose('zoushi',this)"><p class="r-title">号码走势</p></div>
         	<div class="r-child"><p class="r-title">游戏说明</p>
               	<div class="r-msg">
               		<img class="pointer" src="<%=path%>/front/images/lottery/msg-top.png" />
           		</div>
           	</div>
        </div> --%>
    </ul>
    <div class="main-content">
    	  <div class="main-title" style="background:#fff;height:50px;line-height:50px;">
          <span class="shortcut-font" >快捷金额 </span>
          <input type="text" class="shortcut-money"  onkeyup="checkNum(this);lotteryDataDeal.colsChildSet();" />
          <div class="quick-money">
            <img src="<%=path%>/front/images/lottery/cc1.png" data-value="1" alt="">
            <img src="<%=path%>/front/images/lottery/cc2.png" data-value="2" alt="">
            <img src="<%=path%>/front/images/lottery/cc5.png" data-value="5" alt="">
            <img src="<%=path%>/front/images/lottery/cc10.png" data-value="10" alt="">
            <img src="<%=path%>/front/images/lottery/cc20.png" data-value="20" alt="">
            <img src="<%=path%>/front/images/lottery/cc50.png" data-value="50" alt="">
            <img src="<%=path%>/front/images/lottery/cc100.png" data-value="100" alt="">
            <img src="<%=path%>/front/images/lottery/cc500.png" data-value="500" alt="">
            <img src="<%=path%>/front/images/lottery/cc1000.png" data-value="1000" alt="">
            <img src="<%=path%>/front/images/lottery/cc5000.png" data-value="5000" alt="">
          </div>
          <a href="javascript:;" class="right btn-rule" ><img src="<%=path%>/front/images/lottery/icon-gamerule.png" alt=""><span>玩法规则</span></a>
           <a href="<%=path%>/gameBet/lottery/lhc/zst_liuhecai.html" target="_bank" class="right"><img src="<%=path%>/front/images/lottery/icon-zst.png" alt=""><span>开奖历史</span></a> 
        </div>
        <!-- lists -->
        <div class="lists" style="margin-right: 160px;">





        <ul class="lists-liuhecai cols cols-20">
          <li class="cols-child cols-title odd">
            <div class="cols cols-33"><span>号码</span></div>
            <div class="cols cols-33"><span>赔率</span></div>
            <div class="cols cols-34"><span>金额</span></div>
          </li>
          
        </ul>


        <ul class="lists-liuhecai cols cols-20">
          <li class="cols-child cols-title odd">
            <div class="cols cols-33"><span>号码</span></div>
            <div class="cols cols-33"><span>赔率</span></div>
            <div class="cols cols-34"><span>金额</span></div>
          </li>
        </ul>




        <ul class="lists-liuhecai cols cols-20">
          <li class="cols-child cols-title odd">
            <div class="cols cols-33"><span>号码</span></div>
            <div class="cols cols-33"><span>赔率</span></div>
            <div class="cols cols-34"><span>金额</span></div>
          </li>
        </ul>




        <ul class="lists-liuhecai cols cols-20">
          <li class="cols-child cols-title odd">
            <div class="cols cols-33"><span>号码</span></div>
            <div class="cols cols-33"><span>赔率</span></div>
            <div class="cols cols-34"><span>金额</span></div>
          </li>
        </ul>



        <ul class="lists-liuhecai cols cols-20">
          <li class="cols-child cols-title odd">
            <div class="cols cols-33"><span>号码</span></div>
            <div class="cols cols-33"><span>赔率</span></div>
            <div class="cols cols-34"><span>金额</span></div>
          </li>
        </ul>



        </div>
        <!-- lists over -->



        <!-- select-lhc -->
        <div class="select-lhc">
        	<div class="buttons">
	            <div class="button button-selectAll button-red" data-type="all" onclick="lotteryDataDeal.select_type(this)">全选</div>
	            <div class="button button-reset button-gray" data-type="reset" onclick="lotteryDataDeal.select_type(this)">重设</div>
	         </div>
          	<div class="select-btns">
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="da">大</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="xiao">小</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="dan">单</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="shuang">双</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="hedan">合单</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="heshuang">合双</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="dadan">大单</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="xiaodan">小单</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="dashuang">大双</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="xiaoshuang">小双</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="weida">尾大</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="weixiao">尾小</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="shu">鼠</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="niu">牛</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="hu">虎</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="tu">兔</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="long">龙</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="she">蛇</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="ma">马</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="yang">羊</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="hou">猴</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="ji">鸡</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="gou">狗</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="zhu">猪</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="hongda">红大</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="hongxiao">红小</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="hongdan">红单</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="hongshuang">红双</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="landa">蓝大</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="lanxiao">蓝小</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="landan">蓝单</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="lanshuang">蓝双</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="lvda">绿大</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="lvxiao">绿小</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="lvdan">绿单</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="lvshuang">绿双</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="hong">红波</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="lan">蓝波</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="lv">绿波</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="all">全选</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="jiaqin">家禽</div>
	            <div class="btn" onclick="lotteryDataDeal.select_type(this)" data-type="yeshou">野兽</div>
          </div>
        </div>
        <!-- select-lhc over -->
        <div class="buttons">
          <div class="button button-lhc button-red" onClick='lotteryCommonPage.showLotteryMsg()' >提交</div>
          <div id="lhcReset" class="button button-lhc button-gray" data-type="reset" onclick="lotteryDataDeal.select_type(this)" >重设</div>
        </div>

        <div style="height:1px;"></div>

    </div>
     <!-- main-msg -->
    <div class="main-msg">
        <div class="titles">
            <div id="userTodayLottery" class="child on">投注记录</div>
            <div id="userTodayLotteryAfter" class="child">追号记录</div>

            <a href="<%=path%>/gameBet/order.html">查看更多</a>
        </div>
        <div class="contents" id="userTodayLotteryList" >
            <div class="line line-titles">
                <div class="child child1">订单编号</div>
                <div class="child child2">彩种</div>
                <div class="child child3">期号</div>
                <div class="child child4">投注金额</div>
                <div class="child child5">奖金</div>
                <div class="child child6">模式</div>
                <div class="child child7">下单时间</div>
                <div class="child child8">状态</div>
                <div class="child child9 no">内容</div>
            </div>
            
        </div>
        <div class="contents" id="userTodayLotteryAfterList" style="display:none;">
            <div class="line line-titles">
         <div class="child child1">订单编号</div>
                <div class="child child12">彩种</div>
                <div class="child child13">开始期数</div>
                <div class="child child14">追号期数</div>
                <div class="child child15">完成期数</div>
                <div class="child child16">总金额</div>
                <div class="child child17">完成金额</div>
                <div class="child child18">中奖金额</div>
                <div class="child child19">模式</div>
                <div class="child child10">下单时间</div>
                <div class="child child11">状态</div>
                <div class="child child12 no">内容</div>
            </div>
            
        </div>
    </div>
    <!-- main-msg over -->
</div></div>
<!-- main over -->
</div>

<!-- alert-msg over -->

	<jsp:include page="/front/include/chat_common.jsp"></jsp:include>

	<jsp:include page="/front/include/footer.jsp"></jsp:include>
<script type="text/javascript">
var quickmoney = 0;
lotteryDataDeal.colsChildClick();
</script>
</body>
</html>
